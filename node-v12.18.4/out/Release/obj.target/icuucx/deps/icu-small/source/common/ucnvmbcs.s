	.file	"ucnvmbcs.cpp"
	.text
	.p2align 4
	.type	_ZL12getStatePropPA256_KiPai, @function
_ZL12getStatePropPA256_KiPai:
.LFB2808:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movslq	%edx, %r15
	pushq	%r14
	movq	%r15, %r8
	addq	%rsi, %r15
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	salq	$10, %r8
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	addq	%rdi, %r8
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	$0, (%r15)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L28:
	cmpb	$0, (%rbx)
	jns	.L4
.L5:
	addq	$1, %r13
	cmpq	$256, %r13
	je	.L26
.L6:
	movl	(%r8,%r13,4), %r12d
	movl	%r13d, %ecx
	movl	%r12d, %eax
	shrl	$24, %eax
	movl	%eax, %edx
	andl	$127, %eax
	leaq	(%r14,%rax), %rbx
	andl	$127, %edx
	cmpb	$-1, (%rbx)
	je	.L27
.L2:
	testl	%r12d, %r12d
	jns	.L28
	shrl	$20, %r12d
	andl	$15, %r12d
	cmpl	$5, %r12d
	ja	.L5
.L4:
	movl	%ecx, %eax
	sarl	$5, %eax
	sall	$3, %eax
	orb	(%r15), %al
	movb	%al, (%r15)
	cmpl	$255, %ecx
	je	.L8
	movl	$254, %eax
	movl	$254, %r11d
	movl	$255, %r12d
	subl	%r13d, %eax
	subq	%rax, %r11
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L30:
	cmpb	$0, (%rbx)
	jns	.L11
.L12:
	subq	$1, %r12
	leal	-1(%r10), %r9d
	cmpq	%r12, %r11
	je	.L11
.L13:
	movl	(%r8,%r12,4), %r13d
	movl	%r12d, %r10d
	movl	%r12d, %r9d
	movl	%r13d, %ebx
	shrl	$24, %ebx
	movl	%ebx, %edx
	andl	$127, %ebx
	addq	%r14, %rbx
	andl	$127, %edx
	cmpb	$-1, (%rbx)
	je	.L29
.L9:
	testl	%r13d, %r13d
	jns	.L30
	shrl	$20, %r13d
	andl	$15, %r13d
	cmpl	$5, %r13d
	ja	.L12
.L11:
	movl	%r9d, %eax
	sarl	$5, %eax
	orb	(%r15), %al
	movb	%al, (%r15)
	cmpl	%r9d, %ecx
	jg	.L1
.L20:
	movslq	%ecx, %r13
.L19:
	movl	(%r8,%r13,4), %r12d
	movl	%r12d, %eax
	shrl	$24, %eax
	movl	%eax, %edx
	andl	$127, %eax
	leaq	(%r14,%rax), %rbx
	andl	$127, %edx
	cmpb	$-1, (%rbx)
	je	.L31
.L15:
	testl	%r12d, %r12d
	js	.L32
.L17:
	addq	$1, %r13
	cmpl	%r13d, %r9d
	jge	.L19
	movzbl	(%r15), %eax
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r8, -72(%rbp)
	movq	%rdi, -56(%rbp)
	movl	%r13d, -64(%rbp)
	call	_ZL12getStatePropPA256_KiPai
	movq	-72(%rbp), %r8
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %rdi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L32:
	shrl	$20, %r12d
	orb	$64, (%rbx)
	andl	$12, %r12d
	jne	.L17
	orb	$64, (%r15)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r14, %rsi
	movq	%r11, -88(%rbp)
	movq	%r8, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	movl	%r12d, -80(%rbp)
	movl	%r12d, -76(%rbp)
	call	_ZL12getStatePropPA256_KiPai
	movq	-88(%rbp), %r11
	movl	-80(%rbp), %r10d
	movl	-76(%rbp), %r9d
	movq	-72(%rbp), %r8
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %rdi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r14, %rsi
	movl	%r9d, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZL12getStatePropPA256_KiPai
	movl	-72(%rbp), %r9d
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rdi
	jmp	.L15
.L26:
	movb	$-64, (%r15)
	addq	$56, %rsp
	movl	$-64, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	orl	$7, %eax
	movl	$255, %r9d
	movb	%al, (%r15)
	jmp	.L20
	.cfi_endproc
.LFE2808:
	.size	_ZL12getStatePropPA256_KiPai, .-_ZL12getStatePropPA256_KiPai
	.p2align 4
	.type	_ZL20writeStage3RoundtripPKvjPi, @function
_ZL20writeStage3RoundtripPKvjPi:
.LFB2816:
	.cfi_startproc
	endbr64
	movzbl	204(%rdi), %eax
	movq	40(%rdi), %r10
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	184(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	cmpb	$8, %al
	je	.L34
	cmpb	$9, %al
	jne	.L36
	cmpl	$16777215, %esi
	ja	.L50
	.p2align 4,,10
	.p2align 3
.L36:
	leal	32(%rsi), %r11d
	movl	$1, %ebx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L52:
	cmpb	$9, %r13b
	je	.L42
	cmpb	$2, %r13b
	je	.L42
	cltq
	movw	%si, (%r12,%rax,2)
.L44:
	addl	$16, %ecx
	movq	%rbx, %rax
	salq	%cl, %rax
	orl	%eax, %edx
	movl	%edx, (%r9)
.L40:
	addl	$1, %esi
	addq	$4, %r8
	cmpl	%r11d, %esi
	je	.L51
.L39:
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	js	.L40
	movl	%ecx, %eax
	movzbl	204(%rdi), %r13d
	sarl	$10, %eax
	cltq
	movzwl	(%r10,%rax,2), %edx
	movl	%ecx, %eax
	andl	$15, %ecx
	sarl	$4, %eax
	andl	$63, %eax
	addq	%rdx, %rax
	leaq	(%r10,%rax,4), %r9
	movl	(%r9), %edx
	movzwl	%dx, %eax
	sall	$4, %eax
	addl	%ecx, %eax
	cmpb	$3, %r13b
	jne	.L52
	cltq
	movl	%esi, (%r12,%rax,4)
	movl	(%r9), %edx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L42:
	leal	(%rax,%rax,2), %eax
	movl	%esi, %edx
	cltq
	shrl	$16, %edx
	addq	%r12, %rax
	movb	%dl, (%rax)
	movl	%esi, %edx
	rolw	$8, %dx
	movw	%dx, 1(%rax)
	movl	(%r9), %edx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L51:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	cmpl	$65535, %esi
	jbe	.L36
	movl	%esi, %eax
	movl	%esi, %edx
	andl	$32767, %eax
	andl	$65407, %edx
	cmpl	$9371647, %esi
	cmova	%edx, %eax
	movl	%eax, %esi
	jmp	.L36
.L50:
	movl	%esi, %eax
	movl	%esi, %edx
	andl	$8388607, %eax
	andl	$16744447, %edx
	cmpl	$-1895825409, %esi
	cmova	%edx, %eax
	movl	%eax, %esi
	jmp	.L36
	.cfi_endproc
.LFE2816:
	.size	_ZL20writeStage3RoundtripPKvjPi, .-_ZL20writeStage3RoundtripPKvjPi
	.p2align 4
	.type	ucnv_MBCSGetName, @function
ucnv_MBCSGetName:
.LFB2821:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdx
	testb	$16, 56(%rdi)
	je	.L54
	movq	272(%rdx), %rax
	testq	%rax, %rax
	je	.L54
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movq	16(%rdx), %rax
	addq	$4, %rax
	ret
	.cfi_endproc
.LFE2821:
	.size	ucnv_MBCSGetName, .-ucnv_MBCSGetName
	.p2align 4
	.type	_ZL18hasValidTrailBytesPA256_Kih, @function
_ZL18hasValidTrailBytesPA256_Kih:
.LFB2825:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movzbl	%sil, %ebx
	salq	$10, %rbx
	addq	%rdi, %rbx
	subq	$8, %rsp
	movl	644(%rbx), %eax
	testl	%eax, %eax
	js	.L76
.L60:
	movl	260(%rbx), %eax
	testl	%eax, %eax
	js	.L77
.L62:
	leaq	1024(%rbx), %r12
	movq	%rbx, %rdx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L63:
	addq	$4, %rdx
	cmpq	%rdx, %r12
	je	.L66
.L64:
	movl	(%rdx), %eax
	testl	%eax, %eax
	jns	.L63
	shrl	$20, %eax
	andl	$15, %eax
	cmpl	$7, %eax
	je	.L63
.L70:
	movl	$1, %r8d
.L59:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	shrl	$24, %esi
	movq	%r13, %rdi
	call	_ZL18hasValidTrailBytesPA256_Kih
	testb	%al, %al
	jne	.L70
.L65:
	addq	$4, %rbx
	cmpq	%rbx, %r12
	je	.L78
.L66:
	movl	(%rbx), %esi
	testl	%esi, %esi
	js	.L65
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L77:
	shrl	$20, %eax
	movl	$1, %r8d
	andl	$15, %eax
	cmpl	$7, %eax
	je	.L62
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	shrl	$20, %eax
	movl	$1, %r8d
	andl	$15, %eax
	cmpl	$7, %eax
	je	.L60
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L59
	.cfi_endproc
.LFE2825:
	.size	_ZL18hasValidTrailBytesPA256_Kih, .-_ZL18hasValidTrailBytesPA256_Kih
	.p2align 4
	.type	_ZL20ucnv_MBCSGetStartersPK10UConverterPaP10UErrorCode, @function
_ZL20ucnv_MBCSGetStartersPK10UConverterPaP10UErrorCode:
.LFB2838:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	56(%rax), %rdx
	movzbl	49(%rax), %eax
	salq	$10, %rax
	leaq	(%rdx,%rax), %rcx
	leaq	1024(%rdx,%rax), %rax
	cmpq	%rax, %rsi
	jnb	.L86
	leaq	256(%rsi), %rax
	cmpq	%rax, %rcx
	jb	.L85
.L86:
	movdqa	.LC0(%rip), %xmm4
	movdqa	.LC1(%rip), %xmm5
	xorl	%eax, %eax
	pxor	%xmm3, %xmm3
	.p2align 4,,10
	.p2align 3
.L84:
	movdqu	(%rcx,%rax,4), %xmm7
	movdqa	%xmm3, %xmm0
	movdqa	%xmm3, %xmm1
	movdqu	48(%rcx,%rax,4), %xmm6
	pcmpgtd	%xmm7, %xmm0
	movdqu	16(%rcx,%rax,4), %xmm7
	pcmpgtd	%xmm7, %xmm1
	movdqu	32(%rcx,%rax,4), %xmm7
	pandn	%xmm4, %xmm0
	movdqa	%xmm0, %xmm2
	pandn	%xmm4, %xmm1
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	movdqa	%xmm3, %xmm2
	punpcklwd	%xmm1, %xmm0
	pcmpgtd	%xmm6, %xmm2
	movdqa	%xmm3, %xmm1
	pcmpgtd	%xmm7, %xmm1
	pand	%xmm5, %xmm0
	pandn	%xmm4, %xmm2
	pandn	%xmm4, %xmm1
	movdqa	%xmm1, %xmm6
	punpcklwd	%xmm2, %xmm1
	punpckhwd	%xmm2, %xmm6
	movdqa	%xmm1, %xmm2
	punpckhwd	%xmm6, %xmm2
	punpcklwd	%xmm6, %xmm1
	punpcklwd	%xmm2, %xmm1
	pand	%xmm5, %xmm1
	packuswb	%xmm1, %xmm0
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	$256, %rax
	jne	.L84
	ret
.L85:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L81:
	movl	(%rcx,%rdx,4), %eax
	notl	%eax
	shrl	$31, %eax
	movb	%al, (%rsi,%rdx)
	addq	$1, %rdx
	cmpq	$256, %rdx
	jne	.L81
	ret
	.cfi_endproc
.LFE2838:
	.size	_ZL20ucnv_MBCSGetStartersPK10UConverterPaP10UErrorCode, .-_ZL20ucnv_MBCSGetStartersPK10UConverterPaP10UErrorCode
	.p2align 4
	.type	_ZL17ucnv_MBCSWriteSubP25UConverterFromUnicodeArgsiP10UErrorCode, @function
_ZL17ucnv_MBCSWriteSubP25UConverterFromUnicodeArgsiP10UErrorCode:
.LFB2840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	cmpb	$0, 94(%rax)
	movq	48(%rax), %r10
	je	.L92
	cmpq	$0, 288(%r10)
	je	.L93
	movsbl	95(%rax), %edx
.L94:
	testl	%edx, %edx
	je	.L92
	movb	$0, 95(%rax)
	cmpb	$12, 252(%r10)
	leaq	94(%rax), %rsi
	movl	$1, %edx
	jne	.L96
.L95:
	leaq	-12(%rbp), %r10
	cmpl	$2, 80(%rax)
	movq	%r10, %rdx
	jne	.L100
	movl	$1, 80(%rax)
	leaq	-11(%rbp), %rdx
	movb	$15, -12(%rbp)
.L100:
	movzbl	(%rsi), %eax
	leaq	1(%rdx), %r9
	movq	%r10, %rsi
	movb	%al, (%rdx)
	movl	%r9d, %edx
	subl	%r10d, %edx
.L96:
	call	ucnv_cbFromUWriteBytes_67@PLT
.L91:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movzbl	89(%rax), %r9d
	movq	40(%rax), %rsi
	movb	$0, 95(%rax)
	cmpb	$12, 252(%r10)
	movsbl	%r9b, %edx
	jne	.L96
	cmpb	$1, %r9b
	je	.L95
	cmpb	$2, %r9b
	jne	.L116
	leaq	-12(%rbp), %r10
	cmpl	$1, 80(%rax)
	movq	%r10, %rdx
	ja	.L102
	movl	$2, 80(%rax)
	leaq	-11(%rbp), %rdx
	movb	$14, -12(%rbp)
.L102:
	movzbl	(%rsi), %eax
	leaq	2(%rdx), %r9
	movb	%al, (%rdx)
	movzbl	1(%rsi), %eax
	movq	%r10, %rsi
	movb	%al, 1(%rdx)
	movl	%r9d, %edx
	subl	%r10d, %edx
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L116:
	movl	$1, (%r8)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%edx, %edx
	cmpw	$255, 140(%rax)
	setbe	%dl
	jmp	.L94
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2840:
	.size	_ZL17ucnv_MBCSWriteSubP25UConverterFromUnicodeArgsiP10UErrorCode, .-_ZL17ucnv_MBCSWriteSubP25UConverterFromUnicodeArgsiP10UErrorCode
	.p2align 4
	.type	_ZL15ucnv_MBCSUnloadP20UConverterSharedData, @function
_ZL15ucnv_MBCSUnloadP20UConverterSharedData:
.LFB2819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	uprv_free_67@PLT
.L118:
	cmpb	$0, 50(%rbx)
	jne	.L129
.L119:
	movq	280(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L120
	call	ucnv_unload_67@PLT
.L120:
	movq	264(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L117
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	56(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L119
	.cfi_endproc
.LFE2819:
	.size	_ZL15ucnv_MBCSUnloadP20UConverterSharedData, .-_ZL15ucnv_MBCSUnloadP20UConverterSharedData
	.p2align 4
	.type	_ZL9_extFromUP10UConverterPK20UConverterSharedDataiPPKDsS5_PPhPKhPPiiaP10UErrorCode, @function
_ZL9_extFromUP10UConverterPK20UConverterSharedDataiPPKDsS5_PPhPKhPPiiaP10UErrorCode:
.LFB2813:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$40, %rsp
	movq	16(%rbp), %r15
	movq	24(%rbp), %r10
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movl	40(%rbp), %eax
	movq	48(%rbp), %r14
	movb	$0, 95(%r12)
	movq	288(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L135
	subq	$8, %rsp
	movsbl	%al, %eax
	movq	%r12, %rdi
	movq	%r10, -72(%rbp)
	pushq	%r14
	pushq	%rax
	movl	32(%rbp), %eax
	pushq	%rax
	pushq	%r10
	pushq	%r15
	call	ucnv_extInitialMatchFromU_67@PLT
	addq	$48, %rsp
	movq	-72(%rbp), %r10
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L130
.L135:
	testb	$-128, 57(%r12)
	jne	.L132
.L133:
	movl	$10, (%r14)
	movl	%ebx, %eax
.L130:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L165
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	cmpl	$65535, %ebx
	jbe	.L136
	cmpl	$1114111, %ebx
	jbe	.L166
.L138:
	cmpl	$7743, %ebx
	jbe	.L140
	cmpl	$8207, %ebx
	jbe	.L154
	cmpl	$59492, %ebx
	jbe	.L140
	cmpl	$63787, %ebx
	jbe	.L167
.L141:
	cmpl	$64041, %ebx
	jbe	.L142
	cmpl	$65071, %ebx
	jbe	.L168
.L143:
	cmpl	$18871, %ebx
	jbe	.L145
	cmpl	$19574, %ebx
	jbe	.L169
.L146:
	cmpl	$18317, %ebx
	jbe	.L147
	cmpl	$18758, %ebx
	jbe	.L170
.L148:
	cmpl	$65509, %ebx
	jbe	.L133
	leaq	208+_ZL13gb18030Ranges(%rip), %rax
	movl	$65510, %edx
	cmpl	$65535, %ebx
	ja	.L133
	.p2align 4,,10
	.p2align 3
.L137:
	movl	8(%rax), %eax
	movl	$3435973837, %esi
	pushq	%r14
	movq	%r10, %r9
	movq	%r15, %r8
	leal	-1687218(%rbx,%rax), %ecx
	subl	%edx, %ecx
	movl	%ecx, %eax
	imulq	%rsi, %rax
	movq	%rax, %rdx
	shrq	$36, %rax
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %edi
	addl	%edi, %edi
	subl	%edi, %ecx
	movq	%r12, %rdi
	addl	$48, %ecx
	movb	%cl, -57(%rbp)
	movl	$2181570691, %ecx
	imulq	%rcx, %rax
	shrq	$37, %rax
	imull	$126, %eax, %ecx
	subl	%ecx, %edx
	subl	$127, %edx
	movb	%dl, -58(%rbp)
	movl	%eax, %edx
	imulq	%rsi, %rdx
	leaq	-60(%rbp), %rsi
	shrq	$35, %rdx
	leal	(%rdx,%rdx,4), %ecx
	subl	$127, %edx
	addl	%ecx, %ecx
	movb	%dl, -60(%rbp)
	movl	$4, %edx
	subl	%ecx, %eax
	movq	%r13, %rcx
	addl	$48, %eax
	movb	%al, -59(%rbp)
	movl	32(%rbp), %eax
	pushq	%rax
	call	ucnv_fromUWriteBytes_67@PLT
	popq	%rax
	xorl	%eax, %eax
	popq	%rdx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L136:
	cmpl	$40869, %ebx
	ja	.L171
	cmpl	$1105, %ebx
	jbe	.L138
	cmpl	$7742, %ebx
	ja	.L138
	leaq	32+_ZL13gb18030Ranges(%rip), %rax
	movl	$1106, %edx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L140:
	cmpl	$9794, %ebx
	ja	.L172
.L142:
	cmpl	$15584, %ebx
	ja	.L173
	cmpl	$13850, %ebx
	jbe	.L143
	cmpl	$14615, %ebx
	ja	.L143
	leaq	128+_ZL13gb18030Ranges(%rip), %rax
	movl	$13851, %edx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L145:
	cmpl	$16735, %ebx
	jbe	.L146
	cmpl	$17206, %ebx
	ja	.L146
	leaq	160+_ZL13gb18030Ranges(%rip), %rax
	movl	$16736, %edx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L147:
	cmpl	$17622, %ebx
	jbe	.L133
	cmpl	$17995, %ebx
	ja	.L148
	leaq	192+_ZL13gb18030Ranges(%rip), %rax
	movl	$17623, %edx
	jmp	.L137
.L166:
	leaq	_ZL13gb18030Ranges(%rip), %rax
	movl	$65536, %edx
	jmp	.L137
.L171:
	cmpl	$55295, %ebx
	ja	.L138
	leaq	16+_ZL13gb18030Ranges(%rip), %rax
	movl	$40870, %edx
	jmp	.L137
.L154:
	leaq	48+_ZL13gb18030Ranges(%rip), %rax
	movl	$7744, %edx
	jmp	.L137
.L167:
	leaq	64+_ZL13gb18030Ranges(%rip), %rax
	movl	$59493, %edx
	jmp	.L137
.L172:
	cmpl	$11904, %ebx
	ja	.L141
	leaq	80+_ZL13gb18030Ranges(%rip), %rax
	movl	$9795, %edx
	jmp	.L137
.L173:
	cmpl	$16469, %ebx
	ja	.L143
	leaq	112+_ZL13gb18030Ranges(%rip), %rax
	movl	$15585, %edx
	jmp	.L137
.L168:
	leaq	96+_ZL13gb18030Ranges(%rip), %rax
	movl	$64042, %edx
	jmp	.L137
.L169:
	leaq	144+_ZL13gb18030Ranges(%rip), %rax
	movl	$18872, %edx
	jmp	.L137
.L170:
	leaq	176+_ZL13gb18030Ranges(%rip), %rax
	movl	$18318, %edx
	jmp	.L137
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2813:
	.size	_ZL9_extFromUP10UConverterPK20UConverterSharedDataiPPKDsS5_PPhPKhPPiiaP10UErrorCode, .-_ZL9_extFromUP10UConverterPK20UConverterSharedDataiPPKDsS5_PPhPKhPPiiaP10UErrorCode
	.p2align 4
	.type	_ZL7_extToUP10UConverterPK20UConverterSharedDataaPPKhS5_PPDsPKDsPPiiaP10UErrorCode, @function
_ZL7_extToUP10UConverterPK20UConverterSharedDataaPPKhS5_PPDsPKDsPPiiaP10UErrorCode:
.LFB2814:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	movl	40(%rbp), %eax
	.cfi_offset 14, -24
	movq	48(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	288(%rsi), %rsi
	movl	%edx, %ebx
	testq	%rsi, %rsi
	je	.L179
	subq	$8, %rsp
	movsbl	%al, %eax
	movsbl	%dl, %edx
	pushq	%r14
	pushq	%rax
	movl	32(%rbp), %eax
	pushq	%rax
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	ucnv_extInitialMatchToU_67@PLT
	addq	$48, %rsp
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L174
.L179:
	cmpb	$4, %bl
	je	.L211
.L177:
	movl	$10, (%r14)
	movl	%ebx, %eax
.L174:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	testb	$-128, 57(%r12)
	je	.L177
	movzbl	65(%r12), %eax
	leal	(%rax,%rax,4), %edx
	movzbl	66(%r12), %eax
	leal	(%rax,%rdx,2), %eax
	cltq
	movq	%rax, %rdx
	salq	$6, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	movzbl	67(%r12), %edx
	leaq	(%rdx,%rax,2), %rax
	leal	(%rax,%rax,4), %edx
	movzbl	68(%r12), %eax
	leal	(%rax,%rdx,2), %esi
	cmpl	$1876217, %esi
	jbe	.L180
	cmpl	$2924793, %esi
	jbe	.L212
.L182:
	cmpl	$1694675, %esi
	jbe	.L184
	cmpl	$1695139, %esi
	jbe	.L197
	cmpl	$1720767, %esi
	jbe	.L184
	cmpl	$1725062, %esi
	jbe	.L213
.L185:
	cmpl	$1725295, %esi
	jbe	.L186
	cmpl	$1726325, %esi
	jbe	.L214
.L187:
	cmpl	$1705178, %esi
	jbe	.L189
	cmpl	$1705881, %esi
	jbe	.L215
.L190:
	cmpl	$1704635, %esi
	jbe	.L191
	cmpl	$1705076, %esi
	jbe	.L216
.L192:
	cmpl	$1726611, %esi
	jbe	.L177
	leaq	208+_ZL13gb18030Ranges(%rip), %rax
	movl	$1726612, %edx
	cmpl	$1726637, %esi
	ja	.L177
	.p2align 4,,10
	.p2align 3
.L181:
	subq	$8, %rsp
	movl	(%rax), %eax
	movl	$0, (%r14)
	movq	%r12, %rdi
	movl	32(%rbp), %r9d
	movq	24(%rbp), %r8
	pushq	%r14
	subl	%edx, %eax
	movq	16(%rbp), %rcx
	movq	%r13, %rdx
	addl	%eax, %esi
	call	ucnv_toUWriteCodePoint_67@PLT
	popq	%rax
	xorl	%eax, %eax
	popq	%rdx
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L180:
	cmpl	$1706260, %esi
	ja	.L217
	cmpl	$1688037, %esi
	jbe	.L182
	cmpl	$1694674, %esi
	ja	.L182
	leaq	32+_ZL13gb18030Ranges(%rip), %rax
	movl	$1688038, %edx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L184:
	cmpl	$1696436, %esi
	ja	.L218
.L186:
	cmpl	$1701915, %esi
	ja	.L219
	cmpl	$1700190, %esi
	jbe	.L187
	cmpl	$1700955, %esi
	ja	.L187
	leaq	128+_ZL13gb18030Ranges(%rip), %rax
	movl	$1700191, %edx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L189:
	cmpl	$1703064, %esi
	jbe	.L190
	cmpl	$1703535, %esi
	ja	.L190
	leaq	160+_ZL13gb18030Ranges(%rip), %rax
	movl	$1703065, %edx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L191:
	cmpl	$1703946, %esi
	jbe	.L177
	cmpl	$1704319, %esi
	ja	.L192
	leaq	192+_ZL13gb18030Ranges(%rip), %rax
	movl	$1703947, %edx
	jmp	.L181
.L216:
	leaq	176+_ZL13gb18030Ranges(%rip), %rax
	movl	$1704636, %edx
	jmp	.L181
.L217:
	cmpl	$1720686, %esi
	ja	.L182
	leaq	16+_ZL13gb18030Ranges(%rip), %rax
	movl	$1706261, %edx
	jmp	.L181
.L212:
	leaq	_ZL13gb18030Ranges(%rip), %rax
	movl	$1876218, %edx
	jmp	.L181
.L213:
	leaq	64+_ZL13gb18030Ranges(%rip), %rax
	movl	$1720768, %edx
	jmp	.L181
.L197:
	leaq	48+_ZL13gb18030Ranges(%rip), %rax
	movl	$1694676, %edx
	jmp	.L181
.L218:
	cmpl	$1698546, %esi
	ja	.L185
	leaq	80+_ZL13gb18030Ranges(%rip), %rax
	movl	$1696437, %edx
	jmp	.L181
.L219:
	cmpl	$1702800, %esi
	ja	.L187
	leaq	112+_ZL13gb18030Ranges(%rip), %rax
	movl	$1701916, %edx
	jmp	.L181
.L214:
	leaq	96+_ZL13gb18030Ranges(%rip), %rax
	movl	$1725296, %edx
	jmp	.L181
.L215:
	leaq	144+_ZL13gb18030Ranges(%rip), %rax
	movl	$1705179, %edx
	jmp	.L181
	.cfi_endproc
.LFE2814:
	.size	_ZL7_extToUP10UConverterPK20UConverterSharedDataaPPKhS5_PPDsPKDsPPiiaP10UErrorCode, .-_ZL7_extToUP10UConverterPK20UConverterSharedDataaPPKhS5_PPDsPKDsPPiiaP10UErrorCode
	.p2align 4
	.globl	ucnv_MBCSToUnicodeWithOffsets_67
	.type	ucnv_MBCSToUnicodeWithOffsets_67, @function
ucnv_MBCSToUnicodeWithOffsets_67:
.LFB2827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -112(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	cmpb	$0, 282(%rax)
	movq	%rax, -104(%rbp)
	jg	.L221
.L225:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	48(%rax), %rax
	movq	48(%rdx), %rbx
	movq	16(%rdx), %rcx
	movq	32(%rdx), %r10
	cmpb	$1, 48(%rax)
	movq	24(%rdx), %r12
	movq	%rbx, -128(%rbp)
	movq	40(%rdx), %r15
	je	.L457
	movq	-104(%rbp), %rdx
	movq	%rbx, -64(%rbp)
	movq	72(%rax), %rbx
	movq	64(%rax), %r13
	movq	%rcx, -80(%rbp)
	testb	$16, 56(%rdx)
	movl	72(%rdx), %r8d
	movq	%r10, -72(%rbp)
	movq	%rbx, -160(%rbp)
	movzbl	64(%rdx), %ebx
	cmove	56(%rax), %r13
	movb	%bl, -96(%rbp)
	leaq	65(%rdx), %rbx
	movl	76(%rdx), %edx
	movq	%rbx, -144(%rbp)
	movl	%edx, %ebx
	testb	%dl, %dl
	je	.L458
.L276:
	xorl	%edx, %edx
	cmpb	$0, -96(%rbp)
	setne	%dl
	negl	%edx
	cmpq	%rcx, %r12
	jbe	.L277
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L278:
	cmpq	%r10, %r15
	jbe	.L344
	cmpb	$0, -96(%rbp)
	jne	.L280
	cmpq	$0, -64(%rbp)
	movq	%r10, %rsi
	je	.L288
	.p2align 4,,10
	.p2align 3
.L293:
	movzbl	%bl, %edi
	movzbl	(%rcx), %eax
	movq	%rsi, %r10
	salq	$10, %rdi
	addq	%r13, %rdi
	movl	(%rdi,%rax,4), %eax
	testl	%eax, %eax
	js	.L289
	movl	%eax, %r11d
	movl	%eax, %r8d
	leaq	1(%rcx), %rdi
	movq	%rdi, -80(%rbp)
	shrl	$24, %r11d
	andl	$16777215, %r8d
	cmpq	%r12, %rdi
	jnb	.L290
	movl	%r11d, %eax
	movzbl	1(%rcx), %r9d
	salq	$10, %rax
	addq	%r13, %rax
	movl	(%rax,%r9,4), %eax
	testl	%eax, %eax
	jns	.L290
	movl	%eax, %r9d
	shrl	$20, %r9d
	andl	$15, %r9d
	cmpl	$4, %r9d
	je	.L459
.L290:
	addl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L443:
	movzbl	(%rcx), %ecx
	movq	-104(%rbp), %rbx
	movl	$1, %esi
	movb	%cl, 65(%rbx)
	movq	%rdi, %rcx
.L285:
	cmpq	%rcx, %r12
	jbe	.L460
	cmpq	%r10, %r15
	jbe	.L461
	leal	1(%rsi), %ebx
	leaq	1(%rcx), %rdi
	addl	$1, %r14d
	movq	%rdi, -80(%rbp)
	movzbl	(%rcx), %ecx
	movb	%bl, -96(%rbp)
	movq	-144(%rbp), %rbx
	movb	%cl, (%rbx,%rsi)
	movl	%eax, %esi
	movq	-80(%rbp), %rcx
	shrl	$24, %esi
	movl	%esi, %ebx
	testl	%eax, %eax
	jns	.L462
.L297:
	movq	-104(%rbp), %rbx
	movzbl	%r11b, %edi
	movl	%edi, 76(%rbx)
	movl	%eax, %edi
	movl	%esi, %ebx
	shrl	$20, %edi
	andl	$127, %ebx
	movl	%edi, %esi
	andl	$15, %esi
	cmpb	$4, %sil
	je	.L463
	testb	%sil, %sil
	je	.L445
	cmpb	$5, %sil
	je	.L464
	andl	$13, %edi
	cmpb	$1, %dil
	jne	.L314
	andl	$1048575, %eax
	movq	-72(%rbp), %rsi
	movl	%eax, %edi
	sarl	$10, %edi
	leaq	2(%rsi), %r10
	orw	$-10240, %di
	movq	%r10, -72(%rbp)
	movw	%di, (%rsi)
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L315
	leaq	4(%rdi), %r8
	andw	$1023, %ax
	movq	%r8, -64(%rbp)
	orw	$-9216, %ax
	movl	%edx, (%rdi)
	cmpq	%r15, %r10
	jnb	.L454
.L316:
	leaq	4(%rsi), %r10
	movq	%r10, -72(%rbp)
	movw	%ax, 2(%rsi)
	leaq	8(%rdi), %rax
	movq	%rax, -64(%rbp)
	movl	%edx, 4(%rdi)
.L442:
	movb	$0, -96(%rbp)
	movl	%r14d, %edx
	xorl	%r8d, %r8d
.L470:
	cmpq	%rcx, %r12
	ja	.L278
.L449:
	movq	-64(%rbp), %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L277:
	movq	-104(%rbp), %rax
	movzbl	-96(%rbp), %edx
	movzbl	%bl, %ebx
	movl	%r8d, 72(%rax)
	movl	%ebx, 76(%rax)
	movb	%dl, 64(%rax)
.L451:
	movq	-112(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rcx, 16(%rax)
	movq	%r10, 32(%rax)
	movq	%rdx, 48(%rax)
.L220:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L465
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movq	-160(%rbp), %rbx
	movzwl	%ax, %r9d
	addl	%r8d, %r9d
	movzwl	(%rbx,%r9,2), %r9d
	cmpw	$-3, %r9w
	ja	.L443
	shrl	$24, %eax
	leaq	2(%rsi), %rdi
	addq	$2, %rcx
	xorl	%r8d, %r8d
	movl	%eax, %ebx
	movq	%rcx, -80(%rbp)
	movq	%rdi, %r10
	movq	%rdi, -72(%rbp)
	andl	$127, %ebx
	movw	%r9w, (%rsi)
	cmpq	%rcx, %r12
	jbe	.L286
.L467:
	movq	%rdi, %rsi
	cmpq	%rdi, %r15
	jbe	.L344
.L288:
	movzbl	%bl, %edi
	movzbl	(%rcx), %eax
	movq	%rsi, %r10
	salq	$10, %rdi
	addq	%r13, %rdi
	movl	(%rdi,%rax,4), %eax
	testl	%eax, %eax
	js	.L282
	movl	%eax, %r11d
	movl	%eax, %r8d
	leaq	1(%rcx), %rdi
	movq	%rdi, -80(%rbp)
	shrl	$24, %r11d
	andl	$16777215, %r8d
	cmpq	%r12, %rdi
	jnb	.L443
	movl	%r11d, %eax
	movzbl	1(%rcx), %r9d
	salq	$10, %rax
	addq	%r13, %rax
	movl	(%rax,%r9,4), %eax
	testl	%eax, %eax
	jns	.L443
	movl	%eax, %r9d
	shrl	$20, %r9d
	andl	$15, %r9d
	cmpl	$4, %r9d
	jne	.L443
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L289:
	cmpl	$-2146435072, %eax
	jge	.L353
	leaq	2(%rsi), %rdi
	addq	$1, %rcx
	xorl	%ebx, %ebx
	movq	%rcx, -80(%rbp)
	movq	%rdi, %r10
	movq	%rdi, -72(%rbp)
	movw	%ax, (%rsi)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L292
	leaq	4(%rax), %rsi
	movq	%rsi, -64(%rbp)
	movl	%edx, (%rax)
	leal	1(%r14), %edx
	movl	%edx, %r14d
.L292:
	cmpq	%rcx, %r12
	jbe	.L286
	movq	%rdi, %rsi
	cmpq	%rdi, %r15
	ja	.L293
.L344:
	movq	-120(%rbp), %rax
	movl	$15, (%rax)
	movq	-64(%rbp), %rax
	movq	%rax, -128(%rbp)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L282:
	cmpl	$-2146435072, %eax
	jge	.L353
	leaq	2(%rsi), %rdi
	addq	$1, %rcx
	xorl	%ebx, %ebx
	movq	%rcx, -80(%rbp)
	movq	%rdi, %r10
	movq	%rdi, -72(%rbp)
	movw	%ax, (%rsi)
	cmpq	%rcx, %r12
	ja	.L467
	.p2align 4,,10
	.p2align 3
.L286:
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r10
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	1(%rcx), %rax
	movq	-144(%rbp), %rdi
	movl	%ebx, %r11d
	addl	$1, %r14d
	movq	%rax, -80(%rbp)
	movsbq	-96(%rbp), %rax
	movzbl	(%rcx), %ecx
	movq	%rax, %rsi
	movb	%cl, (%rdi,%rax)
	movzbl	%bl, %eax
	addl	$1, %esi
	salq	$10, %rax
	movb	%sil, -96(%rbp)
	addq	%r13, %rax
	movl	(%rax,%rcx,4), %eax
	movq	-80(%rbp), %rcx
	movl	%eax, %esi
	shrl	$24, %esi
	movl	%esi, %ebx
	testl	%eax, %eax
	js	.L297
.L462:
	andl	$16777215, %eax
	movq	-72(%rbp), %r10
	addl	%eax, %r8d
.L298:
	cmpq	%rcx, %r12
	ja	.L278
	movq	-64(%rbp), %rax
	movq	%rax, -128(%rbp)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L459:
	movq	-160(%rbp), %rbx
	movzwl	%ax, %r9d
	addl	%r8d, %r9d
	movzwl	(%rbx,%r9,2), %r9d
	cmpw	$-3, %r9w
	ja	.L290
	leaq	2(%rsi), %rdi
	addq	$2, %rcx
	movq	%rcx, -80(%rbp)
	movq	%rdi, %r10
	movq	%rdi, -72(%rbp)
	movw	%r9w, (%rsi)
	movq	-64(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L291
	leaq	4(%rsi), %r8
	movq	%r8, -64(%rbp)
	movl	%edx, (%rsi)
	leal	2(%r14), %edx
	movl	%edx, %r14d
.L291:
	shrl	$24, %eax
	xorl	%r8d, %r8d
	movl	%eax, %ebx
	andl	$127, %ebx
	jmp	.L292
.L471:
	movq	-104(%rbp), %rsi
	movq	48(%rsi), %rsi
	movl	52(%rsi), %edi
	testl	%edi, %edi
	je	.L302
	movq	80(%rsi), %r11
	xorl	%r8d, %r8d
	leal	-1(%rdi), %r9d
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L469:
	leal	(%rdi,%r8), %esi
	shrl	%esi
	movl	%esi, %r10d
	cmpl	(%r11,%r10,8), %eax
	jnb	.L468
	movl	%esi, %edi
	leal	-1(%rsi), %r9d
.L304:
	cmpl	%r8d, %r9d
	ja	.L469
	leaq	(%r11,%r8,8), %rsi
	cmpl	(%rsi), %eax
	jne	.L302
	movl	4(%rsi), %eax
	cmpl	$65534, %eax
	je	.L302
	.p2align 4,,10
	.p2align 3
.L445:
	movq	-72(%rbp), %rsi
	leaq	2(%rsi), %r10
	movq	%r10, -72(%rbp)
	movw	%ax, (%rsi)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L442
.L441:
	leaq	4(%rax), %rsi
	xorl	%r8d, %r8d
	movq	%rsi, -64(%rbp)
	movl	%edx, (%rax)
	movl	%r14d, %edx
	movb	$0, -96(%rbp)
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L463:
	movzwl	%ax, %eax
	movq	-160(%rbp), %rdi
	leal	(%rax,%r8), %esi
	movq	%rsi, %rax
	movzwl	(%rdi,%rsi,2), %esi
	cmpw	$-3, %si
	ja	.L300
	movq	-72(%rbp), %rax
	leaq	2(%rax), %r10
	movq	%r10, -72(%rbp)
	movw	%si, (%rax)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	jne	.L441
	movb	$0, -96(%rbp)
	movl	%r14d, %edx
	xorl	%r8d, %r8d
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L300:
	cmpw	$-2, %si
	je	.L471
.L450:
	movq	-120(%rbp), %rax
	movq	-72(%rbp), %r10
	movl	$12, (%rax)
.L306:
	cmpb	$0, -96(%rbp)
	jne	.L336
	movl	%r14d, %edx
	xorl	%r8d, %r8d
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L353:
	xorl	%esi, %esi
	movl	%ebx, %r11d
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L464:
	movq	-160(%rbp), %rsi
	movzwl	%ax, %eax
	addl	%r8d, %eax
	movq	%rax, %r8
	movzwl	(%rsi,%rax,2), %eax
	cmpw	$-10241, %ax
	jbe	.L445
	leal	1(%r8), %r9d
	cmpw	$-8193, %ax
	ja	.L310
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	andb	$-37, %ah
	leaq	2(%rsi), %r10
	movq	%r10, -72(%rbp)
	movw	%ax, (%rsi)
	testq	%rdi, %rdi
	je	.L311
	movq	-160(%rbp), %r11
	leaq	4(%rdi), %r8
	movl	%r9d, %eax
	movq	%r8, -64(%rbp)
	movzwl	(%r11,%rax,2), %eax
	movl	%edx, (%rdi)
	cmpq	%r15, %r10
	jb	.L316
.L454:
	movq	%r8, -128(%rbp)
.L343:
	movq	-104(%rbp), %rdx
	movb	$0, -96(%rbp)
	xorl	%r8d, %r8d
	movw	%ax, 144(%rdx)
	movq	-120(%rbp), %rax
	movb	$1, 93(%rdx)
	movl	$15, (%rax)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L314:
	cmpb	$8, %sil
	je	.L472
	cmpb	$2, %sil
	je	.L445
	cmpb	$6, %sil
	je	.L302
	movq	-72(%rbp), %r10
	cmpb	$7, %sil
	jne	.L442
	movq	-120(%rbp), %rax
	movl	$12, (%rax)
	jmp	.L306
.L458:
	movzbl	49(%rax), %ebx
	jmp	.L276
.L310:
	movl	%eax, %esi
	andl	$-2, %esi
	cmpw	$-8192, %si
	je	.L473
	cmpw	$-1, %ax
	je	.L450
	.p2align 4,,10
	.p2align 3
.L302:
	cmpb	$0, -96(%rbp)
	je	.L474
	movq	-120(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L475
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdi
	subq	$8, %rsp
	leaq	-72(%rbp), %r9
	movsbl	-96(%rbp), %r10d
	movq	%r12, %r8
	movq	%rcx, 16(%rax)
	movq	48(%rdi), %rsi
	leaq	-80(%rbp), %rcx
	pushq	-120(%rbp)
	movsbl	2(%rax), %eax
	pushq	%rax
	leaq	-64(%rbp), %rax
	pushq	%rdx
	movl	%r10d, %edx
	pushq	%rax
	pushq	%r15
	call	_ZL7_extToUP10UConverterPK20UConverterSharedDataaPPKhS5_PPDsPKDsPPiiaP10UErrorCode
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r10
	addq	$48, %rsp
	movb	%al, -96(%rbp)
	movq	-112(%rbp), %rax
	movq	%rcx, %rdx
	subq	16(%rax), %rdx
	movq	-120(%rbp), %rax
	addl	%r14d, %edx
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L332
	movl	%edx, %r14d
	xorl	%r8d, %r8d
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%rsi, %rbx
	movq	%rsi, %rcx
	movq	-112(%rbp), %rsi
	movl	$-1, %edx
	movq	%rax, %rdi
	movq	%rax, %r15
	call	ucnv_extContinueMatchToU_67@PLT
	movl	(%rbx), %ebx
	testl	%ebx, %ebx
	jg	.L220
	cmpb	$0, 282(%r15)
	jns	.L225
	jmp	.L220
.L472:
	movq	-104(%rbp), %rax
	movq	-72(%rbp), %r10
	movq	48(%rax), %rax
	cmpb	$0, 49(%rax)
	je	.L442
	movq	-120(%rbp), %rax
	movl	%r11d, %ebx
	movl	$12, (%rax)
	jmp	.L306
.L315:
	andw	$1023, %ax
	orw	$-9216, %ax
	cmpq	%r15, %r10
	jnb	.L455
.L342:
	leaq	4(%rsi), %r10
	movl	%r14d, %edx
	xorl	%r8d, %r8d
	movq	%r10, -72(%rbp)
	movw	%ax, 2(%rsi)
	movb	$0, -96(%rbp)
	jmp	.L298
.L468:
	movl	%esi, %r8d
	jmp	.L304
.L457:
	movq	8(%rdx), %r13
	movl	56(%r13), %edx
	movq	48(%r13), %rsi
	movq	%r13, -104(%rbp)
	movl	%edx, -96(%rbp)
	andl	$16, %edx
	testb	$1, 253(%rax)
	jne	.L226
	subq	%r10, %r15
	movq	%r12, %rax
	movq	%rbx, -64(%rbp)
	movq	%rbx, %rdi
	movq	%r15, %r14
	movq	64(%rsi), %rbx
	movq	%rcx, -80(%rbp)
	sarq	%r14
	testl	%edx, %edx
	cmove	56(%rsi), %rbx
	subq	%rcx, %rax
	cmpl	%eax, %r14d
	movq	%r10, -72(%rbp)
	cmovge	%eax, %r14d
	movq	%rcx, -104(%rbp)
	xorl	%r15d, %r15d
	movl	%r14d, %edx
	cmpl	$15, %edx
	jg	.L476
	.p2align 4,,10
	.p2align 3
.L228:
	testl	%edx, %edx
	jle	.L477
.L237:
	cmpq	%rcx, %r12
	jbe	.L239
	leaq	1(%rcx), %r14
	movq	%r14, -80(%rbp)
	movzbl	-1(%r14), %eax
	movl	(%rbx,%rax,4), %esi
	cmpl	$-2146435072, %esi
	jl	.L438
	movl	%esi, %eax
	shrl	$20, %eax
	andl	$15, %eax
	cmpb	$2, %al
	je	.L438
	cmpb	$6, %al
	je	.L478
	cmpb	$7, %al
	jne	.L246
	movq	-120(%rbp), %rax
	movl	$12, (%rax)
	testq	%rdi, %rdi
	je	.L247
	movl	$12, %esi
.L244:
	movq	%r14, %rax
	subq	-104(%rbp), %rax
	leal	-1(%rax), %edx
	movl	%edx, %r10d
	testl	%edx, %edx
	jle	.L245
	subl	$2, %eax
	cmpl	$2, %eax
	jbe	.L348
	movl	%edx, %r9d
	movd	%r15d, %xmm3
	movdqa	.LC6(%rip), %xmm2
	movq	%rdi, %r8
	shrl	$2, %r9d
	pshufd	$0, %xmm3, %xmm0
	paddd	.LC18(%rip), %xmm0
	salq	$4, %r9
	addq	%rdi, %r9
	.p2align 4,,10
	.p2align 3
.L249:
	movdqa	%xmm0, %xmm1
	addq	$16, %r8
	paddd	%xmm2, %xmm0
	movups	%xmm1, -16(%r8)
	cmpq	%r8, %r9
	jne	.L249
	movl	%edx, %r9d
	movl	%edx, %r10d
	andl	$-4, %r9d
	andl	$3, %r10d
	movl	%r9d, %r11d
	leal	(%r15,%r9), %r8d
	leaq	(%rdi,%r11,4), %r11
	cmpl	%r9d, %edx
	je	.L250
.L248:
	leal	-1(%r10), %r9d
	movl	%r8d, (%r11)
	leal	1(%r8), %edx
	testl	%r9d, %r9d
	jle	.L250
	movl	%edx, 4(%r11)
	addl	$2, %r8d
	cmpl	$2, %r10d
	je	.L250
	movl	%r8d, 8(%r11)
.L250:
	leaq	4(%rdi,%rax,4), %rdi
	leal	1(%rax,%r15), %r15d
	movq	%rdi, -64(%rbp)
.L245:
	testl	%esi, %esi
	jg	.L349
	movzbl	(%rcx), %eax
	movq	-112(%rbp), %rdx
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	movq	48(%r13), %rsi
	leaq	-72(%rbp), %r9
	movq	%r12, %r8
	movq	%r13, %rdi
	movb	%al, 65(%r13)
	pushq	-120(%rbp)
	movsbl	2(%rdx), %eax
	pushq	%rax
	leaq	-64(%rbp), %rax
	pushq	%r15
	pushq	%rax
	pushq	40(%rdx)
	movl	$1, %edx
	call	_ZL7_extToUP10UConverterPK20UConverterSharedDataaPPKhS5_PPDsPKDsPPiiaP10UErrorCode
	addq	$48, %rsp
	movb	%al, 64(%r13)
	movq	-80(%rbp), %rcx
	movq	%rcx, %rax
	subq	%r14, %rax
	leal	1(%r15,%rax), %r15d
	movq	-120(%rbp), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L479
	movq	-112(%rbp), %rax
	movq	-72(%rbp), %r10
	movq	%r14, -104(%rbp)
	movq	-64(%rbp), %rdi
	movq	40(%rax), %rax
	movq	%rax, %rdx
	movq	%rax, -96(%rbp)
	movq	%r12, %rax
	subq	%r10, %rdx
	subq	%rcx, %rax
	sarq	%rdx
	cmpl	%eax, %edx
	cmovge	%eax, %edx
	cmpl	$15, %edx
	jle	.L228
.L476:
	movl	%edx, %eax
	sarl	$4, %eax
	movl	%eax, -128(%rbp)
	movl	%eax, %esi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L229:
	subl	$1, %esi
	je	.L480
.L231:
	leaq	1(%rcx), %rax
	movq	%rcx, %r8
	movq	%r10, %r9
	movq	%rax, -80(%rbp)
	movzbl	(%rcx), %eax
	movl	(%rbx,%rax,4), %r11d
	leaq	2(%r10), %rax
	movq	%rax, -72(%rbp)
	leaq	2(%rcx), %rax
	movw	%r11w, (%r10)
	movl	%r11d, %r14d
	leaq	4(%r10), %r11
	movq	%rax, -80(%rbp)
	movzbl	1(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movq	%r11, -72(%rbp)
	leaq	6(%r10), %r11
	movw	%ax, 2(%r10)
	orl	%eax, %r14d
	leaq	3(%rcx), %rax
	movq	%rax, -80(%rbp)
	movzbl	2(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movq	%r11, -72(%rbp)
	leaq	8(%r10), %r11
	movw	%ax, 4(%r10)
	orl	%eax, %r14d
	leaq	4(%rcx), %rax
	movq	%rax, -80(%rbp)
	movzbl	3(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movq	%r11, -72(%rbp)
	leaq	10(%r10), %r11
	movw	%ax, 6(%r10)
	orl	%eax, %r14d
	leaq	5(%rcx), %rax
	movq	%rax, -80(%rbp)
	movzbl	4(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movq	%r11, -72(%rbp)
	leaq	12(%r10), %r11
	movw	%ax, 8(%r10)
	orl	%eax, %r14d
	leaq	6(%rcx), %rax
	movq	%rax, -80(%rbp)
	movzbl	5(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movq	%r11, -72(%rbp)
	leaq	14(%r10), %r11
	addq	$32, %r10
	movw	%ax, -22(%r10)
	orl	%eax, %r14d
	leaq	7(%rcx), %rax
	movq	%rax, -80(%rbp)
	movzbl	6(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movq	%r11, -72(%rbp)
	orl	%eax, %r14d
	movw	%ax, -20(%r10)
	leaq	8(%rcx), %rax
	addq	$16, %rcx
	movq	%rax, -80(%rbp)
	movzbl	-9(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movw	%ax, -18(%r10)
	orl	%eax, %r14d
	movzbl	-8(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movw	%ax, -16(%r10)
	orl	%eax, %r14d
	movzbl	-7(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movw	%ax, -14(%r10)
	orl	%eax, %r14d
	movzbl	-6(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movw	%ax, -12(%r10)
	orl	%eax, %r14d
	movzbl	-5(%rcx), %eax
	movl	(%rbx,%rax,4), %eax
	movw	%ax, -10(%r10)
	orl	%eax, %r14d
	movzbl	-4(%rcx), %eax
	movl	(%rbx,%rax,4), %r11d
	movl	%r14d, %eax
	movw	%r11w, -8(%r10)
	orl	%r11d, %eax
	movzbl	-3(%rcx), %r11d
	movl	(%rbx,%r11,4), %r11d
	movw	%r11w, -6(%r10)
	orl	%r11d, %eax
	movzbl	-2(%rcx), %r11d
	movl	(%rbx,%r11,4), %r11d
	movw	%r11w, -4(%r10)
	orl	%r11d, %eax
	movq	%rcx, -80(%rbp)
	movzbl	-1(%rcx), %r11d
	movl	(%rbx,%r11,4), %r11d
	movq	%r10, -72(%rbp)
	orl	%r11d, %eax
	movw	%r11w, -2(%r10)
	cmpl	$-2146435072, %eax
	jl	.L229
	subl	%esi, -128(%rbp)
	movl	-128(%rbp), %eax
	movq	%r8, -80(%rbp)
	sall	$4, %eax
	movq	%r9, -72(%rbp)
	subl	%eax, %edx
	testq	%rdi, %rdi
	jne	.L230
	movq	%r9, %r10
	movq	%r8, %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L438:
	movq	-72(%rbp), %r10
	subl	$1, %edx
	movq	%r10, %rax
	addq	$2, %r10
	movq	%r10, -72(%rbp)
	movw	%si, (%rax)
	testl	%edx, %edx
	je	.L238
.L246:
	movq	%r14, %rcx
	jmp	.L237
.L226:
	movq	%rbx, -64(%rbp)
	testl	%edx, %edx
	movq	64(%rsi), %r13
	movq	%rcx, -80(%rbp)
	cmove	56(%rsi), %r13
	movq	%r10, -72(%rbp)
	cmpq	%rcx, %r12
	jbe	.L351
	leaq	-72(%rbp), %r14
	xorl	%ebx, %ebx
	movq	%r14, %r9
	movq	%r12, %r14
	movq	-104(%rbp), %r12
.L261:
	cmpq	%r10, %r15
	jbe	.L481
	leaq	1(%rcx), %rdx
	movq	%rdx, -80(%rbp)
	movzbl	(%rcx), %eax
	movl	0(%r13,%rax,4), %eax
	cmpl	$-2146435072, %eax
	jl	.L453
	movl	%eax, %esi
	shrl	$20, %esi
	movl	%esi, %edi
	andl	$13, %edi
	cmpb	$1, %dil
	jne	.L266
	andl	$1048575, %eax
	leaq	2(%r10), %rsi
	movl	%eax, %ecx
	movq	%rsi, -72(%rbp)
	sarl	$10, %ecx
	orw	$-10240, %cx
	movw	%cx, (%r10)
	movq	-64(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L267
	leaq	4(%rcx), %rdi
	andw	$1023, %ax
	movq	%rdi, -64(%rbp)
	orw	$-9216, %ax
	movl	%ebx, (%rcx)
	cmpq	%r15, %rsi
	jb	.L268
	movq	%rdi, -128(%rbp)
.L339:
	movq	-104(%rbp), %rbx
	movw	%ax, 144(%rbx)
	movq	-120(%rbp), %rax
	movb	$1, 93(%rbx)
	movl	$15, (%rax)
.L260:
	movq	-112(%rbp), %rax
	movq	%rdx, 16(%rax)
	movq	-128(%rbp), %rdx
	movq	%rsi, 32(%rax)
	movq	%rdx, 48(%rax)
	jmp	.L220
.L453:
	leaq	2(%r10), %rcx
	movq	%rcx, -72(%rbp)
	movw	%ax, (%r10)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L270
	leaq	4(%rax), %rsi
	movq	%rsi, -64(%rbp)
	movl	%ebx, (%rax)
.L270:
	movq	%rcx, %r10
	addl	$1, %ebx
	movq	%rdx, %rcx
.L265:
	cmpq	%rcx, %r14
	ja	.L261
	movq	-64(%rbp), %rax
	movq	%r10, %rsi
	movq	%rcx, %rdx
	movq	%rax, -128(%rbp)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L266:
	andl	$15, %esi
	cmpb	$2, %sil
	je	.L453
	cmpb	$6, %sil
	je	.L271
	cmpb	$7, %sil
	jne	.L272
	movq	-120(%rbp), %rax
	movq	%r10, %rsi
	movl	$12, (%rax)
	movq	-64(%rbp), %rax
	movq	%rax, -128(%rbp)
	jmp	.L260
.L338:
	leaq	4(%r10), %rcx
	movq	%rcx, -72(%rbp)
	movw	%ax, 2(%r10)
	movq	%rcx, %r10
.L272:
	addl	$1, %ebx
	movq	%rdx, %rcx
	jmp	.L265
.L271:
	movq	-120(%rbp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L482
	movq	-112(%rbp), %rdi
	subq	$8, %rsp
	movq	48(%r12), %rsi
	movq	%r14, %r8
	movq	%r9, -96(%rbp)
	movq	%rdx, 16(%rdi)
	movzbl	(%rcx), %eax
	movl	$1, %edx
	leaq	-80(%rbp), %rcx
	movb	%al, 65(%r12)
	pushq	-120(%rbp)
	movsbl	2(%rdi), %eax
	movq	%r12, %rdi
	pushq	%rax
	leaq	-64(%rbp), %rax
	pushq	%rbx
	pushq	%rax
	pushq	%r15
	call	_ZL7_extToUP10UConverterPK20UConverterSharedDataaPPKhS5_PPDsPKDsPPiiaP10UErrorCode
	movq	-112(%rbp), %rdi
	addq	$48, %rsp
	movq	-96(%rbp), %r9
	movb	%al, 64(%r12)
	movq	-80(%rbp), %rcx
	movq	%rcx, %rax
	subq	16(%rdi), %rax
	leal	1(%rbx,%rax), %ebx
	movq	-120(%rbp), %rax
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L483
	movq	-72(%rbp), %r10
	jmp	.L265
.L230:
	movl	-128(%rbp), %r11d
	cltq
	movq	%r9, %r10
	addq	%rax, -104(%rbp)
	movq	%r8, %rcx
	testl	%r11d, %r11d
	jle	.L228
.L334:
	movl	-128(%rbp), %eax
	movl	$1, %r9d
	testl	%eax, %eax
	cmovg	%eax, %r9d
	cmpl	$3, %eax
	jle	.L347
	movd	%r15d, %xmm3
	movl	%r9d, %esi
	movq	%rdi, %rax
	pshufd	$0, %xmm3, %xmm0
	shrl	$2, %esi
	paddd	.LC2(%rip), %xmm0
	movdqa	.LC6(%rip), %xmm3
	salq	$8, %rsi
	movaps	%xmm0, -144(%rbp)
	addq	%rdi, %rsi
	movaps	%xmm3, -176(%rbp)
.L234:
	movdqa	-144(%rbp), %xmm4
	addq	$256, %rax
	movdqa	.LC17(%rip), %xmm1
	movdqa	.LC0(%rip), %xmm2
	movdqa	.LC4(%rip), %xmm3
	movdqa	%xmm4, %xmm0
	paddd	.LC3(%rip), %xmm4
	movdqa	.LC10(%rip), %xmm15
	paddd	%xmm0, %xmm1
	movdqa	-176(%rbp), %xmm7
	paddd	%xmm0, %xmm2
	movdqa	.LC7(%rip), %xmm6
	movdqa	.LC8(%rip), %xmm5
	movaps	%xmm4, -144(%rbp)
	movdqa	.LC9(%rip), %xmm8
	paddd	%xmm0, %xmm15
	movdqa	.LC5(%rip), %xmm4
	movaps	%xmm1, -96(%rbp)
	movdqa	.LC11(%rip), %xmm14
	movdqa	%xmm0, %xmm1
	movdqa	.LC12(%rip), %xmm13
	paddd	%xmm0, %xmm8
	movdqa	.LC13(%rip), %xmm12
	paddd	%xmm0, %xmm3
	paddd	%xmm0, %xmm14
	paddd	%xmm0, %xmm4
	punpckldq	%xmm15, %xmm1
	movdqa	.LC14(%rip), %xmm11
	movdqa	.LC15(%rip), %xmm10
	paddd	%xmm0, %xmm13
	paddd	%xmm0, %xmm12
	movdqa	.LC16(%rip), %xmm9
	paddd	%xmm0, %xmm11
	paddd	%xmm0, %xmm7
	paddd	%xmm0, %xmm10
	paddd	%xmm0, %xmm9
	paddd	%xmm0, %xmm6
	paddd	%xmm0, %xmm5
	punpckhdq	%xmm15, %xmm0
	movdqa	%xmm2, %xmm15
	punpckhdq	%xmm14, %xmm2
	punpckldq	%xmm14, %xmm15
	movdqa	%xmm3, %xmm14
	punpckhdq	%xmm13, %xmm3
	punpckldq	%xmm13, %xmm14
	movdqa	%xmm4, %xmm13
	punpckhdq	%xmm12, %xmm4
	punpckldq	%xmm12, %xmm13
	movdqa	%xmm7, %xmm12
	movaps	%xmm4, -160(%rbp)
	punpckhdq	%xmm11, %xmm7
	punpckldq	%xmm11, %xmm12
	movdqa	%xmm8, %xmm4
	movdqa	%xmm6, %xmm11
	punpckldq	-96(%rbp), %xmm4
	punpckldq	%xmm10, %xmm11
	punpckhdq	%xmm10, %xmm6
	movdqa	%xmm5, %xmm10
	punpckhdq	%xmm9, %xmm5
	punpckldq	%xmm9, %xmm10
	movdqa	%xmm4, %xmm9
	movdqa	%xmm8, %xmm4
	movdqa	%xmm1, %xmm8
	punpckhdq	%xmm12, %xmm1
	punpckhdq	-96(%rbp), %xmm4
	punpckldq	%xmm12, %xmm8
	movdqa	%xmm0, %xmm12
	punpckhdq	%xmm7, %xmm0
	punpckldq	%xmm7, %xmm12
	movdqa	%xmm15, %xmm7
	punpckhdq	%xmm11, %xmm15
	punpckldq	%xmm11, %xmm7
	movdqa	%xmm2, %xmm11
	punpckhdq	%xmm6, %xmm2
	punpckldq	%xmm6, %xmm11
	movdqa	%xmm14, %xmm6
	movaps	%xmm2, -96(%rbp)
	punpckhdq	%xmm10, %xmm14
	movdqa	-160(%rbp), %xmm2
	punpckldq	%xmm10, %xmm6
	movdqa	%xmm3, %xmm10
	punpckhdq	%xmm5, %xmm3
	punpckldq	%xmm5, %xmm10
	movdqa	%xmm13, %xmm5
	punpckhdq	%xmm9, %xmm13
	punpckldq	%xmm9, %xmm5
	movdqa	%xmm2, %xmm9
	punpckhdq	%xmm4, %xmm2
	punpckldq	%xmm4, %xmm9
	movdqa	%xmm8, %xmm4
	punpckhdq	%xmm6, %xmm8
	punpckldq	%xmm6, %xmm4
	movdqa	%xmm1, %xmm6
	punpckhdq	%xmm14, %xmm1
	punpckldq	%xmm14, %xmm6
	movdqa	%xmm12, %xmm14
	punpckhdq	%xmm10, %xmm12
	punpckldq	%xmm10, %xmm14
	movdqa	%xmm0, %xmm10
	punpckhdq	%xmm3, %xmm0
	punpckldq	%xmm3, %xmm10
	movdqa	%xmm0, %xmm3
	movdqa	%xmm7, %xmm0
	punpckldq	%xmm5, %xmm0
	punpckhdq	%xmm5, %xmm7
	movdqa	%xmm15, %xmm5
	punpckldq	%xmm13, %xmm5
	punpckhdq	%xmm13, %xmm15
	movdqa	%xmm2, %xmm13
	movaps	%xmm5, -160(%rbp)
	movdqa	%xmm11, %xmm5
	punpckhdq	%xmm9, %xmm11
	punpckldq	%xmm9, %xmm5
	movdqa	-96(%rbp), %xmm9
	punpckldq	%xmm2, %xmm9
	movdqa	-96(%rbp), %xmm2
	punpckhdq	%xmm13, %xmm2
	movdqa	%xmm2, %xmm13
	movdqa	%xmm4, %xmm2
	punpckhdq	%xmm0, %xmm4
	punpckldq	%xmm0, %xmm2
	movdqa	%xmm8, %xmm0
	punpckhdq	%xmm7, %xmm8
	movups	%xmm4, -240(%rax)
	punpckldq	%xmm7, %xmm0
	movdqa	-160(%rbp), %xmm7
	movups	%xmm2, -256(%rax)
	movups	%xmm0, -224(%rax)
	movdqa	%xmm6, %xmm0
	punpckldq	%xmm7, %xmm0
	movups	%xmm8, -208(%rax)
	punpckhdq	%xmm7, %xmm6
	movups	%xmm0, -192(%rax)
	movdqa	%xmm1, %xmm0
	punpckhdq	%xmm15, %xmm1
	punpckldq	%xmm15, %xmm0
	movups	%xmm6, -176(%rax)
	movups	%xmm0, -160(%rax)
	movdqa	%xmm14, %xmm0
	punpckhdq	%xmm5, %xmm14
	punpckldq	%xmm5, %xmm0
	movups	%xmm1, -144(%rax)
	movups	%xmm0, -128(%rax)
	movdqa	%xmm12, %xmm0
	punpckhdq	%xmm11, %xmm12
	punpckldq	%xmm11, %xmm0
	movups	%xmm14, -112(%rax)
	movups	%xmm0, -96(%rax)
	movdqa	%xmm10, %xmm0
	punpckhdq	%xmm9, %xmm10
	punpckldq	%xmm9, %xmm0
	movups	%xmm12, -80(%rax)
	movups	%xmm0, -64(%rax)
	movdqa	%xmm3, %xmm0
	punpckhdq	%xmm13, %xmm3
	punpckldq	%xmm13, %xmm0
	movups	%xmm10, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm3, -16(%rax)
	cmpq	%rax, %rsi
	jne	.L234
	movl	%r9d, %r8d
	movl	-128(%rbp), %r11d
	andl	$-4, %r8d
	movl	%r8d, %esi
	movl	%r8d, %eax
	subl	%r8d, %r11d
	sall	$4, %esi
	salq	$6, %rax
	addl	%r15d, %esi
	addq	%rdi, %rax
	cmpl	%r8d, %r9d
	je	.L235
.L233:
	leal	1(%rsi), %r8d
	leal	15(%rsi), %r9d
	movl	%esi, (%rax)
	movl	%r8d, 4(%rax)
	leal	2(%rsi), %r8d
	movl	%r8d, 8(%rax)
	leal	3(%rsi), %r8d
	movl	%r8d, 12(%rax)
	leal	4(%rsi), %r8d
	movl	%r8d, 16(%rax)
	leal	5(%rsi), %r8d
	movl	%r8d, 20(%rax)
	leal	6(%rsi), %r8d
	movl	%r8d, 24(%rax)
	leal	7(%rsi), %r8d
	movl	%r8d, 28(%rax)
	leal	8(%rsi), %r8d
	movl	%r8d, 32(%rax)
	leal	9(%rsi), %r8d
	movl	%r8d, 36(%rax)
	leal	10(%rsi), %r8d
	movl	%r8d, 40(%rax)
	leal	11(%rsi), %r8d
	movl	%r8d, 44(%rax)
	leal	12(%rsi), %r8d
	movl	%r8d, 48(%rax)
	leal	13(%rsi), %r8d
	movl	%r8d, 52(%rax)
	leal	14(%rsi), %r8d
	movl	%r8d, 56(%rax)
	leal	16(%rsi), %r8d
	movl	%r9d, 60(%rax)
	cmpl	$1, %r11d
	je	.L235
	movl	%r8d, 64(%rax)
	leal	17(%rsi), %r8d
	leal	31(%rsi), %r9d
	movl	%r8d, 68(%rax)
	leal	18(%rsi), %r8d
	movl	%r8d, 72(%rax)
	leal	19(%rsi), %r8d
	movl	%r8d, 76(%rax)
	leal	20(%rsi), %r8d
	movl	%r8d, 80(%rax)
	leal	21(%rsi), %r8d
	movl	%r8d, 84(%rax)
	leal	22(%rsi), %r8d
	movl	%r8d, 88(%rax)
	leal	23(%rsi), %r8d
	movl	%r8d, 92(%rax)
	leal	24(%rsi), %r8d
	movl	%r8d, 96(%rax)
	leal	25(%rsi), %r8d
	movl	%r8d, 100(%rax)
	leal	26(%rsi), %r8d
	movl	%r8d, 104(%rax)
	leal	27(%rsi), %r8d
	movl	%r8d, 108(%rax)
	leal	28(%rsi), %r8d
	movl	%r8d, 112(%rax)
	leal	29(%rsi), %r8d
	movl	%r8d, 116(%rax)
	leal	30(%rsi), %r8d
	movl	%r8d, 120(%rax)
	leal	32(%rsi), %r8d
	movl	%r9d, 124(%rax)
	cmpl	$2, %r11d
	je	.L235
	movl	%r8d, 128(%rax)
	leal	33(%rsi), %r8d
	movl	%r8d, 132(%rax)
	leal	34(%rsi), %r8d
	movl	%r8d, 136(%rax)
	leal	35(%rsi), %r8d
	movl	%r8d, 140(%rax)
	leal	36(%rsi), %r8d
	movl	%r8d, 144(%rax)
	leal	37(%rsi), %r8d
	movl	%r8d, 148(%rax)
	leal	38(%rsi), %r8d
	movl	%r8d, 152(%rax)
	leal	39(%rsi), %r8d
	movl	%r8d, 156(%rax)
	leal	40(%rsi), %r8d
	movl	%r8d, 160(%rax)
	leal	41(%rsi), %r8d
	movl	%r8d, 164(%rax)
	leal	42(%rsi), %r8d
	movl	%r8d, 168(%rax)
	leal	43(%rsi), %r8d
	movl	%r8d, 172(%rax)
	leal	44(%rsi), %r8d
	movl	%r8d, 176(%rax)
	leal	45(%rsi), %r8d
	movl	%r8d, 180(%rax)
	leal	46(%rsi), %r8d
	addl	$47, %esi
	movl	%r8d, 184(%rax)
	movl	%esi, 188(%rax)
.L235:
	movl	-128(%rbp), %r14d
	leal	-1(%r14), %eax
	movl	%eax, %esi
	cltq
	sall	$4, %esi
	addq	$1, %rax
	salq	$6, %rax
	leal	16(%r15,%rsi), %r15d
	testl	%r14d, %r14d
	movl	$64, %esi
	cmovle	%rsi, %rax
	addq	%rax, %rdi
	movq	%rdi, -64(%rbp)
	jmp	.L228
.L268:
	leaq	4(%r10), %rsi
	movq	%rsi, -72(%rbp)
	movw	%ax, 2(%r10)
	leaq	8(%rcx), %rax
	movq	%rsi, %r10
	movq	%rax, -64(%rbp)
	movl	%ebx, 4(%rcx)
	jmp	.L272
.L238:
	movq	%rdi, -128(%rbp)
	movq	%r14, %rcx
.L236:
	movq	-120(%rbp), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jg	.L254
	cmpq	%rcx, %r12
	jbe	.L254
	movq	-112(%rbp), %rdx
	cmpq	%r10, 40(%rdx)
	ja	.L254
	movl	$15, (%rax)
.L254:
	movq	-128(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L451
	movq	%rcx, %rsi
	subq	-104(%rbp), %rsi
	je	.L451
	leaq	-1(%rsi), %rax
	cmpq	$2, %rax
	jbe	.L350
	movq	%rsi, %rdx
	movd	%r15d, %xmm3
	movdqa	.LC6(%rip), %xmm2
	movq	%rbx, %rax
	shrq	$2, %rdx
	pshufd	$0, %xmm3, %xmm0
	paddd	.LC18(%rip), %xmm0
	salq	$4, %rdx
	addq	%rbx, %rdx
.L257:
	movdqa	%xmm0, %xmm1
	addq	$16, %rax
	paddd	%xmm2, %xmm0
	movups	%xmm1, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L257
	movq	-128(%rbp), %rax
	movq	%rsi, %rdi
	movq	%rsi, %rdx
	andq	$-4, %rdi
	andl	$3, %edx
	addl	%edi, %r15d
	leaq	(%rax,%rdi,4), %rax
	cmpq	%rsi, %rdi
	je	.L258
.L256:
	movl	%r15d, (%rax)
	leal	1(%r15), %edi
	cmpq	$1, %rdx
	je	.L258
	movl	%edi, 4(%rax)
	addl	$2, %r15d
	cmpq	$2, %rdx
	je	.L258
	movl	%r15d, 8(%rax)
.L258:
	movq	-128(%rbp), %rax
	leaq	(%rax,%rsi,4), %rax
	movq	%rax, -128(%rbp)
	jmp	.L451
.L480:
	movl	-128(%rbp), %eax
	sall	$4, %eax
	subl	%eax, %edx
	testq	%rdi, %rdi
	je	.L228
	cltq
	addq	%rax, -104(%rbp)
	jmp	.L334
.L475:
	movq	-72(%rbp), %r10
.L336:
	movq	-64(%rbp), %rax
	movzbl	-96(%rbp), %r15d
	xorl	%r8d, %r8d
	movq	%rax, -128(%rbp)
	cmpb	$1, %r15b
	jle	.L277
	movq	-104(%rbp), %rdx
	movzbl	%bl, %r14d
	movl	$1, %r12d
	movq	48(%rdx), %rax
	leaq	66(%rdx), %r9
	cmpb	$0, 49(%rax)
	setne	%r11b
	salq	$10, %r14
	jmp	.L326
.L321:
	shrl	$20, %esi
	andl	$15, %esi
	cmpb	$8, %sil
	jne	.L366
	testb	%r11b, %r11b
	je	.L366
.L323:
	addl	$1, %r12d
	addq	$1, %r9
	cmpb	%r12b, %r15b
	je	.L448
.L326:
	movzbl	(%r9), %eax
	leaq	0(%r13,%rax,4), %rax
	movl	(%rax,%r14), %esi
	testl	%esi, %esi
	js	.L321
	shrl	$24, %esi
	movq	%r13, %rdi
	call	_ZL18hasValidTrailBytesPA256_Kih
.L322:
	testb	%al, %al
	je	.L323
	cmpb	%r12b, -96(%rbp)
	jle	.L448
	movzbl	-96(%rbp), %edx
	movq	-112(%rbp), %rsi
	movq	%rcx, %rax
	subl	%r12d, %edx
	subq	16(%rsi), %rax
	movsbl	%dl, %esi
	cmpl	%eax, %esi
	jg	.L327
	movsbq	%dl, %rdx
	movb	%r12b, -96(%rbp)
	xorl	%r8d, %r8d
	subq	%rdx, %rcx
	jmp	.L277
.L366:
	cmpb	$7, %sil
	setne	%al
	jmp	.L322
.L332:
	movq	-64(%rbp), %rax
	movq	%rax, -128(%rbp)
.L448:
	xorl	%r8d, %r8d
	jmp	.L277
.L473:
	movq	-160(%rbp), %rsi
	movq	-72(%rbp), %rax
	movzwl	(%rsi,%r9,2), %esi
	leaq	2(%rax), %r10
	movq	%r10, -72(%rbp)
	movw	%si, (%rax)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	jne	.L441
	jmp	.L442
.L311:
	movq	-160(%rbp), %rdx
	movl	%r9d, %eax
	movzwl	(%rdx,%rax,2), %eax
	cmpq	%r15, %r10
	jb	.L342
.L455:
	movq	$0, -128(%rbp)
	jmp	.L343
.L239:
	movq	%rdi, -128(%rbp)
.L439:
	movq	-72(%rbp), %r10
	jmp	.L254
.L267:
	andw	$1023, %ax
	orw	$-9216, %ax
	cmpq	%r15, %rsi
	jb	.L338
	movq	$0, -128(%rbp)
	jmp	.L339
.L478:
	movq	-120(%rbp), %rax
	movl	(%rax), %esi
	testq	%rdi, %rdi
	je	.L245
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%rdi, %r11
	movl	%r15d, %r8d
	jmp	.L248
.L477:
	movq	%rdi, -128(%rbp)
	jmp	.L236
.L481:
	movq	-120(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	%r10, %rsi
	movl	$15, (%rax)
	movq	-64(%rbp), %rax
	movq	%rax, -128(%rbp)
	jmp	.L260
.L247:
	movq	%rdi, -128(%rbp)
	movq	-72(%rbp), %r10
	movq	%r14, %rcx
	jmp	.L451
.L479:
	movq	-64(%rbp), %rax
	movq	%r14, -104(%rbp)
	movq	%rax, -128(%rbp)
	jmp	.L439
.L350:
	movq	%rbx, %rax
	movq	%rsi, %rdx
	jmp	.L256
.L327:
	movq	-104(%rbp), %rdx
	subb	-96(%rbp), %al
	addl	%r12d, %eax
	movb	%al, 282(%rdx)
	movsbl	%al, %eax
	addq	$250, %rdx
	negl	%eax
	cltq
	cmpq	$8, %rax
	jnb	.L328
	testb	$4, %al
	jne	.L484
	testq	%rax, %rax
	je	.L329
	movzbl	(%r9), %ecx
	movq	-104(%rbp), %rsi
	movb	%cl, 250(%rsi)
	testb	$2, %al
	jne	.L485
.L329:
	movq	-112(%rbp), %rax
	movb	%r12b, -96(%rbp)
	xorl	%r8d, %r8d
	movq	16(%rax), %rcx
	jmp	.L277
.L328:
	movq	-104(%rbp), %rsi
	movq	(%r9), %rcx
	leaq	258(%rsi), %rdi
	movq	%rcx, 250(%rsi)
	movq	-8(%r9,%rax), %rcx
	movq	%r9, %rsi
	andq	$-8, %rdi
	movq	%rcx, -8(%rdx,%rax)
	subq	%rdi, %rdx
	addq	%rdx, %rax
	subq	%rdx, %rsi
	shrq	$3, %rax
	movq	%rax, %rcx
	rep movsq
	jmp	.L329
.L347:
	movl	%eax, %r11d
	movl	%r15d, %esi
	movq	%rdi, %rax
	jmp	.L233
.L351:
	movq	%r10, %rsi
	movq	%rcx, %rdx
	jmp	.L260
.L460:
	movq	-64(%rbp), %rax
	movb	%sil, -96(%rbp)
	movl	%r11d, %ebx
	movq	%rax, -128(%rbp)
	jmp	.L277
.L465:
	call	__stack_chk_fail@PLT
.L484:
	movl	(%r9), %ecx
	movq	-104(%rbp), %rsi
	movl	%ecx, 250(%rsi)
	movl	-4(%r9,%rax), %ecx
	movl	%ecx, -4(%rdx,%rax)
	jmp	.L329
.L485:
	movzwl	-2(%r9,%rax), %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L329
.L461:
	movb	%sil, -96(%rbp)
	movl	%r11d, %ebx
	jmp	.L344
.L474:
	movq	-72(%rbp), %r10
	movl	%r14d, %edx
	xorl	%r8d, %r8d
	jmp	.L298
.L349:
	movq	%rdi, -128(%rbp)
	movq	%r14, %rcx
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L483:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	%rcx, %rdx
	movq	%rax, -128(%rbp)
	jmp	.L260
.L482:
	movq	-64(%rbp), %rax
	movq	%r10, %rsi
	movq	%rax, -128(%rbp)
	jmp	.L260
	.cfi_endproc
.LFE2827:
	.size	ucnv_MBCSToUnicodeWithOffsets_67, .-ucnv_MBCSToUnicodeWithOffsets_67
	.p2align 4
	.globl	ucnv_MBCSFromUnicodeWithOffsets_67
	.type	ucnv_MBCSFromUnicodeWithOffsets_67, @function
ucnv_MBCSFromUnicodeWithOffsets_67:
.LFB2834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	8(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	208(%r14), %eax
	testl	%eax, %eax
	jns	.L487
.L491:
	movq	48(%r14), %r9
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rcx
	movq	48(%rbx), %rdi
	movzbl	253(%r9), %eax
	movzbl	252(%r9), %r13d
	subq	%rsi, %rcx
	movq	16(%rbx), %rdx
	movb	%al, -92(%rbp)
	movq	24(%rbx), %rax
	movl	%ecx, %r8d
	movq	%rax, -88(%rbp)
	testb	%r13b, %r13b
	jne	.L887
	testb	$2, -92(%rbp)
	je	.L888
	movzbl	254(%r9), %eax
.L535:
	movq	%rdi, -64(%rbp)
	movq	88(%r9), %rdi
	xorl	%r15d, %r15d
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -136(%rbp)
	testb	%al, %al
	je	.L564
	movq	96(%r9), %r15
.L564:
	movl	56(%r14), %eax
	movq	240(%r9), %rsi
	testb	$16, %al
	cmove	232(%r9), %rsi
	movq	%rsi, -128(%rbp)
.L565:
	movl	260(%r9), %edi
	movl	84(%r14), %r10d
	xorl	%esi, %esi
	movl	%edi, -112(%rbp)
	cmpb	$12, %r13b
	jne	.L566
	movl	80(%r14), %esi
	movl	$1, %edi
	testl	%esi, %esi
	cmove	%edi, %esi
.L566:
	testl	%r10d, %r10d
	setne	%r9b
	setne	%dil
	movzbl	%r9b, %r9d
	negl	%r9d
	movl	%r9d, -104(%rbp)
	testb	$16, %ah
	jne	.L688
	testb	$32, %ah
	je	.L889
	movb	$1, -147(%rbp)
	movl	$671088640, -160(%rbp)
	movl	$2621440, -164(%rbp)
	movb	$0, -146(%rbp)
	movb	$41, -145(%rbp)
	movl	$1, -152(%rbp)
	movl	$1, -156(%rbp)
	testl	%ecx, %ecx
	jg	.L890
	.p2align 4,,10
	.p2align 3
.L730:
	movl	$-1, -96(%rbp)
	xorl	%eax, %eax
.L570:
	cmpq	%rdx, -88(%rbp)
	jbe	.L576
.L627:
	addl	%r8d, %eax
	movl	$1, %r9d
	addl	$1, %eax
	testl	%r8d, %r8d
	jle	.L891
	movq	%r15, -144(%rbp)
.L571:
	leaq	2(%rdx), %rdi
	movl	%eax, %ecx
	movzwl	(%rdx), %edx
	subl	%r8d, %ecx
	movq	%rdi, -80(%rbp)
	movl	%ecx, -120(%rbp)
	movl	%edx, %r11d
	movl	%edx, %r10d
	cmpl	$127, %edx
	jg	.L574
	movl	%edx, %ecx
	movl	%r9d, %r15d
	sarl	$2, %ecx
	sall	%cl, %r15d
	testl	%r15d, -112(%rbp)
	jne	.L892
.L574:
	movq	-144(%rbp), %r15
	cmpl	$55295, %edx
	jg	.L578
	testq	%r15, %r15
	je	.L578
	sarl	$6, %edx
	movslq	%edx, %rdx
	movzwl	(%r15,%rdx,2), %ecx
	cmpb	$12, %r13b
	ja	.L579
	testb	%r13b, %r13b
	je	.L580
	cmpb	$12, %r13b
	ja	.L580
	leaq	.L582(%rip), %rdx
	movzbl	%r13b, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L582:
	.long	.L580-.L582
	.long	.L587-.L582
	.long	.L586-.L582
	.long	.L585-.L582
	.long	.L580-.L582
	.long	.L580-.L582
	.long	.L580-.L582
	.long	.L580-.L582
	.long	.L584-.L582
	.long	.L583-.L582
	.long	.L580-.L582
	.long	.L580-.L582
	.long	.L581-.L582
	.text
	.p2align 4,,10
	.p2align 3
.L887:
	movzbl	254(%r9), %eax
	cmpb	$1, %r13b
	jne	.L535
	testb	%al, %al
	jne	.L893
	movq	88(%r9), %rax
	movq	%rsi, -72(%rbp)
	xorl	%r15d, %r15d
	movq	232(%r9), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rax, -136(%rbp)
	movl	56(%r14), %eax
	movq	%rsi, -128(%rbp)
	movq	240(%r9), %rsi
	movq	%rdi, -64(%rbp)
	testb	$16, %al
	je	.L565
	movl	260(%r9), %edi
	movl	84(%r14), %r10d
	movq	%rsi, -128(%rbp)
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	%edi, -112(%rbp)
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%rsi, %rcx
	movl	$-1, %edx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	ucnv_extContinueMatchFromU_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L486
	cmpb	$0, 281(%r14)
	jns	.L491
	.p2align 4,,10
	.p2align 3
.L486:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L894
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	movb	$2, -147(%rbp)
	movl	$172097536, -160(%rbp)
	movl	$655360, -164(%rbp)
	movb	$65, -146(%rbp)
	movb	$10, -145(%rbp)
	movl	$2, -152(%rbp)
	movl	$2, -156(%rbp)
.L567:
	testl	%ecx, %ecx
	jle	.L730
.L890:
	testb	%dil, %dil
	je	.L730
	movl	$0, -120(%rbp)
	movl	$-1, -96(%rbp)
.L568:
	cmpq	-88(%rbp), %rdx
	jnb	.L576
	movzwl	(%rdx), %ecx
	movl	%ecx, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L895
	movl	$12, (%r12)
	movq	-64(%rbp), %rcx
	movl	%esi, %eax
.L604:
	movq	-72(%rbp), %rdi
.L647:
	movl	%r10d, 84(%r14)
	movl	%eax, 80(%r14)
	movq	%rdx, 16(%rbx)
	movq	%rdi, 32(%rbx)
	movq	%rcx, 48(%rbx)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L889:
	andl	$16384, %eax
	cmpl	$1, %eax
	sbbl	%r11d, %r11d
	movl	%r11d, %r9d
	andl	$-208666624, %r11d
	addl	$2, %r9d
	addl	$443547648, %r11d
	cmpl	$1, %eax
	movl	%r11d, -160(%rbp)
	sbbl	%r11d, %r11d
	andl	$-786432, %r11d
	movb	%r9b, -147(%rbp)
	addl	$1703936, %r11d
	cmpl	$1, %eax
	movl	%r11d, -164(%rbp)
	sbbl	%r11d, %r11d
	notl	%r11d
	andl	$113, %r11d
	cmpl	$1, %eax
	movb	%r11b, -146(%rbp)
	sbbl	%r11d, %r11d
	andl	$-11, %r11d
	addl	$26, %r11d
	cmpl	$1, %eax
	sbbl	%r9d, %r9d
	movb	%r11b, -145(%rbp)
	addl	$2, %r9d
	movl	%r9d, -152(%rbp)
	movl	%r9d, -156(%rbp)
	jmp	.L567
.L580:
	cmpq	$0, -64(%rbp)
	je	.L811
.L868:
	movq	-80(%rbp), %rdx
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L578:
	andl	$-2048, %edx
	cmpl	$55296, %edx
	je	.L896
.L601:
	movl	%r10d, %eax
	movq	-136(%rbp), %rcx
	movl	%r10d, %edx
	sarl	$10, %eax
	sarl	$4, %edx
	cltq
	andl	$63, %edx
	movzwl	(%rcx,%rax,2), %eax
	addl	%edx, %eax
	cltq
	movl	(%rcx,%rax,4), %r9d
	cmpb	$12, %r13b
	ja	.L605
	testb	%r13b, %r13b
	je	.L711
	cmpb	$12, %r13b
	ja	.L711
	leaq	.L608(%rip), %rdx
	movzbl	%r13b, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L608:
	.long	.L711-.L608
	.long	.L613-.L608
	.long	.L612-.L608
	.long	.L611-.L608
	.long	.L711-.L608
	.long	.L711-.L608
	.long	.L711-.L608
	.long	.L711-.L608
	.long	.L610-.L608
	.long	.L609-.L608
	.long	.L711-.L608
	.long	.L711-.L608
	.long	.L607-.L608
	.text
	.p2align 4,,10
	.p2align 3
.L605:
	cmpb	$-37, %r13b
	jne	.L711
	movzwl	%r9w, %edx
	movl	%r10d, %eax
	movq	-128(%rbp), %rcx
	sall	$4, %edx
	andl	$15, %eax
	addl	%eax, %edx
	movzwl	(%rcx,%rdx,2), %edx
	cmpl	$255, %edx
	ja	.L897
.L711:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L606:
	cmpb	$0, 63(%r14)
	jne	.L623
	leal	-57344(%r10), %eax
	cmpl	$6399, %eax
	jbe	.L623
	leal	-983040(%r10), %eax
	cmpl	$131071, %eax
	jbe	.L623
.L590:
	movsbl	2(%rbx), %eax
	subq	$8, %rsp
	movq	48(%r14), %rsi
	movslq	%r8d, %r8
	pushq	%r12
	movl	%r10d, %edx
	leaq	-80(%rbp), %rcx
	leaq	-72(%rbp), %r9
	pushq	%rax
	movl	-104(%rbp), %eax
	movq	%rdi, 16(%rbx)
	movq	%r14, %rdi
	pushq	%rax
	leaq	-64(%rbp), %rax
	pushq	%rax
	movq	-72(%rbp), %rax
	addq	%r8, %rax
	movq	-88(%rbp), %r8
	pushq	%rax
	call	_ZL9_extFromUP10UConverterPK20UConverterSharedDataiPPKDsS5_PPhPKhPPiiaP10UErrorCode
	movl	80(%r14), %esi
	movl	(%r12), %edi
	addq	$48, %rsp
	movl	%eax, %r10d
	movq	-80(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movl	%esi, %eax
	testl	%edi, %edi
	jg	.L604
	movq	-72(%rbp), %rdi
	movl	40(%rbx), %r8d
	movq	%rdx, %rax
	subq	16(%rbx), %rax
	sarq	%rax
	subl	%edi, %r8d
	addl	-120(%rbp), %eax
	testq	%rcx, %rcx
	je	.L626
	movl	-104(%rbp), %ecx
	movl	%eax, -104(%rbp)
	movl	%ecx, -96(%rbp)
.L626:
	cmpq	-88(%rbp), %rdx
	jb	.L627
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L623:
	testl	%edx, %edx
	je	.L590
	.p2align 4,,10
	.p2align 3
.L589:
	cmpl	%ecx, %r8d
	jge	.L659
	subl	%r8d, %ecx
	leaq	104(%r14), %rax
	movl	%ecx, %r10d
	cmpl	$2, %ecx
	je	.L641
	cmpl	$3, %ecx
	jne	.L642
	movl	%edx, %edi
	leaq	105(%r14), %rax
	shrl	$16, %edi
	movb	%dil, 104(%r14)
.L641:
	movb	%dh, (%rax)
	addq	$1, %rax
.L642:
	leal	0(,%r10,8), %ecx
	movb	%dl, (%rax)
	movb	%r10b, 91(%r14)
	shrl	%cl, %edx
	cmpl	$2, %r8d
	je	.L643
	cmpl	$3, %r8d
	jne	.L644
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movl	%edx, %ecx
	shrl	$16, %ecx
	movb	%cl, (%rax)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L643
	movl	-104(%rbp), %edi
	leaq	4(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movl	%edi, (%rax)
.L643:
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movb	%dh, (%rax)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L644
	movl	-104(%rbp), %edi
	leaq	4(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movl	%edi, (%rax)
.L644:
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, -72(%rbp)
	xorl	%ecx, %ecx
	movb	%dl, (%rax)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L645
	movl	-104(%rbp), %edi
	leaq	4(%rax), %rcx
	movl	%edi, (%rax)
.L645:
	movl	$15, (%r12)
	movq	-80(%rbp), %rdx
	movl	%esi, %eax
	xorl	%r10d, %r10d
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L579:
	cmpb	$-37, %r13b
	jne	.L580
	movq	-128(%rbp), %rax
	andl	$63, %r11d
	addl	%r11d, %ecx
	movzwl	(%rax,%rcx,2), %edx
	cmpl	$255, %edx
	ja	.L696
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L892:
	movq	-72(%rbp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, -72(%rbp)
	xorl	%ecx, %ecx
	movb	%r11b, (%rdx)
	movq	-64(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L575
	movl	-104(%rbp), %edi
	leaq	4(%rdx), %rcx
	movq	%rcx, -64(%rbp)
	movl	%edi, -96(%rbp)
	movl	%edi, (%rdx)
	movl	-120(%rbp), %edi
	movl	%edi, -104(%rbp)
.L575:
	movq	-80(%rbp), %rdx
	subl	$1, %r8d
	cmpq	-88(%rbp), %rdx
	jnb	.L898
	testl	%r8d, %r8d
	jne	.L571
	xorl	%r10d, %r10d
.L573:
	movl	$15, (%r12)
	movl	%esi, %eax
	jmp	.L604
.L898:
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L576:
	movl	(%r12), %eax
	movq	-72(%rbp), %rdi
	testl	%eax, %eax
	jg	.L731
.L646:
	cmpb	$12, %r13b
	jne	.L731
	cmpl	$2, %esi
	jne	.L731
	cmpb	$0, 2(%rbx)
	je	.L732
	cmpq	-88(%rbp), %rdx
	jb	.L732
	testl	%r10d, %r10d
	jne	.L732
	testl	%r8d, %r8d
	jle	.L653
	leaq	1(%rdi), %rax
	cmpl	$2, -152(%rbp)
	movq	%rax, -72(%rbp)
	movzbl	-145(%rbp), %eax
	movb	%al, (%rdi)
	je	.L899
.L654:
	movq	-64(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rdi
	testq	%rax, %rax
	je	.L726
	movl	-96(%rbp), %esi
	leaq	4(%rax), %rcx
	xorl	%r10d, %r10d
	movl	%esi, (%rax)
	movl	$1, %eax
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L732:
	movq	-64(%rbp), %rcx
	movl	$2, %eax
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L888:
	movq	8(%rbx), %r13
	movq	48(%r13), %rax
	movl	56(%r13), %r9d
	movq	240(%rax), %r14
	movq	232(%rax), %r11
	andl	$16, %r9d
	testl	%r9d, %r9d
	movq	88(%rax), %r10
	movq	%rdx, -80(%rbp)
	cmove	%r11, %r14
	testb	$1, -92(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	jne	.L493
	movl	260(%rax), %eax
	cmpb	$1, 63(%r13)
	sbbl	%r15d, %r15d
	movl	84(%r13), %r11d
	movl	%eax, -104(%rbp)
	movq	-88(%rbp), %rax
	andw	$1024, %r15w
	addw	$2048, %r15w
	subq	%rdx, %rax
	sarq	%rax
	cmpl	%eax, %ecx
	cmovge	%eax, %ecx
	testl	%r11d, %r11d
	jne	.L900
	movl	$0, -92(%rbp)
.L496:
	testl	%ecx, %ecx
	jle	.L901
	leal	-1(%rcx), %eax
	movl	$1, %r8d
	movq	%rdx, %rcx
.L500:
	movzwl	(%rcx), %r11d
	leaq	2(%rcx), %rsi
	movq	%rsi, -112(%rbp)
	movq	%rsi, -80(%rbp)
	movl	%r11d, %edi
	movl	%r11d, %esi
	cmpl	$127, %r11d
	jg	.L501
	movl	%r11d, %ecx
	movl	%r8d, %r9d
	sarl	$2, %ecx
	sall	%cl, %r9d
	testl	%r9d, -104(%rbp)
	jne	.L902
.L501:
	movl	%esi, %ecx
	movl	%esi, %r9d
	sarl	$10, %ecx
	sarl	$4, %r9d
	movslq	%ecx, %rcx
	andl	$63, %r9d
	movzwl	(%r10,%rcx,2), %ecx
	addl	%r9d, %ecx
	movl	%edi, %r9d
	movslq	%ecx, %rcx
	andl	$15, %r9d
	movzwl	(%r10,%rcx,2), %ecx
	addl	%r9d, %ecx
	movslq	%ecx, %rcx
	movzwl	(%r14,%rcx,2), %ecx
	cmpw	%r15w, %cx
	jnb	.L903
	andl	$-2048, %esi
	movl	$1, -120(%rbp)
	cmpl	$55296, %esi
	je	.L904
.L504:
	movq	-64(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L510
	movq	-112(%rbp), %rax
	subq	%rdx, %rax
	sarq	%rax
	subl	-120(%rbp), %eax
	testl	%eax, %eax
	jle	.L510
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movslq	%edx, %r9
	cmpl	$2, %r9d
	jle	.L669
	movd	-92(%rbp), %xmm4
	movl	%eax, %esi
	movdqa	.LC6(%rip), %xmm2
	movq	%rcx, %rdx
	shrl	$2, %esi
	pshufd	$0, %xmm4, %xmm0
	salq	$4, %rsi
	paddd	.LC18(%rip), %xmm0
	addq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L512:
	movdqa	%xmm0, %xmm1
	addq	$16, %rdx
	paddd	%xmm2, %xmm0
	movups	%xmm1, -16(%rdx)
	cmpq	%rdx, %rsi
	jne	.L512
	movl	-92(%rbp), %esi
	movl	%eax, %edx
	andl	$-4, %edx
	leal	(%rsi,%rdx), %edi
	movl	%edx, %esi
	subl	%edx, %eax
	leaq	(%rcx,%rsi,4), %rsi
	cmpl	%r8d, %edx
	je	.L513
	leal	-1(%rax), %edx
.L511:
	movl	%edi, (%rsi)
	leal	1(%rdi), %r8d
	testl	%edx, %edx
	je	.L513
	movl	%r8d, 4(%rsi)
	addl	$2, %edi
	cmpl	$2, %eax
	je	.L513
	movl	%edi, 8(%rsi)
.L513:
	movl	-92(%rbp), %eax
	leal	1(%rax,%r9), %eax
	movl	%eax, -92(%rbp)
	leaq	4(%rcx,%r9,4), %rax
	movq	%rax, -64(%rbp)
.L510:
	movsbl	2(%rbx), %eax
	subq	$8, %rsp
	movq	48(%r13), %rsi
	movl	%r11d, %edx
	pushq	%r12
	movq	-88(%rbp), %r8
	leaq	-80(%rbp), %rcx
	leaq	-72(%rbp), %r9
	pushq	%rax
	movl	-92(%rbp), %eax
	movq	%r13, %rdi
	movq	%r10, -128(%rbp)
	pushq	%rax
	leaq	-64(%rbp), %rax
	pushq	%rax
	pushq	40(%rbx)
	call	_ZL9_extFromUP10UConverterPK20UConverterSharedDataiPPKDsS5_PPhPKhPPiiaP10UErrorCode
	movq	-80(%rbp), %rdx
	addq	$48, %rsp
	movl	%eax, %r11d
	movq	%rdx, %rax
	subq	-112(%rbp), %rax
	movq	%rdx, %r8
	sarq	%rax
	addl	-120(%rbp), %eax
	addl	%eax, -92(%rbp)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L509
	movq	-88(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	40(%rbx), %rax
	movq	-128(%rbp), %r10
	subq	%rdx, %rcx
	subq	%rsi, %rax
	sarq	%rcx
	cmpl	%eax, %ecx
	cmovge	%eax, %ecx
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L731:
	movq	-64(%rbp), %rcx
	movl	%esi, %eax
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L493:
	cmpb	$1, 63(%r13)
	movzbl	253(%rax), %eax
	sbbl	%r15d, %r15d
	movl	84(%r13), %r11d
	andw	$1024, %r15w
	movb	%al, -92(%rbp)
	addw	$2048, %r15w
	testl	%r11d, %r11d
	jne	.L905
	xorl	%eax, %eax
.L522:
	cmpq	-88(%rbp), %rdx
	jnb	.L875
	testl	%r8d, %r8d
	jle	.L527
	movzwl	(%rdx), %r11d
	leaq	2(%rdx), %r9
	leal	1(%rax), %edi
	movq	%r9, -80(%rbp)
	movl	%r11d, %ecx
	movl	%edi, -104(%rbp)
	movl	%r11d, %edx
	andl	$63488, %ecx
	cmpl	$55296, %ecx
	je	.L906
.L528:
	movl	%r11d, %edx
	movl	%r11d, %ecx
	sarl	$10, %edx
	sarl	$4, %ecx
	movslq	%edx, %rdx
	andl	$63, %ecx
	movzwl	(%r10,%rdx,2), %edx
	addl	%ecx, %edx
	movl	%r11d, %ecx
	movslq	%edx, %rdx
	andl	$15, %ecx
	movzwl	(%r10,%rdx,2), %edx
	addl	%ecx, %edx
	movslq	%edx, %rdx
	movzwl	(%r14,%rdx,2), %edx
	cmpw	%r15w, %dx
	jnb	.L907
.L532:
	movsbl	2(%rbx), %edx
	subq	$8, %rsp
	movslq	%r8d, %r8
	movq	48(%r13), %rdi
	addq	%r8, %rsi
	pushq	%r12
	movq	-88(%rbp), %r8
	leaq	-80(%rbp), %rcx
	pushq	%rdx
	movl	%r11d, %edx
	pushq	%rax
	leaq	-64(%rbp), %rax
	pushq	%rax
	movq	%r9, 16(%rbx)
	leaq	-72(%rbp), %r9
	pushq	%rsi
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%r10, -112(%rbp)
	call	_ZL9_extFromUP10UConverterPK20UConverterSharedDataiPPKDsS5_PPhPKhPPiiaP10UErrorCode
	movl	(%r12), %r10d
	addq	$48, %rsp
	movq	-80(%rbp), %rdx
	movl	%eax, %r11d
	testl	%r10d, %r10d
	movq	-112(%rbp), %r10
	jle	.L534
	movq	-72(%rbp), %rsi
	.p2align 4,,10
	.p2align 3
.L875:
	movq	-64(%rbp), %rdi
.L526:
	movl	%r11d, 84(%r13)
	movq	%rdx, 16(%rbx)
	movq	%rsi, 32(%rbx)
	movq	%rdi, 48(%rbx)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L896:
	testb	$2, -92(%rbp)
	jne	.L601
	movq	%rdi, %rdx
	testw	$1024, %r11w
	je	.L568
	movl	$12, (%r12)
	movq	-64(%rbp), %rcx
	movl	%esi, %eax
	jmp	.L604
.L581:
	movq	-128(%rbp), %rax
	andl	$63, %r11d
	movl	%esi, 80(%r14)
	addl	%r11d, %ecx
	movzwl	(%rax,%rcx,2), %edx
	cmpl	$255, %edx
	ja	.L592
	testl	%edx, %edx
	je	.L590
	cmpl	$1, %esi
	jle	.L591
	cmpl	$1, -152(%rbp)
	movzbl	-145(%rbp), %eax
	je	.L908
	movzbl	-146(%rbp), %ecx
	sall	$16, %eax
	movl	$1, %esi
	sall	$8, %ecx
	orl	%ecx, %eax
	movl	$3, %ecx
	orl	%eax, %edx
	jmp	.L589
.L583:
	andl	$63, %r11d
	addl	%ecx, %r11d
	leal	(%r11,%r11,2), %eax
	addq	-128(%rbp), %rax
	movzbl	(%rax), %ecx
	movzbl	1(%rax), %edx
	movzbl	2(%rax), %eax
	sall	$16, %ecx
	sall	$8, %edx
	orl	%ecx, %edx
	orl	%eax, %edx
	cmpl	$255, %edx
	jbe	.L883
	movl	$2, %ecx
	cmpl	$65535, %edx
	jbe	.L589
	testl	$8388608, %edx
	jne	.L600
	orl	$-1904214016, %edx
	movl	$4, %ecx
	jmp	.L589
.L584:
	movq	-128(%rbp), %rax
	andl	$63, %r11d
	addl	%r11d, %ecx
	movzwl	(%rax,%rcx,2), %edx
	movl	%edx, %eax
	cmpw	$255, %dx
	jbe	.L883
	testw	%dx, %dx
	js	.L598
	orl	$9338880, %edx
	movl	$3, %ecx
	jmp	.L589
.L585:
	movq	-128(%rbp), %rax
	andl	$63, %r11d
	addl	%r11d, %ecx
	movl	(%rax,%rcx,4), %edx
	cmpl	$255, %edx
	jbe	.L883
	movl	$2, %ecx
	cmpl	$65535, %edx
	jbe	.L589
	xorl	%ecx, %ecx
	cmpl	$16777215, %edx
	seta	%cl
	addl	$3, %ecx
	jmp	.L589
.L586:
	andl	$63, %r11d
	addl	%ecx, %r11d
	leal	(%r11,%r11,2), %eax
	addq	-128(%rbp), %rax
	movzbl	(%rax), %ecx
	movzbl	1(%rax), %edx
	movzbl	2(%rax), %eax
	sall	$16, %ecx
	sall	$8, %edx
	orl	%ecx, %edx
	orl	%eax, %edx
	cmpl	$255, %edx
	jbe	.L883
	xorl	%ecx, %ecx
	cmpl	$65535, %edx
	seta	%cl
	addl	$2, %ecx
	jmp	.L589
.L587:
	movq	-128(%rbp), %rax
	andl	$63, %r11d
	addl	%r11d, %ecx
	movzwl	(%rax,%rcx,2), %edx
	cmpl	$255, %edx
	jbe	.L883
.L696:
	movl	$2, %ecx
	jmp	.L589
.L607:
	movzwl	%r9w, %eax
	movl	%r10d, %ecx
	movq	-128(%rbp), %rdx
	movl	%esi, 80(%r14)
	andl	$15, %ecx
	sall	$4, %eax
	addl	%ecx, %eax
	addl	$16, %ecx
	movzwl	(%rdx,%rax,2), %edx
	shrl	%cl, %r9d
	andl	$1, %r9d
	cmpl	$255, %edx
	ja	.L616
	testl	%edx, %edx
	jne	.L617
	testl	%r9d, %r9d
	jne	.L909
	xorl	%ecx, %ecx
	jmp	.L606
.L609:
	movzwl	%r9w, %eax
	movl	%r10d, %ecx
	andl	$15, %ecx
	sall	$4, %eax
	addl	%ecx, %eax
	addl	$16, %ecx
	leal	(%rax,%rax,2), %eax
	addq	-128(%rbp), %rax
	shrl	%cl, %r9d
	movl	$1, %ecx
	movzbl	(%rax), %r11d
	movzbl	1(%rax), %edx
	andl	$1, %r9d
	movzbl	2(%rax), %eax
	sall	$16, %r11d
	sall	$8, %edx
	orl	%r11d, %edx
	orl	%eax, %edx
	cmpl	$255, %edx
	jbe	.L615
	movl	$2, %ecx
	cmpl	$65535, %edx
	jbe	.L615
	testl	$8388608, %edx
	jne	.L622
	orl	$-1904214016, %edx
	movl	$4, %ecx
	.p2align 4,,10
	.p2align 3
.L615:
	testl	%r9d, %r9d
	jne	.L589
	jmp	.L606
.L610:
	movzwl	%r9w, %eax
	movl	%r10d, %ecx
	movq	-128(%rbp), %rdx
	andl	$15, %ecx
	sall	$4, %eax
	addl	%ecx, %eax
	addl	$16, %ecx
	movzwl	(%rdx,%rax,2), %edx
	shrl	%cl, %r9d
	movl	$1, %ecx
	andl	$1, %r9d
	movl	%edx, %eax
	cmpw	$255, %dx
	jbe	.L615
	testw	%dx, %dx
	js	.L621
	orl	$9338880, %edx
	movl	$3, %ecx
	jmp	.L615
.L611:
	movzwl	%r9w, %eax
	movl	%r10d, %ecx
	movq	-128(%rbp), %rdx
	andl	$15, %ecx
	sall	$4, %eax
	addl	%ecx, %eax
	addl	$16, %ecx
	movl	(%rdx,%rax,4), %edx
	shrl	%cl, %r9d
	movl	$1, %ecx
	andl	$1, %r9d
	cmpl	$255, %edx
	jbe	.L615
	movl	$2, %ecx
	cmpl	$65535, %edx
	jbe	.L615
	xorl	%ecx, %ecx
	cmpl	$16777215, %edx
	seta	%cl
	addl	$3, %ecx
	jmp	.L615
.L613:
	movzwl	%r9w, %eax
	movl	%r10d, %ecx
	movq	-128(%rbp), %rdx
	andl	$15, %ecx
	sall	$4, %eax
	addl	%ecx, %eax
	addl	$16, %ecx
	movzwl	(%rdx,%rax,2), %edx
	shrl	%cl, %r9d
	xorl	%ecx, %ecx
	andl	$1, %r9d
	cmpl	$255, %edx
	seta	%cl
	addl	$1, %ecx
	jmp	.L615
.L612:
	movzwl	%r9w, %eax
	movl	%r10d, %ecx
	andl	$15, %ecx
	sall	$4, %eax
	addl	%ecx, %eax
	addl	$16, %ecx
	leal	(%rax,%rax,2), %eax
	addq	-128(%rbp), %rax
	shrl	%cl, %r9d
	movl	$1, %ecx
	movzbl	(%rax), %r11d
	movzbl	1(%rax), %edx
	andl	$1, %r9d
	movzbl	2(%rax), %eax
	sall	$16, %r11d
	sall	$8, %edx
	orl	%r11d, %edx
	orl	%eax, %edx
	cmpl	$255, %edx
	jbe	.L615
	xorl	%ecx, %ecx
	cmpl	$65535, %edx
	seta	%cl
	addl	$2, %ecx
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L893:
	movq	8(%rbx), %r13
	movq	48(%r13), %rax
	testb	$16, 56(%r13)
	movzbl	253(%rax), %r10d
	movq	%rdi, -64(%rbp)
	movq	88(%rax), %rdi
	movq	%rsi, -72(%rbp)
	movb	%r10b, -128(%rbp)
	movl	84(%r13), %r10d
	movq	%rdi, -120(%rbp)
	movq	96(%rax), %rdi
	movq	240(%rax), %r14
	movq	%rdx, -80(%rbp)
	movq	%rdi, -112(%rbp)
	cmove	232(%rax), %r14
	movl	260(%rax), %r15d
	testl	%r10d, %r10d
	je	.L678
	movl	$0, -92(%rbp)
	movl	$-1, %edi
	testl	%ecx, %ecx
	jle	.L538
.L539:
	cmpq	-88(%rbp), %rdx
	jnb	.L877
	movzwl	(%rdx), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L910
	movl	$12, (%r12)
	movq	-64(%rbp), %rcx
.L546:
	movl	%r10d, 84(%r13)
	movq	%rdx, 16(%rbx)
	movq	%rsi, 32(%rbx)
	movq	%rcx, 48(%rbx)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L905:
	movl	$0, -104(%rbp)
	movl	$-1, %eax
	testl	%ecx, %ecx
	jle	.L911
.L523:
	cmpq	-88(%rbp), %rdx
	jnb	.L875
	movzwl	(%rdx), %ecx
	movl	%ecx, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	je	.L912
	movl	$12, (%r12)
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L900:
	movl	$-1, -92(%rbp)
	movq	%rdx, %r8
	testl	%ecx, %ecx
	jle	.L873
.L497:
	cmpq	-88(%rbp), %rdx
	jnb	.L506
	movzwl	(%rdx), %ecx
	movl	%ecx, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L913
	movl	$12, (%r12)
.L509:
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
.L514:
	testq	%rdi, %rdi
	je	.L526
	movq	%rdx, %rcx
	subq	%r8, %rcx
	sarq	%rcx
	je	.L526
	cmpl	$11, (%r12)
	je	.L914
.L516:
	leaq	-1(%rcx), %rax
	cmpq	$2, %rax
	jbe	.L674
	movd	-92(%rbp), %xmm3
	movq	%rcx, %r8
	movdqa	.LC6(%rip), %xmm2
	movq	%rdi, %rax
	shrq	$2, %r8
	pshufd	$0, %xmm3, %xmm0
	salq	$4, %r8
	paddd	.LC18(%rip), %xmm0
	addq	%rdi, %r8
	.p2align 4,,10
	.p2align 3
.L518:
	movdqa	%xmm0, %xmm1
	addq	$16, %rax
	paddd	%xmm2, %xmm0
	movups	%xmm1, -16(%rax)
	cmpq	%rax, %r8
	jne	.L518
	movq	%rcx, %rax
	movq	%rcx, %r9
	andq	$-4, %rax
	andl	$3, %r9d
	addl	%eax, -92(%rbp)
	leaq	(%rdi,%rax,4), %r8
	cmpq	%rax, %rcx
	je	.L519
.L517:
	movl	-92(%rbp), %r10d
	movl	%r10d, (%r8)
	leal	1(%r10), %eax
	cmpq	$1, %r9
	je	.L519
	movl	%eax, 4(%r8)
	addl	$2, %r10d
	cmpq	$2, %r9
	je	.L519
	movl	%r10d, 8(%r8)
.L519:
	leaq	(%rdi,%rcx,4), %rdi
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L897:
	leal	16(%rax), %ecx
	shrl	%cl, %r9d
	movl	$2, %ecx
	andl	$1, %r9d
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L883:
	testl	%edx, %edx
	je	.L590
.L591:
	subl	$1, %r8d
	cmpq	$0, -64(%rbp)
	je	.L727
.L638:
	movq	-72(%rbp), %rax
	movl	-104(%rbp), %edi
	leaq	1(%rax), %rcx
	movl	%edi, -96(%rbp)
	movq	%rcx, -72(%rbp)
	movb	%dl, (%rax)
	movq	-64(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movq	-80(%rbp), %rdx
	movl	%edi, (%rax)
	movl	-120(%rbp), %eax
	movl	%eax, -104(%rbp)
.L885:
	xorl	%r10d, %r10d
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L895:
	leaq	2(%rdx), %rdi
	sall	$10, %r10d
	addl	$1, -120(%rbp)
	movq	%rdi, -80(%rbp)
	leal	-56613888(%rcx,%r10), %r10d
	testb	$1, -92(%rbp)
	jne	.L601
	movl	%esi, 80(%r14)
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L678:
	xorl	%edi, %edi
.L538:
	xorl	%eax, %eax
.L540:
	cmpq	-88(%rbp), %rdx
	jnb	.L877
	addl	%r8d, %eax
	movl	$1, %r11d
	addl	$1, %eax
	testl	%r8d, %r8d
	jle	.L542
	movq	%r14, -104(%rbp)
.L541:
	movzwl	(%rdx), %r10d
	movl	%eax, %ecx
	leaq	2(%rdx), %r9
	subl	%r8d, %ecx
	movq	%r9, -80(%rbp)
	movl	%ecx, -92(%rbp)
	movl	%r10d, %edx
	cmpl	$127, %r10d
	jg	.L543
	movl	%r10d, %ecx
	movl	%r11d, %r14d
	sarl	$2, %ecx
	sall	%cl, %r14d
	testl	%r15d, %r14d
	jne	.L915
	movq	-104(%rbp), %r14
.L544:
	movl	%r10d, %eax
	movq	-112(%rbp), %rcx
	andl	$63, %edx
	sarl	$6, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	addl	%edx, %eax
	cltq
	movzwl	(%r14,%rax,2), %eax
	testl	%eax, %eax
	je	.L549
.L550:
	leaq	1(%rsi), %rcx
	movl	%eax, %edx
	movq	%rcx, -72(%rbp)
	cmpl	$255, %eax
	jbe	.L916
	movb	%ah, (%rsi)
	cmpl	$1, %r8d
	jne	.L917
	movq	-64(%rbp), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L563
	movl	%edi, (%rax)
	leaq	4(%rax), %rcx
.L563:
	movb	%dl, 104(%r13)
	xorl	%r10d, %r10d
	movb	$1, 91(%r13)
	movq	-80(%rbp), %rdx
	movl	$15, (%r12)
	movq	-72(%rbp), %rsi
	jmp	.L546
.L909:
	cmpl	$1, %esi
	jg	.L660
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L659:
	subl	%ecx, %r8d
	cmpq	$0, -64(%rbp)
	je	.L918
	cmpl	$3, %ecx
	jne	.L919
.L636:
	movq	-72(%rbp), %rax
	movl	-104(%rbp), %edi
	leaq	1(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movl	%edx, %ecx
	shrl	$16, %ecx
	movb	%cl, (%rax)
	movq	-64(%rbp), %rax
	leaq	4(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movl	%edi, (%rax)
.L639:
	movq	-72(%rbp), %rax
	movl	-104(%rbp), %edi
	leaq	1(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movb	%dh, (%rax)
	movq	-64(%rbp), %rax
	leaq	4(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movl	%edi, (%rax)
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L918:
	cmpl	$3, %ecx
	je	.L630
	cmpl	$4, %ecx
	jne	.L920
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movl	%edx, %ecx
	shrl	$24, %ecx
	movb	%cl, (%rax)
.L630:
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movl	%edx, %ecx
	shrl	$16, %ecx
	movb	%cl, (%rax)
.L633:
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movb	%dh, (%rax)
	movl	-120(%rbp), %eax
.L632:
	movq	-72(%rbp), %rcx
	leaq	1(%rcx), %rdi
	movq	%rdi, -72(%rbp)
	movb	%dl, (%rcx)
	cmpq	$0, -64(%rbp)
	movq	-80(%rbp), %rdx
	je	.L885
.L886:
	movl	-104(%rbp), %eax
	movl	%eax, -96(%rbp)
	movl	-120(%rbp), %eax
	movl	%eax, -104(%rbp)
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L919:
	cmpl	$4, %ecx
	jne	.L921
	movq	-72(%rbp), %rax
	movl	-104(%rbp), %edi
	leaq	1(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movl	%edx, %ecx
	shrl	$24, %ecx
	movb	%cl, (%rax)
	movq	-64(%rbp), %rax
	leaq	4(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movl	%edi, (%rax)
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L921:
	cmpl	$1, %ecx
	je	.L638
	cmpl	$2, %ecx
	je	.L639
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L920:
	cmpl	$1, %ecx
	je	.L727
	cmpl	$2, %ecx
	je	.L633
.L811:
	movq	-80(%rbp), %rdx
	movl	-120(%rbp), %eax
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L903:
	movq	-72(%rbp), %rsi
	movl	%eax, %r11d
	leaq	1(%rsi), %rdi
	movq	%rdi, -72(%rbp)
	movb	%cl, (%rsi)
.L502:
	movq	-80(%rbp), %rcx
	subl	$1, %eax
	testl	%r11d, %r11d
	jne	.L500
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L498:
	movl	(%r12), %r14d
	testl	%r14d, %r14d
	jg	.L673
	cmpq	-88(%rbp), %rcx
	jnb	.L673
	cmpq	%rsi, 40(%rbx)
	ja	.L673
	movq	%rdx, %r8
	movl	$15, (%r12)
	movq	%rcx, %rdx
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L904:
	testw	$1024, %di
	jne	.L505
	movq	%rdx, %r8
	movq	-112(%rbp), %rdx
	jmp	.L497
.L543:
	movq	-104(%rbp), %r14
	cmpl	$55295, %r10d
	jle	.L544
	movl	%r10d, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L922
.L551:
	movl	%r10d, %eax
	movq	-120(%rbp), %rcx
	movl	%r10d, %edx
	sarl	$10, %eax
	sarl	$4, %edx
	cltq
	andl	$63, %edx
	movzwl	(%rcx,%rax,2), %eax
	addl	%edx, %eax
	movl	%r10d, %edx
	cltq
	andl	$15, %edx
	movl	(%rcx,%rax,4), %ecx
	movzwl	%cx, %eax
	sall	$4, %eax
	addl	%edx, %eax
	addl	$16, %edx
	btl	%edx, %ecx
	movzwl	(%r14,%rax,2), %eax
	jc	.L550
	cmpb	$0, 63(%r13)
	jne	.L555
	leal	-57344(%r10), %edx
	cmpl	$6399, %edx
	jbe	.L555
	leal	-983040(%r10), %edx
	cmpl	$131071, %edx
	jbe	.L555
.L549:
	movsbl	2(%rbx), %eax
	movslq	%r8d, %r8
	movq	48(%r13), %r11
	movq	%r9, 16(%rbx)
	addq	%r8, %rsi
	pushq	%r9
	movq	-88(%rbp), %r8
	movl	%r10d, %edx
	pushq	%r12
	leaq	-80(%rbp), %rcx
	leaq	-72(%rbp), %r9
	pushq	%rax
	leaq	-64(%rbp), %rax
	pushq	%rdi
	movq	%r13, %rdi
	pushq	%rax
	pushq	%rsi
	movq	%r11, %rsi
	call	_ZL9_extFromUP10UConverterPK20UConverterSharedDataiPPKDsS5_PPhPKhPPiiaP10UErrorCode
	addq	$48, %rsp
	cmpl	$0, (%r12)
	movq	-80(%rbp), %rdx
	movl	%eax, %r10d
	jle	.L557
	movq	-72(%rbp), %rsi
.L877:
	movq	-64(%rbp), %rcx
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L915:
	leaq	1(%rsi), %rcx
	movq	%rcx, -72(%rbp)
	xorl	%ecx, %ecx
	movb	%r10b, (%rsi)
	movq	-64(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L545
	leaq	4(%rdx), %rcx
	movq	%rcx, -64(%rbp)
	movl	%edi, (%rdx)
	movl	-92(%rbp), %edi
.L545:
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	cmpq	-88(%rbp), %rdx
	jnb	.L923
	subl	$1, %r8d
	jne	.L541
	xorl	%r10d, %r10d
.L542:
	movl	$15, (%r12)
	movq	-64(%rbp), %rcx
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L902:
	movq	-72(%rbp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, -72(%rbp)
	movb	%r11b, (%rcx)
	movl	%eax, %r11d
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L592:
	movl	$2, %ecx
	cmpl	$2, %esi
	je	.L589
	cmpl	$1, -156(%rbp)
	je	.L924
	orl	-160(%rbp), %edx
	movl	$2, %esi
	movl	$4, %ecx
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L506:
	cmpb	$0, 2(%rbx)
	je	.L509
	movl	$11, (%r12)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L616:
	movl	$2, %ecx
	cmpl	$2, %esi
	je	.L615
	cmpl	$1, -156(%rbp)
	je	.L925
	orl	-160(%rbp), %edx
	movl	$2, %esi
	movl	$4, %ecx
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L891:
	movq	-64(%rbp), %rcx
	jmp	.L573
.L901:
	movq	-64(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L873:
	movq	%rdx, %rcx
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L911:
	cmpq	-88(%rbp), %rdx
	jnb	.L526
.L658:
	movl	$15, (%r12)
	jmp	.L526
.L505:
	movq	%rdx, %r8
	movl	$12, (%r12)
	movq	-112(%rbp), %rdx
	jmp	.L509
.L916:
	movb	%al, (%rsi)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L559
	leaq	4(%rax), %rdx
	movq	%rdx, -64(%rbp)
	movl	%edi, (%rax)
.L559:
	subl	$1, %r8d
.L876:
	movl	-92(%rbp), %eax
	movq	-80(%rbp), %rdx
	xorl	%r10d, %r10d
	movq	-72(%rbp), %rsi
	movl	%eax, %edi
	jmp	.L540
.L617:
	movl	$1, %ecx
	cmpl	$1, %esi
	jle	.L615
.L660:
	cmpl	$1, -152(%rbp)
	movzbl	-145(%rbp), %ecx
	je	.L926
	movzbl	-146(%rbp), %eax
	sall	$8, %eax
	movl	%eax, %esi
	movl	%ecx, %eax
	movl	$3, %ecx
	sall	$16, %eax
	orl	%esi, %eax
	movl	$1, %esi
	orl	%eax, %edx
	jmp	.L615
.L907:
	leaq	1(%rsi), %rcx
	movq	%rcx, -72(%rbp)
	movb	%dl, (%rsi)
	movq	-64(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L533
	leaq	4(%rdx), %rcx
	movq	%rcx, -64(%rbp)
	movl	%eax, (%rdx)
.L533:
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rsi
	subl	$1, %r8d
	xorl	%r11d, %r11d
	movl	-104(%rbp), %eax
	jmp	.L522
.L727:
	movl	-120(%rbp), %eax
	jmp	.L632
.L917:
	movq	-72(%rbp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, -72(%rbp)
	movb	%al, (%rdx)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L561
	leaq	8(%rax), %rdx
	movl	%edi, (%rax)
	movq	%rdx, -64(%rbp)
	movl	%edi, 4(%rax)
.L561:
	subl	$2, %r8d
	jmp	.L876
.L555:
	testl	%eax, %eax
	jne	.L550
	jmp	.L549
.L598:
	movl	$2, %ecx
	testb	$-128, %al
	jne	.L589
	orl	$9371776, %edx
	movl	$3, %ecx
	jmp	.L589
.L922:
	testb	$2, -128(%rbp)
	jne	.L551
	andb	$4, %dh
	jne	.L552
	movq	%r9, %rdx
	jmp	.L539
.L621:
	movl	$2, %ecx
	testb	$-128, %al
	jne	.L615
	orl	$9371776, %edx
	movl	$3, %ecx
	jmp	.L615
.L906:
	andb	$4, %dh
	jne	.L529
	movq	%r9, %rdx
	jmp	.L523
.L653:
	movzbl	-145(%rbp), %eax
	cmpl	$2, -152(%rbp)
	movb	%al, 104(%r14)
	jne	.L656
	movzbl	-146(%rbp), %eax
	movb	%al, 105(%r14)
.L656:
	movzbl	-147(%rbp), %eax
	xorl	%r10d, %r10d
	movb	%al, 91(%r14)
	movl	$1, %eax
	movq	-64(%rbp), %rcx
	movl	$15, (%r12)
	jmp	.L647
.L673:
	movq	%rdx, %r8
	movq	%rcx, %rdx
	jmp	.L514
.L527:
	movq	-64(%rbp), %rdi
	jmp	.L658
.L914:
	subq	$1, %rcx
	jne	.L516
	jmp	.L526
.L913:
	sall	$10, %r11d
	leaq	2(%rdx), %rax
	movq	%r8, %rdx
	leal	-56613888(%rcx,%r11), %r11d
	movq	%rax, -112(%rbp)
	cmpl	$65536, %r11d
	movq	%rax, -80(%rbp)
	sbbl	%eax, %eax
	addl	$2, %eax
	movl	%eax, -120(%rbp)
	jmp	.L504
.L912:
	leaq	2(%rdx), %r9
	sall	$10, %r11d
	addl	$1, -104(%rbp)
	movq	%r9, -80(%rbp)
	leal	-56613888(%rcx,%r11), %r11d
	testb	$1, -92(%rbp)
	jne	.L528
	jmp	.L532
.L600:
	movl	$3, %ecx
	testb	$-128, %dh
	jne	.L589
	orl	$-1895792640, %edx
	movl	$4, %ecx
	jmp	.L589
.L622:
	movl	$3, %ecx
	testb	$-128, %dh
	jne	.L615
	orl	$-1895792640, %edx
	movl	$4, %ecx
	jmp	.L615
.L924:
	orl	-164(%rbp), %edx
	movl	$2, %esi
	movl	$3, %ecx
	jmp	.L589
.L910:
	leaq	2(%rdx), %r9
	sall	$10, %r10d
	addl	$1, -92(%rbp)
	movq	%r9, -80(%rbp)
	leal	-56613888(%rax,%r10), %r10d
	testb	$1, -128(%rbp)
	jne	.L551
	jmp	.L549
.L925:
	orl	-164(%rbp), %edx
	movl	$2, %esi
	movl	$3, %ecx
	jmp	.L615
.L529:
	movl	$12, (%r12)
	movq	-64(%rbp), %rdi
	movq	%r9, %rdx
	jmp	.L526
.L899:
	cmpl	$1, %r8d
	je	.L927
	movq	-72(%rbp), %rax
	movzbl	-146(%rbp), %esi
	leaq	1(%rax), %rdx
	movq	%rdx, -72(%rbp)
	movb	%sil, (%rax)
	jmp	.L654
.L726:
	xorl	%ecx, %ecx
	movl	$1, %eax
	xorl	%r10d, %r10d
	jmp	.L647
.L926:
	movl	%ecx, %eax
	movl	$1, %esi
	movl	$2, %ecx
	sall	$8, %eax
	orl	%eax, %edx
	jmp	.L615
.L908:
	sall	$8, %eax
	movl	$1, %esi
	movl	$2, %ecx
	orl	%eax, %edx
	jmp	.L589
.L552:
	movl	$12, (%r12)
	movq	-64(%rbp), %rcx
	movq	%r9, %rdx
	jmp	.L546
.L674:
	movq	%rdi, %r8
	movq	%rcx, %r9
	jmp	.L517
.L927:
	movzbl	-146(%rbp), %eax
	movb	$1, 91(%r14)
	movb	%al, 104(%r14)
	movl	$15, (%r12)
	jmp	.L654
.L534:
	movq	-72(%rbp), %rsi
	movl	40(%rbx), %r8d
	movq	%rdx, %rax
	subq	16(%rbx), %rax
	sarq	%rax
	subl	%esi, %r8d
	addl	-104(%rbp), %eax
	jmp	.L522
.L923:
	xorl	%r10d, %r10d
	jmp	.L546
.L894:
	call	__stack_chk_fail@PLT
.L669:
	movl	-92(%rbp), %edi
	movq	%rcx, %rsi
	jmp	.L511
.L557:
	movq	-72(%rbp), %rsi
	movl	40(%rbx), %r8d
	movq	%rdx, %rdi
	subq	16(%rbx), %rdi
	sarq	%rdi
	addl	-92(%rbp), %edi
	subl	%esi, %r8d
	movl	%edi, %eax
	jmp	.L540
	.cfi_endproc
.LFE2834:
	.size	ucnv_MBCSFromUnicodeWithOffsets_67, .-ucnv_MBCSFromUnicodeWithOffsets_67
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC19:
	.string	"18030"
.LC20:
	.string	"gb18030"
.LC21:
	.string	"GB18030"
.LC22:
	.string	"KEIS"
.LC23:
	.string	"keis"
.LC24:
	.string	"JEF"
.LC25:
	.string	"jef"
.LC26:
	.string	"JIPS"
.LC27:
	.string	"jips"
	.text
	.p2align 4
	.type	_ZL13ucnv_MBCSOpenP10UConverterP18UConverterLoadArgsP10UErrorCode, @function
_ZL13ucnv_MBCSOpenP10UConverterP18UConverterLoadArgsP10UErrorCode:
.LFB2820:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rsi)
	jne	.L975
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	48(%rdi), %r13
	movl	12(%rsi), %eax
	movzbl	252(%r13), %r14d
	cmpb	$-37, %r14b
	je	.L978
	testb	$16, %al
	jne	.L979
.L932:
	movq	24(%r12), %r12
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L944
	leaq	.LC20(%rip), %rsi
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L980
.L945:
	orl	$32768, 56(%rbx)
.L946:
	cmpb	$12, %r14b
	je	.L981
	movq	288(%r13), %rax
	testq	%rax, %rax
	je	.L928
	movzbl	68(%rax), %eax
	movzbl	88(%rbx), %edx
.L954:
	cmpb	%dl, %al
	jle	.L928
	movb	%al, 88(%rbx)
.L928:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L978:
	.cfi_restore_state
	andl	$-17, %eax
	movl	%eax, 12(%rsi)
	movl	%eax, 56(%rdi)
	movl	12(%rsi), %eax
	testb	$16, %al
	je	.L932
.L979:
	xorl	%edi, %edi
	movq	%rdx, -56(%rbp)
	call	umtx_lock_67@PLT
	movq	64(%r13), %r15
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movq	-56(%rbp), %rdx
	testq	%r15, %r15
	jne	.L932
	movq	48(%rbx), %rcx
	movzbl	252(%rcx), %eax
	testb	%al, %al
	je	.L957
	cmpb	$12, %al
	jne	.L934
.L957:
	movq	56(%rcx), %rsi
	cmpl	$-2147483638, 148(%rsi)
	jne	.L934
	cmpl	$-2147483515, 84(%rsi)
	jne	.L934
	movq	88(%rcx), %r8
	movq	232(%rcx), %r9
	movzwl	(%r8), %edi
	testb	%al, %al
	jne	.L936
	movzwl	(%r8,%rdi,2), %eax
	leaq	(%rdi,%rdi), %rsi
	cmpw	$3877, 20(%r9,%rax,2)
	jne	.L934
	movzwl	16(%r8,%rsi), %eax
	cmpw	$3861, 10(%r9,%rax,2)
	jne	.L934
.L937:
	movl	248(%rcx), %r15d
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	testl	%r15d, %r15d
	je	.L938
	movzbl	48(%rcx), %eax
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	sall	$10, %eax
	leal	80(%r15,%rax), %edi
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	testq	%rax, %rax
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	je	.L982
	movzbl	48(%rcx), %r11d
	movq	56(%rcx), %rsi
	movq	%rax, %rdi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, %rdx
	movq	%r9, -80(%rbp)
	salq	$10, %rdx
	movb	%r11b, -56(%rbp)
	andl	$261120, %edx
	call	memcpy@PLT
	movzbl	-56(%rbp), %r11d
	movq	-80(%rbp), %r9
	movq	%r15, %rdx
	movl	$-2147483515, 148(%rax)
	salq	$10, %r11
	movl	$-2147483638, 84(%rax)
	movq	%r9, %rsi
	addq	%rax, %r11
	movq	%rax, -56(%rbp)
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	%rax, %r11
	movq	-56(%rbp), %r10
	cmpb	$0, 252(%rcx)
	movzwl	(%r8), %eax
	jne	.L941
	movzwl	(%r8,%rax,2), %eax
	movw	$3861, 20(%r11,%rax,2)
	movzwl	(%r8), %eax
	movzwl	16(%r8,%rax,2), %eax
	movw	$3877, 10(%r11,%rax,2)
.L942:
	movq	16(%rcx), %rax
	addq	%r11, %r15
	movq	%r10, -72(%rbp)
	movq	%r15, %rdi
	movq	%r11, -64(%rbp)
	leaq	4(%rax), %rsi
	movq	%rcx, -56(%rbp)
	call	stpcpy@PLT
	xorl	%edi, %edi
	movabsq	$7955165021723915052, %rcx
	movq	%rcx, (%rax)
	movw	$108, 8(%rax)
	call	umtx_lock_67@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r10
	cmpq	$0, 64(%rcx)
	je	.L983
	xorl	%edi, %edi
	movq	%r10, -56(%rbp)
	call	umtx_unlock_67@PLT
	movq	-56(%rbp), %r10
	movq	%r10, %rdi
	call	uprv_free_67@PLT
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L975:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L981:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$3, 88(%rbx)
	movq	288(%r13), %rax
	testq	%rax, %rax
	je	.L928
	movl	68(%rax), %eax
	movl	$3, %edx
	addl	$1, %eax
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L944:
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L984
.L947:
	orl	$4096, 56(%rbx)
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L980:
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	jne	.L945
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L984:
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	jne	.L947
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L985
.L949:
	orl	$8192, 56(%rbx)
	jmp	.L946
.L936:
	movl	(%r8,%rdi,4), %eax
	leaq	0(,%rdi,4), %rsi
	testl	$67108864, %eax
	je	.L934
	movzwl	%ax, %eax
	sall	$4, %eax
	addl	$10, %eax
	cmpw	$37, (%r9,%rax,2)
	jne	.L934
	movl	32(%r8,%rsi), %eax
	testl	$2097152, %eax
	je	.L934
	movzwl	%ax, %eax
	sall	$4, %eax
	addl	$5, %eax
	cmpw	$21, (%r9,%rax,2)
	je	.L937
	.p2align 4,,10
	.p2align 3
.L934:
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L928
	movl	12(%r12), %eax
	andl	$-17, %eax
	movl	%eax, 12(%r12)
	movl	%eax, 56(%rbx)
	jmp	.L932
.L985:
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	jne	.L949
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L986
.L951:
	orl	$16384, 56(%rbx)
	jmp	.L946
.L938:
	movl	$3, (%rdx)
	jmp	.L928
.L986:
	leaq	.LC27(%rip), %rsi
	movq	%r12, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	jne	.L951
	jmp	.L946
.L983:
	movq	%r10, 64(%rcx)
	xorl	%edi, %edi
	movq	%r11, 240(%rcx)
	movq	%r15, 272(%rcx)
	call	umtx_unlock_67@PLT
	jmp	.L932
.L941:
	movzwl	(%r8,%rax,4), %eax
	sall	$4, %eax
	addl	$10, %eax
	movw	$21, (%r11,%rax,2)
	movzwl	(%r8), %eax
	movzwl	32(%r8,%rax,4), %eax
	sall	$4, %eax
	addl	$5, %eax
	movw	$37, (%r11,%rax,2)
	jmp	.L942
.L982:
	movl	$7, (%rdx)
	jmp	.L928
	.cfi_endproc
.LFE2820:
	.size	_ZL13ucnv_MBCSOpenP10UConverterP18UConverterLoadArgsP10UErrorCode, .-_ZL13ucnv_MBCSOpenP10UConverterP18UConverterLoadArgsP10UErrorCode
	.section	.rodata
.LC29:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC28:
	.string	" 000000000000\02000"
	.text
	.p2align 4
	.type	_ZL17ucnv_DBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCode, @function
_ZL17ucnv_DBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCode:
.LFB2837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	8(%rdi), %r11
	movq	8(%rsi), %r12
	movq	%rdx, -120(%rbp)
	movq	24(%rsi), %r14
	movq	32(%rdi), %r15
	movq	48(%r11), %rdx
	movq	40(%rdi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	%r14, %r8
	movq	%r15, -72(%rbp)
	movq	88(%rdx), %rsi
	subq	%r15, %rcx
	movq	240(%rdx), %rdi
	testb	$16, 56(%r11)
	cmove	232(%rdx), %rdi
	movl	%ecx, -88(%rbp)
	subq	%rax, %r8
	movq	%rsi, -136(%rbp)
	movq	96(%rdx), %rsi
	movq	%rdi, -104(%rbp)
	movzbl	253(%rdx), %edi
	movq	%rsi, -112(%rbp)
	movl	260(%rdx), %esi
	movb	%dil, -93(%rbp)
	movl	%esi, -92(%rbp)
	movzbl	64(%r12), %esi
	testb	%sil, %sil
	jle	.L989
	movl	76(%r12), %r9d
	movsbq	%sil, %rdi
	movl	72(%r12), %edx
	movb	%r9b, -128(%rbp)
	movsbl	%r9b, %r9d
	subl	%edi, %r9d
	subl	%r9d, %r8d
	testl	%ecx, %ecx
	movl	%r8d, %r9d
	setg	%r8b
	testl	%edx, %edx
	setne	%cl
	andl	%ecx, %r8d
	testl	%r9d, %r9d
	jle	.L990
	movzbl	-1(%r14), %ecx
	testb	%cl, %cl
	js	.L1048
.L990:
	testb	%r8b, %r8b
	jne	.L1096
.L992:
	cmpq	%r14, %rax
	jnb	.L1097
.L1041:
	movl	-88(%rbp), %edx
	movq	%rax, %rcx
	movl	$1, %edi
	leal	(%rdx,%rax), %esi
	testl	%edx, %edx
	jle	.L1003
.L1002:
	movzbl	(%rcx), %r8d
	movl	%esi, %eax
	leaq	1(%rcx), %r13
	subl	%ecx, %eax
	testb	%r8b, %r8b
	js	.L1004
	movzbl	%r8b, %edx
	movl	%edi, %r9d
	movl	%edx, %ecx
	sarl	$2, %ecx
	sall	%cl, %r9d
	testl	%r9d, -92(%rbp)
	jne	.L1098
	movq	-112(%rbp), %rsi
	movzwl	(%rsi), %ecx
	movq	-104(%rbp), %rsi
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L1008
.L1009:
	leaq	1(%r15), %rdx
	movq	%rdx, -72(%rbp)
	cmpw	$255, %cx
	jbe	.L1099
	movzbl	%ch, %edx
	movb	%dl, (%r15)
	cmpl	$1, %eax
	jne	.L1100
	movq	-120(%rbp), %rax
	movq	-72(%rbp), %r15
	movb	%cl, 104(%r11)
	movb	$1, 91(%r11)
	movl	$15, (%rax)
.L1038:
	movq	%r13, 16(%r10)
	movq	%r15, 32(%rbx)
.L987:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1101
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	.cfi_restore_state
	movl	$0, 72(%r12)
	movb	$0, 64(%r12)
	movl	%edi, -140(%rbp)
	movb	%sil, -94(%rbp)
.L1016:
	movzbl	-128(%rbp), %r13d
	cmpb	%r13b, %sil
	jge	.L1050
	movsbl	%r13b, %edi
	movq	24(%r10), %r9
	movl	%edi, -144(%rbp)
	cmpb	$3, %r13b
	je	.L994
	movq	%r14, -152(%rbp)
	leaq	.LC29(%rip), %r8
	movq	%r11, -160(%rbp)
	movl	%edi, %r11d
	.p2align 4,,10
	.p2align 3
.L1000:
	cmpq	%rax, %r9
	jbe	.L995
	movzbl	(%rax), %edi
	movl	%edi, %ecx
	cmpb	$1, %sil
	jg	.L996
	cmpl	$2, %r11d
	jle	.L996
	movl	%edx, %ecx
	movl	$1, %r14d
	andl	$7, %ecx
	sall	%cl, %r14d
	movl	%r14d, %ecx
	movl	%edi, %r14d
	sarl	$4, %r14d
	movslq	%r14d, %r14
	andb	(%r8,%r14), %cl
.L1001:
	testb	%cl, %cl
	je	.L1088
	sall	$6, %edx
	addl	$1, %esi
	addq	$1, %rax
	addl	%edi, %edx
	cmpb	%sil, %r13b
	jne	.L1000
.L1094:
	movq	-152(%rbp), %r14
	movq	-160(%rbp), %r11
	movsbq	%sil, %rdi
	movq	%rax, %r13
.L993:
	movzbl	-128(%rbp), %r9d
	cmpb	%sil, %r9b
	jne	.L1021
	leaq	_ZL12utf8_offsets(%rip), %rax
	subl	(%rax,%rdi,4), %edx
	movl	%edx, %ecx
	andl	$15, %ecx
	cmpb	$3, %r9b
	jle	.L1095
	testb	$1, -93(%rbp)
	jne	.L1095
	movq	-104(%rbp), %rsi
	movl	%ecx, %eax
	movzwl	(%rsi,%rax,2), %ecx
.L1047:
	cmpb	$0, 63(%r11)
	jne	.L1032
	leal	-57344(%rdx), %eax
	cmpl	$6399, %eax
	jbe	.L1032
	leal	-983040(%rdx), %esi
	movl	-88(%rbp), %eax
	cmpl	$131071, %esi
	jbe	.L1032
	.p2align 4,,10
	.p2align 3
.L1008:
	movsbl	2(%rbx), %edi
	cltq
	subq	$8, %rsp
	movq	48(%r11), %rsi
	addq	%r15, %rax
	pushq	-120(%rbp)
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %r9
	pushq	%rdi
	leaq	_ZZL17ucnv_DBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCodeE3nul(%rip), %r8
	movq	%r11, %rdi
	pushq	$-1
	pushq	$0
	pushq	%rax
	movq	%r10, -128(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r8, -64(%rbp)
	call	_ZL9_extFromUP10UConverterPK20UConverterSharedDataiPPKDsS5_PPhPKhPPiiaP10UErrorCode
	movq	-120(%rbp), %rsi
	addq	$48, %rsp
	movq	-88(%rbp), %r11
	movq	-128(%rbp), %r10
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L1037
	movl	%eax, 84(%r11)
	movq	-72(%rbp), %r15
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L989:
	testl	%r8d, %r8d
	jle	.L992
	movzbl	-1(%r14), %ecx
	testb	%cl, %cl
	jns	.L992
	movb	$0, -128(%rbp)
	movl	%r8d, %r9d
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
.L1048:
	cmpb	$-64, %cl
	jge	.L991
	cmpl	$1, %r9d
	jle	.L991
	movzbl	-2(%r14), %r9d
	leal	32(%r9), %r13d
	cmpb	$15, %r13b
	ja	.L990
	andl	$15, %r9d
	leaq	.LC28(%rip), %r13
	shrb	$5, %cl
	movsbl	0(%r13,%r9), %r9d
	sarl	%cl, %r9d
	leaq	-2(%r14), %rcx
	andl	$1, %r9d
	cmovne	%rcx, %r14
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L996:
	cmpb	$-64, %cl
	setl	%cl
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1098:
	leaq	1(%r15), %rax
	movq	%rax, -72(%rbp)
	movb	%r8b, (%r15)
	cmpq	%r13, %r14
	jbe	.L1102
	movl	%esi, %eax
	movq	-72(%rbp), %r15
	subl	%r13d, %eax
	testl	%eax, %eax
	jle	.L1052
	movq	%r13, %rcx
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	-72(%rbp), %rdx
	subl	$2, %eax
	movl	%eax, -88(%rbp)
	movq	%r13, %rax
	leaq	1(%rdx), %rsi
	movq	%rsi, -72(%rbp)
	movb	%cl, (%rdx)
	movq	-72(%rbp), %r15
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L991:
	addl	$62, %ecx
	cmpb	$46, %cl
	sbbq	$0, %r14
	testb	%r8b, %r8b
	je	.L992
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1095:
	movl	%edx, %eax
	movq	-136(%rbp), %rdi
	sarl	$10, %eax
	cltq
	movzwl	(%rdi,%rax,2), %esi
	movl	%edx, %eax
	sarl	$4, %eax
	andl	$63, %eax
	addl	%esi, %eax
	movq	-104(%rbp), %rsi
	cltq
	movl	(%rdi,%rax,4), %edi
	movzwl	%di, %eax
	sall	$4, %eax
	addl	%ecx, %eax
	movzwl	(%rsi,%rax,2), %ecx
	movl	%edx, %esi
	movl	-88(%rbp), %eax
	andl	$15, %esi
	addl	$16, %esi
	btl	%esi, %edi
	jc	.L1009
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1099:
	subl	$1, %eax
	movb	%cl, (%r15)
	movq	-72(%rbp), %r15
	movl	%eax, -88(%rbp)
	movq	%r13, %rax
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L994:
	movq	%r14, -152(%rbp)
	movq	%rax, %rdi
	movl	%esi, %r8d
	movq	%r11, -160(%rbp)
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1017:
	sarl	$5, %ecx
	movl	$1, %r14d
	leaq	.LC28(%rip), %r11
	sall	%cl, %r14d
	movl	%r14d, %ecx
	movl	%edx, %r14d
	andl	$15, %r14d
	testb	%cl, (%r11,%r14)
	je	.L1088
	sall	$6, %edx
	addq	$1, %rdi
	addl	%r13d, %edx
.L1022:
	addl	$1, %r8d
.L1023:
	movl	%r8d, %esi
	movq	%rdi, %rax
	cmpq	%rdi, %r9
	jbe	.L995
	movzbl	(%rdi), %r13d
	movl	%r13d, %ecx
	cmpb	$1, %r8b
	jle	.L1017
	cmpb	$-64, %r13b
	jge	.L1088
	leaq	1(%rdi), %rdi
	sall	$6, %edx
	leal	1(%r8), %esi
	movq	%rdi, %rax
	addl	%r13d, %edx
	cmpb	$2, %sil
	jle	.L1022
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1088:
	movsbl	%sil, %edi
.L1021:
	subl	-140(%rbp), %edi
	movzbl	-94(%rbp), %r11d
	movslq	%edi, %rdi
	subq	%rdi, %rax
	cmpb	%sil, %r11b
	jge	.L1030
	movzbl	(%rax), %edi
	movsbq	%r11b, %rcx
	leal	1(%r11), %edx
	movb	%dil, 65(%r12,%rcx)
	cmpb	%dl, %sil
	jle	.L1031
	movzbl	1(%rax), %edi
	movsbq	%dl, %rdx
	leal	2(%r11), %ecx
	movb	%dil, 65(%r12,%rdx)
	cmpb	%cl, %sil
	jle	.L1031
	movzbl	2(%rax), %edi
	movsbq	%cl, %rcx
	leal	3(%r11), %edx
	movb	%dil, 65(%r12,%rcx)
	cmpb	%sil, %dl
	jge	.L1031
	movzbl	3(%rax), %edi
	movsbq	%dl, %rdx
	leal	4(%r11), %ecx
	movb	%dil, 65(%r12,%rdx)
	cmpb	%sil, %cl
	jge	.L1031
	movzbl	4(%rax), %edi
	movsbq	%cl, %rcx
	leal	5(%r11), %edx
	movb	%dil, 65(%r12,%rcx)
	cmpb	%sil, %dl
	jge	.L1031
	movzbl	5(%rax), %edi
	movsbq	%dl, %rdx
	leal	6(%r11), %ecx
	movb	%dil, 65(%r12,%rdx)
	cmpb	%sil, %cl
	jge	.L1031
	movzbl	6(%rax), %edx
	movsbq	%cl, %rcx
	movb	%dl, 65(%r12,%rcx)
.L1031:
	movzbl	-94(%rbp), %r11d
	notl	%r11d
	leal	(%r11,%rsi), %edx
	movzbl	%dl, %edx
	leaq	1(%rax,%rdx), %rax
.L1030:
	movb	%sil, 64(%r12)
	movq	%rax, 16(%r10)
	movq	-120(%rbp), %rax
	movq	%r15, 32(%rbx)
	movl	$12, (%rax)
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L995:
	movsbl	%sil, %ecx
	movzbl	-94(%rbp), %r11d
	subl	-140(%rbp), %ecx
	movslq	%ecx, %rcx
	subq	%rcx, %rax
	cmpb	%sil, %r11b
	jge	.L1024
	movzbl	(%rax), %r8d
	movsbq	%r11b, %rdi
	leal	1(%r11), %ecx
	movb	%r8b, 65(%r12,%rdi)
	cmpb	%sil, %cl
	jge	.L1025
	movzbl	1(%rax), %r8d
	movsbq	%cl, %rcx
	leal	2(%r11), %edi
	movb	%r8b, 65(%r12,%rcx)
	cmpb	%sil, %dil
	jge	.L1025
	movzbl	2(%rax), %r8d
	movsbq	%dil, %rdi
	leal	3(%r11), %ecx
	movb	%r8b, 65(%r12,%rdi)
	cmpb	%cl, %sil
	jle	.L1025
	movzbl	3(%rax), %r8d
	movsbq	%cl, %rcx
	leal	4(%r11), %edi
	movb	%r8b, 65(%r12,%rcx)
	cmpb	%sil, %dil
	jge	.L1025
	movzbl	4(%rax), %r8d
	movsbq	%dil, %rdi
	leal	5(%r11), %ecx
	movb	%r8b, 65(%r12,%rdi)
	cmpb	%sil, %cl
	jge	.L1025
	movzbl	5(%rax), %r8d
	movsbq	%cl, %rcx
	leal	6(%r11), %edi
	movb	%r8b, 65(%r12,%rcx)
	cmpb	%sil, %dil
	jge	.L1025
	movzbl	6(%rax), %ecx
	movsbq	%dil, %rdi
	movb	%cl, 65(%r12,%rdi)
.L1025:
	movzbl	-94(%rbp), %r11d
	notl	%r11d
	leal	(%r11,%rsi), %ecx
	movzbl	%cl, %ecx
	leaq	1(%rax,%rcx), %rax
.L1024:
	movb	%sil, 64(%r12)
	movl	-144(%rbp), %esi
	movl	%edx, 72(%r12)
	movl	%esi, 76(%r12)
	movq	%rax, 16(%r10)
	movq	%r15, 32(%rbx)
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1032:
	movl	-88(%rbp), %eax
	testw	%cx, %cx
	jne	.L1009
	jmp	.L1008
.L1097:
	movq	-120(%rbp), %rsi
	movq	%rax, %r13
	movl	(%rsi), %edx
.L1042:
	testl	%edx, %edx
	jg	.L1038
	movl	208(%r11), %eax
	testl	%eax, %eax
	jns	.L1038
.L1040:
	movq	24(%r10), %rdi
	cmpq	%r13, %rdi
	jbe	.L1038
	movzbl	0(%r13), %ecx
	leaq	1(%r13), %r9
	movl	$1, %r11d
	movb	%cl, 65(%r12)
	testb	%cl, %cl
	js	.L1103
.L1043:
	cmpq	%r9, %rdi
	jbe	.L1060
	movl	$1, %r8d
	movq	%r9, %rax
	subl	%r9d, %r8d
	.p2align 4,,10
	.p2align 3
.L1045:
	leal	(%r8,%rax), %esi
	movzbl	(%rax), %edx
	addq	$1, %rax
	sall	$6, %ecx
	movsbq	%sil, %rsi
	movb	%dl, 65(%r12,%rsi)
	addl	%edx, %ecx
	cmpq	%rax, %rdi
	jne	.L1045
	movq	%r13, %rax
	leaq	-1(%rdi), %r13
	subq	%rax, %r13
	subl	%eax, %edi
	addq	%r9, %r13
.L1044:
	movl	%ecx, 72(%r12)
	movb	%dil, 64(%r12)
	movl	%r11d, 76(%r12)
	jmp	.L1038
.L1052:
	movq	%r13, %rax
.L1003:
	movq	-120(%rbp), %rsi
	movq	%rax, %r13
	movl	$15, (%rsi)
	jmp	.L1038
.L1004:
	cmpb	$-33, %r8b
	jbe	.L1010
	movzbl	%r8b, %edx
	cmpb	$-19, %r8b
	jbe	.L1104
.L1011:
	leal	62(%r8), %ecx
	cmpb	$50, %cl
	jbe	.L1012
	movb	$0, -128(%rbp)
.L1015:
	movl	%eax, -88(%rbp)
	movl	$1, %edi
	movq	%r13, %rax
	movl	$1, %esi
	movl	$0, -140(%rbp)
	movb	$0, -94(%rbp)
	jmp	.L1016
.L1050:
	movq	%rax, %r13
	jmp	.L993
.L1010:
	movzbl	%r8b, %edx
	cmpb	$-63, %r8b
	jbe	.L1011
	movzbl	0(%r13), %esi
	leal	-128(%rsi), %edx
	cmpb	$63, %dl
	jbe	.L1014
	movzbl	%r8b, %edx
.L1012:
	cmpb	$-33, %r8b
	seta	%sil
	cmpb	$-17, %r8b
	seta	%cl
	leal	2(%rsi,%rcx), %esi
	movb	%sil, -128(%rbp)
	jmp	.L1015
.L1037:
	movl	208(%r11), %edx
	movq	-72(%rbp), %r15
	testl	%edx, %edx
	js	.L1039
	movl	$-127, (%rsi)
	jmp	.L1038
.L1103:
	leal	62(%rcx), %edx
	xorl	%r11d, %r11d
	cmpb	$50, %dl
	ja	.L1043
	cmpb	$-33, %cl
	seta	%dl
	cmpb	$-17, %cl
	seta	%al
	leal	2(%rdx,%rax), %eax
	movzbl	%al, %r11d
	jmp	.L1043
.L1102:
	movq	-120(%rbp), %rax
	movq	-72(%rbp), %r15
	movl	(%rax), %edx
	jmp	.L1042
.L1104:
	movzbl	0(%r13), %r9d
	movq	%r8, %rsi
	leaq	.LC28(%rip), %rdi
	andl	$15, %esi
	movsbl	(%rdi,%rsi), %edi
	movl	%r9d, %esi
	shrb	$5, %sil
	btl	%esi, %edi
	jnc	.L1012
	movzbl	1(%r13), %esi
	addl	$-128, %esi
	cmpb	$63, %sil
	ja	.L1012
	movl	%edx, %edi
	andl	$63, %r9d
	leaq	3(%rcx), %r13
	movzbl	%sil, %edx
	sall	$6, %edi
	movq	-112(%rbp), %rsi
	andl	$960, %edi
	orl	%r9d, %edi
	movslq	%edi, %rcx
	sall	$6, %edi
	movzwl	(%rsi,%rcx,2), %ecx
	movq	-104(%rbp), %rsi
	addl	%edx, %ecx
	orl	%edi, %edx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	testw	%cx, %cx
	jne	.L1009
	jmp	.L1008
.L1014:
	movq	-112(%rbp), %rdi
	movl	%r8d, %esi
	andl	$31, %r8d
	leaq	2(%rcx), %r13
	movzbl	%dl, %edx
	andl	$31, %esi
	movzwl	(%rdi,%r8,2), %ecx
	movq	-104(%rbp), %rdi
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	testw	%cx, %cx
	jne	.L1009
	movzbl	%sil, %ecx
	sall	$6, %ecx
	orl	%ecx, %edx
	jmp	.L1008
.L1060:
	movq	%r9, %r13
	movl	$1, %edi
	jmp	.L1044
.L1039:
	movl	40(%rbx), %eax
	subl	%r15d, %eax
	movl	%eax, -88(%rbp)
	cmpq	%r14, %r13
	jnb	.L1040
	movq	%r13, %rax
	jmp	.L1041
.L1101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2837:
	.size	_ZL17ucnv_DBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCode, .-_ZL17ucnv_DBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCode
	.p2align 4
	.type	_ZL7enumToUP19UConverterMBCSTablePaijjPFaPKvjPiES3_P10UErrorCode.constprop.0, @function
_ZL7enumToUP19UConverterMBCSTablePaijjPFaPKvjPiES3_P10UErrorCode.constprop.0:
.LFB3762:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movslq	%edx, %rdx
	sall	$8, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rdi), %rsi
	movl	%ecx, -220(%rbp)
	movq	%rdx, %rcx
	movzbl	(%r10,%rdx), %edx
	movq	%rdi, -232(%rbp)
	movq	%rax, -240(%rbp)
	salq	$10, %rcx
	movl	%edx, %r15d
	leal	0(,%rdx,4), %ebx
	andl	$7, %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, %r15d
	movq	%rsi, -248(%rbp)
	movq	8(%rdi), %rax
	sall	$5, %r15d
	movl	%r8d, -200(%rbp)
	movl	%r15d, -196(%rbp)
	andl	$224, %ebx
	jne	.L1106
	cmpb	$63, %dl
	jg	.L1137
.L1108:
	leaq	(%rax,%rcx), %r15
	movslq	%ebx, %rbx
	movl	$-1, %r14d
	movq	%r15, %r11
	movq	%r10, %r15
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1139:
	movl	%ecx, %edx
	shrl	$24, %edx
	movl	%edx, %eax
	cmpb	$0, (%r15,%rax)
	jns	.L1113
.L1115:
	andl	$31, %r12d
	movl	$-1, -192(%rbp,%r12,4)
.L1114:
	leal	1(%r13), %eax
	testb	$31, %al
	jne	.L1122
	testl	%r14d, %r14d
	jns	.L1138
.L1122:
	addq	$1, %rbx
	cmpl	%ebx, -196(%rbp)
	jle	.L1110
.L1111:
	movl	(%r11,%rbx,4), %ecx
	movl	%ebx, %r13d
	movl	%ebx, %r12d
	testl	%ecx, %ecx
	jns	.L1139
	movl	%ecx, %eax
	shrl	$20, %eax
	andl	$15, %eax
	jne	.L1116
	movzwl	%cx, %edx
	andl	%edx, %r14d
.L1117:
	andl	$31, %r12d
	movl	%edx, -192(%rbp,%r12,4)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1138:
	leal	-31(%r13), %esi
	movq	%r9, %rdi
	orl	-200(%rbp), %esi
	movq	%r11, -216(%rbp)
	leaq	-192(%rbp), %rdx
	movq	%r9, -208(%rbp)
	call	_ZL20writeStage3RoundtripPKvjPi
	testb	%al, %al
	je	.L1123
	movq	-208(%rbp), %r9
	movq	-216(%rbp), %r11
	movl	$-1, %r14d
	addq	$1, %rbx
	cmpl	%ebx, -196(%rbp)
	jg	.L1111
.L1110:
	movl	$1, %eax
.L1105:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1140
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1113:
	.cfi_restore_state
	subq	$8, %rsp
	movl	-200(%rbp), %r8d
	movq	%r15, %rsi
	pushq	-240(%rbp)
	movq	-232(%rbp), %rdi
	andl	$16777215, %ecx
	movq	%r11, -216(%rbp)
	addl	-220(%rbp), %ecx
	orl	%ebx, %r8d
	movq	%r9, -208(%rbp)
	call	_ZL7enumToUP19UConverterMBCSTablePaijjPFaPKvjPiES3_P10UErrorCode.constprop.0
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	movq	-208(%rbp), %r9
	movq	-216(%rbp), %r11
	jne	.L1115
.L1123:
	xorl	%eax, %eax
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1116:
	cmpl	$4, %eax
	je	.L1141
	cmpl	$5, %eax
	je	.L1142
	movl	$-1, %edx
	cmpl	$1, %eax
	jne	.L1117
	andl	$1048575, %ecx
	leal	65536(%rcx), %edx
	andl	%edx, %r14d
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1141:
	movq	-248(%rbp), %rax
	movzwl	%cx, %ecx
	addl	-220(%rbp), %ecx
	movslq	%ecx, %rcx
	movzwl	(%rax,%rcx,2), %edx
	cmpl	$65533, %edx
	jg	.L1126
.L1136:
	andl	%edx, %r14d
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1121:
	cmpl	$57344, %edx
	je	.L1143
	.p2align 4,,10
	.p2align 3
.L1126:
	movl	$-1, %edx
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1137:
	movl	$-1, -192(%rbp)
	movl	$1, %ebx
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1106:
	cmpl	-196(%rbp), %ebx
	jl	.L1108
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1142:
	movzwl	%cx, %eax
	movq	-248(%rbp), %rdi
	addl	-220(%rbp), %eax
	cltq
	movzwl	(%rdi,%rax,2), %edx
	leaq	(%rax,%rax), %rcx
	cmpl	$55295, %edx
	jle	.L1136
	cmpl	$56319, %edx
	jg	.L1121
	movzwl	2(%rdi,%rcx), %eax
	movl	%edx, %ecx
	sall	$10, %ecx
	andl	$1047552, %ecx
	leal	9216(%rax,%rcx), %edx
	andl	%edx, %r14d
	jmp	.L1117
.L1143:
	movzwl	2(%rdi,%rcx), %edx
	jmp	.L1136
.L1140:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3762:
	.size	_ZL7enumToUP19UConverterMBCSTablePaijjPFaPKvjPiES3_P10UErrorCode.constprop.0, .-_ZL7enumToUP19UConverterMBCSTablePaijjPFaPKvjPiES3_P10UErrorCode.constprop.0
	.p2align 4
	.type	_ZL13ucnv_MBCSLoadP20UConverterSharedDataP18UConverterLoadArgsPKhP10UErrorCode, @function
_ZL13ucnv_MBCSLoadP20UConverterSharedDataP18UConverterLoadArgsPKhP10UErrorCode:
.LFB2818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdx), %eax
	cmpb	$4, %al
	je	.L1145
	cmpb	$5, %al
	je	.L1233
.L1231:
	movl	$13, (%r10)
.L1144:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1234
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1233:
	.cfi_restore_state
	cmpb	$2, 1(%rdx)
	jbe	.L1231
	movl	32(%rdx), %eax
	testl	$65408, %eax
	jne	.L1231
	movl	24(%r13), %esi
	movl	%eax, %edx
	xorl	%r14d, %r14d
	andl	$63, %edx
	movb	%sil, 252(%rdi)
	movl	%esi, %ecx
	testb	$64, %al
	je	.L1148
	testb	%sil, %sil
	je	.L1231
	movl	$1, %r14d
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1145:
	movl	24(%rdx), %eax
	xorl	%r14d, %r14d
	movl	$8, %edx
	movb	%al, 252(%rdi)
	movl	%eax, %ecx
.L1148:
	movl	24(%r13), %eax
	shrl	$8, %eax
	je	.L1150
	movl	%eax, %eax
	addq	%r13, %rax
	movq	%rax, 288(%r12)
.L1150:
	leaq	48(%r12), %r9
	cmpb	$14, %cl
	je	.L1235
	cmpb	$12, %cl
	ja	.L1231
	movl	$1, %eax
	salq	%cl, %rax
	testl	$4879, %eax
	je	.L1231
	cmpb	$0, 8(%rbx)
	movq	%r10, -288(%rbp)
	movq	%r9, -280(%rbp)
	jne	.L1144
	movl	4(%r13), %eax
	movq	8(%r12), %rdi
	movl	$20, %r8d
	leaq	-224(%rbp), %rsi
	movw	%r8w, -224(%rbp)
	movb	%al, 48(%r12)
	movl	8(%r13), %eax
	movl	%eax, 52(%r12)
	leal	0(,%rdx,4), %eax
	movl	4(%r13), %edx
	addq	%r13, %rax
	salq	$10, %rdx
	movq	%rax, 56(%r12)
	addq	%rdx, %rax
	movq	%rax, 80(%r12)
	movl	12(%r13), %eax
	addq	%r13, %rax
	movq	%rax, 72(%r12)
	movl	16(%r13), %eax
	addq	%r13, %rax
	movq	%rax, 88(%r12)
	movl	20(%r13), %eax
	addq	%r13, %rax
	movq	%rax, 232(%r12)
	movl	28(%r13), %eax
	movl	%eax, 248(%r12)
	call	udata_getInfo_67@PLT
	cmpb	$6, -212(%rbp)
	movq	-280(%rbp), %r9
	movq	-288(%rbp), %r10
	ja	.L1170
	je	.L1236
.L1171:
	movb	$3, 253(%r12)
	movl	$3, %r8d
.L1172:
	movq	56(%r12), %rsi
	xorl	%ebx, %ebx
	movl	$-1, %edx
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L1178:
	movl	%ebx, %eax
	orl	$-2147483648, %eax
	cmpl	(%rsi,%rbx,4), %eax
	je	.L1177
	movl	%ebx, %ecx
	movl	%edi, %eax
	sarl	$2, %ecx
	sall	%cl, %eax
	notl	%eax
	andl	%eax, %edx
.L1177:
	addq	$1, %rbx
	cmpq	$128, %rbx
	jne	.L1178
	movq	%r10, -296(%rbp)
	movq	%r9, -288(%rbp)
	movl	%edx, 260(%r12)
	testb	%r14b, %r14b
	je	.L1166
	movl	%r8d, %eax
	movl	20(%r13), %r14d
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%r8d, %r8d
	andl	$-512, %r8d
	addl	$544, %r8d
	cmpb	$1, %al
	sbbl	%edx, %edx
	movl	%r8d, -320(%rbp)
	movl	%r8d, %r15d
	andl	$-1024, %edx
	testb	%al, %al
	movl	$2176, %eax
	cmovne	%rax, %rbx
	subl	16(%r13), %r14d
	shrl	$2, %r14d
	movl	%r14d, %eax
	subl	%r8d, %eax
	movl	%eax, -300(%rbp)
	movl	36(%r13), %eax
	movl	%eax, -280(%rbp)
	leal	1088(%rdx,%rax,2), %edx
	movl	248(%r12), %eax
	leal	(%rax,%rdx,2), %edx
	movq	%rdx, %rdi
	movq	%rdx, -312(%rbp)
	call	uprv_malloc_67@PLT
	movq	-312(%rbp), %rdx
	movl	-320(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, 264(%r12)
	movq	%rax, %r13
	movq	-288(%rbp), %r9
	movq	-296(%rbp), %r10
	je	.L1237
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r10, -320(%rbp)
	movl	%r8d, -312(%rbp)
	movq	%r9, -296(%rbp)
	call	memset@PLT
	movq	88(%r12), %rax
	leaq	8(%r13), %rdi
	andq	$-8, %rdi
	movq	(%rax), %rdx
	movq	%rax, %rsi
	movq	%rdx, 0(%r13)
	movl	%ebx, %edx
	movq	-8(%rax,%rdx), %rcx
	movq	%rcx, -8(%r13,%rdx)
	movq	%r13, %rdx
	subq	%rdi, %rdx
	subq	%rdx, %rsi
	addl	%ebx, %edx
	shrl	$3, %edx
	movl	%edx, %ecx
	rep movsq
	leaq	0(%r13,%rbx), %rcx
	movq	%rcx, -288(%rbp)
	movl	-280(%rbp), %edx
	movl	-300(%rbp), %esi
	subl	%r14d, %edx
	sall	$2, %esi
	addl	%r15d, %edx
	leaq	(%rcx,%rdx,4), %rdi
	movl	%esi, %edx
	leaq	(%rax,%rbx), %rsi
	call	memcpy@PLT
	xorl	%r11d, %r11d
	xorl	%edi, %edi
	movq	%r13, 88(%r12)
	movzwl	256(%r12), %ebx
	movl	-280(%rbp), %r15d
	movq	-288(%rbp), %rcx
	movq	-296(%rbp), %r9
	addl	$1, %ebx
	movl	-312(%rbp), %r8d
	movq	-320(%rbp), %r10
	leaq	(%rcx,%r15,4), %rax
	sarl	$6, %ebx
	movq	%rax, 232(%r12)
	jne	.L1181
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1187:
	addq	$1, %rdi
	movl	%eax, %r11d
	cmpl	%eax, %ebx
	jle	.L1188
.L1181:
	movzwl	0(%r13,%rdi,2), %ecx
	leal	16(%r11), %eax
	movq	%rcx, %rdx
	cmpl	%r8d, %ecx
	je	.L1187
	movq	%rdi, %rcx
	leaq	64(%rdx), %rsi
	salq	$5, %rcx
	addq	96(%r12), %rcx
	.p2align 4,,10
	.p2align 3
.L1186:
	movzwl	(%rcx), %eax
	testl	%eax, %eax
	je	.L1185
	sarl	$4, %eax
	leal	1(%rdx), %r14d
	leal	1(%rax), %r15d
	movslq	%r14d, %r14
	movl	%eax, 0(%r13,%rdx,4)
	movl	%r15d, 0(%r13,%r14,4)
	leal	2(%rdx), %r14d
	leal	2(%rax), %r15d
	addl	$3, %eax
	movslq	%r14d, %r14
	movl	%r15d, 0(%r13,%r14,4)
	leal	3(%rdx), %r14d
	movslq	%r14d, %r14
	movl	%eax, 0(%r13,%r14,4)
.L1185:
	addq	$4, %rdx
	addq	$2, %rcx
	cmpq	%rsi, %rdx
	jne	.L1186
	leal	16(%r11), %eax
	jmp	.L1187
.L1236:
	cmpb	$0, -211(%rbp)
	je	.L1171
.L1170:
	movq	16(%r12), %rax
	movzbl	79(%rax), %eax
	movl	%eax, %r8d
	andl	$3, %r8d
	movb	%r8b, 253(%r12)
	cmpb	$2, 1(%r13)
	jbe	.L1172
	testb	$2, %al
	jne	.L1172
	cmpb	$1, 48(%r12)
	movzbl	2(%r13), %eax
	je	.L1238
	cmpb	$-42, %al
	jbe	.L1172
	movb	$1, 254(%r12)
	movq	232(%r12), %rax
	testb	%r14b, %r14b
	jne	.L1195
	movl	248(%r12), %edx
	addq	%rdx, %rax
.L1195:
	movq	%rax, 96(%r12)
	movzbl	2(%r13), %eax
	sall	$8, %eax
	orb	$-1, %al
	movw	%ax, 256(%r12)
	jmp	.L1172
.L1242:
	movq	56(%r12), %rax
	movl	56(%rax), %eax
	testl	%eax, %eax
	jns	.L1166
	movl	%eax, %edx
	shrl	$20, %edx
	andl	$15, %edx
	cmpl	$8, %edx
	jne	.L1166
	shrl	$24, %eax
	andl	$127, %eax
	je	.L1166
	movb	%al, 49(%r12)
	movb	$-37, 252(%r12)
	.p2align 4,,10
	.p2align 3
.L1166:
	cmpb	$0, 254(%r12)
	movzbl	252(%r12), %eax
	je	.L1191
	cmpb	$1, 48(%r12)
	je	.L1239
	cmpb	$1, %al
	jne	.L1191
	leaq	_ZL13_DBCSUTF8Impl(%rip), %rax
	movq	%rax, 32(%r12)
	jmp	.L1144
.L1239:
	leaq	_ZL13_SBCSUTF8Impl(%rip), %rdi
	movq	%rdi, 32(%r12)
.L1191:
	cmpb	$-37, %al
	je	.L1199
	cmpb	$12, %al
	jne	.L1144
.L1199:
	movl	$0, 260(%r12)
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1235:
	movq	288(%r12), %r14
	pxor	%xmm0, %xmm0
	movq	$0, -240(%rbp)
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm0, -256(%rbp)
	movl	$40, -272(%rbp)
	testq	%r14, %r14
	je	.L1231
	cmpl	$1, 4(%rbx)
	je	.L1154
	movl	$14, (%r10)
	jmp	.L1144
.L1154:
	leal	0(,%rdx,4), %eax
	movq	%r10, -280(%rbp)
	addq	%rax, %r13
	movq	16(%r12), %rax
	movq	%r9, -288(%rbp)
	movq	%r13, %rdi
	leaq	4(%rax), %rsi
	call	strcmp@PLT
	movq	-280(%rbp), %r10
	testl	%eax, %eax
	je	.L1231
	movzbl	8(%rbx), %eax
	movq	%r10, %rsi
	leaq	-272(%rbp), %rdi
	movq	%r13, -248(%rbp)
	movl	$2, -268(%rbp)
	movb	%al, -264(%rbp)
	movzwl	10(%rbx), %eax
	movw	%ax, -262(%rbp)
	movl	12(%rbx), %eax
	movl	%eax, -260(%rbp)
	movq	16(%rbx), %rax
	movq	%rax, -256(%rbp)
	call	ucnv_load_67@PLT
	movq	-280(%rbp), %r10
	movq	%rax, %r13
	movl	(%r10), %r11d
	testl	%r11d, %r11d
	jg	.L1144
	movq	16(%rax), %rax
	cmpb	$2, 69(%rax)
	jne	.L1156
	cmpq	$0, 280(%r13)
	movq	-288(%rbp), %r9
	je	.L1157
.L1156:
	movq	%r13, %rdi
	movq	%r10, -280(%rbp)
	call	ucnv_unload_67@PLT
	movq	-280(%rbp), %r10
	jmp	.L1231
.L1188:
	movq	56(%r12), %rdi
	leaq	-192(%rbp), %r13
	pcmpeqd	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r10, -288(%rbp)
	movq	%r9, -280(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZL12getStatePropPA256_KiPai
	movzbl	48(%r12), %eax
	testb	%al, %al
	je	.L1166
	movq	-280(%rbp), %r9
	movq	-288(%rbp), %r10
	xorl	%ebx, %ebx
.L1183:
	cmpb	$63, 0(%r13,%rbx)
	jg	.L1240
.L1189:
	addq	$1, %rbx
	movzbl	%al, %edx
	cmpl	%ebx, %edx
	jle	.L1166
	cmpb	$63, 0(%r13,%rbx)
	jle	.L1189
.L1240:
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	xorl	%r8d, %r8d
	pushq	%r10
	movq	%r13, %rsi
	movq	%r9, %rdi
	addq	$1, %rbx
	movq	%r10, -288(%rbp)
	movq	%r9, -280(%rbp)
	call	_ZL7enumToUP19UConverterMBCSTablePaijjPFaPKvjPiES3_P10UErrorCode.constprop.0
	movzbl	48(%r12), %edx
	popq	%rcx
	movq	-280(%rbp), %r9
	movq	-288(%rbp), %r10
	cmpl	%ebx, %edx
	movl	%edx, %eax
	popq	%rsi
	jg	.L1183
	jmp	.L1166
.L1238:
	cmpb	$14, %al
	jbe	.L1172
	movb	$1, 254(%r12)
	movq	88(%r12), %rsi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1176:
	movl	%ecx, %eax
	sarl	$4, %eax
	cltq
	movzwl	(%rsi,%rax,2), %edx
	leal	0(,%rcx,4), %eax
	andl	$60, %eax
	addl	%edx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	movw	%ax, 104(%r12,%rcx,2)
	addq	$1, %rcx
	cmpq	$64, %rcx
	jne	.L1176
	movl	$4095, %edi
	movw	%di, 256(%r12)
	jmp	.L1172
.L1157:
	cmpb	$0, 8(%rbx)
	jne	.L1241
	movdqu	48(%r13), %xmm1
	movq	%r13, %xmm0
	movups	%xmm1, 48(%r12)
	movdqu	64(%r13), %xmm2
	movups	%xmm2, 16(%r9)
	movdqu	80(%r13), %xmm3
	movups	%xmm3, 32(%r9)
	movdqu	96(%r13), %xmm4
	movups	%xmm4, 48(%r9)
	movdqu	112(%r13), %xmm5
	movups	%xmm5, 64(%r9)
	movdqu	128(%r13), %xmm6
	movups	%xmm6, 80(%r9)
	movdqu	144(%r13), %xmm7
	movups	%xmm7, 96(%r9)
	movdqu	160(%r13), %xmm1
	movups	%xmm1, 112(%r9)
	movdqu	176(%r13), %xmm2
	movups	%xmm2, 128(%r9)
	movq	%r14, %xmm2
	movdqu	192(%r13), %xmm3
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm3, 144(%r9)
	movdqu	208(%r13), %xmm4
	movups	%xmm4, 160(%r9)
	movdqu	224(%r13), %xmm5
	movups	%xmm5, 176(%r9)
	movdqu	240(%r13), %xmm6
	movups	%xmm6, 192(%r9)
	movdqu	256(%r13), %xmm7
	movups	%xmm7, 208(%r9)
	movdqu	272(%r13), %xmm1
	movups	%xmm1, 224(%r9)
	movq	288(%r13), %rax
	movq	%rax, 240(%r9)
	movups	%xmm0, 280(%r12)
	movq	16(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 64(%r12)
	movq	$0, 240(%r12)
	movups	%xmm0, 264(%r12)
	movzbl	69(%rax), %edx
	cmpb	$1, %dl
	je	.L1159
	cmpb	$2, %dl
	jne	.L1166
	cmpb	$1, 70(%rax)
	jle	.L1166
.L1159:
	cmpb	$12, 252(%r13)
	je	.L1242
	movq	16(%r13), %rax
	movl	68(%rax), %eax
	xorb	%al, %al
	cmpl	$33620480, %eax
	jne	.L1166
	movzbl	48(%r12), %r15d
	movq	%r10, -280(%rbp)
	testb	%r15b, %r15b
	js	.L1166
	movzbl	%r15b, %ebx
	leal	1(%rbx), %r14d
	sall	$10, %r14d
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L1243
	movq	56(%r12), %rsi
	leal	-1024(%r14), %edx
	movq	%rax, %rdi
	sall	$24, %ebx
	movslq	%edx, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	leaq	1024(%rax), %rdx
.L1164:
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jns	.L1163
	movl	%ebx, (%rax)
.L1163:
	addq	$4, %rax
	cmpq	%rdx, %rax
	jne	.L1164
	movzbl	%r15b, %eax
	movdqa	.LC30(%rip), %xmm0
	salq	$10, %rax
	addq	%rcx, %rax
	leaq	1024(%rax), %rdx
.L1165:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1165
	addl	$1, %r15d
	movq	%rcx, 56(%r12)
	movb	%r15b, 48(%r12)
	movb	$1, 50(%r12)
	movb	$-37, 252(%r12)
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	%r13, %rdi
	call	ucnv_unload_67@PLT
	jmp	.L1144
.L1243:
	movq	%r13, %rdi
	call	ucnv_unload_67@PLT
	movq	-280(%rbp), %r10
	movl	$7, (%r10)
	jmp	.L1144
.L1237:
	movl	$7, (%r10)
	jmp	.L1166
.L1234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2818:
	.size	_ZL13ucnv_MBCSLoadP20UConverterSharedDataP18UConverterLoadArgsPKhP10UErrorCode, .-_ZL13ucnv_MBCSLoadP20UConverterSharedDataP18UConverterLoadArgsPKhP10UErrorCode
	.p2align 4
	.type	_ZL17ucnv_SBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCode, @function
_ZL17ucnv_SBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCode:
.LFB2836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%r10), %r14
	movq	40(%r10), %rcx
	movq	%rdx, -128(%rbp)
	movq	16(%rsi), %rbx
	movq	8(%rsi), %r13
	subq	%r14, %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	24(%rsi), %rdi
	movq	%r14, -72(%rbp)
	movl	%ecx, -84(%rbp)
	movq	48(%rax), %r12
	testb	$16, 56(%rax)
	movq	%rax, -144(%rbp)
	movq	%rdi, -96(%rbp)
	movq	88(%r12), %rdx
	movq	240(%r12), %rsi
	cmove	232(%r12), %rsi
	cmpb	$1, 63(%rax)
	movq	%rdx, -136(%rbp)
	movl	260(%r12), %edx
	sbbl	%r11d, %r11d
	movzbl	253(%r12), %eax
	movq	%rsi, -112(%rbp)
	andw	$1024, %r11w
	movq	%rdi, %rsi
	movl	%edx, -88(%rbp)
	addw	$2048, %r11w
	subq	%rbx, %rdi
	movb	%al, -117(%rbp)
	movzbl	64(%r13), %eax
	testb	%al, %al
	jle	.L1247
	movl	76(%r13), %r9d
	movl	72(%r13), %edx
	movl	%r9d, %r8d
	movl	%edx, -104(%rbp)
	movsbl	%r9b, %r9d
	movsbl	%al, %edx
	subl	%edx, %r9d
	movl	%edx, -116(%rbp)
	movl	-104(%rbp), %edx
	subl	%r9d, %edi
	testl	%ecx, %ecx
	movl	%edi, %r9d
	setg	%dil
	testl	%edx, %edx
	setne	%cl
	andl	%ecx, %edi
	testl	%r9d, %r9d
	jle	.L1248
	movzbl	-1(%rsi), %ecx
	testb	%cl, %cl
	js	.L1301
.L1248:
	testb	%dil, %dil
	jne	.L1343
.L1250:
	cmpq	-96(%rbp), %rbx
	jnb	.L1260
.L1287:
	movl	-84(%rbp), %esi
	testl	%esi, %esi
	jle	.L1261
	leaq	104(%r12), %r9
	movl	$1, %r8d
.L1289:
	movzbl	(%rbx), %eax
	movl	%esi, -84(%rbp)
	leaq	1(%rbx), %rdi
	testb	%al, %al
	js	.L1262
	movzbl	%al, %edx
	movl	%r8d, %ebx
	movl	%edx, %ecx
	sarl	$2, %ecx
	sall	%cl, %ebx
	testl	%ebx, -88(%rbp)
	jne	.L1339
	movzwl	104(%r12), %eax
	movq	-112(%rbp), %rbx
	addl	%edx, %eax
	cltq
	movzwl	(%rbx,%rax,2), %eax
.L1265:
	cmpw	%r11w, %ax
	jb	.L1306
	leaq	1(%r14), %rdx
	subl	$1, -84(%rbp)
	movq	%rdi, %rbx
	movq	%rdx, -72(%rbp)
	movb	%al, (%r14)
	movq	-72(%rbp), %r14
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1343:
	movl	-116(%rbp), %edx
	movl	$0, 72(%r13)
	movb	$0, 64(%r13)
	movl	%edx, -148(%rbp)
	movb	%al, -118(%rbp)
.L1272:
	cmpb	%al, %r8b
	jle	.L1251
	movsbl	%r8b, %esi
	movq	24(%r15), %r9
	movl	%esi, -116(%rbp)
	cmpb	$3, %r8b
	je	.L1252
	movq	%r12, -160(%rbp)
	movl	-104(%rbp), %edx
	movw	%r11w, -150(%rbp)
	movl	%esi, %r11d
	.p2align 4,,10
	.p2align 3
.L1258:
	cmpq	%rbx, %r9
	jbe	.L1336
	movzbl	(%rbx), %esi
	movl	%esi, %ecx
	cmpb	$1, %al
	jg	.L1254
	cmpl	$2, %r11d
	jle	.L1254
	movl	%edx, %ecx
	movl	$1, %edi
	leaq	.LC29(%rip), %r12
	andl	$7, %ecx
	sall	%cl, %edi
	movl	%edi, %ecx
	movl	%esi, %edi
	sarl	$4, %edi
	movslq	%edi, %rdi
	andb	(%r12,%rdi), %cl
.L1259:
	testb	%cl, %cl
	je	.L1332
	sall	$6, %edx
	addl	$1, %eax
	addq	$1, %rbx
	addl	%esi, %edx
	cmpb	%r8b, %al
	jne	.L1258
	movl	%edx, -104(%rbp)
	movq	-160(%rbp), %r12
	movzwl	-150(%rbp), %r11d
.L1257:
	movsbl	%al, %edx
	movl	%edx, -116(%rbp)
.L1251:
	cmpb	%r8b, %al
	jne	.L1277
	movslq	-116(%rbp), %rsi
	leaq	_ZL12utf8_offsets(%rip), %rcx
	movl	-104(%rbp), %edx
	subl	(%rcx,%rsi,4), %edx
	cmpb	$3, %al
	jle	.L1340
	testb	$1, -117(%rbp)
	jne	.L1340
.L1284:
	subq	$8, %rsp
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %r9
	movl	%r11d, -116(%rbp)
	movq	-144(%rbp), %rdi
	leaq	_ZZL17ucnv_SBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCodeE3nul(%rip), %r8
	movq	%r10, -104(%rbp)
	movq	%r8, -64(%rbp)
	movq	48(%rdi), %rsi
	pushq	-128(%rbp)
	movsbl	2(%r10), %eax
	pushq	%rax
	movslq	-84(%rbp), %rax
	pushq	$-1
	addq	%r14, %rax
	pushq	$0
	movq	%rdi, %r14
	pushq	%rax
	call	_ZL9_extFromUP10UConverterPK20UConverterSharedDataiPPKDsS5_PPhPKhPPiiaP10UErrorCode
	movq	-128(%rbp), %rdx
	addq	$48, %rsp
	movq	-104(%rbp), %r10
	movl	-116(%rbp), %r11d
	movl	(%rdx), %esi
	testl	%esi, %esi
	jle	.L1290
	movl	%eax, 84(%r14)
	movq	-72(%rbp), %r14
.L1291:
	movq	%rbx, 16(%r15)
	movq	%r14, 32(%r10)
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1339:
	leaq	1(%r14), %rdx
	subl	$1, %esi
	movq	%rdi, %rbx
	movq	%rdx, -72(%rbp)
	movb	%al, (%r14)
	movq	-72(%rbp), %r14
	cmpq	%rdi, -96(%rbp)
	jbe	.L1260
	testl	%esi, %esi
	jne	.L1289
.L1261:
	movq	-128(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1247:
	testl	%edi, %edi
	jle	.L1250
	movq	-96(%rbp), %rax
	movzbl	-1(%rax), %ecx
	testb	%cl, %cl
	jns	.L1250
	movl	$0, -116(%rbp)
	movl	%edi, %r9d
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	movl	$0, -104(%rbp)
	xorl	%eax, %eax
.L1301:
	cmpb	$-64, %cl
	jge	.L1249
	cmpl	$1, %r9d
	jle	.L1249
	movq	-96(%rbp), %rdx
	movzbl	-2(%rdx), %r9d
	leal	32(%r9), %esi
	cmpb	$15, %sil
	ja	.L1248
	andl	$15, %r9d
	leaq	.LC28(%rip), %rsi
	shrb	$5, %cl
	movsbl	(%rsi,%r9), %r9d
	sarl	%cl, %r9d
	movq	%rdx, %rcx
	subq	$2, %rcx
	andl	$1, %r9d
	movq	%rcx, %rsi
	cmove	%rdx, %rsi
	movq	%rsi, -96(%rbp)
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1254:
	cmpb	$-64, %cl
	setl	%cl
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1262:
	cmpb	$-33, %al
	ja	.L1266
	cmpb	$-63, %al
	jbe	.L1267
	movzbl	1(%rbx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L1268
	movl	%eax, %ecx
	leaq	2(%rbx), %rdi
	movzbl	%dl, %edx
	andl	$31, %eax
	andl	$31, %ecx
.L1342:
	movzwl	(%r9,%rax,2), %eax
	movq	-112(%rbp), %rbx
	addl	%edx, %eax
	cltq
	movzwl	(%rbx,%rax,2), %eax
	cmpw	%ax, %r11w
	jbe	.L1339
	movzbl	%cl, %ecx
	sall	$6, %ecx
	orl	%ecx, %edx
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1266:
	cmpb	$-32, %al
	jne	.L1267
	movzbl	1(%rbx), %eax
	leal	-128(%rax), %ecx
	addl	$96, %eax
	cmpb	$31, %al
	ja	.L1305
	movzbl	2(%rbx), %eax
	leal	-128(%rax), %edx
	cmpb	$63, %dl
	ja	.L1305
	leaq	3(%rbx), %rdi
	movzbl	%dl, %edx
	movzbl	%cl, %eax
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1249:
	addl	$62, %ecx
	cmpb	$46, %cl
	sbbq	$0, -96(%rbp)
	testb	%dil, %dil
	je	.L1250
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1340:
	movl	%edx, %eax
	movq	-136(%rbp), %rsi
	movq	%rbx, %rdi
	sarl	$10, %eax
	cltq
	movzwl	(%rsi,%rax,2), %ecx
	movl	%edx, %eax
	sarl	$4, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	movl	%edx, %ecx
	cltq
	andl	$15, %ecx
	movzwl	(%rsi,%rax,2), %eax
	movq	-112(%rbp), %rsi
	addl	%ecx, %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1252:
	movq	%r12, -160(%rbp)
	movl	-104(%rbp), %edx
	movq	%rbx, %rsi
	movl	%eax, %edi
	movw	%r11w, -150(%rbp)
	movb	%r8b, -151(%rbp)
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1273:
	sarl	$5, %ecx
	movl	$1, %r12d
	leaq	.LC28(%rip), %r8
	sall	%cl, %r12d
	movl	%r12d, %ecx
	movl	%edx, %r12d
	andl	$15, %r12d
	testb	%cl, (%r8,%r12)
	je	.L1332
	sall	$6, %edx
	addq	$1, %rsi
	addl	%r11d, %edx
.L1278:
	addl	$1, %edi
.L1279:
	movl	%edi, %eax
	movq	%rsi, %rbx
	cmpq	%rsi, %r9
	jbe	.L1336
	movzbl	(%rsi), %r11d
	movl	%r11d, %ecx
	cmpb	$1, %dil
	jle	.L1273
	cmpb	$-64, %r11b
	jge	.L1332
	leaq	1(%rsi), %rsi
	sall	$6, %edx
	leal	1(%rdi), %eax
	movq	%rsi, %rbx
	addl	%r11d, %edx
	cmpb	$2, %al
	jle	.L1278
	movl	%edx, -104(%rbp)
	movq	-160(%rbp), %r12
	movzwl	-150(%rbp), %r11d
	movzbl	-151(%rbp), %r8d
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1336:
	movsbl	%al, %ecx
	movzbl	-118(%rbp), %r8d
	subl	-148(%rbp), %ecx
	movl	%edx, -104(%rbp)
	movslq	%ecx, %rcx
	subq	%rcx, %rbx
	cmpb	%al, %r8b
	jge	.L1280
	movzbl	(%rbx), %edi
	movsbq	%r8b, %rsi
	leal	1(%r8), %ecx
	movb	%dil, 65(%r13,%rsi)
	cmpb	%al, %cl
	jge	.L1281
	movzbl	1(%rbx), %edi
	movsbq	%cl, %rcx
	leal	2(%r8), %esi
	movb	%dil, 65(%r13,%rcx)
	cmpb	%al, %sil
	jge	.L1281
	movzbl	2(%rbx), %edi
	movsbq	%sil, %rsi
	leal	3(%r8), %ecx
	movb	%dil, 65(%r13,%rsi)
	cmpb	%al, %cl
	jge	.L1281
	movzbl	3(%rbx), %edi
	movsbq	%cl, %rcx
	leal	4(%r8), %esi
	movb	%dil, 65(%r13,%rcx)
	cmpb	%al, %sil
	jge	.L1281
	movzbl	4(%rbx), %edi
	movsbq	%sil, %rsi
	leal	5(%r8), %ecx
	movb	%dil, 65(%r13,%rsi)
	cmpb	%cl, %al
	jle	.L1281
	movzbl	5(%rbx), %edi
	movsbq	%cl, %rcx
	leal	6(%r8), %esi
	movb	%dil, 65(%r13,%rcx)
	cmpb	%al, %sil
	jge	.L1281
	movzbl	6(%rbx), %ecx
	movsbq	%sil, %rsi
	movb	%cl, 65(%r13,%rsi)
.L1281:
	movzbl	-118(%rbp), %ecx
	notl	%ecx
	addl	%eax, %ecx
	movzbl	%cl, %ecx
	leaq	1(%rbx,%rcx), %rbx
.L1280:
	movb	%al, 64(%r13)
	movl	-104(%rbp), %edx
	movl	-116(%rbp), %eax
	movl	%edx, 72(%r13)
	movl	%eax, 76(%r13)
	movq	%rbx, 16(%r15)
	movq	%r14, 32(%r10)
.L1244:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1344
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1332:
	.cfi_restore_state
	movsbl	%al, %edx
	movl	%edx, -116(%rbp)
.L1277:
	movl	-116(%rbp), %esi
	movzbl	-118(%rbp), %edi
	subl	-148(%rbp), %esi
	movslq	%esi, %rsi
	subq	%rsi, %rbx
	cmpb	%dil, %al
	jle	.L1285
	movzbl	(%rbx), %esi
	movsbq	%dil, %rcx
	leal	1(%rdi), %edx
	movb	%sil, 65(%r13,%rcx)
	cmpb	%dl, %al
	jle	.L1286
	movzbl	1(%rbx), %esi
	movsbq	%dl, %rdx
	leal	2(%rdi), %ecx
	movb	%sil, 65(%r13,%rdx)
	cmpb	%cl, %al
	jle	.L1286
	movzbl	2(%rbx), %esi
	movsbq	%cl, %rcx
	leal	3(%rdi), %edx
	movb	%sil, 65(%r13,%rcx)
	cmpb	%dl, %al
	jle	.L1286
	movzbl	3(%rbx), %esi
	movsbq	%dl, %rdx
	leal	4(%rdi), %ecx
	movb	%sil, 65(%r13,%rdx)
	cmpb	%cl, %al
	jle	.L1286
	movzbl	4(%rbx), %esi
	movsbq	%cl, %rcx
	leal	5(%rdi), %edx
	movb	%sil, 65(%r13,%rcx)
	cmpb	%dl, %al
	jle	.L1286
	movzbl	5(%rbx), %esi
	movsbq	%dl, %rdx
	leal	6(%rdi), %ecx
	movb	%sil, 65(%r13,%rdx)
	cmpb	%cl, %al
	jle	.L1286
	movzbl	6(%rbx), %edx
	movsbq	%cl, %rcx
	movb	%dl, 65(%r13,%rcx)
.L1286:
	movzbl	-118(%rbp), %edx
	notl	%edx
	addl	%eax, %edx
	movzbl	%dl, %edx
	leaq	1(%rbx,%rdx), %rbx
.L1285:
	movb	%al, 64(%r13)
	movq	-128(%rbp), %rax
	movq	%rbx, 16(%r15)
	movq	%r14, 32(%r10)
	movl	$12, (%rax)
	jmp	.L1244
.L1306:
	movq	%rdi, %rbx
	jmp	.L1284
.L1305:
	movl	$224, -104(%rbp)
	movl	$3, %r8d
	.p2align 4,,10
	.p2align 3
.L1299:
	movl	$1, -116(%rbp)
	movq	%rdi, %rbx
	movl	$1, %eax
	movl	$0, -148(%rbp)
	movb	$0, -118(%rbp)
	jmp	.L1272
.L1267:
	movzbl	%al, %ebx
	leal	62(%rax), %ecx
	movl	%ebx, -104(%rbp)
	cmpb	$50, %cl
	jbe	.L1298
	xorl	%r8d, %r8d
	jmp	.L1299
.L1260:
	movq	-128(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1291
	movq	-144(%rbp), %rax
	movl	208(%rax), %eax
	testl	%eax, %eax
	jns	.L1291
.L1293:
	movq	24(%r15), %rdi
	cmpq	%rbx, %rdi
	jbe	.L1291
	movzbl	(%rbx), %ecx
	leaq	1(%rbx), %r11
	movl	$1, %r9d
	movb	%cl, 65(%r13)
	testb	%cl, %cl
	js	.L1345
.L1294:
	cmpq	%r11, %rdi
	jbe	.L1309
	movl	$1, %r8d
	movq	%r11, %rax
	subl	%r11d, %r8d
	.p2align 4,,10
	.p2align 3
.L1296:
	leal	(%r8,%rax), %esi
	movzbl	(%rax), %edx
	addq	$1, %rax
	sall	$6, %ecx
	movsbq	%sil, %rsi
	movb	%dl, 65(%r13,%rsi)
	addl	%edx, %ecx
	cmpq	%rax, %rdi
	jne	.L1296
	movq	%rbx, %rax
	leaq	-1(%rdi), %rbx
	subq	%rax, %rbx
	subl	%eax, %edi
	addq	%r11, %rbx
.L1295:
	movl	%ecx, 72(%r13)
	movb	%dil, 64(%r13)
	movl	%r9d, 76(%r13)
	jmp	.L1291
.L1290:
	movq	-144(%rbp), %rax
	movq	-72(%rbp), %r14
	movl	208(%rax), %ecx
	testl	%ecx, %ecx
	js	.L1292
	movl	$-127, (%rdx)
	jmp	.L1291
.L1292:
	movl	40(%r10), %eax
	subl	%r14d, %eax
	movl	%eax, -84(%rbp)
	cmpq	%rbx, -96(%rbp)
	ja	.L1287
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1268:
	movl	%eax, -104(%rbp)
	movl	$2, %r8d
	jmp	.L1299
.L1345:
	leal	62(%rcx), %edx
	xorl	%r9d, %r9d
	cmpb	$50, %dl
	ja	.L1294
	cmpb	$-33, %cl
	seta	%r9b
	cmpb	$-17, %cl
	seta	%al
	leal	2(%r9,%rax), %r9d
	movzbl	%r9b, %r9d
	jmp	.L1294
.L1309:
	movq	%r11, %rbx
	movl	$1, %edi
	jmp	.L1295
.L1344:
	call	__stack_chk_fail@PLT
.L1298:
	cmpb	$-33, %al
	seta	%r8b
	cmpb	$-17, %al
	seta	%al
	leal	2(%r8,%rax), %r8d
	jmp	.L1299
	.cfi_endproc
.LFE2836:
	.size	_ZL17ucnv_SBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCode, .-_ZL17ucnv_SBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCode
	.p2align 4
	.type	_ZL21ucnv_MBCSGetNextUCharP23UConverterToUnicodeArgsP10UErrorCode, @function
_ZL21ucnv_MBCSGetNextUCharP23UConverterToUnicodeArgsP10UErrorCode:
.LFB2829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r11
	movq	%rsi, -64(%rbp)
	cmpb	$0, 282(%r11)
	jg	.L1399
	movq	%rdi, %rbx
	movq	48(%r11), %rdi
	testb	$2, 253(%rdi)
	jne	.L1399
	movl	56(%r11), %eax
	movq	56(%rdi), %rcx
	movq	64(%rdi), %r13
	movq	16(%rbx), %rdx
	andl	$16, %eax
	movq	24(%rbx), %r8
	testl	%eax, %eax
	cmove	%rcx, %r13
	cmpb	$1, 48(%rdi)
	je	.L1356
	movq	72(%rdi), %rax
	movq	%rax, -72(%rbp)
	movl	72(%r11), %eax
	movl	%eax, -56(%rbp)
	movl	76(%r11), %eax
	movl	%eax, %r14d
	testb	%al, %al
	je	.L1446
.L1358:
	cmpq	%rdx, %r8
	jbe	.L1447
	movq	%rdx, %r15
	movl	$-1, %r9d
.L1359:
	movzbl	%r14b, %eax
	movzbl	(%rdx), %ecx
	leaq	1(%rdx), %r10
	salq	$10, %rax
	addq	%r13, %rax
	movl	(%rax,%rcx,4), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	movl	%ecx, %esi
	testl	%eax, %eax
	js	.L1363
	cmpq	%r10, %r8
	ja	.L1435
.L1402:
	movzbl	%sil, %eax
.L1362:
	movl	%eax, %r12d
	testl	%r9d, %r9d
	jns	.L1365
	movq	-64(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L1361
	cmpq	%r10, %r8
	jne	.L1360
	cmpq	%r10, %r15
	jnb	.L1360
	leaq	16(%r15), %rsi
	leaq	65(%r11), %rdx
	movq	%r10, %rcx
	subq	%r15, %rcx
	cmpq	%rsi, %rdx
	leaq	81(%r11), %rsi
	setnb	%dil
	cmpq	%rsi, %r15
	movb	%cl, 64(%r11)
	leaq	1(%r15), %rax
	setnb	%sil
	orb	%sil, %dil
	je	.L1425
	leaq	-1(%r10), %rsi
	subq	%r15, %rsi
	cmpq	$14, %rsi
	seta	%dil
	cmpq	%rax, %r10
	setnb	%sil
	testb	%sil, %dil
	je	.L1425
	cmpq	%rax, %r10
	movl	$1, %eax
	cmovb	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rsi
	andq	$-16, %rsi
	.p2align 4,,10
	.p2align 3
.L1387:
	movdqu	(%r15,%rax), %xmm0
	movups	%xmm0, 65(%r11,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1387
	movq	%rcx, %rsi
	andq	$-16, %rsi
	leaq	(%r15,%rsi), %rax
	addq	%rsi, %rdx
	cmpq	%rcx, %rsi
	je	.L1389
	movzbl	(%rax), %ecx
	movb	%cl, (%rdx)
	leaq	1(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	1(%rax), %ecx
	movb	%cl, 1(%rdx)
	leaq	2(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	2(%rax), %ecx
	movb	%cl, 2(%rdx)
	leaq	3(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	3(%rax), %ecx
	movb	%cl, 3(%rdx)
	leaq	4(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	4(%rax), %ecx
	movb	%cl, 4(%rdx)
	leaq	5(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	5(%rax), %ecx
	movb	%cl, 5(%rdx)
	leaq	6(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	6(%rax), %ecx
	movb	%cl, 6(%rdx)
	leaq	7(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	7(%rax), %ecx
	movb	%cl, 7(%rdx)
	leaq	8(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	8(%rax), %ecx
	movb	%cl, 8(%rdx)
	leaq	9(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	9(%rax), %ecx
	movb	%cl, 9(%rdx)
	leaq	10(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	10(%rax), %ecx
	movb	%cl, 10(%rdx)
	leaq	11(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	11(%rax), %ecx
	movb	%cl, 11(%rdx)
	leaq	12(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	12(%rax), %ecx
	movb	%cl, 12(%rdx)
	leaq	13(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	13(%rax), %ecx
	movb	%cl, 13(%rdx)
	leaq	14(%rax), %rcx
	cmpq	%rcx, %r10
	jbe	.L1389
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdx)
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	-64(%rbp), %rax
	movl	$65535, %r9d
	movl	$11, (%rax)
	.p2align 4,,10
	.p2align 3
.L1365:
	movl	$0, 72(%r11)
	movl	%r12d, 76(%r11)
	movq	%r10, 16(%rbx)
.L1346:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1363:
	.cfi_restore_state
	movl	%eax, %ecx
	movzbl	%r14b, %r12d
	andl	$127, %esi
	shrl	$20, %ecx
	movl	%r12d, 76(%r11)
	movl	%ecx, %edx
	andl	$15, %edx
	je	.L1444
	cmpb	$4, %dl
	je	.L1448
	cmpb	$5, %dl
	je	.L1449
	andl	$13, %ecx
	cmpb	$1, %cl
	je	.L1450
	cmpb	$8, %dl
	je	.L1451
	cmpb	$2, %dl
	je	.L1444
	cmpb	$6, %dl
	je	.L1381
	cmpb	$7, %dl
	jne	.L1384
	movq	-64(%rbp), %rax
	movzbl	%sil, %r12d
	movl	$12, (%rax)
.L1385:
	cmpl	$-1, %r9d
	jne	.L1365
.L1361:
	movzbl	(%r15), %eax
	leaq	1(%r15), %rcx
	movzbl	49(%rdi), %edx
	movb	%al, 65(%r11)
	cmpq	%r10, %rcx
	je	.L1452
	jnb	.L1404
	testb	%dl, %dl
	movzbl	%sil, %r8d
	movq	%rbx, -64(%rbp)
	leaq	66(%r11), %r15
	setne	-56(%rbp)
	movzbl	-56(%rbp), %eax
	salq	$10, %r8
	movq	%r13, %rbx
	movq	%r11, -56(%rbp)
	movl	$1, %r14d
	movq	%r10, %r11
	movq	%r8, %r13
	movl	%eax, %r10d
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1392:
	shrl	$20, %esi
	andl	$15, %esi
	cmpb	$8, %sil
	jne	.L1405
	testb	%r10b, %r10b
	je	.L1405
.L1394:
	addq	$1, %rcx
	addq	$1, %r15
	addl	$1, %r14d
	movb	%r9b, -1(%r15)
	cmpq	%r11, %rcx
	je	.L1443
.L1396:
	movzbl	(%rcx), %eax
	movq	%rax, %r9
	leaq	(%rbx,%rax,4), %rax
	movl	(%rax,%r13), %esi
	testl	%esi, %esi
	js	.L1392
	shrl	$24, %esi
	movq	%rbx, %rdi
	call	_ZL18hasValidTrailBytesPA256_Kih
.L1393:
	testb	%al, %al
	je	.L1394
.L1443:
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %rbx
.L1391:
	movb	%r14b, 64(%r11)
	movq	%rcx, %r10
	movl	$65535, %r9d
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	%r10, %r15
	cmpq	%r10, %r8
	jbe	.L1402
	movl	$0, -56(%rbp)
.L1397:
	movl	%esi, %r14d
	movq	%r10, %rdx
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1435:
	andl	$16777215, %eax
	salq	$10, %rcx
	addl	%eax, -56(%rbp)
	movzbl	(%r10), %eax
	addq	%r13, %rcx
	movl	(%rcx,%rax,4), %r12d
	testl	%r12d, %r12d
	jns	.L1397
	movl	%r12d, %eax
	shrl	$20, %eax
	andl	$15, %eax
	cmpl	$4, %eax
	jne	.L1397
	movl	-56(%rbp), %ecx
	movzwl	%r12w, %eax
	addl	%ecx, %eax
	movq	-72(%rbp), %rcx
	movzwl	(%rcx,%rax,2), %r9d
	cmpl	$65533, %r9d
	jg	.L1397
	shrl	$24, %r12d
	leaq	2(%rdx), %r10
	andl	$127, %r12d
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1446:
	movzbl	49(%rdi), %r14d
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1454:
	movq	%rsi, %rdx
.L1356:
	cmpq	%rdx, %r8
	jbe	.L1355
	movzbl	(%rdx), %eax
	leaq	1(%rdx), %rsi
	movl	0(%r13,%rax,4), %r9d
	movq	%rsi, 16(%rbx)
	cmpl	$-2146435072, %r9d
	jl	.L1445
	movl	%r9d, %eax
	shrl	$20, %eax
	movl	%eax, %ecx
	andl	$13, %eax
	andl	$15, %ecx
	cmpb	$1, %al
	je	.L1453
	cmpb	$2, %cl
	je	.L1445
	cmpb	$6, %cl
	je	.L1354
	cmpb	$7, %cl
	jne	.L1454
.L1355:
	movq	-64(%rbp), %rax
	movl	$65535, %r9d
	movl	$8, (%rax)
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1455:
	addq	$1, %rax
.L1425:
	movzbl	-1(%rax), %ecx
	addq	$1, %rdx
	movb	%cl, -1(%rdx)
	cmpq	%rax, %r10
	ja	.L1455
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1405:
	cmpb	$7, %sil
	setne	%al
	jmp	.L1393
.L1447:
	movq	-64(%rbp), %rax
	movzbl	%r14b, %r12d
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L1456
	movq	%rdx, %r10
.L1360:
	movq	-64(%rbp), %rax
	movl	$65535, %r9d
	movl	$8, (%rax)
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1444:
	movzwl	%ax, %r9d
	movzbl	%sil, %r12d
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1452:
	movb	$1, 64(%r11)
	movl	$65535, %r9d
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	-64(%rbp), %rax
	movzbl	%sil, %r12d
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L1385
.L1370:
	movl	$0, 72(%r11)
	movl	$-9, %r9d
	movl	%r12d, 76(%r11)
	movq	%r15, 16(%rbx)
	jmp	.L1346
.L1399:
	movl	$-9, %r9d
	jmp	.L1346
.L1448:
	movq	-72(%rbp), %rdx
	movzwl	%ax, %eax
	addl	-56(%rbp), %eax
	movzbl	%sil, %r12d
	movl	%eax, -76(%rbp)
	movzwl	(%rdx,%rax,2), %r9d
	cmpl	$65533, %r9d
	jle	.L1365
	cmpl	$65534, %r9d
	je	.L1457
	movq	-64(%rbp), %rax
	movl	$65535, %r9d
	movl	$12, (%rax)
	jmp	.L1365
.L1449:
	movl	-56(%rbp), %edx
	movq	-72(%rbp), %rdi
	movzwl	%ax, %eax
	movzbl	%sil, %r12d
	addl	%edx, %eax
	movzwl	(%rdi,%rax,2), %r9d
	movq	%rax, %rdx
	movl	%r9d, %eax
	cmpl	$55295, %r9d
	jle	.L1365
	addl	$1, %edx
	cmpl	$57343, %r9d
	jg	.L1377
	movl	%edx, %r15d
	sall	$10, %eax
	movzwl	(%rdi,%r15,2), %edx
	andl	$1047552, %eax
	leal	9216(%rdx,%rax), %r9d
	jmp	.L1365
.L1450:
	andl	$1048575, %eax
	movzbl	%sil, %r12d
	leal	65536(%rax), %r9d
	jmp	.L1365
.L1451:
	cmpb	$0, 49(%rdi)
	je	.L1381
	movq	-64(%rbp), %rax
	movl	%r14d, %esi
	movl	$12, (%rax)
	jmp	.L1385
.L1445:
	movzwl	%r9w, %r9d
	jmp	.L1346
.L1453:
	andl	$1048575, %r9d
	addl	$65536, %r9d
	jmp	.L1346
.L1354:
	movq	-64(%rbp), %rax
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L1355
	movq	%rdx, 16(%rbx)
	movl	$-9, %r9d
	jmp	.L1346
.L1404:
	movl	$1, %r14d
	jmp	.L1391
.L1377:
	andl	$-2, %eax
	cmpw	$-8192, %ax
	je	.L1458
	cmpl	$65535, %r9d
	je	.L1459
.L1375:
	movq	-64(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1365
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1457:
	movl	52(%rdi), %r14d
	testl	%r14d, %r14d
	je	.L1375
	movq	80(%rdi), %rdx
	movq	%rdi, -88(%rbp)
	xorl	%ecx, %ecx
	movb	%sil, -77(%rbp)
	movq	%rdx, -72(%rbp)
	leal	-1(%r14), %edx
	movl	%edx, -56(%rbp)
	movl	%r12d, -92(%rbp)
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1461:
	leal	(%r14,%rcx), %edx
	movq	-72(%rbp), %rax
	movl	-76(%rbp), %esi
	shrl	%edx
	movl	%edx, %edi
	cmpl	(%rax,%rdi,8), %esi
	jnb	.L1460
	leal	-1(%rdx), %eax
	movl	%edx, %r14d
	movl	%eax, -56(%rbp)
.L1372:
	cmpl	%ecx, -56(%rbp)
	ja	.L1461
	movl	%ecx, %edx
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rdi
	movzbl	-77(%rbp), %esi
	movl	-92(%rbp), %eax
	leaq	(%rcx,%rdx,8), %rdx
	movl	-76(%rbp), %ecx
	cmpl	(%rdx), %ecx
	jne	.L1375
	movl	4(%rdx), %r9d
	cmpl	$65534, %r9d
	jne	.L1362
	jmp	.L1375
.L1460:
	movl	%edx, %ecx
	jmp	.L1372
.L1458:
	movl	%edx, %r15d
	movzwl	(%rdi,%r15,2), %r9d
	jmp	.L1365
.L1459:
	movq	-64(%rbp), %rax
	movl	$12, (%rax)
	jmp	.L1365
.L1456:
	movq	%rdx, %r15
	movl	%r14d, %esi
	movq	%rdx, %r10
	jmp	.L1361
	.cfi_endproc
.LFE2829:
	.size	_ZL21ucnv_MBCSGetNextUCharP23UConverterToUnicodeArgsP10UErrorCode, .-_ZL21ucnv_MBCSGetNextUCharP23UConverterToUnicodeArgsP10UErrorCode
	.p2align 4
	.globl	ucnv_MBCSGetFilteredUnicodeSetForUnicode_67
	.type	ucnv_MBCSGetFilteredUnicodeSetForUnicode_67, @function
ucnv_MBCSGetFilteredUnicodeSetForUnicode_67:
.LFB2810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	253(%rdi), %eax
	movq	88(%rdi), %rsi
	movq	%rdi, -128(%rbp)
	movl	%ecx, -92(%rbp)
	movq	%rdi, %rcx
	movb	%al, -56(%rbp)
	andl	$1, %eax
	cmpb	$1, %al
	movl	%edx, -96(%rbp)
	sbbl	%edi, %edi
	movq	%r8, -120(%rbp)
	andl	$-512, %edi
	movq	%rsi, -104(%rbp)
	addl	$544, %edi
	cmpb	$1, %al
	sbbl	%eax, %eax
	movl	%edi, -112(%rbp)
	movl	%eax, %edi
	movzbl	252(%rcx), %eax
	movq	232(%rcx), %rcx
	andw	$-1024, %di
	addw	$1088, %di
	movq	%rcx, -64(%rbp)
	movw	%di, -80(%rbp)
	testb	%al, %al
	jne	.L1464
	cmpl	$1, %edx
	movzwl	%di, %eax
	movq	%rsi, -72(%rbp)
	sbbl	%r13d, %r13d
	leaq	(%rsi,%rax,2), %rax
	xorl	%r14d, %r14d
	movq	%rax, -88(%rbp)
	andw	$1792, %r13w
	leaq	128(%rsi), %rax
	addw	$2048, %r13w
	movq	%rax, -112(%rbp)
	movq	%r15, %rax
	movl	%r13d, %r15d
	movq	%rax, %r13
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1466:
	addl	$1024, %r14d
.L1471:
	addq	$2, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -88(%rbp)
	je	.L1561
.L1473:
	movq	-72(%rbp), %rax
	movzwl	(%rax), %edx
	cmpw	%dx, -80(%rbp)
	jnb	.L1466
	movq	-104(%rbp), %rax
	movzwl	%dx, %ebx
	addq	%rbx, %rbx
	leaq	(%rax,%rbx), %r12
	addq	-112(%rbp), %rbx
	movq	%rbx, -56(%rbp)
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1467:
	addl	$16, %r14d
.L1470:
	addq	$2, %r12
	cmpq	-56(%rbp), %r12
	je	.L1471
.L1472:
	movzwl	(%r12), %eax
	testw	%ax, %ax
	je	.L1467
	movq	-64(%rbp), %rdx
	leaq	(%rdx,%rax,2), %rbx
	.p2align 4,,10
	.p2align 3
.L1469:
	addq	$2, %rbx
	cmpw	%r15w, -2(%rbx)
	jb	.L1468
	movq	0(%r13), %rdi
	movl	%r14d, %esi
	call	*8(%r13)
.L1468:
	addl	$1, %r14d
	testb	$15, %r14b
	jne	.L1469
	jmp	.L1470
.L1464:
	movl	$4, -88(%rbp)
	cmpb	$3, %al
	je	.L1474
	movl	$3, -88(%rbp)
	cmpb	$9, %al
	je	.L1474
	cmpb	$2, %al
	sete	%al
	movzbl	%al, %eax
	addl	$2, %eax
	movl	%eax, -88(%rbp)
.L1474:
	movzwl	-80(%rbp), %r14d
	movq	-104(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%r15, %r13
	leaq	(%rax,%r14,2), %rcx
	movq	%rax, -56(%rbp)
	movq	%rcx, -80(%rbp)
.L1512:
	movq	-56(%rbp), %rax
	movzwl	(%rax), %ecx
	movq	%rcx, %rax
	cmpl	-112(%rbp), %ecx
	jg	.L1564
	addl	$1024, %ebx
.L1509:
	addq	$2, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L1512
.L1561:
	movq	-120(%rbp), %r8
	movl	-92(%rbp), %ecx
	movq	%r13, %rsi
	movl	-96(%rbp), %edx
	movq	-128(%rbp), %rdi
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ucnv_extGetUnicodeSet_67@PLT
.L1564:
	.cfi_restore_state
	movq	-104(%rbp), %rdx
	salq	$2, %rax
	movq	%r13, %r15
	leaq	(%rdx,%rax), %r12
	leaq	256(%rdx,%rax), %rax
	movq	%rax, -72(%rbp)
	movq	%r12, %rax
	movl	%ebx, %r12d
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1510:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L1476
	movzwl	%ax, %ecx
	imull	-88(%rbp), %ecx
	movq	-64(%rbp), %rdx
	shrl	$16, %eax
	sall	$4, %ecx
	leaq	(%rdx,%rcx), %r14
	movl	-92(%rbp), %ecx
	cmpl	$5, %ecx
	ja	.L1477
	movl	%ecx, %esi
	leaq	.L1479(%rip), %rcx
	movslq	(%rcx,%rsi,4), %rsi
	addq	%rcx, %rsi
	cmpl	$1, -96(%rbp)
	notrack jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L1479:
	.long	.L1484-.L1479
	.long	.L1483-.L1479
	.long	.L1482-.L1479
	.long	.L1481-.L1479
	.long	.L1480-.L1479
	.long	.L1478-.L1479
	.text
.L1480:
	sete	%r13b
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movl	%r13d, %r15d
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L1504:
	testb	$1, %r13b
	jne	.L1523
	testb	%r15b, %r15b
	je	.L1502
.L1523:
	movzwl	(%r14), %eax
	leal	24159(%rax), %edx
	cmpw	$23901, %dx
	ja	.L1502
	addl	$95, %eax
	cmpb	$93, %al
	ja	.L1502
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	*8(%rbx)
	.p2align 4,,10
	.p2align 3
.L1502:
	addl	$1, %r12d
	shrl	%r13d
	addq	$2, %r14
	testb	$15, %r12b
	jne	.L1504
.L1562:
	movq	%rbx, %r15
.L1563:
	movq	-136(%rbp), %rbx
.L1488:
	addq	$4, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L1510
	movl	%r12d, %ebx
	movq	%r15, %r13
	jmp	.L1509
.L1484:
	movl	%eax, %r13d
	je	.L1559
	.p2align 4,,10
	.p2align 3
.L1487:
	testb	$1, %r13b
	je	.L1486
	movq	(%r15), %rdi
	movl	%r12d, %esi
	call	*8(%r15)
.L1486:
	addl	$1, %r12d
	shrl	%r13d
	testb	$15, %r12b
	jne	.L1487
	jmp	.L1488
.L1482:
	sete	%r13b
	movq	%rbx, -136(%rbp)
	movl	%r13d, %ebx
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L1498:
	testb	$1, %r13b
	jne	.L1521
	testb	%bl, %bl
	je	.L1496
.L1521:
	movzbl	(%r14), %eax
	subl	$129, %eax
	cmpl	$1, %eax
	ja	.L1496
	movq	(%r15), %rdi
	movl	%r12d, %esi
	call	*8(%r15)
.L1496:
	addl	$1, %r12d
	shrl	%r13d
	addq	$3, %r14
	testb	$15, %r12b
	jne	.L1498
	jmp	.L1563
.L1478:
	sete	%r13b
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movl	%r13d, %r15d
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L1507:
	testb	$1, %r13b
	jne	.L1524
	testb	%r15b, %r15b
	je	.L1505
.L1524:
	movzwl	(%r14), %eax
	leal	24159(%rax), %edx
	cmpw	$23645, %dx
	ja	.L1505
	addl	$95, %eax
	cmpb	$93, %al
	ja	.L1505
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	*8(%rbx)
	.p2align 4,,10
	.p2align 3
.L1505:
	addl	$1, %r12d
	shrl	%r13d
	addq	$2, %r14
	testb	$15, %r12b
	jne	.L1507
	movq	%rbx, %r15
	jmp	.L1563
.L1483:
	sete	%r13b
	movq	%rbx, -136(%rbp)
	movl	%r13d, %ebx
	movl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L1495:
	testb	$1, %r13b
	jne	.L1520
	testb	%bl, %bl
	je	.L1493
.L1520:
	cmpw	$255, (%r14)
	jbe	.L1493
	movq	(%r15), %rdi
	movl	%r12d, %esi
	call	*8(%r15)
.L1493:
	addl	$1, %r12d
	shrl	%r13d
	addq	$2, %r14
	testb	$15, %r12b
	jne	.L1495
	jmp	.L1563
.L1481:
	sete	%r13b
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movl	%r13d, %r15d
	movl	%eax, %r13d
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1499:
	addl	$1, %r12d
	shrl	%r13d
	addq	$2, %r14
	testb	$15, %r12b
	je	.L1562
.L1501:
	testb	$1, %r13b
	jne	.L1522
	testb	%r15b, %r15b
	je	.L1499
.L1522:
	movzwl	(%r14), %eax
	subl	$33088, %eax
	cmpl	$28348, %eax
	ja	.L1499
	movq	(%rbx), %rdi
	movl	%r12d, %esi
	call	*8(%rbx)
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1476:
	addl	$16, %r12d
	jmp	.L1488
.L1477:
	movq	-120(%rbp), %rax
	movl	$5, (%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1559:
	.cfi_restore_state
	movq	%rbx, -136(%rbp)
	movl	-88(%rbp), %ebx
	jmp	.L1485
.L1489:
	movzbl	(%r14), %ecx
	movzbl	1(%r14), %edx
	cmpl	$3, %ebx
	je	.L1518
	xorl	%esi, %esi
	cmpl	$4, %ebx
	jne	.L1492
	orl	%edx, %ecx
	movzbl	2(%r14), %edx
	leaq	1(%r14), %rdi
.L1491:
	movl	%ecx, %esi
	movl	%edx, %ecx
	movzbl	2(%rdi), %edx
	leaq	1(%rdi), %r14
.L1492:
	orl	%esi, %ecx
	addq	$2, %r14
	orb	%dl, %cl
	je	.L1490
	movq	(%r15), %rdi
	movl	%r12d, %esi
	call	*8(%r15)
.L1490:
	addl	$1, %r12d
	shrl	%r13d
	testb	$15, %r12b
	je	.L1563
.L1485:
	testb	$1, %r13b
	je	.L1489
	movq	(%r15), %rdi
	movl	%r12d, %esi
	call	*8(%r15)
	movl	%ebx, %edx
	addq	%rdx, %r14
	jmp	.L1490
.L1518:
	movq	%r14, %rdi
	jmp	.L1491
	.cfi_endproc
.LFE2810:
	.size	ucnv_MBCSGetFilteredUnicodeSetForUnicode_67, .-ucnv_MBCSGetFilteredUnicodeSetForUnicode_67
	.p2align 4
	.type	_ZL22ucnv_MBCSGetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode, @function
_ZL22ucnv_MBCSGetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode:
.LFB2812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	testb	$-128, 57(%rdi)
	je	.L1566
	movq	(%r12), %rdi
	movl	$55295, %edx
	xorl	%esi, %esi
	call	*16(%r12)
	movq	16(%r12), %rax
	movq	(%r12), %rdi
	addq	$8, %rsp
	popq	%r12
	movl	$1114111, %edx
	movl	$57344, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1566:
	.cfi_restore_state
	movq	48(%rdi), %rdi
	xorl	%r9d, %r9d
	movq	%rcx, %r8
	cmpb	$-37, 252(%rdi)
	sete	%r9b
	addq	$8, %rsp
	popq	%r12
	movl	%r9d, %ecx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ucnv_MBCSGetFilteredUnicodeSetForUnicode_67
	.cfi_endproc
.LFE2812:
	.size	_ZL22ucnv_MBCSGetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode, .-_ZL22ucnv_MBCSGetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode
	.p2align 4
	.globl	ucnv_MBCSGetUnicodeSetForUnicode_67
	.type	ucnv_MBCSGetUnicodeSetForUnicode_67, @function
ucnv_MBCSGetUnicodeSetForUnicode_67:
.LFB2811:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	cmpb	$-37, 252(%rdi)
	sete	%cl
	jmp	ucnv_MBCSGetFilteredUnicodeSetForUnicode_67
	.cfi_endproc
.LFE2811:
	.size	ucnv_MBCSGetUnicodeSetForUnicode_67, .-ucnv_MBCSGetUnicodeSetForUnicode_67
	.p2align 4
	.globl	ucnv_MBCSSimpleGetNextUChar_67
	.type	ucnv_MBCSSimpleGetNextUChar_67, @function
ucnv_MBCSSimpleGetNextUChar_67:
.LFB2830:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L1589
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rdx), %r11d
	movl	$1, %r9d
	xorl	%r10d, %r10d
	addq	$2, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	56(%rdi), %rbx
	movzbl	49(%rdi), %r8d
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1607:
	movl	%eax, %r8d
	addq	$1, %r9
	andl	$16777215, %eax
	shrl	$24, %r8d
	addl	%eax, %r10d
	cmpq	%r9, %r11
	je	.L1596
.L1572:
	movzbl	%r8b, %r8d
	movzbl	-1(%rsi,%r9), %eax
	movl	%r9d, %r12d
	salq	$10, %r8
	addq	%rbx, %r8
	movl	(%r8,%rax,4), %eax
	testl	%eax, %eax
	jns	.L1607
	movl	%eax, %r9d
	movq	72(%rdi), %r8
	shrl	$20, %r9d
	andl	$15, %r9d
	cmpb	$4, %r9b
	je	.L1608
	testb	%r9b, %r9b
	jne	.L1582
.L1603:
	movzwl	%ax, %eax
.L1581:
	cmpl	%r12d, %edx
	jne	.L1596
	cmpl	$65534, %eax
	jne	.L1569
.L1576:
	movq	288(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1597
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movsbl	%cl, %ecx
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ucnv_extSimpleMatchToU_67@PLT
	.p2align 4,,10
	.p2align 3
.L1582:
	.cfi_restore_state
	cmpb	$5, %r9b
	je	.L1609
	cmpb	$1, %r9b
	je	.L1604
	cmpb	$2, %r9b
	je	.L1603
	cmpb	$3, %r9b
	je	.L1604
	cmpl	%r12d, %edx
	jne	.L1596
	cmpb	$6, %r9b
	je	.L1576
	.p2align 4,,10
	.p2align 3
.L1596:
	movl	$65535, %eax
.L1569:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1589:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$65535, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1608:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movzwl	%ax, %eax
	addl	%r10d, %eax
	movq	%rax, %r10
	movzwl	(%r8,%rax,2), %eax
	cmpl	$65534, %eax
	je	.L1610
.L1574:
	cmpl	%r12d, %edx
	movl	$65535, %edx
	popq	%rbx
	popq	%r12
	cmovne	%edx, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1610:
	.cfi_restore_state
	movl	52(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L1606
	movq	80(%rdi), %r13
	xorl	%r9d, %r9d
	leal	-1(%r8), %r11d
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1612:
	leal	(%r8,%r9), %eax
	shrl	%eax
	movl	%eax, %ebx
	cmpl	0(%r13,%rbx,8), %r10d
	jnb	.L1611
	movl	%eax, %r8d
	leal	-1(%rax), %r11d
.L1578:
	cmpl	%r9d, %r11d
	ja	.L1612
	leaq	0(%r13,%r9,8), %rax
	cmpl	(%rax), %r10d
	je	.L1580
.L1606:
	cmpl	%r12d, %edx
	je	.L1576
	jmp	.L1596
.L1609:
	movzwl	%ax, %eax
	addl	%r10d, %eax
	movq	%rax, %r10
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %r9d
	cmpl	$55295, %eax
	jle	.L1581
	addl	$1, %r10d
	cmpl	$57343, %eax
	jg	.L1584
	movzwl	(%r8,%r10,2), %r8d
	sall	$10, %eax
	andl	$1047552, %eax
	leal	9216(%r8,%rax), %eax
	jmp	.L1581
.L1604:
	andl	$1048575, %eax
	addl	$65536, %eax
	jmp	.L1574
.L1611:
	movl	%eax, %r9d
	jmp	.L1578
.L1597:
	movl	$65534, %eax
	jmp	.L1569
.L1584:
	andl	$-2, %r9d
	cmpw	$-8192, %r9w
	je	.L1613
	cmpl	%r12d, %edx
	jne	.L1596
	cmpl	$65535, %eax
	jne	.L1576
	jmp	.L1596
.L1580:
	movl	4(%rax), %eax
	jmp	.L1581
.L1613:
	movzwl	(%r8,%r10,2), %eax
	jmp	.L1581
	.cfi_endproc
.LFE2830:
	.size	ucnv_MBCSSimpleGetNextUChar_67, .-ucnv_MBCSSimpleGetNextUChar_67
	.p2align 4
	.globl	ucnv_MBCSFromUChar32_67
	.type	ucnv_MBCSFromUChar32_67, @function
ucnv_MBCSFromUChar32_67:
.LFB2835:
	.cfi_startproc
	endbr64
	cmpl	$65535, %esi
	jle	.L1615
	testb	$1, 253(%rdi)
	je	.L1616
.L1615:
	movl	%esi, %eax
	movq	88(%rdi), %r9
	movzbl	252(%rdi), %r10d
	sarl	$10, %eax
	cltq
	movzwl	(%r9,%rax,2), %r8d
	movl	%esi, %eax
	sarl	$4, %eax
	andl	$63, %eax
	addl	%r8d, %eax
	cltq
	testb	%r10b, %r10b
	je	.L1645
	movl	(%r9,%rax,4), %r9d
	cmpb	$1, %r10b
	jne	.L1625
	movzwl	%r9w, %eax
	movl	%esi, %r8d
	movq	232(%rdi), %r10
	andl	$15, %r8d
	sall	$4, %eax
	addl	%r8d, %eax
	movzwl	(%r10,%rax,2), %r10d
	xorl	%eax, %eax
	cmpl	$255, %r10d
	seta	%al
	addl	$16, %r8d
	addl	$1, %eax
	btl	%r8d, %r9d
	jc	.L1622
	testb	%cl, %cl
	je	.L1646
.L1623:
	testl	%r10d, %r10d
	je	.L1616
.L1622:
	movl	%r10d, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L1646:
	leal	-57344(%rsi), %r8d
	cmpl	$6399, %r8d
	jbe	.L1623
	leal	-983040(%rsi), %r8d
	cmpl	$131071, %r8d
	jbe	.L1623
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	288(%rdi), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.L1647
	ret
	.p2align 4,,10
	.p2align 3
.L1645:
	movzwl	(%r9,%rax,2), %eax
	movl	%esi, %r8d
	andl	$15, %r8d
	addl	%r8d, %eax
	movq	232(%rdi), %r8
	cltq
	movzwl	(%r8,%rax,2), %r8d
	movl	%r8d, %eax
	testb	%cl, %cl
	jne	.L1648
	cmpl	$3071, %r8d
	seta	%r8b
.L1619:
	testb	%r8b, %r8b
	je	.L1616
	movzbl	%al, %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1647:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%cl, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ucnv_extSimpleMatchFromU_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cltd
	xorl	%edx, %eax
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1648:
	.cfi_restore 6
	cmpl	$2047, %r8d
	seta	%r8b
	jmp	.L1619
.L1625:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2835:
	.size	ucnv_MBCSFromUChar32_67, .-ucnv_MBCSFromUChar32_67
	.p2align 4
	.globl	ucnv_MBCSIsLeadByte_67
	.type	ucnv_MBCSIsLeadByte_67, @function
ucnv_MBCSIsLeadByte_67:
.LFB2839:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	movzbl	%sil, %esi
	movl	(%rax,%rsi,4), %eax
	notl	%eax
	shrl	$31, %eax
	ret
	.cfi_endproc
.LFE2839:
	.size	ucnv_MBCSIsLeadByte_67, .-ucnv_MBCSIsLeadByte_67
	.p2align 4
	.globl	ucnv_MBCSGetType_67
	.type	ucnv_MBCSGetType_67, @function
ucnv_MBCSGetType_67:
.LFB2841:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdx
	xorl	%eax, %eax
	cmpb	$1, 48(%rdx)
	je	.L1650
	cmpb	$12, 252(%rdx)
	je	.L1653
	movq	16(%rdx), %rax
	cmpw	$514, 70(%rax)
	setne	%al
	movzbl	%al, %eax
	addl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1653:
	movl	$9, %eax
.L1650:
	ret
	.cfi_endproc
.LFE2841:
	.size	ucnv_MBCSGetType_67, .-ucnv_MBCSGetType_67
	.section	.rodata
	.align 2
	.type	_ZZL17ucnv_DBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCodeE3nul, @object
	.size	_ZZL17ucnv_DBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCodeE3nul, 2
_ZZL17ucnv_DBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCodeE3nul:
	.zero	2
	.align 2
	.type	_ZZL17ucnv_SBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCodeE3nul, @object
	.size	_ZZL17ucnv_SBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCodeE3nul, 2
_ZZL17ucnv_SBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCodeE3nul:
	.zero	2
	.align 16
	.type	_ZL12utf8_offsets, @object
	.size	_ZL12utf8_offsets, 20
_ZL12utf8_offsets:
	.long	0
	.long	0
	.long	12416
	.long	925824
	.long	63447168
	.align 32
	.type	_ZL13gb18030Ranges, @object
	.size	_ZL13gb18030Ranges, 224
_ZL13gb18030Ranges:
	.long	65536
	.long	1114111
	.long	1876218
	.long	2924793
	.long	40870
	.long	55295
	.long	1706261
	.long	1720686
	.long	1106
	.long	7742
	.long	1688038
	.long	1694674
	.long	7744
	.long	8207
	.long	1694676
	.long	1695139
	.long	59493
	.long	63787
	.long	1720768
	.long	1725062
	.long	9795
	.long	11904
	.long	1696437
	.long	1698546
	.long	64042
	.long	65071
	.long	1725296
	.long	1726325
	.long	15585
	.long	16469
	.long	1701916
	.long	1702800
	.long	13851
	.long	14615
	.long	1700191
	.long	1700955
	.long	18872
	.long	19574
	.long	1705179
	.long	1705881
	.long	16736
	.long	17206
	.long	1703065
	.long	1703535
	.long	18318
	.long	18758
	.long	1704636
	.long	1705076
	.long	17623
	.long	17995
	.long	1703947
	.long	1704319
	.long	65510
	.long	65535
	.long	1726612
	.long	1726637
	.globl	_MBCSData_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_MBCSData_67, @object
	.size	_MBCSData_67, 296
_MBCSData_67:
	.long	296
	.long	1
	.quad	0
	.quad	0
	.byte	0
	.byte	1
	.zero	6
	.quad	_ZL9_MBCSImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	_ZL9_MBCSImpl, @object
	.size	_ZL9_MBCSImpl, 144
_ZL9_MBCSImpl:
	.long	2
	.zero	4
	.quad	_ZL13ucnv_MBCSLoadP20UConverterSharedDataP18UConverterLoadArgsPKhP10UErrorCode
	.quad	_ZL15ucnv_MBCSUnloadP20UConverterSharedData
	.quad	_ZL13ucnv_MBCSOpenP10UConverterP18UConverterLoadArgsP10UErrorCode
	.quad	0
	.quad	0
	.quad	ucnv_MBCSToUnicodeWithOffsets_67
	.quad	ucnv_MBCSToUnicodeWithOffsets_67
	.quad	ucnv_MBCSFromUnicodeWithOffsets_67
	.quad	ucnv_MBCSFromUnicodeWithOffsets_67
	.quad	_ZL21ucnv_MBCSGetNextUCharP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL20ucnv_MBCSGetStartersPK10UConverterPaP10UErrorCode
	.quad	ucnv_MBCSGetName
	.quad	_ZL17ucnv_MBCSWriteSubP25UConverterFromUnicodeArgsiP10UErrorCode
	.quad	0
	.quad	_ZL22ucnv_MBCSGetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode
	.quad	0
	.quad	0
	.align 32
	.type	_ZL13_DBCSUTF8Impl, @object
	.size	_ZL13_DBCSUTF8Impl, 144
_ZL13_DBCSUTF8Impl:
	.long	2
	.zero	4
	.quad	_ZL13ucnv_MBCSLoadP20UConverterSharedDataP18UConverterLoadArgsPKhP10UErrorCode
	.quad	_ZL15ucnv_MBCSUnloadP20UConverterSharedData
	.quad	_ZL13ucnv_MBCSOpenP10UConverterP18UConverterLoadArgsP10UErrorCode
	.quad	0
	.quad	0
	.quad	ucnv_MBCSToUnicodeWithOffsets_67
	.quad	ucnv_MBCSToUnicodeWithOffsets_67
	.quad	ucnv_MBCSFromUnicodeWithOffsets_67
	.quad	ucnv_MBCSFromUnicodeWithOffsets_67
	.quad	_ZL21ucnv_MBCSGetNextUCharP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL20ucnv_MBCSGetStartersPK10UConverterPaP10UErrorCode
	.quad	ucnv_MBCSGetName
	.quad	_ZL17ucnv_MBCSWriteSubP25UConverterFromUnicodeArgsiP10UErrorCode
	.quad	0
	.quad	_ZL22ucnv_MBCSGetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode
	.quad	0
	.quad	_ZL17ucnv_DBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCode
	.align 32
	.type	_ZL13_SBCSUTF8Impl, @object
	.size	_ZL13_SBCSUTF8Impl, 144
_ZL13_SBCSUTF8Impl:
	.long	2
	.zero	4
	.quad	_ZL13ucnv_MBCSLoadP20UConverterSharedDataP18UConverterLoadArgsPKhP10UErrorCode
	.quad	_ZL15ucnv_MBCSUnloadP20UConverterSharedData
	.quad	_ZL13ucnv_MBCSOpenP10UConverterP18UConverterLoadArgsP10UErrorCode
	.quad	0
	.quad	0
	.quad	ucnv_MBCSToUnicodeWithOffsets_67
	.quad	ucnv_MBCSToUnicodeWithOffsets_67
	.quad	ucnv_MBCSFromUnicodeWithOffsets_67
	.quad	ucnv_MBCSFromUnicodeWithOffsets_67
	.quad	_ZL21ucnv_MBCSGetNextUCharP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL20ucnv_MBCSGetStartersPK10UConverterPaP10UErrorCode
	.quad	ucnv_MBCSGetName
	.quad	_ZL17ucnv_MBCSWriteSubP25UConverterFromUnicodeArgsiP10UErrorCode
	.quad	0
	.quad	_ZL22ucnv_MBCSGetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode
	.quad	0
	.quad	_ZL17ucnv_SBCSFromUTF8P25UConverterFromUnicodeArgsP23UConverterToUnicodeArgsP10UErrorCode
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1
	.long	1
	.long	1
	.long	1
	.align 16
.LC1:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.align 16
.LC2:
	.long	0
	.long	16
	.long	32
	.long	48
	.align 16
.LC3:
	.long	64
	.long	64
	.long	64
	.long	64
	.align 16
.LC4:
	.long	2
	.long	2
	.long	2
	.long	2
	.align 16
.LC5:
	.long	3
	.long	3
	.long	3
	.long	3
	.align 16
.LC6:
	.long	4
	.long	4
	.long	4
	.long	4
	.align 16
.LC7:
	.long	5
	.long	5
	.long	5
	.long	5
	.align 16
.LC8:
	.long	6
	.long	6
	.long	6
	.long	6
	.align 16
.LC9:
	.long	7
	.long	7
	.long	7
	.long	7
	.align 16
.LC10:
	.long	8
	.long	8
	.long	8
	.long	8
	.align 16
.LC11:
	.long	9
	.long	9
	.long	9
	.long	9
	.align 16
.LC12:
	.long	10
	.long	10
	.long	10
	.long	10
	.align 16
.LC13:
	.long	11
	.long	11
	.long	11
	.long	11
	.align 16
.LC14:
	.long	12
	.long	12
	.long	12
	.long	12
	.align 16
.LC15:
	.long	13
	.long	13
	.long	13
	.long	13
	.align 16
.LC16:
	.long	14
	.long	14
	.long	14
	.long	14
	.align 16
.LC17:
	.long	15
	.long	15
	.long	15
	.long	15
	.align 16
.LC18:
	.long	0
	.long	1
	.long	2
	.long	3
	.align 16
.LC30:
	.long	-2140143616
	.long	-2140143616
	.long	-2140143616
	.long	-2140143616
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
