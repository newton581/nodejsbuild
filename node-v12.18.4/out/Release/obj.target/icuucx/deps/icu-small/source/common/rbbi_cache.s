	.file	"rbbi_cache.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0:
.LFB4237:
	.cfi_startproc
	movl	40(%rdi), %eax
	movl	16(%rdi), %r10d
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	js	.L2
	cmpl	%r10d, %eax
	jl	.L17
.L2:
	movl	$0, 40(%rdi)
	testl	%r10d, %r10d
	jle	.L11
.L6:
	leal	-1(%r10), %r11d
	xorl	%eax, %eax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	leal	1(%rax), %r8d
	movl	%r8d, 40(%rdi)
	leaq	1(%rax), %r8
	cmpq	%r11, %rax
	je	.L18
	movq	%r8, %rax
.L10:
	movl	%r10d, %r9d
	xorl	%r8d, %r8d
	subl	%eax, %r9d
	testl	%r9d, %r9d
	jle	.L8
	movq	32(%rdi), %r8
	movl	(%r8,%rax,4), %r8d
.L8:
	cmpl	%r8d, %esi
	jge	.L9
	movl	%r8d, (%rdx)
	movl	56(%rdi), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%eax, (%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	32(%rdi), %r9
	movslq	%eax, %r8
	cmpl	(%r9,%r8,4), %esi
	je	.L19
	movl	$0, 40(%rdi)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	cmpl	%r10d, %eax
	jl	.L20
	xorl	%eax, %eax
	movl	$-1, 40(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L20:
	.cfi_restore_state
	cltq
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	(%r9,%rax,4), %eax
	movl	%eax, (%rdx)
	movl	56(%rdi), %eax
	movl	%eax, (%rcx)
	movl	$1, %eax
	ret
.L18:
	.cfi_restore_state
	jmp	.L11
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0.cold, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0.cold:
.LFSB4237:
.L11:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE4237:
	.text
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0
	.section	.text.unlikely
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0.cold, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii.part.0, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii.part.0:
.LFB4239:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$56, %rsp
	movl	%esi, -76(%rbp)
	movdqa	.LC1(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, 56(%rdi)
	leaq	8(%rdi), %rax
	movups	%xmm0, 40(%rdi)
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN6icu_679UVector3217removeAllElementsEv@PLT
	movl	%r14d, 52(%r12)
	movq	(%r12), %r14
	movslq	%r15d, %rsi
	movl	%ebx, 56(%r12)
	leaq	328(%r14), %rbx
	movl	$0, -60(%rbp)
	movq	%rbx, %rdi
	call	utext_setNativeIndex_67@PLT
	movq	%rbx, %rdi
	call	utext_current32_67@PLT
	movl	%eax, %esi
	movq	(%r12), %rax
	movq	472(%rax), %rax
	movq	48(%rax), %rax
	movq	(%rax), %rdx
	cmpl	$55295, %esi
	ja	.L22
	movl	%esi, %eax
	sarl	$5, %eax
.L74:
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%esi, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
.L23:
	movl	-60(%rbp), %r8d
	movzwl	(%rdx,%rax), %r15d
	movl	$0, -72(%rbp)
	testl	%r8d, %r8d
	jle	.L40
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L80:
	addl	360(%r14), %eax
	cmpl	%r13d, %eax
	jge	.L32
.L81:
	testw	$16384, %r15w
	jne	.L33
	movq	%rbx, %rdi
	call	utext_next32_67@PLT
	movq	%rbx, %rdi
	call	utext_current32_67@PLT
	movl	%eax, %esi
	movq	(%r12), %rax
	movq	472(%rax), %rax
	movq	48(%rax), %rax
	movq	(%rax), %rdx
	cmpl	$55295, %esi
	ja	.L34
	movl	%esi, %eax
	sarl	$5, %eax
.L76:
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%esi, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
.L35:
	movzwl	(%rdx,%rax), %r15d
.L40:
	movl	368(%r14), %eax
	cmpl	356(%r14), %eax
	jle	.L80
	movq	384(%r14), %rax
	movl	%esi, -68(%rbp)
	movq	%rbx, %rdi
	call	*64(%rax)
	movl	-68(%rbp), %esi
	cmpl	%r13d, %eax
	jl	.L81
.L32:
	movl	-72(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L21
	movslq	16(%r12), %rax
	testl	%eax, %eax
	jle	.L48
	movq	32(%r12), %rdx
	movl	-76(%rbp), %ebx
	cmpl	(%rdx), %ebx
	jl	.L64
.L49:
	leal	-1(%rax), %ecx
	movslq	%ecx, %rsi
	cmpl	(%rdx,%rsi,4), %r13d
	jg	.L82
	movl	$0, 40(%r12)
.L63:
	movl	(%rdx), %eax
	movslq	%ecx, %rcx
	movl	%eax, 44(%r12)
	movl	(%rdx,%rcx,4), %eax
	movl	%eax, 48(%r12)
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	cmpl	$65535, %esi
	ja	.L36
	cmpl	$56320, %esi
	movl	$0, %eax
	movl	$320, %ecx
	cmovge	%eax, %ecx
	movl	%esi, %eax
	sarl	$5, %eax
	addl	%ecx, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L36:
	cmpl	$1114111, %esi
	jbe	.L38
	movl	24(%rax), %eax
	subl	$-128, %eax
	cltq
	addq	%rax, %rax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L38:
	cmpl	44(%rax), %esi
	jl	.L39
	movslq	48(%rax), %rax
	addq	%rax, %rax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator22getLanguageBreakEngineEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L61
	movq	(%rdi), %rax
	movq	-88(%rbp), %r8
	movl	%r13d, %ecx
	movq	%rbx, %rsi
	movl	-76(%rbp), %edx
	call	*24(%rax)
	addl	%eax, -72(%rbp)
.L61:
	movq	%rbx, %rdi
	call	utext_current32_67@PLT
	movl	%eax, %esi
	movq	(%r12), %rax
	movq	472(%rax), %rax
	movq	48(%rax), %rax
	movq	(%rax), %rdx
	cmpl	$55295, %esi
	ja	.L41
	movl	%esi, %eax
	sarl	$5, %eax
.L78:
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%esi, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
.L42:
	movl	-60(%rbp), %edi
	movzwl	(%rdx,%rax), %r15d
	testl	%edi, %edi
	jle	.L40
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L39:
	movl	%esi, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%esi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L41:
	cmpl	$65535, %esi
	ja	.L43
	cmpl	$56320, %esi
	movl	$0, %eax
	movl	$320, %ecx
	cmovge	%eax, %ecx
	movl	%esi, %eax
	sarl	$5, %eax
	addl	%ecx, %eax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L43:
	cmpl	$1114111, %esi
	jbe	.L45
	movl	24(%rax), %eax
	subl	$-128, %eax
	cltq
	addq	%rax, %rax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	44(%rax), %esi
	jl	.L46
	movslq	48(%rax), %rax
	addq	%rax, %rax
	jmp	.L42
.L46:
	movl	%esi, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%esi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	jmp	.L78
.L22:
	cmpl	$65535, %esi
	ja	.L24
	cmpl	$56320, %esi
	movl	$320, %eax
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%esi, %eax
	sarl	$5, %eax
	addl	%ecx, %eax
	jmp	.L74
.L24:
	cmpl	$1114111, %esi
	ja	.L84
	cmpl	44(%rax), %esi
	jl	.L27
	movslq	48(%rax), %rax
	addq	%rax, %rax
	jmp	.L23
.L48:
	movl	-76(%rbp), %edx
	testl	%edx, %edx
	js	.L64
.L50:
	testl	%r13d, %r13d
	jg	.L85
	movl	$0, 40(%r12)
.L58:
	movl	$0, 44(%r12)
	xorl	%eax, %eax
	movl	%eax, 48(%r12)
	jmp	.L21
.L64:
	movl	-76(%rbp), %esi
	movq	-88(%rbp), %rdi
	leaq	-60(%rbp), %rcx
	xorl	%edx, %edx
	call	_ZN6icu_679UVector3215insertElementAtEiiR10UErrorCode@PLT
	movslq	16(%r12), %rax
	testl	%eax, %eax
	jle	.L50
	movq	32(%r12), %rdx
	jmp	.L49
.L84:
	movl	24(%rax), %eax
	subl	$-128, %eax
	cltq
	addq	%rax, %rax
	jmp	.L23
.L85:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L54
.L62:
	cmpl	%esi, 20(%r12)
	jge	.L55
.L54:
	movq	-88(%rbp), %rdi
	leaq	-60(%rbp), %rdx
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L56
	movl	16(%r12), %eax
.L57:
	movl	$0, 40(%r12)
	testl	%eax, %eax
	jle	.L58
	movq	32(%r12), %rdx
	leal	-1(%rax), %ecx
	jmp	.L63
.L56:
	movslq	16(%r12), %rax
.L55:
	movq	32(%r12), %rdx
	movl	%r13d, (%rdx,%rax,4)
	movl	16(%r12), %eax
	addl	$1, %eax
	movl	%eax, 16(%r12)
	jmp	.L57
.L27:
	movl	%esi, %eax
	movl	%esi, %ecx
	sarl	$11, %eax
	sarl	$5, %ecx
	addl	$2080, %eax
	andl	$63, %ecx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%ecx, %eax
	jmp	.L74
.L82:
	leal	1(%rax), %esi
	jmp	.L62
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4239:
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii.part.0, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC2EPS0_R10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC2EPS0_R10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC2EPS0_R10UErrorCode:
.LFB3161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%r8, -8(%rdi)
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movdqa	.LC1(%rip), %xmm0
	movl	$0, 56(%rbx)
	movups	%xmm0, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3161:
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC2EPS0_R10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC2EPS0_R10UErrorCode
	.globl	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC1EPS0_R10UErrorCode
	.set	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC1EPS0_R10UErrorCode,_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC2EPS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache5resetEv
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache5resetEv, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache5resetEv:
.LFB3166:
	.cfi_startproc
	endbr64
	movl	$0, 56(%rdi)
	movdqa	.LC1(%rip), %xmm0
	addq	$8, %rdi
	movups	%xmm0, 32(%rdi)
	jmp	_ZN6icu_679UVector3217removeAllElementsEv@PLT
	.cfi_endproc
.LFE3166:
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache5resetEv, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache5resetEv
	.section	.text.unlikely
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_:
.LFB3167:
	.cfi_startproc
	endbr64
	cmpl	%esi, 48(%rdi)
	jle	.L105
	cmpl	%esi, 44(%rdi)
	jg	.L105
	movl	40(%rdi), %eax
	movl	16(%rdi), %r10d
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	js	.L93
	cmpl	%r10d, %eax
	jl	.L107
.L93:
	movl	$0, 40(%rdi)
	testl	%r10d, %r10d
	jle	.L100
.L96:
	leal	-1(%r10), %r11d
	xorl	%eax, %eax
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L98:
	leal	1(%rax), %r8d
	movl	%r8d, 40(%rdi)
	leaq	1(%rax), %r8
	cmpq	%r11, %rax
	je	.L108
	movq	%r8, %rax
.L99:
	movl	%r10d, %r9d
	xorl	%r8d, %r8d
	subl	%eax, %r9d
	testl	%r9d, %r9d
	jle	.L97
	movq	32(%rdi), %r8
	movl	(%r8,%rax,4), %r8d
.L97:
	cmpl	%r8d, %esi
	jge	.L98
	movl	%r8d, (%rdx)
	movl	56(%rdi), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, (%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore 6
	movl	$-1, 40(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	32(%rdi), %r9
	movslq	%eax, %r8
	cmpl	(%r9,%r8,4), %esi
	je	.L109
	movl	$0, 40(%rdi)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L109:
	addl	$1, %eax
	movl	%eax, 40(%rdi)
	cmpl	%r10d, %eax
	jge	.L90
	cltq
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	(%r9,%rax,4), %eax
	movl	%eax, (%rdx)
	movl	56(%rdi), %eax
	movl	%eax, (%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	xorl	%eax, %eax
	movl	$-1, 40(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L108:
	.cfi_restore_state
	jmp	.L100
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.cold, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.cold:
.LFSB3167:
.L100:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3167:
	.text
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_
	.section	.text.unlikely
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.cold, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.cold
.LCOLDE2:
	.text
.LHOTE2:
	.section	.text.unlikely
	.align 2
.LCOLDB3:
	.text
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9precedingEiPiS2_
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9precedingEiPiS2_, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9precedingEiPiS2_:
.LFB3168:
	.cfi_startproc
	endbr64
	cmpl	%esi, 44(%rdi)
	jge	.L142
	cmpl	%esi, 48(%rdi)
	jl	.L142
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	je	.L114
	movl	40(%rdi), %eax
.L115:
	testl	%eax, %eax
	jle	.L116
	movl	16(%rdi), %r11d
	cmpl	%eax, %r11d
	jg	.L144
.L117:
	leal	-1(%r11), %eax
	movl	%eax, 40(%rdi)
	testl	%eax, %eax
	js	.L131
	testl	%r11d, %r11d
	jg	.L124
	testl	%esi, %esi
	jle	.L140
	xorl	%r8d, %r8d
.L125:
	movl	%r8d, (%rdx)
	movl	56(%rdi), %eax
	cmpl	%r8d, 44(%rdi)
	cmove	52(%rdi), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, (%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore 6
	movl	$-1, 40(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	%r11d, %r9d
	xorl	%r8d, %r8d
	subl	%eax, %r9d
	testl	%r9d, %r9d
	jle	.L118
	movq	32(%rdi), %r9
	movslq	%eax, %r8
	movl	(%r9,%r8,4), %r8d
.L118:
	cmpl	%r8d, %esi
	je	.L145
	leal	-1(%r11), %eax
	movl	%eax, 40(%rdi)
.L124:
	movslq	%eax, %r9
	salq	$2, %r9
	.p2align 4,,10
	.p2align 3
.L130:
	movl	%r11d, %r10d
	xorl	%r8d, %r8d
	subl	%eax, %r10d
	testl	%r10d, %r10d
	jle	.L127
	movq	32(%rdi), %r8
	movl	(%r8,%r9), %r8d
.L127:
	cmpl	%r8d, %esi
	jg	.L125
	subl	$1, %eax
	subq	$4, %r9
	movl	%eax, 40(%rdi)
	cmpl	$-1, %eax
	jne	.L130
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L116:
	jne	.L146
	xorl	%eax, %eax
	movl	$-1, 40(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movl	16(%rdi), %eax
	subl	$1, %eax
	movl	%eax, 40(%rdi)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L146:
	movl	16(%rdi), %r11d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L145:
	subl	$1, %eax
	xorl	%esi, %esi
	subl	%eax, %r11d
	movl	%eax, 40(%rdi)
	testl	%r11d, %r11d
	jle	.L120
	movq	32(%rdi), %rsi
	cltq
	movl	(%rsi,%rax,4), %esi
.L120:
	movl	%esi, (%rdx)
	movl	56(%rdi), %eax
	cmpl	%esi, 44(%rdi)
	cmove	52(%rdi), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, (%rcx)
	movl	$1, %eax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9precedingEiPiS2_.cold, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9precedingEiPiS2_.cold:
.LFSB3168:
.L140:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	subl	$2, %r11d
	movl	%r11d, 40(%rdi)
.L131:
	call	abort@PLT
	.cfi_endproc
.LFE3168:
	.text
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9precedingEiPiS2_, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9precedingEiPiS2_
	.section	.text.unlikely
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9precedingEiPiS2_.cold, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9precedingEiPiS2_.cold
.LCOLDE3:
	.text
.LHOTE3:
	.align 2
	.p2align 4
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode.part.0, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode.part.0:
.LFB4240:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	16(%rdi), %rax
	movl	32(%rdi,%rax,4), %ebx
	testl	%ebx, %ebx
	jne	.L148
.L190:
	xorl	%eax, %eax
.L147:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L221
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	%rdi, %r15
	leaq	-60(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	movq	%rsi, %r12
	movl	%ebx, %esi
	movl	$0, -64(%rbp)
	movl	%ebx, %r14d
	movq	496(%rax), %rdi
	movl	$0, -60(%rbp)
	xorl	%r13d, %r13d
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9precedingEiPiS2_
	testb	%al, %al
	je	.L150
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L152:
	xorl	%eax, %eax
	movl	%r13d, -64(%rbp)
	movl	%r13d, -60(%rbp)
	cmpl	%eax, %ebx
	jg	.L223
.L150:
	leal	-30(%r14), %esi
	xorl	%r14d, %r14d
	testl	%esi, %esi
	jle	.L152
	movq	8(%r15), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator18handleSafePreviousEi@PLT
	movl	%eax, %r14d
	leal	1(%rax), %eax
	cmpl	$1, %eax
	jbe	.L152
	movq	8(%r15), %rdi
	movl	%r14d, 480(%rdi)
	call	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv@PLT
	leal	4(%r14), %edx
	movl	%eax, -64(%rbp)
	cmpl	%edx, %eax
	jle	.L224
.L155:
	movq	8(%r15), %rdx
	movl	484(%rdx), %edx
	movl	%edx, -60(%rbp)
	cmpl	%eax, %ebx
	jle	.L150
.L223:
	leaq	800(%r15), %rax
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN6icu_679UVector3217removeAllElementsEv@PLT
	movslq	808(%r15), %rax
	movl	-64(%rbp), %r13d
	movl	%eax, %esi
	addl	$1, %esi
	js	.L157
	cmpl	812(%r15), %esi
	jle	.L158
.L157:
	movq	-72(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L225
	movslq	808(%r15), %rax
.L158:
	movq	824(%r15), %rdx
	movl	%r13d, (%rdx,%rax,4)
	movl	808(%r15), %eax
	addl	$1, %eax
	movl	%eax, 808(%r15)
.L160:
	movl	%eax, %esi
	movl	-60(%rbp), %r13d
	addl	$1, %esi
	js	.L161
	cmpl	812(%r15), %esi
	jle	.L162
.L161:
	movq	-72(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L226
.L163:
	movl	-64(%rbp), %r14d
	.p2align 4,,10
	.p2align 3
.L188:
	movq	8(%r15), %rdi
	movl	-60(%rbp), %r13d
	movl	%r14d, 480(%rdi)
	call	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv@PLT
	movl	%eax, -64(%rbp)
	movl	%eax, %r10d
	movq	8(%r15), %rax
	movl	484(%rax), %r8d
	movl	%r8d, -60(%rbp)
	cmpl	$-1, %r10d
	je	.L164
	movl	520(%rax), %edx
	testl	%edx, %edx
	jne	.L227
.L165:
	movslq	808(%r15), %rax
	cmpl	%r10d, %ebx
	jg	.L228
.L189:
	testl	%eax, %eax
	je	.L190
	jle	.L191
	leal	-1(%rax), %edx
	movq	824(%r15), %rcx
	movslq	%edx, %rsi
	movl	%edx, 808(%r15)
	movl	(%rcx,%rsi,4), %esi
	movl	%esi, -60(%rbp)
	movl	%esi, %edi
	testl	%edx, %edx
	je	.L202
	subl	$2, %eax
	movslq	%eax, %rdx
	movl	%eax, 808(%r15)
	movl	(%rcx,%rdx,4), %edx
.L192:
	movl	16(%r15), %ebx
	movl	20(%r15), %esi
	movl	%edx, -64(%rbp)
	leal	-1(%rbx), %r10d
	andl	$127, %r10d
	cmpl	%esi, %r10d
	jne	.L193
	leal	-1(%r10), %esi
	andl	$127, %esi
	movl	%esi, 20(%r15)
.L193:
	movslq	%r10d, %rcx
	movl	%edx, 32(%r15,%rcx,4)
	movw	%di, 544(%r15,%rcx,2)
	movl	%r10d, 16(%r15)
	movl	%r10d, 28(%r15)
	movl	%edx, 24(%r15)
	testl	%eax, %eax
	je	.L198
	movl	%r10d, %edx
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L230:
	leal	-1(%rax), %ecx
	movq	824(%r15), %rdi
	movslq	%ecx, %r8
	movl	%ecx, 808(%r15)
	movl	(%rdi,%r8,4), %r8d
	movl	%r8d, -60(%rbp)
	testl	%ecx, %ecx
	je	.L204
	subl	$2, %eax
	movslq	%eax, %rcx
	movl	%eax, 808(%r15)
	movl	(%rdi,%rcx,4), %ecx
.L196:
	subl	$1, %edx
	movl	%ecx, -64(%rbp)
	andl	$127, %edx
	cmpl	%edx, %esi
	je	.L229
.L197:
	movslq	%edx, %rdi
	movl	%ecx, 32(%r15,%rdi,4)
	movw	%r8w, 544(%r15,%rdi,2)
	movl	%edx, 16(%r15)
	testl	%eax, %eax
	je	.L198
.L194:
	testl	%eax, %eax
	jg	.L230
	subl	$1, %edx
	xorl	%ecx, %ecx
	movl	$0, -60(%rbp)
	xorl	%r8d, %r8d
	andl	$127, %edx
	movl	%ecx, -64(%rbp)
	cmpl	%edx, %esi
	jne	.L197
.L229:
	cmpl	%r10d, %esi
	je	.L198
	subl	$1, %esi
	andl	$127, %esi
	movl	%esi, 20(%r15)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L224:
	movslq	%eax, %rsi
	movq	8(%r15), %rax
	leaq	328(%rax), %rdi
	call	utext_setNativeIndex_67@PLT
	movq	8(%r15), %rax
	leaq	328(%rax), %rdi
	call	utext_getPreviousNativeIndex_67@PLT
	movslq	%r14d, %rdx
	cmpq	%rax, %rdx
	je	.L156
	movl	-64(%rbp), %eax
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L226:
	movl	808(%r15), %eax
.L162:
	movq	824(%r15), %rdx
	cltq
	movl	%r13d, (%rdx,%rax,4)
	addl	$1, 808(%r15)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L228:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L181
	cmpl	812(%r15), %esi
	jle	.L182
.L181:
	movq	-72(%rbp), %rdi
	movq	%r12, %rdx
	movl	%r10d, -76(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movl	-76(%rbp), %r10d
	testb	%al, %al
	jne	.L183
	movl	808(%r15), %eax
.L184:
	movl	%eax, %esi
	movl	-60(%rbp), %r13d
	addl	$1, %esi
	js	.L185
	cmpl	812(%r15), %esi
	jle	.L186
.L185:
	movq	-72(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L220
	movl	808(%r15), %eax
.L186:
	movq	824(%r15), %rdx
	cltq
	movl	%r13d, (%rdx,%rax,4)
	addl	$1, 808(%r15)
.L220:
	movl	-64(%rbp), %r14d
.L180:
	cmpl	%r14d, %ebx
	jg	.L188
	.p2align 4,,10
	.p2align 3
.L164:
	movl	808(%r15), %eax
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L227:
	movq	496(%rax), %rdi
	movl	%r10d, %eax
	subl	%r14d, %eax
	cmpl	$1, %eax
	jle	.L231
	movl	%r13d, %ecx
	movl	%r10d, %edx
	movl	%r14d, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii.part.0
	movq	8(%r15), %rax
	movq	496(%rax), %rdi
	cmpl	48(%rdi), %r14d
	jge	.L232
.L167:
	xorl	%r13d, %r13d
.L179:
	cmpl	%r14d, 44(%rdi)
	jg	.L233
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movl	%r14d, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0
	testb	%al, %al
	je	.L170
	movl	-64(%rbp), %r13d
	movslq	808(%r15), %rax
	cmpl	%r13d, %ebx
	jle	.L189
	movl	%eax, %esi
	addl	$1, %esi
	js	.L172
	cmpl	812(%r15), %esi
	jle	.L173
.L172:
	movq	-72(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L174
	movl	808(%r15), %eax
.L175:
	movl	%eax, %esi
	movl	-60(%rbp), %r13d
	addl	$1, %esi
	js	.L176
	cmpl	812(%r15), %esi
	jle	.L177
.L176:
	movq	-72(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L178
	movl	808(%r15), %eax
.L177:
	movq	824(%r15), %rdx
	cltq
	movl	%r13d, (%rdx,%rax,4)
	addl	$1, 808(%r15)
.L178:
	movq	8(%r15), %rax
	movl	-64(%rbp), %r14d
	movl	$1, %r13d
	movq	496(%rax), %rdi
	cmpl	%r14d, 48(%rdi)
	jg	.L179
	movl	$-1, 40(%rdi)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L183:
	movslq	808(%r15), %rax
.L182:
	movq	824(%r15), %rdx
	movl	%r10d, (%rdx,%rax,4)
	movl	808(%r15), %eax
	addl	$1, %eax
	movl	%eax, 808(%r15)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L174:
	movslq	808(%r15), %rax
.L173:
	movq	824(%r15), %rdx
	movl	%r13d, (%rdx,%rax,4)
	movl	808(%r15), %eax
	addl	$1, %eax
	movl	%eax, 808(%r15)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L231:
	cmpl	48(%rdi), %r14d
	jl	.L167
	movl	$-1, 40(%rdi)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L233:
	movl	$-1, 40(%rdi)
	movl	-64(%rbp), %r14d
.L169:
	testb	%r13b, %r13b
	jne	.L180
	movl	%r14d, %r10d
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L225:
	movl	808(%r15), %eax
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$1, %eax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L222:
	movl	16(%r15), %eax
	movl	-60(%rbp), %esi
	movl	-64(%rbp), %edx
	subl	$1, %eax
	andl	$127, %eax
	cmpl	20(%r15), %eax
	jne	.L151
	leal	-1(%rax), %ecx
	andl	$127, %ecx
	movl	%ecx, 20(%r15)
.L151:
	movslq	%eax, %rcx
	movl	%edx, 32(%r15,%rcx,4)
	movw	%si, 544(%r15,%rcx,2)
	movl	%eax, 16(%r15)
	movl	%eax, 28(%r15)
	movl	$1, %eax
	movl	%edx, 24(%r15)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$-1, 40(%rdi)
	movl	-64(%rbp), %r10d
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L156:
	movq	8(%r15), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv@PLT
	movl	%eax, -64(%rbp)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$0, -60(%rbp)
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L192
.L170:
	movl	-64(%rbp), %r14d
	jmp	.L169
.L221:
	call	__stack_chk_fail@PLT
.L204:
	xorl	%eax, %eax
	jmp	.L196
.L202:
	xorl	%eax, %eax
	jmp	.L192
	.cfi_endproc
.LFE4240:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode.part.0, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii:
.LFB3169:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	subl	%esi, %eax
	cmpl	$1, %eax
	jle	.L234
	jmp	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii.part.0
	.p2align 4,,10
	.p2align 3
.L234:
	ret
	.cfi_endproc
.LFE3169:
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC2EPS0_R10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC2EPS0_R10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC2EPS0_R10UErrorCode:
.LFB3171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIterator10BreakCacheE(%rip), %rax
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	800(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -800(%rdi)
	movq	%r8, -792(%rdi)
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$0, 32(%rbx)
	movw	%ax, 544(%rbx)
	movups	%xmm0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3171:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC2EPS0_R10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC2EPS0_R10UErrorCode
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC1EPS0_R10UErrorCode
	.set	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC1EPS0_R10UErrorCode,_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC2EPS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache5resetEii
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache5resetEii, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache5resetEii:
.LFB3177:
	.cfi_startproc
	endbr64
	movq	$0, 16(%rdi)
	movl	%esi, 24(%rdi)
	movl	$0, 28(%rdi)
	movl	%esi, 32(%rdi)
	movw	%dx, 544(%rdi)
	ret
	.cfi_endproc
.LFE3177:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache5resetEii, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache5resetEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv:
.LFB3178:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	24(%rdi), %edx
	movl	%edx, 480(%rax)
	movslq	28(%rdi), %rdx
	movzwl	544(%rdi,%rdx,2), %edx
	movb	$0, 632(%rax)
	movl	%edx, 484(%rax)
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE3178:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache6nextOLEv
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache6nextOLEv, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache6nextOLEv:
.LFB3181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	20(%rdi), %rax
	movl	$0, -48(%rbp)
	movl	$0, -44(%rbp)
	movzwl	544(%rdi,%rax,2), %r13d
	movl	32(%rdi,%rax,4), %r12d
	movq	8(%rdi), %rdi
	movq	496(%rdi), %r8
	cmpl	48(%r8), %r12d
	jge	.L241
	cmpl	44(%r8), %r12d
	jl	.L241
	leaq	-44(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r8, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0
	testb	%al, %al
	jne	.L252
	movq	8(%rbx), %rdi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$-1, 40(%r8)
.L243:
	movl	%r12d, 480(%rdi)
	call	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv@PLT
	movq	8(%rbx), %rdi
	movl	%eax, %edx
	movl	%eax, -48(%rbp)
	movl	$1, %eax
	cmpl	$-1, %edx
	je	.L246
	movl	484(%rdi), %r8d
	movl	520(%rdi), %ecx
	movl	%r8d, -44(%rbp)
	testl	%ecx, %ecx
	je	.L248
	movl	%edx, %eax
	movq	496(%rdi), %r9
	subl	%r12d, %eax
	cmpl	$1, %eax
	jle	.L249
	movq	%r9, %rdi
	movzwl	%r13w, %ecx
	movl	%r12d, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii.part.0
	movq	8(%rbx), %rdi
	movq	496(%rdi), %r9
.L249:
	cmpl	48(%r9), %r12d
	jge	.L250
	cmpl	44(%r9), %r12d
	jl	.L250
	leaq	-44(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r9, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0
	testb	%al, %al
	jne	.L252
	movl	-44(%rbp), %r8d
	movl	-48(%rbp), %edx
	movq	8(%rbx), %rdi
.L248:
	movl	20(%rbx), %eax
	addl	$1, %eax
	andl	$127, %eax
	cmpl	16(%rbx), %eax
	je	.L267
.L254:
	movslq	%eax, %rcx
	movl	$6, %r12d
	movl	%edx, 32(%rbx,%rcx,4)
	movw	%r8w, 544(%rbx,%rcx,2)
	movl	%eax, 20(%rbx)
	movl	%eax, 28(%rbx)
	movl	%edx, 24(%rbx)
.L258:
	call	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv@PLT
	movq	8(%rbx), %rdi
	movl	%eax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L262
	movl	520(%rdi), %edx
	testl	%edx, %edx
	jne	.L262
	movl	20(%rbx), %ecx
	movl	484(%rdi), %esi
	leal	1(%rcx), %edx
	andl	$127, %edx
	cmpl	16(%rbx), %edx
	jne	.L256
	leal	6(%rdx), %ecx
	andl	$127, %ecx
	movl	%ecx, 16(%rbx)
.L256:
	movslq	%edx, %rcx
	movl	%eax, 32(%rbx,%rcx,4)
	movw	%si, 544(%rbx,%rcx,2)
	movl	%edx, 20(%rbx)
	subl	$1, %r12d
	jne	.L258
.L262:
	xorl	%eax, %eax
.L246:
	movb	%al, 632(%rdi)
	movq	8(%rbx), %rax
	movl	24(%rbx), %edx
	movl	%edx, 480(%rax)
	movslq	28(%rbx), %rdx
	movzwl	544(%rbx,%rdx,2), %edx
	movl	%edx, 484(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L268
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	leal	6(%rax), %ecx
	andl	$127, %ecx
	movl	%ecx, 16(%rbx)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L250:
	movl	20(%rbx), %eax
	movl	-44(%rbp), %r8d
	movl	$-1, 40(%r9)
	movl	-48(%rbp), %edx
	addl	$1, %eax
	andl	$127, %eax
	cmpl	16(%rbx), %eax
	jne	.L254
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L252:
	movl	20(%rbx), %eax
	movl	-44(%rbp), %esi
	movl	-48(%rbp), %edx
	addl	$1, %eax
	andl	$127, %eax
	cmpl	16(%rbx), %eax
	jne	.L253
	leal	6(%rax), %ecx
	andl	$127, %ecx
	movl	%ecx, 16(%rbx)
.L253:
	movslq	%eax, %rcx
	movq	8(%rbx), %rdi
	movl	%edx, 32(%rbx,%rcx,4)
	movw	%si, 544(%rbx,%rcx,2)
	movl	%eax, 20(%rbx)
	movl	%eax, 28(%rbx)
	xorl	%eax, %eax
	movl	%edx, 24(%rbx)
	jmp	.L246
.L268:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3181:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache6nextOLEv, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache6nextOLEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache8previousER10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache8previousER10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache8previousER10UErrorCode:
.LFB3182:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L274
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movl	28(%rdi), %r12d
	cmpl	16(%rdi), %r12d
	je	.L277
	leal	-1(%r12), %eax
	andl	$127, %eax
	movslq	%eax, %rdx
	movl	%eax, 28(%rdi)
	movl	32(%rdi,%rdx,4), %edx
	movl	%edx, 24(%rdi)
.L272:
	movq	8(%rdi), %rdx
	cmpl	%eax, %r12d
	sete	632(%rdx)
	movl	24(%rdi), %edx
	movq	8(%rdi), %rax
	movl	%edx, 480(%rax)
	movslq	28(%rdi), %rdx
	movzwl	544(%rdi,%rdx,2), %edx
	movl	%edx, 484(%rax)
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	%rdi, -24(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode.part.0
	movq	-24(%rbp), %rdi
	movl	28(%rdi), %eax
	jmp	.L272
	.cfi_endproc
.LFE3182:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache8previousER10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache8previousER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi:
.LFB3183:
	.cfi_startproc
	endbr64
	movslq	16(%rdi), %rax
	xorl	%r9d, %r9d
	movq	%rax, %r8
	movl	32(%rdi,%rax,4), %eax
	cmpl	%esi, %eax
	jg	.L278
	movslq	20(%rdi), %rdx
	movq	%rdx, %rcx
	movl	32(%rdi,%rdx,4), %edx
	cmpl	%esi, %edx
	jl	.L278
	cmpl	%esi, %eax
	je	.L289
	cmpl	%esi, %edx
	jne	.L284
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L292:
	leal	(%r8,%rcx), %edx
	leal	128(%rdx), %eax
	cmovg	%eax, %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	andl	$127, %eax
	andl	$127, %edx
	cmpl	%esi, 32(%rdi,%rax,4)
	jle	.L291
	movl	%edx, %ecx
.L284:
	cmpl	%ecx, %r8d
	jne	.L292
	subl	$1, %r8d
	movl	$1, %r9d
	andl	$127, %r8d
	movl	%r8d, 28(%rdi)
	movslq	%r8d, %r8
	movl	32(%rdi,%r8,4), %eax
	movl	%eax, 24(%rdi)
.L278:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	leal	1(%rdx), %r8d
	andl	$127, %r8d
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L289:
	movl	%r8d, 28(%rdi)
	movl	$1, %r9d
	movl	%eax, 24(%rdi)
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L290:
	movl	%ecx, 28(%rdi)
	movl	$1, %r9d
	movl	%edx, 24(%rdi)
	jmp	.L278
	.cfi_endproc
.LFE3183:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populateFollowingEv
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populateFollowingEv, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populateFollowingEv:
.LFB3185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	20(%rdi), %rax
	movl	$0, -48(%rbp)
	movl	$0, -44(%rbp)
	movzwl	544(%rdi,%rax,2), %r13d
	movl	32(%rdi,%rax,4), %r12d
	movq	8(%rdi), %rdi
	movq	496(%rdi), %r8
	cmpl	48(%r8), %r12d
	jge	.L294
	cmpl	44(%r8), %r12d
	jl	.L294
	leaq	-44(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r8, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0
	testb	%al, %al
	jne	.L304
	movq	8(%rbx), %rdi
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L294:
	movl	$-1, 40(%r8)
.L296:
	movl	%r12d, 480(%rdi)
	call	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv@PLT
	movl	%eax, %edx
	movl	%eax, -48(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %edx
	je	.L293
	movq	8(%rbx), %rdi
	movl	484(%rdi), %r8d
	movl	520(%rdi), %ecx
	movl	%r8d, -44(%rbp)
	testl	%ecx, %ecx
	je	.L300
	movl	%edx, %eax
	movq	496(%rdi), %r9
	subl	%r12d, %eax
	cmpl	$1, %eax
	jle	.L301
	movq	%r9, %rdi
	movzwl	%r13w, %ecx
	movl	%r12d, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache18populateDictionaryEiiii.part.0
	movq	8(%rbx), %rdi
	movq	496(%rdi), %r9
.L301:
	cmpl	48(%r9), %r12d
	jge	.L302
	cmpl	44(%r9), %r12d
	jl	.L302
	leaq	-44(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r9, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache9followingEiPiS2_.part.0
	testb	%al, %al
	jne	.L304
	movl	-44(%rbp), %r8d
	movl	-48(%rbp), %edx
	movq	8(%rbx), %rdi
.L300:
	movl	20(%rbx), %eax
	addl	$1, %eax
	andl	$127, %eax
	cmpl	16(%rbx), %eax
	je	.L325
.L306:
	movslq	%eax, %rcx
	movl	$6, %r12d
	movl	%edx, 32(%rbx,%rcx,4)
	movw	%r8w, 544(%rbx,%rcx,2)
	movl	%eax, 20(%rbx)
	movl	%eax, 28(%rbx)
	movl	%edx, 24(%rbx)
.L311:
	call	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv@PLT
	movl	%eax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L308
	movq	8(%rbx), %rdi
	movl	520(%rdi), %edx
	testl	%edx, %edx
	jne	.L308
	movl	20(%rbx), %esi
	movl	484(%rdi), %ecx
	leal	1(%rsi), %edx
	andl	$127, %edx
	cmpl	16(%rbx), %edx
	jne	.L309
	leal	6(%rdx), %esi
	andl	$127, %esi
	movl	%esi, 16(%rbx)
.L309:
	movslq	%edx, %rsi
	movl	%eax, 32(%rbx,%rsi,4)
	movw	%cx, 544(%rbx,%rsi,2)
	movl	%edx, 20(%rbx)
	subl	$1, %r12d
	jne	.L311
.L308:
	movl	$1, %eax
.L293:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L326
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	leal	6(%rax), %ecx
	andl	$127, %ecx
	movl	%ecx, 16(%rbx)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L302:
	movl	20(%rbx), %eax
	movl	-44(%rbp), %r8d
	movl	$-1, 40(%r9)
	movl	-48(%rbp), %edx
	addl	$1, %eax
	andl	$127, %eax
	cmpl	16(%rbx), %eax
	jne	.L306
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L304:
	movl	20(%rbx), %eax
	movl	-44(%rbp), %esi
	movl	-48(%rbp), %edx
	addl	$1, %eax
	andl	$127, %eax
	cmpl	16(%rbx), %eax
	jne	.L305
	leal	6(%rax), %ecx
	andl	$127, %ecx
	movl	%ecx, 16(%rbx)
.L305:
	movslq	%eax, %rcx
	movl	%edx, 32(%rbx,%rcx,4)
	movw	%si, 544(%rbx,%rcx,2)
	movl	%eax, 20(%rbx)
	movl	%eax, 28(%rbx)
	movl	$1, %eax
	movl	%edx, 24(%rbx)
	jmp	.L293
.L326:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3185:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populateFollowingEv, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populateFollowingEv
	.section	.text.unlikely
	.align 2
.LCOLDB4:
	.text
.LHOTB4:
	.align 2
	.p2align 4
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0:
.LFB4242:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	16(%rdi), %rax
	movl	32(%rdi,%rax,4), %eax
	leal	-15(%rax), %edx
	cmpl	%esi, %edx
	jg	.L328
	movslq	20(%rdi), %rdx
	movl	32(%rdi,%rdx,4), %r14d
	leal	15(%r14), %edx
	cmpl	%edx, %esi
	jle	.L329
.L328:
	cmpl	$20, %r12d
	jg	.L330
.L332:
	xorl	%eax, %eax
	xorl	%r14d, %r14d
.L331:
	movq	$0, 16(%rbx)
	movl	%r14d, 24(%rbx)
	movl	$0, 28(%rbx)
	movl	%r14d, 32(%rbx)
	movw	%ax, 544(%rbx)
	movl	%r14d, %eax
.L329:
	cmpl	%r14d, %r12d
	jg	.L334
	cmpl	%eax, %r12d
	jl	.L346
.L341:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populateFollowingEv
	testb	%al, %al
	je	.L357
	movslq	20(%rbx), %rax
	movq	%rax, %rdx
	movl	32(%rbx,%rax,4), %eax
	cmpl	%eax, %r12d
	jg	.L334
	movl	%edx, 28(%rbx)
	movl	%eax, 24(%rbx)
	jne	.L344
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L346:
	movl	0(%r13), %eax
.L345:
	testl	%eax, %eax
	jg	.L345
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode.part.0
	movslq	16(%rbx), %rdx
	movq	%rdx, %rax
	movl	32(%rbx,%rdx,4), %edx
	cmpl	%edx, %r12d
	jl	.L346
	movl	%eax, 28(%rbx)
	movl	%edx, 24(%rbx)
	jne	.L347
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L348:
	addl	$1, %eax
	movq	8(%rbx), %rdx
	andl	$127, %eax
	movl	%eax, 28(%rbx)
	cltq
	movl	32(%rbx,%rax,4), %eax
	movl	%eax, 480(%rdx)
	movl	%eax, 24(%rbx)
.L362:
	movslq	28(%rbx), %rax
	movzwl	544(%rbx,%rax,2), %eax
	movl	%eax, 484(%rdx)
	movl	24(%rbx), %eax
	cmpl	%eax, %r12d
	jle	.L363
	movl	28(%rbx), %eax
.L347:
	cmpl	%eax, 20(%rbx)
	jne	.L348
	movq	%rbx, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populateFollowingEv
	movq	8(%rbx), %rdx
	testb	%al, %al
	sete	632(%rdx)
	movq	8(%rbx), %rdx
	movl	24(%rbx), %eax
	movl	%eax, 480(%rdx)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L342:
	leal	-1(%r14), %eax
	andl	$127, %eax
	movslq	%eax, %rdx
	cmpl	%eax, %r14d
	movl	%eax, 28(%rbx)
	movl	32(%rbx,%rdx,4), %edx
	movl	%edx, 24(%rbx)
	movq	8(%rbx), %rdx
	sete	632(%rdx)
.L360:
	movl	24(%rbx), %edx
	movq	8(%rbx), %rax
	movl	%edx, 480(%rax)
	movslq	28(%rbx), %rdx
	movzwl	544(%rbx,%rdx,2), %edx
	movl	%edx, 484(%rax)
	cmpl	24(%rbx), %r12d
	jge	.L341
.L344:
	movl	0(%r13), %eax
.L339:
	testl	%eax, %eax
	jg	.L339
	movl	28(%rbx), %r14d
	cmpl	16(%rbx), %r14d
	jne	.L342
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode.part.0
	movq	8(%rbx), %rax
	cmpl	%r14d, 28(%rbx)
	sete	632(%rax)
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L330:
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator18handleSafePreviousEi@PLT
	movslq	%eax, %r15
	testl	%r15d, %r15d
	jle	.L332
	movq	8(%rbx), %rdi
	movl	%r15d, 480(%rdi)
	call	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv@PLT
	movl	%eax, %r14d
	leal	4(%r15), %eax
	cmpl	%eax, %r14d
	jle	.L364
.L333:
	movq	8(%rbx), %rax
	movzwl	484(%rax), %eax
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L363:
	cmpl	%eax, %r12d
	jge	.L341
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L341
	movl	28(%rbx), %r12d
	cmpl	16(%rbx), %r12d
	je	.L365
	leal	-1(%r12), %eax
	andl	$127, %eax
	movslq	%eax, %rcx
	movl	%eax, 28(%rbx)
	movl	32(%rbx,%rcx,4), %ecx
	movl	%ecx, 24(%rbx)
.L352:
	cmpl	%r12d, %eax
	sete	632(%rdx)
	movl	24(%rbx), %edx
	movq	8(%rbx), %rax
	movl	%edx, 480(%rax)
	movslq	28(%rbx), %rdx
	movzwl	544(%rbx,%rdx,2), %edx
	movl	%edx, 484(%rax)
	jmp	.L341
.L364:
	movq	8(%rbx), %rax
	movslq	%r14d, %rsi
	leaq	328(%rax), %rdi
	call	utext_setNativeIndex_67@PLT
	movq	8(%rbx), %rax
	leaq	328(%rax), %rdi
	call	utext_getPreviousNativeIndex_67@PLT
	cmpq	%rax, %r15
	jne	.L333
	movq	8(%rbx), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv@PLT
	movl	%eax, %r14d
	jmp	.L333
.L365:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode.part.0
	movl	28(%rbx), %eax
	movq	8(%rbx), %rdx
	jmp	.L352
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0.cold, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0.cold:
.LFSB4242:
.L357:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE4242:
	.text
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0
	.section	.text.unlikely
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0.cold, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0.cold
.LCOLDE4:
	.text
.LHOTE4:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode:
.LFB3184:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L367
	jmp	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L367:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3184:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9precedingEiR10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9precedingEiR10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache9precedingEiR10UErrorCode:
.LFB3180:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L391
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	subq	$16, %rsp
	cmpl	%esi, 24(%rdi)
	jne	.L394
.L382:
	movl	28(%rdi), %r12d
	cmpl	16(%rdi), %r12d
	je	.L395
	leal	-1(%r12), %eax
	andl	$127, %eax
	movslq	%eax, %rdx
	movl	%eax, 28(%rdi)
	movl	32(%rdi,%rdx,4), %edx
	movl	%edx, 24(%rdi)
.L383:
	movq	8(%rdi), %rdx
	cmpl	%eax, %r12d
	sete	632(%rdx)
	movl	24(%rdi), %edx
	movq	8(%rdi), %rax
	movl	%edx, 480(%rax)
	movslq	28(%rdi), %rdx
	movzwl	544(%rdi,%rdx,2), %edx
	movl	%edx, 484(%rax)
.L368:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movslq	16(%rdi), %rax
	movq	%rax, %rdx
	movl	32(%rdi,%rax,4), %eax
	cmpl	%eax, %esi
	jl	.L372
	movslq	20(%rdi), %rcx
	movq	%rcx, %rsi
	movl	32(%rdi,%rcx,4), %ecx
	cmpl	%ecx, %r12d
	jg	.L372
	cmpl	%eax, %r12d
	je	.L396
	cmpl	%ecx, %r12d
	jne	.L377
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L399:
	leal	(%rsi,%rdx), %ecx
	leal	128(%rcx), %eax
	cmovl	%eax, %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movl	%eax, %ecx
	andl	$127, %eax
	andl	$127, %ecx
	cmpl	32(%rdi,%rax,4), %r12d
	jge	.L398
	movl	%ecx, %esi
.L377:
	cmpl	%edx, %esi
	jne	.L399
	subl	$1, %esi
	andl	$127, %esi
	movl	%esi, 28(%rdi)
	movslq	%esi, %rsi
	movl	32(%rdi,%rsi,4), %edx
	movl	%edx, 24(%rdi)
	cmpl	%edx, %r12d
	je	.L382
.L378:
	movq	8(%rdi), %rax
	movl	%edx, 480(%rax)
	movslq	28(%rdi), %rdx
	movzwl	544(%rdi,%rdx,2), %edx
	movb	$0, 632(%rax)
	movl	%edx, 484(%rax)
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rdi, -24(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0
	testb	%al, %al
	je	.L368
	movq	-24(%rbp), %rdi
	movl	24(%rdi), %edx
	cmpl	%r12d, %edx
	jne	.L378
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L368
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L398:
	leal	1(%rcx), %edx
	andl	$127, %edx
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L396:
	movl	%edx, 28(%rdi)
	movl	%r12d, 24(%rdi)
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L397:
	movl	%esi, 28(%rdi)
	movl	%r12d, 24(%rdi)
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r13, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode.part.0
	movq	-24(%rbp), %rdi
	movl	28(%rdi), %eax
	jmp	.L383
	.cfi_endproc
.LFE3180:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9precedingEiR10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache9precedingEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9followingEiR10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9followingEiR10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache9followingEiR10UErrorCode:
.LFB3179:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L417
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	%esi, 24(%rdi)
	je	.L404
	movslq	16(%rdi), %rax
	movq	%rax, %rdi
	movl	32(%rbx,%rax,4), %eax
	cmpl	%eax, %esi
	jl	.L405
	movslq	20(%rbx), %r8
	movq	%r8, %rcx
	movl	32(%rbx,%r8,4), %r8d
	cmpl	%r8d, %esi
	jg	.L405
	cmpl	%eax, %esi
	je	.L420
	cmpl	%r8d, %esi
	jne	.L410
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L423:
	leal	(%rcx,%rdi), %edx
	leal	128(%rdx), %eax
	cmovl	%eax, %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, %edx
	andl	$127, %eax
	andl	$127, %edx
	cmpl	32(%rbx,%rax,4), %esi
	jge	.L422
	movl	%edx, %ecx
.L410:
	cmpl	%edi, %ecx
	jne	.L423
	subl	$1, %ecx
	andl	$127, %ecx
	movl	%ecx, 28(%rbx)
	movslq	%ecx, %rcx
	movl	32(%rbx,%rcx,4), %eax
	movl	%eax, 24(%rbx)
.L404:
	movq	8(%rbx), %rax
	movb	$0, 632(%rax)
	movl	28(%rbx), %eax
	cmpl	20(%rbx), %eax
	je	.L424
	addl	$1, %eax
	movq	8(%rbx), %rdx
	andl	$127, %eax
	movl	%eax, 28(%rbx)
	cltq
	movl	32(%rbx,%rax,4), %eax
	movl	%eax, 480(%rdx)
	movl	%eax, 24(%rbx)
	movslq	28(%rbx), %rax
	movzwl	544(%rbx,%rax,2), %eax
	movl	%eax, 484(%rdx)
.L400:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	%rbx, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode.part.0
	testb	%al, %al
	je	.L400
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L422:
	leal	1(%rdx), %edi
	andl	$127, %edi
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rbx, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populateFollowingEv
	movq	8(%rbx), %rdx
	testb	%al, %al
	sete	632(%rdx)
	movl	24(%rbx), %edx
	movq	8(%rbx), %rax
	movl	%edx, 480(%rax)
	movslq	28(%rbx), %rdx
	movzwl	544(%rbx,%rdx,2), %edx
	movl	%edx, 484(%rax)
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L420:
	movl	%edi, 28(%rbx)
	movl	%esi, 24(%rbx)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L421:
	movl	%ecx, 28(%rbx)
	movl	%esi, 24(%rbx)
	jmp	.L404
	.cfi_endproc
.LFE3179:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9followingEiR10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache9followingEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode:
.LFB3186:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L426
	jmp	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L426:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3186:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache17populatePrecedingER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12addFollowingEiiNS1_20UpdatePositionValuesE
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12addFollowingEiiNS1_20UpdatePositionValuesE, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache12addFollowingEiiNS1_20UpdatePositionValuesE:
.LFB3187:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	addl	$1, %eax
	andl	$127, %eax
	cmpl	%eax, 16(%rdi)
	jne	.L428
	leal	6(%rax), %r8d
	andl	$127, %r8d
	movl	%r8d, 16(%rdi)
.L428:
	movslq	%eax, %r8
	movl	%esi, 32(%rdi,%r8,4)
	movw	%dx, 544(%rdi,%r8,2)
	movl	%eax, 20(%rdi)
	cmpl	$1, %ecx
	je	.L430
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	movl	%eax, 28(%rdi)
	movl	%esi, 24(%rdi)
	ret
	.cfi_endproc
.LFE3187:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12addFollowingEiiNS1_20UpdatePositionValuesE, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache12addFollowingEiiNS1_20UpdatePositionValuesE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12addPrecedingEiiNS1_20UpdatePositionValuesE
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12addPrecedingEiiNS1_20UpdatePositionValuesE, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache12addPrecedingEiiNS1_20UpdatePositionValuesE:
.LFB3188:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	movl	20(%rdi), %r8d
	subl	$1, %eax
	andl	$127, %eax
	cmpl	%eax, %r8d
	je	.L441
.L432:
	movslq	%eax, %r8
	movl	%esi, 32(%rdi,%r8,4)
	movw	%dx, 544(%rdi,%r8,2)
	movl	$1, %r8d
	movl	%eax, 16(%rdi)
	cmpl	$1, %ecx
	jne	.L431
	movl	%eax, 28(%rdi)
	movl	%esi, 24(%rdi)
.L431:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	cmpl	%r8d, 28(%rdi)
	jne	.L437
	testl	%ecx, %ecx
	je	.L435
.L437:
	subl	$1, %r8d
	andl	$127, %r8d
	movl	%r8d, 20(%rdi)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L435:
	xorl	%r8d, %r8d
	jmp	.L431
	.cfi_endproc
.LFE3188:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12addPrecedingEiiNS1_20UpdatePositionValuesE, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache12addPrecedingEiiNS1_20UpdatePositionValuesE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9dumpCacheEv
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9dumpCacheEv, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCache9dumpCacheEv:
.LFB3189:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3189:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9dumpCacheEv, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCache9dumpCacheEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheD2Ev
	.type	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheD2Ev, @function
_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheD2Ev:
.LFB3164:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZN6icu_679UVector32D1Ev@PLT
	.cfi_endproc
.LFE3164:
	.size	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheD2Ev, .-_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheD2Ev
	.globl	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheD1Ev
	.set	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheD1Ev,_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD2Ev
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD2Ev, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD2Ev:
.LFB3174:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIterator10BreakCacheE(%rip), %rax
	addq	$800, %rdi
	movq	%rax, -800(%rdi)
	jmp	_ZN6icu_679UVector32D1Ev@PLT
	.cfi_endproc
.LFE3174:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD2Ev, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD2Ev
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD1Ev
	.set	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD1Ev,_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD0Ev
	.type	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD0Ev, @function
_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD0Ev:
.LFB3176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIterator10BreakCacheE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	800(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -800(%rdi)
	call	_ZN6icu_679UVector32D1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3176:
	.size	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD0Ev, .-_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD0Ev
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6722RuleBasedBreakIterator10BreakCacheE
	.section	.rodata._ZTSN6icu_6722RuleBasedBreakIterator10BreakCacheE,"aG",@progbits,_ZTSN6icu_6722RuleBasedBreakIterator10BreakCacheE,comdat
	.align 32
	.type	_ZTSN6icu_6722RuleBasedBreakIterator10BreakCacheE, @object
	.size	_ZTSN6icu_6722RuleBasedBreakIterator10BreakCacheE, 46
_ZTSN6icu_6722RuleBasedBreakIterator10BreakCacheE:
	.string	"N6icu_6722RuleBasedBreakIterator10BreakCacheE"
	.weak	_ZTIN6icu_6722RuleBasedBreakIterator10BreakCacheE
	.section	.data.rel.ro._ZTIN6icu_6722RuleBasedBreakIterator10BreakCacheE,"awG",@progbits,_ZTIN6icu_6722RuleBasedBreakIterator10BreakCacheE,comdat
	.align 8
	.type	_ZTIN6icu_6722RuleBasedBreakIterator10BreakCacheE, @object
	.size	_ZTIN6icu_6722RuleBasedBreakIterator10BreakCacheE, 24
_ZTIN6icu_6722RuleBasedBreakIterator10BreakCacheE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722RuleBasedBreakIterator10BreakCacheE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTVN6icu_6722RuleBasedBreakIterator10BreakCacheE
	.section	.data.rel.ro.local._ZTVN6icu_6722RuleBasedBreakIterator10BreakCacheE,"awG",@progbits,_ZTVN6icu_6722RuleBasedBreakIterator10BreakCacheE,comdat
	.align 8
	.type	_ZTVN6icu_6722RuleBasedBreakIterator10BreakCacheE, @object
	.size	_ZTVN6icu_6722RuleBasedBreakIterator10BreakCacheE, 32
_ZTVN6icu_6722RuleBasedBreakIterator10BreakCacheE:
	.quad	0
	.quad	_ZTIN6icu_6722RuleBasedBreakIterator10BreakCacheE
	.quad	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD1Ev
	.quad	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheD0Ev
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	-1
	.long	0
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
