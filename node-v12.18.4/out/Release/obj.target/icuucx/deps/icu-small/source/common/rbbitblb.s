	.file	"rbbitblb.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE.part.0, @function
_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE.part.0:
.LFB4275:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L2
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L17
	cmpl	$6, %eax
	je	.L17
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L31
	call	_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE.part.0
.L2:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L6
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L18
	cmpl	$6, %eax
	je	.L18
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L32
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE.part.0
.L6:
	movl	(%rbx), %eax
	cmpl	$9, %eax
	je	.L33
.L10:
	cmpl	$8, %eax
	je	.L34
	subl	$10, %eax
	andl	$-3, %eax
	sete	120(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movb	$1, 120(%rsi)
	movl	(%rbx), %eax
	cmpl	$9, %eax
	jne	.L10
.L33:
	movq	16(%rbx), %rdx
	movl	$1, %eax
	cmpb	$0, 120(%rdx)
	je	.L29
.L14:
	movb	%al, 120(%rbx)
.L35:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movb	$1, 120(%rsi)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L34:
	movq	16(%rbx), %rax
	movzbl	120(%rax), %eax
	testb	%al, %al
	je	.L14
.L29:
	movq	24(%rbx), %rax
	cmpb	$0, 120(%rax)
	setne	%al
	movb	%al, 120(%rbx)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L18:
	movb	$0, 120(%rsi)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L17:
	movb	$0, 120(%rsi)
	jmp	.L2
	.cfi_endproc
.LFE4275:
	.size	_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE.part.0, .-_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilderC2EPNS_15RBBIRuleBuilderEPPNS_8RBBINodeER10UErrorCode
	.type	_ZN6icu_6716RBBITableBuilderC2EPNS_15RBBIRuleBuilderEPPNS_8RBBINodeER10UErrorCode, @function
_ZN6icu_6716RBBITableBuilderC2EPNS_15RBBIRuleBuilderEPPNS_8RBBINodeER10UErrorCode:
.LFB3162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rcx), %eax
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	%rcx, 16(%rdi)
	movq	$0, 24(%rdi)
	movups	%xmm0, 32(%rdi)
	testl	%eax, %eax
	jle	.L43
.L36:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	$40, %edi
	movq	%rcx, %r12
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L39
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	%r13, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L39:
	.cfi_restore_state
	cmpl	$0, (%r12)
	movq	$0, 24(%rbx)
	jg	.L36
	movl	$7, (%r12)
	jmp	.L36
	.cfi_endproc
.LFE3162:
	.size	_ZN6icu_6716RBBITableBuilderC2EPNS_15RBBIRuleBuilderEPPNS_8RBBINodeER10UErrorCode, .-_ZN6icu_6716RBBITableBuilderC2EPNS_15RBBIRuleBuilderEPPNS_8RBBINodeER10UErrorCode
	.globl	_ZN6icu_6716RBBITableBuilderC1EPNS_15RBBIRuleBuilderEPPNS_8RBBINodeER10UErrorCode
	.set	_ZN6icu_6716RBBITableBuilderC1EPNS_15RBBIRuleBuilderEPPNS_8RBBINodeER10UErrorCode,_ZN6icu_6716RBBITableBuilderC2EPNS_15RBBIRuleBuilderEPPNS_8RBBINodeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilderD2Ev
	.type	_ZN6icu_6716RBBITableBuilderD2Ev, @function
_ZN6icu_6716RBBITableBuilderD2Ev:
.LFB3165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L45
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L50:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L46
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	je	.L47
	movq	(%rdi), %rax
	call	*8(%rax)
.L47:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L48
	movq	(%rdi), %rax
	call	*8(%rax)
.L48:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	(%rdi), %rax
	call	*8(%rax)
.L49:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L46:
	movq	24(%r13), %rdi
	addl	$1, %ebx
	cmpl	8(%rdi), %ebx
	jl	.L50
.L45:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L51
	movq	(%rdi), %rax
	call	*8(%rax)
.L51:
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.L44
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3165:
	.size	_ZN6icu_6716RBBITableBuilderD2Ev, .-_ZN6icu_6716RBBITableBuilderD2Ev
	.globl	_ZN6icu_6716RBBITableBuilderD1Ev
	.set	_ZN6icu_6716RBBITableBuilderD1Ev,_ZN6icu_6716RBBITableBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE
	.type	_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE, @function
_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE:
.LFB3168:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L108
	movl	(%rsi), %eax
	movq	%rsi, %rcx
	testl	%eax, %eax
	je	.L109
	cmpl	$6, %eax
	je	.L109
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L113
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L75
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L90
	cmpl	$6, %eax
	je	.L90
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L114
	movq	%rdi, -8(%rbp)
	call	_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE.part.0
	movq	-8(%rbp), %rdi
.L75:
	movq	24(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L79
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L91
	cmpl	$6, %eax
	je	.L91
	subl	$4, %eax
	cmpl	$1, %eax
	jbe	.L115
	call	_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE.part.0
.L79:
	movl	(%rcx), %eax
	cmpl	$9, %eax
	je	.L116
	cmpl	$8, %eax
	je	.L117
	subl	$10, %eax
	andl	$-3, %eax
	jne	.L118
	movb	$1, 120(%rcx)
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore 6
	movb	$1, 120(%rsi)
.L108:
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	16(%rcx), %rax
	movzbl	120(%rax), %eax
	testb	%al, %al
	je	.L86
.L112:
	movq	24(%rcx), %rax
	cmpb	$0, 120(%rax)
	setne	%al
.L86:
	movb	%al, 120(%rcx)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movb	$0, 120(%rsi)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L90:
	movb	$0, 120(%rsi)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L118:
	movb	$0, 120(%rcx)
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore 6
	movb	$0, 120(%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	16(%rcx), %rdx
	movl	$1, %eax
	cmpb	$0, 120(%rdx)
	jne	.L86
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L115:
	movb	$1, 120(%rsi)
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L114:
	movb	$1, 120(%rsi)
	jmp	.L75
	.cfi_endproc
.LFE3168:
	.size	_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE, .-_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder16addRuleRootNodesEPNS_7UVectorEPNS_8RBBINodeE
	.type	_ZN6icu_6716RBBITableBuilder16addRuleRootNodesEPNS_7UVectorEPNS_8RBBINodeE, @function
_ZN6icu_6716RBBITableBuilder16addRuleRootNodesEPNS_7UVectorEPNS_8RBBINodeE:
.LFB3172:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L128
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
.L122:
	movq	16(%rbx), %rdx
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L119
	cmpb	$0, 129(%r12)
	jne	.L131
	movq	16(%r12), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6716RBBITableBuilder16addRuleRootNodesEPNS_7UVectorEPNS_8RBBINodeE
	movq	24(%r12), %r12
	testq	%r12, %r12
	jne	.L122
.L119:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L128:
	ret
	.cfi_endproc
.LFE3172:
	.size	_ZN6icu_6716RBBITableBuilder16addRuleRootNodesEPNS_7UVectorEPNS_8RBBINodeE, .-_ZN6icu_6716RBBITableBuilder16addRuleRootNodesEPNS_7UVectorEPNS_8RBBINodeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder17mapLookAheadRulesEv
	.type	_ZN6icu_6716RBBITableBuilder17mapLookAheadRulesEv, @function
_ZN6icu_6716RBBITableBuilder17mapLookAheadRulesEv:
.LFB3176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	104(%rax), %rdi
	call	_ZN6icu_6715RBBIRuleScanner8numRulesEv@PLT
	movl	$32, %edi
	leal	1(%rax), %r14d
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L133
	movq	16(%r13), %rdx
	movq	%rax, %rdi
	movl	%r14d, %esi
	movq	%rax, %r12
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movq	16(%r13), %rax
	movq	%r12, 40(%r13)
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L154
.L132:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	104(%rax), %rdi
	call	_ZN6icu_6715RBBIRuleScanner8numRulesEv@PLT
	movq	%r12, %rdi
	leal	1(%rax), %esi
	call	_ZN6icu_679UVector327setSizeEi@PLT
	movq	24(%r13), %rdi
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jle	.L132
	movl	$0, -56(%rbp)
	movl	$0, -52(%rbp)
	.p2align 4,,10
	.p2align 3
.L142:
	movl	-52(%rbp), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	32(%rax), %rdi
	movq	%rax, %rbx
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L135
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L137:
	movl	%r14d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpl	$4, (%rax)
	jne	.L136
	movslq	124(%rax), %rax
	movl	$1, %r12d
	testl	%eax, %eax
	js	.L136
	movq	40(%r13), %rsi
	movl	8(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L136
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L136
	movq	24(%rsi), %rcx
	movl	(%rcx,%rax,4), %ecx
	testl	%ecx, %ecx
	setne	%r12b
	testl	%r15d, %r15d
	sete	%al
	andb	%al, %r12b
	je	.L155
	movl	%ecx, %r15d
	.p2align 4,,10
	.p2align 3
.L136:
	movq	32(%rbx), %rdi
	addl	$1, %r14d
	movl	8(%rdi), %eax
	cmpl	%r14d, %eax
	jg	.L137
	testb	%r12b, %r12b
	je	.L135
	testl	%r15d, %r15d
	jne	.L138
	addl	$1, -56(%rbp)
	movl	-56(%rbp), %edx
	movl	%edx, %r15d
.L138:
	testl	%eax, %eax
	jle	.L135
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L141:
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpl	$4, (%rax)
	je	.L156
	movq	32(%rbx), %rdi
	addl	$1, %r12d
	cmpl	%r12d, 8(%rdi)
	jg	.L141
.L135:
	movq	24(%r13), %rdi
	addl	$1, -52(%rbp)
	movl	-52(%rbp), %eax
	cmpl	%eax, 8(%rdi)
	jg	.L142
.L157:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	40(%r13), %rdi
	movl	124(%rax), %edx
	movl	%r15d, %esi
	addl	$1, %r12d
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	32(%rbx), %rdi
	cmpl	8(%rdi), %r12d
	jl	.L141
	movq	24(%r13), %rdi
	addl	$1, -52(%rbp)
	movl	-52(%rbp), %eax
	cmpl	%eax, 8(%rdi)
	jg	.L142
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$1, %r12d
	jmp	.L136
.L133:
	movq	16(%r13), %rax
	movq	$0, 40(%r13)
	movl	$7, (%rax)
	jmp	.L132
	.cfi_endproc
.LFE3176:
	.size	_ZN6icu_6716RBBITableBuilder17mapLookAheadRulesEv, .-_ZN6icu_6716RBBITableBuilder17mapLookAheadRulesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder19mergeRuleStatusValsEv
	.type	_ZN6icu_6716RBBITableBuilder19mergeRuleStatusValsEv, @function
_ZN6icu_6716RBBITableBuilder19mergeRuleStatusValsEv:
.LFB3180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	184(%rax), %rdi
	movl	8(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L184
.L159:
	movq	24(%r14), %rdi
	movl	$0, -60(%rbp)
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jle	.L158
	.p2align 4,,10
	.p2align 3
.L160:
	movl	-60(%rbp), %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	16(%rax), %r12
	movq	%rax, -72(%rbp)
	testq	%r12, %r12
	je	.L185
	movl	$-1, 24(%rax)
	movq	(%r14), %rax
	xorl	%r13d, %r13d
	movq	184(%rax), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jg	.L163
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L166:
	movq	(%r14), %rax
	movq	184(%rax), %rdi
	cmpl	8(%rdi), %r13d
	jge	.L186
.L163:
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	movl	%r13d, -56(%rbp)
	movl	%r13d, %esi
	movl	8(%r12), %ebx
	leal	1(%r13,%rax), %r13d
	movq	(%r14), %rax
	movq	184(%rax), %rdi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	cmpl	%ebx, %eax
	jne	.L166
	movl	8(%r12), %eax
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.L167
	movl	-56(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -52(%rbp)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L168:
	movl	8(%r12), %eax
	addl	$1, %r15d
	cmpl	%eax, %r15d
	jge	.L167
.L169:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	movl	%eax, %ebx
	movl	-52(%rbp), %eax
	leal	(%rax,%r15), %esi
	movq	(%r14), %rax
	movq	184(%rax), %rdi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	cmpl	%eax, %ebx
	je	.L168
	movl	8(%r12), %eax
.L167:
	cmpl	%eax, %r15d
	jne	.L166
	movq	-72(%rbp), %rax
	movl	-56(%rbp), %ecx
	cmpl	$-1, -56(%rbp)
	movl	%ecx, 24(%rax)
	je	.L187
	.p2align 4,,10
	.p2align 3
.L162:
	movq	24(%r14), %rdi
	addl	$1, -60(%rbp)
	movl	-60(%rbp), %eax
	cmpl	8(%rdi), %eax
	jl	.L160
.L158:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	movl	24(%rax), %eax
	movl	%eax, -56(%rbp)
	cmpl	$-1, -56(%rbp)
	jne	.L162
.L187:
	movq	(%r14), %rax
	movq	184(%rax), %rdi
	movl	8(%rdi), %eax
.L164:
	movq	-72(%rbp), %rcx
	movq	16(%r14), %rdx
	movl	%eax, 24(%rcx)
	movl	8(%r12), %esi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movl	8(%r12), %eax
	testl	%eax, %eax
	jle	.L162
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L172:
	movq	(%r14), %rax
	movl	%ebx, %esi
	movq	16(%r14), %r15
	movq	%r12, %rdi
	addl	$1, %ebx
	movq	184(%rax), %r13
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	movq	%r15, %rdx
	movl	%eax, %esi
	movq	%r13, %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	cmpl	8(%r12), %ebx
	jl	.L172
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L185:
	movl	$0, 24(%rax)
	jmp	.L162
.L184:
	movq	16(%r14), %rdx
	movl	$1, %esi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	movq	(%r14), %rax
	movq	16(%r14), %rdx
	xorl	%esi, %esi
	movq	184(%rax), %rdi
	call	_ZN6icu_677UVector10addElementEiR10UErrorCode@PLT
	jmp	.L159
	.cfi_endproc
.LFE3180:
	.size	_ZN6icu_6716RBBITableBuilder19mergeRuleStatusValsEv, .-_ZN6icu_6716RBBITableBuilder19mergeRuleStatusValsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder9sortedAddEPPNS_7UVectorEi
	.type	_ZN6icu_6716RBBITableBuilder9sortedAddEPPNS_7UVectorEi, @function
_ZN6icu_6716RBBITableBuilder9sortedAddEPPNS_7UVectorEi:
.LFB3181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r15
	testq	%r15, %r15
	je	.L200
.L189:
	movq	16(%r14), %rcx
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L188
	movl	8(%r15), %ebx
	xorl	%r12d, %r12d
	testl	%ebx, %ebx
	jg	.L195
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L201:
	jg	.L199
	addl	$1, %r12d
	cmpl	%ebx, %r12d
	je	.L199
.L195:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	cmpl	%r13d, %eax
	jne	.L201
.L188:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	16(%r14), %rcx
.L193:
	addq	$8, %rsp
	movl	%r12d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UVector15insertElementAtEiiR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movl	$40, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L190
	movq	16(%r14), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	%r15, (%rbx)
	jmp	.L189
.L190:
	movq	$0, (%rbx)
	jmp	.L188
	.cfi_endproc
.LFE3181:
	.size	_ZN6icu_6716RBBITableBuilder9sortedAddEPPNS_7UVectorEi, .-_ZN6icu_6716RBBITableBuilder9sortedAddEPPNS_7UVectorEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	.type	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_, @function
_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_:
.LFB3182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-336(%rbp), %rbx
	subq	$360, %rsp
	movslq	8(%rsi), %r15
	movl	8(%rax), %r14d
	movq	%rdx, -368(%rbp)
	movq	%rdi, -392(%rbp)
	leaq	-192(%rbp), %rax
	movq	%r15, %rdx
	salq	$3, %r15
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%rax, -208(%rbp)
	movl	$16, %eax
	movq	%rbx, -352(%rbp)
	movl	$16, -344(%rbp)
	movb	$0, -340(%rbp)
	movl	$16, -200(%rbp)
	movb	$0, -196(%rbp)
	cmpl	$16, %edx
	jg	.L242
	cmpl	%r14d, %eax
	jge	.L243
.L206:
	movl	%edx, -360(%rbp)
	testl	%r14d, %r14d
	jle	.L204
	movslq	%r14d, %rsi
	salq	$3, %rsi
	movq	%rsi, %rdi
	movq	%rsi, -376(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L204
	cmpb	$0, -196(%rbp)
	movq	-376(%rbp), %rsi
	movl	-360(%rbp), %edx
	jne	.L244
.L208:
	movq	%r12, -208(%rbp)
	movl	%r14d, -200(%rbp)
	movb	$1, -196(%rbp)
.L207:
	leaq	(%rbx,%r15), %rax
	movq	%r13, %rdi
	leaq	(%r12,%rsi), %r15
	movq	%rbx, %rsi
	movl	%edx, -376(%rbp)
	movq	%rax, -360(%rbp)
	call	_ZNK6icu_677UVector7toArrayEPPv@PLT
	movq	-368(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_677UVector7toArrayEPPv@PLT
	movq	-392(%rbp), %rdi
	movl	-376(%rbp), %edx
	movq	16(%rdi), %r8
	leal	(%rdx,%r14), %esi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	movq	%r8, %rdx
	call	_ZN6icu_677UVector7setSizeEiR10UErrorCode@PLT
	cmpq	-360(%rbp), %rbx
	jnb	.L209
	cmpq	%r12, %r15
	ja	.L210
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L216:
	movl	%r10d, %edx
	movq	%r9, %rsi
	movq	%r13, %rdi
	addq	$8, %r12
	call	_ZN6icu_677UVector12setElementAtEPvi@PLT
.L215:
	cmpq	%r15, %r12
	jnb	.L209
.L246:
	cmpq	-360(%rbp), %rbx
	jnb	.L209
.L210:
	movq	(%rbx), %r8
	movq	(%r12), %r9
	movl	%r14d, %r10d
	addl	$1, %r14d
	cmpq	%r9, %r8
	je	.L245
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%r10d, -376(%rbp)
	movq	%r9, -368(%rbp)
	movq	%r8, -384(%rbp)
	call	memcmp@PLT
	movq	-368(%rbp), %r9
	movl	-376(%rbp), %r10d
	testl	%eax, %eax
	jns	.L216
	movq	-384(%rbp), %r8
	movl	%r10d, %edx
	movq	%r13, %rdi
	addq	$8, %rbx
	movq	%r8, %rsi
	call	_ZN6icu_677UVector12setElementAtEPvi@PLT
	cmpq	%r15, %r12
	jb	.L246
	.p2align 4,,10
	.p2align 3
.L209:
	cmpq	%rbx, -360(%rbp)
	jbe	.L212
	movq	%rbx, %rax
	movl	%r14d, %ecx
	.p2align 4,,10
	.p2align 3
.L213:
	movq	(%rax), %rsi
	movl	%ecx, %edx
	addq	$8, %rax
	addl	$1, %ecx
	movq	%r13, %rdi
	movl	%ecx, -376(%rbp)
	movq	%rax, -368(%rbp)
	call	_ZN6icu_677UVector12setElementAtEPvi@PLT
	movq	-368(%rbp), %rax
	cmpq	%rax, -360(%rbp)
	movl	-376(%rbp), %ecx
	ja	.L213
	movq	-360(%rbp), %rax
	subq	$1, %rax
	subq	%rbx, %rax
	shrq	$3, %rax
	leal	1(%r14,%rax), %r14d
.L212:
	cmpq	%r12, %r15
	jbe	.L217
	movq	%r12, %rbx
	movl	%r14d, %eax
	.p2align 4,,10
	.p2align 3
.L218:
	movq	(%rbx), %rsi
	movl	%eax, %edx
	addq	$8, %rbx
	addl	$1, %eax
	movq	%r13, %rdi
	movl	%eax, -360(%rbp)
	call	_ZN6icu_677UVector12setElementAtEPvi@PLT
	cmpq	%rbx, %r15
	movl	-360(%rbp), %eax
	ja	.L218
	subq	$1, %r15
	subq	%r12, %r15
	shrq	$3, %r15
	leal	1(%r14,%r15), %r14d
.L217:
	movq	-392(%rbp), %rax
	movl	%r14d, %esi
	movq	%r13, %rdi
	movq	16(%rax), %rdx
	call	_ZN6icu_677UVector7setSizeEiR10UErrorCode@PLT
	cmpb	$0, -196(%rbp)
	je	.L222
.L241:
	movq	-208(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -340(%rbp)
	je	.L202
.L247:
	movq	-352(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L204:
	cmpb	$0, -196(%rbp)
	jne	.L241
.L222:
	cmpb	$0, -340(%rbp)
	jne	.L247
.L202:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movl	%r10d, %edx
	movq	%r8, %rsi
	movq	%r13, %rdi
	addq	$8, %r12
	call	_ZN6icu_677UVector12setElementAtEPvi@PLT
	addq	$8, %rbx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r15, %rdi
	movl	%edx, -360(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L204
	cmpb	$0, -340(%rbp)
	movl	-360(%rbp), %edx
	jne	.L249
.L205:
	movl	-200(%rbp), %eax
	movq	%rbx, -352(%rbp)
	movl	%edx, -344(%rbp)
	movb	$1, -340(%rbp)
	cmpl	%r14d, %eax
	jl	.L206
.L243:
	movslq	%r14d, %rsi
	movq	-208(%rbp), %r12
	salq	$3, %rsi
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L244:
	movq	-208(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-376(%rbp), %rsi
	movl	-360(%rbp), %edx
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L249:
	movq	-352(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	-360(%rbp), %edx
	jmp	.L205
.L248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3182:
	.size	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_, .-_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	.align 2
	.p2align 4
	.type	_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE.part.0, @function
_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE.part.0:
.LFB4281:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L251
	movl	(%rsi), %eax
	subl	$3, %eax
	cmpl	$3, %eax
	ja	.L252
	movq	16(%rdi), %rdx
	movq	136(%rsi), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L251:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L253
	movl	(%rsi), %eax
	subl	$3, %eax
	cmpl	$3, %eax
	ja	.L254
	movq	16(%r12), %rdx
	movq	136(%rsi), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L253:
	movl	(%rbx), %eax
	cmpl	$9, %eax
	je	.L268
.L255:
	cmpl	$8, %eax
	je	.L269
	subl	$10, %eax
	cmpl	$2, %eax
	ja	.L250
	movq	16(%rbx), %rax
.L267:
	movq	136(%rbx), %rsi
	movq	136(%rax), %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	call	_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE.part.0
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE.part.0
	movl	(%rbx), %eax
	cmpl	$9, %eax
	jne	.L255
.L268:
	movq	16(%rbx), %rax
	movq	136(%rbx), %rsi
	movq	%r12, %rdi
	movq	136(%rax), %rdx
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	movq	24(%rbx), %rax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L250:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	136(%rbx), %rsi
	movq	%r12, %rdi
	movq	136(%rax), %rdx
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	movq	16(%rbx), %rax
	cmpb	$0, 120(%rax)
	je	.L250
	movq	24(%rbx), %rax
	jmp	.L267
	.cfi_endproc
.LFE4281:
	.size	_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE.part.0, .-_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE
	.type	_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE, @function
_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE:
.LFB3169:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L270
	movl	(%rsi), %eax
	subl	$3, %eax
	cmpl	$3, %eax
	ja	.L272
	movq	16(%rdi), %rdx
	movq	136(%rsi), %rdi
	jmp	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	jmp	_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE.part.0
	.p2align 4,,10
	.p2align 3
.L270:
	ret
	.cfi_endproc
.LFE3169:
	.size	_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE, .-_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE
	.align 2
	.p2align 4
	.type	_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE.part.0, @function
_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE.part.0:
.LFB4282:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L274
	movl	(%rsi), %eax
	subl	$3, %eax
	cmpl	$3, %eax
	ja	.L275
	movq	16(%rdi), %rdx
	movq	144(%rsi), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L274:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L276
	movl	(%rsi), %eax
	subl	$3, %eax
	cmpl	$3, %eax
	ja	.L277
	movq	16(%r12), %rdx
	movq	144(%rsi), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L276:
	movl	(%rbx), %eax
	cmpl	$9, %eax
	je	.L292
.L278:
	cmpl	$8, %eax
	je	.L293
	subl	$10, %eax
	cmpl	$2, %eax
	ja	.L273
.L291:
	movq	16(%rbx), %rax
.L290:
	movq	144(%rbx), %rsi
	movq	144(%rax), %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	call	_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE.part.0
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE.part.0
	movl	(%rbx), %eax
	cmpl	$9, %eax
	jne	.L278
.L292:
	movq	16(%rbx), %rax
	movq	144(%rbx), %rsi
	movq	%r12, %rdi
	movq	144(%rax), %rdx
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	movq	24(%rbx), %rax
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L293:
	movq	24(%rbx), %rax
	movq	144(%rbx), %rsi
	movq	%r12, %rdi
	movq	144(%rax), %rdx
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	movq	24(%rbx), %rax
	cmpb	$0, 120(%rax)
	jne	.L291
.L273:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4282:
	.size	_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE.part.0, .-_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE
	.type	_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE, @function
_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE:
.LFB3170:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L294
	movl	(%rsi), %eax
	subl	$3, %eax
	cmpl	$3, %eax
	ja	.L296
	movq	16(%rdi), %rdx
	movq	144(%rsi), %rdi
	jmp	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	jmp	_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE.part.0
	.p2align 4,,10
	.p2align 3
.L294:
	ret
	.cfi_endproc
.LFE3170:
	.size	_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE, .-_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE
	.align 2
	.p2align 4
	.type	_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE.part.0, @function
_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE.part.0:
.LFB4283:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L298
	movl	(%rsi), %eax
	cmpl	$3, %eax
	je	.L298
	cmpl	$6, %eax
	je	.L298
	call	_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE.part.0
.L298:
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L299
	movl	(%rsi), %eax
	cmpl	$3, %eax
	je	.L299
	cmpl	$6, %eax
	je	.L299
	movq	%r13, %rdi
	call	_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE.part.0
.L299:
	movl	(%rbx), %eax
	cmpl	$8, %eax
	je	.L315
.L300:
	subl	$10, %eax
	cmpl	$1, %eax
	jbe	.L316
.L297:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movq	144(%rbx), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	je	.L297
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L303:
	movl	%r12d, %esi
	addl	$1, %r12d
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	136(%rbx), %rdx
	movq	%r13, %rdi
	movq	152(%rax), %rsi
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	movq	144(%rbx), %rdi
	cmpl	%r12d, 8(%rdi)
	ja	.L303
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movq	16(%rbx), %rax
	xorl	%r12d, %r12d
	movq	144(%rax), %r14
	movl	8(%r14), %edx
	testl	%edx, %edx
	je	.L297
	.p2align 4,,10
	.p2align 3
.L301:
	movl	%r12d, %esi
	movq	%r14, %rdi
	addl	$1, %r12d
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	24(%rbx), %rdx
	movq	%r13, %rdi
	movq	152(%rax), %rsi
	movq	136(%rdx), %rdx
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	cmpl	%r12d, 8(%r14)
	ja	.L301
	movl	(%rbx), %eax
	jmp	.L300
	.cfi_endproc
.LFE4283:
	.size	_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE.part.0, .-_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE
	.type	_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE, @function
_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE:
.LFB3171:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L317
	movl	(%rsi), %eax
	cmpl	$6, %eax
	je	.L317
	cmpl	$3, %eax
	je	.L317
	jmp	_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE.part.0
	.p2align 4,,10
	.p2align 3
.L317:
	ret
	.cfi_endproc
.LFE3171:
	.size	_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE, .-_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE
	.align 2
	.p2align 4
	.type	_ZN6icu_6716RBBITableBuilder15buildStateTableEv.part.0, @function
_ZN6icu_6716RBBITableBuilder15buildStateTableEv.part.0:
.LFB4285:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv@PLT
	movl	$48, %edi
	movl	%eax, -56(%rbp)
	movl	%eax, %r15d
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L323
	movq	16(%r12), %rbx
	movb	$0, (%rax)
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	$0, 4(%rax)
	movq	%rax, %r13
	movl	$0, 24(%rax)
	movq	$0, 16(%rax)
	movups	%xmm0, 32(%rax)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L324
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movl	%r15d, %esi
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	%r14, 40(%r13)
	testl	%eax, %eax
	jg	.L327
	movl	-56(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN6icu_679UVector327setSizeEi@PLT
.L327:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L409
	movq	16(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	16(%r12), %rdx
	movq	%r14, 32(%r13)
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L410
.L328:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L365:
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.L363
	movq	(%rdi), %rax
	call	*8(%rax)
.L363:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L364
	movq	(%rdi), %rax
	call	*8(%rax)
.L364:
	addq	$40, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	movq	24(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	16(%r12), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L411
.L329:
	movq	32(%r13), %r14
	testq	%r14, %r14
	je	.L365
	jmp	.L328
.L411:
	movl	$48, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -72(%rbp)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L330
	movq	16(%r12), %rbx
	movb	$0, (%rax)
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	$0, 4(%rax)
	movl	$0, 24(%rax)
	movq	$0, 16(%rax)
	movups	%xmm0, 32(%rax)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L331
	movl	-56(%rbp), %esi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movq	%r14, 40(%r15)
	movl	(%rbx), %r15d
	testl	%r15d, %r15d
	jg	.L334
	movl	-56(%rbp), %esi
	movq	%r14, %rdi
	call	_ZN6icu_679UVector327setSizeEi@PLT
.L334:
	movq	16(%r12), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L412
.L366:
	movq	-72(%rbp), %rax
	movq	32(%rax), %r14
	testq	%r14, %r14
	je	.L360
.L336:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
.L360:
	movq	-72(%rbp), %rax
	movq	40(%rax), %rdi
	testq	%rdi, %rdi
	je	.L361
	movq	(%rdi), %rax
	call	*8(%rax)
.L361:
	movq	-72(%rbp), %rax
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L362
	movq	(%rdi), %rax
	call	*8(%rax)
.L362:
	movq	-72(%rbp), %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L329
.L412:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L335
	movq	16(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	-72(%rbp), %rax
	movq	%r14, 32(%rax)
	movq	16(%r12), %rax
	movl	(%rax), %ebx
	testl	%ebx, %ebx
	jg	.L336
	movq	8(%r12), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	136(%rax), %rdx
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	movq	16(%r12), %rdx
	movq	24(%r12), %rdi
	movq	-72(%rbp), %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	16(%r12), %rax
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L366
.L342:
	movq	24(%r12), %rdi
	cmpl	$1, 8(%rdi)
	jle	.L322
	movl	$1, %r14d
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L413:
	movq	24(%r12), %rdi
	addl	$1, %r14d
	cmpl	%r14d, 8(%rdi)
	jle	.L322
.L338:
	movl	%r14d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpb	$0, (%rax)
	movq	%rax, %rbx
	jne	.L413
	cmpl	$1, -56(%rbp)
	movb	$1, (%rax)
	jle	.L342
	movl	$1, -52(%rbp)
	.p2align 4,,10
	.p2align 3
.L359:
	movq	32(%rbx), %rdi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movl	8(%rdi), %r10d
	testl	%r10d, %r10d
	jg	.L343
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L345:
	movq	32(%rbx), %rdi
	addl	$1, %r15d
	cmpl	%r15d, 8(%rdi)
	jle	.L414
.L343:
	movl	%r15d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpl	$3, (%rax)
	movq	%rax, %rdx
	jne	.L345
	movl	-52(%rbp), %eax
	cmpl	%eax, 124(%rdx)
	jne	.L345
	testq	%r14, %r14
	je	.L415
.L346:
	movq	152(%rdx), %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	addl	$1, %r15d
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	movq	32(%rbx), %rdi
	cmpl	%r15d, 8(%rdi)
	jg	.L343
	.p2align 4,,10
	.p2align 3
.L414:
	testq	%r14, %r14
	je	.L344
	movq	24(%r12), %rdi
	xorl	%r8d, %r8d
	movl	8(%rdi), %r9d
	testl	%r9d, %r9d
	jg	.L349
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L352:
	movq	24(%r12), %rdi
	addl	$1, %r8d
	cmpl	%r8d, 8(%rdi)
	jle	.L354
.L349:
	movl	%r8d, %esi
	movl	%r8d, -64(%rbp)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r14, %rdi
	movq	32(%rax), %rsi
	call	_ZNK6icu_677UVector6equalsERKS0_@PLT
	movl	-64(%rbp), %r8d
	testb	%al, %al
	je	.L352
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*8(%rax)
	movl	-64(%rbp), %r8d
.L353:
	movq	40(%rbx), %rdi
	movl	-52(%rbp), %edx
	movl	%r8d, %esi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
.L344:
	addl	$1, -52(%rbp)
	movl	-52(%rbp), %eax
	cmpl	%eax, -56(%rbp)
	jne	.L359
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L415:
	movl	$40, %edi
	movq	%rdx, -64(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L402
	movq	16(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	-64(%rbp), %rdx
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L354:
	movl	$48, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L402
	movq	16(%r12), %r15
	movb	$0, (%rax)
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	$0, 4(%rax)
	movl	$0, 24(%rax)
	movq	$0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -64(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L355
	movl	-56(%rbp), %esi
	movq	%r15, %rdx
	movq	%r8, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movq	-64(%rbp), %rdi
	movq	-80(%rbp), %r8
	movl	(%r15), %esi
	movq	%rdi, 40(%r8)
	testl	%esi, %esi
	jg	.L357
	movl	-56(%rbp), %esi
	movq	%r8, -64(%rbp)
	call	_ZN6icu_679UVector327setSizeEi@PLT
	movq	-64(%rbp), %r8
.L357:
	movq	16(%r12), %rdx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L366
	movq	%r14, 32(%r8)
	movq	24(%r12), %rdi
	movq	%r8, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	16(%r12), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L322
	movq	24(%r12), %rax
	movl	8(%rax), %r8d
	subl	$1, %r8d
	jmp	.L353
.L323:
	movq	16(%r12), %rax
	movl	$7, (%rax)
.L322:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L355:
	.cfi_restore_state
	cmpl	$0, (%r15)
	movq	$0, 40(%r8)
	jg	.L357
	movl	$7, (%r15)
	jmp	.L357
.L402:
	movq	16(%r12), %rax
	movl	$7, (%rax)
	jmp	.L366
.L335:
	movq	-72(%rbp), %rax
	movq	$0, 32(%rax)
	movq	16(%r12), %rax
	movl	$7, (%rax)
	jmp	.L360
.L331:
	movq	-72(%rbp), %rax
	cmpl	$0, (%rbx)
	movq	$0, 40(%rax)
	jg	.L334
	movl	$7, (%rbx)
	jmp	.L334
.L330:
	movq	16(%r12), %rax
	movl	$7, (%rax)
	jmp	.L329
.L409:
	movq	16(%r12), %rax
	movq	$0, 32(%r13)
	movl	$7, (%rax)
	jmp	.L365
.L324:
	cmpl	$0, (%rbx)
	movq	$0, 40(%r13)
	jg	.L327
	movl	$7, (%rbx)
	jmp	.L327
	.cfi_endproc
.LFE4285:
	.size	_ZN6icu_6716RBBITableBuilder15buildStateTableEv.part.0, .-_ZN6icu_6716RBBITableBuilder15buildStateTableEv.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder15buildStateTableEv
	.type	_ZN6icu_6716RBBITableBuilder15buildStateTableEv, @function
_ZN6icu_6716RBBITableBuilder15buildStateTableEv:
.LFB3175:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L418
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	jmp	_ZN6icu_6716RBBITableBuilder15buildStateTableEv.part.0
	.cfi_endproc
.LFE3175:
	.size	_ZN6icu_6716RBBITableBuilder15buildStateTableEv, .-_ZN6icu_6716RBBITableBuilder15buildStateTableEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder8bofFixupEv
	.type	_ZN6icu_6716RBBITableBuilder8bofFixupEv, @function
_ZN6icu_6716RBBITableBuilder8bofFixupEv:
.LFB3174:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L425
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %r13
	movq	24(%rax), %rax
	movq	136(%rax), %r14
	movl	8(%r14), %eax
	testl	%eax, %eax
	jle	.L419
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L422:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpl	$3, (%rax)
	jne	.L421
	movl	124(%r13), %ecx
	cmpl	%ecx, 124(%rax)
	je	.L428
.L421:
	addl	$1, %ebx
	cmpl	%ebx, 8(%r14)
	jg	.L422
.L419:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movq	152(%rax), %rdx
	movq	152(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE3174:
	.size	_ZN6icu_6716RBBITableBuilder8bofFixupEv, .-_ZN6icu_6716RBBITableBuilder8bofFixupEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder9setEqualsEPNS_7UVectorES2_
	.type	_ZN6icu_6716RBBITableBuilder9setEqualsEPNS_7UVectorES2_, @function
_ZN6icu_6716RBBITableBuilder9setEqualsEPNS_7UVectorES2_:
.LFB3183:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	jmp	_ZNK6icu_677UVector6equalsERKS0_@PLT
	.cfi_endproc
.LFE3183:
	.size	_ZN6icu_6716RBBITableBuilder9setEqualsEPNS_7UVectorES2_, .-_ZN6icu_6716RBBITableBuilder9setEqualsEPNS_7UVectorES2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder21findDuplCharClassFromEPSt4pairIiiE
	.type	_ZN6icu_6716RBBITableBuilder21findDuplCharClassFromEPSt4pairIiiE, @function
_ZN6icu_6716RBBITableBuilder21findDuplCharClassFromEPSt4pairIiiE:
.LFB3184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	movl	8(%rax), %r12d
	movq	(%rdi), %rax
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv@PLT
	movl	%eax, %r13d
	movl	(%r15), %eax
	leal	-1(%r13), %edx
	movl	%edx, -52(%rbp)
	cmpl	%eax, %edx
	jle	.L441
.L431:
	addl	$1, %eax
	movl	%eax, 4(%r15)
	movl	%eax, %edx
	cmpl	%r13d, %eax
	jge	.L433
	.p2align 4,,10
	.p2align 3
.L440:
	testl	%r12d, %r12d
	jle	.L434
	xorl	%ebx, %ebx
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L451:
	movl	8(%rcx), %esi
	movl	4(%r15), %edx
	testl	%esi, %esi
	jle	.L436
	movl	%esi, %edi
	subl	%eax, %edi
	testl	%edi, %edi
	jle	.L437
	movq	24(%rcx), %rcx
	movzwl	(%rcx,%rax,4), %edi
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L438
	subl	%edx, %esi
	testl	%esi, %esi
	jle	.L438
.L442:
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,4), %eax
.L438:
	cmpw	%di, %ax
	jne	.L434
.L436:
	addl	$1, %ebx
	cmpl	%r12d, %ebx
	je	.L450
.L439:
	movq	24(%r14), %rdi
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	40(%rax), %rcx
	movslq	(%r15), %rax
	testl	%eax, %eax
	jns	.L451
	movl	4(%r15), %edx
.L437:
	testl	%edx, %edx
	js	.L436
	movl	8(%rcx), %eax
	testl	%eax, %eax
	jle	.L436
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L436
	movq	24(%rcx), %rcx
	xorl	%edi, %edi
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L434:
	addl	$1, %edx
	movl	%edx, 4(%r15)
	cmpl	%r13d, %edx
	jl	.L440
	movl	(%r15), %eax
	addl	$1, %eax
.L433:
	movl	%eax, (%r15)
	cmpl	-52(%rbp), %eax
	jl	.L431
.L441:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3184:
	.size	_ZN6icu_6716RBBITableBuilder21findDuplCharClassFromEPSt4pairIiiE, .-_ZN6icu_6716RBBITableBuilder21findDuplCharClassFromEPSt4pairIiiE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder12removeColumnEi
	.type	_ZN6icu_6716RBBITableBuilder12removeColumnEi, @function
_ZN6icu_6716RBBITableBuilder12removeColumnEi:
.LFB3185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	24(%rdi), %rdi
	movl	8(%rdi), %r14d
	testl	%r14d, %r14d
	jle	.L452
	movl	%esi, %r13d
	xorl	%ebx, %ebx
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L456:
	movq	24(%r12), %rdi
.L454:
	movl	%ebx, %esi
	addl	$1, %ebx
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r13d, %esi
	movq	40(%rax), %rdi
	call	_ZN6icu_679UVector3215removeElementAtEi@PLT
	cmpl	%r14d, %ebx
	jne	.L456
.L452:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3185:
	.size	_ZN6icu_6716RBBITableBuilder12removeColumnEi, .-_ZN6icu_6716RBBITableBuilder12removeColumnEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder18findDuplicateStateEPSt4pairIiiE
	.type	_ZN6icu_6716RBBITableBuilder18findDuplicateStateEPSt4pairIiiE, @function
_ZN6icu_6716RBBITableBuilder18findDuplicateStateEPSt4pairIiiE:
.LFB3186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	24(%rdi), %rax
	movl	8(%rax), %r13d
	movq	(%rdi), %rax
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv@PLT
	movl	(%rbx), %esi
	movl	%eax, -52(%rbp)
	leal	-1(%r13), %eax
	movl	%eax, -56(%rbp)
	cmpl	%eax, %esi
	jge	.L483
	.p2align 4,,10
	.p2align 3
.L458:
	movq	24(%r14), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r12
	movl	(%rbx), %eax
	leal	1(%rax), %esi
	movl	%esi, 4(%rbx)
	cmpl	%r13d, %esi
	jl	.L482
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L461:
	movl	4(%rbx), %esi
.L463:
	addl	$1, %esi
	movl	%esi, 4(%rbx)
	cmpl	%r13d, %esi
	jge	.L503
.L482:
	movq	24(%r14), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	4(%rax), %edx
	cmpl	%edx, 4(%r12)
	jne	.L461
	movl	8(%rax), %ecx
	cmpl	%ecx, 8(%r12)
	jne	.L461
	movl	24(%rax), %ecx
	cmpl	%ecx, 24(%r12)
	jne	.L461
	movl	-52(%rbp), %edi
	testl	%edi, %edi
	jle	.L491
	movq	40(%r12), %r10
	movq	40(%rax), %r11
	movl	8(%r10), %ecx
	movl	8(%r11), %edx
	testl	%ecx, %ecx
	jle	.L504
	testl	%edx, %edx
	jle	.L505
	movl	%ecx, %eax
	movl	%ecx, %r15d
	xorl	%ecx, %ecx
	subl	-52(%rbp), %r15d
	subl	%eax, %edx
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L506:
	movq	24(%r10), %rsi
	leal	(%rdx,%rax), %r8d
	xorl	%edi, %edi
	movl	(%rsi,%rcx), %esi
	testl	%r8d, %r8d
	jg	.L484
.L478:
	cmpl	%esi, %edi
	je	.L479
	movl	(%rbx), %r8d
	movl	4(%rbx), %r9d
	cmpl	%esi, %r8d
	je	.L480
	cmpl	%esi, %r9d
	jne	.L490
	cmpl	%edi, %r8d
	jne	.L463
.L479:
	subl	$1, %eax
	addq	$4, %rcx
	cmpl	%eax, %r15d
	je	.L491
.L481:
	testl	%eax, %eax
	jg	.L506
	leal	(%rdx,%rax), %edi
	xorl	%esi, %esi
	testl	%edi, %edi
	jle	.L479
.L484:
	movq	24(%r11), %rdi
	movl	(%rdi,%rcx), %edi
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L480:
	cmpl	%edi, %r9d
	je	.L479
.L490:
	movl	%r9d, %esi
	addl	$1, %esi
	movl	%esi, 4(%rbx)
	cmpl	%r13d, %esi
	jl	.L482
	.p2align 4,,10
	.p2align 3
.L503:
	movl	(%rbx), %eax
	leal	1(%rax), %esi
.L460:
	movl	%esi, (%rbx)
	cmpl	-56(%rbp), %esi
	jl	.L458
.L483:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	testl	%edx, %edx
	jle	.L491
	movl	%edx, %r8d
	xorl	%eax, %eax
	subl	%edi, %r8d
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L466:
	testl	%esi, %esi
	jne	.L463
	cmpl	%ecx, %edi
	jne	.L463
.L465:
	subl	$1, %edx
	addq	$4, %rax
	cmpl	%r8d, %edx
	je	.L491
.L470:
	testl	%edx, %edx
	jle	.L465
	movq	24(%r11), %rcx
	movl	(%rcx,%rax), %ecx
	testl	%ecx, %ecx
	je	.L465
	movl	(%rbx), %edi
	movl	4(%rbx), %esi
	testl	%edi, %edi
	jne	.L466
	cmpl	%esi, %ecx
	je	.L465
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L505:
	movl	%ecx, %r8d
	movl	%ecx, %eax
	subl	-52(%rbp), %r8d
	xorl	%edx, %edx
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L474:
	cmpl	%ecx, %esi
	jne	.L463
	testl	%edi, %edi
	jne	.L463
.L473:
	subl	$1, %eax
	addq	$4, %rdx
	cmpl	%eax, %r8d
	je	.L491
.L476:
	testl	%eax, %eax
	jle	.L473
	movq	24(%r10), %rcx
	movl	(%rcx,%rdx), %ecx
	testl	%ecx, %ecx
	je	.L473
	movl	(%rbx), %edi
	movl	4(%rbx), %esi
	cmpl	%ecx, %edi
	jne	.L474
	testl	%esi, %esi
	je	.L473
	jmp	.L463
	.cfi_endproc
.LFE3186:
	.size	_ZN6icu_6716RBBITableBuilder18findDuplicateStateEPSt4pairIiiE, .-_ZN6icu_6716RBBITableBuilder18findDuplicateStateEPSt4pairIiiE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder22findDuplicateSafeStateEPSt4pairIiiE
	.type	_ZN6icu_6716RBBITableBuilder22findDuplicateSafeStateEPSt4pairIiiE, @function
_ZN6icu_6716RBBITableBuilder22findDuplicateSafeStateEPSt4pairIiiE:
.LFB3187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movl	(%rsi), %esi
	movl	8(%rdi), %r12d
	leal	-1(%r12), %r13d
	cmpl	%esi, %r13d
	jle	.L508
.L561:
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r15
	movl	(%r14), %eax
	leal	1(%rax), %esi
	movl	%esi, 4(%r14)
	cmpl	%r12d, %esi
	jl	.L562
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L603:
	movl	(%r14), %ecx
	movl	4(%r14), %edx
	cmpl	%r11d, %ecx
	je	.L543
	cmpl	%r11d, %edx
	jne	.L520
	cmpl	%ecx, %r10d
	je	.L542
	.p2align 4,,10
	.p2align 3
.L520:
	leal	1(%rdx), %esi
	movl	%esi, 4(%r14)
	cmpl	%r12d, %esi
	jge	.L601
.L562:
	movq	32(%rbx), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movzwl	8(%r15), %edx
	testw	%dx, %dx
	js	.L510
	movswl	%dx, %esi
	sarl	$5, %esi
.L511:
	testl	%esi, %esi
	jle	.L590
	movswl	8(%rax), %r8d
	andl	$2, %edx
	movl	%r8d, %ecx
	sarl	$5, %r8d
	movl	%ecx, %edi
	andl	$2, %edi
	testw	%cx, %cx
	js	.L602
	testw	%dx, %dx
	jne	.L538
	testw	%di, %di
	jne	.L539
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L544:
	cmpl	%edi, %esi
	jbe	.L540
	movq	24(%r15), %rdx
	movl	$65535, %r10d
	movzwl	(%rdx,%r9), %r11d
	cmpl	%r8d, %edi
	jnb	.L541
.L567:
	movq	24(%rax), %rdx
	movzwl	(%rdx,%r9), %r10d
.L541:
	cmpl	%r11d, %r10d
	jne	.L603
.L542:
	addl	$1, %edi
	addq	$2, %r9
	cmpl	%edi, %esi
	jne	.L544
.L590:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	cmpl	%edx, %r10d
	jne	.L520
	addl	$1, %edi
	addq	$2, %r9
	cmpl	%edi, %esi
	jne	.L544
	jmp	.L590
.L601:
	leal	1(%rcx), %esi
.L509:
	movl	%esi, (%r14)
	cmpl	%esi, %r13d
	jle	.L508
	movq	32(%rbx), %rdi
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L540:
	movl	$65535, %r11d
	cmpl	%edi, %r8d
	ja	.L567
	addl	$1, %edi
	addq	$2, %r9
	cmpl	%edi, %esi
	jne	.L544
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L510:
	movl	12(%r15), %esi
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L602:
	testw	%di, %di
	jne	.L514
	testw	%dx, %dx
	jne	.L515
	movl	12(%rax), %r11d
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L519:
	cmpl	%r10d, %edx
	jne	.L520
	cmpl	%ecx, %r9d
	jne	.L520
.L518:
	addl	$1, %edi
	addq	$2, %r8
	cmpl	%edi, %esi
	je	.L590
.L521:
	cmpl	%edi, %esi
	jbe	.L516
	movq	24(%r15), %rdx
	movl	$65535, %r9d
	movzwl	(%rdx,%r8), %r10d
	cmpl	%edi, %r11d
	jbe	.L517
.L563:
	movq	24(%rax), %rdx
	movzwl	(%rdx,%r8), %r9d
.L517:
	cmpl	%r10d, %r9d
	je	.L518
	movl	(%r14), %ecx
	movl	4(%r14), %edx
	cmpl	%r10d, %ecx
	jne	.L519
	cmpl	%edx, %r9d
	je	.L518
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L516:
	movl	$65535, %r10d
	cmpl	%edi, %r11d
	ja	.L563
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L538:
	testw	%di, %di
	leal	-1(%rsi), %r11d
	movl	$0, %edi
	jne	.L560
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L554:
	cmpl	%r10d, %edx
	jne	.L520
	cmpl	%ecx, %r9d
	jne	.L520
.L553:
	leaq	1(%rdi), %rdx
	cmpq	%r11, %rdi
	je	.L590
	movq	%rdx, %rdi
.L555:
	cmpl	%edi, %esi
	jbe	.L551
	movzwl	10(%r15,%rdi,2), %r10d
	movl	$65535, %r9d
	cmpl	%edi, %r8d
	jbe	.L552
.L569:
	movq	24(%rax), %rdx
	movzwl	(%rdx,%rdi,2), %r9d
.L552:
	cmpl	%r10d, %r9d
	je	.L553
	movl	(%r14), %ecx
	movl	4(%r14), %edx
	cmpl	%r10d, %ecx
	jne	.L554
	cmpl	%edx, %r9d
	je	.L553
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L604:
	cmpl	%r10d, %edx
	jne	.L520
	cmpl	%r9d, %ecx
	jne	.L520
.L558:
	leaq	1(%rdi), %rdx
	cmpq	%r11, %rdi
	je	.L590
	movq	%rdx, %rdi
.L560:
	cmpl	%edi, %esi
	jbe	.L556
	movzwl	10(%r15,%rdi,2), %r10d
	movl	$65535, %r9d
	cmpl	%edi, %r8d
	jbe	.L557
.L570:
	movzwl	10(%rax,%rdi,2), %r9d
.L557:
	cmpl	%r10d, %r9d
	je	.L558
	movl	(%r14), %ecx
	movl	4(%r14), %edx
	cmpl	%r10d, %ecx
	jne	.L604
	cmpl	%edx, %r9d
	je	.L558
	movl	%r10d, %ecx
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L551:
	movl	$65535, %r10d
	cmpl	%edi, %r8d
	ja	.L569
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L556:
	movl	$65535, %r10d
	cmpl	%edi, %r8d
	ja	.L570
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L514:
	testw	%dx, %dx
	jne	.L527
	movl	12(%rax), %r10d
	leal	-1(%rsi), %r11d
	xorl	%edi, %edi
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L605:
	cmpl	%r9d, %edx
	jne	.L520
	cmpl	%ecx, %r8d
	jne	.L520
.L530:
	leaq	1(%rdi), %rdx
	cmpq	%rdi, %r11
	je	.L590
	movq	%rdx, %rdi
.L532:
	cmpl	%esi, %edi
	jnb	.L528
	movq	24(%r15), %rdx
	movl	$65535, %r8d
	movzwl	(%rdx,%rdi,2), %r9d
	cmpl	%r10d, %edi
	jnb	.L529
.L565:
	movzwl	10(%rax,%rdi,2), %r8d
.L529:
	cmpl	%r9d, %r8d
	je	.L530
	movl	(%r14), %ecx
	movl	4(%r14), %edx
	cmpl	%r9d, %ecx
	jne	.L605
	cmpl	%edx, %r8d
	je	.L530
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L528:
	movl	$65535, %r9d
	cmpl	%r10d, %edi
	jb	.L565
	jmp	.L530
.L539:
	leal	-1(%rsi), %r11d
	xorl	%edi, %edi
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L548:
	cmpl	%r10d, %edx
	jne	.L520
	cmpl	%ecx, %r9d
	jne	.L520
.L547:
	leaq	1(%rdi), %rdx
	cmpq	%rdi, %r11
	je	.L590
	movq	%rdx, %rdi
.L549:
	cmpl	%esi, %edi
	jnb	.L545
	movq	24(%r15), %rdx
	movl	$65535, %r9d
	movzwl	(%rdx,%rdi,2), %r10d
	cmpl	%r8d, %edi
	jnb	.L546
.L568:
	movzwl	10(%rax,%rdi,2), %r9d
.L546:
	cmpl	%r10d, %r9d
	je	.L547
	movl	(%r14), %ecx
	movl	4(%r14), %edx
	cmpl	%r10d, %ecx
	jne	.L548
	cmpl	%edx, %r9d
	je	.L547
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L545:
	movl	$65535, %r10d
	cmpl	%r8d, %edi
	jb	.L568
	jmp	.L547
.L527:
	movl	12(%rax), %r9d
	leal	-1(%rsi), %r10d
	xorl	%edi, %edi
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L536:
	cmpl	%edx, %r11d
	jne	.L580
	cmpl	%r8d, %ecx
	jne	.L520
.L535:
	leaq	1(%rdi), %rdx
	cmpq	%rdi, %r10
	je	.L590
	movq	%rdx, %rdi
.L537:
	cmpl	%edi, %esi
	jbe	.L533
	movzwl	10(%r15,%rdi,2), %edx
	movl	$65535, %r8d
	cmpl	%edi, %r9d
	jbe	.L534
.L566:
	movzwl	10(%rax,%rdi,2), %r8d
.L534:
	cmpl	%edx, %r8d
	je	.L535
	movl	(%r14), %ecx
	movl	4(%r14), %r11d
	cmpl	%edx, %ecx
	jne	.L536
	cmpl	%r11d, %r8d
	je	.L535
	movl	%edx, %ecx
	movl	%r11d, %edx
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L533:
	movl	$65535, %edx
	cmpl	%edi, %r9d
	ja	.L566
	jmp	.L535
.L515:
	movl	12(%rax), %r10d
	leal	-1(%rsi), %r11d
	xorl	%edi, %edi
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L606:
	cmpl	%r9d, %edx
	jne	.L520
	cmpl	%ecx, %r8d
	jne	.L520
.L524:
	leaq	1(%rdi), %rdx
	cmpq	%rdi, %r11
	je	.L590
	movq	%rdx, %rdi
.L526:
	cmpl	%edi, %esi
	jbe	.L522
	movzwl	10(%r15,%rdi,2), %r9d
	movl	$65535, %r8d
	cmpl	%edi, %r10d
	jbe	.L523
.L564:
	movq	24(%rax), %rdx
	movzwl	(%rdx,%rdi,2), %r8d
.L523:
	cmpl	%r9d, %r8d
	je	.L524
	movl	(%r14), %ecx
	movl	4(%r14), %edx
	cmpl	%r9d, %ecx
	jne	.L606
	cmpl	%edx, %r8d
	je	.L524
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L522:
	movl	$65535, %r9d
	cmpl	%edi, %r10d
	ja	.L564
	jmp	.L524
.L580:
	movl	%r11d, %edx
	jmp	.L520
.L508:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3187:
	.size	_ZN6icu_6716RBBITableBuilder22findDuplicateSafeStateEPSt4pairIiiE, .-_ZN6icu_6716RBBITableBuilder22findDuplicateSafeStateEPSt4pairIiiE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder11removeStateESt4pairIiiE
	.type	_ZN6icu_6716RBBITableBuilder11removeStateESt4pairIiiE, @function
_ZN6icu_6716RBBITableBuilder11removeStateESt4pairIiiE:
.LFB3188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	sarq	$32, %r12
	movl	%r12d, %esi
	.cfi_offset 3, -56
	movl	%r12d, %ebx
	subq	$40, %rsp
	movq	%rdi, -64(%rbp)
	movq	24(%rdi), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	24(%r15), %rdi
	movl	%r12d, %esi
	movq	%rax, %r13
	call	_ZN6icu_677UVector15removeElementAtEi@PLT
	testq	%r13, %r13
	je	.L608
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L609
	movq	(%rdi), %rax
	call	*8(%rax)
.L609:
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.L610
	movq	(%rdi), %rax
	call	*8(%rax)
.L610:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L611
	movq	(%rdi), %rax
	call	*8(%rax)
.L611:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L608:
	movq	-64(%rbp), %rdx
	movq	24(%rdx), %rax
	movl	8(%rax), %r15d
	movq	(%rdx), %rax
	movq	160(%rax), %rdi
	movl	%r15d, -68(%rbp)
	call	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv@PLT
	movl	%eax, -56(%rbp)
	testl	%r15d, %r15d
	jle	.L607
	subl	$1, %eax
	movl	$0, -52(%rbp)
	movl	%eax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L617:
	movq	-64(%rbp), %rax
	movl	-52(%rbp), %esi
	movq	24(%rax), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r12
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	jle	.L613
	movl	-72(%rbp), %r13d
	xorl	%r15d, %r15d
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%rax, %r15
.L616:
	movq	40(%r12), %rdi
	movl	%r15d, %edx
	xorl	%esi, %esi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L614
	subl	%r15d, %eax
	testl	%eax, %eax
	jle	.L614
	movq	24(%rdi), %rax
	movl	(%rax,%r15,4), %esi
.L614:
	cmpl	%esi, %ebx
	movl	%esi, %r8d
	movl	%r14d, %eax
	setl	%cl
	movzbl	%cl, %ecx
	subl	%ecx, %r8d
	cmpl	%esi, %ebx
	cmovne	%r8d, %eax
	movl	%eax, %esi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	leaq	1(%r15), %rax
	cmpq	%r13, %r15
	jne	.L621
.L613:
	addl	$1, -52(%rbp)
	movl	-52(%rbp), %eax
	cmpl	-68(%rbp), %eax
	jne	.L617
.L607:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3188:
	.size	_ZN6icu_6716RBBITableBuilder11removeStateESt4pairIiiE, .-_ZN6icu_6716RBBITableBuilder11removeStateESt4pairIiiE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder15removeSafeStateESt4pairIiiE
	.type	_ZN6icu_6716RBBITableBuilder15removeSafeStateESt4pairIiiE, @function
_ZN6icu_6716RBBITableBuilder15removeSafeStateESt4pairIiiE:
.LFB3189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	sarq	$32, %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	%rdi, -64(%rbp)
	movq	32(%rdi), %rdi
	call	_ZN6icu_677UVector15removeElementAtEi@PLT
	movq	32(%r15), %rdi
	movl	8(%rdi), %eax
	movl	%eax, -56(%rbp)
	testl	%eax, %eax
	jle	.L636
	movzwl	%r14w, %eax
	xorl	%r13d, %r13d
	movl	%eax, -52(%rbp)
	.p2align 4,,10
	.p2align 3
.L649:
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r14
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L638
	movswl	%ax, %edx
	sarl	$5, %edx
.L639:
	testl	%edx, %edx
	jle	.L640
	leal	-1(%rdx), %r12d
	xorl	%r15d, %r15d
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L653:
	movswl	%ax, %esi
	sarl	$5, %esi
.L642:
	movl	$65535, %edx
	cmpl	%r9d, %esi
	jbe	.L643
	testb	$2, %al
	je	.L644
	leaq	10(%r14), %rax
.L645:
	movzwl	(%rax,%r15,2), %edx
.L643:
	cmpl	%edx, %ebx
	je	.L652
	leal	-1(%rdx), %eax
	movzwl	%ax, %eax
	cmovl	%eax, %edx
.L647:
	movl	%r8d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
	leaq	1(%r15), %rdx
	cmpq	%r12, %r15
	je	.L640
	movzwl	8(%r14), %eax
	movq	%rdx, %r15
.L648:
	movl	%r15d, %r9d
	movl	%r15d, %r8d
	testw	%ax, %ax
	jns	.L653
	movl	12(%r14), %esi
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L644:
	movq	24(%r14), %rax
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L652:
	movl	-52(%rbp), %edx
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L640:
	addl	$1, %r13d
	cmpl	-56(%rbp), %r13d
	je	.L636
	movq	-64(%rbp), %rax
	movq	32(%rax), %rdi
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L638:
	movl	12(%r14), %edx
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L636:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3189:
	.size	_ZN6icu_6716RBBITableBuilder15removeSafeStateESt4pairIiiE, .-_ZN6icu_6716RBBITableBuilder15removeSafeStateESt4pairIiiE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder21removeDuplicateStatesEv
	.type	_ZN6icu_6716RBBITableBuilder21removeDuplicateStatesEv, @function
_ZN6icu_6716RBBITableBuilder21removeDuplicateStatesEv:
.LFB3190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$3, -48(%rbp)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L659:
	movq	-48(%rbp), %rsi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	_ZN6icu_6716RBBITableBuilder11removeStateESt4pairIiiE
.L656:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6716RBBITableBuilder18findDuplicateStateEPSt4pairIiiE
	testb	%al, %al
	jne	.L659
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L660
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L660:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3190:
	.size	_ZN6icu_6716RBBITableBuilder21removeDuplicateStatesEv, .-_ZN6icu_6716RBBITableBuilder21removeDuplicateStatesEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716RBBITableBuilder12getTableSizeEv
	.type	_ZNK6icu_6716RBBITableBuilder12getTableSizeEv, @function
_ZNK6icu_6716RBBITableBuilder12getTableSizeEv:
.LFB3207:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	cmpq	$0, (%rdx)
	je	.L664
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rax
	movl	8(%rax), %ebx
	movq	(%rdi), %rax
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv@PLT
	addq	$8, %rsp
	leal	8(%rax,%rax), %eax
	imull	%ebx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	addl	$16, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3207:
	.size	_ZNK6icu_6716RBBITableBuilder12getTableSizeEv, .-_ZNK6icu_6716RBBITableBuilder12getTableSizeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder11exportTableEPv
	.type	_ZN6icu_6716RBBITableBuilder11exportTableEPv, @function
_ZN6icu_6716RBBITableBuilder11exportTableEPv:
.LFB3208:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L689
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	cmpq	$0, (%rax)
	je	.L669
	movq	(%rdi), %rax
	movq	%rsi, %r15
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv@PLT
	movl	%eax, %ebx
	cmpl	$32767, %eax
	jg	.L673
	movq	24(%r12), %rax
	cmpl	$32767, 8(%rax)
	jg	.L673
	leal	8(%rbx,%rbx), %edx
	movl	%edx, 4(%r15)
	movl	8(%rax), %eax
	movl	$0, 8(%r15)
	movl	%eax, (%r15)
	movq	(%r12), %rax
	cmpb	$0, 154(%rax)
	je	.L675
	movl	$1, 8(%r15)
.L675:
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder6sawBOFEv@PLT
	testb	%al, %al
	je	.L676
	orl	$2, 8(%r15)
.L676:
	movl	(%r15), %ecx
	movl	$0, 12(%r15)
	testl	%ecx, %ecx
	je	.L669
	leaq	16(%r15), %rax
	leal	-1(%rbx), %r13d
	xorl	%r14d, %r14d
	movq	%rax, -56(%rbp)
	leaq	2(%r13,%r13), %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L678:
	movq	24(%r12), %rdi
	movl	%r14d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r14d, %edi
	imull	4(%r15), %edi
	addq	-56(%rbp), %rdi
	movl	4(%rax), %edx
	movw	%dx, (%rdi)
	movl	8(%rax), %edx
	movw	%dx, 2(%rdi)
	movl	24(%rax), %edx
	movw	%dx, 4(%rdi)
	testl	%ebx, %ebx
	jle	.L680
	movq	40(%rax), %r9
	xorl	%eax, %eax
	movl	8(%r9), %esi
	testl	%esi, %esi
	jg	.L679
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L693:
	movq	24(%r9), %rdx
	movl	(%rdx,%rax,4), %edx
	movw	%dx, 8(%rdi,%rax,2)
	leaq	1(%rax), %rdx
	cmpq	%rax, %r13
	je	.L680
.L682:
	movq	%rdx, %rax
.L679:
	movl	%esi, %edx
	subl	%eax, %edx
	testl	%edx, %edx
	jg	.L693
	xorl	%edx, %edx
	movw	%dx, 8(%rdi,%rax,2)
	leaq	1(%rax), %rdx
	cmpq	%rax, %r13
	jne	.L682
.L680:
	addl	$1, %r14d
	cmpl	%r14d, (%r15)
	ja	.L678
.L669:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	.cfi_restore_state
	movq	16(%r12), %rax
	movl	$66048, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L689:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-64(%rbp), %rdx
	addq	$8, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L680
	.cfi_endproc
.LFE3208:
	.size	_ZN6icu_6716RBBITableBuilder11exportTableEPv, .-_ZN6icu_6716RBBITableBuilder11exportTableEPv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder21buildSafeReverseTableER10UErrorCode
	.type	_ZN6icu_6716RBBITableBuilder21buildSafeReverseTableER10UErrorCode, @function
_ZN6icu_6716RBBITableBuilder21buildSafeReverseTableER10UErrorCode:
.LFB3209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	(%rdi), %rax
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv@PLT
	movl	%eax, -152(%rbp)
	movl	%eax, %edx
	movq	24(%r14), %rax
	movl	8(%rax), %r15d
	testl	%edx, %edx
	jle	.L695
	movq	$0, -192(%rbp)
	movl	%edx, %eax
	subl	$1, %eax
	movq	%rax, -184(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -200(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-192(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L704:
	movl	%eax, -148(%rbp)
	salq	$2, %rax
	movq	%rax, -168(%rbp)
	movq	$0, -176(%rbp)
	movq	-176(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L702:
	movl	%eax, %r12d
	cmpl	$1, %r15d
	jle	.L729
	salq	$2, %rax
	movl	$1, %r13d
	movl	$-1, %ebx
	movq	%rax, -160(%rbp)
	movq	%r14, %rax
	movl	%r13d, %r14d
	movl	%r12d, %r13d
	movq	%rax, %r12
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L752:
	movl	%esi, %ebx
.L701:
	movq	24(%r12), %rdi
	movl	%r14d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	xorl	%esi, %esi
	movq	40(%rax), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L697
	subl	-148(%rbp), %eax
	testl	%eax, %eax
	jle	.L697
	movq	24(%rdi), %rax
	movq	-168(%rbp), %rdx
	movl	(%rax,%rdx), %esi
.L697:
	movq	24(%r12), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	xorl	%esi, %esi
	movq	40(%rax), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L698
	subl	%r13d, %eax
	testl	%eax, %eax
	jle	.L698
	movq	24(%rdi), %rax
	movq	-160(%rbp), %rdx
	movl	(%rax,%rdx), %esi
.L698:
	testl	%ebx, %ebx
	js	.L699
	cmpl	%ebx, %esi
	jne	.L748
.L699:
	addl	$1, %r14d
	cmpl	%r15d, %r14d
	jne	.L752
	movq	-200(%rbp), %r13
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %r14
	movq	-208(%rbp), %rbx
	movzwl	-192(%rbp), %eax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movw	%ax, -136(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movzwl	-176(%rbp), %eax
	movq	%rbx, %rdi
	movw	%ax, -136(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L729:
	movq	-176(%rbp), %rcx
	leaq	1(%rcx), %rax
	cmpq	-184(%rbp), %rcx
	je	.L753
	movq	%rax, -176(%rbp)
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L748:
	movq	%r12, %r14
	jmp	.L729
.L753:
	movq	-192(%rbp), %rdx
	leaq	1(%rdx), %rax
	cmpq	%rdx, -184(%rbp)
	je	.L703
	movq	%rax, -192(%rbp)
	jmp	.L704
.L703:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	movl	-152(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -148(%rbp)
	testq	%r12, %r12
	je	.L754
	movl	-152(%rbp), %eax
	movq	-216(%rbp), %r8
	movq	%r12, %rdi
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	leal	2(%rax), %ecx
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movq	%r12, 32(%r14)
.L726:
	movl	-152(%rbp), %eax
	movq	%r14, -160(%rbp)
	xorl	%ebx, %ebx
	movq	-216(%rbp), %r15
	leal	4(%rax), %r13d
	jmp	.L710
.L709:
	movq	-160(%rbp), %rax
	movq	32(%rax), %r12
.L710:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L707
	movl	-152(%rbp), %esi
	movq	%rax, %rdi
	movl	%r13d, %ecx
	xorl	%edx, %edx
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeStringC1Eiii@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	cmpl	-148(%rbp), %ebx
	jle	.L709
.L751:
	movq	-160(%rbp), %r14
	movq	32(%r14), %r12
.L706:
	movq	%r12, %rdi
	movl	$1, %esi
	xorl	%r12d, %r12d
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	-152(%rbp), %r13d
	movq	%rax, %rbx
	movl	-152(%rbp), %eax
	testl	%eax, %eax
	jle	.L714
	.p2align 4,,10
	.p2align 3
.L711:
	leal	2(%r12), %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	addl	$1, %r12d
	movzwl	%dx, %edx
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
	cmpl	%r12d, %r13d
	jne	.L711
.L714:
	cmpl	$1, -148(%rbp)
	jle	.L712
	movl	-148(%rbp), %r13d
	movl	$2, %r12d
	addl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L716:
	movq	32(%r14), %rdi
	movl	%r12d, %esi
	addl	$1, %r12d
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	cmpl	%r13d, %r12d
	jne	.L716
.L712:
	xorl	%ebx, %ebx
	jmp	.L715
.L755:
	movswl	%cx, %edx
	leal	(%rbx,%rbx), %eax
	sarl	$5, %edx
	cmpl	%eax, %edx
	jle	.L719
.L756:
	movl	$65535, %r12d
	jbe	.L720
	leaq	-118(%rbp), %rsi
	testb	$2, %cl
	cmove	-104(%rbp), %rsi
	movzwl	(%rsi,%rbx,4), %r12d
.L720:
	addl	$1, %eax
	movl	$65537, %esi
	cmpl	%eax, %edx
	jbe	.L722
	andl	$2, %ecx
	leaq	-118(%rbp), %rax
	cmove	-104(%rbp), %rax
	movzwl	2(%rax,%rbx,4), %esi
	addl	$2, %esi
.L722:
	movq	32(%r14), %rdi
	addq	$1, %rbx
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
.L715:
	movzwl	-120(%rbp), %ecx
	testw	%cx, %cx
	jns	.L755
	movl	-116(%rbp), %edx
	leal	(%rbx,%rbx), %eax
	cmpl	%eax, %edx
	jg	.L756
.L719:
	movq	$1, -136(%rbp)
	movq	-200(%rbp), %rbx
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L757:
	movq	-136(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6716RBBITableBuilder15removeSafeStateESt4pairIiiE
.L725:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6716RBBITableBuilder22findDuplicateSafeStateEPSt4pairIiiE
	testb	%al, %al
	jne	.L757
	movq	-208(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L758
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L695:
	.cfi_restore_state
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	movl	-152(%rbp), %eax
	leal	1(%rax), %ebx
	movl	%ebx, -148(%rbp)
	testq	%r12, %r12
	je	.L705
	movq	-216(%rbp), %r8
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	leal	2(%rax), %ecx
	movq	%r12, %rdi
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	leaq	-136(%rbp), %rax
	movq	%r12, 32(%r14)
	movq	%rax, -200(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -208(%rbp)
	testl	%ebx, %ebx
	js	.L706
	jmp	.L726
.L705:
	cmpl	$0, -148(%rbp)
	movq	$0, 32(%r14)
	js	.L730
	leaq	-136(%rbp), %rax
	movq	%rax, -200(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -208(%rbp)
	jmp	.L726
.L758:
	call	__stack_chk_fail@PLT
.L730:
	movl	$1, %esi
	xorl	%edi, %edi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	leaq	-136(%rbp), %rax
	movq	%rax, -200(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -208(%rbp)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L707:
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	cmpl	%ebx, -148(%rbp)
	jge	.L709
	jmp	.L751
.L754:
	movq	$0, 32(%r14)
	jmp	.L726
	.cfi_endproc
.LFE3209:
	.size	_ZN6icu_6716RBBITableBuilder21buildSafeReverseTableER10UErrorCode, .-_ZN6icu_6716RBBITableBuilder21buildSafeReverseTableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716RBBITableBuilder16getSafeTableSizeEv
	.type	_ZNK6icu_6716RBBITableBuilder16getSafeTableSizeEv, @function
_ZNK6icu_6716RBBITableBuilder16getSafeTableSizeEv:
.LFB3210:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L763
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movl	8(%rdx), %ebx
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv@PLT
	addq	$8, %rsp
	leal	8(%rax,%rax), %eax
	imull	%ebx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	addl	$16, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3210:
	.size	_ZNK6icu_6716RBBITableBuilder16getSafeTableSizeEv, .-_ZNK6icu_6716RBBITableBuilder16getSafeTableSizeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder15exportSafeTableEPv
	.type	_ZN6icu_6716RBBITableBuilder15exportSafeTableEPv, @function
_ZN6icu_6716RBBITableBuilder15exportSafeTableEPv:
.LFB3211:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L799
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	$0, 32(%rdi)
	je	.L768
	movq	(%rdi), %rax
	movq	%rsi, %r15
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv@PLT
	movl	%eax, %ebx
	cmpl	$32767, %eax
	jg	.L770
	movq	32(%r14), %rdi
	cmpl	$32767, 8(%rdi)
	jg	.L770
	leal	8(%rax,%rax), %eax
	movl	%eax, 4(%r15)
	movl	8(%rdi), %eax
	movq	$0, 8(%r15)
	movl	%eax, (%r15)
	testl	%eax, %eax
	je	.L768
	leal	-1(%rbx), %eax
	leaq	16(%r15), %r12
	xorl	%r13d, %r13d
	movl	%eax, -52(%rbp)
	.p2align 4,,10
	.p2align 3
.L792:
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r13d, %ecx
	imull	4(%r15), %ecx
	addq	%r12, %rcx
	movq	$0, (%rcx)
	testl	%ebx, %ebx
	jle	.L791
	movswl	8(%rax), %edi
	movl	%edi, %edx
	sarl	$5, %edi
	movl	%edx, %r8d
	andl	$2, %r8d
	testw	%dx, %dx
	js	.L802
	movl	-52(%rbp), %r9d
	xorl	%edx, %edx
	testw	%r8w, %r8w
	jne	.L790
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L803:
	movq	24(%rax), %r8
	movzwl	(%r8,%rdx,2), %r8d
	movw	%r8w, 8(%rcx,%rdx,2)
	leaq	1(%rdx), %r8
	cmpq	%r9, %rdx
	je	.L791
.L786:
	movq	%r8, %rdx
.L787:
	cmpl	%edx, %edi
	ja	.L803
	movl	$-1, %r8d
	movw	%r8w, 8(%rcx,%rdx,2)
	leaq	1(%rdx), %r8
	cmpq	%r9, %rdx
	jne	.L786
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L770:
	movq	16(%r14), %rax
	movl	$66048, (%rax)
.L768:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L804:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movzwl	10(%rax,%rdx,2), %r8d
	movw	%r8w, 8(%rcx,%rdx,2)
	leaq	1(%rdx), %r8
	cmpq	%r9, %rdx
	je	.L791
.L789:
	movq	%r8, %rdx
.L790:
	cmpl	%edx, %edi
	ja	.L804
	movl	$-1, %esi
	leaq	1(%rdx), %r8
	movw	%si, 8(%rcx,%rdx,2)
	cmpq	%r9, %rdx
	jne	.L789
	.p2align 4,,10
	.p2align 3
.L791:
	addl	$1, %r13d
	cmpl	%r13d, (%r15)
	jbe	.L768
	movq	32(%r14), %rdi
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L802:
	testw	%r8w, %r8w
	movl	$0, %edx
	movl	-52(%rbp), %r8d
	jne	.L783
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L805:
	movq	24(%rax), %rdi
	movzwl	(%rdi,%rdx,2), %edi
	movw	%di, 8(%rcx,%rdx,2)
	leaq	1(%rdx), %rdi
	cmpq	%r8, %rdx
	je	.L791
.L779:
	movq	%rdi, %rdx
.L780:
	cmpl	%edx, 12(%rax)
	ja	.L805
	movl	$-1, %r10d
	leaq	1(%rdx), %rdi
	movw	%r10w, 8(%rcx,%rdx,2)
	cmpq	%r8, %rdx
	jne	.L779
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L806:
	movzwl	10(%rax,%rdx,2), %edi
	movw	%di, 8(%rcx,%rdx,2)
	leaq	1(%rdx), %rdi
	cmpq	%rdx, %r8
	je	.L791
.L782:
	movq	%rdi, %rdx
.L783:
	cmpl	%edx, 12(%rax)
	ja	.L806
	movl	$-1, %r9d
	leaq	1(%rdx), %rdi
	movw	%r9w, 8(%rcx,%rdx,2)
	cmpq	%rdx, %r8
	jne	.L782
	jmp	.L791
	.cfi_endproc
.LFE3211:
	.size	_ZN6icu_6716RBBITableBuilder15exportSafeTableEPv, .-_ZN6icu_6716RBBITableBuilder15exportSafeTableEPv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719RBBIStateDescriptorC2EiP10UErrorCode
	.type	_ZN6icu_6719RBBIStateDescriptorC2EiP10UErrorCode, @function
_ZN6icu_6719RBBIStateDescriptorC2EiP10UErrorCode:
.LFB3213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movb	$0, (%rdi)
	movq	$0, 4(%rdi)
	movl	$0, 24(%rdi)
	movq	$0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L808
	addl	$1, %r13d
	movq	%rax, %rdi
	movq	%r14, %rdx
	movq	%rax, %r12
	movl	%r13d, %esi
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movl	(%r14), %eax
	movq	%r12, 40(%rbx)
	testl	%eax, %eax
	jg	.L807
	popq	%rbx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679UVector327setSizeEi@PLT
.L808:
	.cfi_restore_state
	cmpl	$0, (%r14)
	movq	$0, 40(%rbx)
	jg	.L807
	movl	$7, (%r14)
.L807:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3213:
	.size	_ZN6icu_6719RBBIStateDescriptorC2EiP10UErrorCode, .-_ZN6icu_6719RBBIStateDescriptorC2EiP10UErrorCode
	.globl	_ZN6icu_6719RBBIStateDescriptorC1EiP10UErrorCode
	.set	_ZN6icu_6719RBBIStateDescriptorC1EiP10UErrorCode,_ZN6icu_6719RBBIStateDescriptorC2EiP10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719RBBIStateDescriptorD2Ev
	.type	_ZN6icu_6719RBBIStateDescriptorD2Ev, @function
_ZN6icu_6719RBBIStateDescriptorD2Ev:
.LFB3216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L816
	movq	(%rdi), %rax
	call	*8(%rax)
.L816:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L817
	movq	(%rdi), %rax
	call	*8(%rax)
.L817:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L815
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3216:
	.size	_ZN6icu_6719RBBIStateDescriptorD2Ev, .-_ZN6icu_6719RBBIStateDescriptorD2Ev
	.globl	_ZN6icu_6719RBBIStateDescriptorD1Ev
	.set	_ZN6icu_6719RBBIStateDescriptorD1Ev,_ZN6icu_6719RBBIStateDescriptorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder20calcChainedFollowPosEPNS_8RBBINodeES2_
	.type	_ZN6icu_6716RBBITableBuilder20calcChainedFollowPosEPNS_8RBBINodeES2_, @function
_ZN6icu_6716RBBITableBuilder20calcChainedFollowPosEPNS_8RBBINodeES2_:
.LFB3173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	%rdx, -216(%rbp)
	movq	16(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	16(%rbx), %rcx
	movl	(%rcx), %r9d
	testl	%r9d, %r9d
	jle	.L866
.L838:
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
.L826:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L867
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	movq	%r14, %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN6icu_678RBBINode9findNodesEPNS_7UVectorENS0_8NodeTypeER10UErrorCode@PLT
	movq	16(%rbx), %rsi
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jg	.L838
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	testq	%r12, %r12
	je	.L828
.L831:
	movq	16(%rbx), %rsi
	movl	(%rsi), %edi
	testl	%edi, %edi
	jg	.L829
	cmpb	$0, 129(%r12)
	jne	.L868
	movq	16(%r12), %rdx
	movq	-208(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6716RBBITableBuilder16addRuleRootNodesEPNS_7UVectorEPNS_8RBBINodeE
	movq	24(%r12), %r12
	testq	%r12, %r12
	jne	.L831
.L828:
	movq	16(%rbx), %rsi
.L829:
	leaq	-96(%rbp), %r13
	xorl	%r12d, %r12d
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movl	-136(%rbp), %esi
	testl	%esi, %esi
	jle	.L837
	.p2align 4,,10
	.p2align 3
.L832:
	movq	-208(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpb	$0, 130(%rax)
	jne	.L869
	addl	$1, %r12d
	cmpl	-136(%rbp), %r12d
	jl	.L832
.L837:
	movq	16(%rbx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L870
	movl	$0, -196(%rbp)
	movl	-184(%rbp), %edx
	testl	%edx, %edx
	jle	.L842
	.p2align 4,,10
	.p2align 3
.L839:
	movl	-196(%rbp), %esi
	movq	%r14, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-216(%rbp), %rsi
	xorl	%edx, %edx
	movq	152(%rax), %rdi
	movq	%rax, %r12
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	js	.L845
	movq	(%rbx), %rax
	cmpb	$0, 153(%rax)
	je	.L846
	movq	160(%rax), %rdi
	movl	124(%r12), %esi
	call	_ZNK6icu_6714RBBISetBuilder12getFirstCharEi@PLT
	movl	%eax, %edi
	cmpl	$-1, %eax
	je	.L846
	movl	$4104, %esi
	call	u_getIntPropertyValue_67@PLT
	cmpl	$9, %eax
	je	.L845
.L846:
	movl	-88(%rbp), %eax
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.L845
	.p2align 4,,10
	.p2align 3
.L844:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpl	$3, (%rax)
	jne	.L847
	movl	124(%rax), %ecx
	cmpl	%ecx, 124(%r12)
	je	.L871
.L847:
	addl	$1, %r15d
	cmpl	-88(%rbp), %r15d
	jl	.L844
.L845:
	addl	$1, -196(%rbp)
	movl	-196(%rbp), %eax
	cmpl	-184(%rbp), %eax
	jl	.L839
.L842:
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	-208(%rbp), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L871:
	movq	152(%rax), %rdx
	movq	152(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L869:
	movq	136(%rax), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	addl	$1, %r12d
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	cmpl	%r12d, -136(%rbp)
	jg	.L832
	jmp	.L837
.L868:
	movq	-208(%rbp), %rdi
	movq	%rsi, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	16(%rbx), %rsi
	jmp	.L829
.L870:
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	-208(%rbp), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L838
.L867:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3173:
	.size	_ZN6icu_6716RBBITableBuilder20calcChainedFollowPosEPNS_8RBBINodeES2_, .-_ZN6icu_6716RBBITableBuilder20calcChainedFollowPosEPNS_8RBBINodeES2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder19flagAcceptingStatesEv
	.type	_ZN6icu_6716RBBITableBuilder19flagAcceptingStatesEv, @function
_ZN6icu_6716RBBITableBuilder19flagAcceptingStatesEv:
.LFB3177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jle	.L905
.L872:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L906
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L905:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	movq	%rdi, %r12
	movq	%r15, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	16(%r12), %rcx
	movl	(%rcx), %esi
	testl	%esi, %esi
	jle	.L907
.L874:
	movq	%r15, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L907:
	movq	8(%r12), %rax
	movl	$6, %edx
	movq	%r15, %rsi
	movq	(%rax), %rdi
	call	_ZN6icu_678RBBINode9findNodesEPNS_7UVectorENS0_8NodeTypeER10UErrorCode@PLT
	movq	16(%r12), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L874
	movl	-88(%rbp), %edx
	movl	$0, -100(%rbp)
	testl	%edx, %edx
	jle	.L874
	.p2align 4,,10
	.p2align 3
.L875:
	movl	-100(%rbp), %esi
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	24(%r12), %rdi
	movq	%rax, %r13
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jg	.L877
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L882:
	movq	40(%r12), %rsi
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jle	.L884
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L884
	movq	24(%rsi), %rsi
	movslq	%edx, %rax
	movl	(%rsi,%rax,4), %eax
	movl	%eax, 4(%rbx)
	testl	%eax, %eax
	je	.L884
	.p2align 4,,10
	.p2align 3
.L881:
	cmpl	$-1, %eax
	je	.L908
.L880:
	movq	24(%r12), %rdi
	addl	$1, %r14d
	cmpl	8(%rdi), %r14d
	jge	.L887
.L877:
	movl	%r14d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	32(%rax), %rdi
	movq	%rax, %rbx
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	js	.L880
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jne	.L881
	movslq	124(%r13), %rdx
	testl	%edx, %edx
	jns	.L882
.L884:
	movl	$-1, 4(%rbx)
.L883:
	testl	%edx, %edx
	je	.L880
	movl	$0, %esi
	js	.L886
	movq	40(%r12), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L886
	subl	%edx, %eax
	testl	%eax, %eax
	jle	.L886
	movq	24(%rdi), %rax
	movl	(%rax,%rdx,4), %esi
.L886:
	movq	24(%r12), %rdi
	movl	%esi, 4(%rbx)
	addl	$1, %r14d
	cmpl	8(%rdi), %r14d
	jl	.L877
	.p2align 4,,10
	.p2align 3
.L887:
	addl	$1, -100(%rbp)
	movl	-100(%rbp), %eax
	cmpl	-88(%rbp), %eax
	jl	.L875
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L908:
	movslq	124(%r13), %rdx
	jmp	.L883
.L906:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3177:
	.size	_ZN6icu_6716RBBITableBuilder19flagAcceptingStatesEv, .-_ZN6icu_6716RBBITableBuilder19flagAcceptingStatesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder19flagLookAheadStatesEv
	.type	_ZN6icu_6716RBBITableBuilder19flagLookAheadStatesEv, @function
_ZN6icu_6716RBBITableBuilder19flagLookAheadStatesEv:
.LFB3178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jle	.L926
.L909:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L927
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L926:
	.cfi_restore_state
	leaq	-96(%rbp), %rax
	movq	%rdi, %r13
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rax, -104(%rbp)
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	8(%r13), %rax
	movq	16(%r13), %rcx
	movq	%rbx, %rsi
	movl	$4, %edx
	movq	(%rax), %rdi
	call	_ZN6icu_678RBBINode9findNodesEPNS_7UVectorENS0_8NodeTypeER10UErrorCode@PLT
	movq	16(%r13), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L912
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	jle	.L912
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L917:
	movq	-104(%rbp), %rdi
	movl	%r14d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	24(%r13), %rdi
	movq	%rax, %r12
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L913
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L916:
	movl	%r15d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	32(%rax), %rdi
	movq	%rax, %rbx
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	js	.L914
	movslq	124(%r12), %rax
	xorl	%esi, %esi
	testl	%eax, %eax
	js	.L915
	movq	40(%r13), %rdi
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jle	.L915
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L915
	movq	24(%rdi), %rdx
	movl	(%rdx,%rax,4), %esi
.L915:
	movl	%esi, 8(%rbx)
.L914:
	movq	24(%r13), %rdi
	addl	$1, %r15d
	cmpl	8(%rdi), %r15d
	jl	.L916
.L913:
	addl	$1, %r14d
	cmpl	-88(%rbp), %r14d
	jl	.L917
.L912:
	movq	-104(%rbp), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L909
.L927:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3178:
	.size	_ZN6icu_6716RBBITableBuilder19flagLookAheadStatesEv, .-_ZN6icu_6716RBBITableBuilder19flagLookAheadStatesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder16flagTaggedStatesEv
	.type	_ZN6icu_6716RBBITableBuilder16flagTaggedStatesEv, @function
_ZN6icu_6716RBBITableBuilder16flagTaggedStatesEv:
.LFB3179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jle	.L941
.L928:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L942
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L941:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	movq	%rdi, %r12
	movq	%r15, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	16(%r12), %rcx
	movl	(%rcx), %esi
	testl	%esi, %esi
	jle	.L943
.L930:
	movq	%r15, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L943:
	movq	8(%r12), %rax
	movl	$5, %edx
	movq	%r15, %rsi
	movq	(%rax), %rdi
	call	_ZN6icu_678RBBINode9findNodesEPNS_7UVectorENS0_8NodeTypeER10UErrorCode@PLT
	movq	16(%r12), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L930
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	jle	.L930
	movl	$0, -100(%rbp)
	.p2align 4,,10
	.p2align 3
.L936:
	movl	-100(%rbp), %esi
	movq	%r15, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	24(%r12), %rdi
	movq	%rax, %r13
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L932
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L935:
	movl	%r14d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	32(%rax), %rdi
	movq	%rax, %rbx
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	jns	.L944
	movq	24(%r12), %rdi
	addl	$1, %r14d
	cmpl	8(%rdi), %r14d
	jl	.L935
.L932:
	addl	$1, -100(%rbp)
	movl	-100(%rbp), %eax
	cmpl	-88(%rbp), %eax
	jl	.L936
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L944:
	movl	124(%r13), %edx
	movq	%r12, %rdi
	leaq	16(%rbx), %rsi
	addl	$1, %r14d
	call	_ZN6icu_6716RBBITableBuilder9sortedAddEPPNS_7UVectorEi
	movq	24(%r12), %rdi
	cmpl	%r14d, 8(%rdi)
	jg	.L935
	addl	$1, -100(%rbp)
	movl	-100(%rbp), %eax
	cmpl	-88(%rbp), %eax
	jl	.L936
	jmp	.L930
.L942:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3179:
	.size	_ZN6icu_6716RBBITableBuilder16flagTaggedStatesEv, .-_ZN6icu_6716RBBITableBuilder16flagTaggedStatesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716RBBITableBuilder17buildForwardTableEv
	.type	_ZN6icu_6716RBBITableBuilder17buildForwardTableEv, @function
_ZN6icu_6716RBBITableBuilder17buildForwardTableEv:
.LFB3167:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L992
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rdi), %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L945
	call	_ZN6icu_678RBBINode16flattenVariablesEv@PLT
	movq	%rax, (%rbx)
	movq	(%r12), %rax
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder6sawBOFEv@PLT
	testb	%al, %al
	jne	.L995
.L949:
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L953
	movq	%rax, %rdi
	movl	$8, %esi
	call	_ZN6icu_678RBBINodeC1ENS0_8NodeTypeE@PLT
	movq	8(%r12), %rax
	movl	$160, %edi
	movq	(%rax), %rax
	movq	%rax, 16(%r13)
	movq	%r13, 8(%rax)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L955
	movl	$6, %esi
	movq	%rax, %rdi
	call	_ZN6icu_678RBBINodeC1ENS0_8NodeTypeE@PLT
	movq	8(%r12), %rax
	movq	%r14, 24(%r13)
	movq	%r13, %rdi
	movq	%r13, 8(%r14)
	movq	%r13, (%rax)
	call	_ZN6icu_678RBBINode11flattenSetsEv@PLT
	movq	8(%r12), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN6icu_6716RBBITableBuilder12calcNullableEPNS_8RBBINodeE
	movq	8(%r12), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L971
	movl	(%rsi), %eax
	subl	$3, %eax
	cmpl	$3, %eax
	ja	.L957
	movq	16(%r12), %rdx
	movq	136(%rsi), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L958:
	movq	8(%r12), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L971
	movl	(%rsi), %eax
	subl	$3, %eax
	cmpl	$3, %eax
	ja	.L959
	movq	16(%r12), %rdx
	movq	144(%rsi), %rdi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
.L960:
	movq	8(%r12), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L971
	movl	(%rsi), %eax
	cmpl	$6, %eax
	je	.L971
	cmpl	$3, %eax
	je	.L971
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder13calcFollowPosEPNS_8RBBINodeE.part.0
.L971:
	movq	(%r12), %rax
	cmpb	$0, 152(%rax)
	jne	.L996
.L962:
	movq	160(%rax), %rdi
	call	_ZNK6icu_6714RBBISetBuilder6sawBOFEv@PLT
	testb	%al, %al
	je	.L970
	movq	16(%r12), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L967
	movq	8(%r12), %rax
	xorl	%r13d, %r13d
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %r14
	movq	24(%rax), %rax
	movq	136(%rax), %rbx
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jle	.L965
	.p2align 4,,10
	.p2align 3
.L968:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	cmpl	$3, (%rax)
	jne	.L969
	movl	124(%r14), %ecx
	cmpl	%ecx, 124(%rax)
	je	.L997
.L969:
	addl	$1, %r13d
	cmpl	%r13d, 8(%rbx)
	jg	.L968
.L970:
	movq	16(%r12), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L965
.L967:
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder17mapLookAheadRulesEv
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder19flagAcceptingStatesEv
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder19flagLookAheadStatesEv
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder16flagTaggedStatesEv
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716RBBITableBuilder19mergeRuleStatusValsEv
.L953:
	.cfi_restore_state
	movq	16(%r12), %rax
	movl	$7, (%rax)
.L945:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L992:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L995:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L950
	movq	%rax, %rdi
	movl	$8, %esi
	call	_ZN6icu_678RBBINodeC1ENS0_8NodeTypeE@PLT
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L990
	movq	%rax, %rdi
	movl	$3, %esi
	call	_ZN6icu_678RBBINodeC1ENS0_8NodeTypeE@PLT
	movq	8(%r12), %rax
	movq	%rbx, 16(%r13)
	movq	(%rax), %rdx
	movq	%rdx, 24(%r13)
	movq	%r13, 8(%rbx)
	movl	$2, 124(%rbx)
	movq	%r13, (%rax)
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L965:
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder15buildStateTableEv.part.0
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L957:
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder12calcFirstPosEPNS_8RBBINodeE.part.0
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L959:
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder11calcLastPosEPNS_8RBBINodeE.part.0
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L996:
	movq	8(%r12), %rax
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN6icu_6716RBBITableBuilder20calcChainedFollowPosEPNS_8RBBINodeES2_
	movq	(%r12), %rax
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L997:
	movq	152(%rax), %rdx
	movq	152(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilder6setAddEPNS_7UVectorES2_
	jmp	.L969
.L950:
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L953
	movl	$3, %esi
	movq	%rax, %rdi
	call	_ZN6icu_678RBBINodeC1ENS0_8NodeTypeE@PLT
.L990:
	movq	16(%r12), %rax
	movq	%r13, %rdi
	movl	$7, (%rax)
	call	_ZN6icu_678RBBINodeD1Ev@PLT
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
.L955:
	.cfi_restore_state
	movq	$0, 24(%r13)
	jmp	.L990
	.cfi_endproc
.LFE3167:
	.size	_ZN6icu_6716RBBITableBuilder17buildForwardTableEv, .-_ZN6icu_6716RBBITableBuilder17buildForwardTableEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
