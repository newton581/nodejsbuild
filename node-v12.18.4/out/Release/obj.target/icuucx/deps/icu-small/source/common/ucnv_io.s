	.file	"ucnv_io.cpp"
	.text
	.p2align 4
	.type	_ZL12isAcceptablePvPKcS1_PK9UDataInfo, @function
_ZL12isAcceptablePvPKcS1_PK9UDataInfo:
.LFB2785:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpw	$19, (%rcx)
	jbe	.L1
	cmpw	$0, 4(%rcx)
	jne	.L1
	cmpw	$30275, 8(%rcx)
	je	.L8
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpw	$27713, 10(%rcx)
	jne	.L1
	cmpb	$3, 12(%rcx)
	sete	%al
	ret
	.cfi_endproc
.LFE2785:
	.size	_ZL12isAcceptablePvPKcS1_PK9UDataInfo, .-_ZL12isAcceptablePvPKcS1_PK9UDataInfo
	.p2align 4
	.globl	ucnv_io_stripASCIIForCompare_67
	.type	ucnv_io_stripASCIIForCompare_67, @function
ucnv_io_stripASCIIForCompare_67:
.LFB2791:
	.cfi_startproc
	endbr64
	movzbl	(%rsi), %edx
	movq	%rdi, %rax
	leaq	1(%rsi), %rcx
	movq	%rdi, %r8
	xorl	%r9d, %r9d
	leaq	_ZL10asciiTypes(%rip), %r10
	testb	%dl, %dl
	jne	.L10
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L24:
	movsbq	%dl, %rsi
	movzbl	(%r10,%rsi), %esi
	cmpb	$1, %sil
	je	.L13
	cmpb	$2, %sil
	je	.L18
	movl	%esi, %edx
	xorl	%r9d, %r9d
	testb	%sil, %sil
	je	.L12
.L14:
	movb	%dl, (%r8)
	movzbl	(%rcx), %edx
	addq	$1, %r8
.L16:
	addq	$1, %rcx
	testb	%dl, %dl
	je	.L11
.L10:
	testb	%dl, %dl
	jns	.L24
.L12:
	movzbl	(%rcx), %edx
	xorl	%r9d, %r9d
	addq	$1, %rcx
	testb	%dl, %dl
	jne	.L10
.L11:
	movb	$0, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$1, %r9d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L13:
	testb	%r9b, %r9b
	jne	.L14
	movzbl	(%rcx), %esi
	testb	%sil, %sil
	js	.L14
	movsbq	%sil, %rdi
	movzbl	(%r10,%rdi), %edi
	subl	$1, %edi
	cmpb	$1, %dil
	ja	.L14
	movl	%esi, %edx
	jmp	.L16
	.cfi_endproc
.LFE2791:
	.size	ucnv_io_stripASCIIForCompare_67, .-ucnv_io_stripASCIIForCompare_67
	.p2align 4
	.globl	ucnv_io_stripEBCDICForCompare_67
	.type	ucnv_io_stripEBCDICForCompare_67, @function
ucnv_io_stripEBCDICForCompare_67:
.LFB2792:
	.cfi_startproc
	endbr64
	movzbl	(%rsi), %edx
	movq	%rdi, %rax
	leaq	1(%rsi), %rcx
	movq	%rdi, %r8
	xorl	%r9d, %r9d
	leaq	_ZL11ebcdicTypes(%rip), %r10
	testb	%dl, %dl
	jne	.L26
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rdx, %rsi
	andl	$127, %esi
	movzbl	(%r10,%rsi), %esi
	cmpb	$1, %sil
	je	.L29
	cmpb	$2, %sil
	je	.L35
	xorl	%r9d, %r9d
	movl	%esi, %edx
	testb	%sil, %sil
	je	.L28
.L30:
	movb	%dl, (%r8)
	addq	$1, %r8
.L28:
	movzbl	-1(%rcx), %edx
	testb	%dl, %dl
	je	.L27
.L26:
	addq	$1, %rcx
	testb	%dl, %dl
	js	.L44
	movzbl	-1(%rcx), %edx
	xorl	%r9d, %r9d
	testb	%dl, %dl
	jne	.L26
.L27:
	movb	$0, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$1, %r9d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L29:
	testb	%r9b, %r9b
	jne	.L30
	movzbl	-1(%rcx), %esi
	testb	%sil, %sil
	jns	.L30
	movq	%rsi, %rdi
	andl	$127, %edi
	movzbl	(%r10,%rdi), %edi
	subl	$1, %edi
	cmpb	$1, %dil
	ja	.L30
	movl	%esi, %edx
	jmp	.L26
	.cfi_endproc
.LFE2792:
	.size	ucnv_io_stripEBCDICForCompare_67, .-ucnv_io_stripEBCDICForCompare_67
	.p2align 4
	.type	ucnv_io_countStandardAliases, @function
ucnv_io_countStandardAliases:
.LFB2799:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	xorl	%r8d, %r8d
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L45
	movq	40+_ZL10gMainTable(%rip), %rdx
	movzwl	(%rdx,%rax,2), %r8d
.L45:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2799:
	.size	ucnv_io_countStandardAliases, .-ucnv_io_countStandardAliases
	.p2align 4
	.type	ucnv_io_resetStandardAliases, @function
ucnv_io_resetStandardAliases:
.LFB2801:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	$0, 4(%rax)
	ret
	.cfi_endproc
.LFE2801:
	.size	ucnv_io_resetStandardAliases, .-ucnv_io_resetStandardAliases
	.p2align 4
	.type	ucnv_io_countAllConverters, @function
ucnv_io_countAllConverters:
.LFB2815:
	.cfi_startproc
	endbr64
	movl	72+_ZL10gMainTable(%rip), %eax
	ret
	.cfi_endproc
.LFE2815:
	.size	ucnv_io_countAllConverters, .-ucnv_io_countAllConverters
	.p2align 4
	.type	ucnv_io_resetAllConverters, @function
ucnv_io_resetAllConverters:
.LFB2817:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	xorl	%edx, %edx
	movw	%dx, (%rax)
	ret
	.cfi_endproc
.LFE2817:
	.size	ucnv_io_resetAllConverters, .-ucnv_io_resetAllConverters
	.p2align 4
	.type	_ZL13initAliasDataR10UErrorCode, @function
_ZL13initAliasDataR10UErrorCode:
.LFB2787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL15ucnv_io_cleanupv(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$19, %edi
	subq	$8, %rsp
	call	ucln_common_registerCleanup_67@PLT
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	leaq	_ZL9DATA_NAME(%rip), %rdx
	leaq	_ZL12isAcceptablePvPKcS1_PK9UDataInfo(%rip), %rcx
	leaq	_ZL9DATA_TYPE(%rip), %rsi
	call	udata_openChoice_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L66
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%rax, %rdi
	movq	%rax, %r12
	call	udata_getMemory_67@PLT
	movl	(%rax), %r13d
	cmpl	$7, %r13d
	jbe	.L67
	movl	4(%rax), %r9d
	movq	%r12, _ZL10gAliasData(%rip)
	movl	%r9d, 72+_ZL10gMainTable(%rip)
	movl	8(%rax), %r8d
	movl	%r8d, 76+_ZL10gMainTable(%rip)
	movl	12(%rax), %edi
	movl	%edi, 80+_ZL10gMainTable(%rip)
	movl	16(%rax), %esi
	movl	%esi, 84+_ZL10gMainTable(%rip)
	movl	20(%rax), %ecx
	movl	%ecx, 88+_ZL10gMainTable(%rip)
	movl	24(%rax), %edx
	movl	%edx, 92+_ZL10gMainTable(%rip)
	movl	28(%rax), %r10d
	movl	%r10d, 96+_ZL10gMainTable(%rip)
	movl	32(%rax), %r11d
	movl	%r11d, 100+_ZL10gMainTable(%rip)
	cmpl	$8, %r13d
	je	.L55
	movl	36(%rax), %ebx
	movl	%ebx, 104+_ZL10gMainTable(%rip)
.L55:
	leal	2(%r13,%r13), %r12d
	movq	%r12, %rbx
	leaq	(%rax,%r12,2), %r12
	addl	%r9d, %ebx
	movq	%r12, _ZL10gMainTable(%rip)
	movq	%rbx, %r9
	leaq	(%rax,%rbx,2), %rbx
	addl	%r8d, %r9d
	movq	%rbx, 8+_ZL10gMainTable(%rip)
	movq	%r9, %r8
	leaq	(%rax,%r9,2), %r9
	addl	%edi, %r8d
	movq	%r9, 16+_ZL10gMainTable(%rip)
	movq	%r8, %rdi
	leaq	(%rax,%r8,2), %r8
	addl	%esi, %edi
	movq	%r8, 24+_ZL10gMainTable(%rip)
	movq	%rdi, %rsi
	leaq	(%rax,%rdi,2), %rdi
	addl	%ecx, %esi
	movq	%rdi, 32+_ZL10gMainTable(%rip)
	movq	%rsi, %rcx
	leaq	(%rax,%rsi,2), %rsi
	movq	%rsi, 40+_ZL10gMainTable(%rip)
	addl	%ecx, %edx
	testl	%r10d, %r10d
	je	.L68
	movl	%edx, %ecx
	leaq	(%rax,%rcx,2), %rcx
	movzwl	(%rcx), %esi
	cmpw	$1, %si
	jbe	.L58
	leaq	_ZL19defaultTableOptions(%rip), %rcx
	addl	%r10d, %edx
	movq	%rcx, 48+_ZL10gMainTable(%rip)
	leaq	(%rax,%rdx,2), %rcx
	movq	%rcx, 56+_ZL10gMainTable(%rip)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	_ZL19defaultTableOptions(%rip), %rcx
	movq	%rcx, 48+_ZL10gMainTable(%rip)
	leaq	(%rax,%rdx,2), %rcx
	movq	%rcx, 56+_ZL10gMainTable(%rip)
.L57:
	movq	%rcx, 64+_ZL10gMainTable(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movl	$3, (%rbx)
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	udata_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	%rcx, 48+_ZL10gMainTable(%rip)
	leal	(%rdx,%r10), %ecx
	movq	%rcx, %rdx
	leaq	(%rax,%rcx,2), %rcx
	movq	%rcx, 56+_ZL10gMainTable(%rip)
	testw	%si, %si
	je	.L57
	addl	%r11d, %edx
	leaq	(%rax,%rdx,2), %rcx
	jmp	.L57
	.cfi_endproc
.LFE2787:
	.size	_ZL13initAliasDataR10UErrorCode, .-_ZL13initAliasDataR10UErrorCode
	.p2align 4
	.type	ucnv_io_nextStandardAliases, @function
ucnv_io_nextStandardAliases:
.LFB2800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rdx
	movq	%rsi, %rbx
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L70
	movq	40+_ZL10gMainTable(%rip), %rsi
	movl	4(%rdx), %ecx
	movzwl	(%rsi,%rax,2), %edi
	cmpl	%edi, %ecx
	jb	.L81
.L70:
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	je	.L69
	movl	$0, (%rbx)
.L69:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	leaq	1(%rax,%rcx), %rax
	movq	56+_ZL10gMainTable(%rip), %rdi
	leal	1(%rcx), %r8d
	movzwl	(%rsi,%rax,2), %eax
	movl	%r8d, 4(%rdx)
	leaq	(%rdi,%rax,2), %r12
	testq	%rbx, %rbx
	je	.L69
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, (%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2800:
	.size	ucnv_io_nextStandardAliases, .-ucnv_io_nextStandardAliases
	.p2align 4
	.type	ucnv_io_nextAllConverters, @function
ucnv_io_nextAllConverters:
.LFB2816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rdx
	movq	%rsi, %rbx
	movzwl	(%rdx), %ecx
	cmpl	72+_ZL10gMainTable(%rip), %ecx
	jb	.L91
	xorl	%r12d, %r12d
	testq	%rsi, %rsi
	je	.L82
	movl	$0, (%rsi)
.L82:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	%rcx, %rax
	movq	_ZL10gMainTable(%rip), %rsi
	movq	56+_ZL10gMainTable(%rip), %rcx
	leal	1(%rax), %edi
	movw	%di, (%rdx)
	movzwl	(%rsi,%rax,2), %eax
	leaq	(%rcx,%rax,2), %r12
	testq	%rbx, %rbx
	je	.L82
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, (%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2816:
	.size	ucnv_io_nextAllConverters, .-ucnv_io_nextAllConverters
	.p2align 4
	.type	_ZL14io_compareRowsPKvS0_S0_, @function
_ZL14io_compareRowsPKvS0_S0_:
.LFB2820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$144, %rsp
	movzwl	(%rdx), %esi
	movq	(%rdi), %r12
	leaq	-112(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	%esi, %esi
	movslq	%esi, %rsi
	addq	%r12, %rsi
	call	*24(%rbx)
	movzwl	(%r14), %esi
	leaq	-176(%rbp), %rdi
	movq	%rax, %r13
	addl	%esi, %esi
	movslq	%esi, %rsi
	addq	%r12, %rsi
	call	*24(%rbx)
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	strcmp@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L95
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L95:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2820:
	.size	_ZL14io_compareRowsPKvS0_S0_, .-_ZL14io_compareRowsPKvS0_S0_
	.p2align 4
	.type	ucnv_io_closeUEnumeration, @function
ucnv_io_closeUEnumeration:
.LFB2802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2802:
	.size	ucnv_io_closeUEnumeration, .-ucnv_io_closeUEnumeration
	.p2align 4
	.type	_ZL15ucnv_io_cleanupv, @function
_ZL15ucnv_io_cleanupv:
.LFB2786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZL10gAliasData(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L99
	call	udata_close_67@PLT
	movq	$0, _ZL10gAliasData(%rip)
.L99:
	leaq	_ZL10gMainTable(%rip), %rdx
	xorl	%eax, %eax
	movl	$14, %ecx
	movl	$0, _ZL18gAliasDataInitOnce(%rip)
	movq	%rdx, %rdi
	mfence
	rep stosq
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2786:
	.size	_ZL15ucnv_io_cleanupv, .-_ZL15ucnv_io_cleanupv
	.p2align 4
	.globl	ucnv_compareNames_67
	.type	ucnv_compareNames_67, @function
ucnv_compareNames_67:
.LFB2793:
	.cfi_startproc
	endbr64
	movsbq	(%rsi), %rdx
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	leaq	1(%rsi), %rcx
	leaq	_ZL10asciiTypes(%rip), %r8
.L105:
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	movq	%rcx, %rsi
	testb	%al, %al
	je	.L106
.L135:
	js	.L123
	movsbq	%al, %r11
	movzbl	(%r8,%r11), %r11d
	cmpb	$1, %r11b
	je	.L108
	cmpb	$2, %r11b
	je	.L109
	movl	%r11d, %eax
	xorl	%r10d, %r10d
	testb	%r11b, %r11b
	je	.L123
.L111:
	testb	%dl, %dl
	je	.L134
	.p2align 4,,10
	.p2align 3
.L113:
	testb	%dl, %dl
	js	.L115
	movsbl	%dl, %ecx
	movzbl	(%r8,%rdx), %edx
	cmpb	$1, %dl
	je	.L116
	cmpb	$2, %dl
	je	.L125
	testb	%dl, %dl
	je	.L115
	movzbl	%al, %eax
	xorl	%r9d, %r9d
	subl	%edx, %eax
	jne	.L104
.L136:
	movzbl	(%rdi), %eax
	leaq	1(%rsi), %rcx
	movsbq	(%rsi), %rdx
	addq	$1, %rdi
	movq	%rcx, %rsi
	testb	%al, %al
	jne	.L135
.L106:
	testb	%dl, %dl
	jne	.L113
.L132:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	testb	%r9b, %r9b
	jne	.L112
	movsbq	(%rsi), %rdx
	testb	%dl, %dl
	js	.L112
	movzbl	(%r8,%rdx), %edx
	subl	$1, %edx
	cmpb	$1, %dl
	ja	.L112
	.p2align 4,,10
	.p2align 3
.L115:
	movsbq	(%rsi), %rdx
	addq	$1, %rsi
	xorl	%r9d, %r9d
	testb	%dl, %dl
	jne	.L113
	testb	%al, %al
	je	.L132
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L112:
	movl	%ecx, %edx
.L137:
	movzbl	%al, %eax
	subl	%edx, %eax
	je	.L136
.L104:
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$1, %r9d
	movl	%ecx, %edx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L123:
	xorl	%r10d, %r10d
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L108:
	testb	%r10b, %r10b
	jne	.L111
	movsbq	(%rdi), %r11
	testb	%r11b, %r11b
	js	.L111
	movzbl	(%r8,%r11), %r11d
	subl	$1, %r11d
	cmpb	$1, %r11b
	ja	.L111
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$1, %r10d
	testb	%dl, %dl
	jne	.L113
	xorl	%ecx, %ecx
	jmp	.L112
.L134:
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE2793:
	.size	ucnv_compareNames_67, .-ucnv_compareNames_67
	.p2align 4
	.type	_ZL26findTaggedAliasListsOffsetPKcS0_P10UErrorCode, @function
_ZL26findTaggedAliasListsOffsetPKcS0_P10UErrorCode:
.LFB2796:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8+_ZL10gMainTable(%rip), %rax
	testq	%rax, %rax
	je	.L139
	movl	76+_ZL10gMainTable(%rip), %edx
	testl	%edx, %edx
	je	.L139
	movq	%rsi, %r13
	xorl	%ebx, %ebx
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L210:
	addl	$1, %ebx
	cmpl	%ebx, 76+_ZL10gMainTable(%rip)
	jbe	.L139
	movq	8+_ZL10gMainTable(%rip), %rax
.L141:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movzwl	(%rax,%rdx,2), %edx
	movq	56+_ZL10gMainTable(%rip), %rax
	leaq	(%rax,%rdx,2), %rdi
	call	uprv_stricmp_67@PLT
	testl	%eax, %eax
	jne	.L210
	movq	48+_ZL10gMainTable(%rip), %rax
	cmpw	$0, (%rax)
	jne	.L211
.L142:
	movl	84+_ZL10gMainTable(%rip), %eax
	xorl	%r14d, %r14d
	movl	%eax, %r15d
	movl	%eax, -132(%rbp)
	movq	16+_ZL10gMainTable(%rip), %rax
	shrl	%r15d
	movq	%rax, -144(%rbp)
	movq	56+_ZL10gMainTable(%rip), %rax
	movq	%rax, -152(%rbp)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L212:
	je	.L152
	movl	-132(%rbp), %eax
	movl	%r15d, %r14d
	addl	%r14d, %eax
	shrl	%eax
	cmpl	%r15d, %eax
	je	.L153
.L213:
	movl	%eax, %r15d
.L166:
	movq	-144(%rbp), %rdx
	movl	%r15d, %eax
	movq	-152(%rbp), %rdi
	leaq	(%rax,%rax), %r13
	movzwl	(%rdx,%rax,2), %eax
	leaq	(%rdi,%rax,2), %rsi
	movq	%r12, %rdi
	call	ucnv_compareNames_67
	testl	%eax, %eax
	jns	.L212
	movl	%r15d, -132(%rbp)
	movl	-132(%rbp), %eax
	addl	%r14d, %eax
	shrl	%eax
	cmpl	%r15d, %eax
	jne	.L213
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$-1, %eax
.L138:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L214
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	48+_ZL10gMainTable(%rip), %rax
	movl	$-1, %ebx
	cmpw	$0, (%rax)
	je	.L142
.L211:
	movq	%r12, %rdi
	call	strlen@PLT
	cmpq	$59, %rax
	ja	.L143
	movzbl	(%r12), %edx
	leaq	-128(%rbp), %rax
	leaq	1(%r12), %rsi
	xorl	%r8d, %r8d
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	leaq	_ZL10asciiTypes(%rip), %r9
	testb	%dl, %dl
	jne	.L144
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L215:
	movsbq	%al, %rcx
	movzbl	(%r9,%rcx), %ecx
	cmpb	$1, %cl
	je	.L148
	cmpb	$2, %cl
	je	.L170
	xorl	%r8d, %r8d
	movl	%ecx, %eax
	testb	%cl, %cl
	je	.L147
.L149:
	movb	%al, (%rdi)
	addq	$1, %rdi
.L147:
	addq	$1, %rsi
	testb	%dl, %dl
	je	.L145
.L144:
	movl	%edx, %eax
	movzbl	(%rsi), %edx
	testb	%al, %al
	jns	.L215
	xorl	%r8d, %r8d
	addq	$1, %rsi
	testb	%dl, %dl
	jne	.L144
.L145:
	movq	64+_ZL10gMainTable(%rip), %rax
	movl	84+_ZL10gMainTable(%rip), %ecx
	movb	$0, (%rdi)
	xorl	%r15d, %r15d
	movq	%rax, -152(%rbp)
	movq	16+_ZL10gMainTable(%rip), %rax
	movl	%ecx, %r14d
	shrl	%r14d
	movq	%rax, -144(%rbp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L216:
	je	.L152
	movl	-132(%rbp), %ecx
	movl	%r14d, %r15d
.L151:
	leal	(%rcx,%r15), %eax
	shrl	%eax
	cmpl	%r14d, %eax
	je	.L153
	movl	%eax, %r14d
.L154:
	movq	-144(%rbp), %rdi
	movl	%r14d, %eax
	movl	%ecx, -132(%rbp)
	leaq	(%rax,%rax), %r13
	movq	-152(%rbp), %rcx
	movzwl	(%rdi,%rax,2), %eax
	movq	-160(%rbp), %rdi
	leaq	(%rcx,%rax,2), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L216
	movl	%r14d, %ecx
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L170:
	movl	$1, %r8d
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L148:
	testb	%r8b, %r8b
	jne	.L149
	testb	%dl, %dl
	js	.L149
	movsbq	%dl, %rcx
	movzbl	(%r9,%rcx), %ecx
	subl	$1, %ecx
	cmpb	$1, %cl
	ja	.L149
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L152:
	movq	24+_ZL10gMainTable(%rip), %rax
	xorl	%ecx, %ecx
	movzwl	(%rax,%r13), %eax
	movl	%eax, %esi
	andl	$4095, %esi
	testw	%ax, %ax
	js	.L217
.L157:
	movl	76+_ZL10gMainTable(%rip), %eax
	leal	-1(%rax), %edx
	movl	$-1, %eax
	cmpl	%ebx, %edx
	jbe	.L138
	movl	72+_ZL10gMainTable(%rip), %edi
	movl	%edi, -136(%rbp)
	cmpl	%esi, %edi
	jbe	.L138
	imull	%edi, %ebx
	movq	32+_ZL10gMainTable(%rip), %r13
	leal	(%rbx,%rsi), %eax
	movl	%ebx, -172(%rbp)
	movzwl	0(%r13,%rax,2), %eax
	testl	%eax, %eax
	je	.L159
	movq	40+_ZL10gMainTable(%rip), %rsi
	leal	1(%rax), %edx
	cmpw	$0, (%rsi,%rdx,2)
	jne	.L138
.L159:
	xorl	%eax, %eax
	cmpl	$-122, %ecx
	jne	.L138
	movl	88+_ZL10gMainTable(%rip), %eax
	testl	%eax, %eax
	je	.L138
	movq	40+_ZL10gMainTable(%rip), %rcx
	movq	56+_ZL10gMainTable(%rip), %rbx
	subl	$1, %eax
	movq	%rax, -152(%rbp)
	leaq	2(%rcx), %rax
	movq	%rbx, -144(%rbp)
	xorl	%ebx, %ebx
	movq	%rcx, -160(%rbp)
	movq	%rax, -168(%rbp)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	1(%rbx), %rax
	cmpq	%rbx, -152(%rbp)
	je	.L218
.L178:
	movq	%rax, %rbx
.L164:
	movzwl	0(%r13,%rbx,2), %eax
	movl	%ebx, -132(%rbp)
	testw	%ax, %ax
	je	.L160
	movq	-160(%rbp), %rdi
	leaq	(%rdi,%rax,2), %rdx
	movzwl	(%rdx), %esi
	testl	%esi, %esi
	je	.L160
	movq	-168(%rbp), %rdi
	subl	$1, %esi
	movq	%rdx, %r15
	addq	%rsi, %rax
	leaq	(%rdi,%rax,2), %r14
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L161:
	addq	$2, %r15
	cmpq	%r14, %r15
	je	.L160
.L163:
	movzwl	2(%r15), %eax
	testw	%ax, %ax
	je	.L161
	movq	-144(%rbp), %rcx
	movq	%r12, %rdi
	leaq	(%rcx,%rax,2), %rsi
	call	ucnv_compareNames_67
	testl	%eax, %eax
	jne	.L161
	movl	-132(%rbp), %eax
	xorl	%edx, %edx
	divl	-136(%rbp)
	movl	-172(%rbp), %eax
	addl	%edx, %eax
	movzwl	0(%r13,%rax,2), %eax
	testl	%eax, %eax
	je	.L160
	movq	-160(%rbp), %rcx
	leal	1(%rax), %edx
	cmpw	$0, (%rcx,%rdx,2)
	jne	.L138
	leaq	1(%rbx), %rax
	cmpq	%rbx, -152(%rbp)
	jne	.L178
	.p2align 4,,10
	.p2align 3
.L218:
	xorl	%eax, %eax
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$15, %ecx
	movl	$-1, %esi
.L146:
	movq	-168(%rbp), %rax
	movl	%ecx, (%rax)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$-122, %ecx
	jmp	.L146
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2796:
	.size	_ZL26findTaggedAliasListsOffsetPKcS0_P10UErrorCode, .-_ZL26findTaggedAliasListsOffsetPKcS0_P10UErrorCode
	.p2align 4
	.globl	ucnv_io_getConverterName_67
	.type	ucnv_io_getConverterName_67, @function
ucnv_io_getConverterName_67:
.LFB2798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%rsi, -168(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -156(%rbp)
.L220:
	movq	-136(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L278
.L221:
	xorl	%eax, %eax
.L219:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L279
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L280
.L222:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L223
	movq	-136(%rbp), %rcx
	movl	%eax, (%rcx)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L222
	movq	-136(%rbp), %r15
	movq	%r15, %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	(%r15), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L223:
	movq	-136(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L221
	testq	%rbx, %rbx
	je	.L281
	movzbl	(%rbx), %r13d
	testb	%r13b, %r13b
	je	.L221
	movq	48+_ZL10gMainTable(%rip), %rax
	cmpw	$0, (%rax)
	movq	%rax, -176(%rbp)
	jne	.L282
	movl	84+_ZL10gMainTable(%rip), %r13d
	movq	56+_ZL10gMainTable(%rip), %rax
	xorl	%r12d, %r12d
	movq	16+_ZL10gMainTable(%rip), %r15
	movl	%r13d, %r14d
	movq	%rax, -152(%rbp)
	shrl	%r14d
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L283:
	je	.L236
	movl	%r14d, %r12d
	leal	0(%r13,%r12), %eax
	shrl	%eax
	cmpl	%r14d, %eax
	je	.L242
.L284:
	movl	%eax, %r14d
.L244:
	movl	%r14d, %eax
	movq	-152(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	movzwl	(%r15,%rax,2), %eax
	movq	%rdx, -144(%rbp)
	leaq	(%rdi,%rax,2), %rsi
	movq	%rbx, %rdi
	call	ucnv_compareNames_67
	testl	%eax, %eax
	jns	.L283
	movl	%r14d, %r13d
	leal	0(%r13,%r12), %eax
	shrl	%eax
	cmpl	%r14d, %eax
	jne	.L284
	.p2align 4,,10
	.p2align 3
.L242:
	cmpl	$1, -156(%rbp)
	je	.L221
	cmpb	$120, (%rbx)
	jne	.L221
	cmpb	$45, 1(%rbx)
	jne	.L221
	movl	$1, -156(%rbp)
	addq	$2, %rbx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%rbx, %rdi
	call	strlen@PLT
	cmpq	$59, %rax
	ja	.L227
	leaq	-128(%rbp), %r12
	leaq	1(%rbx), %rcx
	xorl	%edi, %edi
	movq	%r12, %rsi
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L285:
	movsbq	%dl, %rax
	leaq	_ZL10asciiTypes(%rip), %r8
	movzbl	(%r8,%rax), %eax
	cmpb	$1, %al
	je	.L232
	cmpb	$2, %al
	je	.L247
	xorl	%edi, %edi
	movl	%eax, %edx
	testb	%al, %al
	je	.L231
.L233:
	movb	%dl, (%rsi)
	addq	$1, %rsi
.L231:
	addq	$1, %rcx
	testb	%r13b, %r13b
	je	.L230
.L228:
	movl	%r13d, %edx
	movzbl	(%rcx), %r13d
	testb	%dl, %dl
	jns	.L285
	xorl	%edi, %edi
	addq	$1, %rcx
	testb	%r13b, %r13b
	jne	.L228
.L230:
	movq	64+_ZL10gMainTable(%rip), %rax
	movl	84+_ZL10gMainTable(%rip), %r14d
	movb	$0, (%rsi)
	xorl	%r13d, %r13d
	movq	%rbx, -184(%rbp)
	movq	%rax, -152(%rbp)
	movq	16+_ZL10gMainTable(%rip), %rax
	movl	%r14d, %r15d
	shrl	%r15d
	movq	%rax, -144(%rbp)
	movq	%r12, %rax
	movl	%r15d, %ebx
	movl	%r14d, %r12d
	movq	%rax, %r14
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L286:
	je	.L274
	movl	%ebx, %r13d
	leal	(%r12,%r13), %eax
	shrl	%eax
	cmpl	%ebx, %eax
	je	.L275
.L287:
	movl	%eax, %ebx
.L237:
	movq	-144(%rbp), %rcx
	movl	%ebx, %eax
	movq	%r14, %rdi
	leaq	(%rax,%rax), %r15
	movzwl	(%rcx,%rax,2), %eax
	movq	-152(%rbp), %rcx
	leaq	(%rcx,%rax,2), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L286
	movl	%ebx, %r12d
	leal	(%r12,%r13), %eax
	shrl	%eax
	cmpl	%ebx, %eax
	jne	.L287
.L275:
	movq	-184(%rbp), %rbx
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L247:
	movl	$1, %edi
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L232:
	testb	%dil, %dil
	jne	.L233
	testb	%r13b, %r13b
	js	.L233
	movsbq	%r13b, %rax
	leaq	_ZL10asciiTypes(%rip), %r9
	movzbl	(%r9,%rax), %eax
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L233
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%r15, -144(%rbp)
	movq	-184(%rbp), %rbx
.L236:
	movq	24+_ZL10gMainTable(%rip), %rax
	movq	-144(%rbp), %rcx
	movzwl	(%rax,%rcx), %eax
	testw	%ax, %ax
	js	.L288
.L239:
	movq	-168(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L240
	movq	-176(%rbp), %rdx
	shrw	$14, %ax
	movq	-144(%rbp), %rdi
	andl	$1, %eax
	cmpb	$0, 2(%rdx)
	movl	$1, %edx
	cmove	%edx, %eax
	movb	%al, (%rcx)
	movq	24+_ZL10gMainTable(%rip), %rax
	movzwl	(%rax,%rdi), %eax
.L240:
	movl	%eax, %edx
	andl	$4095, %eax
	andw	$4095, %dx
	cmpl	%eax, 72+_ZL10gMainTable(%rip)
	jbe	.L242
	movq	_ZL10gMainTable(%rip), %rax
	movzwl	%dx, %edx
	movzwl	(%rax,%rdx,2), %edx
	movq	56+_ZL10gMainTable(%rip), %rax
	leaq	(%rax,%rdx,2), %rax
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L281:
	movl	$1, (%rax)
	xorl	%eax, %eax
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L288:
	movq	-136(%rbp), %rcx
	movl	$-122, (%rcx)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L227:
	movq	-136(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L242
.L279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2798:
	.size	ucnv_io_getConverterName_67, .-ucnv_io_getConverterName_67
	.p2align 4
	.globl	ucnv_openStandardNames_67
	.type	ucnv_openStandardNames_67, @function
ucnv_openStandardNames_67:
.LFB2803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L304
.L290:
	xorl	%r12d, %r12d
.L289:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	movq	%rdi, %r12
	movq	%rsi, %r13
	movq	%rdx, %rbx
	cmpl	$2, %eax
	jne	.L305
.L291:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L292
	movl	%eax, (%rbx)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L305:
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L291
	movq	%rbx, %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L292:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L290
	testq	%r12, %r12
	je	.L306
	cmpb	$0, (%r12)
	je	.L290
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_ZL26findTaggedAliasListsOffsetPKcS0_P10UErrorCode
	cmpl	%eax, 92+_ZL10gMainTable(%rip)
	movl	%eax, %r13d
	jbe	.L290
	movl	$56, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L307
	movdqa	_ZL12gEnumAliases(%rip), %xmm0
	movdqa	16+_ZL12gEnumAliases(%rip), %xmm1
	movl	$8, %edi
	movdqa	32+_ZL12gEnumAliases(%rip), %xmm2
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	48+_ZL12gEnumAliases(%rip), %rax
	movq	%rax, 48(%r12)
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L308
	movl	%r13d, (%rax)
	movl	$0, 4(%rax)
	movq	%rax, 8(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L307:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L289
.L308:
	movl	$7, (%rbx)
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L289
	.cfi_endproc
.LFE2803:
	.size	ucnv_openStandardNames_67, .-ucnv_openStandardNames_67
	.p2align 4
	.globl	ucnv_getStandard_67
	.type	ucnv_getStandard_67, @function
ucnv_getStandard_67:
.LFB2808:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L326
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movzwl	%di, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edi, %ebx
	subq	$8, %rsp
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L327
.L311:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L312
	movl	%eax, (%r12)
.L310:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L311
	movq	%r12, %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	(%r12), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L312:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L310
	movl	76+_ZL10gMainTable(%rip), %eax
	movzwl	%bx, %ebx
	subl	$1, %eax
	cmpl	%eax, %ebx
	jnb	.L313
	movq	8+_ZL10gMainTable(%rip), %rax
	movzwl	(%rax,%r13,2), %edx
	movq	56+_ZL10gMainTable(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	leaq	(%rax,%rdx,2), %rax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movl	$8, (%r12)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2808:
	.size	ucnv_getStandard_67, .-ucnv_getStandard_67
	.p2align 4
	.globl	ucnv_getStandardName_67
	.type	ucnv_getStandardName_67, @function
ucnv_getStandardName_67:
.LFB2809:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L351
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L352
.L330:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L331
	movl	%eax, (%r12)
.L329:
	xorl	%eax, %eax
.L328:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L330
	movq	%r12, %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	(%r12), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L331:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L329
	testq	%r13, %r13
	je	.L353
	cmpb	$0, 0(%r13)
	je	.L329
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZL26findTaggedAliasListsOffsetPKcS0_P10UErrorCode
	testl	%eax, %eax
	je	.L329
	cmpl	%eax, 92+_ZL10gMainTable(%rip)
	jbe	.L329
	movq	40+_ZL10gMainTable(%rip), %rdx
	movl	%eax, %eax
	movzwl	2(%rdx,%rax,2), %eax
	testw	%ax, %ax
	je	.L329
	movq	56+_ZL10gMainTable(%rip), %rdx
	leaq	(%rdx,%rax,2), %rax
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L353:
	movl	$1, (%r12)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2809:
	.size	ucnv_getStandardName_67, .-ucnv_getStandardName_67
	.p2align 4
	.globl	ucnv_countAliases_67
	.type	ucnv_countAliases_67, @function
ucnv_countAliases_67:
.LFB2810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L400
.L355:
	xorl	%eax, %eax
.L354:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L401
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	movq	%rdi, %r15
	movq	%rsi, %r13
	cmpl	$2, %eax
	jne	.L402
.L356:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L357
	movl	%eax, 0(%r13)
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L402:
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L356
	movq	%r13, %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	0(%r13), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L357:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L355
	testq	%r15, %r15
	je	.L403
	movzbl	(%r15), %ebx
	testb	%bl, %bl
	je	.L355
	movq	48+_ZL10gMainTable(%rip), %rax
	cmpw	$0, (%rax)
	jne	.L404
	movq	16+_ZL10gMainTable(%rip), %rax
	movl	84+_ZL10gMainTable(%rip), %r12d
	xorl	%ebx, %ebx
	movq	%rax, -152(%rbp)
	movq	56+_ZL10gMainTable(%rip), %rax
	movl	%r12d, %r14d
	shrl	%r14d
	movq	%rax, -144(%rbp)
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L405:
	je	.L370
	movl	%r14d, %ebx
.L372:
	leal	(%rbx,%r12), %eax
	shrl	%eax
	cmpl	%r14d, %eax
	je	.L355
	movl	%eax, %r14d
.L374:
	movl	%r14d, %eax
	movq	-152(%rbp), %rdx
	movq	%r15, %rdi
	leaq	(%rax,%rax), %rcx
	movq	%rcx, -136(%rbp)
	movzwl	(%rdx,%rax,2), %eax
	movq	-144(%rbp), %rcx
	leaq	(%rcx,%rax,2), %rsi
	call	ucnv_compareNames_67
	testl	%eax, %eax
	jns	.L405
	movl	%r14d, %r12d
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L403:
	movl	$1, 0(%r13)
	xorl	%eax, %eax
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L404:
	movq	%r15, %rdi
	call	strlen@PLT
	cmpq	$59, %rax
	ja	.L361
	leaq	-128(%rbp), %r14
	leaq	1(%r15), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	leaq	_ZL10asciiTypes(%rip), %r9
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L406:
	movsbq	%al, %rsi
	movzbl	(%r9,%rsi), %esi
	cmpb	$1, %sil
	je	.L366
	cmpb	$2, %sil
	je	.L377
	xorl	%r8d, %r8d
	movl	%esi, %eax
	testb	%sil, %sil
	je	.L365
.L367:
	movb	%al, (%rdi)
	addq	$1, %rdi
.L365:
	addq	$1, %rdx
	testb	%bl, %bl
	je	.L364
.L362:
	movl	%ebx, %eax
	movzbl	(%rdx), %ebx
	testb	%al, %al
	jns	.L406
	xorl	%r8d, %r8d
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L361:
	movl	$15, 0(%r13)
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L377:
	movl	$1, %r8d
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L364:
	movq	64+_ZL10gMainTable(%rip), %rax
	movl	84+_ZL10gMainTable(%rip), %r12d
	movb	$0, (%rdi)
	xorl	%ebx, %ebx
	movq	%rax, -144(%rbp)
	movq	16+_ZL10gMainTable(%rip), %rax
	movl	%r12d, %r15d
	shrl	%r15d
	movq	%rax, -152(%rbp)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L407:
	je	.L370
	movl	%r15d, %ebx
.L369:
	leal	(%r12,%rbx), %eax
	shrl	%eax
	cmpl	%r15d, %eax
	je	.L355
	movl	%eax, %r15d
.L371:
	movl	%r15d, %eax
	movq	-152(%rbp), %rdx
	movq	%r14, %rdi
	leaq	(%rax,%rax), %rcx
	movq	%rcx, -136(%rbp)
	movzwl	(%rdx,%rax,2), %eax
	movq	-144(%rbp), %rcx
	leaq	(%rcx,%rax,2), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L407
	movl	%r15d, %r12d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L366:
	testb	%r8b, %r8b
	jne	.L367
	testb	%bl, %bl
	js	.L367
	movsbq	%bl, %rsi
	movzbl	(%r9,%rsi), %esi
	subl	$1, %esi
	cmpb	$1, %sil
	ja	.L367
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L370:
	movq	24+_ZL10gMainTable(%rip), %rax
	movq	-136(%rbp), %rbx
	movzwl	(%rax,%rbx), %edx
	testw	%dx, %dx
	jns	.L373
	movl	$-122, 0(%r13)
.L373:
	movl	72+_ZL10gMainTable(%rip), %ecx
	andl	$4095, %edx
	cmpl	%edx, %ecx
	jbe	.L355
	movl	76+_ZL10gMainTable(%rip), %eax
	subl	$1, %eax
	imull	%ecx, %eax
	addl	%eax, %edx
	movq	32+_ZL10gMainTable(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	testw	%ax, %ax
	je	.L355
	movq	40+_ZL10gMainTable(%rip), %rdx
	movzwl	(%rdx,%rax,2), %eax
	jmp	.L354
.L401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2810:
	.size	ucnv_countAliases_67, .-ucnv_countAliases_67
	.p2align 4
	.globl	ucnv_getAlias_67
	.type	ucnv_getAlias_67, @function
ucnv_getAlias_67:
.LFB2811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movw	%si, -170(%rbp)
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L455
.L409:
	xorl	%eax, %eax
.L408:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L456
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	movq	%rdi, %r14
	movl	%esi, %r12d
	movq	%rdx, %rbx
	cmpl	$2, %eax
	jne	.L457
.L410:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L411
	movl	%eax, (%rbx)
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L410
	movq	%rbx, %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L411:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L409
	testq	%r14, %r14
	je	.L458
	movzbl	(%r14), %r13d
	testb	%r13b, %r13b
	je	.L409
	movq	48+_ZL10gMainTable(%rip), %rax
	cmpw	$0, (%rax)
	je	.L414
	movq	%r14, %rdi
	call	strlen@PLT
	cmpq	$59, %rax
	ja	.L415
	leaq	-128(%rbp), %rax
	leaq	1(%r14), %rcx
	xorl	%edi, %edi
	movq	%rax, -168(%rbp)
	movq	%rax, %rsi
	leaq	_ZL10asciiTypes(%rip), %r8
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L459:
	movsbq	%al, %rdx
	movzbl	(%r8,%rdx), %edx
	cmpb	$1, %dl
	je	.L420
	cmpb	$2, %dl
	je	.L432
	xorl	%edi, %edi
	movl	%edx, %eax
	testb	%dl, %dl
	je	.L419
.L421:
	movb	%al, (%rsi)
	addq	$1, %rsi
.L419:
	addq	$1, %rcx
	testb	%r13b, %r13b
	je	.L418
.L416:
	movl	%r13d, %eax
	movzbl	(%rcx), %r13d
	testb	%al, %al
	jns	.L459
	xorl	%edi, %edi
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L414:
	movl	84+_ZL10gMainTable(%rip), %eax
	xorl	%r13d, %r13d
	movl	%eax, %r15d
	movl	%eax, -144(%rbp)
	movq	16+_ZL10gMainTable(%rip), %rax
	shrl	%r15d
	movq	%rax, -152(%rbp)
	movq	56+_ZL10gMainTable(%rip), %rax
	movq	%rax, -160(%rbp)
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L460:
	je	.L424
	movl	%r15d, %r13d
.L426:
	movl	-144(%rbp), %eax
	addl	%r13d, %eax
	shrl	%eax
	cmpl	%r15d, %eax
	je	.L409
	movl	%eax, %r15d
.L429:
	movl	%r15d, %eax
	movq	-160(%rbp), %rcx
	leaq	(%rax,%rax), %rdi
	movq	%rdi, -136(%rbp)
	movq	-152(%rbp), %rdi
	movzwl	(%rdi,%rax,2), %eax
	movq	%r14, %rdi
	leaq	(%rcx,%rax,2), %rsi
	call	ucnv_compareNames_67
	testl	%eax, %eax
	jns	.L460
	movl	%r15d, -144(%rbp)
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L432:
	movl	$1, %edi
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L418:
	movq	64+_ZL10gMainTable(%rip), %rax
	movl	84+_ZL10gMainTable(%rip), %r13d
	movb	$0, (%rsi)
	xorl	%r14d, %r14d
	movq	%rax, -144(%rbp)
	movq	16+_ZL10gMainTable(%rip), %rax
	movl	%r13d, %r15d
	shrl	%r15d
	movq	%rax, -152(%rbp)
	movq	56+_ZL10gMainTable(%rip), %rax
	movq	%rax, -160(%rbp)
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L461:
	je	.L424
	movl	%r15d, %r14d
.L423:
	leal	0(%r13,%r14), %eax
	shrl	%eax
	cmpl	%r15d, %eax
	je	.L409
	movl	%eax, %r15d
.L425:
	movl	%r15d, %eax
	movq	-168(%rbp), %rdi
	leaq	(%rax,%rax), %rcx
	movq	%rcx, -136(%rbp)
	movq	-152(%rbp), %rcx
	movzwl	(%rcx,%rax,2), %eax
	movq	-144(%rbp), %rcx
	leaq	(%rcx,%rax,2), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L461
	movl	%r15d, %r13d
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L420:
	testb	%dil, %dil
	jne	.L421
	testb	%r13b, %r13b
	js	.L421
	movsbq	%r13b, %rdx
	movzbl	(%r8,%rdx), %edx
	subl	$1, %edx
	cmpb	$1, %dl
	ja	.L421
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L458:
	movl	$1, (%rbx)
	xorl	%eax, %eax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L424:
	movq	24+_ZL10gMainTable(%rip), %rax
	movq	-136(%rbp), %rdx
	movzwl	(%rax,%rdx), %edx
	testw	%dx, %dx
	js	.L462
.L427:
	movl	72+_ZL10gMainTable(%rip), %ecx
	andl	$4095, %edx
	cmpl	%edx, %ecx
	jbe	.L409
	movl	76+_ZL10gMainTable(%rip), %eax
	subl	$1, %eax
	imull	%ecx, %eax
	addl	%eax, %edx
	movq	32+_ZL10gMainTable(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	testw	%ax, %ax
	je	.L409
	movq	40+_ZL10gMainTable(%rip), %rdx
	cmpw	(%rdx,%rax,2), %r12w
	jnb	.L428
	movzwl	-170(%rbp), %ecx
	movq	-160(%rbp), %rbx
	leaq	1(%rax,%rcx), %rax
	movzwl	(%rdx,%rax,2), %eax
	leaq	(%rbx,%rax,2), %rax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L428:
	movl	$8, (%rbx)
	xorl	%eax, %eax
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L415:
	movl	$15, (%rbx)
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L462:
	movl	$-122, (%rbx)
	jmp	.L427
.L456:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2811:
	.size	ucnv_getAlias_67, .-ucnv_getAlias_67
	.p2align 4
	.globl	ucnv_getAliases_67
	.type	ucnv_getAliases_67, @function
ucnv_getAliases_67:
.LFB2812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L517
.L463:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L518
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%rdx, %r14
	cmpl	$2, %eax
	jne	.L519
.L465:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L466
	movl	%eax, (%r14)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L519:
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L465
	movq	%r14, %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	(%r14), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L466:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L463
	testq	%r13, %r13
	je	.L520
	movzbl	0(%r13), %r12d
	testb	%r12b, %r12b
	je	.L463
	movq	48+_ZL10gMainTable(%rip), %rax
	cmpw	$0, (%rax)
	jne	.L521
	movl	84+_ZL10gMainTable(%rip), %eax
	xorl	%r12d, %r12d
	movl	%eax, %r15d
	movl	%eax, -140(%rbp)
	movq	16+_ZL10gMainTable(%rip), %rax
	shrl	%r15d
	movq	%rax, -152(%rbp)
	movq	56+_ZL10gMainTable(%rip), %rax
	movq	%rax, -168(%rbp)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L522:
	je	.L479
	movl	%r15d, %r12d
.L481:
	movl	-140(%rbp), %eax
	addl	%r12d, %eax
	shrl	%eax
	cmpl	%r15d, %eax
	je	.L463
	movl	%eax, %r15d
.L486:
	movl	%r15d, %eax
	movq	-152(%rbp), %rcx
	movq	%r13, %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rdx, -136(%rbp)
	movzwl	(%rcx,%rax,2), %eax
	movq	-168(%rbp), %rdx
	leaq	(%rdx,%rax,2), %rsi
	call	ucnv_compareNames_67
	testl	%eax, %eax
	jns	.L522
	movl	%r15d, -140(%rbp)
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L520:
	movl	$1, (%r14)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L521:
	movq	%r13, %rdi
	call	strlen@PLT
	cmpq	$59, %rax
	ja	.L470
	leaq	-128(%rbp), %r15
	leaq	1(%r13), %rcx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	leaq	_ZL10asciiTypes(%rip), %r9
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L523:
	movsbq	%al, %rsi
	movzbl	(%r9,%rsi), %esi
	cmpb	$1, %sil
	je	.L475
	cmpb	$2, %sil
	je	.L489
	xorl	%r8d, %r8d
	movl	%esi, %eax
	testb	%sil, %sil
	je	.L474
.L476:
	movb	%al, (%rdi)
	addq	$1, %rdi
.L474:
	addq	$1, %rcx
	testb	%r12b, %r12b
	je	.L473
.L471:
	movl	%r12d, %eax
	movzbl	(%rcx), %r12d
	testb	%al, %al
	jns	.L523
	xorl	%r8d, %r8d
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L470:
	movl	$15, (%r14)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L489:
	movl	$1, %r8d
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L473:
	movl	84+_ZL10gMainTable(%rip), %eax
	movb	$0, (%rdi)
	xorl	%r12d, %r12d
	movl	%eax, %r13d
	movl	%eax, -140(%rbp)
	movq	64+_ZL10gMainTable(%rip), %rax
	shrl	%r13d
	movq	%rax, -160(%rbp)
	movq	16+_ZL10gMainTable(%rip), %rax
	movq	%rax, -152(%rbp)
	movq	56+_ZL10gMainTable(%rip), %rax
	movq	%rax, -168(%rbp)
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L524:
	je	.L479
	movl	%r13d, %r12d
.L478:
	movl	-140(%rbp), %eax
	addl	%r12d, %eax
	shrl	%eax
	cmpl	%r13d, %eax
	je	.L463
	movl	%eax, %r13d
.L480:
	movl	%r13d, %eax
	movq	%r15, %rdi
	leaq	(%rax,%rax), %rcx
	movq	%rcx, -136(%rbp)
	movq	-152(%rbp), %rcx
	movzwl	(%rcx,%rax,2), %eax
	movq	-160(%rbp), %rcx
	leaq	(%rcx,%rax,2), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L524
	movl	%r13d, -140(%rbp)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L475:
	testb	%r8b, %r8b
	jne	.L476
	testb	%r12b, %r12b
	js	.L476
	movsbq	%r12b, %rsi
	movzbl	(%r9,%rsi), %esi
	subl	$1, %esi
	cmpb	$1, %sil
	ja	.L476
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L479:
	movq	24+_ZL10gMainTable(%rip), %rax
	movq	-136(%rbp), %rdi
	movzwl	(%rax,%rdi), %eax
	testw	%ax, %ax
	jns	.L482
	movl	$-122, (%r14)
.L482:
	movl	%eax, %edx
	movl	72+_ZL10gMainTable(%rip), %ecx
	andl	$4095, %edx
	cmpl	%edx, %ecx
	jbe	.L463
	movl	76+_ZL10gMainTable(%rip), %eax
	subl	$1, %eax
	imull	%ecx, %eax
	addl	%eax, %edx
	movq	32+_ZL10gMainTable(%rip), %rax
	movzwl	(%rax,%rdx,2), %eax
	testw	%ax, %ax
	je	.L463
	movq	40+_ZL10gMainTable(%rip), %r8
	leaq	(%rax,%rax), %rdx
	leaq	1(%rax), %rcx
	movzwl	(%r8,%rax,2), %edi
	testl	%edi, %edi
	je	.L463
	leal	-1(%rdi), %eax
	cmpl	$6, %eax
	jbe	.L494
	movl	%edi, %esi
	leaq	2(%r8,%rdx), %rdx
	pxor	%xmm3, %xmm3
	movq	%rbx, %rax
	shrl	$3, %esi
	movdqa	.LC0(%rip), %xmm5
	movq	-168(%rbp), %xmm4
	subl	$1, %esi
	salq	$6, %rsi
	punpcklqdq	%xmm4, %xmm4
	leaq	64(%rbx,%rsi), %rsi
	.p2align 4,,10
	.p2align 3
.L484:
	movdqu	(%rdx), %xmm0
	movdqu	(%rdx), %xmm2
	addq	$64, %rax
	addq	$16, %rdx
	pmullw	%xmm5, %xmm0
	pmulhuw	%xmm5, %xmm2
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm2, %xmm0
	punpcklwd	%xmm2, %xmm1
	movdqa	%xmm1, %xmm2
	punpckhdq	%xmm3, %xmm1
	paddq	%xmm4, %xmm1
	punpckldq	%xmm3, %xmm2
	movups	%xmm1, -48(%rax)
	movdqa	%xmm0, %xmm1
	punpckhdq	%xmm3, %xmm0
	paddq	%xmm4, %xmm2
	punpckldq	%xmm3, %xmm1
	paddq	%xmm4, %xmm0
	movups	%xmm2, -64(%rax)
	paddq	%xmm4, %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, -32(%rax)
	cmpq	%rsi, %rax
	jne	.L484
	movl	%edi, %eax
	andl	$-8, %eax
	testl	$-65529, %edi
	je	.L463
.L483:
	movl	%eax, %edx
	movq	-168(%rbp), %r10
	leaq	(%rcx,%rdx), %rsi
	movzwl	(%r8,%rsi,2), %esi
	leaq	(%r10,%rsi,2), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	leal	1(%rax), %edx
	cmpl	%edx, %edi
	jbe	.L463
	leaq	(%rcx,%rdx), %rsi
	movzwl	(%r8,%rsi,2), %esi
	leaq	(%r10,%rsi,2), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	leal	2(%rax), %edx
	cmpl	%edx, %edi
	jbe	.L463
	leaq	(%rcx,%rdx), %rsi
	movzwl	(%r8,%rsi,2), %esi
	leaq	(%r10,%rsi,2), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	leal	3(%rax), %edx
	cmpl	%edx, %edi
	jbe	.L463
	leaq	(%rcx,%rdx), %rsi
	movzwl	(%r8,%rsi,2), %esi
	leaq	(%r10,%rsi,2), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	leal	4(%rax), %edx
	cmpl	%edx, %edi
	jbe	.L463
	leaq	(%rcx,%rdx), %rsi
	movzwl	(%r8,%rsi,2), %esi
	leaq	(%r10,%rsi,2), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	leal	5(%rax), %edx
	cmpl	%edx, %edi
	jbe	.L463
	leaq	(%rcx,%rdx), %rsi
	addl	$6, %eax
	movzwl	(%r8,%rsi,2), %esi
	leaq	(%r10,%rsi,2), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	cmpl	%eax, %edi
	jbe	.L463
	addq	%rax, %rcx
	movzwl	(%r8,%rcx,2), %edx
	leaq	(%r10,%rdx,2), %rdx
	movq	%rdx, (%rbx,%rax,8)
	jmp	.L463
.L494:
	xorl	%eax, %eax
	jmp	.L483
.L518:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2812:
	.size	ucnv_getAliases_67, .-ucnv_getAliases_67
	.p2align 4
	.globl	ucnv_countStandards_67
	.type	ucnv_countStandards_67, @function
ucnv_countStandards_67:
.LFB2813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, -12(%rbp)
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L538
.L526:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %edx
	testl	%edx, %edx
	jle	.L527
.L529:
	xorl	%eax, %eax
.L525:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L539
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L526
	leaq	-12(%rbp), %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	-12(%rbp), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L527:
	movl	-12(%rbp), %eax
	testl	%eax, %eax
	jg	.L529
	movzwl	76+_ZL10gMainTable(%rip), %eax
	subl	$1, %eax
	jmp	.L525
.L539:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2813:
	.size	ucnv_countStandards_67, .-ucnv_countStandards_67
	.p2align 4
	.globl	ucnv_getCanonicalName_67
	.type	ucnv_getCanonicalName_67, @function
ucnv_getCanonicalName_67:
.LFB2814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L628
.L541:
	xorl	%eax, %eax
.L540:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L629
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	.cfi_restore_state
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	%rdx, %r13
	cmpl	$2, %eax
	jne	.L630
.L542:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L543
	movl	%eax, 0(%r13)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L630:
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L542
	movq	%r13, %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	0(%r13), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L543:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L541
	testq	%rbx, %rbx
	je	.L631
	cmpb	$0, (%rbx)
	je	.L541
	movq	8+_ZL10gMainTable(%rip), %rax
	testq	%rax, %rax
	je	.L546
	movl	76+_ZL10gMainTable(%rip), %edx
	testl	%edx, %edx
	je	.L546
	xorl	%r12d, %r12d
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L632:
	addl	$1, %r12d
	cmpl	%r12d, 76+_ZL10gMainTable(%rip)
	jbe	.L546
	movq	8+_ZL10gMainTable(%rip), %rax
.L548:
	movl	%r12d, %edx
	movq	%r14, %rsi
	movzwl	(%rax,%rdx,2), %edx
	movq	56+_ZL10gMainTable(%rip), %rax
	leaq	(%rax,%rdx,2), %rdi
	call	uprv_stricmp_67@PLT
	testl	%eax, %eax
	jne	.L632
	movq	48+_ZL10gMainTable(%rip), %rax
	cmpw	$0, (%rax)
	je	.L549
.L635:
	movq	%rbx, %rdi
	call	strlen@PLT
	cmpq	$59, %rax
	ja	.L550
	movzbl	(%rbx), %edx
	leaq	-128(%rbp), %rax
	leaq	1(%rbx), %rsi
	xorl	%r8d, %r8d
	movq	%rax, -168(%rbp)
	movq	%rax, %rdi
	leaq	_ZL10asciiTypes(%rip), %r9
	testb	%dl, %dl
	jne	.L551
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L633:
	movsbq	%al, %rcx
	movzbl	(%r9,%rcx), %ecx
	cmpb	$1, %cl
	je	.L555
	cmpb	$2, %cl
	je	.L578
	xorl	%r8d, %r8d
	movl	%ecx, %eax
	testb	%cl, %cl
	je	.L554
.L556:
	movb	%al, (%rdi)
	addq	$1, %rdi
.L554:
	addq	$1, %rsi
	testb	%dl, %dl
	je	.L552
.L551:
	movl	%edx, %eax
	movzbl	(%rsi), %edx
	testb	%al, %al
	jns	.L633
	xorl	%r8d, %r8d
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L549:
	movl	84+_ZL10gMainTable(%rip), %eax
	xorl	%r15d, %r15d
	movl	%eax, %r14d
	movl	%eax, -140(%rbp)
	movq	16+_ZL10gMainTable(%rip), %rax
	shrl	%r14d
	movq	%rax, -152(%rbp)
	movq	56+_ZL10gMainTable(%rip), %rax
	movq	%rax, -160(%rbp)
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L634:
	je	.L559
	movl	%r14d, %r15d
.L562:
	movl	-140(%rbp), %eax
	addl	%r15d, %eax
	shrl	%eax
	cmpl	%r14d, %eax
	je	.L541
	movl	%eax, %r14d
.L574:
	movq	-152(%rbp), %rdx
	movl	%r14d, %eax
	movq	-160(%rbp), %rcx
	leaq	(%rax,%rax), %rdi
	movzwl	(%rdx,%rax,2), %eax
	movq	%rdi, -136(%rbp)
	movq	%rbx, %rdi
	leaq	(%rcx,%rax,2), %rsi
	call	ucnv_compareNames_67
	testl	%eax, %eax
	jns	.L634
	movl	%r14d, -140(%rbp)
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L578:
	movl	$1, %r8d
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L546:
	movq	48+_ZL10gMainTable(%rip), %rax
	movl	$-1, %r12d
	cmpw	$0, (%rax)
	je	.L549
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L552:
	movl	84+_ZL10gMainTable(%rip), %eax
	movb	$0, (%rdi)
	xorl	%r15d, %r15d
	movl	%eax, %r14d
	movl	%eax, -140(%rbp)
	movq	64+_ZL10gMainTable(%rip), %rax
	shrl	%r14d
	movq	%rax, -152(%rbp)
	movq	16+_ZL10gMainTable(%rip), %rax
	movq	%rax, -160(%rbp)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L636:
	je	.L559
	movl	%r14d, %r15d
.L558:
	movl	-140(%rbp), %eax
	addl	%r15d, %eax
	shrl	%eax
	cmpl	%r14d, %eax
	je	.L541
	movl	%eax, %r14d
.L561:
	movl	%r14d, %eax
	movq	-168(%rbp), %rdi
	leaq	(%rax,%rax), %rcx
	movq	%rcx, -136(%rbp)
	movq	-160(%rbp), %rcx
	movzwl	(%rcx,%rax,2), %eax
	movq	-152(%rbp), %rcx
	leaq	(%rcx,%rax,2), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L636
	movl	%r14d, -140(%rbp)
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L555:
	testb	%r8b, %r8b
	jne	.L556
	testb	%dl, %dl
	js	.L556
	movsbq	%dl, %rcx
	movzbl	(%r9,%rcx), %ecx
	subl	$1, %ecx
	cmpb	$1, %cl
	ja	.L556
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$1, 0(%r13)
	xorl	%eax, %eax
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L559:
	movq	24+_ZL10gMainTable(%rip), %rax
	movq	-136(%rbp), %rdi
	xorl	%ecx, %ecx
	movzwl	(%rax,%rdi), %eax
	movl	%eax, %edx
	andl	$4095, %edx
	testw	%ax, %ax
	js	.L637
.L564:
	movl	76+_ZL10gMainTable(%rip), %eax
	subl	$1, %eax
	cmpl	%r12d, %eax
	jbe	.L541
	movl	72+_ZL10gMainTable(%rip), %eax
	movl	%eax, -152(%rbp)
	cmpl	%edx, %eax
	jbe	.L541
	imull	%eax, %r12d
	movq	32+_ZL10gMainTable(%rip), %r13
	leal	(%r12,%rdx), %eax
	movl	%r12d, -160(%rbp)
	movzwl	0(%r13,%rax,2), %eax
	testw	%ax, %ax
	jne	.L638
.L565:
	cmpl	$-122, %ecx
	jne	.L541
	movl	-160(%rbp), %eax
	movl	-152(%rbp), %ecx
	addl	%eax, %ecx
	movl	%eax, %r12d
	movl	%ecx, -140(%rbp)
	cmpl	%ecx, %eax
	jnb	.L541
	movq	40+_ZL10gMainTable(%rip), %rdi
	movl	%eax, %eax
	movq	56+_ZL10gMainTable(%rip), %r14
	leaq	0(%r13,%rax,2), %r13
	leaq	2(%rdi), %rax
	movq	%rdi, -168(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L569:
	addl	$1, %r12d
	addq	$2, %r13
	cmpl	%r12d, -140(%rbp)
	jbe	.L541
.L573:
	movzwl	0(%r13), %eax
	testw	%ax, %ax
	je	.L569
	movq	-168(%rbp), %rdi
	leaq	(%rdi,%rax,2), %rdx
	movzwl	(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L569
	movq	-176(%rbp), %rdi
	subl	$1, %ecx
	movq	%rdx, %r15
	addq	%rcx, %rax
	leaq	(%rdi,%rax,2), %rax
	movq	%rax, -136(%rbp)
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L570:
	addq	$2, %r15
	cmpq	-136(%rbp), %r15
	je	.L569
.L572:
	movzwl	2(%r15), %eax
	testw	%ax, %ax
	je	.L570
	leaq	(%r14,%rax,2), %rsi
	movq	%rbx, %rdi
	call	ucnv_compareNames_67
	testl	%eax, %eax
	jne	.L570
	movl	%r12d, %edx
	subl	-160(%rbp), %edx
	cmpl	%edx, -152(%rbp)
	jbe	.L541
.L567:
	movq	_ZL10gMainTable(%rip), %rax
	movl	%edx, %r12d
	movzwl	(%rax,%r12,2), %eax
	leaq	(%r14,%rax,2), %rax
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L550:
	movl	$15, %ecx
	movl	$-1, %edx
.L553:
	movl	%ecx, 0(%r13)
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L637:
	movl	$-122, %ecx
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L638:
	movq	40+_ZL10gMainTable(%rip), %rdi
	leaq	(%rdi,%rax,2), %r15
	movzwl	(%r15), %esi
	testl	%esi, %esi
	je	.L565
	subl	$1, %esi
	movq	56+_ZL10gMainTable(%rip), %r14
	addq	%rsi, %rax
	leaq	2(%rdi,%rax,2), %r12
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L566:
	addq	$2, %r15
	cmpq	%r15, %r12
	je	.L565
.L568:
	movzwl	2(%r15), %eax
	testw	%ax, %ax
	je	.L566
	leaq	(%r14,%rax,2), %rsi
	movq	%rbx, %rdi
	movl	%edx, -140(%rbp)
	movl	%ecx, -136(%rbp)
	call	ucnv_compareNames_67
	movl	-136(%rbp), %ecx
	movl	-140(%rbp), %edx
	testl	%eax, %eax
	jne	.L566
	jmp	.L567
.L629:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2814:
	.size	ucnv_getCanonicalName_67, .-ucnv_getCanonicalName_67
	.p2align 4
	.globl	ucnv_openAllNames_67
	.type	ucnv_openAllNames_67, @function
ucnv_openAllNames_67:
.LFB2818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rdi), %esi
	testl	%esi, %esi
	jle	.L653
.L640:
	xorl	%r12d, %r12d
.L639:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	.cfi_restore_state
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	movq	%rdi, %rbx
	cmpl	$2, %eax
	jne	.L654
.L641:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L642
	movl	%eax, (%rbx)
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L654:
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L641
	movq	%rbx, %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L642:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L640
	movl	$56, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L655
	movdqa	_ZL18gEnumAllConverters(%rip), %xmm0
	movdqa	16+_ZL18gEnumAllConverters(%rip), %xmm1
	movl	$2, %edi
	movdqa	32+_ZL18gEnumAllConverters(%rip), %xmm2
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	48+_ZL18gEnumAllConverters(%rip), %rax
	movq	%rax, 48(%r12)
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L656
	xorl	%edx, %edx
	movq	%rax, 8(%r12)
	movw	%dx, (%rax)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L656:
	.cfi_restore_state
	movl	$7, (%rbx)
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L639
.L655:
	movl	$7, (%rbx)
	jmp	.L639
	.cfi_endproc
.LFE2818:
	.size	ucnv_openAllNames_67, .-ucnv_openAllNames_67
	.p2align 4
	.globl	ucnv_io_countKnownConverters_67
	.type	ucnv_io_countKnownConverters_67, @function
ucnv_io_countKnownConverters_67:
.LFB2819:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	testl	%edx, %edx
	jle	.L673
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZL18gAliasDataInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L674
.L659:
	movl	4+_ZL18gAliasDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L660
	movl	%eax, (%rbx)
.L658:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L659
	movq	%rbx, %rdi
	call	_ZL13initAliasDataR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL18gAliasDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL18gAliasDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L660:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L658
	movzwl	72+_ZL10gMainTable(%rip), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2819:
	.size	ucnv_io_countKnownConverters_67, .-ucnv_io_countKnownConverters_67
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"ucnv_swapAliases(): data format %02x.%02x.%02x.%02x (format version %02x) is not an alias table\n"
	.align 8
.LC2:
	.string	"ucnv_swapAliases(): too few bytes (%d after header) for an alias table\n"
	.align 8
.LC3:
	.string	"ucnv_swapAliases(): table of contents contains unsupported number of sections (%u sections)\n"
	.align 8
.LC4:
	.string	"ucnv_swapAliases().swapInvChars(charset names) failed\n"
	.align 8
.LC5:
	.string	"ucnv_swapAliases(): unable to allocate memory for sorting tables (max length: %u)\n"
	.align 8
.LC6:
	.string	"ucnv_swapAliases().uprv_sortArray(%u items) failed\n"
	.text
	.p2align 4
	.globl	ucnv_swapAliases_67
	.type	ucnv_swapAliases_67, @function
ucnv_swapAliases_67:
.LFB2821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$3288, %rsp
	movq	%rcx, -3216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	udata_swapDataHeader_67@PLT
	movl	%eax, -3204(%rbp)
	testq	%r12, %r12
	je	.L675
	movl	(%r12), %r10d
	testl	%r10d, %r10d
	jg	.L675
	cmpw	$30275, 12(%r13)
	movzbl	16(%r13), %eax
	jne	.L677
	cmpw	$27713, 14(%r13)
	jne	.L677
	cmpb	$3, %al
	jne	.L677
	testl	%ebx, %ebx
	js	.L679
	movl	%ebx, %edx
	subl	-3204(%rbp), %edx
	cmpl	$35, %edx
	jle	.L727
.L679:
	movslq	-3204(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3168(%rbp)
	leaq	0(%r13,%rax), %r14
	movq	%rax, -3240(%rbp)
	movl	$1, %r13d
	movl	(%r14), %edi
	movaps	%xmm0, -3152(%rbp)
	call	*16(%r15)
	leaq	-3168(%rbp), %rsi
	movl	%eax, -3208(%rbp)
	movl	%eax, -3168(%rbp)
	subl	$8, %eax
	cmpl	$1, %eax
	ja	.L728
	movq	%r12, -3232(%rbp)
	movq	%r14, %r12
	movq	%r13, %r14
	movl	-3208(%rbp), %r13d
	movl	%ebx, -3220(%rbp)
	movq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L680:
	movl	(%r12,%r14,4), %edi
	call	*16(%r15)
	movl	%eax, (%rbx,%r14,4)
	addq	$1, %r14
	cmpl	%r14d, %r13d
	jnb	.L680
	movl	-3208(%rbp), %esi
	pxor	%xmm0, %xmm0
	movl	-3164(%rbp), %edx
	movq	$0, -3088(%rbp)
	movaps	%xmm0, -3120(%rbp)
	movl	-3220(%rbp), %ebx
	movq	%r12, %r14
	movq	-3232(%rbp), %r12
	leal	1(%rsi), %ecx
	leal	(%rcx,%rcx), %eax
	addl	%eax, %edx
	movl	%eax, -3220(%rbp)
	movl	%edx, -3112(%rbp)
	addl	-3160(%rbp), %edx
	movl	%edx, -3108(%rbp)
	addl	-3156(%rbp), %edx
	movl	%edx, -3104(%rbp)
	addl	-3152(%rbp), %edx
	movl	%edx, -3100(%rbp)
	addl	-3148(%rbp), %edx
	movl	%edx, -3096(%rbp)
	addl	-3144(%rbp), %edx
	movl	%edx, -3092(%rbp)
	addl	-3140(%rbp), %edx
	movl	%eax, -3116(%rbp)
	movl	%edx, -3088(%rbp)
	cmpl	$8, %esi
	je	.L681
	addl	-3136(%rbp), %edx
	movl	%edx, -3084(%rbp)
.L681:
	movl	-3208(%rbp), %r13d
	addl	-3168(%rbp,%r13,4), %edx
	leal	(%rdx,%rdx), %eax
	movl	%eax, -3208(%rbp)
	testl	%ebx, %ebx
	js	.L682
	subl	-3204(%rbp), %ebx
	cmpl	%eax, %ebx
	jl	.L729
	movq	-3216(%rbp), %rbx
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	%r15, %rdi
	addq	-3240(%rbp), %rbx
	leal	0(,%rcx,4), %edx
	movq	%rbx, -3240(%rbp)
	movq	%rbx, %rcx
	call	*56(%r15)
	movl	-3088(%rbp), %esi
	movq	%r15, %rdi
	movq	%r12, %r8
	movl	-3132(%rbp), %edx
	addl	-3136(%rbp), %edx
	movl	%esi, -3224(%rbp)
	addq	%rsi, %rsi
	addl	%edx, %edx
	addq	%rsi, %rbx
	addq	%r14, %rsi
	movq	%rbx, %rcx
	call	*72(%r15)
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L730
	movzbl	3(%r15), %eax
	cmpb	%al, 1(%r15)
	je	.L731
	movl	-3156(%rbp), %esi
	movq	%rbx, -3200(%rbp)
	movl	%esi, -3216(%rbp)
	movl	%esi, %ebx
	cmpl	$500, %esi
	ja	.L686
	leaq	-3072(%rbp), %rdx
	movq	-3240(%rbp), %rdi
	leaq	-2064(%rbp), %rsi
	movq	%rdx, %xmm1
	movl	-3104(%rbp), %edx
	movq	%rsi, %xmm0
	movq	%rsi, -3272(%rbp)
	movl	-3108(%rbp), %esi
	punpcklqdq	%xmm1, %xmm0
	addq	%rdx, %rdx
	movups	%xmm0, -3192(%rbp)
	leaq	(%rdi,%rdx), %rcx
	addq	%r14, %rdx
	movl	%esi, -3276(%rbp)
	movq	%rdx, -3248(%rbp)
	movl	%esi, %edx
	addq	%rdx, %rdx
	testb	%al, %al
	leaq	ucnv_io_stripEBCDICForCompare_67(%rip), %rax
	movq	%rcx, -3256(%rbp)
	leaq	(%rdi,%rdx), %rsi
	movq	-3272(%rbp), %rdi
	movq	%rsi, -3296(%rbp)
	leaq	(%r14,%rdx), %rsi
	leaq	ucnv_io_stripASCIIForCompare_67(%rip), %rdx
	cmove	%rdx, %rax
	movq	%rsi, -3232(%rbp)
	movq	%rax, -3176(%rbp)
	testl	%ebx, %ebx
	je	.L732
.L707:
	xorl	%r13d, %r13d
	movq	%r14, -3264(%rbp)
	movq	%r12, -3288(%rbp)
	movq	%r13, %r12
	movq	-3232(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L692:
	leaq	0(,%r12,4), %r14
	leaq	(%rdi,%r14), %rbx
	movzwl	0(%r13,%r12,2), %edi
	call	*8(%r15)
	movq	-3192(%rbp), %rdi
	movw	%ax, (%rbx)
	movw	%r12w, 2(%rdi,%r14)
	addq	$1, %r12
	cmpl	%r12d, -3216(%rbp)
	ja	.L692
	movq	-3264(%rbp), %r14
	movq	-3288(%rbp), %r12
.L691:
	subq	$8, %rsp
	movl	-3216(%rbp), %ebx
	movl	$4, %edx
	xorl	%r9d, %r9d
	pushq	%r12
	leaq	_ZL14io_compareRowsPKvS0_S0_(%rip), %rcx
	leaq	-3200(%rbp), %r8
	movl	%ebx, %esi
	call	uprv_sortArray_67@PLT
	movl	(%r12), %esi
	popq	%rdx
	movq	-3192(%rbp), %rdi
	popq	%rcx
	testl	%esi, %esi
	jle	.L733
	cmpq	-3272(%rbp), %rdi
	je	.L705
.L704:
	call	uprv_free_67@PLT
.L702:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L705
.L703:
	movl	-3220(%rbp), %eax
	movl	-3276(%rbp), %edx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-3240(%rbp), %r13
	movq	%rax, %rbx
	addq	%rax, %rax
	subl	%ebx, %edx
	leaq	0(%r13,%rax), %rcx
	leaq	(%r14,%rax), %rsi
	addl	%edx, %edx
	call	*48(%r15)
	movl	-3100(%rbp), %eax
	movl	-3224(%rbp), %ebx
	movq	%rax, %rdx
	addq	%rax, %rax
	subl	%edx, %ebx
	leaq	0(%r13,%rax), %rcx
	movl	%ebx, %edx
.L726:
	addl	%edx, %edx
	leaq	(%r14,%rax), %rsi
	movq	%r12, %r8
	movq	%r15, %rdi
	call	*48(%r15)
.L682:
	movl	-3204(%rbp), %r14d
	addl	-3208(%rbp), %r14d
	.p2align 4,,10
	.p2align 3
.L675:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L734
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	.cfi_restore_state
	subq	$8, %rsp
	movzbl	13(%r13), %ecx
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	movzbl	12(%r13), %edx
	pushq	%rax
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	movzbl	15(%r13), %r9d
	movzbl	14(%r13), %r8d
	call	udata_printError_67@PLT
	movl	$16, (%r12)
	popq	%r8
	popq	%r9
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L728:
	movl	-3208(%rbp), %edx
	movq	%r15, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC3(%rip), %rsi
	call	udata_printError_67@PLT
	movl	$3, (%r12)
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L727:
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$8, (%r12)
	jmp	.L675
.L730:
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	udata_printError_67@PLT
	jmp	.L675
.L729:
	movl	%ebx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	xorl	%r14d, %r14d
	movl	$8, (%r12)
	jmp	.L675
.L731:
	movl	-3220(%rbp), %eax
	movq	-3240(%rbp), %rcx
	movl	-3224(%rbp), %edx
	movq	%rax, %rbx
	addq	%rax, %rax
	addq	%rax, %rcx
	subl	%ebx, %edx
	jmp	.L726
.L733:
	movq	-3296(%rbp), %rsi
	cmpq	%rsi, -3232(%rbp)
	je	.L694
	testl	%ebx, %ebx
	je	.L695
	leal	-1(%rbx), %eax
	movq	%r14, -3288(%rbp)
	xorl	%r13d, %r13d
	movq	%rsi, %r14
	addq	%rax, %rax
	movq	%r12, %rbx
	movq	%rax, -3264(%rbp)
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L735:
	movq	-3192(%rbp), %rdi
	addq	$2, %r13
.L697:
	movzwl	2(%rdi,%r13,2), %r9d
	movq	-3232(%rbp), %rax
	movq	%rbx, %r8
	movq	%r15, %rdi
	leaq	(%r14,%r13), %rcx
	movl	$2, %edx
	leaq	(%r9,%r9), %r12
	leaq	(%rax,%r12), %rsi
	call	*48(%r15)
	movq	%rbx, %r8
	movl	$2, %edx
	movq	%r15, %rdi
	movq	-3256(%rbp), %rax
	leaq	(%rax,%r13), %rcx
	movq	-3248(%rbp), %rax
	leaq	(%rax,%r12), %rsi
	call	*48(%r15)
	cmpq	-3264(%rbp), %r13
	jne	.L735
	movq	-3288(%rbp), %r14
	movq	%rbx, %r12
.L696:
	movq	-3192(%rbp), %rdi
	cmpq	-3272(%rbp), %rdi
	jne	.L704
	jmp	.L702
.L686:
	movl	%esi, %r13d
	movq	%r13, %rax
	salq	$2, %r13
	leal	(%rax,%rax), %edi
	addq	%r13, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -3192(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L736
	addq	%rax, %r13
	movl	-3104(%rbp), %eax
	movq	-3240(%rbp), %rsi
	movl	-3108(%rbp), %ebx
	movq	%r13, -3184(%rbp)
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	addq	%r14, %rax
	movl	%ebx, -3276(%rbp)
	movq	%rax, -3248(%rbp)
	movl	%ebx, %eax
	addq	%rax, %rax
	movq	%rdx, -3256(%rbp)
	leaq	(%rsi,%rax), %rbx
	addq	%r14, %rax
	cmpb	$0, 3(%r15)
	movq	%rbx, -3296(%rbp)
	movq	%rax, -3232(%rbp)
	jne	.L737
	leaq	ucnv_io_stripASCIIForCompare_67(%rip), %rax
	movq	%rax, -3176(%rbp)
	leaq	-2064(%rbp), %rax
	movq	%rax, -3272(%rbp)
	jmp	.L707
.L705:
	movl	-3216(%rbp), %edx
	movq	%r15, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC6(%rip), %rsi
	call	udata_printError_67@PLT
	jmp	.L675
.L737:
	leaq	ucnv_io_stripEBCDICForCompare_67(%rip), %rax
	movq	%rax, -3176(%rbp)
	leaq	-2064(%rbp), %rax
	movq	%rax, -3272(%rbp)
	jmp	.L707
.L694:
	movl	-3216(%rbp), %eax
	movq	-3184(%rbp), %rcx
	movq	%rax, %rbx
	addq	%rax, %rax
	movq	%rcx, -3288(%rbp)
	movq	%rax, -3304(%rbp)
	testl	%ebx, %ebx
	je	.L698
	leal	-1(%rbx), %eax
	movq	%r14, -3320(%rbp)
	movq	%r12, %r13
	xorl	%ebx, %ebx
	movq	%rax, -3312(%rbp)
	salq	$2, %rax
	movq	-3232(%rbp), %r12
	movq	%rcx, %r14
	movq	%rax, -3264(%rbp)
	movq	%rcx, -3328(%rbp)
	jmp	.L700
.L738:
	movq	-3192(%rbp), %rdi
	addq	$4, %rbx
.L700:
	movzwl	2(%rdi,%rbx), %eax
	movq	%r14, %rcx
	movq	%r13, %r8
	movl	$2, %edx
	movq	%r15, %rdi
	addq	$2, %r14
	leaq	(%r12,%rax,2), %rsi
	call	*48(%r15)
	cmpq	%rbx, -3264(%rbp)
	jne	.L738
	movq	-3288(%rbp), %rbx
	movq	-3304(%rbp), %rdx
	movq	%r13, %r12
	movq	-3296(%rbp), %rdi
	movq	-3320(%rbp), %r14
	movq	%rbx, %rsi
	movq	-3328(%rbp), %r13
	call	memcpy@PLT
	movq	%rbx, %rax
	movq	-3312(%rbp), %rbx
	movq	%r14, -3264(%rbp)
	movq	%r12, %r14
	movq	-3248(%rbp), %r12
	leaq	2(%rax,%rbx,2), %rax
	xorl	%ebx, %ebx
	movq	%rax, -3232(%rbp)
	.p2align 4,,10
	.p2align 3
.L701:
	movq	-3192(%rbp), %rax
	movq	%r13, %rcx
	movq	%r14, %r8
	movq	%r15, %rdi
	movl	$2, %edx
	addq	$2, %r13
	movzwl	2(%rax,%rbx), %eax
	addq	$4, %rbx
	leaq	(%r12,%rax,2), %rsi
	call	*48(%r15)
	cmpq	%r13, -3232(%rbp)
	jne	.L701
	movq	%r14, %r12
	movq	-3264(%rbp), %r14
.L706:
	movq	-3304(%rbp), %rdx
	movq	-3288(%rbp), %rsi
	movq	-3256(%rbp), %rdi
	call	memcpy@PLT
	jmp	.L696
.L732:
	movq	-3272(%rbp), %rdi
	jmp	.L691
.L734:
	call	__stack_chk_fail@PLT
.L695:
	cmpq	-3272(%rbp), %rdi
	jne	.L704
	jmp	.L703
.L698:
	movq	-3304(%rbp), %rdx
	movq	-3288(%rbp), %rsi
	movq	-3232(%rbp), %rdi
	call	memcpy@PLT
	jmp	.L706
.L736:
	movl	-3216(%rbp), %edx
	movq	%r15, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC5(%rip), %rsi
	call	udata_printError_67@PLT
	movl	$7, (%r12)
	jmp	.L675
	.cfi_endproc
.LFE2821:
	.size	ucnv_swapAliases_67, .-ucnv_swapAliases_67
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL18gEnumAllConverters, @object
	.size	_ZL18gEnumAllConverters, 56
_ZL18gEnumAllConverters:
	.quad	0
	.quad	0
	.quad	ucnv_io_closeUEnumeration
	.quad	ucnv_io_countAllConverters
	.quad	uenum_unextDefault_67
	.quad	ucnv_io_nextAllConverters
	.quad	ucnv_io_resetAllConverters
	.align 32
	.type	_ZL12gEnumAliases, @object
	.size	_ZL12gEnumAliases, 56
_ZL12gEnumAliases:
	.quad	0
	.quad	0
	.quad	ucnv_io_closeUEnumeration
	.quad	ucnv_io_countStandardAliases
	.quad	uenum_unextDefault_67
	.quad	ucnv_io_nextStandardAliases
	.quad	ucnv_io_resetStandardAliases
	.section	.rodata
	.align 32
	.type	_ZL11ebcdicTypes, @object
	.size	_ZL11ebcdicTypes, 128
_ZL11ebcdicTypes:
	.string	""
	.string	"\201\202\203\204\205\206\207\210\211"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\221\222\223\224\225\226\227\230\231"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\242\243\244\245\246\247\250\251"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\201\202\203\204\205\206\207\210\211"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\221\222\223\224\225\226\227\230\231"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\242\243\244\245\246\247\250\251"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\002\002\002\002\002\002\002\002\002"
	.zero	5
	.align 32
	.type	_ZL10asciiTypes, @object
	.size	_ZL10asciiTypes, 128
_ZL10asciiTypes:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\002\002\002\002\002\002\002\002\002"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"abcdefghijklmnopqrstuvwxyz"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"abcdefghijklmnopqrstuvwxyz"
	.zero	4
	.local	_ZL10gMainTable
	.comm	_ZL10gMainTable,112,32
	.align 2
	.type	_ZL19defaultTableOptions, @object
	.size	_ZL19defaultTableOptions, 4
_ZL19defaultTableOptions:
	.zero	4
	.local	_ZL18gAliasDataInitOnce
	.comm	_ZL18gAliasDataInitOnce,8,8
	.local	_ZL10gAliasData
	.comm	_ZL10gAliasData,8,8
	.type	_ZL9DATA_TYPE, @object
	.size	_ZL9DATA_TYPE, 4
_ZL9DATA_TYPE:
	.string	"icu"
	.align 8
	.type	_ZL9DATA_NAME, @object
	.size	_ZL9DATA_NAME, 9
_ZL9DATA_NAME:
	.string	"cnvalias"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
