	.file	"resbund.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle17getDynamicClassIDEv
	.type	_ZNK6icu_6714ResourceBundle17getDynamicClassIDEv, @function
_ZNK6icu_6714ResourceBundle17getDynamicClassIDEv:
.LFB3099:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714ResourceBundle16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3099:
	.size	_ZNK6icu_6714ResourceBundle17getDynamicClassIDEv, .-_ZNK6icu_6714ResourceBundle17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundleD2Ev
	.type	_ZN6icu_6714ResourceBundleD2Ev, @function
_ZN6icu_6714ResourceBundleD2Ev:
.LFB3117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	call	ures_close_67@PLT
.L4:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	call	*8(%rax)
.L5:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3117:
	.size	_ZN6icu_6714ResourceBundleD2Ev, .-_ZN6icu_6714ResourceBundleD2Ev
	.globl	_ZN6icu_6714ResourceBundleD1Ev
	.set	_ZN6icu_6714ResourceBundleD1Ev,_ZN6icu_6714ResourceBundleD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundleD0Ev
	.type	_ZN6icu_6714ResourceBundleD0Ev, @function
_ZN6icu_6714ResourceBundleD0Ev:
.LFB3119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	ures_close_67@PLT
.L14:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	call	*8(%rax)
.L15:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3119:
	.size	_ZN6icu_6714ResourceBundleD0Ev, .-_ZN6icu_6714ResourceBundleD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundle16getStaticClassIDEv
	.type	_ZN6icu_6714ResourceBundle16getStaticClassIDEv, @function
_ZN6icu_6714ResourceBundle16getStaticClassIDEv:
.LFB3098:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714ResourceBundle16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3098:
	.size	_ZN6icu_6714ResourceBundle16getStaticClassIDEv, .-_ZN6icu_6714ResourceBundle16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundleC2ER10UErrorCode
	.type	_ZN6icu_6714ResourceBundleC2ER10UErrorCode, @function
_ZN6icu_6714ResourceBundleC2ER10UErrorCode:
.LFB3101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%r12, %rdx
	xorl	%edi, %edi
	movq	40(%rax), %rsi
	call	ures_open_67@PLT
	movq	%rax, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3101:
	.size	_ZN6icu_6714ResourceBundleC2ER10UErrorCode, .-_ZN6icu_6714ResourceBundleC2ER10UErrorCode
	.globl	_ZN6icu_6714ResourceBundleC1ER10UErrorCode
	.set	_ZN6icu_6714ResourceBundleC1ER10UErrorCode,_ZN6icu_6714ResourceBundleC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundleC2ERKS0_
	.type	_ZN6icu_6714ResourceBundleC2ERKS0_, @function
_ZN6icu_6714ResourceBundleC2ERKS0_:
.LFB3107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movl	$0, -28(%rbp)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	testq	%rsi, %rsi
	je	.L27
	leaq	-28(%rbp), %rdx
	xorl	%edi, %edi
	call	ures_copyResb_67@PLT
	movq	%rax, 8(%rbx)
.L26:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	$0, 8(%rdi)
	jmp	.L26
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3107:
	.size	_ZN6icu_6714ResourceBundleC2ERKS0_, .-_ZN6icu_6714ResourceBundleC2ERKS0_
	.globl	_ZN6icu_6714ResourceBundleC1ERKS0_
	.set	_ZN6icu_6714ResourceBundleC1ERKS0_,_ZN6icu_6714ResourceBundleC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundleC2EP15UResourceBundleR10UErrorCode
	.type	_ZN6icu_6714ResourceBundleC2EP15UResourceBundleR10UErrorCode, @function
_ZN6icu_6714ResourceBundleC2EP15UResourceBundleR10UErrorCode:
.LFB3110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	testq	%rsi, %rsi
	je	.L33
	xorl	%edi, %edi
	call	ures_copyResb_67@PLT
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	$0, 8(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3110:
	.size	_ZN6icu_6714ResourceBundleC2EP15UResourceBundleR10UErrorCode, .-_ZN6icu_6714ResourceBundleC2EP15UResourceBundleR10UErrorCode
	.globl	_ZN6icu_6714ResourceBundleC1EP15UResourceBundleR10UErrorCode
	.set	_ZN6icu_6714ResourceBundleC1EP15UResourceBundleR10UErrorCode,_ZN6icu_6714ResourceBundleC2EP15UResourceBundleR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundleC2EPKcRKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714ResourceBundleC2EPKcRKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714ResourceBundleC2EPKcRKNS_6LocaleER10UErrorCode:
.LFB3113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	%rdx, %r8
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rax, (%rbx)
	movq	40(%r8), %rsi
	movq	$0, 16(%rbx)
	call	ures_open_67@PLT
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3113:
	.size	_ZN6icu_6714ResourceBundleC2EPKcRKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714ResourceBundleC2EPKcRKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6714ResourceBundleC1EPKcRKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6714ResourceBundleC1EPKcRKNS_6LocaleER10UErrorCode,_ZN6icu_6714ResourceBundleC2EPKcRKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundleaSERKS0_
	.type	_ZN6icu_6714ResourceBundleaSERKS0_, @function
_ZN6icu_6714ResourceBundleaSERKS0_:
.LFB3115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L39
	movq	8(%rdi), %rdi
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L40
	call	ures_close_67@PLT
	movq	$0, 8(%r12)
.L40:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L41
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 16(%r12)
.L41:
	movq	8(%rbx), %rsi
	movl	$0, -28(%rbp)
	testq	%rsi, %rsi
	je	.L42
	leaq	-28(%rbp), %rdx
	xorl	%edi, %edi
	call	ures_copyResb_67@PLT
	movq	%rax, 8(%r12)
.L39:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	$0, 8(%r12)
	jmp	.L39
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3115:
	.size	_ZN6icu_6714ResourceBundleaSERKS0_, .-_ZN6icu_6714ResourceBundleaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle5cloneEv
	.type	_ZNK6icu_6714ResourceBundle5cloneEv, @function
_ZNK6icu_6714ResourceBundle5cloneEv:
.LFB3120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L52
	movq	8(%rbx), %rsi
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	$0, 16(%r12)
	movq	%rax, (%r12)
	movl	$0, -28(%rbp)
	testq	%rsi, %rsi
	je	.L54
	leaq	-28(%rbp), %rdx
	xorl	%edi, %edi
	call	ures_copyResb_67@PLT
	movq	%rax, 8(%r12)
.L52:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	$0, 8(%r12)
	jmp	.L52
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3120:
	.size	_ZNK6icu_6714ResourceBundle5cloneEv, .-_ZNK6icu_6714ResourceBundle5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle9getStringER10UErrorCode
	.type	_ZNK6icu_6714ResourceBundle9getStringER10UErrorCode, @function
_ZNK6icu_6714ResourceBundle9getStringER10UErrorCode:
.LFB3121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-36(%rbp), %rsi
	subq	$40, %rsp
	movq	8(%r8), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -36(%rbp)
	call	ures_getString_67@PLT
	movl	-36(%rbp), %ecx
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3121:
	.size	_ZNK6icu_6714ResourceBundle9getStringER10UErrorCode, .-_ZNK6icu_6714ResourceBundle9getStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle9getBinaryERiR10UErrorCode
	.type	_ZNK6icu_6714ResourceBundle9getBinaryERiR10UErrorCode, @function
_ZNK6icu_6714ResourceBundle9getBinaryERiR10UErrorCode:
.LFB3122:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_getBinary_67@PLT
	.cfi_endproc
.LFE3122:
	.size	_ZNK6icu_6714ResourceBundle9getBinaryERiR10UErrorCode, .-_ZNK6icu_6714ResourceBundle9getBinaryERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle12getIntVectorERiR10UErrorCode
	.type	_ZNK6icu_6714ResourceBundle12getIntVectorERiR10UErrorCode, @function
_ZNK6icu_6714ResourceBundle12getIntVectorERiR10UErrorCode:
.LFB3123:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_getIntVector_67@PLT
	.cfi_endproc
.LFE3123:
	.size	_ZNK6icu_6714ResourceBundle12getIntVectorERiR10UErrorCode, .-_ZNK6icu_6714ResourceBundle12getIntVectorERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle7getUIntER10UErrorCode
	.type	_ZNK6icu_6714ResourceBundle7getUIntER10UErrorCode, @function
_ZNK6icu_6714ResourceBundle7getUIntER10UErrorCode:
.LFB3124:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_getUInt_67@PLT
	.cfi_endproc
.LFE3124:
	.size	_ZNK6icu_6714ResourceBundle7getUIntER10UErrorCode, .-_ZNK6icu_6714ResourceBundle7getUIntER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle6getIntER10UErrorCode
	.type	_ZNK6icu_6714ResourceBundle6getIntER10UErrorCode, @function
_ZNK6icu_6714ResourceBundle6getIntER10UErrorCode:
.LFB3125:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_getInt_67@PLT
	.cfi_endproc
.LFE3125:
	.size	_ZNK6icu_6714ResourceBundle6getIntER10UErrorCode, .-_ZNK6icu_6714ResourceBundle6getIntER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle7getNameEv
	.type	_ZNK6icu_6714ResourceBundle7getNameEv, @function
_ZNK6icu_6714ResourceBundle7getNameEv:
.LFB3126:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_getName_67@PLT
	.cfi_endproc
.LFE3126:
	.size	_ZNK6icu_6714ResourceBundle7getNameEv, .-_ZNK6icu_6714ResourceBundle7getNameEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle6getKeyEv
	.type	_ZNK6icu_6714ResourceBundle6getKeyEv, @function
_ZNK6icu_6714ResourceBundle6getKeyEv:
.LFB3127:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_getKey_67@PLT
	.cfi_endproc
.LFE3127:
	.size	_ZNK6icu_6714ResourceBundle6getKeyEv, .-_ZNK6icu_6714ResourceBundle6getKeyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle7getTypeEv
	.type	_ZNK6icu_6714ResourceBundle7getTypeEv, @function
_ZNK6icu_6714ResourceBundle7getTypeEv:
.LFB3128:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_getType_67@PLT
	.cfi_endproc
.LFE3128:
	.size	_ZNK6icu_6714ResourceBundle7getTypeEv, .-_ZNK6icu_6714ResourceBundle7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle7getSizeEv
	.type	_ZNK6icu_6714ResourceBundle7getSizeEv, @function
_ZNK6icu_6714ResourceBundle7getSizeEv:
.LFB3129:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_getSize_67@PLT
	.cfi_endproc
.LFE3129:
	.size	_ZNK6icu_6714ResourceBundle7getSizeEv, .-_ZNK6icu_6714ResourceBundle7getSizeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle7hasNextEv
	.type	_ZNK6icu_6714ResourceBundle7hasNextEv, @function
_ZNK6icu_6714ResourceBundle7hasNextEv:
.LFB3130:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_hasNext_67@PLT
	.cfi_endproc
.LFE3130:
	.size	_ZNK6icu_6714ResourceBundle7hasNextEv, .-_ZNK6icu_6714ResourceBundle7hasNextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundle13resetIteratorEv
	.type	_ZN6icu_6714ResourceBundle13resetIteratorEv, @function
_ZN6icu_6714ResourceBundle13resetIteratorEv:
.LFB3131:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_resetIterator_67@PLT
	.cfi_endproc
.LFE3131:
	.size	_ZN6icu_6714ResourceBundle13resetIteratorEv, .-_ZN6icu_6714ResourceBundle13resetIteratorEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundle7getNextER10UErrorCode
	.type	_ZN6icu_6714ResourceBundle7getNextER10UErrorCode, @function
_ZN6icu_6714ResourceBundle7getNextER10UErrorCode:
.LFB3132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-240(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$208, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ures_initStackObject_67@PLT
	movq	8(%r14), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	ures_getNextResource_67@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	movq	$0, 16(%r12)
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	%rax, (%r12)
	call	ures_copyResb_67@PLT
	movq	%rax, 8(%r12)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L80
.L76:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$208, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L76
.L81:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3132:
	.size	_ZN6icu_6714ResourceBundle7getNextER10UErrorCode, .-_ZN6icu_6714ResourceBundle7getNextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundle13getNextStringER10UErrorCode
	.type	_ZN6icu_6714ResourceBundle13getNextStringER10UErrorCode, @function
_ZN6icu_6714ResourceBundle13getNextStringER10UErrorCode:
.LFB3133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-36(%rbp), %rsi
	subq	$40, %rsp
	movq	8(%r8), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -36(%rbp)
	call	ures_getNextString_67@PLT
	movl	-36(%rbp), %ecx
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3133:
	.size	_ZN6icu_6714ResourceBundle13getNextStringER10UErrorCode, .-_ZN6icu_6714ResourceBundle13getNextStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundle13getNextStringEPPKcR10UErrorCode
	.type	_ZN6icu_6714ResourceBundle13getNextStringEPPKcR10UErrorCode, @function
_ZN6icu_6714ResourceBundle13getNextStringEPPKcR10UErrorCode:
.LFB3134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-36(%rbp), %rsi
	subq	$40, %rsp
	movq	8(%r8), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -36(%rbp)
	call	ures_getNextString_67@PLT
	movl	-36(%rbp), %ecx
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3134:
	.size	_ZN6icu_6714ResourceBundle13getNextStringEPPKcR10UErrorCode, .-_ZN6icu_6714ResourceBundle13getNextStringEPPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle3getEiR10UErrorCode
	.type	_ZNK6icu_6714ResourceBundle3getEiR10UErrorCode, @function
_ZNK6icu_6714ResourceBundle3getEiR10UErrorCode:
.LFB3135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-256(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ures_initStackObject_67@PLT
	movq	8(%r15), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movl	%r14d, %esi
	call	ures_getByIndex_67@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	movq	$0, 16(%r12)
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	%rax, (%r12)
	call	ures_copyResb_67@PLT
	movq	%rax, 8(%r12)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L94
.L90:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L90
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3135:
	.size	_ZNK6icu_6714ResourceBundle3getEiR10UErrorCode, .-_ZNK6icu_6714ResourceBundle3getEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle11getStringExEiR10UErrorCode
	.type	_ZNK6icu_6714ResourceBundle11getStringExEiR10UErrorCode, @function
_ZNK6icu_6714ResourceBundle11getStringExEiR10UErrorCode:
.LFB3136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movl	%edx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-36(%rbp), %rdx
	subq	$40, %rsp
	movq	8(%r8), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -36(%rbp)
	call	ures_getStringByIndex_67@PLT
	movl	-36(%rbp), %ecx
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L99:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3136:
	.size	_ZNK6icu_6714ResourceBundle11getStringExEiR10UErrorCode, .-_ZNK6icu_6714ResourceBundle11getStringExEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle3getEPKcR10UErrorCode
	.type	_ZNK6icu_6714ResourceBundle3getEPKcR10UErrorCode, @function
_ZNK6icu_6714ResourceBundle3getEPKcR10UErrorCode:
.LFB3137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-256(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ures_initStackObject_67@PLT
	movq	8(%r15), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	ures_getByKey_67@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	movq	$0, 16(%r12)
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	%rax, (%r12)
	call	ures_copyResb_67@PLT
	movq	%rax, 8(%r12)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L104
.L100:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L100
.L105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3137:
	.size	_ZNK6icu_6714ResourceBundle3getEPKcR10UErrorCode, .-_ZNK6icu_6714ResourceBundle3getEPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundle15getWithFallbackEPKcR10UErrorCode
	.type	_ZN6icu_6714ResourceBundle15getWithFallbackEPKcR10UErrorCode, @function
_ZN6icu_6714ResourceBundle15getWithFallbackEPKcR10UErrorCode:
.LFB3138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-256(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ures_initStackObject_67@PLT
	movq	8(%r15), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	ures_getByKeyWithFallback_67@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	movq	$0, 16(%r12)
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	%rax, (%r12)
	call	ures_copyResb_67@PLT
	movq	%rax, 8(%r12)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L110
.L106:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L106
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3138:
	.size	_ZN6icu_6714ResourceBundle15getWithFallbackEPKcR10UErrorCode, .-_ZN6icu_6714ResourceBundle15getWithFallbackEPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle11getStringExEPKcR10UErrorCode
	.type	_ZNK6icu_6714ResourceBundle11getStringExEPKcR10UErrorCode, @function
_ZNK6icu_6714ResourceBundle11getStringExEPKcR10UErrorCode:
.LFB3139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-36(%rbp), %rdx
	subq	$40, %rsp
	movq	8(%r8), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -36(%rbp)
	call	ures_getStringByKey_67@PLT
	movl	-36(%rbp), %ecx
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3139:
	.size	_ZNK6icu_6714ResourceBundle11getStringExEPKcR10UErrorCode, .-_ZNK6icu_6714ResourceBundle11getStringExEPKcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle16getVersionNumberEv
	.type	_ZNK6icu_6714ResourceBundle16getVersionNumberEv, @function
_ZNK6icu_6714ResourceBundle16getVersionNumberEv:
.LFB3140:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_getVersionNumberInternal_67@PLT
	.cfi_endproc
.LFE3140:
	.size	_ZNK6icu_6714ResourceBundle16getVersionNumberEv, .-_ZNK6icu_6714ResourceBundle16getVersionNumberEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle10getVersionEPh
	.type	_ZNK6icu_6714ResourceBundle10getVersionEPh, @function
_ZNK6icu_6714ResourceBundle10getVersionEPh:
.LFB3141:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	ures_getVersion_67@PLT
	.cfi_endproc
.LFE3141:
	.size	_ZNK6icu_6714ResourceBundle10getVersionEPh, .-_ZNK6icu_6714ResourceBundle10getVersionEPh
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle9getLocaleEv
	.type	_ZNK6icu_6714ResourceBundle9getLocaleEv, @function
_ZNK6icu_6714ResourceBundle9getLocaleEv:
.LFB3142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	_ZZNK6icu_6714ResourceBundle9getLocaleEvE11gLocaleLock(%rip), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	umtx_lock_67@PLT
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L124
.L119:
	leaq	_ZZNK6icu_6714ResourceBundle9getLocaleEvE11gLocaleLock(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	leaq	-44(%rbp), %rsi
	movl	$0, -44(%rbp)
	call	ures_getLocaleInternal_67@PLT
	movl	$224, %edi
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L120
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r12, 16(%rbx)
	jmp	.L119
.L125:
	call	__stack_chk_fail@PLT
.L120:
	movq	$0, 16(%rbx)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%rax, %r12
	jmp	.L119
	.cfi_endproc
.LFE3142:
	.size	_ZNK6icu_6714ResourceBundle9getLocaleEv, .-_ZNK6icu_6714ResourceBundle9getLocaleEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714ResourceBundle9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_6714ResourceBundle9getLocaleE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_6714ResourceBundle9getLocaleE18ULocDataLocaleTypeR10UErrorCode:
.LFB3149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movl	%edx, %esi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%r8), %rdi
	call	ures_getLocaleByType_67@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3149:
	.size	_ZNK6icu_6714ResourceBundle9getLocaleE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_6714ResourceBundle9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.weak	_ZTSN6icu_6714ResourceBundleE
	.section	.rodata._ZTSN6icu_6714ResourceBundleE,"aG",@progbits,_ZTSN6icu_6714ResourceBundleE,comdat
	.align 16
	.type	_ZTSN6icu_6714ResourceBundleE, @object
	.size	_ZTSN6icu_6714ResourceBundleE, 26
_ZTSN6icu_6714ResourceBundleE:
	.string	"N6icu_6714ResourceBundleE"
	.weak	_ZTIN6icu_6714ResourceBundleE
	.section	.data.rel.ro._ZTIN6icu_6714ResourceBundleE,"awG",@progbits,_ZTIN6icu_6714ResourceBundleE,comdat
	.align 8
	.type	_ZTIN6icu_6714ResourceBundleE, @object
	.size	_ZTIN6icu_6714ResourceBundleE, 24
_ZTIN6icu_6714ResourceBundleE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714ResourceBundleE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6714ResourceBundleE
	.section	.data.rel.ro.local._ZTVN6icu_6714ResourceBundleE,"awG",@progbits,_ZTVN6icu_6714ResourceBundleE,comdat
	.align 8
	.type	_ZTVN6icu_6714ResourceBundleE, @object
	.size	_ZTVN6icu_6714ResourceBundleE, 40
_ZTVN6icu_6714ResourceBundleE:
	.quad	0
	.quad	_ZTIN6icu_6714ResourceBundleE
	.quad	_ZN6icu_6714ResourceBundleD1Ev
	.quad	_ZN6icu_6714ResourceBundleD0Ev
	.quad	_ZNK6icu_6714ResourceBundle17getDynamicClassIDEv
	.local	_ZZNK6icu_6714ResourceBundle9getLocaleEvE11gLocaleLock
	.comm	_ZZNK6icu_6714ResourceBundle9getLocaleEvE11gLocaleLock,56,32
	.local	_ZZN6icu_6714ResourceBundle16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714ResourceBundle16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
