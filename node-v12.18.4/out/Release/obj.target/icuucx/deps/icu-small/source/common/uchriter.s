	.file	"uchriter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UCharCharacterIterator17getDynamicClassIDEv
	.type	_ZNK6icu_6722UCharCharacterIterator17getDynamicClassIDEv, @function
_ZNK6icu_6722UCharCharacterIterator17getDynamicClassIDEv:
.LFB1310:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722UCharCharacterIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE1310:
	.size	_ZNK6icu_6722UCharCharacterIterator17getDynamicClassIDEv, .-_ZNK6icu_6722UCharCharacterIterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator5firstEv
	.type	_ZN6icu_6722UCharCharacterIterator5firstEv, @function
_ZN6icu_6722UCharCharacterIterator5firstEv:
.LFB1334:
	.cfi_startproc
	endbr64
	movslq	16(%rdi), %rax
	movl	$-1, %r8d
	movl	%eax, 12(%rdi)
	cmpl	20(%rdi), %eax
	jge	.L3
	movq	24(%rdi), %rdx
	movzwl	(%rdx,%rax,2), %r8d
.L3:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1334:
	.size	_ZN6icu_6722UCharCharacterIterator5firstEv, .-_ZN6icu_6722UCharCharacterIterator5firstEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator12firstPostIncEv
	.type	_ZN6icu_6722UCharCharacterIterator12firstPostIncEv, @function
_ZN6icu_6722UCharCharacterIterator12firstPostIncEv:
.LFB1335:
	.cfi_startproc
	endbr64
	movslq	16(%rdi), %rax
	movl	$-1, %r8d
	movl	%eax, 12(%rdi)
	cmpl	20(%rdi), %eax
	jge	.L6
	movq	24(%rdi), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 12(%rdi)
	movzwl	(%rdx,%rax,2), %r8d
.L6:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1335:
	.size	_ZN6icu_6722UCharCharacterIterator12firstPostIncEv, .-_ZN6icu_6722UCharCharacterIterator12firstPostIncEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator4lastEv
	.type	_ZN6icu_6722UCharCharacterIterator4lastEv, @function
_ZN6icu_6722UCharCharacterIterator4lastEv:
.LFB1336:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	movl	$-1, %r8d
	movl	%eax, 12(%rdi)
	cmpl	16(%rdi), %eax
	jle	.L9
	subl	$1, %eax
	movq	24(%rdi), %rdx
	movl	%eax, 12(%rdi)
	cltq
	movzwl	(%rdx,%rax,2), %r8d
.L9:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1336:
	.size	_ZN6icu_6722UCharCharacterIterator4lastEv, .-_ZN6icu_6722UCharCharacterIterator4lastEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator8setIndexEi
	.type	_ZN6icu_6722UCharCharacterIterator8setIndexEi, @function
_ZN6icu_6722UCharCharacterIterator8setIndexEi:
.LFB1337:
	.cfi_startproc
	endbr64
	movslq	16(%rdi), %rax
	movl	20(%rdi), %edx
	cmpl	%esi, %eax
	jle	.L13
	movl	%eax, 12(%rdi)
	cmpl	%eax, %edx
	jle	.L17
.L18:
	movq	24(%rdi), %rdx
	movzwl	(%rdx,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	cmpl	%edx, %esi
	jle	.L15
	movl	%edx, 12(%rdi)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movslq	%esi, %rax
	movl	%esi, 12(%rdi)
	cmpl	%eax, %edx
	jg	.L18
.L17:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1337:
	.size	_ZN6icu_6722UCharCharacterIterator8setIndexEi, .-_ZN6icu_6722UCharCharacterIterator8setIndexEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UCharCharacterIterator7currentEv
	.type	_ZNK6icu_6722UCharCharacterIterator7currentEv, @function
_ZNK6icu_6722UCharCharacterIterator7currentEv:
.LFB1338:
	.cfi_startproc
	endbr64
	movslq	12(%rdi), %rax
	movl	$-1, %r8d
	cmpl	16(%rdi), %eax
	jl	.L19
	cmpl	20(%rdi), %eax
	jge	.L19
	movq	24(%rdi), %rdx
	movzwl	(%rdx,%rax,2), %r8d
.L19:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1338:
	.size	_ZNK6icu_6722UCharCharacterIterator7currentEv, .-_ZNK6icu_6722UCharCharacterIterator7currentEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator4nextEv
	.type	_ZN6icu_6722UCharCharacterIterator4nextEv, @function
_ZN6icu_6722UCharCharacterIterator4nextEv:
.LFB1339:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	movl	20(%rdi), %edx
	addl	$1, %eax
	cmpl	%edx, %eax
	jge	.L25
	movq	24(%rdi), %rcx
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %r8d
	movl	%eax, 12(%rdi)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movl	%edx, %eax
	movl	$-1, %r8d
	movl	%eax, 12(%rdi)
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1339:
	.size	_ZN6icu_6722UCharCharacterIterator4nextEv, .-_ZN6icu_6722UCharCharacterIterator4nextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator11nextPostIncEv
	.type	_ZN6icu_6722UCharCharacterIterator11nextPostIncEv, @function
_ZN6icu_6722UCharCharacterIterator11nextPostIncEv:
.LFB1340:
	.cfi_startproc
	endbr64
	movslq	12(%rdi), %rax
	movl	$-1, %r8d
	cmpl	20(%rdi), %eax
	jge	.L26
	movq	24(%rdi), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 12(%rdi)
	movzwl	(%rdx,%rax,2), %r8d
.L26:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1340:
	.size	_ZN6icu_6722UCharCharacterIterator11nextPostIncEv, .-_ZN6icu_6722UCharCharacterIterator11nextPostIncEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator7hasNextEv
	.type	_ZN6icu_6722UCharCharacterIterator7hasNextEv, @function
_ZN6icu_6722UCharCharacterIterator7hasNextEv:
.LFB1341:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	cmpl	%eax, 12(%rdi)
	setl	%al
	ret
	.cfi_endproc
.LFE1341:
	.size	_ZN6icu_6722UCharCharacterIterator7hasNextEv, .-_ZN6icu_6722UCharCharacterIterator7hasNextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator8previousEv
	.type	_ZN6icu_6722UCharCharacterIterator8previousEv, @function
_ZN6icu_6722UCharCharacterIterator8previousEv:
.LFB1342:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	movl	$-1, %r8d
	cmpl	16(%rdi), %eax
	jle	.L30
	subl	$1, %eax
	movq	24(%rdi), %rdx
	movl	%eax, 12(%rdi)
	cltq
	movzwl	(%rdx,%rax,2), %r8d
.L30:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1342:
	.size	_ZN6icu_6722UCharCharacterIterator8previousEv, .-_ZN6icu_6722UCharCharacterIterator8previousEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator11hasPreviousEv
	.type	_ZN6icu_6722UCharCharacterIterator11hasPreviousEv, @function
_ZN6icu_6722UCharCharacterIterator11hasPreviousEv:
.LFB1343:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	cmpl	%eax, 12(%rdi)
	setg	%al
	ret
	.cfi_endproc
.LFE1343:
	.size	_ZN6icu_6722UCharCharacterIterator11hasPreviousEv, .-_ZN6icu_6722UCharCharacterIterator11hasPreviousEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator7first32Ev
	.type	_ZN6icu_6722UCharCharacterIterator7first32Ev, @function
_ZN6icu_6722UCharCharacterIterator7first32Ev:
.LFB1344:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	movl	20(%rdi), %edx
	movl	$65535, %r8d
	movl	%eax, 12(%rdi)
	cmpl	%edx, %eax
	jge	.L34
	movq	24(%rdi), %rsi
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx), %rdi
	movzwl	(%rsi,%rcx,2), %r8d
	movl	%r8d, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L34
	addl	$1, %eax
	cmpl	%edx, %eax
	jne	.L43
.L34:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movzwl	2(%rsi,%rdi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L34
	sall	$10, %r8d
	leal	-56613888(%rax,%r8), %r8d
	jmp	.L34
	.cfi_endproc
.LFE1344:
	.size	_ZN6icu_6722UCharCharacterIterator7first32Ev, .-_ZN6icu_6722UCharCharacterIterator7first32Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator14first32PostIncEv
	.type	_ZN6icu_6722UCharCharacterIterator14first32PostIncEv, @function
_ZN6icu_6722UCharCharacterIterator14first32PostIncEv:
.LFB1345:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	movl	20(%rdi), %edx
	movl	$65535, %r8d
	movl	%eax, 12(%rdi)
	cmpl	%edx, %eax
	jge	.L44
	movq	24(%rdi), %r9
	movslq	%eax, %rcx
	leal	1(%rax), %esi
	leaq	(%rcx,%rcx), %r10
	movl	%esi, 12(%rdi)
	movzwl	(%r9,%rcx,2), %r8d
	movl	%r8d, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L44
	cmpl	%esi, %edx
	jne	.L53
.L44:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movzwl	2(%r9,%r10), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L44
	addl	$2, %eax
	sall	$10, %r8d
	movl	%eax, 12(%rdi)
	leal	-56613888(%rdx,%r8), %r8d
	jmp	.L44
	.cfi_endproc
.LFE1345:
	.size	_ZN6icu_6722UCharCharacterIterator14first32PostIncEv, .-_ZN6icu_6722UCharCharacterIterator14first32PostIncEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator6last32Ev
	.type	_ZN6icu_6722UCharCharacterIterator6last32Ev, @function
_ZN6icu_6722UCharCharacterIterator6last32Ev:
.LFB1346:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	movl	16(%rdi), %edx
	movl	$65535, %r8d
	movl	%eax, 12(%rdi)
	cmpl	%edx, %eax
	jle	.L54
	leal	-1(%rax), %ecx
	movq	24(%rdi), %r9
	movslq	%ecx, %rsi
	movl	%ecx, 12(%rdi)
	movzwl	(%r9,%rsi,2), %r8d
	leaq	(%rsi,%rsi), %r10
	movl	%r8d, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L54
	cmpl	%ecx, %edx
	jl	.L63
.L54:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movzwl	-2(%r9,%r10), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L54
	subl	$2, %eax
	sall	$10, %edx
	movl	%eax, 12(%rdi)
	leal	-56613888(%r8,%rdx), %r8d
	jmp	.L54
	.cfi_endproc
.LFE1346:
	.size	_ZN6icu_6722UCharCharacterIterator6last32Ev, .-_ZN6icu_6722UCharCharacterIterator6last32Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator10setIndex32Ei
	.type	_ZN6icu_6722UCharCharacterIterator10setIndex32Ei, @function
_ZN6icu_6722UCharCharacterIterator10setIndex32Ei:
.LFB1347:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %edx
	movl	20(%rdi), %r8d
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	cmpl	%esi, %edx
	jg	.L65
	cmpl	%r8d, %esi
	movl	%r8d, %ecx
	cmovle	%esi, %ecx
	cmpl	%r8d, %ecx
	jge	.L66
	movq	24(%rdi), %r9
	movslq	%ecx, %r11
	leaq	(%r11,%r11), %rbx
	movzwl	(%r9,%r11,2), %eax
	movl	%eax, %r10d
	andl	$-1024, %r10d
	cmpl	%ecx, %edx
	jge	.L76
	cmpl	$56320, %r10d
	jne	.L76
	movzwl	-2(%r9,%rbx), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L90
	movl	%ecx, 12(%rdi)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L65:
	movl	%edx, %ecx
	cmpl	%r8d, %edx
	jge	.L66
	movq	24(%rdi), %r9
	movslq	%edx, %rax
	leal	1(%rdx), %esi
	movzwl	(%r9,%rax,2), %eax
	movl	%eax, %r10d
	andl	$-1024, %r10d
.L69:
	movl	%edx, 12(%rdi)
	cmpl	$55296, %r10d
	jne	.L64
	cmpl	%esi, %r8d
	jne	.L91
.L64:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$65535, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%ecx, 12(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	leal	1(%rcx), %esi
	movl	%ecx, %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L91:
	movslq	%esi, %rsi
	movzwl	(%r9,%rsi,2), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L64
.L75:
	sall	$10, %eax
	leal	-56613888(%rax,%rdx), %eax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L90:
	subl	$1, %ecx
	movslq	%ecx, %rax
	movzwl	(%r9,%rax,2), %eax
	movl	%ecx, 12(%rdi)
	movl	%eax, %edx
	andl	$64512, %edx
	cmpl	$55296, %edx
	jne	.L64
	cmpl	%r8d, %esi
	jge	.L64
	movzwl	(%r9,%r11,2), %edx
	jmp	.L75
	.cfi_endproc
.LFE1347:
	.size	_ZN6icu_6722UCharCharacterIterator10setIndex32Ei, .-_ZN6icu_6722UCharCharacterIterator10setIndex32Ei
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UCharCharacterIterator9current32Ev
	.type	_ZNK6icu_6722UCharCharacterIterator9current32Ev, @function
_ZNK6icu_6722UCharCharacterIterator9current32Ev:
.LFB1348:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	movl	16(%rdi), %edx
	movl	$65535, %r8d
	cmpl	%edx, %eax
	jl	.L92
	movl	20(%rdi), %esi
	cmpl	%esi, %eax
	jge	.L92
	movq	24(%rdi), %rdi
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx), %r10
	movzwl	(%rdi,%rcx,2), %r8d
	movl	%r8d, %r9d
	movl	%r8d, %ecx
	andl	$63488, %r9d
	cmpl	$55296, %r9d
	je	.L97
.L92:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	andb	$4, %ch
	jne	.L94
	addl	$1, %eax
	cmpl	%eax, %esi
	je	.L92
	movzwl	2(%rdi,%r10), %edx
	movl	%edx, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L92
	sall	$10, %r8d
	leal	-56613888(%rdx,%r8), %r8d
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L94:
	cmpl	%edx, %eax
	jle	.L92
	movzwl	-2(%rdi,%r10), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L92
	sall	$10, %eax
	leal	-56613888(%r8,%rax), %r8d
	jmp	.L92
	.cfi_endproc
.LFE1348:
	.size	_ZNK6icu_6722UCharCharacterIterator9current32Ev, .-_ZNK6icu_6722UCharCharacterIterator9current32Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator6next32Ev
	.type	_ZN6icu_6722UCharCharacterIterator6next32Ev, @function
_ZN6icu_6722UCharCharacterIterator6next32Ev:
.LFB1349:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movl	20(%rdi), %eax
	cmpl	%eax, %edx
	jge	.L99
	movq	24(%rdi), %r9
	movslq	%edx, %rsi
	leal	1(%rdx), %ecx
	leaq	(%rsi,%rsi), %r8
	movl	%ecx, 12(%rdi)
	movzwl	(%r9,%rsi,2), %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L100
	cmpl	%ecx, %eax
	jne	.L114
.L100:
	cmpl	%ecx, %eax
	jg	.L115
.L99:
	movl	%eax, 12(%rdi)
	movl	$65535, %r8d
.L98:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	movslq	%ecx, %rdx
	movzwl	(%r9,%rdx,2), %r8d
	leaq	(%rdx,%rdx), %rsi
	movl	%r8d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L98
	addl	$1, %ecx
	cmpl	%eax, %ecx
	je	.L98
	movzwl	2(%r9,%rsi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L98
	sall	$10, %r8d
	leal	-56613888(%rax,%r8), %r8d
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L114:
	movzwl	2(%r9,%r8), %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L100
	leal	2(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L100
	.cfi_endproc
.LFE1349:
	.size	_ZN6icu_6722UCharCharacterIterator6next32Ev, .-_ZN6icu_6722UCharCharacterIterator6next32Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator13next32PostIncEv
	.type	_ZN6icu_6722UCharCharacterIterator13next32PostIncEv, @function
_ZN6icu_6722UCharCharacterIterator13next32PostIncEv:
.LFB1350:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movl	20(%rdi), %ecx
	movl	$65535, %eax
	cmpl	%ecx, %edx
	jge	.L116
	movq	24(%rdi), %r8
	movslq	%edx, %rax
	leal	1(%rdx), %esi
	leaq	(%rax,%rax), %r10
	movl	%esi, 12(%rdi)
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %r9d
	andl	$-1024, %r9d
	cmpl	$55296, %r9d
	jne	.L116
	cmpl	%esi, %ecx
	jne	.L125
.L116:
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	movzwl	2(%r8,%r10), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L116
	addl	$2, %edx
	sall	$10, %eax
	movl	%edx, 12(%rdi)
	leal	-56613888(%rcx,%rax), %eax
	ret
	.cfi_endproc
.LFE1350:
	.size	_ZN6icu_6722UCharCharacterIterator13next32PostIncEv, .-_ZN6icu_6722UCharCharacterIterator13next32PostIncEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator10previous32Ev
	.type	_ZN6icu_6722UCharCharacterIterator10previous32Ev, @function
_ZN6icu_6722UCharCharacterIterator10previous32Ev:
.LFB1351:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movl	16(%rdi), %ecx
	movl	$65535, %eax
	cmpl	%ecx, %edx
	jle	.L126
	leal	-1(%rdx), %esi
	movq	24(%rdi), %r8
	movslq	%esi, %rax
	movl	%esi, 12(%rdi)
	leaq	(%rax,%rax), %r10
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %r9d
	andl	$-1024, %r9d
	cmpl	$56320, %r9d
	jne	.L126
	cmpl	%esi, %ecx
	jl	.L135
.L126:
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	movzwl	-2(%r8,%r10), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L126
	subl	$2, %edx
	sall	$10, %ecx
	movl	%edx, 12(%rdi)
	leal	-56613888(%rax,%rcx), %eax
	ret
	.cfi_endproc
.LFE1351:
	.size	_ZN6icu_6722UCharCharacterIterator10previous32Ev, .-_ZN6icu_6722UCharCharacterIterator10previous32Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator4moveEiNS_17CharacterIterator7EOriginE
	.type	_ZN6icu_6722UCharCharacterIterator4moveEiNS_17CharacterIterator7EOriginE, @function
_ZN6icu_6722UCharCharacterIterator4moveEiNS_17CharacterIterator7EOriginE:
.LFB1352:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %ecx
	cmpl	$1, %edx
	je	.L137
	cmpl	$2, %edx
	je	.L138
	testl	%edx, %edx
	je	.L139
	movl	12(%rdi), %eax
.L140:
	cmpl	%ecx, %eax
	jge	.L141
	movl	%ecx, 12(%rdi)
	movl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	movl	20(%rdi), %edx
	cmpl	%eax, %edx
	jge	.L136
	movl	%edx, 12(%rdi)
	movl	%edx, %eax
.L136:
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	leal	(%rsi,%rcx), %eax
	movl	%eax, 12(%rdi)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L138:
	movl	20(%rdi), %eax
	addl	%esi, %eax
	movl	%eax, 12(%rdi)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L137:
	movl	12(%rdi), %eax
	addl	%esi, %eax
	movl	%eax, 12(%rdi)
	jmp	.L140
	.cfi_endproc
.LFE1352:
	.size	_ZN6icu_6722UCharCharacterIterator4moveEiNS_17CharacterIterator7EOriginE, .-_ZN6icu_6722UCharCharacterIterator4moveEiNS_17CharacterIterator7EOriginE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator6move32EiNS_17CharacterIterator7EOriginE
	.type	_ZN6icu_6722UCharCharacterIterator6move32EiNS_17CharacterIterator7EOriginE, @function
_ZN6icu_6722UCharCharacterIterator6move32EiNS_17CharacterIterator7EOriginE:
.LFB1353:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L144
	cmpl	$2, %edx
	je	.L145
	testl	%edx, %edx
	je	.L242
	movl	12(%rdi), %eax
.L143:
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	movl	12(%rdi), %eax
	testl	%esi, %esi
	jle	.L155
	movl	20(%rdi), %r9d
	testl	%r9d, %r9d
	jns	.L156
	movq	24(%rdi), %r8
	.p2align 4,,10
	.p2align 3
.L161:
	movslq	%eax, %rdx
	movzwl	(%r8,%rdx,2), %edx
	cmpl	%eax, %r9d
	jg	.L158
	testw	%dx, %dx
	je	.L143
.L158:
	leal	1(%rax), %ecx
	andl	$64512, %edx
	movl	%ecx, 12(%rdi)
	cmpl	$55296, %edx
	jne	.L172
	cmpl	%ecx, %r9d
	jne	.L243
.L172:
	movl	%ecx, %eax
.L159:
	subl	$1, %esi
	jne	.L161
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	movl	16(%rdi), %eax
	movl	%eax, 12(%rdi)
	testl	%esi, %esi
	jle	.L143
	movl	20(%rdi), %r9d
	testl	%r9d, %r9d
	jns	.L148
	movq	24(%rdi), %r8
	.p2align 4,,10
	.p2align 3
.L153:
	movslq	%eax, %rdx
	movzwl	(%r8,%rdx,2), %edx
	cmpl	%eax, %r9d
	jg	.L150
	testw	%dx, %dx
	je	.L143
.L150:
	leal	1(%rax), %ecx
	andl	$64512, %edx
	movl	%ecx, 12(%rdi)
	cmpl	$55296, %edx
	jne	.L168
	cmpl	%ecx, %r9d
	jne	.L244
.L168:
	movl	%ecx, %eax
.L151:
	subl	$1, %esi
	jne	.L153
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	movl	20(%rdi), %eax
	movl	%eax, 12(%rdi)
	testl	%esi, %esi
	jns	.L143
	movl	16(%rdi), %r8d
	negl	%esi
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L165:
	subl	$1, %esi
	je	.L143
.L239:
	movl	%eax, %r10d
	cmpl	%eax, %r8d
	jge	.L143
	subl	$1, %eax
	movq	24(%rdi), %rcx
	movslq	%eax, %rdx
	movl	%eax, 12(%rdi)
	leaq	(%rdx,%rdx), %r9
	movzwl	(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L165
	cmpl	%eax, %r8d
	jge	.L165
	movzwl	-2(%rcx,%r9), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L165
	leal	-2(%r10), %eax
	movl	%eax, 12(%rdi)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L244:
	movslq	%ecx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L168
	addl	$2, %eax
	movl	%eax, 12(%rdi)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L243:
	movslq	%ecx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L172
	addl	$2, %eax
	movl	%eax, 12(%rdi)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L155:
	movl	%esi, %r8d
	negl	%r8d
	testl	%esi, %esi
	je	.L143
	movl	16(%rdi), %r9d
	.p2align 4,,10
	.p2align 3
.L164:
	cmpl	%eax, %r9d
	jge	.L143
	leal	-1(%rax), %edx
	movq	24(%rdi), %rsi
	movslq	%edx, %rcx
	movl	%edx, 12(%rdi)
	leaq	(%rcx,%rcx), %r10
	movzwl	(%rsi,%rcx,2), %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L176
	cmpl	%edx, %r9d
	jl	.L245
.L176:
	movl	%edx, %eax
.L163:
	subl	$1, %r8d
	jne	.L164
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	movzwl	-2(%rsi,%r10), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L176
	subl	$2, %eax
	movl	%eax, 12(%rdi)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L248:
	movq	24(%rdi), %r8
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	leaq	(%rdx,%rdx), %r10
	movzwl	(%r8,%rdx,2), %edx
	movl	%ecx, 12(%rdi)
	andl	$64512, %edx
	cmpl	$55296, %edx
	je	.L246
.L170:
	movl	%ecx, %eax
.L154:
	subl	$1, %esi
	je	.L247
.L148:
	cmpl	%eax, %r9d
	jg	.L248
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	movq	24(%rdi), %r8
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	leaq	(%rdx,%rdx), %r10
	movzwl	(%r8,%rdx,2), %edx
	movl	%ecx, 12(%rdi)
	andl	$64512, %edx
	cmpl	$55296, %edx
	je	.L249
.L174:
	movl	%ecx, %eax
.L162:
	subl	$1, %esi
	je	.L250
.L156:
	cmpl	%eax, %r9d
	jg	.L251
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	cmpl	%ecx, %r9d
	je	.L170
	movzwl	2(%r8,%r10), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L170
	addl	$2, %eax
	movl	%eax, 12(%rdi)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L249:
	cmpl	%ecx, %r9d
	je	.L174
	movzwl	2(%r8,%r10), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L174
	addl	$2, %eax
	movl	%eax, 12(%rdi)
	jmp	.L162
.L250:
	ret
.L247:
	ret
	.cfi_endproc
.LFE1353:
	.size	_ZN6icu_6722UCharCharacterIterator6move32EiNS_17CharacterIterator7EOriginE, .-_ZN6icu_6722UCharCharacterIterator6move32EiNS_17CharacterIterator7EOriginE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIteratorD2Ev
	.type	_ZN6icu_6722UCharCharacterIteratorD2Ev, @function
_ZN6icu_6722UCharCharacterIteratorD2Ev:
.LFB1328:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6722UCharCharacterIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717CharacterIteratorD2Ev@PLT
	.cfi_endproc
.LFE1328:
	.size	_ZN6icu_6722UCharCharacterIteratorD2Ev, .-_ZN6icu_6722UCharCharacterIteratorD2Ev
	.globl	_ZN6icu_6722UCharCharacterIteratorD1Ev
	.set	_ZN6icu_6722UCharCharacterIteratorD1Ev,_ZN6icu_6722UCharCharacterIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIteratorD0Ev
	.type	_ZN6icu_6722UCharCharacterIteratorD0Ev, @function
_ZN6icu_6722UCharCharacterIteratorD0Ev:
.LFB1330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722UCharCharacterIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CharacterIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE1330:
	.size	_ZN6icu_6722UCharCharacterIteratorD0Ev, .-_ZN6icu_6722UCharCharacterIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UCharCharacterIterator8hashCodeEv
	.type	_ZNK6icu_6722UCharCharacterIterator8hashCodeEv, @function
_ZNK6icu_6722UCharCharacterIterator8hashCodeEv:
.LFB1332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %esi
	movq	24(%rdi), %rdi
	call	ustr_hashUCharsN_67@PLT
	movl	%eax, %r8d
	movl	12(%rbx), %eax
	xorl	16(%rbx), %eax
	xorl	20(%rbx), %eax
	addq	$8, %rsp
	xorl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1332:
	.size	_ZNK6icu_6722UCharCharacterIterator8hashCodeEv, .-_ZNK6icu_6722UCharCharacterIterator8hashCodeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UCharCharacterIterator5cloneEv
	.type	_ZNK6icu_6722UCharCharacterIterator5cloneEv, @function
_ZNK6icu_6722UCharCharacterIterator5cloneEv:
.LFB1333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L257
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6717CharacterIteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6722UCharCharacterIteratorE(%rip), %rax
	movq	%rax, (%r12)
	movq	24(%rbx), %rax
	movq	%rax, 24(%r12)
.L257:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1333:
	.size	_ZNK6icu_6722UCharCharacterIterator5cloneEv, .-_ZNK6icu_6722UCharCharacterIterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722UCharCharacterIteratoreqERKNS_24ForwardCharacterIteratorE
	.type	_ZNK6icu_6722UCharCharacterIteratoreqERKNS_24ForwardCharacterIteratorE, @function
_ZNK6icu_6722UCharCharacterIteratoreqERKNS_24ForwardCharacterIteratorE:
.LFB1331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L267
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L265
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L263
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L263
.L265:
	movq	24(%rbx), %rax
	xorl	%r13d, %r13d
	cmpq	%rax, 24(%r12)
	je	.L273
.L263:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movq	8(%rbx), %rax
	cmpq	%rax, 8(%r12)
	jne	.L263
	movq	16(%rbx), %rax
	cmpq	%rax, 16(%r12)
	sete	%r13b
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L267:
	addq	$8, %rsp
	movl	$1, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1331:
	.size	_ZNK6icu_6722UCharCharacterIteratoreqERKNS_24ForwardCharacterIteratorE, .-_ZNK6icu_6722UCharCharacterIteratoreqERKNS_24ForwardCharacterIteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator16getStaticClassIDEv
	.type	_ZN6icu_6722UCharCharacterIterator16getStaticClassIDEv, @function
_ZN6icu_6722UCharCharacterIterator16getStaticClassIDEv:
.LFB1309:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722UCharCharacterIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE1309:
	.size	_ZN6icu_6722UCharCharacterIterator16getStaticClassIDEv, .-_ZN6icu_6722UCharCharacterIterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIteratorC2Ev
	.type	_ZN6icu_6722UCharCharacterIteratorC2Ev, @function
_ZN6icu_6722UCharCharacterIteratorC2Ev:
.LFB1312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6717CharacterIteratorC2Ev@PLT
	leaq	16+_ZTVN6icu_6722UCharCharacterIteratorE(%rip), %rax
	movq	$0, 24(%rbx)
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1312:
	.size	_ZN6icu_6722UCharCharacterIteratorC2Ev, .-_ZN6icu_6722UCharCharacterIteratorC2Ev
	.globl	_ZN6icu_6722UCharCharacterIteratorC1Ev
	.set	_ZN6icu_6722UCharCharacterIteratorC1Ev,_ZN6icu_6722UCharCharacterIteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEi
	.type	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEi, @function
_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEi:
.LFB1315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rsi), %rdi
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.L278
	movl	%edx, %esi
	testl	%edx, %edx
	js	.L281
.L278:
	movq	%rbx, %rdi
	call	_ZN6icu_6717CharacterIteratorC2Ei@PLT
	leaq	16+_ZTVN6icu_6722UCharCharacterIteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	(%r12), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	call	u_strlen_67@PLT
	movl	%eax, %esi
	jmp	.L278
	.cfi_endproc
.LFE1315:
	.size	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEi, .-_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEi
	.globl	_ZN6icu_6722UCharCharacterIteratorC1ENS_14ConstChar16PtrEi
	.set	_ZN6icu_6722UCharCharacterIteratorC1ENS_14ConstChar16PtrEi,_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEii
	.type	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEii, @function
_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEii:
.LFB1318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	%edx, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	%ecx, %edx
	subq	$16, %rsp
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L284
	testl	%esi, %esi
	js	.L286
.L283:
	movq	%rbx, %rdi
	call	_ZN6icu_6717CharacterIteratorC2Eii@PLT
	leaq	16+_ZTVN6icu_6722UCharCharacterIteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	(%r12), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	xorl	%esi, %esi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L286:
	movl	%ecx, -20(%rbp)
	call	u_strlen_67@PLT
	movl	-20(%rbp), %edx
	movl	%eax, %esi
	jmp	.L283
	.cfi_endproc
.LFE1318:
	.size	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEii, .-_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEii
	.globl	_ZN6icu_6722UCharCharacterIteratorC1ENS_14ConstChar16PtrEii
	.set	_ZN6icu_6722UCharCharacterIteratorC1ENS_14ConstChar16PtrEii,_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEiiii
	.type	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEiiii, @function
_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEiiii:
.LFB1321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	movl	%edx, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	%ecx, %edx
	movl	%r8d, %ecx
	movl	%r9d, %r8d
	subq	$16, %rsp
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L289
	testl	%esi, %esi
	js	.L291
.L288:
	movq	%rbx, %rdi
	call	_ZN6icu_6717CharacterIteratorC2Eiiii@PLT
	leaq	16+_ZTVN6icu_6722UCharCharacterIteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	(%r12), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	xorl	%esi, %esi
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L291:
	movl	%ecx, -24(%rbp)
	movl	%edx, -20(%rbp)
	movl	%r9d, -28(%rbp)
	call	u_strlen_67@PLT
	movl	-20(%rbp), %edx
	movl	-24(%rbp), %ecx
	movl	-28(%rbp), %r8d
	movl	%eax, %esi
	jmp	.L288
	.cfi_endproc
.LFE1321:
	.size	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEiiii, .-_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEiiii
	.globl	_ZN6icu_6722UCharCharacterIteratorC1ENS_14ConstChar16PtrEiiii
	.set	_ZN6icu_6722UCharCharacterIteratorC1ENS_14ConstChar16PtrEiiii,_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEiiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIteratorC2ERKS0_
	.type	_ZN6icu_6722UCharCharacterIteratorC2ERKS0_, @function
_ZN6icu_6722UCharCharacterIteratorC2ERKS0_:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717CharacterIteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6722UCharCharacterIteratorE(%rip), %rax
	movq	%rax, (%rbx)
	movq	24(%r12), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1324:
	.size	_ZN6icu_6722UCharCharacterIteratorC2ERKS0_, .-_ZN6icu_6722UCharCharacterIteratorC2ERKS0_
	.globl	_ZN6icu_6722UCharCharacterIteratorC1ERKS0_
	.set	_ZN6icu_6722UCharCharacterIteratorC1ERKS0_,_ZN6icu_6722UCharCharacterIteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIteratoraSERKS0_
	.type	_ZN6icu_6722UCharCharacterIteratoraSERKS0_, @function
_ZN6icu_6722UCharCharacterIteratoraSERKS0_:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN6icu_6717CharacterIteratoraSERKS0_@PLT
	movq	24(%rbx), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1326:
	.size	_ZN6icu_6722UCharCharacterIteratoraSERKS0_, .-_ZN6icu_6722UCharCharacterIteratoraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator7setTextENS_14ConstChar16PtrEi
	.type	_ZN6icu_6722UCharCharacterIterator7setTextENS_14ConstChar16PtrEi, @function
_ZN6icu_6722UCharCharacterIterator7setTextENS_14ConstChar16PtrEi:
.LFB1354:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rcx
	xorl	%eax, %eax
	movq	%rcx, 24(%rdi)
	testq	%rcx, %rcx
	je	.L297
	testl	%edx, %edx
	cmovns	%edx, %eax
.L297:
	movl	%eax, 8(%rdi)
	movl	%eax, 20(%rdi)
	movq	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE1354:
	.size	_ZN6icu_6722UCharCharacterIterator7setTextENS_14ConstChar16PtrEi, .-_ZN6icu_6722UCharCharacterIterator7setTextENS_14ConstChar16PtrEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722UCharCharacterIterator7getTextERNS_13UnicodeStringE
	.type	_ZN6icu_6722UCharCharacterIterator7getTextERNS_13UnicodeStringE, @function
_ZN6icu_6722UCharCharacterIterator7getTextERNS_13UnicodeStringE:
.LFB1355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	subq	$80, %rsp
	movl	8(%rdi), %edx
	movq	24(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L303:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1355:
	.size	_ZN6icu_6722UCharCharacterIterator7getTextERNS_13UnicodeStringE, .-_ZN6icu_6722UCharCharacterIterator7getTextERNS_13UnicodeStringE
	.weak	_ZTSN6icu_6722UCharCharacterIteratorE
	.section	.rodata._ZTSN6icu_6722UCharCharacterIteratorE,"aG",@progbits,_ZTSN6icu_6722UCharCharacterIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6722UCharCharacterIteratorE, @object
	.size	_ZTSN6icu_6722UCharCharacterIteratorE, 34
_ZTSN6icu_6722UCharCharacterIteratorE:
	.string	"N6icu_6722UCharCharacterIteratorE"
	.weak	_ZTIN6icu_6722UCharCharacterIteratorE
	.section	.data.rel.ro._ZTIN6icu_6722UCharCharacterIteratorE,"awG",@progbits,_ZTIN6icu_6722UCharCharacterIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6722UCharCharacterIteratorE, @object
	.size	_ZTIN6icu_6722UCharCharacterIteratorE, 24
_ZTIN6icu_6722UCharCharacterIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722UCharCharacterIteratorE
	.quad	_ZTIN6icu_6717CharacterIteratorE
	.weak	_ZTVN6icu_6722UCharCharacterIteratorE
	.section	.data.rel.ro.local._ZTVN6icu_6722UCharCharacterIteratorE,"awG",@progbits,_ZTVN6icu_6722UCharCharacterIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6722UCharCharacterIteratorE, @object
	.size	_ZTVN6icu_6722UCharCharacterIteratorE, 232
_ZTVN6icu_6722UCharCharacterIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6722UCharCharacterIteratorE
	.quad	_ZN6icu_6722UCharCharacterIteratorD1Ev
	.quad	_ZN6icu_6722UCharCharacterIteratorD0Ev
	.quad	_ZNK6icu_6722UCharCharacterIterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6722UCharCharacterIteratoreqERKNS_24ForwardCharacterIteratorE
	.quad	_ZNK6icu_6722UCharCharacterIterator8hashCodeEv
	.quad	_ZN6icu_6722UCharCharacterIterator11nextPostIncEv
	.quad	_ZN6icu_6722UCharCharacterIterator13next32PostIncEv
	.quad	_ZN6icu_6722UCharCharacterIterator7hasNextEv
	.quad	_ZNK6icu_6722UCharCharacterIterator5cloneEv
	.quad	_ZN6icu_6722UCharCharacterIterator5firstEv
	.quad	_ZN6icu_6722UCharCharacterIterator12firstPostIncEv
	.quad	_ZN6icu_6722UCharCharacterIterator7first32Ev
	.quad	_ZN6icu_6722UCharCharacterIterator14first32PostIncEv
	.quad	_ZN6icu_6722UCharCharacterIterator4lastEv
	.quad	_ZN6icu_6722UCharCharacterIterator6last32Ev
	.quad	_ZN6icu_6722UCharCharacterIterator8setIndexEi
	.quad	_ZN6icu_6722UCharCharacterIterator10setIndex32Ei
	.quad	_ZNK6icu_6722UCharCharacterIterator7currentEv
	.quad	_ZNK6icu_6722UCharCharacterIterator9current32Ev
	.quad	_ZN6icu_6722UCharCharacterIterator4nextEv
	.quad	_ZN6icu_6722UCharCharacterIterator6next32Ev
	.quad	_ZN6icu_6722UCharCharacterIterator8previousEv
	.quad	_ZN6icu_6722UCharCharacterIterator10previous32Ev
	.quad	_ZN6icu_6722UCharCharacterIterator11hasPreviousEv
	.quad	_ZN6icu_6722UCharCharacterIterator4moveEiNS_17CharacterIterator7EOriginE
	.quad	_ZN6icu_6722UCharCharacterIterator6move32EiNS_17CharacterIterator7EOriginE
	.quad	_ZN6icu_6722UCharCharacterIterator7getTextERNS_13UnicodeStringE
	.local	_ZZN6icu_6722UCharCharacterIterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6722UCharCharacterIterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
