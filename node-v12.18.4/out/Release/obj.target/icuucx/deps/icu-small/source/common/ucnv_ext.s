	.file	"ucnv_ext.cpp"
	.text
	.p2align 4
	.type	_ZL18ucnv_extMatchFromUPKiiPKDsiS2_iPjaa, @function
_ZL18ucnv_extMatchFromUPKiiPKDsiS2_iPjaa:
.LFB2121:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -48(%rbp)
	movq	%r8, -64(%rbp)
	movl	24(%rbp), %eax
	movl	32(%rbp), %ebx
	movl	%r9d, -52(%rbp)
	movl	%eax, -68(%rbp)
	movl	%ebx, -72(%rbp)
	movb	%al, -53(%rbp)
	testq	%rdi, %rdi
	je	.L49
	movl	%esi, %eax
	xorl	%r15d, %r15d
	sarl	$10, %eax
	cmpl	%eax, 44(%rdi)
	jle	.L1
	movslq	40(%rdi), %rdx
	cltq
	addq	%rdi, %rdx
	movzwl	(%rdx,%rax,2), %r8d
	movl	%esi, %eax
	sarl	$4, %eax
	andl	$63, %eax
	addl	%r8d, %eax
	cltq
	movzwl	(%rdx,%rax,2), %edx
	movl	%esi, %eax
	andl	$15, %eax
	leal	(%rax,%rdx,4), %eax
	movslq	52(%rdi), %rdx
	cltq
	leaq	(%rdi,%rax,2), %rax
	movzwl	(%rax,%rdx), %edx
	movslq	60(%rdi), %rax
	leaq	(%rdi,%rdx,4), %rdx
	movl	(%rdx,%rax), %r14d
	testl	%r14d, %r14d
	je	.L1
	cmpl	$2031615, %r14d
	ja	.L3
	movslq	20(%rdi), %r11
	movslq	24(%rdi), %r10
	movslq	%r14d, %rax
	movl	%r15d, -80(%rbp)
	leal	-57344(%rsi), %ebx
	cmpl	$6399, %ebx
	movl	%ebx, -84(%rbp)
	leal	-983040(%rsi), %ebx
	movq	%r10, %r15
	setbe	%dl
	cmpl	$131071, %ebx
	movl	%ebx, -88(%rbp)
	setbe	%sil
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	orl	%esi, %edx
	movl	%r14d, -76(%rbp)
	movq	%r11, %r14
	movb	%dl, -54(%rbp)
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	(%r15,%rax,4), %r11
	leaq	(%r14,%rax,2), %r9
	addq	%rdi, %r11
	addq	%rdi, %r9
	movl	(%r11), %eax
	movzwl	(%r9), %edx
	leaq	2(%r9), %r10
	testl	%eax, %eax
	je	.L4
	testl	$-1073741824, %eax
	jne	.L5
	cmpb	$0, -53(%rbp)
	je	.L51
.L5:
	testl	$536870912, %eax
	jne	.L4
	leal	2(%r12,%r13), %ebx
	movl	%eax, -76(%rbp)
	movl	%ebx, -80(%rbp)
.L4:
	cmpl	%ecx, %r12d
	jge	.L6
	movq	-48(%rbp), %rbx
	movslq	%r12d, %rax
	addl	$1, %r12d
	movzwl	(%rbx,%rax,2), %r8d
.L7:
	xorl	%eax, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L53:
	cmpl	$4, %esi
	jle	.L52
	leal	(%rdx,%rax), %esi
	sarl	%esi
	movslq	%esi, %rbx
	cmpw	(%r10,%rbx,2), %r8w
	cmovnb	%esi, %eax
	cmovb	%esi, %edx
.L15:
	movl	%edx, %esi
	subl	%eax, %esi
	cmpl	$1, %esi
	jg	.L53
.L10:
	cmpl	%eax, %edx
	jle	.L47
	movslq	%eax, %rsi
	movzwl	(%r10,%rsi,2), %r9d
.L13:
	cmpw	%r9w, %r8w
	jne	.L47
	movslq	4(%r11,%rsi,4), %rax
	movl	%eax, %ebx
	shrl	$24, %ebx
	je	.L18
	movl	-76(%rbp), %r14d
	movl	-80(%rbp), %r15d
	testl	$-1073741824, %eax
	jne	.L19
	cmpb	$0, -68(%rbp)
	jne	.L19
	cmpl	$6399, -84(%rbp)
	jbe	.L19
	cmpl	$131071, -88(%rbp)
	jbe	.L19
	.p2align 4,,10
	.p2align 3
.L9:
	testl	%r15d, %r15d
	jne	.L21
.L1:
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	testl	$-1073741824, %r14d
	jne	.L22
	cmpb	$0, -68(%rbp)
	je	.L54
.L22:
	xorl	%r15d, %r15d
	testl	$536870912, %r14d
	jne	.L1
	movl	$2, %r15d
.L21:
	cmpl	$-2147483647, %r14d
	je	.L55
.L24:
	movq	16(%rbp), %rax
	movl	%r14d, (%rax)
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	testl	$536870912, %eax
	jne	.L9
	leal	2(%r12,%r13), %r15d
	movl	%eax, %r14d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	-52(%rbp), %r13d
	jge	.L8
	movq	-64(%rbp), %rbx
	movslq	%r13d, %rax
	addl	$1, %r13d
	movzwl	(%rbx,%rax,2), %r8d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L52:
	movslq	%eax, %rsi
	leaq	(%rsi,%rsi), %rbx
	cmpw	2(%r9,%rsi,2), %r8w
	jbe	.L10
	leal	1(%rax), %esi
	cmpl	%esi, %edx
	jle	.L12
	movzwl	2(%r10,%rbx), %r9d
	movslq	%esi, %rsi
	cmpw	%r9w, %r8w
	jbe	.L13
.L12:
	leal	2(%rax), %esi
	cmpl	%esi, %edx
	jle	.L14
	movzwl	4(%r10,%rbx), %r9d
	movslq	%esi, %rsi
	cmpw	%r9w, %r8w
	jbe	.L13
.L14:
	addl	$3, %eax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L51:
	cmpb	$0, -54(%rbp)
	je	.L4
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%r15d, %r15d
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	-76(%rbp), %r14d
	movl	-80(%rbp), %r15d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L54:
	leal	-57344(%rsi), %eax
	cmpl	$6399, %eax
	jbe	.L23
	subl	$983040, %esi
	cmpl	$131071, %esi
	ja	.L1
.L23:
	testl	$536870912, %r14d
	jne	.L49
	movl	$2, %r15d
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$1, %r15d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	cmpb	$0, -72(%rbp)
	movl	-76(%rbp), %r14d
	movl	-80(%rbp), %r15d
	jne	.L9
	addl	%r13d, %r12d
	cmpl	$19, %r12d
	jg	.L9
	movl	$-2, %r15d
	subl	%r12d, %r15d
	jmp	.L1
	.cfi_endproc
.LFE2121:
	.size	_ZL18ucnv_extMatchFromUPKiiPKDsiS2_iPjaa, .-_ZL18ucnv_extMatchFromUPKiiPKDsiS2_iPjaa
	.p2align 4
	.type	_ZL27ucnv_extGetUnicodeSetStringPK20UConverterSharedDataPKiPK9USetAdder20UConverterUnicodeSetiiPDsiiP10UErrorCode, @function
_ZL27ucnv_extGetUnicodeSetStringPK20UConverterSharedDataPKiPK9USetAdder20UConverterUnicodeSetiiPDsiiP10UErrorCode:
.LFB2127:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movslq	32(%rbp), %rax
	movslq	20(%rsi), %rsi
	movl	%r9d, -52(%rbp)
	movq	%rdi, -64(%rbp)
	movl	24(%rbp), %edx
	leaq	(%rsi,%rax,2), %r14
	movslq	24(%r12), %rsi
	addq	%r12, %r14
	leaq	(%rsi,%rax,4), %rbx
	movzwl	(%r14), %r9d
	addq	%r12, %rbx
	movl	(%rbx), %eax
	testl	%ecx, %ecx
	jne	.L57
	movl	%eax, %esi
	andl	$-1610612736, %esi
	cmpl	$-2147483648, %esi
	je	.L59
	testl	%r9d, %r9d
	je	.L56
	movq	16(%rbp), %rdi
	movslq	%edx, %rax
	leaq	(%rdi,%rax,2), %rax
.L65:
	leal	1(%rdx), %edi
	leal	-1(%r9), %ecx
	movq	%r12, -72(%rbp)
	addq	$4, %rbx
	movl	%edi, -56(%rbp)
	leaq	2(%r14), %r13
	movq	%rax, %r12
	leaq	4(%r14,%rcx,2), %r14
.L74:
	movzwl	0(%r13), %edx
	movw	%dx, (%r12)
	movl	(%rbx), %edx
	testl	%edx, %edx
	je	.L72
	movl	%edx, %ecx
	shrl	$24, %ecx
	je	.L101
	andl	$-1610612736, %edx
	cmpl	$-2147483648, %edx
	je	.L102
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$2, %r13
	addq	$4, %rbx
	cmpq	%r14, %r13
	jne	.L74
.L56:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	testl	$536870912, %eax
	jne	.L60
.L59:
	shrl	$24, %eax
	andl	$31, %eax
	cmpl	%eax, %r8d
	jg	.L61
	xorl	%eax, %eax
	cmpl	$65535, -52(%rbp)
	movq	(%r15), %rdi
	movl	%r9d, -76(%rbp)
	seta	%al
	addl	$1, %eax
	cmpl	%edx, %eax
	je	.L103
	movl	%r8d, -72(%rbp)
	movq	16(%rbp), %rsi
	movl	%ecx, -56(%rbp)
	movl	%edx, 24(%rbp)
	call	*24(%r15)
	movl	-76(%rbp), %r9d
	movl	-72(%rbp), %r8d
	movl	-56(%rbp), %ecx
	movl	24(%rbp), %edx
.L61:
	testl	%r9d, %r9d
	je	.L56
	movq	16(%rbp), %rdi
	movslq	%edx, %rax
	leaq	(%rdi,%rax,2), %rax
	testl	%ecx, %ecx
	je	.L65
.L76:
	leal	1(%rdx), %edi
	movl	%ecx, -76(%rbp)
	leal	-1(%r9), %esi
	addq	$4, %rbx
	movl	%edi, -56(%rbp)
	leaq	2(%r14), %r13
	leaq	4(%r14,%rsi,2), %r14
	movq	%r12, -72(%rbp)
	movq	%rax, %r12
.L71:
	movzwl	0(%r13), %edx
	movw	%dx, (%r12)
	movl	(%rbx), %edx
	testl	%edx, %edx
	je	.L66
	movl	%edx, %esi
	shrl	$24, %esi
	je	.L104
	andl	$536870912, %edx
	jne	.L66
	andl	$31, %esi
	cmpl	%esi, %r8d
	jg	.L66
	movl	-56(%rbp), %edx
	movq	16(%rbp), %rsi
	movl	%r8d, -80(%rbp)
	movq	(%r15), %rdi
	call	*24(%r15)
	movl	-80(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L66:
	addq	$2, %r13
	addq	$4, %rbx
	cmpq	%r14, %r13
	jne	.L71
.L105:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	andl	$31, %ecx
	cmpl	%ecx, %r8d
	jg	.L72
	addq	$2, %r13
	movl	%r8d, -76(%rbp)
	movl	-56(%rbp), %edx
	addq	$4, %rbx
	movq	16(%rbp), %rsi
	movq	(%r15), %rdi
	call	*24(%r15)
	movl	-76(%rbp), %r8d
	cmpq	%r14, %r13
	jne	.L74
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L104:
	movl	-56(%rbp), %eax
	pushq	40(%rbp)
	addq	$2, %r13
	addq	$4, %rbx
	movl	-52(%rbp), %r9d
	pushq	%rdx
	movq	%r15, %rdx
	movl	-76(%rbp), %ecx
	movq	-72(%rbp), %rsi
	pushq	%rax
	pushq	16(%rbp)
	movq	-64(%rbp), %rdi
	movl	%r8d, -80(%rbp)
	call	_ZL27ucnv_extGetUnicodeSetStringPK20UConverterSharedDataPKiPK9USetAdder20UConverterUnicodeSetiiPDsiiP10UErrorCode
	movl	-80(%rbp), %r8d
	addq	$32, %rsp
	cmpq	%r14, %r13
	jne	.L71
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L101:
	movl	-56(%rbp), %eax
	pushq	40(%rbp)
	xorl	%ecx, %ecx
	addq	$2, %r13
	movl	-52(%rbp), %r9d
	pushq	%rdx
	movq	%r15, %rdx
	addq	$4, %rbx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	pushq	%rax
	pushq	16(%rbp)
	movl	%r8d, -76(%rbp)
	call	_ZL27ucnv_extGetUnicodeSetStringPK20UConverterSharedDataPKiPK9USetAdder20UConverterUnicodeSetiiPDsiiP10UErrorCode
	movl	-76(%rbp), %r8d
	addq	$32, %rsp
	cmpq	%r14, %r13
	jne	.L74
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L60:
	testl	%r9d, %r9d
	je	.L56
	movq	16(%rbp), %rdi
	movslq	%edx, %rax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L103:
	movl	%edx, 24(%rbp)
	movl	-52(%rbp), %esi
	movl	%r8d, -72(%rbp)
	movl	%ecx, -56(%rbp)
	call	*8(%r15)
	movl	-56(%rbp), %ecx
	movl	-72(%rbp), %r8d
	movl	24(%rbp), %edx
	movl	-76(%rbp), %r9d
	jmp	.L61
	.cfi_endproc
.LFE2127:
	.size	_ZL27ucnv_extGetUnicodeSetStringPK20UConverterSharedDataPKiPK9USetAdder20UConverterUnicodeSetiiPDsiiP10UErrorCode, .-_ZL27ucnv_extGetUnicodeSetStringPK20UConverterSharedDataPKiPK9USetAdder20UConverterUnicodeSetiiPDsiiP10UErrorCode
	.p2align 4
	.type	_ZL16ucnv_extMatchToUPKiaPKciS2_iPjaa.part.0, @function
_ZL16ucnv_extMatchToUPKiaPKciS2_iPjaa.part.0:
.LFB2775:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -72(%rbp)
	movq	%rdx, -56(%rbp)
	movzbl	24(%rbp), %eax
	movslq	4(%rdi), %r13
	movq	%r8, -64(%rbp)
	movl	%r9d, -44(%rbp)
	movb	%sil, -45(%rbp)
	movb	%al, -46(%rbp)
	testb	%sil, %sil
	jne	.L107
	xorl	%r12d, %r12d
	cmpl	$1, %ecx
	jg	.L106
	je	.L126
	testl	%r9d, %r9d
	movl	$1, %eax
	movb	$1, -46(%rbp)
	cmovle	%r9d, %eax
	movl	%eax, -44(%rbp)
.L107:
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	xorl	%r14d, %r14d
	movl	%ecx, %r10d
	.p2align 4,,10
	.p2align 3
.L124:
	cltq
	leaq	0(%r13,%rax,4), %rax
	leaq	4(%rdi,%rax), %rcx
	movl	(%rdi,%rax), %eax
	movl	%eax, %edx
	shrl	$24, %edx
	andl	$16777215, %eax
	je	.L109
	cmpb	$0, -45(%rbp)
	leal	(%r11,%rbx), %esi
	js	.L127
	sete	%r9b
	cmpl	$1, %esi
	sete	%r8b
	cmpb	%r8b, %r9b
	cmove	%esi, %r12d
	cmove	%eax, %r14d
.L109:
	cmpl	%r11d, %r10d
	jle	.L110
	movq	-56(%rbp), %rsi
	movslq	%r11d, %rax
	addl	$1, %r11d
	movzbl	(%rsi,%rax), %r8d
.L111:
	movl	%edx, %esi
	movl	(%rcx), %eax
	movzbl	-1(%rcx,%rsi,4), %esi
	shrl	$24, %eax
	cmpl	%esi, %r8d
	jg	.L113
	cmpl	%eax, %r8d
	jl	.L113
	subl	%eax, %esi
	addl	$1, %esi
	cmpl	%esi, %edx
	je	.L146
	movl	%r8d, %eax
	xorl	%esi, %esi
	sall	$24, %eax
	movl	%eax, %r9d
	movl	%eax, -68(%rbp)
	orl	$16777215, %r9d
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L148:
	cmpl	$4, %eax
	jle	.L147
	leal	(%rdx,%rsi), %r15d
	movl	%r15d, %eax
	shrl	$31, %eax
	addl	%r15d, %eax
	sarl	%eax
	movslq	%eax, %r15
	cmpl	(%rcx,%r15,4), %r9d
	cmovnb	%eax, %esi
	cmovb	%eax, %edx
.L121:
	movl	%edx, %eax
	subl	%esi, %eax
	cmpl	$1, %eax
	jg	.L148
.L116:
	cmpl	%esi, %edx
	jle	.L113
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %eax
.L119:
	movl	%eax, %edx
	shrl	$24, %edx
	cmpl	%edx, %r8d
	je	.L149
	.p2align 4,,10
	.p2align 3
.L113:
	testl	%r12d, %r12d
	je	.L106
	movq	16(%rbp), %rdx
	movl	%r14d, %eax
	andl	$-8388609, %eax
	movl	%eax, (%rdx)
.L106:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	cmpl	-44(%rbp), %ebx
	jge	.L112
	movq	-64(%rbp), %rsi
	movslq	%ebx, %rax
	addl	$1, %ebx
	movzbl	(%rsi,%rax), %r8d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L127:
	movl	%esi, %r12d
	movl	%eax, %r14d
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L147:
	movslq	%esi, %rax
	movl	-68(%rbp), %r15d
	leaq	0(,%rax,4), %r9
	cmpl	(%rcx,%rax,4), %r15d
	jbe	.L116
	leal	1(%rsi), %eax
	cmpl	%eax, %edx
	jle	.L118
	movl	4(%rcx,%r9), %eax
	cmpl	%eax, %r15d
	jbe	.L119
.L118:
	leal	2(%rsi), %eax
	cmpl	%eax, %edx
	jle	.L120
	movl	8(%rcx,%r9), %eax
	cmpl	%eax, -68(%rbp)
	jbe	.L119
.L120:
	addl	$3, %esi
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L146:
	subl	%eax, %r8d
	movslq	%r8d, %r8
	movl	(%rcx,%r8,4), %eax
	andl	$16777215, %eax
.L115:
	testl	%eax, %eax
	je	.L113
	cmpl	$2031615, %eax
	jbe	.L124
	addl	%ebx, %r11d
	cmpb	$0, -72(%rbp)
	js	.L129
	sete	%cl
	cmpl	$1, %r11d
	sete	%dl
	cmpb	%dl, %cl
	cmove	%r11d, %r12d
	cmove	%eax, %r14d
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L149:
	andl	$16777215, %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L129:
	movl	%r11d, %r12d
	movl	%eax, %r14d
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L126:
	movb	$1, -46(%rbp)
	movl	$0, -44(%rbp)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L112:
	cmpb	$0, -46(%rbp)
	jne	.L113
	addl	%ebx, %r11d
	cmpl	$31, %r11d
	jg	.L113
	negl	%r11d
	movl	%r11d, %r12d
	jmp	.L106
	.cfi_endproc
.LFE2775:
	.size	_ZL16ucnv_extMatchToUPKiaPKciS2_iPjaa.part.0, .-_ZL16ucnv_extMatchToUPKiaPKciS2_iPjaa.part.0
	.p2align 4
	.globl	ucnv_extInitialMatchToU_67
	.type	ucnv_extInitialMatchToU_67, @function
ucnv_extInitialMatchToU_67:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	48(%rbp), %rsi
	movq	48(%r12), %rdx
	movq	16(%rbp), %r10
	movq	24(%rbp), %r11
	movzbl	252(%rdx), %edx
	movl	40(%rbp), %ecx
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movl	$0, -60(%rbp)
	cmpb	$12, %dl
	je	.L181
	xorl	%esi, %esi
	cmpb	$-37, %dl
	sete	%sil
	leal	-1(%rsi,%rsi), %esi
.L152:
	testq	%rdi, %rdi
	je	.L155
	movq	%r10, -72(%rbp)
	movl	8(%rdi), %r10d
	movq	%r11, -80(%rbp)
	testl	%r10d, %r10d
	jle	.L155
	movsbl	%cl, %ecx
	movq	0(%r13), %r8
	movl	%eax, %r9d
	leaq	65(%r12), %r15
	pushq	%rcx
	leaq	-60(%rbp), %rcx
	movq	%r15, %rdx
	pushq	%rcx
	subl	%r8d, %r9d
	movl	%ebx, %ecx
	call	_ZL16ucnv_extMatchToUPKiaPKciS2_iPjaa.part.0
	popq	%r8
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	movq	-80(%rbp), %r11
	popq	%r9
	jg	.L182
	je	.L155
	movl	%eax, %esi
	movb	%bl, 283(%r12)
	negl	%esi
	testl	%ebx, %ebx
	jle	.L158
	leal	-1(%rbx), %ecx
	cmpl	$14, %ecx
	jbe	.L169
	movl	%ebx, %ecx
	movdqu	65(%r12), %xmm0
	andl	$-16, %ecx
	movups	%xmm0, 250(%r12)
	movl	%ecx, %edi
	addq	%rdi, %r15
	cmpl	%ecx, %ebx
	je	.L160
.L159:
	movzbl	(%r15), %r8d
	movslq	%ecx, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	1(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	1(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	2(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	2(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	3(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	3(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	4(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	4(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	5(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	5(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	6(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	6(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	7(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	7(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	8(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	8(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	9(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	9(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	10(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	10(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	11(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	11(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	12(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	12(%r15), %r8d
	movslq	%edi, %rdi
	movb	%r8b, 250(%r12,%rdi)
	leal	13(%rcx), %edi
	cmpl	%edi, %ebx
	jle	.L160
	movzbl	13(%r15), %r8d
	movslq	%edi, %rdi
	addl	$14, %ecx
	movb	%r8b, 250(%r12,%rdi)
	cmpl	%ecx, %ebx
	jle	.L160
	movzbl	14(%r15), %edx
	movslq	%ecx, %rcx
	movb	%dl, 250(%r12,%rcx)
	.p2align 4,,10
	.p2align 3
.L160:
	movq	0(%r13), %r8
	cmpl	%ebx, %esi
	jle	.L161
.L166:
	movslq	%ebx, %rdx
	notl	%eax
	leaq	266(%r12,%rdx), %rcx
	subl	%ebx, %eax
	leaq	250(%r12,%rdx), %rdi
	cmpq	%rcx, %r8
	leaq	16(%r8), %rcx
	setnb	%r9b
	cmpq	%rcx, %rdi
	setnb	%cl
	orb	%cl, %r9b
	je	.L170
	cmpl	%ebx, %esi
	setg	%r9b
	cmpl	$14, %eax
	seta	%cl
	testb	%cl, %r9b
	je	.L170
	movl	%esi, %ecx
	movl	$1, %edx
	movdqu	(%r8), %xmm1
	subl	%ebx, %ecx
	cmpl	%ebx, %esi
	cmovle	%edx, %ecx
	movups	%xmm1, (%rdi)
	movl	%ecx, %edi
	andl	$-16, %edi
	movl	%edi, %r9d
	leal	(%rdi,%rbx), %edx
	addq	%r8, %r9
	cmpl	%edi, %ecx
	je	.L165
	movzbl	(%r9), %edi
	movslq	%edx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	1(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	2(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	2(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	3(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	3(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	4(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	4(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	5(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	5(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	6(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	6(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	7(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	7(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	8(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	8(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	9(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	9(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	10(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	10(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	11(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	11(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	12(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	12(%r9), %edi
	movslq	%ecx, %rcx
	movb	%dil, 250(%r12,%rcx)
	leal	13(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L165
	movzbl	13(%r9), %edi
	movslq	%ecx, %rcx
	addl	$14, %edx
	movb	%dil, 250(%r12,%rcx)
	cmpl	%edx, %esi
	jle	.L165
	movzbl	14(%r9), %ecx
	movslq	%edx, %rdx
	movb	%cl, 250(%r12,%rdx)
	.p2align 4,,10
	.p2align 3
.L165:
	addq	$1, %rax
	movl	$1, %edx
	cmpl	%ebx, %esi
	cmovle	%rdx, %rax
	addq	%rax, %r8
.L161:
	movq	%r8, 0(%r13)
	movl	$1, %eax
	movb	%sil, 282(%r12)
.L150:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L183
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L181:
	movsbl	76(%r12), %esi
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L182:
	movl	-60(%rbp), %esi
	subl	%ebx, %eax
	cltq
	addq	%rax, 0(%r13)
	cmpl	$3145727, %esi
	ja	.L157
	subq	$8, %rsp
	pushq	-88(%rbp)
	movl	32(%rbp), %r9d
	movq	%r10, %rcx
	subl	$2031616, %esi
	movq	%r11, %r8
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	ucnv_toUWriteCodePoint_67@PLT
	popq	%rcx
	movl	$1, %eax
	popq	%rsi
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L157:
	movslq	12(%rdi), %rax
	movl	%esi, %edx
	andl	$262143, %esi
	pushq	-88(%rbp)
	shrl	$18, %edx
	movq	%r11, %r9
	movq	%r10, %r8
	movq	%r14, %rcx
	leaq	(%rax,%rsi,2), %rsi
	movl	32(%rbp), %eax
	subl	$12, %edx
	addq	%rdi, %rsi
	movq	%r12, %rdi
	pushq	%rax
	call	ucnv_toUWriteUChars_67@PLT
	popq	%rax
	movl	$1, %eax
	popq	%rdx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%r8, %rdi
	subq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L162:
	movzbl	(%rdi,%rdx), %ecx
	movb	%cl, 250(%r12,%rdx)
	addq	$1, %rdx
	cmpl	%edx, %esi
	jg	.L162
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L158:
	movq	0(%r13), %r8
	xorl	%ebx, %ebx
	jmp	.L166
.L169:
	xorl	%ecx, %ecx
	jmp	.L159
.L183:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2116:
	.size	ucnv_extInitialMatchToU_67, .-ucnv_extInitialMatchToU_67
	.p2align 4
	.globl	ucnv_extSimpleMatchToU_67
	.type	ucnv_extSimpleMatchToU_67, @function
ucnv_extSimpleMatchToU_67:
.LFB2117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	testl	%edx, %edx
	jle	.L189
	testq	%rdi, %rdi
	je	.L187
	movl	8(%rdi), %r8d
	testl	%r8d, %r8d
	jle	.L187
	leaq	-28(%rbp), %rax
	pushq	$1
	movl	%edx, %ecx
	movl	%edx, %ebx
	pushq	%rax
	movq	%rsi, %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$-1, %esi
	call	_ZL16ucnv_extMatchToUPKiaPKciS2_iPjaa.part.0
	popq	%rdx
	popq	%rcx
	cmpl	%eax, %ebx
	jne	.L187
	movl	-28(%rbp), %eax
	cmpl	$3145727, %eax
	ja	.L187
	subl	$2031616, %eax
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$65534, %eax
.L184:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L191
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movl	$65535, %eax
	jmp	.L184
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2117:
	.size	ucnv_extSimpleMatchToU_67, .-ucnv_extSimpleMatchToU_67
	.p2align 4
	.globl	ucnv_extContinueMatchToU_67
	.type	ucnv_extContinueMatchToU_67, @function
ucnv_extContinueMatchToU_67:
.LFB2118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	250(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movl	$0, -60(%rbp)
	movzbl	252(%rax), %edx
	cmpb	$12, %dl
	je	.L232
	xorl	%esi, %esi
	cmpb	$-37, %dl
	sete	%sil
	leal	-1(%rsi,%rsi), %esi
.L194:
	movq	288(%rax), %rdi
	testq	%rdi, %rdi
	je	.L195
	movl	8(%rdi), %r9d
	testl	%r9d, %r9d
	jle	.L195
	movsbl	2(%rbx), %eax
	movq	16(%rbx), %r8
	movq	%r14, %rdx
	movsbl	282(%r12), %ecx
	movl	24(%rbx), %r9d
	pushq	%rax
	leaq	-60(%rbp), %rax
	pushq	%rax
	subl	%r8d, %r9d
	call	_ZL16ucnv_extMatchToUPKiaPKciS2_iPjaa.part.0
	popq	%rdi
	popq	%r8
	testl	%eax, %eax
	jle	.L196
	movsbl	282(%r12), %edx
	cmpl	%eax, %edx
	jg	.L197
	subl	%edx, %eax
	cltq
	addq	%rax, 16(%rbx)
	movb	$0, 282(%r12)
.L198:
	movl	-60(%rbp), %eax
	movq	40(%rbx), %rcx
	leaq	48(%rbx), %r8
	leaq	32(%rbx), %rdx
	cmpl	$3145727, %eax
	ja	.L199
	subq	$8, %rsp
	leal	-2031616(%rax), %esi
	movl	%r15d, %r9d
	movq	%r12, %rdi
	pushq	%r13
	call	ucnv_toUWriteCodePoint_67@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L196:
	je	.L195
	movsbq	282(%r12), %rcx
	movl	%eax, %esi
	movq	16(%rbx), %r8
	negl	%esi
	movq	%rcx, %rdx
	cmpl	%ecx, %esi
	jle	.L201
	leaq	266(%r12,%rcx), %r9
	leaq	250(%r12,%rcx), %rdi
	cmpq	%r9, %r8
	leaq	16(%r8), %r9
	setnb	%r10b
	cmpq	%r9, %rdi
	setnb	%r9b
	orb	%r9b, %r10b
	je	.L233
	movl	%ecx, %r10d
	notl	%r10d
	movl	%r10d, %r9d
	subl	%eax, %r9d
	cmpl	$14, %r9d
	jbe	.L212
	movl	%esi, %r11d
	movdqu	(%r8), %xmm0
	subl	%ecx, %r11d
	movl	%r11d, %r9d
	movups	%xmm0, (%rdi)
	andl	$-16, %r9d
	movl	%r9d, %edi
	leal	(%r9,%rcx), %edx
	addq	%r8, %rdi
	cmpl	%r9d, %r11d
	je	.L204
	movzbl	(%rdi), %r9d
	movslq	%edx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	1(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	2(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	2(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	3(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	3(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	4(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	4(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	5(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	5(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	6(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	6(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	7(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	7(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	8(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	8(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	9(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	9(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	10(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	10(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	11(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	11(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	12(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	12(%rdi), %r9d
	movslq	%ecx, %rcx
	movb	%r9b, 250(%r12,%rcx)
	leal	13(%rdx), %ecx
	cmpl	%ecx, %esi
	jle	.L204
	movzbl	13(%rdi), %r9d
	movslq	%ecx, %rcx
	addl	$14, %edx
	movb	%r9b, 250(%r12,%rcx)
	cmpl	%edx, %esi
	jle	.L204
	movzbl	14(%rdi), %ecx
	movslq	%edx, %rdx
	movb	%cl, 250(%r12,%rdx)
	.p2align 4,,10
	.p2align 3
.L204:
	subl	%eax, %r10d
	leaq	1(%r8,%r10), %r8
.L201:
	movq	%r8, 16(%rbx)
	movb	%sil, 282(%r12)
.L192:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L234
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movsbq	283(%r12), %rax
	leaq	65(%r12), %rcx
	cmpq	$8, %rax
	jnb	.L205
	testb	$4, %al
	jne	.L235
	testq	%rax, %rax
	je	.L206
	movzbl	250(%r12), %edx
	movb	%dl, 65(%r12)
	testb	$2, %al
	jne	.L236
.L206:
	movsbq	283(%r12), %rdx
	movsbl	282(%r12), %ebx
	subl	%edx, %ebx
	movb	%dl, 64(%r12)
	movq	%rdx, %rax
	testl	%ebx, %ebx
	jg	.L237
.L209:
	negl	%ebx
	movb	%bl, 282(%r12)
	movl	$10, 0(%r13)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L232:
	movsbl	76(%rdi), %esi
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L205:
	movq	250(%r12), %rdx
	leaq	73(%r12), %rdi
	movq	%r14, %rsi
	andq	$-8, %rdi
	movq	%rdx, 65(%r12)
	movq	-8(%r14,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%rax, %rcx
	shrq	$3, %rcx
	rep movsq
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L199:
	movq	48(%r12), %rsi
	movl	%eax, %r10d
	andl	$262143, %eax
	movq	%r8, %r9
	shrl	$18, %r10d
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	288(%rsi), %rdi
	subl	$12, %r10d
	movl	%r10d, %edx
	movslq	12(%rdi), %rsi
	pushq	%r13
	pushq	%r15
	leaq	(%rsi,%rax,2), %rsi
	addq	%rdi, %rsi
	movq	%r12, %rdi
	call	ucnv_toUWriteUChars_67@PLT
	popq	%rax
	popq	%rdx
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L197:
	subl	%eax, %edx
	cltq
	movq	%r14, %rdi
	movl	%edx, %ecx
	leaq	(%r14,%rax), %rsi
	movslq	%edx, %rdx
	movl	%ecx, -68(%rbp)
	call	memmove@PLT
	movl	-68(%rbp), %ecx
	movl	%ecx, %edx
	negl	%edx
	movb	%dl, 282(%r12)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L237:
	movslq	%ebx, %rdx
	leaq	(%r14,%rax), %rsi
	movq	%r14, %rdi
	call	memmove@PLT
	jmp	.L209
.L233:
	notl	%ecx
	movl	%ecx, %r10d
.L212:
	movq	%r8, %rdi
	subq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L202:
	movzbl	(%rdi,%rdx), %ecx
	movb	%cl, 250(%r12,%rdx)
	addq	$1, %rdx
	cmpl	%edx, %esi
	jg	.L202
	jmp	.L204
.L235:
	movl	250(%r12), %edx
	movl	%edx, 65(%r12)
	movl	-4(%r14,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L206
.L236:
	movzwl	-2(%r14,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L206
.L234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2118:
	.size	ucnv_extContinueMatchToU_67, .-ucnv_extContinueMatchToU_67
	.p2align 4
	.globl	ucnv_extInitialMatchFromU_67
	.type	ucnv_extInitialMatchFromU_67, @function
ucnv_extInitialMatchFromU_67:
.LFB2123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movl	%ebx, %esi
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movsbl	40(%rbp), %edx
	movq	24(%rbp), %r14
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	pushq	%rdx
	xorl	%ecx, %ecx
	movsbl	63(%r12), %edx
	movl	$0, -100(%rbp)
	subq	%rax, %r8
	pushq	%rdx
	leaq	-100(%rbp), %rdx
	movq	%r8, %r9
	movq	%rax, %r8
	pushq	%rdx
	sarq	%r9
	xorl	%edx, %edx
	call	_ZL18ucnv_extMatchFromUPKiiPKDsiS2_iPjaa
	addq	$24, %rsp
	cmpl	$1, %eax
	jle	.L239
	movl	-100(%rbp), %edx
	movl	%edx, %ecx
	shrl	$24, %ecx
	movl	%ecx, %r11d
	andl	$31, %r11d
	cmpl	$1, %r11d
	jne	.L240
	movq	48(%r12), %rsi
	xorl	%r8d, %r8d
	cmpb	$-37, 252(%rsi)
	je	.L238
	cltq
	andl	$16777215, %edx
	leaq	-4(%rax,%rax), %rax
	addq	%rax, (%r15)
	testb	$28, %cl
	je	.L299
.L243:
	movslq	32(%rdi), %rsi
	addq	%rsi, %rdx
	leaq	(%rdi,%rdx), %rsi
.L247:
	movl	80(%r12), %eax
	testl	%eax, %eax
	je	.L248
	cmpl	$1, %eax
	jle	.L249
	cmpl	$1, %r11d
	jne	.L249
	movl	$1, 80(%r12)
	movl	$15, %eax
.L250:
	leaq	-95(%rbp), %rdi
	movb	%al, -96(%rbp)
	cmpq	%rdi, %rsi
	je	.L251
	movslq	%r11d, %rdx
	movl	$31, %ecx
	movl	%r11d, -132(%rbp)
	call	__memcpy_chk@PLT
	movl	-132(%rbp), %r11d
.L251:
	addl	$1, %r11d
	leaq	-96(%rbp), %rsi
.L248:
	movl	32(%rbp), %eax
	pushq	-128(%rbp)
	movl	%r11d, %edx
	movq	%r14, %r9
	movq	-120(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	pushq	%rax
	call	ucnv_fromUWriteBytes_67@PLT
	popq	%rax
	movl	$1, %r8d
	popq	%rdx
.L238:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L300
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	testl	%eax, %eax
	js	.L301
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	jne	.L238
	movb	$1, 95(%r12)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L240:
	cltq
	andl	$16777215, %edx
	leaq	-4(%rax,%rax), %rax
	addq	%rax, (%r15)
	testb	$28, %cl
	jne	.L243
	cmpl	$3, %r11d
	je	.L244
	leaq	-95(%rbp), %rsi
	movq	%rsi, %rax
	cmpl	$2, %r11d
	jne	.L302
.L246:
	leaq	1(%rax), %rdi
	movb	%dh, (%rax)
	movb	%dl, (%rdi)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L301:
	movl	$-2, %edx
	movl	%ebx, 208(%r12)
	movq	(%r15), %rcx
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L253
	leaq	212(%r12), %rdi
	leaq	16(%rcx), %rax
	cmpq	%rax, %rdi
	leaq	228(%r12), %rax
	leal	-1(%rdx), %esi
	setnb	%dil
	cmpq	%rax, %rcx
	setnb	%al
	orb	%al, %dil
	je	.L254
	cmpl	$6, %esi
	jbe	.L254
	movdqu	(%rcx), %xmm0
	movl	%edx, %eax
	shrl	$3, %eax
	movups	%xmm0, 212(%r12)
	cmpl	$1, %eax
	je	.L255
	movdqu	16(%rcx), %xmm1
	movups	%xmm1, 228(%r12)
.L255:
	movl	%edx, %eax
	andl	$-8, %eax
	movl	%eax, %edi
	leaq	(%rcx,%rdi,2), %rdi
	cmpl	%eax, %edx
	je	.L257
	movzwl	(%rdi), %r9d
	movslq	%eax, %r8
	movw	%r9w, 212(%r12,%r8,2)
	leal	1(%rax), %r8d
	cmpl	%r8d, %edx
	jle	.L257
	movzwl	2(%rdi), %r9d
	movslq	%r8d, %r8
	movw	%r9w, 212(%r12,%r8,2)
	leal	2(%rax), %r8d
	cmpl	%r8d, %edx
	jle	.L257
	movzwl	4(%rdi), %r9d
	movslq	%r8d, %r8
	movw	%r9w, 212(%r12,%r8,2)
	leal	3(%rax), %r8d
	cmpl	%r8d, %edx
	jle	.L257
	movzwl	6(%rdi), %r9d
	movslq	%r8d, %r8
	movw	%r9w, 212(%r12,%r8,2)
	leal	4(%rax), %r8d
	cmpl	%r8d, %edx
	jle	.L257
	movzwl	8(%rdi), %r9d
	movslq	%r8d, %r8
	movw	%r9w, 212(%r12,%r8,2)
	leal	5(%rax), %r8d
	cmpl	%r8d, %edx
	jle	.L257
	movzwl	10(%rdi), %r9d
	movslq	%r8d, %r8
	addl	$6, %eax
	movw	%r9w, 212(%r12,%r8,2)
	cmpl	%eax, %edx
	jle	.L257
	movzwl	12(%rdi), %edi
	cltq
	movw	%di, 212(%r12,%rax,2)
.L257:
	leaq	2(%rcx,%rsi,2), %rcx
.L253:
	movq	%rcx, (%r15)
	movl	$1, %r8d
	movb	%dl, 281(%r12)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L302:
	movl	80(%r12), %eax
	testl	%eax, %eax
	je	.L248
.L249:
	andl	$30, %ecx
	je	.L248
	cmpl	$1, %eax
	jne	.L248
	movl	$2, 80(%r12)
	movl	$14, %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L244:
	movl	%edx, %eax
	leaq	-95(%rbp), %rsi
	shrl	$16, %eax
	movb	%al, -95(%rbp)
	leaq	-94(%rbp), %rax
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L299:
	leaq	-95(%rbp), %rsi
	movq	%rsi, %rdi
	movb	%dl, (%rdi)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L254:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L258:
	movzwl	(%rcx,%rax,2), %edi
	movw	%di, 212(%r12,%rax,2)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rsi, %rdi
	jne	.L258
	jmp	.L257
.L300:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2123:
	.size	ucnv_extInitialMatchFromU_67, .-ucnv_extInitialMatchFromU_67
	.p2align 4
	.globl	ucnv_extSimpleMatchFromU_67
	.type	ucnv_extSimpleMatchFromU_67, @function
ucnv_extSimpleMatchFromU_67:
.LFB2124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%cl, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-28(%rbp), %rax
	pushq	$1
	pushq	%rcx
	xorl	%ecx, %ecx
	pushq	%rax
	call	_ZL18ucnv_extMatchFromUPKiiPKDsiS2_iPjaa
	addq	$24, %rsp
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$1, %r8d
	jle	.L303
	movl	-28(%rbp), %edx
	movl	%edx, %ecx
	shrl	$24, %ecx
	testb	$28, %cl
	je	.L309
.L303:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L310
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movl	%ecx, %eax
	movl	%edx, %ecx
	andl	$31, %eax
	andl	$16777215, %ecx
	movl	%ecx, (%rbx)
	movl	%eax, %ecx
	negl	%ecx
	testl	%edx, %edx
	cmovns	%ecx, %eax
	jmp	.L303
.L310:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2124:
	.size	ucnv_extSimpleMatchFromU_67, .-ucnv_extSimpleMatchFromU_67
	.p2align 4
	.globl	ucnv_extContinueMatchFromU_67
	.type	ucnv_extContinueMatchFromU_67, @function
ucnv_extContinueMatchFromU_67:
.LFB2125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	212(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movq	%r15, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	16(%rsi), %r8
	movq	24(%rsi), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movsbl	281(%rdi), %ecx
	movl	$0, -100(%rbp)
	movl	208(%rdi), %esi
	subq	%r8, %r9
	movq	288(%rax), %rdi
	movsbl	2(%r14), %eax
	sarq	%r9
	pushq	%rax
	movsbl	63(%r12), %eax
	pushq	%rax
	leaq	-100(%rbp), %rax
	pushq	%rax
	call	_ZL18ucnv_extMatchFromUPKiiPKDsiS2_iPjaa
	addq	$24, %rsp
	cmpl	$1, %eax
	jle	.L312
	movsbl	281(%r12), %edx
	subl	$2, %eax
	cmpl	%eax, %edx
	jg	.L313
	subl	%edx, %eax
	cltq
	addq	%rax, %rax
	addq	%rax, 16(%r14)
	movb	$0, 281(%r12)
.L314:
	movl	-100(%rbp), %eax
	movq	40(%r14), %r8
	leaq	48(%r14), %r9
	movl	$-1, 208(%r12)
	addq	$32, %r14
	movl	%eax, %edx
	andl	$16777215, %eax
	shrl	$24, %edx
	movl	%edx, %r15d
	andl	$31, %r15d
	testb	$28, %dl
	jne	.L315
	cmpl	$2, %r15d
	je	.L335
	cmpl	$3, %r15d
	je	.L317
	cmpl	$1, %r15d
	je	.L364
	movl	80(%r12), %eax
	leaq	-95(%rbp), %rsi
	testl	%eax, %eax
	je	.L321
.L322:
	andl	$30, %edx
	je	.L321
	cmpl	$1, %eax
	jne	.L321
	movl	$2, 80(%r12)
	movl	$14, %eax
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L312:
	movsbq	281(%r12), %rdx
	testl	%eax, %eax
	js	.L365
	cmpl	$1, %eax
	je	.L366
.L333:
	movl	208(%r12), %eax
	negl	%edx
	movl	$-1, 208(%r12)
	movb	%dl, 281(%r12)
	movl	%eax, 84(%r12)
	movl	$10, (%rbx)
.L311:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L367
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movq	48(%r12), %rcx
	movq	288(%rcx), %rcx
	movslq	32(%rcx), %rsi
	addq	%rsi, %rax
	leaq	(%rcx,%rax), %rsi
.L320:
	movl	80(%r12), %eax
	testl	%eax, %eax
	je	.L321
	cmpl	$1, %eax
	jle	.L322
	cmpl	$1, %r15d
	jne	.L322
	movl	$1, 80(%r12)
	movl	$15, %eax
.L323:
	leaq	-95(%rbp), %rdi
	movb	%al, -96(%rbp)
	cmpq	%rdi, %rsi
	je	.L324
	movslq	%r15d, %rdx
	movl	$31, %ecx
	movq	%r8, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	__memcpy_chk@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %r9
.L324:
	addl	$1, %r15d
	leaq	-96(%rbp), %rsi
.L321:
	pushq	%rbx
	movl	%r15d, %edx
	movq	%r14, %rcx
	movq	%r12, %rdi
	pushq	%r13
	call	ucnv_fromUWriteBytes_67@PLT
	popq	%rax
	popq	%rdx
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L313:
	subl	%eax, %edx
	cltq
	movq	%r15, %rdi
	leaq	(%r15,%rax,2), %rsi
	movl	%edx, -120(%rbp)
	call	u_memmove_67@PLT
	movl	-120(%rbp), %edx
	negl	%edx
	movb	%dl, 281(%r12)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L364:
	leaq	-95(%rbp), %rsi
	movq	%rsi, %r10
.L318:
	movb	%al, (%r10)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L317:
	movl	%eax, %ecx
	leaq	-94(%rbp), %rdi
	leaq	-95(%rbp), %rsi
	shrl	$16, %ecx
	movb	%cl, -95(%rbp)
.L316:
	movb	%ah, (%rdi)
	leaq	1(%rdi), %r10
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L366:
	movb	$1, 95(%r12)
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L365:
	movl	$-2, %ecx
	movq	16(%r14), %rsi
	subl	%eax, %ecx
	movl	%ecx, %eax
	movsbl	%dl, %ecx
	cmpl	%ecx, %eax
	jle	.L327
	leaq	228(%rdx,%rdx), %rdi
	leaq	(%r12,%rdi), %r8
	leaq	-16(%r12,%rdi), %rdx
	cmpq	%r8, %rsi
	leaq	16(%rsi), %r8
	setnb	%r9b
	cmpq	%r8, %rdx
	setnb	%r8b
	orb	%r8b, %r9b
	je	.L368
	movl	%ecx, %r8d
	notl	%r8d
	leal	(%r8,%rax), %r9d
	cmpl	$6, %r9d
	jbe	.L328
	movl	%eax, %r9d
	movdqu	(%rsi), %xmm0
	subl	%ecx, %r9d
	movl	%r9d, %edi
	movups	%xmm0, (%rdx)
	shrl	$3, %edi
	cmpl	$1, %edi
	je	.L329
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 16(%rdx)
.L329:
	movl	%r9d, %edx
	andl	$-8, %edx
	movl	%edx, %edi
	addl	%edx, %ecx
	leaq	(%rsi,%rdi,2), %rdi
	cmpl	%edx, %r9d
	je	.L331
	movzwl	(%rdi), %r9d
	movslq	%ecx, %rdx
	movw	%r9w, 212(%r12,%rdx,2)
	leal	1(%rcx), %edx
	cmpl	%edx, %eax
	jle	.L331
	movzwl	2(%rdi), %r9d
	movslq	%edx, %rdx
	movw	%r9w, 212(%r12,%rdx,2)
	leal	2(%rcx), %edx
	cmpl	%edx, %eax
	jle	.L331
	movzwl	4(%rdi), %r9d
	movslq	%edx, %rdx
	movw	%r9w, 212(%r12,%rdx,2)
	leal	3(%rcx), %edx
	cmpl	%edx, %eax
	jle	.L331
	movzwl	6(%rdi), %r9d
	movslq	%edx, %rdx
	movw	%r9w, 212(%r12,%rdx,2)
	leal	4(%rcx), %edx
	cmpl	%edx, %eax
	jle	.L331
	movzwl	8(%rdi), %r9d
	movslq	%edx, %rdx
	movw	%r9w, 212(%r12,%rdx,2)
	leal	5(%rcx), %edx
	cmpl	%edx, %eax
	jle	.L331
	movzwl	10(%rdi), %r9d
	movslq	%edx, %rdx
	movw	%r9w, 212(%r12,%rdx,2)
	leal	6(%rcx), %edx
	cmpl	%edx, %eax
	jle	.L331
	movzwl	12(%rdi), %ecx
	movslq	%edx, %rdx
	movw	%cx, 212(%r12,%rdx,2)
.L331:
	leal	(%r8,%rax), %edx
	leaq	2(%rsi,%rdx,2), %rsi
.L327:
	movq	%rsi, 16(%r14)
	movb	%al, 281(%r12)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	-95(%rbp), %rsi
	movq	%rsi, %rdi
	jmp	.L316
.L368:
	notl	%ecx
	movl	%ecx, %r8d
.L328:
	leal	(%r8,%rax), %r9d
	leaq	-228(%r12,%rdi), %rdi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L332:
	movzwl	(%rsi,%rdx,2), %ecx
	movw	%cx, 212(%rdi,%rdx,2)
	movq	%rdx, %rcx
	addq	$1, %rdx
	cmpq	%r9, %rcx
	jne	.L332
	jmp	.L331
.L367:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2125:
	.size	ucnv_extContinueMatchFromU_67, .-ucnv_extContinueMatchFromU_67
	.p2align 4
	.globl	ucnv_extGetUnicodeSet_67
	.type	ucnv_extGetUnicodeSet_67, @function
ucnv_extGetUnicodeSet_67:
.LFB2128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movq	288(%rax), %r14
	movq	%rsi, -120(%rbp)
	movl	%ecx, -104(%rbp)
	movq	%r8, -136(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	testq	%r14, %r14
	je	.L369
	movl	%edx, %r11d
	movslq	40(%r14), %rdx
	movslq	60(%r14), %r15
	movl	$3, %r8d
	leaq	(%r14,%rdx), %rdi
	movq	%rdi, -192(%rbp)
	movslq	52(%r14), %rdi
	movq	%rdi, -160(%rbp)
	movl	44(%r14), %edi
	movl	%edi, -172(%rbp)
	cmpl	$2, %ecx
	je	.L371
	cmpb	$-37, 252(%rax)
	je	.L397
	movl	$1, %r8d
	testl	%ecx, %ecx
	jne	.L397
.L371:
	movl	-172(%rbp), %eax
	testl	%eax, %eax
	jle	.L369
	leal	-1(%rax), %edx
	movq	-192(%rbp), %rax
	xorl	%r13d, %r13d
	leaq	2(%rax,%rdx,2), %rdi
	movq	%rax, -168(%rbp)
	subq	$-128, %rax
	movq	%rax, -200(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rdi, -184(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L374:
	addl	$1024, %r13d
.L390:
	addq	$2, -168(%rbp)
	movq	-168(%rbp), %rax
	cmpq	%rax, -184(%rbp)
	je	.L369
.L392:
	movq	-168(%rbp), %rax
	movzwl	(%rax), %ecx
	movq	%rcx, %rdx
	cmpl	%ecx, -172(%rbp)
	jge	.L374
	movq	-192(%rbp), %rax
	addq	%rdx, %rdx
	leaq	(%rax,%rdx), %r12
	addq	-200(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L375:
	addl	$16, %r13d
.L389:
	addq	$2, %r12
	cmpq	%r12, -112(%rbp)
	je	.L390
.L391:
	movzwl	(%r12), %edx
	sall	$2, %edx
	je	.L375
	movq	-160(%rbp), %rax
	movslq	%edx, %rdx
	movq	%r12, -152(%rbp)
	movq	%r14, %r12
	leaq	(%rax,%rdx,2), %rbx
	addq	%r14, %rbx
	movq	%r15, %r14
	movl	%r8d, %r15d
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L408:
	cmpl	$65535, %r13d
	jg	.L378
	movl	%r13d, %ecx
	movl	$1, %esi
.L379:
	pushq	-136(%rbp)
	movq	-128(%rbp), %rdi
	movl	%r13d, %r9d
	movl	%r15d, %r8d
	pushq	%rdx
	pushq	%rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rsi
	pushq	-144(%rbp)
	movw	%cx, -96(%rbp)
	movl	%r11d, %ecx
	movl	%r11d, -100(%rbp)
	call	_ZL27ucnv_extGetUnicodeSetStringPK20UConverterSharedDataPKiPK9USetAdder20UConverterUnicodeSetiiPDsiiP10UErrorCode
	movl	-100(%rbp), %r11d
	addq	$32, %rsp
.L376:
	addl	$1, %r13d
	testb	$15, %r13b
	je	.L407
.L388:
	movzwl	(%rbx), %edx
	addq	$2, %rbx
	leaq	(%r12,%rdx,4), %rdx
	movl	(%rdx,%r14), %edx
	testl	%edx, %edx
	je	.L376
	movl	%edx, %ecx
	shrl	$24, %ecx
	je	.L408
	testl	%r11d, %r11d
	jne	.L380
	movl	%edx, %esi
	andl	$-1610612736, %esi
	cmpl	$-2147483648, %esi
	jne	.L376
.L381:
	andl	$31, %ecx
	cmpl	%ecx, %r15d
	jg	.L376
	movl	-104(%rbp), %eax
	cmpl	$4, %eax
	je	.L382
	ja	.L383
	cmpl	$2, %eax
	je	.L384
	cmpl	$3, %eax
	jne	.L386
	cmpl	$2, %ecx
	jne	.L376
	andl	$16777215, %edx
	subl	$33088, %edx
	cmpl	$28348, %edx
	ja	.L376
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-120(%rbp), %rax
	movl	%r13d, %esi
	addl	$1, %r13d
	movl	%r11d, -100(%rbp)
	movq	(%rax), %rdi
	call	*8(%rax)
	movl	-100(%rbp), %r11d
	testb	$15, %r13b
	jne	.L388
	.p2align 4,,10
	.p2align 3
.L407:
	movl	%r15d, %r8d
	movq	%r14, %r15
	movq	%r12, %r14
	movq	-152(%rbp), %r12
	jmp	.L389
.L369:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	testl	$536870912, %edx
	je	.L381
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L378:
	movl	%r13d, %esi
	movl	%r13d, %ecx
	andw	$1023, %si
	sarl	$10, %ecx
	orw	$-9216, %si
	subw	$10304, %cx
	movw	%si, -94(%rbp)
	movl	$2, %esi
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L383:
	cmpl	$5, -104(%rbp)
	jne	.L386
	cmpl	$2, %ecx
	jne	.L376
	movl	%edx, %ecx
	addw	$24159, %dx
	andl	$16777215, %ecx
	cmpw	$23645, %dx
	ja	.L376
.L406:
	addl	$95, %ecx
	cmpb	$93, %cl
	jbe	.L386
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L382:
	cmpl	$2, %ecx
	jne	.L376
	movl	%edx, %ecx
	addw	$24159, %dx
	andl	$16777215, %ecx
	cmpw	$23901, %dx
	jbe	.L406
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L384:
	cmpl	$3, %ecx
	jne	.L376
	andl	$16777215, %edx
	cmpl	$8585215, %edx
	jbe	.L386
	jmp	.L376
.L397:
	movl	$2, %r8d
	jmp	.L371
.L409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2128:
	.size	ucnv_extGetUnicodeSet_67, .-ucnv_extGetUnicodeSet_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
