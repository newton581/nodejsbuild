	.file	"ucnv_set.cpp"
	.text
	.p2align 4
	.globl	ucnv_getUnicodeSet_67
	.type	ucnv_getUnicodeSet_67, @function
ucnv_getUnicodeSet_67:
.LFB2027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L1
	movl	(%rcx), %eax
	movq	%rcx, %r12
	testl	%eax, %eax
	jg	.L1
	testq	%rsi, %rsi
	sete	%cl
	cmpl	$1, %edx
	seta	%al
	orb	%al, %cl
	jne	.L7
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L7
	movq	48(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	$0, 120(%rax)
	je	.L13
	movq	uset_add_67@GOTPCREL(%rip), %xmm0
	movq	uset_removeRange_67@GOTPCREL(%rip), %rax
	movq	%rsi, %rdi
	movl	%edx, -84(%rbp)
	movq	%rsi, -80(%rbp)
	movhps	uset_addRange_67@GOTPCREL(%rip), %xmm0
	movq	%rax, -40(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	uset_addString_67@GOTPCREL(%rip), %xmm0
	movhps	uset_remove_67@GOTPCREL(%rip), %xmm0
	movups	%xmm0, -56(%rbp)
	call	uset_clear_67@PLT
	movq	48(%r13), %rax
	movl	-84(%rbp), %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	32(%rax), %rax
	call	*120(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	$1, (%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$16, (%r12)
	jmp	.L1
.L14:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2027:
	.size	ucnv_getUnicodeSet_67, .-ucnv_getUnicodeSet_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
