	.file	"sharedobject.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SharedObjectD2Ev
	.type	_ZN6icu_6712SharedObjectD2Ev, @function
_ZN6icu_6712SharedObjectD2Ev:
.LFB3036:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712SharedObjectE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3036:
	.size	_ZN6icu_6712SharedObjectD2Ev, .-_ZN6icu_6712SharedObjectD2Ev
	.globl	_ZN6icu_6712SharedObjectD1Ev
	.set	_ZN6icu_6712SharedObjectD1Ev,_ZN6icu_6712SharedObjectD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712SharedObjectD0Ev
	.type	_ZN6icu_6712SharedObjectD0Ev, @function
_ZN6icu_6712SharedObjectD0Ev:
.LFB3038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712SharedObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3038:
	.size	_ZN6icu_6712SharedObjectD0Ev, .-_ZN6icu_6712SharedObjectD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716UnifiedCacheBaseD2Ev
	.type	_ZN6icu_6716UnifiedCacheBaseD2Ev, @function
_ZN6icu_6716UnifiedCacheBaseD2Ev:
.LFB3040:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6716UnifiedCacheBaseE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3040:
	.size	_ZN6icu_6716UnifiedCacheBaseD2Ev, .-_ZN6icu_6716UnifiedCacheBaseD2Ev
	.globl	_ZN6icu_6716UnifiedCacheBaseD1Ev
	.set	_ZN6icu_6716UnifiedCacheBaseD1Ev,_ZN6icu_6716UnifiedCacheBaseD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716UnifiedCacheBaseD0Ev
	.type	_ZN6icu_6716UnifiedCacheBaseD0Ev, @function
_ZN6icu_6716UnifiedCacheBaseD0Ev:
.LFB3042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6716UnifiedCacheBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3042:
	.size	_ZN6icu_6716UnifiedCacheBaseD0Ev, .-_ZN6icu_6716UnifiedCacheBaseD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SharedObject6addRefEv
	.type	_ZNK6icu_6712SharedObject6addRefEv, @function
_ZNK6icu_6712SharedObject6addRefEv:
.LFB3043:
	.cfi_startproc
	endbr64
	lock addl	$1, 12(%rdi)
	ret
	.cfi_endproc
.LFE3043:
	.size	_ZNK6icu_6712SharedObject6addRefEv, .-_ZNK6icu_6712SharedObject6addRefEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SharedObject9removeRefEv
	.type	_ZNK6icu_6712SharedObject9removeRefEv, @function
_ZNK6icu_6712SharedObject9removeRefEv:
.LFB3044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	lock subl	$1, 12(%r12)
	je	.L14
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	(%r12), %rax
	leaq	_ZN6icu_6712SharedObjectD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L12
	leaq	16+_ZTVN6icu_6712SharedObjectE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3044:
	.size	_ZNK6icu_6712SharedObject9removeRefEv, .-_ZNK6icu_6712SharedObject9removeRefEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SharedObject11getRefCountEv
	.type	_ZNK6icu_6712SharedObject11getRefCountEv, @function
_ZNK6icu_6712SharedObject11getRefCountEv:
.LFB3045:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	ret
	.cfi_endproc
.LFE3045:
	.size	_ZNK6icu_6712SharedObject11getRefCountEv, .-_ZNK6icu_6712SharedObject11getRefCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712SharedObject20deleteIfZeroRefCountEv
	.type	_ZNK6icu_6712SharedObject20deleteIfZeroRefCountEv, @function
_ZNK6icu_6712SharedObject20deleteIfZeroRefCountEv:
.LFB3046:
	.cfi_startproc
	endbr64
	cmpq	$0, 16(%rdi)
	je	.L24
.L16:
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jne	.L16
	movq	(%rdi), %rax
	leaq	_ZN6icu_6712SharedObjectD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L20
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712SharedObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rax, (%rdi)
	movq	%rdi, -8(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	-8(%rbp), %rdi
	leave
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	jmp	*%rax
	.cfi_endproc
.LFE3046:
	.size	_ZNK6icu_6712SharedObject20deleteIfZeroRefCountEv, .-_ZNK6icu_6712SharedObject20deleteIfZeroRefCountEv
	.weak	_ZTSN6icu_6716UnifiedCacheBaseE
	.section	.rodata._ZTSN6icu_6716UnifiedCacheBaseE,"aG",@progbits,_ZTSN6icu_6716UnifiedCacheBaseE,comdat
	.align 16
	.type	_ZTSN6icu_6716UnifiedCacheBaseE, @object
	.size	_ZTSN6icu_6716UnifiedCacheBaseE, 28
_ZTSN6icu_6716UnifiedCacheBaseE:
	.string	"N6icu_6716UnifiedCacheBaseE"
	.weak	_ZTIN6icu_6716UnifiedCacheBaseE
	.section	.data.rel.ro._ZTIN6icu_6716UnifiedCacheBaseE,"awG",@progbits,_ZTIN6icu_6716UnifiedCacheBaseE,comdat
	.align 8
	.type	_ZTIN6icu_6716UnifiedCacheBaseE, @object
	.size	_ZTIN6icu_6716UnifiedCacheBaseE, 24
_ZTIN6icu_6716UnifiedCacheBaseE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6716UnifiedCacheBaseE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6712SharedObjectE
	.section	.rodata._ZTSN6icu_6712SharedObjectE,"aG",@progbits,_ZTSN6icu_6712SharedObjectE,comdat
	.align 16
	.type	_ZTSN6icu_6712SharedObjectE, @object
	.size	_ZTSN6icu_6712SharedObjectE, 24
_ZTSN6icu_6712SharedObjectE:
	.string	"N6icu_6712SharedObjectE"
	.weak	_ZTIN6icu_6712SharedObjectE
	.section	.data.rel.ro._ZTIN6icu_6712SharedObjectE,"awG",@progbits,_ZTIN6icu_6712SharedObjectE,comdat
	.align 8
	.type	_ZTIN6icu_6712SharedObjectE, @object
	.size	_ZTIN6icu_6712SharedObjectE, 24
_ZTIN6icu_6712SharedObjectE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712SharedObjectE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6712SharedObjectE
	.section	.data.rel.ro._ZTVN6icu_6712SharedObjectE,"awG",@progbits,_ZTVN6icu_6712SharedObjectE,comdat
	.align 8
	.type	_ZTVN6icu_6712SharedObjectE, @object
	.size	_ZTVN6icu_6712SharedObjectE, 40
_ZTVN6icu_6712SharedObjectE:
	.quad	0
	.quad	_ZTIN6icu_6712SharedObjectE
	.quad	_ZN6icu_6712SharedObjectD1Ev
	.quad	_ZN6icu_6712SharedObjectD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.weak	_ZTVN6icu_6716UnifiedCacheBaseE
	.section	.data.rel.ro._ZTVN6icu_6716UnifiedCacheBaseE,"awG",@progbits,_ZTVN6icu_6716UnifiedCacheBaseE,comdat
	.align 8
	.type	_ZTVN6icu_6716UnifiedCacheBaseE, @object
	.size	_ZTVN6icu_6716UnifiedCacheBaseE, 48
_ZTVN6icu_6716UnifiedCacheBaseE:
	.quad	0
	.quad	_ZTIN6icu_6716UnifiedCacheBaseE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
