	.file	"rbbi.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722RuleBasedBreakIterator17getDynamicClassIDEv
	.type	_ZNK6icu_6722RuleBasedBreakIterator17getDynamicClassIDEv, @function
_ZNK6icu_6722RuleBasedBreakIterator17getDynamicClassIDEv:
.LFB3165:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722RuleBasedBreakIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3165:
	.size	_ZNK6icu_6722RuleBasedBreakIterator17getDynamicClassIDEv, .-_ZNK6icu_6722RuleBasedBreakIterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722RuleBasedBreakIterator7getTextEv
	.type	_ZNK6icu_6722RuleBasedBreakIterator7getTextEv, @function
_ZNK6icu_6722RuleBasedBreakIterator7getTextEv:
.LFB3195:
	.cfi_startproc
	endbr64
	movq	528(%rdi), %rax
	ret
	.cfi_endproc
.LFE3195:
	.size	_ZNK6icu_6722RuleBasedBreakIterator7getTextEv, .-_ZNK6icu_6722RuleBasedBreakIterator7getTextEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722RuleBasedBreakIterator7currentEv
	.type	_ZNK6icu_6722RuleBasedBreakIterator7currentEv, @function
_ZNK6icu_6722RuleBasedBreakIterator7currentEv:
.LFB3207:
	.cfi_startproc
	endbr64
	movl	480(%rdi), %eax
	ret
	.cfi_endproc
.LFE3207:
	.size	_ZNK6icu_6722RuleBasedBreakIterator7currentEv, .-_ZNK6icu_6722RuleBasedBreakIterator7currentEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722RuleBasedBreakIterator13getRuleStatusEv
	.type	_ZNK6icu_6722RuleBasedBreakIterator13getRuleStatusEv, @function
_ZNK6icu_6722RuleBasedBreakIterator13getRuleStatusEv:
.LFB3215:
	.cfi_startproc
	endbr64
	movq	472(%rdi), %rdx
	movslq	484(%rdi), %rcx
	movq	32(%rdx), %rdx
	movq	%rcx, %rax
	addl	(%rdx,%rcx,4), %eax
	cltq
	movl	(%rdx,%rax,4), %eax
	ret
	.cfi_endproc
.LFE3215:
	.size	_ZNK6icu_6722RuleBasedBreakIterator13getRuleStatusEv, .-_ZNK6icu_6722RuleBasedBreakIterator13getRuleStatusEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator14getBinaryRulesERj
	.type	_ZN6icu_6722RuleBasedBreakIterator14getBinaryRulesERj, @function
_ZN6icu_6722RuleBasedBreakIterator14getBinaryRulesERj:
.LFB3217:
	.cfi_startproc
	endbr64
	movq	472(%rdi), %rax
	movl	$0, (%rsi)
	testq	%rax, %rax
	je	.L6
	movq	(%rax), %rax
	movl	8(%rax), %edx
	movl	%edx, (%rsi)
.L6:
	ret
	.cfi_endproc
.LFE3217:
	.size	_ZN6icu_6722RuleBasedBreakIterator14getBinaryRulesERj, .-_ZN6icu_6722RuleBasedBreakIterator14getBinaryRulesERj
	.p2align 4
	.type	_deleteFactory, @function
_deleteFactory:
.LFB3220:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE3220:
	.size	_deleteFactory, .-_deleteFactory
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722RuleBasedBreakIterator8getUTextEP5UTextR10UErrorCode
	.type	_ZNK6icu_6722RuleBasedBreakIterator8getUTextEP5UTextR10UErrorCode, @function
_ZNK6icu_6722RuleBasedBreakIterator8getUTextEP5UTextR10UErrorCode:
.LFB3194:
	.cfi_startproc
	endbr64
	movq	%rdi, %r9
	movq	%rdx, %r8
	movq	%rsi, %rdi
	movl	$1, %ecx
	leaq	328(%r9), %rsi
	xorl	%edx, %edx
	jmp	utext_clone_67@PLT
	.cfi_endproc
.LFE3194:
	.size	_ZNK6icu_6722RuleBasedBreakIterator8getUTextEP5UTextR10UErrorCode, .-_ZNK6icu_6722RuleBasedBreakIterator8getUTextEP5UTextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722RuleBasedBreakIterator8hashCodeEv
	.type	_ZNK6icu_6722RuleBasedBreakIterator8hashCodeEv, @function
_ZNK6icu_6722RuleBasedBreakIterator8hashCodeEv:
.LFB3192:
	.cfi_startproc
	endbr64
	movq	472(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	jmp	_ZN6icu_6715RBBIDataWrapper8hashCodeEv@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3192:
	.size	_ZNK6icu_6722RuleBasedBreakIterator8hashCodeEv, .-_ZNK6icu_6722RuleBasedBreakIterator8hashCodeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator5firstEv
	.type	_ZN6icu_6722RuleBasedBreakIterator5firstEv, @function
_ZN6icu_6722RuleBasedBreakIterator5firstEv:
.LFB3199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	488(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi@PLT
	testb	%al, %al
	je	.L20
.L17:
	movq	488(%rbx), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	488(%rbx), %rdi
	leaq	-28(%rbp), %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode@PLT
	jmp	.L17
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3199:
	.size	_ZN6icu_6722RuleBasedBreakIterator5firstEv, .-_ZN6icu_6722RuleBasedBreakIterator5firstEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator4nextEv
	.type	_ZN6icu_6722RuleBasedBreakIterator4nextEv, @function
_ZN6icu_6722RuleBasedBreakIterator4nextEv:
.LFB3202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	488(%rdi), %rdi
	movl	28(%rdi), %eax
	cmpl	20(%rdi), %eax
	je	.L28
	addl	$1, %eax
	movq	8(%rdi), %rdx
	andl	$127, %eax
	movl	%eax, 28(%rdi)
	cltq
	movl	32(%rdi,%rax,4), %eax
	movl	%eax, 480(%rdx)
	movl	%eax, 24(%rdi)
	movslq	28(%rdi), %rax
	movzwl	544(%rdi,%rax,2), %eax
	movl	%eax, 484(%rdx)
.L24:
	cmpb	$0, 632(%rbx)
	jne	.L26
	movl	480(%rbx), %eax
.L22:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache6nextOLEv@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$-1, %eax
	jmp	.L22
	.cfi_endproc
.LFE3202:
	.size	_ZN6icu_6722RuleBasedBreakIterator4nextEv, .-_ZN6icu_6722RuleBasedBreakIterator4nextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator8previousEv
	.type	_ZN6icu_6722RuleBasedBreakIterator8previousEv, @function
_ZN6icu_6722RuleBasedBreakIterator8previousEv:
.LFB3203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-28(%rbp), %rsi
	subq	$24, %rsp
	movq	488(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache8previousER10UErrorCode@PLT
	cmpb	$0, 632(%rbx)
	jne	.L32
	movl	480(%rbx), %eax
.L29:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L34
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L29
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3203:
	.size	_ZN6icu_6722RuleBasedBreakIterator8previousEv, .-_ZN6icu_6722RuleBasedBreakIterator8previousEv
	.p2align 4
	.globl	rbbi_cleanup_67
	.type	rbbi_cleanup_67, @function
rbbi_cleanup_67:
.LFB3219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZL23gLanguageBreakFactories(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L36
	movq	(%rdi), %rax
	call	*8(%rax)
.L36:
	movq	_ZL12gEmptyString(%rip), %rdi
	movq	$0, _ZL23gLanguageBreakFactories(%rip)
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	call	*8(%rax)
.L37:
	movq	$0, _ZL12gEmptyString(%rip)
	movl	$1, %eax
	movl	$0, _ZL31gLanguageBreakFactoriesInitOnce(%rip)
	mfence
	movl	$0, _ZL13gRBBIInitOnce(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3219:
	.size	rbbi_cleanup_67, .-rbbi_cleanup_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator16getRuleStatusVecEPiiR10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator16getRuleStatusVecEPiiR10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator16getRuleStatusVecEPiiR10UErrorCode:
.LFB3216:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	xorl	%r9d, %r9d
	testl	%eax, %eax
	jg	.L45
	movq	472(%rdi), %rax
	movq	32(%rax), %r8
	movslq	484(%rdi), %rax
	movl	(%r8,%rax,4), %r9d
	cmpl	%r9d, %edx
	jl	.L53
	movl	%r9d, %edx
.L47:
	testl	%edx, %edx
	jle	.L45
	movl	4(%r8,%rax,4), %eax
	movl	%eax, (%rsi)
	cmpl	$1, %edx
	je	.L45
	leal	-2(%rdx), %ecx
	movl	$1, %eax
	addq	$2, %rcx
	.p2align 4,,10
	.p2align 3
.L50:
	movl	484(%rdi), %edx
	addl	%eax, %edx
	movslq	%edx, %rdx
	movl	4(%r8,%rdx,4), %edx
	movl	%edx, (%rsi,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L50
.L45:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$15, (%rcx)
	jmp	.L47
	.cfi_endproc
.LFE3216:
	.size	_ZN6icu_6722RuleBasedBreakIterator16getRuleStatusVecEPiiR10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator16getRuleStatusVecEPiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator16refreshInputTextEP5UTextR10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator16refreshInputTextEP5UTextR10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator16refreshInputTextEP5UTextR10UErrorCode:
.LFB3198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L56
	movq	%rsi, %r13
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	je	.L59
	leaq	328(%rdi), %r14
	movq	%r14, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rbx, %r8
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	utext_clone_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L61
.L56:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	utext_setNativeIndex_67@PLT
	movq	%r14, %rdi
	call	utext_getNativeIndex_67@PLT
	cmpq	%rax, %r15
	je	.L56
.L59:
	movl	$1, (%rbx)
	jmp	.L56
	.cfi_endproc
.LFE3198:
	.size	_ZN6icu_6722RuleBasedBreakIterator16refreshInputTextEP5UTextR10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator16refreshInputTextEP5UTextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722RuleBasedBreakIteratoreqERKNS_13BreakIteratorE
	.type	_ZNK6icu_6722RuleBasedBreakIteratoreqERKNS_13BreakIteratorE, @function
_ZNK6icu_6722RuleBasedBreakIteratoreqERKNS_13BreakIteratorE:
.LFB3191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L63
	cmpb	$42, (%rdi)
	je	.L66
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L63
.L66:
	xorl	%eax, %eax
.L62:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	cmpq	%r12, %rbx
	je	.L68
	leaq	328(%r12), %rsi
	leaq	328(%rbx), %rdi
	call	utext_equals_67@PLT
	testb	%al, %al
	je	.L66
	movq	480(%r12), %rax
	cmpq	%rax, 480(%rbx)
	jne	.L66
	movzbl	632(%r12), %eax
	cmpb	%al, 632(%rbx)
	jne	.L66
	movq	472(%r12), %rdi
	movq	472(%rbx), %rsi
	cmpq	%rsi, %rdi
	je	.L68
	testq	%rdi, %rdi
	je	.L66
	testq	%rsi, %rsi
	je	.L66
	call	_ZNK6icu_6715RBBIDataWrappereqERKS0_@PLT
	testb	%al, %al
	setne	%al
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L68:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3191:
	.size	_ZNK6icu_6722RuleBasedBreakIteratoreqERKNS_13BreakIteratorE, .-_ZNK6icu_6722RuleBasedBreakIteratoreqERKNS_13BreakIteratorE
	.align 2
	.p2align 4
	.type	_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_.part.0, @function
_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_.part.0:
.LFB4281:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-44(%rbp), %r8
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	addq	$328, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$328, %rdi
	leaq	536(%rbx), %r13
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	utext_clone_67@PLT
	movq	528(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L80
	cmpq	%r13, %rdi
	je	.L80
	movq	(%rdi), %rax
	call	*8(%rax)
.L80:
	movq	%r13, 528(%rbx)
	movq	528(%r12), %rdi
	leaq	536(%r12), %r14
	testq	%rdi, %rdi
	je	.L81
	cmpq	%r14, %rdi
	je	.L81
	movq	(%rdi), %rax
	call	*64(%rax)
	movq	%rax, 528(%rbx)
.L81:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6723StringCharacterIteratoraSERKS0_@PLT
	cmpq	$0, 528(%rbx)
	je	.L102
.L82:
	movq	472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L83
	call	_ZN6icu_6715RBBIDataWrapper15removeReferenceEv@PLT
	movq	$0, 472(%rbx)
.L83:
	movq	472(%r12), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZN6icu_6715RBBIDataWrapper12addReferenceEv@PLT
	movq	%rax, 472(%rbx)
.L84:
	movl	480(%r12), %esi
	movq	488(%rbx), %rdi
	movl	%esi, 480(%rbx)
	movl	484(%r12), %edx
	movl	%edx, 484(%rbx)
	movzbl	632(%r12), %eax
	movb	%al, 632(%rbx)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache5resetEii@PLT
	movq	496(%rbx), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache5resetEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	%r13, 528(%rbx)
	jmp	.L82
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4281:
	.size	_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_.part.0, .-_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator4nextEi
	.type	_ZN6icu_6722RuleBasedBreakIterator4nextEi, @function
_ZN6icu_6722RuleBasedBreakIterator4nextEi:
.LFB3201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	testl	%esi, %esi
	jle	.L105
	leaq	_ZN6icu_6722RuleBasedBreakIterator4nextEv(%rip), %r13
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L124:
	movq	488(%r12), %rdi
	movl	28(%rdi), %eax
	cmpl	20(%rdi), %eax
	je	.L123
	addl	$1, %eax
	movq	8(%rdi), %rdx
	andl	$127, %eax
	movl	%eax, 28(%rdi)
	cltq
	movl	32(%rdi,%rax,4), %eax
	movl	%eax, 480(%rdx)
	movl	%eax, 24(%rdi)
	movslq	28(%rdi), %rax
	movzwl	544(%rdi,%rax,2), %eax
	movl	%eax, 484(%rdx)
.L108:
	cmpb	$0, 632(%r12)
	jne	.L121
	subl	$1, %ebx
	movl	480(%r12), %eax
	testl	%ebx, %ebx
	jle	.L104
.L125:
	cmpl	$-1, %eax
	je	.L104
	movq	(%r12), %rax
.L111:
	movq	104(%rax), %rax
	cmpq	%r13, %rax
	je	.L124
	subl	$1, %ebx
	movq	%r12, %rdi
	call	*%rax
	testl	%ebx, %ebx
	jg	.L125
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L126
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache6nextOLEv@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L105:
	je	.L112
	leaq	_ZN6icu_6722RuleBasedBreakIterator8previousEv(%rip), %r14
	leaq	-60(%rbp), %r15
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L127:
	movq	488(%r12), %rdi
	movq	%r15, %rsi
	movl	$0, -60(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache8previousER10UErrorCode@PLT
	cmpb	$0, 632(%r12)
	jne	.L121
	movl	480(%r12), %eax
	cmpl	$-1, %eax
	sete	%dl
	orl	%edx, %r13d
.L116:
	addl	$1, %ebx
	testb	%r13b, %r13b
	jne	.L104
	movq	(%r12), %rax
.L118:
	movq	96(%rax), %rax
	cmpl	$-1, %ebx
	setge	%r13b
	cmpq	%r14, %rax
	je	.L127
	movq	%r12, %rdi
	call	*%rax
	cmpl	$-1, %eax
	sete	%dl
	orl	%edx, %r13d
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L112:
	movq	112(%rax), %rax
	leaq	_ZNK6icu_6722RuleBasedBreakIterator7currentEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L119
	movl	480(%rdi), %eax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$-1, %eax
	jmp	.L104
.L119:
	call	*%rax
	jmp	.L104
.L126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3201:
	.size	_ZN6icu_6722RuleBasedBreakIterator4nextEi, .-_ZN6icu_6722RuleBasedBreakIterator4nextEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator9precedingEi
	.type	_ZN6icu_6722RuleBasedBreakIterator9precedingEi, @function
_ZN6icu_6722RuleBasedBreakIterator9precedingEi:
.LFB3205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	328(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movslq	%esi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	utext_nativeLength_67@PLT
	cmpq	%rax, %r13
	jle	.L129
	movq	(%r12), %rax
	leaq	_ZN6icu_6722RuleBasedBreakIterator4lastEv(%rip), %rdx
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L130
	movq	%r14, %rdi
	call	utext_nativeLength_67@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	%eax, %r13d
	movq	(%r12), %rax
	call	*136(%rax)
.L128:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	utext_setNativeIndex_67@PLT
	movq	%r14, %rdi
	call	utext_getNativeIndex_67@PLT
	leaq	-44(%rbp), %rdx
	movq	488(%r12), %rdi
	movl	$0, -44(%rbp)
	movq	%rax, %rsi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9precedingEiR10UErrorCode@PLT
	cmpb	$0, 632(%r12)
	jne	.L133
	movl	480(%r12), %r13d
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r13d
	jmp	.L128
.L133:
	movl	$-1, %r13d
	jmp	.L128
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3205:
	.size	_ZN6icu_6722RuleBasedBreakIterator9precedingEi, .-_ZN6icu_6722RuleBasedBreakIterator9precedingEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722RuleBasedBreakIterator8getRulesEv
	.type	_ZNK6icu_6722RuleBasedBreakIterator8getRulesEv, @function
_ZNK6icu_6722RuleBasedBreakIterator8getRulesEv:
.LFB3227:
	.cfi_startproc
	endbr64
	movq	472(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L137
	jmp	_ZNK6icu_6715RBBIDataWrapper19getRuleSourceStringEv@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	movl	_ZL13gRBBIInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L149
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL13gRBBIInitOnce(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L139
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L141
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rdx, (%rax)
	movl	$2, %edx
	movw	%dx, 8(%rax)
.L141:
	movl	$3, %edi
	leaq	rbbi_cleanup_67(%rip), %rsi
	movq	%rax, _ZL12gEmptyString(%rip)
	call	ucln_common_registerCleanup_67@PLT
	leaq	_ZL13gRBBIInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L139:
	movq	_ZL12gEmptyString(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore 6
	movq	_ZL12gEmptyString(%rip), %rax
	ret
	.cfi_endproc
.LFE3227:
	.size	_ZNK6icu_6722RuleBasedBreakIterator8getRulesEv, .-_ZNK6icu_6722RuleBasedBreakIterator8getRulesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator9followingEi
	.type	_ZN6icu_6722RuleBasedBreakIterator9followingEi, @function
_ZN6icu_6722RuleBasedBreakIterator9followingEi:
.LFB3204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jns	.L153
	movq	(%rdi), %rax
	leaq	_ZN6icu_6722RuleBasedBreakIterator5firstEv(%rip), %rdx
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L154
	movq	488(%rdi), %rdi
	xorl	%esi, %esi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi@PLT
	testb	%al, %al
	je	.L161
.L155:
	movq	488(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv@PLT
	xorl	%eax, %eax
.L152:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L162
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	leaq	328(%rdi), %r13
	movslq	%esi, %rsi
	movq	%r13, %rdi
	call	utext_setNativeIndex_67@PLT
	movq	%r13, %rdi
	call	utext_getNativeIndex_67@PLT
	leaq	-28(%rbp), %rdx
	movq	488(%r12), %rdi
	movl	$0, -28(%rbp)
	movq	%rax, %rsi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9followingEiR10UErrorCode@PLT
	cmpb	$0, 632(%r12)
	jne	.L159
	movl	480(%r12), %eax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L154:
	call	*%rax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L161:
	movq	488(%r12), %rdi
	leaq	-28(%rbp), %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode@PLT
	jmp	.L155
.L159:
	movl	$-1, %eax
	jmp	.L152
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3204:
	.size	_ZN6icu_6722RuleBasedBreakIterator9followingEi, .-_ZN6icu_6722RuleBasedBreakIterator9followingEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator7setTextERKNS_13UnicodeStringE
	.type	_ZN6icu_6722RuleBasedBreakIterator7setTextERKNS_13UnicodeStringE, @function
_ZN6icu_6722RuleBasedBreakIterator7setTextERKNS_13UnicodeStringE:
.LFB3197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	536(%r12), %rbx
	subq	$24, %rsp
	movq	488(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -48(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache5resetEii@PLT
	movq	496(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache5resetEv@PLT
	leaq	-48(%rbp), %rdx
	leaq	328(%r12), %rdi
	movq	%r13, %rsi
	call	utext_openConstUnicodeString_67@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6723StringCharacterIterator7setTextERKNS_13UnicodeStringE@PLT
	movq	528(%r12), %rdi
	testq	%rdi, %rdi
	je	.L164
	cmpq	%rdi, %rbx
	je	.L164
	movq	(%rdi), %rax
	call	*8(%rax)
.L164:
	movq	(%r12), %rax
	leaq	_ZN6icu_6722RuleBasedBreakIterator5firstEv(%rip), %rdx
	movq	%rbx, 528(%r12)
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L165
	movq	488(%r12), %rdi
	xorl	%esi, %esi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi@PLT
	testb	%al, %al
	je	.L176
.L166:
	movq	488(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv@PLT
.L163:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	488(%r12), %rdi
	leaq	-44(%rbp), %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode@PLT
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L163
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3197:
	.size	_ZN6icu_6722RuleBasedBreakIterator7setTextERKNS_13UnicodeStringE, .-_ZN6icu_6722RuleBasedBreakIterator7setTextERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator9adoptTextEPNS_17CharacterIteratorE
	.type	_ZN6icu_6722RuleBasedBreakIterator9adoptTextEPNS_17CharacterIteratorE, @function
_ZN6icu_6722RuleBasedBreakIterator9adoptTextEPNS_17CharacterIteratorE:
.LFB3196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	528(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	536(%r12), %rax
	cmpq	%rax, %rdi
	je	.L179
	testq	%rdi, %rdi
	je	.L179
	movq	(%rdi), %rax
	call	*8(%rax)
.L179:
	movq	%r13, 528(%r12)
	movq	488(%r12), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$0, -32(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache5resetEii@PLT
	movq	496(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache5resetEv@PLT
	leaq	328(%r12), %rdi
	testq	%r13, %r13
	je	.L180
	movl	16(%r13), %eax
	testl	%eax, %eax
	je	.L181
.L180:
	leaq	-32(%rbp), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	utext_openUChars_67@PLT
.L182:
	movq	(%r12), %rax
	leaq	_ZN6icu_6722RuleBasedBreakIterator5firstEv(%rip), %rdx
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L183
	movq	488(%r12), %rdi
	xorl	%esi, %esi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi@PLT
	testb	%al, %al
	je	.L197
.L184:
	movq	488(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv@PLT
.L178:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	movq	%r13, %rsi
	call	utext_openCharacterIterator_67@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L197:
	movq	488(%r12), %rdi
	leaq	-28(%rbp), %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L178
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3196:
	.size	_ZN6icu_6722RuleBasedBreakIterator9adoptTextEPNS_17CharacterIteratorE, .-_ZN6icu_6722RuleBasedBreakIterator9adoptTextEPNS_17CharacterIteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator7setTextEP5UTextR10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator7setTextEP5UTextR10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator7setTextEP5UTextR10UErrorCode:
.LFB3193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L212
.L199:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L213
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	488(%rdi), %rdi
	movq	%rsi, %r13
	movq	%rdx, %rbx
	xorl	%esi, %esi
	xorl	%edx, %edx
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache5resetEii@PLT
	movq	496(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCache5resetEv@PLT
	movq	%rbx, %r8
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	leaq	328(%r12), %rdi
	leaq	-112(%rbp), %r13
	call	utext_clone_67@PLT
	leaq	536(%r12), %rbx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r13, %rsi
	movq	%rax, -112(%rbp)
	movq	%rbx, %rdi
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZN6icu_6723StringCharacterIterator7setTextERKNS_13UnicodeStringE@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	528(%r12), %rdi
	cmpq	%rdi, %rbx
	je	.L201
	testq	%rdi, %rdi
	je	.L201
	movq	(%rdi), %rax
	call	*8(%rax)
.L201:
	movq	(%r12), %rax
	leaq	_ZN6icu_6722RuleBasedBreakIterator5firstEv(%rip), %rdx
	movq	%rbx, 528(%r12)
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L202
	movq	488(%r12), %rdi
	xorl	%esi, %esi
	movl	$0, -116(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi@PLT
	testb	%al, %al
	je	.L214
.L203:
	movq	488(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L214:
	movq	488(%r12), %rdi
	leaq	-116(%rbp), %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L199
.L213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3193:
	.size	_ZN6icu_6722RuleBasedBreakIterator7setTextEP5UTextR10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator7setTextEP5UTextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10isBoundaryEi
	.type	_ZN6icu_6722RuleBasedBreakIterator10isBoundaryEi, @function
_ZN6icu_6722RuleBasedBreakIterator10isBoundaryEi:
.LFB3206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L239
	leaq	328(%rdi), %r14
	movslq	%esi, %r15
	movl	%esi, %ebx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	utext_setNativeIndex_67@PLT
	movq	%r14, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	488(%r12), %rdi
	movl	$0, -60(%rbp)
	movl	%eax, %esi
	movq	%rax, %r13
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi@PLT
	testb	%al, %al
	je	.L220
.L224:
	movq	488(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv@PLT
	movl	%eax, %edx
	cmpl	%eax, %ebx
	jne	.L223
	cmpl	%r13d, %ebx
	jle	.L223
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	utext_char32At_67@PLT
	cmpl	$-1, %eax
	setne	%al
	.p2align 4,,10
	.p2align 3
.L215:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L240
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	movq	488(%r12), %rdi
	leaq	-60(%rbp), %rdx
	movl	%r13d, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L224
.L225:
	movq	(%r12), %rax
	leaq	_ZN6icu_6722RuleBasedBreakIterator4nextEv(%rip), %rdx
	movq	104(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L228
	movq	488(%r12), %rdi
	movl	28(%rdi), %eax
	cmpl	20(%rdi), %eax
	je	.L241
	addl	$1, %eax
	movq	8(%rdi), %rdx
	andl	$127, %eax
	movl	%eax, 28(%rdi)
	cltq
	movl	32(%rdi,%rax,4), %eax
	movl	%eax, 480(%rdx)
	movl	%eax, 24(%rdi)
	movslq	28(%rdi), %rax
	movzwl	544(%rdi,%rax,2), %eax
	movl	%eax, 484(%rdx)
	xorl	%eax, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L223:
	movl	$1, %eax
	cmpl	%edx, %ebx
	je	.L215
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L239:
	movq	(%rdi), %rax
	leaq	_ZN6icu_6722RuleBasedBreakIterator5firstEv(%rip), %rdx
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L228
	movq	488(%rdi), %rdi
	xorl	%esi, %esi
	movl	$0, -60(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi@PLT
	testb	%al, %al
	je	.L242
.L218:
	movq	488(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv@PLT
	xorl	%eax, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%r12, %rdi
	call	*%rax
	xorl	%eax, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L242:
	movq	488(%r12), %rdi
	leaq	-60(%rbp), %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L241:
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache6nextOLEv@PLT
	xorl	%eax, %eax
	jmp	.L215
.L240:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3206:
	.size	_ZN6icu_6722RuleBasedBreakIterator10isBoundaryEi, .-_ZN6icu_6722RuleBasedBreakIterator10isBoundaryEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator4lastEv
	.type	_ZN6icu_6722RuleBasedBreakIterator4lastEv, @function
_ZN6icu_6722RuleBasedBreakIterator4lastEv:
.LFB3200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	328(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	utext_nativeLength_67@PLT
	leaq	_ZN6icu_6722RuleBasedBreakIterator10isBoundaryEi(%rip), %rcx
	movq	%rax, %rbx
	movq	(%r12), %rax
	movq	136(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L244
	testl	%ebx, %ebx
	js	.L266
	movslq	%ebx, %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	utext_setNativeIndex_67@PLT
	movq	%r13, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	488(%r12), %rdi
	movl	$0, -60(%rbp)
	movl	%eax, %esi
	movq	%rax, %r14
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi@PLT
	testb	%al, %al
	jne	.L253
	movq	488(%r12), %rdi
	leaq	-60(%rbp), %rdx
	movl	%r14d, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L258
.L253:
	movq	488(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv@PLT
	cmpl	%ebx, %eax
	jne	.L252
	cmpl	%r14d, %ebx
	jle	.L252
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	utext_char32At_67@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L266:
	movq	80(%rax), %rax
	leaq	_ZN6icu_6722RuleBasedBreakIterator5firstEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L264
	movq	488(%r12), %rdi
	xorl	%esi, %esi
	movl	$0, -60(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache4seekEi@PLT
	testb	%al, %al
	je	.L267
.L247:
	movq	488(%r12), %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache7currentEv@PLT
.L243:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L268
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	cmpl	%eax, %ebx
	je	.L243
.L258:
	movq	(%r12), %rax
	leaq	_ZN6icu_6722RuleBasedBreakIterator4nextEv(%rip), %rdx
	movq	104(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L264
	movq	488(%r12), %rdi
	movl	28(%rdi), %eax
	cmpl	20(%rdi), %eax
	je	.L269
	addl	$1, %eax
	movq	8(%rdi), %rdx
	andl	$127, %eax
	movl	%eax, 28(%rdi)
	cltq
	movl	32(%rdi,%rax,4), %eax
	movl	%eax, 480(%rdx)
	movl	%eax, 24(%rdi)
	movslq	28(%rdi), %rax
	movzwl	544(%rdi,%rax,2), %eax
	movl	%eax, 484(%rdx)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L244:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L267:
	movq	488(%r12), %rdi
	leaq	-60(%rbp), %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache12populateNearEiR10UErrorCode@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L264:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L269:
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCache6nextOLEv@PLT
	jmp	.L243
.L268:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3200:
	.size	_ZN6icu_6722RuleBasedBreakIterator4lastEv, .-_ZN6icu_6722RuleBasedBreakIterator4lastEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator16getStaticClassIDEv
	.type	_ZN6icu_6722RuleBasedBreakIterator16getStaticClassIDEv, @function
_ZN6icu_6722RuleBasedBreakIterator16getStaticClassIDEv:
.LFB3164:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6722RuleBasedBreakIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3164:
	.size	_ZN6icu_6722RuleBasedBreakIterator16getStaticClassIDEv, .-_ZN6icu_6722RuleBasedBreakIterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC2EPKhjR10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIteratorC2EPKhjR10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIteratorC2EPKhjR10UErrorCode:
.LFB3170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713BreakIteratorC2Ev@PLT
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIteratorE(%rip), %rax
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%rax, (%rbx)
	leaq	536(%rbx), %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movw	%dx, -120(%rbp)
	call	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	328(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movdqa	_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm1
	movdqa	16+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm2
	movq	$0, 528(%rbx)
	movups	%xmm1, 328(%rbx)
	movdqa	32+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm3
	movdqa	48+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm4
	movb	$0, 632(%rbx)
	movdqa	64+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm5
	movups	%xmm0, 488(%rbx)
	movdqa	80+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm6
	movdqa	96+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm7
	movups	%xmm0, 504(%rbx)
	movdqa	112+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm1
	movq	$0, 472(%rbx)
	movq	$0, 480(%rbx)
	movl	$0, 520(%rbx)
	movups	%xmm2, 16(%rdi)
	movdqa	128+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm2
	movups	%xmm3, 32(%rdi)
	movups	%xmm4, 48(%rdi)
	movups	%xmm5, 64(%rdi)
	movups	%xmm6, 80(%rdi)
	movups	%xmm7, 96(%rdi)
	movups	%xmm1, 112(%rdi)
	movups	%xmm2, 128(%rdi)
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L290
.L271:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	movq	%r12, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	utext_openUChars_67@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L273
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC1EPS0_R10UErrorCode@PLT
.L273:
	movq	%r15, 496(%rbx)
	movl	$832, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L274
	movq	%rax, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC1EPS0_R10UErrorCode@PLT
	movl	(%r12), %eax
	movq	%r15, 488(%rbx)
	testl	%eax, %eax
	jg	.L271
	cmpq	$0, 496(%rbx)
	je	.L287
	testq	%r13, %r13
	je	.L278
	cmpl	$79, %r14d
	jbe	.L278
	cmpl	%r14d, 8(%r13)
	ja	.L278
	movl	$144, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L279
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715RBBIDataWrapperC1EPKNS_14RBBIDataHeaderENS0_10EDontAdoptER10UErrorCode@PLT
	movq	%r14, 472(%rbx)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L278:
	movl	$1, (%r12)
	jmp	.L271
.L279:
	movq	$0, 472(%rbx)
	cmpl	$0, (%r12)
	jg	.L271
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$7, (%r12)
	jmp	.L271
.L291:
	call	__stack_chk_fail@PLT
.L274:
	movq	$0, 488(%rbx)
	cmpl	$0, (%r12)
	jg	.L271
	jmp	.L287
	.cfi_endproc
.LFE3170:
	.size	_ZN6icu_6722RuleBasedBreakIteratorC2EPKhjR10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIteratorC2EPKhjR10UErrorCode
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC1EPKhjR10UErrorCode
	.set	_ZN6icu_6722RuleBasedBreakIteratorC1EPKhjR10UErrorCode,_ZN6icu_6722RuleBasedBreakIteratorC2EPKhjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC2Ev
	.type	_ZN6icu_6722RuleBasedBreakIteratorC2Ev, @function
_ZN6icu_6722RuleBasedBreakIteratorC2Ev:
.LFB3179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r12
	leaq	-116(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713BreakIteratorC2Ev@PLT
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIteratorE(%rip), %rax
	movq	%r12, %rsi
	leaq	536(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	328(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movdqa	_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm1
	pxor	%xmm0, %xmm0
	movdqa	16+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm2
	movq	$0, 528(%rbx)
	movups	%xmm0, 488(%rbx)
	movq	%r13, %rcx
	movdqa	32+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm3
	movdqa	48+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm4
	movups	%xmm0, 504(%rbx)
	movdqa	64+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm5
	movdqa	80+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm6
	movups	%xmm1, 328(%rbx)
	movdqa	96+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm7
	movdqa	112+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm1
	movb	$0, 632(%rbx)
	movq	$0, 472(%rbx)
	movq	$0, 480(%rbx)
	movl	$0, 520(%rbx)
	movups	%xmm2, 16(%rdi)
	movdqa	128+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm2
	movups	%xmm3, 32(%rdi)
	movups	%xmm4, 48(%rdi)
	movups	%xmm5, 64(%rdi)
	movups	%xmm6, 80(%rdi)
	movups	%xmm7, 96(%rdi)
	movups	%xmm1, 112(%rdi)
	movups	%xmm2, 128(%rdi)
	movl	$0, -116(%rbp)
	call	utext_openUChars_67@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L293
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC1EPS0_R10UErrorCode@PLT
.L293:
	movq	%r12, 496(%rbx)
	movl	$832, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L294
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC1EPS0_R10UErrorCode@PLT
.L294:
	movq	%r12, 488(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L303:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3179:
	.size	_ZN6icu_6722RuleBasedBreakIteratorC2Ev, .-_ZN6icu_6722RuleBasedBreakIteratorC2Ev
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC1Ev
	.set	_ZN6icu_6722RuleBasedBreakIteratorC1Ev,_ZN6icu_6722RuleBasedBreakIteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_
	.type	_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_, @function
_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_:
.LFB3188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	cmpq	%rsi, %rdi
	je	.L305
	movq	%rsi, %r13
	call	_ZN6icu_6713BreakIteratoraSERKS0_@PLT
	movq	504(%r12), %rdi
	testq	%rdi, %rdi
	je	.L306
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 504(%r12)
.L306:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_.part.0
.L305:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3188:
	.size	_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_, .-_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator4initER10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator4initER10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator4initER10UErrorCode:
.LFB3189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$328, %rdi
	subq	$8, %rsp
	movb	$0, 304(%rdi)
	movdqa	_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm1
	movq	$0, 200(%rdi)
	movdqa	16+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm2
	movups	%xmm0, 160(%rdi)
	movdqa	32+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm3
	movdqa	48+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm4
	movups	%xmm0, 176(%rdi)
	movdqa	64+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm5
	movdqa	80+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm6
	movq	$0, 144(%rdi)
	movdqa	96+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm7
	movq	$0, 152(%rdi)
	movl	$0, 192(%rdi)
	movups	%xmm1, 328(%rbx)
	movdqa	112+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm1
	movups	%xmm2, 16(%rdi)
	movdqa	128+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm2
	movups	%xmm3, 32(%rdi)
	movups	%xmm4, 48(%rdi)
	movups	%xmm5, 64(%rdi)
	movups	%xmm6, 80(%rdi)
	movups	%xmm7, 96(%rdi)
	movups	%xmm1, 112(%rdi)
	movups	%xmm2, 128(%rdi)
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L322
.L311:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	%rsi, %r12
	xorl	%esi, %esi
	call	utext_openUChars_67@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L314
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC1EPS0_R10UErrorCode@PLT
.L314:
	movq	%r13, 496(%rbx)
	movl	$832, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L315
	movq	%rax, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC1EPS0_R10UErrorCode@PLT
	movl	(%r12), %eax
	movq	%r13, 488(%rbx)
	testl	%eax, %eax
	jg	.L311
	cmpq	$0, 496(%rbx)
	jne	.L311
.L317:
	movl	$7, (%r12)
	jmp	.L311
.L315:
	movq	$0, 488(%rbx)
	cmpl	$0, (%r12)
	jg	.L311
	jmp	.L317
	.cfi_endproc
.LFE3189:
	.size	_ZN6icu_6722RuleBasedBreakIterator4initER10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator4initER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC2EPNS_14RBBIDataHeaderER10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIteratorC2EPNS_14RBBIDataHeaderER10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIteratorC2EPNS_14RBBIDataHeaderER10UErrorCode:
.LFB3167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713BreakIteratorC2Ev@PLT
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIteratorE(%rip), %rax
	movq	%r12, %rsi
	leaq	536(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6722RuleBasedBreakIterator4initER10UErrorCode
	movl	$144, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L324
	movq	%rax, %r12
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715RBBIDataWrapperC1EPKNS_14RBBIDataHeaderER10UErrorCode@PLT
	movq	%r12, 472(%rbx)
.L323:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L330
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L324:
	.cfi_restore_state
	movq	$0, 472(%rbx)
	cmpl	$0, 0(%r13)
	jg	.L323
	movl	$7, 0(%r13)
	jmp	.L323
.L330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3167:
	.size	_ZN6icu_6722RuleBasedBreakIteratorC2EPNS_14RBBIDataHeaderER10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIteratorC2EPNS_14RBBIDataHeaderER10UErrorCode
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC1EPNS_14RBBIDataHeaderER10UErrorCode
	.set	_ZN6icu_6722RuleBasedBreakIteratorC1EPNS_14RBBIDataHeaderER10UErrorCode,_ZN6icu_6722RuleBasedBreakIteratorC2EPNS_14RBBIDataHeaderER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC2EP11UDataMemoryR10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIteratorC2EP11UDataMemoryR10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIteratorC2EP11UDataMemoryR10UErrorCode:
.LFB3173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713BreakIteratorC2Ev@PLT
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIteratorE(%rip), %rax
	movq	%r12, %rsi
	leaq	536(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6722RuleBasedBreakIterator4initER10UErrorCode
	movl	$144, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L332
	movq	%rax, %r12
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715RBBIDataWrapperC1EP11UDataMemoryR10UErrorCode@PLT
	movq	%r12, 472(%rbx)
.L331:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L332:
	.cfi_restore_state
	movq	$0, 472(%rbx)
	cmpl	$0, 0(%r13)
	jg	.L331
	movl	$7, 0(%r13)
	jmp	.L331
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3173:
	.size	_ZN6icu_6722RuleBasedBreakIteratorC2EP11UDataMemoryR10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIteratorC2EP11UDataMemoryR10UErrorCode
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC1EP11UDataMemoryR10UErrorCode
	.set	_ZN6icu_6722RuleBasedBreakIteratorC1EP11UDataMemoryR10UErrorCode,_ZN6icu_6722RuleBasedBreakIteratorC2EP11UDataMemoryR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC2ERKS0_
	.type	_ZN6icu_6722RuleBasedBreakIteratorC2ERKS0_, @function
_ZN6icu_6722RuleBasedBreakIteratorC2ERKS0_:
.LFB3182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	-112(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIteratorE(%rip), %rax
	movq	%r14, %rsi
	leaq	536(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-116(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -116(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator4initER10UErrorCode
	cmpq	%r13, %r12
	je	.L339
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713BreakIteratoraSERKS0_@PLT
	movq	504(%r12), %rdi
	testq	%rdi, %rdi
	je	.L341
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 504(%r12)
.L341:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_.part.0
.L339:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	addq	$104, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L347:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3182:
	.size	_ZN6icu_6722RuleBasedBreakIteratorC2ERKS0_, .-_ZN6icu_6722RuleBasedBreakIteratorC2ERKS0_
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC1ERKS0_
	.set	_ZN6icu_6722RuleBasedBreakIteratorC1ERKS0_,_ZN6icu_6722RuleBasedBreakIteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722RuleBasedBreakIterator5cloneEv
	.type	_ZNK6icu_6722RuleBasedBreakIterator5cloneEv, @function
_ZNK6icu_6722RuleBasedBreakIterator5cloneEv:
.LFB3190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$640, %edi
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L348
	movq	%rax, %rdi
	movq	%r13, %rsi
	leaq	-112(%rbp), %r14
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIteratorE(%rip), %rax
	movq	%r14, %rsi
	leaq	536(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-116(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -116(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator4initER10UErrorCode
	cmpq	%r13, %r12
	je	.L348
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713BreakIteratoraSERKS0_@PLT
	movq	504(%r12), %rdi
	testq	%rdi, %rdi
	je	.L351
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 504(%r12)
.L351:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_.part.0
.L348:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L360
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L360:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3190:
	.size	_ZNK6icu_6722RuleBasedBreakIterator5cloneEv, .-_ZNK6icu_6722RuleBasedBreakIterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator17createBufferCloneEPvRiR10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIterator17createBufferCloneEPvRiR10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIterator17createBufferCloneEPvRiR10UErrorCode:
.LFB3218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L361
	movl	(%rdx), %esi
	testl	%esi, %esi
	je	.L379
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6722RuleBasedBreakIterator5cloneEv(%rip), %rdx
	movq	%rdi, %r13
	movq	%rcx, %rbx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L364
	movl	$640, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L369
	movq	%rax, %rdi
	movq	%r13, %rsi
	leaq	-112(%rbp), %r14
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIteratorE(%rip), %rax
	movq	%r14, %rsi
	leaq	536(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	-116(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -116(%rbp)
	call	_ZN6icu_6722RuleBasedBreakIterator4initER10UErrorCode
	cmpq	%r12, %r13
	je	.L368
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713BreakIteratoraSERKS0_@PLT
	movq	504(%r12), %rdi
	testq	%rdi, %rdi
	je	.L367
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 504(%r12)
.L367:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_.part.0
.L368:
	movl	$-126, (%rbx)
.L361:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L380
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	call	*%rax
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L368
.L369:
	movl	$7, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L379:
	movl	$1, (%rdx)
	jmp	.L361
.L380:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3218:
	.size	_ZN6icu_6722RuleBasedBreakIterator17createBufferCloneEPvRiR10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIterator17createBufferCloneEPvRiR10UErrorCode
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv
	.type	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv, @function
_ZN6icu_6722RuleBasedBreakIterator10handleNextEv:
.LFB3213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	472(%rdi), %rax
	movl	$0, -112(%rbp)
	movups	%xmm0, -108(%rbp)
	movq	8(%rax), %r14
	movups	%xmm0, -92(%rbp)
	movups	%xmm0, -76(%rbp)
	movl	4(%r14), %eax
	leaq	16(%r14), %r12
	movl	$0, 484(%rdi)
	movl	$0, 520(%rdi)
	movl	%eax, -116(%rbp)
	movslq	480(%rdi), %rax
	movl	%eax, -140(%rbp)
	movq	%rax, %rdx
	movq	%rax, -152(%rbp)
	subq	360(%rdi), %rdx
	js	.L382
	movslq	356(%rdi), %rax
	cmpq	%rdx, %rax
	jg	.L500
.L382:
	movq	-152(%rbp), %rsi
	leaq	328(%r13), %rdi
	call	utext_setNativeIndex_67@PLT
	movl	368(%r13), %eax
.L383:
	cmpl	%eax, 372(%r13)
	jle	.L384
	movq	376(%r13), %rsi
	movslq	%eax, %rdx
	movzwl	(%rsi,%rdx,2), %r8d
	cmpw	$-10241, %r8w
	jbe	.L501
.L384:
	leaq	328(%r13), %rdi
	call	utext_next32_67@PLT
	movl	%eax, %r8d
	cmpl	$-1, %eax
	je	.L502
.L385:
	movl	8(%r14), %eax
	movl	-116(%rbp), %ebx
	xorl	%esi, %esi
	leaq	328(%r13), %r15
	movq	%r13, %r14
	movq	%r15, %r13
	andl	$2, %eax
	addq	%r12, %rbx
	cmpl	$1, %eax
	movl	-140(%rbp), %eax
	setb	%sil
	sbbl	%r10d, %r10d
	notl	%r10d
	movl	%eax, -136(%rbp)
	andl	$2, %r10d
	movl	%r10d, %r15d
	cmpl	$-1, %r8d
	je	.L503
	.p2align 4,,10
	.p2align 3
.L388:
	movzwl	%r15w, %eax
	cmpl	$1, %esi
	je	.L416
.L415:
	movzwl	8(%rbx,%rax,2), %ebx
	movl	%ebx, %r9d
	imull	-116(%rbp), %ebx
	addq	%r12, %rbx
	movzwl	(%rbx), %eax
	cmpw	$-1, %ax
	je	.L504
.L391:
	testw	%ax, %ax
	jg	.L505
.L402:
	movzwl	2(%rbx), %edx
	testw	%dx, %dx
	je	.L406
	movl	368(%r14), %eax
	cmpl	356(%r14), %eax
	jg	.L407
	movslq	-112(%rbp), %rdi
	addl	360(%r14), %eax
	testl	%edi, %edi
	jle	.L432
.L508:
	cmpw	-76(%rbp), %dx
	je	.L433
	cmpl	$1, %edi
	je	.L411
	cmpw	-74(%rbp), %dx
	je	.L434
	cmpl	$2, %edi
	je	.L411
	cmpw	-72(%rbp), %dx
	je	.L435
	cmpl	$3, %edi
	je	.L411
	cmpw	-70(%rbp), %dx
	je	.L436
	cmpl	$4, %edi
	je	.L411
	cmpw	-68(%rbp), %dx
	je	.L437
	cmpl	$5, %edi
	je	.L411
	cmpw	-66(%rbp), %dx
	je	.L438
	cmpl	$6, %edi
	je	.L411
	cmpw	-64(%rbp), %dx
	je	.L439
	cmpl	$7, %edi
	je	.L411
	cmpw	-62(%rbp), %dx
	jne	.L403
	movl	$7, %edx
	movl	%eax, -108(%rbp,%rdx,4)
	.p2align 4,,10
	.p2align 3
.L406:
	testw	%r9w, %r9w
	je	.L389
.L509:
	cmpl	$1, %esi
	je	.L506
	testl	%esi, %esi
	movl	$1, %eax
	cmove	%eax, %esi
	cmpl	$-1, %r8d
	jne	.L388
.L503:
	cmpl	$2, %esi
	je	.L389
	movzwl	10(%rbx), %ebx
	movl	$2, %esi
	movl	$1, %r15d
	movl	%ebx, %r9d
	imull	-116(%rbp), %ebx
	addq	%r12, %rbx
	movzwl	(%rbx), %eax
	cmpw	$-1, %ax
	jne	.L391
.L390:
	movl	368(%r14), %eax
	cmpl	356(%r14), %eax
	jg	.L401
.L507:
	addl	360(%r14), %eax
	movl	%eax, -136(%rbp)
	movl	$-1, %eax
.L400:
	movswl	4(%rbx), %edx
	movl	%edx, 484(%r14)
	testw	%ax, %ax
	jle	.L402
.L505:
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	jle	.L403
	cmpw	-76(%rbp), %ax
	je	.L425
	cmpl	$1, %edx
	je	.L403
	cmpw	-74(%rbp), %ax
	je	.L426
	cmpl	$2, %edx
	je	.L403
	cmpw	-72(%rbp), %ax
	je	.L427
	cmpl	$3, %edx
	je	.L403
	cmpw	-70(%rbp), %ax
	je	.L428
	cmpl	$4, %edx
	je	.L403
	cmpw	-68(%rbp), %ax
	je	.L429
	cmpl	$5, %edx
	je	.L403
	cmpw	-66(%rbp), %ax
	je	.L430
	cmpl	$6, %edx
	je	.L403
	cmpw	-64(%rbp), %ax
	je	.L431
	cmpl	$7, %edx
	je	.L403
	cmpw	%ax, -62(%rbp)
	jne	.L403
	movl	$7, %eax
.L404:
	movl	-108(%rbp,%rax,4), %eax
	testl	%eax, %eax
	js	.L402
	movswl	4(%rbx), %edx
	movl	%eax, -136(%rbp)
	movl	%eax, 480(%r14)
	movl	%edx, 484(%r14)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L506:
	movl	368(%r14), %edx
	cmpl	372(%r14), %edx
	jge	.L413
	movq	376(%r14), %rsi
	movslq	%edx, %rax
	movzwl	(%rsi,%rax,2), %r8d
	cmpw	$-10241, %r8w
	ja	.L413
	movq	472(%r14), %rax
	addl	$1, %edx
	movl	%edx, 368(%r14)
	movq	48(%rax), %rax
	movq	(%rax), %rdx
.L414:
	movl	%r8d, %eax
	sarl	$5, %eax
.L496:
	cltq
	movzwl	(%rdx,%rax,2), %esi
	movl	%r8d, %eax
	andl	$31, %eax
	leal	(%rax,%rsi,4), %eax
	cltq
	addq	%rax, %rax
.L394:
	movzwl	(%rdx,%rax), %r15d
	testw	$16384, %r15w
	je	.L499
	addl	$1, 520(%r14)
	andw	$-16385, %r15w
.L499:
	movzwl	%r15w, %eax
	movl	$1, %esi
	movzwl	8(%rbx,%rax,2), %ebx
	movl	%ebx, %r9d
	imull	-116(%rbp), %ebx
	addq	%r12, %rbx
	movzwl	(%rbx), %eax
	cmpw	$-1, %ax
	jne	.L391
	movl	368(%r14), %eax
	cmpl	356(%r14), %eax
	jle	.L507
.L401:
	movq	384(%r14), %rax
	movl	%r8d, -128(%rbp)
	movq	%r13, %rdi
	movl	%esi, -124(%rbp)
	movl	%r9d, -120(%rbp)
	call	*64(%rax)
	movl	-128(%rbp), %r8d
	movl	-124(%rbp), %esi
	movl	%eax, -136(%rbp)
	movl	-120(%rbp), %r9d
	movzwl	(%rbx), %eax
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L407:
	movq	384(%r14), %rax
	movl	%r8d, -132(%rbp)
	movq	%r13, %rdi
	movl	%esi, -128(%rbp)
	movl	%edx, -124(%rbp)
	movl	%r9d, -120(%rbp)
	call	*64(%rax)
	movslq	-112(%rbp), %rdi
	movl	-132(%rbp), %r8d
	movl	-128(%rbp), %esi
	movl	-124(%rbp), %edx
	movl	-120(%rbp), %r9d
	testl	%edi, %edi
	jg	.L508
.L432:
	movl	$1, %r11d
	xorl	%edi, %edi
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L411:
	leal	1(%rdi), %r11d
.L409:
	movw	%dx, -76(%rbp,%rdi,2)
	movl	%eax, -108(%rbp,%rdi,4)
	movl	%r11d, -112(%rbp)
	testw	%r9w, %r9w
	jne	.L509
	.p2align 4,,10
	.p2align 3
.L389:
	movl	-140(%rbp), %ecx
	movq	%r14, %r13
	cmpl	%ecx, -136(%rbp)
	je	.L510
.L418:
	movl	-136(%rbp), %eax
	movl	%eax, 480(%r13)
.L381:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	movl	-136(%rbp), %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movq	%r13, %rdi
	call	utext_next32_67@PLT
	movl	%eax, %r8d
	cmpl	$-1, %eax
	je	.L512
.L416:
	movq	472(%r14), %rax
	movq	48(%rax), %rax
	movq	(%rax), %rdx
	cmpl	$55295, %r8d
	jbe	.L414
	cmpl	$65535, %r8d
	ja	.L395
	cmpl	$56320, %r8d
	movl	$320, %eax
	movl	$0, %esi
	cmovl	%eax, %esi
	movl	%r8d, %eax
	sarl	$5, %eax
	addl	%esi, %eax
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L504:
	testl	%esi, %esi
	je	.L400
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L512:
	movl	$1, %eax
	movl	$2, %esi
	movl	$1, %r15d
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L395:
	cmpl	$1114111, %r8d
	ja	.L513
	cmpl	%r8d, 44(%rax)
	jg	.L398
	movslq	48(%rax), %rax
	addq	%rax, %rax
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L425:
	xorl	%eax, %eax
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L426:
	movl	$1, %eax
	jmp	.L404
.L513:
	movl	24(%rax), %eax
	subl	$-128, %eax
	cltq
	addq	%rax, %rax
	jmp	.L394
.L500:
	movq	376(%rdi), %rax
	cmpw	$-9217, (%rax,%rdx,2)
	ja	.L382
	movl	%edx, 368(%rdi)
	movl	%edx, %eax
	jmp	.L383
.L501:
	addl	$1, %eax
	movl	%eax, 368(%r13)
	jmp	.L385
.L427:
	movl	$2, %eax
	jmp	.L404
.L428:
	movl	$3, %eax
	jmp	.L404
.L433:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L410:
	movl	%eax, -108(%rbp,%rdx,4)
	jmp	.L406
.L429:
	movl	$4, %eax
	jmp	.L404
.L434:
	movl	$1, %edx
	jmp	.L410
.L430:
	movl	$5, %eax
	jmp	.L404
.L435:
	movl	$2, %edx
	jmp	.L410
.L431:
	movl	$6, %eax
	jmp	.L404
.L436:
	movl	$3, %edx
	jmp	.L410
.L437:
	movl	$4, %edx
	jmp	.L410
.L438:
	movl	$5, %edx
	jmp	.L410
.L398:
	movl	%r8d, %eax
	movl	%r8d, %esi
	sarl	$11, %eax
	sarl	$5, %esi
	addl	$2080, %eax
	andl	$63, %esi
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	jmp	.L496
.L439:
	movl	$6, %edx
	jmp	.L410
.L510:
	movq	-152(%rbp), %rsi
	leaq	328(%r14), %r12
	movq	%r12, %rdi
	call	utext_setNativeIndex_67@PLT
	movq	%r12, %rdi
	call	utext_next32_67@PLT
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	movl	$0, 484(%r14)
	movl	%eax, -136(%rbp)
	jmp	.L418
.L502:
	movb	$1, 632(%r13)
	movl	$-1, -136(%rbp)
	jmp	.L381
.L511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv.cold, @function
_ZN6icu_6722RuleBasedBreakIterator10handleNextEv.cold:
.LFSB3213:
.L403:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3213:
	.text
	.size	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv, .-_ZN6icu_6722RuleBasedBreakIterator10handleNextEv
	.section	.text.unlikely
	.size	_ZN6icu_6722RuleBasedBreakIterator10handleNextEv.cold, .-_ZN6icu_6722RuleBasedBreakIterator10handleNextEv.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator18handleSafePreviousEi
	.type	_ZN6icu_6722RuleBasedBreakIterator18handleSafePreviousEi, @function
_ZN6icu_6722RuleBasedBreakIterator18handleSafePreviousEi:
.LFB3214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	472(%rdi), %rax
	movq	16(%rax), %r12
	subq	360(%rdi), %rcx
	js	.L515
	movslq	356(%rdi), %rax
	movq	%rax, %rdx
	cmpq	%rcx, %rax
	jg	.L547
.L515:
	leaq	328(%r15), %rdi
	call	utext_setNativeIndex_67@PLT
	cmpq	$0, 472(%r15)
	je	.L521
	movslq	368(%r15), %rax
	movl	356(%r15), %edx
.L516:
	cmpl	%edx, %eax
	jle	.L548
	movq	384(%r15), %rax
	leaq	328(%r15), %rdi
	call	*64(%rax)
.L520:
	testq	%rax, %rax
	je	.L521
	movl	368(%r15), %edx
	leaq	16(%r12), %r13
	testl	%edx, %edx
	jle	.L522
	movq	376(%r15), %rax
	movslq	%edx, %rcx
	cmpw	$-10241, -2(%rax,%rcx,2)
	ja	.L522
	subl	$1, %edx
	movl	%edx, 368(%r15)
	movslq	%edx, %rcx
	movl	4(%r12), %ebx
	movzwl	(%rax,%rcx,2), %eax
	movq	%rbx, %rsi
	addq	%r13, %rbx
.L523:
	movq	472(%r15), %rcx
	movl	$320, %r14d
	movq	48(%rcx), %rdi
	movq	(%rdi), %rcx
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L549:
	movl	%eax, %r8d
	sarl	$5, %r8d
.L545:
	movslq	%r8d, %r8
	andl	$31, %eax
	movzwl	(%rcx,%r8,2), %r8d
	leal	(%rax,%r8,4), %eax
	cltq
	addq	%rax, %rax
.L528:
	movzwl	(%rcx,%rax), %eax
	andl	$49151, %eax
	movzwl	8(%rbx,%rax,2), %ebx
	imull	%ebx, %esi
	movl	%ebx, %eax
	leaq	0(%r13,%rsi), %rbx
	testw	%ax, %ax
	je	.L525
	testl	%edx, %edx
	jle	.L533
	movq	376(%r15), %rax
	movslq	%edx, %rsi
	cmpw	$-10241, -2(%rax,%rsi,2)
	ja	.L533
	subl	$1, %edx
	movslq	%edx, %rsi
	movl	%edx, 368(%r15)
	movzwl	(%rax,%rsi,2), %eax
.L534:
	movl	4(%r12), %esi
.L526:
	cmpl	$55295, %eax
	jbe	.L549
	cmpl	$65535, %eax
	ja	.L529
	movl	$0, %r8d
	cmpl	$56320, %eax
	movl	%r8d, %r9d
	movl	%eax, %r8d
	cmovl	%r14d, %r9d
	sarl	$5, %r8d
.L546:
	addl	%r9d, %r8d
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L548:
	addq	360(%r15), %rax
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L547:
	movq	376(%rdi), %rax
	cmpw	$-9217, (%rax,%rcx,2)
	ja	.L515
	movl	%ecx, 368(%rdi)
	movslq	%ecx, %rax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L533:
	leaq	328(%r15), %rdi
	call	utext_previous32_67@PLT
	cmpl	$-1, %eax
	je	.L550
	movq	472(%r15), %rdx
	movq	48(%rdx), %rdi
	movl	368(%r15), %edx
	movq	(%rdi), %rcx
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L529:
	cmpl	$1114111, %eax
	jbe	.L531
	movl	24(%rdi), %eax
	subl	$-128, %eax
	cltq
	addq	%rax, %rax
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L531:
	cmpl	%eax, 44(%rdi)
	jg	.L532
	movslq	48(%rdi), %rax
	addq	%rax, %rax
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L550:
	movl	368(%r15), %edx
.L525:
	cmpl	%edx, 356(%r15)
	jl	.L536
	movl	360(%r15), %eax
	addl	%edx, %eax
.L514:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	leaq	328(%r15), %rdi
	call	utext_previous32_67@PLT
	movl	4(%r12), %ebx
	movl	368(%r15), %edx
	movq	%rbx, %rsi
	addq	%r13, %rbx
	cmpl	$-1, %eax
	jne	.L523
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L532:
	movl	%eax, %r8d
	sarl	$11, %r8d
	addl	$2080, %r8d
	movslq	%r8d, %r8
	movzwl	(%rcx,%r8,2), %r9d
	movl	%eax, %r8d
	sarl	$5, %r8d
	andl	$63, %r8d
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L536:
	movq	384(%r15), %rax
	leaq	328(%r15), %rdi
	call	*64(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L521:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L514
	.cfi_endproc
.LFE3214:
	.size	_ZN6icu_6722RuleBasedBreakIterator18handleSafePreviousEi, .-_ZN6icu_6722RuleBasedBreakIterator18handleSafePreviousEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator22getLanguageBreakEngineEi
	.type	_ZN6icu_6722RuleBasedBreakIterator22getLanguageBreakEngineEi, @function
_ZN6icu_6722RuleBasedBreakIterator22getLanguageBreakEngineEi:
.LFB3224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	504(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -64(%rbp)
	testq	%r12, %r12
	je	.L590
.L552:
	movl	8(%r12), %ebx
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L591:
	movq	504(%r14), %rdi
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r13d, %esi
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testb	%al, %al
	jne	.L551
.L556:
	subl	$1, %ebx
	jns	.L591
	movl	_ZL31gLanguageBreakFactoriesInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L558
	leaq	_ZL31gLanguageBreakFactoriesInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	jne	.L592
.L558:
	movq	_ZL23gLanguageBreakFactories(%rip), %rax
	testq	%rax, %rax
	je	.L563
	movl	8(%rax), %ebx
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L594:
	movq	_ZL23gLanguageBreakFactories(%rip), %rdi
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L593
.L565:
	subl	$1, %ebx
	jns	.L594
.L563:
	movq	512(%r14), %r12
	testq	%r12, %r12
	je	.L595
.L566:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	*32(%rax)
	movq	512(%r14), %r12
.L551:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L596
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_restore_state
	movl	$40, %edi
	movl	$0, -60(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L560
	leaq	-60(%rbp), %r12
	xorl	%edx, %edx
	leaq	_deleteFactory(%rip), %rsi
	movq	%rax, %rdi
	movq	%r12, %rcx
	call	_ZN6icu_676UStackC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	-60(%rbp), %edx
	movq	%rbx, _ZL23gLanguageBreakFactories(%rip)
	testl	%edx, %edx
	jle	.L597
.L561:
	leaq	rbbi_cleanup_67(%rip), %rsi
	movl	$3, %edi
	call	ucln_common_registerCleanup_67@PLT
	leaq	_ZL31gLanguageBreakFactoriesInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L593:
	movq	504(%r14), %rdi
	leaq	-64(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L590:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L553
	leaq	-64(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676UStackC1ER10UErrorCode@PLT
	movl	-64(%rbp), %ecx
	movq	%r15, 504(%r14)
	testl	%ecx, %ecx
	jg	.L598
	movq	%r15, %r12
	jmp	.L552
.L595:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L567
	leaq	-64(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN6icu_6715UnhandledEngineC1ER10UErrorCode@PLT
	movq	%r15, 512(%r14)
	movq	-72(%rbp), %rsi
.L570:
	movq	504(%r14), %rdi
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode@PLT
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	jg	.L568
	movq	512(%r14), %r12
	jmp	.L566
.L568:
	movq	512(%r14), %rdi
	testq	%rdi, %rdi
	je	.L569
	movq	(%rdi), %rax
	call	*8(%rax)
.L569:
	movq	$0, 512(%r14)
	jmp	.L551
.L597:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L562
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6723ICULanguageBreakFactoryC1ER10UErrorCode@PLT
.L562:
	movq	_ZL23gLanguageBreakFactories(%rip), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L561
.L598:
	movq	%r15, %rdi
	call	_ZN6icu_676UStackD0Ev@PLT
.L553:
	movq	$0, 504(%r14)
	jmp	.L551
.L567:
	cmpl	$0, -64(%rbp)
	leaq	-64(%rbp), %rsi
	movq	$0, 512(%r14)
	jle	.L551
	jmp	.L570
.L560:
	movq	$0, _ZL23gLanguageBreakFactories(%rip)
	jmp	.L561
.L596:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3224:
	.size	_ZN6icu_6722RuleBasedBreakIterator22getLanguageBreakEngineEi, .-_ZN6icu_6722RuleBasedBreakIterator22getLanguageBreakEngineEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator9dumpCacheEv
	.type	_ZN6icu_6722RuleBasedBreakIterator9dumpCacheEv, @function
_ZN6icu_6722RuleBasedBreakIterator9dumpCacheEv:
.LFB3225:
	.cfi_startproc
	endbr64
	movq	488(%rdi), %rdi
	jmp	_ZN6icu_6722RuleBasedBreakIterator10BreakCache9dumpCacheEv@PLT
	.cfi_endproc
.LFE3225:
	.size	_ZN6icu_6722RuleBasedBreakIterator9dumpCacheEv, .-_ZN6icu_6722RuleBasedBreakIterator9dumpCacheEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIterator10dumpTablesEv
	.type	_ZN6icu_6722RuleBasedBreakIterator10dumpTablesEv, @function
_ZN6icu_6722RuleBasedBreakIterator10dumpTablesEv:
.LFB3226:
	.cfi_startproc
	endbr64
	movq	472(%rdi), %rdi
	jmp	_ZN6icu_6715RBBIDataWrapper9printDataEv@PLT
	.cfi_endproc
.LFE3226:
	.size	_ZN6icu_6722RuleBasedBreakIterator10dumpTablesEv, .-_ZN6icu_6722RuleBasedBreakIterator10dumpTablesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIteratorD2Ev
	.type	_ZN6icu_6722RuleBasedBreakIteratorD2Ev, @function
_ZN6icu_6722RuleBasedBreakIteratorD2Ev:
.LFB3185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	536(%r12), %r13
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	528(%rdi), %rdi
	cmpq	%r13, %rdi
	je	.L602
	testq	%rdi, %rdi
	je	.L602
	movq	(%rdi), %rax
	call	*8(%rax)
.L602:
	movq	$0, 528(%r12)
	leaq	328(%r12), %rdi
	call	utext_close_67@PLT
	movq	472(%r12), %rdi
	testq	%rdi, %rdi
	je	.L603
	call	_ZN6icu_6715RBBIDataWrapper15removeReferenceEv@PLT
	movq	$0, 472(%r12)
.L603:
	movq	488(%r12), %rdi
	testq	%rdi, %rdi
	je	.L604
	movq	(%rdi), %rax
	call	*8(%rax)
.L604:
	movq	496(%r12), %r14
	movq	$0, 488(%r12)
	testq	%r14, %r14
	je	.L605
	movq	%r14, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L605:
	movq	504(%r12), %rdi
	movq	$0, 496(%r12)
	testq	%rdi, %rdi
	je	.L606
	movq	(%rdi), %rax
	call	*8(%rax)
.L606:
	movq	512(%r12), %rdi
	movq	$0, 504(%r12)
	testq	%rdi, %rdi
	je	.L607
	movq	(%rdi), %rax
	call	*8(%rax)
.L607:
	movq	$0, 512(%r12)
	movq	%r13, %rdi
	call	_ZN6icu_6723StringCharacterIteratorD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713BreakIteratorD2Ev@PLT
	.cfi_endproc
.LFE3185:
	.size	_ZN6icu_6722RuleBasedBreakIteratorD2Ev, .-_ZN6icu_6722RuleBasedBreakIteratorD2Ev
	.globl	_ZN6icu_6722RuleBasedBreakIteratorD1Ev
	.set	_ZN6icu_6722RuleBasedBreakIteratorD1Ev,_ZN6icu_6722RuleBasedBreakIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIteratorD0Ev
	.type	_ZN6icu_6722RuleBasedBreakIteratorD0Ev, @function
_ZN6icu_6722RuleBasedBreakIteratorD0Ev:
.LFB3187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6722RuleBasedBreakIteratorD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3187:
	.size	_ZN6icu_6722RuleBasedBreakIteratorD0Ev, .-_ZN6icu_6722RuleBasedBreakIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.type	_ZN6icu_6722RuleBasedBreakIteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, @function
_ZN6icu_6722RuleBasedBreakIteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode:
.LFB3176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713BreakIteratorC2Ev@PLT
	leaq	16+_ZTVN6icu_6722RuleBasedBreakIteratorE(%rip), %rax
	movq	%r15, %rsi
	movl	$2, %ecx
	movq	%rax, (%r12)
	leaq	536(%r12), %rdi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movw	%cx, -120(%rbp)
	call	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movdqa	_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm1
	pxor	%xmm0, %xmm0
	movdqa	16+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm2
	leaq	328(%r12), %rdi
	movq	$0, 528(%r12)
	movdqa	32+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm3
	movups	%xmm1, 328(%r12)
	movdqa	48+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm4
	movdqa	64+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm5
	movb	$0, 632(%r12)
	movdqa	80+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm6
	movq	$0, 472(%r12)
	movdqa	96+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm7
	movq	$0, 480(%r12)
	movdqa	112+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm1
	movl	$0, 520(%r12)
	movups	%xmm0, 488(%r12)
	movups	%xmm0, 504(%r12)
	movups	%xmm2, 16(%rdi)
	movdqa	128+_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText(%rip), %xmm2
	movups	%xmm3, 32(%rdi)
	movups	%xmm4, 48(%rdi)
	movups	%xmm5, 64(%rdi)
	movups	%xmm6, 80(%rdi)
	movups	%xmm7, 96(%rdi)
	movups	%xmm1, 112(%rdi)
	movups	%xmm2, 128(%rdi)
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L655
.L632:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L656
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_restore_state
	movq	%rbx, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	utext_openUChars_67@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L634
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator15DictionaryCacheC1EPS0_R10UErrorCode@PLT
.L634:
	movq	%r15, 496(%r12)
	movl	$832, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L635
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722RuleBasedBreakIterator10BreakCacheC1EPS0_R10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	%r15, 488(%r12)
	testl	%edx, %edx
	jg	.L632
	cmpq	$0, 496(%r12)
	je	.L645
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN6icu_6715RBBIRuleBuilder28createRuleBasedBreakIteratorERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movq	%rax, %r13
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L632
	cmpq	%r13, %r12
	je	.L642
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713BreakIteratoraSERKS0_@PLT
	movq	504(%r12), %rdi
	testq	%rdi, %rdi
	je	.L641
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 504(%r12)
.L641:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6722RuleBasedBreakIteratoraSERKS0_.part.0
	testq	%r13, %r13
	je	.L632
.L642:
	movq	0(%r13), %rax
	leaq	_ZN6icu_6722RuleBasedBreakIteratorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L657
	movq	%r13, %rdi
	call	_ZN6icu_6722RuleBasedBreakIteratorD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L632
.L635:
	movq	$0, 488(%r12)
	cmpl	$0, (%rbx)
	jg	.L632
	.p2align 4,,10
	.p2align 3
.L645:
	movl	$7, (%rbx)
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L657:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L632
.L656:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3176:
	.size	_ZN6icu_6722RuleBasedBreakIteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode, .-_ZN6icu_6722RuleBasedBreakIteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6722RuleBasedBreakIteratorC1ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.set	_ZN6icu_6722RuleBasedBreakIteratorC1ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode,_ZN6icu_6722RuleBasedBreakIteratorC2ERKNS_13UnicodeStringER11UParseErrorR10UErrorCode
	.weak	_ZTSN6icu_6722RuleBasedBreakIteratorE
	.section	.rodata._ZTSN6icu_6722RuleBasedBreakIteratorE,"aG",@progbits,_ZTSN6icu_6722RuleBasedBreakIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6722RuleBasedBreakIteratorE, @object
	.size	_ZTSN6icu_6722RuleBasedBreakIteratorE, 34
_ZTSN6icu_6722RuleBasedBreakIteratorE:
	.string	"N6icu_6722RuleBasedBreakIteratorE"
	.weak	_ZTIN6icu_6722RuleBasedBreakIteratorE
	.section	.data.rel.ro._ZTIN6icu_6722RuleBasedBreakIteratorE,"awG",@progbits,_ZTIN6icu_6722RuleBasedBreakIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6722RuleBasedBreakIteratorE, @object
	.size	_ZTIN6icu_6722RuleBasedBreakIteratorE, 24
_ZTIN6icu_6722RuleBasedBreakIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722RuleBasedBreakIteratorE
	.quad	_ZTIN6icu_6713BreakIteratorE
	.weak	_ZTVN6icu_6722RuleBasedBreakIteratorE
	.section	.data.rel.ro.local._ZTVN6icu_6722RuleBasedBreakIteratorE,"awG",@progbits,_ZTVN6icu_6722RuleBasedBreakIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6722RuleBasedBreakIteratorE, @object
	.size	_ZTVN6icu_6722RuleBasedBreakIteratorE, 224
_ZTVN6icu_6722RuleBasedBreakIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6722RuleBasedBreakIteratorE
	.quad	_ZN6icu_6722RuleBasedBreakIteratorD1Ev
	.quad	_ZN6icu_6722RuleBasedBreakIteratorD0Ev
	.quad	_ZNK6icu_6722RuleBasedBreakIterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6722RuleBasedBreakIteratoreqERKNS_13BreakIteratorE
	.quad	_ZNK6icu_6722RuleBasedBreakIterator5cloneEv
	.quad	_ZNK6icu_6722RuleBasedBreakIterator7getTextEv
	.quad	_ZNK6icu_6722RuleBasedBreakIterator8getUTextEP5UTextR10UErrorCode
	.quad	_ZN6icu_6722RuleBasedBreakIterator7setTextERKNS_13UnicodeStringE
	.quad	_ZN6icu_6722RuleBasedBreakIterator7setTextEP5UTextR10UErrorCode
	.quad	_ZN6icu_6722RuleBasedBreakIterator9adoptTextEPNS_17CharacterIteratorE
	.quad	_ZN6icu_6722RuleBasedBreakIterator5firstEv
	.quad	_ZN6icu_6722RuleBasedBreakIterator4lastEv
	.quad	_ZN6icu_6722RuleBasedBreakIterator8previousEv
	.quad	_ZN6icu_6722RuleBasedBreakIterator4nextEv
	.quad	_ZNK6icu_6722RuleBasedBreakIterator7currentEv
	.quad	_ZN6icu_6722RuleBasedBreakIterator9followingEi
	.quad	_ZN6icu_6722RuleBasedBreakIterator9precedingEi
	.quad	_ZN6icu_6722RuleBasedBreakIterator10isBoundaryEi
	.quad	_ZN6icu_6722RuleBasedBreakIterator4nextEi
	.quad	_ZNK6icu_6722RuleBasedBreakIterator13getRuleStatusEv
	.quad	_ZN6icu_6722RuleBasedBreakIterator16getRuleStatusVecEPiiR10UErrorCode
	.quad	_ZN6icu_6722RuleBasedBreakIterator17createBufferCloneEPvRiR10UErrorCode
	.quad	_ZN6icu_6722RuleBasedBreakIterator16refreshInputTextEP5UTextR10UErrorCode
	.quad	_ZNK6icu_6722RuleBasedBreakIterator8hashCodeEv
	.quad	_ZNK6icu_6722RuleBasedBreakIterator8getRulesEv
	.quad	_ZN6icu_6722RuleBasedBreakIterator14getBinaryRulesERj
	.local	_ZL13gRBBIInitOnce
	.comm	_ZL13gRBBIInitOnce,8,8
	.local	_ZL31gLanguageBreakFactoriesInitOnce
	.comm	_ZL31gLanguageBreakFactoriesInitOnce,8,8
	.local	_ZL12gEmptyString
	.comm	_ZL12gEmptyString,8,8
	.local	_ZL23gLanguageBreakFactories
	.comm	_ZL23gLanguageBreakFactories,8,8
	.section	.rodata
	.align 32
	.type	_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText, @object
	.size	_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText, 144
_ZZN6icu_6722RuleBasedBreakIterator4initER10UErrorCodeE16initializedUText:
	.long	878368812
	.long	0
	.long	0
	.long	144
	.quad	0
	.long	0
	.long	0
	.quad	0
	.long	0
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	0
	.long	0
	.quad	0
	.long	0
	.long	0
	.local	_ZZN6icu_6722RuleBasedBreakIterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6722RuleBasedBreakIterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
