	.file	"ucnv_ct.cpp"
	.text
	.p2align 4
	.type	_CompoundTextReset, @function
_CompoundTextReset:
.LFB2118:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2118:
	.size	_CompoundTextReset, .-_CompoundTextReset
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"x11-compound-text"
	.text
	.p2align 4
	.type	_CompoundTextgetName, @function
_CompoundTextgetName:
.LFB2119:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE2119:
	.size	_CompoundTextgetName, .-_CompoundTextgetName
	.p2align 4
	.type	_CompoundText_GetUnicodeSet, @function
_CompoundText_GetUnicodeSet:
.LFB2122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	leaq	8(%r13), %rbx
	addq	$160, %r13
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rbx), %rdi
	movq	%r14, %rcx
	movl	%r15d, %edx
	movq	%r12, %rsi
	addq	$8, %rbx
	call	ucnv_MBCSGetUnicodeSetForUnicode_67@PLT
	cmpq	%r13, %rbx
	jne	.L5
	movq	(%r12), %rdi
	xorl	%esi, %esi
	call	*8(%r12)
	movq	(%r12), %rdi
	movl	$9, %esi
	call	*8(%r12)
	movq	(%r12), %rdi
	movl	$10, %esi
	call	*8(%r12)
	movq	(%r12), %rdi
	movl	$127, %edx
	movl	$32, %esi
	call	*16(%r12)
	movq	16(%r12), %rax
	movq	(%r12), %rdi
	addq	$8, %rsp
	popq	%rbx
	movl	$255, %edx
	popq	%r12
	movl	$160, %esi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2122:
	.size	_CompoundText_GetUnicodeSet, .-_CompoundText_GetUnicodeSet
	.p2align 4
	.type	UConverter_fromUnicode_CompoundText_OFFSETS, @function
UConverter_fromUnicode_CompoundText_OFFSETS:
.LFB2120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	8(%rdi), %r12
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %r10
	movq	16(%rdi), %rax
	movq	16(%r12), %r13
	movl	84(%r12), %esi
	movq	24(%rdi), %r9
	movzbl	63(%r12), %r15d
	movslq	160(%r13), %r8
	testl	%esi, %esi
	je	.L9
	cmpq	%r10, %r14
	jb	.L10
	cmpq	%r9, %rax
	jb	.L13
.L12:
	movl	%r8d, 160(%r13)
	movq	%rax, 16(%rbx)
	movq	%r14, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movl	$0, 84(%r12)
	addq	$2, %rax
	sall	$10, %esi
	movq	%rax, -88(%rbp)
	leal	-56613888(%rdx,%rsi), %esi
	.p2align 4,,10
	.p2align 3
.L14:
	testl	%esi, %esi
	je	.L295
	cmpl	$9, %esi
	je	.L295
	cmpl	$10, %esi
	je	.L295
	leal	-32(%rsi), %eax
	cmpl	$95, %eax
	jbe	.L295
	leal	-160(%rsi), %eax
	cmpl	$95, %eax
	jbe	.L295
	leal	-258(%rsi), %eax
	cmpl	$5, %eax
	jbe	.L300
	leal	-268(%rsi), %eax
	cmpl	$5, %eax
	jbe	.L300
	leal	-280(%rsi), %eax
	cmpl	$3, %eax
	jbe	.L300
	leal	-313(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L300
	leal	-317(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L300
	leal	-321(%rsi), %eax
	cmpl	$3, %eax
	jbe	.L300
	cmpl	$327, %esi
	je	.L300
	cmpl	$336, %esi
	je	.L300
	cmpl	$337, %esi
	je	.L300
	cmpl	$340, %esi
	je	.L300
	cmpl	$341, %esi
	je	.L300
	leal	-344(%rsi), %eax
	cmpl	$3, %eax
	jbe	.L300
	leal	-350(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L300
	leal	-352(%rsi), %eax
	cmpl	$5, %eax
	jbe	.L300
	leal	-366(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L300
	leal	-368(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L300
	leal	-377(%rsi), %eax
	cmpl	$5, %eax
	jbe	.L300
	cmpl	$711, %esi
	je	.L300
	cmpl	$728, %esi
	je	.L300
	movl	%esi, %eax
	andl	$-3, %eax
	cmpl	$729, %eax
	je	.L300
	cmpl	$733, %esi
	je	.L300
	leal	-264(%rsi), %eax
	cmpl	$3, %eax
	jbe	.L301
	cmpl	$284, %esi
	je	.L301
	cmpl	$285, %esi
	je	.L301
	cmpl	$288, %esi
	je	.L301
	cmpl	$289, %esi
	je	.L301
	leal	-292(%rsi), %eax
	cmpl	$3, %eax
	jbe	.L301
	leal	-308(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L301
	leal	-348(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L301
	leal	-364(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L301
	leal	-372(%rsi), %eax
	cmpl	$3, %eax
	jbe	.L302
	cmpl	$7690, %esi
	je	.L302
	cmpl	$7691, %esi
	je	.L302
	cmpl	$7710, %esi
	je	.L302
	cmpl	$7711, %esi
	je	.L302
	cmpl	$7744, %esi
	je	.L302
	cmpl	$7745, %esi
	je	.L302
	cmpl	$7766, %esi
	je	.L302
	cmpl	$7767, %esi
	je	.L302
	cmpl	$7776, %esi
	je	.L302
	cmpl	$7777, %esi
	je	.L302
	cmpl	$7786, %esi
	je	.L302
	cmpl	$7787, %esi
	je	.L302
	cmpl	$7922, %esi
	je	.L302
	cmpl	$7923, %esi
	je	.L302
	leal	-7808(%rsi), %eax
	cmpl	$5, %eax
	jbe	.L302
	leal	-338(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L303
	cmpl	$376, %esi
	je	.L303
	cmpl	$8364, %esi
	je	.L303
	leal	-3585(%rsi), %eax
	cmpl	$57, %eax
	jbe	.L237
	leal	-3647(%rsi), %eax
	cmpl	$28, %eax
	jbe	.L237
	leal	-256(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L238
	cmpl	$274, %esi
	je	.L238
	cmpl	$275, %esi
	je	.L298
	cmpl	$278, %esi
	je	.L298
	cmpl	$279, %esi
	je	.L298
	cmpl	$290, %esi
	je	.L298
	cmpl	$291, %esi
	je	.L298
	leal	-296(%rsi), %eax
	cmpl	$3, %eax
	jbe	.L298
	leal	-302(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L298
	leal	-310(%rsi), %eax
	cmpl	$2, %eax
	jbe	.L298
	leal	-315(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L298
	leal	-325(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L298
	leal	-330(%rsi), %eax
	cmpl	$3, %eax
	jbe	.L298
	leal	-342(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L298
	leal	-358(%rsi), %eax
	cmpl	$5, %eax
	jbe	.L298
	leal	-370(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L298
	leal	-700(%rsi), %eax
	cmpl	$1, %eax
	leal	-900(%rsi), %eax
	setbe	%dl
	cmpl	$74, %eax
	setbe	%al
	orb	%al, %dl
	jne	.L97
	cmpl	$8213, %esi
	je	.L97
	movl	%esi, %eax
	andl	$-5, %eax
	cmpl	$1563, %eax
	je	.L98
	cmpl	$1548, %esi
	je	.L98
	leal	-1569(%rsi), %eax
	cmpl	$25, %eax
	jbe	.L98
	leal	-1600(%rsi), %eax
	cmpl	$18, %eax
	jbe	.L98
	leal	-1632(%rsi), %eax
	cmpl	$13, %eax
	jbe	.L98
	cmpl	$8203, %esi
	je	.L98
	leal	-65136(%rsi), %eax
	cmpl	$2, %eax
	jbe	.L98
	cmpl	$65140, %esi
	je	.L98
	leal	-65142(%rsi), %eax
	cmpl	$72, %eax
	jbe	.L98
	cmpl	$8215, %esi
	sete	%dl
	cmpl	$8254, %esi
	sete	%al
	orb	%al, %dl
	jne	.L243
	leal	-1488(%rsi), %eax
	cmpl	$26, %eax
	jbe	.L243
	leal	-1025(%rsi), %eax
	cmpl	$94, %eax
	jbe	.L244
	cmpl	$8470, %esi
	je	.L244
	leal	-286(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L299
	leal	-304(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L299
	leal	-536(%rsi), %eax
	cmpl	$3, %eax
	ja	.L112
.L299:
	cmpl	$1, %r8d
	je	.L28
	movl	$1, %r8d
.L82:
	movslq	%r8d, %rax
	leaq	_ZL18escSeqCompoundText(%rip), %rdx
	movb	$27, -63(%rbp)
	leaq	(%rax,%rax,4), %rcx
	movzbl	1(%rdx,%rcx), %ecx
	testb	%cl, %cl
	je	.L294
.L114:
	movb	%cl, -62(%rbp)
	leaq	(%rax,%rax,4), %rcx
	movzbl	2(%rdx,%rcx), %ecx
	testb	%cl, %cl
	je	.L305
	.p2align 4,,10
	.p2align 3
.L132:
	movb	%cl, -61(%rbp)
	leaq	(%rax,%rax,4), %rcx
	movzbl	3(%rdx,%rcx), %edx
	testb	%dl, %dl
	je	.L195
	movb	%dl, -60(%rbp)
	movl	$4, -96(%rbp)
.L115:
	testl	%r8d, %r8d
	je	.L306
.L123:
	movq	0(%r13,%rax,8), %rdi
	movsbl	%r15b, %ecx
	leaq	-68(%rbp), %rdx
	movq	%r11, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r10, -112(%rbp)
	movl	%r8d, -104(%rbp)
	call	ucnv_MBCSFromUChar32_67@PLT
	movslq	-104(%rbp), %r8
	movq	-112(%rbp), %r10
	testl	%eax, %eax
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r11
	jle	.L127
.L131:
	movslq	-96(%rbp), %rsi
	movl	-68(%rbp), %edi
	leal	-8(,%rax,8), %ecx
	leal	1(%rsi), %edx
	movl	%edx, -104(%rbp)
	movl	%edi, %edx
	shrl	%cl, %edx
	leal	-2(%rax), %ecx
	movb	%dl, -63(%rbp,%rsi)
	cmpl	$1, %eax
	je	.L124
	movslq	-104(%rbp), %rdx
	sall	$3, %ecx
	movl	-96(%rbp), %esi
	movq	%rdx, -104(%rbp)
	movl	%edi, %edx
	addl	$2, %esi
	shrl	%cl, %edx
	movl	%edx, %ecx
	movq	-104(%rbp), %rdx
	movb	%cl, -63(%rbp,%rdx)
	leal	-3(%rax), %ecx
	cmpl	$2, %eax
	je	.L124
	movslq	%esi, %rsi
	sall	$3, %ecx
	movl	-96(%rbp), %edx
	movq	%rsi, -104(%rbp)
	movl	%edi, %esi
	shrl	%cl, %esi
	addl	$3, %edx
	movl	%esi, %ecx
	movq	-104(%rbp), %rsi
	movb	%cl, -63(%rbp,%rsi)
	leal	-4(%rax), %ecx
	cmpl	$3, %eax
	je	.L124
	movslq	%edx, %rdx
	sall	$3, %ecx
	movl	-96(%rbp), %esi
	movq	%rdx, -104(%rbp)
	movl	%edi, %edx
	shrl	%cl, %edx
	addl	$4, %esi
	movl	%edx, %ecx
	movq	-104(%rbp), %rdx
	movb	%cl, -63(%rbp,%rdx)
	leal	-5(%rax), %edx
	cmpl	$4, %eax
	je	.L124
	movl	-96(%rbp), %ecx
	movslq	%esi, %rsi
	addl	$5, %ecx
	movl	%ecx, -104(%rbp)
	leal	0(,%rdx,8), %ecx
	movl	%edi, %edx
	shrl	%cl, %edx
	movb	%dl, -63(%rbp,%rsi)
	leal	-6(%rax), %edx
	cmpl	$5, %eax
	je	.L124
	movl	-96(%rbp), %ecx
	leal	6(%rcx), %esi
	movslq	-104(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	leal	0(,%rdx,8), %ecx
	movl	%edi, %edx
	shrl	%cl, %edx
	movq	-104(%rbp), %rcx
	movb	%dl, -63(%rbp,%rcx)
	leal	-7(%rax), %ecx
	cmpl	$6, %eax
	je	.L124
	sall	$3, %ecx
	movslq	%esi, %rsi
	shrl	%cl, %edi
	movb	%dil, -63(%rbp,%rsi)
.L124:
	addl	%eax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L127:
	cmpq	%r10, %r14
	jnb	.L307
	movzbl	-63(%rbp), %edx
	movl	-96(%rbp), %ecx
	leaq	1(%r14), %rax
	movb	%dl, (%r14)
	cmpl	$1, %ecx
	je	.L211
	cmpq	%rax, %r10
	jbe	.L202
	movzbl	-62(%rbp), %edx
	leaq	2(%r14), %rax
	movb	%dl, 1(%r14)
	cmpl	$2, %ecx
	je	.L211
	cmpq	%rax, %r10
	jbe	.L204
	movzbl	-61(%rbp), %edx
	leaq	3(%r14), %rax
	movb	%dl, 2(%r14)
	cmpl	$3, %ecx
	je	.L211
	cmpq	%r10, %rax
	jnb	.L206
	movzbl	-60(%rbp), %edx
	leaq	4(%r14), %rax
	movb	%dl, 3(%r14)
	cmpl	$4, %ecx
	je	.L211
	cmpq	%r10, %rax
	jnb	.L208
	movzbl	-59(%rbp), %edx
	leaq	5(%r14), %rax
	movb	%dl, 4(%r14)
	cmpl	$5, %ecx
	je	.L211
	cmpq	%rax, %r10
	jbe	.L210
	movzbl	-58(%rbp), %edx
	leaq	6(%r14), %rax
	movb	%dl, 5(%r14)
	cmpl	$6, %ecx
	je	.L211
	cmpq	%rax, %r10
	jbe	.L212
	movzbl	-57(%rbp), %edx
	leaq	7(%r14), %rax
	movb	%dl, 6(%r14)
	movl	$7, %edx
	cmpl	$7, %ecx
	je	.L211
.L126:
	movl	$15, (%r11)
	movq	%rax, %r14
.L128:
	movq	8(%rbx), %rcx
	movsbq	91(%rcx), %rax
	leal	1(%rax), %esi
	movb	%sil, 91(%rcx)
	movslq	%edx, %rsi
	movzbl	-63(%rbp,%rsi), %esi
	movb	%sil, 104(%rcx,%rax)
	leal	1(%rdx), %eax
	cmpl	-96(%rbp), %eax
	jge	.L219
	movq	8(%rbx), %rsi
	cltq
	movzbl	-63(%rbp,%rax), %eax
	movsbq	91(%rsi), %rcx
	leal	1(%rcx), %edi
	movb	%dil, 91(%rsi)
	movb	%al, 104(%rsi,%rcx)
	leal	2(%rdx), %eax
	cmpl	-96(%rbp), %eax
	jge	.L219
	movq	8(%rbx), %rsi
	cltq
	movzbl	-63(%rbp,%rax), %eax
	movsbq	91(%rsi), %rcx
	leal	1(%rcx), %edi
	movb	%dil, 91(%rsi)
	movb	%al, 104(%rsi,%rcx)
	leal	3(%rdx), %eax
	cmpl	-96(%rbp), %eax
	jge	.L219
	movq	8(%rbx), %rsi
	cltq
	movzbl	-63(%rbp,%rax), %eax
	movsbq	91(%rsi), %rcx
	leal	1(%rcx), %edi
	movb	%dil, 91(%rsi)
	movb	%al, 104(%rsi,%rcx)
	leal	4(%rdx), %eax
	cmpl	-96(%rbp), %eax
	jge	.L219
	movq	8(%rbx), %rsi
	cltq
	movzbl	-63(%rbp,%rax), %eax
	movsbq	91(%rsi), %rcx
	leal	1(%rcx), %edi
	movb	%dil, 91(%rsi)
	movb	%al, 104(%rsi,%rcx)
	leal	5(%rdx), %eax
	cmpl	-96(%rbp), %eax
	jge	.L219
	movq	8(%rbx), %rsi
	cltq
	addl	$6, %edx
	movzbl	-63(%rbp,%rax), %eax
	movsbq	91(%rsi), %rcx
	leal	1(%rcx), %edi
	movb	%dil, 91(%rsi)
	movb	%al, 104(%rsi,%rcx)
	cmpl	-96(%rbp), %edx
	jge	.L219
	movq	8(%rbx), %rcx
	movslq	%edx, %rdx
	movzbl	-63(%rbp,%rdx), %edx
	movsbq	91(%rcx), %rax
	leal	1(%rax), %esi
	movb	%sil, 91(%rcx)
	movb	%dl, 104(%rcx,%rax)
	movq	-88(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L9:
	cmpq	%r9, %rax
	jnb	.L12
	cmpq	%r10, %r14
	jnb	.L13
	movzwl	(%rax), %esi
	leaq	2(%rax), %rcx
	movq	%rcx, -88(%rbp)
	movl	%esi, %edx
	movl	%esi, %eax
	andl	$63488, %edx
	cmpl	$55296, %edx
	jne	.L14
	testb	$4, %ah
	movq	%rcx, %rax
	jne	.L15
	.p2align 4,,10
	.p2align 3
.L10:
	cmpq	%r9, %rax
	jnb	.L293
	movzwl	(%rax), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L308
.L15:
	movl	$12, (%r11)
.L293:
	movl	%esi, 84(%r12)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$15, (%r11)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L295:
	testl	%r8d, %r8d
	je	.L309
	movl	$11547, %eax
	xorl	%r8d, %r8d
	movl	$65, %ecx
	movw	%ax, -63(%rbp)
	leaq	_ZL18escSeqCompoundText(%rip), %rdx
	xorl	%eax, %eax
	jmp	.L132
.L112:
	movl	%r8d, -112(%rbp)
	movsbl	%r15b, %eax
	movl	$1, %ecx
	leaq	-68(%rbp), %rdx
	movq	%r10, -128(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r11, -160(%rbp)
	movq	%r14, -96(%rbp)
	movl	%eax, %r14d
	movq	%r12, -120(%rbp)
	movl	%esi, %r12d
	movb	%r15b, -137(%rbp)
	movq	%rdx, %r15
	movq	%rbx, -152(%rbp)
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L121:
	movq	0(%r13,%rbx,8), %rdi
	movl	%r14d, %ecx
	movq	%r15, %rdx
	movl	%r12d, %esi
	movl	%ebx, -104(%rbp)
	call	ucnv_MBCSFromUChar32_67@PLT
	testl	%eax, %eax
	jg	.L310
	addq	$1, %rbx
	cmpq	$12, %rbx
	jne	.L121
	movq	-96(%rbp), %r14
	movslq	-112(%rbp), %r8
	movq	-120(%rbp), %r12
	movq	-128(%rbp), %r10
	movq	-136(%rbp), %r9
	movzbl	-137(%rbp), %r15d
	movq	-152(%rbp), %rbx
	movq	-160(%rbp), %r11
.L219:
	movq	-88(%rbp), %rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L309:
	movl	$1, -96(%rbp)
.L21:
	movb	%sil, -63(%rbp,%r8)
	xorl	%r8d, %r8d
	jmp	.L127
.L211:
	movq	%rax, %r14
	jmp	.L219
.L300:
	cmpl	$16, %r8d
	je	.L28
	movl	$16, %r8d
.L27:
	movslq	%r8d, %rax
	leaq	_ZL18escSeqCompoundText(%rip), %rdx
	movb	$27, -63(%rbp)
	leaq	(%rax,%rax,4), %rcx
	movzbl	1(%rdx,%rcx), %ecx
	testb	%cl, %cl
	jne	.L114
.L294:
	movl	$1, -96(%rbp)
	jmp	.L123
.L195:
	movl	$3, -96(%rbp)
	jmp	.L115
.L202:
	movl	$1, %edx
	jmp	.L126
.L204:
	movl	$2, %edx
	jmp	.L126
.L28:
	movslq	%r8d, %rax
	movsbl	%r15b, %ecx
	leaq	-68(%rbp), %rdx
	movq	%r11, -120(%rbp)
	movq	0(%r13,%rax,8), %rdi
	movq	%r9, -112(%rbp)
	movq	%r10, -104(%rbp)
	movl	%r8d, -96(%rbp)
	call	ucnv_MBCSFromUChar32_67@PLT
	movslq	-96(%rbp), %r8
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	movq	-112(%rbp), %r9
	movq	-120(%rbp), %r11
	jle	.L219
	movl	$0, -96(%rbp)
	jmp	.L131
.L206:
	movl	$3, %edx
	jmp	.L126
.L208:
	movl	$4, %edx
	jmp	.L126
.L210:
	movl	$5, %edx
	jmp	.L126
.L212:
	movl	$6, %edx
	jmp	.L126
.L304:
	call	__stack_chk_fail@PLT
.L305:
	movl	$2, -96(%rbp)
	jmp	.L123
.L306:
	movl	-96(%rbp), %eax
	movslq	%eax, %r8
	addl	$1, %eax
	movl	%eax, -96(%rbp)
	jmp	.L21
.L243:
	cmpl	$13, %r8d
	je	.L28
	movl	$13, %r8d
	jmp	.L82
.L98:
	cmpl	$3, %r8d
	je	.L28
	movl	$3, %r8d
	jmp	.L82
.L244:
	cmpl	$12, %r8d
	je	.L28
	movl	$12, %r8d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L310:
	movl	-112(%rbp), %r8d
	movslq	-104(%rbp), %rcx
	movq	-96(%rbp), %r14
	movq	-120(%rbp), %r12
	movl	$0, -96(%rbp)
	movq	-128(%rbp), %r10
	movq	-136(%rbp), %r9
	movzbl	-137(%rbp), %r15d
	movq	-152(%rbp), %rbx
	movq	-160(%rbp), %r11
	cmpl	%ecx, %r8d
	je	.L117
	leaq	(%rcx,%rcx,4), %rcx
	leaq	_ZL18escSeqCompoundText(%rip), %rdx
	addq	%rcx, %rdx
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	je	.L117
	movb	%cl, -63(%rbp)
	movzbl	1(%rdx), %ecx
	testb	%cl, %cl
	je	.L198
	movb	%cl, -62(%rbp)
	movzbl	2(%rdx), %ecx
	testb	%cl, %cl
	je	.L199
	movzbl	3(%rdx), %edx
	movb	%cl, -61(%rbp)
	testb	%dl, %dl
	je	.L200
	movb	%dl, -60(%rbp)
	movl	$4, -96(%rbp)
.L117:
	movl	-68(%rbp), %r8d
	movslq	-96(%rbp), %rsi
	leal	-8(,%rax,8), %ecx
	movl	%r8d, %edi
	leal	1(%rsi), %edx
	shrl	%cl, %edi
	leal	-2(%rax), %ecx
	movb	%dil, -63(%rbp,%rsi)
	cmpl	$1, %eax
	je	.L119
	movl	-96(%rbp), %edi
	sall	$3, %ecx
	movslq	%edx, %rdx
	leal	2(%rdi), %esi
	movl	%r8d, %edi
	shrl	%cl, %edi
	movb	%dil, -63(%rbp,%rdx)
	leal	-3(%rax), %edx
	cmpl	$2, %eax
	je	.L119
	movl	-96(%rbp), %ecx
	movslq	%esi, %rsi
	leal	3(%rcx), %edi
	leal	0(,%rdx,8), %ecx
	movl	%r8d, %edx
	shrl	%cl, %edx
	movb	%dl, -63(%rbp,%rsi)
	leal	-4(%rax), %edx
	cmpl	$3, %eax
	je	.L119
	movl	-96(%rbp), %ecx
	movslq	%edi, %rdi
	leal	4(%rcx), %esi
	leal	0(,%rdx,8), %ecx
	movl	%r8d, %edx
	shrl	%cl, %edx
	movb	%dl, -63(%rbp,%rdi)
	leal	-5(%rax), %edx
	cmpl	$4, %eax
	je	.L119
	movl	-96(%rbp), %ecx
	movslq	%esi, %rsi
	leal	5(%rcx), %edi
	leal	0(,%rdx,8), %ecx
	movl	%r8d, %edx
	shrl	%cl, %edx
	leal	-6(%rax), %ecx
	movb	%dl, -63(%rbp,%rsi)
	cmpl	$5, %eax
	je	.L119
	movl	-96(%rbp), %edx
	sall	$3, %ecx
	leal	6(%rdx), %esi
	movslq	%edi, %rdx
	movl	%r8d, %edi
	shrl	%cl, %edi
	leal	-7(%rax), %ecx
	movb	%dil, -63(%rbp,%rdx)
	cmpl	$6, %eax
	je	.L119
	sall	$3, %ecx
	movslq	%esi, %rdx
	shrl	%cl, %r8d
	movb	%r8b, -63(%rbp,%rdx)
.L119:
	addl	-96(%rbp), %eax
	movslq	-104(%rbp), %r8
	movl	%eax, -96(%rbp)
	jmp	.L127
.L198:
	movl	$1, -96(%rbp)
	jmp	.L117
.L200:
	movl	$3, -96(%rbp)
	jmp	.L117
.L199:
	movl	$2, -96(%rbp)
	jmp	.L117
.L307:
	movl	$15, (%r11)
	xorl	%edx, %edx
	jmp	.L128
.L301:
	cmpl	$17, %r8d
	je	.L28
	movl	$17, %r8d
	jmp	.L27
.L303:
	cmpl	$19, %r8d
	je	.L28
	movl	$19, %r8d
	jmp	.L27
.L302:
	cmpl	$18, %r8d
	je	.L28
	movl	$18, %r8d
	jmp	.L27
.L97:
	cmpl	$2, %r8d
	je	.L28
	movl	$2, %r8d
	jmp	.L82
.L298:
	cmpl	$14, %r8d
	je	.L28
	movl	$14, %r8d
	jmp	.L82
.L238:
	cmpl	$14, %r8d
	je	.L28
	movl	$14, %r8d
	jmp	.L27
.L237:
	cmpl	$15, %r8d
	je	.L28
	movl	$15, %r8d
	jmp	.L27
	.cfi_endproc
.LFE2120:
	.size	UConverter_fromUnicode_CompoundText_OFFSETS, .-UConverter_fromUnicode_CompoundText_OFFSETS
	.p2align 4
	.type	UConverter_toUnicode_CompoundText_OFFSETS, @function
UConverter_toUnicode_CompoundText_OFFSETS:
.LFB2121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movzwl	(%rdi), %r14d
	movq	16(%rdi), %rbx
	movq	32(%rdi), %r12
	movq	24(%rdi), %r13
	movzwl	%r14w, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	16(%rax), %rax
	movq	%rax, -144(%rbp)
	cmpw	$55, %r14w
	jbe	.L312
	movl	$56, %edx
	movl	$56, %r14d
.L312:
	leaq	-112(%rbp), %rdi
	movq	%r10, %rsi
	movl	$56, %ecx
	movq	%r10, -128(%rbp)
	movq	%rdi, -152(%rbp)
	call	__memcpy_chk@PLT
	movq	-144(%rbp), %rax
	cmpq	%r13, %rbx
	movq	-128(%rbp), %r10
	movw	%r14w, -112(%rbp)
	movl	160(%rax), %eax
	movl	%eax, -120(%rbp)
	jnb	.L313
	.p2align 4,,10
	.p2align 3
.L314:
	cmpq	%r12, 40(%r10)
	jbe	.L315
	movq	8(%r10), %r15
	movsbl	64(%r15), %edx
	testb	%dl, %dl
	jle	.L316
	movzbl	65(%r15), %eax
	cmpl	$27, %eax
	je	.L410
.L318:
	movl	-120(%rbp), %eax
	testl	%eax, %eax
	jne	.L349
.L348:
	cmpq	%r13, %rbx
	jb	.L339
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L411:
	cmpq	%r12, 40(%r10)
	jbe	.L338
	addq	$1, %rbx
	addq	$2, %r12
	movw	%ax, -2(%r12)
	cmpq	%rbx, %r13
	je	.L362
.L339:
	movzbl	(%rbx), %eax
	cmpb	$27, %al
	jne	.L411
	movl	$0, -120(%rbp)
.L337:
	cmpq	%rbx, %r13
	ja	.L314
	.p2align 4,,10
	.p2align 3
.L313:
	movq	-144(%rbp), %rax
	movl	-120(%rbp), %esi
	movl	%esi, 160(%rax)
	movq	%r12, 32(%r10)
	movq	%rbx, 16(%r10)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L412
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movzbl	(%rbx), %eax
	cmpl	$27, %eax
	jne	.L318
.L410:
	movl	$4, %ecx
	movl	%edx, %r9d
	movl	$1, %esi
	xorl	%r14d, %r14d
	movl	$2, %edi
	movl	$3, %r8d
	subl	%edx, %ecx
	negl	%r9d
	subl	%edx, %esi
	subl	%edx, %edi
	subl	%edx, %r8d
	movslq	%ecx, %rcx
	movslq	%r9d, %r9
	movslq	%esi, %rsi
	movslq	%edi, %rdi
	movslq	%r8d, %r8
	addq	%rbx, %rcx
	leaq	_ZL18escSeqCompoundText(%rip), %rax
	addq	%rbx, %r9
	addq	%rbx, %rsi
	movq	%rcx, -128(%rbp)
	addq	%rbx, %rdi
	addq	%rbx, %r8
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L414:
	cmpq	%r13, %r9
	jnb	.L321
	movsbl	(%r9), %r11d
	cmpl	%r11d, %ecx
	jne	.L322
	movzbl	1(%rax), %ecx
	testb	%cl, %cl
	je	.L319
.L356:
	cmpq	%r13, %rsi
	jnb	.L321
	movsbl	(%rsi), %r11d
	cmpl	%ecx, %r11d
	jne	.L322
	movzbl	2(%rax), %ecx
	testb	%cl, %cl
	je	.L319
.L355:
	cmpq	%rdi, %r13
	jbe	.L321
	movsbl	(%rdi), %r11d
	cmpl	%ecx, %r11d
	jne	.L322
	movzbl	3(%rax), %ecx
	testb	%cl, %cl
	je	.L319
.L354:
	cmpq	%r8, %r13
	jbe	.L321
	movsbl	(%r8), %r11d
	cmpl	%r11d, %ecx
	jne	.L322
	cmpb	$0, 4(%rax)
	je	.L319
.L353:
	cmpq	-128(%rbp), %r13
	jbe	.L321
.L322:
	addl	$1, %r14d
	addq	$5, %rax
	cmpl	$20, %r14d
	je	.L413
.L331:
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L319
	testl	%edx, %edx
	jle	.L414
	cmpb	%cl, 65(%r15)
	jne	.L322
	movzbl	1(%rax), %ecx
	testb	%cl, %cl
	je	.L319
	cmpl	$1, %edx
	je	.L356
	cmpb	%cl, 66(%r15)
	jne	.L322
	movzbl	2(%rax), %ecx
	testb	%cl, %cl
	je	.L319
	cmpl	$2, %edx
	je	.L355
	cmpb	67(%r15), %cl
	jne	.L322
	movzbl	3(%rax), %ecx
	testb	%cl, %cl
	je	.L319
	cmpl	$3, %edx
	je	.L354
	cmpb	68(%r15), %cl
	jne	.L322
	cmpb	$0, 4(%rax)
	je	.L319
	cmpl	$4, %edx
	je	.L353
	addl	$1, %r14d
	addq	$5, %rax
	cmpl	$20, %r14d
	jne	.L331
.L413:
	movq	-136(%rbp), %rax
	cmpl	$11, (%rax)
	je	.L350
	cmpb	$1, 64(%r15)
	movl	$12, (%rax)
	adcq	$0, %rbx
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L321:
	movq	-136(%rbp), %rcx
	movl	$11, (%rcx)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-136(%rbp), %rax
	cmpl	$11, (%rax)
	je	.L350
	movslq	%r14d, %rax
	movq	%r10, -120(%rbp)
	leaq	(%rax,%rax,4), %rdi
	leaq	_ZL18escSeqCompoundText(%rip), %rax
	addq	%rax, %rdi
	call	strlen@PLT
	movsbl	64(%r15), %edx
	movq	-120(%rbp), %r10
	movb	$0, 64(%r15)
	subl	%edx, %eax
	cltq
	addq	%rax, %rbx
	testl	%r14d, %r14d
	je	.L348
	cmpq	%rbx, %r13
	jbe	.L336
	movl	%r14d, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%r13, %rcx
	subq	%rbx, %rcx
	cmpl	$1, %ecx
	jle	.L340
	leal	-2(%rcx), %edx
	leaq	1(%rbx), %rax
	leaq	2(%rbx,%rdx), %rdx
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L415:
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L340
.L342:
	cmpb	$27, (%rax)
	jne	.L415
.L341:
	movq	-144(%rbp), %rsi
	movslq	-120(%rbp), %rdx
	movq	%r10, -128(%rbp)
	movq	%rax, %xmm1
	movq	-104(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r12, -80(%rbp)
	movq	(%rsi,%rdx,8), %rdx
	movq	-136(%rbp), %r14
	punpcklqdq	%xmm1, %xmm0
	movq	48(%rax), %rbx
	movq	-152(%rbp), %rdi
	movaps	%xmm0, -96(%rbp)
	movq	%rdx, 48(%rax)
	movq	%r14, %rsi
	call	ucnv_MBCSToUnicodeWithOffsets_67@PLT
	movl	(%r14), %edx
	movq	-104(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	-128(%rbp), %r10
	testl	%edx, %edx
	movq	%rbx, 48(%rax)
	movq	-96(%rbp), %rbx
	jle	.L337
	cmpl	$15, %edx
	jne	.L313
	movzbl	93(%rax), %edi
	movq	8(%r10), %rcx
	testb	%dil, %dil
	jg	.L416
.L343:
	movb	%dil, 93(%rcx)
	movb	$0, 93(%rax)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L362:
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L336:
	movl	%r14d, -120(%rbp)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L340:
	movslq	%ecx, %rax
	addq	%rbx, %rax
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L338:
	movq	-136(%rbp), %rax
	movl	$0, -120(%rbp)
	movl	$15, (%rax)
	jmp	.L337
.L315:
	movq	-136(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L313
.L350:
	cmpq	%r13, %rbx
	jnb	.L359
	movsbq	64(%r15), %rax
	movzbl	(%rbx), %ecx
	leaq	1(%rbx), %rdx
	leal	1(%rax), %esi
	movb	%sil, 64(%r15)
	movb	%cl, 65(%r15,%rax)
	cmpq	%rdx, %r13
	jbe	.L332
	.p2align 4,,10
	.p2align 3
.L333:
	movq	8(%r10), %rcx
	movzbl	(%rdx), %esi
	addq	$1, %rdx
	movsbq	64(%rcx), %rax
	leal	1(%rax), %edi
	movb	%dil, 64(%rcx)
	movb	%sil, 65(%rcx,%rax)
	cmpq	%rdx, %r13
	jne	.L333
.L332:
	movq	-136(%rbp), %rax
	movq	%r13, %rbx
	movl	$0, (%rax)
	jmp	.L313
.L416:
	movsbq	%dil, %rdx
	leaq	144(%rcx), %rsi
	leaq	144(%rax), %r8
	cmpl	$8, %edx
	jnb	.L344
	testb	$4, %dl
	jne	.L417
	testl	%edx, %edx
	je	.L343
	movzbl	144(%rax), %ecx
	movb	%cl, (%rsi)
	testb	$2, %dl
	jne	.L395
.L408:
	movq	8(%r10), %rcx
	movzbl	93(%rax), %edi
	jmp	.L343
.L359:
	movq	%rbx, %r13
	jmp	.L332
.L344:
	movq	144(%rax), %rdi
	movq	%rdi, 144(%rcx)
	movl	%edx, %edi
	movq	-8(%r8,%rdi), %r9
	movq	%r9, -8(%rsi,%rdi)
	leaq	152(%rcx), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rsi
	movq	%rsi, %rcx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	addl	%edx, %ecx
	shrl	$3, %ecx
	rep movsq
	jmp	.L408
.L412:
	call	__stack_chk_fail@PLT
.L417:
	movl	144(%rax), %ecx
	movl	%edx, %edx
	movl	%ecx, (%rsi)
	movl	-4(%r8,%rdx), %ecx
	movl	%ecx, -4(%rsi,%rdx)
	movq	8(%r10), %rcx
	movzbl	93(%rax), %edi
	jmp	.L343
.L395:
	movl	%edx, %edx
	movzwl	-2(%r8,%rdx), %ecx
	movw	%cx, -2(%rsi,%rdx)
	movq	8(%r10), %rcx
	movzbl	93(%rax), %edi
	jmp	.L343
	.cfi_endproc
.LFE2121:
	.size	UConverter_toUnicode_CompoundText_OFFSETS, .-UConverter_toUnicode_CompoundText_OFFSETS
	.p2align 4
	.type	_CompoundTextClose, @function
_CompoundTextClose:
.LFB2117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L418
	movq	%rdi, %r13
	leaq	160(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L421:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L420
	call	ucnv_unloadSharedDataIfReady_67@PLT
.L420:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L421
	movq	16(%r13), %rdi
	call	uprv_free_67@PLT
	movq	$0, 16(%r13)
.L418:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2117:
	.size	_CompoundTextClose, .-_CompoundTextClose
	.section	.rodata.str1.1
.LC1:
	.string	"icu-internal-compound-s1"
.LC2:
	.string	"icu-internal-compound-s2"
.LC3:
	.string	"icu-internal-compound-s3"
.LC4:
	.string	"icu-internal-compound-d1"
.LC5:
	.string	"icu-internal-compound-d2"
.LC6:
	.string	"icu-internal-compound-d3"
.LC7:
	.string	"icu-internal-compound-d4"
.LC8:
	.string	"icu-internal-compound-d5"
.LC9:
	.string	"icu-internal-compound-d6"
.LC10:
	.string	"icu-internal-compound-d7"
.LC11:
	.string	"icu-internal-compound-t"
.LC12:
	.string	"ibm-915_P100-1995"
.LC13:
	.string	"ibm-916_P100-1995"
.LC14:
	.string	"ibm-914_P100-1995"
.LC15:
	.string	"ibm-874_P100-1995"
.LC16:
	.string	"ibm-912_P100-1995"
.LC17:
	.string	"ibm-913_P100-2000"
.LC18:
	.string	"iso-8859_14-1998"
.LC19:
	.string	"ibm-923_P100-1998"
	.text
	.p2align 4
	.type	_CompoundTextOpen, @function
_CompoundTextOpen:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$168, %edi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -344(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%r15)
	testq	%rax, %rax
	je	.L431
	movq	$0, (%rax)
	pxor	%xmm0, %xmm0
	movq	%r12, %rcx
	movq	%rax, %rbx
	leaq	-336(%rbp), %r14
	leaq	-288(%rbp), %r13
	movaps	%xmm0, -336(%rbp)
	movq	%r14, %rdx
	movq	%r13, %rsi
	leaq	.LC1(%rip), %rdi
	movaps	%xmm0, -320(%rbp)
	movq	$0, -304(%rbp)
	movl	$40, -336(%rbp)
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 8(%rbx)
	leaq	.LC2(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 16(%rbx)
	leaq	.LC3(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 24(%rbx)
	leaq	.LC4(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 32(%rbx)
	leaq	.LC5(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 40(%rbx)
	leaq	.LC6(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 48(%rbx)
	leaq	.LC7(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 56(%rbx)
	leaq	.LC8(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 64(%rbx)
	leaq	.LC9(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 72(%rbx)
	leaq	.LC10(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 80(%rbx)
	leaq	.LC11(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 88(%rbx)
	leaq	.LC12(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 96(%rbx)
	leaq	.LC13(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 104(%rbx)
	leaq	.LC14(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 112(%rbx)
	leaq	.LC15(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 120(%rbx)
	leaq	.LC16(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 128(%rbx)
	leaq	.LC17(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 136(%rbx)
	leaq	.LC18(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 144(%rbx)
	leaq	.LC19(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%rax, 152(%rbx)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L432
	movq	-344(%rbp), %rax
	cmpb	$0, 8(%rax)
	jne	.L432
	movl	$0, 160(%rbx)
.L430:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L446
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	16(%r15), %rbx
	testq	%rbx, %rbx
	je	.L430
	leaq	160(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L436:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L435
	call	ucnv_unloadSharedDataIfReady_67@PLT
.L435:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L436
	movq	16(%r15), %rdi
	call	uprv_free_67@PLT
	movq	$0, 16(%r15)
	jmp	.L430
.L431:
	movl	$7, (%r12)
	jmp	.L430
.L446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2116:
	.size	_CompoundTextOpen, .-_CompoundTextOpen
	.globl	_CompoundTextData_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_CompoundTextData_67, @object
	.size	_CompoundTextData_67, 296
_CompoundTextData_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL23_CompoundTextStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL17_CompoundTextImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL23_CompoundTextStaticData, @object
	.size	_ZL23_CompoundTextStaticData, 100
_ZL23_CompoundTextStaticData:
	.long	100
	.string	"COMPOUND_TEXT"
	.zero	46
	.long	0
	.byte	0
	.byte	33
	.byte	1
	.byte	6
	.string	"\357"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL17_CompoundTextImpl, @object
	.size	_ZL17_CompoundTextImpl, 144
_ZL17_CompoundTextImpl:
	.long	33
	.zero	4
	.quad	0
	.quad	0
	.quad	_CompoundTextOpen
	.quad	_CompoundTextClose
	.quad	_CompoundTextReset
	.quad	UConverter_toUnicode_CompoundText_OFFSETS
	.quad	UConverter_toUnicode_CompoundText_OFFSETS
	.quad	UConverter_fromUnicode_CompoundText_OFFSETS
	.quad	UConverter_fromUnicode_CompoundText_OFFSETS
	.quad	0
	.quad	0
	.quad	_CompoundTextgetName
	.quad	0
	.quad	0
	.quad	_CompoundText_GetUnicodeSet
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18escSeqCompoundText, @object
	.size	_ZL18escSeqCompoundText, 100
_ZL18escSeqCompoundText:
	.string	"\033-A"
	.zero	1
	.string	"\033-M"
	.zero	1
	.string	"\033-F"
	.zero	1
	.string	"\033-G"
	.zero	1
	.string	"\033$)A"
	.string	"\033$)B"
	.string	"\033$)C"
	.string	"\033$)D"
	.string	"\033$)G"
	.string	"\033$)H"
	.string	"\033$)I"
	.string	"\033%G"
	.zero	1
	.string	"\033-L"
	.zero	1
	.string	"\033-H"
	.zero	1
	.string	"\033-D"
	.zero	1
	.string	"\033-T"
	.zero	1
	.string	"\033-B"
	.zero	1
	.string	"\033-C"
	.zero	1
	.string	"\033-_"
	.zero	1
	.string	"\033-b"
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
