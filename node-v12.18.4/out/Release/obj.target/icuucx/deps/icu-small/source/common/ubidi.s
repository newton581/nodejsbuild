	.file	"ubidi.cpp"
	.text
	.p2align 4
	.type	_ZL11bracketInitP5UBiDiP11BracketData, @function
_ZL11bracketInitP5UBiDiP11BracketData:
.LFB2109:
	.cfi_startproc
	movq	%rdi, (%rsi)
	movl	$0, 500(%rsi)
	movl	$0, 508(%rsi)
	cmpb	$0, 142(%rdi)
	je	.L2
	movq	208(%rdi), %r10
	movl	(%r10), %eax
	testl	%eax, %eax
	jle	.L20
.L2:
	movzbl	141(%rdi), %eax
	movb	%al, 512(%rsi)
	andl	$1, %eax
.L14:
	movb	%al, 514(%rsi)
	movb	%al, 513(%rsi)
	movq	72(%rdi), %rcx
	movzbl	%al, %eax
	movl	%eax, 516(%rsi)
	movl	$0, 504(%rsi)
	testq	%rcx, %rcx
	je	.L12
	movabsq	$-6148914691236517205, %r8
	movslq	36(%rdi), %rdx
	movq	%rdx, %rax
	mulq	%r8
	shrq	$4, %rdx
.L13:
	movl	%edx, 496(%rsi)
	movl	132(%rdi), %edx
	movq	%rcx, 488(%rsi)
	cmpl	$1, %edx
	sete	%al
	cmpl	$6, %edx
	sete	%dl
	orl	%edx, %eax
	movb	%al, 2536(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	200(%rdi), %r9d
	testl	%r9d, %r9d
	jle	.L3
	leaq	8(%r10), %rax
	leal	-1(%r9), %ecx
	movq	%rcx, %r9
	movq	%rax, %rdx
	leaq	(%rax,%rcx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L4:
	cmpq	%rcx, %rdx
	je	.L21
	movq	%rdx, %r8
	movl	(%rdx), %r11d
	addq	$8, %rdx
	testl	%r11d, %r11d
	jle	.L4
	movl	4(%r8), %edx
	movb	%dl, 512(%rsi)
.L15:
	movl	%r9d, %edx
	leaq	(%rax,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L8:
	cmpq	%rcx, %rax
	je	.L10
	movq	%rax, %rdx
	movl	(%rax), %r8d
	addq	$8, %rax
	testl	%r8d, %r8d
	jle	.L8
	movl	4(%rdx), %eax
	andl	$1, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	8(%rsi), %rcx
	movl	$20, %edx
	jmp	.L13
.L3:
	subl	$1, %r9d
	movslq	%r9d, %rax
	movl	4(%r10,%rax,8), %eax
	movb	%al, 512(%rsi)
	.p2align 4,,10
	.p2align 3
.L10:
	movslq	%r9d, %r9
	leaq	(%r10,%r9,8), %rdx
	movl	4(%rdx), %eax
	andl	$1, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L21:
	movslq	%r9d, %rdx
	movl	4(%r10,%rdx,8), %edx
	movb	%dl, 512(%rsi)
	jmp	.L15
	.cfi_endproc
.LFE2109:
	.size	_ZL11bracketInitP5UBiDiP11BracketData, .-_ZL11bracketInitP5UBiDiP11BracketData
	.p2align 4
	.type	_ZL6fixN0cP11BracketDataiih, @function
_ZL6fixN0cP11BracketDataiih:
.LFB2115:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movl	%ecx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leal	1(%rsi), %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movslq	500(%rdi), %r14
	movq	112(%rdx), %r9
	movslq	%r12d, %rdx
	salq	$4, %r14
	leaq	(%rdx,%rdx,2), %rcx
	movq	488(%rdi), %rdx
	leaq	(%rdi,%r14), %rsi
	movq	%rsi, -72(%rbp)
	leaq	(%rdx,%rcx,8), %rbx
	movzwl	510(%rsi), %ecx
	cmpl	%ecx, %r12d
	jge	.L22
	movq	%rdi, %r13
	movzbl	%r8b, %r14d
	.p2align 4,,10
	.p2align 3
.L23:
	movl	4(%rbx), %edx
	testl	%edx, %edx
	jns	.L24
	cmpl	%eax, 8(%rbx)
	jg	.L22
	movl	(%rbx), %edx
	cmpl	%eax, %edx
	jle	.L24
	movl	%eax, -52(%rbp)
	cmpl	16(%rbx), %r14d
	je	.L22
	movslq	%edx, %rcx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movb	%r8b, -53(%rbp)
	movb	%r8b, (%r9,%rcx)
	movl	4(%rbx), %r15d
	movq	%r9, -64(%rbp)
	negl	%r15d
	movslq	%r15d, %rcx
	movb	%r8b, (%r9,%rcx)
	movl	%r14d, %ecx
	movl	$0, 4(%rbx)
	call	_ZL6fixN0cP11BracketDataiih
	movl	%r14d, %ecx
	movl	%r15d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZL6fixN0cP11BracketDataiih
	movq	-72(%rbp), %rax
	movzbl	-53(%rbp), %r8d
	movq	-64(%rbp), %r9
	movzwl	510(%rax), %ecx
	movl	-52(%rbp), %eax
.L24:
	addl	$1, %r12d
	addq	$24, %rbx
	cmpl	%r12d, %ecx
	jg	.L23
.L22:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2115:
	.size	_ZL6fixN0cP11BracketDataiih, .-_ZL6fixN0cP11BracketDataiih
	.p2align 4
	.type	_ZL17bracketAddOpeningP11BracketDataDsi, @function
_ZL17bracketAddOpeningP11BracketDataDsi:
.LFB2114:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movslq	500(%rdi), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	movzwl	510(%rbx), %edx
	cmpl	496(%rdi), %edx
	jge	.L29
	movq	488(%rdi), %rdi
	movl	%edx, %eax
.L30:
	movzwl	%ax, %edx
	movl	516(%rbx), %ecx
	addl	$1, %eax
	movzwl	%r13w, %r13d
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movl	%ecx, 16(%rdx)
	movl	504(%rbx), %ecx
	movl	%r14d, (%rdx)
	movl	%ecx, 8(%rdx)
	xorl	%ecx, %ecx
	movl	%r13d, 4(%rdx)
	movw	%cx, 12(%rdx)
	movw	%ax, 510(%rbx)
	movl	$1, %eax
.L28:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	(%rdi), %r8
	leal	0(,%rdx,4), %eax
	leal	(%rax,%rdx,2), %r15d
	movq	72(%r8), %rdi
	sall	$3, %r15d
	testq	%rdi, %rdi
	je	.L42
	movslq	36(%r8), %rax
	cmpl	%eax, %r15d
	jle	.L34
	movslq	%r15d, %rsi
	movq	%r8, -56(%rbp)
	call	uprv_realloc_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L35
	movq	-56(%rbp), %r8
	movq	%rax, 72(%r8)
.L41:
	movl	%r15d, 36(%r8)
	movslq	%r15d, %rax
.L34:
	leaq	8(%r12), %rsi
	cmpq	%rsi, 488(%r12)
	je	.L43
.L36:
	movabsq	$-6148914691236517205, %rdx
	movq	%rdi, 488(%r12)
	mulq	%rdx
	shrq	$4, %rdx
	movl	%edx, 496(%r12)
	movzwl	510(%rbx), %eax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L43:
	movq	8(%r12), %rax
	leaq	8(%rdi), %rdx
	andq	$-8, %rdx
	movq	%rax, (%rdi)
	movq	480(%r12), %rax
	movq	%rax, 472(%rdi)
	movq	%rdi, %rax
	movq	%rdx, %rdi
	subq	%rdx, %rax
	leal	480(%rax), %ecx
	subq	%rax, %rsi
	movl	%ecx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	movq	72(%r8), %rdi
	movslq	36(%r8), %rax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%r15d, %edi
	movq	%r8, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, 72(%r8)
	testq	%rax, %rax
	jne	.L41
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
	jmp	.L28
	.cfi_endproc
.LFE2114:
	.size	_ZL17bracketAddOpeningP11BracketDataDsi, .-_ZL17bracketAddOpeningP11BracketDataDsi
	.p2align 4
	.type	_ZL18bracketProcessCharP11BracketDatai, @function
_ZL18bracketProcessCharP11BracketDatai:
.LFB2117:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movslq	%esi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r15, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rdx
	movslq	500(%rdi), %r13
	movq	112(%rdx), %rcx
	leaq	(%rcx,%r15), %rax
	movzbl	(%rax), %r14d
	cmpb	$10, %r14b
	je	.L125
	movq	120(%rdx), %rdx
	movzbl	(%rdx,%r15), %ecx
	testb	%cl, %cl
	js	.L126
	cmpb	$1, %r14b
	jbe	.L98
	cmpb	$13, %r14b
	je	.L98
	cmpb	$2, %r14b
	jne	.L81
	movq	%r13, %rdx
	salq	$4, %rdx
	addq	%rdi, %rdx
	movzbl	513(%rdx), %ecx
	movb	$2, 514(%rdx)
	testb	%cl, %cl
	jne	.L82
	cmpb	$0, 2536(%rdi)
	jne	.L83
	movb	$23, (%rax)
.L83:
	movl	$0, 516(%rdx)
	movl	$1, %ecx
	movl	%ebx, 504(%rdx)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L126:
	subl	$8, %r14d
	andl	$1, %ecx
	cmpb	$2, %r14b
	jbe	.L77
	movb	%cl, (%rax)
.L77:
	movq	%r13, %rax
	salq	$4, %rax
	addq	%r12, %rax
	movb	%cl, 514(%rax)
	movb	%cl, 513(%rax)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L98:
	testb	%r14b, %r14b
	movq	%r13, %rax
	setne	%cl
	salq	$4, %rax
	addq	%r12, %rax
	movb	%r14b, 514(%rax)
	movb	%r14b, 513(%rax)
.L122:
	movzbl	%cl, %edx
	movl	%ebx, 504(%rax)
	movl	%edx, 516(%rax)
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, %ecx
.L78:
	salq	$4, %r13
	addq	%r12, %r13
	movzwl	508(%r13), %esi
	movzwl	510(%r13), %edx
	cmpw	%dx, %si
	jnb	.L89
	subl	$1, %edx
	movzwl	%si, %edi
	movq	488(%r12), %r8
	subl	%esi, %edx
	leaq	(%rdi,%rdi,2), %rax
	addq	%rdi, %rdx
	leaq	(%r8,%rax,8), %rax
	leaq	(%rdx,%rdx,2), %rdx
	leaq	24(%r8,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L91:
	cmpl	%ebx, (%rax)
	jge	.L90
	orw	%cx, 12(%rax)
.L90:
	addq	$24, %rax
	cmpq	%rdx, %rax
	jne	.L91
.L89:
	movl	$1, %eax
.L44:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	%r13, %r10
	movq	8(%rdx), %rax
	salq	$4, %r10
	addq	%r12, %r10
	movzwl	(%rax,%r15,2), %edi
	movzwl	510(%r10), %esi
	movzwl	508(%r10), %r9d
	movw	%di, -52(%rbp)
	subl	$1, %esi
	movl	%r9d, -64(%rbp)
	cmpl	%r9d, %esi
	jl	.L46
	movslq	%esi, %r8
	movq	488(%r12), %rax
	subl	$1, %r9d
	leaq	(%r8,%r8,2), %r11
	salq	$3, %r11
.L48:
	leaq	(%rax,%r11), %r8
	cmpl	%edi, 4(%rax,%r11)
	je	.L47
	subl	$1, %esi
	subq	$24, %r11
	cmpl	%r9d, %esi
	jne	.L48
.L46:
	cmpw	$0, -52(%rbp)
	je	.L69
	movzwl	-52(%rbp), %edi
	movl	%edi, -64(%rbp)
	call	u_getBidiPairedBracket_67@PLT
	cmpw	%ax, -52(%rbp)
	movl	-64(%rbp), %edi
	je	.L74
	movl	%eax, -52(%rbp)
	call	ubidi_getPairedBracketType_67@PLT
	movl	-52(%rbp), %ecx
	cmpl	$1, %eax
	je	.L71
.L74:
	movq	(%r12), %rdx
.L69:
	movq	120(%rdx), %rax
	movzbl	(%rax,%r15), %ecx
	testb	%cl, %cl
	js	.L127
.L86:
	salq	$4, %r13
	movl	$1, %eax
	movb	%r14b, 514(%r13,%r12)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L47:
	movzbl	512(%r10), %eax
	movzwl	12(%r8), %edi
	andl	$1, %eax
	jne	.L49
	testb	$1, %dil
	je	.L51
	xorl	%r9d, %r9d
	xorl	%edx, %edx
.L50:
	movslq	(%r8), %rdi
	movl	%r9d, -56(%rbp)
	movb	%al, -72(%rbp)
	movb	%al, (%rcx,%rdi)
	movq	(%r12), %rcx
	movq	%r12, %rdi
	movq	%r8, -64(%rbp)
	movq	112(%rcx), %rcx
	movl	%esi, -52(%rbp)
	movb	%al, (%rcx,%r15)
	movl	%edx, %ecx
	movl	(%r8), %edx
	call	_ZL6fixN0cP11BracketDataiih
	movl	-52(%rbp), %esi
	movq	-64(%rbp), %r8
	cmpw	508(%r10), %si
	movzbl	-72(%rbp), %eax
	movw	%si, 510(%r10)
	movl	-56(%rbp), %r9d
	movl	%esi, %edx
	jbe	.L128
.L92:
	movq	488(%r12), %rdi
	movl	(%r8), %r8d
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L129:
	subl	$1, %edx
	movw	%dx, 510(%r10)
	cmpw	%dx, 508(%r10)
	jnb	.L56
.L57:
	movzwl	%dx, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	cmpl	%r8d, -24(%rdi,%rcx,8)
	je	.L129
.L56:
	movq	(%r12), %rdx
	cmpb	$10, %al
	je	.L69
	movzbl	%al, %r9d
.L93:
	movq	120(%rdx), %rax
	movb	$10, 514(%r10)
	movl	%r9d, 516(%r10)
	addq	%rax, %r15
	movl	%ebx, 504(%r10)
	movzbl	(%r15), %ecx
	testb	%cl, %cl
	js	.L130
.L64:
	movq	488(%r12), %rdx
	movslq	(%rdx,%r11), %rdx
	andb	$127, (%rax,%rdx)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	testb	$2, %dil
	jne	.L96
.L51:
	andl	$3, %edi
	je	.L52
	movl	16(%r8), %edi
	movzbl	%al, %edx
	cmpl	%edi, %edx
	je	.L53
	movzbl	%dil, %edx
	movl	%edx, %eax
.L53:
	movslq	(%r8), %rdi
	movb	%al, -56(%rbp)
	movq	%r8, -72(%rbp)
	movb	%al, (%rcx,%rdi)
	movq	(%r12), %rcx
	movq	%r12, %rdi
	movl	%esi, -52(%rbp)
	movq	112(%rcx), %rcx
	movb	%al, (%rcx,%r15)
	movl	%edx, %ecx
	movl	(%r8), %edx
	call	_ZL6fixN0cP11BracketDataiih
	movl	-52(%rbp), %esi
	cmpl	-64(%rbp), %esi
	movq	-72(%rbp), %r8
	movzbl	-56(%rbp), %eax
	je	.L131
	movl	%ebx, %edx
	movzwl	508(%r10), %r9d
	negl	%edx
	movl	%edx, 4(%r8)
	leal	-1(%rsi), %edx
	cmpl	%r9d, %edx
	jl	.L60
	movslq	%esi, %rdx
	movq	488(%r12), %rcx
	movl	(%r8), %edi
	notl	%r9d
	leaq	(%rdx,%rdx,2), %r8
	salq	$3, %r8
	leaq	-24(%rcx,%r8), %rdx
	leaq	-48(%rcx,%r8), %rcx
	leal	(%r9,%rsi), %r8d
	leaq	(%r8,%r8,2), %r8
	salq	$3, %r8
	subq	%r8, %rcx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L132:
	movl	$0, 4(%rdx)
	subq	$24, %rdx
	cmpq	%rdx, %rcx
	je	.L60
.L61:
	cmpl	%edi, (%rdx)
	je	.L132
.L60:
	movzwl	510(%r10), %ecx
	leal	1(%rsi), %edx
	cmpl	%ecx, %edx
	jge	.L56
	subl	$2, %ecx
	movslq	%esi, %r8
	movq	488(%r12), %rdi
	subl	%esi, %ecx
	leaq	(%r8,%r8,2), %rdx
	addq	%r8, %rcx
	leaq	28(%rdi,%rdx,8), %rdx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	52(%rdi,%rcx,8), %rcx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L133:
	movl	(%rdx), %edi
	testl	%edi, %edi
	jle	.L62
	movl	$0, (%rdx)
.L62:
	addq	$24, %rdx
	cmpq	%rdx, %rcx
	je	.L56
.L63:
	cmpl	-4(%rdx), %ebx
	jg	.L133
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L127:
	andl	$1, %ecx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L81:
	cmpb	$5, %r14b
	jne	.L85
	movq	%r13, %rax
	movl	$2, %ecx
	salq	$4, %rax
	addq	%rdi, %rax
	movb	$5, 514(%rax)
	movl	$1, 516(%rax)
	movl	%r15d, 504(%rax)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L130:
	movzwl	508(%r10), %edi
	andl	$1, %ecx
	movl	$1, %edx
	movb	%cl, 513(%r10)
	sall	%cl, %edx
	movq	%rdi, %rcx
	cmpl	%edi, %esi
	jle	.L66
	leaq	(%rdi,%rdi,2), %rax
	notl	%edi
	movq	488(%r12), %r8
	addl	%edi, %esi
	addq	%rsi, %rcx
	leaq	(%r8,%rax,8), %rax
	leaq	(%rcx,%rcx,2), %rcx
	leaq	24(%r8,%rcx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L67:
	orw	%dx, 12(%rax)
	addq	$24, %rax
	cmpq	%rax, %rcx
	jne	.L67
.L66:
	andb	$127, (%r15)
	movq	(%r12), %rax
	movq	120(%rax), %rax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L82:
	cmpb	$13, %cl
	movl	$24, %esi
	movl	$5, %ecx
	cmovne	%esi, %ecx
	movb	%cl, (%rax)
	movl	$2, %ecx
	movl	$1, 516(%rdx)
	movl	%r15d, 504(%rdx)
	jmp	.L78
.L96:
	movl	$1, %r9d
	movl	$1, %edx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L52:
	movw	%si, 510(%r10)
	jmp	.L69
.L85:
	cmpb	$17, %r14b
	jne	.L86
	movq	%r13, %rdx
	salq	$4, %rdx
	movzbl	514(%rdx,%rdi), %edx
	cmpb	$10, %dl
	je	.L134
	cmpb	$1, %dl
	jbe	.L88
	cmpb	$13, %dl
	jne	.L89
.L88:
	testb	%dl, %dl
	movl	$1, %eax
	setne	%cl
	sall	%cl, %eax
	movl	%eax, %ecx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L71:
	cmpw	$9002, %cx
	je	.L135
	cmpw	$12297, %cx
	je	.L136
.L75:
	movzwl	%cx, %esi
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	_ZL17bracketAddOpeningP11BracketDataDsi
	testb	%al, %al
	jne	.L74
	xorl	%eax, %eax
	jmp	.L44
.L131:
	movw	%si, 510(%r10)
	movl	%esi, %edx
	cmpw	%si, 508(%r10)
	jb	.L92
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L134:
	movb	$10, (%rax)
	movl	$1, %eax
	jmp	.L44
.L135:
	movl	%ecx, -52(%rbp)
	movl	%ebx, %edx
	movl	$12297, %esi
.L120:
	movq	%r12, %rdi
	call	_ZL17bracketAddOpeningP11BracketDataDsi
	movl	-52(%rbp), %ecx
	testb	%al, %al
	jne	.L75
	xorl	%eax, %eax
	jmp	.L44
.L128:
	movq	(%r12), %rdx
	jmp	.L93
.L136:
	movl	%ecx, -52(%rbp)
	movl	%ebx, %edx
	movl	$9002, %esi
	jmp	.L120
	.cfi_endproc
.LFE2117:
	.size	_ZL18bracketProcessCharP11BracketDatai, .-_ZL18bracketProcessCharP11BracketDatai
	.p2align 4
	.type	_ZL21resolveExplicitLevelsP5UBiDiP10UErrorCode, @function
_ZL21resolveExplicitLevelsP5UBiDiP10UErrorCode:
.LFB2119:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2904, %rsp
	movq	112(%rdi), %rdx
	movq	120(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	cmpb	$0, 142(%rdi)
	movl	188(%rdi), %ecx
	movq	%rax, -2880(%rbp)
	movl	20(%rdi), %eax
	movl	%eax, -2868(%rbp)
	je	.L138
	movq	208(%rdi), %r10
	movl	(%r10), %r14d
	testl	%r14d, %r14d
	jle	.L139
.L138:
	movzbl	141(%rbx), %r14d
.L140:
	movl	(%r12), %r10d
	xorl	%r15d, %r15d
	movl	$0, 324(%rbx)
	testl	%r10d, %r10d
	jg	.L137
	testl	$2154498, %ecx
	jne	.L146
	movl	%ecx, %r15d
	andl	$32, %r15d
	je	.L137
	movl	%ecx, %r15d
	andl	$8249304, %r15d
	je	.L137
.L146:
	movl	$1, %r15d
	testl	$26220581, %ecx
	jne	.L290
.L137:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L291
	addq	$2904, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movl	200(%rdi), %esi
	leal	-1(%rsi), %r9d
	testl	%esi, %esi
	jle	.L141
	subl	$1, %esi
	leaq	8(%r10), %rax
	movq	%rsi, %r9
	leaq	(%rax,%rsi,8), %rdi
	.p2align 4,,10
	.p2align 3
.L142:
	cmpq	%rax, %rdi
	je	.L141
	movq	%rax, %rsi
	movl	(%rax), %r11d
	addq	$8, %rax
	testl	%r11d, %r11d
	jle	.L142
	movzbl	4(%rsi), %r14d
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L290:
	cmpl	$1, 132(%rbx)
	jg	.L292
	andl	$7985152, %ecx
	movl	%ecx, %r15d
	jne	.L153
	leaq	-2608(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rdx, -2888(%rbp)
	movq	%rax, %rsi
	movq	%rax, -2896(%rbp)
	call	_ZL11bracketInitP5UBiDiP11BracketData
	movl	200(%rbx), %ecx
	movq	-2888(%rbp), %rdx
	testl	%ecx, %ecx
	jle	.L164
	movq	$0, -2904(%rbp)
	movq	208(%rbx), %rax
	movq	%r13, %r14
	xorl	%esi, %esi
	movl	%r15d, -2928(%rbp)
	movq	%rdx, %r13
	movq	%rbx, -2920(%rbp)
	movq	%r12, -2936(%rbp)
.L156:
	movq	-2904(%rbp), %rdi
	salq	$3, %rdi
	addq	%rdi, %rax
	movq	%rdi, -2912(%rbp)
	movl	(%rax), %ebx
	movzbl	4(%rax), %r12d
	cmpl	%ebx, %esi
	jge	.L157
	movl	%r12d, %eax
	movslq	%esi, %r15
	andl	$1, %eax
	movq	%r15, %rdx
	movq	%r14, %r15
	movl	%ebx, %r14d
	movb	%al, -2872(%rbp)
	movl	%esi, %ebx
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L294:
	addl	$1, %ebx
	cmpl	%ebx, -2868(%rbp)
	jle	.L160
	movq	-2880(%rbp), %rax
	cmpw	$13, (%rax,%rdx,2)
	jne	.L161
	cmpw	$10, 2(%rax,%rdx,2)
	je	.L160
.L161:
	movzbl	-2872(%rbp), %eax
	xorl	%r9d, %r9d
	movb	%r12b, -2096(%rbp)
	movw	%r9w, -2098(%rbp)
	movb	%al, -2094(%rbp)
	movb	%al, -2095(%rbp)
	movl	%eax, -2092(%rbp)
	movq	$0, -2108(%rbp)
.L160:
	addq	$1, %rdx
	cmpl	%ebx, %r14d
	jle	.L293
.L163:
	movb	%r12b, (%r15,%rdx)
	movzbl	0(%r13,%rdx), %eax
	cmpb	$18, %al
	je	.L158
	cmpb	$7, %al
	je	.L294
	movq	-2896(%rbp), %rdi
	movl	%ebx, %esi
	movq	%rdx, -2888(%rbp)
	call	_ZL18bracketProcessCharP11BracketDatai
	testb	%al, %al
	je	.L162
	movq	-2888(%rbp), %rdx
	addl	$1, %ebx
	addq	$1, %rdx
	cmpl	%ebx, %r14d
	jg	.L163
.L293:
	movq	-2920(%rbp), %rax
	movq	%r15, %r14
	movl	200(%rax), %ecx
.L157:
	movl	-2904(%rbp), %eax
	addl	$1, %eax
	cmpl	%ecx, %eax
	jge	.L164
	movq	-2920(%rbp), %rax
	movq	-2912(%rbp), %rsi
	addq	$1, -2904(%rbp)
	movq	208(%rax), %rax
	movl	(%rax,%rsi), %esi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L141:
	movslq	%r9d, %rsi
	leaq	(%r10,%rsi,8), %rsi
	movzbl	4(%rsi), %r14d
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L292:
	movl	200(%rbx), %edi
	testl	%edi, %edi
	jle	.L164
	movq	208(%rbx), %rsi
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L295:
	movl	%eax, %edx
	movslq	%eax, %rdi
	movzbl	%cl, %esi
	addq	$1, %r12
	notl	%edx
	addq	%r13, %rdi
	addl	%r8d, %edx
	addq	$1, %rdx
	call	memset@PLT
	movl	200(%rbx), %edi
	cmpl	%r12d, %edi
	jle	.L164
	movq	208(%rbx), %rsi
.L152:
	movl	(%rsi,%r14), %eax
.L149:
	leaq	0(,%r12,8), %r14
	leaq	(%rsi,%r14), %rdx
	movl	(%rdx), %r8d
	movl	4(%rdx), %ecx
	cmpl	%r8d, %eax
	jl	.L295
	addq	$1, %r12
	cmpl	%r12d, %edi
	jg	.L152
	.p2align 4,,10
	.p2align 3
.L164:
	movl	$2, %r15d
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	-2608(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rdx, -2904(%rbp)
	movq	%rax, %rsi
	movq	%rax, -2896(%rbp)
	call	_ZL11bracketInitP5UBiDiP11BracketData
	movl	-2868(%rbp), %r8d
	movzbl	%r14b, %eax
	movw	%ax, -2864(%rbp)
	testl	%r8d, %r8d
	jle	.L166
	movq	-2904(%rbp), %rdx
	xorl	%r11d, %r11d
	movq	%r13, %r9
	xorl	%r12d, %r12d
	movl	$0, -2920(%rbp)
	movl	%r14d, %r15d
	movl	%r11d, %r13d
	leaq	.L169(%rip), %r10
	movl	$0, -2872(%rbp)
	movl	$0, -2888(%rbp)
	movl	$0, -2912(%rbp)
	movq	%rbx, -2904(%rbp)
	.p2align 4,,10
	.p2align 3
.L221:
	movzbl	(%rdx,%r12), %ecx
	movl	%r12d, %ebx
	movl	%r12d, %esi
	leal	-7(%rcx), %eax
	cmpb	$15, %al
	ja	.L167
	movzbl	%al, %eax
	movslq	(%r10,%rax,4), %rax
	addq	%r10, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L169:
	.long	.L174-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L173-.L169
	.long	.L173-.L169
	.long	.L167-.L169
	.long	.L173-.L169
	.long	.L173-.L169
	.long	.L172-.L169
	.long	.L167-.L169
	.long	.L171-.L169
	.long	.L167-.L169
	.long	.L170-.L169
	.long	.L170-.L169
	.long	.L168-.L169
	.text
	.p2align 4,,10
	.p2align 3
.L173:
	leal	-11(%rcx), %eax
	movb	%r14b, (%r9,%r12)
	orl	$262144, %r13d
	cmpb	$1, %al
	jbe	.L296
	movl	%r15d, %eax
	andl	$127, %eax
	addl	$1, %eax
	orl	$1, %eax
.L176:
	cmpb	$125, %al
	ja	.L177
	testl	%r11d, %r11d
	jne	.L181
	movl	-2872(%rbp), %edi
	testl	%edi, %edi
	jne	.L178
	cmpb	$12, %cl
	je	.L235
	movl	%eax, %r15d
	cmpb	$15, %cl
	je	.L235
.L179:
	addl	$1, -2888(%rbp)
	movl	-2888(%rbp), %eax
	movzbl	%r15b, %ecx
	xorl	%r11d, %r11d
	movl	%esi, -2912(%rbp)
	movw	%cx, -2864(%rbp,%rax,2)
	.p2align 4,,10
	.p2align 3
.L181:
	addl	$1, %ebx
	cmpl	%ebx, -2868(%rbp)
	jle	.L206
.L226:
	addq	$1, %r12
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L167:
	movl	%r15d, %eax
	xorl	%r14d, %eax
	testb	$127, %al
	je	.L216
	movq	-2608(%rbp), %rcx
	movslq	-2912(%rbp), %rax
	movq	112(%rcx), %rcx
	movq	%rax, %rdi
	movzbl	(%rcx,%rax), %eax
	movl	$7864320, %ecx
	btq	%rax, %rcx
	jc	.L217
	movl	%r14d, %eax
	movl	%r15d, %ecx
	andl	$127, %ecx
	andl	$127, %eax
	cmpl	%eax, %ecx
	movslq	-2108(%rbp), %rax
	cmovg	%r15d, %r14d
	addq	$31, %rax
	salq	$4, %rax
	andl	$1, %r14d
	movzwl	-2596(%rbp,%rax), %ecx
	movb	%r14b, -2590(%rbp,%rax)
	movb	%r14b, -2591(%rbp,%rax)
	movzbl	%r14b, %r14d
	movw	%cx, -2594(%rbp,%rax)
	movb	%r15b, -2592(%rbp,%rax)
	movl	%r14d, -2588(%rbp,%rax)
	movl	%edi, -2600(%rbp,%rax)
.L217:
	movl	%r15d, %eax
	orl	$-2147483648, %r13d
	andl	$1, %eax
	cltq
	testb	%r15b, %r15b
	js	.L297
	leaq	_ZL5flagE(%rip), %rcx
	orl	(%rcx,%rax,4), %r13d
.L216:
	movb	%r15b, (%r9,%r12)
	movq	-2896(%rbp), %rdi
	movq	%rdx, -2944(%rbp)
	movl	%r11d, -2936(%rbp)
	movq	%r9, -2928(%rbp)
	call	_ZL18bracketProcessCharP11BracketDatai
	movq	-2928(%rbp), %r9
	movl	-2936(%rbp), %r11d
	leaq	.L169(%rip), %r10
	testb	%al, %al
	movq	-2944(%rbp), %rdx
	je	.L298
	movzbl	(%rdx,%r12), %ecx
	movl	$1, %eax
	movl	%r15d, %r14d
	salq	%cl, %rax
	orl	%eax, %r13d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L170:
	movl	%r15d, %eax
	leaq	_ZL6flagLR(%rip), %rdi
	movl	%r15d, %r8d
	andl	$1, %eax
	andl	$1, %r8d
	movl	%eax, -2936(%rbp)
	movq	%r15, %rax
	andl	$1, %eax
	movb	%r8b, -2928(%rbp)
	orl	(%rdi,%rax,4), %r13d
	movl	%r15d, %edi
	movl	%r15d, %eax
	xorl	%r14d, %edi
	andl	$127, %eax
	andl	$127, %edi
	movb	%al, (%r9,%r12)
	jne	.L183
	orl	$1024, %r13d
.L184:
	addl	$1, %eax
	leal	2(%r15), %edi
	andl	$126, %edi
	orl	$1, %eax
	cmpb	$20, %cl
	cmove	%edi, %eax
	movl	-2872(%rbp), %edi
	orl	%r11d, %edi
	jne	.L189
	cmpb	$125, %al
	jbe	.L299
.L189:
	movb	$9, (%rdx,%r12)
	addl	$1, %r11d
	movl	%r15d, %r14d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L171:
	movb	%r14b, (%r9,%r12)
	orl	$262144, %r13d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L172:
	movb	%r14b, (%r9,%r12)
	orl	$262144, %r13d
	testl	%r11d, %r11d
	jne	.L181
	movl	-2872(%rbp), %eax
	testl	%eax, %eax
	je	.L182
	subl	$1, %eax
	movl	%eax, -2872(%rbp)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L168:
	movl	%r15d, %eax
	xorl	%r14d, %eax
	testb	$127, %al
	je	.L191
	movq	-2608(%rbp), %rcx
	movslq	-2912(%rbp), %rax
	movq	112(%rcx), %rcx
	movq	%rax, %rdi
	movzbl	(%rcx,%rax), %eax
	movl	$7864320, %ecx
	btq	%rax, %rcx
	jc	.L192
	movl	%r14d, %eax
	movl	%r15d, %ecx
	andl	$127, %ecx
	andl	$127, %eax
	cmpl	%eax, %ecx
	movslq	-2108(%rbp), %rax
	cmovg	%r15d, %r14d
	addq	$31, %rax
	salq	$4, %rax
	andl	$1, %r14d
	movzwl	-2596(%rbp,%rax), %ecx
	movb	%r14b, -2590(%rbp,%rax)
	movb	%r14b, -2591(%rbp,%rax)
	movzbl	%r14b, %r14d
	movw	%cx, -2594(%rbp,%rax)
	movb	%r15b, -2592(%rbp,%rax)
	movl	%r14d, -2588(%rbp,%rax)
	movl	%edi, -2600(%rbp,%rax)
.L192:
	orl	$-2147483648, %r13d
.L191:
	movl	-2888(%rbp), %eax
	movzwl	-2864(%rbp,%rax,2), %r14d
	testl	%r11d, %r11d
	je	.L194
	movb	$9, (%rdx,%r12)
	subl	$1, %r11d
.L195:
	movq	%r14, %rax
	leaq	_ZL6flagLR(%rip), %rcx
	movl	%r14d, %r15d
	andl	$1, %eax
	orl	(%rcx,%rax,4), %r13d
	movl	%r14d, %eax
	andl	$127, %eax
	orl	$1024, %r13d
	movb	%al, (%r9,%r12)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L174:
	movq	-2904(%rbp), %rax
	orb	$-128, %r13b
	cmpb	$0, 142(%rax)
	je	.L199
	movq	208(%rax), %r8
	movl	%r12d, %ecx
	cmpl	%r12d, (%r8)
	jle	.L200
.L199:
	movq	-2904(%rbp), %rax
	movzbl	141(%rax), %eax
.L201:
	movb	%al, (%r9,%r12)
	addl	$1, %ebx
	cmpl	%ebx, -2868(%rbp)
	jg	.L300
.L206:
	movq	-2904(%rbp), %rbx
	movl	%r13d, %r11d
	testl	$8380376, %r13d
	je	.L222
	movzbl	141(%rbx), %eax
	leaq	_ZL6flagLR(%rip), %rdx
	andl	$1, %eax
	orl	(%rdx,%rax,4), %r11d
.L222:
	cmpb	$0, 140(%rbx)
	je	.L223
	movl	%r11d, %eax
	orl	$1, %eax
	testb	$-128, %r11b
	cmovne	%eax, %r11d
.L223:
	movl	%r11d, 188(%rbx)
	testl	$2154498, %r11d
	je	.L227
.L224:
	xorl	%r15d, %r15d
	andl	$26220581, %r11d
	setne	%r15b
	addl	$1, %r15d
	jmp	.L137
.L166:
	movl	$0, 188(%rbx)
	xorl	%r11d, %r11d
.L227:
	movl	%r11d, %r15d
	andl	$32, %r15d
	je	.L137
	movl	%r11d, %r15d
	andl	$8249304, %r15d
	jne	.L224
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L158:
	addl	$1, %ebx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L194:
	movl	-2920(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L301
	movb	$9, (%rdx,%r12)
	movl	-2920(%rbp), %r11d
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L183:
	movq	-2608(%rbp), %r8
	movslq	-2912(%rbp), %rdi
	movq	112(%r8), %r8
	movzbl	(%r8,%rdi), %edi
	movl	$7864320, %r8d
	btq	%rdi, %r8
	jc	.L185
	movl	%r15d, %r8d
	movl	%r14d, %edi
	andl	$127, %r8d
	andl	$127, %edi
	cmpl	%edi, %r8d
	jg	.L186
	movl	%r14d, %r8d
	movl	%r14d, %edi
	andl	$1, %r8d
	andl	$1, %edi
	movb	%r8b, -2928(%rbp)
	movl	%edi, -2936(%rbp)
.L186:
	movslq	-2108(%rbp), %rdi
	movzbl	-2928(%rbp), %r8d
	addq	$31, %rdi
	salq	$4, %rdi
	movb	%r8b, -2590(%rbp,%rdi)
	movzwl	-2596(%rbp,%rdi), %r14d
	movb	%r8b, -2591(%rbp,%rdi)
	movl	-2936(%rbp), %r8d
	movw	%r14w, -2594(%rbp,%rdi)
	movl	%r8d, -2588(%rbp,%rdi)
	movl	-2912(%rbp), %r8d
	movb	%r15b, -2592(%rbp,%rdi)
	movl	%r8d, -2600(%rbp,%rdi)
.L185:
	orl	$-2147482624, %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L296:
	leal	2(%r15), %eax
	andl	$126, %eax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-2936(%rbp), %r12
	movl	-2928(%rbp), %r15d
	movl	$7, (%r12)
	jmp	.L137
.L177:
	testl	%r11d, %r11d
	jne	.L181
.L178:
	addl	$1, -2872(%rbp)
	xorl	%r11d, %r11d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L300:
	movq	-2880(%rbp), %rax
	cmpw	$13, (%rax,%r12,2)
	je	.L302
.L207:
	movq	-2904(%rbp), %rax
	cmpb	$0, 142(%rax)
	je	.L209
	movq	208(%rax), %rdi
	cmpl	%ebx, (%rdi)
	jle	.L210
.L209:
	movq	-2904(%rbp), %rax
	movzbl	141(%rax), %r15d
.L211:
	movzbl	%r15b, %eax
	movl	%r15d, %r14d
	xorl	%r11d, %r11d
	movb	%r15b, -2096(%rbp)
	movw	%ax, -2864(%rbp)
	xorl	%eax, %eax
	movw	%ax, -2098(%rbp)
	movl	%r15d, %eax
	movq	$0, -2108(%rbp)
	andl	$1, %eax
	movb	%al, -2094(%rbp)
	movb	%al, -2095(%rbp)
	movzbl	%al, %eax
	movl	%eax, -2092(%rbp)
	movl	$0, -2920(%rbp)
	movl	$0, -2872(%rbp)
	movl	$0, -2888(%rbp)
	jmp	.L226
.L299:
	movl	$1, %edi
	addl	$1, -2920(%rbp)
	salq	%cl, %rdi
	movl	-2920(%rbp), %ecx
	orl	%edi, %r13d
	movq	-2904(%rbp), %rdi
	cmpl	%ecx, 324(%rdi)
	jge	.L190
	movl	%ecx, 324(%rdi)
.L190:
	addl	$1, -2888(%rbp)
	movzbl	%al, %ecx
	movl	%r15d, %r14d
	xorl	%r11d, %r11d
	movl	-2888(%rbp), %edi
	addw	$256, %cx
	movl	%eax, %r15d
	movl	%esi, -2912(%rbp)
	movl	$0, -2872(%rbp)
	movw	%cx, -2864(%rbp,%rdi,2)
	movslq	-2108(%rbp), %rdi
	movq	-2896(%rbp), %rcx
	movq	%rdi, %r8
	addq	$31, %rdi
	salq	$4, %rdi
	addl	$1, %r8d
	movb	$10, -2590(%rbp,%rdi)
	leaq	8(%rcx,%rdi), %rcx
	movzwl	-2594(%rbp,%rdi), %edi
	movl	%r8d, -2108(%rbp)
	movw	%di, 22(%rcx)
	movw	%di, 20(%rcx)
	movl	%eax, %edi
	andl	$1, %edi
	movb	%al, 24(%rcx)
	movb	%dil, 26(%rcx)
	movb	%dil, 25(%rcx)
	movzbl	%dil, %edi
	movl	%edi, 28(%rcx)
	movl	$0, 16(%rcx)
	jmp	.L181
.L182:
	movl	-2888(%rbp), %ecx
	movl	%eax, %r11d
	testl	%ecx, %ecx
	je	.L181
	movl	%ecx, %eax
	cmpw	$255, -2864(%rbp,%rax,2)
	ja	.L181
	leal	-1(%rcx), %eax
	movl	%r12d, -2912(%rbp)
	movzbl	-2864(%rbp,%rax,2), %r15d
	movl	%eax, -2888(%rbp)
	jmp	.L181
.L297:
	leaq	_ZL5flagO(%rip), %rcx
	orl	(%rcx,%rax,4), %r13d
	jmp	.L216
.L200:
	movl	200(%rax), %esi
	testl	%esi, %esi
	jle	.L303
	subl	$1, %esi
	leaq	8(%r8), %rax
	movl	%esi, -2928(%rbp)
	leaq	(%rax,%rsi,8), %rdi
	.p2align 4,,10
	.p2align 3
.L203:
	cmpq	%rax, %rdi
	je	.L202
	movq	%rax, %rsi
	addq	$8, %rax
	cmpl	%ecx, -8(%rax)
	jle	.L203
.L204:
	movzbl	4(%rsi), %eax
	jmp	.L201
.L301:
	orl	$4194304, %r13d
	cmpw	$255, %r14w
	ja	.L197
	movl	-2888(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L198:
	leal	-1(%rax), %ecx
	cmpw	$255, -2864(%rbp,%rcx,2)
	movq	%rcx, %rax
	jbe	.L198
	movl	%ecx, -2888(%rbp)
.L197:
	movl	-2108(%rbp), %eax
	movl	%esi, -2912(%rbp)
	subl	$1, -2888(%rbp)
	movl	-2888(%rbp), %ecx
	subl	$1, %eax
	subl	$1, -2920(%rbp)
	movl	%eax, -2108(%rbp)
	cltq
	movl	$0, -2872(%rbp)
	salq	$4, %rax
	movb	$10, -2094(%rax,%rbp)
	movl	%ecx, %eax
	movzwl	-2864(%rbp,%rax,2), %r14d
	jmp	.L195
.L235:
	orl	$-128, %eax
	movl	%eax, %r15d
	jmp	.L179
.L302:
	cmpw	$10, 2(%rax,%r12,2)
	jne	.L207
	jmp	.L226
.L303:
	leal	-1(%rsi), %eax
	movl	%eax, -2928(%rbp)
.L202:
	movslq	-2928(%rbp), %rsi
	leaq	(%r8,%rsi,8), %rsi
	jmp	.L204
.L298:
	movl	$-1, %r15d
	jmp	.L137
.L210:
	movl	200(%rax), %r8d
	testl	%r8d, %r8d
	jle	.L304
	leal	-1(%r8), %ecx
	leaq	8(%rdi), %rax
	movq	%rcx, %r8
	leaq	(%rax,%rcx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L213:
	cmpq	%rax, %rsi
	je	.L212
	movq	%rax, %rcx
	addq	$8, %rax
	cmpl	%ebx, -8(%rax)
	jle	.L213
.L214:
	movzbl	4(%rcx), %r15d
	jmp	.L211
.L304:
	subl	$1, %r8d
.L212:
	movslq	%r8d, %r8
	leaq	(%rdi,%r8,8), %rcx
	jmp	.L214
.L291:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2119:
	.size	_ZL21resolveExplicitLevelsP5UBiDiP10UErrorCode, .-_ZL21resolveExplicitLevelsP5UBiDiP10UErrorCode
	.p2align 4
	.type	_ZL11getDirPropsP5UBiDi, @function
_ZL11getDirPropsP5UBiDi:
.LFB2107:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1080, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	136(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -1096(%rbp)
	movq	56(%rdi), %rax
	movq	%rax, -1104(%rbp)
	movl	16(%rdi), %eax
	movl	%eax, -1076(%rbp)
	movzbl	141(%rdi), %eax
	movl	%eax, %ebx
	movb	%al, -1079(%rbp)
	andl	$1, %ebx
	movb	%bl, -1117(%rbp)
	movl	%ecx, %ebx
	andl	$4, %ecx
	andl	$2, %ebx
	movb	%bl, -1077(%rbp)
	cmpb	$-3, %al
	jbe	.L306
	movl	132(%rdi), %eax
	subl	$5, %eax
	cmpl	$1, %eax
	jbe	.L467
	movb	$0, -1080(%rbp)
	testl	%ecx, %ecx
	jne	.L372
.L373:
	movzbl	-1117(%rbp), %edx
	movq	208(%r15), %rax
	movl	$1, -1088(%rbp)
	movl	152(%r15), %r13d
	movl	%edx, 4(%rax)
	movb	%dl, -1078(%rbp)
	testl	%r13d, %r13d
	jle	.L309
	movq	144(%r15), %rax
	movl	$10, %r14d
	movq	%r15, %rbx
	movq	%rax, -1088(%rbp)
	xorl	%eax, %eax
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L314:
	cmpl	$22, %eax
	jg	.L316
	movl	%eax, %edx
	cmpb	$10, %r14b
	je	.L468
	cmpb	$7, %al
	movl	$10, %eax
	cmove	%eax, %r14d
.L316:
	cmpl	%r15d, %r13d
	jle	.L311
.L488:
	movl	%r15d, %eax
.L310:
	movq	-1088(%rbp), %rdi
	movslq	%eax, %rdx
	leal	1(%rax), %r15d
	leaq	(%rdx,%rdx), %rsi
	movzwl	(%rdi,%rdx,2), %r12d
	movl	%r12d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L312
	cmpl	%r15d, %r13d
	jne	.L469
.L312:
	movq	448(%rbx), %rax
	testq	%rax, %rax
	je	.L315
	movq	456(%rbx), %rdi
	movl	%r12d, %esi
	call	*%rax
	cmpl	$23, %eax
	jne	.L314
.L315:
	movl	%r12d, %edi
	call	ubidi_getClass_67@PLT
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L306:
	testl	%ecx, %ecx
	jne	.L375
.L376:
	movq	208(%r15), %rax
	movzbl	-1079(%rbp), %edx
	movb	$0, -1080(%rbp)
	movb	$10, -1078(%rbp)
	movl	%edx, 4(%rax)
	movl	$0, -1088(%rbp)
.L309:
	movl	-1076(%rbp), %eax
	testl	%eax, %eax
	jle	.L470
	movq	-1096(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%r15, %rbx
	xorl	%ecx, %ecx
	movl	$-1, -1108(%rbp)
	movl	%r13d, %r15d
	movq	%rbx, %r13
	movl	$0, -1116(%rbp)
	movzwl	(%rax), %eax
	movl	$-1, -1112(%rbp)
	.p2align 4,,10
	.p2align 3
.L322:
	leal	1(%rcx), %ebx
	movzwl	%ax, %r12d
	andl	$64512, %eax
	movslq	%ebx, %r14
	cmpl	$55296, %eax
	jne	.L324
	cmpl	%ebx, -1076(%rbp)
	jne	.L471
.L324:
	movq	448(%r13), %rax
	testq	%rax, %rax
	je	.L327
	movq	456(%r13), %rdi
	movl	%r12d, %esi
	call	*%rax
	movl	%eax, %ecx
	cmpl	$23, %eax
	je	.L327
	cmpl	$22, %ecx
	jg	.L380
.L476:
	movl	$1, %eax
	salq	%cl, %rax
.L328:
	movq	-1104(%rbp), %rsi
	orl	%eax, %r15d
	leaq	-1(%rsi,%r14), %rax
	movb	%cl, (%rax)
	cmpl	$65535, %r12d
	jle	.L329
	orl	$262144, %r15d
	cmpb	$0, -1077(%rbp)
	movb	$18, -2(%rsi,%r14)
	je	.L331
.L330:
	leal	-8294(%r12), %edi
	cmpl	$3, %edi
	ja	.L331
	.p2align 4,,10
	.p2align 3
.L332:
	addl	$1, -1116(%rbp)
.L331:
	testb	%cl, %cl
	je	.L472
	cmpb	$1, %cl
	je	.L389
	cmpb	$13, %cl
	je	.L389
	leal	-19(%rcx), %edi
	cmpb	$2, %dil
	jbe	.L473
	cmpb	$22, %cl
	je	.L474
	cmpb	$7, %cl
	je	.L475
	.p2align 4,,10
	.p2align 3
.L335:
	cmpl	-1076(%rbp), %ebx
	jge	.L323
.L478:
	movq	-1096(%rbp), %rax
	movzwl	(%rax,%r14,2), %eax
.L374:
	movl	%ebx, %ecx
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L329:
	cmpb	$0, -1077(%rbp)
	je	.L331
	movl	%r12d, %edi
	andl	$-4, %edi
	cmpl	$8204, %edi
	je	.L332
	leal	-8234(%r12), %edi
	cmpl	$4, %edi
	jbe	.L332
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L327:
	movl	%r12d, %edi
	call	ubidi_getClass_67@PLT
	movl	%eax, %ecx
	cmpl	$22, %ecx
	jle	.L476
.L380:
	movl	$10, %ecx
	movl	$1024, %eax
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L472:
	cmpl	$1, -1088(%rbp)
	je	.L477
	cmpl	$2, -1088(%rbp)
	movb	$0, -1078(%rbp)
	jne	.L335
	cmpl	$125, -1108(%rbp)
	movl	$3, -1088(%rbp)
	jg	.L335
	orl	$1048576, %r15d
	cmpl	-1076(%rbp), %ebx
	jl	.L478
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%r13, %rax
	movl	%r15d, %r13d
	cmpb	$1, -1078(%rbp)
	movl	136(%rax), %ecx
	movq	%rax, %r15
	movl	-1108(%rbp), %eax
	sete	%sil
	andb	-1080(%rbp), %sil
	cmpl	$125, %eax
	jg	.L361
.L483:
	cmpl	$-1, %eax
	je	.L321
	cmpl	$2, -1088(%rbp)
	je	.L361
	movslq	-1108(%rbp), %rax
	leaq	-560(%rbp), %rdi
	leaq	-560(%rbp,%rax,4), %rax
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L364:
	subq	$4, %rax
	cmpl	$2, %edx
	je	.L361
.L363:
	movl	(%rax), %edx
	cmpq	%rax, %rdi
	jne	.L364
	andl	$4, %ecx
	jne	.L479
.L365:
	movslq	200(%r15), %rcx
	movq	208(%r15), %rax
	movl	-1076(%rbp), %ebx
	movq	%rcx, %rdx
	movl	%ebx, -8(%rax,%rcx,8)
	movl	-1116(%rbp), %eax
	movl	%eax, 440(%r15)
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L471:
	movq	-1096(%rbp), %rax
	movzwl	(%rax,%r14,2), %eax
	movl	%eax, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L324
	movl	%r12d, %esi
	leal	2(%rcx), %ebx
	sall	$10, %esi
	movslq	%ebx, %r14
	leal	-56613888(%rax,%rsi), %r12d
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L389:
	cmpl	$1, -1088(%rbp)
	je	.L480
	cmpl	$2, -1088(%rbp)
	je	.L481
.L339:
	movl	-1112(%rbp), %eax
	cmpb	$13, %cl
	leal	-1(%rbx), %esi
	movb	$1, -1078(%rbp)
	cmove	%esi, %eax
	movl	%eax, -1112(%rbp)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L473:
	addl	$1, -1108(%rbp)
	movl	-1108(%rbp), %edx
	cmpl	$125, %edx
	jg	.L341
	movslq	%edx, %rsi
	leal	-1(%rbx), %edi
	movl	%edi, -1072(%rbp,%rsi,4)
	movl	-1088(%rbp), %edi
	movl	%edi, -560(%rbp,%rsi,4)
.L341:
	movl	$3, -1088(%rbp)
	cmpb	$19, %cl
	jne	.L335
	movb	$20, (%rax)
	movl	$2, -1088(%rbp)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L477:
	movslq	200(%r13), %rcx
	movq	208(%r13), %rax
	movb	$0, -1078(%rbp)
	movl	$0, -1088(%rbp)
	movl	$0, -4(%rax,%rcx,8)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L475:
	cmpl	-1076(%rbp), %ebx
	jge	.L346
	cmpl	$13, %r12d
	jne	.L346
	movq	-1096(%rbp), %rax
	movzwl	(%rax,%r14,2), %eax
	cmpw	$10, %ax
	je	.L374
.L346:
	movslq	200(%r13), %rcx
	movq	208(%r13), %rdi
	cmpb	$1, -1078(%rbp)
	movq	%rcx, %rax
	leaq	-8(%rdi,%rcx,8), %rcx
	sete	%sil
	movl	%ebx, (%rcx)
	andb	-1080(%rbp), %sil
	je	.L348
	movl	$1, 4(%rcx)
.L348:
	movl	136(%r13), %ecx
	testb	$4, %cl
	je	.L349
	movl	-1116(%rbp), %edx
	movl	%ebx, 20(%r13)
	movl	%edx, 440(%r13)
.L349:
	cmpl	-1076(%rbp), %ebx
	jl	.L482
	movq	%r13, %rax
	movl	%r15d, %r13d
	movq	%rax, %r15
	movl	-1108(%rbp), %eax
	cmpl	$125, %eax
	jle	.L483
.L361:
	orl	$1048576, %r13d
.L321:
	andl	$4, %ecx
	je	.L365
.L479:
	movl	-1076(%rbp), %eax
	cmpl	%eax, 20(%r15)
	jl	.L461
.L466:
	movl	200(%r15), %edx
.L366:
	testb	%sil, %sil
	je	.L367
	movq	208(%r15), %rcx
	movslq	%edx, %rax
	movl	$1, -4(%rcx,%rax,8)
.L367:
	cmpb	$-3, -1079(%rbp)
	jbe	.L368
	movq	208(%r15), %rax
	movl	4(%rax), %eax
	movb	%al, 141(%r15)
.L368:
	testl	%edx, %edx
	jle	.L369
	movq	208(%r15), %rcx
	subl	$1, %edx
	leaq	4(%rcx), %rax
	leaq	12(%rcx,%rdx,8), %rsi
	leaq	_ZL6flagLR(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L370:
	movl	(%rax), %edx
	addq	$8, %rax
	andl	$1, %edx
	orl	(%rcx,%rdx,4), %r13d
	cmpq	%rax, %rsi
	jne	.L370
.L369:
	cmpb	$0, 140(%r15)
	je	.L371
	movl	%r13d, %eax
	orl	$1, %eax
	testb	$-128, %r13b
	cmovne	%eax, %r13d
.L371:
	movl	-1112(%rbp), %eax
	movl	%r13d, 188(%r15)
	movl	%eax, 192(%r15)
	movl	$1, %eax
.L305:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L484
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	movslq	200(%r13), %rsi
	movq	208(%r13), %rax
	movl	$0, -1088(%rbp)
	movl	$1, -4(%rax,%rsi,8)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L474:
	cmpl	$2, -1088(%rbp)
	movl	-1108(%rbp), %eax
	je	.L485
	cmpl	$-1, %eax
	je	.L335
	cmpl	$125, %eax
	jg	.L344
.L345:
	movslq	-1108(%rbp), %rax
	movl	-560(%rbp,%rax,4), %eax
	movl	%eax, -1088(%rbp)
.L344:
	subl	$1, -1108(%rbp)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L481:
	movslq	-1108(%rbp), %rax
	movl	$3, -1088(%rbp)
	cmpl	$125, %eax
	jg	.L339
	movslq	-1072(%rbp,%rax,4), %rax
	movq	-1104(%rbp), %rdi
	orl	$2097152, %r15d
	movb	$21, (%rdi,%rax)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L482:
	addl	$1, %eax
	leaq	216(%r13), %r12
	movl	%eax, 200(%r13)
	cmpq	%r12, %rdi
	je	.L486
	movq	80(%r13), %r12
	movl	%eax, %ecx
	sall	$4, %ecx
	testq	%r12, %r12
	je	.L487
	cmpl	40(%r13), %ecx
	jle	.L359
	movq	%r12, %rdi
	movslq	%ecx, %rsi
	movl	%ecx, -1088(%rbp)
	call	uprv_realloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L357
	movq	%rax, 80(%r13)
.L465:
	movl	-1088(%rbp), %ecx
	movl	200(%r13), %eax
	movl	%ecx, 40(%r13)
.L359:
	movq	%r12, 208(%r13)
.L352:
	movq	-1096(%rbp), %rdi
	cltq
	cmpb	$-3, -1079(%rbp)
	leaq	-8(%r12,%rax,8), %rax
	leaq	(%rdi,%r14,2), %rcx
	jbe	.L360
	movzbl	-1117(%rbp), %esi
	movl	$-1, -1108(%rbp)
	movl	$1, -1088(%rbp)
	movl	%esi, 4(%rax)
	movzwl	(%rcx), %eax
	movl	%ebx, %ecx
	movb	%sil, -1078(%rbp)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L375:
	movl	$0, 20(%rdi)
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L467:
	movb	$1, -1080(%rbp)
	testl	%ecx, %ecx
	je	.L373
.L372:
	movl	$0, 20(%r15)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L469:
	movzwl	2(%rdi,%rsi), %edx
	movl	%edx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L312
	sall	$10, %r12d
	leal	2(%rax), %r15d
	leal	-56613888(%rdx,%r12), %r12d
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L468:
	cmpb	$1, %al
	jbe	.L388
	cmpb	$13, %al
	jne	.L316
.L388:
	movl	%edx, %r14d
	cmpl	%r15d, %r13d
	jg	.L488
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%rbx, %r15
	cmpb	$10, %r14b
	je	.L379
	movq	208(%rbx), %rax
	testb	%r14b, %r14b
	jne	.L319
	movl	$0, 4(%rax)
	movzbl	-1117(%rbp), %eax
	movl	$0, -1088(%rbp)
	movb	%al, -1078(%rbp)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L485:
	cmpl	$125, %eax
	jg	.L344
	orl	$1048576, %r15d
	cmpl	$-1, %eax
	jne	.L345
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L486:
	cmpl	$10, %eax
	jle	.L352
	movq	80(%r13), %rdi
	testq	%rdi, %rdi
	je	.L489
	cmpl	$159, 40(%r13)
	jg	.L356
	movl	$160, %esi
	call	uprv_realloc_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L357
	movq	%rax, 80(%r13)
.L464:
	movl	$160, 40(%r13)
.L356:
	movdqu	216(%r13), %xmm0
	movq	%rdi, 208(%r13)
	movups	%xmm0, (%rdi)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rdi)
	movdqu	48(%r12), %xmm3
	movups	%xmm3, 48(%rdi)
	movdqu	64(%r12), %xmm4
	movups	%xmm4, 64(%rdi)
	movq	208(%r13), %r12
	movl	200(%r13), %eax
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L461:
	subl	$1, 200(%r15)
	jmp	.L466
.L360:
	movzbl	141(%r13), %esi
	movl	$-1, -1108(%rbp)
	movl	$0, -1088(%rbp)
	movl	%esi, 4(%rax)
	movzwl	(%rcx), %eax
	movl	%ebx, %ecx
	jmp	.L322
.L379:
	movzbl	-1117(%rbp), %eax
	movl	$1, -1088(%rbp)
	movb	%al, -1078(%rbp)
	jmp	.L309
.L487:
	movslq	%ecx, %rdi
	movl	%ecx, -1088(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, 80(%r13)
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L465
.L357:
	xorl	%eax, %eax
	jmp	.L305
.L319:
	movl	$1, 4(%rax)
	movzbl	-1117(%rbp), %eax
	movl	$0, -1088(%rbp)
	movb	%al, -1078(%rbp)
	jmp	.L309
.L470:
	cmpb	$1, -1078(%rbp)
	movl	136(%r15), %ecx
	movl	$0, -1116(%rbp)
	movl	$-1, -1112(%rbp)
	sete	%sil
	xorl	%r13d, %r13d
	andb	-1080(%rbp), %sil
	jmp	.L321
.L489:
	movl	$160, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 80(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L464
	xorl	%eax, %eax
	jmp	.L305
.L484:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2107:
	.size	_ZL11getDirPropsP5UBiDi, .-_ZL11getDirPropsP5UBiDi
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.type	_ZL18processPropertySeqP5UBiDiP8LevStatehii, @function
_ZL18processPropertySeqP5UBiDiP8LevStatehii:
.LFB2123:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%dl, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%ecx, %rbx
	subq	$56, %rsp
	movq	(%rsi), %rcx
	movzbl	28(%rsi), %r8d
	movq	8(%rsi), %rdi
	movq	120(%r14), %r9
	leaq	(%rcx,%r8,8), %rsi
	movzbl	(%rsi,%rax), %eax
	movl	%eax, %esi
	shrq	$4, %rax
	andl	$15, %esi
	andl	$15, %eax
	movl	%esi, 28(%r12)
	movzbl	(%rdi,%rax), %eax
	movslq	%esi, %rsi
	movzbl	7(%rcx,%rsi,8), %r15d
	testb	%al, %al
	jne	.L676
.L673:
	xorl	%ecx, %ecx
.L508:
	testb	%r15b, %r15b
	jne	.L597
	testb	%cl, %cl
	jne	.L597
.L490:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L676:
	.cfi_restore_state
	cmpb	$14, %al
	ja	.L492
	leaq	.L494(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L494:
	.long	.L492-.L494
	.long	.L670-.L494
	.long	.L506-.L494
	.long	.L505-.L494
	.long	.L504-.L494
	.long	.L503-.L494
	.long	.L502-.L494
	.long	.L501-.L494
	.long	.L500-.L494
	.long	.L499-.L494
	.long	.L498-.L494
	.long	.L497-.L494
	.long	.L496-.L494
	.long	.L495-.L494
	.long	.L493-.L494
	.text
	.p2align 4,,10
	.p2align 3
.L597:
	addb	36(%r12), %r15b
	cmpl	%ebx, 32(%r12)
	jg	.L598
	cmpl	%r13d, %ebx
	jge	.L490
	leal	-1(%r13), %edx
	addq	$56, %rsp
	movslq	%ebx, %rdi
	movzbl	%r15b, %esi
	movl	%edx, %r13d
	addq	%r9, %rdi
	subl	%ebx, %r13d
	popq	%rbx
	popq	%r12
	leaq	1(%r13), %rdx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memset@PLT
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore_state
	movq	112(%r14), %rcx
	movq	120(%r14), %rsi
	cmpl	%r13d, %ebx
	jge	.L490
	xorl	%edx, %edx
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L600:
	testl	%edx, %edx
	jne	.L603
.L601:
	movb	%r15b, (%rsi,%rbx)
	xorl	%edx, %edx
.L603:
	subl	$20, %eax
	cmpb	$2, %al
	adcl	$0, %edx
.L602:
	addq	$1, %rbx
	cmpl	%ebx, %r13d
	jle	.L490
.L604:
	movzbl	(%rcx,%rbx), %eax
	cmpb	$22, %al
	jne	.L600
	subl	$1, %edx
	jne	.L602
	jmp	.L601
.L496:
	movzbl	36(%r12), %edx
	movl	16(%r12), %esi
	addl	%r15d, %edx
	cmpl	%esi, %ebx
	jle	.L584
	movslq	%esi, %rdi
	leal	-1(%rbx), %ecx
	subl	%esi, %ecx
	leaq	1(%r9,%rdi), %rsi
	leaq	(%r9,%rdi), %rax
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L585:
	cmpb	%dl, (%rax)
	jnb	.L583
	movb	%dl, (%rax)
.L583:
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L585
.L584:
	movl	420(%r14), %eax
.L674:
	movl	%eax, 424(%r14)
.L670:
	movl	%ebx, 16(%r12)
	jmp	.L673
.L506:
	movl	16(%r12), %eax
	cmpl	%eax, %ebx
	movslq	%eax, %rbx
	setg	%cl
	jmp	.L508
.L493:
	movzbl	36(%r12), %eax
	movl	16(%r12), %ecx
	leal	1(%rax), %esi
	leal	-1(%rbx), %eax
	cmpl	%ecx, %eax
	jl	.L673
	cltq
	.p2align 4,,10
	.p2align 3
.L596:
	movzbl	(%r9,%rax), %edx
	cmpb	%sil, %dl
	jbe	.L594
.L677:
	subl	$2, %edx
	movb	%dl, (%r9,%rax)
	movl	16(%r12), %ecx
	subq	$1, %rax
	cmpl	%eax, %ecx
	jg	.L673
	movzbl	(%r9,%rax), %edx
	cmpb	%sil, %dl
	ja	.L677
.L594:
	subq	$1, %rax
	cmpl	%eax, %ecx
	jle	.L596
	jmp	.L673
.L495:
	movzbl	36(%r12), %r8d
	leal	-1(%rbx), %esi
	cmpl	16(%r12), %esi
	jl	.L673
	movzbl	%r8b, %eax
	movb	%r15b, -64(%rbp)
	leal	3(%rax), %r11d
	leal	2(%rax), %r10d
	movq	%r14, -72(%rbp)
	leaq	-1(%r9), %rax
	movq	%rax, -56(%rbp)
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L587:
	cmpl	%ecx, %r10d
	setne	%dl
	subl	$1, %esi
	addl	%r8d, %edx
	movb	%dl, (%rax)
	cmpl	%esi, 16(%r12)
	jg	.L678
.L593:
	movslq	%esi, %r14
	leaq	(%r9,%r14), %rax
	movzbl	(%rax), %ecx
	movl	%ecx, %edx
	cmpl	%r11d, %ecx
	jne	.L587
	leaq	-1(%r9,%r14), %rdi
	leal	-1(%rsi), %r15d
	addq	-56(%rbp), %r14
	subl	%eax, %r15d
	subq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L590:
	subl	$2, %edx
	leal	(%r15,%rax), %esi
	subq	$1, %rdi
	movb	%dl, (%rax)
	movzbl	1(%rdi), %ecx
	addq	%r14, %rax
	movl	%ecx, %edx
	cmpl	%r11d, %ecx
	je	.L590
	cmpb	%r8b, %dl
	jne	.L587
	movslq	%esi, %rax
	leal	-1(%rsi), %edi
	leaq	-1(%r9,%rax), %rdx
	subl	%edx, %edi
	.p2align 4,,10
	.p2align 3
.L591:
	leal	(%rdi,%rdx), %esi
	movq	%rdx, %rax
	movzbl	(%rdx), %ecx
	leaq	-1(%rdx), %rdx
	cmpb	%r8b, %cl
	je	.L591
	jmp	.L587
.L497:
	movl	424(%r14), %eax
	movl	%eax, 420(%r14)
	cmpb	$5, %dl
	jne	.L673
	movl	416(%r14), %edx
	testl	%edx, %edx
	je	.L576
	movq	432(%r14), %rcx
.L577:
	cmpl	%edx, %eax
	jge	.L679
.L580:
	movslq	%eax, %rdx
	addl	$1, %eax
	leaq	(%rcx,%rdx,8), %rdx
	movl	%ebx, (%rdx)
	movl	$4, 4(%rdx)
	movl	%eax, 420(%r14)
.L579:
	movl	%eax, 424(%r14)
	xorl	%ecx, %ecx
	jmp	.L508
.L498:
	movl	416(%r14), %eax
	testl	%eax, %eax
	je	.L566
	movq	432(%r14), %rdx
.L567:
	movl	420(%r14), %ecx
	cmpl	%eax, %ecx
	jge	.L680
.L570:
	movslq	%ecx, %rsi
	addl	$1, %ecx
	leaq	(%rdx,%rsi,8), %rsi
	movl	%ebx, (%rsi)
	movl	$1, 4(%rsi)
	movl	%ecx, 420(%r14)
.L569:
	testl	%eax, %eax
	je	.L681
.L572:
	movl	420(%r14), %ecx
	cmpl	%eax, %ecx
	jge	.L682
.L574:
	movslq	%ecx, %rax
	addl	$1, %ecx
	leaq	(%rdx,%rax,8), %rax
	movl	%ebx, (%rax)
	movl	$2, 4(%rax)
	movl	%ecx, 420(%r14)
	xorl	%ecx, %ecx
	jmp	.L508
.L499:
	movl	%ebx, %eax
	subl	$1, %eax
	js	.L670
	cltq
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L683:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L670
.L561:
	movl	%eax, %edx
	testb	$1, (%r9,%rax)
	je	.L683
	movl	416(%r14), %ecx
	testl	%ecx, %ecx
	je	.L605
	movq	432(%r14), %rdi
.L606:
	movl	420(%r14), %eax
	cmpl	%ecx, %eax
	jge	.L684
.L564:
	movslq	%eax, %rcx
	addl	$1, %eax
	leaq	(%rdi,%rcx,8), %rcx
	movl	%edx, (%rcx)
	movl	$4, 4(%rcx)
	movl	%eax, 420(%r14)
	jmp	.L674
.L502:
	movl	416(%r14), %eax
	testl	%eax, %eax
	jle	.L546
	movl	424(%r14), %eax
	movl	%eax, 420(%r14)
.L546:
	movq	$-1, 16(%r12)
	leal	-1(%r13), %eax
	xorl	%ecx, %ecx
	movl	%eax, 24(%r12)
	jmp	.L508
.L500:
	movl	$-1, 16(%r12)
	leal	-1(%r13), %eax
	xorl	%ecx, %ecx
	movl	%eax, 24(%r12)
	jmp	.L508
.L501:
	movl	20(%r12), %ecx
	cmpb	$3, %dl
	je	.L685
.L547:
	cmpl	$-1, %ecx
	jne	.L673
	movl	%ebx, 20(%r12)
	xorl	%ecx, %ecx
	jmp	.L508
.L504:
	movzbl	36(%r12), %eax
	movq	112(%r14), %rsi
	leal	2(%rax), %edi
	movslq	16(%r12), %rax
	cmpl	%ebx, %eax
	jge	.L673
	xorl	%ecx, %ecx
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L514:
	testl	%ecx, %ecx
	jne	.L517
.L515:
	movb	%dil, (%r9,%rax)
	xorl	%ecx, %ecx
.L517:
	subl	$20, %edx
	cmpb	$2, %dl
	adcl	$0, %ecx
.L516:
	addq	$1, %rax
	cmpl	%eax, %ebx
	jle	.L673
.L518:
	movzbl	(%rsi,%rax), %edx
	cmpb	$22, %dl
	jne	.L514
	subl	$1, %ecx
	je	.L515
	jmp	.L516
.L503:
	movl	20(%r12), %r10d
	movl	416(%r14), %eax
	testl	%r10d, %r10d
	jns	.L686
.L519:
	movl	$-1, 20(%r12)
	testl	%eax, %eax
	je	.L525
	movl	420(%r14), %esi
	cmpl	424(%r14), %esi
	jle	.L525
	movslq	24(%r12), %rax
	leal	1(%rax), %ecx
	cmpl	%ecx, %ebx
	jle	.L539
	leal	-2(%rbx), %esi
	leal	-1(%rbx), %r8d
	subl	%eax, %esi
	subl	%eax, %r8d
	cmpl	$14, %esi
	jbe	.L536
	movl	%r8d, %edi
	movdqa	.LC0(%rip), %xmm1
	leaq	1(%r9,%rax), %rsi
	xorl	%eax, %eax
	shrl	$4, %edi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L538:
	movdqu	(%rsi,%rax), %xmm0
	paddb	%xmm1, %xmm0
	pand	%xmm1, %xmm0
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L538
	movl	%r8d, %eax
	andl	$-16, %eax
	addl	%eax, %ecx
	cmpl	%eax, %r8d
	je	.L664
.L536:
	movslq	%ecx, %rsi
	addq	%r9, %rsi
	movzbl	(%rsi), %eax
	subl	$2, %eax
	andl	$-2, %eax
	movb	%al, (%rsi)
	leal	1(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	2(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	3(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	4(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	5(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	6(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	7(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	8(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	9(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	10(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	11(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	12(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	leal	13(%rcx), %eax
	cmpl	%eax, %ebx
	jle	.L664
	cltq
	addl	$14, %ecx
	addq	%r9, %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %esi
	andl	$-2, %esi
	movb	%sil, (%rax)
	cmpl	%ecx, %ebx
	jle	.L664
	movslq	%ecx, %rcx
	leaq	(%r9,%rcx), %rax
	movzbl	(%rax), %edi
	leal	-2(%rdi), %ecx
	andl	$-2, %ecx
	movb	%cl, (%rax)
.L664:
	movl	420(%r14), %esi
.L539:
	movl	%esi, 424(%r14)
	movl	$-1, 24(%r12)
	cmpb	$5, %dl
	jne	.L673
	movl	416(%r14), %eax
	testl	%eax, %eax
	je	.L687
.L541:
	movq	432(%r14), %rdx
	cmpl	%eax, %esi
	jge	.L688
.L544:
	movslq	%esi, %rax
	addl	$1, %esi
	leaq	(%rdx,%rax,8), %rax
	movl	%ebx, (%rax)
	movl	$1, 4(%rax)
	movl	%esi, 420(%r14)
.L543:
	movl	%esi, 424(%r14)
	xorl	%ecx, %ecx
	jmp	.L508
.L505:
	movzbl	36(%r12), %eax
	movq	112(%r14), %rsi
	leal	1(%rax), %edi
	movslq	16(%r12), %rax
	cmpl	%ebx, %eax
	jge	.L673
	xorl	%ecx, %ecx
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L509:
	testl	%ecx, %ecx
	jne	.L512
.L510:
	movb	%dil, (%r9,%rax)
	xorl	%ecx, %ecx
.L512:
	subl	$20, %edx
	cmpb	$2, %dl
	adcl	$0, %ecx
.L511:
	addq	$1, %rax
	cmpl	%eax, %ebx
	jle	.L673
.L513:
	movzbl	(%rsi,%rax), %edx
	cmpb	$22, %dl
	jne	.L509
	subl	$1, %ecx
	je	.L510
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L678:
	movzbl	-64(%rbp), %r15d
	movq	-72(%rbp), %r14
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L525:
	movl	$-1, 24(%r12)
	testb	$1, 7(%rcx,%r8,8)
	je	.L608
	movl	16(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L608
	cmpl	%ebx, %r8d
	setl	%cl
.L527:
	cmpb	$5, %dl
	je	.L689
	movslq	%r8d, %rbx
	jmp	.L508
.L608:
	movl	%ebx, %r8d
	xorl	%ecx, %ecx
	jmp	.L527
.L686:
	testl	%eax, %eax
	je	.L520
	movq	432(%r14), %rdi
.L521:
	movl	420(%r14), %esi
	cmpl	%eax, %esi
	jge	.L690
.L523:
	movslq	%esi, %r11
	addl	$1, %esi
	leaq	(%rdi,%r11,8), %rdi
	movl	%r10d, (%rdi)
	movl	$1, 4(%rdi)
	movl	%esi, 420(%r14)
	jmp	.L519
.L685:
	movq	112(%r14), %rdx
	movslq	%ebx, %rax
	cmpb	$5, (%rdx,%rax)
	jne	.L547
	cmpl	$6, 132(%r14)
	je	.L547
	cmpl	$-1, %ecx
	je	.L691
	movl	416(%r14), %eax
	movl	%eax, %edx
	testl	%ecx, %ecx
	jns	.L692
.L549:
	testl	%eax, %eax
	je	.L693
.L555:
	movl	420(%r14), %eax
	movq	432(%r14), %rcx
	cmpl	%edx, %eax
	jge	.L694
.L557:
	movslq	%eax, %rdx
	addl	$1, %eax
	leaq	(%rcx,%rdx,8), %rdx
	xorl	%ecx, %ecx
	movl	%ebx, (%rdx)
	movl	$1, 4(%rdx)
	movl	%eax, 420(%r14)
	jmp	.L508
.L682:
	leal	(%rax,%rax), %esi
	movq	%rdx, %rdi
	movq	%r9, -64(%rbp)
	movslq	%esi, %rsi
	movq	%rdx, -56(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	je	.L695
	movl	420(%r14), %ecx
	sall	416(%r14)
	movq	%rax, %rdx
	jmp	.L574
.L681:
	movl	$80, %edi
	movq	%r9, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movq	%rax, %rdx
	je	.L672
	movl	$10, 416(%r14)
	movl	$10, %eax
	jmp	.L572
.L680:
	leal	(%rax,%rax), %esi
	movq	%rdx, %rdi
	movq	%r9, -64(%rbp)
	movslq	%esi, %rsi
	movq	%rdx, -56(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movq	%rax, %rsi
	je	.L696
	movl	416(%r14), %eax
	movl	420(%r14), %ecx
	movq	%rsi, %rdx
	addl	%eax, %eax
	movl	%eax, 416(%r14)
	jmp	.L570
.L566:
	movl	$80, %edi
	movq	%r9, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movq	%rax, %rdx
	je	.L668
	movl	$10, 416(%r14)
	movl	$10, %eax
	jmp	.L567
.L689:
	testl	%eax, %eax
	je	.L528
	movq	432(%r14), %rdx
.L529:
	movl	420(%r14), %esi
	cmpl	%eax, %esi
	jge	.L697
.L532:
	movslq	%esi, %rax
	addl	$1, %esi
	leaq	(%rdx,%rax,8), %rax
	movl	%ebx, (%rax)
	movl	$1, 4(%rax)
	movl	%esi, 420(%r14)
.L531:
	movl	%esi, 424(%r14)
	movslq	%r8d, %rbx
	jmp	.L508
.L690:
	leal	(%rax,%rax), %esi
	movq	%r8, -96(%rbp)
	movslq	%esi, %rsi
	movl	%edx, -88(%rbp)
	salq	$3, %rsi
	movq	%r9, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movl	%r10d, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rdi
	movl	-64(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movl	-88(%rbp), %edx
	movq	%rax, %r11
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r9
	movq	-96(%rbp), %r8
	je	.L698
	movl	416(%r14), %eax
	movl	420(%r14), %esi
	movq	%r11, %rdi
	addl	%eax, %eax
	movl	%eax, 416(%r14)
	jmp	.L523
.L520:
	movl	$80, %edi
	movq	%r8, -88(%rbp)
	movl	%edx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movl	%r10d, -56(%rbp)
	call	uprv_malloc_67@PLT
	movl	-56(%rbp), %r10d
	movq	-64(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movl	-80(%rbp), %edx
	movq	%rax, %rdi
	movq	-72(%rbp), %r9
	movq	-88(%rbp), %r8
	je	.L662
	movl	$10, 416(%r14)
	movl	$10, %eax
	jmp	.L521
.L684:
	leal	(%rcx,%rcx), %esi
	movl	%edx, -72(%rbp)
	movslq	%esi, %rsi
	movq	%r9, -64(%rbp)
	salq	$3, %rsi
	movq	%rdi, -56(%rbp)
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movl	-72(%rbp), %edx
	movq	%rax, %rcx
	je	.L699
	movl	420(%r14), %eax
	sall	416(%r14)
	movq	%rcx, %rdi
	jmp	.L564
.L605:
	movl	$80, %edi
	movl	%edx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %r9
	movl	-64(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movq	%rax, %rdi
	je	.L667
	movl	$10, 416(%r14)
	movl	$10, %ecx
	jmp	.L606
.L679:
	leal	(%rdx,%rdx), %esi
	movq	%rcx, %rdi
	movq	%r9, -64(%rbp)
	movslq	%esi, %rsi
	movq	%rcx, -56(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movq	%rax, %rdx
	je	.L700
	movl	420(%r14), %eax
	sall	416(%r14)
	movq	%rdx, %rcx
	jmp	.L580
.L576:
	movl	$80, %edi
	movq	%r9, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movq	%rax, %rcx
	je	.L669
	movl	$10, 416(%r14)
	movl	420(%r14), %eax
	movl	$10, %edx
	jmp	.L577
.L695:
	movq	%rdx, 432(%r14)
	xorl	%ecx, %ecx
	movl	$7, 428(%r14)
	jmp	.L508
.L696:
	movq	%rdx, 432(%r14)
.L668:
	movl	$7, 428(%r14)
	movl	416(%r14), %eax
	jmp	.L569
.L697:
	leal	(%rax,%rax), %esi
	movq	%rdx, %rdi
	movb	%cl, -80(%rbp)
	movslq	%esi, %rsi
	movq	%r9, -72(%rbp)
	salq	$3, %rsi
	movl	%r8d, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rdx
	movl	-64(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movq	-72(%rbp), %r9
	movzbl	-80(%rbp), %ecx
	je	.L701
	movl	420(%r14), %esi
	sall	416(%r14)
	movq	%rax, %rdx
	jmp	.L532
.L528:
	movl	$80, %edi
	movb	%cl, -72(%rbp)
	movq	%r9, -64(%rbp)
	movl	%r8d, -56(%rbp)
	call	uprv_malloc_67@PLT
	movl	-56(%rbp), %r8d
	movq	-64(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movzbl	-72(%rbp), %ecx
	movq	%rax, %rdx
	je	.L663
	movl	$10, 416(%r14)
	movl	$10, %eax
	jmp	.L529
.L691:
	leal	-1(%r13), %eax
	xorl	%ecx, %ecx
	movl	%eax, 24(%r12)
	jmp	.L508
.L698:
	movq	%rdi, 432(%r14)
.L662:
	movl	$7, 428(%r14)
	movl	416(%r14), %eax
	jmp	.L519
.L699:
	movq	%rdi, 432(%r14)
.L667:
	movl	$7, 428(%r14)
	movl	420(%r14), %eax
	jmp	.L674
.L688:
	leal	(%rax,%rax), %esi
	movq	%rdx, %rdi
	movq	%r9, -64(%rbp)
	movslq	%esi, %rsi
	movq	%rdx, -56(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	je	.L702
	movl	420(%r14), %esi
	sall	416(%r14)
	movq	%rax, %rdx
	jmp	.L544
.L687:
	movl	$80, %edi
	movq	%r9, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	je	.L665
	movl	$10, 416(%r14)
	movl	420(%r14), %esi
	movl	$10, %eax
	jmp	.L541
.L694:
	leal	(%rdx,%rdx), %esi
	movq	%rcx, %rdi
	movq	%r9, -64(%rbp)
	movslq	%esi, %rsi
	movq	%rcx, -56(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movq	%rax, %rdx
	je	.L703
	movl	420(%r14), %eax
	sall	416(%r14)
	movq	%rdx, %rcx
	jmp	.L557
.L692:
	testl	%eax, %eax
	je	.L704
.L550:
	cmpl	%eax, 420(%r14)
	jge	.L705
.L553:
	movslq	420(%r14), %rdi
	movq	432(%r14), %rsi
	movq	%rdi, %rdx
	leaq	(%rsi,%rdi,8), %rsi
	addl	$1, %edx
	movl	%ecx, (%rsi)
	movl	$1, 4(%rsi)
	movl	%edx, 420(%r14)
.L552:
	movl	$-2, 20(%r12)
	movl	%eax, %edx
	jmp	.L549
.L693:
	movl	$80, %edi
	movq	%r9, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	je	.L672
	movl	$10, 416(%r14)
	movl	$10, %edx
	jmp	.L555
.L700:
	movq	%rcx, 432(%r14)
.L669:
	movl	$7, 428(%r14)
	movl	420(%r14), %eax
	jmp	.L579
.L701:
	movq	%rdx, 432(%r14)
.L663:
	movl	$7, 428(%r14)
	movl	420(%r14), %esi
	jmp	.L531
.L705:
	movq	432(%r14), %rdx
	leal	(%rax,%rax), %esi
	movl	%ecx, -72(%rbp)
	movslq	%esi, %rsi
	movq	%r9, -64(%rbp)
	salq	$3, %rsi
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	movl	-72(%rbp), %ecx
	je	.L706
	sall	416(%r14)
	movl	416(%r14), %eax
	jmp	.L553
.L704:
	movl	$80, %edi
	movl	%ecx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %r9
	movl	-64(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, 432(%r14)
	je	.L666
	movl	$10, 416(%r14)
	movl	$10, %eax
	jmp	.L550
.L703:
	movq	%rcx, 432(%r14)
.L672:
	movl	$7, 428(%r14)
	jmp	.L673
.L702:
	movq	%rdx, 432(%r14)
.L665:
	movl	$7, 428(%r14)
	movl	420(%r14), %esi
	jmp	.L543
.L706:
	movq	%rdx, 432(%r14)
.L666:
	movl	$7, 428(%r14)
	movl	416(%r14), %eax
	jmp	.L552
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZL18processPropertySeqP5UBiDiP8LevStatehii.cold, @function
_ZL18processPropertySeqP5UBiDiP8LevStatehii.cold:
.LFSB2123:
.L492:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE2123:
	.text
	.size	_ZL18processPropertySeqP5UBiDiP8LevStatehii, .-_ZL18processPropertySeqP5UBiDiP8LevStatehii
	.section	.text.unlikely
	.size	_ZL18processPropertySeqP5UBiDiP8LevStatehii.cold, .-_ZL18processPropertySeqP5UBiDiP8LevStatehii.cold
.LCOLDE1:
	.text
.LHOTE1:
	.section	.text.unlikely
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4
	.type	_ZL21resolveImplicitLevelsP5UBiDiiihh, @function
_ZL21resolveImplicitLevelsP5UBiDiiihh:
.LFB2126:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movb	%cl, -100(%rbp)
	movq	112(%rdi), %rbx
	movb	%r8b, -103(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -102(%rbp)
	cmpl	%esi, 192(%rdi)
	jle	.L708
	cmpb	$0, 142(%rdi)
	jne	.L830
.L709:
	movzbl	141(%r15), %eax
	andl	$1, %eax
.L711:
	movb	$0, -102(%rbp)
	testl	%eax, %eax
	je	.L708
	movl	132(%r15), %eax
	subl	$5, %eax
	cmpl	$1, %eax
	setbe	-102(%rbp)
.L708:
	movq	120(%r15), %rax
	movslq	%r12d, %r11
	movl	%r12d, -64(%rbp)
	movq	$-1, -76(%rbp)
	movq	176(%r15), %rdx
	movzbl	(%rax,%r11), %eax
	movb	%al, -60(%rbp)
	andl	$1, %eax
	leaq	(%rdx,%rax,8), %rax
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	movq	%rdx, -96(%rbp)
	movq	%rax, -88(%rbp)
	testl	%r12d, %r12d
	jne	.L723
	movl	152(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L831
.L723:
	leaq	(%rbx,%r11), %rax
	cmpb	$22, (%rax)
	je	.L832
.L724:
	movl	$-1, -80(%rbp)
	xorl	%r10d, %r10d
	cmpb	$17, (%rax)
	jne	.L726
	movzbl	-100(%rbp), %r10d
	addl	$1, %r10d
.L726:
	movzbl	-100(%rbp), %edx
	movl	%r12d, %ecx
	leaq	-96(%rbp), %rsi
	movl	%r12d, %r8d
	movq	%r15, %rdi
	movq	%r11, -120(%rbp)
	movl	%r10d, -112(%rbp)
	movl	$0, -68(%rbp)
	call	_ZL18processPropertySeqP5UBiDiP8LevStatehii
	movq	-120(%rbp), %r11
	movl	-112(%rbp), %r10d
	movl	%r12d, %ecx
.L725:
	leal	-1(%r13), %eax
	movl	%eax, -120(%rbp)
	cmpl	%r13d, %r12d
	jg	.L727
	movzbl	-103(%rbp), %eax
	movl	%r12d, -100(%rbp)
	movq	%r11, %r14
	movq	%r15, %rdi
	movb	$1, -104(%rbp)
	leaq	_ZL11impTabProps(%rip), %r9
	movl	$-1, -144(%rbp)
	movq	%rax, -112(%rbp)
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L728:
	movzbl	(%rbx,%r14), %edx
	cmpb	$7, %dl
	je	.L833
	cmpb	$0, -102(%rbp)
	je	.L828
	movl	$1, %eax
	cmpb	$13, %dl
	je	.L733
	cmpb	$2, %dl
	jne	.L828
	cmpl	%r14d, -144(%rbp)
	jle	.L834
.L736:
	xorl	%eax, %eax
	cmpb	$13, -104(%rbp)
	sete	%al
	addl	$2, %eax
	.p2align 4,,10
	.p2align 3
.L733:
	movzwl	%r10w, %edx
	cltq
	movq	%rdx, %rsi
	salq	$4, %rsi
	addq	%r9, %rsi
	movzbl	(%rsi,%rax), %eax
	movl	%eax, %r10d
	sarl	$5, %eax
	andl	$31, %r10d
	movl	%eax, %r8d
.L738:
	testl	%eax, %eax
	je	.L740
.L814:
	salq	$4, %rdx
	movzbl	15(%r9,%rdx), %edx
	cmpw	$3, %r8w
	je	.L741
	ja	.L742
	cmpw	$1, %r8w
	je	.L739
	cmpw	$2, %r8w
	jne	.L835
	movl	%r15d, -100(%rbp)
.L740:
	addq	$1, %r14
	cmpl	%r14d, %r13d
	jl	.L829
.L745:
	movl	%r14d, %r15d
	movl	%r14d, %r11d
	cmpl	%r14d, %r13d
	jg	.L728
	movslq	-120(%rbp), %rax
	movzbl	(%rbx,%rax), %edx
	cmpl	%eax, %r12d
	jge	.L729
	movl	$382976, %esi
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L836:
	movzbl	-1(%rbx,%rax), %edx
	subq	$1, %rax
	cmpl	%eax, %r12d
	jge	.L729
.L730:
	movzbl	(%rbx,%rax), %edx
	btq	%rdx, %rsi
	jc	.L836
.L729:
	subl	$20, %edx
	cmpb	$1, %dl
	jbe	.L829
	movzwl	%r10w, %edx
	movq	-112(%rbp), %rax
	movq	%rdx, %rsi
	salq	$4, %rsi
	addq	%r9, %rsi
	movzbl	(%rsi,%rax), %eax
	movl	%eax, %r10d
	sarl	$5, %eax
	andl	$31, %r10d
	movl	%eax, %r8d
	cmpl	%r11d, %r13d
	jne	.L738
	testl	%eax, %eax
	jne	.L814
	movzbl	15(%rsi), %edx
.L739:
	leaq	-96(%rbp), %rsi
	movl	%r15d, %r8d
	addq	$1, %r14
	movq	%rdi, -128(%rbp)
	movl	%r10d, -136(%rbp)
	call	_ZL18processPropertySeqP5UBiDiP8LevStatehii
	movq	-128(%rbp), %rdi
	movl	%r15d, %ecx
	movl	-136(%rbp), %r10d
	leaq	_ZL11impTabProps(%rip), %r9
	cmpl	%r14d, %r13d
	jge	.L745
	.p2align 4,,10
	.p2align 3
.L829:
	movq	%rdi, %r15
.L727:
	cmpl	%r13d, 20(%r15)
	je	.L837
.L747:
	movslq	-120(%rbp), %rax
	movq	%rax, %rdi
	addq	%rbx, %rax
	movzbl	(%rax), %edx
	cmpl	%edi, %r12d
	jge	.L755
	movslq	%r13d, %rdx
	movl	$382976, %edi
	leaq	-2(%rbx,%rdx), %rsi
	leal	-2(%r13), %edx
	subl	%r12d, %edx
	subq	%rdx, %rsi
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L838:
	subq	$1, %rax
	movzbl	(%rax), %edx
	cmpq	%rsi, %rax
	je	.L755
.L756:
	movzbl	(%rax), %edx
	btq	%rdx, %rdi
	jc	.L838
.L755:
	subl	$20, %edx
	cmpb	$1, %dl
	ja	.L757
	cmpl	%r13d, 20(%r15)
	jg	.L839
.L757:
	movzbl	-103(%rbp), %edx
	leaq	-96(%rbp), %rsi
	movl	%r13d, %r8d
	movl	%r13d, %ecx
	movq	%r15, %rdi
	call	_ZL18processPropertySeqP5UBiDiP8LevStatehii
.L707:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L840
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	.cfi_restore_state
	cmpw	$4, %r8w
	jne	.L841
	movl	-100(%rbp), %r8d
	leaq	-96(%rbp), %rsi
	movl	%r10d, -136(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZL18processPropertySeqP5UBiDiP8LevStatehii
	movl	-100(%rbp), %ecx
	movq	-128(%rbp), %rdi
	movl	%r15d, -100(%rbp)
	movl	-136(%rbp), %r10d
	leaq	_ZL11impTabProps(%rip), %r9
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L828:
	leaq	_ZL9groupProp(%rip), %rax
	movzbl	(%rax,%rdx), %eax
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L833:
	movl	$-1, 324(%rdi)
	movl	$6, %eax
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L741:
	movl	-100(%rbp), %r8d
	leaq	-96(%rbp), %rsi
	movl	%r10d, -140(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdi, -128(%rbp)
	call	_ZL18processPropertySeqP5UBiDiP8LevStatehii
	movl	-100(%rbp), %ecx
	movq	-128(%rbp), %rdi
	movl	%r15d, %r8d
	movq	-136(%rbp), %rsi
	movl	$4, %edx
	call	_ZL18processPropertySeqP5UBiDiP8LevStatehii
	movq	-128(%rbp), %rdi
	movl	%r15d, %ecx
	movl	-140(%rbp), %r10d
	leaq	_ZL11impTabProps(%rip), %r9
	jmp	.L740
.L839:
	movl	324(%r15), %eax
	movl	-68(%rbp), %edx
	addl	$1, %eax
	movl	%eax, 324(%r15)
	cltq
	salq	$4, %rax
	addq	328(%r15), %rax
	movl	%edx, 8(%rax)
	movl	-80(%rbp), %edx
	movw	%r10w, 12(%rax)
	movl	%ecx, 4(%rax)
	movl	%edx, (%rax)
	jmp	.L707
.L830:
	movq	208(%rdi), %rdi
	cmpl	%esi, (%rdi)
	jg	.L709
	movl	200(%r15), %edx
	leal	-1(%rdx), %esi
	testl	%edx, %edx
	jle	.L712
	subl	$1, %edx
	leaq	8(%rdi), %rax
	movq	%rdx, %rsi
	leaq	(%rax,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L713:
	cmpq	%rax, %rcx
	je	.L712
	movq	%rax, %rdx
	addq	$8, %rax
	cmpl	-8(%rax), %r12d
	jge	.L713
.L714:
	movl	4(%rdx), %eax
	andl	$1, %eax
	jmp	.L711
.L831:
	movq	%rbx, -112(%rbp)
	movq	144(%r15), %r14
	movq	%r15, %rbx
	movl	%r13d, %r15d
	movq	%r11, -120(%rbp)
	movl	%r12d, -128(%rbp)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L843:
	testb	%al, %al
	je	.L763
	cmpb	$1, %al
	je	.L764
	cmpb	$13, %al
	je	.L764
	cmpb	$7, %al
	sete	%al
.L722:
	testl	%r12d, %r12d
	je	.L821
	testb	%al, %al
	jne	.L821
	movl	%r12d, %ecx
.L717:
	leal	-1(%rcx), %r12d
	movslq	%r12d, %rax
	movzwl	(%r14,%rax,2), %r13d
	leaq	(%rax,%rax), %rsi
	movl	%r13d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L842
.L718:
	movq	448(%rbx), %rax
	testq	%rax, %rax
	je	.L721
	movq	456(%rbx), %rdi
	movl	%r13d, %esi
	call	*%rax
	cmpl	$23, %eax
	je	.L721
.L720:
	cmpl	$22, %eax
	jle	.L843
	xorl	%eax, %eax
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L721:
	movl	%r13d, %edi
	call	ubidi_getClass_67@PLT
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L842:
	testl	%r12d, %r12d
	je	.L718
	movzwl	-2(%r14,%rsi), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L718
	sall	$10, %eax
	leal	-2(%rcx), %r12d
	leal	-56613888(%r13,%rax), %r13d
	jmp	.L718
.L837:
	movl	168(%r15), %r14d
	testl	%r14d, %r14d
	jle	.L747
	movq	160(%r15), %rdi
	movq	%rbx, -112(%rbp)
	xorl	%eax, %eax
	movl	%r12d, -128(%rbp)
	movq	%r15, %r12
	movl	%ecx, -100(%rbp)
	movq	%rdi, %rbx
	movw	%r10w, -102(%rbp)
	movl	%r13d, -136(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L752:
	cmpl	$22, %eax
	jg	.L754
	testb	%al, %al
	je	.L771
	cmpb	$1, %al
	je	.L772
	cmpb	$13, %al
	je	.L772
	cmpb	$2, %al
	je	.L773
	cmpb	$5, %al
	je	.L844
.L754:
	cmpl	%r13d, %r14d
	jle	.L825
	movl	%r13d, %eax
.L749:
	movslq	%eax, %rsi
	leal	1(%rax), %r13d
	movzwl	(%rbx,%rsi,2), %r15d
	leaq	(%rsi,%rsi), %rdi
	movl	%r15d, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L750
	cmpl	%r13d, %r14d
	jne	.L845
.L750:
	movq	448(%r12), %rax
	testq	%rax, %rax
	je	.L753
	movq	456(%r12), %rdi
	movl	%r15d, %esi
	call	*%rax
	cmpl	$23, %eax
	jne	.L752
.L753:
	movl	%r15d, %edi
	call	ubidi_getClass_67@PLT
	jmp	.L752
.L832:
	movl	324(%r15), %edx
	testl	%edx, %edx
	js	.L724
	movslq	%edx, %rax
	subl	$1, %edx
	salq	$4, %rax
	addq	328(%r15), %rax
	movl	(%rax), %ecx
	movzwl	12(%rax), %r10d
	movl	%ecx, -80(%rbp)
	movl	4(%rax), %ecx
	movl	8(%rax), %eax
	movl	%edx, 324(%r15)
	movl	%eax, -68(%rbp)
	jmp	.L725
.L834:
	leal	1(%r14), %eax
	cmpl	%eax, %r13d
	jle	.L768
	cltq
	.p2align 4,,10
	.p2align 3
.L737:
	movzbl	(%rbx,%rax), %edx
	movl	%eax, %esi
	cmpb	$1, %dl
	jbe	.L824
	cmpb	$13, %dl
	je	.L824
	addq	$1, %rax
	cmpl	%eax, %r13d
	jg	.L737
.L768:
	movl	%r13d, -144(%rbp)
	movl	$2, %eax
	movb	$1, -104(%rbp)
	jmp	.L733
.L845:
	movzwl	2(%rbx,%rdi), %esi
	movl	%esi, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L750
	movl	%r15d, %r8d
	leal	2(%rax), %r13d
	sall	$10, %r8d
	leal	-56613888(%rsi,%r8), %r15d
	jmp	.L750
.L824:
	movb	%dl, -104(%rbp)
	movl	%esi, -144(%rbp)
	jmp	.L736
.L821:
	movl	%r15d, %r13d
	movq	-120(%rbp), %r11
	movq	%rbx, %r15
	movl	-128(%rbp), %r12d
	movq	-112(%rbp), %rbx
	jmp	.L723
.L712:
	movslq	%esi, %rdx
	leaq	(%rdi,%rdx,8), %rdx
	jmp	.L714
.L764:
	movl	%r15d, %r13d
	movb	$1, -100(%rbp)
	movq	%rbx, %r15
	movq	-120(%rbp), %r11
	movl	-128(%rbp), %r12d
	movq	-112(%rbp), %rbx
	jmp	.L723
.L763:
	movl	%r15d, %r13d
	movb	$0, -100(%rbp)
	movq	%rbx, %r15
	movq	-120(%rbp), %r11
	movl	-128(%rbp), %r12d
	movq	-112(%rbp), %rbx
	jmp	.L723
.L773:
	movq	%r12, %r15
	movl	-100(%rbp), %ecx
	movq	-112(%rbp), %rbx
	movb	$2, -103(%rbp)
	movzwl	-102(%rbp), %r10d
	movl	-128(%rbp), %r12d
	movl	-136(%rbp), %r13d
	jmp	.L747
.L844:
	movq	%r12, %r15
	movl	-100(%rbp), %ecx
	movq	-112(%rbp), %rbx
	movb	$3, -103(%rbp)
	movzwl	-102(%rbp), %r10d
	movl	-128(%rbp), %r12d
	movl	-136(%rbp), %r13d
	jmp	.L747
.L771:
	movq	%r12, %r15
	movl	-100(%rbp), %ecx
	movq	-112(%rbp), %rbx
	movb	$0, -103(%rbp)
	movzwl	-102(%rbp), %r10d
	movl	-128(%rbp), %r12d
	movl	-136(%rbp), %r13d
	jmp	.L747
.L825:
	movq	%r12, %r15
	movl	-100(%rbp), %ecx
	movq	-112(%rbp), %rbx
	movzwl	-102(%rbp), %r10d
	movl	-128(%rbp), %r12d
	movl	-136(%rbp), %r13d
	jmp	.L747
.L772:
	movq	%r12, %r15
	movl	-100(%rbp), %ecx
	movq	-112(%rbp), %rbx
	movb	$1, -103(%rbp)
	movzwl	-102(%rbp), %r10d
	movl	-128(%rbp), %r12d
	movl	-136(%rbp), %r13d
	jmp	.L747
.L840:
	call	__stack_chk_fail@PLT
.L841:
	jmp	.L743
.L835:
	jmp	.L743
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZL21resolveImplicitLevelsP5UBiDiiihh.cold, @function
_ZL21resolveImplicitLevelsP5UBiDiiihh.cold:
.LFSB2126:
.L743:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE2126:
	.text
	.size	_ZL21resolveImplicitLevelsP5UBiDiiihh, .-_ZL21resolveImplicitLevelsP5UBiDiiihh
	.section	.text.unlikely
	.size	_ZL21resolveImplicitLevelsP5UBiDiiihh.cold, .-_ZL21resolveImplicitLevelsP5UBiDiiihh.cold
.LCOLDE2:
	.text
.LHOTE2:
	.section	.text.unlikely
.LCOLDB4:
	.text
.LHOTB4:
	.p2align 4
	.type	ubidi_setPara_67.part.0, @function
ubidi_setPara_67.part.0:
.LFB2734:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%r9, -56(%rbp)
	cmpl	$-1, %edx
	je	.L1198
	cmpl	$3, 132(%rbx)
	je	.L1199
.L848:
	movl	%r13d, %edx
	movl	%r13d, %eax
	pxor	%xmm0, %xmm0
	movq	%r14, 8(%rbx)
	andl	$1, %eax
	andl	$1, %edx
	cmpb	$-3, %r13b
	movl	%r11d, 24(%rbx)
	movq	$0, (%rbx)
	movl	%r11d, 16(%rbx)
	movl	%r11d, 20(%rbx)
	movb	%r13b, 141(%rbx)
	movl	%eax, 184(%rbx)
	movl	$1, 200(%rbx)
	movq	$0, 304(%rbx)
	movq	$0, 420(%rbx)
	movups	%xmm0, 112(%rbx)
	seta	142(%rbx)
	testl	%r11d, %r11d
	je	.L1200
	movq	80(%rbx), %rax
	movq	56(%rbx), %rdi
	leaq	216(%rbx), %rdx
	movl	$-1, 296(%rbx)
	testq	%rax, %rax
	cmove	%rdx, %rax
	movq	%rax, 208(%rbx)
	movzbl	104(%rbx), %eax
	testq	%rdi, %rdi
	je	.L1201
	cmpl	28(%rbx), %r11d
	jle	.L904
	testb	%al, %al
	jne	.L1202
.L906:
	movq	-56(%rbp), %rax
	movl	$7, (%rax)
.L846:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1200:
	.cfi_restore_state
	cmpb	$-3, %r13b
	jbe	.L900
	movb	%dl, 141(%rbx)
	movb	$0, 142(%rbx)
.L900:
	cltq
	movq	%rbx, (%rbx)
	leaq	_ZL6flagLR(%rip), %rdx
	movl	$0, 296(%rbx)
	movl	(%rdx,%rax,4), %eax
	movl	$0, 200(%rbx)
	movl	%eax, 188(%rbx)
	movl	$0, 152(%rbx)
	movl	$0, 168(%rbx)
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1201:
	.cfi_restore_state
	testb	%al, %al
	je	.L906
	movslq	%r11d, %rdi
	movl	%r11d, -64(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, 56(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L906
.L1193:
	movl	-64(%rbp), %r11d
	movl	%r11d, 28(%rbx)
.L904:
	movq	%rdi, 112(%rbx)
	movq	%rbx, %rdi
	call	_ZL11getDirPropsP5UBiDi
	testb	%al, %al
	je	.L906
	movl	20(%rbx), %r10d
	movq	112(%rbx), %r15
	movl	%r10d, 196(%rbx)
	testq	%r12, %r12
	je	.L1203
	movq	208(%rbx), %rax
	movq	%r12, 120(%rbx)
	movl	$0, 324(%rbx)
	movzbl	141(%rbx), %r9d
	movl	(%rax), %r13d
	testl	%r10d, %r10d
	jle	.L1204
	movl	$0, -64(%rbp)
	leal	-1(%r10), %r14d
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L923:
	movzbl	(%r15,%rax), %ecx
	movzbl	(%r12,%rax), %edx
	leal	-20(%rcx), %edi
	cmpb	$1, %dil
	jbe	.L1205
	cmpb	$22, %cl
	je	.L1206
	cmpb	$7, %cl
	movl	$0, %edi
	cmove	%edi, %r8d
.L914:
	cmpb	$0, 142(%rbx)
	je	.L916
	cmpl	%eax, %r13d
	je	.L1207
.L916:
	movl	%edx, %r11d
	movl	%edx, %edi
	andl	$127, %r11d
	andl	$127, %edi
	cmpl	%r9d, %r11d
	jl	.L917
	cmpb	$125, %dil
	jbe	.L1208
.L918:
	movq	-56(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	%rsi, %rdi
	call	u_strlen_67@PLT
	cmpl	$3, 132(%rbx)
	movl	%eax, %r11d
	jne	.L848
.L1199:
	movl	$0, 132(%rbx)
	testl	%r11d, %r11d
	je	.L1209
	movslq	%r11d, %r12
	movl	%r11d, -72(%rbp)
	leaq	0(,%r12,8), %rdi
	subq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movl	-72(%rbp), %r11d
	testq	%rax, %rax
	movq	%rax, -64(%rbp)
	je	.L1210
	movl	136(%rbx), %r15d
	testb	$1, %r15b
	je	.L854
	movl	%r15d, %eax
	andl	$-2, %eax
	orl	$2, %eax
	movl	%eax, 136(%rbx)
.L854:
	movl	%r13d, %eax
	movq	-56(%rbp), %r13
	andl	$1, %eax
	movl	0(%r13), %r10d
	movb	%al, -72(%rbp)
	testl	%r10d, %r10d
	jg	.L850
	cmpl	$-1, %r11d
	jl	.L856
	testq	%r14, %r14
	je	.L856
	movq	%r13, %r9
	xorl	%r8d, %r8d
	movl	%r11d, %edx
	movzbl	%al, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%r11d, -80(%rbp)
	call	ubidi_setPara_67.part.0
	movl	0(%r13), %r9d
	movl	-80(%rbp), %r11d
	testl	%r9d, %r9d
	jg	.L850
	movq	-64(%rbp), %rax
	movq	%rbx, %rdi
	movl	%r11d, -88(%rbp)
	leaq	(%rax,%r12,4), %rax
	leaq	(%rax,%r12,2), %r13
	movq	-56(%rbp), %r12
	movq	%rax, -80(%rbp)
	movq	%r12, %rsi
	call	ubidi_getLevels_67@PLT
	movslq	20(%rbx), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movl	%edx, -92(%rbp)
	call	memcpy@PLT
	movl	-88(%rbp), %r11d
	movq	-80(%rbp), %rsi
	movq	%r12, %r8
	movl	196(%rbx), %ecx
	movq	%rbx, %rdi
	movq	%r12, -56(%rbp)
	movl	%r11d, %edx
	movl	%ecx, -96(%rbp)
	movl	184(%rbx), %ecx
	movl	%ecx, -100(%rbp)
	movl	$2, %ecx
	call	ubidi_writeReordered_67@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	ubidi_getVisualMap_67@PLT
	movq	-56(%rbp), %rax
	movl	-88(%rbp), %r11d
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jle	.L1211
.L858:
	movl	-92(%rbp), %eax
	movl	-100(%rbp), %esi
	movq	%r14, 8(%rbx)
	cmpl	%eax, 32(%rbx)
	movq	120(%rbx), %rdi
	movl	%r11d, 16(%rbx)
	movl	%esi, 184(%rbx)
	movl	%eax, %edx
	cmovle	32(%rbx), %edx
	movq	%r13, %rsi
	movl	%eax, 20(%rbx)
	movslq	%edx, %rdx
	call	memcpy@PLT
	movl	-96(%rbp), %eax
	cmpl	$1, 296(%rbx)
	movl	%eax, 196(%rbx)
	jle	.L850
	movl	$2, 184(%rbx)
.L850:
	movq	-64(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	$3, 132(%rbx)
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1202:
	.cfi_restore_state
	movslq	%r11d, %rsi
	movl	%r11d, -64(%rbp)
	call	uprv_realloc_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L906
	movq	%rax, 56(%rbx)
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L917:
	testb	%dil, %dil
	jne	.L918
	xorl	%edi, %edi
	cmpb	$7, %cl
	je	.L920
	movl	%edx, %edi
	andl	$-128, %edi
	orl	%r9d, %edi
	movb	%dil, (%r12,%rax)
	movl	%r9d, %edi
	andl	$1, %edi
.L920:
	testb	%dl, %dl
	js	.L1212
	movl	$1, %edx
	movslq	%edi, %rdi
	salq	%cl, %rdx
	leaq	_ZL5flagE(%rip), %rcx
	orl	%edx, %esi
	orl	(%rcx,%rdi,4), %esi
.L922:
	leaq	1(%rax), %rdx
	cmpq	%rax, %r14
	je	.L1213
	movq	%rdx, %rax
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L1205:
	addl	$1, %r8d
	cmpl	324(%rbx), %r8d
	jle	.L914
	movl	%r8d, 324(%rbx)
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L1212:
	movslq	%edi, %rdi
	leaq	_ZL5flagO(%rip), %rcx
	orl	(%rcx,%rdi,4), %esi
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L1208:
	movl	%edx, %edi
	andl	$1, %edi
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L1206:
	subl	$1, %r8d
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L1207:
	movl	-64(%rbp), %edi
	addl	$1, %edi
	cmpl	200(%rbx), %edi
	jge	.L916
	movq	208(%rbx), %r9
	movslq	%edi, %r11
	movl	%edi, -64(%rbp)
	leaq	(%r9,%r11,8), %r11
	movl	4(%r11), %r9d
	movl	(%r11), %r13d
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L1213:
	testl	$8380376, %esi
	je	.L1195
	movzbl	141(%rbx), %eax
	leaq	_ZL6flagLR(%rip), %rdx
	andl	$1, %eax
	orl	(%rdx,%rax,4), %esi
.L1195:
	movl	%esi, %eax
	movl	%esi, 188(%rbx)
	andl	$2154498, %eax
	testl	%eax, %eax
	je	.L1214
.L926:
	xorl	%r13d, %r13d
	andl	$26220581, %esi
	setne	%r13b
	addl	$1, %r13d
.L912:
	movq	-56(%rbp), %rax
	movl	(%rax), %r14d
	testl	%r14d, %r14d
	jg	.L846
.L910:
	movl	324(%rbx), %eax
	cmpl	$5, %eax
	jle	.L1215
	sall	$4, %eax
	movq	96(%rbx), %rdi
	movl	%eax, %r14d
	cmpl	48(%rbx), %eax
	jg	.L930
	movq	%rdi, 328(%rbx)
.L929:
	movl	$-1, 324(%rbx)
	movl	%r13d, 184(%rbx)
	cmpl	$1, %r13d
	ja	.L933
	movl	$0, 196(%rbx)
	.p2align 4,,10
	.p2align 3
.L934:
	cmpb	$0, 142(%rbx)
	movl	136(%rbx), %edx
	je	.L1021
	testb	$1, %dl
	je	.L1021
	movl	132(%rbx), %eax
	subl	$5, %eax
	cmpl	$1, %eax
	jbe	.L1216
	.p2align 4,,10
	.p2align 3
.L1021:
	andl	$2, %edx
	movl	24(%rbx), %eax
	je	.L1034
	subl	440(%rbx), %eax
.L1035:
	movl	%eax, 24(%rbx)
	movl	$0, 152(%rbx)
	movl	$0, 168(%rbx)
	movq	%rbx, (%rbx)
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L1203:
	movq	64(%rbx), %rdi
	movzbl	104(%rbx), %eax
	testq	%rdi, %rdi
	je	.L1217
	cmpl	32(%rbx), %r10d
	jle	.L909
	testb	%al, %al
	je	.L906
	movslq	%r10d, %rsi
	movl	%r10d, -64(%rbp)
	call	uprv_realloc_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L906
	movq	%rax, 64(%rbx)
.L1194:
	movl	-64(%rbp), %r10d
	movl	%r10d, 32(%rbx)
.L909:
	movq	-56(%rbp), %r14
	movq	%rdi, 120(%rbx)
	movq	%rbx, %rdi
	movl	%r10d, -64(%rbp)
	movq	%r14, %rsi
	call	_ZL21resolveExplicitLevelsP5UBiDiP10UErrorCode
	movl	-64(%rbp), %r10d
	movl	%eax, %r13d
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L910
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L1215:
	leaq	336(%rbx), %rax
	movq	%rax, 328(%rbx)
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L1214:
	movl	%esi, %r13d
	andl	$32, %r13d
	je	.L912
	movl	%esi, %r13d
	andl	$8249304, %r13d
	jne	.L926
	jmp	.L912
.L1217:
	testb	%al, %al
	je	.L906
	movslq	%r10d, %rdi
	movl	%r10d, -64(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L1194
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L1209:
	movq	-56(%rbp), %rax
	movq	$0, -64(%rbp)
	testq	%rax, %rax
	je	.L850
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L850
	testq	%r14, %r14
	je	.L851
	movl	%r13d, %eax
	subb	$126, %al
	jns	.L851
	movq	-56(%rbp), %r9
	movzbl	%r13b, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	ubidi_setPara_67.part.0
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L1034:
	addl	420(%rbx), %eax
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L933:
	cmpl	$6, 132(%rbx)
	ja	.L935
	movl	132(%rbx), %eax
	leaq	.L937(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L937:
	.long	.L942-.L937
	.long	.L941-.L937
	.long	.L940-.L937
	.long	.L935-.L937
	.long	.L939-.L937
	.long	.L938-.L937
	.long	.L936-.L937
	.text
.L936:
	testb	$1, 136(%rbx)
	leaq	_ZL45impTab_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS(%rip), %rax
	leaq	_ZL34impTab_INVERSE_FOR_NUMBERS_SPECIAL(%rip), %rdx
	cmove	%rdx, %rax
	movq	%rax, 176(%rbx)
	.p2align 4,,10
	.p2align 3
.L943:
	movzbl	142(%rbx), %eax
	testq	%r12, %r12
	je	.L1218
.L946:
	movq	120(%rbx), %r14
	testb	%al, %al
	je	.L955
	movq	208(%rbx), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jle	.L956
.L955:
	movzbl	141(%rbx), %edx
.L957:
	movzbl	(%r14), %eax
	movl	%edx, %r9d
	leal	-1(%r10), %ecx
	movq	%rbx, -64(%rbp)
	andl	$1, %r9d
	movl	%ecx, -80(%rbp)
	movl	$382976, %r11d
	movl	%r10d, %r13d
	movl	%eax, %r8d
	movdqa	.LC3(%rip), %xmm1
	andl	$1, %r8d
	cmpb	%dl, %al
	cmovbe	%r9d, %r8d
	xorl	%esi, %esi
	movl	%r8d, %r9d
	.p2align 4,,10
	.p2align 3
.L988:
	testl	%esi, %esi
	jle	.L964
	movslq	%esi, %rdx
	cmpb	$7, -1(%r15,%rdx)
	je	.L1219
.L964:
	leal	1(%rsi), %r10d
	movl	%r10d, %edi
	cmpl	%r10d, %r13d
	jle	.L1049
.L971:
	movslq	%r10d, %rcx
	movl	%esi, %ebx
	.p2align 4,,10
	.p2align 3
.L974:
	movzbl	(%r14,%rcx), %r12d
	movl	%ebx, %edx
	movl	%ecx, %edi
	movl	%ecx, %ebx
	cmpb	%al, %r12b
	je	.L975
	movzbl	(%r15,%rcx), %r8d
	btq	%r8, %r11
	jnc	.L1220
.L975:
	addq	$1, %rcx
	addl	$1, %edi
	cmpl	%ecx, %r13d
	jg	.L974
.L973:
	movq	-64(%rbp), %rcx
	cmpb	$0, 142(%rcx)
	je	.L1036
	movq	208(%rcx), %rcx
	movq	%rcx, -72(%rbp)
.L972:
	cmpl	(%rcx), %r13d
	jg	.L977
.L1036:
	movq	-64(%rbp), %rcx
	movb	%r9b, -72(%rbp)
	movl	%ebx, %edx
	movl	%edi, %ebx
	movzbl	141(%rcx), %r12d
.L976:
	movl	%eax, %ecx
	movl	%r12d, %r9d
	movl	%r12d, %edi
	movl	%eax, %r8d
	andl	$127, %r9d
	andl	$127, %ecx
	andl	$1, %edi
	andl	$1, %r8d
	cmpl	%r9d, %ecx
	cmovge	%r8d, %edi
	movl	%edi, %r9d
	testb	%al, %al
	js	.L983
	movl	%r9d, %r8d
	movzbl	-72(%rbp), %ecx
	movb	%dil, -72(%rbp)
	movl	%ebx, %edx
	movq	-64(%rbp), %rdi
	andl	$1, %r8d
	call	_ZL21resolveImplicitLevelsP5UBiDiiihh
	movzbl	-72(%rbp), %r9d
	movdqa	.LC3(%rip), %xmm1
	movl	$382976, %r11d
.L984:
	cmpl	%ebx, %r13d
	jle	.L1221
	movl	%r12d, %eax
	movl	%ebx, %esi
	jmp	.L988
.L939:
	leaq	_ZL27impTab_INVERSE_NUMBERS_AS_L(%rip), %rax
	movq	%rax, 176(%rbx)
	jmp	.L943
.L940:
	leaq	_ZL27impTab_GROUP_NUMBERS_WITH_R(%rip), %rax
	movq	%rax, 176(%rbx)
	jmp	.L943
.L938:
	testb	$1, 136(%rbx)
	leaq	_ZL37impTab_INVERSE_LIKE_DIRECT_WITH_MARKS(%rip), %rax
	leaq	_ZL26impTab_INVERSE_LIKE_DIRECT(%rip), %rdx
	cmove	%rdx, %rax
	movq	%rax, 176(%rbx)
	jmp	.L943
.L941:
	leaq	_ZL22impTab_NUMBERS_SPECIAL(%rip), %rax
	movq	%rax, 176(%rbx)
	jmp	.L943
.L942:
	leaq	_ZL14impTab_DEFAULT(%rip), %rax
	movq	%rax, 176(%rbx)
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L1220:
	movb	%r9b, -72(%rbp)
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L983:
	movl	%edx, %ecx
	movl	$1, %eax
	subl	%esi, %ecx
	cmpl	%edx, %esi
	leal	1(%rcx), %edi
	cmovle	%edi, %eax
	cmpl	$14, %ecx
	jbe	.L985
	cmpl	%edx, %esi
	jg	.L985
	movl	%eax, %edi
	movslq	%esi, %rcx
	shrl	$4, %edi
	addq	%r14, %rcx
	salq	$4, %rdi
	addq	%rcx, %rdi
	.p2align 4,,10
	.p2align 3
.L986:
	movdqu	(%rcx), %xmm0
	addq	$16, %rcx
	pand	%xmm1, %xmm0
	movups	%xmm0, -16(%rcx)
	cmpq	%rcx, %rdi
	jne	.L986
	movl	%eax, %ecx
	andl	$-16, %ecx
	addl	%ecx, %esi
	cmpl	%eax, %ecx
	je	.L984
	leal	1(%rsi), %r10d
.L985:
	movslq	%esi, %rax
	andb	$127, (%r14,%rax)
	cmpl	%esi, %edx
	jle	.L984
	movslq	%r10d, %rax
	leal	2(%rsi), %ecx
	andb	$127, (%r14,%rax)
	cmpl	%r10d, %edx
	jle	.L984
	movslq	%ecx, %rax
	leal	3(%rsi), %edi
	andb	$127, (%r14,%rax)
	cmpl	%ecx, %edx
	jle	.L984
	movslq	%edi, %rax
	leal	4(%rsi), %ecx
	andb	$127, (%r14,%rax)
	cmpl	%edi, %edx
	jle	.L984
	movslq	%ecx, %rax
	leal	5(%rsi), %edi
	andb	$127, (%r14,%rax)
	cmpl	%ecx, %edx
	jle	.L984
	movslq	%edi, %rax
	leal	6(%rsi), %ecx
	andb	$127, (%r14,%rax)
	cmpl	%edi, %edx
	jle	.L984
	movslq	%ecx, %rax
	leal	7(%rsi), %edi
	andb	$127, (%r14,%rax)
	cmpl	%ecx, %edx
	jle	.L984
	movslq	%edi, %rax
	leal	8(%rsi), %ecx
	andb	$127, (%r14,%rax)
	cmpl	%edi, %edx
	jle	.L984
	movslq	%ecx, %rax
	leal	9(%rsi), %edi
	andb	$127, (%r14,%rax)
	cmpl	%ecx, %edx
	jle	.L984
	movslq	%edi, %rax
	leal	10(%rsi), %ecx
	andb	$127, (%r14,%rax)
	cmpl	%edi, %edx
	jle	.L984
	movslq	%ecx, %rax
	leal	11(%rsi), %edi
	andb	$127, (%r14,%rax)
	cmpl	%ecx, %edx
	jle	.L984
	movslq	%edi, %rax
	leal	12(%rsi), %ecx
	andb	$127, (%r14,%rax)
	cmpl	%edi, %edx
	jle	.L984
	movslq	%ecx, %rax
	leal	13(%rsi), %edi
	andb	$127, (%r14,%rax)
	cmpl	%ecx, %edx
	jle	.L984
	movslq	%edi, %rcx
	leal	14(%rsi), %eax
	andb	$127, (%r14,%rcx)
	cmpl	%edi, %edx
	jle	.L984
	cltq
	andb	$127, (%r14,%rax)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1219:
	movq	-64(%rbp), %rbx
	cmpb	$0, 142(%rbx)
	je	.L965
	movq	208(%rbx), %rbx
	movq	%rbx, -72(%rbp)
	cmpl	(%rbx), %esi
	jge	.L966
.L965:
	movq	-64(%rbp), %rbx
	movzbl	141(%rbx), %ebx
	movl	%ebx, %r9d
	movb	%bl, -72(%rbp)
	andl	$1, %r9d
	jmp	.L964
.L1221:
	movq	-64(%rbp), %rbx
.L954:
	movl	428(%rbx), %eax
	testl	%eax, %eax
	jle	.L989
	movq	-56(%rbp), %rsi
	movl	%eax, (%rsi)
	jmp	.L846
.L856:
	movq	-56(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L850
.L977:
	movq	-64(%rbp), %rcx
	movl	200(%rcx), %r12d
	testl	%r12d, %r12d
	jle	.L1222
	movq	-72(%rbp), %r8
	leal	-1(%r12), %ecx
	movl	-80(%rbp), %r12d
	movl	%ecx, -88(%rbp)
	movq	%r8, %rdx
	leaq	8(%r8,%rcx,8), %r8
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1224:
	addq	$8, %rdx
	cmpq	%rdx, %r8
	je	.L1223
.L980:
	movq	%rdx, %rcx
	cmpl	(%rdx), %r12d
	jge	.L1224
.L979:
	movl	%ebx, %edx
	movb	%r9b, -72(%rbp)
	movzbl	4(%rcx), %r12d
	movl	%edi, %ebx
	jmp	.L976
.L930:
	movl	%r10d, -64(%rbp)
	movslq	%eax, %rsi
	testq	%rdi, %rdi
	je	.L1225
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L906
	movq	%rax, 96(%rbx)
.L1196:
	movl	%r14d, 48(%rbx)
	movl	-64(%rbp), %r10d
	movq	%rax, 328(%rbx)
	jmp	.L929
.L1049:
	movl	%esi, %ebx
	jmp	.L973
.L1211:
	movl	%r15d, 136(%rbx)
	movzbl	104(%rbx), %r15d
	movq	%rax, %r9
	movb	$0, 104(%rbx)
	movl	(%rax), %edi
	movl	$5, 132(%rbx)
	testl	%edi, %edi
	jg	.L859
	cmpl	$-1, %r12d
	jl	.L860
	movzbl	-72(%rbp), %ecx
	movq	-80(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	%r12d, %edx
	movq	%rbx, %rdi
	xorl	$1, %ecx
	movzbl	%cl, %ecx
	call	ubidi_setPara_67.part.0
	movl	-88(%rbp), %r11d
.L859:
	movb	%r15b, 104(%rbx)
	movq	-56(%rbp), %r15
	movq	%rbx, %rdi
	movl	%r11d, -72(%rbp)
	movq	%r15, %rsi
	call	ubidi_getRuns_67@PLT
	movl	(%r15), %esi
	movl	-72(%rbp), %r11d
	testl	%esi, %esi
	jg	.L862
	movl	296(%rbx), %eax
	movq	304(%rbx), %rsi
	movl	%eax, -72(%rbp)
	movq	%rsi, -56(%rbp)
	testl	%eax, %eax
	jle	.L862
	subl	$1, %eax
	movq	%rbx, -88(%rbp)
	movq	%rsi, %rcx
	xorl	%r12d, %r12d
	movl	%eax, -80(%rbp)
	leaq	(%rax,%rax,2), %rax
	xorl	%r15d, %r15d
	leaq	12(%rsi,%rax,4), %rsi
	movq	-64(%rbp), %rax
	movq	%r14, -112(%rbp)
	movq	%rsi, %r14
	.p2align 4,,10
	.p2align 3
.L874:
	movl	%r15d, %edx
	movl	4(%rcx), %r15d
	movl	%r15d, %esi
	subl	%edx, %esi
	movl	%esi, %edx
	cmpl	$1, %esi
	jle	.L863
	movl	(%rcx), %esi
	andl	$2147483647, %esi
	leal	1(%rsi), %edi
	addl	%esi, %edx
	leal	2(%rsi), %r9d
	movslq	%edi, %rdi
	leal	-1(%rdx), %r10d
	leaq	(%rax,%rdi,4), %r8
	cmpl	%r10d, %r9d
	jge	.L873
	addl	$3, %esi
	movslq	-4(%r8), %r8
	movslq	%esi, %rsi
	jmp	.L870
.L865:
	addl	$1, %r12d
.L866:
	movslq	-4(%rax,%rsi,4), %r8
	movl	%r8d, %edi
	subl	%r9d, %edi
	movl	%edi, %ebx
	sarl	$31, %ebx
	xorl	%ebx, %edi
	subl	%ebx, %edi
	cmpl	$1, %edi
	je	.L867
.L869:
	addl	$1, %r12d
.L868:
	movslq	%esi, %rdi
	addq	$2, %rsi
	leal	-1(%rsi), %r9d
	cmpl	%r9d, %r10d
	jle	.L873
.L870:
	movslq	-8(%rax,%rsi,4), %r9
	movl	%r9d, %edi
	subl	%r8d, %edi
	movl	%edi, %ebx
	sarl	$31, %ebx
	xorl	%ebx, %edi
	subl	%ebx, %edi
	cmpl	$1, %edi
	jne	.L865
	movslq	%r9d, %rdi
	movzbl	0(%r13,%r8), %ebx
	cmpb	%bl, 0(%r13,%rdi)
	jne	.L865
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L1226:
	movzbl	0(%r13,%r8), %esi
	cmpb	%sil, 0(%r13,%r9)
	je	.L872
.L871:
	addl	$1, %r12d
.L872:
	addq	$1, %rdi
	cmpl	%edi, %edx
	jle	.L863
.L873:
	movslq	(%rax,%rdi,4), %r8
	movslq	-4(%rax,%rdi,4), %r9
	movl	%r8d, %esi
	subl	%r9d, %esi
	movl	%esi, %r10d
	sarl	$31, %r10d
	xorl	%r10d, %esi
	subl	%r10d, %esi
	cmpl	$1, %esi
	jne	.L871
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L863:
	addq	$12, %rcx
	cmpq	%rcx, %r14
	jne	.L874
	movq	-88(%rbp), %rbx
	movq	-112(%rbp), %r14
	testl	%r12d, %r12d
	jne	.L1227
.L875:
	movslq	-80(%rbp), %rax
	movq	-56(%rbp), %rcx
	movq	%r14, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	%rax, %rsi
	leaq	(%rax,%rax,2), %rax
	movl	%r11d, -104(%rbp)
	leaq	(%rcx,%rax,4), %r15
	movl	%esi, %r14d
	jmp	.L897
.L1230:
	testl	%r12d, %r12d
	je	.L882
	movq	(%r15), %rcx
	movq	%rcx, (%rax)
	movl	8(%r15), %ecx
	movl	%ecx, 8(%rax)
.L882:
	movq	-64(%rbp), %rsi
	movslq	%edx, %rdx
	movslq	(%rsi,%rdx,4), %rdx
	movq	%rdx, %rcx
.L1192:
	movzbl	0(%r13,%rdx), %edx
	xorl	-88(%rbp), %edx
	subl	$1, %r14d
	subq	$12, %r15
	sall	$31, %edx
	orl	%ecx, %edx
	movl	%edx, (%rax)
	cmpl	$-1, %r14d
	je	.L1228
.L897:
	testl	%r14d, %r14d
	je	.L1229
	movl	4(%r15), %ecx
	subl	-8(%r15), %ecx
.L880:
	movl	(%r15), %edx
	leal	(%r14,%r12), %edi
	movq	-56(%rbp), %rsi
	movl	%edx, %eax
	andl	$2147483647, %edx
	shrl	$31, %eax
	movl	%eax, -88(%rbp)
	movslq	%edi, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rsi,%rax,4), %rax
	cmpl	$1, %ecx
	jle	.L1230
	leal	-1(%rcx,%rdx), %esi
	movl	-88(%rbp), %ecx
	movl	%esi, -80(%rbp)
	testl	%ecx, %ecx
	je	.L884
	cmpl	%esi, %edx
	je	.L885
	movq	-64(%rbp), %rsi
	movslq	%edx, %rax
	movl	%edx, %ebx
	leaq	4(%rsi,%rax,4), %r8
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L890:
	movslq	%edi, %rdi
	cmpl	%esi, (%r11)
	cmovle	(%r11), %esi
	subl	%ebx, %eax
	leaq	(%rdi,%rdi,2), %rcx
	movq	-56(%rbp), %rdi
	subl	$1, %r12d
	movl	%edx, %ebx
	leaq	(%rdi,%rcx,4), %rdi
	movslq	%esi, %rcx
	movzbl	0(%r13,%rcx), %ecx
	xorl	$1, %ecx
	sall	$31, %ecx
	orl	%ecx, %esi
	movl	%esi, (%rdi)
	movl	4(%r15), %ecx
	movl	%ecx, 4(%rdi)
	movl	%eax, %ecx
	sarl	$31, %ecx
	xorl	%ecx, %eax
	subl	%ecx, %eax
	addl	$1, %eax
	subl	%eax, 4(%r15)
	movl	8(%r15), %eax
	andl	$10, %eax
	movl	%eax, 8(%rdi)
	notl	%eax
	leal	(%r14,%r12), %edi
	andl	%eax, 8(%r15)
.L891:
	addq	$4, %r8
	cmpl	%edx, -80(%rbp)
	je	.L888
.L892:
	movq	-64(%rbp), %r10
	movl	-4(%r8), %esi
	movslq	%ebx, %rcx
	movl	%edx, %eax
	movslq	(%r8), %r9
	movq	%r8, -72(%rbp)
	addl	$1, %edx
	leaq	(%r10,%rcx,4), %r11
	movl	%esi, %ecx
	subl	%r9d, %ecx
	movl	%ecx, %r10d
	sarl	$31, %r10d
	xorl	%r10d, %ecx
	subl	%r10d, %ecx
	cmpl	$1, %ecx
	jne	.L890
	movslq	%esi, %rcx
	movzbl	0(%r13,%rcx), %ecx
	cmpb	%cl, 0(%r13,%r9)
	jne	.L890
	movq	%r11, -72(%rbp)
	jmp	.L891
.L1232:
	movl	%esi, -80(%rbp)
.L888:
	movq	-56(%rbp), %rsi
	movslq	%edi, %rdi
	leaq	(%rdi,%rdi,2), %rax
	leaq	(%rsi,%rax,4), %rax
.L893:
	testl	%r12d, %r12d
	je	.L896
	movq	(%r15), %rdx
	movq	%rdx, (%rax)
	movl	8(%r15), %edx
	movl	%edx, 8(%rax)
.L896:
	movq	-72(%rbp), %rsi
	movslq	-80(%rbp), %rdx
	movl	(%rsi), %ecx
	movq	-64(%rbp), %rsi
	cmpl	%ecx, (%rsi,%rdx,4)
	cmovle	(%rsi,%rdx,4), %ecx
	movslq	%ecx, %rdx
	jmp	.L1192
.L1229:
	movq	-56(%rbp), %rax
	movl	4(%rax), %ecx
	jmp	.L880
.L884:
	cmpl	-80(%rbp), %edx
	je	.L1231
	movq	-64(%rbp), %rbx
	movslq	%esi, %rax
	movl	%edx, -80(%rbp)
	leaq	-4(%rbx,%rax,4), %r8
	movl	%esi, %ebx
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L894:
	movslq	%edi, %rdi
	cmpl	%ecx, (%r11)
	cmovle	(%r11), %ecx
	subl	%ebx, %eax
	leaq	(%rdi,%rdi,2), %rdx
	movq	-56(%rbp), %rdi
	subl	$1, %r12d
	movl	%esi, %ebx
	leaq	(%rdi,%rdx,4), %rdi
	movslq	%ecx, %rdx
	movzbl	0(%r13,%rdx), %edx
	sall	$31, %edx
	orl	%edx, %ecx
	movl	%ecx, (%rdi)
	movl	4(%r15), %edx
	movl	%edx, 4(%rdi)
	cltd
	xorl	%edx, %eax
	subl	%edx, %eax
	addl	$1, %eax
	subl	%eax, 4(%r15)
	movl	8(%r15), %eax
	andl	$10, %eax
	movl	%eax, 8(%rdi)
	notl	%eax
	leal	(%r14,%r12), %edi
	andl	%eax, 8(%r15)
.L895:
	subq	$4, %r8
	cmpl	%esi, -80(%rbp)
	je	.L1232
.L889:
	movq	-64(%rbp), %r10
	movl	4(%r8), %ecx
	movslq	%ebx, %rdx
	movl	%esi, %eax
	movslq	(%r8), %r9
	movq	%r8, -72(%rbp)
	subl	$1, %esi
	leaq	(%r10,%rdx,4), %r11
	movl	%ecx, %edx
	subl	%r9d, %edx
	movl	%edx, %r10d
	sarl	$31, %r10d
	xorl	%r10d, %edx
	subl	%r10d, %edx
	cmpl	$1, %edx
	jne	.L894
	movslq	%ecx, %rdx
	movzbl	0(%r13,%r9), %r10d
	cmpb	%r10b, 0(%r13,%rdx)
	jne	.L894
	movq	%r11, -72(%rbp)
	jmp	.L895
.L1228:
	movq	-112(%rbp), %rbx
	movq	-120(%rbp), %r14
	movl	-104(%rbp), %r11d
.L862:
	xorb	$1, 141(%rbx)
	jmp	.L858
.L989:
	testl	$8248192, 188(%rbx)
	je	.L934
	movq	112(%rbx), %r9
	movq	120(%rbx), %rdi
	movl	$1, %r8d
	movzbl	140(%rbx), %r10d
	movslq	196(%rbx), %r11
	.p2align 4,,10
	.p2align 3
.L991:
	testl	%r11d, %r11d
	jle	.L934
	leal	-1(%r11), %eax
	subq	$2, %r11
	movslq	%eax, %rdx
	movl	%eax, %eax
	subq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L992:
	movzbl	(%r9,%rdx), %ecx
	movq	%r8, %rax
	movslq	%edx, %rsi
	salq	%cl, %rax
	testl	$8248192, %eax
	jne	.L994
	testl	%esi, %esi
	je	.L934
	leal	-1(%rsi), %eax
	subl	$1, %edx
	subq	$2, %rsi
	cltq
	subq	%rdx, %rsi
	testb	%r10b, %r10b
	je	.L1000
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1233:
	testl	$384, %edx
	jne	.L998
.L999:
	subq	$1, %rax
	cmpq	%rax, %rsi
	je	.L934
.L1000:
	movzbl	(%r9,%rax), %ecx
	movq	%r8, %rdx
	movslq	%eax, %r11
	salq	%cl, %rdx
	testl	$382976, %edx
	je	.L1233
	movzbl	1(%rdi,%rax), %edx
	movb	%dl, (%rdi,%rax)
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1010:
	testb	$-128, %dl
	jne	.L1234
	testl	$384, %edx
	jne	.L998
.L1011:
	subq	$1, %rax
	cmpq	%rax, %rsi
	je	.L934
.L996:
	movzbl	(%r9,%rax), %ecx
	movq	%r8, %rdx
	movslq	%eax, %r11
	salq	%cl, %rdx
	testl	$382976, %edx
	je	.L1010
	movzbl	1(%rdi,%rax), %edx
	movb	%dl, (%rdi,%rax)
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L994:
	testb	%r10b, %r10b
	je	.L1001
	testb	$-128, %al
	je	.L1001
	movb	$0, (%rdi,%rdx)
.L1002:
	subq	$1, %rdx
	cmpq	%rdx, %r11
	jne	.L992
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L1001:
	cmpb	$0, 142(%rbx)
	je	.L1003
	movq	208(%rbx), %r13
	movl	%edx, %ecx
	cmpl	0(%r13), %edx
	jge	.L1004
.L1003:
	movzbl	141(%rbx), %eax
.L1005:
	movb	%al, (%rdi,%rdx)
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1004:
	movl	200(%rbx), %esi
	leal	-1(%rsi), %r14d
	testl	%esi, %esi
	jle	.L1006
	subl	$1, %esi
	leaq	8(%r13), %rax
	movq	%rsi, %r14
	leaq	(%rax,%rsi,8), %r12
	.p2align 4,,10
	.p2align 3
.L1007:
	cmpq	%rax, %r12
	je	.L1006
	movq	%rax, %rsi
	addq	$8, %rax
	cmpl	-8(%rax), %ecx
	jge	.L1007
.L1008:
	movzbl	4(%rsi), %eax
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1006:
	movslq	%r14d, %rax
	leaq	0(%r13,%rax,8), %rsi
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L998:
	cmpb	$0, 142(%rbx)
	je	.L1014
	movq	208(%rbx), %r12
	cmpl	(%r12), %r11d
	jge	.L1015
.L1014:
	movzbl	141(%rbx), %edx
.L1016:
	movb	%dl, (%rdi,%rax)
	jmp	.L991
.L1234:
	movb	$0, (%rdi,%rax)
	jmp	.L991
.L851:
	movq	-56(%rbp), %rax
	movq	$0, -64(%rbp)
	movl	$1, (%rax)
	jmp	.L850
.L867:
	movslq	%r8d, %rdi
	movzbl	0(%r13,%rdi), %ebx
	cmpb	%bl, 0(%r13,%r9)
	jne	.L869
	jmp	.L868
.L1218:
	movl	200(%rbx), %edx
	cmpl	$1, %edx
	jg	.L946
	movl	188(%rbx), %r11d
	testl	%r11d, %r11d
	js	.L946
	testb	%al, %al
	je	.L947
	movq	208(%rbx), %rcx
	movl	(%rcx), %esi
	cmpl	%esi, %r10d
	jle	.L948
	leal	-1(%r10), %eax
	cmpl	%eax, %esi
	jg	.L1052
	cmpl	$1, %edx
	jne	.L1052
.L949:
	leal	-1(%rdx), %eax
.L951:
	cltq
	movl	4(%rcx,%rax,8), %r8d
	andl	$1, %r8d
.L1037:
	testl	%esi, %esi
	jle	.L952
	movzbl	141(%rbx), %ecx
	andl	$1, %ecx
.L953:
	movl	%r10d, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZL21resolveImplicitLevelsP5UBiDiiihh
	jmp	.L954
.L1223:
	movl	-88(%rbp), %r12d
.L978:
	movq	-72(%rbp), %rcx
	movslq	%r12d, %r12
	leaq	(%rcx,%r12,8), %rcx
	jmp	.L979
.L1204:
	movl	$0, 188(%rbx)
	xorl	%r13d, %r13d
	jmp	.L912
.L1216:
	movl	200(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L1021
	xorl	%r12d, %r12d
	movl	$8194, %r13d
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1022:
	addq	$1, %r12
	cmpl	%r12d, %r8d
	jle	.L1235
.L1033:
	movq	208(%rbx), %rsi
	leaq	0(,%r12,8), %rdx
	leaq	(%rsi,%rdx), %rax
	cmpb	$0, 4(%rax)
	je	.L1022
	xorl	%ecx, %ecx
	testq	%r12, %r12
	je	.L1023
	movl	-8(%rsi,%rdx), %ecx
.L1023:
	movl	(%rax), %edi
	leal	-1(%rdi), %r14d
	cmpl	%ecx, %r14d
	jl	.L1022
	movslq	%r14d, %r9
	movq	%r9, %rax
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1024:
	btq	%rdx, %r13
	jc	.L1022
	subq	$1, %rax
	cmpl	%eax, %ecx
	jg	.L1022
.L1032:
	movzbl	(%r15,%rax), %edx
	testb	%dl, %dl
	jne	.L1024
	cmpl	%eax, %r14d
	jg	.L1236
.L1025:
	movl	416(%rbx), %eax
	testl	%eax, %eax
	je	.L1027
	movq	432(%rbx), %rcx
.L1028:
	movl	420(%rbx), %edx
	cmpl	%eax, %edx
	jge	.L1237
.L1030:
	movslq	%edx, %rax
	addl	$1, %edx
	movl	200(%rbx), %r8d
	leaq	(%rcx,%rax,8), %rax
	movl	%r14d, (%rax)
	movl	$4, 4(%rax)
	movl	%edx, 420(%rbx)
	jmp	.L1022
.L966:
	movq	-64(%rbp), %rbx
	movl	200(%rbx), %ecx
	leal	-1(%rcx), %r8d
	testl	%ecx, %ecx
	jle	.L967
	movq	-72(%rbp), %rbx
	subl	$1, %ecx
	movq	%rcx, %r8
	leaq	8(%rbx), %rdx
	leaq	(%rdx,%rcx,8), %rdi
	.p2align 4,,10
	.p2align 3
.L968:
	cmpq	%rdi, %rdx
	je	.L967
	movq	%rdx, %rcx
	addq	$8, %rdx
	cmpl	-8(%rdx), %esi
	jge	.L968
.L969:
	movl	4(%rcx), %edi
	leal	1(%rsi), %r10d
	movq	-72(%rbp), %rcx
	movl	%esi, %ebx
	movl	%edi, %r9d
	movl	%r10d, %edi
	andl	$1, %r9d
	cmpl	%r10d, %r13d
	jg	.L971
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1235:
	movl	136(%rbx), %edx
	jmp	.L1021
.L1015:
	movl	200(%rbx), %ecx
	leal	-1(%rcx), %r13d
	testl	%ecx, %ecx
	jle	.L1017
	subl	$1, %ecx
	leaq	8(%r12), %rdx
	movq	%rcx, %r13
	leaq	(%rdx,%rcx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L1018:
	cmpq	%rdx, %rsi
	je	.L1017
	movq	%rdx, %rcx
	addq	$8, %rdx
	cmpl	-8(%rdx), %r11d
	jge	.L1018
.L1019:
	movzbl	4(%rcx), %edx
	jmp	.L1016
.L956:
	movl	200(%rbx), %ecx
	leal	-1(%rcx), %edi
	testl	%ecx, %ecx
	jle	.L958
	subl	$1, %ecx
	leaq	8(%rax), %rdx
	movq	%rcx, %rdi
	leaq	(%rdx,%rcx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L959:
	cmpq	%rdx, %rsi
	je	.L958
	movq	%rdx, %rcx
	movl	(%rdx), %r8d
	addq	$8, %rdx
	testl	%r8d, %r8d
	jle	.L959
.L960:
	movzbl	4(%rcx), %edx
	jmp	.L957
.L860:
	movl	$1, (%rax)
	jmp	.L859
.L967:
	movq	-72(%rbp), %rbx
	movslq	%r8d, %rcx
	leaq	(%rbx,%rcx,8), %rcx
	jmp	.L969
.L1231:
	movl	%edx, -80(%rbp)
.L885:
	movq	-64(%rbp), %rsi
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx,4), %rsi
	movq	%rsi, -72(%rbp)
	jmp	.L893
.L958:
	movslq	%edi, %rcx
	leaq	(%rax,%rcx,8), %rcx
	jmp	.L960
.L1237:
	leal	(%rax,%rax), %esi
	movq	%rcx, %rdi
	movq	%rcx, -56(%rbp)
	movslq	%esi, %rsi
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, 432(%rbx)
	je	.L1238
	movl	420(%rbx), %edx
	sall	416(%rbx)
	movq	%rax, %rcx
	jmp	.L1030
.L1027:
	movl	$80, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 432(%rbx)
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1197
	movl	$10, 416(%rbx)
	movl	$10, %eax
	jmp	.L1028
.L1017:
	movslq	%r13d, %rcx
	leaq	(%r12,%rcx,8), %rcx
	jmp	.L1019
.L1227:
	movl	-72(%rbp), %eax
	movq	88(%rbx), %rdi
	addl	%r12d, %eax
	leal	(%rax,%rax,2), %eax
	leal	0(,%rax,4), %r15d
	movzbl	105(%rbx), %eax
	testq	%rdi, %rdi
	je	.L1239
	cmpl	44(%rbx), %r15d
	jle	.L877
	testb	%al, %al
	je	.L862
	movslq	%r15d, %rsi
	movl	%r11d, -88(%rbp)
	call	uprv_realloc_67@PLT
	movl	-88(%rbp), %r11d
	testq	%rax, %rax
	je	.L862
	movq	%rax, 88(%rbx)
.L1191:
	movl	%r15d, 44(%rbx)
.L877:
	cmpl	$1, -72(%rbp)
	movq	88(%rbx), %rax
	jne	.L878
	movq	-56(%rbp), %rsi
	movq	(%rsi), %rdx
	movq	%rdx, (%rax)
	movl	8(%rsi), %edx
	movl	%edx, 8(%rax)
.L878:
	addl	%r12d, 296(%rbx)
	movq	%rax, 304(%rbx)
	movq	%rax, -56(%rbp)
	jmp	.L875
.L1225:
	movq	%rsi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 96(%rbx)
	testq	%rax, %rax
	jne	.L1196
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L1236:
	cmpb	$7, (%r15,%r9)
	jne	.L1025
	leal	-2(%rdi), %eax
	cltq
.L1026:
	movl	%eax, %r14d
	subq	$1, %rax
	cmpb	$7, 1(%r15,%rax)
	je	.L1026
	jmp	.L1025
.L947:
	movzbl	141(%rbx), %r8d
	andl	$1, %r8d
	movl	%r8d, %ecx
	jmp	.L953
.L1239:
	testb	%al, %al
	je	.L862
	movslq	%r15d, %rdi
	movl	%r11d, -88(%rbp)
	call	uprv_malloc_67@PLT
	movl	-88(%rbp), %r11d
	testq	%rax, %rax
	movq	%rax, 88(%rbx)
	jne	.L1191
	jmp	.L862
.L1222:
	subl	$1, %r12d
	jmp	.L978
.L1238:
	movq	%rcx, 432(%rbx)
.L1197:
	movl	$7, 428(%rbx)
	movl	200(%rbx), %r8d
	jmp	.L1022
.L948:
	movzbl	141(%rbx), %r8d
	andl	$1, %r8d
	jmp	.L1037
.L952:
	leal	-1(%rdx), %eax
	cltq
	movl	4(%rcx,%rax,8), %ecx
	andl	$1, %ecx
	jmp	.L953
.L1052:
	xorl	%eax, %eax
	cmpl	$1, %edx
	je	.L951
	jmp	.L949
.L1210:
	movq	-56(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L850
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	ubidi_setPara_67.part.0.cold, @function
ubidi_setPara_67.part.0.cold:
.LFSB2734:
.L935:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE2734:
	.text
	.size	ubidi_setPara_67.part.0, .-ubidi_setPara_67.part.0
	.section	.text.unlikely
	.size	ubidi_setPara_67.part.0.cold, .-ubidi_setPara_67.part.0.cold
.LCOLDE4:
	.text
.LHOTE4:
	.p2align 4
	.globl	ubidi_getMemory_67
	.type	ubidi_getMemory_67, @function
ubidi_getMemory_67:
.LFB2096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movl	%ecx, %ebx
	testq	%rdi, %rdi
	je	.L1253
	movl	$1, %r12d
	cmpl	%ecx, (%rsi)
	jge	.L1240
	testb	%dl, %dl
	je	.L1244
	movslq	%ecx, %rsi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L1244
	movq	%rax, 0(%r13)
	movl	%ebx, (%r14)
.L1240:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1253:
	.cfi_restore_state
	testb	%dl, %dl
	jne	.L1242
.L1244:
	xorl	%r12d, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1242:
	.cfi_restore_state
	movslq	%ecx, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L1244
	movl	$1, %r12d
	movl	%ebx, (%r14)
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2096:
	.size	ubidi_getMemory_67, .-ubidi_getMemory_67
	.p2align 4
	.globl	ubidi_close_67
	.type	ubidi_close_67, @function
ubidi_close_67:
.LFB2097:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1254
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	$0, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1256
	call	uprv_free_67@PLT
.L1256:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1257
	call	uprv_free_67@PLT
.L1257:
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1258
	call	uprv_free_67@PLT
.L1258:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1259
	call	uprv_free_67@PLT
.L1259:
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1260
	call	uprv_free_67@PLT
.L1260:
	movq	96(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1261
	call	uprv_free_67@PLT
.L1261:
	movq	432(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1262
	call	uprv_free_67@PLT
.L1262:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L1254:
	ret
	.cfi_endproc
.LFE2097:
	.size	ubidi_close_67, .-ubidi_close_67
	.p2align 4
	.globl	ubidi_openSized_67
	.type	ubidi_openSized_67, @function
ubidi_openSized_67:
.LFB2095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L1302
	movq	%rdx, %rbx
	movl	(%rdx), %edx
	xorl	%r12d, %r12d
	testl	%edx, %edx
	jg	.L1287
	movl	%edi, %r14d
	testl	%edi, %edi
	js	.L1304
	movl	%esi, %r13d
	testl	%esi, %esi
	js	.L1304
	movl	$464, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1323
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	$0, 456(%rax)
	andq	$-8, %rdi
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$464, %ecx
	shrl	$3, %ecx
	rep stosq
	testl	%r14d, %r14d
	jne	.L1324
	movb	$1, 104(%r12)
.L1295:
	testl	%r13d, %r13d
	je	.L1296
	cmpl	$1, %r13d
	jne	.L1297
	movl	$12, 44(%r12)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1301
.L1287:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1304:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1296:
	movb	$1, 105(%r12)
.L1298:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1287
.L1301:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ubidi_close_67
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1297:
	movq	88(%r12), %rdi
	leal	0(%r13,%r13,2), %r13d
	sall	$2, %r13d
	testq	%rdi, %rdi
	je	.L1325
	cmpl	44(%r12), %r13d
	jle	.L1298
	movslq	%r13d, %rsi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L1300
	movq	%rax, 88(%r12)
.L1322:
	movl	%r13d, 44(%r12)
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1325:
	movslq	%r13d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 88(%r12)
	testq	%rax, %rax
	jne	.L1322
	.p2align 4,,10
	.p2align 3
.L1300:
	movl	$7, (%rbx)
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1324:
	movslq	%r14d, %r15
	movq	%r15, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 56(%r12)
	testq	%rax, %rax
	je	.L1293
	movq	64(%r12), %rdi
	movl	%r14d, 28(%r12)
	testq	%rdi, %rdi
	je	.L1326
	cmpl	32(%r12), %r14d
	jle	.L1295
	movq	%r15, %rsi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L1293
	movq	%rax, 64(%r12)
.L1321:
	movl	%r14d, 32(%r12)
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	%r15, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r12)
	testq	%rax, %rax
	jne	.L1321
.L1293:
	movl	$7, (%rbx)
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1302:
	xorl	%r12d, %r12d
	jmp	.L1287
.L1323:
	movl	$7, (%rbx)
	jmp	.L1287
	.cfi_endproc
.LFE2095:
	.size	ubidi_openSized_67, .-ubidi_openSized_67
	.p2align 4
	.globl	ubidi_open_67
	.type	ubidi_open_67, @function
ubidi_open_67:
.LFB2094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$464, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1327
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	$0, 456(%rax)
	andq	$-8, %rdi
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$464, %ecx
	shrl	$3, %ecx
	rep stosq
	movl	$257, %eax
	movw	%ax, 104(%r8)
.L1327:
	movq	%r8, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2094:
	.size	ubidi_open_67, .-ubidi_open_67
	.p2align 4
	.globl	ubidi_setInverse_67
	.type	ubidi_setInverse_67, @function
ubidi_setInverse_67:
.LFB2098:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1333
	xorl	%eax, %eax
	testb	%sil, %sil
	movb	%sil, 128(%rdi)
	setne	%al
	sall	$2, %eax
	movl	%eax, 132(%rdi)
.L1333:
	ret
	.cfi_endproc
.LFE2098:
	.size	ubidi_setInverse_67, .-ubidi_setInverse_67
	.p2align 4
	.globl	ubidi_isInverse_67
	.type	ubidi_isInverse_67, @function
ubidi_isInverse_67:
.LFB2099:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1340
	movzbl	128(%rdi), %eax
.L1340:
	ret
	.cfi_endproc
.LFE2099:
	.size	ubidi_isInverse_67, .-ubidi_isInverse_67
	.p2align 4
	.globl	ubidi_setReorderingMode_67
	.type	ubidi_setReorderingMode_67, @function
ubidi_setReorderingMode_67:
.LFB2100:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1344
	cmpl	$6, %esi
	jbe	.L1352
.L1344:
	ret
	.p2align 4,,10
	.p2align 3
.L1352:
	cmpl	$4, %esi
	movl	%esi, 132(%rdi)
	sete	128(%rdi)
	ret
	.cfi_endproc
.LFE2100:
	.size	ubidi_setReorderingMode_67, .-ubidi_setReorderingMode_67
	.p2align 4
	.globl	ubidi_getReorderingMode_67
	.type	ubidi_getReorderingMode_67, @function
ubidi_getReorderingMode_67:
.LFB2101:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1353
	movl	132(%rdi), %eax
.L1353:
	ret
	.cfi_endproc
.LFE2101:
	.size	ubidi_getReorderingMode_67, .-ubidi_getReorderingMode_67
	.p2align 4
	.globl	ubidi_setReorderingOptions_67
	.type	ubidi_setReorderingOptions_67, @function
ubidi_setReorderingOptions_67:
.LFB2102:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$-2, %eax
	testb	$2, %sil
	cmovne	%eax, %esi
	testq	%rdi, %rdi
	je	.L1357
	movl	%esi, 136(%rdi)
.L1357:
	ret
	.cfi_endproc
.LFE2102:
	.size	ubidi_setReorderingOptions_67, .-ubidi_setReorderingOptions_67
	.p2align 4
	.globl	ubidi_getReorderingOptions_67
	.type	ubidi_getReorderingOptions_67, @function
ubidi_getReorderingOptions_67:
.LFB2103:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1365
	movl	136(%rdi), %eax
.L1365:
	ret
	.cfi_endproc
.LFE2103:
	.size	ubidi_getReorderingOptions_67, .-ubidi_getReorderingOptions_67
	.p2align 4
	.globl	ubidi_getBaseDirection_67
	.type	ubidi_getBaseDirection_67, @function
ubidi_getBaseDirection_67:
.LFB2104:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1388
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpl	$-1, %esi
	jl	.L1370
	movq	%rdi, %r13
	je	.L1391
.L1371:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1374:
	cmpl	%ebx, %r12d
	jle	.L1370
	movslq	%ebx, %rax
	leal	1(%rbx), %edx
	movzwl	0(%r13,%rax,2), %edi
	leaq	(%rax,%rax), %rcx
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L1376
	cmpl	%edx, %r12d
	jne	.L1392
.L1376:
	movl	%edx, %ebx
.L1372:
	call	u_charDirection_67@PLT
	testl	%eax, %eax
	je	.L1369
	cmpl	$1, %eax
	je	.L1377
	cmpl	$13, %eax
	jne	.L1374
.L1377:
	movl	$1, %eax
.L1369:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1392:
	.cfi_restore_state
	movzwl	2(%r13,%rcx), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L1376
	sall	$10, %edi
	addl	$2, %ebx
	leal	-56613888(%rax,%rdi), %edi
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1370:
	addq	$8, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1391:
	.cfi_restore_state
	call	u_strlen_67@PLT
	movl	%eax, %r12d
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1388:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE2104:
	.size	ubidi_getBaseDirection_67, .-ubidi_getBaseDirection_67
	.p2align 4
	.globl	ubidi_getParaLevelAtIndex_67
	.type	ubidi_getParaLevelAtIndex_67, @function
ubidi_getParaLevelAtIndex_67:
.LFB2108:
	.cfi_startproc
	endbr64
	movl	200(%rdi), %edx
	movq	208(%rdi), %r8
	leal	-1(%rdx), %edi
	testl	%edx, %edx
	jle	.L1394
	subl	$1, %edx
	movq	%r8, %rax
	movq	%rdx, %rdi
	leaq	8(%r8,%rdx,8), %rcx
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1400:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L1394
.L1396:
	movq	%rax, %rdx
	cmpl	%esi, (%rax)
	jle	.L1400
	movl	4(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1394:
	movslq	%edi, %rdx
	leaq	(%r8,%rdx,8), %rdx
	movl	4(%rdx), %eax
	ret
	.cfi_endproc
.LFE2108:
	.size	ubidi_getParaLevelAtIndex_67, .-ubidi_getParaLevelAtIndex_67
	.p2align 4
	.globl	ubidi_setContext_67
	.type	ubidi_setContext_67, @function
ubidi_setContext_67:
.LFB2128:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L1424
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1424
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	testq	%rdi, %rdi
	sete	%dil
	cmpl	$-1, %edx
	setl	%al
	orb	%al, %dil
	jne	.L1403
	cmpl	$-1, %r8d
	jl	.L1403
	testq	%rsi, %rsi
	jne	.L1410
	testl	%edx, %edx
	jne	.L1403
.L1410:
	testq	%rcx, %rcx
	jne	.L1405
	testl	%r8d, %r8d
	je	.L1405
	.p2align 4,,10
	.p2align 3
.L1403:
	movl	$1, (%r9)
.L1401:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1424:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L1405:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpl	$-1, %edx
	je	.L1427
	movl	%edx, 152(%rbx)
.L1407:
	cmpl	$-1, %r8d
	je	.L1428
	movl	%r8d, 168(%rbx)
.L1409:
	movq	%rsi, 144(%rbx)
	movq	%rcx, 160(%rbx)
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	%rsi, %rdi
	movl	%r8d, -36(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	u_strlen_67@PLT
	movq	-24(%rbp), %rsi
	movq	-32(%rbp), %rcx
	movl	%eax, 152(%rbx)
	movl	-36(%rbp), %r8d
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	%rcx, %rdi
	movq	%rsi, -32(%rbp)
	movq	%rcx, -24(%rbp)
	call	u_strlen_67@PLT
	movq	-24(%rbp), %rcx
	movq	-32(%rbp), %rsi
	movl	%eax, 168(%rbx)
	jmp	.L1409
	.cfi_endproc
.LFE2128:
	.size	ubidi_setContext_67, .-ubidi_setContext_67
	.p2align 4
	.globl	ubidi_setPara_67
	.type	ubidi_setPara_67, @function
ubidi_setPara_67:
.LFB2131:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L1429
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1429
	testq	%rdi, %rdi
	sete	%r10b
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %r10b
	jne	.L1431
	cmpl	$-1, %edx
	jl	.L1431
	movl	%ecx, %eax
	subb	$126, %al
	jns	.L1431
	movzbl	%cl, %ecx
	jmp	ubidi_setPara_67.part.0
	.p2align 4,,10
	.p2align 3
.L1429:
	ret
	.p2align 4,,10
	.p2align 3
.L1431:
	movl	$1, (%r9)
	ret
	.cfi_endproc
.LFE2131:
	.size	ubidi_setPara_67, .-ubidi_setPara_67
	.p2align 4
	.globl	ubidi_orderParagraphsLTR_67
	.type	ubidi_orderParagraphsLTR_67, @function
ubidi_orderParagraphsLTR_67:
.LFB2132:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1436
	movb	%sil, 140(%rdi)
.L1436:
	ret
	.cfi_endproc
.LFE2132:
	.size	ubidi_orderParagraphsLTR_67, .-ubidi_orderParagraphsLTR_67
	.p2align 4
	.globl	ubidi_isOrderParagraphsLTR_67
	.type	ubidi_isOrderParagraphsLTR_67, @function
ubidi_isOrderParagraphsLTR_67:
.LFB2133:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1441
	movzbl	140(%rdi), %eax
.L1441:
	ret
	.cfi_endproc
.LFE2133:
	.size	ubidi_isOrderParagraphsLTR_67, .-ubidi_isOrderParagraphsLTR_67
	.p2align 4
	.globl	ubidi_getDirection_67
	.type	ubidi_getDirection_67, @function
ubidi_getDirection_67:
.LFB2134:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1448
	movq	(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L1447
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L1445
	cmpq	(%rax), %rax
	je	.L1447
.L1445:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1447:
	movl	184(%rdi), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1448:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2134:
	.size	ubidi_getDirection_67, .-ubidi_getDirection_67
	.p2align 4
	.globl	ubidi_getText_67
	.type	ubidi_getText_67, @function
ubidi_getText_67:
.LFB2135:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1455
	movq	(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L1453
	testq	%rax, %rax
	je	.L1451
	cmpq	(%rax), %rax
	jne	.L1455
.L1453:
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1455:
	xorl	%eax, %eax
.L1451:
	ret
	.cfi_endproc
.LFE2135:
	.size	ubidi_getText_67, .-ubidi_getText_67
	.p2align 4
	.globl	ubidi_getLength_67
	.type	ubidi_getLength_67, @function
ubidi_getLength_67:
.LFB2136:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1462
	movq	(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L1461
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L1459
	cmpq	(%rax), %rax
	je	.L1461
.L1459:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1461:
	movl	16(%rdi), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1462:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2136:
	.size	ubidi_getLength_67, .-ubidi_getLength_67
	.p2align 4
	.globl	ubidi_getProcessedLength_67
	.type	ubidi_getProcessedLength_67, @function
ubidi_getProcessedLength_67:
.LFB2137:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1468
	movq	(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L1467
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L1465
	cmpq	(%rax), %rax
	je	.L1467
.L1465:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1467:
	movl	20(%rdi), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1468:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2137:
	.size	ubidi_getProcessedLength_67, .-ubidi_getProcessedLength_67
	.p2align 4
	.globl	ubidi_getResultLength_67
	.type	ubidi_getResultLength_67, @function
ubidi_getResultLength_67:
.LFB2138:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1474
	movq	(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L1473
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L1471
	cmpq	(%rax), %rax
	je	.L1473
.L1471:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1473:
	movl	24(%rdi), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1474:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2138:
	.size	ubidi_getResultLength_67, .-ubidi_getResultLength_67
	.p2align 4
	.globl	ubidi_getParaLevel_67
	.type	ubidi_getParaLevel_67, @function
ubidi_getParaLevel_67:
.LFB2139:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1480
	movq	(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L1479
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L1477
	cmpq	(%rax), %rax
	je	.L1479
.L1477:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1479:
	movzbl	141(%rdi), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1480:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2139:
	.size	ubidi_getParaLevel_67, .-ubidi_getParaLevel_67
	.p2align 4
	.globl	ubidi_countParagraphs_67
	.type	ubidi_countParagraphs_67, @function
ubidi_countParagraphs_67:
.LFB2140:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1486
	movq	(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L1485
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L1483
	cmpq	(%rax), %rax
	je	.L1485
.L1483:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1485:
	movl	200(%rdi), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1486:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2140:
	.size	ubidi_countParagraphs_67, .-ubidi_countParagraphs_67
	.p2align 4
	.globl	ubidi_getParagraphByIndex_67
	.type	ubidi_getParagraphByIndex_67, @function
ubidi_getParagraphByIndex_67:
.LFB2141:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L1489
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1489
	testq	%rdi, %rdi
	je	.L1493
	movq	(%rdi), %rax
	cmpq	%rax, %rdi
	je	.L1494
	testq	%rax, %rax
	je	.L1493
	cmpq	(%rax), %rax
	je	.L1494
.L1493:
	movl	$27, (%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L1489:
	ret
	.p2align 4,,10
	.p2align 3
.L1494:
	testl	%esi, %esi
	js	.L1495
	cmpl	200(%rdi), %esi
	jge	.L1495
	xorl	%edi, %edi
	testl	%esi, %esi
	je	.L1497
	movq	208(%rax), %r9
	movslq	%esi, %rdi
	movl	-8(%r9,%rdi,8), %edi
.L1497:
	testq	%rdx, %rdx
	je	.L1498
	movl	%edi, (%rdx)
.L1498:
	testq	%rcx, %rcx
	je	.L1499
	movq	208(%rax), %rdx
	movslq	%esi, %rsi
	movl	(%rdx,%rsi,8), %edx
	movl	%edx, (%rcx)
.L1499:
	testq	%r8, %r8
	je	.L1489
	cmpb	$0, 142(%rax)
	je	.L1500
	movq	208(%rax), %r9
	cmpl	(%r9), %edi
	jl	.L1500
	movl	200(%rax), %edx
	leal	-1(%rdx), %esi
	testl	%edx, %edx
	jle	.L1503
	subl	$1, %edx
	leaq	8(%r9), %rax
	movq	%rdx, %rsi
	leaq	(%rax,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L1504:
	cmpq	%rcx, %rax
	je	.L1503
	movq	%rax, %rdx
	addq	$8, %rax
	cmpl	-8(%rax), %edi
	jge	.L1504
.L1505:
	movzbl	4(%rdx), %eax
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1495:
	movl	$1, (%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L1500:
	movzbl	141(%rax), %eax
.L1502:
	movb	%al, (%r8)
	ret
.L1503:
	movslq	%esi, %rdx
	leaq	(%r9,%rdx,8), %rdx
	jmp	.L1505
	.cfi_endproc
.LFE2141:
	.size	ubidi_getParagraphByIndex_67, .-ubidi_getParagraphByIndex_67
	.p2align 4
	.globl	ubidi_getParagraph_67
	.type	ubidi_getParagraph_67, @function
ubidi_getParagraph_67:
.LFB2142:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L1549
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1549
	testq	%rdi, %rdi
	je	.L1529
	movq	(%rdi), %r11
	cmpq	%rdi, %r11
	je	.L1530
	testq	%r11, %r11
	je	.L1529
	cmpq	(%r11), %r11
	jne	.L1529
.L1530:
	testl	%esi, %esi
	js	.L1531
	cmpl	%esi, 20(%r11)
	jle	.L1531
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	208(%r11), %rdi
	cmpl	(%rdi), %esi
	jl	.L1550
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1534:
	movl	%eax, %r10d
	leaq	0(,%rax,8), %rbx
	addq	$1, %rax
	cmpl	%esi, -8(%rdi,%rax,8)
	jle	.L1534
.L1533:
	movq	(%r11), %rax
	cmpq	%rax, %r11
	je	.L1535
	testq	%rax, %rax
	je	.L1536
	cmpq	(%rax), %rax
	jne	.L1536
.L1535:
	cmpl	%r10d, 200(%r11)
	jle	.L1578
	xorl	%esi, %esi
	testl	%r10d, %r10d
	je	.L1538
	movq	208(%rax), %rsi
	movl	-8(%rsi,%rbx), %esi
.L1538:
	testq	%rdx, %rdx
	je	.L1539
	movl	%esi, (%rdx)
.L1539:
	testq	%rcx, %rcx
	je	.L1540
	movq	208(%rax), %rdx
	movl	(%rdx,%rbx), %edx
	movl	%edx, (%rcx)
.L1540:
	testq	%r8, %r8
	je	.L1527
	cmpb	$0, 142(%rax)
	je	.L1541
	movq	208(%rax), %r9
	cmpl	(%r9), %esi
	jl	.L1541
	movl	200(%rax), %edx
	leal	-1(%rdx), %edi
	testl	%edx, %edx
	jle	.L1544
	subl	$1, %edx
	leaq	8(%r9), %rax
	movq	%rdx, %rdi
	leaq	(%rax,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L1545:
	cmpq	%rcx, %rax
	je	.L1544
	movq	%rax, %rdx
	addq	$8, %rax
	cmpl	-8(%rax), %esi
	jge	.L1545
.L1546:
	movzbl	4(%rdx), %eax
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1536:
	movl	$27, (%r9)
.L1527:
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1578:
	.cfi_restore_state
	movl	$1, (%r9)
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1541:
	.cfi_restore_state
	movzbl	141(%rax), %eax
.L1543:
	movb	%al, (%r8)
	popq	%rbx
	movl	%r10d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1550:
	.cfi_restore_state
	xorl	%r10d, %r10d
	xorl	%ebx, %ebx
	jmp	.L1533
.L1529:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$27, (%r9)
	movl	$-1, %r10d
.L1576:
	movl	%r10d, %eax
	ret
.L1549:
	movl	$-1, %r10d
	jmp	.L1576
.L1544:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movslq	%edi, %rdx
	leaq	(%r9,%rdx,8), %rdx
	jmp	.L1546
.L1531:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, (%r9)
	movl	$-1, %r10d
	jmp	.L1576
	.cfi_endproc
.LFE2142:
	.size	ubidi_getParagraph_67, .-ubidi_getParagraph_67
	.p2align 4
	.globl	ubidi_setClassCallback_67
	.type	ubidi_setClassCallback_67, @function
ubidi_setClassCallback_67:
.LFB2143:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L1579
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1579
	testq	%rdi, %rdi
	je	.L1593
	testq	%rcx, %rcx
	je	.L1582
	movq	448(%rdi), %rax
	movq	%rax, (%rcx)
.L1582:
	testq	%r8, %r8
	je	.L1583
	movq	456(%rdi), %rax
	movq	%rax, (%r8)
.L1583:
	movq	%rsi, 448(%rdi)
	movq	%rdx, 456(%rdi)
.L1579:
	ret
	.p2align 4,,10
	.p2align 3
.L1593:
	movl	$1, (%r9)
	ret
	.cfi_endproc
.LFE2143:
	.size	ubidi_setClassCallback_67, .-ubidi_setClassCallback_67
	.p2align 4
	.globl	ubidi_getClassCallback_67
	.type	ubidi_getClassCallback_67, @function
ubidi_getClassCallback_67:
.LFB2144:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1594
	testq	%rsi, %rsi
	je	.L1597
	movq	448(%rdi), %rax
	movq	%rax, (%rsi)
.L1597:
	testq	%rdx, %rdx
	je	.L1594
	movq	456(%rdi), %rax
	movq	%rax, (%rdx)
.L1594:
	ret
	.cfi_endproc
.LFE2144:
	.size	ubidi_getClassCallback_67, .-ubidi_getClassCallback_67
	.p2align 4
	.globl	ubidi_getCustomizedClass_67
	.type	ubidi_getCustomizedClass_67, @function
ubidi_getCustomizedClass_67:
.LFB2145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$8, %rsp
	movq	448(%rdi), %rax
	testq	%rax, %rax
	je	.L1608
	movq	456(%rdi), %rdi
	call	*%rax
	cmpl	$23, %eax
	je	.L1608
.L1607:
	cmpl	$23, %eax
	movl	$10, %edx
	cmovge	%edx, %eax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1608:
	.cfi_restore_state
	movl	%r12d, %edi
	call	ubidi_getClass_67@PLT
	jmp	.L1607
	.cfi_endproc
.LFE2145:
	.size	ubidi_getCustomizedClass_67, .-ubidi_getCustomizedClass_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL45impTab_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS, @object
	.size	_ZL45impTab_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS, 32
_ZL45impTab_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS:
	.quad	_ZL46impTabL_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS
	.quad	_ZL38impTabR_INVERSE_LIKE_DIRECT_WITH_MARKS
	.quad	_ZL7impAct2
	.quad	_ZL7impAct3
	.section	.rodata
	.align 32
	.type	_ZL46impTabL_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS, @object
	.size	_ZL46impTabL_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS, 40
_ZL46impTabL_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS:
	.string	""
	.string	"b\001\001"
	.zero	3
	.string	""
	.string	"b\001\001"
	.string	"0"
	.ascii	"\004"
	.string	""
	.string	"bTT\0230"
	.ascii	"\003"
	.ascii	"0BTT\00300\003"
	.ascii	"0B\004\004\02300\004"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL34impTab_INVERSE_FOR_NUMBERS_SPECIAL, @object
	.size	_ZL34impTab_INVERSE_FOR_NUMBERS_SPECIAL, 32
_ZL34impTab_INVERSE_FOR_NUMBERS_SPECIAL:
	.quad	_ZL23impTabL_NUMBERS_SPECIAL
	.quad	_ZL27impTabR_INVERSE_LIKE_DIRECT
	.quad	_ZL7impAct0
	.quad	_ZL7impAct1
	.align 32
	.type	_ZL37impTab_INVERSE_LIKE_DIRECT_WITH_MARKS, @object
	.size	_ZL37impTab_INVERSE_LIKE_DIRECT_WITH_MARKS, 32
_ZL37impTab_INVERSE_LIKE_DIRECT_WITH_MARKS:
	.quad	_ZL38impTabL_INVERSE_LIKE_DIRECT_WITH_MARKS
	.quad	_ZL38impTabR_INVERSE_LIKE_DIRECT_WITH_MARKS
	.quad	_ZL7impAct2
	.quad	_ZL7impAct3
	.section	.rodata
	.type	_ZL7impAct3, @object
	.size	_ZL7impAct3, 6
_ZL7impAct3:
	.string	""
	.ascii	"\001\t\n\013\f"
	.type	_ZL7impAct2, @object
	.size	_ZL7impAct2, 7
_ZL7impAct2:
	.string	""
	.ascii	"\001\002\005\006\007\b"
	.align 32
	.type	_ZL38impTabR_INVERSE_LIKE_DIRECT_WITH_MARKS, @object
	.size	_ZL38impTabR_INVERSE_LIKE_DIRECT_WITH_MARKS, 56
_ZL38impTabR_INVERSE_LIKE_DIRECT_WITH_MARKS:
	.string	"\023"
	.string	"\001\001"
	.zero	3
	.string	"#"
	.string	"\001\001\002@"
	.ascii	"\001"
	.string	"#"
	.string	"\001\001\002@"
	.zero	1
	.string	"\003"
	.string	"\0036\024@"
	.ascii	"\001"
	.string	"S@\0056\004@@"
	.ascii	"S@\0056\004@@\001"
	.ascii	"S@\006\006\004@@\003"
	.align 32
	.type	_ZL38impTabL_INVERSE_LIKE_DIRECT_WITH_MARKS, @object
	.size	_ZL38impTabL_INVERSE_LIKE_DIRECT_WITH_MARKS, 56
_ZL38impTabL_INVERSE_LIKE_DIRECT_WITH_MARKS:
	.string	""
	.string	"c"
	.string	"\001"
	.zero	3
	.string	""
	.string	"c"
	.string	"\001\0220"
	.ascii	"\004"
	.ascii	" c \001\0020 \003"
	.string	""
	.string	"cUV\0240"
	.ascii	"\003"
	.ascii	"0CUV\00400\003"
	.ascii	"0C\005V\02400\004"
	.ascii	"0CU\006\02400\004"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL26impTab_INVERSE_LIKE_DIRECT, @object
	.size	_ZL26impTab_INVERSE_LIKE_DIRECT, 32
_ZL26impTab_INVERSE_LIKE_DIRECT:
	.quad	_ZL15impTabL_DEFAULT
	.quad	_ZL27impTabR_INVERSE_LIKE_DIRECT
	.quad	_ZL7impAct0
	.quad	_ZL7impAct1
	.section	.rodata
	.type	_ZL7impAct1, @object
	.size	_ZL7impAct1, 4
_ZL7impAct1:
	.string	""
	.ascii	"\001\r\016"
	.align 32
	.type	_ZL27impTabR_INVERSE_LIKE_DIRECT, @object
	.size	_ZL27impTabR_INVERSE_LIKE_DIRECT, 56
_ZL27impTabR_INVERSE_LIKE_DIRECT:
	.string	"\001"
	.string	"\002\002"
	.zero	3
	.string	"\001"
	.string	"\001\002\023\023"
	.ascii	"\001"
	.string	"\001"
	.string	"\002\002"
	.string	""
	.string	""
	.ascii	"\001"
	.string	"!0\006\004\003\0030"
	.ascii	"!0\006\004\005\0050\003"
	.ascii	"!0\006\004\005\0050\002"
	.ascii	"!0\006\004\003\0030\001"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL27impTab_INVERSE_NUMBERS_AS_L, @object
	.size	_ZL27impTab_INVERSE_NUMBERS_AS_L, 32
_ZL27impTab_INVERSE_NUMBERS_AS_L:
	.quad	_ZL28impTabL_INVERSE_NUMBERS_AS_L
	.quad	_ZL28impTabR_INVERSE_NUMBERS_AS_L
	.quad	_ZL7impAct0
	.quad	_ZL7impAct0
	.section	.rodata
	.align 32
	.type	_ZL28impTabR_INVERSE_NUMBERS_AS_L, @object
	.size	_ZL28impTabR_INVERSE_NUMBERS_AS_L, 48
_ZL28impTabR_INVERSE_NUMBERS_AS_L:
	.string	"\001"
	.string	"\001\001"
	.zero	3
	.string	"\001"
	.string	"\001\001\024\024"
	.ascii	"\001"
	.string	"\001"
	.string	"\001\001"
	.string	""
	.string	""
	.ascii	"\001"
	.string	"\001"
	.string	"\001\001\005\005"
	.ascii	"\001"
	.string	"!"
	.string	"!!\004\004"
	.zero	1
	.string	"\001"
	.string	"\001\001\005\005"
	.zero	1
	.align 32
	.type	_ZL28impTabL_INVERSE_NUMBERS_AS_L, @object
	.size	_ZL28impTabL_INVERSE_NUMBERS_AS_L, 48
_ZL28impTabL_INVERSE_NUMBERS_AS_L:
	.string	""
	.string	"\001"
	.zero	5
	.string	""
	.string	"\001"
	.string	""
	.string	"\024\024"
	.ascii	"\001"
	.string	""
	.string	"\001"
	.string	""
	.string	"\025\025"
	.ascii	"\002"
	.string	""
	.string	"\001"
	.string	""
	.string	"\024\024"
	.ascii	"\002"
	.ascii	" \001  \004\004 \001"
	.ascii	" \001  \005\005 \001"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL27impTab_GROUP_NUMBERS_WITH_R, @object
	.size	_ZL27impTab_GROUP_NUMBERS_WITH_R, 32
_ZL27impTab_GROUP_NUMBERS_WITH_R:
	.quad	_ZL28impTabL_GROUP_NUMBERS_WITH_R
	.quad	_ZL28impTabR_GROUP_NUMBERS_WITH_R
	.quad	_ZL7impAct0
	.quad	_ZL7impAct0
	.section	.rodata
	.align 32
	.type	_ZL28impTabR_GROUP_NUMBERS_WITH_R, @object
	.size	_ZL28impTabR_GROUP_NUMBERS_WITH_R, 40
_ZL28impTabR_GROUP_NUMBERS_WITH_R:
	.string	"\002"
	.string	"\001\001"
	.zero	3
	.string	"\002"
	.string	"\001\001"
	.string	""
	.string	""
	.ascii	"\001"
	.string	"\002"
	.string	"\024\024\023"
	.string	""
	.ascii	"\001"
	.string	"\""
	.string	"\004\004\003"
	.zero	2
	.string	"\""
	.string	"\004\004\003"
	.string	""
	.ascii	"\001"
	.align 32
	.type	_ZL28impTabL_GROUP_NUMBERS_WITH_R, @object
	.size	_ZL28impTabL_GROUP_NUMBERS_WITH_R, 48
_ZL28impTabL_GROUP_NUMBERS_WITH_R:
	.string	""
	.string	"\003\021\021"
	.zero	3
	.ascii	" \003\001\001\002  \002"
	.ascii	" \003\001\001\002  \001"
	.string	""
	.string	"\003\005\005\024"
	.string	""
	.ascii	"\001"
	.ascii	" \003\005\005\004  \001"
	.string	""
	.string	"\003\005\005\024"
	.string	""
	.ascii	"\002"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL22impTab_NUMBERS_SPECIAL, @object
	.size	_ZL22impTab_NUMBERS_SPECIAL, 32
_ZL22impTab_NUMBERS_SPECIAL:
	.quad	_ZL23impTabL_NUMBERS_SPECIAL
	.quad	_ZL15impTabR_DEFAULT
	.quad	_ZL7impAct0
	.quad	_ZL7impAct0
	.section	.rodata
	.align 32
	.type	_ZL23impTabL_NUMBERS_SPECIAL, @object
	.size	_ZL23impTabL_NUMBERS_SPECIAL, 40
_ZL23impTabL_NUMBERS_SPECIAL:
	.string	""
	.string	"\002\021\021"
	.zero	3
	.string	""
	.string	"B\001\001"
	.zero	3
	.string	""
	.string	"\002\004\004\023\023"
	.ascii	"\001"
	.string	""
	.string	"\"44\003\003"
	.zero	1
	.string	""
	.string	"\002\004\004\023\023"
	.ascii	"\002"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL14impTab_DEFAULT, @object
	.size	_ZL14impTab_DEFAULT, 32
_ZL14impTab_DEFAULT:
	.quad	_ZL15impTabL_DEFAULT
	.quad	_ZL15impTabR_DEFAULT
	.quad	_ZL7impAct0
	.quad	_ZL7impAct0
	.section	.rodata
	.type	_ZL7impAct0, @object
	.size	_ZL7impAct0, 5
_ZL7impAct0:
	.string	""
	.ascii	"\001\002\003\004"
	.align 32
	.type	_ZL15impTabR_DEFAULT, @object
	.size	_ZL15impTabR_DEFAULT, 48
_ZL15impTabR_DEFAULT:
	.string	"\001"
	.string	"\002\002"
	.zero	3
	.string	"\001"
	.string	"\001\003\024\024"
	.ascii	"\001"
	.string	"\001"
	.string	"\002\002"
	.string	""
	.string	""
	.ascii	"\001"
	.string	"\001"
	.string	"\001\003\005\005"
	.ascii	"\001"
	.string	"!"
	.string	"!\003\004\004"
	.zero	1
	.string	"\001"
	.string	"\001\003\005\005"
	.zero	1
	.align 32
	.type	_ZL15impTabL_DEFAULT, @object
	.size	_ZL15impTabL_DEFAULT, 48
_ZL15impTabL_DEFAULT:
	.string	""
	.string	"\001"
	.string	"\002"
	.zero	3
	.string	""
	.string	"\001\003\003\024\024"
	.ascii	"\001"
	.string	""
	.string	"\001"
	.string	"\002\025\025"
	.ascii	"\002"
	.string	""
	.string	"\001\003\003\024\024"
	.ascii	"\002"
	.string	""
	.string	"!33\004\004"
	.zero	1
	.string	""
	.string	"!"
	.string	"2\005\005"
	.zero	1
	.align 32
	.type	_ZL11impTabProps, @object
	.size	_ZL11impTabProps, 384
_ZL11impTabProps:
	.string	"\001\002\004\005\007\017\021\007\t\007"
	.ascii	"\007\003\022\025\004"
	.string	"\001\"$%'/1')'\001\001#25"
	.ascii	"!\002$%'/1')'\002\002#25\001"
	.ascii	"!\"&&(01(((\003\003\00325\001"
	.ascii	"!\"\004%'/1J\013J\004\004#\022\025\002"
	.ascii	"!\"$\005'/1')L\005\005#25\003"
	.ascii	"!\"\006\006(01((M\006\006#\022\025\003"
	.ascii	"!\"$%\007/1\007N\007\007\007#25\004"
	.ascii	"!\"&&\b01\b\b\b\b\b#25\004"
	.ascii	"!\"\004%\007/1\007\t\007\t\t#\022\025\004"
	.ascii	"ab\004e\207oq\207\216\207\n\207c\022\025\002"
	.ascii	"!\"\004%'/1'\013'\013\013#\022\025\002"
	.ascii	"abd\005\207oq\207\216\207\f\207cru\003"
	.ascii	"ab\006\006\210pq\210\210\210\r\210c\022\025\003"
	.ascii	"!\"\204%\007/1\007\016\007\016\016#\222\225\004"
	.ascii	"!\"$%'\0171')'\017'#25\005"
	.ascii	"!\"&&(\0201(((\020(#25\005"
	.ascii	"!\"$%'/\021')'\021'#25\006"
	.string	"!\"\022%'/1S\024S\022\022#\022\025"
	.string	"ab\022e\207oq\207\216\207\023\207c\022\025"
	.string	"!\"\022%'/1'\024'\024\024#\022\025"
	.ascii	"!\"\025%'/1V\027V\025\025#\022\025\003"
	.ascii	"ab\025e\207oq\207\216\207\026\207c\022\025\003"
	.ascii	"!\"\025%'/1'\027'\027\027#\022\025\003"
	.align 16
	.type	_ZL9groupProp, @object
	.size	_ZL9groupProp, 25
_ZL9groupProp:
	.string	""
	.ascii	"\001\002\007\b\003\t\006\005\004\004\n\n\f\n\n\n\013\n\004\004"
	.ascii	"\004\004\r\016"
	.align 8
	.type	_ZL5flagO, @object
	.size	_ZL5flagO, 8
_ZL5flagO:
	.long	4096
	.long	32768
	.align 8
	.type	_ZL5flagE, @object
	.size	_ZL5flagE, 8
_ZL5flagE:
	.long	2048
	.long	16384
	.align 8
	.type	_ZL6flagLR, @object
	.size	_ZL6flagLR, 8
_ZL6flagLR:
	.long	1
	.long	2
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.byte	-2
	.align 16
.LC3:
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
