	.file	"udataswp.cpp"
	.text
	.p2align 4
	.type	_ZL19uprv_readSwapUInt16t, @function
_ZL19uprv_readSwapUInt16t:
.LFB2080:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	rolw	$8, %ax
	ret
	.cfi_endproc
.LFE2080:
	.size	_ZL19uprv_readSwapUInt16t, .-_ZL19uprv_readSwapUInt16t
	.p2align 4
	.type	_ZL21uprv_readDirectUInt16t, @function
_ZL21uprv_readDirectUInt16t:
.LFB2081:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE2081:
	.size	_ZL21uprv_readDirectUInt16t, .-_ZL21uprv_readDirectUInt16t
	.p2align 4
	.type	_ZL19uprv_readSwapUInt32j, @function
_ZL19uprv_readSwapUInt32j:
.LFB2082:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	bswap	%eax
	ret
	.cfi_endproc
.LFE2082:
	.size	_ZL19uprv_readSwapUInt32j, .-_ZL19uprv_readSwapUInt32j
	.p2align 4
	.type	_ZL21uprv_readDirectUInt32j, @function
_ZL21uprv_readDirectUInt32j:
.LFB2083:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE2083:
	.size	_ZL21uprv_readDirectUInt32j, .-_ZL21uprv_readDirectUInt32j
	.p2align 4
	.type	_ZL20uprv_writeSwapUInt16Ptt, @function
_ZL20uprv_writeSwapUInt16Ptt:
.LFB2084:
	.cfi_startproc
	endbr64
	rolw	$8, %si
	movw	%si, (%rdi)
	ret
	.cfi_endproc
.LFE2084:
	.size	_ZL20uprv_writeSwapUInt16Ptt, .-_ZL20uprv_writeSwapUInt16Ptt
	.p2align 4
	.type	_ZL22uprv_writeDirectUInt16Ptt, @function
_ZL22uprv_writeDirectUInt16Ptt:
.LFB2085:
	.cfi_startproc
	endbr64
	movw	%si, (%rdi)
	ret
	.cfi_endproc
.LFE2085:
	.size	_ZL22uprv_writeDirectUInt16Ptt, .-_ZL22uprv_writeDirectUInt16Ptt
	.p2align 4
	.type	_ZL20uprv_writeSwapUInt32Pjj, @function
_ZL20uprv_writeSwapUInt32Pjj:
.LFB2086:
	.cfi_startproc
	endbr64
	bswap	%esi
	movl	%esi, (%rdi)
	ret
	.cfi_endproc
.LFE2086:
	.size	_ZL20uprv_writeSwapUInt32Pjj, .-_ZL20uprv_writeSwapUInt32Pjj
	.p2align 4
	.type	_ZL22uprv_writeDirectUInt32Pjj, @function
_ZL22uprv_writeDirectUInt32Pjj:
.LFB2087:
	.cfi_startproc
	endbr64
	movl	%esi, (%rdi)
	ret
	.cfi_endproc
.LFE2087:
	.size	_ZL22uprv_writeDirectUInt32Pjj, .-_ZL22uprv_writeDirectUInt32Pjj
	.p2align 4
	.type	_ZL16uprv_swapArray16PK12UDataSwapperPKviPvP10UErrorCode, @function
_ZL16uprv_swapArray16PK12UDataSwapperPKviPvP10UErrorCode:
.LFB2074:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L10
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L10
	testq	%rdi, %rdi
	sete	%dil
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %dil
	jne	.L12
	testl	%edx, %edx
	js	.L12
	testq	%rcx, %rcx
	je	.L12
	testb	$1, %dl
	jne	.L12
	movl	%edx, %r8d
	sarl	%r8d
	je	.L19
	leaq	15(%rsi), %rax
	subq	%rcx, %rax
	cmpq	$30, %rax
	leal	-1(%r8), %eax
	jbe	.L16
	cmpl	$6, %eax
	jbe	.L16
	movl	%r8d, %edi
	xorl	%eax, %eax
	shrl	$3, %edi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L18:
	movdqu	(%rsi,%rax), %xmm0
	movdqa	%xmm0, %xmm1
	psllw	$8, %xmm0
	psrlw	$8, %xmm1
	por	%xmm1, %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L18
	movl	%r8d, %r9d
	movl	%r8d, %edi
	andl	$-8, %r9d
	andl	$7, %edi
	movl	%r9d, %eax
	addq	%rax, %rax
	addq	%rax, %rsi
	addq	%rax, %rcx
	cmpl	%r9d, %r8d
	je	.L19
	movzwl	(%rsi), %eax
	rolw	$8, %ax
	movw	%ax, (%rcx)
	cmpl	$1, %edi
	je	.L19
	movzwl	2(%rsi), %eax
	rolw	$8, %ax
	movw	%ax, 2(%rcx)
	cmpl	$2, %edi
	je	.L19
	movzwl	4(%rsi), %eax
	rolw	$8, %ax
	movw	%ax, 4(%rcx)
	cmpl	$3, %edi
	je	.L19
	movzwl	6(%rsi), %eax
	rolw	$8, %ax
	movw	%ax, 6(%rcx)
	cmpl	$4, %edi
	je	.L19
	movzwl	8(%rsi), %eax
	rolw	$8, %ax
	movw	%ax, 8(%rcx)
	cmpl	$5, %edi
	je	.L19
	movzwl	10(%rsi), %eax
	rolw	$8, %ax
	movw	%ax, 10(%rcx)
	cmpl	$6, %edi
	je	.L19
	movzwl	12(%rsi), %eax
	rolw	$8, %ax
	movw	%ax, 12(%rcx)
.L19:
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, (%r8)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	%eax, %r8d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L22:
	movzwl	(%rsi,%rax,2), %edi
	rolw	$8, %di
	movw	%di, (%rcx,%rax,2)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%r8, %rdi
	jne	.L22
	jmp	.L19
	.cfi_endproc
.LFE2074:
	.size	_ZL16uprv_swapArray16PK12UDataSwapperPKviPvP10UErrorCode, .-_ZL16uprv_swapArray16PK12UDataSwapperPKviPvP10UErrorCode
	.p2align 4
	.type	_ZL16uprv_copyArray32PK12UDataSwapperPKviPvP10UErrorCode, @function
_ZL16uprv_copyArray32PK12UDataSwapperPKviPvP10UErrorCode:
.LFB2077:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L54
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	subq	$8, %rsp
	testq	%rdi, %rdi
	sete	%dl
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %dl
	jne	.L51
	testl	%ebx, %ebx
	js	.L51
	testb	$3, %bl
	jne	.L51
	testq	%rcx, %rcx
	je	.L51
	testl	%ebx, %ebx
	jle	.L56
	cmpq	%rcx, %rsi
	jne	.L67
.L56:
	movl	%ebx, %eax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$1, (%r8)
	xorl	%eax, %eax
.L49:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movslq	%ebx, %rdx
	movq	%rcx, %rdi
	call	memcpy@PLT
	movl	%ebx, %eax
	jmp	.L49
	.cfi_endproc
.LFE2077:
	.size	_ZL16uprv_copyArray32PK12UDataSwapperPKviPvP10UErrorCode, .-_ZL16uprv_copyArray32PK12UDataSwapperPKviPvP10UErrorCode
	.p2align 4
	.type	_ZL16uprv_swapArray32PK12UDataSwapperPKviPvP10UErrorCode, @function
_ZL16uprv_swapArray32PK12UDataSwapperPKviPvP10UErrorCode:
.LFB2076:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L68
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L68
	testq	%rdi, %rdi
	sete	%dil
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %dil
	jne	.L70
	testl	%edx, %edx
	js	.L70
	testb	$3, %dl
	jne	.L70
	testq	%rcx, %rcx
	je	.L70
	movl	%edx, %edi
	movl	$0, %eax
	sarl	$2, %edi
	leal	-1(%rdi), %r8d
	je	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	movl	(%rsi,%rax,4), %edi
	bswap	%edi
	movl	%edi, (%rcx,%rax,4)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%r8, %rdi
	jne	.L75
.L74:
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$1, (%r8)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2076:
	.size	_ZL16uprv_swapArray32PK12UDataSwapperPKviPvP10UErrorCode, .-_ZL16uprv_swapArray32PK12UDataSwapperPKviPvP10UErrorCode
	.p2align 4
	.type	_ZL16uprv_swapArray64PK12UDataSwapperPKviPvP10UErrorCode, @function
_ZL16uprv_swapArray64PK12UDataSwapperPKviPvP10UErrorCode:
.LFB2078:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L79
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L79
	testq	%rdi, %rdi
	sete	%dil
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %dil
	jne	.L81
	testl	%edx, %edx
	js	.L81
	testb	$7, %dl
	jne	.L81
	testq	%rcx, %rcx
	je	.L81
	movl	%edx, %edi
	movl	$0, %eax
	sarl	$3, %edi
	leal	-1(%rdi), %r8d
	je	.L85
	.p2align 4,,10
	.p2align 3
.L86:
	movq	(%rsi,%rax,8), %rdi
	bswap	%rdi
	movq	%rdi, (%rcx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%r8, %rdi
	jne	.L86
.L85:
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$1, (%r8)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2078:
	.size	_ZL16uprv_swapArray64PK12UDataSwapperPKviPvP10UErrorCode, .-_ZL16uprv_swapArray64PK12UDataSwapperPKviPvP10UErrorCode
	.p2align 4
	.type	_ZL16uprv_copyArray64PK12UDataSwapperPKviPvP10UErrorCode, @function
_ZL16uprv_copyArray64PK12UDataSwapperPKviPvP10UErrorCode:
.LFB2079:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L95
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L105
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	subq	$8, %rsp
	testq	%rdi, %rdi
	sete	%dl
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %dl
	jne	.L92
	testl	%ebx, %ebx
	js	.L92
	testb	$7, %bl
	jne	.L92
	testq	%rcx, %rcx
	je	.L92
	testl	%ebx, %ebx
	jle	.L97
	cmpq	%rcx, %rsi
	jne	.L108
.L97:
	movl	%ebx, %eax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$1, (%r8)
	xorl	%eax, %eax
.L90:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movslq	%ebx, %rdx
	movq	%rcx, %rdi
	call	memcpy@PLT
	movl	%ebx, %eax
	jmp	.L90
	.cfi_endproc
.LFE2079:
	.size	_ZL16uprv_copyArray64PK12UDataSwapperPKviPvP10UErrorCode, .-_ZL16uprv_copyArray64PK12UDataSwapperPKviPvP10UErrorCode
	.p2align 4
	.type	_ZL16uprv_copyArray16PK12UDataSwapperPKviPvP10UErrorCode, @function
_ZL16uprv_copyArray16PK12UDataSwapperPKviPvP10UErrorCode:
.LFB2075:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L114
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L124
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	subq	$8, %rsp
	testq	%rdi, %rdi
	sete	%dl
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %dl
	jne	.L111
	testl	%ebx, %ebx
	js	.L111
	testq	%rcx, %rcx
	je	.L111
	testb	$1, %bl
	jne	.L111
	testl	%ebx, %ebx
	jle	.L116
	cmpq	%rcx, %rsi
	jne	.L127
.L116:
	movl	%ebx, %eax
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$1, (%r8)
	xorl	%eax, %eax
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movslq	%ebx, %rdx
	movq	%rcx, %rdi
	call	memcpy@PLT
	movl	%ebx, %eax
	jmp	.L109
	.cfi_endproc
.LFE2075:
	.size	_ZL16uprv_copyArray16PK12UDataSwapperPKviPvP10UErrorCode, .-_ZL16uprv_copyArray16PK12UDataSwapperPKviPvP10UErrorCode
	.p2align 4
	.globl	udata_readInt16_67
	.type	udata_readInt16_67, @function
udata_readInt16_67:
.LFB2088:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movzwl	%si, %edi
	jmp	*8(%r8)
	.cfi_endproc
.LFE2088:
	.size	udata_readInt16_67, .-udata_readInt16_67
	.p2align 4
	.globl	udata_readInt32_67
	.type	udata_readInt32_67, @function
udata_readInt32_67:
.LFB2089:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movl	%esi, %edi
	jmp	*16(%r8)
	.cfi_endproc
.LFE2089:
	.size	udata_readInt32_67, .-udata_readInt32_67
	.p2align 4
	.globl	udata_swapInvStringBlock_67
	.type	udata_swapInvStringBlock_67, @function
udata_swapInvStringBlock_67:
.LFB2090:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L158
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L157
	movl	%edx, %r12d
	testq	%rsi, %rsi
	movl	%r12d, %eax
	sete	%dl
	shrl	$31, %eax
	orb	%al, %dl
	jne	.L134
	testq	%rdi, %rdi
	je	.L134
	testl	%r12d, %r12d
	setg	%r15b
	testq	%rcx, %rcx
	jne	.L135
	testb	%r15b, %r15b
	jne	.L134
.L135:
	movq	72(%rdi), %rax
	testl	%r12d, %r12d
	je	.L136
	movslq	%r12d, %rbx
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L160:
	subq	$1, %rbx
	movl	%ebx, %r14d
	testl	%ebx, %ebx
	je	.L159
.L139:
	cmpb	$0, -1(%rsi,%rbx)
	movl	%ebx, %r14d
	jne	.L160
	cmpl	%ebx, %r12d
	setg	%r15b
.L138:
	movq	%rcx, -64(%rbp)
	movq	%r13, %r8
	movl	%r14d, %edx
	movq	%rsi, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	cmpq	%rcx, %rsi
	je	.L140
	testb	%r15b, %r15b
	je	.L140
	movl	%r12d, %edx
	leaq	(%rcx,%rbx), %rdi
	addq	%rbx, %rsi
	subl	%r14d, %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
.L140:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L157
	movl	%r12d, %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$1, 0(%r13)
.L157:
	xorl	%eax, %eax
.L130:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	xorl	%ebx, %ebx
	jmp	.L138
.L136:
	movq	%r13, %r8
	xorl	%edx, %edx
	call	*%rax
	jmp	.L140
	.cfi_endproc
.LFE2090:
	.size	udata_swapInvStringBlock_67, .-udata_swapInvStringBlock_67
	.p2align 4
	.globl	udata_printError_67
	.type	udata_printError_67, @function
udata_printError_67:
.LFB2091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L162
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L162:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 80(%r10)
	je	.L161
	leaq	16(%rbp), %rax
	movq	88(%r10), %rdi
	leaq	-208(%rbp), %rdx
	movl	$16, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	call	*80(%r10)
.L161:
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L166:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2091:
	.size	udata_printError_67, .-udata_printError_67
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"udata_swapDataHeader(): initial bytes do not look like ICU data\n"
	.align 8
.LC1:
	.string	"udata_swapDataHeader(): header size mismatch - headerSize %d infoSize %d length %d\n"
	.text
	.p2align 4
	.globl	udata_swapDataHeader_67
	.type	udata_swapDataHeader_67, @function
udata_swapDataHeader_67:
.LFB2092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r8, %r8
	je	.L181
	movl	(%r8), %eax
	movq	%r8, %rbx
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jg	.L167
	testq	%rsi, %rsi
	movq	%rcx, %r15
	movq	%rsi, %r13
	sete	%cl
	cmpl	$-1, %edx
	setl	%al
	orb	%al, %cl
	jne	.L169
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L169
	testl	%edx, %edx
	jle	.L170
	testq	%r15, %r15
	je	.L169
.L170:
	cmpl	$23, %edx
	jbe	.L171
	cmpw	$10202, 2(%r13)
	jne	.L171
	cmpb	$2, 10(%r13)
	jne	.L171
	movl	%edx, -56(%rbp)
	movzwl	0(%r13), %edi
	call	*8(%r14)
	movzwl	4(%r13), %edi
	movzwl	%ax, %r12d
	call	*8(%r14)
	cmpw	$23, %r12w
	movl	-56(%rbp), %edx
	movl	%eax, %r10d
	jbe	.L175
	cmpw	$19, %ax
	jbe	.L175
	movzwl	%ax, %ecx
	movzwl	%r12w, %r8d
	leaq	4(%rcx), %r9
	cmpq	%r9, %r8
	jb	.L175
	cmpl	$-1, %edx
	je	.L167
	cmpl	%r12d, %edx
	jl	.L175
	cmpq	%r15, %r13
	je	.L178
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%r10d, -60(%rbp)
	movq	%r9, -56(%rbp)
	call	memcpy@PLT
	movl	-60(%rbp), %r10d
	movq	-56(%rbp), %r9
.L178:
	movzbl	2(%r14), %eax
	movl	%r10d, -60(%rbp)
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r9, -56(%rbp)
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movb	%al, 8(%r15)
	movzbl	3(%r14), %eax
	movb	%al, 9(%r15)
	call	*48(%r14)
	leaq	4(%r13), %rsi
	movq	%r14, %rdi
	leaq	4(%r15), %rcx
	movq	%rbx, %r8
	movl	$4, %edx
	call	*48(%r14)
	movl	-60(%rbp), %r10d
	movq	-56(%rbp), %r9
	movl	%r12d, %edi
	leal	4(%r10), %eax
	leaq	0(%r13,%r9), %rsi
	movzwl	%ax, %eax
	subl	%eax, %edi
	testl	%edi, %edi
	jle	.L183
	leal	-1(%rdi), %edi
	xorl	%eax, %eax
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L196:
	leal	1(%rax), %edx
	leaq	1(%rax), %rcx
	cmpq	%rax, %rdi
	je	.L179
	movq	%rcx, %rax
.L180:
	cmpb	$0, (%rsi,%rax)
	movl	%eax, %edx
	jne	.L196
.L179:
	leaq	(%r15,%r9), %rcx
	movq	%rbx, %r8
	movq	%r14, %rdi
	call	*72(%r14)
.L167:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	udata_printError_67
	movl	$16, (%rbx)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L181:
	xorl	%r12d, %r12d
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L175:
	movl	%edx, %r8d
	movzwl	%r10w, %ecx
	movl	%r12d, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	udata_printError_67
	movl	$8, (%rbx)
	jmp	.L167
.L183:
	xorl	%edx, %edx
	jmp	.L179
	.cfi_endproc
.LFE2092:
	.size	udata_swapDataHeader_67, .-udata_swapDataHeader_67
	.p2align 4
	.globl	udata_openSwapper_67
	.type	udata_openSwapper_67, @function
udata_openSwapper_67:
.LFB2093:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L210
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L197
	movl	%esi, %r15d
	cmpb	$1, %sil
	ja	.L217
	movl	%ecx, %r12d
	cmpb	$1, %cl
	ja	.L217
	movl	%edi, %r14d
	movl	$96, %edi
	movl	%edx, %r13d
	call	uprv_malloc_67@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L222
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	leaq	_ZL21uprv_readDirectUInt16t(%rip), %rdx
	andq	$-8, %rdi
	movq	$0, 88(%rax)
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$96, %ecx
	shrl	$3, %ecx
	testb	%r14b, %r14b
	rep stosq
	leaq	_ZL19uprv_readSwapUInt16t(%rip), %rax
	movb	%r14b, (%r9)
	cmovne	%rax, %rdx
	leaq	_ZL19uprv_readSwapUInt32j(%rip), %rax
	movb	%r15b, 1(%r9)
	movb	%r13b, 2(%r9)
	movq	%rdx, %xmm0
	leaq	_ZL21uprv_readDirectUInt32j(%rip), %rdx
	movb	%r12b, 3(%r9)
	cmove	%rdx, %rax
	testb	%r13b, %r13b
	leaq	_ZL20uprv_writeSwapUInt16Ptt(%rip), %rdx
	movq	%rax, %xmm1
	leaq	_ZL22uprv_writeDirectUInt16Ptt(%rip), %rax
	cmove	%rax, %rdx
	punpcklqdq	%xmm1, %xmm0
	leaq	_ZL22uprv_writeDirectUInt32Pjj(%rip), %rax
	movups	%xmm0, 8(%r9)
	movq	%rdx, %xmm0
	leaq	_ZL20uprv_writeSwapUInt32Pjj(%rip), %rdx
	cmovne	%rdx, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 32(%r9)
	testb	%r12b, %r12b
	je	.L214
	movq	uprv_compareInvEbcdic_67@GOTPCREL(%rip), %rax
	movq	%rax, 24(%r9)
	cmpb	%r13b, %r14b
	je	.L223
.L205:
	leaq	_ZL16uprv_swapArray32PK12UDataSwapperPKviPvP10UErrorCode(%rip), %rax
	leaq	_ZL16uprv_swapArray16PK12UDataSwapperPKviPvP10UErrorCode(%rip), %rsi
	movq	%rax, %xmm3
	leaq	_ZL16uprv_swapArray64PK12UDataSwapperPKviPvP10UErrorCode(%rip), %rax
	movq	%rsi, %xmm0
	movq	%rax, 64(%r9)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 48(%r9)
.L206:
	testb	%r15b, %r15b
	jne	.L207
	testb	%r12b, %r12b
	jne	.L224
	movq	uprv_copyAscii_67@GOTPCREL(%rip), %rax
	movq	%rax, 72(%r9)
.L197:
	addq	$8, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r9d, %r9d
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L207:
	cmpb	$1, %r12b
	je	.L216
	movq	uprv_asciiFromEbcdic_67@GOTPCREL(%rip), %rax
.L209:
	movq	%rax, 72(%r9)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L214:
	movq	uprv_compareInvAscii_67@GOTPCREL(%rip), %rax
	movq	%rax, 24(%r9)
	cmpb	%r13b, %r14b
	jne	.L205
.L223:
	leaq	_ZL16uprv_copyArray32PK12UDataSwapperPKviPvP10UErrorCode(%rip), %rax
	leaq	_ZL16uprv_copyArray16PK12UDataSwapperPKviPvP10UErrorCode(%rip), %rbx
	movq	%rax, %xmm4
	movq	%rbx, %xmm0
	leaq	_ZL16uprv_copyArray64PK12UDataSwapperPKviPvP10UErrorCode(%rip), %rax
	punpcklqdq	%xmm4, %xmm0
	movq	%rax, 64(%r9)
	movups	%xmm0, 48(%r9)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L224:
	movq	uprv_ebcdicFromAscii_67@GOTPCREL(%rip), %rax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L216:
	movq	uprv_copyEbcdic_67@GOTPCREL(%rip), %rax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%r9d, %r9d
	movq	%r9, %rax
	ret
.L222:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, (%rbx)
	jmp	.L197
	.cfi_endproc
.LFE2093:
	.size	udata_openSwapper_67, .-udata_openSwapper_67
	.p2align 4
	.globl	udata_openSwapperForInputData_67
	.type	udata_openSwapperForInputData_67, @function
udata_openSwapperForInputData_67:
.LFB2094:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L225
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L225
	testq	%rdi, %rdi
	sete	%r9b
	cmpl	$23, %esi
	setbe	%al
	orb	%al, %r9b
	jne	.L234
	cmpb	$1, %cl
	ja	.L234
	cmpw	$10202, 2(%rdi)
	jne	.L229
	cmpb	$2, 10(%rdi)
	jne	.L229
	movzbl	8(%rdi), %r10d
	movzwl	(%rdi), %r9d
	movzwl	4(%rdi), %eax
	testb	%r10b, %r10b
	jne	.L239
.L231:
	cmpw	$23, %r9w
	jbe	.L229
	cmpw	$19, %ax
	jbe	.L229
	movzwl	%ax, %eax
	movzwl	%r9w, %r11d
	addq	$4, %rax
	cmpq	%rax, %r11
	jb	.L229
	movzwl	%r9w, %r9d
	cmpl	%r9d, %esi
	jb	.L229
	movzbl	9(%rdi), %esi
	movzbl	%cl, %ecx
	movsbl	%dl, %edx
	movsbl	%r10b, %edi
	jmp	udata_openSwapper_67
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$1, (%r8)
.L225:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$16, (%r8)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	rolw	$8, %r9w
	rolw	$8, %ax
	jmp	.L231
	.cfi_endproc
.LFE2094:
	.size	udata_openSwapperForInputData_67, .-udata_openSwapperForInputData_67
	.p2align 4
	.globl	udata_closeSwapper_67
	.type	udata_closeSwapper_67, @function
udata_closeSwapper_67:
.LFB2095:
	.cfi_startproc
	endbr64
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2095:
	.size	udata_closeSwapper_67, .-udata_closeSwapper_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
