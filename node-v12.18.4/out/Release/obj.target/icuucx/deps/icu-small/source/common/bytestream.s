	.file	"bytestream.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_678ByteSink15GetAppendBufferEiiPciPi
	.type	_ZN6icu_678ByteSink15GetAppendBufferEiiPciPi, @function
_ZN6icu_678ByteSink15GetAppendBufferEiiPciPi:
.LFB2067:
	.cfi_startproc
	endbr64
	movq	%rcx, %rax
	testl	%esi, %esi
	jle	.L4
	cmpl	%r8d, %esi
	jg	.L4
	movl	%r8d, (%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movl	%r8d, (%r9)
	ret
	.cfi_endproc
.LFE2067:
	.size	_ZN6icu_678ByteSink15GetAppendBufferEiiPciPi, .-_ZN6icu_678ByteSink15GetAppendBufferEiiPciPi
	.align 2
	.p2align 4
	.globl	_ZN6icu_678ByteSink5FlushEv
	.type	_ZN6icu_678ByteSink5FlushEv, @function
_ZN6icu_678ByteSink5FlushEv:
.LFB2068:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2068:
	.size	_ZN6icu_678ByteSink5FlushEv, .-_ZN6icu_678ByteSink5FlushEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CheckedArrayByteSinkD2Ev
	.type	_ZN6icu_6720CheckedArrayByteSinkD2Ev, @function
_ZN6icu_6720CheckedArrayByteSinkD2Ev:
.LFB2073:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2073:
	.size	_ZN6icu_6720CheckedArrayByteSinkD2Ev, .-_ZN6icu_6720CheckedArrayByteSinkD2Ev
	.globl	_ZN6icu_6720CheckedArrayByteSinkD1Ev
	.set	_ZN6icu_6720CheckedArrayByteSinkD1Ev,_ZN6icu_6720CheckedArrayByteSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CheckedArrayByteSink5ResetEv
	.type	_ZN6icu_6720CheckedArrayByteSink5ResetEv, @function
_ZN6icu_6720CheckedArrayByteSink5ResetEv:
.LFB2076:
	.cfi_startproc
	endbr64
	movq	$0, 20(%rdi)
	movq	%rdi, %rax
	movb	$0, 28(%rdi)
	ret
	.cfi_endproc
.LFE2076:
	.size	_ZN6icu_6720CheckedArrayByteSink5ResetEv, .-_ZN6icu_6720CheckedArrayByteSink5ResetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CheckedArrayByteSink15GetAppendBufferEiiPciPi
	.type	_ZN6icu_6720CheckedArrayByteSink15GetAppendBufferEiiPciPi, @function
_ZN6icu_6720CheckedArrayByteSink15GetAppendBufferEiiPciPi:
.LFB2078:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jle	.L17
	cmpl	%r8d, %esi
	jg	.L17
	movl	16(%rdi), %eax
	subl	20(%rdi), %eax
	cmpl	%eax, %esi
	jg	.L16
	movl	%eax, (%r9)
	movslq	20(%rdi), %rax
	addq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	%r8d, (%r9)
	movq	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$0, (%r9)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2078:
	.size	_ZN6icu_6720CheckedArrayByteSink15GetAppendBufferEiiPciPi, .-_ZN6icu_6720CheckedArrayByteSink15GetAppendBufferEiiPciPi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CheckedArrayByteSinkD0Ev
	.type	_ZN6icu_6720CheckedArrayByteSinkD0Ev, @function
_ZN6icu_6720CheckedArrayByteSinkD0Ev:
.LFB2075:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2075:
	.size	_ZN6icu_6720CheckedArrayByteSinkD0Ev, .-_ZN6icu_6720CheckedArrayByteSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CheckedArrayByteSink6AppendEPKci
	.type	_ZN6icu_6720CheckedArrayByteSink6AppendEPKci, @function
_ZN6icu_6720CheckedArrayByteSink6AppendEPKci:
.LFB2077:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483647, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	24(%rdi), %edx
	movq	%rdi, %rbx
	subl	%edx, %eax
	cmpl	%r12d, %eax
	jl	.L29
	addl	%r12d, %edx
	movl	16(%rdi), %eax
	movl	%edx, 24(%rdi)
	movl	20(%rdi), %edx
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	%ecx, %r12d
	jle	.L22
	movb	$1, 28(%rdi)
	testl	%ecx, %ecx
	jg	.L30
.L23:
	movl	%eax, 20(%rbx)
.L31:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	$2147483647, 24(%rdi)
	movb	$1, 28(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	%ecx, %r12d
	.p2align 4,,10
	.p2align 3
.L22:
	movslq	%edx, %rdi
	addq	8(%rbx), %rdi
	leal	(%rdx,%r12), %eax
	cmpq	%rsi, %rdi
	je	.L23
	movslq	%r12d, %rdx
	call	memcpy@PLT
	movl	20(%rbx), %eax
	addl	%r12d, %eax
	movl	%eax, 20(%rbx)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2077:
	.size	_ZN6icu_6720CheckedArrayByteSink6AppendEPKci, .-_ZN6icu_6720CheckedArrayByteSink6AppendEPKci
	.align 2
	.p2align 4
	.globl	_ZN6icu_678ByteSinkD2Ev
	.type	_ZN6icu_678ByteSinkD2Ev, @function
_ZN6icu_678ByteSinkD2Ev:
.LFB2064:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2064:
	.size	_ZN6icu_678ByteSinkD2Ev, .-_ZN6icu_678ByteSinkD2Ev
	.globl	_ZN6icu_678ByteSinkD1Ev
	.set	_ZN6icu_678ByteSinkD1Ev,_ZN6icu_678ByteSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_678ByteSinkD0Ev
	.type	_ZN6icu_678ByteSinkD0Ev, @function
_ZN6icu_678ByteSinkD0Ev:
.LFB2066:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2066:
	.size	_ZN6icu_678ByteSinkD0Ev, .-_ZN6icu_678ByteSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720CheckedArrayByteSinkC2EPci
	.type	_ZN6icu_6720CheckedArrayByteSinkC2EPci, @function
_ZN6icu_6720CheckedArrayByteSinkC2EPci:
.LFB2070:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720CheckedArrayByteSinkE(%rip), %rax
	testl	%edx, %edx
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$0, %eax
	cmovs	%eax, %edx
	movq	$0, 20(%rdi)
	movb	$0, 28(%rdi)
	movl	%edx, 16(%rdi)
	ret
	.cfi_endproc
.LFE2070:
	.size	_ZN6icu_6720CheckedArrayByteSinkC2EPci, .-_ZN6icu_6720CheckedArrayByteSinkC2EPci
	.globl	_ZN6icu_6720CheckedArrayByteSinkC1EPci
	.set	_ZN6icu_6720CheckedArrayByteSinkC1EPci,_ZN6icu_6720CheckedArrayByteSinkC2EPci
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_678ByteSinkE
	.section	.rodata._ZTSN6icu_678ByteSinkE,"aG",@progbits,_ZTSN6icu_678ByteSinkE,comdat
	.align 16
	.type	_ZTSN6icu_678ByteSinkE, @object
	.size	_ZTSN6icu_678ByteSinkE, 19
_ZTSN6icu_678ByteSinkE:
	.string	"N6icu_678ByteSinkE"
	.weak	_ZTIN6icu_678ByteSinkE
	.section	.data.rel.ro._ZTIN6icu_678ByteSinkE,"awG",@progbits,_ZTIN6icu_678ByteSinkE,comdat
	.align 8
	.type	_ZTIN6icu_678ByteSinkE, @object
	.size	_ZTIN6icu_678ByteSinkE, 24
_ZTIN6icu_678ByteSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_678ByteSinkE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6720CheckedArrayByteSinkE
	.section	.rodata._ZTSN6icu_6720CheckedArrayByteSinkE,"aG",@progbits,_ZTSN6icu_6720CheckedArrayByteSinkE,comdat
	.align 32
	.type	_ZTSN6icu_6720CheckedArrayByteSinkE, @object
	.size	_ZTSN6icu_6720CheckedArrayByteSinkE, 32
_ZTSN6icu_6720CheckedArrayByteSinkE:
	.string	"N6icu_6720CheckedArrayByteSinkE"
	.weak	_ZTIN6icu_6720CheckedArrayByteSinkE
	.section	.data.rel.ro._ZTIN6icu_6720CheckedArrayByteSinkE,"awG",@progbits,_ZTIN6icu_6720CheckedArrayByteSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6720CheckedArrayByteSinkE, @object
	.size	_ZTIN6icu_6720CheckedArrayByteSinkE, 24
_ZTIN6icu_6720CheckedArrayByteSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720CheckedArrayByteSinkE
	.quad	_ZTIN6icu_678ByteSinkE
	.weak	_ZTVN6icu_678ByteSinkE
	.section	.data.rel.ro._ZTVN6icu_678ByteSinkE,"awG",@progbits,_ZTVN6icu_678ByteSinkE,comdat
	.align 8
	.type	_ZTVN6icu_678ByteSinkE, @object
	.size	_ZTVN6icu_678ByteSinkE, 56
_ZTVN6icu_678ByteSinkE:
	.quad	0
	.quad	_ZTIN6icu_678ByteSinkE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_678ByteSink15GetAppendBufferEiiPciPi
	.quad	_ZN6icu_678ByteSink5FlushEv
	.weak	_ZTVN6icu_6720CheckedArrayByteSinkE
	.section	.data.rel.ro.local._ZTVN6icu_6720CheckedArrayByteSinkE,"awG",@progbits,_ZTVN6icu_6720CheckedArrayByteSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6720CheckedArrayByteSinkE, @object
	.size	_ZTVN6icu_6720CheckedArrayByteSinkE, 64
_ZTVN6icu_6720CheckedArrayByteSinkE:
	.quad	0
	.quad	_ZTIN6icu_6720CheckedArrayByteSinkE
	.quad	_ZN6icu_6720CheckedArrayByteSinkD1Ev
	.quad	_ZN6icu_6720CheckedArrayByteSinkD0Ev
	.quad	_ZN6icu_6720CheckedArrayByteSink6AppendEPKci
	.quad	_ZN6icu_6720CheckedArrayByteSink15GetAppendBufferEiiPciPi
	.quad	_ZN6icu_678ByteSink5FlushEv
	.quad	_ZN6icu_6720CheckedArrayByteSink5ResetEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
