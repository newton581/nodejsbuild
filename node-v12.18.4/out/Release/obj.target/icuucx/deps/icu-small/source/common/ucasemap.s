	.file	"ucasemap.cpp"
	.text
	.section	.rodata
.LC1:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	" 000000000000\02000"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_124utf8_caseContextIteratorEPva, @function
_ZN12_GLOBAL__N_124utf8_caseContextIteratorEPva:
.LFB3145:
	.cfi_startproc
	endbr64
	testb	%sil, %sil
	js	.L20
	jne	.L21
	cmpb	$0, 28(%rdi)
	movl	12(%rdi), %eax
	js	.L3
.L5:
	movl	16(%rdi), %esi
	cmpl	%eax, %esi
	jle	.L8
	movq	(%rdi), %r8
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 12(%rdi)
	movzbl	(%r8,%rdx), %r9d
	movl	%r9d, %edx
	testb	%r9b, %r9b
	js	.L22
.L1:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	24(%rdi), %eax
	movb	%sil, 28(%rdi)
	movl	%eax, 12(%rdi)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L20:
	movl	20(%rdi), %eax
	movb	%sil, 28(%rdi)
	movl	%eax, 12(%rdi)
.L3:
	movl	8(%rdi), %esi
	cmpl	%eax, %esi
	jge	.L8
	subl	$1, %eax
	movq	(%rdi), %r10
	movl	%eax, 12(%rdi)
	cltq
	movzbl	(%r10,%rax), %r9d
	testb	%r9b, %r9b
	jns	.L1
	leaq	12(%rdi), %rdx
	movl	$-1, %r8d
	movl	%r9d, %ecx
	movq	%r10, %rdi
	jmp	utf8_prevCharSafeBody_67@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$193, %r9d
	jg	.L23
.L8:
	movl	$-1, %r9d
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	cmpl	%ecx, %esi
	je	.L8
	cmpl	$223, %r9d
	jle	.L9
	cmpl	$239, %r9d
	jle	.L24
	subl	$240, %r9d
	cmpl	$4, %r9d
	jg	.L8
	movslq	%ecx, %rcx
	movzbl	(%r8,%rcx), %r10d
	leaq	.LC1(%rip), %rcx
	movq	%r10, %rdx
	shrq	$4, %rdx
	andl	$15, %edx
	movsbl	(%rcx,%rdx), %edx
	btl	%r9d, %edx
	jnc	.L8
	leal	2(%rax), %ecx
	movl	%ecx, 12(%rdi)
	cmpl	%ecx, %esi
	je	.L8
	movslq	%ecx, %rax
	movzbl	(%r8,%rax), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L8
	movl	%r10d, %eax
	sall	$6, %r9d
	andl	$63, %eax
	orl	%r9d, %eax
	.p2align 4,,10
	.p2align 3
.L11:
	addl	$1, %ecx
	movl	%ecx, 12(%rdi)
	cmpl	%ecx, %esi
	je	.L8
	sall	$6, %eax
	movzbl	%dl, %edx
	orl	%edx, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L23:
	movl	%r9d, %eax
	andl	$31, %eax
.L12:
	movslq	%ecx, %rdx
	movzbl	(%r8,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L8
	sall	$6, %eax
	movzbl	%dl, %r9d
	addl	$1, %ecx
	movl	%ecx, 12(%rdi)
	orl	%eax, %r9d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	movl	%r9d, %eax
	movslq	%ecx, %r9
	leaq	.LC0(%rip), %r10
	andl	$15, %edx
	movzbl	(%r8,%r9), %r9d
	movsbl	(%r10,%rdx), %r10d
	andl	$15, %eax
	movl	%r9d, %edx
	shrb	$5, %dl
	btl	%edx, %r10d
	jnc	.L8
	movl	%r9d, %edx
	andl	$63, %edx
	jmp	.L11
	.cfi_endproc
.LFE3145:
	.size	_ZN12_GLOBAL__N_124utf8_caseContextIteratorEPva, .-_ZN12_GLOBAL__N_124utf8_caseContextIteratorEPva
	.p2align 4
	.type	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode, @function
_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode:
.LFB3146:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZN6icu_679LatinCase15TO_LOWER_NORMALE(%rip), %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	32(%rbp), %r14
	movl	%esi, -84(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movl	%r9d, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -120(%rbp)
	cmpl	$1, %edi
	je	.L26
	testl	%edi, %edi
	js	.L27
	leal	-2(%rdi), %eax
	cmpl	$1, %eax
	leaq	_ZN6icu_679LatinCase14TO_LOWER_TR_LTE(%rip), %rax
	cmova	%rbx, %rax
	movq	%rax, -120(%rbp)
.L26:
	call	ucase_getTrie_67@PLT
	movq	%rax, -128(%rbp)
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L125
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	testb	$7, -84(%rbp)
	leaq	_ZN6icu_679LatinCase14TO_LOWER_TR_LTE(%rip), %rax
	cmove	-120(%rbp), %rax
	movq	%rax, -120(%rbp)
	call	ucase_getTrie_67@PLT
	movq	%rax, -128(%rbp)
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L25
.L125:
	cmpl	-72(%rbp), %r15d
	jge	.L25
	leaq	-64(%rbp), %rbx
	movl	%r15d, -68(%rbp)
	movq	%rbx, -80(%rbp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L128:
	movl	%ebx, %r15d
.L30:
	movq	-112(%rbp), %rcx
	movslq	%r15d, %rdx
	leal	1(%r15), %ebx
	movzbl	(%rcx,%rdx), %r10d
	testb	%r10b, %r10b
	js	.L32
	movq	-120(%rbp), %rcx
	movzbl	%r10b, %edx
	movzbl	(%rcx,%rdx), %r11d
	cmpb	$-128, %r11b
	je	.L34
	testb	%r11b, %r11b
	jne	.L127
	.p2align 4,,10
	.p2align 3
.L38:
	testl	%eax, %eax
	jg	.L25
.L69:
	cmpl	%ebx, -72(%rbp)
	jg	.L128
.L31:
	subl	-68(%rbp), %ebx
	testl	%ebx, %ebx
	jle	.L25
	movq	-96(%rbp), %r8
	movl	-84(%rbp), %ecx
	movl	%ebx, %esi
	movq	-104(%rbp), %rdx
	movslq	-68(%rbp), %rdi
	addq	-112(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L32:
	cmpb	$-30, %r10b
	ja	.L39
	leal	62(%r10), %edx
	cmpb	$3, %dl
	ja	.L40
	cmpl	%ebx, -72(%rbp)
	jle	.L40
	movslq	%ebx, %rdx
	movzbl	(%rcx,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L69
	subl	$192, %r10d
	leal	2(%r15), %ebx
	movl	%r10d, %ecx
	movzbl	%dl, %r10d
	sall	$6, %ecx
	orl	%ecx, %r10d
	movq	-120(%rbp), %rcx
	movslq	%r10d, %rdx
	movzbl	(%rcx,%rdx), %r11d
	cmpb	$-128, %r11b
	jne	.L129
	.p2align 4,,10
	.p2align 3
.L34:
	testl	%r12d, %r12d
	js	.L130
	movl	%r15d, 20(%r13)
	movq	-80(%rbp), %rcx
	movl	%r10d, %edi
	movl	%r12d, %r8d
	movl	%ebx, 24(%r13)
	movq	%r13, %rdx
	leaq	_ZN12_GLOBAL__N_124utf8_caseContextIteratorEPva(%rip), %rsi
	call	ucase_toFullLower_67@PLT
	movl	%eax, %r10d
.L60:
	movl	(%r14), %eax
	testl	%r10d, %r10d
	js	.L38
	movl	%r15d, %esi
	subl	-68(%rbp), %esi
	testl	%eax, %eax
	jg	.L62
	testl	%esi, %esi
	jle	.L62
	movq	-96(%rbp), %r8
	movl	-84(%rbp), %ecx
	movl	%r10d, -88(%rbp)
	movq	-104(%rbp), %rdx
	movslq	-68(%rbp), %rdi
	addq	-112(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	movl	-88(%rbp), %r10d
.L62:
	movl	%ebx, %edi
	subl	%r15d, %edi
	cmpl	$31, %r10d
	jg	.L63
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rcx
	movq	%r14, %r9
	movl	%r10d, %edx
	movq	-64(%rbp), %rsi
	call	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode@PLT
	movl	%ebx, -68(%rbp)
	movl	(%r14), %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L130:
	movl	-84(%rbp), %edx
	movq	-80(%rbp), %rsi
	movl	%r10d, %edi
	call	ucase_toFullFolding_67@PLT
	movl	%eax, %r10d
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L63:
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movl	%r10d, %esi
	call	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE@PLT
.L122:
	movl	%ebx, -68(%rbp)
	movl	(%r14), %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L40:
	cmpl	%ebx, -72(%rbp)
	je	.L31
	cmpb	$-33, %r10b
	ja	.L67
	cmpb	$-63, %r10b
	jbe	.L69
	movq	-112(%rbp), %rcx
	movslq	%ebx, %rdx
	movzbl	(%rcx,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L69
	movq	-128(%rbp), %rcx
	andl	$31, %r10d
	movzbl	%dl, %edx
	leal	2(%r15), %ebx
	sall	$6, %r10d
	movq	(%rcx), %rcx
	orl	%edx, %r10d
.L66:
	movl	%r10d, %edx
	sarl	$5, %edx
.L123:
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %esi
	movl	%r10d, %edx
	andl	$31, %edx
	leal	(%rdx,%rsi,4), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
.L52:
	movswl	(%rcx,%rdx), %edx
	testb	$8, %dl
	jne	.L34
	testb	$2, %dl
	je	.L69
	sarl	$7, %edx
	movl	%edx, %r11d
	je	.L69
	movl	-68(%rbp), %eax
	movl	%r15d, %esi
	subl	%eax, %esi
	testl	%esi, %esi
	jle	.L57
	movq	-96(%rbp), %r8
	movl	-84(%rbp), %ecx
	movl	%edx, -68(%rbp)
	movslq	%eax, %rdi
	movq	-104(%rbp), %rdx
	addq	-112(%rbp), %rdi
	movl	%r10d, -88(%rbp)
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	movl	-88(%rbp), %r10d
	movl	-68(%rbp), %r11d
.L57:
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movl	%ebx, %edi
	leal	(%r11,%r10), %esi
	subl	%r15d, %edi
	call	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE@PLT
	movl	%ebx, -68(%rbp)
	movl	(%r14), %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	leal	21(%r10), %edx
	cmpb	$1, %dl
	jbe	.L76
	cmpb	$-23, %r10b
	ja	.L44
.L76:
	leal	2(%r15), %edx
	cmpl	-72(%rbp), %edx
	jge	.L44
	movq	-112(%rbp), %rcx
	movslq	%ebx, %rdx
	cmpb	$-64, (%rcx,%rdx)
	jge	.L44
	cmpb	$-64, 1(%rcx,%rdx)
	jl	.L131
	.p2align 4,,10
	.p2align 3
.L44:
	cmpl	%ebx, -72(%rbp)
	je	.L31
	movzbl	%r10b, %edx
	cmpb	$-17, %r10b
	jbe	.L67
	subl	$240, %edx
	cmpl	$4, %edx
	jg	.L69
	movq	-112(%rbp), %rdi
	movslq	%ebx, %rcx
	leaq	.LC1(%rip), %r11
	movzbl	(%rdi,%rcx), %esi
	movq	%rsi, %rcx
	shrq	$4, %rcx
	andl	$15, %ecx
	movsbl	(%r11,%rcx), %ecx
	btl	%edx, %ecx
	jnc	.L69
	leal	2(%r15), %ebx
	cmpl	%ebx, -72(%rbp)
	je	.L31
	movslq	%ebx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L69
	sall	$6, %edx
	andl	$63, %esi
	orl	%esi, %edx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L127:
	movl	-68(%rbp), %eax
	subl	%eax, %r15d
	testl	%r15d, %r15d
	jg	.L132
.L36:
	movq	-104(%rbp), %rdi
	addl	%r11d, %r10d
	movq	-80(%rbp), %rsi
	movl	$1, %edx
	movb	%r10b, -64(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpq	$0, -96(%rbp)
	je	.L122
	movq	-96(%rbp), %rdi
	movl	$1, %edx
	movl	$1, %esi
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movl	%ebx, -68(%rbp)
	movl	(%r14), %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L67:
	movq	-112(%rbp), %rsi
	movslq	%ebx, %rcx
	movl	%r10d, %edx
	andl	$15, %r10d
	andl	$15, %edx
	movzbl	(%rsi,%rcx), %ecx
	leaq	.LC0(%rip), %rsi
	movsbl	(%rsi,%r10), %edi
	movl	%ecx, %esi
	andl	$63, %ecx
	shrb	$5, %sil
	btl	%esi, %edi
	jnc	.L69
.L48:
	sall	$6, %edx
	movzbl	%cl, %ecx
	movl	%edx, %r10d
	orl	%ecx, %r10d
	leal	1(%rbx), %ecx
	cmpl	%ecx, -72(%rbp)
	je	.L133
	movq	-112(%rbp), %rsi
	movslq	%ecx, %rdx
	movzbl	(%rsi,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L74
	movzbl	%dl, %edx
	sall	$6, %r10d
	addl	$2, %ebx
	orl	%edx, %r10d
	movq	-128(%rbp), %rdx
	movq	(%rdx), %rcx
	cmpl	$55295, %r10d
	jle	.L66
	cmpl	$65535, %r10d
	jg	.L53
	cmpl	$56320, %r10d
	movl	$0, %edx
	movl	$320, %esi
	cmovge	%edx, %esi
	movl	%r10d, %edx
	sarl	$5, %edx
	addl	%esi, %edx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L132:
	movq	-96(%rbp), %r8
	movl	-84(%rbp), %ecx
	movslq	%eax, %rdi
	movl	%r15d, %esi
	movq	-104(%rbp), %rdx
	addq	-112(%rbp), %rdi
	movb	%r11b, -88(%rbp)
	movb	%r10b, -68(%rbp)
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	movzbl	-88(%rbp), %r11d
	movzbl	-68(%rbp), %r10d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L53:
	cmpl	$1114111, %r10d
	jbe	.L55
	movl	24(%rdx), %edx
	movl	%edx, -88(%rbp)
	leal	128(%rdx), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	jmp	.L52
.L55:
	cmpl	44(%rdx), %r10d
	jl	.L56
	movslq	48(%rdx), %rdx
	addq	%rdx, %rdx
	jmp	.L52
.L129:
	testb	%r11b, %r11b
	je	.L69
	movl	-68(%rbp), %eax
	movl	%r15d, %esi
	subl	%eax, %esi
	testl	%esi, %esi
	jle	.L42
	movq	-96(%rbp), %r8
	movl	-84(%rbp), %ecx
	movslq	%eax, %rdi
	movl	%r10d, -88(%rbp)
	movq	-104(%rbp), %rdx
	addq	-112(%rbp), %rdi
	movb	%r11b, -68(%rbp)
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	movl	-88(%rbp), %r10d
	movzbl	-68(%rbp), %r11d
.L42:
	movsbl	%r11b, %edi
	movq	-104(%rbp), %rsi
	addl	%r10d, %edi
	call	_ZN6icu_6712ByteSinkUtil14appendTwoBytesEiRNS_8ByteSinkE@PLT
	cmpq	$0, -96(%rbp)
	je	.L122
	movq	-96(%rbp), %rdi
	movl	$2, %edx
	movl	$2, %esi
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	jmp	.L122
.L56:
	movl	%r10d, %edx
	movl	%r10d, %esi
	sarl	$11, %edx
	sarl	$5, %esi
	addl	$2080, %edx
	andl	$63, %esi
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	addl	%esi, %edx
	jmp	.L123
.L74:
	movl	%ecx, %ebx
	jmp	.L69
.L131:
	leal	3(%r15), %ebx
	jmp	.L69
.L133:
	movl	-72(%rbp), %ebx
	jmp	.L31
.L126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3146:
	.size	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode, .-_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	.p2align 4
	.type	_ZN12_GLOBAL__N_17toUpperEijPKhP12UCaseContextiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode, @function
_ZN12_GLOBAL__N_17toUpperEijPKhP12UCaseContextiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode:
.LFB3147:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -112(%rbp)
	leaq	_ZN6icu_679LatinCase11TO_UPPER_TRE(%rip), %rdx
	movl	%edi, -72(%rbp)
	movq	24(%rbp), %r13
	movl	%esi, -88(%rbp)
	movl	%r8d, -84(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, %edi
	leaq	_ZN6icu_679LatinCase15TO_UPPER_NORMALE(%rip), %rax
	cmove	%rdx, %rax
	movq	%rax, -120(%rbp)
	call	ucase_getTrie_67@PLT
	movl	0(%r13), %edx
	movq	%rax, -128(%rbp)
	testl	%edx, %edx
	jle	.L223
	.p2align 4,,10
	.p2align 3
.L134:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	testl	%ebx, %ebx
	jle	.L134
	xorl	%r15d, %r15d
	leaq	-64(%rbp), %rax
	movq	%r14, %rbx
	movl	$0, -68(%rbp)
	movq	%r13, %r14
	movq	%rax, -80(%rbp)
	movl	%r15d, %r13d
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L226:
	movl	%r15d, %r13d
.L138:
	movq	-112(%rbp), %rcx
	movslq	%r13d, %rax
	leal	1(%r13), %r15d
	movzbl	(%rcx,%rax), %r12d
	testb	%r12b, %r12b
	js	.L140
	movq	-120(%rbp), %rcx
	movzbl	%r12b, %eax
	movzbl	(%rcx,%rax), %eax
	cmpb	$-128, %al
	je	.L142
	testb	%al, %al
	jne	.L225
	.p2align 4,,10
	.p2align 3
.L146:
	testl	%edx, %edx
	jg	.L134
.L175:
	cmpl	%r15d, -84(%rbp)
	jg	.L226
.L218:
	movl	%r15d, %ebx
.L139:
	subl	-68(%rbp), %ebx
	movl	%ebx, %r10d
	testl	%ebx, %ebx
	jle	.L134
	movslq	-68(%rbp), %rbx
	movq	-112(%rbp), %rdi
	movl	%r10d, %esi
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %ecx
	movq	-104(%rbp), %rdx
	addq	%rbx, %rdi
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L140:
	cmpb	$-30, %r12b
	ja	.L147
	leal	62(%r12), %eax
	cmpb	$3, %al
	ja	.L148
	cmpl	%r15d, -84(%rbp)
	jle	.L148
	movslq	%r15d, %rax
	movzbl	(%rcx,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L175
	subl	$192, %r12d
	movzbl	%al, %eax
	movq	-120(%rbp), %rcx
	leal	2(%r13), %r15d
	sall	$6, %r12d
	orl	%eax, %r12d
	movslq	%r12d, %rax
	movsbl	(%rcx,%rax), %eax
	cmpb	$-128, %al
	jne	.L227
	.p2align 4,,10
	.p2align 3
.L142:
	movl	%r13d, 20(%rbx)
	movl	-72(%rbp), %r8d
	movl	%r12d, %edi
	movq	%rbx, %rdx
	movl	%r15d, 24(%rbx)
	movq	-80(%rbp), %rcx
	leaq	_ZN12_GLOBAL__N_124utf8_caseContextIteratorEPva(%rip), %rsi
	call	ucase_toFullUpper_67@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L170
	movl	(%r14), %eax
	movl	%r13d, %esi
	subl	-68(%rbp), %esi
	testl	%eax, %eax
	jg	.L168
	testl	%esi, %esi
	jle	.L168
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %ecx
	movq	-104(%rbp), %rdx
	movslq	-68(%rbp), %rdi
	addq	-112(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
.L168:
	movl	%r15d, %edi
	subl	%r13d, %edi
	cmpl	$31, %r12d
	jg	.L169
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %rcx
	movq	%r14, %r9
	movl	%r12d, %edx
	movq	-64(%rbp), %rsi
	call	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode@PLT
	movl	%r15d, -68(%rbp)
.L170:
	movl	(%r14), %edx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L169:
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movl	%r12d, %esi
	call	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE@PLT
	movl	%r15d, -68(%rbp)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L148:
	cmpl	%r15d, -84(%rbp)
	je	.L218
	cmpb	$-33, %r12b
	ja	.L173
	cmpb	$-63, %r12b
	jbe	.L175
	movq	-112(%rbp), %rcx
	movslq	%r15d, %rax
	movzbl	(%rcx,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L175
	andl	$31, %r12d
	movzbl	%al, %eax
	leal	2(%r13), %r15d
	sall	$6, %r12d
	orl	%eax, %r12d
	movq	-128(%rbp), %rax
	movq	(%rax), %rcx
.L172:
	movl	%r12d, %eax
	sarl	$5, %eax
.L221:
	cltq
	movzwl	(%rcx,%rax,2), %esi
	movl	%r12d, %eax
	andl	$31, %eax
	leal	(%rax,%rsi,4), %eax
	cltq
	addq	%rax, %rax
.L160:
	movswl	(%rcx,%rax), %eax
	testb	$8, %al
	jne	.L142
	movl	%eax, %ecx
	andl	$3, %ecx
	cmpw	$1, %cx
	jne	.L175
	sarl	$7, %eax
	je	.L175
	movl	-68(%rbp), %ecx
	movl	%r13d, %esi
	subl	%ecx, %esi
	testl	%esi, %esi
	jg	.L228
.L165:
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rcx
	movl	%r15d, %edi
	leal	(%rax,%r12), %esi
	subl	%r13d, %edi
	call	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE@PLT
	movl	%r15d, -68(%rbp)
	movl	(%r14), %edx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L147:
	leal	21(%r12), %eax
	cmpb	$1, %al
	jbe	.L180
	cmpb	$-23, %r12b
	ja	.L152
.L180:
	leal	2(%r13), %eax
	cmpl	-84(%rbp), %eax
	jge	.L152
	movq	-112(%rbp), %rcx
	movslq	%r15d, %rax
	cmpb	$-64, (%rcx,%rax)
	jge	.L152
	cmpb	$-64, 1(%rcx,%rax)
	jl	.L229
	.p2align 4,,10
	.p2align 3
.L152:
	cmpl	%r15d, -84(%rbp)
	je	.L218
	movzbl	%r12b, %eax
	cmpb	$-17, %r12b
	jbe	.L173
	subl	$240, %eax
	cmpl	$4, %eax
	jg	.L175
	movq	-112(%rbp), %rdi
	movslq	%r15d, %rcx
	leaq	.LC1(%rip), %r11
	movzbl	(%rdi,%rcx), %esi
	movq	%rsi, %rcx
	shrq	$4, %rcx
	andl	$15, %ecx
	movsbl	(%r11,%rcx), %ecx
	btl	%eax, %ecx
	jnc	.L175
	leal	2(%r13), %r15d
	cmpl	%r15d, -84(%rbp)
	je	.L218
	movslq	%r15d, %rcx
	movzbl	(%rdi,%rcx), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L175
	sall	$6, %eax
	andl	$63, %esi
	orl	%esi, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L225:
	movl	-68(%rbp), %ecx
	subl	%ecx, %r13d
	testl	%r13d, %r13d
	jg	.L230
.L144:
	movq	-104(%rbp), %rdi
	addl	%eax, %r12d
	movq	-80(%rbp), %rsi
	movl	$1, %edx
	movb	%r12b, -64(%rbp)
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpq	$0, -96(%rbp)
	je	.L220
	movq	-96(%rbp), %rdi
	movl	$1, %edx
	movl	$1, %esi
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movl	%r15d, -68(%rbp)
	movl	(%r14), %edx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-112(%rbp), %rsi
	movslq	%r15d, %rcx
	movl	%r12d, %eax
	andl	$15, %r12d
	andl	$15, %eax
	movzbl	(%rsi,%rcx), %ecx
	leaq	.LC0(%rip), %rsi
	movsbl	(%rsi,%r12), %edi
	movl	%ecx, %esi
	andl	$63, %ecx
	shrb	$5, %sil
	btl	%esi, %edi
	jnc	.L175
.L156:
	sall	$6, %eax
	movl	%eax, %r12d
	movzbl	%cl, %eax
	leal	1(%r15), %ecx
	orl	%eax, %r12d
	cmpl	%ecx, -84(%rbp)
	je	.L231
	movq	-112(%rbp), %rsi
	movslq	%ecx, %rax
	movzbl	(%rsi,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L178
	movzbl	%al, %eax
	sall	$6, %r12d
	addl	$2, %r15d
	orl	%eax, %r12d
	movq	-128(%rbp), %rax
	movq	(%rax), %rcx
	cmpl	$55295, %r12d
	jle	.L172
	cmpl	$65535, %r12d
	jg	.L161
	cmpl	$56320, %r12d
	movl	$0, %eax
	movl	$320, %esi
	cmovge	%eax, %esi
	movl	%r12d, %eax
	sarl	$5, %eax
	addl	%esi, %eax
	jmp	.L221
.L227:
	testb	%al, %al
	je	.L175
	movl	-68(%rbp), %ecx
	movl	%r13d, %esi
	subl	%ecx, %esi
	testl	%esi, %esi
	jle	.L150
	movslq	%ecx, %rdi
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %ecx
	movb	%al, -68(%rbp)
	movq	-104(%rbp), %rdx
	addq	-112(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	movsbl	-68(%rbp), %eax
.L150:
	movq	-104(%rbp), %rsi
	leal	(%rax,%r12), %edi
	call	_ZN6icu_6712ByteSinkUtil14appendTwoBytesEiRNS_8ByteSinkE@PLT
	cmpq	$0, -96(%rbp)
	je	.L220
	movq	-96(%rbp), %rdi
	movl	$2, %edx
	movl	$2, %esi
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	.p2align 4,,10
	.p2align 3
.L220:
	movl	%r15d, -68(%rbp)
	movl	(%r14), %edx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L230:
	movslq	%ecx, %rdi
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %ecx
	movl	%r13d, %esi
	movq	-104(%rbp), %rdx
	addq	-112(%rbp), %rdi
	movb	%al, -68(%rbp)
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	movzbl	-68(%rbp), %eax
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L161:
	cmpl	$1114111, %r12d
	jbe	.L163
	movl	24(%rax), %eax
	movl	%eax, -132(%rbp)
	subl	$-128, %eax
	cltq
	addq	%rax, %rax
	jmp	.L160
.L163:
	cmpl	44(%rax), %r12d
	jl	.L164
	movslq	48(%rax), %rax
	addq	%rax, %rax
	jmp	.L160
.L164:
	movl	%r12d, %eax
	movl	%r12d, %esi
	sarl	$11, %eax
	sarl	$5, %esi
	addl	$2080, %eax
	andl	$63, %esi
	cltq
	movzwl	(%rcx,%rax,2), %eax
	addl	%esi, %eax
	jmp	.L221
.L228:
	movslq	%ecx, %rdi
	movq	-96(%rbp), %r8
	movl	-88(%rbp), %ecx
	movl	%eax, -68(%rbp)
	movq	-104(%rbp), %rdx
	addq	-112(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	movl	-68(%rbp), %eax
	jmp	.L165
.L178:
	movl	%ecx, %r15d
	jmp	.L175
.L229:
	leal	3(%r13), %r15d
	jmp	.L175
.L231:
	movl	-84(%rbp), %ebx
	jmp	.L139
.L224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3147:
	.size	_ZN12_GLOBAL__N_17toUpperEijPKhP12UCaseContextiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode, .-_ZN12_GLOBAL__N_17toUpperEijPKhP12UCaseContextiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN8UCaseMapC2EPKcjP10UErrorCode
	.type	_ZN8UCaseMapC2EPKcjP10UErrorCode, @function
_ZN8UCaseMapC2EPKcjP10UErrorCode:
.LFB3131:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	movq	$0, (%rdi)
	movl	$0, 40(%rdi)
	movl	%edx, 44(%rdi)
	testl	%eax, %eax
	jg	.L247
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L234
	cmpb	$0, (%rsi)
	je	.L238
.L234:
	leaq	8(%rbx), %r14
	movl	$32, %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	uloc_getName_67@PLT
	movl	(%r12), %edx
	cmpl	$15, %edx
	je	.L239
	cmpl	$32, %eax
	je	.L239
.L235:
	testl	%edx, %edx
	jle	.L250
.L238:
	movb	$0, 8(%rbx)
	movl	$1, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$0, 40(%rbx)
	movq	%r14, %rdi
	call	ucase_getCaseLocale_67@PLT
	movl	%eax, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movq	%r12, %rcx
	movl	$32, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$0, (%r12)
	call	uloc_getLanguage_67@PLT
	cmpl	$32, %eax
	jne	.L251
	movl	$15, (%r12)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L251:
	movl	(%r12), %edx
	jmp	.L235
	.cfi_endproc
.LFE3131:
	.size	_ZN8UCaseMapC2EPKcjP10UErrorCode, .-_ZN8UCaseMapC2EPKcjP10UErrorCode
	.globl	_ZN8UCaseMapC1EPKcjP10UErrorCode
	.set	_ZN8UCaseMapC1EPKcjP10UErrorCode,_ZN8UCaseMapC2EPKcjP10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN8UCaseMapD2Ev
	.type	_ZN8UCaseMapD2Ev, @function
_ZN8UCaseMapD2Ev:
.LFB3134:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L252
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L252:
	ret
	.cfi_endproc
.LFE3134:
	.size	_ZN8UCaseMapD2Ev, .-_ZN8UCaseMapD2Ev
	.globl	_ZN8UCaseMapD1Ev
	.set	_ZN8UCaseMapD1Ev,_ZN8UCaseMapD2Ev
	.p2align 4
	.globl	ucasemap_open_67
	.type	ucasemap_open_67, @function
ucasemap_open_67:
.LFB3136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L254
	movq	%rdi, %r14
	movl	$48, %edi
	movl	%esi, %r13d
	movq	%rdx, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L256
	movl	(%rbx), %edx
	movq	$0, (%rax)
	movl	$0, 40(%rax)
	movl	%r13d, 44(%rax)
	testl	%edx, %edx
	jg	.L257
	testq	%r14, %r14
	je	.L258
	cmpb	$0, (%r14)
	je	.L263
.L258:
	leaq	8(%r12), %r13
	movq	%rbx, %rcx
	movl	$32, %edx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	uloc_getName_67@PLT
	movl	%eax, %r8d
	movl	(%rbx), %eax
	cmpl	$32, %r8d
	je	.L266
	cmpl	$15, %eax
	je	.L266
.L260:
	testl	%eax, %eax
	jg	.L263
	movl	$0, 40(%r12)
	movq	%r13, %rdi
	call	ucase_getCaseLocale_67@PLT
	movl	%eax, 40(%r12)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L266:
	movl	$0, (%rbx)
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$32, %edx
	call	uloc_getLanguage_67@PLT
	cmpl	$32, %eax
	jne	.L274
	movl	$15, (%rbx)
.L263:
	movb	$0, 8(%r12)
	movl	$1, 40(%r12)
.L259:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L275
.L254:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L275:
	.cfi_restore_state
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L257
	movq	(%rdi), %rax
	call	*8(%rax)
.L257:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movl	(%rbx), %eax
	jmp	.L260
.L256:
	movl	$7, (%rbx)
	jmp	.L254
	.cfi_endproc
.LFE3136:
	.size	ucasemap_open_67, .-ucasemap_open_67
	.p2align 4
	.globl	ucasemap_close_67
	.type	ucasemap_close_67, @function
ucasemap_close_67:
.LFB3137:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L276
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L278
	movq	(%rdi), %rax
	call	*8(%rax)
.L278:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L276:
	ret
	.cfi_endproc
.LFE3137:
	.size	ucasemap_close_67, .-ucasemap_close_67
	.p2align 4
	.globl	ucasemap_getLocale_67
	.type	ucasemap_getLocale_67, @function
ucasemap_getLocale_67:
.LFB3138:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE3138:
	.size	ucasemap_getLocale_67, .-ucasemap_getLocale_67
	.p2align 4
	.globl	ucasemap_getOptions_67
	.type	ucasemap_getOptions_67, @function
ucasemap_getOptions_67:
.LFB3139:
	.cfi_startproc
	endbr64
	movl	44(%rdi), %eax
	ret
	.cfi_endproc
.LFE3139:
	.size	ucasemap_getOptions_67, .-ucasemap_getOptions_67
	.p2align 4
	.globl	ucasemap_setLocale_67
	.type	ucasemap_setLocale_67, @function
ucasemap_setLocale_67:
.LFB3140:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L302
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	je	.L289
	cmpb	$0, (%rsi)
	je	.L293
.L289:
	leaq	8(%r13), %r14
	movl	$32, %edx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	uloc_getName_67@PLT
	movl	(%rbx), %edx
	cmpl	$15, %edx
	je	.L294
	cmpl	$32, %eax
	je	.L294
.L290:
	testl	%edx, %edx
	jle	.L305
.L293:
	movb	$0, 8(%r13)
	movl	$1, 40(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$0, 40(%r13)
	movq	%r14, %rdi
	call	ucase_getCaseLocale_67@PLT
	movl	%eax, 40(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	movl	$0, (%rbx)
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$32, %edx
	call	uloc_getLanguage_67@PLT
	cmpl	$32, %eax
	jne	.L306
	movl	$15, (%rbx)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L306:
	movl	(%rbx), %edx
	jmp	.L290
	.cfi_endproc
.LFE3140:
	.size	ucasemap_setLocale_67, .-ucasemap_setLocale_67
	.p2align 4
	.globl	ucasemap_setOptions_67
	.type	ucasemap_setOptions_67, @function
ucasemap_setOptions_67:
.LFB3141:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L307
	movl	%esi, 44(%rdi)
.L307:
	ret
	.cfi_endproc
.LFE3141:
	.size	ucasemap_setOptions_67, .-ucasemap_setOptions_67
	.p2align 4
	.globl	ucasemap_internalUTF8ToTitle_67
	.type	ucasemap_internalUTF8ToTitle_67, @function
ucasemap_internalUTF8ToTitle_67:
.LFB3148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rcx, -128(%rbp)
	movl	%edi, -164(%rbp)
	movq	%rax, -184(%rbp)
	movq	24(%rbp), %rax
	movl	%esi, -140(%rbp)
	movq	%r9, -192(%rbp)
	movl	(%rax), %r11d
	movq	%rax, -152(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testl	%r11d, %r11d
	jg	.L309
	movl	%esi, %eax
	andl	$1536, %eax
	cmpl	$1536, %eax
	je	.L401
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -72(%rbp)
	movl	%r8d, %r13d
	movups	%xmm0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movl	%r8d, -80(%rbp)
	testl	%r8d, %r8d
	jle	.L309
	movq	(%rdx), %rax
	movq	%rdx, %r12
	movq	%rdx, %rdi
	xorl	%r15d, %r15d
	call	*80(%rax)
	movq	%r12, -136(%rbp)
	movl	%eax, %ebx
	movl	-140(%rbp), %eax
	movl	%eax, %esi
	movl	%eax, %edx
	andl	$512, %esi
	andl	$1024, %edx
	movl	%esi, -120(%rbp)
	movl	%eax, %esi
	andl	$256, %eax
	movl	%eax, -156(%rbp)
	andl	$16384, %esi
	movl	%r15d, %eax
	movl	%ebx, %r15d
	movl	%edx, -144(%rbp)
	movl	%eax, %ebx
	movl	%esi, -160(%rbp)
	cmpl	$-1, %r15d
	je	.L316
.L402:
	cmpl	%r13d, %r15d
	jg	.L316
	cmpl	%ebx, %r15d
	jg	.L350
	cmpl	%r13d, %r15d
	je	.L309
.L314:
	movq	-136(%rbp), %rdi
	movl	%r15d, %ebx
	movq	(%rdi), %rax
	call	*104(%rax)
	movl	%eax, %r15d
	cmpl	$-1, %r15d
	jne	.L402
.L316:
	cmpl	%ebx, %r13d
	jle	.L309
	movl	%r13d, %r15d
.L350:
	movq	-128(%rbp), %rsi
	movslq	%ebx, %rax
	leal	1(%rbx), %r14d
	addq	%rsi, %rax
	movzbl	(%rax), %r12d
	movq	%rax, -176(%rbp)
	movl	%r12d, %eax
	testb	%r12b, %r12b
	js	.L403
.L318:
	movl	-120(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L404
.L324:
	cmpl	%r14d, %ebx
	jge	.L400
	movl	%r14d, %r10d
	subl	%ebx, %r10d
	cmpl	$-1, %r12d
	jne	.L405
	movq	-152(%rbp), %rax
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L309
	movq	-184(%rbp), %r8
	movl	-140(%rbp), %ecx
	movslq	%ebx, %rdi
	movl	%r10d, %esi
	movq	-192(%rbp), %rdx
	addq	-128(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
.L343:
	leal	1(%rbx), %eax
	cmpl	%r15d, %eax
	jge	.L344
	cmpl	$5, -164(%rbp)
	je	.L406
.L344:
	cmpl	%r15d, %r14d
	jge	.L400
	movl	-156(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L407
	movq	-152(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L309
	movl	%r15d, %esi
	movl	-140(%rbp), %ecx
	movslq	%r14d, %rdi
	movq	-184(%rbp), %r8
	movq	-192(%rbp), %rdx
	subl	%r14d, %esi
	addq	-128(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	.p2align 4,,10
	.p2align 3
.L400:
	cmpl	%r15d, %r13d
	jg	.L314
	.p2align 4,,10
	.p2align 3
.L309:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L408
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	cmpl	%r15d, %r14d
	je	.L352
	cmpl	$223, %r12d
	jle	.L319
	cmpl	$239, %r12d
	jg	.L320
	movslq	%r14d, %rdx
	andl	$15, %eax
	movl	%r12d, %ecx
	movl	$-1, %r12d
	movzbl	(%rsi,%rdx), %edx
	leaq	.LC0(%rip), %rsi
	andl	$15, %ecx
	movsbl	(%rsi,%rax), %esi
	movl	%edx, %eax
	andl	$63, %edx
	shrb	$5, %al
	btl	%eax, %esi
	jnc	.L318
.L321:
	addl	$1, %r14d
	movl	$-1, %r12d
	cmpl	%r15d, %r14d
	je	.L318
	sall	$6, %ecx
	movzbl	%dl, %eax
	orl	%ecx, %eax
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L404:
	movl	%ebx, -116(%rbp)
	movl	%ebx, -168(%rbp)
	movq	-128(%rbp), %rbx
	movl	%r13d, -196(%rbp)
	movl	-144(%rbp), %r13d
	.p2align 4,,10
	.p2align 3
.L323:
	movl	%r12d, %edi
	testl	%r13d, %r13d
	jne	.L396
	call	u_charType_67@PLT
	movl	$251792942, %edx
	btl	%eax, %edx
	movsbl	%al, %ecx
	jnc	.L409
.L327:
	movl	-168(%rbp), %ebx
	movl	-196(%rbp), %r13d
	cmpl	%ebx, -116(%rbp)
	jg	.L349
	movl	-116(%rbp), %ebx
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L329:
	movl	-168(%rbp), %ebx
	movl	%r14d, -116(%rbp)
	movl	-196(%rbp), %r13d
	cmpl	%ebx, %r14d
	jle	.L400
.L349:
	movq	-152(%rbp), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jg	.L309
	movl	-116(%rbp), %esi
	movq	-184(%rbp), %r8
	movl	-140(%rbp), %ecx
	movq	-192(%rbp), %rdx
	movq	-176(%rbp), %rdi
	subl	%ebx, %esi
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	movl	-116(%rbp), %ebx
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L409:
	cmpl	$4, %ecx
	jne	.L328
	movl	%r12d, %edi
	.p2align 4,,10
	.p2align 3
.L396:
	call	ucase_getType_67@PLT
	testl	%eax, %eax
	sete	%al
	testb	%al, %al
	je	.L327
.L328:
	cmpl	%r15d, %r14d
	je	.L329
	movslq	%r14d, %rax
	leal	1(%r14), %edi
	movzbl	(%rbx,%rax), %r12d
	movl	%r12d, %eax
	testb	%r12b, %r12b
	js	.L410
.L330:
	movl	%r14d, -116(%rbp)
	movl	%edi, %r14d
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L410:
	cmpl	%r15d, %edi
	je	.L362
	cmpl	$223, %r12d
	jle	.L331
	cmpl	$239, %r12d
	jg	.L332
	movslq	%edi, %rcx
	andl	$15, %eax
	leaq	.LC0(%rip), %rdx
	movl	%r12d, %r8d
	movzbl	(%rbx,%rcx), %ecx
	movsbl	(%rdx,%rax), %r9d
	andl	$15, %r8d
	movl	$-1, %r12d
	movl	%ecx, %eax
	andl	$63, %ecx
	shrb	$5, %al
	btl	%eax, %r9d
	jnc	.L330
.L333:
	addl	$1, %edi
	movl	$-1, %r12d
	cmpl	%r15d, %edi
	je	.L330
	sall	$6, %r8d
	movzbl	%cl, %eax
	orl	%r8d, %eax
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L362:
	movl	%r15d, %edi
	movl	$-1, %r12d
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L331:
	cmpl	$193, %r12d
	jle	.L369
	andl	$31, %eax
.L334:
	movslq	%edi, %rcx
	movl	$-1, %r12d
	movzbl	(%rbx,%rcx), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L330
	sall	$6, %eax
	movzbl	%cl, %esi
	addl	$1, %edi
	orl	%esi, %eax
	movl	%eax, %r12d
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L405:
	movl	%r12d, %edi
	leaq	-104(%rbp), %rcx
	leaq	-96(%rbp), %rdx
	movl	%r10d, -116(%rbp)
	movl	-164(%rbp), %r8d
	leaq	_ZN12_GLOBAL__N_124utf8_caseContextIteratorEPva(%rip), %rsi
	movl	%ebx, -76(%rbp)
	movl	%r14d, -72(%rbp)
	call	ucase_toFullTitle_67@PLT
	movl	-116(%rbp), %r10d
	testl	%eax, %eax
	movl	%eax, %r12d
	js	.L411
	cmpl	$31, %eax
	jg	.L341
	movq	-152(%rbp), %r9
	movq	-104(%rbp), %rsi
	movl	%eax, %edx
	movl	%r10d, %edi
	movq	-184(%rbp), %r8
	movq	-192(%rbp), %rcx
	call	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode@PLT
	testb	%al, %al
	je	.L309
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L332:
	leal	-240(%r12), %eax
	movl	$-1, %r12d
	cmpl	$4, %eax
	jg	.L330
	movslq	%edi, %rcx
	leaq	.LC1(%rip), %rdx
	movzbl	(%rbx,%rcx), %r8d
	movq	%r8, %rcx
	shrq	$4, %rcx
	andl	$15, %ecx
	movsbl	(%rdx,%rcx), %ecx
	btl	%eax, %ecx
	jnc	.L330
	leal	2(%r14), %edi
	cmpl	%r15d, %edi
	je	.L366
	movslq	%edi, %rcx
	movzbl	(%rbx,%rcx), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L330
	sall	$6, %eax
	andl	$63, %r8d
	orl	%eax, %r8d
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L369:
	movl	$-1, %r12d
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L352:
	movl	%r15d, %r14d
	movl	$-1, %r12d
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L319:
	cmpl	$193, %r12d
	jle	.L359
	andl	$31, %eax
.L322:
	movq	-128(%rbp), %rcx
	movslq	%r14d, %rdx
	movl	$-1, %r12d
	movzbl	(%rcx,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L318
	sall	$6, %eax
	movzbl	%dl, %r9d
	addl	$1, %r14d
	orl	%r9d, %eax
	movl	%eax, %r12d
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L411:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L338
	movl	%r10d, %esi
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movl	-116(%rbp), %r10d
.L338:
	movl	-160(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L343
	movl	%r12d, %esi
	movq	-192(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	%r10d, %edi
	notl	%esi
	call	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L401:
	movq	-152(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L359:
	movl	$-1, %r12d
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L320:
	leal	-240(%r12), %eax
	movl	$-1, %r12d
	cmpl	$4, %eax
	jg	.L318
	movslq	%r14d, %rdx
	leaq	.LC1(%rip), %rdi
	movzbl	(%rsi,%rdx), %ecx
	movq	%rcx, %rdx
	shrq	$4, %rdx
	andl	$15, %edx
	movsbl	(%rdi,%rdx), %edx
	btl	%eax, %edx
	jnc	.L318
	leal	2(%rbx), %r14d
	cmpl	%r15d, %r14d
	je	.L356
	movslq	%r14d, %rdx
	movzbl	(%rsi,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L318
	sall	$6, %eax
	andl	$63, %ecx
	orl	%eax, %ecx
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L407:
	subq	$8, %rsp
	movq	-128(%rbp), %rdx
	leaq	-96(%rbp), %rcx
	movl	%r15d, %r9d
	movq	-152(%rbp), %rbx
	movl	-140(%rbp), %esi
	movl	%r14d, %r8d
	movl	-164(%rbp), %edi
	pushq	%rbx
	pushq	-184(%rbp)
	pushq	-192(%rbp)
	call	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	movl	(%rbx), %edx
	addq	$32, %rsp
	testl	%edx, %edx
	jle	.L400
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L406:
	movq	-128(%rbp), %rcx
	movslq	%ebx, %r11
	movzbl	(%rcx,%r11), %eax
	movb	%al, -116(%rbp)
	andl	$-33, %eax
	cmpb	$73, %al
	jne	.L344
	leaq	1(%rcx,%r11), %rdi
	movzbl	(%rdi), %eax
	cmpb	$106, %al
	je	.L412
	cmpb	$74, %al
	jne	.L344
	movq	-152(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L309
	movq	-184(%rbp), %r8
	movl	-140(%rbp), %ecx
	movl	$1, %esi
	addl	$1, %r14d
	movq	-192(%rbp), %rdx
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L341:
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %rdx
	movl	%eax, %esi
	movl	%r10d, %edi
	call	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE@PLT
	jmp	.L343
.L366:
	movl	%r15d, %edi
	jmp	.L330
.L412:
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %rdx
	movl	$74, %esi
	movl	$1, %edi
	addl	$1, %r14d
	call	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE@PLT
	jmp	.L344
.L356:
	movl	%r15d, %r14d
	jmp	.L318
.L408:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3148:
	.size	ucasemap_internalUTF8ToTitle_67, .-ucasemap_internalUTF8ToTitle_67
	.p2align 4
	.globl	_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKhii
	.type	_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKhii, @function
_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKhii:
.LFB3149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC1(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC0(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L422:
	movl	%eax, %ebx
.L415:
	movl	%r8d, %edi
	call	ucase_getTypeOrIgnorable_67@PLT
	testb	$4, %al
	je	.L434
.L420:
	cmpl	%r12d, %ebx
	jge	.L421
	movslq	%ebx, %rdx
	leal	1(%rbx), %eax
	movzbl	0(%r13,%rdx), %r8d
	movl	%r8d, %edi
	testb	%r8b, %r8b
	jns	.L422
	cmpl	%eax, %r12d
	je	.L423
	cmpl	$223, %r8d
	jle	.L416
	cmpl	$239, %r8d
	jg	.L417
	movslq	%eax, %rdx
	andl	$15, %edi
	movl	%r8d, %ecx
	movzbl	0(%r13,%rdx), %edx
	movsbl	(%r14,%rdi), %edi
	andl	$15, %ecx
	movl	%edx, %esi
	andl	$63, %edx
	shrb	$5, %sil
	btl	%esi, %edi
	jnc	.L430
.L418:
	leal	1(%rax), %ebx
	movl	$-1, %r8d
	cmpl	%ebx, %r12d
	je	.L415
	sall	$6, %ecx
	movzbl	%dl, %edi
	orl	%ecx, %edi
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L417:
	leal	-240(%r8), %ecx
	cmpl	$4, %ecx
	jg	.L430
	movslq	%eax, %rdx
	movzbl	0(%r13,%rdx), %esi
	movq	%rsi, %rdx
	shrq	$4, %rdx
	andl	$15, %edx
	movsbl	(%r15,%rdx), %edx
	btl	%ecx, %edx
	jc	.L435
	.p2align 4,,10
	.p2align 3
.L430:
	movl	$-1, %r8d
	movl	%eax, %ebx
	movl	%r8d, %edi
	call	ucase_getTypeOrIgnorable_67@PLT
	testb	$4, %al
	jne	.L420
.L434:
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	.cfi_restore_state
	movl	%r12d, %ebx
	movl	$-1, %r8d
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L416:
	cmpl	$193, %r8d
	jle	.L430
	andl	$31, %edi
	movl	%eax, %ebx
.L419:
	movslq	%ebx, %rax
	movl	$-1, %r8d
	movzbl	0(%r13,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L415
	sall	$6, %edi
	movzbl	%al, %r8d
	addl	$1, %ebx
	orl	%edi, %r8d
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L421:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L435:
	.cfi_restore_state
	addl	$2, %ebx
	movl	$-1, %r8d
	cmpl	%ebx, %r12d
	je	.L415
	movslq	%ebx, %rax
	movzbl	0(%r13,%rax), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L415
	sall	$6, %ecx
	andl	$63, %esi
	movl	%ebx, %eax
	orl	%esi, %ecx
	jmp	.L418
	.cfi_endproc
.LFE3149:
	.size	_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKhii, .-_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKhii
	.section	.rodata.str1.1
.LC2:
	.string	"\314\210"
.LC3:
	.string	"\314\201"
.LC4:
	.string	"\316\231"
	.text
	.p2align 4
	.globl	_ZN6icu_6710GreekUpper7toUpperEjPKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZN6icu_6710GreekUpper7toUpperEjPKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZN6icu_6710GreekUpper7toUpperEjPKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB3150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, -140(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r9, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L436
	andl	$16384, %edi
	movl	$0, -76(%rbp)
	movl	%edx, %r13d
	movl	%edi, -120(%rbp)
	movl	$0, -84(%rbp)
	.p2align 4,,10
	.p2align 3
.L438:
	movslq	-76(%rbp), %rax
	movq	-72(%rbp), %rsi
	leal	1(%rax), %ebx
	movq	%rax, %rcx
	movq	%rax, -112(%rbp)
	movq	%rsi, %rax
	addq	%rcx, %rax
	movl	%ebx, -88(%rbp)
	movzbl	(%rax), %r12d
	movq	%rax, -96(%rbp)
	movl	%r12d, %eax
	testb	%r12b, %r12b
	js	.L568
.L439:
	movl	%r12d, %edi
	call	ucase_getTypeOrIgnorable_67@PLT
	testb	$4, %al
	je	.L444
	movl	-84(%rbp), %eax
	movl	%r12d, %edi
	andl	$1, %eax
	movl	%eax, -80(%rbp)
	call	_ZN6icu_6710GreekUpper13getLetterDataEi@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L569
.L446:
	movl	%ebx, %r10d
	subl	-76(%rbp), %r10d
	cmpl	$-1, %r12d
	je	.L476
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%r12d, %edi
	movl	$4, %r8d
	leaq	-64(%rbp), %rcx
	movl	%r10d, -76(%rbp)
	call	ucase_toFullUpper_67@PLT
	movl	-76(%rbp), %r10d
	testl	%eax, %eax
	movl	%eax, %r12d
	js	.L570
	cmpl	$31, %eax
	jg	.L481
	movq	-136(%rbp), %r9
	movq	-104(%rbp), %r8
	movl	%eax, %edx
	movl	%r10d, %edi
	movq	-128(%rbp), %rcx
	movq	-64(%rbp), %rsi
	call	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode@PLT
	testb	%al, %al
	jne	.L516
.L436:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L571
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	movq	-104(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movl	%eax, %esi
	movl	%r10d, %edi
	call	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE@PLT
.L516:
	movl	-80(%rbp), %eax
	movl	%ebx, -76(%rbp)
	movl	%eax, -84(%rbp)
.L472:
	cmpl	%ebx, %r13d
	jg	.L438
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L444:
	testl	%eax, %eax
	movl	%r12d, %edi
	setne	%al
	movzbl	%al, %eax
	movl	%eax, -80(%rbp)
	call	_ZN6icu_6710GreekUpper13getLetterDataEi@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L446
.L569:
	andl	$1023, %eax
	movl	%eax, -116(%rbp)
	testl	$4096, %r14d
	je	.L447
	testb	$2, -84(%rbp)
	jne	.L572
.L447:
	movl	%r14d, %r12d
	shrl	$13, %r12d
	andl	$1, %r12d
	cmpl	%r13d, %ebx
	jl	.L564
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L451:
	movl	%r8d, %edi
	call	_ZN6icu_6710GreekUpper16getDiacriticDataEi@PLT
	testl	%eax, %eax
	je	.L565
.L573:
	orl	%eax, %r14d
	andl	$8192, %eax
	cmpl	$1, %eax
	sbbl	$-1, %r12d
	cmpl	%r13d, %ebx
	jge	.L449
.L564:
	movl	%ebx, %r15d
	movq	-72(%rbp), %rsi
	leal	1(%rbx), %ebx
	movslq	%r15d, %rax
	movzbl	(%rsi,%rax), %r8d
	movl	%r8d, %edi
	testb	%r8b, %r8b
	jns	.L451
	cmpl	%ebx, %r13d
	je	.L503
	cmpl	$223, %r8d
	jle	.L452
	cmpl	$239, %r8d
	jg	.L453
	movslq	%ebx, %rax
	andl	$15, %edi
	leaq	.LC0(%rip), %rdx
	movl	%r8d, %ecx
	movzbl	(%rsi,%rax), %eax
	movsbl	(%rdx,%rdi), %edi
	andl	$15, %ecx
	movl	$-1, %r8d
	movl	%eax, %esi
	andl	$63, %eax
	shrb	$5, %sil
	btl	%esi, %edi
	jnc	.L451
.L454:
	addl	$1, %ebx
	movl	$-1, %r8d
	cmpl	%ebx, %r13d
	je	.L451
	sall	$6, %ecx
	movzbl	%al, %edi
	orl	%ecx, %edi
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L568:
	cmpl	%ebx, %r13d
	je	.L486
	cmpl	$223, %r12d
	jle	.L440
	cmpl	$239, %r12d
	jg	.L441
	movslq	%ebx, %rcx
	andl	$15, %eax
	movl	%r12d, %edx
	movzbl	(%rsi,%rcx), %ecx
	leaq	.LC0(%rip), %rsi
	andl	$15, %edx
	movsbl	(%rsi,%rax), %esi
	movl	%ecx, %eax
	andl	$63, %ecx
	shrb	$5, %al
	btl	%eax, %esi
	jnc	.L489
.L442:
	addl	$1, %ebx
	movl	$-1, %r12d
	cmpl	%ebx, %r13d
	je	.L439
	sall	$6, %edx
	movzbl	%cl, %eax
	orl	%edx, %eax
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L476:
	movq	-136(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L436
	movl	-80(%rbp), %eax
	movl	%ebx, -76(%rbp)
	movl	%eax, -84(%rbp)
	testl	%r10d, %r10d
	jle	.L472
	movq	-104(%rbp), %r8
	movl	-140(%rbp), %ecx
	movl	%r10d, %esi
	movq	-128(%rbp), %rdx
	movq	-96(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE@PLT
	movl	%ebx, -76(%rbp)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L503:
	movl	$-1, %r8d
	movl	%r8d, %edi
	call	_ZN6icu_6710GreekUpper16getDiacriticDataEi@PLT
	testl	%eax, %eax
	jne	.L573
.L565:
	movl	%r15d, %ebx
.L449:
	movl	-80(%rbp), %edx
	movl	%r14d, %ecx
	andl	$53248, %ecx
	movl	%edx, %eax
	orl	$2, %eax
	cmpl	$20480, %ecx
	cmovne	%edx, %eax
	cmpl	$919, -116(%rbp)
	movl	%eax, -80(%rbp)
	je	.L574
	testl	$32768, %r14d
	je	.L567
	movl	-116(%rbp), %eax
	cmpl	$921, %eax
	je	.L506
	cmpl	$933, %eax
	jne	.L567
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	cmpq	$0, -104(%rbp)
	movl	$939, -116(%rbp)
	jne	.L462
	.p2align 4,,10
	.p2align 3
.L578:
	movl	-120(%rbp), %edi
	testl	%edi, %edi
	je	.L463
	cmpl	-88(%rbp), %ebx
	jg	.L575
.L508:
	movl	$1, %esi
	movl	$1, %edi
.L464:
	movl	-76(%rbp), %r11d
	movl	$2, %eax
	leal	2(%r11), %edx
	testl	%ecx, %ecx
	je	.L465
	leal	3(%r11), %eax
	movl	$1, %esi
	cmpl	%eax, %ebx
	jg	.L576
.L466:
	movl	-76(%rbp), %eax
	leal	4(%rax), %edx
	movl	$4, %eax
.L465:
	testb	%r8b, %r8b
	je	.L467
	leal	1(%rdx), %eax
	cmpl	%eax, %ebx
	jg	.L577
.L514:
	movl	%r8d, %esi
.L468:
	leal	2(%rdx), %eax
	subl	-76(%rbp), %eax
.L467:
	leal	(%rax,%r12,2), %edx
	movl	%ebx, %r9d
	subl	-76(%rbp), %r9d
	movq	-104(%rbp), %rdi
	cmpl	%edx, %r9d
	setne	%al
	orb	%sil, %al
	je	.L469
	testq	%rdi, %rdi
	je	.L463
	movl	%r9d, %esi
	movl	%ecx, -84(%rbp)
	movb	%r8b, -76(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movzbl	-76(%rbp), %r8d
	movl	-84(%rbp), %ecx
.L463:
	movq	-128(%rbp), %r15
	movl	-116(%rbp), %edi
	movl	%ecx, -84(%rbp)
	movb	%r8b, -76(%rbp)
	movq	%r15, %rsi
	call	_ZN6icu_6712ByteSinkUtil14appendTwoBytesEiRNS_8ByteSinkE@PLT
	movl	-84(%rbp), %ecx
	movzbl	-76(%rbp), %r8d
	testl	%ecx, %ecx
	je	.L473
	movq	(%r15), %rax
	movq	%r15, %rdi
	movl	$2, %edx
	leaq	.LC2(%rip), %rsi
	call	*16(%rax)
	movzbl	-76(%rbp), %r8d
.L473:
	testb	%r8b, %r8b
	je	.L474
	movq	-128(%rbp), %rdi
	movl	$2, %edx
	leaq	.LC3(%rip), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
.L474:
	testl	%r12d, %r12d
	je	.L516
	movq	-128(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L475:
	movq	(%r14), %rax
	movl	$2, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	subl	$1, %r12d
	jne	.L475
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L452:
	cmpl	$193, %r8d
	jle	.L503
	andl	$31, %edi
.L455:
	movq	-72(%rbp), %rdx
	movslq	%ebx, %rax
	movl	$-1, %r8d
	movzbl	(%rdx,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L451
	sall	$6, %edi
	movzbl	%al, %r8d
	addl	$1, %ebx
	orl	%edi, %r8d
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L567:
	movl	%r14d, %ecx
	xorl	%r8d, %r8d
	andl	$98304, %ecx
.L460:
	cmpq	$0, -104(%rbp)
	je	.L578
.L462:
	cmpl	-88(%rbp), %ebx
	jle	.L508
.L575:
	movl	-116(%rbp), %eax
	movq	-96(%rbp), %rdx
	movl	$1, %esi
	movl	$1, %edi
	sarl	$6, %eax
	orl	$-64, %eax
	cmpb	%al, (%rdx)
	jne	.L464
	movzbl	-116(%rbp), %eax
	movq	-112(%rbp), %rsi
	movq	-72(%rbp), %rdx
	andl	$63, %eax
	orl	$-128, %eax
	cmpb	%al, 1(%rdx,%rsi)
	setne	%dil
	testl	%r12d, %r12d
	setne	%al
	orl	%eax, %edi
	movl	%edi, %esi
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L453:
	leal	-240(%r8), %ecx
	movl	$-1, %r8d
	cmpl	$4, %ecx
	jg	.L451
	movslq	%ebx, %rax
	movq	%rsi, %rdx
	leaq	.LC1(%rip), %rdi
	movzbl	(%rsi,%rax), %esi
	movq	%rsi, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%rdi,%rax), %eax
	btl	%ecx, %eax
	jnc	.L451
	leal	2(%r15), %ebx
	cmpl	%ebx, %r13d
	je	.L451
	movslq	%ebx, %rax
	movzbl	(%rdx,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L451
	sall	$6, %ecx
	andl	$63, %esi
	orl	%esi, %ecx
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L570:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L478
	movl	%r10d, %esi
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movl	-76(%rbp), %r10d
.L478:
	movl	-120(%rbp), %edx
	testl	%edx, %edx
	jne	.L516
	movl	%r12d, %esi
	movq	-128(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	%r10d, %edi
	notl	%esi
	call	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE@PLT
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L486:
	movl	%r13d, %ebx
	movl	$-1, %r12d
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L440:
	cmpl	$193, %r12d
	jle	.L493
	andl	$31, %eax
.L443:
	movq	-72(%rbp), %rcx
	movslq	%ebx, %rdx
	movl	$-1, %r12d
	movzbl	(%rcx,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L439
	sall	$6, %eax
	movzbl	%dl, %r12d
	addl	$1, %ebx
	orl	%eax, %r12d
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L469:
	testq	%rdi, %rdi
	je	.L471
	movl	%r9d, %esi
	movl	%ecx, -84(%rbp)
	movb	%r8b, -76(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movl	-84(%rbp), %ecx
	movzbl	-76(%rbp), %r8d
.L471:
	movl	-120(%rbp), %esi
	testl	%esi, %esi
	jne	.L516
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L493:
	movl	$-1, %r12d
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L441:
	leal	-240(%r12), %eax
	cmpl	$4, %eax
	jg	.L488
	movslq	-88(%rbp), %rdx
	leaq	.LC1(%rip), %rdi
	movzbl	(%rsi,%rdx), %edx
	movq	%rdx, %rcx
	shrq	$4, %rcx
	andl	$15, %ecx
	movsbl	(%rdi,%rcx), %ecx
	btl	%eax, %ecx
	jnc	.L489
	movl	-76(%rbp), %ecx
	movl	$-1, %r12d
	leal	2(%rcx), %ebx
	cmpl	%ebx, %r13d
	je	.L439
	movslq	%ebx, %rcx
	movzbl	(%rsi,%rcx), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L439
	sall	$6, %eax
	andl	$63, %edx
	orl	%eax, %edx
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L572:
	cmpl	$921, %eax
	je	.L518
	cmpl	$933, %eax
	jne	.L447
.L518:
	orl	$32768, %r14d
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L577:
	movq	-72(%rbp), %rdi
	movslq	%edx, %rax
	cmpb	$-52, (%rdi,%rax)
	jne	.L514
	cmpb	$-127, 1(%rdi,%rax)
	setne	%al
	orl	%eax, %esi
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L576:
	movslq	%edx, %rax
	movq	-72(%rbp), %rdx
	cmpb	$-52, (%rdx,%rax)
	jne	.L466
	cmpb	$-120, 1(%rdx,%rax)
	setne	%sil
	orl	%edi, %esi
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L489:
	movl	-88(%rbp), %ebx
	movl	$-1, %r12d
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L574:
	movl	%r14d, %ecx
	andl	$98304, %ecx
	andl	$16384, %r14d
	jne	.L579
.L459:
	xorl	%r8d, %r8d
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L506:
	movl	$938, -116(%rbp)
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L488:
	movl	$-1, %r12d
	jmp	.L439
.L579:
	movl	-84(%rbp), %eax
	andl	$1, %eax
	orl	%r12d, %eax
	jne	.L459
	movq	-72(%rbp), %rdi
	movl	%r13d, %edx
	movl	%ebx, %esi
	movl	%ecx, -84(%rbp)
	call	_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKhii
	movl	-84(%rbp), %ecx
	testb	%al, %al
	jne	.L459
	movl	-76(%rbp), %eax
	cmpl	%eax, %ebx
	movl	$905, %eax
	cmovne	-116(%rbp), %eax
	setne	%r8b
	movl	%eax, -116(%rbp)
	jmp	.L460
.L571:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3150:
	.size	_ZN6icu_6710GreekUpper7toUpperEjPKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZN6icu_6710GreekUpper7toUpperEjPKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.p2align 4
	.globl	_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeES7_S9_SB_
	.type	_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeES7_S9_SB_, @function
_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeES7_S9_SB_:
.LFB3154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$40, %rsp
	movq	32(%rbp), %r15
	movq	16(%rbp), %r9
	movq	24(%rbp), %r13
	movl	(%r15), %r10d
	testl	%r10d, %r10d
	jg	.L580
	testq	%rcx, %rcx
	movq	%rcx, %r12
	sete	%cl
	testl	%r8d, %r8d
	setne	%al
	testb	%al, %cl
	jne	.L590
	cmpl	$-1, %r8d
	jl	.L590
	movl	%edi, %r14d
	je	.L592
.L585:
	testq	%r13, %r13
	je	.L586
	testl	$8192, %esi
	je	.L593
.L587:
	pushq	%r15
	movq	%r12, %rcx
	movl	%r14d, %edi
	pushq	%r13
	movq	%r9, -56(%rbp)
	call	*%rbx
	movq	-56(%rbp), %r9
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*32(%rax)
	movl	(%r15), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L580
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	movl	$1, (%r15)
.L580:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r9, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%esi, -56(%rbp)
	call	strlen@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rdx
	movl	-56(%rbp), %esi
	movl	%eax, %r8d
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L586:
	pushq	%r15
	movl	%r14d, %edi
	movq	%r12, %rcx
	pushq	$0
	movq	%r9, -56(%rbp)
	call	*%rbx
	movq	-56(%rbp), %r9
	popq	%rsi
	popq	%rdi
	movq	(%r9), %rax
	movq	%r9, %rdi
	movq	32(%rax), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%r9, -80(%rbp)
	movl	%r8d, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%esi, -56(%rbp)
	call	_ZN6icu_675Edits5resetEv@PLT
	movl	-56(%rbp), %esi
	movq	-64(%rbp), %rdx
	movl	-72(%rbp), %r8d
	movq	-80(%rbp), %r9
	jmp	.L587
	.cfi_endproc
.LFE3154:
	.size	_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeES7_S9_SB_, .-_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeES7_S9_SB_
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap11utf8ToLowerEPKcjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap11utf8ToLowerEPKcjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap11utf8ToLowerEPKcjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB3159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movl	%esi, -108(%rbp)
	movq	16(%rbp), %r14
	movq	%r9, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ustrcase_getCaseLocale_67@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L594
	testq	%r13, %r13
	movl	%eax, %r10d
	sete	%cl
	testl	%ebx, %ebx
	setne	%al
	testb	%al, %cl
	jne	.L605
	cmpl	$-1, %ebx
	jl	.L605
	je	.L607
.L599:
	cmpq	$0, -104(%rbp)
	je	.L600
	testl	$8192, -108(%rbp)
	je	.L608
.L601:
	subq	$8, %rsp
	movl	-108(%rbp), %esi
	pxor	%xmm0, %xmm0
	movl	%r10d, %edi
	pushq	%r14
	leaq	-96(%rbp), %rcx
	movl	%r15d, %r9d
	xorl	%r8d, %r8d
	pushq	-104(%rbp)
	movq	%r13, %rdx
	pushq	%r12
	movups	%xmm0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%r13, -96(%rbp)
	movl	%r15d, -80(%rbp)
	call	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	movq	(%r12), %rax
	addq	$32, %rsp
	movq	%r12, %rdi
	call	*32(%rax)
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L594
	movq	-104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L594:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L609
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	.cfi_restore_state
	movl	$1, (%r14)
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%r13, %rdi
	movl	%r10d, -112(%rbp)
	call	strlen@PLT
	movl	-112(%rbp), %r10d
	movl	%eax, %r15d
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L600:
	subq	$8, %rsp
	movl	-108(%rbp), %esi
	pxor	%xmm0, %xmm0
	movl	%r10d, %edi
	pushq	%r14
	leaq	-96(%rbp), %rcx
	movl	%r15d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r13, %rdx
	pushq	%r12
	movups	%xmm0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%r13, -96(%rbp)
	movl	%r15d, -80(%rbp)
	call	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	movq	(%r12), %rax
	addq	$32, %rsp
	movq	%r12, %rdi
	call	*32(%rax)
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L608:
	movq	-104(%rbp), %rdi
	movl	%r10d, -112(%rbp)
	call	_ZN6icu_675Edits5resetEv@PLT
	movl	-112(%rbp), %r10d
	jmp	.L601
.L609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3159:
	.size	_ZN6icu_677CaseMap11utf8ToLowerEPKcjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap11utf8ToLowerEPKcjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap11utf8ToUpperEPKcjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap11utf8ToUpperEPKcjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap11utf8ToUpperEPKcjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB3160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movl	%esi, -100(%rbp)
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ecx, -104(%rbp)
	call	ustrcase_getCaseLocale_67@PLT
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L610
	testq	%r14, %r14
	movl	%eax, %r10d
	sete	%dl
	testl	%ebx, %ebx
	setne	%al
	testb	%al, %dl
	jne	.L621
	cmpl	$-1, %ebx
	jl	.L621
	je	.L632
.L615:
	testq	%r12, %r12
	je	.L616
	testl	$8192, -100(%rbp)
	jne	.L616
	movq	%r12, %rdi
	movl	%r10d, -108(%rbp)
	call	_ZN6icu_675Edits5resetEv@PLT
	movl	-108(%rbp), %r10d
.L616:
	cmpl	$4, %r10d
	je	.L633
	pushq	%r15
	movl	-104(%rbp), %r8d
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rcx
	pushq	%r12
	movl	-100(%rbp), %esi
	movq	%r14, %rdx
	movq	%r13, %r9
	movl	%r10d, %edi
	movups	%xmm0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%r14, -96(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN12_GLOBAL__N_17toUpperEijPKhP12UCaseContextiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	popq	%rdx
	popq	%rcx
.L618:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*32(%rax)
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L610
	testq	%r12, %r12
	je	.L610
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L610:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L634
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movl	$1, (%r15)
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L633:
	movl	-104(%rbp), %edx
	movl	-100(%rbp), %edi
	movq	%r15, %r9
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rsi
	call	_ZN6icu_6710GreekUpper7toUpperEjPKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCode
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L632:
	movq	%r14, %rdi
	movl	%r10d, -108(%rbp)
	call	strlen@PLT
	movl	-108(%rbp), %r10d
	movl	%eax, -104(%rbp)
	jmp	.L615
.L634:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3160:
	.size	_ZN6icu_677CaseMap11utf8ToUpperEPKcjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap11utf8ToUpperEPKcjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap8utf8FoldEjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap8utf8FoldEjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap8utf8FoldEjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB3161:
	.cfi_startproc
	endbr64
	movl	(%r9), %r10d
	testl	%r10d, %r10d
	jg	.L647
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	movl	%edx, %r9d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	sete	%sil
	testl	%edx, %edx
	setne	%cl
	testb	%cl, %sil
	jne	.L645
	cmpl	$-1, %edx
	jl	.L645
	movl	%edi, %r13d
	movq	%r8, %rbx
	je	.L650
.L640:
	testq	%rbx, %rbx
	je	.L641
	testl	$8192, %r13d
	je	.L651
.L642:
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	pushq	%r12
	movl	%r13d, %esi
	movl	$-1, %edi
	pushq	%rbx
	pushq	%r15
	call	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	movq	(%r15), %rax
	addq	$32, %rsp
	movq	%r15, %rdi
	call	*32(%rax)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L635
	leaq	-40(%rbp), %rsp
	movq	%r12, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	movl	$1, (%r12)
.L635:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%eax, %r9d
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L641:
	subq	$8, %rsp
	movq	%r14, %rdx
	movl	%r13d, %esi
	movl	$-1, %edi
	pushq	%r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	pushq	%r15
	call	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	movq	(%r15), %rax
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	32(%rax), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	movq	%rbx, %rdi
	movl	%r9d, -52(%rbp)
	call	_ZN6icu_675Edits5resetEv@PLT
	movl	-52(%rbp), %r9d
	jmp	.L642
	.cfi_endproc
.LFE3161:
	.size	_ZN6icu_677CaseMap8utf8FoldEjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap8utf8FoldEjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.p2align 4
	.globl	_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPciPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeESA_SC_
	.type	_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPciPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeESA_SC_, @function
_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPciPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeESA_SC_:
.LFB3155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rbp), %rax
	movq	40(%rbp), %r10
	movq	24(%rbp), %r11
	movq	%rax, -104(%rbp)
	movl	(%r10), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	jg	.L652
	movl	%r8d, %r12d
	testl	%r8d, %r8d
	js	.L661
	movq	%rcx, %r13
	movl	%edi, %r15d
	movl	%esi, %r14d
	movq	%rdx, %rbx
	movq	%r9, %rcx
	testq	%r13, %r13
	jne	.L668
	testl	%r8d, %r8d
	jle	.L668
.L661:
	movl	$1, (%r10)
	xorl	%eax, %eax
.L652:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L682
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	.cfi_restore_state
	movl	16(%rbp), %esi
	testq	%rcx, %rcx
	sete	%dl
	testl	%esi, %esi
	setne	%al
	testb	%al, %dl
	jne	.L661
	cmpl	$-1, 16(%rbp)
	jl	.L661
	cmpl	$-1, 16(%rbp)
	jne	.L658
	movq	%rcx, %rdi
	movq	%r10, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%rcx, -112(%rbp)
	call	strlen@PLT
	movq	-128(%rbp), %r10
	movq	-120(%rbp), %r11
	movl	%eax, 16(%rbp)
	movq	-112(%rbp), %rcx
.L658:
	testq	%r13, %r13
	je	.L659
	cmpq	%rcx, %r13
	jbe	.L683
.L660:
	movslq	16(%rbp), %rax
	addq	%rcx, %rax
	cmpq	%rax, %r13
	jb	.L661
.L659:
	leaq	-96(%rbp), %rax
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r11, -128(%rbp)
	movq	%rax, %rdi
	movq	%r10, -136(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	cmpq	$0, -104(%rbp)
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r11
	movq	-136(%rbp), %r10
	je	.L663
	testl	$8192, %r14d
	je	.L684
.L663:
	pushq	%r10
	movq	%rbx, %rdx
	movq	-112(%rbp), %r9
	movl	%r14d, %esi
	pushq	-104(%rbp)
	movl	16(%rbp), %r8d
	movl	%r15d, %edi
	movq	%r10, -120(%rbp)
	call	*%r11
	movq	-112(%rbp), %rdi
	call	_ZN6icu_678ByteSink5FlushEv@PLT
	movq	-120(%rbp), %r10
	popq	%rax
	popq	%rdx
	movl	(%r10), %ecx
	testl	%ecx, %ecx
	jle	.L685
.L664:
	movl	-72(%rbp), %edx
	movq	%r10, %rcx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	u_terminateChars_67@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, -104(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-104(%rbp), %eax
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L683:
	movslq	%r12d, %rax
	addq	%r13, %rax
	cmpq	%rax, %rcx
	jb	.L661
	cmpq	%rcx, %r13
	jne	.L659
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L684:
	movq	-104(%rbp), %rdi
	call	_ZN6icu_675Edits5resetEv@PLT
	movq	-136(%rbp), %r10
	movq	-128(%rbp), %r11
	movq	-120(%rbp), %rcx
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L685:
	cmpb	$0, -68(%rbp)
	je	.L665
	movl	$15, (%r10)
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L665:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L664
	movq	%r10, %rsi
	movq	%r10, -104(%rbp)
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	movq	-104(%rbp), %r10
	jmp	.L664
.L682:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3155:
	.size	_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPciPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeESA_SC_, .-_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPciPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeESA_SC_
	.p2align 4
	.globl	ucasemap_utf8FoldCase_67
	.type	ucasemap_utf8FoldCase_67, @function
ucasemap_utf8FoldCase_67:
.LFB3158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L686
	movl	%edx, %r13d
	movq	%r9, %r12
	testl	%edx, %edx
	js	.L695
	movq	%rdi, %r14
	movq	%rsi, %r15
	movq	%rcx, %rbx
	testq	%rsi, %rsi
	jne	.L700
	testl	%edx, %edx
	jle	.L700
.L695:
	movl	$1, (%r12)
	xorl	%eax, %eax
.L686:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L708
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L700:
	.cfi_restore_state
	testq	%rbx, %rbx
	sete	%dl
	testl	%r8d, %r8d
	setne	%al
	testb	%al, %dl
	jne	.L695
	cmpl	$-1, %r8d
	jl	.L695
	jne	.L692
	movq	%rbx, %rdi
	call	strlen@PLT
	movl	%eax, %r8d
.L692:
	testq	%r15, %r15
	je	.L693
	cmpq	%rbx, %r15
	jbe	.L709
.L694:
	movslq	%r8d, %rax
	addq	%rbx, %rax
	cmpq	%rax, %r15
	jb	.L695
.L693:
	movl	44(%r14), %r10d
	leaq	-96(%rbp), %r14
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%r8d, -104(%rbp)
	movl	%r10d, -100(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	subq	$8, %rsp
	movl	-104(%rbp), %r8d
	xorl	%ecx, %ecx
	pushq	%r12
	movl	-100(%rbp), %r10d
	movq	%rbx, %rdx
	movl	$-1, %edi
	pushq	$0
	movl	%r8d, %r9d
	xorl	%r8d, %r8d
	pushq	%r14
	movl	%r10d, %esi
	call	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	addq	$32, %rsp
	movq	%r14, %rdi
	call	_ZN6icu_678ByteSink5FlushEv@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L710
.L697:
	movl	-72(%rbp), %edx
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	u_terminateChars_67@PLT
	movq	%r14, %rdi
	movl	%eax, -100(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-100(%rbp), %eax
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L709:
	movslq	%r13d, %rax
	addq	%r15, %rax
	cmpq	%rax, %rbx
	jb	.L695
	cmpq	%rbx, %r15
	jne	.L693
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L710:
	cmpb	$0, -68(%rbp)
	je	.L697
	movl	$15, (%r12)
	jmp	.L697
.L708:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3158:
	.size	ucasemap_utf8FoldCase_67, .-ucasemap_utf8FoldCase_67
	.p2align 4
	.globl	ucasemap_utf8ToLower_67
	.type	ucasemap_utf8ToLower_67, @function
ucasemap_utf8ToLower_67:
.LFB3156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L711
	movl	%edx, %r13d
	movq	%r9, %r12
	testl	%edx, %edx
	js	.L720
	movq	%rdi, %r14
	movq	%rsi, %r15
	movq	%rcx, %rbx
	testq	%rsi, %rsi
	jne	.L725
	testl	%edx, %edx
	jle	.L725
.L720:
	movl	$1, (%r12)
	xorl	%eax, %eax
.L711:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L733
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	testq	%rbx, %rbx
	sete	%dl
	testl	%r8d, %r8d
	setne	%al
	testb	%al, %dl
	jne	.L720
	cmpl	$-1, %r8d
	jl	.L720
	jne	.L717
	movq	%rbx, %rdi
	call	strlen@PLT
	movl	%eax, %r8d
.L717:
	testq	%r15, %r15
	je	.L718
	cmpq	%rbx, %r15
	jbe	.L734
.L719:
	movslq	%r8d, %rax
	addq	%rbx, %rax
	cmpq	%rax, %r15
	jb	.L720
.L718:
	movl	44(%r14), %r10d
	movl	40(%r14), %r11d
	leaq	-128(%rbp), %r14
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%r8d, -140(%rbp)
	movl	%r10d, -136(%rbp)
	movl	%r11d, -132(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	subq	$8, %rsp
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	pushq	%r12
	movl	-140(%rbp), %r8d
	leaq	-96(%rbp), %rcx
	movl	-136(%rbp), %r10d
	movl	-132(%rbp), %r11d
	pushq	$0
	pushq	%r14
	movl	%r8d, %r9d
	movups	%xmm0, -88(%rbp)
	movl	%r11d, %edi
	movl	%r10d, %esi
	movl	%r8d, -80(%rbp)
	xorl	%r8d, %r8d
	movq	$0, -72(%rbp)
	movq	%rbx, -96(%rbp)
	call	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	addq	$32, %rsp
	movq	%r14, %rdi
	call	_ZN6icu_678ByteSink5FlushEv@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L735
.L722:
	movl	-104(%rbp), %edx
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	u_terminateChars_67@PLT
	movq	%r14, %rdi
	movl	%eax, -132(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-132(%rbp), %eax
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L734:
	movslq	%r13d, %rax
	addq	%r15, %rax
	cmpq	%rax, %rbx
	jb	.L720
	cmpq	%rbx, %r15
	jne	.L718
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L735:
	cmpb	$0, -100(%rbp)
	je	.L722
	movl	$15, (%r12)
	jmp	.L722
.L733:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3156:
	.size	ucasemap_utf8ToLower_67, .-ucasemap_utf8ToLower_67
	.p2align 4
	.globl	ucasemap_utf8ToUpper_67
	.type	ucasemap_utf8ToUpper_67, @function
ucasemap_utf8ToUpper_67:
.LFB3157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L736
	movl	%edx, %r13d
	movq	%r9, %r12
	testl	%edx, %edx
	js	.L745
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	%rcx, %r10
	movl	%r8d, %r15d
	testq	%rsi, %rsi
	jne	.L752
	testl	%edx, %edx
	jle	.L752
.L745:
	movl	$1, (%r12)
	xorl	%eax, %eax
.L736:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L760
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L752:
	.cfi_restore_state
	testq	%r10, %r10
	sete	%dl
	testl	%r15d, %r15d
	setne	%al
	testb	%al, %dl
	jne	.L745
	cmpl	$-1, %r15d
	jl	.L745
	jne	.L742
	movq	%r10, %rdi
	movq	%r10, -136(%rbp)
	call	strlen@PLT
	movq	-136(%rbp), %r10
	movl	%eax, %r15d
.L742:
	testq	%r14, %r14
	je	.L743
	cmpq	%r10, %r14
	jbe	.L761
.L744:
	movslq	%r15d, %rax
	addq	%r10, %rax
	cmpq	%rax, %r14
	jb	.L745
.L743:
	movl	44(%rbx), %r11d
	movl	40(%rbx), %eax
	leaq	-128(%rbp), %rbx
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r10, -152(%rbp)
	movl	%r11d, -140(%rbp)
	movl	%eax, -136(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	movl	-136(%rbp), %eax
	movl	-140(%rbp), %r11d
	movq	-152(%rbp), %r10
	cmpl	$4, %eax
	je	.L762
	pushq	%r12
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rcx
	movq	%r10, %rdx
	pushq	$0
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	%r11d, %esi
	movl	%eax, %edi
	movups	%xmm0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%r10, -96(%rbp)
	movl	%r15d, -80(%rbp)
	call	_ZN12_GLOBAL__N_17toUpperEijPKhP12UCaseContextiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	popq	%rdx
	popq	%rcx
.L748:
	movq	%rbx, %rdi
	call	_ZN6icu_678ByteSink5FlushEv@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L763
.L749:
	movl	-104(%rbp), %edx
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	u_terminateChars_67@PLT
	movq	%rbx, %rdi
	movl	%eax, -136(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-136(%rbp), %eax
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L761:
	movslq	%r13d, %rax
	addq	%r14, %rax
	cmpq	%rax, %r10
	jb	.L745
	cmpq	%r10, %r14
	jne	.L743
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L763:
	cmpb	$0, -100(%rbp)
	je	.L749
	movl	$15, (%r12)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L762:
	movq	%r12, %r9
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	%r15d, %edx
	movq	%r10, %rsi
	movl	%r11d, %edi
	call	_ZN6icu_6710GreekUpper7toUpperEjPKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCode
	jmp	.L748
.L760:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3157:
	.size	ucasemap_utf8ToUpper_67, .-ucasemap_utf8ToUpper_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap8utf8FoldEjPKciPciPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap8utf8FoldEjPKciPciPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap8utf8FoldEjPKciPciPNS_5EditsER10UErrorCode:
.LFB3164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	xorl	%r8d, %r8d
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %r11
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movl	(%r11), %r10d
	testl	%r10d, %r10d
	jg	.L764
	testl	%r12d, %r12d
	js	.L773
	movl	%edi, %r10d
	movq	%rsi, %r14
	movl	%edx, %eax
	movq	%rcx, %r13
	movq	%r9, %r15
	testq	%rcx, %rcx
	jne	.L780
	testl	%r12d, %r12d
	jle	.L780
.L773:
	movl	$1, (%r11)
	xorl	%r8d, %r8d
.L764:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L794
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	testq	%r14, %r14
	sete	%cl
	testl	%eax, %eax
	setne	%dl
	testb	%dl, %cl
	jne	.L773
	cmpl	$-1, %eax
	jl	.L773
	jne	.L770
	movq	%r14, %rdi
	movq	%r11, -112(%rbp)
	movl	%r10d, -104(%rbp)
	call	strlen@PLT
	movq	-112(%rbp), %r11
	movl	-104(%rbp), %r10d
.L770:
	testq	%r13, %r13
	je	.L771
	cmpq	%r14, %r13
	jbe	.L795
.L772:
	movslq	%eax, %rdx
	addq	%r14, %rdx
	cmpq	%rdx, %r13
	jb	.L773
.L771:
	leaq	-96(%rbp), %rbx
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r11, -120(%rbp)
	movq	%rbx, %rdi
	movl	%eax, -112(%rbp)
	movl	%r10d, -104(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	testq	%r15, %r15
	movl	-104(%rbp), %r10d
	movl	-112(%rbp), %eax
	movq	-120(%rbp), %r11
	je	.L775
	testl	$8192, %r10d
	je	.L796
.L775:
	subq	$8, %rsp
	movl	%eax, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%r11
	movq	%r14, %rdx
	movl	%r10d, %esi
	movl	$-1, %edi
	pushq	%r15
	pushq	%rbx
	movq	%r11, -104(%rbp)
	call	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	addq	$32, %rsp
	movq	%rbx, %rdi
	call	_ZN6icu_678ByteSink5FlushEv@PLT
	movq	-104(%rbp), %r11
	movl	(%r11), %eax
	testl	%eax, %eax
	jle	.L797
.L776:
	movl	-72(%rbp), %edx
	movq	%r11, %rcx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	u_terminateChars_67@PLT
	movq	%rbx, %rdi
	movl	%eax, -104(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-104(%rbp), %r8d
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L795:
	movslq	%r12d, %rdx
	addq	%r13, %rdx
	cmpq	%rdx, %r14
	jb	.L773
	cmpq	%r14, %r13
	jne	.L771
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L796:
	movq	%r15, %rdi
	call	_ZN6icu_675Edits5resetEv@PLT
	movq	-120(%rbp), %r11
	movl	-112(%rbp), %eax
	movl	-104(%rbp), %r10d
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L797:
	cmpb	$0, -68(%rbp)
	je	.L777
	movl	$15, (%r11)
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L777:
	testq	%r15, %r15
	je	.L776
	movq	%r11, %rsi
	movq	%r15, %rdi
	movq	%r11, -104(%rbp)
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	movq	-104(%rbp), %r11
	jmp	.L776
.L794:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3164:
	.size	_ZN6icu_677CaseMap8utf8FoldEjPKciPciPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap8utf8FoldEjPKciPciPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap11utf8ToLowerEPKcjS2_iPciPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap11utf8ToLowerEPKcjS2_iPciPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap11utf8ToLowerEPKcjS2_iPciPNS_5EditsER10UErrorCode:
.LFB3162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movl	%esi, -140(%rbp)
	movq	24(%rbp), %r15
	movq	%rax, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ustrcase_getCaseLocale_67@PLT
	movl	(%r15), %edx
	movl	%eax, %r11d
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L798
	testl	%r12d, %r12d
	js	.L807
	testq	%r13, %r13
	jne	.L814
	testl	%r12d, %r12d
	jle	.L814
.L807:
	movl	$1, (%r15)
	xorl	%eax, %eax
.L798:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L828
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	.cfi_restore_state
	testq	%r14, %r14
	sete	%dl
	testl	%ebx, %ebx
	setne	%al
	testb	%al, %dl
	jne	.L807
	cmpl	$-1, %ebx
	jl	.L807
	jne	.L804
	movq	%r14, %rdi
	movl	%r11d, -152(%rbp)
	call	strlen@PLT
	movl	-152(%rbp), %r11d
	movl	%eax, %ebx
.L804:
	testq	%r13, %r13
	je	.L805
	cmpq	%r14, %r13
	jbe	.L829
.L806:
	movslq	%ebx, %rax
	addq	%r14, %rax
	cmpq	%rax, %r13
	jb	.L807
.L805:
	leaq	-128(%rbp), %rax
	movl	%r12d, %edx
	movq	%r13, %rsi
	movl	%r11d, -144(%rbp)
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	cmpq	$0, -136(%rbp)
	movl	-144(%rbp), %r11d
	je	.L809
	testl	$8192, -140(%rbp)
	je	.L830
.L809:
	subq	$8, %rsp
	movl	-140(%rbp), %esi
	movl	%r11d, %edi
	movl	%ebx, %r9d
	pushq	%r15
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rcx
	xorl	%r8d, %r8d
	pushq	-136(%rbp)
	movq	%r14, %rdx
	pushq	-152(%rbp)
	movups	%xmm0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%r14, -96(%rbp)
	movl	%ebx, -80(%rbp)
	call	_ZN12_GLOBAL__N_17toLowerEijPKhP12UCaseContextiiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	movq	-152(%rbp), %rdi
	addq	$32, %rsp
	call	_ZN6icu_678ByteSink5FlushEv@PLT
	movl	(%r15), %eax
	testl	%eax, %eax
	jle	.L831
.L810:
	movl	-104(%rbp), %edx
	movq	%r15, %rcx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	u_terminateChars_67@PLT
	movq	-152(%rbp), %rdi
	movl	%eax, -136(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-136(%rbp), %eax
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L829:
	movslq	%r12d, %rax
	addq	%r13, %rax
	cmpq	%rax, %r14
	jb	.L807
	cmpq	%r14, %r13
	jne	.L805
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L830:
	movq	-136(%rbp), %rdi
	call	_ZN6icu_675Edits5resetEv@PLT
	movl	-144(%rbp), %r11d
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L831:
	cmpb	$0, -100(%rbp)
	je	.L811
	movl	$15, (%r15)
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L811:
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L810
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	jmp	.L810
.L828:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3162:
	.size	_ZN6icu_677CaseMap11utf8ToLowerEPKcjS2_iPciPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap11utf8ToLowerEPKcjS2_iPciPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap11utf8ToUpperEPKcjS2_iPciPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap11utf8ToUpperEPKcjS2_iPciPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap11utf8ToUpperEPKcjS2_iPciPNS_5EditsER10UErrorCode:
.LFB3163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movl	%esi, -140(%rbp)
	movq	24(%rbp), %rbx
	movq	%rax, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ustrcase_getCaseLocale_67@PLT
	movl	(%rbx), %esi
	movl	%eax, %r11d
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L832
	testl	%r12d, %r12d
	js	.L841
	testq	%r13, %r13
	jne	.L850
	testl	%r12d, %r12d
	jle	.L850
.L841:
	movl	$1, (%rbx)
	xorl	%eax, %eax
.L832:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L864
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L850:
	.cfi_restore_state
	testq	%r15, %r15
	sete	%dl
	testl	%r14d, %r14d
	setne	%al
	testb	%al, %dl
	jne	.L841
	cmpl	$-1, %r14d
	jl	.L841
	jne	.L838
	movq	%r15, %rdi
	movl	%r11d, -152(%rbp)
	call	strlen@PLT
	movl	-152(%rbp), %r11d
	movl	%eax, %r14d
.L838:
	testq	%r13, %r13
	je	.L839
	cmpq	%r15, %r13
	jbe	.L865
.L840:
	movslq	%r14d, %rax
	addq	%r15, %rax
	cmpq	%rax, %r13
	jb	.L841
.L839:
	leaq	-128(%rbp), %rax
	movl	%r12d, %edx
	movq	%r13, %rsi
	movl	%r11d, -144(%rbp)
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	cmpq	$0, -136(%rbp)
	movl	-144(%rbp), %r11d
	je	.L843
	testl	$8192, -140(%rbp)
	je	.L866
.L843:
	cmpl	$4, %r11d
	je	.L867
	pushq	%rbx
	movq	-152(%rbp), %r9
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rcx
	pushq	-136(%rbp)
	movq	%r15, %rdx
	movl	%r14d, %r8d
	movl	%r11d, %edi
	movl	-140(%rbp), %esi
	movups	%xmm0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%r15, -96(%rbp)
	movl	%r14d, -80(%rbp)
	call	_ZN12_GLOBAL__N_17toUpperEijPKhP12UCaseContextiRN6icu_678ByteSinkEPNS4_5EditsER10UErrorCode
	popq	%rdx
	popq	%rcx
.L845:
	movq	-152(%rbp), %rdi
	call	_ZN6icu_678ByteSink5FlushEv@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L868
.L846:
	movl	-104(%rbp), %edx
	movq	%rbx, %rcx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	u_terminateChars_67@PLT
	movq	-152(%rbp), %rdi
	movl	%eax, -136(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-136(%rbp), %eax
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L865:
	movslq	%r12d, %rax
	addq	%r13, %rax
	cmpq	%rax, %r15
	jb	.L841
	cmpq	%r15, %r13
	jne	.L839
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L866:
	movq	-136(%rbp), %rdi
	call	_ZN6icu_675Edits5resetEv@PLT
	movl	-144(%rbp), %r11d
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L868:
	cmpb	$0, -100(%rbp)
	je	.L847
	movl	$15, (%rbx)
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L867:
	movq	-136(%rbp), %r8
	movq	%rbx, %r9
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	-152(%rbp), %rcx
	movl	-140(%rbp), %edi
	call	_ZN6icu_6710GreekUpper7toUpperEjPKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCode
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L847:
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L846
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	jmp	.L846
.L864:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3163:
	.size	_ZN6icu_677CaseMap11utf8ToUpperEPKcjS2_iPciPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap11utf8ToUpperEPKcjS2_iPciPNS_5EditsER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
