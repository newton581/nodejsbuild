	.file	"ucnv_err.cpp"
	.text
	.p2align 4
	.globl	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67
	.type	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67, @function
UCNV_FROM_U_CALLBACK_SUBSTITUTE_67:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rdx
	cmpl	$2, %r9d
	jg	.L1
	movq	%rsi, %r10
	testl	%r9d, %r9d
	jne	.L3
	cmpl	$173, %r8d
	je	.L4
	cmpl	$847, %r8d
	je	.L4
	leal	-4447(%r8), %eax
	cmpl	$1, %eax
	jbe	.L4
	cmpl	$1564, %r8d
	jne	.L15
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$0, (%rdx)
.L1:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1
.L6:
	movl	$0, (%rdx)
	xorl	%esi, %esi
	movq	%r10, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ucnv_cbFromUWriteSub_67@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	leal	-6068(%r8), %eax
	cmpl	$1, %eax
	jbe	.L4
	leal	-6155(%r8), %eax
	cmpl	$3, %eax
	jbe	.L4
	leal	-8203(%r8), %eax
	cmpl	$4, %eax
	jbe	.L4
	leal	-8234(%r8), %eax
	cmpl	$4, %eax
	jbe	.L4
	leal	-8288(%r8), %eax
	cmpl	$15, %eax
	jbe	.L4
	cmpl	$12644, %r8d
	je	.L4
	leal	-65024(%r8), %eax
	cmpl	$15, %eax
	jbe	.L4
	cmpl	$65279, %r8d
	je	.L4
	cmpl	$65440, %r8d
	je	.L4
	leal	-65520(%r8), %eax
	cmpl	$8, %eax
	jbe	.L4
	leal	-113824(%r8), %eax
	cmpl	$3, %eax
	jbe	.L4
	leal	-119155(%r8), %eax
	cmpl	$7, %eax
	jbe	.L4
	subl	$917504, %r8d
	cmpl	$4095, %r8d
	jbe	.L4
	testq	%rdi, %rdi
	je	.L6
	cmpb	$105, (%rdi)
	je	.L6
	jmp	.L1
	.cfi_endproc
.LFE2116:
	.size	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67, .-UCNV_FROM_U_CALLBACK_SUBSTITUTE_67
	.p2align 4
	.globl	UCNV_FROM_U_CALLBACK_STOP_67
	.type	UCNV_FROM_U_CALLBACK_STOP_67, @function
UCNV_FROM_U_CALLBACK_STOP_67:
.LFB2113:
	.cfi_startproc
	endbr64
	testl	%r9d, %r9d
	jne	.L20
	cmpl	$173, %r8d
	je	.L18
	cmpl	$847, %r8d
	je	.L18
	leal	-4447(%r8), %eax
	cmpl	$1, %eax
	jbe	.L18
	cmpl	$1564, %r8d
	je	.L18
	leal	-6068(%r8), %eax
	cmpl	$1, %eax
	jbe	.L18
	leal	-6155(%r8), %eax
	cmpl	$3, %eax
	jbe	.L18
	leal	-8203(%r8), %eax
	cmpl	$4, %eax
	jbe	.L18
	leal	-8234(%r8), %eax
	cmpl	$4, %eax
	jbe	.L18
	leal	-8288(%r8), %eax
	cmpl	$15, %eax
	jbe	.L18
	cmpl	$12644, %r8d
	je	.L18
	leal	-65024(%r8), %eax
	cmpl	$15, %eax
	jbe	.L18
	cmpl	$65279, %r8d
	je	.L18
	cmpl	$65440, %r8d
	je	.L18
	leal	-65520(%r8), %eax
	cmpl	$8, %eax
	jbe	.L18
	leal	-113824(%r8), %eax
	cmpl	$3, %eax
	jbe	.L18
	leal	-119155(%r8), %eax
	cmpl	$7, %eax
	jbe	.L18
	subl	$917504, %r8d
	cmpl	$4095, %r8d
	ja	.L20
.L18:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rax
	movl	$0, (%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2113:
	.size	UCNV_FROM_U_CALLBACK_STOP_67, .-UCNV_FROM_U_CALLBACK_STOP_67
	.p2align 4
	.globl	UCNV_TO_U_CALLBACK_STOP_67
	.type	UCNV_TO_U_CALLBACK_STOP_67, @function
UCNV_TO_U_CALLBACK_STOP_67:
.LFB2114:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2114:
	.size	UCNV_TO_U_CALLBACK_STOP_67, .-UCNV_TO_U_CALLBACK_STOP_67
	.p2align 4
	.globl	UCNV_FROM_U_CALLBACK_SKIP_67
	.type	UCNV_FROM_U_CALLBACK_SKIP_67, @function
UCNV_FROM_U_CALLBACK_SKIP_67:
.LFB2115:
	.cfi_startproc
	endbr64
	cmpl	$2, %r9d
	jg	.L35
	testl	%r9d, %r9d
	jne	.L25
	cmpl	$173, %r8d
	je	.L26
	cmpl	$847, %r8d
	je	.L26
	leal	-4447(%r8), %eax
	cmpl	$1, %eax
	jbe	.L26
	cmpl	$1564, %r8d
	jne	.L37
.L26:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rax
	movl	$0, (%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	testq	%rdi, %rdi
	je	.L26
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	leal	-6068(%r8), %eax
	cmpl	$1, %eax
	jbe	.L26
	leal	-6155(%r8), %eax
	cmpl	$3, %eax
	jbe	.L26
	leal	-8203(%r8), %eax
	cmpl	$4, %eax
	jbe	.L26
	leal	-8234(%r8), %eax
	cmpl	$4, %eax
	jbe	.L26
	leal	-8288(%r8), %eax
	cmpl	$15, %eax
	jbe	.L26
	cmpl	$12644, %r8d
	je	.L26
	leal	-65024(%r8), %eax
	cmpl	$15, %eax
	jbe	.L26
	cmpl	$65279, %r8d
	je	.L26
	cmpl	$65440, %r8d
	je	.L26
	leal	-65520(%r8), %eax
	cmpl	$8, %eax
	jbe	.L26
	leal	-113824(%r8), %eax
	cmpl	$3, %eax
	jbe	.L26
	leal	-119155(%r8), %eax
	cmpl	$7, %eax
	jbe	.L26
	subl	$917504, %r8d
	cmpl	$4095, %r8d
	jbe	.L26
	testq	%rdi, %rdi
	je	.L26
	cmpb	$105, (%rdi)
	je	.L26
	ret
	.cfi_endproc
.LFE2115:
	.size	UCNV_FROM_U_CALLBACK_SKIP_67, .-UCNV_FROM_U_CALLBACK_SKIP_67
	.p2align 4
	.globl	UCNV_FROM_U_CALLBACK_ESCAPE_67
	.type	UCNV_FROM_U_CALLBACK_ESCAPE_67, @function
UCNV_FROM_U_CALLBACK_ESCAPE_67:
.LFB2117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r14
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	$0, -200(%rbp)
	movl	$0, -204(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -176(%rbp)
	cmpl	$2, %r9d
	jg	.L38
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %rbx
	movl	%r8d, %r15d
	testl	%r9d, %r9d
	jne	.L41
	cmpl	$173, %r8d
	je	.L42
	cmpl	$847, %r8d
	je	.L42
	leal	-4447(%r8), %edx
	cmpl	$1, %edx
	jbe	.L42
	cmpl	$1564, %r8d
	je	.L42
	leal	-6068(%r8), %edx
	cmpl	$1, %edx
	jbe	.L42
	leal	-6155(%r8), %edx
	cmpl	$3, %edx
	jbe	.L42
	leal	-8203(%r8), %edx
	cmpl	$4, %edx
	jbe	.L42
	leal	-8234(%r8), %edx
	cmpl	$4, %edx
	jbe	.L42
	leal	-8288(%r8), %edx
	cmpl	$15, %edx
	jbe	.L42
	cmpl	$12644, %r8d
	je	.L42
	leal	-65024(%r8), %edx
	cmpl	$15, %edx
	jbe	.L42
	cmpl	$65279, %r8d
	je	.L42
	cmpl	$65440, %r8d
	je	.L42
	leal	-65520(%r8), %edx
	cmpl	$8, %edx
	jbe	.L42
	leal	-113824(%r8), %edx
	cmpl	$3, %edx
	jbe	.L42
	leal	-119155(%r8), %edx
	cmpl	$7, %edx
	jbe	.L42
	leal	-917504(%r8), %edx
	cmpl	$4095, %edx
	jbe	.L42
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%eax, -216(%rbp)
	movq	8(%r12), %rdi
	xorl	%edx, %edx
	leaq	-204(%rbp), %rax
	movq	%rax, %r9
	leaq	-192(%rbp), %rcx
	leaq	-184(%rbp), %r8
	movq	%rax, -224(%rbp)
	leaq	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67(%rip), %rsi
	call	ucnv_setFromUCallBack_67@PLT
	movl	-204(%rbp), %edx
	movl	-216(%rbp), %eax
	testl	%edx, %edx
	jg	.L76
	testq	%r13, %r13
	je	.L77
	movzbl	0(%r13), %edx
	subl	$67, %edx
	cmpb	$21, %dl
	ja	.L47
	leaq	.L49(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L49:
	.long	.L54-.L49
	.long	.L53-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L52-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L51-.L49
	.long	.L47-.L49
	.long	.L50-.L49
	.long	.L47-.L49
	.long	.L47-.L49
	.long	.L48-.L49
	.text
.L54:
	movl	$92, %edx
	movw	%dx, -160(%rbp)
	cmpl	$2, %eax
	je	.L78
	movl	$117, %r15d
	movzwl	(%rbx), %edx
	leaq	-160(%rbp), %r13
	leaq	-156(%rbp), %rdi
	movw	%r15w, -158(%rbp)
	movl	$4, %r8d
	movl	$16, %ecx
.L73:
	movl	$46, %esi
	call	uprv_itou_67@PLT
	addl	$2, %eax
	.p2align 4,,10
	.p2align 3
.L75:
	cltq
	leaq	0(%r13,%rax,2), %rdx
.L45:
	movl	$0, (%r14)
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	-200(%rbp), %rsi
	movq	%r13, -200(%rbp)
	call	ucnv_cbFromUWriteUChars_67@PLT
	movq	8(%r12), %rdi
	movq	-224(%rbp), %r9
	leaq	-176(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	leaq	-168(%rbp), %r8
	call	ucnv_setFromUCallBack_67@PLT
	movl	-204(%rbp), %eax
	testl	%eax, %eax
	jle	.L38
	movl	%eax, (%r14)
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movl	$0, (%r14)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L76:
	movl	%edx, (%r14)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L47:
	testl	%eax, %eax
	jle	.L68
	subl	$1, %eax
	leaq	-160(%rbp), %r13
	leaq	2(%rbx,%rax,2), %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L63:
	leal	2(%rax), %r15d
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	$37, %ecx
	cltq
	movl	$85, %esi
	movl	$4, %r8d
	movw	%cx, -160(%rbp,%rdx,2)
	movw	%si, -160(%rbp,%rax,2)
	movzwl	(%rbx), %edx
	movslq	%r15d, %rax
	movl	$48, %esi
	subl	%r15d, %esi
	leaq	0(%r13,%rax,2), %rdi
	movl	$16, %ecx
	addq	$2, %rbx
	call	uprv_itou_67@PLT
	addl	%r15d, %eax
	cmpq	%rbx, -216(%rbp)
	jne	.L63
	jmp	.L75
.L53:
	movl	$2293798, -160(%rbp)
	cmpl	$2, %eax
	je	.L80
	movzwl	(%rbx), %edx
	xorl	%r8d, %r8d
	movl	$10, %ecx
	movl	$46, %esi
	leaq	-156(%rbp), %rdi
	call	uprv_itou_67@PLT
	addl	$2, %eax
.L60:
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	$59, %r11d
	leaq	-160(%rbp), %r13
	cltq
	movw	%r11w, -160(%rbp,%rdx,2)
	leaq	0(%r13,%rax,2), %rdx
	jmp	.L45
.L48:
	movl	$2293798, -160(%rbp)
	movl	$120, %r13d
	movw	%r13w, -156(%rbp)
	cmpl	$2, %eax
	je	.L81
	movzwl	(%rbx), %edx
	xorl	%r8d, %r8d
	movl	$16, %ecx
	movl	$45, %esi
	leaq	-154(%rbp), %rdi
	call	uprv_itou_67@PLT
	addl	$3, %eax
	jmp	.L60
.L51:
	movl	$92, %edi
	xorl	%r8d, %r8d
	movl	%r15d, %edx
	movl	$16, %ecx
	movw	%di, -160(%rbp)
	movl	$47, %esi
	leaq	-158(%rbp), %rdi
	leaq	-160(%rbp), %r13
	call	uprv_itou_67@PLT
	movl	$32, %r8d
	leal	1(%rax), %edx
	addl	$2, %eax
	movslq	%edx, %rdx
	movw	%r8w, -160(%rbp,%rdx,2)
	jmp	.L75
.L50:
	movl	$5570683, -160(%rbp)
	movl	$43, %r10d
	movw	%r10w, -156(%rbp)
	cmpl	$2, %eax
	je	.L82
	movzwl	(%rbx), %edx
	movl	$4, %r8d
	movl	$16, %ecx
	leaq	-154(%rbp), %rdi
	movl	$45, %esi
	call	uprv_itou_67@PLT
	addl	$3, %eax
.L62:
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	$125, %r9d
	leaq	-160(%rbp), %r13
	cltq
	movw	%r9w, -160(%rbp,%rdx,2)
	leaq	0(%r13,%rax,2), %rdx
	jmp	.L45
.L52:
	testl	%eax, %eax
	jle	.L68
	subl	$1, %eax
	leaq	-160(%rbp), %r13
	leaq	2(%rbx,%rax,2), %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L55:
	leal	2(%rax), %r15d
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	$92, %ecx
	cltq
	movl	$117, %esi
	movl	$4, %r8d
	movw	%cx, -160(%rbp,%rdx,2)
	movw	%si, -160(%rbp,%rax,2)
	movzwl	(%rbx), %edx
	movslq	%r15d, %rax
	movl	$48, %esi
	subl	%r15d, %esi
	leaq	0(%r13,%rax,2), %rdi
	movl	$16, %ecx
	addq	$2, %rbx
	call	uprv_itou_67@PLT
	addl	%r15d, %eax
	cmpq	%rbx, -216(%rbp)
	jne	.L55
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L77:
	testl	%eax, %eax
	jle	.L68
	subl	$1, %eax
	leaq	-160(%rbp), %r13
	leaq	2(%rbx,%rax,2), %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L46:
	leal	2(%rax), %r15d
	movslq	%eax, %rdx
	addl	$1, %eax
	movl	$37, %edi
	cltq
	movl	$48, %esi
	movl	$16, %ecx
	addq	$2, %rbx
	movl	$85, %r8d
	movw	%di, -160(%rbp,%rdx,2)
	movzwl	-2(%rbx), %edx
	subl	%r15d, %esi
	movw	%r8w, -160(%rbp,%rax,2)
	movslq	%r15d, %rax
	movl	$4, %r8d
	leaq	0(%r13,%rax,2), %rdi
	call	uprv_itou_67@PLT
	addl	%r15d, %eax
	cmpq	-216(%rbp), %rbx
	jne	.L46
	jmp	.L75
.L68:
	leaq	-160(%rbp), %r13
	movq	%r13, %rdx
	jmp	.L45
.L80:
	xorl	%r8d, %r8d
	movl	$10, %ecx
	movl	%r15d, %edx
	movl	$46, %esi
	leaq	-156(%rbp), %rdi
	call	uprv_itou_67@PLT
	addl	$2, %eax
	jmp	.L60
.L81:
	xorl	%r8d, %r8d
	movl	$16, %ecx
	movl	%r15d, %edx
	movl	$45, %esi
	leaq	-154(%rbp), %rdi
	call	uprv_itou_67@PLT
	addl	$3, %eax
	jmp	.L60
.L82:
	leaq	-154(%rbp), %rdi
	movl	$16, %ecx
	movl	%r15d, %edx
	movl	$45, %esi
	movl	$4, %r8d
	call	uprv_itou_67@PLT
	addl	$3, %eax
	jmp	.L62
.L78:
	movl	$85, %eax
	leaq	-160(%rbp), %r13
	movl	$16, %ecx
	movl	$8, %r8d
	movw	%ax, -158(%rbp)
	leaq	-156(%rbp), %rdi
	movl	%r15d, %edx
	jmp	.L73
.L79:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2117:
	.size	UCNV_FROM_U_CALLBACK_ESCAPE_67, .-UCNV_FROM_U_CALLBACK_ESCAPE_67
	.p2align 4
	.globl	UCNV_TO_U_CALLBACK_SKIP_67
	.type	UCNV_TO_U_CALLBACK_SKIP_67, @function
UCNV_TO_U_CALLBACK_SKIP_67:
.LFB2118:
	.cfi_startproc
	endbr64
	cmpl	$2, %r8d
	jg	.L83
	testq	%rdi, %rdi
	je	.L85
	cmpb	$105, (%rdi)
	jne	.L83
	testl	%r8d, %r8d
	je	.L85
.L83:
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$0, (%r9)
	ret
	.cfi_endproc
.LFE2118:
	.size	UCNV_TO_U_CALLBACK_SKIP_67, .-UCNV_TO_U_CALLBACK_SKIP_67
	.p2align 4
	.globl	UCNV_TO_U_CALLBACK_SUBSTITUTE_67
	.type	UCNV_TO_U_CALLBACK_SUBSTITUTE_67, @function
UCNV_TO_U_CALLBACK_SUBSTITUTE_67:
.LFB2119:
	.cfi_startproc
	endbr64
	movq	%rsi, %r10
	cmpl	$2, %r8d
	jg	.L95
	testq	%rdi, %rdi
	je	.L97
	cmpb	$105, (%rdi)
	jne	.L95
	testl	%r8d, %r8d
	je	.L97
.L95:
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$0, (%r9)
	movq	%r9, %rdx
	xorl	%esi, %esi
	movq	%r10, %rdi
	jmp	ucnv_cbToUWriteSub_67@PLT
	.cfi_endproc
.LFE2119:
	.size	UCNV_TO_U_CALLBACK_SUBSTITUTE_67, .-UCNV_TO_U_CALLBACK_SUBSTITUTE_67
	.p2align 4
	.globl	UCNV_TO_U_CALLBACK_ESCAPE_67
	.type	UCNV_TO_U_CALLBACK_ESCAPE_67, @function
UCNV_TO_U_CALLBACK_ESCAPE_67:
.LFB2120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, %r8d
	jg	.L107
	movq	%rsi, %r13
	movq	%rdx, %r14
	movl	%ecx, %r12d
	movq	%r9, %rbx
	testq	%rdi, %rdi
	je	.L130
	movzbl	(%rdi), %eax
	cmpb	$68, %al
	je	.L113
	cmpb	$88, %al
	je	.L114
	cmpb	$67, %al
	je	.L131
	testl	%ecx, %ecx
	jle	.L126
	leal	-1(%rcx), %eax
	leaq	-156(%rbp), %rdi
	movl	$46, %r15d
	leaq	1(%rdx,%rax), %rax
	movq	%rax, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$37, %eax
	movl	$88, %edx
	movl	%r15d, %esi
	movl	$2, %r8d
	movw	%dx, -2(%rdi)
	movzbl	(%r14), %edx
	movl	$16, %ecx
	addq	$1, %r14
	movw	%ax, -4(%rdi)
	subl	$4, %r15d
	movq	%rdi, -168(%rbp)
	call	uprv_itou_67@PLT
	movq	-168(%rbp), %rdi
	addq	$8, %rdi
	cmpq	%r14, -176(%rbp)
	jne	.L120
	leal	0(,%r12,4), %edx
	leaq	-160(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$0, (%rbx)
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ucnv_cbToUWriteUChars_67@PLT
.L107:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	testl	%ecx, %ecx
	jle	.L126
	leal	-1(%rcx), %eax
	leaq	-160(%rbp), %r12
	leaq	1(%rdx,%rax), %rax
	xorl	%edx, %edx
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L112:
	leal	2(%rdx), %r15d
	movslq	%edx, %rax
	addl	$1, %edx
	movl	$37, %ecx
	movslq	%edx, %rdx
	movl	$88, %esi
	movl	$2, %r8d
	movw	%cx, -160(%rbp,%rax,2)
	movw	%si, -160(%rbp,%rdx,2)
	movslq	%r15d, %rax
	movzbl	(%r14), %edx
	movl	$48, %esi
	subl	%r15d, %esi
	leaq	(%r12,%rax,2), %rdi
	movl	$16, %ecx
	addq	$1, %r14
	call	uprv_itou_67@PLT
	leal	(%r15,%rax), %edx
	cmpq	-168(%rbp), %r14
	jne	.L112
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L126:
	xorl	%edx, %edx
	leaq	-160(%rbp), %r12
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L131:
	testl	%ecx, %ecx
	jle	.L126
	leal	-1(%rcx), %eax
	leaq	-160(%rbp), %r12
	leaq	1(%rdx,%rax), %rax
	xorl	%edx, %edx
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L119:
	leal	2(%rdx), %r15d
	movslq	%edx, %rax
	addl	$1, %edx
	movl	$92, %ecx
	movslq	%edx, %rdx
	movl	$120, %esi
	movl	$2, %r8d
	movw	%cx, -160(%rbp,%rax,2)
	movw	%si, -160(%rbp,%rdx,2)
	movslq	%r15d, %rax
	movzbl	(%r14), %edx
	movl	$48, %esi
	subl	%r15d, %esi
	leaq	(%r12,%rax,2), %rdi
	movl	$16, %ecx
	addq	$1, %r14
	call	uprv_itou_67@PLT
	leal	(%r15,%rax), %edx
	cmpq	-168(%rbp), %r14
	jne	.L119
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L114:
	testl	%ecx, %ecx
	jle	.L126
	leal	-1(%rcx), %eax
	leaq	-160(%rbp), %r12
	leaq	1(%rdx,%rax), %rax
	xorl	%edx, %edx
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L118:
	movslq	%edx, %rax
	movl	$38, %edi
	leal	3(%rdx), %r15d
	addq	$1, %r14
	movl	$35, %r8d
	movl	$120, %r9d
	movl	$48, %esi
	movw	%di, -160(%rbp,%rax,2)
	leal	1(%rdx), %eax
	addl	$2, %edx
	subl	%r15d, %esi
	movl	$16, %ecx
	cltq
	movslq	%edx, %rdx
	movw	%r8w, -160(%rbp,%rax,2)
	movslq	%r15d, %rax
	xorl	%r8d, %r8d
	movw	%r9w, -160(%rbp,%rdx,2)
	movzbl	-1(%r14), %edx
	leaq	(%r12,%rax,2), %rdi
	call	uprv_itou_67@PLT
	movl	$59, %r11d
	leal	(%r15,%rax), %r10d
	leal	1(%r10), %edx
	movslq	%r10d, %r10
	movw	%r11w, -160(%rbp,%r10,2)
	cmpq	%r14, -168(%rbp)
	jne	.L118
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L113:
	testl	%ecx, %ecx
	jle	.L126
	leal	-1(%rcx), %eax
	leaq	-160(%rbp), %r12
	leaq	1(%rdx,%rax), %rax
	xorl	%edx, %edx
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L117:
	movslq	%edx, %rax
	movl	$38, %r15d
	xorl	%r8d, %r8d
	addq	$1, %r14
	movw	%r15w, -160(%rbp,%rax,2)
	leal	2(%rdx), %r15d
	addl	$1, %edx
	movl	$35, %eax
	movslq	%edx, %rdx
	movl	$48, %esi
	movl	$10, %ecx
	movw	%ax, -160(%rbp,%rdx,2)
	movzbl	-1(%r14), %edx
	movslq	%r15d, %rax
	subl	%r15d, %esi
	leaq	(%r12,%rax,2), %rdi
	call	uprv_itou_67@PLT
	leal	(%r15,%rax), %r10d
	movl	$59, %eax
	leal	1(%r10), %edx
	movslq	%r10d, %r10
	movw	%ax, -160(%rbp,%r10,2)
	cmpq	%r14, -168(%rbp)
	jne	.L117
	jmp	.L111
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2120:
	.size	UCNV_TO_U_CALLBACK_ESCAPE_67, .-UCNV_TO_U_CALLBACK_ESCAPE_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
