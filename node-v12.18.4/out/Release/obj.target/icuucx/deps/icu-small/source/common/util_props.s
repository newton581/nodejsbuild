	.file	"util_props.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility12parseIntegerERKNS_13UnicodeStringERii
	.type	_ZN6icu_6711ICU_Utility12parseIntegerERKNS_13UnicodeStringERii, @function
_ZN6icu_6711ICU_Utility12parseIntegerERKNS_13UnicodeStringERii:
.LFB1301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -52(%rbp)
	movl	(%rsi), %edx
	movq	%rsi, -72(%rbp)
	cmpl	%eax, %edx
	jge	.L16
	movzwl	8(%rdi), %eax
	movq	%rdi, %r12
	testw	%ax, %ax
	js	.L4
	movswl	%ax, %edi
	sarl	$5, %edi
	cmpl	%edx, %edi
	jbe	.L18
.L32:
	testb	$2, %al
	jne	.L29
	movq	24(%r12), %rax
.L8:
	movslq	%edx, %r9
	xorl	%ecx, %ecx
	movl	$10, %r13d
	cmpw	$48, (%rax,%r9,2)
	leaq	(%r9,%r9), %r8
	je	.L30
.L6:
	subl	%edx, %ecx
	leaq	(%r9,%r9), %rbx
	movl	%edx, %r14d
	xorl	%r15d, %r15d
	movl	%ecx, -60(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L31:
	movswl	%ax, %edi
	sarl	$5, %edi
.L12:
	movl	$65535, %r10d
	cmpl	%edi, %r14d
	jnb	.L13
	testb	$2, %al
	je	.L14
	leaq	10(%r12), %rax
.L15:
	movzwl	(%rax,%rbx), %r10d
.L13:
	movl	%r13d, %esi
	movl	%r10d, %edi
	call	u_digit_67@PLT
	testl	%eax, %eax
	js	.L10
	movl	%r15d, %edi
	addl	$1, %r14d
	addq	$2, %rbx
	imull	%r13d, %edi
	addl	%edi, %eax
	cmpl	%eax, %r15d
	jge	.L16
	movl	%eax, %r15d
.L17:
	movl	-60(%rbp), %eax
	movl	%r14d, -56(%rbp)
	addl	%r14d, %eax
	movl	%eax, -64(%rbp)
	cmpl	-52(%rbp), %r14d
	jge	.L10
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	jns	.L31
	movl	12(%r12), %edi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%r15d, %r15d
.L1:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movl	12(%rdi), %edi
	cmpl	%edx, %edi
	ja	.L32
.L18:
	movl	$10, %r13d
	xorl	%ecx, %ecx
	movslq	%edx, %r9
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	10(%r12), %rax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L14:
	movq	24(%r12), %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L30:
	leal	1(%rdx), %ebx
	movl	%ebx, -56(%rbp)
	cmpl	-52(%rbp), %ebx
	jge	.L20
	cmpl	%ebx, %edi
	jbe	.L22
	movzwl	2(%rax,%r8), %eax
	andl	$-33, %eax
	cmpw	$88, %ax
	je	.L33
.L22:
	movslq	%ebx, %r9
	movl	$8, %r13d
	movl	$1, %ecx
	movq	%r9, %rdx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L10:
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	je	.L1
.L9:
	movq	-72(%rbp), %rax
	movl	-56(%rbp), %ecx
	movl	%ecx, (%rax)
	jmp	.L1
.L20:
	xorl	%r15d, %r15d
	jmp	.L9
.L33:
	addl	$2, %edx
	movl	$16, %r13d
	movslq	%edx, %r9
	jmp	.L6
	.cfi_endproc
.LFE1301:
	.size	_ZN6icu_6711ICU_Utility12parseIntegerERKNS_13UnicodeStringERii, .-_ZN6icu_6711ICU_Utility12parseIntegerERKNS_13UnicodeStringERii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi
	.type	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi, @function
_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi:
.LFB1302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movl	%esi, -68(%rbp)
	movl	%edx, -72(%rbp)
	movq	%r8, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L70:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%r14d, %edx
	jle	.L37
.L71:
	jbe	.L59
	testb	$2, %al
	je	.L39
	leaq	10(%rbx), %rax
	movzwl	(%rax,%r14,2), %r15d
	cmpw	$35, %r15w
	je	.L41
.L72:
	cmpw	$126, %r15w
	je	.L42
	cmpw	$32, %r15w
	je	.L69
	movslq	-68(%rbp), %rax
	cmpl	-72(%rbp), %eax
	jge	.L50
.L73:
	leal	1(%rax), %edx
	movl	%edx, -68(%rbp)
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L52
	movswl	%dx, %esi
	sarl	$5, %esi
.L53:
	movl	$65535, %edi
	cmpl	%eax, %esi
	jbe	.L54
	andl	$2, %edx
	leaq	10(%r12), %rdx
	jne	.L56
	movq	24(%r12), %rdx
.L56:
	movzwl	(%rdx,%rax,2), %edi
.L54:
	call	u_tolower_67@PLT
	cmpw	%ax, %r15w
	jne	.L50
.L51:
	addq	$1, %r14
.L57:
	movzwl	8(%rbx), %eax
	testw	%ax, %ax
	jns	.L70
	movl	12(%rbx), %edx
	cmpl	%r14d, %edx
	jg	.L71
.L37:
	movl	-68(%rbp), %eax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L39:
	movq	24(%rbx), %rax
	movzwl	(%rax,%r14,2), %r15d
	cmpw	$35, %r15w
	jne	.L72
.L41:
	movl	-68(%rbp), %eax
	movl	-72(%rbp), %edx
	leaq	-60(%rbp), %rsi
	movq	%r12, %rdi
	leal	1(%r13), %r15d
	movl	%eax, -60(%rbp)
	movq	-80(%rbp), %rax
	leaq	(%rax,%r13,4), %r13
	call	_ZN6icu_6711ICU_Utility12parseIntegerERKNS_13UnicodeStringERii
	movl	%eax, 0(%r13)
	movl	-60(%rbp), %eax
	cmpl	-68(%rbp), %eax
	je	.L50
	movl	%eax, -68(%rbp)
	movslq	%r15d, %r13
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L69:
	movslq	-68(%rbp), %rax
	cmpl	-72(%rbp), %eax
	jge	.L50
	leal	1(%rax), %edx
	movl	%edx, -68(%rbp)
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L45
	movswl	%dx, %esi
	sarl	$5, %esi
.L46:
	movl	$65535, %edi
	cmpl	%eax, %esi
	jbe	.L47
	andl	$2, %edx
	leaq	10(%r12), %rdx
	jne	.L49
	movq	24(%r12), %rdx
.L49:
	movzwl	(%rdx,%rax,2), %edi
.L47:
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	je	.L50
.L42:
	leaq	-68(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia@PLT
	movl	%eax, -68(%rbp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L59:
	movslq	-68(%rbp), %rax
	movl	$-1, %r15d
	cmpl	-72(%rbp), %eax
	jl	.L73
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$-1, %eax
.L34:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L74
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movl	12(%r12), %esi
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L45:
	movl	12(%r12), %esi
	jmp	.L46
.L74:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1302:
	.size	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi, .-_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringEiiS3_Pi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility22parseUnicodeIdentifierERKNS_13UnicodeStringERi
	.type	_ZN6icu_6711ICU_Utility22parseUnicodeIdentifierERKNS_13UnicodeStringERi, @function
_ZN6icu_6711ICU_Utility22parseUnicodeIdentifierERKNS_13UnicodeStringERi:
.LFB1303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	movl	(%rdx), %ebx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L99:
	call	u_isIDStart_67@PLT
	testb	%al, %al
	je	.L82
.L98:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	xorl	%eax, %eax
	cmpl	$65535, %r15d
	seta	%al
	leal	1(%rbx,%rax), %ebx
.L89:
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L76
	sarl	$5, %eax
.L77:
	cmpl	%eax, %ebx
	jge	.L78
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r15d
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L79
	sarl	$5, %eax
.L80:
	movl	%r15d, %edi
	testl	%eax, %eax
	je	.L99
	call	u_isIDPart_67@PLT
	testb	%al, %al
	jne	.L98
.L78:
	movl	%ebx, (%r14)
.L75:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movl	12(%r13), %eax
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L79:
	movl	12(%r12), %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L82:
	movzwl	8(%r12), %eax
	testb	$1, %al
	jne	.L100
	testw	%ax, %ax
	js	.L86
	movswl	%ax, %edx
	sarl	$5, %edx
.L87:
	testl	%edx, %edx
	je	.L75
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L75
.L100:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L75
.L86:
	movl	12(%r12), %edx
	jmp	.L87
	.cfi_endproc
.LFE1303:
	.size	_ZN6icu_6711ICU_Utility22parseUnicodeIdentifierERKNS_13UnicodeStringERi, .-_ZN6icu_6711ICU_Utility22parseUnicodeIdentifierERKNS_13UnicodeStringERi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility11parseNumberERKNS_13UnicodeStringERia
	.type	_ZN6icu_6711ICU_Utility11parseNumberERKNS_13UnicodeStringERia, @function
_ZN6icu_6711ICU_Utility11parseNumberERKNS_13UnicodeStringERia:
.LFB1304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movsbl	%dl, %ebx
	subq	$8, %rsp
	movl	(%rsi), %r14d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L110:
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L104
.L111:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%ebx, %esi
	movl	%eax, %edi
	call	u_digit_67@PLT
	testl	%eax, %eax
	js	.L104
	imull	%ebx, %r13d
	addl	%eax, %r13d
	js	.L108
	addl	$1, %r14d
.L107:
	movswl	8(%r15), %eax
	testw	%ax, %ax
	jns	.L110
	movl	12(%r15), %eax
	cmpl	%eax, %r14d
	jl	.L111
.L104:
	cmpl	%r14d, (%r12)
	je	.L108
	movl	%r14d, (%r12)
.L101:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movl	$-1, %r13d
	jmp	.L101
	.cfi_endproc
.LFE1304:
	.size	_ZN6icu_6711ICU_Utility11parseNumberERKNS_13UnicodeStringERia, .-_ZN6icu_6711ICU_Utility11parseNumberERKNS_13UnicodeStringERia
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
