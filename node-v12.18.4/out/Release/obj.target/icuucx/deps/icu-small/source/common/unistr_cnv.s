	.file	"unistr_cnv.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode.part.0, @function
_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode.part.0:
.LFB3011:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movslq	%edx, %r12
	pushq	%rbx
	addq	%rsi, %r12
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -72(%rbp)
	movl	%edx, %esi
	movl	$27, %eax
	sarl	$2, %esi
	addl	%edx, %esi
	cmpl	$27, %edx
	cmovle	%eax, %esi
	leaq	10(%rdi), %rax
	xorl	%ecx, %ecx
	movq	%rax, -88(%rbp)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L22:
	movq	-88(%rbp), %r14
	testw	%ax, %ax
	js	.L7
.L23:
	sarl	$5, %eax
.L8:
	cltq
	movl	$54, %edx
	leaq	(%r14,%rax,2), %rax
	movq	%rax, -64(%rbp)
	testw	%cx, %cx
	jne	.L9
	movslq	16(%r15), %rdx
	addq	%rdx, %rdx
.L9:
	pushq	%rbx
	leaq	-72(%rbp), %rcx
	addq	%r14, %rdx
	leaq	-64(%rbp), %rsi
	pushq	$1
	xorl	%r9d, %r9d
	movq	%r12, %r8
	movq	%r13, %rdi
	call	ucnv_toUnicode_67@PLT
	movq	-64(%rbp), %rax
	popq	%rdx
	popq	%rcx
	subq	%r14, %rax
	sarq	%rax
	cmpl	$1023, %eax
	jg	.L10
	movzwl	8(%r15), %edx
	sall	$5, %eax
	andl	$31, %edx
	orl	%edx, %eax
	cmpl	$15, (%rbx)
	movw	%ax, 8(%r15)
	jne	.L1
	movl	$0, (%rbx)
	testw	%ax, %ax
	js	.L13
	cwtl
	sarl	$5, %eax
.L14:
	movq	%r12, %rdx
	subq	-72(%rbp), %rdx
	movl	$1, %ecx
	leal	(%rax,%rdx,2), %esi
.L15:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%esi, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia@PLT
	testb	%al, %al
	je	.L21
	movswl	8(%r15), %eax
	movl	%eax, %ecx
	andw	$2, %cx
	jne	.L22
	movq	24(%r15), %r14
	testw	%ax, %ax
	jns	.L23
.L7:
	movl	12(%r15), %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	orw	$-32, 8(%r15)
	cmpl	$15, (%rbx)
	movl	%eax, 12(%r15)
	jne	.L1
	movl	$0, (%rbx)
.L13:
	movl	12(%r15), %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L24:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3011:
	.size	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode.part.0, .-_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EPKcS2_
	.type	_ZN6icu_6713UnicodeStringC2EPKcS2_, @function
_ZN6icu_6713UnicodeStringC2EPKcS2_:
.LFB2343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, 8(%rdi)
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L25
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	%rsi, %r13
	movq	%rdx, %r14
	call	strlen@PLT
	movq	%rax, %rbx
	cmpl	$-1, %eax
	jl	.L25
	testl	%eax, %eax
	je	.L25
	movl	$0, -60(%rbp)
	testq	%r14, %r14
	je	.L50
	cmpb	$0, (%r14)
	je	.L51
	leaq	-60(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	movq	%r8, -72(%rbp)
	call	ucnv_open_67@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r15
.L34:
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jg	.L36
	movq	%r15, %rcx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode.part.0
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L40
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L40:
	movq	%r15, %rdi
	testq	%r14, %r14
	je	.L52
	call	ucnv_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%eax, %edx
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia@PLT
	testb	%al, %al
	je	.L36
	leaq	10(%r12), %rsi
	testb	$2, 8(%r12)
	jne	.L38
	movq	24(%r12), %rsi
.L38:
	movl	%ebx, %edx
	movq	%r13, %rdi
	call	u_charsToUChars_67@PLT
	cmpl	$1023, %ebx
	jg	.L39
	movzwl	8(%r12), %edx
	sall	$5, %ebx
	andl	$31, %edx
	orl	%edx, %ebx
	movw	%bx, 8(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L50:
	call	ucnv_getDefaultName_67@PLT
	movzbl	(%rax), %edx
	cmpb	$85, %dl
	je	.L54
	cmpb	$117, %dl
	je	.L55
.L30:
	leaq	-60(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -72(%rbp)
	call	u_getDefaultConverter_67@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r15
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L52:
	call	u_releaseDefaultConverter_67@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L39:
	orw	$-32, 8(%r12)
	movl	%ebx, 12(%r12)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L54:
	cmpb	$84, 1(%rax)
	jne	.L30
	cmpb	$70, 2(%rax)
	jne	.L30
.L31:
	movzbl	3(%rax), %edx
	cmpb	$45, %dl
	je	.L56
	cmpb	$56, %dl
	jne	.L30
	cmpb	$0, 4(%rax)
	jne	.L30
.L33:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9setToUTF8ENS_11StringPieceE@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L55:
	cmpb	$116, 1(%rax)
	jne	.L30
	cmpb	$102, 2(%rax)
	jne	.L30
	jmp	.L31
.L56:
	cmpb	$56, 4(%rax)
	jne	.L30
	cmpb	$0, 5(%rax)
	jne	.L30
	jmp	.L33
.L53:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2343:
	.size	_ZN6icu_6713UnicodeStringC2EPKcS2_, .-_ZN6icu_6713UnicodeStringC2EPKcS2_
	.globl	_ZN6icu_6713UnicodeStringC1EPKcS2_
	.set	_ZN6icu_6713UnicodeStringC1EPKcS2_,_ZN6icu_6713UnicodeStringC2EPKcS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EPKciS2_
	.type	_ZN6icu_6713UnicodeStringC2EPKciS2_, @function
_ZN6icu_6713UnicodeStringC2EPKciS2_:
.LFB2346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r8w, 8(%rdi)
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L57
	movl	%edx, %r13d
	cmpl	$-1, %edx
	jl	.L57
	testl	%edx, %edx
	je	.L57
	movq	%rdi, %r12
	movq	%rsi, %r14
	movq	%rcx, %rbx
	cmpl	$-1, %edx
	je	.L83
.L60:
	movl	$0, -60(%rbp)
	testq	%rbx, %rbx
	je	.L84
	cmpb	$0, (%rbx)
	je	.L85
	leaq	-60(%rbp), %r8
	movq	%rbx, %rdi
	movq	%r8, %rsi
	movq	%r8, -72(%rbp)
	call	ucnv_open_67@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r15
.L67:
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jg	.L69
	movq	%r15, %rcx
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode.part.0
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L73
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L73:
	movq	%r15, %rdi
	testq	%rbx, %rbx
	je	.L86
	call	ucnv_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %r13d
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L85:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia@PLT
	testb	%al, %al
	je	.L69
	leaq	10(%r12), %rsi
	testb	$2, 8(%r12)
	jne	.L71
	movq	24(%r12), %rsi
.L71:
	movl	%r13d, %edx
	movq	%r14, %rdi
	call	u_charsToUChars_67@PLT
	cmpl	$1023, %r13d
	jg	.L72
	movzwl	8(%r12), %ecx
	sall	$5, %r13d
	andl	$31, %ecx
	orl	%r13d, %ecx
	movw	%cx, 8(%r12)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L84:
	call	ucnv_getDefaultName_67@PLT
	movzbl	(%rax), %edx
	cmpb	$85, %dl
	je	.L88
	cmpb	$117, %dl
	je	.L89
.L63:
	leaq	-60(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -72(%rbp)
	call	u_getDefaultConverter_67@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r15
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L86:
	call	u_releaseDefaultConverter_67@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L72:
	orw	$-32, 8(%r12)
	movl	%r13d, 12(%r12)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L88:
	cmpb	$84, 1(%rax)
	jne	.L63
	cmpb	$70, 2(%rax)
	jne	.L63
.L64:
	movzbl	3(%rax), %edx
	cmpb	$45, %dl
	je	.L90
	cmpb	$56, %dl
	jne	.L63
	cmpb	$0, 4(%rax)
	jne	.L63
.L66:
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9setToUTF8ENS_11StringPieceE@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L89:
	cmpb	$116, 1(%rax)
	jne	.L63
	cmpb	$102, 2(%rax)
	jne	.L63
	jmp	.L64
.L90:
	cmpb	$56, 4(%rax)
	jne	.L63
	cmpb	$0, 5(%rax)
	jne	.L63
	jmp	.L66
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2346:
	.size	_ZN6icu_6713UnicodeStringC2EPKciS2_, .-_ZN6icu_6713UnicodeStringC2EPKciS2_
	.globl	_ZN6icu_6713UnicodeStringC1EPKciS2_
	.set	_ZN6icu_6713UnicodeStringC1EPKciS2_,_ZN6icu_6713UnicodeStringC2EPKciS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EPKciP10UConverterR10UErrorCode
	.type	_ZN6icu_6713UnicodeStringC2EPKciP10UConverterR10UErrorCode, @function
_ZN6icu_6713UnicodeStringC2EPKciP10UConverterR10UErrorCode:
.LFB2349:
	.cfi_startproc
	endbr64
	movl	(%r8), %r10d
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r9d
	movq	%rax, (%rdi)
	movw	%r9w, 8(%rdi)
	testl	%r10d, %r10d
	jle	.L108
.L105:
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	testq	%rsi, %rsi
	je	.L105
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%r8, %rbx
	subq	$40, %rsp
	cmpl	$-1, %edx
	jl	.L109
	jne	.L97
	movq	%rsi, %rdi
	movq	%rcx, -48(%rbp)
	movq	%rsi, -40(%rbp)
	call	strlen@PLT
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rsi
	movl	%eax, %edx
.L97:
	testl	%edx, %edx
	jle	.L91
	testq	%rcx, %rcx
	je	.L98
	movq	%rcx, %rdi
	movl	%edx, -52(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rcx, -40(%rbp)
	call	ucnv_resetToUnicode_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L96
	movq	-40(%rbp), %rcx
	movl	-52(%rbp), %edx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movq	-48(%rbp), %rsi
	call	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode.part.0
	movl	(%rbx), %eax
.L99:
	testl	%eax, %eax
	jg	.L96
.L91:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movl	$1, (%r8)
.L96:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	%rbx, %rdi
	movl	%edx, -48(%rbp)
	movq	%rsi, -40(%rbp)
	call	u_getDefaultConverter_67@PLT
	movq	%rax, %r13
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L100
	movl	-48(%rbp), %edx
	movq	-40(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode.part.0
.L100:
	movq	%r13, %rdi
	call	u_releaseDefaultConverter_67@PLT
	movl	(%rbx), %eax
	jmp	.L99
	.cfi_endproc
.LFE2349:
	.size	_ZN6icu_6713UnicodeStringC2EPKciP10UConverterR10UErrorCode, .-_ZN6icu_6713UnicodeStringC2EPKciP10UConverterR10UErrorCode
	.globl	_ZN6icu_6713UnicodeStringC1EPKciP10UConverterR10UErrorCode
	.set	_ZN6icu_6713UnicodeStringC1EPKciP10UConverterR10UErrorCode,_ZN6icu_6713UnicodeStringC2EPKciP10UConverterR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString9doExtractEiiPciP10UConverterR10UErrorCode
	.type	_ZNK6icu_6713UnicodeString9doExtractEiiPciP10UConverterR10UErrorCode, @function
_ZNK6icu_6713UnicodeString9doExtractEiiPciP10UConverterR10UErrorCode:
.LFB2353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1112, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	movq	%rcx, -1112(%rbp)
	movl	%r8d, -1132(%rbp)
	movl	(%r15), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L125
	movq	%r9, %r12
	testb	$2, 8(%rdi)
	jne	.L126
	movq	24(%rdi), %rdi
	movq	%rcx, %rax
.L114:
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	movq	%rax, -1144(%rbp)
	leaq	(%rdi,%rsi,2), %rcx
	movl	-1132(%rbp), %edi
	movq	%rcx, -1096(%rbp)
	leaq	(%rcx,%rdx,2), %rbx
	testl	%edi, %edi
	jne	.L115
	movq	$0, -1112(%rbp)
	xorl	%edx, %edx
.L116:
	pushq	%r15
	leaq	-1096(%rbp), %r14
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	pushq	$1
	leaq	-1112(%rbp), %r13
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	ucnv_fromUnicode_67@PLT
	movl	-1112(%rbp), %r10d
	popq	%rcx
	subl	-1144(%rbp), %r10d
	cmpl	$15, (%r15)
	popq	%rsi
	je	.L127
.L118:
	movl	-1132(%rbp), %esi
	movq	-1144(%rbp), %rdi
	movq	%r15, %rcx
	movl	%r10d, %edx
	call	u_terminateChars_67@PLT
.L110:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L128
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	addq	$10, %rdi
	movq	%rcx, %rax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L125:
	testl	%r8d, %r8d
	je	.L110
	movb	$0, (%rcx)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L115:
	movslq	-1132(%rbp), %rdx
	movq	%rdx, %rax
	addq	-1144(%rbp), %rdx
	cmpl	$-1, %eax
	jne	.L116
	movq	-1144(%rbp), %rax
	movq	$-2147483648, %rdx
	movl	$2147483647, -1132(%rbp)
	cmpq	$-2147483648, %rax
	cmovbe	%rax, %rdx
	addq	$2147483647, %rdx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	-1088(%rbp), %rax
	movq	%rbx, %r8
	movq	%r15, %rbx
	movq	%r14, %r15
	movq	%rax, -1120(%rbp)
	leaq	-64(%rbp), %rax
	movl	%r10d, %r14d
	movq	%rax, -1152(%rbp)
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$0, (%rbx)
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	movq	%r13, %rsi
	pushq	%rbx
	movq	-1120(%rbp), %rax
	movq	%r12, %rdi
	pushq	$1
	movq	-1152(%rbp), %rdx
	movq	%rax, -1112(%rbp)
	movq	%r8, -1128(%rbp)
	call	ucnv_fromUnicode_67@PLT
	movl	-1112(%rbp), %r10d
	popq	%rax
	movq	-1128(%rbp), %r8
	popq	%rdx
	addl	%r14d, %r10d
	movl	%r10d, %r14d
	subl	-1120(%rbp), %r14d
	cmpl	$15, (%rbx)
	je	.L119
	movl	%r14d, %r10d
	movq	%rbx, %r15
	jmp	.L118
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2353:
	.size	_ZNK6icu_6713UnicodeString9doExtractEiiPciP10UConverterR10UErrorCode, .-_ZNK6icu_6713UnicodeString9doExtractEiiPciP10UConverterR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString7extractEiiPcjPKc
	.type	_ZNK6icu_6713UnicodeString7extractEiiPcjPKc, @function
_ZNK6icu_6713UnicodeString7extractEiiPcjPKc:
.LFB2351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L152
	testq	%rcx, %rcx
	jne	.L152
.L129:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L157
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movzwl	8(%r14), %ecx
	testw	%cx, %cx
	js	.L132
	movswl	%cx, %eax
	sarl	$5, %eax
.L133:
	xorl	%r15d, %r15d
	testl	%esi, %esi
	js	.L134
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	movslq	%esi, %r15
.L134:
	xorl	%ebx, %ebx
	testl	%edx, %edx
	js	.L135
	subl	%r15d, %eax
	movl	%edx, %ebx
	cmpl	%edx, %eax
	cmovle	%eax, %ebx
.L135:
	movl	%r8d, %r12d
	cmpl	$2147483646, %r8d
	jbe	.L137
	cmpq	$-2147483648, %r13
	movq	$-2147483648, %r8
	cmovbe	%r13, %r8
	addq	$2147483647, %r8
	movl	%r8d, %r12d
	subl	%r13d, %r12d
.L137:
	movl	$0, -60(%rbp)
	testl	%ebx, %ebx
	je	.L158
	testq	%r9, %r9
	je	.L159
	cmpb	$0, (%r9)
	jne	.L145
	cmpl	%ebx, %r12d
	movl	%ebx, %edx
	cmovle	%r12d, %edx
	andl	$2, %ecx
	jne	.L160
	movq	24(%r14), %r14
.L147:
	leaq	(%r14,%r15,2), %rdi
	movq	%r13, %rsi
	call	u_UCharsToChars_67@PLT
	leaq	-60(%rbp), %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	u_terminateChars_67@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L132:
	movl	12(%r14), %eax
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	-60(%rbp), %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	u_terminateChars_67@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L160:
	addq	$10, %r14
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	-60(%rbp), %rdx
	movq	%r9, %rdi
	movq	%rdx, %rsi
	movq	%rdx, -72(%rbp)
	call	ucnv_open_67@PLT
	movq	-72(%rbp), %rdx
	subq	$8, %rsp
	movl	%r12d, %r8d
	movq	%rax, %r9
	movq	%r13, %rcx
	movl	%r15d, %esi
	movq	%r14, %rdi
	pushq	%rdx
	movl	%ebx, %edx
	movq	%rax, -80(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPciP10UConverterR10UErrorCode
	movq	-80(%rbp), %r9
	movl	%eax, -72(%rbp)
	movq	%r9, %rdi
	call	ucnv_close_67@PLT
	popq	%rax
	movl	-72(%rbp), %eax
	popq	%rdx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L159:
	call	ucnv_getDefaultName_67@PLT
	movzbl	(%rax), %edx
	cmpb	$85, %dl
	je	.L161
	cmpb	$117, %dl
	je	.L162
.L141:
	leaq	-60(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	call	u_getDefaultConverter_67@PLT
	movq	-72(%rbp), %rdx
	subq	$8, %rsp
	movq	%r13, %rcx
	movl	%r15d, %esi
	movq	%rax, %r9
	movl	%r12d, %r8d
	movq	%r14, %rdi
	pushq	%rdx
	movl	%ebx, %edx
	movq	%rax, -80(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPciP10UConverterR10UErrorCode
	movq	-80(%rbp), %r9
	movl	%eax, -72(%rbp)
	movq	%r9, %rdi
	call	u_releaseDefaultConverter_67@PLT
	popq	%rcx
	movl	-72(%rbp), %eax
	popq	%rsi
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L162:
	cmpb	$116, 1(%rax)
	jne	.L141
	cmpb	$102, 2(%rax)
	jne	.L141
.L142:
	movzbl	3(%rax), %edx
	cmpb	$45, %dl
	je	.L163
	cmpb	$56, %dl
	jne	.L141
	cmpb	$0, 4(%rax)
	jne	.L141
.L144:
	movl	%r12d, %r8d
	movq	%r13, %rcx
	movl	%ebx, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString6toUTF8EiiPci@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L161:
	cmpb	$84, 1(%rax)
	jne	.L141
	cmpb	$70, 2(%rax)
	jne	.L141
	jmp	.L142
.L163:
	cmpb	$56, 4(%rax)
	jne	.L141
	cmpb	$0, 5(%rax)
	je	.L144
	jmp	.L141
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2351:
	.size	_ZNK6icu_6713UnicodeString7extractEiiPcjPKc, .-_ZNK6icu_6713UnicodeString7extractEiiPcjPKc
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString7extractEPciP10UConverterR10UErrorCode
	.type	_ZNK6icu_6713UnicodeString7extractEPciP10UConverterR10UErrorCode, @function
_ZNK6icu_6713UnicodeString7extractEPciP10UConverterR10UErrorCode:
.LFB2352:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L188
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movswl	8(%rdi), %eax
	testl	%edx, %edx
	js	.L167
	testb	$1, %al
	jne	.L167
	movq	%rsi, %rbx
	movq	%rcx, %r15
	testl	%edx, %edx
	jle	.L176
	testq	%rsi, %rsi
	je	.L167
.L176:
	shrl	$5, %eax
	je	.L189
	testq	%r15, %r15
	je	.L190
	movq	%r15, %rdi
	movq	%r8, -40(%rbp)
	call	ucnv_resetFromUnicode_67@PLT
	movq	-40(%rbp), %r8
	xorl	%r10d, %r10d
.L173:
	movzwl	8(%r14), %eax
	testw	%ax, %ax
	js	.L174
	movswl	%ax, %edx
	sarl	$5, %edx
.L175:
	subq	$8, %rsp
	movq	%rbx, %rcx
	movq	%r15, %r9
	xorl	%esi, %esi
	pushq	%r8
	movq	%r14, %rdi
	movl	%r13d, %r8d
	movb	%r10b, -40(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPciP10UConverterR10UErrorCode
	movzbl	-40(%rbp), %r10d
	popq	%rdx
	popq	%rcx
	testb	%r10b, %r10b
	je	.L164
	movq	%r15, %rdi
	movl	%eax, -40(%rbp)
	call	u_releaseDefaultConverter_67@PLT
	movl	-40(%rbp), %eax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$1, (%r8)
.L187:
	xorl	%eax, %eax
.L164:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-32(%rbp), %rsp
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	%r8, %rcx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	xorl	%edx, %edx
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	u_terminateChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movl	12(%r14), %edx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r8, %rdi
	movq	%r8, -40(%rbp)
	call	u_getDefaultConverter_67@PLT
	movq	-40(%rbp), %r8
	movl	$1, %r10d
	movq	%rax, %r15
	movl	(%r8), %esi
	testl	%esi, %esi
	jle	.L173
	jmp	.L187
	.cfi_endproc
.LFE2352:
	.size	_ZNK6icu_6713UnicodeString7extractEPciP10UConverterR10UErrorCode, .-_ZNK6icu_6713UnicodeString7extractEPciP10UConverterR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciS2_
	.type	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciS2_, @function
_ZN6icu_6713UnicodeString16doCodepageCreateEPKciS2_:
.LFB2354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%dl
	cmpl	$-1, %r12d
	setl	%al
	orb	%al, %dl
	jne	.L191
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L191
	movq	%rdi, %r14
	movq	%rcx, %rbx
	cmpl	$-1, %r12d
	jne	.L193
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %r12d
.L193:
	movl	$0, -60(%rbp)
	testq	%rbx, %rbx
	je	.L213
	cmpb	$0, (%rbx)
	je	.L214
	leaq	-60(%rbp), %r8
	movq	%rbx, %rdi
	movq	%r8, %rsi
	movq	%r8, -72(%rbp)
	call	ucnv_open_67@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r15
.L200:
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jg	.L202
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode.part.0
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L215
.L206:
	movq	%r15, %rdi
	testq	%rbx, %rbx
	je	.L216
	call	ucnv_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L191:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L216:
	call	u_releaseDefaultConverter_67@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L214:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%r12d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia@PLT
	testb	%al, %al
	je	.L202
	leaq	10(%r14), %rsi
	testb	$2, 8(%r14)
	jne	.L204
	movq	24(%r14), %rsi
.L204:
	movl	%r12d, %edx
	movq	%r13, %rdi
	call	u_charsToUChars_67@PLT
	cmpl	$1023, %r12d
	jg	.L205
	movzwl	8(%r14), %ecx
	sall	$5, %r12d
	andl	$31, %ecx
	orl	%r12d, %ecx
	movw	%cx, 8(%r14)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L213:
	call	ucnv_getDefaultName_67@PLT
	movzbl	(%rax), %edx
	cmpb	$85, %dl
	je	.L218
	cmpb	$117, %dl
	je	.L219
.L196:
	leaq	-60(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -72(%rbp)
	call	u_getDefaultConverter_67@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r15
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L205:
	orw	$-32, 8(%r14)
	movl	%r12d, 12(%r14)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L219:
	cmpb	$116, 1(%rax)
	jne	.L196
	cmpb	$102, 2(%rax)
	jne	.L196
.L197:
	movzbl	3(%rax), %edx
	cmpb	$45, %dl
	je	.L220
	cmpb	$56, %dl
	jne	.L196
	cmpb	$0, 4(%rax)
	jne	.L196
.L199:
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9setToUTF8ENS_11StringPieceE@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L218:
	cmpb	$84, 1(%rax)
	jne	.L196
	cmpb	$70, 2(%rax)
	jne	.L196
	jmp	.L197
.L220:
	cmpb	$56, 4(%rax)
	jne	.L196
	cmpb	$0, 5(%rax)
	jne	.L196
	jmp	.L199
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2354:
	.size	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciS2_, .-_ZN6icu_6713UnicodeString16doCodepageCreateEPKciS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode
	.type	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode, @function
_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode:
.LFB2355:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L221
	jmp	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L221:
	ret
	.cfi_endproc
.LFE2355:
	.size	_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode, .-_ZN6icu_6713UnicodeString16doCodepageCreateEPKciP10UConverterR10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
