	.file	"utrace.cpp"
	.text
	.p2align 4
	.type	_ZL10outputCharcPcPiii, @function
_ZL10outputCharcPcPiii:
.LFB2056:
	.cfi_startproc
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L2
	cmpb	$10, %dil
	setne	%r10b
	testb	%dil, %dil
	setne	%r9b
	testb	%r9b, %r10b
	je	.L3
	cmpl	%ecx, %eax
	jl	.L34
.L30:
	cmpb	$10, %dil
	je	.L2
.L6:
	cmpl	%eax, %ecx
	jg	.L35
.L11:
	testb	%dil, %dil
	je	.L1
.L37:
	addl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testl	%r8d, %r8d
	jle	.L6
.L31:
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	%eax, %ecx
	jle	.L7
.L36:
	cltq
	addl	$1, %r9d
	movb	$32, (%rsi,%rax)
	movl	(%rdx), %eax
	addl	$1, %eax
	movl	%eax, (%rdx)
	cmpl	%r8d, %r9d
	jge	.L6
	cmpl	%eax, %ecx
	jg	.L36
.L7:
	addl	$1, %eax
	addl	$1, %r9d
	movl	%eax, (%rdx)
	cmpl	%r9d, %r8d
	jg	.L10
	cmpl	%eax, %ecx
	jle	.L11
.L35:
	movslq	%eax, %r9
.L13:
	movb	%dil, (%rsi,%r9)
	testb	%dil, %dil
	jne	.L37
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	%ecx, %eax
	jl	.L6
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L34:
	movslq	%eax, %r9
	cmpb	$10, -1(%rsi,%r9)
	jne	.L13
	testl	%r8d, %r8d
	jg	.L31
	jmp	.L13
	.cfi_endproc
.LFE2056:
	.size	_ZL10outputCharcPcPiii, .-_ZL10outputCharcPcPiii
	.p2align 4
	.type	_ZL10outputCharcPcPiii.constprop.1, @function
_ZL10outputCharcPcPiii.constprop.1:
.LFB2530:
	.cfi_startproc
	movslq	(%rdx), %rax
	cmpl	%ecx, %eax
	jge	.L39
	movb	%dil, (%rsi,%rax)
.L39:
	testb	%dil, %dil
	je	.L38
	addl	$1, (%rdx)
.L38:
	ret
	.cfi_endproc
.LFE2530:
	.size	_ZL10outputCharcPcPiii.constprop.1, .-_ZL10outputCharcPcPiii.constprop.1
	.p2align 4
	.type	_ZL14outputPtrBytesPvPcPii, @function
_ZL14outputPtrBytesPvPcPii:
.LFB2058:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZL14outputHexBytesliPcPiiE9gHexChars(%rip), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	leaq	-17(%rbp), %r8
	leaq	-25(%rbp), %r11
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, -24(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L47:
	andl	$15, %edi
	movzbl	(%r10,%rdi), %edi
	testl	%eax, %eax
	je	.L48
	cmpb	$10, %dil
	setne	%bl
	testb	%dil, %dil
	setne	%r9b
	testb	%r9b, %bl
	je	.L56
	cmpl	%eax, %ecx
	jg	.L49
.L56:
	cmpb	$10, %dil
	je	.L77
.L48:
	cmpl	%eax, %ecx
	jg	.L49
	testb	%dil, %dil
	jne	.L78
.L53:
	subq	$1, %r8
	cmpq	%r11, %r8
	je	.L79
.L54:
	movzbl	(%r8), %edi
	movq	%rdi, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movzbl	(%r10,%rax), %r9d
	movslq	(%rdx), %rax
	testl	%eax, %eax
	je	.L42
	testb	%r9b, %r9b
	setne	%r12b
	cmpb	$10, %r9b
	setne	%bl
	testb	%bl, %r12b
	je	.L55
	cmpl	%eax, %ecx
	jg	.L43
.L55:
	cmpb	$10, %r9b
	je	.L80
.L42:
	cmpl	%eax, %ecx
	jg	.L43
.L46:
	testb	%r9b, %r9b
	je	.L47
.L45:
	addl	$1, %eax
	movl	%eax, (%rdx)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L77:
	cmpl	%eax, %ecx
	jle	.L51
	.p2align 4,,10
	.p2align 3
.L49:
	cltq
	movb	%dil, (%rsi,%rax)
	testb	%dil, %dil
	je	.L53
.L78:
	movl	(%rdx), %eax
.L51:
	addl	$1, %eax
	subq	$1, %r8
	movl	%eax, (%rdx)
	cmpq	%r11, %r8
	jne	.L54
.L79:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	cmpl	%eax, %ecx
	jle	.L45
	.p2align 4,,10
	.p2align 3
.L43:
	movb	%r9b, (%rsi,%rax)
	movl	(%rdx), %eax
	jmp	.L46
	.cfi_endproc
.LFE2058:
	.size	_ZL14outputPtrBytesPvPcPii, .-_ZL14outputPtrBytesPvPcPii
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"*NULL*"
	.text
	.p2align 4
	.type	_ZL13outputUStringPKDsiPcPiii, @function
_ZL13outputUStringPKDsiPcPiii:
.LFB2060:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rcx, %rdx
	movl	%r8d, %ecx
	movl	%r9d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L185
	cmpl	$-1, %esi
	movl	%esi, %ebx
	setne	%r14b
	testl	%esi, %esi
	jg	.L125
	testb	%r14b, %r14b
	jne	.L81
.L125:
	cmpl	$-1, %ebx
	movl	(%rdx), %eax
	movl	$1, %r12d
	leaq	_ZZL14outputHexBytesliPcPiiE9gHexChars(%rip), %rsi
	sete	-50(%rbp)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L113:
	cmpl	%eax, %ecx
	jle	.L116
	movslq	%eax, %r9
	cmpb	$10, -1(%r11,%r9)
	je	.L186
	.p2align 4,,10
	.p2align 3
.L118:
	movb	$32, (%r11,%r9)
	movl	(%rdx), %eax
.L116:
	addl	$1, %eax
	movl	%eax, (%rdx)
	testw	%r10w, %r10w
	jne	.L130
	cmpb	$0, -50(%rbp)
	jne	.L81
.L130:
	cmpl	%r12d, %ebx
	setle	%r9b
	addq	$1, %r12
	testb	%r14b, %r9b
	jne	.L81
.L124:
	movzwl	-2(%rdi,%r12,2), %r9d
	movl	%r9d, %r13d
	movq	%r9, %r10
	shrw	$12, %r13w
	movzwl	%r13w, %r13d
	movzbl	(%rsi,%r13), %r13d
	testl	%eax, %eax
	je	.L86
	cmpb	$10, %r13b
	setne	%r15b
	testb	%r13b, %r13b
	setne	-49(%rbp)
	testb	%r15b, -49(%rbp)
	je	.L126
	cmpl	%eax, %ecx
	jg	.L87
.L126:
	cmpb	$10, %r13b
	je	.L187
.L86:
	cmpl	%eax, %ecx
	jg	.L87
.L90:
	testb	%r13b, %r13b
	jne	.L89
.L91:
	movq	%r9, %r13
	sarq	$8, %r13
	andl	$15, %r13d
	movzbl	(%rsi,%r13), %r13d
	testl	%eax, %eax
	je	.L92
	cmpb	$10, %r13b
	je	.L93
	testb	%r13b, %r13b
	je	.L188
.L93:
	cmpl	%eax, %ecx
	jle	.L95
.L96:
	cltq
	movb	%r13b, (%r11,%rax)
	movl	(%rdx), %eax
	testb	%r13b, %r13b
	jne	.L95
.L98:
	sarq	$4, %r9
	andl	$15, %r9d
	movzbl	(%rsi,%r9), %r9d
	testl	%eax, %eax
	je	.L99
	cmpb	$10, %r9b
	je	.L100
	testb	%r9b, %r9b
	je	.L189
.L100:
	cmpl	%eax, %ecx
	jle	.L102
.L103:
	cltq
	movb	%r9b, (%r11,%rax)
	movl	(%rdx), %eax
	testb	%r9b, %r9b
	jne	.L102
.L105:
	movq	%r10, %r9
	andl	$15, %r9d
	movzbl	(%rsi,%r9), %r9d
	testl	%eax, %eax
	je	.L106
	cmpb	$10, %r9b
	je	.L107
	testb	%r9b, %r9b
	je	.L190
.L107:
	cmpl	%eax, %ecx
	jle	.L109
.L110:
	cltq
	movb	%r9b, (%r11,%rax)
	movl	(%rdx), %eax
	testb	%r9b, %r9b
	jne	.L109
.L112:
	testl	%eax, %eax
	jne	.L113
	testl	%r8d, %r8d
	jle	.L114
.L115:
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L122:
	cmpl	%eax, %ecx
	jle	.L119
	cltq
	movb	$32, (%r11,%rax)
	movl	(%rdx), %eax
.L119:
	addl	$1, %eax
	addl	$1, %r9d
	movl	%eax, (%rdx)
	cmpl	%r9d, %r8d
	jg	.L122
.L114:
	cmpl	%eax, %ecx
	jle	.L116
	movslq	%eax, %r9
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L190:
	cmpb	$10, %r9b
	je	.L107
	.p2align 4,,10
	.p2align 3
.L106:
	cmpl	%eax, %ecx
	jg	.L110
	testb	%r9b, %r9b
	je	.L112
.L109:
	addl	$1, %eax
	movl	%eax, (%rdx)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L189:
	cmpb	$10, %r9b
	je	.L100
	.p2align 4,,10
	.p2align 3
.L99:
	cmpl	%eax, %ecx
	jg	.L103
	testb	%r9b, %r9b
	je	.L105
.L102:
	addl	$1, %eax
	movl	%eax, (%rdx)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L188:
	cmpb	$10, %r13b
	je	.L93
	.p2align 4,,10
	.p2align 3
.L92:
	cmpl	%eax, %ecx
	jg	.L96
	testb	%r13b, %r13b
	je	.L98
.L95:
	addl	$1, %eax
	movl	%eax, (%rdx)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L187:
	cmpl	%eax, %ecx
	jg	.L87
	.p2align 4,,10
	.p2align 3
.L89:
	addl	$1, %eax
	movl	%eax, (%rdx)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L87:
	cltq
	movb	%r13b, (%r11,%rax)
	movl	(%rdx), %eax
	jmp	.L90
.L185:
	leaq	.LC0(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L83:
	movsbl	(%rbx), %edi
	movq	%r11, %rsi
	addq	$1, %rbx
	call	_ZL10outputCharcPcPiii
	testb	%dil, %dil
	jne	.L83
.L81:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L186:
	.cfi_restore_state
	testl	%r8d, %r8d
	jg	.L115
	jmp	.L118
	.cfi_endproc
.LFE2060:
	.size	_ZL13outputUStringPKDsiPcPiii, .-_ZL13outputUStringPKDsiPcPiii
	.p2align 4
	.globl	utrace_entry_67
	.type	utrace_entry_67, @function
utrace_entry_67:
.LFB2053:
	.cfi_startproc
	endbr64
	movq	_ZL15pTraceEntryFunc(%rip), %rax
	movl	%edi, %esi
	testq	%rax, %rax
	je	.L191
	movq	_ZL13gTraceContext(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L191:
	ret
	.cfi_endproc
.LFE2053:
	.size	utrace_entry_67, .-utrace_entry_67
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.globl	utrace_exit_67
	.type	utrace_exit_67, @function
utrace_exit_67:
.LFB2054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rdx, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L194
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L194:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	movq	_ZL14pTraceExitFunc(%rip), %r8
	testq	%r8, %r8
	je	.L193
	cmpl	$18, %esi
	ja	.L196
	leaq	.L198(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L198:
	.long	.L202-.L198
	.long	.L201-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L196-.L198
	.long	.L204-.L198
	.long	.L199-.L198
	.long	.L197-.L198
	.text
.L204:
	leaq	_ZL14gExitFmtStatus(%rip), %rdx
.L200:
	leaq	16(%rbp), %rax
	movl	%edi, %esi
	movl	$16, -208(%rbp)
	movq	_ZL13gTraceContext(%rip), %rdi
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	leaq	-208(%rbp), %rcx
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	call	*%r8
.L193:
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L209
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L201:
	.cfi_restore_state
	leaq	_ZL13gExitFmtValue(%rip), %rdx
	jmp	.L200
.L197:
	leaq	_ZL17gExitFmtPtrStatus(%rip), %rdx
	jmp	.L200
.L202:
	leaq	_ZL8gExitFmt(%rip), %rdx
	jmp	.L200
.L199:
	leaq	_ZL19gExitFmtValueStatus(%rip), %rdx
	jmp	.L200
.L209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	utrace_exit_67.cold, @function
utrace_exit_67.cold:
.LFSB2054:
.L196:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE2054:
	.text
	.size	utrace_exit_67, .-utrace_exit_67
	.section	.text.unlikely
	.size	utrace_exit_67.cold, .-utrace_exit_67.cold
.LCOLDE1:
	.text
.LHOTE1:
	.p2align 4
	.globl	utrace_data_67
	.type	utrace_data_67, @function
utrace_data_67:
.LFB2055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L211
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L211:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	movq	_ZL14pTraceDataFunc(%rip), %rax
	testq	%rax, %rax
	je	.L210
	leaq	16(%rbp), %rcx
	leaq	-208(%rbp), %r8
	movl	$24, -208(%rbp)
	movq	%rcx, -200(%rbp)
	leaq	-176(%rbp), %rcx
	movq	%rcx, -192(%rbp)
	movq	%rdx, %rcx
	movl	%esi, %edx
	movl	%edi, %esi
	movl	$48, -204(%rbp)
	movq	_ZL13gTraceContext(%rip), %rdi
	call	*%rax
.L210:
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L218:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2055:
	.size	utrace_data_67, .-utrace_data_67
	.section	.rodata.str1.1
.LC2:
	.string	"*NULL* "
	.text
	.p2align 4
	.globl	utrace_vformat_67
	.type	utrace_vformat_67, @function
utrace_vformat_67:
.LFB2061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-60(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	xorl	%edx, %edx
	subq	$136, %rsp
	movq	%rcx, -72(%rbp)
	movzbl	(%rcx), %r13d
	movq	%r8, -80(%rbp)
	movl	%r13d, %r11d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	movq	$0, -104(%rbp)
.L220:
	leal	1(%rdx), %r13d
	cmpb	$37, %r11b
	je	.L221
	movsbl	%r11b, %edi
	movl	%ebx, %r8d
	movl	%r14d, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZL10outputCharcPcPiii
	testb	%r11b, %r11b
	jne	.L402
.L222:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jne	.L329
	testl	%ebx, %ebx
	jle	.L329
	testl	%r14d, %r14d
	jle	.L330
	cmpl	%ebx, %r14d
	movl	%ebx, %eax
	movl	$32, %esi
	movq	%r15, %rdi
	cmovle	%r14d, %eax
	leal	-1(%rax), %edx
	addq	$1, %rdx
	testl	%eax, %eax
	movl	$1, %eax
	cmovle	%rax, %rdx
	call	memset@PLT
	.p2align 4,,10
	.p2align 3
.L330:
	movl	%ebx, %eax
.L329:
	cmpl	%eax, %r14d
	jg	.L403
.L331:
	addl	$1, %eax
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L404
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movslq	%r13d, %rcx
	addq	-72(%rbp), %rcx
	leal	2(%rdx), %eax
	movsbl	(%rcx), %edi
	movl	%eax, -84(%rbp)
	testb	%dil, %dil
	je	.L224
	leal	-83(%rdi), %eax
	cmpb	$35, %al
	ja	.L225
	leaq	.L227(%rip), %rsi
	movzbl	%al, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L227:
	.long	.L235-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L234-.L227
	.long	.L233-.L227
	.long	.L232-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L231-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L230-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L229-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L228-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L226-.L227
	.text
	.p2align 4,,10
	.p2align 3
.L403:
	movslq	%eax, %rdx
	movb	$0, (%r15,%rdx)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L402:
	movq	-72(%rbp), %rcx
	movslq	%r13d, %rax
	movl	%r13d, %edx
	movzbl	(%rcx,%rax), %r11d
	jmp	.L220
.L233:
	movq	-80(%rbp), %rsi
	movl	(%rsi), %eax
	cmpl	$47, %eax
	ja	.L236
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%rsi), %rdx
	movl	%eax, (%rsi)
.L237:
	movsbl	(%rdx), %edi
.L225:
	movl	%ebx, %r8d
	movl	%r14d, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZL10outputCharcPcPiii
.L395:
	movslq	-84(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	%rax, %rdx
	movzbl	(%rsi,%rax), %r11d
	jmp	.L220
.L235:
	movq	-80(%rbp), %rdi
	movl	(%rdi), %eax
	cmpl	$47, %eax
	ja	.L243
	movq	16(%rdi), %rsi
	leal	8(%rax), %edx
	movl	%eax, %ecx
	movl	%edx, (%rdi)
	movq	(%rsi,%rcx), %rdi
	cmpl	$47, %edx
	ja	.L405
	movq	-80(%rbp), %rcx
	addl	$16, %eax
	addq	%rdx, %rsi
	movl	%eax, (%rcx)
.L247:
	movl	(%rsi), %esi
	movq	%r15, %rdx
	movl	%ebx, %r9d
	movl	%r14d, %r8d
	movq	%r12, %rcx
	call	_ZL13outputUStringPKDsiPcPiii
	movslq	-84(%rbp), %rax
	movq	-72(%rbp), %rdi
	movq	%rax, %rdx
	movzbl	(%rdi,%rax), %r11d
	jmp	.L220
.L234:
	movq	-80(%rbp), %rsi
	movl	(%rsi), %eax
	cmpl	$47, %eax
	ja	.L248
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%rsi), %rdx
	movl	%eax, (%rsi)
.L249:
	movl	(%rdx), %r8d
	leaq	_ZZL14outputHexBytesliPcPiiE9gHexChars(%rip), %r10
	movq	%r12, %rdx
	movq	%r15, %rsi
	movl	%r14d, %ecx
	movq	%r8, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%r10,%rax), %edi
	call	_ZL10outputCharcPcPiii.constprop.1
	andl	$15, %r8d
	movsbl	(%r10,%r8), %edi
	call	_ZL10outputCharcPcPiii.constprop.1
	movslq	-84(%rbp), %rax
	movq	-72(%rbp), %rsi
	movq	%rax, %rdx
	movzbl	(%rsi,%rax), %r11d
	jmp	.L220
.L232:
	movq	-80(%rbp), %rdi
	movl	(%rdi), %eax
	cmpl	$47, %eax
	ja	.L253
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%rdi), %rdx
	movl	%eax, (%rdi)
.L254:
	movslq	(%rdx), %r8
	movl	$28, %r9d
	.p2align 4,,10
	.p2align 3
.L255:
	movl	%r9d, %ecx
	movq	%r8, %rax
	leaq	_ZZL14outputHexBytesliPcPiiE9gHexChars(%rip), %rdi
	movq	%r12, %rdx
	sarq	%cl, %rax
	movq	%r15, %rsi
	movl	%r14d, %ecx
	andl	$15, %eax
	movsbl	(%rdi,%rax), %edi
	call	_ZL10outputCharcPcPiii.constprop.1
	subl	$4, %r9d
	cmpl	$-4, %r9d
	jne	.L255
	jmp	.L395
.L231:
	movq	-80(%rbp), %rcx
	movl	(%rcx), %eax
	cmpl	$47, %eax
	ja	.L250
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%rcx), %rdx
	movl	%eax, (%rcx)
.L251:
	movslq	(%rdx), %r8
	movl	$12, %r9d
	.p2align 4,,10
	.p2align 3
.L252:
	movl	%r9d, %ecx
	movq	%r8, %rax
	leaq	_ZZL14outputHexBytesliPcPiiE9gHexChars(%rip), %rsi
	movq	%r12, %rdx
	sarq	%cl, %rax
	movl	%r14d, %ecx
	andl	$15, %eax
	movsbl	(%rsi,%rax), %edi
	movq	%r15, %rsi
	call	_ZL10outputCharcPcPiii.constprop.1
	subl	$4, %r9d
	cmpl	$-4, %r9d
	jne	.L252
	jmp	.L394
.L230:
	movq	-80(%rbp), %rsi
	movl	(%rsi), %eax
	cmpl	$47, %eax
	ja	.L256
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%rsi), %rdx
	movl	%eax, (%rsi)
.L257:
	movq	(%rdx), %r9
	movl	$60, %r8d
	movq	%r9, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L258:
	movl	%r8d, %ecx
	movq	%r9, %rax
	leaq	_ZZL14outputHexBytesliPcPiiE9gHexChars(%rip), %rdx
	movq	%r15, %rsi
	sarq	%cl, %rax
	movl	%r14d, %ecx
	andl	$15, %eax
	movsbl	(%rdx,%rax), %edi
	movq	%r12, %rdx
	call	_ZL10outputCharcPcPiii.constprop.1
	subl	$4, %r8d
	cmpl	$-4, %r8d
	jne	.L258
	movslq	-84(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	%rax, %rdx
	movzbl	(%rcx,%rax), %r11d
	jmp	.L220
.L229:
	movq	-80(%rbp), %rcx
	movl	(%rcx), %eax
	cmpl	$47, %eax
	ja	.L259
	movl	%eax, %edi
	addl	$8, %eax
	addq	16(%rcx), %rdi
	movl	%eax, (%rcx)
.L260:
	movq	(%rdi), %rdi
	movl	%r14d, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZL14outputPtrBytesPvPcPii
.L394:
	movslq	-84(%rbp), %rax
	movq	-72(%rbp), %rdi
	movq	%rax, %rdx
	movzbl	(%rdi,%rax), %r11d
	jmp	.L220
.L228:
	movq	-80(%rbp), %rcx
	movl	(%rcx), %eax
	cmpl	$47, %eax
	ja	.L239
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%rcx), %rdx
	movl	%eax, (%rcx)
.L240:
	movq	(%rdx), %r11
	leaq	.LC0(%rip), %rax
	movl	%ebx, %r8d
	testq	%r11, %r11
	cmove	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L242:
	movsbl	(%r11), %edi
	movl	%r14d, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZL10outputCharcPcPiii
	addq	$1, %r11
	testb	%dil, %dil
	jne	.L242
	movslq	-84(%rbp), %rax
	movq	-72(%rbp), %rsi
	movl	%r8d, %ebx
	movzbl	(%rsi,%rax), %r11d
	movq	%rax, %rdx
	jmp	.L220
.L226:
	movslq	-84(%rbp), %rax
	movq	-72(%rbp), %rcx
	addq	%rcx, %rax
	movzbl	(%rax), %esi
	movq	%rax, -144(%rbp)
	testb	%sil, %sil
	je	.L270
	leal	3(%rdx), %eax
	movl	%eax, -84(%rbp)
	cltq
	addq	%rcx, %rax
	movq	%rax, -144(%rbp)
.L270:
	movq	-80(%rbp), %rdi
	movl	(%rdi), %edx
	cmpl	$47, %edx
	ja	.L271
	movq	16(%rdi), %rax
	leal	8(%rdx), %ecx
	movl	%ecx, (%rdi)
	movl	%edx, %edi
	movq	(%rax,%rdi), %rdi
	movq	%rdi, -128(%rbp)
	cmpl	$47, %ecx
	ja	.L406
	movq	-80(%rbp), %rdi
	addl	$16, %edx
	addq	%rcx, %rax
	movl	%edx, (%rdi)
.L275:
	movl	(%rax), %eax
	cmpq	$0, -128(%rbp)
	movl	%eax, -88(%rbp)
	je	.L407
	cmpl	$-1, %eax
	setne	-110(%rbp)
	movzbl	-110(%rbp), %ecx
	testl	%eax, %eax
	jg	.L338
	testb	%cl, %cl
	jne	.L277
.L338:
	movq	-128(%rbp), %rax
	cmpl	$-1, -88(%rbp)
	movl	$0, -108(%rbp)
	sete	-111(%rbp)
	xorl	%r13d, %r13d
	movq	%rax, -120(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rax, -152(%rbp)
	leal	-83(%rsi), %eax
	movb	%al, -109(%rbp)
	movzbl	%al, %eax
	movq	%rax, -136(%rbp)
	movl	%ebx, %eax
	movl	%r14d, %ebx
	movl	%r13d, -96(%rbp)
	movq	-104(%rbp), %r13
	movl	%eax, %r14d
	.p2align 4,,10
	.p2align 3
.L372:
	cmpb	$32, -109(%rbp)
	ja	.L280
	movq	-136(%rbp), %rax
	leaq	.L282(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L282:
	.long	.L289-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L288-.L282
	.long	.L287-.L282
	.long	.L286-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L285-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L284-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L283-.L282
	.long	.L280-.L282
	.long	.L280-.L282
	.long	.L281-.L282
	.text
	.p2align 4,,10
	.p2align 3
.L224:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jne	.L261
	testl	%ebx, %ebx
	jle	.L262
	cmpl	$0, %r14d
	jle	.L352
	xorl	%esi, %esi
.L335:
	leal	(%rbx,%rax), %r8d
	movl	%r14d, %edi
	movl	%eax, %edx
	movq	%rcx, -96(%rbp)
	cmpl	%r14d, %r8d
	notl	%edx
	movl	%r8d, -84(%rbp)
	cmovle	%r8d, %edi
	addl	%edi, %edx
	addq	$1, %rdx
	cmpl	%edi, %eax
	leaq	(%r15,%rsi), %rdi
	movl	$1, %eax
	cmovge	%rax, %rdx
	movl	$32, %esi
	call	memset@PLT
	movq	-96(%rbp), %rcx
	movl	-84(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L267:
	movl	%r8d, %eax
.L262:
	movslq	%eax, %rsi
	cmpl	%eax, %r14d
	jle	.L396
.L266:
	movb	$37, (%r15,%rsi)
.L396:
	addl	$1, %eax
	movzbl	(%rcx), %r11d
	movl	%r13d, %edx
	movl	%eax, -60(%rbp)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L280:
	movl	-108(%rbp), %eax
	testl	%eax, %eax
	jne	.L408
	.p2align 4,,10
	.p2align 3
.L291:
	testq	%r13, %r13
	jne	.L341
	cmpb	$0, -111(%rbp)
	jne	.L337
.L341:
	addl	$1, -96(%rbp)
	movl	-96(%rbp), %eax
	cmpl	%eax, -88(%rbp)
	jg	.L372
	cmpb	$0, -110(%rbp)
	je	.L372
	movq	%r13, -104(%rbp)
	movl	%r14d, %eax
	movl	%ebx, %r14d
	movl	%eax, %ebx
.L277:
	movl	-60(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L409
	cmpl	%ecx, %r14d
	jle	.L317
	movslq	%ecx, %rsi
	cmpb	$10, -1(%r15,%rsi)
	movq	%rsi, %rax
	je	.L410
	.p2align 4,,10
	.p2align 3
.L319:
	movb	$91, (%r15,%rax)
.L317:
	addl	$1, %ecx
	movslq	-88(%rbp), %r13
	movl	$28, %r8d
	movl	%ecx, -60(%rbp)
	.p2align 4,,10
	.p2align 3
.L321:
	movl	%r8d, %ecx
	movq	%r13, %rax
	leaq	_ZZL14outputHexBytesliPcPiiE9gHexChars(%rip), %rsi
	movq	%r12, %rdx
	sarq	%cl, %rax
	movl	%r14d, %ecx
	andl	$15, %eax
	movsbl	(%rsi,%rax), %edi
	movq	%r15, %rsi
	call	_ZL10outputCharcPcPiii.constprop.1
	subl	$4, %r8d
	cmpl	$-4, %r8d
	jne	.L321
	movl	-60(%rbp), %r13d
	testl	%r13d, %r13d
	je	.L411
	cmpl	%r13d, %r14d
	jle	.L325
	movslq	%r13d, %rsi
	cmpb	$10, -1(%r15,%rsi)
	movq	%rsi, %rax
	je	.L412
	.p2align 4,,10
	.p2align 3
.L327:
	movb	$93, (%r15,%rax)
.L325:
	movq	-144(%rbp), %rax
	leal	1(%r13), %ecx
	movl	-84(%rbp), %edx
	movl	%ecx, -60(%rbp)
	movzbl	(%rax), %r11d
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L259:
	movq	8(%rcx), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 8(%rcx)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L256:
	movq	8(%rsi), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rsi)
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L250:
	movq	8(%rcx), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rcx)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L253:
	movq	8(%rdi), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rdi)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L271:
	movq	8(%rdi), %rdx
	movq	%rdi, %rcx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rdi)
	movq	(%rdx), %rdi
	movq	%rdi, -128(%rbp)
	movq	%rcx, %rdi
.L274:
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%rdi)
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L239:
	movq	8(%rcx), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rcx)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L236:
	movq	8(%rsi), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rsi)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L248:
	movq	8(%rsi), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%rsi)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L243:
	movq	8(%rdi), %rax
	movq	%rdi, %rcx
	leaq	8(%rax), %rsi
	movq	%rsi, 8(%rdi)
	movq	(%rax), %rdi
.L246:
	leaq	8(%rsi), %rax
	movq	%rax, 8(%rcx)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L261:
	cmpl	%eax, %r14d
	jg	.L264
	addl	$1, %eax
	movl	%ebx, %r8d
	movl	%r14d, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	xorl	%edi, %edi
	movl	%eax, -60(%rbp)
	call	_ZL10outputCharcPcPiii
	jmp	.L222
.L281:
	movq	-120(%rbp), %rax
	movl	%r14d, %r8d
	movq	(%rax), %r11
	leaq	.LC0(%rip), %rax
	testq	%r11, %r11
	cmove	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L293:
	movsbl	(%r11), %edi
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZL10outputCharcPcPiii
	addq	$1, %r11
	testb	%dil, %dil
	jne	.L293
	movslq	-60(%rbp), %rax
	movl	%r8d, %r14d
	testl	%eax, %eax
	je	.L340
.L401:
	cmpl	%eax, %ebx
	jg	.L299
.L340:
	testl	%r14d, %r14d
	jle	.L301
	leal	(%r14,%rax), %r13d
	cmpl	%eax, %ebx
	jle	.L302
	cmpl	%ebx, %r13d
	movl	%ebx, %ecx
	movl	%eax, %edx
	movl	$32, %esi
	cmovle	%r13d, %ecx
	notl	%edx
	leaq	(%r15,%rax), %rdi
	addl	%ecx, %edx
	addq	$1, %rdx
	cmpl	%ecx, %eax
	movl	$1, %ecx
	cmovge	%rcx, %rdx
	call	memset@PLT
	.p2align 4,,10
	.p2align 3
.L302:
	movl	%r13d, %eax
.L301:
	cmpl	%eax, %ebx
	jg	.L299
.L303:
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	movq	-120(%rbp), %rax
.L397:
	xorl	%r13d, %r13d
	cmpq	$0, (%rax)
	movl	$0, -108(%rbp)
	setne	%r13b
	addq	$8, %rax
	movq	%rax, -120(%rbp)
	jmp	.L291
.L286:
	movq	-160(%rbp), %rax
	movl	$8, -108(%rbp)
	movl	$28, %r8d
	movslq	(%rax), %r13
	addq	$4, %rax
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L304:
	movl	%r8d, %ecx
	movq	%r13, %rax
	leaq	_ZZL14outputHexBytesliPcPiiE9gHexChars(%rip), %rdx
	movq	%r15, %rsi
	sarq	%cl, %rax
	movl	%ebx, %ecx
	andl	$15, %eax
	movsbl	(%rdx,%rax), %edi
	movq	%r12, %rdx
	call	_ZL10outputCharcPcPiii.constprop.1
	subl	$4, %r8d
	jns	.L304
	movl	-60(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L413
	cmpl	%ecx, %ebx
	jle	.L308
	movslq	%ecx, %rdi
	cmpb	$10, -1(%r15,%rdi)
	movq	%rdi, %rax
	je	.L414
	.p2align 4,,10
	.p2align 3
.L310:
	movb	$32, (%r15,%rax)
.L308:
	addl	$1, %ecx
	movl	%ecx, -60(%rbp)
	jmp	.L291
.L288:
	movq	-128(%rbp), %rax
	movl	$2, -108(%rbp)
	movl	$4, %r8d
	movsbq	(%rax), %r13
	addq	$1, %rax
	movq	%rax, -128(%rbp)
	jmp	.L304
.L289:
	movq	-120(%rbp), %rax
	movl	%r14d, %r9d
	movl	%ebx, %r8d
	movq	%r12, %rcx
	movq	%r15, %rdx
	movl	$-1, %esi
	movq	(%rax), %rdi
	call	_ZL13outputUStringPKDsiPcPiii
	movslq	-60(%rbp), %rax
	testl	%eax, %eax
	jne	.L401
	jmp	.L340
.L287:
	movq	-128(%rbp), %r11
	movl	%r14d, %r8d
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movsbl	(%r11), %edi
	call	_ZL10outputCharcPcPiii
	movl	$0, -108(%rbp)
	movq	%r11, %rax
	movsbq	(%r11), %r13
	addq	$1, %rax
	movq	%rax, -128(%rbp)
	jmp	.L291
.L284:
	movq	-168(%rbp), %rax
	movl	$16, -108(%rbp)
	movl	$60, %r8d
	movq	(%rax), %r13
	addq	$8, %rax
	movq	%rax, -168(%rbp)
	jmp	.L304
.L285:
	movq	-152(%rbp), %rax
	movl	$4, -108(%rbp)
	movl	$12, %r8d
	movswq	(%rax), %r13
	addq	$2, %rax
	movq	%rax, -152(%rbp)
	jmp	.L304
.L283:
	movq	-120(%rbp), %r13
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	0(%r13), %rdi
	call	_ZL14outputPtrBytesPvPcPii
	movq	%r13, %rax
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L413:
	testl	%r14d, %r14d
	jle	.L306
	testl	%ebx, %ebx
	jle	.L311
	xorl	%edi, %edi
.L334:
	leal	(%r14,%rcx), %eax
	movl	%ecx, %edx
	movl	$32, %esi
	movl	%ecx, -104(%rbp)
	cmpl	%ebx, %eax
	notl	%edx
	cmovg	%ebx, %eax
	addl	%eax, %edx
	addq	$1, %rdx
	cmpl	%eax, %ecx
	movl	$1, %eax
	cmovge	%rax, %rdx
	addq	%r15, %rdi
	call	memset@PLT
	movl	-104(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L311:
	leal	-1(%r14), %eax
	testl	%r14d, %r14d
	movl	$0, %edx
	cmovle	%edx, %eax
	leal	1(%rcx,%rax), %ecx
.L306:
	cmpl	%ecx, %ebx
	jle	.L308
	movslq	%ecx, %rax
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L299:
	movslq	%eax, %rdx
	movb	$10, (%r15,%rdx)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L411:
	testl	%ebx, %ebx
	jle	.L323
	testl	%r14d, %r14d
	jle	.L328
	xorl	%esi, %esi
.L332:
	leal	(%rbx,%r13), %eax
	movl	%r13d, %edx
	leaq	(%r15,%rsi), %rdi
	movl	$32, %esi
	cmpl	%r14d, %eax
	notl	%edx
	cmovg	%r14d, %eax
	addl	%eax, %edx
	addq	$1, %rdx
	cmpl	%eax, %r13d
	movl	$1, %eax
	cmovge	%rax, %rdx
	call	memset@PLT
	.p2align 4,,10
	.p2align 3
.L328:
	leal	-1(%rbx), %eax
	testl	%ebx, %ebx
	movl	$0, %edx
	cmovle	%edx, %eax
	leal	1(%r13,%rax), %r13d
.L323:
	cmpl	%r13d, %r14d
	jle	.L325
	movslq	%r13d, %rax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L409:
	testl	%ebx, %ebx
	jle	.L315
	testl	%r14d, %r14d
	jle	.L320
	xorl	%esi, %esi
.L333:
	leal	(%rbx,%rcx), %eax
	movl	%ecx, %edx
	leaq	(%r15,%rsi), %rdi
	movl	$32, %esi
	cmpl	%r14d, %eax
	notl	%edx
	movl	%ecx, -96(%rbp)
	cmovg	%r14d, %eax
	addl	%eax, %edx
	addq	$1, %rdx
	cmpl	%eax, %ecx
	movl	$1, %eax
	cmovge	%rax, %rdx
	call	memset@PLT
	movl	-96(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L320:
	leal	-1(%rbx), %eax
	testl	%ebx, %ebx
	movl	$0, %edx
	cmovle	%edx, %eax
	leal	1(%rcx,%rax), %ecx
.L315:
	cmpl	%ecx, %r14d
	jle	.L317
	movslq	%ecx, %rax
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L352:
	movl	%ebx, %r8d
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L337:
	movl	%r14d, %eax
	movq	$0, -104(%rbp)
	movl	%ebx, %r14d
	movl	%eax, %ebx
	jmp	.L277
.L407:
	leaq	.LC2(%rip), %r11
	movl	%ebx, %r8d
	.p2align 4,,10
	.p2align 3
.L278:
	movsbl	(%r11), %edi
	movl	%r14d, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZL10outputCharcPcPiii
	addq	$1, %r11
	testb	%dil, %dil
	jne	.L278
	movl	%r8d, %ebx
	jmp	.L277
.L264:
	movslq	%eax, %rsi
	cmpb	$10, -1(%r15,%rsi)
	jne	.L266
	testl	%ebx, %ebx
	jg	.L335
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L414:
	testl	%r14d, %r14d
	jg	.L334
	jmp	.L310
.L408:
	movl	-108(%rbp), %eax
	leal	-4(,%rax,4), %r8d
	jmp	.L304
.L412:
	testl	%ebx, %ebx
	jg	.L332
	jmp	.L327
.L410:
	testl	%ebx, %ebx
	jg	.L333
	jmp	.L319
.L404:
	call	__stack_chk_fail@PLT
.L406:
	movq	-80(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	8(%rax), %rax
	jmp	.L274
.L405:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rsi
	movq	%rax, %rcx
	jmp	.L246
	.cfi_endproc
.LFE2061:
	.size	utrace_vformat_67, .-utrace_vformat_67
	.p2align 4
	.globl	utrace_format_67
	.type	utrace_format_67, @function
utrace_format_67:
.LFB2062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L416
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L416:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %r8
	movl	$32, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movl	$48, -204(%rbp)
	movq	%rax, -192(%rbp)
	call	utrace_vformat_67
	movq	-184(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L419
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L419:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2062:
	.size	utrace_format_67, .-utrace_format_67
	.p2align 4
	.globl	utrace_setFunctions_67
	.type	utrace_setFunctions_67, @function
utrace_setFunctions_67:
.LFB2063:
	.cfi_startproc
	endbr64
	movq	%rsi, _ZL15pTraceEntryFunc(%rip)
	movq	%rdx, _ZL14pTraceExitFunc(%rip)
	movq	%rcx, _ZL14pTraceDataFunc(%rip)
	movq	%rdi, _ZL13gTraceContext(%rip)
	ret
	.cfi_endproc
.LFE2063:
	.size	utrace_setFunctions_67, .-utrace_setFunctions_67
	.p2align 4
	.globl	utrace_getFunctions_67
	.type	utrace_getFunctions_67, @function
utrace_getFunctions_67:
.LFB2064:
	.cfi_startproc
	endbr64
	movq	_ZL15pTraceEntryFunc(%rip), %rax
	movq	%rax, (%rsi)
	movq	_ZL14pTraceExitFunc(%rip), %rax
	movq	%rax, (%rdx)
	movq	_ZL14pTraceDataFunc(%rip), %rax
	movq	%rax, (%rcx)
	movq	_ZL13gTraceContext(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE2064:
	.size	utrace_getFunctions_67, .-utrace_getFunctions_67
	.p2align 4
	.globl	utrace_setLevel_67
	.type	utrace_setLevel_67, @function
utrace_setLevel_67:
.LFB2065:
	.cfi_startproc
	endbr64
	cmpl	$9, %edi
	movl	$9, %eax
	cmovg	%eax, %edi
	movl	$-1, %eax
	testl	%edi, %edi
	cmovs	%eax, %edi
	movl	%edi, _ZL12utrace_level(%rip)
	ret
	.cfi_endproc
.LFE2065:
	.size	utrace_setLevel_67, .-utrace_setLevel_67
	.p2align 4
	.globl	utrace_getLevel_67
	.type	utrace_getLevel_67, @function
utrace_getLevel_67:
.LFB2066:
	.cfi_startproc
	endbr64
	movl	_ZL12utrace_level(%rip), %eax
	ret
	.cfi_endproc
.LFE2066:
	.size	utrace_getLevel_67, .-utrace_getLevel_67
	.p2align 4
	.globl	utrace_cleanup_67
	.type	utrace_cleanup_67, @function
utrace_cleanup_67:
.LFB2067:
	.cfi_startproc
	endbr64
	movq	$0, _ZL15pTraceEntryFunc(%rip)
	movl	$1, %eax
	movq	$0, _ZL14pTraceExitFunc(%rip)
	movq	$0, _ZL14pTraceDataFunc(%rip)
	movl	$-1, _ZL12utrace_level(%rip)
	movq	$0, _ZL13gTraceContext(%rip)
	ret
	.cfi_endproc
.LFE2067:
	.size	utrace_cleanup_67, .-utrace_cleanup_67
	.section	.rodata.str1.1
.LC3:
	.string	"[BOGUS Trace Function Number]"
	.text
	.p2align 4
	.globl	utrace_functionName_67
	.type	utrace_functionName_67, @function
utrace_functionName_67:
.LFB2068:
	.cfi_startproc
	endbr64
	cmpl	$1, %edi
	jbe	.L431
	leal	-4096(%rdi), %eax
	cmpl	$7, %eax
	jbe	.L432
	leal	-8192(%rdi), %eax
	cmpl	$8, %eax
	jbe	.L433
	subl	$12288, %edi
	leaq	.LC3(%rip), %rax
	cmpl	$3, %edi
	ja	.L425
	movslq	%edi, %rdi
	leaq	_ZL14trResDataNames(%rip), %rax
	movq	(%rax,%rdi,8), %rax
.L425:
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	leaq	_ZL11trConvNames(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	movslq	%edi, %rdi
	leaq	_ZL8trFnName(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	leaq	_ZL11trCollNames(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.cfi_endproc
.LFE2068:
	.size	utrace_functionName_67, .-utrace_functionName_67
	.section	.rodata.str1.1
.LC4:
	.string	"resc"
.LC5:
	.string	"bundle-open"
.LC6:
	.string	"file-open"
.LC7:
	.string	"res-open"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL14trResDataNames, @object
	.size	_ZL14trResDataNames, 40
_ZL14trResDataNames:
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	0
	.section	.rodata.str1.1
.LC8:
	.string	"ucol_open"
.LC9:
	.string	"ucol_close"
.LC10:
	.string	"ucol_strcoll"
.LC11:
	.string	"ucol_getSortKey"
.LC12:
	.string	"ucol_getLocale"
.LC13:
	.string	"ucol_nextSortKeyPart"
.LC14:
	.string	"ucol_strcollIter"
.LC15:
	.string	"ucol_openFromShortString"
.LC16:
	.string	"ucol_strcollUTF8"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL11trCollNames, @object
	.size	_ZL11trCollNames, 80
_ZL11trCollNames:
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	0
	.section	.rodata.str1.1
.LC17:
	.string	"ucnv_open"
.LC18:
	.string	"ucnv_openPackage"
.LC19:
	.string	"ucnv_openAlgorithmic"
.LC20:
	.string	"ucnv_clone"
.LC21:
	.string	"ucnv_close"
.LC22:
	.string	"ucnv_flushCache"
.LC23:
	.string	"ucnv_load"
.LC24:
	.string	"ucnv_unload"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL11trConvNames, @object
	.size	_ZL11trConvNames, 72
_ZL11trConvNames:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	0
	.section	.rodata.str1.1
.LC25:
	.string	"u_init"
.LC26:
	.string	"u_cleanup"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZL8trFnName, @object
	.size	_ZL8trFnName, 24
_ZL8trFnName:
	.quad	.LC25
	.quad	.LC26
	.quad	0
	.section	.rodata
	.align 16
	.type	_ZZL14outputHexBytesliPcPiiE9gHexChars, @object
	.size	_ZZL14outputHexBytesliPcPiiE9gHexChars, 17
_ZZL14outputHexBytesliPcPiiE9gHexChars:
	.string	"0123456789abcdef"
	.align 16
	.type	_ZL17gExitFmtPtrStatus, @object
	.size	_ZL17gExitFmtPtrStatus, 26
_ZL17gExitFmtPtrStatus:
	.string	"Returns %d.  Status = %p."
	.align 16
	.type	_ZL19gExitFmtValueStatus, @object
	.size	_ZL19gExitFmtValueStatus, 26
_ZL19gExitFmtValueStatus:
	.string	"Returns %d.  Status = %d."
	.align 16
	.type	_ZL14gExitFmtStatus, @object
	.size	_ZL14gExitFmtStatus, 23
_ZL14gExitFmtStatus:
	.string	"Returns.  Status = %d."
	.align 8
	.type	_ZL13gExitFmtValue, @object
	.size	_ZL13gExitFmtValue, 12
_ZL13gExitFmtValue:
	.string	"Returns %d."
	.align 8
	.type	_ZL8gExitFmt, @object
	.size	_ZL8gExitFmt, 9
_ZL8gExitFmt:
	.string	"Returns."
	.local	_ZL12utrace_level
	.comm	_ZL12utrace_level,4,4
	.local	_ZL13gTraceContext
	.comm	_ZL13gTraceContext,8,8
	.local	_ZL14pTraceDataFunc
	.comm	_ZL14pTraceDataFunc,8,8
	.local	_ZL14pTraceExitFunc
	.comm	_ZL14pTraceExitFunc,8,8
	.local	_ZL15pTraceEntryFunc
	.comm	_ZL15pTraceEntryFunc,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
