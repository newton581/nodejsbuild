	.file	"locresdata.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Countries"
.LC1:
	.string	"Languages"
.LC2:
	.string	"Fallback"
	.text
	.p2align 4
	.globl	uloc_getTableStringWithFallback_67
	.type	uloc_getTableStringWithFallback_67, @function
uloc_getTableStringWithFallback_67:
.LFB2403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-648(%rbp), %rbx
	subq	$696, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -672(%rbp)
	movq	%rbx, %rdx
	movq	%rdi, -712(%rbp)
	movq	%rsi, -696(%rbp)
	movq	%rcx, -688(%rbp)
	movq	%r8, -720(%rbp)
	movq	%r9, -736(%rbp)
	movq	%rax, -680(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -648(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	ures_open_67@PLT
	movq	%rax, %r13
	movl	-648(%rbp), %eax
	testl	%eax, %eax
	jg	.L43
	cmpl	$-127, %eax
	je	.L4
	cmpl	$-128, %eax
	je	.L44
.L5:
	leaq	-644(%rbp), %rax
	leaq	-640(%rbp), %r12
	movq	$0, -728(%rbp)
	movq	%rax, -704(%rbp)
	leaq	-432(%rbp), %r14
	leaq	-224(%rbp), %r15
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movl	-648(%rbp), %eax
	testl	%eax, %eax
	jg	.L16
.L17:
	movq	%r14, %rdi
	call	_ZN6icu_6720StackUResourceBundleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6720StackUResourceBundleD1Ev@PLT
	movq	-664(%rbp), %r13
.L18:
	movq	%r12, %rdi
	call	_ZN6icu_6720StackUResourceBundleC1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6720StackUResourceBundleC1Ev@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	-672(%rbp), %rsi
	call	ures_getByKeyWithFallback_67@PLT
	movq	-688(%rbp), %rax
	testq	%rax, %rax
	je	.L6
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	ures_getByKeyWithFallback_67@PLT
.L6:
	movl	-648(%rbp), %eax
	testl	%eax, %eax
	jle	.L45
.L20:
	movq	-680(%rbp), %rcx
	movq	-704(%rbp), %rdx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movl	$0, -644(%rbp)
	movl	%eax, (%rcx)
	movq	%rbx, %rcx
	movl	$0, -648(%rbp)
	call	ures_getStringByKeyWithFallback_67@PLT
	movq	%rax, %rdi
	movl	-648(%rbp), %eax
	testl	%eax, %eax
	jg	.L41
	movl	-644(%rbp), %edx
	movq	%r15, %rsi
	call	u_UCharsToChars_67@PLT
	movq	-696(%rbp), %rsi
	movq	%r15, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L46
	movq	-712(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	ures_open_67@PLT
	movq	%rax, -664(%rbp)
	testq	%r13, %r13
	jne	.L47
	movl	-648(%rbp), %eax
	testl	%eax, %eax
	jle	.L17
.L16:
	movq	-680(%rbp), %rbx
	movq	-664(%rbp), %r13
	movl	%eax, (%rbx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L45:
	movq	-736(%rbp), %rdx
	movq	-720(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r12, %rdi
	call	ures_getStringByKeyWithFallback_67@PLT
	movq	%rax, -728(%rbp)
	movl	-648(%rbp), %eax
	testl	%eax, %eax
	jle	.L8
	movq	-680(%rbp), %rdx
	movq	-672(%rbp), %rsi
	movl	$10, %ecx
	leaq	.LC0(%rip), %rdi
	movl	$0, -648(%rbp)
	movl	%eax, (%rdx)
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L48
	movq	-672(%rbp), %rsi
	movl	$10, %ecx
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L49
.L8:
	movq	%r14, %rdi
	call	_ZN6icu_6720StackUResourceBundleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6720StackUResourceBundleD1Ev@PLT
.L3:
	testq	%r13, %r13
	je	.L1
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	movq	-728(%rbp), %rax
	addq	$696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	-720(%rbp), %rdi
	call	uloc_getCurrentCountryID_67@PLT
	movq	%rax, %rsi
.L10:
	testq	%rsi, %rsi
	je	.L11
	cmpq	-720(%rbp), %rsi
	je	.L11
	movq	-736(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	call	ures_getStringByKeyWithFallback_67@PLT
	movq	%rax, -728(%rbp)
	movl	-648(%rbp), %eax
	testl	%eax, %eax
	jg	.L20
	.p2align 4,,10
	.p2align 3
.L41:
	movq	-680(%rbp), %rbx
	movl	%eax, (%rbx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	movl	-648(%rbp), %eax
	testl	%eax, %eax
	jg	.L20
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-720(%rbp), %rdi
	call	uloc_getCurrentLanguageID_67@PLT
	movq	%rax, %rsi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L44:
	movq	-680(%rbp), %rdx
	cmpl	$-127, (%rdx)
	je	.L5
.L4:
	movq	-680(%rbp), %rdx
	movl	%eax, (%rdx)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L43:
	movq	-680(%rbp), %rbx
	movq	$0, -728(%rbp)
	movl	%eax, (%rbx)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-680(%rbp), %rax
	movl	$5, (%rax)
	jmp	.L8
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2403:
	.size	uloc_getTableStringWithFallback_67, .-uloc_getTableStringWithFallback_67
	.section	.rodata.str1.1
.LC3:
	.string	"characters"
.LC4:
	.string	"layout"
	.text
	.p2align 4
	.globl	uloc_getCharacterOrientation_67
	.type	uloc_getCharacterOrientation_67, @function
uloc_getCharacterOrientation_67:
.LFB2405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$192, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rsi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$4, %eax
	testl	%r9d, %r9d
	jle	.L68
.L51:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L69
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	leaq	-192(%rbp), %r12
	movq	%rsi, %rbx
	movq	%rsi, %rcx
	movl	$157, %edx
	movl	$0, -196(%rbp)
	movq	%r12, %rsi
	call	uloc_canonicalize_67@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L53
.L67:
	movl	$4, %eax
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L53:
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	xorl	%edi, %edi
	pushq	%rbx
	leaq	-196(%rbp), %r9
	leaq	.LC3(%rip), %r8
	leaq	.LC4(%rip), %rdx
	call	uloc_getTableStringWithFallback_67
	movl	(%rbx), %edi
	popq	%rcx
	popq	%rsi
	testl	%edi, %edi
	jg	.L67
	movl	-196(%rbp), %edx
	testl	%edx, %edx
	je	.L67
	movzwl	(%rax), %edx
	cmpw	$114, %dx
	je	.L60
	ja	.L56
	movl	$3, %eax
	cmpw	$98, %dx
	je	.L51
	cmpw	$108, %dx
	jne	.L57
	xorl	%eax, %eax
	jmp	.L51
.L56:
	movl	$2, %eax
	cmpw	$116, %dx
	je	.L51
.L57:
	movl	$5, (%rbx)
	jmp	.L67
.L60:
	movl	$1, %eax
	jmp	.L51
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2405:
	.size	uloc_getCharacterOrientation_67, .-uloc_getCharacterOrientation_67
	.section	.rodata.str1.1
.LC5:
	.string	"lines"
	.text
	.p2align 4
	.globl	uloc_getLineOrientation_67
	.type	uloc_getLineOrientation_67, @function
uloc_getLineOrientation_67:
.LFB2406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$192, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rsi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$4, %eax
	testl	%r9d, %r9d
	jle	.L87
.L70:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L88
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	leaq	-192(%rbp), %r12
	movq	%rsi, %rbx
	movq	%rsi, %rcx
	movl	$157, %edx
	movl	$0, -196(%rbp)
	movq	%r12, %rsi
	call	uloc_canonicalize_67@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L72
.L86:
	movl	$4, %eax
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L72:
	subq	$8, %rsp
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	xorl	%edi, %edi
	pushq	%rbx
	leaq	-196(%rbp), %r9
	leaq	.LC5(%rip), %r8
	leaq	.LC4(%rip), %rdx
	call	uloc_getTableStringWithFallback_67
	movl	(%rbx), %edi
	popq	%rcx
	popq	%rsi
	testl	%edi, %edi
	jg	.L86
	movl	-196(%rbp), %edx
	testl	%edx, %edx
	je	.L86
	movzwl	(%rax), %edx
	cmpw	$114, %dx
	je	.L79
	ja	.L75
	movl	$3, %eax
	cmpw	$98, %dx
	je	.L70
	cmpw	$108, %dx
	jne	.L76
	xorl	%eax, %eax
	jmp	.L70
.L75:
	movl	$2, %eax
	cmpw	$116, %dx
	je	.L70
.L76:
	movl	$5, (%rbx)
	jmp	.L86
.L79:
	movl	$1, %eax
	jmp	.L70
.L88:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2406:
	.size	uloc_getLineOrientation_67, .-uloc_getLineOrientation_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
