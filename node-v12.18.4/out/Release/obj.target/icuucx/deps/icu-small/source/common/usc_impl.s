	.file	"usc_impl.cpp"
	.text
	.p2align 4
	.globl	uscript_openRun_67
	.type	uscript_openRun_67, @function
uscript_openRun_67:
.LFB2059:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L7
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L1
	movq	%rdi, %r13
	movl	$296, %edi
	movl	%esi, %r12d
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L14
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L4
	testl	%r12d, %r12d
	js	.L5
	testq	%r13, %r13
	sete	%cl
	testl	%r12d, %r12d
	sete	%dl
	cmpb	%dl, %cl
	jne	.L5
	movq	%r13, 8(%rax)
	movl	%r12d, (%rax)
	movq	$0, 16(%rax)
	movl	$-1, 24(%rax)
	movl	$-1, 284(%rax)
	movq	$0, 288(%rax)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	$1, (%rbx)
.L4:
	movq	%rax, %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, (%rbx)
	jmp	.L1
	.cfi_endproc
.LFE2059:
	.size	uscript_openRun_67, .-uscript_openRun_67
	.p2align 4
	.globl	uscript_closeRun_67
	.type	uscript_closeRun_67, @function
uscript_closeRun_67:
.LFB2060:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L15
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	ret
	.cfi_endproc
.LFE2060:
	.size	uscript_closeRun_67, .-uscript_closeRun_67
	.p2align 4
	.globl	uscript_resetRun_67
	.type	uscript_resetRun_67, @function
uscript_resetRun_67:
.LFB2061:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L17
	movq	$0, 16(%rdi)
	movl	$-1, 24(%rdi)
	movl	$-1, 284(%rdi)
	movq	$0, 288(%rdi)
.L17:
	ret
	.cfi_endproc
.LFE2061:
	.size	uscript_resetRun_67, .-uscript_resetRun_67
	.p2align 4
	.globl	uscript_setRunText_67
	.type	uscript_setRunText_67, @function
uscript_setRunText_67:
.LFB2062:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L22
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L22
	testq	%rdi, %rdi
	je	.L24
	testl	%edx, %edx
	js	.L24
	testq	%rsi, %rsi
	sete	%r8b
	testl	%edx, %edx
	sete	%al
	cmpb	%al, %r8b
	je	.L25
.L24:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rsi, 8(%rdi)
	movl	%edx, (%rdi)
	movq	$0, 16(%rdi)
	movl	$-1, 24(%rdi)
	movl	$-1, 284(%rdi)
	movq	$0, 288(%rdi)
.L22:
	ret
	.cfi_endproc
.LFE2062:
	.size	uscript_setRunText_67, .-uscript_setRunText_67
	.p2align 4
	.globl	uscript_nextRun_67
	.type	uscript_nextRun_67, @function
uscript_nextRun_67:
.LFB2063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	testq	%rdi, %rdi
	je	.L64
	movl	20(%rdi), %eax
	movl	(%rdi), %ecx
	movq	%rdi, %rbx
	xorl	%r8d, %r8d
	cmpl	%ecx, %eax
	jge	.L29
	movl	$0, 24(%rdi)
	movq	%rsi, %r13
	leaq	-60(%rbp), %r12
	leaq	_ZL11pairedChars(%rip), %r15
	movl	$0, 292(%rdi)
	movl	%eax, 16(%rdi)
.L60:
	movq	8(%rbx), %rsi
	movslq	%eax, %rdx
	leaq	(%rdx,%rdx), %rdi
	movzwl	(%rsi,%rdx,2), %r14d
	movl	%r14d, %edx
	addw	$10240, %dx
	cmpw	$1023, %dx
	ja	.L31
	subl	$1, %ecx
	cmpl	%eax, %ecx
	jg	.L98
.L31:
	movq	%r12, %rsi
	movl	%r14d, %edi
	call	uscript_getScript_67@PLT
	xorl	%ecx, %ecx
	cmpl	$59, %r14d
	jle	.L36
	cmpl	$12297, %r14d
	movl	$2, %ecx
	movl	$18, %edi
	cmovg	%edi, %ecx
.L36:
	leal	8(%rcx), %edx
	movl	24(%rbx), %edi
	movslq	%edx, %rsi
	cmpl	(%r15,%rsi,4), %r14d
	cmovge	%edx, %ecx
	leal	4(%rcx), %edx
	movslq	%edx, %rsi
	cmpl	(%r15,%rsi,4), %r14d
	cmovge	%edx, %ecx
	leal	2(%rcx), %edx
	movslq	%edx, %rsi
	cmpl	(%r15,%rsi,4), %r14d
	cmovge	%edx, %ecx
	leal	1(%rcx), %edx
	movslq	%edx, %rsi
	cmpl	%r14d, (%r15,%rsi,4)
	cmovle	%edx, %ecx
	movslq	%ecx, %rdx
	cmpl	(%r15,%rdx,4), %r14d
	jne	.L66
	movl	288(%rbx), %esi
	testb	$1, %cl
	jne	.L41
	cmpl	$31, %esi
	movl	$31, %edx
	cmovg	%edx, %esi
	addl	$1, %esi
	cmpl	$31, 292(%rbx)
	cmovle	292(%rbx), %edx
	movl	%esi, 288(%rbx)
	movl	284(%rbx), %esi
	addl	$1, %edx
	movl	%edx, 292(%rbx)
	leal	1(%rsi), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$27, %esi
	addl	%esi, %edx
	andl	$31, %edx
	subl	%esi, %edx
	movl	%edx, 284(%rbx)
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,8), %rdx
	movl	%ecx, 28(%rdx)
	movl	%edi, 32(%rdx)
.L34:
	cmpl	$1, %edi
	setle	%dl
	cmpl	$1, %eax
	jle	.L50
	testb	%dl, %dl
	jne	.L48
	cmpl	%edi, %eax
	je	.L50
	cmpl	$65535, %r14d
	jle	.L52
	subl	$1, 20(%rbx)
.L52:
	testq	%r13, %r13
	je	.L61
	movl	16(%rbx), %eax
	movl	%eax, 0(%r13)
.L61:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	movl	20(%rbx), %eax
	movl	%eax, (%rdi)
.L62:
	movq	-80(%rbp), %rdi
	movl	$1, %r8d
	testq	%rdi, %rdi
	je	.L29
	movl	24(%rbx), %eax
	movl	%eax, (%rdi)
.L29:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	cmpl	$1, %eax
	jle	.L50
	testb	%dl, %dl
	je	.L50
	movl	284(%rbx), %edi
	movl	292(%rbx), %r8d
	movl	%eax, 24(%rbx)
	leal	32(%rdi), %edx
	leal	-1(%r8), %edi
	subl	%r8d, %edx
	movl	%edi, 292(%rbx)
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$27, %esi
	addl	%esi, %edx
	andl	$31, %edx
	subl	%esi, %edx
	testl	%r8d, %r8d
	jle	.L50
	.p2align 4,,10
	.p2align 3
.L54:
	addl	$1, %edx
	subl	$1, %edi
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$27, %esi
	addl	%esi, %edx
	andl	$31, %edx
	subl	%esi, %edx
	movslq	%edx, %rsi
	movl	%eax, 32(%rbx,%rsi,8)
	cmpl	$-1, %edi
	jne	.L54
	movl	$-1, 292(%rbx)
	.p2align 4,,10
	.p2align 3
.L50:
	cmpl	$-1, %ecx
	je	.L58
	andl	$1, %ecx
	je	.L58
	movl	288(%rbx), %eax
	testl	%eax, %eax
	jle	.L58
	movl	292(%rbx), %edx
	testl	%edx, %edx
	jle	.L56
	subl	$1, %edx
	movl	%edx, 292(%rbx)
.L56:
	subl	$1, %eax
	movl	%eax, 288(%rbx)
	jne	.L100
	movl	$-1, 284(%rbx)
	.p2align 4,,10
	.p2align 3
.L58:
	movl	20(%rbx), %eax
	movl	(%rbx), %ecx
	addl	$1, %eax
	movl	%eax, 20(%rbx)
	cmpl	%eax, %ecx
	jg	.L60
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L98:
	movzwl	2(%rsi,%rdi), %edx
	leal	9216(%rdx), %ecx
	cmpw	$1023, %cx
	ja	.L31
	subl	$55296, %r14d
	addl	$1, %eax
	sall	$10, %r14d
	movl	%eax, 20(%rbx)
	leal	9216(%r14,%rdx), %r14d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%ecx, %r8d
	andl	$-2, %r8d
	.p2align 4,,10
	.p2align 3
.L46:
	testl	%esi, %esi
	jle	.L34
	movslq	284(%rbx), %r9
	movq	%r9, %rdx
	leaq	2(%r9), %r10
	cmpl	%r8d, 28(%rbx,%r9,8)
	je	.L42
	movl	292(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L43
	subl	$1, %r9d
	movl	%r9d, 292(%rbx)
.L43:
	addl	$31, %edx
	subl	$1, %esi
	movl	%edx, %r9d
	movl	%esi, 288(%rbx)
	sarl	$31, %r9d
	shrl	$27, %r9d
	addl	%r9d, %edx
	andl	$31, %edx
	subl	%r9d, %edx
	movl	%edx, 284(%rbx)
	testl	%esi, %esi
	jne	.L46
	movl	$-1, 284(%rbx)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L100:
	movl	284(%rbx), %eax
	movl	(%rbx), %ecx
	addl	$31, %eax
	cltd
	shrl	$27, %edx
	addl	%edx, %eax
	andl	$31, %eax
	subl	%edx, %eax
	movl	%eax, 284(%rbx)
	movl	20(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 20(%rbx)
	cmpl	%eax, %ecx
	jg	.L60
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%r8d, %r8d
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L42:
	movl	16(%rbx,%r10,8), %eax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$-1, %ecx
	jmp	.L34
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2063:
	.size	uscript_nextRun_67, .-uscript_nextRun_67
	.section	.rodata
	.align 32
	.type	_ZL11pairedChars, @object
	.size	_ZL11pairedChars, 136
_ZL11pairedChars:
	.long	40
	.long	41
	.long	60
	.long	62
	.long	91
	.long	93
	.long	123
	.long	125
	.long	171
	.long	187
	.long	8216
	.long	8217
	.long	8220
	.long	8221
	.long	8249
	.long	8250
	.long	12296
	.long	12297
	.long	12298
	.long	12299
	.long	12300
	.long	12301
	.long	12302
	.long	12303
	.long	12304
	.long	12305
	.long	12308
	.long	12309
	.long	12310
	.long	12311
	.long	12312
	.long	12313
	.long	12314
	.long	12315
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
