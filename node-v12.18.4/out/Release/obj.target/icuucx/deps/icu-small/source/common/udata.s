	.file	"udata.cpp"
	.text
	.p2align 4
	.type	_ZL16setCommonICUDataP11UDataMemoryaP10UErrorCode, @function
_ZL16setCommonICUDataP11UDataMemoryaP10UErrorCode:
.LFB3048:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	call	UDataMemory_createNewInstance_67@PLT
	movl	(%rbx), %edx
	movq	%rax, %r14
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L19
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	UDatamemory_assign_67@PLT
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L8
	movq	8(%r13), %rdx
	cmpq	%rdx, 8(%rax)
	je	.L4
	movq	8+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L9
	cmpq	%rdx, 8(%rax)
	je	.L4
	movq	16+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L10
	cmpq	%rdx, 8(%rax)
	je	.L4
	movq	24+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L11
	cmpq	%rdx, 8(%rax)
	je	.L4
	movq	32+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L12
	cmpq	8(%rax), %rdx
	je	.L4
	movq	40+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L13
	cmpq	8(%rax), %rdx
	je	.L4
	movq	48+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L14
	cmpq	8(%rax), %rdx
	je	.L4
	movq	56+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L15
	cmpq	8(%rax), %rdx
	je	.L4
	movq	64+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L16
	cmpq	8(%rax), %rdx
	je	.L4
	movq	72+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L20
	cmpq	%rdx, 8(%rax)
	je	.L4
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	testb	%r12b, %r12b
	je	.L6
	movl	$-127, (%rbx)
.L6:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	jmp	.L6
.L20:
	movl	$9, %eax
.L3:
	leaq	_ZL19gCommonICUDataArray(%rip), %rdx
	xorl	%edi, %edi
	movq	%r14, (%rdx,%rax,8)
	call	umtx_unlock_67@PLT
	leaq	_ZL13udata_cleanupv(%rip), %rsi
	movl	$20, %edi
	call	ucln_common_registerCleanup_67@PLT
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L3
.L9:
	movl	$1, %eax
	jmp	.L3
.L10:
	movl	$2, %eax
	jmp	.L3
.L11:
	movl	$3, %eax
	jmp	.L3
.L12:
	movl	$4, %eax
	jmp	.L3
.L13:
	movl	$5, %eax
	jmp	.L3
.L14:
	movl	$6, %eax
	jmp	.L3
.L15:
	movl	$7, %eax
	jmp	.L3
.L16:
	movl	$8, %eax
	jmp	.L3
	.cfi_endproc
.LFE3048:
	.size	_ZL16setCommonICUDataP11UDataMemoryaP10UErrorCode, .-_ZL16setCommonICUDataP11UDataMemoryaP10UErrorCode
	.p2align 4
	.type	_ZL24DataCacheElement_deleterPv, @function
_ZL24DataCacheElement_deleterPv:
.LFB3051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	udata_close_67@PLT
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3051:
	.size	_ZL24DataCacheElement_deleterPv, .-_ZL24DataCacheElement_deleterPv
	.p2align 4
	.type	_ZL13udata_cleanupv, @function
_ZL13udata_cleanupv:
.LFB3046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZL16gCommonDataCache(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L24
	call	uhash_close_67@PLT
	movq	$0, _ZL16gCommonDataCache(%rip)
.L24:
	leaq	_ZL19gCommonICUDataArray(%rip), %rbx
	movl	$0, _ZL24gCommonDataCacheInitOnce(%rip)
	leaq	80(%rbx), %r12
	mfence
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	udata_close_67@PLT
	addq	$8, %rbx
	movq	$0, -8(%rbx)
	cmpq	%r12, %rbx
	jne	.L26
.L25:
	movl	$1, %eax
	movl	$0, _ZL26gHaveTriedToLoadCommonData(%rip)
	mfence
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3046:
	.size	_ZL13udata_cleanupv, .-_ZL13udata_cleanupv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	".dat"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode.part.0, @function
_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode.part.0:
.LFB3907:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
.L52:
	movq	8(%rbx), %r15
	testq	%r15, %r15
	je	.L51
	cmpq	48(%rbx), %r15
	je	.L75
	movl	$58, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L76
	movl	%eax, %r12d
	addq	$1, %rax
	movq	%rax, 8(%rbx)
	subl	%r15d, %r12d
.L39:
	testl	%r12d, %r12d
	je	.L41
	movq	112(%rbx), %rax
	leaq	112(%rbx), %r14
	movl	%r12d, %edx
	movq	%r15, %rsi
	movl	$0, 168(%rbx)
	movq	%r13, %rcx
	movq	%r14, %rdi
	movb	$0, (%rax)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	112(%rbx), %r15
	movl	$47, %esi
	movq	%r15, %rdi
	call	strrchr@PLT
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmove	%r15, %rdx
	cmpb	$1, 240(%rbx)
	jne	.L43
	cmpl	$3, %r12d
	jg	.L77
.L43:
	leal	-1(%r12), %eax
	cltq
	cmpb	$47, (%r15,%rax)
	je	.L46
	cmpl	$3, %r12d
	jle	.L47
	movslq	%r12d, %rax
	leaq	-4(%r15,%rax), %r10
.L53:
	movl	$4, %ecx
	movq	%r10, %rsi
	leaq	.LC0(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L41
.L47:
	movl	232(%rbx), %edx
	cmpl	%edx, %r12d
	jle	.L48
	testl	%edx, %edx
	jne	.L78
.L48:
	movq	%r13, %rdx
	movl	$47, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L46:
	movl	232(%rbx), %eax
	movq	%r13, %rcx
	movq	%r14, %rdi
	leal	-1(%rax), %edx
	movq	176(%rbx), %rax
	leaq	1(%rax), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	32(%rbx), %edx
	testl	%edx, %edx
	je	.L74
	cmpl	$4, %edx
	jg	.L79
.L50:
	movq	24(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L74:
	movq	112(%rbx), %r15
.L35:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	cmpq	$0, (%rbx)
	jne	.L52
.L51:
	xorl	%r15d, %r15d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L75:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	%rax, 8(%rbx)
	call	strlen@PLT
	movl	%eax, %r12d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r15, %rdi
	call	strlen@PLT
	movl	%eax, %r12d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L78:
	movslq	%edx, %rax
	movslq	%r12d, %rdi
	movq	176(%rbx), %rsi
	movl	%edx, -56(%rbp)
	subq	%rax, %rdi
	addq	%r15, %rdi
	call	strcmp@PLT
	movl	-56(%rbp), %edx
	testl	%eax, %eax
	jne	.L48
	movl	%r12d, %esi
	movq	%r14, %rdi
	subl	%edx, %esi
	call	_ZN6icu_6710CharString8truncateEi@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L77:
	movslq	%r12d, %rax
	movq	24(%rbx), %rsi
	movq	%rdx, -64(%rbp)
	movl	$4, %edx
	leaq	-4(%r15,%rax), %r10
	movq	%r10, %rdi
	movq	%r10, -56(%rbp)
	call	strncmp@PLT
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	jne	.L44
	movq	-64(%rbp), %r11
	movl	40(%rbx), %edx
	movq	16(%rbx), %rsi
	movq	%r11, %rdi
	movl	%edx, -68(%rbp)
	call	strncmp@PLT
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	jne	.L44
	movq	-64(%rbp), %r11
	movq	%r11, %rdi
	call	strlen@PLT
	movq	-56(%rbp), %r10
	movq	%rax, %r11
	movl	-68(%rbp), %eax
	addl	$4, %eax
	cmpq	%rax, %r11
	je	.L35
.L44:
	leal	-1(%r12), %eax
	cltq
	cmpb	$47, (%r15,%rax)
	jne	.L53
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString27ensureEndsWithFileSeparatorER10UErrorCode@PLT
	movl	32(%rbx), %edx
	jmp	.L50
	.cfi_endproc
.LFE3907:
	.size	_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode.part.0, .-_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode.part.0
	.p2align 4
	.type	_ZL20udata_findCachedDataPKcR10UErrorCode, @function
_ZL20udata_findCachedDataPKcR10UErrorCode:
.LFB3054:
	.cfi_startproc
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L103
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	_ZL24gCommonDataCacheInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L104
.L82:
	movl	4+_ZL24gCommonDataCacheInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L84
	movl	%eax, (%rbx)
.L81:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	leaq	_ZL24gCommonDataCacheInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L82
	movq	uhash_hashChars_67@GOTPCREL(%rip), %rdi
	movq	uhash_compareChars_67@GOTPCREL(%rip), %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	call	uhash_open_67@PLT
	movq	%rax, _ZL16gCommonDataCache(%rip)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L83
	leaq	_ZL24DataCacheElement_deleterPv(%rip), %rsi
	call	uhash_setValueDeleter_67@PLT
	leaq	_ZL13udata_cleanupv(%rip), %rsi
	movl	$20, %edi
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
.L83:
	leaq	_ZL24gCommonDataCacheInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL24gCommonDataCacheInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L84:
	movl	(%rbx), %eax
	movq	_ZL16gCommonDataCache(%rip), %r13
	testl	%eax, %eax
	jg	.L81
	movl	$47, %esi
	movq	%r12, %rdi
	call	strrchr@PLT
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmovne	%rdx, %r12
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	uhash_get_67@PLT
	xorl	%edi, %edi
	movq	%rax, %rbx
	call	umtx_unlock_67@PLT
	testq	%rbx, %rbx
	je	.L81
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3054:
	.size	_ZL20udata_findCachedDataPKcR10UErrorCode, .-_ZL20udata_findCachedDataPKcR10UErrorCode
	.p2align 4
	.type	_ZL19udata_cacheDataItemPKcP11UDataMemoryP10UErrorCode, @function
_ZL19udata_cacheDataItemPKcP11UDataMemoryP10UErrorCode:
.LFB3055:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	testl	%ecx, %ecx
	jle	.L128
.L106:
	xorl	%r12d, %r12d
.L105:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movl	_ZL24gCommonDataCacheInitOnce(%rip), %eax
	movq	%rdi, %r14
	movq	%rsi, %r12
	movq	%rdx, %rbx
	cmpl	$2, %eax
	jne	.L130
.L107:
	movl	4+_ZL24gCommonDataCacheInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L109
	movl	%eax, (%rbx)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	_ZL24gCommonDataCacheInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L107
	movq	uhash_hashChars_67@GOTPCREL(%rip), %rdi
	movq	uhash_compareChars_67@GOTPCREL(%rip), %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	call	uhash_open_67@PLT
	movq	%rax, _ZL16gCommonDataCache(%rip)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L108
	leaq	_ZL24DataCacheElement_deleterPv(%rip), %rsi
	call	uhash_setValueDeleter_67@PLT
	leaq	_ZL13udata_cleanupv(%rip), %rsi
	movl	$20, %edi
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
.L108:
	leaq	_ZL24gCommonDataCacheInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL24gCommonDataCacheInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L109:
	movl	(%rbx), %edx
	movq	_ZL16gCommonDataCache(%rip), %r15
	testl	%edx, %edx
	jg	.L106
	movl	$16, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L131
	movq	%rbx, %rdi
	call	UDataMemory_createNewInstance_67@PLT
	movq	%rax, 8(%r13)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L132
	movq	%r12, %rsi
	call	UDatamemory_assign_67@PLT
	movl	$47, %esi
	movq	%r14, %rdi
	call	strrchr@PLT
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmove	%r14, %rsi
	movq	%rsi, %rdi
	movq	%rsi, -72(%rbp)
	call	strlen@PLT
	leal	1(%rax), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	-72(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 0(%r13)
	movq	%rax, %r12
	je	.L133
	movq	%rax, %rdi
	call	strcpy@PLT
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	uhash_get_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L115
	xorl	%edi, %edi
	movl	$-127, -60(%rbp)
	call	umtx_unlock_67@PLT
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L116
	testl	%eax, %eax
	jg	.L116
.L118:
	movq	8(%r13), %r12
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L116:
	movl	%eax, (%rbx)
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	movq	8(%r13), %rdi
	call	uprv_free_67@PLT
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movq	8(%r12), %r12
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L115:
	movq	0(%r13), %rsi
	leaq	-60(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	uhash_put_67@PLT
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L117
	cmpl	$-127, %eax
	jne	.L118
.L117:
	movl	%eax, (%rbx)
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	movq	8(%r13), %rdi
	call	uprv_free_67@PLT
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	jmp	.L105
.L129:
	call	__stack_chk_fail@PLT
.L131:
	movl	$7, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L105
.L133:
	movl	$7, (%rbx)
	movq	8(%r13), %rdi
	call	uprv_free_67@PLT
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	jmp	.L105
	.cfi_endproc
.LFE3055:
	.size	_ZL19udata_cacheDataItemPKcP11UDataMemoryP10UErrorCode, .-_ZL19udata_cacheDataItemPKcP11UDataMemoryP10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3335:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3335:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3338:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L147
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L135
	cmpb	$0, 12(%rbx)
	jne	.L148
.L139:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L135:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L139
	.cfi_endproc
.LFE3338:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3341:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L151
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3341:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3344:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L154
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3344:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L160
.L156:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L161
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3346:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3347:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3347:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3348:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3348:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3349:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3349:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3350:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3350:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3351:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3351:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3352:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L177
	testl	%edx, %edx
	jle	.L177
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L180
.L169:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L169
	.cfi_endproc
.LFE3352:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L184
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L184
	testl	%r12d, %r12d
	jg	.L191
	cmpb	$0, 12(%rbx)
	jne	.L192
.L186:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L186
.L192:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L184:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3353:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L194
	movq	(%rdi), %r8
.L195:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L198
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L198
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L198:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3354:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3355:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L205
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3355:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3356:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3356:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3357:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3357:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3358:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3358:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3360:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3360:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3362:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3362:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.section	.rodata.str1.1
.LC1:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UDataPathIteratorC2EPKcS2_S2_S2_aP10UErrorCode
	.type	_ZN6icu_6717UDataPathIteratorC2EPKcS2_S2_S2_aP10UErrorCode, @function
_ZN6icu_6717UDataPathIteratorC2EPKcS2_S2_S2_aP10UErrorCode:
.LFB3057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	%r9d, -84(%rbp)
	movq	16(%rbp), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	61(%rdi), %rax
	movl	$0, 32(%rdi)
	movq	%rax, 48(%rdi)
	xorl	%eax, %eax
	movw	%ax, 60(%rdi)
	leaq	125(%rdi), %rax
	movq	%rax, 112(%rdi)
	leaq	189(%rdi), %rax
	movq	$0, 24(%rdi)
	movl	$0, 104(%rdi)
	movl	$40, 56(%rdi)
	movl	$0, 168(%rdi)
	movl	$40, 120(%rdi)
	movw	%dx, 124(%rdi)
	movq	%rax, 176(%rdi)
	movl	$0, 232(%rdi)
	movl	$40, 184(%rdi)
	movw	%cx, 188(%rdi)
	testq	%rsi, %rsi
	je	.L228
	movq	%rsi, (%rdi)
.L213:
	testq	%r11, %r11
	movq	%r11, -96(%rbp)
	leaq	-80(%rbp), %r15
	je	.L214
	movq	%r14, %rdx
	leaq	176(%rbx), %rdi
	movl	$47, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-96(%rbp), %r11
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	movq	%r11, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-104(%rbp), %r8
	movl	-72(%rbp), %edx
	movq	%r14, %rcx
	movq	-80(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L214:
	movl	$47, %esi
	movq	%r13, %rdi
	call	strrchr@PLT
	testq	%rax, %rax
	je	.L215
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	%rdx, %rdi
	movq	%rdx, -96(%rbp)
	call	strlen@PLT
	movq	-96(%rbp), %rdx
	movl	%eax, 40(%rbx)
	cmpq	%rdx, %r13
	je	.L220
	movq	%r13, %rsi
	subq	%r13, %rdx
	leaq	48(%rbx), %rdi
	movq	%r14, %rcx
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	48(%rbx), %rax
	movq	%r12, %rsi
	movq	%rax, 8(%rbx)
	testq	%r12, %r12
	je	.L229
.L227:
	movq	%r15, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-80(%rbp), %rax
	movq	%rax, 24(%rbx)
	movl	-72(%rbp), %eax
	movl	%eax, 32(%rbx)
	movzbl	-84(%rbp), %eax
	movb	%al, 240(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	%r13, 16(%rbx)
	movq	%r13, %rdi
	call	strlen@PLT
	movl	%eax, 40(%rbx)
.L220:
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	%rax, 8(%rbx)
	testq	%r12, %r12
	jne	.L227
.L229:
	leaq	.LC1(%rip), %rsi
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%r11, -96(%rbp)
	call	u_getDataDirectory_67@PLT
	movq	-96(%rbp), %r11
	movq	%rax, (%rbx)
	jmp	.L213
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3057:
	.size	_ZN6icu_6717UDataPathIteratorC2EPKcS2_S2_S2_aP10UErrorCode, .-_ZN6icu_6717UDataPathIteratorC2EPKcS2_S2_S2_aP10UErrorCode
	.globl	_ZN6icu_6717UDataPathIteratorC1EPKcS2_S2_S2_aP10UErrorCode
	.set	_ZN6icu_6717UDataPathIteratorC1EPKcS2_S2_S2_aP10UErrorCode,_ZN6icu_6717UDataPathIteratorC2EPKcS2_S2_S2_aP10UErrorCode
	.p2align 4
	.type	_ZL25doLoadFromIndividualFilesPKcS0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_, @function
_ZL25doLoadFromIndividualFilesPKcS0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_:
.LFB3068:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-368(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-304(%rbp), %rbx
	subq	$384, %rsp
	movq	16(%rbp), %rax
	movq	40(%rbp), %r15
	movq	%r8, -384(%rbp)
	movq	%r9, -392(%rbp)
	movq	%rdx, %r8
	xorl	%r9d, %r9d
	movq	%rdi, %rdx
	movq	%rax, -376(%rbp)
	movq	24(%rbp), %rax
	movq	%rbx, %rdi
	movq	32(%rbp), %r14
	movq	%rax, -400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	pushq	%r15
	call	_ZN6icu_6717UDataPathIteratorC1EPKcS2_S2_S2_aP10UErrorCode
	popq	%rdi
	popq	%r8
.L233:
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L232
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode.part.0
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L232
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	uprv_mapFile_67@PLT
	testb	%al, %al
	je	.L233
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L235
	movq	-360(%rbp), %r8
	cmpw	$10202, 2(%r8)
	jne	.L238
	movq	-376(%rbp), %rax
	testq	%rax, %rax
	je	.L237
	movq	%r8, -408(%rbp)
	leaq	4(%r8), %rcx
	movq	-392(%rbp), %rdx
	movq	-384(%rbp), %rsi
	movq	-400(%rbp), %rdi
	call	*%rax
	movq	-408(%rbp), %r8
	testb	%al, %al
	je	.L238
.L237:
	movq	%r15, %rdi
	movq	%r8, -408(%rbp)
	call	UDataMemory_createNewInstance_67@PLT
	movq	-408(%rbp), %r8
	movq	%rax, %r13
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L235
	movq	%r8, 8(%r13)
	movdqa	-336(%rbp), %xmm0
	movups	%xmm0, 32(%r13)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L232:
	xorl	%r13d, %r13d
.L243:
	cmpb	$0, -116(%rbp)
	jne	.L257
.L240:
	cmpb	$0, -180(%rbp)
	jne	.L258
.L241:
	cmpb	$0, -244(%rbp)
	jne	.L259
.L231:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L258:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L257:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$3, (%r14)
.L235:
	movq	%r12, %rdi
	call	udata_close_67@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L232
	movl	$3, (%r14)
	jmp	.L233
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3068:
	.size	_ZL25doLoadFromIndividualFilesPKcS0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_, .-_ZL25doLoadFromIndividualFilesPKcS0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_
	.section	.rodata.str1.1
.LC2:
	.string	"icudt67l"
	.text
	.p2align 4
	.type	_ZL20doLoadFromCommonDataaPKcS0_S0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_.constprop.0, @function
_ZL20doLoadFromCommonDataaPKcS0_S0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_.constprop.0:
.LFB3912:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movb	%dil, -433(%rbp)
	movsbl	%dil, %edi
	movq	%rsi, -456(%rbp)
	movq	24(%rbp), %r12
	leal	-1(%rdi), %ebx
	movq	%rax, -496(%rbp)
	movq	32(%rbp), %rax
	movq	%rdx, -464(%rbp)
	movq	%rcx, -480(%rbp)
	movq	%r8, -488(%rbp)
	movq	%r9, -472(%rbp)
	movq	%rax, -448(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -434(%rbp)
	movl	(%r12), %eax
.L262:
	testl	%eax, %eax
	jg	.L297
.L547:
	leaq	-432(%rbp), %r13
	movq	%r13, %rdi
	call	UDataMemory_init_67@PLT
	cmpl	$-1, %ebx
	je	.L264
	cmpl	$9, %ebx
	jg	.L545
	xorl	%edi, %edi
	leaq	_ZL19gCommonICUDataArray(%rip), %r13
	movslq	%ebx, %r14
	call	umtx_lock_67@PLT
	movq	0(%r13,%r14,8), %r15
	testq	%r15, %r15
	je	.L546
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L289
.L290:
	cmpl	$7, %eax
	je	.L294
.L554:
	cmpb	$0, -433(%rbp)
	je	.L528
	testq	%r15, %r15
	je	.L295
	addl	$1, %ebx
	testl	%eax, %eax
	jle	.L547
.L297:
	cmpl	$7, %eax
	je	.L294
.L519:
	cmpb	$0, -433(%rbp)
	je	.L528
.L295:
	cmpb	$0, -434(%rbp)
	jne	.L528
	movl	_ZL26gHaveTriedToLoadCommonData(%rip), %eax
	testl	%eax, %eax
	jne	.L318
	movl	(%r12), %r9d
	testl	%r9d, %r9d
	jg	.L548
	leaq	-368(%rbp), %r14
	movq	%r14, %rdi
	call	UDataMemory_init_67@PLT
	movq	%r12, %rsi
	leaq	.LC2(%rip), %rdi
	call	_ZL20udata_findCachedDataPKcR10UErrorCode
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L549
	movq	%r14, %rdi
	call	UDataMemory_init_67@PLT
.L346:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	UDatamemory_assign_67@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movaps	%xmm0, -336(%rbp)
	call	_ZL16setCommonICUDataP11UDataMemoryaP10UErrorCode
.L320:
	movl	$1, _ZL26gHaveTriedToLoadCommonData(%rip)
.L318:
	movq	%r12, %rsi
	leaq	.LC2(%rip), %rdi
	call	_ZL20udata_findCachedDataPKcR10UErrorCode
	movq	%rax, %r13
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L528
	testq	%r13, %r13
	je	.L528
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L336
	movq	8(%rax), %rax
	cmpq	%rax, 8(%r13)
	je	.L515
.L336:
	movq	8+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L337
	movq	8(%r13), %rcx
	cmpq	%rcx, 8(%rax)
	je	.L515
.L337:
	movq	16+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L338
	movq	8(%r13), %rcx
	cmpq	%rcx, 8(%rax)
	je	.L515
.L338:
	movq	24+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L339
	movq	8(%r13), %rcx
	cmpq	%rcx, 8(%rax)
	je	.L515
.L339:
	movq	32+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L340
	movq	8(%rax), %rax
	cmpq	%rax, 8(%r13)
	je	.L515
.L340:
	movq	40+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L341
	movq	8(%rax), %rax
	cmpq	%rax, 8(%r13)
	je	.L515
.L341:
	movq	48+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L342
	movq	8(%r13), %rcx
	cmpq	%rcx, 8(%rax)
	je	.L515
.L342:
	movq	56+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L343
	movq	8(%r13), %rcx
	cmpq	%rcx, 8(%rax)
	je	.L515
.L343:
	movq	64+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L344
	movq	8(%r13), %rcx
	cmpq	%rcx, 8(%rax)
	je	.L515
.L344:
	movq	72+_ZL19gCommonICUDataArray(%rip), %rax
	testq	%rax, %rax
	je	.L345
	movq	8(%r13), %rcx
	cmpq	%rcx, 8(%rax)
	je	.L515
.L345:
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L528:
	xorl	%eax, %eax
.L261:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L550
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	testl	%ebx, %ebx
	je	.L269
	movq	_ZL19gCommonICUDataArray(%rip), %rdx
	leaq	icudt67_dat(%rip), %rax
	cmpq	%rax, 8(%rdx)
	je	.L277
	cmpl	$1, %ebx
	je	.L269
	movq	8+_ZL19gCommonICUDataArray(%rip), %rdx
	cmpq	%rax, 8(%rdx)
	je	.L277
	cmpl	$2, %ebx
	je	.L269
	movq	16+_ZL19gCommonICUDataArray(%rip), %rdx
	cmpq	%rax, 8(%rdx)
	je	.L277
	cmpl	$3, %ebx
	je	.L269
	movq	24+_ZL19gCommonICUDataArray(%rip), %rdx
	cmpq	%rax, 8(%rdx)
	je	.L277
	cmpl	$4, %ebx
	je	.L269
	movq	32+_ZL19gCommonICUDataArray(%rip), %rdx
	cmpq	%rax, 8(%rdx)
	je	.L277
	cmpl	$5, %ebx
	je	.L269
	movq	40+_ZL19gCommonICUDataArray(%rip), %rdx
	cmpq	%rax, 8(%rdx)
	je	.L277
	cmpl	$6, %ebx
	je	.L269
	movq	48+_ZL19gCommonICUDataArray(%rip), %rdx
	cmpq	%rax, 8(%rdx)
	je	.L277
	cmpl	$7, %ebx
	je	.L269
	movq	56+_ZL19gCommonICUDataArray(%rip), %rdx
	cmpq	%rax, 8(%rdx)
	je	.L277
	cmpl	$8, %ebx
	je	.L269
	movq	64+_ZL19gCommonICUDataArray(%rip), %rdx
	cmpq	%rax, 8(%rdx)
	je	.L278
.L269:
	leaq	-368(%rbp), %r15
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movq	%r15, %rdi
	call	UDataMemory_init_67@PLT
	movq	%r15, %rdi
	leaq	icudt67_dat(%rip), %rsi
	call	UDataMemory_setData_67@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	udata_checkCommonData_67@PLT
	movq	%r15, %rdi
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	_ZL16setCommonICUDataP11UDataMemoryaP10UErrorCode
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	xorl	%edi, %edi
	movq	0(%r13,%r14,8), %r15
	call	umtx_unlock_67@PLT
.L279:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L290
	testq	%r15, %r15
	je	.L519
.L289:
	movq	(%r15), %rax
	movq	-456(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	leaq	-368(%rbp), %rdx
	call	*(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L551
	movq	-448(%rbp), %rax
	movl	(%rax), %r14d
	testl	%r14d, %r14d
	jg	.L528
	cmpw	$10202, 2(%r13)
	jne	.L311
	cmpq	$0, -472(%rbp)
	je	.L314
	movq	-488(%rbp), %rdx
	movq	-480(%rbp), %rsi
	leaq	4(%r13), %rcx
	movq	-496(%rbp), %rdi
	movq	-472(%rbp), %rax
	call	*%rax
	testb	%al, %al
	je	.L311
.L314:
	movq	-448(%rbp), %rbx
	movq	%rbx, %rdi
	call	UDataMemory_createNewInstance_67@PLT
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jg	.L528
	movl	-368(%rbp), %edx
	movq	%r13, 8(%rax)
	movl	%edx, 48(%rax)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-464(%rbp), %r15
	movl	$47, %esi
	movq	%r15, %rdi
	call	strrchr@PLT
	testq	%rax, %rax
	leaq	1(%rax), %r14
	cmove	%r15, %r14
	cmpb	$0, (%r14)
	je	.L552
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZL20udata_findCachedDataPKcR10UErrorCode
	movq	%rax, %r15
	movl	(%r12), %eax
	testq	%r15, %r15
	je	.L553
	testl	%eax, %eax
	jle	.L289
	cmpl	$7, %eax
	jne	.L554
	.p2align 4,,10
	.p2align 3
.L294:
	movq	-448(%rbp), %rax
	movl	$7, (%rax)
	xorl	%eax, %eax
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L551:
	movl	(%r12), %eax
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L545:
	cmpl	$7, (%r12)
	jne	.L519
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L553:
	testl	%eax, %eax
	jg	.L297
	call	u_getDataDirectory_67@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	-464(%rbp), %rcx
	pushq	%r12
	movq	%rax, %rsi
	leaq	-304(%rbp), %rax
	movl	$1, %r9d
	movq	%rax, %rdi
	leaq	.LC0(%rip), %r8
	movq	%rax, -504(%rbp)
	call	_ZN6icu_6717UDataPathIteratorC1EPKcS2_S2_S2_aP10UErrorCode
	popq	%rax
	movq	%r13, %rdi
	popq	%rdx
	movq	%r14, -512(%rbp)
	movq	-504(%rbp), %r14
	call	UDataMemory_isLoaded_67@PLT
	testb	%al, %al
	jne	.L303
.L298:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L302
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode.part.0
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L303
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	uprv_mapFile_67@PLT
	movq	%r13, %rdi
	call	UDataMemory_isLoaded_67@PLT
	testb	%al, %al
	je	.L298
.L303:
	movl	(%r12), %eax
	movq	-512(%rbp), %r14
	testl	%eax, %eax
	jg	.L302
	movq	%r13, %rdi
	call	UDataMemory_isLoaded_67@PLT
	testb	%al, %al
	jne	.L305
	movl	$4, (%r12)
.L302:
	cmpb	$0, -116(%rbp)
	jne	.L555
.L306:
	cmpb	$0, -180(%rbp)
	jne	.L556
.L307:
	cmpb	$0, -244(%rbp)
	je	.L279
.L558:
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L552:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L297
	movl	$4, (%r12)
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L311:
	movq	-448(%rbp), %rax
	movl	$3, (%r12)
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L528
	cmpb	$0, -433(%rbp)
	je	.L528
	addl	$1, %ebx
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L548:
	leaq	-368(%rbp), %rdi
	call	UDataMemory_init_67@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L515:
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movb	$1, -434(%rbp)
	movl	(%r12), %eax
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L277:
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L297
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L549:
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L557
	movq	%r14, %rdi
	call	UDataMemory_init_67@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L555:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -180(%rbp)
	je	.L307
.L556:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -244(%rbp)
	je	.L279
	jmp	.L558
.L305:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	udata_checkCommonData_67@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZL19udata_cacheDataItemPKcP11UDataMemoryP10UErrorCode
	cmpb	$0, -116(%rbp)
	movq	%rax, %r15
	je	.L306
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L557:
	call	u_getDataDirectory_67@PLT
	subq	$8, %rsp
	leaq	.LC2(%rip), %rcx
	leaq	-304(%rbp), %r15
	pushq	%r12
	movq	%rax, %rsi
	movq	%r15, %rdi
	movl	$1, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rcx, %rdx
	call	_ZN6icu_6717UDataPathIteratorC1EPKcS2_S2_S2_aP10UErrorCode
	popq	%rsi
	popq	%rdi
	movq	%r14, %rdi
	call	UDataMemory_isLoaded_67@PLT
	testb	%al, %al
	jne	.L328
.L323:
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L327
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode.part.0
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L328
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	uprv_mapFile_67@PLT
	movq	%r14, %rdi
	call	UDataMemory_isLoaded_67@PLT
	testb	%al, %al
	je	.L323
.L328:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L327
	movq	%r14, %rdi
	call	UDataMemory_isLoaded_67@PLT
	testb	%al, %al
	jne	.L330
	movl	$4, (%r12)
	.p2align 4,,10
	.p2align 3
.L327:
	cmpb	$0, -116(%rbp)
	jne	.L559
.L331:
	cmpb	$0, -180(%rbp)
	jne	.L560
.L332:
	cmpb	$0, -244(%rbp)
	jne	.L561
.L333:
	movq	%r14, %rdi
	call	UDataMemory_init_67@PLT
	testq	%r13, %r13
	jne	.L346
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L559:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L331
.L561:
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L333
.L560:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L332
.L330:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	udata_checkCommonData_67@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	leaq	.LC2(%rip), %rdi
	call	_ZL19udata_cacheDataItemPKcP11UDataMemoryP10UErrorCode
	movq	%rax, %r13
	jmp	.L327
.L278:
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L562
	cmpl	$7, %eax
	je	.L294
	cmpb	$0, -433(%rbp)
	je	.L528
	movl	$9, %ebx
	jmp	.L295
.L562:
	movl	$9, %ebx
	jmp	.L519
.L550:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3912:
	.size	_ZL20doLoadFromCommonDataaPKcS0_S0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_.constprop.0, .-_ZL20doLoadFromCommonDataaPKcS0_S0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_.constprop.0
	.section	.rodata.str1.1
.LC3:
	.string	"ICUDATA"
.LC4:
	.string	"icudt67l-"
.LC5:
	.string	"ICUDATA-"
.LC6:
	.string	"."
.LC7:
	.string	"res"
.LC8:
	.string	"zoneinfo64"
.LC9:
	.string	"timezoneTypes"
.LC10:
	.string	"windowsZones"
.LC11:
	.string	"metaZones"
	.text
	.p2align 4
	.type	_ZL12doOpenChoicePKcS0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCode, @function
_ZL12doOpenChoicePKcS0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCode:
.LFB3071:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$392, %rsp
	movq	%rsi, -384(%rbp)
	movq	%rdx, -376(%rbp)
	movq	%rcx, -416(%rbp)
	movq	%r8, -424(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, -392(%rbp)
	movl	$0, -356(%rbp)
	testq	%rdi, %rdi
	je	.L564
	movl	$8, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L630
.L564:
	leaq	-307(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	xorl	%r11d, %r11d
	movq	%rax, -320(%rbp)
	xorl	%r13d, %r13d
	leaq	-243(%rbp), %rax
	leaq	-192(%rbp), %r15
	movq	%rax, -256(%rbp)
	leaq	-179(%rbp), %rax
	leaq	-336(%rbp), %r14
	movq	%rax, -192(%rbp)
	leaq	-128(%rbp), %rax
	leaq	.LC2(%rip), %rsi
	movq	%rax, -400(%rbp)
	leaq	-115(%rbp), %rax
	movl	$0, -264(%rbp)
	movl	$40, -312(%rbp)
	movw	%r9w, -308(%rbp)
	movl	$0, -200(%rbp)
	movl	$40, -248(%rbp)
	movw	%r10w, -244(%rbp)
	movl	$0, -136(%rbp)
	movl	$40, -184(%rbp)
	movw	%r11w, -180(%rbp)
	movq	%rax, -128(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	movw	%r13w, -116(%rbp)
	testq	%r12, %r12
	je	.L626
	movl	$47, %esi
	movq	%r12, %rdi
	call	strrchr@PLT
	movq	%r12, %rdi
	movl	$47, %esi
	movq	%rax, %r14
	call	strchr@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	uprv_pathIsAbsolute_67@PLT
	cmpq	%r13, %r14
	jne	.L567
	testb	%al, %al
	je	.L631
.L567:
	testq	%r14, %r14
	je	.L570
	leaq	1(%r14), %rsi
	leaq	-336(%rbp), %r14
.L626:
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-328(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-336(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L627:
	movq	-192(%rbp), %rsi
.L566:
	leaq	-320(%rbp), %r8
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	leaq	-256(%rbp), %r13
	movq	%r8, %rdi
	movq	%r8, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-136(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-192(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-72(%rbp), %r8d
	movl	-264(%rbp), %eax
	testl	%r8d, %r8d
	movl	%eax, -400(%rbp)
	movq	-408(%rbp), %r8
	jne	.L632
.L574:
	movq	%r8, %rdi
	movq	%rbx, %rdx
	movl	$47, %esi
	movq	%r8, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-376(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-328(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-336(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rdx
	movl	$47, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-376(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-328(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-336(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-384(%rbp), %rax
	testq	%rax, %rax
	je	.L575
	cmpb	$0, (%rax)
	movq	-408(%rbp), %r8
	jne	.L633
.L575:
	movslq	-400(%rbp), %r14
	movq	-256(%rbp), %rax
	testq	%r12, %r12
	leaq	1(%rax,%r14), %r13
	leaq	.LC2(%rip), %rax
	cmove	%rax, %r12
	call	u_getDataDirectory_67@PLT
	cmpb	$0, -392(%rbp)
	movq	%rax, %r14
	je	.L580
	movq	-384(%rbp), %rsi
	movl	$4, %ecx
	leaq	.LC7(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L634
.L580:
	movl	_ZL15gDataFileAccess(%rip), %eax
	cmpl	$2, %eax
	je	.L635
.L579:
	testl	$-3, %eax
	jne	.L584
	testq	%r14, %r14
	je	.L585
	cmpb	$0, (%r14)
	je	.L585
.L586:
	leaq	-356(%rbp), %r10
	pushq	%rbx
	movq	%r12, %rcx
	movq	%r13, %rdx
	pushq	%r10
	movq	-376(%rbp), %r9
	movq	%r14, %rsi
	pushq	-424(%rbp)
	movq	-384(%rbp), %r8
	movq	-192(%rbp), %rdi
	pushq	-416(%rbp)
	call	_ZL25doLoadFromIndividualFilesPKcS0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_
	addq	$32, %rsp
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L636
.L582:
	cmpb	$0, -116(%rbp)
	jne	.L637
.L594:
	cmpb	$0, -180(%rbp)
	jne	.L638
.L595:
	cmpb	$0, -244(%rbp)
	jne	.L639
.L596:
	cmpb	$0, -308(%rbp)
	jne	.L640
.L563:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L641
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	movl	$9, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L564
	movl	$8, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	-392(%rbp)
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L634:
	movq	-376(%rbp), %rsi
	movl	$11, %ecx
	leaq	.LC8(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L642
.L581:
	movq	%rbx, %rdi
	call	u_getTimeZoneFilesDirectory_67@PLT
	cmpb	$0, (%rax)
	je	.L580
	leaq	-356(%rbp), %r10
	pushq	%rbx
	movq	%r13, %rdx
	movq	%rax, %rsi
	pushq	%r10
	leaq	.LC1(%rip), %rcx
	movq	-376(%rbp), %r9
	pushq	-424(%rbp)
	movq	-384(%rbp), %r8
	movq	%rcx, %rdi
	pushq	-416(%rbp)
	call	_ZL25doLoadFromIndividualFilesPKcS0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_
	addq	$32, %rsp
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L582
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L580
	.p2align 4,,10
	.p2align 3
.L628:
	xorl	%r15d, %r15d
.L644:
	cmpb	$0, -116(%rbp)
	je	.L594
.L637:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -180(%rbp)
	je	.L595
.L638:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -244(%rbp)
	je	.L596
.L639:
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -308(%rbp)
	je	.L563
.L640:
	movq	-320(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	-352(%rbp), %r9
	leaq	.LC6(%rip), %rsi
	movq	%r8, -432(%rbp)
	movq	%r9, %rdi
	movq	%r9, -408(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-432(%rbp), %r8
	movl	-344(%rbp), %edx
	movq	%rbx, %rcx
	movq	-352(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-384(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-328(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-336(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-408(%rbp), %r9
	leaq	.LC6(%rip), %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-344(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-352(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-384(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-328(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	-336(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L585:
	cmpb	$0, -392(%rbp)
	je	.L586
	cmpl	$2, %eax
	jne	.L599
.L590:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L628
.L593:
	movl	-356(%rbp), %eax
	testl	%eax, %eax
	jle	.L643
	movl	%eax, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L636:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L628
	movl	_ZL15gDataFileAccess(%rip), %eax
.L584:
	cmpl	$1, %eax
	jbe	.L599
	cmpl	$3, %eax
	jne	.L590
	movsbl	-392(%rbp), %r13d
	leaq	-356(%rbp), %r10
.L598:
	subq	$8, %rsp
	movq	-416(%rbp), %r9
	movq	-376(%rbp), %r8
	movq	%r12, %rdx
	pushq	%rbx
	movq	-384(%rbp), %rcx
	movl	%r13d, %edi
	pushq	%r10
	movq	-320(%rbp), %rsi
	pushq	-424(%rbp)
	call	_ZL20doLoadFromCommonDataaPKcS0_S0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_.constprop.0
	addq	$32, %rsp
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L582
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L632:
	movq	%r8, %rdi
	movq	%rbx, %rdx
	movl	$47, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-72(%rbp), %edx
	movq	-128(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rdx
	movl	$47, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-72(%rbp), %edx
	movq	-128(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-408(%rbp), %r8
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L570:
	leaq	-336(%rbp), %r14
.L573:
	movq	%r12, %rsi
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L642:
	movq	-376(%rbp), %rsi
	movl	$14, %ecx
	leaq	.LC9(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L581
	movq	-376(%rbp), %rsi
	movl	$13, %ecx
	leaq	.LC10(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L581
	movq	-376(%rbp), %rsi
	movl	$10, %ecx
	leaq	.LC11(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L580
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$45, %esi
	movq	%r12, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L645
	leaq	-336(%rbp), %r14
	leaq	1(%rax), %rsi
	movq	%rax, -408(%rbp)
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-328(%rbp), %edx
	movq	-336(%rbp), %rsi
	movq	%rbx, %rcx
	movq	-400(%rbp), %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	cmpb	$0, -392(%rbp)
	movq	-408(%rbp), %r8
	jne	.L629
	movq	%r8, %rdx
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	subq	%r12, %rdx
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	testq	%r13, %r13
	jne	.L627
	movq	-192(%rbp), %r12
	movq	%r12, %rsi
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L643:
	movl	$4, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L599:
	subq	$8, %rsp
	leaq	-356(%rbp), %r10
	movsbl	-392(%rbp), %r13d
	movq	-416(%rbp), %r9
	pushq	%rbx
	movq	-376(%rbp), %r8
	movq	%r12, %rdx
	pushq	%r10
	movq	-384(%rbp), %rcx
	movl	%r13d, %edi
	pushq	-424(%rbp)
	movq	-320(%rbp), %rsi
	movq	%r10, -392(%rbp)
	call	_ZL20doLoadFromCommonDataaPKcS0_S0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_.constprop.0
	addq	$32, %rsp
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L582
	movl	(%rbx), %edx
	movq	-392(%rbp), %r10
	testl	%edx, %edx
	jg	.L628
	cmpl	$3, _ZL15gDataFileAccess(%rip)
	jne	.L593
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L645:
	cmpb	$0, -392(%rbp)
	leaq	-336(%rbp), %r14
	je	.L573
.L629:
	leaq	.LC2(%rip), %rsi
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L635:
	subq	$8, %rsp
	leaq	-356(%rbp), %r10
	movsbl	-392(%rbp), %edi
	movq	-416(%rbp), %r9
	pushq	%rbx
	movq	-376(%rbp), %r8
	movq	%r12, %rdx
	movq	-384(%rbp), %rcx
	movq	-320(%rbp), %rsi
	pushq	%r10
	pushq	-424(%rbp)
	call	_ZL20doLoadFromCommonDataaPKcS0_S0_S0_S0_S0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCodeS8_.constprop.0
	addq	$32, %rsp
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L582
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L628
	movl	_ZL15gDataFileAccess(%rip), %eax
	jmp	.L579
.L641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3071:
	.size	_ZL12doOpenChoicePKcS0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCode, .-_ZL12doOpenChoicePKcS0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode
	.type	_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode, @function
_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode:
.LFB3059:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L647
	jmp	_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L647:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3059:
	.size	_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode, .-_ZN6icu_6717UDataPathIterator4nextEP10UErrorCode
	.p2align 4
	.globl	udata_setCommonData_67
	.type	udata_setCommonData_67, @function
udata_setCommonData_67:
.LFB3065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$72, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L648
	movl	(%rsi), %edx
	movq	%rsi, %r12
	testl	%edx, %edx
	jg	.L648
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L658
	leaq	-96(%rbp), %r14
	movq	%r14, %rdi
	call	UDataMemory_init_67@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	UDataMemory_setData_67@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	udata_checkCommonData_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L659
	.p2align 4,,10
	.p2align 3
.L648:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L660
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L659:
	.cfi_restore_state
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZL16setCommonICUDataP11UDataMemoryaP10UErrorCode
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L658:
	movl	$1, (%rsi)
	jmp	.L648
.L660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3065:
	.size	udata_setCommonData_67, .-udata_setCommonData_67
	.p2align 4
	.globl	udata_setAppData_67
	.type	udata_setAppData_67, @function
udata_setAppData_67:
.LFB3066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L661
	movl	(%rdx), %eax
	movq	%rdx, %r12
	testl	%eax, %eax
	jg	.L661
	movq	%rsi, %r15
	testq	%rsi, %rsi
	je	.L670
	leaq	-96(%rbp), %r14
	movq	%rdi, %r13
	movq	%r14, %rdi
	call	UDataMemory_init_67@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	UDataMemory_setData_67@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	udata_checkCommonData_67@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZL19udata_cacheDataItemPKcP11UDataMemoryP10UErrorCode
.L661:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L671
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L670:
	.cfi_restore_state
	movl	$1, (%rdx)
	jmp	.L661
.L671:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3066:
	.size	udata_setAppData_67, .-udata_setAppData_67
	.p2align 4
	.globl	udata_open_67
	.type	udata_open_67, @function
udata_open_67:
.LFB3072:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L672
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L672
	testq	%rdx, %rdx
	je	.L674
	cmpb	$0, (%rdx)
	je	.L674
	movq	%rcx, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	_ZL12doOpenChoicePKcS0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCode
	.p2align 4,,10
	.p2align 3
.L674:
	movl	$1, (%rcx)
.L672:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3072:
	.size	udata_open_67, .-udata_open_67
	.p2align 4
	.globl	udata_openChoice_67
	.type	udata_openChoice_67, @function
udata_openChoice_67:
.LFB3073:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L682
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L682
	testq	%rdx, %rdx
	je	.L684
	cmpb	$0, (%rdx)
	je	.L684
	testq	%rcx, %rcx
	je	.L684
	jmp	_ZL12doOpenChoicePKcS0_S0_PFaPvS0_S0_PK9UDataInfoES1_P10UErrorCode
	.p2align 4,,10
	.p2align 3
.L684:
	movl	$1, (%r9)
.L682:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3073:
	.size	udata_openChoice_67, .-udata_openChoice_67
	.p2align 4
	.globl	udata_getInfo_67
	.type	udata_getInfo_67, @function
udata_getInfo_67:
.LFB3074:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L706
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L696
	movq	8(%rdi), %r12
	testq	%r12, %r12
	je	.L696
	leaq	4(%r12), %rdi
	call	udata_getInfoSize_67@PLT
	movzwl	(%rbx), %edx
	cmpw	%ax, %dx
	jbe	.L697
	movw	%ax, (%rbx)
	movzwl	%ax, %edx
.L697:
	subl	$2, %edx
	leaq	2(%rbx), %rdi
	leaq	6(%r12), %rsi
	movslq	%edx, %rdx
	call	memcpy@PLT
	cmpb	$0, 8(%r12)
	je	.L693
	movzwl	6(%r12), %eax
	rolw	$8, %ax
	movw	%ax, 2(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L696:
	.cfi_restore_state
	xorl	%eax, %eax
	movw	%ax, (%rbx)
.L693:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3074:
	.size	udata_getInfo_67, .-udata_getInfo_67
	.p2align 4
	.globl	udata_setFileAccess_67
	.type	udata_setFileAccess_67, @function
udata_setFileAccess_67:
.LFB3075:
	.cfi_startproc
	endbr64
	movl	%edi, _ZL15gDataFileAccess(%rip)
	ret
	.cfi_endproc
.LFE3075:
	.size	udata_setFileAccess_67, .-udata_setFileAccess_67
	.local	_ZL15gDataFileAccess
	.comm	_ZL15gDataFileAccess,4,4
	.local	_ZL24gCommonDataCacheInitOnce
	.comm	_ZL24gCommonDataCacheInitOnce,8,8
	.local	_ZL16gCommonDataCache
	.comm	_ZL16gCommonDataCache,8,8
	.local	_ZL26gHaveTriedToLoadCommonData
	.comm	_ZL26gHaveTriedToLoadCommonData,4,4
	.local	_ZL19gCommonICUDataArray
	.comm	_ZL19gCommonICUDataArray,80,32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
