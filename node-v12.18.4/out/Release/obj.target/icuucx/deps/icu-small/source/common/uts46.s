	.file	"uts46.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_675UTS46D2Ev
	.type	_ZN6icu_675UTS46D2Ev, @function
_ZN6icu_675UTS46D2Ev:
.LFB2386:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_674IDNAE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2386:
	.size	_ZN6icu_675UTS46D2Ev, .-_ZN6icu_675UTS46D2Ev
	.globl	_ZN6icu_675UTS46D1Ev
	.set	_ZN6icu_675UTS46D1Ev,_ZN6icu_675UTS46D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_675UTS46D0Ev
	.type	_ZN6icu_675UTS46D0Ev, @function
_ZN6icu_675UTS46D0Ev:
.LFB2388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_674IDNAE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2388:
	.size	_ZN6icu_675UTS46D0Ev, .-_ZN6icu_675UTS46D0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_674IDNA16nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_674IDNA16nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_674IDNA16nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode:
.LFB2376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L9
.L5:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L10
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, %r12
	movq	%rcx, %r14
	movq	%r8, %r13
	movq	%rax, -192(%rbp)
	movl	$2, %eax
	leaq	-128(%rbp), %r15
	movq	%r9, %rbx
	movw	%ax, -184(%rbp)
	movq	(%rdi), %rax
	movq	%r15, %rdi
	movq	40(%rax), %rax
	movq	%rax, -208(%rbp)
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	leaq	-192(%rbp), %r9
	movq	%r12, %rdi
	movq	-208(%rbp), %rax
	movq	%r9, -200(%rbp)
	movq	%r9, %rdx
	call	*%rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-200(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L5
.L10:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2376:
	.size	_ZNK6icu_674IDNA16nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_674IDNA16nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode.part.0, @function
_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode.part.0:
.LFB3086:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -240(%rbp)
	movl	%edx, -228(%rbp)
	movq	%r8, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L12
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L13:
	movl	%r13d, %esi
	cmpl	%r12d, %r13d
	jbe	.L14
	leaq	10(%r15), %rdx
	testb	$2, %al
	je	.L52
.L16:
	movslq	%r12d, %rax
	movl	%r13d, %esi
	cmpw	$223, (%rdx,%rax,2)
	jne	.L14
	leal	1(%r13), %esi
.L14:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L51
	movl	$27, %r11d
	testb	$2, 8(%r15)
	jne	.L19
	movl	16(%r15), %r11d
.L19:
	movl	%r12d, %ebx
	xorl	%edx, %edx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L54:
	cmpw	$223, %ax
	jne	.L23
	movslq	%ebx, %rax
	movl	$115, %r9d
	leal	1(%rbx), %edx
	movw	%r9w, (%r14,%rax,2)
	leaq	(%rax,%rax), %r8
	leal	1(%r13), %r9d
	cmpl	%ebx, %r12d
	je	.L25
	leaq	2(%r14,%r8), %rsi
	movl	%edi, %r12d
.L26:
	movl	$115, %edi
	addl	$2, %ebx
	movl	%r9d, %r13d
	movl	$1, %edx
	movw	%di, (%rsi)
.L29:
	cmpl	%ebx, %r13d
	jle	.L53
.L30:
	movslq	%r12d, %rax
	leal	1(%r12), %edi
	movzwl	(%r14,%rax,2), %eax
	cmpw	$962, %ax
	je	.L20
	jbe	.L54
	leal	-8204(%rax), %esi
	cmpw	$1, %si
	ja	.L23
	subl	$1, %r13d
	movl	%edi, %r12d
	movl	$1, %edx
	cmpl	%ebx, %r13d
	jg	.L30
.L53:
	movl	%r13d, %esi
	movq	%r15, %rdi
	movb	%dl, -200(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movzbl	-200(%rbp), %edx
	testb	%dl, %dl
	jne	.L55
.L11:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$200, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movslq	%ebx, %rax
	movl	$963, %ecx
	addl	$1, %ebx
	movl	%edi, %r12d
	movw	%cx, (%r14,%rax,2)
	movl	$1, %edx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L23:
	movslq	%ebx, %rsi
	movl	%edi, %r12d
	addl	$1, %ebx
	movw	%ax, (%r14,%rsi,2)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	%r11d, %r13d
	je	.L57
.L27:
	subl	%edx, %r13d
	leaq	2(%r14,%r8), %rsi
	leaq	4(%r14,%r8), %rdi
	movl	%r9d, -216(%rbp)
	movl	%r13d, %edx
	movl	%r11d, -208(%rbp)
	addl	$2, %r12d
	movq	%rsi, -200(%rbp)
	call	u_memmove_67@PLT
	movl	-216(%rbp), %r9d
	movl	-208(%rbp), %r11d
	movq	-200(%rbp), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L52:
	movq	24(%r15), %rdx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%r13d, %esi
	movq	%r15, %rdi
	movq	%r8, -216(%rbp)
	movl	%edx, -208(%rbp)
	movl	%r9d, -200(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movl	-200(%rbp), %r9d
	movq	%r15, %rdi
	movl	%r9d, %esi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movl	-200(%rbp), %r9d
	movl	-208(%rbp), %edx
	testq	%rax, %rax
	movq	-216(%rbp), %r8
	movq	%rax, %r14
	je	.L51
	movl	$27, %r11d
	testb	$2, 8(%r15)
	jne	.L27
	movl	16(%r15), %r11d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L12:
	movl	12(%rsi), %r13d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r12
	movl	$2147483647, %ecx
	movq	%r15, %rsi
	movq	%rax, -192(%rbp)
	movl	$2, %eax
	movl	-228(%rbp), %edx
	movq	%r12, %rdi
	movw	%ax, -184(%rbp)
	movq	-240(%rbp), %rax
	leaq	-192(%rbp), %rbx
	movq	8(%rax), %r8
	movq	(%r8), %rax
	movq	%r8, -208(%rbp)
	movq	24(%rax), %rax
	movq	%rax, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-224(%rbp), %r14
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	-208(%rbp), %r8
	movq	-200(%rbp), %rax
	movq	%r14, %rcx
	movq	%r8, %rdi
	call	*%rax
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L31
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L32
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L33:
	movl	-228(%rbp), %esi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movl	$2147483647, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movzwl	8(%r15), %eax
	testb	$1, %al
	je	.L34
	movq	-224(%rbp), %rcx
	movl	$7, (%rcx)
.L34:
	testw	%ax, %ax
	js	.L35
	movswl	%ax, %r13d
	sarl	$5, %r13d
.L31:
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L51:
	movq	-224(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L11
.L32:
	movl	-180(%rbp), %r9d
	jmp	.L33
.L35:
	movl	12(%r15), %r13d
	jmp	.L31
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3086:
	.size	_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode.part.0, .-_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZNK6icu_675UTS4615markBadACELabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0, @function
_ZNK6icu_675UTS4615markBadACELabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0:
.LFB3087:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	16(%rdi), %ecx
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	andl	$2, %ecx
	testb	$17, %al
	jne	.L72
	leaq	10(%rsi), %rsi
	testb	$2, %al
	je	.L86
.L59:
	movslq	%edx, %rax
	movslq	%r12d, %rdi
	movl	$1, %r10d
	leaq	(%rsi,%rax,2), %rsi
	leaq	8(%rsi), %rax
	leaq	(%rsi,%rdi,2), %rdi
	movl	$1, %esi
	testl	%ecx, %ecx
	jne	.L73
	xorl	%r14d, %r14d
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r11
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L87:
	cmpw	$46, %cx
	je	.L63
	cmpb	$0, (%r11,%rcx)
	cmovs	%r14d, %esi
.L62:
	addq	$2, %rax
	cmpq	%rax, %rdi
	jbe	.L65
.L64:
	movzwl	(%rax), %ecx
	cmpw	$127, %cx
	jbe	.L87
	addq	$2, %rax
	xorl	%esi, %esi
	xorl	%r10d, %r10d
	cmpq	%rax, %rdi
	ja	.L64
.L65:
	testb	%sil, %sil
	jne	.L88
	testb	%r8b, %r8b
	setne	%dl
	cmpl	$63, %r12d
	setg	%al
	testb	%al, %dl
	je	.L70
	testb	%r10b, %r10b
	jne	.L89
.L70:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L90
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$-3, %r10d
	orl	$512, 4(%r9)
	xorl	%esi, %esi
	movw	%r10w, (%rax)
	xorl	%r10d, %r10d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r11
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L93:
	cmpw	$46, %cx
	je	.L91
	cmpb	$0, (%r11,%rcx)
	js	.L92
.L66:
	addq	$2, %rax
	cmpq	%rax, %rdi
	jbe	.L65
.L61:
	movzwl	(%rax), %ecx
	cmpw	$127, %cx
	jbe	.L93
	xorl	%esi, %esi
	xorl	%r10d, %r10d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$-3, %esi
	orl	$512, 4(%r9)
	xorl	%r10d, %r10d
	movw	%si, (%rax)
	xorl	%esi, %esi
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L86:
	movq	24(%r13), %rsi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L88:
	leal	(%rdx,%r12), %esi
	movl	$-3, %eax
	leaq	-42(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %r9d
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	addl	$1, %r12d
	testb	$1, 8(%r13)
	je	.L70
	movl	$7, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$-3, %ecx
	xorl	%esi, %esi
	xorl	%r10d, %r10d
	movw	%cx, (%rax)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L89:
	orl	$2, 4(%r9)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%esi, %esi
	jmp	.L59
.L90:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3087:
	.size	_ZNK6icu_675UTS4615markBadACELabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0, .-_ZNK6icu_675UTS4615markBadACELabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_674IDNAD2Ev
	.type	_ZN6icu_674IDNAD2Ev, @function
_ZN6icu_674IDNAD2Ev:
.LFB2371:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_674IDNAE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2371:
	.size	_ZN6icu_674IDNAD2Ev, .-_ZN6icu_674IDNAD2Ev
	.globl	_ZN6icu_674IDNAD1Ev
	.set	_ZN6icu_674IDNAD1Ev,_ZN6icu_674IDNAD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_674IDNAD0Ev
	.type	_ZN6icu_674IDNAD0Ev, @function
_ZN6icu_674IDNAD0Ev:
.LFB2373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_674IDNAE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2373:
	.size	_ZN6icu_674IDNAD0Ev, .-_ZN6icu_674IDNAD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"uts46"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_674IDNA19createUTS46InstanceEjR10UErrorCode
	.type	_ZN6icu_674IDNA19createUTS46InstanceEjR10UErrorCode, @function
_ZN6icu_674IDNA19createUTS46InstanceEjR10UErrorCode:
.LFB2378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L103
.L97:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movl	%edi, %r13d
	movl	$24, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L99
	leaq	16+_ZTVN6icu_675UTS46E(%rip), %rax
	movq	%rbx, %rcx
	xorl	%edx, %edx
	xorl	%edi, %edi
	movq	%rax, (%r12)
	leaq	.LC0(%rip), %rsi
	call	_ZN6icu_6711Normalizer211getInstanceEPKcS2_19UNormalization2ModeR10UErrorCode@PLT
	movl	%r13d, 16(%r12)
	movq	%rax, 8(%r12)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L97
	movq	(%r12), %rax
	leaq	_ZN6icu_675UTS46D0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L100
	leaq	16+_ZTVN6icu_674IDNAE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L97
.L99:
	movl	$7, (%rbx)
	jmp	.L97
	.cfi_endproc
.LFE2378:
	.size	_ZN6icu_674IDNA19createUTS46InstanceEjR10UErrorCode, .-_ZN6icu_674IDNA19createUTS46InstanceEjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_675UTS46C2EjR10UErrorCode
	.type	_ZN6icu_675UTS46C2EjR10UErrorCode, @function
_ZN6icu_675UTS46C2EjR10UErrorCode:
.LFB2383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_675UTS46E(%rip), %rax
	movq	%rdx, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	xorl	%edi, %edi
	call	_ZN6icu_6711Normalizer211getInstanceEPKcS2_19UNormalization2ModeR10UErrorCode@PLT
	movl	%r12d, 16(%rbx)
	movq	%rax, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2383:
	.size	_ZN6icu_675UTS46C2EjR10UErrorCode, .-_ZN6icu_675UTS46C2EjR10UErrorCode
	.globl	_ZN6icu_675UTS46C1EjR10UErrorCode
	.set	_ZN6icu_675UTS46C1EjR10UErrorCode,_ZN6icu_675UTS46C2EjR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode, @function
_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode:
.LFB2400:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L107
	jmp	_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2400:
	.size	_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode, .-_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4615markBadACELabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4615markBadACELabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4615markBadACELabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode:
.LFB2404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rbp), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%r12), %r11d
	testl	%r11d, %r11d
	jg	.L108
	movl	%ecx, %ebx
	movzwl	8(%rsi), %eax
	movl	16(%rdi), %ecx
	movq	%rsi, %r13
	andl	$2, %ecx
	testb	$17, %al
	jne	.L123
	leaq	10(%rsi), %rsi
	testb	$2, %al
	jne	.L110
	movq	24(%r13), %rsi
.L110:
	movslq	%edx, %rax
	movslq	%ebx, %rdi
	movl	$1, %r10d
	leaq	(%rsi,%rax,2), %rsi
	leaq	8(%rsi), %rax
	leaq	(%rsi,%rdi,2), %rdi
	movl	$1, %esi
	testl	%ecx, %ecx
	jne	.L124
	xorl	%r14d, %r14d
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r11
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L138:
	cmpw	$46, %cx
	je	.L114
	cmpb	$0, (%r11,%rcx)
	cmovs	%r14d, %esi
.L113:
	addq	$2, %rax
	cmpq	%rax, %rdi
	jbe	.L116
.L115:
	movzwl	(%rax), %ecx
	cmpw	$127, %cx
	jbe	.L138
	addq	$2, %rax
	xorl	%esi, %esi
	xorl	%r10d, %r10d
	cmpq	%rax, %rdi
	ja	.L115
.L116:
	testb	%sil, %sil
	jne	.L139
	testb	%r8b, %r8b
	setne	%dl
	cmpl	$63, %ebx
	setg	%al
	testb	%al, %dl
	je	.L127
	testb	%r10b, %r10b
	jne	.L140
.L127:
	movl	%ebx, %eax
.L108:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L141
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movl	$-3, %r10d
	orl	$512, 4(%r9)
	xorl	%esi, %esi
	movw	%r10w, (%rax)
	xorl	%r10d, %r10d
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r11
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L144:
	cmpw	$46, %cx
	je	.L142
	cmpb	$0, (%r11,%rcx)
	js	.L143
.L117:
	addq	$2, %rax
	cmpq	%rax, %rdi
	jbe	.L116
.L112:
	movzwl	(%rax), %ecx
	cmpw	$127, %cx
	jbe	.L144
	xorl	%esi, %esi
	xorl	%r10d, %r10d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$-3, %esi
	orl	$512, 4(%r9)
	xorl	%r10d, %r10d
	movw	%si, (%rax)
	xorl	%esi, %esi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L123:
	xorl	%esi, %esi
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L140:
	orl	$2, 4(%r9)
	movl	%ebx, %eax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$-3, %eax
	leal	(%rdx,%rbx), %esi
	leaq	-42(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	$1, %r9d
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	leal	1(%rbx), %eax
	testb	$1, 8(%r13)
	je	.L108
	movl	$7, (%r12)
	xorl	%eax, %eax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$-3, %ecx
	xorl	%esi, %esi
	xorl	%r10d, %r10d
	movw	%cx, (%rax)
	jmp	.L117
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2404:
	.size	_ZNK6icu_675UTS4615markBadACELabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4615markBadACELabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4614checkLabelBiDiEPKDsiRNS_8IDNAInfoE
	.type	_ZNK6icu_675UTS4614checkLabelBiDiEPKDsiRNS_8IDNAInfoE, @function
_ZNK6icu_675UTS4614checkLabelBiDiEPKDsiRNS_8IDNAInfoE:
.LFB2405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$1, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movzwl	(%rsi), %edi
	movq	%rcx, -64(%rbp)
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L146
	movzwl	2(%rsi), %eax
	sall	$10, %edi
	movl	$2, %r14d
	leal	-56613888(%rdi,%rax), %edi
.L146:
	call	u_charDirection_67@PLT
	movl	$1, %r13d
	movl	%eax, %ecx
	movl	%eax, -56(%rbp)
	sall	%cl, %r13d
	movl	%r13d, -52(%rbp)
	andl	$-8196, %r13d
	je	.L152
	movq	-64(%rbp), %rax
	movb	$0, 10(%rax)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L149:
	call	u_charDirection_67@PLT
	cmpl	$17, %eax
	jne	.L150
.L151:
	movl	%r13d, %ebx
.L152:
	cmpl	%r14d, %ebx
	jle	.L168
	leal	-1(%rbx), %r13d
	movslq	%r13d, %rax
	movzwl	(%r12,%rax,2), %edi
	leaq	(%rax,%rax), %rcx
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L149
	movzwl	-2(%r12,%rcx), %eax
	leal	-2(%rbx), %r13d
	sall	$10, %eax
	leal	-56613888(%rdi,%rax), %edi
	call	u_charDirection_67@PLT
	cmpl	$17, %eax
	je	.L151
.L150:
	movl	-52(%rbp), %r15d
	movl	$1, %edi
	movl	%eax, %ecx
	movl	-56(%rbp), %edx
	sall	%cl, %edi
	movl	%r13d, %ebx
	movl	%edi, -52(%rbp)
	orl	%edi, %r15d
	testl	%edx, %edx
	je	.L186
.L153:
	testl	$-8231, -52(%rbp)
	setne	%al
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L168:
	movl	-56(%rbp), %edx
	movl	-52(%rbp), %r15d
	testl	%edx, %edx
	jne	.L153
.L186:
	testl	$-6, -52(%rbp)
	setne	%al
.L154:
	testb	%al, %al
	je	.L155
	movq	-64(%rbp), %rax
	movb	$0, 10(%rax)
.L155:
	cmpl	%r14d, %ebx
	jg	.L160
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L157:
	call	u_charDirection_67@PLT
	movl	$1, %edx
	movl	%eax, %ecx
	sall	%cl, %edx
	orl	%edx, %r15d
	cmpl	%r13d, %ebx
	jle	.L156
.L159:
	movl	%r13d, %r14d
.L160:
	movslq	%r14d, %rax
	leal	1(%r14), %r13d
	movzwl	(%r12,%rax,2), %edi
	leaq	(%rax,%rax), %rcx
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L157
	movzwl	2(%r12,%rcx), %eax
	sall	$10, %edi
	leal	2(%r14), %r13d
	leal	-56613888(%rax,%rdi), %edi
	call	u_charDirection_67@PLT
	movl	$1, %esi
	movl	%eax, %ecx
	sall	%cl, %esi
	orl	%esi, %r15d
	cmpl	%r13d, %ebx
	jg	.L159
.L156:
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	jne	.L161
	testl	$-394334, %r15d
	jne	.L162
.L163:
	andl	$8226, %r15d
	je	.L145
	movq	-64(%rbp), %rax
	movb	$1, 9(%rax)
.L145:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	testl	$-402559, %r15d
	je	.L164
	movq	-64(%rbp), %rax
	movb	$0, 10(%rax)
.L164:
	movl	%r15d, %eax
	andl	$36, %eax
	cmpl	$36, %eax
	jne	.L163
.L162:
	movq	-64(%rbp), %rax
	movb	$0, 10(%rax)
	jmp	.L163
	.cfi_endproc
.LFE2405:
	.size	_ZNK6icu_675UTS4614checkLabelBiDiEPKDsiRNS_8IDNAInfoE, .-_ZNK6icu_675UTS4614checkLabelBiDiEPKDsiRNS_8IDNAInfoE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4617isLabelOkContextJEPKDsi
	.type	_ZNK6icu_675UTS4617isLabelOkContextJEPKDsi, @function
_ZNK6icu_675UTS4617isLabelOkContextJEPKDsi:
.LFB2408:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L216
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	-1(%rdx), %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movzwl	(%r12,%rbx,2), %eax
	cmpw	$8204, %ax
	je	.L220
.L189:
	cmpw	$8205, %ax
	je	.L221
.L200:
	leaq	1(%rbx), %rax
	cmpq	%rbx, %r15
	je	.L188
	movq	%rax, %rbx
	movzwl	(%r12,%rbx,2), %eax
	cmpw	$8204, %ax
	jne	.L189
.L220:
	testq	%rbx, %rbx
	jne	.L190
.L196:
	xorl	%eax, %eax
.L187:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L196
	movzwl	-2(%r12,%rbx,2), %esi
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L201
	movzwl	-4(%r12,%rbx,2), %eax
	sall	$10, %eax
	leal	-56613888(%rsi,%rax), %esi
.L201:
	movq	8(%r13), %rdi
	movl	%edx, -52(%rbp)
	movq	(%rdi), %rax
	call	*80(%rax)
	movl	-52(%rbp), %edx
	cmpb	$9, %al
	je	.L200
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$1, %eax
	jmp	.L187
.L216:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
.L190:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movzwl	-2(%r12,%rbx,2), %r8d
	movl	%ebx, -52(%rbp)
	leal	-1(%rbx), %r14d
	movl	%r8d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L192
	movzwl	-4(%r12,%rbx,2), %eax
	leal	-2(%rbx), %r14d
	sall	$10, %eax
	leal	-56613888(%r8,%rax), %r8d
.L192:
	movq	8(%r13), %rdi
	movl	%edx, -60(%rbp)
	movl	%r8d, %esi
	movl	%r8d, -56(%rbp)
	movq	(%rdi), %rax
	call	*80(%rax)
	movl	-56(%rbp), %r8d
	movl	-60(%rbp), %edx
	cmpb	$9, %al
	je	.L200
.L193:
	movl	%r8d, %edi
	movl	%edx, -56(%rbp)
	call	ubidi_getJoiningType_67@PLT
	movl	-56(%rbp), %edx
	cmpl	$5, %eax
	jne	.L195
	testl	%r14d, %r14d
	je	.L196
	leal	-1(%r14), %esi
	movslq	%esi, %rax
	movzwl	(%r12,%rax,2), %r8d
	leaq	(%rax,%rax), %rdi
	movl	%r8d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L197
	movzwl	-2(%r12,%rdi), %eax
	leal	-2(%r14), %esi
	sall	$10, %eax
	leal	-56613888(%r8,%rax), %r8d
.L197:
	movl	%esi, %r14d
	jmp	.L193
.L195:
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L196
	movl	-52(%rbp), %r14d
	addl	$1, %r14d
.L199:
	cmpl	%edx, %r14d
	je	.L196
	movslq	%r14d, %rax
	leal	1(%r14), %ecx
	movzwl	(%r12,%rax,2), %edi
	leaq	(%rax,%rax), %rsi
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L222
	movl	%ecx, %r14d
.L198:
	movl	%edx, -52(%rbp)
	call	ubidi_getJoiningType_67@PLT
	movl	-52(%rbp), %edx
	cmpl	$5, %eax
	je	.L199
	subl	$2, %eax
	andl	$-3, %eax
	je	.L200
	jmp	.L196
.L222:
	movzwl	2(%r12,%rsi), %eax
	sall	$10, %edi
	addl	$2, %r14d
	leal	-56613888(%rax,%rdi), %edi
	jmp	.L198
	.cfi_endproc
.LFE2408:
	.size	_ZNK6icu_675UTS4617isLabelOkContextJEPKDsi, .-_ZNK6icu_675UTS4617isLabelOkContextJEPKDsi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4618checkLabelContextOEPKDsiRNS_8IDNAInfoE
	.type	_ZNK6icu_675UTS4618checkLabelContextOEPKDsiRNS_8IDNAInfoE, @function
_ZNK6icu_675UTS4618checkLabelContextOEPKDsiRNS_8IDNAInfoE:
.LFB2409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leal	-1(%rdx), %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r14d, %r14d
	js	.L223
	movq	%rsi, %r13
	movq	%rcx, %r12
	jle	.L262
	leaq	-60(%rbp), %rax
	movl	$0, -84(%rbp)
	movl	$1, %r15d
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L244:
	movzwl	-2(%r13,%r15,2), %eax
	movslq	%r15d, %rbx
	cmpl	$182, %eax
	jle	.L226
	cmpl	$1785, %eax
	jg	.L227
	cmpl	$183, %eax
	je	.L310
	cmpl	$885, %eax
	je	.L311
	leal	-1523(%rax), %esi
	cmpl	$1, %esi
	jbe	.L312
	cmpl	$1631, %eax
	jle	.L226
	cmpl	$1641, %eax
	jg	.L238
	cmpl	$1, -84(%rbp)
	jne	.L263
	orl	$16384, 4(%r12)
	movl	$-1, -84(%rbp)
	.p2align 4,,10
	.p2align 3
.L226:
	addq	$1, %r15
	leal	-1(%r15), %eax
	cmpl	%eax, %r14d
	jg	.L244
.L318:
	cmpl	%ebx, %r14d
	jl	.L223
.L225:
	leaq	-60(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L260:
	movzwl	0(%r13,%rbx,2), %eax
	cmpl	$182, %eax
	jle	.L246
	cmpl	$1785, %eax
	jg	.L247
	cmpl	$183, %eax
	je	.L313
	cmpl	$885, %eax
	je	.L306
	leal	-1523(%rax), %edx
	cmpl	$1, %edx
	jbe	.L314
	cmpl	$1631, %eax
	jle	.L246
	cmpl	$1641, %eax
	jg	.L255
	cmpl	$1, -84(%rbp)
	jne	.L266
	orl	$16384, 4(%r12)
	movl	$-1, -84(%rbp)
	.p2align 4,,10
	.p2align 3
.L246:
	addq	$1, %rbx
	cmpl	%ebx, %r14d
	jge	.L260
.L223:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L315
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	cmpl	$12539, %eax
	jne	.L226
	movl	$0, -60(%rbp)
	xorl	%eax, %eax
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L241:
	movq	-80(%rbp), %rsi
	movl	%r9d, -68(%rbp)
	call	uscript_getScript_67@PLT
	movl	%eax, %esi
	andl	$-3, %esi
	cmpl	$20, %esi
	je	.L226
	cmpl	$17, %eax
	movl	-68(%rbp), %r9d
	je	.L226
	cmpl	%r14d, %r9d
	jg	.L305
	movl	%r9d, %eax
.L239:
	movslq	%eax, %rsi
	leal	1(%rax), %r9d
	movzwl	0(%r13,%rsi,2), %edi
	leaq	(%rsi,%rsi), %r10
	movl	%edi, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L241
	cmpl	%r9d, -72(%rbp)
	je	.L241
	movzwl	2(%r13,%r10), %esi
	movl	%esi, %r10d
	andl	$-1024, %r10d
	cmpl	$56320, %r10d
	jne	.L241
	sall	$10, %edi
	leal	2(%rax), %r9d
	leal	-56613888(%rsi,%rdi), %edi
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L247:
	cmpl	$12539, %eax
	jne	.L246
	movl	$0, -60(%rbp)
	xorl	%eax, %eax
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%r15, %rsi
	movl	%edx, -68(%rbp)
	call	uscript_getScript_67@PLT
	movl	%eax, %esi
	andl	$-3, %esi
	cmpl	$20, %esi
	je	.L246
	cmpl	$17, %eax
	je	.L246
	movl	-68(%rbp), %edx
	cmpl	%edx, %r14d
	jl	.L306
	movl	%edx, %eax
.L258:
	movslq	%eax, %rsi
	leal	1(%rax), %edx
	movzwl	0(%r13,%rsi,2), %edi
	leaq	(%rsi,%rsi), %r9
	movl	%edi, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L256
	cmpl	%edx, -72(%rbp)
	je	.L256
	movzwl	2(%r13,%r9), %esi
	movl	%esi, %r9d
	andl	$-1024, %r9d
	cmpl	$56320, %r9d
	jne	.L256
	sall	$10, %edi
	leal	2(%rax), %edx
	leal	-56613888(%rsi,%rdi), %edi
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L313:
	testl	%ebx, %ebx
	je	.L306
	cmpw	$108, -2(%r13,%rbx,2)
	jne	.L306
	cmpl	%ebx, %r14d
	jg	.L316
	.p2align 4,,10
	.p2align 3
.L306:
	orl	$8192, 4(%r12)
.L320:
	addq	$1, %rbx
	cmpl	%ebx, %r14d
	jge	.L260
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$0, -84(%rbp)
	xorl	%ebx, %ebx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L310:
	movl	%ebx, %eax
	subl	$1, %eax
	je	.L305
	cmpw	$108, -4(%r13,%r15,2)
	jne	.L305
	cmpl	%eax, %r14d
	jg	.L317
.L305:
	orl	$8192, 4(%r12)
.L322:
	addq	$1, %r15
	leal	-1(%r15), %eax
	cmpl	%eax, %r14d
	jg	.L244
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L314:
	testl	%ebx, %ebx
	je	.L306
	leal	-1(%rbx), %edx
	movl	$0, -60(%rbp)
	movslq	%edx, %rax
	movzwl	0(%r13,%rax,2), %edi
	leaq	(%rax,%rax), %rsi
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L253
	testl	%edx, %edx
	je	.L253
	movzwl	-2(%r13,%rsi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L319
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%r15, %rsi
	call	uscript_getScript_67@PLT
	cmpl	$19, %eax
	je	.L246
	orl	$8192, 4(%r12)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L312:
	cmpl	$1, %ebx
	je	.L305
	movzwl	-4(%r13,%r15,2), %edi
	movl	$0, -60(%rbp)
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L236
	cmpl	$2, %ebx
	je	.L236
	movzwl	-6(%r13,%r15,2), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	je	.L321
	.p2align 4,,10
	.p2align 3
.L236:
	movq	-80(%rbp), %rsi
	call	uscript_getScript_67@PLT
	cmpl	$19, %eax
	je	.L226
	orl	$8192, 4(%r12)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L238:
	cmpl	$1775, %eax
	jle	.L226
	cmpl	$-1, -84(%rbp)
	je	.L323
	movl	$1, -84(%rbp)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L255:
	cmpl	$1775, %eax
	jle	.L246
	cmpl	$-1, -84(%rbp)
	je	.L324
	movl	$1, -84(%rbp)
	jmp	.L246
.L317:
	cmpw	$108, 0(%r13,%r15,2)
	jne	.L305
	jmp	.L226
.L316:
	cmpw	$108, 2(%r13,%rbx,2)
	jne	.L306
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L311:
	movzwl	0(%r13,%r15,2), %edi
	movl	$0, -60(%rbp)
	leal	1(%rbx), %eax
	movl	%edi, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L232
	cmpl	%eax, -72(%rbp)
	jne	.L325
.L232:
	movq	-80(%rbp), %rsi
	call	uscript_getScript_67@PLT
	cmpl	$14, %eax
	je	.L226
	orl	$8192, 4(%r12)
	jmp	.L322
.L263:
	movl	$-1, -84(%rbp)
	jmp	.L226
.L266:
	movl	$-1, -84(%rbp)
	jmp	.L246
.L324:
	orl	$16384, 4(%r12)
	movl	$1, -84(%rbp)
	jmp	.L246
.L323:
	orl	$16384, 4(%r12)
	movl	$1, -84(%rbp)
	jmp	.L226
.L319:
	sall	$10, %eax
	leal	-56613888(%rdi,%rax), %edi
	jmp	.L253
.L321:
	sall	$10, %eax
	leal	-56613888(%rdi,%rax), %edi
	jmp	.L236
.L325:
	cltq
	movzwl	0(%r13,%rax,2), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L232
	sall	$10, %edi
	leal	-56613888(%rax,%rdi), %edi
	jmp	.L232
.L315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2409:
	.size	_ZNK6icu_675UTS4618checkLabelContextOEPKDsiRNS_8IDNAInfoE, .-_ZNK6icu_675UTS4618checkLabelContextOEPKDsiRNS_8IDNAInfoE
	.align 2
	.p2align 4
	.type	_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0, @function
_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0:
.LFB3088:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	movl	$2, %r9d
	subq	$248, %rsp
	movq	16(%rbp), %rax
	movq	%rdi, -224(%rbp)
	movl	%r8d, -252(%rbp)
	movq	%rax, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r9w, -184(%rbp)
	movq	%rax, -192(%rbp)
	movzwl	8(%rsi), %eax
	testb	$17, %al
	jne	.L389
	leaq	10(%rsi), %r12
	testb	$2, %al
	je	.L442
.L327:
	movslq	%r11d, %rax
	addq	%rax, %rax
	movq	%rax, -248(%rbp)
	addq	%rax, %r12
	cmpl	$3, %r14d
	jle	.L329
	movzwl	(%r12), %eax
	movzwl	4(%r12), %edx
	cmpw	$120, %ax
	je	.L443
.L391:
	leaq	-192(%rbp), %rdi
	movq	%r13, -240(%rbp)
	movl	%r11d, %esi
	movl	%r14d, %r15d
	movb	$0, -264(%rbp)
	movq	%rdi, -216(%rbp)
.L330:
	cmpw	$45, %dx
	je	.L332
.L348:
	cmpw	$45, %ax
	jne	.L331
	orl	$8, 4(%rbx)
.L331:
	movslq	%r15d, %rax
	cmpw	$45, -2(%r12,%rax,2)
	leaq	(%rax,%rax), %rcx
	jne	.L349
	orl	$16, 4(%rbx)
.L349:
	movq	-224(%rbp), %rax
	addq	%r12, %rcx
	testb	$2, 16(%rax)
	jne	.L401
	movq	%r12, %rax
	xorl	%r10d, %r10d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L445:
	orl	%edx, %r10d
	cmpw	$-3, %dx
	je	.L444
.L352:
	addq	$2, %rax
	cmpq	%rax, %rcx
	jbe	.L353
.L354:
	movzwl	(%rax), %edx
	cmpw	$127, %dx
	ja	.L445
	cmpw	$46, %dx
	jne	.L352
	movl	$-3, %edx
	orl	$512, 4(%rbx)
	addq	$2, %rax
	movw	%dx, -2(%rax)
	cmpq	%rax, %rcx
	ja	.L354
	.p2align 4,,10
	.p2align 3
.L353:
	movzwl	(%r12), %edi
	movl	$1, %edx
	movl	$0, -280(%rbp)
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L360
	movzwl	2(%r12), %eax
	sall	$10, %edi
	movl	$2, %edx
	movl	$-1, -280(%rbp)
	leal	-56613888(%rdi,%rax), %edi
.L360:
	movl	%r11d, -276(%rbp)
	movl	%esi, -272(%rbp)
	movl	%r10d, -268(%rbp)
	movl	%edx, -256(%rbp)
	call	u_charType_67@PLT
	movl	$448, %ecx
	movl	-256(%rbp), %edx
	movl	-268(%rbp), %r10d
	btl	%eax, %ecx
	movl	-272(%rbp), %esi
	movl	-276(%rbp), %r11d
	jc	.L446
.L361:
	movl	4(%rbx), %eax
	testl	$1984, %eax
	jne	.L364
	movq	-224(%rbp), %rax
	movl	16(%rax), %eax
	testb	$4, %al
	je	.L365
	cmpb	$0, 9(%rbx)
	je	.L366
	cmpb	$0, 10(%rbx)
	je	.L365
.L366:
	movq	-224(%rbp), %rdi
	movq	%rbx, %rcx
	movl	%r15d, %edx
	movq	%r12, %rsi
	movl	%r11d, -256(%rbp)
	movl	%r10d, -248(%rbp)
	call	_ZNK6icu_675UTS4614checkLabelBiDiEPKDsiRNS_8IDNAInfoE
	movq	-224(%rbp), %rax
	movl	-256(%rbp), %r11d
	movl	-248(%rbp), %r10d
	movl	16(%rax), %eax
.L365:
	testb	$8, %al
	je	.L367
	movl	%r10d, %edx
	andw	$8204, %dx
	cmpw	$8204, %dx
	je	.L447
.L367:
	testb	$64, %al
	je	.L369
	cmpw	$182, %r10w
	ja	.L448
.L369:
	cmpb	$0, -252(%rbp)
	je	.L438
	cmpb	$0, -264(%rbp)
	jne	.L449
	cmpw	$127, %r10w
	ja	.L450
	cmpl	$63, %r15d
	jle	.L438
	orl	$2, 4(%rbx)
	.p2align 4,,10
	.p2align 3
.L438:
	movq	-232(%rbp), %rax
	movl	(%rax), %edx
.L383:
	testl	%edx, %edx
	jg	.L439
	movq	-240(%rbp), %rax
	cmpq	%rax, %r13
	je	.L346
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L385
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L386:
	movq	-240(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movl	%r11d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	testb	$1, 8(%r13)
	je	.L346
.L347:
	movq	-232(%rbp), %rax
	movl	$7, (%rax)
.L439:
	xorl	%r15d, %r15d
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L329:
	testl	%r14d, %r14d
	jne	.L451
	leaq	-192(%rbp), %rax
	orl	$1, 4(%rbx)
	xorl	%r15d, %r15d
	movq	%rax, -216(%rbp)
.L346:
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L452
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movq	24(%rsi), %r12
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L444:
	orl	$128, 4(%rbx)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L364:
	movq	-232(%rbp), %rsi
	cmpb	$0, -264(%rbp)
	movl	(%rsi), %edx
	je	.L383
	orb	$4, %ah
	movl	%eax, 4(%rbx)
	testl	%edx, %edx
	jg	.L439
	subq	$8, %rsp
	pushq	%rsi
.L441:
	movsbl	-252(%rbp), %r8d
	movq	%rbx, %r9
	movl	%r14d, %ecx
.L440:
	movq	-224(%rbp), %rdi
	movl	%r11d, %edx
	movq	%r13, %rsi
	call	_ZNK6icu_675UTS4615markBadACELabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0
	movl	%eax, %r15d
	popq	%rax
	popq	%rdx
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%r12, %rdx
	xorl	%r10d, %r10d
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r8
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L454:
	cmpw	$46, %ax
	je	.L453
	cmpb	$0, (%r8,%rax)
	js	.L358
	.p2align 4,,10
	.p2align 3
.L357:
	addq	$2, %rdx
	cmpq	%rdx, %rcx
	jbe	.L353
.L350:
	movzwl	(%rdx), %eax
	cmpw	$127, %ax
	jbe	.L454
	leal	-8814(%rax), %edi
	orl	%eax, %r10d
	cmpw	$1, %di
	jbe	.L358
	cmpw	$8800, %ax
	jne	.L455
.L358:
	movl	$-3, %r9d
	orl	$128, 4(%rbx)
	movw	%r9w, (%rdx)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L455:
	cmpw	$-3, %ax
	jne	.L357
	orl	$128, 4(%rbx)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L453:
	movl	$-3, %eax
	orl	$512, 4(%rbx)
	movw	%ax, (%rdx)
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L443:
	cmpw	$110, 2(%r12)
	jne	.L391
	cmpw	$45, %dx
	je	.L456
	leaq	-192(%rbp), %rax
	movq	%r13, -240(%rbp)
	movl	%r11d, %esi
	movl	%r14d, %r15d
	movb	$0, -264(%rbp)
	movq	%rax, -216(%rbp)
	jmp	.L331
.L456:
	cmpw	$45, 6(%r12)
	je	.L457
	leaq	-192(%rbp), %rdi
	movq	%r13, -240(%rbp)
	movl	%r11d, %esi
	movl	%r14d, %r15d
	movb	$0, -264(%rbp)
	movq	%rdi, -216(%rbp)
	.p2align 4,,10
	.p2align 3
.L332:
	cmpw	$45, 6(%r12)
	jne	.L348
	orl	$32, 4(%rbx)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L389:
	xorl	%r12d, %r12d
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L446:
	orl	$64, 4(%rbx)
	movq	-240(%rbp), %r12
	movl	$-3, %edi
	xorl	%r8d, %r8d
	movw	%di, -196(%rbp)
	leaq	-196(%rbp), %rcx
	movl	$1, %r9d
	movq	%r12, %rdi
	movl	%r11d, -268(%rbp)
	movl	%r10d, -256(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	8(%r12), %eax
	movl	-256(%rbp), %r10d
	movl	-268(%rbp), %r11d
	testb	$17, %al
	jne	.L403
	leaq	10(%r12), %r12
	testb	$2, %al
	jne	.L362
	movq	-240(%rbp), %rax
	movq	24(%rax), %r12
.L362:
	addl	-280(%rbp), %r15d
	addq	-248(%rbp), %r12
	cmpq	-240(%rbp), %r13
	cmove	%r15d, %r14d
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L385:
	movq	-240(%rbp), %rax
	movl	12(%rax), %r9d
	jmp	.L386
.L448:
	movq	-224(%rbp), %rdi
	movq	%rbx, %rcx
	movl	%r15d, %edx
	movq	%r12, %rsi
	movl	%r11d, -256(%rbp)
	movl	%r10d, -248(%rbp)
	call	_ZNK6icu_675UTS4618checkLabelContextOEPKDsiRNS_8IDNAInfoE
	movl	-256(%rbp), %r11d
	movl	-248(%rbp), %r10d
	jmp	.L369
.L447:
	movq	-224(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r12, %rsi
	movl	%r11d, -256(%rbp)
	movl	%r10d, -248(%rbp)
	call	_ZNK6icu_675UTS4617isLabelOkContextJEPKDsi
	movl	-248(%rbp), %r10d
	movl	-256(%rbp), %r11d
	testb	%al, %al
	jne	.L436
	orl	$4096, 4(%rbx)
.L436:
	movq	-224(%rbp), %rax
	movl	16(%rax), %eax
	jmp	.L367
.L451:
	leaq	-192(%rbp), %rdi
	movzwl	(%r12), %eax
	movl	%r11d, %esi
	movl	%r14d, %r15d
	movq	%r13, -240(%rbp)
	movb	$0, -264(%rbp)
	movq	%rdi, -216(%rbp)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L449:
	movl	%r14d, %r15d
	cmpl	$63, %r14d
	jle	.L346
	orl	$2, 4(%rbx)
	jmp	.L346
.L403:
	xorl	%r12d, %r12d
	jmp	.L362
.L450:
	leaq	-128(%rbp), %r10
	movl	$2, %esi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	%r11d, -240(%rbp)
	movw	%si, -120(%rbp)
	movq	%r10, %rdi
	movl	$63, %esi
	movq	%r10, -224(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	-224(%rbp), %r10
	movl	-240(%rbp), %r11d
	testq	%rax, %rax
	je	.L437
	movabsq	$12666567232716920, %rsi
	movl	$23, %ecx
	movq	%rsi, (%rax)
	testb	$2, -120(%rbp)
	jne	.L375
	movl	-112(%rbp), %esi
	leal	-4(%rsi), %ecx
.L375:
	movq	-232(%rbp), %r9
	xorl	%r8d, %r8d
	movl	%r15d, %esi
	movq	%r12, %rdi
	leaq	8(%rax), %rdx
	movq	%r10, -240(%rbp)
	movl	%r11d, -224(%rbp)
	call	u_strToPunycode_67@PLT
	movq	-232(%rbp), %rsi
	movl	-224(%rbp), %r11d
	movq	-240(%rbp), %r10
	cmpl	$15, (%rsi)
	je	.L458
.L376:
	leal	4(%rax), %r15d
	movq	%r10, %rdi
	movq	%r10, -224(%rbp)
	movl	%r15d, %esi
	movl	%r11d, -240(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	-232(%rbp), %rax
	movq	-224(%rbp), %r10
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L407
	cmpl	$63, %r15d
	movl	-240(%rbp), %r11d
	jle	.L379
	orl	$2, 4(%rbx)
.L379:
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L380
	sarl	$5, %eax
	movl	%eax, %r9d
.L381:
	xorl	%r8d, %r8d
	movq	%r10, %rcx
	movl	%r14d, %edx
	movl	%r11d, %esi
	movq	%r13, %rdi
	movq	%r10, -224(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	testb	$1, 8(%r13)
	movq	-224(%rbp), %r10
	je	.L374
	movq	-232(%rbp), %rax
	xorl	%r15d, %r15d
	movl	$7, (%rax)
.L374:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L346
.L407:
	movl	%r14d, %r15d
	jmp	.L374
.L458:
	movl	$0, (%rsi)
	movq	%r10, %rdi
	movl	$4, %esi
	movl	%r11d, -248(%rbp)
	movl	%eax, -240(%rbp)
	movq	%r10, -224(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	-224(%rbp), %r10
	movl	-240(%rbp), %eax
	movq	%r10, %rdi
	leal	4(%rax), %esi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	-224(%rbp), %r10
	movl	-248(%rbp), %r11d
	testq	%rax, %rax
	je	.L437
	movl	$23, %ecx
	testb	$2, -120(%rbp)
	jne	.L378
	movl	-112(%rbp), %esi
	leal	-4(%rsi), %ecx
.L378:
	movq	-232(%rbp), %r9
	xorl	%r8d, %r8d
	movl	%r15d, %esi
	movq	%r12, %rdi
	leaq	8(%rax), %rdx
	movq	%r10, -240(%rbp)
	movl	%r11d, -224(%rbp)
	call	u_strToPunycode_67@PLT
	movq	-240(%rbp), %r10
	movl	-224(%rbp), %r11d
	jmp	.L376
.L437:
	movq	-232(%rbp), %rax
	movl	%r14d, %r15d
	movl	$7, (%rax)
	jmp	.L374
.L380:
	movl	-116(%rbp), %r9d
	jmp	.L381
.L457:
	leaq	-192(%rbp), %rax
	movl	$-1, %esi
	movl	%r11d, -240(%rbp)
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movl	-240(%rbp), %r11d
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L337
	testb	$2, -184(%rbp)
	movl	$27, %ecx
	leal	-4(%r14), %r15d
	cmove	-176(%rbp), %ecx
	addq	$8, %r12
	xorl	%r8d, %r8d
	movl	%r15d, %esi
	movl	%r11d, -248(%rbp)
	leaq	-196(%rbp), %r9
	movq	%r12, %rdi
	movl	$0, -196(%rbp)
	movq	%r9, -240(%rbp)
	call	u_strFromPunycode_67@PLT
	cmpl	$15, -196(%rbp)
	movq	-240(%rbp), %r9
	movl	-248(%rbp), %r11d
	movl	%eax, %r8d
	je	.L459
.L336:
	movq	-216(%rbp), %rdi
	movl	%r8d, %esi
	movl	%r11d, -240(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movl	-196(%rbp), %r8d
	movl	-240(%rbp), %r11d
	testl	%r8d, %r8d
	jle	.L339
	movq	-232(%rbp), %rax
	orl	$256, 4(%rbx)
	xorl	%r15d, %r15d
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L346
	subq	$8, %rsp
	pushq	%rax
	jmp	.L441
.L339:
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %r12
	movl	%r14d, %r15d
	movl	%r11d, -240(%rbp)
	movq	-216(%rbp), %rsi
	movq	8(%rax), %rdi
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*88(%rax)
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L346
	testb	%al, %al
	movl	-240(%rbp), %r11d
	je	.L460
	movswl	-184(%rbp), %eax
	testb	$17, %al
	jne	.L398
	leaq	-182(%rbp), %r12
	testb	$2, %al
	jne	.L341
	movq	-168(%rbp), %r12
.L341:
	testw	%ax, %ax
	js	.L342
	sarl	$5, %eax
	movl	%eax, %r15d
.L343:
	testl	%r15d, %r15d
	jne	.L433
	orl	$1, 4(%rbx)
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movq	-216(%rbp), %rcx
	movl	%r11d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	testb	$1, 8(%r13)
	jne	.L347
	xorl	%r15d, %r15d
	jmp	.L346
.L433:
	movzwl	(%r12), %eax
	cmpl	$3, %r15d
	jg	.L461
	movq	-216(%rbp), %rdi
	movb	$1, -264(%rbp)
	xorl	%esi, %esi
	movq	$0, -248(%rbp)
	movq	%rdi, -240(%rbp)
	jmp	.L348
.L459:
	movq	-216(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r9, -264(%rbp)
	movl	%eax, -240(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movl	-240(%rbp), %r8d
	movq	-216(%rbp), %rdi
	movl	%r8d, %esi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L337
	testb	$2, -184(%rbp)
	movl	-248(%rbp), %r11d
	movl	$0, -196(%rbp)
	movq	-264(%rbp), %r9
	jne	.L395
	movl	-176(%rbp), %ecx
.L338:
	xorl	%r8d, %r8d
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	%r11d, -240(%rbp)
	call	u_strFromPunycode_67@PLT
	movl	-240(%rbp), %r11d
	movl	%eax, %r8d
	jmp	.L336
.L337:
	movq	-232(%rbp), %rax
	movl	%r14d, %r15d
	movl	$7, (%rax)
	jmp	.L346
.L460:
	movsbl	-252(%rbp), %r8d
	pushq	%rcx
	movq	%rbx, %r9
	movl	%r14d, %ecx
	orl	$1024, 4(%rbx)
	pushq	-232(%rbp)
	jmp	.L440
.L452:
	call	__stack_chk_fail@PLT
.L395:
	movl	$27, %ecx
	jmp	.L338
.L461:
	movq	-216(%rbp), %rdi
	movb	$1, -264(%rbp)
	xorl	%esi, %esi
	movq	$0, -248(%rbp)
	movzwl	4(%r12), %edx
	movq	%rdi, -240(%rbp)
	jmp	.L330
.L342:
	movl	-180(%rbp), %r15d
	jmp	.L343
.L398:
	xorl	%r12d, %r12d
	jmp	.L341
	.cfi_endproc
.LFE3088:
	.size	_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0, .-_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode:
.LFB2403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L463
	movsbl	%r8b, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2403:
	.size	_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode:
.LFB2399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	16(%rbp), %r13
	movq	%rdi, -136(%rbp)
	movq	24(%rbp), %r14
	movq	32(%rbp), %r11
	movl	%r9d, -140(%rbp)
	movb	%r9b, -141(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	testl	%ecx, %ecx
	jne	.L466
	movq	%r11, -152(%rbp)
	movq	%r11, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	-152(%rbp), %r11
.L467:
	movl	(%r11), %ebx
	testl	%ebx, %ebx
	jg	.L469
	movq	-136(%rbp), %rax
	cmpb	$0, -140(%rbp)
	movl	16(%rax), %eax
	jne	.L521
	shrl	$5, %eax
	xorl	$1, %eax
	andl	$1, %eax
	movb	%al, -168(%rbp)
.L471:
	movswl	8(%r13), %ebx
	testb	$17, %bl
	jne	.L499
	leaq	10(%r13), %rdx
	testb	$2, %bl
	jne	.L472
	movq	24(%r13), %rdx
.L472:
	testw	%bx, %bx
	js	.L474
	sarl	$5, %ebx
.L477:
	cmpl	%r12d, %ebx
	jle	.L475
	testb	%r15b, %r15b
	movq	%r14, %r9
	movl	%r12d, %r15d
	movq	%r11, %r14
	sete	%r10b
.L476:
	movslq	%r12d, %rax
	leaq	(%rax,%rax), %rsi
	movzwl	(%rdx,%rax,2), %eax
	cmpw	$46, %ax
	jne	.L479
	testb	%r10b, %r10b
	je	.L479
	movl	(%r14), %r11d
	movb	%r10b, -152(%rbp)
	subl	%r15d, %r12d
	testl	%r11d, %r11d
	jg	.L480
	subq	$8, %rsp
	movl	%r12d, %ecx
	movl	%r15d, %edx
	movq	%r13, %rsi
	pushq	%r14
	movsbl	-141(%rbp), %r8d
	movq	-136(%rbp), %rdi
	movq	%r9, -160(%rbp)
	call	_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0
	movq	-160(%rbp), %r9
	movl	(%r14), %edx
	movl	4(%r9), %ecx
	movl	$0, 4(%r9)
	orl	%ecx, (%r9)
	popq	%r8
	popq	%r10
	testl	%edx, %edx
	jg	.L469
	movzwl	8(%r13), %edx
	movzbl	-152(%rbp), %r10d
	testb	$17, %dl
	jne	.L500
	andl	$2, %edx
	leaq	10(%r13), %rdx
	je	.L522
.L482:
	movl	%eax, %ecx
	subl	%r12d, %ecx
	leal	1(%r15,%rax), %r12d
	addl	%ecx, %ebx
	movl	%r12d, %r15d
.L484:
	cmpl	%ebx, %r12d
	jl	.L476
	movq	%r14, %r11
	movq	%r9, %r14
	testl	%r15d, %r15d
	je	.L505
	cmpl	%r15d, %r12d
	jle	.L469
.L505:
	movl	(%r11), %ecx
	testl	%ecx, %ecx
	jle	.L523
.L495:
	movl	4(%r14), %eax
	orl	%eax, (%r14)
	.p2align 4,,10
	.p2align 3
.L469:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L524
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	shrl	$4, %eax
	xorl	$1, %eax
	andl	$1, %eax
	movb	%al, -168(%rbp)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L479:
	cmpw	$222, %ax
	jbe	.L520
	cmpw	$8205, %ax
	ja	.L487
	cmpw	$223, %ax
	sete	%sil
	cmpw	$962, %ax
	sete	%cl
	orb	%cl, %sil
	jne	.L488
	cmpw	$8203, %ax
	jbe	.L520
.L488:
	cmpb	$0, -168(%rbp)
	movb	$1, 8(%r9)
	je	.L520
	movl	(%r14), %edi
	movq	%r9, -160(%rbp)
	movb	%r10b, -152(%rbp)
	testl	%edi, %edi
	jg	.L469
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r14, %r8
	movl	%r12d, %ecx
	movl	%r15d, %edx
	call	_ZNK6icu_675UTS4611mapDevCharsERNS_13UnicodeStringEiiR10UErrorCode.part.0
	movl	(%r14), %esi
	movl	%eax, %ebx
	testl	%esi, %esi
	jg	.L469
	movzwl	8(%r13), %eax
	movzbl	-152(%rbp), %r10d
	movq	-160(%rbp), %r9
	testb	$17, %al
	jne	.L501
	testb	$2, %al
	je	.L490
	movb	$0, -168(%rbp)
	leaq	10(%r13), %rdx
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L520:
	addl	$1, %r12d
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L480:
	movl	4(%r9), %eax
	movl	$0, 4(%r9)
	orl	%eax, (%r9)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L466:
	leaq	-128(%rbp), %r8
	movq	40(%rax), %rax
	movl	%ecx, %edx
	movl	$2147483647, %ecx
	movq	%r8, %rdi
	movq	%r11, -160(%rbp)
	movq	%rax, -168(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-160(%rbp), %r11
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	-152(%rbp), %r8
	movq	-168(%rbp), %rax
	movq	%r11, %rcx
	movq	%r8, %rdx
	call	*%rax
	movq	-152(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-160(%rbp), %r11
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L474:
	movl	12(%r13), %ebx
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L487:
	movl	%eax, %edi
	leal	1(%r12), %ecx
	andl	$63488, %edi
	cmpl	$55296, %edi
	je	.L525
.L503:
	movl	%ecx, %r12d
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L522:
	movq	24(%r13), %rdx
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L499:
	xorl	%edx, %edx
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L525:
	testb	$4, %ah
	je	.L526
	cmpl	%r12d, %r15d
	je	.L492
	movzwl	-2(%rdx,%rsi), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L503
.L492:
	orl	$128, 4(%r9)
	movl	$65533, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movb	%r10b, -142(%rbp)
	movl	%ecx, -160(%rbp)
	movq	%r9, -152(%rbp)
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
	movzwl	8(%r13), %eax
	movq	-152(%rbp), %r9
	movl	-160(%rbp), %ecx
	movzbl	-142(%rbp), %r10d
	testb	$17, %al
	jne	.L504
	testb	$2, %al
	je	.L493
	leaq	10(%r13), %rdx
	movl	%ecx, %r12d
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L500:
	xorl	%edx, %edx
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L526:
	cmpl	%ecx, %ebx
	je	.L492
	movzwl	2(%rdx,%rsi), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L492
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L493:
	movq	24(%r13), %rdx
	movl	%ecx, %r12d
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L475:
	testl	%r12d, %r12d
	jne	.L469
	movsbl	-140(%rbp), %r8d
	xorl	%r15d, %r15d
.L496:
	subq	$8, %rsp
	movl	%r15d, %edx
	movq	%r14, %r9
	movl	%r12d, %ecx
	pushq	%r11
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_675UTS4612processLabelERNS_13UnicodeStringEiiaRNS_8IDNAInfoER10UErrorCode.part.0
	popq	%rax
	popq	%rdx
	jmp	.L495
.L490:
	movb	$0, -168(%rbp)
	movq	24(%r13), %rdx
	jmp	.L484
.L504:
	movl	%ecx, %r12d
	xorl	%edx, %edx
	jmp	.L484
.L523:
	movsbl	-140(%rbp), %r8d
	subl	%r15d, %r12d
	jmp	.L496
.L501:
	movb	$0, -168(%rbp)
	xorl	%edx, %edx
	jmp	.L484
.L524:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2399:
	.size	_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode:
.LFB2397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movl	%ecx, -72(%rbp)
	movb	%cl, -66(%rbp)
	movl	(%rax), %ecx
	movq	%rdi, -64(%rbp)
	movl	%edx, -76(%rbp)
	movb	%dl, -65(%rbp)
	testl	%ecx, %ecx
	jg	.L593
	movzwl	8(%rsi), %eax
	movq	%rsi, %r13
	testb	$17, %al
	jne	.L530
	movq	%r9, %rbx
	leaq	10(%rsi), %r8
	testb	$2, %al
	je	.L594
.L532:
	cmpq	%r12, %r13
	je	.L530
	testq	%r8, %r8
	je	.L530
	movzwl	8(%r12), %edx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	xorl	%edx, %edx
	movw	%ax, 8(%r12)
	movq	$0, (%rbx)
	movw	%dx, 8(%rbx)
	movb	$1, 10(%rbx)
	movswl	8(%r13), %r15d
	testw	%r15w, %r15w
	js	.L536
	sarl	$5, %r15d
	testl	%r15d, %r15d
	jne	.L538
.L595:
	movl	$1, (%rbx)
.L529:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	movq	24(%rsi), %r8
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L536:
	movl	12(%r13), %r15d
	testl	%r15d, %r15d
	je	.L595
.L538:
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	je	.L596
	movq	-64(%rbp), %rsi
	movq	%r13, -88(%rbp)
	xorl	%r14d, %r14d
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r9
	movl	16(%rsi), %esi
	movl	%esi, %edx
	movl	4(%rbx), %esi
	shrl	%edx
	movl	%edx, %edi
	movl	(%rbx), %edx
	andl	$1, %edi
	movl	%edx, -56(%rbp)
	movl	%edi, %r13d
	xorl	%edx, %edx
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L598:
	addl	$32, %ecx
	addl	$1, %edi
	movw	%cx, (%rax,%rdx,2)
.L545:
	cmpl	%edi, %r15d
	je	.L597
.L551:
	addq	$1, %rdx
.L540:
	movzwl	(%r8,%rdx,2), %ecx
	movl	%edx, %edi
	movl	%edx, %r11d
	cmpw	$127, %cx
	ja	.L586
	movzwl	%cx, %r10d
	movsbl	(%r9,%r10), %r10d
	testl	%r10d, %r10d
	jg	.L598
	jns	.L569
	testb	%r13b, %r13b
	jne	.L586
.L569:
	movw	%cx, (%rax,%rdx,2)
	addl	$1, %edi
	cmpw	$45, %cx
	je	.L599
	cmpw	$46, %cx
	jne	.L545
	cmpb	$0, -65(%rbp)
	jne	.L567
	movl	%esi, %ecx
	orl	$1, %ecx
	cmpl	%edx, %r14d
	cmove	%ecx, %esi
	cmpb	$0, -66(%rbp)
	je	.L553
	movl	%esi, %ecx
	subl	%r14d, %r11d
	orl	$2, %ecx
	cmpl	$63, %r11d
	cmovg	%ecx, %esi
.L553:
	orl	%esi, -56(%rbp)
	movl	-56(%rbp), %ecx
	movl	%edi, %r14d
	xorl	%esi, %esi
	movl	$0, 4(%rbx)
	movl	%ecx, (%rbx)
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L530:
	movq	16(%rbp), %rax
	movl	$1, (%rax)
.L593:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L599:
	leal	3(%r14), %ecx
	movl	%edx, %r10d
	cmpl	%edx, %ecx
	je	.L600
.L548:
	cmpl	%r14d, %r10d
	jne	.L549
	orl	$8, %esi
	movl	%esi, 4(%rbx)
.L549:
	cmpl	%edi, %r15d
	je	.L550
	cmpw	$46, 2(%r8,%rdx,2)
	jne	.L551
.L550:
	orl	$16, %esi
	movl	%esi, 4(%rbx)
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L600:
	cmpw	$45, -2(%r8,%rdx,2)
	jne	.L548
.L567:
	movq	-88(%rbp), %r13
	movl	%edi, %r11d
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L586:
	movq	-88(%rbp), %r13
.L543:
	orl	-56(%rbp), %esi
	movq	%r12, %rdi
	movl	%r11d, -56(%rbp)
	movl	%esi, (%rbx)
	movl	%r11d, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	subq	$8, %rsp
	movl	-56(%rbp), %r11d
	pushq	16(%rbp)
	movsbl	-72(%rbp), %r9d
	pushq	%rbx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movsbl	-76(%rbp), %r8d
	movq	-64(%rbp), %rdi
	pushq	%r12
	movl	%r11d, %ecx
	call	_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode
	addq	$32, %rsp
	cmpb	$0, 9(%rbx)
	je	.L529
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L529
	movl	(%rbx), %edi
	testl	$1984, %edi
	jne	.L529
	cmpb	$0, 10(%rbx)
	je	.L555
	testl	%r14d, %r14d
	je	.L529
	movzwl	8(%r12), %eax
	testb	$17, %al
	jne	.L568
	leaq	10(%r12), %rsi
	testb	$2, %al
	jne	.L556
	movq	24(%r12), %rsi
.L556:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L558:
	cmpl	%eax, %ecx
	je	.L601
	cmpw	$32, %dx
	ja	.L592
	leal	-9(%rdx), %r8d
	cmpw	$4, %r8w
	jbe	.L555
	cmpw	$27, %dx
	ja	.L555
.L592:
	leal	1(%rax), %edx
.L561:
	addq	$1, %rax
	cmpl	%r14d, %edx
	jge	.L529
.L564:
	movzwl	(%rsi,%rax,2), %edx
	cmpw	$46, %dx
	jne	.L558
	cmpl	%eax, %ecx
	jge	.L559
	movzwl	-2(%rsi,%rax,2), %edx
	leal	-97(%rdx), %ecx
	cmpw	$25, %cx
	jbe	.L559
	subl	$48, %edx
	cmpw	$9, %dx
	ja	.L555
.L559:
	leal	1(%rax), %ecx
	movl	%ecx, %edx
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L597:
	cmpb	$0, -72(%rbp)
	je	.L541
	movl	%r15d, %eax
	subl	%r14d, %eax
	cmpl	$63, %eax
	jle	.L542
	orl	$2, %esi
	movl	%esi, 4(%rbx)
.L542:
	cmpb	$0, -76(%rbp)
	jne	.L541
	cmpl	$253, %r15d
	jle	.L541
	movl	-56(%rbp), %edi
	cmpl	$254, %r14d
	movl	$254, %eax
	cmovg	%eax, %r14d
	movl	%edi, %eax
	orl	$4, %eax
	cmpl	%r14d, %r15d
	cmovle	%edi, %eax
	movl	%eax, -56(%rbp)
.L541:
	orl	-56(%rbp), %esi
	movq	%r12, %rdi
	movl	%esi, (%rbx)
	movl	%r15d, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L596:
	movq	16(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L529
.L601:
	subl	$97, %edx
	cmpw	$25, %dx
	jbe	.L592
.L555:
	orl	$2048, %edi
	movl	%edi, (%rbx)
	jmp	.L529
.L568:
	xorl	%esi, %esi
	jmp	.L556
	.cfi_endproc
.LFE2397:
	.size	_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_674IDNA17labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_674IDNA17labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_674IDNA17labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode:
.LFB2374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L608
.L602:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L609
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, %r12
	movq	%rcx, %r14
	movq	%r8, %r13
	movq	%rax, -192(%rbp)
	movq	(%rdi), %rax
	leaq	-128(%rbp), %r15
	movl	$2, %ecx
	movq	%r15, %rdi
	movw	%cx, -184(%rbp)
	movq	%r9, %rbx
	movq	24(%rax), %rax
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	-200(%rbp), %rax
	leaq	_ZNK6icu_675UTS4612labelToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode(%rip), %rdx
	leaq	-192(%rbp), %r10
	cmpq	%rdx, %rax
	jne	.L604
	subq	$8, %rsp
	movq	%r10, %r8
	movl	$1, %edx
	movq	%r12, %rdi
	pushq	%rbx
	movq	%r13, %r9
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r10, -200(%rbp)
	call	_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode
	movq	-200(%rbp), %r10
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
.L605:
	movq	%r14, %rsi
	movq	%r10, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-200(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L604:
	movq	%r10, -200(%rbp)
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	call	*%rax
	movq	-200(%rbp), %r10
	movq	%rax, %rdi
	jmp	.L605
.L609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2374:
	.size	_ZNK6icu_674IDNA17labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_674IDNA17labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_674IDNA18labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_674IDNA18labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_674IDNA18labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode:
.LFB2375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L616
.L610:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L617
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, %r12
	movq	%rcx, %r14
	movq	%r8, %r13
	movq	%rax, -192(%rbp)
	movq	(%rdi), %rax
	leaq	-128(%rbp), %r15
	movl	$2, %ecx
	movq	%r15, %rdi
	movw	%cx, -184(%rbp)
	movq	%r9, %rbx
	movq	32(%rax), %rax
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	-200(%rbp), %rax
	leaq	_ZNK6icu_675UTS4614labelToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode(%rip), %rdx
	leaq	-192(%rbp), %r10
	cmpq	%rdx, %rax
	jne	.L612
	subq	$8, %rsp
	movq	%r10, %r8
	movl	$1, %edx
	movq	%r12, %rdi
	pushq	%rbx
	movq	%r13, %r9
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r10, -200(%rbp)
	call	_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode
	movq	-200(%rbp), %r10
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
.L613:
	movq	%r14, %rsi
	movq	%r10, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-200(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L612:
	movq	%r10, -200(%rbp)
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	call	*%rax
	movq	-200(%rbp), %r10
	movq	%rax, %rdi
	jmp	.L613
.L617:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2375:
	.size	_ZNK6icu_674IDNA18labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_674IDNA18labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_674IDNA17nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_674IDNA17nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_674IDNA17nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode:
.LFB2377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L624
.L618:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L625
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, %r12
	movq	%rcx, %r14
	movq	%r8, %r13
	movq	%rax, -192(%rbp)
	movq	(%rdi), %rax
	leaq	-128(%rbp), %r15
	movl	$2, %ecx
	movq	%r15, %rdi
	movw	%cx, -184(%rbp)
	movq	%r9, %rbx
	movq	48(%rax), %rax
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	-200(%rbp), %rax
	leaq	_ZNK6icu_675UTS4613nameToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode(%rip), %rdx
	leaq	-192(%rbp), %r10
	cmpq	%rdx, %rax
	jne	.L620
	subq	$8, %rsp
	movq	%r10, %r8
	xorl	%edx, %edx
	movq	%r12, %rdi
	pushq	%rbx
	movq	%r13, %r9
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r10, -200(%rbp)
	call	_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode
	movq	-200(%rbp), %r10
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
.L621:
	movq	%r14, %rsi
	movq	%r10, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-200(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L620:
	movq	%r10, -200(%rbp)
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	call	*%rax
	movq	-200(%rbp), %r10
	movq	%rax, %rdi
	jmp	.L621
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2377:
	.size	_ZNK6icu_674IDNA17nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_674IDNA17nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4611processUTF8ENS_11StringPieceEaaRNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4611processUTF8ENS_11StringPieceEaaRNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4611processUTF8ENS_11StringPieceEaaRNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode:
.LFB2398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %r15
	movq	%rdi, -512(%rbp)
	movq	%rsi, -496(%rbp)
	movq	16(%rbp), %r12
	movq	%rdx, -488(%rbp)
	movl	(%r15), %edi
	movl	%ecx, -504(%rbp)
	movl	%r8d, -500(%rbp)
	movb	%r8b, -520(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L626
	movq	%rsi, %rbx
	movq	%r9, %r13
	movl	-488(%rbp), %esi
	movl	%ecx, %r11d
	testq	%rbx, %rbx
	jne	.L628
	testl	%esi, %esi
	je	.L628
	movl	$1, (%r15)
	.p2align 4,,10
	.p2align 3
.L626:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L708
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	$0, (%r12)
	movw	%cx, 8(%r12)
	movb	$1, 10(%r12)
	testl	%esi, %esi
	je	.L709
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movb	%r11b, -528(%rbp)
	movq	%rax, -448(%rbp)
	movw	%dx, -440(%rbp)
	cmpl	$256, %esi
	jg	.L630
	movq	0(%r13), %rax
	movl	%esi, -544(%rbp)
	leal	20(%rsi), %edx
	movq	%r13, %rdi
	leaq	-320(%rbp), %rcx
	movl	$256, %r8d
	leaq	-468(%rbp), %r9
	xorl	%r14d, %r14d
	call	*24(%rax)
	movl	-544(%rbp), %esi
	movzbl	-528(%rbp), %r11d
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r10
	movq	%rax, %r8
	movq	-512(%rbp), %rax
	movq	%r15, -528(%rbp)
	movl	16(%rax), %eax
	movl	%eax, -532(%rbp)
	shrl	%eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	andl	$1, %ecx
	movl	%ecx, %r15d
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L711:
	addl	$32, %edx
	addl	$1, %r9d
	movb	%dl, (%r8,%rax)
.L636:
	cmpl	%r9d, %esi
	je	.L710
.L643:
	addq	$1, %rax
.L631:
	movzbl	(%rbx,%rax), %edx
	movl	%eax, %r9d
	movl	%eax, %ecx
	testb	%dl, %dl
	js	.L706
	movsbq	%dl, %rdi
	movsbl	(%r10,%rdi), %edi
	testl	%edi, %edi
	jg	.L711
	jns	.L637
	testb	%r15b, %r15b
	jne	.L706
.L637:
	movb	%dl, (%r8,%rax)
	cmpb	$45, %dl
	je	.L712
	cmpb	$46, %dl
	je	.L644
	addl	$1, %r9d
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L630:
	leaq	-384(%rbp), %r14
	movq	-496(%rbp), %rsi
	movq	-488(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	subq	$8, %rsp
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	leaq	-448(%rbp), %r10
	pushq	%r15
	movsbl	-500(%rbp), %r9d
	xorl	%edx, %edx
	pushq	%r12
	movsbl	-504(%rbp), %r8d
	pushq	%r10
	movq	-512(%rbp), %rdi
	movq	%r10, -520(%rbp)
	call	_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode
	movq	%r14, %rdi
	addq	$32, %rsp
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-520(%rbp), %r10
.L648:
	movq	%r10, %rdi
	movq	%r13, %rsi
	movq	%r10, -512(%rbp)
	call	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE@PLT
	cmpb	$0, -500(%rbp)
	movq	-512(%rbp), %r10
	je	.L649
	cmpb	$0, -504(%rbp)
	jne	.L649
	movzwl	-440(%rbp), %esi
	testw	%si, %si
	js	.L650
	movswl	%si, %ecx
	sarl	$5, %ecx
.L651:
	leal	(%r14,%rcx), %edi
	cmpl	$253, %edi
	jle	.L649
	testb	$17, %sil
	jne	.L668
	leaq	-438(%rbp), %rax
	testb	$2, %sil
	cmove	-424(%rbp), %rax
.L652:
	movslq	%ecx, %rdx
	leaq	(%rax,%rdx,2), %rdx
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L713:
	addq	$2, %rax
	cmpw	$127, -2(%rax)
	ja	.L649
.L654:
	cmpq	%rax, %rdx
	ja	.L713
	cmpl	$254, %edi
	je	.L714
.L655:
	orl	$4, (%r12)
	.p2align 4,,10
	.p2align 3
.L649:
	cmpb	$0, 9(%r12)
	je	.L657
	movl	(%r15), %eax
	testl	%eax, %eax
	jle	.L715
.L657:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L712:
	leal	3(%r14), %edx
	movl	%eax, %edi
	cmpl	%eax, %edx
	je	.L716
.L640:
	cmpl	%r14d, %edi
	jne	.L641
	orl	$8, 4(%r12)
.L641:
	addl	$1, %r9d
	cmpl	%r9d, %esi
	je	.L642
	cmpb	$46, 1(%rbx,%rax)
	jne	.L643
.L642:
	orl	$16, 4(%r12)
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L644:
	movl	4(%r12), %edi
	testb	%r11b, %r11b
	jne	.L699
	movl	%edi, %edx
	orl	$1, %edx
	cmpl	%eax, %r14d
	cmove	%edx, %edi
	cmpb	$0, -520(%rbp)
	je	.L646
	movl	%edi, %edx
	subl	%r14d, %ecx
	orl	$2, %edx
	cmpl	$63, %ecx
	cmovg	%edx, %edi
.L646:
	leal	1(%r9), %r14d
	orl	%edi, (%r12)
	movl	$0, 4(%r12)
	movl	%r14d, %r9d
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L716:
	cmpb	$45, -1(%rbx,%rax)
	jne	.L640
	.p2align 4,,10
	.p2align 3
.L706:
	movq	-528(%rbp), %r15
	movl	4(%r12), %edi
.L638:
	orl	%edi, (%r12)
	subl	%r14d, %ecx
	leaq	-384(%rbp), %r11
	movslq	%r14d, %rsi
	movl	%ecx, %edx
	addq	%r8, %rsi
	movq	%r11, %rdi
	movl	%ecx, -532(%rbp)
	movq	%r8, -544(%rbp)
	movq	%r11, -520(%rbp)
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	-520(%rbp), %r11
	leaq	-448(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -528(%rbp)
	movq	%r11, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-520(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	0(%r13), %rax
	movl	%r14d, %edx
	movq	%r13, %rdi
	movq	-544(%rbp), %r8
	movq	%r8, %rsi
	call	*16(%rax)
	leaq	-496(%rbp), %rsi
	leaq	-464(%rbp), %rdi
	movl	%r14d, %edx
	call	_ZN6icu_6711StringPieceC1ERKS0_i@PLT
	movq	-520(%rbp), %r11
	movq	-464(%rbp), %rsi
	movq	-456(%rbp), %rdx
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	-528(%rbp), %r10
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	-520(%rbp), %r11
	movl	-532(%rbp), %ecx
	pushq	%r15
	pushq	%r12
	movsbl	-500(%rbp), %r9d
	pushq	%r10
	movsbl	-504(%rbp), %r8d
	movq	%r11, %rsi
	movq	-512(%rbp), %rdi
	call	_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode
	movq	-520(%rbp), %r11
	addq	$32, %rsp
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-528(%rbp), %r10
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L709:
	movq	0(%r13), %rax
	movl	$1, (%r12)
	movq	%r13, %rdi
	call	*32(%rax)
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L710:
	cmpb	$0, -500(%rbp)
	movl	4(%r12), %eax
	movl	%r9d, %edx
	movl	(%r12), %ecx
	je	.L632
	movl	%r9d, %esi
	subl	%r14d, %esi
	cmpl	$63, %esi
	jle	.L633
	orl	$2, %eax
	movl	%eax, 4(%r12)
.L633:
	cmpb	$0, -504(%rbp)
	jne	.L632
	cmpl	$253, %edx
	jle	.L632
	cmpl	$254, %r14d
	movl	$254, %esi
	cmovg	%esi, %r14d
	movl	%ecx, %esi
	orl	$4, %esi
	cmpl	%edx, %r14d
	cmovl	%esi, %ecx
.L632:
	orl	%ecx, %eax
	movq	%r8, %rsi
	movq	%r13, %rdi
	movl	%eax, (%r12)
	movq	0(%r13), %rax
	call	*16(%rax)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*32(%rax)
	leaq	-448(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L650:
	movl	-436(%rbp), %ecx
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L715:
	movl	(%r12), %esi
	testl	$1984, %esi
	jne	.L657
	cmpb	$0, 10(%r12)
	je	.L658
	testl	%r14d, %r14d
	je	.L657
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L659:
	cmpl	%eax, %ecx
	je	.L717
	cmpb	$32, %dl
	jg	.L707
	leal	-9(%rdx), %edi
	cmpb	$4, %dil
	jbe	.L658
	cmpb	$27, %dl
	jg	.L658
.L707:
	leal	1(%rax), %edx
.L661:
	addq	$1, %rax
	cmpl	%edx, %r14d
	jle	.L657
.L664:
	movzbl	(%rbx,%rax), %edx
	cmpb	$46, %dl
	jne	.L659
	cmpl	%eax, %ecx
	jge	.L660
	movzbl	-1(%rbx,%rax), %ecx
	movl	%ecx, %edx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	jbe	.L660
	subl	$48, %ecx
	cmpb	$9, %cl
	ja	.L658
.L660:
	leal	1(%rax), %ecx
	movl	%ecx, %edx
	jmp	.L661
.L717:
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	jbe	.L707
.L658:
	orl	$2048, %esi
	movl	%esi, (%r12)
	jmp	.L657
.L668:
	xorl	%eax, %eax
	jmp	.L652
.L714:
	cmpl	$253, %r14d
	jg	.L649
	movl	$253, %eax
	subl	%r14d, %eax
	cmpl	%eax, %ecx
	jbe	.L655
	andl	$2, %esi
	leaq	-438(%rbp), %rdx
	cltq
	cmove	-424(%rbp), %rdx
	cmpw	$46, (%rdx,%rax,2)
	jne	.L655
	jmp	.L649
.L699:
	movq	-528(%rbp), %r15
	jmp	.L638
.L708:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2398:
	.size	_ZNK6icu_675UTS4611processUTF8ENS_11StringPieceEaaRNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4611processUTF8ENS_11StringPieceEaaRNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4617labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4617labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4617labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode:
.LFB2393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r9
	movq	%rcx, %r9
	movl	$1, %ecx
	pushq	%r8
	movl	$1, %r8d
	call	_ZNK6icu_675UTS4611processUTF8ENS_11StringPieceEaaRNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2393:
	.size	_ZNK6icu_675UTS4617labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4617labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4618labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4618labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4618labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode:
.LFB2394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r9
	movq	%rcx, %r9
	movl	$1, %ecx
	pushq	%r8
	xorl	%r8d, %r8d
	call	_ZNK6icu_675UTS4611processUTF8ENS_11StringPieceEaaRNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2394:
	.size	_ZNK6icu_675UTS4618labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4618labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4616nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4616nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4616nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode:
.LFB2395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r9
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	pushq	%r8
	movl	$1, %r8d
	call	_ZNK6icu_675UTS4611processUTF8ENS_11StringPieceEaaRNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2395:
	.size	_ZNK6icu_675UTS4616nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4616nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4617nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4617nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4617nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode:
.LFB2396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r9
	movq	%rcx, %r9
	xorl	%ecx, %ecx
	pushq	%r8
	xorl	%r8d, %r8d
	call	_ZNK6icu_675UTS4611processUTF8ENS_11StringPieceEaaRNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2396:
	.size	_ZNK6icu_675UTS4617nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4617nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4612labelToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4612labelToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4612labelToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode:
.LFB2389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L761
	movzwl	8(%rsi), %eax
	movq	%rsi, %r13
	movq	%r8, %r14
	testb	$17, %al
	jne	.L729
	movq	%rdi, %r15
	movq	%rcx, %rbx
	leaq	10(%rsi), %r8
	testb	$2, %al
	je	.L762
.L731:
	cmpq	%r12, %r13
	je	.L729
	testq	%r8, %r8
	je	.L729
	movzwl	8(%r12), %edx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	xorl	%edx, %edx
	movw	%ax, 8(%r12)
	movq	$0, (%rbx)
	movw	%dx, 8(%rbx)
	movb	$1, 10(%rbx)
	movswl	8(%r13), %esi
	testw	%si, %si
	js	.L735
	sarl	$5, %esi
	testl	%esi, %esi
	jne	.L737
.L763:
	movl	$1, (%rbx)
.L728:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L762:
	.cfi_restore_state
	movq	24(%rsi), %r8
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L735:
	movl	12(%r13), %esi
	testl	%esi, %esi
	je	.L763
.L737:
	movq	%r12, %rdi
	movq	%r8, -64(%rbp)
	movl	%esi, -52(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movl	-52(%rbp), %esi
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	je	.L764
	movl	16(%r15), %edx
	movl	4(%rbx), %ecx
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r11
	shrl	%edx
	movl	%ecx, -52(%rbp)
	xorl	%ecx, %ecx
	movl	%edx, %edi
	andl	$1, %edi
	movb	%dil, -64(%rbp)
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L766:
	addl	$32, %edx
	addl	$1, %r10d
	movw	%dx, (%rax,%rcx,2)
.L743:
	cmpl	%r10d, %esi
	je	.L765
.L749:
	addq	$1, %rcx
.L739:
	movzwl	(%r8,%rcx,2), %edx
	movl	%ecx, %r9d
	movl	%ecx, %r10d
	cmpw	$127, %dx
	ja	.L741
	movzwl	%dx, %edi
	movsbl	(%r11,%rdi), %edi
	testl	%edi, %edi
	jg	.L766
	jns	.L753
	cmpb	$0, -64(%rbp)
	jne	.L741
.L753:
	movw	%dx, (%rax,%rcx,2)
	cmpw	$45, %dx
	je	.L767
	leal	1(%r9), %r10d
	cmpw	$46, %dx
	jne	.L743
.L741:
	movl	-52(%rbp), %eax
	orl	%eax, (%rbx)
	movl	%r10d, %esi
	movq	%r12, %rdi
	movl	%r10d, -52(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	subq	$8, %rsp
	movl	-52(%rbp), %r10d
	xorl	%edx, %edx
	pushq	%r14
	movl	$1, %r9d
	movq	%r13, %rsi
	movq	%r15, %rdi
	pushq	%rbx
	movl	$1, %r8d
	movl	%r10d, %ecx
	pushq	%r12
	call	_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode
	addq	$32, %rsp
	cmpb	$0, 9(%rbx)
	je	.L728
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L728
	movl	(%rbx), %eax
	testl	$1984, %eax
	jne	.L728
	cmpb	$0, 10(%rbx)
	jne	.L728
	orb	$8, %ah
	movl	%eax, (%rbx)
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L729:
	movl	$1, (%r14)
.L761:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L767:
	cmpq	$3, %rcx
	je	.L768
	testl	%ecx, %ecx
	jne	.L747
	orl	$8, -52(%rbp)
	movl	-52(%rbp), %edi
	movl	%edi, 4(%rbx)
.L747:
	leal	1(%r9), %r10d
	cmpl	%r10d, %esi
	je	.L748
	cmpw	$46, 2(%r8,%rcx,2)
	jne	.L749
.L748:
	orl	$16, -52(%rbp)
	movl	-52(%rbp), %edi
	movl	%edi, 4(%rbx)
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L768:
	cmpw	$45, 4(%r8)
	jne	.L747
	movl	$4, %r10d
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L764:
	movl	$7, (%r14)
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L765:
	cmpl	$63, %r10d
	jle	.L740
	orl	$2, -52(%rbp)
	movl	-52(%rbp), %eax
	movl	%eax, 4(%rbx)
.L740:
	movl	-52(%rbp), %eax
	orl	%eax, (%rbx)
	movl	%r10d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L728
	.cfi_endproc
.LFE2389:
	.size	_ZNK6icu_675UTS4612labelToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4612labelToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4613nameToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4613nameToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4613nameToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode:
.LFB2392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movl	(%r8), %edi
	testl	%edi, %edi
	jg	.L819
	movzwl	8(%rsi), %eax
	movq	%rsi, %r14
	movq	%r8, %r15
	testb	$17, %al
	jne	.L772
	movq	%rcx, %rbx
	leaq	10(%rsi), %r8
	testb	$2, %al
	je	.L820
.L774:
	cmpq	%r12, %r14
	je	.L772
	testq	%r8, %r8
	je	.L772
	movzwl	8(%r12), %edx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	xorl	%edx, %edx
	movw	%ax, 8(%r12)
	movq	$0, (%rbx)
	movw	%dx, 8(%rbx)
	movb	$1, 10(%rbx)
	movswl	8(%r14), %esi
	testw	%si, %si
	js	.L778
	sarl	$5, %esi
	testl	%esi, %esi
	jne	.L780
.L821:
	movl	$1, (%rbx)
.L771:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_restore_state
	movq	24(%rsi), %r8
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L778:
	movl	12(%r14), %esi
	testl	%esi, %esi
	je	.L821
.L780:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	movl	%esi, -64(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movl	-64(%rbp), %esi
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	je	.L822
	movq	-56(%rbp), %rdi
	movl	(%rbx), %r11d
	movq	%r14, -72(%rbp)
	xorl	%r10d, %r10d
	movq	%r15, -80(%rbp)
	movl	16(%rdi), %edi
	movq	%r12, -64(%rbp)
	movl	%r11d, %r15d
	movl	%edi, %edx
	movl	4(%rbx), %edi
	shrl	%edx
	movl	%edx, %ecx
	xorl	%edx, %edx
	andl	$1, %ecx
	movl	%ecx, %r14d
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L824:
	addl	$32, %ecx
	addl	$1, %r13d
	movw	%cx, (%rax,%rdx,2)
.L785:
	cmpl	%r13d, %esi
	je	.L823
.L791:
	addq	$1, %rdx
.L782:
	movzwl	(%r8,%rdx,2), %ecx
	movl	%edx, %r11d
	movl	%edx, %r13d
	cmpw	$127, %cx
	ja	.L783
	movzwl	%cx, %r9d
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r12
	movsbl	(%r12,%r9), %r9d
	testl	%r9d, %r9d
	jg	.L824
	jns	.L806
	testb	%r14b, %r14b
	jne	.L783
.L806:
	movw	%cx, (%rax,%rdx,2)
	leal	1(%r11), %r13d
	cmpw	$45, %cx
	je	.L825
	cmpw	$46, %cx
	jne	.L785
	movl	%edi, %ecx
	movl	$0, 4(%rbx)
	orl	$1, %ecx
	cmpl	%edx, %r10d
	movl	%r13d, %r10d
	cmove	%ecx, %edi
	orl	%edi, %r15d
	xorl	%edi, %edi
	movl	%r15d, (%rbx)
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L772:
	movl	$1, (%r15)
.L819:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L825:
	leal	3(%r10), %ecx
	movl	%edx, %r9d
	cmpl	%ecx, %edx
	je	.L826
.L788:
	cmpl	%r10d, %r9d
	jne	.L789
	orl	$8, %edi
	movl	%edi, 4(%rbx)
.L789:
	cmpl	%r13d, %esi
	je	.L790
	cmpw	$46, 2(%r8,%rdx,2)
	jne	.L791
.L790:
	orl	$16, %edi
	movl	%edi, 4(%rbx)
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L826:
	cmpw	$45, -2(%r8,%rdx,2)
	jne	.L788
	.p2align 4,,10
	.p2align 3
.L783:
	movl	%r15d, %eax
	movq	-64(%rbp), %r12
	movq	-80(%rbp), %r15
	movl	%r13d, %esi
	orl	%eax, %edi
	movl	%r10d, -84(%rbp)
	movq	-72(%rbp), %r14
	movl	%edi, (%rbx)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	subq	$8, %rsp
	movl	-84(%rbp), %r10d
	xorl	%r9d, %r9d
	pushq	%r15
	movq	-56(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	%r13d, %ecx
	pushq	%rbx
	movl	%r10d, %edx
	movq	%r14, %rsi
	pushq	%r12
	movl	%r10d, -64(%rbp)
	call	_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode
	addq	$32, %rsp
	cmpb	$0, 9(%rbx)
	je	.L771
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L771
	movl	(%rbx), %edi
	testl	$1984, %edi
	jne	.L771
	cmpb	$0, 10(%rbx)
	je	.L794
	movl	-64(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L771
	movzwl	8(%r12), %eax
	testb	$17, %al
	jne	.L805
	leaq	10(%r12), %rsi
	testb	$2, %al
	jne	.L795
	movq	24(%r12), %rsi
.L795:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L797:
	cmpl	%eax, %ecx
	je	.L827
	cmpw	$32, %dx
	ja	.L818
	leal	-9(%rdx), %r8d
	cmpw	$4, %r8w
	jbe	.L794
	cmpw	$27, %dx
	ja	.L794
.L818:
	leal	1(%rax), %edx
.L800:
	addq	$1, %rax
	cmpl	%r10d, %edx
	jge	.L771
.L803:
	movzwl	(%rsi,%rax,2), %edx
	cmpw	$46, %dx
	jne	.L797
	cmpl	%eax, %ecx
	jge	.L798
	movzwl	-2(%rsi,%rax,2), %edx
	leal	-97(%rdx), %ecx
	cmpw	$25, %cx
	jbe	.L798
	subl	$48, %edx
	cmpw	$9, %dx
	ja	.L794
.L798:
	leal	1(%rax), %ecx
	movl	%ecx, %edx
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L823:
	orl	%r15d, %edi
	movq	-64(%rbp), %r12
	movl	%edi, (%rbx)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L822:
	movl	$7, (%r15)
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L827:
	subl	$97, %edx
	cmpw	$25, %dx
	jbe	.L818
.L794:
	orl	$2048, %edi
	movl	%edi, (%rbx)
	jmp	.L771
.L805:
	xorl	%esi, %esi
	jmp	.L795
	.cfi_endproc
.LFE2392:
	.size	_ZNK6icu_675UTS4613nameToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4613nameToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4614labelToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4614labelToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4614labelToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode:
.LFB2390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L862
	movzwl	8(%rsi), %eax
	movq	%rsi, %r13
	movq	%r8, %r14
	testb	$17, %al
	jne	.L831
	movq	%rdi, %r15
	movq	%rcx, %rbx
	leaq	10(%rsi), %r8
	testb	$2, %al
	je	.L863
.L833:
	cmpq	%r12, %r13
	je	.L831
	testq	%r8, %r8
	je	.L831
	movzwl	8(%r12), %edx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	xorl	%edx, %edx
	movw	%ax, 8(%r12)
	movq	$0, (%rbx)
	movw	%dx, 8(%rbx)
	movb	$1, 10(%rbx)
	movswl	8(%r13), %esi
	testw	%si, %si
	js	.L837
	sarl	$5, %esi
	testl	%esi, %esi
	jne	.L839
.L864:
	movl	$1, (%rbx)
.L830:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L863:
	.cfi_restore_state
	movq	24(%rsi), %r8
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L837:
	movl	12(%r13), %esi
	testl	%esi, %esi
	je	.L864
.L839:
	movq	%r12, %rdi
	movq	%r8, -64(%rbp)
	movl	%esi, -52(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movl	-52(%rbp), %esi
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	je	.L865
	movl	16(%r15), %edx
	movl	4(%rbx), %ecx
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r11
	shrl	%edx
	movl	%ecx, -52(%rbp)
	xorl	%ecx, %ecx
	movl	%edx, %edi
	andl	$1, %edi
	movb	%dil, -64(%rbp)
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L867:
	addl	$32, %edx
	addl	$1, %r10d
	movw	%dx, (%rax,%rcx,2)
.L844:
	cmpl	%r10d, %esi
	je	.L866
.L850:
	addq	$1, %rcx
.L841:
	movzwl	(%r8,%rcx,2), %edx
	movl	%ecx, %r9d
	movl	%ecx, %r10d
	cmpw	$127, %dx
	ja	.L842
	movzwl	%dx, %edi
	movsbl	(%r11,%rdi), %edi
	testl	%edi, %edi
	jg	.L867
	jns	.L854
	cmpb	$0, -64(%rbp)
	jne	.L842
.L854:
	movw	%dx, (%rax,%rcx,2)
	cmpw	$45, %dx
	je	.L868
	leal	1(%r9), %r10d
	cmpw	$46, %dx
	jne	.L844
.L842:
	movl	-52(%rbp), %eax
	orl	%eax, (%rbx)
	movl	%r10d, %esi
	movq	%r12, %rdi
	movl	%r10d, -52(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	subq	$8, %rsp
	movl	-52(%rbp), %r10d
	xorl	%r9d, %r9d
	pushq	%r14
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r13, %rsi
	pushq	%rbx
	movl	%r10d, %ecx
	movq	%r15, %rdi
	pushq	%r12
	call	_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode
	addq	$32, %rsp
	cmpb	$0, 9(%rbx)
	je	.L830
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L830
	movl	(%rbx), %eax
	testl	$1984, %eax
	jne	.L830
	cmpb	$0, 10(%rbx)
	jne	.L830
	orb	$8, %ah
	movl	%eax, (%rbx)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L831:
	movl	$1, (%r14)
.L862:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L868:
	cmpq	$3, %rcx
	je	.L869
	testl	%ecx, %ecx
	jne	.L848
	orl	$8, -52(%rbp)
	movl	-52(%rbp), %edi
	movl	%edi, 4(%rbx)
.L848:
	leal	1(%r9), %r10d
	cmpl	%r10d, %esi
	je	.L849
	cmpw	$46, 2(%r8,%rcx,2)
	jne	.L850
.L849:
	orl	$16, -52(%rbp)
	movl	-52(%rbp), %edi
	movl	%edi, 4(%rbx)
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L869:
	cmpw	$45, 4(%r8)
	jne	.L848
	movl	$4, %r10d
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L865:
	movl	$7, (%r14)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L866:
	movl	-52(%rbp), %eax
	orl	%eax, (%rbx)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L830
	.cfi_endproc
.LFE2390:
	.size	_ZNK6icu_675UTS4614labelToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4614labelToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675UTS4611nameToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.type	_ZNK6icu_675UTS4611nameToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode, @function
_ZNK6icu_675UTS4611nameToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode:
.LFB2391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%rdi, -56(%rbp)
	movl	(%r8), %edi
	testl	%edi, %edi
	jg	.L943
	movzwl	8(%rsi), %eax
	movq	%rsi, %r13
	movq	%r8, %r14
	testb	$17, %al
	jne	.L873
	leaq	10(%rsi), %r8
	testb	$2, %al
	je	.L944
.L875:
	cmpq	%r12, %r13
	je	.L873
	testq	%r8, %r8
	je	.L873
	movzwl	8(%r12), %edx
	movl	$2, %eax
	movl	%edx, %ecx
	andl	$31, %ecx
	andl	$1, %edx
	cmove	%ecx, %eax
	xorl	%ecx, %ecx
	movw	%ax, 8(%r12)
	movq	$0, (%rbx)
	movw	%cx, 8(%rbx)
	movb	$1, 10(%rbx)
	movswl	8(%r13), %esi
	testw	%si, %si
	js	.L879
	sarl	$5, %esi
	testl	%esi, %esi
	jne	.L881
.L948:
	movl	$1, (%rbx)
.L882:
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L911:
	cmpl	$253, %ecx
	jle	.L928
	movl	(%rbx), %esi
	testb	$4, %sil
	jne	.L928
	testb	$17, %al
	jne	.L924
	leaq	10(%r12), %rdx
	testb	$2, %al
	je	.L945
.L913:
	movslq	%ecx, %rdi
	leaq	(%rdx,%rdi,2), %rdi
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L946:
	addq	$2, %rdx
	cmpw	$127, -2(%rdx)
	ja	.L928
.L916:
	cmpq	%rdx, %rdi
	ja	.L946
	cmpl	$254, %ecx
	je	.L947
.L919:
	orl	$4, %esi
	movl	%esi, (%rbx)
.L928:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore_state
	movq	24(%rsi), %r8
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L879:
	movl	12(%r13), %esi
	testl	%esi, %esi
	je	.L948
.L881:
	movq	%r12, %rdi
	movq	%r8, -72(%rbp)
	movl	%esi, -64(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movl	-64(%rbp), %esi
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	je	.L949
	movq	-56(%rbp), %rcx
	movl	(%rbx), %edi
	movq	%r13, -72(%rbp)
	xorl	%r15d, %r15d
	movq	%r14, -80(%rbp)
	movl	4(%rbx), %r10d
	movl	16(%rcx), %ecx
	movq	%r12, -64(%rbp)
	movl	%edi, %r14d
	movl	%ecx, %edx
	shrl	%edx
	movl	%edx, %ecx
	xorl	%edx, %edx
	andl	$1, %ecx
	movl	%ecx, %r13d
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L951:
	addl	$32, %edi
	addl	$1, %r9d
	movw	%di, (%rax,%rdx,2)
.L889:
	cmpl	%esi, %r9d
	je	.L950
.L895:
	addq	$1, %rdx
.L884:
	movzwl	(%r8,%rdx,2), %edi
	movl	%edx, %r9d
	movl	%edx, %ecx
	cmpw	$127, %di
	ja	.L935
	movzwl	%di, %r11d
	leaq	_ZN6icu_67L9asciiDataE(%rip), %r12
	movsbl	(%r12,%r11), %r11d
	testl	%r11d, %r11d
	jg	.L951
	jns	.L925
	testb	%r13b, %r13b
	jne	.L935
.L925:
	movw	%di, (%rax,%rdx,2)
	addl	$1, %r9d
	cmpw	$45, %di
	je	.L952
	cmpw	$46, %di
	jne	.L889
	movl	%r10d, %edi
	movl	$0, 4(%rbx)
	orl	$1, %edi
	cmpl	%edx, %r15d
	cmove	%edi, %r10d
	subl	%r15d, %ecx
	movl	%r9d, %r15d
	movl	%r10d, %edi
	orl	$2, %edi
	cmpl	$63, %ecx
	cmovg	%edi, %r10d
	orl	%r10d, %r14d
	xorl	%r10d, %r10d
	movl	%r14d, (%rbx)
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L873:
	movl	$1, (%r14)
.L943:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movzwl	8(%r12), %eax
.L872:
	testw	%ax, %ax
	jns	.L882
	movl	12(%r12), %ecx
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L952:
	leal	3(%r15), %ecx
	movl	%edx, %edi
	cmpl	%edx, %ecx
	je	.L953
.L892:
	cmpl	%r15d, %edi
	jne	.L893
	orl	$8, %r10d
	movl	%r10d, 4(%rbx)
.L893:
	cmpl	%r9d, %esi
	je	.L894
	cmpw	$46, 2(%r8,%rdx,2)
	jne	.L895
.L894:
	orl	$16, %r10d
	movl	%r10d, 4(%rbx)
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L945:
	movq	24(%r12), %rdx
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L953:
	cmpw	$45, -2(%r8,%rdx,2)
	jne	.L892
	movl	%r14d, %edx
	movq	-72(%rbp), %r13
	movq	-64(%rbp), %r12
	movl	%r9d, %ecx
	movq	-80(%rbp), %r14
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L935:
	movl	%r14d, %edx
	movq	-72(%rbp), %r13
	movq	-64(%rbp), %r12
	movq	-80(%rbp), %r14
.L887:
	orl	%edx, %r10d
	movl	%ecx, %esi
	movq	%r12, %rdi
	movl	%ecx, -64(%rbp)
	movl	%r10d, (%rbx)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	subq	$8, %rsp
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %rdi
	pushq	%r14
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movl	%r15d, %edx
	pushq	%rbx
	movq	%r13, %rsi
	pushq	%r12
	call	_ZNK6icu_675UTS4614processUnicodeERKNS_13UnicodeStringEiiaaRS1_RNS_8IDNAInfoER10UErrorCode
	addq	$32, %rsp
	cmpb	$0, 9(%rbx)
	movzwl	8(%r12), %eax
	je	.L872
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L872
	movl	(%rbx), %r8d
	testl	$1984, %r8d
	jne	.L872
	cmpb	$0, 10(%rbx)
	je	.L900
	testl	%r15d, %r15d
	je	.L872
	testb	$17, %al
	jne	.L923
	leaq	10(%r12), %rdi
	testb	$2, %al
	jne	.L901
	movq	24(%r12), %rdi
.L901:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L903:
	cmpl	%edx, %esi
	je	.L954
	cmpw	$32, %cx
	ja	.L942
	leal	-9(%rcx), %r9d
	cmpw	$4, %r9w
	jbe	.L900
	cmpw	$27, %cx
	ja	.L900
.L942:
	leal	1(%rdx), %ecx
.L906:
	addq	$1, %rdx
	cmpl	%r15d, %ecx
	jge	.L872
.L909:
	movzwl	(%rdi,%rdx,2), %ecx
	cmpw	$46, %cx
	jne	.L903
	cmpl	%edx, %esi
	jge	.L904
	movzwl	-2(%rdi,%rdx,2), %ecx
	leal	-97(%rcx), %esi
	cmpw	$25, %si
	jbe	.L904
	subl	$48, %ecx
	cmpw	$9, %cx
	ja	.L900
.L904:
	leal	1(%rdx), %esi
	movl	%esi, %ecx
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L950:
	movl	%esi, %eax
	movq	-64(%rbp), %r12
	movl	%r14d, %edx
	subl	%r15d, %eax
	cmpl	$63, %eax
	jle	.L885
	orl	$2, %r10d
	movl	%r10d, 4(%rbx)
.L885:
	cmpl	$253, %esi
	jle	.L886
	cmpl	$254, %r15d
	movl	$254, %eax
	cmovg	%eax, %r15d
	movl	%edx, %eax
	orl	$4, %eax
	cmpl	%r15d, %esi
	cmovle	%edx, %eax
	movl	%eax, %edx
.L886:
	orl	%edx, %r10d
	movq	%r12, %rdi
	movl	%r10d, (%rbx)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movzwl	8(%r12), %eax
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L949:
	movl	$7, (%r14)
	movzwl	8(%r12), %eax
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L924:
	xorl	%edx, %edx
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L947:
	testb	$2, %al
	je	.L917
	leaq	10(%r12), %rax
.L918:
	cmpw	$46, 506(%rax)
	jne	.L919
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L917:
	movq	24(%r12), %rax
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L954:
	subl	$97, %ecx
	cmpw	$25, %cx
	jbe	.L942
.L900:
	orl	$2048, %r8d
	movl	%r8d, (%rbx)
	jmp	.L872
.L923:
	xorl	%edi, %edi
	jmp	.L901
	.cfi_endproc
.LFE2391:
	.size	_ZNK6icu_675UTS4611nameToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode, .-_ZNK6icu_675UTS4611nameToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.p2align 4
	.globl	uidna_openUTS46_67
	.type	uidna_openUTS46_67, @function
uidna_openUTS46_67:
.LFB2410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L961
.L955:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L961:
	.cfi_restore_state
	movl	%edi, %r13d
	movl	$24, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L957
	leaq	16+_ZTVN6icu_675UTS46E(%rip), %rax
	movq	%rbx, %rcx
	xorl	%edx, %edx
	xorl	%edi, %edi
	movq	%rax, (%r12)
	leaq	.LC0(%rip), %rsi
	call	_ZN6icu_6711Normalizer211getInstanceEPKcS2_19UNormalization2ModeR10UErrorCode@PLT
	movl	%r13d, 16(%r12)
	movq	%rax, 8(%r12)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L955
	movq	(%r12), %rax
	leaq	_ZN6icu_675UTS46D0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L958
	leaq	16+_ZTVN6icu_674IDNAE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L958:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L955
.L957:
	movl	$7, (%rbx)
	jmp	.L955
	.cfi_endproc
.LFE2410:
	.size	uidna_openUTS46_67, .-uidna_openUTS46_67
	.p2align 4
	.globl	uidna_close_67
	.type	uidna_close_67, @function
uidna_close_67:
.LFB2411:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L962
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_675UTS46D0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L964
	leaq	16+_ZTVN6icu_674IDNAE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L962:
	ret
	.p2align 4,,10
	.p2align 3
.L964:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2411:
	.size	uidna_close_67, .-uidna_close_67
	.p2align 4
	.globl	uidna_labelToASCII_67
	.type	uidna_labelToASCII_67, @function
uidna_labelToASCII_67:
.LFB2414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r11
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r11), %r10d
	testl	%r10d, %r10d
	jg	.L968
	movq	%r9, %rbx
	testq	%r9, %r9
	je	.L970
	movl	%edx, %r13d
	movswq	(%r9), %rdx
	cmpw	$15, %dx
	jle	.L970
	movq	%rdi, %r10
	movq	%rsi, %r14
	movq	%rcx, %r12
	movl	%r8d, %r15d
	testq	%rsi, %rsi
	je	.L989
	cmpl	$-1, %r13d
	jl	.L970
.L973:
	testq	%r12, %r12
	je	.L990
	testl	%r15d, %r15d
	js	.L970
.L975:
	cmpq	%r12, %r14
	jne	.L981
	testq	%r14, %r14
	je	.L981
	.p2align 4,,10
	.p2align 3
.L970:
	movl	$1, (%r11)
	xorl	%eax, %eax
.L968:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L991
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L990:
	.cfi_restore_state
	testl	%r15d, %r15d
	je	.L975
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L981:
	subq	$2, %rdx
	leaq	2(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r11, -248(%rbp)
	movq	%r10, -240(%rbp)
	call	memset@PLT
	leaq	-208(%rbp), %r9
	movl	%r13d, %esi
	movl	%r13d, %ecx
	movq	%r14, -208(%rbp)
	shrl	$31, %esi
	movq	%r9, %rdx
	leaq	-192(%rbp), %r14
	movq	%r14, %rdi
	movq	%r9, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-128(%rbp), %r13
	xorl	%edx, %edx
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	-240(%rbp), %r10
	xorl	%ecx, %ecx
	movb	$1, -198(%rbp)
	movw	%cx, -200(%rbp)
	leaq	_ZNK6icu_675UTS4612labelToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode(%rip), %rdx
	movq	-232(%rbp), %r9
	movq	(%r10), %rax
	movq	-248(%rbp), %r11
	movq	$0, -208(%rbp)
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L977
	subq	$8, %rsp
	movl	$1, %edx
	movq	%r13, %r8
	movq	%r14, %rsi
	pushq	%r11
	movl	$1, %ecx
	movq	%r10, %rdi
	movq	%r11, -232(%rbp)
	call	_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode
	movq	-232(%rbp), %r11
	popq	%rax
	popq	%rdx
.L978:
	movzbl	-200(%rbp), %eax
	movq	%r11, %rcx
	movl	%r15d, %edx
	movq	%r13, %rdi
	leaq	-216(%rbp), %rsi
	movq	%r12, -216(%rbp)
	movb	%al, 2(%rbx)
	movl	-208(%rbp), %eax
	movl	%eax, 4(%rbx)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -232(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-232(%rbp), %eax
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L989:
	testl	%r13d, %r13d
	je	.L973
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L977:
	movq	%r11, -232(%rbp)
	movq	%r11, %r8
	movq	%r9, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	*%rax
	movq	-232(%rbp), %r11
	jmp	.L978
.L991:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2414:
	.size	uidna_labelToASCII_67, .-uidna_labelToASCII_67
	.p2align 4
	.globl	uidna_labelToUnicode_67
	.type	uidna_labelToUnicode_67, @function
uidna_labelToUnicode_67:
.LFB2415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r11
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r11), %r10d
	testl	%r10d, %r10d
	jg	.L992
	movq	%r9, %rbx
	testq	%r9, %r9
	je	.L994
	movl	%edx, %r13d
	movswq	(%r9), %rdx
	cmpw	$15, %dx
	jle	.L994
	movq	%rdi, %r10
	movq	%rsi, %r14
	movq	%rcx, %r12
	movl	%r8d, %r15d
	testq	%rsi, %rsi
	je	.L1013
	cmpl	$-1, %r13d
	jl	.L994
.L997:
	testq	%r12, %r12
	je	.L1014
	testl	%r15d, %r15d
	js	.L994
.L999:
	cmpq	%r12, %r14
	jne	.L1005
	testq	%r14, %r14
	je	.L1005
	.p2align 4,,10
	.p2align 3
.L994:
	movl	$1, (%r11)
	xorl	%eax, %eax
.L992:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1015
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	.cfi_restore_state
	testl	%r15d, %r15d
	je	.L999
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1005:
	subq	$2, %rdx
	leaq	2(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r11, -248(%rbp)
	movq	%r10, -240(%rbp)
	call	memset@PLT
	leaq	-208(%rbp), %r9
	movl	%r13d, %esi
	movl	%r13d, %ecx
	movq	%r14, -208(%rbp)
	shrl	$31, %esi
	movq	%r9, %rdx
	leaq	-192(%rbp), %r14
	movq	%r14, %rdi
	movq	%r9, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-128(%rbp), %r13
	xorl	%edx, %edx
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	-240(%rbp), %r10
	xorl	%ecx, %ecx
	movb	$1, -198(%rbp)
	movw	%cx, -200(%rbp)
	leaq	_ZNK6icu_675UTS4614labelToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode(%rip), %rdx
	movq	-232(%rbp), %r9
	movq	(%r10), %rax
	movq	-248(%rbp), %r11
	movq	$0, -208(%rbp)
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1001
	subq	$8, %rsp
	movl	$1, %edx
	movq	%r13, %r8
	xorl	%ecx, %ecx
	pushq	%r11
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r11, -232(%rbp)
	call	_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode
	movq	-232(%rbp), %r11
	popq	%rax
	popq	%rdx
.L1002:
	movzbl	-200(%rbp), %eax
	movq	%r11, %rcx
	movl	%r15d, %edx
	movq	%r13, %rdi
	leaq	-216(%rbp), %rsi
	movq	%r12, -216(%rbp)
	movb	%al, 2(%rbx)
	movl	-208(%rbp), %eax
	movl	%eax, 4(%rbx)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -232(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-232(%rbp), %eax
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1013:
	testl	%r13d, %r13d
	je	.L997
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	%r11, -232(%rbp)
	movq	%r11, %r8
	movq	%r9, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	*%rax
	movq	-232(%rbp), %r11
	jmp	.L1002
.L1015:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2415:
	.size	uidna_labelToUnicode_67, .-uidna_labelToUnicode_67
	.p2align 4
	.globl	uidna_nameToASCII_67
	.type	uidna_nameToASCII_67, @function
uidna_nameToASCII_67:
.LFB2416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %r10d
	testl	%r10d, %r10d
	jg	.L1016
	movq	%r9, %rbx
	testq	%r9, %r9
	je	.L1018
	movl	%edx, %r13d
	movswq	(%r9), %rdx
	cmpw	$15, %dx
	jle	.L1018
	movq	%rdi, %r10
	movq	%rsi, %r14
	movq	%rcx, %r12
	testq	%rsi, %rsi
	je	.L1035
	cmpl	$-1, %r13d
	jl	.L1018
.L1021:
	testq	%r12, %r12
	je	.L1036
	testl	%r15d, %r15d
	js	.L1018
.L1023:
	cmpq	%r12, %r14
	jne	.L1027
	testq	%r14, %r14
	je	.L1027
	.p2align 4,,10
	.p2align 3
.L1018:
	movl	$1, (%r8)
	xorl	%eax, %eax
.L1016:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1037
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1036:
	.cfi_restore_state
	testl	%r15d, %r15d
	je	.L1023
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1027:
	subq	$2, %rdx
	leaq	2(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r8, -248(%rbp)
	movq	%r10, -232(%rbp)
	call	memset@PLT
	leaq	-208(%rbp), %r9
	movl	%r13d, %esi
	movl	%r13d, %ecx
	movq	%r14, -208(%rbp)
	shrl	$31, %esi
	movq	%r9, %rdx
	leaq	-192(%rbp), %r14
	movq	%r14, %rdi
	movq	%r9, -240(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-128(%rbp), %r13
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	xorl	%eax, %eax
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	-232(%rbp), %r10
	movq	-248(%rbp), %r8
	movw	%ax, -200(%rbp)
	movq	-240(%rbp), %r9
	movb	$1, -198(%rbp)
	movq	(%r10), %rax
	movq	%r10, %rdi
	movq	%r8, -232(%rbp)
	movq	$0, -208(%rbp)
	movq	%r9, %rcx
	call	*40(%rax)
	movzbl	-200(%rbp), %eax
	movl	%r15d, %edx
	movq	%r13, %rdi
	movq	-232(%rbp), %r8
	leaq	-216(%rbp), %rsi
	movq	%r12, -216(%rbp)
	movb	%al, 2(%rbx)
	movl	-208(%rbp), %eax
	movq	%r8, %rcx
	movl	%eax, 4(%rbx)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -232(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-232(%rbp), %eax
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1035:
	testl	%r13d, %r13d
	je	.L1021
	jmp	.L1018
.L1037:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2416:
	.size	uidna_nameToASCII_67, .-uidna_nameToASCII_67
	.p2align 4
	.globl	uidna_nameToUnicode_67
	.type	uidna_nameToUnicode_67, @function
uidna_nameToUnicode_67:
.LFB2417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r11
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r11), %r10d
	testl	%r10d, %r10d
	jg	.L1038
	movq	%r9, %rbx
	testq	%r9, %r9
	je	.L1040
	movl	%edx, %r13d
	movswq	(%r9), %rdx
	cmpw	$15, %dx
	jle	.L1040
	movq	%rdi, %r10
	movq	%rsi, %r14
	movq	%rcx, %r12
	movl	%r8d, %r15d
	testq	%rsi, %rsi
	je	.L1059
	cmpl	$-1, %r13d
	jl	.L1040
.L1043:
	testq	%r12, %r12
	je	.L1060
	testl	%r15d, %r15d
	js	.L1040
.L1045:
	cmpq	%r12, %r14
	jne	.L1051
	testq	%r14, %r14
	je	.L1051
	.p2align 4,,10
	.p2align 3
.L1040:
	movl	$1, (%r11)
	xorl	%eax, %eax
.L1038:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1061
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1060:
	.cfi_restore_state
	testl	%r15d, %r15d
	je	.L1045
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1051:
	subq	$2, %rdx
	leaq	2(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r11, -248(%rbp)
	movq	%r10, -240(%rbp)
	call	memset@PLT
	leaq	-208(%rbp), %r9
	movl	%r13d, %esi
	movl	%r13d, %ecx
	movq	%r14, -208(%rbp)
	shrl	$31, %esi
	movq	%r9, %rdx
	leaq	-192(%rbp), %r14
	movq	%r14, %rdi
	movq	%r9, -232(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-128(%rbp), %r13
	xorl	%edx, %edx
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	-240(%rbp), %r10
	xorl	%ecx, %ecx
	movb	$1, -198(%rbp)
	movw	%cx, -200(%rbp)
	leaq	_ZNK6icu_675UTS4613nameToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode(%rip), %rdx
	movq	-232(%rbp), %r9
	movq	(%r10), %rax
	movq	-248(%rbp), %r11
	movq	$0, -208(%rbp)
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1047
	subq	$8, %rsp
	xorl	%edx, %edx
	movq	%r13, %r8
	xorl	%ecx, %ecx
	pushq	%r11
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r11, -232(%rbp)
	call	_ZNK6icu_675UTS467processERKNS_13UnicodeStringEaaRS1_RNS_8IDNAInfoER10UErrorCode
	movq	-232(%rbp), %r11
	popq	%rax
	popq	%rdx
.L1048:
	movzbl	-200(%rbp), %eax
	movq	%r11, %rcx
	movl	%r15d, %edx
	movq	%r13, %rdi
	leaq	-216(%rbp), %rsi
	movq	%r12, -216(%rbp)
	movb	%al, 2(%rbx)
	movl	-208(%rbp), %eax
	movl	%eax, 4(%rbx)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -232(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-232(%rbp), %eax
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1059:
	testl	%r13d, %r13d
	je	.L1043
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1047:
	movq	%r11, -232(%rbp)
	movq	%r11, %r8
	movq	%r9, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r10, %rdi
	call	*%rax
	movq	-232(%rbp), %r11
	jmp	.L1048
.L1061:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2417:
	.size	uidna_nameToUnicode_67, .-uidna_nameToUnicode_67
	.p2align 4
	.globl	uidna_labelToASCII_UTF8_67
	.type	uidna_labelToASCII_UTF8_67, @function
uidna_labelToASCII_UTF8_67:
.LFB2418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	movq	%rdi, -120(%rbp)
	movl	(%r15), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L1063
	movq	%r9, %rbx
	testq	%r9, %r9
	je	.L1064
	movl	%edx, %r11d
	movswq	(%r9), %rdx
	cmpw	$15, %dx
	jle	.L1064
	movq	%rsi, %r13
	movq	%rcx, %r12
	movl	%r8d, %r14d
	testq	%rsi, %rsi
	je	.L1082
	cmpl	$-1, %r11d
	jl	.L1064
.L1067:
	testq	%r12, %r12
	je	.L1083
	testl	%r14d, %r14d
	js	.L1064
.L1069:
	cmpq	%r12, %r13
	jne	.L1074
	testq	%r13, %r13
	je	.L1074
	.p2align 4,,10
	.p2align 3
.L1064:
	movl	$1, (%r15)
	xorl	%eax, %eax
.L1063:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1084
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1083:
	.cfi_restore_state
	testl	%r14d, %r14d
	je	.L1069
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1074:
	subq	$2, %rdx
	leaq	2(%rbx), %rdi
	xorl	%esi, %esi
	movl	%r11d, -128(%rbp)
	call	memset@PLT
	movl	-128(%rbp), %r11d
	testl	%r11d, %r11d
	jns	.L1071
	movq	%r13, %rdi
	call	strlen@PLT
	movl	%eax, %r11d
.L1071:
	leaq	-96(%rbp), %r10
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r11, -136(%rbp)
	movq	%r10, %rdi
	movq	%r10, -128(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	movq	-120(%rbp), %rdi
	xorl	%eax, %eax
	movq	-128(%rbp), %r10
	movw	%ax, -100(%rbp)
	movq	%r15, %r9
	leaq	-108(%rbp), %r8
	movq	%r13, %rsi
	movq	-136(%rbp), %r11
	movq	(%rdi), %rax
	movq	%r10, %rcx
	movq	$0, -108(%rbp)
	movb	$1, -98(%rbp)
	movq	%r11, %rdx
	call	*56(%rax)
	movzbl	-100(%rbp), %eax
	movl	-72(%rbp), %edx
	movq	%r15, %rcx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movb	%al, 2(%rbx)
	movl	-108(%rbp), %eax
	movl	%eax, 4(%rbx)
	call	u_terminateChars_67@PLT
	movq	-128(%rbp), %r10
	movl	%eax, -120(%rbp)
	movq	%r10, %rdi
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-120(%rbp), %eax
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1082:
	testl	%r11d, %r11d
	je	.L1067
	jmp	.L1064
.L1084:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2418:
	.size	uidna_labelToASCII_UTF8_67, .-uidna_labelToASCII_UTF8_67
	.p2align 4
	.globl	uidna_labelToUnicodeUTF8_67
	.type	uidna_labelToUnicodeUTF8_67, @function
uidna_labelToUnicodeUTF8_67:
.LFB2419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	movq	%rdi, -120(%rbp)
	movl	(%r15), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L1086
	movq	%r9, %rbx
	testq	%r9, %r9
	je	.L1087
	movl	%edx, %r11d
	movswq	(%r9), %rdx
	cmpw	$15, %dx
	jle	.L1087
	movq	%rsi, %r13
	movq	%rcx, %r12
	movl	%r8d, %r14d
	testq	%rsi, %rsi
	je	.L1105
	cmpl	$-1, %r11d
	jl	.L1087
.L1090:
	testq	%r12, %r12
	je	.L1106
	testl	%r14d, %r14d
	js	.L1087
.L1092:
	cmpq	%r12, %r13
	jne	.L1097
	testq	%r13, %r13
	je	.L1097
	.p2align 4,,10
	.p2align 3
.L1087:
	movl	$1, (%r15)
	xorl	%eax, %eax
.L1086:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1107
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	.cfi_restore_state
	testl	%r14d, %r14d
	je	.L1092
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1097:
	subq	$2, %rdx
	leaq	2(%rbx), %rdi
	xorl	%esi, %esi
	movl	%r11d, -128(%rbp)
	call	memset@PLT
	movl	-128(%rbp), %r11d
	testl	%r11d, %r11d
	jns	.L1094
	movq	%r13, %rdi
	call	strlen@PLT
	movl	%eax, %r11d
.L1094:
	leaq	-96(%rbp), %r10
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r11, -136(%rbp)
	movq	%r10, %rdi
	movq	%r10, -128(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	movq	-120(%rbp), %rdi
	xorl	%eax, %eax
	movq	-128(%rbp), %r10
	movw	%ax, -100(%rbp)
	movq	%r15, %r9
	leaq	-108(%rbp), %r8
	movq	%r13, %rsi
	movq	-136(%rbp), %r11
	movq	(%rdi), %rax
	movq	%r10, %rcx
	movq	$0, -108(%rbp)
	movb	$1, -98(%rbp)
	movq	%r11, %rdx
	call	*64(%rax)
	movzbl	-100(%rbp), %eax
	movl	-72(%rbp), %edx
	movq	%r15, %rcx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movb	%al, 2(%rbx)
	movl	-108(%rbp), %eax
	movl	%eax, 4(%rbx)
	call	u_terminateChars_67@PLT
	movq	-128(%rbp), %r10
	movl	%eax, -120(%rbp)
	movq	%r10, %rdi
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-120(%rbp), %eax
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1105:
	testl	%r11d, %r11d
	je	.L1090
	jmp	.L1087
.L1107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2419:
	.size	uidna_labelToUnicodeUTF8_67, .-uidna_labelToUnicodeUTF8_67
	.p2align 4
	.globl	uidna_nameToASCII_UTF8_67
	.type	uidna_nameToASCII_UTF8_67, @function
uidna_nameToASCII_UTF8_67:
.LFB2420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	movq	%rdi, -120(%rbp)
	movl	(%r15), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L1109
	movq	%r9, %rbx
	testq	%r9, %r9
	je	.L1110
	movl	%edx, %r11d
	movswq	(%r9), %rdx
	cmpw	$15, %dx
	jle	.L1110
	movq	%rsi, %r13
	movq	%rcx, %r12
	movl	%r8d, %r14d
	testq	%rsi, %rsi
	je	.L1128
	cmpl	$-1, %r11d
	jl	.L1110
.L1113:
	testq	%r12, %r12
	je	.L1129
	testl	%r14d, %r14d
	js	.L1110
.L1115:
	cmpq	%r12, %r13
	jne	.L1120
	testq	%r13, %r13
	je	.L1120
	.p2align 4,,10
	.p2align 3
.L1110:
	movl	$1, (%r15)
	xorl	%eax, %eax
.L1109:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1130
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1129:
	.cfi_restore_state
	testl	%r14d, %r14d
	je	.L1115
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1120:
	subq	$2, %rdx
	leaq	2(%rbx), %rdi
	xorl	%esi, %esi
	movl	%r11d, -128(%rbp)
	call	memset@PLT
	movl	-128(%rbp), %r11d
	testl	%r11d, %r11d
	jns	.L1117
	movq	%r13, %rdi
	call	strlen@PLT
	movl	%eax, %r11d
.L1117:
	leaq	-96(%rbp), %r10
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r11, -136(%rbp)
	movq	%r10, %rdi
	movq	%r10, -128(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	movq	-120(%rbp), %rdi
	xorl	%eax, %eax
	movq	-128(%rbp), %r10
	movw	%ax, -100(%rbp)
	movq	%r15, %r9
	leaq	-108(%rbp), %r8
	movq	%r13, %rsi
	movq	-136(%rbp), %r11
	movq	(%rdi), %rax
	movq	%r10, %rcx
	movq	$0, -108(%rbp)
	movb	$1, -98(%rbp)
	movq	%r11, %rdx
	call	*72(%rax)
	movzbl	-100(%rbp), %eax
	movl	-72(%rbp), %edx
	movq	%r15, %rcx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movb	%al, 2(%rbx)
	movl	-108(%rbp), %eax
	movl	%eax, 4(%rbx)
	call	u_terminateChars_67@PLT
	movq	-128(%rbp), %r10
	movl	%eax, -120(%rbp)
	movq	%r10, %rdi
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-120(%rbp), %eax
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1128:
	testl	%r11d, %r11d
	je	.L1113
	jmp	.L1110
.L1130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2420:
	.size	uidna_nameToASCII_UTF8_67, .-uidna_nameToASCII_UTF8_67
	.p2align 4
	.globl	uidna_nameToUnicodeUTF8_67
	.type	uidna_nameToUnicodeUTF8_67, @function
uidna_nameToUnicodeUTF8_67:
.LFB2421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	movq	%rdi, -120(%rbp)
	movl	(%r15), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L1132
	movq	%r9, %rbx
	testq	%r9, %r9
	je	.L1133
	movl	%edx, %r11d
	movswq	(%r9), %rdx
	cmpw	$15, %dx
	jle	.L1133
	movq	%rsi, %r13
	movq	%rcx, %r12
	movl	%r8d, %r14d
	testq	%rsi, %rsi
	je	.L1151
	cmpl	$-1, %r11d
	jl	.L1133
.L1136:
	testq	%r12, %r12
	je	.L1152
	testl	%r14d, %r14d
	js	.L1133
.L1138:
	cmpq	%r12, %r13
	jne	.L1143
	testq	%r13, %r13
	je	.L1143
	.p2align 4,,10
	.p2align 3
.L1133:
	movl	$1, (%r15)
	xorl	%eax, %eax
.L1132:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1153
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1152:
	.cfi_restore_state
	testl	%r14d, %r14d
	je	.L1138
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1143:
	subq	$2, %rdx
	leaq	2(%rbx), %rdi
	xorl	%esi, %esi
	movl	%r11d, -128(%rbp)
	call	memset@PLT
	movl	-128(%rbp), %r11d
	testl	%r11d, %r11d
	jns	.L1140
	movq	%r13, %rdi
	call	strlen@PLT
	movl	%eax, %r11d
.L1140:
	leaq	-96(%rbp), %r10
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r11, -136(%rbp)
	movq	%r10, %rdi
	movq	%r10, -128(%rbp)
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	movq	-120(%rbp), %rdi
	xorl	%eax, %eax
	movq	-128(%rbp), %r10
	movw	%ax, -100(%rbp)
	movq	%r15, %r9
	leaq	-108(%rbp), %r8
	movq	%r13, %rsi
	movq	-136(%rbp), %r11
	movq	(%rdi), %rax
	movq	%r10, %rcx
	movq	$0, -108(%rbp)
	movb	$1, -98(%rbp)
	movq	%r11, %rdx
	call	*80(%rax)
	movzbl	-100(%rbp), %eax
	movl	-72(%rbp), %edx
	movq	%r15, %rcx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movb	%al, 2(%rbx)
	movl	-108(%rbp), %eax
	movl	%eax, 4(%rbx)
	call	u_terminateChars_67@PLT
	movq	-128(%rbp), %r10
	movl	%eax, -120(%rbp)
	movq	%r10, %rdi
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	movl	-120(%rbp), %eax
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1151:
	testl	%r11d, %r11d
	je	.L1136
	jmp	.L1133
.L1153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2421:
	.size	uidna_nameToUnicodeUTF8_67, .-uidna_nameToUnicodeUTF8_67
	.weak	_ZTSN6icu_674IDNAE
	.section	.rodata._ZTSN6icu_674IDNAE,"aG",@progbits,_ZTSN6icu_674IDNAE,comdat
	.align 8
	.type	_ZTSN6icu_674IDNAE, @object
	.size	_ZTSN6icu_674IDNAE, 15
_ZTSN6icu_674IDNAE:
	.string	"N6icu_674IDNAE"
	.weak	_ZTIN6icu_674IDNAE
	.section	.data.rel.ro._ZTIN6icu_674IDNAE,"awG",@progbits,_ZTIN6icu_674IDNAE,comdat
	.align 8
	.type	_ZTIN6icu_674IDNAE, @object
	.size	_ZTIN6icu_674IDNAE, 24
_ZTIN6icu_674IDNAE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_674IDNAE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_675UTS46E
	.section	.rodata._ZTSN6icu_675UTS46E,"aG",@progbits,_ZTSN6icu_675UTS46E,comdat
	.align 16
	.type	_ZTSN6icu_675UTS46E, @object
	.size	_ZTSN6icu_675UTS46E, 16
_ZTSN6icu_675UTS46E:
	.string	"N6icu_675UTS46E"
	.weak	_ZTIN6icu_675UTS46E
	.section	.data.rel.ro._ZTIN6icu_675UTS46E,"awG",@progbits,_ZTIN6icu_675UTS46E,comdat
	.align 8
	.type	_ZTIN6icu_675UTS46E, @object
	.size	_ZTIN6icu_675UTS46E, 24
_ZTIN6icu_675UTS46E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_675UTS46E
	.quad	_ZTIN6icu_674IDNAE
	.weak	_ZTVN6icu_674IDNAE
	.section	.data.rel.ro._ZTVN6icu_674IDNAE,"awG",@progbits,_ZTVN6icu_674IDNAE,comdat
	.align 8
	.type	_ZTVN6icu_674IDNAE, @object
	.size	_ZTVN6icu_674IDNAE, 104
_ZTVN6icu_674IDNAE:
	.quad	0
	.quad	_ZTIN6icu_674IDNAE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_674IDNA17labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.quad	_ZNK6icu_674IDNA18labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.quad	_ZNK6icu_674IDNA16nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.quad	_ZNK6icu_674IDNA17nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.weak	_ZTVN6icu_675UTS46E
	.section	.data.rel.ro._ZTVN6icu_675UTS46E,"awG",@progbits,_ZTVN6icu_675UTS46E,comdat
	.align 8
	.type	_ZTVN6icu_675UTS46E, @object
	.size	_ZTVN6icu_675UTS46E, 104
_ZTVN6icu_675UTS46E:
	.quad	0
	.quad	_ZTIN6icu_675UTS46E
	.quad	_ZN6icu_675UTS46D1Ev
	.quad	_ZN6icu_675UTS46D0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_675UTS4612labelToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.quad	_ZNK6icu_675UTS4614labelToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.quad	_ZNK6icu_675UTS4611nameToASCIIERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.quad	_ZNK6icu_675UTS4613nameToUnicodeERKNS_13UnicodeStringERS1_RNS_8IDNAInfoER10UErrorCode
	.quad	_ZNK6icu_675UTS4617labelToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.quad	_ZNK6icu_675UTS4618labelToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.quad	_ZNK6icu_675UTS4616nameToASCII_UTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.quad	_ZNK6icu_675UTS4617nameToUnicodeUTF8ENS_11StringPieceERNS_8ByteSinkERNS_8IDNAInfoER10UErrorCode
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L9asciiDataE, @object
	.size	_ZN6icu_67L9asciiDataE, 128
_ZN6icu_67L9asciiDataE:
	.string	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.string	""
	.string	"\377"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\377\377\377\377\377\377\377\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\377\377\377\377\377\377"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\377\377\377\377\377"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
