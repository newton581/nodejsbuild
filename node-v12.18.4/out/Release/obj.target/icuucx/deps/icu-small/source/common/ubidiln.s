	.file	"ubidiln.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	ubidi_getRuns_67.part.0, @function
ubidi_getRuns_67.part.0:
.LFB2699:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	cmpl	$2, 184(%rdi)
	movl	20(%rdi), %r13d
	je	.L2
	movzbl	141(%rdi), %eax
.L80:
	leaq	312(%rbx), %r9
	sall	$31, %eax
	movl	$1, 296(%rbx)
	movq	%r9, 304(%rbx)
	movl	%eax, 312(%rbx)
	movl	%r13d, 316(%rbx)
	movl	$0, 320(%rbx)
.L3:
	movslq	420(%rbx), %rax
	testl	%eax, %eax
	jle	.L36
	movq	432(%rbx), %rdi
	leaq	(%rdi,%rax,8), %r11
	cmpq	%r11, %rdi
	jnb	.L36
	.p2align 4,,10
	.p2align 3
.L39:
	movl	296(%rbx), %edx
	movl	(%rdi), %r8d
	testl	%edx, %edx
	jle	.L40
	subl	$1, %edx
	movq	%r9, %rax
	xorl	%esi, %esi
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%r9,%rdx,4), %r10
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L38:
	addq	$12, %rax
	cmpq	%rax, %r10
	je	.L82
.L41:
	movl	(%rax), %edx
	movl	%esi, %ecx
	movl	4(%rax), %esi
	andl	$2147483647, %edx
	cmpl	%edx, %r8d
	jl	.L38
	movl	%esi, %r14d
	subl	%ecx, %r14d
	addl	%r14d, %edx
	cmpl	%edx, %r8d
	jge	.L38
	movl	4(%rdi), %edx
	addq	$8, %rdi
	orl	%edx, 8(%rax)
	cmpq	%rdi, %r11
	ja	.L39
.L36:
	movl	440(%rbx), %eax
	testl	%eax, %eax
	jle	.L43
	movq	8(%rbx), %r10
	movslq	20(%rbx), %rax
	leaq	(%r10,%rax,2), %r11
	cmpq	%r11, %r10
	jnb	.L43
	movq	%r10, %r8
	.p2align 4,,10
	.p2align 3
.L48:
	movzwl	(%r8), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L44
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L44
	subl	$8294, %eax
	cmpl	$3, %eax
	ja	.L45
.L44:
	movq	%r8, %rsi
	movl	296(%rbx), %edx
	subq	%r10, %rsi
	sarq	%rsi
	testl	%edx, %edx
	jle	.L40
	subl	$1, %edx
	movq	%r9, %rax
	xorl	%edi, %edi
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%r9,%rdx,4), %r12
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$12, %rax
	cmpq	%r12, %rax
	je	.L83
.L47:
	movl	(%rax), %edx
	movl	%edi, %ecx
	movl	4(%rax), %edi
	andl	$2147483647, %edx
	cmpl	%edx, %esi
	jl	.L46
	movl	%edi, %r15d
	subl	%ecx, %r15d
	addl	%r15d, %edx
	cmpl	%edx, %esi
	jge	.L46
	subl	$1, 8(%rax)
.L45:
	addq	$2, %r8
	cmpq	%r8, %r11
	ja	.L48
.L43:
	movl	$1, %eax
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	120(%rdi), %rax
	movl	196(%rbx), %r12d
	movq	%rax, -64(%rbp)
	testl	%r12d, %r12d
	jle	.L52
	leal	-1(%r12), %edx
	xorl	%ecx, %ecx
	leaq	1(%rax,%rdx), %rsi
	movl	$-2, %edx
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%edx, %edi
	movzbl	(%rax), %edx
	cmpb	%dl, %dil
	setne	%dil
	addq	$1, %rax
	movzbl	%dil, %edi
	addl	%edi, %ecx
	cmpq	%rax, %rsi
	jne	.L6
	movl	%ecx, -56(%rbp)
	cmpl	$1, %ecx
	jne	.L4
	cmpl	%r13d, %r12d
	jne	.L4
	movq	-64(%rbp), %rax
	movzbl	(%rax), %eax
	jmp	.L80
.L52:
	movl	$0, -56(%rbp)
.L4:
	xorl	%eax, %eax
	cmpl	%r13d, %r12d
	movsbl	105(%rbx), %edx
	leaq	44(%rbx), %rsi
	setl	%al
	addl	%eax, -56(%rbp)
	movl	-56(%rbp), %edi
	leal	(%rdi,%rdi,2), %ecx
	leaq	88(%rbx), %rdi
	sall	$2, %ecx
	call	ubidi_getMemory_67@PLT
	testb	%al, %al
	je	.L1
	movq	88(%rbx), %r9
	movq	-64(%rbp), %r11
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	movl	$126, %r14d
	xorl	%esi, %esi
	movq	%r9, -88(%rbp)
	movq	%r9, %rdi
	.p2align 4,,10
	.p2align 3
.L51:
	movslq	%esi, %rax
	movzbl	(%r11,%rax), %edx
	leal	1(%rsi), %eax
	cltq
	cmpb	%dl, %r14b
	cmova	%edx, %r14d
	cmpb	%dl, %r10b
	cmovb	%edx, %r10d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$1, %rax
	cmpb	-1(%r11,%rax), %dl
	jne	.L84
.L10:
	movl	%eax, %ecx
	cmpl	%eax, %r12d
	jg	.L85
	movslq	%r8d, %rax
	subl	%esi, %ecx
	movl	%r14d, %edi
	leaq	(%rax,%rax,2), %rax
	salq	$2, %rax
	movq	%rax, -96(%rbp)
	addq	%r9, %rax
	movl	%esi, (%rax)
	movl	%ecx, 4(%rax)
	movl	$0, 8(%rax)
	leal	1(%r8), %eax
	movl	%eax, -72(%rbp)
	cmpl	%r13d, %r12d
	jge	.L50
	movq	-96(%rbp), %rax
	subl	%r12d, %r13d
	leaq	12(%r9,%rax), %rax
	movl	%r12d, (%rax)
	movl	%r13d, 4(%rax)
	movzbl	141(%rbx), %eax
	cmpb	%al, %dil
	cmova	%eax, %edi
.L50:
	movl	-56(%rbp), %eax
	movq	%r9, 304(%rbx)
	movl	%eax, 296(%rbx)
	movl	%edi, %eax
	orl	$1, %eax
	cmpb	%al, %r10b
	jbe	.L28
	movl	20(%rbx), %eax
	cmpl	%eax, 196(%rbx)
	movl	%edi, %r14d
	setl	%al
	movl	-56(%rbp), %r11d
	leal	1(%r14), %edi
	subl	$1, %r10d
	movzbl	%al, %eax
	movb	%dil, -65(%rbp)
	movq	120(%rbx), %r15
	subl	%eax, %r11d
	cmpb	%r10b, %dil
	ja	.L15
	testl	%r11d, %r11d
	jle	.L15
	movq	%rbx, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L17:
	movslq	%r8d, %rax
	leaq	(%rax,%rax,2), %rdx
	salq	$2, %rdx
	leaq	(%r9,%rdx), %rax
	movslq	(%rax), %rcx
	cmpb	%r10b, (%r15,%rcx)
	jb	.L19
	leaq	12(%r9,%rdx), %rdx
	movl	%r8d, %ecx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L87:
	movslq	(%rdx), %rsi
	addq	$12, %rdx
	cmpb	%r10b, (%r15,%rsi)
	jb	.L86
.L21:
	movl	%ecx, %edi
	addl	$1, %ecx
	cmpl	%ecx, %r11d
	jg	.L87
	cmpl	%r8d, %edi
	jle	.L25
.L23:
	movslq	%edi, %rdx
	movl	%ecx, -52(%rbp)
	movl	%edi, %esi
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r9,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rdx), %rcx
	movl	8(%rax), %ebx
	subl	$1, %esi
	addq	$12, %rax
	movl	-12(%rax), %r13d
	movl	-8(%rax), %r12d
	subq	$12, %rdx
	movq	%rcx, -12(%rax)
	movl	20(%rdx), %ecx
	movl	%ecx, -4(%rax)
	movl	%ebx, 20(%rdx)
	movl	%r8d, %ebx
	subl	%esi, %ebx
	movl	%r13d, 12(%rdx)
	addl	%edi, %ebx
	movl	%r12d, 16(%rdx)
	cmpl	%ebx, %esi
	jg	.L26
	movl	-52(%rbp), %ecx
.L25:
	cmpl	%ecx, %r11d
	je	.L18
.L24:
	leal	2(%rdi), %r8d
.L27:
	cmpl	%r8d, %r11d
	jg	.L17
.L18:
	subl	$1, %r10d
	cmpb	%r10b, %r14b
	jne	.L16
	movq	-80(%rbp), %rbx
.L15:
	testb	$1, -65(%rbp)
	jne	.L28
	movl	20(%rbx), %eax
	cmpl	%eax, 196(%rbx)
	sete	%al
	movzbl	%al, %eax
	subl	%eax, %r11d
	testl	%r11d, %r11d
	jle	.L28
	movslq	%r11d, %rax
	xorl	%ecx, %ecx
	leaq	(%rax,%rax,2), %rax
	leaq	(%r9,%rax,4), %rdx
	movq	%r9, %rax
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%rdx), %r10
	movl	8(%rax), %esi
	addl	$1, %ecx
	addq	$12, %rax
	movl	-12(%rax), %r8d
	movl	-8(%rax), %edi
	subq	$12, %rdx
	movq	%r10, -12(%rax)
	movl	20(%rdx), %r10d
	movl	%r10d, -4(%rax)
	movl	%esi, 20(%rdx)
	movl	%r11d, %esi
	subl	%ecx, %esi
	movl	%r8d, 12(%rdx)
	movl	%edi, 16(%rdx)
	cmpl	%esi, %ecx
	jl	.L32
	.p2align 4,,10
	.p2align 3
.L28:
	movl	-56(%rbp), %edx
	testl	%edx, %edx
	je	.L3
	movl	-56(%rbp), %eax
	movq	-64(%rbp), %r8
	xorl	%edx, %edx
	subl	$1, %eax
	leaq	(%rax,%rax,2), %rax
	leaq	12(%r9,%rax,4), %rsi
	movq	-88(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L34:
	movslq	(%rax), %rcx
	addl	4(%rax), %edx
	addq	$12, %rax
	movq	%rcx, %rdi
	movzbl	(%r8,%rcx), %ecx
	movl	%edx, -8(%rax)
	sall	$31, %ecx
	orl	%edi, %ecx
	movl	%ecx, -12(%rax)
	cmpq	%rax, %rsi
	jne	.L34
	movl	-72(%rbp), %edi
	cmpl	%edi, -56(%rbp)
	jle	.L3
	movzbl	141(%rbx), %edx
	movq	-96(%rbp), %rax
	testb	$1, %dl
	leaq	12(%r9,%rax), %rax
	cmovne	%r9, %rax
	sall	$31, %edx
	orl	%edx, (%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L84:
	movl	%ecx, %eax
	movl	%esi, (%rdi)
	addl	$1, %r8d
	addq	$12, %rdi
	subl	%esi, %eax
	movl	$0, -4(%rdi)
	movl	%ecx, %esi
	movl	%eax, -8(%rdi)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L86:
	cmpl	%r8d, %edi
	jg	.L23
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L19:
	addl	$1, %r8d
	jmp	.L27
.L83:
	jmp	.L40
.L82:
	jmp	.L40
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	ubidi_getRuns_67.part.0.cold, @function
ubidi_getRuns_67.part.0.cold:
.LFSB2699:
.L40:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE2699:
	.text
	.size	ubidi_getRuns_67.part.0, .-ubidi_getRuns_67.part.0
	.section	.text.unlikely
	.size	ubidi_getRuns_67.part.0.cold, .-ubidi_getRuns_67.part.0.cold
.LCOLDE0:
	.text
.LHOTE0:
	.p2align 4
	.globl	ubidi_setLine_67
	.type	ubidi_setLine_67, @function
ubidi_setLine_67:
.LFB2095:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L142
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 3, -48
	movq	%r8, %rbx
	subq	$16, %rsp
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L88
	testq	%rdi, %rdi
	je	.L90
	cmpq	%rdi, (%rdi)
	je	.L91
.L90:
	movl	$27, (%rbx)
.L88:
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	%esi, %r13d
	testl	%esi, %esi
	js	.L94
	movl	%edx, %r14d
	cmpl	%edx, %esi
	jge	.L94
	cmpl	%edx, 20(%rdi)
	jl	.L94
	testq	%rcx, %rcx
	movq	%rcx, -48(%rbp)
	je	.L94
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %r9
	movq	%rdi, -40(%rbp)
	call	ubidi_getParagraph_67@PLT
	movq	-40(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leal	-1(%r14), %esi
	movq	%rbx, %r9
	movl	%eax, %r15d
	call	ubidi_getParagraph_67@PLT
	cmpl	%eax, %r15d
	jne	.L94
	movq	-40(%rbp), %rdi
	movq	-48(%rbp), %r10
	movslq	%r13d, %r15
	movl	%r14d, %ebx
	subl	%r13d, %ebx
	movq	8(%rdi), %rax
	movq	$0, (%r10)
	movl	%ebx, 20(%r10)
	leaq	(%rax,%r15,2), %rax
	movl	%ebx, 16(%r10)
	movq	%rax, 8(%r10)
	movl	%ebx, 24(%r10)
	cmpb	$0, 142(%rdi)
	je	.L95
	movq	208(%rdi), %rax
	cmpl	%r13d, (%rax)
	jle	.L96
.L95:
	movzbl	141(%rdi), %r9d
.L97:
	movb	%r9b, 141(%r10)
	movl	200(%rdi), %eax
	movq	$0, 304(%r10)
	movl	%eax, 200(%r10)
	movl	132(%rdi), %eax
	movl	$0, 188(%r10)
	movl	%eax, 132(%r10)
	movl	136(%rdi), %eax
	movl	$0, 440(%r10)
	movl	%eax, 136(%r10)
	movl	440(%rdi), %eax
	testl	%eax, %eax
	jle	.L98
	movq	8(%rdi), %r8
	movq	%r15, %rax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L101:
	movzwl	(%r8,%rax,2), %edx
	movl	%edx, %esi
	andl	$-4, %esi
	cmpl	$8204, %esi
	je	.L99
	leal	-8234(%rdx), %esi
	cmpl	$4, %esi
	jbe	.L99
	subl	$8294, %edx
	cmpl	$3, %edx
	ja	.L100
	.p2align 4,,10
	.p2align 3
.L99:
	addl	$1, %ecx
	movl	%ecx, 440(%r10)
.L100:
	addq	$1, %rax
	cmpl	%eax, %r14d
	jg	.L101
	subl	%ecx, 24(%r10)
.L98:
	movq	112(%rdi), %rdx
	movl	184(%rdi), %eax
	addq	%r15, %rdx
	addq	120(%rdi), %r15
	movl	$-1, 296(%r10)
	movq	%rdx, 112(%r10)
	movq	%r15, 120(%r10)
	cmpl	$2, %eax
	je	.L102
	movl	%eax, 184(%r10)
	movl	196(%rdi), %eax
	cmpl	%r13d, %eax
	jle	.L146
	movl	%eax, %edx
	subl	%r13d, %edx
	cmpl	%r14d, %eax
	cmovl	%edx, %ebx
	movl	%ebx, 196(%r10)
.L104:
	movq	%rdi, (%r10)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$1, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L121:
	.cfi_restore_state
	movl	%r9d, %eax
	orl	$1, %eax
	movb	%al, 141(%r10)
.L146:
	movl	$0, 196(%r10)
	jmp	.L104
.L102:
	movslq	20(%r10), %rcx
	leaq	-1(%rdx,%rcx), %rdx
	movq	%rcx, %rax
	cmpb	$7, (%rdx)
	je	.L108
	testl	%ecx, %ecx
	jle	.L108
	movl	$8248192, %esi
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L147:
	subq	$1, %rdx
	subl	$1, %eax
	je	.L145
.L110:
	movzbl	(%rdx), %ecx
	btq	%rcx, %rsi
	jc	.L147
	movslq	%eax, %rdx
	leaq	-1(%r15,%rdx), %rdx
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L148:
	subq	$1, %rdx
	subl	$1, %eax
	je	.L145
.L112:
	cmpb	(%rdx), %r9b
	je	.L148
.L108:
	movl	%eax, 196(%r10)
	testl	%eax, %eax
	je	.L111
	movzbl	(%r15), %ecx
	andl	$1, %ecx
	cmpl	%eax, %ebx
	jle	.L118
	movl	%r9d, %edx
	andl	$1, %edx
	cmpb	%cl, %dl
	je	.L118
.L119:
	movl	$2, 184(%r10)
	jmp	.L104
.L96:
	movl	%r13d, %esi
	movq	%r10, -48(%rbp)
	movq	%rdi, -40(%rbp)
	call	ubidi_getParaLevelAtIndex_67@PLT
	movq	-48(%rbp), %r10
	movq	-40(%rbp), %rdi
	movl	%eax, %r9d
	jmp	.L97
.L145:
	movl	$0, 196(%r10)
.L111:
	movl	%r9d, %ecx
	andl	$1, %ecx
	movl	%ecx, 184(%r10)
.L114:
	testl	%ecx, %ecx
	jne	.L121
	movl	$0, 196(%r10)
	leal	1(%r9), %eax
	andl	$-2, %eax
	movb	%al, 141(%r10)
	jmp	.L104
.L118:
	cmpl	$1, %eax
	je	.L116
	subl	$2, %eax
	leaq	1(%r15), %rdx
	leaq	2(%r15,%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L120:
	movzbl	(%rdx), %eax
	andl	$1, %eax
	cmpb	%cl, %al
	jne	.L119
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L120
.L116:
	movzbl	%cl, %ecx
	movl	%ecx, 184(%r10)
	jmp	.L114
	.cfi_endproc
.LFE2095:
	.size	ubidi_setLine_67, .-ubidi_setLine_67
	.p2align 4
	.globl	ubidi_getLevelAt_67
	.type	ubidi_getLevelAt_67, @function
ubidi_getLevelAt_67:
.LFB2096:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L160
	movq	(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L151
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L149
	cmpq	(%rax), %rax
	jne	.L149
.L151:
	testl	%esi, %esi
	js	.L160
	cmpl	%esi, 20(%rdi)
	jle	.L160
	cmpl	$2, 184(%rdi)
	je	.L161
.L152:
	cmpb	$0, 142(%rdi)
	je	.L154
	movq	208(%rdi), %rax
	cmpl	%esi, (%rax)
	jle	.L155
.L154:
	movzbl	141(%rdi), %r8d
.L149:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	cmpl	%esi, 196(%rdi)
	jle	.L152
	movq	120(%rdi), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %r8d
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L155:
	jmp	ubidi_getParaLevelAtIndex_67@PLT
	.cfi_endproc
.LFE2096:
	.size	ubidi_getLevelAt_67, .-ubidi_getLevelAt_67
	.p2align 4
	.globl	ubidi_getLevels_67
	.type	ubidi_getLevels_67, @function
ubidi_getLevels_67:
.LFB2097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L170
	movl	(%rsi), %eax
	movq	%rsi, %r12
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jg	.L162
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L164
	movq	(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L165
	testq	%rax, %rax
	je	.L164
	cmpq	(%rax), %rax
	jne	.L164
.L165:
	movl	20(%rbx), %r14d
	testl	%r14d, %r14d
	jle	.L179
	movl	196(%rbx), %r15d
	cmpl	%r15d, %r14d
	je	.L180
	movsbl	104(%rbx), %edx
	leaq	32(%rbx), %rsi
	leaq	64(%rbx), %rdi
	movl	%r14d, %ecx
	call	ubidi_getMemory_67@PLT
	testb	%al, %al
	je	.L168
	movq	64(%rbx), %r13
	movslq	%r15d, %r12
	testl	%r15d, %r15d
	jle	.L169
	movq	120(%rbx), %rsi
	cmpq	%r13, %rsi
	je	.L169
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	memcpy@PLT
.L169:
	movl	%r14d, %edx
	movzbl	141(%rbx), %esi
	leaq	0(%r13,%r12), %rdi
	subl	%r15d, %edx
	movslq	%edx, %rdx
	call	memset@PLT
	movl	%r14d, 196(%rbx)
	movq	%r13, 120(%rbx)
.L162:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movl	$27, (%r12)
	xorl	%r13d, %r13d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L180:
	movq	120(%rbx), %r13
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L170:
	xorl	%r13d, %r13d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L179:
	movl	$1, (%r12)
	xorl	%r13d, %r13d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$7, (%r12)
	xorl	%r13d, %r13d
	jmp	.L162
	.cfi_endproc
.LFE2097:
	.size	ubidi_getLevels_67, .-ubidi_getLevels_67
	.p2align 4
	.globl	ubidi_getLogicalRun_67
	.type	ubidi_getLogicalRun_67, @function
ubidi_getLogicalRun_67:
.LFB2098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movslq	%esi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	testl	%r12d, %r12d
	js	.L181
	movq	%rdi, %r13
	cmpl	%r12d, 20(%rdi)
	jg	.L219
.L181:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r14
	movq	%rcx, %rbx
	cmpq	%rax, %rdi
	je	.L185
	testq	%rax, %rax
	je	.L181
	cmpq	(%rax), %rax
	jne	.L181
.L185:
	movl	296(%r13), %esi
	testl	%esi, %esi
	jns	.L186
	movq	%r13, %rdi
	call	ubidi_getRuns_67.part.0
	movl	296(%r13), %esi
.L186:
	movq	304(%r13), %rax
	movl	(%rax), %r10d
	testl	%esi, %esi
	jle	.L199
	movl	4(%rax), %r11d
	movl	%r10d, %edi
	andl	$2147483647, %edi
	leal	(%rdi,%r11), %r9d
	cmpl	%r9d, %r12d
	jge	.L200
	cmpl	%edi, %r12d
	jge	.L187
.L200:
	subl	$1, %esi
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%rax,%rsi,4), %rdi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L191:
	movl	12(%rax), %r10d
	movl	16(%rax), %r8d
	addq	$12, %rax
	movl	%r10d, %esi
	andl	$2147483647, %esi
	leal	(%rsi,%r8), %r9d
	subl	%r11d, %r9d
	cmpl	%esi, %r12d
	jl	.L201
	cmpl	%r9d, %r12d
	jl	.L187
.L201:
	movl	%r8d, %r11d
.L189:
	cmpq	%rax, %rdi
	jne	.L191
.L187:
	testq	%r14, %r14
	je	.L192
	movl	%r9d, (%r14)
.L192:
	testq	%rbx, %rbx
	je	.L181
	cmpl	$3, 132(%r13)
	je	.L220
	cmpl	$2, 184(%r13)
	je	.L221
.L194:
	cmpb	$0, 142(%r13)
	je	.L196
	movq	208(%r13), %rax
	cmpl	%r12d, (%rax)
	jg	.L196
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	ubidi_getParaLevelAtIndex_67@PLT
	movb	%al, (%rbx)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L196:
	movzbl	141(%r13), %eax
	movb	%al, (%rbx)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L220:
	movl	%r10d, %eax
	shrl	$31, %eax
	movb	%al, (%rbx)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L221:
	cmpl	%r12d, 196(%r13)
	jle	.L194
	movq	120(%r13), %rax
	movzbl	(%rax,%r12), %eax
	movb	%al, (%rbx)
	jmp	.L181
.L199:
	xorl	%r9d, %r9d
	jmp	.L187
	.cfi_endproc
.LFE2098:
	.size	ubidi_getLogicalRun_67, .-ubidi_getLogicalRun_67
	.p2align 4
	.globl	ubidi_countRuns_67
	.type	ubidi_countRuns_67, @function
ubidi_countRuns_67:
.LFB2099:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L239
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rsi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testl	%edx, %edx
	jg	.L238
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L226
	movq	(%rdi), %rax
	cmpq	%rdi, %rax
	je	.L227
	testq	%rax, %rax
	je	.L226
	cmpq	(%rax), %rax
	jne	.L226
.L227:
	movl	296(%rbx), %eax
	testl	%eax, %eax
	jns	.L222
	movq	%rbx, %rdi
	call	ubidi_getRuns_67.part.0
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L238
	movl	296(%rbx), %eax
.L222:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movl	$27, (%r12)
.L238:
	movl	$-1, %eax
	jmp	.L222
.L239:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE2099:
	.size	ubidi_countRuns_67, .-ubidi_countRuns_67
	.p2align 4
	.globl	ubidi_getVisualRun_67
	.type	ubidi_getVisualRun_67, @function
ubidi_getVisualRun_67:
.LFB2100:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L261
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	cmpq	%rdi, %rax
	je	.L243
	testq	%rax, %rax
	je	.L244
	cmpq	(%rax), %rax
	jne	.L244
.L243:
	movl	296(%rbx), %eax
	testl	%eax, %eax
	jns	.L245
	movq	%rbx, %rdi
	call	ubidi_getRuns_67.part.0
.L245:
	testl	%r12d, %r12d
	js	.L244
	cmpl	%r12d, 296(%rbx)
	jle	.L244
	movslq	%r12d, %rax
	movq	304(%rbx), %rcx
	leaq	(%rax,%rax,2), %rdx
	salq	$2, %rdx
	leaq	(%rcx,%rdx), %rsi
	movl	(%rsi), %eax
	testq	%r14, %r14
	je	.L246
	movl	%eax, %edi
	andl	$2147483647, %edi
	movl	%edi, (%r14)
.L246:
	testq	%r13, %r13
	je	.L247
	testl	%r12d, %r12d
	je	.L248
	movl	4(%rsi), %esi
	subl	-8(%rcx,%rdx), %esi
	movl	%esi, 0(%r13)
.L247:
	popq	%rbx
	shrl	$31, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	4(%rcx), %edx
	movl	%edx, 0(%r13)
	jmp	.L247
	.cfi_endproc
.LFE2100:
	.size	ubidi_getVisualRun_67, .-ubidi_getVisualRun_67
	.p2align 4
	.globl	ubidi_getRuns_67
	.type	ubidi_getRuns_67, @function
ubidi_getRuns_67:
.LFB2104:
	.cfi_startproc
	endbr64
	movl	296(%rdi), %eax
	testl	%eax, %eax
	jns	.L263
	jmp	ubidi_getRuns_67.part.0
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2104:
	.size	ubidi_getRuns_67, .-ubidi_getRuns_67
	.p2align 4
	.globl	ubidi_reorderLogical_67
	.type	ubidi_reorderLogical_67, @function
ubidi_reorderLogical_67:
.LFB2106:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L299
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L264
	movl	%esi, %r8d
	testl	%esi, %esi
	jle	.L264
	movslq	%esi, %rax
	movq	%rdx, %r11
	movl	$126, %r12d
	leaq	-1(%rax), %rdi
	leaq	-2(%rbx,%rax), %rcx
	leal	-1(%rsi), %eax
	leaq	(%rbx,%rdi), %rdx
	xorl	%esi, %esi
	subq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L267:
	movzbl	(%rdx), %eax
	cmpb	$126, %al
	ja	.L264
	cmpb	%al, %r12b
	cmova	%eax, %r12d
	cmpb	%al, %sil
	cmovb	%eax, %esi
	subq	$1, %rdx
	cmpq	%rcx, %rdx
	jne	.L267
	cmpl	$3, %r8d
	jle	.L283
	movl	%r8d, %edx
	leaq	-12(%r11,%rdi,4), %rax
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LC2(%rip), %xmm3
	shrl	$2, %edx
	movd	%r8d, %xmm6
	movq	%rax, %rdi
	salq	$4, %rdx
	pshufd	$0, %xmm6, %xmm1
	paddd	.LC1(%rip), %xmm1
	subq	%rdx, %rdi
	movq	%rdi, %rdx
.L269:
	movdqa	%xmm1, %xmm0
	subq	$16, %rax
	paddd	%xmm3, %xmm1
	paddd	%xmm2, %xmm0
	pshufd	$27, %xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	cmpq	%rdx, %rax
	jne	.L269
	movl	%r8d, %eax
	andl	$3, %eax
	testb	$3, %r8b
	je	.L270
.L268:
	leal	-1(%rax), %edx
	movslq	%edx, %rdi
	movl	%edx, (%r11,%rdi,4)
	leaq	0(,%rdi,4), %rcx
	testl	%edx, %edx
	je	.L270
	leal	-2(%rax), %edx
	movl	%edx, -4(%r11,%rcx)
	testl	%edx, %edx
	je	.L270
	subl	$3, %eax
	movl	%eax, -8(%r11,%rcx)
.L270:
	cmpb	%sil, %r12b
	je	.L302
.L282:
	orl	$1, %r12d
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L280:
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L271:
	movslq	%edi, %r9
	cmpb	%sil, (%rbx,%r9)
	jb	.L273
	leaq	1(%rbx,%r9), %rdx
	movl	%edi, %eax
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L303:
	addq	$1, %rdx
	cmpb	%sil, -1(%rdx)
	jb	.L274
.L275:
	movl	%eax, %ecx
	addl	$1, %eax
	cmpl	%eax, %r8d
	jg	.L303
.L274:
	movl	%ecx, %edx
	leal	-1(%rax,%rdi), %r14d
	subl	%edi, %edx
	cmpl	%edi, %ecx
	leal	1(%rdx), %r10d
	cmovl	%r13d, %r10d
	cmpl	$2, %edx
	jbe	.L276
	cmpl	%edi, %ecx
	jl	.L276
	leaq	(%r11,%r9,4), %rdx
	movl	%r10d, %r9d
	movd	%r14d, %xmm5
	shrl	$2, %r9d
	pshufd	$0, %xmm5, %xmm1
	salq	$4, %r9
	addq	%rdx, %r9
	.p2align 4,,10
	.p2align 3
.L277:
	movdqu	(%rdx), %xmm4
	movdqa	%xmm1, %xmm0
	addq	$16, %rdx
	psubd	%xmm4, %xmm0
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %r9
	jne	.L277
	movl	%r10d, %edx
	andl	$-4, %edx
	addl	%edx, %edi
	cmpl	%r10d, %edx
	je	.L278
	movslq	%edi, %r9
.L276:
	salq	$2, %r9
	movl	%r14d, %r10d
	leaq	(%r11,%r9), %rdx
	subl	(%rdx), %r10d
	movl	%r10d, (%rdx)
	leal	1(%rdi), %r10d
	cmpl	%edi, %ecx
	jle	.L278
	leaq	4(%r11,%r9), %rdx
	movl	%r14d, %edi
	subl	(%rdx), %edi
	movl	%edi, (%rdx)
	cmpl	%ecx, %r10d
	jge	.L278
	leaq	8(%r11,%r9), %rdx
	subl	(%rdx), %r14d
	movl	%r14d, (%rdx)
.L278:
	cmpl	%eax, %r8d
	je	.L272
	leal	2(%rcx), %edi
.L279:
	cmpl	%edi, %r8d
	jg	.L271
.L272:
	subl	$1, %esi
	cmpb	%sil, %r12b
	jbe	.L280
.L264:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	addl	$1, %edi
	jmp	.L279
.L299:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
.L302:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	testb	$1, %r12b
	jne	.L282
	jmp	.L264
.L283:
	movl	%r8d, %eax
	jmp	.L268
	.cfi_endproc
.LFE2106:
	.size	ubidi_reorderLogical_67, .-ubidi_reorderLogical_67
	.p2align 4
	.globl	ubidi_reorderVisual_67
	.type	ubidi_reorderVisual_67, @function
ubidi_reorderVisual_67:
.LFB2107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	testq	%rdx, %rdx
	je	.L304
	testq	%rdi, %rdi
	je	.L304
	movl	%esi, %r15d
	testl	%esi, %esi
	jle	.L304
	movslq	%esi, %rax
	movq	%rdx, %r8
	movl	$126, %ecx
	leaq	-2(%rdi,%rax), %rsi
	leaq	-1(%rax), %r9
	leal	-1(%r15), %eax
	leaq	(%rdi,%r9), %rdx
	xorl	%edi, %edi
	subq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L307:
	movzbl	(%rdx), %eax
	cmpb	$126, %al
	ja	.L304
	cmpb	%al, %cl
	cmova	%eax, %ecx
	cmpb	%al, %dil
	cmovb	%eax, %edi
	subq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L307
	cmpl	$3, %r15d
	jle	.L329
	movl	%r15d, %edx
	leaq	-12(%r8,%r9,4), %rax
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LC2(%rip), %xmm3
	shrl	$2, %edx
	movd	%r15d, %xmm5
	movq	%rax, %rbx
	salq	$4, %rdx
	pshufd	$0, %xmm5, %xmm1
	paddd	.LC1(%rip), %xmm1
	subq	%rdx, %rbx
	movq	%rbx, %rdx
.L309:
	movdqa	%xmm1, %xmm0
	subq	$16, %rax
	paddd	%xmm3, %xmm1
	paddd	%xmm2, %xmm0
	pshufd	$27, %xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	cmpq	%rax, %rdx
	jne	.L309
	movl	%r15d, %eax
	andl	$3, %eax
	testb	$3, %r15b
	je	.L310
.L308:
	leal	-1(%rax), %edx
	movslq	%edx, %r9
	movl	%edx, (%r8,%r9,4)
	leaq	0(,%r9,4), %rsi
	testl	%edx, %edx
	je	.L310
	leal	-2(%rax), %edx
	movl	%edx, -4(%r8,%rsi)
	testl	%edx, %edx
	je	.L310
	subl	$3, %eax
	movl	%eax, -8(%r8,%rsi)
.L310:
	cmpb	%dil, %cl
	je	.L355
.L328:
	orl	$1, %ecx
	movb	%cl, -53(%rbp)
	.p2align 4,,10
	.p2align 3
.L326:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L311:
	movq	-72(%rbp), %rax
	movslq	%esi, %r10
	cmpb	%dil, (%rax,%r10)
	jb	.L313
	leaq	1(%r10), %r11
	leaq	(%rax,%r11), %rdx
	movl	%esi, %eax
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L357:
	addq	$1, %rdx
	cmpb	%dil, -1(%rdx)
	jb	.L356
.L315:
	movl	%eax, %ecx
	addl	$1, %eax
	cmpl	%eax, %r15d
	jg	.L357
	movq	%r11, -64(%rbp)
	cmpl	%esi, %ecx
	jle	.L322
.L317:
	movl	%esi, %r13d
	movl	$4, %r11d
	leaq	0(,%r10,4), %rdx
	notl	%r13d
	leal	0(%r13,%rcx), %ebx
	movl	%ebx, %r9d
	movl	%ebx, -52(%rbp)
	shrl	%r9d
	cmpl	%esi, %ecx
	leal	1(%r9), %ebx
	movslq	%ecx, %r9
	leaq	0(,%rbx,4), %r12
	movq	%rbx, %r13
	leaq	0(,%r9,4), %r14
	cmovle	%r11, %r12
	negq	%rbx
	movl	$0, %r11d
	leaq	4(,%rbx,4), %rbx
	addq	%rdx, %r12
	cmpl	%esi, %ecx
	cmovle	%r11, %rbx
	addq	%r14, %rbx
	cmpq	%rbx, %r12
	leaq	4(%r14), %rbx
	setle	%r12b
	cmpq	%rdx, %rbx
	setle	%bl
	orb	%bl, %r12b
	je	.L351
	cmpl	$5, -52(%rbp)
	seta	%r12b
	cmpl	%esi, %ecx
	setg	%bl
	testb	%bl, %r12b
	je	.L351
	cmpl	%esi, %ecx
	movl	$1, %r11d
	leaq	-12(%r8,%r14), %r9
	cmovg	%r13d, %r11d
	addq	%r8, %rdx
	movl	%r11d, %r10d
	shrl	$2, %r10d
	salq	$4, %r10
	addq	%rdx, %r10
	.p2align 4,,10
	.p2align 3
.L321:
	movdqu	(%r9), %xmm4
	movdqu	(%rdx), %xmm0
	addq	$16, %rdx
	subq	$16, %r9
	pshufd	$27, %xmm4, %xmm1
	pshufd	$27, %xmm0, %xmm0
	movups	%xmm1, -16(%rdx)
	movups	%xmm0, 16(%r9)
	cmpq	%r10, %rdx
	jne	.L321
	movl	%r11d, %r9d
	movl	%ecx, %edx
	andl	$-4, %r9d
	addl	%r9d, %esi
	subl	%r9d, %edx
	cmpl	%r9d, %r11d
	je	.L322
	movslq	%esi, %r9
	leaq	(%r8,%r9,4), %r10
	movslq	%edx, %r9
	leaq	(%r8,%r9,4), %r9
	movl	(%r10), %r11d
	movl	(%r9), %ebx
	movl	%ebx, (%r10)
	leal	1(%rsi), %r10d
	movl	%r11d, (%r9)
	leal	-1(%rdx), %r9d
	cmpl	%r9d, %r10d
	jge	.L322
	movslq	%r10d, %r10
	movslq	%r9d, %r9
	addl	$2, %esi
	subl	$2, %edx
	salq	$2, %r10
	salq	$2, %r9
	leaq	(%r8,%r10), %r11
	leaq	(%r8,%r9), %r12
	movl	(%r11), %ebx
	movl	(%r12), %r13d
	movl	%r13d, (%r11)
	movl	%ebx, (%r12)
	cmpl	%edx, %esi
	jge	.L322
	leaq	4(%r8,%r10), %rsi
	leaq	-4(%r8,%r9), %rdx
	movl	(%rsi), %r10d
	movl	(%rdx), %r9d
	movl	%r9d, (%rsi)
	movl	%r10d, (%rdx)
.L322:
	cmpl	%eax, %r15d
	je	.L312
.L318:
	leal	2(%rcx), %esi
.L325:
	cmpl	%esi, %r15d
	jg	.L311
.L312:
	subl	$1, %edi
	cmpb	%dil, -53(%rbp)
	jbe	.L326
.L304:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movq	%r11, -64(%rbp)
	cmpl	%esi, %ecx
	jg	.L317
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L313:
	addl	$1, %esi
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L351:
	movq	-64(%rbp), %r11
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L358:
	addq	$1, %r11
.L342:
	movl	(%r8,%r10,4), %edx
	movl	(%r8,%r9,4), %esi
	movl	%esi, (%r8,%r10,4)
	movq	%r11, %r10
	movl	%edx, (%r8,%r9,4)
	subq	$1, %r9
	cmpl	%r11d, %r9d
	jg	.L358
	jmp	.L322
.L355:
	testb	$1, %cl
	jne	.L328
	jmp	.L304
.L329:
	movl	%r15d, %eax
	jmp	.L308
	.cfi_endproc
.LFE2107:
	.size	ubidi_reorderVisual_67, .-ubidi_reorderVisual_67
	.p2align 4
	.globl	ubidi_getVisualIndex_67
	.type	ubidi_getVisualIndex_67, @function
ubidi_getVisualIndex_67:
.LFB2108:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L457
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L456
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L363
	movq	(%rdi), %rax
	movl	%esi, %r12d
	cmpq	%rdi, %rax
	je	.L364
	testq	%rax, %rax
	je	.L363
	cmpq	(%rax), %rax
	jne	.L363
.L364:
	testl	%r12d, %r12d
	js	.L365
	movl	20(%rbx), %eax
	cmpl	%r12d, %eax
	jle	.L365
	movl	184(%rbx), %edx
	testl	%edx, %edx
	je	.L408
	cmpl	$1, %edx
	jne	.L368
	subl	%r12d, %eax
	subl	$1, %eax
.L367:
	movl	420(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L458
	movl	440(%rbx), %edx
	testl	%edx, %edx
	jle	.L359
	movq	8(%rbx), %r8
	movslq	%r12d, %rdx
	movq	304(%rbx), %rsi
	movzwl	(%r8,%rdx,2), %edx
	movl	%edx, %ecx
	andl	$-4, %ecx
	cmpl	$8204, %ecx
	je	.L456
	leal	-8234(%rdx), %ecx
	cmpl	$4, %ecx
	jbe	.L456
	subl	$8294, %edx
	cmpl	$3, %edx
	jbe	.L456
	movl	4(%rsi), %edx
	movl	8(%rsi), %edi
	xorl	%ecx, %ecx
	leaq	12(%rsi), %r9
	cmpl	%eax, %edx
	jle	.L384
.L383:
	testl	%edi, %edi
	je	.L387
	movl	(%rsi), %esi
	testl	%esi, %esi
	jns	.L386
	leal	1(%r12), %edi
	andl	$2147483647, %esi
	leal	(%rsi,%rdx), %r12d
	movl	%edi, %esi
.L386:
	cmpl	%r12d, %esi
	jge	.L387
	leal	-1(%r12), %edx
	movl	%r12d, %r9d
	subl	%esi, %edx
	subl	%esi, %r9d
	cmpl	$6, %edx
	jbe	.L388
	movl	%r9d, %edi
	pxor	%xmm5, %xmm5
	pxor	%xmm11, %xmm11
	movslq	%esi, %rdx
	movdqa	.LC7(%rip), %xmm8
	shrl	$3, %edi
	leaq	(%r8,%rdx,2), %rdx
	movdqa	%xmm5, %xmm4
	movdqa	.LC6(%rip), %xmm0
	salq	$4, %rdi
	movdqa	.LC2(%rip), %xmm15
	movdqa	.LC3(%rip), %xmm14
	addq	%rdx, %rdi
	movdqa	.LC4(%rip), %xmm13
	movdqa	.LC5(%rip), %xmm12
	psubd	%xmm8, %xmm0
	movaps	%xmm0, -48(%rbp)
	movdqa	.LC8(%rip), %xmm0
	psubd	%xmm8, %xmm0
	movaps	%xmm0, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L390:
	movdqu	(%rdx), %xmm0
	movdqa	%xmm15, %xmm9
	movdqa	%xmm14, %xmm7
	movdqa	%xmm15, %xmm1
	movdqa	%xmm14, %xmm3
	addq	$16, %rdx
	movdqa	%xmm0, %xmm6
	punpckhwd	%xmm11, %xmm0
	punpcklwd	%xmm11, %xmm6
	pand	%xmm0, %xmm1
	paddd	%xmm0, %xmm3
	pand	%xmm6, %xmm9
	paddd	%xmm6, %xmm7
	paddd	%xmm12, %xmm6
	pcmpeqd	%xmm13, %xmm9
	psubd	%xmm8, %xmm6
	paddd	%xmm12, %xmm0
	pcmpeqd	%xmm13, %xmm1
	pcmpgtd	-48(%rbp), %xmm6
	psubd	%xmm8, %xmm0
	pcmpgtd	-48(%rbp), %xmm0
	psubd	%xmm8, %xmm7
	psubd	%xmm8, %xmm3
	pcmpgtd	-64(%rbp), %xmm7
	pcmpgtd	-64(%rbp), %xmm3
	movdqa	%xmm9, %xmm10
	pcmpeqd	%xmm4, %xmm10
	pcmpeqd	%xmm4, %xmm6
	movdqa	%xmm1, %xmm2
	pcmpeqd	%xmm4, %xmm2
	pcmpeqd	%xmm4, %xmm0
	pand	%xmm10, %xmm6
	pand	%xmm7, %xmm6
	pcmpeqd	%xmm4, %xmm7
	pand	%xmm2, %xmm0
	pand	%xmm3, %xmm0
	pcmpeqd	%xmm4, %xmm3
	pand	%xmm7, %xmm10
	por	%xmm10, %xmm9
	pand	%xmm3, %xmm2
	por	%xmm9, %xmm6
	por	%xmm2, %xmm1
	psubd	%xmm6, %xmm5
	por	%xmm1, %xmm0
	psubd	%xmm0, %xmm5
	cmpq	%rdi, %rdx
	jne	.L390
	movdqa	%xmm5, %xmm0
	psrldq	$8, %xmm0
	paddd	%xmm0, %xmm5
	movdqa	%xmm5, %xmm0
	psrldq	$4, %xmm0
	paddd	%xmm0, %xmm5
	movd	%xmm5, %edx
	addl	%edx, %ecx
	movl	%r9d, %edx
	andl	$-8, %edx
	addl	%edx, %esi
	cmpl	%r9d, %edx
	je	.L387
.L388:
	movslq	%esi, %rdx
	movzwl	(%r8,%rdx,2), %edx
	movl	%edx, %edi
	andl	$-4, %edi
	cmpl	$8204, %edi
	je	.L392
	leal	-8234(%rdx), %edi
	cmpl	$4, %edi
	ja	.L459
.L392:
	addl	$1, %ecx
.L393:
	leal	1(%rsi), %edx
	cmpl	%edx, %r12d
	jle	.L387
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	movl	%edx, %edi
	andl	$-4, %edi
	cmpl	$8204, %edi
	je	.L394
	leal	-8234(%rdx), %edi
	cmpl	$4, %edi
	ja	.L460
.L394:
	addl	$1, %ecx
.L395:
	leal	2(%rsi), %edx
	cmpl	%edx, %r12d
	jle	.L387
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	movl	%edx, %edi
	andl	$-4, %edi
	cmpl	$8204, %edi
	je	.L396
	leal	-8234(%rdx), %edi
	cmpl	$4, %edi
	ja	.L461
.L396:
	addl	$1, %ecx
.L397:
	leal	3(%rsi), %edx
	cmpl	%edx, %r12d
	jle	.L387
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	movl	%edx, %edi
	andl	$-4, %edi
	cmpl	$8204, %edi
	je	.L398
	leal	-8234(%rdx), %edi
	cmpl	$4, %edi
	ja	.L462
.L398:
	addl	$1, %ecx
.L399:
	leal	4(%rsi), %edx
	cmpl	%edx, %r12d
	jle	.L387
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	movl	%edx, %edi
	andl	$-4, %edi
	cmpl	$8204, %edi
	je	.L400
	leal	-8234(%rdx), %edi
	cmpl	$4, %edi
	ja	.L463
.L400:
	addl	$1, %ecx
.L401:
	leal	5(%rsi), %edx
	cmpl	%edx, %r12d
	jle	.L387
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	movl	%edx, %edi
	andl	$-4, %edi
	cmpl	$8204, %edi
	je	.L402
	leal	-8234(%rdx), %edi
	cmpl	$4, %edi
	jbe	.L402
	subl	$8294, %edx
	cmpl	$3, %edx
	ja	.L403
	.p2align 4,,10
	.p2align 3
.L402:
	addl	$1, %ecx
.L403:
	addl	$6, %esi
	cmpl	%esi, %r12d
	jle	.L387
	movslq	%esi, %rsi
	movzwl	(%r8,%rsi,2), %edx
	movl	%edx, %esi
	andl	$-4, %esi
	cmpl	$8204, %esi
	je	.L404
	leal	-8234(%rdx), %esi
	cmpl	$4, %esi
	jbe	.L404
	subl	$8294, %edx
	cmpl	$3, %edx
	jbe	.L404
	.p2align 4,,10
	.p2align 3
.L387:
	subl	%ecx, %eax
.L359:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movl	296(%rbx), %r9d
	testl	%r9d, %r9d
	jns	.L369
	movq	%rbx, %rdi
	call	ubidi_getRuns_67.part.0
	testb	%al, %al
	je	.L370
	movl	296(%rbx), %r9d
.L369:
	movq	304(%rbx), %rdx
	testl	%r9d, %r9d
	jle	.L456
	movl	(%rdx), %r8d
	movl	%r12d, %esi
	movl	4(%rdx), %edi
	movl	%r8d, %eax
	andl	$2147483647, %eax
	subl	%eax, %esi
	js	.L373
	cmpl	%esi, %edi
	jg	.L464
.L373:
	xorl	%ecx, %ecx
	addq	$12, %rdx
	addl	$1, %ecx
	cmpl	%r9d, %ecx
	je	.L456
	.p2align 4,,10
	.p2align 3
.L450:
	movl	(%rdx), %r8d
	movl	4(%rdx), %eax
	movl	%r12d, %r11d
	addq	$12, %rdx
	movl	%r8d, %esi
	movl	%eax, %r10d
	andl	$2147483647, %esi
	subl	%edi, %r10d
	subl	%esi, %r11d
	movl	%r11d, %esi
	cmpl	%r11d, %r10d
	jle	.L410
	testl	%r11d, %r11d
	jns	.L371
.L410:
	addl	$1, %ecx
	movl	%eax, %edi
	cmpl	%r9d, %ecx
	jne	.L450
.L456:
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movq	304(%rbx), %rcx
	xorl	%esi, %esi
	addq	$4, %rcx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L380:
	andl	$10, %edx
	cmpl	$1, %edx
	sbbl	$-1, %esi
	addq	$12, %rcx
.L382:
	movl	4(%rcx), %edx
	movl	%edx, %edi
	andl	$5, %edi
	cmpl	$1, %edi
	sbbl	$-1, %esi
	cmpl	%eax, (%rcx)
	jle	.L380
	addq	$40, %rsp
	addl	%esi, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	movl	%r12d, %eax
	jmp	.L367
.L464:
	movl	%edi, %eax
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L371:
	subl	%esi, %eax
	addl	%esi, %edi
	subl	$1, %eax
	testl	%r8d, %r8d
	cmovns	%edi, %eax
	cmpl	%r9d, %ecx
	jl	.L367
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L412:
	movl	%r10d, %edx
.L384:
	movl	4(%r9), %r10d
	movq	%r9, %rsi
	leaq	12(%r9), %r9
	subl	%edi, %ecx
	movl	-4(%r9), %edi
	cmpl	%eax, %r10d
	jle	.L412
	subl	%edx, %r10d
	movl	%r10d, %edx
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L404:
	addl	$1, %ecx
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L459:
	subl	$8294, %edx
	cmpl	$3, %edx
	ja	.L393
	jmp	.L392
.L460:
	subl	$8294, %edx
	cmpl	$3, %edx
	ja	.L395
	jmp	.L394
.L461:
	subl	$8294, %edx
	cmpl	$3, %edx
	ja	.L397
	jmp	.L396
.L462:
	subl	$8294, %edx
	cmpl	$3, %edx
	ja	.L399
	jmp	.L398
.L463:
	subl	$8294, %edx
	cmpl	$3, %edx
	ja	.L401
	jmp	.L400
.L363:
	movl	$27, 0(%r13)
	jmp	.L456
.L365:
	movl	$1, 0(%r13)
	movl	$-1, %eax
	jmp	.L359
.L370:
	movl	$7, 0(%r13)
	movl	$-1, %eax
	jmp	.L359
.L457:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE2108:
	.size	ubidi_getVisualIndex_67, .-ubidi_getVisualIndex_67
	.p2align 4
	.globl	ubidi_getLogicalIndex_67
	.type	ubidi_getLogicalIndex_67, @function
ubidi_getLogicalIndex_67:
.LFB2109:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L541
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L538
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L469
	movq	(%rdi), %rax
	movl	%esi, %ebx
	cmpq	%rdi, %rax
	je	.L470
	testq	%rax, %rax
	je	.L469
	cmpq	(%rax), %rax
	jne	.L469
.L470:
	testl	%ebx, %ebx
	js	.L471
	cmpl	%ebx, 24(%r12)
	jle	.L471
	movl	420(%r12), %eax
	testl	%eax, %eax
	je	.L542
	movl	296(%r12), %r9d
	testl	%r9d, %r9d
	jns	.L475
.L507:
	movq	%r12, %rdi
	call	ubidi_getRuns_67.part.0
	testb	%al, %al
	je	.L476
	movl	420(%r12), %eax
	movl	296(%r12), %r9d
.L475:
	movq	304(%r12), %r8
	testl	%eax, %eax
	jle	.L477
	leaq	4(%r8), %rax
	xorl	%edi, %edi
	xorl	%edx, %edx
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L479:
	andl	$10, %ecx
	je	.L481
	cmpl	%ebx, %edi
	je	.L538
	addl	$1, %edx
.L481:
	addq	$12, %rax
	movl	%esi, %edi
.L482:
	movl	4(%rax), %ecx
	movl	(%rax), %esi
	testb	$5, %cl
	je	.L478
	addl	%edx, %edi
	cmpl	%ebx, %edi
	jge	.L538
	addl	$1, %edx
.L478:
	leal	(%rsi,%rdx), %edi
	cmpl	%ebx, %edi
	jle	.L479
	subl	%edx, %ebx
.L480:
	cmpl	$10, %r9d
	jg	.L515
	movl	4(%r8), %ecx
	movl	$12, %eax
	cmpl	%ecx, %ebx
	jl	.L543
	.p2align 4,,10
	.p2align 3
.L497:
	leaq	(%r8,%rax), %rdx
	movq	%rax, %rsi
	addq	$12, %rax
	movl	-8(%r8,%rax), %ecx
	cmpl	%ebx, %ecx
	jle	.L497
	movl	(%rdx), %eax
	testl	%eax, %eax
	js	.L499
	movl	-8(%r8,%rsi), %edx
	subl	%edx, %ebx
.L498:
	addl	%ebx, %eax
.L551:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L542:
	.cfi_restore_state
	movl	440(%r12), %edx
	testl	%edx, %edx
	jne	.L474
	movl	184(%r12), %ecx
	movl	%ebx, %eax
	testl	%ecx, %ecx
	je	.L465
	cmpl	$1, %ecx
	je	.L544
.L474:
	movl	296(%r12), %r9d
	testl	%r9d, %r9d
	js	.L507
	movq	304(%r12), %r8
.L505:
	testl	%edx, %edx
	jle	.L480
	movl	4(%r8), %ecx
	movl	8(%r8), %esi
	leaq	12(%r8), %r11
	xorl	%edx, %edx
	leal	(%rcx,%rsi), %eax
	cmpl	%eax, %ebx
	jge	.L484
	movq	%r8, %rdi
	xorl	%eax, %eax
	xorl	%edx, %edx
.L483:
	testl	%esi, %esi
	je	.L486
	movl	(%rdi), %edi
	movl	%edi, %esi
	andl	$2147483647, %esi
	leal	-1(%rsi,%rcx), %r10d
	testl	%ecx, %ecx
	jle	.L486
	movq	8(%r12), %r11
	testl	%edi, %edi
	js	.L545
	movslq	%esi, %rsi
	leal	(%rcx,%rax), %edi
	leaq	(%r11,%rsi,2), %rsi
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L547:
	addl	$1, %eax
	addq	$2, %rsi
	cmpl	%eax, %edi
	je	.L546
.L495:
	movzwl	(%rsi), %ecx
	movl	%ecx, %r10d
	andl	$-4, %r10d
	cmpl	$8204, %r10d
	je	.L493
	leal	-8234(%rcx), %r10d
	cmpl	$4, %r10d
	jbe	.L493
	subl	$8294, %ecx
	cmpl	$3, %ecx
	ja	.L494
	.p2align 4,,10
	.p2align 3
.L493:
	addl	$1, %edx
.L494:
	leal	(%rdx,%rbx), %ecx
	cmpl	%eax, %ecx
	jne	.L547
	movl	%eax, %ebx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L503:
	movl	(%r8), %eax
	testl	%eax, %eax
	jns	.L498
	movl	4(%r8), %ecx
.L499:
	notl	%ebx
	andl	$2147483647, %eax
	addl	%ebx, %ecx
	addl	%ecx, %eax
.L465:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L496:
	leal	(%r10,%r9), %esi
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%esi, %eax
	sarl	%eax
	movslq	%eax, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	salq	$2, %rdx
	leaq	(%r8,%rdx), %rdi
	movl	4(%rdi), %ecx
	cmpl	%ebx, %ecx
	jg	.L501
	leal	1(%rax), %r10d
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L501:
	testl	%eax, %eax
	je	.L503
	movl	-8(%r8,%rdx), %edx
	cmpl	%ebx, %edx
	jle	.L548
	movl	%eax, %r9d
	jmp	.L496
.L469:
	movl	$27, 0(%r13)
.L538:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L510:
	.cfi_restore_state
	movl	%r10d, %ecx
.L484:
	movl	4(%r11), %r10d
	subl	%esi, %edx
	movl	8(%r11), %esi
	movq	%r11, %rdi
	leaq	12(%r11), %r11
	movl	%r10d, %eax
	subl	%edx, %eax
	addl	%esi, %eax
	cmpl	%ebx, %eax
	jle	.L510
	subl	%ecx, %r10d
	movl	%ecx, %eax
	movl	%r10d, %ecx
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L477:
	movl	440(%r12), %edx
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L548:
	movl	(%rdi), %eax
	testl	%eax, %eax
	js	.L499
	cmpl	$1, %esi
	jle	.L498
	subl	%edx, %ebx
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L486:
	addl	%edx, %ebx
	jmp	.L480
.L544:
	notl	%eax
	addl	20(%r12), %eax
	jmp	.L465
.L545:
	movslq	%r10d, %r10
	addl	%eax, %ecx
	leaq	(%r11,%r10,2), %rdi
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L550:
	addl	$1, %eax
	subq	$2, %rdi
	cmpl	%eax, %ecx
	je	.L549
.L492:
	movzwl	(%rdi), %esi
	movl	%esi, %r10d
	andl	$-4, %r10d
	cmpl	$8204, %r10d
	je	.L490
	leal	-8234(%rsi), %r10d
	cmpl	$4, %r10d
	jbe	.L490
	subl	$8294, %esi
	cmpl	$3, %esi
	ja	.L489
	.p2align 4,,10
	.p2align 3
.L490:
	addl	$1, %edx
.L489:
	leal	(%rbx,%rdx), %esi
	cmpl	%esi, %eax
	jne	.L550
	movl	%eax, %ebx
	jmp	.L480
.L543:
	movl	(%r8), %eax
	testl	%eax, %eax
	js	.L499
	addl	%ebx, %eax
	jmp	.L551
.L546:
	movl	%ecx, %ebx
	jmp	.L480
.L549:
	movl	%esi, %ebx
	jmp	.L480
.L471:
	movl	$1, 0(%r13)
	movl	$-1, %eax
	jmp	.L465
.L476:
	movl	$7, 0(%r13)
	movl	$-1, %eax
	jmp	.L465
.L541:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2109:
	.size	ubidi_getLogicalIndex_67, .-ubidi_getLogicalIndex_67
	.p2align 4
	.globl	ubidi_getLogicalMap_67
	.type	ubidi_getLogicalMap_67, @function
ubidi_getLogicalMap_67:
.LFB2110:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L619
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r9d
	testl	%r9d, %r9d
	jle	.L623
.L552:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	.cfi_restore_state
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L556
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	cmpq	%rax, %rdi
	je	.L557
	testq	%rax, %rax
	je	.L556
	cmpq	(%rax), %rax
	jne	.L556
.L557:
	movl	296(%r12), %r8d
	testl	%r8d, %r8d
	jns	.L558
	movq	%r12, %rdi
	call	ubidi_getRuns_67.part.0
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L552
	.p2align 4,,10
	.p2align 3
.L558:
	testq	%rbx, %rbx
	je	.L624
	movslq	20(%r12), %rax
	testl	%eax, %eax
	jle	.L552
	movq	304(%r12), %rcx
	movq	%rcx, -56(%rbp)
	cmpl	24(%r12), %eax
	jg	.L625
.L560:
	movl	296(%r12), %esi
	testl	%esi, %esi
	jle	.L552
	movq	-56(%rbp), %r9
	xorl	%r11d, %r11d
	xorl	%ecx, %ecx
	movl	$1, %r14d
	movdqa	.LC11(%rip), %xmm5
	movdqa	.LC8(%rip), %xmm3
	movq	%r9, %r10
	movdqa	%xmm5, %xmm4
	movdqa	%xmm3, %xmm2
	.p2align 4,,10
	.p2align 3
.L570:
	movl	(%r9), %eax
	movl	4(%r9), %edi
	movl	%eax, %edx
	andl	$2147483647, %edx
	testl	%eax, %eax
	js	.L561
	movl	%edi, %r8d
	movl	%r14d, %r15d
	subl	%ecx, %r8d
	cmpl	%ecx, %edi
	cmovg	%r8d, %r15d
	subl	$1, %r8d
	cmpl	%ecx, %edi
	jle	.L596
	cmpl	$2, %r8d
	jbe	.L596
	movl	%r15d, %esi
	movd	%ecx, %xmm6
	movslq	%edx, %rax
	shrl	$2, %esi
	pshufd	$0, %xmm6, %xmm0
	leaq	(%rbx,%rax,4), %rax
	salq	$4, %rsi
	paddd	%xmm4, %xmm0
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L563:
	movdqa	%xmm0, %xmm1
	addq	$16, %rax
	paddd	%xmm2, %xmm0
	movups	%xmm1, -16(%rax)
	cmpq	%rsi, %rax
	jne	.L563
	movl	%r15d, %esi
	andl	$-4, %esi
	leal	(%rcx,%rsi), %eax
	addl	%esi, %edx
	cmpl	%esi, %r15d
	je	.L564
.L562:
	movslq	%edx, %rdx
	leal	1(%rax), %esi
	movl	%eax, (%rbx,%rdx,4)
	leaq	0(,%rdx,4), %r15
	cmpl	%esi, %edi
	jle	.L564
	addl	$2, %eax
	movl	%esi, 4(%rbx,%r15)
	cmpl	%eax, %edi
	jle	.L564
	movl	%eax, 8(%rbx,%r15)
.L564:
	cmpl	%ecx, %edi
	movl	$0, %eax
	cmovle	%eax, %r8d
	leal	1(%rcx,%r8), %ecx
.L568:
	movl	296(%r12), %eax
	addl	$1, %r11d
	addq	$12, %r9
	cmpl	%r11d, %eax
	jg	.L570
	movl	420(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L571
	testl	%eax, %eax
	jle	.L552
	movq	-56(%rbp), %rcx
	subl	$1, %eax
	movl	8(%r10), %edx
	xorl	%r8d, %r8d
	leaq	(%rax,%rax,2), %rax
	leaq	12(%rcx,%rax,4), %r9
	movl	%r8d, %eax
	xorl	%ecx, %ecx
	movl	4(%r10), %r8d
	testb	$5, %dl
	je	.L572
	.p2align 4,,10
	.p2align 3
.L626:
	addl	$1, %ecx
.L573:
	movl	(%r10), %esi
	movl	%r8d, %r12d
	subl	%eax, %r12d
	andl	$2147483647, %esi
	leal	(%rsi,%r12), %r11d
	cmpl	%r11d, %esi
	jge	.L574
	leal	-1(%r12), %eax
	cmpl	$2, %eax
	jbe	.L575
	movl	%r12d, %edi
	movslq	%esi, %rax
	movd	%ecx, %xmm5
	shrl	$2, %edi
	leaq	(%rbx,%rax,4), %rax
	pshufd	$0, %xmm5, %xmm1
	salq	$4, %rdi
	addq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L576:
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	paddd	%xmm1, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rdi, %rax
	jne	.L576
	movl	%r12d, %eax
	andl	$-4, %eax
	addl	%eax, %esi
	cmpl	%r12d, %eax
	je	.L574
.L575:
	movslq	%esi, %rax
	leal	1(%rsi), %edi
	salq	$2, %rax
	addl	%ecx, (%rbx,%rax)
	cmpl	%r11d, %edi
	jge	.L574
	addl	$2, %esi
	addl	%ecx, 4(%rbx,%rax)
	cmpl	%esi, %r11d
	jle	.L574
	addl	%ecx, 8(%rbx,%rax)
	.p2align 4,,10
	.p2align 3
.L574:
	andl	$10, %edx
	cmpl	$1, %edx
	sbbl	$-1, %ecx
	addq	$12, %r10
	cmpq	%r9, %r10
	je	.L552
	movl	8(%r10), %edx
	movl	%r8d, %eax
	movl	4(%r10), %r8d
	testb	$5, %dl
	jne	.L626
.L572:
	testl	%ecx, %ecx
	je	.L574
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L561:
	movl	%edi, %esi
	movl	%r14d, %r8d
	subl	%ecx, %esi
	addl	%esi, %edx
	cmpl	%ecx, %edi
	cmovg	%esi, %r8d
	subl	$1, %esi
	cmpl	%ecx, %edi
	jle	.L597
	cmpl	$2, %esi
	jbe	.L597
	movslq	%edx, %rax
	movl	%r8d, %r15d
	movd	%ecx, %xmm7
	leaq	-16(%rbx,%rax,4), %rax
	shrl	$2, %r15d
	pshufd	$0, %xmm7, %xmm1
	salq	$4, %r15
	movq	%rax, %r13
	paddd	%xmm5, %xmm1
	subq	%r15, %r13
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L567:
	movdqa	%xmm1, %xmm0
	subq	$16, %rax
	paddd	%xmm3, %xmm1
	pshufd	$27, %xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	cmpq	%r15, %rax
	jne	.L567
	movl	%r8d, %r15d
	andl	$-4, %r15d
	leal	(%rcx,%r15), %eax
	subl	%r15d, %edx
	cmpl	%r15d, %r8d
	je	.L569
.L565:
	subl	$1, %edx
	leal	1(%rax), %r8d
	movslq	%edx, %rdx
	movl	%eax, (%rbx,%rdx,4)
	leaq	0(,%rdx,4), %r15
	cmpl	%r8d, %edi
	jle	.L569
	addl	$2, %eax
	movl	%r8d, -4(%rbx,%r15)
	cmpl	%eax, %edi
	jle	.L569
	movl	%eax, -8(%rbx,%r15)
.L569:
	cmpl	%ecx, %edi
	movl	$0, %eax
	cmovle	%eax, %esi
	leal	1(%rcx,%rsi), %ecx
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L571:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	440(%r12), %edx
	testl	%edx, %edx
	jle	.L552
	testl	%eax, %eax
	jle	.L552
	movq	-56(%rbp), %rdi
	subl	$1, %eax
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	leaq	(%rax,%rax,2), %rax
	leaq	-4(%rbx), %r10
	leaq	12(%rdi,%rax,4), %r9
	.p2align 4,,10
	.p2align 3
.L595:
	movl	%r8d, %eax
	movl	4(%rdi), %r8d
	movl	%r8d, %r13d
	subl	%eax, %r13d
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	je	.L580
	movl	(%rdi), %r11d
	movl	%r11d, %ecx
	andl	$2147483647, %ecx
	leal	(%rcx,%r13), %edx
	testl	%eax, %eax
	je	.L627
	testl	%r13d, %r13d
	jle	.L580
	movq	8(%r12), %rax
	testl	%r11d, %r11d
	js	.L628
	movslq	%ecx, %r11
	subl	$1, %r13d
	leaq	(%rax,%r11,2), %rdx
	leaq	(%rbx,%r11,4), %rcx
	addq	%r13, %r11
	leaq	2(%rax,%r11,2), %r13
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L629:
	subl	$8294, %eax
	cmpl	$3, %eax
	jbe	.L591
	subl	%esi, (%rcx)
.L593:
	addq	$2, %rdx
	addq	$4, %rcx
	cmpq	%r13, %rdx
	je	.L580
.L594:
	movzwl	(%rdx), %eax
	movl	%eax, %r11d
	andl	$-4, %r11d
	cmpl	$8204, %r11d
	je	.L591
	leal	-8234(%rax), %r11d
	cmpl	$4, %r11d
	ja	.L629
.L591:
	movl	$-1, (%rcx)
	addl	$1, %esi
	jmp	.L593
.L625:
	leaq	0(,%rax,4), %rdx
	movl	$255, %esi
	movq	%rbx, %rdi
	call	memset@PLT
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L627:
	cmpl	%edx, %ecx
	jge	.L580
	leal	-1(%r13), %eax
	cmpl	$2, %eax
	jbe	.L582
	movl	%r13d, %r11d
	movslq	%ecx, %rax
	movd	%esi, %xmm4
	shrl	$2, %r11d
	leaq	(%rbx,%rax,4), %rax
	pshufd	$0, %xmm4, %xmm1
	salq	$4, %r11
	addq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L583:
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	psubd	%xmm1, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%r11, %rax
	jne	.L583
	movl	%r13d, %eax
	andl	$-4, %eax
	addl	%eax, %ecx
	cmpl	%eax, %r13d
	je	.L580
.L582:
	movslq	%ecx, %rax
	leal	1(%rcx), %r11d
	salq	$2, %rax
	subl	%esi, (%rbx,%rax)
	cmpl	%edx, %r11d
	jge	.L580
	addl	$2, %ecx
	subl	%esi, 4(%rbx,%rax)
	cmpl	%edx, %ecx
	jge	.L580
	subl	%esi, 8(%rbx,%rax)
	.p2align 4,,10
	.p2align 3
.L580:
	addq	$12, %rdi
	cmpq	%r9, %rdi
	jne	.L595
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L624:
	movl	$1, 0(%r13)
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L596:
	movl	%ecx, %eax
	jmp	.L562
.L628:
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rcx
	salq	$2, %rdx
	leaq	(%r10,%rdx), %r11
	leaq	(%rbx,%rdx), %rax
	leal	-1(%r13), %edx
	salq	$2, %rdx
	subq	%rdx, %r11
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L630:
	subl	$8294, %edx
	cmpl	$3, %edx
	jbe	.L588
	subl	%esi, -4(%rax)
.L587:
	subq	$4, %rax
	subq	$2, %rcx
	cmpq	%r11, %rax
	je	.L580
.L590:
	movzwl	-2(%rcx), %edx
	movl	%edx, %r13d
	andl	$-4, %r13d
	cmpl	$8204, %r13d
	je	.L588
	leal	-8234(%rdx), %r13d
	cmpl	$4, %r13d
	ja	.L630
.L588:
	movl	$-1, -4(%rax)
	addl	$1, %esi
	jmp	.L587
.L597:
	movl	%ecx, %eax
	jmp	.L565
.L556:
	movl	$27, 0(%r13)
	jmp	.L552
	.cfi_endproc
.LFE2110:
	.size	ubidi_getLogicalMap_67, .-ubidi_getLogicalMap_67
	.p2align 4
	.globl	ubidi_getVisualMap_67
	.type	ubidi_getVisualMap_67, @function
ubidi_getVisualMap_67:
.LFB2111:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L734
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L631
	movq	%rsi, %r15
	testq	%rsi, %rsi
	je	.L737
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L636
	movq	(%rdi), %rax
	cmpq	%rax, %rdi
	je	.L637
	testq	%rax, %rax
	je	.L636
	cmpq	(%rax), %rax
	jne	.L636
.L637:
	movl	296(%rbx), %edi
	testl	%edi, %edi
	jns	.L638
	movq	%rbx, %rdi
	call	ubidi_getRuns_67.part.0
	movl	(%r12), %esi
	testl	%esi, %esi
	jle	.L638
.L631:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L638:
	.cfi_restore_state
	movl	24(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L631
	movslq	296(%rbx), %rax
	movq	304(%rbx), %rcx
	movq	%r15, %r8
	xorl	%edx, %edx
	leaq	(%rax,%rax,2), %rax
	movq	%rcx, -56(%rbp)
	movq	%rcx, %rdi
	leaq	(%rcx,%rax,4), %r11
	cmpq	%r11, %rcx
	jnb	.L651
	movdqa	.LC1(%rip), %xmm6
	movdqa	.LC2(%rip), %xmm4
	movl	$1, %r12d
	pcmpeqd	%xmm3, %xmm3
	movdqa	.LC11(%rip), %xmm5
	movdqa	.LC8(%rip), %xmm2
	.p2align 4,,10
	.p2align 3
.L639:
	movl	(%rdi), %ecx
	movl	4(%rdi), %esi
	testl	%ecx, %ecx
	js	.L642
	movl	%esi, %r10d
	movl	%r12d, %r13d
	subl	%edx, %r10d
	cmpl	%esi, %edx
	cmovl	%r10d, %r13d
	subl	$1, %r10d
	cmpl	%esi, %edx
	jge	.L684
	cmpl	$2, %r10d
	jbe	.L684
	movl	%r13d, %r9d
	movd	%ecx, %xmm7
	movq	%r8, %rax
	shrl	$2, %r9d
	pshufd	$0, %xmm7, %xmm0
	salq	$4, %r9
	paddd	%xmm5, %xmm0
	addq	%r8, %r9
	.p2align 4,,10
	.p2align 3
.L644:
	movdqa	%xmm0, %xmm1
	addq	$16, %rax
	paddd	%xmm2, %xmm0
	movups	%xmm1, -16(%rax)
	cmpq	%r9, %rax
	jne	.L644
	movl	%r13d, %eax
	andl	$-4, %eax
	movl	%eax, %r14d
	leal	(%rdx,%rax), %r9d
	addl	%eax, %ecx
	leaq	(%r8,%r14,4), %r14
	cmpl	%eax, %r13d
	je	.L645
.L643:
	leal	1(%r9), %eax
	movl	%ecx, (%r14)
	leal	1(%rcx), %r13d
	cmpl	%eax, %esi
	jle	.L645
	addl	$2, %r9d
	movl	%r13d, 4(%r14)
	addl	$2, %ecx
	cmpl	%r9d, %esi
	jle	.L645
	movl	%ecx, 8(%r14)
.L645:
	movl	%r10d, %eax
	cmpl	%esi, %edx
	movl	$4, %ecx
	leaq	4(,%rax,4), %rax
	cmovge	%rcx, %rax
	addq	%rax, %r8
	cmpl	%esi, %edx
	movl	$0, %eax
	cmovge	%eax, %r10d
	addq	$12, %rdi
	leal	1(%rdx,%r10), %edx
	cmpq	%rdi, %r11
	ja	.L639
.L651:
	movl	420(%rbx), %edx
	testl	%edx, %edx
	jle	.L738
	movslq	296(%rbx), %r8
	testl	%r8d, %r8d
	jle	.L631
	movq	-56(%rbp), %rcx
	leal	-1(%r8), %eax
	movq	%rax, %r9
	leaq	(%rax,%rax,2), %rax
	leaq	8(%rcx), %rdx
	leaq	20(%rcx,%rax,4), %rdi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L654:
	movl	(%rdx), %eax
	movl	%eax, %esi
	andl	$5, %esi
	cmpl	$1, %esi
	sbbl	$-1, %ecx
	andl	$10, %eax
	cmpl	$1, %eax
	sbbl	$-1, %ecx
	addq	$12, %rdx
	cmpq	%rdi, %rdx
	jne	.L654
	movl	24(%rbx), %r12d
	testl	%ecx, %ecx
	jle	.L631
	movq	-56(%rbp), %rbx
	leaq	(%r8,%r8,2), %rax
	leaq	-20(%rbx,%rax,4), %r14
	movl	%r9d, %ebx
	.p2align 4,,10
	.p2align 3
.L666:
	movl	16(%r14), %eax
	movl	%eax, -64(%rbp)
	testb	$10, %al
	je	.L655
	subl	$1, %r12d
	subl	$1, %ecx
	movslq	%r12d, %rax
	movl	$-1, (%r15,%rax,4)
.L655:
	xorl	%edx, %edx
	testl	%ebx, %ebx
	je	.L656
	movl	(%r14), %edx
.L656:
	movl	12(%r14), %r9d
	testl	%ecx, %ecx
	setle	-56(%rbp)
	movzbl	-56(%rbp), %eax
	leal	-1(%r9), %r8d
	cmpl	%r8d, %edx
	jg	.L657
	testb	%al, %al
	jne	.L657
	movl	%r9d, %r10d
	movslq	%r12d, %rsi
	subl	%edx, %r10d
	leal	-1(%r10), %eax
	movl	%eax, -60(%rbp)
	leaq	0(,%rsi,4), %rax
	leaq	-16(,%rsi,4), %rsi
	movq	%rax, -72(%rbp)
	movslq	%r8d, %rax
	leaq	4(,%rax,4), %r11
	leaq	-12(,%rax,4), %rdi
	cmpq	%r11, %rsi
	setge	%r13b
	cmpq	%rdi, -72(%rbp)
	setle	%r11b
	orl	%r13d, %r11d
	cmpl	%edx, %r9d
	setg	%r13b
	testb	%r13b, %r11b
	je	.L658
	cmpl	$3, -60(%rbp)
	jbe	.L658
	cmpl	%edx, %r9d
	movl	$1, %eax
	cmovle	%eax, %r10d
	addq	%r15, %rdi
	addq	%r15, %rsi
	xorl	%eax, %eax
	movl	%r10d, %r11d
	shrl	$2, %r11d
	negq	%r11
	salq	$4, %r11
	.p2align 4,,10
	.p2align 3
.L660:
	movdqu	(%rdi,%rax), %xmm7
	movups	%xmm7, (%rsi,%rax)
	subq	$16, %rax
	cmpq	%r11, %rax
	jne	.L660
	movl	%r10d, %esi
	movl	%r12d, %eax
	andl	$-4, %esi
	subl	%esi, %r8d
	subl	%esi, %eax
	cmpl	%esi, %r10d
	je	.L662
	movslq	%r8d, %rdi
	subl	$1, %eax
	movl	(%r15,%rdi,4), %edi
	cltq
	leaq	0(,%rax,4), %rsi
	movl	%edi, (%r15,%rax,4)
	leal	-1(%r8), %eax
	cmpl	%eax, %edx
	jg	.L662
	cltq
	subl	$2, %r8d
	leaq	0(,%rax,4), %rdi
	movl	(%r15,%rax,4), %eax
	movl	%eax, -4(%r15,%rsi)
	cmpl	%r8d, %edx
	jg	.L662
	movl	-4(%r15,%rdi), %eax
	movl	%eax, -8(%r15,%rsi)
.L662:
	movl	-60(%rbp), %eax
	movl	$0, %edi
	negl	%eax
	cmpl	%edx, %r9d
	cmovle	%edi, %eax
	leal	-1(%r12,%rax), %r12d
.L657:
	testb	$5, -64(%rbp)
	je	.L664
	subl	$1, %r12d
	subl	$1, %ecx
	subq	$12, %r14
	movslq	%r12d, %rax
	movl	$-1, (%r15,%rax,4)
	subl	$1, %ebx
	js	.L631
	testl	%ecx, %ecx
	jg	.L666
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L642:
	movl	%esi, %r10d
	andl	$2147483647, %ecx
	movl	%r12d, %r13d
	subl	%edx, %r10d
	addl	%r10d, %ecx
	cmpl	%esi, %edx
	cmovl	%r10d, %r13d
	subl	$1, %r10d
	cmpl	%esi, %edx
	jge	.L685
	cmpl	$2, %r10d
	jbe	.L685
	movl	%r13d, %r9d
	movd	%ecx, %xmm7
	movq	%r8, %rax
	shrl	$2, %r9d
	pshufd	$0, %xmm7, %xmm1
	salq	$4, %r9
	paddd	%xmm6, %xmm1
	addq	%r8, %r9
	.p2align 4,,10
	.p2align 3
.L647:
	movdqa	%xmm1, %xmm0
	addq	$16, %rax
	paddd	%xmm4, %xmm1
	paddd	%xmm3, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%r9, %rax
	jne	.L647
	movl	%r13d, %eax
	andl	$-4, %eax
	movl	%eax, %r9d
	leal	(%rdx,%rax), %r14d
	subl	%eax, %ecx
	leaq	(%r8,%r9,4), %r9
	cmpl	%eax, %r13d
	je	.L645
.L646:
	leal	-1(%rcx), %eax
	movl	%eax, (%r9)
	leal	1(%r14), %eax
	cmpl	%eax, %esi
	jle	.L645
	leal	-2(%rcx), %eax
	addl	$2, %r14d
	movl	%eax, 4(%r9)
	cmpl	%r14d, %esi
	jle	.L645
	subl	$3, %ecx
	movl	%ecx, 8(%r9)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L664:
	subq	$12, %r14
	subl	$1, %ebx
	js	.L631
	cmpb	$0, -56(%rbp)
	je	.L666
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L734:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, (%rdx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L658:
	.cfi_restore_state
	movq	-72(%rbp), %rdi
	movslq	%r9d, %rsi
	negq	%rsi
	leaq	(%rdi,%rsi,4), %rdi
	addq	%r15, %rdi
	.p2align 4,,10
	.p2align 3
.L663:
	movl	(%r15,%rax,4), %esi
	movl	%esi, (%rdi,%rax,4)
	subq	$1, %rax
	cmpl	%eax, %edx
	jle	.L663
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%r8, %r14
	movl	%edx, %r9d
	jmp	.L643
.L738:
	movl	440(%rbx), %eax
	testl	%eax, %eax
	jle	.L631
	movl	296(%rbx), %edx
	testl	%edx, %edx
	jle	.L631
	movq	-56(%rbp), %rax
	subl	$1, %edx
	leaq	4(%r15), %rdi
	xorl	%ecx, %ecx
	leaq	(%rdx,%rdx,2), %rdx
	movq	%rdi, -56(%rbp)
	xorl	%esi, %esi
	leaq	12(%rax,%rdx,4), %r9
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L739:
	cmpl	%ecx, %edi
	jne	.L667
	addl	%r8d, %ecx
.L668:
	addq	$12, %rax
	cmpq	%r9, %rax
	je	.L631
.L682:
	movl	%esi, %edi
	movl	4(%rax), %esi
	movl	8(%rax), %edx
	movl	%esi, %r8d
	subl	%edi, %r8d
	testl	%edx, %edx
	je	.L739
.L669:
	movl	(%rax), %r10d
	movl	%r10d, %edx
	andl	$2147483647, %edx
	leal	(%rdx,%r8), %r11d
	leal	-1(%r11), %edi
	testl	%r8d, %r8d
	jle	.L668
	movq	8(%rbx), %r8
	testl	%r10d, %r10d
	js	.L740
	movslq	%edx, %rdi
	leaq	(%r8,%rdi,2), %r8
	.p2align 4,,10
	.p2align 3
.L681:
	movzwl	(%r8), %edi
	movl	%edi, %r10d
	andl	$-4, %r10d
	cmpl	$8204, %r10d
	je	.L680
	leal	-8234(%rdi), %r10d
	cmpl	$4, %r10d
	jbe	.L680
	subl	$8294, %edi
	cmpl	$3, %edi
	jbe	.L680
	movslq	%ecx, %rdi
	addl	$1, %ecx
	movl	%edx, (%r15,%rdi,4)
	.p2align 4,,10
	.p2align 3
.L680:
	addl	$1, %edx
	addq	$2, %r8
	cmpl	%edx, %r11d
	jne	.L681
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L667:
	testl	%edx, %edx
	jne	.L669
	cmpl	%edi, %esi
	jle	.L668
	movslq	%ecx, %r8
	movslq	%edi, %r12
	movl	%esi, %r11d
	leaq	0(,%r8,4), %r10
	leaq	0(,%r12,4), %rdx
	subl	%edi, %r11d
	leaq	16(%r10), %r14
	leaq	16(,%r12,4), %r13
	cmpq	%rdx, %r14
	setle	%r14b
	cmpq	%r10, %r13
	setle	%r13b
	orb	%r13b, %r14b
	je	.L670
	leal	-1(%r11), %r13d
	cmpl	$3, %r13d
	jbe	.L670
	movl	%r11d, %r12d
	leaq	(%r15,%rdx), %r8
	addq	%r15, %r10
	xorl	%edx, %edx
	shrl	$2, %r12d
	salq	$4, %r12
	.p2align 4,,10
	.p2align 3
.L672:
	movdqu	(%r8,%rdx), %xmm6
	movups	%xmm6, (%r10,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r12
	jne	.L672
	movl	%r11d, %r8d
	andl	$-4, %r8d
	leal	(%rdi,%r8), %edx
	leal	(%rcx,%r8), %r10d
	cmpl	%r8d, %r11d
	je	.L674
	movslq	%edx, %r8
	leal	1(%r10), %edi
	movslq	%r10d, %r10
	movl	(%r15,%r8,4), %r8d
	movl	%r8d, (%r15,%r10,4)
	leal	1(%rdx), %r8d
	cmpl	%r8d, %esi
	jle	.L674
	movslq	%r8d, %r8
	movslq	%edi, %rdi
	addl	$2, %edx
	leaq	0(,%r8,4), %r12
	movl	(%r15,%r8,4), %r8d
	leaq	0(,%rdi,4), %r10
	movl	%r8d, (%r15,%rdi,4)
	cmpl	%edx, %esi
	jle	.L674
	movl	4(%r15,%r12), %edx
	movl	%edx, 4(%r15,%r10)
.L674:
	addl	%r11d, %ecx
	jmp	.L668
.L685:
	movq	%r8, %r9
	movl	%edx, %r14d
	jmp	.L646
.L740:
	movslq	%edi, %r10
	leaq	(%r8,%r10,2), %r10
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L741:
	movl	%r8d, %edi
.L679:
	movzwl	(%r10), %r8d
	movl	%r8d, %r11d
	andl	$-4, %r11d
	cmpl	$8204, %r11d
	je	.L677
	leal	-8234(%r8), %r11d
	cmpl	$4, %r11d
	jbe	.L677
	subl	$8294, %r8d
	cmpl	$3, %r8d
	jbe	.L677
	movslq	%ecx, %r8
	addl	$1, %ecx
	movl	%edi, (%r15,%r8,4)
	.p2align 4,,10
	.p2align 3
.L677:
	subq	$2, %r10
	leal	-1(%rdi), %r8d
	cmpl	%edi, %edx
	jne	.L741
	jmp	.L668
.L636:
	movl	$27, (%r12)
	jmp	.L631
.L670:
	notl	%edi
	movq	-56(%rbp), %r14
	addq	%r15, %rdx
	addl	%esi, %edi
	addq	%r12, %rdi
	leaq	(%r14,%rdi,4), %r10
	movq	%r8, %rdi
	subq	%r12, %rdi
	.p2align 4,,10
	.p2align 3
.L675:
	movl	(%rdx), %r8d
	movl	%r8d, (%rdx,%rdi,4)
	addq	$4, %rdx
	cmpq	%r10, %rdx
	jne	.L675
	jmp	.L674
	.cfi_endproc
.LFE2111:
	.size	ubidi_getVisualMap_67, .-ubidi_getVisualMap_67
	.p2align 4
	.globl	ubidi_invertMap_67
	.type	ubidi_invertMap_67, @function
ubidi_invertMap_67:
.LFB2112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	testq	%rsi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	%edx, %ebx
	setne	%dl
	testl	%ebx, %ebx
	setg	%al
	testb	%al, %dl
	je	.L742
	testq	%rdi, %rdi
	jne	.L770
.L742:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	.cfi_restore_state
	movslq	%ebx, %rax
	movq	%rsi, %rcx
	salq	$2, %rax
	leaq	(%rdi,%rax), %r12
	cmpq	%r12, %rdi
	jnb	.L755
	leaq	-1(%rax), %rdx
	movq	%rdx, %rsi
	shrq	$2, %rsi
	addq	$1, %rsi
	cmpq	$15, %rdx
	jbe	.L771
	leaq	-16(%rdi,%rax), %rax
	pxor	%xmm3, %xmm3
	pcmpeqd	%xmm1, %xmm1
	movq	%rsi, %rdx
	shrq	$2, %rdx
	movdqa	%xmm3, %xmm5
	movdqa	%xmm1, %xmm4
	movq	%rax, %r10
	salq	$4, %rdx
	subq	%rdx, %r10
	movq	%r10, %rdx
	.p2align 4,,10
	.p2align 3
.L754:
	movdqu	(%rax), %xmm7
	movdqa	%xmm1, %xmm2
	movdqa	%xmm5, %xmm6
	subq	$16, %rax
	pshufd	$27, %xmm7, %xmm0
	pcmpgtd	%xmm0, %xmm2
	pcmpgtd	%xmm0, %xmm6
	pand	%xmm2, %xmm1
	pandn	%xmm0, %xmm2
	movdqa	%xmm6, %xmm0
	pandn	%xmm4, %xmm0
	por	%xmm2, %xmm1
	psubd	%xmm0, %xmm3
	cmpq	%rdx, %rax
	jne	.L754
	movdqa	%xmm3, %xmm0
	movq	%rsi, %r9
	psrldq	$8, %xmm0
	andq	$-4, %r9
	paddd	%xmm0, %xmm3
	movq	%r9, %r8
	movdqa	%xmm3, %xmm0
	negq	%r8
	psrldq	$4, %xmm0
	leaq	(%r12,%r8,4), %r8
	paddd	%xmm0, %xmm3
	movdqa	%xmm1, %xmm0
	psrldq	$8, %xmm0
	movd	%xmm3, %edx
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm1, %xmm2
	pand	%xmm2, %xmm0
	pandn	%xmm1, %xmm2
	por	%xmm2, %xmm0
	movdqa	%xmm0, %xmm2
	psrldq	$4, %xmm2
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm0, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	movd	%xmm1, %eax
	cmpq	%r9, %rsi
	je	.L747
.L745:
	movl	-4(%r8), %esi
	leaq	-4(%r8), %r9
	cmpl	%esi, %eax
	cmovl	%esi, %eax
	cmpl	$-2147483648, %esi
	adcl	$0, %edx
	cmpq	%r9, %rdi
	jnb	.L747
	movl	-8(%r8), %esi
	leaq	-8(%r8), %r9
	cmpl	%esi, %eax
	cmovl	%esi, %eax
	cmpl	$-2147483648, %esi
	adcl	$0, %edx
	cmpq	%r9, %rdi
	jnb	.L747
	movl	-12(%r8), %esi
	leaq	-12(%r8), %r9
	cmpl	%esi, %eax
	cmovl	%esi, %eax
	cmpl	$-2147483648, %esi
	adcl	$0, %edx
	cmpq	%rdi, %r9
	jbe	.L747
	movl	-16(%r8), %esi
	cmpl	%esi, %eax
	cmovl	%esi, %eax
	cmpl	$-2147483648, %esi
	adcl	$0, %edx
.L747:
	addl	$1, %eax
	cmpl	%edx, %eax
	jg	.L772
	.p2align 4,,10
	.p2align 3
.L755:
	movslq	-4(%r12), %rax
	subq	$4, %r12
	subl	$1, %ebx
	testl	%eax, %eax
	js	.L756
	movl	%ebx, (%rcx,%rax,4)
.L756:
	testl	%ebx, %ebx
	jne	.L755
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L772:
	.cfi_restore_state
	cltq
	movq	%rcx, %rdi
	movl	$255, %esi
	leaq	0(,%rax,4), %rdx
	call	memset@PLT
	movq	%rax, %rcx
	jmp	.L755
.L771:
	movq	%r12, %r8
	xorl	%edx, %edx
	movl	$-1, %eax
	jmp	.L745
	.cfi_endproc
.LFE2112:
	.size	ubidi_invertMap_67, .-ubidi_invertMap_67
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	-1
	.long	-2
	.long	-3
	.align 16
.LC2:
	.long	-4
	.long	-4
	.long	-4
	.long	-4
	.align 16
.LC3:
	.long	-8234
	.long	-8234
	.long	-8234
	.long	-8234
	.align 16
.LC4:
	.long	8204
	.long	8204
	.long	8204
	.long	8204
	.align 16
.LC5:
	.long	-8294
	.long	-8294
	.long	-8294
	.long	-8294
	.align 16
.LC6:
	.long	3
	.long	3
	.long	3
	.long	3
	.align 16
.LC7:
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.align 16
.LC8:
	.long	4
	.long	4
	.long	4
	.long	4
	.align 16
.LC11:
	.long	0
	.long	1
	.long	2
	.long	3
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
