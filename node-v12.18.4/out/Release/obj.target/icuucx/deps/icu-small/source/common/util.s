	.file	"util.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii
	.type	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii, @function
_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii:
.LFB1301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-2(%rdx), %eax
	cmpl	$34, %eax
	ja	.L19
	leaq	-58(%rbp), %rax
	movl	%esi, %r12d
	movl	%edx, %r13d
	movl	%ecx, %r14d
	movq	%rax, -72(%rbp)
	testl	%esi, %esi
	js	.L20
.L4:
	movl	%r12d, %eax
	movl	$1, %ebx
	cmpl	%r13d, %r12d
	jl	.L8
	.p2align 4,,10
	.p2align 3
.L5:
	cltd
	imull	%r13d, %ebx
	subl	$1, %r14d
	idivl	%r13d
	cmpl	%eax, %r13d
	jle	.L5
.L8:
	subl	$1, %r14d
	testl	%r14d, %r14d
	jle	.L7
	.p2align 4,,10
	.p2align 3
.L6:
	movq	-72(%rbp), %rsi
	movl	$48, %eax
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	subl	$1, %r14d
	jne	.L6
.L7:
	leaq	_ZL6DIGITS(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%r12d, %eax
	movq	-72(%rbp), %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	cltd
	idivl	%ebx
	cltq
	movl	%edx, %r12d
	xorl	%edx, %edx
	movzwl	(%r14,%rax,2), %eax
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	%ebx, %eax
	cltd
	idivl	%r13d
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L9
.L3:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L21
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$63, %ecx
	leaq	-58(%rbp), %rsi
	xorl	%edx, %edx
	movw	%cx, -58(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%rax, %r15
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$45, %edx
	movl	$1, %ecx
	movq	%rax, %rsi
	negl	%r12d
	movw	%dx, -58(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L4
.L21:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1301:
	.size	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii, .-_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility13isUnprintableEi
	.type	_ZN6icu_6711ICU_Utility13isUnprintableEi, @function
_ZN6icu_6711ICU_Utility13isUnprintableEi:
.LFB1302:
	.cfi_startproc
	endbr64
	subl	$32, %edi
	cmpl	$94, %edi
	seta	%al
	ret
	.cfi_endproc
.LFE1302:
	.size	_ZN6icu_6711ICU_Utility13isUnprintableEi, .-_ZN6icu_6711ICU_Utility13isUnprintableEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi
	.type	_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi, @function
_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi:
.LFB1303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-32(%rsi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$94, %edx
	ja	.L30
.L23:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L31
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	$92, %ecx
	leaq	-42(%rbp), %r13
	movl	%esi, %ebx
	xorl	%edx, %edx
	movw	%cx, -42(%rbp)
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%rdi, %r12
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testl	$-65536, %ebx
	jne	.L32
	movl	$117, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	leaq	_ZL6DIGITS(%rip), %r14
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L26:
	movl	%ebx, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	sarl	$12, %eax
	movq	%r12, %rdi
	andl	$15, %eax
	movzwl	(%r14,%rax,2), %eax
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	%ebx, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	sarl	$8, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	andl	$15, %eax
	movzwl	(%r14,%rax,2), %eax
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	%ebx, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	sarl	$4, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	andl	$15, %ebx
	andl	$15, %eax
	movzwl	(%r14,%rax,2), %eax
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	(%r14,%rbx,2), %eax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%r12, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$1, %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$85, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	movw	%dx, -42(%rbp)
	xorl	%edx, %edx
	leaq	_ZL6DIGITS(%rip), %r14
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	%ebx, %eax
	xorl	%edx, %edx
	movq	%r13, %rsi
	shrl	$28, %eax
	movq	%r12, %rdi
	movl	$1, %ecx
	movzwl	(%r14,%rax,2), %eax
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	%ebx, %eax
	xorl	%edx, %edx
	movq	%r13, %rsi
	sarl	$24, %eax
	movq	%r12, %rdi
	movl	$1, %ecx
	andl	$15, %eax
	movzwl	(%r14,%rax,2), %eax
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	%ebx, %eax
	xorl	%edx, %edx
	movq	%r13, %rsi
	sarl	$20, %eax
	movq	%r12, %rdi
	movl	$1, %ecx
	andl	$15, %eax
	movzwl	(%r14,%rax,2), %eax
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	%ebx, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	sarl	$16, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	andl	$15, %eax
	movzwl	(%r14,%rax,2), %eax
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L26
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1303:
	.size	_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi, .-_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia
	.type	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia, @function
_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia:
.LFB1304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movslq	(%rsi), %rax
	movswl	8(%rdi), %esi
	testb	$17, %sil
	jne	.L39
	leaq	10(%rdi), %r13
	testb	$2, %sil
	je	.L41
.L34:
	testw	%si, %si
	js	.L36
.L42:
	sarl	$5, %esi
.L37:
	subl	%eax, %esi
	leaq	0(%r13,%rax,2), %rdi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	subq	%r13, %rax
	sarq	%rax
	testb	%r12b, %r12b
	je	.L33
	movl	%eax, (%rbx)
.L33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	24(%rdi), %r13
	testw	%si, %si
	jns	.L42
.L36:
	movl	12(%rdi), %esi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%r13d, %r13d
	jmp	.L34
	.cfi_endproc
.LFE1304:
	.size	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia, .-_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs
	.type	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs, @function
_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs:
.LFB1305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	movl	(%rsi), %r14d
	movswl	8(%rdi), %esi
	testb	$17, %sil
	jne	.L54
	leaq	10(%rdi), %r15
	testb	$2, %sil
	je	.L59
.L44:
	testw	%si, %si
	js	.L46
.L62:
	sarl	$5, %esi
.L47:
	movslq	%r14d, %rax
	subl	%r14d, %esi
	leaq	(%r15,%rax,2), %rdi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	subq	%r15, %rax
	sarq	%rax
	movl	%eax, 0(%r13)
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L48
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L49:
	xorl	%r8d, %r8d
	cmpl	%eax, %ecx
	je	.L50
	movl	$-1, %ecx
	jbe	.L51
	andl	$2, %edx
	jne	.L60
	movq	24(%r12), %r12
.L53:
	movslq	%eax, %rdx
	movzwl	(%r12,%rdx,2), %ecx
.L51:
	xorl	%r8d, %r8d
	cmpw	%cx, %bx
	je	.L61
.L50:
	movl	%r14d, 0(%r13)
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	24(%rdi), %r15
	testw	%si, %si
	jns	.L62
.L46:
	movl	12(%r12), %esi
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L61:
	leal	1(%rax), %r14d
	movl	$1, %r8d
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L60:
	addq	$10, %r12
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L48:
	movl	12(%r12), %ecx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%r15d, %r15d
	jmp	.L44
	.cfi_endproc
.LFE1305:
	.size	_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs, .-_ZN6icu_6711ICU_Utility9parseCharERKNS_13UnicodeStringERiDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringERKNS_11ReplaceableEii
	.type	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringERKNS_11ReplaceableEii, @function
_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringERKNS_11ReplaceableEii:
.LFB1306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movswl	8(%rdi), %eax
	movl	%ecx, -52(%rbp)
	testw	%ax, %ax
	js	.L64
	sarl	$5, %eax
.L65:
	testl	%eax, %eax
	jne	.L92
.L84:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %ebx
	cmpl	-52(%rbp), %r15d
	jl	.L67
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$1, %eax
	addl	$1, %r15d
	addl	%eax, %r12d
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L78
.L90:
	sarl	$5, %eax
.L79:
	cmpl	%eax, %r12d
	je	.L84
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %ebx
	cmpl	%r15d, -52(%rbp)
	jle	.L68
.L67:
	movq	(%r14), %rax
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	*80(%rax)
	cmpl	$126, %ebx
	je	.L93
	cmpl	%ebx, %eax
	jne	.L68
	cmpl	$65535, %ebx
	jbe	.L76
	movl	$2, %eax
	addl	$2, %r15d
	addl	%eax, %r12d
	movswl	8(%r13), %eax
	testw	%ax, %ax
	jns	.L90
.L78:
	movl	12(%r13), %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L93:
	movl	%eax, %edi
	movl	%eax, -56(%rbp)
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	movl	-56(%rbp), %edx
	testb	%al, %al
	jne	.L94
	movswl	8(%r13), %eax
	addl	$1, %r12d
	testw	%ax, %ax
	jns	.L90
	movl	12(%r13), %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L94:
	xorl	%eax, %eax
	cmpl	$65535, %edx
	seta	%al
	leal	1(%rax,%r15), %r15d
	cmpl	%r15d, -52(%rbp)
	jg	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$-1, %r15d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L64:
	movl	12(%rdi), %eax
	jmp	.L65
	.cfi_endproc
.LFE1306:
	.size	_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringERKNS_11ReplaceableEii, .-_ZN6icu_6711ICU_Utility12parsePatternERKNS_13UnicodeStringERKNS_11ReplaceableEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility17parseAsciiIntegerERKNS_13UnicodeStringERi
	.type	_ZN6icu_6711ICU_Utility17parseAsciiIntegerERKNS_13UnicodeStringERi, @function
_ZN6icu_6711ICU_Utility17parseAsciiIntegerERKNS_13UnicodeStringERi:
.LFB1307:
	.cfi_startproc
	endbr64
	movslq	(%rsi), %r9
	xorl	%r8d, %r8d
	leaq	10(%rdi), %r10
	movq	%r9, %rax
	addq	%r9, %r9
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L103:
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%eax, %edx
	jle	.L95
.L104:
	cmpl	%edx, %eax
	jnb	.L95
	andl	$2, %ecx
	movq	%r10, %rdx
	jne	.L100
	movq	24(%rdi), %rdx
.L100:
	movzwl	(%rdx,%r9), %edx
	addq	$2, %r9
	leal	-48(%rdx), %ecx
	cmpw	$9, %cx
	ja	.L95
	addl	$1, %eax
	leal	(%r8,%r8,4), %ecx
	movl	%eax, (%rsi)
	leal	-48(%rdx,%rcx,2), %r8d
.L102:
	movzwl	8(%rdi), %ecx
	testw	%cx, %cx
	jns	.L103
	movl	12(%rdi), %edx
	cmpl	%eax, %edx
	jg	.L104
.L95:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE1307:
	.size	_ZN6icu_6711ICU_Utility17parseAsciiIntegerERKNS_13UnicodeStringERi, .-_ZN6icu_6711ICU_Utility17parseAsciiIntegerERKNS_13UnicodeStringERi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_
	.type	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_, @function
_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_:
.LFB1308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	%ecx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L106
	testb	%cl, %cl
	jne	.L195
.L107:
	movswl	8(%r12), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	jns	.L151
	movl	12(%r12), %eax
.L151:
	testl	%eax, %eax
	jne	.L152
	cmpl	$39, %r13d
	je	.L162
	cmpl	$92, %r13d
	je	.L162
.L153:
	leal	-33(%r13), %eax
	cmpl	$93, %eax
	ja	.L156
	movl	%r13d, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L156
	leal	-48(%r13), %eax
	cmpl	$9, %eax
	ja	.L155
.L156:
	movl	%r13d, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L155
	.p2align 4,,10
	.p2align 3
.L194:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L105:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	leal	-32(%rsi), %eax
	cmpl	$94, %eax
	jbe	.L107
.L106:
	movswl	8(%r12), %edx
	movl	%edx, %eax
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L197
	leaq	10(%r12), %r14
	testl	%edx, %edx
	jg	.L113
.L137:
	cmpl	$-1, %r13d
	je	.L105
.L114:
	cmpl	$32, %r13d
	je	.L198
	cmpb	$0, -68(%rbp)
	je	.L194
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi
	testb	%al, %al
	jne	.L105
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L199:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	$1, %edx
	jle	.L161
.L200:
	movq	%r14, %rdx
	testb	$2, %al
	jne	.L120
	movq	24(%r12), %rdx
.L120:
	cmpw	$39, (%rdx)
	jne	.L161
	cmpw	$39, 2(%rdx)
	jne	.L161
	leaq	-58(%rbp), %r15
	movl	$92, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$1, %ecx
	movq	%r15, %rsi
	movw	%r8w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$39, %r9d
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movl	$1, %ecx
	movw	%r9w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$2, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	8(%r12), %eax
.L113:
	testw	%ax, %ax
	jns	.L199
	movl	12(%r12), %edx
	cmpl	$1, %edx
	jg	.L200
	.p2align 4,,10
	.p2align 3
.L161:
	xorl	%r14d, %r14d
	leaq	10(%r12), %r15
	testw	%ax, %ax
	js	.L121
.L203:
	movswl	%ax, %edx
	sarl	$5, %edx
.L122:
	cmpl	$1, %edx
	jle	.L123
	leal	-2(%rdx), %ecx
	jbe	.L124
	movq	%r15, %rdi
	testb	$2, %al
	jne	.L126
	movq	24(%r12), %rdi
.L126:
	movslq	%ecx, %rsi
	cmpw	$39, (%rdi,%rsi,2)
	leaq	(%rsi,%rsi), %r8
	jne	.L124
	testl	%edx, %edx
	je	.L124
	cmpw	$39, 2(%rdi,%r8)
	jne	.L124
	testl	%ecx, %ecx
	jne	.L129
	testb	$1, %al
	jne	.L201
.L129:
	cmpl	$1023, %ecx
	jle	.L202
	orl	$-32, %eax
	movl	%ecx, 12(%r12)
	movw	%ax, 8(%r12)
.L130:
	addl	$1, %r14d
	testw	%ax, %ax
	jns	.L203
.L121:
	movl	12(%r12), %edx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L197:
	movl	12(%r12), %edx
	leaq	10(%r12), %r14
	testl	%edx, %edx
	jg	.L113
	jmp	.L137
.L123:
	je	.L124
.L128:
	leal	-1(%r14), %r12d
	leaq	-58(%rbp), %r15
	testl	%r14d, %r14d
	je	.L137
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$92, %edx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movw	%dx, -58(%rbp)
	xorl	%edx, %edx
	subl	$1, %r12d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$39, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movw	%cx, -58(%rbp)
	movq	%rax, %rdi
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	cmpl	$-1, %r12d
	jne	.L136
	cmpl	$-1, %r13d
	jne	.L114
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L152:
	jle	.L153
.L155:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	$39, %r13d
	jne	.L105
	movl	$39, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L202:
	andl	$31, %eax
	sall	$5, %ecx
	orl	%ecx, %eax
	movw	%ax, 8(%r12)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$39, %edi
	leaq	-58(%rbp), %r15
	movl	$1, %ecx
	xorl	%edx, %edx
	movw	%di, -58(%rbp)
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L133
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L134:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$39, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movw	%si, -58(%rbp)
	movl	$1, %ecx
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L135
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L198:
	movzwl	8(%rbx), %edx
	testw	%dx, %dx
	js	.L143
	movswl	%dx, %eax
	sarl	$5, %eax
.L144:
	testl	%eax, %eax
	jle	.L105
	leal	-1(%rax), %ecx
	je	.L145
	andl	$2, %edx
	leaq	10(%rbx), %rax
	jne	.L147
	movq	24(%rbx), %rax
.L147:
	movslq	%ecx, %rcx
	cmpw	$32, (%rax,%rcx,2)
	je	.L105
.L145:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L135:
	testw	%ax, %ax
	js	.L138
	movswl	%ax, %edx
	sarl	$5, %edx
.L139:
	testl	%edx, %edx
	je	.L128
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L133:
	movl	12(%r12), %ecx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$92, %eax
	leaq	-58(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L143:
	movl	12(%rbx), %eax
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L201:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	8(%r12), %eax
	jmp	.L130
.L138:
	movl	12(%r12), %edx
	jmp	.L139
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1308:
	.size	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_, .-_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringERKS1_aaS2_
	.type	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringERKS1_aaS2_, @function
_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringERKS1_aaS2_:
.LFB1309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movsbl	%cl, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$24, %rsp
	movb	%dl, -49(%rbp)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L214:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%ebx, %edx
	jle	.L204
.L215:
	movsbl	-49(%rbp), %edx
	movl	$65535, %esi
	jbe	.L208
	testb	$2, %al
	je	.L209
	leaq	10(%r13), %rax
.L210:
	movzwl	(%rax,%rbx,2), %esi
.L208:
	movq	%r15, %r8
	movl	%r12d, %ecx
	movq	%r14, %rdi
	addq	$1, %rbx
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_
.L211:
	movzwl	8(%r13), %eax
	testw	%ax, %ax
	jns	.L214
	movl	12(%r13), %edx
	cmpl	%ebx, %edx
	jg	.L215
.L204:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	24(%r13), %rax
	jmp	.L210
	.cfi_endproc
.LFE1309:
	.size	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringERKS1_aaS2_, .-_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringERKS1_aaS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_
	.type	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_, @function
_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_:
.LFB1310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L216
	movsbl	%dl, %r12d
	leaq	-128(%rbp), %r15
	movq	%rdi, %r13
	movq	%rcx, %r14
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsi, %rdi
	movl	%r12d, %edx
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	movq	(%rsi), %rax
	movq	%r15, %rsi
	call	*24(%rax)
	xorl	%r9d, %r9d
	movq	%rax, %rbx
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L231:
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	%r9d, %eax
	jle	.L220
.L232:
	movl	$65535, %esi
	jbe	.L221
	andl	$2, %edx
	leaq	10(%rbx), %rax
	jne	.L223
	movq	24(%rbx), %rax
.L223:
	movzwl	(%rax,%r9,2), %esi
.L221:
	movq	%r14, %r8
	movl	%r12d, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r9, -136(%rbp)
	call	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEiaaS2_
	movq	-136(%rbp), %r9
	addq	$1, %r9
.L224:
	movzwl	8(%rbx), %edx
	testw	%dx, %dx
	jns	.L231
	movl	12(%rbx), %eax
	cmpl	%r9d, %eax
	jg	.L232
.L220:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L216:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L233
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L233:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1310:
	.size	_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_, .-_ZN6icu_6711ICU_Utility12appendToRuleERNS_13UnicodeStringEPKNS_14UnicodeMatcherEaS2_
	.section	.rodata
	.align 32
	.type	_ZL6DIGITS, @object
	.size	_ZL6DIGITS, 72
_ZL6DIGITS:
	.value	48
	.value	49
	.value	50
	.value	51
	.value	52
	.value	53
	.value	54
	.value	55
	.value	56
	.value	57
	.value	65
	.value	66
	.value	67
	.value	68
	.value	69
	.value	70
	.value	71
	.value	72
	.value	73
	.value	74
	.value	75
	.value	76
	.value	77
	.value	78
	.value	79
	.value	80
	.value	81
	.value	82
	.value	83
	.value	84
	.value	85
	.value	86
	.value	87
	.value	88
	.value	89
	.value	90
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
