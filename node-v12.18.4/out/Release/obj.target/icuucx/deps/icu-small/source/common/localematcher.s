	.file	"localematcher.cpp"
	.text
	.section	.text._ZNK6icu_6718LocalePriorityList8Iterator7hasNextEv,"axG",@progbits,_ZNK6icu_6718LocalePriorityList8Iterator7hasNextEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718LocalePriorityList8Iterator7hasNextEv
	.type	_ZNK6icu_6718LocalePriorityList8Iterator7hasNextEv, @function
_ZNK6icu_6718LocalePriorityList8Iterator7hasNextEv:
.LFB2331:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	cmpl	%eax, 20(%rdi)
	setl	%al
	ret
	.cfi_endproc
.LFE2331:
	.size	_ZNK6icu_6718LocalePriorityList8Iterator7hasNextEv, .-_ZNK6icu_6718LocalePriorityList8Iterator7hasNextEv
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_17hashLSRE8UElement, @function
_ZN6icu_6712_GLOBAL__N_17hashLSRE8UElement:
.LFB2495:
	.cfi_startproc
	endbr64
	movl	40(%rdi), %eax
	ret
	.cfi_endproc
.LFE2495:
	.size	_ZN6icu_6712_GLOBAL__N_17hashLSRE8UElement, .-_ZN6icu_6712_GLOBAL__N_17hashLSRE8UElement
	.align 2
	.p2align 4
	.type	_ZNK6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEE7hasNextEv, @function
_ZNK6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEE7hasNextEv:
.LFB3212:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE3212:
	.size	_ZNK6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEE7hasNextEv, .-_ZNK6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEE7hasNextEv
	.section	.text._ZN6icu_6718LocalePriorityList8Iterator4nextEv,"axG",@progbits,_ZN6icu_6718LocalePriorityList8Iterator4nextEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6718LocalePriorityList8Iterator4nextEv
	.type	_ZN6icu_6718LocalePriorityList8Iterator4nextEv, @function
_ZN6icu_6718LocalePriorityList8Iterator4nextEv:
.LFB2332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L6:
	movl	16(%rbx), %esi
	movq	8(%rbx), %rdi
	leal	1(%rsi), %eax
	movl	%eax, 16(%rbx)
	call	_ZNK6icu_6718LocalePriorityList8localeAtEi@PLT
	testq	%rax, %rax
	je	.L6
	addl	$1, 20(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2332:
	.size	_ZN6icu_6718LocalePriorityList8Iterator4nextEv, .-_ZN6icu_6718LocalePriorityList8Iterator4nextEv
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_111compareLSRsE8UElementS1_, @function
_ZN6icu_6712_GLOBAL__N_111compareLSRsE8UElementS1_:
.LFB2496:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_673LSReqERKS0_@PLT
	.cfi_endproc
.LFE2496:
	.size	_ZN6icu_6712_GLOBAL__N_111compareLSRsE8UElementS1_, .-_ZN6icu_6712_GLOBAL__N_111compareLSRsE8UElementS1_
	.section	.text._ZN6icu_6718LocalePriorityList8IteratorD2Ev,"axG",@progbits,_ZN6icu_6718LocalePriorityList8IteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6718LocalePriorityList8IteratorD2Ev
	.type	_ZN6icu_6718LocalePriorityList8IteratorD2Ev, @function
_ZN6icu_6718LocalePriorityList8IteratorD2Ev:
.LFB3209:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6718LocalePriorityList8IteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_676Locale8IteratorD2Ev@PLT
	.cfi_endproc
.LFE3209:
	.size	_ZN6icu_6718LocalePriorityList8IteratorD2Ev, .-_ZN6icu_6718LocalePriorityList8IteratorD2Ev
	.weak	_ZN6icu_6718LocalePriorityList8IteratorD1Ev
	.set	_ZN6icu_6718LocalePriorityList8IteratorD1Ev,_ZN6icu_6718LocalePriorityList8IteratorD2Ev
	.section	.text._ZN6icu_6718LocalePriorityList8IteratorD0Ev,"axG",@progbits,_ZN6icu_6718LocalePriorityList8IteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6718LocalePriorityList8IteratorD0Ev
	.type	_ZN6icu_6718LocalePriorityList8IteratorD0Ev, @function
_ZN6icu_6718LocalePriorityList8IteratorD0Ev:
.LFB3211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718LocalePriorityList8IteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_676Locale8IteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE3211:
	.size	_ZN6icu_6718LocalePriorityList8IteratorD0Ev, .-_ZN6icu_6718LocalePriorityList8IteratorD0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode.constprop.0, @function
_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode.constprop.0:
.LFB3241:
	.cfi_startproc
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %eax
	cmpq	$0, 56(%rdi)
	leal	0(,%rax,8), %r13d
	je	.L17
	movq	%rsi, %rdi
	call	_ZN6icu_673LSR11setHashCodeEv@PLT
	movq	56(%rbx), %rdi
	movq	%r12, %rsi
	call	uhash_geti_67@PLT
	testl	%eax, %eax
	jne	.L27
.L17:
	movl	28(%rbx), %eax
	subq	$8, %rsp
	movl	80(%rbx), %ecx
	movl	%r13d, %r8d
	movq	64(%rbx), %rdx
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	pushq	%rax
	movl	24(%rbx), %r9d
	call	_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	js	.L28
	movq	72(%rbx), %rdx
	sarl	$10, %eax
	cltq
	movl	(%rdx,%rax,4), %eax
.L14:
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	leaq	-24(%rbp), %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	subl	$1, %eax
	jmp	.L14
.L26:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3241:
	.size	_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode.constprop.0, .-_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode.constprop.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher6ResultC2EOS1_
	.type	_ZN6icu_6713LocaleMatcher6ResultC2EOS1_, @function
_ZN6icu_6713LocaleMatcher6ResultC2EOS1_:
.LFB2470:
	.cfi_startproc
	endbr64
	movzbl	24(%rsi), %eax
	movdqu	(%rsi), %xmm0
	movq	16(%rsi), %rdx
	movb	%al, 24(%rdi)
	movq	%rdx, 16(%rdi)
	movups	%xmm0, (%rdi)
	testb	%al, %al
	je	.L29
	movq	$0, (%rsi)
	movl	$-1, 16(%rsi)
	movb	$0, 24(%rsi)
.L29:
	ret
	.cfi_endproc
.LFE2470:
	.size	_ZN6icu_6713LocaleMatcher6ResultC2EOS1_, .-_ZN6icu_6713LocaleMatcher6ResultC2EOS1_
	.globl	_ZN6icu_6713LocaleMatcher6ResultC1EOS1_
	.set	_ZN6icu_6713LocaleMatcher6ResultC1EOS1_,_ZN6icu_6713LocaleMatcher6ResultC2EOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher6ResultD2Ev
	.type	_ZN6icu_6713LocaleMatcher6ResultD2Ev, @function
_ZN6icu_6713LocaleMatcher6ResultD2Ev:
.LFB2473:
	.cfi_startproc
	endbr64
	cmpb	$0, 24(%rdi)
	je	.L34
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L34
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.cfi_endproc
.LFE2473:
	.size	_ZN6icu_6713LocaleMatcher6ResultD2Ev, .-_ZN6icu_6713LocaleMatcher6ResultD2Ev
	.globl	_ZN6icu_6713LocaleMatcher6ResultD1Ev
	.set	_ZN6icu_6713LocaleMatcher6ResultD1Ev,_ZN6icu_6713LocaleMatcher6ResultD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher6ResultaSEOS1_
	.type	_ZN6icu_6713LocaleMatcher6ResultaSEOS1_, @function
_ZN6icu_6713LocaleMatcher6ResultaSEOS1_:
.LFB2475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 24(%rdi)
	movq	%rsi, %rbx
	je	.L40
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L40
	movq	(%rdi), %rax
	call	*8(%rax)
.L40:
	movl	16(%rbx), %eax
	movdqu	(%rbx), %xmm0
	movl	%eax, 16(%r12)
	movl	20(%rbx), %eax
	movups	%xmm0, (%r12)
	movl	%eax, 20(%r12)
	movzbl	24(%rbx), %eax
	movb	%al, 24(%r12)
	testb	%al, %al
	je	.L41
	movq	$0, (%rbx)
	movl	$-1, 16(%rbx)
	movb	$0, 24(%rbx)
.L41:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2475:
	.size	_ZN6icu_6713LocaleMatcher6ResultaSEOS1_, .-_ZN6icu_6713LocaleMatcher6ResultaSEOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7BuilderC2EOS1_
	.type	_ZN6icu_6713LocaleMatcher7BuilderC2EOS1_, @function
_ZN6icu_6713LocaleMatcher7BuilderC2EOS1_:
.LFB2478:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movq	8(%rsi), %rdx
	movq	$0, 8(%rsi)
	movl	%eax, (%rdi)
	movq	24(%rsi), %rax
	movq	%rdx, 8(%rdi)
	movq	16(%rsi), %rdx
	movq	%rax, 24(%rdi)
	movq	32(%rsi), %rax
	movq	%rdx, 16(%rdi)
	movq	%rax, 32(%rdi)
	movq	$0, 24(%rsi)
	ret
	.cfi_endproc
.LFE2478:
	.size	_ZN6icu_6713LocaleMatcher7BuilderC2EOS1_, .-_ZN6icu_6713LocaleMatcher7BuilderC2EOS1_
	.globl	_ZN6icu_6713LocaleMatcher7BuilderC1EOS1_
	.set	_ZN6icu_6713LocaleMatcher7BuilderC1EOS1_,_ZN6icu_6713LocaleMatcher7BuilderC2EOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7BuilderD2Ev
	.type	_ZN6icu_6713LocaleMatcher7BuilderD2Ev, @function
_ZN6icu_6713LocaleMatcher7BuilderD2Ev:
.LFB2481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L51
	movq	(%rdi), %rax
	call	*8(%rax)
.L51:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2481:
	.size	_ZN6icu_6713LocaleMatcher7BuilderD2Ev, .-_ZN6icu_6713LocaleMatcher7BuilderD2Ev
	.globl	_ZN6icu_6713LocaleMatcher7BuilderD1Ev
	.set	_ZN6icu_6713LocaleMatcher7BuilderD1Ev,_ZN6icu_6713LocaleMatcher7BuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7BuilderaSEOS1_
	.type	_ZN6icu_6713LocaleMatcher7BuilderaSEOS1_, @function
_ZN6icu_6713LocaleMatcher7BuilderaSEOS1_:
.LFB2483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdi), %rdi
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L58
	movq	(%rdi), %rax
	call	*8(%rax)
.L58:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L59
	movq	(%rdi), %rax
	call	*8(%rax)
.L59:
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movl	16(%rbx), %eax
	movl	%eax, 16(%r12)
	movl	20(%rbx), %eax
	movl	%eax, 20(%r12)
	movq	24(%rbx), %rax
	movq	%rax, 24(%r12)
	movl	32(%rbx), %eax
	movl	%eax, 32(%r12)
	movl	36(%rbx), %eax
	movl	%eax, 36(%r12)
	movq	%r12, %rax
	movq	$0, 8(%rbx)
	movq	$0, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2483:
	.size	_ZN6icu_6713LocaleMatcher7BuilderaSEOS1_, .-_ZN6icu_6713LocaleMatcher7BuilderaSEOS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7Builder21clearSupportedLocalesEv
	.type	_ZN6icu_6713LocaleMatcher7Builder21clearSupportedLocalesEv, @function
_ZN6icu_6713LocaleMatcher7Builder21clearSupportedLocalesEv:
.LFB2484:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L67
	jmp	_ZN6icu_677UVector17removeAllElementsEv@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	ret
	.cfi_endproc
.LFE2484:
	.size	_ZN6icu_6713LocaleMatcher7Builder21clearSupportedLocalesEv, .-_ZN6icu_6713LocaleMatcher7Builder21clearSupportedLocalesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7Builder27ensureSupportedLocaleVectorEv
	.type	_ZN6icu_6713LocaleMatcher7Builder27ensureSupportedLocaleVectorEv, @function
_ZN6icu_6713LocaleMatcher7Builder27ensureSupportedLocaleVectorEv:
.LFB2485:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	testl	%edx, %edx
	jg	.L81
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 8(%rdi)
	movq	%rdi, %rbx
	je	.L82
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L72
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	%r12, 8(%rbx)
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	setle	%al
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
.L72:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	cmpl	$0, (%rbx)
	movq	$0, 8(%rbx)
	jg	.L80
	movl	$7, (%rbx)
.L80:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2485:
	.size	_ZN6icu_6713LocaleMatcher7Builder27ensureSupportedLocaleVectorEv, .-_ZN6icu_6713LocaleMatcher7Builder27ensureSupportedLocaleVectorEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7Builder33setSupportedLocalesFromListStringENS_11StringPieceE
	.type	_ZN6icu_6713LocaleMatcher7Builder33setSupportedLocalesFromListStringENS_11StringPieceE, @function
_ZN6icu_6713LocaleMatcher7Builder33setSupportedLocalesFromListStringENS_11StringPieceE:
.LFB2486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6718LocalePriorityListC1ENS_11StringPieceER10UErrorCode@PLT
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L85
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L103
.L85:
	movq	%r14, %rdi
	call	_ZN6icu_6718LocalePriorityListD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	cmpq	$0, 8(%r13)
	je	.L86
.L88:
	movl	-88(%rbp), %ebx
	testl	%ebx, %ebx
	jle	.L85
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L91:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6718LocalePriorityList14orphanLocaleAtEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L90
	movq	8(%r13), %rdi
	movq	%rax, %rsi
	movq	%r13, %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L105
.L90:
	addl	$1, %r15d
	cmpl	%ebx, %r15d
	jne	.L91
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L89
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	xorl	%edx, %edx
	movq	%r13, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	0(%r13), %edx
	movq	%rbx, 8(%r13)
	testl	%edx, %edx
	jle	.L88
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L85
.L104:
	call	__stack_chk_fail@PLT
.L89:
	cmpl	$0, 0(%r13)
	movq	$0, 8(%r13)
	jg	.L85
	movl	$7, 0(%r13)
	jmp	.L85
	.cfi_endproc
.LFE2486:
	.size	_ZN6icu_6713LocaleMatcher7Builder33setSupportedLocalesFromListStringENS_11StringPieceE, .-_ZN6icu_6713LocaleMatcher7Builder33setSupportedLocalesFromListStringENS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7Builder19setSupportedLocalesERNS_6Locale8IteratorE
	.type	_ZN6icu_6713LocaleMatcher7Builder19setSupportedLocalesERNS_6Locale8IteratorE, @function
_ZN6icu_6713LocaleMatcher7Builder19setSupportedLocalesERNS_6Locale8IteratorE:
.LFB2487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rdi), %edi
	testl	%edi, %edi
	jg	.L118
	movq	8(%r12), %rdi
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L109
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L128
.L118:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	cmpq	$0, 8(%r12)
	jne	.L114
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L130:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	%rax, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L113
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	movq	%r12, %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L129
.L114:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	testb	%al, %al
	jne	.L130
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L112
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	xorl	%edx, %edx
	movq	%r12, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%r13, 8(%r12)
	testl	%edx, %edx
	jle	.L114
	jmp	.L118
.L112:
	movq	$0, 8(%r12)
	cmpl	$0, (%r12)
	jg	.L118
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$7, (%r12)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L129:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L118
	.cfi_endproc
.LFE2487:
	.size	_ZN6icu_6713LocaleMatcher7Builder19setSupportedLocalesERNS_6Locale8IteratorE, .-_ZN6icu_6713LocaleMatcher7Builder19setSupportedLocalesERNS_6Locale8IteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7Builder18addSupportedLocaleERKNS_6LocaleE
	.type	_ZN6icu_6713LocaleMatcher7Builder18addSupportedLocaleERKNS_6LocaleE, @function
_ZN6icu_6713LocaleMatcher7Builder18addSupportedLocaleERKNS_6LocaleE:
.LFB2488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L139
	cmpq	$0, 8(%rdi)
	movq	%rsi, %r13
	je	.L143
.L134:
	movq	%r13, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L137
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	movq	%r12, %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L139
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L139:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L135
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	xorl	%edx, %edx
	movq	%r12, %rcx
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%rbx, 8(%r12)
	testl	%edx, %edx
	jle	.L134
	jmp	.L139
.L135:
	movq	$0, 8(%r12)
	cmpl	$0, (%r12)
	jg	.L139
.L137:
	movl	$7, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2488:
	.size	_ZN6icu_6713LocaleMatcher7Builder18addSupportedLocaleERKNS_6LocaleE, .-_ZN6icu_6713LocaleMatcher7Builder18addSupportedLocaleERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7Builder16setDefaultLocaleEPKNS_6LocaleE
	.type	_ZN6icu_6713LocaleMatcher7Builder16setDefaultLocaleEPKNS_6LocaleE, @function
_ZN6icu_6713LocaleMatcher7Builder16setDefaultLocaleEPKNS_6LocaleE:
.LFB2489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	(%rdi), %eax
	movq	%rdi, %r12
	testl	%eax, %eax
	jg	.L145
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L146
	movq	%rsi, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L155
.L146:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L147
	movq	(%rdi), %rax
	call	*8(%rax)
.L147:
	movq	%r13, 24(%r12)
.L145:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L145
	.cfi_endproc
.LFE2489:
	.size	_ZN6icu_6713LocaleMatcher7Builder16setDefaultLocaleEPKNS_6LocaleE, .-_ZN6icu_6713LocaleMatcher7Builder16setDefaultLocaleEPKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7Builder14setFavorSubtagE20ULocMatchFavorSubtag
	.type	_ZN6icu_6713LocaleMatcher7Builder14setFavorSubtagE20ULocMatchFavorSubtag, @function
_ZN6icu_6713LocaleMatcher7Builder14setFavorSubtagE20ULocMatchFavorSubtag:
.LFB2490:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	jg	.L157
	movl	%esi, 32(%rdi)
.L157:
	ret
	.cfi_endproc
.LFE2490:
	.size	_ZN6icu_6713LocaleMatcher7Builder14setFavorSubtagE20ULocMatchFavorSubtag, .-_ZN6icu_6713LocaleMatcher7Builder14setFavorSubtagE20ULocMatchFavorSubtag
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher7Builder27setDemotionPerDesiredLocaleE17ULocMatchDemotion
	.type	_ZN6icu_6713LocaleMatcher7Builder27setDemotionPerDesiredLocaleE17ULocMatchDemotion, @function
_ZN6icu_6713LocaleMatcher7Builder27setDemotionPerDesiredLocaleE17ULocMatchDemotion:
.LFB2491:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	jg	.L159
	movl	%esi, 20(%rdi)
.L159:
	ret
	.cfi_endproc
.LFE2491:
	.size	_ZN6icu_6713LocaleMatcher7Builder27setDemotionPerDesiredLocaleE17ULocMatchDemotion, .-_ZN6icu_6713LocaleMatcher7Builder27setDemotionPerDesiredLocaleE17ULocMatchDemotion
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleMatcher7Builder11copyErrorToER10UErrorCode
	.type	_ZNK6icu_6713LocaleMatcher7Builder11copyErrorToER10UErrorCode, @function
_ZNK6icu_6713LocaleMatcher7Builder11copyErrorToER10UErrorCode:
.LFB2492:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jg	.L160
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L160
	movl	%edx, (%rsi)
	movl	$1, %eax
.L160:
	ret
	.cfi_endproc
.LFE2492:
	.size	_ZNK6icu_6713LocaleMatcher7Builder11copyErrorToER10UErrorCode, .-_ZNK6icu_6713LocaleMatcher7Builder11copyErrorToER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcher11putIfAbsentERKNS_3LSREiiR10UErrorCode
	.type	_ZN6icu_6713LocaleMatcher11putIfAbsentERKNS_3LSREiiR10UErrorCode, @function
_ZN6icu_6713LocaleMatcher11putIfAbsentERKNS_3LSREiiR10UErrorCode:
.LFB2497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	movl	(%r8), %ecx
	testl	%ecx, %ecx
	jle	.L165
.L167:
	movl	%ebx, %eax
.L164:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	56(%rdi), %rdi
	movq	%rsi, %r14
	movl	%edx, %r15d
	movq	%r8, %r12
	call	uhash_geti_67@PLT
	testl	%eax, %eax
	jne	.L167
	movq	56(%r13), %rdi
	leal	1(%r15), %edx
	movq	%r12, %rcx
	movq	%r14, %rsi
	call	uhash_puti_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L167
	movq	64(%r13), %rax
	movq	72(%r13), %rcx
	movslq	%ebx, %rdx
	movq	%r14, (%rax,%rdx,8)
	leal	1(%rbx), %eax
	movl	%r15d, (%rcx,%rdx,4)
	jmp	.L164
	.cfi_endproc
.LFE2497:
	.size	_ZN6icu_6713LocaleMatcher11putIfAbsentERKNS_3LSREiiR10UErrorCode, .-_ZN6icu_6713LocaleMatcher11putIfAbsentERKNS_3LSREiiR10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"und"
.LC1:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcherC2ERKNS0_7BuilderER10UErrorCode
	.type	_ZN6icu_6713LocaleMatcherC2ERKNS0_7BuilderER10UErrorCode, @function
_ZN6icu_6713LocaleMatcherC2ERKNS0_7BuilderER10UErrorCode:
.LFB2499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6714XLikelySubtags12getSingletonER10UErrorCode@PLT
	movq	%rbx, %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_6714LocaleDistance12getSingletonER10UErrorCode@PLT
	movq	32(%r12), %rcx
	movl	16(%r12), %edx
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r14)
	movq	%rcx, 24(%r14)
	movl	(%rbx), %ecx
	movl	%edx, 16(%r14)
	movl	$0, 20(%r14)
	movl	$0, 48(%r14)
	movq	$0, 72(%r14)
	movl	$0, 80(%r14)
	movups	%xmm0, 32(%r14)
	movups	%xmm0, 56(%r14)
	movups	%xmm0, 88(%r14)
	testl	%ecx, %ecx
	jg	.L169
	testl	%edx, %edx
	js	.L250
.L172:
	leaq	.LC0(%rip), %rax
	movq	24(%r12), %r15
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	%rax, %xmm1
	leaq	.LC1(%rip), %rax
	movhps	.LC2(%rip), %xmm1
	movq	%rax, -256(%rbp)
	movl	$0, -232(%rbp)
	movaps	%xmm1, -288(%rbp)
	movaps	%xmm1, -272(%rbp)
	testq	%r15, %r15
	je	.L228
	movq	%r15, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, 88(%r14)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L188
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L176
	cmpb	$0, 216(%r15)
	jne	.L176
	movq	40(%r15), %rax
	cmpb	$0, (%rax)
	jne	.L177
.L176:
	leaq	.LC1(%rip), %rax
	movdqa	-288(%rbp), %xmm4
	movq	$0, -200(%rbp)
	leaq	-224(%rbp), %r13
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	movaps	%xmm4, -224(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, -188(%rbp)
	movl	%eax, -192(%rbp)
.L178:
	leaq	-272(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rdx, %rdi
	movq	%rdx, -296(%rbp)
	call	_ZN6icu_673LSRaSEOS0_@PLT
	cmpq	$0, -200(%rbp)
	movq	-296(%rbp), %rdx
	je	.L179
	movq	%r13, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movq	-296(%rbp), %rdx
.L179:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L221
	movq	8(%r12), %rax
	movq	%rdx, -312(%rbp)
	testq	%rax, %rax
	jne	.L180
.L253:
	movl	$0, 48(%r14)
.L181:
	cmpl	$1, 20(%r12)
	movq	%r15, 96(%r14)
	je	.L251
.L221:
	cmpq	$0, -248(%rbp)
	je	.L169
	leaq	-272(%rbp), %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
.L169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L252
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	$0, -312(%rbp)
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L253
.L180:
	movslq	8(%rax), %rdi
	movl	%edi, 48(%r14)
	testl	%edi, %edi
	jle	.L181
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movslq	48(%r14), %r13
	movq	$-1, %rdi
	movq	%rax, 32(%r14)
	movabsq	$192153584101141162, %rax
	cmpq	%rax, %r13
	ja	.L182
	leaq	0(%r13,%r13,2), %rdi
	salq	$4, %rdi
	addq	$8, %rdi
.L182:
	call	_ZN6icu_677UMemorynaEm@PLT
	testq	%rax, %rax
	je	.L183
	movq	%r13, %rdx
	movq	%r13, (%rax)
	leaq	8(%rax), %rcx
	subq	$1, %rdx
	js	.L184
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L185:
	movdqa	-288(%rbp), %xmm2
	subq	$1, %rdx
	leaq	.LC1(%rip), %rsi
	movq	$0, 24(%rax)
	movq	%rsi, 16(%rax)
	addq	$48, %rax
	movups	%xmm2, -48(%rax)
	movl	$0, -16(%rax)
	movl	$0, -12(%rax)
	movl	$0, -8(%rax)
	cmpq	$-1, %rdx
	jne	.L185
.L184:
	movq	32(%r14), %rdi
	movq	%rcx, 40(%r14)
	testq	%rdi, %rdi
	je	.L188
	movslq	48(%r14), %rdx
	xorl	%esi, %esi
	salq	$3, %rdx
	call	memset@PLT
	movl	48(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L187
	leaq	-224(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -304(%rbp)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	.LC1(%rip), %rax
	movdqa	-288(%rbp), %xmm3
	movq	$0, -200(%rbp)
	movq	%rax, %rdi
	movq	%rax, -208(%rbp)
	movaps	%xmm3, -224(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, -188(%rbp)
	movl	%eax, -192(%rbp)
.L191:
	movq	-304(%rbp), %rsi
	leaq	0(%r13,%r13,2), %rdi
	salq	$4, %rdi
	addq	40(%r14), %rdi
	call	_ZN6icu_673LSRaSEOS0_@PLT
	cmpq	$0, -200(%rbp)
	movq	%rax, %r8
	je	.L192
	movq	-304(%rbp), %rdi
	movq	%rax, -296(%rbp)
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movq	-296(%rbp), %r8
.L192:
	movq	%r8, %rdi
	call	_ZN6icu_673LSR11setHashCodeEv@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L221
	movl	48(%r14), %ecx
	addq	$1, %r13
	cmpl	%r13d, %ecx
	jle	.L187
.L194:
	movq	8(%r12), %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rdi
	movq	32(%r14), %rax
	leaq	(%rax,%r13,8), %rdx
	movq	%rdx, -296(%rbp)
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	-296(%rbp), %rdx
	movq	%rax, (%rdx)
	movq	32(%r14), %rax
	movq	(%rax,%r13,8), %rdx
	testq	%rdx, %rdx
	je	.L188
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L189
	cmpb	$0, 216(%rdx)
	jne	.L189
	movq	40(%rdx), %rax
	cmpb	$0, (%rax)
	je	.L189
	movq	(%r14), %rsi
	movq	-304(%rbp), %rdi
	movq	%rbx, %rcx
	call	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L250:
	movl	72(%rax), %eax
	movl	%eax, 16(%r14)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L251:
	movq	8(%r14), %rax
	movl	84(%rax), %eax
	movl	%eax, 20(%r14)
	jmp	.L221
.L183:
	movq	$0, 40(%r14)
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$7, (%rbx)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	-224(%rbp), %r13
	movq	(%r14), %rsi
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L187:
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rdx
	movq	%rbx, %r8
	leaq	_ZN6icu_6712_GLOBAL__N_111compareLSRsE8UElementS1_(%rip), %rsi
	leaq	_ZN6icu_6712_GLOBAL__N_17hashLSRE8UElement(%rip), %rdi
	call	uhash_openSize_67@PLT
	movl	(%rbx), %r13d
	movq	%rax, 56(%r14)
	testl	%r13d, %r13d
	jg	.L221
	movslq	48(%r14), %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movslq	48(%r14), %rdi
	movq	%rax, 64(%r14)
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	cmpq	$0, 64(%r14)
	movq	%rax, 72(%r14)
	je	.L188
	testq	%rax, %rax
	je	.L188
	movl	48(%r14), %r13d
	leaq	-163(%rbp), %rax
	movl	$100, -168(%rbp)
	movq	%rax, -176(%rbp)
	movb	$0, -164(%rbp)
	cmpl	$100, %r13d
	jg	.L254
.L226:
	movl	$0, -316(%rbp)
	movq	-176(%rbp), %rdi
	xorl	%r10d, %r10d
	movl	$0, -304(%rbp)
	testl	%r13d, %r13d
	jle	.L202
	.p2align 4,,10
	.p2align 3
.L201:
	leaq	(%r10,%r10,2), %r8
	movl	%r10d, %r13d
	salq	$4, %r8
	addq	40(%r14), %r8
	cmpq	$0, -312(%rbp)
	je	.L255
	movq	-312(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r10, -296(%rbp)
	movq	%r8, -288(%rbp)
	call	_ZNK6icu_673LSR14isEquivalentToERKS0_@PLT
	movq	-288(%rbp), %r8
	movq	-296(%rbp), %r10
	testb	%al, %al
	je	.L207
	movq	-176(%rbp), %rax
	movb	$1, (%rax,%r10)
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L256
.L209:
	movq	-176(%rbp), %rdi
.L200:
	cmpb	$0, -164(%rbp)
	je	.L221
	call	uprv_free_67@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L207:
	movq	8(%r14), %rdi
	movq	%r8, %rsi
	movq	%r10, -288(%rbp)
	call	_ZNK6icu_6714LocaleDistance13isParadigmLSRERKNS_3LSRE@PLT
	movq	-288(%rbp), %r10
	testb	%al, %al
	movq	-176(%rbp), %rax
	je	.L208
	addl	$1, -316(%rbp)
	movb	$2, (%rax,%r10)
.L205:
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L209
	addl	$1, %r13d
.L225:
	movl	48(%r14), %eax
	addq	$1, %r10
	cmpl	%r13d, %eax
	jg	.L201
	movl	-316(%rbp), %r8d
	movl	-304(%rbp), %ecx
	movq	-176(%rbp), %rdi
	addl	%ecx, %r8d
	testl	%eax, %eax
	jle	.L202
	cmpl	%ecx, %r8d
	jle	.L216
	xorl	%r13d, %r13d
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L213:
	addq	$1, %r13
	cmpl	%eax, %edx
	jge	.L216
	cmpl	%r8d, -304(%rbp)
	jge	.L216
.L210:
	cmpb	$2, (%rdi,%r13)
	leal	1(%r13), %edx
	jne	.L213
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L213
	movq	56(%r14), %rdi
	leaq	0(%r13,%r13,2), %rsi
	movl	%edx, -312(%rbp)
	salq	$4, %rsi
	addq	40(%r14), %rsi
	movl	%r8d, -296(%rbp)
	movq	%rsi, -288(%rbp)
	call	uhash_geti_67@PLT
	movq	-288(%rbp), %rsi
	movl	-296(%rbp), %r8d
	testl	%eax, %eax
	movl	-312(%rbp), %edx
	je	.L214
.L248:
	movl	48(%r14), %eax
	movq	-176(%rbp), %rdi
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L255:
	movq	32(%r14), %rax
	movq	(%rax,%r10,8), %r15
	movq	-176(%rbp), %rax
	movb	$1, (%rax,%r10)
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jg	.L209
	movq	56(%r14), %rdi
	movq	%r8, %rsi
	movq	%r10, -296(%rbp)
	movq	%r8, -288(%rbp)
	call	uhash_geti_67@PLT
	movq	-288(%rbp), %r8
	movq	-296(%rbp), %r10
	testl	%eax, %eax
	movq	%r8, -312(%rbp)
	jne	.L205
	movq	56(%r14), %rdi
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r8, %rsi
	movq	%r10, -288(%rbp)
	call	uhash_puti_67@PLT
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L209
	movslq	-304(%rbp), %rax
	movq	64(%r14), %rdx
	addl	$1, %r13d
	movq	-312(%rbp), %rcx
	movq	-288(%rbp), %r10
	movq	%rax, %rdi
	movq	%rcx, (%rdx,%rax,8)
	movq	72(%r14), %rdx
	addl	$1, %edi
	movl	%edi, -304(%rbp)
	movl	$0, (%rdx,%rax,4)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L208:
	movb	$3, (%rax,%r10)
	jmp	.L205
.L256:
	movq	56(%r14), %rdi
	movq	%r8, %rsi
	call	uhash_geti_67@PLT
	movq	-288(%rbp), %r8
	movq	-296(%rbp), %r10
	testl	%eax, %eax
	jne	.L205
	movq	56(%r14), %rdi
	addl	$1, %r13d
	movq	%r8, %rsi
	movq	%rbx, %rcx
	movl	%r13d, %edx
	call	uhash_puti_67@PLT
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L209
	movslq	-304(%rbp), %rax
	movq	64(%r14), %rdx
	movq	-288(%rbp), %r8
	movq	-296(%rbp), %r10
	movq	%rax, %rcx
	movq	%r8, (%rdx,%rax,8)
	movq	72(%r14), %rdx
	addl	$1, %ecx
	movl	%ecx, -304(%rbp)
	movl	%r10d, (%rdx,%rax,4)
	jmp	.L225
.L202:
	movl	-304(%rbp), %eax
	cmpb	$0, -164(%rbp)
	movl	%eax, 80(%r14)
	je	.L181
	call	uprv_free_67@PLT
	jmp	.L181
.L216:
	testl	%eax, %eax
	jle	.L202
	xorl	%r13d, %r13d
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L217:
	addq	$1, %r13
	cmpl	%r13d, %eax
	jle	.L202
.L212:
	cmpb	$3, (%rdi,%r13)
	leal	1(%r13), %edx
	jne	.L217
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L217
	movq	56(%r14), %rdi
	leaq	0(%r13,%r13,2), %rsi
	movl	%edx, -296(%rbp)
	salq	$4, %rsi
	addq	40(%r14), %rsi
	movq	%rsi, -288(%rbp)
	call	uhash_geti_67@PLT
	movq	-288(%rbp), %rsi
	movl	-296(%rbp), %edx
	testl	%eax, %eax
	je	.L218
.L249:
	movl	48(%r14), %eax
	movq	-176(%rbp), %rdi
	jmp	.L217
.L254:
	movslq	%r13d, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L197
	cmpb	$0, -164(%rbp)
	jne	.L257
.L198:
	movl	%r13d, -168(%rbp)
	movl	48(%r14), %r13d
	movq	%rax, -176(%rbp)
	movb	$1, -164(%rbp)
	jmp	.L226
.L197:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L199
	movl	$7, (%rbx)
	jmp	.L200
.L257:
	movq	-176(%rbp), %rdi
	movq	%rax, -288(%rbp)
	call	uprv_free_67@PLT
	movq	-288(%rbp), %rax
	jmp	.L198
.L214:
	movq	56(%r14), %rdi
	movq	%rbx, %rcx
	movl	%r8d, -312(%rbp)
	movl	%edx, -296(%rbp)
	movq	%rsi, -288(%rbp)
	call	uhash_puti_67@PLT
	cmpl	$0, (%rbx)
	movq	-288(%rbp), %rsi
	movl	-296(%rbp), %edx
	movl	-312(%rbp), %r8d
	jg	.L248
	movslq	-304(%rbp), %rax
	movq	64(%r14), %rcx
	movq	%rax, %rdi
	movq	%rsi, (%rcx,%rax,8)
	movq	72(%r14), %rcx
	addl	$1, %edi
	movl	%edi, -304(%rbp)
	movl	%r13d, (%rcx,%rax,4)
	jmp	.L248
.L218:
	movq	56(%r14), %rdi
	movq	%rbx, %rcx
	movq	%rsi, -288(%rbp)
	call	uhash_puti_67@PLT
	cmpl	$0, (%rbx)
	movq	-288(%rbp), %rsi
	jg	.L249
	movslq	-304(%rbp), %rax
	movq	64(%r14), %rdx
	movq	%rax, %rcx
	movq	%rsi, (%rdx,%rax,8)
	movq	72(%r14), %rdx
	addl	$1, %ecx
	movl	%ecx, -304(%rbp)
	movl	%r13d, (%rdx,%rax,4)
	jmp	.L249
.L252:
	call	__stack_chk_fail@PLT
.L199:
	movl	48(%r14), %r13d
	jmp	.L226
	.cfi_endproc
.LFE2499:
	.size	_ZN6icu_6713LocaleMatcherC2ERKNS0_7BuilderER10UErrorCode, .-_ZN6icu_6713LocaleMatcherC2ERKNS0_7BuilderER10UErrorCode
	.globl	_ZN6icu_6713LocaleMatcherC1ERKNS0_7BuilderER10UErrorCode
	.set	_ZN6icu_6713LocaleMatcherC1ERKNS0_7BuilderER10UErrorCode,_ZN6icu_6713LocaleMatcherC2ERKNS0_7BuilderER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleMatcher7Builder5buildER10UErrorCode
	.type	_ZNK6icu_6713LocaleMatcher7Builder5buildER10UErrorCode, @function
_ZNK6icu_6713LocaleMatcher7Builder5buildER10UErrorCode:
.LFB2493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L261
.L259:
	movq	%r12, %rdi
	call	_ZN6icu_6713LocaleMatcherC1ERKNS0_7BuilderER10UErrorCode
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L259
	movl	%eax, (%rdx)
	jmp	.L259
	.cfi_endproc
.LFE2493:
	.size	_ZNK6icu_6713LocaleMatcher7Builder5buildER10UErrorCode, .-_ZNK6icu_6713LocaleMatcher7Builder5buildER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcherC2EOS0_
	.type	_ZN6icu_6713LocaleMatcherC2EOS0_, @function
_ZN6icu_6713LocaleMatcherC2EOS0_:
.LFB2502:
	.cfi_startproc
	endbr64
	movl	48(%rsi), %eax
	movq	16(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movl	$0, 48(%rsi)
	movdqu	32(%rsi), %xmm2
	movdqu	56(%rsi), %xmm3
	movups	%xmm0, 32(%rsi)
	movdqu	88(%rsi), %xmm4
	movdqu	(%rsi), %xmm1
	movl	%eax, 48(%rdi)
	movq	72(%rsi), %rax
	movq	%rdx, 16(%rdi)
	movq	24(%rsi), %rdx
	movq	$0, 72(%rsi)
	movq	%rax, 72(%rdi)
	movl	80(%rsi), %eax
	movq	%rdx, 24(%rdi)
	movl	%eax, 80(%rdi)
	movl	$0, 80(%rsi)
	movups	%xmm1, (%rdi)
	movups	%xmm2, 32(%rdi)
	movups	%xmm3, 56(%rdi)
	movups	%xmm4, 88(%rdi)
	movups	%xmm0, 56(%rsi)
	movups	%xmm0, 88(%rsi)
	ret
	.cfi_endproc
.LFE2502:
	.size	_ZN6icu_6713LocaleMatcherC2EOS0_, .-_ZN6icu_6713LocaleMatcherC2EOS0_
	.globl	_ZN6icu_6713LocaleMatcherC1EOS0_
	.set	_ZN6icu_6713LocaleMatcherC1EOS0_,_ZN6icu_6713LocaleMatcherC2EOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcherD2Ev
	.type	_ZN6icu_6713LocaleMatcherD2Ev, @function
_ZN6icu_6713LocaleMatcherD2Ev:
.LFB2505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	48(%rdi), %eax
	movq	32(%rdi), %r8
	testl	%eax, %eax
	jle	.L264
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L268:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L265
	movq	(%rdi), %rax
	addq	$1, %rbx
	call	*8(%rax)
	movl	48(%r12), %eax
	movq	32(%r12), %r8
	cmpl	%ebx, %eax
	jg	.L268
.L264:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L269
	movq	-8(%rdi), %rax
	leaq	(%rax,%rax,2), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	cmpq	%rbx, %rdi
	je	.L270
	.p2align 4,,10
	.p2align 3
.L274:
	subq	$48, %rbx
	cmpq	$0, 24(%rbx)
	je	.L271
	movq	%rbx, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movq	40(%r12), %rdi
	cmpq	%rdi, %rbx
	jne	.L274
.L270:
	subq	$8, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L269:
	movq	56(%r12), %rdi
	call	uhash_close_67@PLT
	movq	64(%r12), %rdi
	call	uprv_free_67@PLT
	movq	72(%r12), %rdi
	call	uprv_free_67@PLT
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L263
	movq	(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	cmpq	%rdi, %rbx
	jne	.L274
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L265:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L268
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L263:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2505:
	.size	_ZN6icu_6713LocaleMatcherD2Ev, .-_ZN6icu_6713LocaleMatcherD2Ev
	.globl	_ZN6icu_6713LocaleMatcherD1Ev
	.set	_ZN6icu_6713LocaleMatcherD1Ev,_ZN6icu_6713LocaleMatcherD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleMatcheraSEOS0_
	.type	_ZN6icu_6713LocaleMatcheraSEOS0_, @function
_ZN6icu_6713LocaleMatcheraSEOS0_:
.LFB2507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN6icu_6713LocaleMatcherD1Ev
	movl	16(%rbx), %eax
	pxor	%xmm0, %xmm0
	movl	%eax, 16(%r12)
	movl	20(%rbx), %eax
	movl	%eax, 20(%r12)
	movl	24(%rbx), %eax
	movl	%eax, 24(%r12)
	movl	28(%rbx), %eax
	movl	%eax, 28(%r12)
	movdqu	32(%rbx), %xmm1
	movups	%xmm1, 32(%r12)
	movl	48(%rbx), %eax
	movl	%eax, 48(%r12)
	movdqu	56(%rbx), %xmm2
	movups	%xmm2, 56(%r12)
	movq	72(%rbx), %rax
	movdqu	88(%rbx), %xmm3
	movq	%rax, 72(%r12)
	movl	80(%rbx), %eax
	movups	%xmm3, 88(%r12)
	movl	%eax, 80(%r12)
	movq	%r12, %rax
	movl	$0, 48(%rbx)
	movq	$0, 72(%rbx)
	movl	$0, 80(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 56(%rbx)
	movups	%xmm0, 88(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2507:
	.size	_ZN6icu_6713LocaleMatcheraSEOS0_, .-_ZN6icu_6713LocaleMatcheraSEOS0_
	.section	.text._ZN6icu_6717LocaleLsrIteratorD2Ev,"axG",@progbits,_ZN6icu_6717LocaleLsrIteratorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717LocaleLsrIteratorD2Ev
	.type	_ZN6icu_6717LocaleLsrIteratorD2Ev, @function
_ZN6icu_6717LocaleLsrIteratorD2Ev:
.LFB2512:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jne	.L283
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L283
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L283:
	ret
	.cfi_endproc
.LFE2512:
	.size	_ZN6icu_6717LocaleLsrIteratorD2Ev, .-_ZN6icu_6717LocaleLsrIteratorD2Ev
	.weak	_ZN6icu_6717LocaleLsrIteratorD1Ev
	.set	_ZN6icu_6717LocaleLsrIteratorD1Ev,_ZN6icu_6717LocaleLsrIteratorD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleMatcher12getBestMatchERKNS_6LocaleER10UErrorCode
	.type	_ZNK6icu_6713LocaleMatcher12getBestMatchERKNS_6LocaleER10UErrorCode, @function
_ZNK6icu_6713LocaleMatcher12getBestMatchERKNS_6LocaleER10UErrorCode:
.LFB2519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L288
	cmpb	$0, 216(%rsi)
	movq	%rdi, %r12
	movq	%rdx, %rbx
	je	.L301
.L290:
	leaq	.LC1(%rip), %rdi
	movq	$0, -72(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	movq	%rdi, -80(%rbp)
	leaq	-96(%rbp), %r14
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, -60(%rbp)
	movl	%eax, -64(%rbp)
.L292:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode.constprop.0
	cmpq	$0, -72(%rbp)
	movl	%eax, %r13d
	je	.L293
	movq	%r14, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
.L293:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L294
	testl	%r13d, %r13d
	jns	.L302
.L294:
	movq	96(%r12), %rax
.L288:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L303
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movq	40(%rsi), %rax
	cmpb	$0, (%rax)
	je	.L290
	leaq	-96(%rbp), %r14
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	(%rdi), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L302:
	movq	32(%r12), %rdx
	movslq	%r13d, %rax
	movq	(%rdx,%rax,8), %rax
	jmp	.L288
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2519:
	.size	_ZNK6icu_6713LocaleMatcher12getBestMatchERKNS_6LocaleER10UErrorCode, .-_ZNK6icu_6713LocaleMatcher12getBestMatchERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleMatcher18getBestMatchResultERKNS_6LocaleER10UErrorCode
	.type	_ZNK6icu_6713LocaleMatcher18getBestMatchResultERKNS_6LocaleER10UErrorCode, @function
_ZNK6icu_6713LocaleMatcher18getBestMatchResultERKNS_6LocaleER10UErrorCode:
.LFB2522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L305
.L311:
	movq	96(%r13), %rax
	movq	$0, (%r12)
	movq	$-1, 16(%r12)
	movq	%rax, 8(%r12)
	movb	$0, 24(%r12)
.L304:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	cmpb	$0, 216(%rdx)
	movq	%rdx, %r14
	movq	%rcx, %rbx
	je	.L315
.L307:
	leaq	.LC1(%rip), %rdi
	movq	$0, -88(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	movq	%rdi, -96(%rbp)
	leaq	-112(%rbp), %r15
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, -76(%rbp)
	movl	%eax, -80(%rbp)
.L309:
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode.constprop.0
	cmpq	$0, -88(%rbp)
	movl	%eax, %ecx
	je	.L310
	movq	%r15, %rdi
	movl	%eax, -116(%rbp)
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movl	-116(%rbp), %ecx
.L310:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L311
	testl	%ecx, %ecx
	js	.L311
	movq	32(%r13), %rax
	movslq	%ecx, %rdx
	movq	(%rax,%rdx,8), %rax
	movq	%r14, (%r12)
	movl	$0, 16(%r12)
	movq	%rax, 8(%r12)
	movl	%ecx, 20(%r12)
	movb	$0, 24(%r12)
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L315:
	movq	40(%rdx), %rax
	cmpb	$0, (%rax)
	je	.L307
	leaq	-112(%rbp), %r15
	movq	0(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L309
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2522:
	.size	_ZNK6icu_6713LocaleMatcher18getBestMatchResultERKNS_6LocaleER10UErrorCode, .-_ZNK6icu_6713LocaleMatcher18getBestMatchResultERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode
	.type	_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode, @function
_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode:
.LFB2524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r11d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jg	.L317
	movl	16(%rdi), %eax
	movl	$0, -124(%rbp)
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movl	$-1, -152(%rbp)
	movq	%rdx, %r12
	movq	%rcx, %r15
	leal	0(,%rax,8), %r13d
	leaq	.LC0(%rip), %rax
	movq	%rax, %xmm1
	leaq	-112(%rbp), %rax
	movhps	.LC2(%rip), %xmm1
	movq	%rax, -120(%rbp)
	movaps	%xmm1, -144(%rbp)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L359:
	movq	-120(%rbp), %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L317
.L335:
	addl	$1, -124(%rbp)
.L336:
	cmpq	$0, 56(%rbx)
	je	.L318
	movq	%r14, %rdi
	call	_ZN6icu_673LSR11setHashCodeEv@PLT
	movq	56(%rbx), %rdi
	movq	%r14, %rsi
	call	uhash_geti_67@PLT
	testl	%eax, %eax
	jne	.L357
.L318:
	movl	28(%rbx), %eax
	subq	$8, %rsp
	movq	64(%rbx), %rdx
	movl	%r13d, %r8d
	movl	80(%rbx), %ecx
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	pushq	%rax
	movl	24(%rbx), %r9d
	call	_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection@PLT
	popq	%r8
	popq	%r9
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L323
	movl	%eax, %r13d
	andl	$1023, %r13d
	testq	%r12, %r12
	je	.L325
	movl	(%r15), %edi
	testl	%edi, %edi
	jg	.L317
	movl	-124(%rbp), %eax
	cmpl	$1, 16(%r12)
	movl	%eax, 40(%r12)
	je	.L358
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L327
	movq	(%rdi), %rax
	movl	%edx, -152(%rbp)
	call	*8(%rax)
	movl	-152(%rbp), %edx
.L327:
	movl	$224, %edi
	movl	%edx, -128(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L328
	movq	24(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	(%r15), %esi
	movq	-152(%rbp), %rax
	movl	-128(%rbp), %edx
	testl	%esi, %esi
	movq	%rax, 32(%r12)
	jg	.L317
.L325:
	sarl	$10, %edx
	movl	%edx, -152(%rbp)
.L323:
	movl	20(%rbx), %eax
	sall	$3, %eax
	subl	%eax, %r13d
	testq	%r12, %r12
	je	.L330
	testl	%r13d, %r13d
	jle	.L330
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	testb	%al, %al
	je	.L330
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	(%r15), %ecx
	movq	%rax, 24(%r12)
	testl	%ecx, %ecx
	jg	.L331
	cmpb	$0, 216(%rax)
	jne	.L331
	movq	40(%rax), %rdx
	cmpb	$0, (%rdx)
	jne	.L332
.L331:
	leaq	.LC1(%rip), %rax
	movdqa	-144(%rbp), %xmm0
	movq	$0, -88(%rbp)
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, -76(%rbp)
	movl	%eax, -80(%rbp)
.L333:
	movq	-120(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_673LSRaSEOS0_@PLT
	cmpq	$0, -88(%rbp)
	jne	.L359
	movl	(%r15), %eax
	testl	%eax, %eax
	jle	.L335
	.p2align 4,,10
	.p2align 3
.L317:
	movl	$-1, %r13d
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L332:
	movq	(%r12), %rsi
	movq	-120(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rax, %rdx
	call	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L358:
	movq	24(%r12), %rax
	movq	%rax, 32(%r12)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L330:
	movslq	-152(%rbp), %rax
	cmpl	$-1, %eax
	je	.L317
	movq	72(%rbx), %rdx
	movl	(%rdx,%rax,4), %r13d
.L316:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L360
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	leal	-1(%rax), %r13d
	testq	%r12, %r12
	je	.L316
	movl	(%r15), %r10d
	testl	%r10d, %r10d
	jg	.L316
	movl	-124(%rbp), %eax
	cmpl	$1, 16(%r12)
	movl	%eax, 40(%r12)
	je	.L361
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L321
	movq	(%rdi), %rax
	call	*8(%rax)
.L321:
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L322
	movq	24(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	%rbx, 32(%r12)
	jmp	.L316
.L361:
	movq	24(%r12), %rax
	movq	%rax, 32(%r12)
	jmp	.L316
.L328:
	movq	$0, 32(%r12)
	movl	$7, (%r15)
	jmp	.L317
.L322:
	movq	$0, 32(%r12)
	movl	$7, (%r15)
	jmp	.L316
.L360:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2524:
	.size	_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode, .-_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode.part.0, @function
_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode.part.0:
.LFB3234:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	%rsi, -152(%rbp)
	movl	$0, -144(%rbp)
	movq	%rax, -160(%rbp)
	movq	(%rsi), %rax
	movl	$-1, -120(%rbp)
	movups	%xmm0, -136(%rbp)
	call	*24(%rax)
	movl	(%rbx), %edx
	movq	%rax, -136(%rbp)
	testl	%edx, %edx
	jg	.L363
	cmpb	$0, 216(%rax)
	je	.L374
.L363:
	leaq	.LC1(%rip), %rdi
	movq	$0, -88(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	movq	%rdi, -96(%rbp)
	leaq	-112(%rbp), %r15
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, -76(%rbp)
	movl	%eax, -80(%rbp)
.L365:
	leaq	-160(%rbp), %r14
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode
	cmpq	$0, -88(%rbp)
	movl	%eax, %r13d
	je	.L366
	movq	%r15, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
.L366:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L367
	testl	%r13d, %r13d
	jns	.L375
.L367:
	movq	96(%r12), %r12
.L368:
	movq	%r14, %rdi
	call	_ZN6icu_6717LocaleLsrIteratorD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L376
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movq	40(%rax), %rdx
	cmpb	$0, (%rdx)
	je	.L363
	leaq	-112(%rbp), %r15
	movq	-160(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%rax, %rdx
	movq	%r15, %rdi
	call	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L375:
	movq	32(%r12), %rdx
	movslq	%r13d, %rax
	movq	(%rdx,%rax,8), %r12
	jmp	.L368
.L376:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3234:
	.size	_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode.part.0, .-_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode
	.type	_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode, @function
_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode:
.LFB2520:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L380
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rax
	call	*16(%rax)
	testb	%al, %al
	jne	.L379
	movq	96(%r14), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L380:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2520:
	.size	_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode, .-_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleMatcher25getBestMatchForListStringENS_11StringPieceER10UErrorCode
	.type	_ZNK6icu_6713LocaleMatcher25getBestMatchForListStringENS_11StringPieceER10UErrorCode, @function
_ZNK6icu_6713LocaleMatcher25getBestMatchForListStringENS_11StringPieceER10UErrorCode:
.LFB2521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	movq	%r13, %xmm1
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6718LocalePriorityList8IteratorE(%rip), %rbx
	movq	%rbx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -144(%rbp)
	call	_ZN6icu_6718LocalePriorityListC1ENS_11StringPieceER10UErrorCode@PLT
	movdqa	-144(%rbp), %xmm0
	movl	-120(%rbp), %eax
	movq	$0, -80(%rbp)
	movl	(%r12), %edx
	subl	-116(%rbp), %eax
	movl	%eax, -72(%rbp)
	movaps	%xmm0, -96(%rbp)
	testl	%edx, %edx
	jg	.L389
	testl	%eax, %eax
	jg	.L387
	movq	96(%r14), %r12
	leaq	-96(%rbp), %r15
.L386:
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	call	_ZN6icu_676Locale8IteratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6718LocalePriorityListD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L391
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK6icu_6713LocaleMatcher12getBestMatchERNS_6Locale8IteratorER10UErrorCode.part.0
	movq	%rax, %r12
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L389:
	xorl	%r12d, %r12d
	leaq	-96(%rbp), %r15
	jmp	.L386
.L391:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2521:
	.size	_ZNK6icu_6713LocaleMatcher25getBestMatchForListStringENS_11StringPieceER10UErrorCode, .-_ZNK6icu_6713LocaleMatcher25getBestMatchForListStringENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleMatcher18getBestMatchResultERNS_6Locale8IteratorER10UErrorCode
	.type	_ZNK6icu_6713LocaleMatcher18getBestMatchResultERNS_6Locale8IteratorER10UErrorCode, @function
_ZNK6icu_6713LocaleMatcher18getBestMatchResultERNS_6Locale8IteratorER10UErrorCode:
.LFB2523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L393
.L395:
	movq	96(%r14), %rax
	movq	$0, (%r12)
	movq	$-1, 16(%r12)
	movq	%rax, 8(%r12)
	movb	$0, 24(%r12)
.L392:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L407
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movq	(%rdx), %rax
	movq	%rdx, %r13
	movq	%rcx, %rbx
	movq	%rdx, %rdi
	call	*16(%rax)
	testb	%al, %al
	je	.L395
	movq	(%r14), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%r13, -152(%rbp)
	movl	$0, -144(%rbp)
	movq	%rax, -160(%rbp)
	movq	0(%r13), %rax
	movl	$-1, -120(%rbp)
	movups	%xmm0, -136(%rbp)
	call	*24(%rax)
	movl	(%rbx), %edx
	movq	%rax, -136(%rbp)
	testl	%edx, %edx
	jg	.L396
	cmpb	$0, 216(%rax)
	je	.L408
.L396:
	leaq	.LC1(%rip), %rdi
	movq	$0, -88(%rbp)
	leaq	.LC0(%rip), %rax
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	movq	%rdi, -96(%rbp)
	leaq	-112(%rbp), %r13
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, -76(%rbp)
	movl	%eax, -80(%rbp)
.L398:
	leaq	-160(%rbp), %r15
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r15, %rdx
	call	_ZNK6icu_6713LocaleMatcher16getBestSuppIndexENS_3LSREPNS_17LocaleLsrIteratorER10UErrorCode
	cmpq	$0, -88(%rbp)
	movl	%eax, %ecx
	je	.L399
	movq	%r13, %rdi
	movl	%eax, -164(%rbp)
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movl	-164(%rbp), %ecx
.L399:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L400
	testl	%ecx, %ecx
	jns	.L409
.L400:
	movq	96(%r14), %rax
	movq	$0, (%r12)
	movq	$-1, 16(%r12)
	movq	%rax, 8(%r12)
	movb	$0, 24(%r12)
.L401:
	movq	%r15, %rdi
	call	_ZN6icu_6717LocaleLsrIteratorD1Ev
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L408:
	movq	40(%rax), %rdx
	cmpb	$0, (%rdx)
	je	.L396
	leaq	-112(%rbp), %r13
	movq	-160(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%rax, %rdx
	movq	%r13, %rdi
	call	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L409:
	movq	32(%r14), %rax
	movslq	%ecx, %rdx
	movq	-128(%rbp), %xmm0
	movq	(%rax,%rdx,8), %rax
	movq	$0, -128(%rbp)
	movl	%ecx, 20(%r12)
	movq	%rax, %xmm2
	movl	-120(%rbp), %eax
	movb	$1, 24(%r12)
	punpcklqdq	%xmm2, %xmm0
	movl	%eax, 16(%r12)
	movups	%xmm0, (%r12)
	jmp	.L401
.L407:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2523:
	.size	_ZNK6icu_6713LocaleMatcher18getBestMatchResultERNS_6Locale8IteratorER10UErrorCode, .-_ZNK6icu_6713LocaleMatcher18getBestMatchResultERNS_6Locale8IteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleMatcher13internalMatchERKNS_6LocaleES3_R10UErrorCode
	.type	_ZNK6icu_6713LocaleMatcher13internalMatchERKNS_6LocaleES3_R10UErrorCode, @function
_ZNK6icu_6713LocaleMatcher13internalMatchERKNS_6LocaleES3_R10UErrorCode:
.LFB2525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movl	(%rcx), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L411
	cmpb	$0, 216(%rdx)
	je	.L423
.L411:
	leaq	.LC1(%rip), %rdi
	leaq	.LC0(%rip), %rax
	movq	$0, -136(%rbp)
	movq	%rax, %xmm0
	movq	%rdi, %xmm2
	movq	%rdi, -144(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, -124(%rbp)
	movl	%eax, -128(%rbp)
.L413:
	movl	(%r12), %ecx
	pxor	%xmm0, %xmm0
	testl	%ecx, %ecx
	jle	.L424
.L414:
	cmpq	$0, -136(%rbp)
	je	.L410
	leaq	-160(%rbp), %rdi
	movsd	%xmm0, -184(%rbp)
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movsd	-184(%rbp), %xmm0
.L410:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L425
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	.cfi_restore_state
	movq	40(%rdx), %rax
	cmpb	$0, (%rax)
	je	.L411
	movq	(%rbx), %rsi
	leaq	-160(%rbp), %rdi
	call	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L424:
	leaq	-160(%rbp), %rax
	cmpb	$0, 216(%r13)
	movq	8(%rbx), %r10
	movl	28(%rbx), %r8d
	movl	24(%rbx), %r9d
	movq	%rax, -168(%rbp)
	movl	16(%rbx), %eax
	leal	0(,%rax,8), %r14d
	je	.L426
.L415:
	leaq	.LC1(%rip), %rdi
	leaq	.LC0(%rip), %rax
	movl	%r9d, -192(%rbp)
	movq	%rax, %xmm0
	movq	%rdi, %xmm4
	movq	%rdi, -96(%rbp)
	leaq	-112(%rbp), %r15
	punpcklqdq	%xmm4, %xmm0
	movl	%r8d, -188(%rbp)
	movq	%r10, -184(%rbp)
	movq	$0, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, -76(%rbp)
	movq	-184(%rbp), %r10
	movl	%eax, -80(%rbp)
	movl	-188(%rbp), %r8d
	movl	-192(%rbp), %r9d
.L417:
	subq	$8, %rsp
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r10, %rdi
	pushq	%r8
	leaq	-168(%rbp), %rdx
	movl	%r14d, %r8d
	call	_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection@PLT
	cmpq	$0, -88(%rbp)
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	je	.L418
	movq	%r15, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
.L418:
	andl	$1023, %r12d
	pxor	%xmm0, %xmm0
	movsd	.LC5(%rip), %xmm1
	cvtsi2sdl	%r12d, %xmm0
	mulsd	.LC4(%rip), %xmm0
	movapd	%xmm1, %xmm3
	subsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm0
	divsd	%xmm1, %xmm0
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L426:
	movq	40(%r13), %rax
	cmpb	$0, (%rax)
	je	.L415
	movq	(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movl	%r9d, -192(%rbp)
	movl	%r8d, -188(%rbp)
	movq	%r10, -184(%rbp)
	call	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode@PLT
	movl	-192(%rbp), %r9d
	movl	-188(%rbp), %r8d
	movq	-184(%rbp), %r10
	jmp	.L417
.L425:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2525:
	.size	_ZNK6icu_6713LocaleMatcher13internalMatchERKNS_6LocaleES3_R10UErrorCode, .-_ZNK6icu_6713LocaleMatcher13internalMatchERKNS_6LocaleES3_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleMatcher6Result18makeResolvedLocaleER10UErrorCode
	.type	_ZNK6icu_6713LocaleMatcher6Result18makeResolvedLocaleER10UErrorCode, @function
_ZNK6icu_6713LocaleMatcher6Result18makeResolvedLocaleER10UErrorCode:
.LFB2476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L428
	movq	8(%rsi), %rdi
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L428
	movq	(%rsi), %r14
	testq	%r14, %r14
	je	.L429
	movq	%r14, %rsi
	movq	%rdx, %r13
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	testb	%al, %al
	je	.L432
	movq	8(%rbx), %rdi
.L429:
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L428:
	call	_ZN6icu_676Locale7getRootEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
.L427:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L445
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	leaq	-112(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6713LocaleBuilderC1Ev@PLT
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713LocaleBuilder9setLocaleERKNS_6LocaleE@PLT
	cmpb	$0, 26(%r14)
	jne	.L446
.L434:
	movslq	32(%r14), %rsi
	addq	208(%r14), %rsi
	cmpb	$0, (%rsi)
	jne	.L447
.L435:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713LocaleBuilder18copyExtensionsFromERKNS_6LocaleER10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN6icu_6713LocaleBuilder5buildER10UErrorCode@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713LocaleBuilderD1Ev@PLT
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	26(%r14), %rsi
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6713LocaleBuilder9setRegionENS_11StringPieceE@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L447:
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6713LocaleBuilder10setVariantENS_11StringPieceE@PLT
	jmp	.L435
.L445:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2476:
	.size	_ZNK6icu_6713LocaleMatcher6Result18makeResolvedLocaleER10UErrorCode, .-_ZNK6icu_6713LocaleMatcher6Result18makeResolvedLocaleER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEE4nextEv, @function
_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEE4nextEv:
.LFB3213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-272(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	leaq	8(%rax), %rdx
	movq	(%rax), %rsi
	movq	%rdx, 8(%rdi)
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	leaq	24(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L451
	addq	$248, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L451:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3213:
	.size	_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEE4nextEv, .-_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEE4nextEv
	.p2align 4
	.type	_ZN12_GLOBAL__N_114acceptLanguageER12UEnumerationRN6icu_676Locale8IteratorEPciP13UAcceptResultR10UErrorCode.part.0, @function
_ZN12_GLOBAL__N_114acceptLanguageER12UEnumerationRN6icu_676Locale8IteratorEPciP13UAcceptResultR10UErrorCode.part.0:
.LFB3239:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-288(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$472, %rsp
	movq	%rdx, -496(%rbp)
	movl	%ecx, -484(%rbp)
	movq	%r8, -504(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movabsq	$8589934591, %rax
	movl	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L458:
	movq	%r12, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L461
.L497:
	movq	-440(%rbp), %rdi
	movq	%rax, %rsi
	leaq	-448(%rbp), %rdx
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	-448(%rbp), %esi
	testl	%esi, %esi
	jle	.L457
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
.L457:
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L462:
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	uenum_next_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L453
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	cmpb	$0, -72(%rbp)
	jne	.L496
	movl	-448(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L457
	cmpq	$0, -440(%rbp)
	jne	.L458
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L459
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	leaq	-448(%rbp), %rcx
	xorl	%edx, %edx
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	-448(%rbp), %edi
	movq	%r15, -440(%rbp)
	testl	%edi, %edi
	jg	.L457
	movq	%r12, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L497
	.p2align 4,,10
	.p2align 3
.L461:
	movl	$7, -448(%rbp)
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L453:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L498
.L463:
	leaq	-400(%rbp), %r12
	leaq	-448(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6713LocaleMatcherC1ERKNS0_7BuilderER10UErrorCode
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-480(%rbp), %rdi
	xorl	%r14d, %r14d
	call	_ZNK6icu_6713LocaleMatcher18getBestMatchResultERNS_6Locale8IteratorER10UErrorCode
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L464
	movl	-464(%rbp), %eax
	testl	%eax, %eax
	js	.L465
	movq	-504(%rbp), %rbx
	movq	-472(%rbp), %rsi
	testq	%rbx, %rbx
	je	.L466
	movq	-480(%rbp), %rdi
	call	_ZNK6icu_676LocaleeqERKS0_@PLT
	movq	-472(%rbp), %rsi
	testb	%al, %al
	sete	%al
	movzbl	%al, %eax
	addl	$1, %eax
	movl	%eax, (%rbx)
.L466:
	movq	40(%rsi), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rbx
	cmpl	-484(%rbp), %eax
	jle	.L499
.L468:
	movl	-484(%rbp), %esi
	movq	-496(%rbp), %rdi
	movq	%r13, %rcx
	movl	%ebx, %edx
	call	u_terminateChars_67@PLT
	movl	%eax, %r14d
.L464:
	cmpb	$0, -456(%rbp)
	jne	.L500
.L470:
	movq	%r12, %rdi
	call	_ZN6icu_6713LocaleMatcherD1Ev
.L455:
	movq	-440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L471
	movq	(%rdi), %rax
	call	*8(%rax)
.L471:
	movq	-424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L452
	movq	(%rdi), %rax
	call	*8(%rax)
.L452:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L501
	addq	$472, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	.cfi_restore_state
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L470
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L498:
	movl	-448(%rbp), %eax
	testl	%eax, %eax
	jle	.L463
	movl	%eax, 0(%r13)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L496:
	movl	$1, 0(%r13)
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L465:
	movq	-504(%rbp), %rax
	testq	%rax, %rax
	je	.L469
	movl	$0, (%rax)
.L469:
	movl	-484(%rbp), %esi
	movq	-496(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rcx
	call	u_terminateChars_67@PLT
	cmpb	$0, -456(%rbp)
	movl	%eax, %r14d
	je	.L470
	jmp	.L500
.L499:
	movq	-496(%rbp), %rdi
	movslq	%eax, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	jmp	.L468
.L459:
	cmpl	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	jle	.L461
	jmp	.L457
.L501:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3239:
	.size	_ZN12_GLOBAL__N_114acceptLanguageER12UEnumerationRN6icu_676Locale8IteratorEPciP13UAcceptResultR10UErrorCode.part.0, .-_ZN12_GLOBAL__N_114acceptLanguageER12UEnumerationRN6icu_676Locale8IteratorEPciP13UAcceptResultR10UErrorCode.part.0
	.p2align 4
	.globl	uloc_acceptLanguageFromHTTP_67
	.type	uloc_acceptLanguageFromHTTP_67, @function
uloc_acceptLanguageFromHTTP_67:
.LFB2538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movl	(%r9), %esi
	testl	%esi, %esi
	jg	.L502
	movq	%rdi, %r12
	movq	%rdx, %r10
	movq	%r8, %rax
	movq	%r9, %rbx
	testq	%rdi, %rdi
	je	.L514
	testl	%r13d, %r13d
	js	.L505
.L506:
	testq	%rcx, %rcx
	je	.L505
	testq	%rax, %rax
	je	.L505
	leaq	-128(%rbp), %r14
	leaq	-96(%rbp), %r11
	movq	%rcx, %rsi
	xorl	%r15d, %r15d
	movq	%r14, %xmm1
	movq	%r11, %rdi
	movq	.LC6(%rip), %xmm0
	movq	%rax, -168(%rbp)
	movq	%r11, -136(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%r10, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6718LocalePriorityListC1ENS_11StringPieceER10UErrorCode@PLT
	movl	(%rbx), %eax
	movl	-120(%rbp), %edx
	movq	$0, -80(%rbp)
	movdqa	-160(%rbp), %xmm0
	subl	-116(%rbp), %edx
	testl	%eax, %eax
	movl	%edx, -72(%rbp)
	movq	-136(%rbp), %r11
	movaps	%xmm0, -96(%rbp)
	jg	.L509
	movq	-144(%rbp), %r10
	movq	%r11, %rsi
	movq	%rbx, %r9
	movl	%r13d, %ecx
	movq	-168(%rbp), %rax
	movq	%r12, %rdx
	movq	%r10, %r8
	movq	%rax, %rdi
	call	_ZN12_GLOBAL__N_114acceptLanguageER12UEnumerationRN6icu_676Locale8IteratorEPciP13UAcceptResultR10UErrorCode.part.0
	movq	-136(%rbp), %r11
	movl	%eax, %r15d
.L509:
	leaq	16+_ZTVN6icu_6718LocalePriorityList8IteratorE(%rip), %rax
	movq	%r11, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN6icu_676Locale8IteratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6718LocalePriorityListD1Ev@PLT
.L502:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L515
	addq	$136, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	testl	%r13d, %r13d
	je	.L506
	.p2align 4,,10
	.p2align 3
.L505:
	movl	$1, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L502
.L515:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2538:
	.size	uloc_acceptLanguageFromHTTP_67, .-uloc_acceptLanguageFromHTTP_67
	.align 2
	.p2align 4
	.type	_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED2Ev, @function
_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED2Ev:
.LFB3205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -24(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_676Locale8IteratorD2Ev@PLT
	.cfi_endproc
.LFE3205:
	.size	_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED2Ev, .-_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED2Ev
	.set	_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED1Ev,_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED0Ev, @function
_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED0Ev:
.LFB3207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -24(%rdi)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676Locale8IteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3207:
	.size	_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED0Ev, .-_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED0Ev
	.p2align 4
	.globl	uloc_acceptLanguage_67
	.type	uloc_acceptLanguage_67, @function
uloc_acceptLanguage_67:
.LFB2531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$760, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r9
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %r11d
	testl	%r11d, %r11d
	jg	.L520
	movq	%rdi, %r12
	movl	%esi, %r13d
	movslq	%r8d, %r15
	testq	%rdi, %rdi
	je	.L533
	testl	%esi, %esi
	js	.L523
.L524:
	testq	%rcx, %rcx
	je	.L534
	testl	%r15d, %r15d
	js	.L523
.L526:
	testq	%r10, %r10
	je	.L523
	movq	%r9, -776(%rbp)
	leaq	-752(%rbp), %r14
	leaq	16+_ZTVN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE(%rip), %rbx
	movq	%rdx, -784(%rbp)
	movq	%r10, -792(%rbp)
	movq	%rcx, -768(%rbp)
	call	_ZN6icu_676Locale7getRootEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	leaq	-528(%rbp), %r8
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r8, -760(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	-768(%rbp), %rcx
	movq	-760(%rbp), %r8
	leaq	-280(%rbp), %r11
	movq	%r11, %rdi
	movq	%r11, -760(%rbp)
	leaq	(%rcx,%r15,8), %rax
	movq	%r8, %rsi
	movq	%rcx, -296(%rbp)
	leaq	-304(%rbp), %r15
	movq	%rax, -288(%rbp)
	movq	%r8, -768(%rbp)
	movq	%rbx, -304(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	-768(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-776(%rbp), %r9
	xorl	%eax, %eax
	movq	-760(%rbp), %r11
	movl	(%r9), %edx
	testl	%edx, %edx
	jg	.L528
	movq	-784(%rbp), %rdx
	movq	-792(%rbp), %r10
	movl	%r13d, %ecx
	movq	%r15, %rsi
	movq	%rdx, %r8
	movq	%r10, %rdi
	movq	%r12, %rdx
	call	_ZN12_GLOBAL__N_114acceptLanguageER12UEnumerationRN6icu_676Locale8IteratorEPciP13UAcceptResultR10UErrorCode.part.0
	movq	-760(%rbp), %r11
.L528:
	movq	%r11, %rdi
	movl	%eax, -760(%rbp)
	movq	%rbx, -304(%rbp)
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676Locale8IteratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movl	-760(%rbp), %eax
.L520:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L535
	addq	$760, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L533:
	.cfi_restore_state
	testl	%esi, %esi
	je	.L524
	.p2align 4,,10
	.p2align 3
.L523:
	movl	$1, (%r9)
	xorl	%eax, %eax
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L534:
	testl	%r15d, %r15d
	je	.L526
	jmp	.L523
.L535:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2531:
	.size	uloc_acceptLanguage_67, .-uloc_acceptLanguage_67
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6718LocalePriorityList8IteratorE
	.section	.rodata._ZTSN6icu_6718LocalePriorityList8IteratorE,"aG",@progbits,_ZTSN6icu_6718LocalePriorityList8IteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6718LocalePriorityList8IteratorE, @object
	.size	_ZTSN6icu_6718LocalePriorityList8IteratorE, 39
_ZTSN6icu_6718LocalePriorityList8IteratorE:
	.string	"N6icu_6718LocalePriorityList8IteratorE"
	.weak	_ZTIN6icu_6718LocalePriorityList8IteratorE
	.section	.data.rel.ro._ZTIN6icu_6718LocalePriorityList8IteratorE,"awG",@progbits,_ZTIN6icu_6718LocalePriorityList8IteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6718LocalePriorityList8IteratorE, @object
	.size	_ZTIN6icu_6718LocalePriorityList8IteratorE, 24
_ZTIN6icu_6718LocalePriorityList8IteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718LocalePriorityList8IteratorE
	.quad	_ZTIN6icu_676Locale8IteratorE
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE, @object
	.size	_ZTIN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE, 56
_ZTIN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE
	.long	0
	.long	2
	.quad	_ZTIN6icu_676Locale8IteratorE
	.quad	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.section	.rodata
	.align 32
	.type	_ZTSN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE, @object
	.size	_ZTSN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE, 75
_ZTSN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE:
	.string	"*N6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE"
	.weak	_ZTVN6icu_6718LocalePriorityList8IteratorE
	.section	.data.rel.ro.local._ZTVN6icu_6718LocalePriorityList8IteratorE,"awG",@progbits,_ZTVN6icu_6718LocalePriorityList8IteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6718LocalePriorityList8IteratorE, @object
	.size	_ZTVN6icu_6718LocalePriorityList8IteratorE, 48
_ZTVN6icu_6718LocalePriorityList8IteratorE:
	.quad	0
	.quad	_ZTIN6icu_6718LocalePriorityList8IteratorE
	.quad	_ZN6icu_6718LocalePriorityList8IteratorD1Ev
	.quad	_ZN6icu_6718LocalePriorityList8IteratorD0Ev
	.quad	_ZNK6icu_6718LocalePriorityList8Iterator7hasNextEv
	.quad	_ZN6icu_6718LocalePriorityList8Iterator4nextEv
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE, @object
	.size	_ZTVN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE, 48
_ZTVN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE:
	.quad	0
	.quad	_ZTIN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEEE
	.quad	_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED1Ev
	.quad	_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEED0Ev
	.quad	_ZNK6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEE7hasNextEv
	.quad	_ZN6icu_676Locale18ConvertingIteratorIPPKcN12_GLOBAL__N_113LocaleFromTagEE4nextEv
	.align 8
.LC2:
	.quad	.LC1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	1069547520
	.align 8
.LC5:
	.long	0
	.long	1079574528
	.section	.data.rel.ro.local
	.align 8
.LC6:
	.quad	_ZTVN6icu_6718LocalePriorityList8IteratorE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
