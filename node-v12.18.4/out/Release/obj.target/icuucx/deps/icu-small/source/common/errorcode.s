	.file	"errorcode.cpp"
	.text
	.section	.text._ZNK6icu_679ErrorCode13handleFailureEv,"axG",@progbits,_ZNK6icu_679ErrorCode13handleFailureEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_679ErrorCode13handleFailureEv
	.type	_ZNK6icu_679ErrorCode13handleFailureEv, @function
_ZNK6icu_679ErrorCode13handleFailureEv:
.LFB13:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE13:
	.size	_ZNK6icu_679ErrorCode13handleFailureEv, .-_ZNK6icu_679ErrorCode13handleFailureEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ErrorCodeD2Ev
	.type	_ZN6icu_679ErrorCodeD2Ev, @function
_ZN6icu_679ErrorCodeD2Ev:
.LFB15:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE15:
	.size	_ZN6icu_679ErrorCodeD2Ev, .-_ZN6icu_679ErrorCodeD2Ev
	.globl	_ZN6icu_679ErrorCodeD1Ev
	.set	_ZN6icu_679ErrorCodeD1Ev,_ZN6icu_679ErrorCodeD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ErrorCodeD0Ev
	.type	_ZN6icu_679ErrorCodeD0Ev, @function
_ZN6icu_679ErrorCodeD0Ev:
.LFB17:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE17:
	.size	_ZN6icu_679ErrorCodeD0Ev, .-_ZN6icu_679ErrorCodeD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679ErrorCode5resetEv
	.type	_ZN6icu_679ErrorCode5resetEv, @function
_ZN6icu_679ErrorCode5resetEv:
.LFB18:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	movl	$0, 8(%rdi)
	ret
	.cfi_endproc
.LFE18:
	.size	_ZN6icu_679ErrorCode5resetEv, .-_ZN6icu_679ErrorCode5resetEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ErrorCode13assertSuccessEv
	.type	_ZNK6icu_679ErrorCode13assertSuccessEv, @function
_ZNK6icu_679ErrorCode13assertSuccessEv:
.LFB19:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L6
	movq	(%rdi), %rax
	leaq	_ZNK6icu_679ErrorCode13handleFailureEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L8
.L6:
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	jmp	*%rax
	.cfi_endproc
.LFE19:
	.size	_ZNK6icu_679ErrorCode13assertSuccessEv, .-_ZNK6icu_679ErrorCode13assertSuccessEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679ErrorCode9errorNameEv
	.type	_ZNK6icu_679ErrorCode9errorNameEv, @function
_ZNK6icu_679ErrorCode9errorNameEv:
.LFB20:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edi
	jmp	u_errorName_67@PLT
	.cfi_endproc
.LFE20:
	.size	_ZNK6icu_679ErrorCode9errorNameEv, .-_ZNK6icu_679ErrorCode9errorNameEv
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_679ErrorCodeE
	.section	.rodata._ZTSN6icu_679ErrorCodeE,"aG",@progbits,_ZTSN6icu_679ErrorCodeE,comdat
	.align 16
	.type	_ZTSN6icu_679ErrorCodeE, @object
	.size	_ZTSN6icu_679ErrorCodeE, 20
_ZTSN6icu_679ErrorCodeE:
	.string	"N6icu_679ErrorCodeE"
	.weak	_ZTIN6icu_679ErrorCodeE
	.section	.data.rel.ro._ZTIN6icu_679ErrorCodeE,"awG",@progbits,_ZTIN6icu_679ErrorCodeE,comdat
	.align 8
	.type	_ZTIN6icu_679ErrorCodeE, @object
	.size	_ZTIN6icu_679ErrorCodeE, 24
_ZTIN6icu_679ErrorCodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_679ErrorCodeE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTVN6icu_679ErrorCodeE
	.section	.data.rel.ro.local._ZTVN6icu_679ErrorCodeE,"awG",@progbits,_ZTVN6icu_679ErrorCodeE,comdat
	.align 8
	.type	_ZTVN6icu_679ErrorCodeE, @object
	.size	_ZTVN6icu_679ErrorCodeE, 40
_ZTVN6icu_679ErrorCodeE:
	.quad	0
	.quad	_ZTIN6icu_679ErrorCodeE
	.quad	_ZN6icu_679ErrorCodeD1Ev
	.quad	_ZN6icu_679ErrorCodeD0Ev
	.quad	_ZNK6icu_679ErrorCode13handleFailureEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
