	.file	"locdispnames.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Languages"
	.text
	.p2align 4
	.type	_ZL19_getStringOrCopyKeyPKcS0_S0_S0_S0_S0_PDsiP10UErrorCode, @function
_ZL19_getStringOrCopyKeyPKcS0_S0_S0_S0_S0_PDsiP10UErrorCode:
.LFB2484:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movl	24(%rbp), %r15d
	movq	%rcx, -72(%rbp)
	movq	32(%rbp), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	testq	%r8, %r8
	je	.L26
	movl	$9, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L5
	xorl	%esi, %esi
	movq	%r8, %rdi
	movl	$10, %edx
	movq	%r11, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	strtol@PLT
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r10
	testq	%rax, %rax
	movq	-96(%rbp), %r11
	je	.L5
	movl	$2, (%r12)
.L6:
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%r15d, %esi
	movq	%rax, %rdi
	movl	%eax, -60(%rbp)
	call	uprv_min_67@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	u_charsToUChars_67@PLT
	movl	$-127, (%r12)
.L8:
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	movq	%r12, %rcx
	movl	%r15d, %esi
	call	u_terminateUChars_67@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L27
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	subq	$8, %rsp
	movq	-72(%rbp), %rcx
	movq	%r13, %rdx
	leaq	-60(%rbp), %r9
	pushq	%r12
	movq	%r11, %rsi
	movq	%r10, %rdi
	call	uloc_getTableStringWithFallback_67@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %r8
.L4:
	movl	(%r12), %eax
	movq	%r8, -72(%rbp)
	testl	%eax, %eax
	jg	.L6
	movl	-60(%rbp), %edi
	movl	%r15d, %esi
	call	uprv_min_67@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jle	.L8
	movq	-72(%rbp), %r8
	testq	%r8, %r8
	je	.L8
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	u_memcpy_67@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r12, %rdx
	movq	%r8, -72(%rbp)
	call	ures_open_67@PLT
	movl	(%r12), %esi
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
	testl	%esi, %esi
	jle	.L28
	testq	%rax, %rax
	je	.L6
.L9:
	movq	%r8, -72(%rbp)
	call	ures_close_67@PLT
	movq	-72(%rbp), %r8
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	-60(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%rax, -72(%rbp)
	call	ures_getStringByKey_67@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	jne	.L9
	jmp	.L4
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2484:
	.size	_ZL19_getStringOrCopyKeyPKcS0_S0_S0_S0_S0_PDsiP10UErrorCode, .-_ZL19_getStringOrCopyKeyPKcS0_S0_S0_S0_S0_PDsiP10UErrorCode
	.section	.rodata.str1.1
.LC1:
	.string	"icudt67l-region"
.LC2:
	.string	"icudt67l-lang"
	.text
	.p2align 4
	.type	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0, @function
_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0:
.LFB3276:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-688(%rbp), %r15
	movl	%ecx, %r14d
	leaq	-692(%rbp), %rcx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$680, %rsp
	movq	%rsi, -720(%rbp)
	movq	16(%rbp), %r13
	movq	%r15, %rsi
	movq	%rdx, -712(%rbp)
	movl	$628, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -692(%rbp)
	call	*%r8
	movl	-692(%rbp), %edx
	cmpl	$-124, %edx
	je	.L30
	testl	%edx, %edx
	jg	.L30
	testl	%eax, %eax
	jne	.L32
	cmpq	uloc_getLanguage_67@GOTPCREL(%rip), %rbx
	je	.L39
	movq	-712(%rbp), %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	movl	%r14d, %esi
	call	u_terminateUChars_67@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$1, 0(%r13)
	xorl	%eax, %eax
.L29:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L40
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	$6581877, (%r15)
.L32:
	leaq	_ZL11_kCountries(%rip), %rax
	movq	%r15, %r9
	movq	%r15, %r8
	movq	%r12, %rdx
	cmpq	%rax, %r12
	leaq	.LC1(%rip), %rdi
	leaq	.LC2(%rip), %rax
	movq	-720(%rbp), %rsi
	cmovne	%rax, %rdi
	subq	$8, %rsp
	xorl	%ecx, %ecx
	pushq	%r13
	pushq	%r14
	pushq	-712(%rbp)
	call	_ZL19_getStringOrCopyKeyPKcS0_S0_S0_S0_S0_PDsiP10UErrorCode
	addq	$32, %rsp
	jmp	.L29
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3276:
	.size	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0, .-_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	.section	.rodata.str1.1
.LC3:
	.string	"icudt67l-curr"
	.text
	.p2align 4
	.type	uloc_getDisplayKeywordValue_67.part.0, @function
uloc_getDisplayKeywordValue_67.part.0:
.LFB3279:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-688(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movq	%r15, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$696, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -720(%rbp)
	movl	$628, %ecx
	movl	%r8d, -712(%rbp)
	movq	%r9, %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -688(%rbp)
	call	uloc_getKeywordValue_67@PLT
	cmpl	$-124, (%r12)
	movl	%eax, -728(%rbp)
	jne	.L42
	movl	$15, (%r12)
.L42:
	leaq	_ZL10_kCurrency(%rip), %rsi
	movq	%r14, %rdi
	call	uprv_stricmp_67@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L43
	movq	%r13, %rsi
	movq	%r12, %rdx
	leaq	.LC3(%rip), %rdi
	movl	$0, -692(%rbp)
	call	ures_open_67@PLT
	xorl	%edx, %edx
	movq	%r12, %rcx
	leaq	_ZL12_kCurrencies(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getByKey_67@PLT
	xorl	%edx, %edx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	ures_getByKeyWithFallback_67@PLT
	leaq	-692(%rbp), %rdx
	movq	%r12, %rcx
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, -736(%rbp)
	call	ures_getStringByIndex_67@PLT
	movl	(%r12), %edx
	movq	-736(%rbp), %r11
	testl	%edx, %edx
	jle	.L44
	cmpl	$2, %edx
	je	.L65
.L45:
	testq	%r11, %r11
	je	.L49
	movq	%r11, %rdi
	call	ures_close_67@PLT
.L49:
	testq	%r14, %r14
	je	.L50
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L50:
	testq	%r13, %r13
	je	.L41
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L41:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	$-127, (%r12)
.L44:
	testq	%rax, %rax
	je	.L46
	movl	-692(%rbp), %ebx
	movl	-712(%rbp), %r15d
	cmpl	%r15d, %ebx
	jle	.L67
.L64:
	movl	$15, (%r12)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L43:
	movl	-712(%rbp), %eax
	subq	$8, %rsp
	movq	%r15, %r9
	movq	%r15, %r8
	pushq	%r12
	movq	%r14, %rcx
	leaq	_ZL7_kTypes(%rip), %rdx
	movq	%r13, %rsi
	pushq	%rax
	leaq	.LC2(%rip), %rdi
	pushq	-720(%rbp)
	call	_ZL19_getStringOrCopyKeyPKcS0_S0_S0_S0_S0_PDsiP10UErrorCode
	addq	$32, %rsp
	movl	%eax, %ebx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L67:
	movl	%ebx, %edx
	movq	-720(%rbp), %rbx
	movq	%rax, %rsi
	movq	%r11, -712(%rbp)
	movq	%rbx, %rdi
	call	u_memcpy_67@PLT
	movq	%rbx, %rdi
	movq	%r12, %rcx
	movl	%r15d, %esi
	movl	-692(%rbp), %edx
	call	u_terminateUChars_67@PLT
	movq	-712(%rbp), %r11
	movl	%eax, %ebx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	movl	-728(%rbp), %eax
	movl	-712(%rbp), %ebx
	cmpl	%ebx, %eax
	jg	.L48
	movq	-720(%rbp), %rsi
	movl	%eax, %edx
	movq	%r15, %rdi
	movl	%eax, %ebx
	movq	%r11, -728(%rbp)
	call	u_charsToUChars_67@PLT
	movl	-712(%rbp), %esi
	movl	%ebx, %edx
	movq	%r12, %rcx
	movq	-720(%rbp), %rdi
	call	u_terminateUChars_67@PLT
	movq	-728(%rbp), %r11
	movl	%eax, %ebx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L48:
	movl	%eax, %ebx
	jmp	.L64
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3279:
	.size	uloc_getDisplayKeywordValue_67.part.0, .-uloc_getDisplayKeywordValue_67.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale18getDisplayLanguageERNS_13UnicodeStringE
	.type	_ZNK6icu_676Locale18getDisplayLanguageERNS_13UnicodeStringE, @function
_ZNK6icu_676Locale18getDisplayLanguageERNS_13UnicodeStringE:
.LFB2472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	$157, %esi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	movq	%rax, %rbx
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L102
	movq	%rax, %rdx
	movl	-44(%rbp), %eax
	testb	$2, 8(%r12)
	jne	.L103
	testl	%eax, %eax
	jg	.L76
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L77
	movl	$1, -44(%rbp)
.L76:
	xorl	%esi, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	je	.L104
.L71:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movl	$27, %ecx
	testl	%eax, %eax
	jg	.L76
.L77:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%rbx), %rsi
	movq	40(%r13), %rdi
	pushq	%rax
	movq	uloc_getLanguage_67@GOTPCREL(%rip), %r8
	leaq	_ZL11_kLanguages(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %r8d
	popq	%rsi
	movl	$0, %esi
	popq	%rdi
	movq	%r12, %rdi
	movl	%eax, %r14d
	testl	%r8d, %r8d
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	jne	.L71
.L104:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L102
	movl	$0, -44(%rbp)
	testb	$2, 8(%r12)
	jne	.L91
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L86
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, -44(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$27, %ecx
.L86:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%rbx), %rsi
	movq	40(%r13), %rdi
	pushq	%rax
	movq	uloc_getLanguage_67@GOTPCREL(%rip), %r8
	leaq	_ZL11_kLanguages(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %ecx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%rax
	movl	$0, %eax
	popq	%rdx
	testl	%ecx, %ecx
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L102:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L101
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L101:
	testw	%ax, %ax
	js	.L84
	movswl	%ax, %edx
	sarl	$5, %edx
.L85:
	testl	%edx, %edx
	je	.L71
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L84:
	movl	12(%r12), %edx
	jmp	.L85
.L105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2472:
	.size	_ZNK6icu_676Locale18getDisplayLanguageERNS_13UnicodeStringE, .-_ZNK6icu_676Locale18getDisplayLanguageERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale18getDisplayLanguageERKS0_RNS_13UnicodeStringE
	.type	_ZNK6icu_676Locale18getDisplayLanguageERKS0_RNS_13UnicodeStringE, @function
_ZNK6icu_676Locale18getDisplayLanguageERKS0_RNS_13UnicodeStringE:
.LFB2473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$157, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L140
	movq	%rax, %rdx
	movl	-44(%rbp), %eax
	testb	$2, 8(%r12)
	jne	.L141
	testl	%eax, %eax
	jg	.L114
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L115
	movl	$1, -44(%rbp)
.L114:
	xorl	%esi, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	je	.L142
.L109:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L143
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	movl	$27, %ecx
	testl	%eax, %eax
	jg	.L114
.L115:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	pushq	%rax
	movq	uloc_getLanguage_67@GOTPCREL(%rip), %r8
	leaq	_ZL11_kLanguages(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %r8d
	popq	%rsi
	movl	$0, %esi
	popq	%rdi
	movq	%r12, %rdi
	movl	%eax, %r14d
	testl	%r8d, %r8d
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	jne	.L109
.L142:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L140
	movl	$0, -44(%rbp)
	testb	$2, 8(%r12)
	jne	.L129
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L124
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, -44(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$27, %ecx
.L124:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	pushq	%rax
	movq	uloc_getLanguage_67@GOTPCREL(%rip), %r8
	leaq	_ZL11_kLanguages(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %ecx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%rax
	movl	$0, %eax
	popq	%rdx
	testl	%ecx, %ecx
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L140:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L139
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L139:
	testw	%ax, %ax
	js	.L122
	movswl	%ax, %edx
	sarl	$5, %edx
.L123:
	testl	%edx, %edx
	je	.L109
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L122:
	movl	12(%r12), %edx
	jmp	.L123
.L143:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2473:
	.size	_ZNK6icu_676Locale18getDisplayLanguageERKS0_RNS_13UnicodeStringE, .-_ZNK6icu_676Locale18getDisplayLanguageERKS0_RNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale16getDisplayScriptERNS_13UnicodeStringE
	.type	_ZNK6icu_676Locale16getDisplayScriptERNS_13UnicodeStringE, @function
_ZNK6icu_676Locale16getDisplayScriptERNS_13UnicodeStringE:
.LFB2474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	$157, %esi
	movq	%r12, %rdi
	movl	$0, -64(%rbp)
	movq	%rax, %rbx
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L187
	movq	40(%rbx), %rsi
	movq	40(%r14), %rdi
	movq	%rax, %r13
	testb	$2, 8(%r12)
	jne	.L188
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L189
	movl	$1, -64(%rbp)
	xorl	%r15d, %r15d
	xorl	%esi, %esi
.L156:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -64(%rbp)
	je	.L190
.L147:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L191
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movl	$0, -60(%rbp)
.L152:
	subq	$8, %rsp
	leaq	-60(%rbp), %rax
	movq	%r13, %rdx
	movl	%ecx, -84(%rbp)
	pushq	%rax
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	leaq	_ZL19_kScriptsStandAlone(%rip), %r9
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%rdx
	popq	%rcx
	movl	%eax, %r15d
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L192
	movl	%eax, -64(%rbp)
.L158:
	testl	%eax, %eax
	movl	$0, %esi
	movq	%r12, %rdi
	cmovle	%r15d, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -64(%rbp)
	jne	.L147
.L190:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L187
	movq	40(%rbx), %r15
	movq	40(%r14), %rbx
	movl	$0, -64(%rbp)
	testb	$2, 8(%r12)
	jne	.L193
	movl	16(%r12), %r14d
	testl	%r14d, %r14d
	jns	.L194
	movl	$1, -64(%rbp)
	xorl	%esi, %esi
.L169:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$0, -60(%rbp)
	movl	$27, %ecx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L192:
	movl	-64(%rbp), %r15d
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %rsi
	movl	-84(%rbp), %ecx
	testl	%r15d, %r15d
	jg	.L157
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movq	%r13, %rdx
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	pushq	%rax
	leaq	_ZL9_kScripts(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%r10
	popq	%r11
	movl	%eax, %r15d
	movl	-64(%rbp), %eax
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L157:
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L187:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L186
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L186:
	testw	%ax, %ax
	js	.L162
	movswl	%ax, %edx
	sarl	$5, %edx
.L163:
	testl	%edx, %edx
	je	.L147
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$0, -60(%rbp)
	movl	$27, %r14d
.L165:
	subq	$8, %rsp
	leaq	-60(%rbp), %rax
	movq	%r15, %rsi
	movl	%r14d, %ecx
	pushq	%rax
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	_ZL19_kScriptsStandAlone(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%r8
	popq	%r9
	movl	%eax, %esi
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L195
	movl	%eax, -64(%rbp)
.L171:
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %esi
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L195:
	movl	-64(%rbp), %edi
	xorl	%esi, %esi
	testl	%edi, %edi
	jg	.L169
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%r14d, %ecx
	movq	%r13, %rdx
	pushq	%rax
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	leaq	_ZL9_kScripts(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%rdx
	popq	%rcx
	movl	%eax, %esi
	movl	-64(%rbp), %eax
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L162:
	movl	12(%r12), %edx
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$0, -60(%rbp)
	jmp	.L165
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2474:
	.size	_ZNK6icu_676Locale16getDisplayScriptERNS_13UnicodeStringE, .-_ZNK6icu_676Locale16getDisplayScriptERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale16getDisplayScriptERKS0_RNS_13UnicodeStringE
	.type	_ZNK6icu_676Locale16getDisplayScriptERKS0_RNS_13UnicodeStringE, @function
_ZNK6icu_676Locale16getDisplayScriptERKS0_RNS_13UnicodeStringE:
.LFB2475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$157, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -64(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L239
	movq	40(%r14), %rsi
	movq	40(%rbx), %rdi
	movq	%rax, %r13
	testb	$2, 8(%r12)
	jne	.L240
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L241
	movl	$1, -64(%rbp)
	xorl	%r15d, %r15d
	xorl	%esi, %esi
.L208:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -64(%rbp)
	je	.L242
.L199:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L243
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	movl	$0, -60(%rbp)
.L204:
	subq	$8, %rsp
	leaq	-60(%rbp), %rax
	movq	%r13, %rdx
	movl	%ecx, -84(%rbp)
	pushq	%rax
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	leaq	_ZL19_kScriptsStandAlone(%rip), %r9
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%rdx
	popq	%rcx
	movl	%eax, %r15d
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L244
	movl	%eax, -64(%rbp)
.L210:
	testl	%eax, %eax
	movl	$0, %esi
	movq	%r12, %rdi
	cmovle	%r15d, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -64(%rbp)
	jne	.L199
.L242:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L239
	movq	40(%r14), %r15
	movq	40(%rbx), %rbx
	movl	$0, -64(%rbp)
	testb	$2, 8(%r12)
	jne	.L245
	movl	16(%r12), %r14d
	testl	%r14d, %r14d
	jns	.L246
	movl	$1, -64(%rbp)
	xorl	%esi, %esi
.L221:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L240:
	movl	$0, -60(%rbp)
	movl	$27, %ecx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L244:
	movl	-64(%rbp), %r15d
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %rsi
	movl	-84(%rbp), %ecx
	testl	%r15d, %r15d
	jg	.L209
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movq	%r13, %rdx
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	pushq	%rax
	leaq	_ZL9_kScripts(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%r10
	popq	%r11
	movl	%eax, %r15d
	movl	-64(%rbp), %eax
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L209:
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L239:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L238
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L238:
	testw	%ax, %ax
	js	.L214
	movswl	%ax, %edx
	sarl	$5, %edx
.L215:
	testl	%edx, %edx
	je	.L199
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$0, -60(%rbp)
	movl	$27, %r14d
.L217:
	subq	$8, %rsp
	leaq	-60(%rbp), %rax
	movq	%r15, %rsi
	movl	%r14d, %ecx
	pushq	%rax
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	_ZL19_kScriptsStandAlone(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%r8
	popq	%r9
	movl	%eax, %esi
	movl	-60(%rbp), %eax
	cmpl	$-127, %eax
	je	.L247
	movl	%eax, -64(%rbp)
.L223:
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %esi
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L247:
	movl	-64(%rbp), %edi
	xorl	%esi, %esi
	testl	%edi, %edi
	jg	.L221
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	movl	%r14d, %ecx
	movq	%r13, %rdx
	pushq	%rax
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	leaq	_ZL9_kScripts(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%rdx
	popq	%rcx
	movl	%eax, %esi
	movl	-64(%rbp), %eax
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L214:
	movl	12(%r12), %edx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$0, -60(%rbp)
	jmp	.L217
.L243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2475:
	.size	_ZNK6icu_676Locale16getDisplayScriptERKS0_RNS_13UnicodeStringE, .-_ZNK6icu_676Locale16getDisplayScriptERKS0_RNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale17getDisplayCountryERNS_13UnicodeStringE
	.type	_ZNK6icu_676Locale17getDisplayCountryERNS_13UnicodeStringE, @function
_ZNK6icu_676Locale17getDisplayCountryERNS_13UnicodeStringE:
.LFB2476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	$157, %esi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	movq	%rax, %rbx
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L282
	movq	%rax, %rdx
	movl	-44(%rbp), %eax
	testb	$2, 8(%r12)
	jne	.L283
	testl	%eax, %eax
	jg	.L256
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L257
	movl	$1, -44(%rbp)
.L256:
	xorl	%esi, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	je	.L284
.L251:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movl	$27, %ecx
	testl	%eax, %eax
	jg	.L256
.L257:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%rbx), %rsi
	movq	40(%r13), %rdi
	pushq	%rax
	movq	uloc_getCountry_67@GOTPCREL(%rip), %r8
	leaq	_ZL11_kCountries(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %r8d
	popq	%rsi
	movl	$0, %esi
	popq	%rdi
	movq	%r12, %rdi
	movl	%eax, %r14d
	testl	%r8d, %r8d
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	jne	.L251
.L284:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L282
	movl	$0, -44(%rbp)
	testb	$2, 8(%r12)
	jne	.L271
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L266
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, -44(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L271:
	movl	$27, %ecx
.L266:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%rbx), %rsi
	movq	40(%r13), %rdi
	pushq	%rax
	movq	uloc_getCountry_67@GOTPCREL(%rip), %r8
	leaq	_ZL11_kCountries(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %ecx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%rax
	movl	$0, %eax
	popq	%rdx
	testl	%ecx, %ecx
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L282:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L281
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L281:
	testw	%ax, %ax
	js	.L264
	movswl	%ax, %edx
	sarl	$5, %edx
.L265:
	testl	%edx, %edx
	je	.L251
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L264:
	movl	12(%r12), %edx
	jmp	.L265
.L285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2476:
	.size	_ZNK6icu_676Locale17getDisplayCountryERNS_13UnicodeStringE, .-_ZNK6icu_676Locale17getDisplayCountryERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale17getDisplayCountryERKS0_RNS_13UnicodeStringE
	.type	_ZNK6icu_676Locale17getDisplayCountryERKS0_RNS_13UnicodeStringE, @function
_ZNK6icu_676Locale17getDisplayCountryERKS0_RNS_13UnicodeStringE:
.LFB2477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$157, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L320
	movq	%rax, %rdx
	movl	-44(%rbp), %eax
	testb	$2, 8(%r12)
	jne	.L321
	testl	%eax, %eax
	jg	.L294
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L295
	movl	$1, -44(%rbp)
.L294:
	xorl	%esi, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	je	.L322
.L289:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movl	$27, %ecx
	testl	%eax, %eax
	jg	.L294
.L295:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	pushq	%rax
	movq	uloc_getCountry_67@GOTPCREL(%rip), %r8
	leaq	_ZL11_kCountries(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %r8d
	popq	%rsi
	movl	$0, %esi
	popq	%rdi
	movq	%r12, %rdi
	movl	%eax, %r14d
	testl	%r8d, %r8d
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	jne	.L289
.L322:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L320
	movl	$0, -44(%rbp)
	testb	$2, 8(%r12)
	jne	.L309
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L304
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, -44(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L309:
	movl	$27, %ecx
.L304:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	pushq	%rax
	movq	uloc_getCountry_67@GOTPCREL(%rip), %r8
	leaq	_ZL11_kCountries(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %ecx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%rax
	movl	$0, %eax
	popq	%rdx
	testl	%ecx, %ecx
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L320:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L319
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L319:
	testw	%ax, %ax
	js	.L302
	movswl	%ax, %edx
	sarl	$5, %edx
.L303:
	testl	%edx, %edx
	je	.L289
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L302:
	movl	12(%r12), %edx
	jmp	.L303
.L323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2477:
	.size	_ZNK6icu_676Locale17getDisplayCountryERKS0_RNS_13UnicodeStringE, .-_ZNK6icu_676Locale17getDisplayCountryERKS0_RNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale17getDisplayVariantERNS_13UnicodeStringE
	.type	_ZNK6icu_676Locale17getDisplayVariantERNS_13UnicodeStringE, @function
_ZNK6icu_676Locale17getDisplayVariantERNS_13UnicodeStringE:
.LFB2478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	$157, %esi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	movq	%rax, %rbx
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L358
	movq	%rax, %rdx
	movl	-44(%rbp), %eax
	testb	$2, 8(%r12)
	jne	.L359
	testl	%eax, %eax
	jg	.L332
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L333
	movl	$1, -44(%rbp)
.L332:
	xorl	%esi, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	je	.L360
.L327:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movl	$27, %ecx
	testl	%eax, %eax
	jg	.L332
.L333:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%rbx), %rsi
	movq	40(%r13), %rdi
	pushq	%rax
	movq	uloc_getVariant_67@GOTPCREL(%rip), %r8
	leaq	_ZL10_kVariants(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %r8d
	popq	%rsi
	movl	$0, %esi
	popq	%rdi
	movq	%r12, %rdi
	movl	%eax, %r14d
	testl	%r8d, %r8d
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	jne	.L327
.L360:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L358
	movl	$0, -44(%rbp)
	testb	$2, 8(%r12)
	jne	.L347
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L342
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, -44(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$27, %ecx
.L342:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%rbx), %rsi
	movq	40(%r13), %rdi
	pushq	%rax
	movq	uloc_getVariant_67@GOTPCREL(%rip), %r8
	leaq	_ZL10_kVariants(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %ecx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%rax
	movl	$0, %eax
	popq	%rdx
	testl	%ecx, %ecx
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L358:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L357
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L357:
	testw	%ax, %ax
	js	.L340
	movswl	%ax, %edx
	sarl	$5, %edx
.L341:
	testl	%edx, %edx
	je	.L327
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L340:
	movl	12(%r12), %edx
	jmp	.L341
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2478:
	.size	_ZNK6icu_676Locale17getDisplayVariantERNS_13UnicodeStringE, .-_ZNK6icu_676Locale17getDisplayVariantERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale17getDisplayVariantERKS0_RNS_13UnicodeStringE
	.type	_ZNK6icu_676Locale17getDisplayVariantERKS0_RNS_13UnicodeStringE, @function
_ZNK6icu_676Locale17getDisplayVariantERKS0_RNS_13UnicodeStringE:
.LFB2479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$157, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L396
	movq	%rax, %rdx
	movl	-44(%rbp), %eax
	testb	$2, 8(%r12)
	jne	.L397
	testl	%eax, %eax
	jg	.L370
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L371
	movl	$1, -44(%rbp)
.L370:
	xorl	%esi, %esi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	je	.L398
.L365:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	movl	$27, %ecx
	testl	%eax, %eax
	jg	.L370
.L371:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	pushq	%rax
	movq	uloc_getVariant_67@GOTPCREL(%rip), %r8
	leaq	_ZL10_kVariants(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %r8d
	popq	%rsi
	movl	$0, %esi
	popq	%rdi
	movq	%r12, %rdi
	movl	%eax, %r14d
	testl	%r8d, %r8d
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -44(%rbp)
	jne	.L365
.L398:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L396
	movl	$0, -44(%rbp)
	testb	$2, 8(%r12)
	jne	.L385
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jns	.L380
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, -44(%rbp)
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L385:
	movl	$27, %ecx
.L380:
	subq	$8, %rsp
	leaq	-44(%rbp), %rax
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	pushq	%rax
	movq	uloc_getVariant_67@GOTPCREL(%rip), %r8
	leaq	_ZL10_kVariants(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-44(%rbp), %ecx
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%rax
	movl	$0, %eax
	popq	%rdx
	testl	%ecx, %ecx
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L396:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L395
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L395:
	testw	%ax, %ax
	js	.L378
	movswl	%ax, %edx
	sarl	$5, %edx
.L379:
	testl	%edx, %edx
	je	.L365
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L378:
	movl	12(%r12), %edx
	jmp	.L379
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2479:
	.size	_ZNK6icu_676Locale17getDisplayVariantERKS0_RNS_13UnicodeStringE, .-_ZNK6icu_676Locale17getDisplayVariantERKS0_RNS_13UnicodeStringE
	.p2align 4
	.globl	uloc_getDisplayLanguage_67
	.type	uloc_getDisplayLanguage_67, @function
uloc_getDisplayLanguage_67:
.LFB2486:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L404
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L413
	testl	%ecx, %ecx
	js	.L402
	jle	.L403
	testq	%rdx, %rdx
	jne	.L403
.L402:
	movl	$1, (%r8)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL11_kLanguages(%rip), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r8
	movq	uloc_getLanguage_67@GOTPCREL(%rip), %r8
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2486:
	.size	uloc_getDisplayLanguage_67, .-uloc_getDisplayLanguage_67
	.p2align 4
	.globl	uloc_getDisplayScript_67
	.type	uloc_getDisplayScript_67, @function
uloc_getDisplayScript_67:
.LFB2487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	testl	%ecx, %ecx
	js	.L421
	movq	%rdi, %r14
	movq	%rsi, %r15
	movq	%rdx, %r13
	movl	%ecx, %r12d
	jle	.L424
	testq	%rdx, %rdx
	jne	.L424
.L421:
	xorl	%eax, %eax
	movl	$1, %edx
.L416:
	movl	%edx, (%rbx)
.L415:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L429
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	subq	$8, %rsp
	leaq	-60(%rbp), %rax
	movq	%r13, %rdx
	movq	%r14, %rdi
	pushq	%rax
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	movl	%r12d, %ecx
	movq	%r15, %rsi
	leaq	_ZL19_kScriptsStandAlone(%rip), %r9
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movl	-60(%rbp), %edx
	popq	%rdi
	popq	%r8
	cmpl	$-127, %edx
	jne	.L416
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L415
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L415
	subq	$8, %rsp
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	pushq	%rbx
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	leaq	_ZL9_kScripts(%rip), %r9
	movq	%r14, %rdi
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%rdx
	popq	%rcx
	jmp	.L415
.L429:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2487:
	.size	uloc_getDisplayScript_67, .-uloc_getDisplayScript_67
	.p2align 4
	.globl	uloc_getDisplayScriptInContext_67
	.type	uloc_getDisplayScriptInContext_67, @function
uloc_getDisplayScriptInContext_67:
.LFB2488:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L434
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L443
	testl	%ecx, %ecx
	js	.L432
	jle	.L433
	testq	%rdx, %rdx
	jne	.L433
.L432:
	movl	$1, (%r8)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL9_kScripts(%rip), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r8
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2488:
	.size	uloc_getDisplayScriptInContext_67, .-uloc_getDisplayScriptInContext_67
	.p2align 4
	.globl	uloc_getDisplayCountry_67
	.type	uloc_getDisplayCountry_67, @function
uloc_getDisplayCountry_67:
.LFB2489:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L449
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L458
	testl	%ecx, %ecx
	js	.L447
	jle	.L448
	testq	%rdx, %rdx
	jne	.L448
.L447:
	movl	$1, (%r8)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	ret
	.p2align 4,,10
	.p2align 3
.L448:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL11_kCountries(%rip), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r8
	movq	uloc_getCountry_67@GOTPCREL(%rip), %r8
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2489:
	.size	uloc_getDisplayCountry_67, .-uloc_getDisplayCountry_67
	.p2align 4
	.globl	uloc_getDisplayVariant_67
	.type	uloc_getDisplayVariant_67, @function
uloc_getDisplayVariant_67:
.LFB2490:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L464
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L473
	testl	%ecx, %ecx
	js	.L462
	jle	.L463
	testq	%rdx, %rdx
	jne	.L463
.L462:
	movl	$1, (%r8)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL10_kVariants(%rip), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r8
	movq	uloc_getVariant_67@GOTPCREL(%rip), %r8
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2490:
	.size	uloc_getDisplayVariant_67, .-uloc_getDisplayVariant_67
	.p2align 4
	.globl	uloc_getDisplayName_67
	.type	uloc_getDisplayName_67, @function
uloc_getDisplayName_67:
.LFB2491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -144(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdx, -104(%rbp)
	movl	%ecx, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	testq	%r8, %r8
	je	.L561
	movl	(%r8), %esi
	movq	%r8, %r11
	testl	%esi, %esi
	jg	.L475
	testl	%ecx, %ecx
	js	.L477
	jle	.L478
	testq	%rdx, %rdx
	jne	.L478
.L477:
	movl	$1, (%r11)
	xorl	%eax, %eax
.L475:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L722
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	leaq	-60(%rbp), %rbx
	movq	-152(%rbp), %rsi
	movq	%r11, -96(%rbp)
	leaq	.LC2(%rip), %rdi
	movq	%rbx, %rdx
	movl	$0, -60(%rbp)
	movq	%rbx, -200(%rbp)
	call	ures_open_67@PLT
	xorl	%edx, %edx
	movq	%rbx, %rcx
	leaq	_ZL22_kLocaleDisplayPattern(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getByKeyWithFallback_67@PLT
	leaq	-68(%rbp), %rdx
	movq	%rbx, %rcx
	leaq	_ZL11_kSeparator(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	ures_getStringByKeyWithFallback_67@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r14, %rdi
	leaq	_ZL9_kPattern(%rip), %rsi
	movq	%rax, %r12
	call	ures_getStringByKeyWithFallback_67@PLT
	testq	%r14, %r14
	movq	-96(%rbp), %r11
	movq	%rax, -192(%rbp)
	je	.L479
	movq	%r14, %rdi
	call	ures_close_67@PLT
	movq	-96(%rbp), %r11
.L479:
	testq	%r13, %r13
	je	.L480
	movq	%r13, %rdi
	movq	%r11, -96(%rbp)
	call	ures_close_67@PLT
	movq	-96(%rbp), %r11
.L480:
	movl	-68(%rbp), %r15d
	leaq	_ZZ22uloc_getDisplayName_67E16defaultSeparator(%rip), %rax
	leaq	_ZZ22uloc_getDisplayName_67E4sub0(%rip), %rsi
	movq	%r11, -96(%rbp)
	testl	%r15d, %r15d
	cmove	%rax, %r12
	movq	%r12, %rdi
	call	u_strstr_67@PLT
	leaq	_ZZ22uloc_getDisplayName_67E4sub1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, -216(%rbp)
	call	u_strstr_67@PLT
	testq	%rbx, %rbx
	movq	-96(%rbp), %r11
	movq	%rbx, %rsi
	sete	%cl
	testq	%rax, %rax
	sete	%dl
	orb	%dl, %cl
	jne	.L477
	cmpq	%rax, %rbx
	ja	.L477
	addq	$6, %rsi
	subq	%rsi, %rax
	movq	%rsi, -208(%rbp)
	sarq	%rax
	movl	%eax, -68(%rbp)
	movl	-64(%rbp), %eax
	testl	%eax, %eax
	jne	.L482
.L485:
	movl	$93, %ebx
	movl	$41, %r12d
	movl	$91, %r13d
	movl	$40, %r14d
	leaq	_ZZ22uloc_getDisplayName_67E14defaultPattern(%rip), %rax
	movl	$9, -64(%rbp)
	movl	$0, -88(%rbp)
	movw	%bx, -184(%rbp)
	movw	%r12w, -220(%rbp)
	movw	%r13w, -218(%rbp)
	movw	%r14w, -108(%rbp)
	movl	$5, -168(%rbp)
	movl	$0, -132(%rbp)
	movq	%rax, -192(%rbp)
.L483:
	movq	-104(%rbp), %rax
	movb	$0, -181(%rbp)
	movq	%r11, %r15
	movb	$1, -96(%rbp)
	addq	$15, %rax
	subq	-192(%rbp), %rax
	movb	$1, -105(%rbp)
	movq	%rax, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L556:
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	je	.L565
	movl	%eax, -112(%rbp)
	movq	-104(%rbp), %r14
	cmpl	-84(%rbp), %eax
	jle	.L723
.L487:
	movq	$0, -128(%rbp)
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	movl	-132(%rbp), %ebx
	movl	$0, -160(%rbp)
	movq	%r15, %r10
	movl	$0, -164(%rbp)
	movl	$0, -180(%rbp)
	movl	$0, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L493:
	movl	-84(%rbp), %r13d
	xorl	%r15d, %r15d
	subl	%ebx, %r13d
	testl	%r13d, %r13d
	jle	.L494
	movq	-104(%rbp), %rcx
	movslq	%ebx, %rax
	movl	%r13d, %r15d
	leaq	(%rcx,%rax,2), %r14
.L494:
	cmpl	%r12d, -88(%rbp)
	je	.L724
	cmpb	$0, -96(%rbp)
	jne	.L500
	movl	(%r10), %eax
.L496:
	cmpl	$15, %eax
	je	.L725
.L558:
	cmpb	$0, -96(%rbp)
	je	.L499
	cmpb	$0, -105(%rbp)
	je	.L499
	movl	-112(%rbp), %esi
	movl	-168(%rbp), %eax
	addl	$3, %esi
	testl	%r12d, %r12d
	cmovne	-64(%rbp), %eax
	movl	%eax, %edx
	movl	%eax, -112(%rbp)
	subl	%esi, %edx
	leal	(%rdx,%rbx), %ecx
	cmpl	-84(%rbp), %ecx
	jl	.L726
.L716:
	movl	%ecx, %ebx
.L544:
	addl	$1, %r12d
	cmpl	$2, %r12d
	jne	.L493
	movq	-128(%rbp), %rax
	movq	%r10, %r15
	testq	%rax, %rax
	je	.L555
	movq	%rax, %rdi
	call	uenum_close_67@PLT
.L555:
	cmpb	$0, -181(%rbp)
	jne	.L556
	movl	-84(%rbp), %esi
	movq	-104(%rbp), %rdi
	movq	%r15, %rcx
	movl	%ebx, %edx
	call	u_terminateUChars_67@PLT
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L561:
	xorl	%eax, %eax
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L726:
	movq	-104(%rbp), %r15
	movslq	%ebx, %rbx
	addq	%rbx, %rbx
	leaq	(%r15,%rbx), %r14
	testl	%edx, %edx
	jle	.L583
	movq	-192(%rbp), %r13
	movslq	%esi, %rax
	leal	-1(%rdx), %r8d
	leaq	16(%rax,%rax), %rdi
	leaq	0(%r13,%rax,2), %r9
	leaq	16(%r15,%rbx), %rax
	cmpq	%rax, %r9
	leaq	0(%r13,%rdi), %rax
	setnb	%bl
	cmpq	%rax, %r14
	setnb	%al
	orb	%al, %bl
	je	.L545
	cmpl	$6, %r8d
	jbe	.L545
	movl	%edx, %edi
	xorl	%eax, %eax
	shrl	$3, %edi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L547:
	movdqu	(%r9,%rax), %xmm0
	movups	%xmm0, (%r14,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L547
	movl	%edx, %eax
	andl	$-8, %eax
	movl	%eax, %edi
	leal	(%rsi,%rax), %r9d
	leaq	(%r14,%rdi,2), %rdi
	cmpl	%edx, %eax
	je	.L549
	movq	-192(%rbp), %r15
	leal	1(%r9), %ebx
	movslq	%r9d, %r9
	movzwl	(%r15,%r9,2), %r9d
	movw	%r9w, (%rdi)
	leal	1(%rax), %r9d
	cmpl	%edx, %r9d
	jge	.L549
	movslq	%ebx, %r9
	leaq	(%r9,%r9), %rbx
	movzwl	(%r15,%r9,2), %r9d
	movw	%r9w, 2(%rdi)
	leal	2(%rax), %r9d
	cmpl	%edx, %r9d
	jge	.L549
	movzwl	2(%r15,%rbx), %r9d
	movw	%r9w, 4(%rdi)
	leal	3(%rax), %r9d
	cmpl	%r9d, %edx
	jle	.L549
	movzwl	4(%r15,%rbx), %r9d
	movw	%r9w, 6(%rdi)
	leal	4(%rax), %r9d
	cmpl	%r9d, %edx
	jle	.L549
	movzwl	6(%r15,%rbx), %r9d
	movw	%r9w, 8(%rdi)
	leal	5(%rax), %r9d
	cmpl	%r9d, %edx
	jle	.L549
	movzwl	8(%r15,%rbx), %r9d
	addl	$6, %eax
	movw	%r9w, 10(%rdi)
	cmpl	%eax, %edx
	jle	.L549
	movzwl	10(%r15,%rbx), %eax
	movw	%ax, 12(%rdi)
.L549:
	leal	(%rsi,%rdx), %eax
	leaq	2(%r14,%r8,2), %r14
	movl	%ecx, %ebx
	movl	%eax, -112(%rbp)
	jmp	.L544
.L482:
	cmpl	$9, %eax
	je	.L727
.L484:
	movq	-192(%rbp), %r14
	leaq	_ZZ22uloc_getDisplayName_67E4sub0(%rip), %rsi
	movq	%r11, -96(%rbp)
	movq	%r14, %rdi
	call	u_strstr_67@PLT
	leaq	_ZZ22uloc_getDisplayName_67E4sub1(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	u_strstr_67@PLT
	testq	%rbx, %rbx
	movq	-96(%rbp), %r11
	movq	%rax, %rdx
	je	.L477
	testq	%rax, %rax
	je	.L477
	movq	%rbx, %rax
	subq	%r14, %rdx
	movq	%r14, %rdi
	movl	$65288, %esi
	subq	%r14, %rax
	sarq	%rdx
	sarq	%rax
	cmpl	%edx, %eax
	movl	%eax, %ecx
	cmovg	%edx, %ecx
	movl	%ecx, -132(%rbp)
	movl	%edx, %ecx
	cmovg	%eax, %ecx
	setg	%al
	movzbl	%al, %eax
	movl	%ecx, -168(%rbp)
	movl	%eax, -88(%rbp)
	call	u_strchr_67@PLT
	movq	-96(%rbp), %r11
	cmpq	$1, %rax
	sbbl	%edi, %edi
	movl	%edi, %ecx
	movl	%edi, %esi
	movl	%edi, %eax
	andw	$288, %cx
	andw	$288, %si
	andw	$288, %ax
	subw	$195, %cx
	subw	$197, %si
	subw	$248, %ax
	movw	%cx, -184(%rbp)
	movl	%edi, %ecx
	andw	$288, %cx
	movw	%si, -218(%rbp)
	subw	$247, %cx
	movw	%ax, -108(%rbp)
	movw	%cx, -220(%rbp)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L724:
	cmpb	$0, -105(%rbp)
	movl	(%r10), %eax
	je	.L496
	testl	%eax, %eax
	jg	.L569
	testl	%r15d, %r15d
	jle	.L498
	testq	%r14, %r14
	jne	.L498
	movl	$1, (%r10)
	movl	%ebx, -180(%rbp)
	movl	$0, -136(%rbp)
	movb	$0, -105(%rbp)
	.p2align 4,,10
	.p2align 3
.L499:
	testl	%r12d, %r12d
	je	.L584
	testl	%ebx, %ebx
	jle	.L544
	movzbl	-105(%rbp), %esi
	movl	-164(%rbp), %ebx
	testb	%sil, %sil
	cmovne	-136(%rbp), %ebx
	cmpq	$0, -104(%rbp)
	je	.L544
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	je	.L544
	addl	%ebx, %eax
	cmpl	-84(%rbp), %eax
	jg	.L586
	testb	%sil, %sil
	je	.L552
	movslq	-180(%rbp), %rsi
	addq	%rsi, %rsi
.L553:
	movq	-104(%rbp), %rdi
	movl	%ebx, %edx
	movq	%r10, -176(%rbp)
	movl	%r11d, -120(%rbp)
	addq	%rdi, %rsi
	call	u_memmove_67@PLT
	movl	-120(%rbp), %r11d
	movq	-176(%rbp), %r10
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L500:
	leal	1(%r11), %eax
	movl	%eax, -120(%rbp)
	cmpl	$2, %r11d
	je	.L501
	jg	.L502
	testl	%r11d, %r11d
	je	.L503
	cmpl	$1, %r11d
	jne	.L505
	movl	(%r10), %ecx
	testl	%ecx, %ecx
	jg	.L578
	testl	%r15d, %r15d
	jle	.L511
	testq	%r14, %r14
	je	.L717
.L511:
	subq	$8, %rsp
	movq	%r10, -176(%rbp)
	movq	uloc_getCountry_67@GOTPCREL(%rip), %r8
	leaq	_ZL11_kCountries(%rip), %r9
	pushq	%r10
.L719:
	movq	-152(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movq	-176(%rbp), %r10
	popq	%rsi
	movl	%eax, -60(%rbp)
	popq	%rdi
	movl	(%r10), %ecx
	testl	%eax, %eax
	jle	.L578
.L510:
	movl	-68(%rbp), %esi
	leal	(%rsi,%rax), %edi
	cmpl	%r15d, %edi
	jle	.L728
.L530:
	addl	%edi, %ebx
	xorl	%eax, %eax
.L507:
	movl	-120(%rbp), %r11d
	cmpl	$15, %ecx
	je	.L557
.L541:
	testb	%al, %al
	je	.L493
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L725:
	movl	$1, %eax
.L557:
	movl	$0, (%r10)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L502:
	cmpl	$3, %r11d
	jne	.L505
	movq	-144(%rbp), %rdi
	movq	%r10, %rsi
	movq	%r10, -176(%rbp)
	call	uloc_openKeywords_67@PLT
	movq	-128(%rbp), %rdi
	movq	-176(%rbp), %r10
	movq	%rax, -128(%rbp)
	testq	%rdi, %rdi
	je	.L505
	call	uenum_close_67@PLT
	movq	-176(%rbp), %r10
.L505:
	movq	-200(%rbp), %rsi
	movq	-128(%rbp), %rdi
	movq	%r10, %rdx
	movq	%r10, -176(%rbp)
	call	uenum_next_67@PLT
	movq	-176(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r11
	je	.L729
	movl	(%r10), %ecx
	testl	%ecx, %ecx
	jg	.L730
	testl	%r15d, %r15d
	jle	.L519
	testq	%r14, %r14
	jne	.L519
	movl	$1, (%r10)
	movl	$1, %ecx
.L578:
	xorl	%eax, %eax
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L584:
	movl	$0, -132(%rbp)
	xorl	%ebx, %ebx
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L729:
	cmpl	-160(%rbp), %ebx
	je	.L731
	subl	-68(%rbp), %ebx
	movl	%ebx, %eax
	subl	-160(%rbp), %eax
	movl	%eax, -164(%rbp)
	testl	%eax, %eax
	setg	%dl
.L515:
	movzbl	-96(%rbp), %eax
	movl	(%r10), %ecx
	movb	%dl, -96(%rbp)
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L565:
	movl	$0, -112(%rbp)
	movq	-104(%rbp), %r14
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L503:
	movl	(%r10), %ecx
	testl	%ecx, %ecx
	jg	.L570
	testl	%r15d, %r15d
	jle	.L508
	testq	%r14, %r14
	jne	.L508
	movl	$1, (%r10)
	movl	%eax, %r11d
	movl	%ebx, -160(%rbp)
.L509:
	xorl	%r14d, %r14d
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L501:
	movl	(%r10), %ecx
	testl	%ecx, %ecx
	jg	.L578
	testl	%r15d, %r15d
	jle	.L512
	testq	%r14, %r14
	je	.L717
.L512:
	subq	$8, %rsp
	movq	%r10, -176(%rbp)
	movq	uloc_getVariant_67@GOTPCREL(%rip), %r8
	leaq	_ZL10_kVariants(%rip), %r9
	pushq	%r10
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L728:
	cltq
	leaq	(%rax,%rax), %r8
	movq	%r14, %rax
	leaq	(%r14,%r8), %r9
	cmpq	%r14, %r9
	jbe	.L535
	movzwl	-108(%rbp), %r11d
	movzwl	-218(%rbp), %r15d
	movzwl	-220(%rbp), %r13d
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L533:
	cmpw	%r13w, %dx
	jne	.L534
	movzwl	-184(%rbp), %edx
	movw	%dx, (%rax)
.L534:
	addq	$2, %rax
	cmpq	%rax, %r9
	jbe	.L732
.L531:
	movzwl	(%rax), %edx
	cmpw	%r11w, %dx
	jne	.L533
	movw	%r15w, (%rax)
	addq	$2, %rax
	cmpq	%rax, %r9
	ja	.L531
.L732:
	addq	%r8, %r14
.L535:
	testl	%esi, %esi
	jle	.L530
	movq	-216(%rbp), %r9
	leal	-1(%rsi), %edx
	leaq	22(%r9), %rax
	cmpq	%rax, %r14
	leaq	16(%r14), %rax
	setnb	%r8b
	cmpq	%rax, -208(%rbp)
	setnb	%al
	orb	%al, %r8b
	je	.L536
	cmpl	$6, %edx
	jbe	.L536
	movl	%esi, %r8d
	xorl	%eax, %eax
	shrl	$3, %r8d
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L537:
	movdqu	6(%r9,%rax), %xmm1
	movups	%xmm1, (%r14,%rax)
	addq	$16, %rax
	cmpq	%r8, %rax
	jne	.L537
	movl	%esi, %eax
	andl	$-8, %eax
	movl	%eax, %r11d
	leaq	(%r11,%r11), %r8
	leaq	(%r14,%r8), %r9
	cmpl	%esi, %eax
	je	.L539
	movq	-208(%rbp), %r15
	movzwl	(%r15,%r11,2), %r11d
	movw	%r11w, (%r9)
	leal	1(%rax), %r11d
	cmpl	%r11d, %esi
	jle	.L539
	movzwl	2(%r15,%r8), %r11d
	movw	%r11w, 2(%r9)
	leal	2(%rax), %r11d
	cmpl	%r11d, %esi
	jle	.L539
	movzwl	4(%r15,%r8), %r11d
	movw	%r11w, 4(%r9)
	leal	3(%rax), %r11d
	cmpl	%r11d, %esi
	jle	.L539
	movzwl	6(%r15,%r8), %r11d
	movw	%r11w, 6(%r9)
	leal	4(%rax), %r11d
	cmpl	%r11d, %esi
	jle	.L539
	movzwl	8(%r15,%r8), %r11d
	movw	%r11w, 8(%r9)
	leal	5(%rax), %r11d
	cmpl	%r11d, %esi
	jle	.L539
	movzwl	10(%r15,%r8), %r11d
	addl	$6, %eax
	movw	%r11w, 10(%r9)
	cmpl	%eax, %esi
	jle	.L539
	movzwl	12(%r15,%r8), %eax
	movw	%ax, 12(%r9)
.L539:
	leaq	2(%r14,%rdx,2), %r14
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L569:
	movb	$0, -105(%rbp)
	movl	%ebx, %edx
	movl	$0, -136(%rbp)
.L497:
	movl	%ebx, -180(%rbp)
	movl	%edx, %ebx
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L717:
	movl	$1, (%r10)
	movl	%eax, %r11d
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L508:
	subq	$8, %rsp
	movq	uloc_getScript_67@GOTPCREL(%rip), %r8
	movq	-152(%rbp), %rsi
	leaq	_ZL9_kScripts(%rip), %r9
	pushq	%r10
	movq	-144(%rbp), %rdi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r10, -160(%rbp)
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	movq	-160(%rbp), %r10
	popq	%r8
	movl	%ebx, -160(%rbp)
	movl	%eax, -60(%rbp)
	popq	%r9
	movl	(%r10), %ecx
	testl	%eax, %eax
	jg	.L510
	xorl	%eax, %eax
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L498:
	subq	$8, %rsp
	movq	%r14, %rdx
	movl	%r15d, %ecx
	movq	uloc_getLanguage_67@GOTPCREL(%rip), %r8
	pushq	%r10
	movq	-152(%rbp), %rsi
	leaq	_ZL11_kLanguages(%rip), %r9
	movq	-144(%rbp), %rdi
	movl	%r11d, -176(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZL27_getDisplayNameForComponentPKcS0_PDsiPFiS0_PciP10UErrorCodeES0_S4_.part.0
	popq	%r10
	movq	-120(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, -136(%rbp)
	leal	(%rax,%rbx), %edx
	popq	%r11
	movl	(%r10), %eax
	movl	-176(%rbp), %r11d
	setg	-105(%rbp)
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L519:
	subq	$8, %rsp
	movq	-152(%rbp), %rsi
	movq	%r11, %r9
	xorl	%ecx, %ecx
	pushq	%r10
	movq	%r11, %r8
	leaq	_ZL6_kKeys(%rip), %rdx
	leaq	.LC2(%rip), %rdi
	pushq	%r15
	pushq	%r14
	movq	%r10, -232(%rbp)
	movq	%r11, -176(%rbp)
	call	_ZL19_getStringOrCopyKeyPKcS0_S0_S0_S0_S0_PDsiP10UErrorCode
	addq	$32, %rsp
	movq	-176(%rbp), %r11
	movq	-232(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, -60(%rbp)
	je	.L733
	cmpl	%eax, %r15d
	jle	.L522
	movslq	%eax, %rdx
	movl	$61, %ecx
	movw	%cx, (%r14,%rdx,2)
.L522:
	leal	1(%rax), %edx
	subl	%edx, %r15d
	movl	%edx, -60(%rbp)
	testl	%r15d, %r15d
	jle	.L575
	movslq	%edx, %rax
	leaq	(%r14,%rax,2), %r14
.L523:
	movl	(%r10), %ecx
	cmpl	$15, %ecx
	je	.L517
	testl	%ecx, %ecx
	jle	.L521
	testl	%edx, %edx
	je	.L578
.L525:
	leal	-1(%rdx), %eax
.L529:
	xorl	%r15d, %r15d
	testl	%r13d, %r13d
	jle	.L528
	movq	-104(%rbp), %rdi
	movslq	%ebx, %rdx
	movl	%r13d, %r15d
	leaq	(%rdi,%rdx,2), %r14
.L528:
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jg	.L510
	xorl	%eax, %eax
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L730:
	movl	$0, -60(%rbp)
	cmpl	$15, %ecx
	jne	.L578
.L517:
	movl	$0, (%r10)
.L521:
	testl	%r15d, %r15d
	jle	.L526
	testq	%r14, %r14
	jne	.L526
	movl	-60(%rbp), %edx
	movl	$1, (%r10)
	movl	$1, %ecx
	testl	%edx, %edx
	je	.L578
	xorl	%r14d, %r14d
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L570:
	movl	%ebx, -160(%rbp)
	xorl	%eax, %eax
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L731:
	movl	$0, -164(%rbp)
	xorl	%edx, %edx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L526:
	movq	-152(%rbp), %rdx
	movq	%r10, %r9
	movq	%r14, %rcx
	movl	%r15d, %r8d
	movq	-144(%rbp), %rdi
	movq	%r11, %rsi
	movq	%r10, -176(%rbp)
	call	uloc_getDisplayKeywordValue_67.part.0
	movl	-60(%rbp), %edx
	movq	-176(%rbp), %r10
	testl	%edx, %edx
	movl	(%r10), %ecx
	je	.L528
	testl	%eax, %eax
	je	.L525
	addl	%edx, %eax
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L586:
	movb	$1, -181(%rbp)
	movl	$0, -132(%rbp)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L723:
	testl	%eax, %eax
	jle	.L567
	cmpq	$30, -240(%rbp)
	leal	-1(%rax), %ecx
	jbe	.L488
	cmpl	$6, %ecx
	jbe	.L488
	shrl	$3, %eax
	movq	-192(%rbp), %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	xorl	%eax, %eax
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L489:
	movdqu	(%rsi,%rax), %xmm2
	movups	%xmm2, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L489
	movl	-132(%rbp), %ebx
	movq	-104(%rbp), %rsi
	movl	%ebx, %eax
	andl	$-8, %eax
	movl	%eax, %edi
	leaq	(%rdi,%rdi), %rdx
	addq	%rdx, %rsi
	cmpl	%eax, %ebx
	je	.L491
	movq	-192(%rbp), %r8
	movzwl	(%r8,%rdi,2), %edi
	movw	%di, (%rsi)
	leal	1(%rax), %edi
	cmpl	%edi, %ebx
	jle	.L491
	movzwl	2(%r8,%rdx), %edi
	movw	%di, 2(%rsi)
	leal	2(%rax), %edi
	cmpl	%edi, %ebx
	jle	.L491
	movzwl	4(%r8,%rdx), %edi
	movw	%di, 4(%rsi)
	leal	3(%rax), %edi
	cmpl	%edi, %ebx
	jle	.L491
	movzwl	6(%r8,%rdx), %edi
	movw	%di, 6(%rsi)
	leal	4(%rax), %edi
	cmpl	%edi, %ebx
	jle	.L491
	movzwl	8(%r8,%rdx), %edi
	movw	%di, 8(%rsi)
	leal	5(%rax), %edi
	cmpl	%edi, %ebx
	jle	.L491
	movzwl	10(%r8,%rdx), %edi
	addl	$6, %eax
	movw	%di, 10(%rsi)
	cmpl	%eax, %ebx
	jle	.L491
	movzwl	12(%r8,%rdx), %eax
	movw	%ax, 12(%rsi)
.L491:
	movq	-104(%rbp), %rax
	leaq	2(%rax,%rcx,2), %r14
	movl	-132(%rbp), %eax
	movl	%eax, -112(%rbp)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L575:
	xorl	%r15d, %r15d
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L545:
	movq	-192(%rbp), %rax
	leaq	-22(%rax,%rdi), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L550:
	movzwl	6(%r9,%rax,2), %edi
	movw	%di, (%r14,%rax,2)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%r8, %rdi
	jne	.L550
	jmp	.L549
.L536:
	movq	-216(%rbp), %r8
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L540:
	movzwl	6(%r8,%rax,2), %esi
	movw	%si, (%r14,%rax,2)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rdx, %rsi
	jne	.L540
	jmp	.L539
.L552:
	movslq	-160(%rbp), %rsi
	addq	%rsi, %rsi
	jmp	.L553
.L583:
	movl	%esi, -112(%rbp)
	jmp	.L716
.L488:
	movq	-192(%rbp), %rsi
	movq	-104(%rbp), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L492:
	movzwl	(%rsi,%rax,2), %edx
	movw	%dx, (%rdi,%rax,2)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L492
	jmp	.L491
.L727:
	movq	-192(%rbp), %rdi
	movl	$9, %edx
	leaq	_ZZ22uloc_getDisplayName_67E14defaultPattern(%rip), %rsi
	movq	%r11, -96(%rbp)
	call	u_strncmp_67@PLT
	movq	-96(%rbp), %r11
	testl	%eax, %eax
	je	.L485
	jmp	.L484
.L567:
	movl	$0, -112(%rbp)
	jmp	.L487
.L733:
	movl	(%r10), %ecx
	cmpl	$15, %ecx
	je	.L517
	testl	%ecx, %ecx
	jle	.L521
	xorl	%eax, %eax
	jmp	.L507
.L722:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2491:
	.size	uloc_getDisplayName_67, .-uloc_getDisplayName_67
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale14getDisplayNameERKS0_RNS_13UnicodeStringE
	.type	_ZNK6icu_676Locale14getDisplayNameERKS0_RNS_13UnicodeStringE, @function
_ZNK6icu_676Locale14getDisplayNameERKS0_RNS_13UnicodeStringE:
.LFB2481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$157, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L763
	movq	%rax, %rdx
	movl	$27, %ecx
	testb	$2, 8(%r12)
	jne	.L741
	movl	16(%r12), %ecx
.L741:
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	leaq	-60(%rbp), %r15
	movq	%r15, %r8
	call	uloc_getDisplayName_67
	movl	-60(%rbp), %edx
	movl	$0, %esi
	movq	%r12, %rdi
	movl	%eax, %r14d
	testl	%edx, %edx
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -60(%rbp)
	je	.L764
.L737:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L765
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L763
	movl	$0, -60(%rbp)
	movl	$27, %ecx
	testb	$2, 8(%r12)
	jne	.L748
	movl	16(%r12), %ecx
.L748:
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	movq	%r15, %r8
	call	uloc_getDisplayName_67
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L763:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L762
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L762:
	testw	%ax, %ax
	js	.L746
	movswl	%ax, %edx
	sarl	$5, %edx
.L747:
	testl	%edx, %edx
	je	.L737
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L746:
	movl	12(%r12), %edx
	jmp	.L747
.L765:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2481:
	.size	_ZNK6icu_676Locale14getDisplayNameERKS0_RNS_13UnicodeStringE, .-_ZNK6icu_676Locale14getDisplayNameERKS0_RNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE
	.type	_ZN6icu_6713BreakIterator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE, @function
_ZN6icu_6713BreakIterator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE:
.LFB2483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$157, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L795
	movq	%rax, %rdx
	movl	$27, %ecx
	testb	$2, 8(%r12)
	jne	.L773
	movl	16(%r12), %ecx
.L773:
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	leaq	-60(%rbp), %r15
	movq	%r15, %r8
	call	uloc_getDisplayName_67
	movl	-60(%rbp), %edx
	movl	$0, %esi
	movq	%r12, %rdi
	movl	%eax, %r14d
	testl	%edx, %edx
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -60(%rbp)
	je	.L796
.L769:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L797
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L795
	movl	$0, -60(%rbp)
	movl	$27, %ecx
	testb	$2, 8(%r12)
	jne	.L780
	movl	16(%r12), %ecx
.L780:
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	movq	%r15, %r8
	call	uloc_getDisplayName_67
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L795:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L794
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L794:
	testw	%ax, %ax
	js	.L778
	movswl	%ax, %edx
	sarl	$5, %edx
.L779:
	testl	%edx, %edx
	je	.L769
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L778:
	movl	12(%r12), %edx
	jmp	.L779
.L797:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2483:
	.size	_ZN6icu_6713BreakIterator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE, .-_ZN6icu_6713BreakIterator14getDisplayNameERKNS_6LocaleES3_RNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale14getDisplayNameERNS_13UnicodeStringE
	.type	_ZNK6icu_676Locale14getDisplayNameERNS_13UnicodeStringE, @function
_ZNK6icu_676Locale14getDisplayNameERNS_13UnicodeStringE:
.LFB2480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	$157, %esi
	movq	%r12, %rdi
	movl	$0, -60(%rbp)
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L827
	movq	%rax, %rdx
	movl	$27, %ecx
	testb	$2, 8(%r12)
	jne	.L805
	movl	16(%r12), %ecx
.L805:
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	leaq	-60(%rbp), %r15
	movq	%r15, %r8
	call	uloc_getDisplayName_67
	movl	-60(%rbp), %edx
	movl	$0, %esi
	movq	%r12, %rdi
	movl	%eax, %r14d
	testl	%edx, %edx
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -60(%rbp)
	je	.L828
.L801:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L829
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L828:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L827
	movl	$0, -60(%rbp)
	movl	$27, %ecx
	testb	$2, 8(%r12)
	jne	.L812
	movl	16(%r12), %ecx
.L812:
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	movq	%r15, %r8
	call	uloc_getDisplayName_67
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L827:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L826
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L826:
	testw	%ax, %ax
	js	.L810
	movswl	%ax, %edx
	sarl	$5, %edx
.L811:
	testl	%edx, %edx
	je	.L801
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L810:
	movl	12(%r12), %edx
	jmp	.L811
.L829:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2480:
	.size	_ZNK6icu_676Locale14getDisplayNameERNS_13UnicodeStringE, .-_ZNK6icu_676Locale14getDisplayNameERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator14getDisplayNameERKNS_6LocaleERNS_13UnicodeStringE
	.type	_ZN6icu_6713BreakIterator14getDisplayNameERKNS_6LocaleERNS_13UnicodeStringE, @function
_ZN6icu_6713BreakIterator14getDisplayNameERKNS_6LocaleERNS_13UnicodeStringE:
.LFB2482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movl	$157, %esi
	movq	%r12, %rdi
	movl	$0, -60(%rbp)
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	testq	%rax, %rax
	je	.L859
	movq	%rax, %rdx
	movl	$27, %ecx
	testb	$2, 8(%r12)
	jne	.L837
	movl	16(%r12), %ecx
.L837:
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	leaq	-60(%rbp), %r15
	movq	%r15, %r8
	call	uloc_getDisplayName_67
	movl	-60(%rbp), %edx
	movl	$0, %esi
	movq	%r12, %rdi
	movl	%eax, %r14d
	testl	%edx, %edx
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	cmpl	$15, -60(%rbp)
	je	.L860
.L833:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L861
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L860:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L859
	movl	$0, -60(%rbp)
	movl	$27, %ecx
	testb	$2, 8(%r12)
	jne	.L844
	movl	16(%r12), %ecx
.L844:
	movq	40(%r13), %rsi
	movq	40(%rbx), %rdi
	movq	%r15, %r8
	call	uloc_getDisplayName_67
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L859:
	movzwl	8(%r12), %eax
	testb	$1, %al
	je	.L858
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L858:
	testw	%ax, %ax
	js	.L842
	movswl	%ax, %edx
	sarl	$5, %edx
.L843:
	testl	%edx, %edx
	je	.L833
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L842:
	movl	12(%r12), %edx
	jmp	.L843
.L861:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2482:
	.size	_ZN6icu_6713BreakIterator14getDisplayNameERKNS_6LocaleERNS_13UnicodeStringE, .-_ZN6icu_6713BreakIterator14getDisplayNameERKNS_6LocaleERNS_13UnicodeStringE
	.p2align 4
	.globl	uloc_getDisplayKeyword_67
	.type	uloc_getDisplayKeyword_67, @function
uloc_getDisplayKeyword_67:
.LFB2492:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L866
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L875
	testl	%ecx, %ecx
	js	.L864
	jle	.L865
	testq	%rdx, %rdx
	jne	.L865
.L864:
	movl	$1, (%r8)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L875:
	ret
	.p2align 4,,10
	.p2align 3
.L865:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r8
	movq	%rdi, %r8
	leaq	.LC2(%rip), %rdi
	pushq	%rcx
	xorl	%ecx, %ecx
	pushq	%rdx
	leaq	_ZL6_kKeys(%rip), %rdx
	call	_ZL19_getStringOrCopyKeyPKcS0_S0_S0_S0_S0_PDsiP10UErrorCode
	addq	$32, %rsp
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2492:
	.size	uloc_getDisplayKeyword_67, .-uloc_getDisplayKeyword_67
	.p2align 4
	.globl	uloc_getDisplayKeywordValue_67
	.type	uloc_getDisplayKeywordValue_67, @function
uloc_getDisplayKeywordValue_67:
.LFB2493:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L877
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L877
	testl	%r8d, %r8d
	js	.L879
	jle	.L880
	testq	%rcx, %rcx
	jne	.L880
.L879:
	movl	$1, (%r9)
.L877:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L880:
	jmp	uloc_getDisplayKeywordValue_67.part.0
	.cfi_endproc
.LFE2493:
	.size	uloc_getDisplayKeywordValue_67, .-uloc_getDisplayKeywordValue_67
	.section	.rodata
	.align 16
	.type	_ZZ22uloc_getDisplayName_67E14defaultPattern, @object
	.size	_ZZ22uloc_getDisplayName_67E14defaultPattern, 20
_ZZ22uloc_getDisplayName_67E14defaultPattern:
	.value	123
	.value	48
	.value	125
	.value	32
	.value	40
	.value	123
	.value	49
	.value	125
	.value	41
	.zero	2
	.align 8
	.type	_ZZ22uloc_getDisplayName_67E4sub1, @object
	.size	_ZZ22uloc_getDisplayName_67E4sub1, 8
_ZZ22uloc_getDisplayName_67E4sub1:
	.value	123
	.value	49
	.value	125
	.zero	2
	.align 8
	.type	_ZZ22uloc_getDisplayName_67E4sub0, @object
	.size	_ZZ22uloc_getDisplayName_67E4sub0, 8
_ZZ22uloc_getDisplayName_67E4sub0:
	.value	123
	.value	48
	.value	125
	.zero	2
	.align 16
	.type	_ZZ22uloc_getDisplayName_67E16defaultSeparator, @object
	.size	_ZZ22uloc_getDisplayName_67E16defaultSeparator, 18
_ZZ22uloc_getDisplayName_67E16defaultSeparator:
	.value	123
	.value	48
	.value	125
	.value	44
	.value	32
	.value	123
	.value	49
	.value	125
	.zero	2
	.align 8
	.type	_ZL11_kSeparator, @object
	.size	_ZL11_kSeparator, 10
_ZL11_kSeparator:
	.string	"separator"
	.align 8
	.type	_ZL9_kPattern, @object
	.size	_ZL9_kPattern, 8
_ZL9_kPattern:
	.string	"pattern"
	.align 16
	.type	_ZL22_kLocaleDisplayPattern, @object
	.size	_ZL22_kLocaleDisplayPattern, 21
_ZL22_kLocaleDisplayPattern:
	.string	"localeDisplayPattern"
	.align 8
	.type	_ZL12_kCurrencies, @object
	.size	_ZL12_kCurrencies, 11
_ZL12_kCurrencies:
	.string	"Currencies"
	.align 8
	.type	_ZL10_kCurrency, @object
	.size	_ZL10_kCurrency, 9
_ZL10_kCurrency:
	.string	"currency"
	.type	_ZL7_kTypes, @object
	.size	_ZL7_kTypes, 6
_ZL7_kTypes:
	.string	"Types"
	.type	_ZL6_kKeys, @object
	.size	_ZL6_kKeys, 5
_ZL6_kKeys:
	.string	"Keys"
	.align 8
	.type	_ZL10_kVariants, @object
	.size	_ZL10_kVariants, 9
_ZL10_kVariants:
	.string	"Variants"
	.align 8
	.type	_ZL11_kCountries, @object
	.size	_ZL11_kCountries, 10
_ZL11_kCountries:
	.string	"Countries"
	.align 16
	.type	_ZL19_kScriptsStandAlone, @object
	.size	_ZL19_kScriptsStandAlone, 20
_ZL19_kScriptsStandAlone:
	.string	"Scripts%stand-alone"
	.align 8
	.type	_ZL9_kScripts, @object
	.size	_ZL9_kScripts, 8
_ZL9_kScripts:
	.string	"Scripts"
	.align 8
	.type	_ZL11_kLanguages, @object
	.size	_ZL11_kLanguages, 10
_ZL11_kLanguages:
	.string	"Languages"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
