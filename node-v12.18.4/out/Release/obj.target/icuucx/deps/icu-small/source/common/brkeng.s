	.file	"brkeng.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723ICULanguageBreakFactoryD2Ev
	.type	_ZN6icu_6723ICULanguageBreakFactoryD2Ev, @function
_ZN6icu_6723ICULanguageBreakFactoryD2Ev:
.LFB3311:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6723ICULanguageBreakFactoryE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE3311:
	.size	_ZN6icu_6723ICULanguageBreakFactoryD2Ev, .-_ZN6icu_6723ICULanguageBreakFactoryD2Ev
	.globl	_ZN6icu_6723ICULanguageBreakFactoryD1Ev
	.set	_ZN6icu_6723ICULanguageBreakFactoryD1Ev,_ZN6icu_6723ICULanguageBreakFactoryD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723ICULanguageBreakFactoryD0Ev
	.type	_ZN6icu_6723ICULanguageBreakFactoryD0Ev, @function
_ZN6icu_6723ICULanguageBreakFactoryD0Ev:
.LFB3313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6723ICULanguageBreakFactoryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	call	*8(%rax)
.L5:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3313:
	.size	_ZN6icu_6723ICULanguageBreakFactoryD0Ev, .-_ZN6icu_6723ICULanguageBreakFactoryD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3698:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3698:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3701:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L23
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L11
	cmpb	$0, 12(%rbx)
	jne	.L24
.L15:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L15
	.cfi_endproc
.LFE3701:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3704:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L27
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3704:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3707:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L30
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3707:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L36
.L32:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L37
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3709:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3710:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3710:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3711:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3711:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3712:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3712:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3713:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3713:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3714:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3714:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3715:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L53
	testl	%edx, %edx
	jle	.L53
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L56
.L45:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L45
	.cfi_endproc
.LFE3715:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L60
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L60
	testl	%r12d, %r12d
	jg	.L67
	cmpb	$0, 12(%rbx)
	jne	.L68
.L62:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L62
.L68:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3716:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L70
	movq	(%rdi), %r8
.L71:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L74
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L74
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3717:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3718:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L81
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3718:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3719:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3719:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3720:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3720:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3721:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3721:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3723:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3723:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3725:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3725:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719LanguageBreakEngineC2Ev
	.type	_ZN6icu_6719LanguageBreakEngineC2Ev, @function
_ZN6icu_6719LanguageBreakEngineC2Ev:
.LFB3284:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6719LanguageBreakEngineE(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE3284:
	.size	_ZN6icu_6719LanguageBreakEngineC2Ev, .-_ZN6icu_6719LanguageBreakEngineC2Ev
	.globl	_ZN6icu_6719LanguageBreakEngineC1Ev
	.set	_ZN6icu_6719LanguageBreakEngineC1Ev,_ZN6icu_6719LanguageBreakEngineC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719LanguageBreakEngineD2Ev
	.type	_ZN6icu_6719LanguageBreakEngineD2Ev, @function
_ZN6icu_6719LanguageBreakEngineD2Ev:
.LFB3287:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3287:
	.size	_ZN6icu_6719LanguageBreakEngineD2Ev, .-_ZN6icu_6719LanguageBreakEngineD2Ev
	.globl	_ZN6icu_6719LanguageBreakEngineD1Ev
	.set	_ZN6icu_6719LanguageBreakEngineD1Ev,_ZN6icu_6719LanguageBreakEngineD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719LanguageBreakEngineD0Ev
	.type	_ZN6icu_6719LanguageBreakEngineD0Ev, @function
_ZN6icu_6719LanguageBreakEngineD0Ev:
.LFB3289:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3289:
	.size	_ZN6icu_6719LanguageBreakEngineD0Ev, .-_ZN6icu_6719LanguageBreakEngineD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720LanguageBreakFactoryC2Ev
	.type	_ZN6icu_6720LanguageBreakFactoryC2Ev, @function
_ZN6icu_6720LanguageBreakFactoryC2Ev:
.LFB3291:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6720LanguageBreakFactoryE(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE3291:
	.size	_ZN6icu_6720LanguageBreakFactoryC2Ev, .-_ZN6icu_6720LanguageBreakFactoryC2Ev
	.globl	_ZN6icu_6720LanguageBreakFactoryC1Ev
	.set	_ZN6icu_6720LanguageBreakFactoryC1Ev,_ZN6icu_6720LanguageBreakFactoryC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720LanguageBreakFactoryD2Ev
	.type	_ZN6icu_6720LanguageBreakFactoryD2Ev, @function
_ZN6icu_6720LanguageBreakFactoryD2Ev:
.LFB3294:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3294:
	.size	_ZN6icu_6720LanguageBreakFactoryD2Ev, .-_ZN6icu_6720LanguageBreakFactoryD2Ev
	.globl	_ZN6icu_6720LanguageBreakFactoryD1Ev
	.set	_ZN6icu_6720LanguageBreakFactoryD1Ev,_ZN6icu_6720LanguageBreakFactoryD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720LanguageBreakFactoryD0Ev
	.type	_ZN6icu_6720LanguageBreakFactoryD0Ev, @function
_ZN6icu_6720LanguageBreakFactoryD0Ev:
.LFB3296:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3296:
	.size	_ZN6icu_6720LanguageBreakFactoryD0Ev, .-_ZN6icu_6720LanguageBreakFactoryD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715UnhandledEngineC2ER10UErrorCode
	.type	_ZN6icu_6715UnhandledEngineC2ER10UErrorCode, @function
_ZN6icu_6715UnhandledEngineC2ER10UErrorCode:
.LFB3298:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6715UnhandledEngineE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE3298:
	.size	_ZN6icu_6715UnhandledEngineC2ER10UErrorCode, .-_ZN6icu_6715UnhandledEngineC2ER10UErrorCode
	.globl	_ZN6icu_6715UnhandledEngineC1ER10UErrorCode
	.set	_ZN6icu_6715UnhandledEngineC1ER10UErrorCode,_ZN6icu_6715UnhandledEngineC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723ICULanguageBreakFactoryC2ER10UErrorCode
	.type	_ZN6icu_6723ICULanguageBreakFactoryC2ER10UErrorCode, @function
_ZN6icu_6723ICULanguageBreakFactoryC2ER10UErrorCode:
.LFB3308:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6723ICULanguageBreakFactoryE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE3308:
	.size	_ZN6icu_6723ICULanguageBreakFactoryC2ER10UErrorCode, .-_ZN6icu_6723ICULanguageBreakFactoryC2ER10UErrorCode
	.globl	_ZN6icu_6723ICULanguageBreakFactoryC1ER10UErrorCode
	.set	_ZN6icu_6723ICULanguageBreakFactoryC1ER10UErrorCode,_ZN6icu_6723ICULanguageBreakFactoryC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715UnhandledEngineD2Ev
	.type	_ZN6icu_6715UnhandledEngineD2Ev, @function
_ZN6icu_6715UnhandledEngineD2Ev:
.LFB3301:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6715UnhandledEngineE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L95
	jmp	_ZN6icu_6710UnicodeSetD0Ev@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	ret
	.cfi_endproc
.LFE3301:
	.size	_ZN6icu_6715UnhandledEngineD2Ev, .-_ZN6icu_6715UnhandledEngineD2Ev
	.globl	_ZN6icu_6715UnhandledEngineD1Ev
	.set	_ZN6icu_6715UnhandledEngineD1Ev,_ZN6icu_6715UnhandledEngineD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715UnhandledEngineD0Ev
	.type	_ZN6icu_6715UnhandledEngineD0Ev, @function
_ZN6icu_6715UnhandledEngineD0Ev:
.LFB3303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715UnhandledEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L98
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L98:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3303:
	.size	_ZN6icu_6715UnhandledEngineD0Ev, .-_ZN6icu_6715UnhandledEngineD0Ev
	.p2align 4
	.type	_deleteEngine, @function
_deleteEngine:
.LFB3314:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L103
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6715UnhandledEngineD0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L105
	leaq	16+_ZTVN6icu_6715UnhandledEngineE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L106:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L103:
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE3314:
	.size	_deleteEngine, .-_deleteEngine
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723ICULanguageBreakFactory13loadEngineForEi
	.type	_ZN6icu_6723ICULanguageBreakFactory13loadEngineForEi, @function
_ZN6icu_6723ICULanguageBreakFactory13loadEngineForEi:
.LFB3322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	%esi, %edi
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-44(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rsi
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	uscript_getScript_67@PLT
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jg	.L154
	movl	%eax, %ebx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movl	%ebx, %esi
	call	*32(%rax)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L154
	leal	-17(%rbx), %eax
	cmpl	$21, %eax
	ja	.L117
	leaq	.L119(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L119:
	.long	.L123-.L119
	.long	.L124-.L119
	.long	.L117-.L119
	.long	.L123-.L119
	.long	.L117-.L119
	.long	.L123-.L119
	.long	.L122-.L119
	.long	.L121-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L120-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L117-.L119
	.long	.L118-.L119
	.text
.L123:
	movl	$1024, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L117
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6714CjkBreakEngineC1EPNS_17DictionaryMatcherENS_12LanguageTypeER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L113
	movq	0(%r13), %rax
	leaq	_ZN6icu_6715UnhandledEngineD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L128
	movq	8(%r13), %rdi
	leaq	16+_ZTVN6icu_6715UnhandledEngineE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L129
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L129:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L154:
	xorl	%r13d, %r13d
.L113:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	*8(%rax)
	jmp	.L113
.L124:
	movl	$1024, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L117
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6714CjkBreakEngineC1EPNS_17DictionaryMatcherENS_12LanguageTypeER10UErrorCode@PLT
	jmp	.L126
.L122:
	movl	$1016, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L117
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6716KhmerBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode@PLT
	jmp	.L126
.L121:
	movl	$1016, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L117
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6714LaoBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode@PLT
	jmp	.L126
.L120:
	movl	$1016, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L117
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6718BurmeseBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode@PLT
	jmp	.L126
.L118:
	movl	$1216, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L117
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715ThaiBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode@PLT
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*%rax
	jmp	.L113
.L155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3322:
	.size	_ZN6icu_6723ICULanguageBreakFactory13loadEngineForEi, .-_ZN6icu_6723ICULanguageBreakFactory13loadEngineForEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715UnhandledEngine7handlesEi
	.type	_ZNK6icu_6715UnhandledEngine7handlesEi, @function
_ZNK6icu_6715UnhandledEngine7handlesEi:
.LFB3304:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L160
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3304:
	.size	_ZNK6icu_6715UnhandledEngine7handlesEi, .-_ZNK6icu_6715UnhandledEngine7handlesEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715UnhandledEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.type	_ZNK6icu_6715UnhandledEngine10findBreaksEP5UTextiiRNS_9UVector32E, @function
_ZNK6icu_6715UnhandledEngine10findBreaksEP5UTextiiRNS_9UVector32E:
.LFB3305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	utext_current32_67@PLT
	movl	%eax, %r12d
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L172:
	movq	8(%r14), %rdi
	movl	%r12d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L166
	movq	%rbx, %rdi
	call	utext_next32_67@PLT
	movq	%rbx, %rdi
	call	utext_current32_67@PLT
	movl	%eax, %r12d
.L167:
	movq	%rbx, %rdi
	call	utext_getNativeIndex_67@PLT
	cmpl	%eax, %r13d
	jg	.L172
.L166:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3305:
	.size	_ZNK6icu_6715UnhandledEngine10findBreaksEP5UTextiiRNS_9UVector32E, .-_ZNK6icu_6715UnhandledEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715UnhandledEngine15handleCharacterEi
	.type	_ZN6icu_6715UnhandledEngine15handleCharacterEi, @function
_ZN6icu_6715UnhandledEngine15handleCharacterEi:
.LFB3306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L179
.L174:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L180
.L173:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L181
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movl	%r13d, %edi
	movl	$4106, %esi
	movl	$0, -44(%rbp)
	call	u_getIntPropertyValue_67@PLT
	movq	8(%rbx), %rdi
	leaq	-44(%rbp), %rcx
	movl	$4106, %esi
	movl	%eax, %edx
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L179:
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L175
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, 8(%rbx)
	jmp	.L174
.L181:
	call	__stack_chk_fail@PLT
.L175:
	movq	$0, 8(%rbx)
	jmp	.L173
	.cfi_endproc
.LFE3306:
	.size	_ZN6icu_6715UnhandledEngine15handleCharacterEi, .-_ZN6icu_6715UnhandledEngine15handleCharacterEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723ICULanguageBreakFactory12getEngineForEi
	.type	_ZN6icu_6723ICULanguageBreakFactory12getEngineForEi, @function
_ZN6icu_6723ICULanguageBreakFactory12getEngineForEi:
.LFB3315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	_ZZN6icu_6723ICULanguageBreakFactory12getEngineForEiE17gBreakEngineMutex(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	umtx_lock_67@PLT
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L211
	movl	8(%r12), %ebx
	leaq	_ZNK6icu_6715UnhandledEngine7handlesEi(%rip), %r15
	subl	$1, %ebx
	jns	.L186
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L212:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L188
	movl	%r14d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	setne	%al
	testb	%al, %al
	jne	.L184
.L188:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	je	.L191
	movq	8(%r13), %r12
.L186:
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L188
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	je	.L212
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*%rax
	testb	%al, %al
	je	.L188
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	_ZZN6icu_6723ICULanguageBreakFactory12getEngineForEiE17gBreakEngineMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L213
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L184
	movq	%rax, %rdi
	leaq	-60(%rbp), %rcx
	leaq	_deleteEngine(%rip), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_676UStackC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L214
	movq	%r15, 8(%r13)
.L191:
	movq	0(%r13), %rax
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L184
	movq	8(%r13), %rdi
	leaq	-60(%rbp), %rdx
	movq	%rax, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	jmp	.L184
.L214:
	movq	%r15, %rdi
	call	_ZN6icu_676UStackD0Ev@PLT
	jmp	.L184
.L213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3315:
	.size	_ZN6icu_6723ICULanguageBreakFactory12getEngineForEi, .-_ZN6icu_6723ICULanguageBreakFactory12getEngineForEi
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"icudt67l-brkitr"
.LC2:
	.string	"dictionaries"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723ICULanguageBreakFactory24loadDictionaryMatcherForE11UScriptCode
	.type	_ZN6icu_6723ICULanguageBreakFactory24loadDictionaryMatcherForE11UScriptCode, @function
_ZN6icu_6723ICULanguageBreakFactory24loadDictionaryMatcherForE11UScriptCode:
.LFB3323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-272(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdx
	.cfi_offset 12, -48
	movl	%esi, %r12d
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -272(%rbp)
	call	ures_open_67@PLT
	movq	%r13, %rcx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	call	ures_getByKeyWithFallback_67@PLT
	movl	%r12d, %edi
	movl	$0, -268(%rbp)
	movq	%rax, %r14
	call	uscript_getShortName_67@PLT
	leaq	-268(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	ures_getStringByKeyWithFallback_67@PLT
	movl	-272(%rbp), %esi
	testl	%esi, %esi
	jg	.L236
	xorl	%edx, %edx
	movq	%rax, %r12
	leaq	-243(%rbp), %rax
	xorl	%ecx, %ecx
	movw	%dx, -244(%rbp)
	movl	-268(%rbp), %edx
	movl	$46, %esi
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	leaq	-179(%rbp), %rax
	leaq	-192(%rbp), %r15
	movl	$0, -200(%rbp)
	movl	$40, -248(%rbp)
	movq	%rax, -192(%rbp)
	movl	$0, -136(%rbp)
	movl	$40, -184(%rbp)
	movw	%cx, -180(%rbp)
	call	u_memrchr_67@PLT
	testq	%rax, %rax
	je	.L237
	movq	%rax, %r8
	leaq	-264(%rbp), %r10
	leaq	-128(%rbp), %rbx
	xorl	%esi, %esi
	subq	%r12, %r8
	movq	%r10, %rdx
	addq	$2, %rax
	movq	%rbx, %rdi
	sarq	%r8
	movq	%r10, -280(%rbp)
	movl	%r8d, %ecx
	movl	%r8d, -292(%rbp)
	notl	%ecx
	addl	-268(%rbp), %ecx
	movq	%r8, -288(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-288(%rbp), %r8
	movl	-292(%rbp), %r9d
	movq	-280(%rbp), %r10
	movl	%r8d, -268(%rbp)
.L219:
	movq	%r10, %rdx
	movl	%r9d, %ecx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	leaq	-256(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	ures_close_67@PLT
	movq	-256(%rbp), %rdx
	movq	%r13, %rcx
	movq	-192(%rbp), %rsi
	leaq	.LC1(%rip), %rdi
	call	udata_open_67@PLT
	movq	%rax, %r13
	movl	-272(%rbp), %eax
	testl	%eax, %eax
	jle	.L238
	testq	%r12, %r12
	je	.L223
	movl	$0, -272(%rbp)
	xorl	%r12d, %r12d
.L223:
	cmpb	$0, -180(%rbp)
	jne	.L239
.L224:
	cmpb	$0, -244(%rbp)
	jne	.L240
.L215:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	addq	$264, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movl	-268(%rbp), %r9d
	leaq	-264(%rbp), %r10
	leaq	-128(%rbp), %rbx
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L240:
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	ures_close_67@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%r13, %rdi
	call	udata_getMemory_67@PLT
	movl	16(%rax), %edx
	andl	_ZN6icu_6714DictionaryData14TRIE_TYPE_MASKE(%rip), %edx
	cmpl	%edx, _ZN6icu_6714DictionaryData15TRIE_TYPE_BYTESE(%rip)
	movslq	(%rax), %rbx
	je	.L242
	cmpl	%edx, _ZN6icu_6714DictionaryData16TRIE_TYPE_UCHARSE(%rip)
	je	.L243
.L222:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	udata_close_67@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L242:
	movl	$32, %edi
	movl	20(%rax), %r14d
	addq	%rax, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L222
	leaq	16+_ZTVN6icu_6722BytesDictionaryMatcherE(%rip), %rax
	movq	%rbx, 8(%r12)
	movq	%rax, (%r12)
	movl	%r14d, 16(%r12)
	movq	%r13, 24(%r12)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$24, %edi
	addq	%rax, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L222
	leaq	16+_ZTVN6icu_6723UCharsDictionaryMatcherE(%rip), %rax
	movq	%rbx, 8(%r12)
	movq	%rax, (%r12)
	movq	%r13, 16(%r12)
	jmp	.L223
.L241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3323:
	.size	_ZN6icu_6723ICULanguageBreakFactory24loadDictionaryMatcherForE11UScriptCode, .-_ZN6icu_6723ICULanguageBreakFactory24loadDictionaryMatcherForE11UScriptCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6719LanguageBreakEngineE
	.section	.rodata._ZTSN6icu_6719LanguageBreakEngineE,"aG",@progbits,_ZTSN6icu_6719LanguageBreakEngineE,comdat
	.align 16
	.type	_ZTSN6icu_6719LanguageBreakEngineE, @object
	.size	_ZTSN6icu_6719LanguageBreakEngineE, 31
_ZTSN6icu_6719LanguageBreakEngineE:
	.string	"N6icu_6719LanguageBreakEngineE"
	.weak	_ZTIN6icu_6719LanguageBreakEngineE
	.section	.data.rel.ro._ZTIN6icu_6719LanguageBreakEngineE,"awG",@progbits,_ZTIN6icu_6719LanguageBreakEngineE,comdat
	.align 8
	.type	_ZTIN6icu_6719LanguageBreakEngineE, @object
	.size	_ZTIN6icu_6719LanguageBreakEngineE, 24
_ZTIN6icu_6719LanguageBreakEngineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719LanguageBreakEngineE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6720LanguageBreakFactoryE
	.section	.rodata._ZTSN6icu_6720LanguageBreakFactoryE,"aG",@progbits,_ZTSN6icu_6720LanguageBreakFactoryE,comdat
	.align 32
	.type	_ZTSN6icu_6720LanguageBreakFactoryE, @object
	.size	_ZTSN6icu_6720LanguageBreakFactoryE, 32
_ZTSN6icu_6720LanguageBreakFactoryE:
	.string	"N6icu_6720LanguageBreakFactoryE"
	.weak	_ZTIN6icu_6720LanguageBreakFactoryE
	.section	.data.rel.ro._ZTIN6icu_6720LanguageBreakFactoryE,"awG",@progbits,_ZTIN6icu_6720LanguageBreakFactoryE,comdat
	.align 8
	.type	_ZTIN6icu_6720LanguageBreakFactoryE, @object
	.size	_ZTIN6icu_6720LanguageBreakFactoryE, 24
_ZTIN6icu_6720LanguageBreakFactoryE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720LanguageBreakFactoryE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6715UnhandledEngineE
	.section	.rodata._ZTSN6icu_6715UnhandledEngineE,"aG",@progbits,_ZTSN6icu_6715UnhandledEngineE,comdat
	.align 16
	.type	_ZTSN6icu_6715UnhandledEngineE, @object
	.size	_ZTSN6icu_6715UnhandledEngineE, 27
_ZTSN6icu_6715UnhandledEngineE:
	.string	"N6icu_6715UnhandledEngineE"
	.weak	_ZTIN6icu_6715UnhandledEngineE
	.section	.data.rel.ro._ZTIN6icu_6715UnhandledEngineE,"awG",@progbits,_ZTIN6icu_6715UnhandledEngineE,comdat
	.align 8
	.type	_ZTIN6icu_6715UnhandledEngineE, @object
	.size	_ZTIN6icu_6715UnhandledEngineE, 24
_ZTIN6icu_6715UnhandledEngineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715UnhandledEngineE
	.quad	_ZTIN6icu_6719LanguageBreakEngineE
	.weak	_ZTSN6icu_6723ICULanguageBreakFactoryE
	.section	.rodata._ZTSN6icu_6723ICULanguageBreakFactoryE,"aG",@progbits,_ZTSN6icu_6723ICULanguageBreakFactoryE,comdat
	.align 32
	.type	_ZTSN6icu_6723ICULanguageBreakFactoryE, @object
	.size	_ZTSN6icu_6723ICULanguageBreakFactoryE, 35
_ZTSN6icu_6723ICULanguageBreakFactoryE:
	.string	"N6icu_6723ICULanguageBreakFactoryE"
	.weak	_ZTIN6icu_6723ICULanguageBreakFactoryE
	.section	.data.rel.ro._ZTIN6icu_6723ICULanguageBreakFactoryE,"awG",@progbits,_ZTIN6icu_6723ICULanguageBreakFactoryE,comdat
	.align 8
	.type	_ZTIN6icu_6723ICULanguageBreakFactoryE, @object
	.size	_ZTIN6icu_6723ICULanguageBreakFactoryE, 24
_ZTIN6icu_6723ICULanguageBreakFactoryE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6723ICULanguageBreakFactoryE
	.quad	_ZTIN6icu_6720LanguageBreakFactoryE
	.weak	_ZTVN6icu_6719LanguageBreakEngineE
	.section	.data.rel.ro._ZTVN6icu_6719LanguageBreakEngineE,"awG",@progbits,_ZTVN6icu_6719LanguageBreakEngineE,comdat
	.align 8
	.type	_ZTVN6icu_6719LanguageBreakEngineE, @object
	.size	_ZTVN6icu_6719LanguageBreakEngineE, 48
_ZTVN6icu_6719LanguageBreakEngineE:
	.quad	0
	.quad	_ZTIN6icu_6719LanguageBreakEngineE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6720LanguageBreakFactoryE
	.section	.data.rel.ro._ZTVN6icu_6720LanguageBreakFactoryE,"awG",@progbits,_ZTVN6icu_6720LanguageBreakFactoryE,comdat
	.align 8
	.type	_ZTVN6icu_6720LanguageBreakFactoryE, @object
	.size	_ZTVN6icu_6720LanguageBreakFactoryE, 40
_ZTVN6icu_6720LanguageBreakFactoryE:
	.quad	0
	.quad	_ZTIN6icu_6720LanguageBreakFactoryE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6715UnhandledEngineE
	.section	.data.rel.ro.local._ZTVN6icu_6715UnhandledEngineE,"awG",@progbits,_ZTVN6icu_6715UnhandledEngineE,comdat
	.align 8
	.type	_ZTVN6icu_6715UnhandledEngineE, @object
	.size	_ZTVN6icu_6715UnhandledEngineE, 56
_ZTVN6icu_6715UnhandledEngineE:
	.quad	0
	.quad	_ZTIN6icu_6715UnhandledEngineE
	.quad	_ZN6icu_6715UnhandledEngineD1Ev
	.quad	_ZN6icu_6715UnhandledEngineD0Ev
	.quad	_ZNK6icu_6715UnhandledEngine7handlesEi
	.quad	_ZNK6icu_6715UnhandledEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.quad	_ZN6icu_6715UnhandledEngine15handleCharacterEi
	.weak	_ZTVN6icu_6723ICULanguageBreakFactoryE
	.section	.data.rel.ro.local._ZTVN6icu_6723ICULanguageBreakFactoryE,"awG",@progbits,_ZTVN6icu_6723ICULanguageBreakFactoryE,comdat
	.align 8
	.type	_ZTVN6icu_6723ICULanguageBreakFactoryE, @object
	.size	_ZTVN6icu_6723ICULanguageBreakFactoryE, 56
_ZTVN6icu_6723ICULanguageBreakFactoryE:
	.quad	0
	.quad	_ZTIN6icu_6723ICULanguageBreakFactoryE
	.quad	_ZN6icu_6723ICULanguageBreakFactoryD1Ev
	.quad	_ZN6icu_6723ICULanguageBreakFactoryD0Ev
	.quad	_ZN6icu_6723ICULanguageBreakFactory12getEngineForEi
	.quad	_ZN6icu_6723ICULanguageBreakFactory13loadEngineForEi
	.quad	_ZN6icu_6723ICULanguageBreakFactory24loadDictionaryMatcherForE11UScriptCode
	.local	_ZZN6icu_6723ICULanguageBreakFactory12getEngineForEiE17gBreakEngineMutex
	.comm	_ZZN6icu_6723ICULanguageBreakFactory12getEngineForEiE17gBreakEngineMutex,56,32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
