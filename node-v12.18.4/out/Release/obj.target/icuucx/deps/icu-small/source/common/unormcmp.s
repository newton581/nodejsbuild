	.file	"unormcmp.cpp"
	.text
	.p2align 4
	.type	_ZL10_normalizePKN6icu_6711Normalizer2EPKDsiRNS_13UnicodeStringEP10UErrorCode, @function
_ZL10_normalizePKN6icu_6711Normalizer2EPKDsiRNS_13UnicodeStringEP10UErrorCode:
.LFB3124:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-200(%rbp), %r15
	movq	%rcx, %r14
	movl	%edx, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -200(%rbp)
	movl	%edx, %esi
	movq	%r15, %rdx
	shrl	$31, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	(%r12), %rax
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*112(%rax)
	movl	(%rbx), %edx
	movl	%eax, %r8d
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L2
	movswl	-184(%rbp), %edx
	testw	%dx, %dx
	js	.L3
	sarl	$5, %edx
	xorl	%eax, %eax
	cmpl	%edx, %r8d
	jl	.L14
.L2:
	movq	%r13, %rdi
	movb	%al, -216(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movzbl	-216(%rbp), %eax
	jne	.L15
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movl	-180(%rbp), %edx
	xorl	%eax, %eax
	cmpl	%edx, %r8d
	jge	.L2
.L14:
	leaq	-128(%rbp), %r9
	movl	%r8d, %edx
	movl	$2147483647, %ecx
	movq	%r13, %rsi
	movq	%r9, %rdi
	movl	%r8d, -220(%rbp)
	movq	%r9, -216(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movzwl	-184(%rbp), %edx
	movq	-216(%rbp), %r9
	movl	-220(%rbp), %r8d
	testb	$17, %dl
	jne	.L10
	leaq	-182(%rbp), %rax
	andl	$2, %edx
	cmove	-168(%rbp), %rax
.L5:
	movl	%r8d, %ecx
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%r9, -216(%rbp)
	movq	%r14, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	-216(%rbp), %r9
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	(%r12), %rax
	movq	%r9, %rdx
	call	*40(%rax)
	movl	(%rbx), %eax
	movq	-216(%rbp), %r9
	testl	%eax, %eax
	movq	%r9, %rdi
	jle	.L16
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	xorl	%eax, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L16:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$1, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	jmp	.L5
.L15:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3124:
	.size	_ZL10_normalizePKN6icu_6711Normalizer2EPKDsiRNS_13UnicodeStringEP10UErrorCode, .-_ZL10_normalizePKN6icu_6711Normalizer2EPKDsiRNS_13UnicodeStringEP10UErrorCode
	.p2align 4
	.globl	unorm_compare_67
	.type	unorm_compare_67, @function
unorm_compare_67:
.LFB3125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	jg	.L17
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L19
	movl	%esi, %r15d
	cmpl	$-1, %esi
	jl	.L19
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L19
	movl	%ecx, %r14d
	cmpl	$-1, %ecx
	jl	.L19
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	movl	$2, %r11d
	movl	%r8d, %r13d
	movq	%rax, -320(%rbp)
	movq	%rax, -256(%rbp)
	movl	%r8d, %eax
	andl	$131073, %eax
	movw	%r10w, -312(%rbp)
	movw	%r11w, -248(%rbp)
	cmpl	$131072, %eax
	je	.L215
	movq	%r9, -488(%rbp)
	movq	%r9, %rdi
	testb	$1, %r8b
	je	.L23
	call	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode@PLT
	movq	-488(%rbp), %r9
	movq	%rax, %r10
.L24:
	movl	(%r9), %r8d
	testl	%r8d, %r8d
	jg	.L216
	testl	$33554432, %r13d
	je	.L27
	movq	%r9, %rdi
	movq	%r10, -496(%rbp)
	movq	%r9, -488(%rbp)
	call	uniset_getUnicode32Instance_67@PLT
	movq	-488(%rbp), %r9
	movl	%r15d, %edx
	movq	-496(%rbp), %r10
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rsi
	leaq	-464(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	%rsi, -464(%rbp)
	movq	%r9, %r8
	movq	%rbx, %rsi
	leaq	-320(%rbp), %rcx
	movq	%r9, -504(%rbp)
	movq	%rdi, -488(%rbp)
	movq	%r10, -456(%rbp)
	movq	%rcx, -496(%rbp)
	call	_ZL10_normalizePKN6icu_6711Normalizer2EPKDsiRNS_13UnicodeStringEP10UErrorCode
	movq	-488(%rbp), %rdi
	movq	-504(%rbp), %r9
	testb	%al, %al
	je	.L28
	movzwl	-312(%rbp), %eax
	testb	$17, %al
	jne	.L106
	leaq	-310(%rbp), %rbx
	testb	$2, %al
	cmove	-296(%rbp), %rbx
.L29:
	testw	%ax, %ax
	js	.L30
	movswl	%ax, %r15d
	sarl	$5, %r15d
.L28:
	leaq	-256(%rbp), %rcx
	movq	%r9, %r8
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r9, -512(%rbp)
	movq	%rdi, -488(%rbp)
	movq	%rcx, -504(%rbp)
	call	_ZL10_normalizePKN6icu_6711Normalizer2EPKDsiRNS_13UnicodeStringEP10UErrorCode
	movq	-488(%rbp), %rdi
	movq	-512(%rbp), %r9
	testb	%al, %al
	je	.L31
	movzwl	-248(%rbp), %eax
	testb	$17, %al
	jne	.L108
	leaq	-246(%rbp), %r12
	testb	$2, %al
	cmove	-232(%rbp), %r12
.L32:
	testw	%ax, %ax
	js	.L33
	movswl	%ax, %r14d
	sarl	$5, %r14d
.L31:
	movq	%r9, -488(%rbp)
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
	movq	-488(%rbp), %r9
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	-320(%rbp), %rcx
	movq	%r9, %r8
	movq	%r10, %rdi
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r9, -504(%rbp)
	movq	%r10, -488(%rbp)
	movq	%rcx, -496(%rbp)
	call	_ZL10_normalizePKN6icu_6711Normalizer2EPKDsiRNS_13UnicodeStringEP10UErrorCode
	movq	-488(%rbp), %r10
	movq	-504(%rbp), %r9
	testb	%al, %al
	je	.L35
	movzwl	-312(%rbp), %eax
	testb	$17, %al
	jne	.L110
	leaq	-310(%rbp), %rbx
	testb	$2, %al
	cmove	-296(%rbp), %rbx
.L36:
	testw	%ax, %ax
	js	.L37
	movswl	%ax, %r15d
	sarl	$5, %r15d
.L35:
	leaq	-256(%rbp), %rcx
	movq	%r9, %r8
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r9, -488(%rbp)
	movq	%rcx, -504(%rbp)
	call	_ZL10_normalizePKN6icu_6711Normalizer2EPKDsiRNS_13UnicodeStringEP10UErrorCode
	movq	-488(%rbp), %r9
	testb	%al, %al
	je	.L34
	movzwl	-248(%rbp), %eax
	testb	$17, %al
	jne	.L112
	leaq	-246(%rbp), %r12
	testb	$2, %al
	cmove	-232(%rbp), %r12
.L38:
	testw	%ax, %ax
	js	.L39
	movswl	%ax, %r14d
	sarl	$5, %r14d
	.p2align 4,,10
	.p2align 3
.L34:
	movl	(%r9), %edi
	testl	%edi, %edi
	jle	.L22
.L59:
	xorl	%eax, %eax
.L26:
	movq	-504(%rbp), %rdi
	movl	%eax, -488(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-496(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-488(%rbp), %eax
.L17:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L217
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$1, (%r9)
	xorl	%eax, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L215:
	leaq	-320(%rbp), %rax
	movq	%rax, -496(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, -504(%rbp)
.L22:
	movq	%r9, %rdi
	movq	%r9, -488(%rbp)
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	movq	-488(%rbp), %r9
	movq	%rax, -520(%rbp)
	movl	(%r9), %esi
	testl	%esi, %esi
	jg	.L59
	xorl	%r10d, %r10d
	cmpl	$-1, %r15d
	je	.L43
	movslq	%r15d, %r15
	leaq	(%rbx,%r15,2), %r10
.L43:
	xorl	%r11d, %r11d
	cmpl	$-1, %r14d
	je	.L44
	movslq	%r14d, %r14
	leaq	(%r12,%r14,2), %r11
.L44:
	movl	%r13d, %eax
	xorl	%r8d, %r8d
	movl	$-1, %esi
	xorl	%r14d, %r14d
	orl	$524288, %eax
	movl	%r13d, -576(%rbp)
	movq	%r12, %r15
	movl	%esi, %r9d
	movl	%eax, -580(%rbp)
	movl	%r13d, %eax
	movl	$-1, -488(%rbp)
	andl	$4096, %eax
	movl	%eax, -540(%rbp)
	movl	%r13d, %eax
	movl	%r8d, %r13d
	movq	%rbx, %r8
	andl	$65536, %eax
	movl	%eax, -572(%rbp)
	leaq	-468(%rbp), %rax
	movq	%rax, -536(%rbp)
.L45:
	cmpl	$-1, %r9d
	je	.L218
.L46:
	cmpl	$-1, -488(%rbp)
	je	.L219
.L52:
	cmpl	-488(%rbp), %r9d
	je	.L220
	testl	%r9d, %r9d
	js	.L118
	movl	-488(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L119
	movl	%r9d, %eax
	movl	%r9d, -528(%rbp)
	andl	$-2048, %eax
	movl	%eax, -552(%rbp)
	cmpl	$55296, %eax
	je	.L221
.L61:
	movl	-488(%rbp), %edx
	movl	%edx, %eax
	movl	%edx, -512(%rbp)
	andl	$-2048, %eax
	movl	%eax, -544(%rbp)
	cmpl	$55296, %eax
	je	.L222
.L63:
	testl	%r13d, %r13d
	jne	.L65
	movl	-572(%rbp), %edx
	testl	%edx, %edx
	jne	.L223
.L79:
	movq	-536(%rbp), %rcx
	movl	-528(%rbp), %esi
	leaq	-432(%rbp), %rdx
	movq	%r8, -600(%rbp)
	movq	-520(%rbp), %rdi
	movq	%r11, -592(%rbp)
	movq	%r10, -568(%rbp)
	movl	%r9d, -560(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	-560(%rbp), %r9d
	movq	-568(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -464(%rbp)
	movq	-592(%rbp), %r11
	movq	-600(%rbp), %r8
	je	.L89
	cmpl	$55296, -552(%rbp)
	je	.L224
.L93:
	movslq	%r13d, %rdx
	movq	%r8, %xmm0
	movq	%rbx, %xmm4
	leaq	(%rdx,%rdx,2), %rdx
	punpcklqdq	%xmm4, %xmm0
	salq	$3, %rdx
	movq	%r10, -400(%rbp,%rdx)
	movups	%xmm0, -416(%rbp,%rdx)
	leal	1(%r13), %edx
	cmpl	$1, %r13d
	je	.L130
	movslq	%edx, %rdx
	addl	$2, %r13d
	leaq	(%rdx,%rdx,2), %rdx
	movq	$0, -416(%rbp,%rdx,8)
.L95:
	movslq	-468(%rbp), %rdx
	movq	%rax, %r8
	movq	%rax, %rbx
	movl	$-1, %r9d
	leaq	(%rax,%rdx,2), %r10
	movl	-540(%rbp), %edx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L225:
	subl	$2, %r13d
	movslq	%r13d, %rax
	leaq	(%rax,%rax,2), %rcx
	movq	-416(%rbp,%rcx,8), %r8
.L50:
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	movq	-408(%rbp,%rax), %rbx
	movq	-400(%rbp,%rax), %r10
.L51:
	cmpq	%r10, %rbx
	je	.L47
	movzwl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L48
	testq	%r10, %r10
	je	.L47
	testl	%edx, %edx
	je	.L48
.L47:
	testl	%r13d, %r13d
	je	.L46
	leal	-1(%r13), %ecx
	movslq	%ecx, %rax
	leaq	(%rax,%rax,2), %rsi
	movq	-416(%rbp,%rsi,8), %r8
	testq	%r8, %r8
	je	.L225
	movl	%ecx, %r13d
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L23:
	call	_ZN6icu_6718Normalizer2Factory14getFCDInstanceER10UErrorCode@PLT
	movq	-488(%rbp), %r9
	movq	%rax, %r10
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	-320(%rbp), %rax
	movq	%rax, -496(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, -504(%rbp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L227:
	cmpl	$1, %r13d
	jle	.L79
.L89:
	cmpl	$1, %r14d
	jg	.L213
.L80:
	movq	-536(%rbp), %rcx
	movl	-512(%rbp), %esi
	leaq	-424(%rbp), %rdx
	movq	%r8, -568(%rbp)
	movq	-520(%rbp), %rdi
	movq	%r11, -560(%rbp)
	movq	%r10, -552(%rbp)
	movl	%r9d, -528(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	movl	-528(%rbp), %r9d
	movq	-552(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, -464(%rbp)
	movq	-560(%rbp), %r11
	movq	-568(%rbp), %r8
	je	.L213
	cmpl	$55296, -544(%rbp)
	je	.L226
.L96:
	movslq	%r14d, %rdx
	movq	%r15, %xmm0
	movq	%r12, %xmm1
	leaq	(%rdx,%rdx,2), %rdx
	punpcklqdq	%xmm1, %xmm0
	salq	$3, %rdx
	movq	%r11, -352(%rbp,%rdx)
	movups	%xmm0, -368(%rbp,%rdx)
	leal	1(%r14), %edx
	cmpl	$1, %r14d
	je	.L131
	movslq	%edx, %rdx
	addl	$2, %r14d
	leaq	(%rdx,%rdx,2), %rdx
	movq	$0, -368(%rbp,%rdx,8)
.L98:
	movslq	-468(%rbp), %rdx
	movq	%rax, %r15
	movq	%rax, %r12
	movl	$-1, -488(%rbp)
	leaq	(%rax,%rdx,2), %r11
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L220:
	cmpl	$-1, %r9d
	je	.L59
	movl	$-1, -488(%rbp)
	movl	-540(%rbp), %edx
	movl	$-1, %r9d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L65:
	testl	%r14d, %r14d
	jne	.L227
	movl	-572(%rbp), %eax
	testl	%eax, %eax
	jne	.L228
.L77:
	cmpl	$1, %r13d
	jg	.L80
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L213:
	movl	%r9d, %edi
	movl	-576(%rbp), %r13d
	movq	%r15, %r9
	movq	%r8, %r15
	cmpl	$55295, %edi
	jle	.L92
	cmpl	$55295, -488(%rbp)
	jle	.L92
	andl	$32768, %r13d
	je	.L92
	cmpl	$56319, %edi
	jg	.L99
	cmpq	%r10, %rbx
	je	.L99
	movzwl	(%rbx), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L100
.L99:
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L229
.L101:
	movl	%edi, %eax
	subl	$10240, %eax
	movl	%eax, %edi
.L100:
	cmpl	$56319, -488(%rbp)
	jg	.L102
	cmpq	%r11, %r12
	je	.L102
	movzwl	(%r12), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L92
.L102:
	movl	-488(%rbp), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L230
.L103:
	subl	$10240, -488(%rbp)
	.p2align 4,,10
	.p2align 3
.L92:
	movl	%edi, %eax
	subl	-488(%rbp), %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L222:
	movl	%edx, %esi
	testb	$4, %dh
	jne	.L64
	cmpq	%r11, %r12
	je	.L63
	movzwl	(%r12), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L63
	movl	%esi, %edx
	sall	$10, %edx
	leal	-56613888(%rax,%rdx), %eax
	movl	%eax, -512(%rbp)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L221:
	testl	$1024, %r9d
	jne	.L62
	cmpq	%r10, %rbx
	je	.L61
	movzwl	(%rbx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L61
	movl	%r9d, %edx
	sall	$10, %edx
	leal	-56613888(%rax,%rdx), %eax
	movl	%eax, -528(%rbp)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L219:
	movl	-540(%rbp), %edx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L231:
	subl	$2, %r14d
	movslq	%r14d, %rax
	leaq	(%rax,%rax,2), %rcx
	movq	-368(%rbp,%rcx,8), %r15
.L56:
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	movq	-360(%rbp,%rax), %r12
	movq	-352(%rbp,%rax), %r11
.L57:
	cmpq	%r11, %r12
	je	.L53
	movzwl	(%r12), %eax
	testl	%eax, %eax
	jne	.L54
	testq	%r11, %r11
	je	.L53
	testl	%edx, %edx
	je	.L54
.L53:
	testl	%r14d, %r14d
	je	.L52
	leal	-1(%r14), %ecx
	movslq	%ecx, %rax
	leaq	(%rax,%rax,2), %rsi
	movq	-368(%rbp,%rsi,8), %r15
	testq	%r15, %r15
	je	.L231
	movl	%ecx, %r14d
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L218:
	movl	-540(%rbp), %edx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L54:
	movl	%eax, -488(%rbp)
	addq	$2, %r12
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L48:
	addq	$2, %rbx
	movl	%eax, %r9d
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	-4(%rbx), %rax
	movl	%r9d, -528(%rbp)
	cmpq	%rax, %r8
	ja	.L61
	movzwl	-4(%rbx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L61
	sall	$10, %eax
	leal	-56613888(%r9,%rax), %eax
	movl	%eax, -528(%rbp)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	-4(%r12), %rax
	movl	%edx, -512(%rbp)
	cmpq	%rax, %r15
	ja	.L63
	movzwl	-4(%r12), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L63
	sall	$10, %eax
	leal	-56613888(%rsi,%rax), %eax
	movl	%eax, -512(%rbp)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L223:
	movl	-580(%rbp), %edx
	movl	-528(%rbp), %edi
	leaq	-464(%rbp), %rsi
	movq	%r8, -608(%rbp)
	movq	%r11, -600(%rbp)
	movq	%r10, -592(%rbp)
	movl	%r9d, -568(%rbp)
	movq	%rsi, -560(%rbp)
	call	ucase_toFullFolding_67@PLT
	movq	-560(%rbp), %rsi
	movl	-568(%rbp), %r9d
	testl	%eax, %eax
	movl	%eax, -468(%rbp)
	movq	-592(%rbp), %r10
	movq	-600(%rbp), %r11
	movq	-608(%rbp), %r8
	jns	.L232
	testl	%r14d, %r14d
	jne	.L79
	movl	-580(%rbp), %edx
	movl	-512(%rbp), %edi
	movq	%r8, -600(%rbp)
	movq	%r11, -592(%rbp)
	movq	%r10, -568(%rbp)
	movl	%r9d, -560(%rbp)
	call	ucase_toFullFolding_67@PLT
	movl	-560(%rbp), %r9d
	movq	-568(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, -468(%rbp)
	movq	-592(%rbp), %r11
	movq	-600(%rbp), %r8
	js	.L79
.L78:
	cmpl	$55296, -544(%rbp)
	je	.L233
.L82:
	movq	%r15, %xmm0
	movq	%r12, %xmm3
	movq	%r11, -352(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -368(%rbp)
	cmpl	$31, %eax
	jle	.L234
	cmpl	$65535, %eax
	jg	.L86
	movl	%eax, %ecx
	movl	$2, %r11d
	movl	$1, %eax
.L87:
	movw	%cx, -128(%rbp)
	leaq	-128(%rbp), %r15
	movl	%eax, -468(%rbp)
.L85:
	movl	$-1, -488(%rbp)
	addq	%r15, %r11
	movl	$1, %r14d
	movq	%r15, %r12
	jmp	.L45
.L33:
	movl	-244(%rbp), %r14d
	jmp	.L31
.L37:
	movl	-308(%rbp), %r15d
	jmp	.L35
.L39:
	movl	-244(%rbp), %r14d
	jmp	.L34
.L30:
	movl	-308(%rbp), %r15d
	jmp	.L28
.L226:
	testl	$1024, -488(%rbp)
	jne	.L97
	addq	$2, %r12
	jmp	.L96
.L131:
	movl	$2, %r14d
	jmp	.L98
.L228:
	movl	-580(%rbp), %edx
	movl	-512(%rbp), %edi
	leaq	-464(%rbp), %rsi
	movq	%r8, -600(%rbp)
	movq	%r11, -592(%rbp)
	movq	%r10, -568(%rbp)
	movl	%r9d, -560(%rbp)
	call	ucase_toFullFolding_67@PLT
	movl	-560(%rbp), %r9d
	movq	-568(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, -468(%rbp)
	movq	-592(%rbp), %r11
	movq	-600(%rbp), %r8
	js	.L77
	jmp	.L78
.L97:
	movzwl	-4(%rbx), %r9d
	subq	$2, %rbx
	jmp	.L96
.L232:
	cmpl	$55296, -552(%rbp)
	je	.L235
.L71:
	movq	%r8, %xmm0
	movq	%rbx, %xmm2
	movq	%r10, -400(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -416(%rbp)
	cmpl	$31, %eax
	jle	.L236
	cmpl	$65535, %eax
	jg	.L75
	movl	%eax, %edx
	movl	$2, %r10d
	movl	$1, %eax
.L76:
	movw	%dx, -192(%rbp)
	leaq	-192(%rbp), %rbx
	movl	%eax, -468(%rbp)
.L74:
	movl	-540(%rbp), %edx
	addq	%rbx, %r10
	movl	$-1, %r9d
	movq	%rbx, %r8
	movl	$1, %r13d
	jmp	.L51
.L112:
	xorl	%r12d, %r12d
	jmp	.L38
.L108:
	xorl	%r12d, %r12d
	jmp	.L32
.L110:
	xorl	%ebx, %ebx
	jmp	.L36
.L106:
	xorl	%ebx, %ebx
	jmp	.L29
.L229:
	leaq	-2(%rbx), %rax
	cmpq	%rax, %r15
	je	.L101
	movzwl	-4(%rbx), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L101
	jmp	.L100
.L230:
	leaq	-2(%r12), %rax
	cmpq	%rax, %r9
	je	.L103
	movzwl	-4(%r12), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L103
	jmp	.L92
.L235:
	andl	$1024, %r9d
	jne	.L72
	addq	$2, %rbx
	jmp	.L71
.L75:
	movl	%eax, %edx
	andw	$1023, %ax
	movl	$4, %r10d
	orw	$-9216, %ax
	sarl	$10, %edx
	movw	%ax, -190(%rbp)
	subw	$10304, %dx
	movl	$2, %eax
	jmp	.L76
.L233:
	testl	$1024, -488(%rbp)
	jne	.L83
	addq	$2, %r12
	jmp	.L82
.L236:
	movq	-464(%rbp), %rsi
	leaq	-192(%rbp), %rbx
	movl	%eax, %edx
	movq	%r11, -512(%rbp)
	movq	%rbx, %rdi
	call	u_memcpy_67@PLT
	movslq	-468(%rbp), %r10
	movq	-512(%rbp), %r11
	addq	%r10, %r10
	jmp	.L74
.L118:
	movl	$-1, %eax
	jmp	.L26
.L130:
	movl	$2, %r13d
	jmp	.L95
.L224:
	andl	$1024, %r9d
	jne	.L94
	addq	$2, %rbx
	jmp	.L93
.L86:
	movl	%eax, %ecx
	andw	$1023, %ax
	movl	$4, %r11d
	orw	$-9216, %ax
	sarl	$10, %ecx
	movw	%ax, -126(%rbp)
	subw	$10304, %cx
	movl	$2, %eax
	jmp	.L87
.L234:
	movq	-464(%rbp), %rsi
	leaq	-128(%rbp), %r15
	movl	%eax, %edx
	movq	%r8, -528(%rbp)
	movq	%r15, %rdi
	movq	%r10, -512(%rbp)
	movl	%r9d, -488(%rbp)
	call	u_memcpy_67@PLT
	movslq	-468(%rbp), %r11
	movl	-488(%rbp), %r9d
	movq	-512(%rbp), %r10
	movq	-528(%rbp), %r8
	addq	%r11, %r11
	jmp	.L85
.L119:
	movl	$1, %eax
	jmp	.L26
.L94:
	movzwl	-4(%r12), %edi
	subq	$2, %r12
	movl	%edi, -488(%rbp)
	jmp	.L93
.L72:
	movzwl	-4(%r12), %edi
	subq	$2, %r12
	movl	%edi, -488(%rbp)
	jmp	.L71
.L83:
	movzwl	-4(%rbx), %r9d
	subq	$2, %rbx
	jmp	.L82
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3125:
	.size	unorm_compare_67, .-unorm_compare_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
