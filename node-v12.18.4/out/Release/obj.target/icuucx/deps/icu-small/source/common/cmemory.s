	.file	"cmemory.cpp"
	.text
	.p2align 4
	.globl	uprv_malloc_67
	.type	uprv_malloc_67, @function
uprv_malloc_67:
.LFB2054:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	testq	%rdi, %rdi
	je	.L2
	movq	_ZL6pAlloc(%rip), %rax
	testq	%rax, %rax
	je	.L3
	movq	_ZL8pContext(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	_ZL7zeroMem(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	jmp	malloc@PLT
	.cfi_endproc
.LFE2054:
	.size	uprv_malloc_67, .-uprv_malloc_67
	.p2align 4
	.globl	uprv_realloc_67
	.type	uprv_realloc_67, @function
uprv_realloc_67:
.LFB2055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZL7zeroMem(%rip), %rbx
	subq	$8, %rsp
	cmpq	%rbx, %rdi
	je	.L14
	testq	%rsi, %rsi
	je	.L15
	movq	_ZL8pRealloc(%rip), %rax
	testq	%rax, %rax
	je	.L11
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	_ZL8pContext(%rip), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	_ZL5pFree(%rip), %rax
	testq	%rax, %rax
	je	.L10
	movq	%rdi, %rsi
	movq	_ZL8pContext(%rip), %rdi
	call	*%rax
	movq	%rbx, %rax
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	realloc@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L12
	movq	_ZL6pAlloc(%rip), %rax
	testq	%rax, %rax
	je	.L8
	movq	_ZL8pContext(%rip), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	%rdi, %rax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L10:
	call	free@PLT
	movq	%rbx, %rax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$8, %rsp
	movq	%rsi, %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	malloc@PLT
	.cfi_endproc
.LFE2055:
	.size	uprv_realloc_67, .-uprv_realloc_67
	.p2align 4
	.globl	uprv_free_67
	.type	uprv_free_67, @function
uprv_free_67:
.LFB2056:
	.cfi_startproc
	endbr64
	leaq	_ZL7zeroMem(%rip), %rax
	movq	%rdi, %rsi
	cmpq	%rax, %rdi
	je	.L16
	movq	_ZL5pFree(%rip), %rax
	testq	%rax, %rax
	je	.L18
	movq	_ZL8pContext(%rip), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L16:
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	jmp	free@PLT
	.cfi_endproc
.LFE2056:
	.size	uprv_free_67, .-uprv_free_67
	.p2align 4
	.globl	uprv_calloc_67
	.type	uprv_calloc_67, @function
uprv_calloc_67:
.LFB2057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	imulq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L24
	movq	_ZL6pAlloc(%rip), %rax
	testq	%rax, %rax
	je	.L21
	movq	_ZL8pContext(%rip), %rdi
	call	*%rax
	movq	%rax, %r8
.L22:
	testq	%r8, %r8
	je	.L19
.L20:
	movq	%r8, %rdi
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %r8
.L19:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	leaq	_ZL7zeroMem(%rip), %r8
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rsi, %rdi
	call	malloc@PLT
	movq	%rax, %r8
	jmp	.L22
	.cfi_endproc
.LFE2057:
	.size	uprv_calloc_67, .-uprv_calloc_67
	.p2align 4
	.globl	u_setMemoryFunctions_67
	.type	u_setMemoryFunctions_67, @function
u_setMemoryFunctions_67:
.LFB2058:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L29
	testq	%rdx, %rdx
	sete	%r9b
	testq	%rcx, %rcx
	sete	%al
	orb	%al, %r9b
	jne	.L33
	testq	%rsi, %rsi
	je	.L33
	movq	%rdi, _ZL8pContext(%rip)
	movq	%rsi, _ZL6pAlloc(%rip)
	movq	%rdx, _ZL8pRealloc(%rip)
	movq	%rcx, _ZL5pFree(%rip)
.L29:
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$1, (%r8)
	ret
	.cfi_endproc
.LFE2058:
	.size	u_setMemoryFunctions_67, .-u_setMemoryFunctions_67
	.p2align 4
	.globl	cmemory_cleanup_67
	.type	cmemory_cleanup_67, @function
cmemory_cleanup_67:
.LFB2059:
	.cfi_startproc
	endbr64
	movq	$0, _ZL8pContext(%rip)
	movl	$1, %eax
	movq	$0, _ZL6pAlloc(%rip)
	movq	$0, _ZL8pRealloc(%rip)
	movq	$0, _ZL5pFree(%rip)
	ret
	.cfi_endproc
.LFE2059:
	.size	cmemory_cleanup_67, .-cmemory_cleanup_67
	.local	_ZL5pFree
	.comm	_ZL5pFree,8,8
	.local	_ZL8pRealloc
	.comm	_ZL8pRealloc,8,8
	.local	_ZL6pAlloc
	.comm	_ZL6pAlloc,8,8
	.local	_ZL8pContext
	.comm	_ZL8pContext,8,8
	.section	.rodata
	.align 16
	.type	_ZL7zeroMem, @object
	.size	_ZL7zeroMem, 24
_ZL7zeroMem:
	.zero	24
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
