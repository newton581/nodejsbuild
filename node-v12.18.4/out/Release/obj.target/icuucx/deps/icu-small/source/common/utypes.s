	.file	"utypes.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"[BOGUS UErrorCode]"
	.text
	.p2align 4
	.globl	u_errorName_67
	.type	u_errorName_67, @function
u_errorName_67:
.LFB2:
	.cfi_startproc
	endbr64
	cmpl	$30, %edi
	jbe	.L12
	leal	128(%rdi), %eax
	cmpl	$8, %eax
	jbe	.L13
	leal	-65536(%rdi), %eax
	cmpl	$34, %eax
	jbe	.L14
	leal	-65792(%rdi), %eax
	cmpl	$19, %eax
	jbe	.L15
	leal	-66048(%rdi), %eax
	cmpl	$13, %eax
	jbe	.L16
	leal	-66304(%rdi), %eax
	cmpl	$21, %eax
	jbe	.L17
	leal	-66560(%rdi), %eax
	cmpl	$8, %eax
	jbe	.L18
	subl	$66816, %edi
	leaq	.LC0(%rip), %rax
	cmpl	$1, %edi
	jbe	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	_ZL15_uErrorInfoName(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movslq	%edi, %rdi
	leaq	_ZL11_uErrorName(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	_ZL14_uFmtErrorName(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	_ZL16_uTransErrorName(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	_ZL14_uBrkErrorName(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movslq	%edi, %rdi
	leaq	_ZL17_uPluginErrorName(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	_ZL16_uRegexErrorName(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	_ZL15_uIDNAErrorName(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.cfi_endproc
.LFE2:
	.size	u_errorName_67, .-u_errorName_67
	.section	.rodata.str1.1
.LC1:
	.string	"U_PLUGIN_TOO_HIGH"
.LC2:
	.string	"U_PLUGIN_DIDNT_SET_LEVEL"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZL17_uPluginErrorName, @object
	.size	_ZL17_uPluginErrorName, 16
_ZL17_uPluginErrorName:
	.quad	.LC1
	.quad	.LC2
	.section	.rodata.str1.1
.LC3:
	.string	"U_STRINGPREP_PROHIBITED_ERROR"
.LC4:
	.string	"U_STRINGPREP_UNASSIGNED_ERROR"
.LC5:
	.string	"U_STRINGPREP_CHECK_BIDI_ERROR"
.LC6:
	.string	"U_IDNA_STD3_ASCII_RULES_ERROR"
.LC7:
	.string	"U_IDNA_ACE_PREFIX_ERROR"
.LC8:
	.string	"U_IDNA_VERIFICATION_ERROR"
.LC9:
	.string	"U_IDNA_LABEL_TOO_LONG_ERROR"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"U_IDNA_ZERO_LENGTH_LABEL_ERROR"
	.align 8
.LC11:
	.string	"U_IDNA_DOMAIN_NAME_TOO_LONG_ERROR"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL15_uIDNAErrorName, @object
	.size	_ZL15_uIDNAErrorName, 72
_ZL15_uIDNAErrorName:
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.section	.rodata.str1.1
.LC12:
	.string	"U_REGEX_INTERNAL_ERROR"
.LC13:
	.string	"U_REGEX_RULE_SYNTAX"
.LC14:
	.string	"U_REGEX_INVALID_STATE"
.LC15:
	.string	"U_REGEX_BAD_ESCAPE_SEQUENCE"
.LC16:
	.string	"U_REGEX_PROPERTY_SYNTAX"
.LC17:
	.string	"U_REGEX_UNIMPLEMENTED"
.LC18:
	.string	"U_REGEX_MISMATCHED_PAREN"
.LC19:
	.string	"U_REGEX_NUMBER_TOO_BIG"
.LC20:
	.string	"U_REGEX_BAD_INTERVAL"
.LC21:
	.string	"U_REGEX_MAX_LT_MIN"
.LC22:
	.string	"U_REGEX_INVALID_BACK_REF"
.LC23:
	.string	"U_REGEX_INVALID_FLAG"
.LC24:
	.string	"U_REGEX_LOOK_BEHIND_LIMIT"
.LC25:
	.string	"U_REGEX_SET_CONTAINS_STRING"
.LC26:
	.string	"U_REGEX_OCTAL_TOO_BIG"
.LC27:
	.string	"U_REGEX_MISSING_CLOSE_BRACKET"
.LC28:
	.string	"U_REGEX_INVALID_RANGE"
.LC29:
	.string	"U_REGEX_STACK_OVERFLOW"
.LC30:
	.string	"U_REGEX_TIME_OUT"
.LC31:
	.string	"U_REGEX_STOPPED_BY_CALLER"
.LC32:
	.string	"U_REGEX_PATTERN_TOO_BIG"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"U_REGEX_INVALID_CAPTURE_GROUP_NAME"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL16_uRegexErrorName, @object
	.size	_ZL16_uRegexErrorName, 176
_ZL16_uRegexErrorName:
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.section	.rodata.str1.1
.LC34:
	.string	"U_BRK_INTERNAL_ERROR"
.LC35:
	.string	"U_BRK_HEX_DIGITS_EXPECTED"
.LC36:
	.string	"U_BRK_SEMICOLON_EXPECTED"
.LC37:
	.string	"U_BRK_RULE_SYNTAX"
.LC38:
	.string	"U_BRK_UNCLOSED_SET"
.LC39:
	.string	"U_BRK_ASSIGN_ERROR"
.LC40:
	.string	"U_BRK_VARIABLE_REDFINITION"
.LC41:
	.string	"U_BRK_MISMATCHED_PAREN"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"U_BRK_NEW_LINE_IN_QUOTED_STRING"
	.section	.rodata.str1.1
.LC43:
	.string	"U_BRK_UNDEFINED_VARIABLE"
.LC44:
	.string	"U_BRK_INIT_ERROR"
.LC45:
	.string	"U_BRK_RULE_EMPTY_SET"
.LC46:
	.string	"U_BRK_UNRECOGNIZED_OPTION"
.LC47:
	.string	"U_BRK_MALFORMED_RULE_TAG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL14_uBrkErrorName, @object
	.size	_ZL14_uBrkErrorName, 112
_ZL14_uBrkErrorName:
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.section	.rodata.str1.1
.LC48:
	.string	"U_UNEXPECTED_TOKEN"
.LC49:
	.string	"U_MULTIPLE_DECIMAL_SEPARATORS"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"U_MULTIPLE_EXPONENTIAL_SYMBOLS"
	.align 8
.LC51:
	.string	"U_MALFORMED_EXPONENTIAL_PATTERN"
	.section	.rodata.str1.1
.LC52:
	.string	"U_MULTIPLE_PERCENT_SYMBOLS"
.LC53:
	.string	"U_MULTIPLE_PERMILL_SYMBOLS"
.LC54:
	.string	"U_MULTIPLE_PAD_SPECIFIERS"
.LC55:
	.string	"U_PATTERN_SYNTAX_ERROR"
.LC56:
	.string	"U_ILLEGAL_PAD_POSITION"
.LC57:
	.string	"U_UNMATCHED_BRACES"
.LC58:
	.string	"U_UNSUPPORTED_PROPERTY"
.LC59:
	.string	"U_UNSUPPORTED_ATTRIBUTE"
.LC60:
	.string	"U_ARGUMENT_TYPE_MISMATCH"
.LC61:
	.string	"U_DUPLICATE_KEYWORD"
.LC62:
	.string	"U_UNDEFINED_KEYWORD"
.LC63:
	.string	"U_DEFAULT_KEYWORD_MISSING"
.LC64:
	.string	"U_DECIMAL_NUMBER_SYNTAX_ERROR"
.LC65:
	.string	"U_FORMAT_INEXACT_ERROR"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"U_NUMBER_ARG_OUTOFBOUNDS_ERROR"
	.align 8
.LC67:
	.string	"U_NUMBER_SKELETON_SYNTAX_ERROR"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL14_uFmtErrorName, @object
	.size	_ZL14_uFmtErrorName, 160
_ZL14_uFmtErrorName:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.section	.rodata.str1.1
.LC68:
	.string	"U_ZERO_ERROR"
.LC69:
	.string	"U_ILLEGAL_ARGUMENT_ERROR"
.LC70:
	.string	"U_MISSING_RESOURCE_ERROR"
.LC71:
	.string	"U_INVALID_FORMAT_ERROR"
.LC72:
	.string	"U_FILE_ACCESS_ERROR"
.LC73:
	.string	"U_INTERNAL_PROGRAM_ERROR"
.LC74:
	.string	"U_MESSAGE_PARSE_ERROR"
.LC75:
	.string	"U_MEMORY_ALLOCATION_ERROR"
.LC76:
	.string	"U_INDEX_OUTOFBOUNDS_ERROR"
.LC77:
	.string	"U_PARSE_ERROR"
.LC78:
	.string	"U_INVALID_CHAR_FOUND"
.LC79:
	.string	"U_TRUNCATED_CHAR_FOUND"
.LC80:
	.string	"U_ILLEGAL_CHAR_FOUND"
.LC81:
	.string	"U_INVALID_TABLE_FORMAT"
.LC82:
	.string	"U_INVALID_TABLE_FILE"
.LC83:
	.string	"U_BUFFER_OVERFLOW_ERROR"
.LC84:
	.string	"U_UNSUPPORTED_ERROR"
.LC85:
	.string	"U_RESOURCE_TYPE_MISMATCH"
.LC86:
	.string	"U_ILLEGAL_ESCAPE_SEQUENCE"
.LC87:
	.string	"U_UNSUPPORTED_ESCAPE_SEQUENCE"
.LC88:
	.string	"U_NO_SPACE_AVAILABLE"
.LC89:
	.string	"U_CE_NOT_FOUND_ERROR"
.LC90:
	.string	"U_PRIMARY_TOO_LONG_ERROR"
.LC91:
	.string	"U_STATE_TOO_OLD_ERROR"
.LC92:
	.string	"U_TOO_MANY_ALIASES_ERROR"
.LC93:
	.string	"U_ENUM_OUT_OF_SYNC_ERROR"
.LC94:
	.string	"U_INVARIANT_CONVERSION_ERROR"
.LC95:
	.string	"U_INVALID_STATE_ERROR"
.LC96:
	.string	"U_COLLATOR_VERSION_MISMATCH"
.LC97:
	.string	"U_USELESS_COLLATOR_ERROR"
.LC98:
	.string	"U_NO_WRITE_PERMISSION"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL11_uErrorName, @object
	.size	_ZL11_uErrorName, 248
_ZL11_uErrorName:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.section	.rodata.str1.1
.LC99:
	.string	"U_BAD_VARIABLE_DEFINITION"
.LC100:
	.string	"U_MALFORMED_RULE"
.LC101:
	.string	"U_MALFORMED_SET"
.LC102:
	.string	"U_MALFORMED_SYMBOL_REFERENCE"
.LC103:
	.string	"U_MALFORMED_UNICODE_ESCAPE"
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"U_MALFORMED_VARIABLE_DEFINITION"
	.align 8
.LC105:
	.string	"U_MALFORMED_VARIABLE_REFERENCE"
	.align 8
.LC106:
	.string	"U_MISMATCHED_SEGMENT_DELIMITERS"
	.section	.rodata.str1.1
.LC107:
	.string	"U_MISPLACED_ANCHOR_START"
.LC108:
	.string	"U_MISPLACED_CURSOR_OFFSET"
.LC109:
	.string	"U_MISPLACED_QUANTIFIER"
.LC110:
	.string	"U_MISSING_OPERATOR"
.LC111:
	.string	"U_MISSING_SEGMENT_CLOSE"
.LC112:
	.string	"U_MULTIPLE_ANTE_CONTEXTS"
.LC113:
	.string	"U_MULTIPLE_CURSORS"
.LC114:
	.string	"U_MULTIPLE_POST_CONTEXTS"
.LC115:
	.string	"U_TRAILING_BACKSLASH"
.LC116:
	.string	"U_UNDEFINED_SEGMENT_REFERENCE"
.LC117:
	.string	"U_UNDEFINED_VARIABLE"
.LC118:
	.string	"U_UNQUOTED_SPECIAL"
.LC119:
	.string	"U_UNTERMINATED_QUOTE"
.LC120:
	.string	"U_RULE_MASK_ERROR"
.LC121:
	.string	"U_MISPLACED_COMPOUND_FILTER"
.LC122:
	.string	"U_MULTIPLE_COMPOUND_FILTERS"
.LC123:
	.string	"U_INVALID_RBT_SYNTAX"
.LC124:
	.string	"U_INVALID_PROPERTY_PATTERN"
.LC125:
	.string	"U_MALFORMED_PRAGMA"
.LC126:
	.string	"U_UNCLOSED_SEGMENT"
.LC127:
	.string	"U_ILLEGAL_CHAR_IN_SEGMENT"
.LC128:
	.string	"U_VARIABLE_RANGE_EXHAUSTED"
.LC129:
	.string	"U_VARIABLE_RANGE_OVERLAP"
.LC130:
	.string	"U_ILLEGAL_CHARACTER"
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"U_INTERNAL_TRANSLITERATOR_ERROR"
	.section	.rodata.str1.1
.LC132:
	.string	"U_INVALID_ID"
.LC133:
	.string	"U_INVALID_FUNCTION"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL16_uTransErrorName, @object
	.size	_ZL16_uTransErrorName, 280
_ZL16_uTransErrorName:
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.quad	.LC133
	.section	.rodata.str1.1
.LC134:
	.string	"U_USING_FALLBACK_WARNING"
.LC135:
	.string	"U_USING_DEFAULT_WARNING"
.LC136:
	.string	"U_SAFECLONE_ALLOCATED_WARNING"
.LC137:
	.string	"U_STATE_OLD_WARNING"
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"U_STRING_NOT_TERMINATED_WARNING"
	.section	.rodata.str1.1
.LC139:
	.string	"U_SORT_KEY_TOO_SHORT_WARNING"
.LC140:
	.string	"U_AMBIGUOUS_ALIAS_WARNING"
.LC141:
	.string	"U_DIFFERENT_UCA_VERSION"
	.section	.rodata.str1.8
	.align 8
.LC142:
	.string	"U_PLUGIN_CHANGED_LEVEL_WARNING"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL15_uErrorInfoName, @object
	.size	_ZL15_uErrorInfoName, 72
_ZL15_uErrorInfoName:
	.quad	.LC134
	.quad	.LC135
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.quad	.LC142
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
