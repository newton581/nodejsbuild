	.file	"putil.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PST8PDT"
.LC1:
	.string	"MST7MDT"
.LC2:
	.string	"CST6CDT"
.LC3:
	.string	"EST5EDT"
	.text
	.p2align 4
	.type	_ZL14isValidOlsonIDPKc, @function
_ZL14isValidOlsonIDPKc:
.LFB3247:
	.cfi_startproc
	movzbl	(%rdi), %eax
	movq	%rdi, %r8
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L2
	testb	%al, %al
	je	.L2
	leaq	1(%rdi), %rdx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L22:
	movzbl	(%rdx), %eax
	addq	$1, %rdx
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L2
	testb	%al, %al
	je	.L2
.L4:
	cmpb	$44, %al
	jne	.L22
.L3:
	movl	$8, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%r8, %rsi
	movl	$1, %r9d
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L23
.L1:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$1, %r9d
	testb	%al, %al
	jne	.L3
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$8, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1
	movl	$8, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1
	movl	$8, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	%r9b
	jmp	.L1
	.cfi_endproc
.LFE3247:
	.size	_ZL14isValidOlsonIDPKc, .-_ZL14isValidOlsonIDPKc
	.p2align 4
	.type	_ZL13putil_cleanupv, @function
_ZL13putil_cleanupv:
.LFB3254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZL14gDataDirectory(%rip), %rdi
	testq	%rdi, %rdi
	je	.L25
	cmpb	$0, (%rdi)
	jne	.L44
.L25:
	movq	$0, _ZL14gDataDirectory(%rip)
	movl	$0, _ZL16gDataDirInitOnce(%rip)
	mfence
	movq	_ZL23gTimeZoneFilesDirectory(%rip), %r12
	testq	%r12, %r12
	je	.L26
	cmpb	$0, 12(%r12)
	jne	.L45
.L27:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L26:
	movq	$0, _ZL23gTimeZoneFilesDirectory(%rip)
	movl	$0, gTimeZoneFilesInitOnce_67(%rip)
	mfence
	movq	_ZL19gSearchTZFileResult(%rip), %r12
	testq	%r12, %r12
	je	.L28
	cmpb	$0, 12(%r12)
	jne	.L46
.L29:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L28:
	movq	_ZL21gCorrectedPOSIXLocale(%rip), %rdi
	movq	$0, _ZL19gSearchTZFileResult(%rip)
	testq	%rdi, %rdi
	je	.L30
	cmpb	$0, _ZL34gCorrectedPOSIXLocaleHeapAllocated(%rip)
	jne	.L47
.L30:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	call	uprv_free_67@PLT
	movb	$0, _ZL34gCorrectedPOSIXLocaleHeapAllocated(%rip)
	movl	$1, %eax
	movq	$0, _ZL21gCorrectedPOSIXLocale(%rip)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L45:
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L44:
	call	uprv_free_67@PLT
	jmp	.L25
	.cfi_endproc
.LFE3254:
	.size	_ZL13putil_cleanupv, .-_ZL13putil_cleanupv
	.section	.rodata.str1.1
.LC4:
	.string	"."
.LC5:
	.string	".."
.LC6:
	.string	"posixrules"
.LC7:
	.string	"localtime"
.LC8:
	.string	"r"
.LC9:
	.string	"/etc/localtime"
.LC10:
	.string	"posix/"
.LC11:
	.string	"right/"
	.text
	.p2align 4
	.type	_ZL15searchForTZFilePKcP13DefaultTZInfo, @function
_ZL15searchForTZFilePKcP13DefaultTZInfo:
.LFB3251:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$-1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-708(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rcx
	leaq	-704(%rbp), %rdi
	pushq	%rbx
	subq	$760, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -736(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-691(%rbp), %rax
	movl	$0, -708(%rbp)
	movq	%rax, -704(%rbp)
	movl	$0, -648(%rbp)
	movl	$40, -696(%rbp)
	movw	%r8w, -692(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-708(%rbp), %r9d
	testl	%r9d, %r9d
	jg	.L49
	movq	%r12, %rdi
	call	opendir@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L49
	cmpq	$0, _ZL19gSearchTZFileResult(%rip)
	je	.L108
.L50:
	leaq	-627(%rbp), %rax
	leaq	-640(%rbp), %r14
	movabsq	$7815278589862178672, %rbx
	movabsq	$7883960631527960428, %r15
	movq	%rax, -744(%rbp)
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r12, %rdi
	call	readdir@PLT
	testq	%rax, %rax
	je	.L102
.L113:
	cmpw	$46, 19(%rax)
	leaq	19(%rax), %r8
	je	.L52
	cmpw	$11822, 19(%rax)
	je	.L109
	cmpq	%rbx, (%r8)
	je	.L110
.L59:
	cmpq	%r15, (%r8)
	je	.L111
.L61:
	movq	-744(%rbp), %rax
	movl	-648(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	-704(%rbp), %rsi
	movw	%cx, -628(%rbp)
	movq	%r13, %rcx
	movq	%r8, -728(%rbp)
	movq	%rax, -640(%rbp)
	movl	$0, -584(%rbp)
	movl	$40, -632(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r13, %rcx
	movl	$-1, %edx
	movq	%r14, %rdi
	movq	-728(%rbp), %r8
	movq	%r8, %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-708(%rbp), %esi
	testl	%esi, %esi
	jg	.L63
	movq	-640(%rbp), %rdi
	call	opendir@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L64
	call	closedir@PLT
	movq	%r13, %rdx
	movl	$47, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-708(%rbp), %edx
	testl	%edx, %edx
	jg	.L63
	movq	-736(%rbp), %rsi
	movq	-640(%rbp), %rdi
	call	_ZL15searchForTZFilePKcP13DefaultTZInfo
	testq	%rax, %rax
	jne	.L112
.L66:
	cmpb	$0, -628(%rbp)
	je	.L52
	movq	-640(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	readdir@PLT
	testq	%rax, %rax
	jne	.L113
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rax, %r8
.L53:
	movq	%r12, %rdi
	movq	%r8, -728(%rbp)
	call	closedir@PLT
	cmpb	$0, -692(%rbp)
	movq	-728(%rbp), %r8
	je	.L48
.L114:
	movq	-704(%rbp), %rdi
	movq	%r8, -728(%rbp)
	call	uprv_free_67@PLT
	movq	-728(%rbp), %r8
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L109:
	cmpb	$0, 2(%r8)
	je	.L52
	cmpq	%rbx, (%r8)
	jne	.L59
.L110:
	cmpw	$29541, 8(%r8)
	jne	.L59
	cmpb	$0, 10(%r8)
	je	.L52
	cmpq	%r15, (%r8)
	jne	.L61
	.p2align 4,,10
	.p2align 3
.L111:
	cmpw	$101, 8(%r8)
	jne	.L61
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%r8d, %r8d
	cmpb	$0, -692(%rbp)
	jne	.L114
.L48:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$760, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	-736(%rbp), %rax
	movq	-640(%rbp), %r8
	cmpq	$0, 16(%rax)
	je	.L116
.L67:
	movq	%r8, %rdi
	leaq	.LC8(%rip), %rsi
	call	fopen@PLT
	movq	%rax, %r8
	movq	-736(%rbp), %rax
	movl	$0, 28(%rax)
	testq	%r8, %r8
	je	.L66
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L107
	cmpq	$0, 8(%rax)
	je	.L117
.L70:
	xorl	%esi, %esi
	movq	%r8, %rdi
	movl	$2, %edx
	movq	%r8, -728(%rbp)
	call	fseek@PLT
	movq	-728(%rbp), %r8
	movq	%r8, %rdi
	call	ftell@PLT
	movq	-728(%rbp), %r8
	movq	%rax, %r9
	movq	-736(%rbp), %rax
	cmpq	8(%rax), %r9
	je	.L118
.L107:
	movq	%r8, %rdi
	call	fclose@PLT
	jmp	.L66
.L108:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L51
	xorl	%edi, %edi
	leaq	13(%rax), %rax
	movl	$0, 56(%r8)
	leaq	_ZL13putil_cleanupv(%rip), %rsi
	movw	%di, 12(%r8)
	movl	$21, %edi
	movq	%rax, (%r8)
	movl	$40, 8(%r8)
	movq	%r8, _ZL19gSearchTZFileResult(%rip)
	call	ucln_common_registerCleanup_67@PLT
	jmp	.L50
.L118:
	cmpq	$0, (%rax)
	je	.L119
.L72:
	movq	%r8, %rdi
	movq	%r9, -752(%rbp)
	movq	%r8, -728(%rbp)
	call	rewind@PLT
	movq	-752(%rbp), %r9
	movq	-728(%rbp), %r8
	testq	%r9, %r9
	jle	.L106
	leaq	-576(%rbp), %r10
	movq	%r12, -776(%rbp)
	movq	-736(%rbp), %r12
	movq	%r13, -784(%rbp)
	movq	%r10, %r13
	movq	%r14, -792(%rbp)
	movq	%r9, %r14
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-752(%rbp), %rdx
	movl	-756(%rbp), %r11d
	movq	-768(%rbp), %rcx
	subq	%rdx, %r14
	addl	%r11d, %ecx
	movl	%ecx, 28(%r12)
	testq	%r14, %r14
	jle	.L120
.L76:
	xorl	%eax, %eax
	movl	$64, %ecx
	movq	%r13, %rdi
	movl	$1, %edx
	rep stosq
	movl	$512, %ecx
	movl	$512, %esi
	movq	%r13, %rdi
	cmpq	$512, %r14
	movq	%r8, -728(%rbp)
	cmovle	%r14, %rcx
	call	__fread_chk@PLT
	movslq	28(%r12), %rdi
	movq	%r13, %rsi
	movslq	%eax, %rdx
	movq	%rax, -768(%rbp)
	movl	%edi, -756(%rbp)
	addq	(%r12), %rdi
	movq	%rdx, -752(%rbp)
	call	memcmp@PLT
	movq	-728(%rbp), %r8
	testl	%eax, %eax
	je	.L121
	movq	-776(%rbp), %r12
	movq	-784(%rbp), %r13
	movq	-792(%rbp), %r14
	jmp	.L107
.L117:
	movl	$2, %edx
	xorl	%esi, %esi
	movq	%r8, -728(%rbp)
	call	fseek@PLT
	movq	-736(%rbp), %rax
	movq	16(%rax), %rdi
	call	ftell@PLT
	movq	-736(%rbp), %rdx
	movq	-728(%rbp), %r8
	movq	%rax, 8(%rdx)
	jmp	.L70
.L63:
	xorl	%r8d, %r8d
.L65:
	cmpb	$0, -628(%rbp)
	je	.L53
	movq	-640(%rbp), %rdi
	movq	%r8, -728(%rbp)
	call	uprv_free_67@PLT
	movq	-728(%rbp), %r8
	jmp	.L53
.L116:
	leaq	.LC8(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movq	%r8, -728(%rbp)
	call	fopen@PLT
	movq	-736(%rbp), %rdx
	movq	-728(%rbp), %r8
	movq	%rax, 16(%rdx)
	jmp	.L67
.L112:
	movq	%rax, %r8
	jmp	.L65
.L120:
	movq	-776(%rbp), %r12
	movq	-784(%rbp), %r13
.L106:
	movq	%r8, %rdi
	call	fclose@PLT
	movl	$20, %edx
	movslq	-584(%rbp), %rax
	movl	$6, %ecx
	leaq	.LC10(%rip), %rdi
	cmpl	$20, %eax
	cmovge	%rdx, %rax
	addq	-640(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, %r8
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L78
	movl	$6, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L79
.L78:
	addq	$6, %r8
.L79:
	movq	_ZL19gSearchTZFileResult(%rip), %rdi
	movq	%r13, %rcx
	movl	$-1, %edx
	movq	%r8, %rsi
	movq	(%rdi), %rax
	movl	$0, 56(%rdi)
	movb	$0, (%rax)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-708(%rbp), %eax
	testl	%eax, %eax
	jg	.L63
	movq	_ZL19gSearchTZFileResult(%rip), %rax
	movq	(%rax), %r8
	jmp	.L65
.L119:
	movq	16(%rax), %rdi
	movq	%r8, -752(%rbp)
	movq	%r9, -728(%rbp)
	call	rewind@PLT
	movq	-736(%rbp), %rax
	movq	8(%rax), %rdi
	call	uprv_malloc_67@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	movq	-736(%rbp), %rax
	movq	16(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rdi, (%rax)
	call	fread@PLT
	movq	-752(%rbp), %r8
	movq	-728(%rbp), %r9
	jmp	.L72
.L51:
	movq	$0, _ZL19gSearchTZFileResult(%rip)
	jmp	.L53
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3251:
	.size	_ZL15searchForTZFilePKcP13DefaultTZInfo, .-_ZL15searchForTZFilePKcP13DefaultTZInfo
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3580:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3580:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3583:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L135
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L123
	cmpb	$0, 12(%rbx)
	jne	.L136
.L127:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L123:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L127
	.cfi_endproc
.LFE3583:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3586:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L139
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3586:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3589:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L142
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3589:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L148
.L144:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L149
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3591:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3592:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3592:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3593:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3593:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3594:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3594:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3595:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3595:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3596:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3596:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3597:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L165
	testl	%edx, %edx
	jle	.L165
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L168
.L157:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L157
	.cfi_endproc
.LFE3597:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L172
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L172
	testl	%r12d, %r12d
	jg	.L179
	cmpb	$0, 12(%rbx)
	jne	.L180
.L174:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L174
.L180:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L172:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3598:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L182
	movq	(%rdi), %r8
.L183:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L186
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L186
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L186:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3599:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3600:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L193
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3600:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3601:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3601:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3602:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3602:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3603:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3603:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3605:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3605:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3607:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3607:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.p2align 4
	.globl	uprv_getUTCtime_67
	.type	uprv_getUTCtime_67, @function
uprv_getUTCtime_67:
.LFB3221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdi
	call	gettimeofday@PLT
	movq	-24(%rbp), %rsi
	movabsq	$2361183241434822607, %rdx
	imulq	$1000, -32(%rbp), %rcx
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rsi, %rdx
	addq	%rcx, %rdx
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L202
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	ret
.L202:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3221:
	.size	uprv_getUTCtime_67, .-uprv_getUTCtime_67
	.p2align 4
	.globl	uprv_getRawUTCtime_67
	.type	uprv_getRawUTCtime_67, @function
uprv_getRawUTCtime_67:
.LFB3981:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rdi
	call	gettimeofday@PLT
	movq	-24(%rbp), %rsi
	movabsq	$2361183241434822607, %rdx
	imulq	$1000, -32(%rbp), %rcx
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rsi, %rdx
	addq	%rcx, %rdx
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	ret
.L206:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3981:
	.size	uprv_getRawUTCtime_67, .-uprv_getRawUTCtime_67
	.p2align 4
	.globl	uprv_isNaN_67
	.type	uprv_isNaN_67, @function
uprv_isNaN_67:
.LFB3223:
	.cfi_startproc
	endbr64
	movabsq	$9218868437227405312, %rdx
	movq	%xmm0, %rax
	btrq	$63, %rax
	cmpq	%rdx, %rax
	setg	%al
	ret
	.cfi_endproc
.LFE3223:
	.size	uprv_isNaN_67, .-uprv_isNaN_67
	.p2align 4
	.globl	uprv_isInfinite_67
	.type	uprv_isInfinite_67, @function
uprv_isInfinite_67:
.LFB3224:
	.cfi_startproc
	endbr64
	movabsq	$9218868437227405312, %rdx
	movq	%xmm0, %rax
	btrq	$63, %rax
	cmpq	%rdx, %rax
	sete	%al
	ret
	.cfi_endproc
.LFE3224:
	.size	uprv_isInfinite_67, .-uprv_isInfinite_67
	.p2align 4
	.globl	uprv_isPositiveInfinity_67
	.type	uprv_isPositiveInfinity_67, @function
uprv_isPositiveInfinity_67:
.LFB3225:
	.cfi_startproc
	endbr64
	comisd	.LC12(%rip), %xmm0
	jbe	.L214
	movabsq	$9218868437227405312, %rdx
	movq	%xmm0, %rax
	btrq	$63, %rax
	cmpq	%rdx, %rax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3225:
	.size	uprv_isPositiveInfinity_67, .-uprv_isPositiveInfinity_67
	.p2align 4
	.globl	uprv_isNegativeInfinity_67
	.type	uprv_isNegativeInfinity_67, @function
uprv_isNegativeInfinity_67:
.LFB3226:
	.cfi_startproc
	endbr64
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	ja	.L221
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	movabsq	$9218868437227405312, %rdx
	movq	%xmm0, %rax
	btrq	$63, %rax
	cmpq	%rdx, %rax
	sete	%al
	ret
	.cfi_endproc
.LFE3226:
	.size	uprv_isNegativeInfinity_67, .-uprv_isNegativeInfinity_67
	.p2align 4
	.globl	uprv_getNaN_67
	.type	uprv_getNaN_67, @function
uprv_getNaN_67:
.LFB3227:
	.cfi_startproc
	endbr64
	movsd	.LC13(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3227:
	.size	uprv_getNaN_67, .-uprv_getNaN_67
	.p2align 4
	.globl	uprv_getInfinity_67
	.type	uprv_getInfinity_67, @function
uprv_getInfinity_67:
.LFB3228:
	.cfi_startproc
	endbr64
	movsd	.LC14(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3228:
	.size	uprv_getInfinity_67, .-uprv_getInfinity_67
	.p2align 4
	.globl	uprv_floor_67
	.type	uprv_floor_67, @function
uprv_floor_67:
.LFB3229:
	.cfi_startproc
	endbr64
	movsd	.LC16(%rip), %xmm3
	movsd	.LC15(%rip), %xmm4
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm1
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L225
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC17(%rip), %xmm4
	andnpd	%xmm1, %xmm3
	cvtsi2sdq	%rax, %xmm2
	movapd	%xmm2, %xmm5
	cmpnlesd	%xmm0, %xmm5
	movapd	%xmm5, %xmm0
	andpd	%xmm4, %xmm0
	subsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	orpd	%xmm3, %xmm0
.L225:
	ret
	.cfi_endproc
.LFE3229:
	.size	uprv_floor_67, .-uprv_floor_67
	.p2align 4
	.globl	uprv_ceil_67
	.type	uprv_ceil_67, @function
uprv_ceil_67:
.LFB3230:
	.cfi_startproc
	endbr64
	movsd	.LC16(%rip), %xmm3
	movsd	.LC15(%rip), %xmm4
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm1
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L227
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC17(%rip), %xmm4
	andnpd	%xmm1, %xmm3
	cvtsi2sdq	%rax, %xmm2
	cmpnlesd	%xmm2, %xmm0
	andpd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	orpd	%xmm3, %xmm0
.L227:
	ret
	.cfi_endproc
.LFE3230:
	.size	uprv_ceil_67, .-uprv_ceil_67
	.p2align 4
	.globl	uprv_round_67
	.type	uprv_round_67, @function
uprv_round_67:
.LFB3231:
	.cfi_startproc
	endbr64
	movsd	.LC18(%rip), %xmm1
	movsd	.LC16(%rip), %xmm3
	movsd	.LC15(%rip), %xmm4
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm0
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L229
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC17(%rip), %xmm4
	andnpd	%xmm1, %xmm3
	cvtsi2sdq	%rax, %xmm2
	movapd	%xmm2, %xmm5
	cmpnlesd	%xmm1, %xmm5
	movapd	%xmm5, %xmm0
	andpd	%xmm4, %xmm0
	subsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	orpd	%xmm3, %xmm0
.L229:
	ret
	.cfi_endproc
.LFE3231:
	.size	uprv_round_67, .-uprv_round_67
	.p2align 4
	.globl	uprv_fabs_67
	.type	uprv_fabs_67, @function
uprv_fabs_67:
.LFB3232:
	.cfi_startproc
	endbr64
	andpd	.LC16(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3232:
	.size	uprv_fabs_67, .-uprv_fabs_67
	.p2align 4
	.globl	uprv_modf_67
	.type	uprv_modf_67, @function
uprv_modf_67:
.LFB3233:
	.cfi_startproc
	endbr64
	jmp	modf@PLT
	.cfi_endproc
.LFE3233:
	.size	uprv_modf_67, .-uprv_modf_67
	.p2align 4
	.globl	uprv_fmod_67
	.type	uprv_fmod_67, @function
uprv_fmod_67:
.LFB3234:
	.cfi_startproc
	endbr64
	jmp	fmod@PLT
	.cfi_endproc
.LFE3234:
	.size	uprv_fmod_67, .-uprv_fmod_67
	.p2align 4
	.globl	uprv_pow_67
	.type	uprv_pow_67, @function
uprv_pow_67:
.LFB3235:
	.cfi_startproc
	endbr64
	jmp	pow@PLT
	.cfi_endproc
.LFE3235:
	.size	uprv_pow_67, .-uprv_pow_67
	.p2align 4
	.globl	uprv_pow10_67
	.type	uprv_pow10_67, @function
uprv_pow10_67:
.LFB3236:
	.cfi_startproc
	endbr64
	pxor	%xmm1, %xmm1
	movsd	.LC19(%rip), %xmm0
	cvtsi2sdl	%edi, %xmm1
	jmp	pow@PLT
	.cfi_endproc
.LFE3236:
	.size	uprv_pow10_67, .-uprv_pow10_67
	.p2align 4
	.globl	uprv_fmax_67
	.type	uprv_fmax_67, @function
uprv_fmax_67:
.LFB3237:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rdx
	movq	%xmm0, %rax
	movabsq	$9218868437227405312, %rcx
	movq	%rdx, %rsi
	andq	%rax, %rsi
	cmpq	%rcx, %rsi
	jg	.L239
	movq	%xmm1, %rdi
	movsd	.LC13(%rip), %xmm0
	andq	%rdi, %rdx
	cmpq	%rcx, %rdx
	jle	.L250
.L235:
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	movsd	.LC13(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	pxor	%xmm0, %xmm0
	movq	%rax, %xmm2
	movl	$0, %ecx
	ucomisd	%xmm0, %xmm2
	setnp	%dl
	cmovne	%ecx, %edx
	testb	%dl, %dl
	je	.L237
	ucomisd	%xmm0, %xmm1
	setnp	%dl
	cmovne	%ecx, %edx
	testb	%dl, %dl
	je	.L237
	movq	%rax, %rdx
	movapd	%xmm1, %xmm0
	shrq	$56, %rdx
	testb	%dl, %dl
	js	.L235
.L237:
	movq	%rax, %xmm0
	maxsd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE3237:
	.size	uprv_fmax_67, .-uprv_fmax_67
	.p2align 4
	.globl	uprv_fmin_67
	.type	uprv_fmin_67, @function
uprv_fmin_67:
.LFB3238:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rdx
	movq	%xmm0, %rax
	movabsq	$9218868437227405312, %rcx
	movq	%rdx, %rsi
	andq	%rax, %rsi
	cmpq	%rcx, %rsi
	jg	.L254
	movq	%xmm1, %rdi
	movsd	.LC13(%rip), %xmm0
	andq	%rdi, %rdx
	cmpq	%rcx, %rdx
	jle	.L264
.L251:
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	movsd	.LC13(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	pxor	%xmm0, %xmm0
	movq	%rax, %xmm2
	movl	$0, %ecx
	ucomisd	%xmm0, %xmm2
	setnp	%dl
	cmovne	%ecx, %edx
	testb	%dl, %dl
	je	.L253
	ucomisd	%xmm0, %xmm1
	setnp	%dl
	cmovne	%ecx, %edx
	testb	%dl, %dl
	je	.L253
	movq	%xmm1, %rdx
	movapd	%xmm1, %xmm0
	shrq	$56, %rdx
	testb	%dl, %dl
	js	.L251
.L253:
	movq	%rax, %xmm4
	minsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE3238:
	.size	uprv_fmin_67, .-uprv_fmin_67
	.p2align 4
	.globl	uprv_add32_overflow_67
	.type	uprv_add32_overflow_67, @function
uprv_add32_overflow_67:
.LFB3239:
	.cfi_startproc
	endbr64
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	addq	%rdi, %rsi
	movslq	%esi, %rax
	movl	%esi, (%rdx)
	cmpq	%rsi, %rax
	setne	%al
	ret
	.cfi_endproc
.LFE3239:
	.size	uprv_add32_overflow_67, .-uprv_add32_overflow_67
	.p2align 4
	.globl	uprv_mul32_overflow_67
	.type	uprv_mul32_overflow_67, @function
uprv_mul32_overflow_67:
.LFB3240:
	.cfi_startproc
	endbr64
	movslq	%esi, %r8
	movslq	%edi, %rsi
	imulq	%r8, %rsi
	movslq	%esi, %rax
	movl	%esi, (%rdx)
	cmpq	%rsi, %rax
	setne	%al
	ret
	.cfi_endproc
.LFE3240:
	.size	uprv_mul32_overflow_67, .-uprv_mul32_overflow_67
	.p2align 4
	.globl	uprv_trunc_67
	.type	uprv_trunc_67, @function
uprv_trunc_67:
.LFB3241:
	.cfi_startproc
	endbr64
	movq	%xmm0, %rcx
	movq	%xmm0, %rax
	movabsq	$9218868437227405312, %rdx
	btrq	$63, %rcx
	cmpq	%rdx, %rcx
	jg	.L272
	movsd	.LC14(%rip), %xmm0
	je	.L267
	movsd	.LC16(%rip), %xmm2
	movq	%rax, %rdx
	movq	%rax, %xmm1
	movsd	.LC15(%rip), %xmm3
	shrq	$56, %rdx
	movq	%rax, %xmm0
	andpd	%xmm2, %xmm1
	testb	%dl, %dl
	js	.L274
	ucomisd	%xmm1, %xmm3
	jbe	.L267
	cvttsd2siq	%xmm0, %rdx
	pxor	%xmm1, %xmm1
	movq	%rax, %xmm5
	movsd	.LC17(%rip), %xmm3
	andnpd	%xmm5, %xmm2
	cvtsi2sdq	%rdx, %xmm1
	movapd	%xmm1, %xmm4
	cmpnlesd	%xmm0, %xmm4
	movapd	%xmm4, %xmm0
	andpd	%xmm3, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	orpd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	movsd	.LC13(%rip), %xmm0
.L267:
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	ucomisd	%xmm1, %xmm3
	jbe	.L267
	cvttsd2siq	%xmm0, %rdx
	pxor	%xmm1, %xmm1
	movq	%rax, %xmm6
	movsd	.LC17(%rip), %xmm3
	andnpd	%xmm6, %xmm2
	cvtsi2sdq	%rdx, %xmm1
	cmpnlesd	%xmm1, %xmm0
	andpd	%xmm3, %xmm0
	addsd	%xmm1, %xmm0
	orpd	%xmm2, %xmm0
	ret
	.cfi_endproc
.LFE3241:
	.size	uprv_trunc_67, .-uprv_trunc_67
	.p2align 4
	.globl	uprv_maxMantissa_67
	.type	uprv_maxMantissa_67, @function
uprv_maxMantissa_67:
.LFB3242:
	.cfi_startproc
	endbr64
	movsd	.LC20(%rip), %xmm0
	ret
	.cfi_endproc
.LFE3242:
	.size	uprv_maxMantissa_67, .-uprv_maxMantissa_67
	.p2align 4
	.globl	uprv_log_67
	.type	uprv_log_67, @function
uprv_log_67:
.LFB3243:
	.cfi_startproc
	endbr64
	jmp	log@PLT
	.cfi_endproc
.LFE3243:
	.size	uprv_log_67, .-uprv_log_67
	.p2align 4
	.globl	uprv_maximumPtr_67
	.type	uprv_maximumPtr_67, @function
uprv_maximumPtr_67:
.LFB3244:
	.cfi_startproc
	endbr64
	cmpq	$-2147483648, %rdi
	movq	$-2147483648, %rax
	cmova	%rax, %rdi
	leaq	2147483647(%rdi), %rax
	ret
	.cfi_endproc
.LFE3244:
	.size	uprv_maximumPtr_67, .-uprv_maximumPtr_67
	.p2align 4
	.globl	uprv_tzset_67
	.type	uprv_tzset_67, @function
uprv_tzset_67:
.LFB3245:
	.cfi_startproc
	endbr64
	jmp	tzset@PLT
	.cfi_endproc
.LFE3245:
	.size	uprv_tzset_67, .-uprv_tzset_67
	.p2align 4
	.globl	uprv_timezone_67
	.type	uprv_timezone_67, @function
uprv_timezone_67:
.LFB3246:
	.cfi_startproc
	endbr64
	movl	__timezone(%rip), %eax
	ret
	.cfi_endproc
.LFE3246:
	.size	uprv_timezone_67, .-uprv_timezone_67
	.p2align 4
	.globl	uprv_tzname_clear_cache_67
	.type	uprv_tzname_clear_cache_67, @function
uprv_tzname_clear_cache_67:
.LFB3252:
	.cfi_startproc
	endbr64
	movq	$0, _ZL18gTimeZoneBufferPtr(%rip)
	ret
	.cfi_endproc
.LFE3252:
	.size	uprv_tzname_clear_cache_67, .-uprv_tzname_clear_cache_67
	.section	.rodata.str1.1
.LC21:
	.string	"TZ"
.LC22:
	.string	"/zoneinfo/"
.LC23:
	.string	"/usr/share/zoneinfo/"
	.text
	.p2align 4
	.globl	uprv_tzname_67
	.type	uprv_tzname_67, @function
uprv_tzname_67:
.LFB3253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, -180(%rbp)
	leaq	.LC21(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	getenv@PLT
	testq	%rax, %rax
	je	.L282
	movq	%rax, %rdi
	movq	%rax, %r10
	call	_ZL14isValidOlsonIDPKc
	testb	%al, %al
	jne	.L332
.L282:
	movq	_ZL18gTimeZoneBufferPtr(%rip), %r10
	testq	%r10, %r10
	je	.L333
.L281:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L334
	addq	$152, %rsp
	movq	%r10, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	xorl	%eax, %eax
	cmpb	$58, (%r10)
	movl	$6, %ecx
	sete	%al
	leaq	.LC10(%rip), %rdi
	addq	%rax, %r10
	movq	%r10, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L335
.L287:
	addq	$6, %r10
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L333:
	movl	$4095, %edx
	leaq	_ZL15gTimeZoneBuffer(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	call	readlink@PLT
	testl	%eax, %eax
	jle	.L288
	leaq	_ZL15gTimeZoneBuffer(%rip), %rdi
	cltq
	leaq	.LC22(%rip), %rsi
	movb	$0, (%rdi,%rax)
	call	strstr@PLT
	testq	%rax, %rax
	je	.L292
	leaq	10(%rax), %r10
	movq	%r10, %rdi
	call	_ZL14isValidOlsonIDPKc
	testb	%al, %al
	je	.L292
	movq	%r10, _ZL18gTimeZoneBufferPtr(%rip)
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L335:
	movl	$6, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%r10, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L281
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L288:
	movl	$32, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L293
	movb	$0, 24(%rax)
	leaq	.LC23(%rip), %rdi
	movq	%rax, %rsi
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movl	$0, 28(%rax)
	call	_ZL15searchForTZFilePKcP13DefaultTZInfo
	movq	(%r12), %rdi
	movq	%rax, _ZL18gTimeZoneBufferPtr(%rip)
	testq	%rdi, %rdi
	je	.L294
	call	uprv_free_67@PLT
.L294:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L295
	call	fclose@PLT
.L295:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
.L293:
	movq	_ZL18gTimeZoneBufferPtr(%rip), %r10
	testq	%r10, %r10
	je	.L292
	movq	%r10, %rdi
	call	_ZL14isValidOlsonIDPKc
	testb	%al, %al
	jne	.L281
	.p2align 4,,10
	.p2align 3
.L292:
	leaq	-176(%rbp), %rsi
	leaq	_ZZ14uprv_tzname_67E12juneSolstice(%rip), %rdi
	movl	$2, %r12d
	call	localtime_r@PLT
	leaq	-112(%rbp), %rsi
	leaq	_ZZ14uprv_tzname_67E16decemberSolstice(%rip), %rdi
	call	localtime_r@PLT
	movl	-80(%rbp), %edx
	testl	%edx, %edx
	jg	.L290
	movl	-144(%rbp), %eax
	xorl	%r12d, %r12d
	testl	%eax, %eax
	setg	%r12b
.L290:
	movq	8+tzname(%rip), %rax
	movl	__timezone(%rip), %r13d
	xorl	%r15d, %r15d
	leaq	_ZL20OFFSET_ZONE_MAPPINGS(%rip), %rbx
	movq	tzname(%rip), %r14
	movq	%rax, -192(%rbp)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L296:
	addl	$1, %r15d
	addq	$32, %rbx
	cmpl	$59, %r15d
	je	.L298
.L299:
	cmpl	(%rbx), %r13d
	jne	.L296
	cmpl	4(%rbx), %r12d
	jne	.L296
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L296
	movq	16(%rbx), %rdi
	movq	-192(%rbp), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L296
	movslq	%r15d, %rcx
	leaq	_ZL20OFFSET_ZONE_MAPPINGS(%rip), %rax
	salq	$5, %rcx
	movq	24(%rax,%rcx), %r10
	testq	%r10, %r10
	jne	.L281
	.p2align 4,,10
	.p2align 3
.L298:
	movslq	-180(%rbp), %rbx
	leaq	tzname(%rip), %rax
	movq	(%rax,%rbx,8), %r10
	jmp	.L281
.L334:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3253:
	.size	uprv_tzname_67, .-uprv_tzname_67
	.section	.rodata.str1.1
.LC24:
	.string	""
	.text
	.p2align 4
	.globl	u_setDataDirectory_67
	.type	u_setDataDirectory_67, @function
u_setDataDirectory_67:
.LFB3255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L340
	cmpb	$0, (%rdi)
	movq	%rdi, %r12
	leaq	.LC24(%rip), %rbx
	jne	.L346
.L337:
	movq	_ZL14gDataDirectory(%rip), %rdi
	testq	%rdi, %rdi
	je	.L339
	cmpb	$0, (%rdi)
	jne	.L347
.L339:
	movq	%rbx, _ZL14gDataDirectory(%rip)
	leaq	_ZL13putil_cleanupv(%rip), %rsi
	popq	%rbx
	movl	$21, %edi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ucln_common_registerCleanup_67@PLT
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	call	uprv_free_67@PLT
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L346:
	call	strlen@PLT
	leal	2(%rax), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L336
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	strcpy@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L340:
	leaq	.LC24(%rip), %rbx
	jmp	.L337
.L336:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3255:
	.size	u_setDataDirectory_67, .-u_setDataDirectory_67
	.p2align 4
	.globl	uprv_pathIsAbsolute_67
	.type	uprv_pathIsAbsolute_67, @function
uprv_pathIsAbsolute_67:
.LFB3256:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L350
	cmpb	$47, (%rdi)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3256:
	.size	uprv_pathIsAbsolute_67, .-uprv_pathIsAbsolute_67
	.section	.rodata.str1.1
.LC25:
	.string	"ICU_DATA"
	.text
	.p2align 4
	.globl	u_getDataDirectory_67
	.type	u_getDataDirectory_67, @function
u_getDataDirectory_67:
.LFB3258:
	.cfi_startproc
	endbr64
	movl	_ZL16gDataDirInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L377
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL16gDataDirInitOnce(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L353
	cmpq	$0, _ZL14gDataDirectory(%rip)
	je	.L380
.L356:
	leaq	_ZL16gDataDirInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L353:
	popq	%rbx
	movq	_ZL14gDataDirectory(%rip), %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movq	_ZL14gDataDirectory(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	.LC25(%rip), %rdi
	call	getenv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L363
	cmpb	$0, (%rax)
	je	.L363
	movq	%r12, %rdi
	call	strlen@PLT
	leal	2(%rax), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L356
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	strcpy@PLT
.L357:
	movq	_ZL14gDataDirectory(%rip), %rdi
	testq	%rdi, %rdi
	je	.L360
	cmpb	$0, (%rdi)
	jne	.L381
.L360:
	leaq	_ZL13putil_cleanupv(%rip), %rsi
	movl	$21, %edi
	movq	%rbx, _ZL14gDataDirectory(%rip)
	call	ucln_common_registerCleanup_67@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L363:
	leaq	.LC24(%rip), %rbx
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L381:
	call	uprv_free_67@PLT
	jmp	.L360
	.cfi_endproc
.LFE3258:
	.size	u_getDataDirectory_67, .-u_getDataDirectory_67
	.section	.rodata.str1.1
.LC26:
	.string	"ICU_TIMEZONE_FILES_DIR"
	.text
	.p2align 4
	.globl	u_getTimeZoneFilesDirectory_67
	.type	u_getTimeZoneFilesDirectory_67, @function
u_getTimeZoneFilesDirectory_67:
.LFB3261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L398
.L383:
	leaq	.LC24(%rip), %rax
.L382:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L399
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	movl	gTimeZoneFilesInitOnce_67(%rip), %eax
	movq	%rdi, %rbx
	cmpl	$2, %eax
	jne	.L400
.L384:
	movl	4+gTimeZoneFilesInitOnce_67(%rip), %eax
	testl	%eax, %eax
	jle	.L388
	movl	%eax, (%rbx)
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L400:
	leaq	gTimeZoneFilesInitOnce_67(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L384
	movl	$21, %edi
	leaq	_ZL13putil_cleanupv(%rip), %rsi
	call	ucln_common_registerCleanup_67@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L385
	leaq	13(%rax), %rdx
	movl	$0, 56(%rax)
	leaq	.LC26(%rip), %rdi
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movl	$40, 8(%rax)
	movw	%dx, 12(%rax)
	movq	%rax, _ZL23gTimeZoneFilesDirectory(%rip)
	call	getenv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	leaq	.LC24(%rip), %rax
	cmove	%rax, %rsi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L387
	movq	_ZL23gTimeZoneFilesDirectory(%rip), %r12
	leaq	-48(%rbp), %rdi
	movl	$0, 56(%r12)
	movq	(%r12), %rax
	movb	$0, (%rax)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-40(%rbp), %edx
	movq	-48(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%rbx), %eax
.L387:
	leaq	gTimeZoneFilesInitOnce_67(%rip), %rdi
	movl	%eax, 4+gTimeZoneFilesInitOnce_67(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L388:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L383
	movq	_ZL23gTimeZoneFilesDirectory(%rip), %rax
	movq	(%rax), %rax
	jmp	.L382
.L399:
	call	__stack_chk_fail@PLT
.L385:
	movq	$0, _ZL23gTimeZoneFilesDirectory(%rip)
	movl	$7, %eax
	movl	$7, (%rbx)
	jmp	.L387
	.cfi_endproc
.LFE3261:
	.size	u_getTimeZoneFilesDirectory_67, .-u_getTimeZoneFilesDirectory_67
	.p2align 4
	.globl	u_setTimeZoneFilesDirectory_67
	.type	u_setTimeZoneFilesDirectory_67, @function
u_setTimeZoneFilesDirectory_67:
.LFB3262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L417
.L401:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L418
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	movl	gTimeZoneFilesInitOnce_67(%rip), %eax
	movq	%rdi, %r13
	movq	%rsi, %r12
	cmpl	$2, %eax
	jne	.L419
.L403:
	movl	4+gTimeZoneFilesInitOnce_67(%rip), %eax
	testl	%eax, %eax
	jle	.L407
	movl	%eax, (%r12)
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L419:
	leaq	gTimeZoneFilesInitOnce_67(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L403
	movl	$21, %edi
	leaq	_ZL13putil_cleanupv(%rip), %rsi
	call	ucln_common_registerCleanup_67@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L404
	leaq	13(%rax), %rdx
	movl	$0, 56(%rax)
	leaq	.LC26(%rip), %rdi
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movl	$40, 8(%rax)
	movw	%dx, 12(%rax)
	movq	%rax, _ZL23gTimeZoneFilesDirectory(%rip)
	call	getenv@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	leaq	.LC24(%rip), %rax
	cmove	%rax, %rsi
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L406
	movq	_ZL23gTimeZoneFilesDirectory(%rip), %r14
	leaq	-64(%rbp), %rdi
	movq	(%r14), %rax
	movl	$0, 56(%r14)
	movb	$0, (%rax)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%r12), %eax
.L406:
	leaq	gTimeZoneFilesInitOnce_67(%rip), %rdi
	movl	%eax, 4+gTimeZoneFilesInitOnce_67(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L407:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L401
	movq	_ZL23gTimeZoneFilesDirectory(%rip), %r14
	leaq	-64(%rbp), %rdi
	movq	%r13, %rsi
	movq	(%r14), %rax
	movl	$0, 56(%r14)
	movb	$0, (%rax)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L401
.L418:
	call	__stack_chk_fail@PLT
.L404:
	movq	$0, _ZL23gTimeZoneFilesDirectory(%rip)
	movl	$7, %eax
	movl	$7, (%r12)
	jmp	.L406
	.cfi_endproc
.LFE3262:
	.size	u_setTimeZoneFilesDirectory_67, .-u_setTimeZoneFilesDirectory_67
	.section	.rodata.str1.1
.LC27:
	.string	"en_US_POSIX"
.LC28:
	.string	"NY"
.LC29:
	.string	"POSIX"
.LC30:
	.string	"LC_ALL"
.LC31:
	.string	"LC_MESSAGES"
.LC32:
	.string	"LANG"
.LC33:
	.string	"nynorsk"
	.text
	.p2align 4
	.globl	uprv_getDefaultLocaleID_67
	.type	uprv_getDefaultLocaleID_67, @function
uprv_getDefaultLocaleID_67:
.LFB3265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	_ZZL31uprv_getPOSIXIDForDefaultLocalevE7posixID(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%r13, %r13
	je	.L471
.L421:
	movq	_ZL21gCorrectedPOSIXLocale(%rip), %r14
	testq	%r14, %r14
	je	.L472
.L420:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	movq	%r13, %rdi
	call	strlen@PLT
	leaq	11(%rax), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L420
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	strcpy@PLT
	movl	$46, %esi
	movq	%r12, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L429
	movb	$0, (%rax)
.L429:
	movl	$64, %esi
	movq	%r12, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L430
	movb	$0, (%rax)
.L430:
	cmpb	$67, (%r12)
	je	.L473
.L443:
	movl	$6, %ecx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L433
.L432:
	movabsq	$5715172738652401253, %rax
	movl	$5785939, 8(%r12)
	movq	%rax, (%r12)
.L433:
	movl	$64, %esi
	movq	%r13, %rdi
	call	strrchr@PLT
	testq	%rax, %rax
	je	.L434
	leaq	1(%rax), %r13
	movl	$8, %ecx
	leaq	.LC33(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	movl	$95, %esi
	movq	%r12, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	leaq	.LC28(%rip), %rax
	cmove	%rax, %r13
	call	strchr@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	strlen@PLT
	addq	%r12, %rax
	testq	%rbx, %rbx
	je	.L474
	movl	$95, %edx
	movw	%dx, (%rax)
.L437:
	movl	$46, %esi
	movq	%r13, %rdi
	call	strchr@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L438
	movq	%r12, %rdi
	subq	%r13, %r14
	call	strlen@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	strncat@PLT
	addl	%ebx, %r14d
	movslq	%r14d, %r14
	movb	$0, (%r12,%r14)
.L434:
	movq	_ZL21gCorrectedPOSIXLocale(%rip), %r14
	testq	%r14, %r14
	je	.L475
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	cmpb	$0, 1(%r12)
	je	.L432
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L475:
	leaq	_ZL13putil_cleanupv(%rip), %rsi
	movl	$21, %edi
	movq	%r12, _ZL21gCorrectedPOSIXLocale(%rip)
	movb	$1, _ZL34gCorrectedPOSIXLocaleHeapAllocated(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movq	_ZL21gCorrectedPOSIXLocale(%rip), %r14
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	strcat@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L474:
	movl	$24415, %ecx
	movb	$0, 2(%rax)
	movw	%cx, (%rax)
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L471:
	xorl	%esi, %esi
	movl	$5, %edi
	call	setlocale@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L422
	cmpb	$67, (%rax)
	jne	.L441
	cmpb	$0, 1(%rax)
	jne	.L441
.L422:
	leaq	.LC30(%rip), %rdi
	call	getenv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L476
.L425:
	cmpb	$67, 0(%r13)
	je	.L477
.L442:
	movl	$6, %ecx
	leaq	.LC29(%rip), %rsi
	movq	%r13, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L424
.L427:
	leaq	.LC27(%rip), %r13
.L424:
	movq	%r13, _ZZL31uprv_getPOSIXIDForDefaultLocalevE7posixID(%rip)
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L441:
	movl	$6, %ecx
	leaq	.LC29(%rip), %rsi
	movq	%r13, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L424
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L477:
	cmpb	$0, 1(%r13)
	je	.L427
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L476:
	leaq	.LC31(%rip), %rdi
	call	getenv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L425
	leaq	.LC32(%rip), %rdi
	call	getenv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L427
	jmp	.L425
	.cfi_endproc
.LFE3265:
	.size	uprv_getDefaultLocaleID_67, .-uprv_getDefaultLocaleID_67
	.p2align 4
	.globl	u_versionFromString_67
	.type	u_versionFromString_67, @function
u_versionFromString_67:
.LFB3266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L478
	movq	%rdi, %r15
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L487
	xorl	%ebx, %ebx
	leaq	-64(%rbp), %r14
.L484:
	movl	$10, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%ebx, %r13d
	call	strtoul@PLT
	leal	1(%rbx), %edx
	movb	%al, (%r15,%rbx)
	movq	-64(%rbp), %rax
	cmpq	%r12, %rax
	je	.L480
	cmpq	$3, %rbx
	je	.L478
	addq	$1, %rbx
	cmpb	$46, (%rax)
	jne	.L488
	leaq	1(%rax), %r12
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L487:
	xorl	%r13d, %r13d
.L480:
	movl	$3, %edx
	leal	1(%r13), %eax
	movzwl	%r13w, %edi
	subl	%r13d, %edx
	movzwl	%dx, %edx
	addq	$1, %rdx
	cmpw	$4, %ax
	movl	$1, %eax
	cmova	%rax, %rdx
	addq	%r15, %rdi
	xorl	%esi, %esi
	call	memset@PLT
.L478:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L490
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	movl	%edx, %r13d
	jmp	.L480
.L490:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3266:
	.size	u_versionFromString_67, .-u_versionFromString_67
	.p2align 4
	.globl	u_versionFromUString_67
	.type	u_versionFromUString_67, @function
u_versionFromUString_67:
.LFB3267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L491
	movq	%rsi, %r13
	testq	%rsi, %rsi
	jne	.L506
.L491:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L507
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	-80(%rbp), %r14
	movl	$20, %ebx
	call	u_strlen_67@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r14, %r13
	cmpl	$20, %eax
	leaq	-88(%rbp), %r15
	cmovle	%eax, %ebx
	movl	%ebx, %edx
	movslq	%ebx, %rbx
	call	u_UCharsToChars_67@PLT
	movb	$0, -80(%rbp,%rbx)
	xorl	%ebx, %ebx
.L495:
	movl	$10, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%ebx, %r14d
	call	strtoul@PLT
	leal	1(%rbx), %edx
	movb	%al, (%r12,%rbx)
	movq	-88(%rbp), %rax
	cmpq	%rax, %r13
	je	.L493
	cmpq	$3, %rbx
	je	.L491
	addq	$1, %rbx
	cmpb	$46, (%rax)
	jne	.L498
	leaq	1(%rax), %r13
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L498:
	movl	%edx, %r14d
.L493:
	movl	$3, %edx
	leal	1(%r14), %eax
	movzwl	%r14w, %edi
	subl	%r14d, %edx
	movzwl	%dx, %edx
	addq	$1, %rdx
	cmpw	$4, %ax
	movl	$1, %eax
	cmova	%rax, %rdx
	addq	%r12, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L491
.L507:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3267:
	.size	u_versionFromUString_67, .-u_versionFromUString_67
	.p2align 4
	.globl	u_versionToString_67
	.type	u_versionToString_67, @function
u_versionToString_67:
.LFB3268:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L508
	testq	%rdi, %rdi
	je	.L538
	cmpb	$0, 3(%rdi)
	jne	.L524
	cmpb	$0, 2(%rdi)
	jne	.L525
	cmpb	$0, 1(%rdi)
	jne	.L526
	cmpb	$0, (%rdi)
	jne	.L527
	xorl	%edx, %edx
	leaq	1(%rsi), %r9
	movl	$2, %r8d
.L513:
	addl	$48, %edx
	movb	%dl, (%rsi)
	leaq	1(%r9), %rdx
	movb	$46, (%r9)
	movzbl	1(%rdi), %ecx
	cmpb	$99, %cl
	ja	.L539
	leaq	1(%rdx), %rsi
	cmpb	$9, %cl
	jbe	.L516
.L541:
	movsbw	%cl, %ax
	movl	%ecx, %r9d
	imull	$103, %eax, %eax
	sarb	$7, %r9b
	sarw	$10, %ax
	subl	%r9d, %eax
	leal	48(%rax), %r9d
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	movb	%r9b, (%rdx)
	subl	%eax, %ecx
	leaq	2(%rdx), %rax
	movq	%rsi, %rdx
	movq	%rax, %rsi
.L516:
	addl	$48, %ecx
	movb	%cl, (%rdx)
	cmpw	$2, %r8w
	je	.L517
	movb	$46, (%rsi)
	movzbl	2(%rdi), %ecx
	leaq	1(%rsi), %rdx
	cmpb	$99, %cl
	jbe	.L519
	movzbl	%cl, %r9d
	leaq	2(%rsi), %rdx
	leal	(%r9,%r9,4), %eax
	leal	(%r9,%rax,8), %eax
	shrw	$12, %ax
	leal	48(%rax), %r9d
	movb	%r9b, 1(%rsi)
	movl	$100, %esi
	imull	%esi, %eax
	subl	%eax, %ecx
.L519:
	leaq	1(%rdx), %rsi
	cmpb	$9, %cl
	jbe	.L520
	movsbw	%cl, %ax
	movl	%ecx, %r9d
	imull	$103, %eax, %eax
	sarb	$7, %r9b
	sarw	$10, %ax
	subl	%r9d, %eax
	leal	48(%rax), %r9d
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	movb	%r9b, (%rdx)
	subl	%eax, %ecx
	leaq	2(%rdx), %rax
	movq	%rsi, %rdx
	movq	%rax, %rsi
.L520:
	addl	$48, %ecx
	movb	%cl, (%rdx)
	cmpw	$3, %r8w
	je	.L517
	movb	$46, (%rsi)
	movzbl	3(%rdi), %ecx
	leaq	1(%rsi), %rdx
	cmpb	$99, %cl
	ja	.L540
	leaq	1(%rdx), %rsi
	cmpb	$9, %cl
	jbe	.L523
.L542:
	movsbw	%cl, %ax
	movl	%ecx, %edi
	imull	$103, %eax, %eax
	sarb	$7, %dil
	sarw	$10, %ax
	subl	%edi, %eax
	leal	48(%rax), %edi
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	movb	%dil, (%rdx)
	movq	%rsi, %rdx
	subl	%eax, %ecx
.L523:
	addl	$48, %ecx
	movb	%cl, (%rdx)
.L517:
	movb	$0, 1(%rdx)
.L508:
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	movzbl	%cl, %esi
	leaq	2(%r9), %rdx
	leal	(%rsi,%rsi,4), %eax
	leal	(%rsi,%rax,8), %eax
	shrw	$12, %ax
	leal	48(%rax), %esi
	movb	%sil, 1(%r9)
	movl	$100, %esi
	imull	%esi, %eax
	leaq	1(%rdx), %rsi
	subl	%eax, %ecx
	cmpb	$9, %cl
	jbe	.L516
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L540:
	movzbl	%cl, %edi
	leaq	2(%rsi), %rdx
	leal	(%rdi,%rdi,4), %eax
	leal	(%rdi,%rax,8), %eax
	shrw	$12, %ax
	leal	48(%rax), %edi
	movb	%dil, 1(%rsi)
	movl	$100, %esi
	imull	%esi, %eax
	leaq	1(%rdx), %rsi
	subl	%eax, %ecx
	cmpb	$9, %cl
	jbe	.L523
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L538:
	movb	$0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	movl	$4, %r8d
.L511:
	movzbl	(%rdi), %edx
	cmpw	$2, %r8w
	leaq	1(%rsi), %r10
	leaq	2(%rsi), %r9
	movl	$2, %eax
	cmovb	%eax, %r8d
	cmpb	$99, %dl
	jbe	.L528
	movzbl	%dl, %ecx
	leal	(%rcx,%rcx,4), %eax
	leal	(%rcx,%rax,8), %eax
	shrw	$12, %ax
	leal	48(%rax), %ecx
	movb	%cl, (%rsi)
	movl	$100, %ecx
	imull	%ecx, %eax
	leaq	3(%rsi), %rcx
	movq	%r10, %rsi
	subl	%eax, %edx
.L512:
	cmpb	$9, %dl
	jbe	.L513
	movsbw	%dl, %ax
	movl	%edx, %r10d
	imull	$103, %eax, %eax
	sarb	$7, %r10b
	sarw	$10, %ax
	subl	%r10d, %eax
	leal	48(%rax), %r10d
	leal	(%rax,%rax,4), %eax
	addl	%eax, %eax
	movb	%r10b, (%rsi)
	movq	%r9, %rsi
	movq	%rcx, %r9
	subl	%eax, %edx
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L525:
	movl	$3, %r8d
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L526:
	movl	$2, %r8d
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L527:
	movl	$1, %r8d
	jmp	.L511
.L528:
	movq	%r9, %rcx
	movq	%r10, %r9
	jmp	.L512
	.cfi_endproc
.LFE3268:
	.size	u_versionToString_67, .-u_versionToString_67
	.section	.rodata.str1.1
.LC34:
	.string	"67.1"
	.text
	.p2align 4
	.globl	u_getVersion_67
	.type	u_getVersion_67, @function
u_getVersion_67:
.LFB3269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L543
	movq	%rdi, %r12
	leaq	.LC34(%rip), %r13
	leaq	-64(%rbp), %r15
	xorl	%ebx, %ebx
.L544:
	movl	$10, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%ebx, %r14d
	call	strtoul@PLT
	leal	1(%rbx), %edx
	movb	%al, (%r12,%rbx)
	movq	-64(%rbp), %rax
	cmpq	%rax, %r13
	je	.L545
	cmpq	$3, %rbx
	je	.L543
	addq	$1, %rbx
	cmpb	$46, (%rax)
	jne	.L551
	leaq	1(%rax), %r13
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L551:
	movl	%edx, %r14d
.L545:
	movl	$3, %edx
	leal	1(%r14), %eax
	movzwl	%r14w, %edi
	subl	%r14d, %edx
	movzwl	%dx, %edx
	addq	$1, %rdx
	cmpw	$4, %ax
	movl	$1, %eax
	cmova	%rax, %rdx
	addq	%r12, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	.p2align 4,,10
	.p2align 3
.L543:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L554
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L554:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3269:
	.size	u_getVersion_67, .-u_getVersion_67
	.p2align 4
	.globl	uprv_dl_open_67
	.type	uprv_dl_open_67, @function
uprv_dl_open_67:
.LFB3270:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L556
	movl	$16, (%rsi)
.L556:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3270:
	.size	uprv_dl_open_67, .-uprv_dl_open_67
	.p2align 4
	.globl	uprv_dl_close_67
	.type	uprv_dl_close_67, @function
uprv_dl_close_67:
.LFB3271:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L557
	movl	$16, (%rsi)
.L557:
	ret
	.cfi_endproc
.LFE3271:
	.size	uprv_dl_close_67, .-uprv_dl_close_67
	.p2align 4
	.globl	uprv_dlsym_func_67
	.type	uprv_dlsym_func_67, @function
uprv_dlsym_func_67:
.LFB3272:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L560
	movl	$16, (%rdx)
.L560:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3272:
	.size	uprv_dlsym_func_67, .-uprv_dlsym_func_67
	.local	_ZZL31uprv_getPOSIXIDForDefaultLocalevE7posixID
	.comm	_ZZL31uprv_getPOSIXIDForDefaultLocalevE7posixID,8,8
	.local	_ZL34gCorrectedPOSIXLocaleHeapAllocated
	.comm	_ZL34gCorrectedPOSIXLocaleHeapAllocated,1,1
	.local	_ZL21gCorrectedPOSIXLocale
	.comm	_ZL21gCorrectedPOSIXLocale,8,8
	.local	_ZL23gTimeZoneFilesDirectory
	.comm	_ZL23gTimeZoneFilesDirectory,8,8
	.globl	gTimeZoneFilesInitOnce_67
	.bss
	.align 8
	.type	gTimeZoneFilesInitOnce_67, @object
	.size	gTimeZoneFilesInitOnce_67, 8
gTimeZoneFilesInitOnce_67:
	.zero	8
	.local	_ZL14gDataDirectory
	.comm	_ZL14gDataDirectory,8,8
	.local	_ZL16gDataDirInitOnce
	.comm	_ZL16gDataDirInitOnce,8,8
	.section	.rodata
	.align 8
	.type	_ZZ14uprv_tzname_67E16decemberSolstice, @object
	.size	_ZZ14uprv_tzname_67E16decemberSolstice, 8
_ZZ14uprv_tzname_67E16decemberSolstice:
	.quad	1198332540
	.align 8
	.type	_ZZ14uprv_tzname_67E12juneSolstice, @object
	.size	_ZZ14uprv_tzname_67E12juneSolstice, 8
_ZZ14uprv_tzname_67E12juneSolstice:
	.quad	1182478260
	.local	_ZL19gSearchTZFileResult
	.comm	_ZL19gSearchTZFileResult,8,8
	.section	.rodata.str1.1
.LC35:
	.string	"CHAST"
.LC36:
	.string	"CHADT"
.LC37:
	.string	"Pacific/Chatham"
.LC38:
	.string	"PETT"
.LC39:
	.string	"PETST"
.LC40:
	.string	"Asia/Kamchatka"
.LC41:
	.string	"NZST"
.LC42:
	.string	"NZDT"
.LC43:
	.string	"Pacific/Auckland"
.LC44:
	.string	"ANAT"
.LC45:
	.string	"ANAST"
.LC46:
	.string	"Asia/Anadyr"
.LC47:
	.string	"MAGT"
.LC48:
	.string	"MAGST"
.LC49:
	.string	"Asia/Magadan"
.LC50:
	.string	"LHST"
.LC51:
	.string	"Australia/Lord_Howe"
.LC52:
	.string	"EST"
.LC53:
	.string	"Australia/Sydney"
.LC54:
	.string	"SAKT"
.LC55:
	.string	"SAKST"
.LC56:
	.string	"Asia/Sakhalin"
.LC57:
	.string	"VLAT"
.LC58:
	.string	"VLAST"
.LC59:
	.string	"Asia/Vladivostok"
.LC60:
	.string	"CST"
.LC61:
	.string	"Australia/South"
.LC62:
	.string	"YAKT"
.LC63:
	.string	"YAKST"
.LC64:
	.string	"Asia/Yakutsk"
.LC65:
	.string	"CHOT"
.LC66:
	.string	"CHOST"
.LC67:
	.string	"Asia/Choibalsan"
.LC68:
	.string	"CWST"
.LC69:
	.string	"Australia/Eucla"
.LC70:
	.string	"IRKT"
.LC71:
	.string	"IRKST"
.LC72:
	.string	"Asia/Irkutsk"
.LC73:
	.string	"ULAT"
.LC74:
	.string	"ULAST"
.LC75:
	.string	"Asia/Ulaanbaatar"
.LC76:
	.string	"WST"
.LC77:
	.string	"Australia/West"
.LC78:
	.string	"HOVT"
.LC79:
	.string	"HOVST"
.LC80:
	.string	"Asia/Hovd"
.LC81:
	.string	"KRAT"
.LC82:
	.string	"KRAST"
.LC83:
	.string	"Asia/Krasnoyarsk"
.LC84:
	.string	"NOVT"
.LC85:
	.string	"NOVST"
.LC86:
	.string	"Asia/Novosibirsk"
.LC87:
	.string	"OMST"
.LC88:
	.string	"OMSST"
.LC89:
	.string	"Asia/Omsk"
.LC90:
	.string	"YEKT"
.LC91:
	.string	"YEKST"
.LC92:
	.string	"Asia/Yekaterinburg"
.LC93:
	.string	"SAMT"
.LC94:
	.string	"SAMST"
.LC95:
	.string	"Europe/Samara"
.LC96:
	.string	"AMT"
.LC97:
	.string	"AMST"
.LC98:
	.string	"Asia/Yerevan"
.LC99:
	.string	"AZT"
.LC100:
	.string	"AZST"
.LC101:
	.string	"Asia/Baku"
.LC102:
	.string	"AST"
.LC103:
	.string	"ADT"
.LC104:
	.string	"Asia/Baghdad"
.LC105:
	.string	"MSK"
.LC106:
	.string	"MSD"
.LC107:
	.string	"Europe/Moscow"
.LC108:
	.string	"VOLT"
.LC109:
	.string	"VOLST"
.LC110:
	.string	"Europe/Volgograd"
.LC111:
	.string	"EET"
.LC112:
	.string	"CEST"
.LC113:
	.string	"Africa/Tripoli"
.LC114:
	.string	"EEST"
.LC115:
	.string	"Europe/Athens"
.LC116:
	.string	"IST"
.LC117:
	.string	"IDT"
.LC118:
	.string	"Asia/Jerusalem"
.LC119:
	.string	"CET"
.LC120:
	.string	"WEST"
.LC121:
	.string	"Africa/Algiers"
.LC122:
	.string	"WAT"
.LC123:
	.string	"WAST"
.LC124:
	.string	"Africa/Windhoek"
.LC125:
	.string	"GMT"
.LC126:
	.string	"Europe/Dublin"
.LC127:
	.string	"BST"
.LC128:
	.string	"Europe/London"
.LC129:
	.string	"WET"
.LC130:
	.string	"Africa/Casablanca"
.LC131:
	.string	"Africa/El_Aaiun"
.LC132:
	.string	"AZOT"
.LC133:
	.string	"AZOST"
.LC134:
	.string	"Atlantic/Azores"
.LC135:
	.string	"EGT"
.LC136:
	.string	"EGST"
.LC137:
	.string	"America/Scoresbysund"
.LC138:
	.string	"PMST"
.LC139:
	.string	"PMDT"
.LC140:
	.string	"America/Miquelon"
.LC141:
	.string	"UYT"
.LC142:
	.string	"UYST"
.LC143:
	.string	"America/Montevideo"
.LC144:
	.string	"WGT"
.LC145:
	.string	"WGST"
.LC146:
	.string	"America/Godthab"
.LC147:
	.string	"BRT"
.LC148:
	.string	"BRST"
.LC149:
	.string	"Brazil/East"
.LC150:
	.string	"NST"
.LC151:
	.string	"NDT"
.LC152:
	.string	"America/St_Johns"
.LC153:
	.string	"Canada/Atlantic"
.LC154:
	.string	"America/Cuiaba"
.LC155:
	.string	"CLT"
.LC156:
	.string	"CLST"
.LC157:
	.string	"Chile/Continental"
.LC158:
	.string	"FKT"
.LC159:
	.string	"FKST"
.LC160:
	.string	"Atlantic/Stanley"
.LC161:
	.string	"PYT"
.LC162:
	.string	"PYST"
.LC163:
	.string	"America/Asuncion"
.LC164:
	.string	"CDT"
.LC165:
	.string	"America/Havana"
.LC166:
	.string	"EDT"
.LC167:
	.string	"US/Eastern"
.LC168:
	.string	"EAST"
.LC169:
	.string	"EASST"
.LC170:
	.string	"Chile/EasterIsland"
.LC171:
	.string	"MDT"
.LC172:
	.string	"Canada/Saskatchewan"
.LC173:
	.string	"America/Guatemala"
.LC174:
	.string	"US/Central"
.LC175:
	.string	"MST"
.LC176:
	.string	"US/Mountain"
.LC177:
	.string	"PST"
.LC178:
	.string	"Pacific/Pitcairn"
.LC179:
	.string	"PDT"
.LC180:
	.string	"US/Pacific"
.LC181:
	.string	"AKST"
.LC182:
	.string	"AKDT"
.LC183:
	.string	"US/Alaska"
.LC184:
	.string	"HAST"
.LC185:
	.string	"HADT"
.LC186:
	.string	"US/Aleutian"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL20OFFSET_ZONE_MAPPINGS, @object
	.size	_ZL20OFFSET_ZONE_MAPPINGS, 1888
_ZL20OFFSET_ZONE_MAPPINGS:
	.long	-45900
	.long	2
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.long	-43200
	.long	1
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.long	-43200
	.long	2
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.long	-43200
	.long	1
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.long	-39600
	.long	1
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.long	-37800
	.long	2
	.quad	.LC50
	.quad	.LC50
	.quad	.LC51
	.long	-36000
	.long	2
	.quad	.LC52
	.quad	.LC52
	.quad	.LC53
	.long	-36000
	.long	1
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.long	-36000
	.long	1
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.long	-34200
	.long	2
	.quad	.LC60
	.quad	.LC60
	.quad	.LC61
	.long	-32400
	.long	1
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.long	-32400
	.long	1
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.long	-31500
	.long	2
	.quad	.LC68
	.quad	.LC68
	.quad	.LC69
	.long	-28800
	.long	1
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.long	-28800
	.long	1
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.long	-28800
	.long	2
	.quad	.LC76
	.quad	.LC76
	.quad	.LC77
	.long	-25200
	.long	1
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.long	-25200
	.long	1
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.long	-21600
	.long	1
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.long	-21600
	.long	1
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.long	-18000
	.long	1
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.long	-14400
	.long	1
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.long	-14400
	.long	1
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.long	-14400
	.long	1
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.long	-10800
	.long	1
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.long	-10800
	.long	1
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.long	-10800
	.long	1
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.long	-7200
	.long	0
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.long	-7200
	.long	1
	.quad	.LC111
	.quad	.LC114
	.quad	.LC115
	.long	-7200
	.long	1
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.long	-3600
	.long	0
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.long	-3600
	.long	2
	.quad	.LC122
	.quad	.LC123
	.quad	.LC124
	.long	0
	.long	1
	.quad	.LC125
	.quad	.LC116
	.quad	.LC126
	.long	0
	.long	1
	.quad	.LC125
	.quad	.LC127
	.quad	.LC128
	.long	0
	.long	0
	.quad	.LC129
	.quad	.LC120
	.quad	.LC130
	.long	0
	.long	0
	.quad	.LC129
	.quad	.LC129
	.quad	.LC131
	.long	3600
	.long	1
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.long	3600
	.long	1
	.quad	.LC135
	.quad	.LC136
	.quad	.LC137
	.long	10800
	.long	1
	.quad	.LC138
	.quad	.LC139
	.quad	.LC140
	.long	10800
	.long	2
	.quad	.LC141
	.quad	.LC142
	.quad	.LC143
	.long	10800
	.long	1
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.long	10800
	.long	2
	.quad	.LC147
	.quad	.LC148
	.quad	.LC149
	.long	12600
	.long	1
	.quad	.LC150
	.quad	.LC151
	.quad	.LC152
	.long	14400
	.long	1
	.quad	.LC102
	.quad	.LC103
	.quad	.LC153
	.long	14400
	.long	2
	.quad	.LC96
	.quad	.LC97
	.quad	.LC154
	.long	14400
	.long	2
	.quad	.LC155
	.quad	.LC156
	.quad	.LC157
	.long	14400
	.long	2
	.quad	.LC158
	.quad	.LC159
	.quad	.LC160
	.long	14400
	.long	2
	.quad	.LC161
	.quad	.LC162
	.quad	.LC163
	.long	18000
	.long	1
	.quad	.LC60
	.quad	.LC164
	.quad	.LC165
	.long	18000
	.long	1
	.quad	.LC52
	.quad	.LC166
	.quad	.LC167
	.long	21600
	.long	2
	.quad	.LC168
	.quad	.LC169
	.quad	.LC170
	.long	21600
	.long	0
	.quad	.LC60
	.quad	.LC171
	.quad	.LC172
	.long	21600
	.long	0
	.quad	.LC60
	.quad	.LC164
	.quad	.LC173
	.long	21600
	.long	1
	.quad	.LC60
	.quad	.LC164
	.quad	.LC174
	.long	25200
	.long	1
	.quad	.LC175
	.quad	.LC171
	.quad	.LC176
	.long	28800
	.long	0
	.quad	.LC177
	.quad	.LC177
	.quad	.LC178
	.long	28800
	.long	1
	.quad	.LC177
	.quad	.LC179
	.quad	.LC180
	.long	32400
	.long	1
	.quad	.LC181
	.quad	.LC182
	.quad	.LC183
	.long	36000
	.long	1
	.quad	.LC184
	.quad	.LC185
	.quad	.LC186
	.local	_ZL18gTimeZoneBufferPtr
	.comm	_ZL18gTimeZoneBufferPtr,8,8
	.local	_ZL15gTimeZoneBuffer
	.comm	_ZL15gTimeZoneBuffer,4096,32
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC12:
	.long	0
	.long	0
	.align 8
.LC13:
	.long	0
	.long	2146959360
	.align 8
.LC14:
	.long	0
	.long	2146435072
	.align 8
.LC15:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC16:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC17:
	.long	0
	.long	1072693248
	.align 8
.LC18:
	.long	0
	.long	1071644672
	.align 8
.LC19:
	.long	0
	.long	1076101120
	.align 8
.LC20:
	.long	0
	.long	1129316352
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
