	.file	"ucnvbocu.cpp"
	.text
	.p2align 4
	.type	_ZL8packDiffi, @function
_ZL8packDiffi:
.LFB2029:
	.cfi_startproc
	cmpl	$-64, %edi
	jl	.L2
	cmpl	$10512, %edi
	jg	.L3
	subl	$64, %edi
	movslq	%edi, %rdx
	movl	%edi, %ecx
	imulq	$-2032597691, %rdx, %rdx
	sarl	$31, %ecx
	shrq	$32, %rdx
	addl	%edi, %edx
	sarl	$7, %edx
	movl	%edx, %eax
	subl	%ecx, %edx
	subl	%ecx, %eax
	imull	$243, %eax, %eax
	subl	%eax, %edi
	movslq	%edi, %rax
	leal	13(%rax), %ecx
	cmpl	$19, %eax
	jg	.L5
	leaq	_ZL16bocu1TrailToByte(%rip), %rcx
	movsbl	(%rcx,%rax), %ecx
.L5:
	leal	208(%rdx), %eax
	sall	$8, %eax
	orl	%ecx, %eax
	orl	$33554432, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$-10513, %edi
	jl	.L18
	addl	$64, %edi
	movl	$1, %esi
	movslq	%edi, %rdx
	movl	%edi, %eax
	imulq	$-2032597691, %rdx, %rdx
	sarl	$31, %eax
	shrq	$32, %rdx
	addl	%edi, %edx
	sarl	$7, %edx
	movl	%edx, %ecx
	subl	%eax, %edx
	subl	%eax, %ecx
	imull	$243, %ecx, %ecx
	subl	%ecx, %edi
	je	.L21
	leal	243(%rdi), %eax
	subl	$1, %edx
	cmpl	$19, %eax
	jle	.L48
	leal	256(%rdi), %esi
.L21:
	leal	80(%rdx), %eax
	sall	$8, %eax
	orl	%esi, %eax
	orl	$33554432, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	$-187660, %edi
	jge	.L49
	addl	$187660, %edi
	movslq	%edi, %rsi
	movl	%edi, %edx
	imulq	$-2032597691, %rsi, %rsi
	sarl	$31, %edx
	shrq	$32, %rsi
	addl	%edi, %esi
	sarl	$7, %esi
	movl	%esi, %eax
	subl	%edx, %esi
	subl	%edx, %eax
	movl	$1, %edx
	imull	$243, %eax, %eax
	subl	%eax, %edi
	je	.L31
	leal	243(%rdi), %edx
	subl	$1, %esi
	cmpl	$19, %edx
	jle	.L50
	leal	256(%rdi), %edx
.L31:
	movslq	%esi, %rcx
	movl	%esi, %eax
	movl	$256, %r9d
	imulq	$-2032597691, %rcx, %rcx
	sarl	$31, %eax
	shrq	$32, %rcx
	addl	%esi, %ecx
	sarl	$7, %ecx
	movl	%ecx, %edi
	subl	%eax, %edi
	imull	$243, %edi, %r8d
	movl	%edi, %ecx
	subl	%r8d, %esi
	je	.L34
	leal	243(%rsi), %eax
	subl	$1, %ecx
	cmpl	$19, %eax
	jle	.L51
	leal	256(%rsi), %eax
	sall	$8, %eax
	movl	%eax, %r9d
.L34:
	leal	243(%rcx), %eax
	cmpl	$19, %eax
	jle	.L35
	leal	256(%rcx), %eax
	sall	$16, %eax
.L36:
	orl	%edx, %eax
	orl	%r9d, %eax
	orl	$553648128, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$187659, %edi
	jle	.L52
	leal	-187660(%rdi), %esi
	movl	$2262369605, %eax
	movq	%rsi, %rdi
	imulq	%rax, %rsi
	movl	%edi, %edx
	movq	%rsi, %rax
	shrq	$39, %rsi
	shrq	$39, %rax
	imull	$243, %eax, %eax
	subl	%eax, %edx
	movslq	%edx, %rax
	cmpl	$19, %eax
	jle	.L12
	addl	$13, %eax
.L13:
	movl	%esi, %edx
	movl	$2262369605, %ecx
	imulq	%rcx, %rdx
	movq	%rdx, %rcx
	shrq	$39, %rdx
	shrq	$39, %rcx
	imull	$243, %ecx, %ecx
	subl	%ecx, %esi
	movslq	%esi, %rcx
	cmpl	$19, %ecx
	jle	.L14
	addl	$13, %ecx
	sall	$8, %ecx
.L15:
	orl	%eax, %ecx
	cmpl	$1180979, %edi
	jle	.L16
	leal	13(%rdx), %eax
	sall	$16, %eax
.L17:
	orl	%ecx, %eax
	orl	$-33554432, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	leal	-10513(%rdi), %esi
	movl	$2262369605, %eax
	movq	%rsi, %rdi
	imulq	%rax, %rsi
	movq	%rsi, %rax
	shrq	$39, %rsi
	shrq	$39, %rax
	imull	$243, %eax, %eax
	subl	%eax, %edi
	movslq	%edi, %rax
	leal	13(%rax), %edi
	cmpl	$19, %eax
	jg	.L9
	leaq	_ZL16bocu1TrailToByte(%rip), %rdx
	movsbl	(%rdx,%rax), %edi
.L9:
	movl	%esi, %edx
	movl	$2262369605, %eax
	imulq	%rax, %rdx
	movq	%rdx, %rax
	shrq	$39, %rax
	imull	$243, %eax, %ecx
	movq	%rax, %rdx
	subl	%ecx, %esi
	movslq	%esi, %rcx
	cmpl	$19, %ecx
	jle	.L10
	addl	$13, %ecx
	sall	$8, %ecx
.L11:
	leal	251(%rdx), %eax
	sall	$16, %eax
	orl	%ecx, %eax
	orl	%edi, %eax
	orl	$50331648, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	addl	$10513, %edi
	movl	$1, %r8d
	movslq	%edi, %rcx
	movl	%edi, %eax
	imulq	$-2032597691, %rcx, %rcx
	sarl	$31, %eax
	shrq	$32, %rcx
	addl	%edi, %ecx
	sarl	$7, %ecx
	movl	%ecx, %esi
	subl	%eax, %ecx
	subl	%eax, %esi
	imull	$243, %esi, %esi
	subl	%esi, %edi
	je	.L25
	leal	243(%rdi), %eax
	subl	$1, %ecx
	cmpl	$19, %eax
	jle	.L53
	leal	256(%rdi), %r8d
.L25:
	movslq	%ecx, %rdx
	movl	%ecx, %eax
	movl	$256, %r9d
	imulq	$-2032597691, %rdx, %rdx
	sarl	$31, %eax
	shrq	$32, %rdx
	addl	%ecx, %edx
	sarl	$7, %edx
	movl	%edx, %esi
	subl	%eax, %esi
	imull	$243, %esi, %edi
	movl	%esi, %edx
	movl	%ecx, %esi
	subl	%edi, %esi
	je	.L28
	leal	243(%rsi), %eax
	subl	$1, %edx
	cmpl	$19, %eax
	jle	.L54
	leal	256(%rsi), %eax
	sall	$8, %eax
	movl	%eax, %r9d
.L28:
	leal	37(%rdx), %eax
	sall	$16, %eax
	orl	%r8d, %eax
	orl	%r9d, %eax
	orl	$50331648, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movslq	%edx, %rdx
	leaq	_ZL16bocu1TrailToByte(%rip), %rax
	movsbl	(%rax,%rdx), %eax
	sall	$16, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	_ZL16bocu1TrailToByte(%rip), %rax
	movsbl	(%rax,%rcx), %ecx
	sall	$8, %ecx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L35:
	cltq
	leaq	_ZL16bocu1TrailToByte(%rip), %rcx
	movsbl	(%rcx,%rax), %eax
	sall	$16, %eax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	_ZL16bocu1TrailToByte(%rip), %rsi
	movsbl	(%rsi,%rcx), %ecx
	sall	$8, %ecx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	_ZL16bocu1TrailToByte(%rip), %rdx
	movsbl	(%rdx,%rax), %eax
	jmp	.L13
.L50:
	movslq	%edx, %rdx
	leaq	_ZL16bocu1TrailToByte(%rip), %rax
	movsbl	(%rax,%rdx), %edx
	jmp	.L31
.L48:
	cltq
	leaq	_ZL16bocu1TrailToByte(%rip), %rcx
	movsbl	(%rcx,%rax), %esi
	jmp	.L21
.L54:
	cltq
	leaq	_ZL16bocu1TrailToByte(%rip), %rcx
	movsbl	(%rcx,%rax), %eax
	sall	$8, %eax
	movl	%eax, %r9d
	jmp	.L28
.L53:
	cltq
	leaq	_ZL16bocu1TrailToByte(%rip), %rdx
	movsbl	(%rdx,%rax), %r8d
	jmp	.L25
.L51:
	cltq
	leaq	_ZL16bocu1TrailToByte(%rip), %rsi
	movsbl	(%rsi,%rax), %eax
	sall	$8, %eax
	movl	%eax, %r9d
	jmp	.L34
	.cfi_endproc
.LFE2029:
	.size	_ZL8packDiffi, .-_ZL8packDiffi
	.p2align 4
	.type	_ZL26_Bocu1ToUnicodeWithOffsetsP23UConverterToUnicodeArgsP10UErrorCode, @function
_ZL26_Bocu1ToUnicodeWithOffsetsP23UConverterToUnicodeArgsP10UErrorCode:
.LFB2034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	%rdi, -88(%rbp)
	movq	8(%rdi), %rsi
	movq	24(%rax), %rcx
	movq	40(%rax), %rdx
	movq	32(%rax), %r9
	movl	72(%rsi), %r8d
	movzbl	64(%rsi), %ebx
	movq	%rsi, -72(%rbp)
	movq	48(%rax), %r10
	movl	$64, %eax
	movq	16(%rdi), %rdi
	movq	%rcx, -48(%rbp)
	testl	%r8d, %r8d
	movq	%rdx, -64(%rbp)
	movb	%bl, -53(%rbp)
	cmove	%eax, %r8d
	testb	%bl, %bl
	je	.L104
	movl	76(%rsi), %eax
	movl	%eax, %ecx
	andl	$3, %ecx
	testb	%bl, %bl
	setg	%sil
	cmpq	%rdx, %r9
	setb	%dl
	testb	%dl, %sil
	je	.L105
	testl	%ecx, %ecx
	je	.L105
	sarl	$2, %eax
	xorl	%edx, %edx
	movl	$-1, %r14d
	movsbq	%bl, %r12
	movl	%eax, -52(%rbp)
	movl	%eax, %r15d
.L58:
	movq	-72(%rbp), %rax
	addl	$1, %edx
	movl	%r14d, -92(%rbp)
	leaq	_ZL16bocu1ByteToTrail(%rip), %r13
	movq	-48(%rbp), %r14
	subl	%edi, %edx
	leaq	65(%rax), %rsi
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L89:
	imull	$59049, %r12d, %r12d
	testl	%r12d, %r12d
	js	.L90
.L92:
	addl	%r12d, %r15d
	subl	$1, %ecx
	movsbq	%r11b, %r12
.L93:
	movl	%ecx, %eax
	cmpq	%r14, %rdi
	jnb	.L138
	leal	(%rdx,%rdi), %ebx
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	leal	1(%r12), %r11d
	movb	%al, (%rsi,%r12)
	leal	-13(%rax), %r12d
	cmpl	$32, %eax
	jg	.L87
	cltq
	movsbl	0(%r13,%rax), %r12d
.L87:
	cmpl	$1, %ecx
	je	.L88
	cmpl	$2, %ecx
	jne	.L89
	imull	$243, %r12d, %r12d
	testl	%r12d, %r12d
	jns	.L92
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%rdx, %rcx
	xorl	%edx, %edx
.L57:
	movq	-48(%rbp), %rsi
	movq	%rcx, %rax
	subq	%r9, %rax
	subq	%rdi, %rsi
	sarq	%rax
	cmpl	%eax, %esi
	movl	%esi, -52(%rbp)
	cmovle	%esi, %eax
	testl	%eax, %eax
	jle	.L106
	movq	%r9, %rsi
	movq	%r10, %rcx
	leal	(%rax,%rdx), %r13d
	movl	$64, %r15d
	leal	1(%rdx,%rax), %r14d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L60:
	cmpl	$32, %r11d
	jg	.L59
	movw	%r11w, (%rsi)
	leaq	2(%rsi), %r9
	movl	%r14d, %r11d
	leaq	4(%rcx), %r10
	cmovne	%r15d, %r8d
	movl	%ebx, (%rcx)
	subl	%eax, %r11d
	addq	$1, %rdi
	movq	%r9, %rsi
	movq	%r10, %rcx
	subl	$1, %eax
	je	.L140
.L65:
	movzbl	(%rdi), %r11d
	movl	%r13d, %ebx
	movq	%rcx, %r10
	movq	%rsi, %r9
	subl	%eax, %ebx
	leal	-80(%r11), %r12d
	cmpl	$127, %r12d
	ja	.L60
	leal	-144(%r8,%r11), %edx
	cmpl	$12287, %edx
	jg	.L59
	movw	%dx, (%rsi)
	leaq	2(%rsi), %r9
	movl	%r14d, %r11d
	leaq	4(%rcx), %r10
	andl	$-128, %edx
	movl	%ebx, (%rcx)
	subl	%eax, %r11d
	addq	$1, %rdi
	leal	64(%rdx), %r8d
	movq	%r9, %rsi
	movq	%r10, %rcx
	subl	$1, %eax
	jne	.L65
.L140:
	movl	%r11d, %ebx
.L59:
	cmpq	-48(%rbp), %rdi
	jnb	.L66
	cmpq	-64(%rbp), %r9
	jnb	.L97
	movl	$64, %r14d
.L67:
	movzbl	(%rdi), %ecx
	leaq	1(%rdi), %rsi
	leal	1(%rbx), %edx
	leal	-80(%rcx), %r11d
	movl	%ecx, %r13d
	cmpl	$127, %r11d
	jbe	.L141
	cmpl	$32, %ecx
	jle	.L142
	leal	-37(%rcx), %r12d
	cmpl	$213, %r12d
	ja	.L74
	cmpq	%rsi, -48(%rbp)
	ja	.L143
.L74:
	cmpl	$255, %ecx
	je	.L110
	movq	-72(%rbp), %rax
	movb	%r13b, 65(%rax)
	cmpl	$79, %ecx
	jle	.L82
	cmpl	$250, %ecx
	jg	.L83
	leal	-208(%rcx), %eax
	movl	$1, %ecx
	imull	$243, %eax, %eax
	leal	256(,%rax,4), %eax
	sarl	$2, %eax
	movl	%eax, -52(%rbp)
.L84:
	movb	$1, -53(%rbp)
	movl	-52(%rbp), %r15d
	movl	%ebx, %r14d
	movq	%rsi, %rdi
	movsbq	-53(%rbp), %r12
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L105:
	movq	-64(%rbp), %rcx
	xorl	%edx, %edx
	jmp	.L57
.L108:
	movq	-48(%rbp), %rdi
.L66:
	movq	-80(%rbp), %rcx
	cmpl	$12, (%rcx)
	je	.L117
.L100:
	movl	-52(%rbp), %edx
	sall	$2, %edx
	orl	%edx, %eax
.L99:
	movq	-72(%rbp), %rcx
	movl	%eax, 76(%rcx)
	movq	%rcx, %rax
	movl	%r8d, 72(%rcx)
	movzbl	-53(%rbp), %ecx
	movb	%cl, 64(%rax)
	movq	-88(%rbp), %rax
	movq	%rdi, 16(%rax)
	movq	%r9, 32(%rax)
	movq	%r10, 48(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	movl	%r15d, -52(%rbp)
	movl	-92(%rbp), %r14d
	testl	%r12d, %r12d
	jns	.L144
.L90:
	movq	-80(%rbp), %rax
	movl	$12, (%rax)
.L81:
	movb	%r11b, -53(%rbp)
	xorl	%eax, %eax
	movl	$64, %r8d
	jmp	.L99
.L138:
	movl	%r15d, -52(%rbp)
	movb	%r12b, -53(%rbp)
	jmp	.L66
.L144:
	addl	%r12d, -52(%rbp)
	movl	-52(%rbp), %eax
	leal	(%rax,%r8), %ecx
	cmpl	$1114111, %ecx
	ja	.L101
	movb	$0, -53(%rbp)
	xorl	%eax, %eax
.L80:
	leal	-12352(%rcx), %edx
	leaq	2(%r9), %rsi
	cmpl	$42851, %edx
	jbe	.L94
	movl	%ecx, %r8d
	andl	$-128, %r8d
	addl	$64, %r8d
	cmpl	$65535, %ecx
	jle	.L95
	movl	%ecx, %edx
	sarl	$10, %edx
	subw	$10304, %dx
	movw	%dx, (%r9)
	movl	%ecx, %edx
	andw	$1023, %dx
	orw	$-9216, %dx
	cmpq	%rsi, -64(%rbp)
	jbe	.L98
	movw	%dx, 2(%r9)
	addq	$8, %r10
	addq	$4, %r9
	movl	%r14d, -8(%r10)
	movl	%r14d, -4(%r10)
	jmp	.L59
.L101:
	movq	-80(%rbp), %rax
	xorl	%r11d, %r11d
	movl	$12, (%rax)
	jmp	.L81
.L106:
	movl	%edx, %ebx
	jmp	.L59
.L94:
	movl	$12400, %r8d
	cmpl	$12447, %ecx
	jle	.L95
	leal	-19968(%rcx), %edx
	movl	$30481, %r8d
	cmpl	$20901, %edx
	jbe	.L95
	movl	%ecx, %edx
	movl	$49617, %r8d
	andl	$-128, %edx
	addl	$64, %edx
	cmpl	$44032, %ecx
	cmovl	%edx, %r8d
.L95:
	movw	%cx, (%r9)
	addq	$4, %r10
	movq	%rsi, %r9
	movl	%r14d, -4(%r10)
	jmp	.L59
.L142:
	movw	%cx, (%r9)
	cmovne	%r14d, %r8d
	addq	$4, %r10
	addq	$2, %r9
	movl	%ebx, -4(%r10)
.L73:
	cmpq	%rsi, -48(%rbp)
	je	.L108
	cmpq	%r9, -64(%rbp)
	jbe	.L145
	movl	%edx, %ebx
	movq	%rsi, %rdi
	jmp	.L67
.L82:
	cmpl	$36, %ecx
	jle	.L85
	imull	$243, %r11d, %r11d
	movl	$1, %ecx
	leal	-256(,%r11,4), %eax
	sarl	$2, %eax
	movl	%eax, -52(%rbp)
	jmp	.L84
.L98:
	movq	-72(%rbp), %rcx
	movl	%r14d, (%r10)
	movq	%rsi, %r9
	addq	$4, %r10
	movw	%dx, 144(%rcx)
	movb	$1, 93(%rcx)
	movq	-80(%rbp), %rcx
	movl	$15, (%rcx)
	jmp	.L100
.L110:
	movl	$64, %r8d
	jmp	.L73
.L83:
	cmpl	$254, %ecx
	je	.L111
	leal	-251(%rcx), %eax
	movl	$2, %ecx
	imull	$59049, %eax, %eax
	leal	42052(,%rax,4), %eax
	sarl	$2, %eax
	movl	%eax, -52(%rbp)
	jmp	.L84
.L85:
	cmpl	$33, %ecx
	je	.L112
	imull	$59049, %r12d, %r12d
	movl	$2, %ecx
	leal	-42052(,%r12,4), %eax
	sarl	$2, %eax
	movl	%eax, -52(%rbp)
	jmp	.L84
.L111:
	movl	$187660, -52(%rbp)
	movl	$3, %ecx
	jmp	.L84
.L112:
	movl	$-14536567, -52(%rbp)
	movl	$3, %ecx
	jmp	.L84
.L141:
	leal	-144(%r8,%rcx), %ecx
	cmpl	$12287, %ecx
	jle	.L146
	movl	%ebx, %r14d
	movq	%rsi, %rdi
	movl	%edx, %ebx
	jmp	.L80
.L145:
	movq	%rsi, %rdi
.L97:
	movq	-80(%rbp), %rcx
	movl	$15, (%rcx)
	jmp	.L100
.L117:
	xorl	%eax, %eax
	movl	$64, %r8d
	jmp	.L99
.L143:
	cmpl	$143, %ecx
	jle	.L75
	leal	-208(%rcx), %edx
	imull	$243, %edx, %edx
	leal	64(%rdx), %ecx
	movl	%ecx, -52(%rbp)
.L76:
	movzbl	(%rsi), %edx
	addq	$2, %rdi
	cmpl	$32, %edx
	jg	.L77
	leaq	_ZL16bocu1ByteToTrail(%rip), %rcx
	movsbl	(%rcx,%rdx), %edx
	testl	%edx, %edx
	jns	.L79
.L78:
	movq	-72(%rbp), %rcx
	movl	$2, %r11d
	movb	%r13b, 65(%rcx)
	movzbl	(%rsi), %eax
	movb	%al, 66(%rcx)
	movq	-80(%rbp), %rax
	movl	$12, (%rax)
	jmp	.L81
.L146:
	movq	-88(%rbp), %rax
	movw	%cx, (%r9)
	andl	$-128, %ecx
	addq	$4, %r10
	leal	64(%rcx), %r8d
	movl	%ebx, -4(%r10)
	addq	$2, %r9
	movq	%rsi, %rdi
	movq	40(%rax), %rcx
	jmp	.L57
.L77:
	subl	$13, %edx
.L79:
	movl	-52(%rbp), %ecx
	addl	%r8d, %ecx
	addl	%edx, %ecx
	cmpl	$1114111, %ecx
	ja	.L78
	movl	%ebx, %r14d
	addl	$2, %ebx
	jmp	.L80
.L75:
	imull	$243, %r11d, %r11d
	leal	-64(%r11), %ecx
	movl	%ecx, -52(%rbp)
	jmp	.L76
	.cfi_endproc
.LFE2034:
	.size	_ZL26_Bocu1ToUnicodeWithOffsetsP23UConverterToUnicodeArgsP10UErrorCode, .-_ZL26_Bocu1ToUnicodeWithOffsetsP23UConverterToUnicodeArgsP10UErrorCode
	.p2align 4
	.type	_ZL17_Bocu1FromUnicodeP25UConverterFromUnicodeArgsP10UErrorCode, @function
_ZL17_Bocu1FromUnicodeP25UConverterFromUnicodeArgsP10UErrorCode:
.LFB2031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$64, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	32(%rdi), %r12
	movq	40(%rdi), %rax
	movq	16(%rdi), %r11
	movl	80(%r14), %ebx
	movl	84(%r14), %r10d
	subq	%r12, %rax
	movq	24(%rdi), %rcx
	testl	%ebx, %ebx
	movl	%eax, %edx
	cmove	%esi, %ebx
	testl	%r10d, %r10d
	je	.L151
	testl	%eax, %eax
	jle	.L151
.L149:
	endbr64
	cmpq	%rcx, %r11
	jnb	.L165
.L239:
	movzwl	(%r11), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L164
	sall	$10, %r10d
	addq	$2, %r11
	leal	-56613888(%rax,%r10), %r10d
	.p2align 4,,10
	.p2align 3
.L164:
	movl	%r10d, %edi
	leal	-12352(%r10), %esi
	subl	%ebx, %edi
	leal	64(%rdi), %eax
	cmpl	$42851, %esi
	jbe	.L167
	movl	%r10d, %ebx
	andl	$-128, %ebx
	addl	$64, %ebx
	cmpl	$127, %eax
	jbe	.L168
.L169:
	leal	10513(%rdi), %esi
	cmpl	$21025, %esi
	ja	.L171
	cmpl	$1, %edx
	jne	.L234
.L171:
	movl	%edx, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZL8packDiffi
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %edx
	cmpl	$67108863, %eax
	ja	.L235
	movl	%eax, %esi
	sarl	$24, %esi
	cmpl	%esi, %edx
	jl	.L181
	cmpl	$3, %esi
	jne	.L183
	movq	%r12, %rdi
	jmp	.L182
.L168:
	subl	$112, %edi
	subl	$1, %edx
	addq	$1, %r12
	movb	%dil, -1(%r12)
	cmpl	$12287, %r10d
	jg	.L157
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%rcx, %rax
	subq	%r11, %rax
	sarq	%rax
	cmpl	%eax, %edx
	cmovge	%eax, %edx
	testl	%edx, %edx
	jle	.L152
	subl	$1, %edx
	movq	%r12, %rax
	movl	$64, %edi
	leaq	1(%r12,%rdx), %rsi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	1(%rax), %r8
	movb	%r10b, (%rax)
	cmovne	%edi, %ebx
	addq	$2, %r11
	movq	%r8, %rax
	cmpq	%r8, %rsi
	je	.L236
.L156:
	movzwl	(%r11), %r10d
	movq	%rax, %r12
	movl	%r10d, %edx
	cmpl	$12287, %r10d
	jg	.L152
	cmpl	$32, %r10d
	jle	.L237
	subl	%ebx, %edx
	leal	64(%rdx), %r8d
	cmpl	$127, %r8d
	ja	.L152
	movl	%r10d, %ebx
	leaq	1(%rax), %r8
	subl	$112, %edx
	addq	$2, %r11
	andl	$-128, %ebx
	movb	%dl, (%rax)
	movq	%r8, %rax
	addl	$64, %ebx
	cmpq	%r8, %rsi
	jne	.L156
.L236:
	movq	%rsi, %r12
.L152:
	movl	40(%r13), %edx
	subl	%r12d, %edx
.L157:
	cmpq	%r11, %rcx
	jbe	.L166
.L177:
	movl	$64, %esi
	testl	%edx, %edx
	jle	.L159
.L158:
	movzwl	(%r11), %r10d
	addq	$2, %r11
	movl	%r10d, %eax
	cmpl	$32, %r10d
	jle	.L238
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L164
	cmpq	%rcx, %r11
	jnb	.L165
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L238:
	cmovne	%esi, %ebx
	addq	$1, %r12
	subl	$1, %edx
	movb	%r10b, -1(%r12)
	cmpq	%r11, %rcx
	jbe	.L233
	testl	%edx, %edx
	jne	.L158
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$15, (%r15)
.L166:
	movl	%r10d, %eax
	movl	$0, %edx
	negl	%eax
	testl	%r10d, %r10d
	cmovns	%edx, %eax
.L162:
	movl	%eax, 84(%r14)
	movl	%ebx, 80(%r14)
	movq	%r12, 32(%r13)
	movq	%r11, 16(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	negl	%r10d
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L167:
	movl	$12400, %ebx
	cmpl	$12447, %r10d
	jle	.L170
	leal	-19968(%r10), %esi
	movl	$30481, %ebx
	cmpl	$20901, %esi
	jbe	.L170
	movl	%r10d, %ebx
	movl	$49617, %esi
	andl	$-128, %ebx
	addl	$64, %ebx
	cmpl	$44032, %r10d
	cmovge	%esi, %ebx
.L170:
	cmpl	$127, %eax
	ja	.L169
	leaq	1(%r12), %rax
	subl	$112, %edi
	subl	$1, %edx
	movb	%dil, (%r12)
	movq	%rax, %r12
	cmpq	%rcx, %r11
	jb	.L177
.L233:
	xorl	%eax, %eax
	jmp	.L162
.L235:
	cmpl	$3, %edx
	jle	.L240
	movl	%eax, %esi
	leaq	1(%r12), %rdi
	sarl	$24, %esi
	movb	%sil, (%r12)
	movl	$4, %esi
.L182:
	movl	%eax, %r8d
	rolw	$8, %ax
	leaq	3(%rdi), %r12
	sarl	$16, %r8d
	movw	%ax, 1(%rdi)
	movb	%r8b, (%rdi)
.L183:
	subl	%esi, %edx
	jmp	.L157
.L234:
	testl	%edi, %edi
	js	.L172
	subl	$64, %edi
	movslq	%edi, %rax
	movl	%edi, %esi
	imulq	$-2032597691, %rax, %rax
	sarl	$31, %esi
	shrq	$32, %rax
	addl	%edi, %eax
	sarl	$7, %eax
	movl	%eax, %r8d
	subl	%esi, %eax
	subl	%esi, %r8d
	addl	$208, %eax
	imull	$243, %r8d, %r8d
	subl	%r8d, %edi
	movl	%edi, %r8d
.L173:
	movb	%al, (%r12)
	cmpl	$19, %r8d
	jle	.L175
	addl	$13, %r8d
.L176:
	movb	%r8b, 1(%r12)
	subl	$2, %edx
	addq	$2, %r12
	jmp	.L157
.L240:
	movl	$4, %ecx
	leaq	104(%r14), %rsi
	subl	%edx, %ecx
	movl	%ecx, %r9d
	cmpl	$2, %ecx
	je	.L204
	cmpl	$3, %ecx
	jne	.L193
	movl	%eax, %esi
	leaq	105(%r14), %rdi
	sarl	$16, %esi
	movb	%sil, 104(%r14)
.L184:
	movb	%ah, (%rdi)
	leaq	1(%rdi), %rsi
.L186:
	movb	%al, (%rsi)
.L187:
	leal	0(,%r9,8), %ecx
	movb	%r9b, 91(%r14)
	sarl	%cl, %eax
	cmpl	$2, %edx
	je	.L188
	cmpl	$3, %edx
	je	.L189
	cmpl	$1, %edx
	jne	.L159
	movq	%r12, %rdx
.L190:
	movb	%al, (%rdx)
	leaq	1(%rdx), %r12
	jmp	.L159
.L193:
	cmpl	$1, %r9d
	jne	.L187
	jmp	.L186
.L189:
	movl	%eax, %edx
	addq	$1, %r12
	sarl	$16, %edx
	movb	%dl, -1(%r12)
.L188:
	movzbl	%ah, %ecx
	leaq	1(%r12), %rdx
	movb	%cl, (%r12)
	jmp	.L190
.L181:
	movl	%esi, %ecx
	leaq	104(%r14), %rsi
	subl	%edx, %ecx
	movl	%ecx, %r9d
	cmpl	$2, %ecx
	jne	.L193
.L204:
	movq	%rsi, %rdi
	jmp	.L184
.L175:
	movslq	%r8d, %r8
	leaq	_ZL16bocu1TrailToByte(%rip), %rax
	movzbl	(%rax,%r8), %r8d
	jmp	.L176
.L172:
	movslq	%eax, %rsi
	movl	%eax, %edi
	imulq	$-2032597691, %rsi, %rsi
	sarl	$31, %edi
	shrq	$32, %rsi
	addl	%eax, %esi
	sarl	$7, %esi
	movl	%esi, %r8d
	subl	%edi, %esi
	subl	%edi, %r8d
	imull	$243, %r8d, %r8d
	subl	%r8d, %eax
	movl	%eax, %r8d
	jns	.L174
	subl	$1, %esi
	addl	$243, %r8d
.L174:
	leal	80(%rsi), %eax
	jmp	.L173
	.cfi_endproc
.LFE2031:
	.size	_ZL17_Bocu1FromUnicodeP25UConverterFromUnicodeArgsP10UErrorCode, .-_ZL17_Bocu1FromUnicodeP25UConverterFromUnicodeArgsP10UErrorCode
	.p2align 4
	.type	_ZL28_Bocu1FromUnicodeWithOffsetsP25UConverterFromUnicodeArgsP10UErrorCode, @function
_ZL28_Bocu1FromUnicodeWithOffsetsP25UConverterFromUnicodeArgsP10UErrorCode:
.LFB2030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$64, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	movq	24(%r14), %rax
	movq	%rsi, -80(%rbp)
	movq	32(%r14), %rbx
	movq	16(%r14), %r10
	movl	80(%rdi), %r12d
	movl	84(%rdi), %r11d
	movq	%rax, -56(%rbp)
	movq	40(%r14), %rax
	movq	48(%r14), %r13
	movq	%rdi, -64(%rbp)
	subq	%rbx, %rax
	testl	%r12d, %r12d
	cmove	%ecx, %r12d
	movl	%eax, %edx
	xorl	%esi, %esi
	testl	%r11d, %r11d
	je	.L243
	movl	$-1, %r15d
	testl	%eax, %eax
	jle	.L243
.L244:
	cmpq	-56(%rbp), %r10
	jnb	.L258
	movzwl	(%r10), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L257
	sall	$10, %r11d
	addq	$2, %r10
	addl	$1, %esi
	leal	-56613888(%rax,%r11), %r11d
	jmp	.L257
.L295:
	movq	%rcx, %r13
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L243:
	movq	-56(%rbp), %r9
	subq	%r10, %r9
	sarq	%r9
	cmpl	%r9d, %edx
	cmovl	%edx, %r9d
	testl	%r9d, %r9d
	jle	.L289
	movl	%esi, %r8d
	leal	1(%rsi), %edi
	movq	%rbx, %rax
	movq	%r14, -72(%rbp)
	subl	%ebx, %r8d
	subl	%ebx, %edi
	leal	-1(%r9,%rbx), %ebx
	movq	%r10, %rdx
	movq	%r13, %rcx
	movl	%ebx, %r14d
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L334:
	movl	$64, %ebx
	cmovne	%ebx, %r12d
	leaq	1(%rax), %rbx
.L333:
	movb	%dl, (%rax)
	leal	(%rdi,%rsi), %r9d
	leaq	4(%rcx), %r13
	movl	%r14d, %eax
	subl	%esi, %eax
	leaq	2(%r10), %rdx
	movl	%r15d, (%rcx)
	movq	%r13, %rcx
	movl	%eax, %esi
	movq	%rdx, %r10
	movq	%rbx, %rax
	testl	%esi, %esi
	je	.L290
.L246:
	movzwl	(%rdx), %r11d
	movq	%rdx, %r10
	movq	%rax, %rbx
	movq	%rcx, %r13
	movl	%eax, %esi
	leal	(%r8,%rax), %r15d
	movl	%r11d, %edx
	cmpl	$12287, %r11d
	jg	.L329
	cmpl	$32, %r11d
	jle	.L334
	subl	%r12d, %edx
	leal	64(%rdx), %r9d
	cmpl	$127, %r9d
	ja	.L329
	movl	%r11d, %r12d
	leaq	1(%rax), %rbx
	subl	$112, %edx
	andl	$-128, %r12d
	addl	$64, %r12d
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L329:
	movq	-72(%rbp), %r14
.L245:
	movl	40(%r14), %edx
	subl	%ebx, %edx
.L250:
	cmpq	-56(%rbp), %r10
	jnb	.L260
	movl	$64, %ecx
	testl	%edx, %edx
	jle	.L252
.L251:
	movzwl	(%r10), %r11d
	addq	$2, %r10
	leal	1(%r15), %esi
	movl	%r11d, %eax
	cmpl	$32, %r11d
	jle	.L335
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L244
.L257:
	movl	%r11d, %edi
	leal	-12352(%r11), %eax
	subl	%r12d, %edi
	cmpl	$42851, %eax
	ja	.L336
	movl	$12400, %r12d
	cmpl	$12447, %r11d
	jle	.L262
	leal	-19968(%r11), %eax
	movl	$30481, %r12d
	cmpl	$20901, %eax
	jbe	.L262
	movl	%r11d, %r12d
	movl	$49617, %eax
	andl	$-128, %r12d
	addl	$64, %r12d
	cmpl	$44032, %r11d
	cmovge	%eax, %r12d
.L262:
	leal	64(%rdi), %eax
	cmpl	$127, %eax
	ja	.L263
	subl	$112, %edi
	leaq	1(%rbx), %rax
	leaq	4(%r13), %rcx
	subl	$1, %edx
	movb	%dil, (%rbx)
	movl	%r15d, 0(%r13)
	cmpl	$12287, %r11d
	jle	.L295
	movl	%esi, %r15d
	movq	%rcx, %r13
	movq	%rax, %rbx
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L335:
	cmovne	%ecx, %r12d
	addq	$4, %r13
	addq	$1, %rbx
	subl	$1, %edx
	movb	%r11b, -1(%rbx)
	movl	%r15d, -4(%r13)
	cmpq	%r10, -56(%rbp)
	jbe	.L337
	testl	%edx, %edx
	je	.L252
	movl	%esi, %r15d
	jmp	.L251
.L339:
	movq	-64(%rbp), %rcx
	movl	$4, %edi
	subl	%edx, %edi
	leaq	104(%rcx), %r8
	cmpl	$2, %edi
	je	.L300
	cmpl	$3, %edi
	jne	.L286
	movq	%rcx, %r8
	leaq	105(%rcx), %rsi
	movl	%eax, %ecx
	sarl	$16, %ecx
	movb	%cl, 104(%r8)
.L278:
	movb	%ah, (%rsi)
	leaq	1(%rsi), %r8
.L280:
	movb	%al, (%r8)
.L281:
	movq	-64(%rbp), %rcx
	movb	%dil, 91(%rcx)
	leal	0(,%rdi,8), %ecx
	sarl	%cl, %eax
	cmpl	$2, %edx
	je	.L282
	cmpl	$3, %edx
	je	.L283
	cmpl	$1, %edx
	jne	.L252
	movq	%r13, %rdx
	movq	%rbx, %rcx
.L284:
	movb	%al, (%rcx)
	leaq	1(%rcx), %rbx
	leaq	4(%rdx), %r13
	movl	%r15d, (%rdx)
.L252:
	movq	-80(%rbp), %rax
	movl	$15, (%rax)
.L260:
	movl	%r11d, %eax
	movl	$0, %edx
	negl	%eax
	testl	%r11d, %r11d
	cmovns	%edx, %eax
.L255:
	movq	-64(%rbp), %rdi
	movl	%eax, 84(%rdi)
	movl	%r12d, 80(%rdi)
	movq	%r10, 16(%r14)
	movq	%rbx, 32(%r14)
	movq	%r13, 48(%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	movq	-72(%rbp), %r14
	movl	%r9d, %r15d
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L258:
	negl	%r11d
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L336:
	movl	%r11d, %r12d
	andl	$-128, %r12d
	addl	$64, %r12d
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L263:
	leal	10513(%rdi), %ecx
	cmpl	$21025, %ecx
	ja	.L264
	cmpl	$1, %edx
	jne	.L338
.L264:
	movl	%esi, -84(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZL8packDiffi
	movl	-72(%rbp), %edx
	movl	-84(%rbp), %esi
	cmpl	$67108863, %eax
	jbe	.L271
	cmpl	$3, %edx
	jle	.L339
	movl	%eax, %ecx
	addq	$4, %r13
	addq	$1, %rbx
	movl	$4, %edi
	sarl	$24, %ecx
	movb	%cl, -1(%rbx)
	movl	%r15d, -4(%r13)
.L276:
	movl	%eax, %ecx
	leaq	1(%rbx), %r8
	sarl	$16, %ecx
	movb	%cl, (%rbx)
	leaq	4(%r13), %rcx
	movl	%r15d, 0(%r13)
.L275:
	movzbl	%ah, %ebx
	leaq	8(%rcx), %r13
	movb	%bl, (%r8)
	leaq	2(%r8), %rbx
	movl	%r15d, (%rcx)
	movb	%al, 1(%r8)
	movl	%r15d, 4(%rcx)
.L277:
	subl	%edi, %edx
	movl	%esi, %r15d
	jmp	.L250
.L271:
	movl	%eax, %edi
	sarl	$24, %edi
	cmpl	%edi, %edx
	jl	.L274
	cmpl	$2, %edi
	je	.L296
	cmpl	$3, %edi
	je	.L276
	jmp	.L277
.L338:
	testl	%edi, %edi
	js	.L265
	subl	$64, %edi
	movslq	%edi, %rax
	movl	%edi, %r8d
	imulq	$-2032597691, %rax, %rax
	sarl	$31, %r8d
	shrq	$32, %rax
	addl	%edi, %eax
	sarl	$7, %eax
	movl	%eax, %ecx
	subl	%r8d, %eax
	subl	%r8d, %ecx
	imull	$243, %ecx, %ecx
	subl	%ecx, %edi
	movl	%edi, %ecx
	movl	%eax, %edi
	addl	$208, %edi
.L266:
	movb	%dil, (%rbx)
	cmpl	$19, %ecx
	jle	.L268
	addl	$13, %ecx
.L269:
	movb	%cl, 1(%rbx)
	subl	$2, %edx
	addq	$8, %r13
	addq	$2, %rbx
	movl	%r15d, -8(%r13)
	movl	%r15d, -4(%r13)
	movl	%esi, %r15d
	jmp	.L250
.L289:
	movl	%esi, %r15d
	jmp	.L245
.L286:
	cmpl	$1, %edi
	jne	.L281
	jmp	.L280
.L283:
	movl	%eax, %edx
	addq	$4, %r13
	addq	$1, %rbx
	sarl	$16, %edx
	movb	%dl, -1(%rbx)
	movl	%r15d, -4(%r13)
.L282:
	movb	%ah, (%rbx)
	leaq	1(%rbx), %rcx
	leaq	4(%r13), %rdx
	movl	%r15d, 0(%r13)
	jmp	.L284
.L274:
	movq	-64(%rbp), %rcx
	subl	%edx, %edi
	leaq	104(%rcx), %r8
	cmpl	$2, %edi
	jne	.L286
.L300:
	movq	%r8, %rsi
	jmp	.L278
.L337:
	xorl	%eax, %eax
	jmp	.L255
.L268:
	movslq	%ecx, %rcx
	leaq	_ZL16bocu1TrailToByte(%rip), %rax
	movzbl	(%rax,%rcx), %ecx
	jmp	.L269
.L265:
	movslq	%eax, %rdi
	movl	%eax, %r8d
	imulq	$-2032597691, %rdi, %rdi
	sarl	$31, %r8d
	shrq	$32, %rdi
	addl	%eax, %edi
	sarl	$7, %edi
	movl	%edi, %ecx
	subl	%r8d, %edi
	subl	%r8d, %ecx
	imull	$243, %ecx, %ecx
	subl	%ecx, %eax
	movl	%eax, %ecx
	jns	.L267
	subl	$1, %edi
	addl	$243, %ecx
.L267:
	addl	$80, %edi
	jmp	.L266
.L296:
	movq	%r13, %rcx
	movq	%rbx, %r8
	jmp	.L275
	.cfi_endproc
.LFE2030:
	.size	_ZL28_Bocu1FromUnicodeWithOffsetsP25UConverterFromUnicodeArgsP10UErrorCode, .-_ZL28_Bocu1FromUnicodeWithOffsetsP25UConverterFromUnicodeArgsP10UErrorCode
	.p2align 4
	.type	_ZL15_Bocu1ToUnicodeP23UConverterToUnicodeArgsP10UErrorCode, @function
_ZL15_Bocu1ToUnicodeP23UConverterToUnicodeArgsP10UErrorCode:
.LFB2035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$64, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	8(%rdi), %r14
	movq	40(%rdi), %rbx
	movq	32(%rdi), %r9
	movq	16(%rdi), %rsi
	movl	72(%r14), %ecx
	movl	76(%r14), %r13d
	movq	%rbx, -48(%rbp)
	movq	24(%rdi), %r15
	movsbq	64(%r14), %rdi
	testl	%ecx, %ecx
	movl	%r13d, %edx
	cmove	%eax, %ecx
	andl	$3, %edx
	testb	%dil, %dil
	setg	%r8b
	cmpq	%rbx, %r9
	setb	%al
	testb	%al, %r8b
	je	.L389
	sarl	$2, %r13d
	testl	%edx, %edx
	jne	.L343
.L389:
	movq	-48(%rbp), %r8
.L342:
	subq	%r9, %r8
	movq	%r15, %rax
	subq	%rsi, %rax
	sarq	%r8
	cmpl	%r8d, %eax
	movl	%eax, %r13d
	cmovle	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L344
	movq	%r9, %rdx
	movl	$64, %ebx
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L345:
	cmpl	$32, %r10d
	jg	.L344
	movw	%r10w, (%rdx)
	cmovne	%ebx, %ecx
	leaq	2(%rdx), %r9
.L348:
	addq	$1, %rsi
	movq	%r9, %rdx
	subl	$1, %r8d
	je	.L344
.L350:
	movzbl	(%rsi), %r10d
	movq	%rdx, %r9
	leal	-80(%r10), %r11d
	cmpl	$127, %r11d
	ja	.L345
	leal	-144(%rcx,%r10), %eax
	cmpl	$12287, %eax
	jg	.L344
	movw	%ax, (%rdx)
	andl	$-128, %eax
	leaq	2(%rdx), %r9
	leal	64(%rax), %ecx
	jmp	.L348
.L425:
	addl	%edi, %r13d
	leal	(%rcx,%r13), %edx
	cmpl	$1114111, %edx
	ja	.L387
	xorl	%edi, %edi
	xorl	%r8d, %r8d
.L365:
	leal	-12352(%rdx), %eax
	leaq	2(%r9), %r10
	cmpl	$42851, %eax
	jbe	.L379
	movl	%edx, %ecx
	andl	$-128, %ecx
	addl	$64, %ecx
	cmpl	$65535, %edx
	jle	.L380
	movl	%edx, %eax
	sarl	$10, %eax
	subw	$10304, %ax
	movw	%ax, (%r9)
	movl	%edx, %eax
	andw	$1023, %ax
	orw	$-9216, %ax
	cmpq	%r10, -48(%rbp)
	jbe	.L383
	movw	%ax, 2(%r9)
	addq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L344:
	cmpq	%r15, %rsi
	jnb	.L351
	cmpq	-48(%rbp), %r9
	jnb	.L382
.L352:
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %rax
	leal	-80(%rdx), %r10d
	movl	%edx, %r11d
	cmpl	$127, %r10d
	jbe	.L422
	cmpl	$32, %edx
	jle	.L423
	leal	-37(%rdx), %ebx
	cmpl	$213, %ebx
	ja	.L359
	cmpq	%rax, %r15
	ja	.L424
.L359:
	cmpl	$255, %edx
	je	.L393
	movb	%r11b, 65(%r14)
	cmpl	$79, %edx
	jle	.L367
	cmpl	$250, %edx
	jg	.L368
	subl	$208, %edx
	imull	$243, %edx, %edx
	leal	256(,%rdx,4), %r13d
	movl	$1, %edx
	sarl	$2, %r13d
.L369:
	movq	%rax, %rsi
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	65(%r14), %r10
	leaq	_ZL16bocu1ByteToTrail(%rip), %r11
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L374:
	imull	$59049, %edi, %edi
	testl	%edi, %edi
	js	.L375
.L377:
	addl	%edi, %r13d
	subl	$1, %edx
	movsbq	%r8b, %rdi
.L378:
	movl	%edx, %r8d
	cmpq	%r15, %rsi
	jnb	.L351
	movzbl	(%rsi), %eax
	addq	$1, %rsi
	leal	1(%rdi), %r8d
	movb	%al, (%r10,%rdi)
	leal	-13(%rax), %edi
	cmpl	$32, %eax
	jg	.L372
	cltq
	movsbl	(%r11,%rax), %edi
.L372:
	cmpl	$1, %edx
	je	.L373
	cmpl	$2, %edx
	jne	.L374
	imull	$243, %edi, %edi
	testl	%edi, %edi
	jns	.L377
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L373:
	testl	%edi, %edi
	jns	.L425
.L375:
	movq	-56(%rbp), %rax
	movl	$12, (%rax)
.L366:
	movl	%r8d, %edi
	movl	$64, %ecx
	xorl	%r8d, %r8d
.L384:
	movl	%r8d, 76(%r14)
	movl	%ecx, 72(%r14)
	movb	%dil, 64(%r14)
	movq	%rsi, 16(%r12)
	movq	%r9, 32(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L391:
	.cfi_restore_state
	movq	%r15, %rsi
.L351:
	movq	-56(%rbp), %rax
	cmpl	$12, (%rax)
	je	.L399
.L385:
	sall	$2, %r13d
	orl	%r13d, %r8d
	jmp	.L384
.L387:
	movq	-56(%rbp), %rax
	xorl	%r8d, %r8d
	movl	$12, (%rax)
	jmp	.L366
.L379:
	movl	$12400, %ecx
	cmpl	$12447, %edx
	jle	.L380
	leal	-19968(%rdx), %eax
	movl	$30481, %ecx
	cmpl	$20901, %eax
	jbe	.L380
	movl	%edx, %ecx
	movl	$49617, %eax
	andl	$-128, %ecx
	addl	$64, %ecx
	cmpl	$44032, %edx
	cmovge	%eax, %ecx
.L380:
	movw	%dx, (%r9)
	movq	%r10, %r9
	jmp	.L344
.L423:
	movl	$64, %ebx
	movw	%dx, (%r9)
	cmovne	%ebx, %ecx
	addq	$2, %r9
.L358:
	cmpq	%rax, %r15
	je	.L391
	movq	%rax, %rsi
	cmpq	%r9, -48(%rbp)
	ja	.L352
.L382:
	movq	-56(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L367:
	cmpl	$36, %edx
	jle	.L370
	imull	$243, %r10d, %r10d
	movl	$1, %edx
	leal	-256(,%r10,4), %r13d
	sarl	$2, %r13d
	jmp	.L369
.L383:
	movw	%ax, 144(%r14)
	movq	-56(%rbp), %rax
	movq	%r10, %r9
	movb	$1, 93(%r14)
	movl	$15, (%rax)
	jmp	.L385
.L370:
	cmpl	$33, %edx
	je	.L395
	imull	$59049, %ebx, %ebx
	movl	$2, %edx
	leal	-42052(,%rbx,4), %r13d
	sarl	$2, %r13d
	jmp	.L369
.L393:
	movl	$64, %ecx
	jmp	.L358
.L368:
	cmpl	$254, %edx
	je	.L394
	subl	$251, %edx
	imull	$59049, %edx, %edx
	leal	42052(,%rdx,4), %r13d
	movl	$2, %edx
	sarl	$2, %r13d
	jmp	.L369
.L395:
	movl	$-14536567, %r13d
	movl	$3, %edx
	jmp	.L369
.L394:
	movl	$187660, %r13d
	movl	$3, %edx
	jmp	.L369
.L422:
	leal	-144(%rcx,%rdx), %edx
	movq	%rax, %rsi
	cmpl	$12287, %edx
	jg	.L365
	movw	%dx, (%r9)
	andl	$-128, %edx
	movq	40(%r12), %r8
	addq	$2, %r9
	leal	64(%rdx), %ecx
	jmp	.L342
.L399:
	xorl	%r8d, %r8d
	movl	$64, %ecx
	jmp	.L384
.L424:
	cmpl	$143, %edx
	jle	.L360
	leal	-208(%rdx), %r13d
	imull	$243, %r13d, %r13d
	addl	$64, %r13d
.L361:
	movzbl	(%rax), %edx
	addq	$2, %rsi
	cmpl	$32, %edx
	jg	.L362
	leaq	_ZL16bocu1ByteToTrail(%rip), %r10
	movsbl	(%r10,%rdx), %edx
	testl	%edx, %edx
	jns	.L364
.L363:
	movb	%r11b, 65(%r14)
	movzbl	(%rax), %eax
	movl	$2, %r8d
	movb	%al, 66(%r14)
	movq	-56(%rbp), %rax
	movl	$12, (%rax)
	jmp	.L366
.L362:
	subl	$13, %edx
.L364:
	addl	%r13d, %ecx
	addl	%ecx, %edx
	cmpl	$1114111, %edx
	jbe	.L365
	jmp	.L363
.L360:
	imull	$243, %r10d, %r10d
	leal	-64(%r10), %r13d
	jmp	.L361
	.cfi_endproc
.LFE2035:
	.size	_ZL15_Bocu1ToUnicodeP23UConverterToUnicodeArgsP10UErrorCode, .-_ZL15_Bocu1ToUnicodeP23UConverterToUnicodeArgsP10UErrorCode
	.globl	_Bocu1Data_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_Bocu1Data_67, @object
	.size	_Bocu1Data_67, 296
_Bocu1Data_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL16_Bocu1StaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL10_Bocu1Impl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL16_Bocu1StaticData, @object
	.size	_ZL16_Bocu1StaticData, 100
_ZL16_Bocu1StaticData:
	.long	100
	.string	"BOCU-1"
	.zero	53
	.long	1214
	.byte	0
	.byte	28
	.byte	1
	.byte	4
	.string	"\032"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL10_Bocu1Impl, @object
	.size	_ZL10_Bocu1Impl, 144
_ZL10_Bocu1Impl:
	.long	28
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZL15_Bocu1ToUnicodeP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL26_Bocu1ToUnicodeWithOffsetsP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL17_Bocu1FromUnicodeP25UConverterFromUnicodeArgsP10UErrorCode
	.quad	_ZL28_Bocu1FromUnicodeWithOffsetsP25UConverterFromUnicodeArgsP10UErrorCode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.section	.rodata
	.align 16
	.type	_ZL16bocu1TrailToByte, @object
	.size	_ZL16bocu1TrailToByte, 20
_ZL16bocu1TrailToByte:
	.ascii	"\001\002\003\004\005\006\020\021\022\023\024\025\026\027\030"
	.ascii	"\031\034\035\036\037"
	.align 32
	.type	_ZL16bocu1ByteToTrail, @object
	.size	_ZL16bocu1ByteToTrail, 33
_ZL16bocu1ByteToTrail:
	.string	"\377"
	.ascii	"\001\002\003\004\005\377\377\377\377\377\377\377\377\377\006"
	.ascii	"\007\b\t\n\013\f\r\016\017\377\377\020\021\022\023\377"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
