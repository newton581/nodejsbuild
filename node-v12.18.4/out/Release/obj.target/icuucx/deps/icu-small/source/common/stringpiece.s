	.file	"stringpiece.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711StringPieceC2EPKc
	.type	_ZN6icu_6711StringPieceC2EPKc, @function
_ZN6icu_6711StringPieceC2EPKc:
.LFB2077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, (%rdi)
	testq	%rsi, %rsi
	je	.L2
	movq	%rsi, %rdi
	call	strlen@PLT
.L2:
	movl	%eax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2077:
	.size	_ZN6icu_6711StringPieceC2EPKc, .-_ZN6icu_6711StringPieceC2EPKc
	.globl	_ZN6icu_6711StringPieceC1EPKc
	.set	_ZN6icu_6711StringPieceC1EPKc,_ZN6icu_6711StringPieceC2EPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711StringPieceC2ERKS0_i
	.type	_ZN6icu_6711StringPieceC2ERKS0_i, @function
_ZN6icu_6711StringPieceC2ERKS0_i:
.LFB2080:
	.cfi_startproc
	endbr64
	movl	8(%rsi), %ecx
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L9
	cmpl	%ecx, %edx
	cmovg	%ecx, %edx
	movslq	%edx, %rax
	subl	%edx, %ecx
.L9:
	addq	(%rsi), %rax
	movl	%ecx, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE2080:
	.size	_ZN6icu_6711StringPieceC2ERKS0_i, .-_ZN6icu_6711StringPieceC2ERKS0_i
	.globl	_ZN6icu_6711StringPieceC1ERKS0_i
	.set	_ZN6icu_6711StringPieceC1ERKS0_i,_ZN6icu_6711StringPieceC2ERKS0_i
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711StringPieceC2ERKS0_ii
	.type	_ZN6icu_6711StringPieceC2ERKS0_ii, @function
_ZN6icu_6711StringPieceC2ERKS0_ii:
.LFB2083:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	js	.L14
	cmpl	%edx, 8(%rsi)
	cmovle	8(%rsi), %edx
	movslq	%edx, %r8
.L12:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	js	.L13
	movl	8(%rsi), %eax
	subl	%edx, %eax
	cmpl	%ecx, %eax
	cmovg	%ecx, %eax
.L13:
	movq	(%rsi), %rdx
	movl	%eax, 8(%rdi)
	addq	%r8, %rdx
	movq	%rdx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L12
	.cfi_endproc
.LFE2083:
	.size	_ZN6icu_6711StringPieceC2ERKS0_ii, .-_ZN6icu_6711StringPieceC2ERKS0_ii
	.globl	_ZN6icu_6711StringPieceC1ERKS0_ii
	.set	_ZN6icu_6711StringPieceC1ERKS0_ii,_ZN6icu_6711StringPieceC2ERKS0_ii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711StringPiece3setEPKc
	.type	_ZN6icu_6711StringPiece3setEPKc, @function
_ZN6icu_6711StringPiece3setEPKc:
.LFB2085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, (%rdi)
	testq	%rsi, %rsi
	je	.L17
	movq	%rsi, %rdi
	call	strlen@PLT
.L17:
	movl	%eax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2085:
	.size	_ZN6icu_6711StringPiece3setEPKc, .-_ZN6icu_6711StringPiece3setEPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711StringPiece4findES0_i
	.type	_ZN6icu_6711StringPiece4findES0_i, @function
_ZN6icu_6711StringPiece4findES0_i:
.LFB2086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 3, -32
	movl	8(%rdi), %ebx
	movl	%ebx, %eax
	orl	%edx, %eax
	je	.L22
	movl	%edx, %r9d
	cmpl	%ebx, %ecx
	jge	.L28
.L26:
	testl	%r9d, %r9d
	jle	.L29
	movslq	%ecx, %rcx
	movq	%rsi, %r10
	movq	(%rdi), %r11
	xorl	%r8d, %r8d
	subq	%rcx, %r10
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L25:
	addl	$1, %r8d
	leal	1(%rcx), %eax
	addq	$1, %rcx
	cmpl	%r8d, %r9d
	je	.L36
.L27:
	movzbl	(%r10,%rcx), %r14d
	movl	%ecx, %eax
	cmpb	%r14b, (%r11,%rcx)
	je	.L25
	subl	%r8d, %eax
	leal	1(%rax), %ecx
	cmpl	%ebx, %ecx
	jl	.L26
.L28:
	movl	$-1, %eax
.L22:
	popq	%rbx
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	popq	%rbx
	subl	%edx, %eax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	movl	%ecx, %eax
	jmp	.L22
	.cfi_endproc
.LFE2086:
	.size	_ZN6icu_6711StringPiece4findES0_i, .-_ZN6icu_6711StringPiece4findES0_i
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711StringPiece7compareES0_
	.type	_ZN6icu_6711StringPiece7compareES0_, @function
_ZN6icu_6711StringPiece7compareES0_:
.LFB2087:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L42
	testl	%edx, %edx
	je	.L46
	leal	-1(%rax), %ecx
	movq	(%rdi), %r8
	leal	-1(%rdx), %edi
	movl	$1, %eax
	addq	$2, %rdi
	addq	$1, %rcx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L47:
	jg	.L46
	movl	%eax, %r9d
	cmpq	%rax, %rcx
	je	.L38
	addq	$1, %rax
	cmpq	%rax, %rdi
	je	.L46
.L40:
	movzbl	-1(%rax,%rsi), %r10d
	cmpb	%r10b, -1(%r8,%rax)
	jge	.L47
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$1, %eax
	ret
.L42:
	xorl	%r9d, %r9d
.L38:
	xorl	%eax, %eax
	cmpl	%r9d, %edx
	setg	%al
	negl	%eax
	ret
	.cfi_endproc
.LFE2087:
	.size	_ZN6icu_6711StringPiece7compareES0_, .-_ZN6icu_6711StringPiece7compareES0_
	.p2align 4
	.globl	_ZN6icu_67eqERKNS_11StringPieceES2_
	.type	_ZN6icu_67eqERKNS_11StringPieceES2_, @function
_ZN6icu_67eqERKNS_11StringPieceES2_:
.LFB2088:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	cmpl	8(%rsi), %edx
	jne	.L55
	movl	$1, %eax
	testl	%edx, %edx
	je	.L55
	movq	(%rsi), %rsi
	subl	$1, %edx
	movq	(%rdi), %rdi
	xorl	%eax, %eax
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %ecx
	cmpb	%cl, (%rdi,%rdx)
	je	.L58
.L55:
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	memcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE2088:
	.size	_ZN6icu_67eqERKNS_11StringPieceES2_, .-_ZN6icu_67eqERKNS_11StringPieceES2_
	.globl	_ZN6icu_6711StringPiece4nposE
	.section	.rodata
	.align 4
	.type	_ZN6icu_6711StringPiece4nposE, @object
	.size	_ZN6icu_6711StringPiece4nposE, 4
_ZN6icu_6711StringPiece4nposE:
	.long	2147483647
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
