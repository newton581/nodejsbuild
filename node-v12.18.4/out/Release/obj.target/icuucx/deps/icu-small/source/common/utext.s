	.file	"utext.cpp"
	.text
	.p2align 4
	.type	utf8TextLength, @function
utf8TextLength:
.LFB2337:
	.cfi_startproc
	endbr64
	movslq	120(%rdi), %rax
	testl	%eax, %eax
	js	.L10
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	72(%rdi), %rdx
	movslq	124(%rdi), %rax
	addq	%rdx, %rax
	cmpb	$0, (%rax)
	je	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$1, %rax
	cmpb	$0, (%rax)
	jne	.L5
.L4:
	subq	%rdx, %rax
	cmpq	$2147483646, %rax
	jg	.L7
	movl	%eax, %edx
	cltq
.L6:
	andl	$-3, 8(%rdi)
	movl	%edx, 120(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$2147483647, %eax
	movl	$2147483647, %edx
	jmp	.L6
	.cfi_endproc
.LFE2337:
	.size	utf8TextLength, .-utf8TextLength
	.p2align 4
	.type	utf8TextMapOffsetToNative, @function
utf8TextMapOffsetToNative:
.LFB2341:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rdx
	movl	8(%rdx), %eax
	addl	40(%rdi), %eax
	cltq
	movzbl	96(%rdx,%rax), %eax
	addl	20(%rdx), %eax
	cltq
	ret
	.cfi_endproc
.LFE2341:
	.size	utf8TextMapOffsetToNative, .-utf8TextMapOffsetToNative
	.p2align 4
	.type	utf8TextMapIndexToUTF16, @function
utf8TextMapIndexToUTF16:
.LFB2342:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rdx
	subl	20(%rdx), %esi
	movslq	%esi, %rsi
	movzbl	132(%rdx,%rsi), %eax
	subl	8(%rdx), %eax
	ret
	.cfi_endproc
.LFE2342:
	.size	utf8TextMapIndexToUTF16, .-utf8TextMapIndexToUTF16
	.p2align 4
	.type	repTextLength, @function
repTextLength:
.LFB2348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	72(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*64(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	cltq
	ret
	.cfi_endproc
.LFE2348:
	.size	repTextLength, .-repTextLength
	.p2align 4
	.type	unistrTextLength, @function
unistrTextLength:
.LFB2356:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdx
	movswl	8(%rdx), %eax
	testw	%ax, %ax
	js	.L16
	sarl	$5, %eax
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	12(%rdx), %eax
	cltq
	ret
	.cfi_endproc
.LFE2356:
	.size	unistrTextLength, .-unistrTextLength
	.p2align 4
	.type	unistrTextAccess, @function
unistrTextAccess:
.LFB2357:
	.cfi_startproc
	endbr64
	movslq	44(%rdi), %rcx
	testq	%rsi, %rsi
	js	.L23
	cmpq	%rsi, %rcx
	cmovle	%rcx, %rsi
	movl	%esi, 40(%rdi)
	testb	%dl, %dl
	je	.L21
	cmpq	%rsi, %rcx
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	testq	%rsi, %rsi
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	testb	%dl, %dl
	movl	$0, 40(%rdi)
	setne	%al
	testq	%rcx, %rcx
	setg	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE2357:
	.size	unistrTextAccess, .-unistrTextAccess
	.p2align 4
	.type	ucstrTextLength, @function
ucstrTextLength:
.LFB2365:
	.cfi_startproc
	endbr64
	movq	112(%rdi), %rax
	testq	%rax, %rax
	js	.L29
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	72(%rdi), %rcx
	movq	16(%rdi), %rax
	cmpw	$0, (%rcx,%rax,2)
	je	.L26
	leaq	1(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rdx, %rax
	leaq	1(%rdx), %rdx
	cmpw	$0, (%rcx,%rax,2)
	jne	.L27
	movq	%rax, 16(%rdi)
.L26:
	andl	$-3, 8(%rdi)
	movq	%rax, 112(%rdi)
	movl	%eax, 44(%rdi)
	movl	%eax, 28(%rdi)
	ret
	.cfi_endproc
.LFE2365:
	.size	ucstrTextLength, .-ucstrTextLength
	.p2align 4
	.type	ucstrTextAccess, @function
ucstrTextAccess:
.LFB2366:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	js	.L46
	movq	16(%rdi), %rax
	movq	72(%rdi), %r8
	cmpq	%rsi, %rax
	jle	.L32
	movzwl	(%r8,%rsi,2), %eax
	leaq	(%rsi,%rsi), %r9
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L33
.L59:
	movl	%esi, %ecx
.L31:
	movl	%ecx, 40(%rdi)
	testb	%dl, %dl
	je	.L44
.L60:
	cmpq	%rsi, 16(%rdi)
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	112(%rdi), %r9
	testq	%r9, %r9
	js	.L35
	movl	%r9d, %ecx
	movq	%r9, %rsi
	movl	%ecx, 40(%rdi)
	testb	%dl, %dl
	jne	.L60
.L44:
	testq	%rsi, %rsi
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	32(%rsi), %rcx
	leal	32(%rsi), %r9d
	movl	%esi, %r10d
	cmpq	$2147483647, %rcx
	movl	$2147483647, %ecx
	cmova	%ecx, %r9d
	movl	%eax, %ecx
	cmpl	%eax, %r9d
	jle	.L37
	cltq
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L38:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %r9d
	jle	.L37
.L40:
	cmpw	$0, (%r8,%rax,2)
	movl	%eax, %ecx
	jne	.L38
	movslq	%eax, %r9
	movl	%eax, 44(%rdi)
	movq	%r9, 112(%rdi)
	movl	%eax, 28(%rdi)
	cmpq	%rsi, %r9
	jle	.L49
	movzwl	(%r8,%rsi,2), %eax
	leaq	(%rsi,%rsi), %r11
	movl	%r10d, %ecx
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L61
.L39:
	andl	$-3, 8(%rdi)
	movq	%r9, 16(%rdi)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	je	.L31
	movzwl	-2(%r8,%r9), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L59
	subq	$1, %rsi
	movl	%esi, %ecx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L37:
	movzwl	(%r8,%rsi,2), %eax
	leaq	(%rsi,%rsi), %r9
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L62
.L41:
	cmpl	$2147483647, %ecx
	je	.L63
	movslq	%ecx, %r9
	movzwl	-2(%r8,%r9,2), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L43
	subl	$1, %ecx
	movslq	%ecx, %r9
.L43:
	movl	%ecx, 28(%rdi)
	movl	%ecx, 44(%rdi)
	movl	%esi, %ecx
	movq	%r9, 16(%rdi)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r9, %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L63:
	cmpq	$2147483647, %rsi
	movl	$2147483647, %eax
	movq	$2147483647, 112(%rdi)
	cmovg	%rax, %rsi
	movl	$2147483647, 44(%rdi)
	andl	$-3, 8(%rdi)
	movl	$2147483647, 28(%rdi)
	movq	$2147483647, 16(%rdi)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L62:
	testq	%rsi, %rsi
	je	.L41
	movzwl	-2(%r8,%r9), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	sete	%al
	movzbl	%al, %eax
	subq	%rax, %rsi
	jmp	.L41
.L61:
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	je	.L39
	movzwl	-2(%r8,%r11), %eax
	movl	%r10d, %ecx
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L39
	subq	$1, %rsi
	movl	%esi, %ecx
	jmp	.L39
	.cfi_endproc
.LFE2366:
	.size	ucstrTextAccess, .-ucstrTextAccess
	.p2align 4
	.type	charIterTextClose, @function
charIterTextClose:
.LFB2369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	96(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L65
	movq	(%rdi), %rax
	call	*8(%rax)
.L65:
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2369:
	.size	charIterTextClose, .-charIterTextClose
	.p2align 4
	.type	charIterTextLength, @function
charIterTextLength:
.LFB2370:
	.cfi_startproc
	endbr64
	movslq	112(%rdi), %rax
	ret
	.cfi_endproc
.LFE2370:
	.size	charIterTextLength, .-charIterTextLength
	.p2align 4
	.type	charIterTextAccess, @function
charIterTextAccess:
.LFB2371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%edx, -56(%rbp)
	testl	%esi, %esi
	js	.L72
	movq	112(%r12), %rax
	movl	%esi, %edi
	movslq	%esi, %rsi
	movl	%edx, %ecx
	cmpq	%rax, %rsi
	cmovge	%eax, %edi
	testl	%edi, %edi
	setg	%dl
	testb	%cl, %cl
	jne	.L106
	testb	%dl, %dl
	je	.L74
	leal	-1(%rdi), %ebx
	andl	$-16, %ebx
	subl	%ebx, %edi
	movslq	%ebx, %rax
	movl	%edi, -52(%rbp)
	movq	%rax, -72(%rbp)
.L75:
	movq	-72(%rbp), %rax
	cmpq	32(%r12), %rax
	je	.L108
.L90:
	cmpl	%ebx, 120(%r12)
	je	.L109
	cmpl	%ebx, 124(%r12)
	je	.L110
	movq	80(%r12), %r13
	cmpq	%r13, 48(%r12)
	je	.L111
.L82:
	movq	72(%r12), %r14
	movl	%ebx, %esi
	movl	$1, %r15d
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*120(%rax)
	movslq	%ebx, %rax
	subq	$1, %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*40(%rax)
	movq	-64(%rbp), %rcx
	movw	%ax, -2(%r13,%r15,2)
	movq	112(%r12), %rax
	leaq	(%rcx,%r15), %rsi
	cmpq	%rsi, %rax
	jl	.L80
	addq	$1, %r15
	cmpq	$17, %r15
	jne	.L83
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L108:
	movl	-52(%rbp), %eax
	movl	%eax, 40(%r12)
.L86:
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	setg	%al
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	80(%r12), %r13
	movq	112(%r12), %rax
.L80:
	movq	-72(%rbp), %rdx
	movq	%r13, 48(%r12)
	movl	$16, 44(%r12)
	movq	%rdx, 32(%r12)
	leal	16(%rbx), %edx
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	jg	.L84
	movq	%rdx, 16(%r12)
	movl	$16, %eax
.L85:
	movl	-52(%rbp), %ebx
	cmpb	$0, -56(%rbp)
	movl	%eax, 28(%r12)
	movl	%ebx, 40(%r12)
	je	.L86
.L88:
	cmpl	-52(%rbp), %eax
	setg	%al
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	cmpb	$0, -56(%rbp)
	movq	$0, -72(%rbp)
	movl	$0, %ebx
	movl	$0, -52(%rbp)
	je	.L75
.L78:
	movq	-72(%rbp), %rax
	cmpq	32(%r12), %rax
	jne	.L90
	movl	-52(%rbp), %eax
	movl	%eax, 40(%r12)
	movl	44(%r12), %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%rax, 16(%r12)
	subl	%ebx, %eax
	movl	%eax, 44(%r12)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L110:
	movq	88(%r12), %r13
	movq	112(%r12), %rax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L111:
	movq	88(%r12), %r13
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L106:
	movslq	%edi, %rsi
	cmpq	%rax, %rsi
	jne	.L77
	testb	%dl, %dl
	je	.L77
	leal	-1(%rdi), %ebx
	andl	$-16, %ebx
	subl	%ebx, %edi
	movslq	%ebx, %rax
	movl	%edi, -52(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L74:
	cmpb	$0, -56(%rbp)
	jne	.L106
	movl	%edi, %eax
	movl	%edi, %ebx
	sarl	$31, %eax
	shrl	$28, %eax
	leal	(%rdi,%rax), %r15d
	andl	$15, %r15d
	subl	%eax, %r15d
	subl	%r15d, %ebx
	movl	%r15d, -52(%rbp)
	movslq	%ebx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L75
.L77:
	movl	%edi, %eax
	movl	$16, %ecx
	movl	%edi, %ebx
	cltd
	idivl	%ecx
	subl	%edx, %ebx
	movl	%edx, -52(%rbp)
	movslq	%ebx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L78
	.cfi_endproc
.LFE2371:
	.size	charIterTextAccess, .-charIterTextAccess
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	" 000000000000\02000"
	.section	.rodata
.LC1:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.text
	.p2align 4
	.type	utf8TextAccess, @function
utf8TextAccess:
.LFB2338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r11d, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movl	120(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	72(%rdi), %rax
	movq	%rax, -72(%rbp)
	testq	%rsi, %rsi
	js	.L113
	cmpq	$2147483647, %rsi
	movl	$2147483647, %eax
	cmovle	%esi, %eax
	movl	%eax, %r11d
.L113:
	cmpl	%ecx, %r11d
	jle	.L114
	testl	%ecx, %ecx
	js	.L207
	movl	%ecx, %r11d
.L114:
	movslq	%r11d, %rax
	testb	%dl, %dl
	je	.L118
	movq	16(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L208
	movq	88(%rbx), %r12
	cmpl	%r11d, (%r12)
	jg	.L199
	cmpl	4(%r12), %r11d
	jge	.L199
.L123:
	movslq	8(%r12), %rdx
	movq	80(%rbx), %rax
	movq	%r12, 80(%rbx)
	movl	12(%r12), %edi
	movq	%rax, 88(%rbx)
	movq	%rdx, %rax
	leaq	24(%r12,%rdx,2), %rdx
	subl	%eax, %edi
	movq	%rdx, 48(%rbx)
	movl	%edi, 44(%rbx)
	movslq	(%r12), %rax
	movq	%rax, 32(%rbx)
	movslq	4(%r12), %rax
	movq	%rax, 16(%rbx)
	movl	16(%r12), %eax
	movl	%eax, 28(%rbx)
	movl	%r11d, %eax
	subl	20(%r12), %eax
	cltq
	movzbl	132(%r12,%rax), %eax
	subl	8(%r12), %eax
	movl	%eax, 40(%rbx)
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L112:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L209
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	32(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L210
	movq	88(%rbx), %r12
	movl	(%r12), %esi
	cmpl	%r11d, %esi
	jge	.L137
	cmpl	4(%r12), %r11d
	jle	.L123
.L137:
	testl	%r11d, %r11d
	jne	.L136
	testl	%esi, %esi
	je	.L127
.L128:
	movl	%r11d, (%r12)
	movl	%r11d, 4(%r12)
	movq	$0, 8(%r12)
	movl	$0, 16(%r12)
	movl	%r11d, 20(%r12)
	movb	$0, 96(%r12)
	movb	$0, 132(%r12)
	movq	88(%rbx), %r12
.L127:
	movslq	8(%r12), %rdx
	movq	80(%rbx), %rax
	movq	%r12, 80(%rbx)
	movl	12(%r12), %edi
	movq	%rax, 88(%rbx)
	movq	%rdx, %rax
	leaq	24(%r12,%rdx,2), %rdx
	subl	%eax, %edi
	movq	%rdx, 48(%rbx)
	movl	%edi, 44(%rbx)
	movslq	(%r12), %rdx
	movq	%rdx, 32(%rbx)
	movslq	4(%r12), %rdx
	movq	%rdx, 16(%rbx)
	movl	16(%r12), %edx
	movl	%edx, 28(%rbx)
	cmpl	%r11d, 4(%r12)
	je	.L211
.L141:
	movl	$0, 40(%rbx)
	xorl	%eax, %eax
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L207:
	movslq	124(%rbx), %rax
	cmpl	%r11d, %eax
	jg	.L114
	je	.L177
	movq	-72(%rbp), %rdi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L212:
	leal	1(%rax), %esi
	addq	$1, %rax
	movl	%esi, 124(%rbx)
	cmpl	%eax, %r11d
	jle	.L115
.L117:
	cmpb	$0, (%rdi,%rax)
	movl	%eax, %esi
	jne	.L212
.L116:
	andl	$-3, 8(%rbx)
	movl	%esi, %r11d
	movl	%esi, %ecx
	movl	%esi, 120(%rbx)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L199:
	cmpl	%r11d, %ecx
	je	.L213
.L124:
	cmpq	%rax, 32(%rbx)
	jg	.L122
	cmpq	%rax, %rdx
	jle	.L122
	movq	80(%rbx), %rdx
	movl	%r11d, %eax
	subl	20(%rdx), %eax
	cltq
	movzbl	132(%rdx,%rax), %eax
	subl	8(%rdx), %eax
	movl	%eax, 40(%rbx)
	movl	$1, %eax
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L208:
	cmpl	%r11d, %ecx
	je	.L214
	movq	88(%rbx), %r12
	cmpl	%r11d, (%r12)
	jg	.L122
	cmpl	%r11d, 4(%r12)
	jle	.L124
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L210:
	testl	%r11d, %r11d
	je	.L141
	movq	88(%rbx), %r12
	cmpl	%r11d, (%r12)
	jge	.L135
	cmpl	%r11d, 4(%r12)
	jge	.L123
.L136:
	cmpq	%rax, %rdx
	jge	.L135
	cmpq	%rax, 16(%rbx)
	jl	.L135
	movq	80(%rbx), %rdx
	movl	%r11d, %eax
	subl	20(%rdx), %eax
	cltq
	movzbl	132(%rdx,%rax), %eax
	subl	8(%rdx), %eax
	movl	%eax, 40(%rbx)
	setne	%al
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L213:
	cmpl	%r11d, 4(%r12)
	jne	.L128
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L122:
	movq	-72(%rbp), %rdi
	cmpb	$-64, (%rdi,%rax)
	jl	.L215
.L132:
	movq	80(%rbx), %rax
	movb	$0, -104(%rbp)
	movq	%r12, 80(%rbx)
	movq	%rax, 88(%rbx)
	testl	%ecx, %ecx
	jns	.L142
	movb	$1, -104(%rbp)
	movl	$2147483647, %ecx
.L142:
	leaq	96(%r12), %rax
	leaq	24(%r12), %rdi
	xorl	%r13d, %r13d
	movslq	%r11d, %rdx
	movq	%rax, -80(%rbp)
	leaq	132(%r12), %rax
	xorl	%esi, %esi
	movq	%rax, -88(%rbp)
	movl	%r11d, %eax
	movq	%r12, -120(%rbp)
	movl	%ecx, %r12d
	movq	%rbx, -136(%rbp)
	movl	%r13d, %ebx
	movq	%rdi, %r13
	movq	%rdi, -128(%rbp)
	movb	%r11b, -96(%rbp)
	movq	%rdx, -112(%rbp)
	movl	%r11d, -100(%rbp)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L216:
	movl	%eax, %edx
	movq	-88(%rbp), %rcx
	subl	-100(%rbp), %eax
	movw	%r14w, 0(%r13,%r8,2)
	cltq
	subb	-96(%rbp), %dl
	movb	%dl, (%r10)
	movb	%sil, (%rcx,%rax)
	addl	$1, %esi
	movl	%r15d, %eax
.L144:
	cmpl	$31, %esi
	jg	.L191
	cmpl	%r15d, %r12d
	jle	.L191
.L157:
	movq	-72(%rbp), %rcx
	movslq	%eax, %rdi
	movq	-80(%rbp), %r10
	movslq	%esi, %r8
	movl	%esi, %r11d
	leal	1(%rax), %r15d
	addq	%rdi, %rcx
	addq	%r8, %r10
	movzbl	(%rcx), %r14d
	leal	-1(%r14), %r9d
	movl	%r14d, %edx
	cmpl	$126, %r9d
	jbe	.L216
	testb	%bl, %bl
	jne	.L145
	movq	-120(%rbp), %rbx
	movl	%esi, 16(%rbx)
	movzbl	(%rcx), %r14d
	movl	%r14d, %edx
.L145:
	testb	%dl, %dl
	js	.L217
	testl	%r14d, %r14d
	jne	.L205
	movl	%eax, %r9d
.L171:
	cmpb	$0, -104(%rbp)
	jne	.L218
	xorl	%edx, %edx
	xorl	%r14d, %r14d
.L147:
	movw	%dx, 0(%r13,%r8,2)
	movl	%eax, %edx
	subb	-96(%rbp), %dl
	leal	1(%rsi), %ebx
	movb	%dl, (%r10)
.L156:
	movl	%eax, %edx
	notl	%edx
	addl	%r15d, %edx
	addq	$1, %rdx
	cmpl	%eax, %r15d
	movl	$1, %eax
	cmovle	%rax, %rdx
	subq	-112(%rbp), %rdi
	addq	-88(%rbp), %rdi
	call	memset@PLT
	movl	%ebx, %esi
	movl	%r15d, %eax
	movl	$1, %ebx
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L217:
	cmpl	%r15d, %r12d
	je	.L187
	cmpl	$223, %r14d
	jle	.L148
	cmpl	$239, %r14d
	jg	.L149
	movq	-72(%rbp), %rbx
	movslq	%r15d, %rcx
	movl	%edx, %r14d
	andl	$15, %edx
	andl	$15, %r14d
	movzbl	(%rbx,%rcx), %ecx
	leaq	.LC0(%rip), %rbx
	movsbl	(%rbx,%rdx), %r9d
	movl	%ecx, %edx
	shrb	$5, %dl
	btl	%edx, %r9d
	jnc	.L187
	andl	$63, %ecx
	movl	%ecx, %r9d
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L148:
	cmpl	$193, %r14d
	jle	.L187
	movl	%edx, %r14d
	movl	%r15d, %r9d
	andl	$31, %r14d
.L151:
	movq	-72(%rbp), %rcx
	movslq	%r15d, %rdx
	movzbl	(%rcx,%rdx), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	jbe	.L219
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$-3, %edx
	movl	$65533, %r14d
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L191:
	movq	-80(%rbp), %rdi
	movl	-100(%rbp), %r11d
	movslq	%esi, %rdx
	movl	%r15d, %ecx
	subb	-96(%rbp), %cl
	movq	-120(%rbp), %r12
	movl	%ebx, %r13d
	movb	%cl, (%rdi,%rdx)
	movl	%r15d, %edx
	movq	-88(%rbp), %rdi
	subl	%r11d, %edx
	movq	-136(%rbp), %rbx
	movslq	%edx, %rdx
	movb	%sil, (%rdi,%rdx)
	movl	%r11d, (%r12)
	movl	%r15d, 4(%r12)
	movl	$0, 8(%r12)
	movl	%esi, 12(%r12)
	testb	%r13b, %r13b
	jne	.L159
	movl	%esi, 16(%r12)
.L159:
	movq	-128(%rbp), %rcx
	cmpb	$0, -104(%rbp)
	movl	%r11d, 20(%r12)
	movl	$0, 40(%rbx)
	movl	12(%r12), %edx
	movq	%rcx, 48(%rbx)
	movl	%edx, 44(%rbx)
	movslq	(%r12), %rdx
	movq	%rdx, 32(%rbx)
	movslq	4(%r12), %rdx
	movq	%rdx, 16(%rbx)
	movl	16(%r12), %edx
	movl	%edx, 28(%rbx)
	jne	.L160
.L206:
	movl	$1, %eax
	jmp	.L112
.L219:
	sall	$6, %r14d
	movzbl	%cl, %ecx
	addl	$1, %r15d
	orl	%ecx, %r14d
	je	.L171
	leaq	(%r8,%r8), %rcx
	movl	%eax, %edx
	subb	-96(%rbp), %dl
	leaq	0(%r13,%rcx), %r9
	cmpl	$65535, %r14d
	jle	.L205
	movl	%r14d, %r8d
	leal	2(%rsi), %ebx
	sarl	$10, %r8d
	subw	$10304, %r8w
	movw	%r8w, (%r9)
	movl	%r14d, %r8d
	andw	$1023, %r8w
	orw	$-9216, %r8w
	movw	%r8w, 2(%r13,%rcx)
	movzbl	%dl, %ecx
	sall	$8, %edx
	addl	%ecx, %edx
	movw	%dx, (%r10)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L135:
	cmpl	%r11d, %ecx
	je	.L140
	movq	-72(%rbp), %rdi
	cmpb	$-64, (%rdi,%rax)
	jl	.L220
.L140:
	movq	80(%rbx), %rax
	leal	-101(%r11), %r9d
	movq	%r12, %rdi
	leaq	-60(%rbp), %rdx
	movslq	%r9d, %rcx
	movq	%r12, 80(%rbx)
	movl	$34, %esi
	movl	$34, %r15d
	movq	%rax, 88(%rbx)
	leaq	24(%r12), %rax
	subq	%rcx, %rdi
	movq	%rax, -80(%rbp)
	leaq	96(%r12), %rax
	movq	%rax, -88(%rbp)
	leaq	132(%r12), %rax
	movl	%r11d, -60(%rbp)
	movq	%rax, -96(%rbp)
	movb	$34, 233(%r12)
	movl	-60(%rbp), %eax
	movq	%rdi, -120(%rbp)
	movq	-72(%rbp), %rdi
	movl	%r9d, -136(%rbp)
	movb	%r9b, -100(%rbp)
	movb	$101, 130(%r12)
	movl	%r11d, -140(%rbp)
	movq	%r12, -152(%rbp)
	movq	%rbx, -160(%rbp)
	movq	%rdx, -128(%rbp)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L221:
	movw	%cx, (%r14)
	movl	%r13d, %r15d
	movb	%r13b, (%rax)
	movzbl	-60(%rbp), %eax
	subb	-100(%rbp), %al
	movb	%al, (%r12)
	movl	-60(%rbp), %eax
.L167:
	cmpl	$2, %r15d
	jle	.L201
.L162:
	movl	%eax, %edx
	subl	%r9d, %edx
	cmpl	$5, %edx
	jle	.L192
	testl	%eax, %eax
	jle	.L192
	subl	$1, %eax
	leal	-1(%r15), %r13d
	movq	-80(%rbp), %r8
	movslq	%eax, %rbx
	movl	%eax, -60(%rbp)
	movslq	%r13d, %r12
	subl	%r9d, %eax
	movzbl	(%rdi,%rbx), %ecx
	leaq	(%r12,%r12), %r10
	cltq
	addq	-88(%rbp), %r12
	addq	-96(%rbp), %rax
	leaq	(%r8,%r10), %r14
	cmpl	$127, %ecx
	jle	.L221
	movq	-128(%rbp), %rdx
	xorl	%esi, %esi
	movl	$-3, %r8d
	movl	%r9d, -104(%rbp)
	movq	%r10, -112(%rbp)
	movq	%rdi, -72(%rbp)
	call	utf8_prevCharSafeBody_67@PLT
	movq	-72(%rbp), %rdi
	movq	-112(%rbp), %r10
	cmpl	$65535, %eax
	movl	-104(%rbp), %r9d
	jg	.L168
	movw	%ax, (%r14)
	movzbl	-60(%rbp), %eax
	movl	%r13d, %r15d
	subb	-100(%rbp), %al
	movb	%al, (%r12)
.L169:
	movq	-120(%rbp), %r11
	movl	%r15d, %edx
	.p2align 4,,10
	.p2align 3
.L170:
	movb	%dl, 132(%r11,%rbx)
	movl	-60(%rbp), %eax
	subq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L170
	movl	%r15d, %esi
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L211:
	movl	%edi, 40(%rbx)
	xorl	%eax, %eax
	jmp	.L112
.L192:
	movq	-80(%rbp), %rdi
	movslq	%r15d, %rdx
	movl	-140(%rbp), %r11d
	movq	-152(%rbp), %r12
	movq	-160(%rbp), %rbx
	leaq	(%rdi,%rdx,2), %r10
.L163:
	movl	%eax, (%r12)
	movl	-136(%rbp), %eax
	subl	%r15d, %esi
	movl	%r11d, 4(%r12)
	movl	%eax, 20(%r12)
	movl	$34, %eax
	subl	%r15d, %eax
	movl	%esi, 16(%r12)
	movl	%r15d, 8(%r12)
	movl	$34, 12(%r12)
	movl	%eax, 44(%rbx)
	movl	%eax, 40(%rbx)
	movslq	(%r12), %rax
	movq	%r10, 48(%rbx)
	movq	%rax, 32(%rbx)
	movslq	4(%r12), %rax
	movq	%rax, 16(%rbx)
	movl	16(%r12), %eax
	movl	%eax, 28(%rbx)
	movl	$1, %eax
	jmp	.L112
.L168:
	movl	%eax, %edx
	movzbl	-100(%rbp), %ecx
	movq	-80(%rbp), %rsi
	sarl	$10, %eax
	andw	$1023, %dx
	subw	$10304, %ax
	subl	$2, %r15d
	orw	$-9216, %dx
	movw	%dx, (%r14)
	movzbl	-60(%rbp), %edx
	leaq	-2(%rsi,%r10), %r14
	subl	%ecx, %edx
	movb	%dl, (%r12)
	movslq	%r15d, %rdx
	movw	%ax, (%r14)
	movzbl	-60(%rbp), %eax
	subl	%ecx, %eax
	movq	-88(%rbp), %rcx
	movb	%al, (%rcx,%rdx)
	jmp	.L169
.L214:
	movl	44(%rbx), %eax
	movl	%eax, 40(%rbx)
	xorl	%eax, %eax
	jmp	.L112
.L149:
	subl	$240, %r14d
	cmpl	$4, %r14d
	jg	.L187
	movq	-72(%rbp), %rbx
	movslq	%r15d, %rdx
	leaq	.LC1(%rip), %r9
	movzbl	(%rbx,%rdx), %ecx
	movq	%rcx, %rdx
	shrq	$4, %rdx
	andl	$15, %edx
	movsbl	(%r9,%rdx), %edx
	btl	%r14d, %edx
	jnc	.L187
	leal	2(%rax), %r15d
	cmpl	%r15d, %r12d
	je	.L187
	movslq	%r15d, %rdx
	movzbl	(%rbx,%rdx), %edx
	leal	-128(%rdx), %r9d
	cmpb	$63, %r9b
	ja	.L187
	movl	%ecx, %edx
	sall	$6, %r14d
	andl	$63, %edx
	orl	%edx, %r14d
.L150:
	addl	$1, %r15d
	cmpl	%r15d, %r12d
	je	.L187
	movzbl	%r9b, %edx
	sall	$6, %r14d
	movl	%r15d, %r9d
	orl	%edx, %r14d
	jmp	.L151
.L160:
	cmpl	%r15d, 124(%rbx)
	jge	.L206
	movl	%r15d, 124(%rbx)
	testl	%r14d, %r14d
	jne	.L206
.L161:
	andl	$-3, 8(%rbx)
	movl	%eax, 120(%rbx)
	jmp	.L206
.L177:
	movl	%r11d, %esi
.L115:
	movq	-72(%rbp), %rdi
	movslq	%esi, %rax
	cmpb	$0, (%rdi,%rax)
	jne	.L114
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L220:
	movl	%r11d, %edx
	xorl	%esi, %esi
	call	utf8_back1SafeBody_67@PLT
	movq	88(%rbx), %r12
	movl	%eax, %r11d
	jmp	.L140
.L215:
	movl	%r11d, %edx
	xorl	%esi, %esi
	call	utf8_back1SafeBody_67@PLT
	movq	88(%rbx), %r12
	movl	120(%rbx), %ecx
	movl	%eax, %r11d
	jmp	.L132
.L201:
	movl	-140(%rbp), %r11d
	movq	-152(%rbp), %r12
	movq	%r14, %r10
	movq	-160(%rbp), %rbx
	jmp	.L163
.L218:
	movl	%r11d, %ecx
	movl	%r9d, %eax
	movl	-100(%rbp), %r11d
	subb	-96(%rbp), %al
	movq	-88(%rbp), %rdi
	movq	-120(%rbp), %r12
	movb	%al, (%r10)
	movl	%r9d, %eax
	subl	%r11d, %eax
	movq	-136(%rbp), %rbx
	cltq
	movb	%cl, (%rdi,%rax)
	movq	-128(%rbp), %rax
	movl	%r11d, (%r12)
	movl	%r9d, 4(%r12)
	movl	%esi, 12(%r12)
	movl	$0, 8(%r12)
	movl	%r11d, 20(%r12)
	movl	$0, 40(%rbx)
	movq	%rax, 48(%rbx)
	movl	12(%r12), %eax
	movl	%eax, 44(%rbx)
	movslq	(%r12), %rax
	movq	%rax, 32(%rbx)
	movslq	4(%r12), %rax
	movq	%rax, 16(%rbx)
	movl	16(%r12), %eax
	movl	%eax, 28(%rbx)
	cmpl	%r9d, 124(%rbx)
	jge	.L206
	movl	%r9d, 124(%rbx)
	movl	%r9d, %eax
	jmp	.L161
.L209:
	call	__stack_chk_fail@PLT
.L205:
	movl	%r14d, %edx
	jmp	.L147
	.cfi_endproc
.LFE2338:
	.size	utf8TextAccess, .-utf8TextAccess
	.p2align 4
	.type	repTextAccess, @function
repTextAccess:
.LFB2349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	72(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*64(%rax)
	movslq	%eax, %r15
	testq	%r12, %r12
	js	.L242
	cmpq	%r12, %r15
	cmovle	%r15, %r12
	movl	%r12d, %edx
.L223:
	movq	32(%rbx), %rcx
	testb	%r13b, %r13b
	je	.L224
	cmpq	%rcx, %r12
	jge	.L251
.L225:
	cmpl	%edx, %eax
	jg	.L227
	cmpq	16(%rbx), %r15
	je	.L252
.L227:
	leaq	9(%r12), %rax
	cmpq	%rax, %r15
	jl	.L228
	movq	%rax, 16(%rbx)
.L229:
	subq	$10, %rax
	movl	$0, %edx
	cmovs	%rdx, %rax
	movq	%rax, 32(%rbx)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L224:
	cmpq	%rcx, %r12
	jg	.L253
.L232:
	orq	%r12, %rcx
	je	.L254
	leal	-9(%rdx), %ecx
	testl	%ecx, %ecx
	movslq	%ecx, %rax
	movl	$0, %ecx
	cmovs	%rcx, %rax
	addl	$1, %edx
	movslq	%edx, %rdx
	cmpq	%rdx, %r15
	movq	%rax, 32(%rbx)
	cmovl	%r15, %rdx
	movq	%rdx, 16(%rbx)
.L231:
	movq	64(%rbx), %r13
	leaq	-128(%rbp), %r8
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%r8, %rdi
	movq	%r8, -136(%rbp)
	movq	%r13, %rsi
	movq	%r13, -144(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	(%r14), %rax
	movl	16(%rbx), %edx
	movq	%r14, %rdi
	movq	-136(%rbp), %r8
	movl	32(%rbx), %esi
	movq	%r8, %rcx
	call	*24(%rax)
	movq	16(%rbx), %rcx
	movq	32(%rbx), %rdx
	movq	%r13, 48(%rbx)
	movq	-136(%rbp), %r8
	movq	-144(%rbp), %r9
	movl	%ecx, %eax
	subl	%edx, %r12d
	subl	%edx, %eax
	cmpq	%rcx, %r15
	movl	%r12d, 40(%rbx)
	movl	%eax, 44(%rbx)
	jle	.L238
	leal	-1(%rax), %edi
	movslq	%edi, %rsi
	movzwl	0(%r13,%rsi,2), %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	je	.L255
.L238:
	testq	%rdx, %rdx
	jle	.L239
	movzwl	0(%r13), %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L239
	leaq	2(%r13), %r9
	addq	$1, %rdx
	subl	$1, %eax
	subl	$1, %r12d
	movq	%r9, 48(%rbx)
	movq	%rdx, 32(%rbx)
	movl	%eax, 44(%rbx)
	movl	%r12d, 40(%rbx)
.L239:
	movslq	%r12d, %rdx
	leaq	(%rdx,%rdx), %rcx
	movzwl	(%r9,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L240
	testl	%r12d, %r12d
	jg	.L256
.L240:
	movl	%eax, 28(%rbx)
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$1, %eax
.L222:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L257
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	cmpq	%r12, 16(%rbx)
	jle	.L225
	subl	%ecx, %r12d
	movl	$1, %eax
	movl	%r12d, 40(%rbx)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L253:
	cmpq	%r12, 16(%rbx)
	jl	.L232
	subl	%ecx, %edx
	movl	$1, %eax
	movl	%edx, 40(%rbx)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L242:
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%r15, 16(%rbx)
	movq	%r15, %rax
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L256:
	movzwl	-2(%r9,%rcx), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L240
	subl	$1, %r12d
	movl	%r12d, 40(%rbx)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L255:
	subq	$1, %rcx
	movl	%edi, 44(%rbx)
	movl	%edi, %eax
	movq	%rcx, 16(%rbx)
	cmpl	%edi, %r12d
	jle	.L238
	movl	%edi, 40(%rbx)
	movl	%edi, %r12d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$0, 40(%rbx)
	movl	%r13d, %eax
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L252:
	subl	%ecx, %eax
	movl	%eax, 40(%rbx)
	xorl	%eax, %eax
	jmp	.L222
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2349:
	.size	repTextAccess, .-repTextAccess
	.p2align 4
	.type	repTextReplace, @function
repTextReplace:
.LFB2351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	xorl	%r9d, %r9d
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L258
	movq	%rdx, %r13
	movq	%rdi, %r12
	movq	%rsi, %r14
	movq	%rcx, %rdx
	testq	%rcx, %rcx
	jne	.L260
	testl	%r8d, %r8d
	je	.L260
	movl	$1, (%rbx)
	.p2align 4,,10
	.p2align 3
.L258:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L284
	addq	$136, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	movq	72(%r12), %r15
	movl	%r8d, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*64(%rax)
	cmpq	%r13, %r14
	movq	-152(%rbp), %rdx
	movl	-160(%rbp), %r8d
	movl	%eax, %r10d
	jg	.L285
	cltq
	xorl	%ebx, %ebx
	testq	%r14, %r14
	js	.L262
	cmpq	%r14, %rax
	cmovle	%rax, %r14
	movq	%r14, %rbx
.L262:
	xorl	%r14d, %r14d
	testq	%r13, %r13
	js	.L263
	cmpq	%r13, %rax
	cmovle	%rax, %r13
	movq	%r13, %r14
.L263:
	cmpl	%ebx, %r10d
	jle	.L264
	movq	(%r15), %rax
	movl	%r8d, -164(%rbp)
	movl	%ebx, %esi
	movq	%r15, %rdi
	movq	%rdx, -160(%rbp)
	movl	%r10d, -152(%rbp)
	call	*72(%rax)
	movl	-152(%rbp), %r10d
	movq	-160(%rbp), %rdx
	andl	$64512, %eax
	movl	-164(%rbp), %r8d
	cmpl	$56320, %eax
	jne	.L264
	testl	%ebx, %ebx
	jg	.L286
	.p2align 4,,10
	.p2align 3
.L264:
	cmpl	%r14d, %r10d
	jle	.L265
	movq	(%r15), %rax
	leal	-1(%r14), %esi
	movq	%r15, %rdi
	movl	%r8d, -164(%rbp)
	movq	%rdx, -160(%rbp)
	movl	%r10d, -152(%rbp)
	call	*72(%rax)
	movl	-152(%rbp), %r10d
	movq	-160(%rbp), %rdx
	andl	$64512, %eax
	movl	-164(%rbp), %r8d
	cmpl	$55296, %eax
	je	.L287
.L265:
	movl	%r8d, %esi
	leaq	-128(%rbp), %r13
	movl	%r8d, %ecx
	movq	%rdx, -136(%rbp)
	shrl	$31, %esi
	leaq	-136(%rbp), %rdx
	movq	%r13, %rdi
	movl	%r10d, -152(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	(%r15), %rax
	movl	%ebx, %esi
	movq	%r13, %rcx
	movl	%r14d, %edx
	movq	%r15, %rdi
	movslq	%ebx, %rbx
	call	*32(%rax)
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*64(%rax)
	movl	-152(%rbp), %r10d
	subl	%r10d, %eax
	movl	%eax, %r9d
	cmpq	%rbx, 16(%r12)
	jle	.L266
	movq	$0, 16(%r12)
	movl	$0, 28(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
.L266:
	leal	(%r14,%r9), %esi
	movl	$1, %edx
	movq	%r12, %rdi
	movl	%r9d, -152(%rbp)
	movslq	%esi, %rsi
	call	repTextAccess
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-152(%rbp), %r9d
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L285:
	movl	$8, (%rbx)
	xorl	%r9d, %r9d
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L287:
	movq	(%r15), %rax
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*72(%rax)
	movl	-164(%rbp), %r8d
	movq	-160(%rbp), %rdx
	andl	$64512, %eax
	movl	-152(%rbp), %r10d
	cmpl	$56320, %eax
	sete	%al
	movzbl	%al, %eax
	addl	%eax, %r14d
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L286:
	movq	(%r15), %rax
	leal	-1(%rbx), %r13d
	movq	%r15, %rdi
	movl	%r13d, %esi
	call	*72(%rax)
	movl	-164(%rbp), %r8d
	movq	-160(%rbp), %rdx
	andl	$64512, %eax
	movl	-152(%rbp), %r10d
	cmpl	$55296, %eax
	cmove	%r13d, %ebx
	jmp	.L264
.L284:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2351:
	.size	repTextReplace, .-repTextReplace
	.p2align 4
	.type	repTextCopy, @function
repTextCopy:
.LFB2352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$104, %rsp
	movq	%r9, -136(%rbp)
	movq	72(%rdi), %r12
	movl	%r8d, -144(%rbp)
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	call	*64(%rax)
	movq	-136(%rbp), %r9
	movl	(%r9), %ecx
	testl	%ecx, %ecx
	jg	.L288
	cmpq	%r14, %r15
	jg	.L290
	cmpq	%rbx, %r14
	jle	.L291
	cmpq	%rbx, %r15
	jl	.L290
.L291:
	cltq
	xorl	%r8d, %r8d
	testq	%r15, %r15
	js	.L292
	cmpq	%r15, %rax
	cmovle	%rax, %r15
	movq	%r15, %r8
.L292:
	xorl	%r15d, %r15d
	testq	%r14, %r14
	js	.L293
	cmpq	%r14, %rax
	cmovle	%rax, %r14
	movq	%r14, %r15
.L293:
	testq	%rbx, %rbx
	js	.L304
	cmpq	%rbx, %rax
	cmovle	%rax, %rbx
	movl	%ebx, %r14d
.L294:
	movq	(%r12), %rax
	movl	%r14d, %ecx
	movl	%r15d, %edx
	movl	%r8d, %esi
	cmpb	$0, -144(%rbp)
	movl	%r8d, -136(%rbp)
	movq	%r12, %rdi
	movq	40(%rax), %rax
	je	.L295
	call	*%rax
	movl	-136(%rbp), %r8d
	cmpl	%r8d, %r14d
	jge	.L296
	movl	%r15d, %eax
	subl	%r8d, %eax
	movl	%r15d, %r8d
	addl	%eax, %r15d
.L296:
	movq	(%r12), %rax
	leaq	-128(%rbp), %r9
	movl	$2, %edx
	movl	%r8d, %esi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movw	%dx, -120(%rbp)
	movq	%r12, %rdi
	movl	%r15d, %edx
	movl	%r8d, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%r9, %rcx
	movq	%r9, -144(%rbp)
	call	*32(%rax)
	movq	-144(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movslq	-136(%rbp), %r8
	cmpl	%r8d, %r14d
	jle	.L298
	movq	%rbx, %rsi
	cmpq	16(%r13), %r8
	jge	.L299
.L300:
	movq	$0, 16(%r13)
	movl	$0, 28(%r13)
	movq	$0, 32(%r13)
	movq	$0, 40(%r13)
.L299:
	movl	$1, %edx
	movq	%r13, %rdi
	call	repTextAccess
.L288:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L313
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	movl	$8, (%r9)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L304:
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L295:
	call	*%rax
	movl	-136(%rbp), %r8d
.L298:
	leal	(%r14,%r15), %esi
	subl	%r8d, %esi
	movslq	%esi, %rsi
	cmpq	%rbx, 16(%r13)
	jg	.L300
	jmp	.L299
.L313:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2352:
	.size	repTextCopy, .-repTextCopy
	.p2align 4
	.type	repTextExtract, @function
repTextExtract:
.LFB2350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%rdx, -144(%rbp)
	movq	72(%rdi), %r14
	movq	%rcx, -136(%rbp)
	movq	(%r14), %rax
	movq	%r14, %rdi
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	call	*64(%rax)
	movl	0(%r13), %edx
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L314
	testl	%r12d, %r12d
	movq	-144(%rbp), %rdx
	js	.L316
	cmpq	$0, -136(%rbp)
	jne	.L317
	testl	%r12d, %r12d
	jle	.L317
.L316:
	movl	$1, 0(%r13)
.L317:
	cmpq	%rdx, %rbx
	jg	.L335
	movslq	%ecx, %rax
	xorl	%r8d, %r8d
	testq	%rbx, %rbx
	js	.L319
	cmpq	%rbx, %rax
	cmovle	%rax, %rbx
	movq	%rbx, %r8
.L319:
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	js	.L320
	cmpq	%rdx, %rax
	cmovle	%rax, %rdx
	movq	%rdx, %rbx
.L320:
	cmpl	%r8d, %ecx
	jle	.L321
	movq	(%r14), %rax
	movl	%ecx, -148(%rbp)
	movl	%r8d, %esi
	movq	%r14, %rdi
	movl	%r8d, -144(%rbp)
	call	*72(%rax)
	movl	-144(%rbp), %r8d
	movl	-148(%rbp), %ecx
	andl	$64512, %eax
	cmpl	$56320, %eax
	jne	.L321
	movq	(%r14), %rax
	movl	%r8d, %esi
	movq	%r14, %rdi
	call	*80(%rax)
	movl	-144(%rbp), %r8d
	movl	-148(%rbp), %ecx
	subl	$65536, %eax
	cmpl	$1048576, %eax
	sbbl	$0, %r8d
	.p2align 4,,10
	.p2align 3
.L321:
	cmpl	%ebx, %ecx
	jle	.L322
	movq	(%r14), %rax
	movl	%r8d, -144(%rbp)
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	*72(%rax)
	movl	-144(%rbp), %r8d
	andl	$64512, %eax
	cmpl	$56320, %eax
	jne	.L322
	movq	(%r14), %rax
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	*80(%rax)
	movl	-144(%rbp), %r8d
	subl	$65536, %eax
	cmpl	$1048576, %eax
	sbbl	$0, %ebx
	.p2align 4,,10
	.p2align 3
.L322:
	movl	%ebx, %r11d
	leal	(%r8,%r12), %eax
	leaq	-128(%rbp), %r10
	movl	%r12d, %ecx
	subl	%r8d, %r11d
	movq	-136(%rbp), %rsi
	movq	%r10, %rdi
	movl	%r8d, -152(%rbp)
	cmpl	%r11d, %r12d
	movl	%r11d, -148(%rbp)
	cmovl	%eax, %ebx
	xorl	%edx, %edx
	movq	%r10, -144(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	-144(%rbp), %r10
	movq	(%r14), %rax
	movq	%r14, %rdi
	movl	-152(%rbp), %r8d
	movl	%ebx, %edx
	movq	%r10, %rcx
	movl	%r8d, %esi
	call	*24(%rax)
	movslq	%ebx, %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	call	repTextAccess
	movl	-148(%rbp), %r11d
	movq	%r13, %rcx
	movl	%r12d, %esi
	movq	-136(%rbp), %rdi
	movl	%r11d, %edx
	call	u_terminateUChars_67@PLT
	movq	-144(%rbp), %r10
	movl	%eax, -136(%rbp)
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-136(%rbp), %eax
.L314:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L336
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movl	$8, 0(%r13)
	xorl	%eax, %eax
	jmp	.L314
.L336:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2350:
	.size	repTextExtract, .-repTextExtract
	.p2align 4
	.type	unistrTextReplace, @function
unistrTextReplace:
.LFB2359:
	.cfi_startproc
	endbr64
	movl	(%r9), %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L362
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r12
	testq	%rcx, %rcx
	jne	.L339
	testl	%r8d, %r8d
	je	.L339
	movl	$1, (%r9)
.L339:
	cmpq	%rdx, %rsi
	jg	.L365
	movswl	8(%r12), %r14d
	testw	%r14w, %r14w
	js	.L341
	sarl	$5, %r14d
.L342:
	movslq	%r14d, %rax
	xorl	%r10d, %r10d
	testq	%rsi, %rsi
	js	.L343
	cmpq	%rsi, %rax
	cmovle	%rax, %rsi
	movq	%rsi, %r10
.L343:
	xorl	%r13d, %r13d
	testq	%rdx, %rdx
	js	.L344
	cmpq	%rdx, %rax
	cmovle	%rax, %rdx
	movq	%rdx, %r13
.L344:
	cmpl	%r10d, %r14d
	jg	.L366
	cmpl	%r13d, %r14d
	jg	.L367
.L346:
	movl	%r13d, %edx
	movl	%r8d, %r9d
	movq	%r15, %rcx
	xorl	%r8d, %r8d
	subl	%r10d, %edx
	movl	%r10d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	8(%r12), %edx
	testw	%dx, %dx
	js	.L347
	movswl	%dx, %eax
	sarl	$5, %eax
.L348:
	testb	$17, %dl
	jne	.L354
	andl	$2, %edx
	jne	.L368
	movq	24(%r12), %r12
.L349:
	movl	%eax, 44(%rbx)
	movslq	%eax, %rdx
	movl	%eax, 28(%rbx)
	subl	%r14d, %eax
	addl	%eax, %r13d
	movq	%r12, 48(%rbx)
	movq	%rdx, 16(%rbx)
	movl	%r13d, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$8, (%r9)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	addq	$10, %r12
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L347:
	movl	12(%r12), %eax
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L341:
	movl	12(%r12), %r14d
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L366:
	movl	%r10d, %esi
	movq	%r12, %rdi
	movl	%r8d, -52(%rbp)
	call	_ZNK6icu_6713UnicodeString14getChar32StartEi@PLT
	movl	-52(%rbp), %r8d
	movl	%eax, %r10d
	cmpl	%r13d, %r14d
	jle	.L346
.L367:
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%r8d, -56(%rbp)
	movl	%r10d, -52(%rbp)
	call	_ZNK6icu_6713UnicodeString14getChar32StartEi@PLT
	movl	-56(%rbp), %r8d
	movl	-52(%rbp), %r10d
	movl	%eax, %r13d
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L354:
	xorl	%r12d, %r12d
	jmp	.L349
	.cfi_endproc
.LFE2359:
	.size	unistrTextReplace, .-unistrTextReplace
	.p2align 4
	.type	unistrTextExtract, @function
unistrTextExtract:
.LFB2358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	72(%rdi), %rdi
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L370
	movswl	%ax, %edx
	sarl	$5, %edx
.L371:
	movl	(%r12), %eax
	xorl	%r9d, %r9d
	testl	%eax, %eax
	jg	.L369
	testl	%r13d, %r13d
	js	.L373
	testq	%r14, %r14
	jne	.L374
	testl	%r13d, %r13d
	jle	.L374
.L373:
	movl	$1, (%r12)
.L374:
	testq	%rsi, %rsi
	js	.L383
	cmpq	%r15, %rsi
	jg	.L383
	movslq	%edx, %rcx
	movl	%edx, %r10d
	cmpq	%rsi, %rcx
	jg	.L397
.L377:
	cmpq	%r15, %rcx
	jg	.L398
.L378:
	subl	%r10d, %edx
	movl	%edx, %r9d
	testl	%r13d, %r13d
	jle	.L379
	testq	%r14, %r14
	jne	.L399
.L379:
	movl	%r10d, 40(%rbx)
.L380:
	movl	%r9d, %edx
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%r9d, -56(%rbp)
	call	u_terminateUChars_67@PLT
	movl	-56(%rbp), %r9d
.L369:
	addq	$40, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movl	12(%rdi), %edx
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L399:
	cmpl	%edx, %r13d
	movl	%edx, %r15d
	movl	%edx, -64(%rbp)
	movq	%r14, %rcx
	cmovle	%r13d, %r15d
	movl	%r10d, %esi
	xorl	%r8d, %r8d
	movl	%r10d, -56(%rbp)
	movl	%r15d, %edx
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movl	-56(%rbp), %r10d
	movl	-64(%rbp), %r9d
	addl	%r15d, %r10d
	movl	%r10d, 40(%rbx)
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L383:
	movl	$8, (%r12)
	xorl	%r9d, %r9d
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L398:
	movl	%r15d, %esi
	movl	%r10d, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNK6icu_6713UnicodeString14getChar32StartEi@PLT
	movl	-64(%rbp), %r10d
	movq	-56(%rbp), %rdi
	movl	%eax, %edx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L397:
	movl	%edx, -68(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZNK6icu_6713UnicodeString14getChar32StartEi@PLT
	movl	-68(%rbp), %edx
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rdi
	movl	%eax, %r10d
	jmp	.L377
	.cfi_endproc
.LFE2358:
	.size	unistrTextExtract, .-unistrTextExtract
	.p2align 4
	.type	repTextClose, @function
repTextClose:
.LFB2347:
	.cfi_startproc
	endbr64
	testb	$32, 8(%rdi)
	je	.L410
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L402
	movq	(%rdi), %rax
	call	*8(%rax)
.L402:
	movq	$0, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2347:
	.size	repTextClose, .-repTextClose
	.p2align 4
	.type	unistrTextClose, @function
unistrTextClose:
.LFB2355:
	.cfi_startproc
	endbr64
	testb	$32, 8(%rdi)
	je	.L423
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L415
	movq	(%rdi), %rax
	call	*8(%rax)
.L415:
	movq	$0, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2355:
	.size	unistrTextClose, .-unistrTextClose
	.p2align 4
	.type	utf8TextClose, @function
utf8TextClose:
.LFB2344:
	.cfi_startproc
	endbr64
	testb	$32, 8(%rdi)
	jne	.L435
	ret
	.p2align 4,,10
	.p2align 3
.L435:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	$0, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2344:
	.size	utf8TextClose, .-utf8TextClose
	.p2align 4
	.type	ucstrTextClose, @function
ucstrTextClose:
.LFB2364:
	.cfi_startproc
	endbr64
	testb	$32, 8(%rdi)
	jne	.L445
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	$0, 72(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2364:
	.size	ucstrTextClose, .-ucstrTextClose
	.p2align 4
	.type	ucstrTextExtract, @function
ucstrTextExtract:
.LFB2367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L446
	movl	%r8d, %r15d
	movq	%r9, %r13
	testl	%r8d, %r8d
	js	.L448
	testq	%rcx, %rcx
	movq	%rdx, %rbx
	movq	%rcx, %r12
	sete	%dl
	testl	%r8d, %r8d
	setg	%al
	testb	%al, %dl
	jne	.L448
	cmpq	%rbx, %rsi
	jle	.L449
.L448:
	movl	$1, 0(%r13)
	xorl	%r14d, %r14d
.L446:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movl	$1, %edx
	call	ucstrTextAccess
	movq	112(%rdi), %rax
	movq	48(%rdi), %r8
	movl	40(%rdi), %ecx
	movl	%eax, %r9d
	testl	%eax, %eax
	js	.L451
	xorl	%r11d, %r11d
	testq	%rbx, %rbx
	js	.L452
	movslq	%eax, %r11
	cmpq	%rbx, %r11
	cmovg	%rbx, %r11
.L452:
	cmpl	%r11d, %ecx
	jge	.L480
	movl	%ecx, %eax
	movslq	%r15d, %rsi
	notl	%eax
	addq	$1, %rsi
	leal	(%rax,%r11), %r10d
	movslq	%ecx, %rax
	leaq	(%r8,%rax,2), %rbx
	addq	$2, %r10
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L457:
	cmpq	%rsi, %rax
	je	.L456
	movzwl	-2(%rbx,%rax,2), %edx
	movl	%eax, %r14d
	movw	%dx, -2(%r12,%rax,2)
	leal	(%rcx,%rax), %edx
	addq	$1, %rax
	cmpq	%rax, %r10
	jne	.L457
.L453:
	movq	16(%rdi), %r10
	movslq	%edx, %rsi
	testl	%edx, %edx
	jle	.L463
	leaq	(%rsi,%rsi), %rax
.L459:
	movzwl	-2(%r8,%rax), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L481
.L463:
	cmpq	%r10, %rsi
	jg	.L466
.L460:
	movl	%edx, 40(%rdi)
.L467:
	movq	%r13, %rcx
	movl	%r14d, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	u_terminateUChars_67@PLT
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L451:
	cmpq	$2147483647, %rbx
	movl	$2147483647, %eax
	cmovg	%rax, %rbx
	movl	$0, %eax
	testq	%rbx, %rbx
	cmovs	%rax, %rbx
	cmpl	%ebx, %ecx
	jge	.L482
	movslq	%ecx, %rdx
	movl	%ebx, %r14d
	leaq	(%rdx,%rdx), %rax
	negq	%rdx
	subl	%ecx, %r14d
	leaq	(%r12,%rdx,2), %r10
	movl	%ecx, %edx
	xorl	%ecx, %ecx
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L458:
	cmpl	%ecx, %r15d
	jle	.L461
	movw	%si, (%r10,%rax)
.L461:
	addl	$1, %ecx
	addl	$1, %edx
	addq	$2, %rax
	cmpl	%r14d, %ecx
	je	.L453
.L462:
	movzwl	(%r8,%rax), %esi
	testw	%si, %si
	jne	.L458
	movslq	%edx, %rsi
	movl	%edx, 44(%rdi)
	movl	%ecx, %r14d
	movl	%edx, %r9d
	movq	%rsi, 112(%rdi)
	movq	%rsi, %r10
	movq	%rsi, 16(%rdi)
	movl	%edx, 28(%rdi)
	testl	%edx, %edx
	jg	.L459
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L466:
	movl	$1, %edx
	call	ucstrTextAccess
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L456:
	movl	%r11d, %r14d
	movl	%r11d, %edx
	subl	%ecx, %r14d
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L481:
	cmpl	%edx, %r9d
	jg	.L474
	testl	%r9d, %r9d
	jns	.L463
.L474:
	movzwl	(%r8,%rax), %eax
	movl	%eax, %ecx
	andl	$64512, %ecx
	cmpl	$56320, %ecx
	jne	.L463
	cmpl	%r14d, %r15d
	jle	.L465
	movslq	%r14d, %rcx
	addl	$1, %r14d
	movw	%ax, (%r12,%rcx,2)
.L465:
	addl	$1, %edx
	movslq	%edx, %rsi
	jmp	.L463
.L480:
	movl	%ecx, %edx
	xorl	%r14d, %r14d
	jmp	.L453
.L482:
	movl	%ecx, %edx
	jmp	.L453
	.cfi_endproc
.LFE2367:
	.size	ucstrTextExtract, .-ucstrTextExtract
	.p2align 4
	.type	unistrTextCopy, @function
unistrTextCopy:
.LFB2360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movq	72(%rbx), %r12
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L484
	sarl	$5, %eax
.L485:
	movl	(%r9), %esi
	testl	%esi, %esi
	jg	.L483
	cltq
	xorl	%esi, %esi
	testq	%rdi, %rdi
	js	.L488
	cmpq	%rdi, %rax
	cmovle	%rax, %rdi
	movq	%rdi, %rsi
.L488:
	xorl	%r14d, %r14d
	testq	%rdx, %rdx
	js	.L489
	cmpq	%rdx, %rax
	cmovle	%rax, %rdx
	movq	%rdx, %r14
.L489:
	xorl	%r10d, %r10d
	testq	%rcx, %rcx
	js	.L490
	cmpq	%rcx, %rax
	cmovle	%rax, %rcx
	movq	%rcx, %r10
.L490:
	cmpl	%r14d, %esi
	jg	.L491
	cmpl	%r10d, %esi
	jge	.L492
	cmpl	%r14d, %r10d
	jge	.L492
.L491:
	movl	$8, (%r9)
.L483:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movq	(%r12), %rax
	leal	(%r10,%r14), %r15d
	movq	40(%rax), %rax
	testb	%r13b, %r13b
	jne	.L532
	movl	%r10d, -56(%rbp)
	movl	%r10d, %ecx
	movl	%r14d, %edx
	movq	%r12, %rdi
	movl	%esi, -52(%rbp)
	call	*%rax
	movzwl	8(%r12), %eax
	movl	-52(%rbp), %esi
	movl	-56(%rbp), %r10d
	subl	%esi, %r15d
	testb	$17, %al
	jne	.L499
.L508:
	testb	$2, %al
	jne	.L497
	movq	24(%r12), %r12
.L501:
	movq	%r12, 48(%rbx)
	testb	%r13b, %r13b
	je	.L502
.L529:
	cmpl	%esi, %r10d
	jle	.L531
	movl	%r10d, 40(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movl	12(%r12), %eax
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L532:
	movl	%r14d, %r11d
	movl	%r10d, -56(%rbp)
	movl	%r10d, %ecx
	movl	%r14d, %edx
	subl	%esi, %r11d
	movl	%esi, -52(%rbp)
	movq	%r12, %rdi
	movl	%r11d, -60(%rbp)
	call	*%rax
	movl	-52(%rbp), %esi
	movl	-56(%rbp), %r10d
	movl	-60(%rbp), %r11d
	subl	%esi, %r15d
	cmpl	%r10d, %esi
	jle	.L494
	movl	%r10d, %r15d
	movl	%r14d, %esi
.L494:
	testl	%esi, %esi
	jg	.L495
	cmpl	$2147483647, %r11d
	jne	.L495
	movzwl	8(%r12), %edx
	testb	$1, %dl
	jne	.L533
	movl	%edx, %eax
	andl	$17, %edx
	andl	$31, %eax
	movw	%ax, 8(%r12)
.L498:
	testw	%dx, %dx
	je	.L508
	movq	$0, 48(%rbx)
	testb	%r13b, %r13b
	jne	.L529
.L531:
	movl	%r15d, 40(%rbx)
.L534:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movq	$0, 48(%rbx)
	.p2align 4,,10
	.p2align 3
.L502:
	subl	%esi, %r14d
	addl	44(%rbx), %r14d
	movl	%r15d, 40(%rbx)
	movslq	%r14d, %rax
	movl	%r14d, 44(%rbx)
	movq	%rax, 16(%rbx)
	movl	%r14d, 28(%rbx)
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L533:
	movl	$2, %eax
	movw	%ax, 8(%r12)
.L497:
	addq	$10, %r12
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L495:
	movl	%r11d, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	%r10d, -56(%rbp)
	movl	%esi, -52(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movzwl	8(%r12), %eax
	movl	-52(%rbp), %esi
	movl	-56(%rbp), %r10d
	movl	%eax, %edx
	andl	$17, %edx
	jmp	.L498
	.cfi_endproc
.LFE2360:
	.size	unistrTextCopy, .-unistrTextCopy
	.p2align 4
	.type	charIterTextExtract, @function
charIterTextExtract:
.LFB2373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %eax
	movq	%rdi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	testl	%eax, %eax
	jg	.L535
	movl	%r8d, %r12d
	movq	%r9, %r13
	testl	%r8d, %r8d
	js	.L537
	testq	%rcx, %rcx
	sete	%cl
	testl	%r8d, %r8d
	setg	%al
	testb	%al, %cl
	jne	.L537
	cmpq	%rdx, %rsi
	jg	.L537
	movslq	112(%rdi), %rax
	xorl	%r8d, %r8d
	testq	%rsi, %rsi
	js	.L540
	cmpq	%rax, %rsi
	cmovg	%rax, %rsi
	movq	%rsi, %r8
.L540:
	movl	$0, -56(%rbp)
	testq	%rdx, %rdx
	js	.L541
	cmpq	%rax, %rdx
	cmovle	%rdx, %rax
	movq	%rax, -56(%rbp)
.L541:
	movq	-72(%rbp), %rax
	movl	%r8d, %esi
	movl	$0, %r15d
	movq	72(%rax), %r14
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*128(%rax)
	movl	12(%r14), %ebx
	cmpl	-56(%rbp), %ebx
	movl	%ebx, -76(%rbp)
	jl	.L547
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L543:
	leal	1(%r15), %esi
	cmpl	%esi, %r12d
	jge	.L548
	movl	%esi, %r15d
	movl	$1, %eax
.L549:
	movl	$15, 0(%r13)
	addl	%eax, %ebx
	cmpl	-56(%rbp), %ebx
	jge	.L542
.L547:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*48(%rax)
	cmpl	$65535, %eax
	jbe	.L543
	leal	2(%r15), %esi
	cmpl	%esi, %r12d
	jge	.L550
	movl	%esi, %r15d
	movl	$2, %eax
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L537:
	movl	$1, 0(%r13)
	xorl	%r15d, %r15d
.L535:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	movq	-64(%rbp), %rcx
	movslq	%r15d, %r8
	movl	%esi, %r15d
	movl	$1, %esi
	movw	%ax, (%rcx,%r8,2)
.L545:
	leal	(%rbx,%rsi), %eax
	movl	%eax, -76(%rbp)
	movl	%eax, %ebx
	cmpl	-56(%rbp), %ebx
	jl	.L547
.L542:
	movslq	-76(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movl	$1, %edx
	call	charIterTextAccess
	movq	-64(%rbp), %rdi
	movq	%r13, %rcx
	movl	%r15d, %edx
	movl	%r12d, %esi
	call	u_terminateUChars_67@PLT
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L550:
	movl	%eax, %edi
	movq	-64(%rbp), %rdx
	andw	$1023, %ax
	movslq	%r15d, %r8
	sarl	$10, %edi
	orw	$-9216, %ax
	movl	%esi, %r15d
	movl	$2, %esi
	subw	$10304, %di
	movw	%ax, 2(%rdx,%r8,2)
	movw	%di, (%rdx,%r8,2)
	jmp	.L545
	.cfi_endproc
.LFE2373:
	.size	charIterTextExtract, .-charIterTextExtract
	.p2align 4
	.type	utext_setup_67.part.0, @function
utext_setup_67.part.0:
.LFB2919:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L590
	cmpl	$878368812, (%rdi)
	je	.L564
	movl	$1, (%rdx)
	movq	%rdi, %r12
.L558:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movl	4(%rdi), %eax
	testb	$4, %al
	je	.L565
	movq	56(%rdi), %rdx
	movq	80(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L565
	call	*%rdx
	movl	4(%r13), %eax
.L565:
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 4(%r13)
	movl	24(%r13), %edx
	cmpl	%edx, %ebx
	jle	.L572
	testb	$2, %al
	jne	.L591
.L566:
	movslq	%ebx, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.L592
	orl	$2, 4(%r13)
	movq	%r13, %r12
	movl	%ebx, 24(%r13)
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L590:
	testl	%esi, %esi
	jle	.L560
	leal	144(%rsi), %r15d
	movslq	%r15d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L569
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r13, %rax
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
	testl	%r15d, %r15d
	jg	.L568
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L563:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L558
	movq	64(%r12), %rdi
	pxor	%xmm0, %xmm0
	orl	$4, 4(%r12)
	movq	$0, 16(%r12)
	movl	$0, 28(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	testq	%rdi, %rdi
	je	.L558
	testl	%ebx, %ebx
	jle	.L558
	movslq	%ebx, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L572:
	movl	%edx, %ebx
	movq	%r13, %r12
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L560:
	movl	$144, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L569
	movq	%r13, %rax
	movl	$18, %ecx
	movq	%r12, %rdi
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
.L568:
	leaq	144(%r12), %rax
	movl	%ebx, 24(%r12)
	movq	%rax, 64(%r12)
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L591:
	movq	64(%r13), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r13)
	jmp	.L566
.L569:
	movl	$7, (%r14)
	xorl	%r12d, %r12d
	jmp	.L558
.L592:
	movl	$7, (%r14)
	movq	%r13, %r12
	jmp	.L558
	.cfi_endproc
.LFE2919:
	.size	utext_setup_67.part.0, .-utext_setup_67.part.0
	.p2align 4
	.type	utf8TextExtract, @function
utf8TextExtract:
.LFB2340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movl	%r8d, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L593
	testl	%r8d, %r8d
	js	.L595
	movq	%rcx, %r15
	testq	%rcx, %rcx
	jne	.L596
	testl	%r8d, %r8d
	jle	.L596
.L595:
	movl	$1, (%r9)
	xorl	%r14d, %r14d
.L593:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L653
	addq	$72, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	movslq	120(%rax), %rcx
	testq	%rsi, %rsi
	js	.L597
	cmpq	%rcx, %rsi
	cmovg	%rcx, %rsi
	xorl	%eax, %eax
	testq	%rdx, %rdx
	js	.L598
.L621:
	cmpq	%rcx, %rdx
	cmovle	%rdx, %rcx
	movq	%rcx, %rax
.L598:
	cmpl	%eax, %esi
	jg	.L654
	movslq	%eax, %rbx
	movslq	%esi, %r8
	movq	%rbx, -96(%rbp)
	movq	%r8, %rdi
.L620:
	movq	-72(%rbp), %rbx
	movq	72(%rbx), %rdx
	movq	16(%rbx), %rcx
	leaq	(%rdx,%rdi), %r12
	cmpq	%r8, %rcx
	jle	.L601
	movslq	%esi, %rdi
	leaq	(%rdx,%rdi), %r12
	movzbl	(%r12), %r8d
	testb	%r8b, %r8b
	jns	.L601
	addl	$62, %r8d
	cmpb	$50, %r8b
	jbe	.L601
	testl	%esi, %esi
	je	.L625
	leal	-1(%rsi), %r11d
	movslq	%r11d, %rdi
	leaq	(%rdx,%rdi), %r12
	movzbl	(%r12), %r8d
	testb	%r8b, %r8b
	jns	.L630
	addl	$62, %r8d
	cmpb	$50, %r8b
	jbe	.L630
	testl	%r11d, %r11d
	je	.L631
	leal	-2(%rsi), %r11d
	movslq	%r11d, %rdi
	leaq	(%rdx,%rdi), %r12
	movzbl	(%r12), %r8d
	testb	%r8b, %r8b
	jns	.L630
	addl	$62, %r8d
	cmpb	$50, %r8b
	jbe	.L630
	testl	%r11d, %r11d
	je	.L631
	subl	$3, %esi
	movslq	%esi, %rdi
	leaq	(%rdx,%rdi), %r12
.L601:
	cmpq	-96(%rbp), %rcx
	jle	.L602
	movslq	%eax, %rcx
	movzbl	(%rdx,%rcx), %r8d
	testb	%r8b, %r8b
	jns	.L604
	addl	$62, %r8d
	cmpb	$50, %r8b
	jbe	.L604
	testl	%eax, %eax
	je	.L651
	leal	-1(%rax), %r11d
	movslq	%r11d, %rcx
	movzbl	(%rdx,%rcx), %r8d
	testb	%r8b, %r8b
	jns	.L634
	addl	$62, %r8d
	cmpb	$50, %r8b
	jbe	.L634
	testl	%r11d, %r11d
	je	.L636
	leal	-2(%rax), %r8d
	movslq	%r8d, %rcx
	movzbl	(%rdx,%rcx), %edx
	testb	%dl, %dl
	jns	.L652
	addl	$62, %edx
	cmpb	$50, %dl
	jbe	.L652
	testl	%r8d, %r8d
	je	.L636
	subl	$3, %eax
	movslq	%eax, %rbx
	movq	%rbx, -96(%rbp)
.L602:
	subl	%esi, %eax
	movl	%eax, %r13d
	testq	%r15, %r15
	je	.L607
	movslq	-84(%rbp), %rax
	movl	$0, -60(%rbp)
	leaq	(%r15,%rax,2), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rax, %r15
	jnb	.L637
	testl	%r13d, %r13d
	jle	.L637
	movq	%r15, %rbx
	leaq	-60(%rbp), %rdi
	movq	%r15, -104(%rbp)
	xorl	%eax, %eax
	movq	%r12, %r15
	movq	%r9, -112(%rbp)
	movl	%r13d, %r12d
	movq	%rbx, %r13
	movq	%rdi, %rbx
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L655:
	movw	%r10w, 0(%r13)
	movslq	%ecx, %rax
	movq	%r14, %r13
.L612:
	cmpl	%eax, %r12d
	jle	.L640
	cmpq	%r13, -80(%rbp)
	jbe	.L640
.L609:
	leal	1(%rax), %ecx
	leaq	2(%r13), %r14
	movl	%ecx, -60(%rbp)
	movzbl	(%r15,%rax), %r10d
	cmpl	$127, %r10d
	jle	.L655
	movl	%r10d, %ecx
	movl	$-3, %r8d
	movl	%r12d, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	utf8_nextCharSafeBody_67@PLT
	movl	%eax, %ecx
	cmpl	$65535, %eax
	ja	.L613
	movw	%ax, 0(%r13)
	movslq	-60(%rbp), %rax
	movq	%r14, %r13
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L597:
	xorl	%esi, %esi
	testq	%rdx, %rdx
	jns	.L621
	movq	$0, -96(%rbp)
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	xorl	%esi, %esi
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L613:
	sarl	$10, %eax
	subw	$10304, %ax
	movw	%ax, 0(%r13)
	movslq	-60(%rbp), %rax
	cmpq	%r14, -80(%rbp)
	jbe	.L656
	andw	$1023, %cx
	addq	$4, %r13
	orw	$-9216, %cx
	movw	%cx, -2(%r13)
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L640:
	movq	%r13, %rbx
	movl	%r12d, %r13d
	movq	%r15, %r12
	movq	-104(%rbp), %r15
	movq	-112(%rbp), %r9
	xorl	%r14d, %r14d
	subq	%r15, %rbx
	sarq	%rbx
	movl	%ebx, -80(%rbp)
.L608:
	cmpl	%eax, %r13d
	jle	.L615
	movq	%r9, -104(%rbp)
	leaq	-60(%rbp), %rbx
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L657:
	movslq	%edx, %rax
	addl	$1, %r14d
	cmpl	%eax, %r13d
	jle	.L649
.L616:
	leal	1(%rax), %edx
	movl	%edx, -60(%rbp)
	movzbl	(%r12,%rax), %ecx
	cmpl	$127, %ecx
	jle	.L657
	movl	$-3, %r8d
	movl	%r13d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	utf8_nextCharSafeBody_67@PLT
	cmpl	$65535, %eax
	seta	%al
	movzbl	%al, %eax
	leal	1(%rax,%r14), %r14d
	movslq	-60(%rbp), %rax
	cmpl	%eax, %r13d
	jg	.L616
.L649:
	movq	-104(%rbp), %r9
.L615:
	movl	-84(%rbp), %esi
	addl	-80(%rbp), %r14d
	movq	%r15, %rdi
	movq	%r9, %rcx
	movl	%r14d, %edx
	call	u_terminateUChars_67@PLT
	movq	-96(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movl	$1, %edx
	call	utf8TextAccess
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L654:
	movl	$8, (%r9)
	xorl	%r14d, %r14d
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L656:
	movl	%r12d, %r13d
	movq	%r15, %r12
	movq	-104(%rbp), %r15
	movq	%r14, %rbx
	movq	-112(%rbp), %r9
	movl	$1, %r14d
	subq	%r15, %rbx
	sarq	%rbx
	movl	%ebx, -80(%rbp)
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L607:
	movl	$0, -60(%rbp)
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	movl	$0, -80(%rbp)
	jmp	.L608
.L634:
	movl	%r11d, %eax
.L604:
	movq	%rcx, -96(%rbp)
	jmp	.L602
.L630:
	movl	%r11d, %esi
	jmp	.L601
.L636:
	xorl	%eax, %eax
.L651:
	movq	$0, -96(%rbp)
	jmp	.L602
.L631:
	movq	%rdx, %r12
	xorl	%esi, %esi
	jmp	.L601
.L652:
	movl	%r8d, %eax
	jmp	.L604
.L625:
	movq	%rdx, %r12
	jmp	.L601
.L653:
	call	__stack_chk_fail@PLT
.L637:
	movl	$0, -80(%rbp)
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L608
	.cfi_endproc
.LFE2340:
	.size	utf8TextExtract, .-utf8TextExtract
	.p2align 4
	.type	ucstrTextClone, @function
ucstrTextClone:
.LFB2363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movl	%edx, -52(%rbp)
	testl	%r8d, %r8d
	jg	.L658
	movl	24(%rsi), %r14d
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%rcx, %r15
	testq	%rdi, %rdi
	je	.L720
	cmpl	$878368812, (%rdi)
	je	.L665
	movl	$1, (%rcx)
	movq	%rdi, %r12
.L658:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L665:
	.cfi_restore_state
	movl	4(%rdi), %eax
	testb	$4, %al
	je	.L666
	movq	56(%rdi), %rdx
	movq	80(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L666
	call	*%rdx
	movl	4(%r13), %eax
.L666:
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 4(%r13)
	movslq	24(%r13), %rdx
	cmpl	%edx, %r14d
	jle	.L695
	testb	$2, %al
	jne	.L721
.L667:
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.L722
	orl	$2, 4(%r13)
	movslq	%r14d, %rdx
	movq	%r13, %r12
	movl	%r14d, 24(%r13)
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L720:
	testl	%r14d, %r14d
	jle	.L661
	leal	144(%r14), %esi
	movslq	%esi, %rdi
	movl	%esi, -64(%rbp)
	call	uprv_malloc_67@PLT
	movl	-64(%rbp), %esi
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L691
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r13, %rax
	xorl	%edx, %edx
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
	testl	%esi, %esi
	jg	.L690
	.p2align 4,,10
	.p2align 3
.L664:
	movl	(%r15), %edi
	testl	%edi, %edi
	jg	.L658
	movl	4(%r12), %r13d
	movq	64(%r12), %r9
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r12)
	movl	$0, 28(%r12)
	orl	$4, %r13d
	movups	%xmm0, 72(%r12)
	movl	%r13d, 4(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 88(%r12)
	testq	%r9, %r9
	je	.L669
	testl	%edx, %edx
	jg	.L723
.L669:
	movl	12(%rbx), %edx
	cmpl	%edx, 12(%r12)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	cmovle	12(%r12), %edx
	movq	%r9, -64(%rbp)
	movslq	%edx, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	movl	%r13d, 4(%r12)
	movq	%r9, 64(%r12)
	testl	%r14d, %r14d
	jle	.L670
	movq	64(%rbx), %rsi
	movslq	%r14d, %rdx
	movq	%r9, %rdi
	call	memcpy@PLT
.L670:
	movq	72(%r12), %rdx
	movq	64(%rbx), %rax
	cmpq	%rax, %rdx
	jb	.L671
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L671
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 72(%r12)
	movq	64(%rbx), %rax
.L672:
	movq	80(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L673
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L673
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 80(%r12)
	movq	64(%rbx), %rax
.L674:
	movq	88(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L675
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L675
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 88(%r12)
	movq	64(%rbx), %rax
.L676:
	movq	96(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L677
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L677
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 96(%r12)
	movq	64(%rbx), %rax
.L678:
	movq	48(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L679
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L679
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 48(%r12)
.L680:
	andl	$-33, 8(%r12)
	cmpb	$0, -52(%rbp)
	je	.L658
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L658
	movq	56(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	movq	72(%rbx), %rbx
	leal	1(%rax), %r13d
	movq	%rax, %r14
	movslq	%r13d, %r13
	addq	%r13, %r13
	movq	%r13, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L724
	movslq	%r14d, %rcx
	testq	%rcx, %rcx
	jle	.L687
	leaq	15(%rbx), %rdx
	subq	%rax, %rdx
	cmpq	$30, %rdx
	jbe	.L696
	leaq	-1(%rcx), %rdx
	cmpq	$6, %rdx
	jbe	.L696
	movq	%rcx, %rsi
	xorl	%edx, %edx
	shrq	$3, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L686:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L686
	movq	%rcx, %rdx
	andq	$-8, %rdx
	testb	$7, %cl
	je	.L687
	movzwl	(%rbx,%rdx,2), %edi
	leaq	(%rdx,%rdx), %rsi
	movw	%di, (%rax,%rdx,2)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rcx
	jle	.L687
	movzwl	2(%rbx,%rsi), %edi
	movw	%di, 2(%rax,%rsi)
	leaq	2(%rdx), %rdi
	cmpq	%rcx, %rdi
	jge	.L687
	movzwl	4(%rbx,%rsi), %edi
	movw	%di, 4(%rax,%rsi)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %rcx
	jle	.L687
	movzwl	6(%rbx,%rsi), %edi
	movw	%di, 6(%rax,%rsi)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %rcx
	jle	.L687
	movzwl	8(%rbx,%rsi), %edi
	movw	%di, 8(%rax,%rsi)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %rcx
	jle	.L687
	movzwl	10(%rbx,%rsi), %edi
	addq	$6, %rdx
	movw	%di, 10(%rax,%rsi)
	cmpq	%rdx, %rcx
	jle	.L687
	movzwl	12(%rbx,%rsi), %edx
	movw	%dx, 12(%rax,%rsi)
.L687:
	xorl	%edx, %edx
	orl	$32, 8(%r12)
	movw	%dx, -2(%rax,%r13)
	movq	%rax, 72(%r12)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%r13, %r12
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L679:
	cmpq	%rdx, %rbx
	ja	.L680
	movslq	12(%rbx), %rax
	addq	%rbx, %rax
	cmpq	%rax, %rdx
	jnb	.L680
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 48(%r12)
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L677:
	cmpq	%rdx, %rbx
	ja	.L678
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L678
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 96(%r12)
	movq	64(%rbx), %rax
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L675:
	cmpq	%rdx, %rbx
	ja	.L676
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L676
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 88(%r12)
	movq	64(%rbx), %rax
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L673:
	cmpq	%rdx, %rbx
	ja	.L674
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L674
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 80(%r12)
	movq	64(%rbx), %rax
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L671:
	cmpq	%rdx, %rbx
	ja	.L672
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L672
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 72(%r12)
	movq	64(%rbx), %rax
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L723:
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	memset@PLT
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L658
	movq	64(%r12), %r9
	movl	4(%r12), %r13d
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L661:
	movl	$144, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L691
	movq	%r13, %rax
	movl	$18, %ecx
	movq	%r12, %rdi
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
.L690:
	leaq	144(%r12), %rax
	movl	%r14d, 24(%r12)
	movslq	%r14d, %rdx
	movq	%rax, 64(%r12)
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L696:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L684:
	movzwl	(%rbx,%rdx,2), %esi
	movw	%si, (%rax,%rdx,2)
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	jne	.L684
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L721:
	movq	64(%r13), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r13)
	jmp	.L667
.L722:
	movl	$7, (%r15)
	movq	%r13, %r12
	jmp	.L658
.L724:
	movl	$7, (%r15)
	jmp	.L658
.L691:
	movl	$7, (%r15)
	xorl	%r12d, %r12d
	jmp	.L658
	.cfi_endproc
.LFE2363:
	.size	ucstrTextClone, .-ucstrTextClone
	.p2align 4
	.type	repTextClone, @function
repTextClone:
.LFB2346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movl	%edx, -52(%rbp)
	testl	%r8d, %r8d
	jg	.L725
	movl	24(%rsi), %r14d
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%rcx, %r15
	testq	%rdi, %rdi
	je	.L772
	cmpl	$878368812, (%rdi)
	je	.L732
	movl	$1, (%rcx)
	movq	%rdi, %r12
.L725:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	movl	4(%rdi), %eax
	testb	$4, %al
	je	.L733
	movq	56(%rdi), %rdx
	movq	80(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L733
	call	*%rdx
	movl	4(%r13), %eax
.L733:
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 4(%r13)
	movslq	24(%r13), %rdx
	cmpl	%edx, %r14d
	jle	.L754
	testb	$2, %al
	jne	.L773
.L734:
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.L774
	orl	$2, 4(%r13)
	movslq	%r14d, %rdx
	movq	%r13, %r12
	movl	%r14d, 24(%r13)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L772:
	testl	%r14d, %r14d
	jle	.L728
	leal	144(%r14), %esi
	movslq	%esi, %rdi
	movl	%esi, -64(%rbp)
	call	uprv_malloc_67@PLT
	movl	-64(%rbp), %esi
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L750
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r13, %rax
	xorl	%edx, %edx
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
	testl	%esi, %esi
	jg	.L749
	.p2align 4,,10
	.p2align 3
.L731:
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L725
	movl	4(%r12), %r13d
	movq	64(%r12), %r9
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r12)
	movl	$0, 28(%r12)
	orl	$4, %r13d
	movups	%xmm0, 72(%r12)
	movl	%r13d, 4(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 88(%r12)
	testq	%r9, %r9
	je	.L736
	testl	%edx, %edx
	jg	.L775
.L736:
	movl	12(%rbx), %edx
	cmpl	%edx, 12(%r12)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	cmovle	12(%r12), %edx
	movq	%r9, -64(%rbp)
	movslq	%edx, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	movl	%r13d, 4(%r12)
	movq	%r9, 64(%r12)
	testl	%r14d, %r14d
	jle	.L737
	movq	64(%rbx), %rsi
	movslq	%r14d, %rdx
	movq	%r9, %rdi
	call	memcpy@PLT
.L737:
	movq	72(%r12), %rdx
	movq	64(%rbx), %rax
	cmpq	%rax, %rdx
	jb	.L738
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L738
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 72(%r12)
	movq	64(%rbx), %rax
.L739:
	movq	80(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L740
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L740
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 80(%r12)
	movq	64(%rbx), %rax
.L741:
	movq	88(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L742
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L742
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 88(%r12)
	movq	64(%rbx), %rax
.L743:
	movq	96(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L744
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L744
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 96(%r12)
	movq	64(%rbx), %rax
.L745:
	movq	48(%r12), %rdx
	cmpq	%rdx, %rax
	ja	.L746
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L746
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 48(%r12)
.L747:
	andl	$-33, 8(%r12)
	cmpb	$0, -52(%rbp)
	je	.L725
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L725
	movq	72(%rbx), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	orl	$40, 8(%r12)
	movq	%rax, 72(%r12)
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L754:
	movq	%r13, %r12
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L746:
	cmpq	%rdx, %rbx
	ja	.L747
	movslq	12(%rbx), %rax
	addq	%rbx, %rax
	cmpq	%rax, %rdx
	jnb	.L747
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 48(%r12)
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L744:
	cmpq	%rdx, %rbx
	ja	.L745
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L745
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 96(%r12)
	movq	64(%rbx), %rax
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L742:
	cmpq	%rdx, %rbx
	ja	.L743
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L743
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 88(%r12)
	movq	64(%rbx), %rax
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L740:
	cmpq	%rdx, %rbx
	ja	.L741
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L741
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 80(%r12)
	movq	64(%rbx), %rax
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L738:
	cmpq	%rdx, %rbx
	ja	.L739
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L739
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 72(%r12)
	movq	64(%rbx), %rax
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L775:
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	memset@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L725
	movq	64(%r12), %r9
	movl	4(%r12), %r13d
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L728:
	movl	$144, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L750
	movq	%r13, %rax
	movl	$18, %ecx
	movq	%r12, %rdi
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
.L749:
	leaq	144(%r12), %rax
	movl	%r14d, 24(%r12)
	movslq	%r14d, %rdx
	movq	%rax, 64(%r12)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L773:
	movq	64(%r13), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r13)
	jmp	.L734
.L750:
	movl	$7, (%r15)
	xorl	%r12d, %r12d
	jmp	.L725
.L774:
	movl	$7, (%r15)
	movq	%r13, %r12
	jmp	.L725
	.cfi_endproc
.LFE2346:
	.size	repTextClone, .-repTextClone
	.p2align 4
	.type	unistrTextClone, @function
unistrTextClone:
.LFB2354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movl	%edx, -52(%rbp)
	testl	%r8d, %r8d
	jg	.L776
	movl	24(%rsi), %r14d
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%rcx, %r15
	testq	%rdi, %rdi
	je	.L827
	cmpl	$878368812, (%rdi)
	je	.L783
	movl	$1, (%rcx)
	movq	%rdi, %r12
.L776:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore_state
	movl	4(%rdi), %eax
	testb	$4, %al
	je	.L784
	movq	56(%rdi), %rdx
	movq	80(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L784
	call	*%rdx
	movl	4(%r13), %eax
.L784:
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 4(%r13)
	movslq	24(%r13), %rdx
	cmpl	%edx, %r14d
	jle	.L806
	testb	$2, %al
	jne	.L828
.L785:
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.L829
	orl	$2, 4(%r13)
	movslq	%r14d, %rdx
	movq	%r13, %r12
	movl	%r14d, 24(%r13)
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L827:
	testl	%r14d, %r14d
	jle	.L779
	leal	144(%r14), %esi
	movslq	%esi, %rdi
	movl	%esi, -64(%rbp)
	call	uprv_malloc_67@PLT
	movl	-64(%rbp), %esi
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L802
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r13, %rax
	xorl	%edx, %edx
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
	testl	%esi, %esi
	jg	.L801
	.p2align 4,,10
	.p2align 3
.L782:
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L776
	movl	4(%r12), %r13d
	movq	64(%r12), %r9
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r12)
	movl	$0, 28(%r12)
	orl	$4, %r13d
	movups	%xmm0, 72(%r12)
	movl	%r13d, 4(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 88(%r12)
	testq	%r9, %r9
	je	.L787
	testl	%edx, %edx
	jg	.L830
.L787:
	movl	12(%rbx), %edx
	cmpl	%edx, 12(%r12)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	cmovle	12(%r12), %edx
	movq	%r9, -64(%rbp)
	movslq	%edx, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	movl	%r13d, 4(%r12)
	movq	%r9, 64(%r12)
	testl	%r14d, %r14d
	jle	.L788
	movq	64(%rbx), %rsi
	movslq	%r14d, %rdx
	movq	%r9, %rdi
	call	memcpy@PLT
.L788:
	movq	72(%r12), %rdx
	movq	64(%rbx), %rax
	cmpq	%rax, %rdx
	jb	.L789
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L789
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 72(%r12)
	movq	64(%rbx), %rax
.L790:
	movq	80(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L791
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L791
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 80(%r12)
	movq	64(%rbx), %rax
.L792:
	movq	88(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L793
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L793
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 88(%r12)
	movq	64(%rbx), %rax
.L794:
	movq	96(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L795
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L795
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 96(%r12)
	movq	64(%rbx), %rax
.L796:
	movq	48(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L797
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L797
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 48(%r12)
.L798:
	andl	$-33, 8(%r12)
	cmpb	$0, -52(%rbp)
	je	.L776
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L776
	movl	$64, %edi
	movq	72(%rbx), %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L800
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L800:
	orl	$40, 8(%r12)
	movq	%rbx, 72(%r12)
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L806:
	movq	%r13, %r12
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L797:
	cmpq	%rdx, %rbx
	ja	.L798
	movslq	12(%rbx), %rax
	addq	%rbx, %rax
	cmpq	%rax, %rdx
	jnb	.L798
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 48(%r12)
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L795:
	cmpq	%rdx, %rbx
	ja	.L796
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L796
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 96(%r12)
	movq	64(%rbx), %rax
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L793:
	cmpq	%rdx, %rbx
	ja	.L794
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L794
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 88(%r12)
	movq	64(%rbx), %rax
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L791:
	cmpq	%rdx, %rbx
	ja	.L792
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L792
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 80(%r12)
	movq	64(%rbx), %rax
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L789:
	cmpq	%rdx, %rbx
	ja	.L790
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L790
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 72(%r12)
	movq	64(%rbx), %rax
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L830:
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	memset@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L776
	movq	64(%r12), %r9
	movl	4(%r12), %r13d
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L779:
	movl	$144, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L802
	movq	%r13, %rax
	movl	$18, %ecx
	movq	%r12, %rdi
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
.L801:
	leaq	144(%r12), %rax
	movl	%r14d, 24(%r12)
	movslq	%r14d, %rdx
	movq	%rax, 64(%r12)
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L828:
	movq	64(%r13), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r13)
	jmp	.L785
.L802:
	movl	$7, (%r15)
	xorl	%r12d, %r12d
	jmp	.L776
.L829:
	movl	$7, (%r15)
	movq	%r13, %r12
	jmp	.L776
	.cfi_endproc
.LFE2354:
	.size	unistrTextClone, .-unistrTextClone
	.p2align 4
	.type	utf8TextClone, @function
utf8TextClone:
.LFB2343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movl	%edx, -52(%rbp)
	testl	%r8d, %r8d
	jg	.L831
	movl	24(%rsi), %r14d
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%rcx, %r15
	testq	%rdi, %rdi
	je	.L879
	cmpl	$878368812, (%rdi)
	je	.L838
	movl	$1, (%rcx)
	movq	%rdi, %r12
.L831:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L838:
	.cfi_restore_state
	movl	4(%rdi), %eax
	testb	$4, %al
	je	.L839
	movq	56(%rdi), %rdx
	movq	80(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L839
	call	*%rdx
	movl	4(%r13), %eax
.L839:
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 4(%r13)
	movslq	24(%r13), %rdx
	cmpl	%edx, %r14d
	jle	.L861
	testb	$2, %al
	jne	.L880
.L840:
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.L881
	orl	$2, 4(%r13)
	movslq	%r14d, %rdx
	movq	%r13, %r12
	movl	%r14d, 24(%r13)
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L879:
	testl	%r14d, %r14d
	jle	.L834
	leal	144(%r14), %esi
	movslq	%esi, %rdi
	movl	%esi, -64(%rbp)
	call	uprv_malloc_67@PLT
	movl	-64(%rbp), %esi
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L857
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r13, %rax
	xorl	%edx, %edx
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
	testl	%esi, %esi
	jg	.L856
	.p2align 4,,10
	.p2align 3
.L837:
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L831
	movl	4(%r12), %r13d
	movq	64(%r12), %r9
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r12)
	movl	$0, 28(%r12)
	orl	$4, %r13d
	movups	%xmm0, 72(%r12)
	movl	%r13d, 4(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 88(%r12)
	testq	%r9, %r9
	je	.L842
	testl	%edx, %edx
	jg	.L882
.L842:
	movl	12(%rbx), %edx
	cmpl	%edx, 12(%r12)
	movq	%rbx, %rsi
	movq	%r12, %rdi
	cmovle	12(%r12), %edx
	movq	%r9, -64(%rbp)
	movslq	%edx, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	movl	%r13d, 4(%r12)
	movq	%r9, 64(%r12)
	testl	%r14d, %r14d
	jle	.L843
	movq	64(%rbx), %rsi
	movslq	%r14d, %rdx
	movq	%r9, %rdi
	call	memcpy@PLT
.L843:
	movq	72(%r12), %rdx
	movq	64(%rbx), %rax
	cmpq	%rax, %rdx
	jb	.L844
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L844
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 72(%r12)
	movq	64(%rbx), %rax
.L845:
	movq	80(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L846
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L846
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 80(%r12)
	movq	64(%rbx), %rax
.L847:
	movq	88(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L848
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L848
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 88(%r12)
	movq	64(%rbx), %rax
.L849:
	movq	96(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L850
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L850
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 96(%r12)
	movq	64(%rbx), %rax
.L851:
	movq	48(%r12), %rdx
	cmpq	%rax, %rdx
	jb	.L852
	movslq	24(%rbx), %rcx
	addq	%rax, %rcx
	cmpq	%rcx, %rdx
	jnb	.L852
	subq	%rax, %rdx
	addq	64(%r12), %rdx
	movq	%rdx, 48(%r12)
.L853:
	andl	$-33, 8(%r12)
	cmpb	$0, -52(%rbp)
	je	.L831
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L831
	movq	56(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	leal	1(%rax), %r13d
	movslq	%r13d, %r13
	movq	%r13, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L883
	movq	72(%rbx), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	orl	$32, 8(%r12)
	movq	%rax, 72(%r12)
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L861:
	movq	%r13, %r12
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L852:
	cmpq	%rdx, %rbx
	ja	.L853
	movslq	12(%rbx), %rax
	addq	%rbx, %rax
	cmpq	%rax, %rdx
	jnb	.L853
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 48(%r12)
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L850:
	cmpq	%rdx, %rbx
	ja	.L851
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L851
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 96(%r12)
	movq	64(%rbx), %rax
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L848:
	cmpq	%rdx, %rbx
	ja	.L849
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L849
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 88(%r12)
	movq	64(%rbx), %rax
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L846:
	cmpq	%rdx, %rbx
	ja	.L847
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L847
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 80(%r12)
	movq	64(%rbx), %rax
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L844:
	cmpq	%rdx, %rbx
	ja	.L845
	movslq	12(%rbx), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L845
	subq	%rbx, %rdx
	addq	%r12, %rdx
	movq	%rdx, 72(%r12)
	movq	64(%rbx), %rax
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L882:
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	memset@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L831
	movq	64(%r12), %r9
	movl	4(%r12), %r13d
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L834:
	movl	$144, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L857
	movq	%r13, %rax
	movl	$18, %ecx
	movq	%r12, %rdi
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
.L856:
	leaq	144(%r12), %rax
	movl	%r14d, 24(%r12)
	movslq	%r14d, %rdx
	movq	%rax, 64(%r12)
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L880:
	movq	64(%r13), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r13)
	jmp	.L840
.L883:
	movl	$7, (%r15)
	jmp	.L831
.L857:
	movl	$7, (%r15)
	xorl	%r12d, %r12d
	jmp	.L831
.L881:
	movl	$7, (%r15)
	movq	%r13, %r12
	jmp	.L831
	.cfi_endproc
.LFE2343:
	.size	utf8TextClone, .-utf8TextClone
	.p2align 4
	.globl	utext_nativeLength_67
	.type	utext_nativeLength_67, @function
utext_nativeLength_67:
.LFB2312:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE2312:
	.size	utext_nativeLength_67, .-utext_nativeLength_67
	.p2align 4
	.globl	utext_isLengthExpensive_67
	.type	utext_isLengthExpensive_67, @function
utext_isLengthExpensive_67:
.LFB2313:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	shrl	%eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE2313:
	.size	utext_isLengthExpensive_67, .-utext_isLengthExpensive_67
	.p2align 4
	.globl	utext_getNativeIndex_67
	.type	utext_getNativeIndex_67, @function
utext_getNativeIndex_67:
.LFB2314:
	.cfi_startproc
	endbr64
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jle	.L890
	movq	56(%rdi), %rax
	jmp	*64(%rax)
	.p2align 4,,10
	.p2align 3
.L890:
	addq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE2314:
	.size	utext_getNativeIndex_67, .-utext_getNativeIndex_67
	.p2align 4
	.globl	utext_setNativeIndex_67
	.type	utext_setNativeIndex_67, @function
utext_setNativeIndex_67:
.LFB2315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rax
	cmpq	%rsi, %rax
	jg	.L892
	cmpq	%rsi, 16(%rdi)
	jg	.L893
.L892:
	movq	56(%rbx), %rax
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*32(%rax)
	movl	40(%rbx), %eax
.L894:
	cmpl	%eax, 44(%rbx)
	jle	.L891
	movq	48(%rbx), %rcx
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L902
.L891:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	movl	%esi, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	cmpl	28(%rdi), %ecx
	jg	.L895
	movl	%eax, 40(%rbx)
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L902:
	testl	%eax, %eax
	jne	.L899
	movq	56(%rbx), %rax
	movq	32(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	*32(%rax)
	movl	40(%rbx), %eax
.L899:
	testl	%eax, %eax
	jle	.L891
	movq	48(%rbx), %rcx
	movslq	%eax, %rdx
	movzwl	-2(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L891
	subl	$1, %eax
	movl	%eax, 40(%rbx)
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L895:
	movq	56(%rdi), %rax
	call	*72(%rax)
	movl	%eax, 40(%rbx)
	jmp	.L894
	.cfi_endproc
.LFE2315:
	.size	utext_setNativeIndex_67, .-utext_setNativeIndex_67
	.p2align 4
	.type	charIterTextClone, @function
charIterTextClone:
.LFB2372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L903
	movq	%rcx, %rbx
	testb	%dl, %dl
	jne	.L913
	movq	%rdi, %r15
	movq	72(%rsi), %rdi
	movq	%rsi, %r13
	movq	(%rdi), %rax
	call	*64(%rax)
	movl	(%rbx), %ecx
	movq	%rax, %r14
	testl	%ecx, %ecx
	jg	.L903
	movl	16(%rax), %edx
	testl	%edx, %edx
	jg	.L913
	movq	%rbx, %rdx
	movl	$64, %esi
	movq	%r15, %rdi
	call	utext_setup_67.part.0
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L914
.L903:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L914:
	.cfi_restore_state
	leaq	_ZL13charIterFuncs(%rip), %rax
	movq	%r14, 72(%r12)
	movq	%rax, 56(%r12)
	movl	$0, 8(%r12)
	movslq	20(%r14), %rax
	movq	$-1, 120(%r12)
	movq	%rax, 112(%r12)
	movq	64(%r12), %rax
	movq	$1, 40(%r12)
	movslq	40(%r13), %rsi
	leaq	32(%rax), %rdx
	movq	%rax, 80(%r12)
	movq	%rdx, 88(%r12)
	movq	%rax, 48(%r12)
	movq	$0, 16(%r12)
	movl	$1, 28(%r12)
	movq	$-1, 32(%r12)
	cmpl	28(%r13), %esi
	jle	.L915
	movq	56(%r13), %rax
	movq	%r13, %rdi
	call	*64(%rax)
	movq	%rax, %rsi
.L909:
	movq	%r12, %rdi
	call	utext_setNativeIndex_67
	movq	%r14, 96(%r12)
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L913:
	movl	$16, (%rbx)
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L915:
	addq	32(%r13), %rsi
	jmp	.L909
	.cfi_endproc
.LFE2372:
	.size	charIterTextClone, .-charIterTextClone
	.p2align 4
	.globl	utext_current32_67
	.type	utext_current32_67, @function
utext_current32_67:
.LFB2317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	40(%rdi), %r12d
	cmpl	44(%rdi), %r12d
	je	.L931
.L917:
	movq	48(%rbx), %rdx
	movslq	%r12d, %rax
	leaq	(%rax,%rax), %rcx
	movzwl	(%rdx,%rax,2), %r13d
	movl	%r13d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L916
	leal	1(%r12), %eax
	cmpl	44(%rbx), %eax
	jge	.L920
	movzwl	2(%rdx,%rcx), %r14d
.L921:
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L932
.L916:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L931:
	.cfi_restore_state
	movq	56(%rdi), %rax
	movq	16(%rdi), %rsi
	movl	$1, %edx
	call	*32(%rax)
	testb	%al, %al
	je	.L923
	movl	40(%rbx), %r12d
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L920:
	movq	16(%rbx), %r15
	movq	56(%rbx), %rax
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	*32(%rax)
	testb	%al, %al
	je	.L922
	movslq	40(%rbx), %rdx
	movq	48(%rbx), %rax
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movzwl	(%rax,%rdx,2), %r14d
	movq	56(%rbx), %rax
	xorl	%edx, %edx
	call	*32(%rax)
	movl	%r12d, 40(%rbx)
	testb	%al, %al
	jne	.L921
.L923:
	movl	$-1, %r13d
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L932:
	sall	$10, %r13d
	leal	-56613888(%r14,%r13), %r13d
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L922:
	movq	56(%rbx), %rax
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*32(%rax)
	movl	%r12d, 40(%rbx)
	testb	%al, %al
	jne	.L916
	jmp	.L923
	.cfi_endproc
.LFE2317:
	.size	utext_current32_67, .-utext_current32_67
	.p2align 4
	.globl	utext_char32At_67
	.type	utext_char32At_67, @function
utext_char32At_67:
.LFB2318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rsi
	cmpq	%rbx, %rsi
	jg	.L944
	movslq	28(%rdi), %rax
	movq	%rax, %rdx
	addq	%rsi, %rax
	cmpq	%rbx, %rax
	jg	.L949
	movl	$-1, %r13d
.L935:
	cmpq	16(%r12), %rbx
	jge	.L934
	movl	%ebx, %eax
	subl	%esi, %eax
	cmpl	%eax, %edx
	jl	.L938
	movl	%eax, 40(%r12)
	cmpl	%eax, 44(%r12)
	jg	.L950
.L933:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L949:
	.cfi_restore_state
	movl	%ebx, %eax
	movq	48(%rdi), %rcx
	subl	%esi, %eax
	movl	%eax, 40(%rdi)
	cltq
	movzwl	(%rcx,%rax,2), %r13d
	movl	%r13d, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L935
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore_state
	movl	$-1, %r13d
.L934:
	movq	56(%r12), %rax
	movq	%rbx, %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	*32(%rax)
	movl	40(%r12), %eax
	movq	32(%r12), %rsi
.L937:
	cmpl	%eax, 44(%r12)
	jle	.L933
.L950:
	movq	48(%r12), %rcx
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L951
	cmpq	%rsi, %rbx
	jl	.L933
.L943:
	movl	%edx, %eax
	movl	%edx, %r13d
	andl	$-2048, %eax
	cmpl	$55296, %eax
	jne	.L933
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	utext_current32_67
	.p2align 4,,10
	.p2align 3
.L938:
	.cfi_restore_state
	movq	56(%r12), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*72(%rax)
	movq	32(%r12), %rsi
	movl	%eax, 40(%r12)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L951:
	testl	%eax, %eax
	jne	.L940
	movq	56(%r12), %rax
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	*32(%rax)
	movq	32(%r12), %rsi
	movl	40(%r12), %eax
.L940:
	testl	%eax, %eax
	jle	.L941
	movq	48(%r12), %rcx
	movslq	%eax, %rdx
	movzwl	-2(%rcx,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L941
	subl	$1, %eax
	movl	%eax, 40(%r12)
.L941:
	cmpq	%rsi, %rbx
	jl	.L933
	cmpl	%eax, 44(%r12)
	jle	.L933
	movq	48(%r12), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %edx
	jmp	.L943
	.cfi_endproc
.LFE2318:
	.size	utext_char32At_67, .-utext_char32At_67
	.p2align 4
	.globl	utext_next32_67
	.type	utext_next32_67, @function
utext_next32_67:
.LFB2319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movslq	40(%rdi), %rax
	cmpl	44(%rdi), %eax
	jl	.L953
	movq	56(%rdi), %rax
	movq	16(%rdi), %rsi
	movl	$1, %edx
	call	*32(%rax)
	testb	%al, %al
	je	.L956
	movslq	40(%rbx), %rax
.L953:
	movq	48(%rbx), %rcx
	leal	1(%rax), %edx
	movl	%edx, 40(%rbx)
	movzwl	(%rcx,%rax,2), %r12d
	movl	%r12d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L952
	cmpl	44(%rbx), %edx
	jl	.L955
	movq	56(%rbx), %rax
	movq	16(%rbx), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L952
	movq	48(%rbx), %rcx
	movl	40(%rbx), %edx
.L955:
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L952
	addl	$1, %edx
	sall	$10, %r12d
	movl	%edx, 40(%rbx)
	leal	-56613888(%rax,%r12), %r12d
.L952:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L956:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L952
	.cfi_endproc
.LFE2319:
	.size	utext_next32_67, .-utext_next32_67
	.p2align 4
	.globl	utext_previous32_67
	.type	utext_previous32_67, @function
utext_previous32_67:
.LFB2320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	40(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	jle	.L970
.L962:
	subl	$1, %eax
	movq	48(%rbx), %rcx
	movslq	%eax, %rdx
	movl	%eax, 40(%rbx)
	movzwl	(%rcx,%rdx,2), %r12d
	leaq	(%rdx,%rdx), %rsi
	movl	%r12d, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L961
	testl	%eax, %eax
	jle	.L971
.L964:
	movzwl	-2(%rcx,%rsi), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L961
	subl	$1, %eax
	sall	$10, %edx
	movl	%eax, 40(%rbx)
	leal	-56613888(%r12,%rdx), %r12d
.L961:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L970:
	.cfi_restore_state
	movq	56(%rdi), %rax
	xorl	%edx, %edx
	movq	32(%rdi), %rsi
	call	*32(%rax)
	testb	%al, %al
	je	.L965
	movl	40(%rbx), %eax
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L971:
	movq	56(%rbx), %rax
	xorl	%edx, %edx
	movq	32(%rbx), %rsi
	movq	%rbx, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L961
	movslq	40(%rbx), %rsi
	movq	48(%rbx), %rcx
	movq	%rsi, %rax
	addq	%rsi, %rsi
	jmp	.L964
.L965:
	movl	$-1, %r12d
	jmp	.L961
	.cfi_endproc
.LFE2320:
	.size	utext_previous32_67, .-utext_previous32_67
	.p2align 4
	.globl	utext_moveIndex32_67
	.type	utext_moveIndex32_67, @function
utext_moveIndex32_67:
.LFB2311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testl	%esi, %esi
	jg	.L980
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L977:
	addl	$1, %eax
	movl	%eax, 40(%rbx)
.L979:
	subl	$1, %r12d
	je	.L981
.L980:
	movl	40(%rbx), %eax
	cmpl	44(%rbx), %eax
	jl	.L974
	movq	56(%rbx), %rax
	movq	16(%rbx), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L978
	movl	40(%rbx), %eax
.L974:
	movq	48(%rbx), %rcx
	movslq	%eax, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	andl	$-2048, %edx
	cmpl	$55296, %edx
	jne	.L977
	movq	%rbx, %rdi
	call	utext_next32_67
	cmpl	$-1, %eax
	jne	.L979
.L978:
	xorl	%eax, %eax
.L1002:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L981:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L973:
	.cfi_restore_state
	je	.L981
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L982:
	movq	48(%rbx), %rcx
	movslq	%eax, %rdx
	movzwl	-2(%rcx,%rdx,2), %edx
	andl	$-2048, %edx
	cmpl	$55296, %edx
	je	.L1001
	subl	$1, %eax
	movl	%eax, 40(%rbx)
.L984:
	addl	$1, %r12d
	je	.L981
.L985:
	movl	40(%rbx), %eax
	testl	%eax, %eax
	jg	.L982
	movq	56(%rbx), %rax
	xorl	%edx, %edx
	movq	32(%rbx), %rsi
	movq	%rbx, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L978
	movl	40(%rbx), %eax
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	%rbx, %rdi
	call	utext_previous32_67
	cmpl	$-1, %eax
	jne	.L984
	xorl	%eax, %eax
	jmp	.L1002
	.cfi_endproc
.LFE2311:
	.size	utext_moveIndex32_67, .-utext_moveIndex32_67
	.p2align 4
	.globl	utext_getPreviousNativeIndex_67
	.type	utext_getPreviousNativeIndex_67, @function
utext_getPreviousNativeIndex_67:
.LFB2316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	40(%rdi), %edx
	movq	%rdi, %rbx
	movl	%edx, %esi
	subl	$1, %esi
	js	.L1004
	movq	48(%rdi), %rax
	movslq	%esi, %r12
	movzwl	(%rax,%r12,2), %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L1004
	cmpl	%esi, 28(%rdi)
	jl	.L1005
	addq	32(%rdi), %r12
.L1003:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1004:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L1007
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L1003
.L1007:
	movq	%rbx, %rdi
	call	utext_previous32_67
	movslq	40(%rbx), %rdx
	cmpl	28(%rbx), %edx
	jle	.L1025
	movq	56(%rbx), %rax
	movq	%rbx, %rdi
	call	*64(%rax)
	movslq	40(%rbx), %rdx
	movq	%rax, %r12
.L1009:
	cmpl	%edx, 44(%rbx)
	jg	.L1010
	movq	56(%rbx), %rax
	movq	16(%rbx), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L1003
	movslq	40(%rbx), %rdx
.L1010:
	movq	48(%rbx), %rsi
	leal	1(%rdx), %ecx
	movl	%ecx, 40(%rbx)
	movzwl	(%rsi,%rdx,2), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L1003
	cmpl	44(%rbx), %ecx
	jl	.L1011
	movq	56(%rbx), %rax
	movq	16(%rbx), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L1003
	movq	48(%rbx), %rsi
	movl	40(%rbx), %ecx
.L1011:
	movslq	%ecx, %rax
	movzwl	(%rsi,%rax,2), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L1003
	addl	$1, %ecx
	movq	%r12, %rax
	movl	%ecx, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	movslq	%edx, %r12
	addq	32(%rbx), %r12
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	56(%rdi), %rax
	movl	%esi, 40(%rdi)
	call	*64(%rax)
	addl	$1, 40(%rbx)
	popq	%rbx
	movq	%rax, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2316:
	.size	utext_getPreviousNativeIndex_67, .-utext_getPreviousNativeIndex_67
	.p2align 4
	.globl	utext_next32From_67
	.type	utext_next32From_67, @function
utext_next32From_67:
.LFB2321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	32(%rdi), %rax
	movq	%rdi, %r12
	cmpq	%rsi, %rax
	jg	.L1027
	cmpq	%rsi, 16(%rdi)
	jg	.L1028
.L1027:
	movq	56(%r12), %rax
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L1032
	movl	40(%r12), %eax
.L1030:
	movq	48(%r12), %rdx
	leal	1(%rax), %ecx
	cltq
	movl	%ecx, 40(%r12)
	movzwl	(%rdx,%rax,2), %eax
	movl	%eax, %edx
	andl	$-2048, %edx
	cmpl	$55296, %edx
	je	.L1034
.L1026:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1028:
	.cfi_restore_state
	movq	%rsi, %rcx
	movslq	28(%rdi), %rdx
	subq	%rax, %rcx
	cmpq	%rdx, %rcx
	jg	.L1031
	movl	%esi, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	utext_setNativeIndex_67
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	utext_next32_67
	.p2align 4,,10
	.p2align 3
.L1031:
	.cfi_restore_state
	movq	56(%rdi), %rax
	call	*72(%rax)
	jmp	.L1030
.L1032:
	movl	$-1, %eax
	jmp	.L1026
	.cfi_endproc
.LFE2321:
	.size	utext_next32From_67, .-utext_next32From_67
	.p2align 4
	.globl	utext_previous32From_67
	.type	utext_previous32From_67, @function
utext_previous32From_67:
.LFB2322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	32(%rdi), %rax
	movq	%rdi, %r12
	cmpq	%rsi, %rax
	jge	.L1036
	cmpq	%rsi, 16(%rdi)
	jge	.L1037
.L1036:
	movq	56(%r12), %rax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	testb	%al, %al
	je	.L1048
	movl	40(%r12), %eax
.L1040:
	movq	48(%r12), %rdx
	subl	$1, %eax
	movl	%eax, 40(%r12)
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%eax, %edx
	andl	$-2048, %edx
	cmpl	$55296, %edx
	je	.L1049
.L1035:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1037:
	.cfi_restore_state
	movq	%rsi, %rcx
	movslq	28(%rdi), %rdx
	subq	%rax, %rcx
	cmpq	%rdx, %rcx
	jg	.L1041
	movl	%esi, %ecx
	subl	%eax, %ecx
	movl	%ecx, %eax
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	utext_setNativeIndex_67
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	utext_previous32_67
	.p2align 4,,10
	.p2align 3
.L1041:
	.cfi_restore_state
	movq	56(%rdi), %rax
	call	*72(%rax)
	movl	%eax, 40(%r12)
	testl	%eax, %eax
	jne	.L1040
	jmp	.L1036
.L1048:
	movl	$-1, %eax
	jmp	.L1035
	.cfi_endproc
.LFE2322:
	.size	utext_previous32From_67, .-utext_previous32From_67
	.p2align 4
	.globl	utext_extract_67
	.type	utext_extract_67, @function
utext_extract_67:
.LFB2323:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE2323:
	.size	utext_extract_67, .-utext_extract_67
	.p2align 4
	.globl	utext_equals_67
	.type	utext_equals_67, @function
utext_equals_67:
.LFB2324:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1057
	testq	%rsi, %rsi
	sete	%dl
	je	.L1057
	cmpl	$878368812, (%rdi)
	je	.L1066
.L1063:
	ret
	.p2align 4,,10
	.p2align 3
.L1066:
	cmpl	$878368812, (%rsi)
	movl	%edx, %eax
	jne	.L1063
	movq	56(%rdi), %rcx
	cmpq	56(%rsi), %rcx
	jne	.L1063
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	72(%rsi), %rbx
	cmpq	%rbx, 72(%rdi)
	je	.L1067
.L1051:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1057:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L1067:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movslq	40(%rdi), %rax
	cmpl	28(%rdi), %eax
	jle	.L1068
	movq	%rsi, -24(%rbp)
	call	*64(%rcx)
	movq	-24(%rbp), %rsi
	movq	%rax, %rbx
.L1054:
	movslq	40(%rsi), %rax
	cmpl	28(%rsi), %eax
	jle	.L1069
	movq	56(%rsi), %rax
	movq	%rsi, %rdi
	call	*64(%rax)
.L1056:
	cmpq	%rbx, %rax
	sete	%al
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1068:
	addq	32(%rdi), %rax
	movq	%rax, %rbx
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1069:
	addq	32(%rsi), %rax
	jmp	.L1056
	.cfi_endproc
.LFE2324:
	.size	utext_equals_67, .-utext_equals_67
	.p2align 4
	.globl	utext_isWritable_67
	.type	utext_isWritable_67, @function
utext_isWritable_67:
.LFB2325:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	shrl	$3, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE2325:
	.size	utext_isWritable_67, .-utext_isWritable_67
	.p2align 4
	.globl	utext_freeze_67
	.type	utext_freeze_67, @function
utext_freeze_67:
.LFB2326:
	.cfi_startproc
	endbr64
	andl	$-9, 8(%rdi)
	ret
	.cfi_endproc
.LFE2326:
	.size	utext_freeze_67, .-utext_freeze_67
	.p2align 4
	.globl	utext_hasMetaData_67
	.type	utext_hasMetaData_67, @function
utext_hasMetaData_67:
.LFB2327:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	shrl	$4, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE2327:
	.size	utext_hasMetaData_67, .-utext_hasMetaData_67
	.p2align 4
	.globl	utext_replace_67
	.type	utext_replace_67, @function
utext_replace_67:
.LFB2328:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1073
	testb	$8, 8(%rdi)
	je	.L1077
	movq	56(%rdi), %rax
	jmp	*48(%rax)
	.p2align 4,,10
	.p2align 3
.L1077:
	movl	$30, (%r9)
.L1073:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2328:
	.size	utext_replace_67, .-utext_replace_67
	.p2align 4
	.globl	utext_copy_67
	.type	utext_copy_67, @function
utext_copy_67:
.LFB2329:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1078
	testb	$8, 8(%rdi)
	je	.L1081
	movq	56(%rdi), %rax
	movsbl	%r8b, %r8d
	movq	56(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1078:
	ret
	.p2align 4,,10
	.p2align 3
.L1081:
	movl	$30, (%r9)
	ret
	.cfi_endproc
.LFE2329:
	.size	utext_copy_67, .-utext_copy_67
	.p2align 4
	.globl	utext_clone_67
	.type	utext_clone_67, @function
utext_clone_67:
.LFB2330:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movl	(%r8), %edi
	testl	%edi, %edi
	jg	.L1087
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%dl, %edx
	movq	%rax, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%r8, %rbx
	movq	56(%rsi), %r8
	movq	%rbx, %rcx
	call	*16(%r8)
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1082
	testq	%rax, %rax
	je	.L1090
	testb	%r12b, %r12b
	je	.L1082
	andl	$-9, 8(%rax)
.L1082:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1087:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L1090:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$7, (%rbx)
	jmp	.L1082
	.cfi_endproc
.LFE2330:
	.size	utext_clone_67, .-utext_clone_67
	.p2align 4
	.globl	utext_setup_67
	.type	utext_setup_67, @function
utext_setup_67:
.LFB2331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L1111
	movl	%esi, %r13d
	movq	%rdx, %rbx
	testq	%rdi, %rdi
	je	.L1125
	cmpl	$878368812, (%rdi)
	je	.L1097
	movl	$1, (%rdx)
.L1111:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1097:
	.cfi_restore_state
	movl	4(%rdi), %eax
	testb	$4, %al
	je	.L1098
	movq	56(%rdi), %rdx
	movq	80(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1098
	call	*%rdx
	movl	4(%r12), %eax
.L1098:
	movl	%eax, %edx
	andl	$-5, %edx
	movl	%edx, 4(%r12)
	movl	24(%r12), %edx
	cmpl	%edx, %r13d
	jle	.L1105
	testb	$2, %al
	jne	.L1126
.L1099:
	movslq	%r13d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r12)
	testq	%rax, %rax
	je	.L1124
	orl	$2, 4(%r12)
	movl	%r13d, 24(%r12)
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1125:
	testl	%esi, %esi
	jle	.L1094
	leal	144(%rsi), %r14d
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1124
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r12, %rax
	movq	%rdx, %r12
	rep stosq
	movl	$144, 12(%rdx)
	movabsq	$5173336108, %rax
	movq	%rax, (%rdx)
	testl	%r14d, %r14d
	jg	.L1101
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1096:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1111
	movq	64(%r12), %rdi
	pxor	%xmm0, %xmm0
	orl	$4, 4(%r12)
	movq	$0, 16(%r12)
	movl	$0, 28(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	testq	%rdi, %rdi
	je	.L1111
	testl	%r13d, %r13d
	jle	.L1111
	movslq	%r13d, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1105:
	movl	%edx, %r13d
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1094:
	movl	$144, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1124
	movq	%r12, %rax
	movl	$18, %ecx
	movq	%rdx, %rdi
	movq	%rdx, %r12
	rep stosq
	movl	$144, 12(%rdx)
	movabsq	$5173336108, %rax
	movq	%rax, (%rdx)
.L1101:
	leaq	144(%r12), %rax
	movl	%r13d, 24(%r12)
	movq	%rax, 64(%r12)
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	64(%r12), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r12)
	jmp	.L1099
.L1124:
	movl	$7, (%rbx)
	jmp	.L1111
	.cfi_endproc
.LFE2331:
	.size	utext_setup_67, .-utext_setup_67
	.p2align 4
	.globl	utext_close_67
	.type	utext_close_67, @function
utext_close_67:
.LFB2332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L1128
	movabsq	$21474836479, %rax
	andq	(%rdi), %rax
	movabsq	$18058237996, %rdx
	cmpq	%rdx, %rax
	jne	.L1128
	movq	56(%rdi), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1130
	call	*%rax
.L1130:
	movl	4(%r12), %edx
	movl	%edx, %eax
	andl	$-5, %eax
	andl	$2, %edx
	movl	%eax, 4(%r12)
	jne	.L1146
.L1131:
	movq	$0, 56(%r12)
	testb	$1, %al
	jne	.L1147
.L1128:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1147:
	.cfi_restore_state
	movl	$0, (%r12)
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	64(%r12), %rdi
	call	uprv_free_67@PLT
	movl	4(%r12), %eax
	movq	$0, 64(%r12)
	movl	$0, 24(%r12)
	andl	$-3, %eax
	movl	%eax, 4(%r12)
	jmp	.L1131
	.cfi_endproc
.LFE2332:
	.size	utext_close_67, .-utext_close_67
	.p2align 4
	.globl	utext_openUTF8_67
	.type	utext_openUTF8_67, @function
utext_openUTF8_67:
.LFB2345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L1148
	movq	%rsi, %rax
	movq	%rdi, %r13
	movq	%rsi, %r15
	movq	%rdx, %r14
	orq	%rdx, %rax
	movq	%rcx, %rbx
	jne	.L1182
	leaq	_ZL12gEmptyString(%rip), %r15
.L1150:
	testq	%r13, %r13
	je	.L1183
	cmpl	$878368812, 0(%r13)
	jne	.L1184
	movl	4(%r13), %ecx
	testb	$4, %cl
	je	.L1157
	movq	56(%r13), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1157
	movq	%r13, %rdi
	call	*%rax
	movl	4(%r13), %ecx
.L1157:
	movl	%ecx, %eax
	movslq	24(%r13), %rdx
	andl	$-5, %eax
	movl	%eax, 4(%r13)
	cmpl	$479, %edx
	jg	.L1165
	andl	$2, %ecx
	jne	.L1185
.L1158:
	movl	$480, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.L1186
	movl	4(%r13), %eax
	movl	$480, 24(%r13)
	movq	%r13, %r12
	movl	$480, %edx
	orl	$2, %eax
	movl	%eax, 4(%r13)
.L1155:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1148
	movq	64(%r12), %rdi
	pxor	%xmm0, %xmm0
	orl	$4, %eax
	movq	$0, 16(%r12)
	movl	%eax, 4(%r12)
	movl	$0, 28(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	testq	%rdi, %rdi
	je	.L1160
	xorl	%esi, %esi
	call	memset@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1148
	movq	64(%r12), %rdi
.L1160:
	leaq	_ZL9utf8Funcs(%rip), %rax
	movq	%r15, 72(%r12)
	movq	%rax, 56(%r12)
	movl	%r14d, 120(%r12)
	cmpq	$-1, %r14
	je	.L1161
	movl	%r14d, 124(%r12)
.L1162:
	movq	%rdi, 80(%r12)
	addq	$240, %rdi
	movq	%rdi, 88(%r12)
.L1148:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L1151
	leaq	1(%rdx), %rdx
	movl	$2147483648, %eax
	cmpq	%rax, %rdx
	jbe	.L1150
.L1151:
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1184:
	movl	$1, (%rbx)
	movq	%r13, %r12
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1183:
	movl	$624, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1187
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r13, %rax
	movl	$480, %edx
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 64(%r12)
	movl	$1, %eax
	movl	$480, 24(%r12)
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1161:
	orl	$2, 8(%r12)
	movl	$0, 124(%r12)
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	%r13, %r12
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	64(%r13), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r13)
	jmp	.L1158
.L1187:
	movl	$7, (%rbx)
	jmp	.L1148
.L1186:
	movl	$7, (%rbx)
	movq	%r13, %r12
	jmp	.L1148
	.cfi_endproc
.LFE2345:
	.size	utext_openUTF8_67, .-utext_openUTF8_67
	.p2align 4
	.globl	utext_openReplaceable_67
	.type	utext_openReplaceable_67, @function
utext_openReplaceable_67:
.LFB2353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L1188
	movq	%rsi, %r14
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	je	.L1215
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L1216
	cmpl	$878368812, (%rdi)
	je	.L1195
	movl	$1, (%rdx)
	movq	%rdi, %r12
.L1188:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1195:
	.cfi_restore_state
	movl	4(%rdi), %ecx
	testb	$4, %cl
	je	.L1196
	movq	56(%rdi), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1196
	call	*%rax
	movl	4(%r13), %ecx
.L1196:
	movl	%ecx, %eax
	movslq	24(%r13), %rdx
	andl	$-5, %eax
	movl	%eax, 4(%r13)
	cmpl	$21, %edx
	jg	.L1204
	andl	$2, %ecx
	jne	.L1217
.L1197:
	movl	$22, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.L1218
	movl	4(%r13), %eax
	movl	$22, 24(%r13)
	movq	%r13, %r12
	movl	$22, %edx
	orl	$2, %eax
	movl	%eax, 4(%r13)
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1216:
	movl	$166, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1219
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r13, %rax
	movl	$22, %edx
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movl	$22, 24(%r12)
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 64(%r12)
	movl	$1, %eax
.L1194:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1188
	movq	64(%r12), %rdi
	pxor	%xmm0, %xmm0
	orl	$4, %eax
	movq	$0, 16(%r12)
	movl	%eax, 4(%r12)
	movl	$0, 28(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	testq	%rdi, %rdi
	je	.L1202
	xorl	%esi, %esi
	call	memset@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1188
.L1202:
	movl	$8, 8(%r12)
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*48(%rax)
	testb	%al, %al
	je	.L1201
	orl	$16, 8(%r12)
.L1201:
	leaq	_ZL8repFuncs(%rip), %rax
	movq	%r14, 72(%r12)
	movq	%rax, 56(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1204:
	.cfi_restore_state
	movq	%r13, %r12
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1215:
	movl	$1, (%rdx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1217:
	.cfi_restore_state
	movq	64(%r13), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r13)
	jmp	.L1197
.L1219:
	movl	$7, (%rbx)
	jmp	.L1188
.L1218:
	movl	$7, (%rbx)
	movq	%r13, %r12
	jmp	.L1188
	.cfi_endproc
.LFE2353:
	.size	utext_openReplaceable_67, .-utext_openReplaceable_67
	.p2align 4
	.globl	utext_openUnicodeString_67
	.type	utext_openUnicodeString_67, @function
utext_openUnicodeString_67:
.LFB2361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdx), %edi
	testl	%edi, %edi
	jle	.L1258
.L1221:
	movq	%r13, %r12
.L1220:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1258:
	.cfi_restore_state
	movq	%rsi, %r14
	movq	%rdx, %rbx
	testb	$1, 8(%rsi)
	jne	.L1259
	testq	%r13, %r13
	je	.L1260
	cmpl	$878368812, 0(%r13)
	jne	.L1261
	movl	4(%r13), %ecx
	testb	$4, %cl
	je	.L1229
	movq	56(%r13), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1229
	movq	%r13, %rdi
	call	*%rax
	movl	4(%r13), %ecx
.L1229:
	movl	%ecx, %eax
	movslq	24(%r13), %rdx
	andl	$-5, %eax
	movl	%eax, 4(%r13)
	testl	%edx, %edx
	jns	.L1240
	andl	$2, %ecx
	jne	.L1262
.L1230:
	xorl	%edi, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.L1257
	movl	4(%r13), %eax
	movl	$0, 24(%r13)
	movq	%r13, %r12
	xorl	%edx, %edx
	orl	$2, %eax
	movl	%eax, 4(%r13)
.L1227:
	movl	(%rbx), %ecx
	movq	%r12, %r13
	testl	%ecx, %ecx
	jg	.L1221
	movq	64(%r12), %rdi
	pxor	%xmm0, %xmm0
	orl	$4, %eax
	movq	$0, 16(%r12)
	movl	%eax, 4(%r12)
	movl	$0, 28(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	testq	%rdi, %rdi
	je	.L1235
	testl	%edx, %edx
	jne	.L1263
	.p2align 4,,10
	.p2align 3
.L1235:
	leaq	_ZL11unistrFuncs(%rip), %rax
	movq	%r14, 72(%r12)
	movq	%rax, 56(%r12)
	movswl	8(%r14), %eax
	testb	$17, %al
	jne	.L1264
	leaq	10(%r14), %rdx
	testb	$2, %al
	jne	.L1233
	movq	24(%r14), %rdx
.L1233:
	movq	%rdx, 48(%r12)
	testw	%ax, %ax
	js	.L1237
	sarl	$5, %eax
.L1238:
	movslq	%eax, %rdx
	movl	%eax, 44(%r12)
	movl	%eax, 28(%r12)
	movq	%r12, %rax
	movq	$0, 32(%r12)
	movq	%rdx, 16(%r12)
	movl	$12, 8(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1261:
	.cfi_restore_state
	movl	$1, (%rdx)
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1263:
	xorl	%esi, %esi
	call	memset@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1235
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1260:
	movl	$144, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1257
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r13, %rax
	xorl	%edx, %edx
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 64(%r12)
	movl	$1, %eax
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1237:
	movl	12(%r14), %eax
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	%r13, %r12
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1264:
	xorl	%edx, %edx
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1259:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	utext_setup_67.part.0
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L1265
.L1223:
	movl	$1, (%rbx)
	movq	%r13, %r12
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1265:
	leaq	_ZL13gEmptyUString(%rip), %rdx
	leaq	_ZL10ucstrFuncs(%rip), %rcx
	movl	$4, 8(%rax)
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	%rdx, 72(%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 112(%rax)
	movq	$0, 16(%rax)
	movl	$0, 28(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movups	%xmm0, 48(%rax)
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	64(%r13), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r13)
	jmp	.L1230
.L1257:
	movl	$7, (%rbx)
	jmp	.L1221
	.cfi_endproc
.LFE2361:
	.size	utext_openUnicodeString_67, .-utext_openUnicodeString_67
	.p2align 4
	.globl	utext_openConstUnicodeString_67
	.type	utext_openConstUnicodeString_67, @function
utext_openConstUnicodeString_67:
.LFB2362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rdx), %edi
	testl	%edi, %edi
	jle	.L1302
.L1266:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1302:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rdx, %rbx
	testb	$1, 8(%rsi)
	jne	.L1303
	testq	%r12, %r12
	je	.L1304
	cmpl	$878368812, (%r12)
	jne	.L1300
	movl	4(%r12), %ecx
	testb	$4, %cl
	je	.L1275
	movq	56(%r12), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1275
	movq	%r12, %rdi
	call	*%rax
	movl	4(%r12), %ecx
.L1275:
	movl	%ecx, %eax
	movslq	24(%r12), %rdx
	andl	$-5, %eax
	movl	%eax, 4(%r12)
	testl	%edx, %edx
	jns	.L1273
	andl	$2, %ecx
	jne	.L1305
.L1276:
	xorl	%edi, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r12)
	testq	%rax, %rax
	je	.L1301
	movl	$0, 24(%r12)
	movl	4(%r12), %eax
	xorl	%edx, %edx
	orl	$2, %eax
	movl	%eax, 4(%r12)
.L1273:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1266
	movq	64(%r12), %rdi
	pxor	%xmm0, %xmm0
	orl	$4, %eax
	movq	$0, 16(%r12)
	movl	%eax, 4(%r12)
	movl	$0, 28(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	testq	%rdi, %rdi
	je	.L1281
	testl	%edx, %edx
	jne	.L1306
	.p2align 4,,10
	.p2align 3
.L1281:
	leaq	_ZL11unistrFuncs(%rip), %rax
	movq	%r13, 72(%r12)
	movq	%rax, 56(%r12)
	movswl	8(%r13), %eax
	movl	$4, 8(%r12)
	testb	$17, %al
	jne	.L1307
	leaq	10(%r13), %rdx
	testb	$2, %al
	jne	.L1279
	movq	24(%r13), %rdx
.L1279:
	movq	%rdx, 48(%r12)
	testw	%ax, %ax
	js	.L1283
	sarl	$5, %eax
.L1284:
	movslq	%eax, %rdx
	movl	%eax, 44(%r12)
	movl	%eax, 28(%r12)
	movq	%r12, %rax
	movq	$0, 32(%r12)
	movq	%rdx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1303:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	utext_setup_67.part.0
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L1300
	leaq	_ZL13gEmptyUString(%rip), %rdx
	leaq	_ZL10ucstrFuncs(%rip), %rcx
	movl	$4, 8(%rax)
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	%rdx, 72(%rax)
	movq	$0, 112(%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 16(%rax)
	movl	$0, 28(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movups	%xmm0, 48(%rax)
.L1300:
	movl	$1, (%rbx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1306:
	.cfi_restore_state
	xorl	%esi, %esi
	call	memset@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1281
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1304:
	movl	$144, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1301
	xorl	%eax, %eax
	movl	$18, %ecx
	movq	%r12, %rdi
	xorl	%edx, %edx
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 64(%r12)
	movl	$1, %eax
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1283:
	movl	12(%r13), %eax
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1307:
	xorl	%edx, %edx
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	64(%r12), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r12)
	jmp	.L1276
.L1301:
	movl	$7, (%rbx)
	jmp	.L1266
	.cfi_endproc
.LFE2362:
	.size	utext_openConstUnicodeString_67, .-utext_openConstUnicodeString_67
	.p2align 4
	.globl	utext_openUChars_67
	.type	utext_openUChars_67, @function
utext_openUChars_67:
.LFB2368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L1308
	movq	%rsi, %rax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%rdx, %r15
	orq	%rdx, %rax
	movq	%rcx, %r14
	jne	.L1343
	leaq	_ZL13gEmptyUString(%rip), %rbx
.L1310:
	testq	%r13, %r13
	je	.L1344
	cmpl	$878368812, 0(%r13)
	jne	.L1345
	movl	4(%r13), %ecx
	testb	$4, %cl
	je	.L1317
	movq	56(%r13), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1317
	movq	%r13, %rdi
	call	*%rax
	movl	4(%r13), %ecx
.L1317:
	movl	%ecx, %eax
	movslq	24(%r13), %rdx
	andl	$-5, %eax
	movl	%eax, 4(%r13)
	testl	%edx, %edx
	jns	.L1326
	andl	$2, %ecx
	jne	.L1346
.L1318:
	xorl	%edi, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.L1347
	movl	4(%r13), %eax
	movl	$0, 24(%r13)
	movq	%r13, %r12
	xorl	%edx, %edx
	orl	$2, %eax
	movl	%eax, 4(%r13)
.L1315:
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L1308
	movq	64(%r12), %rdi
	pxor	%xmm0, %xmm0
	orl	$4, %eax
	movq	$0, 16(%r12)
	movl	%eax, 4(%r12)
	movl	$0, 28(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	testq	%rdi, %rdi
	je	.L1323
	testl	%edx, %edx
	jne	.L1348
.L1323:
	leaq	_ZL10ucstrFuncs(%rip), %rax
	movl	$0, %edx
	movq	%r15, 112(%r12)
	movq	%rax, 56(%r12)
	xorl	%eax, %eax
	cmpq	$-1, %r15
	sete	%al
	testq	%r15, %r15
	movq	%rbx, 72(%r12)
	cmovs	%rdx, %r15
	leal	4(%rax,%rax), %eax
	movq	%rbx, 48(%r12)
	movl	%eax, 8(%r12)
	movq	$0, 32(%r12)
	movq	%r15, 16(%r12)
	movl	%r15d, 44(%r12)
	movl	$0, 40(%r12)
	movl	%r15d, 28(%r12)
.L1308:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1343:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L1311
	leaq	1(%rdx), %rdx
	movl	$2147483648, %eax
	cmpq	%rax, %rdx
	jbe	.L1310
.L1311:
	movl	$1, (%r14)
	xorl	%r12d, %r12d
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1345:
	movl	$1, (%r14)
	movq	%r13, %r12
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1348:
	xorl	%esi, %esi
	call	memset@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L1323
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1344:
	movl	$144, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1349
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r13, %rax
	xorl	%edx, %edx
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 64(%r12)
	movl	$1, %eax
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	%r13, %r12
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	64(%r13), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r13)
	jmp	.L1318
.L1349:
	movl	$7, (%r14)
	jmp	.L1308
.L1347:
	movl	$7, (%r14)
	movq	%r13, %r12
	jmp	.L1308
	.cfi_endproc
.LFE2368:
	.size	utext_openUChars_67, .-utext_openUChars_67
	.p2align 4
	.globl	utext_openCharacterIterator_67
	.type	utext_openCharacterIterator_67, @function
utext_openCharacterIterator_67:
.LFB2374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L1350
	movq	%rsi, %r14
	movl	16(%rsi), %esi
	movq	%rdx, %rbx
	testl	%esi, %esi
	jg	.L1377
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L1378
	cmpl	$878368812, (%rdi)
	jne	.L1379
	movl	4(%rdi), %ecx
	testb	$4, %cl
	je	.L1358
	movq	56(%rdi), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1358
	call	*%rax
	movl	4(%r13), %ecx
.L1358:
	movl	%ecx, %eax
	movslq	24(%r13), %rdx
	andl	$-5, %eax
	movl	%eax, 4(%r13)
	cmpl	$63, %edx
	jg	.L1363
	andl	$2, %ecx
	jne	.L1380
.L1359:
	movl	$64, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r13)
	testq	%rax, %rax
	je	.L1381
	movl	4(%r13), %eax
	movl	$64, 24(%r13)
	movq	%r13, %r12
	movl	$64, %edx
	orl	$2, %eax
	movl	%eax, 4(%r13)
.L1356:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1350
	movq	64(%r12), %rdi
	pxor	%xmm0, %xmm0
	orl	$4, %eax
	movq	$0, 16(%r12)
	movl	%eax, 4(%r12)
	movl	$0, 28(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movl	$0, 8(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	testq	%rdi, %rdi
	je	.L1361
	xorl	%esi, %esi
	call	memset@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1350
	movq	64(%r12), %rdi
.L1361:
	leaq	_ZL13charIterFuncs(%rip), %rax
	movq	%r14, %xmm0
	movq	%rdi, %xmm1
	movl	$0, 8(%r12)
	movq	%rax, 56(%r12)
	movslq	20(%r14), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	$-1, 120(%r12)
	movq	%rax, 112(%r12)
	leaq	32(%rdi), %rax
	movq	%rax, 88(%r12)
	movq	%rdi, 48(%r12)
	movq	$0, 16(%r12)
	movl	$1, 28(%r12)
	movq	$-1, 32(%r12)
	movq	$1, 40(%r12)
	movups	%xmm0, 72(%r12)
.L1350:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1377:
	.cfi_restore_state
	movl	$16, (%rdx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1379:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	$1, (%rdx)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1378:
	.cfi_restore_state
	movl	$208, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1382
	movq	%rax, %rdi
	movl	$18, %ecx
	movq	%r13, %rax
	movl	$64, %edx
	rep stosq
	movabsq	$5173336108, %rax
	movl	$144, 12(%r12)
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 64(%r12)
	movl	$1, %eax
	movl	$64, 24(%r12)
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	%r13, %r12
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1380:
	movq	64(%r13), %rdi
	call	uprv_free_67@PLT
	movl	$0, 24(%r13)
	jmp	.L1359
.L1382:
	movl	$7, (%rbx)
	jmp	.L1350
.L1381:
	movl	$7, (%rbx)
	movq	%r13, %r12
	jmp	.L1350
	.cfi_endproc
.LFE2374:
	.size	utext_openCharacterIterator_67, .-utext_openCharacterIterator_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL13charIterFuncs, @object
	.size	_ZL13charIterFuncs, 112
_ZL13charIterFuncs:
	.long	112
	.long	0
	.long	0
	.long	0
	.quad	charIterTextClone
	.quad	charIterTextLength
	.quad	charIterTextAccess
	.quad	charIterTextExtract
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	charIterTextClose
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 2
	.type	_ZL13gEmptyUString, @object
	.size	_ZL13gEmptyUString, 2
_ZL13gEmptyUString:
	.zero	2
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10ucstrFuncs, @object
	.size	_ZL10ucstrFuncs, 112
_ZL10ucstrFuncs:
	.long	112
	.long	0
	.long	0
	.long	0
	.quad	ucstrTextClone
	.quad	ucstrTextLength
	.quad	ucstrTextAccess
	.quad	ucstrTextExtract
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ucstrTextClose
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	_ZL11unistrFuncs, @object
	.size	_ZL11unistrFuncs, 112
_ZL11unistrFuncs:
	.long	112
	.long	0
	.long	0
	.long	0
	.quad	unistrTextClone
	.quad	unistrTextLength
	.quad	unistrTextAccess
	.quad	unistrTextExtract
	.quad	unistrTextReplace
	.quad	unistrTextCopy
	.quad	0
	.quad	0
	.quad	unistrTextClose
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	_ZL8repFuncs, @object
	.size	_ZL8repFuncs, 112
_ZL8repFuncs:
	.long	112
	.long	0
	.long	0
	.long	0
	.quad	repTextClone
	.quad	repTextLength
	.quad	repTextAccess
	.quad	repTextExtract
	.quad	repTextReplace
	.quad	repTextCopy
	.quad	0
	.quad	0
	.quad	repTextClose
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.type	_ZL12gEmptyString, @object
	.size	_ZL12gEmptyString, 1
_ZL12gEmptyString:
	.zero	1
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9utf8Funcs, @object
	.size	_ZL9utf8Funcs, 112
_ZL9utf8Funcs:
	.long	112
	.long	0
	.long	0
	.long	0
	.quad	utf8TextClone
	.quad	utf8TextLength
	.quad	utf8TextAccess
	.quad	utf8TextExtract
	.quad	0
	.quad	0
	.quad	utf8TextMapOffsetToNative
	.quad	utf8TextMapIndexToUTF16
	.quad	utf8TextClose
	.quad	0
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
