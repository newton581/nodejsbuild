	.file	"uidna.cpp"
	.text
	.p2align 4
	.type	_ZL17_internal_toASCIIPKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode, @function
_ZL17_internal_toASCIIPKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode:
.LFB2141:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$472, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -480(%rbp)
	movl	%ecx, -468(%rbp)
	movq	24(%rbp), %r14
	movl	%r8d, -472(%rbp)
	movq	%r9, -504(%rbp)
	movq	%rax, -496(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L67
.L2:
	cmpl	$100, %r12d
	jg	.L68
	testl	%r12d, %r12d
	jle	.L6
	leaq	-464(%rbp), %rax
	movl	$100, %r8d
	movq	%rax, -488(%rbp)
	movq	%rax, %r13
.L4:
	xorl	%eax, %eax
	movl	$1, %ecx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L9:
	movzwl	(%rbx,%rax,2), %edx
	cmpw	$128, %dx
	movw	%dx, 0(%r13,%rax,2)
	cmovnb	%esi, %ecx
	addq	$1, %rax
	cmpl	%eax, %r12d
	jg	.L9
	testl	%r12d, %r12d
	movl	$1, %r11d
	cmovg	%r12d, %r11d
	movl	%r11d, %r15d
	testb	%cl, %cl
	je	.L69
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L55
	movl	-472(%rbp), %ebx
	andl	$2, %ebx
.L42:
	leal	-1(%r15), %ecx
	xorl	%edx, %edx
	movl	$-1, %r8d
	xorl	%r10d, %r10d
	movl	$1, %esi
	movl	$1, %edi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L70:
	cmpw	$122, %ax
	ja	.L49
	leal	-48(%rax), %r9d
	cmpw	$9, %r9w
	jbe	.L20
	cmpw	$45, %ax
	je	.L20
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$26, %ax
	cmovnb	%edx, %r8d
	cmovnb	%r10d, %esi
.L20:
	leaq	1(%rdx), %rax
	cmpq	%rcx, %rdx
	je	.L21
.L71:
	movq	%rax, %rdx
.L22:
	movzwl	0(%r13,%rdx,2), %eax
	cmpw	$127, %ax
	jbe	.L70
	xorl	%edi, %edi
	leaq	1(%rdx), %rax
	cmpq	%rcx, %rdx
	jne	.L71
.L21:
	testl	%ebx, %ebx
	je	.L25
	testb	%sil, %sil
	je	.L72
.L23:
	cmpw	$45, 0(%r13)
	je	.L27
	movslq	%r15d, %rax
	cmpw	$45, -2(%r13,%rax,2)
	je	.L28
.L25:
	testb	%dil, %dil
	jne	.L24
	cmpl	$3, %r15d
	jle	.L31
	movzwl	0(%r13), %eax
	leal	-65(%rax), %ecx
	leal	32(%rax), %edx
	cmpw	$26, %cx
	cmovb	%edx, %eax
	cmpw	$120, %ax
	jne	.L31
	movzwl	2(%r13), %eax
	leal	-65(%rax), %ecx
	leal	32(%rax), %edx
	cmpw	$26, %cx
	cmovb	%edx, %eax
	cmpw	$110, %ax
	jne	.L31
	movzwl	4(%r13), %eax
	leal	-65(%rax), %edx
	cmpw	$25, %dx
	jbe	.L31
	cmpw	$45, %ax
	jne	.L31
	movzwl	6(%r13), %eax
	leal	-65(%rax), %edx
	cmpw	$25, %dx
	jbe	.L31
	cmpw	$45, %ax
	jne	.L31
	movl	$66564, (%r14)
.L65:
	movq	-496(%rbp), %rcx
	movl	%r15d, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	uprv_syntaxError_67@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L69:
	pushq	%r14
	movl	-472(%rbp), %r9d
	movq	%r13, %rcx
	movl	%r12d, %edx
	pushq	-496(%rbp)
	movq	-504(%rbp), %rdi
	movq	%rbx, %rsi
	andl	$1, %r9d
	movl	%r9d, -508(%rbp)
	call	usprep_prepare_67@PLT
	popq	%rdi
	movl	-508(%rbp), %r9d
	movl	%eax, %r15d
	movl	(%r14), %eax
	popq	%r8
	cmpl	$15, %eax
	je	.L73
	testl	%eax, %eax
	jle	.L16
.L55:
	xorl	%r15d, %r15d
.L17:
	cmpq	-488(%rbp), %r13
	je	.L10
	leaq	-256(%rbp), %rbx
	movq	%rbx, %r12
.L5:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
.L39:
	cmpq	%rbx, %r12
	je	.L10
.L36:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
.L10:
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movq	-480(%rbp), %rdi
	movq	%r14, %rcx
	movl	%r15d, %edx
	movl	-468(%rbp), %esi
	call	u_terminateUChars_67@PLT
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L74
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	%edx, %r8d
	xorl	%esi, %esi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L68:
	leal	(%r12,%r12), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L15
	leaq	-464(%rbp), %rax
	movl	%r12d, %r8d
	movq	%rax, -488(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L67:
	call	u_strlen_67@PLT
	movl	%eax, %r12d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L73:
	cmpq	-488(%rbp), %r13
	je	.L14
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movl	-508(%rbp), %r9d
.L14:
	leal	(%r15,%r15), %edi
	movl	%r9d, -508(%rbp)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L15
	movl	$0, (%r14)
	movl	%r15d, %r8d
	movq	%rax, %rcx
	movl	%r12d, %edx
	pushq	%r14
	movl	-508(%rbp), %r9d
	movq	%rbx, %rsi
	pushq	-496(%rbp)
	movq	-504(%rbp), %rdi
	call	usprep_prepare_67@PLT
	movl	(%r14), %esi
	popq	%rdx
	movl	%eax, %r15d
	popq	%rcx
	testl	%esi, %esi
	jg	.L75
.L16:
	testl	%r15d, %r15d
	jne	.L18
.L11:
	movl	$66567, (%r14)
	xorl	%r15d, %r15d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	%r15d, -468(%rbp)
	jl	.L17
	movq	-480(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	leaq	-256(%rbp), %rbx
	movq	%rbx, %r12
	call	u_memmove_67@PLT
.L30:
	cmpl	$63, %r15d
	jle	.L37
	movl	$66566, (%r14)
.L37:
	cmpq	-488(%rbp), %r13
	jne	.L5
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	-256(%rbp), %rbx
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movl	%r15d, %esi
	movl	$100, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rbx, %r12
	call	u_strToPunycode_67@PLT
	movl	%eax, %ecx
	movl	(%r14), %eax
	cmpl	$15, %eax
	je	.L76
.L34:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jg	.L37
	leal	4(%rcx), %r15d
	cmpl	-468(%rbp), %r15d
	jle	.L38
	movl	$15, (%r14)
	jmp	.L37
.L28:
	testl	%r15d, %r15d
	leal	-1(%r15), %esi
	movl	%r15d, %edx
	movq	%r13, %rdi
	movl	$66563, (%r14)
	cmovle	%r15d, %esi
	movq	-496(%rbp), %rcx
	xorl	%r15d, %r15d
	call	uprv_syntaxError_67@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L6:
	movl	(%r14), %r9d
	xorl	%r15d, %r15d
	testl	%r9d, %r9d
	jg	.L10
	leaq	-464(%rbp), %rax
	movq	%rax, -488(%rbp)
	movq	%rax, %r13
	jmp	.L11
.L18:
	movl	-472(%rbp), %ebx
	andl	$2, %ebx
	testl	%r15d, %r15d
	jg	.L42
	testl	%ebx, %ebx
	je	.L24
	movl	$1, %edi
	jmp	.L23
.L75:
	leaq	-256(%rbp), %rbx
	xorl	%r15d, %r15d
	movq	%rbx, %r12
	jmp	.L5
.L38:
	movq	-480(%rbp), %rdi
	movl	$4, %edx
	leaq	_ZL10ACE_PREFIX(%rip), %rsi
	movl	%ecx, -472(%rbp)
	call	u_memcpy_67@PLT
	movq	-480(%rbp), %rax
	movl	-472(%rbp), %ecx
	movq	%r12, %rsi
	leaq	8(%rax), %rdi
	movl	%ecx, %edx
	call	u_memcpy_67@PLT
	jmp	.L30
.L72:
	movl	$66563, (%r14)
	movl	%r15d, %edx
	movl	%r8d, %esi
	movq	%r13, %rdi
	movq	-496(%rbp), %rcx
	xorl	%r15d, %r15d
	call	uprv_syntaxError_67@PLT
	jmp	.L17
.L76:
	leal	(%rcx,%rcx), %edi
	movl	%ecx, -472(%rbp)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movl	-472(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L77
	movl	$0, (%r14)
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	u_strToPunycode_67@PLT
	movl	%eax, %ecx
	movl	(%r14), %eax
	jmp	.L34
.L27:
	movl	$66563, (%r14)
	jmp	.L65
.L77:
	movl	$7, (%r14)
	xorl	%r15d, %r15d
	cmpq	-488(%rbp), %r13
	jne	.L5
	jmp	.L36
.L15:
	leaq	-256(%rbp), %rbx
	movl	$7, (%r14)
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	movq	%rbx, %r12
	jmp	.L5
.L74:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2141:
	.size	_ZL17_internal_toASCIIPKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode, .-_ZL17_internal_toASCIIPKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode
	.p2align 4
	.type	_ZL19_internal_toUnicodePKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode, @function
_ZL19_internal_toUnicodePKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode:
.LFB2142:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$728, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdx, -688(%rbp)
	movl	%ecx, -676(%rbp)
	movq	24(%rbp), %r14
	movl	%r8d, -720(%rbp)
	movq	%rax, -704(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L161
	movl	%esi, %ebx
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L78
	leal	-1(%rsi), %edx
	movq	%rdi, %rax
	leaq	2(%rdi,%rdx,2), %rdx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L163:
	addq	$2, %rax
	cmpq	%rax, %rdx
	je	.L162
.L87:
	cmpw	$127, (%rax)
	jbe	.L163
.L85:
	movl	-720(%rbp), %r15d
	pushq	%r14
	movl	%ebx, %edx
	movq	%r11, %rdi
	pushq	-704(%rbp)
	leaq	-672(%rbp), %rax
	movl	$100, %r8d
	movq	%r12, %rsi
	andl	$1, %r15d
	movq	%rax, %rcx
	movq	%r11, -712(%rbp)
	movl	%r15d, %r9d
	movq	%rax, -696(%rbp)
	call	usprep_prepare_67@PLT
	popq	%rdx
	movq	-712(%rbp), %r11
	movl	%eax, %r8d
	movl	(%r14), %eax
	popq	%rcx
	cmpl	$15, %eax
	je	.L164
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jg	.L115
	movl	%ebx, %r15d
	movl	%r8d, %ebx
	movq	-696(%rbp), %r13
	cmpl	$3, %ebx
	jg	.L165
	.p2align 4,,10
	.p2align 3
.L91:
	movl	%r15d, %ebx
	cmpl	%r15d, -676(%rbp)
	jge	.L120
.L113:
	cmpq	-696(%rbp), %r13
	je	.L115
.L90:
	leaq	-464(%rbp), %rax
	movq	%rax, -712(%rbp)
	movq	%rax, %r10
	cmpq	%r13, %r12
	je	.L115
.L118:
	movq	%r13, %rdi
	movq	%r10, -696(%rbp)
	call	uprv_free_67@PLT
	movq	-696(%rbp), %r10
.L114:
	cmpq	-712(%rbp), %r10
	je	.L115
.L97:
	movq	%r10, %rdi
	call	uprv_free_67@PLT
.L115:
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jle	.L116
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L117
	cmpl	%ebx, -676(%rbp)
	jge	.L166
.L117:
	movl	$0, (%r14)
	movl	%ebx, %r15d
.L116:
	movl	-676(%rbp), %esi
	movq	-688(%rbp), %rdi
	movq	%r14, %rcx
	movl	%r15d, %edx
	call	u_terminateUChars_67@PLT
.L78:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L167
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movzwl	(%rdi), %edx
	testw	%dx, %dx
	je	.L80
	movl	$1, %eax
	movl	$1, %ecx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L83:
	cmpw	$128, %dx
	movl	%eax, %ebx
	cmovnb	%esi, %ecx
	addq	$1, %rax
	movzwl	-2(%r12,%rax,2), %edx
	testw	%dx, %dx
	jne	.L83
	leaq	-672(%rbp), %rax
	movl	%ebx, %r15d
	movq	%r12, %r13
	movq	%rax, -696(%rbp)
	testb	%cl, %cl
	je	.L85
.L86:
	cmpl	$3, %ebx
	jle	.L91
.L165:
	movzwl	0(%r13), %eax
	leal	-65(%rax), %ecx
	leal	32(%rax), %edx
	cmpw	$26, %cx
	cmovb	%edx, %eax
	cmpw	$120, %ax
	jne	.L91
	movzwl	2(%r13), %eax
	leal	-65(%rax), %ecx
	leal	32(%rax), %edx
	cmpw	$26, %cx
	cmovb	%edx, %eax
	cmpw	$110, %ax
	jne	.L91
	movzwl	4(%r13), %eax
	leal	-65(%rax), %edx
	cmpw	$25, %dx
	jbe	.L91
	cmpw	$45, %ax
	jne	.L91
	movzwl	6(%r13), %eax
	leal	-65(%rax), %edx
	cmpw	$25, %dx
	jbe	.L91
	cmpw	$45, %ax
	jne	.L91
	leaq	-464(%rbp), %rax
	leal	-4(%rbx), %esi
	xorl	%r8d, %r8d
	movq	%r14, %r9
	leaq	8(%r13), %rdi
	movl	$100, %ecx
	movq	%rax, %rdx
	movl	%esi, -728(%rbp)
	movq	%rdi, -736(%rbp)
	movq	%rax, -712(%rbp)
	call	u_strFromPunycode_67@PLT
	movl	-728(%rbp), %esi
	movl	%eax, %r11d
	movl	(%r14), %eax
	cmpl	$15, %eax
	je	.L168
	movq	-712(%rbp), %r10
	testl	%eax, %eax
	jg	.L100
.L123:
	cmpl	$-1, %r11d
	jl	.L104
	xorl	%edi, %edi
	movq	%r14, %rsi
	movq	%r10, -736(%rbp)
	movl	%r11d, -728(%rbp)
	call	usprep_openByType_67@PLT
	movl	-728(%rbp), %r11d
	movq	-736(%rbp), %r10
	movq	%rax, %r9
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L131
	pushq	%r14
	movl	-720(%rbp), %r8d
	movq	%r10, %rdi
	movl	%r11d, %esi
	pushq	-704(%rbp)
	leaq	-256(%rbp), %rdx
	movl	$100, %ecx
	movq	%rdx, -752(%rbp)
	movl	%r11d, -744(%rbp)
	movq	%r9, -760(%rbp)
	call	_ZL17_internal_toASCIIPKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode
	movq	-760(%rbp), %r9
	movl	%eax, -728(%rbp)
	movq	%r9, %rdi
	call	usprep_close_67@PLT
	movl	(%r14), %eax
	popq	%r8
	movq	-752(%rbp), %rdx
	movl	-728(%rbp), %ecx
	cmpl	$15, %eax
	movq	-736(%rbp), %r10
	movl	-744(%rbp), %r11d
	popq	%r9
	je	.L169
.L101:
	testl	%eax, %eax
	jg	.L100
	cmpl	%ecx, %ebx
	je	.L133
	jge	.L170
	movl	%ebx, %ecx
	movl	$-1, %edi
.L106:
	leal	-1(%rcx), %r8d
	xorl	%eax, %eax
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L109:
	leal	-65(%rcx), %r9d
	cmpw	$25, %r9w
	ja	.L107
.L122:
	addl	$32, %ecx
.L110:
	cmpw	%si, %cx
	jne	.L107
.L108:
	leaq	1(%rax), %rcx
	cmpq	%r8, %rax
	je	.L171
	movq	%rcx, %rax
.L111:
	movzwl	0(%r13,%rax,2), %esi
	movzwl	(%rdx,%rax,2), %ecx
	cmpw	%cx, %si
	je	.L108
	leal	-65(%rsi), %r9d
	cmpw	$25, %r9w
	ja	.L109
	leal	-65(%rcx), %r9d
	addl	$32, %esi
	cmpw	$25, %r9w
	ja	.L110
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L162:
	leaq	-672(%rbp), %rax
	movl	%ebx, %r15d
	movq	%r12, %r13
	movq	%rax, -696(%rbp)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L166:
	movl	%ebx, %edx
	movq	%r12, %rsi
	call	u_memmove_67@PLT
	jmp	.L117
.L104:
	movl	$1, (%r14)
.L100:
	xorl	%r11d, %r11d
.L103:
	movl	%r15d, %ebx
	movl	%r11d, %r15d
	cmpq	-696(%rbp), %r13
	je	.L114
	cmpq	%r12, %r13
	je	.L114
	jmp	.L118
.L142:
	leaq	-672(%rbp), %rax
	movq	%r12, %r13
	xorl	%r15d, %r15d
	movq	%rax, -696(%rbp)
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-688(%rbp), %rdi
	movl	%r15d, %edx
	movq	%r12, %rsi
	movl	%r15d, %ebx
	call	u_memmove_67@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L164:
	leal	(%r8,%r8), %edi
	movq	%r11, -728(%rbp)
	movslq	%edi, %rdi
	movl	%r8d, -712(%rbp)
	call	uprv_malloc_67@PLT
	movl	-712(%rbp), %r8d
	movq	-728(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L172
	movl	$0, (%r14)
	movl	%r15d, %r9d
	movq	%r11, %rdi
	movq	%rax, %rcx
	pushq	%r14
	movl	%ebx, %edx
	movq	%r12, %rsi
	pushq	-704(%rbp)
	call	usprep_prepare_67@PLT
	movl	(%r14), %r15d
	popq	%r10
	popq	%r11
	testl	%r15d, %r15d
	jg	.L129
	movl	%ebx, %r15d
	movl	%eax, %ebx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L168:
	leal	(%r11,%r11), %edi
	movl	%esi, -744(%rbp)
	movslq	%edi, %rdi
	movl	%r11d, -728(%rbp)
	call	uprv_malloc_67@PLT
	movl	-728(%rbp), %r11d
	movl	-744(%rbp), %esi
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L173
	movl	$0, (%r14)
	xorl	%r8d, %r8d
	movl	%r11d, %ecx
	movq	%r14, %r9
	movq	-736(%rbp), %rdi
	movq	%rax, %rdx
	movq	%rax, -728(%rbp)
	call	u_strFromPunycode_67@PLT
	movq	-728(%rbp), %r10
	movl	%eax, %r11d
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L123
	xorl	%edi, %edi
	xorl	%ecx, %ecx
.L98:
	cmpl	$15, %eax
	jne	.L100
.L119:
	movq	%r10, -744(%rbp)
	movl	%r11d, -736(%rbp)
	movl	%ecx, -728(%rbp)
	call	uprv_malloc_67@PLT
	movl	-728(%rbp), %ecx
	movl	-736(%rbp), %r11d
	testq	%rax, %rax
	movq	-744(%rbp), %r10
	je	.L174
	cmpl	$-1, %r11d
	movq	%rax, -736(%rbp)
	movl	$0, (%r14)
	movl	%r11d, -728(%rbp)
	jl	.L104
	testl	%ecx, %ecx
	movl	%ecx, -744(%rbp)
	js	.L104
	xorl	%edi, %edi
	movq	%r14, %rsi
	movq	%r10, -752(%rbp)
	call	usprep_openByType_67@PLT
	movl	(%r14), %edi
	movq	-752(%rbp), %r10
	movl	-744(%rbp), %ecx
	movl	-728(%rbp), %r11d
	movq	%rax, %r9
	testl	%edi, %edi
	movq	-736(%rbp), %rdx
	jg	.L100
	pushq	%r14
	movl	-720(%rbp), %r8d
	movl	%r11d, %esi
	movq	%r10, %rdi
	pushq	-704(%rbp)
	movq	%rdx, -736(%rbp)
	movl	%r11d, -728(%rbp)
	movq	%r10, -720(%rbp)
	movq	%rax, -744(%rbp)
	call	_ZL17_internal_toASCIIPKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode
	movq	-744(%rbp), %r9
	movl	%eax, -704(%rbp)
	movq	%r9, %rdi
	call	usprep_close_67@PLT
	popq	%rcx
	movl	(%r14), %eax
	movq	-736(%rbp), %rdx
	movl	-728(%rbp), %r11d
	movq	-720(%rbp), %r10
	movl	-704(%rbp), %ecx
	popq	%rsi
	jmp	.L101
.L129:
	xorl	%r15d, %r15d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L171:
	testl	%edi, %edi
	jne	.L107
	cmpl	%r11d, -676(%rbp)
	jl	.L103
	movq	-688(%rbp), %rdi
	movl	%r11d, %edx
	movq	%r10, %rsi
	movl	%r11d, -720(%rbp)
	movq	%r10, -704(%rbp)
	call	u_memmove_67@PLT
	movq	-704(%rbp), %r10
	movl	-720(%rbp), %r11d
	jmp	.L103
.L80:
	movl	-676(%rbp), %eax
	testl	%eax, %eax
	jns	.L142
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	jmp	.L115
.L133:
	xorl	%edi, %edi
	jmp	.L106
.L107:
	movl	$66565, (%r14)
	xorl	%r11d, %r11d
	jmp	.L103
.L170:
	testl	%ecx, %ecx
	je	.L107
	movl	$1, %edi
	jmp	.L106
.L169:
	leal	(%rcx,%rcx), %edi
	movslq	%edi, %rdi
	jmp	.L119
.L131:
	movq	$-2, %rdi
	orl	$-1, %ecx
	jmp	.L98
.L167:
	call	__stack_chk_fail@PLT
.L172:
	movl	$7, (%r14)
	xorl	%r15d, %r15d
	jmp	.L90
.L173:
	movl	$7, (%r14)
	cmpq	-696(%rbp), %r13
	je	.L160
	cmpq	%r12, %r13
	jne	.L139
.L160:
	movl	%r15d, %ebx
	xorl	%r15d, %r15d
	jmp	.L97
.L174:
	movl	$7, (%r14)
	xorl	%r11d, %r11d
	jmp	.L103
.L139:
	movl	%r15d, %ebx
	xorl	%r15d, %r15d
	jmp	.L118
	.cfi_endproc
.LFE2142:
	.size	_ZL19_internal_toUnicodePKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode, .-_ZL19_internal_toUnicodePKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode
	.p2align 4
	.globl	uidna_toASCII_67
	.type	uidna_toASCII_67, @function
uidna_toASCII_67:
.LFB2143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 3, -48
	movq	16(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L175
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L175
	testq	%rdi, %rdi
	movq	%rdx, %r15
	movq	%rdi, %r13
	movl	%esi, %r14d
	sete	%dl
	cmpl	$-1, %esi
	setl	%al
	orb	%al, %dl
	jne	.L177
	testl	%ecx, %ecx
	js	.L177
	testq	%r15, %r15
	jne	.L178
	testl	%ecx, %ecx
	jg	.L177
.L178:
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movl	%ecx, -36(%rbp)
	movq	%r9, -48(%rbp)
	movl	%r8d, -40(%rbp)
	call	usprep_openByType_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L181
	movq	-48(%rbp), %r9
	pushq	%rbx
	movq	%r15, %rdx
	movl	%r14d, %esi
	movl	-40(%rbp), %r8d
	movl	-36(%rbp), %ecx
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	pushq	%r9
	movq	%rax, %r9
	call	_ZL17_internal_toASCIIPKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode
	movq	-48(%rbp), %r10
	movl	%eax, -36(%rbp)
	movq	%r10, %rdi
	call	usprep_close_67@PLT
	popq	%rax
	movl	-36(%rbp), %eax
	popq	%rdx
.L175:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movl	$1, (%rbx)
	leaq	-32(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L181:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L175
	.cfi_endproc
.LFE2143:
	.size	uidna_toASCII_67, .-uidna_toASCII_67
	.p2align 4
	.globl	uidna_toUnicode_67
	.type	uidna_toUnicode_67, @function
uidna_toUnicode_67:
.LFB2144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 3, -48
	movq	16(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L189
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L189
	testq	%rdi, %rdi
	movq	%rdx, %r15
	movq	%rdi, %r13
	movl	%esi, %r14d
	sete	%dl
	cmpl	$-1, %esi
	setl	%al
	orb	%al, %dl
	jne	.L191
	testl	%ecx, %ecx
	js	.L191
	testq	%r15, %r15
	jne	.L192
	testl	%ecx, %ecx
	jg	.L191
.L192:
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movl	%ecx, -36(%rbp)
	movq	%r9, -48(%rbp)
	movl	%r8d, -40(%rbp)
	call	usprep_openByType_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L195
	movq	-48(%rbp), %r9
	pushq	%rbx
	movq	%r15, %rdx
	movl	%r14d, %esi
	movl	-40(%rbp), %r8d
	movl	-36(%rbp), %ecx
	movq	%r13, %rdi
	movq	%rax, -48(%rbp)
	pushq	%r9
	movq	%rax, %r9
	call	_ZL19_internal_toUnicodePKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode
	movq	-48(%rbp), %r10
	movl	%eax, -36(%rbp)
	movq	%r10, %rdi
	call	usprep_close_67@PLT
	popq	%rax
	movl	-36(%rbp), %eax
	popq	%rdx
.L189:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movl	$1, (%rbx)
	leaq	-32(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L195:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L189
	.cfi_endproc
.LFE2144:
	.size	uidna_toUnicode_67, .-uidna_toUnicode_67
	.p2align 4
	.globl	uidna_IDNToASCII_67
	.type	uidna_IDNToASCII_67, @function
uidna_IDNToASCII_67:
.LFB2145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r12
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	testq	%r12, %r12
	je	.L203
	movl	(%r12), %r9d
	testl	%r9d, %r9d
	jg	.L203
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movl	%esi, %r13d
	testq	%rbx, %rbx
	sete	%dl
	cmpl	$-1, %esi
	setl	%al
	orb	%al, %dl
	jne	.L207
	movl	%ecx, %r14d
	testl	%ecx, %ecx
	js	.L207
	testq	%rdi, %rdi
	jne	.L208
	testl	%ecx, %ecx
	jg	.L207
.L208:
	movq	%r12, %rsi
	xorl	%edi, %edi
	movl	%r8d, -72(%rbp)
	call	usprep_openByType_67@PLT
	movl	(%r12), %r8d
	movq	%rax, -104(%rbp)
	testl	%r8d, %r8d
	jg	.L203
	movq	-64(%rbp), %rax
	movb	$0, -65(%rbp)
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	movl	%r14d, -96(%rbp)
	movl	-72(%rbp), %r8d
	movl	%r13d, %ebx
	movl	%r13d, %r11d
	movq	%rax, -80(%rbp)
	movq	%rdi, %r10
.L209:
	cmpl	$-1, %ebx
	je	.L270
.L210:
	testl	%ebx, %ebx
	jle	.L218
	xorl	%eax, %eax
	leal	-1(%rbx), %r9d
	movzwl	(%rdi,%rax,2), %edx
	movl	%eax, %ecx
	movl	%eax, %esi
	cmpw	$-242, %dx
	je	.L219
.L271:
	ja	.L220
	cmpw	$46, %dx
	je	.L219
	cmpw	$12290, %dx
	je	.L219
.L221:
	leal	1(%rcx), %esi
	leaq	1(%rax), %rdx
	cmpq	%r9, %rax
	je	.L222
	movq	%rdx, %rax
	movzwl	(%rdi,%rax,2), %edx
	movl	%eax, %ecx
	movl	%eax, %esi
	cmpw	$-242, %dx
	jne	.L271
	.p2align 4,,10
	.p2align 3
.L219:
	leaq	2(%rdi,%rax,2), %r13
.L215:
	testl	%esi, %esi
	jne	.L216
	cmpb	$0, -65(%rbp)
	jne	.L272
.L216:
	movq	-104(%rbp), %r9
	movq	-80(%rbp), %rdx
	pushq	%r12
	movl	%r14d, %ecx
	pushq	-56(%rbp)
	movl	%r11d, -92(%rbp)
	movq	%r10, -88(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZL17_internal_toASCIIPKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode
	popq	%rcx
	movl	-72(%rbp), %r8d
	movl	%eax, %esi
	movl	(%r12), %eax
	popq	%rdi
	movq	%r13, %rdi
	movq	-88(%rbp), %r10
	movl	-92(%rbp), %r11d
	cmpl	$15, %eax
	je	.L273
.L224:
	testl	%eax, %eax
	jg	.L226
.L225:
	addl	%esi, %r15d
	cmpl	%r14d, %esi
	jl	.L227
	cmpb	$1, -65(%rbp)
	je	.L226
	xorl	%r14d, %r14d
.L228:
	addl	$1, %r15d
	testl	%ebx, %ebx
	jle	.L209
	movq	%rdi, %rax
	movl	%r11d, %ebx
	subq	%r10, %rax
	sarq	%rax
	subl	%eax, %ebx
	cmpl	$-1, %ebx
	jne	.L210
.L270:
	movzwl	(%rdi), %eax
	testw	%ax, %ax
	je	.L211
	xorl	%edx, %edx
.L217:
	movl	%edx, %ecx
	movl	%edx, %esi
	cmpw	$-242, %ax
	je	.L212
	ja	.L213
	cmpw	$46, %ax
	je	.L212
	cmpw	$12290, %ax
	je	.L212
.L214:
	addq	$1, %rdx
	leal	1(%rcx), %esi
	movzwl	(%rdi,%rdx,2), %eax
	leaq	(%rdi,%rdx,2), %r13
	testw	%ax, %ax
	jne	.L217
	movb	$1, -65(%rbp)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L207:
	movl	$1, (%r12)
.L203:
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	cmpw	$-159, %dx
	jne	.L221
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L227:
	movq	-80(%rbp), %rcx
	movslq	%esi, %rax
	subl	%esi, %r14d
	cmpb	$1, -65(%rbp)
	leaq	(%rcx,%rax,2), %rax
	je	.L226
	leaq	2(%rax), %rcx
	movl	$46, %edx
	subl	$1, %r14d
	movq	%rcx, -80(%rbp)
	movw	%dx, (%rax)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L213:
	cmpw	$-159, %ax
	jne	.L214
.L212:
	leaq	2(%rdi,%rdx,2), %r13
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L272:
	movl	(%r12), %eax
	movq	%r13, %rdi
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L273:
	movl	$0, (%r12)
	xorl	%r14d, %r14d
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L226:
	movl	-96(%rbp), %r14d
	cmpl	$255, %r15d
	jle	.L230
	movl	$66568, (%r12)
.L230:
	movq	-104(%rbp), %rdi
	call	usprep_close_67@PLT
	movq	-64(%rbp), %rdi
	leaq	-40(%rbp), %rsp
	movq	%r12, %rcx
	popq	%rbx
	movl	%r15d, %edx
	popq	%r12
	movl	%r14d, %esi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movslq	%ebx, %rax
	movb	$1, -65(%rbp)
	xorl	%esi, %esi
	leaq	(%rdi,%rax,2), %rdi
	movl	(%r12), %eax
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L222:
	movslq	%ebx, %rax
	movb	$1, -65(%rbp)
	leaq	(%rdi,%rax,2), %r13
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L211:
	movb	$1, -65(%rbp)
	movl	(%r12), %eax
	xorl	%esi, %esi
	jmp	.L224
	.cfi_endproc
.LFE2145:
	.size	uidna_IDNToASCII_67, .-uidna_IDNToASCII_67
	.p2align 4
	.globl	uidna_IDNToUnicode_67
	.type	uidna_IDNToUnicode_67, @function
uidna_IDNToUnicode_67:
.LFB2146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r12
	movq	%rdx, -96(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%r9, -56(%rbp)
	testq	%r12, %r12
	je	.L274
	movl	(%r12), %r9d
	testl	%r9d, %r9d
	jg	.L274
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movl	%esi, %r13d
	testq	%rbx, %rbx
	sete	%dl
	cmpl	$-1, %esi
	setl	%al
	orb	%al, %dl
	jne	.L278
	testl	%ecx, %ecx
	js	.L278
	testq	%rdi, %rdi
	jne	.L279
	testl	%ecx, %ecx
	jg	.L278
.L279:
	xorl	%edi, %edi
	movq	%r12, %rsi
	movl	%r8d, -64(%rbp)
	call	usprep_openByType_67@PLT
	movl	(%r12), %edi
	movq	%rax, -104(%rbp)
	testl	%edi, %edi
	jg	.L274
	movl	%r13d, %r10d
	movl	-72(%rbp), %ecx
	movq	%r12, %r11
	movl	%r13d, -112(%rbp)
	movl	%r10d, %r12d
	movb	$0, -105(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rbx, %r14
	movq	%rbx, -120(%rbp)
	movl	-64(%rbp), %r8d
	xorl	%r15d, %r15d
	movl	%ecx, %r13d
	cmpl	$-1, %r12d
	je	.L337
	.p2align 4,,10
	.p2align 3
.L281:
	testl	%r12d, %r12d
	jle	.L301
	xorl	%eax, %eax
	leal	-1(%r12), %edi
	movzwl	(%r14,%rax,2), %ecx
	movl	%eax, %esi
	movl	%eax, %ebx
	cmpw	$-242, %cx
	je	.L288
.L338:
	ja	.L289
	cmpw	$46, %cx
	je	.L288
	cmpw	$12290, %cx
	je	.L288
.L290:
	leal	1(%rsi), %ebx
	leaq	1(%rax), %rcx
	cmpq	%rdi, %rax
	je	.L287
	movq	%rcx, %rax
	movzwl	(%r14,%rax,2), %ecx
	movl	%eax, %esi
	movl	%eax, %ebx
	cmpw	$-242, %cx
	jne	.L338
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	2(%r14,%rax,2), %r10
.L282:
	pushq	%r11
	movq	-104(%rbp), %r9
	movl	%r13d, %ecx
	movl	%ebx, %esi
	pushq	-56(%rbp)
	movq	%r14, %rdi
	movq	%r10, -88(%rbp)
	movq	%r11, -80(%rbp)
	movl	%r8d, -68(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZL19_internal_toUnicodePKDsiPDsiiP18UStringPrepProfileP11UParseErrorP10UErrorCode
	movq	-80(%rbp), %r11
	popq	%rdx
	movl	-68(%rbp), %r8d
	movq	-64(%rbp), %rdx
	movl	(%r11), %ecx
	movq	-88(%rbp), %r10
	popq	%rsi
	cmpl	$15, %ecx
	je	.L339
	testl	%ecx, %ecx
	jg	.L294
.L293:
	addl	%eax, %r15d
	cmpl	%r13d, %eax
	jl	.L295
	cmpb	$1, -105(%rbp)
	je	.L294
	xorl	%r13d, %r13d
.L296:
	addl	$1, %r15d
	testl	%r12d, %r12d
	jle	.L297
	movl	-112(%rbp), %r12d
	movq	%r10, %rax
	subq	-120(%rbp), %rax
	sarq	%rax
	subl	%eax, %r12d
.L297:
	movq	%r10, %r14
	cmpl	$-1, %r12d
	jne	.L281
.L337:
	movzwl	(%r14), %eax
	testw	%ax, %ax
	je	.L299
	xorl	%ecx, %ecx
.L286:
	movl	%ecx, %esi
	movl	%ecx, %ebx
	cmpw	$-242, %ax
	je	.L283
	ja	.L284
	cmpw	$46, %ax
	je	.L283
	cmpw	$12290, %ax
	je	.L283
.L285:
	addq	$1, %rcx
	leal	1(%rsi), %ebx
	movzwl	(%r14,%rcx,2), %eax
	leaq	(%r14,%rcx,2), %r10
	testw	%ax, %ax
	jne	.L286
	movb	$1, -105(%rbp)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L278:
	movl	$1, (%r12)
.L274:
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	cmpw	$-159, %cx
	jne	.L290
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L295:
	movslq	%eax, %rcx
	subl	%eax, %r13d
	cmpb	$1, -105(%rbp)
	leaq	(%rdx,%rcx,2), %rcx
	je	.L294
	movslq	%ebx, %rbx
	leaq	2(%rcx), %rdx
	subl	$1, %r13d
	movzwl	(%r14,%rbx,2), %eax
	movw	%ax, (%rcx)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L284:
	cmpw	$-159, %ax
	jne	.L285
.L283:
	leaq	2(%r14,%rcx,2), %r10
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L339:
	movl	$0, (%r11)
	xorl	%r13d, %r13d
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%r11, %r12
	cmpl	$255, %r15d
	jle	.L298
	movl	$66568, (%r11)
.L298:
	movq	-104(%rbp), %rdi
	call	usprep_close_67@PLT
	movl	-72(%rbp), %esi
	movq	-96(%rbp), %rdi
	leaq	-40(%rbp), %rsp
	popq	%rbx
	movq	%r12, %rcx
	movl	%r15d, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	xorl	%ebx, %ebx
.L287:
	movslq	%r12d, %rax
	movb	$1, -105(%rbp)
	leaq	(%r14,%rax,2), %r10
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L299:
	movb	$1, -105(%rbp)
	movq	%r14, %r10
	xorl	%ebx, %ebx
	jmp	.L282
	.cfi_endproc
.LFE2146:
	.size	uidna_IDNToUnicode_67, .-uidna_IDNToUnicode_67
	.p2align 4
	.globl	uidna_compare_67
	.type	uidna_compare_67, @function
uidna_compare_67:
.LFB2147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1176, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -1184(%rbp)
	movl	%ecx, -1188(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L357
	movl	(%r9), %r12d
	movq	%r9, %rbx
	testl	%r12d, %r12d
	jg	.L357
	subq	$8, %rsp
	movl	$256, %ecx
	movq	%rdi, %r14
	movl	%r8d, %r13d
	pushq	%r9
	leaq	-1088(%rbp), %r12
	leaq	-1168(%rbp), %r15
	movq	%r15, %r9
	movq	%r12, %rdx
	movq	%r12, -1200(%rbp)
	movl	%esi, -1208(%rbp)
	call	uidna_IDNToASCII_67
	cmpl	$15, (%rbx)
	popq	%r10
	movl	%eax, -1176(%rbp)
	popq	%r11
	je	.L369
.L342:
	subq	$8, %rsp
	movl	-1188(%rbp), %esi
	movq	%r15, %r9
	movl	%r13d, %r8d
	pushq	%rbx
	movq	-1184(%rbp), %rdi
	movl	$256, %ecx
	leaq	-576(%rbp), %r14
	movq	%r14, %rdx
	call	uidna_IDNToASCII_67
	cmpl	$15, (%rbx)
	popq	%rsi
	movq	%r14, %rdx
	movl	%eax, %ecx
	popq	%rdi
	je	.L370
.L345:
	xorl	%r13d, %r13d
	cmpl	%ecx, -1176(%rbp)
	je	.L348
	movl	$1, %r13d
	jl	.L371
.L348:
	testl	%ecx, %ecx
	je	.L349
	leal	-1(%rcx), %r8d
	xorl	%esi, %esi
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%rax, %rsi
.L353:
	movzwl	(%r12,%rsi,2), %eax
	movzwl	(%rdx,%rsi,2), %ecx
	cmpw	%cx, %ax
	je	.L350
	leal	-65(%rax), %r9d
	leal	32(%rax), %edi
	cmpw	$26, %r9w
	leal	-65(%rcx), %r9d
	cmovb	%edi, %eax
	leal	32(%rcx), %edi
	cmpw	$26, %r9w
	cmovb	%edi, %ecx
	movzwl	%ax, %eax
	movzwl	%cx, %ecx
	subl	%ecx, %eax
	jne	.L363
.L350:
	leaq	1(%rsi), %rax
	cmpq	%rsi, %r8
	jne	.L364
.L349:
	cmpq	-1200(%rbp), %r12
	je	.L354
.L344:
	movq	%r12, %rdi
	movq	%rdx, -1176(%rbp)
	call	uprv_free_67@PLT
	movq	-1176(%rbp), %rdx
.L354:
	cmpq	%r14, %rdx
	je	.L340
.L347:
	movq	%rdx, %rdi
	call	uprv_free_67@PLT
.L340:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L372
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movl	-1176(%rbp), %ecx
	movl	$-1, %r13d
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L363:
	movl	%eax, %r13d
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L369:
	leal	(%rax,%rax), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movl	-1208(%rbp), %esi
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L373
	subq	$8, %rsp
	movl	$0, (%rbx)
	movq	%r15, %r9
	movl	%r13d, %r8d
	pushq	%rbx
	movl	-1176(%rbp), %ecx
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	uidna_IDNToASCII_67
	popq	%r8
	popq	%r9
	movl	%eax, -1176(%rbp)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L370:
	leal	(%rax,%rax), %edi
	movl	%eax, -1208(%rbp)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movl	-1208(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L374
	subq	$8, %rsp
	movl	$0, (%rbx)
	movq	%r15, %r9
	movl	%r13d, %r8d
	movl	-1188(%rbp), %esi
	movq	-1184(%rbp), %rdi
	pushq	%rbx
	movq	%rax, -1208(%rbp)
	call	uidna_IDNToASCII_67
	movl	%eax, %ecx
	popq	%rax
	popq	%rdx
	movq	-1208(%rbp), %rdx
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L357:
	movl	$-1, %r13d
	jmp	.L340
.L372:
	call	__stack_chk_fail@PLT
.L374:
	movl	$7, (%rbx)
	orl	$-1, %r13d
	cmpq	-1200(%rbp), %r12
	jne	.L344
	jmp	.L347
.L373:
	leaq	-576(%rbp), %r14
	movl	$7, (%rbx)
	orl	$-1, %r13d
	movq	%r14, %rdx
	jmp	.L344
	.cfi_endproc
.LFE2147:
	.size	uidna_compare_67, .-uidna_compare_67
	.section	.rodata
	.align 8
	.type	_ZL10ACE_PREFIX, @object
	.size	_ZL10ACE_PREFIX, 8
_ZL10ACE_PREFIX:
	.value	120
	.value	110
	.value	45
	.value	45
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
