	.file	"unistr_case_locale.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString7toLowerEv
	.type	_ZN6icu_6713UnicodeString7toLowerEv, @function
_ZN6icu_6713UnicodeString7toLowerEv:
.LFB2963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	subq	$8, %rsp
	call	ustrcase_getCaseLocale_67@PLT
	movq	ustrcase_internalToLower_67@GOTPCREL(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE@PLT
	.cfi_endproc
.LFE2963:
	.size	_ZN6icu_6713UnicodeString7toLowerEv, .-_ZN6icu_6713UnicodeString7toLowerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString7toLowerERKNS_6LocaleE
	.type	_ZN6icu_6713UnicodeString7toLowerERKNS_6LocaleE, @function
_ZN6icu_6713UnicodeString7toLowerERKNS_6LocaleE:
.LFB2964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%rax, %rdi
	call	ustrcase_getCaseLocale_67@PLT
	movq	ustrcase_internalToLower_67@GOTPCREL(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE@PLT
	.cfi_endproc
.LFE2964:
	.size	_ZN6icu_6713UnicodeString7toLowerERKNS_6LocaleE, .-_ZN6icu_6713UnicodeString7toLowerERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString7toUpperEv
	.type	_ZN6icu_6713UnicodeString7toUpperEv, @function
_ZN6icu_6713UnicodeString7toUpperEv:
.LFB2965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	subq	$8, %rsp
	call	ustrcase_getCaseLocale_67@PLT
	movq	ustrcase_internalToUpper_67@GOTPCREL(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE@PLT
	.cfi_endproc
.LFE2965:
	.size	_ZN6icu_6713UnicodeString7toUpperEv, .-_ZN6icu_6713UnicodeString7toUpperEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString7toUpperERKNS_6LocaleE
	.type	_ZN6icu_6713UnicodeString7toUpperERKNS_6LocaleE, @function
_ZN6icu_6713UnicodeString7toUpperERKNS_6LocaleE:
.LFB2966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%rax, %rdi
	call	ustrcase_getCaseLocale_67@PLT
	movq	ustrcase_internalToUpper_67@GOTPCREL(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE@PLT
	.cfi_endproc
.LFE2966:
	.size	_ZN6icu_6713UnicodeString7toUpperERKNS_6LocaleE, .-_ZN6icu_6713UnicodeString7toUpperERKNS_6LocaleE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
