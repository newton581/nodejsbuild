	.file	"uset.cpp"
	.text
	.p2align 4
	.globl	uset_openEmpty_67
	.type	uset_openEmpty_67, @function
uset_openEmpty_67:
.LFB2332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$200, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
.L1:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2332:
	.size	uset_openEmpty_67, .-uset_openEmpty_67
	.p2align 4
	.globl	uset_open_67
	.type	uset_open_67, @function
uset_open_67:
.LFB2333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edi, %r13d
	movl	$200, %edi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
.L8:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2333:
	.size	uset_open_67, .-uset_open_67
	.p2align 4
	.globl	uset_isFrozen_67
	.type	uset_isFrozen_67, @function
uset_isFrozen_67:
.LFB2336:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	movl	$1, %eax
	je	.L17
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	cmpq	$0, 88(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE2336:
	.size	uset_isFrozen_67, .-uset_isFrozen_67
	.p2align 4
	.globl	uset_freeze_67
	.type	uset_freeze_67, @function
uset_freeze_67:
.LFB2337:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	.cfi_endproc
.LFE2337:
	.size	uset_freeze_67, .-uset_freeze_67
	.p2align 4
	.globl	uset_cloneAsThawed_67
	.type	uset_cloneAsThawed_67, @function
uset_cloneAsThawed_67:
.LFB2338:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet13cloneAsThawedEv@PLT
	.cfi_endproc
.LFE2338:
	.size	uset_cloneAsThawed_67, .-uset_cloneAsThawed_67
	.p2align 4
	.globl	uset_set_67
	.type	uset_set_67, @function
uset_set_67:
.LFB2339:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet3setEii@PLT
	.cfi_endproc
.LFE2339:
	.size	uset_set_67, .-uset_set_67
	.p2align 4
	.globl	uset_add_67
	.type	uset_add_67, @function
uset_add_67:
.LFB2341:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet3addEi@PLT
	.cfi_endproc
.LFE2341:
	.size	uset_add_67, .-uset_add_67
	.p2align 4
	.globl	uset_remove_67
	.type	uset_remove_67, @function
uset_remove_67:
.LFB2345:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet6removeEi@PLT
	.cfi_endproc
.LFE2345:
	.size	uset_remove_67, .-uset_remove_67
	.p2align 4
	.globl	uset_containsNone_67
	.type	uset_containsNone_67, @function
uset_containsNone_67:
.LFB2362:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet12containsNoneERKS0_@PLT
	.cfi_endproc
.LFE2362:
	.size	uset_containsNone_67, .-uset_containsNone_67
	.p2align 4
	.globl	uset_containsSome_67
	.type	uset_containsSome_67, @function
uset_containsSome_67:
.LFB2363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6710UnicodeSet12containsNoneERKS0_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	sete	%al
	ret
	.cfi_endproc
.LFE2363:
	.size	uset_containsSome_67, .-uset_containsSome_67
	.p2align 4
	.globl	uset_span_67
	.type	uset_span_67, @function
uset_span_67:
.LFB2364:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	.cfi_endproc
.LFE2364:
	.size	uset_span_67, .-uset_span_67
	.p2align 4
	.globl	uset_spanBack_67
	.type	uset_spanBack_67, @function
uset_spanBack_67:
.LFB2365:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition@PLT
	.cfi_endproc
.LFE2365:
	.size	uset_spanBack_67, .-uset_spanBack_67
	.p2align 4
	.globl	uset_spanUTF8_67
	.type	uset_spanUTF8_67, @function
uset_spanUTF8_67:
.LFB2366:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition@PLT
	.cfi_endproc
.LFE2366:
	.size	uset_spanUTF8_67, .-uset_spanUTF8_67
	.p2align 4
	.globl	uset_spanBackUTF8_67
	.type	uset_spanBackUTF8_67, @function
uset_spanBackUTF8_67:
.LFB2367:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet12spanBackUTF8EPKci17USetSpanCondition@PLT
	.cfi_endproc
.LFE2367:
	.size	uset_spanBackUTF8_67, .-uset_spanBackUTF8_67
	.p2align 4
	.globl	uset_indexOf_67
	.type	uset_indexOf_67, @function
uset_indexOf_67:
.LFB2369:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet7indexOfEi@PLT
	.cfi_endproc
.LFE2369:
	.size	uset_indexOf_67, .-uset_indexOf_67
	.p2align 4
	.globl	uset_charAt_67
	.type	uset_charAt_67, @function
uset_charAt_67:
.LFB2370:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet6charAtEi@PLT
	.cfi_endproc
.LFE2370:
	.size	uset_charAt_67, .-uset_charAt_67
	.p2align 4
	.globl	uset_serialize_67
	.type	uset_serialize_67, @function
uset_serialize_67:
.LFB2376:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L32
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L37
.L32:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	jmp	_ZNK6icu_6710UnicodeSet9serializeEPtiR10UErrorCode@PLT
	.cfi_endproc
.LFE2376:
	.size	uset_serialize_67, .-uset_serialize_67
	.p2align 4
	.globl	uset_getSerializedSet_67
	.type	uset_getSerializedSet_67, @function
uset_getSerializedSet_67:
.LFB2377:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L45
	testq	%rsi, %rsi
	je	.L43
	testl	%edx, %edx
	jle	.L43
	movzwl	(%rsi), %eax
	testw	%ax, %ax
	js	.L46
	cmpl	%eax, %edx
	jle	.L43
	leaq	2(%rsi), %rdx
	movl	%eax, 8(%rdi)
	movl	%eax, 12(%rdi)
	movl	$1, %eax
	movq	%rdx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	andl	$32767, %eax
	leal	1(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L43
	movzwl	2(%rsi), %ecx
	leaq	4(%rsi), %rdx
	movl	%eax, 12(%rdi)
	movl	$1, %eax
	movq	%rdx, (%rdi)
	movl	%ecx, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	$0, 8(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2377:
	.size	uset_getSerializedSet_67, .-uset_getSerializedSet_67
	.p2align 4
	.globl	uset_setSerializedToOne_67
	.type	uset_setSerializedToOne_67, @function
uset_setSerializedToOne_67:
.LFB2378:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L47
	cmpl	$1114111, %esi
	ja	.L47
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	cmpl	$65534, %esi
	jle	.L52
	cmpl	$65535, %esi
	je	.L53
	cmpl	$1114111, %esi
	je	.L51
	movabsq	$17179869184, %rax
	movw	%si, 18(%rdi)
	movq	%rax, 8(%rdi)
	movl	%esi, %eax
	addl	$1, %esi
	sarl	$16, %eax
	movw	%si, 22(%rdi)
	movw	%ax, 16(%rdi)
	movl	%esi, %eax
	sarl	$16, %eax
	movw	%ax, 20(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movabsq	$8589934592, %rax
	movl	$-65520, 16(%rdi)
	movq	%rax, 8(%rdi)
.L47:
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	movabsq	$8589934594, %rax
	movw	%si, 16(%rdi)
	addl	$1, %esi
	movq	%rax, 8(%rdi)
	movw	%si, 18(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movabsq	$12884901889, %rax
	movl	$131071, 16(%rdi)
	movq	%rax, 8(%rdi)
	xorl	%eax, %eax
	movw	%ax, 20(%rdi)
	ret
	.cfi_endproc
.LFE2378:
	.size	uset_setSerializedToOne_67, .-uset_setSerializedToOne_67
	.p2align 4
	.globl	uset_serializedContains_67
	.type	uset_serializedContains_67, @function
uset_serializedContains_67:
.LFB2379:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L68
	cmpl	$1114111, %esi
	ja	.L68
	movq	(%rdi), %rcx
	movl	8(%rdi), %r9d
	cmpl	$65535, %esi
	jg	.L56
	movzwl	(%rcx), %edx
	xorl	%eax, %eax
	cmpl	%esi, %edx
	jg	.L81
	leal	-1(%r9), %edx
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %eax
	cmpl	%esi, %eax
	jg	.L70
	movl	%r9d, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movslq	%r9d, %rdx
	xorl	%r8d, %r8d
	sarl	$16, %eax
	movl	%eax, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	(%rdx,%rdx), %rbx
	cmpw	%ax, (%rcx,%rdx,2)
	ja	.L61
	movl	%esi, %r11d
	je	.L83
.L62:
	movl	12(%rdi), %edx
	subl	$2, %edx
	movl	%edx, %r8d
	movslq	%edx, %rdx
	subl	%r9d, %r8d
	leaq	(%rdx,%rdx), %rdi
	cmpw	%ax, (%rcx,%rdx,2)
	ja	.L66
	je	.L84
.L65:
	addl	$2, %r8d
.L61:
	leal	(%r8,%r9,2), %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrl	%eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%edi, %edi
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L59:
	movslq	%edx, %r8
	movzwl	(%rcx,%r8,2), %r8d
	cmpl	%esi, %r8d
	jle	.L85
.L60:
	movl	%edx, %eax
	leal	(%rdi,%rdx), %edx
	sarl	%edx
	cmpl	%edx, %edi
	jne	.L59
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpw	%si, 2(%rcx,%rdi)
	jbe	.L65
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%r8d, %eax
	sarl	%eax
	andl	$-2, %eax
	leal	(%rax,%r9), %edx
	je	.L61
	xorl	%esi, %esi
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L87:
	je	.L86
	movl	%eax, %esi
.L67:
	leal	(%r8,%rsi), %eax
	sarl	%eax
	andl	$-2, %eax
	leal	(%rax,%r9), %edx
	cmpl	%eax, %esi
	je	.L61
.L64:
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx), %rdi
	cmpw	%r10w, (%rcx,%rdx,2)
	jbe	.L87
	movl	%eax, %r8d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L86:
	cmpw	%r11w, 2(%rcx,%rdi)
	cmovbe	%eax, %esi
	cmova	%eax, %r8d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	%edx, %edi
	movl	%eax, %edx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpw	%si, 2(%rcx,%rbx)
	ja	.L61
	jmp	.L62
	.cfi_endproc
.LFE2379:
	.size	uset_serializedContains_67, .-uset_serializedContains_67
	.p2align 4
	.globl	uset_getSerializedRangeCount_67
	.type	uset_getSerializedRangeCount_67, @function
uset_getSerializedRangeCount_67:
.LFB2380:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L90
	movl	8(%rdi), %ecx
	movl	12(%rdi), %eax
	subl	%ecx, %eax
	movl	%eax, %edx
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	leal	1(%rcx,%rax), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2380:
	.size	uset_getSerializedRangeCount_67, .-uset_getSerializedRangeCount_67
	.p2align 4
	.globl	uset_getSerializedRange_67
	.type	uset_getSerializedRange_67, @function
uset_getSerializedRange_67:
.LFB2381:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L97
	testl	%esi, %esi
	js	.L97
	testq	%rdx, %rdx
	je	.L97
	testq	%rcx, %rcx
	je	.L97
	movq	(%rdi), %r9
	movl	12(%rdi), %eax
	addl	%esi, %esi
	movslq	8(%rdi), %rdi
	cmpl	%esi, %edi
	jg	.L99
	subl	%edi, %eax
	subl	%edi, %esi
	movl	%eax, %r8d
	addl	%esi, %esi
	xorl	%eax, %eax
	cmpl	%r8d, %esi
	jge	.L91
	leaq	(%r9,%rdi,2), %r9
	movslq	%esi, %rdi
	addl	$2, %esi
	movzwl	2(%r9,%rdi,2), %eax
	leaq	(%rdi,%rdi), %r10
	movzwl	(%r9,%rdi,2), %edi
	sall	$16, %edi
	orl	%edi, %eax
	movl	%eax, (%rdx)
	cmpl	%esi, %r8d
	jle	.L95
	movzwl	4(%r9,%r10), %eax
	movslq	%esi, %rsi
	movzwl	2(%r9,%rsi,2), %edx
	sall	$16, %eax
	orl	%edx, %eax
	subl	$1, %eax
	movl	%eax, (%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%eax, %eax
.L91:
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	leal	1(%rsi), %r8d
	movslq	%esi, %rsi
	movzwl	(%r9,%rsi,2), %esi
	movl	%esi, (%rdx)
	cmpl	%r8d, %edi
	jle	.L94
	movslq	%r8d, %r8
	movzwl	(%r9,%r8,2), %eax
	subl	$1, %eax
	movl	%eax, (%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	cmpl	%r8d, %eax
	jg	.L100
.L95:
	movl	$1114111, (%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	movslq	%r8d, %r8
	movzwl	(%r9,%r8,2), %eax
	movzwl	2(%r9,%r8,2), %edx
	sall	$16, %eax
	orl	%edx, %eax
	subl	$1, %eax
	movl	%eax, (%rcx)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2381:
	.size	uset_getSerializedRange_67, .-uset_getSerializedRange_67
	.p2align 4
	.globl	uset_close_67
	.type	uset_close_67, @function
uset_close_67:
.LFB2334:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L101
	jmp	_ZN6icu_6710UnicodeSetD0Ev@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	ret
	.cfi_endproc
.LFE2334:
	.size	uset_close_67, .-uset_close_67
	.p2align 4
	.globl	uset_clone_67
	.type	uset_clone_67, @function
uset_clone_67:
.LFB2335:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet5cloneEv@PLT
	.cfi_endproc
.LFE2335:
	.size	uset_clone_67, .-uset_clone_67
	.p2align 4
	.globl	uset_addAll_67
	.type	uset_addAll_67, @function
uset_addAll_67:
.LFB2340:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	.cfi_endproc
.LFE2340:
	.size	uset_addAll_67, .-uset_addAll_67
	.p2align 4
	.globl	uset_addRange_67
	.type	uset_addRange_67, @function
uset_addRange_67:
.LFB2342:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet3addEii@PLT
	.cfi_endproc
.LFE2342:
	.size	uset_addRange_67, .-uset_addRange_67
	.p2align 4
	.globl	uset_addString_67
	.type	uset_addString_67, @function
uset_addString_67:
.LFB2343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	movl	%ecx, %esi
	shrl	$31, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L109:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2343:
	.size	uset_addString_67, .-uset_addString_67
	.p2align 4
	.globl	uset_addAllCodePoints_67
	.type	uset_addAllCodePoints_67, @function
uset_addAllCodePoints_67:
.LFB2344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L113:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2344:
	.size	uset_addAllCodePoints_67, .-uset_addAllCodePoints_67
	.p2align 4
	.globl	uset_removeString_67
	.type	uset_removeString_67, @function
uset_removeString_67:
.LFB2347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	xorl	%esi, %esi
	cmpl	$-1, %ecx
	sete	%sil
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6removeERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L117:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2347:
	.size	uset_removeString_67, .-uset_removeString_67
	.p2align 4
	.globl	uset_containsString_67
	.type	uset_containsString_67, @function
uset_containsString_67:
.LFB2359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	xorl	%esi, %esi
	cmpl	$-1, %ecx
	sete	%sil
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$96, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L121:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2359:
	.size	uset_containsString_67, .-uset_containsString_67
	.p2align 4
	.globl	uset_containsAllCodePoints_67
	.type	uset_containsAllCodePoints_67, @function
uset_containsAllCodePoints_67:
.LFB2361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	xorl	%esi, %esi
	cmpl	$-1, %ecx
	sete	%sil
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_6710UnicodeSet11containsAllERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$96, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L125:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2361:
	.size	uset_containsAllCodePoints_67, .-uset_containsAllCodePoints_67
	.p2align 4
	.globl	uset_removeRange_67
	.type	uset_removeRange_67, @function
uset_removeRange_67:
.LFB2346:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet6removeEii@PLT
	.cfi_endproc
.LFE2346:
	.size	uset_removeRange_67, .-uset_removeRange_67
	.p2align 4
	.globl	uset_removeAll_67
	.type	uset_removeAll_67, @function
uset_removeAll_67:
.LFB2348:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	.cfi_endproc
.LFE2348:
	.size	uset_removeAll_67, .-uset_removeAll_67
	.p2align 4
	.globl	uset_retain_67
	.type	uset_retain_67, @function
uset_retain_67:
.LFB2349:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet6retainEii@PLT
	.cfi_endproc
.LFE2349:
	.size	uset_retain_67, .-uset_retain_67
	.p2align 4
	.globl	uset_retainAll_67
	.type	uset_retainAll_67, @function
uset_retainAll_67:
.LFB2350:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet9retainAllERKS0_@PLT
	.cfi_endproc
.LFE2350:
	.size	uset_retainAll_67, .-uset_retainAll_67
	.p2align 4
	.globl	uset_compact_67
	.type	uset_compact_67, @function
uset_compact_67:
.LFB2351:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet7compactEv@PLT
	.cfi_endproc
.LFE2351:
	.size	uset_compact_67, .-uset_compact_67
	.p2align 4
	.globl	uset_complement_67
	.type	uset_complement_67, @function
uset_complement_67:
.LFB2352:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet10complementEv@PLT
	.cfi_endproc
.LFE2352:
	.size	uset_complement_67, .-uset_complement_67
	.p2align 4
	.globl	uset_complementAll_67
	.type	uset_complementAll_67, @function
uset_complementAll_67:
.LFB2353:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet13complementAllERKS0_@PLT
	.cfi_endproc
.LFE2353:
	.size	uset_complementAll_67, .-uset_complementAll_67
	.p2align 4
	.globl	uset_clear_67
	.type	uset_clear_67, @function
uset_clear_67:
.LFB2354:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet5clearEv@PLT
	.cfi_endproc
.LFE2354:
	.size	uset_clear_67, .-uset_clear_67
	.p2align 4
	.globl	uset_removeAllStrings_67
	.type	uset_removeAllStrings_67, @function
uset_removeAllStrings_67:
.LFB2355:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet16removeAllStringsEv@PLT
	.cfi_endproc
.LFE2355:
	.size	uset_removeAllStrings_67, .-uset_removeAllStrings_67
	.p2align 4
	.globl	uset_isEmpty_67
	.type	uset_isEmpty_67, @function
uset_isEmpty_67:
.LFB2356:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet7isEmptyEv@PLT
	.cfi_endproc
.LFE2356:
	.size	uset_isEmpty_67, .-uset_isEmpty_67
	.p2align 4
	.globl	uset_contains_67
	.type	uset_contains_67, @function
uset_contains_67:
.LFB2357:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	.cfi_endproc
.LFE2357:
	.size	uset_contains_67, .-uset_contains_67
	.p2align 4
	.globl	uset_containsRange_67
	.type	uset_containsRange_67, @function
uset_containsRange_67:
.LFB2358:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet8containsEii@PLT
	.cfi_endproc
.LFE2358:
	.size	uset_containsRange_67, .-uset_containsRange_67
	.p2align 4
	.globl	uset_containsAll_67
	.type	uset_containsAll_67, @function
uset_containsAll_67:
.LFB2360:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet11containsAllERKS0_@PLT
	.cfi_endproc
.LFE2360:
	.size	uset_containsAll_67, .-uset_containsAll_67
	.p2align 4
	.globl	uset_equals_67
	.type	uset_equals_67, @function
uset_equals_67:
.LFB2368:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSeteqERKS0_@PLT
	.cfi_endproc
.LFE2368:
	.size	uset_equals_67, .-uset_equals_67
	.p2align 4
	.globl	uset_size_67
	.type	uset_size_67, @function
uset_size_67:
.LFB2371:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_6710UnicodeSet4sizeEv@PLT
	.cfi_endproc
.LFE2371:
	.size	uset_size_67, .-uset_size_67
	.p2align 4
	.globl	uset_getItemCount_67
	.type	uset_getItemCount_67, @function
uset_getItemCount_67:
.LFB2374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_6710UnicodeSet11stringsSizeEv@PLT
	addl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2374:
	.size	uset_getItemCount_67, .-uset_getItemCount_67
	.p2align 4
	.globl	uset_getItem_67
	.type	uset_getItem_67, @function
uset_getItem_67:
.LFB2375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -80(%rbp)
	movq	16(%rbp), %r15
	movq	%rcx, -88(%rbp)
	movl	%r9d, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L143
	movl	%esi, %r12d
	testl	%esi, %esi
	js	.L151
	movq	%rdi, %r13
	movq	%r8, %rbx
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	cmpl	%eax, %r12d
	jl	.L152
	movq	%r13, %rdi
	subl	%eax, %r12d
	call	_ZNK6icu_6710UnicodeSet11stringsSizeEv@PLT
	cmpl	%eax, %r12d
	jge	.L147
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet9getStringEi@PLT
	movl	-68(%rbp), %edx
	leaq	-64(%rbp), %rsi
	movq	%r15, %rcx
	movq	%rax, %rdi
	movq	%rbx, -64(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r14d
.L143:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	-80(%rbp), %rbx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%eax, (%rbx)
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movq	-88(%rbp), %rbx
	movl	%eax, (%rbx)
	jmp	.L143
.L151:
	movl	$1, (%r15)
	movl	$-1, %r14d
	jmp	.L143
.L147:
	movl	$8, (%r15)
	movl	$-1, %r14d
	jmp	.L143
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2375:
	.size	uset_getItem_67, .-uset_getItem_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
