	.file	"ustrtrns.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	" 000000000000\02000"
	.text
	.p2align 4
	.type	u_strFromUTF8WithSub_67.part.0, @function
u_strFromUTF8WithSub_67.part.0:
.LFB2529:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	16(%rbp), %rax
	movq	24(%rbp), %rdi
	movl	%esi, -92(%rbp)
	movq	%rdx, -104(%rbp)
	movl	%r9d, -68(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rdi, -88(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L2
	movl	$0, (%rax)
.L2:
	movslq	-92(%rbp), %rbx
	movq	-80(%rbp), %rax
	movl	$0, -60(%rbp)
	addq	%rbx, %rbx
	leaq	(%rax,%rbx), %r13
	cmpl	$-1, %r12d
	je	.L144
	movslq	%r12d, %rax
	movl	%r12d, %ecx
	sarq	%rbx
	movl	$0, -72(%rbp)
	imulq	$1431655766, %rax, %rax
	sarl	$31, %ecx
	movq	-80(%rbp), %r15
	shrq	$32, %rax
	subl	%ecx, %eax
	cmpl	%ebx, %eax
	cmovle	%eax, %ebx
	xorl	%eax, %eax
	cmpl	$2, %ebx
	jg	.L51
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L145:
	movw	%dx, (%r15)
	movl	%ecx, %eax
	addq	$2, %r15
.L38:
	subl	$1, %ebx
	je	.L47
.L51:
	movslq	%eax, %rdx
	leal	1(%rax), %ecx
	movzbl	(%r14,%rdx), %edx
	movl	%ecx, -60(%rbp)
	testb	%dl, %dl
	jns	.L145
	movzbl	%dl, %r9d
	leal	-224(%r9), %esi
	cmpl	$15, %esi
	ja	.L39
	leal	2(%rax), %esi
	cmpl	%esi, %r12d
	jle	.L40
	movslq	%ecx, %rcx
	andl	$15, %edx
	leaq	.LC0(%rip), %rdi
	movzbl	(%r14,%rcx), %esi
	movsbl	(%rdi,%rdx), %edi
	movl	%esi, %edx
	shrb	$5, %dl
	btl	%edx, %edi
	jnc	.L40
	movzbl	1(%r14,%rcx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L40
	sall	$6, %esi
	movzbl	%dl, %edx
	sall	$12, %r9d
	addl	$3, %eax
	andw	$4032, %si
	movl	%eax, -60(%rbp)
	addq	$2, %r15
	orl	%edx, %esi
	orl	%r9d, %esi
	movw	%si, -2(%r15)
	subl	$1, %ebx
	jne	.L51
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r13, %rbx
	movl	%r12d, %ecx
	subq	%r15, %rbx
	subl	%eax, %ecx
	sarq	%rbx
	movq	%rbx, %rdx
	movslq	%ecx, %rbx
	sarl	$31, %ecx
	imulq	$1431655766, %rbx, %rbx
	shrq	$32, %rbx
	subl	%ecx, %ebx
	cmpl	%edx, %ebx
	cmovg	%edx, %ebx
	cmpl	$2, %ebx
	jg	.L51
.L33:
	cmpl	%eax, %r12d
	jle	.L142
	cmpq	%r15, %r13
	jbe	.L142
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rbx
	movq	%r13, %r14
	movq	%rdi, %r13
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L146:
	movw	%dx, (%r15)
	movl	%esi, %eax
	addq	$2, %r15
.L53:
	cmpl	%eax, %r12d
	jle	.L84
	cmpq	%r15, %r14
	jbe	.L84
.L36:
	movslq	%eax, %rcx
	leal	1(%rax), %esi
	movzbl	0(%r13,%rcx), %edx
	movl	%esi, -60(%rbp)
	testb	%dl, %dl
	jns	.L146
	movzbl	%dl, %ecx
	leal	-224(%rcx), %r8d
	cmpl	$15, %r8d
	ja	.L54
	leal	2(%rax), %r8d
	cmpl	%r8d, %r12d
	jle	.L55
	movslq	%esi, %rsi
	andl	$15, %edx
	movzbl	0(%r13,%rsi), %r8d
	movsbl	(%rbx,%rdx), %r9d
	movl	%r8d, %edx
	shrb	$5, %dl
	btl	%edx, %r9d
	jnc	.L55
	movzbl	1(%r13,%rsi), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L55
	movl	%r8d, %esi
	movzbl	%dl, %edx
	sall	$12, %ecx
	addl	$3, %eax
	sall	$6, %esi
	movl	%eax, -60(%rbp)
	addq	$2, %r15
	andw	$4032, %si
	orl	%edx, %esi
	orl	%ecx, %esi
	movw	%si, -2(%r15)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L40:
	cmpl	$239, %r9d
	jg	.L45
	cmpl	$65535, -68(%rbp)
	jle	.L46
.L45:
	subl	$1, %ebx
	je	.L147
.L46:
	leaq	-60(%rbp), %rsi
	movl	$-1, %r8d
	movl	%r9d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rdi
	call	utf8_nextCharSafeBody_67@PLT
	testl	%eax, %eax
	js	.L148
.L48:
	cmpl	$65535, %eax
	jg	.L50
	movw	%ax, (%r15)
	movl	-60(%rbp), %eax
	addq	$2, %r15
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	leal	-194(%r9), %edx
	cmpl	$29, %edx
	ja	.L40
	cmpl	%ecx, %r12d
	je	.L40
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L40
	sall	$6, %r9d
	movzbl	%dl, %edx
	addl	$2, %eax
	addq	$2, %r15
	andw	$1984, %r9w
	movl	%eax, -60(%rbp)
	orl	%r9d, %edx
	movw	%dx, -2(%r15)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L50:
	movl	%eax, %edx
	andw	$1023, %ax
	addq	$4, %r15
	sarl	$10, %edx
	orw	$-9216, %ax
	subw	$10304, %dx
	movw	%ax, -2(%r15)
	movl	-60(%rbp), %eax
	movw	%dx, -4(%r15)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L148:
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	js	.L49
	addl	$1, -72(%rbp)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-88(%rbp), %rax
	movl	$10, (%rax)
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L149
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L147:
	.cfi_restore_state
	movl	%eax, -60(%rbp)
	jmp	.L47
.L144:
	movzbl	(%r14), %ecx
	movl	%ecx, %eax
	testb	%cl, %cl
	je	.L87
	movq	-80(%rbp), %rdi
	movl	-68(%rbp), %r12d
	movl	$0, -72(%rbp)
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rbx
	movq	%rdi, %r15
	cmpq	%r13, %rdi
	jb	.L4
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L150:
	movw	%ax, (%r15)
	movl	%esi, %edx
	addq	$2, %r15
.L8:
	movslq	%edx, %rax
	movzbl	(%r14,%rax), %ecx
	movl	%ecx, %eax
	testb	%cl, %cl
	je	.L80
.L151:
	cmpq	%r15, %r13
	jbe	.L80
.L4:
	leal	1(%rdx), %esi
	movl	%esi, -60(%rbp)
	testb	%al, %al
	jns	.L150
	leal	-224(%rcx), %edi
	cmpl	$15, %edi
	ja	.L9
	movslq	%esi, %rsi
	andl	$15, %eax
	movzbl	(%r14,%rsi), %edi
	movsbl	(%rbx,%rax), %r8d
	movl	%edi, %eax
	shrb	$5, %al
	btl	%eax, %r8d
	jnc	.L10
	movzbl	1(%r14,%rsi), %esi
	addl	$-128, %esi
	cmpb	$63, %sil
	ja	.L10
	movl	%edi, %eax
	movzbl	%sil, %esi
	sall	$12, %ecx
	addl	$3, %edx
	sall	$6, %eax
	movl	%edx, -60(%rbp)
	addq	$2, %r15
	andw	$4032, %ax
	orl	%esi, %eax
	orl	%ecx, %eax
	movw	%ax, -2(%r15)
	movslq	%edx, %rax
	movzbl	(%r14,%rax), %ecx
	movl	%ecx, %eax
	testb	%cl, %cl
	jne	.L151
.L80:
	xorl	%r13d, %r13d
.L6:
	movl	-68(%rbp), %r12d
	leaq	.LC0(%rip), %rbx
	testb	%al, %al
	jne	.L20
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L152:
	addl	$1, %r13d
	movl	%esi, %edx
.L23:
	movslq	%edx, %rax
	movzbl	(%r14,%rax), %ecx
	movl	%ecx, %eax
	testb	%cl, %cl
	je	.L32
.L20:
	leal	1(%rdx), %esi
	movl	%esi, -60(%rbp)
	testb	%al, %al
	jns	.L152
	leal	-224(%rcx), %edi
	cmpl	$15, %edi
	ja	.L24
	andl	$15, %eax
	movslq	%esi, %rsi
	movsbl	(%rbx,%rax), %edi
	movzbl	(%r14,%rsi), %eax
	sarl	$5, %eax
	btl	%eax, %edi
	jnc	.L25
	movzbl	1(%r14,%rsi), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L25
	addl	$3, %edx
	addl	$1, %r13d
	movslq	%edx, %rax
	movl	%edx, -60(%rbp)
	movzbl	(%r14,%rax), %ecx
	movl	%ecx, %eax
	testb	%cl, %cl
	jne	.L20
.L32:
	movq	%r15, %r9
	movq	-112(%rbp), %rax
	subq	-80(%rbp), %r9
	sarq	%r9
	leal	0(%r13,%r9), %edx
	testq	%rax, %rax
	je	.L76
	movl	-72(%rbp), %edi
	movl	%edi, (%rax)
.L76:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L77
	movl	%edx, (%rax)
.L77:
	movq	-80(%rbp), %rbx
	movq	-88(%rbp), %rcx
	movl	-92(%rbp), %esi
	movq	%rbx, %rdi
	call	u_terminateUChars_67@PLT
	movq	%rbx, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	leal	-194(%rcx), %eax
	cmpl	$29, %eax
	ja	.L10
	movslq	%esi, %rsi
	movzbl	(%r14,%rsi), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L10
	sall	$6, %ecx
	movzbl	%al, %eax
	addl	$2, %edx
	addq	$2, %r15
	andw	$1984, %cx
	movl	%edx, -60(%rbp)
	orl	%ecx, %eax
	movw	%ax, -2(%r15)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	-60(%rbp), %rsi
	movl	$-1, %r8d
	movl	$-1, %edx
	movq	%r14, %rdi
	call	utf8_nextCharSafeBody_67@PLT
	testl	%eax, %eax
	js	.L153
.L15:
	leaq	2(%r15), %rsi
	cmpl	$65535, %eax
	jg	.L18
	movw	%ax, (%r15)
	movl	-60(%rbp), %edx
	movq	%rsi, %r15
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L18:
	movl	%eax, %edx
	sarl	$10, %edx
	subw	$10304, %dx
	movw	%dx, (%r15)
	movl	-60(%rbp), %edx
	cmpq	%rsi, %r13
	jbe	.L154
	andw	$1023, %ax
	addq	$4, %r15
	orw	$-9216, %ax
	movw	%ax, -2(%r15)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L153:
	testl	%r12d, %r12d
	js	.L49
	addl	$1, -72(%rbp)
	movl	%r12d, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L24:
	leal	-194(%rcx), %eax
	cmpl	$29, %eax
	ja	.L25
	movslq	%esi, %rsi
	movzbl	(%r14,%rsi), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L25
	addl	$2, %edx
	addl	$1, %r13d
	movl	%edx, -60(%rbp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	-60(%rbp), %rsi
	movl	$-1, %r8d
	movl	$-1, %edx
	movq	%r14, %rdi
	call	utf8_nextCharSafeBody_67@PLT
	testl	%eax, %eax
	js	.L155
.L30:
	cmpl	$65535, %eax
	movl	-60(%rbp), %edx
	setg	%al
	movzbl	%al, %eax
	leal	1(%rax,%r13), %r13d
	jmp	.L23
.L155:
	testl	%r12d, %r12d
	js	.L49
	addl	$1, -72(%rbp)
	movl	%r12d, %eax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L55:
	movl	%r12d, %edx
	leaq	-60(%rbp), %rsi
	movl	$-1, %r8d
	movq	%r13, %rdi
	call	utf8_nextCharSafeBody_67@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L156
.L61:
	leaq	2(%r15), %rsi
	cmpl	$65535, %edx
	jg	.L62
	movw	%dx, (%r15)
	movl	-60(%rbp), %eax
	movq	%rsi, %r15
	jmp	.L53
.L54:
	leal	-194(%rcx), %edx
	cmpl	$29, %edx
	ja	.L55
	cmpl	%esi, %r12d
	je	.L55
	movslq	%esi, %rdx
	movzbl	0(%r13,%rdx), %esi
	addl	$-128, %esi
	cmpb	$63, %sil
	ja	.L55
	sall	$6, %ecx
	movzbl	%sil, %esi
	addl	$2, %eax
	addq	$2, %r15
	movl	%ecx, %edx
	movl	%eax, -60(%rbp)
	andw	$1984, %dx
	orl	%esi, %edx
	movw	%dx, -2(%r15)
	jmp	.L53
.L62:
	movl	%edx, %eax
	sarl	$10, %eax
	subw	$10304, %ax
	movw	%ax, (%r15)
	movl	-60(%rbp), %eax
	cmpq	%rsi, %r14
	jbe	.L85
	andw	$1023, %dx
	addq	$4, %r15
	orw	$-9216, %dx
	movw	%dx, -2(%r15)
	jmp	.L53
.L156:
	movl	-68(%rbp), %edx
	testl	%edx, %edx
	js	.L49
	addl	$1, -72(%rbp)
	jmp	.L61
.L84:
	movq	%r13, %r14
.L142:
	xorl	%r13d, %r13d
.L35:
	leaq	.LC0(%rip), %rbx
	cmpl	%eax, %r12d
	jg	.L64
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L157:
	addl	$1, %r13d
	movl	%edx, %eax
.L66:
	cmpl	%eax, %r12d
	jle	.L32
.L64:
	movslq	%eax, %rcx
	leal	1(%rax), %edx
	movzbl	(%r14,%rcx), %ecx
	movl	%edx, -60(%rbp)
	testb	%cl, %cl
	jns	.L157
	movzbl	%cl, %r10d
	leal	-224(%r10), %esi
	cmpl	$15, %esi
	ja	.L67
	leal	2(%rax), %esi
	cmpl	%esi, %r12d
	jle	.L68
	andl	$15, %ecx
	movslq	%edx, %rdx
	movsbl	(%rbx,%rcx), %esi
	movzbl	(%r14,%rdx), %ecx
	sarl	$5, %ecx
	btl	%ecx, %esi
	jnc	.L68
	movzbl	1(%r14,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	jbe	.L69
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	-60(%rbp), %rsi
	movl	$-1, %r8d
	movl	%r10d, %ecx
	movl	%r12d, %edx
	movq	%r14, %rdi
	call	utf8_nextCharSafeBody_67@PLT
	testl	%eax, %eax
	js	.L158
.L74:
	cmpl	$65535, %eax
	setg	%al
	movzbl	%al, %eax
	leal	1(%rax,%r13), %r13d
	movl	-60(%rbp), %eax
	jmp	.L66
.L67:
	leal	-194(%r10), %ecx
	cmpl	$29, %ecx
	ja	.L68
	cmpl	%edx, %r12d
	je	.L68
	movslq	%edx, %rdx
	movzbl	(%r14,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L68
	addl	$2, %eax
	addl	$1, %r13d
	movl	%eax, -60(%rbp)
	jmp	.L66
.L158:
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	js	.L49
	addl	$1, -72(%rbp)
	jmp	.L74
.L69:
	addl	$3, %eax
	addl	$1, %r13d
	movl	%eax, -60(%rbp)
	jmp	.L66
.L87:
	movl	$0, -72(%rbp)
	movq	-80(%rbp), %r15
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	jmp	.L6
.L154:
	movslq	%edx, %rax
	movq	%rsi, %r15
	movl	$1, %r13d
	movzbl	(%r14,%rax), %ecx
	movl	%ecx, %eax
	jmp	.L6
.L85:
	movq	%r13, %r14
	movq	%rsi, %r15
	movl	$1, %r13d
	jmp	.L35
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2529:
	.size	u_strFromUTF8WithSub_67.part.0, .-u_strFromUTF8WithSub_67.part.0
	.p2align 4
	.globl	u_strFromUTF32WithSub_67
	.type	u_strFromUTF32WithSub_67, @function
u_strFromUTF32WithSub_67:
.LFB2054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L189
	testq	%rcx, %rcx
	jne	.L195
	testl	%r8d, %r8d
	jne	.L161
.L195:
	cmpl	$-1, %r8d
	jl	.L161
	testl	%esi, %esi
	js	.L161
	testq	%rdi, %rdi
	sete	%r10b
	testl	%esi, %esi
	setg	%al
	testb	%al, %r10b
	jne	.L161
	cmpl	$1114111, %r9d
	jg	.L161
	movl	%r9d, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L161
	cmpq	$0, 16(%rbp)
	je	.L164
	movq	16(%rbp), %rax
	movl	$0, (%rax)
.L164:
	xorl	%r12d, %r12d
	testq	%rdi, %rdi
	je	.L165
	movslq	%esi, %rax
	leaq	(%rdi,%rax,2), %r12
.L165:
	cmpl	$-1, %r8d
	je	.L221
	testq	%rcx, %rcx
	je	.L194
	movslq	%r8d, %r8
	movq	%rdi, %r10
	xorl	%ebx, %ebx
	leaq	(%rcx,%r8,4), %r11
.L174:
	cmpq	%rcx, %r11
	jbe	.L220
	leal	-57344(%r9), %eax
	movq	%rdi, -56(%rbp)
	leal	-65536(%r9), %r15d
	cmpl	$8191, %eax
	setbe	%r13b
	cmpl	$55295, %r9d
	setbe	%al
	orl	%eax, %r13d
	xorl	%eax, %eax
.L176:
	movl	(%rcx), %edi
	addq	$4, %rcx
	leal	-57344(%rdi), %r8d
	cmpl	$8191, %r8d
	jbe	.L177
	cmpl	$55295, %edi
	jbe	.L177
	leal	-65536(%rdi), %r8d
	cmpl	$1048575, %r8d
	jbe	.L178
	testl	%r9d, %r9d
	js	.L222
	testb	%r13b, %r13b
	jne	.L184
	cmpl	$1048575, %r15d
	jbe	.L185
.L186:
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L161:
	movq	24(%rbp), %rax
	movl	$1, (%rax)
	xorl	%eax, %eax
.L159:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L194:
	.cfi_restore_state
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L168:
	testq	%rdx, %rdx
	je	.L187
	movl	%r8d, (%rdx)
.L187:
	cmpq	$0, 16(%rbp)
	je	.L188
	movq	16(%rbp), %rbx
	movl	%eax, (%rbx)
.L188:
	movq	24(%rbp), %rcx
	movl	%r8d, %edx
	movq	%rdi, -56(%rbp)
	call	u_terminateUChars_67@PLT
	movq	-56(%rbp), %rdi
	movq	%rdi, %rax
	jmp	.L159
.L185:
	addl	$1, %eax
	movl	%r9d, %edi
.L178:
	testq	%r10, %r10
	je	.L183
	leaq	4(%r10), %r8
	cmpq	%r8, %r12
	jb	.L183
	movl	%edi, %r14d
	andw	$1023, %di
	sarl	$10, %r14d
	orw	$-9216, %di
	subw	$10304, %r14w
	movw	%di, 2(%r10)
	movw	%r14w, (%r10)
	movq	%r8, %r10
	.p2align 4,,10
	.p2align 3
.L182:
	cmpq	%rcx, %r11
	ja	.L176
	movq	-56(%rbp), %rdi
	movq	%r10, %rcx
	subq	%rdi, %rcx
	sarq	%rcx
	leal	(%rbx,%rcx), %r8d
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L184:
	addl	$1, %eax
	movl	%r9d, %edi
.L177:
	cmpq	%r10, %r12
	jbe	.L181
	movw	%di, (%r10)
	addq	$2, %r10
	jmp	.L182
.L222:
	movq	24(%rbp), %rax
	movl	$10, (%rax)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	addl	$2, %ebx
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L181:
	addl	$1, %ebx
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L221:
	movl	(%rcx), %eax
	movq	%rdi, %r10
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jne	.L167
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L223:
	movw	%ax, (%r10)
	movl	(%r8), %eax
	leaq	2(%r10), %rcx
	testl	%eax, %eax
	je	.L219
	movq	%rcx, %r10
.L172:
	movq	%r8, %rcx
.L167:
	leal	-57344(%rax), %r11d
	leaq	4(%rcx), %r8
	cmpl	$8191, %r11d
	jbe	.L196
	cmpl	$55295, %eax
	ja	.L192
.L196:
	cmpq	%r10, %r12
	ja	.L223
	movl	(%r8), %eax
	addl	$1, %ebx
	testl	%eax, %eax
	jne	.L172
.L220:
	movq	%r10, %rcx
.L219:
	subq	%rdi, %rcx
	xorl	%eax, %eax
	sarq	%rcx
	leal	(%rbx,%rcx), %r8d
	jmp	.L168
.L192:
	movq	%rcx, %r11
	.p2align 4,,10
	.p2align 3
.L169:
	movl	4(%r11), %eax
	addq	$4, %r11
	testl	%eax, %eax
	jne	.L169
	jmp	.L174
	.cfi_endproc
.LFE2054:
	.size	u_strFromUTF32WithSub_67, .-u_strFromUTF32WithSub_67
	.p2align 4
	.globl	u_strFromUTF32_67
	.type	u_strFromUTF32_67, @function
u_strFromUTF32_67:
.LFB2055:
	.cfi_startproc
	endbr64
	movl	(%r9), %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L277
	testq	%rcx, %rcx
	jne	.L253
	testl	%r8d, %r8d
	jne	.L226
.L253:
	cmpl	$-1, %r8d
	jl	.L226
	testl	%esi, %esi
	js	.L226
	testq	%rdi, %rdi
	jne	.L228
	testl	%esi, %esi
	jg	.L226
.L228:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L229
	movslq	%esi, %rax
	leaq	(%rdi,%rax,2), %r12
.L229:
	cmpl	$-1, %r8d
	je	.L282
	testq	%rcx, %rcx
	je	.L252
	movslq	%r8d, %r8
	movq	%rdi, %r10
	xorl	%r11d, %r11d
	leaq	(%rcx,%r8,4), %rax
.L281:
	cmpq	%rcx, %rax
	jbe	.L283
	movl	(%rcx), %r8d
	leal	-57344(%r8), %ebx
	cmpl	$8191, %ebx
	jbe	.L255
	cmpl	$55295, %r8d
	jbe	.L255
	leal	-65536(%r8), %ebx
	cmpl	$1048575, %ebx
	jbe	.L284
	movl	$10, (%r9)
	xorl	%eax, %eax
.L224:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, (%r9)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	xorl	%r8d, %r8d
.L232:
	testq	%rdx, %rdx
	je	.L246
	movl	%r8d, (%rdx)
.L246:
	movq	%r9, %rcx
	movl	%r8d, %edx
	movq	%rdi, -40(%rbp)
	call	u_terminateUChars_67@PLT
	movq	-40(%rbp), %rdi
	movq	%rdi, %rax
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L282:
	movl	(%rcx), %r8d
	movq	%rdi, %r10
	xorl	%r11d, %r11d
	testl	%r8d, %r8d
	jne	.L231
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L285:
	movw	%r8w, (%r10)
	movl	(%rax), %r8d
	leaq	2(%r10), %rcx
	testl	%r8d, %r8d
	je	.L237
	movq	%rcx, %r10
.L236:
	movq	%rax, %rcx
.L231:
	leal	-57344(%r8), %ebx
	leaq	4(%rcx), %rax
	cmpl	$8191, %ebx
	jbe	.L254
	cmpl	$55295, %r8d
	ja	.L250
.L254:
	cmpq	%r10, %r12
	ja	.L285
	movl	(%rax), %r8d
	addl	$1, %r11d
	testl	%r8d, %r8d
	jne	.L236
	movq	%r10, %rcx
.L237:
	subq	%rdi, %rcx
	sarq	%rcx
	leal	(%r11,%rcx), %r8d
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L233:
	movl	4(%rax), %r8d
	addq	$4, %rax
	testl	%r8d, %r8d
	jne	.L233
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L255:
	cmpq	%r10, %r12
	jbe	.L242
	movw	%r8w, (%r10)
	addq	$2, %r10
.L243:
	addq	$4, %rcx
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L284:
	testq	%r10, %r10
	je	.L245
	leaq	4(%r10), %rbx
	cmpq	%rbx, %r12
	jb	.L245
	movl	%r8d, %r13d
	andw	$1023, %r8w
	sarl	$10, %r13d
	orw	$-9216, %r8w
	subw	$10304, %r13w
	movw	%r8w, 2(%r10)
	movw	%r13w, (%r10)
	movq	%rbx, %r10
	jmp	.L243
.L245:
	addl	$2, %r11d
	jmp	.L243
.L242:
	addl	$1, %r11d
	jmp	.L243
.L283:
	subq	%rdi, %r10
	movq	%r10, %r8
	sarq	%r8
	addl	%r11d, %r8d
	jmp	.L232
	.cfi_endproc
.LFE2055:
	.size	u_strFromUTF32_67, .-u_strFromUTF32_67
	.p2align 4
	.globl	u_strToUTF32WithSub_67
	.type	u_strToUTF32WithSub_67, @function
u_strToUTF32WithSub_67:
.LFB2056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rbx
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L286
	movq	%rdi, %r13
	testq	%rcx, %rcx
	jne	.L325
	testl	%r8d, %r8d
	jne	.L288
.L325:
	cmpl	$-1, %r8d
	jl	.L288
	testl	%esi, %esi
	js	.L288
	testq	%r13, %r13
	sete	%dil
	testl	%esi, %esi
	setg	%al
	testb	%al, %dil
	jne	.L288
	cmpl	$1114111, %r9d
	jg	.L288
	movl	%r9d, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L288
	cmpq	$0, 16(%rbp)
	je	.L291
	movq	16(%rbp), %rax
	movl	$0, (%rax)
.L291:
	xorl	%r14d, %r14d
	testq	%r13, %r13
	je	.L292
	movslq	%esi, %rax
	leaq	0(%r13,%rax,4), %r14
.L292:
	cmpl	$-1, %r8d
	je	.L355
	testq	%rcx, %rcx
	je	.L322
	movslq	%r8d, %r8
	movq	%r13, %rax
	xorl	%edi, %edi
	leaq	(%rcx,%r8,2), %r12
.L300:
	cmpq	%rcx, %r12
	jbe	.L356
	testl	%r9d, %r9d
	js	.L302
	xorl	%r15d, %r15d
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L358:
	addl	$1, %edi
	cmpq	%rcx, %r12
	jbe	.L309
.L310:
	movzwl	(%rcx), %ebx
	leaq	2(%rcx), %r10
	movl	%ebx, %r11d
	andl	$63488, %r11d
	cmpl	$55296, %r11d
	je	.L357
	movq	%r10, %rcx
.L303:
	cmpq	%rax, %r14
	jbe	.L358
	movl	%ebx, (%rax)
	addq	$4, %rax
	cmpq	%rcx, %r12
	ja	.L310
.L309:
	subq	%r13, %rax
	sarq	$2, %rax
	leal	(%rdi,%rax), %r8d
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L322:
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
.L295:
	testq	%rdx, %rdx
	je	.L315
	movl	%r8d, (%rdx)
.L315:
	cmpq	$0, 16(%rbp)
	je	.L316
	movq	16(%rbp), %rax
	movl	%r15d, (%rax)
.L316:
	movq	24(%rbp), %rcx
	movl	%r8d, %edx
	movq	%r13, %rdi
	call	u_terminateUChar32s_67@PLT
	movq	%r13, %rax
.L286:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	movq	24(%rbp), %rax
	movl	$1, (%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movl	%r11d, (%rax)
	addq	$4, %rax
.L314:
	cmpq	%rcx, %r12
	jbe	.L359
.L302:
	movzwl	(%rcx), %r11d
	leaq	2(%rcx), %r10
	movl	%r11d, %r9d
	andl	$63488, %r9d
	cmpl	$55296, %r9d
	je	.L360
	movq	%r10, %rcx
.L311:
	cmpq	%rax, %r14
	ja	.L361
	addl	$1, %edi
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L357:
	testb	$4, %bh
	jne	.L306
	cmpq	%r10, %r12
	ja	.L304
.L306:
	addl	$1, %r15d
	movl	%r9d, %ebx
	movq	%r10, %rcx
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L304:
	movzwl	2(%rcx), %r8d
	movl	%r8d, %r11d
	andl	$-1024, %r11d
	cmpl	$56320, %r11d
	jne	.L306
	sall	$10, %ebx
	addq	$4, %rcx
	leal	-56613888(%r8,%rbx), %ebx
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L355:
	movzwl	(%rcx), %r15d
	movq	%r13, %rax
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movl	%r15d, %r10d
	testl	%r15d, %r15d
	jne	.L294
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L362:
	movl	%r10d, (%rax)
	movzwl	(%r11), %r10d
	leaq	4(%rax), %r8
	testl	%r10d, %r10d
	je	.L299
	movq	%r8, %rax
.L298:
	movq	%r11, %rcx
.L294:
	movl	%r10d, %r8d
	leaq	2(%rcx), %r11
	andl	$-2048, %r8d
	cmpl	$55296, %r8d
	je	.L320
	cmpq	%rax, %r14
	ja	.L362
	movzwl	(%r11), %r10d
	addl	$1, %edi
	testl	%r10d, %r10d
	jne	.L298
	movq	%rax, %r8
.L299:
	subq	%r13, %r8
	xorl	%r15d, %r15d
	sarq	$2, %r8
	addl	%edi, %r8d
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L360:
	testw	$1024, %r11w
	jne	.L312
	cmpq	%r10, %r12
	ja	.L363
.L312:
	movq	24(%rbp), %rax
	movl	$10, (%rax)
	xorl	%eax, %eax
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L320:
	movq	%rcx, %r12
	.p2align 4,,10
	.p2align 3
.L296:
	addq	$2, %r12
	cmpw	$0, (%r12)
	jne	.L296
	jmp	.L300
.L363:
	movzwl	2(%rcx), %r8d
	movl	%r8d, %r9d
	andl	$-1024, %r9d
	cmpl	$56320, %r9d
	jne	.L312
	sall	$10, %r11d
	addq	$4, %rcx
	leal	-56613888(%r8,%r11), %r11d
	jmp	.L311
.L359:
	xorl	%r15d, %r15d
	jmp	.L309
.L356:
	subq	%r13, %rax
	xorl	%r15d, %r15d
	movq	%rax, %r8
	sarq	$2, %r8
	addl	%edi, %r8d
	jmp	.L295
	.cfi_endproc
.LFE2056:
	.size	u_strToUTF32WithSub_67, .-u_strToUTF32WithSub_67
	.p2align 4
	.globl	u_strToUTF32_67
	.type	u_strToUTF32_67, @function
u_strToUTF32_67:
.LFB2057:
	.cfi_startproc
	endbr64
	movl	(%r9), %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L416
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rcx, %rcx
	jne	.L391
	testl	%r8d, %r8d
	jne	.L366
.L391:
	cmpl	$-1, %r8d
	jl	.L366
	testl	%esi, %esi
	js	.L366
	testq	%rbx, %rbx
	jne	.L368
	testl	%esi, %esi
	jg	.L366
.L368:
	xorl	%r10d, %r10d
	testq	%rbx, %rbx
	je	.L369
	movslq	%esi, %rax
	leaq	(%rbx,%rax,4), %r10
.L369:
	cmpl	$-1, %r8d
	je	.L421
	testq	%rcx, %rcx
	je	.L389
	movslq	%r8d, %r8
	movq	%rbx, %rax
	xorl	%r11d, %r11d
	leaq	(%rcx,%r8,2), %r8
.L382:
	cmpq	%rcx, %r8
	jbe	.L422
.L378:
	movzwl	(%rcx), %r14d
	leaq	2(%rcx), %r13
	movl	%r14d, %r12d
	andl	$63488, %r12d
	cmpl	$55296, %r12d
	je	.L423
	movq	%r13, %rcx
.L379:
	cmpq	%rax, %r10
	jbe	.L381
	movl	%r14d, (%rax)
	addq	$4, %rax
	cmpq	%rcx, %r8
	ja	.L378
.L422:
	subq	%rbx, %rax
	sarq	$2, %rax
	leal	(%r11,%rax), %r8d
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L366:
	movl	$1, (%r9)
	xorl	%eax, %eax
.L364:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	xorl	%r8d, %r8d
.L372:
	testq	%rdx, %rdx
	je	.L383
	movl	%r8d, (%rdx)
.L383:
	movq	%r9, %rcx
	movl	%r8d, %edx
	movq	%rbx, %rdi
	call	u_terminateUChar32s_67@PLT
	movq	%rbx, %rax
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L381:
	addl	$1, %r11d
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L423:
	testw	$1024, %r14w
	jne	.L380
	cmpq	%r13, %r8
	ja	.L424
.L380:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	movl	$10, (%r9)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	.cfi_restore_state
	movzwl	(%rcx), %edi
	movq	%rbx, %rax
	xorl	%r11d, %r11d
	movl	%edi, %r8d
	testl	%edi, %edi
	jne	.L371
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L425:
	movl	%edi, (%rax)
	movzwl	(%r8), %edi
	leaq	4(%rax), %rcx
	testl	%edi, %edi
	je	.L376
	movq	%rcx, %rax
.L375:
	movq	%r8, %rcx
.L371:
	movl	%edi, %r12d
	leaq	2(%rcx), %r8
	andl	$-2048, %r12d
	cmpl	$55296, %r12d
	je	.L387
	cmpq	%rax, %r10
	ja	.L425
	movzwl	(%r8), %edi
	addl	$1, %r11d
	testl	%edi, %edi
	jne	.L375
	movq	%rax, %rcx
.L376:
	subq	%rbx, %rcx
	sarq	$2, %rcx
	leal	(%r11,%rcx), %r8d
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L424:
	movzwl	2(%rcx), %edi
	movl	%edi, %r12d
	andl	$-1024, %r12d
	cmpl	$56320, %r12d
	jne	.L380
	sall	$10, %r14d
	addq	$4, %rcx
	leal	-56613888(%rdi,%r14), %r14d
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%rcx, %r8
	.p2align 4,,10
	.p2align 3
.L373:
	addq	$2, %r8
	cmpw	$0, (%r8)
	jne	.L373
	jmp	.L382
	.cfi_endproc
.LFE2057:
	.size	u_strToUTF32_67, .-u_strToUTF32_67
	.p2align 4
	.globl	u_strFromUTF8WithSub_67
	.type	u_strFromUTF8WithSub_67, @function
u_strFromUTF8WithSub_67:
.LFB2058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	24(%rbp), %rax
	movq	16(%rbp), %r10
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L426
	testq	%rcx, %rcx
	jne	.L432
	testl	%r8d, %r8d
	jne	.L428
.L432:
	cmpl	$-1, %r8d
	jl	.L428
	testl	%esi, %esi
	js	.L428
	testq	%rdi, %rdi
	sete	%bl
	testl	%esi, %esi
	setg	%r11b
	testb	%r11b, %bl
	jne	.L428
	cmpl	$1114111, %r9d
	jg	.L428
	movl	%r9d, %r11d
	andl	$-2048, %r11d
	cmpl	$55296, %r11d
	je	.L428
	movq	%rax, 24(%rbp)
	popq	%rbx
	movq	%r10, 16(%rbp)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	u_strFromUTF8WithSub_67.part.0
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movl	$1, (%rax)
.L426:
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2058:
	.size	u_strFromUTF8WithSub_67, .-u_strFromUTF8WithSub_67
	.p2align 4
	.globl	u_strFromUTF8_67
	.type	u_strFromUTF8_67, @function
u_strFromUTF8_67:
.LFB2059:
	.cfi_startproc
	endbr64
	movl	(%r9), %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L454
	testq	%rcx, %rcx
	jne	.L443
	testl	%r8d, %r8d
	jne	.L439
.L443:
	cmpl	$-1, %r8d
	jl	.L439
	testl	%esi, %esi
	js	.L439
	testq	%rdi, %rdi
	jne	.L441
	testl	%esi, %esi
	jg	.L439
.L441:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r9
	movl	$-1, %r9d
	pushq	$0
	call	u_strFromUTF8WithSub_67.part.0
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore 6
	movl	$1, (%r9)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	ret
	.cfi_endproc
.LFE2059:
	.size	u_strFromUTF8_67, .-u_strFromUTF8_67
	.p2align 4
	.globl	u_strFromUTF8Lenient_67
	.type	u_strFromUTF8Lenient_67, @function
u_strFromUTF8Lenient_67:
.LFB2060:
	.cfi_startproc
	endbr64
	movl	(%r9), %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L562
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rcx, %rcx
	jne	.L506
	testl	%r8d, %r8d
	jne	.L458
.L506:
	cmpl	$-1, %r8d
	jl	.L458
	testl	%esi, %esi
	js	.L458
	testq	%rbx, %rbx
	jne	.L460
	testl	%esi, %esi
	jg	.L458
.L460:
	cmpl	$-1, %r8d
	je	.L570
	testq	%rcx, %rcx
	je	.L483
	movslq	%r8d, %r10
	addq	%rcx, %r10
	cmpl	%esi, %r8d
	jg	.L501
	leaq	-3(%r10), %r11
	movq	%rbx, %rdi
	cmpl	$3, %r8d
	jg	.L493
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L571:
	movw	%r8w, (%rdi)
	addq	$1, %rcx
	addq	$2, %rdi
.L489:
	cmpq	%r11, %rcx
	jnb	.L492
.L493:
	movzbl	(%rcx), %r8d
	movl	%r8d, %eax
	cmpl	$191, %r8d
	jle	.L571
	movzbl	1(%rcx), %r12d
	cmpl	$223, %r8d
	jg	.L490
	sall	$6, %eax
	addq	$2, %rcx
	addq	$2, %rdi
	leal	-12416(%rax,%r12), %eax
	movw	%ax, -2(%rdi)
	cmpq	%r11, %rcx
	jb	.L493
	.p2align 4,,10
	.p2align 3
.L492:
	cmpq	%rcx, %r10
	jbe	.L567
.L487:
	movzbl	(%rcx), %r11d
	leaq	1(%rcx), %r12
	movl	%r11d, %eax
	cmpl	$191, %r11d
	jle	.L572
	cmpl	$223, %r11d
	jg	.L496
	leaq	2(%rdi), %r8
	cmpq	%r12, %r10
	ja	.L573
.L497:
	movl	$-3, %eax
	movw	%ax, (%rdi)
.L566:
	subq	%rbx, %r8
	sarq	%r8
.L473:
	testq	%rdx, %rdx
	je	.L500
	movl	%r8d, (%rdx)
.L500:
	movq	%rbx, %rdi
	movq	%r9, %rcx
	movl	%r8d, %edx
	call	u_terminateUChars_67@PLT
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movl	$1, (%r9)
	xorl	%eax, %eax
.L456:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movzbl	2(%rcx), %r13d
	cmpl	$239, %r8d
	jg	.L491
	sall	$12, %r8d
	sall	$6, %r12d
	addq	$3, %rcx
	addq	$2, %rdi
	addl	%r12d, %r8d
	leal	-8320(%r13,%r8), %r8d
	movw	%r8w, -2(%rdi)
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L496:
	movq	%r10, %rax
	subq	%r12, %rax
	cmpl	$239, %r11d
	jg	.L498
	leaq	2(%rdi), %r8
	cmpq	$1, %rax
	jle	.L497
	movzbl	1(%rcx), %eax
	sall	$12, %r11d
	addq	$6, %rcx
	sall	$6, %eax
	addl	%eax, %r11d
	movzbl	-4(%rcx), %eax
	leal	-8320(%rax,%r11), %r11d
	movw	%r11w, (%rdi)
	movq	%r8, %rdi
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L572:
	movw	%r11w, (%rdi)
	movq	%r12, %rcx
	addq	$2, %rdi
.L495:
	cmpq	%r10, %rcx
	jb	.L487
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L570:
	movzbl	(%rcx), %eax
	movl	%eax, %edi
	testq	%rbx, %rbx
	je	.L504
	movslq	%esi, %r8
	leaq	(%rbx,%r8,2), %r11
	cmpq	%r11, %rbx
	jnb	.L504
	testl	%eax, %eax
	je	.L504
	movq	%rbx, %r10
.L463:
	leaq	2(%r10), %r8
	cmpl	$191, %eax
	jle	.L574
	movzbl	1(%rcx), %r12d
	cmpl	$223, %eax
	jg	.L468
	testb	%r12b, %r12b
	jne	.L575
.L469:
	movl	$-3, %edi
	movw	%di, (%r10)
	.p2align 4,,10
	.p2align 3
.L472:
	addq	$1, %rcx
	cmpb	$0, (%rcx)
	jne	.L472
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L491:
	sall	$18, %r8d
	sall	$12, %r12d
	movzbl	3(%rcx), %eax
	addq	$4, %rdi
	addl	%r12d, %r8d
	sall	$6, %r13d
	addq	$4, %rcx
	addl	%r13d, %r8d
	leal	-63447168(%r8,%rax), %eax
	movl	%eax, %r8d
	andw	$1023, %ax
	sarl	$10, %r8d
	orw	$-9216, %ax
	subw	$10304, %r8w
	movw	%ax, -2(%rdi)
	movw	%r8w, -4(%rdi)
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L468:
	cmpl	$239, %eax
	jg	.L470
	testb	%r12b, %r12b
	je	.L469
	movzbl	2(%rcx), %eax
	testb	%al, %al
	je	.L469
	sall	$12, %edi
	sall	$6, %r12d
	addq	$3, %rcx
	leal	-8320(%rdi,%rax), %edi
	leal	(%rdi,%r12), %eax
	movw	%ax, (%r10)
	movq	%r8, %r10
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L574:
	movw	%di, (%r10)
	addq	$1, %rcx
	movq	%r8, %r10
.L467:
	movzbl	(%rcx), %eax
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L508
	cmpq	%r11, %r10
	jb	.L463
.L508:
	subq	%rbx, %r10
	xorl	%edi, %edi
	movq	%r10, %r8
	sarq	%r8
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L483:
	cmpl	%esi, %r8d
	jg	.L501
	movq	%rbx, %rdi
.L567:
	subq	%rbx, %rdi
	movq	%rdi, %r8
	sarq	%r8
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L498:
	cmpq	$2, %rax
	jg	.L499
	leaq	2(%rdi), %r8
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L573:
	movzbl	1(%rcx), %r11d
	sall	$6, %eax
	addq	$2, %rcx
	leal	-12416(%r11,%rax), %eax
	movw	%ax, (%rdi)
	movq	%r8, %rdi
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L470:
	testb	%r12b, %r12b
	je	.L469
	movzbl	2(%rcx), %r13d
	testb	%r13b, %r13b
	je	.L469
	movzbl	3(%rcx), %r14d
	testb	%r14b, %r14b
	je	.L469
	sall	$12, %r12d
	sall	$18, %eax
	leaq	4(%rcx), %rdi
	addl	%r12d, %eax
	sall	$6, %r13d
	addl	%eax, %r13d
	leal	-63447168(%r13,%r14), %r12d
	movl	%r12d, %eax
	sarl	$10, %eax
	subw	$10304, %ax
	movw	%ax, (%r10)
	cmpq	%r8, %r11
	ja	.L471
	subq	%rbx, %r8
	movzbl	4(%rcx), %eax
	movq	%rdi, %rcx
	movl	$1, %edi
	sarq	%r8
	.p2align 4,,10
	.p2align 3
.L569:
	testl	%eax, %eax
	je	.L478
	movzbl	1(%rcx), %r10d
	cmpl	$191, %eax
	jle	.L576
	cmpl	$223, %eax
	jg	.L477
	addl	$1, %edi
	testb	%r10b, %r10b
	jne	.L577
.L478:
	addl	%edi, %r8d
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L501:
	testq	%rdx, %rdx
	je	.L485
	movl	%r8d, (%rdx)
.L485:
	movl	$15, (%r9)
	xorl	%eax, %eax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L477:
	cmpl	$239, %eax
	jg	.L479
	addl	$1, %edi
	testb	%r10b, %r10b
	je	.L478
	cmpb	$0, 2(%rcx)
	je	.L478
	movzbl	3(%rcx), %r10d
	addq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L476:
	movzbl	%r10b, %eax
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L576:
	addl	$1, %edi
	addq	$1, %rcx
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L479:
	testb	%r10b, %r10b
	je	.L565
	cmpb	$0, 2(%rcx)
	je	.L565
	cmpb	$0, 3(%rcx)
	jne	.L482
.L565:
	addl	$1, %edi
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L504:
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L577:
	movzbl	2(%rcx), %r10d
	addq	$2, %rcx
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L575:
	sall	$6, %edi
	movzbl	%r12b, %eax
	addq	$2, %rcx
	leal	-12416(%rdi,%rax), %eax
	movw	%ax, (%r10)
	movq	%r8, %r10
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L499:
	movzbl	1(%rcx), %eax
	sall	$18, %r11d
	addq	$8, %rcx
	addq	$4, %rdi
	sall	$12, %eax
	addl	%r11d, %eax
	movzbl	-6(%rcx), %r11d
	sall	$6, %r11d
	addl	%eax, %r11d
	movzbl	-5(%rcx), %eax
	leal	-63447168(%r11,%rax), %eax
	movl	%eax, %r8d
	andw	$1023, %ax
	sarl	$10, %r8d
	orw	$-9216, %ax
	subw	$10304, %r8w
	movw	%ax, -2(%rdi)
	movw	%r8w, -4(%rdi)
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L482:
	movzbl	4(%rcx), %r10d
	addl	$2, %edi
	addq	$4, %rcx
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L471:
	andw	$1023, %r12w
	movq	%rdi, %rcx
	addq	$4, %r10
	orw	$-9216, %r12w
	movw	%r12w, -2(%r10)
	jmp	.L467
	.cfi_endproc
.LFE2060:
	.size	u_strFromUTF8Lenient_67, .-u_strFromUTF8Lenient_67
	.p2align 4
	.globl	u_strToUTF8WithSub_67
	.type	u_strToUTF8WithSub_67, @function
u_strToUTF8WithSub_67:
.LFB2062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$0, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	addq	%rbx, %rdi
	subq	$40, %rsp
	movq	%rdx, -72(%rbp)
	movq	24(%rbp), %rdx
	testq	%rbx, %rbx
	cmove	%rax, %rdi
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L578
	testq	%rcx, %rcx
	jne	.L738
	testl	%r8d, %r8d
	jne	.L581
.L738:
	cmpl	$-1, %r8d
	jl	.L581
	testl	%esi, %esi
	js	.L581
	testq	%rbx, %rbx
	sete	%dl
	testl	%esi, %esi
	setg	%al
	testb	%al, %dl
	jne	.L581
	cmpl	$1114111, %r9d
	jg	.L581
	movl	%r9d, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L581
	cmpq	$0, 16(%rbp)
	je	.L584
	movq	16(%rbp), %rax
	movl	$0, (%rax)
.L584:
	cmpl	$-1, %r8d
	je	.L815
	movabsq	$6148914691236517206, %r10
	movq	%rdi, %r11
	subq	%rbx, %r11
	movq	%r11, %rax
	sarq	$63, %r11
	imulq	%r10
	subq	%r11, %rdx
	testq	%rcx, %rcx
	je	.L729
	movslq	%r8d, %rax
	addq	%rax, %rax
	leaq	(%rcx,%rax), %r13
	sarq	%rax
	cmpl	%eax, %edx
	cmovge	%eax, %edx
	cmpl	$2, %edx
	jle	.L816
	movl	%r9d, %eax
	movl	%r9d, %r8d
	movq	%r10, -64(%rbp)
	xorl	%r12d, %r12d
	sarl	$18, %eax
	sarl	$12, %r8d
	orl	$-16, %eax
	movl	%r8d, %r15d
	movb	%al, -53(%rbp)
	movl	%r8d, %eax
	orl	$-32, %r15d
	movq	%rbx, %r8
	andl	$63, %eax
	movb	%r15b, -52(%rbp)
	orl	$-128, %eax
	movb	%al, -54(%rbp)
	movl	%r9d, %eax
	sarl	$6, %eax
	movl	%eax, %r11d
	orl	$-64, %eax
	movb	%al, -50(%rbp)
	movl	%r9d, %eax
	andl	$63, %r11d
	andl	$63, %eax
	orl	$-128, %r11d
	orl	$-128, %eax
	movb	%r11b, -51(%rbp)
	movb	%al, -49(%rbp)
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L817:
	movb	%r10b, (%r8)
	movl	%r11d, %edx
	addq	$1, %r8
	movq	%r14, %rcx
.L645:
	testl	%edx, %edx
	je	.L648
.L655:
	movzwl	(%rcx), %r10d
	leaq	2(%rcx), %r14
	leal	-1(%rdx), %r11d
	movl	%r10d, %eax
	cmpw	$127, %r10w
	jbe	.L817
	cmpl	$2047, %r10d
	ja	.L646
	shrl	$6, %r10d
	andl	$63, %eax
	movl	%r11d, %edx
	addq	$2, %r8
	orl	$-64, %r10d
	orl	$-128, %eax
	movq	%r14, %rcx
	movb	%r10b, -2(%r8)
	movb	%al, -1(%r8)
	testl	%edx, %edx
	jne	.L655
.L648:
	movq	-64(%rbp), %rax
	movq	%rdi, %r10
	subq	%r8, %r10
	imulq	%r10
	movq	%r13, %rax
	sarq	$63, %r10
	subq	%rcx, %rax
	sarq	%rax
	subq	%r10, %rdx
	cmpl	%eax, %edx
	cmovge	%eax, %edx
	cmpl	$2, %edx
	jg	.L655
	.p2align 4,,10
	.p2align 3
.L658:
	cmpq	%rcx, %r13
	jbe	.L818
.L643:
	movzwl	(%rcx), %edx
	leaq	2(%rcx), %r11
	movl	%edx, %eax
	cmpw	$127, %dx
	ja	.L656
	cmpq	%r8, %rdi
	jbe	.L731
	movq	%r11, %rcx
	movb	%dl, (%r8)
	addq	$1, %r8
	cmpq	%rcx, %r13
	ja	.L643
.L818:
	movl	%r8d, %edx
.L813:
	subl	%ebx, %edx
	jmp	.L632
.L729:
	xorl	%edx, %edx
	xorl	%r12d, %r12d
.L632:
	cmpq	$0, 16(%rbp)
	je	.L710
	movq	16(%rbp), %rax
	movl	%r12d, (%rax)
.L710:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L711
	movl	%edx, (%rax)
.L711:
	movq	24(%rbp), %rcx
	movq	%rbx, %rdi
	call	u_terminateChars_67@PLT
	movq	%rbx, %rax
.L578:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	movq	24(%rbp), %rax
	movl	$1, (%rax)
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	leal	-55296(%r10), %r15d
	cmpl	$2047, %r15d
	jbe	.L647
	movl	%r10d, %edx
	shrl	$6, %r10d
	andl	$63, %eax
	addq	$3, %r8
	shrl	$12, %edx
	andl	$63, %r10d
	orl	$-128, %eax
	movq	%r14, %rcx
	orl	$-32, %edx
	orl	$-128, %r10d
	movb	%al, -1(%r8)
	movb	%dl, -3(%r8)
	movl	%r11d, %edx
	movb	%r10b, -2(%r8)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L647:
	testl	%r11d, %r11d
	je	.L648
	testb	$4, %ah
	jne	.L649
	movzwl	2(%rcx), %eax
	movl	%eax, %r11d
	andl	$-1024, %r11d
	cmpl	$56320, %r11d
	je	.L650
.L649:
	testl	%r9d, %r9d
	js	.L603
	addl	$1, %r12d
	subl	$2, %edx
	cmpl	$127, %r9d
	jg	.L652
	movb	%r9b, (%r8)
	movq	%r14, %rcx
	addq	$1, %r8
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L652:
	cmpl	$2047, %r9d
	jg	.L653
	movzbl	-50(%rbp), %eax
	addq	$2, %r8
	movq	%r14, %rcx
	movb	%al, -2(%r8)
	movzbl	-49(%rbp), %eax
	movb	%al, -1(%r8)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L656:
	cmpl	$2047, %edx
	ja	.L659
	movq	%rdi, %rcx
	subq	%r8, %rcx
	cmpq	$1, %rcx
	jle	.L732
	shrl	$6, %edx
	andl	$63, %eax
	addq	$2, %r8
	movq	%r11, %rcx
	orl	$-64, %edx
	orl	$-128, %eax
	movb	%dl, -2(%r8)
	movb	%al, -1(%r8)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L653:
	cmpl	$65535, %r9d
	jg	.L654
	movzbl	-52(%rbp), %eax
	addq	$3, %r8
	movq	%r14, %rcx
	movb	%al, -3(%r8)
	movzbl	-51(%rbp), %eax
	movb	%al, -2(%r8)
	movzbl	-49(%rbp), %eax
	movb	%al, -1(%r8)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L650:
	sall	$10, %r10d
	addq	$4, %rcx
	subl	$2, %edx
	addq	$4, %r8
	leal	-56613888(%rax,%r10), %eax
	movl	%eax, %r10d
	shrl	$18, %r10d
	orl	$-16, %r10d
	movb	%r10b, -4(%r8)
	movl	%eax, %r10d
	shrl	$12, %r10d
	andl	$63, %r10d
	orl	$-128, %r10d
	movb	%r10b, -3(%r8)
	movl	%eax, %r10d
	andl	$63, %eax
	shrl	$6, %r10d
	orl	$-128, %eax
	andl	$63, %r10d
	movb	%al, -1(%r8)
	orl	$-128, %r10d
	movb	%r10b, -2(%r8)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L659:
	leal	-55296(%rdx), %r10d
	cmpl	$2047, %r10d
	jbe	.L660
	movq	%rdi, %rcx
	subq	%r8, %rcx
	cmpq	$2, %rcx
	jle	.L733
	movl	%edx, %ecx
	shrl	$6, %edx
	andl	$63, %eax
	addq	$3, %r8
	shrl	$12, %ecx
	andl	$63, %edx
	orl	$-128, %eax
	orl	$-32, %ecx
	orl	$-128, %edx
	movb	%al, -1(%r8)
	movb	%cl, -3(%r8)
	movq	%r11, %rcx
	movb	%dl, -2(%r8)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L654:
	movzbl	-53(%rbp), %eax
	addq	$4, %r8
	movq	%r14, %rcx
	movb	%al, -4(%r8)
	movzbl	-54(%rbp), %eax
	movb	%al, -3(%r8)
	movzbl	-51(%rbp), %eax
	movb	%al, -2(%r8)
	movzbl	-49(%rbp), %eax
	movb	%al, -1(%r8)
	jmp	.L645
.L660:
	testb	$4, %dh
	jne	.L661
	cmpq	%r11, %r13
	ja	.L819
.L661:
	testl	%r9d, %r9d
	js	.L603
	addl	$1, %r12d
	movl	%r9d, %r10d
	movl	%r9d, %eax
	movq	%r11, %rcx
.L664:
	movq	%rdi, %r11
	subq	%r8, %r11
	cmpl	$127, %eax
	jbe	.L734
	cmpl	$2047, %eax
	jbe	.L735
	cmpl	$55295, %eax
	jbe	.L736
	leal	-57344(%rax), %edx
	cmpl	$1056767, %edx
	ja	.L737
	cmpl	$65535, %eax
	ja	.L820
	cmpq	$2, %r11
	jle	.L821
	movl	%r10d, %edx
	andl	$63, %eax
	movl	%r10d, %r11d
	sarl	$6, %edx
	orl	$-128, %eax
	sarl	$12, %r11d
	andl	$63, %edx
	orl	$-128, %edx
.L715:
	orl	$-32, %r11d
	movb	%dl, 1(%r8)
	addq	$3, %r8
	movb	%r11b, -3(%r8)
	movb	%al, -1(%r8)
	jmp	.L658
.L603:
	movq	24(%rbp), %rax
	movl	$10, (%rax)
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L734:
	.cfi_restore_state
	movl	$1, %r14d
	movl	$1, %edx
.L665:
	cmpq	%r11, %r14
	jg	.L822
	cmpl	$127, %r10d
	jg	.L700
	movb	%al, (%r8)
	addq	$1, %r8
	jmp	.L658
.L815:
	movzwl	(%rcx), %eax
	movl	%eax, %r10d
	testl	%eax, %eax
	je	.L729
	movq	%rbx, %rdx
	xorl	%r12d, %r12d
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L823:
	cmpq	%rdx, %rdi
	jbe	.L721
	movb	%r10b, (%rdx)
	movq	%r8, %rcx
	addq	$1, %rdx
.L621:
	movzwl	(%rcx), %eax
	movl	%eax, %r10d
	testl	%eax, %eax
	je	.L813
.L586:
	leaq	2(%rcx), %r8
	cmpl	$127, %eax
	jbe	.L823
	cmpl	$2047, %eax
	ja	.L622
	movq	%rdi, %rcx
	subq	%rdx, %rcx
	cmpq	$1, %rcx
	jle	.L722
	shrl	$6, %eax
	addq	$2, %rdx
	movl	%eax, %ecx
	movl	%r10d, %eax
	andl	$63, %eax
	orl	$-64, %ecx
	orl	$-128, %eax
	movb	%cl, -2(%rdx)
	movq	%r8, %rcx
	movb	%al, -1(%rdx)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L622:
	leal	-55296(%rax), %r11d
	cmpl	$2047, %r11d
	jbe	.L623
	movq	%rdi, %rcx
	subq	%rdx, %rcx
	cmpq	$2, %rcx
	jle	.L811
	movl	%eax, %ecx
	shrl	$6, %eax
	addq	$3, %rdx
	shrl	$12, %ecx
	orl	$-32, %ecx
	movb	%cl, -3(%rdx)
	movl	%eax, %ecx
	movl	%r10d, %eax
	andl	$63, %ecx
	andl	$63, %eax
	orl	$-128, %ecx
	orl	$-128, %eax
	movb	%cl, -2(%rdx)
	movq	%r8, %rcx
	movb	%al, -1(%rdx)
	jmp	.L621
.L623:
	testw	$1024, %r10w
	jne	.L624
	movzwl	2(%rcx), %r10d
	movl	%r10d, %r11d
	andl	$-1024, %r11d
	cmpl	$56320, %r11d
	je	.L824
.L624:
	testl	%r9d, %r9d
	js	.L603
	addl	$1, %r12d
	movl	%r9d, %r11d
	movl	%r9d, %eax
	movq	%r8, %rcx
.L625:
	movq	%rdi, %r8
	subq	%rdx, %r8
	cmpl	$127, %eax
	jbe	.L724
	cmpl	$2047, %eax
	jbe	.L725
	cmpl	$55295, %eax
	jbe	.L726
	leal	-57344(%rax), %r10d
	cmpl	$1056767, %r10d
	ja	.L727
	cmpl	$65535, %eax
	ja	.L825
	cmpq	$2, %r8
	jle	.L826
	movl	%r11d, %r13d
	sarl	$6, %r11d
	andl	$63, %eax
	movl	%r11d, %r8d
	orl	$-128, %eax
	sarl	$12, %r13d
	andl	$63, %r8d
	orl	$-128, %r8d
.L713:
	orl	$-32, %r13d
	movb	%r8b, 1(%rdx)
	addq	$3, %rdx
	movb	%r13b, -3(%rdx)
	movb	%al, -1(%rdx)
	jmp	.L621
.L724:
	movl	$1, %r13d
	movl	$1, %r10d
.L626:
	cmpq	%r8, %r13
	jg	.L728
	cmpl	$127, %r11d
	jg	.L629
	movb	%al, (%rdx)
	addq	$1, %rdx
	jmp	.L621
.L629:
	movl	%r11d, %r10d
	andl	$63, %eax
	sarl	$6, %r10d
	orl	$-128, %eax
	movl	%r10d, %r8d
	cmpl	$2047, %r11d
	jg	.L630
	orl	$-64, %r10d
	movb	%al, 1(%rdx)
	addq	$2, %rdx
	movb	%r10b, -2(%rdx)
	jmp	.L621
.L700:
	movl	%r10d, %r11d
	andl	$63, %eax
	sarl	$6, %r11d
	orl	$-128, %eax
	movl	%r11d, %edx
	cmpl	$2047, %r10d
	jg	.L701
	orl	$-64, %r11d
	movb	%al, 1(%r8)
	addq	$2, %r8
	movb	%r11b, -2(%r8)
	jmp	.L658
.L725:
	movl	$2, %r13d
	movl	$2, %r10d
	jmp	.L626
.L825:
	cmpq	$3, %r8
	jle	.L827
	movl	%r11d, %r8d
	andl	$63, %eax
	sarl	$6, %r8d
	orl	$-128, %eax
.L630:
	movl	%r11d, %r10d
	andl	$63, %r8d
	sarl	$12, %r10d
	orl	$-128, %r8d
	movl	%r10d, %r13d
	cmpl	$65535, %r11d
	jle	.L713
	sarl	$18, %r11d
	andl	$63, %r10d
	movb	%r8b, 2(%rdx)
	addq	$4, %rdx
	orl	$-16, %r11d
	orl	$-128, %r10d
	movb	%al, -1(%rdx)
	movb	%r11b, -4(%rdx)
	movb	%r10b, -3(%rdx)
	jmp	.L621
.L819:
	movzwl	2(%rcx), %eax
	movl	%eax, %r10d
	andl	$-1024, %r10d
	cmpl	$56320, %r10d
	jne	.L661
	sall	$10, %edx
	addq	$4, %rcx
	leal	-56613888(%rax,%rdx), %r10d
	movl	%r10d, %eax
	jmp	.L664
.L735:
	movl	$2, %r14d
	movl	$2, %edx
	jmp	.L665
.L824:
	sall	$10, %eax
	addq	$4, %rcx
	leal	-56613888(%r10,%rax), %r11d
	movl	%r11d, %eax
	jmp	.L625
.L820:
	cmpq	$3, %r11
	jle	.L828
	movl	%r10d, %edx
	andl	$63, %eax
	sarl	$6, %edx
	orl	$-128, %eax
.L701:
	movl	%r10d, %r14d
	andl	$63, %edx
	sarl	$12, %r14d
	orl	$-128, %edx
	movl	%r14d, %r11d
	cmpl	$65535, %r10d
	jle	.L715
	sarl	$18, %r10d
	movb	%dl, 2(%r8)
	addq	$4, %r8
	orl	$-16, %r10d
	movb	%al, -1(%r8)
	movb	%r10b, -4(%r8)
	movl	%r14d, %r10d
	andl	$63, %r10d
	orl	$-128, %r10d
	movb	%r10b, -3(%r8)
	jmp	.L658
.L726:
	movl	$3, %r13d
	movl	$3, %r10d
	jmp	.L626
.L736:
	movl	$3, %r14d
	movl	$3, %edx
	jmp	.L665
.L721:
	movl	$1, %r10d
.L620:
	movzwl	(%r8), %eax
	leaq	2(%r8), %rdi
	movl	%eax, %ecx
	testl	%eax, %eax
	je	.L588
	leal	-57344(%r9), %r11d
	cmpl	$1056767, %r11d
	jbe	.L589
	cmpl	$127, %r9d
	jbe	.L596
	testl	%r9d, %r9d
	js	.L604
	cmpl	$2047, %r9d
	ja	.L605
	jmp	.L611
.L829:
	addl	$1, %r10d
	movq	%rdi, %r8
.L607:
	movzwl	(%r8), %eax
	leaq	2(%r8), %rdi
	movl	%eax, %ecx
	testl	%eax, %eax
	je	.L588
.L611:
	cmpl	$127, %eax
	jbe	.L829
	cmpl	$2047, %eax
	ja	.L608
	addl	$2, %r10d
	movq	%rdi, %r8
	jmp	.L607
.L830:
	addl	$1, %r10d
	movq	%rdi, %r8
.L592:
	movzwl	(%r8), %eax
	leaq	2(%r8), %rdi
	movl	%eax, %ecx
	testl	%eax, %eax
	je	.L588
.L596:
	cmpl	$127, %eax
	jbe	.L830
	cmpl	$2047, %eax
	ja	.L593
	addl	$2, %r10d
	movq	%rdi, %r8
	jmp	.L592
.L832:
	cmpl	$2047, %eax
	jbe	.L613
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L831
	addl	$3, %r10d
	movq	%rdi, %r8
.L617:
	movzwl	(%r8), %eax
	leaq	2(%r8), %rdi
	movl	%eax, %ecx
	testl	%eax, %eax
	je	.L588
.L605:
	cmpl	$127, %eax
	ja	.L832
	addl	$1, %r10d
	movq	%rdi, %r8
	jmp	.L617
.L833:
	addl	$1, %r10d
	movq	%rdi, %r8
.L634:
	movzwl	(%r8), %eax
	leaq	2(%r8), %rdi
	movl	%eax, %ecx
	testl	%eax, %eax
	je	.L588
.L589:
	cmpl	$127, %eax
	jbe	.L833
	cmpl	$2047, %eax
	ja	.L635
	addl	$2, %r10d
	movq	%rdi, %r8
	jmp	.L634
.L834:
	addl	$1, %r10d
	movq	%rdi, %r8
.L600:
	movzwl	(%r8), %eax
	leaq	2(%r8), %rdi
	movl	%eax, %ecx
	testl	%eax, %eax
	je	.L588
.L604:
	cmpl	$127, %eax
	jbe	.L834
	cmpl	$2047, %eax
	ja	.L601
	addl	$2, %r10d
	movq	%rdi, %r8
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L593:
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L594
	addl	$3, %r10d
	movq	%rdi, %r8
	jmp	.L592
.L588:
	subq	%rbx, %rdx
	addl	%r10d, %edx
	jmp	.L632
.L613:
	addl	$2, %r10d
	movq	%rdi, %r8
	jmp	.L617
.L608:
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L609
	addl	$3, %r10d
	movq	%rdi, %r8
	jmp	.L607
.L594:
	andb	$4, %ch
	jne	.L595
	movzwl	2(%r8), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L835
.L595:
	addl	$1, %r10d
	addl	$1, %r12d
	movq	%rdi, %r8
	jmp	.L592
.L635:
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L636
	addl	$3, %r10d
	movq	%rdi, %r8
	jmp	.L634
.L727:
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	jmp	.L626
.L731:
	movl	$1, %edx
.L657:
	cmpq	%r11, %r13
	jbe	.L677
	leal	-57344(%r9), %eax
	cmpl	$1056767, %eax
	jbe	.L670
	cmpl	$127, %r9d
	jbe	.L678
	testl	%r9d, %r9d
	js	.L684
	cmpl	$2047, %r9d
	ja	.L685
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L836:
	addl	$1, %edx
	movq	%rcx, %r11
.L687:
	cmpq	%r11, %r13
	jbe	.L677
.L691:
	movzwl	(%r11), %edi
	leaq	2(%r11), %rcx
	movl	%edi, %eax
	cmpw	$127, %di
	jbe	.L836
	cmpl	$2047, %edi
	ja	.L688
	addl	$2, %edx
	movq	%rcx, %r11
	jmp	.L687
.L837:
	addl	$1, %edx
	movq	%rdi, %r11
.L673:
	cmpq	%r11, %r13
	jbe	.L677
.L678:
	movzwl	(%r11), %ecx
	leaq	2(%r11), %rdi
	movl	%ecx, %eax
	cmpw	$127, %cx
	jbe	.L837
	cmpl	$2047, %ecx
	ja	.L674
	addl	$2, %edx
	movq	%rdi, %r11
	jmp	.L673
.L839:
	cmpl	$2047, %edi
	jbe	.L693
	andl	$-2048, %edi
	cmpl	$55296, %edi
	je	.L838
	addl	$3, %edx
	movq	%rcx, %r11
.L698:
	cmpq	%r11, %r13
	jbe	.L677
.L685:
	movzwl	(%r11), %edi
	leaq	2(%r11), %rcx
	movl	%edi, %eax
	cmpw	$127, %di
	ja	.L839
	addl	$1, %edx
	movq	%rcx, %r11
	jmp	.L698
.L840:
	addl	$1, %edx
	movq	%rax, %r11
.L704:
	cmpq	%r13, %r11
	jnb	.L677
.L670:
	movzwl	(%r11), %edi
	leaq	2(%r11), %rax
	movl	%edi, %ecx
	cmpw	$127, %di
	jbe	.L840
	cmpl	$2047, %edi
	ja	.L705
	addl	$2, %edx
	movq	%rax, %r11
	jmp	.L704
.L841:
	addl	$1, %edx
	movq	%rax, %r11
.L681:
	cmpq	%r11, %r13
	jbe	.L677
.L684:
	movzwl	(%r11), %ecx
	leaq	2(%r11), %rax
	movl	%ecx, %edi
	cmpw	$127, %cx
	jbe	.L841
	cmpl	$2047, %ecx
	ja	.L682
	addl	$2, %edx
	movq	%rax, %r11
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L674:
	andl	$-2048, %ecx
	cmpl	$55296, %ecx
	je	.L675
	addl	$3, %edx
	movq	%rdi, %r11
	jmp	.L673
.L693:
	addl	$2, %edx
	movq	%rcx, %r11
	jmp	.L698
.L688:
	andl	$-2048, %edi
	cmpl	$55296, %edi
	je	.L689
	addl	$3, %edx
	movq	%rcx, %r11
	jmp	.L687
.L677:
	subq	%rbx, %r8
	addl	%r8d, %edx
	jmp	.L632
.L675:
	testb	$4, %ah
	jne	.L676
	cmpq	%rdi, %r13
	ja	.L842
.L676:
	addl	$1, %edx
	addl	$1, %r12d
	movq	%rdi, %r11
	jmp	.L673
.L705:
	andl	$-2048, %edi
	cmpl	$55296, %edi
	je	.L706
	addl	$3, %edx
	movq	%rax, %r11
	jmp	.L704
.L831:
	andb	$4, %ch
	je	.L615
.L616:
	cmpl	$55296, %r9d
	leal	3(%r10), %eax
	movq	%rdi, %r8
	cmovb	%eax, %r10d
	addl	$1, %r12d
	jmp	.L617
.L737:
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	jmp	.L665
.L609:
	andb	$4, %ch
	jne	.L610
	movzwl	2(%r8), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L843
.L610:
	addl	$2, %r10d
	addl	$1, %r12d
	movq	%rdi, %r8
	jmp	.L607
.L838:
	testb	$4, %ah
	jne	.L697
	cmpq	%rcx, %r13
	ja	.L695
.L697:
	cmpl	$55296, %r9d
	leal	3(%rdx), %eax
	movq	%rcx, %r11
	cmovb	%eax, %edx
	addl	$1, %r12d
	jmp	.L698
.L689:
	testb	$4, %ah
	jne	.L690
	cmpq	%rcx, %r13
	ja	.L844
.L690:
	addl	$2, %edx
	addl	$1, %r12d
	movq	%rcx, %r11
	jmp	.L687
.L722:
	movl	$2, %r10d
	jmp	.L620
.L732:
	movl	$2, %edx
	jmp	.L657
.L816:
	movq	%rbx, %r8
	xorl	%r12d, %r12d
	jmp	.L658
.L636:
	andb	$4, %ch
	je	.L845
.L637:
	cmpl	$65535, %r9d
	ja	.L638
	addl	$3, %r10d
.L639:
	addl	$1, %r12d
	movq	%rdi, %r8
	jmp	.L634
.L826:
	movq	%rcx, %r8
.L811:
	movl	$3, %r10d
	jmp	.L620
.L728:
	movq	%rcx, %r8
	jmp	.L620
.L706:
	andb	$4, %ch
	jne	.L707
	cmpq	%rax, %r13
	ja	.L846
.L707:
	cmpl	$65535, %r9d
	ja	.L708
	addl	$3, %edx
.L709:
	addl	$1, %r12d
	movq	%rax, %r11
	jmp	.L704
.L842:
	movzwl	2(%r11), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L676
	addq	$4, %r11
	addl	$4, %edx
	jmp	.L673
.L615:
	movzwl	2(%r8), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L616
	addq	$4, %r8
	addl	$4, %r10d
	jmp	.L617
.L835:
	addq	$4, %r8
	addl	$4, %r10d
	jmp	.L592
.L601:
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L602
	addl	$3, %r10d
	movq	%rdi, %r8
	jmp	.L600
.L733:
	movl	$3, %edx
	jmp	.L657
.L602:
	andb	$4, %ch
	jne	.L603
	movzwl	2(%r8), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L603
	addq	$4, %r8
	addl	$4, %r10d
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L695:
	movzwl	2(%r11), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L697
	addq	$4, %r11
	addl	$4, %edx
	jmp	.L698
.L843:
	addq	$4, %r8
	addl	$4, %r10d
	jmp	.L607
.L845:
	movzwl	2(%r8), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L637
	addq	$4, %r8
	addl	$4, %r10d
	jmp	.L634
.L846:
	movzwl	2(%r11), %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L707
	addq	$4, %r11
	addl	$4, %edx
	jmp	.L704
.L822:
	movq	%rcx, %r11
	jmp	.L657
.L682:
	andl	$-2048, %ecx
	cmpl	$55296, %ecx
	je	.L683
	addl	$3, %edx
	movq	%rax, %r11
	jmp	.L681
.L844:
	movzwl	2(%r11), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L690
	addq	$4, %r11
	addl	$4, %edx
	jmp	.L687
.L827:
	movq	%rcx, %r8
	movl	$4, %r10d
	jmp	.L620
.L683:
	testw	$1024, %di
	jne	.L603
	cmpq	%rax, %r13
	jbe	.L603
	movzwl	2(%r11), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L603
	addq	$4, %r11
	addl	$4, %edx
	jmp	.L681
.L708:
	addl	$4, %edx
	jmp	.L709
.L821:
	movq	%rcx, %r11
	movl	$3, %edx
	jmp	.L657
.L828:
	movq	%rcx, %r11
	movl	$4, %edx
	jmp	.L657
.L638:
	addl	$4, %r10d
	jmp	.L639
	.cfi_endproc
.LFE2062:
	.size	u_strToUTF8WithSub_67, .-u_strToUTF8WithSub_67
	.p2align 4
	.globl	u_strToUTF8_67
	.type	u_strToUTF8_67, @function
u_strToUTF8_67:
.LFB2063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r9
	movl	$-1, %r9d
	pushq	$0
	call	u_strToUTF8WithSub_67
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2063:
	.size	u_strToUTF8_67, .-u_strToUTF8_67
	.p2align 4
	.globl	u_strFromJavaModifiedUTF8WithSub_67
	.type	u_strFromJavaModifiedUTF8WithSub_67, @function
u_strFromJavaModifiedUTF8WithSub_67:
.LFB2064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %rcx
	movq	%rdx, -128(%rbp)
	movq	%rdi, -104(%rbp)
	movl	%esi, -72(%rbp)
	movl	(%rcx), %edx
	movq	%rax, -112(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L849
	testq	%r12, %r12
	movl	%r8d, %r14d
	sete	%cl
	testl	%r8d, %r8d
	setne	%al
	testb	%al, %cl
	jne	.L851
	cmpl	$-1, %r8d
	jl	.L851
	testq	%rdi, %rdi
	jne	.L903
	testl	%esi, %esi
	jne	.L851
.L903:
	movl	-72(%rbp), %eax
	shrl	$31, %eax
	cmpl	$1114111, %r9d
	setg	%cl
	orb	%al, %cl
	jne	.L851
	movl	%r9d, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L851
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L854
	movl	$0, (%rax)
.L854:
	movslq	-72(%rbp), %rax
	movq	-104(%rbp), %r13
	leaq	0(%r13,%rax,2), %rax
	movq	%rax, -96(%rbp)
	cmpl	$-1, %r14d
	je	.L950
.L855:
	leaq	-60(%rbp), %rdi
	testl	%r14d, %r14d
	movq	%r13, %rcx
	movl	%r9d, %r15d
	movl	$0, -60(%rbp)
	movl	%r14d, %r13d
	movq	%r12, %r14
	movl	$0, -68(%rbp)
	movq	%rdi, -80(%rbp)
	setg	-114(%rbp)
	xorl	%eax, %eax
.L876:
	movq	-96(%rbp), %r10
	movl	%r13d, %esi
	subl	%eax, %esi
	subq	%rcx, %r10
	sarq	%r10
	cmpl	%esi, %r10d
	movl	%r10d, %ebx
	setge	%r8b
	andb	-114(%rbp), %r8b
	je	.L861
	cmpb	$0, (%r14)
	jns	.L951
.L861:
	cmpl	$65535, %r15d
	jg	.L869
	movslq	%esi, %rdi
	sarl	$31, %esi
	imulq	$1431655766, %rdi, %rdi
	shrq	$32, %rdi
	subl	%esi, %edi
	movl	%edi, %esi
	cmpl	%ebx, %edi
	cmovg	%ebx, %esi
	cmpl	$2, %esi
	jle	.L869
	subl	$1, %esi
	movl	%r15d, %edi
	leaq	2(%rcx), %rbx
	leaq	4(%rcx,%rsi,2), %r12
	movq	%r12, %r15
	movl	%r13d, %r12d
	movl	%edi, %r13d
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L953:
	movw	%dx, -2(%rbx)
	movq	%rbx, %rdi
	movl	%esi, %eax
.L871:
	addq	$2, %rbx
	cmpq	%rbx, %r15
	je	.L952
.L875:
	movslq	%eax, %rdx
	leal	1(%rax), %esi
	movzbl	(%r14,%rdx), %edx
	movl	%esi, -60(%rbp)
	testb	%dl, %dl
	jns	.L953
	movzbl	%dl, %ecx
	cmpb	$-33, %dl
	jbe	.L872
	cmpb	$-17, %dl
	ja	.L873
	movslq	%esi, %rsi
	movzbl	(%r14,%rsi), %edi
	leal	-128(%rdi), %edx
	cmpb	$63, %dl
	ja	.L873
	movzbl	1(%r14,%rsi), %esi
	addl	$-128, %esi
	cmpb	$63, %sil
	ja	.L873
	sall	$12, %ecx
	movzbl	%dl, %edx
	movq	%rbx, %rdi
	addl	$3, %eax
	movl	%ecx, %r8d
	movzbl	%sil, %ecx
	sall	$6, %edx
	addq	$2, %rbx
	orl	%r8d, %ecx
	movl	%eax, -60(%rbp)
	orl	%edx, %ecx
	movw	%cx, -4(%rbx)
	cmpq	%rbx, %r15
	jne	.L875
	.p2align 4,,10
	.p2align 3
.L952:
	movl	%r13d, %r15d
	movq	%rdi, %rcx
	movl	%r12d, %r13d
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L881:
	movq	-88(%rbp), %rax
	movl	$10, (%rax)
	xorl	%eax, %eax
.L849:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L954
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L873:
	.cfi_restore_state
	testl	%r13d, %r13d
	js	.L881
	movq	-80(%rbp), %rsi
	movq	%r14, %rdi
	movl	$-1, %r8d
	movl	%r12d, %edx
	call	utf8_nextCharSafeBody_67@PLT
	addl	$1, -68(%rbp)
	movl	-60(%rbp), %eax
	movq	%rbx, %rdi
	movw	%r13w, -2(%rbx)
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L872:
	cmpb	$-65, %dl
	jbe	.L873
	movslq	%esi, %rsi
	movzbl	(%r14,%rsi), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L873
	sall	$6, %ecx
	movzbl	%dl, %edx
	addl	$2, %eax
	movq	%rbx, %rdi
	andw	$1984, %cx
	movl	%eax, -60(%rbp)
	orl	%ecx, %edx
	movw	%dx, -2(%rbx)
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L851:
	movq	-88(%rbp), %rax
	movl	$1, (%rax)
	xorl	%eax, %eax
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L951:
	cmpl	%eax, %r13d
	jle	.L861
	movslq	%eax, %rsi
	xorl	%ebx, %ebx
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L863:
	leal	1(%rsi), %edi
	addq	$1, %rsi
	addq	$2, %rcx
	movw	%dx, -2(%rcx)
	cmpl	%esi, %r13d
	jle	.L955
	movl	%r8d, %ebx
.L865:
	movzbl	(%r14,%rsi), %edx
	movl	%esi, %r11d
	testb	%dl, %dl
	jns	.L863
	movl	%eax, %edi
	testb	%bl, %bl
	je	.L864
	movl	%esi, -60(%rbp)
	movl	%esi, %edi
.L864:
	movl	%r11d, %ebx
	movl	%r13d, %esi
	subl	%eax, %ebx
	subl	%r11d, %esi
	movl	%ebx, %eax
	movl	%r10d, %ebx
	subl	%eax, %ebx
	movl	%edi, %eax
	jmp	.L861
.L950:
	movzbl	(%r12), %eax
	testb	%al, %al
	je	.L896
	movq	-96(%rbp), %rcx
	cmpq	%rcx, %r13
	jnb	.L904
	movq	%rcx, %rdx
	testb	%al, %al
	jns	.L857
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L860:
	js	.L859
	cmpq	%r13, %rdx
	jbe	.L859
.L857:
	addq	$1, %r12
	movw	%ax, 0(%r13)
	addq	$2, %r13
	movzbl	(%r12), %eax
	testb	%al, %al
	jne	.L860
	movq	%r13, %rdx
	subq	-104(%rbp), %rdx
	sarq	%rdx
.L856:
	cmpq	$0, -128(%rbp)
	je	.L890
	movq	-128(%rbp), %rax
	movl	%edx, (%rax)
.L890:
	movq	-104(%rbp), %rbx
	movq	-88(%rbp), %rcx
	movl	-72(%rbp), %esi
	movq	%rbx, %rdi
	call	u_terminateUChars_67@PLT
	movq	%rbx, %rax
	jmp	.L849
.L955:
	movl	%edi, %ebx
	movl	%r13d, %esi
	movl	%edi, -60(%rbp)
	subl	%eax, %ebx
	subl	%edi, %esi
	movl	%ebx, %eax
	movl	%r10d, %ebx
	subl	%eax, %ebx
	movl	%edi, %eax
	jmp	.L861
.L904:
	movq	-104(%rbp), %r13
.L859:
	movq	%r12, %rdi
	movl	%r9d, -68(%rbp)
	call	strlen@PLT
	movl	-68(%rbp), %r9d
	movl	%eax, %r14d
	jmp	.L855
.L869:
	movq	-96(%rbp), %rdi
	movq	%r14, %r12
	movl	%r15d, %r9d
	movl	%r13d, %r14d
	movq	%rcx, %r13
	cmpq	%rdi, %rcx
	jnb	.L949
	cmpl	%eax, %r14d
	jle	.L949
	movl	%r9d, %ecx
	sarl	$10, %r15d
	leaq	-60(%rbp), %rbx
	andw	$1023, %cx
	subw	$10304, %r15w
	movq	%rbx, -80(%rbp)
	movl	%r14d, %ebx
	orw	$-9216, %cx
	movw	%r15w, -114(%rbp)
	movq	%rdi, %r14
	movl	%r9d, %r15d
	movw	%cx, -96(%rbp)
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L956:
	movw	%cx, 0(%r13)
	movl	%esi, %eax
	addq	$2, %r13
.L878:
	cmpl	%eax, %ebx
	jle	.L901
	cmpq	%r14, %r13
	jnb	.L901
.L868:
	movslq	%eax, %rcx
	leal	1(%rax), %esi
	movzbl	(%r12,%rcx), %ecx
	movl	%esi, -60(%rbp)
	testb	%cl, %cl
	jns	.L956
	movzbl	%cl, %r11d
	cmpb	$-33, %cl
	jbe	.L879
	cmpb	$-17, %cl
	ja	.L880
	leal	2(%rax), %ecx
	cmpl	%ebx, %ecx
	jge	.L880
	movslq	%esi, %rsi
	movzbl	(%r12,%rsi), %edi
	leal	-128(%rdi), %r8d
	cmpb	$63, %r8b
	jbe	.L957
	.p2align 4,,10
	.p2align 3
.L880:
	testl	%r15d, %r15d
	js	.L881
	movq	-80(%rbp), %rsi
	movl	%r11d, %ecx
	movl	$-1, %r8d
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	utf8_nextCharSafeBody_67@PLT
	addl	$1, -68(%rbp)
	leaq	2(%r13), %rcx
	cmpl	$65535, %r15d
	jg	.L882
	movw	%r15w, 0(%r13)
	movl	-60(%rbp), %eax
	movq	%rcx, %r13
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L879:
	cmpl	$191, %r11d
	jle	.L880
	cmpl	%ebx, %esi
	jge	.L880
	movslq	%esi, %rsi
	movzbl	(%r12,%rsi), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L880
	movl	%r11d, %esi
	movzbl	%cl, %ecx
	addl	$2, %eax
	addq	$2, %r13
	sall	$6, %esi
	movl	%eax, -60(%rbp)
	andw	$1984, %si
	orl	%esi, %ecx
	movw	%cx, -2(%r13)
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L882:
	movzwl	-114(%rbp), %eax
	movw	%ax, 0(%r13)
	movl	-60(%rbp), %eax
	cmpq	%rcx, %r14
	jbe	.L902
	movzwl	-96(%rbp), %edi
	addq	$4, %r13
	movw	%di, -2(%r13)
	jmp	.L878
.L901:
	movl	%ebx, %r14d
	movl	%r15d, %r9d
.L949:
	xorl	%esi, %esi
.L867:
	cmpl	%eax, %r14d
	jle	.L883
	leaq	-60(%rbp), %rbx
	movq	%r13, -96(%rbp)
	leal	1(%rsi), %r15d
	movq	%r12, %r13
	movl	%r9d, -80(%rbp)
	movl	%r14d, %r12d
	movq	%rbx, %r14
	movl	-68(%rbp), %ebx
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L958:
	movl	%r15d, %esi
	movl	%edx, %eax
.L886:
	addl	$1, %r15d
	cmpl	%eax, %r12d
	jle	.L947
.L884:
	movslq	%eax, %rcx
	leal	1(%rax), %edx
	movzbl	0(%r13,%rcx), %ecx
	movl	%edx, -60(%rbp)
	testb	%cl, %cl
	jns	.L958
	movzbl	%cl, %r10d
	cmpb	$-33, %cl
	jbe	.L887
	cmpb	$-17, %cl
	ja	.L888
	leal	2(%rax), %ecx
	cmpl	%r12d, %ecx
	jge	.L888
	movslq	%edx, %rdx
	movzbl	0(%r13,%rdx), %edi
	leal	-128(%rdi), %ecx
	cmpb	$63, %cl
	ja	.L888
	movzbl	1(%r13,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L888
	addl	$3, %eax
	movl	%r15d, %esi
	movl	%eax, -60(%rbp)
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L887:
	cmpl	$191, %r10d
	jle	.L888
	cmpl	%r12d, %edx
	jl	.L959
	.p2align 4,,10
	.p2align 3
.L888:
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	js	.L881
	movq	%r14, %rsi
	movl	$-1, %r8d
	movl	%r10d, %ecx
	movl	%r12d, %edx
	movq	%r13, %rdi
	addl	$1, %ebx
	call	utf8_nextCharSafeBody_67@PLT
	movl	-60(%rbp), %eax
	movl	%r15d, %esi
	jmp	.L886
.L959:
	movslq	%edx, %rdx
	movzbl	0(%r13,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L888
	addl	$2, %eax
	movl	%r15d, %esi
	movl	%eax, -60(%rbp)
	jmp	.L886
.L957:
	movzbl	1(%r12,%rsi), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L880
	sall	$12, %r11d
	movzbl	%cl, %ecx
	movzbl	%r8b, %r8d
	addl	$3, %eax
	orl	%r11d, %ecx
	sall	$6, %r8d
	movl	%eax, -60(%rbp)
	addq	$2, %r13
	orl	%r8d, %ecx
	movw	%cx, -2(%r13)
	jmp	.L878
.L947:
	movl	%ebx, -68(%rbp)
	movq	-96(%rbp), %r13
.L883:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L889
	movl	-68(%rbp), %edi
	movl	%edi, (%rax)
.L889:
	movq	%r13, %rdx
	movq	-128(%rbp), %rax
	subq	-104(%rbp), %rdx
	sarq	%rdx
	addl	%esi, %edx
	testq	%rax, %rax
	je	.L890
	movl	%edx, (%rax)
	jmp	.L890
.L896:
	xorl	%edx, %edx
	jmp	.L856
.L902:
	movl	%ebx, %r14d
	movl	%r15d, %r9d
	movq	%rcx, %r13
	movl	$1, %esi
	jmp	.L867
.L954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2064:
	.size	u_strFromJavaModifiedUTF8WithSub_67, .-u_strFromJavaModifiedUTF8WithSub_67
	.p2align 4
	.globl	u_strToJavaModifiedUTF8_67
	.type	u_strToJavaModifiedUTF8_67, @function
u_strToJavaModifiedUTF8_67:
.LFB2065:
	.cfi_startproc
	endbr64
	movl	(%r9), %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L1032
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	testq	%rcx, %rcx
	sete	%dil
	testl	%r8d, %r8d
	setne	%al
	testb	%al, %dil
	jne	.L962
	cmpl	$-1, %r8d
	jl	.L962
	testq	%r12, %r12
	sete	%dil
	testl	%esi, %esi
	setne	%al
	testb	%al, %dil
	jne	.L962
	testl	%esi, %esi
	js	.L962
	movslq	%esi, %r13
	addq	%r12, %r13
	cmpl	$-1, %r8d
	je	.L1037
	movq	%r12, %rbx
	xorl	%r15d, %r15d
	testq	%rcx, %rcx
	je	.L985
.L972:
	movslq	%r8d, %r8
	leaq	(%rcx,%r8,2), %r15
	.p2align 4,,10
	.p2align 3
.L985:
	movq	%r15, %rdi
	movq	%r13, %r11
	subq	%rcx, %rdi
	subq	%rbx, %r11
	sarq	%rdi
	movl	%r11d, %r8d
	movl	%edi, %eax
	cmpl	%edi, %r11d
	jl	.L974
	testl	%edi, %edi
	jle	.L974
	cmpw	$127, (%rcx)
	jbe	.L1038
.L974:
	movslq	%r8d, %r11
	sarl	$31, %r8d
	imulq	$1431655766, %r11, %r11
	shrq	$32, %r11
	subl	%r8d, %r11d
	cmpl	%r11d, %eax
	cmovle	%eax, %r11d
	cmpl	$2, %r11d
	jle	.L1039
	leal	-1(%r11), %eax
	movq	%rcx, %r8
	movq	%rax, %r11
	leaq	2(%rcx,%rax,2), %r14
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1041:
	movb	%dil, (%rbx)
	addq	$1, %rbx
.L982:
	cmpq	%r8, %r14
	je	.L1040
.L984:
	movzwl	(%r8), %edi
	addq	$2, %r8
	leal	-1(%rdi), %r10d
	movl	%edi, %eax
	cmpl	$126, %r10d
	jbe	.L1041
	movl	%edi, %r10d
	andl	$63, %eax
	shrl	$6, %r10d
	orl	$-128, %eax
	cmpl	$2047, %edi
	ja	.L983
	orl	$-64, %r10d
	movb	%al, 1(%rbx)
	addq	$2, %rbx
	movb	%r10b, -2(%rbx)
	cmpq	%r8, %r14
	jne	.L984
.L1040:
	movslq	%r11d, %r11
	leaq	2(%rcx,%r11,2), %rcx
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L962:
	movl	$1, (%r9)
	xorl	%eax, %eax
.L960:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L983:
	.cfi_restore_state
	shrl	$12, %edi
	andl	$63, %r10d
	movb	%al, 2(%rbx)
	addq	$3, %rbx
	orl	$-32, %edi
	orl	$-128, %r10d
	movb	%dil, -3(%rbx)
	movb	%r10b, -2(%rbx)
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1038:
	cmpq	%r15, %rcx
	jnb	.L974
	movq	%rcx, %r10
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L976:
	addq	$2, %r10
	addq	$1, %rbx
	movb	%r8b, -1(%rbx)
	cmpq	%r10, %r15
	jbe	.L1035
.L977:
	movzwl	(%r10), %eax
	movl	%eax, %r8d
	subl	$1, %eax
	cmpl	$126, %eax
	jbe	.L976
.L1035:
	movq	%r10, %rax
	movl	%r11d, %r8d
	subq	%rcx, %rax
	movl	%edi, %ecx
	sarq	%rax
	subl	%eax, %ecx
	subl	%eax, %r8d
	movl	%ecx, %eax
	movq	%r10, %rcx
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L1039:
	cmpq	%r15, %rcx
	jb	.L980
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1042:
	cmpq	%rbx, %r13
	jbe	.L1002
	movb	%al, (%rbx)
	addq	$1, %rbx
.L988:
	cmpq	%rcx, %r15
	jbe	.L1001
.L980:
	movzwl	(%rcx), %eax
	addq	$2, %rcx
	leal	-1(%rax), %r8d
	movl	%eax, %edi
	cmpl	$126, %r8d
	jbe	.L1042
	movq	%r13, %r8
	subq	%rbx, %r8
	cmpl	$2047, %eax
	ja	.L989
	cmpq	$1, %r8
	jle	.L1003
	shrl	$6, %eax
	andl	$63, %edi
	addq	$2, %rbx
	orl	$-64, %eax
	orl	$-128, %edi
	movb	%al, -2(%rbx)
	movb	%dil, -1(%rbx)
	cmpq	%rcx, %r15
	ja	.L980
.L1001:
	xorl	%eax, %eax
.L979:
	subq	%r12, %rbx
	leal	(%rax,%rbx), %r8d
	testq	%rdx, %rdx
	je	.L995
	movl	%r8d, (%rdx)
.L995:
	movq	%r12, %rdi
	movq	%r9, %rcx
	movl	%r8d, %edx
	call	u_terminateChars_67@PLT
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L989:
	.cfi_restore_state
	cmpq	$2, %r8
	jle	.L1043
	movl	%eax, %r8d
	shrl	$6, %eax
	andl	$63, %edi
	addq	$3, %rbx
	shrl	$12, %r8d
	andl	$63, %eax
	orl	$-128, %edi
	orl	$-32, %r8d
	orl	$-128, %eax
	movb	%dil, -1(%rbx)
	movb	%r8b, -3(%rbx)
	movb	%al, -2(%rbx)
	jmp	.L988
.L1032:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L1037:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movzwl	(%rcx), %edi
	movl	%edi, %eax
	cmpw	$127, %di
	ja	.L997
	testl	%edi, %edi
	je	.L1004
	movq	%r12, %rbx
	cmpq	%r13, %r12
	jb	.L967
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L970:
	testl	%edi, %edi
	je	.L1030
	cmpq	%rbx, %r13
	jbe	.L969
.L967:
	addq	$2, %rcx
	movb	%al, (%rbx)
	addq	$1, %rbx
	movzwl	(%rcx), %edi
	movl	%edi, %eax
	cmpw	$127, %di
	jbe	.L970
.L966:
	movq	%rcx, %rdi
	movq	%r9, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movl	%esi, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	u_strlen_67@PLT
	movq	-56(%rbp), %rcx
	movl	-60(%rbp), %esi
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %r9
	movl	%eax, %r8d
	jmp	.L972
.L1002:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L993:
	cmpq	%rcx, %r15
	jbe	.L979
.L991:
	movzwl	(%rcx), %edi
	addq	$2, %rcx
	leal	-1(%rdi), %r8d
	cmpl	$126, %r8d
	ja	.L992
	addl	$1, %eax
	cmpq	%rcx, %r15
	ja	.L991
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L992:
	movl	%eax, %r8d
	xorl	%eax, %eax
	cmpl	$2047, %edi
	seta	%al
	leal	2(%rax,%r8), %eax
	jmp	.L993
.L1004:
	movq	%r12, %rbx
.L969:
	testl	%edi, %edi
	jne	.L966
.L1030:
	subq	%r12, %rbx
	testq	%rdx, %rdx
	je	.L971
	movl	%ebx, (%rdx)
.L971:
	movq	%r9, %rcx
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	u_terminateChars_67@PLT
	movq	%r12, %rax
	jmp	.L960
.L1043:
	movl	$3, %eax
	jmp	.L993
.L1003:
	movl	$2, %eax
	jmp	.L993
.L997:
	movq	%r12, %rbx
	jmp	.L966
	.cfi_endproc
.LFE2065:
	.size	u_strToJavaModifiedUTF8_67, .-u_strToJavaModifiedUTF8_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
