	.file	"ucnv_lmb.cpp"
	.text
	.p2align 4
	.type	_LMBCSSafeClone, @function
_LMBCSSafeClone:
.LFB2117:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L14
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	288(%rsi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	movdqu	(%rbx), %xmm0
	leaq	160(%rbx), %r14
	movups	%xmm0, 288(%rsi)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%r12)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%r12)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%r12)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%r12)
	movdqu	80(%rbx), %xmm5
	movups	%xmm5, 80(%r12)
	movdqu	96(%rbx), %xmm6
	movups	%xmm6, 96(%r12)
	movdqu	112(%rbx), %xmm7
	movups	%xmm7, 112(%r12)
	movdqu	128(%rbx), %xmm0
	movups	%xmm0, 128(%r12)
	movdqu	144(%rbx), %xmm1
	movups	%xmm1, 144(%r12)
	movq	160(%rbx), %rax
	movq	%rax, 160(%r12)
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4
	call	ucnv_incrementRefCount_67@PLT
.L4:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L5
	movq	%r12, 16(%r13)
	movq	%r13, %rax
	movb	$1, 62(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$456, (%rdx)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2117:
	.size	_LMBCSSafeClone, .-_LMBCSSafeClone
	.p2align 4
	.type	LMBCSConversionWorker, @function
LMBCSConversionWorker:
.LFB2118:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-60(%rbp), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movb	%sil, -65(%rbp)
	movzwl	(%rcx), %esi
	xorl	%ecx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	%bl, %eax
	movq	(%rdi,%rax,8), %rdi
	call	ucnv_MBCSFromUChar32_67@PLT
	testl	%eax, %eax
	jle	.L16
	movl	-60(%rbp), %esi
	cmpl	$1, %eax
	leal	-8(,%rax,8), %ecx
	movb	%bl, (%r12)
	sete	%dil
	movl	%esi, %edx
	shrl	%cl, %edx
	cmpb	$31, %dl
	setbe	%dl
	andl	%edi, %edx
	testb	%bl, %bl
	jne	.L17
.L29:
	movq	%r15, %rcx
	xorl	%r8d, %r8d
.L18:
	testb	%dl, %dl
	jne	.L22
	cmpl	$3, %eax
	je	.L24
	jg	.L25
	cmpl	$1, %eax
	je	.L30
	cmpl	$2, %eax
	jne	.L15
.L26:
	movl	%esi, %eax
	leaq	1(%rcx), %r8
	movb	%ah, (%rcx)
.L23:
	movb	%sil, (%r8)
	addq	$1, %r8
	subq	%r15, %r8
.L15:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$40, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	cmpb	%bl, 160(%r14)
	je	.L29
	movb	%bl, (%r15)
	cmpb	$15, %bl
	jbe	.L31
	testb	%dil, %dil
	je	.L31
	movb	%bl, 1(%r15)
	leaq	2(%r15), %r8
	testb	%dl, %dl
	je	.L23
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%r8d, %r8d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	$4, %eax
	jne	.L15
	movl	%esi, %eax
	addq	$1, %rcx
	shrl	$24, %eax
	movb	%al, -1(%rcx)
.L24:
	movl	%esi, %eax
	addq	$1, %rcx
	shrl	$16, %eax
	movb	%al, -1(%rcx)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L16:
	movzbl	-65(%rbp), %r14d
	xorl	%r8d, %r8d
	movb	$1, 0(%r13,%r14)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	1(%r15), %rcx
	movl	$1, %r8d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rcx, %r8
	jmp	.L23
.L42:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2118:
	.size	LMBCSConversionWorker, .-LMBCSConversionWorker
	.p2align 4
	.type	_LMBCSGetNextUCharWorker, @function
_LMBCSGetNextUCharWorker:
.LFB2122:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdi
	cmpq	%rdi, %rax
	jnb	.L75
	leaq	1(%rax), %r9
	movq	%r9, 16(%rbx)
	movzbl	(%rax), %ecx
	leal	-32(%rcx), %edx
	cmpb	$95, %dl
	setbe	%dl
	cmpb	$25, %cl
	ja	.L46
	movl	$33564161, %esi
	shrq	%cl, %rsi
	andl	$1, %esi
	orl	%esi, %edx
.L46:
	movzbl	%cl, %r8d
	testb	%dl, %dl
	je	.L76
.L43:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	cmpb	$15, %cl
	je	.L78
	cmpb	$20, %cl
	je	.L79
	movq	8(%rbx), %rsi
	movq	16(%rsi), %rdx
	cmpb	$32, %cl
	ja	.L54
	cmpb	$19, %cl
	ja	.L55
	movslq	%r8d, %r10
	movq	(%rdx,%r10,8), %r10
	testq	%r10, %r10
	je	.L55
	cmpb	$15, %cl
	jbe	.L57
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rdi
	jb	.L74
	movsbl	1(%rax), %edx
	cmpl	%edx, %r8d
	je	.L80
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r9, %rsi
	movq	%r10, %rdi
	call	ucnv_MBCSSimpleGetNextUChar_67@PLT
	addq	$2, 16(%rbx)
	movl	%eax, %r8d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$1, (%rsi)
	movl	$65535, %r8d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rdi
	jb	.L74
	leaq	2(%rax), %rcx
	movq	%rcx, 16(%rbx)
	movzbl	1(%rax), %ecx
	movq	%rdx, 16(%rbx)
	movzbl	2(%rax), %r8d
	xorl	%eax, %eax
	cmpb	$-10, %cl
	je	.L53
	movzbl	%r8b, %eax
	movl	%ecx, %r8d
.L53:
	sall	$8, %r8d
	orl	%eax, %r8d
	movzwl	%r8w, %r8d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rdi
	jb	.L74
	movq	%rdx, 16(%rbx)
	movzbl	1(%rax), %r8d
	leal	-32(%r8), %edx
	testb	%r8b, %r8b
	cmovns	%edx, %r8d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L54:
	movzbl	160(%rdx), %esi
	movq	(%rdx,%rsi,8), %r13
	cmpb	$15, %sil
	ja	.L81
	movq	56(%r13), %rax
	movzwl	(%rax,%r8,4), %r8d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$10, (%r12)
	xorl	%r8d, %r8d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$11, (%r12)
	movl	$65535, %r8d
	movq	%rdi, 16(%rbx)
	jmp	.L43
.L81:
	movsbl	%cl, %esi
	movq	%r13, %rdi
	call	ucnv_MBCSIsLeadByte_67@PLT
	movq	24(%rbx), %rdx
	testb	%al, %al
	movq	16(%rbx), %rax
	jne	.L63
	cmpq	%rdx, %rax
	ja	.L73
	leaq	-1(%rax), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	call	ucnv_MBCSSimpleGetNextUChar_67@PLT
	movl	%eax, %r8d
	jmp	.L43
.L57:
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rdi
	jb	.L74
	movq	%rdx, 16(%rbx)
	movzbl	1(%rax), %eax
	testb	%al, %al
	jns	.L61
	movq	56(%r10), %rdx
	movzwl	(%rdx,%rax,4), %r8d
	jmp	.L43
.L63:
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L73
	leaq	-1(%rax), %rsi
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r13, %rdi
	call	ucnv_MBCSSimpleGetNextUChar_67@PLT
	addq	$1, 16(%rbx)
	movl	%eax, %r8d
	jmp	.L43
.L73:
	movl	$11, (%r12)
	movl	$65535, %r8d
	movq	%rdx, 16(%rbx)
	jmp	.L43
.L61:
	movq	16(%rsi), %rdx
	leaq	-42(%rbp), %rsi
	movq	(%rdx), %rdi
	movb	%cl, -42(%rbp)
	movl	$2, %edx
	xorl	%ecx, %ecx
	movb	%al, -41(%rbp)
	call	ucnv_MBCSSimpleGetNextUChar_67@PLT
	movl	%eax, %r8d
	jmp	.L43
.L80:
	leaq	2(%rax), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r10, %rdi
	movq	%rsi, 16(%rbx)
	call	ucnv_MBCSSimpleGetNextUChar_67@PLT
	addq	$1, 16(%rbx)
	movl	%eax, %r8d
	jmp	.L43
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2122:
	.size	_LMBCSGetNextUCharWorker, .-_LMBCSGetNextUCharWorker
	.p2align 4
	.type	_LMBCSToUnicodeWithOffsets, @function
_LMBCSToUnicodeWithOffsets:
.LFB2123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%rax, -96(%rbp)
	testl	%ecx, %ecx
	jg	.L83
	movq	%rax, %r14
.L105:
	movq	24(%rbx), %r9
	cmpq	%r14, %r9
	jbe	.L82
	movq	32(%rbx), %rax
	cmpq	%rax, 40(%rbx)
	jbe	.L86
	movq	8(%rbx), %rsi
	movzbl	64(%rsi), %r11d
	testb	%r11b, %r11b
	je	.L87
	movsbq	%r11b, %r10
	movq	%r9, %rax
	leaq	-59(%rbp), %r13
	movl	$3, %ecx
	movl	$3, %r12d
	subq	%r14, %rax
	movq	%r10, %rdx
	movq	%r13, %rdi
	subq	%r10, %r12
	movq	%r9, -80(%rbp)
	cmpq	%rax, %r12
	movb	%r11b, -81(%rbp)
	cmova	%rax, %r12
	addq	$65, %rsi
	movq	%r10, -72(%rbp)
	call	__memcpy_chk@PLT
	movq	-72(%rbp), %r10
	movq	%r14, %rsi
	movq	%r12, %rdx
	leaq	0(%r13,%r10), %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r10
	movq	%r13, 16(%rbx)
	movq	%r15, %rsi
	movzbl	-81(%rbp), %r11d
	movq	%rbx, %rdi
	leaq	(%r10,%r12), %rax
	addq	%r13, %rax
	addl	%r11d, %r12d
	movq	%rax, 24(%rbx)
	call	_LMBCSGetNextUCharWorker
	movq	16(%rbx), %rdx
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r9
	subq	%r13, %rdx
	subq	%r10, %rdx
	movq	%r9, 24(%rbx)
	addq	%r14, %rdx
	cmpl	$11, (%r15)
	movq	%rdx, 16(%rbx)
	je	.L121
	movq	8(%rbx), %rdx
	movb	$0, 64(%rdx)
	movl	(%r15), %edx
	testl	%edx, %edx
	jle	.L122
.L95:
	movq	8(%rbx), %rax
	movb	%r12b, 64(%rax)
	testb	%r12b, %r12b
	jg	.L123
	.p2align 4,,10
	.p2align 3
.L104:
	cmpl	$11, (%r15)
	je	.L124
.L82:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r14, %r13
	call	_LMBCSGetNextUCharWorker
	movzbl	16(%rbx), %r12d
	movl	(%r15), %edx
	subl	%r14d, %r12d
	testl	%edx, %edx
	jg	.L95
.L122:
	cmpw	$-3, %ax
	ja	.L96
	movq	32(%rbx), %rdx
	leaq	2(%rdx), %rcx
	movq	%rcx, 32(%rbx)
	movw	%ax, (%rdx)
	movq	48(%rbx), %rax
	testq	%rax, %rax
	je	.L97
	leaq	4(%rax), %rdx
	subq	-96(%rbp), %r14
	movq	%rdx, 48(%rbx)
	movl	%r14d, (%rax)
.L97:
	movq	16(%rbx), %r14
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L96:
	cmpw	$-2, %ax
	setne	%al
	movzbl	%al, %eax
	leal	10(%rax,%rax), %eax
	movl	%eax, (%r15)
	movq	8(%rbx), %rax
	movb	%r12b, 64(%rax)
	testb	%r12b, %r12b
	jle	.L104
.L123:
	movq	8(%rbx), %rdi
	movsbq	%r12b, %r12
	leaq	65(%rdi), %rdx
	cmpl	$8, %r12d
	jnb	.L99
	testb	$4, %r12b
	jne	.L126
	testl	%r12d, %r12d
	je	.L104
	movzbl	0(%r13), %eax
	movb	%al, 65(%rdi)
	testb	$2, %r12b
	je	.L104
	movl	%r12d, %eax
	movzwl	-2(%r13,%rax), %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$0, (%r15)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L121:
	movq	8(%rbx), %rax
	movb	%r12b, 64(%rax)
	movq	8(%rbx), %rdi
	movsbq	%r12b, %r12
	leaq	65(%rdi), %rcx
	cmpq	$8, %r12
	jnb	.L89
	testb	$4, %r12b
	jne	.L127
	testq	%r12, %r12
	je	.L90
	movzbl	0(%r13), %eax
	movb	%al, 65(%rdi)
	testb	$2, %r12b
	je	.L90
	movzwl	-2(%r13,%r12), %eax
	movw	%ax, -2(%rcx,%r12)
	.p2align 4,,10
	.p2align 3
.L90:
	movq	24(%rbx), %rax
	movq	%rax, 16(%rbx)
	movl	$0, (%r15)
	jmp	.L82
.L99:
	movq	0(%r13), %rax
	addq	$73, %rdi
	movq	%r13, %rsi
	movq	%rax, -8(%rdi)
	movl	%r12d, %eax
	andq	$-8, %rdi
	movq	-8(%r13,%rax), %rcx
	movq	%rcx, -8(%rdx,%rax)
	subq	%rdi, %rdx
	addl	%edx, %r12d
	subq	%rdx, %rsi
	shrl	$3, %r12d
	movl	%r12d, %ecx
	rep movsq
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$15, (%r15)
	jmp	.L82
.L89:
	movq	0(%r13), %rax
	addq	$73, %rdi
	movq	%r13, %rsi
	movq	%rax, -8(%rdi)
	movq	-8(%r13,%r12), %rax
	andq	$-8, %rdi
	movq	%rax, -8(%rcx,%r12)
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%r12, %rcx
	shrq	$3, %rcx
	rep movsq
	jmp	.L90
.L83:
	movq	8(%rdi), %rax
	movb	$0, 64(%rax)
	jmp	.L104
.L126:
	movl	0(%r13), %eax
	movl	%eax, 65(%rdi)
	movl	%r12d, %eax
	movl	-4(%r13,%rax), %ecx
	movl	%ecx, -4(%rdx,%rax)
	jmp	.L104
.L127:
	movl	0(%r13), %eax
	movl	%eax, 65(%rdi)
	movl	-4(%r13,%r12), %eax
	movl	%eax, -4(%rcx,%r12)
	jmp	.L90
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2123:
	.size	_LMBCSToUnicodeWithOffsets, .-_LMBCSToUnicodeWithOffsets
	.p2align 4
	.type	_LMBCSClose, @function
_LMBCSClose:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L128
	movq	%rdi, %r13
	leaq	160(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L132:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L131
	call	ucnv_unloadSharedDataIfReady_67@PLT
.L131:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L132
	cmpb	$0, 62(%r13)
	je	.L140
.L128:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	16(%r13), %rdi
	call	uprv_free_67@PLT
	movq	$0, 16(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2116:
	.size	_LMBCSClose, .-_LMBCSClose
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ar"
	.text
	.p2align 4
	.type	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh, @function
_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh:
.LFB2115:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$328, %rsp
	movq	%rdi, -344(%rbp)
	movl	$168, %edi
	movq	%rsi, -352(%rbp)
	movl	%ecx, -364(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uprv_malloc_67@PLT
	movq	%rax, -360(%rbp)
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L142
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rbx
	andq	$-8, %rdi
	movaps	%xmm0, -336(%rbp)
	leaq	-336(%rbp), %r13
	subq	%rdi, %rcx
	movaps	%xmm0, -320(%rbp)
	movl	$40, -336(%rbp)
	addl	$168, %ecx
	movq	$0, -304(%rbp)
	shrl	$3, %ecx
	movq	$0, (%rax)
	movq	$0, 160(%rax)
	xorl	%eax, %eax
	rep stosq
	movzbl	8(%r15), %eax
	leaq	_ZL20OptGroupByteToCPName(%rip), %r15
	leaq	160(%r15), %r12
	movb	%al, -328(%rbp)
	movl	(%r14), %eax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L175:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L145
	leaq	-288(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	ucnv_loadSharedData_67@PLT
	movq	%rax, (%rbx)
	movl	(%r14), %eax
.L145:
	addq	$8, %r15
	addq	$8, %rbx
	cmpq	%r12, %r15
	je	.L174
.L147:
	testl	%eax, %eax
	jle	.L175
.L143:
	movq	-344(%rbp), %rax
	movq	16(%rax), %rbx
	leaq	160(%rbx), %r12
	testq	%rbx, %rbx
	je	.L141
	.p2align 4,,10
	.p2align 3
.L157:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L156
	call	ucnv_unloadSharedDataIfReady_67@PLT
.L156:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L157
	movq	-344(%rbp), %rax
	cmpb	$0, 62(%rax)
	je	.L176
.L141:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	testl	%eax, %eax
	jg	.L143
	movq	-352(%rbp), %rax
	cmpb	$0, 8(%rax)
	jne	.L143
	movq	-360(%rbp), %rdx
	movzbl	-364(%rbp), %esi
	movq	32(%rax), %r14
	movb	%sil, 160(%rdx)
	testq	%r14, %r14
	je	.L160
	movzbl	(%r14), %r15d
	xorl	%eax, %eax
	testb	%r15b, %r15b
	je	.L149
	movl	$97, %eax
	leaq	.LC0(%rip), %r12
	leaq	_ZL17LocaleLMBCSGrpMap(%rip), %r13
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L150:
	jl	.L163
.L151:
	movq	16(%r13), %r12
	addq	$16, %r13
	testq	%r12, %r12
	je	.L163
	movzbl	(%r12), %eax
.L152:
	cmpb	%al, %r15b
	jne	.L150
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L151
	movzbl	8(%r13), %eax
	.p2align 4,,10
	.p2align 3
.L149:
	movq	-360(%rbp), %rdx
	movb	%al, 161(%rdx)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L176:
	movq	16(%rax), %rdi
	movq	%rax, %rbx
	call	uprv_free_67@PLT
	movq	$0, 16(%rbx)
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L163:
	movq	-360(%rbp), %rdx
	movl	$1, %eax
	movb	%al, 161(%rdx)
	jmp	.L141
.L160:
	xorl	%eax, %eax
	jmp	.L149
.L142:
	movl	$7, (%r14)
	jmp	.L141
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2115:
	.size	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh, .-_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.p2align 4
	.type	_LMBCSOpen1, @function
_LMBCSOpen1:
.LFB2124:
	.cfi_startproc
	endbr64
	movl	$1, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2124:
	.size	_LMBCSOpen1, .-_LMBCSOpen1
	.p2align 4
	.type	_LMBCSOpen2, @function
_LMBCSOpen2:
.LFB2125:
	.cfi_startproc
	endbr64
	movl	$2, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2125:
	.size	_LMBCSOpen2, .-_LMBCSOpen2
	.p2align 4
	.type	_LMBCSOpen3, @function
_LMBCSOpen3:
.LFB2126:
	.cfi_startproc
	endbr64
	movl	$3, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2126:
	.size	_LMBCSOpen3, .-_LMBCSOpen3
	.p2align 4
	.type	_LMBCSOpen4, @function
_LMBCSOpen4:
.LFB2127:
	.cfi_startproc
	endbr64
	movl	$4, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2127:
	.size	_LMBCSOpen4, .-_LMBCSOpen4
	.p2align 4
	.type	_LMBCSOpen5, @function
_LMBCSOpen5:
.LFB2128:
	.cfi_startproc
	endbr64
	movl	$5, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2128:
	.size	_LMBCSOpen5, .-_LMBCSOpen5
	.p2align 4
	.type	_LMBCSOpen6, @function
_LMBCSOpen6:
.LFB2129:
	.cfi_startproc
	endbr64
	movl	$6, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2129:
	.size	_LMBCSOpen6, .-_LMBCSOpen6
	.p2align 4
	.type	_LMBCSOpen8, @function
_LMBCSOpen8:
.LFB2130:
	.cfi_startproc
	endbr64
	movl	$8, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2130:
	.size	_LMBCSOpen8, .-_LMBCSOpen8
	.p2align 4
	.type	_LMBCSOpen11, @function
_LMBCSOpen11:
.LFB2131:
	.cfi_startproc
	endbr64
	movl	$11, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2131:
	.size	_LMBCSOpen11, .-_LMBCSOpen11
	.p2align 4
	.type	_LMBCSOpen16, @function
_LMBCSOpen16:
.LFB2132:
	.cfi_startproc
	endbr64
	movl	$16, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2132:
	.size	_LMBCSOpen16, .-_LMBCSOpen16
	.p2align 4
	.type	_LMBCSOpen17, @function
_LMBCSOpen17:
.LFB2133:
	.cfi_startproc
	endbr64
	movl	$17, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2133:
	.size	_LMBCSOpen17, .-_LMBCSOpen17
	.p2align 4
	.type	_LMBCSOpen18, @function
_LMBCSOpen18:
.LFB2134:
	.cfi_startproc
	endbr64
	movl	$18, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2134:
	.size	_LMBCSOpen18, .-_LMBCSOpen18
	.p2align 4
	.type	_LMBCSOpen19, @function
_LMBCSOpen19:
.LFB2135:
	.cfi_startproc
	endbr64
	movl	$19, %ecx
	jmp	_ZL16_LMBCSOpenWorkerP10UConverterP18UConverterLoadArgsP10UErrorCodeh
	.cfi_endproc
.LFE2135:
	.size	_LMBCSOpen19, .-_LMBCSOpen19
	.p2align 4
	.type	_LMBCSFromUnicode, @function
_LMBCSFromUnicode:
.LFB2120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movb	$0, -91(%rbp)
	movq	16(%rax), %r10
	cmpq	%rdx, 24(%rdi)
	jbe	.L190
	movq	%r10, %r14
	movq	%rdi, %r12
	xorl	%r13d, %r13d
	movq	%rsi, %r10
	.p2align 4,,10
	.p2align 3
.L191:
	movl	(%r10), %eax
	testl	%eax, %eax
	jg	.L190
	movq	32(%r12), %rsi
	movzbl	161(%r14), %r15d
	movq	%rsi, %rax
	cmpq	40(%r12), %rsi
	jnb	.L445
	movzwl	(%rdx), %edx
	movl	%edx, %ebx
	leal	-128(%rdx), %r8d
	movl	%edx, %ecx
	movw	%dx, -90(%rbp)
	rolw	$8, %bx
	cmpw	$127, %r8w
	setbe	%r11b
	andl	$-33, %ecx
	cmpw	$215, %cx
	leal	-167(%rdx), %ecx
	setne	%r9b
	cmpw	$15, %cx
	ja	.L195
	movl	$42499, %edi
	shrq	%cl, %rdi
	notq	%rdi
	testb	%dil, %r9b
	jne	.L446
.L198:
	cmpw	$31, %dx
	jbe	.L203
.L200:
	leaq	_ZL14UniLMBCSGrpMap(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L205:
	addq	$6, %rcx
	cmpw	2(%rcx), %dx
	ja	.L205
	cmpw	(%rcx), %dx
	jnb	.L447
.L206:
	movb	$20, -83(%rbp)
	movzbl	%dh, %ecx
	testb	%dl, %dl
	je	.L299
	addq	$2, 16(%r12)
	movl	$3, %edx
	movw	%bx, -82(%rbp)
	leaq	-83(%rbp), %rbx
.L209:
	leal	-1(%rdx), %edi
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L195:
	testb	%r9b, %r9b
	je	.L199
	testb	%r11b, %r11b
	jne	.L196
.L199:
	leal	-32(%rdx), %ecx
	cmpw	$25, %dx
	ja	.L201
	movl	$33564161, %edi
	btq	%rdx, %rdi
	jc	.L202
	cmpw	$95, %cx
	ja	.L203
.L202:
	addq	$2, 16(%r12)
	xorl	%edi, %edi
	leaq	-83(%rbp), %rbx
	movb	%dl, -83(%rbp)
.L204:
	movl	%edi, %edx
	addl	%ebx, %edi
	leaq	1(%rbx,%rdx), %r8
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L448:
	cmpq	%r8, %rbx
	je	.L438
.L278:
	movl	%edi, %edx
	leaq	1(%rax), %rsi
	subl	%ebx, %edx
	movzbl	(%rbx), %ecx
	movq	%rsi, 32(%r12)
	addq	$1, %rbx
	movb	%cl, (%rax)
	movq	48(%r12), %rax
	testq	%rax, %rax
	je	.L282
	leaq	4(%rax), %rcx
	movq	%rcx, 48(%r12)
	movl	%r13d, (%rax)
.L282:
	movq	32(%r12), %rax
	cmpq	40(%r12), %rax
	jb	.L448
.L277:
	testl	%edx, %edx
	jle	.L438
	movq	8(%r12), %rsi
	leaq	16(%rbx), %rax
	leal	-1(%rdx), %edi
	movl	$15, (%r10)
	leaq	104(%rsi), %r8
	movb	%dl, 91(%rsi)
	cmpq	%rax, %r8
	leaq	120(%rsi), %rax
	setnb	%cl
	cmpq	%rax, %rbx
	setnb	%al
	orb	%al, %cl
	je	.L283
	cmpl	$14, %edi
	jbe	.L283
	movl	%edx, %ecx
	xorl	%eax, %eax
	shrl	$4, %ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L284:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, 104(%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L284
	movl	%edx, %esi
	andl	$-16, %esi
	movl	%esi, %eax
	subl	%esi, %edi
	addq	%rax, %rbx
	addq	%r8, %rax
	cmpl	%edx, %esi
	je	.L438
	movzbl	(%rbx), %edx
	movb	%dl, (%rax)
	testl	%edi, %edi
	je	.L438
	movzbl	1(%rbx), %edx
	movb	%dl, 1(%rax)
	cmpl	$1, %edi
	je	.L438
	movzbl	2(%rbx), %edx
	movb	%dl, 2(%rax)
	cmpl	$2, %edi
	je	.L438
	movzbl	3(%rbx), %edx
	movb	%dl, 3(%rax)
	cmpl	$3, %edi
	je	.L438
	movzbl	4(%rbx), %edx
	movb	%dl, 4(%rax)
	cmpl	$4, %edi
	je	.L438
	movzbl	5(%rbx), %edx
	movb	%dl, 5(%rax)
	cmpl	$5, %edi
	je	.L438
	movzbl	6(%rbx), %edx
	movb	%dl, 6(%rax)
	cmpl	$6, %edi
	je	.L438
	movzbl	7(%rbx), %edx
	movb	%dl, 7(%rax)
	cmpl	$7, %edi
	je	.L438
	movzbl	8(%rbx), %edx
	movb	%dl, 8(%rax)
	cmpl	$8, %edi
	je	.L438
	movzbl	9(%rbx), %edx
	movb	%dl, 9(%rax)
	cmpl	$9, %edi
	je	.L438
	movzbl	10(%rbx), %edx
	movb	%dl, 10(%rax)
	cmpl	$10, %edi
	je	.L438
	movzbl	11(%rbx), %edx
	movb	%dl, 11(%rax)
	cmpl	$11, %edi
	je	.L438
	movzbl	12(%rbx), %edx
	movb	%dl, 12(%rax)
	cmpl	$12, %edi
	je	.L438
	movzbl	13(%rbx), %edx
	movb	%dl, 13(%rax)
	cmpl	$13, %edi
	je	.L438
	movzbl	14(%rbx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L438:
	movq	16(%r12), %rdx
	addl	$1, %r13d
	movb	%r15b, 161(%r14)
	cmpq	%rdx, 24(%r12)
	ja	.L191
.L190:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L449
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L201:
	.cfi_restore_state
	cmpw	$95, %cx
	jbe	.L202
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L446:
	testb	%r11b, %r11b
	je	.L198
.L196:
	movb	$1, 161(%r14)
	jmp	.L200
.L457:
	movq	%rdx, %rbx
	movq	-112(%rbp), %r12
	movl	%esi, %edx
	testl	%esi, %esi
	jne	.L266
	cmpb	$0, -104(%rbp)
	je	.L266
	movzwl	-90(%rbp), %esi
	movq	(%r14), %rdi
	xorl	%ecx, %ecx
	leaq	-88(%rbp), %rdx
	movq	%r10, -104(%rbp)
	call	ucnv_MBCSFromUChar32_67@PLT
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	jle	.L267
	movl	-88(%rbp), %edx
	leal	-8(,%rax,8), %ecx
	movb	$0, -91(%rbp)
	movl	%edx, %edi
	shrl	%cl, %edi
	cmpb	$31, %dil
	ja	.L268
	cmpl	$1, %eax
	jne	.L268
.L444:
	movq	40(%r12), %rcx
	movq	32(%r12), %rax
.L269:
	movzwl	-90(%rbp), %edx
	movb	$20, -83(%rbp)
	movl	%edx, %esi
	movzbl	%dh, %edx
	movl	%edx, %edi
	testb	%sil, %sil
	jne	.L275
	movl	%edx, %esi
	movl	$-10, %edi
.L275:
	movb	%dil, -82(%rbp)
	movl	$2, %edi
	addq	$2, 16(%r12)
	movb	%sil, -81(%rbp)
	cmpq	%rcx, %rax
	jb	.L204
	movq	8(%r12), %rsi
	movl	$15, (%r10)
	movl	$2, %edi
	movb	$3, 91(%rsi)
.L283:
	movl	%edi, %ecx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L286:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, 104(%rsi,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L286
	jmp	.L438
.L445:
	movl	$15, (%r10)
	jmp	.L190
.L447:
	movzbl	4(%rcx), %r11d
	cmpb	$20, %r11b
	je	.L206
	cmpb	$15, %r11b
	je	.L450
	cmpb	$19, %r11b
	jbe	.L214
	movzbl	160(%r14), %edx
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r9
	movl	$0, -64(%rbp)
	movzbl	161(%r14), %eax
	movaps	%xmm0, -80(%rbp)
	cmpb	$1, %dl
	je	.L443
	cmpb	$-128, %r11b
	jne	.L451
	cmpb	$15, %dl
	jbe	.L220
	leaq	-83(%rbp), %rbx
	testb	%al, %al
	je	.L452
.L221:
	cmpb	$15, %al
	ja	.L453
	movl	$-128, %r11d
.L251:
	movq	%rbx, %rdx
	leaq	-90(%rbp), %rcx
	movzbl	%al, %esi
	movq	%r14, %rdi
	leaq	-91(%rbp), %r8
	movq	%r10, -120(%rbp)
	movb	%r11b, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	LMBCSConversionWorker
	movq	-104(%rbp), %r9
	movzbl	-112(%rbp), %r11d
	testl	%eax, %eax
	movq	-120(%rbp), %r10
	movl	%eax, %edx
	jne	.L300
.L249:
	movzbl	-91(%rbp), %eax
	testb	%al, %al
	je	.L293
	cmpb	$-128, %r11b
	jne	.L256
.L290:
	cmpb	$15, %al
	ja	.L291
	movq	%rbx, %rdx
	leaq	-90(%rbp), %rcx
	movzbl	%al, %esi
	movq	%r14, %rdi
	leaq	-91(%rbp), %r8
	movq	%r10, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	LMBCSConversionWorker
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %edx
	je	.L291
.L300:
	movq	40(%r12), %rcx
	movq	32(%r12), %rax
.L236:
	addq	$2, 16(%r12)
	cmpq	%rax, %rcx
	ja	.L209
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L440:
	movb	$20, -83(%rbp)
	xorl	%ecx, %ecx
.L299:
	addq	$2, 16(%r12)
	movl	$2, %edi
	leaq	-83(%rbp), %rbx
	movb	$-10, -82(%rbp)
	movb	%cl, -81(%rbp)
	jmp	.L204
.L214:
	movzbl	%r11b, %eax
	leaq	-83(%rbp), %rbx
	leaq	-80(%rbp), %r9
	movq	%r14, %rdi
	movq	%rbx, %rdx
	leaq	-90(%rbp), %rcx
	leaq	-91(%rbp), %r8
	movl	%eax, %esi
	movq	%r10, -120(%rbp)
	movb	%r11b, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	LMBCSConversionWorker
	movq	-104(%rbp), %r9
	movzbl	-112(%rbp), %r11d
	testl	%eax, %eax
	movq	-120(%rbp), %r10
	movl	%eax, %edx
	jne	.L454
.L213:
	pxor	%xmm0, %xmm0
	cmpb	$1, 160(%r14)
	movl	$0, 16(%r9)
	movaps	%xmm0, (%r9)
	je	.L255
	movzbl	161(%r14), %eax
	testb	%al, %al
	je	.L249
.L250:
	cmpb	$-127, %r11b
	jne	.L253
.L295:
	cmpb	$15, %al
	jbe	.L455
	movl	$-127, %r11d
	jmp	.L251
.L203:
	testw	%dx, %dx
	je	.L440
	movb	$15, -83(%rbp)
	addl	$32, %edx
	leaq	-83(%rbp), %rbx
	movb	%dl, -82(%rbp)
	movl	$2, %edx
.L212:
	movq	40(%r12), %rcx
	movq	%rsi, %rax
	jmp	.L236
.L458:
	cmpb	$-126, %r11b
	jne	.L443
.L220:
	cmpb	$15, %al
	ja	.L224
	leaq	-88(%rbp), %r8
	movzwl	-90(%rbp), %esi
	movq	8(%r14), %rdi
	xorl	%ecx, %ecx
	movq	%r8, %rdx
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	movb	%r11b, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	ucnv_MBCSFromUChar32_67@PLT
	movq	-104(%rbp), %r8
	movzbl	-112(%rbp), %r11d
	testl	%eax, %eax
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r10
	movl	%eax, %esi
	jle	.L225
	movl	-88(%rbp), %edi
	leal	-8(,%rax,8), %ecx
	movb	$1, -91(%rbp)
	movl	%edi, %eax
	shrl	%cl, %eax
	cmpb	$1, 160(%r14)
	movl	%eax, %ecx
	je	.L456
	movb	$1, -83(%rbp)
	movl	$1, %edx
	leaq	-82(%rbp), %rax
	leaq	-83(%rbp), %rbx
.L227:
	cmpl	$1, %esi
	jne	.L320
	cmpb	$31, %cl
	ja	.L320
.L235:
	movzwl	-90(%rbp), %esi
	movq	(%r14), %rdi
	xorl	%ecx, %ecx
	movq	%r8, %rdx
	movq	%r10, -120(%rbp)
	movq	%r9, -112(%rbp)
	movb	%r11b, -104(%rbp)
	call	ucnv_MBCSFromUChar32_67@PLT
	movzbl	-104(%rbp), %r11d
	movq	-112(%rbp), %r9
	testl	%eax, %eax
	movq	-120(%rbp), %r10
	movl	%eax, %edx
	jle	.L237
	movl	-88(%rbp), %esi
	leal	-8(,%rax,8), %ecx
	movzwl	-88(%rbp), %edi
	movb	$0, -91(%rbp)
	movl	%esi, %eax
	rolw	$8, %di
	shrl	%cl, %eax
	cmpb	$31, %al
	ja	.L240
	cmpl	$1, %edx
	je	.L246
.L240:
	cmpl	$3, %edx
	je	.L304
	jg	.L242
	cmpl	$1, %edx
	je	.L243
	cmpl	$2, %edx
	jne	.L246
	movq	%rbx, %rax
.L244:
	movw	%di, (%rax)
	addq	$2, %rax
	subq	%rbx, %rax
	movl	%eax, %edx
	testl	%eax, %eax
	jne	.L300
.L246:
	movq	%rbx, %rdx
	leaq	-90(%rbp), %rcx
	leaq	-91(%rbp), %r8
	movq	%r14, %rdi
	movzbl	161(%r14), %esi
	movq	%r10, -120(%rbp)
	movb	%r11b, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	LMBCSConversionWorker
	movq	-104(%rbp), %r9
	movzbl	-112(%rbp), %r11d
	movq	-120(%rbp), %r10
	movl	%eax, %edx
.L247:
	testl	%edx, %edx
	jne	.L300
	movzbl	161(%r14), %eax
.L216:
	testb	%al, %al
	je	.L249
	cmpb	$-128, %r11b
	je	.L221
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	-83(%rbp), %rbx
	leaq	-90(%rbp), %rcx
	movzbl	%al, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	leaq	-91(%rbp), %r8
	movq	%r10, -120(%rbp)
	movb	%r11b, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	LMBCSConversionWorker
	movq	-120(%rbp), %r10
	movzbl	-112(%rbp), %r11d
	movq	-104(%rbp), %r9
	movl	%eax, %edx
	jmp	.L247
.L450:
	cmpw	$32, %r8w
	jbe	.L431
	leaq	-80(%rbp), %r9
	leaq	-83(%rbp), %rbx
	jmp	.L213
.L253:
	cmpb	$-126, %r11b
	je	.L251
.L255:
	movzbl	-91(%rbp), %eax
	testb	%al, %al
	je	.L293
.L256:
	cmpb	$-127, %r11b
	je	.L288
	cmpb	$-126, %r11b
	jne	.L291
	movq	%rbx, %rdx
	leaq	-90(%rbp), %rcx
	movzbl	%al, %esi
	movq	%r14, %rdi
	leaq	-91(%rbp), %r8
	movq	%r10, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	LMBCSConversionWorker
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L300
	movb	$1, -104(%rbp)
	movl	$19, %r11d
	movl	$1, %eax
.L261:
	movq	%r12, -112(%rbp)
	movq	%rbx, %rdx
	movl	%r11d, %r12d
	movq	%rax, %rbx
	jmp	.L265
.L264:
	leal	1(%rbx), %ecx
	cmpb	%cl, %r12b
	setb	%cl
	addq	$1, %rbx
	orb	%al, %cl
	jne	.L457
.L265:
	xorl	%eax, %eax
	xorl	%esi, %esi
	cmpq	$0, (%r14,%rbx,8)
	je	.L264
	cmpb	$0, (%r9,%rbx)
	jne	.L264
	movl	%ebx, %esi
	leaq	-90(%rbp), %rcx
	leaq	-91(%rbp), %r8
	movq	%r14, %rdi
	movq	%r10, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	LMBCSConversionWorker
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %esi
	movq	-136(%rbp), %r10
	setne	%al
	jmp	.L264
.L268:
	cmpl	$3, %eax
	je	.L311
	jg	.L271
	cmpl	$1, %eax
	je	.L312
	movq	%rbx, %rcx
	cmpl	$2, %eax
	jne	.L274
.L273:
	movb	%dh, (%rcx)
	leaq	1(%rcx), %rdi
.L272:
	movb	%dl, (%rdi)
	leaq	1(%rdi), %rcx
.L274:
	movl	%ecx, %edx
	subl	%ebx, %edx
.L266:
	movq	40(%r12), %rcx
	movq	32(%r12), %rax
	testl	%edx, %edx
	jne	.L236
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L443:
	leaq	-83(%rbp), %rbx
	jmp	.L216
.L451:
	cmpb	$-127, %r11b
	jne	.L458
	cmpb	$15, %dl
	ja	.L220
	leaq	-83(%rbp), %rbx
	testb	%al, %al
	jne	.L295
	movzbl	-91(%rbp), %eax
	leaq	-83(%rbp), %rbx
	testb	%al, %al
	je	.L319
.L288:
	cmpb	$15, %al
	jbe	.L319
	movq	%rbx, %rdx
	leaq	-90(%rbp), %rcx
	movzbl	%al, %esi
	movq	%r14, %rdi
	leaq	-91(%rbp), %r8
	movq	%r10, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	LMBCSConversionWorker
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L300
.L319:
	movb	$0, -104(%rbp)
	movl	$19, %r11d
	movl	$16, %eax
	jmp	.L261
.L293:
	cmpb	$-127, %r11b
	je	.L319
	cmpb	$-126, %r11b
	movb	$1, -104(%rbp)
	movl	$1, %eax
	sete	%r11b
	leal	11(,%r11,8), %r11d
	jmp	.L261
.L452:
	movzbl	-91(%rbp), %eax
	testb	%al, %al
	jne	.L290
	leaq	-83(%rbp), %rbx
.L291:
	movb	$1, -104(%rbp)
	movl	$11, %r11d
	movl	$1, %eax
	jmp	.L261
.L320:
	cmpl	$3, %esi
	je	.L230
	jg	.L231
	cmpl	$1, %esi
	je	.L232
	cmpl	$2, %esi
	jne	.L234
.L233:
	movl	%edi, %edx
	addq	$1, %rax
	movb	%dh, -1(%rax)
.L232:
	movb	%dil, (%rax)
	addq	$1, %rax
	movl	%eax, %edx
	subl	%ebx, %edx
.L234:
	testl	%edx, %edx
	jne	.L300
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L231:
	cmpl	$4, %esi
	jne	.L234
	movl	%edi, %edx
	addq	$1, %rax
	shrl	$24, %edx
	movb	%dl, -1(%rax)
.L230:
	movl	%edi, %edx
	addq	$1, %rax
	shrl	$16, %edx
	movb	%dl, -1(%rax)
	jmp	.L233
.L431:
	movb	%dl, -82(%rbp)
	movq	32(%r12), %rsi
	movl	$2, %edx
	leaq	-83(%rbp), %rbx
	movb	$15, -83(%rbp)
	jmp	.L212
.L225:
	movb	$1, -79(%rbp)
	leaq	-83(%rbp), %rbx
	jmp	.L235
.L237:
	movb	$1, -80(%rbp)
	jmp	.L246
.L455:
	movzbl	-91(%rbp), %eax
	testb	%al, %al
	jne	.L288
	jmp	.L319
.L454:
	movq	32(%r12), %rsi
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L456:
	leaq	-83(%rbp), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rax
	jmp	.L227
.L243:
	movb	%sil, -83(%rbp)
	jmp	.L300
.L449:
	call	__stack_chk_fail@PLT
.L242:
	cmpl	$4, %edx
	jne	.L246
	movl	%esi, %eax
	leaq	-82(%rbp), %rdx
	shrl	$24, %eax
	movb	%al, -83(%rbp)
.L241:
	shrl	$16, %esi
	leaq	1(%rdx), %rax
	movb	%sil, (%rdx)
	jmp	.L244
.L304:
	movq	%rbx, %rdx
	jmp	.L241
.L312:
	movq	%rbx, %rdi
	jmp	.L272
.L271:
	movq	%rbx, %rcx
	cmpl	$4, %eax
	jne	.L274
	movl	%edx, %eax
	shrl	$24, %eax
	movb	%al, -83(%rbp)
	leaq	-82(%rbp), %rax
.L270:
	movl	%edx, %esi
	leaq	1(%rax), %rcx
	shrl	$16, %esi
	movb	%sil, (%rax)
	jmp	.L273
.L311:
	movq	%rbx, %rax
	jmp	.L270
.L267:
	movb	$1, -80(%rbp)
	jmp	.L444
.L453:
	movzbl	-91(%rbp), %eax
	testb	%al, %al
	jne	.L290
	jmp	.L291
	.cfi_endproc
.LFE2120:
	.size	_LMBCSFromUnicode, .-_LMBCSFromUnicode
	.globl	_LMBCSData19_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_LMBCSData19_67, @object
	.size	_LMBCSData19_67, 296
_LMBCSData19_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_LMBCSStaticData19
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_LMBCSImpl19
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_LMBCSStaticData19, @object
	.size	_ZL18_LMBCSStaticData19, 100
_ZL18_LMBCSStaticData19:
	.long	100
	.string	"LMBCS-19"
	.zero	51
	.long	0
	.byte	0
	.byte	22
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL12_LMBCSImpl19, @object
	.size	_ZL12_LMBCSImpl19, 144
_ZL12_LMBCSImpl19:
	.long	22
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen19
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData18_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData18_67, @object
	.size	_LMBCSData18_67, 296
_LMBCSData18_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_LMBCSStaticData18
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_LMBCSImpl18
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_LMBCSStaticData18, @object
	.size	_ZL18_LMBCSStaticData18, 100
_ZL18_LMBCSStaticData18:
	.long	100
	.string	"LMBCS-18"
	.zero	51
	.long	0
	.byte	0
	.byte	21
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL12_LMBCSImpl18, @object
	.size	_ZL12_LMBCSImpl18, 144
_ZL12_LMBCSImpl18:
	.long	21
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen18
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData17_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData17_67, @object
	.size	_LMBCSData17_67, 296
_LMBCSData17_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_LMBCSStaticData17
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_LMBCSImpl17
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_LMBCSStaticData17, @object
	.size	_ZL18_LMBCSStaticData17, 100
_ZL18_LMBCSStaticData17:
	.long	100
	.string	"LMBCS-17"
	.zero	51
	.long	0
	.byte	0
	.byte	20
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL12_LMBCSImpl17, @object
	.size	_ZL12_LMBCSImpl17, 144
_ZL12_LMBCSImpl17:
	.long	20
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen17
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData16_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData16_67, @object
	.size	_LMBCSData16_67, 296
_LMBCSData16_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_LMBCSStaticData16
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_LMBCSImpl16
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_LMBCSStaticData16, @object
	.size	_ZL18_LMBCSStaticData16, 100
_ZL18_LMBCSStaticData16:
	.long	100
	.string	"LMBCS-16"
	.zero	51
	.long	0
	.byte	0
	.byte	19
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL12_LMBCSImpl16, @object
	.size	_ZL12_LMBCSImpl16, 144
_ZL12_LMBCSImpl16:
	.long	19
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen16
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData11_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData11_67, @object
	.size	_LMBCSData11_67, 296
_LMBCSData11_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_LMBCSStaticData11
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_LMBCSImpl11
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_LMBCSStaticData11, @object
	.size	_ZL18_LMBCSStaticData11, 100
_ZL18_LMBCSStaticData11:
	.long	100
	.string	"LMBCS-11"
	.zero	51
	.long	0
	.byte	0
	.byte	18
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL12_LMBCSImpl11, @object
	.size	_ZL12_LMBCSImpl11, 144
_ZL12_LMBCSImpl11:
	.long	18
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen11
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData8_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData8_67, @object
	.size	_LMBCSData8_67, 296
_LMBCSData8_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL17_LMBCSStaticData8
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL11_LMBCSImpl8
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL17_LMBCSStaticData8, @object
	.size	_ZL17_LMBCSStaticData8, 100
_ZL17_LMBCSStaticData8:
	.long	100
	.string	"LMBCS-8"
	.zero	52
	.long	0
	.byte	0
	.byte	17
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL11_LMBCSImpl8, @object
	.size	_ZL11_LMBCSImpl8, 144
_ZL11_LMBCSImpl8:
	.long	17
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen8
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData6_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData6_67, @object
	.size	_LMBCSData6_67, 296
_LMBCSData6_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL17_LMBCSStaticData6
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL11_LMBCSImpl6
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL17_LMBCSStaticData6, @object
	.size	_ZL17_LMBCSStaticData6, 100
_ZL17_LMBCSStaticData6:
	.long	100
	.string	"LMBCS-6"
	.zero	52
	.long	0
	.byte	0
	.byte	16
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL11_LMBCSImpl6, @object
	.size	_ZL11_LMBCSImpl6, 144
_ZL11_LMBCSImpl6:
	.long	16
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen6
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData5_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData5_67, @object
	.size	_LMBCSData5_67, 296
_LMBCSData5_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL17_LMBCSStaticData5
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL11_LMBCSImpl5
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL17_LMBCSStaticData5, @object
	.size	_ZL17_LMBCSStaticData5, 100
_ZL17_LMBCSStaticData5:
	.long	100
	.string	"LMBCS-5"
	.zero	52
	.long	0
	.byte	0
	.byte	15
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL11_LMBCSImpl5, @object
	.size	_ZL11_LMBCSImpl5, 144
_ZL11_LMBCSImpl5:
	.long	15
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen5
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData4_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData4_67, @object
	.size	_LMBCSData4_67, 296
_LMBCSData4_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL17_LMBCSStaticData4
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL11_LMBCSImpl4
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL17_LMBCSStaticData4, @object
	.size	_ZL17_LMBCSStaticData4, 100
_ZL17_LMBCSStaticData4:
	.long	100
	.string	"LMBCS-4"
	.zero	52
	.long	0
	.byte	0
	.byte	14
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL11_LMBCSImpl4, @object
	.size	_ZL11_LMBCSImpl4, 144
_ZL11_LMBCSImpl4:
	.long	14
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen4
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData3_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData3_67, @object
	.size	_LMBCSData3_67, 296
_LMBCSData3_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL17_LMBCSStaticData3
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL11_LMBCSImpl3
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL17_LMBCSStaticData3, @object
	.size	_ZL17_LMBCSStaticData3, 100
_ZL17_LMBCSStaticData3:
	.long	100
	.string	"LMBCS-3"
	.zero	52
	.long	0
	.byte	0
	.byte	13
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL11_LMBCSImpl3, @object
	.size	_ZL11_LMBCSImpl3, 144
_ZL11_LMBCSImpl3:
	.long	13
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen3
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData2_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData2_67, @object
	.size	_LMBCSData2_67, 296
_LMBCSData2_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL17_LMBCSStaticData2
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL11_LMBCSImpl2
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL17_LMBCSStaticData2, @object
	.size	_ZL17_LMBCSStaticData2, 100
_ZL17_LMBCSStaticData2:
	.long	100
	.string	"LMBCS-2"
	.zero	52
	.long	0
	.byte	0
	.byte	12
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL11_LMBCSImpl2, @object
	.size	_ZL11_LMBCSImpl2, 144
_ZL11_LMBCSImpl2:
	.long	12
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen2
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_LMBCSData1_67
	.section	.data.rel.ro.local
	.align 32
	.type	_LMBCSData1_67, @object
	.size	_LMBCSData1_67, 296
_LMBCSData1_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL17_LMBCSStaticData1
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL11_LMBCSImpl1
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL17_LMBCSStaticData1, @object
	.size	_ZL17_LMBCSStaticData1, 100
_ZL17_LMBCSStaticData1:
	.long	100
	.string	"LMBCS-1"
	.zero	52
	.long	0
	.byte	0
	.byte	11
	.byte	1
	.byte	3
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL11_LMBCSImpl1, @object
	.size	_ZL11_LMBCSImpl1, 144
_ZL11_LMBCSImpl1:
	.long	11
	.zero	4
	.quad	0
	.quad	0
	.quad	_LMBCSOpen1
	.quad	_LMBCSClose
	.quad	0
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSToUnicodeWithOffsets
	.quad	_LMBCSFromUnicode
	.quad	_LMBCSFromUnicode
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_LMBCSSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC1:
	.string	"be"
.LC2:
	.string	"bg"
.LC3:
	.string	"cs"
.LC4:
	.string	"el"
.LC5:
	.string	"he"
.LC6:
	.string	"hu"
.LC7:
	.string	"iw"
.LC8:
	.string	"ja"
.LC9:
	.string	"ko"
.LC10:
	.string	"mk"
.LC11:
	.string	"pl"
.LC12:
	.string	"ro"
.LC13:
	.string	"ru"
.LC14:
	.string	"sh"
.LC15:
	.string	"sk"
.LC16:
	.string	"sl"
.LC17:
	.string	"sq"
.LC18:
	.string	"sr"
.LC19:
	.string	"th"
.LC20:
	.string	"tr"
.LC21:
	.string	"uk"
.LC22:
	.string	"zhTW"
.LC23:
	.string	"zh"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL17LocaleLMBCSGrpMap, @object
	.size	_ZL17LocaleLMBCSGrpMap, 400
_ZL17LocaleLMBCSGrpMap:
	.quad	.LC0
	.byte	4
	.zero	7
	.quad	.LC1
	.byte	5
	.zero	7
	.quad	.LC2
	.byte	6
	.zero	7
	.quad	.LC3
	.byte	6
	.zero	7
	.quad	.LC4
	.byte	2
	.zero	7
	.quad	.LC5
	.byte	3
	.zero	7
	.quad	.LC6
	.byte	6
	.zero	7
	.quad	.LC7
	.byte	3
	.zero	7
	.quad	.LC8
	.byte	16
	.zero	7
	.quad	.LC9
	.byte	17
	.zero	7
	.quad	.LC10
	.byte	5
	.zero	7
	.quad	.LC11
	.byte	6
	.zero	7
	.quad	.LC12
	.byte	6
	.zero	7
	.quad	.LC13
	.byte	5
	.zero	7
	.quad	.LC14
	.byte	6
	.zero	7
	.quad	.LC15
	.byte	6
	.zero	7
	.quad	.LC16
	.byte	6
	.zero	7
	.quad	.LC17
	.byte	6
	.zero	7
	.quad	.LC18
	.byte	5
	.zero	7
	.quad	.LC19
	.byte	11
	.zero	7
	.quad	.LC20
	.byte	8
	.zero	7
	.quad	.LC21
	.byte	5
	.zero	7
	.quad	.LC22
	.byte	18
	.zero	7
	.quad	.LC23
	.byte	19
	.zero	7
	.quad	0
	.byte	1
	.zero	7
	.section	.rodata
	.align 32
	.type	_ZL14UniLMBCSGrpMap, @object
	.size	_ZL14UniLMBCSGrpMap, 816
_ZL14UniLMBCSGrpMap:
	.value	1
	.value	31
	.byte	15
	.zero	1
	.value	128
	.value	159
	.byte	15
	.zero	1
	.value	160
	.value	166
	.byte	-128
	.zero	1
	.value	167
	.value	168
	.byte	-126
	.zero	1
	.value	169
	.value	175
	.byte	-128
	.zero	1
	.value	176
	.value	177
	.byte	-126
	.zero	1
	.value	178
	.value	179
	.byte	-128
	.zero	1
	.value	180
	.value	180
	.byte	-126
	.zero	1
	.value	181
	.value	181
	.byte	-128
	.zero	1
	.value	182
	.value	182
	.byte	-126
	.zero	1
	.value	183
	.value	214
	.byte	-128
	.zero	1
	.value	215
	.value	215
	.byte	-126
	.zero	1
	.value	216
	.value	246
	.byte	-128
	.zero	1
	.value	247
	.value	247
	.byte	-126
	.zero	1
	.value	248
	.value	461
	.byte	-128
	.zero	1
	.value	462
	.value	462
	.byte	18
	.zero	1
	.value	463
	.value	697
	.byte	-128
	.zero	1
	.value	698
	.value	698
	.byte	19
	.zero	1
	.value	700
	.value	712
	.byte	-128
	.zero	1
	.value	713
	.value	720
	.byte	-127
	.zero	1
	.value	728
	.value	733
	.byte	-128
	.zero	1
	.value	900
	.value	912
	.byte	-128
	.zero	1
	.value	913
	.value	937
	.byte	-126
	.zero	1
	.value	938
	.value	944
	.byte	-128
	.zero	1
	.value	945
	.value	969
	.byte	-126
	.zero	1
	.value	970
	.value	974
	.byte	-128
	.zero	1
	.value	1024
	.value	1024
	.byte	5
	.zero	1
	.value	1025
	.value	1025
	.byte	-126
	.zero	1
	.value	1026
	.value	1039
	.byte	5
	.zero	1
	.value	1040
	.value	1073
	.byte	-126
	.zero	1
	.value	1074
	.value	1102
	.byte	5
	.zero	1
	.value	1103
	.value	1103
	.byte	-126
	.zero	1
	.value	1104
	.value	1169
	.byte	5
	.zero	1
	.value	1456
	.value	1522
	.byte	3
	.zero	1
	.value	1548
	.value	1711
	.byte	4
	.zero	1
	.value	3585
	.value	3675
	.byte	11
	.zero	1
	.value	8204
	.value	8207
	.byte	-128
	.zero	1
	.value	8208
	.value	8208
	.byte	-127
	.zero	1
	.value	8211
	.value	8212
	.byte	-128
	.zero	1
	.value	8213
	.value	8213
	.byte	-127
	.zero	1
	.value	8214
	.value	8214
	.byte	-127
	.zero	1
	.value	8215
	.value	8215
	.byte	-128
	.zero	1
	.value	8216
	.value	8217
	.byte	-126
	.zero	1
	.value	8218
	.value	8219
	.byte	-128
	.zero	1
	.value	8220
	.value	8221
	.byte	-126
	.zero	1
	.value	8222
	.value	8223
	.byte	-128
	.zero	1
	.value	8224
	.value	8225
	.byte	-126
	.zero	1
	.value	8226
	.value	8228
	.byte	-128
	.zero	1
	.value	8229
	.value	8229
	.byte	-127
	.zero	1
	.value	8230
	.value	8230
	.byte	-126
	.zero	1
	.value	8231
	.value	8231
	.byte	18
	.zero	1
	.value	8240
	.value	8240
	.byte	-126
	.zero	1
	.value	8241
	.value	8241
	.byte	-128
	.zero	1
	.value	8242
	.value	8243
	.byte	-127
	.zero	1
	.value	8245
	.value	8245
	.byte	-127
	.zero	1
	.value	8249
	.value	8250
	.byte	-128
	.zero	1
	.value	8251
	.value	8251
	.byte	-127
	.zero	1
	.value	8252
	.value	8252
	.byte	0
	.zero	1
	.value	8308
	.value	8308
	.byte	17
	.zero	1
	.value	8319
	.value	8319
	.byte	0
	.zero	1
	.value	8321
	.value	8324
	.byte	17
	.zero	1
	.value	8356
	.value	8364
	.byte	-128
	.zero	1
	.value	8451
	.value	8457
	.byte	-127
	.zero	1
	.value	8465
	.value	8480
	.byte	-128
	.zero	1
	.value	8481
	.value	8481
	.byte	-127
	.zero	1
	.value	8482
	.value	8486
	.byte	-128
	.zero	1
	.value	8491
	.value	8491
	.byte	-127
	.zero	1
	.value	8501
	.value	8501
	.byte	-128
	.zero	1
	.value	8531
	.value	8532
	.byte	17
	.zero	1
	.value	8539
	.value	8542
	.byte	0
	.zero	1
	.value	8544
	.value	8569
	.byte	-127
	.zero	1
	.value	8592
	.value	8595
	.byte	-126
	.zero	1
	.value	8596
	.value	8597
	.byte	0
	.zero	1
	.value	8598
	.value	8601
	.byte	-127
	.zero	1
	.value	8616
	.value	8616
	.byte	0
	.zero	1
	.value	8632
	.value	8633
	.byte	19
	.zero	1
	.value	8656
	.value	8657
	.byte	0
	.zero	1
	.value	8658
	.value	8658
	.byte	-127
	.zero	1
	.value	8659
	.value	8659
	.byte	0
	.zero	1
	.value	8660
	.value	8660
	.byte	-127
	.zero	1
	.value	8661
	.value	8661
	.byte	0
	.zero	1
	.value	8679
	.value	8679
	.byte	19
	.zero	1
	.value	8704
	.value	8704
	.byte	-127
	.zero	1
	.value	8705
	.value	8705
	.byte	0
	.zero	1
	.value	8706
	.value	8706
	.byte	-127
	.zero	1
	.value	8707
	.value	8707
	.byte	-127
	.zero	1
	.value	8708
	.value	8710
	.byte	0
	.zero	1
	.value	8711
	.value	8712
	.byte	-127
	.zero	1
	.value	8713
	.value	8714
	.byte	0
	.zero	1
	.value	8715
	.value	8715
	.byte	-127
	.zero	1
	.value	8719
	.value	8725
	.byte	-127
	.zero	1
	.value	8729
	.value	8729
	.byte	0
	.zero	1
	.value	8730
	.value	8730
	.byte	-127
	.zero	1
	.value	8731
	.value	8732
	.byte	0
	.zero	1
	.value	8733
	.value	8734
	.byte	-127
	.zero	1
	.value	8735
	.value	8735
	.byte	0
	.zero	1
	.value	8736
	.value	8736
	.byte	-127
	.zero	1
	.value	8739
	.value	8746
	.byte	-127
	.zero	1
	.value	8747
	.value	8765
	.byte	-127
	.zero	1
	.value	8773
	.value	8776
	.byte	0
	.zero	1
	.value	8780
	.value	8780
	.byte	18
	.zero	1
	.value	8786
	.value	8786
	.byte	-127
	.zero	1
	.value	8800
	.value	8801
	.byte	-127
	.zero	1
	.value	8802
	.value	8805
	.byte	0
	.zero	1
	.value	8806
	.value	8815
	.byte	-127
	.zero	1
	.value	8834
	.value	8835
	.byte	-127
	.zero	1
	.value	8836
	.value	8837
	.byte	0
	.zero	1
	.value	8838
	.value	8839
	.byte	-127
	.zero	1
	.value	8840
	.value	8855
	.byte	0
	.zero	1
	.value	8857
	.value	8895
	.byte	-127
	.zero	1
	.value	8896
	.value	8896
	.byte	0
	.zero	1
	.value	8976
	.value	8976
	.byte	0
	.zero	1
	.value	8978
	.value	8978
	.byte	-127
	.zero	1
	.value	8984
	.value	8993
	.byte	0
	.zero	1
	.value	8984
	.value	8993
	.byte	19
	.zero	1
	.value	9312
	.value	9449
	.byte	-127
	.zero	1
	.value	9472
	.value	9472
	.byte	-128
	.zero	1
	.value	9473
	.value	9473
	.byte	-127
	.zero	1
	.value	9474
	.value	9474
	.byte	-126
	.zero	1
	.value	9475
	.value	9475
	.byte	-127
	.zero	1
	.value	9476
	.value	9477
	.byte	18
	.zero	1
	.value	9478
	.value	9829
	.byte	-126
	.zero	1
	.value	9830
	.value	9830
	.byte	0
	.zero	1
	.value	9831
	.value	9833
	.byte	-128
	.zero	1
	.value	9834
	.value	9834
	.byte	-126
	.zero	1
	.value	9835
	.value	9836
	.byte	-128
	.zero	1
	.value	9837
	.value	9837
	.byte	-127
	.zero	1
	.value	9838
	.value	9838
	.byte	-128
	.zero	1
	.value	9839
	.value	9839
	.byte	16
	.zero	1
	.value	9840
	.value	11903
	.byte	-128
	.zero	1
	.value	11904
	.value	-1951
	.byte	-127
	.zero	1
	.value	-1950
	.value	-1793
	.byte	0
	.zero	1
	.value	-1792
	.value	-1491
	.byte	-127
	.zero	1
	.value	-1280
	.value	-257
	.byte	-128
	.zero	1
	.value	-255
	.value	-18
	.byte	-127
	.zero	1
	.value	-1
	.value	-1
	.byte	20
	.zero	1
	.section	.rodata.str1.1
.LC24:
	.string	"lmb-excp"
.LC25:
	.string	"ibm-850"
.LC26:
	.string	"ibm-851"
.LC27:
	.string	"windows-1255"
.LC28:
	.string	"windows-1256"
.LC29:
	.string	"windows-1251"
.LC30:
	.string	"ibm-852"
.LC31:
	.string	"windows-1254"
.LC32:
	.string	"windows-874"
.LC33:
	.string	"windows-932"
.LC34:
	.string	"windows-949"
.LC35:
	.string	"windows-950"
.LC36:
	.string	"windows-936"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL20OptGroupByteToCPName, @object
	.size	_ZL20OptGroupByteToCPName, 160
_ZL20OptGroupByteToCPName:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	0
	.quad	.LC31
	.quad	0
	.quad	0
	.quad	.LC32
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
