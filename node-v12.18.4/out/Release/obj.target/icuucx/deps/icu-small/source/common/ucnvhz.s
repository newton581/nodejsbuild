	.file	"ucnvhz.cpp"
	.text
	.p2align 4
	.type	_HZ_GetUnicodeSet, @function
_HZ_GetUnicodeSet:
.LFB2120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	movl	$127, %edx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%r12), %rdi
	movq	%rcx, %rbx
	call	*16(%r12)
	movq	16(%r14), %rax
	movq	%rbx, %r8
	movl	%r13d, %edx
	popq	%rbx
	movq	%r12, %rsi
	movl	$5, %ecx
	popq	%r12
	movq	(%rax), %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	48(%rax), %rdi
	jmp	ucnv_MBCSGetFilteredUnicodeSetForUnicode_67@PLT
	.cfi_endproc
.LFE2120:
	.size	_HZ_GetUnicodeSet, .-_HZ_GetUnicodeSet
	.p2align 4
	.type	_HZ_WriteSub, @function
_HZ_WriteSub:
.LFB2118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	16(%rdx), %rax
	cmpb	$0, 18(%rax)
	je	.L7
	movl	$32126, %esi
	movb	$0, 18(%rax)
	leaq	-10(%rbp), %rax
	movw	%si, -12(%rbp)
	leaq	-12(%rbp), %rsi
.L5:
	movq	40(%rdx), %rdx
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	leaq	1(%rax), %rdx
	subq	%rsi, %rdx
	call	ucnv_cbFromUWriteBytes_67@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	leaq	-12(%rbp), %rsi
	movq	%rsi, %rax
	jmp	.L5
.L9:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2118:
	.size	_HZ_WriteSub, .-_HZ_WriteSub
	.p2align 4
	.type	UConverter_fromUnicode_HZ_OFFSETS_LOGIC, @function
UConverter_fromUnicode_HZ_OFFSETS_LOGIC:
.LFB2117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rcx
	movq	24(%r14), %rdx
	movq	%rsi, -72(%rbp)
	movq	48(%r14), %r15
	subq	%rcx, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	sarq	%rdx
	movq	32(%rdi), %rax
	movq	%rcx, -80(%rbp)
	movq	%rdx, -136(%rbp)
	movq	8(%r14), %rdx
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	movq	%rax, %r13
	movq	40(%r14), %rax
	movq	16(%rdx), %r11
	movl	84(%rdx), %esi
	movl	$0, -60(%rbp)
	subq	%rdi, %rax
	movzbl	18(%r11), %r8d
	movl	%eax, %r9d
	testl	%esi, %esi
	je	.L64
	testl	%eax, %eax
	jg	.L61
.L64:
	movl	-136(%rbp), %edx
	testl	%edx, %edx
	jle	.L62
	movl	$65535, -60(%rbp)
	testl	%eax, %eax
	jle	.L80
	movq	-136(%rbp), %rax
	movl	$1, %ebx
	xorl	%r12d, %r12d
	subl	$1, %eax
	addq	$1, %rax
	movq	%rax, -96(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L86:
	cmpq	$0, 48(%r14)
	je	.L36
	movl	%r10d, (%r15)
	addq	$4, %r15
.L36:
	leal	1(%r12), %edx
	movslq	%edx, %r13
	cmpl	%edx, %r9d
	jle	.L81
	movq	32(%r14), %rdx
	movb	$123, (%rdx,%r13)
	cmpq	$0, 48(%r14)
	je	.L39
	movl	%r10d, (%r15)
	addq	$4, %r15
.L39:
	addl	$2, %r12d
	movslq	%r12d, %r13
.L37:
	movl	-60(%rbp), %ecx
	movb	$1, 16(%r11)
	movl	%ecx, %esi
	shrl	$8, %esi
	cmpl	%r12d, %r9d
	jg	.L40
	movq	8(%r14), %rdx
	movl	$1, %r8d
	movsbq	91(%rdx), %rax
	leal	1(%rax), %edi
	movb	%dil, 91(%rdx)
	movb	%sil, 104(%rdx,%rax)
	movq	8(%r14), %rdx
	movsbq	91(%rdx), %rax
	leal	1(%rax), %esi
	movb	%sil, 91(%rdx)
	movb	%cl, 104(%rdx,%rax)
	movq	-72(%rbp), %rax
	movl	$15, (%rax)
.L23:
	leaq	(%rbx,%rbx), %rax
	cmpq	%rbx, -96(%rbp)
	je	.L82
.L16:
	movl	$65535, -60(%rbp)
	addq	$1, %rbx
	cmpl	%r12d, %r9d
	jle	.L83
.L17:
	movq	-80(%rbp), %rax
	movl	%ebx, -84(%rbp)
	leal	-1(%rbx), %r10d
	movslq	%r12d, %r13
	movzwl	-2(%rax,%rbx,2), %esi
	movw	%si, -98(%rbp)
	movl	%esi, -88(%rbp)
	cmpl	$126, %esi
	je	.L84
	cmpl	$127, %esi
	jg	.L26
	movl	%esi, -60(%rbp)
	movl	%esi, %edx
.L27:
	cmpl	$65535, %edx
	je	.L29
	cmpl	$255, %edx
	seta	%al
	movb	%al, 18(%r11)
	cmpb	%r8b, %al
	je	.L85
.L30:
	addq	32(%r14), %r13
	movb	$126, 0(%r13)
	cmpl	$255, %edx
	ja	.L86
	cmpq	$0, 48(%r14)
	je	.L45
	movl	%r10d, (%r15)
	addq	$4, %r15
.L45:
	leal	1(%r12), %eax
	movslq	%eax, %r13
	cmpl	%eax, %r9d
	jle	.L87
	movq	32(%r14), %rax
	movb	$125, (%rax,%r13)
	cmpq	$0, 48(%r14)
	je	.L48
	movl	%r10d, (%r15)
	addq	$4, %r15
.L48:
	addl	$2, %r12d
	movslq	%r12d, %r13
.L46:
	movb	$1, 16(%r11)
	movl	-60(%rbp), %edx
.L49:
	cmpl	%r9d, %r12d
	jge	.L52
	movq	-112(%rbp), %rax
	addl	$1, %r12d
	xorl	%r8d, %r8d
	movb	%dl, (%rax,%r13)
	movslq	%r12d, %r13
	testq	%r15, %r15
	je	.L23
	movl	%r10d, (%r15)
	leaq	(%rbx,%rbx), %rax
	addq	$4, %r15
	cmpq	%rbx, -96(%rbp)
	jne	.L16
	.p2align 4,,10
	.p2align 3
.L82:
	addq	32(%r14), %r13
	addq	16(%r14), %rax
.L13:
	movq	%r13, 32(%r14)
	movq	%rax, 16(%r14)
	movb	%r8b, 18(%r11)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	8(%r14), %rax
	leaq	-60(%rbp), %rdx
	movb	%r8b, -99(%rbp)
	movl	%r9d, -128(%rbp)
	movsbl	63(%rax), %ecx
	movq	(%r11), %rax
	movl	%r10d, -124(%rbp)
	movq	%r11, -120(%rbp)
	movq	48(%rax), %rdi
	movl	%esi, -104(%rbp)
	call	ucnv_MBCSFromUChar32_67@PLT
	movl	-104(%rbp), %esi
	movq	-120(%rbp), %r11
	cmpl	$2, %eax
	movl	-124(%rbp), %r10d
	movl	-128(%rbp), %r9d
	movzbl	-99(%rbp), %r8d
	je	.L89
.L28:
	movl	$65535, -60(%rbp)
.L29:
	movslq	-84(%rbp), %rax
	movl	%esi, %ecx
	movq	8(%r14), %rdx
	andl	$-2048, %ecx
	addq	%rax, %rax
	cmpl	$55296, %ecx
	je	.L90
	movq	-72(%rbp), %rbx
	addq	16(%r14), %rax
	movl	$10, (%rbx)
	movq	32(%r14), %rbx
	movq	%rbx, -112(%rbp)
.L57:
	movl	-88(%rbp), %ebx
	addq	-112(%rbp), %r13
	movl	%ebx, 84(%rdx)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L85:
	cmpb	$0, 16(%r11)
	je	.L30
	cmpl	$255, %edx
	jbe	.L49
	movl	%edx, %esi
	shrl	$8, %esi
.L40:
	movq	-112(%rbp), %rcx
	leal	1(%r12), %edx
	movb	%sil, (%rcx,%r13)
	movslq	%edx, %r13
	testq	%r15, %r15
	je	.L50
	movl	%r10d, (%r15)
	cmpl	%edx, %r9d
	jg	.L51
	movl	-60(%rbp), %edi
	addq	$4, %r15
.L59:
	movq	8(%r14), %rsi
	movl	%edx, %r12d
	movsbq	91(%rsi), %rcx
	leal	1(%rcx), %r8d
	movb	%r8b, 91(%rsi)
	movl	%eax, %r8d
	movb	%dil, 104(%rsi,%rcx)
	movq	-72(%rbp), %rdi
	movl	$15, (%rdi)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L52:
	movq	8(%r14), %rcx
	xorl	%r8d, %r8d
	movsbq	91(%rcx), %rax
	leal	1(%rax), %esi
	movb	%sil, 91(%rcx)
	movb	%dl, 104(%rcx,%rax)
	movq	-72(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%r14), %rcx
	movq	-72(%rbp), %rdi
	movl	%eax, %r12d
	movsbq	91(%rcx), %rdx
	leal	1(%rdx), %esi
	movb	%sil, 91(%rcx)
	movb	$125, 104(%rcx,%rdx)
	movl	$15, (%rdi)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L81:
	movq	8(%r14), %rsi
	movl	%edx, %r12d
	movsbq	91(%rsi), %rcx
	leal	1(%rcx), %edi
	movb	%dil, 91(%rsi)
	movq	-72(%rbp), %rdi
	movb	$123, 104(%rsi,%rcx)
	movl	$15, (%rdi)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L89:
	movl	-60(%rbp), %edx
	leal	24159(%rdx), %eax
	cmpw	$23645, %ax
	ja	.L28
	leal	95(%rdx), %eax
	cmpb	$93, %al
	ja	.L28
	subl	$32896, %edx
	movl	%edx, -60(%rbp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L84:
	movq	32(%r14), %rax
	movb	$126, (%rax,%r13)
	cmpq	$0, 48(%r14)
	je	.L22
	movl	%r10d, (%r15)
	addq	$4, %r15
.L22:
	leal	1(%r12), %eax
	movslq	%eax, %r13
	cmpl	%eax, %r9d
	jle	.L91
	movq	32(%r14), %rax
	movb	$126, (%rax,%r13)
	cmpq	$0, 48(%r14)
	je	.L25
	movl	%r10d, (%r15)
	addq	$4, %r15
.L25:
	addl	$2, %r12d
	movslq	%r12d, %r13
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L51:
	movl	-60(%rbp), %edx
	addl	$2, %r12d
	movl	%eax, %r8d
	addq	$8, %r15
	movb	%dl, (%rcx,%r13)
	movslq	%r12d, %r13
	movl	%r10d, -4(%r15)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L91:
	movq	8(%r14), %rcx
	movq	-72(%rbp), %rdi
	movl	%eax, %r12d
	movsbq	91(%rcx), %rdx
	leal	1(%rdx), %esi
	movb	%sil, 91(%rcx)
	movb	$126, 104(%rcx,%rdx)
	movl	$15, (%rdi)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$0, -88(%rbp)
	movq	%rcx, %rax
	xorl	%r13d, %r13d
	movl	$0, -84(%rbp)
.L11:
	movl	-136(%rbp), %edi
	cmpl	%edi, -84(%rbp)
	jge	.L55
	movzwl	(%rax), %ecx
	movl	%ecx, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	je	.L92
	movq	-72(%rbp), %rbx
	movl	$12, (%rbx)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L50:
	movl	-60(%rbp), %edi
	cmpl	%edx, %r9d
	jle	.L59
	addl	$2, %r12d
	movb	%dil, (%rcx,%r13)
	movl	%eax, %r8d
	movslq	%r12d, %r13
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L83:
	addq	32(%r14), %r13
	addq	16(%r14), %rax
.L15:
	movq	-72(%rbp), %rbx
	movl	$15, (%rbx)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L55:
	movq	-72(%rbp), %rbx
	movl	$0, (%rbx)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L90:
	testw	$1024, -98(%rbp)
	jne	.L54
	movq	16(%r14), %rbx
	movl	%esi, 84(%rdx)
	movq	%rbx, -80(%rbp)
	addq	%rbx, %rax
	movq	32(%r14), %rbx
	movq	%rbx, -112(%rbp)
	jmp	.L11
.L54:
	movq	-72(%rbp), %rcx
	movq	32(%r14), %rbx
	addq	16(%r14), %rax
	movl	$12, (%rcx)
	movq	%rbx, -112(%rbp)
	jmp	.L57
.L92:
	sall	$10, %esi
	movl	$0, 84(%rdx)
	movq	-80(%rbp), %rbx
	leal	-56613888(%rcx,%rsi), %eax
	movl	%eax, -88(%rbp)
	movq	-72(%rbp), %rax
	movl	$10, (%rax)
	movl	-84(%rbp), %eax
	addl	$1, %eax
	cltq
	leaq	(%rbx,%rax,2), %rax
	jmp	.L57
.L62:
	movq	-80(%rbp), %rax
	jmp	.L13
.L80:
	movq	-80(%rbp), %rax
	jmp	.L15
.L88:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2117:
	.size	UConverter_fromUnicode_HZ_OFFSETS_LOGIC, .-UConverter_fromUnicode_HZ_OFFSETS_LOGIC
	.p2align 4
	.type	UConverter_toUnicode_HZ_OFFSETS_LOGIC, @function
UConverter_toUnicode_HZ_OFFSETS_LOGIC:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rdi), %rcx
	movq	24(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	32(%rdi), %r13
	movw	%dx, -58(%rbp)
	movq	16(%rax), %r15
	cmpq	%r8, %rcx
	jnb	.L125
	leaq	-58(%rbp), %rax
	movq	%rsi, %r9
	movq	%rax, -88(%rbp)
	cmpq	%r13, 40(%rbx)
	jbe	.L96
	.p2align 4,,10
	.p2align 3
.L159:
	movq	8(%rbx), %rdx
	movzbl	(%rcx), %r14d
	leaq	1(%rcx), %r12
	cmpl	$126, 76(%rdx)
	movl	%r14d, %eax
	movl	%r14d, %r10d
	je	.L154
	cmpb	$0, 17(%r15)
	je	.L109
	movl	72(%rdx), %edi
	testl	%edi, %edi
	je	.L155
	movzbl	%dil, %esi
	leal	-33(%rdi), %r10d
	movq	%r8, -80(%rbp)
	leal	-33(%r14), %r11d
	movl	%esi, -68(%rbp)
	cmpb	$92, %r10b
	ja	.L112
	cmpb	$93, %r11b
	jbe	.L156
.L115:
	movl	-68(%rbp), %eax
	movl	$0, 72(%rdx)
	movl	$12, (%r9)
	sall	$8, %eax
	orl	%eax, %r14d
	orl	$65536, %r14d
.L117:
	movl	%r14d, %eax
	movq	%r12, %r8
	movb	%ah, 65(%rdx)
	movq	8(%rbx), %rax
	movb	%r14b, 66(%rax)
	movq	8(%rbx), %rax
	movb	$2, 64(%rax)
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%r13, 32(%rbx)
	movq	%r8, 16(%rbx)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L154:
	movl	$0, 76(%rdx)
	cmpb	$125, %r14b
	je	.L98
	jg	.L99
	cmpb	$10, %r14b
	je	.L100
	cmpb	$123, %r14b
	jne	.L101
.L98:
	cmpl	$123, %r14d
	sete	17(%r15)
	cmpb	$0, 19(%r15)
	je	.L103
	movb	$0, 19(%r15)
	movl	$18, (%r9)
	movb	$126, 65(%rdx)
	movl	$2, 284(%rdx)
	movq	8(%rbx), %rdx
	movb	%al, 66(%rdx)
	movq	8(%rbx), %rax
	movb	$2, 64(%rax)
	movq	%r13, 32(%rbx)
	movq	%r12, 16(%rbx)
.L93:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	addl	$-128, %edi
	addl	$-128, %eax
	movsbl	63(%rdx), %ecx
	movq	-88(%rbp), %rsi
	movb	%dil, -58(%rbp)
	movl	$2, %edx
	movb	%al, -57(%rbp)
	movq	(%r15), %rax
	movq	%r9, -96(%rbp)
	movq	48(%rax), %rdi
	call	ucnv_MBCSSimpleGetNextUChar_67@PLT
	movq	8(%rbx), %rdx
	movq	-96(%rbp), %r9
	cmpl	$65533, %eax
	movq	-80(%rbp), %r8
	movl	%eax, %r10d
	movl	$0, 72(%rdx)
	jg	.L158
.L113:
	movq	48(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L120
	movsbl	17(%r15), %eax
	movq	%r13, %rdx
	movq	%r12, %rsi
	subq	32(%rbx), %rdx
	subq	16(%rbx), %rsi
	sarq	%rdx
	notl	%eax
	addl	%esi, %eax
	movl	%eax, (%rcx,%rdx,4)
.L120:
	movw	%r10w, 0(%r13)
	addq	$2, %r13
.L100:
	cmpq	%r12, %r8
	je	.L94
	movq	%r12, %rcx
	cmpq	%r13, 40(%rbx)
	ja	.L159
.L96:
	movq	%rcx, %r8
	movl	$15, (%r9)
	movq	%r13, 32(%rbx)
	movq	%r8, 16(%rbx)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L99:
	cmpb	$126, %r14b
	jne	.L101
	movq	48(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L102
	movq	%r13, %rax
	movq	%r12, %rdx
	subq	32(%rbx), %rax
	subq	16(%rbx), %rdx
	sarq	%rax
	subl	$2, %edx
	movl	%edx, (%rcx,%rax,4)
.L102:
	movl	$126, %eax
	addq	$2, %r13
	movw	%ax, -2(%r13)
	movb	$0, 19(%r15)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L109:
	cmpb	$126, %r14b
	je	.L153
	movb	$0, 19(%r15)
	cmpl	$127, %r14d
	jle	.L113
.L116:
	movl	$12, (%r9)
.L122:
	movb	%al, 65(%rdx)
	movq	8(%rbx), %rax
	movq	%r12, %r8
	movb	$1, 64(%rax)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L155:
	cmpb	$126, %r14b
	je	.L153
	orl	$256, %r10d
	movl	%r10d, 72(%rdx)
	movb	$0, 19(%r15)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$126, 76(%rdx)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L103:
	movb	$1, 19(%r15)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L101:
	movb	$0, 19(%r15)
	movl	$18, (%r9)
	movb	$126, 65(%rdx)
	cmpb	$0, 17(%r15)
	je	.L105
	subl	$33, %r14d
	cmpl	$93, %r14d
	setbe	%sil
.L106:
	movq	8(%rbx), %rdx
	testb	%sil, %sil
	je	.L107
	movb	$1, 64(%rdx)
.L108:
	movq	%r13, 32(%rbx)
	movq	%rcx, 16(%rbx)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L112:
	cmpb	$93, %r11b
	ja	.L115
	movl	$0, 72(%rdx)
	movl	%edi, %eax
	movq	%rcx, %r12
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L107:
	movb	%al, 66(%rdx)
	movq	8(%rbx), %rax
	movq	%r12, %rcx
	movb	$2, 64(%rax)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L105:
	cmpl	$127, %r14d
	setle	%sil
	jmp	.L106
.L158:
	xorl	%ecx, %ecx
	cmpl	$65534, %eax
	movl	-68(%rbp), %eax
	setne	%cl
	sall	$8, %eax
	leal	10(%rcx,%rcx), %ecx
	orl	%eax, %r14d
	movl	%ecx, (%r9)
	movl	%r14d, %eax
	cmpl	$255, %r14d
	jbe	.L122
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%rcx, %r8
	jmp	.L94
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2116:
	.size	UConverter_toUnicode_HZ_OFFSETS_LOGIC, .-UConverter_toUnicode_HZ_OFFSETS_LOGIC
	.p2align 4
	.type	_HZClose, @function
_HZClose:
.LFB2114:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L167
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rax), %rdi
	call	ucnv_close_67@PLT
	cmpb	$0, 62(%rbx)
	je	.L170
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	16(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2114:
	.size	_HZClose, .-_HZClose
	.p2align 4
	.type	_HZReset, @function
_HZReset:
.LFB2115:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpl	$1, %esi
	jle	.L183
.L172:
	movq	$0, 80(%rdi)
	testq	%rax, %rax
	je	.L171
	movb	$0, 16(%rax)
	movq	16(%rdi), %rax
	movq	$0, 8(%rax)
	movb	$0, 18(%rax)
.L171:
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	movq	$0, 72(%rdi)
	testq	%rax, %rax
	je	.L173
	movb	$0, 17(%rax)
	movq	16(%rdi), %rax
	movb	$0, 19(%rax)
.L173:
	cmpl	$1, %esi
	je	.L171
	movq	16(%rdi), %rax
	jmp	.L172
	.cfi_endproc
.LFE2115:
	.size	_HZReset, .-_HZReset
	.p2align 4
	.type	_HZ_SafeClone, @function
_HZ_SafeClone:
.LFB2119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movl	(%rcx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L184
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	je	.L190
	movq	16(%rdi), %rdx
	leaq	576(%rsi), %rax
	movq	%rsi, %rbx
	leaq	288(%rsi), %rsi
	movdqu	(%rdx), %xmm0
	movl	$288, -28(%rbp)
	movups	%xmm0, 288(%rsi)
	movq	16(%rdx), %rdx
	movb	$1, -226(%rsi)
	movq	%rdx, 304(%rsi)
	leaq	-28(%rbp), %rdx
	movq	%rax, -272(%rsi)
	movq	16(%rdi), %rax
	movq	(%rax), %rdi
	call	ucnv_safeClone_67@PLT
	movq	%rax, 576(%rbx)
	movq	%rbx, %rax
.L184:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L191
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movl	$600, (%rdx)
	jmp	.L184
.L191:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2119:
	.size	_HZ_SafeClone, .-_HZ_SafeClone
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"GBK"
	.text
	.p2align 4
	.type	_HZOpen, @function
_HZOpen:
.LFB2113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 8(%rsi)
	jne	.L197
	movq	%rdi, %rbx
	movq	%rdx, %rsi
	leaq	.LC0(%rip), %rdi
	call	ucnv_open_67@PLT
	movq	%rax, %r13
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L198
.L192:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	$0, 72(%rbx)
	movl	$24, %esi
	movl	$1, %edi
	movq	$0, 80(%rbx)
	call	uprv_calloc_67@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L195
	movq	%r13, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdx, %rsi
	leaq	.LC0(%rip), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ucnv_canCreateConverter_67@PLT
.L195:
	.cfi_restore_state
	movq	%r13, %rdi
	call	ucnv_close_67@PLT
	movl	$7, (%r12)
	jmp	.L192
	.cfi_endproc
.LFE2113:
	.size	_HZOpen, .-_HZOpen
	.globl	_HZData_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_HZData_67, @object
	.size	_HZData_67, 296
_HZData_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL13_HZStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL7_HZImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL13_HZStaticData, @object
	.size	_ZL13_HZStaticData, 100
_ZL13_HZStaticData:
	.long	100
	.string	"HZ"
	.zero	57
	.long	0
	.byte	0
	.byte	23
	.byte	1
	.byte	4
	.string	"\032"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL7_HZImpl, @object
	.size	_ZL7_HZImpl, 144
_ZL7_HZImpl:
	.long	23
	.zero	4
	.quad	0
	.quad	0
	.quad	_HZOpen
	.quad	_HZClose
	.quad	_HZReset
	.quad	UConverter_toUnicode_HZ_OFFSETS_LOGIC
	.quad	UConverter_toUnicode_HZ_OFFSETS_LOGIC
	.quad	UConverter_fromUnicode_HZ_OFFSETS_LOGIC
	.quad	UConverter_fromUnicode_HZ_OFFSETS_LOGIC
	.quad	0
	.quad	0
	.quad	0
	.quad	_HZ_WriteSub
	.quad	_HZ_SafeClone
	.quad	_HZ_GetUnicodeSet
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
