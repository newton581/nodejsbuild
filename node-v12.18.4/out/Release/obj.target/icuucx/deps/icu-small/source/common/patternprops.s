	.file	"patternprops.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PatternProps8isSyntaxEi
	.type	_ZN6icu_6712PatternProps8isSyntaxEi, @function
_ZN6icu_6712PatternProps8isSyntaxEi:
.LFB1300:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edi, %edi
	js	.L1
	cmpl	$255, %edi
	jle	.L9
	cmpl	$8207, %edi
	jle	.L1
	cmpl	$12336, %edi
	jle	.L10
	leal	-64830(%rdi), %edx
	cmpl	$264, %edx
	ja	.L1
	subl	$64832, %edi
	cmpl	$260, %edi
	seta	%al
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movslq	%edi, %rdi
	leaq	_ZN6icu_67L6latin1E(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	sarl	%eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leal	-8192(%rdi), %eax
	leaq	_ZN6icu_67L9index2000E(%rip), %rdx
	movl	%edi, %ecx
	sarl	$5, %eax
	cltq
	movzbl	(%rdx,%rax), %edx
	leaq	_ZN6icu_67L10syntax2000E(%rip), %rax
	movl	(%rax,%rdx,4), %eax
	shrl	%cl, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE1300:
	.size	_ZN6icu_6712PatternProps8isSyntaxEi, .-_ZN6icu_6712PatternProps8isSyntaxEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PatternProps20isSyntaxOrWhiteSpaceEi
	.type	_ZN6icu_6712PatternProps20isSyntaxOrWhiteSpaceEi, @function
_ZN6icu_6712PatternProps20isSyntaxOrWhiteSpaceEi:
.LFB1301:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edi, %edi
	js	.L11
	cmpl	$255, %edi
	jle	.L18
	cmpl	$8205, %edi
	jle	.L11
	cmpl	$12336, %edi
	jle	.L19
	leal	-64830(%rdi), %edx
	cmpl	$264, %edx
	ja	.L11
	subl	$64832, %edi
	cmpl	$260, %edi
	seta	%al
.L11:
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movslq	%edi, %rdi
	leaq	_ZN6icu_67L6latin1E(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	leal	-8192(%rdi), %eax
	leaq	_ZN6icu_67L9index2000E(%rip), %rdx
	movl	%edi, %ecx
	sarl	$5, %eax
	cltq
	movzbl	(%rdx,%rax), %edx
	leaq	_ZN6icu_67L22syntaxOrWhiteSpace2000E(%rip), %rax
	movl	(%rax,%rdx,4), %eax
	shrl	%cl, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE1301:
	.size	_ZN6icu_6712PatternProps20isSyntaxOrWhiteSpaceEi, .-_ZN6icu_6712PatternProps20isSyntaxOrWhiteSpaceEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PatternProps12isWhiteSpaceEi
	.type	_ZN6icu_6712PatternProps12isWhiteSpaceEi, @function
_ZN6icu_6712PatternProps12isWhiteSpaceEi:
.LFB1302:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edi, %edi
	js	.L20
	cmpl	$255, %edi
	jle	.L25
	leal	-8206(%rdi), %edx
	cmpl	$27, %edx
	ja	.L20
	subl	$8208, %edi
	cmpl	$23, %edi
	seta	%al
.L20:
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movslq	%edi, %rdi
	leaq	_ZN6icu_67L6latin1E(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	sarl	$2, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE1302:
	.size	_ZN6icu_6712PatternProps12isWhiteSpaceEi, .-_ZN6icu_6712PatternProps12isWhiteSpaceEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi
	.type	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi, @function
_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi:
.LFB1303:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	testl	%esi, %esi
	jle	.L26
	leal	-1(%rsi), %eax
	leaq	_ZN6icu_67L6latin1E(%rip), %rsi
	leaq	2(%rdi,%rax,2), %rcx
	.p2align 4,,10
	.p2align 3
.L30:
	movzwl	(%r8), %eax
	cmpl	$255, %eax
	jle	.L36
	leal	-8206(%rax), %edx
	cmpl	$27, %edx
	jbe	.L37
.L26:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movzbl	(%rsi,%rax), %eax
	sarl	$2, %eax
	andl	$1, %eax
.L29:
	testb	%al, %al
	je	.L26
	addq	$2, %r8
	cmpq	%r8, %rcx
	jne	.L30
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	subl	$8208, %eax
	cmpl	$23, %eax
	seta	%al
	jmp	.L29
	.cfi_endproc
.LFE1303:
	.size	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi, .-_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PatternProps14skipWhiteSpaceERKNS_13UnicodeStringEi
	.type	_ZN6icu_6712PatternProps14skipWhiteSpaceERKNS_13UnicodeStringEi, @function
_ZN6icu_6712PatternProps14skipWhiteSpaceERKNS_13UnicodeStringEi:
.LFB1304:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L39
	movswl	%ax, %edx
	sarl	$5, %edx
.L40:
	cmpl	%edx, %esi
	jge	.L56
	testb	$2, %al
	jne	.L42
	movslq	%esi, %rcx
	movl	%esi, %r8d
	leaq	_ZN6icu_67L6latin1E(%rip), %r9
	addq	%rcx, %rcx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L62:
	leal	-8206(%rax), %esi
	cmpl	$27, %esi
	ja	.L38
	subl	$8208, %eax
	cmpl	$23, %eax
	seta	%al
.L46:
	testb	%al, %al
	je	.L38
	addl	$1, %r8d
	addq	$2, %rcx
	cmpl	%r8d, %edx
	je	.L38
.L48:
	cmpl	%r8d, %edx
	jbe	.L38
	movq	24(%rdi), %rax
	movzwl	(%rax,%rcx), %eax
	cmpl	$255, %eax
	jg	.L62
	movzbl	(%r9,%rax), %eax
	sarl	$2, %eax
	andl	$1, %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L42:
	movslq	%esi, %rax
	leaq	_ZN6icu_67L6latin1E(%rip), %r10
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	leal	-8206(%rcx), %r9d
	cmpl	$27, %r9d
	ja	.L38
	subl	$8208, %ecx
	cmpl	$23, %ecx
	seta	%cl
.L52:
	testb	%cl, %cl
	je	.L38
	addq	$1, %rax
	leal	1(%rsi), %r8d
	cmpl	%eax, %edx
	jle	.L38
.L55:
	movl	%eax, %esi
	movl	%eax, %r8d
	cmpl	%eax, %edx
	jbe	.L38
	movzwl	10(%rdi,%rax,2), %ecx
	cmpl	$255, %ecx
	jg	.L51
	movzbl	(%r10,%rcx), %ecx
	sarl	$2, %ecx
	andl	$1, %ecx
	jmp	.L52
.L56:
	movl	%esi, %r8d
.L38:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movl	12(%rdi), %edx
	jmp	.L40
	.cfi_endproc
.LFE1304:
	.size	_ZN6icu_6712PatternProps14skipWhiteSpaceERKNS_13UnicodeStringEi, .-_ZN6icu_6712PatternProps14skipWhiteSpaceERKNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PatternProps14trimWhiteSpaceEPKDsRi
	.type	_ZN6icu_6712PatternProps14trimWhiteSpaceEPKDsRi, @function
_ZN6icu_6712PatternProps14trimWhiteSpaceEPKDsRi:
.LFB1305:
	.cfi_startproc
	endbr64
	movl	(%rsi), %r8d
	movq	%rdi, %r9
	testl	%r8d, %r8d
	jle	.L63
	movzwl	(%rdi), %eax
	cmpl	$255, %eax
	jle	.L85
	leal	-8206(%rax), %edx
	cmpl	$27, %edx
	ja	.L67
	subl	$8208, %eax
	cmpl	$23, %eax
	seta	%al
.L66:
	testb	%al, %al
	je	.L67
.L68:
	movq	%rdi, %rdx
	xorl	%ecx, %ecx
	leaq	_ZN6icu_67L6latin1E(%rip), %r11
	.p2align 4,,10
	.p2align 3
.L77:
	movzwl	(%rdx), %eax
	movq	%rdx, %r9
	cmpl	$255, %eax
	jle	.L86
	leal	-8206(%rax), %r10d
	cmpl	$27, %r10d
	jbe	.L87
.L73:
	cmpl	%ecx, %r8d
	jle	.L75
	movslq	%r8d, %rdx
	leaq	_ZN6icu_67L6latin1E(%rip), %r11
	.p2align 4,,10
	.p2align 3
.L76:
	movzwl	-2(%rdi,%rdx,2), %eax
	movl	%edx, %r8d
	cmpl	$255, %eax
	jle	.L88
	leal	-8206(%rax), %r10d
	cmpl	$27, %r10d
	jbe	.L89
.L75:
	subl	%ecx, %r8d
	movl	%r8d, (%rsi)
.L63:
	movq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	_ZN6icu_67L6latin1E(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	sarl	$2, %eax
	andl	$1, %eax
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L86:
	movzbl	(%r11,%rax), %eax
	sarl	$2, %eax
	andl	$1, %eax
.L72:
	testb	%al, %al
	je	.L73
	addl	$1, %ecx
	addq	$2, %rdx
	cmpl	%ecx, %r8d
	jne	.L77
	movslq	%r8d, %rax
	movl	%r8d, %ecx
	leaq	(%rdi,%rax,2), %r9
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L87:
	subl	$8208, %eax
	cmpl	$23, %eax
	seta	%al
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L88:
	movzbl	(%r11,%rax), %eax
	sarl	$2, %eax
	andl	$1, %eax
.L79:
	subq	$1, %rdx
	testb	%al, %al
	jne	.L76
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L89:
	subl	$8208, %eax
	cmpl	$23, %eax
	seta	%al
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L67:
	movslq	%r8d, %rax
	movzwl	-2(%rdi,%rax,2), %eax
	cmpl	$255, %eax
	jle	.L90
	leal	-8206(%rax), %edx
	movq	%rdi, %r9
	cmpl	$27, %edx
	ja	.L63
	subl	$8208, %eax
	cmpl	$23, %eax
	seta	%al
.L70:
	testb	%al, %al
	jne	.L68
	movq	%rdi, %r9
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	_ZN6icu_67L6latin1E(%rip), %rdx
	movzbl	(%rdx,%rax), %eax
	sarl	$2, %eax
	andl	$1, %eax
	jmp	.L70
	.cfi_endproc
.LFE1305:
	.size	_ZN6icu_6712PatternProps14trimWhiteSpaceEPKDsRi, .-_ZN6icu_6712PatternProps14trimWhiteSpaceEPKDsRi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PatternProps12isIdentifierEPKDsi
	.type	_ZN6icu_6712PatternProps12isIdentifierEPKDsi, @function
_ZN6icu_6712PatternProps12isIdentifierEPKDsi:
.LFB1306:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L91
	movslq	%esi, %rsi
	leaq	_ZN6icu_67L22syntaxOrWhiteSpace2000E(%rip), %r9
	leaq	_ZN6icu_67L9index2000E(%rip), %r8
	leaq	(%rdi,%rsi,2), %rdx
	leaq	_ZN6icu_67L6latin1E(%rip), %rsi
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L93:
	cmpl	$8205, %eax
	jle	.L95
	cmpl	$12336, %eax
	jle	.L101
	leal	-64830(%rax), %ecx
	cmpl	$264, %ecx
	ja	.L95
	subl	$64832, %eax
	cmpl	$260, %eax
	seta	%al
.L94:
	testb	%al, %al
	jne	.L99
.L95:
	cmpq	%rdi, %rdx
	jbe	.L102
.L97:
	movzwl	(%rdi), %eax
	addq	$2, %rdi
	movl	%eax, %ecx
	cmpl	$255, %eax
	jg	.L93
	cltq
	movzbl	(%rsi,%rax), %eax
	andl	$1, %eax
	testb	%al, %al
	je	.L95
.L99:
	xorl	%eax, %eax
.L91:
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	subl	$8192, %eax
	sarl	$5, %eax
	cltq
	movzbl	(%r8,%rax), %eax
	movl	(%r9,%rax,4), %eax
	shrl	%cl, %eax
	andl	$1, %eax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1306:
	.size	_ZN6icu_6712PatternProps12isIdentifierEPKDsi, .-_ZN6icu_6712PatternProps12isIdentifierEPKDsi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PatternProps14skipIdentifierEPKDsi
	.type	_ZN6icu_6712PatternProps14skipIdentifierEPKDsi, @function
_ZN6icu_6712PatternProps14skipIdentifierEPKDsi:
.LFB1307:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	testl	%esi, %esi
	jle	.L103
	leal	-1(%rsi), %eax
	leaq	_ZN6icu_67L22syntaxOrWhiteSpace2000E(%rip), %r9
	leaq	2(%rdi,%rax,2), %rdx
	leaq	_ZN6icu_67L6latin1E(%rip), %rsi
	leaq	_ZN6icu_67L9index2000E(%rip), %rdi
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L105:
	cmpl	$8205, %eax
	jle	.L107
	cmpl	$12336, %eax
	jle	.L112
	leal	-64830(%rax), %ecx
	cmpl	$264, %ecx
	ja	.L107
	subl	$64832, %eax
	cmpl	$260, %eax
	seta	%al
.L106:
	testb	%al, %al
	jne	.L103
.L107:
	addq	$2, %r8
	cmpq	%rdx, %r8
	je	.L103
.L109:
	movzwl	(%r8), %eax
	movl	%eax, %ecx
	cmpl	$255, %eax
	jg	.L105
	cltq
	movzbl	(%rsi,%rax), %eax
	andl	$1, %eax
	testb	%al, %al
	je	.L107
.L103:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	subl	$8192, %eax
	sarl	$5, %eax
	cltq
	movzbl	(%rdi,%rax), %eax
	movl	(%r9,%rax,4), %eax
	shrl	%cl, %eax
	andl	$1, %eax
	jmp	.L106
	.cfi_endproc
.LFE1307:
	.size	_ZN6icu_6712PatternProps14skipIdentifierEPKDsi, .-_ZN6icu_6712PatternProps14skipIdentifierEPKDsi
	.section	.rodata
	.align 32
	.type	_ZN6icu_67L22syntaxOrWhiteSpace2000E, @object
	.size	_ZN6icu_67L22syntaxOrWhiteSpace2000E, 40
_ZN6icu_67L22syntaxOrWhiteSpace2000E:
	.long	0
	.long	-1
	.long	-16384
	.long	2147419135
	.long	2146435070
	.long	-65536
	.long	4194303
	.long	-1048576
	.long	-242
	.long	65537
	.align 32
	.type	_ZN6icu_67L10syntax2000E, @object
	.size	_ZN6icu_67L10syntax2000E, 40
_ZN6icu_67L10syntax2000E:
	.long	0
	.long	-1
	.long	-65536
	.long	2147418367
	.long	2146435070
	.long	-65536
	.long	4194303
	.long	-1048576
	.long	-242
	.long	65537
	.align 32
	.type	_ZN6icu_67L9index2000E, @object
	.size	_ZN6icu_67L9index2000E, 130
_ZN6icu_67L9index2000E:
	.string	"\002\003\004"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\005\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\006\007\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001\001\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\b\t"
	.align 32
	.type	_ZN6icu_67L6latin1E, @object
	.size	_ZN6icu_67L6latin1E, 256
_ZN6icu_67L6latin1E:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\005\005\005\005\005"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\005\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003"
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\005"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003"
	.string	"\003"
	.string	"\003\003"
	.string	"\003"
	.string	"\003\003"
	.string	""
	.string	""
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003"
	.zero	7
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
