	.file	"punycode.cpp"
	.text
	.p2align 4
	.globl	u_strToPunycode_67
	.type	u_strToPunycode_67, @function
u_strToPunycode_67:
.LFB2057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$872, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -872(%rbp)
	movq	%r9, -896(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testq	%r9, %r9
	je	.L1
	movl	(%r9), %r9d
	testl	%r9d, %r9d
	jg	.L1
	testq	%rdi, %rdi
	je	.L3
	cmpl	$-1, %esi
	jl	.L3
	movl	%ecx, %r15d
	testq	%rdx, %rdx
	jne	.L4
	testl	%ecx, %ecx
	je	.L4
.L3:
	movq	-896(%rbp), %rax
	movl	$1, (%rax)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$872, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	cmpl	$-1, %esi
	je	.L106
	testl	%esi, %esi
	je	.L6
	movq	-872(%rbp), %rbx
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	-864(%rbp), %r14
	leal	1(%rdx), %r12d
	movl	$0, (%r14,%rdx,4)
	cmpl	%r11d, %r15d
	jle	.L24
	testq	%r8, %r8
	je	.L25
	cmpb	$0, (%r8,%r10)
	movl	%eax, %r9d
	je	.L26
	subl	$97, %eax
	leal	-32(%r9), %r10d
	cmpb	$26, %al
	cmovb	%r10d, %r9d
.L27:
	movsbw	%r9b, %ax
.L25:
	movslq	%r11d, %r9
	movw	%ax, (%rbx,%r9,2)
.L24:
	addl	$1, %r11d
.L28:
	addl	$1, %ecx
	cmpl	%ecx, %esi
	jle	.L107
	addq	$1, %rdx
	cmpq	$200, %rdx
	je	.L22
.L21:
	movslq	%ecx, %r10
	movzwl	(%rdi,%r10,2), %eax
	leaq	(%r10,%r10), %r12
	cmpw	$127, %ax
	jbe	.L108
	xorl	%r9d, %r9d
	testq	%r8, %r8
	je	.L29
	cmpb	$1, (%r8,%r10)
	sbbl	%r9d, %r9d
	notl	%r9d
	andl	$-2147483648, %r9d
.L29:
	movzwl	%ax, %r10d
	andl	$63488, %eax
	cmpl	$55296, %eax
	je	.L30
	orl	%r10d, %r9d
.L31:
	leaq	-864(%rbp), %r14
	leal	1(%rdx), %r12d
	movl	%r9d, (%r14,%rdx,4)
	jmp	.L28
.L106:
	movzwl	(%rdi), %eax
	testw	%ax, %ax
	je	.L61
	movq	-872(%rbp), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	-864(%rbp), %r14
	leal	1(%rdx), %r10d
	movl	$0, (%r14,%rdx,4)
	cmpl	%r11d, %r15d
	jle	.L9
	testq	%r8, %r8
	je	.L10
	cmpb	$0, (%r8,%r9)
	movl	%eax, %ecx
	je	.L11
	subl	$97, %eax
	leal	-32(%rcx), %r9d
	cmpb	$26, %al
	cmovb	%r9d, %ecx
.L12:
	movsbw	%cl, %ax
.L10:
	movslq	%r11d, %rcx
	movw	%ax, (%rbx,%rcx,2)
.L9:
	addl	$1, %r11d
.L13:
	addl	$1, %esi
	movslq	%esi, %r9
	movzwl	(%rdi,%r9,2), %eax
	testw	%ax, %ax
	je	.L109
	addq	$1, %rdx
	cmpq	$200, %rdx
	je	.L22
.L7:
	cmpw	$127, %ax
	jbe	.L110
	xorl	%ecx, %ecx
	testq	%r8, %r8
	je	.L14
	cmpb	$1, (%r8,%r9)
	sbbl	%ecx, %ecx
	notl	%ecx
	andl	$-2147483648, %ecx
.L14:
	movzwl	%ax, %r10d
	andl	$63488, %eax
	cmpl	$55296, %eax
	je	.L15
	orl	%r10d, %ecx
.L16:
	leaq	-864(%rbp), %r14
	leal	1(%rdx), %r10d
	movl	%ecx, (%r14,%rdx,4)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L30:
	movl	%r10d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L18
	addl	$1, %ecx
	cmpl	%esi, %ecx
	jge	.L18
	movzwl	2(%rdi,%r12), %eax
	movl	%eax, %r12d
	andl	$-1024, %r12d
	cmpl	$56320, %r12d
	jne	.L18
	sall	$10, %r10d
	leal	-56613888(%rax,%r10), %eax
	orl	%eax, %r9d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L26:
	subl	$65, %eax
	leal	32(%r9), %r10d
	cmpb	$26, %al
	cmovb	%r10d, %r9d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%r10d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L18
	movzwl	2(%rdi,%r9,2), %eax
	movl	%eax, %r9d
	andl	$-1024, %r9d
	cmpl	$56320, %r9d
	jne	.L18
	sall	$10, %r10d
	addl	$1, %esi
	leal	-56613888(%rax,%r10), %eax
	orl	%eax, %ecx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L11:
	subl	$65, %eax
	leal	32(%rcx), %r9d
	cmpb	$26, %al
	cmovb	%r9d, %ecx
	jmp	.L12
.L107:
	movl	%r11d, -908(%rbp)
	movslq	%r11d, %rax
	movl	%r12d, -880(%rbp)
.L19:
	testl	%eax, %eax
	je	.L66
	cmpl	%r15d, %eax
	jl	.L111
.L34:
	movl	-908(%rbp), %eax
	leal	1(%rax), %esi
	cmpl	-880(%rbp), %eax
	jge	.L6
.L33:
	movl	-880(%rbp), %eax
	movq	%r14, -888(%rbp)
	xorl	%ecx, %ecx
	movl	$128, %edi
	subl	$1, %eax
	leaq	-860(%rbp,%rax,4), %r12
	movl	-908(%rbp), %eax
	movl	%eax, -876(%rbp)
	movl	$72, %eax
	movl	%eax, %r14d
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-888(%rbp), %rdx
	movl	$2147483647, %r8d
	.p2align 4,,10
	.p2align 3
.L37:
	movl	(%rdx), %eax
	andl	$2147483647, %eax
	cmpl	%r8d, %eax
	jge	.L35
	cmpl	%eax, %edi
	cmovle	%eax, %r8d
.L35:
	addq	$4, %rdx
	cmpq	%rdx, %r12
	jne	.L37
	movl	-876(%rbp), %eax
	movl	%r8d, %r13d
	subl	%edi, %r13d
	leal	1(%rax), %edi
	movl	$2147483447, %eax
	subl	%ecx, %eax
	cltd
	idivl	%edi
	cmpl	%eax, %r13d
	jg	.L112
	imull	%edi, %r13d
	movq	-888(%rbp), %rdi
	addl	%ecx, %r13d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L114:
	addl	$1, %r13d
.L40:
	addq	$4, %rdi
	cmpq	%rdi, %r12
	je	.L113
.L55:
	movl	(%rdi), %ebx
	movl	%ebx, %eax
	andl	$2147483647, %eax
	cmpl	%r8d, %eax
	jl	.L114
	jne	.L40
	movl	$36, %r10d
	leal	25(%r14), %eax
	movslq	%esi, %rsi
	movl	%r13d, %ecx
	movq	%rdi, -904(%rbp)
	subl	%r14d, %r10d
	movl	%eax, %edi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L115:
	movl	$36, %r11d
	subl	%r9d, %ecx
	subl	%r9d, %r11d
	cmpl	%esi, %r15d
	jle	.L43
	movl	%ecx, %eax
	cltd
	idivl	%r11d
	addl	%edx, %r9d
	leal	97(%r9), %edx
	leal	22(%r9), %eax
	cmpl	$25, %r9d
	cmovle	%edx, %eax
	movq	-872(%rbp), %rdx
	cbtw
	movw	%ax, (%rdx,%rsi,2)
.L43:
	movl	%ecx, %eax
	addq	$1, %rsi
	addl	$36, %r10d
	cltd
	idivl	%r11d
	movl	%eax, %ecx
.L46:
	movl	%esi, %eax
	movl	$1, %r9d
	testl	%r10d, %r10d
	jle	.L41
	leal	(%r14,%r10), %edx
	movl	$26, %r9d
	cmpl	%edx, %edi
	cmovge	%r10d, %r9d
.L41:
	cmpl	%r9d, %ecx
	jge	.L115
	movq	-904(%rbp), %rdi
	cmpl	%r15d, %eax
	jge	.L47
	movl	%ecx, %esi
	leal	22(%rcx), %edx
	cmpl	$25, %ecx
	jg	.L50
	leal	65(%rsi), %edx
	addl	$97, %esi
	testl	%ebx, %ebx
	cmovns	%esi, %edx
.L50:
	movq	-872(%rbp), %rbx
	movslq	%eax, %rcx
	movsbw	%dl, %dx
	movw	%dx, (%rbx,%rcx,2)
.L47:
	leal	1(%rax), %esi
	movl	-876(%rbp), %eax
	leal	1(%rax), %ecx
	cmpl	%eax, -908(%rbp)
	jne	.L51
	movslq	%r13d, %rax
	sarl	$31, %r13d
	imulq	$1570730897, %rax, %rax
	sarq	$40, %rax
	subl	%r13d, %eax
	movl	%eax, %r13d
.L52:
	movl	%r13d, %eax
	xorl	%r9d, %r9d
	cltd
	idivl	%ecx
	addl	%eax, %r13d
	cmpl	$455, %r13d
	jle	.L53
	.p2align 4,,10
	.p2align 3
.L54:
	movslq	%r13d, %r13
	addl	$36, %r9d
	movq	%r13, %rax
	imulq	$-368140053, %r13, %r13
	cltd
	shrq	$32, %r13
	addl	%eax, %r13d
	sarl	$5, %r13d
	subl	%edx, %r13d
	cmpl	$15959, %eax
	jg	.L54
.L53:
	leal	0(%r13,%r13,8), %eax
	addl	$38, %r13d
	addq	$4, %rdi
	movl	%ecx, -876(%rbp)
	sall	$2, %eax
	cltd
	idivl	%r13d
	xorl	%r13d, %r13d
	leal	(%rax,%r9), %r14d
	cmpq	%rdi, %r12
	jne	.L55
	.p2align 4,,10
	.p2align 3
.L113:
	movl	-880(%rbp), %ebx
	leal	1(%r13), %ecx
	leal	1(%r8), %edi
	cmpl	%ebx, -876(%rbp)
	jl	.L56
.L6:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	movq	-896(%rbp), %rcx
	movq	-872(%rbp), %rdi
	movl	%esi, %edx
	addq	$872, %rsp
	popq	%rbx
	movl	%r15d, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movl	%r13d, %eax
	shrl	$31, %eax
	addl	%eax, %r13d
	sarl	%r13d
	jmp	.L52
.L112:
	movq	-896(%rbp), %rax
	movl	$5, (%rax)
	jmp	.L1
.L111:
	movq	-872(%rbp), %rbx
	movl	$45, %edx
	movw	%dx, (%rbx,%rax,2)
	jmp	.L34
.L18:
	movq	-896(%rbp), %rax
	movl	$10, (%rax)
	jmp	.L1
.L22:
	movq	-896(%rbp), %rax
	movl	$8, (%rax)
	jmp	.L1
.L66:
	xorl	%esi, %esi
	jmp	.L33
.L109:
	movl	%r11d, -908(%rbp)
	movslq	%r11d, %rax
	movl	%r10d, -880(%rbp)
	jmp	.L19
.L61:
	xorl	%esi, %esi
	jmp	.L6
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2057:
	.size	u_strToPunycode_67, .-u_strToPunycode_67
	.p2align 4
	.globl	u_strFromPunycode_67
	.type	u_strFromPunycode_67, @function
u_strFromPunycode_67:
.LFB2058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -56(%rbp)
	testq	%r9, %r9
	je	.L116
	movl	(%r9), %r8d
	testl	%r8d, %r8d
	jg	.L116
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L118
	movl	%esi, %r12d
	cmpl	$-1, %esi
	jl	.L118
	movq	%rdx, %r15
	movl	%ecx, %r13d
	testq	%rdx, %rdx
	jne	.L119
	testl	%ecx, %ecx
	je	.L119
.L118:
	movl	$1, (%r9)
.L116:
	addq	$88, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	cmpl	$-1, %r12d
	je	.L222
.L120:
	movslq	%r12d, %rax
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L224:
	leal	-1(%rax), %ecx
	subq	$1, %rax
	cmpw	$45, (%rbx,%rax,2)
	je	.L223
.L122:
	movl	%eax, %r14d
	testl	%eax, %eax
	jg	.L224
.L219:
	xorl	%eax, %eax
.L121:
	cmpl	%r12d, %eax
	jge	.L130
	movq	%r15, -88(%rbp)
	movl	$72, %esi
	movl	$2147483647, %r11d
	movl	$1000000000, -80(%rbp)
	movl	$0, -64(%rbp)
	movl	$128, -72(%rbp)
	movl	%r14d, -76(%rbp)
	movl	%r14d, -68(%rbp)
	movl	%esi, %r14d
	movl	%r13d, -92(%rbp)
	movl	%eax, %r13d
.L131:
	leal	1(%r13), %esi
	movl	$36, %r8d
	movl	-64(%rbp), %r10d
	movl	$1, %ecx
	movslq	%esi, %rsi
	subl	%r14d, %r8d
	leal	25(%r14), %r15d
	.p2align 4,,10
	.p2align 3
.L135:
	movzbl	-2(%rbx,%rsi,2), %eax
	leaq	_ZL12basicToDigit(%rip), %rdx
	movl	%esi, %r13d
	movsbl	(%rdx,%rax), %edi
	testl	%edi, %edi
	js	.L125
	movl	%r11d, %eax
	subl	%r10d, %eax
	cltd
	idivl	%ecx
	cmpl	%edi, %eax
	jl	.L132
	movl	%edi, %eax
	imull	%ecx, %eax
	addl	%eax, %r10d
	movl	$1, %eax
	testl	%r8d, %r8d
	jle	.L133
	leal	(%r14,%r8), %eax
	cmpl	%eax, %r15d
	movl	$26, %eax
	cmovge	%r8d, %eax
.L133:
	cmpl	%edi, %eax
	jg	.L134
	movl	$36, %edi
	subl	%eax, %edi
	movl	%r11d, %eax
	cltd
	idivl	%edi
	cmpl	%ecx, %eax
	jl	.L132
	addq	$1, %rsi
	imull	%edi, %ecx
	addl	$36, %r8d
	leal	-1(%rsi), %eax
	cmpl	%r12d, %eax
	jl	.L135
.L132:
	movl	$12, (%r9)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L223:
	testl	%ecx, %ecx
	jle	.L168
	leal	-2(%r14), %edx
	movslq	%ecx, %rsi
	movq	-56(%rbp), %rdi
	movslq	%edx, %rax
	subq	$2, %rsi
	movl	%edx, %edx
	subq	%rdx, %rsi
	testq	%rdi, %rdi
	jne	.L124
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	cmpl	%eax, %r13d
	jle	.L129
	movw	%dx, (%r15,%rax,2)
	subl	$65, %edx
	cmpw	$25, %dx
	setbe	(%rdi,%rax)
.L129:
	subq	$1, %rax
	cmpq	%rax, %rsi
	je	.L170
.L124:
	movzwl	(%rbx,%rax,2), %edx
	cmpw	$127, %dx
	jbe	.L128
.L125:
	movl	$10, (%r9)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L225:
	cmpl	%eax, %r13d
	jle	.L126
	movw	%dx, (%r15,%rax,2)
.L126:
	subq	$1, %rax
	cmpq	%rax, %rsi
	je	.L170
.L127:
	movzwl	(%rbx,%rax,2), %edx
	cmpw	$127, %dx
	jbe	.L225
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L170:
	movl	%r14d, %eax
	movl	%ecx, %r14d
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%rbx, %rdi
	movq	%r9, -64(%rbp)
	call	u_strlen_67@PLT
	movq	-64(%rbp), %r9
	movl	%eax, %r12d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L134:
	movl	-64(%rbp), %eax
	addl	$1, -68(%rbp)
	testl	%eax, %eax
	jne	.L136
	movslq	%r10d, %rcx
	movl	%r10d, %eax
	imulq	$1570730897, %rcx, %rcx
	sarl	$31, %eax
	sarq	$40, %rcx
	subl	%eax, %ecx
.L137:
	movl	%ecx, %eax
	xorl	%esi, %esi
	cltd
	idivl	-68(%rbp)
	addl	%eax, %ecx
	cmpl	$455, %ecx
	jle	.L138
	.p2align 4,,10
	.p2align 3
.L139:
	movslq	%ecx, %rcx
	addl	$36, %esi
	movq	%rcx, %rax
	imulq	$-368140053, %rcx, %rcx
	cltd
	shrq	$32, %rcx
	addl	%eax, %ecx
	sarl	$5, %ecx
	subl	%edx, %ecx
	cmpl	$15959, %eax
	jg	.L139
.L138:
	leal	(%rcx,%rcx,8), %eax
	addl	$38, %ecx
	sall	$2, %eax
	cltd
	idivl	%ecx
	leal	(%rax,%rsi), %r14d
	movl	%r10d, %eax
	movl	-72(%rbp), %esi
	cltd
	idivl	-68(%rbp)
	movl	%edx, -64(%rbp)
	movl	%r11d, %edx
	subl	%esi, %edx
	cmpl	%edx, %eax
	jg	.L132
	addl	%eax, %esi
	movl	%esi, -72(%rbp)
	cmpl	$1114111, %esi
	jg	.L132
	movl	%esi, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L132
	cmpl	$65535, %esi
	ja	.L140
	cmpq	$0, -88(%rbp)
	je	.L220
	movl	-76(%rbp), %eax
	movl	-92(%rbp), %esi
	cmpl	%esi, %eax
	jl	.L143
	addl	$1, %eax
	movl	%eax, -76(%rbp)
	.p2align 4,,10
	.p2align 3
.L142:
	movl	-64(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -64(%rbp)
	cmpl	%r13d, %r12d
	jg	.L131
	movq	-88(%rbp), %r15
	movl	-92(%rbp), %r13d
	movl	-76(%rbp), %r14d
.L130:
	addq	$88, %rsp
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	popq	%rbx
	movq	%r9, %rcx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
.L136:
	.cfi_restore_state
	movl	%r10d, %eax
	subl	-64(%rbp), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	jmp	.L137
.L140:
	movl	-76(%rbp), %eax
	addl	$2, %eax
	cmpq	$0, -88(%rbp)
	je	.L175
	cmpl	%eax, -92(%rbp)
	jl	.L175
	movl	-80(%rbp), %esi
	cmpl	%esi, -64(%rbp)
	jg	.L226
	movslq	-64(%rbp), %r15
	movq	-88(%rbp), %rsi
	movl	$2, -96(%rbp)
	movl	%r15d, -80(%rbp)
	movq	%r15, %rax
	leaq	(%rsi,%r15,2), %r8
.L147:
	cmpl	%eax, -76(%rbp)
	jg	.L227
.L151:
	cmpl	$1, -96(%rbp)
	je	.L228
	movl	-72(%rbp), %esi
	movl	%esi, %eax
	movl	%esi, %edx
	movq	-88(%rbp), %rsi
	sarl	$10, %eax
	andw	$1023, %dx
	subw	$10304, %ax
	orw	$-9216, %dx
	cmpq	$0, -56(%rbp)
	movw	%ax, (%r8)
	leaq	1(%r15), %rax
	movw	%dx, (%rsi,%rax,2)
	jne	.L161
.L221:
	addl	$2, -76(%rbp)
	jmp	.L142
.L175:
	movl	%eax, -76(%rbp)
	jmp	.L142
.L228:
	movzwl	-72(%rbp), %eax
	cmpq	$0, -56(%rbp)
	movw	%ax, (%r8)
	je	.L220
.L160:
	movslq	%r13d, %rax
	movzwl	-2(%rbx,%rax,2), %eax
	subl	$65, %eax
	cmpw	$25, %ax
	movq	-56(%rbp), %rax
	setbe	(%rax,%r15)
.L220:
	addl	$1, -76(%rbp)
	jmp	.L142
.L143:
	movl	-80(%rbp), %esi
	cmpl	%esi, -64(%rbp)
	jg	.L229
	movslq	-64(%rbp), %r15
	movq	-88(%rbp), %rsi
	movl	$1, -96(%rbp)
	addl	$1, -80(%rbp)
	movq	%r15, %rax
	leaq	(%rsi,%r15,2), %r8
	jmp	.L147
.L168:
	movl	%ecx, %r14d
	jmp	.L219
.L227:
	movl	-76(%rbp), %ecx
	movslq	-96(%rbp), %r10
	movq	%r9, -112(%rbp)
	movq	-88(%rbp), %rsi
	movq	%r8, -104(%rbp)
	subl	%eax, %ecx
	addq	%r15, %r10
	leal	(%rcx,%rcx), %edx
	leaq	(%rsi,%r10,2), %rdi
	movq	%r8, %rsi
	movl	%ecx, -124(%rbp)
	movslq	%edx, %rdx
	movq	%r10, -120(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rax
	movq	-104(%rbp), %r8
	movl	$2147483647, %r11d
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	je	.L156
	movq	-120(%rbp), %r10
	movslq	-124(%rbp), %rdx
	leaq	(%rax,%r15), %rsi
	leaq	(%rax,%r10), %rdi
	call	memmove@PLT
	cmpl	$1, -96(%rbp)
	movq	-104(%rbp), %r8
	movl	$2147483647, %r11d
	movq	-112(%rbp), %r9
	je	.L230
	movl	-72(%rbp), %esi
	movl	%esi, %eax
	movl	%esi, %edx
	movq	-88(%rbp), %rsi
	sarl	$10, %eax
	andw	$1023, %dx
	subw	$10304, %ax
	orw	$-9216, %dx
	movw	%ax, (%r8)
	leaq	1(%r15), %rax
	movw	%dx, (%rsi,%rax,2)
.L161:
	movslq	%r13d, %rdx
	movq	-56(%rbp), %rsi
	movzwl	-2(%rbx,%rdx,2), %edx
	subl	$65, %edx
	cmpw	$25, %dx
	setbe	(%rsi,%r15)
	movb	$0, (%rsi,%rax)
	jmp	.L221
.L156:
	cmpl	$1, -96(%rbp)
	je	.L163
	movl	-72(%rbp), %esi
	movl	%esi, %eax
	sarl	$10, %eax
	subw	$10304, %ax
	movw	%ax, (%r8)
	movl	%esi, %eax
	movq	-88(%rbp), %rsi
	andw	$1023, %ax
	orw	$-9216, %ax
	movw	%ax, 2(%rsi,%r15,2)
	jmp	.L221
.L229:
	movl	$1, -96(%rbp)
.L145:
	movl	-64(%rbp), %r10d
	subl	-80(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L231
	movl	-76(%rbp), %esi
	movl	-80(%rbp), %eax
	testl	%esi, %esi
	jns	.L149
	movq	-88(%rbp), %rdi
	movl	%r13d, -104(%rbp)
	movl	%esi, %r13d
	.p2align 4,,10
	.p2align 3
.L154:
	movslq	%eax, %r15
	leaq	(%r15,%r15), %rsi
	leaq	(%rdi,%rsi), %r8
	movzwl	(%r8), %edx
	cmpl	%eax, %r13d
	jg	.L152
	testw	%dx, %dx
	je	.L232
.L152:
	leal	1(%rax), %ecx
	andl	$64512, %edx
	leaq	2(%rdi,%rsi), %r8
	movslq	%ecx, %r15
	cmpl	$55296, %edx
	jne	.L178
	cmpl	%ecx, %r13d
	jne	.L233
.L178:
	movl	%ecx, %eax
.L153:
	subl	$1, %r10d
	jne	.L154
	movl	-104(%rbp), %r13d
	jmp	.L147
.L233:
	movzwl	(%r8), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L178
	addl	$2, %eax
	leaq	4(%rdi,%rsi), %r8
	movslq	%eax, %r15
	jmp	.L153
.L180:
	movl	%edx, %eax
.L155:
	subl	$1, %r10d
	je	.L147
.L149:
	movq	-88(%rbp), %rsi
	movslq	%eax, %r15
	leaq	(%r15,%r15), %rcx
	leaq	(%rsi,%rcx), %r8
	cmpl	%eax, -76(%rbp)
	jle	.L151
	movzwl	(%r8), %esi
	leal	1(%rax), %edx
	movq	-88(%rbp), %r8
	movslq	%edx, %r15
	andl	$-1024, %esi
	leaq	2(%r8,%rcx), %r8
	cmpl	$55296, %esi
	sete	%dil
	cmpl	%edx, -76(%rbp)
	setne	%sil
	testb	%sil, %dil
	je	.L180
	movzwl	(%r8), %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L180
	movq	-88(%rbp), %rsi
	addl	$2, %eax
	movslq	%eax, %r15
	leaq	4(%rsi,%rcx), %r8
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L226:
	movl	$2, -96(%rbp)
	jmp	.L145
.L230:
	movzwl	-72(%rbp), %eax
	movw	%ax, (%r8)
	jmp	.L160
.L232:
	movl	-104(%rbp), %r13d
	jmp	.L151
.L231:
	movslq	-80(%rbp), %r15
	leaq	(%r15,%r15), %r8
	movq	%r15, %rax
	addq	-88(%rbp), %r8
	jmp	.L147
.L163:
	movzwl	-72(%rbp), %eax
	movw	%ax, (%r8)
	jmp	.L220
	.cfi_endproc
.LFE2058:
	.size	u_strFromPunycode_67, .-u_strFromPunycode_67
	.section	.rodata
	.align 32
	.type	_ZL12basicToDigit, @object
	.size	_ZL12basicToDigit, 256
_ZL12basicToDigit:
	.string	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\032\033\034\035\036\037 !\"#\377\377\377\377\377\377\377"
	.string	"\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022\023\024\025\026\027\030\031\377\377\377\377\377\377"
	.ascii	"\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022"
	.ascii	"\023\024\025\026\027\030\031\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
