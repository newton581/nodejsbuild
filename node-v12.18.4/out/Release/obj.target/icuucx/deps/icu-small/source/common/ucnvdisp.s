	.file	"ucnvdisp.cpp"
	.text
	.p2align 4
	.globl	ucnv_getDisplayName_67
	.type	ucnv_getDisplayName_67, @function
ucnv_getDisplayName_67:
.LFB2366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	testq	%r8, %r8
	je	.L18
	movq	%r8, %r12
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L18
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L5
	movl	%ecx, %r13d
	testl	%ecx, %ecx
	js	.L5
	movq	%rdx, %r14
	jle	.L6
	testq	%rdx, %rdx
	jne	.L6
.L5:
	movl	$1, (%r12)
.L18:
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L20
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	xorl	%edi, %edi
	movq	%r12, %rdx
	call	ures_open_67@PLT
	movl	(%r12), %ecx
	movq	%rax, %rdi
	testl	%ecx, %ecx
	jg	.L18
	movq	48(%rbx), %rax
	leaq	-60(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	movq	%rdi, -72(%rbp)
	movq	16(%rax), %rsi
	addq	$4, %rsi
	call	ures_getStringByKey_67@PLT
	movq	-72(%rbp), %rdi
	movq	%rax, %r15
	call	ures_close_67@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L7
	movl	(%r12), %edx
	testl	%edx, %edx
	jne	.L8
	movl	%eax, (%r12)
.L8:
	movl	-64(%rbp), %edi
	movl	%r13d, %esi
	call	uprv_min_67@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	leal	(%rax,%rax), %edx
	call	u_memcpy_67@PLT
.L9:
	movl	-64(%rbp), %edx
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	u_terminateUChars_67@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	movq	48(%rbx), %rax
	movq	16(%rax), %rdi
	addq	$4, %rdi
	call	strlen@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	movl	%eax, -64(%rbp)
	call	uprv_min_67@PLT
	movq	%r14, %rsi
	movl	%eax, %edx
	movq	48(%rbx), %rax
	movq	16(%rax), %rdi
	addq	$4, %rdi
	call	u_charsToUChars_67@PLT
	jmp	.L9
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2366:
	.size	ucnv_getDisplayName_67, .-ucnv_getDisplayName_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
