	.file	"locdistance.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_17cleanupEv, @function
_ZN6icu_6712_GLOBAL__N_17cleanupEv:
.LFB3111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZN6icu_6712_GLOBAL__N_115gLocaleDistanceE(%rip), %r12
	testq	%r12, %r12
	je	.L2
	leaq	8(%r12), %rdi
	call	_ZN6icu_679BytesTrieD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L2:
	movq	$0, _ZN6icu_6712_GLOBAL__N_115gLocaleDistanceE(%rip)
	movl	$1, %eax
	movl	$0, _ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip)
	mfence
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3111:
	.size	_ZN6icu_6712_GLOBAL__N_17cleanupEv, .-_ZN6icu_6712_GLOBAL__N_17cleanupEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714LocaleDistance24getDesSuppScriptDistanceERNS_9BytesTrieEmPKcS4_
	.type	_ZN6icu_6714LocaleDistance24getDesSuppScriptDistanceERNS_9BytesTrieEmPKcS4_, @function
_ZN6icu_6714LocaleDistance24getDesSuppScriptDistanceERNS_9BytesTrieEmPKcS4_:
.LFB3121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	(%rdx), %r15d
	testb	%r15b, %r15b
	jne	.L9
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rbx, %rdi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movq	-56(%rbp), %rdx
	testb	$1, %al
	je	.L13
.L9:
	movzbl	%r15b, %esi
	movzbl	1(%rdx), %r15d
	addq	$1, %rdx
	movq	%rdx, -56(%rbp)
	testb	%r15b, %r15b
	jne	.L36
	orl	$-128, %esi
	movq	%rbx, %rdi
	movzbl	%sil, %esi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	testb	$1, %al
	je	.L13
	movzbl	0(%r13), %r15d
	testb	%r15b, %r15b
	je	.L13
	movq	%r13, %rdx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rbx, %rdi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movq	-56(%rbp), %rdx
	testb	$1, %al
	je	.L13
.L15:
	movzbl	%r15b, %esi
	movzbl	1(%rdx), %r15d
	addq	$1, %rdx
	movq	%rdx, -56(%rbp)
	testb	%r15b, %r15b
	jne	.L37
	orl	$-128, %esi
	movq	%rbx, %rdi
	movzbl	%sil, %esi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movl	%eax, %r15d
	cmpl	$1, %eax
	jle	.L13
	movq	16(%rbx), %rdi
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	sarl	%esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	movl	%eax, %edx
	orb	$1, %dh
	cmpl	$2, %r15d
	cmove	%edx, %eax
	testl	%eax, %eax
	jns	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	movabsq	$576460752303423487, %rsi
	movq	%r14, %rax
	movq	%rbx, %rdi
	shrq	$59, %rax
	andq	%rsi, %r14
	addq	8(%rbx), %r14
	movl	$42, %esi
	subl	$2, %eax
	movq	%r14, 16(%rbx)
	movl	%eax, 24(%rbx)
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L38
.L10:
	movl	%eax, %edx
	orb	$1, %dh
	cmpl	$2, %r14d
	cmove	%edx, %eax
.L8:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	sarl	%esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	jmp	.L10
	.cfi_endproc
.LFE3121:
	.size	_ZN6icu_6714LocaleDistance24getDesSuppScriptDistanceERNS_9BytesTrieEmPKcS4_, .-_ZN6icu_6714LocaleDistance24getDesSuppScriptDistanceERNS_9BytesTrieEmPKcS4_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714LocaleDistance27getRegionPartitionsDistanceERNS_9BytesTrieEmPKcS4_i
	.type	_ZN6icu_6714LocaleDistance27getRegionPartitionsDistanceERNS_9BytesTrieEmPKcS4_i, @function
_ZN6icu_6714LocaleDistance27getRegionPartitionsDistanceERNS_9BytesTrieEmPKcS4_i:
.LFB3122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$72, %rsp
	movq	%rcx, -96(%rbp)
	addq	$1, %rcx
	movq	%rcx, -104(%rbp)
	movzbl	(%rbx), %ecx
	movzbl	1(%rbx), %ebx
	movq	%rax, -64(%rbp)
	movq	%rsi, -80(%rbp)
	movzbl	(%rdx), %eax
	movb	%bl, -86(%rbp)
	orb	1(%rdx), %bl
	movl	%r8d, -52(%rbp)
	je	.L40
	movq	%rsi, %rdx
	movb	%cl, -85(%rbp)
	xorl	%r14d, %r14d
	movabsq	$576460752303423487, %r13
	shrq	$59, %rdx
	leal	-2(%rdx), %ebx
	movl	%ebx, -84(%rbp)
	xorl	%ebx, %ebx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L43:
	testb	%r14b, %r14b
	je	.L64
.L48:
	addq	$1, -64(%rbp)
	movq	-64(%rbp), %rax
	movzbl	-1(%rax), %eax
	testb	%al, %al
	je	.L52
	movl	-84(%rbp), %edx
	movq	-80(%rbp), %rcx
	movl	%edx, 24(%r15)
	movq	-96(%rbp), %rdx
	andq	%r13, %rcx
	addq	8(%r15), %rcx
	movq	%rcx, 16(%r15)
	movzbl	(%rdx), %edx
	movb	%dl, -85(%rbp)
.L41:
	movsbl	%al, %esi
	movq	%r15, %rdi
	orb	$-128, %sil
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	testb	$1, %al
	je	.L43
	cmpb	$0, -86(%rbp)
	je	.L50
	movl	24(%r15), %eax
	movq	16(%r15), %rsi
	subq	8(%r15), %rsi
	addl	$2, %eax
	salq	$59, %rax
	orq	%rsi, %rax
	movq	%rax, -72(%rbp)
	shrq	$59, %rax
	subl	$2, %eax
	movl	%eax, -56(%rbp)
.L44:
	movq	%r15, %rax
	movq	-104(%rbp), %r12
	movl	%r14d, %r15d
	movzbl	-85(%rbp), %ecx
	movq	%rax, %r14
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%eax, %eax
	testb	%r15b, %r15b
	je	.L65
.L46:
	cmpl	-52(%rbp), %eax
	jg	.L39
.L66:
	cmpl	%eax, %ebx
	cmovl	%eax, %ebx
	movzbl	(%r12), %ecx
	addq	$1, %r12
	testb	%cl, %cl
	je	.L60
	movl	-56(%rbp), %eax
	movl	%eax, 24(%r14)
	movq	-72(%rbp), %rax
	andq	%r13, %rax
	addq	8(%r14), %rax
	movq	%rax, 16(%r14)
.L49:
	movsbl	%cl, %esi
	movq	%r14, %rdi
	orb	$-128, %sil
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	cmpl	$1, %eax
	jle	.L45
	movq	16(%r14), %rdi
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	sarl	%esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	cmpl	-52(%rbp), %eax
	jle	.L66
.L39:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movl	-84(%rbp), %eax
	movl	$42, %esi
	movq	%r14, %rdi
	movl	$1, %r15d
	movl	%eax, 24(%r14)
	movq	-80(%rbp), %rax
	andq	%r13, %rax
	addq	8(%r14), %rax
	movq	%rax, 16(%r14)
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movq	16(%r14), %rdi
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	sarl	%esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L64:
	movl	-84(%rbp), %eax
	movl	$42, %esi
	movq	%r15, %rdi
	movl	%eax, 24(%r15)
	movq	-80(%rbp), %rax
	andq	%r13, %rax
	addq	8(%r15), %rax
	movq	%rax, 16(%r15)
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movq	16(%r15), %rdi
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	sarl	%esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	cmpl	%eax, -52(%rbp)
	jl	.L39
	cmpl	%eax, %ebx
	movl	$1, %r14d
	cmovl	%eax, %ebx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$-2, -56(%rbp)
	movq	$0, -72(%rbp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r14, %rax
	movl	%r15d, %r14d
	movq	%rax, %r15
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L40:
	movsbl	%al, %esi
	movb	%cl, -52(%rbp)
	orb	$-128, %sil
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	testb	$1, %al
	je	.L42
	movsbl	-52(%rbp), %esi
	movq	%r15, %rdi
	orb	$-128, %sil
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	cmpl	$1, %eax
	jg	.L63
.L42:
	movq	-80(%rbp), %rbx
	movl	$42, %esi
	movq	%r15, %rdi
	movq	%rbx, %rax
	shrq	$59, %rax
	subl	$2, %eax
	movl	%eax, 24(%r15)
	movabsq	$576460752303423487, %rax
	andq	%rax, %rbx
	movq	8(%r15), %rax
	addq	%rbx, %rax
	movq	%rax, 16(%r15)
	call	_ZN6icu_679BytesTrie4nextEi@PLT
.L63:
	movq	16(%r15), %rax
	movzbl	(%rax), %esi
	addq	$72, %rsp
	leaq	1(%rax), %rdi
	popq	%rbx
	popq	%r12
	sarl	%esi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	addq	$72, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3122:
	.size	_ZN6icu_6714LocaleDistance27getRegionPartitionsDistanceERNS_9BytesTrieEmPKcS4_i, .-_ZN6icu_6714LocaleDistance27getRegionPartitionsDistanceERNS_9BytesTrieEmPKcS4_i
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection
	.type	_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection, @function
_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection:
.LFB3120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$152, %rsp
	movq	%rsi, -120(%rbp)
	movq	(%rsi), %r12
	movq	%rdi, -168(%rbp)
	movq	%rdx, -144(%rbp)
	movl	%r8d, -124(%rbp)
	movl	%r9d, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	24(%rdi), %rax
	movq	%rax, -80(%rbp)
	movl	32(%rdi), %eax
	movl	%eax, -72(%rbp)
	movzbl	(%r12), %r13d
	testb	%r13b, %r13b
	jne	.L68
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r14, %rdi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	testb	$1, %al
	je	.L71
.L68:
	addq	$1, %r12
	movzbl	%r13b, %esi
	movzbl	(%r12), %r13d
	testb	%r13b, %r13b
	jne	.L140
	orl	$-128, %esi
	movq	%r14, %rdi
	movzbl	%sil, %esi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	testb	$1, %al
	je	.L71
	cmpl	$1, %ebx
	jle	.L101
	movl	-72(%rbp), %eax
	movq	-80(%rbp), %rdx
	movl	$0, -148(%rbp)
	subq	-88(%rbp), %rdx
	addl	$2, %eax
	salq	$59, %rax
	orq	%rdx, %rax
	movq	%rax, -176(%rbp)
.L72:
	movq	-176(%rbp), %rax
	movl	$-1, -188(%rbp)
	movl	$-1, -180(%rbp)
	shrq	$59, %rax
	subl	$2, %eax
	movl	%eax, -184(%rbp)
	leal	-1(%rbx), %eax
	xorl	%ebx, %ebx
	movq	%rax, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L98:
	movq	-144(%rbp), %rax
	movl	-148(%rbp), %r11d
	movl	%ebx, -132(%rbp)
	movq	(%rax,%rbx,8), %r13
	movq	0(%r13), %r12
	testl	%r11d, %r11d
	jne	.L74
	testq	%rbx, %rbx
	je	.L75
	movl	-184(%rbp), %eax
	movl	%eax, -72(%rbp)
	movabsq	$576460752303423487, %rax
	andq	-176(%rbp), %rax
	addq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
.L75:
	movzbl	(%r12), %r15d
	testb	%r15b, %r15b
	jne	.L77
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r14, %rdi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	testb	$1, %al
	je	.L139
.L77:
	addq	$1, %r12
	movzbl	%r15b, %esi
	movzbl	(%r12), %r15d
	testb	%r15b, %r15b
	jne	.L141
	orl	$-128, %esi
	movq	%r14, %rdi
	movzbl	%sil, %esi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movl	%eax, %r12d
	cmpl	$1, %eax
	jle	.L139
	movq	-80(%rbp), %rdi
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	sarl	%esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	movl	%eax, %r9d
	orb	$1, %ah
	cmpl	$2, %r12d
	cmove	%eax, %r9d
	testl	%r9d, %r9d
	js	.L139
	movb	$0, -128(%rbp)
	movl	%r9d, %edx
	andl	$-385, %r9d
	andl	$384, %edx
	movl	%r9d, %r15d
.L81:
	movl	-124(%rbp), %eax
	leal	7(%rax), %r12d
	movl	%r15d, %eax
	sarl	$2, %eax
	sarl	$3, %r12d
	cmpl	$1, -136(%rbp)
	cmove	%eax, %r15d
	cmpl	%r12d, %r15d
	jg	.L83
	movq	-120(%rbp), %rax
	movq	8(%r13), %r8
	movq	8(%rax), %rdi
	testl	%edx, %edx
	jne	.L107
	cmpb	$0, -128(%rbp)
	jne	.L107
	movl	-72(%rbp), %eax
	movq	%rdi, %rdx
	movq	%r8, %rcx
	movq	%r14, %rdi
	leal	2(%rax), %esi
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	salq	$59, %rsi
	orq	%rax, %rsi
	call	_ZN6icu_6714LocaleDistance24getDesSuppScriptDistanceERNS_9BytesTrieEmPKcS4_
	movl	%eax, %edx
	andb	$-2, %ah
	andl	$256, %edx
.L87:
	addl	%eax, %r15d
	cmpl	%r15d, %r12d
	jl	.L83
.L86:
	movq	-120(%rbp), %rax
	movq	16(%r13), %rsi
	movl	%edx, -152(%rbp)
	movq	16(%rax), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L88
	cmpb	$0, -128(%rbp)
	jne	.L89
	movl	-152(%rbp), %edx
	andb	$1, %dh
	jne	.L89
	movq	-168(%rbp), %rax
	subl	%r15d, %r12d
	cmpl	%r12d, 80(%rax)
	jle	.L142
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	1(%rbx), %rax
	cmpq	%rbx, -160(%rbp)
	je	.L143
.L106:
	movq	%rax, %rbx
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$-1, -148(%rbp)
.L69:
	movq	$0, -176(%rbp)
	testl	%ebx, %ebx
	jg	.L72
.L99:
	movl	$-224, %r12d
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r8, %rsi
	movl	%edx, -152(%rbp)
	call	strcmp@PLT
	movl	-152(%rbp), %edx
	testl	%eax, %eax
	je	.L86
	movq	-168(%rbp), %rax
	movl	72(%rax), %eax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L142:
	movq	-120(%rbp), %rdi
	movq	40(%rax), %rdx
	movl	%r12d, %r8d
	movslq	32(%r13), %rcx
	movq	48(%rax), %rax
	movslq	32(%rdi), %rsi
	movq	%r14, %rdi
	movzbl	(%rdx,%rcx), %ecx
	movzbl	(%rdx,%rsi), %edx
	movq	(%rax,%rcx,8), %rcx
	movq	(%rax,%rdx,8), %rdx
	movl	-72(%rbp), %eax
	leal	2(%rax), %esi
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	salq	$59, %rsi
	orq	%rax, %rsi
	call	_ZN6icu_6714LocaleDistance27getRegionPartitionsDistanceERNS_9BytesTrieEmPKcS4_i
	addl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L88:
	sall	$3, %r15d
	movl	%r15d, %r12d
	jne	.L91
	movq	-120(%rbp), %rax
	movl	36(%r13), %r15d
	movl	36(%rax), %r12d
	movl	%r12d, %r10d
	xorl	%r15d, %r10d
	cmpl	-124(%rbp), %r10d
	jge	.L83
	cmpl	$1, 16(%rbp)
	je	.L92
.L94:
	cmpl	%r15d, %r12d
	je	.L93
	movl	-132(%rbp), %eax
	movl	%r10d, -124(%rbp)
	movl	$-1, -188(%rbp)
	movl	%eax, -180(%rbp)
	leaq	1(%rbx), %rax
	cmpq	%rbx, -160(%rbp)
	jne	.L106
	.p2align 4,,10
	.p2align 3
.L143:
	movl	-180(%rbp), %eax
	cmpl	$-1, %eax
	je	.L99
	movl	-124(%rbp), %r12d
	sall	$10, %eax
	orl	%eax, %r12d
.L73:
	movq	%r14, %rdi
	call	_ZN6icu_679BytesTrieD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	0(%r13), %r12
.L74:
	movq	-120(%rbp), %rax
	movq	%r12, %rsi
	movq	(%rax), %rdi
	call	strcmp@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L103
	movq	-168(%rbp), %rax
	movb	$1, -128(%rbp)
	xorl	%edx, %edx
	movl	68(%rax), %r15d
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L91:
	cmpl	%r15d, -124(%rbp)
	jle	.L95
	cmpl	$1, 16(%rbp)
	je	.L145
	movl	-132(%rbp), %eax
	movl	%r15d, -124(%rbp)
	movl	$-1, -188(%rbp)
	movl	%eax, -180(%rbp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L103:
	movb	$1, -128(%rbp)
	xorl	%edx, %edx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L89:
	movq	-168(%rbp), %rax
	addl	76(%rax), %r15d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L95:
	jne	.L83
	movl	-180(%rbp), %esi
	testl	%esi, %esi
	js	.L83
	cmpl	$1, 16(%rbp)
	je	.L96
.L97:
	movslq	-180(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%r13, %rsi
	movq	(%rcx,%rax,8), %rdx
	movq	%rax, %r15
	movq	-168(%rbp), %rax
	movl	-188(%rbp), %ecx
	movq	(%rax), %rdi
	call	_ZNK6icu_6714XLikelySubtags13compareLikelyERKNS_3LSRES3_i@PLT
	testb	$1, %al
	movl	%eax, -188(%rbp)
	movl	-132(%rbp), %eax
	cmove	%r15d, %eax
	movl	%eax, -180(%rbp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L92:
	subq	$8, %rsp
	movl	-124(%rbp), %r8d
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	pushq	$0
	movl	-136(%rbp), %r9d
	movl	$1, %ecx
	movq	-168(%rbp), %rdi
	movl	%r10d, -128(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection
	popq	%r9
	popq	%r10
	testl	%eax, %eax
	movl	-128(%rbp), %r10d
	jns	.L94
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L145:
	subq	$8, %rsp
	movl	-124(%rbp), %r15d
	movq	-120(%rbp), %rax
	leaq	-104(%rbp), %rdx
	pushq	$0
	movq	-168(%rbp), %rdi
	movl	$1, %ecx
	movq	%r13, %rsi
	movl	-136(%rbp), %r9d
	movl	%r15d, %r8d
	movq	%rax, -104(%rbp)
	call	_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection
	movl	-132(%rbp), %ecx
	popq	%rdi
	testl	%eax, %eax
	movl	$-1, %eax
	cmovs	-180(%rbp), %ecx
	popq	%r8
	cmovs	%r15d, %r12d
	cmovs	-188(%rbp), %eax
	movl	%ecx, -180(%rbp)
	movl	%r12d, -124(%rbp)
	movl	%eax, -188(%rbp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L96:
	subq	$8, %rsp
	movq	-120(%rbp), %rax
	movl	-124(%rbp), %r8d
	leaq	-104(%rbp), %rdx
	pushq	$0
	movl	-136(%rbp), %r9d
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	-168(%rbp), %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jns	.L97
	jmp	.L83
.L93:
	movl	-132(%rbp), %r12d
	sall	$10, %r12d
	jmp	.L73
.L101:
	movl	$0, -148(%rbp)
	jmp	.L69
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3120:
	.size	_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection, .-_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"en"
.LC1:
	.string	"Latn"
.LC2:
	.string	"US"
.LC3:
	.string	"GB"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714LocaleDistanceC2ERKNS_18LocaleDistanceDataERKNS_14XLikelySubtagsE
	.type	_ZN6icu_6714LocaleDistanceC2ERKNS_18LocaleDistanceDataERKNS_14XLikelySubtagsE, @function
_ZN6icu_6714LocaleDistanceC2ERKNS_18LocaleDistanceDataERKNS_14XLikelySubtagsE:
.LFB3118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-144(%rbp), %r12
	leaq	-96(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rax
	movq	%rdx, (%rdi)
	movq	%rax, %xmm1
	movq	(%rsi), %rax
	movl	$-1, 32(%rdi)
	movq	$0, 8(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 16(%rdi)
	movq	%rax, 24(%rdi)
	movq	24(%rsi), %rax
	movdqu	8(%rsi), %xmm2
	movaps	%xmm0, -176(%rbp)
	movq	%rax, 56(%rdi)
	movl	32(%rsi), %eax
	movups	%xmm2, 40(%rdi)
	movl	%eax, 64(%rdi)
	movq	40(%rsi), %rax
	movl	(%rax), %edx
	movl	%edx, 68(%rdi)
	movl	4(%rax), %edx
	movl	%edx, 72(%rdi)
	movl	8(%rax), %edx
	movl	%edx, 76(%rdi)
	movl	12(%rax), %eax
	movaps	%xmm0, -144(%rbp)
	movl	%eax, 80(%rdi)
	leaq	.LC2(%rip), %rdi
	movq	%rdi, -128(%rbp)
	movq	$0, -120(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movdqa	-176(%rbp), %xmm0
	leaq	.LC3(%rip), %rdi
	movq	$7, -108(%rbp)
	movq	%rdi, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -112(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r12, %rsi
	pushq	$0
	movl	$400, %r8d
	movl	$1, %ecx
	movq	%rbx, %rdi
	leaq	-152(%rbp), %rdx
	movl	%eax, -64(%rbp)
	movq	$7, -60(%rbp)
	movq	%r13, -152(%rbp)
	call	_ZNK6icu_6714LocaleDistance23getBestIndexAndDistanceERKNS_3LSREPPS2_ii20ULocMatchFavorSubtag18ULocMatchDirection
	sarl	$3, %eax
	andl	$127, %eax
	cmpq	$0, -72(%rbp)
	movl	%eax, 84(%rbx)
	popq	%rax
	popq	%rdx
	je	.L147
	movq	%r13, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
.L147:
	cmpq	$0, -120(%rbp)
	je	.L146
	movq	%r12, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
.L146:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L151
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L151:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3118:
	.size	_ZN6icu_6714LocaleDistanceC2ERKNS_18LocaleDistanceDataERKNS_14XLikelySubtagsE, .-_ZN6icu_6714LocaleDistanceC2ERKNS_18LocaleDistanceDataERKNS_14XLikelySubtagsE
	.globl	_ZN6icu_6714LocaleDistanceC1ERKNS_18LocaleDistanceDataERKNS_14XLikelySubtagsE
	.set	_ZN6icu_6714LocaleDistanceC1ERKNS_18LocaleDistanceDataERKNS_14XLikelySubtagsE,_ZN6icu_6714LocaleDistanceC2ERKNS_18LocaleDistanceDataERKNS_14XLikelySubtagsE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714LocaleDistance18initLocaleDistanceER10UErrorCode
	.type	_ZN6icu_6714LocaleDistance18initLocaleDistanceER10UErrorCode, @function
_ZN6icu_6714LocaleDistance18initLocaleDistanceER10UErrorCode:
.LFB3115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6714XLikelySubtags12getSingletonER10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L152
	cmpq	$0, 304(%rax)
	movq	%rax, %r12
	je	.L154
	cmpq	$0, 312(%rax)
	je	.L154
	cmpq	$0, 320(%rax)
	je	.L154
	cmpq	$0, 344(%rax)
	je	.L154
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L156
	leaq	304(%r12), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6714LocaleDistanceC1ERKNS_18LocaleDistanceDataERKNS_14XLikelySubtagsE
	movq	%r13, _ZN6icu_6712_GLOBAL__N_115gLocaleDistanceE(%rip)
	addq	$8, %rsp
	leaq	_ZN6icu_6712_GLOBAL__N_17cleanupEv(%rip), %rsi
	popq	%rbx
	movl	$9, %edi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ucln_common_registerCleanup_67@PLT
.L156:
	.cfi_restore_state
	movq	$0, _ZN6icu_6712_GLOBAL__N_115gLocaleDistanceE(%rip)
	movl	$7, (%rbx)
.L152:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movl	$2, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3115:
	.size	_ZN6icu_6714LocaleDistance18initLocaleDistanceER10UErrorCode, .-_ZN6icu_6714LocaleDistance18initLocaleDistanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714LocaleDistance12getSingletonER10UErrorCode
	.type	_ZN6icu_6714LocaleDistance12getSingletonER10UErrorCode, @function
_ZN6icu_6714LocaleDistance12getSingletonER10UErrorCode:
.LFB3116:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L169
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L174
.L160:
	movl	4+_ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L161
	movl	%eax, (%rbx)
.L161:
	movq	_ZN6icu_6712_GLOBAL__N_115gLocaleDistanceE(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	_ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L160
	movq	%rbx, %rdi
	call	_ZN6icu_6714LocaleDistance18initLocaleDistanceER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L161
	.cfi_endproc
.LFE3116:
	.size	_ZN6icu_6714LocaleDistance12getSingletonER10UErrorCode, .-_ZN6icu_6714LocaleDistance12getSingletonER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714LocaleDistance25getFallbackRegionDistanceERNS_9BytesTrieEm
	.type	_ZN6icu_6714LocaleDistance25getFallbackRegionDistanceERNS_9BytesTrieEm, @function
_ZN6icu_6714LocaleDistance25getFallbackRegionDistanceERNS_9BytesTrieEm:
.LFB3123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	shrq	$59, %rax
	subl	$2, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	%eax, 24(%rdi)
	movabsq	$576460752303423487, %rax
	andq	%rax, %rsi
	addq	8(%rdi), %rsi
	movq	%rsi, 16(%rdi)
	movl	$42, %esi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movq	16(%rbx), %rdi
	movzbl	(%rdi), %esi
	addq	$8, %rsp
	addq	$1, %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	sarl	%esi
	jmp	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	.cfi_endproc
.LFE3123:
	.size	_ZN6icu_6714LocaleDistance25getFallbackRegionDistanceERNS_9BytesTrieEm, .-_ZN6icu_6714LocaleDistance25getFallbackRegionDistanceERNS_9BytesTrieEm
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714LocaleDistance8trieNextERNS_9BytesTrieEPKcb
	.type	_ZN6icu_6714LocaleDistance8trieNextERNS_9BytesTrieEPKcb, @function
_ZN6icu_6714LocaleDistance8trieNextERNS_9BytesTrieEPKcb:
.LFB3124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzbl	(%rsi), %r14d
	testb	%r14b, %r14b
	je	.L178
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movl	%edx, %r12d
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%rbx, %rdi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	testb	$1, %al
	je	.L178
.L180:
	addq	$1, %r13
	movzbl	%r14b, %esi
	movzbl	0(%r13), %r14d
	testb	%r14b, %r14b
	jne	.L191
	orl	$-128, %esi
	movq	%rbx, %rdi
	movzbl	%sil, %esi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movl	%eax, %r13d
	testb	%r12b, %r12b
	je	.L181
	cmpl	$1, %eax
	jle	.L178
	movq	16(%rbx), %rdi
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	sarl	%esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	popq	%rbx
	popq	%r12
	movl	%eax, %edx
	orb	$1, %dh
	cmpl	$2, %r13d
	popq	%r13
	popq	%r14
	cmove	%edx, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	andl	$1, %r13d
	je	.L178
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3124:
	.size	_ZN6icu_6714LocaleDistance8trieNextERNS_9BytesTrieEPKcb, .-_ZN6icu_6714LocaleDistance8trieNextERNS_9BytesTrieEPKcb
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714LocaleDistance13isParadigmLSRERKNS_3LSRE
	.type	_ZNK6icu_6714LocaleDistance13isParadigmLSRERKNS_3LSRE, @function
_ZNK6icu_6714LocaleDistance13isParadigmLSRERKNS_3LSRE:
.LFB3125:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	testl	%eax, %eax
	jle	.L199
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L203:
	addq	$1, %rbx
	cmpl	%ebx, 64(%r13)
	jle	.L202
.L195:
	leaq	(%rbx,%rbx,2), %rsi
	movq	%r12, %rdi
	salq	$4, %rsi
	addq	56(%r13), %rsi
	call	_ZNK6icu_673LSR14isEquivalentToERKS0_@PLT
	testb	%al, %al
	je	.L203
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L199:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3125:
	.size	_ZNK6icu_6714LocaleDistance13isParadigmLSRERKNS_3LSRE, .-_ZNK6icu_6714LocaleDistance13isParadigmLSRERKNS_3LSRE
	.local	_ZN6icu_6712_GLOBAL__N_19gInitOnceE
	.comm	_ZN6icu_6712_GLOBAL__N_19gInitOnceE,8,8
	.local	_ZN6icu_6712_GLOBAL__N_115gLocaleDistanceE
	.comm	_ZN6icu_6712_GLOBAL__N_115gLocaleDistanceE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
