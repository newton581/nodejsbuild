	.file	"lsr.cpp"
	.text
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2567:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2567:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2570:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	cmpb	$0, 12(%rbx)
	jne	.L16
.L7:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L7
	.cfi_endproc
.LFE2570:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2573:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2573:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2576:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L22
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2576:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L28
.L24:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L29
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2578:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2579:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2579:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2580:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2580:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2581:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2581:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2582:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2582:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2583:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2583:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2584:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L45
	testl	%edx, %edx
	jle	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L48
.L37:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L37
	.cfi_endproc
.LFE2584:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L52
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L52
	testl	%r12d, %r12d
	jg	.L59
	cmpb	$0, 12(%rbx)
	jne	.L60
.L54:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L54
.L60:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2585:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L62
	movq	(%rdi), %r8
.L63:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L66
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L66
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2586:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2587:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2587:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2588:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2588:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2589:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2589:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2590:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2590:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2592:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2592:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2594:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2594:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_673LSRC2EcPKcS2_S2_iR10UErrorCode
	.type	_ZN6icu_673LSRC2EcPKcS2_S2_iR10UErrorCode, @function
_ZN6icu_673LSRC2EcPKcS2_S2_iR10UErrorCode:
.LFB2320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movsbl	(%r8), %eax
	movq	%r8, 16(%rdi)
	movq	$0, 24(%rdi)
	leal	-48(%rax), %ecx
	movups	%xmm0, (%rdi)
	cmpl	$9, %ecx
	ja	.L80
	movsbl	1(%r8), %eax
	xorl	%edx, %edx
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L81
	movsbl	2(%r8), %edi
	subl	$48, %edi
	cmpl	$9, %edi
	ja	.L81
	cmpb	$0, 3(%r8)
	je	.L93
	.p2align 4,,10
	.p2align 3
.L81:
	movl	(%r14), %ecx
	movl	%edx, 32(%rbx)
	movl	%r9d, 36(%rbx)
	movl	$0, 40(%rbx)
	testl	%ecx, %ecx
	jle	.L94
.L79:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	subl	$65, %eax
	xorl	%edx, %edx
	cmpl	$25, %eax
	ja	.L81
	movsbl	1(%r8), %ecx
	subl	$65, %ecx
	cmpl	$25, %ecx
	ja	.L81
	cmpb	$0, 2(%r8)
	jne	.L81
	imull	$26, %eax, %eax
	leal	1001(%rcx,%rax), %edx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L94:
	movsbl	%sil, %r8d
	leaq	-128(%rbp), %r15
	leaq	-115(%rbp), %rax
	movq	%r14, %rdx
	movl	%r8d, %esi
	movq	%rax, -128(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rdi
	movl	%r8d, -164(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	movw	%ax, -116(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	-144(%rbp), %r9
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r9, -152(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-160(%rbp), %r10
	movl	-136(%rbp), %edx
	movq	%r14, %rcx
	movq	-144(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-72(%rbp), %eax
	movq	%r14, %rdx
	movq	%r15, %rdi
	movl	-164(%rbp), %r8d
	movl	%eax, -160(%rbp)
	movl	%r8d, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-152(%rbp), %r9
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	%r9, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-136(%rbp), %edx
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	-144(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6710CharString9cloneDataER10UErrorCode@PLT
	movl	(%r14), %edx
	movq	%rax, 24(%rbx)
	testl	%edx, %edx
	jle	.L96
	cmpb	$0, -116(%rbp)
	je	.L79
.L97:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L93:
	leal	(%rcx,%rcx,4), %edx
	leal	(%rax,%rdx,2), %eax
	leal	(%rax,%rax,4), %eax
	leal	1(%rdi,%rax,2), %edx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L96:
	movslq	-160(%rbp), %r13
	movq	%rax, (%rbx)
	addq	%r13, %rax
	cmpb	$0, -116(%rbp)
	movq	%rax, 8(%rbx)
	je	.L79
	jmp	.L97
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2320:
	.size	_ZN6icu_673LSRC2EcPKcS2_S2_iR10UErrorCode, .-_ZN6icu_673LSRC2EcPKcS2_S2_iR10UErrorCode
	.globl	_ZN6icu_673LSRC1EcPKcS2_S2_iR10UErrorCode
	.set	_ZN6icu_673LSRC1EcPKcS2_S2_iR10UErrorCode,_ZN6icu_673LSRC2EcPKcS2_S2_iR10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_673LSRC2EOS0_
	.type	_ZN6icu_673LSRC2EOS0_, @function
_ZN6icu_673LSRC2EOS0_:
.LFB2323:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm1
	movdqu	16(%rsi), %xmm2
	movl	40(%rsi), %eax
	movq	32(%rsi), %rdx
	cmpq	$0, 24(%rsi)
	movups	%xmm1, (%rdi)
	movq	%rdx, 32(%rdi)
	movl	%eax, 40(%rdi)
	movups	%xmm2, 16(%rdi)
	je	.L98
	leaq	.LC0(%rip), %rax
	movq	$0, 24(%rsi)
	movq	%rax, %xmm0
	movl	$0, 40(%rsi)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
.L98:
	ret
	.cfi_endproc
.LFE2323:
	.size	_ZN6icu_673LSRC2EOS0_, .-_ZN6icu_673LSRC2EOS0_
	.globl	_ZN6icu_673LSRC1EOS0_
	.set	_ZN6icu_673LSRC1EOS0_,_ZN6icu_673LSRC2EOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_673LSR11deleteOwnedEv
	.type	_ZN6icu_673LSR11deleteOwnedEv, @function
_ZN6icu_673LSR11deleteOwnedEv:
.LFB2325:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2325:
	.size	_ZN6icu_673LSR11deleteOwnedEv, .-_ZN6icu_673LSR11deleteOwnedEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_673LSRaSEOS0_
	.type	_ZN6icu_673LSRaSEOS0_, @function
_ZN6icu_673LSRaSEOS0_:
.LFB2326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rdi
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L105
	call	uprv_free_67@PLT
.L105:
	movq	(%rbx), %rax
	movl	40(%rbx), %edx
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	movl	32(%rbx), %eax
	movl	%eax, 32(%r12)
	movl	36(%rbx), %eax
	movl	%eax, 36(%r12)
	movq	24(%rbx), %rax
	movl	%edx, 40(%r12)
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L106
	leaq	.LC0(%rip), %rax
	movq	$0, 24(%rbx)
	movq	%rax, %xmm0
	movl	$0, 40(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
.L106:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2326:
	.size	_ZN6icu_673LSRaSEOS0_, .-_ZN6icu_673LSRaSEOS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_673LSR14isEquivalentToERKS0_
	.type	_ZNK6icu_673LSR14isEquivalentToERKS0_, @function
_ZNK6icu_673LSR14isEquivalentToERKS0_:
.LFB2327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L114
	movq	8(%r12), %rsi
	movq	8(%rbx), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L114
	movl	32(%rbx), %eax
	cmpl	32(%r12), %eax
	je	.L121
.L114:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	$1, %r13d
	testl	%eax, %eax
	jg	.L114
	movq	16(%r12), %rsi
	movq	16(%rbx), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	sete	%r13b
	jmp	.L114
	.cfi_endproc
.LFE2327:
	.size	_ZNK6icu_673LSR14isEquivalentToERKS0_, .-_ZNK6icu_673LSR14isEquivalentToERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_673LSReqERKS0_
	.type	_ZNK6icu_673LSReqERKS0_, @function
_ZNK6icu_673LSReqERKS0_:
.LFB2328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L122
	movq	8(%rbx), %rsi
	movq	8(%r12), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L122
	movl	32(%r12), %eax
	cmpl	32(%rbx), %eax
	je	.L130
.L122:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	testl	%eax, %eax
	jle	.L131
.L124:
	movl	36(%rbx), %eax
	cmpl	%eax, 36(%r12)
	sete	%r13b
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L131:
	movq	16(%rbx), %rsi
	movq	16(%r12), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L122
	jmp	.L124
	.cfi_endproc
.LFE2328:
	.size	_ZNK6icu_673LSReqERKS0_, .-_ZNK6icu_673LSReqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_673LSR14indexForRegionEPKc
	.type	_ZN6icu_673LSR14indexForRegionEPKc, @function
_ZN6icu_673LSR14indexForRegionEPKc:
.LFB2329:
	.cfi_startproc
	endbr64
	movsbl	(%rdi), %eax
	leal	-48(%rax), %edx
	cmpl	$9, %edx
	ja	.L133
	movsbl	1(%rdi), %eax
	xorl	%r8d, %r8d
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L132
	movsbl	2(%rdi), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L132
	cmpb	$0, 3(%rdi)
	je	.L141
.L132:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	subl	$65, %eax
	xorl	%r8d, %r8d
	cmpl	$25, %eax
	ja	.L132
	movsbl	1(%rdi), %edx
	subl	$65, %edx
	cmpl	$25, %edx
	ja	.L132
	cmpb	$0, 2(%rdi)
	jne	.L132
	imull	$26, %eax, %eax
	leal	1001(%rdx,%rax), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	leal	(%rdx,%rdx,4), %edx
	leal	(%rax,%rdx,2), %eax
	leal	(%rax,%rax,4), %eax
	leal	1(%rcx,%rax,2), %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2329:
	.size	_ZN6icu_673LSR14indexForRegionEPKc, .-_ZN6icu_673LSR14indexForRegionEPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_673LSR11setHashCodeEv
	.type	_ZN6icu_673LSR11setHashCodeEv, @function
_ZN6icu_673LSR11setHashCodeEv:
.LFB2330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	40(%rdi), %eax
	testl	%eax, %eax
	je	.L145
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	(%rdi), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	ustr_hashCharsN_67@PLT
	movq	8(%r12), %r13
	leal	(%rax,%rax,8), %edx
	movq	%r13, %rdi
	leal	(%rax,%rdx,4), %ebx
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	ustr_hashCharsN_67@PLT
	addl	%ebx, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	addl	32(%r12), %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	addl	36(%r12), %eax
	movl	%eax, 40(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2330:
	.size	_ZN6icu_673LSR11setHashCodeEv, .-_ZN6icu_673LSR11setHashCodeEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
