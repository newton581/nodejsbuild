	.file	"propname.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii.part.0, @function
_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii.part.0:
.LFB3834:
	.cfi_startproc
	leal	2(%rdi), %eax
	addl	$1, %edi
	leaq	_ZN6icu_6712PropNameData9valueMapsE(%rip), %r9
	movslq	%edi, %rdi
	movl	(%r9,%rdi,4), %r8d
	cmpl	$15, %r8d
	jg	.L2
	testl	%r8d, %r8d
	jg	.L5
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%r8d, %r8d
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	%esi, %edx
	jg	.L16
	subl	%ecx, %edx
	addl	%edx, %eax
	subl	$1, %r8d
	je	.L1
.L5:
	movslq	%eax, %rdx
	movl	(%r9,%rdx,4), %ecx
	leal	1(%rax), %edx
	addl	$2, %eax
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edx
	cmpl	%esi, %ecx
	jle	.L17
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L2:
	leal	-16(%rax,%r8), %ecx
	movslq	%eax, %rdx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L19:
	je	.L18
	addq	$1, %rdx
	cmpl	%edx, %ecx
	jle	.L10
.L7:
	cmpl	(%r9,%rdx,4), %esi
	jge	.L19
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L18:
	addl	%edx, %ecx
	subl	%eax, %ecx
	movslq	%ecx, %rax
	movl	(%r9,%rax,4), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	addl	%esi, %eax
	subl	%ecx, %eax
	cltq
	movl	(%r9,%rax,4), %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3834:
	.size	_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii.part.0, .-_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii.part.0
	.p2align 4
	.globl	uprv_compareASCIIPropertyNames_67
	.type	uprv_compareASCIIPropertyNames_67, @function
uprv_compareASCIIPropertyNames_67:
.LFB3023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$35188667072000, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L57:
	cmpb	$45, %dil
	jbe	.L28
	sall	$8, %ebx
.L29:
	call	uprv_asciitolower_67@PLT
	movzbl	%al, %eax
	orl	%eax, %ebx
	movl	%ebx, %eax
	orl	%r12d, %eax
	andl	$255, %eax
	je	.L20
.L58:
	cmpl	%r12d, %ebx
	je	.L32
	movzbl	%r12b, %eax
	movzbl	%bl, %edx
	subl	%edx, %eax
	jne	.L20
.L32:
	sarl	$8, %r12d
	sarl	$8, %ebx
	movslq	%r12d, %r12
	movslq	%ebx, %rbx
	addq	%r12, %r14
	addq	%rbx, %r13
.L33:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	movsbl	-1(%r14,%rax), %edi
	movl	%eax, %r12d
	cmpb	$95, %dil
	je	.L22
	cmpb	$45, %dil
	jbe	.L23
	sall	$8, %r12d
.L24:
	call	uprv_asciitolower_67@PLT
	movzbl	%al, %eax
	orl	%eax, %r12d
.L25:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L26:
	movsbl	-1(%r13,%rax), %edi
	movl	%eax, %ebx
	cmpb	$95, %dil
	jne	.L57
.L27:
	addq	$1, %rax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L28:
	btq	%rdi, %r15
	jc	.L27
	sall	$8, %ebx
	testb	%dil, %dil
	jne	.L29
	movl	%ebx, %eax
	orl	%r12d, %eax
	andl	$255, %eax
	jne	.L58
.L20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	btq	%rdi, %r15
	jnc	.L59
.L22:
	addq	$1, %rax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L59:
	sall	$8, %r12d
	testb	%dil, %dil
	je	.L25
	jmp	.L24
	.cfi_endproc
.LFE3023:
	.size	uprv_compareASCIIPropertyNames_67, .-uprv_compareASCIIPropertyNames_67
	.p2align 4
	.globl	uprv_compareEBCDICPropertyNames_67
	.type	uprv_compareEBCDICPropertyNames_67, @function
uprv_compareEBCDICPropertyNames_67:
.LFB3024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movabsq	$137441064992, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L91:
	cmpb	$37, %dil
	jbe	.L68
	sall	$8, %ebx
.L69:
	call	uprv_ebcdictolower_67@PLT
	movzbl	%al, %eax
	orl	%eax, %ebx
	movl	%ebx, %eax
	orl	%r12d, %eax
	andl	$255, %eax
	je	.L60
.L92:
	cmpl	%r12d, %ebx
	je	.L72
	movzbl	%r12b, %eax
	movzbl	%bl, %edx
	subl	%edx, %eax
	jne	.L60
.L72:
	sarl	$8, %r12d
	sarl	$8, %ebx
	movslq	%r12d, %r12
	movslq	%ebx, %rbx
	addq	%r12, %r14
	addq	%rbx, %r13
.L73:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L61:
	movsbl	-1(%r14,%rax), %edi
	movl	%eax, %r12d
	movl	%edi, %edx
	andl	$-33, %edx
	cmpb	$64, %dl
	je	.L62
	cmpb	$109, %dil
	je	.L62
	cmpb	$37, %dil
	jbe	.L63
	sall	$8, %r12d
.L64:
	call	uprv_ebcdictolower_67@PLT
	movzbl	%al, %eax
	orl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L66:
	movsbl	-1(%r13,%rax), %edi
	movl	%eax, %ebx
	movl	%edi, %edx
	andl	$-33, %edx
	cmpb	$64, %dl
	je	.L67
	cmpb	$109, %dil
	jne	.L91
.L67:
	addq	$1, %rax
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L68:
	btq	%rdi, %r15
	jc	.L67
	sall	$8, %ebx
	testb	%dil, %dil
	jne	.L69
	movl	%ebx, %eax
	orl	%r12d, %eax
	andl	$255, %eax
	jne	.L92
.L60:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	btq	%rdi, %r15
	jnc	.L93
.L62:
	addq	$1, %rax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L93:
	sall	$8, %r12d
	testb	%dil, %dil
	je	.L65
	jmp	.L64
	.cfi_endproc
.LFE3024:
	.size	uprv_compareEBCDICPropertyNames_67, .-uprv_compareEBCDICPropertyNames_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PropNameData12findPropertyEi
	.type	_ZN6icu_6712PropNameData12findPropertyEi, @function
_ZN6icu_6712PropNameData12findPropertyEi:
.LFB3025:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	js	.L105
	cmpl	$64, %edi
	jle	.L98
	cmpl	$4095, %edi
	jle	.L105
	cmpl	$4120, %edi
	jle	.L100
	cmpl	$8191, %edi
	jle	.L105
	cmpl	$8192, %edi
	je	.L102
	cmpl	$12287, %edi
	jle	.L105
	cmpl	$12288, %edi
	je	.L104
	cmpl	$16383, %edi
	jle	.L105
	cmpl	$16397, %edi
	jle	.L106
	xorl	%eax, %eax
	cmpl	$28671, %edi
	jle	.L94
	cmpl	$28672, %edi
	je	.L109
.L94:
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	xorl	%eax, %eax
	ret
.L109:
	movl	$28672, %edx
	movl	$225, %eax
.L96:
	subl	%edx, %edi
	leal	(%rax,%rdi,2), %eax
	ret
.L98:
	movl	$3, %eax
	xorl	%edx, %edx
	jmp	.L96
.L100:
	movl	$135, %eax
	movl	$4096, %edx
	jmp	.L96
.L102:
	movl	$8192, %edx
	movl	$187, %eax
	jmp	.L96
.L104:
	movl	$12288, %edx
	movl	$191, %eax
	jmp	.L96
.L106:
	movl	$195, %eax
	movl	$16384, %edx
	jmp	.L96
	.cfi_endproc
.LFE3025:
	.size	_ZN6icu_6712PropNameData12findPropertyEi, .-_ZN6icu_6712PropNameData12findPropertyEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii
	.type	_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii, @function
_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii:
.LFB3026:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	je	.L111
	jmp	_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii.part.0
	.p2align 4,,10
	.p2align 3
.L111:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3026:
	.size	_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii, .-_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PropNameData7getNameEPKci
	.type	_ZN6icu_6712PropNameData7getNameEPKci, @function
_ZN6icu_6712PropNameData7getNameEPKci:
.LFB3027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movsbl	(%rdi), %eax
	cmpl	%esi, %eax
	jle	.L116
	movl	%esi, %ebx
	testl	%esi, %esi
	js	.L116
	leaq	1(%rdi), %r12
	je	.L114
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	1(%r12,%rax), %r12
	subl	$1, %ebx
	jne	.L115
.L114:
	movl	$0, %eax
	cmpb	$0, (%r12)
	popq	%rbx
	cmove	%rax, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3027:
	.size	_ZN6icu_6712PropNameData7getNameEPKci, .-_ZN6icu_6712PropNameData7getNameEPKci
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PropNameData12containsNameERNS_9BytesTrieEPKc
	.type	_ZN6icu_6712PropNameData12containsNameERNS_9BytesTrieEPKc, @function
_ZN6icu_6712PropNameData12containsNameERNS_9BytesTrieEPKc:
.LFB3028:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L145
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	1(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movabsq	$-9223372036854767615, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	(%rsi), %eax
	movq	%rdi, %rbx
	testb	%al, %al
	jne	.L126
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L147:
	leal	-9(%rax), %edx
	cmpb	$4, %dl
	jbe	.L129
.L130:
	andl	$1, %r13d
	je	.L131
	movzbl	%al, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movl	%eax, %r13d
	movl	%eax, %ecx
.L129:
	movzbl	(%r14), %eax
	addq	$1, %r14
	testb	%al, %al
	je	.L146
.L126:
	movsbl	%al, %edi
	call	uprv_asciitolower_67@PLT
	movl	%r13d, %ecx
	leal	-32(%rax), %edx
	cmpb	$63, %dl
	ja	.L147
	btq	%rdx, %r12
	jnc	.L130
	movzbl	(%r14), %eax
	addq	$1, %r14
	testb	%al, %al
	jne	.L126
.L146:
	cmpl	$1, %ecx
	setg	%al
.L123:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3028:
	.size	_ZN6icu_6712PropNameData12containsNameERNS_9BytesTrieEPKc, .-_ZN6icu_6712PropNameData12containsNameERNS_9BytesTrieEPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PropNameData15getPropertyNameEii
	.type	_ZN6icu_6712PropNameData15getPropertyNameEii, @function
_ZN6icu_6712PropNameData15getPropertyNameEii:
.LFB3029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testl	%edi, %edi
	js	.L165
	movl	%esi, %ebx
	cmpl	$64, %edi
	jle	.L154
	cmpl	$4095, %edi
	jle	.L165
	cmpl	$4120, %edi
	jle	.L156
	cmpl	$8191, %edi
	jle	.L165
	cmpl	$8192, %edi
	je	.L158
	cmpl	$12287, %edi
	jle	.L165
	cmpl	$12288, %edi
	je	.L160
	cmpl	$16383, %edi
	jle	.L165
	cmpl	$16397, %edi
	jle	.L162
	xorl	%r12d, %r12d
	cmpl	$28671, %edi
	jle	.L148
	movl	$28672, %edx
	movl	$225, %eax
	cmpl	$28672, %edi
	je	.L150
.L148:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L156:
	.cfi_restore_state
	movl	$4096, %edx
	movl	$135, %eax
	.p2align 4,,10
	.p2align 3
.L150:
	subl	%edx, %edi
	leaq	_ZN6icu_6712PropNameData9valueMapsE(%rip), %rdx
	leal	(%rax,%rdi,2), %eax
	cltq
	movslq	(%rdx,%rax,4), %rax
	leaq	_ZN6icu_6712PropNameData10nameGroupsE(%rip), %rdx
	addq	%rdx, %rax
	leaq	1(%rax), %r12
	movsbl	(%rax), %eax
	cmpl	%eax, %ebx
	jge	.L165
	testl	%ebx, %ebx
	js	.L165
	je	.L172
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	1(%r12,%rax), %r12
	subl	$1, %ebx
	jne	.L152
.L172:
	movl	$0, %eax
	cmpb	$0, (%r12)
	popq	%rbx
	cmove	%rax, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L154:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$3, %eax
	jmp	.L150
.L158:
	movl	$8192, %edx
	movl	$187, %eax
	jmp	.L150
.L160:
	movl	$12288, %edx
	movl	$191, %eax
	jmp	.L150
.L162:
	movl	$16384, %edx
	movl	$195, %eax
	jmp	.L150
	.cfi_endproc
.LFE3029:
	.size	_ZN6icu_6712PropNameData15getPropertyNameEii, .-_ZN6icu_6712PropNameData15getPropertyNameEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PropNameData20getPropertyValueNameEiii
	.type	_ZN6icu_6712PropNameData20getPropertyValueNameEiii, @function
_ZN6icu_6712PropNameData20getPropertyValueNameEiii:
.LFB3030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testl	%edi, %edi
	js	.L192
	movl	%edx, %ebx
	cmpl	$64, %edi
	jle	.L179
	cmpl	$4095, %edi
	jle	.L192
	cmpl	$4120, %edi
	jle	.L181
	cmpl	$8191, %edi
	jle	.L192
	cmpl	$8192, %edi
	je	.L183
	cmpl	$12287, %edi
	jle	.L192
	cmpl	$12288, %edi
	je	.L185
	cmpl	$16383, %edi
	jle	.L192
	cmpl	$16397, %edi
	jle	.L187
	xorl	%r12d, %r12d
	cmpl	$28671, %edi
	jle	.L173
	movl	$28672, %edx
	movl	$225, %eax
	cmpl	$28672, %edi
	je	.L175
.L173:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L181:
	.cfi_restore_state
	movl	$4096, %edx
	movl	$135, %eax
	.p2align 4,,10
	.p2align 3
.L175:
	subl	%edx, %edi
	leaq	_ZN6icu_6712PropNameData9valueMapsE(%rip), %rdx
	xorl	%r12d, %r12d
	leal	1(%rax,%rdi,2), %eax
	cltq
	movl	(%rdx,%rax,4), %edi
	testl	%edi, %edi
	je	.L173
	call	_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii.part.0
	testl	%eax, %eax
	je	.L173
	cltq
	leaq	_ZN6icu_6712PropNameData10nameGroupsE(%rip), %rdx
	addq	%rdx, %rax
	leaq	1(%rax), %r12
	movsbl	(%rax), %eax
	cmpl	%eax, %ebx
	jge	.L192
	testl	%ebx, %ebx
	js	.L192
	je	.L200
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	1(%r12,%rax), %r12
	subl	$1, %ebx
	jne	.L177
.L200:
	movl	$0, %eax
	cmpb	$0, (%r12)
	popq	%rbx
	cmove	%rax, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L179:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$3, %eax
	jmp	.L175
.L183:
	movl	$8192, %edx
	movl	$187, %eax
	jmp	.L175
.L185:
	movl	$12288, %edx
	movl	$191, %eax
	jmp	.L175
.L187:
	movl	$16384, %edx
	movl	$195, %eax
	jmp	.L175
	.cfi_endproc
.LFE3030:
	.size	_ZN6icu_6712PropNameData20getPropertyValueNameEiii, .-_ZN6icu_6712PropNameData20getPropertyValueNameEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PropNameData22getPropertyOrValueEnumEiPKc
	.type	_ZN6icu_6712PropNameData22getPropertyOrValueEnumEiPKc, @function
_ZN6icu_6712PropNameData22getPropertyOrValueEnumEiPKc:
.LFB3031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	_ZN6icu_6712PropNameData10bytesTriesE(%rip), %rax
	movl	$-1, -56(%rbp)
	addq	%rax, %rdi
	movq	$0, -80(%rbp)
	movq	%rdi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	testq	%rsi, %rsi
	je	.L221
	movsbl	(%rsi), %edi
	leaq	1(%rsi), %rbx
	movl	$1, %r14d
	leaq	-80(%rbp), %r12
	movabsq	$-9223372036854767615, %r13
	testb	%dil, %dil
	jne	.L204
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L222:
	leal	-9(%rax), %ecx
	cmpb	$4, %cl
	jbe	.L208
.L209:
	andl	$1, %r14d
	je	.L205
	movzbl	%al, %esi
	movq	%r12, %rdi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movl	%eax, %r14d
	movl	%eax, %edx
.L208:
	movsbl	(%rbx), %edi
	addq	$1, %rbx
	testb	%dil, %dil
	je	.L206
.L204:
	call	uprv_asciitolower_67@PLT
	movl	%r14d, %edx
	leal	-32(%rax), %ecx
	cmpb	$63, %cl
	ja	.L222
	btq	%rcx, %r13
	jnc	.L209
	movsbl	(%rbx), %edi
	addq	$1, %rbx
	testb	%dil, %dil
	jne	.L204
.L206:
	cmpl	$1, %edx
	jle	.L205
	movq	-64(%rbp), %rdi
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	sarl	%esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	movl	%eax, %r13d
.L203:
	movq	%r12, %rdi
	call	_ZN6icu_679BytesTrieD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	addq	$48, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	leaq	-80(%rbp), %r12
.L205:
	movl	$-1, %r13d
	jmp	.L203
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3031:
	.size	_ZN6icu_6712PropNameData22getPropertyOrValueEnumEiPKc, .-_ZN6icu_6712PropNameData22getPropertyOrValueEnumEiPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PropNameData15getPropertyEnumEPKc
	.type	_ZN6icu_6712PropNameData15getPropertyEnumEPKc, @function
_ZN6icu_6712PropNameData15getPropertyEnumEPKc:
.LFB3032:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	_ZN6icu_6712PropNameData22getPropertyOrValueEnumEiPKc
	.cfi_endproc
.LFE3032:
	.size	_ZN6icu_6712PropNameData15getPropertyEnumEPKc, .-_ZN6icu_6712PropNameData15getPropertyEnumEPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PropNameData20getPropertyValueEnumEiPKc
	.type	_ZN6icu_6712PropNameData20getPropertyValueEnumEiPKc, @function
_ZN6icu_6712PropNameData20getPropertyValueEnumEiPKc:
.LFB3033:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	js	.L225
	cmpl	$64, %edi
	jle	.L229
	cmpl	$4095, %edi
	jle	.L225
	cmpl	$4120, %edi
	jle	.L231
	cmpl	$8191, %edi
	jle	.L225
	cmpl	$8192, %edi
	je	.L233
	cmpl	$12287, %edi
	jle	.L225
	cmpl	$12288, %edi
	je	.L235
	cmpl	$16383, %edi
	jle	.L225
	cmpl	$16397, %edi
	jle	.L237
	cmpl	$28671, %edi
	jle	.L225
	movl	$28672, %ecx
	movl	$225, %eax
	cmpl	$28672, %edi
	je	.L227
.L225:
	movl	$-1, %eax
	ret
.L231:
	movl	$4096, %ecx
	movl	$135, %eax
	.p2align 4,,10
	.p2align 3
.L227:
	subl	%ecx, %edi
	leaq	_ZN6icu_6712PropNameData9valueMapsE(%rip), %rdx
	leal	1(%rax,%rdi,2), %eax
	cltq
	movslq	(%rdx,%rax,4), %rax
	testl	%eax, %eax
	je	.L225
	movl	(%rdx,%rax,4), %edi
	jmp	_ZN6icu_6712PropNameData22getPropertyOrValueEnumEiPKc
.L229:
	xorl	%ecx, %ecx
	movl	$3, %eax
	jmp	.L227
.L233:
	movl	$8192, %ecx
	movl	$187, %eax
	jmp	.L227
.L235:
	movl	$12288, %ecx
	movl	$191, %eax
	jmp	.L227
.L237:
	movl	$16384, %ecx
	movl	$195, %eax
	jmp	.L227
	.cfi_endproc
.LFE3033:
	.size	_ZN6icu_6712PropNameData20getPropertyValueEnumEiPKc, .-_ZN6icu_6712PropNameData20getPropertyValueEnumEiPKc
	.p2align 4
	.globl	u_getPropertyName_67
	.type	u_getPropertyName_67, @function
u_getPropertyName_67:
.LFB3034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testl	%edi, %edi
	js	.L259
	movl	%esi, %ebx
	cmpl	$64, %edi
	jle	.L248
	cmpl	$4095, %edi
	jle	.L259
	cmpl	$4120, %edi
	jle	.L250
	cmpl	$8191, %edi
	jle	.L259
	cmpl	$8192, %edi
	je	.L252
	cmpl	$12287, %edi
	jle	.L259
	cmpl	$12288, %edi
	je	.L254
	cmpl	$16383, %edi
	jle	.L259
	cmpl	$16397, %edi
	jle	.L256
	xorl	%r12d, %r12d
	cmpl	$28671, %edi
	jle	.L242
	movl	$28672, %edx
	movl	$225, %eax
	cmpl	$28672, %edi
	je	.L244
.L242:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movl	$4096, %edx
	movl	$135, %eax
	.p2align 4,,10
	.p2align 3
.L244:
	subl	%edx, %edi
	leaq	_ZN6icu_6712PropNameData9valueMapsE(%rip), %rdx
	leal	(%rax,%rdi,2), %eax
	cltq
	movslq	(%rdx,%rax,4), %rax
	leaq	_ZN6icu_6712PropNameData10nameGroupsE(%rip), %rdx
	addq	%rdx, %rax
	leaq	1(%rax), %r12
	movsbl	(%rax), %eax
	cmpl	%eax, %ebx
	jge	.L259
	testl	%ebx, %ebx
	js	.L259
	je	.L266
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	1(%r12,%rax), %r12
	subl	$1, %ebx
	jne	.L246
.L266:
	movl	$0, %eax
	cmpb	$0, (%r12)
	popq	%rbx
	cmove	%rax, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$3, %eax
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L252:
	movl	$8192, %edx
	movl	$187, %eax
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$12288, %edx
	movl	$191, %eax
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L256:
	movl	$16384, %edx
	movl	$195, %eax
	jmp	.L244
	.cfi_endproc
.LFE3034:
	.size	u_getPropertyName_67, .-u_getPropertyName_67
	.p2align 4
	.globl	u_getPropertyEnum_67
	.type	u_getPropertyEnum_67, @function
u_getPropertyEnum_67:
.LFB3035:
	.cfi_startproc
	endbr64
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	_ZN6icu_6712PropNameData22getPropertyOrValueEnumEiPKc
	.cfi_endproc
.LFE3035:
	.size	u_getPropertyEnum_67, .-u_getPropertyEnum_67
	.p2align 4
	.globl	u_getPropertyValueName_67
	.type	u_getPropertyValueName_67, @function
u_getPropertyValueName_67:
.LFB3036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testl	%edi, %edi
	js	.L287
	movl	%edx, %ebx
	cmpl	$64, %edi
	jle	.L274
	cmpl	$4095, %edi
	jle	.L287
	cmpl	$4120, %edi
	jle	.L276
	cmpl	$8191, %edi
	jle	.L287
	cmpl	$8192, %edi
	je	.L278
	cmpl	$12287, %edi
	jle	.L287
	cmpl	$12288, %edi
	je	.L280
	cmpl	$16383, %edi
	jle	.L287
	cmpl	$16397, %edi
	jle	.L282
	xorl	%r12d, %r12d
	cmpl	$28671, %edi
	jle	.L268
	movl	$28672, %edx
	movl	$225, %eax
	cmpl	$28672, %edi
	je	.L270
.L268:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movl	$4096, %edx
	movl	$135, %eax
	.p2align 4,,10
	.p2align 3
.L270:
	subl	%edx, %edi
	leaq	_ZN6icu_6712PropNameData9valueMapsE(%rip), %rdx
	xorl	%r12d, %r12d
	leal	1(%rax,%rdi,2), %eax
	cltq
	movl	(%rdx,%rax,4), %edi
	testl	%edi, %edi
	je	.L268
	call	_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii.part.0
	testl	%eax, %eax
	je	.L268
	cltq
	leaq	_ZN6icu_6712PropNameData10nameGroupsE(%rip), %rdx
	addq	%rdx, %rax
	leaq	1(%rax), %r12
	movsbl	(%rax), %eax
	cmpl	%eax, %ebx
	jge	.L287
	testl	%ebx, %ebx
	js	.L287
	je	.L295
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	1(%r12,%rax), %r12
	subl	$1, %ebx
	jne	.L272
.L295:
	movl	$0, %eax
	cmpb	$0, (%r12)
	popq	%rbx
	cmove	%rax, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$3, %eax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L278:
	movl	$8192, %edx
	movl	$187, %eax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L280:
	movl	$12288, %edx
	movl	$191, %eax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L282:
	movl	$16384, %edx
	movl	$195, %eax
	jmp	.L270
	.cfi_endproc
.LFE3036:
	.size	u_getPropertyValueName_67, .-u_getPropertyValueName_67
	.p2align 4
	.globl	u_getPropertyValueEnum_67
	.type	u_getPropertyValueEnum_67, @function
u_getPropertyValueEnum_67:
.LFB3037:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	js	.L296
	cmpl	$64, %edi
	jle	.L300
	cmpl	$4095, %edi
	jle	.L296
	cmpl	$4120, %edi
	jle	.L302
	cmpl	$8191, %edi
	jle	.L296
	cmpl	$8192, %edi
	je	.L304
	cmpl	$12287, %edi
	jle	.L296
	cmpl	$12288, %edi
	je	.L306
	cmpl	$16383, %edi
	jle	.L296
	cmpl	$16397, %edi
	jle	.L308
	cmpl	$28671, %edi
	jle	.L296
	movl	$28672, %ecx
	movl	$225, %eax
	cmpl	$28672, %edi
	je	.L298
.L296:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$4096, %ecx
	movl	$135, %eax
	.p2align 4,,10
	.p2align 3
.L298:
	subl	%ecx, %edi
	leaq	_ZN6icu_6712PropNameData9valueMapsE(%rip), %rdx
	leal	1(%rax,%rdi,2), %eax
	cltq
	movslq	(%rdx,%rax,4), %rax
	testl	%eax, %eax
	je	.L296
	movl	(%rdx,%rax,4), %edi
	jmp	_ZN6icu_6712PropNameData22getPropertyOrValueEnumEiPKc
	.p2align 4,,10
	.p2align 3
.L300:
	xorl	%ecx, %ecx
	movl	$3, %eax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$8192, %ecx
	movl	$187, %eax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L306:
	movl	$12288, %ecx
	movl	$191, %eax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L308:
	movl	$16384, %ecx
	movl	$195, %eax
	jmp	.L298
	.cfi_endproc
.LFE3037:
	.size	u_getPropertyValueEnum_67, .-u_getPropertyValueEnum_67
	.p2align 4
	.globl	uscript_getName_67
	.type	uscript_getName_67, @function
uscript_getName_67:
.LFB3038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %esi
	movl	$928, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	call	_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii.part.0
	testl	%eax, %eax
	je	.L316
	cltq
	leaq	_ZN6icu_6712PropNameData10nameGroupsE(%rip), %rdx
	addq	%rdx, %rax
	cmpb	$1, (%rax)
	leaq	1(%rax), %rbx
	jle	.L316
	movq	%rbx, %rdi
	call	strlen@PLT
	movl	$0, %edx
	leaq	1(%rbx,%rax), %rax
	cmpb	$0, (%rax)
	cmove	%rdx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3038:
	.size	uscript_getName_67, .-uscript_getName_67
	.p2align 4
	.globl	uscript_getShortName_67
	.type	uscript_getShortName_67, @function
uscript_getShortName_67:
.LFB3039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %esi
	movl	$928, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_6712PropNameData26findPropertyValueNameGroupEii.part.0
	testl	%eax, %eax
	je	.L323
	cltq
	leaq	_ZN6icu_6712PropNameData10nameGroupsE(%rip), %rdx
	addq	%rdx, %rax
	cmpb	$0, (%rax)
	jle	.L323
	cmpb	$0, 1(%rax)
	je	.L323
	addq	$1, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3039:
	.size	uscript_getShortName_67, .-uscript_getShortName_67
	.globl	_ZN6icu_6712PropNameData10nameGroupsE
	.section	.rodata
	.align 32
	.type	_ZN6icu_6712PropNameData10nameGroupsE, @object
	.size	_ZN6icu_6712PropNameData10nameGroupsE, 22098
_ZN6icu_6712PropNameData10nameGroupsE:
	.string	"\002Alpha"
	.string	"Alphabetic"
	.string	"\004N"
	.string	"No"
	.string	"F"
	.string	"False"
	.string	"\004Y"
	.string	"Yes"
	.string	"T"
	.string	"True"
	.string	"\002NR"
	.string	"Not_Reordered"
	.string	"\002OV"
	.string	"Overlay"
	.string	"\002HANR"
	.string	"Han_Reading"
	.string	"\002NK"
	.string	"Nukta"
	.string	"\002KV"
	.string	"Kana_Voicing"
	.string	"\002VR"
	.string	"Virama"
	.string	"\002CCC10"
	.string	"CCC10"
	.string	"\002CCC11"
	.string	"CCC11"
	.string	"\002CCC12"
	.string	"CCC12"
	.string	"\002CCC13"
	.string	"CCC13"
	.string	"\002CCC14"
	.string	"CCC14"
	.string	"\002CCC15"
	.string	"CCC15"
	.string	"\002CCC16"
	.string	"CCC16"
	.string	"\002CCC17"
	.string	"CCC17"
	.string	"\002CCC18"
	.string	"CCC18"
	.string	"\002CCC19"
	.string	"CCC19"
	.string	"\002CCC20"
	.string	"CCC20"
	.string	"\002CCC21"
	.string	"CCC21"
	.string	"\002CCC22"
	.string	"CCC22"
	.string	"\002CCC23"
	.string	"CCC23"
	.string	"\002CCC24"
	.string	"CCC24"
	.string	"\002CCC25"
	.string	"CCC25"
	.string	"\002CCC26"
	.string	"CCC26"
	.string	"\002CCC27"
	.string	"CCC27"
	.string	"\002CCC28"
	.string	"CCC28"
	.string	"\002CCC29"
	.string	"CCC29"
	.string	"\002CCC30"
	.string	"CCC30"
	.string	"\002CCC31"
	.string	"CCC31"
	.string	"\002CCC32"
	.string	"CCC32"
	.string	"\002CCC33"
	.string	"CCC33"
	.string	"\002CCC34"
	.string	"CCC34"
	.string	"\002CCC35"
	.string	"CCC35"
	.string	"\002CCC36"
	.string	"CCC36"
	.string	"\002CCC84"
	.string	"CCC84"
	.string	"\002CCC91"
	.string	"CCC91"
	.string	"\002CCC103"
	.string	"CCC103"
	.string	"\002CCC107"
	.string	"CCC107"
	.string	"\002CCC118"
	.string	"CCC118"
	.string	"\002CCC122"
	.string	"CCC122"
	.string	"\002CCC129"
	.string	"CCC129"
	.string	"\002CCC130"
	.string	"CCC130"
	.string	"\002CCC132"
	.string	"CCC132"
	.string	"\002CCC133"
	.string	"CCC133"
	.string	"\002ATBL"
	.string	"Attached_Below_Left"
	.string	"\002ATB"
	.string	"Attached_Below"
	.string	"\002ATA"
	.string	"Attached_Above"
	.string	"\002ATAR"
	.string	"Attached_Above_Right"
	.string	"\002BL"
	.string	"Below_Left"
	.string	"\002B"
	.string	"Below"
	.string	"\002BR"
	.string	"Below_Right"
	.string	"\002L"
	.string	"Left"
	.string	"\002R"
	.string	"Right"
	.string	"\002AL"
	.string	"Above_Left"
	.string	"\002A"
	.string	"Above"
	.string	"\002AR"
	.string	"Above_Right"
	.string	"\002DB"
	.string	"Double_Below"
	.string	"\002DA"
	.string	"Double_Above"
	.string	"\002IS"
	.string	"Iota_Subscript"
	.string	"\002AHex"
	.string	"ASCII_Hex_Digit"
	.string	"\002Bidi_C"
	.string	"Bidi_Control"
	.string	"\002Bidi_M"
	.string	"Bidi_Mirrored"
	.string	"\002Dash"
	.string	"Dash"
	.string	"\002DI"
	.string	"Default_Ignorable_Code_Point"
	.string	"\002Dep"
	.string	"Deprecated"
	.string	"\002Dia"
	.string	"Diacritic"
	.string	"\002Ext"
	.string	"Extender"
	.string	"\002Comp_Ex"
	.string	"Full_Composition_Exclusion"
	.string	"\002Gr_Base"
	.string	"Grapheme_Base"
	.string	"\002Gr_Ext"
	.string	"Grapheme_Extend"
	.string	"\002Gr_Link"
	.string	"Grapheme_Link"
	.string	"\002Hex"
	.string	"Hex_Digit"
	.string	"\002Hyphen"
	.string	"Hyphen"
	.string	"\002IDC"
	.string	"ID_Continue"
	.string	"\002IDS"
	.string	"ID_Start"
	.string	"\002Ideo"
	.string	"Ideographic"
	.string	"\002IDSB"
	.string	"IDS_Binary_Operator"
	.string	"\002IDST"
	.string	"IDS_Trinary_Operator"
	.string	"\002Join_C"
	.string	"Join_Control"
	.string	"\002LOE"
	.string	"Logical_Order_Exception"
	.string	"\002Lower"
	.string	"Lowercase"
	.string	"\002Math"
	.string	"Math"
	.string	"\002NChar"
	.string	"Noncharacter_Code_Point"
	.string	"\002QMark"
	.string	"Quotation_Mark"
	.string	"\002Radical"
	.string	"Radical"
	.string	"\002SD"
	.string	"Soft_Dotted"
	.string	"\002Term"
	.string	"Terminal_Punctuation"
	.string	"\002UIdeo"
	.string	"Unified_Ideograph"
	.string	"\002Upper"
	.string	"Uppercase"
	.string	"\003WSpace"
	.string	"White_Space"
	.string	"space"
	.string	"\002XIDC"
	.string	"XID_Continue"
	.string	"\002XIDS"
	.string	"XID_Start"
	.string	"\002Sensitive"
	.string	"Case_Sensitive"
	.string	"\002STerm"
	.string	"Sentence_Terminal"
	.string	"\002VS"
	.string	"Variation_Selector"
	.string	"\002nfdinert"
	.string	"NFD_Inert"
	.string	"\002nfkdinert"
	.string	"NFKD_Inert"
	.string	"\002nfcinert"
	.string	"NFC_Inert"
	.string	"\002nfkcinert"
	.string	"NFKC_Inert"
	.string	"\002segstart"
	.string	"Segment_Starter"
	.string	"\002Pat_Syn"
	.string	"Pattern_Syntax"
	.string	"\002Pat_WS"
	.string	"Pattern_White_Space"
	.string	"\002"
	.string	"alnum"
	.string	"\002"
	.string	"blank"
	.string	"\002"
	.string	"graph"
	.string	"\002"
	.string	"print"
	.string	"\002"
	.string	"xdigit"
	.string	"\002Cased"
	.string	"Cased"
	.string	"\002CI"
	.string	"Case_Ignorable"
	.string	"\002CWL"
	.string	"Changes_When_Lowercased"
	.string	"\002CWU"
	.string	"Changes_When_Uppercased"
	.string	"\002CWT"
	.string	"Changes_When_Titlecased"
	.string	"\002CWCF"
	.string	"Changes_When_Casefolded"
	.string	"\002CWCM"
	.ascii	"Changes_Wh"
	.string	"en_Casemapped"
	.string	"\002CWKCF"
	.string	"Changes_When_NFKC_Casefolded"
	.string	"\002Emoji"
	.string	"Emoji"
	.string	"\002EPres"
	.string	"Emoji_Presentation"
	.string	"\002EMod"
	.string	"Emoji_Modifier"
	.string	"\002EBase"
	.string	"Emoji_Modifier_Base"
	.string	"\002EComp"
	.string	"Emoji_Component"
	.string	"\002RI"
	.string	"Regional_Indicator"
	.string	"\002PCM"
	.string	"Prepended_Concatenation_Mark"
	.string	"\002ExtPict"
	.string	"Extended_Pictographic"
	.string	"\002bc"
	.string	"Bidi_Class"
	.string	"\002L"
	.string	"Left_To_Right"
	.string	"\002R"
	.string	"Right_To_Left"
	.string	"\002EN"
	.string	"European_Number"
	.string	"\002ES"
	.string	"European_Separator"
	.string	"\002ET"
	.string	"European_Terminator"
	.string	"\002AN"
	.string	"Arabic_Number"
	.string	"\002CS"
	.string	"Common_Separator"
	.string	"\002B"
	.string	"Paragraph_Separator"
	.string	"\002S"
	.string	"Segment_Separator"
	.string	"\002WS"
	.string	"White_Space"
	.string	"\002ON"
	.string	"Other_Neutral"
	.string	"\002LRE"
	.string	"Left_To_Right_Embedding"
	.string	"\002LRO"
	.string	"Left_To_Right_Override"
	.string	"\002AL"
	.string	"Arabic_Letter"
	.string	"\002RLE"
	.string	"Right_To_Left_Embedding"
	.string	"\002RLO"
	.string	"Right_To_Left_Override"
	.string	"\002PDF"
	.string	"Pop_Directional_Format"
	.string	"\002NSM"
	.string	"Nonspacing_Mark"
	.string	"\002BN"
	.string	"Boundary_Neutral"
	.string	"\002FSI"
	.string	"First_Strong_Isolate"
	.string	"\002LRI"
	.string	"Left_To_Right_Isolate"
	.string	"\002RLI"
	.string	"Right_To_Left_Isolate"
	.string	"\002PDI"
	.string	"Pop_Directional_Isolate"
	.string	"\002blk"
	.string	"Block"
	.string	"\002NB"
	.string	"No_Block"
	.string	"\002ASCII"
	.string	"Basic_Latin"
	.string	"\003Latin_1_Sup"
	.string	"Latin_1_Supplement"
	.string	"Latin_1"
	.string	"\002Latin_Ext_A"
	.string	"Latin_Extended_A"
	.string	"\002Latin_Ext_B"
	.string	"Latin_Extended_B"
	.string	"\002IPA_Ext"
	.string	"IPA_Extensions"
	.string	"\002Modifier_Letters"
	.string	"Spacing_Modifier_Letters"
	.string	"\002Diacriticals"
	.string	"Combining_Diacritical_Marks"
	.string	"\002Greek"
	.string	"Greek_And_Coptic"
	.string	"\002Cyrillic"
	.string	"Cyrillic"
	.string	"\002Armenian"
	.string	"Armenian"
	.string	"\002Hebrew"
	.string	"Hebrew"
	.string	"\002Arabic"
	.string	"Arabic"
	.string	"\002Syriac"
	.string	"Syriac"
	.string	"\002Thaana"
	.string	"Thaana"
	.string	"\002Devanagari"
	.string	"Devanagari"
	.string	"\002Bengali"
	.string	"Bengali"
	.string	"\002Gurmukhi"
	.string	"Gurmukhi"
	.string	"\002Gujarati"
	.string	"Gujarati"
	.string	"\002Oriya"
	.string	"Oriya"
	.string	"\002Tamil"
	.string	"Tamil"
	.string	"\002Telugu"
	.string	"Telugu"
	.string	"\002Kannada"
	.string	"Kannada"
	.string	"\002Malayalam"
	.string	"Malayalam"
	.string	"\002Sinhala"
	.string	"Sinhala"
	.string	"\002Thai"
	.string	"Thai"
	.string	"\002Lao"
	.string	"Lao"
	.string	"\002Tibetan"
	.string	"Tibetan"
	.string	"\002Myanmar"
	.string	"Myanmar"
	.string	"\002Georgian"
	.string	"Georgian"
	.string	"\002Jamo"
	.string	"Hangul_Jamo"
	.string	"\002Ethiopic"
	.string	"Ethiopic"
	.string	"\002Cherokee"
	.string	"Cherokee"
	.string	"\003UCAS"
	.string	"Unified_Canadian_Aboriginal_Syllabics"
	.string	"Canadian_Syllabics"
	.string	"\002Ogham"
	.string	"Ogham"
	.string	"\002Runic"
	.string	"Runic"
	.string	"\002Khmer"
	.string	"Khmer"
	.string	"\002Mongolian"
	.string	"Mongolian"
	.string	"\002Latin_Ext_Additional"
	.string	"Latin_Extended_Additional"
	.string	"\002Greek_Ext"
	.string	"Greek_Extended"
	.string	"\002Punctuation"
	.string	"General_Punctuation"
	.string	"\002Super_And_Sub"
	.string	"Superscripts_And_Subscripts"
	.string	"\002Currency_Symbols"
	.string	"Currency_Symbols"
	.string	"\003Diacriticals_For_Symbols"
	.string	"Combining_Diacritical_Marks_For_Symbols"
	.string	"Combining_Marks_For_Symbols"
	.string	"\002Letterlike_Symbols"
	.string	"Letterlike_Symbols"
	.string	"\002Number_Forms"
	.string	"Number_Forms"
	.string	"\002Arrows"
	.string	"Arrows"
	.string	"\002Math_Operators"
	.string	"Mathematical_Operators"
	.ascii	"\002Misc_Tec"
	.string	"hnical"
	.string	"Miscellaneous_Technical"
	.string	"\002Control_Pictures"
	.string	"Control_Pictures"
	.string	"\002OCR"
	.string	"Optical_Character_Recognition"
	.string	"\002Enclosed_Alphanum"
	.string	"Enclosed_Alphanumerics"
	.string	"\002Box_Drawing"
	.string	"Box_Drawing"
	.string	"\002Block_Elements"
	.string	"Block_Elements"
	.string	"\002Geometric_Shapes"
	.string	"Geometric_Shapes"
	.string	"\002Misc_Symbols"
	.string	"Miscellaneous_Symbols"
	.string	"\002Dingbats"
	.string	"Dingbats"
	.string	"\002Braille"
	.string	"Braille_Patterns"
	.string	"\002CJK_Radicals_Sup"
	.string	"CJK_Radicals_Supplement"
	.string	"\002Kangxi"
	.string	"Kangxi_Radicals"
	.string	"\002IDC"
	.string	"Ideographic_Description_Characters"
	.string	"\002CJK_Symbols"
	.string	"CJK_Symbols_And_Punctuation"
	.string	"\002Hiragana"
	.string	"Hiragana"
	.string	"\002Katakana"
	.string	"Katakana"
	.string	"\002Bopomofo"
	.string	"Bopomofo"
	.string	"\002Compat_Jamo"
	.string	"Hangul_Compatibility_Jamo"
	.string	"\002Kanbun"
	.string	"Kanbun"
	.string	"\002Bopomofo_Ext"
	.string	"Bopomofo_Extended"
	.string	"\002Enclosed_CJK"
	.string	"Enclosed_CJK_Letters_And_Months"
	.string	"\002CJK_Compat"
	.string	"CJK_Compatibility"
	.string	"\002CJK_Ext_A"
	.string	"CJK_Unified_Ideographs_Extension_A"
	.string	"\002CJK"
	.string	"CJK_Unified_Ideographs"
	.string	"\002Yi_Syllables"
	.string	"Yi_Syllables"
	.string	"\002Yi_Radicals"
	.string	"Yi_Radicals"
	.string	"\002Hangul"
	.string	"Hangul_Syllables"
	.string	"\002High_Surrogates"
	.string	"High_Surrogates"
	.string	"\002High_PU_Surrogates"
	.string	"High_Private_Use_Surrogates"
	.string	"\002Low_Surrogates"
	.string	"Low_Surrogates"
	.string	"\003PUA"
	.string	"Private_Use_Area"
	.string	"Private_Use"
	.string	"\002CJK_Compat_Ideographs"
	.string	"CJK_Compatibility_Ideographs"
	.string	"\002Alphabetic_PF"
	.string	"Alphabetic_Presentation_Forms"
	.string	"\003Arabic_PF_A"
	.string	"Arabic_Presentation_Forms_A"
	.string	"Arabic_Presentation_Forms-A"
	.string	"\002Half_Marks"
	.string	"Combining_Half_Marks"
	.string	"\002CJK_Compat_Forms"
	.string	"CJK_Compatibility_Forms"
	.string	"\002Small_Forms"
	.string	"Small_Form_Variants"
	.string	"\002Arabic_PF_B"
	.string	"Arabic_Presentation_Forms_B"
	.string	"\002Specials"
	.string	"Specials"
	.string	"\002Half_And_Full_Forms"
	.string	"Halfwidth_And_Fullwidth_Forms"
	.string	"\002Old_Italic"
	.string	"Old_Italic"
	.string	"\002Gothic"
	.string	"Gothic"
	.string	"\002Deseret"
	.string	"Deseret"
	.string	"\002Byzantine_Music"
	.string	"Byzantine_Musical_Symbols"
	.string	"\002Music"
	.string	"Musical_Symbols"
	.string	"\002Math_Alphanum"
	.string	"Mathematical_Alphanumeric_Symbols"
	.string	"\002CJK_Ext_B"
	.string	"CJK_Unified_Ideographs_Extension_B"
	.string	"\002CJK_Compat_Ideographs_Sup"
	.string	"CJK_Compatibility_Ideographs_Supplement"
	.string	"\002Tags"
	.string	"Tags"
	.string	"\003Cyrillic_Sup"
	.string	"Cyrillic_Supplement"
	.string	"Cyrillic_Supplementary"
	.string	"\002Tagalog"
	.string	"Tagalog"
	.string	"\002Hanunoo"
	.string	"Hanunoo"
	.string	"\002Buhid"
	.string	"Buhid"
	.string	"\002Tagbanwa"
	.string	"Tagbanwa"
	.string	"\002Misc_Math_Symbols_A"
	.string	"Miscellaneous_Mathematical_Symbols_A"
	.string	"\002Sup_Arrows_A"
	.string	"Supplemental_Arrows_A"
	.string	"\002Sup_Arrows_B"
	.string	"Supplemental_Arrows_B"
	.string	"\002Misc_Math_Symbols_B"
	.string	"Miscellaneous_Mathematical_Symbols_B"
	.string	"\002Sup_Math_Operators"
	.ascii	"Supplemental"
	.string	"_Mathematical_Operators"
	.string	"\002Katakana_Ext"
	.string	"Katakana_Phonetic_Extensions"
	.string	"\002VS"
	.string	"Variation_Selectors"
	.string	"\002Sup_PUA_A"
	.string	"Supplementary_Private_Use_Area_A"
	.string	"\002Sup_PUA_B"
	.string	"Supplementary_Private_Use_Area_B"
	.string	"\002Limbu"
	.string	"Limbu"
	.string	"\002Tai_Le"
	.string	"Tai_Le"
	.string	"\002Khmer_Symbols"
	.string	"Khmer_Symbols"
	.string	"\002Phonetic_Ext"
	.string	"Phonetic_Extensions"
	.string	"\002Misc_Arrows"
	.string	"Miscellaneous_Symbols_And_Arrows"
	.string	"\002Yijing"
	.string	"Yijing_Hexagram_Symbols"
	.string	"\002Linear_B_Syllabary"
	.string	"Linear_B_Syllabary"
	.string	"\002Linear_B_Ideograms"
	.string	"Linear_B_Ideograms"
	.string	"\002Aegean_Numbers"
	.string	"Aegean_Numbers"
	.string	"\002Ugaritic"
	.string	"Ugaritic"
	.string	"\002Shavian"
	.string	"Shavian"
	.string	"\002Osmanya"
	.string	"Osmanya"
	.string	"\002Cypriot_Syllabary"
	.string	"Cypriot_Syllabary"
	.string	"\002Tai_Xuan_Jing"
	.string	"Tai_Xuan_Jing_Symbols"
	.string	"\002VS_Sup"
	.string	"Variation_Selectors_Supplement"
	.string	"\002Ancient_Greek_Music"
	.string	"Ancient_Greek_Musical_Notation"
	.string	"\002Ancient_Greek_Numbers"
	.string	"Ancient_Greek_Numbers"
	.string	"\002Arabic_Sup"
	.string	"Arabic_Supplement"
	.string	"\002Buginese"
	.string	"Buginese"
	.string	"\002CJK_Strokes"
	.string	"CJK_Strokes"
	.string	"\002Diacriticals_Sup"
	.string	"Combining_Diacritical_Marks_Supplement"
	.string	"\002Coptic"
	.string	"Coptic"
	.string	"\002Ethiopic_Ext"
	.string	"Ethiopic_Extended"
	.string	"\002Ethiopic_Sup"
	.string	"Ethiopic_Supplement"
	.string	"\002Georgian_Sup"
	.string	"Georgian_Supplement"
	.string	"\002Glagolitic"
	.string	"Glagolitic"
	.string	"\002Kharoshthi"
	.string	"Kharoshthi"
	.string	"\002Modifier_Tone_Letters"
	.string	"Modifier_Tone_Letters"
	.string	"\002New_Tai_Lue"
	.string	"New_Tai_Lue"
	.string	"\002Old_Persian"
	.string	"Old_Persian"
	.string	"\002Phonetic_Ext_Sup"
	.string	"Phonetic_Extensions_Supplement"
	.string	"\002Sup_Punctuation"
	.string	"Supplemental_Punctuation"
	.string	"\002Syloti_Nagri"
	.string	"Syloti_Nagri"
	.string	"\002Tifinagh"
	.string	"Tifinagh"
	.string	"\002Vertical_Forms"
	.string	"Vertical_Forms"
	.string	"\002NKo"
	.string	"NKo"
	.string	"\002Balinese"
	.string	"Balinese"
	.string	"\002Latin_Ext_C"
	.string	"Latin_Extended_C"
	.string	"\002Latin_Ext_D"
	.string	"Latin_Extended_D"
	.string	"\002Phags_Pa"
	.string	"Phags_Pa"
	.string	"\002Phoenician"
	.string	"Phoenician"
	.string	"\002Cuneiform"
	.string	"Cuneiform"
	.string	"\002Cuneiform_Numbers"
	.string	"Cuneiform_Numbers_And_Punctuation"
	.string	"\002Counting_Rod"
	.string	"Counting_Rod_Numerals"
	.string	"\002Sundanese"
	.string	"Sundanese"
	.string	"\002Lepcha"
	.string	"Lepcha"
	.string	"\002Ol_Chiki"
	.string	"Ol_Chiki"
	.string	"\002Cyrillic_Ext_A"
	.string	"Cyrillic_Extended_A"
	.string	"\002Vai"
	.string	"Vai"
	.string	"\002Cyrillic_Ext_B"
	.string	"Cyrillic_Extended_B"
	.string	"\002Saurashtra"
	.string	"Saurashtra"
	.string	"\002Kayah_Li"
	.string	"Kayah_Li"
	.string	"\002Rejang"
	.string	"Rejang"
	.string	"\002Cham"
	.string	"Cham"
	.string	"\002Ancient_Symbols"
	.string	"Ancient_Symbols"
	.string	"\002Phaistos"
	.string	"Phaistos_Disc"
	.string	"\002Lycian"
	.string	"Lycian"
	.string	"\002Carian"
	.string	"Carian"
	.string	"\002Lydian"
	.string	"Lydian"
	.string	"\002Mahjong"
	.string	"Mahjong_Tiles"
	.string	"\002Domino"
	.string	"Domino_Tiles"
	.string	"\002Samaritan"
	.string	"Samaritan"
	.string	"\002UCAS_Ext"
	.string	"Unified_Canadian_Aboriginal_Syllabics_Extended"
	.string	"\002Tai_Tham"
	.string	"Tai_Tham"
	.string	"\002Vedic_Ext"
	.string	"Vedic_Extensions"
	.string	"\002Lisu"
	.ascii	"Lisu"
	.string	""
	.string	"\002Bamum"
	.string	"Bamum"
	.string	"\002Indic_Number_Forms"
	.string	"Common_Indic_Number_Forms"
	.string	"\002Devanagari_Ext"
	.string	"Devanagari_Extended"
	.string	"\002Jamo_Ext_A"
	.string	"Hangul_Jamo_Extended_A"
	.string	"\002Javanese"
	.string	"Javanese"
	.string	"\002Myanmar_Ext_A"
	.string	"Myanmar_Extended_A"
	.string	"\002Tai_Viet"
	.string	"Tai_Viet"
	.string	"\002Meetei_Mayek"
	.string	"Meetei_Mayek"
	.string	"\002Jamo_Ext_B"
	.string	"Hangul_Jamo_Extended_B"
	.string	"\002Imperial_Aramaic"
	.string	"Imperial_Aramaic"
	.string	"\002Old_South_Arabian"
	.string	"Old_South_Arabian"
	.string	"\002Avestan"
	.string	"Avestan"
	.string	"\002Inscriptional_Parthian"
	.string	"Inscriptional_Parthian"
	.string	"\002Inscriptional_Pahlavi"
	.string	"Inscriptional_Pahlavi"
	.string	"\002Old_Turkic"
	.string	"Old_Turkic"
	.string	"\002Rumi"
	.string	"Rumi_Numeral_Symbols"
	.string	"\002Kaithi"
	.string	"Kaithi"
	.string	"\002Egyptian_Hieroglyphs"
	.string	"Egyptian_Hieroglyphs"
	.string	"\002Enclosed_Alphanum_Sup"
	.string	"Enclosed_Alphanumeric_Supplement"
	.string	"\002Enclosed_Ideographic_Sup"
	.string	"Enclosed_Ideographic_Supplement"
	.string	"\002CJK_Ext_C"
	.string	"CJK_Unified_Ideographs_Extension_C"
	.string	"\002Mandaic"
	.string	"Mandaic"
	.string	"\002Batak"
	.string	"Batak"
	.string	"\002Ethiopic_Ext_A"
	.string	"Ethiopic_Extended_A"
	.string	"\002Brahmi"
	.string	"Brahmi"
	.string	"\002Bamum_Sup"
	.string	"Bamum_Supplement"
	.string	"\002Kana_Sup"
	.string	"Kana_Supplement"
	.string	"\002Playing_Cards"
	.string	"Playing_Cards"
	.string	"\002Misc_Pictographs"
	.string	"Miscellaneous_Symbols_And_Pictographs"
	.string	"\002Emoticons"
	.string	"Emoticons"
	.string	"\002Transport_And_Map"
	.string	"Transport_And_Map_Symbols"
	.string	"\002Alchemical"
	.string	"Alchemical_Symbols"
	.string	"\002CJK_Ext_D"
	.string	"CJK_Unified_Ideographs_Extension_D"
	.string	"\002Arabic_Ext_A"
	.string	"Arabic_Extended_A"
	.string	"\002Arabic_Math"
	.string	"Arabic_Mathematical_Alphabetic_Symbols"
	.string	"\002Chakma"
	.string	"Chakma"
	.string	"\002Meetei_Mayek_Ext"
	.string	"Meetei_Mayek_Extensions"
	.string	"\002Meroitic_Cursive"
	.string	"Meroitic_Cursive"
	.string	"\002Meroitic_Hieroglyphs"
	.string	"Meroitic_Hieroglyphs"
	.string	"\002Miao"
	.string	"Miao"
	.string	"\002Sharada"
	.string	"Sharada"
	.string	"\002Sora_Sompeng"
	.string	"Sora_Sompeng"
	.string	"\002Sundanese_Sup"
	.string	"Sundanese_Supplement"
	.string	"\002Takri"
	.string	"Takri"
	.string	"\002Bassa_Vah"
	.string	"Bassa_Vah"
	.string	"\002Caucasian_Albanian"
	.string	"Caucasian_Albanian"
	.string	"\002Coptic_Epact_Numbers"
	.string	"Coptic_Epact_Numbers"
	.string	"\002Diacriticals_Ext"
	.string	"Combining_Diacritical_Marks_Extended"
	.string	"\002Duployan"
	.string	"Duployan"
	.string	"\002Elbasan"
	.string	"Elbasan"
	.string	"\002Geometric_Shapes_Ext"
	.string	"Geometric_Shapes_Extended"
	.string	"\002Grantha"
	.string	"Grantha"
	.string	"\002Khojki"
	.string	"Khojki"
	.string	"\002Khudawadi"
	.string	"Khudawadi"
	.string	"\002Latin_Ext_E"
	.string	"Latin_Extended_E"
	.string	"\002Linear_A"
	.string	"Linear_A"
	.string	"\002Mahajani"
	.string	"Mahajani"
	.string	"\002Manichaean"
	.string	"Manichaean"
	.string	"\002Mende_Kikakui"
	.string	"Mende_Kikakui"
	.string	"\002Modi"
	.string	"Modi"
	.string	"\002Mro"
	.string	"Mro"
	.string	"\002Myanmar_Ext_B"
	.string	"Myanmar_Extended_B"
	.string	"\002Nabataean"
	.string	"Nabataean"
	.string	"\002Old_North_Arabian"
	.string	"Old_North_Arabian"
	.string	"\002Old_Permic"
	.string	"Old_Permic"
	.string	"\002Ornamental_Dingbats"
	.string	"Ornamental_Dingbats"
	.ascii	"\002Pahawh_H"
	.string	"mong"
	.string	"Pahawh_Hmong"
	.string	"\002Palmyrene"
	.string	"Palmyrene"
	.string	"\002Pau_Cin_Hau"
	.string	"Pau_Cin_Hau"
	.string	"\002Psalter_Pahlavi"
	.string	"Psalter_Pahlavi"
	.string	"\002Shorthand_Format_Controls"
	.string	"Shorthand_Format_Controls"
	.string	"\002Siddham"
	.string	"Siddham"
	.string	"\002Sinhala_Archaic_Numbers"
	.string	"Sinhala_Archaic_Numbers"
	.string	"\002Sup_Arrows_C"
	.string	"Supplemental_Arrows_C"
	.string	"\002Tirhuta"
	.string	"Tirhuta"
	.string	"\002Warang_Citi"
	.string	"Warang_Citi"
	.string	"\002Ahom"
	.string	"Ahom"
	.string	"\002Anatolian_Hieroglyphs"
	.string	"Anatolian_Hieroglyphs"
	.string	"\002Cherokee_Sup"
	.string	"Cherokee_Supplement"
	.string	"\002CJK_Ext_E"
	.string	"CJK_Unified_Ideographs_Extension_E"
	.string	"\002Early_Dynastic_Cuneiform"
	.string	"Early_Dynastic_Cuneiform"
	.string	"\002Hatran"
	.string	"Hatran"
	.string	"\002Multani"
	.string	"Multani"
	.string	"\002Old_Hungarian"
	.string	"Old_Hungarian"
	.string	"\002Sup_Symbols_And_Pictographs"
	.string	"Supplemental_Symbols_And_Pictographs"
	.string	"\002Sutton_SignWriting"
	.string	"Sutton_SignWriting"
	.string	"\002Adlam"
	.string	"Adlam"
	.string	"\002Bhaiksuki"
	.string	"Bhaiksuki"
	.string	"\002Cyrillic_Ext_C"
	.string	"Cyrillic_Extended_C"
	.string	"\002Glagolitic_Sup"
	.string	"Glagolitic_Supplement"
	.string	"\002Ideographic_Symbols"
	.string	"Ideographic_Symbols_And_Punctuation"
	.string	"\002Marchen"
	.string	"Marchen"
	.string	"\002Mongolian_Sup"
	.string	"Mongolian_Supplement"
	.string	"\002Newa"
	.string	"Newa"
	.string	"\002Osage"
	.string	"Osage"
	.string	"\002Tangut"
	.string	"Tangut"
	.string	"\002Tangut_Components"
	.string	"Tangut_Components"
	.string	"\002CJK_Ext_F"
	.string	"CJK_Unified_Ideographs_Extension_F"
	.string	"\002Kana_Ext_A"
	.string	"Kana_Extended_A"
	.string	"\002Masaram_Gondi"
	.string	"Masaram_Gondi"
	.string	"\002Nushu"
	.string	"Nushu"
	.string	"\002Soyombo"
	.string	"Soyombo"
	.string	"\002Syriac_Sup"
	.string	"Syriac_Supplement"
	.string	"\002Zanabazar_Square"
	.string	"Zanabazar_Square"
	.string	"\002Chess_Symbols"
	.string	"Chess_Symbols"
	.string	"\002Dogra"
	.string	"Dogra"
	.string	"\002Georgian_Ext"
	.string	"Georgian_Extended"
	.string	"\002Gunjala_Gondi"
	.string	"Gunjala_Gondi"
	.string	"\002Hanifi_Rohingya"
	.string	"Hanifi_Rohingya"
	.string	"\002Indic_Siyaq_Numbers"
	.string	"Indic_Siyaq_Numbers"
	.string	"\002Makasar"
	.string	"Makasar"
	.string	"\002Mayan_Numerals"
	.string	"Mayan_Numerals"
	.string	"\002Medefaidrin"
	.string	"Medefaidrin"
	.string	"\002Old_Sogdian"
	.string	"Old_Sogdian"
	.string	"\002Sogdian"
	.string	"Sogdian"
	.string	"\002Egyptian_Hieroglyph_Format_Controls"
	.string	"Egyptian_Hieroglyph_Format_Controls"
	.string	"\002Elymaic"
	.string	"Elymaic"
	.string	"\002Nandinagari"
	.string	"Nandinagari"
	.string	"\002Nyiakeng_Puachue_Hmong"
	.string	"Nyiakeng_Puachue_Hmong"
	.string	"\002Ottoman_Siyaq_Numbers"
	.string	"Ottoman_Siyaq_Numbers"
	.string	"\002Small_Kana_Ext"
	.string	"Small_Kana_Extension"
	.string	"\002Symbols_And_Pictographs_Ext_A"
	.string	"Symbols_And_Pictographs_Extended_A"
	.string	"\002Tamil_Sup"
	.string	"Tamil_Supplement"
	.string	"\002Wancho"
	.string	"Wancho"
	.string	"\002Chorasmian"
	.string	"Chorasmian"
	.string	"\002CJK_Ext_G"
	.string	"CJK_Unified_Ideographs_Extension_G"
	.string	"\002Dives_Akuru"
	.string	"Dives_Akuru"
	.string	"\002Khitan_Small_Script"
	.string	"Khitan_Small_Script"
	.string	"\002Lisu_Sup"
	.string	"Lisu_Supplement"
	.string	"\002Symbols_For_Legacy_Computing"
	.ascii	"Symbols_For_Legacy_Comput"
	.string	"ing"
	.string	"\002Tangut_Sup"
	.string	"Tangut_Supplement"
	.string	"\002Yezidi"
	.string	"Yezidi"
	.string	"\002ccc"
	.string	"Canonical_Combining_Class"
	.string	"\002dt"
	.string	"Decomposition_Type"
	.string	"\003None"
	.string	"None"
	.string	"none"
	.string	"\003Can"
	.string	"Canonical"
	.string	"can"
	.string	"\003Com"
	.string	"Compat"
	.string	"com"
	.string	"\003Enc"
	.string	"Circle"
	.string	"enc"
	.string	"\003Fin"
	.string	"Final"
	.string	"fin"
	.string	"\003Font"
	.string	"Font"
	.string	"font"
	.string	"\003Fra"
	.string	"Fraction"
	.string	"fra"
	.string	"\003Init"
	.string	"Initial"
	.string	"init"
	.string	"\003Iso"
	.string	"Isolated"
	.string	"iso"
	.string	"\003Med"
	.string	"Medial"
	.string	"med"
	.string	"\003Nar"
	.string	"Narrow"
	.string	"nar"
	.string	"\003Nb"
	.string	"Nobreak"
	.string	"nb"
	.string	"\003Sml"
	.string	"Small"
	.string	"sml"
	.string	"\003Sqr"
	.string	"Square"
	.string	"sqr"
	.string	"\003Sub"
	.string	"Sub"
	.string	"sub"
	.string	"\003Sup"
	.string	"Super"
	.string	"sup"
	.string	"\003Vert"
	.string	"Vertical"
	.string	"vert"
	.string	"\003Wide"
	.string	"Wide"
	.string	"wide"
	.string	"\002ea"
	.string	"East_Asian_Width"
	.string	"\002N"
	.string	"Neutral"
	.string	"\002A"
	.string	"Ambiguous"
	.string	"\002H"
	.string	"Halfwidth"
	.string	"\002F"
	.string	"Fullwidth"
	.string	"\002Na"
	.string	"Narrow"
	.string	"\002W"
	.string	"Wide"
	.string	"\002gc"
	.string	"General_Category"
	.string	"\002Cn"
	.string	"Unassigned"
	.string	"\002Lu"
	.string	"Uppercase_Letter"
	.string	"\002Ll"
	.string	"Lowercase_Letter"
	.string	"\002Lt"
	.string	"Titlecase_Letter"
	.string	"\002Lm"
	.string	"Modifier_Letter"
	.string	"\002Lo"
	.string	"Other_Letter"
	.string	"\002Mn"
	.string	"Nonspacing_Mark"
	.string	"\002Me"
	.string	"Enclosing_Mark"
	.string	"\002Mc"
	.string	"Spacing_Mark"
	.string	"\003Nd"
	.string	"Decimal_Number"
	.string	"digit"
	.string	"\002Nl"
	.string	"Letter_Number"
	.string	"\002No"
	.string	"Other_Number"
	.string	"\002Zs"
	.string	"Space_Separator"
	.string	"\002Zl"
	.string	"Line_Separator"
	.string	"\002Zp"
	.string	"Paragraph_Separator"
	.string	"\003Cc"
	.string	"Control"
	.string	"cntrl"
	.string	"\002Cf"
	.string	"Format"
	.string	"\002Co"
	.string	"Private_Use"
	.string	"\002Cs"
	.string	"Surrogate"
	.string	"\002Pd"
	.string	"Dash_Punctuation"
	.string	"\002Ps"
	.string	"Open_Punctuation"
	.string	"\002Pe"
	.string	"Close_Punctuation"
	.string	"\002Pc"
	.string	"Connector_Punctuation"
	.string	"\002Po"
	.string	"Other_Punctuation"
	.string	"\002Sm"
	.string	"Math_Symbol"
	.string	"\002Sc"
	.string	"Currency_Symbol"
	.string	"\002Sk"
	.string	"Modifier_Symbol"
	.string	"\002So"
	.string	"Other_Symbol"
	.string	"\002Pi"
	.string	"Initial_Punctuation"
	.string	"\002Pf"
	.string	"Final_Punctuation"
	.string	"\002jg"
	.string	"Joining_Group"
	.string	"\002No_Joining_Group"
	.string	"No_Joining_Group"
	.string	"\002Ain"
	.string	"Ain"
	.string	"\002Alaph"
	.string	"Alaph"
	.string	"\002Alef"
	.string	"Alef"
	.string	"\002Beh"
	.string	"Beh"
	.string	"\002Beth"
	.string	"Beth"
	.string	"\002Dal"
	.string	"Dal"
	.string	"\002Dalath_Rish"
	.string	"Dalath_Rish"
	.string	"\002E"
	.string	"E"
	.string	"\002Feh"
	.string	"Feh"
	.string	"\002Final_Semkath"
	.string	"Final_Semkath"
	.string	"\002Gaf"
	.string	"Gaf"
	.string	"\002Gamal"
	.string	"Gamal"
	.string	"\002Hah"
	.string	"Hah"
	.string	"\002Teh_Marbuta_Goal"
	.string	"Hamza_On_Heh_Goal"
	.string	"\002He"
	.string	"He"
	.string	"\002Heh"
	.string	"Heh"
	.string	"\002Heh_Goal"
	.string	"Heh_Goal"
	.string	"\002Heth"
	.string	"Heth"
	.string	"\002Kaf"
	.string	"Kaf"
	.string	"\002Kaph"
	.string	"Kaph"
	.string	"\002Knotted_Heh"
	.string	"Knotted_Heh"
	.string	"\002Lam"
	.string	"Lam"
	.string	"\002Lamadh"
	.string	"Lamadh"
	.string	"\002Meem"
	.string	"Meem"
	.string	"\002Mim"
	.string	"Mim"
	.string	"\002Noon"
	.string	"Noon"
	.string	"\002Nun"
	.string	"Nun"
	.string	"\002Pe"
	.string	"Pe"
	.string	"\002Qaf"
	.string	"Qaf"
	.string	"\002Qaph"
	.string	"Qaph"
	.string	"\002Reh"
	.string	"Reh"
	.string	"\002Reversed_Pe"
	.string	"Reversed_Pe"
	.string	"\002Sad"
	.string	"Sad"
	.string	"\002Sadhe"
	.string	"Sadhe"
	.string	"\002Seen"
	.string	"Seen"
	.string	"\002Semkath"
	.string	"Semkath"
	.string	"\002Shin"
	.string	"Shin"
	.string	"\002Swash_Kaf"
	.string	"Swash_Kaf"
	.string	"\002Syriac_Waw"
	.string	"Syriac_Waw"
	.string	"\002Tah"
	.string	"Tah"
	.string	"\002Taw"
	.string	"Taw"
	.string	"\002Teh_Marbuta"
	.string	"Teh_Marbuta"
	.string	"\002Teth"
	.string	"Teth"
	.string	"\002Waw"
	.string	"Waw"
	.string	"\002Yeh"
	.string	"Yeh"
	.string	"\002Yeh_Barree"
	.string	"Yeh_Barree"
	.string	"\002Yeh_With_Tail"
	.string	"Yeh_With_Tail"
	.string	"\002Yudh"
	.string	"Yudh"
	.string	"\002Yudh_He"
	.string	"Yudh_He"
	.string	"\002Zain"
	.string	"Zain"
	.string	"\002Fe"
	.string	"Fe"
	.string	"\002Khaph"
	.string	"Khaph"
	.string	"\002Zhain"
	.string	"Zhain"
	.string	"\002Burushaski_Yeh_Barree"
	.string	"Burushaski_Yeh_Barree"
	.string	"\002Farsi_Yeh"
	.string	"Farsi_Yeh"
	.string	"\002Nya"
	.string	"Nya"
	.string	"\002Rohingya_Yeh"
	.string	"Rohingya_Yeh"
	.string	"\002Manichaean_Aleph"
	.string	"Manichaean_Aleph"
	.ascii	"\002Ma"
	.string	"nichaean_Ayin"
	.string	"Manichaean_Ayin"
	.string	"\002Manichaean_Beth"
	.string	"Manichaean_Beth"
	.string	"\002Manichaean_Daleth"
	.string	"Manichaean_Daleth"
	.string	"\002Manichaean_Dhamedh"
	.string	"Manichaean_Dhamedh"
	.string	"\002Manichaean_Five"
	.string	"Manichaean_Five"
	.string	"\002Manichaean_Gimel"
	.string	"Manichaean_Gimel"
	.string	"\002Manichaean_Heth"
	.string	"Manichaean_Heth"
	.string	"\002Manichaean_Hundred"
	.string	"Manichaean_Hundred"
	.string	"\002Manichaean_Kaph"
	.string	"Manichaean_Kaph"
	.string	"\002Manichaean_Lamedh"
	.string	"Manichaean_Lamedh"
	.string	"\002Manichaean_Mem"
	.string	"Manichaean_Mem"
	.string	"\002Manichaean_Nun"
	.string	"Manichaean_Nun"
	.string	"\002Manichaean_One"
	.string	"Manichaean_One"
	.string	"\002Manichaean_Pe"
	.string	"Manichaean_Pe"
	.string	"\002Manichaean_Qoph"
	.string	"Manichaean_Qoph"
	.string	"\002Manichaean_Resh"
	.string	"Manichaean_Resh"
	.string	"\002Manichaean_Sadhe"
	.string	"Manichaean_Sadhe"
	.string	"\002Manichaean_Samekh"
	.string	"Manichaean_Samekh"
	.string	"\002Manichaean_Taw"
	.string	"Manichaean_Taw"
	.string	"\002Manichaean_Ten"
	.string	"Manichaean_Ten"
	.string	"\002Manichaean_Teth"
	.string	"Manichaean_Teth"
	.string	"\002Manichaean_Thamedh"
	.string	"Manichaean_Thamedh"
	.string	"\002Manichaean_Twenty"
	.string	"Manichaean_Twenty"
	.string	"\002Manichaean_Waw"
	.string	"Manichaean_Waw"
	.string	"\002Manichaean_Yodh"
	.string	"Manichaean_Yodh"
	.string	"\002Manichaean_Zayin"
	.string	"Manichaean_Zayin"
	.string	"\002Straight_Waw"
	.string	"Straight_Waw"
	.string	"\002African_Feh"
	.string	"African_Feh"
	.string	"\002African_Noon"
	.string	"African_Noon"
	.string	"\002African_Qaf"
	.string	"African_Qaf"
	.string	"\002Malayalam_Bha"
	.string	"Malayalam_Bha"
	.string	"\002Malayalam_Ja"
	.string	"Malayalam_Ja"
	.string	"\002Malayalam_Lla"
	.string	"Malayalam_Lla"
	.string	"\002Malayalam_Llla"
	.string	"Malayalam_Llla"
	.string	"\002Malayalam_Nga"
	.string	"Malayalam_Nga"
	.string	"\002Malayalam_Nna"
	.string	"Malayalam_Nna"
	.string	"\002Malayalam_Nnna"
	.string	"Malayalam_Nnna"
	.string	"\002Malayalam_Nya"
	.string	"Malayalam_Nya"
	.string	"\002Malayalam_Ra"
	.string	"Malayalam_Ra"
	.string	"\002Malayalam_Ssa"
	.string	"Malayalam_Ssa"
	.string	"\002Malayalam_Tta"
	.string	"Malayalam_Tta"
	.string	"\002Hanifi_Rohingya_Kinna_Ya"
	.string	"Hanifi_Rohingya_Kinna_Ya"
	.string	"\002Hanifi_Rohingya_Pa"
	.string	"Hanifi_Rohingya_Pa"
	.string	"\002jt"
	.string	"Joining_Type"
	.string	"\002U"
	.string	"Non_Joining"
	.string	"\002C"
	.string	"Join_Causing"
	.string	"\002D"
	.string	"Dual_Joining"
	.string	"\002L"
	.string	"Left_Joining"
	.string	"\002R"
	.string	"Right_Joining"
	.string	"\002T"
	.string	"Transparent"
	.string	"\002lb"
	.string	"Line_Break"
	.string	"\002XX"
	.string	"Unknown"
	.string	"\002AI"
	.string	"Ambiguous"
	.string	"\002AL"
	.string	"Alphabetic"
	.string	"\002B2"
	.string	"Break_Both"
	.string	"\002BA"
	.string	"Break_After"
	.string	"\002BB"
	.string	"Break_Before"
	.string	"\002BK"
	.string	"Mandatory_Break"
	.string	"\002CB"
	.string	"Contingent_Break"
	.string	"\002CL"
	.string	"Close_Punctuation"
	.string	"\002CM"
	.string	"Combining_Mark"
	.string	"\002CR"
	.string	"Carriage_Return"
	.string	"\002EX"
	.string	"Exclamation"
	.string	"\002GL"
	.string	"Glue"
	.string	"\002HY"
	.string	"Hyphen"
	.string	"\002ID"
	.string	"Ideographic"
	.string	"\003IN"
	.string	"Inseparable"
	.string	"Inseperable"
	.string	"\002IS"
	.string	"Infix_Numeric"
	.string	"\002LF"
	.string	"Line_Feed"
	.string	"\002NS"
	.string	"Nonstarter"
	.string	"\002NU"
	.string	"Numeric"
	.string	"\002OP"
	.string	"Open_Punctuation"
	.string	"\002PO"
	.string	"Postfix_Numeric"
	.string	"\002PR"
	.string	"Prefix_Numeric"
	.string	"\002QU"
	.string	"Quotation"
	.string	"\002SA"
	.string	"Complex_Context"
	.string	"\002SG"
	.string	"Surrogate"
	.string	"\002SP"
	.string	"Space"
	.string	"\002SY"
	.string	"Break_Symbols"
	.string	"\002ZW"
	.string	"ZWSpace"
	.ascii	"\002NL"
	.string	""
	.string	"Next_Line"
	.string	"\002WJ"
	.string	"Word_Joiner"
	.string	"\002H2"
	.string	"H2"
	.string	"\002H3"
	.string	"H3"
	.string	"\002JL"
	.string	"JL"
	.string	"\002JT"
	.string	"JT"
	.string	"\002JV"
	.string	"JV"
	.string	"\002CP"
	.string	"Close_Parenthesis"
	.string	"\002CJ"
	.string	"Conditional_Japanese_Starter"
	.string	"\002HL"
	.string	"Hebrew_Letter"
	.string	"\002EB"
	.string	"E_Base"
	.string	"\002EM"
	.string	"E_Modifier"
	.string	"\002ZWJ"
	.string	"ZWJ"
	.string	"\002nt"
	.string	"Numeric_Type"
	.string	"\002None"
	.string	"None"
	.string	"\002De"
	.string	"Decimal"
	.string	"\002Di"
	.string	"Digit"
	.string	"\002Nu"
	.string	"Numeric"
	.string	"\002sc"
	.string	"Script"
	.string	"\002Zyyy"
	.string	"Common"
	.string	"\003Zinh"
	.string	"Inherited"
	.string	"Qaai"
	.string	"\002Arab"
	.string	"Arabic"
	.string	"\002Armn"
	.string	"Armenian"
	.string	"\002Beng"
	.string	"Bengali"
	.string	"\002Bopo"
	.string	"Bopomofo"
	.string	"\002Cher"
	.string	"Cherokee"
	.string	"\003Copt"
	.string	"Coptic"
	.string	"Qaac"
	.string	"\002Cyrl"
	.string	"Cyrillic"
	.string	"\002Dsrt"
	.string	"Deseret"
	.string	"\002Deva"
	.string	"Devanagari"
	.string	"\002Ethi"
	.string	"Ethiopic"
	.string	"\002Geor"
	.string	"Georgian"
	.string	"\002Goth"
	.string	"Gothic"
	.string	"\002Grek"
	.string	"Greek"
	.string	"\002Gujr"
	.string	"Gujarati"
	.string	"\002Guru"
	.string	"Gurmukhi"
	.string	"\002Hani"
	.string	"Han"
	.string	"\002Hang"
	.string	"Hangul"
	.string	"\002Hebr"
	.string	"Hebrew"
	.string	"\002Hira"
	.string	"Hiragana"
	.string	"\002Knda"
	.string	"Kannada"
	.string	"\002Kana"
	.string	"Katakana"
	.string	"\002Khmr"
	.string	"Khmer"
	.string	"\002Laoo"
	.string	"Lao"
	.string	"\002Latn"
	.string	"Latin"
	.string	"\002Mlym"
	.string	"Malayalam"
	.string	"\002Mong"
	.string	"Mongolian"
	.string	"\002Mymr"
	.string	"Myanmar"
	.string	"\002Ogam"
	.string	"Ogham"
	.string	"\002Ital"
	.string	"Old_Italic"
	.string	"\002Orya"
	.string	"Oriya"
	.string	"\002Runr"
	.string	"Runic"
	.string	"\002Sinh"
	.string	"Sinhala"
	.string	"\002Syrc"
	.string	"Syriac"
	.string	"\002Taml"
	.string	"Tamil"
	.string	"\002Telu"
	.string	"Telugu"
	.string	"\002Thaa"
	.string	"Thaana"
	.string	"\002Tibt"
	.string	"Tibetan"
	.string	"\002Cans"
	.string	"Canadian_Aboriginal"
	.string	"\002Yiii"
	.string	"Yi"
	.string	"\002Tglg"
	.string	"Tagalog"
	.string	"\002Hano"
	.string	"Hanunoo"
	.string	"\002Buhd"
	.string	"Buhid"
	.string	"\002Tagb"
	.string	"Tagbanwa"
	.string	"\002Brai"
	.string	"Braille"
	.string	"\002Cprt"
	.string	"Cypriot"
	.string	"\002Limb"
	.string	"Limbu"
	.string	"\002Linb"
	.string	"Linear_B"
	.string	"\002Osma"
	.string	"Osmanya"
	.string	"\002Shaw"
	.string	"Shavian"
	.string	"\002Tale"
	.string	"Tai_Le"
	.string	"\002Ugar"
	.string	"Ugaritic"
	.string	"\002Hrkt"
	.string	"Katakana_Or_Hiragana"
	.string	"\002Bugi"
	.string	"Buginese"
	.string	"\002Glag"
	.string	"Glagolitic"
	.string	"\002Khar"
	.string	"Kharoshthi"
	.string	"\002Sylo"
	.string	"Syloti_Nagri"
	.string	"\002Talu"
	.string	"New_Tai_Lue"
	.string	"\002Tfng"
	.string	"Tifinagh"
	.string	"\002Xpeo"
	.string	"Old_Persian"
	.string	"\002Bali"
	.string	"Balinese"
	.string	"\002Batk"
	.string	"Batak"
	.string	"\002Blis"
	.string	"Blis"
	.string	"\002Brah"
	.string	"Brahmi"
	.string	"\002Cirt"
	.string	"Cirt"
	.string	"\002Cyrs"
	.string	"Cyrs"
	.string	"\002Egyd"
	.string	"Egyd"
	.string	"\002Egyh"
	.string	"Egyh"
	.string	"\002Egyp"
	.string	"Egyptian_Hieroglyphs"
	.string	"\002Geok"
	.string	"Geok"
	.string	"\002Hans"
	.string	"Hans"
	.string	"\002Hant"
	.string	"Hant"
	.string	"\002Hmng"
	.string	"Pahawh_Hmong"
	.string	"\002Hung"
	.string	"Old_Hungarian"
	.string	"\002Inds"
	.string	"Inds"
	.string	"\002Java"
	.string	"Javanese"
	.string	"\002Kali"
	.string	"Kayah_Li"
	.string	"\002Latf"
	.string	"Latf"
	.string	"\002Latg"
	.string	"Latg"
	.string	"\002Lepc"
	.string	"Lepcha"
	.string	"\002Lina"
	.string	"Linear_A"
	.string	"\002Mand"
	.string	"Mandaic"
	.string	"\002Maya"
	.string	"Maya"
	.string	"\002Mero"
	.string	"Meroitic_Hieroglyphs"
	.string	"\002Nkoo"
	.string	"Nko"
	.string	"\002Orkh"
	.string	"Old_Turkic"
	.string	"\002Perm"
	.string	"Old_Permic"
	.string	"\002Phag"
	.string	"Phags_Pa"
	.string	"\002Phnx"
	.string	"Phoenician"
	.string	"\002Plrd"
	.string	"Miao"
	.string	"\002Roro"
	.string	"Roro"
	.string	"\002Sara"
	.string	"Sara"
	.string	"\002Syre"
	.string	"Syre"
	.string	"\002Syrj"
	.string	"Syrj"
	.string	"\002Syrn"
	.string	"Syrn"
	.string	"\002Teng"
	.string	"Teng"
	.string	"\002Vaii"
	.string	"Vai"
	.string	"\002Visp"
	.string	"Visp"
	.string	"\002Xsux"
	.string	"Cuneiform"
	.string	"\002Zxxx"
	.string	"Zxxx"
	.string	"\002Zzzz"
	.string	"Unknown"
	.string	"\002Cari"
	.string	"Carian"
	.string	"\002Jpan"
	.string	"Jpan"
	.string	"\002Lana"
	.string	"Tai_Tham"
	.string	"\002Lyci"
	.string	"Lycian"
	.string	"\002Lydi"
	.string	"Lydian"
	.string	"\002Olck"
	.string	"Ol_Chiki"
	.string	"\002Rjng"
	.string	"Rejang"
	.string	"\002Saur"
	.string	"Saurashtra"
	.string	"\002Sgnw"
	.string	"SignWriting"
	.string	"\002Sund"
	.string	"Sundanese"
	.string	"\002Moon"
	.string	"Moon"
	.string	"\002Mtei"
	.string	"Meetei_Mayek"
	.string	"\002Armi"
	.string	"Imperial_Aramaic"
	.string	"\002Avst"
	.string	"Avestan"
	.string	"\002Cakm"
	.string	"Chakma"
	.string	"\002Kore"
	.string	"Kore"
	.string	"\002Kthi"
	.string	"Kaithi"
	.string	"\002Mani"
	.string	"Manichaean"
	.string	"\002Phli"
	.string	"Inscriptional_Pahlavi"
	.string	"\002Phlp"
	.ascii	"Psalter_Pahl"
	.string	"avi"
	.string	"\002Phlv"
	.string	"Phlv"
	.string	"\002Prti"
	.string	"Inscriptional_Parthian"
	.string	"\002Samr"
	.string	"Samaritan"
	.string	"\002Tavt"
	.string	"Tai_Viet"
	.string	"\002Zmth"
	.string	"Zmth"
	.string	"\002Zsym"
	.string	"Zsym"
	.string	"\002Bamu"
	.string	"Bamum"
	.string	"\002Nkgb"
	.string	"Nkgb"
	.string	"\002Sarb"
	.string	"Old_South_Arabian"
	.string	"\002Bass"
	.string	"Bassa_Vah"
	.string	"\002Dupl"
	.string	"Duployan"
	.string	"\002Elba"
	.string	"Elbasan"
	.string	"\002Gran"
	.string	"Grantha"
	.string	"\002Kpel"
	.string	"Kpel"
	.string	"\002Loma"
	.string	"Loma"
	.string	"\002Mend"
	.string	"Mende_Kikakui"
	.string	"\002Merc"
	.string	"Meroitic_Cursive"
	.string	"\002Narb"
	.string	"Old_North_Arabian"
	.string	"\002Nbat"
	.string	"Nabataean"
	.string	"\002Palm"
	.string	"Palmyrene"
	.string	"\002Sind"
	.string	"Khudawadi"
	.string	"\002Wara"
	.string	"Warang_Citi"
	.string	"\002Afak"
	.string	"Afak"
	.string	"\002Jurc"
	.string	"Jurc"
	.string	"\002Mroo"
	.string	"Mro"
	.string	"\002Nshu"
	.string	"Nushu"
	.string	"\002Shrd"
	.string	"Sharada"
	.string	"\002Sora"
	.string	"Sora_Sompeng"
	.string	"\002Takr"
	.string	"Takri"
	.string	"\002Tang"
	.string	"Tangut"
	.string	"\002Wole"
	.string	"Wole"
	.string	"\002Hluw"
	.string	"Anatolian_Hieroglyphs"
	.string	"\002Khoj"
	.string	"Khojki"
	.string	"\002Tirh"
	.string	"Tirhuta"
	.string	"\002Aghb"
	.string	"Caucasian_Albanian"
	.string	"\002Mahj"
	.string	"Mahajani"
	.string	"\002Hatr"
	.string	"Hatran"
	.string	"\002Mult"
	.string	"Multani"
	.string	"\002Pauc"
	.string	"Pau_Cin_Hau"
	.string	"\002Sidd"
	.string	"Siddham"
	.string	"\002Adlm"
	.string	"Adlam"
	.string	"\002Bhks"
	.string	"Bhaiksuki"
	.string	"\002Marc"
	.string	"Marchen"
	.string	"\002Osge"
	.string	"Osage"
	.string	"\002Hanb"
	.string	"Hanb"
	.string	"\002Jamo"
	.string	"Jamo"
	.string	"\002Zsye"
	.string	"Zsye"
	.string	"\002Gonm"
	.string	"Masaram_Gondi"
	.string	"\002Soyo"
	.string	"Soyombo"
	.string	"\002Zanb"
	.string	"Zanabazar_Square"
	.string	"\002Dogr"
	.string	"Dogra"
	.string	"\002Gong"
	.string	"Gunjala_Gondi"
	.string	"\002Maka"
	.string	"Makasar"
	.string	"\002Medf"
	.string	"Medefaidrin"
	.string	"\002Rohg"
	.string	"Hanifi_Rohingya"
	.string	"\002Sogd"
	.string	"Sogdian"
	.string	"\002Sogo"
	.string	"Old_Sogdian"
	.string	"\002Elym"
	.string	"Elymaic"
	.string	"\002Hmnp"
	.string	"Nyiakeng_Puachue_Hmong"
	.string	"\002Nand"
	.string	"Nandinagari"
	.string	"\002Wcho"
	.string	"Wancho"
	.string	"\002Chrs"
	.string	"Chorasmian"
	.string	"\002Diak"
	.string	"Dives_Akuru"
	.string	"\002Kits"
	.string	"Khitan_Small_Script"
	.string	"\002Yezi"
	.string	"Yezidi"
	.string	"\002hst"
	.string	"Hangul_Syllable_Type"
	.string	"\002NA"
	.string	"Not_Applicable"
	.string	"\002L"
	.string	"Leading_Jamo"
	.string	"\002V"
	.string	"Vowel_Jamo"
	.string	"\002T"
	.string	"Trailing_Jamo"
	.string	"\002LV"
	.string	"LV_Syllable"
	.string	"\002LVT"
	.string	"LVT_Syllable"
	.string	"\002NFD_QC"
	.string	"NFD_Quick_Check"
	.string	"\002N"
	.string	"No"
	.string	"\002Y"
	.string	"Yes"
	.string	"\002NFKD_QC"
	.string	"NFKD_Quick_Check"
	.string	"\002NFC_QC"
	.string	"NFC_Quick_Check"
	.string	"\002M"
	.string	"Maybe"
	.string	"\002NFKC_QC"
	.string	"NFKC_Quick_Check"
	.string	"\002lccc"
	.string	"Lead_Canonical_Combining_Class"
	.string	"\002tccc"
	.string	"Trail_Canonical_Combining_Class"
	.string	"\002GCB"
	.string	"Grapheme_Cluster_Break"
	.string	"\002XX"
	.string	"Other"
	.string	"\002CN"
	.string	"Control"
	.string	"\002CR"
	.string	"CR"
	.string	"\002EX"
	.string	"Extend"
	.string	"\002L"
	.string	"L"
	.string	"\002LF"
	.string	"LF"
	.string	"\002LV"
	.string	"LV"
	.string	"\002LVT"
	.string	"LVT"
	.string	"\002T"
	.string	"T"
	.string	"\002V"
	.string	"V"
	.string	"\002SM"
	.string	"SpacingMark"
	.string	"\002PP"
	.string	"Prepend"
	.string	"\002EBG"
	.string	"E_Base_GAZ"
	.string	"\002GAZ"
	.string	"Glue_After_Zwj"
	.string	"\002SB"
	.string	"Sentence_Break"
	.string	"\002AT"
	.string	"ATerm"
	.string	"\002CL"
	.string	"Close"
	.string	"\002FO"
	.string	"Format"
	.string	"\002LO"
	.string	"Lower"
	.string	"\002LE"
	.string	"OLetter"
	.string	"\002SE"
	.string	"Sep"
	.string	"\002SP"
	.string	"Sp"
	.string	"\002ST"
	.string	"STerm"
	.string	"\002UP"
	.string	"Upper"
	.string	"\002SC"
	.string	"SContinue"
	.string	"\002WB"
	.string	"Word_Break"
	.string	"\002LE"
	.string	"ALetter"
	.string	"\002KA"
	.string	"Katakana"
	.string	"\002ML"
	.string	"MidLetter"
	.string	"\002MN"
	.string	"MidNum"
	.string	"\002EX"
	.string	"ExtendNumLet"
	.string	"\002Extend"
	.string	"Extend"
	.string	"\002MB"
	.string	"MidNumLet"
	.string	"\002NL"
	.string	"Newline"
	.string	"\002SQ"
	.string	"Single_Quote"
	.string	"\002DQ"
	.string	"Double_Quote"
	.string	"\002WSegSpace"
	.string	"WSegSpace"
	.string	"\002bpt"
	.string	"Bidi_Paired_Bracket_Type"
	.string	"\002n"
	.string	"None"
	.string	"\002o"
	.string	"Open"
	.string	"\002c"
	.string	"Close"
	.string	"\002InPC"
	.string	"Indic_Positional_Category"
	.string	"\002NA"
	.string	"NA"
	.string	"\002Bottom"
	.string	"Bottom"
	.string	"\002Bottom_And_Left"
	.string	"Bottom_And_Left"
	.string	"\002Bottom_And_Right"
	.ascii	"Bottom_And_Ri"
	.string	"ght"
	.string	"\002Left"
	.string	"Left"
	.string	"\002Left_And_Right"
	.string	"Left_And_Right"
	.string	"\002Overstruck"
	.string	"Overstruck"
	.string	"\002Right"
	.string	"Right"
	.string	"\002Top"
	.string	"Top"
	.string	"\002Top_And_Bottom"
	.string	"Top_And_Bottom"
	.string	"\002Top_And_Bottom_And_Right"
	.string	"Top_And_Bottom_And_Right"
	.string	"\002Top_And_Left"
	.string	"Top_And_Left"
	.string	"\002Top_And_Left_And_Right"
	.string	"Top_And_Left_And_Right"
	.string	"\002Top_And_Right"
	.string	"Top_And_Right"
	.string	"\002Visual_Order_Left"
	.string	"Visual_Order_Left"
	.string	"\002Top_And_Bottom_And_Left"
	.string	"Top_And_Bottom_And_Left"
	.string	"\002InSC"
	.string	"Indic_Syllabic_Category"
	.string	"\002Other"
	.string	"Other"
	.string	"\002Avagraha"
	.string	"Avagraha"
	.string	"\002Bindu"
	.string	"Bindu"
	.string	"\002Brahmi_Joining_Number"
	.string	"Brahmi_Joining_Number"
	.string	"\002Cantillation_Mark"
	.string	"Cantillation_Mark"
	.string	"\002Consonant"
	.string	"Consonant"
	.string	"\002Consonant_Dead"
	.string	"Consonant_Dead"
	.string	"\002Consonant_Final"
	.string	"Consonant_Final"
	.string	"\002Consonant_Head_Letter"
	.string	"Consonant_Head_Letter"
	.string	"\002Consonant_Initial_Postfixed"
	.string	"Consonant_Initial_Postfixed"
	.string	"\002Consonant_Killer"
	.string	"Consonant_Killer"
	.string	"\002Consonant_Medial"
	.string	"Consonant_Medial"
	.string	"\002Consonant_Placeholder"
	.string	"Consonant_Placeholder"
	.string	"\002Consonant_Preceding_Repha"
	.string	"Consonant_Preceding_Repha"
	.string	"\002Consonant_Prefixed"
	.string	"Consonant_Prefixed"
	.string	"\002Consonant_Subjoined"
	.string	"Consonant_Subjoined"
	.string	"\002Consonant_Succeeding_Repha"
	.string	"Consonant_Succeeding_Repha"
	.string	"\002Consonant_With_Stacker"
	.string	"Consonant_With_Stacker"
	.string	"\002Gemination_Mark"
	.string	"Gemination_Mark"
	.string	"\002Invisible_Stacker"
	.string	"Invisible_Stacker"
	.string	"\002Joiner"
	.string	"Joiner"
	.string	"\002Modifying_Letter"
	.string	"Modifying_Letter"
	.string	"\002Non_Joiner"
	.string	"Non_Joiner"
	.string	"\002Nukta"
	.string	"Nukta"
	.string	"\002Number"
	.string	"Number"
	.string	"\002Number_Joiner"
	.string	"Number_Joiner"
	.string	"\002Pure_Killer"
	.string	"Pure_Killer"
	.string	"\002Register_Shifter"
	.string	"Register_Shifter"
	.string	"\002Syllable_Modifier"
	.string	"Syllable_Modifier"
	.string	"\002Tone_Letter"
	.string	"Tone_Letter"
	.string	"\002Tone_Mark"
	.string	"Tone_Mark"
	.string	"\002Virama"
	.string	"Virama"
	.string	"\002Visarga"
	.string	"Visarga"
	.string	"\002Vowel"
	.string	"Vowel"
	.string	"\002Vowel_Dependent"
	.string	"Vowel_Dependent"
	.string	"\002Vowel_Independent"
	.string	"Vowel_Independent"
	.string	"\002vo"
	.string	"Vertical_Orientation"
	.string	"\002R"
	.string	"Rotated"
	.string	"\002Tr"
	.string	"Transformed_Rotated"
	.string	"\002Tu"
	.string	"Transformed_Upright"
	.string	"\002U"
	.string	"Upright"
	.string	"\002gcm"
	.string	"General_Category_Mask"
	.string	"\002C"
	.string	"Other"
	.string	"\002L"
	.string	"Letter"
	.string	"\002LC"
	.string	"Cased_Letter"
	.string	"\003M"
	.string	"Mark"
	.string	"Combining_Mark"
	.string	"\002N"
	.string	"Number"
	.string	"\003P"
	.string	"Punctuation"
	.string	"punct"
	.string	"\002S"
	.string	"Symbol"
	.string	"\002Z"
	.string	"Separator"
	.string	"\002nv"
	.string	"Numeric_Value"
	.string	"\002age"
	.string	"Age"
	.string	"\002bmg"
	.string	"Bidi_Mirroring_Glyph"
	.string	"\002cf"
	.string	"Case_Folding"
	.string	"\002isc"
	.string	"ISO_Comment"
	.string	"\002lc"
	.string	"Lowercase_Mapping"
	.string	"\002na"
	.string	"Name"
	.string	"\003scf"
	.string	"Simple_Case_Folding"
	.string	"sfc"
	.string	"\002slc"
	.string	"Simple_Lowercase_Mapping"
	.string	"\002stc"
	.string	"Simple_Titlecase_Mapping"
	.string	"\002suc"
	.string	"Simple_Uppercase_Mapping"
	.string	"\002tc"
	.ascii	"Titlecase_"
	.string	"Mapping"
	.string	"\002na1"
	.string	"Unicode_1_Name"
	.string	"\002uc"
	.string	"Uppercase_Mapping"
	.string	"\002bpb"
	.string	"Bidi_Paired_Bracket"
	.string	"\002scx"
	.string	"Script_Extensions"
	.globl	_ZN6icu_6712PropNameData10bytesTriesE
	.align 32
	.type	_ZN6icu_6712PropNameData10bytesTriesE, @object
	.size	_ZN6icu_6712PropNameData10bytesTriesE, 14992
_ZN6icu_6712PropNameData10bytesTriesE:
	.string	""
	.ascii	"\025m\303xs\302\022vzvjw\242Rx\001dPi\020d\001c0sb\023tartc`"
	.ascii	"\026ontinuea\023igit\201\003a.eLo\303\030si\036riationselect"
	.ascii	"ori\020r\037ticalorientation\303\030\003b\303\024h2oBs\023pa"
	.ascii	"ce_\027i"
	.string	"tespace_\026rdbreak\303\024s\242It\244;u\003c\331@\fiRnXp\022per\\\023case\\\026mapping\331@\f\022deo[\020i\001c>f\033iedideograph[\027ode1name\331@\013\ni\204p\031p0t6u\020c\331@\t\022ace_\001c\331@\be\021rmgi<l\242_o\027ftdottedW\023mple\003cPlht\212u\036ppercasemapping\331@\t\031asefolding\331@\006\036owercasemapping\331@\007\036itlecasemapping\331@\b\020c\331@\007b\303\023c4dWenf\020c\331@\006\302\n\002f\331@\006r(x\331p"
	.string	"\022ipt\302\n\031extensions\331p"
	.string	"\001gjn\001sTt\023ence\001b4t\026erminalg\023reak\303\023\024itivee\001m.s\023tarts\031entstarters\003cferi\230r\031ailcanonic\037alcombiningclass\303\021\330@\n\021cc\303\021\021rmX\036inalpunctuationY\035tlecasemapping\331@\nmpnvp\242\361q\244Cr\002a(e2i\235\024dicalU\036gionalindicator\235\022athO\006o9o2t\303\tuTv\3310"
	.string	"\022nch\037aractercodepointQ\024meric\001t2v\023alue\3310"
	.ascii	"\022ype\303\ta\242wc\242\202f\002c\230d\242Sk\001cVd\001iBq\001"
	.ascii	"c\303\ru\027ickcheck\303\r\023nertm\001iBq\001c\303\017u\027"
	.ascii	"ickcheck\303\017\023nertq\001iBq\001c\303\016u\027ickcheck\303"
	.ascii	"\016\023nerto\001iBq\001c\303\fu\027ickcheck\303\f\023nertk\330"
	.ascii	"@\005\0011\331@\013m\020e\331@\005\022harQ\002alc\242Lr\001e"
	.ascii	"*i\021nt\177\026pendedc\037oncatenationmark\237\020t\002s,t0"
	.ascii	"w\020sw\021ynu\022ern\001s8w\030hitespacew\024yntaxu\020m\237"
	.ascii	"\001m<u\032otationmarkS\022arkSf\301\370i\301<i\242oj\244\tl"
	.ascii	"\004b\303\bc\214e\230i\242Vo\002eKgLw\021erL\023caseL\026map"
	.ascii	"ping\331@\004\021ic\037alorderexceptionK\330@\004\021cc\303\020"
	.ascii	"\030adcanonic\037alcombiningclass\303\020\026nebreak\303\b\002"
	.ascii	"dJn\242[s\001c\331@\003o\026comment\331@\003\002c\200e\220s@"
	.ascii	"\001bRtF\001a@r\034inaryoperatorG\021rtAD\034inaryoperatorE>"
	.ascii	"\026ontinue?\020oB\026graphicC\002d.p\206s\020c\303"
	.string	"\027\021ic\001pFs\036yllabiccategory\303\027\020o\037sitionalcategory\303\026\020c\303\026\002g\303\006o&t\303\007\021in\001cJi\021ng\001g.t\022ype\303\007\023roup\303\006H\025ontrolIf\206g\242Jh\003a6eXshy\023phen=\037ngulsyllabletype\303\013\020x:\024digit;\020t\303\013\026ullcomp\037ositionexclusion3\002c\242De\242Kr\003a4b\204e\212l\022ink9\021ph|\022eme\003b^c0eHl\022ink9\032lusterbreak\303\022\024xtend7\022ase5\021xt7\302\005\001b\303\022m\331 "
	.string	"\034neralcategory\302\005\023mask\331 "
	.string	"a\242\220b\242\276c\2440d\244\375e\005mcmnp\242Yx\020t0\001e,p\022ict\241\022nde\001d$r1\033pictographic\241\020o\001d\227j\020i\222\002c@mPp\032resentation\225\027omponent\233\026odifier\226\023base\231\022res\225a0bNc\022omp\233\302\004\033stasianwidth\303\004\022ase\231\003gDhJlNs\032ciihexdigit#\020e\331@"
	.string	"\021ex#\001n8p\021ha \024betic!\021umy\004c\303"
	.string	"i>l\242Wm\242dp\001b\331@\rt\303\025\021di\002cTmtp\033airedbracket\330@\r\023type\303\025$\001l0o\024ntrol%\022ass\303"
	.ascii	"&\024irror\001e8i\026ngglyph\331@\001\020d'\002a2k\303\001o\021"
	.ascii	"ck\303\001\021nk{\020g\331@\001\006h|hTi\205o\242ow\004c0k6l"
	.ascii	"\207t\213u\211\001f\215m\217\021cf\221\030angeswhen\004cDlln"
	.ascii	"~t\230u\030ppercased\211\022ase\001f0m\024apped\217\024olded"
	.ascii	"\215\030owercased\207\034fkccasefolded\221\030itlecased\213\023"
	.ascii	"mpex3a.c\242Hf\331@\002\001nrs\020e\003d\203f:iJs\027ensitiv"
	.ascii	"ee\025olding\331@\002\027gnorable\205\023onic\037alcombining"
	.ascii	"class\303\002\020c\303\002\003a0e4i\242At\303\003\021sh)\002"
	.ascii	"c:fXp,\026recated-\035ompositiontype\303\003\025aultig\037no"
	.ascii	"rablecodepoint+*\020a.\025c"
	.ascii	"ritic/\003f4n>tBy\"\021es# \023alse! \020o!\"\022rue#\013k[o"
	.ascii	"#o<rLv\001i$r3\023rama3\020v\"\024erlay#\242\342\023ight\243"
	.ascii	"\342kXltn\003k/o0r!u\022kta/\031treordered!\001a$v1\030navoi"
	.ascii	"cing1\242\340\022eft\243\340dEdNh\210i\001o&s\243\360\032tas"
	.ascii	"ubscript\243\360\002a\243\352b\243\351o\023uble\001a0b\023el"
	.ascii	"ow\243\351\023bove\243\352\022anr,\025eading-a\242{b\242\324"
	.ascii	"c\021cc\0041<2\242B3\242V8\242d9\0201\243[\t5\n5?6A7C8E9G001"
	.ascii	"<2B3N4=4\0013\243g7\243k6\0208\243v8\0012\243z9\243\201:\002"
	.ascii	"0\243\2022\243\2043\243\205\t5\n5S6U7W8Y9[0I1K2M3O4Q\0063\b3"
	.ascii	"c4e5g6i0]1_2a\0204\243T\242\346\003b\240l\243\344r\243\350t\002"
	.ascii	"atb|t\024ached\001a>b\023elow\242\312\023left\243\310\023bov"
	.ascii	"e\242\326\024right\243\330\242\326\020r\243\330\242\312\020l"
	.ascii	"\243\310\022ove\242\346\001l0r\023ight\243\350\022eft\243\344"
	.ascii	"\242\334\002e,l\243\332r\243\336\022low\242\334\001l0r\023ig"
	.ascii	"ht\243\336\022eft\243\332\013n\300\312r_rFs\242Hw\001h$s3\027"
	.ascii	"itespace3\"\001i0l\002e=iKo?\030ghttoleft\"\002e8iHo\026verr"
	.ascii	"ide?\027mbedding=\025solateK0\036egmentseparator1n\242Ao\242"
	.ascii	"Sp\002afd\206o\033pdirectional\001f2i\025solateM\024ormatA\037"
	.ascii	"ragraphseparato"
	.string	"r/\001fAiM\001o(s\020mC\033nspacingmarkC\001n5t\031herneutral5e\210e\230f\242jl \001e0r\002e7iIo9\030fttoright \002e8iHo\026verride9\027mbedding7\025solateI\003n%s't)u\025ropean\002n<sFt\030erminator)\024umber%\027eparator'\001i(s\020iG\037rststrongisolateGaNb\204c\001o$s-\034mmonseparator-\002l;n+r\023abic\001l0n\024umber+\024etter;.\001nEo\034undaryneutralE"
	.ascii	"\026m\310\310t\301\356wjwHypz\035anabazarsquare\245\030\020a"
	.ascii	"\001n6r\026angciti\243\374\022cho\245,\001e\210i\002j<rhs\027"
	.ascii	"yllables\243H\022ing\242t\036hexagramsymbols\243t\026adicals"
	.ascii	"\243I\023zidi\2454t\242Yu\2445v\002a6ezs\242l\022sup\243}\001"
	.ascii	"i\243\237r\036iationselectors\242l\031supplement\243}\001d<r"
	.ascii	"\031ticalforms\243\221\024icext\242\257\026ensions\243\257\004"
	.ascii	"ahe\242\255h\242\260i\242\270r\034ansportandmap\242\317\026s"
	.ascii	"ymbols\243\317\004g~i\242Ak\242jm\242ln\022gut\244\020\001c@"
	.ascii	"s\021up\2443\026plement\2453\030omponents\245\021\002a*b2s\243"
	.ascii	"`\022log\243b\023anwa\243e\003lRtVv^x\026uanjing\242|\026sym"
	.ascii	"bols\243|\020e\243p\022ham\243\256\022iet\243\267\021ri\243\334"
	.ascii	"\021ilH\022sup\244+\026plement\245+\023luguK\020a\001a$iS\021"
	.ascii	"na=\002b4f<r\023huta\243\373\023etanW\024inagh\243\220\002c\202"
	.ascii	"g\222n\037ifiedcanadianabo\037riginalsyllabicsb\027extended\243"
	.ascii	"\255\021asb\022ext\243\255\025aritic\243xp\303Kp\246ar\250\035"
	.ascii	"s\007o\301\276o\242ip\242\205u\242\244y\002lPmbr\022iac:\022"
	.ascii	"sup\244\027\026plement\245\027\027otinagri\243\217\023bols\001"
	.ascii	"aLf\020o\037rlegacycomputing\2452\037ndpictographsext\001a\245"
	.ascii	"*e\024ndeda\245*\002g4r>y\023ombo\245\026\023dian\245#\027as"
	.ascii	"ompeng\243\332\001a2e\024cials\243V\022cin\037gmodifierlette"
	.ascii	"rs-\002nHpvt\035tonsignwriting\245\006\025danese\242\233\022"
	.ascii	"sup\242\333\026plement\243\333\004a\242\250e\\m\236p\242Ks\023"
	.ascii	"ymbo\037lsandpictographs\245\005\020r\001aNs\022cri\037ptsan"
	.ascii	"dsubscriptss\024ndsubs\033athoperators\243j\001l@u\001ann\027"
	.ascii	"ctuation\243\216\025ementa\001lPr\036yprivateusearea\001a\243"
	.ascii	"mb\243n\003a\\mxp\242As\023ymbo\037lsandpictographs\245\005\024"
	.ascii	"rrows\002a\243gb\243hc\243\372\023athe\037maticaloperators\243"
	.ascii	"j\031unctuation\243\216a\210h\242Hi\242qm\022all\001fFk\025a"
	.ascii	"naext\244)\025ension\245)\022orm\001s\243Tv\026ariants\243T\001"
	.ascii	"m6u\026rashtra\243\241\025aritan\243\254\001aRo\023rtha\037n"
	.ascii	"dformatcontrols\243\367\001r.v\022ian\243y\022ada\243\331\001"
	.ascii	"dPn\023halaP\035archaicnumbers\243\371\023dham\243\370\005r5"
	.ascii	"rDsdu\001a\243N"
	.ascii	"n\027ctuationq\027ivateuse\242N\023area\243N\033alterpahlavi"
	.ascii	"\243\366a@h\202l\031ayingcards\243\314\002h8lJu\025cinhau\243"
	.ascii	"\365\027awhhmong\243\363\025myrene\243\364\001a\216o\001etn\026"
	.ascii	"eticext\242r\001e,s\021up\243\215\025nsions\242r\031suppleme"
	.ascii	"nt\243\215\025nician\243\227\001g>i\023stos\242\246\023disc\243"
	.ascii	"\246\022spa\243\226\001e\\u\001m*n\021icg\020i\242\300\035nu"
	.ascii	"meralsymbols\243\300\023jang\243\243m\242\346n\250\031o\006p"
	.ascii	"cpVr\212s\242Lt\020t\037omansiyaqnumbers\245(\030ticalchar\037"
	.ascii	"acterrecognition\205\001iFn\036amentaldingbats\243\362\021ya"
	.ascii	"G\001a0m\023anya\243z\021ge\245\017c\242qg\242ql\001c\242bd\005"
	.ascii	"p8p6sVt\024urkic\243\277\021er\001m.s\022ian\243\214\021ic\243"
	.ascii	"\361\020o\001g:u\030tharabian\243\273\023dian\245\"hBiTn\032"
	.ascii	"ortharabian\243\360\027ungarian\245\004\024talic\243X\023hik"
	.ascii	"i\243\235\020r\205\022hame\006o\206olr\242au\242by\024anmarX"
	.ascii	"\022ext\002a\243\266b\243\356e\023nded\001a\243\266b\243\356"
	.ascii	"\001dRn\025golianj\022sup\244\r\026plement\245\r\020i\242\354"
	.ascii	"\023fier\001l<t\031oneletters\243\212\025etters-\020o\243\355"
	.ascii	"\001lDs\021ic\242\\\030alsymbols\243\\\023tani\245\003a\242\233"
	.ascii	"e\244Li\001a\242\217s\020c\005p\030p\242qs6t\027echnical\201"
	.ascii	"\025ymbols\217a\242feFm\031athsymbols\001a\243fb\243i\027lla"
	.ascii	"neous\002m:slt\027echnical\201\021at\037hematicalsymbols\001"
	.ascii	"a\243fb\243i\025ymbols\216\022and\001a<p\031ictographs\243\315"
	.ascii	"\024rrows\243s\020o\243\330\007rorDsNtby\031annumerals\245 \023"
	.ascii	"chen\245\f\030aramgondi\245\024\020h\002a:eJo\027perators\177"
	.ascii	"\026lphanum\243]\026matical\001a6o\027perators\177\021lp\037"
	.ascii	"hanumericsymbols\243]hPk~l\210n\001d4i\025chaean\243\352\022"
	.ascii	"aic\243\306\001a>j\022ong\242\252\024tiles\243\252\023jani\243"
	.ascii	"\351\023asar\245\037\025ayalamO\003dle~n\242Gr\024oitic\001c"
	.ascii	"<h\031ieroglyphs\243\327\025ursive\243\326\027efaidrin\245!\027"
	.ascii	"teimayek\242\270\022ext\242\325\026ensions\243\325\030dekika"
	.ascii	"kui\243\353\006k;kVoZudy\021ia\037kengpuachuehmong\245'\020o"
	.ascii	"\243\222\024block!\001m,s\021hu\245\025\027berforms{aDb!e\020"
	.ascii	"w\001a\245\016t\024ailue\243\213\001b8n\027dinagari\245&\025"
	.ascii	"ataean\243\357g\3042j\301\271j\242\325k\242\356l\004aTe\242a"
	.ascii	"i\242xo\242\267y\001c.d\022ian\243\251\022ian\243\247\001oUt"
	.ascii	"\021in\0011\202e\021xt\004a\\b)c\243\224d\243\225e\242\347\023"
	.ascii	"nded\004a6b)c\243\224d\243\225e\243\347&\030dditionalm$\022s"
	.ascii	"up$\026plement%\001pBt\035terlikesymbolsy\022cha\243\234\002"
	.ascii	"mNnTs\020u\242\260\022sup\2441\026plement\2451\021bu\243o\022"
	.ascii	"ear\001a\243\350b\001i8s\027yllabary\243u\027deograms\243v\032"
	.ascii	"wsurrogates\243M\020a\001m2v\024anese\243\265\020o\\\022ext\001"
	.ascii	"a\243\264b\243\271\001a\242Ch\004a@iPmno\206u\025dawadi\243\346"
	.ascii	"\026roshthi\243\211\035tansmallscript\2450\021erh\026symbols"
	.ascii	"\243q\022jki\243\345\003i:nBt\242Qy\023ahli\243\242\022thi\243"
	.ascii	"\301\003a4bvg|n\022adaM\001e@s\021up\242\313\026plement\243\313"
	.ascii	"\021xt\001a\245\023e\024ndeda\245\023\021un\243B\021xi\226\027"
	.ascii	"radicals\227\024akana\236\001eLp\020h\037oneticextensions\243"
	.ascii	"k\021xt\243kg\242\265h\244\204i\003dLm\242Un\242bp\023aext*\026"
	.ascii	"ensions+\001c\231e\027ographic\001dVs\025ymbols\244\013\035a"
	.ascii	"ndpunctuation\245\013\023escr\037iptioncharacters\231\034per"
	.ascii	"ialaramaic\243\272\001dbs\033criptionalpa\001h2r\024thian\243"
	.ascii	"\275\023lavi\243\276\021ic\001n>s\032iyaqnumbers\245\036\031"
	.ascii	"umberforms\243\262\004etl\242\202o\242\232r\242\236u\002j4n>"
	.ascii	"r\024mukhiC\024aratiE\030jalagondi\245\034\001n\242Fo\001mnr"
	.ascii	"\023gianZ\001e@s\021up\242\207\026plement\243\207\021xt\244\033"
	.ascii	"\024ended\245\033\032etricshapes\214\022ext\242\343\024ended"
	.ascii	"\243\343\036eralpunctuationq\027agolitic\242\210\022sup\244\n"
	.ascii	"\026plement\245\n\023thic\243Y\001a\\e\021ek0\001a8e\021xtn\024"
	.ascii	"endedo\027ndcoptic1\023ntha\243\344\002a\242He\242\337i\001g"
	.ascii	"0r\024agana\235\020h\001p:s\030urrogates"
	.ascii	"\243K\001r<u\031surrogates\243L\021iv\037ateusesurrogates\243"
	.ascii	"L\002l2n\232t\022ran\245\002\020f\002aXmpw\024idtha\037ndful"
	.ascii	"lwidthforms\243W\032ndfullforms\243W\023arks\243R\002g4i\242"
	.ascii	"Eu\022noo\243c\021ul\242J\002c<j^s\027yllables\243J\037ompat"
	.ascii	"ibilityjamo\243A\022amo\\\027extended\001a\243\264b\243\271\031"
	.ascii	"firohingya\245\035\023brew7a\244\005b\246Ec\250\032d\254\270"
	.ascii	"e\005m\242m\206n\226t\025hiopic^\001e@s\021up\242\206\026ple"
	.ascii	"ment\243\206\021xt\242\205\001a\243\310e\023nded\242\205\020"
	.ascii	"a\243\310\026oticons\243\316\025closed\002aZc\236i\034deogra"
	.ascii	"phicsup\242\304\026plement\243\304\026lphanum\206\001e,s\021"
	.ascii	"up\243\303\023rics\206\030upplement\243\303\021jk\242D\037le"
	.ascii	"ttersandmonths\243DaJgvl\001b0y\023maic\245%\023asan\243\342"
	.ascii	"\023rlyd\037ynasticcuneiform\245\001\037yptianhieroglyph\001"
	.ascii	"f&s\243\302\034ormatcontrols\245$\007n\300\345n>r\242]s\242\330"
	.ascii	"v\024estan\243\274\001a\222c\023ient\001g4s\025ymbols\243\245"
	.ascii	"\023reek\001m4n\025umbers\243\177\023usic\242~\031alnotation"
	.ascii	"\243~\020t\037olianhieroglyphs\243\376\002a2m\242qr\022ows}\022"
	.ascii	"bic8\003eJmfp\242Cs\021up\242\200\026plement\243\200\021xt\001"
	.ascii	"a\243\322e\024ndeda\243\322\022ath\242\323\030ematicala\037l"
	.ascii	"phabeticsymbols\243\323\001fBr\036esentationforms\001a\243Qb"
	.ascii	"\243U\024enian5\022cii#d\236e\242Bh\242Ml\001cbp\027habeticp"
	.ascii	"\001f\243Pr\036esentationforms\243P\026hemical\242\320\026sy"
	.ascii	"mbols\243\320\022lam\245\007\032geannumbers\243w\021om\243\375"
	.ascii	"\007oqodr\242Au\242Xy\033zantinemusic\242[\030alsymbols\243["
	.ascii	"\001p4x\026drawing\211\024omofo\240\022ext\242C\024ended\243"
	.ascii	"C\020a\001h@i\022lle\222\027patterns\223\021mi\243\311\001g,"
	.ascii	"h\021id\243d\024inese\243\201aHe\242Nh\242Rl\032ockelements\213"
	.ascii	"\003l4m@sft\021ak\243\307\024inese\243\223\021um\242\261\022"
	.ascii	"sup\242\312\026plement\243\312\001i0s\023avah\243\335\025cla"
	.ascii	"tin#\024ngaliA\026aiksuki\245\b\005o\301Lo\242Uu\244\020y\001"
	.ascii	"p\234r\024illic2\001eLs\021up\242a\026plement\242a\022ary\243"
	.ascii	"a\021xt\003a\243\236b\243\240c\245\te\023nded\002a\243\236b\243"
	.ascii	"\240c\245\t\034riotsyllabary\243{\003mZn\242\225p\242\240u\027"
	.ascii	"ntingrod\242\232\027numerals\243\232\002b:m\242_p\025atjamo\243"
	.ascii	"A\024"
	.string	"ining\002dFh\236m\035arksforsymbolsw\036iacriticalmarks.\002e@f\246As\030upplement\243\203\026xtended\243\340\027alfmarks\243R\021on\037indicnumberforms\243\262\033trolpictures\203\022tic\242\204\033epactnumbers\243\337\001n>r\033rencysymbolsu\025eiform\242\230\026numbers\242\231\035andpunctuation\243\231a\242\341h\244\013j\020k\242G\004c\214e\242\200r\242\230s\242\252u\037nifiedideographs\242G\030extension\006dkd\243\321e\245"
	.string	"f\245\022g\245.\024ompat\242E\001f\226i\001bDd\027eographs\242O\022sup\243_\024ility\242E\001fTi\030deographs\242O\031supplement\243_\023orms\243S\021xt\006d\fd\243\321e\245"
	.ascii	"f\245\022g\245.a\243Fb\243^c\243\305\031adicalssup\224\026pl"
	.ascii	"ement\225\001tPy\024mbols\232\035andpunctuation\233\024rokes"
	.ascii	"\243\202\002nHrdu\035casianalbanian\243\336\035adiansyllabic"
	.ascii	"sc\022ian\243\250\002a:eLo\026rasmian\245-\001k&m\243\244\021"
	.ascii	"ma\243\324\001r8s\027ssymbols\245\031\023okee`\022sup\242\377"
	.ascii	"\026plement\243\377\003e>i~o\242iu\025ployan\243\341\001sPv\026"
	.ascii	"anagari>\022ext\242\263\024ended\243\263\023eret\243Z\002a:n"
	.ascii	"\202v\026esakuru\245/\030criticals.\002e0f6s\021up\243\203\021"
	.ascii	"xt\243\340\030orsymbolsw\024gbats\221\001g>m\022ino\242\253\024"
	.ascii	"tiles\243\253\021ra\245\032\bm_m:nHszv\242Kw\022ideC\021ed2\022"
	.ascii	"ial3\002a@b7o\001b(n\020e!\023reak7\020r4\022row5\002m8qFu\001"
	.ascii	"b=p>\021er?\001a$l9\021ll9\001r;u\022are;\022ert@\023icalAcX"
	.ascii	"e\222f\226i\001n6s\020o0\024lated1\021it.\022ial/\002a6iHo\020"
	.ascii	"m$\022pat%\020n\"\025onical#\023rcle'\021nc'\002i:oDr\020a,\024"
	.ascii	"ction-\020n(\021al)\021"
	.ascii	"nt+\004a:fLh^npw*\022ide+\"\027mbiguous#&\027ullwidth'$\027a"
	.ascii	"lfwidth% \001a0e\024utral!(\023rrow)\rn\300\373sms:t\230u\242"
	.ascii	"Iz\002l;p=s9\005o(oWp4u\026rrogateE\021ac\001e2i\025ngmark1\030"
	.ascii	"separator9cSkUmQ\035itlecaseletter'\001n@p\034percaseletter#"
	.ascii	"\027assigned!n\212o\242Gp\bf\024f[iYoOr$sI\027ivateuseCa,cMd"
	.ascii	"GeK\037ragraphseparator=\002d3l5o6\033nspacingmark-\001p|t\022"
	.ascii	"her\003l8nBpLs\024ymbolW\024etter+\024umber7\031unctuationO\034"
	.ascii	"enpunctuationIf\236f\210i\242Kl\242\\m\004a`c1e/n-o\025difie"
	.ascii	"r\001l0s\024ymbolU\024ette"
	.string	"r)\027thsymbolQ\001i.o\023rmatA\035nalpunctuation[\020n\037itialpunctuationY\006m\030m)o(t'u#*\034wercaseletter%e(i<l%\031tternumber5\032neseparator;cDd\242`e\033nclosingmark/\006n9nFoNsEu\033rrencysymbolS \022trl?B\020n\001n,t\022rol?\037ectorpunctuationMc?fAl\035osepunctuationK\002a0eJi\022git3\034shpunctuationG\032cimalnumber3"
	.string	"\022m\302?s\241sNt\242Vw\242ry\242sz\001a,h\022ain\213\021in\205\005t\"t8wLy\026riacwawo\030raightwaw\243U\025ashkafma.e8h\021ink\020db\021hee\001e.m\023kathi\020ng\001aNe\001h(t\020hw\026marbutat\023goal=\001hqws\021awy\001e2u\021dh\200\021he\203\020hz\001b4w\026ithtail\177\024arree}mln\244kp\244\210q\244\210r\001e8o\030hingyayeh\223\001h_v\026ersedpea\002a.e\244>i\020mS\001l\242\347n\026ichaean"
	.ascii	"\022nvsQs>t\\w\240y\242Bz\023ayin\243T\020a\001d.m\022ekh\243"
	.ascii	"L\021he\243K\003a8e<hJw\023enty\243Q\020w\243M\001n\243Nt\020"
	.ascii	"h\243O\024amedh\243P\021aw\243R\022odh\243Sn:o@pFqJr\022esh\243"
	.ascii	"J\021un\243F\021ne\243G\020e\243H\022oph\243Ig3g8h@k^lfm\021"
	.ascii	"em\243E\023imel\241\001e2u\024ndred\243B\021th\243A\022aph\243"
	.ascii	"C\024amedh\243Da4bJdPf\022ive\237\001l*y\021in\227\022eph\225"
	.ascii	"\022eth\231\001a0h\024amedh\235\023leth\233\025ayalam\006n,n"
	.ascii	"4r^sbt\021ta\243c\002g.n2y\020a\243`\020a\243]\001a\243^n\020"
	.ascii	"a\243_\020a\243a\021sa\243bb<jBl\020l\001a\243[l\020a\243\\\021"
	.ascii	"ha\243Y\020a\243Z\021emQ\002o,uPy\020a\221\001j(o\020nU\032o"
	.ascii	"ininggroup!\020nW\020eY\020a\001f[p\020h]f\232fBgzh\212k\242"
	.ascii	"ul\021amL\022adhO\002a>eJi\031nalsemkath5\025rsiyeh\217\206\020"
	.ascii	"h3\020a\001f7m\021al9\001a@e>\001h(t\020hE@\023goalC\002h;m\\"
	.ascii	"n\032ifirohin"
	.string	"gya\001k*p\020a\243e\025innaya\243d\032zaonhehgoal=\002a:hDn\027ottedhehK\001fGp\020hI\022aph\211a.b\212d\242Qe1\002f<ipl\001a(e\020f'\021ph%\024rican\002f0n6q\021af\243X\021eh\243V\022oon\243W\020n#\001eJu\020r\037ushaskiyehbarree\215\001h)t\020h+\021al,\026athrish/\007n.n,r>tVu!\030onjoining!(\032ightjoining)*\031ransparent+c#d@jVl&\031eftjoining'$\031ualjoining%\031oincausing#"
	.ascii	"\023n\300\320sIsHuxw\204x\234z\020wX\001jus\023paceY\004aQgS"
	.ascii	"p(u0yWT\022aceU\026rrogateS\025nknown!\001j]o\027rdjoiner]\020"
	.ascii	"x!n`o\242Ap\242Pq\242nr\001e$io\036gionalindicatoro\004e>l[o"
	.ascii	"FsEuF\024mericG\025xtline[\027nstarterE\020pH\034enpunctuati"
	.ascii	"onI\001o>rL\032efixnumericMJ\033stfixnumericK\020uN\026otati"
	.ascii	"onOh{hPi\206j\242al\242em\034andatorybreak-\0042_3ae4lmy:\023"
	.ascii	"phen;\031brewletterm\002d(n<sA<\030eographic=>\001f>s\021ep\001"
	.ascii	"a\"e\024rable?\030ixnumericA\002lctevg\001fCi\025nefeedCa@bp"
	.ascii	"c\242Ue\242\333g\020l8\021ue9\002i#l4m\026biguous#$\027phabe"
	.ascii	"tic%\0042'a)b+k-r\022eak\002a6b>s\025ymbolsW\023fter)\001e*o"
	.ascii	"\021th'\023fore+\007mQm3o(pir5\001m"
	.string	"vn\001d<t\032ingentbreak/\025itiona\037ljapanesestarterk\001b:p\031lexcontextQ\030iningmark3ajb/jkl0\023osep\001a8u\030nctuation1\030renthesisi\033rriagereturn5\002b>mFx6\030clamation7p\022aseqr\026odifiers\001dBn\001o2u&\024meric'\021ne!\001e.i$\022git%\"\024cimal#"
	.ascii	"\030n\304*t\301mw\226w\242Lx\242py\242zz\006s\036s4xByHz\021"
	.ascii	"zz\243g\020y\001e\243\256m\243\201\021xx\243f\021yy!a0iXm\021"
	.ascii	"th\243\200\020n\001a&b\243\261\032bazarsquare\243\261\021nh#"
	.ascii	"\002a0cZo\021le\243\233\001n<r\020a\242\222\025ngciti\243\222"
	.ascii	"\022cho\243\274\021ho\243\274\001p,s\021ux\243e\021eo\233\001"
	.ascii	"e,ir\021iis\021zi\242\300\021di\243\300tJu\242\272v\001a,i\021"
	.ascii	"sp\243d\020i\242c\020i\243c\005g6ghhli\002b:fJr\020h\242\236"
	.ascii	"\022uta\243\236\001e$to\022tano\024inagh\231\021lgu\020a\001"
	.ascii	"a$imj\021naka0e\242[f\021ng\231\006l!l2m8nDv\020t\243\177\001"
	.ascii	"e\211u\227\001i$lg\020lg\020g\242\232\021ut\243\232g6iRk\020"
	.ascii	"r\242\231\020i\243\231\001a0bz\023anwa{\022logu\002l2t4v\022"
	.ascii	"iet\243\177\020e\211\022ham\243j\001l*n\020g\243b\020uh\021g"
	.ascii	"ui\001g2n\024known\243g\021ar\212\023itic\213q\301\023q\242\336"
	.ascii	"r\242\343s\006i\212iro\242Lu\242uy\001lFr\004cee\243_i,j\243"
	.ascii	"`n\243a\021ace\020o\224\026tinagri\225\002d<gLn\001d\243\221"
	.ascii	"hb\022alac\020d\242\246\022ham\243\246\027nwriting\243p\002g"
	.ascii	":rRy\020o\242\260\022mbo\243\260\001d&o\243\270\242\267\022i"
	.ascii	"an\243\267\020a\242\230\026sompeng\243\230\021nd\242q\024ane"
	.ascii	"se\243qa\\g\242Ch\001a*r\020d\243\227\002r(v0w\207\022ada\243"
	.ascii	"\227\022ian\207\002m@rXu\020r\242o\025ashtra\243o\001a&r\243"
	.ascii	"~\024ritan\243~\001a\243^b\243\205\021nw\243p\021aa\001c/i#\003"
	.ascii	"e>jHoNu\020n\001i$ra\020ca\023jang\243n\021ng\243n\001h*r\020"
	.ascii	"o\243]\020g\243\266n\242\203o\242\362p\005l\036lDrJs\033alte"
	.ascii	"rpahlavi\243{\021rd\243\\\021ti\243}a|e\242Th\003a>lNn^o\026"
	.ascii	"enician\243[\020g\242Z\022spa\243Z\002i\243zp\243{v\243|\020"
	.ascii	"x\243[\002h>lPu\020c\242\245\024inhau\243\245\027awhhmong\243"
	.ascii	"K\020m\242\220\024yrene\243\220\021rm\243Y\006k6kVsnuty\021i"
	.ascii	"a\037kengpuachuehmong\243\272\001g.o\242W\020o\243W\020b\243"
	.ascii	"\204\021hu\243\226\022shu\243\226aBb\200e\020w\001a\243\252t"
	.ascii	"\024ailue\227\002b.n<r\020b\243\216\025ataean\243\217\020d\242"
	.ascii	"\273\026inagari\243\273\021at\243\217\003gZllr\242\223s\002a"
	.ascii	"6g<m\020a\204\022nya\205\021ge\243\253\020e\243\253\001a*h\021"
	.ascii	"am[\020m[\001c\242`d\005p7p6sTt\024urkic\243X\021er\001m,s\022"
	.ascii	"ian\233\021ic\243Y\020o\001g:u\030tharabian\243\205\023dian\243"
	.ascii	"\270hBiTn\032ortharabian\243\216\027ungarian\243L\024talic]\001"
	.ascii	"h&k\243m\022iki\243m\002i,k0y\020a_\021ya_\020h\243Xh\303\rk"
	.ascii	"\302$k\244\027l\244\262m\boFoHrtt\200u\206y\001a(m\020rY\023"
	.ascii	"nmarY\002d.n2o\020n\243r\020i\243\243\020gV\024olianW\020o\242"
	.ascii	"\225\020o\243\225\021ei\243s\021lt\242\244\022ani\243\244a6e"
	.ascii	"\242gi\242\275l\021ymU\006n8n2r\\sly\020a\243U\001d8i\242y\025"
	.ascii	"chaean\243y\242T\022aic\243T\020c\242\251\022hen\243\251\030"
	.ascii	"aramgondi\243\257h6kLl\025ayalamU\001a&j\243\240\023jani\243"
	.ascii	"\240\020a\242\264\022sar\243\264\003dxe\224n\242Br\001c\243\215"
	.ascii	"o\242V\023itic\001c<h\031ieroglyphs\243V\025ursive\243\215\001"
	.ascii	"e&f\243\265\026faidrin\243\265\027teimayek\243s\020d\242\214"
	.ascii	"\027ekikakui\243\214\021ao\243\\\006n\032n4o8p>t\021hi\243x\021"
	.ascii	"daK\021re\243w\021el\243\212a0h\232i\021ts\243\277\004i<lDnH"
	.ascii	"tVy\023ahli\243O\022thi\243x\020i\243O\001aMn\022adaK\024aka"
	.ascii	"naL\031orhiragana\215\004a@iRmpo|u\025dawadi\243\221\020r\222"
	.ascii	"\025oshthi\223\035tansmallscript\243\277\001e$rO\020rO\020j\242"
	.ascii	"\235\021ki\243\235\004a\\e\220i\240o\242]y\001c4d\020i\242l\021"
	.ascii	"an\243l\020i\242k\021an\243k\002nBoFt\003f\243Pg\243Qi$nS\020"
	.ascii	"nS\020a\243jP\020oQ\021pc\242R\021ha\243R\002m.n6s\020u\243\203"
	.ascii	"\020b\200\020u\201\002a\243Sb\203e\021ar\001a\243Sb\203\021m"
	.ascii	"a\243\213hni\242\225j\002a0pRu\021rc\243\224\001m8v\020a\242"
	.ascii	"N\023nese\243N\020o\243\255\021an\243i\006l\036l4m:rHu\021ng"
	.ascii	"\243L\021uw\243\234\020n\001g\243Kp\243\272\021kt\215a<e\242"
	.ascii	"Ci\021raH\023ganaI\001n4t\020r\242\242\021an\243\242B\006o\016"
	.ascii	"ows\243It\243Ju\022noowb\243\254g>iB\031firohingya\243\266D\021"
	.ascii	"ulE\021brF\021ewG\002m.nJt\021al]\034perialaramaic\243"
	.ascii	"t\002dfhjs\033criptionalpa\001h2r\024thian\243}\023lavi\243z"
	.ascii	"\020s\243M\025erited#d\301\rd\242ze\242\301g\004e\202l\232o\242"
	.ascii	"Fr\242Uu\002j<nNr\001m$uA\023ukhiA\001a$r?\023rati?\030jalag"
	.ascii	"ondi\243\263\020o\001k\243Hr8\023gian9\021ag\220\025olitic\221"
	.ascii	"\001n0t\020h:\021ic;\001g\243\263m\243\257\001a2e\001e$k=\020"
	.ascii	"k=\020n\242\211\022tha\243\211\004eFilo\214s\232u\021pl\242\207"
	.ascii	"\023oyan\243\207\001s8v\020a4\025nagari5\023eret3\001a6v\026"
	.ascii	"esakuru\243\276\020k\243\276\021gr\242\262\020a\243\262\021r"
	.ascii	"t3\002g:lrt\021hi6\023opic7\020y\002d\243Eh\243Fp\242G\036ti"
	.ascii	"anhieroglyphs\243G\001b6y\020m\242\271\022aic\243\271\020a\242"
	.ascii	"\210\022san\243\210a\242\264b\244\031c\006o=oZpvuzy\001p>r\002"
	.ascii	"i*l1s\243D\023llic1\023riot\177\001m0p\020t.\021ic/\022mon!\021"
	.ascii	"rt\177\026neiform\243ea2h\242Ai\021rt\243C\003kLnPrvu\035cas"
	.ascii	"ianalbanian\243\237\020m\243v\001a$sq\035dianaboriginalq\020"
	.ascii	"i\242h\021an\243h\003a2eDoRr\020s\243\275\001k&m\243B\021ma\243"
	.ascii	"v\020r,\023okee-\026rasmian\243\275\006hJhHnNrvv\001e*s\020t"
	.ascii	"\243u\023stan\243u\021om\243\241\021at\037olianhieroglyphs\243"
	.ascii	"\234\001a>m\002e*i\243tn'\023nian'\020b$\021ic%d0fDg\021hb\243"
	.ascii	"\237\020l\001a&m\243\247\020m\243\247\021ak\243\223\006l<lRo"
	.ascii	"Vrfu\001g0h\001dyi\020dy\020i\216\023nese\217\021is\241\021p"
	.ascii	"o*\023mofo+\020a\001h.i|\022lle}\242A\021mi\243AaHe\234h\001"
	.ascii	"a*k\020s\243\250\025iksuki\243\250\003l:mHsTt\001a$k\237\020"
	.ascii	"k\237\020i\234\023nese\235\020u\242\202\020m\243\202\020s\242"
	.ascii	"\206\023avah\243\206\021ng(\022ali)\003lBn\220t\242Fv$\027ow"
	.ascii	"eljamo%\"\001eTv(\001s8t*\027syllable+\026yllable)\030adingj"
	.ascii	"amo#\001a!o\032tapplicable!&\032railingjamo'\001n,y\"\021es#"
	.ascii	" \020o!\001n,y\"\021es# \020o!\002m0n:y\"\021es#$\023aybe% \020"
	.ascii	"o!\002m0n:y\"\021es#$\023aybe% \020o!\013r9v\fv3x*z\021wjC\020"
	.ascii	"x!r(sPt1\001e$i9\036gionalindicator9\001m5"
	.string	"p\030acingmark5l\037l<oJp\001p7r\024epend7(\001f+v,\020t/\023ther!cLedg\001a:l\031ueafterzwjA\020zA\002n#o$r%\024ntrol#\002b4mNx&\023tend':\001a$g=\021se:\022gaz=>\026odifier?\tnJn4oDs`u\224x\020x!\020u*\024meric+\001l,t\022her!\024etter-\003c6eFp1t2\022erm3<\026ontinue=.\020p/\020p4\022per5aFcRedfrl\002e-f;o(\022wer)\020t\"\022erm#\001l$r7$\022ose%\020x8\023tend9\020o&\023rmat'"
	.ascii	"\020l\210r@r6s^wzx\212z\021wjK\001e$i;\036gionalindicator;\001"
	.ascii	"i$q?\030nglequote?\027segspaceM\020x!l6m<nvo\023ther!\001e#f"
	.ascii	"5\003b7i(l)n+\020d\001l4n\021um*\022let7\024etter)\002e6l9u,"
	.ascii	"\024meric-\024wline9f?f@gNhpk\020a&\025takana'\020o$\023rmat"
	.ascii	"%\001a:l\031ueafterzwjI\020zI\001e$l=\031brewletter=a\206c\222"
	.ascii	"d\224e\002bDm^x.\023tend2\025numlet/B\001a$gE\021seB\022gazE"
	.ascii	"F\026odifierG\025letter#\020r1\001o$qA\030ublequoteA\002c2n<"
	.ascii	"o\"\022pen#$\023lose% \022one!\006oeoJr\\tdv\035isualorderle"
	.ascii	"ft=\030verstruck-\023ight/\021op0\022and\002b2lbr\023ight;\024"
	.ascii	"ottom2\022and\001l.r\023ight5\022eft?\022eft6\027andright9b,"
	.ascii	"l\\n\020a!\024ottom\"\022and\001l.r\023ight'\022eft%\022eft("
	.ascii	"\027andright+\rn\252rpr\222s\242Ft\242Tv\001i`o\022welb\001d"
	.ascii	":i\031ndependentg\027ependente\001r.s\023argaa\022ama_\035eg"
	.ascii	"istershifterW\036yllablemodifierY\022one\001l,m\022ark]\024e"
	.ascii	"tter[n<o|p\030urekillerU\001oLu\001k<m\022berP\025joinerS\021"
	.ascii	"taO\026njoinerM\023ther!g>gJidj\202m\035odifyingletterK\034e"
	.ascii	"minationmarkE\036nvisiblestackerG\024oinerIa\242\272b\242\300"
	.ascii	"c\001a\242\242o\026nsonant*\bkgkHmRp\\s\242Bw\031ithstackerC"
	.ascii	"\024iller5\024edial7\001lRr\020e\001c.f\023ixed=\031edingrep"
	.ascii	"h"
	.string	"a;\030aceholder9\020u\001b>c\033ceedingrephaA\025joined?dLfRhZi\036nitialpostfixed3\022ead-\023inal/\030eadletter1\035ntillationmark)\026vagraha#\001iJr\020a\037hmijoiningnumber'\022ndu%\002r8tFu&\025pright' \025otated!\001r$u%\"\030ansformed\001r2u\025pright%\025otated#\rn\301\206s\250sLt\242vu\242\203z\330p"
	.string	"\002l\331 "
	.string	"p\331@"
	.string	"s\303"
	.string	"\376\017"
	.string	""
	.string	""
	.string	"\007o<o\377\b"
	.string	""
	.string	""
	.string	"p:uny\023mbol\377\017"
	.string	""
	.string	""
	.string	"\021ac\001e4i\025ngmark\245"
	.string	"\030separator\303"
	.string	"\026rrogate\341"
	.string	""
	.string	"c\377\002"
	.string	""
	.string	""
	.string	"e8k\377\004"
	.string	""
	.string	""
	.string	"m\377\001"
	.string	""
	.string	""
	.string	"\026parator\331p"
	.string	"\035itlecaseletter1\001n@p\034percaseletter%\027assigned#n\242io\242\211p\3760\370"
	.string	""
	.string	"\ti3i\377\020"
	.string	""
	.string	""
	.string	"o\375\200"
	.string	""
	.string	"rTs\371"
	.string	""
	.string	"u\022nct\3760\370"
	.string	""
	.string	"\025uation\3770\370"
	.string	""
	.string	"\027ivateuse\335"
	.string	""
	.string	"aHc\375@"
	.string	""
	.string	"d\351"
	.string	""
	.string	"e\375 "
	.string	""
	.string	"f\377 "
	.string	""
	.string	""
	.string	"\037ragraphseparator\331@"
	.string	"\276"
	.string	"\003d\247"
	.string	"l\253"
	.string	"o0u\023mber\277"
	.string	"\262"
	.string	"\033nspacingmark\241\001p\222t\022her\346\200\001\003l@nJpVs\024ymbol\377\b"
	.string	""
	.string	""
	.string	"\024ettera\024umber\263"
	.string	"\031unctuation\375\200"
	.string	""
	.string	"\034enpunctuation\371"
	.string	""
	.string	"f\300\304f\242Gi\242dl\242ym\244\300\004alc\245"
	.string	"e\243\200n\241o\025difier\001l8s\024ymbol\377\004"
	.string	""
	.string	""
	.string	"\024etterA\001r<t\026hsymbol\377\001"
	.string	""
	.string	""
	.string	"\020k\245\300\001i2o\023rmat\333"
	.string	""
	.string	"\035nalpunctuation\377 "
	.string	""
	.string	""
	.string	"\020n\037itialpunctuation\377\020"
	.string	""
	.string	""
	.string	"\234\007m\030mAo(t1u%`\034wercaseletter)c=e(iBl)\023tter\234\025number\253"
	.string	"\032neseparator\331 "
	.string	"cFd\242\226e\033nclosingmark\243\200\346\200\001\007nWnRo^s\341"
	.string	""
	.string	"u\033rrencysymbol\377\002"
	.string	""
	.string	""
	.string	"\"\022trl\331\200"
	.string	"\334"
	.string	""
	.string	"\001mbn\001n0t\022rol\331\200"
	.string	"\037ectorpunctuation\375@"
	.string	""
	.string	"\031biningmark\245\300aXc\331\200"
	.string	"f\333"
	.string	""
	.string	"l\035osepunctuation\375 "
	.string	""
	.string	"\030sedletter=\002a2ePi\022git\247"
	.string	"\034shpunctuation\351"
	.string	""
	.string	"\032cimalnumber\247"
	.globl	_ZN6icu_6712PropNameData9valueMapsE
	.align 32
	.type	_ZN6icu_6712PropNameData9valueMapsE, @object
	.size	_ZN6icu_6712PropNameData9valueMapsE, 5528
_ZN6icu_6712PropNameData9valueMapsE:
	.long	6
	.long	0
	.long	65
	.long	0
	.long	227
	.long	872
	.long	227
	.long	894
	.long	227
	.long	915
	.long	227
	.long	937
	.long	227
	.long	948
	.long	227
	.long	981
	.long	227
	.long	997
	.long	227
	.long	1012
	.long	227
	.long	1026
	.long	227
	.long	1062
	.long	227
	.long	1085
	.long	227
	.long	1109
	.long	227
	.long	1132
	.long	227
	.long	1147
	.long	227
	.long	1162
	.long	227
	.long	1179
	.long	227
	.long	1193
	.long	227
	.long	1211
	.long	227
	.long	1237
	.long	227
	.long	1264
	.long	227
	.long	1285
	.long	227
	.long	1314
	.long	227
	.long	1331
	.long	227
	.long	1342
	.long	227
	.long	1373
	.long	227
	.long	1395
	.long	227
	.long	1412
	.long	227
	.long	1428
	.long	227
	.long	1455
	.long	227
	.long	1480
	.long	227
	.long	1497
	.long	227
	.long	1523
	.long	227
	.long	1542
	.long	227
	.long	1558
	.long	227
	.long	1584
	.long	227
	.long	1609
	.long	227
	.long	1632
	.long	227
	.long	1652
	.long	227
	.long	1674
	.long	227
	.long	1694
	.long	227
	.long	1716
	.long	227
	.long	1742
	.long	227
	.long	1766
	.long	227
	.long	1794
	.long	227
	.long	1802
	.long	227
	.long	1810
	.long	227
	.long	1818
	.long	227
	.long	1826
	.long	227
	.long	1835
	.long	227
	.long	1848
	.long	227
	.long	1867
	.long	227
	.long	1896
	.long	227
	.long	1925
	.long	227
	.long	1954
	.long	227
	.long	1984
	.long	227
	.long	2014
	.long	227
	.long	2050
	.long	227
	.long	2063
	.long	227
	.long	2089
	.long	227
	.long	2110
	.long	227
	.long	2137
	.long	227
	.long	2160
	.long	227
	.long	2183
	.long	227
	.long	2217
	.long	227
	.long	4096
	.long	4121
	.long	2248
	.long	351
	.long	2792
	.long	378
	.long	12049
	.long	233
	.long	12080
	.long	691
	.long	12398
	.long	713
	.long	12488
	.long	723
	.long	13093
	.long	757
	.long	15392
	.long	863
	.long	15504
	.long	873
	.long	16170
	.long	920
	.long	16232
	.long	928
	.long	19035
	.long	1125
	.long	19161
	.long	1135
	.long	19198
	.long	1141
	.long	19224
	.long	1147
	.long	19257
	.long	1154
	.long	19283
	.long	233
	.long	19320
	.long	233
	.long	19358
	.long	1161
	.long	19528
	.long	1183
	.long	19649
	.long	1202
	.long	19827
	.long	1229
	.long	19882
	.long	1236
	.long	20362
	.long	1256
	.long	21514
	.long	1296
	.long	8192
	.long	8193
	.long	21609
	.long	1304
	.long	12288
	.long	12289
	.long	21749
	.long	0
	.long	16384
	.long	16398
	.long	21767
	.long	0
	.long	21776
	.long	0
	.long	21802
	.long	0
	.long	21819
	.long	0
	.long	21836
	.long	0
	.long	21858
	.long	0
	.long	21867
	.long	0
	.long	21896
	.long	0
	.long	21926
	.long	0
	.long	21956
	.long	0
	.long	21986
	.long	0
	.long	22008
	.long	0
	.long	22028
	.long	0
	.long	22050
	.long	0
	.long	28672
	.long	28673
	.long	22075
	.long	0
	.long	2006
	.long	18
	.long	0
	.long	1
	.long	18
	.long	32
	.long	2036
	.long	74
	.long	0
	.long	1
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	35
	.long	36
	.long	84
	.long	91
	.long	103
	.long	107
	.long	118
	.long	122
	.long	129
	.long	130
	.long	132
	.long	133
	.long	200
	.long	202
	.long	214
	.long	216
	.long	218
	.long	220
	.long	222
	.long	224
	.long	226
	.long	228
	.long	230
	.long	232
	.long	233
	.long	234
	.long	240
	.long	46
	.long	64
	.long	76
	.long	94
	.long	104
	.long	121
	.long	132
	.long	145
	.long	158
	.long	171
	.long	184
	.long	197
	.long	210
	.long	223
	.long	236
	.long	249
	.long	262
	.long	275
	.long	288
	.long	301
	.long	314
	.long	327
	.long	340
	.long	353
	.long	366
	.long	379
	.long	392
	.long	405
	.long	418
	.long	431
	.long	444
	.long	457
	.long	470
	.long	483
	.long	496
	.long	509
	.long	524
	.long	539
	.long	554
	.long	569
	.long	584
	.long	599
	.long	614
	.long	629
	.long	655
	.long	675
	.long	695
	.long	722
	.long	737
	.long	746
	.long	762
	.long	770
	.long	779
	.long	794
	.long	803
	.long	819
	.long	836
	.long	853
	.long	2453
	.long	1
	.long	0
	.long	23
	.long	2263
	.long	2280
	.long	2297
	.long	2317
	.long	2340
	.long	2364
	.long	2382
	.long	2403
	.long	2426
	.long	2447
	.long	2463
	.long	2481
	.long	2510
	.long	2538
	.long	2556
	.long	2585
	.long	2613
	.long	2641
	.long	2662
	.long	2683
	.long	2709
	.long	2736
	.long	2763
	.long	2871
	.long	1
	.long	0
	.long	309
	.long	2803
	.long	2816
	.long	2835
	.long	2875
	.long	2905
	.long	2935
	.long	2959
	.long	3002
	.long	3044
	.long	3068
	.long	3087
	.long	3106
	.long	3121
	.long	3136
	.long	3151
	.long	3166
	.long	3189
	.long	3206
	.long	3225
	.long	3244
	.long	3257
	.long	3270
	.long	3285
	.long	3302
	.long	3323
	.long	3340
	.long	3351
	.long	3360
	.long	3377
	.long	3394
	.long	3413
	.long	3431
	.long	3450
	.long	3469
	.long	3532
	.long	3545
	.long	3558
	.long	3571
	.long	3592
	.long	3640
	.long	3666
	.long	3699
	.long	3742
	.long	3777
	.long	3871
	.long	3910
	.long	3937
	.long	3952
	.long	3991
	.long	4031
	.long	4066
	.long	4101
	.long	4143
	.long	4168
	.long	4199
	.long	4234
	.long	4270
	.long	4289
	.long	4315
	.long	4357
	.long	4381
	.long	4421
	.long	4462
	.long	4481
	.long	4500
	.long	4519
	.long	4558
	.long	4573
	.long	4605
	.long	4651
	.long	4681
	.long	4727
	.long	4755
	.long	4782
	.long	4807
	.long	4832
	.long	4865
	.long	4913
	.long	4944
	.long	4978
	.long	5030
	.long	5075
	.long	5144
	.long	5177
	.long	5219
	.long	5252
	.long	5293
	.long	5312
	.long	5363
	.long	5386
	.long	5401
	.long	5418
	.long	5461
	.long	5484
	.long	5533
	.long	5579
	.long	5646
	.long	5657
	.long	5714
	.long	5731
	.long	5748
	.long	5761
	.long	5780
	.long	5838
	.long	5874
	.long	5910
	.long	5968
	.long	6024
	.long	6067
	.long	6091
	.long	6135
	.long	6179
	.long	6192
	.long	6207
	.long	6236
	.long	6270
	.long	6316
	.long	6348
	.long	6387
	.long	6426
	.long	6457
	.long	6476
	.long	6493
	.long	6510
	.long	6547
	.long	6584
	.long	6623
	.long	6675
	.long	6720
	.long	6750
	.long	6769
	.long	6794
	.long	6851
	.long	6866
	.long	6898
	.long	6932
	.long	6966
	.long	6989
	.long	7012
	.long	7057
	.long	7082
	.long	7107
	.long	7156
	.long	7198
	.long	7225
	.long	7244
	.long	7275
	.long	7284
	.long	7303
	.long	7333
	.long	7363
	.long	7382
	.long	7405
	.long	7426
	.long	7479
	.long	7515
	.long	7536
	.long	7551
	.long	7570
	.long	7606
	.long	7615
	.long	7651
	.long	7674
	.long	7693
	.long	7708
	.long	7719
	.long	7752
	.long	7776
	.long	7791
	.long	7806
	.long	7821
	.long	7844
	.long	7865
	.long	7886
	.long	7943
	.long	7962
	.long	7990
	.long	8001
	.long	8014
	.long	8060
	.long	8096
	.long	8131
	.long	8150
	.long	8184
	.long	8203
	.long	8230
	.long	8265
	.long	8300
	.long	8337
	.long	8354
	.long	8401
	.long	8446
	.long	8469
	.long	8496
	.long	8511
	.long	8554
	.long	8610
	.long	8668
	.long	8714
	.long	8731
	.long	8744
	.long	8780
	.long	8795
	.long	8823
	.long	8849
	.long	8878
	.long	8934
	.long	8955
	.long	9000
	.long	9031
	.long	9077
	.long	9109
	.long	9161
	.long	9176
	.long	9218
	.long	9253
	.long	9296
	.long	9307
	.long	9324
	.long	9351
	.long	9387
	.long	9400
	.long	9421
	.long	9460
	.long	9503
	.long	9558
	.long	9577
	.long	9594
	.long	9642
	.long	9659
	.long	9674
	.long	9695
	.long	9725
	.long	9744
	.long	9763
	.long	9786
	.long	9815
	.long	9826
	.long	9835
	.long	9869
	.long	9890
	.long	9927
	.long	9950
	.long	9991
	.long	10018
	.long	10039
	.long	10064
	.long	10097
	.long	10150
	.long	10167
	.long	10216
	.long	10252
	.long	10269
	.long	10294
	.long	10305
	.long	10350
	.long	10384
	.long	10430
	.long	10481
	.long	10496
	.long	10513
	.long	10542
	.long	10608
	.long	10647
	.long	10660
	.long	10681
	.long	10717
	.long	10755
	.long	10812
	.long	10829
	.long	10865
	.long	10876
	.long	10889
	.long	10904
	.long	10941
	.long	10987
	.long	11015
	.long	11044
	.long	11057
	.long	11074
	.long	11104
	.long	11139
	.long	11168
	.long	11181
	.long	11213
	.long	11242
	.long	11275
	.long	11316
	.long	11333
	.long	11364
	.long	11389
	.long	11414
	.long	11431
	.long	11504
	.long	11521
	.long	11546
	.long	11593
	.long	11638
	.long	11675
	.long	11741
	.long	11769
	.long	11784
	.long	11807
	.long	11853
	.long	11878
	.long	11919
	.long	11945
	.long	12004
	.long	12034
	.long	7813
	.long	1
	.long	0
	.long	18
	.long	12103
	.long	12119
	.long	12138
	.long	12154
	.long	12170
	.long	12185
	.long	12201
	.long	12219
	.long	12238
	.long	12256
	.long	12272
	.long	12288
	.long	12303
	.long	12318
	.long	12334
	.long	12347
	.long	12362
	.long	12382
	.long	8003
	.long	1
	.long	0
	.long	6
	.long	12419
	.long	12430
	.long	12443
	.long	12456
	.long	12469
	.long	12480
	.long	8071
	.long	1
	.long	0
	.long	30
	.long	12509
	.long	12524
	.long	12545
	.long	12566
	.long	12587
	.long	12607
	.long	12624
	.long	12644
	.long	12663
	.long	12680
	.long	12705
	.long	12723
	.long	12740
	.long	12760
	.long	12779
	.long	12803
	.long	12821
	.long	12832
	.long	12848
	.long	12862
	.long	12883
	.long	12904
	.long	12926
	.long	12952
	.long	12974
	.long	12990
	.long	13010
	.long	13030
	.long	13047
	.long	13071
	.long	8626
	.long	1
	.long	0
	.long	102
	.long	13111
	.long	13146
	.long	13155
	.long	13168
	.long	13179
	.long	13188
	.long	13199
	.long	13208
	.long	13233
	.long	13238
	.long	13247
	.long	13276
	.long	13285
	.long	13298
	.long	13307
	.long	13343
	.long	13350
	.long	13359
	.long	13378
	.long	13389
	.long	13398
	.long	13409
	.long	13434
	.long	13443
	.long	13458
	.long	13469
	.long	13478
	.long	13489
	.long	13498
	.long	13505
	.long	13514
	.long	13525
	.long	13534
	.long	13559
	.long	13568
	.long	13581
	.long	13592
	.long	13609
	.long	13620
	.long	13641
	.long	13664
	.long	13673
	.long	13682
	.long	13707
	.long	13718
	.long	13727
	.long	13736
	.long	13759
	.long	13788
	.long	13799
	.long	13816
	.long	13827
	.long	13834
	.long	13847
	.long	13860
	.long	13905
	.long	13926
	.long	13935
	.long	13962
	.long	13997
	.long	14030
	.long	14063
	.long	14100
	.long	14139
	.long	14172
	.long	14207
	.long	14240
	.long	14279
	.long	14312
	.long	14349
	.long	14380
	.long	14411
	.long	14442
	.long	14471
	.long	14504
	.long	14537
	.long	14572
	.long	14609
	.long	14640
	.long	14671
	.long	14704
	.long	14743
	.long	14780
	.long	14811
	.long	14844
	.long	14879
	.long	14906
	.long	14931
	.long	14958
	.long	14983
	.long	15012
	.long	15039
	.long	15068
	.long	15099
	.long	15128
	.long	15157
	.long	15188
	.long	15217
	.long	15244
	.long	15273
	.long	15302
	.long	15353
	.long	9463
	.long	1
	.long	0
	.long	6
	.long	15409
	.long	15424
	.long	15440
	.long	15456
	.long	15472
	.long	15489
	.long	9557
	.long	1
	.long	0
	.long	43
	.long	15519
	.long	15531
	.long	15545
	.long	15560
	.long	15575
	.long	15591
	.long	15608
	.long	15628
	.long	15649
	.long	15671
	.long	15690
	.long	15710
	.long	15726
	.long	15735
	.long	15746
	.long	15762
	.long	15790
	.long	15808
	.long	15822
	.long	15837
	.long	15849
	.long	15870
	.long	15890
	.long	15909
	.long	15923
	.long	15943
	.long	15957
	.long	15967
	.long	15985
	.long	15997
	.long	16011
	.long	16027
	.long	16034
	.long	16041
	.long	16048
	.long	16055
	.long	16062
	.long	16084
	.long	16117
	.long	2160
	.long	16135
	.long	16146
	.long	16161
	.long	10158
	.long	1
	.long	0
	.long	4
	.long	16187
	.long	16198
	.long	16210
	.long	16220
	.long	10196
	.long	1
	.long	0
	.long	193
	.long	16243
	.long	16256
	.long	16277
	.long	16290
	.long	16305
	.long	16319
	.long	16334
	.long	16349
	.long	16367
	.long	16382
	.long	16396
	.long	16413
	.long	16428
	.long	16443
	.long	16456
	.long	16468
	.long	16483
	.long	16498
	.long	16508
	.long	16521
	.long	16534
	.long	16549
	.long	16563
	.long	16578
	.long	16590
	.long	16600
	.long	16612
	.long	16628
	.long	16644
	.long	16658
	.long	16670
	.long	16687
	.long	16699
	.long	16711
	.long	16725
	.long	16738
	.long	16750
	.long	16763
	.long	3340
	.long	16776
	.long	16790
	.long	16816
	.long	16825
	.long	16839
	.long	16853
	.long	16865
	.long	16880
	.long	16894
	.long	16908
	.long	16920
	.long	16935
	.long	16949
	.long	16963
	.long	16976
	.long	16991
	.long	17018
	.long	17033
	.long	17050
	.long	17067
	.long	17086
	.long	17104
	.long	17119
	.long	17137
	.long	17152
	.long	17164
	.long	17175
	.long	7708
	.long	17188
	.long	17199
	.long	17210
	.long	17221
	.long	17232
	.long	17259
	.long	17270
	.long	17281
	.long	17292
	.long	17311
	.long	17331
	.long	17342
	.long	17357
	.long	17372
	.long	17383
	.long	17394
	.long	17407
	.long	17422
	.long	17436
	.long	17447
	.long	17474
	.long	17484
	.long	17501
	.long	17518
	.long	17533
	.long	17550
	.long	17561
	.long	17572
	.long	17583
	.long	17594
	.long	17605
	.long	17616
	.long	17627
	.long	17637
	.long	17648
	.long	17664
	.long	17675
	.long	17689
	.long	17702
	.long	17713
	.long	17728
	.long	17741
	.long	17754
	.long	17769
	.long	17782
	.long	17799
	.long	17817
	.long	17833
	.long	17844
	.long	17863
	.long	17886
	.long	17900
	.long	17913
	.long	17924
	.long	17937
	.long	17954
	.long	17982
	.long	18004
	.long	18015
	.long	18044
	.long	18060
	.long	18075
	.long	18086
	.long	18097
	.long	7990
	.long	18109
	.long	18120
	.long	18144
	.long	18160
	.long	18175
	.long	18189
	.long	18203
	.long	18214
	.long	18225
	.long	18245
	.long	18268
	.long	18292
	.long	18308
	.long	18324
	.long	18340
	.long	18358
	.long	18369
	.long	18380
	.long	18390
	.long	18402
	.long	18416
	.long	18435
	.long	18447
	.long	18460
	.long	18471
	.long	18499
	.long	18512
	.long	18526
	.long	18551
	.long	10294
	.long	18566
	.long	9815
	.long	18579
	.long	18593
	.long	18611
	.long	18625
	.long	18637
	.long	18653
	.long	10865
	.long	18667
	.long	18679
	.long	18690
	.long	18701
	.long	18712
	.long	18732
	.long	18746
	.long	18769
	.long	18781
	.long	18801
	.long	18815
	.long	18833
	.long	18855
	.long	18869
	.long	18887
	.long	18901
	.long	18930
	.long	18948
	.long	18961
	.long	18978
	.long	18996
	.long	19022
	.long	12748
	.long	1
	.long	0
	.long	6
	.long	19061
	.long	19080
	.long	19096
	.long	19110
	.long	19127
	.long	19143
	.long	12840
	.long	18
	.long	0
	.long	1
	.long	19185
	.long	19191
	.long	12853
	.long	18
	.long	0
	.long	1
	.long	19185
	.long	19191
	.long	12866
	.long	1
	.long	0
	.long	3
	.long	19185
	.long	19191
	.long	19248
	.long	12888
	.long	1
	.long	0
	.long	3
	.long	19185
	.long	19191
	.long	19248
	.long	12910
	.long	1
	.long	0
	.long	18
	.long	19386
	.long	19396
	.long	19408
	.long	19415
	.long	19426
	.long	19431
	.long	19438
	.long	19445
	.long	19454
	.long	19459
	.long	19464
	.long	19480
	.long	2160
	.long	16135
	.long	19492
	.long	16146
	.long	19508
	.long	16161
	.long	13079
	.long	1
	.long	0
	.long	15
	.long	19386
	.long	19547
	.long	19557
	.long	19567
	.long	19578
	.long	15837
	.long	19588
	.long	19600
	.long	19608
	.long	19615
	.long	19625
	.long	19408
	.long	19415
	.long	19431
	.long	19635
	.long	13214
	.long	1
	.long	0
	.long	23
	.long	19386
	.long	19664
	.long	19567
	.long	19676
	.long	19689
	.long	19703
	.long	15837
	.long	19714
	.long	19408
	.long	19731
	.long	19431
	.long	19746
	.long	19760
	.long	2160
	.long	16117
	.long	19772
	.long	19789
	.long	16135
	.long	19492
	.long	16146
	.long	19508
	.long	16161
	.long	19806
	.long	13499
	.long	1
	.long	0
	.long	3
	.long	19857
	.long	19865
	.long	19873
	.long	13524
	.long	1
	.long	0
	.long	16
	.long	19914
	.long	19921
	.long	19936
	.long	19969
	.long	20004
	.long	20015
	.long	20046
	.long	20069
	.long	20082
	.long	20091
	.long	20122
	.long	20173
	.long	20200
	.long	20247
	.long	20276
	.long	20313
	.long	13677
	.long	1
	.long	0
	.long	36
	.long	20392
	.long	20405
	.long	20424
	.long	20437
	.long	20482
	.long	20519
	.long	20540
	.long	20571
	.long	20604
	.long	20649
	.long	20706
	.long	20741
	.long	20776
	.long	20821
	.long	20874
	.long	20913
	.long	20954
	.long	21009
	.long	21056
	.long	21089
	.long	21126
	.long	21141
	.long	21176
	.long	21199
	.long	21212
	.long	21227
	.long	21256
	.long	21281
	.long	21316
	.long	21353
	.long	21378
	.long	21399
	.long	21414
	.long	21431
	.long	21444
	.long	21477
	.long	14141
	.long	1
	.long	0
	.long	4
	.long	21539
	.long	21550
	.long	21574
	.long	21598
	.long	14201
	.long	54
	.long	1
	.long	2
	.long	4
	.long	8
	.long	14
	.long	16
	.long	32
	.long	62
	.long	64
	.long	128
	.long	256
	.long	448
	.long	512
	.long	1024
	.long	2048
	.long	3584
	.long	4096
	.long	8192
	.long	16384
	.long	28672
	.long	32768
	.long	65536
	.long	131072
	.long	262144
	.long	491521
	.long	524288
	.long	1048576
	.long	2097152
	.long	4194304
	.long	8388608
	.long	16777216
	.long	33554432
	.long	67108864
	.long	134217728
	.long	251658240
	.long	268435456
	.long	536870912
	.long	821559296
	.long	12509
	.long	12524
	.long	12545
	.long	12566
	.long	21655
	.long	12587
	.long	12607
	.long	21645
	.long	12624
	.long	12644
	.long	12663
	.long	21672
	.long	12680
	.long	12705
	.long	12723
	.long	21695
	.long	12740
	.long	12760
	.long	12779
	.long	21736
	.long	12803
	.long	12821
	.long	12832
	.long	12848
	.long	21636
	.long	12862
	.long	12883
	.long	12904
	.long	12926
	.long	12952
	.long	12974
	.long	12990
	.long	13010
	.long	13030
	.long	21726
	.long	13047
	.long	13071
	.long	21705
	.globl	_ZN6icu_6712PropNameData7indexesE
	.align 32
	.type	_ZN6icu_6712PropNameData7indexesE, @object
	.size	_ZN6icu_6712PropNameData7indexesE, 32
_ZN6icu_6712PropNameData7indexesE:
	.long	32
	.long	5560
	.long	20552
	.long	42650
	.long	42650
	.long	42650
	.long	47
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
