	.file	"ustrenum.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringEnumeration5cloneEv
	.type	_ZNK6icu_6717StringEnumeration5cloneEv, @function
_ZNK6icu_6717StringEnumeration5cloneEv:
.LFB2307:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2307:
	.size	_ZNK6icu_6717StringEnumeration5cloneEv, .-_ZNK6icu_6717StringEnumeration5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718UStringEnumeration17getDynamicClassIDEv
	.type	_ZNK6icu_6718UStringEnumeration17getDynamicClassIDEv, @function
_ZNK6icu_6718UStringEnumeration17getDynamicClassIDEv:
.LFB2328:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718UStringEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2328:
	.size	_ZNK6icu_6718UStringEnumeration17getDynamicClassIDEv, .-_ZNK6icu_6718UStringEnumeration17getDynamicClassIDEv
	.p2align 4
	.type	ustrenum_next, @function
ustrenum_next:
.LFB2332:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE2332:
	.size	ustrenum_next, .-ustrenum_next
	.p2align 4
	.type	ucharstrenum_count, @function
ucharstrenum_count:
.LFB2336:
	.cfi_startproc
	endbr64
	movl	60(%rdi), %eax
	ret
	.cfi_endproc
.LFE2336:
	.size	ucharstrenum_count, .-ucharstrenum_count
	.p2align 4
	.type	ucharstrenum_reset, @function
ucharstrenum_reset:
.LFB2339:
	.cfi_startproc
	endbr64
	movl	$0, 56(%rdi)
	ret
	.cfi_endproc
.LFE2339:
	.size	ucharstrenum_reset, .-ucharstrenum_reset
	.p2align 4
	.type	ucharstrenum_close, @function
ucharstrenum_close:
.LFB2335:
	.cfi_startproc
	endbr64
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2335:
	.size	ucharstrenum_close, .-ucharstrenum_close
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.type	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode, @function
_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode:
.LFB2309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*56(%rax)
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L9
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	testq	%r13, %r13
	je	.L10
	movswl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L11
	sarl	$5, %eax
.L12:
	movl	%eax, 0(%r13)
.L10:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	20(%rbx), %eax
	jmp	.L12
	.cfi_endproc
.LFE2309:
	.size	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode, .-_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.p2align 4
	.type	ucharstrenum_next, @function
ucharstrenum_next:
.LFB2338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movslq	56(%rdi), %rax
	cmpl	60(%rdi), %eax
	jge	.L25
	movq	8(%rdi), %rdx
	leal	1(%rax), %ecx
	movq	%rsi, %rbx
	movl	%ecx, 56(%rdi)
	movq	(%rdx,%rax,8), %r12
	testq	%rsi, %rsi
	je	.L23
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, (%rbx)
.L23:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2338:
	.size	ucharstrenum_next, .-ucharstrenum_next
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringEnumerationeqERKS0_
	.type	_ZNK6icu_6717StringEnumerationeqERKS0_, @function
_ZNK6icu_6717StringEnumerationeqERKS0_:
.LFB2313:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L35
	xorl	%eax, %eax
	cmpb	$42, (%rdi)
	je	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2313:
	.size	_ZNK6icu_6717StringEnumerationeqERKS0_, .-_ZNK6icu_6717StringEnumerationeqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718UStringEnumeration5countER10UErrorCode
	.type	_ZNK6icu_6718UStringEnumeration5countER10UErrorCode, @function
_ZNK6icu_6718UStringEnumeration5countER10UErrorCode:
.LFB2323:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rdi
	jmp	uenum_count_67@PLT
	.cfi_endproc
.LFE2323:
	.size	_ZNK6icu_6718UStringEnumeration5countER10UErrorCode, .-_ZNK6icu_6718UStringEnumeration5countER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UStringEnumeration4nextEPiR10UErrorCode
	.type	_ZN6icu_6718UStringEnumeration4nextEPiR10UErrorCode, @function
_ZN6icu_6718UStringEnumeration4nextEPiR10UErrorCode:
.LFB2324:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rdi
	jmp	uenum_next_67@PLT
	.cfi_endproc
.LFE2324:
	.size	_ZN6icu_6718UStringEnumeration4nextEPiR10UErrorCode, .-_ZN6icu_6718UStringEnumeration4nextEPiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UStringEnumeration5snextER10UErrorCode
	.type	_ZN6icu_6718UStringEnumeration5snextER10UErrorCode, @function
_ZN6icu_6718UStringEnumeration5snextER10UErrorCode:
.LFB2325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	-44(%rbp), %rsi
	pushq	%r12
	movq	%r13, %rdx
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	120(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	uenum_unext_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L40
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L45
	leaq	8(%rbx), %r13
	movl	-44(%rbp), %r14d
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	16(%rbx), %edx
	testw	%dx, %dx
	js	.L42
	sarl	$5, %edx
.L43:
	movq	%r12, %rcx
	movl	%r14d, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movq	%rax, %r12
.L40:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movl	20(%rbx), %edx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%r12d, %r12d
	jmp	.L40
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2325:
	.size	_ZN6icu_6718UStringEnumeration5snextER10UErrorCode, .-_ZN6icu_6718UStringEnumeration5snextER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UStringEnumeration5resetER10UErrorCode
	.type	_ZN6icu_6718UStringEnumeration5resetER10UErrorCode, @function
_ZN6icu_6718UStringEnumeration5resetER10UErrorCode:
.LFB2326:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rdi
	jmp	uenum_reset_67@PLT
	.cfi_endproc
.LFE2326:
	.size	_ZN6icu_6718UStringEnumeration5resetER10UErrorCode, .-_ZN6icu_6718UStringEnumeration5resetER10UErrorCode
	.p2align 4
	.type	ucharstrenum_unext, @function
ucharstrenum_unext:
.LFB2337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movslq	56(%rdi), %rax
	cmpl	60(%rdi), %eax
	jge	.L54
	movq	8(%rdi), %rdx
	leal	1(%rax), %ecx
	movq	%rsi, %rbx
	movl	%ecx, 56(%rdi)
	movq	(%rdx,%rax,8), %r12
	testq	%rsi, %rsi
	je	.L52
	movq	%r12, %rdi
	call	u_strlen_67@PLT
	movl	%eax, (%rbx)
.L52:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2337:
	.size	ucharstrenum_unext, .-ucharstrenum_unext
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode
	.type	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode, @function
_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode:
.LFB2308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	call	*56(%rax)
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L60
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L60
	leaq	8(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L61
	sarl	$5, %eax
.L62:
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L80
.L60:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	112(%rbx), %r8d
	addl	$1, %eax
	movq	104(%rbx), %rcx
	cmpl	%r8d, %eax
	jle	.L63
	movl	%r8d, %edx
	leaq	72(%rbx), %r15
	shrl	$31, %edx
	addl	%r8d, %edx
	sarl	%edx
	addl	%edx, %r8d
	cmpl	%eax, %r8d
	cmovl	%eax, %r8d
	cmpq	%rcx, %r15
	je	.L64
	movq	%rcx, %rdi
	movl	%r8d, -52(%rbp)
	call	uprv_free_67@PLT
	movl	-52(%rbp), %r8d
.L64:
	movslq	%r8d, %rdi
	movl	%r8d, -52(%rbp)
	call	uprv_malloc_67@PLT
	movl	-52(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, 104(%rbx)
	movq	%rax, %rcx
	je	.L81
	movl	(%r12), %eax
	movl	%r8d, 112(%rbx)
	testl	%eax, %eax
	jg	.L60
.L63:
	testq	%r13, %r13
	je	.L66
	movswl	16(%rbx), %eax
	testw	%ax, %ax
	js	.L67
	sarl	$5, %eax
.L68:
	movl	%eax, 0(%r13)
	movl	112(%rbx), %r8d
.L66:
	movq	%r14, %rdi
	xorl	%r9d, %r9d
	movl	$2147483647, %edx
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	104(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movl	20(%rbx), %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L67:
	movl	20(%rbx), %eax
	jmp	.L68
.L81:
	movq	%r15, 104(%rbx)
	movl	$32, 112(%rbx)
	movl	$7, (%r12)
	jmp	.L60
	.cfi_endproc
.LFE2308:
	.size	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode, .-_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringEnumeration5snextER10UErrorCode
	.type	_ZN6icu_6717StringEnumeration5snextER10UErrorCode, @function
_ZN6icu_6717StringEnumeration5snextER10UErrorCode:
.LFB2310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	-60(%rbp), %rsi
	pushq	%r12
	movq	%r13, %rdx
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*40(%rax)
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L87
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L87
	movl	-60(%rbp), %r15d
	testl	%r15d, %r15d
	js	.L95
.L84:
	addq	$8, %r12
	leal	1(%r15), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L85
	movq	%rax, %rsi
	movl	%r15d, %edx
	movq	%r14, %rdi
	call	u_charsToUChars_67@PLT
	movslq	%r15d, %rax
	xorl	%edx, %edx
	movl	%r15d, %esi
	movw	%dx, (%rbx,%rax,2)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
.L82:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	%rax, %rdi
	call	strlen@PLT
	movl	%eax, %r15d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$7, 0(%r13)
	xorl	%r12d, %r12d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L87:
	xorl	%r12d, %r12d
	jmp	.L82
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2310:
	.size	_ZN6icu_6717StringEnumeration5snextER10UErrorCode, .-_ZN6icu_6717StringEnumeration5snextER10UErrorCode
	.p2align 4
	.type	ustrenum_reset, @function
ustrenum_reset:
.LFB2333:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	leaq	_ZN6icu_6718UStringEnumeration5resetER10UErrorCode(%rip), %rdx
	movq	(%rdi), %rax
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L98
	movq	120(%rdi), %rdi
	jmp	uenum_reset_67@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	jmp	*%rax
	.cfi_endproc
.LFE2333:
	.size	ustrenum_reset, .-ustrenum_reset
	.p2align 4
	.type	ustrenum_count, @function
ustrenum_count:
.LFB2330:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	leaq	_ZNK6icu_6718UStringEnumeration5countER10UErrorCode(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L100
	movq	120(%rdi), %rdi
	jmp	uenum_count_67@PLT
	.p2align 4,,10
	.p2align 3
.L100:
	jmp	*%rax
	.cfi_endproc
.LFE2330:
	.size	ustrenum_count, .-ustrenum_count
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringEnumerationneERKS0_
	.type	_ZNK6icu_6717StringEnumerationneERKS0_, @function
_ZNK6icu_6717StringEnumerationneERKS0_:
.LFB2314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6717StringEnumerationeqERKS0_(%rip), %rcx
	movq	72(%rax), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	%rcx, %rdx
	jne	.L102
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L101
	cmpb	$42, (%rdi)
	movl	$1, %eax
	je	.L101
	call	strcmp@PLT
	testl	%eax, %eax
	setne	%al
.L101:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	call	*%rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	sete	%al
	ret
	.cfi_endproc
.LFE2314:
	.size	_ZNK6icu_6717StringEnumerationneERKS0_, .-_ZNK6icu_6717StringEnumerationneERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UStringEnumerationD2Ev
	.type	_ZN6icu_6718UStringEnumerationD2Ev, @function
_ZN6icu_6718UStringEnumerationD2Ev:
.LFB2320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718UStringEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	call	uenum_close_67@PLT
	movq	104(%r12), %rdi
	leaq	16+_ZTVN6icu_6717StringEnumerationE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L109
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L109
	call	uprv_free_67@PLT
.L109:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2320:
	.size	_ZN6icu_6718UStringEnumerationD2Ev, .-_ZN6icu_6718UStringEnumerationD2Ev
	.globl	_ZN6icu_6718UStringEnumerationD1Ev
	.set	_ZN6icu_6718UStringEnumerationD1Ev,_ZN6icu_6718UStringEnumerationD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UStringEnumerationD0Ev
	.type	_ZN6icu_6718UStringEnumerationD0Ev, @function
_ZN6icu_6718UStringEnumerationD0Ev:
.LFB2322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718UStringEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	120(%rdi), %rdi
	call	uenum_close_67@PLT
	movq	104(%r12), %rdi
	leaq	16+_ZTVN6icu_6717StringEnumerationE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L115
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L115
	call	uprv_free_67@PLT
.L115:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2322:
	.size	_ZN6icu_6718UStringEnumerationD0Ev, .-_ZN6icu_6718UStringEnumerationD0Ev
	.p2align 4
	.type	ustrenum_unext, @function
ustrenum_unext:
.LFB2331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	leaq	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode(%rip), %rdx
	subq	$8, %rsp
	movq	8(%rdi), %r14
	movq	(%r14), %rax
	movq	48(%rax), %rcx
	cmpq	%rdx, %rcx
	jne	.L121
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*56(%rax)
	movq	%rax, %rsi
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L122
	testq	%rsi, %rsi
	je	.L122
	leaq	8(%r14), %r12
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	testq	%r13, %r13
	je	.L123
	movswl	16(%r14), %eax
	testw	%ax, %ax
	js	.L124
	sarl	$5, %eax
.L125:
	movl	%eax, 0(%r13)
.L123:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString19getTerminatedBufferEv@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movl	20(%r14), %eax
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L121:
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rcx
	.cfi_endproc
.LFE2331:
	.size	ustrenum_unext, .-ustrenum_unext
	.p2align 4
	.type	ustrenum_close, @function
ustrenum_close:
.LFB2329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	8(%rdi), %r12
	testq	%r12, %r12
	je	.L137
	movq	(%r12), %rax
	leaq	_ZN6icu_6718UStringEnumerationD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L138
	leaq	16+_ZTVN6icu_6718UStringEnumerationE(%rip), %rax
	movq	120(%r12), %rdi
	movq	%rax, (%r12)
	call	uenum_close_67@PLT
	movq	104(%r12), %rdi
	leaq	16+_ZTVN6icu_6717StringEnumerationE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L139
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L139
	call	uprv_free_67@PLT
.L139:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L137:
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2329:
	.size	ustrenum_close, .-ustrenum_close
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringEnumerationC2Ev
	.type	_ZN6icu_6717StringEnumerationC2Ev, @function
_ZN6icu_6717StringEnumerationC2Ev:
.LFB2301:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	16+_ZTVN6icu_6717StringEnumerationE(%rip), %rdx
	movl	$32, 112(%rdi)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movl	$2, %eax
	movw	%ax, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	leaq	72(%rdi), %rax
	movq	%rax, 104(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE2301:
	.size	_ZN6icu_6717StringEnumerationC2Ev, .-_ZN6icu_6717StringEnumerationC2Ev
	.globl	_ZN6icu_6717StringEnumerationC1Ev
	.set	_ZN6icu_6717StringEnumerationC1Ev,_ZN6icu_6717StringEnumerationC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringEnumerationD2Ev
	.type	_ZN6icu_6717StringEnumerationD2Ev, @function
_ZN6icu_6717StringEnumerationD2Ev:
.LFB2304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	104(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L149
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L149
	call	uprv_free_67@PLT
.L149:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2304:
	.size	_ZN6icu_6717StringEnumerationD2Ev, .-_ZN6icu_6717StringEnumerationD2Ev
	.globl	_ZN6icu_6717StringEnumerationD1Ev
	.set	_ZN6icu_6717StringEnumerationD1Ev,_ZN6icu_6717StringEnumerationD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringEnumerationD0Ev
	.type	_ZN6icu_6717StringEnumerationD0Ev, @function
_ZN6icu_6717StringEnumerationD0Ev:
.LFB2306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	104(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L155
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L155
	call	uprv_free_67@PLT
.L155:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2306:
	.size	_ZN6icu_6717StringEnumerationD0Ev, .-_ZN6icu_6717StringEnumerationD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringEnumeration19ensureCharsCapacityEiR10UErrorCode
	.type	_ZN6icu_6717StringEnumeration19ensureCharsCapacityEiR10UErrorCode, @function
_ZN6icu_6717StringEnumeration19ensureCharsCapacityEiR10UErrorCode:
.LFB2311:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L168
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	112(%rdi), %edx
	movq	%rdi, %rbx
	cmpl	%edx, %esi
	jle	.L160
	movl	%edx, %eax
	movq	104(%rdi), %rdi
	leaq	72(%rbx), %r14
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	addl	%edx, %eax
	cmpl	%esi, %eax
	cmovge	%eax, %esi
	movl	%esi, %r13d
	cmpq	%r14, %rdi
	je	.L162
	call	uprv_free_67@PLT
.L162:
	movslq	%r13d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 104(%rbx)
	testq	%rax, %rax
	je	.L169
	movl	%r13d, 112(%rbx)
.L160:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L169:
	.cfi_restore_state
	movq	%r14, 104(%rbx)
	movl	$32, 112(%rbx)
	movl	$7, (%r12)
	jmp	.L160
	.cfi_endproc
.LFE2311:
	.size	_ZN6icu_6717StringEnumeration19ensureCharsCapacityEiR10UErrorCode, .-_ZN6icu_6717StringEnumeration19ensureCharsCapacityEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringEnumeration8setCharsEPKciR10UErrorCode
	.type	_ZN6icu_6717StringEnumeration8setCharsEPKciR10UErrorCode, @function
_ZN6icu_6717StringEnumeration8setCharsEPKciR10UErrorCode:
.LFB2312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L174
	movq	%rsi, %r14
	testq	%rsi, %rsi
	je	.L174
	movq	%rdi, %r12
	movl	%edx, %r13d
	movq	%rcx, %rbx
	testl	%edx, %edx
	js	.L182
.L172:
	addq	$8, %r12
	leal	1(%r13), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L173
	movq	%rax, %rsi
	movl	%r13d, %edx
	movq	%r14, %rdi
	call	u_charsToUChars_67@PLT
	movslq	%r13d, %rax
	xorl	%edx, %edx
	movl	%r13d, %esi
	movw	%dx, (%r15,%rax,2)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
.L170:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %r13d
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$7, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L174:
	xorl	%r12d, %r12d
	jmp	.L170
	.cfi_endproc
.LFE2312:
	.size	_ZN6icu_6717StringEnumeration8setCharsEPKciR10UErrorCode, .-_ZN6icu_6717StringEnumeration8setCharsEPKciR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UStringEnumeration16fromUEnumerationEP12UEnumerationR10UErrorCode
	.type	_ZN6icu_6718UStringEnumeration16fromUEnumerationEP12UEnumerationR10UErrorCode, @function
_ZN6icu_6718UStringEnumeration16fromUEnumerationEP12UEnumerationR10UErrorCode:
.LFB2315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L188
	movl	$128, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L186
	movl	$2, %edx
	leaq	16+_ZTVN6icu_6718UStringEnumerationE(%rip), %rcx
	movl	$32, 112(%rax)
	movw	%dx, 16(%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %xmm0
	movq	%rdx, 104(%rax)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rdx, %xmm1
	movq	%r12, 120(%rax)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
.L183:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	call	uenum_close_67@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L186:
	.cfi_restore_state
	movl	$7, (%rbx)
	movq	%r12, %rdi
	movq	%rax, -24(%rbp)
	call	uenum_close_67@PLT
	movq	-24(%rbp), %rax
	jmp	.L183
	.cfi_endproc
.LFE2315:
	.size	_ZN6icu_6718UStringEnumeration16fromUEnumerationEP12UEnumerationR10UErrorCode, .-_ZN6icu_6718UStringEnumeration16fromUEnumerationEP12UEnumerationR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UStringEnumerationC2EP12UEnumeration
	.type	_ZN6icu_6718UStringEnumerationC2EP12UEnumeration, @function
_ZN6icu_6718UStringEnumerationC2EP12UEnumeration:
.LFB2317:
	.cfi_startproc
	endbr64
	movl	$2, %eax
	leaq	16+_ZTVN6icu_6718UStringEnumerationE(%rip), %rdx
	movl	$32, 112(%rdi)
	movw	%ax, 16(%rdi)
	leaq	72(%rdi), %rax
	movq	%rdx, %xmm0
	movq	%rax, 104(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, %xmm1
	movq	%rsi, 120(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE2317:
	.size	_ZN6icu_6718UStringEnumerationC2EP12UEnumeration, .-_ZN6icu_6718UStringEnumerationC2EP12UEnumeration
	.globl	_ZN6icu_6718UStringEnumerationC1EP12UEnumeration
	.set	_ZN6icu_6718UStringEnumerationC1EP12UEnumeration,_ZN6icu_6718UStringEnumerationC2EP12UEnumeration
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UStringEnumeration16getStaticClassIDEv
	.type	_ZN6icu_6718UStringEnumeration16getStaticClassIDEv, @function
_ZN6icu_6718UStringEnumeration16getStaticClassIDEv:
.LFB2327:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718UStringEnumeration16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2327:
	.size	_ZN6icu_6718UStringEnumeration16getStaticClassIDEv, .-_ZN6icu_6718UStringEnumeration16getStaticClassIDEv
	.p2align 4
	.globl	uenum_openFromStringEnumeration_67
	.type	uenum_openFromStringEnumeration_67, @function
uenum_openFromStringEnumeration_67:
.LFB2334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testl	%eax, %eax
	jle	.L205
	testq	%rdi, %rdi
	je	.L199
.L195:
	movq	(%r12), %rax
	leaq	_ZN6icu_6718UStringEnumerationD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L196
	leaq	16+_ZTVN6icu_6718UStringEnumerationE(%rip), %rax
	movq	120(%r12), %rdi
	movq	%rax, (%r12)
	call	uenum_close_67@PLT
	movq	104(%r12), %rdi
	leaq	16+_ZTVN6icu_6717StringEnumerationE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L197
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L197
	call	uprv_free_67@PLT
.L197:
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	xorl	%eax, %eax
.L191:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L191
	movl	$56, %edi
	movq	%rsi, %rbx
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L206
	movdqa	_ZL11USTRENUM_VT(%rip), %xmm0
	movdqa	16+_ZL11USTRENUM_VT(%rip), %xmm1
	movdqa	32+_ZL11USTRENUM_VT(%rip), %xmm2
	movq	48+_ZL11USTRENUM_VT(%rip), %rdx
	movups	%xmm0, (%rax)
	movq	%rdx, 48(%rax)
	movq	%r12, 8(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L191
.L206:
	movl	$7, (%rbx)
	jmp	.L195
	.cfi_endproc
.LFE2334:
	.size	uenum_openFromStringEnumeration_67, .-uenum_openFromStringEnumeration_67
	.p2align 4
	.globl	uenum_openCharStringsEnumeration_67
	.type	uenum_openCharStringsEnumeration_67, @function
uenum_openCharStringsEnumeration_67:
.LFB2340:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L219
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	testl	%esi, %esi
	js	.L207
	movq	%rdi, %r13
	movq	%rdx, %r12
	je	.L213
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.L213
.L207:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movl	$64, %edi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L223
	movdqa	_ZL15UCHARSTRENUM_VT(%rip), %xmm0
	movdqa	16+_ZL15UCHARSTRENUM_VT(%rip), %xmm1
	movl	$0, 56(%rax)
	movdqa	32+_ZL15UCHARSTRENUM_VT(%rip), %xmm2
	movq	48+_ZL15UCHARSTRENUM_VT(%rip), %rdx
	movl	%ebx, 60(%rax)
	movups	%xmm0, (%rax)
	movq	%rdx, 48(%rax)
	movq	%r13, 8(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
.L223:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, (%r12)
	jmp	.L207
	.cfi_endproc
.LFE2340:
	.size	uenum_openCharStringsEnumeration_67, .-uenum_openCharStringsEnumeration_67
	.p2align 4
	.globl	uenum_openUCharStringsEnumeration_67
	.type	uenum_openUCharStringsEnumeration_67, @function
uenum_openUCharStringsEnumeration_67:
.LFB2341:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L236
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	testl	%esi, %esi
	js	.L224
	movq	%rdi, %r13
	movq	%rdx, %r12
	je	.L230
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.L230
.L224:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movl	$64, %edi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L240
	movdqa	_ZL17UCHARSTRENUM_U_VT(%rip), %xmm0
	movdqa	16+_ZL17UCHARSTRENUM_U_VT(%rip), %xmm1
	movl	$0, 56(%rax)
	movdqa	32+_ZL17UCHARSTRENUM_U_VT(%rip), %xmm2
	movq	48+_ZL17UCHARSTRENUM_U_VT(%rip), %rdx
	movl	%ebx, 60(%rax)
	movups	%xmm0, (%rax)
	movq	%rdx, 48(%rax)
	movq	%r13, 8(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
.L240:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, (%r12)
	jmp	.L224
	.cfi_endproc
.LFE2341:
	.size	uenum_openUCharStringsEnumeration_67, .-uenum_openUCharStringsEnumeration_67
	.weak	_ZTSN6icu_6717StringEnumerationE
	.section	.rodata._ZTSN6icu_6717StringEnumerationE,"aG",@progbits,_ZTSN6icu_6717StringEnumerationE,comdat
	.align 16
	.type	_ZTSN6icu_6717StringEnumerationE, @object
	.size	_ZTSN6icu_6717StringEnumerationE, 29
_ZTSN6icu_6717StringEnumerationE:
	.string	"N6icu_6717StringEnumerationE"
	.weak	_ZTIN6icu_6717StringEnumerationE
	.section	.data.rel.ro._ZTIN6icu_6717StringEnumerationE,"awG",@progbits,_ZTIN6icu_6717StringEnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringEnumerationE, @object
	.size	_ZTIN6icu_6717StringEnumerationE, 24
_ZTIN6icu_6717StringEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringEnumerationE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6718UStringEnumerationE
	.section	.rodata._ZTSN6icu_6718UStringEnumerationE,"aG",@progbits,_ZTSN6icu_6718UStringEnumerationE,comdat
	.align 16
	.type	_ZTSN6icu_6718UStringEnumerationE, @object
	.size	_ZTSN6icu_6718UStringEnumerationE, 30
_ZTSN6icu_6718UStringEnumerationE:
	.string	"N6icu_6718UStringEnumerationE"
	.weak	_ZTIN6icu_6718UStringEnumerationE
	.section	.data.rel.ro._ZTIN6icu_6718UStringEnumerationE,"awG",@progbits,_ZTIN6icu_6718UStringEnumerationE,comdat
	.align 8
	.type	_ZTIN6icu_6718UStringEnumerationE, @object
	.size	_ZTIN6icu_6718UStringEnumerationE, 24
_ZTIN6icu_6718UStringEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718UStringEnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.weak	_ZTVN6icu_6717StringEnumerationE
	.section	.data.rel.ro._ZTVN6icu_6717StringEnumerationE,"awG",@progbits,_ZTVN6icu_6717StringEnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6717StringEnumerationE, @object
	.size	_ZTVN6icu_6717StringEnumerationE, 104
_ZTVN6icu_6717StringEnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6717StringEnumerationE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_6717StringEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5snextER10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.weak	_ZTVN6icu_6718UStringEnumerationE
	.section	.data.rel.ro.local._ZTVN6icu_6718UStringEnumerationE,"awG",@progbits,_ZTVN6icu_6718UStringEnumerationE,comdat
	.align 8
	.type	_ZTVN6icu_6718UStringEnumerationE, @object
	.size	_ZTVN6icu_6718UStringEnumerationE, 104
_ZTVN6icu_6718UStringEnumerationE:
	.quad	0
	.quad	_ZTIN6icu_6718UStringEnumerationE
	.quad	_ZN6icu_6718UStringEnumerationD1Ev
	.quad	_ZN6icu_6718UStringEnumerationD0Ev
	.quad	_ZNK6icu_6718UStringEnumeration17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	_ZNK6icu_6718UStringEnumeration5countER10UErrorCode
	.quad	_ZN6icu_6718UStringEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6718UStringEnumeration5snextER10UErrorCode
	.quad	_ZN6icu_6718UStringEnumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL17UCHARSTRENUM_U_VT, @object
	.size	_ZL17UCHARSTRENUM_U_VT, 56
_ZL17UCHARSTRENUM_U_VT:
	.quad	0
	.quad	0
	.quad	ucharstrenum_close
	.quad	ucharstrenum_count
	.quad	ucharstrenum_unext
	.quad	uenum_nextDefault_67
	.quad	ucharstrenum_reset
	.align 32
	.type	_ZL15UCHARSTRENUM_VT, @object
	.size	_ZL15UCHARSTRENUM_VT, 56
_ZL15UCHARSTRENUM_VT:
	.quad	0
	.quad	0
	.quad	ucharstrenum_close
	.quad	ucharstrenum_count
	.quad	uenum_unextDefault_67
	.quad	ucharstrenum_next
	.quad	ucharstrenum_reset
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL11USTRENUM_VT, @object
	.size	_ZL11USTRENUM_VT, 56
_ZL11USTRENUM_VT:
	.quad	0
	.quad	0
	.quad	ustrenum_close
	.quad	ustrenum_count
	.quad	ustrenum_unext
	.quad	ustrenum_next
	.quad	ustrenum_reset
	.local	_ZZN6icu_6718UStringEnumeration16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6718UStringEnumeration16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
