	.file	"uscript.cpp"
	.text
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2621:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2621:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2624:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	cmpb	$0, 12(%rbx)
	jne	.L16
.L7:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L7
	.cfi_endproc
.LFE2624:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2627:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2627:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2630:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L22
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2630:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L28
.L24:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L29
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2632:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2633:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2633:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2634:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2634:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2635:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2635:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2636:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2636:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2637:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2637:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2638:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L45
	testl	%edx, %edx
	jle	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L48
.L37:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L37
	.cfi_endproc
.LFE2638:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L52
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L52
	testl	%r12d, %r12d
	jg	.L59
	cmpb	$0, 12(%rbx)
	jne	.L60
.L54:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L54
.L60:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2639:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L62
	movq	(%rdi), %r8
.L63:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L66
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L66
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2640:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2641:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2641:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2642:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2642:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2643:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2643:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2644:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2644:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2646:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2646:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2648:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2648:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Hant"
.LC1:
	.string	"zh"
	.text
	.p2align 4
	.globl	uscript_getCode_67
	.type	uscript_getCode_67, @function
uscript_getCode_67:
.LFB2361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L156
	movq	%rdi, %r15
	movq	%rcx, %r14
	testq	%rdi, %rdi
	je	.L82
	movq	%rsi, %r12
	movl	%edx, %ebx
	testq	%rsi, %rsi
	je	.L163
	testl	%edx, %edx
	js	.L82
.L84:
	movl	$45, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L164
.L85:
	movl	$0, -164(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movb	$0, -180(%rbp)
.L88:
	leaq	-72(%rbp), %rax
	leaq	-160(%rbp), %r13
	movl	$8, %edx
	movq	%r15, %rdi
	movq	%r13, %rcx
	movq	%rax, %rsi
	movq	%rax, -192(%rbp)
	call	uloc_getLanguage_67@PLT
	movl	-160(%rbp), %eax
	testl	%eax, %eax
	jg	.L91
	cmpl	$-124, %eax
	je	.L91
	movq	-192(%rbp), %rax
	cmpb	$106, (%rax)
	je	.L165
.L93:
	cmpb	$107, -72(%rbp)
	je	.L166
.L96:
	leaq	-64(%rbp), %r10
	movl	$8, %edx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	%r10, %rsi
	movq	%r10, -200(%rbp)
	call	uloc_getScript_67@PLT
	movl	-160(%rbp), %edx
	testl	%edx, %edx
	jg	.L91
	cmpl	$-124, %edx
	je	.L91
	cmpb	$122, -72(%rbp)
	movq	-200(%rbp), %r10
	jne	.L100
	cmpb	$104, -71(%rbp)
	jne	.L100
	movq	-192(%rbp), %rcx
	cmpb	$0, 2(%rcx)
	jne	.L100
	movl	$5, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%r10, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L100
	movl	(%r14), %r13d
	testl	%r13d, %r13d
	jg	.L156
	subl	$1, %ebx
	jle	.L97
	movabsq	$21474836497, %rax
	movq	%rax, (%r12)
	movl	$2, %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L163:
	testl	%edx, %edx
	je	.L84
.L82:
	movl	$1, (%r14)
.L156:
	xorl	%eax, %eax
.L79:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L167
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movl	$95, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L85
	movq	%r15, %rsi
	movl	$4106, %edi
	call	u_getPropertyValueEnum_67@PLT
	cmpl	$-1, %eax
	je	.L168
.L162:
	movl	(%r14), %r11d
	testl	%r11d, %r11d
	jg	.L156
	testl	%ebx, %ebx
	jle	.L169
	movl	%eax, (%r12)
	movl	$1, %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L91:
	movl	(%r14), %r9d
	testl	%r9d, %r9d
	jg	.L156
	leaq	-131(%rbp), %rax
	xorl	%r10d, %r10d
	leaq	-144(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -144(%rbp)
	movw	%r10w, -132(%rbp)
	movl	$0, -88(%rbp)
	movl	$40, -136(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	leaq	-164(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	ulocimp_addLikelySubtags_67@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	-164(%rbp), %eax
	cmpl	$-124, %eax
	je	.L106
	testl	%eax, %eax
	jle	.L170
.L106:
	cmpb	$0, -180(%rbp)
	je	.L171
.L122:
	xorl	%eax, %eax
.L113:
	cmpb	$0, -132(%rbp)
	je	.L79
	movq	-144(%rbp), %rdi
	movl	%eax, -180(%rbp)
	call	uprv_free_67@PLT
	movl	-180(%rbp), %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L165:
	cmpb	$97, 1(%rax)
	jne	.L93
	cmpb	$0, 2(%rax)
	jne	.L93
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L156
	cmpl	$2, %ebx
	jle	.L94
	movabsq	$85899345942, %rax
	movl	$17, 8(%r12)
	movq	%rax, (%r12)
	movl	$3, %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r15, %rsi
	movl	$4106, %edi
	call	u_getPropertyValueEnum_67@PLT
	cmpl	$-1, %eax
	je	.L122
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L122
.L161:
	testl	%ebx, %ebx
	jle	.L172
	movl	%eax, (%r12)
	movl	$1, %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L170:
	movl	(%r14), %r8d
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	-144(%rbp), %rdi
	movl	$0, -160(%rbp)
	testl	%r8d, %r8d
	jg	.L122
	movq	-192(%rbp), %rsi
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%rdi, -200(%rbp)
	call	uloc_getLanguage_67@PLT
	movl	-160(%rbp), %eax
	testl	%eax, %eax
	jg	.L119
	cmpl	$-124, %eax
	je	.L119
	movq	-192(%rbp), %rax
	movq	-200(%rbp), %rdi
	cmpb	$106, (%rax)
	jne	.L111
	cmpb	$97, 1(%rax)
	jne	.L111
	cmpb	$0, 2(%rax)
	jne	.L111
	movl	(%r14), %edi
	testl	%edi, %edi
	jg	.L122
	cmpl	$2, %ebx
	jle	.L112
	movabsq	$85899345942, %rax
	movl	$17, 8(%r12)
	movq	%rax, (%r12)
	movl	$3, %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L168:
	movl	(%r14), %eax
	movq	$0, -72(%rbp)
	movl	$0, -164(%rbp)
	movl	$0, -160(%rbp)
	movq	$0, -64(%rbp)
	testl	%eax, %eax
	jg	.L156
	movb	$1, -180(%rbp)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L166:
	cmpb	$111, -71(%rbp)
	jne	.L96
	movq	-192(%rbp), %rax
	cmpb	$0, 2(%rax)
	jne	.L96
	movl	(%r14), %r15d
	testl	%r15d, %r15d
	jg	.L156
	cmpl	$1, %ebx
	jle	.L97
	movabsq	$73014444050, %rax
	movq	%rax, (%r12)
	movl	$2, %eax
	jmp	.L79
.L94:
	movl	$15, (%r14)
	movl	$3, %eax
	jmp	.L79
.L119:
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L106
	jmp	.L122
.L97:
	movl	$15, (%r14)
	movl	$2, %eax
	jmp	.L79
.L100:
	testl	%eax, %eax
	je	.L91
	movq	%r10, %rsi
	movl	$4106, %edi
	call	u_getPropertyValueEnum_67@PLT
	cmpl	$-1, %eax
	je	.L91
	leal	-73(%rax), %edx
	cmpl	$1, %edx
	movl	$17, %edx
	cmovbe	%edx, %eax
	jmp	.L162
.L169:
	movl	$15, (%r14)
	movl	$1, %eax
	jmp	.L79
.L172:
	movl	$15, (%r14)
	movl	$1, %eax
	jmp	.L113
.L111:
	cmpb	$107, -72(%rbp)
	je	.L173
.L115:
	leaq	-64(%rbp), %r10
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%r10, %rsi
	movq	%r10, -200(%rbp)
	call	uloc_getScript_67@PLT
	movl	%eax, %r13d
	movl	-160(%rbp), %eax
	testl	%eax, %eax
	jg	.L119
	cmpl	$-124, %eax
	je	.L119
	movq	-192(%rbp), %rdi
	leaq	.LC1(%rip), %rsi
	call	strcmp@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jne	.L118
	movq	%r10, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%r10, -192(%rbp)
	call	strcmp@PLT
	movq	-192(%rbp), %r10
	testl	%eax, %eax
	jne	.L118
	cmpl	$0, (%r14)
	jg	.L122
	subl	$1, %ebx
	jle	.L116
	movabsq	$21474836497, %rax
	movq	%rax, (%r12)
	movl	$2, %eax
	jmp	.L113
.L173:
	cmpb	$111, -71(%rbp)
	jne	.L115
	movq	-192(%rbp), %rax
	cmpb	$0, 2(%rax)
	jne	.L115
	movl	(%r14), %esi
	testl	%esi, %esi
	jg	.L122
	subl	$1, %ebx
	jle	.L116
	movabsq	$73014444050, %rax
	movq	%rax, (%r12)
	movl	$2, %eax
	jmp	.L113
.L167:
	call	__stack_chk_fail@PLT
.L112:
	movl	$15, (%r14)
	movl	$3, %eax
	jmp	.L113
.L116:
	movl	$15, (%r14)
	movl	$2, %eax
	jmp	.L113
.L118:
	testl	%r13d, %r13d
	je	.L119
	movq	%r10, %rsi
	movl	$4106, %edi
	call	u_getPropertyValueEnum_67@PLT
	cmpl	$-1, %eax
	je	.L119
	leal	-73(%rax), %edx
	cmpl	$1, %edx
	movl	$17, %edx
	cmovbe	%edx, %eax
	cmpl	$0, (%r14)
	jle	.L161
	jmp	.L122
	.cfi_endproc
.LFE2361:
	.size	uscript_getCode_67, .-uscript_getCode_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
