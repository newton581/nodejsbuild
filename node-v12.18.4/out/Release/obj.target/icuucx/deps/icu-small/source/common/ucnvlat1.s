	.file	"ucnvlat1.cpp"
	.text
	.p2align 4
	.type	_Latin1ToUnicodeWithOffsets, @function
_Latin1ToUnicodeWithOffsets:
.LFB2028:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rcx
	movq	40(%rdi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %r8
	movq	24(%rdi), %rdx
	subq	%rcx, %rax
	movq	48(%rdi), %r10
	subq	%r8, %rdx
	sarq	%rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	movl	%edx, %r9d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpl	%edx, %eax
	jge	.L2
	movl	$15, (%rsi)
	movl	%eax, %r9d
.L2:
	cmpl	$7, %r9d
	jle	.L3
	movl	%r9d, %esi
	andl	$7, %r9d
	sarl	$3, %esi
	movslq	%esi, %rax
	leaq	(%r8,%rax,8), %rdx
	cmpq	%rdx, %rcx
	setnb	%dl
	salq	$4, %rax
	addq	%rcx, %rax
	cmpq	%rax, %r8
	setnb	%al
	orb	%al, %dl
	je	.L4
	cmpl	$1, %esi
	je	.L4
	movl	%esi, %edx
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	shrl	%edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L5:
	movdqu	(%r8,%rax), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, 16(%rcx,%rax,2)
	movups	%xmm2, (%rcx,%rax,2)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L5
	movl	%esi, %r11d
	leal	-1(%rsi), %ebx
	andl	$-2, %r11d
	movl	%r11d, %eax
	leaq	(%r8,%rax,8), %rdx
	salq	$4, %rax
	addq	%rcx, %rax
	cmpl	%r11d, %esi
	je	.L8
	movzbl	(%rdx), %r11d
	movw	%r11w, (%rax)
	movzbl	1(%rdx), %r11d
	movw	%r11w, 2(%rax)
	movzbl	2(%rdx), %r11d
	movw	%r11w, 4(%rax)
	movzbl	3(%rdx), %r11d
	movw	%r11w, 6(%rax)
	movzbl	4(%rdx), %r11d
	movw	%r11w, 8(%rax)
	movzbl	5(%rdx), %r11d
	movw	%r11w, 10(%rax)
	movzbl	6(%rdx), %r11d
	movw	%r11w, 12(%rax)
	movzbl	7(%rdx), %edx
	movw	%dx, 14(%rax)
.L8:
	movslq	%ebx, %r11
	addq	$1, %r11
	movq	%r11, %rax
	leaq	(%r8,%r11,8), %r8
	salq	$4, %rax
	addq	%rax, %rcx
	testq	%r10, %r10
	je	.L9
	cmpl	$2, %ebx
	jle	.L24
	movl	%esi, %edx
	movq	%r10, %rax
	movdqa	.LC0(%rip), %xmm5
	movdqa	.LC1(%rip), %xmm14
	movdqa	.LC2(%rip), %xmm13
	shrl	$2, %edx
	movdqa	.LC3(%rip), %xmm12
	movdqa	.LC4(%rip), %xmm11
	salq	$7, %rdx
	movdqa	.LC5(%rip), %xmm10
	movdqa	.LC6(%rip), %xmm9
	addq	%r10, %rdx
	.p2align 4,,10
	.p2align 3
.L11:
	movdqa	%xmm5, %xmm0
	movdqa	.LC7(%rip), %xmm7
	subq	$-128, %rax
	movdqa	.LC8(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm15
	movdqa	%xmm0, %xmm4
	paddd	%xmm10, %xmm15
	paddd	%xmm13, %xmm1
	movdqa	%xmm0, %xmm8
	movdqa	%xmm0, %xmm2
	paddd	%xmm0, %xmm7
	paddd	%xmm0, %xmm6
	paddd	%xmm9, %xmm8
	punpckldq	%xmm15, %xmm2
	paddd	%xmm12, %xmm4
	movdqa	%xmm0, %xmm3
	punpckhdq	%xmm15, %xmm0
	movdqa	%xmm1, %xmm15
	punpckldq	%xmm8, %xmm15
	punpckhdq	%xmm8, %xmm1
	paddd	%xmm11, %xmm3
	movdqa	%xmm4, %xmm8
	punpckhdq	%xmm7, %xmm4
	paddd	%xmm14, %xmm5
	punpckldq	%xmm7, %xmm8
	movdqa	%xmm3, %xmm7
	punpckhdq	%xmm6, %xmm3
	punpckldq	%xmm6, %xmm7
	movdqa	%xmm2, %xmm6
	punpckhdq	%xmm8, %xmm2
	punpckldq	%xmm8, %xmm6
	movdqa	%xmm0, %xmm8
	punpckhdq	%xmm4, %xmm0
	punpckldq	%xmm4, %xmm8
	movdqa	%xmm15, %xmm4
	punpckhdq	%xmm7, %xmm15
	punpckldq	%xmm7, %xmm4
	movdqa	%xmm1, %xmm7
	punpckhdq	%xmm3, %xmm1
	punpckldq	%xmm3, %xmm7
	movdqa	%xmm6, %xmm3
	punpckhdq	%xmm4, %xmm6
	punpckldq	%xmm4, %xmm3
	movups	%xmm6, -112(%rax)
	movups	%xmm3, -128(%rax)
	movdqa	%xmm2, %xmm3
	punpckhdq	%xmm15, %xmm2
	movups	%xmm2, -80(%rax)
	movdqa	%xmm8, %xmm2
	punpckldq	%xmm15, %xmm3
	punpckhdq	%xmm7, %xmm8
	punpckldq	%xmm7, %xmm2
	movups	%xmm3, -96(%rax)
	movups	%xmm2, -64(%rax)
	movdqa	%xmm0, %xmm2
	punpckhdq	%xmm1, %xmm0
	punpckldq	%xmm1, %xmm2
	movups	%xmm8, -48(%rax)
	movups	%xmm2, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rdx, %rax
	jne	.L11
	movl	%esi, %ebx
	movl	%esi, %r12d
	andl	$-4, %ebx
	movl	%ebx, %eax
	leal	0(,%rbx,8), %edx
	subl	%ebx, %r12d
	salq	$5, %rax
	addq	%r10, %rax
	cmpl	%esi, %ebx
	je	.L15
.L10:
	leal	1(%rdx), %ebx
	movl	%edx, (%rax)
	leal	8(%rdx), %r13d
	movl	%ebx, 4(%rax)
	leal	2(%rdx), %ebx
	movl	%ebx, 8(%rax)
	leal	3(%rdx), %ebx
	movl	%ebx, 12(%rax)
	leal	4(%rdx), %ebx
	movl	%ebx, 16(%rax)
	leal	5(%rdx), %ebx
	movl	%ebx, 20(%rax)
	leal	6(%rdx), %ebx
	movl	%ebx, 24(%rax)
	leal	7(%rdx), %ebx
	movl	%ebx, 28(%rax)
	cmpl	$1, %r12d
	je	.L15
	leal	9(%rdx), %ebx
	movl	%r13d, 32(%rax)
	leal	16(%rdx), %r13d
	movl	%ebx, 36(%rax)
	leal	10(%rdx), %ebx
	movl	%ebx, 40(%rax)
	leal	11(%rdx), %ebx
	movl	%ebx, 44(%rax)
	leal	12(%rdx), %ebx
	movl	%ebx, 48(%rax)
	leal	13(%rdx), %ebx
	movl	%ebx, 52(%rax)
	leal	14(%rdx), %ebx
	movl	%ebx, 56(%rax)
	leal	15(%rdx), %ebx
	movl	%ebx, 60(%rax)
	cmpl	$2, %r12d
	je	.L15
	leal	17(%rdx), %ebx
	movl	%r13d, 64(%rax)
	movl	%ebx, 68(%rax)
	leal	18(%rdx), %ebx
	movl	%ebx, 72(%rax)
	leal	19(%rdx), %ebx
	movl	%ebx, 76(%rax)
	leal	20(%rdx), %ebx
	movl	%ebx, 80(%rax)
	leal	21(%rdx), %ebx
	movl	%ebx, 84(%rax)
	leal	22(%rdx), %ebx
	addl	$23, %edx
	movl	%ebx, 88(%rax)
	movl	%edx, 92(%rax)
.L15:
	salq	$5, %r11
	sall	$3, %esi
	addq	%r11, %r10
	testl	%r9d, %r9d
	jne	.L14
	movq	%r8, 16(%rdi)
	movq	%rcx, 32(%rdi)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L3:
	testl	%r9d, %r9d
	jle	.L16
.L80:
	xorl	%esi, %esi
.L14:
	movzbl	(%r8), %eax
	movl	%r9d, %r11d
	movw	%ax, (%rcx)
	subl	$1, %r11d
	je	.L17
	movzbl	1(%r8), %eax
	movw	%ax, 2(%rcx)
	cmpl	$2, %r9d
	je	.L17
	movzbl	2(%r8), %eax
	movw	%ax, 4(%rcx)
	cmpl	$3, %r9d
	je	.L17
	movzbl	3(%r8), %eax
	movw	%ax, 6(%rcx)
	cmpl	$4, %r9d
	je	.L17
	movzbl	4(%r8), %eax
	movw	%ax, 8(%rcx)
	cmpl	$5, %r9d
	je	.L17
	movzbl	5(%r8), %eax
	movw	%ax, 10(%rcx)
	cmpl	$6, %r9d
	je	.L17
	movzbl	6(%r8), %eax
	movw	%ax, 12(%rcx)
.L17:
	movslq	%r11d, %rax
	leaq	1(%rax), %rdx
	addq	%rdx, %r8
	leaq	(%rcx,%rdx,2), %rax
	movq	%r8, 16(%rdi)
	movq	%rax, 32(%rdi)
	testq	%r10, %r10
	je	.L1
	movl	%esi, (%r10)
	leal	1(%rsi), %eax
	testl	%r11d, %r11d
	je	.L18
	movl	%eax, 4(%r10)
	leal	2(%rsi), %ecx
	cmpl	$2, %r9d
	je	.L18
	movl	%ecx, 8(%r10)
	leal	3(%rsi), %eax
	cmpl	$3, %r9d
	je	.L18
	movl	%eax, 12(%r10)
	leal	4(%rsi), %ecx
	cmpl	$4, %r9d
	je	.L18
	movl	%ecx, 16(%r10)
	leal	5(%rsi), %eax
	cmpl	$5, %r9d
	je	.L18
	movl	%eax, 20(%r10)
	addl	$6, %esi
	cmpl	$6, %r9d
	je	.L18
	movl	%esi, 24(%r10)
.L18:
	leaq	(%r10,%rdx,4), %r10
.L19:
	movq	%r10, 48(%rdi)
.L1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L16:
	.cfi_restore_state
	movq	%r8, 16(%rdi)
	movq	%rcx, 32(%rdi)
	testq	%r10, %r10
	jne	.L19
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	leal	-1(%rsi), %eax
	movq	%rcx, %rdx
	movq	%rax, %rbx
	leaq	8(%r8,%rax,8), %r12
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L7:
	movzbl	(%rax), %r11d
	addq	$8, %rax
	addq	$16, %rdx
	movw	%r11w, -16(%rdx)
	movzbl	-7(%rax), %r11d
	movw	%r11w, -14(%rdx)
	movzbl	-6(%rax), %r11d
	movw	%r11w, -12(%rdx)
	movzbl	-5(%rax), %r11d
	movw	%r11w, -10(%rdx)
	movzbl	-4(%rax), %r11d
	movw	%r11w, -8(%rdx)
	movzbl	-3(%rax), %r11d
	movw	%r11w, -6(%rdx)
	movzbl	-2(%rax), %r11d
	movw	%r11w, -4(%rdx)
	movzbl	-1(%rax), %r11d
	movw	%r11w, -2(%rdx)
	cmpq	%r12, %rax
	jne	.L7
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	testl	%r9d, %r9d
	jne	.L80
	movq	%r8, 16(%rdi)
	movq	%rcx, 32(%rdi)
	jmp	.L1
.L24:
	movl	%esi, %r12d
	movq	%r10, %rax
	xorl	%edx, %edx
	jmp	.L10
	.cfi_endproc
.LFE2028:
	.size	_Latin1ToUnicodeWithOffsets, .-_Latin1ToUnicodeWithOffsets
	.p2align 4
	.type	_Latin1GetNextUChar, @function
_Latin1GetNextUChar:
.LFB2029:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpq	%rax, 24(%rdi)
	ja	.L85
	movl	$8, (%rsi)
	movl	$65535, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rdi)
	movzbl	(%rax), %eax
	ret
	.cfi_endproc
.LFE2029:
	.size	_Latin1GetNextUChar, .-_Latin1GetNextUChar
	.p2align 4
	.type	_Latin1FromUnicodeWithOffsets, @function
_Latin1FromUnicodeWithOffsets:
.LFB2030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_Latin1Data_67(%rip), %rcx
	movl	$255, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movq	8(%rdi), %r15
	movq	32(%rdi), %r14
	movq	40(%rdi), %rax
	movq	24(%rdi), %rbx
	movq	16(%rdi), %rdx
	movl	84(%r15), %r9d
	subq	%r14, %rax
	cmpq	%rcx, 48(%r15)
	movl	$127, %ecx
	movq	%rbx, -112(%rbp)
	cmovne	%ecx, %r10d
	subq	%rdx, %rbx
	movq	48(%rdi), %r11
	movq	%rbx, %r13
	sarq	%r13
	cmpl	%r13d, %eax
	cmovl	%eax, %r13d
	testl	%r9d, %r9d
	je	.L88
	movq	%r14, %rax
	movl	$-1, %r10d
	testl	%r13d, %r13d
	jle	.L156
.L89:
	cmpq	-112(%rbp), %rdx
	jnb	.L104
	movzwl	(%rdx), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	je	.L105
	movl	%r9d, %esi
	movl	%r9d, %ecx
	movq	%rdx, %r8
	andl	$-2048, %esi
.L106:
	cmpl	$55296, %esi
	movl	%r10d, %r9d
	movq	%r14, %rsi
	movl	$10, %edx
	movq	%rax, %r14
	jne	.L103
.L123:
	movl	$12, %edx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L88:
	cmpl	$15, %r13d
	jg	.L157
	movq	%r14, %rsi
	testl	%r13d, %r13d
	jle	.L90
	movq	%rdx, %r8
.L117:
	leal	-1(%r13), %eax
	leaq	2(%r8,%rax,2), %rdx
	.p2align 4,,10
	.p2align 3
.L102:
	movzwl	(%r8), %eax
	addq	$2, %r8
	cmpw	%ax, %r10w
	jnb	.L158
	movl	%eax, %r10d
	movzwl	%ax, %ecx
	movl	$10, %edx
	andl	$63488, %r10d
	cmpl	$55296, %r10d
	je	.L159
.L103:
	movq	-104(%rbp), %rax
	movl	%edx, (%rax)
	movq	%r8, %rdx
	movl	%ecx, 84(%r15)
.L107:
	testq	%r11, %r11
	jne	.L114
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L157:
	movl	%r13d, %eax
	movq	%r14, %rcx
	sarl	$4, %eax
	movl	%eax, -116(%rbp)
	movl	%eax, %r12d
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L161:
	subl	$1, %r12d
	je	.L160
.L93:
	movzwl	(%rdx), %eax
	movq	%rdx, %r8
	leaq	32(%rdx), %rdx
	movb	%al, (%rcx)
	movzwl	-30(%rdx), %esi
	movb	%sil, 1(%rcx)
	orl	%esi, %eax
	movzwl	-28(%rdx), %esi
	movb	%sil, 2(%rcx)
	orl	%esi, %eax
	movzwl	-26(%rdx), %esi
	movb	%sil, 3(%rcx)
	orl	%esi, %eax
	movzwl	-24(%rdx), %esi
	movb	%sil, 4(%rcx)
	orl	%esi, %eax
	movzwl	-22(%rdx), %esi
	movb	%sil, 5(%rcx)
	orl	%esi, %eax
	movzwl	-20(%rdx), %esi
	movb	%sil, 6(%rcx)
	orl	%esi, %eax
	movzwl	-18(%rdx), %esi
	movb	%sil, 7(%rcx)
	orl	%esi, %eax
	movzwl	-16(%rdx), %esi
	movb	%sil, 8(%rcx)
	orl	%esi, %eax
	movzwl	-14(%rdx), %esi
	movb	%sil, 9(%rcx)
	orl	%esi, %eax
	movzwl	-12(%rdx), %esi
	movb	%sil, 10(%rcx)
	orl	%esi, %eax
	movzwl	-10(%rdx), %esi
	movb	%sil, 11(%rcx)
	orl	%esi, %eax
	movzwl	-8(%rdx), %esi
	movb	%sil, 12(%rcx)
	orl	%esi, %eax
	movzwl	-6(%rdx), %esi
	movb	%sil, 13(%rcx)
	orl	%esi, %eax
	movzwl	-4(%rdx), %esi
	movb	%sil, 14(%rcx)
	movzwl	-2(%rdx), %ebx
	orl	%esi, %eax
	movq	%rcx, %rsi
	leaq	16(%rcx), %rcx
	orl	%ebx, %eax
	movb	%bl, -1(%rcx)
	cmpw	%ax, %r10w
	jnb	.L161
	subl	%r12d, -116(%rbp)
	movl	-116(%rbp), %ebx
	movl	%ebx, %eax
	sall	$4, %eax
	subl	%eax, %r13d
	testq	%r11, %r11
	je	.L95
	cltq
	addq	%rax, %r14
	testl	%ebx, %ebx
	jle	.L96
.L115:
	movl	-116(%rbp), %eax
	movl	$1, %edx
	testl	%eax, %eax
	cmovg	%eax, %edx
	cmpl	$3, %eax
	jle	.L120
	movl	%edx, %ecx
	movdqa	.LC9(%rip), %xmm4
	movdqa	.LC5(%rip), %xmm1
	movq	%r11, %rax
	shrl	$2, %ecx
	salq	$8, %rcx
	movaps	%xmm4, -96(%rbp)
	addq	%r11, %rcx
	movaps	%xmm1, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L99:
	movdqa	-96(%rbp), %xmm7
	addq	$256, %rax
	movdqa	.LC18(%rip), %xmm1
	movdqa	.LC2(%rip), %xmm5
	movdqa	.LC11(%rip), %xmm15
	movdqa	%xmm7, %xmm0
	paddd	.LC10(%rip), %xmm7
	movdqa	.LC3(%rip), %xmm2
	paddd	%xmm0, %xmm1
	movdqa	-144(%rbp), %xmm6
	paddd	%xmm0, %xmm5
	movdqa	.LC4(%rip), %xmm12
	movdqa	.LC6(%rip), %xmm4
	movaps	%xmm7, -96(%rbp)
	movdqa	.LC7(%rip), %xmm3
	paddd	%xmm0, %xmm15
	movdqa	.LC8(%rip), %xmm7
	movaps	%xmm1, -64(%rbp)
	movdqa	.LC12(%rip), %xmm14
	movdqa	%xmm0, %xmm1
	movdqa	.LC13(%rip), %xmm13
	paddd	%xmm0, %xmm2
	movdqa	.LC14(%rip), %xmm11
	paddd	%xmm0, %xmm12
	paddd	%xmm0, %xmm7
	paddd	%xmm0, %xmm14
	punpckldq	%xmm15, %xmm1
	movdqa	.LC15(%rip), %xmm10
	movdqa	.LC16(%rip), %xmm9
	paddd	%xmm0, %xmm13
	paddd	%xmm0, %xmm11
	movdqa	.LC17(%rip), %xmm8
	paddd	%xmm0, %xmm10
	paddd	%xmm0, %xmm6
	paddd	%xmm0, %xmm9
	paddd	%xmm0, %xmm8
	paddd	%xmm0, %xmm4
	paddd	%xmm0, %xmm3
	punpckhdq	%xmm15, %xmm0
	movdqa	%xmm5, %xmm15
	punpckhdq	%xmm14, %xmm5
	punpckldq	%xmm14, %xmm15
	movdqa	%xmm2, %xmm14
	punpckhdq	%xmm13, %xmm2
	punpckldq	%xmm13, %xmm14
	movdqa	%xmm12, %xmm13
	punpckhdq	%xmm11, %xmm12
	punpckldq	%xmm11, %xmm13
	movdqa	%xmm6, %xmm11
	movaps	%xmm12, -80(%rbp)
	punpckhdq	%xmm10, %xmm6
	punpckldq	%xmm10, %xmm11
	movdqa	%xmm7, %xmm12
	movdqa	%xmm4, %xmm10
	punpckldq	-64(%rbp), %xmm12
	punpckldq	%xmm9, %xmm10
	punpckhdq	%xmm9, %xmm4
	movdqa	%xmm3, %xmm9
	punpckhdq	%xmm8, %xmm3
	punpckldq	%xmm8, %xmm9
	movdqa	%xmm12, %xmm8
	movdqa	%xmm7, %xmm12
	movdqa	%xmm1, %xmm7
	punpckhdq	%xmm11, %xmm1
	punpckhdq	-64(%rbp), %xmm12
	punpckldq	%xmm11, %xmm7
	movdqa	%xmm0, %xmm11
	punpckhdq	%xmm6, %xmm0
	punpckldq	%xmm6, %xmm11
	movdqa	%xmm15, %xmm6
	punpckhdq	%xmm10, %xmm15
	punpckldq	%xmm10, %xmm6
	movdqa	%xmm5, %xmm10
	punpckhdq	%xmm4, %xmm5
	movaps	%xmm5, -64(%rbp)
	movdqa	%xmm2, %xmm5
	punpckhdq	%xmm3, %xmm2
	punpckldq	%xmm4, %xmm10
	punpckldq	%xmm3, %xmm5
	movdqa	%xmm13, %xmm3
	punpckhdq	%xmm8, %xmm13
	punpckldq	%xmm8, %xmm3
	movdqa	-80(%rbp), %xmm8
	movdqa	%xmm14, %xmm4
	punpckhdq	%xmm9, %xmm14
	punpckldq	%xmm9, %xmm4
	movdqa	%xmm12, %xmm9
	punpckldq	%xmm12, %xmm8
	movdqa	-80(%rbp), %xmm12
	punpckhdq	%xmm9, %xmm12
	movdqa	%xmm7, %xmm9
	punpckhdq	%xmm4, %xmm7
	punpckldq	%xmm4, %xmm9
	movdqa	%xmm1, %xmm4
	punpckhdq	%xmm14, %xmm1
	punpckldq	%xmm14, %xmm4
	movdqa	%xmm11, %xmm14
	punpckhdq	%xmm5, %xmm11
	punpckldq	%xmm5, %xmm14
	movdqa	%xmm0, %xmm5
	punpckhdq	%xmm2, %xmm0
	punpckldq	%xmm2, %xmm5
	movdqa	%xmm0, %xmm2
	movdqa	%xmm6, %xmm0
	punpckldq	%xmm3, %xmm0
	punpckhdq	%xmm3, %xmm6
	movdqa	%xmm15, %xmm3
	punpckldq	%xmm13, %xmm3
	punpckhdq	%xmm13, %xmm15
	movdqa	-64(%rbp), %xmm13
	movaps	%xmm3, -80(%rbp)
	movdqa	%xmm10, %xmm3
	punpckhdq	%xmm8, %xmm10
	punpckldq	%xmm8, %xmm3
	movdqa	%xmm13, %xmm8
	punpckhdq	%xmm12, %xmm13
	punpckldq	%xmm12, %xmm8
	movdqa	%xmm13, %xmm12
	movdqa	%xmm9, %xmm13
	punpckldq	%xmm0, %xmm13
	punpckhdq	%xmm0, %xmm9
	movdqa	%xmm7, %xmm0
	punpckldq	%xmm6, %xmm0
	punpckhdq	%xmm6, %xmm7
	movdqa	-80(%rbp), %xmm6
	movups	%xmm13, -256(%rax)
	movups	%xmm0, -224(%rax)
	movdqa	%xmm4, %xmm0
	punpckldq	%xmm6, %xmm0
	movups	%xmm9, -240(%rax)
	punpckhdq	%xmm6, %xmm4
	movups	%xmm7, -208(%rax)
	movups	%xmm0, -192(%rax)
	movdqa	%xmm1, %xmm0
	punpckhdq	%xmm15, %xmm1
	punpckldq	%xmm15, %xmm0
	movups	%xmm4, -176(%rax)
	movups	%xmm0, -160(%rax)
	movdqa	%xmm14, %xmm0
	punpckhdq	%xmm3, %xmm14
	punpckldq	%xmm3, %xmm0
	movups	%xmm1, -144(%rax)
	movups	%xmm0, -128(%rax)
	movdqa	%xmm11, %xmm0
	punpckhdq	%xmm10, %xmm11
	punpckldq	%xmm10, %xmm0
	movups	%xmm14, -112(%rax)
	movups	%xmm0, -96(%rax)
	movdqa	%xmm5, %xmm0
	punpckhdq	%xmm8, %xmm5
	punpckldq	%xmm8, %xmm0
	movups	%xmm11, -80(%rax)
	movups	%xmm0, -64(%rax)
	movdqa	%xmm2, %xmm0
	punpckhdq	%xmm12, %xmm2
	punpckldq	%xmm12, %xmm0
	movups	%xmm5, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm2, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L99
	movl	%edx, %ecx
	movl	-116(%rbp), %ebx
	andl	$-4, %ecx
	movl	%ecx, %eax
	movl	%ecx, %r9d
	subl	%ecx, %ebx
	salq	$6, %rax
	sall	$4, %r9d
	addq	%r11, %rax
	cmpl	%ecx, %edx
	je	.L100
.L97:
	leal	1(%r9), %edx
	movl	%r9d, (%rax)
	leal	16(%r9), %ecx
	movl	%edx, 4(%rax)
	leal	2(%r9), %edx
	movl	%edx, 8(%rax)
	leal	3(%r9), %edx
	movl	%edx, 12(%rax)
	leal	4(%r9), %edx
	movl	%edx, 16(%rax)
	leal	5(%r9), %edx
	movl	%edx, 20(%rax)
	leal	6(%r9), %edx
	movl	%edx, 24(%rax)
	leal	7(%r9), %edx
	movl	%edx, 28(%rax)
	leal	8(%r9), %edx
	movl	%edx, 32(%rax)
	leal	9(%r9), %edx
	movl	%edx, 36(%rax)
	leal	10(%r9), %edx
	movl	%edx, 40(%rax)
	leal	11(%r9), %edx
	movl	%edx, 44(%rax)
	leal	12(%r9), %edx
	movl	%edx, 48(%rax)
	leal	13(%r9), %edx
	movl	%edx, 52(%rax)
	leal	14(%r9), %edx
	movl	%edx, 56(%rax)
	leal	15(%r9), %edx
	movl	%edx, 60(%rax)
	cmpl	$1, %ebx
	je	.L100
	leal	17(%r9), %edx
	movl	%ecx, 64(%rax)
	leal	32(%r9), %ecx
	movl	%edx, 68(%rax)
	leal	18(%r9), %edx
	movl	%edx, 72(%rax)
	leal	19(%r9), %edx
	movl	%edx, 76(%rax)
	leal	20(%r9), %edx
	movl	%edx, 80(%rax)
	leal	21(%r9), %edx
	movl	%edx, 84(%rax)
	leal	22(%r9), %edx
	movl	%edx, 88(%rax)
	leal	23(%r9), %edx
	movl	%edx, 92(%rax)
	leal	24(%r9), %edx
	movl	%edx, 96(%rax)
	leal	25(%r9), %edx
	movl	%edx, 100(%rax)
	leal	26(%r9), %edx
	movl	%edx, 104(%rax)
	leal	27(%r9), %edx
	movl	%edx, 108(%rax)
	leal	28(%r9), %edx
	movl	%edx, 112(%rax)
	leal	29(%r9), %edx
	movl	%edx, 116(%rax)
	leal	30(%r9), %edx
	movl	%edx, 120(%rax)
	leal	31(%r9), %edx
	movl	%edx, 124(%rax)
	cmpl	$2, %ebx
	je	.L100
	leal	33(%r9), %edx
	movl	%ecx, 128(%rax)
	movl	%edx, 132(%rax)
	leal	34(%r9), %edx
	movl	%edx, 136(%rax)
	leal	35(%r9), %edx
	movl	%edx, 140(%rax)
	leal	36(%r9), %edx
	movl	%edx, 144(%rax)
	leal	37(%r9), %edx
	movl	%edx, 148(%rax)
	leal	38(%r9), %edx
	movl	%edx, 152(%rax)
	leal	39(%r9), %edx
	movl	%edx, 156(%rax)
	leal	40(%r9), %edx
	movl	%edx, 160(%rax)
	leal	41(%r9), %edx
	movl	%edx, 164(%rax)
	leal	42(%r9), %edx
	movl	%edx, 168(%rax)
	leal	43(%r9), %edx
	movl	%edx, 172(%rax)
	leal	44(%r9), %edx
	movl	%edx, 176(%rax)
	leal	45(%r9), %edx
	movl	%edx, 180(%rax)
	leal	46(%r9), %edx
	addl	$47, %r9d
	movl	%edx, 184(%rax)
	movl	%r9d, 188(%rax)
.L100:
	movl	-116(%rbp), %ebx
	movl	$64, %edx
	leal	-1(%rbx), %eax
	movl	%ebx, %r9d
	cltq
	sall	$4, %r9d
	addq	$1, %rax
	salq	$6, %rax
	testl	%ebx, %ebx
	cmovle	%rdx, %rax
	addq	%rax, %r11
.L96:
	movq	%r8, %rdx
	testl	%r13d, %r13d
	jg	.L117
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r14, %rsi
	movl	$-1, %r9d
	.p2align 4,,10
	.p2align 3
.L90:
	testq	%r11, %r11
	je	.L108
.L114:
	movq	%rsi, %r8
	subq	%r14, %r8
	je	.L108
	leaq	-1(%r8), %rax
	cmpq	$2, %rax
	jbe	.L125
	movq	%r8, %rcx
	movd	%r9d, %xmm2
	movq	%r11, %rax
	shrq	$2, %rcx
	pshufd	$0, %xmm2, %xmm0
	movdqa	.LC5(%rip), %xmm2
	paddd	.LC19(%rip), %xmm0
	salq	$4, %rcx
	addq	%r11, %rcx
	.p2align 4,,10
	.p2align 3
.L111:
	movdqa	%xmm0, %xmm1
	addq	$16, %rax
	paddd	%xmm2, %xmm0
	movups	%xmm1, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L111
	movq	%r8, %rax
	movq	%r8, %r10
	andq	$-4, %rax
	leaq	(%r11,%rax,4), %rcx
	addl	%eax, %r9d
	subq	%rax, %r10
	cmpq	%r8, %rax
	je	.L112
.L109:
	movl	%r9d, (%rcx)
	leal	1(%r9), %eax
	cmpq	$1, %r10
	je	.L112
	movl	%eax, 4(%rcx)
	addl	$2, %r9d
	cmpq	$2, %r10
	je	.L112
	movl	%r9d, 8(%rcx)
.L112:
	leaq	(%r11,%r8,4), %r11
.L108:
	movq	-104(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L113
	cmpq	%rdx, -112(%rbp)
	ja	.L162
.L113:
	movq	%rdx, 16(%rdi)
	movq	%rsi, 32(%rdi)
	movq	%r11, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	cmpq	%rsi, 40(%rdi)
	ja	.L113
	movl	$15, (%rax)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L105:
	sall	$10, %r9d
	leaq	2(%rdx), %r8
	leal	-56613888(%rcx,%r9), %ecx
	movl	%ecx, %esi
	andl	$-2048, %esi
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L158:
	addq	$1, %rsi
	movb	%al, -1(%rsi)
	cmpq	%rdx, %r8
	jne	.L102
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L160:
	movl	-116(%rbp), %eax
	movq	%rcx, %rsi
	movq	%rdx, %r8
	sall	$4, %eax
	subl	%eax, %r13d
	testq	%r11, %r11
	je	.L95
	cltq
	movq	%rcx, %rsi
	movq	%rdx, %r8
	addq	%rax, %r14
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L104:
	movl	%r9d, 84(%r15)
	movq	%r14, %rsi
	movl	%r10d, %r9d
	movq	%rax, %r14
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L159:
	testb	$4, %ah
	jne	.L123
	movl	%r9d, %r10d
	movq	%r14, %rax
	movl	%ecx, %r9d
	movq	%rsi, %r14
	movq	%r8, %rdx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%r11d, %r11d
	movq	%r8, %rdx
	testl	%r13d, %r13d
	jg	.L117
	jmp	.L108
.L125:
	movq	%r8, %r10
	movq	%r11, %rcx
	jmp	.L109
.L120:
	movl	%eax, %ebx
	movq	%r11, %rax
	jmp	.L97
	.cfi_endproc
.LFE2030:
	.size	_Latin1FromUnicodeWithOffsets, .-_Latin1FromUnicodeWithOffsets
	.p2align 4
	.type	ucnv_Latin1FromUTF8, @function
ucnv_Latin1FromUTF8:
.LFB2031:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r11
	movq	40(%rdi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rsi), %rcx
	subq	%r11, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	8(%rsi), %r13
	movl	%eax, %r9d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpb	$0, 64(%r13)
	movq	24(%rsi), %rbx
	jle	.L164
	movl	72(%r13), %r8d
	testl	%r8d, %r8d
	je	.L164
	cmpq	%rbx, %rcx
	jnb	.L168
	testl	%eax, %eax
	je	.L191
	leal	-194(%r8), %r9d
	cmpl	$1, %r9d
	ja	.L190
	movzbl	(%rcx), %r10d
	leal	-128(%r10), %r9d
	cmpb	$63, %r9b
	ja	.L190
	sall	$6, %r8d
	addq	$1, %rcx
	addq	$1, %r11
	orl	%r9d, %r8d
	leal	-1(%rax), %r9d
	movb	%r8b, -1(%r11)
	movl	$0, 72(%r13)
	movb	$0, 64(%r13)
.L164:
	cmpq	%rbx, %rcx
	jnb	.L168
	movzbl	-1(%rbx), %eax
	addl	$62, %eax
	cmpb	$50, %al
	jbe	.L192
.L169:
	testl	%r9d, %r9d
	jle	.L170
	movq	%r11, %r8
	leal	-1(%r9,%r11), %r12d
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L193:
	movb	%al, (%r8)
	leaq	1(%r8), %r9
	movl	%r12d, %eax
	addq	$1, %rcx
	movq	%r9, %r11
	subl	%r8d, %eax
.L173:
	cmpq	%rbx, %rcx
	jnb	.L168
	movq	%r9, %r8
	testl	%eax, %eax
	je	.L170
.L171:
	movzbl	(%rcx), %eax
	testb	%al, %al
	jns	.L193
	leal	62(%rax), %r9d
	cmpb	$1, %r9b
	ja	.L175
	movzbl	1(%rcx), %r10d
	addl	$-128, %r10d
	cmpb	$63, %r10b
	ja	.L175
	sall	$6, %eax
	leaq	1(%r8), %r9
	addq	$2, %rcx
	orl	%r10d, %eax
	movq	%r9, %r11
	movb	%al, (%r8)
	movl	%r12d, %eax
	subl	%r8d, %eax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L192:
	subq	$1, %rbx
	cmpq	%rbx, %rcx
	jb	.L169
	.p2align 4,,10
	.p2align 3
.L168:
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L176
	cmpq	%rcx, 24(%rsi)
	jbe	.L176
	movzbl	(%rcx), %edx
	leaq	1(%rcx), %r8
	movb	$1, 64(%r13)
	movl	%edx, %eax
	movb	%dl, 65(%r13)
	movl	%edx, 72(%r13)
	movl	$1, %edx
	testb	%al, %al
	js	.L194
.L177:
	movl	%edx, 76(%r13)
	movq	%r8, %rcx
.L176:
	movq	%rcx, 16(%rsi)
	movq	%r11, 32(%rdi)
.L195:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movq	%rcx, 16(%rsi)
	movq	%r8, 32(%rdi)
.L190:
	movl	$-127, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$15, (%rdx)
	movq	%rcx, 16(%rsi)
	movq	%r11, 32(%rdi)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$15, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L194:
	.cfi_restore_state
	leal	62(%rax), %ecx
	xorl	%edx, %edx
	cmpb	$50, %cl
	ja	.L177
	xorl	%edx, %edx
	cmpb	$-33, %al
	seta	%dl
	cmpb	$-17, %al
	seta	%al
	movzbl	%al, %eax
	leal	2(%rdx,%rax), %edx
	jmp	.L177
	.cfi_endproc
.LFE2031:
	.size	ucnv_Latin1FromUTF8, .-ucnv_Latin1FromUTF8
	.p2align 4
	.type	_Latin1GetUnicodeSet, @function
_Latin1GetUnicodeSet:
.LFB2032:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	movq	16(%rsi), %rcx
	movl	$255, %edx
	xorl	%esi, %esi
	movq	(%rax), %rdi
	jmp	*%rcx
	.cfi_endproc
.LFE2032:
	.size	_Latin1GetUnicodeSet, .-_Latin1GetUnicodeSet
	.p2align 4
	.type	_ASCIIToUnicodeWithOffsets, @function
_ASCIIToUnicodeWithOffsets:
.LFB2033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	32(%rdi), %r14
	movq	40(%rdi), %rdx
	movq	24(%rdi), %rbx
	movq	16(%rdi), %rax
	movq	%rdx, -72(%rbp)
	subq	%r14, %rdx
	movq	48(%rdi), %r15
	sarq	%rdx
	movq	%rbx, -88(%rbp)
	subq	%rax, %rbx
	cmpl	%ebx, %edx
	movq	%r14, -56(%rbp)
	cmovl	%edx, %ebx
	movl	%ebx, -60(%rbp)
	cmpl	$7, %ebx
	jle	.L219
	sarl	$3, %ebx
	movq	%r14, %rdx
	movl	%ebx, -64(%rbp)
	movl	%ebx, %esi
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L254:
	addq	$8, %rax
	addq	$16, %rdx
	subl	$1, %esi
	je	.L253
.L200:
	movzbl	(%rax), %r8d
	movw	%r8w, (%rdx)
	movl	%r8d, %ecx
	movzbl	1(%rax), %r8d
	movw	%r8w, 2(%rdx)
	movl	%r8d, %r13d
	movzbl	2(%rax), %r8d
	orl	%r13d, %ecx
	movw	%r8w, 4(%rdx)
	movl	%r8d, %r12d
	movzbl	3(%rax), %r8d
	orl	%r12d, %ecx
	movw	%r8w, 6(%rdx)
	movl	%r8d, %ebx
	movzbl	4(%rax), %r8d
	orl	%ebx, %ecx
	movw	%r8w, 8(%rdx)
	movl	%r8d, %r11d
	movzbl	5(%rax), %r8d
	orl	%r11d, %ecx
	movw	%r8w, 10(%rdx)
	movl	%r8d, %r10d
	movzbl	6(%rax), %r8d
	orl	%r10d, %ecx
	movw	%r8w, 12(%rdx)
	movl	%r8d, %r9d
	movzbl	7(%rax), %r14d
	orl	%r9d, %ecx
	orb	%r14b, %cl
	movw	%r14w, 14(%rdx)
	jns	.L254
	subl	%esi, -64(%rbp)
	movl	-64(%rbp), %ebx
	xorl	%esi, %esi
	leal	0(,%rbx,8), %ecx
	subl	%ecx, -60(%rbp)
	testq	%r15, %r15
	je	.L198
	movq	-56(%rbp), %r11
	movslq	%ecx, %rcx
	leaq	(%r11,%rcx,2), %rcx
	movq	%rcx, -56(%rbp)
	testl	%ebx, %ebx
	jle	.L198
.L217:
	movl	-64(%rbp), %esi
	movl	$1, %r8d
	testl	%esi, %esi
	cmovg	%esi, %r8d
	cmpl	$3, %esi
	jle	.L222
	movl	%r8d, %esi
	movq	%r15, %rcx
	movdqa	.LC0(%rip), %xmm6
	movdqa	.LC1(%rip), %xmm14
	shrl	$2, %esi
	movdqa	.LC2(%rip), %xmm13
	movdqa	.LC3(%rip), %xmm12
	salq	$7, %rsi
	movdqa	.LC4(%rip), %xmm11
	movdqa	.LC5(%rip), %xmm5
	movdqa	.LC6(%rip), %xmm10
	addq	%r15, %rsi
	.p2align 4,,10
	.p2align 3
.L204:
	movdqa	%xmm6, %xmm0
	movdqa	.LC8(%rip), %xmm7
	subq	$-128, %rcx
	movdqa	.LC7(%rip), %xmm8
	movdqa	%xmm0, %xmm1
	movdqa	%xmm0, %xmm15
	movdqa	%xmm0, %xmm4
	paddd	%xmm5, %xmm15
	paddd	%xmm13, %xmm1
	movdqa	%xmm0, %xmm9
	movdqa	%xmm0, %xmm2
	paddd	%xmm0, %xmm8
	paddd	%xmm0, %xmm7
	paddd	%xmm10, %xmm9
	punpckldq	%xmm15, %xmm2
	paddd	%xmm12, %xmm4
	movdqa	%xmm0, %xmm3
	punpckhdq	%xmm15, %xmm0
	movdqa	%xmm1, %xmm15
	punpckldq	%xmm9, %xmm15
	punpckhdq	%xmm9, %xmm1
	paddd	%xmm11, %xmm3
	movdqa	%xmm4, %xmm9
	punpckhdq	%xmm8, %xmm4
	paddd	%xmm14, %xmm6
	punpckldq	%xmm8, %xmm9
	movdqa	%xmm3, %xmm8
	punpckhdq	%xmm7, %xmm3
	punpckldq	%xmm7, %xmm8
	movdqa	%xmm2, %xmm7
	punpckhdq	%xmm9, %xmm2
	punpckldq	%xmm9, %xmm7
	movdqa	%xmm0, %xmm9
	punpckhdq	%xmm4, %xmm0
	punpckldq	%xmm4, %xmm9
	movdqa	%xmm15, %xmm4
	punpckhdq	%xmm8, %xmm15
	punpckldq	%xmm8, %xmm4
	movdqa	%xmm1, %xmm8
	punpckhdq	%xmm3, %xmm1
	punpckldq	%xmm3, %xmm8
	movdqa	%xmm7, %xmm3
	punpckhdq	%xmm4, %xmm7
	punpckldq	%xmm4, %xmm3
	movups	%xmm7, -112(%rcx)
	movups	%xmm3, -128(%rcx)
	movdqa	%xmm2, %xmm3
	punpckhdq	%xmm15, %xmm2
	movups	%xmm2, -80(%rcx)
	movdqa	%xmm9, %xmm2
	punpckldq	%xmm15, %xmm3
	punpckhdq	%xmm8, %xmm9
	punpckldq	%xmm8, %xmm2
	movups	%xmm3, -96(%rcx)
	movups	%xmm2, -64(%rcx)
	movdqa	%xmm0, %xmm2
	punpckhdq	%xmm1, %xmm0
	punpckldq	%xmm1, %xmm2
	movups	%xmm9, -48(%rcx)
	movups	%xmm2, -32(%rcx)
	movups	%xmm0, -16(%rcx)
	cmpq	%rsi, %rcx
	jne	.L204
	movl	%r8d, %r9d
	movl	-64(%rbp), %r10d
	andl	$-4, %r9d
	movl	%r9d, %ecx
	leal	0(,%r9,8), %esi
	subl	%r9d, %r10d
	salq	$5, %rcx
	addq	%r15, %rcx
	cmpl	%r9d, %r8d
	je	.L205
.L202:
	leal	1(%rsi), %r8d
	movl	%esi, (%rcx)
	leal	8(%rsi), %r9d
	movl	%r8d, 4(%rcx)
	leal	2(%rsi), %r8d
	movl	%r8d, 8(%rcx)
	leal	3(%rsi), %r8d
	movl	%r8d, 12(%rcx)
	leal	4(%rsi), %r8d
	movl	%r8d, 16(%rcx)
	leal	5(%rsi), %r8d
	movl	%r8d, 20(%rcx)
	leal	6(%rsi), %r8d
	movl	%r8d, 24(%rcx)
	leal	7(%rsi), %r8d
	movl	%r8d, 28(%rcx)
	cmpl	$1, %r10d
	je	.L205
	leal	9(%rsi), %r8d
	movl	%r9d, 32(%rcx)
	leal	16(%rsi), %r9d
	movl	%r8d, 36(%rcx)
	leal	10(%rsi), %r8d
	movl	%r8d, 40(%rcx)
	leal	11(%rsi), %r8d
	movl	%r8d, 44(%rcx)
	leal	12(%rsi), %r8d
	movl	%r8d, 48(%rcx)
	leal	13(%rsi), %r8d
	movl	%r8d, 52(%rcx)
	leal	14(%rsi), %r8d
	movl	%r8d, 56(%rcx)
	leal	15(%rsi), %r8d
	movl	%r8d, 60(%rcx)
	cmpl	$2, %r10d
	je	.L205
	leal	17(%rsi), %r8d
	movl	%r9d, 64(%rcx)
	movl	%r8d, 68(%rcx)
	leal	18(%rsi), %r8d
	movl	%r8d, 72(%rcx)
	leal	19(%rsi), %r8d
	movl	%r8d, 76(%rcx)
	leal	20(%rsi), %r8d
	movl	%r8d, 80(%rcx)
	leal	21(%rsi), %r8d
	movl	%r8d, 84(%rcx)
	leal	22(%rsi), %r8d
	addl	$23, %esi
	movl	%r8d, 88(%rcx)
	movl	%esi, 92(%rcx)
.L205:
	movl	-64(%rbp), %ebx
	movl	$32, %r8d
	leal	-1(%rbx), %ecx
	leal	0(,%rbx,8), %esi
	movslq	%ecx, %rcx
	addq	$1, %rcx
	salq	$5, %rcx
	testl	%ebx, %ebx
	movl	-60(%rbp), %ebx
	cmovle	%r8, %rcx
	addq	%rcx, %r15
	testl	%ebx, %ebx
	jg	.L255
.L223:
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L206:
	cmpq	%rdx, -72(%rbp)
	ja	.L216
	cmpq	-88(%rbp), %r8
	jb	.L256
.L216:
	testq	%r15, %r15
	je	.L209
	movq	%rdx, %rcx
	subq	-56(%rbp), %rcx
	sarq	%rcx
	je	.L209
	leaq	-1(%rcx), %rax
	cmpq	$2, %rax
	jbe	.L224
	movq	%rcx, %r9
	movd	%esi, %xmm6
	movdqa	.LC5(%rip), %xmm5
	movq	%r15, %rax
	shrq	$2, %r9
	pshufd	$0, %xmm6, %xmm0
	paddd	.LC19(%rip), %xmm0
	salq	$4, %r9
	addq	%r15, %r9
	.p2align 4,,10
	.p2align 3
.L212:
	movdqa	%xmm0, %xmm1
	addq	$16, %rax
	paddd	%xmm5, %xmm0
	movups	%xmm1, -16(%rax)
	cmpq	%rax, %r9
	jne	.L212
	movq	%rcx, %rax
	movq	%rcx, %r10
	andq	$-4, %rax
	leaq	(%r15,%rax,4), %r9
	addl	%eax, %esi
	subq	%rax, %r10
	cmpq	%rax, %rcx
	je	.L213
.L210:
	movl	%esi, (%r9)
	leal	1(%rsi), %eax
	cmpq	$1, %r10
	je	.L213
	movl	%eax, 4(%r9)
	addl	$2, %esi
	cmpq	$2, %r10
	je	.L213
	movl	%esi, 8(%r9)
.L213:
	leaq	(%r15,%rcx,4), %r15
.L209:
	movq	%r15, 48(%rdi)
	movq	%r8, 16(%rdi)
	movq	%rdx, 32(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	%r14, %rdx
	xorl	%esi, %esi
.L198:
	movl	-60(%rbp), %ebx
	testl	%ebx, %ebx
	jle	.L223
.L255:
	leal	-1(%rbx), %ecx
	leaq	1(%rax,%rcx), %r8
	movzbl	(%rax), %ecx
	addq	$1, %rax
	testb	%cl, %cl
	js	.L207
	.p2align 4,,10
	.p2align 3
.L257:
	addq	$2, %rdx
	movw	%cx, -2(%rdx)
	cmpq	%r8, %rax
	je	.L206
	movzbl	(%rax), %ecx
	addq	$1, %rax
	testb	%cl, %cl
	jns	.L257
.L207:
	movq	8(%rdi), %r8
	movq	-80(%rbp), %rbx
	movb	%cl, 65(%r8)
	movb	$1, 64(%r8)
	movq	%rax, %r8
	movl	$12, (%rbx)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L253:
	movl	-64(%rbp), %ebx
	leal	0(,%rbx,8), %ecx
	subl	%ecx, -60(%rbp)
	testq	%r15, %r15
	je	.L198
	movq	-56(%rbp), %rsi
	movslq	%ecx, %rcx
	leaq	(%rsi,%rcx,2), %rsi
	movq	%rsi, -56(%rbp)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L256:
	movq	-80(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L216
.L224:
	movq	%rcx, %r10
	movq	%r15, %r9
	jmp	.L210
.L222:
	movl	%esi, %r10d
	movq	%r15, %rcx
	xorl	%esi, %esi
	jmp	.L202
	.cfi_endproc
.LFE2033:
	.size	_ASCIIToUnicodeWithOffsets, .-_ASCIIToUnicodeWithOffsets
	.p2align 4
	.type	_ASCIIGetNextUChar, @function
_ASCIIGetNextUChar:
.LFB2034:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	cmpq	%rdx, 24(%rdi)
	jbe	.L259
	movzbl	(%rdx), %eax
	addq	$1, %rdx
	movq	%rdx, 16(%rdi)
	testb	%al, %al
	js	.L262
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$8, (%rsi)
	movl	$65535, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	movq	8(%rdi), %rdx
	movb	%al, 65(%rdx)
	movl	$65535, %eax
	movb	$1, 64(%rdx)
	movl	$12, (%rsi)
	ret
	.cfi_endproc
.LFE2034:
	.size	_ASCIIGetNextUChar, .-_ASCIIGetNextUChar
	.p2align 4
	.type	_ASCIIGetUnicodeSet, @function
_ASCIIGetUnicodeSet:
.LFB2036:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	movq	16(%rsi), %rcx
	movl	$127, %edx
	xorl	%esi, %esi
	movq	(%rax), %rdi
	jmp	*%rcx
	.cfi_endproc
.LFE2036:
	.size	_ASCIIGetUnicodeSet, .-_ASCIIGetUnicodeSet
	.p2align 4
	.type	ucnv_ASCIIFromUTF8, @function
ucnv_ASCIIFromUTF8:
.LFB2035:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	cmpb	$0, 64(%rax)
	jg	.L282
	movq	16(%rsi), %r8
	movq	32(%rdi), %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	subq	%rcx, %r10
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rsi), %rbx
	movq	%rbx, %rax
	subq	%r8, %rax
	cmpl	%eax, %r10d
	cmovge	%eax, %r10d
	cmpl	$15, %r10d
	jg	.L283
.L267:
	testl	%r10d, %r10d
	jle	.L276
	leal	-1(%r10), %eax
	leaq	1(%rcx,%rax), %r9
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L284:
	addq	$1, %rcx
	addq	$1, %r8
	movb	%al, -1(%rcx)
	cmpq	%r9, %rcx
	je	.L271
.L273:
	movzbl	(%r8), %eax
	testb	%al, %al
	jns	.L284
	movl	$-127, (%rdx)
	movq	%rcx, %r9
.L274:
	popq	%rbx
	popq	%r12
	movq	%r8, 16(%rsi)
	popq	%r13
	popq	%r14
	movq	%r9, 32(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movl	%r10d, %r11d
	sarl	$4, %r11d
	movl	%r11d, %r9d
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L285:
	subl	$1, %r9d
	je	.L270
.L269:
	movzbl	(%r8), %eax
	movq	%r8, %r13
	addq	$16, %r8
	movq	%rcx, %r14
	addq	$16, %rcx
	movb	%al, -16(%rcx)
	movzbl	-15(%r8), %r12d
	movb	%r12b, -15(%rcx)
	orl	%r12d, %eax
	movzbl	-14(%r8), %r12d
	movb	%r12b, -14(%rcx)
	orl	%r12d, %eax
	movzbl	-13(%r8), %r12d
	movb	%r12b, -13(%rcx)
	orl	%r12d, %eax
	movzbl	-12(%r8), %r12d
	movb	%r12b, -12(%rcx)
	orl	%r12d, %eax
	movzbl	-11(%r8), %r12d
	movb	%r12b, -11(%rcx)
	orl	%r12d, %eax
	movzbl	-10(%r8), %r12d
	movb	%r12b, -10(%rcx)
	orl	%r12d, %eax
	movzbl	-9(%r8), %r12d
	movb	%r12b, -9(%rcx)
	orl	%r12d, %eax
	movzbl	-8(%r8), %r12d
	movb	%r12b, -8(%rcx)
	orl	%r12d, %eax
	movzbl	-7(%r8), %r12d
	movb	%r12b, -7(%rcx)
	orl	%r12d, %eax
	movzbl	-6(%r8), %r12d
	movb	%r12b, -6(%rcx)
	orl	%r12d, %eax
	movzbl	-5(%r8), %r12d
	movb	%r12b, -5(%rcx)
	orl	%r12d, %eax
	movzbl	-4(%r8), %r12d
	movb	%r12b, -4(%rcx)
	orl	%r12d, %eax
	movzbl	-3(%r8), %r12d
	movb	%r12b, -3(%rcx)
	orl	%r12d, %eax
	movzbl	-2(%r8), %r12d
	movb	%r12b, -2(%rcx)
	orl	%r12d, %eax
	movzbl	-1(%r8), %r12d
	orb	%r12b, %al
	movb	%r12b, -1(%rcx)
	jns	.L285
	subl	%r9d, %r11d
	movq	%r14, %rcx
	movq	%r13, %r8
.L270:
	sall	$4, %r11d
	subl	%r11d, %r10d
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$-127, (%rdx)
	ret
.L276:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%rcx, %r9
	.p2align 4,,10
	.p2align 3
.L271:
	cmpq	%r8, %rbx
	jbe	.L274
	cmpq	40(%rdi), %r9
	jb	.L274
	movl	$15, (%rdx)
	jmp	.L274
	.cfi_endproc
.LFE2035:
	.size	ucnv_ASCIIFromUTF8, .-ucnv_ASCIIFromUTF8
	.globl	_ASCIIData_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ASCIIData_67, @object
	.size	_ASCIIData_67, 296
_ASCIIData_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL16_ASCIIStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL10_ASCIIImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL16_ASCIIStaticData, @object
	.size	_ZL16_ASCIIStaticData, 100
_ZL16_ASCIIStaticData:
	.long	100
	.string	"US-ASCII"
	.zero	51
	.long	367
	.byte	0
	.byte	26
	.byte	1
	.byte	1
	.string	"\032"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10_ASCIIImpl, @object
	.size	_ZL10_ASCIIImpl, 144
_ZL10_ASCIIImpl:
	.long	26
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ASCIIToUnicodeWithOffsets
	.quad	_ASCIIToUnicodeWithOffsets
	.quad	_Latin1FromUnicodeWithOffsets
	.quad	_Latin1FromUnicodeWithOffsets
	.quad	_ASCIIGetNextUChar
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ASCIIGetUnicodeSet
	.quad	0
	.quad	ucnv_ASCIIFromUTF8
	.globl	_Latin1Data_67
	.align 32
	.type	_Latin1Data_67, @object
	.size	_Latin1Data_67, 296
_Latin1Data_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL17_Latin1StaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL11_Latin1Impl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL17_Latin1StaticData, @object
	.size	_ZL17_Latin1StaticData, 100
_ZL17_Latin1StaticData:
	.long	100
	.string	"ISO-8859-1"
	.zero	49
	.long	819
	.byte	0
	.byte	3
	.byte	1
	.byte	1
	.string	"\032"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL11_Latin1Impl, @object
	.size	_ZL11_Latin1Impl, 144
_ZL11_Latin1Impl:
	.long	3
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_Latin1ToUnicodeWithOffsets
	.quad	_Latin1ToUnicodeWithOffsets
	.quad	_Latin1FromUnicodeWithOffsets
	.quad	_Latin1FromUnicodeWithOffsets
	.quad	_Latin1GetNextUChar
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_Latin1GetUnicodeSet
	.quad	0
	.quad	ucnv_Latin1FromUTF8
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	8
	.long	16
	.long	24
	.align 16
.LC1:
	.long	32
	.long	32
	.long	32
	.long	32
	.align 16
.LC2:
	.long	1
	.long	1
	.long	1
	.long	1
	.align 16
.LC3:
	.long	2
	.long	2
	.long	2
	.long	2
	.align 16
.LC4:
	.long	3
	.long	3
	.long	3
	.long	3
	.align 16
.LC5:
	.long	4
	.long	4
	.long	4
	.long	4
	.align 16
.LC6:
	.long	5
	.long	5
	.long	5
	.long	5
	.align 16
.LC7:
	.long	6
	.long	6
	.long	6
	.long	6
	.align 16
.LC8:
	.long	7
	.long	7
	.long	7
	.long	7
	.align 16
.LC9:
	.long	0
	.long	16
	.long	32
	.long	48
	.align 16
.LC10:
	.long	64
	.long	64
	.long	64
	.long	64
	.align 16
.LC11:
	.long	8
	.long	8
	.long	8
	.long	8
	.align 16
.LC12:
	.long	9
	.long	9
	.long	9
	.long	9
	.align 16
.LC13:
	.long	10
	.long	10
	.long	10
	.long	10
	.align 16
.LC14:
	.long	11
	.long	11
	.long	11
	.long	11
	.align 16
.LC15:
	.long	12
	.long	12
	.long	12
	.long	12
	.align 16
.LC16:
	.long	13
	.long	13
	.long	13
	.long	13
	.align 16
.LC17:
	.long	14
	.long	14
	.long	14
	.long	14
	.align 16
.LC18:
	.long	15
	.long	15
	.long	15
	.long	15
	.align 16
.LC19:
	.long	0
	.long	1
	.long	2
	.long	3
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
