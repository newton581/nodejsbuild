	.file	"ucnvscsu.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	_SCSUToUnicodeWithOffsets, @function
_SCSUToUnicodeWithOffsets:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	movq	8(%rdi), %r14
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	movq	32(%rdi), %rdx
	movq	16(%r14), %r11
	movq	40(%rdi), %r10
	movq	48(%rdi), %rcx
	movzbl	64(%r11), %esi
	movzbl	65(%r11), %ebx
	movb	%sil, -56(%rbp)
	movzbl	66(%r11), %esi
	movb	%sil, -44(%rbp)
	movzbl	67(%r11), %esi
	movb	%sil, -43(%rbp)
	movzbl	68(%r11), %esi
	movb	%sil, -42(%rbp)
	testb	%bl, %bl
	jne	.L161
	xorl	%esi, %esi
	cmpb	$0, -56(%rbp)
	je	.L26
.L17:
	cmpq	%r10, %rdx
	jnb	.L72
	cmpq	%r9, %rax
	jnb	.L72
	movsbq	-43(%rbp), %r15
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L155:
	movw	%bx, (%rdx)
	movq	%r12, %rdx
	testq	%rcx, %rcx
	je	.L6
	movl	%esi, (%rcx)
	addq	$4, %rcx
.L6:
	movl	%r8d, %esi
	cmpq	%rax, %r9
	jbe	.L159
	cmpq	%r10, %rdx
	jnb	.L159
.L32:
	movzbl	(%rax), %ebx
	cmpb	$31, %bl
	jbe	.L72
	addq	$1, %rax
	leal	1(%rsi), %r8d
	leaq	2(%rdx), %r12
	testb	%bl, %bl
	jns	.L155
	andl	$127, %ebx
	addl	(%r11,%r15,4), %ebx
	cmpl	$65535, %ebx
	jbe	.L155
	movl	%ebx, %r13d
	andw	$1023, %bx
	shrl	$10, %r13d
	orw	$-9216, %bx
	subw	$10304, %r13w
	movw	%r13w, (%rdx)
	cmpq	%r12, %r10
	jbe	.L8
	movw	%bx, 2(%rdx)
	addq	$4, %rdx
	testq	%rcx, %rcx
	je	.L6
	movl	%esi, (%rcx)
	addq	$8, %rcx
	movl	%esi, -4(%rcx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L49:
	leal	32(%r15), %ebx
	cmpb	$18, %bl
	jbe	.L55
	movb	%r15b, 65(%r14)
	movb	$1, 64(%r14)
	cmpq	%r12, %r9
	jbe	.L97
	movb	%r15b, -42(%rbp)
	movzbl	(%r12), %ebx
	addq	$2, %rax
	leal	2(%r13), %esi
.L51:
	movzwl	-42(%rbp), %r12d
	movq	-56(%rbp), %r15
	addq	$2, %rdx
	sall	$8, %r12d
	orl	%r12d, %ebx
	movw	%bx, (%r15)
	testq	%rcx, %rcx
	je	.L26
	movl	%r8d, (%rcx)
	addq	$4, %rcx
.L26:
	leaq	1(%rax), %r8
	cmpq	%r8, %r9
	jbe	.L102
	movl	%esi, %r8d
	cmpq	%rdx, %r10
	ja	.L45
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L162:
	movzbl	1(%rax), %r12d
	sall	$8, %ebx
	addq	$2, %rdx
	orl	%r12d, %ebx
	movw	%bx, -2(%rdx)
	testq	%rcx, %rcx
	je	.L44
	movl	%esi, (%rcx)
	addq	$4, %rcx
.L44:
	addq	$2, %rax
	leal	2(%r8), %r13d
	movl	%r8d, %esi
	leaq	1(%rax), %rbx
	cmpq	%rbx, %r9
	jbe	.L158
	cmpq	%rdx, %r10
	jbe	.L158
	movl	%r13d, %r8d
.L45:
	movzbl	(%rax), %ebx
	leal	32(%rbx), %r12d
	cmpb	$18, %r12b
	ja	.L162
	movl	%r8d, %r13d
	movl	%esi, %r8d
.L158:
	movq	%rdx, -56(%rbp)
	xorl	%ebx, %ebx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L161:
	cmpb	$0, -56(%rbp)
	jne	.L99
	movq	%rdx, -56(%rbp)
	xorl	%r13d, %r13d
	movl	$-1, %r8d
.L43:
	cmpq	%rdx, %r10
	jbe	.L47
	cmpq	%rax, %r9
	jbe	.L48
.L163:
	leaq	1(%rax), %r12
	leal	1(%r13), %esi
	movzbl	-1(%r12), %r15d
	testb	%bl, %bl
	je	.L49
	cmpb	$1, %bl
	je	.L50
	cmpb	$2, %bl
	je	.L88
	movq	%r12, %rax
	movl	%esi, %r13d
	cmpq	%rax, %r9
	ja	.L163
.L48:
	movb	$0, -56(%rbp)
	movq	%rax, %r12
.L54:
	movq	-64(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L67
.L66:
	testb	%bl, %bl
	jne	.L38
.L10:
	movb	$0, 64(%r14)
	xorl	%ebx, %ebx
.L38:
	movzbl	-56(%rbp), %eax
	movb	%bl, 65(%r11)
	movb	%al, 64(%r11)
	movzbl	-44(%rbp), %eax
	movb	%al, 66(%r11)
	movzbl	-43(%rbp), %eax
	movb	%al, 67(%r11)
	movzbl	-42(%rbp), %eax
	movb	%al, 68(%r11)
	movq	%r12, 16(%rdi)
	movq	%rdx, 32(%rdi)
	movq	%rcx, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	xorl	%esi, %esi
	movl	$-1, %r8d
.L4:
	cmpq	%rdx, %r10
	jbe	.L164
.L60:
	cmpq	%rax, %r9
	jbe	.L13
	leal	1(%rsi), %r15d
	leaq	1(%rax), %r12
	movzbl	-1(%r12), %r13d
	movl	%r15d, -48(%rbp)
	cmpb	$6, %bl
	ja	.L90
	leaq	.L61(%rip), %r15
	movslq	(%r15,%rbx,4), %rbx
	addq	%r15, %rbx
	notrack jmp	*%rbx
	.section	.rodata
	.align 4
	.align 4
.L61:
	.long	.L15-.L61
	.long	.L28-.L61
	.long	.L12-.L61
	.long	.L30-.L61
	.long	.L29-.L61
	.long	.L11-.L61
	.long	.L31-.L61
	.text
	.p2align 4,,10
	.p2align 3
.L55:
	cmpb	$-25, %r15b
	jbe	.L165
	cmpb	$-17, %r15b
	jbe	.L166
	cmpb	$-15, %r15b
	je	.L167
	cmpb	$-16, %r15b
	jne	.L63
	movl	$-4095, %esi
	movw	%si, 64(%r14)
	cmpq	%r12, %r9
	jbe	.L168
	movzbl	(%r12), %r15d
	leal	2(%r13), %esi
	leaq	2(%rax), %r12
.L50:
	movb	%r15b, 66(%r14)
	movb	$2, 64(%r14)
	cmpq	%r12, %r9
	jbe	.L97
	movb	%r15b, -42(%rbp)
	movzbl	(%r12), %ebx
	leaq	1(%r12), %rax
	addl	$1, %esi
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L72:
	movl	%esi, %r8d
.L159:
	xorl	%ebx, %ebx
	jmp	.L4
.L24:
	movl	$2817, %r13d
	movw	%r13w, 64(%r14)
	cmpq	%r12, %r9
	jbe	.L81
	addl	$2, %esi
	movzbl	(%r12), %r13d
	leaq	2(%rax), %r12
	movl	%esi, -48(%rbp)
	.p2align 4,,10
	.p2align 3
.L29:
	movl	%r13d, %eax
	movb	%r13b, 66(%r14)
	shrb	$5, %al
	movb	$2, 64(%r14)
	movb	%al, -43(%rbp)
	movl	%r13d, %eax
	andl	$31, %eax
	movb	%al, -42(%rbp)
	cmpq	%r12, %r9
	jbe	.L169
	addl	$1, -48(%rbp)
	movzbl	(%r12), %r13d
	addq	$1, %r12
.L11:
	movzbl	-42(%rbp), %eax
	sall	$7, %r13d
	movsbq	-43(%rbp), %rsi
	sall	$15, %eax
	orl	%r13d, %eax
	addl	$65536, %eax
	movl	%eax, (%r11,%rsi,4)
	movl	-48(%rbp), %esi
	movq	%r12, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	cmpb	$8, %r13b
	ja	.L22
	leal	-1(%r13), %ebx
	movb	%r13b, 65(%r14)
	movb	%bl, -44(%rbp)
	movb	$1, 64(%r14)
	cmpq	%r12, %r9
	jbe	.L170
	addl	$2, %esi
	movzbl	(%r12), %r13d
	leaq	2(%rax), %r12
	movl	%esi, -48(%rbp)
.L30:
	movsbq	-44(%rbp), %rax
	leaq	2(%rdx), %rbx
	testb	%r13b, %r13b
	jns	.L171
	andl	$127, %r13d
	addl	(%r11,%rax,4), %r13d
	cmpl	$65535, %r13d
	jbe	.L172
	movl	%r13d, %eax
	andw	$1023, %r13w
	shrl	$10, %eax
	orw	$-9216, %r13w
	subw	$10304, %ax
	movw	%ax, (%rdx)
	cmpq	%rbx, %r10
	jbe	.L36
	movw	%r13w, 2(%rdx)
	leaq	4(%rdx), %rax
	testq	%rcx, %rcx
	je	.L85
	movl	%r8d, (%rcx)
	movq	%rax, %rdx
	movl	-48(%rbp), %esi
	addq	$8, %rcx
	movl	%r8d, -4(%rcx)
	movq	%r12, %rax
	jmp	.L17
.L22:
	cmpb	$11, %r13b
	je	.L24
	cmpb	$14, %r13b
	jne	.L173
	movl	$3585, %r15d
	movw	%r15w, 64(%r14)
	cmpq	%r12, %r9
	jbe	.L80
	addl	$2, %esi
	movzbl	(%r12), %r13d
	leaq	2(%rax), %r12
	movl	%esi, -48(%rbp)
	.p2align 4,,10
	.p2align 3
.L28:
	movb	%r13b, 66(%r14)
	movb	$2, 64(%r14)
	cmpq	%r12, %r9
	jbe	.L174
	addl	$1, -48(%rbp)
	movzbl	(%r12), %eax
	addq	$1, %r12
	movb	%r13b, -42(%rbp)
	movzbl	%al, %r13d
.L12:
	movzwl	-42(%rbp), %esi
	leaq	2(%rdx), %rax
	sall	$8, %esi
	orl	%esi, %r13d
	movw	%r13w, (%rdx)
	testq	%rcx, %rcx
	je	.L85
.L156:
	movl	%r8d, (%rcx)
	movq	%rax, %rdx
	movl	-48(%rbp), %esi
	addq	$4, %rcx
	movq	%r12, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$9729, %ebx
	btq	%r13, %rbx
	jc	.L175
	cmpb	$15, %r13b
	jbe	.L18
	cmpb	$23, %r13b
	jbe	.L176
	leal	-24(%r13), %ebx
	movb	%r13b, 65(%r14)
	movb	%bl, -43(%rbp)
	movb	$1, 64(%r14)
	cmpq	%r12, %r9
	jbe	.L177
	addl	$2, %esi
	movzbl	(%r12), %r13d
	leaq	2(%rax), %r12
	movl	%esi, -48(%rbp)
.L31:
	testb	%r13b, %r13b
	je	.L178
	cmpb	$103, %r13b
	jbe	.L179
	leal	-104(%r13), %eax
	cmpb	$63, %al
	jbe	.L180
	cmpb	$-8, %r13b
	jbe	.L42
	movzbl	%r13b, %eax
	leaq	_ZL12fixedOffsets(%rip), %rsi
	movsbq	-43(%rbp), %r8
	subl	$249, %eax
	cltq
	movl	(%rsi,%rax,4), %eax
	movl	-48(%rbp), %esi
	movl	%eax, (%r11,%r8,4)
	movq	%r12, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L88:
	movzbl	%r15b, %ebx
	movq	%r12, %rax
	jmp	.L51
.L179:
	movsbq	-43(%rbp), %rax
	sall	$7, %r13d
	movl	-48(%rbp), %esi
	movl	%r13d, (%r11,%rax,4)
	movq	%r12, %rax
	jmp	.L17
.L172:
	movw	%r13w, (%rdx)
	testq	%rcx, %rcx
	je	.L84
.L157:
	movl	%r8d, (%rcx)
	movl	-48(%rbp), %esi
	addq	$4, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rax
	jmp	.L17
.L166:
	leal	24(%r15), %eax
	movb	%r15b, 65(%r14)
	movl	$6, %ebx
	movb	%al, -43(%rbp)
	movq	%r12, %rax
	movb	$1, 64(%r14)
	movb	$1, -56(%rbp)
	jmp	.L4
.L63:
	movq	-64(%rbp), %rax
	movb	$0, -56(%rbp)
	movl	$12, (%rax)
	movb	%r15b, 65(%r14)
	movb	$1, 64(%r14)
.L27:
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L10
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	$15, %eax
	je	.L66
	xorl	%ebx, %ebx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rdx, -56(%rbp)
	movl	%esi, %r13d
	movl	%esi, %r8d
	xorl	%ebx, %ebx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	_ZL13staticOffsets(%rip), %rsi
	addw	(%rsi,%rax,4), %r13w
	movw	%r13w, (%rdx)
	testq	%rcx, %rcx
	jne	.L157
.L84:
	movl	-48(%rbp), %esi
	movq	%rbx, %rdx
	movq	%r12, %rax
	jmp	.L17
.L175:
	movw	%r13w, (%rdx)
	leaq	2(%rdx), %rax
	testq	%rcx, %rcx
	jne	.L156
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rax, %rdx
	movl	-48(%rbp), %esi
	movq	%r12, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L178:
	movb	$0, 66(%r14)
	movl	$6, %ebx
	movb	$2, 64(%r14)
.L20:
	movq	-64(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L38
	jmp	.L67
.L8:
	testq	%rcx, %rcx
	je	.L9
	movl	%esi, (%rcx)
	addq	$4, %rcx
.L9:
	movq	-64(%rbp), %rdx
	movw	%bx, 144(%r14)
	movb	$1, 93(%r14)
	movl	$15, (%rdx)
	movq	%r12, %rdx
	movq	%rax, %r12
	jmp	.L10
.L36:
	testq	%rcx, %rcx
	je	.L37
	movl	%r8d, (%rcx)
	addq	$4, %rcx
.L37:
	movq	-64(%rbp), %rax
	movw	%r13w, 144(%r14)
	movq	%rbx, %rdx
	movl	$3, %ebx
	movb	$1, 93(%r14)
	movl	$15, (%rax)
	jmp	.L38
.L47:
	cmpq	%rax, %r9
	jbe	.L48
	movq	-64(%rbp), %rsi
	movb	$0, -56(%rbp)
	movq	%rax, %r12
	movl	$15, (%rsi)
	jmp	.L66
.L180:
	movzbl	%r13b, %esi
	movsbq	-43(%rbp), %rax
	sall	$7, %esi
	addl	$44032, %esi
	movl	%esi, (%r11,%rax,4)
	movl	-48(%rbp), %esi
	movq	%r12, %rax
	jmp	.L17
.L42:
	movb	%r13b, 66(%r14)
	movl	$6, %ebx
	movb	$2, 64(%r14)
	jmp	.L20
.L13:
	movq	%rax, %r12
	jmp	.L54
.L164:
	cmpq	%rax, %r9
	jbe	.L13
	movq	-64(%rbp), %rsi
	movq	%rax, %r12
	movl	$15, (%rsi)
	jmp	.L66
.L97:
	movb	%r15b, -42(%rbp)
	movl	$2, %ebx
	movb	$0, -56(%rbp)
	jmp	.L20
.L167:
	movl	$-3839, %ebx
	movb	$1, -56(%rbp)
	movq	%r12, %rax
	movw	%bx, 64(%r14)
	movl	$4, %ebx
	jmp	.L4
.L165:
	movb	%bl, -43(%rbp)
	movq	%r12, %rax
	movb	$1, -56(%rbp)
	jmp	.L17
.L168:
	movb	$0, -56(%rbp)
	movl	$1, %ebx
	jmp	.L20
.L169:
	movl	$5, %ebx
	jmp	.L20
.L174:
	movb	%r13b, -42(%rbp)
	movl	$2, %ebx
	jmp	.L20
.L177:
	movl	$6, %ebx
	jmp	.L20
.L176:
	leal	-16(%r13), %eax
	movl	-48(%rbp), %esi
	movb	%al, -43(%rbp)
	movq	%r12, %rax
	jmp	.L17
.L170:
	movl	$3, %ebx
	jmp	.L20
.L80:
	movl	$1, %ebx
	jmp	.L20
.L173:
	cmpb	$15, %r13b
	je	.L79
	movq	-64(%rbp), %rax
	movl	$12, (%rax)
	movb	%r13b, 65(%r14)
	movb	$1, 64(%r14)
	jmp	.L27
.L81:
	movl	$4, %ebx
	jmp	.L20
.L79:
	movl	-48(%rbp), %esi
	movq	%r12, %rax
	jmp	.L26
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_SCSUToUnicodeWithOffsets.cold, @function
_SCSUToUnicodeWithOffsets.cold:
.LFSB2116:
.L90:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	-48(%rbp), %esi
	movq	%r12, %rax
	jmp	.L60
	.cfi_endproc
.LFE2116:
	.text
	.size	_SCSUToUnicodeWithOffsets, .-_SCSUToUnicodeWithOffsets
	.section	.text.unlikely
	.size	_SCSUToUnicodeWithOffsets.cold, .-_SCSUToUnicodeWithOffsets.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.text.unlikely
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.type	_SCSUToUnicode, @function
_SCSUToUnicode:
.LFB2117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	8(%rdi), %r11
	movq	16(%rdi), %rax
	movq	24(%rdi), %rsi
	movq	32(%rdi), %rdx
	movq	16(%r11), %r9
	movq	40(%rdi), %r8
	movzbl	66(%r9), %ebx
	movzbl	64(%r9), %r12d
	movzbl	65(%r9), %ecx
	movzbl	67(%r9), %r13d
	movb	%bl, -41(%rbp)
	movzbl	68(%r9), %ebx
	testb	%r12b, %r12b
	je	.L182
	testb	%cl, %cl
	jne	.L184
.L183:
	cmpq	%rdx, %r8
	jbe	.L248
	cmpq	%rsi, %rax
	jnb	.L248
	movsbq	%r13b, %r15
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L308:
	movw	%cx, (%rdx)
	movq	%r10, %rdx
.L186:
	cmpq	%rax, %rsi
	jbe	.L248
	cmpq	%rdx, %r8
	jbe	.L248
.L232:
	movzbl	(%rax), %ecx
	cmpb	$31, %cl
	jbe	.L248
	addq	$1, %rax
	leaq	2(%rdx), %r10
	testb	%cl, %cl
	jns	.L308
	andl	$127, %ecx
	addl	(%r9,%r15,4), %ecx
	cmpl	$65535, %ecx
	jbe	.L308
	movl	%ecx, %r14d
	andw	$1023, %cx
	shrl	$10, %r14d
	orw	$-9216, %cx
	subw	$10304, %r14w
	movw	%r14w, (%rdx)
	cmpq	%r10, %r8
	jbe	.L188
	movw	%cx, 2(%rdx)
	addq	$4, %rdx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L182:
	testb	%cl, %cl
	jne	.L218
.L313:
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rsi
	jbe	.L254
.L314:
	cmpq	%rdx, %r8
	jbe	.L254
	movzbl	(%rax), %ecx
	leal	32(%rcx), %r10d
	cmpb	$18, %r10b
	jbe	.L254
	movzbl	1(%rax), %r10d
	sall	$8, %ecx
	addq	$2, %rdx
	addq	$2, %rax
	orl	%r10d, %ecx
	movw	%cx, -2(%rdx)
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rsi
	ja	.L314
.L254:
	xorl	%ecx, %ecx
.L218:
	cmpq	%rdx, %r8
	jbe	.L221
.L226:
	cmpq	%rax, %rsi
	jbe	.L222
	leaq	1(%rax), %r10
	movzbl	-1(%r10), %r12d
	testb	%cl, %cl
	je	.L223
	cmpb	$1, %cl
	je	.L224
	cmpb	$2, %cl
	je	.L255
	movq	%r10, %rax
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L229:
	cmpb	$-25, %r12b
	jbe	.L315
	cmpb	$-17, %r12b
	jbe	.L316
	cmpb	$-15, %r12b
	je	.L317
	cmpb	$-16, %r12b
	jne	.L239
	movl	$-4095, %ecx
	movw	%cx, 64(%r11)
	cmpq	%r10, %rsi
	jbe	.L318
	movzbl	(%r10), %r12d
	leaq	2(%rax), %r10
.L224:
	movb	%r12b, 66(%r11)
	movb	$2, 64(%r11)
	cmpq	%r10, %rsi
	jbe	.L263
	movzbl	(%r10), %ecx
	leaq	1(%r10), %rax
	movl	%r12d, %ebx
.L225:
	movl	%ebx, %r10d
	addq	$2, %rdx
	sall	$8, %r10d
	orl	%r10d, %ecx
	movw	%cx, -2(%rdx)
	jmp	.L313
.L317:
	movl	$-3839, %r12d
	movq	%r10, %rax
	movl	$4, %ecx
	movw	%r12w, 64(%r11)
	movl	$1, %r12d
	.p2align 4,,10
	.p2align 3
.L184:
	cmpq	%r8, %rdx
	jnb	.L319
.L236:
	cmpq	%rax, %rsi
	jbe	.L192
	leaq	1(%rax), %r10
	movzbl	-1(%r10), %r14d
	cmpb	$6, %cl
	ja	.L257
	leaq	.L237(%rip), %r15
	movslq	(%r15,%rcx,4), %rcx
	addq	%rcx, %r15
	notrack jmp	*%r15
	.section	.rodata
	.align 4
	.align 4
.L237:
	.long	.L194-.L237
	.long	.L206-.L237
	.long	.L191-.L237
	.long	.L208-.L237
	.long	.L207-.L237
	.long	.L190-.L237
	.long	.L209-.L237
	.text
	.p2align 4,,10
	.p2align 3
.L223:
	leal	32(%r12), %ecx
	cmpb	$18, %cl
	jbe	.L229
	movb	%r12b, 65(%r11)
	movb	$1, 64(%r11)
	cmpq	%r10, %rsi
	jbe	.L263
	movzbl	(%r10), %ecx
	addq	$2, %rax
	movl	%r12d, %ebx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L248:
	xorl	%ecx, %ecx
	jmp	.L184
.L202:
	movl	$2817, %r14d
	movw	%r14w, 64(%r11)
	cmpq	%r10, %rsi
	jbe	.L253
	movzbl	(%r10), %r14d
	leaq	2(%rax), %r10
	.p2align 4,,10
	.p2align 3
.L207:
	movl	%r14d, %r13d
	movl	%r14d, %ebx
	movb	%r14b, 66(%r11)
	movb	$2, 64(%r11)
	shrb	$5, %r13b
	andl	$31, %ebx
	cmpq	%r10, %rsi
	jbe	.L320
	movzbl	(%r10), %r14d
	addq	$1, %r10
.L190:
	movzbl	%bl, %eax
	sall	$7, %r14d
	movsbq	%r13b, %rcx
	sall	$15, %eax
	orl	%r14d, %eax
	addl	$65536, %eax
	movl	%eax, (%r9,%rcx,4)
	movq	%r10, %rax
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L196:
	cmpb	$8, %r14b
	ja	.L200
	leal	-1(%r14), %ecx
	movb	%r14b, 65(%r11)
	movb	%cl, -41(%rbp)
	movb	$1, 64(%r11)
	cmpq	%r10, %rsi
	jbe	.L321
	movzbl	(%r10), %r14d
	leaq	2(%rax), %r10
.L208:
	movsbq	-41(%rbp), %rcx
	leaq	2(%rdx), %r15
	testb	%r14b, %r14b
	jns	.L322
	andl	$127, %r14d
	addl	(%r9,%rcx,4), %r14d
	cmpl	$65535, %r14d
	jbe	.L323
	movl	%r14d, %eax
	andw	$1023, %r14w
	shrl	$10, %eax
	orw	$-9216, %r14w
	subw	$10304, %ax
	movw	%ax, (%rdx)
	cmpq	%r15, %r8
	jbe	.L212
	movw	%r14w, 2(%rdx)
	movq	%r10, %rax
	addq	$4, %rdx
	jmp	.L183
.L200:
	cmpb	$11, %r14b
	je	.L202
	cmpb	$14, %r14b
	jne	.L324
	movl	$3585, %r15d
	movw	%r15w, 64(%r11)
	cmpq	%r10, %rsi
	jbe	.L252
	movzbl	(%r10), %r14d
	leaq	2(%rax), %r10
	.p2align 4,,10
	.p2align 3
.L206:
	movb	%r14b, 66(%r11)
	movb	$2, 64(%r11)
	cmpq	%r10, %rsi
	jbe	.L325
	movzbl	(%r10), %eax
	movl	%r14d, %ebx
	addq	$1, %r10
	movzbl	%al, %r14d
.L191:
	movl	%ebx, %eax
	sall	$8, %eax
	orl	%eax, %r14d
.L309:
	movw	%r14w, (%rdx)
	movq	%r10, %rax
	addq	$2, %rdx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$9729, %ecx
	btq	%r14, %rcx
	jc	.L309
	cmpb	$15, %r14b
	jbe	.L196
	cmpb	$23, %r14b
	jbe	.L326
	movb	%r14b, 65(%r11)
	leal	-24(%r14), %r13d
	movb	$1, 64(%r11)
	cmpq	%r10, %rsi
	jbe	.L327
	movzbl	(%r10), %r14d
	leaq	2(%rax), %r10
.L209:
	testb	%r14b, %r14b
	je	.L328
	cmpb	$103, %r14b
	jbe	.L329
	leal	-104(%r14), %eax
	cmpb	$63, %al
	jbe	.L330
	cmpb	$-8, %r14b
	jbe	.L217
	movzbl	%r14b, %eax
	leaq	_ZL12fixedOffsets(%rip), %rcx
	movsbq	%r13b, %r15
	subl	$249, %eax
	cltq
	movl	(%rcx,%rax,4), %eax
	movl	%eax, (%r9,%r15,4)
	movq	%r10, %rax
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L255:
	movzbl	%r12b, %ecx
	movq	%r10, %rax
	jmp	.L225
.L329:
	movsbq	%r13b, %rax
	sall	$7, %r14d
	movl	%r14d, (%r9,%rax,4)
	movq	%r10, %rax
	jmp	.L183
.L323:
	movw	%r14w, (%rdx)
	movq	%r10, %rax
	movq	%r15, %rdx
	jmp	.L183
.L221:
	cmpq	%rax, %rsi
	jbe	.L222
	movq	-56(%rbp), %rsi
	movq	%rax, %r10
	xorl	%r12d, %r12d
	movl	$15, (%rsi)
.L242:
	testb	%cl, %cl
	jne	.L213
.L189:
	movb	$0, 64(%r11)
	xorl	%ecx, %ecx
.L213:
	movzbl	-41(%rbp), %eax
	movb	%r12b, 64(%r9)
	movb	%cl, 65(%r9)
	movb	%al, 66(%r9)
	movb	%r13b, 67(%r9)
	movb	%bl, 68(%r9)
	movq	%r10, 16(%rdi)
	movq	%rdx, 32(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L316:
	.cfi_restore_state
	movb	%r12b, 65(%r11)
	leal	24(%r12), %r13d
	movq	%r10, %rax
	movl	$6, %ecx
	movb	$1, 64(%r11)
	movl	$1, %r12d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L322:
	leaq	_ZL13staticOffsets(%rip), %rax
	addw	(%rax,%rcx,4), %r14w
	movq	%r10, %rax
	movw	%r14w, (%rdx)
	movq	%r15, %rdx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L328:
	movb	$0, 66(%r11)
	movl	$6, %ecx
	movb	$2, 64(%r11)
.L198:
	movq	-56(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L213
.L243:
	cmpl	$15, %eax
	je	.L242
	xorl	%ecx, %ecx
	jmp	.L213
.L188:
	movq	-56(%rbp), %rsi
	movw	%cx, 144(%r11)
	movq	%r10, %rdx
	movq	%rax, %r10
	movb	$1, 93(%r11)
	movl	$15, (%rsi)
	jmp	.L189
.L212:
	movq	-56(%rbp), %rax
	movb	$1, 93(%r11)
	movq	%r15, %rdx
	movl	$3, %ecx
	movw	%r14w, 144(%r11)
	movl	$15, (%rax)
	jmp	.L213
.L222:
	movq	%rax, %r10
	xorl	%r12d, %r12d
.L228:
	movq	-56(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L242
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L330:
	movzbl	%r14b, %ecx
	movsbq	%r13b, %rax
	sall	$7, %ecx
	addl	$44032, %ecx
	movl	%ecx, (%r9,%rax,4)
	movq	%r10, %rax
	jmp	.L183
.L217:
	movb	%r14b, 66(%r11)
	movl	$6, %ecx
	movb	$2, 64(%r11)
	jmp	.L198
.L319:
	cmpq	%rsi, %rax
	jnb	.L192
	movq	-56(%rbp), %rsi
	movq	%rax, %r10
	movl	$15, (%rsi)
	jmp	.L242
.L192:
	movq	%rax, %r10
	jmp	.L228
.L263:
	movl	%r12d, %ebx
	movl	$2, %ecx
	xorl	%r12d, %r12d
	jmp	.L198
.L315:
	movl	%ecx, %r13d
	movq	%r10, %rax
	movl	$1, %r12d
	jmp	.L183
.L318:
	movl	$1, %ecx
	xorl	%r12d, %r12d
	jmp	.L198
.L239:
	movq	-56(%rbp), %rax
	movl	$12, (%rax)
	movb	$1, 64(%r11)
	movb	%r12b, 65(%r11)
	xorl	%r12d, %r12d
.L205:
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L189
	xorl	%ecx, %ecx
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L320:
	movl	$5, %ecx
	jmp	.L198
.L325:
	movl	%r14d, %ebx
	movl	$2, %ecx
	jmp	.L198
.L326:
	leal	-16(%r14), %r13d
	movq	%r10, %rax
	jmp	.L183
.L327:
	movl	$6, %ecx
	jmp	.L198
.L321:
	movl	$3, %ecx
	jmp	.L198
.L252:
	movl	$1, %ecx
	jmp	.L198
.L324:
	cmpb	$15, %r14b
	je	.L251
	movq	-56(%rbp), %rax
	movl	$12, (%rax)
	movb	%r14b, 65(%r11)
	movb	$1, 64(%r11)
	jmp	.L205
.L253:
	movl	$4, %ecx
	jmp	.L198
.L251:
	movq	%r10, %rax
	jmp	.L313
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_SCSUToUnicode.cold, @function
_SCSUToUnicode.cold:
.LFSB2117:
.L257:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r10, %rax
	jmp	.L236
	.cfi_endproc
.LFE2117:
	.text
	.size	_SCSUToUnicode, .-_SCSUToUnicode
	.section	.text.unlikely
	.size	_SCSUToUnicode.cold, .-_SCSUToUnicode.cold
.LCOLDE1:
	.text
.LHOTE1:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"SCSU,locale=ja"
.LC3:
	.string	"SCSU"
	.text
	.p2align 4
	.type	_SCSUGetName, @function
_SCSUGetName:
.LFB2125:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	leaq	.LC3(%rip), %rdx
	cmpb	$1, 74(%rax)
	leaq	.LC2(%rip), %rax
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE2125:
	.size	_SCSUGetName, .-_SCSUGetName
	.p2align 4
	.type	_SCSUReset, @function
_SCSUReset:
.LFB2113:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movdqa	_ZL21initialDynamicOffsets(%rip), %xmm0
	cmpl	$1, %esi
	jle	.L335
	movdqa	16+_ZL21initialDynamicOffsets(%rip), %xmm1
.L338:
	movl	$1, %edx
	cmpb	$1, 74(%rax)
	movb	$0, 75(%rax)
	movw	%dx, 72(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm1, 48(%rax)
	je	.L343
	movq	_ZL16initialWindowUse(%rip), %rdx
	movq	%rdx, 76(%rax)
.L340:
	movl	$0, 84(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	movdqa	16+_ZL21initialDynamicOffsets(%rip), %xmm1
	movl	$1, 64(%rax)
	movb	$0, 68(%rax)
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movb	$0, 64(%rdi)
	jne	.L338
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	movq	_ZL19initialWindowUse_ja(%rip), %rdx
	movq	%rdx, 76(%rax)
	jmp	.L340
	.cfi_endproc
.LFE2113:
	.size	_SCSUReset, .-_SCSUReset
	.p2align 4
	.type	_SCSUClose, @function
_SCSUClose:
.LFB2115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L344
	cmpb	$0, 62(%rbx)
	je	.L351
.L346:
	movq	$0, 16(%rbx)
.L344:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	call	uprv_free_67@PLT
	jmp	.L346
	.cfi_endproc
.LFE2115:
	.size	_SCSUClose, .-_SCSUClose
	.p2align 4
	.type	_ZL16getDynamicOffsetjPj, @function
_ZL16getDynamicOffsetjPj:
.LFB2122:
	.cfi_startproc
	leal	-192(%rdi), %eax
	cmpl	$127, %eax
	jbe	.L358
	leal	-592(%rdi), %eax
	cmpl	$127, %eax
	jbe	.L359
	leal	-880(%rdi), %eax
	cmpl	$127, %eax
	jbe	.L360
	leal	-1328(%rdi), %eax
	cmpl	$127, %eax
	jbe	.L361
	leal	-12352(%rdi), %eax
	cmpl	$127, %eax
	jbe	.L362
	leal	-12448(%rdi), %eax
	cmpl	$127, %eax
	jbe	.L363
	leal	-65376(%rdi), %eax
	cmpl	$127, %eax
	jbe	.L372
	cmpl	$127, %edi
	jbe	.L365
	leal	-65536(%rdi), %eax
	cmpl	$16383, %eax
	jbe	.L356
	cmpl	$13311, %edi
	jbe	.L356
	leal	-118784(%rdi), %eax
	cmpl	$12287, %eax
	jbe	.L356
	leal	-57344(%rdi), %eax
	cmpl	$8175, %eax
	ja	.L365
	cmpl	$65279, %edi
	je	.L365
	movl	%edi, %eax
	subl	$44032, %edi
	andl	$2147483520, %eax
	movl	%eax, (%rsi)
	movl	%edi, %eax
	shrl	$7, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	movl	%edi, %eax
	andl	$2147483520, %eax
	movl	%eax, (%rsi)
	movl	%edi, %eax
	shrl	$7, %eax
	ret
.L358:
	xorl	%edi, %edi
	movl	$192, %eax
.L353:
	movl	%eax, (%rsi)
	leal	249(%rdi), %eax
	ret
.L359:
	movl	$1, %edi
	movl	$592, %eax
	jmp	.L353
.L360:
	movl	$2, %edi
	movl	$880, %eax
	jmp	.L353
.L361:
	movl	$3, %edi
	movl	$1328, %eax
	jmp	.L353
.L362:
	movl	$4, %edi
	movl	$12352, %eax
	jmp	.L353
.L363:
	movl	$5, %edi
	movl	$12448, %eax
	jmp	.L353
.L372:
	movl	$6, %edi
	movl	$65376, %eax
	jmp	.L353
.L365:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2122:
	.size	_ZL16getDynamicOffsetjPj, .-_ZL16getDynamicOffsetjPj
	.p2align 4
	.type	_SCSUSafeClone, @function
_SCSUSafeClone:
.LFB2126:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L373
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L377
	movq	16(%rdi), %rdx
	leaq	288(%rsi), %rax
	movdqu	(%rdx), %xmm0
	movups	%xmm0, 288(%rsi)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 304(%rsi)
	movdqu	32(%rdx), %xmm2
	movups	%xmm2, 320(%rsi)
	movdqu	48(%rdx), %xmm3
	movups	%xmm3, 336(%rsi)
	movdqu	64(%rdx), %xmm4
	movups	%xmm4, 352(%rsi)
	movl	80(%rdx), %edx
	movb	$1, 62(%rsi)
	movl	%edx, 368(%rsi)
	movq	%rax, 16(%rsi)
	movq	%rsi, %rax
.L373:
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	movl	$376, (%rdx)
	ret
	.cfi_endproc
.LFE2126:
	.size	_SCSUSafeClone, .-_SCSUSafeClone
	.p2align 4
	.type	_SCSUOpen, @function
_SCSUOpen:
.LFB2114:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rsi)
	je	.L392
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$84, %edi
	subq	$8, %rsp
	movq	32(%rsi), %r13
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L380
	testq	%r13, %r13
	je	.L381
	cmpb	$106, 0(%r13)
	je	.L393
.L381:
	movq	_ZL16initialWindowUse(%rip), %rdx
	movdqa	_ZL21initialDynamicOffsets(%rip), %xmm1
	movb	$0, 68(%rax)
	movdqa	16+_ZL21initialDynamicOffsets(%rip), %xmm0
	movl	$1, 64(%rax)
	movb	$0, 64(%rbx)
	movl	$1, 72(%rax)
	movq	%rdx, 76(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
.L384:
	movl	$0, 84(%rbx)
.L383:
	movl	$-3, %eax
	movb	$-1, 89(%rbx)
	movw	%ax, 136(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	cmpb	$97, 1(%r13)
	jne	.L381
	movzbl	2(%r13), %edx
	testb	%dl, %dl
	je	.L382
	cmpb	$95, %dl
	jne	.L381
.L382:
	movdqa	_ZL21initialDynamicOffsets(%rip), %xmm1
	movdqa	16+_ZL21initialDynamicOffsets(%rip), %xmm0
	movb	$0, 68(%rax)
	movq	_ZL19initialWindowUse_ja(%rip), %rdx
	movl	$1, 64(%rax)
	movb	$0, 64(%rbx)
	movl	$65537, 72(%rax)
	movq	%rdx, 76(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 48(%rax)
	jmp	.L384
.L380:
	movl	$7, (%r12)
	jmp	.L383
	.cfi_endproc
.LFE2114:
	.size	_SCSUOpen, .-_SCSUOpen
	.p2align 4
	.type	_SCSUFromUnicode, @function
_SCSUFromUnicode:
.LFB2124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	32(%rbx), %rax
	movq	16(%rbx), %r8
	movq	%rdi, -96(%rbp)
	movq	%rsi, -104(%rbp)
	movq	24(%rbx), %r15
	movq	%rax, %r10
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	8(%rbx), %rdi
	movq	40(%rbx), %rbx
	movq	16(%rdi), %r14
	movq	%rbx, -72(%rbp)
	movq	%rdi, -88(%rbp)
	movl	-72(%rbp), %r13d
	movzbl	73(%r14), %edx
	movzbl	72(%r14), %ebx
	subl	%eax, %r13d
	movb	%bl, -73(%rbp)
	movl	32(%r14,%rdx,4), %ebx
	movb	%dl, -74(%rbp)
	movl	84(%rdi), %edx
	movl	%ebx, -72(%rbp)
.L395:
	testl	%edx, %edx
	setne	%al
	testl	%r13d, %r13d
	setg	%cl
	andl	%ecx, %eax
	cmpb	$0, -73(%rbp)
	je	.L396
	testb	%al, %al
	je	.L398
.L397:
	movl	%edx, %esi
	cmpq	%r15, %r8
	jnb	.L707
	movzwl	(%r8), %edi
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L518
	movl	%edx, %eax
	addq	$2, %r8
	sall	$10, %eax
	addl	%edi, %eax
	leal	-56613888(%rax), %ecx
	movl	%ecx, %esi
	subl	-72(%rbp), %esi
	cmpl	$127, %esi
	jbe	.L410
	movl	32(%r14), %r9d
	movl	%ecx, %esi
	subl	%r9d, %esi
	cmpl	$127, %esi
	jbe	.L572
	movl	36(%r14), %r9d
	movl	%ecx, %esi
	subl	%r9d, %esi
	cmpl	$127, %esi
	jbe	.L573
	movl	40(%r14), %r9d
	movl	%ecx, %esi
	subl	%r9d, %esi
	cmpl	$127, %esi
	jbe	.L574
	movl	44(%r14), %r9d
	movl	%ecx, %esi
	subl	%r9d, %esi
	cmpl	$127, %esi
	jbe	.L575
	movl	48(%r14), %r9d
	movl	%ecx, %esi
	subl	%r9d, %esi
	cmpl	$127, %esi
	jbe	.L576
	movl	52(%r14), %r9d
	movl	%ecx, %esi
	subl	%r9d, %esi
	cmpl	$127, %esi
	jbe	.L577
	movl	56(%r14), %r9d
	movl	%ecx, %esi
	subl	%r9d, %esi
	cmpl	$127, %esi
	jbe	.L578
	movl	60(%r14), %r9d
	movl	%ecx, %esi
	subl	%r9d, %esi
	cmpl	$127, %esi
	jbe	.L719
	leal	-56614080(%rax), %esi
	cmpl	$127, %esi
	jbe	.L579
	leal	-56614480(%rax), %esi
	cmpl	$127, %esi
	jbe	.L580
	leal	-56614768(%rax), %esi
	cmpl	$127, %esi
	jbe	.L581
	leal	-56615216(%rax), %esi
	cmpl	$127, %esi
	jbe	.L582
	leal	-56626240(%rax), %esi
	cmpl	$127, %esi
	jbe	.L583
	leal	-56626336(%rax), %esi
	cmpl	$127, %esi
	jbe	.L584
	leal	-56679264(%rax), %esi
	cmpl	$127, %esi
	jbe	.L720
	cmpl	$127, %ecx
	jbe	.L427
	leal	-56679424(%rax), %esi
	cmpl	$16383, %esi
	jbe	.L430
	cmpl	$13311, %ecx
	jbe	.L430
	leal	-56732672(%rax), %esi
	cmpl	$12287, %esi
	jbe	.L430
	leal	-56671232(%rax), %esi
	cmpl	$8175, %esi
	ja	.L427
	cmpl	$65279, %ecx
	je	.L427
	movl	%ecx, %edi
	subl	$56657920, %eax
	andl	$2147483520, %edi
	movl	%edi, -72(%rbp)
	movl	%edi, -60(%rbp)
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L396:
	testb	%al, %al
	jne	.L721
	cmpq	%r15, %r8
	jnb	.L714
	testl	%r13d, %r13d
	jle	.L718
	leal	-1(%r13), %eax
	leal	-2(%r13), %esi
	andl	$-2, %eax
	subl	%eax, %esi
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L723:
	movl	%edx, %ebx
	shrl	$8, %ebx
	cmpl	$1, %r13d
	je	.L484
	movb	%bl, (%r10)
	subl	$2, %r13d
	addq	$2, %r10
	movb	%dl, -1(%r10)
	cmpq	%r8, %r15
	jbe	.L571
	cmpl	%esi, %r13d
	je	.L722
.L482:
	movzwl	(%r8), %edx
	addq	$2, %r8
	leal	-13312(%rdx), %ecx
	movl	%edx, %eax
	cmpl	$41983, %ecx
	jbe	.L723
	cmpl	$48895, %ecx
	jbe	.L486
	cmpq	%r8, %r15
	jbe	.L487
	movzwl	(%r8), %ecx
	subl	$13312, %ecx
	cmpl	$41983, %ecx
	ja	.L487
.L632:
	movq	%r10, %r9
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L410:
	orl	$-128, %esi
	subl	$1, %r13d
	addq	$1, %r10
	xorl	%edx, %edx
	movb	%sil, -1(%r10)
.L398:
	cmpq	%r8, %r15
	jbe	.L714
	testl	%r13d, %r13d
	jle	.L718
	leal	0(%r13,%r10), %r12d
	movq	%r10, %rcx
	leal	-1(%r13,%r10), %r13d
	movl	$9729, %eax
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L725:
	movb	%sil, (%rcx)
	leaq	1(%rcx), %rdx
	movl	%r13d, %esi
	movq	%rdx, %r10
	subl	%ecx, %esi
.L402:
	cmpq	%r8, %r15
	jbe	.L571
.L726:
	movq	%rdx, %rcx
	testl	%esi, %esi
	je	.L724
.L400:
	movq	%r8, %rbx
	movzwl	(%r8), %edx
	movl	%r12d, %r10d
	addq	$2, %r8
	subl	%ecx, %r10d
	movq	%rcx, %r9
	leal	-32(%rdx), %edi
	movl	%edx, %esi
	cmpl	$95, %edi
	jbe	.L725
	cmpl	$31, %edx
	ja	.L403
	btq	%rdx, %rax
	jnc	.L404
	movb	%sil, (%rcx)
	leaq	1(%rcx), %rdx
	movl	%r13d, %esi
	movq	%rdx, %r10
	subl	%ecx, %esi
	cmpq	%r8, %r15
	ja	.L726
	.p2align 4,,10
	.p2align 3
.L571:
	movq	%r10, %rax
	xorl	%esi, %esi
.L399:
	movzbl	-73(%rbp), %edi
	movq	-96(%rbp), %rbx
	movb	%dil, 72(%r14)
	movzbl	-74(%rbp), %edi
	movb	%dil, 73(%r14)
	movq	-88(%rbp), %rdi
	movl	%esi, 84(%rdi)
	movq	%r8, 16(%rbx)
	movq	%rax, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L727
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movl	%edx, %edi
	subl	-72(%rbp), %edi
	cmpl	$127, %edi
	ja	.L406
	leaq	1(%rcx), %rdx
	orl	$-128, %edi
	movl	%r13d, %esi
	movb	%dil, (%rcx)
	movq	%rdx, %r10
	subl	%ecx, %esi
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L721:
	movl	%edx, %eax
.L477:
	movl	%edx, %esi
	cmpq	%r15, %r8
	jnb	.L707
	movzwl	(%r8), %r11d
	movl	%r11d, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L518
	movl	%edx, %r9d
	movl	32(%r14), %ecx
	leaq	2(%r8), %r12
	sall	$10, %r9d
	addl	%r11d, %r9d
	leal	-56613888(%r9), %edi
	movl	%edi, %esi
	subl	%ecx, %esi
	cmpl	$127, %esi
	jbe	.L636
	movl	36(%r14), %ecx
	movl	%edi, %esi
	subl	%ecx, %esi
	cmpl	$127, %esi
	jbe	.L637
	movl	40(%r14), %ecx
	movl	%edi, %esi
	subl	%ecx, %esi
	cmpl	$127, %esi
	jbe	.L638
	movl	44(%r14), %ecx
	movl	%edi, %esi
	subl	%ecx, %esi
	cmpl	$127, %esi
	jbe	.L639
	movl	48(%r14), %ecx
	movl	%edi, %esi
	subl	%ecx, %esi
	cmpl	$127, %esi
	jbe	.L640
	movl	52(%r14), %ecx
	movl	%edi, %esi
	subl	%ecx, %esi
	cmpl	$127, %esi
	jbe	.L641
	movl	56(%r14), %ecx
	movl	%edi, %esi
	subl	%ecx, %esi
	cmpl	$127, %esi
	jbe	.L642
	movl	60(%r14), %ecx
	movl	%edi, %esi
	subl	%ecx, %esi
	cmpl	$127, %esi
	jbe	.L728
	cmpq	%r12, %r15
	ja	.L729
.L524:
	sall	$16, %edx
	movq	%r12, %r8
	orl	%r11d, %edx
.L441:
	cmpl	$3, %r13d
	jle	.L730
	movl	%edx, %eax
	leaq	1(%r10), %rcx
	shrl	$24, %eax
	movb	%al, (%r10)
	movl	$4, %eax
.L557:
	movl	%edx, %esi
	leaq	1(%rcx), %r9
	shrl	$16, %esi
	movb	%sil, (%rcx)
.L559:
	rolw	$8, %dx
	leaq	2(%r9), %r10
	subl	%eax, %r13d
	movw	%dx, (%r9)
	xorl	%edx, %edx
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L724:
	movq	%r10, %rax
.L481:
	movq	-104(%rbp), %rdi
	movl	$15, (%rdi)
	jmp	.L399
.L487:
	leal	-48(%rdx), %ecx
	cmpl	$9, %ecx
	jbe	.L488
	movl	%eax, %ecx
	andl	$-33, %ecx
	subl	$65, %ecx
	cmpw	$25, %cx
	ja	.L489
.L488:
	movzbl	-74(%rbp), %eax
	movb	$1, -73(%rbp)
	movq	%r10, %r9
	addl	$224, %eax
	sall	$8, %eax
	orl	%eax, %edx
	jmp	.L405
.L427:
	sall	$16, %edx
	movb	$15, (%r10)
	subl	$1, %r13d
	addq	$1, %r10
	movb	$0, -73(%rbp)
	orl	%edi, %edx
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L722:
	movq	%r10, %rax
	xorl	%esi, %esi
	jmp	.L481
.L486:
	cmpl	$57343, %edx
	jbe	.L731
	orl	$15728640, %edx
	movq	%r10, %rcx
.L445:
	cmpl	$2, %r13d
	jle	.L732
	movl	$3, %eax
	jmp	.L557
.L406:
	movl	%edx, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L733
	cmpl	$159, %edx
	jbe	.L734
	cmpl	$65279, %edx
	je	.L474
	cmpl	$65519, %edx
	ja	.L474
	movl	%edx, %eax
	subl	32(%r14), %eax
	cmpl	$127, %eax
	jbe	.L592
	movl	%edx, %eax
	subl	36(%r14), %eax
	cmpl	$127, %eax
	jbe	.L593
	movl	%edx, %eax
	subl	40(%r14), %eax
	cmpl	$127, %eax
	jbe	.L594
	movl	%edx, %eax
	subl	44(%r14), %eax
	cmpl	$127, %eax
	jbe	.L595
	movl	%edx, %eax
	subl	48(%r14), %eax
	cmpl	$127, %eax
	jbe	.L596
	movl	%edx, %eax
	subl	52(%r14), %eax
	cmpl	$127, %eax
	jbe	.L597
	movl	%edx, %eax
	subl	56(%r14), %eax
	cmpl	$127, %eax
	jbe	.L598
	movl	%edx, %eax
	subl	60(%r14), %eax
	cmpl	$127, %eax
	jbe	.L735
	leal	-128(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L599
	leal	-256(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L600
	leal	-768(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L601
	leal	-8192(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L602
	leal	-8320(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L603
	leal	-8448(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L604
	leal	-12288(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L736
	leaq	-60(%rbp), %rsi
	movl	%edx, %edi
	call	_ZL16getDynamicOffsetjPj
	movl	%eax, %r9d
	testl	%eax, %eax
	jns	.L737
	leal	-13312(%rdx), %eax
	cmpl	$41983, %eax
	ja	.L474
	cmpq	%r8, %r15
	jbe	.L475
	movzwl	2(%rbx), %eax
	subl	$13312, %eax
	cmpl	$41983, %eax
	ja	.L474
.L475:
	movb	$0, -73(%rbp)
	orl	$983040, %edx
	movl	%r10d, %r13d
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L404:
	orw	$256, %si
	movl	%r10d, %r13d
	movzwl	%si, %edx
	.p2align 4,,10
	.p2align 3
.L405:
	cmpl	$1, %r13d
	jle	.L738
	movl	$2, %eax
	jmp	.L559
.L430:
	movl	%ecx, %edi
	movl	%ecx, %eax
	andl	$2147483520, %edi
	movl	%edi, -72(%rbp)
	movl	%edi, -60(%rbp)
.L716:
	shrl	$7, %eax
	subl	%edi, %ecx
	subl	$512, %eax
	movl	%ecx, %esi
	sall	$8, %eax
.L429:
	movsbq	75(%r14), %rcx
	movzbl	76(%r14,%rcx), %r11d
	leal	1(%rcx), %ebx
	cmpb	$7, %cl
	je	.L563
	movb	%bl, 75(%r14)
	movsbl	%bl, %ebx
.L564:
	movl	-72(%rbp), %edi
	movzbl	%r11b, %edx
	movb	%r11b, -74(%rbp)
	movzbl	%r11b, %r12d
	movl	%edi, 32(%r14,%rdx,4)
	movl	%ebx, %edx
	movl	%edx, %ecx
	subl	$1, %ecx
	js	.L739
	.p2align 4,,10
	.p2align 3
.L432:
	movslq	%ecx, %rdi
	cmpb	76(%r14,%rdi), %r11b
	je	.L435
	movl	%ecx, %edx
.L740:
	movl	%edx, %ecx
	subl	$1, %ecx
	jns	.L432
.L739:
	cmpb	83(%r14), %r11b
	je	.L589
	movl	$7, %ecx
	movl	%ecx, %edx
	jmp	.L740
.L731:
	testb	$4, %dh
	je	.L477
	movq	-104(%rbp), %rdi
	movq	%r10, %rax
	movl	%edx, %esi
	movl	$12, (%rdi)
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L435:
	cmpl	$8, %edx
	je	.L589
.L433:
	cmpl	%ebx, %edx
	jne	.L440
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L438:
	cmpl	%ebx, %edi
	je	.L437
.L439:
	movl	%edx, %ecx
	movl	%edi, %edx
.L440:
	movslq	%edx, %r9
	movslq	%ecx, %rcx
	movzbl	76(%r14,%r9), %edi
	movb	%dil, 76(%r14,%rcx)
	leal	1(%rdx), %edi
	cmpl	$7, %edx
	jne	.L438
	testl	%ebx, %ebx
	je	.L591
	xorl	%edi, %edi
	jmp	.L439
.L729:
	movzwl	2(%r8), %ebx
	movw	%bx, -76(%rbp)
	.p2align 4,,10
	.p2align 3
.L523:
	cmpw	%ax, -76(%rbp)
	jne	.L524
	leal	-56614080(%r9), %esi
	cmpl	$127, %esi
	jbe	.L646
	leal	-56614480(%r9), %esi
	cmpl	$127, %esi
	jbe	.L647
	leal	-56614768(%r9), %esi
	cmpl	$127, %esi
	jbe	.L648
	leal	-56615216(%r9), %esi
	cmpl	$127, %esi
	jbe	.L649
	leal	-56626240(%r9), %esi
	cmpl	$127, %esi
	jbe	.L650
	leal	-56626336(%r9), %esi
	cmpl	$127, %esi
	jbe	.L651
	leal	-56679264(%r9), %esi
	cmpl	$127, %esi
	jbe	.L652
	cmpl	$127, %edi
	jbe	.L524
	leal	-56679424(%r9), %eax
	cmpl	$16383, %eax
	jbe	.L537
	cmpl	$13311, %edi
	jbe	.L537
	leal	-56732672(%r9), %eax
	cmpl	$12287, %eax
	jbe	.L537
	leal	-56671232(%r9), %eax
	cmpl	$8175, %eax
	ja	.L524
	cmpl	$65279, %edi
	je	.L524
	movl	%edi, %eax
	leal	-56657920(%r9), %ecx
	andl	$2147483520, %eax
	movl	%eax, -72(%rbp)
	movl	%eax, -60(%rbp)
.L717:
	shrl	$7, %ecx
	subl	%eax, %edi
	subl	$512, %ecx
	movl	%edi, %esi
	sall	$8, %ecx
.L536:
	movsbq	75(%r14), %rdx
	movzbl	76(%r14,%rdx), %r9d
	leal	1(%rdx), %r11d
	cmpb	$7, %dl
	je	.L565
	movb	%r11b, 75(%r14)
	movsbl	%r11b, %r11d
.L566:
	movl	-72(%rbp), %edi
	movb	%r9b, -74(%rbp)
	movzbl	%r9b, %eax
	movzbl	%r9b, %ebx
	movl	%edi, 32(%r14,%rax,4)
	movl	%r11d, %eax
	.p2align 4,,10
	.p2align 3
.L543:
	movl	%eax, %edx
	subl	$1, %edx
	js	.L741
	movslq	%edx, %rdi
	cmpb	76(%r14,%rdi), %r9b
	je	.L542
	movl	%edx, %eax
	jmp	.L543
.L733:
	testb	$4, %dh
	jne	.L408
	movl	%r10d, %r13d
	movq	%rcx, %r10
	jmp	.L397
.L563:
	movb	$0, 75(%r14)
	xorl	%ebx, %ebx
	jmp	.L564
.L542:
	cmpl	$8, %eax
	je	.L653
.L540:
	cmpl	%eax, %r11d
	jne	.L547
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L545:
	cmpl	%r11d, %edi
	je	.L544
.L546:
	movl	%eax, %edx
	movl	%edi, %eax
.L547:
	movslq	%eax, %r8
	movslq	%edx, %rdx
	movzbl	76(%r14,%r8), %edi
	movb	%dil, 76(%r14,%rdx)
	leal	1(%rax), %edi
	cmpl	$7, %eax
	jne	.L545
	testl	%r11d, %r11d
	je	.L655
	xorl	%edi, %edi
	jmp	.L546
.L578:
	movl	%r9d, -72(%rbp)
	movl	$6, %ebx
	movl	$5632, %edx
	movb	$6, -74(%rbp)
	.p2align 4,,10
	.p2align 3
.L415:
	movsbl	75(%r14), %r11d
	movl	%r11d, %eax
	movl	%eax, %ecx
	subl	$1, %ecx
	js	.L742
	.p2align 4,,10
	.p2align 3
.L420:
	movslq	%ecx, %rdi
	cmpb	76(%r14,%rdi), %bl
	je	.L743
	movl	%ecx, %eax
.L744:
	movl	%eax, %ecx
	subl	$1, %ecx
	jns	.L420
.L742:
	cmpb	%bl, 83(%r14)
	je	.L586
	movl	$7, %ecx
	movl	%ecx, %eax
	jmp	.L744
.L743:
	cmpl	$8, %eax
	je	.L586
.L421:
	cmpl	%eax, %r11d
	jne	.L426
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L424:
	cmpl	%edi, %r11d
	je	.L423
.L425:
	movl	%eax, %ecx
	movl	%edi, %eax
.L426:
	movslq	%eax, %r9
	movslq	%ecx, %rcx
	movzbl	76(%r14,%r9), %edi
	movb	%dil, 76(%r14,%rcx)
	leal	1(%rax), %edi
	cmpl	$7, %eax
	jne	.L424
	testl	%r11d, %r11d
	je	.L588
	xorl	%edi, %edi
	jmp	.L425
.L587:
	movslq	%ecx, %r9
	.p2align 4,,10
	.p2align 3
.L423:
	orl	%esi, %edx
	movb	%bl, 76(%r14,%r9)
	movq	%r10, %r9
	orb	$-128, %dl
	jmp	.L405
.L741:
	cmpb	%r9b, 83(%r14)
	je	.L653
	movl	$7, %edx
	movl	%edx, %eax
	jmp	.L543
.L590:
	movslq	%ecx, %r9
.L437:
	movl	%r12d, %edx
	orl	%esi, %eax
	movb	%r11b, 76(%r14,%r9)
	sall	$21, %edx
	orl	%eax, %edx
	orl	$184549504, %edx
	jmp	.L441
.L586:
	xorl	%eax, %eax
	movl	$7, %ecx
	jmp	.L421
.L714:
	movq	%r10, %rax
	movl	%edx, %esi
	jmp	.L399
.L718:
	movq	%r10, %rax
	movl	%edx, %esi
	jmp	.L481
.L588:
	movl	$7, %r9d
	jmp	.L423
.L537:
	movl	%edi, %eax
	movl	%edi, %ecx
	andl	$2147483520, %eax
	movl	%eax, -72(%rbp)
	movl	%eax, -60(%rbp)
	jmp	.L717
.L589:
	xorl	%edx, %edx
	movl	$7, %ecx
	jmp	.L433
.L654:
	movslq	%edx, %r8
.L544:
	sall	$21, %ebx
	orl	%esi, %ecx
	movb	%r9b, 76(%r14,%r8)
	movq	%r12, %r8
	movl	%ebx, %edx
	movb	$1, -73(%rbp)
	orl	%ecx, %edx
	orl	$-251658112, %edx
	jmp	.L441
.L636:
	movb	$0, -105(%rbp)
	xorl	%ebx, %ebx
	movl	$57344, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L519:
	cmpq	%r12, %r15
	jbe	.L521
	movzwl	2(%r8), %r8d
	movw	%r8w, -76(%rbp)
	subl	$13312, %r8d
	cmpl	$41983, %r8d
	jbe	.L523
.L521:
	movsbl	75(%r14), %r9d
	movl	%r9d, %eax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L745
	.p2align 4,,10
	.p2align 3
.L525:
	movslq	%edx, %rdi
	cmpb	%bl, 76(%r14,%rdi)
	je	.L528
	movl	%edx, %eax
.L746:
	movl	%eax, %edx
	subl	$1, %edx
	jns	.L525
.L745:
	cmpb	%bl, 83(%r14)
	je	.L643
	movl	$7, %edx
	movl	%edx, %eax
	jmp	.L746
.L528:
	cmpl	$8, %eax
	je	.L643
.L526:
	cmpl	%r9d, %eax
	jne	.L533
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L531:
	cmpl	%edi, %r9d
	je	.L530
.L532:
	movl	%eax, %edx
	movl	%edi, %eax
.L533:
	movslq	%eax, %r8
	movslq	%edx, %rdx
	movzbl	76(%r14,%r8), %edi
	movb	%dil, 76(%r14,%rdx)
	leal	1(%rax), %edi
	cmpl	$7, %eax
	jne	.L531
	testl	%r9d, %r9d
	je	.L645
	xorl	%edi, %edi
	jmp	.L532
.L644:
	movslq	%edx, %r8
.L530:
	movl	-80(%rbp), %edx
	movzbl	-105(%rbp), %eax
	movb	%bl, 76(%r14,%r8)
	movq	%r10, %r9
	movl	%ecx, -72(%rbp)
	movq	%r12, %r8
	orl	%esi, %edx
	movb	%al, -74(%rbp)
	movb	$1, -73(%rbp)
	orb	$-128, %dl
	jmp	.L405
.L643:
	xorl	%eax, %eax
	movl	$7, %edx
	jmp	.L526
.L645:
	movl	$7, %r8d
	jmp	.L530
.L707:
	movq	%r10, %rax
	jmp	.L399
.L489:
	movl	32(%r14), %esi
	movl	%edx, %ecx
	subl	%esi, %ecx
	cmpl	$127, %ecx
	jbe	.L615
	movl	36(%r14), %esi
	movl	%edx, %ecx
	subl	%esi, %ecx
	cmpl	$127, %ecx
	jbe	.L616
	movl	40(%r14), %esi
	movl	%edx, %ecx
	subl	%esi, %ecx
	cmpl	$127, %ecx
	jbe	.L617
	movl	44(%r14), %esi
	movl	%edx, %ecx
	subl	%esi, %ecx
	cmpl	$127, %ecx
	jbe	.L618
	movl	48(%r14), %esi
	movl	%edx, %ecx
	subl	%esi, %ecx
	cmpl	$127, %ecx
	jbe	.L619
	movl	52(%r14), %esi
	movl	%edx, %ecx
	subl	%esi, %ecx
	cmpl	$127, %ecx
	jbe	.L620
	movl	56(%r14), %esi
	movl	%edx, %ecx
	subl	%esi, %ecx
	cmpl	$127, %ecx
	jbe	.L621
	movl	60(%r14), %esi
	movl	%edx, %ecx
	subl	%esi, %ecx
	cmpl	$127, %ecx
	jbe	.L747
	leal	-192(%rdx), %ecx
	cmpl	$127, %ecx
	jbe	.L622
	leal	-592(%rdx), %ecx
	cmpl	$127, %ecx
	jbe	.L623
	leal	-880(%rdx), %ecx
	cmpl	$127, %ecx
	jbe	.L624
	leal	-1328(%rdx), %ecx
	cmpl	$127, %ecx
	jbe	.L625
	leal	-12352(%rdx), %ecx
	cmpl	$127, %ecx
	jbe	.L626
	leal	-12448(%rdx), %ecx
	cmpl	$127, %ecx
	jbe	.L627
	leal	-65376(%rdx), %ecx
	cmpl	$127, %ecx
	jbe	.L748
	cmpl	$127, %edx
	jbe	.L632
	cmpl	$13311, %edx
	jbe	.L504
	addw	$8192, %ax
	cmpw	$8175, %ax
	ja	.L632
	cmpl	$65279, %edx
	je	.L632
	movl	%edx, %eax
	leal	-44032(%rdx), %ecx
	andl	$2147483520, %eax
	shrl	$7, %ecx
	movl	%eax, -72(%rbp)
	movl	%eax, -60(%rbp)
.L503:
	movsbq	75(%r14), %rsi
	movzbl	76(%r14,%rsi), %r9d
	leal	1(%rsi), %edi
	cmpb	$7, %sil
	je	.L567
	movb	%dil, 75(%r14)
	movsbl	%dil, %edi
.L568:
	movl	-72(%rbp), %ebx
	movb	%r9b, -74(%rbp)
	movzbl	%r9b, %eax
	movzbl	%r9b, %r12d
	movl	%ebx, 32(%r14,%rax,4)
	movl	%edi, %eax
.L511:
	movl	%eax, %esi
	subl	$1, %esi
	js	.L749
	movslq	%esi, %r11
	cmpb	76(%r14,%r11), %r9b
	je	.L510
	movl	%esi, %eax
	jmp	.L511
.L510:
	cmpl	$8, %eax
	je	.L633
.L508:
	cmpl	%edi, %eax
	jne	.L515
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L513:
	cmpl	%edi, %ebx
	je	.L512
.L514:
	movl	%eax, %esi
	movl	%ebx, %eax
.L515:
	movslq	%eax, %r11
	movslq	%esi, %rsi
	movzbl	76(%r14,%r11), %ebx
	movb	%bl, 76(%r14,%rsi)
	leal	1(%rax), %ebx
	cmpl	$7, %eax
	jne	.L513
	testl	%edi, %edi
	je	.L635
	xorl	%ebx, %ebx
	jmp	.L514
.L637:
	movb	$1, -105(%rbp)
	movl	$1, %ebx
	movl	$57600, -80(%rbp)
	jmp	.L519
.L518:
	movq	-104(%rbp), %rdi
	movq	%r10, %rax
	movl	$12, (%rdi)
	jmp	.L399
.L638:
	movb	$2, -105(%rbp)
	movl	$2, %ebx
	movl	$57856, -80(%rbp)
	jmp	.L519
.L474:
	orl	$917504, %edx
	movl	%r10d, %r13d
	jmp	.L445
.L639:
	movb	$3, -105(%rbp)
	movl	$3, %ebx
	movl	$58112, -80(%rbp)
	jmp	.L519
.L640:
	movb	$4, -105(%rbp)
	movl	$4, %ebx
	movl	$58368, -80(%rbp)
	jmp	.L519
.L591:
	movl	$7, %r9d
	jmp	.L437
.L641:
	movb	$5, -105(%rbp)
	movl	$5, %ebx
	movl	$58624, -80(%rbp)
	jmp	.L519
.L572:
	movl	%r9d, -72(%rbp)
	xorl	%ebx, %ebx
	movl	$4096, %edx
	movb	$0, -74(%rbp)
	jmp	.L415
.L734:
	andl	$127, %edx
	movl	%r10d, %r13d
	orb	$2, %dh
	jmp	.L405
.L642:
	movb	$6, -105(%rbp)
	movl	$6, %ebx
	movl	$58880, -80(%rbp)
	jmp	.L519
.L573:
	movl	%r9d, -72(%rbp)
	movl	$1, %ebx
	movl	$4352, %edx
	movb	$1, -74(%rbp)
	jmp	.L415
.L728:
	movb	$7, -105(%rbp)
	movl	$7, %ebx
	movl	$59136, -80(%rbp)
	jmp	.L519
.L574:
	movl	%r9d, -72(%rbp)
	movl	$2, %ebx
	movl	$4608, %edx
	movb	$2, -74(%rbp)
	jmp	.L415
.L575:
	movl	%r9d, -72(%rbp)
	movl	$3, %ebx
	movl	$4864, %edx
	movb	$3, -74(%rbp)
	jmp	.L415
.L749:
	cmpb	%r9b, 83(%r14)
	je	.L633
	movl	$7, %esi
	movl	%esi, %eax
	jmp	.L511
.L576:
	movl	%r9d, -72(%rbp)
	movl	$4, %ebx
	movl	$5120, %edx
	movb	$4, -74(%rbp)
	jmp	.L415
.L577:
	movl	%r9d, -72(%rbp)
	movl	$5, %ebx
	movl	$5376, %edx
	movb	$5, -74(%rbp)
	jmp	.L415
.L719:
	movl	%r9d, -72(%rbp)
	movl	$7, %ebx
	movl	$5888, %edx
	movb	$7, -74(%rbp)
	jmp	.L415
.L653:
	xorl	%eax, %eax
	movl	$7, %edx
	jmp	.L540
.L580:
	movl	$592, -72(%rbp)
	movl	$-67072, %eax
.L418:
	movl	-72(%rbp), %edi
	movl	%edi, -60(%rbp)
	jmp	.L429
.L579:
	movl	$192, -72(%rbp)
	movl	$-67328, %eax
	jmp	.L418
.L581:
	movl	$880, -72(%rbp)
	movl	$-66816, %eax
	jmp	.L418
.L582:
	movl	$1328, -72(%rbp)
	movl	$-66560, %eax
	jmp	.L418
.L584:
	movl	$12448, -72(%rbp)
	movl	$-66048, %eax
	jmp	.L418
.L583:
	movl	$12352, -72(%rbp)
	movl	$-66304, %eax
	jmp	.L418
.L565:
	movb	$0, 75(%r14)
	xorl	%r11d, %r11d
	jmp	.L566
.L655:
	movl	$7, %r8d
	jmp	.L544
.L720:
	movl	$65376, -72(%rbp)
	movl	$-65792, %eax
	jmp	.L418
.L504:
	movl	%edx, %eax
	movl	%edx, %ecx
	andl	$2147483520, %eax
	shrl	$7, %ecx
	movl	%eax, -72(%rbp)
	movl	%eax, -60(%rbp)
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L484:
	movq	-88(%rbp), %rdi
	movq	%r10, %rax
	movl	$1, %r11d
	addq	$104, %rdi
.L550:
	movb	%dl, (%rdi)
.L552:
	movq	-88(%rbp), %rdi
	movb	%r11b, 91(%rdi)
	cmpl	$2, %r13d
	je	.L553
	cmpl	$3, %r13d
	je	.L554
	cmpl	$1, %r13d
	je	.L750
.L556:
	movq	-104(%rbp), %rdi
	xorl	%esi, %esi
	movl	$15, (%rdi)
	jmp	.L399
.L554:
	movl	%ebx, %edx
	addq	$1, %rax
	shrl	$16, %edx
	movb	%dl, -1(%rax)
.L553:
	movb	%bh, (%rax)
	leaq	1(%rax), %rdx
.L555:
	movb	%bl, (%rdx)
	leaq	1(%rdx), %rax
	jmp	.L556
.L750:
	movq	%rax, %rdx
	jmp	.L555
.L634:
	movslq	%esi, %r11
.L512:
	sall	$8, %ecx
	subl	-72(%rbp), %edx
	movb	%r9b, 76(%r14,%r11)
	leal	232(%r12), %eax
	orl	%ecx, %edx
	sall	$16, %eax
	movb	$1, -73(%rbp)
	movq	%r10, %rcx
	orl	%eax, %edx
	orb	$-128, %dl
	jmp	.L445
.L647:
	movl	$592, -72(%rbp)
	movl	$-67072, %ecx
.L534:
	movl	-72(%rbp), %eax
	movl	%eax, -60(%rbp)
	jmp	.L536
.L646:
	movl	$192, -72(%rbp)
	movl	$-67328, %ecx
	jmp	.L534
.L651:
	movl	$12448, -72(%rbp)
	movl	$-66048, %ecx
	jmp	.L534
.L650:
	movl	$12352, -72(%rbp)
	movl	$-66304, %ecx
	jmp	.L534
.L649:
	movl	$1328, -72(%rbp)
	movl	$-66560, %ecx
	jmp	.L534
.L648:
	movl	$880, -72(%rbp)
	movl	$-66816, %ecx
	jmp	.L534
.L652:
	movl	$65376, -72(%rbp)
	movl	$-65792, %ecx
	jmp	.L534
.L624:
	movl	$880, -72(%rbp)
	movl	$2, %ecx
.L493:
	movl	-72(%rbp), %eax
	addl	$249, %ecx
	movl	%eax, -60(%rbp)
	jmp	.L503
.L623:
	movl	$592, -72(%rbp)
	movl	$1, %ecx
	jmp	.L493
.L622:
	movl	$192, -72(%rbp)
	xorl	%ecx, %ecx
	jmp	.L493
.L747:
	movl	%esi, -72(%rbp)
	movl	$59136, %r9d
	movb	$7, -73(%rbp)
	movb	$7, -74(%rbp)
.L490:
	movsbl	75(%r14), %r11d
	movzbl	-73(%rbp), %edi
	movl	%r11d, %eax
.L492:
	movl	%eax, %edx
	subl	$1, %edx
	js	.L751
	movslq	%edx, %rsi
	cmpb	%dil, 76(%r14,%rsi)
	je	.L752
	movl	%edx, %eax
	jmp	.L492
.L752:
	cmpl	$8, %eax
	je	.L629
.L496:
	cmpl	%eax, %r11d
	jne	.L501
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L499:
	cmpl	%esi, %r11d
	je	.L498
.L500:
	movl	%eax, %edx
	movl	%esi, %eax
.L501:
	movslq	%eax, %rdi
	movslq	%edx, %rdx
	movzbl	76(%r14,%rdi), %esi
	movb	%sil, 76(%r14,%rdx)
	leal	1(%rax), %esi
	cmpl	$7, %eax
	jne	.L499
	testl	%r11d, %r11d
	je	.L631
	xorl	%esi, %esi
	jmp	.L500
.L751:
	cmpb	83(%r14), %dil
	je	.L629
	movl	$7, %edx
	movl	%edx, %eax
	jmp	.L492
.L630:
	movslq	%edx, %rdi
.L498:
	movzbl	-73(%rbp), %eax
	movl	%ecx, %edx
	movb	$1, -73(%rbp)
	orl	%r9d, %edx
	movq	%r10, %r9
	movb	%al, 76(%r14,%rdi)
	orb	$-128, %dl
	jmp	.L405
.L629:
	xorl	%eax, %eax
	movl	$7, %edx
	jmp	.L496
.L631:
	movl	$7, %edi
	jmp	.L498
.L621:
	movl	%esi, -72(%rbp)
	movl	$58880, %r9d
	movb	$6, -73(%rbp)
	movb	$6, -74(%rbp)
	jmp	.L490
.L620:
	movl	%esi, -72(%rbp)
	movl	$58624, %r9d
	movb	$5, -73(%rbp)
	movb	$5, -74(%rbp)
	jmp	.L490
.L619:
	movl	%esi, -72(%rbp)
	movl	$58368, %r9d
	movb	$4, -73(%rbp)
	movb	$4, -74(%rbp)
	jmp	.L490
.L618:
	movl	%esi, -72(%rbp)
	movl	$58112, %r9d
	movb	$3, -73(%rbp)
	movb	$3, -74(%rbp)
	jmp	.L490
.L567:
	movb	$0, 75(%r14)
	xorl	%edi, %edi
	jmp	.L568
.L617:
	movl	%esi, -72(%rbp)
	movl	$57856, %r9d
	movb	$2, -73(%rbp)
	movb	$2, -74(%rbp)
	jmp	.L490
.L616:
	movl	%esi, -72(%rbp)
	movl	$57600, %r9d
	movb	$1, -73(%rbp)
	movb	$1, -74(%rbp)
	jmp	.L490
.L615:
	movl	%esi, -72(%rbp)
	movl	$57344, %r9d
	movb	$0, -74(%rbp)
	jmp	.L490
.L635:
	movl	$7, %r11d
	jmp	.L512
.L593:
	movl	$1, %esi
	movl	$512, %ecx
	movl	$4352, %edi
	movl	$1, %eax
.L446:
	movslq	%eax, %r11
	movl	32(%r14,%r11,4), %r12d
	cmpq	%r8, %r15
	jbe	.L448
	movzwl	2(%rbx), %r11d
	leal	127(%r12), %ebx
	cmpl	%ebx, %r11d
	ja	.L452
	cmpl	%r12d, %r11d
	jnb	.L448
	cmpl	$127, %r11d
	ja	.L452
	cmpl	$31, %r11d
	ja	.L448
	movl	$9729, %ebx
	btq	%r11, %rbx
	jnc	.L452
.L448:
	movsbl	75(%r14), %r13d
	movb	%al, -72(%rbp)
	movl	%eax, %ebx
	movl	%r13d, %eax
.L457:
	movl	%eax, %ecx
	subl	$1, %ecx
	js	.L753
	movslq	%ecx, %r11
	cmpb	76(%r14,%r11), %bl
	je	.L456
	movl	%ecx, %eax
	jmp	.L457
.L456:
	cmpl	$8, %eax
	je	.L606
.L454:
	cmpl	%r13d, %eax
	jne	.L461
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L459:
	cmpl	%ebx, %r13d
	je	.L458
.L460:
	movl	%eax, %ecx
	movl	%ebx, %eax
.L461:
	movslq	%eax, %r11
	movslq	%ecx, %rcx
	movzbl	76(%r14,%r11), %ebx
	movb	%bl, 76(%r14,%rcx)
	leal	1(%rax), %ebx
	cmpl	$7, %eax
	jne	.L459
	testl	%r13d, %r13d
	je	.L608
	xorl	%ebx, %ebx
	jmp	.L460
.L753:
	cmpb	83(%r14), %bl
	je	.L606
	movl	$7, %ecx
	movl	%ecx, %eax
	jmp	.L457
.L452:
	subl	%r12d, %edx
	movl	%r10d, %r13d
	orl	%ecx, %edx
	orb	$-128, %dl
	jmp	.L405
.L607:
	movslq	%ecx, %r11
.L458:
	movzbl	-72(%rbp), %eax
	subl	%r12d, %edx
	movl	%r12d, -72(%rbp)
	movl	%r10d, %r13d
	orl	%edi, %edx
	movb	%sil, -74(%rbp)
	movb	%al, 76(%r14,%r11)
	orb	$-128, %dl
	jmp	.L405
.L606:
	xorl	%eax, %eax
	movl	$7, %ecx
	jmp	.L454
.L608:
	movl	$7, %r11d
	jmp	.L458
.L592:
	xorl	%esi, %esi
	movl	$256, %ecx
	movl	$4096, %edi
	xorl	%eax, %eax
	jmp	.L446
.L633:
	xorl	%eax, %eax
	movl	$7, %esi
	jmp	.L508
.L597:
	movl	$5, %esi
	movl	$1536, %ecx
	movl	$5376, %edi
	movl	$5, %eax
	jmp	.L446
.L596:
	movl	$4, %esi
	movl	$1280, %ecx
	movl	$5120, %edi
	movl	$4, %eax
	jmp	.L446
.L595:
	movl	$3, %esi
	movl	$1024, %ecx
	movl	$4864, %edi
	movl	$3, %eax
	jmp	.L446
.L594:
	movl	$2, %esi
	movl	$768, %ecx
	movl	$4608, %edi
	movl	$2, %eax
	jmp	.L446
.L600:
	movl	$2, %eax
.L450:
	movslq	%eax, %rcx
	leaq	_ZL13staticOffsets(%rip), %rdi
	addl	$1, %eax
	movl	%r10d, %r13d
	subl	(%rdi,%rcx,4), %edx
	sall	$8, %eax
	orl	%eax, %edx
	jmp	.L405
.L599:
	movl	$1, %eax
	jmp	.L450
.L735:
	movl	$7, %esi
	movl	$2048, %ecx
	movl	$5888, %edi
	movl	$7, %eax
	jmp	.L446
.L598:
	movl	$6, %esi
	movl	$1792, %ecx
	movl	$5632, %edi
	movl	$6, %eax
	jmp	.L446
.L748:
	movl	$65376, -72(%rbp)
	movl	$6, %ecx
	jmp	.L493
.L627:
	movl	$12448, -72(%rbp)
	movl	$5, %ecx
	jmp	.L493
.L626:
	movl	$12352, -72(%rbp)
	movl	$4, %ecx
	jmp	.L493
.L625:
	movl	$1328, -72(%rbp)
	movl	$3, %ecx
	jmp	.L493
.L604:
	movl	$6, %eax
	jmp	.L450
.L603:
	movl	$5, %eax
	jmp	.L450
.L602:
	movl	$4, %eax
	jmp	.L450
.L601:
	movl	$3, %eax
	jmp	.L450
.L732:
	movl	$3, %edi
	movq	-88(%rbp), %rax
	movq	%rcx, %rsi
	movl	%edx, %ebx
	subl	%r13d, %edi
	leal	0(,%rdi,8), %ecx
	leaq	104(%rax), %r10
	movl	%edi, %r11d
	movq	%rsi, %rax
	shrl	%cl, %ebx
	cmpl	$3, %edi
	je	.L715
.L569:
	cmpl	$1, %edi
	je	.L657
	cmpl	$2, %edi
	jne	.L552
.L551:
	movzbl	%dh, %ecx
	leaq	1(%r10), %rdi
	movb	%cl, (%r10)
	jmp	.L550
.L715:
	movl	$3, %r11d
.L548:
	movl	%edx, %ecx
	addq	$1, %r10
	shrl	$16, %ecx
	movb	%cl, -1(%r10)
	jmp	.L551
.L657:
	movq	%r10, %rdi
	jmp	.L550
.L737:
	movsbq	75(%r14), %rax
	movl	-60(%rbp), %ebx
	movq	%rax, %rsi
	movzbl	76(%r14,%rax), %r11d
	leal	1(%rax), %eax
	movl	%ebx, -72(%rbp)
	cmpb	$7, %sil
	movl	$0, %esi
	cmove	%esi, %eax
	movzbl	%r11b, %edi
	movb	%dil, -74(%rbp)
	movb	%al, 75(%r14)
	movl	%ebx, 32(%r14,%rdi,4)
	movsbl	75(%r14), %ebx
	movl	%ebx, %eax
	jmp	.L469
.L465:
	movslq	%r12d, %rsi
	cmpb	76(%r14,%rsi), %r11b
	je	.L468
.L467:
	movl	%r12d, %eax
.L469:
	movl	%eax, %r12d
	subl	$1, %r12d
	jns	.L465
	cmpb	%r11b, 83(%r14)
	je	.L609
	movl	$7, %r12d
	jmp	.L467
.L736:
	movl	$7, %eax
	jmp	.L450
.L468:
	cmpl	$8, %eax
	je	.L609
.L466:
	cmpl	%eax, %ebx
	jne	.L473
	jmp	.L470
.L471:
	cmpl	%r13d, %ebx
	je	.L611
.L472:
	movslq	%eax, %rsi
	movl	%r13d, %eax
.L473:
	movslq	%eax, %r12
	movzbl	76(%r14,%r12), %r13d
	movb	%r13b, 76(%r14,%rsi)
	leal	1(%rax), %r13d
	cmpl	$7, %eax
	jne	.L471
	testl	%ebx, %ebx
	je	.L610
	xorl	%r13d, %r13d
	jmp	.L472
.L609:
	xorl	%eax, %eax
	movl	$7, %esi
	jmp	.L466
.L611:
	movq	%r12, %rsi
.L470:
	leal	24(%rdi), %eax
	sall	$8, %r9d
	subl	-72(%rbp), %edx
	movb	%r11b, 76(%r14,%rsi)
	sall	$16, %eax
	movl	%r10d, %r13d
	orl	%r9d, %eax
	orl	%eax, %edx
	orb	$-128, %dl
	jmp	.L445
.L730:
	movq	-88(%rbp), %rbx
	movl	$4, %edi
	movq	%r10, %rax
	subl	%r13d, %edi
	leaq	104(%rbx), %r10
	leal	0(,%rdi,8), %ecx
	movl	%edx, %ebx
	movl	%edi, %r11d
	shrl	%cl, %ebx
	cmpl	$3, %edi
	je	.L715
	cmpl	$4, %edi
	jne	.L569
	movq	-88(%rbp), %rdi
	movl	%edx, %ecx
	shrl	$24, %ecx
	movb	%cl, 104(%rdi)
	leaq	105(%rdi), %r10
	jmp	.L548
.L727:
	call	__stack_chk_fail@PLT
.L408:
	movq	-104(%rbp), %rax
	movl	$12, (%rax)
	movq	%rcx, %rax
	jmp	.L399
.L738:
	movl	$2, %edi
	movq	-88(%rbp), %rax
	movl	%edx, %ebx
	subl	%r13d, %edi
	leal	0(,%rdi,8), %ecx
	leaq	104(%rax), %r10
	movl	%edi, %r11d
	movq	%r9, %rax
	shrl	%cl, %ebx
	jmp	.L569
.L610:
	movl	$7, %esi
	jmp	.L470
	.cfi_endproc
.LFE2124:
	.size	_SCSUFromUnicode, .-_SCSUFromUnicode
	.p2align 4
	.type	_SCSUFromUnicodeWithOffsets, @function
_SCSUFromUnicodeWithOffsets:
.LFB2123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r8
	movq	40(%rax), %rcx
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	48(%rax), %r9
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	8(%rdi), %rbx
	movq	24(%rdi), %rdi
	movq	%rcx, -80(%rbp)
	movl	-80(%rbp), %r14d
	xorl	%ecx, %ecx
	movq	16(%rbx), %r12
	movl	84(%rbx), %edx
	movq	%rdi, -72(%rbp)
	movq	32(%rax), %rdi
	movq	%rbx, -96(%rbp)
	movzbl	72(%r12), %eax
	subl	%edi, %r14d
	testl	%edx, %edx
	movq	%rdi, %rbx
	movb	%al, -84(%rbp)
	movzbl	73(%r12), %eax
	setne	%cl
	xorl	%esi, %esi
	negl	%ecx
	movb	%al, -85(%rbp)
	movl	32(%r12,%rax,4), %eax
	movl	%eax, -80(%rbp)
.L919:
	testl	%r14d, %r14d
	setg	%al
	testl	%edx, %edx
	setne	%dil
	andl	%edi, %eax
	cmpb	$0, -84(%rbp)
	je	.L755
	testb	%al, %al
	je	.L778
.L756:
	movl	%edx, %r10d
	cmpq	-72(%rbp), %r8
	jnb	.L1096
	movzwl	(%r8), %eax
	movl	%eax, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L882
	movl	%edx, %r11d
	addq	$2, %r8
	addl	$1, %esi
	sall	$10, %r11d
	addl	%eax, %r11d
	leal	-56613888(%r11), %r13d
	movl	%r13d, %edi
	subl	-80(%rbp), %edi
	cmpl	$127, %edi
	jbe	.L775
	movl	32(%r12), %edi
	movl	%r13d, %r10d
	subl	%edi, %r10d
	cmpl	$127, %r10d
	jbe	.L941
	movl	36(%r12), %edi
	movl	%r13d, %r10d
	subl	%edi, %r10d
	cmpl	$127, %r10d
	jbe	.L942
	movl	40(%r12), %edi
	movl	%r13d, %r10d
	subl	%edi, %r10d
	cmpl	$127, %r10d
	jbe	.L943
	movl	44(%r12), %edi
	movl	%r13d, %r10d
	subl	%edi, %r10d
	cmpl	$127, %r10d
	jbe	.L944
	movl	48(%r12), %edi
	movl	%r13d, %r10d
	subl	%edi, %r10d
	cmpl	$127, %r10d
	jbe	.L945
	movl	52(%r12), %edi
	movl	%r13d, %r10d
	subl	%edi, %r10d
	cmpl	$127, %r10d
	jbe	.L946
	movl	56(%r12), %edi
	movl	%r13d, %r10d
	subl	%edi, %r10d
	cmpl	$127, %r10d
	jbe	.L947
	movl	60(%r12), %edi
	movl	%r13d, %r10d
	subl	%edi, %r10d
	cmpl	$127, %r10d
	jbe	.L1106
	leal	-56614080(%r11), %edi
	cmpl	$127, %edi
	jbe	.L948
	leal	-56614480(%r11), %edi
	cmpl	$127, %edi
	jbe	.L949
	leal	-56614768(%r11), %edi
	cmpl	$127, %edi
	jbe	.L950
	leal	-56615216(%r11), %edi
	cmpl	$127, %edi
	jbe	.L951
	leal	-56626240(%r11), %edi
	cmpl	$127, %edi
	jbe	.L952
	leal	-56626336(%r11), %edi
	cmpl	$127, %edi
	jbe	.L953
	leal	-56679264(%r11), %edi
	cmpl	$127, %edi
	jbe	.L1107
	cmpl	$127, %r13d
	jbe	.L791
	leal	-56679424(%r11), %edi
	cmpl	$16383, %edi
	jbe	.L794
	cmpl	$13311, %r13d
	jbe	.L794
	leal	-56732672(%r11), %edi
	cmpl	$12287, %edi
	jbe	.L794
	leal	-56671232(%r11), %edi
	cmpl	$8175, %edi
	ja	.L791
	cmpl	$65279, %r13d
	je	.L791
	movl	%r13d, %eax
	leal	-56657920(%r11), %edi
	andl	$2147483520, %eax
	movl	%eax, -80(%rbp)
	movl	%eax, -60(%rbp)
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L755:
	testb	%al, %al
	jne	.L1108
	cmpq	-72(%rbp), %r8
	jnb	.L757
	testl	%r14d, %r14d
	jle	.L1104
	leal	-1(%r14), %eax
	leal	-2(%r14), %edi
	movq	-72(%rbp), %r10
	andl	$-2, %eax
	subl	%eax, %edi
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L1110:
	movl	%edx, %esi
	shrl	$8, %esi
	cmpl	$1, %r14d
	je	.L847
	movb	%sil, (%rbx)
	addq	$2, %rbx
	movb	%dl, -1(%rbx)
	testq	%r9, %r9
	je	.L848
	movl	%r15d, (%r9)
	addq	$8, %r9
	movl	%r15d, -4(%r9)
.L848:
	subl	$2, %r14d
	cmpq	%r8, %r10
	jbe	.L1109
	cmpl	%r14d, %edi
	je	.L982
	movl	%ecx, %esi
.L845:
	movzwl	(%r8), %edx
	movl	%ecx, %r15d
	leal	1(%rsi), %ecx
	addq	$2, %r8
	leal	-13312(%rdx), %esi
	movl	%edx, %eax
	cmpl	$41983, %esi
	jbe	.L1110
	cmpl	$48895, %esi
	jbe	.L850
	cmpq	%r8, -72(%rbp)
	jbe	.L851
	movzwl	(%r8), %esi
	subl	$13312, %esi
	cmpl	$41983, %esi
	ja	.L851
.L1002:
	movq	%r9, %r13
	movq	%rbx, %r11
	movl	$2, %eax
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L775:
	orl	$-128, %edi
	leaq	1(%rbx), %rax
	movb	%dil, (%rbx)
	testq	%r9, %r9
	je	.L777
	movl	%ecx, (%r9)
	addq	$4, %r9
.L777:
	subl	$1, %r14d
	movl	%esi, %ecx
	movq	%rax, %rbx
	xorl	%edx, %edx
.L778:
	cmpq	%r8, -72(%rbp)
	jbe	.L757
	testl	%r14d, %r14d
	jle	.L1104
	movq	%r12, -120(%rbp)
	movq	%rbx, %rax
	movq	-72(%rbp), %r12
	addl	%esi, %r14d
	movl	$9729, %edi
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L1111:
	btq	%rdx, %rdi
	jnc	.L767
.L1103:
	leaq	1(%rax), %rdx
	movb	%r13b, (%rax)
	movq	%rdx, %rbx
	testq	%r9, %r9
	je	.L771
.L1100:
	movl	%r15d, (%r9)
	addq	$4, %r9
.L771:
	movl	%r14d, %r10d
	subl	%ecx, %r10d
	cmpq	%r8, %r12
	jbe	.L940
.L1112:
	movq	%rdx, %rax
	testl	%r10d, %r10d
	je	.L1089
	movl	%ecx, %esi
.L762:
	movq	%r8, %rbx
	movzwl	(%r8), %edx
	movl	%r14d, %r10d
	movl	%ecx, %r15d
	subl	%esi, %r10d
	leal	1(%rsi), %ecx
	addq	$2, %r8
	movq	%rax, %r11
	leal	-32(%rdx), %esi
	movl	%edx, %r13d
	cmpl	$95, %esi
	jbe	.L1103
	cmpl	$31, %edx
	jbe	.L1111
	movl	%edx, %esi
	subl	-80(%rbp), %esi
	cmpl	$127, %esi
	ja	.L770
	leaq	1(%rax), %rdx
	orl	$-128, %esi
	movb	%sil, (%rax)
	movq	%rdx, %rbx
	testq	%r9, %r9
	jne	.L1100
	movl	%r14d, %r10d
	subl	%ecx, %r10d
	cmpq	%r8, %r12
	ja	.L1112
	.p2align 4,,10
	.p2align 3
.L940:
	movq	-120(%rbp), %r12
	movq	%rbx, %rdi
	xorl	%r10d, %r10d
.L760:
	movzbl	-84(%rbp), %eax
	movb	%al, 72(%r12)
	movzbl	-85(%rbp), %eax
	movb	%al, 73(%r12)
	movq	-96(%rbp), %rax
	movl	%r10d, 84(%rax)
	movq	-104(%rbp), %rax
	movq	%r8, 16(%rax)
	movq	%rdi, 32(%rax)
	movq	%r9, 48(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1113
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1108:
	.cfi_restore_state
	movq	%r8, -128(%rbp)
	movl	%edx, %eax
	movl	%ecx, %r15d
	movq	%r9, %r13
	movq	%rbx, %r11
.L840:
	movl	%edx, %r10d
	cmpq	-72(%rbp), %r8
	jnb	.L1096
	movzwl	(%r8), %edi
	movl	%edi, %ecx
	movl	%edi, -120(%rbp)
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L882
	leal	1(%rsi), %ecx
	movl	%edx, %esi
	addq	$2, %r8
	sall	$10, %esi
	leal	(%rsi,%rdi), %ebx
	movl	32(%r12), %esi
	leal	-56613888(%rbx), %r9d
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$127, %edi
	jbe	.L1007
	movl	36(%r12), %esi
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$127, %edi
	jbe	.L1008
	movl	40(%r12), %esi
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$127, %edi
	jbe	.L1009
	movl	44(%r12), %esi
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$127, %edi
	jbe	.L1010
	movl	48(%r12), %esi
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$127, %edi
	jbe	.L1011
	movl	52(%r12), %esi
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$127, %edi
	jbe	.L1012
	movl	56(%r12), %esi
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$127, %edi
	jbe	.L1013
	movl	60(%r12), %esi
	movl	%r9d, %edi
	subl	%esi, %edi
	cmpl	$127, %edi
	jbe	.L1114
	cmpq	%r8, -72(%rbp)
	ja	.L1115
.L888:
	sall	$16, %edx
	movl	$4, %eax
	orl	-120(%rbp), %edx
.L769:
	cmpl	%eax, %r14d
	jl	.L912
	testq	%r13, %r13
	je	.L1116
	cmpl	$3, %eax
	je	.L917
	cmpl	$4, %eax
	jne	.L918
	movl	%edx, %esi
	addq	$4, %r13
	addq	$1, %r11
	shrl	$24, %esi
	movb	%sil, -1(%r11)
	movl	%r15d, -4(%r13)
.L917:
	movl	%edx, %esi
	addq	$4, %r13
	addq	$1, %r11
	shrl	$16, %esi
	movb	%sil, -1(%r11)
	movl	%r15d, -4(%r13)
.L918:
	movzbl	%dh, %ebx
	leaq	8(%r13), %r9
	movb	%bl, (%r11)
	leaq	2(%r11), %rbx
	movl	%r15d, 0(%r13)
	movb	%dl, 1(%r11)
	movl	%r15d, 4(%r13)
.L916:
	subl	%eax, %r14d
	movl	%ecx, %esi
	xorl	%edx, %edx
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L1116:
	cmpl	$3, %eax
	je	.L1027
	cmpl	$4, %eax
	jne	.L915
	movl	%edx, %edi
	leaq	1(%r11), %rsi
	shrl	$24, %edi
	movb	%dil, (%r11)
.L914:
	movl	%edx, %edi
	leaq	1(%rsi), %r11
	shrl	$16, %edi
	movb	%dil, (%rsi)
.L915:
	rolw	$8, %dx
	leaq	2(%r11), %rbx
	xorl	%r9d, %r9d
	movw	%dx, (%r11)
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L1089:
	movq	-120(%rbp), %r12
	movq	%rbx, %rdi
.L844:
	movq	-112(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L760
.L851:
	leal	-48(%rdx), %esi
	cmpl	$9, %esi
	jbe	.L852
	movl	%eax, %esi
	andl	$-33, %esi
	subl	$65, %esi
	cmpw	$25, %si
	ja	.L853
.L852:
	movzbl	-85(%rbp), %eax
	movb	$1, -84(%rbp)
	movq	%r9, %r13
	movq	%rbx, %r11
	addl	$224, %eax
	sall	$8, %eax
	orl	%eax, %edx
	movl	$2, %eax
	jmp	.L769
.L791:
	movb	$15, (%rbx)
	leaq	1(%rbx), %r11
	xorl	%r13d, %r13d
	testq	%r9, %r9
	je	.L805
	movl	%ecx, (%r9)
	leaq	4(%r9), %r13
.L805:
	sall	$16, %edx
	movl	%ecx, %r15d
	movb	$0, -84(%rbp)
	subl	$1, %r14d
	orl	%eax, %edx
	movl	%esi, %ecx
	movl	$4, %eax
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L1109:
	movq	%rbx, %rdi
	xorl	%r10d, %r10d
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L982:
	movq	%rbx, %rdi
	xorl	%r10d, %r10d
	jmp	.L844
.L1027:
	movq	%r11, %rsi
	jmp	.L914
.L850:
	cmpl	$57343, %edx
	jbe	.L1117
	orl	$15728640, %edx
	movq	%r9, %r13
	movq	%rbx, %r11
	movl	$3, %eax
	jmp	.L769
.L767:
	orw	$256, %r13w
	movq	-120(%rbp), %r12
	movl	%r10d, %r14d
	movl	$2, %eax
	movzwl	%r13w, %edx
	movq	%r9, %r13
	jmp	.L769
.L770:
	movl	%edx, %esi
	movq	-120(%rbp), %r12
	andl	$-2048, %esi
	cmpl	$55296, %esi
	je	.L1118
	cmpl	$159, %edx
	jbe	.L1119
	cmpl	$65279, %edx
	je	.L837
	cmpl	$65519, %edx
	ja	.L837
	movl	%edx, %eax
	subl	32(%r12), %eax
	cmpl	$127, %eax
	jbe	.L962
	movl	%edx, %eax
	subl	36(%r12), %eax
	cmpl	$127, %eax
	jbe	.L963
	movl	%edx, %eax
	subl	40(%r12), %eax
	cmpl	$127, %eax
	jbe	.L964
	movl	%edx, %eax
	subl	44(%r12), %eax
	cmpl	$127, %eax
	jbe	.L965
	movl	%edx, %eax
	subl	48(%r12), %eax
	cmpl	$127, %eax
	jbe	.L966
	movl	%edx, %eax
	subl	52(%r12), %eax
	cmpl	$127, %eax
	jbe	.L967
	movl	%edx, %eax
	subl	56(%r12), %eax
	cmpl	$127, %eax
	jbe	.L968
	movl	%edx, %eax
	subl	60(%r12), %eax
	cmpl	$127, %eax
	jbe	.L1120
	leal	-128(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L969
	leal	-256(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L970
	leal	-768(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L971
	leal	-8192(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L972
	leal	-8320(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L973
	leal	-8448(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L974
	leal	-12288(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L1121
	leaq	-60(%rbp), %rsi
	movl	%edx, %edi
	call	_ZL16getDynamicOffsetjPj
	movl	%eax, -128(%rbp)
	testl	%eax, %eax
	jns	.L1122
	leal	-13312(%rdx), %eax
	cmpl	$41983, %eax
	ja	.L837
	cmpq	%r8, -72(%rbp)
	jbe	.L838
	movzwl	2(%rbx), %eax
	subl	$13312, %eax
	cmpl	$41983, %eax
	ja	.L837
.L838:
	movb	$0, -84(%rbp)
	orl	$983040, %edx
	movq	%r9, %r13
	movl	%r10d, %r14d
	movl	$3, %eax
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L794:
	movl	%r13d, %eax
	movl	%r13d, %edi
	andl	$2147483520, %eax
	movl	%eax, -80(%rbp)
	movl	%eax, -60(%rbp)
.L1101:
	shrl	$7, %edi
	subl	$512, %edi
	sall	$8, %edi
	movl	%edi, -120(%rbp)
	movl	%r13d, %edi
	subl	%eax, %edi
.L793:
	movsbq	75(%r12), %rdx
	movzbl	76(%r12,%rdx), %r13d
	leal	1(%rdx), %r15d
	cmpb	$7, %dl
	je	.L933
	movb	%r15b, 75(%r12)
	movsbl	%r15b, %r15d
.L934:
	movl	-80(%rbp), %edx
	movzbl	%r13b, %eax
	movb	%r13b, -85(%rbp)
	movl	%eax, -128(%rbp)
	movzbl	%r13b, %eax
	movl	%edx, 32(%r12,%rax,4)
	movl	%r15d, %eax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L1123
	.p2align 4,,10
	.p2align 3
.L796:
	movslq	%edx, %r10
	cmpb	76(%r12,%r10), %r13b
	je	.L799
	movl	%edx, %eax
.L1124:
	movl	%eax, %edx
	subl	$1, %edx
	jns	.L796
.L1123:
	cmpb	83(%r12), %r13b
	je	.L958
	movl	$7, %edx
	movl	%edx, %eax
	jmp	.L1124
.L1117:
	testb	$4, %dh
	jne	.L881
	movq	%r8, -128(%rbp)
	movl	%ecx, %esi
	movq	%r9, %r13
	movq	%rbx, %r11
	jmp	.L840
.L799:
	cmpl	$8, %eax
	je	.L958
.L797:
	cmpl	%r15d, %eax
	jne	.L804
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L802:
	cmpl	%r15d, %r10d
	je	.L801
.L803:
	movl	%eax, %edx
	movl	%r10d, %eax
.L804:
	movslq	%eax, %r11
	movslq	%edx, %rdx
	movzbl	76(%r12,%r11), %r10d
	movb	%r10b, 76(%r12,%rdx)
	leal	1(%rax), %r10d
	cmpl	$7, %eax
	jne	.L802
	testl	%r15d, %r15d
	je	.L960
	xorl	%r10d, %r10d
	jmp	.L803
.L1115:
	movq	-128(%rbp), %rdi
	movzwl	2(%rdi), %edi
	movw	%di, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L887:
	cmpw	%ax, -128(%rbp)
	jne	.L888
	leal	-56614080(%rbx), %esi
	cmpl	$127, %esi
	jbe	.L1017
	leal	-56614480(%rbx), %esi
	cmpl	$127, %esi
	jbe	.L1018
	leal	-56614768(%rbx), %esi
	cmpl	$127, %esi
	jbe	.L1019
	leal	-56615216(%rbx), %esi
	cmpl	$127, %esi
	jbe	.L1020
	leal	-56626240(%rbx), %esi
	cmpl	$127, %esi
	jbe	.L1021
	leal	-56626336(%rbx), %esi
	cmpl	$127, %esi
	jbe	.L1022
	leal	-56679264(%rbx), %esi
	cmpl	$127, %esi
	jbe	.L1023
	cmpl	$127, %r9d
	jbe	.L888
	leal	-56679424(%rbx), %eax
	cmpl	$16383, %eax
	jbe	.L901
	cmpl	$13311, %r9d
	jbe	.L901
	leal	-56732672(%rbx), %eax
	cmpl	$12287, %eax
	jbe	.L901
	leal	-56671232(%rbx), %eax
	cmpl	$8175, %eax
	ja	.L888
	cmpl	$65279, %r9d
	je	.L888
	leal	-56657920(%rbx), %eax
	movl	%r9d, %edi
	movl	%r9d, %esi
	shrl	$7, %eax
	andl	$2147483520, %edi
	subl	$512, %eax
	movl	%edi, -80(%rbp)
	subl	%edi, %esi
	sall	$8, %eax
	movl	%edi, -60(%rbp)
	movl	%eax, -84(%rbp)
.L900:
	movsbq	75(%r12), %rdx
	movzbl	76(%r12,%rdx), %r10d
	leal	1(%rdx), %ebx
	cmpb	$7, %dl
	je	.L935
	movb	%bl, 75(%r12)
	movsbl	%bl, %ebx
.L936:
	movzbl	%r10b, %eax
	movl	-80(%rbp), %edi
	movb	%r10b, -85(%rbp)
	movl	%eax, -120(%rbp)
	movzbl	%r10b, %eax
	movl	%edi, 32(%r12,%rax,4)
	movl	%ebx, %eax
	.p2align 4,,10
	.p2align 3
.L907:
	movl	%eax, %edx
	subl	$1, %edx
	js	.L1125
	movslq	%edx, %rdi
	cmpb	%r10b, 76(%r12,%rdi)
	je	.L906
	movl	%edx, %eax
	jmp	.L907
.L1118:
	testb	$4, %dh
	jne	.L773
	movl	%ecx, %esi
	movl	%r10d, %r14d
	movl	%r15d, %ecx
	movq	%rax, %rbx
	jmp	.L756
.L933:
	movb	$0, 75(%r12)
	xorl	%r15d, %r15d
	jmp	.L934
.L906:
	cmpl	$8, %eax
	je	.L1024
.L904:
	cmpl	%ebx, %eax
	jne	.L911
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L909:
	cmpl	%ebx, %edi
	je	.L908
.L910:
	movl	%eax, %edx
	movl	%edi, %eax
.L911:
	movslq	%eax, %r9
	movslq	%edx, %rdx
	movzbl	76(%r12,%r9), %edi
	movb	%dil, 76(%r12,%rdx)
	leal	1(%rax), %edi
	cmpl	$7, %eax
	jne	.L909
	testl	%ebx, %ebx
	je	.L1026
	xorl	%edi, %edi
	jmp	.L910
.L947:
	movl	%edi, -80(%rbp)
	movl	$6, %r15d
	movb	$6, -85(%rbp)
	movl	$5632, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L779:
	movsbl	75(%r12), %r13d
	movl	%r13d, %eax
	movl	%eax, %edi
	subl	$1, %edi
	js	.L1126
	.p2align 4,,10
	.p2align 3
.L784:
	movslq	%edi, %rdx
	cmpb	%r15b, 76(%r12,%rdx)
	je	.L1127
	movl	%edi, %eax
.L1128:
	movl	%eax, %edi
	subl	$1, %edi
	jns	.L784
.L1126:
	cmpb	%r15b, 83(%r12)
	je	.L955
	movl	$7, %edi
	movl	%edi, %eax
	jmp	.L1128
.L1127:
	cmpl	$8, %eax
	je	.L955
.L785:
	cmpl	%r13d, %eax
	jne	.L790
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L788:
	cmpl	%edx, %r13d
	je	.L787
.L789:
	movl	%eax, %edi
	movl	%edx, %eax
.L790:
	movslq	%eax, %r11
	movslq	%edi, %rdi
	movzbl	76(%r12,%r11), %edx
	movb	%dl, 76(%r12,%rdi)
	leal	1(%rax), %edx
	cmpl	$7, %eax
	jne	.L788
	testl	%r13d, %r13d
	je	.L957
	xorl	%edx, %edx
	jmp	.L789
.L956:
	movslq	%edi, %r11
	.p2align 4,,10
	.p2align 3
.L787:
	movl	-120(%rbp), %edx
	movb	%r15b, 76(%r12,%r11)
	movq	%r9, %r13
	movl	%ecx, %r15d
	movq	%rbx, %r11
	movl	%esi, %ecx
	movl	$2, %eax
	orl	%r10d, %edx
	orb	$-128, %dl
	jmp	.L769
.L1125:
	cmpb	%r10b, 83(%r12)
	je	.L1024
	movl	$7, %edx
	movl	%edx, %eax
	jmp	.L907
.L959:
	movslq	%edx, %r11
.L801:
	movl	-128(%rbp), %edx
	orl	-120(%rbp), %edi
	movb	%r13b, 76(%r12,%r11)
	movl	%ecx, %r15d
	movq	%r9, %r13
	movl	%esi, %ecx
	movq	%rbx, %r11
	movl	$4, %eax
	sall	$21, %edx
	orl	%edi, %edx
	orl	$184549504, %edx
	jmp	.L769
.L955:
	xorl	%eax, %eax
	movl	$7, %edi
	jmp	.L785
.L757:
	movq	%rbx, %rdi
	movl	%edx, %r10d
	jmp	.L760
.L957:
	movl	$7, %r11d
	jmp	.L787
.L1104:
	movq	%rbx, %rdi
	movl	%edx, %r10d
	jmp	.L844
.L901:
	movl	%r9d, %esi
	movl	%r9d, %eax
	shrl	$7, %esi
	andl	$2147483520, %eax
	subl	$512, %esi
	movl	%eax, -80(%rbp)
	sall	$8, %esi
	movl	%eax, -60(%rbp)
	movl	%esi, -84(%rbp)
	movl	%r9d, %esi
	subl	%eax, %esi
	jmp	.L900
.L958:
	xorl	%eax, %eax
	movl	$7, %edx
	jmp	.L797
.L1025:
	movslq	%edx, %r9
.L908:
	movl	-120(%rbp), %edx
	orl	-84(%rbp), %esi
	movb	%r10b, 76(%r12,%r9)
	movl	$4, %eax
	movb	$1, -84(%rbp)
	sall	$21, %edx
	orl	%esi, %edx
	orl	$-251658112, %edx
	jmp	.L769
.L1007:
	movb	$0, -86(%rbp)
	movb	$0, -87(%rbp)
	movl	$57344, -132(%rbp)
	.p2align 4,,10
	.p2align 3
.L883:
	cmpq	%r8, -72(%rbp)
	jbe	.L885
	movq	-128(%rbp), %r10
	movzwl	2(%r10), %r10d
	movw	%r10w, -128(%rbp)
	subl	$13312, %r10d
	cmpl	$41983, %r10d
	jbe	.L887
.L885:
	movsbl	75(%r12), %ebx
	movzbl	-86(%rbp), %r10d
	movl	%ebx, %eax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L1129
	.p2align 4,,10
	.p2align 3
.L889:
	movslq	%edx, %r9
	cmpb	%r10b, 76(%r12,%r9)
	je	.L892
	movl	%edx, %eax
.L1130:
	movl	%eax, %edx
	subl	$1, %edx
	jns	.L889
.L1129:
	cmpb	%r10b, 83(%r12)
	je	.L1014
	movl	$7, %edx
	movl	%edx, %eax
	jmp	.L1130
.L892:
	cmpl	$8, %eax
	je	.L1014
.L890:
	cmpl	%ebx, %eax
	jne	.L897
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L895:
	cmpl	%r9d, %ebx
	je	.L894
.L896:
	movl	%eax, %edx
	movl	%r9d, %eax
.L897:
	movslq	%eax, %r10
	movslq	%edx, %rdx
	movzbl	76(%r12,%r10), %r9d
	movb	%r9b, 76(%r12,%rdx)
	leal	1(%rax), %r9d
	cmpl	$7, %eax
	jne	.L895
	testl	%ebx, %ebx
	je	.L1016
	xorl	%r9d, %r9d
	jmp	.L896
.L1015:
	movslq	%edx, %r10
.L894:
	movzbl	-86(%rbp), %eax
	movl	-132(%rbp), %edx
	movl	%esi, -80(%rbp)
	movb	$1, -84(%rbp)
	movb	%al, 76(%r12,%r10)
	movzbl	-87(%rbp), %eax
	orl	%edi, %edx
	orb	$-128, %dl
	movb	%al, -85(%rbp)
	movl	$2, %eax
	jmp	.L769
.L1014:
	xorl	%eax, %eax
	movl	$7, %edx
	jmp	.L890
.L1016:
	movl	$7, %r10d
	jmp	.L894
.L882:
	movq	-112(%rbp), %rax
	movq	%rbx, %rdi
	movl	$12, (%rax)
	jmp	.L760
.L1096:
	movq	%rbx, %rdi
	jmp	.L760
.L853:
	movl	32(%r12), %edi
	movl	%edx, %esi
	subl	%edi, %esi
	cmpl	$127, %esi
	jbe	.L985
	movl	36(%r12), %edi
	movl	%edx, %esi
	subl	%edi, %esi
	cmpl	$127, %esi
	jbe	.L986
	movl	40(%r12), %edi
	movl	%edx, %esi
	subl	%edi, %esi
	cmpl	$127, %esi
	jbe	.L987
	movl	44(%r12), %edi
	movl	%edx, %esi
	subl	%edi, %esi
	cmpl	$127, %esi
	jbe	.L988
	movl	48(%r12), %edi
	movl	%edx, %esi
	subl	%edi, %esi
	cmpl	$127, %esi
	jbe	.L989
	movl	52(%r12), %edi
	movl	%edx, %esi
	subl	%edi, %esi
	cmpl	$127, %esi
	jbe	.L990
	movl	56(%r12), %edi
	movl	%edx, %esi
	subl	%edi, %esi
	cmpl	$127, %esi
	jbe	.L991
	movl	60(%r12), %edi
	movl	%edx, %esi
	subl	%edi, %esi
	cmpl	$127, %esi
	jbe	.L1131
	leal	-192(%rdx), %esi
	cmpl	$127, %esi
	jbe	.L992
	leal	-592(%rdx), %esi
	cmpl	$127, %esi
	jbe	.L993
	leal	-880(%rdx), %esi
	cmpl	$127, %esi
	jbe	.L994
	leal	-1328(%rdx), %esi
	cmpl	$127, %esi
	jbe	.L995
	leal	-12352(%rdx), %esi
	cmpl	$127, %esi
	jbe	.L996
	leal	-12448(%rdx), %esi
	cmpl	$127, %esi
	jbe	.L997
	leal	-65376(%rdx), %esi
	cmpl	$127, %esi
	jbe	.L1132
	cmpl	$127, %edx
	jbe	.L1002
	cmpl	$13311, %edx
	jbe	.L868
	addw	$8192, %ax
	cmpw	$8175, %ax
	ja	.L1002
	cmpl	$65279, %edx
	je	.L1002
	movl	%edx, %eax
	andl	$2147483520, %eax
	movl	%eax, -80(%rbp)
	movl	%eax, -60(%rbp)
	leal	-44032(%rdx), %eax
	shrl	$7, %eax
	movl	%eax, -84(%rbp)
.L867:
	movsbq	75(%r12), %rsi
	movzbl	76(%r12,%rsi), %r11d
	leal	1(%rsi), %r13d
	cmpb	$7, %sil
	je	.L937
	movb	%r13b, 75(%r12)
	movsbl	%r13b, %r13d
.L938:
	movzbl	%r11b, %eax
	movl	-80(%rbp), %edi
	movb	%r11b, -85(%rbp)
	movl	%eax, -120(%rbp)
	movzbl	%r11b, %eax
	movl	%edi, 32(%r12,%rax,4)
	movl	%r13d, %eax
.L875:
	movl	%eax, %esi
	subl	$1, %esi
	js	.L1133
	movslq	%esi, %rdi
	cmpb	%r11b, 76(%r12,%rdi)
	je	.L874
	movl	%esi, %eax
	jmp	.L875
.L874:
	cmpl	$8, %eax
	je	.L1003
.L872:
	cmpl	%r13d, %eax
	jne	.L879
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L877:
	cmpl	%r13d, %r10d
	je	.L876
.L878:
	movl	%eax, %esi
	movl	%r10d, %eax
.L879:
	movslq	%eax, %rdi
	movslq	%esi, %rsi
	movzbl	76(%r12,%rdi), %r10d
	movb	%r10b, 76(%r12,%rsi)
	leal	1(%rax), %r10d
	cmpl	$7, %eax
	jne	.L877
	testl	%r13d, %r13d
	je	.L1005
	xorl	%r10d, %r10d
	jmp	.L878
.L1008:
	movb	$1, -87(%rbp)
	movl	$57600, -132(%rbp)
	movb	$1, -86(%rbp)
	jmp	.L883
.L837:
	orl	$917504, %edx
	movq	%r9, %r13
	movl	%r10d, %r14d
	movl	$3, %eax
	jmp	.L769
.L1009:
	movb	$2, -87(%rbp)
	movl	$57856, -132(%rbp)
	movb	$2, -86(%rbp)
	jmp	.L883
.L1011:
	movb	$4, -87(%rbp)
	movl	$58368, -132(%rbp)
	movb	$4, -86(%rbp)
	jmp	.L883
.L1010:
	movb	$3, -87(%rbp)
	movl	$58112, -132(%rbp)
	movb	$3, -86(%rbp)
	jmp	.L883
.L912:
	movq	-96(%rbp), %rbx
	subl	%r14d, %eax
	movq	%r11, %rdi
	leal	0(,%rax,8), %ecx
	movl	%eax, %r10d
	leaq	104(%rbx), %rsi
	movl	%edx, %ebx
	shrl	%cl, %ebx
	cmpl	$3, %eax
	je	.L920
	cmpl	$4, %eax
	jne	.L1134
	movq	-96(%rbp), %rcx
	movl	%edx, %eax
	shrl	$24, %eax
	movb	%al, 104(%rcx)
	leaq	105(%rcx), %rsi
.L920:
	movl	%edx, %eax
	addq	$1, %rsi
	shrl	$16, %eax
	movb	%al, -1(%rsi)
.L923:
	movb	%dh, (%rsi)
	leaq	1(%rsi), %rax
	movq	%r13, %r9
.L922:
	movb	%dl, (%rax)
.L924:
	movq	-96(%rbp), %rax
	movb	%r10b, 91(%rax)
	cmpl	$2, %r14d
	je	.L925
	cmpl	$3, %r14d
	je	.L926
	cmpl	$1, %r14d
	je	.L1135
.L928:
	movq	-112(%rbp), %rax
	xorl	%r10d, %r10d
	movl	$15, (%rax)
	jmp	.L760
.L1134:
	cmpl	$1, %eax
	je	.L1028
	movq	%r13, %r9
	cmpl	$2, %eax
	je	.L923
	jmp	.L924
.L960:
	movl	$7, %r11d
	jmp	.L801
.L941:
	movl	%edi, -80(%rbp)
	xorl	%r15d, %r15d
	movb	$0, -85(%rbp)
	movl	$4096, -120(%rbp)
	jmp	.L779
.L1012:
	movb	$5, -87(%rbp)
	movl	$58624, -132(%rbp)
	movb	$5, -86(%rbp)
	jmp	.L883
.L1119:
	andl	$127, %edx
	movq	%r9, %r13
	movl	%r10d, %r14d
	movl	$2, %eax
	orb	$2, %dh
	jmp	.L769
.L942:
	movl	%edi, -80(%rbp)
	movl	$1, %r15d
	movb	$1, -85(%rbp)
	movl	$4352, -120(%rbp)
	jmp	.L779
.L1013:
	movb	$6, -87(%rbp)
	movl	$58880, -132(%rbp)
	movb	$6, -86(%rbp)
	jmp	.L883
.L1114:
	movb	$7, -87(%rbp)
	movl	$59136, -132(%rbp)
	movb	$7, -86(%rbp)
	jmp	.L883
.L943:
	movl	%edi, -80(%rbp)
	movl	$2, %r15d
	movb	$2, -85(%rbp)
	movl	$4608, -120(%rbp)
	jmp	.L779
.L944:
	movl	%edi, -80(%rbp)
	movl	$3, %r15d
	movb	$3, -85(%rbp)
	movl	$4864, -120(%rbp)
	jmp	.L779
.L1133:
	cmpb	%r11b, 83(%r12)
	je	.L1003
	movl	$7, %esi
	movl	%esi, %eax
	jmp	.L875
.L945:
	movl	%edi, -80(%rbp)
	movl	$4, %r15d
	movb	$4, -85(%rbp)
	movl	$5120, -120(%rbp)
	jmp	.L779
.L946:
	movl	%edi, -80(%rbp)
	movl	$5, %r15d
	movb	$5, -85(%rbp)
	movl	$5376, -120(%rbp)
	jmp	.L779
.L1106:
	movl	%edi, -80(%rbp)
	movl	$7, %r15d
	movb	$7, -85(%rbp)
	movl	$5888, -120(%rbp)
	jmp	.L779
.L1024:
	xorl	%eax, %eax
	movl	$7, %edx
	jmp	.L904
.L948:
	movl	$-67328, -120(%rbp)
	movl	$192, -80(%rbp)
.L782:
	movl	-80(%rbp), %eax
	movl	%eax, -60(%rbp)
	jmp	.L793
.L950:
	movl	$-66816, -120(%rbp)
	movl	$880, -80(%rbp)
	jmp	.L782
.L949:
	movl	$-67072, -120(%rbp)
	movl	$592, -80(%rbp)
	jmp	.L782
.L953:
	movl	$-66048, -120(%rbp)
	movl	$12448, -80(%rbp)
	jmp	.L782
.L952:
	movl	$-66304, -120(%rbp)
	movl	$12352, -80(%rbp)
	jmp	.L782
.L951:
	movl	$-66560, -120(%rbp)
	movl	$1328, -80(%rbp)
	jmp	.L782
.L935:
	movb	$0, 75(%r12)
	xorl	%ebx, %ebx
	jmp	.L936
.L1107:
	movl	$-65792, -120(%rbp)
	movl	$65376, -80(%rbp)
	jmp	.L782
.L1026:
	movl	$7, %r9d
	jmp	.L908
.L1135:
	movb	%bl, (%rdi)
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L928
	movq	%r9, %rdx
.L931:
	movl	%r15d, (%rdx)
	leaq	4(%rdx), %r9
	jmp	.L928
.L926:
	movl	%ebx, %eax
	movl	%ebx, %edx
	shrl	$16, %eax
	shrl	$8, %edx
	movb	%al, (%rdi)
	leaq	2(%rdi), %rax
	testq	%r9, %r9
	je	.L929
	movl	%r15d, (%r9)
	addq	$4, %r9
	movb	%dl, 1(%rdi)
.L930:
	movl	%r15d, (%r9)
	leaq	4(%r9), %rdx
	leaq	1(%rax), %rdi
	movb	%bl, (%rax)
	jmp	.L931
.L925:
	movb	%bh, (%rdi)
	leaq	1(%rdi), %rax
	testq	%r9, %r9
	jne	.L930
.L932:
	movb	%bl, (%rax)
	leaq	1(%rax), %rdi
	xorl	%r9d, %r9d
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L847:
	movq	-96(%rbp), %rax
	movq	%rbx, %rdi
	movl	$1, %r10d
	movl	%esi, %ebx
	addq	$104, %rax
	jmp	.L922
.L1004:
	movslq	%esi, %rdi
.L876:
	movl	-84(%rbp), %eax
	subl	-80(%rbp), %edx
	movb	%r11b, 76(%r12,%rdi)
	movq	%r9, %r13
	movb	$1, -84(%rbp)
	movq	%rbx, %r11
	sall	$8, %eax
	orl	%eax, %edx
	movl	-120(%rbp), %eax
	addl	$232, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movl	$3, %eax
	orb	$-128, %dl
	jmp	.L769
.L868:
	movl	%edx, %eax
	andl	$2147483520, %eax
	movl	%eax, -80(%rbp)
	movl	%eax, -60(%rbp)
	movl	%edx, %eax
	shrl	$7, %eax
	movl	%eax, -84(%rbp)
	jmp	.L867
.L1019:
	movl	$-66816, -84(%rbp)
	movl	$880, -80(%rbp)
.L898:
	movl	-80(%rbp), %eax
	movl	%eax, -60(%rbp)
	jmp	.L900
.L1018:
	movl	$-67072, -84(%rbp)
	movl	$592, -80(%rbp)
	jmp	.L898
.L1022:
	movl	$-66048, -84(%rbp)
	movl	$12448, -80(%rbp)
	jmp	.L898
.L1021:
	movl	$-66304, -84(%rbp)
	movl	$12352, -80(%rbp)
	jmp	.L898
.L1020:
	movl	$-66560, -84(%rbp)
	movl	$1328, -80(%rbp)
	jmp	.L898
.L1017:
	movl	$-67328, -84(%rbp)
	movl	$192, -80(%rbp)
	jmp	.L898
.L1023:
	movl	$-65792, -84(%rbp)
	movl	$65376, -80(%rbp)
	jmp	.L898
.L929:
	movb	%dl, 1(%rdi)
	jmp	.L932
.L963:
	movl	$1, %esi
	movl	$512, %r13d
	movl	$4352, %r14d
	movl	$1, %eax
.L809:
	movslq	%eax, %rdi
	movl	32(%r12,%rdi,4), %edi
	movl	%edi, -120(%rbp)
	cmpq	%r8, -72(%rbp)
	jbe	.L811
	movzwl	2(%rbx), %edi
	movl	-120(%rbp), %ebx
	addl	$127, %ebx
	cmpl	%ebx, %edi
	ja	.L815
	cmpl	-120(%rbp), %edi
	jnb	.L811
	cmpl	$127, %edi
	ja	.L815
	cmpl	$31, %edi
	ja	.L811
	movl	$9729, %ebx
	btq	%rdi, %rbx
	jnc	.L815
.L811:
	movb	%al, -85(%rbp)
	movl	%eax, %r13d
	movsbl	75(%r12), %eax
	movl	%eax, -80(%rbp)
.L820:
	movl	%eax, %edi
	subl	$1, %edi
	js	.L1136
	movslq	%edi, %rbx
	cmpb	76(%r12,%rbx), %r13b
	je	.L819
	movl	%edi, %eax
	jmp	.L820
.L819:
	cmpl	$8, %eax
	je	.L976
.L817:
	cmpl	%eax, -80(%rbp)
	jne	.L824
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L822:
	cmpl	%r13d, -80(%rbp)
	je	.L821
.L823:
	movl	%eax, %edi
	movl	%r13d, %eax
.L824:
	movslq	%eax, %rbx
	movslq	%edi, %rdi
	movzbl	76(%r12,%rbx), %r13d
	movb	%r13b, 76(%r12,%rdi)
	leal	1(%rax), %r13d
	cmpl	$7, %eax
	jne	.L822
	movl	-80(%rbp), %edi
	testl	%edi, %edi
	je	.L978
	xorl	%r13d, %r13d
	jmp	.L823
.L1136:
	cmpb	%r13b, 83(%r12)
	je	.L976
	movl	$7, %edi
	movl	%edi, %eax
	jmp	.L820
.L815:
	subl	-120(%rbp), %edx
	movl	%r10d, %r14d
	movl	$2, %eax
	orl	%r13d, %edx
	movq	%r9, %r13
	orb	$-128, %dl
	jmp	.L769
.L977:
	movslq	%edi, %rbx
.L821:
	movzbl	-85(%rbp), %eax
	movq	%r9, %r13
	movb	%sil, -85(%rbp)
	movb	%al, 76(%r12,%rbx)
	movl	-120(%rbp), %eax
	subl	%eax, %edx
	movl	%eax, -80(%rbp)
	movl	$2, %eax
	orl	%r14d, %edx
	movl	%r10d, %r14d
	orb	$-128, %dl
	jmp	.L769
.L976:
	xorl	%eax, %eax
	movl	$7, %edi
	jmp	.L817
.L978:
	movl	$7, %ebx
	jmp	.L821
.L962:
	xorl	%esi, %esi
	movl	$256, %r13d
	movl	$4096, %r14d
	xorl	%eax, %eax
	jmp	.L809
.L937:
	movb	$0, 75(%r12)
	xorl	%r13d, %r13d
	jmp	.L938
.L881:
	movq	-112(%rbp), %rax
	movq	%rbx, %rdi
	movl	%edx, %r10d
	movl	$12, (%rax)
	jmp	.L760
.L1005:
	movl	$7, %edi
	jmp	.L876
.L1003:
	xorl	%eax, %eax
	movl	$7, %esi
	jmp	.L872
.L994:
	movl	$880, -80(%rbp)
	movl	$2, %eax
.L857:
	movl	-80(%rbp), %edi
	addl	$249, %eax
	movl	%eax, -84(%rbp)
	movl	%edi, -60(%rbp)
	jmp	.L867
.L993:
	movl	$592, -80(%rbp)
	movl	$1, %eax
	jmp	.L857
.L992:
	movl	$192, -80(%rbp)
	xorl	%eax, %eax
	jmp	.L857
.L1131:
	movl	%edi, -80(%rbp)
	movl	$59136, %r11d
	movb	$7, -84(%rbp)
	movb	$7, -85(%rbp)
.L854:
	movsbl	75(%r12), %r13d
	movzbl	-84(%rbp), %r10d
	movl	%r13d, %eax
.L856:
	movl	%eax, %edx
	subl	$1, %edx
	js	.L1137
	movslq	%edx, %rdi
	cmpb	%r10b, 76(%r12,%rdi)
	je	.L1138
	movl	%edx, %eax
	jmp	.L856
.L1138:
	cmpl	$8, %eax
	je	.L999
.L860:
	cmpl	%r13d, %eax
	jne	.L865
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L863:
	cmpl	%r13d, %edi
	je	.L862
.L864:
	movl	%eax, %edx
	movl	%edi, %eax
.L865:
	movslq	%eax, %r10
	movslq	%edx, %rdx
	movzbl	76(%r12,%r10), %edi
	movb	%dil, 76(%r12,%rdx)
	leal	1(%rax), %edi
	cmpl	$7, %eax
	jne	.L863
	testl	%r13d, %r13d
	je	.L1001
	xorl	%edi, %edi
	jmp	.L864
.L1137:
	cmpb	%r10b, 83(%r12)
	je	.L999
	movl	$7, %edx
	movl	%edx, %eax
	jmp	.L856
.L1000:
	movslq	%edx, %r10
.L862:
	movzbl	-84(%rbp), %eax
	movl	%r11d, %edx
	movb	$1, -84(%rbp)
	movq	%r9, %r13
	orl	%esi, %edx
	movq	%rbx, %r11
	movb	%al, 76(%r12,%r10)
	orb	$-128, %dl
	movl	$2, %eax
	jmp	.L769
.L999:
	xorl	%eax, %eax
	movl	$7, %edx
	jmp	.L860
.L1001:
	movl	$7, %r10d
	jmp	.L862
.L991:
	movl	%edi, -80(%rbp)
	movl	$58880, %r11d
	movb	$6, -84(%rbp)
	movb	$6, -85(%rbp)
	jmp	.L854
.L990:
	movl	%edi, -80(%rbp)
	movl	$58624, %r11d
	movb	$5, -84(%rbp)
	movb	$5, -85(%rbp)
	jmp	.L854
.L989:
	movl	%edi, -80(%rbp)
	movl	$58368, %r11d
	movb	$4, -84(%rbp)
	movb	$4, -85(%rbp)
	jmp	.L854
.L988:
	movl	%edi, -80(%rbp)
	movl	$58112, %r11d
	movb	$3, -84(%rbp)
	movb	$3, -85(%rbp)
	jmp	.L854
.L987:
	movl	%edi, -80(%rbp)
	movl	$57856, %r11d
	movb	$2, -84(%rbp)
	movb	$2, -85(%rbp)
	jmp	.L854
.L986:
	movl	%edi, -80(%rbp)
	movl	$57600, %r11d
	movb	$1, -84(%rbp)
	movb	$1, -85(%rbp)
	jmp	.L854
.L985:
	movl	%edi, -80(%rbp)
	movl	$57344, %r11d
	movb	$0, -85(%rbp)
	jmp	.L854
.L967:
	movl	$5, %esi
	movl	$1536, %r13d
	movl	$5376, %r14d
	movl	$5, %eax
	jmp	.L809
.L966:
	movl	$4, %esi
	movl	$1280, %r13d
	movl	$5120, %r14d
	movl	$4, %eax
	jmp	.L809
.L965:
	movl	$3, %esi
	movl	$1024, %r13d
	movl	$4864, %r14d
	movl	$3, %eax
	jmp	.L809
.L964:
	movl	$2, %esi
	movl	$768, %r13d
	movl	$4608, %r14d
	movl	$2, %eax
	jmp	.L809
.L773:
	movq	-112(%rbp), %rbx
	movl	%edx, %r10d
	movq	%rax, %rdi
	movl	$12, (%rbx)
	jmp	.L760
.L1122:
	movsbq	75(%r12), %rax
	movq	%rax, %rsi
	movzbl	76(%r12,%rax), %edi
	leal	1(%rax), %eax
	cmpb	$7, %sil
	movl	$0, %esi
	cmove	%esi, %eax
	movzbl	%dil, %ebx
	movb	%bl, -85(%rbp)
	movb	%al, 75(%r12)
	movq	%rbx, %rax
	movl	%ebx, -120(%rbp)
	movl	-60(%rbp), %ebx
	movl	%ebx, 32(%r12,%rax,4)
	movsbl	75(%r12), %r14d
	movl	%ebx, -80(%rbp)
	movl	%r14d, %eax
.L832:
	movl	%eax, %ebx
	subl	$1, %ebx
	js	.L1139
	movslq	%ebx, %rsi
	cmpb	76(%r12,%rsi), %dil
	je	.L831
	movl	%ebx, %eax
	jmp	.L832
.L1139:
	cmpb	83(%r12), %dil
	je	.L979
	movl	$7, %ebx
	movl	%ebx, %eax
	jmp	.L832
.L831:
	cmpl	$8, %eax
	je	.L979
.L829:
	cmpl	%eax, %r14d
	jne	.L836
	jmp	.L833
.L834:
	cmpl	%r13d, %r14d
	je	.L981
.L835:
	movslq	%eax, %rsi
	movl	%r13d, %eax
.L836:
	movslq	%eax, %rbx
	movzbl	76(%r12,%rbx), %r13d
	movb	%r13b, 76(%r12,%rsi)
	leal	1(%rax), %r13d
	cmpl	$7, %eax
	jne	.L834
	testl	%r14d, %r14d
	je	.L980
	xorl	%r13d, %r13d
	jmp	.L835
.L1121:
	movl	$7, %eax
.L813:
	movslq	%eax, %rsi
	leaq	_ZL13staticOffsets(%rip), %rbx
	addl	$1, %eax
	movq	%r9, %r13
	sall	$8, %eax
	subl	(%rbx,%rsi,4), %edx
	movl	%r10d, %r14d
	orl	%eax, %edx
	movl	$2, %eax
	jmp	.L769
.L979:
	xorl	%eax, %eax
	movl	$7, %esi
	jmp	.L829
.L981:
	movq	%rbx, %rsi
.L833:
	movl	-120(%rbp), %eax
	movb	%dil, 76(%r12,%rsi)
	movq	%r9, %r13
	movl	%r10d, %r14d
	movl	-128(%rbp), %esi
	subl	-80(%rbp), %edx
	addl	$24, %eax
	sall	$16, %eax
	sall	$8, %esi
	orl	%esi, %eax
	orl	%eax, %edx
	movl	$3, %eax
	orb	$-128, %dl
	jmp	.L769
.L1028:
	movq	%r13, %r9
	movq	%rsi, %rax
	movl	$1, %r10d
	jmp	.L922
.L974:
	movl	$6, %eax
	jmp	.L813
.L973:
	movl	$5, %eax
	jmp	.L813
.L972:
	movl	$4, %eax
	jmp	.L813
.L971:
	movl	$3, %eax
	jmp	.L813
.L1113:
	call	__stack_chk_fail@PLT
.L1132:
	movl	$65376, -80(%rbp)
	movl	$6, %eax
	jmp	.L857
.L997:
	movl	$12448, -80(%rbp)
	movl	$5, %eax
	jmp	.L857
.L970:
	movl	$2, %eax
	jmp	.L813
.L969:
	movl	$1, %eax
	jmp	.L813
.L1120:
	movl	$7, %esi
	movl	$2048, %r13d
	movl	$5888, %r14d
	movl	$7, %eax
	jmp	.L809
.L968:
	movl	$6, %esi
	movl	$1792, %r13d
	movl	$5632, %r14d
	movl	$6, %eax
	jmp	.L809
.L996:
	movl	$12352, -80(%rbp)
	movl	$4, %eax
	jmp	.L857
.L995:
	movl	$1328, -80(%rbp)
	movl	$3, %eax
	jmp	.L857
.L980:
	movl	$7, %esi
	jmp	.L833
	.cfi_endproc
.LFE2123:
	.size	_SCSUFromUnicodeWithOffsets, .-_SCSUFromUnicodeWithOffsets
	.globl	_SCSUData_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_SCSUData_67, @object
	.size	_SCSUData_67, 296
_SCSUData_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL15_SCSUStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL9_SCSUImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL15_SCSUStaticData, @object
	.size	_ZL15_SCSUStaticData, 100
_ZL15_SCSUStaticData:
	.long	100
	.string	"SCSU"
	.zero	55
	.long	1212
	.byte	0
	.byte	24
	.byte	1
	.byte	3
	.string	"\016\377\375"
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL9_SCSUImpl, @object
	.size	_ZL9_SCSUImpl, 144
_ZL9_SCSUImpl:
	.long	24
	.zero	4
	.quad	0
	.quad	0
	.quad	_SCSUOpen
	.quad	_SCSUClose
	.quad	_SCSUReset
	.quad	_SCSUToUnicode
	.quad	_SCSUToUnicodeWithOffsets
	.quad	_SCSUFromUnicode
	.quad	_SCSUFromUnicodeWithOffsets
	.quad	0
	.quad	0
	.quad	_SCSUGetName
	.quad	0
	.quad	_SCSUSafeClone
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.section	.rodata
	.align 8
	.type	_ZL19initialWindowUse_ja, @object
	.size	_ZL19initialWindowUse_ja, 8
_ZL19initialWindowUse_ja:
	.string	"\003\002\004\001"
	.ascii	"\007\005\006"
	.align 8
	.type	_ZL16initialWindowUse, @object
	.size	_ZL16initialWindowUse, 8
_ZL16initialWindowUse:
	.string	"\007"
	.ascii	"\003\002\004\005\006\001"
	.align 16
	.type	_ZL12fixedOffsets, @object
	.size	_ZL12fixedOffsets, 28
_ZL12fixedOffsets:
	.long	192
	.long	592
	.long	880
	.long	1328
	.long	12352
	.long	12448
	.long	65376
	.align 32
	.type	_ZL21initialDynamicOffsets, @object
	.size	_ZL21initialDynamicOffsets, 32
_ZL21initialDynamicOffsets:
	.long	128
	.long	192
	.long	1024
	.long	1536
	.long	2304
	.long	12352
	.long	12448
	.long	65280
	.align 32
	.type	_ZL13staticOffsets, @object
	.size	_ZL13staticOffsets, 32
_ZL13staticOffsets:
	.long	0
	.long	128
	.long	256
	.long	768
	.long	8192
	.long	8320
	.long	8448
	.long	12288
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
