	.file	"schriter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723StringCharacterIterator17getDynamicClassIDEv
	.type	_ZNK6icu_6723StringCharacterIterator17getDynamicClassIDEv, @function
_ZNK6icu_6723StringCharacterIterator17getDynamicClassIDEv:
.LFB1309:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6723StringCharacterIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE1309:
	.size	_ZNK6icu_6723StringCharacterIterator17getDynamicClassIDEv, .-_ZNK6icu_6723StringCharacterIterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIteratorD2Ev
	.type	_ZN6icu_6723StringCharacterIteratorD2Ev, @function
_ZN6icu_6723StringCharacterIteratorD2Ev:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6723StringCharacterIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	32(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6722UCharCharacterIteratorD2Ev@PLT
	.cfi_endproc
.LFE1326:
	.size	_ZN6icu_6723StringCharacterIteratorD2Ev, .-_ZN6icu_6723StringCharacterIteratorD2Ev
	.globl	_ZN6icu_6723StringCharacterIteratorD1Ev
	.set	_ZN6icu_6723StringCharacterIteratorD1Ev,_ZN6icu_6723StringCharacterIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIteratorD0Ev
	.type	_ZN6icu_6723StringCharacterIteratorD0Ev, @function
_ZN6icu_6723StringCharacterIteratorD0Ev:
.LFB1328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6723StringCharacterIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	32(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6722UCharCharacterIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE1328:
	.size	_ZN6icu_6723StringCharacterIteratorD0Ev, .-_ZN6icu_6723StringCharacterIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIterator7getTextERNS_13UnicodeStringE
	.type	_ZN6icu_6723StringCharacterIterator7getTextERNS_13UnicodeStringE, @function
_ZN6icu_6723StringCharacterIterator7getTextERNS_13UnicodeStringE:
.LFB1333:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	32(%r8), %rsi
	jmp	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	.cfi_endproc
.LFE1333:
	.size	_ZN6icu_6723StringCharacterIterator7getTextERNS_13UnicodeStringE, .-_ZN6icu_6723StringCharacterIterator7getTextERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723StringCharacterIteratoreqERKNS_24ForwardCharacterIteratorE
	.type	_ZNK6icu_6723StringCharacterIteratoreqERKNS_24ForwardCharacterIteratorE, @function
_ZNK6icu_6723StringCharacterIteratoreqERKNS_24ForwardCharacterIteratorE:
.LFB1330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L17
	movq	(%rdi), %rax
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L10
	xorl	%r12d, %r12d
	cmpb	$42, (%rdi)
	je	.L8
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L8
.L10:
	movswl	40(%r13), %edx
	movswl	40(%rbx), %eax
	movl	%edx, %r12d
	movl	%eax, %ecx
	andl	$1, %ecx
	andl	$1, %r12d
	jne	.L11
	testw	%dx, %dx
	js	.L12
	sarl	$5, %edx
.L13:
	testw	%ax, %ax
	js	.L14
	sarl	$5, %eax
.L15:
	testb	%cl, %cl
	jne	.L8
	cmpl	%edx, %eax
	je	.L29
.L8:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	leaq	32(%rbx), %rsi
	leaq	32(%r13), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%r12d, %r12d
	testb	%cl, %cl
	je	.L8
	movl	12(%rbx), %eax
	cmpl	%eax, 12(%r13)
	jne	.L8
	movq	16(%rbx), %rax
	cmpq	%rax, 16(%r13)
	sete	%r12b
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L12:
	movl	44(%r13), %edx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$8, %rsp
	movl	$1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	44(%rbx), %eax
	jmp	.L15
	.cfi_endproc
.LFE1330:
	.size	_ZNK6icu_6723StringCharacterIteratoreqERKNS_24ForwardCharacterIteratorE, .-_ZNK6icu_6723StringCharacterIteratoreqERKNS_24ForwardCharacterIteratorE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723StringCharacterIterator5cloneEv
	.type	_ZNK6icu_6723StringCharacterIterator5cloneEv, @function
_ZNK6icu_6723StringCharacterIterator5cloneEv:
.LFB1331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L30
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6722UCharCharacterIteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6723StringCharacterIteratorE(%rip), %rax
	leaq	32(%rbx), %rsi
	movq	%rax, (%r12)
	leaq	32(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	40(%r12), %eax
	testb	$17, %al
	jne	.L34
	testb	$2, %al
	jne	.L39
	movq	56(%r12), %rax
.L32:
	movq	%rax, 24(%r12)
.L30:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	leaq	42(%r12), %rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%eax, %eax
	jmp	.L32
	.cfi_endproc
.LFE1331:
	.size	_ZNK6icu_6723StringCharacterIterator5cloneEv, .-_ZNK6icu_6723StringCharacterIterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIterator16getStaticClassIDEv
	.type	_ZN6icu_6723StringCharacterIterator16getStaticClassIDEv, @function
_ZN6icu_6723StringCharacterIterator16getStaticClassIDEv:
.LFB1308:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6723StringCharacterIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE1308:
	.size	_ZN6icu_6723StringCharacterIterator16getStaticClassIDEv, .-_ZN6icu_6723StringCharacterIterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIteratorC2Ev
	.type	_ZN6icu_6723StringCharacterIteratorC2Ev, @function
_ZN6icu_6723StringCharacterIteratorC2Ev:
.LFB1311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6722UCharCharacterIteratorC2Ev@PLT
	leaq	16+_ZTVN6icu_6723StringCharacterIteratorE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 32(%rbx)
	movl	$2, %eax
	movw	%ax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1311:
	.size	_ZN6icu_6723StringCharacterIteratorC2Ev, .-_ZN6icu_6723StringCharacterIteratorC2Ev
	.globl	_ZN6icu_6723StringCharacterIteratorC1Ev
	.set	_ZN6icu_6723StringCharacterIteratorC1Ev,_ZN6icu_6723StringCharacterIteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringE
	.type	_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringE, @function
_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringE:
.LFB1314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L44
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L51
.L56:
	testb	$2, %al
	jne	.L54
	movq	24(%r12), %rax
.L46:
	leaq	-32(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEi@PLT
	leaq	16+_ZTVN6icu_6723StringCharacterIteratorE(%rip), %rax
	leaq	32(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, (%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	40(%rbx), %eax
	testb	$17, %al
	jne	.L52
	testb	$2, %al
	je	.L49
	leaq	42(%rbx), %rax
.L48:
	movq	%rax, 24(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	leaq	10(%r12), %rax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L49:
	movq	56(%rbx), %rax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L44:
	movl	12(%rsi), %edx
	testb	$17, %al
	je	.L56
.L51:
	xorl	%eax, %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%eax, %eax
	jmp	.L48
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1314:
	.size	_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringE, .-_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringE
	.globl	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE
	.set	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE,_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEi
	.type	_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEi, @function
_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEi:
.LFB1317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L58
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L65
.L70:
	testb	$2, %al
	jne	.L68
	movq	24(%r12), %rax
.L60:
	leaq	-32(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEii@PLT
	leaq	16+_ZTVN6icu_6723StringCharacterIteratorE(%rip), %rax
	leaq	32(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, (%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	40(%rbx), %eax
	testb	$17, %al
	jne	.L66
	testb	$2, %al
	je	.L63
	leaq	42(%rbx), %rax
.L62:
	movq	%rax, 24(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	leaq	10(%r12), %rax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L63:
	movq	56(%rbx), %rax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L58:
	movl	12(%rsi), %edx
	testb	$17, %al
	je	.L70
.L65:
	xorl	%eax, %eax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%eax, %eax
	jmp	.L62
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1317:
	.size	_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEi, .-_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEi
	.globl	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringEi
	.set	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringEi,_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEiii
	.type	_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEiii, @function
_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEiii:
.LFB1320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r10d
	movl	%r8d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L72
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L79
.L84:
	testb	$2, %al
	jne	.L82
	movq	24(%r12), %rax
.L74:
	movl	%ecx, %r8d
	leaq	-32(%rbp), %rsi
	movl	%r10d, %ecx
	movq	%rbx, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6722UCharCharacterIteratorC2ENS_14ConstChar16PtrEiiii@PLT
	leaq	16+_ZTVN6icu_6723StringCharacterIteratorE(%rip), %rax
	leaq	32(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, (%rbx)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	40(%rbx), %eax
	testb	$17, %al
	jne	.L80
	testb	$2, %al
	je	.L77
	leaq	42(%rbx), %rax
.L76:
	movq	%rax, 24(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	leaq	10(%r12), %rax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L77:
	movq	56(%rbx), %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L72:
	movl	12(%rsi), %edx
	testb	$17, %al
	je	.L84
.L79:
	xorl	%eax, %eax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%eax, %eax
	jmp	.L76
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1320:
	.size	_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEiii, .-_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEiii
	.globl	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringEiii
	.set	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringEiii,_ZN6icu_6723StringCharacterIteratorC2ERKNS_13UnicodeStringEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIteratorC2ERKS0_
	.type	_ZN6icu_6723StringCharacterIteratorC2ERKS0_, @function
_ZN6icu_6723StringCharacterIteratorC2ERKS0_:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6722UCharCharacterIteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6723StringCharacterIteratorE(%rip), %rax
	leaq	32(%r12), %rsi
	movq	%rax, (%rbx)
	leaq	32(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzwl	40(%rbx), %eax
	testb	$17, %al
	jne	.L88
	testb	$2, %al
	jne	.L90
	movq	56(%rbx), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	leaq	42(%rbx), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	xorl	%eax, %eax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1323:
	.size	_ZN6icu_6723StringCharacterIteratorC2ERKS0_, .-_ZN6icu_6723StringCharacterIteratorC2ERKS0_
	.globl	_ZN6icu_6723StringCharacterIteratorC1ERKS0_
	.set	_ZN6icu_6723StringCharacterIteratorC1ERKS0_,_ZN6icu_6723StringCharacterIteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIteratoraSERKS0_
	.type	_ZN6icu_6723StringCharacterIteratoraSERKS0_, @function
_ZN6icu_6723StringCharacterIteratoraSERKS0_:
.LFB1329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN6icu_6722UCharCharacterIteratoraSERKS0_@PLT
	leaq	32(%rbx), %rsi
	leaq	32(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzwl	40(%r12), %eax
	testb	$17, %al
	jne	.L94
	testb	$2, %al
	jne	.L96
	movq	56(%r12), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	leaq	42(%r12), %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbx
	movq	%rax, 24(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1329:
	.size	_ZN6icu_6723StringCharacterIteratoraSERKS0_, .-_ZN6icu_6723StringCharacterIteratoraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723StringCharacterIterator7setTextERKNS_13UnicodeStringE
	.type	_ZN6icu_6723StringCharacterIterator7setTextERKNS_13UnicodeStringE, @function
_ZN6icu_6723StringCharacterIterator7setTextERKNS_13UnicodeStringE:
.LFB1332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzwl	40(%r12), %eax
	testw	%ax, %ax
	js	.L98
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L103
.L107:
	testb	$2, %al
	jne	.L105
	movq	56(%r12), %rax
.L100:
	leaq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN6icu_6722UCharCharacterIterator7setTextENS_14ConstChar16PtrEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	leaq	42(%r12), %rax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L98:
	movl	44(%r12), %edx
	testb	$17, %al
	je	.L107
.L103:
	xorl	%eax, %eax
	jmp	.L100
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1332:
	.size	_ZN6icu_6723StringCharacterIterator7setTextERKNS_13UnicodeStringE, .-_ZN6icu_6723StringCharacterIterator7setTextERKNS_13UnicodeStringE
	.weak	_ZTSN6icu_6723StringCharacterIteratorE
	.section	.rodata._ZTSN6icu_6723StringCharacterIteratorE,"aG",@progbits,_ZTSN6icu_6723StringCharacterIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6723StringCharacterIteratorE, @object
	.size	_ZTSN6icu_6723StringCharacterIteratorE, 35
_ZTSN6icu_6723StringCharacterIteratorE:
	.string	"N6icu_6723StringCharacterIteratorE"
	.weak	_ZTIN6icu_6723StringCharacterIteratorE
	.section	.data.rel.ro._ZTIN6icu_6723StringCharacterIteratorE,"awG",@progbits,_ZTIN6icu_6723StringCharacterIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6723StringCharacterIteratorE, @object
	.size	_ZTIN6icu_6723StringCharacterIteratorE, 24
_ZTIN6icu_6723StringCharacterIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6723StringCharacterIteratorE
	.quad	_ZTIN6icu_6722UCharCharacterIteratorE
	.weak	_ZTVN6icu_6723StringCharacterIteratorE
	.section	.data.rel.ro._ZTVN6icu_6723StringCharacterIteratorE,"awG",@progbits,_ZTVN6icu_6723StringCharacterIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6723StringCharacterIteratorE, @object
	.size	_ZTVN6icu_6723StringCharacterIteratorE, 232
_ZTVN6icu_6723StringCharacterIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6723StringCharacterIteratorE
	.quad	_ZN6icu_6723StringCharacterIteratorD1Ev
	.quad	_ZN6icu_6723StringCharacterIteratorD0Ev
	.quad	_ZNK6icu_6723StringCharacterIterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6723StringCharacterIteratoreqERKNS_24ForwardCharacterIteratorE
	.quad	_ZNK6icu_6722UCharCharacterIterator8hashCodeEv
	.quad	_ZN6icu_6722UCharCharacterIterator11nextPostIncEv
	.quad	_ZN6icu_6722UCharCharacterIterator13next32PostIncEv
	.quad	_ZN6icu_6722UCharCharacterIterator7hasNextEv
	.quad	_ZNK6icu_6723StringCharacterIterator5cloneEv
	.quad	_ZN6icu_6722UCharCharacterIterator5firstEv
	.quad	_ZN6icu_6722UCharCharacterIterator12firstPostIncEv
	.quad	_ZN6icu_6722UCharCharacterIterator7first32Ev
	.quad	_ZN6icu_6722UCharCharacterIterator14first32PostIncEv
	.quad	_ZN6icu_6722UCharCharacterIterator4lastEv
	.quad	_ZN6icu_6722UCharCharacterIterator6last32Ev
	.quad	_ZN6icu_6722UCharCharacterIterator8setIndexEi
	.quad	_ZN6icu_6722UCharCharacterIterator10setIndex32Ei
	.quad	_ZNK6icu_6722UCharCharacterIterator7currentEv
	.quad	_ZNK6icu_6722UCharCharacterIterator9current32Ev
	.quad	_ZN6icu_6722UCharCharacterIterator4nextEv
	.quad	_ZN6icu_6722UCharCharacterIterator6next32Ev
	.quad	_ZN6icu_6722UCharCharacterIterator8previousEv
	.quad	_ZN6icu_6722UCharCharacterIterator10previous32Ev
	.quad	_ZN6icu_6722UCharCharacterIterator11hasPreviousEv
	.quad	_ZN6icu_6722UCharCharacterIterator4moveEiNS_17CharacterIterator7EOriginE
	.quad	_ZN6icu_6722UCharCharacterIterator6move32EiNS_17CharacterIterator7EOriginE
	.quad	_ZN6icu_6723StringCharacterIterator7getTextERNS_13UnicodeStringE
	.local	_ZZN6icu_6723StringCharacterIterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6723StringCharacterIterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
