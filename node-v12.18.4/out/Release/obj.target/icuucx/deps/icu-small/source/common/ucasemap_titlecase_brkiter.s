	.file	"ucasemap_titlecase_brkiter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap11utf8ToTitleEPKcjPNS_13BreakIteratorENS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap11utf8ToTitleEPKcjPNS_13BreakIteratorENS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap11utf8ToTitleEPKcjPNS_13BreakIteratorENS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB3097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %rbx
	movq	%r9, -232(%rbp)
	movq	%rax, -240(%rbp)
	movl	(%rbx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L17
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	%rdi, %r14
	movq	%rcx, %r13
	leaq	-208(%rbp), %rdi
	movl	%esi, %r12d
	movl	$18, %ecx
	leaq	-208(%rbp), %r15
	movq	%r13, %rsi
	movq	%rdx, -256(%rbp)
	rep stosq
	movslq	%r8d, %rdx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	%r8, -248(%rbp)
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	call	utext_openUTF8_67@PLT
	xorl	%edi, %edi
	movq	%rbx, %r9
	movl	%r12d, %edx
	movq	-256(%rbp), %r10
	movq	%r14, %rsi
	leaq	-216(%rbp), %r8
	movq	$0, -216(%rbp)
	movq	%r10, %rcx
	call	ustrcase_getTitleBreakIterator_67@PLT
	movq	-248(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L15
	movq	(%rax), %rax
	movq	%r10, %rdi
	movq	%r11, -256(%rbp)
	movq	%rbx, %rdx
	movq	%r10, -248(%rbp)
	movq	%r15, %rsi
	call	*64(%rax)
	movq	%r14, %rdi
	call	ustrcase_getCaseLocale_67@PLT
	subq	$8, %rsp
	movq	%r13, %rcx
	movl	%r12d, %esi
	pushq	%rbx
	movq	-256(%rbp), %r11
	movl	%eax, %edi
	pushq	-240(%rbp)
	movq	-248(%rbp), %r10
	movq	ucasemap_internalUTF8ToTitle_67@GOTPCREL(%rip), %r9
	pushq	-232(%rbp)
	movl	%r11d, %r8d
	movq	%r10, %rdx
	call	_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeES7_S9_SB_@PLT
	addq	$32, %rsp
.L15:
	movq	%r15, %rdi
	call	utext_close_67@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3097:
	.size	_ZN6icu_677CaseMap11utf8ToTitleEPKcjPNS_13BreakIteratorENS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap11utf8ToTitleEPKcjPNS_13BreakIteratorENS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap11utf8ToTitleEPKcjPNS_13BreakIteratorES2_iPciPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap11utf8ToTitleEPKcjPNS_13BreakIteratorES2_iPciPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap11utf8ToTitleEPKcjPNS_13BreakIteratorES2_iPciPNS_5EditsER10UErrorCode:
.LFB3098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	%r9, -232(%rbp)
	movq	32(%rbp), %r9
	movq	%rax, -240(%rbp)
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L30
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	leaq	-208(%rbp), %r11
	movq	%rdi, %r14
	movq	%rcx, %rbx
	movq	%rdx, %r15
	leaq	-208(%rbp), %rdi
	movl	$18, %ecx
	movslq	%r8d, %rdx
	movl	%esi, %r12d
	rep stosq
	movq	%rbx, %rsi
	movq	%r11, %rdi
	movq	%r9, %rcx
	movq	%r11, -256(%rbp)
	movl	%r8d, -260(%rbp)
	movq	%r9, -248(%rbp)
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	call	utext_openUTF8_67@PLT
	movq	%r15, %rcx
	xorl	%edi, %edi
	movl	%r12d, %edx
	movq	-248(%rbp), %r9
	movq	%r14, %rsi
	leaq	-216(%rbp), %r8
	movq	$0, -216(%rbp)
	call	ustrcase_getTitleBreakIterator_67@PLT
	movq	-248(%rbp), %r9
	movq	-256(%rbp), %r11
	testq	%rax, %rax
	movl	-260(%rbp), %r10d
	movq	%rax, %r15
	je	.L29
	movq	(%rax), %rax
	movq	%r11, %rsi
	movq	%r11, -248(%rbp)
	movq	%r9, %rdx
	movl	%r10d, -260(%rbp)
	movq	%r15, %rdi
	movq	%r9, -256(%rbp)
	call	*64(%rax)
	movq	%r14, %rdi
	call	ustrcase_getCaseLocale_67@PLT
	movl	16(%rbp), %r8d
	movq	%r15, %rdx
	movl	%r12d, %esi
	movq	-256(%rbp), %r9
	movl	-260(%rbp), %r10d
	movl	%eax, %edi
	movq	-232(%rbp), %rcx
	pushq	%r9
	movq	%rbx, %r9
	pushq	-240(%rbp)
	pushq	ucasemap_internalUTF8ToTitle_67@GOTPCREL(%rip)
	pushq	%r10
	call	_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPciPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeESA_SC_@PLT
	movq	-248(%rbp), %r11
	addq	$32, %rsp
	movl	%eax, %r13d
.L29:
	movq	%r11, %rdi
	call	utext_close_67@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L19
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3098:
	.size	_ZN6icu_677CaseMap11utf8ToTitleEPKcjPNS_13BreakIteratorES2_iPciPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap11utf8ToTitleEPKcjPNS_13BreakIteratorES2_iPciPNS_5EditsER10UErrorCode
	.p2align 4
	.globl	ucasemap_getBreakIterator_67
	.type	ucasemap_getBreakIterator_67, @function
ucasemap_getBreakIterator_67:
.LFB3099:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3099:
	.size	ucasemap_getBreakIterator_67, .-ucasemap_getBreakIterator_67
	.p2align 4
	.globl	ucasemap_setBreakIterator_67
	.type	ucasemap_setBreakIterator_67, @function
ucasemap_setBreakIterator_67:
.LFB3100:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L40
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L35
	movq	(%rdi), %rax
	call	*8(%rax)
.L35:
	movq	%r12, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3100:
	.size	ucasemap_setBreakIterator_67, .-ucasemap_setBreakIterator_67
	.p2align 4
	.globl	ucasemap_utf8ToTitle_67
	.type	ucasemap_utf8ToTitle_67, @function
ucasemap_utf8ToTitle_67:
.LFB3101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L44
.L49:
	xorl	%r12d, %r12d
.L43:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	leaq	-208(%rbp), %r11
	movq	%rcx, %r13
	xorl	%eax, %eax
	movq	%rdi, %r12
	movl	$18, %ecx
	movq	%r9, %rbx
	movq	%rsi, %r15
	movq	%r13, %rsi
	leaq	-208(%rbp), %rdi
	movl	%edx, -240(%rbp)
	movslq	%r8d, %rdx
	movl	%r8d, %r14d
	rep stosq
	movq	%r9, %rcx
	movq	%r11, %rdi
	movq	%r11, -232(%rbp)
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	call	utext_openUTF8_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L49
	movq	(%r12), %rdi
	movq	-232(%rbp), %r11
	movl	-240(%rbp), %r10d
	testq	%rdi, %rdi
	je	.L56
.L47:
	movq	(%rdi), %rax
	movq	%r11, -232(%rbp)
	movq	%r11, %rsi
	movq	%rbx, %rdx
	movl	%r10d, -240(%rbp)
	call	*64(%rax)
	movl	44(%r12), %esi
	movl	40(%r12), %edi
	pushq	%rbx
	movl	-240(%rbp), %r10d
	pushq	$0
	movq	%r13, %r9
	movq	%r15, %rcx
	pushq	ucasemap_internalUTF8ToTitle_67@GOTPCREL(%rip)
	movq	(%r12), %rdx
	pushq	%r14
	movl	%r10d, %r8d
	call	_Z16ucasemap_mapUTF8ijPN6icu_6713BreakIteratorEPciPKciPFvijS1_PKhiRNS_8ByteSinkEPNS_5EditsER10UErrorCodeESA_SC_@PLT
	movq	-232(%rbp), %r11
	addq	$32, %rsp
	movl	%eax, %r12d
	movq	%r11, %rdi
	call	utext_close_67@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L56:
	movl	44(%r12), %edx
	xorl	%ecx, %ecx
	leaq	8(%r12), %rsi
	movq	%rbx, %r9
	leaq	-216(%rbp), %r8
	movq	%r11, -240(%rbp)
	movl	%r10d, -232(%rbp)
	movq	$0, -216(%rbp)
	call	ustrcase_getTitleBreakIterator_67@PLT
	movl	-232(%rbp), %r10d
	movq	-240(%rbp), %r11
	testq	%rax, %rax
	je	.L57
	movq	-216(%rbp), %rdi
	movq	%rdi, (%r12)
	jmp	.L47
.L57:
	movq	%r11, %rdi
	call	utext_close_67@PLT
	movq	-216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L49
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3101:
	.size	ucasemap_utf8ToTitle_67, .-ucasemap_utf8ToTitle_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
