	.file	"ucnv2022.cpp"
	.text
	.p2align 4
	.type	_ISO2022getName, @function
_ISO2022getName:
.LFB2119:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	leaq	113(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE2119:
	.size	_ISO2022getName, .-_ISO2022getName
	.p2align 4
	.type	_ZL16changeState_2022P10UConverterPPKcS2_11Variant2022P10UErrorCode, @function
_ZL16changeState_2022P10UConverterPPKcS2_11Variant2022P10UErrorCode:
.LFB2121:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsi, %r9
	xorl	%r11d, %r11d
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZL27escSeqStateTable_Value_2022(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZL24normalize_esq_chars_2022(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	_ZL25escSeqStateTable_Key_2022(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -44(%rbp)
	movzbl	64(%rax), %ebx
	movq	16(%rdi), %r12
	movb	%bl, -45(%rbp)
	movl	104(%r12), %edi
.L7:
	movq	(%r9), %rcx
	cmpq	%rdx, %rcx
	jnb	.L8
	leaq	1(%rcx), %rsi
	movq	%rsi, (%r9)
	movsbq	64(%rax), %rsi
	movzbl	(%rcx), %ecx
	leal	1(%rsi), %r10d
	movb	%r10b, 64(%rax)
	movb	%cl, 65(%rax,%rsi)
	movsbl	(%r14,%rcx), %ecx
	testl	%ecx, %ecx
	je	.L9
	sall	$5, %edi
	movl	$36938, %ebx
	movl	$74, %r11d
	movl	$37, %esi
	addl	%edi, %ecx
	xorl	%edi, %edi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L85:
	jle	.L84
	movl	%esi, %edi
	cmpl	%edi, %r11d
	je	.L9
.L86:
	leal	(%r11,%rdi), %r10d
	sarl	%r10d
	cmpl	%esi, %r10d
	je	.L9
	movslq	%r10d, %rsi
	movl	0(%r13,%rsi,4), %ebx
	movslq	%r10d, %rsi
.L10:
	cmpl	%ebx, %ecx
	jge	.L85
	movl	%esi, %r11d
	cmpl	%edi, %r11d
	jne	.L86
.L9:
	movl	$0, 104(%r12)
.L27:
	movl	$18, (%r8)
.L18:
	movzbl	64(%rax), %edx
	cmpb	$1, %dl
	jle	.L6
	leal	-1(%rdx), %ecx
	subb	-45(%rbp), %dl
	cmpb	%dl, %cl
	jg	.L43
	movzbl	%cl, %ecx
	subq	%rcx, (%r9)
.L44:
	movb	$1, 64(%rax)
.L6:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movslq	%esi, %r10
	movl	%ecx, %edi
	movsbl	(%r15,%r10), %r11d
	cmpb	$2, %r11b
	je	.L12
	jg	.L7
	cmpb	$-1, %r11b
	je	.L14
	cmpb	$1, %r11b
	jne	.L7
.L12:
	movl	$0, 104(%r12)
.L15:
	movl	-44(%rbp), %ebx
	cmpl	$2, %ebx
	je	.L19
	cmpl	$3, %ebx
	jne	.L87
	leaq	_ZL20nextStateToUnicodeCN(%rip), %rdx
	movzbl	(%rdx,%rsi), %ecx
	leal	1(%rcx), %edx
	cmpb	$35, %dl
	ja	.L32
	leaq	.L34(%rip), %rsi
	movzbl	%dl, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L34:
	.long	.L31-.L34
	.long	.L32-.L34
	.long	.L35-.L34
	.long	.L38-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L37-.L34
	.long	.L36-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L32-.L34
	.long	.L35-.L34
	.long	.L33-.L34
	.text
	.p2align 4,,10
	.p2align 3
.L19:
	cmpl	$48, %esi
	je	.L29
.L31:
	movl	$19, (%r8)
.L26:
	movl	$0, 284(%rax)
	jmp	.L6
.L36:
	cmpb	$0, 95(%r12)
	je	.L27
	movzbl	96(%r12), %edx
	cmpb	$1, %dl
	jg	.L40
	movb	%dl, 97(%r12)
.L40:
	movb	$3, 96(%r12)
	.p2align 4,,10
	.p2align 3
.L29:
	movl	(%r8), %edx
	testl	%edx, %edx
	jg	.L41
	movb	$0, 64(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	cmpl	$18, %edx
	je	.L18
	cmpl	$19, %edx
	je	.L26
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	leaq	_ZL20nextStateToUnicodeJP(%rip), %rdx
	movzbl	(%rdx,%rsi), %edx
	cmpb	$2, %dl
	jg	.L21
	testb	%dl, %dl
	jg	.L22
	cmpb	$-1, %dl
	je	.L31
.L24:
	movl	108(%r12), %esi
	leaq	_ZL14jpCharsetMasks(%rip), %rcx
	movzwl	(%rcx,%rsi,2), %ecx
	btl	%edx, %ecx
	jnc	.L31
	movb	%dl, 92(%r12)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L21:
	cmpb	$16, %dl
	jne	.L24
.L37:
	cmpb	$0, 94(%r12)
	je	.L27
	movzbl	96(%r12), %edx
	cmpb	$1, %dl
	jg	.L39
	movb	%dl, 97(%r12)
.L39:
	movb	$2, 96(%r12)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L32:
	movl	108(%r12), %edx
	testl	%edx, %edx
	je	.L31
	movb	%cl, 95(%r12)
	jmp	.L29
.L38:
	movl	108(%r12), %esi
	testl	%esi, %esi
	je	.L31
.L35:
	movb	%cl, 93(%r12)
	jmp	.L29
.L33:
	movb	$34, 94(%r12)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L8:
	movl	%edi, 104(%r12)
	testl	%r11d, %r11d
	je	.L6
	addl	$1, %r11d
	je	.L27
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L22:
	movl	108(%r12), %esi
	leaq	_ZL14jpCharsetMasks(%rip), %rcx
	movzwl	(%rcx,%rsi,2), %ecx
	btl	%edx, %ecx
	jnc	.L31
	movb	%dl, 94(%r12)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L43:
	movl	%edx, %esi
	leaq	250(%rax), %r8
	subl	%ecx, %esi
	movb	%sil, 282(%rax)
	movsbl	%sil, %esi
	negl	%esi
	movslq	%esi, %rcx
	leaq	66(%rax), %rsi
	cmpq	$8, %rcx
	jnb	.L45
	testb	$4, %cl
	jne	.L88
	testq	%rcx, %rcx
	je	.L46
	movzbl	66(%rax), %edi
	movb	%dil, 250(%rax)
	testb	$2, %cl
	jne	.L89
.L46:
	movsbq	%dl, %rdx
	subq	%rdx, (%r9)
	jmp	.L44
.L45:
	movq	66(%rax), %rdi
	movq	%rdi, 250(%rax)
	movq	-8(%rsi,%rcx), %rdi
	movq	%rdi, -8(%r8,%rcx)
	leaq	258(%rax), %rdi
	andq	$-8, %rdi
	subq	%rdi, %r8
	addq	%r8, %rcx
	subq	%r8, %rsi
	shrq	$3, %rcx
	rep movsq
	jmp	.L46
.L88:
	movl	66(%rax), %edi
	movl	%edi, 250(%rax)
	movl	-4(%rsi,%rcx), %esi
	movl	%esi, -4(%r8,%rcx)
	jmp	.L46
.L89:
	movzwl	-2(%rsi,%rcx), %esi
	movw	%si, -2(%r8,%rcx)
	jmp	.L46
.L14:
	movl	%ecx, 104(%r12)
	jmp	.L27
	.cfi_endproc
.LFE2121:
	.size	_ZL16changeState_2022P10UConverterPPKcS2_11Variant2022P10UErrorCode, .-_ZL16changeState_2022P10UConverterPPKcS2_11Variant2022P10UErrorCode
	.p2align 4
	.type	_ZL18_ISO_2022_WriteSubP25UConverterFromUnicodeArgsiP10UErrorCode, @function
_ZL18_ISO_2022_WriteSubP25UConverterFromUnicodeArgsiP10UErrorCode:
.LFB2139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %r12
	movq	40(%rbx), %rdx
	movzbl	143(%r12), %eax
	cmpb	$106, %al
	je	.L91
	cmpb	$107, %al
	je	.L92
	cmpb	$99, %al
	je	.L93
	xorl	%edx, %edx
	leaq	-48(%rbp), %rsi
.L94:
	call	ucnv_cbFromUWriteBytes_67@PLT
.L90:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	leaq	-48(%rbp), %rsi
	cmpb	$1, 102(%r12)
	movq	%rsi, %rax
	je	.L126
.L95:
	movzbl	98(%r12), %r9d
	testb	%r9b, %r9b
	je	.L100
	cmpb	$3, %r9b
	je	.L100
	movl	$10267, %r9d
	movb	$0, 98(%r12)
	addq	$3, %rax
	movw	%r9w, -3(%rax)
	movb	$66, -1(%rax)
.L100:
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	leaq	1(%rax), %rdx
	subl	%esi, %edx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L93:
	cmpb	$0, 102(%r12)
	je	.L112
	movb	$0, 102(%r12)
	leaq	-47(%rbp), %rax
	leaq	-48(%rbp), %rsi
	movb	$15, -48(%rbp)
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	leaq	1(%rax), %rdx
	subl	%esi, %edx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L92:
	movl	108(%r12), %esi
	movzbl	89(%rbx), %eax
	testl	%esi, %esi
	jne	.L98
	movl	80(%rbx), %esi
	cmpb	$1, %al
	je	.L127
	testl	%esi, %esi
	jne	.L113
	movl	$1, 80(%rbx)
	leaq	-47(%rbp), %rax
	leaq	-48(%rbp), %rsi
	movb	$14, -48(%rbp)
.L101:
	movzbl	(%rdx), %r9d
	movb	%r9b, (%rax)
	movzbl	1(%rdx), %edx
	movb	%dl, 1(%rax)
	leaq	2(%rax), %rdx
	subl	%esi, %edx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L127:
	testl	%esi, %esi
	jne	.L128
.L112:
	movzbl	(%rdx), %edx
	leaq	-48(%rbp), %rsi
	movq	%rsi, %rax
	movb	%dl, (%rax)
	leaq	1(%rax), %rdx
	subl	%esi, %edx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L126:
	movb	$0, 102(%r12)
	leaq	-47(%rbp), %rax
	movb	$15, -48(%rbp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L98:
	movq	80(%r12), %rcx
	xorl	%esi, %esi
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	movq	40(%rcx), %r14
	movzbl	89(%rcx), %r13d
	movq	%rdx, 40(%rcx)
	movb	%al, 89(%rcx)
	movq	80(%r12), %rax
	movl	84(%rbx), %edx
	movq	%rax, 8(%rdi)
	movl	%edx, 84(%rax)
	movq	%r8, %rdx
	call	ucnv_cbFromUWriteSub_67@PLT
	movq	80(%r12), %rax
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %r8
	movl	84(%rax), %eax
	movl	%eax, 84(%rbx)
	movq	%rbx, 8(%rdi)
	movq	80(%r12), %rax
	movb	%r13b, 89(%rax)
	cmpl	$15, (%r8)
	movq	%r14, 40(%rax)
	jne	.L90
	movq	80(%r12), %rdx
	movsbq	91(%rdx), %rax
	testb	%al, %al
	jg	.L129
.L103:
	movb	%al, 91(%rbx)
	movq	80(%r12), %rax
	movb	$0, 91(%rax)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L113:
	leaq	-48(%rbp), %rsi
	movq	%rsi, %rax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$0, 80(%rbx)
	leaq	-47(%rbp), %rax
	leaq	-48(%rbp), %rsi
	movb	$15, -48(%rbp)
	jmp	.L100
.L129:
	leaq	104(%rbx), %rcx
	leaq	104(%rdx), %rsi
	cmpl	$8, %eax
	jnb	.L104
	testb	$4, %al
	jne	.L130
	testl	%eax, %eax
	je	.L105
	movzbl	104(%rdx), %edx
	movb	%dl, 104(%rbx)
	testb	$2, %al
	jne	.L122
.L124:
	movq	80(%r12), %rdx
.L105:
	movzbl	91(%rdx), %eax
	jmp	.L103
.L104:
	movq	104(%rdx), %rdx
	movq	%rdx, 104(%rbx)
	movl	%eax, %edx
	movq	-8(%rsi,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	112(%rbx), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rsi
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L124
.L130:
	movl	104(%rdx), %edx
	movl	%eax, %eax
	movl	%edx, 104(%rbx)
	movl	-4(%rsi,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	movq	80(%r12), %rdx
	jmp	.L105
.L125:
	call	__stack_chk_fail@PLT
.L122:
	movl	%eax, %eax
	movzwl	-2(%rsi,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
	movq	80(%r12), %rdx
	jmp	.L105
	.cfi_endproc
.LFE2139:
	.size	_ZL18_ISO_2022_WriteSubP25UConverterFromUnicodeArgsiP10UErrorCode, .-_ZL18_ISO_2022_WriteSubP25UConverterFromUnicodeArgsiP10UErrorCode
	.p2align 4
	.type	_ZL13_ISO2022ResetP10UConverter21UConverterResetChoice, @function
_ZL13_ISO2022ResetP10UConverter21UConverterResetChoice:
.LFB2118:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	%rdi, %r8
	cmpl	$1, %esi
	jle	.L142
	xorl	%eax, %eax
	cmpb	$107, 143(%rdx)
	movl	$0, 98(%rdx)
	movw	%ax, 102(%rdx)
	je	.L135
.L131:
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$0, %ecx
	movl	$0, 92(%rdx)
	movw	%cx, 96(%rdx)
	movl	$0, 104(%rdx)
	movb	$0, 112(%rdx)
	jne	.L143
	cmpb	$107, 143(%rdx)
	jne	.L131
	cmpl	$1, 108(%rdx)
	jne	.L131
	movq	80(%rdx), %rax
.L139:
	movq	$0, 72(%rax)
	movb	$0, 64(%rax)
	cmpl	$1, %esi
	je	.L131
	.p2align 4,,10
	.p2align 3
.L135:
	cmpb	$0, 91(%r8)
	jne	.L137
	movb	$4, 91(%r8)
	movl	$1126769691, 104(%r8)
.L137:
	cmpl	$1, 108(%rdx)
	jne	.L131
	movq	80(%rdx), %rax
	movq	$1, 80(%rax)
	ret
.L143:
	leaq	98(%rdx), %r9
	xorl	%eax, %eax
	movl	$6, %ecx
	movq	%r9, %rdi
	rep stosb
	cmpb	$107, 143(%rdx)
	jne	.L131
	cmpl	$1, 108(%rdx)
	movq	80(%rdx), %rax
	jne	.L135
	jmp	.L139
	.cfi_endproc
.LFE2118:
	.size	_ZL13_ISO2022ResetP10UConverter21UConverterResetChoice, .-_ZL13_ISO2022ResetP10UConverter21UConverterResetChoice
	.p2align 4
	.type	_ZL46UConverter_toUnicode_ISO_2022_CN_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode, @function
_ZL46UConverter_toUnicode_ISO_2022_CN_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode:
.LFB2138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rdi), %r12
	movq	32(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %r9
	movq	8(%rdi), %rdi
	movq	%r12, -72(%rbp)
	movq	16(%rdi), %r13
	movzbl	64(%rdi), %eax
	movl	104(%r13), %r8d
	testl	%r8d, %r8d
	jne	.L145
	cmpb	$1, %al
	jne	.L146
	cmpq	%r9, %r12
	jb	.L224
.L146:
	cmpq	%r12, %r9
	jbe	.L225
	movq	40(%rbx), %rdx
	leaq	.L153(%rip), %rsi
.L150:
	cmpq	%rdx, %r15
	jnb	.L147
	leaq	1(%r12), %rax
	movq	%rax, -72(%rbp)
	movzbl	-1(%rax), %r8d
	leal	-10(%r8), %edx
	movl	%r8d, %ecx
	cmpb	$17, %dl
	ja	.L151
	movzbl	%dl, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L153:
	.long	.L156-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L156-.L153
	.long	.L155-.L153
	.long	.L154-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L151-.L153
	.long	.L152-.L153
	.text
	.p2align 4,,10
	.p2align 3
.L156:
	xorl	%edx, %edx
	movl	$0, 92(%r13)
	movw	%dx, 96(%r13)
.L151:
	movsbq	96(%r13), %rdx
	movb	$0, 112(%r13)
	testb	%dl, %dl
	jne	.L226
	cmpl	$127, %r8d
	jbe	.L176
	movq	8(%rbx), %rdx
	movb	%cl, 65(%rdx)
	movb	$1, 64(%rdx)
.L177:
	movl	$12, (%r14)
.L149:
	movq	%r15, 32(%rbx)
	movq	%rax, 16(%rbx)
.L144:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	%r12, -72(%rbp)
	movzbl	64(%rdi), %eax
.L145:
	movq	%r9, %rdx
	leaq	-72(%rbp), %rsi
	movq	%r14, %r8
	movl	$3, %ecx
	movb	%al, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_ZL16changeState_2022P10UConverterPPKcS2_11Variant2022P10UErrorCode
	movl	104(%r13), %edi
	movq	-88(%rbp), %r9
	movzbl	-96(%rbp), %eax
	testl	%edi, %edi
	je	.L161
	movq	-72(%rbp), %r12
.L162:
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L146
.L163:
	movq	%r15, 32(%rbx)
	movq	%r12, 16(%rbx)
	movb	$0, 112(%r13)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L226:
	cmpq	%r9, %rax
	jnb	.L165
.L148:
	movzbl	(%rax), %r12d
	leal	-33(%rcx), %edi
	leal	-33(%r12), %esi
	cmpb	$93, %dil
	ja	.L166
	cmpb	$93, %sil
	ja	.L171
	movsbl	92(%r13,%rdx), %ecx
	addq	$1, %rax
	movl	%r8d, %edx
	movq	%rax, -72(%rbp)
	movl	%ecx, %eax
	cmpb	$31, %cl
	jg	.L228
	movslq	%ecx, %rcx
	movl	%r8d, %eax
	movl	%r12d, %edx
	movl	$2, %r10d
	movq	0(%r13,%rcx,8), %rdi
.L168:
	movb	%dl, -58(%rbp)
	leaq	-59(%rbp), %rsi
	xorl	%ecx, %ecx
	movl	%r10d, %edx
	movq	%r9, -96(%rbp)
	movl	%r8d, -88(%rbp)
	movb	%al, -59(%rbp)
	call	ucnv_MBCSSimpleGetNextUChar_67@PLT
	movl	-88(%rbp), %r8d
	movzbl	%r12b, %esi
	movq	-96(%rbp), %r9
	sall	$8, %r8d
	orl	%r8d, %esi
	cmpb	$1, 96(%r13)
	jle	.L170
	movzbl	97(%r13), %edx
	movb	%dl, 96(%r13)
.L170:
	cmpl	$65533, %eax
	ja	.L178
	movq	48(%rbx), %rcx
	movq	-72(%rbp), %r12
	testq	%rcx, %rcx
	je	.L200
	movl	%r12d, %edx
	subl	16(%rbx), %edx
	cmpl	$256, %esi
	movl	%eax, %r8d
	sbbl	%esi, %esi
	movq	%r15, %rdi
	addl	$2, %esi
.L180:
	movq	%r15, %rax
	subq	32(%rbx), %rax
	subl	%esi, %edx
	sarq	%rax
	movl	%edx, (%rcx,%rax,4)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L161:
	movl	(%r14), %esi
	movq	-72(%rbp), %rdx
	testl	%esi, %esi
	jle	.L229
	movq	%rdx, %r12
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L224:
	cmpq	%r15, 40(%rbx)
	ja	.L230
.L147:
	movl	$15, (%r14)
	movq	-72(%rbp), %rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L155:
	cmpb	$0, 93(%r13)
	jne	.L231
	movb	$0, 112(%r13)
	movl	$14, %ecx
.L160:
	movq	8(%rbx), %rax
	movb	%cl, 65(%rax)
	movb	$1, 64(%rax)
.L174:
	movq	-72(%rbp), %rax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L154:
	cmpb	$0, 112(%r13)
	movb	$0, 96(%r13)
	je	.L157
	movb	$0, 112(%r13)
	movq	8(%rbx), %rdx
	movl	$18, (%r14)
	movl	$2, 284(%rdx)
	movb	$15, 65(%rdx)
	movq	8(%rbx), %rdx
	movb	$1, 64(%rdx)
	movq	%r15, 32(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L231:
	movb	$1, 96(%r13)
	movb	$1, 112(%r13)
.L157:
	cmpq	%r9, %rax
	jnb	.L149
	movq	40(%rbx), %rdx
	movq	%rax, %r12
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L176:
	movq	48(%rbx), %rcx
	testq	%rcx, %rcx
	jne	.L193
	movq	%rax, %r12
	movq	%r15, %rdi
.L179:
	movw	%r8w, (%rdi)
	addq	$2, %r15
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L229:
	cmpb	$0, 112(%r13)
	je	.L199
	movq	%rdx, %rsi
	movq	8(%rbx), %rcx
	movl	$18, (%r14)
	subq	%r12, %rsi
	movq	%rdx, %r12
	movl	$2, 284(%rcx)
	addl	%esi, %eax
	movb	%al, 64(%rcx)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L165:
	movq	8(%rbx), %rdx
	movb	%cl, 65(%rdx)
	movq	8(%rbx), %rdx
	movb	$1, 64(%rdx)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L228:
	movb	%r12b, -57(%rbp)
	movq	24(%r13), %rdi
	addl	$96, %eax
	movl	$3, %r10d
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%rdx, %r12
	jmp	.L146
.L178:
	cmpl	$65535, %eax
	jbe	.L232
	subl	$65536, %eax
	movq	48(%rbx), %rcx
	movq	-72(%rbp), %r12
	movl	%eax, %edx
	andw	$1023, %ax
	movq	40(%rbx), %r8
	shrl	$10, %edx
	subw	$9216, %ax
	subw	$10240, %dx
	movw	%dx, (%r15)
	leaq	2(%r15), %rdx
	testq	%rcx, %rcx
	je	.L185
	movq	%r12, %rdi
	subq	16(%rbx), %rdi
	cmpl	$255, %esi
	ja	.L233
	movq	32(%rbx), %rsi
	movq	%r15, %r10
	leal	-1(%rdi), %r11d
	subq	%rsi, %r10
	sarq	%r10
	movl	%r11d, (%rcx,%r10,4)
	cmpq	%r8, %rdx
	jnb	.L192
	movw	%ax, 2(%r15)
	movl	$1, %eax
.L194:
	subq	%rsi, %rdx
	subl	%eax, %edi
	sarq	%rdx
	movl	%edi, (%rcx,%rdx,4)
.L190:
	addq	$4, %r15
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L192:
	movq	8(%rbx), %rsi
	movq	%rdx, %r15
	movsbq	93(%rsi), %rcx
	leal	1(%rcx), %edi
	movb	%dil, 93(%rsi)
	movw	%ax, 144(%rsi,%rcx,2)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L233:
	movq	32(%rbx), %rsi
	movq	%r15, %r10
	leal	-2(%rdi), %r11d
	subq	%rsi, %r10
	sarq	%r10
	movl	%r11d, (%rcx,%r10,4)
	cmpq	%r8, %rdx
	jnb	.L192
	movw	%ax, 2(%r15)
	movl	$2, %eax
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L230:
	movzbl	65(%rdi), %r8d
	movb	$0, 64(%rdi)
	movq	%r12, %rax
	movsbq	96(%r13), %rdx
	movl	%r8d, %ecx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L166:
	cmpb	$93, %sil
	jbe	.L223
	.p2align 4,,10
	.p2align 3
.L171:
	cmpb	$31, %r12b
	ja	.L173
	movl	$134266880, %esi
	btl	%r12d, %esi
	jc	.L223
.L173:
	sall	$8, %r8d
	addq	$1, %rax
	orl	%r8d, %r12d
	movq	%rax, -72(%rbp)
	orl	$65536, %r12d
	cmpb	$1, %dl
	jle	.L175
	movzbl	97(%r13), %eax
	rolw	$8, %r12w
	movb	%al, 96(%r13)
	movq	8(%rbx), %rax
	movw	%r12w, 65(%rax)
	movb	$2, 64(%rax)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L223:
	cmpb	$1, %dl
	jle	.L160
	movzbl	97(%r13), %eax
	movb	%al, 96(%r13)
	movq	8(%rbx), %rax
	movb	%cl, 65(%rax)
	movb	$1, 64(%rax)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L185:
	cmpq	%r8, %rdx
	jnb	.L192
	movw	%ax, 2(%r15)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L193:
	movl	%eax, %edx
	movq	%rax, %r12
	subl	16(%rbx), %edx
	movq	%r15, %rdi
	movl	$1, %esi
	jmp	.L180
.L232:
	movq	8(%rbx), %rdx
	cmpl	$255, %esi
	jbe	.L234
	rolw	$8, %si
	movb	$2, 64(%rdx)
	movw	%si, 65(%rdx)
.L188:
	cmpl	$65534, %eax
	jne	.L174
	movl	$10, (%r14)
	movq	-72(%rbp), %rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%r12, %rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L175:
	movq	8(%rbx), %rax
	rolw	$8, %r12w
	movw	%r12w, 65(%rax)
	movb	$2, 64(%rax)
	jmp	.L174
.L227:
	call	__stack_chk_fail@PLT
.L200:
	movl	%eax, %r8d
	movq	%r15, %rdi
	jmp	.L179
.L234:
	movb	%sil, 65(%rdx)
	movb	$1, 64(%rdx)
	jmp	.L188
	.cfi_endproc
.LFE2138:
	.size	_ZL46UConverter_toUnicode_ISO_2022_CN_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode, .-_ZL46UConverter_toUnicode_ISO_2022_CN_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode
	.p2align 4
	.type	_ZL46UConverter_toUnicode_ISO_2022_JP_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode, @function
_ZL46UConverter_toUnicode_ISO_2022_JP_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode:
.LFB2132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rdi), %r12
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %r9
	movq	8(%rdi), %rdi
	movq	%r12, -72(%rbp)
	movq	16(%rdi), %r13
	movzbl	64(%rdi), %eax
	movl	104(%r13), %esi
	testl	%esi, %esi
	jne	.L236
	cmpq	%r9, %r12
	jnb	.L237
	cmpb	$1, %al
	je	.L347
.L237:
	cmpq	%r12, %r9
	jbe	.L348
	movq	40(%rbx), %rax
	leaq	.L244(%rip), %rcx
.L241:
	cmpq	%rax, %r14
	jnb	.L238
	leaq	1(%r12), %rdi
	movq	%rdi, -72(%rbp)
	movzbl	-1(%rdi), %edx
	leal	-10(%rdx), %eax
	movl	%edx, %r10d
	movl	%edx, %r8d
	cmpb	$17, %al
	ja	.L242
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L244:
	.long	.L247-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L247-.L244
	.long	.L246-.L244
	.long	.L245-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L243-.L244
	.text
	.p2align 4,,10
	.p2align 3
.L243:
	movq	8(%rbx), %rdi
	movq	%r12, -72(%rbp)
	movzbl	64(%rdi), %eax
.L236:
	movq	%r9, %rdx
	leaq	-72(%rbp), %rsi
	movq	%r15, %r8
	movl	$1, %ecx
	movb	%al, -92(%rbp)
	movq	%r9, -88(%rbp)
	call	_ZL16changeState_2022P10UConverterPPKcS2_11Variant2022P10UErrorCode
	cmpq	$0, 104(%r13)
	movq	-88(%rbp), %r9
	movzbl	-92(%rbp), %eax
	je	.L254
	movq	-72(%rbp), %r12
.L255:
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L256
.L257:
	movl	104(%r13), %eax
	testl	%eax, %eax
	jne	.L237
	movb	$1, 112(%r13)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L242:
	movsbl	96(%r13), %eax
	movl	%eax, %esi
.L248:
	cltq
	leal	95(%r10), %ecx
	movb	$0, 112(%r13)
	movsbl	92(%r13,%rax), %eax
	cmpb	$62, %cl
	ja	.L261
	cmpl	$4, 108(%r13)
	je	.L349
.L262:
	cmpb	$8, %al
	ja	.L264
	leaq	.L306(%rip), %r11
	movzbl	%al, %esi
	movslq	(%r11,%rsi,4), %rcx
	addq	%r11, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L306:
	.long	.L270-.L306
	.long	.L269-.L306
	.long	.L268-.L306
	.long	.L267-.L306
	.long	.L264-.L306
	.long	.L264-.L306
	.long	.L264-.L306
	.long	.L264-.L306
	.long	.L253-.L306
	.text
	.p2align 4,,10
	.p2align 3
.L246:
	cmpl	$3, 108(%r13)
	je	.L350
	movb	$0, 112(%r13)
	movl	$14, %r10d
.L253:
	movq	8(%rbx), %rax
	movb	%r10b, 65(%rax)
	movb	$1, 64(%rax)
.L287:
	movl	$12, (%r15)
.L240:
	movq	%r14, 32(%rbx)
	movq	%rdi, 16(%rbx)
.L235:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movl	(%r15), %ecx
	movq	-72(%rbp), %rdx
	testl	%ecx, %ecx
	jle	.L352
	movq	%rdx, %r12
.L256:
	movq	%r14, 32(%rbx)
	movq	%r12, 16(%rbx)
	movb	$0, 112(%r13)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L347:
	cmpq	%r14, 40(%rbx)
	ja	.L353
.L238:
	movl	$15, (%r15)
	movq	-72(%rbp), %rdi
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L247:
	movzbl	92(%r13), %eax
	cmpb	$3, %al
	je	.L260
	testb	%al, %al
	je	.L260
	movb	$0, 92(%r13)
.L260:
	movb	$0, 94(%r13)
	xorl	%eax, %eax
	xorl	%esi, %esi
	movb	$0, 96(%r13)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L245:
	cmpl	$3, 108(%r13)
	je	.L354
	movb	$0, 112(%r13)
.L251:
	movq	8(%rbx), %rax
	movb	%r10b, 65(%rax)
	movb	$1, 64(%rax)
	.p2align 4,,10
	.p2align 3
.L298:
	movq	-72(%rbp), %rdi
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L261:
	cmpb	$8, %al
	ja	.L264
	leaq	.L266(%rip), %r11
	movzbl	%al, %esi
	movslq	(%r11,%rsi,4), %rcx
	addq	%r11, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L266:
	.long	.L270-.L266
	.long	.L269-.L266
	.long	.L268-.L266
	.long	.L267-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L264-.L266
	.long	.L265-.L266
	.text
	.p2align 4,,10
	.p2align 3
.L352:
	cmpb	$0, 112(%r13)
	je	.L313
	movq	%rdx, %rsi
	movq	8(%rbx), %rcx
	movl	$18, (%r15)
	subq	%r12, %rsi
	movq	%rdx, %r12
	movl	$2, 284(%rcx)
	addl	%esi, %eax
	movb	%al, 64(%rcx)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L349:
	movsbl	%al, %ecx
	subl	$4, %ecx
	cmpl	$3, %ecx
	jbe	.L262
	leal	65216(%rdx), %r8d
	cmpb	$1, %sil
	jle	.L263
	movzbl	97(%r13), %eax
	movb	%al, 96(%r13)
	jmp	.L263
.L267:
	cmpl	$127, %edx
	ja	.L253
	cmpl	$91, %edx
	jbe	.L263
	cmpl	$92, %edx
	je	.L315
	cmpl	$126, %edx
	je	.L355
	.p2align 4,,10
	.p2align 3
.L263:
	movq	48(%rbx), %rsi
	movl	%r8d, %eax
	testq	%rsi, %rsi
	jne	.L307
.L346:
	movq	%rdi, %r12
	movq	%r14, %rcx
.L289:
	movw	%ax, (%rcx)
	addq	$2, %r14
	jmp	.L237
.L264:
	cmpq	%r9, %rdi
	jnb	.L277
.L239:
	movzbl	(%rdi), %ecx
	leal	-33(%r10), %r8d
	leal	-33(%rcx), %esi
	cmpb	$93, %r8b
	ja	.L278
	cmpb	$93, %sil
	ja	.L341
	movl	%edx, %r8d
	addq	$1, %rdi
	movzbl	%cl, %esi
	sall	$8, %r8d
	movq	%rdi, -72(%rbp)
	orl	%esi, %r8d
	cmpl	$4, %eax
	je	.L356
	cmpl	$7, %eax
	leal	32896(%r8), %edx
	cmovne	%r8d, %edx
	rolw	$8, %dx
	movw	%dx, -58(%rbp)
.L284:
	cltq
	xorl	%ecx, %ecx
	leaq	-58(%rbp), %rsi
	movl	$2, %edx
	movq	0(%r13,%rax,8), %rdi
	movl	%r8d, -92(%rbp)
	movq	%r9, -88(%rbp)
	call	ucnv_MBCSSimpleGetNextUChar_67@PLT
	movq	-88(%rbp), %r9
	movl	-92(%rbp), %r8d
	movq	%r14, %rcx
.L276:
	cmpl	$65533, %eax
	jbe	.L357
	cmpl	$65535, %eax
	jbe	.L358
	subl	$65536, %eax
	movq	-72(%rbp), %r12
	movq	40(%rbx), %rdi
	addq	$2, %r14
	movl	%eax, %edx
	andw	$1023, %ax
	shrl	$10, %edx
	subw	$9216, %ax
	subw	$10240, %dx
	movw	%dx, -2(%r14)
	movq	48(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L294
	movq	%r12, %rsi
	subq	16(%rbx), %rsi
	cmpl	$255, %r8d
	ja	.L359
	movq	32(%rbx), %r8
	movq	%rcx, %r10
	leal	-1(%rsi), %r11d
	subq	%r8, %r10
	sarq	%r10
	movl	%r11d, (%rdx,%r10,4)
	cmpq	%rdi, %r14
	jnb	.L302
	movw	%ax, 2(%rcx)
	movl	$1, %eax
.L308:
	subq	%r8, %r14
	subl	%eax, %esi
	sarq	%r14
	movl	%esi, (%rdx,%r14,4)
.L300:
	leaq	4(%rcx), %r14
	jmp	.L237
.L270:
	cmpl	$127, %edx
	jbe	.L263
	jmp	.L253
.L268:
	movzbl	97(%r13), %ecx
	cmpl	$127, %edx
	jbe	.L272
	movb	%cl, 96(%r13)
	jmp	.L251
.L269:
	movzbl	97(%r13), %eax
	cmpl	$127, %edx
	jbe	.L271
	movb	%al, 96(%r13)
	jmp	.L253
.L265:
	leal	-33(%r10), %eax
	cmpb	$62, %al
	ja	.L253
	leal	65344(%rdx), %eax
	movq	%r14, %rcx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%rdx, %r12
	jmp	.L257
.L355:
	movl	$8254, %eax
.L275:
	movq	48(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L346
.L307:
	movl	%edi, %r10d
	movq	%rdi, %r12
	subl	16(%rbx), %r10d
	movq	%r14, %rcx
	movl	$1, %edi
.L290:
	movq	%r14, %rdx
	subq	32(%rbx), %rdx
	subl	%edi, %r10d
	sarq	%rdx
	movl	%r10d, (%rsi,%rdx,4)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L357:
	movq	48(%rbx), %rsi
	movq	-72(%rbp), %r12
	testq	%rsi, %rsi
	je	.L289
	movl	%r12d, %r10d
	subl	16(%rbx), %r10d
	cmpl	$256, %r8d
	sbbl	%edi, %edi
	addl	$2, %edi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L302:
	movq	8(%rbx), %rcx
	movsbq	93(%rcx), %rdx
	leal	1(%rdx), %esi
	movb	%sil, 93(%rcx)
	movw	%ax, 144(%rcx,%rdx,2)
	jmp	.L237
.L358:
	movq	8(%rbx), %rdx
	movl	%r8d, %r10d
	cmpl	$255, %r8d
	jbe	.L293
	rolw	$8, %r8w
	movb	$2, 64(%rdx)
	movw	%r8w, 65(%rdx)
.L297:
	cmpl	$65534, %eax
	jne	.L298
	movl	$10, (%r15)
	movq	-72(%rbp), %rdi
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L359:
	movq	32(%rbx), %r8
	movq	%rcx, %r10
	leal	-2(%rsi), %r11d
	subq	%r8, %r10
	sarq	%r10
	movl	%r11d, (%rdx,%r10,4)
	cmpq	%rdi, %r14
	jnb	.L302
	movw	%ax, 2(%rcx)
	movl	$2, %eax
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L294:
	cmpq	%rdi, %r14
	jnb	.L302
	movw	%ax, 2(%rcx)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L353:
	movb	$0, 64(%rdi)
	movzbl	65(%rdi), %edx
	movq	%r12, %rdi
	movsbq	96(%r13), %rax
	movl	%edx, %r10d
	movsbl	92(%r13,%rax), %eax
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L350:
	movb	$8, 93(%r13)
	movb	$1, 96(%r13)
.L250:
	cmpq	%r9, %rdi
	jnb	.L240
	movq	40(%rbx), %rax
	movq	%rdi, %r12
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L354:
	movb	$0, 96(%r13)
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L271:
	movb	%al, 96(%r13)
	leal	128(%rdx), %r8d
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L272:
	movq	16(%r13), %rax
	movq	56(%rax), %rdx
	leal	-128(%r10), %eax
	movzbl	%al, %eax
	movl	(%rdx,%rax,4), %edx
	movb	%cl, 96(%r13)
	movzwl	%dx, %eax
	cmpl	$65533, %eax
	jg	.L360
	movq	48(%rbx), %rsi
	movl	%edx, %eax
	testq	%rsi, %rsi
	je	.L346
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%r12, %rdi
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L278:
	cmpb	$93, %sil
	jbe	.L253
	.p2align 4,,10
	.p2align 3
.L341:
	cmpb	$31, %cl
	ja	.L286
	movl	$134266880, %eax
	btl	%ecx, %eax
	jc	.L253
.L286:
	sall	$8, %edx
	movq	8(%rbx), %rax
	addq	$1, %rdi
	orl	%edx, %ecx
	rolw	$8, %cx
	movb	$2, 64(%rax)
	movw	%cx, 65(%rax)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L277:
	movq	8(%rbx), %rax
	movb	%r10b, 65(%rax)
	movq	8(%rbx), %rax
	movb	$1, 64(%rax)
	jmp	.L240
.L356:
	leal	126(%rcx), %esi
	testb	$1, %r10b
	je	.L282
	addl	$1, %r10d
	cmpb	$95, %cl
	jbe	.L361
	leal	32(%rcx), %esi
	movzbl	%r10b, %edx
.L282:
	sarl	%edx
	movb	%sil, -57(%rbp)
	leal	-80(%rdx), %edi
	leal	112(%rdx), %ecx
	cmpl	$48, %edx
	movl	%edi, %edx
	cmovl	%ecx, %edx
	movb	%dl, -58(%rbp)
	jmp	.L284
.L315:
	movl	$165, %eax
	jmp	.L275
.L361:
	leal	31(%rcx), %esi
	movzbl	%r10b, %edx
	jmp	.L282
.L351:
	call	__stack_chk_fail@PLT
.L360:
	movq	8(%rbx), %rdx
.L293:
	movb	%r10b, 65(%rdx)
	movb	$1, 64(%rdx)
	jmp	.L297
	.cfi_endproc
.LFE2132:
	.size	_ZL46UConverter_toUnicode_ISO_2022_JP_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode, .-_ZL46UConverter_toUnicode_ISO_2022_JP_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode
	.p2align 4
	.type	_ZL46UConverter_toUnicode_ISO_2022_KR_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode, @function
_ZL46UConverter_toUnicode_ISO_2022_KR_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode:
.LFB2136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	8(%rdi), %r15
	movq	16(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %r9
	movq	24(%rdi), %r10
	movq	16(%r15), %r12
	movq	%r13, -136(%rbp)
	cmpl	$1, 108(%r12)
	je	.L456
	movq	80(%r12), %rax
	movl	104(%r12), %edx
	movq	48(%rax), %rax
	movq	%rax, -152(%rbp)
	movzbl	63(%r15), %eax
	movb	%al, -160(%rbp)
	testl	%edx, %edx
	jne	.L393
	cmpb	$1, 64(%r15)
	jne	.L394
	cmpq	%r10, %r13
	jb	.L457
.L394:
	cmpq	%r13, %r10
	jbe	.L458
	movq	40(%rbx), %rdx
.L398:
	cmpq	%rdx, %r9
	jnb	.L395
	leaq	1(%r13), %rax
	movq	%rax, -136(%rbp)
	movzbl	-1(%rax), %r8d
	cmpb	$15, %r8b
	je	.L459
	movzbl	%r8b, %r11d
	cmpb	$14, %r8b
	je	.L460
	cmpw	$27, %r11w
	je	.L461
	cmpb	$1, 96(%r12)
	movb	$0, 112(%r12)
	je	.L462
	cmpw	$127, %r11w
	jbe	.L463
.L409:
	movq	8(%rbx), %rax
	movb	$1, 64(%rax)
	movb	%r8b, 65(%rax)
.L418:
	movl	$12, (%r14)
	movq	-136(%rbp), %rax
.L397:
	movq	%r9, 32(%rbx)
	movq	%rax, 16(%rbx)
.L362:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L464
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	%r13, -136(%rbp)
.L393:
	movb	$0, 112(%r12)
	movq	8(%rbx), %rdi
	movq	%r10, %rdx
	movq	%r14, %r8
	leaq	-136(%rbp), %rsi
	movl	$2, %ecx
	movq	%r9, -176(%rbp)
	movq	%r10, -168(%rbp)
	call	_ZL16changeState_2022P10UConverterPPKcS2_11Variant2022P10UErrorCode
	movl	(%r14), %eax
	movq	-136(%rbp), %r13
	movq	-168(%rbp), %r10
	movq	-176(%rbp), %r9
	testl	%eax, %eax
	jle	.L394
	movq	-136(%rbp), %rax
	movq	%r9, 32(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L459:
	cmpb	$0, 112(%r12)
	movb	$0, 96(%r12)
	je	.L400
	movb	$0, 112(%r12)
	movq	8(%rbx), %rdx
	movl	$18, (%r14)
	movl	$2, 284(%rdx)
	movb	$15, 65(%rdx)
	movq	8(%rbx), %rdx
	movb	$1, 64(%rdx)
	movq	%r9, 32(%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L460:
	movb	$1, 96(%r12)
	movb	$1, 112(%r12)
.L400:
	cmpq	%r10, %rax
	jnb	.L397
	movq	40(%rbx), %rdx
	movq	%rax, %r13
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L457:
	cmpq	%r9, 40(%rdi)
	ja	.L465
.L395:
	movl	$15, (%r14)
	movq	-136(%rbp), %rax
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L463:
	movsbl	-160(%rbp), %ecx
	movq	-152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r10, -184(%rbp)
	movb	%r8b, -176(%rbp)
	movq	%r9, -168(%rbp)
	call	ucnv_MBCSSimpleGetNextUChar_67@PLT
	movq	-168(%rbp), %r9
	movzbl	-176(%rbp), %r8d
	cmpl	$65533, %eax
	movq	-184(%rbp), %r10
	jg	.L466
	movq	48(%rbx), %rsi
	movq	-136(%rbp), %r13
	movq	%r9, %rdi
	testq	%rsi, %rsi
	je	.L413
	movl	%r13d, %r8d
	movq	%r9, %rdi
	subl	16(%rbx), %r8d
	movl	$1, %ecx
.L414:
	movq	%r9, %rdx
	subq	32(%rbx), %rdx
	subl	%ecx, %r8d
	sarq	%rdx
	movl	%r8d, (%rsi,%rdx,4)
.L413:
	movw	%ax, (%rdi)
	addq	$2, %r9
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L456:
	movzwl	(%rdi), %r8d
	movzwl	%r8w, %edx
	cmpw	$55, %r8w
	jbe	.L364
	movl	$56, %edx
	movl	$56, %r8d
.L364:
	leaq	-128(%rbp), %rax
	movq	%rbx, %rsi
	movl	$56, %ecx
	movq	%r10, -160(%rbp)
	movq	%rax, %rdi
	movl	%r8d, -152(%rbp)
	movq	%rax, -168(%rbp)
	call	__memcpy_chk@PLT
	movl	104(%r12), %esi
	movl	-152(%rbp), %r8d
	movq	80(%r12), %rax
	movq	-160(%rbp), %r10
	testl	%esi, %esi
	movw	%r8w, -128(%rbp)
	movq	%rax, -120(%rbp)
	jne	.L365
.L392:
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L362
	movq	16(%rbx), %rdx
	movq	24(%rbx), %r10
	cmpq	%r10, %rdx
	jnb	.L362
	movq	%rdx, -112(%rbp)
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L367:
	cmpb	$27, (%rax)
	je	.L369
	addq	$1, %rax
	cmpq	%rax, %r10
	jne	.L367
	movq	%r10, -104(%rbp)
.L420:
	movq	8(%rbx), %rax
	movq	-120(%rbp), %r8
	movsbq	64(%rax), %rdx
	testb	%dl, %dl
	jg	.L467
.L372:
	movb	%dl, 64(%r8)
	movq	-168(%rbp), %rdi
	movq	%r14, %rsi
	call	ucnv_MBCSToUnicodeWithOffsets_67@PLT
	movq	48(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L377
	movq	16(%rbx), %rdi
	movq	-96(%rbp), %r8
	cmpq	%rdi, %r13
	je	.L378
	movq	32(%rbx), %rax
	subl	%r13d, %edi
	cmpq	%r8, %rax
	jnb	.L378
	leaq	-1(%r8), %rsi
	subq	%rax, %rsi
	xorl	%eax, %eax
	shrq	%rsi
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%rdx, %rax
.L380:
	movl	(%rcx,%rax,4), %edx
	testl	%edx, %edx
	js	.L379
	addl	%edi, %edx
	movl	%edx, (%rcx,%rax,4)
.L379:
	leaq	1(%rax), %rdx
	cmpq	%rax, %rsi
	jne	.L468
.L378:
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	%r8, 32(%rbx)
	movq	%rax, 16(%rbx)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%rbx)
	movzbl	64(%rdx), %ecx
	movq	8(%rbx), %rax
	testb	%cl, %cl
	jg	.L469
.L381:
	movb	%cl, 64(%rax)
	movl	(%r14), %eax
	cmpl	$15, %eax
	je	.L470
.L386:
	testl	%eax, %eax
	jg	.L362
	movq	16(%rbx), %rax
	movq	24(%rbx), %r10
.L371:
	cmpq	%rax, %r10
	je	.L362
	movq	8(%rbx), %r15
.L365:
	leaq	16(%rbx), %rsi
	movq	%r14, %r8
	movl	$2, %ecx
	movq	%r10, %rdx
	movq	%r15, %rdi
	call	_ZL16changeState_2022P10UConverterPPKcS2_11Variant2022P10UErrorCode
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L462:
	cmpq	%r10, %rax
	jb	.L396
	movq	8(%rbx), %rdx
	movb	%r8b, 65(%rdx)
	movq	8(%rbx), %rdx
	movb	$1, 64(%rdx)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L465:
	movzbl	65(%r15), %r11d
	movb	$0, 64(%r15)
	movq	%r13, %rax
	movl	%r11d, %r8d
.L396:
	movzbl	(%rax), %r13d
	leal	-33(%r8), %edx
	movq	%r10, -168(%rbp)
	leal	-33(%r13), %ecx
	cmpb	$93, %dl
	ja	.L406
	cmpb	$93, %cl
	jbe	.L471
.L446:
	cmpb	$31, %r13b
	ja	.L410
	movl	$134266880, %edx
	btl	%r13d, %edx
	jc	.L409
.L410:
	addq	$1, %rax
	sall	$8, %r11d
	movq	%rax, -136(%rbp)
	orl	%r11d, %r13d
	movl	$65535, %eax
.L408:
	movq	8(%rbx), %rdx
	movzwl	%r13w, %ecx
	movl	%r13d, %r8d
	cmpw	$255, %r13w
	jbe	.L416
	movb	%r13b, 66(%rdx)
	movzbl	%ch, %ecx
	movl	$2, %esi
.L417:
	movb	%sil, 64(%rdx)
	movb	%cl, 65(%rdx)
	cmpl	$65534, %eax
	jne	.L418
	movl	$10, (%r14)
	movq	-136(%rbp), %rax
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L471:
	addq	$1, %rax
	movsbl	-160(%rbp), %ecx
	addl	$-128, %r8d
	leaq	-58(%rbp), %rsi
	movq	-152(%rbp), %rdi
	movl	$2, %edx
	movq	%rax, -136(%rbp)
	leal	-128(%r13), %eax
	movl	%r11d, -184(%rbp)
	movq	%r9, -176(%rbp)
	movb	%r8b, -58(%rbp)
	movb	%al, -57(%rbp)
	call	ucnv_MBCSSimpleGetNextUChar_67@PLT
	movl	-184(%rbp), %r11d
	movq	-176(%rbp), %r9
	movq	-168(%rbp), %r10
	sall	$8, %r11d
	orl	%r13d, %r11d
	cmpl	$65533, %eax
	movl	%r11d, %r13d
	jg	.L408
	movq	48(%rbx), %rsi
	movq	-136(%rbp), %r13
	movq	%r9, %rdi
	testq	%rsi, %rsi
	je	.L413
	movl	%r13d, %r8d
	subl	16(%rbx), %r8d
	cmpw	$256, %r11w
	sbbl	%ecx, %ecx
	addl	$2, %ecx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L406:
	cmpb	$93, %cl
	jbe	.L409
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L458:
	movq	%r13, %rax
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%rax, -104(%rbp)
	cmpq	%rax, %rdx
	je	.L371
	jmp	.L420
.L469:
	movsbq	%cl, %r8
	leaq	65(%rax), %r10
	leaq	65(%rdx), %rsi
	cmpl	$8, %r8d
	jnb	.L382
	testb	$4, %r8b
	jne	.L472
	testl	%r8d, %r8d
	je	.L381
	movzbl	65(%rdx), %eax
	movb	%al, (%r10)
	testb	$2, %r8b
	jne	.L448
.L454:
	movq	8(%rbx), %rax
	movzbl	64(%rdx), %ecx
	jmp	.L381
.L467:
	leaq	65(%r8), %r9
	leaq	65(%rax), %rsi
	cmpl	$8, %edx
	jnb	.L373
	testb	$4, %dl
	jne	.L473
	testl	%edx, %edx
	je	.L374
	movzbl	65(%rax), %eax
	movb	%al, 65(%r8)
	testb	$2, %dl
	jne	.L447
.L453:
	movq	8(%rbx), %rax
.L374:
	movzbl	64(%rax), %edx
	jmp	.L372
.L470:
	movzbl	93(%rdx), %esi
	movq	8(%rbx), %rax
	testb	%sil, %sil
	jg	.L474
.L387:
	movb	%sil, 93(%rax)
	movb	$0, 93(%rdx)
	movl	(%r14), %eax
	jmp	.L386
.L377:
	movq	-96(%rbp), %r8
	jmp	.L378
.L373:
	movq	65(%rax), %rcx
	movq	%r9, %rax
	movq	%rcx, 65(%r8)
	movl	%edx, %ecx
	movq	-8(%rsi,%rcx), %rdi
	movq	%rdi, -8(%r9,%rcx)
	leaq	73(%r8), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rax
	addl	%eax, %edx
	subq	%rax, %rsi
	shrl	$3, %edx
	movl	%edx, %ecx
	rep movsq
	jmp	.L453
.L382:
	movq	65(%rdx), %rdi
	movq	%r10, %rcx
	movq	%rdi, 65(%rax)
	movl	%r8d, %edi
	movq	-8(%rsi,%rdi), %r9
	movq	%r9, -8(%r10,%rdi)
	leaq	73(%rax), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%r8d, %ecx
	shrl	$3, %ecx
	rep movsq
	jmp	.L454
.L474:
	movsbq	%sil, %rcx
	leaq	144(%rax), %r8
	leaq	144(%rdx), %r9
	cmpl	$8, %ecx
	jnb	.L388
	testb	$4, %cl
	jne	.L475
	testl	%ecx, %ecx
	je	.L387
	movzbl	144(%rdx), %eax
	movb	%al, (%r8)
	testb	$2, %cl
	jne	.L449
.L455:
	movq	8(%rbx), %rax
	movzbl	93(%rdx), %esi
	jmp	.L387
.L388:
	movq	144(%rdx), %rsi
	movl	%ecx, %edi
	movq	%rsi, 144(%rax)
	movq	-8(%r9,%rdi), %rsi
	movq	%rsi, -8(%r8,%rdi)
	leaq	152(%rax), %rdi
	movq	%r9, %rsi
	andq	$-8, %rdi
	subq	%rdi, %r8
	addl	%r8d, %ecx
	subq	%r8, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L455
.L473:
	movl	65(%rax), %eax
	movl	%edx, %edx
	movl	%eax, 65(%r8)
	movl	-4(%rsi,%rdx), %eax
	movl	%eax, -4(%r9,%rdx)
	movq	8(%rbx), %rax
	jmp	.L374
.L464:
	call	__stack_chk_fail@PLT
.L472:
	movl	65(%rdx), %eax
	movl	%r8d, %r8d
	movl	%eax, (%r10)
	movl	-4(%rsi,%r8), %eax
	movl	%eax, -4(%r10,%r8)
	movq	8(%rbx), %rax
	movzbl	64(%rdx), %ecx
	jmp	.L381
.L448:
	movl	%r8d, %r8d
	movzwl	-2(%rsi,%r8), %eax
	movw	%ax, -2(%r10,%r8)
	movq	8(%rbx), %rax
	movzbl	64(%rdx), %ecx
	jmp	.L381
.L447:
	movl	%edx, %edx
	movzwl	-2(%rsi,%rdx), %eax
	movw	%ax, -2(%r9,%rdx)
	movq	8(%rbx), %rax
	jmp	.L374
.L449:
	movl	%ecx, %eax
	movzwl	-2(%r9,%rax), %ecx
	movw	%cx, -2(%r8,%rax)
	movq	8(%rbx), %rax
	movzbl	93(%rdx), %esi
	jmp	.L387
.L475:
	movl	144(%rdx), %eax
	movl	%eax, (%r8)
	movl	%ecx, %eax
	movl	-4(%r9,%rax), %ecx
	movl	%ecx, -4(%r8,%rax)
	movq	8(%rbx), %rax
	movzbl	93(%rdx), %esi
	jmp	.L387
.L466:
	movq	8(%rbx), %rdx
.L416:
	movl	%r8d, %ecx
	movl	$1, %esi
	jmp	.L417
	.cfi_endproc
.LFE2136:
	.size	_ZL46UConverter_toUnicode_ISO_2022_KR_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode, .-_ZL46UConverter_toUnicode_ISO_2022_KR_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode
	.p2align 4
	.type	_ZL23_ISO_2022_GetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode, @function
_ZL23_ISO_2022_GetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode:
.LFB2141:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L476
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movzbl	143(%r12), %eax
	cmpb	$107, %al
	je	.L478
	jg	.L479
	cmpb	$99, %al
	je	.L480
	cmpb	$106, %al
	jne	.L482
	movq	(%r14), %rdi
	movl	$165, %esi
	call	*8(%r14)
	movq	(%r14), %rdi
	movl	$8254, %esi
	call	*8(%r14)
	movl	108(%r12), %edx
	leaq	_ZL14jpCharsetMasks(%rip), %rax
	testb	$2, (%rax,%rdx,2)
	je	.L483
	movq	(%r14), %rdi
	movl	$255, %edx
	xorl	%esi, %esi
	call	*16(%r14)
.L484:
	movl	108(%r12), %eax
	subl	$3, %eax
	cmpl	$1, %eax
	jbe	.L496
	cmpl	$1, %r13d
	je	.L496
	.p2align 4,,10
	.p2align 3
.L482:
	xorl	%ebx, %ebx
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L487:
	cmpb	$99, %al
	je	.L497
	cmpb	$122, %al
	jne	.L489
.L497:
	movl	108(%r12), %eax
	testl	%eax, %eax
	jne	.L489
	cmpl	$3, %ebx
	je	.L494
.L489:
	xorl	%ecx, %ecx
	cmpq	$7, %rbx
	sete	%cl
	sall	$2, %ecx
.L488:
	movq	%r15, %r8
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	ucnv_MBCSGetFilteredUnicodeSetForUnicode_67@PLT
.L486:
	addq	$1, %rbx
	cmpq	$10, %rbx
	je	.L521
.L492:
	movq	(%r12,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L486
	movzbl	143(%r12), %eax
	cmpb	$106, %al
	jne	.L487
	movl	$3, %ecx
	cmpq	$4, %rbx
	jne	.L489
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L479:
	cmpb	$122, %al
	jne	.L482
.L480:
	movq	(%r14), %rdi
	movl	$127, %edx
	xorl	%esi, %esi
	call	*16(%r14)
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L521:
	movq	(%r14), %rdi
	movl	$14, %esi
	call	*32(%r14)
	movq	(%r14), %rdi
	movl	$15, %esi
	call	*32(%r14)
	movq	(%r14), %rdi
	movl	$27, %esi
	call	*32(%r14)
	movq	40(%r14), %rax
	movq	(%r14), %rdi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	movl	$159, %edx
	popq	%r12
	.cfi_restore 12
	movl	$128, %esi
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L476:
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$2, %ecx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L478:
	movq	80(%r12), %rdi
	movq	48(%rdi), %rax
	movq	32(%rax), %rax
	call	*120(%rax)
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L483:
	movq	(%r14), %rdi
	movl	$127, %edx
	xorl	%esi, %esi
	call	*16(%r14)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L496:
	movq	(%r14), %rdi
	movl	$65439, %edx
	movl	$65377, %esi
	call	*16(%r14)
	jmp	.L482
	.cfi_endproc
.LFE2141:
	.size	_ZL23_ISO_2022_GetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode, .-_ZL23_ISO_2022_GetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode
	.p2align 4
	.type	_ISO_2022_SafeClone, @function
_ISO_2022_SafeClone:
.LFB2140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rcx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L522
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	je	.L538
	movq	16(%rdi), %rbx
	leaq	576(%rsi), %rax
	movq	%rsi, %r12
	movdqu	(%rbx), %xmm0
	movups	%xmm0, 576(%rsi)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 592(%rsi)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 608(%rsi)
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 624(%rsi)
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 640(%rsi)
	movdqu	80(%rbx), %xmm5
	movups	%xmm5, 656(%rsi)
	movdqu	96(%rbx), %xmm6
	movups	%xmm6, 672(%rsi)
	movdqu	112(%rbx), %xmm7
	movups	%xmm7, 688(%rsi)
	movdqu	128(%rbx), %xmm0
	movups	%xmm0, 704(%rsi)
	movq	144(%rbx), %rdx
	movq	%rax, 16(%rsi)
	movq	%rdx, 720(%rsi)
	movb	$1, 62(%rsi)
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L528
	leaq	-44(%rbp), %rdx
	leaq	288(%rsi), %rsi
	movq	%rcx, %r13
	movl	$288, -44(%rbp)
	call	ucnv_safeClone_67@PLT
	movq	%rax, 656(%r12)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L522
.L528:
	leaq	80(%rbx), %r13
	.p2align 4,,10
	.p2align 3
.L526:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L529
	call	ucnv_incrementRefCount_67@PLT
.L529:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L526
	movq	%r12, %r14
.L522:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L539
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movl	$728, (%rdx)
	jmp	.L522
.L539:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2140:
	.size	_ISO_2022_SafeClone, .-_ISO_2022_SafeClone
	.p2align 4
	.type	_ZL13_ISO2022CloseP10UConverter, @function
_ZL13_ISO2022CloseP10UConverter:
.LFB2117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %r14
	testq	%r14, %r14
	je	.L540
	movq	%rdi, %r13
	movq	%r14, %rbx
	leaq	80(%r14), %r12
	.p2align 4,,10
	.p2align 3
.L544:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L543
	call	ucnv_unloadSharedDataIfReady_67@PLT
.L543:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L544
	movq	80(%r14), %rdi
	call	ucnv_close_67@PLT
	cmpb	$0, 62(%r13)
	je	.L552
.L540:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	movq	16(%r13), %rdi
	call	uprv_free_67@PLT
	movq	$0, 16(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2117:
	.size	_ZL13_ISO2022CloseP10UConverter, .-_ZL13_ISO2022CloseP10UConverter
	.p2align 4
	.type	_ZL48UConverter_fromUnicode_ISO_2022_KR_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode, @function
_ZL48UConverter_fromUnicode_ISO_2022_KR_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode:
.LFB2134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	16(%rdi), %r12
	movq	%rsi, -96(%rbp)
	movq	24(%rdi), %r11
	movq	32(%rdi), %rbx
	movq	16(%r14), %r15
	movq	40(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movl	$0, -76(%rbp)
	cmpl	$1, 108(%r15)
	movl	84(%r14), %r10d
	movq	%rax, -72(%rbp)
	movq	80(%r15), %rax
	je	.L648
	movl	80(%r14), %edx
	movq	%r14, %rdi
	movsbl	%dl, %r9d
	testl	%r10d, %r10d
	je	.L611
	cmpq	%r8, %rbx
	jb	.L562
.L611:
	cmpq	%r11, %r12
	jnb	.L564
	movq	48(%rax), %r15
	movzbl	63(%r14), %eax
	movb	%al, -81(%rbp)
	movq	%r8, %rax
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L652:
	cmpq	%rbx, %r8
	jbe	.L586
	movb	%al, (%rbx)
	movq	-72(%rbp), %rcx
	addq	$1, %rbx
	testq	%rcx, %rcx
	je	.L587
.L643:
	movq	%r12, %rax
	subq	16(%r13), %rax
	leaq	4(%rcx), %rsi
	sarq	%rax
	movq	%rsi, -72(%rbp)
	subl	$1, %eax
	movl	%eax, (%rcx)
.L587:
	cmpq	%r12, %r11
	jbe	.L649
.L566:
	movq	40(%r13), %rax
	movsbl	%dl, %r9d
.L565:
	movl	$65535, -76(%rbp)
	cmpq	%rax, %rbx
	jnb	.L568
	movzwl	(%r12), %esi
	addq	$2, %r12
	movl	%esi, %r14d
	movl	%esi, %r10d
	cmpl	$31, %esi
	jg	.L569
	movl	$134266880, %eax
	btl	%esi, %eax
	jc	.L592
.L569:
	movl	%esi, %eax
	movq	88(%r15), %rcx
	movq	232(%r15), %rdi
	sarl	$10, %eax
	cltq
	movzwl	(%rcx,%rax,2), %edx
	movl	%esi, %eax
	sarl	$4, %eax
	andl	$63, %eax
	addl	%edx, %eax
	movl	%r14d, %edx
	cltq
	andl	$15, %edx
	movl	(%rcx,%rax,4), %ecx
	movzwl	%cx, %eax
	sall	$4, %eax
	addl	%edx, %eax
	movzwl	(%rdi,%rax,2), %eax
	xorl	%edi, %edi
	cmpl	$255, %eax
	seta	%dil
	addl	$16, %edx
	addl	$1, %edi
	btl	%edx, %ecx
	jc	.L644
	cmpb	$0, -81(%rbp)
	jne	.L574
	leal	-57344(%rsi), %edx
	cmpl	$6399, %edx
	ja	.L575
.L574:
	testl	%eax, %eax
	jne	.L644
.L575:
	movq	288(%r15), %rdi
	testq	%rdi, %rdi
	je	.L578
	movsbl	-81(%rbp), %ecx
	leaq	-76(%rbp), %rdx
	movl	%r10d, -116(%rbp)
	movb	%r9b, -82(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r11, -104(%rbp)
	movl	%esi, -88(%rbp)
	call	ucnv_extSimpleMatchFromU_67@PLT
	movl	-88(%rbp), %esi
	movq	-104(%rbp), %r11
	cltd
	movq	-112(%rbp), %r8
	movsbl	-82(%rbp), %r9d
	movl	%edx, %edi
	movl	-116(%rbp), %r10d
	xorl	%eax, %edi
	subl	%edx, %edi
	cmpl	$2, %edi
	jg	.L578
	testl	%eax, %eax
	je	.L578
	movl	-76(%rbp), %eax
.L603:
	cmpl	$1, %edi
	je	.L650
	cmpl	$2, %edi
	je	.L651
.L581:
	cmpl	$65535, %eax
	je	.L577
.L580:
	cmpl	$255, %eax
	seta	%dl
	cmpb	%r9b, %dl
	je	.L582
	cmpl	$256, %eax
	leaq	1(%rbx), %rcx
	setb	%al
	addl	$14, %eax
	movb	%al, (%rbx)
	movq	-72(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L645
	movq	%r12, %rax
	subq	16(%r13), %rax
	leaq	4(%rsi), %rdi
	sarq	%rax
	movq	%rdi, -72(%rbp)
	subl	$1, %eax
	movl	%eax, (%rsi)
.L645:
	movl	-76(%rbp), %eax
	movq	%rcx, %rbx
.L582:
	cmpl	$255, %eax
	jbe	.L652
	movl	%eax, %ecx
	shrl	$8, %ecx
	leal	-128(%rcx), %esi
	cmpq	%rbx, %r8
	jbe	.L588
	movb	%sil, (%rbx)
	movq	-72(%rbp), %rsi
	leaq	1(%rbx), %rcx
	testq	%rsi, %rsi
	je	.L589
	movq	%r12, %rax
	subq	16(%r13), %rax
	leaq	4(%rsi), %rdi
	sarq	%rax
	movq	%rdi, -72(%rbp)
	subl	$1, %eax
	movl	%eax, (%rsi)
.L589:
	movzbl	-76(%rbp), %eax
	addl	$-128, %eax
	cmpq	%r8, %rcx
	jnb	.L590
	movb	%al, 1(%rbx)
	movq	-72(%rbp), %rcx
	addq	$2, %rbx
	testq	%rcx, %rcx
	jne	.L643
	cmpq	%r12, %r11
	ja	.L566
	.p2align 4,,10
	.p2align 3
.L649:
	movq	-96(%rbp), %rax
	movq	8(%r13), %rdi
	movl	(%rax), %eax
.L567:
	testl	%eax, %eax
	jg	.L646
.L596:
	xorl	%r9d, %r9d
	testb	%dl, %dl
	je	.L597
	cmpb	$0, 2(%r13)
	je	.L646
	cmpq	%r12, %r11
	jbe	.L598
.L646:
	movsbl	%dl, %r9d
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L650:
	cmpl	$127, %eax
	jbe	.L580
	.p2align 4,,10
	.p2align 3
.L578:
	movl	$65535, -76(%rbp)
.L577:
	movl	%esi, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L653
	movq	-96(%rbp), %rax
	movq	8(%r13), %rdi
	movl	$10, (%rax)
	movl	%esi, 84(%rdi)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L644:
	movl	%eax, -76(%rbp)
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L588:
	movq	8(%r13), %rdi
	addl	$-128, %eax
	movsbq	91(%rdi), %rcx
	leal	1(%rcx), %r9d
	movb	%r9b, 91(%rdi)
	movb	%sil, 104(%rdi,%rcx)
	movq	8(%r13), %rsi
	movsbq	91(%rsi), %rcx
	leal	1(%rcx), %edi
	movb	%dil, 91(%rsi)
	movb	%al, 104(%rsi,%rcx)
	movq	-96(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L586:
	movq	8(%r13), %rsi
	movsbq	91(%rsi), %rcx
	leal	1(%rcx), %edi
	movb	%dil, 91(%rsi)
	movb	%al, 104(%rsi,%rcx)
	movq	-96(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L651:
	leal	24159(%rax), %edx
	cmpw	$23901, %dx
	ja	.L578
	leal	95(%rax), %edx
	cmpb	$93, %dl
	ja	.L578
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L590:
	movq	8(%r13), %rdi
	movq	%rcx, %rbx
	movsbq	91(%rdi), %rsi
	leal	1(%rsi), %r9d
	movb	%r9b, 91(%rdi)
	movb	%al, 104(%rdi,%rsi)
	movq	-96(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L568:
	movq	-96(%rbp), %rax
	movq	8(%r13), %rdi
	movl	$15, (%rax)
.L597:
	movq	%r12, 16(%r13)
	movq	%rbx, 32(%r13)
	movl	%r9d, 80(%rdi)
.L553:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L654
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L653:
	.cfi_restore_state
	testw	$1024, %r14w
	jne	.L592
	movq	8(%r13), %r14
	.p2align 4,,10
	.p2align 3
.L562:
	cmpq	%r11, %r12
	jnb	.L593
	movzwl	(%r12), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L655
	movq	-96(%rbp), %rax
	movq	%r14, %rdi
	movl	$12, (%rax)
	movl	%r10d, 84(%r14)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L592:
	movq	-96(%rbp), %rax
	movq	8(%r13), %rdi
	movl	$12, (%rax)
	movl	%esi, 84(%rdi)
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L648:
	movq	%rax, 8(%rdi)
	movq	%rsi, %rbx
	movl	%r10d, 84(%rax)
	call	ucnv_MBCSFromUnicodeWithOffsets_67@PLT
	movq	80(%r15), %rax
	cmpl	$15, (%rbx)
	movl	84(%rax), %edx
	movl	%edx, 84(%r14)
	je	.L656
.L555:
	movq	%r14, 8(%r13)
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L593:
	movq	-96(%rbp), %rax
	movq	%r14, %rdi
	movl	%r9d, %edx
	movl	$0, (%rax)
	movl	%r10d, 84(%r14)
	jmp	.L596
.L598:
	movl	84(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L646
	movq	16(%r13), %rax
	movq	%r12, %rdx
	movl	$-1, %esi
	subq	%rax, %rdx
	sarq	%rdx
	testl	%edx, %edx
	jle	.L601
	leal	-1(%rdx), %esi
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx), %r9
	movzwl	(%rax,%rcx,2), %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L657
.L601:
	pushq	-96(%rbp)
	leaq	-72(%rbp), %r9
	movl	$1, %edx
	leaq	-64(%rbp), %rcx
	pushq	%rsi
	leaq	_ZL12SHIFT_IN_STR(%rip), %rsi
	movq	%rbx, -64(%rbp)
	call	ucnv_fromUWriteBytes_67@PLT
	popq	%rax
	movq	-64(%rbp), %rbx
	xorl	%r9d, %r9d
	movq	8(%r13), %rdi
	popq	%rdx
	jmp	.L597
.L656:
	movsbq	91(%rax), %rdx
	testb	%dl, %dl
	jg	.L658
.L556:
	movb	%dl, 91(%r14)
	movq	80(%r15), %rax
	movb	$0, 91(%rax)
	jmp	.L555
.L655:
	movq	-96(%rbp), %rdi
	sall	$10, %r10d
	addq	$2, %r12
	leal	-56613888(%rax,%r10), %eax
	movl	$10, (%rdi)
	movq	%r14, %rdi
	movl	%eax, 84(%r14)
	jmp	.L597
.L564:
	movq	-96(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L567
.L658:
	leaq	104(%r14), %rcx
	leaq	104(%rax), %rsi
	cmpl	$8, %edx
	jnb	.L557
	testb	$4, %dl
	jne	.L659
	testl	%edx, %edx
	je	.L558
	movzbl	104(%rax), %eax
	movb	%al, (%rcx)
	testb	$2, %dl
	jne	.L639
.L642:
	movq	80(%r15), %rax
.L558:
	movzbl	91(%rax), %edx
	jmp	.L556
.L557:
	movq	104(%rax), %rax
	movq	%rax, 104(%r14)
	movl	%edx, %eax
	movq	-8(%rsi,%rax), %rdi
	movq	%rdi, -8(%rcx,%rax)
	leaq	112(%r14), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %edx
	subq	%rcx, %rsi
	movl	%edx, %ecx
	shrl	$3, %ecx
	rep movsq
	jmp	.L642
.L657:
	testl	%esi, %esi
	je	.L602
	movzwl	-2(%rax,%r9), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L601
.L602:
	leal	-2(%rdx), %esi
	jmp	.L601
.L654:
	call	__stack_chk_fail@PLT
.L659:
	movl	104(%rax), %eax
	movl	%edx, %edx
	movl	%eax, (%rcx)
	movl	-4(%rsi,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	movq	80(%r15), %rax
	jmp	.L558
.L639:
	movl	%edx, %edx
	movzwl	-2(%rsi,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	movq	80(%r15), %rax
	jmp	.L558
	.cfi_endproc
.LFE2134:
	.size	_ZL48UConverter_fromUnicode_ISO_2022_KR_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode, .-_ZL48UConverter_fromUnicode_ISO_2022_KR_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ibm-949"
.LC1:
	.string	"icu-internal-25546"
.LC2:
	.string	"ISO8859_7"
.LC3:
	.string	"Shift-JIS"
.LC4:
	.string	"jisx-212"
.LC5:
	.string	"ibm-5478"
.LC6:
	.string	"ksc_5601"
.LC7:
	.string	"iso-ir-165"
.LC8:
	.string	"cns-11643-1992"
	.text
	.p2align 4
	.type	_ZL12_ISO2022OpenP10UConverterP18UConverterLoadArgsP10UErrorCode, @function
_ZL12_ISO2022OpenP10UConverterP18UConverterLoadArgsP10UErrorCode:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8224, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$152, %edi
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$538976288, -63(%rbp)
	movw	%r10w, -59(%rbp)
	movb	$0, -57(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L661
	movq	%rax, %rbx
	movzbl	8(%r14), %eax
	pxor	%xmm0, %xmm0
	movq	$0, -304(%rbp)
	leaq	8(%rbx), %rdi
	movq	%rbx, %rcx
	movaps	%xmm0, -336(%rbp)
	andq	$-8, %rdi
	movb	%al, -328(%rbp)
	xorl	%eax, %eax
	subq	%rdi, %rcx
	movaps	%xmm0, -320(%rbp)
	movl	$40, -336(%rbp)
	addl	$152, %ecx
	shrl	$3, %ecx
	movq	$0, (%rbx)
	movq	$0, 144(%rbx)
	rep stosq
	movl	$0, 88(%rbx)
	movl	$0, 80(%r12)
	movq	32(%r14), %rsi
	testq	%rsi, %rsi
	je	.L759
	movl	$6, %edx
	leaq	-63(%rbp), %rdi
	call	strncpy@PLT
	movl	12(%r14), %edx
	movzbl	-63(%rbp), %eax
	movl	%edx, %r15d
	andl	$15, %r15d
	movl	%r15d, 108(%rbx)
	cmpb	$106, %al
	jne	.L664
	movzbl	-62(%rbp), %eax
	cmpb	$97, %al
	je	.L707
	cmpb	$112, %al
	je	.L707
	.p2align 4,,10
	.p2align 3
.L663:
	movl	$2, 0(%r13)
.L660:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L760
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	cmpb	$107, %al
	je	.L761
	cmpb	$122, %al
	je	.L762
	cmpb	$99, %al
	jne	.L663
	cmpb	$110, -62(%rbp)
	jne	.L663
.L691:
	movzbl	-61(%rbp), %eax
	testb	%al, %al
	je	.L711
	cmpb	$95, %al
	jne	.L663
.L711:
	cmpl	$2, %r15d
	ja	.L663
	leaq	-336(%rbp), %rdx
	leaq	-288(%rbp), %rsi
	movq	%r13, %rcx
	leaq	.LC5(%rip), %rdi
	movq	%rdx, -352(%rbp)
	movq	%rsi, -344(%rbp)
	call	ucnv_loadSharedData_67@PLT
	cmpl	$1, %r15d
	movq	-344(%rbp), %rsi
	movq	-352(%rbp), %rdx
	movq	%rax, 8(%rbx)
	leaq	143(%rbx), %rax
	leaq	113(%rbx), %r8
	movq	%rax, -360(%rbp)
	je	.L763
	movq	%r13, %rcx
	leaq	.LC8(%rip), %rdi
	movq	%r8, -344(%rbp)
	call	ucnv_loadSharedData_67@PLT
	testl	%r15d, %r15d
	movb	$0, 145(%rbx)
	movq	-344(%rbp), %r8
	movq	%rax, 24(%rbx)
	leaq	_ZN12_GLOBAL__N_1L14_ISO2022CNDataE(%rip), %rax
	movq	%rax, 48(%r12)
	movl	$28259, %eax
	movw	%ax, 143(%rbx)
	je	.L764
	movdqa	.LC9(%rip), %xmm0
	movl	$2, 108(%rbx)
	movabsq	$7598542776404043898, %rax
	movups	%xmm0, 113(%rbx)
	movq	%rax, 16(%r8)
	movl	$8, %eax
	movl	$842886767, 24(%r8)
	movb	$0, 28(%r8)
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L761:
	movzbl	-62(%rbp), %eax
	cmpb	$111, %al
	je	.L709
	cmpb	$114, %al
	jne	.L663
.L709:
	movzbl	-61(%rbp), %eax
	testb	%al, %al
	je	.L710
	cmpb	$95, %al
	jne	.L663
.L710:
	andl	$14, %edx
	jne	.L663
	leaq	.LC1(%rip), %rdi
	cmpl	$1, %r15d
	je	.L677
	movl	$0, 108(%rbx)
	xorl	%r15d, %r15d
	leaq	.LC0(%rip), %rdi
.L677:
	cmpb	$0, 8(%r14)
	movq	%r13, %rsi
	jne	.L765
	call	ucnv_open_67@PLT
	movl	0(%r13), %edi
	movq	%rax, 80(%rbx)
	testl	%edi, %edi
	jg	.L766
	movdqa	.LC9(%rip), %xmm0
	leaq	113(%rbx), %rdx
	movabsq	$7598542776404045675, %rsi
	movups	%xmm0, 113(%rbx)
	movq	%rsi, 16(%rdx)
	cmpl	$1, %r15d
	je	.L767
	movl	$809332335, 24(%rdx)
	movb	$0, 28(%rdx)
.L684:
	movl	108(%rbx), %edx
	cmpl	$1, %edx
	je	.L768
	cmpb	$0, 91(%r12)
	jne	.L689
.L703:
	movb	$4, 91(%r12)
	movl	$1126769691, 104(%r12)
.L687:
	cmpl	$1, %edx
	jne	.L689
	movq	80(%rbx), %rax
	movq	$1, 80(%rax)
.L689:
	leaq	_ZN12_GLOBAL__N_1L14_ISO2022KRDataE(%rip), %rax
	movl	$28523, %esi
	movb	$0, 145(%rbx)
	movq	%rax, 48(%r12)
	movl	$8, %eax
	movw	%si, 143(%rbx)
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L762:
	cmpb	$104, -62(%rbp)
	jne	.L663
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L759:
	movl	12(%r14), %eax
	andl	$15, %eax
	movl	%eax, 108(%rbx)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L707:
	movzbl	-61(%rbp), %eax
	cmpb	$95, %al
	je	.L708
	testb	%al, %al
	jne	.L663
.L708:
	cmpl	$4, %r15d
	ja	.L663
	movl	%r15d, %r8d
	leaq	_ZL14jpCharsetMasks(%rip), %rax
	leaq	-336(%rbp), %rdx
	movzwl	(%rax,%r8,2), %r15d
	leaq	-288(%rbp), %rsi
	testb	$4, %r15b
	jne	.L769
.L669:
	movq	%r13, %rcx
	leaq	.LC3(%rip), %rdi
	movq	%rdx, -352(%rbp)
	movq	%rsi, -344(%rbp)
	call	ucnv_loadSharedData_67@PLT
	testb	$32, %r15b
	movq	-344(%rbp), %rsi
	movq	-352(%rbp), %rdx
	movq	%rax, 32(%rbx)
	jne	.L770
.L670:
	testb	$64, %r15b
	jne	.L771
.L671:
	andl	$128, %r15d
	jne	.L772
.L672:
	leaq	_ZN12_GLOBAL__N_1L14_ISO2022JPDataE(%rip), %rax
	movl	$24938, %r8d
	movabsq	$7598542776404042090, %rdx
	movl	$28271, %r9d
	movq	%rax, 48(%r12)
	movzbl	108(%rbx), %eax
	movdqa	.LC9(%rip), %xmm0
	movw	%r8w, 143(%rbx)
	movb	$0, 145(%rbx)
	addl	$48, %eax
	movq	%rdx, 129(%rbx)
	movw	%r9w, 137(%rbx)
	movb	$61, 139(%rbx)
	movb	$0, 141(%rbx)
	movb	%al, 140(%rbx)
	movl	$6, %eax
	movups	%xmm0, 113(%rbx)
.L673:
	movb	%al, 88(%r12)
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L694
	cmpb	$0, 8(%r14)
	je	.L660
.L694:
	movq	16(%r12), %r14
	testq	%r14, %r14
	je	.L660
	movq	%r14, %rbx
	leaq	80(%r14), %r13
	.p2align 4,,10
	.p2align 3
.L698:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L697
	call	ucnv_unloadSharedDataIfReady_67@PLT
.L697:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L698
.L758:
	movq	80(%r14), %rdi
	call	ucnv_close_67@PLT
	cmpb	$0, 62(%r12)
	jne	.L660
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	movq	$0, 16(%r12)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L764:
	movdqa	.LC9(%rip), %xmm0
	movl	$0, 108(%rbx)
	movabsq	$7598542776404043898, %rax
	movups	%xmm0, 113(%rbx)
	movq	%rax, 16(%r8)
	movl	$8, %eax
	movl	$809332335, 24(%r8)
	movb	$0, 28(%r8)
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L765:
	call	ucnv_canCreateConverter_67@PLT
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	movq	$0, 16(%r12)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L772:
	movq	%r13, %rcx
	leaq	.LC6(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	%rax, 56(%rbx)
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L771:
	movq	%r13, %rcx
	leaq	.LC5(%rip), %rdi
	movq	%rdx, -352(%rbp)
	movq	%rsi, -344(%rbp)
	call	ucnv_loadSharedData_67@PLT
	movq	-352(%rbp), %rdx
	movq	-344(%rbp), %rsi
	movq	%rax, 48(%rbx)
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L770:
	movq	%r13, %rcx
	leaq	.LC4(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	-352(%rbp), %rdx
	movq	-344(%rbp), %rsi
	movq	%rax, 40(%rbx)
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L769:
	movq	%r13, %rcx
	leaq	.LC2(%rip), %rdi
	movq	%rdx, -352(%rbp)
	movq	%rsi, -344(%rbp)
	call	ucnv_loadSharedData_67@PLT
	movq	-352(%rbp), %rdx
	movq	-344(%rbp), %rsi
	movq	%rax, 16(%rbx)
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L763:
	movq	%r13, %rcx
	leaq	.LC7(%rip), %rdi
	movq	%r8, -360(%rbp)
	call	ucnv_loadSharedData_67@PLT
	movq	-352(%rbp), %rdx
	movq	%r13, %rcx
	movq	-344(%rbp), %rsi
	movq	%rax, 16(%rbx)
	leaq	.LC8(%rip), %rdi
	call	ucnv_loadSharedData_67@PLT
	movq	-360(%rbp), %r8
	movdqa	.LC9(%rip), %xmm0
	movl	$28259, %ecx
	movq	%rax, 24(%rbx)
	leaq	_ZN12_GLOBAL__N_1L14_ISO2022CNDataE(%rip), %rax
	movq	%rax, 48(%r12)
	movabsq	$7598542776404043898, %rax
	movw	%cx, 143(%rbx)
	movb	$0, 145(%rbx)
	movl	$1, 108(%rbx)
	movups	%xmm0, 113(%rbx)
	movq	%rax, 16(%r8)
	movl	$8, %eax
	movl	$826109551, 24(%r8)
	movb	$0, 28(%r8)
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L766:
	movq	16(%r12), %r14
	testq	%r14, %r14
	je	.L660
	movq	%r14, %rbx
	leaq	80(%r14), %r13
	.p2align 4,,10
	.p2align 3
.L682:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L681
	call	ucnv_unloadSharedDataIfReady_67@PLT
.L681:
	addq	$8, %rbx
	cmpq	%r13, %rbx
	jne	.L682
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L768:
	movq	$0, 72(%rax)
	movb	$0, 64(%rax)
	cmpb	$0, 91(%r12)
	movl	108(%rbx), %edx
	je	.L703
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L767:
	movl	$826109551, 24(%rdx)
	movb	$0, 28(%rdx)
	movq	40(%rax), %rax
	movq	40(%r12), %rdx
	movl	(%rax), %eax
	movl	%eax, (%rdx)
	movq	80(%rbx), %rax
	movzbl	89(%rax), %edx
	movb	%dl, 89(%r12)
	jmp	.L684
.L661:
	movl	$7, 0(%r13)
	jmp	.L660
.L760:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2116:
	.size	_ZL12_ISO2022OpenP10UConverterP18UConverterLoadArgsP10UErrorCode, .-_ZL12_ISO2022OpenP10UConverterP18UConverterLoadArgsP10UErrorCode
	.p2align 4
	.type	_ZL48UConverter_fromUnicode_ISO_2022_JP_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode, @function
_ZL48UConverter_fromUnicode_ISO_2022_JP_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode:
.LFB2131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	8(%rdi), %r13
	movq	48(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %r15
	movq	40(%rdi), %r8
	movl	84(%r13), %r14d
	movq	16(%rdi), %rax
	movq	%rdx, -96(%rbp)
	movq	24(%rdi), %r10
	movq	16(%r13), %r11
	testl	%r14d, %r14d
	je	.L887
	cmpq	%r8, %r15
	jb	.L1013
	cmpq	%r10, %rax
	jb	.L778
.L777:
	movl	(%r12), %r9d
	testl	%r9d, %r9d
	jg	.L873
.L879:
	movzbl	102(%r11), %edx
	testb	%dl, %dl
	je	.L1014
.L874:
	cmpb	$0, 2(%rbx)
	je	.L873
	cmpq	%rax, %r10
	ja	.L873
	movl	84(%r13), %r10d
	testl	%r10d, %r10d
	jne	.L873
	testb	%dl, %dl
	je	.L909
	movb	$15, -74(%rbp)
	movl	$4, %ecx
	movl	$1, %edx
	movl	$1, %r10d
	movb	$0, 102(%r11)
.L875:
	cmpb	$0, 98(%r11)
	leaq	-74(%rbp), %rsi
	jne	.L1015
.L876:
	movq	16(%rbx), %r11
	movq	%rax, %rdx
	movl	$-1, %edi
	subq	%r11, %rdx
	sarq	%rdx
	testl	%edx, %edx
	jle	.L877
	leal	-1(%rdx), %edi
	movslq	%edi, %rcx
	leaq	(%rcx,%rcx), %r9
	movzwl	(%r11,%rcx,2), %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L1016
.L877:
	pushq	%r12
	movl	%r10d, %edx
	leaq	-88(%rbp), %rcx
	leaq	-96(%rbp), %r9
	pushq	%rdi
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	movq	%r15, -88(%rbp)
	call	ucnv_fromUWriteBytes_67@PLT
	popq	%rax
	movq	-88(%rbp), %r15
	movq	-104(%rbp), %rax
	popq	%rdx
	.p2align 4,,10
	.p2align 3
.L873:
	movq	%rax, 16(%rbx)
	movq	%r15, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1017
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	movl	$0, -104(%rbp)
.L774:
	cmpq	%r10, %rax
	jnb	.L777
.L880:
	cmpq	%r15, %r8
	jbe	.L778
	leaq	2(%rax), %rcx
	movq	%rcx, -120(%rbp)
	movzwl	(%rax), %ecx
	movl	%ecx, %edx
	movl	%ecx, -112(%rbp)
	andl	$63488, %edx
	cmpl	$55296, %edx
	je	.L1018
.L779:
	movl	-112(%rbp), %ecx
	cmpl	$31, %ecx
	jg	.L784
	movl	$134266880, %eax
	btl	%ecx, %eax
	jnc	.L784
	movl	$12, (%r12)
	movq	-120(%rbp), %rax
	movl	%ecx, 84(%r13)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L778:
	movl	$15, (%r12)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1013:
	movl	$0, -104(%rbp)
	xorl	%edx, %edx
.L775:
	cmpq	%r10, %rax
	jnb	.L781
	movzwl	(%rax), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	je	.L1019
	movl	$12, (%r12)
	movl	%r14d, 84(%r13)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1014:
	cmpb	$0, 98(%r11)
	je	.L873
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L1018:
	testb	$4, %ch
	jne	.L780
	movl	%r14d, %edx
	movq	-120(%rbp), %rax
	movl	%ecx, %r14d
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L781:
	movl	%r14d, 84(%r13)
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L780:
	movl	$12, (%r12)
	movq	-120(%rbp), %rax
	movl	%ecx, 84(%r13)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1019:
	addq	$2, %rax
	sall	$10, %r14d
	movl	$0, 84(%r13)
	movq	%rax, -120(%rbp)
	leal	-56613888(%rcx,%r14), %eax
	movl	%edx, %r14d
	movl	%eax, -112(%rbp)
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L784:
	testl	%r14d, %r14d
	jne	.L785
	movl	108(%r11), %ecx
	movl	$2, %edi
	movl	$1, %esi
	leaq	_ZL14jpCharsetMasks(%rip), %rdx
	movq	%rcx, %rax
	movzwl	(%rdx,%rcx,2), %r9d
	subl	$3, %eax
	cmpl	$1, %eax
	ja	.L786
	movb	$8, -66(%rbp)
	movl	$3, %edi
	movl	$2, %esi
	movl	$1, %r14d
.L786:
	movzbl	98(%r11), %ecx
	movl	$1, %edx
	movslq	%r14d, %r14
	movl	%edx, %eax
	movb	%cl, -66(%rbp,%r14)
	sall	%cl, %eax
	movl	%r9d, %ecx
	movl	%esi, %r14d
	andb	$-2, %ch
	notl	%eax
	andl	%ecx, %eax
	movzbl	100(%r11), %ecx
	testb	%cl, %cl
	je	.L788
	sall	%cl, %edx
	movb	%cl, -66(%rbp,%rsi)
	movl	%edi, %r14d
	notl	%edx
	andl	%edx, %eax
.L788:
	testb	$1, %al
	je	.L789
	movslq	%r14d, %rdx
	andl	$-2, %eax
	addl	$1, %r14d
	movb	$0, -66(%rbp,%rdx)
.L789:
	movzwl	%ax, %edx
	testb	$8, %al
	je	.L790
	movslq	%r14d, %rdx
	andl	$-9, %eax
	addl	$1, %r14d
	movb	$3, -66(%rbp,%rdx)
	movzwl	%ax, %edx
.L790:
	testb	$2, %dl
	je	.L791
	movslq	%r14d, %rdx
	andl	$-3, %eax
	addl	$1, %r14d
	movb	$1, -66(%rbp,%rdx)
	movzwl	%ax, %edx
.L791:
	testb	$16, %dl
	je	.L792
	movslq	%r14d, %rdx
	andl	$-17, %eax
	addl	$1, %r14d
	movb	$4, -66(%rbp,%rdx)
	movzwl	%ax, %edx
.L792:
	testb	$4, %dl
	je	.L793
	movslq	%r14d, %rdx
	andl	$-5, %eax
	addl	$1, %r14d
	movb	$2, -66(%rbp,%rdx)
	movzwl	%ax, %edx
.L793:
	testb	$32, %dl
	je	.L794
	movslq	%r14d, %rdx
	andl	$-33, %eax
	addl	$1, %r14d
	movb	$5, -66(%rbp,%rdx)
	movzwl	%ax, %edx
.L794:
	testb	$64, %dl
	je	.L795
	movslq	%r14d, %rdx
	andl	$-65, %eax
	addl	$1, %r14d
	movb	$6, -66(%rbp,%rdx)
	movzwl	%ax, %edx
.L795:
	testb	$-128, %dl
	je	.L796
	movslq	%r14d, %rdx
	addl	$1, %r14d
	movb	$7, -66(%rbp,%rdx)
	movl	%eax, %edx
	andl	$65407, %edx
.L796:
	andb	$1, %dh
	je	.L785
	movslq	%r14d, %rax
	addl	$1, %r14d
	movb	$8, -66(%rbp,%rax)
.L785:
	movl	-112(%rbp), %edi
	movzbl	63(%r13), %esi
	movb	$0, -128(%rbp)
	movl	$1, %r9d
	movb	$0, -193(%rbp)
	leal	-65377(%rdi), %eax
	movq	%r15, -152(%rbp)
	movl	%eax, -192(%rbp)
	movl	%edi, %eax
	sarl	$10, %eax
	movq	%r13, -160(%rbp)
	movq	%r9, %r13
	cltq
	movq	%r8, -168(%rbp)
	addq	%rax, %rax
	movq	%r10, -176(%rbp)
	movl	%esi, %r10d
	movq	%rax, -136(%rbp)
	movl	%edi, %eax
	sarl	$4, %eax
	movq	%r12, -184(%rbp)
	movq	%rbx, %r12
	andl	$63, %eax
	movl	%eax, -144(%rbp)
	movl	%edi, %eax
	andl	$15, %eax
	leal	16(%rax), %ecx
	movl	%eax, -204(%rbp)
	movl	$1, %eax
	sall	%cl, %eax
	leal	-160(%rdi), %ecx
	movl	%eax, -188(%rbp)
	leal	-57344(%rdi), %eax
	cmpl	$6399, %eax
	leal	-983040(%rdi), %eax
	movl	%ecx, -200(%rbp)
	leaq	-67(%rbp), %rcx
	setbe	%dl
	cmpl	$131071, %eax
	movq	%rcx, %rbx
	setbe	%al
	orl	%eax, %edx
	cmpl	$92, %edi
	movb	%dl, -195(%rbp)
	setne	%dl
	cmpl	$126, %edi
	setne	%al
	andl	%eax, %edx
	movl	%edi, %eax
	xorl	%edi, %edi
	movb	%dl, -194(%rbp)
	movl	%edi, %r8d
	movl	%eax, %r9d
	.p2align 4,,10
	.p2align 3
.L853:
	movzbl	(%rbx,%r13), %r15d
	cmpb	$8, %r15b
	ja	.L798
	leaq	.L800(%rip), %rcx
	movzbl	%r15b, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L800:
	.long	.L805-.L800
	.long	.L804-.L800
	.long	.L803-.L800
	.long	.L802-.L800
	.long	.L801-.L800
	.long	.L798-.L800
	.long	.L798-.L800
	.long	.L798-.L800
	.long	.L799-.L800
	.text
	.p2align 4,,10
	.p2align 3
.L798:
	movsbq	%r15b, %rax
	movq	(%r11,%rax,8), %rdx
	cmpl	$65535, %r9d
	jle	.L838
	testb	$1, 253(%rdx)
	jne	.L838
.L839:
	movq	288(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1011
	movsbl	%r10b, %ecx
	movl	%r9d, %esi
	leaq	-88(%rbp), %rdx
	movq	%r11, -224(%rbp)
	movl	%r8d, -216(%rbp)
	movb	%r10b, -212(%rbp)
	movl	%r9d, -208(%rbp)
	call	ucnv_extSimpleMatchFromU_67@PLT
	movl	-208(%rbp), %r9d
	movzbl	-212(%rbp), %r10d
	cmpl	$2, %eax
	movl	-216(%rbp), %r8d
	movq	-224(%rbp), %r11
	jne	.L847
	movl	-88(%rbp), %esi
	movl	$1, %edx
.L842:
	cmpb	$7, %r15b
	je	.L1020
.L897:
	movb	%r15b, -193(%rbp)
	movl	%eax, %r8d
	xorl	%r10d, %r10d
	movl	%esi, -104(%rbp)
	movb	$0, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L807:
	cmpl	%r13d, %r14d
	setle	%al
	addq	$1, %r13
	orb	%dl, %al
	je	.L853
	movl	%r8d, %eax
	movl	%r8d, %ecx
	movq	%r12, %rbx
	movq	-152(%rbp), %r15
	movq	-160(%rbp), %r13
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %r12
	testl	%eax, %eax
	je	.L854
	movzbl	-193(%rbp), %eax
	movb	%al, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L881:
	movl	%ecx, %edx
	movl	%ecx, %eax
	movzbl	102(%r11), %esi
	sarl	$31, %edx
	xorl	%edx, %eax
	subl	%edx, %eax
	movsbl	-128(%rbp), %edx
	cmpb	$1, %sil
	jne	.L855
	testb	%dl, %dl
	je	.L1021
.L855:
	movslq	%edx, %rcx
	movzbl	-136(%rbp), %edi
	movq	%rcx, -144(%rbp)
	cmpb	%dil, 98(%r11,%rcx)
	je	.L903
	movl	$0, -152(%rbp)
	leaq	-74(%rbp), %rdi
.L856:
	movsbq	-136(%rbp), %rdx
	leaq	_ZL14escSeqCharsLen(%rip), %rcx
	movq	%rdx, %rsi
	movsbq	(%rcx,%rdx), %rdx
	movsbq	%sil, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	leaq	_ZL11escSeqChars(%rip), %rcx
	movq	%rdx, %r14
	leaq	(%rcx,%rsi,2), %rsi
	cmpq	$8, %rdx
	jnb	.L859
	testb	$4, %dl
	jne	.L1022
	testq	%rdx, %rdx
	je	.L860
	movzbl	(%rsi), %ecx
	movb	%cl, (%rdi)
	testb	$2, %dl
	jne	.L1023
.L860:
	movq	-144(%rbp), %rcx
	movzbl	-136(%rbp), %esi
	movsbl	%r14b, %edx
	xorl	%r14d, %r14d
	addl	-152(%rbp), %edx
	movb	%sil, 98(%r11,%rcx)
	movzbl	102(%r11), %esi
	leal	1(%rdx), %ecx
.L858:
	movzbl	-128(%rbp), %edi
	cmpb	%sil, %dil
	je	.L857
	cmpb	$1, %dil
	je	.L1024
	movslq	%edx, %rsi
	movslq	%ecx, %rcx
	movb	$27, -74(%rbp,%rsi)
	movb	$78, -74(%rbp,%rcx)
	leal	3(%rdx), %ecx
	addl	$2, %edx
.L857:
	cmpl	$1, %eax
	je	.L1025
	movl	-104(%rbp), %eax
	movslq	%edx, %rsi
	addl	$2, %edx
	movb	%ah, -74(%rbp,%rsi)
	movslq	%ecx, %rax
	movzbl	-104(%rbp), %ecx
	movb	%cl, -74(%rbp,%rax)
.L865:
	movl	-112(%rbp), %eax
	cmpl	$13, %eax
	je	.L916
	cmpl	$10, %eax
	je	.L916
.L866:
	cmpl	$1, %edx
	je	.L1026
	cmpl	$2, %edx
	je	.L1027
.L869:
	movq	-120(%rbp), %rax
	subq	16(%rbx), %rax
	xorl	%edi, %edi
	pushq	%r12
	sarq	%rax
	leaq	-88(%rbp), %rcx
	leaq	-74(%rbp), %rsi
	movq	%r10, -128(%rbp)
	cmpl	$65535, -112(%rbp)
	leaq	-96(%rbp), %r9
	movq	%r11, -136(%rbp)
	seta	%dil
	movq	%r15, -88(%rbp)
	addl	$1, %edi
	movq	%r8, -112(%rbp)
	subl	%edi, %eax
	movq	%r13, %rdi
	pushq	%rax
	call	ucnv_fromUWriteBytes_67@PLT
	movl	(%r12), %edx
	popq	%r10
	movq	-88(%rbp), %r15
	movq	-120(%rbp), %rax
	popq	%r11
	testl	%edx, %edx
	jg	.L873
	movq	-128(%rbp), %r10
	movq	-112(%rbp), %r8
	movq	-136(%rbp), %r11
	cmpq	%r10, %rax
	jb	.L880
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L799:
	cmpl	$62, -192(%rbp)
	ja	.L1011
	movl	108(%r11), %eax
	cmpl	$3, %eax
	je	.L1028
	cmpl	$4, %eax
	je	.L811
	.p2align 4,,10
	.p2align 3
.L1011:
	testl	%r8d, %r8d
.L1012:
	setg	%dl
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L802:
	cmpl	$127, %r9d
	ja	.L812
	cmpb	$0, -194(%rbp)
	je	.L1011
	movl	-112(%rbp), %eax
	movb	$0, -128(%rbp)
	movq	%r12, %rbx
	movb	%r15b, -136(%rbp)
	movq	-160(%rbp), %r13
	movl	%eax, -104(%rbp)
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %r10
	movq	-152(%rbp), %r15
	movq	-184(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L806:
	movl	$1, %ecx
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L803:
	movq	16(%r11), %rdx
	cmpl	$65535, %r9d
	jle	.L832
	testb	$1, 253(%rdx)
	je	.L1011
.L832:
	movq	88(%rdx), %rcx
	movq	-136(%rbp), %rax
	movq	232(%rdx), %rdx
	movzwl	(%rcx,%rax), %eax
	addl	-144(%rbp), %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	addl	-204(%rbp), %eax
	cltq
	movzwl	(%rdx,%rax,2), %edx
	movl	%edx, %eax
	cmpl	$3839, %edx
	jg	.L899
	testb	%r10b, %r10b
	je	.L834
	cmpl	$2047, %edx
	setg	%dl
.L835:
	testb	%dl, %dl
	je	.L1011
	testl	%r8d, %r8d
	jne	.L1012
	xorl	%edx, %edx
	orl	$-1, %ecx
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L801:
	movq	32(%r11), %rdx
	cmpl	$65535, %r9d
	jle	.L813
	testb	$1, 253(%rdx)
	jne	.L813
.L814:
	movq	288(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1010
	movsbl	%r10b, %ecx
	movl	%r9d, %esi
	leaq	-88(%rbp), %rdx
	movq	%r11, -224(%rbp)
	movl	%r8d, -216(%rbp)
	movb	%r10b, -212(%rbp)
	movl	%r9d, -208(%rbp)
	call	ucnv_extSimpleMatchFromU_67@PLT
	movl	-208(%rbp), %r9d
	movzbl	-212(%rbp), %r10d
	cmpl	$2, %eax
	movl	-216(%rbp), %r8d
	movq	-224(%rbp), %r11
	jne	.L823
	movl	-88(%rbp), %ecx
	movl	$1, %edx
.L818:
	cmpl	$61436, %ecx
	ja	.L1011
	movl	%ecx, %edi
	movb	%cl, -208(%rbp)
	movzbl	%cl, %ecx
	andl	$65280, %edi
	leal	-28672(%rdi), %esi
	cmpl	$40704, %edi
	movl	%esi, -212(%rbp)
	leal	-45056(%rdi), %esi
	movzbl	-208(%rbp), %edi
	cmovbe	-212(%rbp), %esi
	addl	%esi, %esi
	cmpb	$-98, %dil
	ja	.L827
	subl	$256, %esi
	cmpb	$126, %dil
	ja	.L828
	subl	$31, %ecx
	orl	%ecx, %esi
.L829:
	testl	%esi, %esi
	jne	.L897
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L804:
	cmpl	$95, -200(%rbp)
	ja	.L1011
	movl	-112(%rbp), %eax
	movb	%r15b, -136(%rbp)
	movq	%r12, %rbx
	movb	$2, -128(%rbp)
	movq	-160(%rbp), %r13
	addl	$-128, %eax
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %r10
	movl	%eax, -104(%rbp)
	movq	-152(%rbp), %r15
	movq	-184(%rbp), %r12
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L805:
	cmpl	$127, %r9d
	jg	.L1011
	movl	%r15d, %eax
	movb	%r15b, -136(%rbp)
	movq	%r12, %rbx
	movq	-160(%rbp), %r13
	movb	%al, -128(%rbp)
	movl	-112(%rbp), %eax
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %r10
	movl	%eax, -104(%rbp)
	movq	-152(%rbp), %r15
	movq	-184(%rbp), %r12
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L838:
	movq	88(%rdx), %rcx
	movq	-136(%rbp), %rax
	movl	%r9d, %esi
	andl	$15, %esi
	movzwl	(%rcx,%rax), %eax
	addl	-144(%rbp), %eax
	cltq
	movl	(%rcx,%rax,4), %ecx
	movzwl	%cx, %eax
	sall	$4, %eax
	addl	%esi, %eax
	movq	232(%rdx), %rsi
	movzwl	(%rsi,%rax,2), %esi
	cmpl	$255, %esi
	jbe	.L840
	testl	%ecx, -188(%rbp)
	jne	.L1029
	movl	$1, %eax
	testb	%r10b, %r10b
	je	.L883
.L882:
	movl	%esi, -88(%rbp)
.L845:
	testl	%r8d, %r8d
	jne	.L1011
	testb	%al, %al
	je	.L1011
	movl	-88(%rbp), %esi
	xorl	%edx, %edx
	movl	$-2, %eax
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L813:
	movq	88(%rdx), %rcx
	movq	-136(%rbp), %rax
	movzwl	(%rcx,%rax), %eax
	addl	-144(%rbp), %eax
	cltq
	movl	(%rcx,%rax,4), %esi
	movl	%r9d, %ecx
	andl	$15, %ecx
	movzwl	%si, %eax
	sall	$4, %eax
	addl	%ecx, %eax
	movq	232(%rdx), %rcx
	movzwl	(%rcx,%rax,2), %ecx
	cmpl	$255, %ecx
	ja	.L815
	testl	%esi, -188(%rbp)
	jne	.L1010
	xorl	%esi, %esi
	testb	%r10b, %r10b
	jne	.L820
.L885:
	cmpb	$0, -195(%rbp)
	je	.L814
.L820:
	testl	%ecx, %ecx
	je	.L814
.L884:
	movl	%ecx, -88(%rbp)
.L821:
	testl	%r8d, %r8d
	sete	%al
	jne	.L817
	testb	%sil, %sil
	je	.L817
	movl	-88(%rbp), %ecx
	xorl	%edx, %edx
	movl	$-2, %eax
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L909:
	movl	$3, %ecx
	xorl	%edx, %edx
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L812:
	cmpl	$165, %r9d
	je	.L893
	cmpl	$8254, %r9d
	jne	.L1011
	movq	%r12, %rbx
	movb	%r15b, -136(%rbp)
	movq	-160(%rbp), %r13
	movb	$0, -128(%rbp)
	movq	-168(%rbp), %r8
	movl	$126, -104(%rbp)
	movq	-176(%rbp), %r10
	movq	-152(%rbp), %r15
	movq	-184(%rbp), %r12
	jmp	.L806
.L854:
	movl	-112(%rbp), %eax
	movl	$10, (%r12)
	movl	%eax, 84(%r13)
	movq	-120(%rbp), %rax
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1015:
	addq	%rsi, %rdx
	movl	$10267, %edi
	movl	%ecx, %r10d
	movw	%di, (%rdx)
	movb	$66, 2(%rdx)
	movb	$0, 98(%r11)
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L1010:
	testl	%r8d, %r8d
	sete	%al
.L817:
	testb	%r10b, %r10b
	je	.L1011
	testb	%al, %al
	je	.L1011
	movslq	-192(%rbp), %rax
	cmpl	$62, %eax
	ja	.L898
	leaq	_ZL9hwkana_fb(%rip), %rcx
	movb	$0, -128(%rbp)
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	movzwl	(%rcx,%rax,2), %eax
	movb	%r15b, -193(%rbp)
	movl	$-2, %r8d
	movl	%eax, -104(%rbp)
	jmp	.L807
.L840:
	testl	%ecx, -188(%rbp)
	jne	.L1011
	xorl	%eax, %eax
	testb	%r10b, %r10b
	jne	.L844
	movl	%r10d, %eax
.L883:
	cmpb	$0, -195(%rbp)
	je	.L839
.L844:
	testl	%esi, %esi
	je	.L839
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L815:
	testl	%esi, -188(%rbp)
	jne	.L1030
	movl	$1, %esi
	testb	%r10b, %r10b
	jne	.L884
	jmp	.L885
.L899:
	movl	$1, %edx
	movl	$1, %ecx
.L833:
	movzbl	%al, %eax
	leal	-160(%rax), %esi
	cmpl	$95, %esi
	ja	.L1011
	addl	$-128, %eax
	movb	%r15b, -128(%rbp)
	movl	%ecx, %r8d
	xorl	%r10d, %r10d
	movl	%eax, -104(%rbp)
	movb	%r15b, -193(%rbp)
	jmp	.L807
.L834:
	cmpl	$3071, %edx
	setg	%dl
	jmp	.L835
.L1016:
	testl	%edi, %edi
	je	.L878
	movzwl	-2(%r11,%r9), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L877
.L878:
	leal	-2(%rdx), %edi
	jmp	.L877
.L1020:
	leal	24159(%rsi), %ecx
	cmpw	$23901, %cx
	ja	.L1011
	leal	95(%rsi), %ecx
	cmpb	$93, %cl
	ja	.L1011
	subl	$32896, %esi
	movl	%esi, -88(%rbp)
	jne	.L897
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L827:
	subl	$126, %ecx
	orl	%ecx, %esi
	jmp	.L829
.L916:
	movb	$0, 100(%r11)
	xorl	%r14d, %r14d
	jmp	.L866
.L903:
	movl	$1, %ecx
	xorl	%edx, %edx
	jmp	.L858
.L1028:
	movl	-112(%rbp), %eax
	movb	%r15b, -136(%rbp)
	movq	%r12, %rbx
	movb	$8, 99(%r11)
	movq	-160(%rbp), %r13
	subl	$65344, %eax
	movb	$1, -128(%rbp)
	movq	-168(%rbp), %r8
	movl	%eax, -104(%rbp)
	movq	-176(%rbp), %r10
	movq	-152(%rbp), %r15
	movq	-184(%rbp), %r12
	jmp	.L806
.L1025:
	movzbl	-104(%rbp), %eax
	movslq	%edx, %rdx
	movb	%al, -74(%rbp,%rdx)
	movl	%ecx, %edx
	jmp	.L865
.L1026:
	movzbl	-74(%rbp), %eax
	leaq	1(%r15), %rcx
	movb	%al, (%r15)
	movq	-96(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L905
	movq	-120(%rbp), %rdi
	leaq	4(%rdx), %rsi
	movq	%rcx, %r15
	movq	%rsi, -96(%rbp)
	movq	%rdi, %rax
	subq	16(%rbx), %rax
	sarq	%rax
	subl	$1, %eax
	movl	%eax, (%rdx)
	movq	%rdi, %rax
	jmp	.L774
.L823:
	cmpl	$-2, %eax
	sete	%sil
	jmp	.L821
.L847:
	cmpl	$-2, %eax
	sete	%al
	jmp	.L845
.L1029:
	movl	%esi, -88(%rbp)
	movl	$1, %edx
	movl	$2, %eax
	jmp	.L842
.L1030:
	movl	$1, %edx
	movl	$2, %eax
	jmp	.L818
.L811:
	movl	-112(%rbp), %eax
	movzbl	98(%r11), %ecx
	movq	%r12, %rbx
	movb	$0, -128(%rbp)
	movq	-152(%rbp), %r15
	movq	-160(%rbp), %r13
	subl	$65216, %eax
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %r10
	movl	%eax, -104(%rbp)
	leal	-4(%rcx), %eax
	movq	-184(%rbp), %r12
	cmpb	$3, %al
	movl	$3, %eax
	cmovbe	%eax, %ecx
	movb	%cl, -136(%rbp)
	jmp	.L806
.L828:
	subl	$32, %ecx
	orl	%ecx, %esi
	jmp	.L829
.L859:
	movq	(%rsi), %rcx
	leaq	8(%rdi), %r9
	andq	$-8, %r9
	movq	%rcx, (%rdi)
	movq	-8(%rsi,%rdx), %rcx
	movq	%rcx, -8(%rdi,%rdx)
	movq	%rdi, %rcx
	movq	%r9, %rdi
	subq	%r9, %rcx
	subq	%rcx, %rsi
	addq	%rdx, %rcx
	shrq	$3, %rcx
	rep movsq
	jmp	.L860
.L1027:
	leaq	2(%r15), %rcx
	cmpq	%rcx, %r8
	jb	.L869
	movzbl	-74(%rbp), %eax
	movb	%al, (%r15)
	movzbl	-73(%rbp), %eax
	movb	%al, 1(%r15)
	movq	-96(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L905
	movq	-120(%rbp), %rdi
	movq	%rcx, %r15
	movq	%rdi, %rax
	subq	16(%rbx), %rax
	sarq	%rax
	cmpl	$65536, -112(%rbp)
	sbbl	%esi, %esi
	addl	$2, %esi
	subl	%esi, %eax
	leaq	8(%rdx), %rsi
	movl	%eax, (%rdx)
	movq	%rsi, -96(%rbp)
	movl	%eax, 4(%rdx)
	movq	%rdi, %rax
	jmp	.L774
.L1024:
	movslq	%edx, %rdx
	movb	$1, 102(%r11)
	movb	$14, -74(%rbp,%rdx)
	movl	%ecx, %edx
	addl	$1, %ecx
	jmp	.L857
.L898:
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	jmp	.L807
.L1021:
	movb	$15, -74(%rbp)
	movzbl	-136(%rbp), %ecx
	movb	$0, 102(%r11)
	cmpb	98(%r11), %cl
	jne	.L902
	movl	$2, %ecx
	movl	$1, %edx
	jmp	.L857
.L893:
	movq	%r12, %rbx
	movb	%r15b, -136(%rbp)
	movq	-160(%rbp), %r13
	movb	$0, -128(%rbp)
	movq	-168(%rbp), %r8
	movl	$92, -104(%rbp)
	movq	-176(%rbp), %r10
	movq	-152(%rbp), %r15
	movq	-184(%rbp), %r12
	jmp	.L806
.L905:
	movq	-120(%rbp), %rax
	movq	%rcx, %r15
	jmp	.L774
.L902:
	movslq	%edx, %rcx
	leaq	-73(%rbp), %rdi
	movl	$1, -152(%rbp)
	movq	%rcx, -144(%rbp)
	jmp	.L856
.L1017:
	call	__stack_chk_fail@PLT
.L1022:
	movl	(%rsi), %ecx
	movl	%ecx, (%rdi)
	movl	-4(%rsi,%rdx), %ecx
	movl	%ecx, -4(%rdi,%rdx)
	jmp	.L860
.L1023:
	movzwl	-2(%rsi,%rdx), %ecx
	movw	%cx, -2(%rdi,%rdx)
	jmp	.L860
	.cfi_endproc
.LFE2131:
	.size	_ZL48UConverter_fromUnicode_ISO_2022_JP_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode, .-_ZL48UConverter_fromUnicode_ISO_2022_JP_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode
	.p2align 4
	.type	_ZL48UConverter_fromUnicode_ISO_2022_CN_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode, @function
_ZL48UConverter_fromUnicode_ISO_2022_CN_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode:
.LFB2137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	8(%rdi), %r14
	movq	32(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	movq	40(%rdi), %r8
	movl	84(%r14), %r10d
	movq	16(%rdi), %r13
	movq	%rax, -88(%rbp)
	movq	16(%r14), %rax
	movq	24(%rdi), %r11
	movq	%rax, -104(%rbp)
	testl	%r10d, %r10d
	je	.L1120
	cmpq	%r8, %r15
	jb	.L1204
	cmpq	%r11, %r13
	jb	.L1036
.L1035:
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L1106
.L1110:
	movq	-104(%rbp), %rax
	cmpb	$0, 102(%rax)
	je	.L1106
	cmpb	$0, 2(%rbx)
	je	.L1106
	cmpq	%r13, %r11
	jbe	.L1205
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	%r13, 16(%rbx)
	movq	%r15, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1206
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1120:
	.cfi_restore_state
	movl	$0, -108(%rbp)
.L1032:
	cmpq	%r11, %r13
	jnb	.L1035
.L1111:
	cmpq	%r15, %r8
	jbe	.L1036
	movzwl	0(%r13), %r9d
	leaq	2(%r13), %rax
	movq	%rax, -120(%rbp)
	movl	%r9d, %edx
	movl	%r9d, %eax
	andl	$63488, %edx
	cmpl	$55296, %edx
	je	.L1207
.L1037:
	cmpl	$127, %r9d
	jg	.L1042
	cmpl	$31, %r9d
	jg	.L1043
	movl	$134266880, %eax
	btl	%r9d, %eax
	jc	.L1203
.L1043:
	cmpl	$10, %r9d
	movq	-104(%rbp), %rcx
	sete	%al
	cmpl	$13, %r9d
	sete	%dl
	orl	%edx, %eax
	cmpb	$0, 102(%rcx)
	jne	.L1044
	movb	%r9b, -64(%rbp)
	testb	%al, %al
	jne	.L1208
.L1045:
	movb	%r9b, (%r15)
	movq	-88(%rbp), %rcx
	leaq	1(%r15), %rdx
	testq	%rcx, %rcx
	je	.L1128
	movq	-120(%rbp), %r13
	leaq	4(%rcx), %rsi
	movq	%rdx, %r15
	movq	%rsi, -88(%rbp)
	movq	%r13, %rax
	subq	16(%rbx), %rax
	sarq	%rax
	subl	$1, %eax
	movl	%eax, (%rcx)
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1036:
	movl	$15, (%r12)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1204:
	movl	$0, -108(%rbp)
	xorl	%eax, %eax
.L1033:
	cmpq	%r11, %r13
	jnb	.L1039
	movzwl	0(%r13), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L1209
	movl	$12, (%r12)
	movl	%r10d, 84(%r14)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1207:
	testb	$4, %ah
	jne	.L1203
	movl	%r10d, %eax
	movq	-120(%rbp), %r13
	movl	%r9d, %r10d
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1039:
	movl	%r10d, 84(%r14)
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1205:
	movl	84(%r14), %ecx
	testl	%ecx, %ecx
	jne	.L1106
	movb	$0, 102(%rax)
	movq	16(%rbx), %rdx
	movq	%r13, %rax
	movl	$-1, %esi
	subq	%rdx, %rax
	sarq	%rax
	testl	%eax, %eax
	jle	.L1107
	leal	-1(%rax), %esi
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx), %rdi
	movzwl	(%rdx,%rcx,2), %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L1210
.L1107:
	pushq	%r12
	movl	$1, %edx
	leaq	-80(%rbp), %rcx
	leaq	-88(%rbp), %r9
	pushq	%rsi
	movq	%r14, %rdi
	leaq	_ZL12SHIFT_IN_STR(%rip), %rsi
	movq	%r15, -80(%rbp)
	call	ucnv_fromUWriteBytes_67@PLT
	popq	%rax
	movq	-80(%rbp), %r15
	popq	%rdx
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1203:
	movl	$12, (%r12)
	movq	-120(%rbp), %r13
	movl	%r9d, 84(%r14)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1209:
	sall	$10, %r10d
	leaq	2(%r13), %rcx
	movl	$0, 84(%r14)
	leal	-56613888(%rdx,%r10), %r9d
	movq	%rcx, -120(%rbp)
	movl	%eax, %r10d
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1042:
	testl	%r10d, %r10d
	jne	.L1047
	movq	-104(%rbp), %rcx
	movzbl	99(%rcx), %eax
	movl	108(%rcx), %edx
	movb	%al, -67(%rbp)
	testb	%al, %al
	jne	.L1048
	movb	$1, -67(%rbp)
	testl	%edx, %edx
	je	.L1052
	cmpl	$1, %edx
	je	.L1053
.L1091:
	movl	$10, (%r12)
	movq	-120(%rbp), %r13
	movl	%r9d, 84(%r14)
	jmp	.L1106
.L1051:
	cmpl	$1, %edx
	jne	.L1091
	cmpb	$1, %al
	je	.L1053
	cmpb	$2, %al
	jne	.L1211
	movw	$8449, -66(%rbp)
	movl	$3, %r10d
	.p2align 4,,10
	.p2align 3
.L1047:
	movzbl	63(%r14), %eax
	movl	%r9d, %ecx
	movb	$0, -128(%rbp)
	andl	$15, %ecx
	movb	$0, -112(%rbp)
	movl	%eax, %edi
	movl	%r9d, %eax
	addl	$16, %ecx
	movq	%rbx, -176(%rbp)
	sarl	$10, %eax
	movq	%r12, -184(%rbp)
	cltq
	movq	%r14, -152(%rbp)
	movl	%r10d, %r14d
	movl	%edi, %r10d
	addq	%rax, %rax
	movq	%r8, -160(%rbp)
	movq	%rax, -136(%rbp)
	movl	%r9d, %eax
	sarl	$4, %eax
	movq	%r11, -168(%rbp)
	andl	$63, %eax
	movl	%eax, -140(%rbp)
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, -144(%rbp)
	leal	-57344(%r9), %eax
	cmpl	$6399, %eax
	leal	-983040(%r9), %eax
	setbe	%dl
	cmpl	$131071, %eax
	setbe	%al
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	orl	%eax, %edx
	leaq	-67(%rbp), %rax
	movl	%ecx, %r8d
	movb	%dl, -185(%rbp)
	movq	%rax, %r11
.L1090:
	leal	1(%r13), %eax
	movzbl	(%r11,%r13), %r12d
	cmpl	%eax, %r14d
	setle	%bl
	testb	%r12b, %r12b
	jle	.L1140
	movsbl	%r10b, %ecx
	cmpb	$31, %r12b
	jle	.L1059
	movq	-104(%rbp), %rax
	movq	24(%rax), %rsi
	cmpl	$65535, %r9d
	jle	.L1060
	testb	$1, 253(%rsi)
	jne	.L1060
.L1061:
	movq	288(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L1140
	movl	%r9d, %esi
	leaq	-80(%rbp), %rdx
	movq	%r11, -208(%rbp)
	movb	%r10b, -186(%rbp)
	movl	%r8d, -196(%rbp)
	movl	%r9d, -192(%rbp)
	call	ucnv_extSimpleMatchFromU_67@PLT
	movl	-192(%rbp), %r9d
	movl	-196(%rbp), %r8d
	cmpl	$3, %eax
	movzbl	-186(%rbp), %r10d
	movq	-208(%rbp), %r11
	jne	.L1072
	movl	-80(%rbp), %eax
	movl	%eax, -108(%rbp)
.L1064:
	shrl	$16, %eax
	movl	$2, %r8d
	subl	$96, %eax
	movb	%al, -112(%rbp)
	movl	$1, %eax
.L1109:
	cmpb	$33, -112(%rbp)
	je	.L1212
	cmpb	$34, -112(%rbp)
	je	.L1213
	movq	-104(%rbp), %rcx
	cmpl	$1, 108(%rcx)
	je	.L1214
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L1058:
	addq	$1, %r13
	testb	%bl, %bl
	je	.L1090
	movl	%r8d, %eax
	movl	%r14d, %r10d
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %r14
	movq	-168(%rbp), %r11
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r12
	testl	%eax, %eax
	je	.L1091
.L1114:
	movq	-104(%rbp), %rax
	movsbq	-128(%rbp), %rdx
	movzbl	-112(%rbp), %edi
	movzbl	102(%rax), %ecx
	cmpb	%dil, 98(%rax,%rdx)
	je	.L1092
	movsbq	%dil, %rax
	cmpb	$2, %dil
	jle	.L1202
	subl	$30, %eax
	cltq
.L1202:
	leaq	_ZL13escSeqCharsCN(%rip), %rsi
	cmpb	$1, -128(%rbp)
	movq	(%rsi,%rax,8), %rax
	movl	(%rax), %eax
	movl	%eax, -64(%rbp)
	movq	-104(%rbp), %rax
	movb	%dil, 98(%rax,%rdx)
	je	.L1215
	cmpb	%cl, -128(%rbp)
	je	.L1216
	movl	$8, %edx
	movl	$7, %eax
	movl	$6, %ecx
	movl	$5, %edi
	movl	$4, %esi
.L1113:
	cmpb	$2, -128(%rbp)
	je	.L1099
	movb	$27, -64(%rbp,%rsi)
	movb	$79, -64(%rbp,%rdi)
.L1101:
	movslq	%ecx, %rsi
	movl	-108(%rbp), %ecx
	movb	%ch, -64(%rbp,%rsi)
	movzbl	-108(%rbp), %ecx
	movb	%cl, -64(%rbp,%rax)
.L1102:
	movq	-120(%rbp), %r13
	xorl	%edi, %edi
	leaq	-80(%rbp), %rcx
	leaq	-64(%rbp), %rsi
	movq	%r15, -80(%rbp)
	movq	%r13, %rax
	subq	16(%rbx), %rax
	pushq	%r12
	sarq	%rax
	cmpl	$65535, %r9d
	leaq	-88(%rbp), %r9
	movq	%r8, -120(%rbp)
	seta	%dil
	movq	%r11, -128(%rbp)
	addl	$1, %edi
	movl	%r10d, -112(%rbp)
	subl	%edi, %eax
	movq	%r14, %rdi
	pushq	%rax
	call	ucnv_fromUWriteBytes_67@PLT
	movl	(%r12), %r9d
	popq	%rdi
	movq	-80(%rbp), %r15
	popq	%r8
	testl	%r9d, %r9d
	jg	.L1106
	movq	-128(%rbp), %r11
	movq	-120(%rbp), %r8
	movl	-112(%rbp), %r10d
	cmpq	%r11, %r13
	jnb	.L1110
	jmp	.L1111
.L1063:
	xorl	%eax, %eax
	testl	%r12d, -144(%rbp)
	je	.L1065
	.p2align 4,,10
	.p2align 3
.L1140:
	testl	%r8d, %r8d
	setg	%al
	orl	%eax, %ebx
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	-104(%rbp), %rdx
	movsbq	%r12b, %rax
	movq	(%rdx,%rax,8), %rdx
	cmpl	$65535, %r9d
	jle	.L1077
	testb	$1, 253(%rdx)
	jne	.L1077
.L1078:
	movq	288(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1140
	movl	%r9d, %esi
	leaq	-80(%rbp), %rdx
	movq	%r11, -208(%rbp)
	movb	%r10b, -186(%rbp)
	movl	%r8d, -196(%rbp)
	movl	%r9d, -192(%rbp)
	call	ucnv_extSimpleMatchFromU_67@PLT
	movl	-192(%rbp), %r9d
	movl	-196(%rbp), %r8d
	cmpl	$2, %eax
	movzbl	-186(%rbp), %r10d
	movq	-208(%rbp), %r11
	jne	.L1086
	movl	-80(%rbp), %eax
	movl	%r14d, %r10d
	movl	%r12d, %ecx
	movq	-152(%rbp), %r14
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %r11
	movl	%eax, -108(%rbp)
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r12
.L1081:
	movb	%cl, -112(%rbp)
	movb	$1, -128(%rbp)
	jmp	.L1114
.L1060:
	movq	88(%rsi), %rdx
	movq	-136(%rbp), %rax
	movzwl	(%rdx,%rax), %eax
	addl	-140(%rbp), %eax
	cltq
	movl	(%rdx,%rax,4), %r12d
	movl	%r9d, %edx
	andl	$15, %edx
	movzwl	%r12w, %eax
	sall	$4, %eax
	addl	%edx, %eax
	leal	(%rax,%rax,2), %eax
	addq	232(%rsi), %rax
	movzbl	(%rax), %edi
	movzbl	1(%rax), %edx
	sall	$16, %edi
	sall	$8, %edx
	orl	%edi, %edx
	movzbl	2(%rax), %edi
	orl	%edi, %edx
	cmpl	$255, %edx
	jbe	.L1062
	cmpl	$65535, %edx
	jbe	.L1063
	movl	$1, %eax
	testl	%r12d, -144(%rbp)
	jne	.L1217
.L1065:
	testb	%r10b, %r10b
	je	.L1116
.L1115:
	movl	%edx, -80(%rbp)
.L1070:
	testl	%r8d, %r8d
	jne	.L1140
	testb	%al, %al
	je	.L1140
	movl	-80(%rbp), %eax
	xorl	%r10d, %r10d
	movl	$-2, %r8d
	movl	%eax, -108(%rbp)
	shrl	$16, %eax
	subl	$96, %eax
	movb	%al, -112(%rbp)
	xorl	%eax, %eax
	jmp	.L1109
.L1077:
	movq	88(%rdx), %rsi
	movq	-136(%rbp), %rax
	movzwl	(%rsi,%rax), %eax
	addl	-140(%rbp), %eax
	cltq
	movl	(%rsi,%rax,4), %edi
	movl	%r9d, %esi
	andl	$15, %esi
	movzwl	%di, %eax
	sall	$4, %eax
	addl	%esi, %eax
	movq	232(%rdx), %rsi
	movzwl	(%rsi,%rax,2), %esi
	cmpl	$255, %esi
	jbe	.L1079
	testl	%edi, -144(%rbp)
	jne	.L1218
	movl	$1, %eax
	testb	%r10b, %r10b
	je	.L1118
.L1117:
	movl	%esi, -80(%rbp)
.L1084:
	testl	%r8d, %r8d
	jne	.L1140
	testb	%al, %al
	je	.L1140
	movl	-80(%rbp), %eax
	movb	%r12b, -112(%rbp)
	xorl	%r10d, %r10d
	movl	$-2, %r8d
	movb	$1, -128(%rbp)
	movl	%eax, -108(%rbp)
	jmp	.L1058
.L1044:
	movb	%r9b, -63(%rbp)
	xorl	%r10d, %r10d
	movb	$0, 102(%rcx)
	movb	$15, -64(%rbp)
	testb	%al, %al
	jne	.L1219
.L1046:
	leaq	2(%r15), %rax
	cmpq	%rax, %r8
	jb	.L1129
	movzbl	-64(%rbp), %edx
	movb	%dl, (%r15)
	movzbl	-63(%rbp), %edx
	movb	%dl, 1(%r15)
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1130
	movq	-120(%rbp), %r13
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%r13, %rcx
	subq	16(%rbx), %rcx
	sarq	%rcx
	cmpl	$65535, %r9d
	seta	%sil
	addl	$1, %esi
	subl	%esi, %ecx
	leaq	8(%rdx), %rsi
	movl	%ecx, (%rdx)
	movq	%rsi, -88(%rbp)
	movl	%ecx, 4(%rdx)
	jmp	.L1032
.L1062:
	testl	%r12d, -144(%rbp)
	jne	.L1140
	xorl	%eax, %eax
	testb	%r10b, %r10b
	jne	.L1069
	movl	%r10d, %eax
.L1116:
	cmpb	$0, -185(%rbp)
	je	.L1061
.L1069:
	testl	%edx, %edx
	je	.L1061
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1079:
	testl	%edi, -144(%rbp)
	jne	.L1140
	xorl	%eax, %eax
	testb	%r10b, %r10b
	jne	.L1083
.L1118:
	cmpb	$0, -185(%rbp)
	je	.L1078
.L1083:
	testl	%esi, %esi
	je	.L1078
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1210:
	testl	%esi, %esi
	je	.L1108
	movzwl	-2(%rdx,%rdi), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1107
.L1108:
	leal	-2(%rax), %esi
	jmp	.L1107
.L1048:
	testl	%edx, %edx
	jne	.L1051
	cmpb	$1, %al
	je	.L1052
	movb	$1, -66(%rbp)
	movl	$2, %r10d
	jmp	.L1047
.L1212:
	movb	$1, -128(%rbp)
	orl	%eax, %ebx
	jmp	.L1058
.L1218:
	movl	%r14d, %r10d
	movl	%r12d, %ecx
	movl	%esi, -108(%rbp)
	movq	-152(%rbp), %r14
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %r11
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r12
	jmp	.L1081
.L1092:
	movzbl	-128(%rbp), %eax
	cmpb	%cl, %al
	je	.L1097
	cmpb	$1, %al
	je	.L1127
	movl	$4, %edx
	movl	$3, %eax
	movl	$2, %ecx
	xorl	%esi, %esi
	movl	$1, %edi
	jmp	.L1113
.L1129:
	movl	$2, %edx
	jmp	.L1102
.L1213:
	movb	$2, -128(%rbp)
	orl	%eax, %ebx
	jmp	.L1058
.L1086:
	cmpl	$-2, %eax
	sete	%al
	jmp	.L1084
.L1208:
	xorl	%edx, %edx
	movl	$0, 98(%rcx)
	xorl	%r10d, %r10d
	movw	%dx, 102(%rcx)
	jmp	.L1045
.L1219:
	xorl	%r13d, %r13d
	movl	$0, 98(%rcx)
	movw	%r13w, 102(%rcx)
	jmp	.L1046
.L1128:
	movq	-120(%rbp), %r13
	movq	%rdx, %r15
	jmp	.L1032
.L1072:
	cmpl	$-3, %eax
	sete	%al
	jmp	.L1070
.L1099:
	movb	$27, -64(%rbp,%rsi)
	movb	$78, -64(%rbp,%rdi)
	jmp	.L1101
.L1215:
	cmpb	$1, %cl
	je	.L1220
	movl	$7, %edx
	movl	$6, %eax
	movl	$5, %ecx
	xorl	%r10d, %r10d
	movl	$4, %esi
.L1098:
	movq	-104(%rbp), %rdi
	movb	$14, -64(%rbp,%rsi)
	movb	$1, 102(%rdi)
	jmp	.L1101
.L1214:
	movb	$3, -128(%rbp)
	orl	%eax, %ebx
	jmp	.L1058
.L1052:
	movb	$33, -66(%rbp)
	movl	$2, %r10d
	jmp	.L1047
.L1216:
	movzwl	-108(%rbp), %eax
	movl	$6, %edx
	rolw	$8, %ax
	movw	%ax, -60(%rbp)
	jmp	.L1102
.L1217:
	movl	%edx, -108(%rbp)
	movl	%edx, %eax
	jmp	.L1064
.L1130:
	movq	-120(%rbp), %r13
	movq	%rax, %r15
	jmp	.L1032
.L1220:
	movzwl	-108(%rbp), %eax
	xorl	%r10d, %r10d
	movl	$6, %edx
	rolw	$8, %ax
	movw	%ax, -60(%rbp)
	jmp	.L1102
.L1127:
	movl	$3, %edx
	movl	$2, %eax
	movl	$1, %ecx
	xorl	%esi, %esi
	jmp	.L1098
.L1053:
	movw	$545, -66(%rbp)
	movl	$3, %r10d
	jmp	.L1047
.L1211:
	movw	$513, -66(%rbp)
	movl	$3, %r10d
	jmp	.L1047
.L1206:
	call	__stack_chk_fail@PLT
.L1097:
	movzwl	-108(%rbp), %eax
	rolw	$8, %ax
	movw	%ax, -64(%rbp)
	jmp	.L1046
	.cfi_endproc
.LFE2137:
	.size	_ZL48UConverter_fromUnicode_ISO_2022_CN_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode, .-_ZL48UConverter_fromUnicode_ISO_2022_CN_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZN12_GLOBAL__N_1L14_ISO2022CNDataE, @object
	.size	_ZN12_GLOBAL__N_1L14_ISO2022CNDataE, 296
_ZN12_GLOBAL__N_1L14_ISO2022CNDataE:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL20_ISO2022CNStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL14_ISO2022CNImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL20_ISO2022CNStaticData, @object
	.size	_ZL20_ISO2022CNStaticData, 100
_ZL20_ISO2022CNStaticData:
	.long	100
	.string	"ISO_2022_CN"
	.zero	48
	.long	0
	.byte	0
	.byte	10
	.byte	1
	.byte	8
	.string	"\032"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL14_ISO2022CNImpl, @object
	.size	_ZL14_ISO2022CNImpl, 144
_ZL14_ISO2022CNImpl:
	.long	10
	.zero	4
	.quad	0
	.quad	0
	.quad	_ZL12_ISO2022OpenP10UConverterP18UConverterLoadArgsP10UErrorCode
	.quad	_ZL13_ISO2022CloseP10UConverter
	.quad	_ZL13_ISO2022ResetP10UConverter21UConverterResetChoice
	.quad	_ZL46UConverter_toUnicode_ISO_2022_CN_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL46UConverter_toUnicode_ISO_2022_CN_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL48UConverter_fromUnicode_ISO_2022_CN_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode
	.quad	_ZL48UConverter_fromUnicode_ISO_2022_CN_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode
	.quad	0
	.quad	0
	.quad	_ISO2022getName
	.quad	_ZL18_ISO_2022_WriteSubP25UConverterFromUnicodeArgsiP10UErrorCode
	.quad	_ISO_2022_SafeClone
	.quad	_ZL23_ISO_2022_GetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode
	.quad	0
	.quad	0
	.align 32
	.type	_ZN12_GLOBAL__N_1L14_ISO2022KRDataE, @object
	.size	_ZN12_GLOBAL__N_1L14_ISO2022KRDataE, 296
_ZN12_GLOBAL__N_1L14_ISO2022KRDataE:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL20_ISO2022KRStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL14_ISO2022KRImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL20_ISO2022KRStaticData, @object
	.size	_ZL20_ISO2022KRStaticData, 100
_ZL20_ISO2022KRStaticData:
	.long	100
	.string	"ISO_2022_KR"
	.zero	48
	.long	0
	.byte	0
	.byte	10
	.byte	1
	.byte	8
	.string	"\032"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL14_ISO2022KRImpl, @object
	.size	_ZL14_ISO2022KRImpl, 144
_ZL14_ISO2022KRImpl:
	.long	10
	.zero	4
	.quad	0
	.quad	0
	.quad	_ZL12_ISO2022OpenP10UConverterP18UConverterLoadArgsP10UErrorCode
	.quad	_ZL13_ISO2022CloseP10UConverter
	.quad	_ZL13_ISO2022ResetP10UConverter21UConverterResetChoice
	.quad	_ZL46UConverter_toUnicode_ISO_2022_KR_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL46UConverter_toUnicode_ISO_2022_KR_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL48UConverter_fromUnicode_ISO_2022_KR_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode
	.quad	_ZL48UConverter_fromUnicode_ISO_2022_KR_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode
	.quad	0
	.quad	0
	.quad	_ISO2022getName
	.quad	_ZL18_ISO_2022_WriteSubP25UConverterFromUnicodeArgsiP10UErrorCode
	.quad	_ISO_2022_SafeClone
	.quad	_ZL23_ISO_2022_GetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode
	.quad	0
	.quad	0
	.align 32
	.type	_ZN12_GLOBAL__N_1L14_ISO2022JPDataE, @object
	.size	_ZN12_GLOBAL__N_1L14_ISO2022JPDataE, 296
_ZN12_GLOBAL__N_1L14_ISO2022JPDataE:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL20_ISO2022JPStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL14_ISO2022JPImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL20_ISO2022JPStaticData, @object
	.size	_ZL20_ISO2022JPStaticData, 100
_ZL20_ISO2022JPStaticData:
	.long	100
	.string	"ISO_2022_JP"
	.zero	48
	.long	0
	.byte	0
	.byte	10
	.byte	1
	.byte	6
	.string	"\032"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL14_ISO2022JPImpl, @object
	.size	_ZL14_ISO2022JPImpl, 144
_ZL14_ISO2022JPImpl:
	.long	10
	.zero	4
	.quad	0
	.quad	0
	.quad	_ZL12_ISO2022OpenP10UConverterP18UConverterLoadArgsP10UErrorCode
	.quad	_ZL13_ISO2022CloseP10UConverter
	.quad	_ZL13_ISO2022ResetP10UConverter21UConverterResetChoice
	.quad	_ZL46UConverter_toUnicode_ISO_2022_JP_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL46UConverter_toUnicode_ISO_2022_JP_OFFSETS_LOGICP23UConverterToUnicodeArgsP10UErrorCode
	.quad	_ZL48UConverter_fromUnicode_ISO_2022_JP_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode
	.quad	_ZL48UConverter_fromUnicode_ISO_2022_JP_OFFSETS_LOGICP25UConverterFromUnicodeArgsP10UErrorCode
	.quad	0
	.quad	0
	.quad	_ISO2022getName
	.quad	_ZL18_ISO_2022_WriteSubP25UConverterFromUnicodeArgsiP10UErrorCode
	.quad	_ISO_2022_SafeClone
	.quad	_ZL23_ISO_2022_GetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode
	.quad	0
	.quad	0
	.globl	_ISO2022Data_67
	.align 32
	.type	_ISO2022Data_67, @object
	.size	_ISO2022Data_67, 296
_ISO2022Data_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_ISO2022StaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_ISO2022Impl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_ISO2022StaticData, @object
	.size	_ZL18_ISO2022StaticData, 100
_ZL18_ISO2022StaticData:
	.long	100
	.string	"ISO_2022"
	.zero	51
	.long	2022
	.byte	0
	.byte	10
	.byte	1
	.byte	3
	.string	"\032"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL12_ISO2022Impl, @object
	.size	_ZL12_ISO2022Impl, 144
_ZL12_ISO2022Impl:
	.long	10
	.zero	4
	.quad	0
	.quad	0
	.quad	_ZL12_ISO2022OpenP10UConverterP18UConverterLoadArgsP10UErrorCode
	.quad	_ZL13_ISO2022CloseP10UConverter
	.quad	_ZL13_ISO2022ResetP10UConverter21UConverterResetChoice
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ISO2022getName
	.quad	_ZL18_ISO_2022_WriteSubP25UConverterFromUnicodeArgsiP10UErrorCode
	.quad	_ISO_2022_SafeClone
	.quad	_ZL23_ISO_2022_GetUnicodeSetPK10UConverterPK9USetAdder20UConverterUnicodeSetP10UErrorCode
	.quad	0
	.quad	0
	.align 32
	.type	_ZL13escSeqCharsCN, @object
	.size	_ZL13escSeqCharsCN, 80
_ZL13escSeqCharsCN:
	.quad	_ZL12SHIFT_IN_STR
	.quad	_ZL14GB_2312_80_STR
	.quad	_ZL14ISO_IR_165_STR
	.quad	_ZL26CNS_11643_1992_Plane_1_STR
	.quad	_ZL26CNS_11643_1992_Plane_2_STR
	.quad	_ZL26CNS_11643_1992_Plane_3_STR
	.quad	_ZL26CNS_11643_1992_Plane_4_STR
	.quad	_ZL26CNS_11643_1992_Plane_5_STR
	.quad	_ZL26CNS_11643_1992_Plane_6_STR
	.quad	_ZL26CNS_11643_1992_Plane_7_STR
	.section	.rodata
	.type	_ZL26CNS_11643_1992_Plane_7_STR, @object
	.size	_ZL26CNS_11643_1992_Plane_7_STR, 5
_ZL26CNS_11643_1992_Plane_7_STR:
	.string	"\033$+M"
	.type	_ZL26CNS_11643_1992_Plane_6_STR, @object
	.size	_ZL26CNS_11643_1992_Plane_6_STR, 5
_ZL26CNS_11643_1992_Plane_6_STR:
	.string	"\033$+L"
	.type	_ZL26CNS_11643_1992_Plane_5_STR, @object
	.size	_ZL26CNS_11643_1992_Plane_5_STR, 5
_ZL26CNS_11643_1992_Plane_5_STR:
	.string	"\033$+K"
	.type	_ZL26CNS_11643_1992_Plane_4_STR, @object
	.size	_ZL26CNS_11643_1992_Plane_4_STR, 5
_ZL26CNS_11643_1992_Plane_4_STR:
	.string	"\033$+J"
	.type	_ZL26CNS_11643_1992_Plane_3_STR, @object
	.size	_ZL26CNS_11643_1992_Plane_3_STR, 5
_ZL26CNS_11643_1992_Plane_3_STR:
	.string	"\033$+I"
	.type	_ZL26CNS_11643_1992_Plane_2_STR, @object
	.size	_ZL26CNS_11643_1992_Plane_2_STR, 5
_ZL26CNS_11643_1992_Plane_2_STR:
	.string	"\033$*H"
	.type	_ZL26CNS_11643_1992_Plane_1_STR, @object
	.size	_ZL26CNS_11643_1992_Plane_1_STR, 5
_ZL26CNS_11643_1992_Plane_1_STR:
	.string	"\033$)G"
	.type	_ZL14ISO_IR_165_STR, @object
	.size	_ZL14ISO_IR_165_STR, 5
_ZL14ISO_IR_165_STR:
	.string	"\033$)E"
	.type	_ZL14GB_2312_80_STR, @object
	.size	_ZL14GB_2312_80_STR, 5
_ZL14GB_2312_80_STR:
	.string	"\033$)A"
	.align 32
	.type	_ZL9hwkana_fb, @object
	.size	_ZL9hwkana_fb, 126
_ZL9hwkana_fb:
	.value	8483
	.value	8534
	.value	8535
	.value	8482
	.value	8486
	.value	9586
	.value	9505
	.value	9507
	.value	9509
	.value	9511
	.value	9513
	.value	9571
	.value	9573
	.value	9575
	.value	9539
	.value	8508
	.value	9506
	.value	9508
	.value	9510
	.value	9512
	.value	9514
	.value	9515
	.value	9517
	.value	9519
	.value	9521
	.value	9523
	.value	9525
	.value	9527
	.value	9529
	.value	9531
	.value	9533
	.value	9535
	.value	9537
	.value	9540
	.value	9542
	.value	9544
	.value	9546
	.value	9547
	.value	9548
	.value	9549
	.value	9550
	.value	9551
	.value	9554
	.value	9557
	.value	9560
	.value	9563
	.value	9566
	.value	9567
	.value	9568
	.value	9569
	.value	9570
	.value	9572
	.value	9574
	.value	9576
	.value	9577
	.value	9578
	.value	9579
	.value	9580
	.value	9581
	.value	9583
	.value	9587
	.value	8491
	.value	8492
	.align 8
	.type	_ZL14escSeqCharsLen, @object
	.size	_ZL14escSeqCharsLen, 9
_ZL14escSeqCharsLen:
	.ascii	"\003\003\003\003\003\004\003\004\003"
	.align 32
	.type	_ZL11escSeqChars, @object
	.size	_ZL11escSeqChars, 54
_ZL11escSeqChars:
	.string	"\033(B"
	.zero	2
	.string	"\033.A"
	.zero	2
	.string	"\033.F"
	.zero	2
	.string	"\033(J"
	.zero	2
	.string	"\033$B"
	.zero	2
	.string	"\033$(D"
	.zero	1
	.string	"\033$A"
	.zero	2
	.string	"\033$(C"
	.zero	1
	.string	"\033(I"
	.zero	2
	.align 32
	.type	_ZL20nextStateToUnicodeCN, @object
	.size	_ZL20nextStateToUnicodeCN, 74
_ZL20nextStateToUnicodeCN:
	.ascii	"\377\377\377\377\377\020\021\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\001\377\002!\"#$%&'\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377"
	.align 32
	.type	_ZL20nextStateToUnicodeJP, @object
	.size	_ZL20nextStateToUnicodeJP, 74
_ZL20nextStateToUnicodeJP:
	.string	"\377\377\377\377\377\020\377\377\377\377"
	.ascii	"\377\377\377\377\377\003\b\003\377\377\377\004\006\004\377\377"
	.ascii	"\377\377\377\001\002\004\377\377\377\377\007\005\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377"
	.align 32
	.type	_ZL27escSeqStateTable_Value_2022, @object
	.size	_ZL27escSeqStateTable_Value_2022, 74
_ZL27escSeqStateTable_Value_2022:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001"
	.string	"\001\001\002\001\001\001\001\001\001\001\001\001\001"
	.string	"\001\001\001"
	.string	""
	.string	""
	.string	""
	.string	"\001\001\001\001"
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.ascii	"\001\001\001\001\001\001\001\001\001\001"
	.align 32
	.type	_ZL25escSeqStateTable_Key_2022, @object
	.size	_ZL25escSeqStateTable_Key_2022, 296
_ZL25escSeqStateTable_Key_2022:
	.long	1
	.long	34
	.long	36
	.long	39
	.long	55
	.long	57
	.long	60
	.long	61
	.long	1093
	.long	1096
	.long	1097
	.long	1098
	.long	1099
	.long	1100
	.long	1101
	.long	1102
	.long	1103
	.long	1104
	.long	1105
	.long	1106
	.long	1109
	.long	1154
	.long	1157
	.long	1160
	.long	1161
	.long	1176
	.long	1178
	.long	1179
	.long	1254
	.long	1257
	.long	1768
	.long	1773
	.long	1957
	.long	35105
	.long	36933
	.long	36936
	.long	36937
	.long	36938
	.long	36939
	.long	36940
	.long	36942
	.long	36943
	.long	36944
	.long	36945
	.long	36946
	.long	36947
	.long	36948
	.long	37640
	.long	37642
	.long	37644
	.long	37646
	.long	37711
	.long	37744
	.long	37745
	.long	37746
	.long	37747
	.long	37748
	.long	40133
	.long	40136
	.long	40138
	.long	40139
	.long	40140
	.long	40141
	.long	1123363
	.long	35947624
	.long	35947625
	.long	35947626
	.long	35947627
	.long	35947629
	.long	35947630
	.long	35947631
	.long	35947635
	.long	35947636
	.long	35947638
	.align 32
	.type	_ZL24normalize_esq_chars_2022, @object
	.size	_ZL24normalize_esq_chars_2022, 256
_ZL24normalize_esq_chars_2022:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\004\007\035"
	.string	"\002\030\032\033"
	.string	"\003\027\006"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\005\b\t\n\013\f\r\016\017\020\021\022\023\024\031\034"
	.string	""
	.string	"\025"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\026"
	.zero	164
	.align 8
	.type	_ZL14jpCharsetMasks, @object
	.size	_ZL14jpCharsetMasks, 10
_ZL14jpCharsetMasks:
	.value	281
	.value	313
	.value	511
	.value	511
	.value	511
	.type	_ZL12SHIFT_IN_STR, @object
	.size	_ZL12SHIFT_IN_STR, 2
_ZL12SHIFT_IN_STR:
	.string	"\017"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC9:
	.quad	3617006443637461833
	.quad	4424061374497254444
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
