	.file	"uresdata.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue7getTypeEv
	.type	_ZNK6icu_6717ResourceDataValue7getTypeEv, @function
_ZNK6icu_6717ResourceDataValue7getTypeEv:
.LFB2424:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %eax
	leaq	_ZL12gPublicTypes(%rip), %rdx
	shrl	$28, %eax
	movsbl	(%rdx,%rax), %eax
	ret
	.cfi_endproc
.LFE2424:
	.size	_ZNK6icu_6717ResourceDataValue7getTypeEv, .-_ZNK6icu_6717ResourceDataValue7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue14getAliasStringERiR10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue14getAliasStringERiR10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue14getAliasStringERiR10UErrorCode:
.LFB2426:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L3
	movl	72(%rdi), %ecx
	movl	%ecx, %r8d
	shrl	$28, %r8d
	cmpl	$3, %r8d
	jne	.L10
	xorl	%edx, %edx
	leaq	_ZL12gEmptyString(%rip), %rax
	andl	$268435455, %ecx
	je	.L6
	movq	16(%rdi), %rax
	movl	%ecx, %ecx
	leaq	(%rax,%rcx,4), %rax
	movl	(%rax), %edx
.L6:
	movl	%edx, (%rsi)
	addq	$4, %rax
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$0, (%rsi)
	movl	$17, (%rdx)
	ret
	.cfi_endproc
.LFE2426:
	.size	_ZNK6icu_6717ResourceDataValue14getAliasStringERiR10UErrorCode, .-_ZNK6icu_6717ResourceDataValue14getAliasStringERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue12getIntVectorERiR10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue12getIntVectorERiR10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue12getIntVectorERiR10UErrorCode:
.LFB2429:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L11
	movl	72(%rdi), %ecx
	movl	%ecx, %r8d
	shrl	$28, %r8d
	cmpl	$14, %r8d
	jne	.L18
	xorl	%edx, %edx
	leaq	_ZL8gEmpty32(%rip), %rax
	andl	$268435455, %ecx
	je	.L14
	movq	16(%rdi), %rax
	movl	%ecx, %ecx
	leaq	(%rax,%rcx,4), %rax
	movl	(%rax), %edx
.L14:
	movl	%edx, (%rsi)
	addq	$4, %rax
.L11:
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$0, (%rsi)
	movl	$17, (%rdx)
	ret
	.cfi_endproc
.LFE2429:
	.size	_ZNK6icu_6717ResourceDataValue12getIntVectorERiR10UErrorCode, .-_ZNK6icu_6717ResourceDataValue12getIntVectorERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue9getBinaryERiR10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue9getBinaryERiR10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue9getBinaryERiR10UErrorCode:
.LFB2430:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L19
	movl	72(%rdi), %ecx
	movl	%ecx, %r8d
	shrl	$28, %r8d
	cmpl	$1, %r8d
	jne	.L26
	xorl	%edx, %edx
	leaq	_ZL8gEmpty32(%rip), %rax
	andl	$268435455, %ecx
	je	.L22
	movq	16(%rdi), %rax
	movl	%ecx, %ecx
	leaq	(%rax,%rcx,4), %rax
	movl	(%rax), %edx
.L22:
	movl	%edx, (%rsi)
	addq	$4, %rax
.L19:
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$0, (%rsi)
	movl	$17, (%rdx)
	ret
	.cfi_endproc
.LFE2430:
	.size	_ZNK6icu_6717ResourceDataValue9getBinaryERiR10UErrorCode, .-_ZNK6icu_6717ResourceDataValue9getBinaryERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue21isNoInheritanceMarkerEv
	.type	_ZNK6icu_6717ResourceDataValue21isNoInheritanceMarkerEv, @function
_ZNK6icu_6717ResourceDataValue21isNoInheritanceMarkerEv:
.LFB2433:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %eax
	xorl	%r8d, %r8d
	movl	%eax, %edx
	andl	$268435455, %edx
	je	.L27
	cmpl	%edx, %eax
	je	.L47
	shrl	$28, %eax
	cmpl	$6, %eax
	je	.L48
.L27:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movl	56(%rdi), %eax
	cmpl	%edx, %eax
	jle	.L31
	movq	48(%rdi), %rax
	movl	%edx, %edx
	leaq	(%rax,%rdx,2), %rdx
.L32:
	movzwl	(%rdx), %eax
	xorl	%r8d, %r8d
	cmpl	$8709, %eax
	je	.L49
	cmpl	$56323, %eax
	jne	.L27
	cmpw	$8709, 2(%rdx)
	jne	.L27
	cmpw	$8709, 4(%rdx)
	jne	.L27
	cmpw	$8709, 6(%rdx)
	sete	%r8b
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L47:
	movq	16(%rdi), %rdx
	leaq	(%rdx,%rax,4), %rax
	cmpl	$3, (%rax)
	jne	.L27
	cmpw	$8709, 4(%rax)
	jne	.L27
	cmpw	$8709, 6(%rax)
	jne	.L27
	cmpw	$8709, 8(%rax)
	sete	%r8b
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L31:
	subl	%eax, %edx
	movq	24(%rdi), %rax
	leaq	(%rax,%rdx,2), %rdx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L49:
	cmpw	$8709, 2(%rdx)
	jne	.L27
	cmpw	$8709, 4(%rdx)
	jne	.L27
	cmpw	$0, 6(%rdx)
	sete	%r8b
	jmp	.L27
	.cfi_endproc
.LFE2433:
	.size	_ZNK6icu_6717ResourceDataValue21isNoInheritanceMarkerEv, .-_ZNK6icu_6717ResourceDataValue21isNoInheritanceMarkerEv
	.p2align 4
	.type	_ZL12isAcceptablePvPKcS1_PK9UDataInfo, @function
_ZL12isAcceptablePvPKcS1_PK9UDataInfo:
.LFB2407:
	.cfi_startproc
	endbr64
	movl	12(%rcx), %eax
	movl	%eax, (%rdi)
	xorl	%eax, %eax
	cmpw	$19, (%rcx)
	jbe	.L50
	cmpw	$0, 4(%rcx)
	jne	.L50
	cmpb	$2, 6(%rcx)
	je	.L57
.L50:
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	cmpw	$25938, 8(%rcx)
	jne	.L50
	cmpw	$17011, 10(%rcx)
	jne	.L50
	movzbl	12(%rcx), %eax
	subl	$1, %eax
	cmpb	$2, %al
	setbe	%al
	ret
	.cfi_endproc
.LFE2407:
	.size	_ZL12isAcceptablePvPKcS1_PK9UDataInfo, .-_ZL12isAcceptablePvPKcS1_PK9UDataInfo
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717ResourceDataValueD2Ev
	.type	_ZN6icu_6717ResourceDataValueD2Ev, @function
_ZN6icu_6717ResourceDataValueD2Ev:
.LFB2421:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717ResourceDataValueE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6713ResourceValueD2Ev@PLT
	.cfi_endproc
.LFE2421:
	.size	_ZN6icu_6717ResourceDataValueD2Ev, .-_ZN6icu_6717ResourceDataValueD2Ev
	.globl	_ZN6icu_6717ResourceDataValueD1Ev
	.set	_ZN6icu_6717ResourceDataValueD1Ev,_ZN6icu_6717ResourceDataValueD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717ResourceDataValueD0Ev
	.type	_ZN6icu_6717ResourceDataValueD0Ev, @function
_ZN6icu_6717ResourceDataValueD0Ev:
.LFB2423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717ResourceDataValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6713ResourceValueD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2423:
	.size	_ZN6icu_6717ResourceDataValueD0Ev, .-_ZN6icu_6717ResourceDataValueD0Ev
	.p2align 4
	.type	_ZL16ures_compareRowsPKvS0_S0_, @function
_ZL16ures_compareRowsPKvS0_S0_:
.LFB2447:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movslq	(%rdx), %rsi
	movslq	(%r8), %rax
	addq	%rdi, %rsi
	addq	%rax, %rdi
	jmp	strcmp@PLT
	.cfi_endproc
.LFE2447:
	.size	_ZL16ures_compareRowsPKvS0_S0_, .-_ZL16ures_compareRowsPKvS0_S0_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"ures_swapResource(table res=%08x)[%d].recurse(%08x) failed\n"
	.align 8
.LC2:
	.string	"ures_swapResource(table res=%08x).uprv_sortArray(%d items) failed\n"
	.align 8
.LC3:
	.string	"ures_swapResource(array res=%08x)[%d].recurse(%08x) failed\n"
	.text
	.p2align 4
	.type	_ZL17ures_swapResourcePK12UDataSwapperPKjPjjPKcP9TempTableP10UErrorCode, @function
_ZL17ures_swapResourcePK12UDataSwapperPKjPjjPKcP9TempTableP10UErrorCode:
.LFB2448:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsi, %r11
	movq	%rdx, %r10
	shrl	$28, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %r15
	testl	%ecx, %ecx
	js	.L63
	cmpl	$4, %eax
	ja	.L62
.L65:
	movl	%r12d, %r9d
	andl	$268435455, %r9d
	je	.L62
	movl	%r9d, %edx
	movq	24(%rbx), %rcx
	movl	%r12d, %esi
	sarl	$5, %edx
	andl	$31, %esi
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx,4), %rcx
	movl	(%rcx), %edx
	btl	%r12d, %edx
	jc	.L62
	btsl	%esi, %edx
	movl	%edx, (%rcx)
	movslq	%r9d, %rdx
	salq	$2, %rdx
	leaq	(%r11,%rdx), %rdi
	movq	%rdi, -56(%rbp)
	leaq	(%r10,%rdx), %rdi
	movq	%rdi, -64(%rbp)
	cmpl	$14, %eax
	ja	.L68
	leaq	.L70(%rip), %rsi
	movl	%eax, %ecx
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L70:
	.long	.L73-.L70
	.long	.L74-.L70
	.long	.L72-.L70
	.long	.L73-.L70
	.long	.L72-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L71-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L68-.L70
	.long	.L69-.L70
	.text
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$16, (%r15)
.L62:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	cmpl	$9, %eax
	jne	.L65
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	%r10, -88(%rbp)
	movq	-56(%rbp), %r14
	movq	%r11, -80(%rbp)
	movl	%r9d, -72(%rbp)
	cmpl	$2, %eax
	jne	.L77
	movzwl	(%r14), %edi
	call	*8(%r13)
	movq	-64(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r15, %r8
	movzwl	%ax, %eax
	movl	$2, %edx
	leaq	2(%rcx), %rdi
	movl	%eax, -56(%rbp)
	movq	%rdi, -120(%rbp)
	leaq	2(%r14), %rdi
	movq	%rdi, -64(%rbp)
	movq	%r13, %rdi
	call	*48(%r13)
	movl	-56(%rbp), %eax
	movl	-72(%rbp), %r9d
	movq	$0, -128(%rbp)
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %r10
	movq	$0, -112(%rbp)
	addl	$2, %eax
	sarl	%eax
	addl	%eax, %r9d
.L78:
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	je	.L62
	movslq	%r9d, %r9
	leaq	0(,%r9,4), %rdi
	leaq	(%r11,%rdi), %rsi
	movq	%rdi, -136(%rbp)
	movq	%rsi, -80(%rbp)
	jle	.L90
	subl	$1, %eax
	xorl	%r14d, %r14d
	cmpq	$0, -64(%rbp)
	movq	%r11, -96(%rbp)
	movl	%r12d, -140(%rbp)
	movq	%r10, %r12
	movq	%rax, -104(%rbp)
	je	.L153
.L89:
	movq	-64(%rbp), %rax
	movl	%r14d, -88(%rbp)
	movzwl	(%rax,%r14,2), %edi
	call	*8(%r13)
	leaq	.LC0(%rip), %r8
	movzwl	%ax, %edx
	cmpl	%edx, 32(%rbx)
	jle	.L87
	movzwl	%ax, %eax
	leaq	(%r12,%rax), %r8
.L87:
	movq	-80(%rbp), %rax
	movq	%r8, -72(%rbp)
	movl	(%rax,%r14,4), %edi
	call	*16(%r13)
	subq	$8, %rsp
	movq	-72(%rbp), %r8
	movq	-96(%rbp), %rsi
	pushq	%r15
	movl	%eax, %ecx
	movq	%r12, %rdx
	movq	%rbx, %r9
	movq	%r13, %rdi
	movl	%eax, -72(%rbp)
	call	_ZL17ures_swapResourcePK12UDataSwapperPKjPjjPKcP9TempTableP10UErrorCode
	movl	(%r15), %ecx
	popq	%rax
	movl	-72(%rbp), %eax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L145
	leaq	1(%r14), %rax
	cmpq	%r14, -104(%rbp)
	je	.L154
	movq	%rax, %r14
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-56(%rbp), %r14
	movq	%r13, %rdi
	movl	(%r14), %esi
	call	udata_readInt32_67@PLT
	movq	-64(%rbp), %r12
	movq	%r15, %r8
	movq	%r14, %rsi
	movl	%eax, %ebx
	movl	$4, %edx
	movq	%r13, %rdi
	movq	%r12, %rcx
	call	*56(%r13)
	movq	%r12, %rcx
	movq	48(%r13), %rax
	leal	(%rbx,%rbx), %edx
	addq	$4, %rcx
	leaq	4(%r14), %rsi
	movq	%r15, %r8
.L150:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	-56(%rbp), %rbx
	movq	%r13, %rdi
	movl	(%rbx), %esi
	call	udata_readInt32_67@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %r8
	movq	%rbx, %rsi
	movl	%eax, %r12d
	movl	$4, %edx
	movq	%r13, %rdi
	call	*56(%r13)
	testq	%r14, %r14
	je	.L62
	leaq	.LC0(%rip), %rax
	cmpq	%rax, %r14
	je	.L75
	movl	$14, %r8d
	leaq	_ZL16gCollationBinKey(%rip), %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$-1, %edx
	call	*24(%r13)
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
.L76:
	testl	%eax, %eax
	je	.L62
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
	leaq	-40(%rbp), %rsp
	movq	%r15, %r8
	popq	%rbx
	movl	%r12d, %edx
	movq	%r13, %rdi
	popq	%r12
	addq	$4, %rcx
	popq	%r13
	addq	$4, %rsi
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ucol_swap_67@PLT
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	-56(%rbp), %rbx
	movq	%r13, %rdi
	movl	(%rbx), %esi
	call	udata_readInt32_67@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %r8
	movq	%rbx, %rsi
	leal	4(,%rax,4), %edx
	movq	56(%r13), %rax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-56(%rbp), %r14
	movq	%r13, %rdi
	movq	%r10, -80(%rbp)
	movq	%r11, -72(%rbp)
	movl	(%r14), %esi
	call	udata_readInt32_67@PLT
	leaq	4(%r14), %rdi
	movq	-64(%rbp), %rcx
	movq	%r14, %rsi
	movq	%rdi, -104(%rbp)
	movq	%r15, %r8
	movl	$4, %edx
	movq	%r13, %rdi
	movl	%eax, -96(%rbp)
	xorl	%r14d, %r14d
	call	*56(%r13)
	movl	-96(%rbp), %eax
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r10
	leal	-1(%rax), %ecx
	testl	%eax, %eax
	movq	%rcx, -88(%rbp)
	jle	.L113
	movq	%r10, -80(%rbp)
	movq	%rbx, %rax
	movq	%r14, %rbx
	movl	%r12d, -112(%rbp)
	movq	%rax, %r14
	movq	%r11, %r12
.L114:
	movq	-56(%rbp), %rax
	movl	4(%rax,%rbx,4), %edi
	call	*16(%r13)
	subq	$8, %rsp
	movq	-80(%rbp), %rdx
	xorl	%r8d, %r8d
	pushq	%r15
	movl	%eax, %ecx
	movq	%r14, %r9
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%eax, -72(%rbp)
	call	_ZL17ures_swapResourcePK12UDataSwapperPKjPjjPKcP9TempTableP10UErrorCode
	popq	%r11
	popq	%rax
	movl	(%r15), %eax
	testl	%eax, %eax
	movl	-72(%rbp), %eax
	jg	.L155
	leaq	1(%rbx), %rax
	cmpq	%rbx, -88(%rbp)
	je	.L113
	movq	%rax, %rbx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-64(%rbp), %rcx
	movl	-96(%rbp), %edx
	movq	%r15, %r8
	movq	56(%r13), %rax
	movq	-104(%rbp), %rsi
	addq	$4, %rcx
	sall	$2, %edx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L77:
	movl	(%r14), %esi
	movq	%r13, %rdi
	call	udata_readInt32_67@PLT
	movq	-64(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r15, %r8
	movl	%eax, -56(%rbp)
	movl	$4, %edx
	leaq	4(%rcx), %rdi
	movq	%rdi, -128(%rbp)
	leaq	4(%r14), %rdi
	movq	%rdi, -112(%rbp)
	movq	%r13, %rdi
	call	*56(%r13)
	movl	-56(%rbp), %eax
	movl	-72(%rbp), %r9d
	movq	$0, -120(%rbp)
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r11
	movq	$0, -64(%rbp)
	leal	1(%rax,%r9), %r9d
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L155:
	movl	-112(%rbp), %r12d
	movl	%eax, %r8d
	movl	%ebx, %ecx
	leaq	.LC3(%rip), %rsi
	movl	%r12d, %edx
.L151:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	udata_printError_67@PLT
.L146:
	.cfi_restore_state
	movq	%r12, %r10
	movl	-140(%rbp), %r12d
	movq	%r14, %rbx
.L90:
	addq	-136(%rbp), %r10
	cmpb	$1, 36(%rbx)
	movq	%r10, -72(%rbp)
	jbe	.L156
.L80:
	movq	-64(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L92
	movl	-56(%rbp), %ebx
	movq	-120(%rbp), %rcx
	movq	%r15, %r8
	movq	%r13, %rdi
	leal	(%rbx,%rbx), %edx
	call	*48(%r13)
	movq	56(%r13), %rax
	movq	-72(%rbp), %rcx
	movq	%r15, %r8
	movq	-80(%rbp), %rsi
	leal	0(,%rbx,4), %edx
	jmp	.L150
.L145:
	movl	-140(%rbp), %r12d
	movl	-88(%rbp), %ecx
	movl	%eax, %r8d
	leaq	.LC1(%rip), %rsi
	movl	%r12d, %edx
	jmp	.L151
.L154:
	movq	%r12, %r10
	movl	-140(%rbp), %r12d
	jmp	.L90
.L75:
	movq	-56(%rbp), %rax
	movl	%r12d, %edx
	movq	%r13, %rdi
	leaq	4(%rax), %rsi
	call	ucol_looksLikeCollationBinary_67@PLT
	movsbl	%al, %eax
	jmp	.L76
.L156:
	movzbl	3(%r13), %eax
	cmpb	%al, 1(%r13)
	je	.L80
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L93
	movl	-56(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L94
	xorl	%r14d, %r14d
	subl	$1, %ecx
	movl	%r12d, -88(%rbp)
	movq	%rax, %r12
	movq	%r15, -96(%rbp)
	movq	%r14, %r15
	movq	%rbx, %r14
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L96:
	movzwl	(%r12,%r15,2), %edi
	call	*8(%r13)
	movq	8(%r14), %rdi
	movzwl	%ax, %eax
	leaq	(%rdi,%r15,8), %rcx
	movl	%eax, (%rcx)
	movq	%r15, %rax
	movl	%r15d, 4(%rcx)
	addq	$1, %r15
	cmpq	%rbx, %rax
	jne	.L96
	movl	-88(%rbp), %r12d
	movq	-96(%rbp), %r15
	movq	%r14, %rbx
.L95:
	subq	$8, %rsp
	movl	-56(%rbp), %esi
	movl	$8, %edx
	xorl	%r9d, %r9d
	pushq	%r15
	movq	(%rbx), %r8
	leaq	_ZL16ures_compareRowsPKvS0_S0_(%rip), %rcx
	call	uprv_sortArray_67@PLT
	movl	(%r15), %esi
	popq	%rdx
	popq	%rcx
	testl	%esi, %esi
	jg	.L115
	movq	-120(%rbp), %rcx
	cmpq	%rcx, -64(%rbp)
	je	.L157
	cmpl	$0, -56(%rbp)
	jle	.L103
	movq	-120(%rbp), %r12
.L119:
	movq	%r15, %rax
	xorl	%r14d, %r14d
	movq	%r12, %r15
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L101:
	movq	8(%rbx), %rax
	movq	-64(%rbp), %rsi
	leaq	(%r15,%r14,2), %rcx
	movq	%r12, %r8
	movl	$2, %edx
	movq	%r13, %rdi
	movslq	4(%rax,%r14,8), %rax
	addq	$1, %r14
	leaq	(%rsi,%rax,2), %rsi
	call	*48(%r13)
	cmpl	%r14d, -56(%rbp)
	jg	.L101
	movq	%r12, %rax
	movq	%r15, %r12
	movq	%rax, %r15
.L100:
	movq	-120(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L103
	movl	-56(%rbp), %eax
	movq	%r12, %rsi
	leal	(%rax,%rax), %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
.L103:
	movq	-72(%rbp), %rcx
	cmpq	%rcx, -80(%rbp)
	je	.L158
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	jle	.L62
	movq	%rcx, %r12
.L118:
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L110:
	movq	8(%rbx), %rax
	movq	-80(%rbp), %rsi
	leaq	(%r12,%r14,4), %rcx
	movq	%r15, %r8
	movl	$4, %edx
	movq	%r13, %rdi
	movslq	4(%rax,%r14,8), %rax
	addq	$1, %r14
	leaq	(%rsi,%rax,4), %rsi
	call	*56(%r13)
	cmpl	%r14d, -56(%rbp)
	jg	.L110
.L109:
	movq	-72(%rbp), %rdi
	cmpq	%rdi, %r12
	je	.L62
	movl	-56(%rbp), %edx
	leaq	-40(%rbp), %rsp
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	sall	$2, %edx
	popq	%r13
	popq	%r14
	movslq	%edx, %rdx
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
.L153:
	.cfi_restore_state
	movq	%rbx, %rax
	movq	%r14, %rbx
	movq	%rax, %r14
.L86:
	movq	-112(%rbp), %rax
	movq	%r13, %rdi
	movl	%ebx, -88(%rbp)
	movl	(%rax,%rbx,4), %esi
	call	udata_readInt32_67@PLT
	movslq	%eax, %r8
	addq	%r12, %r8
	testl	%eax, %eax
	leaq	.LC0(%rip), %rax
	cmovs	%rax, %r8
	movq	-80(%rbp), %rax
	movq	%r8, -72(%rbp)
	movl	(%rax,%rbx,4), %edi
	call	*16(%r13)
	subq	$8, %rsp
	movq	-72(%rbp), %r8
	movq	-96(%rbp), %rsi
	pushq	%r15
	movl	%eax, %ecx
	movq	%r13, %rdi
	movq	%r14, %r9
	movq	%r12, %rdx
	movl	%eax, -72(%rbp)
	call	_ZL17ures_swapResourcePK12UDataSwapperPKjPjjPKcP9TempTableP10UErrorCode
	movl	(%r15), %r8d
	popq	%rsi
	movl	-72(%rbp), %eax
	popq	%rdi
	testl	%r8d, %r8d
	jg	.L145
	leaq	1(%rbx), %rax
	cmpq	%rbx, -104(%rbp)
	je	.L146
	movq	%rax, %rbx
	jmp	.L86
.L92:
	movl	-56(%rbp), %edx
	movq	56(%r13), %rax
	movq	%r15, %r8
	movq	-128(%rbp), %rcx
	movq	-112(%rbp), %rsi
	sall	$3, %edx
	jmp	.L150
.L93:
	movl	-56(%rbp), %eax
	movq	8(%rbx), %rdi
	testl	%eax, %eax
	jle	.L97
	subl	$1, %eax
	movq	%rbx, -64(%rbp)
	xorl	%r14d, %r14d
	movq	%rax, -88(%rbp)
	movl	%r12d, -96(%rbp)
	movq	-112(%rbp), %r12
	movq	%r15, -104(%rbp)
	movq	%r14, %r15
	movq	%r13, %r14
.L98:
	leaq	0(,%r15,8), %r13
	movl	(%r12,%r15,4), %esi
	leaq	(%rdi,%r13), %rbx
	movq	%r14, %rdi
	call	udata_readInt32_67@PLT
	movl	%eax, (%rbx)
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	movq	%r15, %rax
	movl	%r15d, 4(%rdi,%r13)
	addq	$1, %r15
	cmpq	%rax, -88(%rbp)
	jne	.L98
	movl	-96(%rbp), %r12d
	movq	-64(%rbp), %rbx
	movq	%r14, %r13
	movq	-104(%rbp), %r15
.L97:
	pushq	%r8
	movl	-56(%rbp), %esi
	xorl	%r9d, %r9d
	leaq	_ZL16ures_compareRowsPKvS0_S0_(%rip), %rcx
	pushq	%r15
	movq	(%rbx), %r8
	movl	$8, %edx
	call	uprv_sortArray_67@PLT
	cmpl	$0, (%r15)
	popq	%r9
	popq	%r10
	jg	.L115
	movq	-128(%rbp), %rcx
	cmpq	%rcx, -112(%rbp)
	je	.L159
	cmpl	$0, -56(%rbp)
	jle	.L103
	movq	-128(%rbp), %r14
.L120:
	movq	%r15, %rax
	xorl	%r12d, %r12d
	movq	%r14, %r15
	movq	%rax, %r14
.L106:
	movq	8(%rbx), %rax
	movq	-112(%rbp), %rdi
	leaq	(%r15,%r12,4), %rcx
	movq	%r14, %r8
	movl	$4, %edx
	movslq	4(%rax,%r12,8), %rax
	addq	$1, %r12
	leaq	(%rdi,%rax,4), %rsi
	movq	%r13, %rdi
	call	*56(%r13)
	cmpl	%r12d, -56(%rbp)
	jg	.L106
	movq	%r14, %rax
	movq	%r15, %r14
	movq	%rax, %r15
.L105:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L103
	movl	-56(%rbp), %eax
	movq	%r14, %rsi
	leal	0(,%rax,4), %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	jmp	.L103
.L115:
	movl	-56(%rbp), %ecx
	leaq	-40(%rbp), %rsp
	movl	%r12d, %edx
	movq	%r13, %rdi
	popq	%rbx
	leaq	.LC2(%rip), %rsi
	popq	%r12
	xorl	%eax, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	udata_printError_67@PLT
.L158:
	.cfi_restore_state
	cmpl	$0, -56(%rbp)
	movq	16(%rbx), %r12
	jg	.L118
	jmp	.L109
.L157:
	cmpl	$0, -56(%rbp)
	movq	16(%rbx), %r12
	jg	.L119
	jmp	.L100
.L94:
	movq	8(%rbx), %rdi
	jmp	.L95
.L159:
	cmpl	$0, -56(%rbp)
	movq	16(%rbx), %r14
	jg	.L120
	jmp	.L105
	.cfi_endproc
.LFE2448:
	.size	_ZL17ures_swapResourcePK12UDataSwapperPKjPjjPKcP9TempTableP10UErrorCode, .-_ZL17ures_swapResourcePK12UDataSwapperPKjPjjPKcP9TempTableP10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue6getIntER10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue6getIntER10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue6getIntER10UErrorCode:
.LFB2427:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L160
	movl	72(%rdi), %eax
	movl	%eax, %edx
	shrl	$28, %edx
	cmpl	$7, %edx
	je	.L162
	movl	$17, (%rsi)
.L162:
	sall	$4, %eax
	sarl	$4, %eax
.L160:
	ret
	.cfi_endproc
.LFE2427:
	.size	_ZNK6icu_6717ResourceDataValue6getIntER10UErrorCode, .-_ZNK6icu_6717ResourceDataValue6getIntER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue7getUIntER10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue7getUIntER10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue7getUIntER10UErrorCode:
.LFB2428:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L167
	movl	72(%rdi), %eax
	movl	%eax, %edx
	shrl	$28, %edx
	cmpl	$7, %edx
	je	.L169
	movl	$17, (%rsi)
.L169:
	andl	$268435455, %eax
.L167:
	ret
	.cfi_endproc
.LFE2428:
	.size	_ZNK6icu_6717ResourceDataValue7getUIntER10UErrorCode, .-_ZNK6icu_6717ResourceDataValue7getUIntER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode:
.LFB2431:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	movq	%rdi, %rax
	testl	%ecx, %ecx
	jle	.L175
	pxor	%xmm0, %xmm0
	movl	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	72(%rsi), %ecx
	movl	%ecx, %edi
	shrl	$28, %ecx
	andl	$268435455, %edi
	cmpl	$8, %ecx
	je	.L177
	cmpl	$9, %ecx
	je	.L178
	movl	$17, (%rdx)
	pxor	%xmm0, %xmm0
	movl	$0, 16(%rax)
	movups	%xmm0, (%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	testl	%edi, %edi
	je	.L181
	movq	16(%rsi), %rdx
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdi,4), %rdx
	leaq	4(%rdx), %rcx
	movl	(%rdx), %edx
	movq	%rcx, -8(%rbp)
.L180:
	movl	%edx, 16(%rax)
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, (%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movq	24(%rsi), %rdx
	movq	$0, -8(%rbp)
	leaq	(%rdx,%rdi,2), %rdx
	leaq	2(%rdx), %rcx
	movzwl	(%rdx), %edx
	movq	%rcx, %xmm0
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L181:
	movq	$0, -8(%rbp)
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	jmp	.L180
	.cfi_endproc
.LFE2431:
	.size	_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode, .-_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue8getTableER10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue8getTableER10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue8getTableER10UErrorCode:
.LFB2432:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	movq	%rdi, %rax
	testl	%ecx, %ecx
	jle	.L187
	pxor	%xmm0, %xmm0
	movl	$0, 32(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	72(%rsi), %ecx
	movl	%ecx, %edi
	shrl	$28, %ecx
	andl	$268435455, %edi
	cmpl	$4, %ecx
	je	.L189
	cmpl	$5, %ecx
	je	.L190
	cmpl	$2, %ecx
	je	.L198
	movl	$17, (%rdx)
	pxor	%xmm0, %xmm0
	movl	$0, 32(%rax)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	testl	%edi, %edi
	je	.L194
	movq	16(%rsi), %rdx
	pxor	%xmm1, %xmm1
	leaq	(%rdx,%rdi,4), %rdx
	leaq	4(%rdx), %rcx
	movslq	(%rdx), %rdx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	%rdx, %rcx
	leaq	(%rsi,%rdx,4), %rdi
	xorl	%edx, %edx
	movq	%rdi, -16(%rbp)
.L192:
	movq	%rdx, %xmm0
	movl	%ecx, 32(%rax)
	movhps	-8(%rbp), %xmm0
	movups	%xmm0, (%rax)
	movdqa	%xmm1, %xmm0
	movhps	-16(%rbp), %xmm0
	movups	%xmm0, 16(%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	testl	%edi, %edi
	je	.L194
	movq	16(%rsi), %rdx
	movq	$0, -8(%rbp)
	pxor	%xmm1, %xmm1
	leaq	(%rdx,%rdi,4), %rcx
	leaq	2(%rcx), %rdx
	movzwl	(%rcx), %ecx
	movl	%ecx, %esi
	notl	%esi
	andl	$1, %esi
	addq	%rcx, %rsi
	leaq	(%rdx,%rsi,2), %rdi
	movq	%rdi, -16(%rbp)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L190:
	movq	24(%rsi), %rdx
	movq	$0, -16(%rbp)
	movq	$0, -8(%rbp)
	leaq	(%rdx,%rdi,2), %rcx
	leaq	2(%rcx), %rdx
	movzwl	(%rcx), %ecx
	leaq	(%rdx,%rcx,2), %rsi
	movq	%rsi, %xmm1
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L194:
	movq	$0, -16(%rbp)
	xorl	%ecx, %ecx
	pxor	%xmm1, %xmm1
	xorl	%edx, %edx
	movq	$0, -8(%rbp)
	jmp	.L192
	.cfi_endproc
.LFE2432:
	.size	_ZNK6icu_6717ResourceDataValue8getTableER10UErrorCode, .-_ZNK6icu_6717ResourceDataValue8getTableER10UErrorCode
	.p2align 4
	.type	_ZN12_GLOBAL__N_114getStringArrayEPK12ResourceDataRKN6icu_6713ResourceArrayEPNS3_13UnicodeStringEiR10UErrorCode.part.0, @function
_ZN12_GLOBAL__N_114getStringArrayEPK12ResourceDataRKN6icu_6713ResourceArrayEPNS3_13UnicodeStringEiR10UErrorCode.part.0:
.LFB3113:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -80(%rbp)
	movl	16(%rsi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r12d, %r12d
	je	.L199
	cmpl	%ecx, %r12d
	jg	.L223
	testl	%r12d, %r12d
	jle	.L199
	leal	-1(%r12), %eax
	movq	%rdi, %r14
	movq	%rsi, %rbx
	movq	%rdx, %r13
	leaq	2(%rax,%rax), %rax
	xorl	%r15d, %r15d
	movq	%rax, -72(%rbp)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L205:
	cmpl	%ecx, %eax
	jge	.L207
	movq	40(%r14), %rdx
	leaq	(%rdx,%rax,2), %rdx
.L208:
	movzwl	(%rdx), %esi
	movl	%esi, %eax
	movl	%esi, %ecx
	andl	$64512, %eax
	cmpl	$56320, %eax
	je	.L209
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	call	u_strlen_67@PLT
	movq	-88(%rbp), %rdx
	movl	%eax, %ecx
.L210:
	movq	%rdx, -64(%rbp)
	movl	$1, %esi
	leaq	-64(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	addq	$64, %r13
	addq	$2, %r15
	cmpq	%r15, -72(%rbp)
	je	.L199
.L215:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L202
	movzwl	(%rax,%r15), %eax
	movl	52(%r14), %edx
	movl	48(%r14), %ecx
	cmpl	%edx, %eax
	jl	.L205
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, %edx
	shrl	$28, %edx
	andl	$-7, %edx
	je	.L224
.L204:
	movq	-80(%rbp), %rax
	xorl	%r12d, %r12d
	movl	$17, (%rax)
.L199:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movl	$15, (%r8)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L202:
	movq	8(%rbx), %rax
	movl	(%rax,%r15,2), %esi
	movl	%esi, %edx
	movl	%esi, %eax
	shrl	$28, %edx
	andl	$268435455, %eax
	cmpl	$6, %edx
	je	.L226
	cmpl	%eax, %esi
	jne	.L204
	xorl	%ecx, %ecx
	leaq	_ZL12gEmptyString(%rip), %rdx
	testl	%esi, %esi
	je	.L214
	movq	8(%r14), %rax
	leaq	(%rax,%rsi,4), %rdx
	movl	(%rdx), %ecx
.L214:
	addq	$4, %rdx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L209:
	cmpw	$-8210, %si
	ja	.L211
	andl	$1023, %ecx
	addq	$2, %rdx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L207:
	movq	16(%r14), %rdx
	subl	%ecx, %eax
	leaq	(%rdx,%rax,2), %rdx
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L224:
	andl	$268435455, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L211:
	movzwl	2(%rdx), %ecx
	cmpl	$57343, %esi
	je	.L212
	leal	-57327(%rsi), %eax
	addq	$4, %rdx
	sall	$16, %eax
	orl	%eax, %ecx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L212:
	movzwl	4(%rdx), %eax
	sall	$16, %ecx
	addq	$6, %rdx
	orl	%eax, %ecx
	jmp	.L210
.L225:
	call	__stack_chk_fail@PLT
.L226:
	movl	48(%r14), %ecx
	jmp	.L205
	.cfi_endproc
.LFE3113:
	.size	_ZN12_GLOBAL__N_114getStringArrayEPK12ResourceDataRKN6icu_6713ResourceArrayEPNS3_13UnicodeStringEiR10UErrorCode.part.0, .-_ZN12_GLOBAL__N_114getStringArrayEPK12ResourceDataRKN6icu_6713ResourceArrayEPNS3_13UnicodeStringEiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue14getStringArrayEPNS_13UnicodeStringEiR10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue14getStringArrayEPNS_13UnicodeStringEiR10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue14getStringArrayEPNS_13UnicodeStringEiR10UErrorCode:
.LFB2434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	leaq	_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L228
	movl	(%rcx), %edx
	testl	%edx, %edx
	jle	.L243
.L229:
	xorl	%eax, %eax
.L227:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L244
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movl	72(%rdi), %eax
	movl	%eax, %edx
	shrl	$28, %eax
	andl	$268435455, %edx
	cmpl	$8, %eax
	je	.L230
	cmpl	$9, %eax
	jne	.L245
	movq	24(%rdi), %rax
	movq	$0, -72(%rbp)
	leaq	(%rax,%rdx,2), %rax
	leaq	2(%rax), %rcx
	movzwl	(%rax), %eax
	movq	%rcx, %xmm0
.L233:
	movl	%eax, -48(%rbp)
	movhps	-72(%rbp), %xmm0
	movl	%r13d, %eax
	leaq	8(%rbx), %rdi
	movaps	%xmm0, -64(%rbp)
	shrl	$31, %eax
	testq	%r14, %r14
	jne	.L236
.L246:
	testl	%r13d, %r13d
	setne	%al
	testb	%al, %al
	je	.L237
.L247:
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	-64(%rbp), %rdi
	movq	%rcx, %rdx
	movq	%rbx, %rsi
	call	*%rax
	movl	(%r12), %eax
	leaq	8(%rbx), %rdi
	testl	%eax, %eax
	jg	.L229
	movl	%r13d, %eax
	shrl	$31, %eax
	testq	%r14, %r14
	je	.L246
.L236:
	testb	%al, %al
	jne	.L247
.L237:
	leaq	-64(%rbp), %rsi
	movq	%r12, %r8
	movl	%r13d, %ecx
	movq	%r14, %rdx
	call	_ZN12_GLOBAL__N_114getStringArrayEPK12ResourceDataRKN6icu_6713ResourceArrayEPNS3_13UnicodeStringEiR10UErrorCode.part.0
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$17, (%rcx)
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L230:
	testl	%edx, %edx
	je	.L240
	movq	16(%rdi), %rax
	pxor	%xmm0, %xmm0
	leaq	(%rax,%rdx,4), %rax
	leaq	4(%rax), %rsi
	movl	(%rax), %eax
	movq	%rsi, -72(%rbp)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L240:
	movq	$0, -72(%rbp)
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	jmp	.L233
.L244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2434:
	.size	_ZNK6icu_6717ResourceDataValue14getStringArrayEPNS_13UnicodeStringEiR10UErrorCode, .-_ZNK6icu_6717ResourceDataValue14getStringArrayEPNS_13UnicodeStringEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue9getStringERiR10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue9getStringERiR10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue9getStringERiR10UErrorCode:
.LFB2425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L248
	movl	72(%rdi), %ecx
	movq	%rsi, %rbx
	movl	%ecx, %esi
	movl	%ecx, %eax
	shrl	$28, %esi
	andl	$268435455, %eax
	cmpl	$6, %esi
	je	.L263
	cmpl	%eax, %ecx
	jne	.L264
	xorl	%eax, %eax
	leaq	_ZL12gEmptyString(%rip), %r12
	testl	%ecx, %ecx
	je	.L258
	movq	16(%rdi), %rax
	leaq	(%rax,%rcx,4), %r12
	movl	(%r12), %eax
.L258:
	movl	%eax, (%rbx)
	addq	$4, %r12
.L248:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	movl	56(%rdi), %edx
	cmpl	%eax, %edx
	jg	.L265
	subl	%edx, %eax
	movq	24(%rdi), %rdx
	leaq	(%rdx,%rax,2), %r12
.L252:
	movzwl	(%r12), %ecx
	movl	%ecx, %edx
	movl	%ecx, %eax
	andl	$64512, %edx
	cmpl	$56320, %edx
	je	.L253
	movq	%r12, %rdi
	call	u_strlen_67@PLT
	movl	%eax, (%rbx)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L265:
	movq	48(%rdi), %rdx
	leaq	(%rdx,%rax,2), %r12
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$0, (%rbx)
	movq	%r12, %rax
	movl	$17, (%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	cmpw	$-8210, %cx
	ja	.L255
	andl	$1023, %eax
	addq	$2, %r12
	movl	%eax, (%rbx)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L255:
	movzwl	2(%r12), %eax
	cmpl	$57343, %ecx
	je	.L256
	subl	$57327, %ecx
	addq	$4, %r12
	sall	$16, %ecx
	orl	%ecx, %eax
	movl	%eax, (%rbx)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L256:
	movzwl	4(%r12), %edx
	sall	$16, %eax
	addq	$6, %r12
	orl	%edx, %eax
	movl	%eax, (%rbx)
	jmp	.L248
	.cfi_endproc
.LFE2425:
	.size	_ZNK6icu_6717ResourceDataValue9getStringERiR10UErrorCode, .-_ZNK6icu_6717ResourceDataValue9getStringERiR10UErrorCode
	.p2align 4
	.globl	res_read_67
	.type	res_read_67, @function
res_read_67:
.LFB2409:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L302
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	cmpw	$19, (%rsi)
	jbe	.L299
	cmpw	$0, 4(%rsi)
	jne	.L299
	cmpb	$2, 6(%rsi)
	je	.L303
.L299:
	movl	$3, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	cmpw	$25938, 8(%rsi)
	jne	.L299
	cmpw	$17011, 10(%rsi)
	jne	.L299
	movzbl	12(%rsi), %eax
	subl	$1, %eax
	cmpb	$2, %al
	ja	.L299
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rdx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	12(%rsi), %ebx
	leaq	_ZL8gEmpty16(%rip), %rsi
	movq	%rdx, 8(%rdi)
	movl	%eax, 32(%rdi)
	movq	%rsi, 16(%rdi)
	testl	%ecx, %ecx
	js	.L270
	movl	%ecx, %r9d
	movl	$6, %esi
	sarl	$2, %r9d
	cmpb	$1, %bl
	jne	.L271
	movzbl	%bh, %esi
	cmpb	$1, %sil
	sbbl	%esi, %esi
	andl	$-5, %esi
	addl	$6, %esi
.L271:
	cmpl	%esi, %r9d
	jl	.L268
.L270:
	shrl	$28, %eax
	leal	-4(%rax), %esi
	cmpl	$1, %esi
	jbe	.L284
	cmpl	$2, %eax
	jne	.L268
.L284:
	cmpb	$1, %bl
	je	.L304
.L273:
	movl	4(%rdx), %esi
	movzbl	%sil, %eax
	cmpl	$4, %eax
	jle	.L268
	leal	1(%rax), %r9d
	testl	%ecx, %ecx
	js	.L275
	leal	0(,%r9,4), %r10d
	cmpl	%r10d, %ecx
	jl	.L268
	movl	16(%rdx), %r11d
	leal	0(,%r11,4), %r10d
	cmpl	%r10d, %ecx
	jl	.L268
.L275:
	movl	8(%rdx), %ecx
	cmpl	%r9d, %ecx
	jle	.L276
	sall	$2, %ecx
	movl	%ecx, 36(%rdi)
.L276:
	xorl	%r10d, %r10d
	cmpb	$2, %bl
	jbe	.L277
	movl	4(%rdx), %r10d
	shrl	$8, %r10d
	movl	%r10d, 48(%rdi)
.L277:
	cmpl	$5, %eax
	je	.L274
	movl	24(%rdx), %ecx
	movl	%ecx, %r9d
	andl	$1, %r9d
	movb	%r9b, 56(%rdi)
	movl	%ecx, %r9d
	shrl	%r9d
	andl	$1, %r9d
	movb	%r9b, 57(%rdi)
	movl	%ecx, %r9d
	shrl	$2, %r9d
	andl	$1, %r9d
	movb	%r9b, 58(%rdi)
	movl	%ecx, %r9d
	shrl	$16, %ecx
	sall	$12, %r9d
	movl	%ecx, 52(%rdi)
	andl	$251658240, %r9d
	orl	%r10d, %r9d
	movl	%r9d, 48(%rdi)
	testl	$16776960, 56(%rdi)
	je	.L279
	andl	$248, %esi
	je	.L268
.L279:
	cmpl	$6, %eax
	je	.L274
	movslq	8(%rdx), %rax
	cmpl	%eax, 28(%rdx)
	jle	.L274
	leaq	(%rdx,%rax,4), %rax
	movq	%rax, 16(%rdi)
.L274:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	$1, 59(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$3, (%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	testb	$-1, %bh
	jne	.L273
	movl	$65536, 36(%rdi)
	jmp	.L274
	.cfi_endproc
.LFE2409:
	.size	res_read_67, .-res_read_67
	.section	.rodata.str1.1
.LC4:
	.string	"res"
	.text
	.p2align 4
	.globl	res_load_67
	.type	res_load_67, @function
res_load_67:
.LFB2410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	leaq	_ZL12isAcceptablePvPKcS1_PK9UDataInfo(%rip), %rcx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-28(%rbp), %r8
	movq	%rsi, %rdi
	leaq	.LC4(%rip), %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, (%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 48(%rbx)
	call	udata_openChoice_67@PLT
	movl	(%r12), %edx
	movq	%rax, (%rbx)
	testl	%edx, %edx
	jle	.L347
.L305:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L348
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movq	%rax, %rdi
	call	udata_getMemory_67@PLT
	leaq	_ZL8gEmpty16(%rip), %rsi
	movl	(%rax), %edx
	movq	%rax, 8(%rbx)
	movq	%rsi, 16(%rbx)
	movl	%edx, 32(%rbx)
	shrl	$28, %edx
	leal	-4(%rdx), %ecx
	cmpl	$1, %ecx
	jbe	.L307
	cmpl	$2, %edx
	jne	.L320
.L307:
	movzbl	-28(%rbp), %ecx
	cmpb	$1, %cl
	je	.L349
.L310:
	movl	4(%rax), %esi
	movzbl	%sil, %edx
	cmpl	$4, %edx
	jle	.L320
	movl	8(%rax), %edi
	leal	1(%rdx), %r8d
	cmpl	%r8d, %edi
	jle	.L314
	sall	$2, %edi
	movl	%edi, 36(%rbx)
.L314:
	cmpb	$2, %cl
	jbe	.L315
	movl	4(%rax), %ecx
	shrl	$8, %ecx
	movl	%ecx, 48(%rbx)
.L315:
	cmpl	$5, %edx
	je	.L316
	movl	24(%rax), %ecx
	movl	%ecx, %edi
	andl	$1, %edi
	movb	%dil, 56(%rbx)
	movl	%ecx, %edi
	shrl	%edi
	andl	$1, %edi
	movb	%dil, 57(%rbx)
	movl	%ecx, %edi
	shrl	$2, %edi
	andl	$1, %edi
	movb	%dil, 58(%rbx)
	movl	%ecx, %edi
	shrl	$16, %ecx
	sall	$12, %edi
	movl	%ecx, 52(%rbx)
	andl	$251658240, %edi
	orl	%edi, 48(%rbx)
	testl	$16776960, 56(%rbx)
	je	.L317
	andl	$248, %esi
	je	.L320
.L317:
	cmpl	$6, %edx
	je	.L311
	movslq	8(%rax), %rdx
	cmpl	%edx, 28(%rax)
	jle	.L311
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, 16(%rbx)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L316:
	testl	$16776960, 56(%rbx)
	je	.L311
	andl	$248, %esi
	jne	.L311
	.p2align 4,,10
	.p2align 3
.L320:
	movq	(%rbx), %rdi
	movl	$3, (%r12)
	testq	%rdi, %rdi
	je	.L305
	call	udata_close_67@PLT
	movq	$0, (%rbx)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L349:
	cmpb	$0, -27(%rbp)
	jne	.L310
	movl	$65536, 36(%rbx)
.L311:
	movb	$1, 59(%rbx)
	jmp	.L305
.L348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2410:
	.size	res_load_67, .-res_load_67
	.p2align 4
	.globl	res_unload_67
	.type	res_unload_67, @function
res_unload_67:
.LFB2411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L350
	call	udata_close_67@PLT
	movq	$0, (%rbx)
.L350:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2411:
	.size	res_unload_67, .-res_unload_67
	.p2align 4
	.globl	res_getPublicType_67
	.type	res_getPublicType_67, @function
res_getPublicType_67:
.LFB2412:
	.cfi_startproc
	endbr64
	shrl	$28, %edi
	leaq	_ZL12gPublicTypes(%rip), %rax
	movsbl	(%rax,%rdi), %eax
	ret
	.cfi_endproc
.LFE2412:
	.size	res_getPublicType_67, .-res_getPublicType_67
	.p2align 4
	.globl	res_getStringNoTrace_67
	.type	res_getStringNoTrace_67, @function
res_getStringNoTrace_67:
.LFB2413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$268435455, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	movl	%esi, %edx
	shrl	$28, %edx
	cmpl	$6, %edx
	je	.L374
	cmpl	%eax, %esi
	movl	$0, %eax
	jne	.L367
	leaq	_ZL12gEmptyString(%rip), %r12
	testl	%esi, %esi
	je	.L365
	movq	8(%rdi), %rax
	movl	%esi, %esi
	leaq	(%rax,%rsi,4), %r12
	movl	(%r12), %eax
.L365:
	addq	$4, %r12
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L367:
	xorl	%r12d, %r12d
.L362:
	testq	%rbx, %rbx
	je	.L357
	movl	%eax, (%rbx)
.L357:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movl	48(%rdi), %edx
	cmpl	%eax, %edx
	jg	.L375
	subl	%edx, %eax
	movq	16(%rdi), %rdx
	leaq	(%rdx,%rax,2), %r12
.L360:
	movzwl	(%r12), %ecx
	movl	%ecx, %edx
	movl	%ecx, %eax
	andl	$64512, %edx
	cmpl	$56320, %edx
	je	.L361
	movq	%r12, %rdi
	call	u_strlen_67@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L375:
	movq	40(%rdi), %rdx
	leaq	(%rdx,%rax,2), %r12
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L361:
	cmpw	$-8210, %cx
	ja	.L363
	andl	$1023, %eax
	addq	$2, %r12
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L363:
	movzwl	2(%r12), %eax
	cmpl	$57343, %ecx
	je	.L364
	subl	$57327, %ecx
	addq	$4, %r12
	sall	$16, %ecx
	orl	%ecx, %eax
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L364:
	movzwl	4(%r12), %edx
	sall	$16, %eax
	addq	$6, %r12
	orl	%edx, %eax
	jmp	.L362
	.cfi_endproc
.LFE2413:
	.size	res_getStringNoTrace_67, .-res_getStringNoTrace_67
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue29getStringArrayOrStringAsArrayEPNS_13UnicodeStringEiR10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue29getStringArrayOrStringAsArrayEPNS_13UnicodeStringEiR10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue29getStringArrayOrStringAsArrayEPNS_13UnicodeStringEiR10UErrorCode:
.LFB2435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	72(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-8(%rsi), %eax
	cmpl	$1, %eax
	jbe	.L394
	movl	(%rcx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L376
	movl	%r14d, %eax
	shrl	$31, %eax
	testq	%r13, %r13
	je	.L395
.L386:
	testb	%al, %al
	jne	.L393
	testl	%r14d, %r14d
	jle	.L396
	leaq	-84(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	res_getStringNoTrace_67
	testq	%rax, %rax
	jne	.L397
	movl	$17, (%r12)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L376:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L398
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode(%rip), %rdx
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L378
	movl	(%rcx), %esi
	testl	%esi, %esi
	jle	.L399
.L379:
	xorl	%eax, %eax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L378:
	leaq	-80(%rbp), %r15
	movq	%rcx, %rdx
	movq	%rdi, %rsi
	movq	%r15, %rdi
	call	*%rax
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L379
	movl	%r14d, %eax
	shrl	$31, %eax
	testq	%r13, %r13
	je	.L400
.L381:
	testb	%al, %al
	jne	.L393
	leaq	8(%rbx), %rdi
	movq	%r12, %r8
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN12_GLOBAL__N_114getStringArrayEPK12ResourceDataRKN6icu_6713ResourceArrayEPNS3_13UnicodeStringEiR10UErrorCode.part.0
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L399:
	movl	$17, (%rcx)
	xorl	%eax, %eax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$15, (%r12)
	movl	$1, %eax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L400:
	testl	%r14d, %r14d
	setne	%al
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L395:
	testl	%r14d, %r14d
	setne	%al
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L397:
	movl	-84(%rbp), %ecx
	leaq	-80(%rbp), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movl	$1, %eax
	jmp	.L376
.L398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2435:
	.size	_ZNK6icu_6717ResourceDataValue29getStringArrayOrStringAsArrayEPNS_13UnicodeStringEiR10UErrorCode, .-_ZNK6icu_6717ResourceDataValue29getStringArrayOrStringAsArrayEPNS_13UnicodeStringEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717ResourceDataValue23getStringOrFirstOfArrayER10UErrorCode
	.type	_ZNK6icu_6717ResourceDataValue23getStringOrFirstOfArrayER10UErrorCode, @function
_ZNK6icu_6717ResourceDataValue23getStringOrFirstOfArrayER10UErrorCode:
.LFB2436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, 8(%rdi)
	movq	%rax, (%rdi)
	movl	(%rdx), %edi
	testl	%edi, %edi
	jle	.L439
.L401:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L440
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movq	%rdx, %rbx
	movl	72(%rsi), %edx
	movl	%edx, %ecx
	movl	%edx, %eax
	shrl	$28, %ecx
	andl	$268435455, %eax
	cmpl	$6, %ecx
	je	.L441
	cmpl	%eax, %edx
	jne	.L442
	xorl	%ecx, %ecx
	leaq	_ZL12gEmptyString(%rip), %rbx
	testl	%edx, %edx
	jne	.L443
.L414:
	addq	$4, %rbx
.L408:
	leaq	-48(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rbx, -48(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L441:
	movl	56(%rsi), %edx
	cmpl	%eax, %edx
	jle	.L405
	movq	48(%rsi), %rdx
	leaq	(%rdx,%rax,2), %rbx
.L406:
	movzwl	(%rbx), %edx
	movl	%edx, %eax
	movl	%edx, %ecx
	andl	$64512, %eax
	cmpl	$56320, %eax
	je	.L407
	movq	%rbx, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %ecx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L442:
	movq	(%rsi), %rdx
	movl	$0, -60(%rbp)
	movq	80(%rdx), %r8
	leaq	_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode(%rip), %rdx
	cmpq	%rdx, %r8
	jne	.L444
	cmpl	$8, %ecx
	je	.L415
	cmpl	$9, %ecx
	jne	.L418
	movq	24(%rsi), %rdx
	leaq	(%rdx,%rax,2), %rax
	cmpw	$0, (%rax)
	leaq	2(%rax), %rdx
	je	.L418
.L420:
	movzwl	(%rdx), %eax
	movl	60(%rsi), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	addl	56(%rsi), %edx
	cmpl	%ecx, %eax
	cmovge	%edx, %eax
	orl	$1610612736, %eax
	movl	%eax, %r8d
.L423:
	leaq	8(%rsi), %rdi
	leaq	-60(%rbp), %rdx
	movl	%r8d, %esi
	call	res_getStringNoTrace_67
	testq	%rax, %rax
	je	.L418
	movl	-60(%rbp), %ecx
	leaq	-56(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L415:
	testl	%eax, %eax
	je	.L418
	movq	16(%rsi), %rdx
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax), %edx
	leaq	4(%rax), %rcx
	testl	%edx, %edx
	jg	.L419
	.p2align 4,,10
	.p2align 3
.L418:
	movl	$17, (%rbx)
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L443:
	movq	16(%rsi), %rax
	leaq	(%rax,%rdx,4), %rbx
	movl	(%rbx), %ecx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L405:
	subl	%edx, %eax
	movq	24(%rsi), %rdx
	leaq	(%rdx,%rax,2), %rbx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L407:
	cmpw	$-8210, %dx
	ja	.L409
	andl	$1023, %ecx
	addq	$2, %rbx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L444:
	movq	%rsi, -72(%rbp)
	leaq	-48(%rbp), %rdi
	movq	%rbx, %rdx
	call	*%r8
	movl	(%rbx), %eax
	movq	-72(%rbp), %rsi
	testl	%eax, %eax
	jg	.L401
	cmpl	$0, -32(%rbp)
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %rcx
	jle	.L418
	testq	%rdx, %rdx
	jne	.L420
.L419:
	movl	(%rcx), %r8d
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L409:
	movzwl	2(%rbx), %ecx
	cmpl	$57343, %edx
	je	.L410
	leal	-57327(%rdx), %eax
	addq	$4, %rbx
	sall	$16, %eax
	orl	%eax, %ecx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L410:
	movzwl	4(%rbx), %eax
	sall	$16, %ecx
	addq	$6, %rbx
	orl	%eax, %ecx
	jmp	.L408
.L440:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2436:
	.size	_ZNK6icu_6717ResourceDataValue23getStringOrFirstOfArrayER10UErrorCode, .-_ZNK6icu_6717ResourceDataValue23getStringOrFirstOfArrayER10UErrorCode
	.p2align 4
	.globl	res_getAlias_67
	.type	res_getAlias_67, @function
res_getAlias_67:
.LFB2416:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	xorl	%ecx, %ecx
	shrl	$28, %eax
	cmpl	$3, %eax
	jne	.L449
	leaq	_ZL12gEmptyString(%rip), %rax
	andl	$268435455, %esi
	jne	.L455
.L447:
	addq	$4, %rax
.L446:
	testq	%rdx, %rdx
	je	.L445
	movl	%ecx, (%rdx)
.L445:
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	movq	8(%rdi), %rax
	movl	%esi, %esi
	leaq	(%rax,%rsi,4), %rax
	movl	(%rax), %ecx
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L449:
	xorl	%eax, %eax
	jmp	.L446
	.cfi_endproc
.LFE2416:
	.size	res_getAlias_67, .-res_getAlias_67
	.p2align 4
	.globl	res_getBinaryNoTrace_67
	.type	res_getBinaryNoTrace_67, @function
res_getBinaryNoTrace_67:
.LFB2417:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	xorl	%ecx, %ecx
	shrl	$28, %eax
	cmpl	$1, %eax
	jne	.L460
	leaq	_ZL8gEmpty32(%rip), %rax
	andl	$268435455, %esi
	jne	.L466
.L458:
	addq	$4, %rax
.L457:
	testq	%rdx, %rdx
	je	.L456
	movl	%ecx, (%rdx)
.L456:
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	movq	8(%rdi), %rax
	movl	%esi, %esi
	leaq	(%rax,%rsi,4), %rax
	movl	(%rax), %ecx
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L460:
	xorl	%eax, %eax
	jmp	.L457
	.cfi_endproc
.LFE2417:
	.size	res_getBinaryNoTrace_67, .-res_getBinaryNoTrace_67
	.p2align 4
	.globl	res_getIntVectorNoTrace_67
	.type	res_getIntVectorNoTrace_67, @function
res_getIntVectorNoTrace_67:
.LFB2418:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	xorl	%ecx, %ecx
	shrl	$28, %eax
	cmpl	$14, %eax
	jne	.L471
	leaq	_ZL8gEmpty32(%rip), %rax
	andl	$268435455, %esi
	jne	.L477
.L469:
	addq	$4, %rax
.L468:
	testq	%rdx, %rdx
	je	.L467
	movl	%ecx, (%rdx)
.L467:
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	movq	8(%rdi), %rax
	movl	%esi, %esi
	leaq	(%rax,%rsi,4), %rax
	movl	(%rax), %ecx
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L471:
	xorl	%eax, %eax
	jmp	.L468
	.cfi_endproc
.LFE2418:
	.size	res_getIntVectorNoTrace_67, .-res_getIntVectorNoTrace_67
	.p2align 4
	.globl	res_countArrayItems_67
	.type	res_countArrayItems_67, @function
res_countArrayItems_67:
.LFB2419:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	movl	%esi, %eax
	andl	$268435455, %edx
	shrl	$28, %eax
	cmpl	$-268435457, %esi
	ja	.L479
	leaq	.L481(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L481:
	.long	.L485-.L481
	.long	.L485-.L481
	.long	.L484-.L481
	.long	.L485-.L481
	.long	.L483-.L481
	.long	.L482-.L481
	.long	.L485-.L481
	.long	.L485-.L481
	.long	.L483-.L481
	.long	.L482-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L479-.L481
	.long	.L485-.L481
	.text
	.p2align 4,,10
	.p2align 3
.L485:
	movl	$1, %eax
.L478:
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	movq	16(%rdi), %rax
	movzwl	(%rax,%rdx,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L478
	movq	8(%rdi), %rax
	movl	(%rax,%rdx,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L478
	movq	8(%rdi), %rax
	movzwl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE2419:
	.size	res_countArrayItems_67, .-res_countArrayItems_67
	.p2align 4
	.globl	res_getTableItemByKey_67
	.type	res_getTableItemByKey_67, @function
res_getTableItemByKey_67:
.LFB2438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	movl	%esi, %edx
	movq	%rcx, -80(%rbp)
	andl	$268435455, %edx
	testq	%rcx, %rcx
	je	.L525
	movq	(%rcx), %r15
	testq	%r15, %r15
	je	.L525
	shrl	$28, %esi
	movq	%rdi, %r14
	cmpl	$4, %esi
	je	.L492
	cmpl	$5, %esi
	je	.L493
	movl	$-1, %eax
	cmpl	$2, %esi
	je	.L526
.L490:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movl	$-1, (%rax)
.L525:
	addq	$72, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L490
	movq	8(%rdi), %rax
	movl	$0, -72(%rbp)
	movq	%rax, -96(%rbp)
	leaq	(%rax,%rdx,4), %rax
	leaq	2(%rax), %rdi
	movzwl	(%rax), %eax
	movq	%rdi, -88(%rbp)
	movw	%ax, -98(%rbp)
	movl	%eax, %ebx
	movl	%eax, -104(%rbp)
	movl	%eax, -56(%rbp)
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L528:
	movq	-96(%rbp), %rax
	leaq	(%rax,%rdx), %r12
.L522:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L527
	movl	%ebx, -56(%rbp)
.L494:
	movl	-72(%rbp), %eax
	cmpl	%eax, %ebx
	jle	.L495
	addl	%eax, %ebx
	movq	-88(%rbp), %rax
	movslq	36(%r14), %rsi
	sarl	%ebx
	movslq	%ebx, %r13
	movzwl	(%rax,%r13,2), %edx
	cmpl	%esi, %edx
	jl	.L528
	subq	%rsi, %rdx
	addq	24(%r14), %rdx
	movq	%rdx, %r12
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L492:
	movl	$-1, %eax
	testl	%edx, %edx
	je	.L490
	movq	8(%rdi), %rax
	movq	%r14, -88(%rbp)
	xorl	%r13d, %r13d
	movq	%r15, %r14
	movq	%rax, -72(%rbp)
	leaq	(%rax,%rdx,4), %rax
	leaq	4(%rax), %rdi
	movl	(%rax), %eax
	movq	%rdi, -56(%rbp)
	movl	%eax, -96(%rbp)
	movl	%eax, %r15d
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L530:
	movq	-72(%rbp), %rdi
	leaq	(%rdi,%rax), %rbx
.L524:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L529
	movl	%r12d, %r15d
.L508:
	cmpl	%r13d, %r15d
	jle	.L495
	leal	(%r15,%r13), %r12d
	movq	-56(%rbp), %rdi
	sarl	%r12d
	movslq	%r12d, %rax
	movslq	(%rdi,%rax,4), %rax
	testl	%eax, %eax
	jns	.L530
	movq	-88(%rbp), %rdi
	andl	$2147483647, %eax
	addq	24(%rdi), %rax
	movq	%rax, %rbx
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L493:
	movq	16(%rdi), %rax
	movq	%r15, %r12
	movl	$0, -56(%rbp)
	leaq	(%rax,%rdx,2), %rax
	leaq	2(%rax), %rdi
	movzwl	(%rax), %eax
	movq	%rdi, -72(%rbp)
	movl	%eax, -88(%rbp)
	movl	%eax, %r15d
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L532:
	addq	8(%r14), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L531
.L503:
	movl	%ebx, %r15d
.L501:
	movl	-56(%rbp), %eax
	cmpl	%eax, %r15d
	jle	.L495
	leal	(%r15,%rax), %ebx
	movq	-72(%rbp), %rdi
	sarl	%ebx
	movslq	%ebx, %rax
	movzwl	(%rdi,%rax,2), %r13d
	movslq	36(%r14), %rax
	cmpl	%eax, %r13d
	jl	.L532
	subq	%rax, %r13
	addq	24(%r14), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	js	.L503
.L531:
	testl	%eax, %eax
	je	.L505
	leal	1(%rbx), %eax
	movl	%eax, -56(%rbp)
	jmp	.L501
.L505:
	movq	-80(%rbp), %rax
	movq	%r13, (%rax)
	movq	-64(%rbp), %rax
	movl	%ebx, (%rax)
	addl	-88(%rbp), %ebx
	movq	-72(%rbp), %rax
	movslq	%ebx, %rbx
	movl	52(%r14), %ecx
	movzwl	(%rax,%rbx,2), %eax
	movl	%eax, %edx
	subl	%ecx, %edx
	addl	48(%r14), %edx
	cmpl	%ecx, %eax
	cmovge	%edx, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	orl	$1610612736, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L512
	leal	1(%r12), %r13d
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L527:
	testl	%eax, %eax
	je	.L499
	leal	1(%rbx), %eax
	movl	-56(%rbp), %ebx
	movl	%eax, -72(%rbp)
	jmp	.L494
.L512:
	movq	-80(%rbp), %rax
	movq	%rbx, %rdx
	movl	%r12d, %ebx
	addl	-96(%rbp), %ebx
	movslq	%ebx, %rbx
	movq	%rdx, (%rax)
	movq	-64(%rbp), %rax
	movl	%r12d, (%rax)
	movq	-56(%rbp), %rax
	movl	(%rax,%rbx,4), %eax
	jmp	.L490
.L499:
	movq	-80(%rbp), %rax
	movzwl	-98(%rbp), %edx
	salq	$2, %r13
	movq	-88(%rbp), %rdi
	movq	%r12, (%rax)
	movq	-64(%rbp), %rax
	movl	%ebx, (%rax)
	movl	-104(%rbp), %eax
	notl	%eax
	andl	$1, %eax
	addq	%rdx, %rax
	leaq	0(%r13,%rax,2), %rax
	movl	(%rdi,%rax), %eax
	jmp	.L490
	.cfi_endproc
.LFE2438:
	.size	res_getTableItemByKey_67, .-res_getTableItemByKey_67
	.p2align 4
	.globl	res_getTableItemByIndex_67
	.type	res_getTableItemByIndex_67, @function
res_getTableItemByIndex_67:
.LFB2439:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	andl	$268435455, %r8d
	testl	%edx, %edx
	js	.L547
	shrl	$28, %esi
	cmpl	$4, %esi
	je	.L535
	cmpl	$5, %esi
	je	.L536
	movl	$-1, %eax
	cmpl	$2, %esi
	je	.L565
.L533:
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	testl	%r8d, %r8d
	je	.L533
	movq	8(%rdi), %r9
	leaq	(%r9,%r8,4), %r10
	movzwl	(%r10), %esi
	movl	%esi, %r8d
	cmpl	%esi, %edx
	jge	.L533
	notl	%esi
	movzwl	%r8w, %eax
	addq	$2, %r10
	movslq	%edx, %rdx
	andl	$1, %esi
	addq	%rsi, %rax
	addq	%rax, %rax
	testq	%rcx, %rcx
	je	.L537
	movzwl	(%r10,%rdx,2), %esi
	movslq	36(%rdi), %r11
	addq	%rsi, %r9
	cmpl	%r11d, %esi
	jl	.L539
	movq	24(%rdi), %r9
	subq	%r11, %rsi
	addq	%rsi, %r9
.L539:
	movq	%r9, (%rcx)
.L537:
	leaq	(%r10,%rdx,4), %rdx
	movl	(%rdx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	movq	16(%rdi), %rax
	leaq	(%rax,%r8,2), %r8
	movl	$-1, %eax
	movzwl	(%r8), %esi
	cmpl	%esi, %edx
	jge	.L533
	addq	$2, %r8
	testq	%rcx, %rcx
	je	.L540
	movslq	%edx, %rax
	movslq	36(%rdi), %r10
	movzwl	(%r8,%rax,2), %r9d
	movq	%r9, %rax
	cmpl	%r10d, %r9d
	jl	.L566
	subq	%r10, %rax
	addq	24(%rdi), %rax
.L542:
	movq	%rax, (%rcx)
.L540:
	addl	%esi, %edx
	movl	52(%rdi), %ecx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %eax
	movl	%eax, %edx
	subl	%ecx, %edx
	addl	48(%rdi), %edx
	cmpl	%ecx, %eax
	cmovge	%edx, %eax
	orl	$1610612736, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	movl	$-1, %eax
	testl	%r8d, %r8d
	je	.L533
	movq	8(%rdi), %r9
	leaq	(%r9,%r8,4), %rsi
	movl	(%rsi), %r8d
	cmpl	%r8d, %edx
	jge	.L533
	addq	$4, %rsi
	testq	%rcx, %rcx
	je	.L544
	movslq	%edx, %rax
	movslq	(%rsi,%rax,4), %rax
	addq	%rax, %r9
	testl	%eax, %eax
	jns	.L546
	andl	$2147483647, %eax
	addq	24(%rdi), %rax
	movq	%rax, %r9
.L546:
	movq	%r9, (%rcx)
.L544:
	addl	%r8d, %edx
	movslq	%edx, %rdx
	movl	(%rsi,%rdx,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	addq	8(%rdi), %rax
	jmp	.L542
	.cfi_endproc
.LFE2439:
	.size	res_getTableItemByIndex_67, .-res_getTableItemByIndex_67
	.p2align 4
	.globl	res_getResource_67
	.type	res_getResource_67, @function
res_getResource_67:
.LFB2440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -16(%rbp)
	movl	32(%rdi), %esi
	leaq	-20(%rbp), %rdx
	leaq	-16(%rbp), %rcx
	call	res_getTableItemByKey_67
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L570
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L570:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2440:
	.size	res_getResource_67, .-res_getResource_67
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE
	.type	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE, @function
_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE:
.LFB2441:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L571
	cmpl	%esi, 32(%rdi)
	jg	.L585
.L571:
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	testq	%rax, %rax
	je	.L573
	movzwl	(%rax,%rsi,2), %eax
	movslq	44(%rcx), %r9
	cmpl	%r9d, %eax
	jl	.L584
	subq	%r9, %rax
	addq	32(%rcx), %rax
.L578:
	movq	%rax, (%rdx)
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L579
	movzwl	(%rax,%rsi,2), %eax
	movl	60(%rcx), %esi
	movl	%eax, %edx
	subl	%esi, %edx
	addl	56(%rcx), %edx
	cmpl	%esi, %eax
	cmovge	%edx, %eax
	orl	$1610612736, %eax
.L581:
	movl	%eax, 72(%rcx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	movq	8(%rdi), %rax
	movslq	(%rax,%rsi,4), %rax
	testl	%eax, %eax
	js	.L577
.L584:
	addq	16(%rcx), %rax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L579:
	movq	24(%rdi), %rax
	movl	(%rax,%rsi,4), %eax
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L577:
	andl	$2147483647, %eax
	addq	32(%rcx), %rax
	jmp	.L578
	.cfi_endproc
.LFE2441:
	.size	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE, .-_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE
	.type	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE, @function
_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE:
.LFB2442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r14
	movl	32(%rdi), %r12d
	movq	%rdi, -72(%rbp)
	testq	%r14, %r14
	je	.L587
	movl	$0, -64(%rbp)
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L614:
	addq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L592
.L591:
	movl	-52(%rbp), %r12d
.L588:
	movl	-64(%rbp), %eax
	cmpl	%eax, %r12d
	jle	.L605
.L615:
	addl	%r12d, %eax
	sarl	%eax
	movslq	%eax, %r15
	movl	%eax, -52(%rbp)
	movslq	44(%rbx), %rax
	movzwl	(%r14,%r15,2), %esi
	cmpl	%eax, %esi
	jl	.L614
	subq	%rax, %rsi
	movq	%r13, %rdi
	addq	32(%rbx), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	js	.L591
.L592:
	testl	%eax, %eax
	je	.L593
	movl	-52(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -64(%rbp)
	movl	-64(%rbp), %eax
	cmpl	%eax, %r12d
	jg	.L615
.L605:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L616
	movzwl	(%rax,%r15,2), %eax
	movl	60(%rbx), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	addl	56(%rbx), %edx
	cmpl	%ecx, %eax
	cmovge	%edx, %eax
	orl	$1610612736, %eax
.L601:
	movl	%eax, 72(%rbx)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movl	$0, -52(%rbp)
	movq	%rax, -64(%rbp)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L617:
	addq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L598
.L597:
	movl	%r14d, %r12d
.L595:
	movl	-52(%rbp), %eax
	cmpl	%eax, %r12d
	jle	.L605
	addl	%r12d, %eax
	sarl	%eax
	movslq	%eax, %r15
	movq	-64(%rbp), %rax
	movq	%r15, %r14
	movslq	(%rax,%r15,4), %rsi
	testl	%esi, %esi
	jns	.L617
	andl	$2147483647, %esi
	movq	%r13, %rdi
	addq	32(%rbx), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	js	.L597
.L598:
	testl	%eax, %eax
	je	.L593
	leal	1(%r14), %eax
	movl	%eax, -52(%rbp)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L616:
	movq	-72(%rbp), %rax
	movq	24(%rax), %rax
	movl	(%rax,%r15,4), %eax
	jmp	.L601
	.cfi_endproc
.LFE2442:
	.size	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE, .-_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE
	.p2align 4
	.globl	res_getArrayItem_67
	.type	res_getArrayItem_67, @function
res_getArrayItem_67:
.LFB2443:
	.cfi_startproc
	endbr64
	movl	%esi, %ecx
	andl	$268435455, %ecx
	testl	%edx, %edx
	js	.L623
	shrl	$28, %esi
	cmpl	$8, %esi
	je	.L620
	cmpl	$9, %esi
	je	.L621
.L623:
	movl	$-1, %eax
.L618:
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	movq	16(%rdi), %rax
	leaq	(%rax,%rcx,2), %rcx
	movl	$-1, %eax
	movzwl	(%rcx), %esi
	cmpl	%edx, %esi
	jle	.L618
	addl	$1, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %eax
	movl	52(%rdi), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	addl	48(%rdi), %edx
	cmpl	%ecx, %eax
	cmovge	%edx, %eax
	orl	$1610612736, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L620:
	movl	$-1, %eax
	testl	%ecx, %ecx
	je	.L618
	movq	8(%rdi), %rsi
	leaq	(%rsi,%rcx,4), %rcx
	cmpl	%edx, (%rcx)
	jle	.L618
	addl	$1, %edx
	movslq	%edx, %rdx
	movl	(%rcx,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE2443:
	.size	res_getArrayItem_67, .-res_getArrayItem_67
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713ResourceArray19internalGetResourceEPK12ResourceDatai
	.type	_ZNK6icu_6713ResourceArray19internalGetResourceEPK12ResourceDatai, @function
_ZNK6icu_6713ResourceArray19internalGetResourceEPK12ResourceDatai:
.LFB2444:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	%edx, %rdx
	testq	%rax, %rax
	je	.L629
	movzwl	(%rax,%rdx,2), %eax
	movl	52(%rsi), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	addl	48(%rsi), %edx
	cmpl	%ecx, %eax
	cmovge	%edx, %eax
	orl	$1610612736, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L629:
	movq	8(%rdi), %rax
	movl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE2444:
	.size	_ZNK6icu_6713ResourceArray19internalGetResourceEPK12ResourceDatai, .-_ZNK6icu_6713ResourceArray19internalGetResourceEPK12ResourceDatai
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE
	.type	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE, @function
_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE:
.LFB2445:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L632
	cmpl	%esi, 16(%rdi)
	jg	.L639
.L632:
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	testq	%rax, %rax
	je	.L634
	movzwl	(%rax,%rsi,2), %eax
	movl	60(%rdx), %esi
	movl	%eax, %ecx
	subl	%esi, %ecx
	addl	56(%rdx), %ecx
	cmpl	%esi, %eax
	cmovge	%ecx, %eax
	orl	$1610612736, %eax
.L636:
	movl	%eax, 72(%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	movq	8(%rdi), %rax
	movl	(%rax,%rsi,4), %eax
	jmp	.L636
	.cfi_endproc
.LFE2445:
	.size	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE, .-_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE
	.p2align 4
	.globl	res_findResource_67
	.type	res_findResource_67, @function
res_findResource_67:
.LFB2446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	(%rdx), %r15
	movq	%rdx, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movl	$0, -68(%rbp)
	cmpb	$0, (%r15)
	je	.L640
	movl	%esi, %r14d
	shrl	$28, %r14d
	cmpl	$2, %r14d
	je	.L642
	cmpl	$5, %r14d
	je	.L642
	cmpl	$4, %r14d
	je	.L642
	leal	-8(%r14), %eax
	cmpl	$1, %eax
	ja	.L644
.L642:
	leaq	-68(%rbp), %rax
	movl	%esi, %r12d
	movq	%rax, -112(%rbp)
	cmpl	$-1, %r12d
	je	.L644
	.p2align 4,,10
	.p2align 3
.L681:
	leal	-4(%r14), %ebx
	testl	$-6, %ebx
	je	.L661
	cmpl	$2, %r14d
	je	.L661
.L640:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L679
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	.cfi_restore_state
	movl	$47, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L646
	cmpq	%r15, %rax
	je	.L644
	movq	-88(%rbp), %rcx
	movb	$0, (%rax)
	leaq	1(%rax), %rax
	movq	%rax, (%rcx)
.L647:
	cmpl	$2, %r14d
	je	.L662
	cmpl	$1, %ebx
	jbe	.L662
	leal	-8(%r14), %edx
	cmpl	$1, %edx
	jbe	.L680
	movq	%rax, %r15
	movl	$15, %r14d
	movl	$-1, %r12d
.L651:
	testq	%r13, %r13
	je	.L640
	cmpb	$0, (%r15)
	je	.L640
	cmpl	$-1, %r12d
	jne	.L681
.L644:
	movl	$-1, %r12d
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L662:
	movq	-96(%rbp), %rax
	movq	-112(%rbp), %rdx
	movl	%r12d, %esi
	movq	-104(%rbp), %rdi
	movq	%r15, (%rax)
	movq	%rax, %rcx
	call	res_getTableItemByKey_67
	cmpl	$-1, %eax
	je	.L650
	movq	-88(%rbp), %rcx
	movl	%eax, %r14d
	movl	%eax, %r12d
	shrl	$28, %r14d
	movq	(%rcx), %r15
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L646:
	movq	%r15, %rdi
	call	strlen@PLT
	movq	-88(%rbp), %rdx
	addq	%r15, %rax
	movq	%rax, (%rdx)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L650:
	leaq	-64(%rbp), %rsi
	movl	$10, %edx
	movq	%r15, %rdi
	call	strtol@PLT
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L653
	movq	-64(%rbp), %rdx
	cmpb	$0, (%rdx)
	je	.L682
.L653:
	movq	-88(%rbp), %rax
	movl	$-1, %r12d
	movl	$15, %r14d
	movq	(%rax), %r15
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L680:
	leaq	-64(%rbp), %rsi
	movl	$10, %edx
	movq	%r15, %rdi
	call	strtol@PLT
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	js	.L660
	movq	-64(%rbp), %rdx
	cmpb	$0, (%rdx)
	jne	.L660
	movq	-104(%rbp), %rdi
	movl	%r12d, %esi
	movl	%eax, %edx
	call	res_getArrayItem_67
	movl	%eax, %r14d
	movl	%eax, %r12d
	shrl	$28, %r14d
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L660:
	movl	$15, %r14d
	movl	$-1, %r12d
.L654:
	movq	-96(%rbp), %rax
	movq	$0, (%rax)
	movq	-88(%rbp), %rax
	movq	(%rax), %r15
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L682:
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdi
	movl	%r12d, %esi
	movl	%eax, %edx
	call	res_getTableItemByIndex_67
	movl	%eax, %r14d
	movl	%eax, %r12d
	movq	-88(%rbp), %rax
	shrl	$28, %r14d
	movq	(%rax), %r15
	jmp	.L651
.L679:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2446:
	.size	res_findResource_67, .-res_findResource_67
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"ures_swap(): data format %02x.%02x.%02x.%02x (format version %02x.%02x) is not a resource bundle\n"
	.align 8
.LC6:
	.string	"ures_swap(): too few bytes (%d after header) for a resource bundle\n"
	.align 8
.LC7:
	.string	"ures_swap(): too few indexes for a 1.1+ resource bundle\n"
	.align 8
.LC8:
	.string	"ures_swap(): resource top %d exceeds bundle length %d\n"
	.align 8
.LC9:
	.string	"ures_swap(): unable to allocate memory for tracking resources\n"
	.align 8
.LC10:
	.string	"ures_swap().udata_swapInvStringBlock(keys[%d]) failed\n"
	.align 8
.LC11:
	.string	"ures_swap().swapArray16(16-bit units[%d]) failed\n"
	.align 8
.LC12:
	.string	"ures_swap(): unable to allocate memory for sorting tables (max length: %d)\n"
	.align 8
.LC13:
	.string	"ures_swapResource(root res=%08x) failed\n"
	.text
	.p2align 4
	.globl	ures_swap_67
	.type	ures_swap_67, @function
ures_swap_67:
.LFB2449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$3336, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	udata_swapDataHeader_67@PLT
	testq	%r12, %r12
	je	.L710
	movl	(%r12), %r11d
	movl	%eax, %r10d
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jg	.L683
	cmpw	$25938, 12(%rbx)
	movzbl	16(%rbx), %eax
	jne	.L687
	cmpw	$17011, 14(%rbx)
	je	.L723
.L687:
	movzbl	17(%rbx), %esi
.L686:
	movzbl	13(%rbx), %ecx
	movzbl	12(%rbx), %edx
	pushq	%rsi
	movq	%r14, %rdi
	pushq	%rax
	movzbl	15(%rbx), %r9d
	xorl	%eax, %eax
	leaq	.LC5(%rip), %rsi
	movzbl	14(%rbx), %r8d
	call	udata_printError_67@PLT
	movl	$16, (%r12)
	popq	%r8
	xorl	%eax, %eax
	popq	%r9
.L683:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L724
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L723:
	.cfi_restore_state
	cmpb	$1, %al
	je	.L725
	leal	-2(%rax), %esi
	cmpb	$1, %sil
	ja	.L687
.L689:
	movb	%al, -3276(%rbp)
	testl	%r15d, %r15d
	js	.L712
	movl	%r15d, %edx
	subl	%r10d, %edx
	leal	3(%rdx), %eax
	cmovns	%edx, %eax
	sarl	$2, %eax
	movl	%eax, -3336(%rbp)
	cmpl	$23, %edx
	jle	.L726
.L690:
	movslq	%r10d, %rax
	movl	%r10d, -3324(%rbp)
	movq	%rax, -3352(%rbp)
	addq	%rbx, %rax
	movl	(%rax), %edi
	movq	%rax, -3320(%rbp)
	call	*16(%r14)
	movq	%r14, %rdi
	movl	%eax, -3356(%rbp)
	movq	-3320(%rbp), %rax
	movl	4(%rax), %esi
	call	udata_readInt32_67@PLT
	movl	-3324(%rbp), %r10d
	movzbl	%al, %r8d
	cmpl	$4, %r8d
	jle	.L727
	leal	1(%r8), %eax
	movq	%r14, %rdi
	movl	%r10d, -3344(%rbp)
	movl	%eax, -3340(%rbp)
	movq	-3320(%rbp), %rax
	movl	%r8d, -3324(%rbp)
	movl	8(%rax), %esi
	call	udata_readInt32_67@PLT
	movl	-3324(%rbp), %r8d
	movl	-3344(%rbp), %r10d
	movl	%eax, -3328(%rbp)
	cmpl	$6, %r8d
	movl	%eax, -3360(%rbp)
	jg	.L728
.L692:
	movq	-3320(%rbp), %rax
	movq	%r14, %rdi
	movl	%r10d, -3344(%rbp)
	movl	16(%rax), %esi
	call	udata_readInt32_67@PLT
	movq	-3320(%rbp), %rdi
	movl	%eax, -3324(%rbp)
	movl	20(%rdi), %esi
	movq	%r14, %rdi
	call	udata_readInt32_67@PLT
	movl	-3336(%rbp), %edi
	movl	-3344(%rbp), %r10d
	movl	%eax, -3364(%rbp)
	testl	%edi, %edi
	js	.L693
	movl	-3324(%rbp), %eax
	cmpl	%eax, %edi
	jge	.L693
	movl	%edi, %ecx
	movl	%eax, %edx
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	movl	$8, (%r12)
	xorl	%eax, %eax
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L693:
	movl	-3328(%rbp), %edi
	movl	$0, %esi
	cmpl	%edi, -3340(%rbp)
	leal	0(,%rdi,4), %eax
	cmovge	%esi, %eax
	movl	%eax, -3280(%rbp)
	testl	%r15d, %r15d
	js	.L695
	movq	-3352(%rbp), %rax
	addq	%r13, %rax
	movq	%rax, -3336(%rbp)
	leal	31(%r15), %eax
	sarl	$5, %eax
	leal	3(%rax), %eax
	andl	$-4, %eax
	movslq	%eax, %r15
	cmpl	$800, %eax
	jg	.L696
	leaq	-2464(%rbp), %rdi
	movq	%rdi, -3288(%rbp)
.L697:
	xorl	%esi, %esi
	movq	%r15, %rdx
	movl	%r10d, -3344(%rbp)
	call	memset@PLT
	cmpq	%r13, %rbx
	movl	-3344(%rbp), %r10d
	je	.L699
	movl	-3324(%rbp), %eax
	movq	-3320(%rbp), %rsi
	movq	-3336(%rbp), %rdi
	leal	0(,%rax,4), %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	movl	-3344(%rbp), %r10d
.L699:
	movslq	-3340(%rbp), %rsi
	movl	-3328(%rbp), %eax
	movq	%r14, %rdi
	movq	%r12, %r8
	movl	%r10d, -3344(%rbp)
	movq	%rsi, %rbx
	salq	$2, %rsi
	subl	%ebx, %eax
	leal	0(,%rax,4), %r13d
	movq	-3336(%rbp), %rax
	movl	%r13d, %edx
	leaq	(%rax,%rsi), %rcx
	addq	-3320(%rbp), %rsi
	call	udata_swapInvStringBlock_67@PLT
	movl	(%r12), %edi
	movl	-3344(%rbp), %r10d
	testl	%edi, %edi
	jg	.L729
	movl	-3360(%rbp), %ebx
	movl	-3328(%rbp), %edx
	cmpl	%edx, %ebx
	jle	.L701
	movq	-3336(%rbp), %rdi
	movslq	%edx, %rax
	subl	%edx, %ebx
	movq	%r12, %r8
	salq	$2, %rax
	movl	%r10d, -3328(%rbp)
	leal	0(,%rbx,4), %edx
	leaq	(%rdi,%rax), %rcx
	movq	-3320(%rbp), %rdi
	leaq	(%rdi,%rax), %rsi
	movq	%r14, %rdi
	call	*48(%r14)
	movl	(%r12), %esi
	movl	-3328(%rbp), %r10d
	testl	%esi, %esi
	jg	.L730
.L701:
	movq	-3336(%rbp), %rax
	cmpb	$1, -3276(%rbp)
	movq	%rax, -3312(%rbp)
	ja	.L715
	cmpl	$200, -3364(%rbp)
	jg	.L702
.L715:
	leaq	-1664(%rbp), %rbx
	leaq	-3264(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -3304(%rbp)
.L704:
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	-3356(%rbp), %r15d
	pushq	%r12
	movq	-3336(%rbp), %rdx
	leaq	-3312(%rbp), %r9
	movq	-3320(%rbp), %rsi
	movl	%r15d, %ecx
	movl	%r10d, -3328(%rbp)
	call	_ZL17ures_swapResourcePK12UDataSwapperPKjPjjPKcP9TempTableP10UErrorCode
	movl	(%r12), %ecx
	popq	%rax
	movl	-3328(%rbp), %r10d
	popq	%rdx
	testl	%ecx, %ecx
	jle	.L706
	movl	%r15d, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	-3328(%rbp), %r10d
.L706:
	movq	-3304(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L707
	movl	%r10d, -3328(%rbp)
	call	uprv_free_67@PLT
	movl	-3328(%rbp), %r10d
.L707:
	movq	-3288(%rbp), %rdi
	leaq	-2464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L708
	movl	%r10d, -3328(%rbp)
	call	uprv_free_67@PLT
	movl	-3328(%rbp), %r10d
.L708:
	movl	-3340(%rbp), %edx
	movq	%r12, %r8
	movq	%r14, %rdi
	movl	%r10d, -3328(%rbp)
	movq	-3336(%rbp), %rcx
	movq	-3320(%rbp), %rsi
	sall	$2, %edx
	call	*56(%r14)
	movl	-3328(%rbp), %r10d
.L695:
	movl	-3324(%rbp), %eax
	leal	(%r10,%rax,4), %eax
	jmp	.L683
.L731:
	movl	-3364(%rbp), %edx
	movq	%r14, %rdi
	leaq	.LC12(%rip), %rsi
	call	udata_printError_67@PLT
	movq	-3288(%rbp), %rdi
	leaq	-2464(%rbp), %rax
	movl	$7, (%r12)
	cmpq	%rax, %rdi
	je	.L710
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L710:
	xorl	%eax, %eax
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L725:
	movzbl	17(%rbx), %esi
	testb	%sil, %sil
	je	.L686
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L728:
	movq	-3320(%rbp), %rax
	movq	%r14, %rdi
	movl	%r10d, -3324(%rbp)
	movl	28(%rax), %esi
	call	udata_readInt32_67@PLT
	movl	-3324(%rbp), %r10d
	movl	%eax, -3360(%rbp)
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L712:
	movl	$-1, -3336(%rbp)
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L727:
	xorl	%eax, %eax
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	movl	$8, (%r12)
	xorl	%eax, %eax
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L726:
	xorl	%eax, %eax
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	movl	$8, (%r12)
	xorl	%eax, %eax
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%r15, %rdi
	movl	%r10d, -3344(%rbp)
	call	uprv_malloc_67@PLT
	movl	-3344(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, -3288(%rbp)
	movq	%rax, %rdi
	jne	.L697
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rsi
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	movl	$7, (%r12)
	xorl	%eax, %eax
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L729:
	xorl	%eax, %eax
	movl	%r13d, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	xorl	%eax, %eax
	jmp	.L683
.L730:
	xorl	%eax, %eax
	leal	(%rbx,%rbx), %edx
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	xorl	%eax, %eax
	jmp	.L683
.L702:
	movslq	-3364(%rbp), %rax
	movl	%r10d, -3328(%rbp)
	movq	%rax, %rdi
	leaq	0(,%rax,8), %rbx
	sall	$2, %edi
	movslq	%edi, %rdi
	addq	%rbx, %rdi
	call	uprv_malloc_67@PLT
	movl	-3328(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, -3304(%rbp)
	je	.L731
	addq	%rbx, %rax
	leaq	-1664(%rbp), %rbx
	movq	%rax, -3296(%rbp)
	jmp	.L704
.L724:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2449:
	.size	ures_swap_67, .-ures_swap_67
	.weak	_ZTSN6icu_6717ResourceDataValueE
	.section	.rodata._ZTSN6icu_6717ResourceDataValueE,"aG",@progbits,_ZTSN6icu_6717ResourceDataValueE,comdat
	.align 16
	.type	_ZTSN6icu_6717ResourceDataValueE, @object
	.size	_ZTSN6icu_6717ResourceDataValueE, 29
_ZTSN6icu_6717ResourceDataValueE:
	.string	"N6icu_6717ResourceDataValueE"
	.weak	_ZTIN6icu_6717ResourceDataValueE
	.section	.data.rel.ro._ZTIN6icu_6717ResourceDataValueE,"awG",@progbits,_ZTIN6icu_6717ResourceDataValueE,comdat
	.align 8
	.type	_ZTIN6icu_6717ResourceDataValueE, @object
	.size	_ZTIN6icu_6717ResourceDataValueE, 24
_ZTIN6icu_6717ResourceDataValueE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717ResourceDataValueE
	.quad	_ZTIN6icu_6713ResourceValueE
	.weak	_ZTVN6icu_6717ResourceDataValueE
	.section	.data.rel.ro._ZTVN6icu_6717ResourceDataValueE,"awG",@progbits,_ZTVN6icu_6717ResourceDataValueE,comdat
	.align 8
	.type	_ZTVN6icu_6717ResourceDataValueE, @object
	.size	_ZTVN6icu_6717ResourceDataValueE, 144
_ZTVN6icu_6717ResourceDataValueE:
	.quad	0
	.quad	_ZTIN6icu_6717ResourceDataValueE
	.quad	_ZN6icu_6717ResourceDataValueD1Ev
	.quad	_ZN6icu_6717ResourceDataValueD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717ResourceDataValue7getTypeEv
	.quad	_ZNK6icu_6717ResourceDataValue9getStringERiR10UErrorCode
	.quad	_ZNK6icu_6717ResourceDataValue14getAliasStringERiR10UErrorCode
	.quad	_ZNK6icu_6717ResourceDataValue6getIntER10UErrorCode
	.quad	_ZNK6icu_6717ResourceDataValue7getUIntER10UErrorCode
	.quad	_ZNK6icu_6717ResourceDataValue12getIntVectorERiR10UErrorCode
	.quad	_ZNK6icu_6717ResourceDataValue9getBinaryERiR10UErrorCode
	.quad	_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode
	.quad	_ZNK6icu_6717ResourceDataValue8getTableER10UErrorCode
	.quad	_ZNK6icu_6717ResourceDataValue21isNoInheritanceMarkerEv
	.quad	_ZNK6icu_6717ResourceDataValue14getStringArrayEPNS_13UnicodeStringEiR10UErrorCode
	.quad	_ZNK6icu_6717ResourceDataValue29getStringArrayOrStringAsArrayEPNS_13UnicodeStringEiR10UErrorCode
	.quad	_ZNK6icu_6717ResourceDataValue23getStringOrFirstOfArrayER10UErrorCode
	.section	.rodata
	.align 16
	.type	_ZL16gCollationBinKey, @object
	.size	_ZL16gCollationBinKey, 30
_ZL16gCollationBinKey:
	.value	37
	.value	37
	.value	67
	.value	111
	.value	108
	.value	108
	.value	97
	.value	116
	.value	105
	.value	111
	.value	110
	.value	66
	.value	105
	.value	110
	.value	0
	.align 16
	.type	_ZL12gPublicTypes, @object
	.size	_ZL12gPublicTypes, 16
_ZL12gPublicTypes:
	.string	""
	.string	"\001\002\003\002\002"
	.ascii	"\007\b\b\377\377\377\377\016\377"
	.align 8
	.type	_ZL12gEmptyString, @object
	.size	_ZL12gEmptyString, 8
_ZL12gEmptyString:
	.zero	8
	.align 8
	.type	_ZL8gEmpty32, @object
	.size	_ZL8gEmpty32, 8
_ZL8gEmpty32:
	.zero	8
	.align 2
	.type	_ZL8gEmpty16, @object
	.size	_ZL8gEmpty16, 2
_ZL8gEmpty16:
	.zero	2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
