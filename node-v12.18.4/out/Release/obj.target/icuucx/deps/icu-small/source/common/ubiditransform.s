	.file	"ubiditransform.cpp"
	.text
	.p2align 4
	.type	_ZL18action_setRunsOnlyP14UBiDiTransformP10UErrorCode, @function
_ZL18action_setRunsOnlyP14UBiDiTransformP10UErrorCode:
.LFB2099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ubidi_setReorderingMode_67@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2099:
	.size	_ZL18action_setRunsOnlyP14UBiDiTransformP10UErrorCode, .-_ZL18action_setRunsOnlyP14UBiDiTransformP10UErrorCode
	.p2align 4
	.type	_ZL17action_setInverseP14UBiDiTransformP10UErrorCode, @function
_ZL17action_setInverseP14UBiDiTransformP10UErrorCode:
.LFB2098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	ubidi_setInverse_67@PLT
	movq	(%rbx), %rdi
	movl	$5, %esi
	call	ubidi_setReorderingMode_67@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2098:
	.size	_ZL17action_setInverseP14UBiDiTransformP10UErrorCode, .-_ZL17action_setInverseP14UBiDiTransformP10UErrorCode
	.p2align 4
	.type	_ZL14action_reverseP14UBiDiTransformP10UErrorCode, @function
_ZL14action_reverseP14UBiDiTransformP10UErrorCode:
.LFB2100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdx
	movl	40(%rdi), %ecx
	movl	32(%rdi), %esi
	movq	16(%rdi), %rdi
	call	ubidi_writeReverse_67@PLT
	movq	48(%rbx), %rax
	movl	32(%rbx), %edx
	movl	%edx, (%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2100:
	.size	_ZL14action_reverseP14UBiDiTransformP10UErrorCode, .-_ZL14action_reverseP14UBiDiTransformP10UErrorCode
	.p2align 4
	.type	_ZL14action_reorderP14UBiDiTransformP10UErrorCode, @function
_ZL14action_reorderP14UBiDiTransformP10UErrorCode:
.LFB2097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	40(%rdi), %edx
	movzwl	56(%rdi), %ecx
	movq	24(%rdi), %rsi
	movq	(%rdi), %rdi
	call	ubidi_writeReordered_67@PLT
	movq	48(%rbx), %rax
	movl	32(%rbx), %edx
	movl	%edx, (%rax)
	movl	$1, %eax
	movl	$0, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2097:
	.size	_ZL14action_reorderP14UBiDiTransformP10UErrorCode, .-_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.p2align 4
	.type	_ZL14action_resolveP14UBiDiTransformP10UErrorCode, @function
_ZL14action_resolveP14UBiDiTransformP10UErrorCode:
.LFB2096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rax
	movq	%rsi, %r9
	xorl	%r8d, %r8d
	movl	32(%rdi), %edx
	movq	16(%rdi), %rsi
	movzbl	24(%rax), %ecx
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ubidi_setPara_67@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2096:
	.size	_ZL14action_resolveP14UBiDiTransformP10UErrorCode, .-_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.p2align 4
	.type	_ZL13action_mirrorP14UBiDiTransformP10UErrorCode, @function
_ZL13action_mirrorP14UBiDiTransformP10UErrorCode:
.LFB2104:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testb	$2, 56(%rdi)
	je	.L35
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	32(%rdi), %ecx
	cmpl	%ecx, 40(%rdi)
	jnb	.L14
	movl	$15, (%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	32(%rbx), %r9d
	movl	%r9d, %r11d
	cmpl	%r9d, %ecx
	je	.L15
	movl	%ecx, %edi
	movzwl	(%r10,%rdi,2), %r10d
	movl	%r10d, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	je	.L39
.L15:
	testb	%al, %al
	je	.L40
.L24:
	movl	%r12d, %edi
	movq	%r8, -64(%rbp)
	movl	%ecx, -52(%rbp)
	call	u_charMirror_67@PLT
	movl	-52(%rbp), %ecx
	movq	-64(%rbp), %r8
	cmpl	$65535, %eax
	ja	.L41
	movl	%r12d, %edi
	movq	%r8, -64(%rbp)
	movl	%ecx, -52(%rbp)
	call	u_charMirror_67@PLT
	movl	-52(%rbp), %ecx
	movl	32(%rbx), %r11d
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %r8
	movl	%eax, %edx
	movl	%ecx, %r13d
.L21:
	movl	%r11d, %r9d
	movw	%dx, (%rdi,%r8,2)
	movl	%r15d, %r14d
	cmpl	%r9d, %r13d
	jnb	.L42
.L14:
	movq	(%rbx), %rdi
	movl	%r13d, %esi
	leal	1(%r14), %r15d
	call	ubidi_getLevelAt_67@PLT
	movq	16(%rbx), %r10
	movl	%r13d, %edx
	movl	%r14d, %r8d
	andl	$1, %eax
	leal	1(%r13), %ecx
	leaq	(%r8,%r8), %rsi
	movzwl	(%r10,%rdx,2), %r12d
	movl	%r12d, %edi
	movl	%r12d, %edx
	andl	$64512, %edi
	cmpl	$55296, %edi
	je	.L43
	testb	%al, %al
	jne	.L24
.L40:
	movl	32(%rbx), %r11d
	movq	24(%rbx), %rdi
.L25:
	movl	%ecx, %r13d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%r12d, %edi
	movl	%ecx, -52(%rbp)
	movq	%r8, -64(%rbp)
	call	u_charMirror_67@PLT
	movq	24(%rbx), %rdx
	movq	-64(%rbp), %r8
	movl	%r12d, %edi
	sarl	$10, %eax
	subw	$10304, %ax
	movw	%ax, (%rdx,%r8,2)
	call	u_charMirror_67@PLT
	movl	-52(%rbp), %ecx
	movl	32(%rbx), %r9d
	movl	%eax, %r12d
	movq	24(%rbx), %rax
	andw	$1023, %r12w
	movl	%ecx, %r13d
	orw	$-9216, %r12w
.L23:
	movw	%r12w, (%rax,%r15,2)
	addl	$2, %r14d
	cmpl	%r9d, %r13d
	jb	.L14
.L42:
	movq	48(%rbx), %rax
	movl	%r9d, (%rax)
	movl	$1, %eax
	movl	$0, 56(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	sall	$10, %r12d
	addl	$2, %r13d
	leal	-56613888(%r10,%r12), %r12d
	testb	%al, %al
	jne	.L44
	movq	24(%rbx), %rax
	movq	%rax, %rdi
	addq	%rax, %rsi
	cmpl	$65535, %r12d
	jbe	.L45
	movl	%r12d, %edx
	andw	$1023, %r12w
	sarl	$10, %edx
	orw	$-9216, %r12w
	subw	$10304, %dx
	movw	%dx, (%rsi)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L44:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	%r13d, %ecx
	jmp	.L24
.L45:
	movl	%r12d, %edx
	movl	%r13d, %ecx
	jmp	.L25
	.cfi_endproc
.LFE2104:
	.size	_ZL13action_mirrorP14UBiDiTransformP10UErrorCode, .-_ZL13action_mirrorP14UBiDiTransformP10UErrorCode
	.p2align 4
	.type	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode, @function
_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode:
.LFB2103:
	.cfi_startproc
	endbr64
	movl	60(%rdi), %eax
	xorl	%r9d, %r9d
	movl	%eax, %r8d
	orl	64(%rdi), %r8d
	je	.L60
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	movq	%r12, %r9
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdx
	movl	40(%rdi), %r11d
	movq	24(%rdi), %r10
	movl	32(%rdi), %esi
	movl	20(%rdx), %ecx
	movl	16(%rdx), %edx
	movq	16(%rdi), %rdi
	cmpl	%edx, %ecx
	je	.L64
	orl	%edx, %eax
	movl	%r11d, %ecx
	movq	%r10, %rdx
	movl	%eax, %r8d
	call	u_shapeArabic_67@PLT
	movl	$1, %r9d
	movl	%eax, %r13d
	movq	48(%rbx), %rax
	movl	%r13d, (%rax)
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L65
.L46:
	addq	$8, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	orl	%ecx, %r8d
	movq	%r10, %rdx
	movl	%r11d, %ecx
.L63:
	call	u_shapeArabic_67@PLT
	movl	$1, %r9d
	movl	%eax, %r8d
	movq	48(%rbx), %rax
	movl	%r8d, (%rax)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	24(%rbx), %r15
	movq	16(%rbx), %rdi
	cmpl	36(%rbx), %r13d
	jbe	.L49
	leal	50(%r13), %r14d
	testq	%rdi, %rdi
	je	.L50
	call	uprv_free_67@PLT
	movq	$0, 16(%rbx)
.L50:
	movl	%r14d, %edi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L66
	movl	%r14d, 36(%rbx)
.L49:
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	u_strncpy_67@PLT
	movl	36(%rbx), %esi
	movq	16(%rbx), %rdi
	movq	%r12, %rcx
	movl	%r13d, %edx
	call	u_terminateUChars_67@PLT
	movl	%eax, 32(%rbx)
	movl	%eax, %esi
.L52:
	movq	8(%rbx), %rax
	movl	40(%rbx), %ecx
	movq	%r12, %r9
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rdi
	movl	20(%rax), %r8d
	orl	64(%rbx), %r8d
	jmp	.L63
.L66:
	movl	$7, (%r12)
	movl	32(%rbx), %esi
	jmp	.L52
	.cfi_endproc
.LFE2103:
	.size	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode, .-_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.p2align 4
	.globl	ubiditransform_open_67
	.type	ubiditransform_open_67, @function
ubiditransform_open_67:
.LFB2094:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L74
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$72, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$1, %edi
	subq	$8, %rsp
	call	uprv_calloc_67@PLT
	testq	%rax, %rax
	je	.L75
.L67:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L75:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L67
	.cfi_endproc
.LFE2094:
	.size	ubiditransform_open_67, .-ubiditransform_open_67
	.p2align 4
	.globl	ubiditransform_close_67
	.type	ubiditransform_close_67, @function
ubiditransform_close_67:
.LFB2095:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L76
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	ubidi_close_67@PLT
.L78:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	uprv_free_67@PLT
.L79:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	ret
	.cfi_endproc
.LFE2095:
	.size	ubiditransform_close_67, .-ubiditransform_close_67
	.p2align 4
	.globl	ubiditransform_transform_67
	.type	ubiditransform_transform_67, @function
ubiditransform_transform_67:
.LFB2107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rbp), %r13
	movl	24(%rbp), %r14d
	movq	%rdi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	0(%r13), %eax
	movl	$0, -60(%rbp)
	testl	%eax, %eax
	jg	.L90
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L94
	movq	%rcx, %r10
	testq	%rcx, %rcx
	je	.L94
	movl	%edx, %ebx
	testl	%edx, %edx
	je	.L90
	cmpl	$-1, %edx
	jl	.L94
	je	.L271
.L95:
	testl	%r8d, %r8d
	je	.L90
	cmpl	$-1, %r8d
	jl	.L94
	je	.L272
.L96:
	movq	-72(%rbp), %rax
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L273
.L97:
	movzbl	%r9b, %eax
	movl	%r9d, %edx
	subl	$254, %eax
	andl	$1, %edx
	cmpl	$1, %eax
	jbe	.L274
.L100:
	movzbl	%r14b, %eax
	subl	$254, %eax
	cmpl	$1, %eax
	jbe	.L101
	andl	$1, %r14d
	testb	%dl, %dl
	je	.L275
.L102:
	cmpb	$1, %dl
	je	.L105
	testb	%dl, %dl
	jne	.L276
.L106:
	cmpb	$1, %r14b
	je	.L277
.L114:
	testb	%r14b, %r14b
	je	.L189
	cmpb	$1, %r14b
	je	.L229
.L122:
	testb	%r14b, %r14b
	je	.L248
	.p2align 4,,10
	.p2align 3
.L126:
	movq	$0, 8(%r15)
.L127:
	cmpq	-72(%rbp), %r15
	jne	.L278
	movq	$0, 24(%r15)
	movq	$0, 48(%r15)
	movl	$0, 32(%r15)
	movl	$0, 40(%r15)
.L146:
	movl	0(%r13), %edx
	movl	-60(%rbp), %eax
	testl	%edx, %edx
	jle	.L89
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%eax, %eax
.L89:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L279
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	$1, 0(%r13)
	xorl	%eax, %eax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L273:
	movl	0(%r13), %eax
	movl	%r9d, -96(%rbp)
	movl	%r8d, -88(%rbp)
	movq	%r10, -80(%rbp)
	testl	%eax, %eax
	jg	.L90
	movl	$72, %esi
	movl	$1, %edi
	call	uprv_calloc_67@PLT
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %r8d
	testq	%rax, %rax
	movl	-96(%rbp), %r9d
	movq	%rax, %r15
	je	.L280
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L97
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L101:
	movl	%edx, %r14d
	testb	%dl, %dl
	jne	.L102
.L155:
	movl	16(%rbp), %eax
	testl	%eax, %eax
	je	.L281
.L189:
	cmpl	$1, 16(%rbp)
	je	.L282
.L248:
	movl	16(%rbp), %eax
	testl	%eax, %eax
	je	.L252
	cmpl	$1, 16(%rbp)
	jne	.L126
.L264:
	cmpl	$1, 32(%rbp)
	jne	.L126
	leaq	1232+_ZL7Schemes(%rip), %rax
.L104:
	movq	%rax, 8(%r15)
	movl	40(%rbp), %eax
	testl	%eax, %eax
	movl	$2, %eax
	cmove	40(%rbp), %eax
	movl	%eax, 40(%rbp)
	movl	%eax, 56(%r15)
	movl	48(%rbp), %eax
	andl	$-29, %eax
	movl	%eax, 60(%r15)
	movl	48(%rbp), %eax
	andb	$27, %al
	movl	%eax, 64(%r15)
	cmpl	%r8d, %ebx
	jge	.L176
	movl	%r8d, %r14d
	jbe	.L128
.L270:
	movl	$15, 0(%r13)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L276:
	cmpb	$1, %dl
	jne	.L283
.L108:
	cmpb	$1, %r14b
	je	.L284
.L116:
	testb	%r14b, %r14b
	je	.L190
.L156:
	cmpb	$1, %r14b
	jne	.L126
.L249:
	movl	16(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L258
	cmpl	$1, 16(%rbp)
	jne	.L126
.L262:
	cmpl	$1, 32(%rbp)
	jne	.L126
	leaq	1320+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L283:
	testb	%dl, %dl
	jne	.L285
	cmpb	$1, %r14b
	jne	.L114
.L187:
	cmpl	$1, 16(%rbp)
	je	.L286
.L229:
	movl	16(%rbp), %edx
	testl	%edx, %edx
	je	.L256
.L230:
	cmpl	$1, 16(%rbp)
	jne	.L126
.L260:
	cmpl	$1, 32(%rbp)
	jne	.L126
	leaq	880+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L285:
	cmpb	$1, %dl
	jne	.L287
	cmpb	$1, %r14b
	jne	.L116
.L188:
	cmpl	$1, 16(%rbp)
	jne	.L249
	movl	32(%rbp), %edi
	testl	%edi, %edi
	jne	.L262
	leaq	440+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L287:
	testb	%dl, %dl
	je	.L114
	cmpb	$1, %dl
	je	.L116
	testb	%dl, %dl
	jne	.L288
	cmpb	$1, %r14b
	je	.L230
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L176:
	movl	%ebx, %r14d
.L128:
	movq	16(%r15), %rdi
	cmpl	%r14d, 36(%r15)
	jnb	.L130
	addl	$50, %r14d
	testq	%rdi, %rdi
	je	.L131
	movl	%r8d, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	uprv_free_67@PLT
	movl	-88(%rbp), %r8d
	movq	-80(%rbp), %r10
	movq	$0, 16(%r15)
.L131:
	movl	%r14d, %edi
	movl	%r8d, -88(%rbp)
	addq	%rdi, %rdi
	movq	%r10, -80(%rbp)
	call	uprv_malloc_67@PLT
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, 16(%r15)
	movq	%rax, %rdi
	je	.L289
	movl	%r14d, 36(%r15)
.L130:
	movl	%ebx, %edx
	movq	%r12, %rsi
	movl	%r8d, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	u_strncpy_67@PLT
	movl	36(%r15), %esi
	movq	16(%r15), %rdi
	movq	%r13, %rcx
	movl	%ebx, %edx
	call	u_terminateUChars_67@PLT
	movl	0(%r13), %r9d
	movl	%eax, 32(%r15)
	testl	%r9d, %r9d
	jg	.L127
	movq	(%r15), %rdi
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %r8d
	testq	%rdi, %rdi
	je	.L290
.L133:
	leaq	-60(%rbp), %rax
	movq	%r10, 24(%r15)
	movq	%rax, 48(%r15)
	movq	8(%r15), %rax
	movl	%r8d, 40(%r15)
	leaq	32(%rax), %r14
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L134
	movb	$0, -104(%rbp)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L136:
	movq	8(%r14), %rax
	addq	$8, %r14
	testq	%rax, %rax
	je	.L141
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L141
.L142:
	movl	%r8d, -88(%rbp)
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r10, -80(%rbp)
	call	*%rax
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %r8d
	testb	%al, %al
	je	.L136
	movq	48(%r15), %rax
	movq	24(%r15), %rsi
	movq	16(%r15), %rdi
	movl	(%rax), %edx
	cmpl	36(%r15), %edx
	jbe	.L138
	leal	50(%rdx), %eax
	movl	%eax, -80(%rbp)
	testq	%rdi, %rdi
	je	.L139
	movl	%r8d, -108(%rbp)
	movq	%r10, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	uprv_free_67@PLT
	movq	$0, 16(%r15)
	movl	-108(%rbp), %r8d
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %rsi
	movl	-88(%rbp), %edx
.L139:
	movl	-80(%rbp), %edi
	movl	%r8d, -108(%rbp)
	movq	%r10, -104(%rbp)
	addq	%rdi, %rdi
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	call	uprv_malloc_67@PLT
	movl	-88(%rbp), %edx
	movq	-96(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, 16(%r15)
	movq	-104(%rbp), %r10
	movq	%rax, %rdi
	movl	-108(%rbp), %r8d
	je	.L291
	movl	-80(%rbp), %eax
	movl	%eax, 36(%r15)
.L138:
	movl	%r8d, -96(%rbp)
	movq	%r10, -88(%rbp)
	movl	%edx, -80(%rbp)
	call	u_strncpy_67@PLT
	movl	36(%r15), %esi
	movq	16(%r15), %rdi
	movq	%r13, %rcx
	movl	-80(%rbp), %edx
	call	u_terminateUChars_67@PLT
	movb	$1, -104(%rbp)
	movl	-96(%rbp), %r8d
	movl	%eax, 32(%r15)
	movq	-88(%rbp), %r10
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L288:
	cmpb	$1, %dl
	je	.L156
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L271:
	movq	%rsi, %rdi
	movl	%r9d, -96(%rbp)
	movl	%r8d, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	u_strlen_67@PLT
	movl	-96(%rbp), %r9d
	movl	-88(%rbp), %r8d
	movq	-80(%rbp), %r10
	movl	%eax, %ebx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L141:
	movq	(%r15), %rdi
	xorl	%esi, %esi
	movl	%r8d, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	ubidi_setInverse_67@PLT
	cmpb	$0, -104(%rbp)
	jne	.L127
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %r8d
.L137:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L127
	cmpl	%r8d, %ebx
	jg	.L270
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	u_strncpy_67@PLT
	movl	%ebx, -60(%rbp)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L278:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L144
	call	ubidi_close_67@PLT
.L144:
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.L145
	call	uprv_free_67@PLT
.L145:
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L105:
	testb	%r14b, %r14b
	jne	.L108
	movl	16(%rbp), %eax
	testl	%eax, %eax
	je	.L292
.L190:
	cmpl	$1, 16(%rbp)
	je	.L293
	movl	16(%rbp), %eax
	testl	%eax, %eax
	je	.L254
	cmpl	$1, 16(%rbp)
	jne	.L126
.L265:
	cmpl	$1, 32(%rbp)
	jne	.L126
	leaq	968+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L274:
	movl	%ebx, %esi
	movq	%r12, %rdi
	movl	%r9d, -96(%rbp)
	movl	%r8d, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	ubidi_getBaseDirection_67@PLT
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %r8d
	cmpb	$3, %al
	movl	-96(%rbp), %r9d
	movl	%eax, %edx
	jne	.L100
	cmpb	$-1, %r9b
	sete	%dl
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%r10, %rdi
	movl	%r9d, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	u_strlen_67@PLT
	movl	-88(%rbp), %r9d
	movq	-80(%rbp), %r10
	movl	%eax, %r8d
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L275:
	testb	%r14b, %r14b
	jne	.L106
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L284:
	movl	16(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L188
	cmpl	$1, 32(%rbp)
	je	.L294
.L258:
	movl	32(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L126
	leaq	1144+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L277:
	movl	16(%rbp), %r14d
	testl	%r14d, %r14d
	jne	.L187
	cmpl	$1, 32(%rbp)
	je	.L295
.L256:
	movl	32(%rbp), %eax
	testl	%eax, %eax
	jne	.L126
	leaq	704+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L290:
	xorl	%esi, %esi
	movq	%r13, %rdx
	call	ubidi_openSized_67@PLT
	movl	0(%r13), %r8d
	movq	-80(%rbp), %r10
	movq	%rax, (%r15)
	movq	%rax, %rdi
	testl	%r8d, %r8d
	movl	-88(%rbp), %r8d
	jle	.L133
	jmp	.L127
.L286:
	movl	32(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L260
	leaq	352+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L282:
	movl	32(%rbp), %esi
	testl	%esi, %esi
	jne	.L264
	leaq	528+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L292:
	cmpl	$1, 32(%rbp)
	je	.L296
.L254:
	movl	32(%rbp), %eax
	testl	%eax, %eax
	jne	.L126
	leaq	792+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L293:
	movl	32(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L265
	leaq	616+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L281:
	cmpl	$1, 32(%rbp)
	je	.L297
.L252:
	movl	32(%rbp), %eax
	testl	%eax, %eax
	jne	.L126
	leaq	1056+_ZL7Schemes(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L134:
	xorl	%esi, %esi
	movl	%r8d, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	ubidi_setInverse_67@PLT
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %r8d
	jmp	.L137
.L279:
	call	__stack_chk_fail@PLT
.L294:
	leaq	264+_ZL7Schemes(%rip), %rax
	jmp	.L104
.L296:
	leaq	88+_ZL7Schemes(%rip), %rax
	jmp	.L104
.L295:
	leaq	176+_ZL7Schemes(%rip), %rax
	jmp	.L104
.L297:
	leaq	_ZL7Schemes(%rip), %rax
	jmp	.L104
.L289:
	movl	$7, 0(%r13)
	jmp	.L127
.L280:
	movl	$7, 0(%r13)
	jmp	.L90
.L291:
	movl	$7, 0(%r13)
	movb	$1, -104(%rbp)
	jmp	.L136
	.cfi_endproc
.LFE2107:
	.size	ubiditransform_transform_67, .-ubiditransform_transform_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL7Schemes, @object
	.size	_ZL7Schemes, 1408
_ZL7Schemes:
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	3
	.long	1
	.long	0
	.long	0
	.byte	0
	.zero	7
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	24
	.byte	1
	.zero	3
	.long	0
	.byte	0
	.zero	3
	.long	1
	.long	0
	.long	4
	.byte	1
	.zero	7
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	24
	.byte	0
	.zero	3
	.long	0
	.byte	1
	.zero	3
	.long	1
	.long	0
	.long	0
	.byte	0
	.zero	7
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reverseP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	16
	.byte	1
	.zero	3
	.long	0
	.byte	1
	.zero	3
	.long	1
	.long	0
	.long	4
	.byte	1
	.zero	7
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reverseP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	16
	.byte	0
	.zero	3
	.long	1
	.byte	1
	.zero	3
	.long	0
	.long	0
	.long	4
	.byte	1
	.zero	7
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	_ZL17action_setInverseP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	16
	.byte	1
	.zero	3
	.long	1
	.byte	1
	.zero	3
	.long	0
	.long	0
	.long	4
	.byte	1
	.zero	7
	.quad	_ZL14action_reverseP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	_ZL17action_setInverseP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	8
	.byte	0
	.zero	3
	.long	1
	.byte	0
	.zero	3
	.long	0
	.long	0
	.long	0
	.byte	0
	.zero	7
	.quad	_ZL17action_setInverseP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	16
	.byte	1
	.zero	3
	.long	1
	.byte	0
	.zero	3
	.long	0
	.long	0
	.long	0
	.byte	0
	.zero	7
	.quad	_ZL14action_reverseP14UBiDiTransformP10UErrorCode
	.quad	_ZL17action_setInverseP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	1
	.zero	3
	.long	0
	.long	0
	.long	0
	.byte	0
	.zero	7
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL13action_mirrorP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_setRunsOnlyP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.quad	0
	.byte	1
	.zero	3
	.long	0
	.byte	0
	.zero	3
	.long	0
	.long	0
	.long	0
	.byte	1
	.zero	7
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL13action_mirrorP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_setRunsOnlyP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reorderP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	0
	.byte	0
	.zero	3
	.long	1
	.byte	1
	.zero	3
	.long	1
	.long	0
	.long	4
	.byte	0
	.zero	7
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	_ZL17action_setInverseP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL13action_mirrorP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reverseP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	8
	.byte	1
	.zero	3
	.long	1
	.byte	0
	.zero	3
	.long	1
	.long	0
	.long	4
	.byte	0
	.zero	7
	.quad	_ZL14action_reverseP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	_ZL17action_setInverseP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL13action_mirrorP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	3
	.long	0
	.long	0
	.long	0
	.byte	0
	.zero	7
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL13action_mirrorP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	24
	.byte	1
	.zero	3
	.long	0
	.byte	1
	.zero	3
	.long	0
	.long	4
	.long	0
	.byte	1
	.zero	7
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL13action_mirrorP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	24
	.byte	0
	.zero	3
	.long	1
	.byte	0
	.zero	3
	.long	1
	.long	0
	.long	4
	.byte	0
	.zero	7
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL13action_mirrorP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	24
	.byte	1
	.zero	3
	.long	1
	.byte	1
	.zero	3
	.long	1
	.long	0
	.long	4
	.byte	0
	.zero	7
	.quad	_ZL14action_reverseP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_resolveP14UBiDiTransformP10UErrorCode
	.quad	_ZL13action_mirrorP14UBiDiTransformP10UErrorCode
	.quad	_ZL18action_shapeArabicP14UBiDiTransformP10UErrorCode
	.quad	_ZL14action_reverseP14UBiDiTransformP10UErrorCode
	.quad	0
	.zero	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
