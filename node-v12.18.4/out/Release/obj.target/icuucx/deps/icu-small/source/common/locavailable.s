	.file	"locavailable.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumeration5resetER10UErrorCode, @function
_ZN12_GLOBAL__N_133AvailableLocalesStringEnumeration5resetER10UErrorCode:
.LFB3110:
	.cfi_startproc
	endbr64
	movl	$0, 120(%rdi)
	ret
	.cfi_endproc
.LFE3110:
	.size	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumeration5resetER10UErrorCode, .-_ZN12_GLOBAL__N_133AvailableLocalesStringEnumeration5resetER10UErrorCode
	.p2align 4
	.type	locale_available_cleanup, @function
locale_available_cleanup:
.LFB3102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	_ZN6icu_67L19availableLocaleListE(%rip), %rax
	testq	%rax, %rax
	je	.L4
	movq	-8(%rax), %rdx
	leaq	0(,%rdx,8), %rbx
	subq	%rdx, %rbx
	salq	$5, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	movq	-224(%rbx), %rax
	subq	$224, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, _ZN6icu_67L19availableLocaleListE(%rip)
	jne	.L6
.L5:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	movq	$0, _ZN6icu_67L19availableLocaleListE(%rip)
.L4:
	movl	$0, _ZN6icu_67L24availableLocaleListCountE(%rip)
	movl	$1, %eax
	movl	$0, _ZN6icu_67L15gInitOnceLocaleE(%rip)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3102:
	.size	locale_available_cleanup, .-locale_available_cleanup
	.p2align 4
	.type	_ZN12_GLOBAL__N_1L12uloc_cleanupEv, @function
_ZN12_GLOBAL__N_1L12uloc_cleanupEv:
.LFB3112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN12_GLOBAL__N_121gAvailableLocaleNamesE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uprv_free_67@PLT
	movq	8+_ZN12_GLOBAL__N_121gAvailableLocaleNamesE(%rip), %rdi
	movq	$0, _ZN12_GLOBAL__N_121gAvailableLocaleNamesE(%rip)
	movl	$0, _ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip)
	call	uprv_free_67@PLT
	movl	$1, %eax
	movq	$0, 8+_ZN12_GLOBAL__N_121gAvailableLocaleNamesE(%rip)
	movl	$0, 4+_ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip)
	movl	$0, _ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3112:
	.size	_ZN12_GLOBAL__N_1L12uloc_cleanupEv, .-_ZN12_GLOBAL__N_1L12uloc_cleanupEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"InstalledLocales"
.LC1:
	.string	"AliasLocales"
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_120AvailableLocalesSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode, @function
_ZN12_GLOBAL__N_120AvailableLocalesSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode:
.LFB3105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-144(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movq	%rsi, -152(%rbp)
	movq	%r12, %rsi
	movq	%r8, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rdi, -168(%rbp)
	movq	%r8, %rdx
	call	*88(%rax)
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L14
	movl	$0, -156(%rbp)
	leaq	-152(%rbp), %r14
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L20:
	addl	$1, -156(%rbp)
.L17:
	movl	-156(%rbp), %esi
	movq	-168(%rbp), %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L14
	movq	-152(%rbp), %rax
	movl	$17, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%bl
	sbbb	$0, %bl
	movsbq	%bl, %rbx
	testl	%ebx, %ebx
	je	.L16
	movq	%rax, %rsi
	movl	$13, %ecx
	leaq	.LC1(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L20
	movl	$1, %ebx
.L16:
	movq	-176(%rbp), %r15
	movq	(%r12), %rax
	leaq	-96(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r15, %rdx
	call	*88(%rax)
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L14
	movslq	-64(%rbp), %rdi
	leaq	_ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip), %rax
	movl	%edi, (%rax,%rbx,4)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	leaq	_ZN12_GLOBAL__N_121gAvailableLocaleNamesE(%rip), %rcx
	movq	%rax, (%rcx,%rbx,8)
	testq	%rax, %rax
	je	.L35
	xorl	%r15d, %r15d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	_ZN12_GLOBAL__N_121gAvailableLocaleNamesE(%rip), %rax
	movq	-152(%rbp), %rdx
	movq	(%rax,%rbx,8), %rax
	movq	%rdx, (%rax,%r15,8)
	addq	$1, %r15
.L19:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713ResourceTable14getKeyAndValueEiRPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	jne	.L36
	jmp	.L20
.L35:
	movq	-176(%rbp), %rax
	movl	$7, (%rax)
	.p2align 4,,10
	.p2align 3
.L14:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L37:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3105:
	.size	_ZN12_GLOBAL__N_120AvailableLocalesSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode, .-_ZN12_GLOBAL__N_120AvailableLocalesSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_120AvailableLocalesSinkD2Ev, @function
_ZN12_GLOBAL__N_120AvailableLocalesSinkD2Ev:
.LFB3984:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6712ResourceSinkD2Ev@PLT
	.cfi_endproc
.LFE3984:
	.size	_ZN12_GLOBAL__N_120AvailableLocalesSinkD2Ev, .-_ZN12_GLOBAL__N_120AvailableLocalesSinkD2Ev
	.set	_ZN12_GLOBAL__N_120AvailableLocalesSinkD1Ev,_ZN12_GLOBAL__N_120AvailableLocalesSinkD2Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_120AvailableLocalesSinkD0Ev, @function
_ZN12_GLOBAL__N_120AvailableLocalesSinkD0Ev:
.LFB3986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3986:
	.size	_ZN12_GLOBAL__N_120AvailableLocalesSinkD0Ev, .-_ZN12_GLOBAL__N_120AvailableLocalesSinkD0Ev
	.section	.rodata.str1.1
.LC2:
	.string	"res_index"
.LC3:
	.string	""
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_1L20loadInstalledLocalesER10UErrorCode, @function
_ZN12_GLOBAL__N_1L20loadInstalledLocalesER10UErrorCode:
.LFB3113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12_GLOBAL__N_1L12uloc_cleanupEv(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$10, %edi
	leaq	-48(%rbp), %r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	16+_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ucln_common_registerCleanup_67@PLT
	xorl	%edi, %edi
	movq	%r13, %rdx
	leaq	.LC2(%rip), %rsi
	call	ures_openDirect_67@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r12
	call	ures_getAllItemsWithFallback_67@PLT
	movq	%r14, %rdi
	movq	%rbx, -48(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	testq	%r12, %r12
	je	.L41
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L41:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L48:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3113:
	.size	_ZN12_GLOBAL__N_1L20loadInstalledLocalesER10UErrorCode, .-_ZN12_GLOBAL__N_1L20loadInstalledLocalesER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumeration4nextEPiR10UErrorCode, @function
_ZN12_GLOBAL__N_133AvailableLocalesStringEnumeration4nextEPiR10UErrorCode:
.LFB3109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	120(%rdi), %edx
	movq	%rsi, %rbx
	movslq	116(%rdi), %rax
	leal	1(%rdx), %ecx
	movl	%ecx, 120(%rdi)
	cmpl	$2, %eax
	je	.L62
.L51:
	leaq	_ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip), %rcx
	cmpl	(%rcx,%rax,4), %edx
	jl	.L52
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	je	.L49
	movl	$0, (%rbx)
.L49:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	_ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip), %eax
	cmpl	%eax, %edx
	jge	.L63
	xorl	%eax, %eax
.L52:
	leaq	_ZN12_GLOBAL__N_121gAvailableLocaleNamesE(%rip), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rax,8), %rax
	movq	(%rax,%rdx,8), %r12
	testq	%rbx, %rbx
	je	.L49
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, (%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	subl	%eax, %edx
	movl	$1, %eax
	jmp	.L51
	.cfi_endproc
.LFE3109:
	.size	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumeration4nextEPiR10UErrorCode, .-_ZN12_GLOBAL__N_133AvailableLocalesStringEnumeration4nextEPiR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD2Ev, @function
_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD2Ev:
.LFB3502:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_133AvailableLocalesStringEnumerationE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6717StringEnumerationD2Ev@PLT
	.cfi_endproc
.LFE3502:
	.size	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD2Ev, .-_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD2Ev
	.set	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD1Ev,_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD2Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD0Ev, @function
_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD0Ev:
.LFB3504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_133AvailableLocalesStringEnumerationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3504:
	.size	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD0Ev, .-_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD0Ev
	.align 2
	.p2align 4
	.type	_ZNK12_GLOBAL__N_133AvailableLocalesStringEnumeration5countER10UErrorCode, @function
_ZNK12_GLOBAL__N_133AvailableLocalesStringEnumeration5countER10UErrorCode:
.LFB3111:
	.cfi_startproc
	endbr64
	movslq	116(%rdi), %rax
	cmpl	$2, %eax
	je	.L70
	leaq	_ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movl	4+_ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip), %eax
	addl	_ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip), %eax
	ret
	.cfi_endproc
.LFE3111:
	.size	_ZNK12_GLOBAL__N_133AvailableLocalesStringEnumeration5countER10UErrorCode, .-_ZNK12_GLOBAL__N_133AvailableLocalesStringEnumeration5countER10UErrorCode
	.p2align 4
	.globl	_ZN6icu_6721locale_available_initEv
	.type	_ZN6icu_6721locale_available_initEv, @function
_ZN6icu_6721locale_available_initEv:
.LFB3103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	movl	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L127
.L72:
	movl	4+_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L74
	movl	%eax, -72(%rbp)
.L75:
	leaq	-80(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	movl	$0, _ZN6icu_67L24availableLocaleListCountE(%rip)
.L77:
	cmpq	$0, _ZN6icu_67L19availableLocaleListE(%rip)
	jne	.L86
.L80:
	movl	$0, _ZN6icu_67L24availableLocaleListCountE(%rip)
.L86:
	leaq	locale_available_cleanup(%rip), %rsi
	movl	$7, %edi
	call	ucln_common_registerCleanup_67@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L128
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	leaq	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L72
	leaq	_ZN12_GLOBAL__N_1L12uloc_cleanupEv(%rip), %rsi
	movl	$10, %edi
	leaq	-72(%rbp), %r15
	call	ucln_common_registerCleanup_67@PLT
	xorl	%edi, %edi
	movq	%r15, %rdx
	leaq	-88(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	leaq	16+_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE(%rip), %rbx
	call	ures_openDirect_67@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%rbx, -88(%rbp)
	movq	%rax, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r12
	call	ures_getAllItemsWithFallback_67@PLT
	movq	%r13, %rdi
	movq	%rbx, -88(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	testq	%r12, %r12
	je	.L73
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L73:
	movl	-72(%rbp), %eax
	leaq	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L74:
	movl	-72(%rbp), %edx
	testl	%edx, %edx
	jg	.L75
	movslq	_ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip), %rbx
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	movl	%ebx, _ZN6icu_67L24availableLocaleListCountE(%rip)
	testl	%ebx, %ebx
	je	.L77
	movabsq	$41175768021673106, %rax
	cmpq	%rax, %rbx
	ja	.L78
	leaq	0(,%rbx,8), %rdi
	subq	%rbx, %rdi
	salq	$5, %rdi
	addq	$8, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L81
	movq	%rbx, (%rax)
	addq	$8, %r12
	subq	$1, %rbx
.L92:
	movq	%r12, %r13
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%r13, %rdi
	subq	$1, %rbx
	addq	$224, %r13
	call	_ZN6icu_676LocaleC1Ev@PLT
	cmpq	$-1, %rbx
	jne	.L83
.L82:
	movl	_ZN6icu_67L24availableLocaleListCountE(%rip), %r14d
	movq	%r12, _ZN6icu_67L19availableLocaleListE(%rip)
	subl	$1, %r14d
	js	.L86
	movslq	%r14d, %rax
	leaq	0(,%rax,8), %r13
	movq	%r13, %rbx
	subq	%rax, %rbx
	salq	$5, %rbx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L87
	leaq	_ZN12_GLOBAL__N_1L12uloc_cleanupEv(%rip), %rsi
	movl	$10, %edi
	call	ucln_common_registerCleanup_67@PLT
	leaq	-72(%rbp), %rcx
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	movq	%rcx, %rdx
	movq	%rcx, -104(%rbp)
	call	ures_openDirect_67@PLT
	leaq	-88(%rbp), %r10
	movq	-104(%rbp), %rcx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r9
	movq	%r10, %rdx
	leaq	16+_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE(%rip), %rax
	movq	%r10, -112(%rbp)
	movq	%r9, %rdi
	movq	%r9, -104(%rbp)
	movq	%rax, -88(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movq	-112(%rbp), %r10
	leaq	16+_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE(%rip), %rax
	movq	%rax, -88(%rbp)
	movq	%r10, %rdi
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	movq	-104(%rbp), %r9
	testq	%r9, %r9
	je	.L88
	movq	%r9, %rdi
	call	ures_close_67@PLT
.L88:
	movl	-72(%rbp), %eax
	leaq	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L89:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jg	.L95
	cmpl	_ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip), %r14d
	jg	.L95
	movq	_ZN12_GLOBAL__N_121gAvailableLocaleNamesE(%rip), %rax
	movq	(%rax,%r13), %rsi
.L90:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	subl	$1, %r14d
	subq	$8, %r13
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	subq	$224, %rbx
	call	_ZN6icu_676Locale14setFromPOSIXIDEPKc@PLT
	cmpl	$-1, %r14d
	je	.L86
	movq	_ZN6icu_67L19availableLocaleListE(%rip), %r12
.L91:
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$0, -72(%rbp)
	addq	%rbx, %r12
	movq	%rax, -80(%rbp)
	movl	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L129
.L87:
	movl	4+_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L89
	movl	%eax, -72(%rbp)
	xorl	%esi, %esi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L78:
	movq	$-1, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L81
	movq	%rbx, (%rax)
	addq	$8, %r12
	subq	$1, %rbx
	jns	.L92
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%esi, %esi
	jmp	.L90
.L128:
	call	__stack_chk_fail@PLT
.L81:
	movq	$0, _ZN6icu_67L19availableLocaleListE(%rip)
	jmp	.L80
	.cfi_endproc
.LFE3103:
	.size	_ZN6icu_6721locale_available_initEv, .-_ZN6icu_6721locale_available_initEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676Locale19getAvailableLocalesERi
	.type	_ZN6icu_676Locale19getAvailableLocalesERi, @function
_ZN6icu_676Locale19getAvailableLocalesERi:
.LFB3104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L15gInitOnceLocaleE(%rip), %eax
	cmpl	$2, %eax
	je	.L132
	leaq	_ZN6icu_67L15gInitOnceLocaleE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L132
	call	_ZN6icu_6721locale_available_initEv
	leaq	_ZN6icu_67L15gInitOnceLocaleE(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L132:
	movl	_ZN6icu_67L24availableLocaleListCountE(%rip), %eax
	movl	%eax, (%rbx)
	movq	_ZN6icu_67L19availableLocaleListE(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3104:
	.size	_ZN6icu_676Locale19getAvailableLocalesERi, .-_ZN6icu_676Locale19getAvailableLocalesERi
	.p2align 4
	.globl	uloc_getAvailable_67
	.type	uloc_getAvailable_67, @function
uloc_getAvailable_67:
.LFB3118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%edi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	movl	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L156
.L139:
	movl	4+_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L141
	movl	%eax, -72(%rbp)
	xorl	%r12d, %r12d
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L139
	leaq	_ZN12_GLOBAL__N_1L12uloc_cleanupEv(%rip), %rsi
	movl	$10, %edi
	leaq	-72(%rbp), %r15
	call	ucln_common_registerCleanup_67@PLT
	xorl	%edi, %edi
	movq	%r15, %rdx
	leaq	-88(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	leaq	16+_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE(%rip), %r14
	call	ures_openDirect_67@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, -88(%rbp)
	movq	%rax, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r12
	call	ures_getAllItemsWithFallback_67@PLT
	movq	%r13, %rdi
	movq	%r14, -88(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	testq	%r12, %r12
	je	.L140
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L140:
	movl	-72(%rbp), %eax
	leaq	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L141:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jg	.L145
	cmpl	%ebx, _ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip)
	jl	.L145
	movq	_ZN12_GLOBAL__N_121gAvailableLocaleNamesE(%rip), %rax
	movq	(%rax,%rbx,8), %r12
.L142:
	leaq	-80(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L142
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3118:
	.size	uloc_getAvailable_67, .-uloc_getAvailable_67
	.p2align 4
	.globl	uloc_countAvailable_67
	.type	uloc_countAvailable_67, @function
uloc_countAvailable_67:
.LFB3119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_679ErrorCodeE(%rip), %rax
	movl	$0, -56(%rbp)
	movq	%rax, -64(%rbp)
	movl	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L175
.L159:
	movl	4+_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L161
	movl	%eax, -56(%rbp)
	xorl	%r12d, %r12d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L159
	leaq	_ZN12_GLOBAL__N_1L12uloc_cleanupEv(%rip), %rsi
	movl	$10, %edi
	leaq	-56(%rbp), %r14
	call	ucln_common_registerCleanup_67@PLT
	xorl	%edi, %edi
	movq	%r14, %rdx
	leaq	-72(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	leaq	16+_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE(%rip), %rbx
	call	ures_openDirect_67@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, -72(%rbp)
	movq	%rax, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r12
	call	ures_getAllItemsWithFallback_67@PLT
	movq	%r13, %rdi
	movq	%rbx, -72(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	testq	%r12, %r12
	je	.L160
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L160:
	movl	-56(%rbp), %eax
	leaq	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L161:
	movl	-56(%rbp), %eax
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jg	.L162
	movl	_ZN12_GLOBAL__N_122gAvailableLocaleCountsE(%rip), %r12d
.L162:
	leaq	-64(%rbp), %rdi
	call	_ZN6icu_679ErrorCodeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	addq	$48, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L176:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3119:
	.size	uloc_countAvailable_67, .-uloc_countAvailable_67
	.p2align 4
	.globl	uloc_openAvailableByType_67
	.type	uloc_openAvailableByType_67, @function
uloc_openAvailableByType_67:
.LFB3120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L202
	movl	%edi, %ebx
	movq	%rsi, %r12
	cmpl	$2, %edi
	ja	.L203
	movl	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L204
.L181:
	movl	4+_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L183
	movl	%eax, (%r12)
	xorl	%r13d, %r13d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L204:
	leaq	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L181
	leaq	_ZN12_GLOBAL__N_1L12uloc_cleanupEv(%rip), %rsi
	movl	$10, %edi
	leaq	-64(%rbp), %r14
	call	ucln_common_registerCleanup_67@PLT
	xorl	%edi, %edi
	movq	%r12, %rdx
	leaq	.LC2(%rip), %rsi
	call	ures_openDirect_67@PLT
	leaq	16+_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE(%rip), %r15
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%rax, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r13
	movq	%r15, -64(%rbp)
	call	ures_getAllItemsWithFallback_67@PLT
	movq	%r14, %rdi
	movq	%r15, -64(%rbp)
	call	_ZN6icu_6712ResourceSinkD2Ev@PLT
	testq	%r13, %r13
	je	.L182
	movq	%r13, %rdi
	call	ures_close_67@PLT
.L182:
	movl	(%r12), %eax
	leaq	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L183:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L202
	movl	$128, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L185
	movq	%rax, %rdi
	call	_ZN6icu_6717StringEnumerationC2Ev@PLT
	movl	(%r12), %edx
	leaq	16+_ZTVN12_GLOBAL__N_133AvailableLocalesStringEnumerationE(%rip), %rax
	movl	%ebx, 116(%r13)
	movq	%rax, 0(%r13)
	movl	$0, 120(%r13)
	testl	%edx, %edx
	jle	.L205
	movq	%r13, %rdi
	call	_ZN6icu_6717StringEnumerationD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L202:
	xorl	%r13d, %r13d
.L177:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movl	$1, (%rsi)
	xorl	%r13d, %r13d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	uenum_openFromStringEnumeration_67@PLT
	movq	%rax, %r13
	jmp	.L177
.L185:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L177
	movl	$7, (%r12)
	jmp	.L177
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3120:
	.size	uloc_openAvailableByType_67, .-uloc_openAvailableByType_67
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTIN12_GLOBAL__N_120AvailableLocalesSinkE, @object
	.size	_ZTIN12_GLOBAL__N_120AvailableLocalesSinkE, 24
_ZTIN12_GLOBAL__N_120AvailableLocalesSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN12_GLOBAL__N_120AvailableLocalesSinkE
	.quad	_ZTIN6icu_6712ResourceSinkE
	.section	.rodata
	.align 32
	.type	_ZTSN12_GLOBAL__N_120AvailableLocalesSinkE, @object
	.size	_ZTSN12_GLOBAL__N_120AvailableLocalesSinkE, 40
_ZTSN12_GLOBAL__N_120AvailableLocalesSinkE:
	.string	"*N12_GLOBAL__N_120AvailableLocalesSinkE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTIN12_GLOBAL__N_133AvailableLocalesStringEnumerationE, @object
	.size	_ZTIN12_GLOBAL__N_133AvailableLocalesStringEnumerationE, 24
_ZTIN12_GLOBAL__N_133AvailableLocalesStringEnumerationE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN12_GLOBAL__N_133AvailableLocalesStringEnumerationE
	.quad	_ZTIN6icu_6717StringEnumerationE
	.section	.rodata
	.align 32
	.type	_ZTSN12_GLOBAL__N_133AvailableLocalesStringEnumerationE, @object
	.size	_ZTSN12_GLOBAL__N_133AvailableLocalesStringEnumerationE, 53
_ZTSN12_GLOBAL__N_133AvailableLocalesStringEnumerationE:
	.string	"*N12_GLOBAL__N_133AvailableLocalesStringEnumerationE"
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE, @object
	.size	_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE, 48
_ZTVN12_GLOBAL__N_120AvailableLocalesSinkE:
	.quad	0
	.quad	_ZTIN12_GLOBAL__N_120AvailableLocalesSinkE
	.quad	_ZN12_GLOBAL__N_120AvailableLocalesSinkD1Ev
	.quad	_ZN12_GLOBAL__N_120AvailableLocalesSinkD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN12_GLOBAL__N_120AvailableLocalesSink3putEPKcRN6icu_6713ResourceValueEaR10UErrorCode
	.align 8
	.type	_ZTVN12_GLOBAL__N_133AvailableLocalesStringEnumerationE, @object
	.size	_ZTVN12_GLOBAL__N_133AvailableLocalesStringEnumerationE, 104
_ZTVN12_GLOBAL__N_133AvailableLocalesStringEnumerationE:
	.quad	0
	.quad	_ZTIN12_GLOBAL__N_133AvailableLocalesStringEnumerationE
	.quad	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD1Ev
	.quad	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumerationD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringEnumeration5cloneEv
	.quad	_ZNK12_GLOBAL__N_133AvailableLocalesStringEnumeration5countER10UErrorCode
	.quad	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumeration4nextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5unextEPiR10UErrorCode
	.quad	_ZN6icu_6717StringEnumeration5snextER10UErrorCode
	.quad	_ZN12_GLOBAL__N_133AvailableLocalesStringEnumeration5resetER10UErrorCode
	.quad	_ZNK6icu_6717StringEnumerationeqERKS0_
	.quad	_ZNK6icu_6717StringEnumerationneERKS0_
	.local	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE
	.comm	_ZN12_GLOBAL__N_125ginstalledLocalesInitOnceE,8,8
	.local	_ZN12_GLOBAL__N_122gAvailableLocaleCountsE
	.comm	_ZN12_GLOBAL__N_122gAvailableLocaleCountsE,8,8
	.local	_ZN12_GLOBAL__N_121gAvailableLocaleNamesE
	.comm	_ZN12_GLOBAL__N_121gAvailableLocaleNamesE,16,16
	.local	_ZN6icu_67L15gInitOnceLocaleE
	.comm	_ZN6icu_67L15gInitOnceLocaleE,8,8
	.local	_ZN6icu_67L24availableLocaleListCountE
	.comm	_ZN6icu_67L24availableLocaleListCountE,4,4
	.local	_ZN6icu_67L19availableLocaleListE
	.comm	_ZN6icu_67L19availableLocaleListE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
