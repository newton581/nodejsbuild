	.file	"uenum.cpp"
	.text
	.p2align 4
	.globl	uenum_close_67
	.type	uenum_close_67, @function
uenum_close_67:
.LFB2074:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L3
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	call	uprv_free_67@PLT
	movq	16(%r12), %rax
.L4:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	addq	$8, %rsp
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2074:
	.size	uenum_close_67, .-uenum_close_67
	.p2align 4
	.globl	uenum_count_67
	.type	uenum_count_67, @function
uenum_count_67:
.LFB2075:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L12
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L12
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L14
	jmp	*%rax
.L14:
	movl	$16, (%rsi)
.L12:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2075:
	.size	uenum_count_67, .-uenum_count_67
	.p2align 4
	.globl	uenum_unextDefault_67
	.type	uenum_unextDefault_67, @function
uenum_unextDefault_67:
.LFB2076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movl	$0, -60(%rbp)
	testq	%rax, %rax
	je	.L19
	movq	%rdi, %r13
	leaq	-60(%rbp), %rsi
	xorl	%r12d, %r12d
	call	*%rax
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L20
	movl	-60(%rbp), %eax
	movq	0(%r13), %rdi
	leal	1(%rax), %edx
	leal	(%rdx,%rdx), %eax
	testq	%rdi, %rdi
	je	.L21
	cmpl	(%rdi), %eax
	jg	.L39
	leaq	4(%rdi), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	u_charsToUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	testq	%rbx, %rbx
	je	.L18
	movl	-60(%rbp), %eax
	movl	%eax, (%rbx)
.L18:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	leal	8(%rax), %r12d
	movslq	%r12d, %rsi
	addq	$4, %rsi
	call	uprv_realloc_67@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L23
.L38:
	movl	%r12d, (%rdi)
	movl	-60(%rbp), %eax
	leaq	4(%rdi), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	leal	1(%rax), %edx
	call	u_charsToUChars_67@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	leal	8(%rax), %r12d
	movslq	%r12d, %rdi
	addq	$4, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L38
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$7, (%r14)
	xorl	%r12d, %r12d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$16, (%rdx)
	xorl	%r12d, %r12d
	jmp	.L20
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2076:
	.size	uenum_unextDefault_67, .-uenum_unextDefault_67
	.p2align 4
	.globl	uenum_nextDefault_67
	.type	uenum_nextDefault_67, @function
uenum_nextDefault_67:
.LFB2077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L42
	movq	%rdi, %r12
	movq	%rsi, %rbx
	call	*%rax
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L47
	movq	(%r12), %rdi
	movl	(%rbx), %eax
	testq	%rdi, %rdi
	je	.L44
	cmpl	(%rdi), %eax
	jge	.L56
.L45:
	leaq	4(%rdi), %r12
	leal	1(%rax), %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	u_UCharsToChars_67@PLT
.L41:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	leal	9(%rax), %r13d
	movslq	%r13d, %rsi
	addq	$4, %rsi
	call	uprv_realloc_67@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L46
.L55:
	movl	%r13d, (%rdi)
	movl	(%rbx), %eax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L44:
	leal	9(%rax), %r13d
	movslq	%r13d, %rdi
	addq	$4, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L55
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$7, (%r15)
	xorl	%r12d, %r12d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L47:
	xorl	%r12d, %r12d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$16, (%rdx)
	xorl	%r12d, %r12d
	jmp	.L41
	.cfi_endproc
.LFE2077:
	.size	uenum_nextDefault_67, .-uenum_nextDefault_67
	.p2align 4
	.globl	uenum_unext_67
	.type	uenum_unext_67, @function
uenum_unext_67:
.LFB2078:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L57
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L57
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L59
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$16, (%rdx)
.L57:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2078:
	.size	uenum_unext_67, .-uenum_unext_67
	.p2align 4
	.globl	uenum_next_67
	.type	uenum_next_67, @function
uenum_next_67:
.LFB2079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L68
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L63
	movq	40(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L65
	testq	%rsi, %rsi
	je	.L66
.L71:
	call	*%rcx
.L63:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L72
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$0, -12(%rbp)
	leaq	-12(%rbp), %rsi
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%eax, %eax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$16, (%rdx)
	jmp	.L63
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2079:
	.size	uenum_next_67, .-uenum_next_67
	.p2align 4
	.globl	uenum_reset_67
	.type	uenum_reset_67, @function
uenum_reset_67:
.LFB2080:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L73
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L73
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.L75
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$16, (%rsi)
.L73:
	ret
	.cfi_endproc
.LFE2080:
	.size	uenum_reset_67, .-uenum_reset_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
