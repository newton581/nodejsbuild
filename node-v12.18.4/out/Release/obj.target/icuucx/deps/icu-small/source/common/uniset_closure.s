	.file	"uniset_closure.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_67L14_set_addStringEP4USetPKDsi, @function
_ZN6icu_67L14_set_addStringEP4USetPKDsi:
.LFB3142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	movl	%ecx, %esi
	shrl	$31, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3142:
	.size	_ZN6icu_67L14_set_addStringEP4USetPKDsi, .-_ZN6icu_67L14_set_addStringEP4USetPKDsi
	.p2align 4
	.type	_ZN6icu_67L13_set_addRangeEP4USetii, @function
_ZN6icu_67L13_set_addRangeEP4USetii:
.LFB3141:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet3addEii@PLT
	.cfi_endproc
.LFE3141:
	.size	_ZN6icu_67L13_set_addRangeEP4USetii, .-_ZN6icu_67L13_set_addRangeEP4USetii
	.p2align 4
	.type	_ZN6icu_67L8_set_addEP4USeti, @function
_ZN6icu_67L8_set_addEP4USeti:
.LFB3140:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet3addEi@PLT
	.cfi_endproc
.LFE3140:
	.size	_ZN6icu_67L8_set_addEP4USeti, .-_ZN6icu_67L8_set_addEP4USeti
	.align 2
	.p2align 4
	.type	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode.part.0, @function
_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode.part.0:
.LFB4070:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r15
	movl	%ecx, %r14d
	movq	%rdx, %rcx
	pushq	%r13
	movq	%r8, %rdx
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6721RuleCharacterIteratorC1ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE@PLT
	subq	$8, %rsp
	xorl	%edx, %edx
	leaq	-128(%rbp), %r10
	leaq	_ZN6icu_6710UnicodeSet9closeOverEi(%rip), %rax
	pushq	%rbx
	xorl	%r9d, %r9d
	movq	%r10, %rcx
	pushq	%rdx
	movl	%r14d, %r8d
	movq	%r12, %rdx
	movq	%r15, %rsi
	pushq	%rax
	movq	%r13, %rdi
	movq	%r10, -184(%rbp)
	call	_ZN6icu_6710UnicodeSet12applyPatternERNS_21RuleCharacterIteratorEPKNS_11SymbolTableERNS_13UnicodeStringEjMS0_FRS0_iEiR10UErrorCode@PLT
	movl	(%rbx), %edx
	addq	$32, %rsp
	movq	-184(%rbp), %r10
	testl	%edx, %edx
	jg	.L9
	movq	-152(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L10
	movl	$65538, (%rbx)
.L9:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L11
	movswl	%ax, %edx
	sarl	$5, %edx
.L12:
	testb	$17, %al
	jne	.L13
	leaq	-118(%rbp), %rsi
	testb	$2, %al
	cmove	-104(%rbp), %rsi
.L13:
	movq	%r13, %rdi
	movq	%r10, -184(%rbp)
	call	_ZN6icu_6710UnicodeSet10setPatternEPKDsi@PLT
	movq	-184(%rbp), %r10
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L11:
	movl	-116(%rbp), %edx
	jmp	.L12
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4070:
	.size	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode.part.0, .-_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode
	.type	_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode, @function
_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode:
.LFB3133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, 32(%rdi)
	leaq	-280(%rax), %rsi
	movq	%rax, %xmm1
	leaq	96(%rdi), %rax
	movl	$0, 56(%rdi)
	movq	%rsi, %xmm0
	movq	%rax, 16(%rdi)
	movabsq	$4294967321, %rax
	movl	(%r8), %esi
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%rdi)
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, -64(%rbp)
	movabsq	$-4294967296, %rax
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 64(%rdi)
	movl	$0, 72(%rdi)
	movq	%rax, -56(%rbp)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 80(%rdi)
	testl	%esi, %esi
	jg	.L20
	leaq	-64(%rbp), %r14
	movq	%r8, %rbx
	movq	%r8, %r9
	movl	%edx, %r13d
	movq	%rcx, %r8
	movq	%r12, %rsi
	movl	%edx, %ecx
	movq	%r14, %rdx
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode.part.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L20
	movl	-56(%rbp), %edx
	andl	$1, %r13d
	movl	%edx, -68(%rbp)
	jne	.L30
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L23
.L32:
	sarl	$5, %eax
.L24:
	cmpl	%edx, %eax
	je	.L20
	movl	$1, (%rbx)
.L20:
	movq	%r14, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movl	12(%r12), %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$1, %edx
	leaq	-68(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia@PLT
	movswl	8(%r12), %eax
	movl	-68(%rbp), %edx
	testw	%ax, %ax
	jns	.L32
	jmp	.L23
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3133:
	.size	_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode, .-_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode
	.globl	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode
	.set	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode,_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode
	.type	_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode, @function
_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode:
.LFB3136:
	.cfi_startproc
	endbr64
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, 32(%rdi)
	leaq	-280(%rax), %r10
	movq	%rax, %xmm1
	leaq	96(%rdi), %rax
	movl	$0, 56(%rdi)
	movq	%rax, 16(%rdi)
	movq	%r10, %xmm0
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%rdi)
	movl	(%r9), %eax
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 64(%rdi)
	movl	$0, 72(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 80(%rdi)
	testl	%eax, %eax
	jg	.L33
	jmp	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L33:
	ret
	.cfi_endproc
.LFE3136:
	.size	_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode, .-_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode
	.globl	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode
	.set	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode,_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode
	.type	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode, @function
_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode:
.LFB3138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, -80(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -72(%rbp)
	testl	%edi, %edi
	jg	.L42
	cmpq	$0, 40(%r12)
	movq	%r8, %rbx
	je	.L50
.L37:
	movl	$30, (%rbx)
	leaq	-80(%rbp), %r15
.L42:
	movq	%r15, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	cmpq	$0, 88(%r12)
	jne	.L37
	movq	%r8, %r9
	movl	%edx, %r14d
	movq	%rcx, %r8
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%r15, %rdx
	movq	%rsi, %r13
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode.part.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L42
	movl	-72(%rbp), %edx
	andl	$1, %r14d
	movl	%edx, -84(%rbp)
	jne	.L52
.L39:
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L40
	sarl	$5, %eax
.L41:
	cmpl	%edx, %eax
	je	.L42
	movl	$1, (%rbx)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L40:
	movl	12(%r13), %eax
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$1, %edx
	leaq	-84(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia@PLT
	movl	-84(%rbp), %edx
	jmp	.L39
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3138:
	.size	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode, .-_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode
	.type	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode, @function
_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode:
.LFB3139:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movl	(%r9), %edi
	testl	%edi, %edi
	jg	.L57
	cmpq	$0, 40(%rax)
	je	.L60
.L55:
	movl	$30, (%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	cmpq	$0, 88(%rax)
	jne	.L55
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rax, -8(%rbp)
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode.part.0
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3139:
	.size	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode, .-_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6710UnicodeSet9closeOverEi.part.0, @function
_ZN6icu_6710UnicodeSet9closeOverEi.part.0:
.LFB4071:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-496(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movq	%rdi, %rsi
	subq	$632, %rsp
	movq	%rdi, -648(%rbp)
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	_ZN6icu_67L8_set_addEP4USeti(%rip), %rdx
	movq	%rax, -560(%rbp)
	leaq	_ZN6icu_67L13_set_addRangeEP4USetii(%rip), %rax
	movl	$2, %r8d
	andl	$2, %ebx
	movq	%rax, %xmm1
	movaps	%xmm0, -576(%rbp)
	movq	%rdx, %xmm0
	leaq	_ZN6icu_67L14_set_addStringEP4USetPKDsi(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movw	%r8w, -552(%rbp)
	movq	%rax, -584(%rbp)
	movq	%r15, -608(%rbp)
	movl	%ebx, -652(%rbp)
	movups	%xmm0, -600(%rbp)
	jne	.L107
.L63:
	movq	-648(%rbp), %rdi
	leaq	-560(%rbp), %r14
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	%eax, -656(%rbp)
	testl	%eax, %eax
	jle	.L65
	leaq	-608(%rbp), %rax
	leaq	-560(%rbp), %r14
	movl	$0, -632(%rbp)
	movq	%rax, -672(%rbp)
	leaq	-624(%rbp), %r12
	leaq	-616(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L78:
	movl	-632(%rbp), %esi
	movq	-648(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	-648(%rbp), %rdi
	movl	-632(%rbp), %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	-652(%rbp), %edi
	testl	%edi, %edi
	je	.L66
	cmpl	%eax, %ebx
	jg	.L67
	movq	%r12, -664(%rbp)
	addl	$1, %eax
	movl	%ebx, %r12d
	movq	-672(%rbp), %rbx
	movq	%r14, -640(%rbp)
	movl	%eax, %r14d
	.p2align 4,,10
	.p2align 3
.L68:
	movl	%r12d, %edi
	movq	%rbx, %rsi
	addl	$1, %r12d
	call	ucase_addCaseClosure_67@PLT
	cmpl	%r14d, %r12d
	jne	.L68
	movq	-640(%rbp), %r14
	movq	-664(%rbp), %r12
.L67:
	addl	$1, -632(%rbp)
	movl	-632(%rbp), %eax
	cmpl	%eax, -656(%rbp)
	jne	.L78
.L65:
	movq	-648(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZNK6icu_6710UnicodeSet10hasStringsEv@PLT
	testb	%al, %al
	je	.L80
	movl	-652(%rbp), %esi
	testl	%esi, %esi
	je	.L81
	movq	80(%rbx), %rdi
	movl	8(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L80
	xorl	%ebx, %ebx
	leaq	-608(%rbp), %r12
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L109:
	movswl	%ax, %esi
	sarl	$5, %esi
	testb	$17, %al
	jne	.L93
.L110:
	testb	$2, %al
	leaq	-550(%rbp), %rdi
	movq	%r12, %rdx
	cmove	-536(%rbp), %rdi
	call	ucase_addStringCaseClosure_67@PLT
	testb	%al, %al
	je	.L108
.L86:
	movq	-648(%rbp), %rax
	addl	$1, %ebx
	movq	80(%rax), %rdi
	cmpl	%ebx, 8(%rdi)
	jle	.L80
.L88:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8foldCaseEj@PLT
	movzwl	-552(%rbp), %eax
	testw	%ax, %ax
	jns	.L109
	movl	-548(%rbp), %esi
	testb	$17, %al
	je	.L110
.L93:
	xorl	%edi, %edi
	movq	%r12, %rdx
	call	ucase_addStringCaseClosure_67@PLT
	testb	%al, %al
	jne	.L86
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L66:
	cmpl	%eax, %ebx
	jg	.L67
	addl	$1, %eax
	movl	%eax, -640(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L111:
	movl	%eax, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
.L69:
	movq	%r12, %rcx
	movl	$1, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%ebx, %edi
	call	ucase_toFullTitle_67@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L71
	cmpl	$31, %eax
	jle	.L72
	movl	%eax, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
.L71:
	movq	%r12, %rcx
	movl	$1, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%ebx, %edi
	call	ucase_toFullUpper_67@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L73
	cmpl	$31, %eax
	jle	.L74
	movl	%eax, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
.L73:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%ebx, %edi
	call	ucase_toFullFolding_67@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L75
	cmpl	$31, %eax
	jle	.L76
	movl	%eax, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
.L75:
	addl	$1, %ebx
	cmpl	-640(%rbp), %ebx
	je	.L67
.L77:
	movq	%r12, %rcx
	movl	$1, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%ebx, %edi
	call	ucase_toFullLower_67@PLT
	movl	%eax, %ecx
	testl	%eax, %eax
	js	.L69
	cmpl	$31, %eax
	jg	.L111
	movq	-624(%rbp), %rax
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-624(%rbp), %rax
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-624(%rbp), %rax
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L74:
	movq	-624(%rbp), %rax
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	jmp	.L73
.L81:
	leaq	-288(%rbp), %r12
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	leaq	-616(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -616(%rbp)
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	-616(%rbp), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jle	.L112
.L89:
	testq	%r13, %r13
	je	.L91
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L91:
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	movq	-648(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$632, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L107:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet10hasStringsEv@PLT
	testb	%al, %al
	je	.L63
	movq	-416(%rbp), %rdi
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L63
.L112:
	movq	-648(%rbp), %rax
	movq	80(%rax), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L89
	movq	%r13, -632(%rbp)
	movl	-652(%rbp), %ebx
	.p2align 4,,10
	.p2align 3
.L90:
	movl	%ebx, %esi
	addl	$1, %ebx
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeString7toLowerERKNS_6LocaleE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-632(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeString7toUpperERKNS_6LocaleE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeString8foldCaseEj@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	-648(%rbp), %rax
	movq	80(%rax), %rdi
	cmpl	%ebx, 8(%rdi)
	jg	.L90
	movq	-632(%rbp), %r13
	jmp	.L89
.L113:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4071:
	.size	_ZN6icu_6710UnicodeSet9closeOverEi.part.0, .-_ZN6icu_6710UnicodeSet9closeOverEi.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet9closeOverEi
	.type	_ZN6icu_6710UnicodeSet9closeOverEi, @function
_ZN6icu_6710UnicodeSet9closeOverEi:
.LFB3144:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	movq	%rdi, %rax
	je	.L123
.L120:
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	cmpq	$0, 88(%rdi)
	jne	.L120
	testb	$1, 32(%rdi)
	jne	.L120
	testb	$6, %sil
	je	.L120
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	_ZN6icu_6710UnicodeSet9closeOverEi.part.0
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3144:
	.size	_ZN6icu_6710UnicodeSet9closeOverEi, .-_ZN6icu_6710UnicodeSet9closeOverEi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
