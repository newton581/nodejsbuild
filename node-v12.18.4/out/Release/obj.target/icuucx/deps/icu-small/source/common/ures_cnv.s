	.file	"ures_cnv.cpp"
	.text
	.p2align 4
	.globl	ures_openU_67
	.type	ures_openU_67, @function
ures_openU_67:
.LFB2262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1064, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L14
	movq	%rdx, %r12
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L14
	movq	%rdi, %r14
	movq	%rsi, %r13
	testq	%rdi, %rdi
	je	.L5
	call	u_strlen_67@PLT
	movl	%eax, %ebx
	cmpl	$1023, %eax
	jg	.L8
	movl	%eax, %esi
	movq	%r14, %rdi
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	jne	.L16
	movq	%r12, %rdi
	call	u_getDefaultConverter_67@PLT
	movl	%ebx, %r8d
	movq	%r12, %r9
	movq	%r14, %rcx
	leaq	-1088(%rbp), %rsi
	movq	%rax, %r15
	movl	$1024, %edx
	movq	%rax, %rdi
	movq	%rsi, -1096(%rbp)
	call	ucnv_fromUChars_67@PLT
	movq	%r15, %rdi
	movl	%eax, %ebx
	call	u_releaseDefaultConverter_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L14
	cmpl	$1023, %ebx
	jg	.L8
	movq	-1096(%rbp), %rsi
	movq	%rsi, %r14
.L5:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	ures_open_67@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1, (%r12)
.L14:
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L17
	addq	$1064, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	leal	1(%rbx), %edx
	leaq	-1088(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rbx, %r14
	call	u_UCharsToChars_67@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	ures_open_67@PLT
	jmp	.L1
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2262:
	.size	ures_openU_67, .-ures_openU_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
