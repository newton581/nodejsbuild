	.file	"dictbe.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DictionaryBreakEngineC2Ev
	.type	_ZN6icu_6721DictionaryBreakEngineC2Ev, @function
_ZN6icu_6721DictionaryBreakEngineC2Ev:
.LFB2491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6719LanguageBreakEngineC2Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%rbx), %rdi
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSetC1Ev@PLT
	.cfi_endproc
.LFE2491:
	.size	_ZN6icu_6721DictionaryBreakEngineC2Ev, .-_ZN6icu_6721DictionaryBreakEngineC2Ev
	.globl	_ZN6icu_6721DictionaryBreakEngineC1Ev
	.set	_ZN6icu_6721DictionaryBreakEngineC1Ev,_ZN6icu_6721DictionaryBreakEngineC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	.type	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi, @function
_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi:
.LFB2509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	utext_getNativeIndex_67@PLT
	movq	%rax, %r12
	cmpl	%eax, 8(%rbx)
	je	.L5
	movl	%r14d, %edx
	movl	%eax, 8(%rbx)
	leaq	4(%rbx), %rcx
	leaq	100(%rbx), %r9
	subl	%eax, %edx
	movq	(%r15), %rax
	pushq	%rcx
	leaq	20(%rbx), %r8
	pushq	$0
	movl	$20, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	*16(%rax)
	popq	%rdx
	popq	%rcx
	movl	%eax, (%rbx)
	testl	%eax, %eax
	jle	.L9
.L6:
	subl	$1, %eax
	movq	%r13, %rdi
	cltq
	movl	20(%rbx,%rax,4), %esi
	addl	%r12d, %esi
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	movl	(%rbx), %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	movslq	%r12d, %rsi
	movq	%r13, %rdi
	call	utext_setNativeIndex_67@PLT
.L5:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L6
.L7:
	leal	-1(%rax), %edx
	movl	%edx, 16(%rbx)
	movl	%edx, 12(%rbx)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2509:
	.size	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi, .-_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PossibleWord12acceptMarkedEP5UText
	.type	_ZN6icu_6712PossibleWord12acceptMarkedEP5UText, @function
_ZN6icu_6712PossibleWord12acceptMarkedEP5UText:
.LFB2510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movslq	12(%rbx), %rax
	movl	8(%rbx), %esi
	addl	20(%rbx,%rax,4), %esi
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	movslq	12(%rbx), %rax
	movl	20(%rbx,%rax,4), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2510:
	.size	_ZN6icu_6712PossibleWord12acceptMarkedEP5UText, .-_ZN6icu_6712PossibleWord12acceptMarkedEP5UText
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712PossibleWord6backUpEP5UText
	.type	_ZN6icu_6712PossibleWord6backUpEP5UText, @function
_ZN6icu_6712PossibleWord6backUpEP5UText:
.LFB2511:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L20
	xorl	%r9d, %r9d
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %eax
	movq	%rsi, %r8
	movl	%eax, 16(%rdi)
	cltq
	movl	20(%rdi,%rax,4), %esi
	addl	8(%rdi), %esi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	movl	$1, %r9d
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE2511:
	.size	_ZN6icu_6712PossibleWord6backUpEP5UText, .-_ZN6icu_6712PossibleWord6backUpEP5UText
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DictionaryBreakEngineD2Ev
	.type	_ZN6icu_6721DictionaryBreakEngineD2Ev, @function
_ZN6icu_6721DictionaryBreakEngineD2Ev:
.LFB2494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	.cfi_endproc
.LFE2494:
	.size	_ZN6icu_6721DictionaryBreakEngineD2Ev, .-_ZN6icu_6721DictionaryBreakEngineD2Ev
	.globl	_ZN6icu_6721DictionaryBreakEngineD1Ev
	.set	_ZN6icu_6721DictionaryBreakEngineD1Ev,_ZN6icu_6721DictionaryBreakEngineD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DictionaryBreakEngineD0Ev
	.type	_ZN6icu_6721DictionaryBreakEngineD0Ev, @function
_ZN6icu_6721DictionaryBreakEngineD0Ev:
.LFB2496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2496:
	.size	_ZN6icu_6721DictionaryBreakEngineD0Ev, .-_ZN6icu_6721DictionaryBreakEngineD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ThaiBreakEngineD2Ev
	.type	_ZN6icu_6715ThaiBreakEngineD2Ev, @function
_ZN6icu_6715ThaiBreakEngineD2Ev:
.LFB2516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715ThaiBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1208(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L26
	movq	(%rdi), %rax
	call	*8(%rax)
.L26:
	leaq	1008(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	808(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	608(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	.cfi_endproc
.LFE2516:
	.size	_ZN6icu_6715ThaiBreakEngineD2Ev, .-_ZN6icu_6715ThaiBreakEngineD2Ev
	.globl	_ZN6icu_6715ThaiBreakEngineD1Ev
	.set	_ZN6icu_6715ThaiBreakEngineD1Ev,_ZN6icu_6715ThaiBreakEngineD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714LaoBreakEngineD2Ev
	.type	_ZN6icu_6714LaoBreakEngineD2Ev, @function
_ZN6icu_6714LaoBreakEngineD2Ev:
.LFB2524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LaoBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1008(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	(%rdi), %rax
	call	*8(%rax)
.L32:
	leaq	808(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	608(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	.cfi_endproc
.LFE2524:
	.size	_ZN6icu_6714LaoBreakEngineD2Ev, .-_ZN6icu_6714LaoBreakEngineD2Ev
	.globl	_ZN6icu_6714LaoBreakEngineD1Ev
	.set	_ZN6icu_6714LaoBreakEngineD1Ev,_ZN6icu_6714LaoBreakEngineD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718BurmeseBreakEngineD2Ev
	.type	_ZN6icu_6718BurmeseBreakEngineD2Ev, @function
_ZN6icu_6718BurmeseBreakEngineD2Ev:
.LFB2532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718BurmeseBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1008(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L38
	movq	(%rdi), %rax
	call	*8(%rax)
.L38:
	leaq	808(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	608(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	.cfi_endproc
.LFE2532:
	.size	_ZN6icu_6718BurmeseBreakEngineD2Ev, .-_ZN6icu_6718BurmeseBreakEngineD2Ev
	.globl	_ZN6icu_6718BurmeseBreakEngineD1Ev
	.set	_ZN6icu_6718BurmeseBreakEngineD1Ev,_ZN6icu_6718BurmeseBreakEngineD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716KhmerBreakEngineD2Ev
	.type	_ZN6icu_6716KhmerBreakEngineD2Ev, @function
_ZN6icu_6716KhmerBreakEngineD2Ev:
.LFB2540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6716KhmerBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1008(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L44
	movq	(%rdi), %rax
	call	*8(%rax)
.L44:
	leaq	808(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	608(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	.cfi_endproc
.LFE2540:
	.size	_ZN6icu_6716KhmerBreakEngineD2Ev, .-_ZN6icu_6716KhmerBreakEngineD2Ev
	.globl	_ZN6icu_6716KhmerBreakEngineD1Ev
	.set	_ZN6icu_6716KhmerBreakEngineD1Ev,_ZN6icu_6716KhmerBreakEngineD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CjkBreakEngineD2Ev
	.type	_ZN6icu_6714CjkBreakEngineD2Ev, @function
_ZN6icu_6714CjkBreakEngineD2Ev:
.LFB2548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714CjkBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1008(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rax
	call	*8(%rax)
.L50:
	leaq	808(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	608(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	.cfi_endproc
.LFE2548:
	.size	_ZN6icu_6714CjkBreakEngineD2Ev, .-_ZN6icu_6714CjkBreakEngineD2Ev
	.globl	_ZN6icu_6714CjkBreakEngineD1Ev
	.set	_ZN6icu_6714CjkBreakEngineD1Ev,_ZN6icu_6714CjkBreakEngineD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714LaoBreakEngineD0Ev
	.type	_ZN6icu_6714LaoBreakEngineD0Ev, @function
_ZN6icu_6714LaoBreakEngineD0Ev:
.LFB2526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714LaoBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1008(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L56
	movq	(%rdi), %rax
	call	*8(%rax)
.L56:
	leaq	808(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	608(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2526:
	.size	_ZN6icu_6714LaoBreakEngineD0Ev, .-_ZN6icu_6714LaoBreakEngineD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718BurmeseBreakEngineD0Ev
	.type	_ZN6icu_6718BurmeseBreakEngineD0Ev, @function
_ZN6icu_6718BurmeseBreakEngineD0Ev:
.LFB2534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718BurmeseBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1008(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L62
	movq	(%rdi), %rax
	call	*8(%rax)
.L62:
	leaq	808(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	608(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2534:
	.size	_ZN6icu_6718BurmeseBreakEngineD0Ev, .-_ZN6icu_6718BurmeseBreakEngineD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716KhmerBreakEngineD0Ev
	.type	_ZN6icu_6716KhmerBreakEngineD0Ev, @function
_ZN6icu_6716KhmerBreakEngineD0Ev:
.LFB2542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6716KhmerBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1008(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	(%rdi), %rax
	call	*8(%rax)
.L68:
	leaq	808(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	608(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2542:
	.size	_ZN6icu_6716KhmerBreakEngineD0Ev, .-_ZN6icu_6716KhmerBreakEngineD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CjkBreakEngineD0Ev
	.type	_ZN6icu_6714CjkBreakEngineD0Ev, @function
_ZN6icu_6714CjkBreakEngineD0Ev:
.LFB2550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714CjkBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1008(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L74
	movq	(%rdi), %rax
	call	*8(%rax)
.L74:
	leaq	808(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	608(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2550:
	.size	_ZN6icu_6714CjkBreakEngineD0Ev, .-_ZN6icu_6714CjkBreakEngineD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ThaiBreakEngineD0Ev
	.type	_ZN6icu_6715ThaiBreakEngineD0Ev, @function
_ZN6icu_6715ThaiBreakEngineD0Ev:
.LFB2518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715ThaiBreakEngineE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	1208(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	(%rdi), %rax
	call	*8(%rax)
.L80:
	leaq	1008(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	808(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	608(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	408(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	208(%r12), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6719LanguageBreakEngineD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2518:
	.size	_ZN6icu_6715ThaiBreakEngineD0Ev, .-_ZN6icu_6715ThaiBreakEngineD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721DictionaryBreakEngine7handlesEi
	.type	_ZNK6icu_6721DictionaryBreakEngine7handlesEi, @function
_ZNK6icu_6721DictionaryBreakEngine7handlesEi:
.LFB2497:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	.cfi_endproc
.LFE2497:
	.size	_ZNK6icu_6721DictionaryBreakEngine7handlesEi, .-_ZNK6icu_6721DictionaryBreakEngine7handlesEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.type	_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E, @function
_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E:
.LFB2498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	8(%rbx), %r14
	subq	$40, %rsp
	movq	%rdi, -64(%rbp)
	movq	%rsi, %rdi
	movq	%r8, -72(%rbp)
	call	utext_getNativeIndex_67@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	utext_current32_67@PLT
	movl	%eax, %r15d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L93:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L87
	movq	%r12, %rdi
	call	utext_next32_67@PLT
	movq	%r12, %rdi
	call	utext_current32_67@PLT
	movl	%eax, %r15d
.L88:
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	movl	%eax, %ebx
	cmpl	%r13d, %eax
	jl	.L93
.L87:
	movq	-64(%rbp), %rdi
	movq	-72(%rbp), %r8
	movl	%ebx, %ecx
	movq	%r12, %rsi
	movl	-56(%rbp), %edx
	movq	(%rdi), %rax
	call	*40(%rax)
	movslq	%ebx, %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	utext_setNativeIndex_67@PLT
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2498:
	.size	_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E, .-_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715ThaiBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.type	_ZNK6icu_6715ThaiBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E, @function
_ZNK6icu_6715ThaiBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E:
.LFB2519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movslq	%edx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$696, %rsp
	movq	%r8, -664(%rbp)
	movq	%rdi, -632(%rbp)
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	utext_setNativeIndex_67@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	utext_moveIndex32_67@PLT
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	movslq	%r15d, %rdx
	movq	%rax, %r8
	xorl	%eax, %eax
	cmpq	%rdx, %r8
	jl	.L187
.L94:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L188
	addq	$696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	utext_setNativeIndex_67@PLT
	movdqa	.LC0(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$0, -612(%rbp)
	movl	$0, -592(%rbp)
	movl	$0, -412(%rbp)
	movl	$0, -232(%rbp)
	movaps	%xmm0, -608(%rbp)
	movups	%xmm0, -428(%rbp)
	movups	%xmm0, -248(%rbp)
	call	utext_setNativeIndex_67@PLT
	leaq	-608(%rbp), %rax
	movl	-612(%rbp), %edx
	movl	$0, -636(%rbp)
	movq	%rax, -656(%rbp)
	leaq	408(%rbx), %rax
	movq	%rax, -680(%rbp)
	testl	%edx, %edx
	jg	.L97
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rax, -648(%rbp)
	cmpl	%eax, %r15d
	jle	.L97
	movl	-636(%rbp), %eax
	movl	$2863311531, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	imulq	%rdx, %rax
	movl	%ecx, %ebx
	movl	%r15d, %ecx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %ebx
	movq	-656(%rbp), %rax
	imulq	$180, %rbx, %r13
	leaq	(%rax,%r13), %r14
	movq	-632(%rbp), %rax
	movq	%r14, %rdi
	movq	1208(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	cmpl	$1, %eax
	je	.L189
	movq	%r12, %rdi
	jg	.L103
	call	utext_getNativeIndex_67@PLT
	xorl	%r13d, %r13d
	cmpl	%eax, %r15d
	jle	.L105
	movq	-632(%rbp), %rax
	movl	%r15d, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$0, %r13d
	movq	1208(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	movl	-648(%rbp), %eax
	movl	%eax, -688(%rbp)
	jle	.L113
.L114:
	movslq	-688(%rbp), %rsi
	movq	%r12, %rdi
	call	utext_setNativeIndex_67@PLT
	.p2align 4,,10
	.p2align 3
.L105:
	movq	-632(%rbp), %rax
	leaq	1008(%rax), %r14
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%r12, %rdi
	call	utext_current32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L123
	movq	%r12, %rdi
	call	utext_next32_67@PLT
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	subl	%ebx, %eax
	addl	%eax, %r13d
.L124:
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rax, %rbx
	cmpl	%eax, %r15d
	jg	.L120
.L123:
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	cmpl	%eax, %r15d
	jle	.L122
	testl	%r13d, %r13d
	jg	.L190
.L135:
	movl	-612(%rbp), %eax
	testl	%eax, %eax
	jle	.L96
.L97:
	movq	-664(%rbp), %rax
	movl	8(%rax), %eax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L191
	testl	%eax, %eax
	jle	.L136
	movq	-664(%rbp), %rcx
	movslq	%edx, %rax
	movq	24(%rcx), %rcx
	cmpl	(%rcx,%rax,4), %r15d
	jg	.L138
.L137:
	movq	-664(%rbp), %rax
	movl	%edx, 8(%rax)
.L139:
	subl	$1, -636(%rbp)
.L138:
	movl	-636(%rbp), %eax
	jmp	.L94
.L126:
	movq	-632(%rbp), %rax
	movq	%r12, %rdi
	leaq	808(%rax), %r14
	call	utext_current32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L128
	cmpl	$3631, %ebx
	je	.L192
	cmpl	$3654, %ebx
	jne	.L186
.L131:
	movq	%r12, %rdi
	call	utext_previous32_67@PLT
	movq	%r12, %rdi
	cmpl	$3654, %eax
	je	.L132
	call	utext_next32_67@PLT
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	utext_next32_67@PLT
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	subl	%ebx, %eax
	addl	%eax, %r13d
	.p2align 4,,10
	.p2align 3
.L122:
	testl	%r13d, %r13d
	jle	.L135
.L186:
	addl	-648(%rbp), %r13d
.L127:
	movq	-664(%rbp), %rcx
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L133
	cmpl	12(%rcx), %esi
	jle	.L134
.L133:
	movq	-664(%rbp), %rbx
	leaq	-612(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L135
	movslq	8(%rbx), %rax
.L134:
	movq	-664(%rbp), %rcx
	movq	24(%rcx), %rdx
	movl	%r13d, (%rdx,%rax,4)
	addl	$1, 8(%rcx)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L103:
	call	utext_getNativeIndex_67@PLT
	movl	-636(%rbp), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, -636(%rbp)
	cmpl	%eax, %r15d
	jg	.L193
	movl	-600(%rbp,%r13), %eax
.L109:
	imulq	$180, %rbx, %r13
	leaq	(%rbx,%rbx,2), %rdx
	movq	%r12, %rdi
	movq	%rdx, %rbx
	salq	$4, %rbx
	movslq	-596(%rbp,%r13), %rcx
	subq	%rdx, %rbx
	leaq	4(%rcx,%rbx), %rdx
	addl	-604(%rbp,%rdx,4), %eax
	movslq	%eax, %rsi
	call	utext_setNativeIndex_67@PLT
	movslq	-596(%rbp,%r13), %rax
	addq	%rax, %rbx
	movl	-588(%rbp,%rbx,4), %r13d
	movl	-508(%rbp,%rbx,4), %ebx
.L102:
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	cmpl	$2, %ebx
	jg	.L105
	cmpl	%eax, %r15d
	jle	.L105
	movl	-636(%rbp), %eax
	movl	$3, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	divl	%ecx
	movq	-632(%rbp), %rax
	movl	%r15d, %ecx
	movl	%edx, %ebx
	movq	1208(%rax), %rdx
	imulq	$180, %rbx, %rbx
	addq	-656(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	movl	-648(%rbp), %eax
	jg	.L112
	testl	%r13d, %r13d
	je	.L143
	addl	%r13d, %eax
	cmpl	$2, 4(%rbx)
	movl	%eax, -688(%rbp)
	jg	.L114
.L113:
	movl	-636(%rbp), %eax
	movl	$2863311531, %ecx
	movl	%r15d, %r14d
	movl	$0, -672(%rbp)
	movl	%r13d, -704(%rbp)
	subl	-688(%rbp), %r14d
	addl	$1, %eax
	movq	%rax, %rdx
	movl	%eax, -712(%rbp)
	imulq	%rcx, %rax
	movl	%edx, %ecx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %ecx
	imulq	$180, %rcx, %rax
	addq	-656(%rbp), %rax
	movq	%rax, -696(%rbp)
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	utext_next32_67@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	utext_getNativeIndex_67@PLT
	subl	%ebx, %eax
	addl	%eax, -672(%rbp)
	subl	%eax, %r14d
	testl	%r14d, %r14d
	jle	.L185
	movq	%r12, %rdi
	call	utext_current32_67@PLT
	movq	-680(%rbp), %rdi
	movl	%r13d, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L115
	movq	-632(%rbp), %rax
	movl	%ebx, %esi
	leaq	608(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L115
	movq	-632(%rbp), %rax
	movq	-696(%rbp), %rdi
	movq	%r12, %rsi
	movl	%r15d, %ecx
	movq	1208(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	movl	-672(%rbp), %esi
	movq	%r12, %rdi
	addl	-688(%rbp), %esi
	movl	%eax, %ebx
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	testl	%ebx, %ebx
	jle	.L115
.L185:
	movl	-704(%rbp), %r13d
	movl	-712(%rbp), %eax
	testl	%r13d, %r13d
	cmovg	-636(%rbp), %eax
	addl	-672(%rbp), %r13d
	movl	%eax, -636(%rbp)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L190:
	movq	-632(%rbp), %rax
	movl	$2863311531, %esi
	movq	1208(%rax), %rdx
	movl	-636(%rbp), %eax
	movq	%rax, %rcx
	imulq	%rsi, %rax
	movq	%r12, %rsi
	movl	%ecx, %edi
	movl	%r15d, %ecx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %edi
	imulq	$180, %rdi, %rdi
	addq	-656(%rbp), %rdi
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	jle	.L126
.L128:
	addl	-648(%rbp), %r13d
	movq	%r12, %rdi
	movslq	%r13d, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	(%rbx,%rbx,2), %rax
	movslq	-596(%rbp,%r13), %rdx
	movl	-600(%rbp,%r13), %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	salq	$4, %rbx
	subq	%rax, %rbx
	leaq	4(%rdx,%rbx), %rax
	addl	-604(%rbp,%rax,4), %esi
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	movslq	-596(%rbp,%r13), %rax
	addl	$1, -636(%rbp)
	addq	%rax, %rbx
	movl	-588(%rbp,%rbx,4), %r13d
	movl	-508(%rbp,%rbx,4), %ebx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L193:
	movl	%ecx, %eax
	movl	$2863311531, %edi
	movl	%ecx, %esi
	movq	-656(%rbp), %r9
	imulq	%rdi, %rax
	addl	$2, %edx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %esi
	imulq	$180, %rsi, %rcx
	movq	%rsi, -704(%rbp)
	leaq	(%r9,%rcx), %rax
	movq	%rcx, -688(%rbp)
	movq	%rax, -672(%rbp)
	movq	%rdx, %rax
	imulq	%rdi, %rdx
	shrq	$33, %rdx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %eax
	imulq	$180, %rax, %rax
	addq	%r9, %rax
	movq	%rax, -696(%rbp)
	leaq	(%rsi,%rsi), %rax
	movq	%rax, -712(%rbp)
	leaq	-48(%rbp), %rax
	addq	%rax, %r13
	movl	%r15d, %eax
	movq	%r12, %r15
	movq	%r14, %r12
	movl	%eax, %r14d
.L111:
	movq	-632(%rbp), %rax
	movq	-672(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	1208(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	jg	.L194
.L107:
	movl	-544(%r13), %edx
	movl	-552(%r13), %eax
	testl	%edx, %edx
	jg	.L195
	movq	%r15, %r12
	movl	%r14d, %r15d
	jmp	.L109
.L191:
	testl	%r15d, %r15d
	jg	.L138
	testl	%eax, %eax
	jg	.L137
	jmp	.L139
.L136:
	testl	%r15d, %r15d
	jle	.L139
	jmp	.L138
.L195:
	leaq	(%rbx,%rbx,2), %rdi
	subl	$1, %edx
	movq	%rdi, %rsi
	movl	%edx, -544(%r13)
	movslq	%edx, %rdx
	salq	$4, %rsi
	subq	%rdi, %rsi
	movq	%r15, %rdi
	leaq	4(%rdx,%rsi), %rdx
	addl	-604(%rbp,%rdx,4), %eax
	movslq	%eax, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L111
.L194:
	movl	16(%r12), %eax
	movq	%r15, %rdi
	movl	%eax, 12(%r12)
	call	utext_getNativeIndex_67@PLT
	cmpl	%eax, %r14d
	jle	.L177
	movq	-712(%rbp), %rax
	addq	-704(%rbp), %rax
	movq	%rax, -720(%rbp)
	salq	$4, %rax
	movq	%rax, -728(%rbp)
.L108:
	movq	-632(%rbp), %rax
	movq	-696(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	1208(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	jne	.L196
	movq	-688(%rbp), %rax
	movl	-592(%rbp,%rax), %eax
	testl	%eax, %eax
	jle	.L107
	movq	-688(%rbp), %rcx
	subl	$1, %eax
	movq	-728(%rbp), %rdx
	movq	%r15, %rdi
	subq	-720(%rbp), %rdx
	movl	%eax, -592(%rbp,%rcx)
	cltq
	leaq	4(%rax,%rdx), %rax
	movl	-604(%rbp,%rax,4), %esi
	addl	-600(%rbp,%rcx), %esi
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L108
.L192:
	movq	%r12, %rdi
	call	utext_previous32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movq	%r12, %rdi
	testb	%al, %al
	jne	.L130
	call	utext_next32_67@PLT
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	utext_next32_67@PLT
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%r12, %rdi
	subl	%ebx, %eax
	addl	%eax, %r13d
	call	utext_current32_67@PLT
	cmpl	$3654, %eax
	jne	.L122
	jmp	.L131
.L130:
	call	utext_next32_67@PLT
	addl	-648(%rbp), %r13d
	jmp	.L127
.L196:
	movl	%r14d, %eax
	movq	%r12, %r14
	movq	%r15, %r12
	movl	%eax, %r15d
	movl	16(%r14), %eax
	movl	%eax, 12(%r14)
	imulq	$180, %rbx, %rax
	movl	-600(%rbp,%rax), %eax
	jmp	.L109
.L132:
	call	utext_next32_67@PLT
	jmp	.L122
.L177:
	imulq	$180, %rbx, %rax
	movq	%r15, %r12
	movl	%r14d, %r15d
	movl	-600(%rbp,%rax), %eax
	jmp	.L109
.L188:
	call	__stack_chk_fail@PLT
.L143:
	movl	%eax, -688(%rbp)
	jmp	.L113
.L112:
	addl	%r13d, %eax
	movl	%eax, -688(%rbp)
	jmp	.L114
	.cfi_endproc
.LFE2519:
	.size	_ZNK6icu_6715ThaiBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E, .-_ZNK6icu_6715ThaiBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.align 2
	.p2align 4
	.type	_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0, @function
_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0:
.LFB3492:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movslq	%edx, %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$696, %rsp
	movq	%rdi, -632(%rbp)
	movdqa	.LC0(%rip), %xmm0
	movq	%r15, %rdi
	movq	%r8, -664(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -608(%rbp)
	movl	$0, -612(%rbp)
	movl	$0, -592(%rbp)
	movl	$0, -412(%rbp)
	movl	$0, -232(%rbp)
	movups	%xmm0, -428(%rbp)
	movups	%xmm0, -248(%rbp)
	call	utext_setNativeIndex_67@PLT
	leaq	-608(%rbp), %rax
	movl	-612(%rbp), %edx
	movl	$0, -636(%rbp)
	movq	%rax, -656(%rbp)
	leaq	408(%rbx), %rax
	movq	%rax, -680(%rbp)
	testl	%edx, %edx
	jg	.L199
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rax, -648(%rbp)
	cmpl	%r12d, %eax
	jge	.L199
	movl	-636(%rbp), %eax
	movl	$2863311531, %edx
	movq	%r15, %rsi
	movq	%rax, %rcx
	imulq	%rdx, %rax
	movl	%ecx, %ebx
	movl	%r12d, %ecx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %ebx
	movq	-656(%rbp), %rax
	imulq	$180, %rbx, %r13
	leaq	(%rax,%r13), %r14
	movq	-632(%rbp), %rax
	movq	%r14, %rdi
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	cmpl	$1, %eax
	je	.L270
	movq	%r15, %rdi
	jg	.L205
	call	utext_getNativeIndex_67@PLT
	xorl	%r13d, %r13d
	cmpl	%eax, %r12d
	jle	.L207
	movq	-632(%rbp), %rax
	movl	%r12d, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	$0, %r13d
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	movl	-648(%rbp), %eax
	movl	%eax, -688(%rbp)
	jle	.L215
.L216:
	movslq	-688(%rbp), %rsi
	movq	%r15, %rdi
	call	utext_setNativeIndex_67@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	movq	-632(%rbp), %rax
	leaq	808(%rax), %r14
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%r15, %rdi
	call	utext_current32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L225
	movq	%r15, %rdi
	call	utext_next32_67@PLT
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	subl	%ebx, %eax
	addl	%eax, %r13d
.L226:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rax, %rbx
	cmpl	%eax, %r12d
	jg	.L222
.L225:
	testl	%r13d, %r13d
	jg	.L271
.L229:
	movl	-612(%rbp), %eax
	testl	%eax, %eax
	jle	.L198
.L199:
	movq	-664(%rbp), %rax
	movl	8(%rax), %eax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L272
	testl	%eax, %eax
	jle	.L230
	movq	-664(%rbp), %rcx
	movslq	%edx, %rax
	movq	24(%rcx), %rcx
	cmpl	(%rcx,%rax,4), %r12d
	jle	.L231
	.p2align 4,,10
	.p2align 3
.L232:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L273
	movl	-636(%rbp), %eax
	addq	$696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	-664(%rbp), %rcx
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L227
	cmpl	12(%rcx), %esi
	jle	.L228
.L227:
	movq	-664(%rbp), %rbx
	leaq	-612(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L229
	movslq	8(%rbx), %rax
.L228:
	movq	-664(%rbp), %rcx
	addl	-648(%rbp), %r13d
	movq	24(%rcx), %rdx
	movl	%r13d, (%rdx,%rax,4)
	addl	$1, 8(%rcx)
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L205:
	call	utext_getNativeIndex_67@PLT
	movl	-636(%rbp), %esi
	movq	%rax, %r8
	movslq	%r12d, %rax
	leal	1(%rsi), %edx
	movl	%edx, -636(%rbp)
	cmpq	%rax, %r8
	jl	.L274
	movl	-600(%rbp,%r13), %eax
.L211:
	imulq	$180, %rbx, %r13
	leaq	(%rbx,%rbx,2), %rdx
	movq	%r15, %rdi
	movq	%rdx, %rbx
	salq	$4, %rbx
	movslq	-596(%rbp,%r13), %rcx
	subq	%rdx, %rbx
	leaq	4(%rcx,%rbx), %rdx
	addl	-604(%rbp,%rdx,4), %eax
	movslq	%eax, %rsi
	call	utext_setNativeIndex_67@PLT
	movslq	-596(%rbp,%r13), %rax
	addq	%rax, %rbx
	movl	-588(%rbp,%rbx,4), %r13d
	movl	-508(%rbp,%rbx,4), %ebx
.L204:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	cmpl	$2, %ebx
	jg	.L207
	cmpl	%eax, %r12d
	jle	.L207
	movl	-636(%rbp), %eax
	movl	$3, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	divl	%ecx
	movq	-632(%rbp), %rax
	movl	%r12d, %ecx
	movl	%edx, %ebx
	movq	1008(%rax), %rdx
	imulq	$180, %rbx, %rbx
	addq	-656(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	movl	-648(%rbp), %eax
	jg	.L214
	testl	%r13d, %r13d
	je	.L236
	addl	%r13d, %eax
	cmpl	$2, 4(%rbx)
	movl	%eax, -688(%rbp)
	jg	.L216
.L215:
	movl	-636(%rbp), %eax
	movl	$2863311531, %edx
	movl	%r12d, %r14d
	movl	$0, -672(%rbp)
	movl	%r13d, -704(%rbp)
	subl	-688(%rbp), %r14d
	addl	$1, %eax
	movq	%rax, %rcx
	movl	%eax, -712(%rbp)
	imulq	%rdx, %rax
	movl	%ecx, %edx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %edx
	imulq	$180, %rdx, %rax
	addq	-656(%rbp), %rax
	movq	%rax, -696(%rbp)
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	utext_next32_67@PLT
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	utext_getNativeIndex_67@PLT
	subl	%ebx, %eax
	addl	%eax, -672(%rbp)
	subl	%eax, %r14d
	testl	%r14d, %r14d
	jle	.L269
	movq	%r15, %rdi
	call	utext_current32_67@PLT
	movq	-680(%rbp), %rdi
	movl	%r13d, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L217
	movq	-632(%rbp), %rax
	movl	%ebx, %esi
	leaq	608(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L217
	movq	-632(%rbp), %rax
	movq	-696(%rbp), %rdi
	movq	%r15, %rsi
	movl	%r12d, %ecx
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	movl	-672(%rbp), %esi
	movq	%r15, %rdi
	addl	-688(%rbp), %esi
	movl	%eax, %ebx
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	testl	%ebx, %ebx
	jle	.L217
.L269:
	movl	-704(%rbp), %r13d
	movl	-712(%rbp), %eax
	testl	%r13d, %r13d
	cmovg	-636(%rbp), %eax
	addl	-672(%rbp), %r13d
	movl	%eax, -636(%rbp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	(%rbx,%rbx,2), %rax
	movslq	-596(%rbp,%r13), %rdx
	movl	-600(%rbp,%r13), %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	salq	$4, %rbx
	subq	%rax, %rbx
	leaq	4(%rdx,%rbx), %rax
	addl	-604(%rbp,%rax,4), %esi
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	movslq	-596(%rbp,%r13), %rax
	addl	$1, -636(%rbp)
	addq	%rax, %rbx
	movl	-588(%rbp,%rbx,4), %r13d
	movl	-508(%rbp,%rbx,4), %ebx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L274:
	movl	%edx, %eax
	movl	$2863311531, %r9d
	movl	%edx, %edi
	movq	-656(%rbp), %r10
	imulq	%r9, %rax
	leal	2(%rsi), %edx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %edi
	imulq	$180, %rdi, %rcx
	movq	%rdi, -704(%rbp)
	leaq	(%r10,%rcx), %rax
	movq	%rcx, -688(%rbp)
	movq	%rax, -672(%rbp)
	movq	%rdx, %rax
	imulq	%r9, %rdx
	shrq	$33, %rdx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %eax
	imulq	$180, %rax, %rax
	addq	%r10, %rax
	movq	%rax, -696(%rbp)
	leaq	(%rdi,%rdi), %rax
	movq	%rax, -712(%rbp)
	leaq	-48(%rbp), %rax
	addq	%rax, %r13
	movl	%r12d, %eax
	movq	%r14, %r12
	movl	%eax, %r14d
.L213:
	movq	-632(%rbp), %rax
	movq	-672(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	jg	.L275
.L209:
	movl	-544(%r13), %edx
	movl	-552(%r13), %eax
	testl	%edx, %edx
	jg	.L276
	movl	%r14d, %r12d
	jmp	.L211
.L272:
	testl	%r12d, %r12d
	jg	.L232
	testl	%eax, %eax
	jle	.L233
.L231:
	movq	-664(%rbp), %rax
	movl	%edx, 8(%rax)
.L233:
	subl	$1, -636(%rbp)
	jmp	.L232
.L230:
	testl	%r12d, %r12d
	jle	.L233
	jmp	.L232
.L276:
	leaq	(%rbx,%rbx,2), %rdi
	subl	$1, %edx
	movq	%rdi, %rsi
	movl	%edx, -544(%r13)
	movslq	%edx, %rdx
	salq	$4, %rsi
	subq	%rdi, %rsi
	movq	%r15, %rdi
	leaq	4(%rdx,%rsi), %rdx
	addl	-604(%rbp,%rdx,4), %eax
	movslq	%eax, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L213
.L275:
	movl	16(%r12), %eax
	movq	%r15, %rdi
	movl	%eax, 12(%r12)
	call	utext_getNativeIndex_67@PLT
	cmpl	%eax, %r14d
	jle	.L261
	movq	-712(%rbp), %rax
	addq	-704(%rbp), %rax
	movq	%rax, -720(%rbp)
	salq	$4, %rax
	movq	%rax, -728(%rbp)
.L210:
	movq	-632(%rbp), %rax
	movq	-696(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	jne	.L277
	movq	-688(%rbp), %rax
	movl	-592(%rbp,%rax), %eax
	testl	%eax, %eax
	jle	.L209
	movq	-688(%rbp), %rcx
	subl	$1, %eax
	movq	-728(%rbp), %rdx
	movq	%r15, %rdi
	subq	-720(%rbp), %rdx
	movl	%eax, -592(%rbp,%rcx)
	cltq
	leaq	4(%rax,%rdx), %rax
	movl	-604(%rbp,%rax,4), %esi
	addl	-600(%rbp,%rcx), %esi
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L210
.L277:
	movl	%r14d, %eax
	movq	%r12, %r14
	movl	%eax, %r12d
	movl	16(%r14), %eax
	movl	%eax, 12(%r14)
	imulq	$180, %rbx, %rax
	movl	-600(%rbp,%rax), %eax
	jmp	.L211
.L261:
	imulq	$180, %rbx, %rax
	movl	%r14d, %r12d
	movl	-600(%rbp,%rax), %eax
	jmp	.L211
.L273:
	call	__stack_chk_fail@PLT
.L236:
	movl	%eax, -688(%rbp)
	jmp	.L215
.L214:
	addl	%r13d, %eax
	movl	%eax, -688(%rbp)
	jmp	.L216
	.cfi_endproc
.LFE3492:
	.size	_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0, .-_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.type	_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E, @function
_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E:
.LFB2527:
	.cfi_startproc
	endbr64
	movl	%ecx, %eax
	subl	%edx, %eax
	cmpl	$3, %eax
	jle	.L279
	jmp	_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0
	.p2align 4,,10
	.p2align 3
.L279:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2527:
	.size	_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E, .-_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.align 2
	.p2align 4
	.type	_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0, @function
_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0:
.LFB3493:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movslq	%edx, %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$696, %rsp
	movq	%rdi, -632(%rbp)
	movdqa	.LC0(%rip), %xmm0
	movq	%r15, %rdi
	movq	%r8, -664(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -608(%rbp)
	movl	$0, -612(%rbp)
	movl	$0, -592(%rbp)
	movl	$0, -412(%rbp)
	movl	$0, -232(%rbp)
	movups	%xmm0, -428(%rbp)
	movups	%xmm0, -248(%rbp)
	call	utext_setNativeIndex_67@PLT
	leaq	-608(%rbp), %rax
	movl	-612(%rbp), %edx
	movl	$0, -636(%rbp)
	movq	%rax, -656(%rbp)
	leaq	408(%rbx), %rax
	movq	%rax, -680(%rbp)
	testl	%edx, %edx
	jg	.L282
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rax, -648(%rbp)
	cmpl	%r12d, %eax
	jge	.L282
	movl	-636(%rbp), %eax
	movl	$2863311531, %edx
	movq	%r15, %rsi
	movq	%rax, %rcx
	imulq	%rdx, %rax
	movl	%ecx, %ebx
	movl	%r12d, %ecx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %ebx
	movq	-656(%rbp), %rax
	imulq	$180, %rbx, %r13
	leaq	(%rax,%r13), %r14
	movq	-632(%rbp), %rax
	movq	%r14, %rdi
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	cmpl	$1, %eax
	je	.L353
	movq	%r15, %rdi
	jg	.L288
	call	utext_getNativeIndex_67@PLT
	xorl	%r13d, %r13d
	cmpl	%eax, %r12d
	jle	.L290
	movq	-632(%rbp), %rax
	movl	%r12d, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	$0, %r13d
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	movl	-648(%rbp), %eax
	movl	%eax, -688(%rbp)
	jle	.L298
.L299:
	movslq	-688(%rbp), %rsi
	movq	%r15, %rdi
	call	utext_setNativeIndex_67@PLT
	.p2align 4,,10
	.p2align 3
.L290:
	movq	-632(%rbp), %rax
	leaq	808(%rax), %r14
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%r15, %rdi
	call	utext_current32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L308
	movq	%r15, %rdi
	call	utext_next32_67@PLT
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	subl	%ebx, %eax
	addl	%eax, %r13d
.L309:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rax, %rbx
	cmpl	%eax, %r12d
	jg	.L305
.L308:
	testl	%r13d, %r13d
	jg	.L354
.L312:
	movl	-612(%rbp), %eax
	testl	%eax, %eax
	jle	.L281
.L282:
	movq	-664(%rbp), %rax
	movl	8(%rax), %eax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L355
	testl	%eax, %eax
	jle	.L313
	movq	-664(%rbp), %rcx
	movslq	%edx, %rax
	movq	24(%rcx), %rcx
	cmpl	(%rcx,%rax,4), %r12d
	jle	.L314
	.p2align 4,,10
	.p2align 3
.L315:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L356
	movl	-636(%rbp), %eax
	addq	$696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movq	-664(%rbp), %rcx
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L310
	cmpl	12(%rcx), %esi
	jle	.L311
.L310:
	movq	-664(%rbp), %rbx
	leaq	-612(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L312
	movslq	8(%rbx), %rax
.L311:
	movq	-664(%rbp), %rcx
	addl	-648(%rbp), %r13d
	movq	24(%rcx), %rdx
	movl	%r13d, (%rdx,%rax,4)
	addl	$1, 8(%rcx)
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L288:
	call	utext_getNativeIndex_67@PLT
	movl	-636(%rbp), %esi
	movq	%rax, %r8
	movslq	%r12d, %rax
	leal	1(%rsi), %edx
	movl	%edx, -636(%rbp)
	cmpq	%rax, %r8
	jl	.L357
	movl	-600(%rbp,%r13), %eax
.L294:
	imulq	$180, %rbx, %r13
	leaq	(%rbx,%rbx,2), %rdx
	movq	%r15, %rdi
	movq	%rdx, %rbx
	salq	$4, %rbx
	movslq	-596(%rbp,%r13), %rcx
	subq	%rdx, %rbx
	leaq	4(%rcx,%rbx), %rdx
	addl	-604(%rbp,%rdx,4), %eax
	movslq	%eax, %rsi
	call	utext_setNativeIndex_67@PLT
	movslq	-596(%rbp,%r13), %rax
	addq	%rax, %rbx
	movl	-588(%rbp,%rbx,4), %r13d
	movl	-508(%rbp,%rbx,4), %ebx
.L287:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	cmpl	$2, %ebx
	jg	.L290
	cmpl	%eax, %r12d
	jle	.L290
	movl	-636(%rbp), %eax
	movl	$3, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	divl	%ecx
	movq	-632(%rbp), %rax
	movl	%r12d, %ecx
	movl	%edx, %ebx
	movq	1008(%rax), %rdx
	imulq	$180, %rbx, %rbx
	addq	-656(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	movl	-648(%rbp), %eax
	jg	.L297
	testl	%r13d, %r13d
	je	.L319
	addl	%r13d, %eax
	cmpl	$2, 4(%rbx)
	movl	%eax, -688(%rbp)
	jg	.L299
.L298:
	movl	-636(%rbp), %eax
	movl	$2863311531, %edx
	movl	%r12d, %r14d
	movl	$0, -672(%rbp)
	movl	%r13d, -704(%rbp)
	subl	-688(%rbp), %r14d
	addl	$1, %eax
	movq	%rax, %rcx
	movl	%eax, -712(%rbp)
	imulq	%rdx, %rax
	movl	%ecx, %edx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %edx
	imulq	$180, %rdx, %rax
	addq	-656(%rbp), %rax
	movq	%rax, -696(%rbp)
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	utext_next32_67@PLT
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	utext_getNativeIndex_67@PLT
	subl	%ebx, %eax
	addl	%eax, -672(%rbp)
	subl	%eax, %r14d
	testl	%r14d, %r14d
	jle	.L352
	movq	%r15, %rdi
	call	utext_current32_67@PLT
	movq	-680(%rbp), %rdi
	movl	%r13d, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L300
	movq	-632(%rbp), %rax
	movl	%ebx, %esi
	leaq	608(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L300
	movq	-632(%rbp), %rax
	movq	-696(%rbp), %rdi
	movq	%r15, %rsi
	movl	%r12d, %ecx
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	movl	-672(%rbp), %esi
	movq	%r15, %rdi
	addl	-688(%rbp), %esi
	movl	%eax, %ebx
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	testl	%ebx, %ebx
	jle	.L300
.L352:
	movl	-704(%rbp), %r13d
	movl	-712(%rbp), %eax
	testl	%r13d, %r13d
	cmovg	-636(%rbp), %eax
	addl	-672(%rbp), %r13d
	movl	%eax, -636(%rbp)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L353:
	leaq	(%rbx,%rbx,2), %rax
	movslq	-596(%rbp,%r13), %rdx
	movl	-600(%rbp,%r13), %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	salq	$4, %rbx
	subq	%rax, %rbx
	leaq	4(%rdx,%rbx), %rax
	addl	-604(%rbp,%rax,4), %esi
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	movslq	-596(%rbp,%r13), %rax
	addl	$1, -636(%rbp)
	addq	%rax, %rbx
	movl	-588(%rbp,%rbx,4), %r13d
	movl	-508(%rbp,%rbx,4), %ebx
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L357:
	movl	%edx, %eax
	movl	$2863311531, %r9d
	movl	%edx, %edi
	movq	-656(%rbp), %r10
	imulq	%r9, %rax
	leal	2(%rsi), %edx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %edi
	imulq	$180, %rdi, %rcx
	movq	%rdi, -704(%rbp)
	leaq	(%r10,%rcx), %rax
	movq	%rcx, -688(%rbp)
	movq	%rax, -672(%rbp)
	movq	%rdx, %rax
	imulq	%r9, %rdx
	shrq	$33, %rdx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %eax
	imulq	$180, %rax, %rax
	addq	%r10, %rax
	movq	%rax, -696(%rbp)
	leaq	(%rdi,%rdi), %rax
	movq	%rax, -712(%rbp)
	leaq	-48(%rbp), %rax
	addq	%rax, %r13
	movl	%r12d, %eax
	movq	%r14, %r12
	movl	%eax, %r14d
.L296:
	movq	-632(%rbp), %rax
	movq	-672(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	jg	.L358
.L292:
	movl	-544(%r13), %edx
	movl	-552(%r13), %eax
	testl	%edx, %edx
	jg	.L359
	movl	%r14d, %r12d
	jmp	.L294
.L355:
	testl	%r12d, %r12d
	jg	.L315
	testl	%eax, %eax
	jle	.L316
.L314:
	movq	-664(%rbp), %rax
	movl	%edx, 8(%rax)
.L316:
	subl	$1, -636(%rbp)
	jmp	.L315
.L313:
	testl	%r12d, %r12d
	jle	.L316
	jmp	.L315
.L359:
	leaq	(%rbx,%rbx,2), %rdi
	subl	$1, %edx
	movq	%rdi, %rsi
	movl	%edx, -544(%r13)
	movslq	%edx, %rdx
	salq	$4, %rsi
	subq	%rdi, %rsi
	movq	%r15, %rdi
	leaq	4(%rdx,%rsi), %rdx
	addl	-604(%rbp,%rdx,4), %eax
	movslq	%eax, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L296
.L358:
	movl	16(%r12), %eax
	movq	%r15, %rdi
	movl	%eax, 12(%r12)
	call	utext_getNativeIndex_67@PLT
	cmpl	%eax, %r14d
	jle	.L344
	movq	-712(%rbp), %rax
	addq	-704(%rbp), %rax
	movq	%rax, -720(%rbp)
	salq	$4, %rax
	movq	%rax, -728(%rbp)
.L293:
	movq	-632(%rbp), %rax
	movq	-696(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	jne	.L360
	movq	-688(%rbp), %rax
	movl	-592(%rbp,%rax), %eax
	testl	%eax, %eax
	jle	.L292
	movq	-688(%rbp), %rcx
	subl	$1, %eax
	movq	-728(%rbp), %rdx
	movq	%r15, %rdi
	subq	-720(%rbp), %rdx
	movl	%eax, -592(%rbp,%rcx)
	cltq
	leaq	4(%rax,%rdx), %rax
	movl	-604(%rbp,%rax,4), %esi
	addl	-600(%rbp,%rcx), %esi
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L293
.L360:
	movl	%r14d, %eax
	movq	%r12, %r14
	movl	%eax, %r12d
	movl	16(%r14), %eax
	movl	%eax, 12(%r14)
	imulq	$180, %rbx, %rax
	movl	-600(%rbp,%rax), %eax
	jmp	.L294
.L344:
	imulq	$180, %rbx, %rax
	movl	%r14d, %r12d
	movl	-600(%rbp,%rax), %eax
	jmp	.L294
.L356:
	call	__stack_chk_fail@PLT
.L319:
	movl	%eax, -688(%rbp)
	jmp	.L298
.L297:
	addl	%r13d, %eax
	movl	%eax, -688(%rbp)
	jmp	.L299
	.cfi_endproc
.LFE3493:
	.size	_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0, .-_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.type	_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E, @function
_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E:
.LFB2535:
	.cfi_startproc
	endbr64
	movl	%ecx, %eax
	subl	%edx, %eax
	cmpl	$3, %eax
	jle	.L362
	jmp	_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0
	.p2align 4,,10
	.p2align 3
.L362:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2535:
	.size	_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E, .-_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.align 2
	.p2align 4
	.type	_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0, @function
_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0:
.LFB3494:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movslq	%edx, %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$696, %rsp
	movq	%rdi, -632(%rbp)
	movdqa	.LC0(%rip), %xmm0
	movq	%r15, %rdi
	movq	%r8, -664(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -608(%rbp)
	movl	$0, -612(%rbp)
	movl	$0, -592(%rbp)
	movl	$0, -412(%rbp)
	movl	$0, -232(%rbp)
	movups	%xmm0, -428(%rbp)
	movups	%xmm0, -248(%rbp)
	call	utext_setNativeIndex_67@PLT
	leaq	-608(%rbp), %rax
	movl	-612(%rbp), %edx
	movl	$0, -636(%rbp)
	movq	%rax, -656(%rbp)
	leaq	408(%rbx), %rax
	movq	%rax, -680(%rbp)
	testl	%edx, %edx
	jg	.L365
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rax, -648(%rbp)
	cmpl	%r12d, %eax
	jge	.L365
	movl	-636(%rbp), %eax
	movl	$2863311531, %edx
	movq	%r15, %rsi
	movq	%rax, %rcx
	imulq	%rdx, %rax
	movl	%ecx, %ebx
	movl	%r12d, %ecx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %ebx
	movq	-656(%rbp), %rax
	imulq	$180, %rbx, %r13
	leaq	(%rax,%r13), %r14
	movq	-632(%rbp), %rax
	movq	%r14, %rdi
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	cmpl	$1, %eax
	je	.L436
	movq	%r15, %rdi
	jg	.L371
	call	utext_getNativeIndex_67@PLT
	xorl	%r13d, %r13d
	cmpl	%eax, %r12d
	jle	.L373
	movq	-632(%rbp), %rax
	movl	%r12d, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	$0, %r13d
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	movl	-648(%rbp), %eax
	movl	%eax, -688(%rbp)
	jle	.L381
.L382:
	movslq	-688(%rbp), %rsi
	movq	%r15, %rdi
	call	utext_setNativeIndex_67@PLT
	.p2align 4,,10
	.p2align 3
.L373:
	movq	-632(%rbp), %rax
	leaq	808(%rax), %r14
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L388:
	movq	%r15, %rdi
	call	utext_current32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L391
	movq	%r15, %rdi
	call	utext_next32_67@PLT
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	subl	%ebx, %eax
	addl	%eax, %r13d
.L392:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rax, %rbx
	cmpl	%eax, %r12d
	jg	.L388
.L391:
	testl	%r13d, %r13d
	jg	.L437
.L395:
	movl	-612(%rbp), %eax
	testl	%eax, %eax
	jle	.L364
.L365:
	movq	-664(%rbp), %rax
	movl	8(%rax), %eax
	movl	%eax, %edx
	subl	$1, %edx
	js	.L438
	testl	%eax, %eax
	jle	.L396
	movq	-664(%rbp), %rcx
	movslq	%edx, %rax
	movq	24(%rcx), %rcx
	cmpl	(%rcx,%rax,4), %r12d
	jle	.L397
	.p2align 4,,10
	.p2align 3
.L398:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L439
	movl	-636(%rbp), %eax
	addq	$696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	movq	-664(%rbp), %rcx
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L393
	cmpl	12(%rcx), %esi
	jle	.L394
.L393:
	movq	-664(%rbp), %rbx
	leaq	-612(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L395
	movslq	8(%rbx), %rax
.L394:
	movq	-664(%rbp), %rcx
	addl	-648(%rbp), %r13d
	movq	24(%rcx), %rdx
	movl	%r13d, (%rdx,%rax,4)
	addl	$1, 8(%rcx)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L371:
	call	utext_getNativeIndex_67@PLT
	movl	-636(%rbp), %esi
	leal	1(%rsi), %edx
	movl	%edx, -636(%rbp)
	cmpl	%eax, %r12d
	jg	.L440
	movl	-600(%rbp,%r13), %eax
.L377:
	imulq	$180, %rbx, %r13
	leaq	(%rbx,%rbx,2), %rdx
	movq	%r15, %rdi
	movq	%rdx, %rbx
	salq	$4, %rbx
	movslq	-596(%rbp,%r13), %rcx
	subq	%rdx, %rbx
	leaq	4(%rcx,%rbx), %rdx
	addl	-604(%rbp,%rdx,4), %eax
	movslq	%eax, %rsi
	call	utext_setNativeIndex_67@PLT
	movslq	-596(%rbp,%r13), %rax
	addq	%rax, %rbx
	movl	-588(%rbp,%rbx,4), %r13d
	movl	-508(%rbp,%rbx,4), %ebx
.L370:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	cmpl	$2, %ebx
	jg	.L373
	cmpl	%eax, %r12d
	jle	.L373
	movl	-636(%rbp), %eax
	movl	$3, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	divl	%ecx
	movq	-632(%rbp), %rax
	movl	%r12d, %ecx
	movl	%edx, %ebx
	movq	1008(%rax), %rdx
	imulq	$180, %rbx, %rbx
	addq	-656(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	movl	-648(%rbp), %eax
	jg	.L380
	testl	%r13d, %r13d
	je	.L402
	addl	%r13d, %eax
	cmpl	$2, 4(%rbx)
	movl	%eax, -688(%rbp)
	jg	.L382
.L381:
	movl	-636(%rbp), %eax
	movl	$2863311531, %edx
	movl	%r12d, %r14d
	movl	$0, -672(%rbp)
	movl	%r13d, -704(%rbp)
	subl	-688(%rbp), %r14d
	addl	$1, %eax
	movq	%rax, %rcx
	movl	%eax, -712(%rbp)
	imulq	%rdx, %rax
	movl	%ecx, %edx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %edx
	imulq	$180, %rdx, %rax
	addq	-656(%rbp), %rax
	movq	%rax, -696(%rbp)
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%r15, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	utext_next32_67@PLT
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	utext_getNativeIndex_67@PLT
	subl	%ebx, %eax
	addl	%eax, -672(%rbp)
	subl	%eax, %r14d
	testl	%r14d, %r14d
	jle	.L435
	movq	%r15, %rdi
	call	utext_current32_67@PLT
	movq	-680(%rbp), %rdi
	movl	%r13d, %esi
	movl	%eax, %ebx
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L383
	movq	-632(%rbp), %rax
	movl	%ebx, %esi
	leaq	608(%rax), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L383
	movq	-632(%rbp), %rax
	movq	-696(%rbp), %rdi
	movq	%r15, %rsi
	movl	%r12d, %ecx
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	movl	-672(%rbp), %esi
	movq	%r15, %rdi
	addl	-688(%rbp), %esi
	movl	%eax, %ebx
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	testl	%ebx, %ebx
	jle	.L383
.L435:
	movl	-704(%rbp), %r13d
	movl	-712(%rbp), %eax
	testl	%r13d, %r13d
	cmovg	-636(%rbp), %eax
	addl	-672(%rbp), %r13d
	movl	%eax, -636(%rbp)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	(%rbx,%rbx,2), %rax
	movslq	-596(%rbp,%r13), %rdx
	movl	-600(%rbp,%r13), %esi
	movq	%r15, %rdi
	movq	%rax, %rbx
	salq	$4, %rbx
	subq	%rax, %rbx
	leaq	4(%rdx,%rbx), %rax
	addl	-604(%rbp,%rax,4), %esi
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	movslq	-596(%rbp,%r13), %rax
	addl	$1, -636(%rbp)
	addq	%rax, %rbx
	movl	-588(%rbp,%rbx,4), %r13d
	movl	-508(%rbp,%rbx,4), %ebx
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L440:
	movl	%edx, %eax
	movl	$2863311531, %r9d
	movl	%edx, %edi
	movq	-656(%rbp), %r10
	imulq	%r9, %rax
	leal	2(%rsi), %edx
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %edi
	imulq	$180, %rdi, %rcx
	movq	%rdi, -704(%rbp)
	leaq	(%r10,%rcx), %rax
	movq	%rcx, -688(%rbp)
	movq	%rax, -672(%rbp)
	movq	%rdx, %rax
	imulq	%r9, %rdx
	shrq	$33, %rdx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %eax
	imulq	$180, %rax, %rax
	addq	%r10, %rax
	movq	%rax, -696(%rbp)
	leaq	(%rdi,%rdi), %rax
	movq	%rax, -712(%rbp)
	leaq	-48(%rbp), %rax
	addq	%rax, %r13
	movl	%r12d, %eax
	movq	%r14, %r12
	movl	%eax, %r14d
.L379:
	movq	-632(%rbp), %rax
	movq	-672(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	jg	.L441
.L375:
	movl	-544(%r13), %edx
	movl	-552(%r13), %eax
	testl	%edx, %edx
	jg	.L442
	movl	%r14d, %r12d
	jmp	.L377
.L438:
	testl	%r12d, %r12d
	jg	.L398
	testl	%eax, %eax
	jle	.L399
.L397:
	movq	-664(%rbp), %rax
	movl	%edx, 8(%rax)
.L399:
	subl	$1, -636(%rbp)
	jmp	.L398
.L396:
	testl	%r12d, %r12d
	jle	.L399
	jmp	.L398
.L442:
	leaq	(%rbx,%rbx,2), %rdi
	subl	$1, %edx
	movq	%rdi, %rsi
	movl	%edx, -544(%r13)
	movslq	%edx, %rdx
	salq	$4, %rsi
	subq	%rdi, %rsi
	movq	%r15, %rdi
	leaq	4(%rdx,%rsi), %rdx
	addl	-604(%rbp,%rdx,4), %eax
	movslq	%eax, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L379
.L441:
	movl	16(%r12), %eax
	movq	%r15, %rdi
	movl	%eax, 12(%r12)
	call	utext_getNativeIndex_67@PLT
	cmpl	%eax, %r14d
	jle	.L427
	movq	-712(%rbp), %rax
	addq	-704(%rbp), %rax
	movq	%rax, -720(%rbp)
	salq	$4, %rax
	movq	%rax, -728(%rbp)
.L376:
	movq	-632(%rbp), %rax
	movq	-696(%rbp), %rdi
	movl	%r14d, %ecx
	movq	%r15, %rsi
	movq	1008(%rax), %rdx
	call	_ZN6icu_6712PossibleWord10candidatesEP5UTextPNS_17DictionaryMatcherEi
	testl	%eax, %eax
	jne	.L443
	movq	-688(%rbp), %rax
	movl	-592(%rbp,%rax), %eax
	testl	%eax, %eax
	jle	.L375
	movq	-688(%rbp), %rcx
	subl	$1, %eax
	movq	-728(%rbp), %rdx
	movq	%r15, %rdi
	subq	-720(%rbp), %rdx
	movl	%eax, -592(%rbp,%rcx)
	cltq
	leaq	4(%rax,%rdx), %rax
	movl	-604(%rbp,%rax,4), %esi
	addl	-600(%rbp,%rcx), %esi
	movslq	%esi, %rsi
	call	utext_setNativeIndex_67@PLT
	jmp	.L376
.L443:
	movl	%r14d, %eax
	movq	%r12, %r14
	movl	%eax, %r12d
	movl	16(%r14), %eax
	movl	%eax, 12(%r14)
	imulq	$180, %rbx, %rax
	movl	-600(%rbp,%rax), %eax
	jmp	.L377
.L427:
	imulq	$180, %rbx, %rax
	movl	%r14d, %r12d
	movl	-600(%rbp,%rax), %eax
	jmp	.L377
.L439:
	call	__stack_chk_fail@PLT
.L402:
	movl	%eax, -688(%rbp)
	jmp	.L381
.L380:
	addl	%r13d, %eax
	movl	%eax, -688(%rbp)
	jmp	.L382
	.cfi_endproc
.LFE3494:
	.size	_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0, .-_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.type	_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E, @function
_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E:
.LFB2543:
	.cfi_startproc
	endbr64
	movl	%ecx, %eax
	subl	%edx, %eax
	cmpl	$3, %eax
	jle	.L445
	jmp	_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0
	.p2align 4,,10
	.p2align 3
.L445:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2543:
	.size	_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E, .-_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE
	.type	_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE, @function
_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE:
.LFB2499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet7compactEv@PLT
	.cfi_endproc
.LFE2499:
	.size	_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE, .-_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC1:
	.string	"["
	.string	"["
	.string	":"
	.string	"T"
	.string	"h"
	.string	"a"
	.string	"i"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"L"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"B"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"k"
	.string	"="
	.string	"S"
	.string	"A"
	.string	":"
	.string	"]"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC2:
	.string	"["
	.string	"["
	.string	":"
	.string	"T"
	.string	"h"
	.string	"a"
	.string	"i"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"L"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"B"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"k"
	.string	"="
	.string	"S"
	.string	"A"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"M"
	.string	":"
	.string	"]"
	.string	"]"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715ThaiBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.type	_ZN6icu_6715ThaiBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode, @function
_ZN6icu_6715ThaiBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode:
.LFB2513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	408(%rbx), %r12
	leaq	608(%rbx), %r14
	leaq	808(%rbx), %r13
	leaq	1008(%rbx), %r15
	subq	$136, %rsp
	movq	%rsi, -168(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN6icu_6719LanguageBreakEngineC2Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rcx
	leaq	8(%rbx), %r9
	movq	%rcx, (%rbx)
	movq	%r9, %rdi
	movq	%r9, -176(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	16+_ZTVN6icu_6715ThaiBreakEngineE(%rip), %rcx
	leaq	208(%rbx), %rax
	movq	%rcx, (%rbx)
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	-168(%rbp), %rsi
	leaq	-136(%rbp), %r8
	leaq	.LC1(%rip), %rcx
	movq	%r8, %rdx
	movq	%rcx, -136(%rbp)
	movl	$-1, %ecx
	movq	%rsi, 1208(%rbx)
	leaq	-128(%rbp), %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %r9
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L452
.L449:
	movq	%r8, %rdx
	movl	$-1, %ecx
	movq	%rbx, %rdi
	movl	$1, %esi
	leaq	.LC2(%rip), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movl	$3633, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6removeEi@PLT
	movl	$3652, %edx
	movl	$3648, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6removeEii@PLT
	movl	$3630, %edx
	movl	$3585, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$3652, %edx
	movl	$3648, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$3631, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$3654, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L453
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	movq	-160(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	-168(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	-176(%rbp), %r8
	jmp	.L449
.L453:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2513:
	.size	_ZN6icu_6715ThaiBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode, .-_ZN6icu_6715ThaiBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.globl	_ZN6icu_6715ThaiBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode
	.set	_ZN6icu_6715ThaiBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode,_ZN6icu_6715ThaiBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.section	.rodata.str2.8
	.align 8
.LC3:
	.string	"["
	.string	"["
	.string	":"
	.string	"L"
	.string	"a"
	.string	"o"
	.string	"o"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"L"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"B"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"k"
	.string	"="
	.string	"S"
	.string	"A"
	.string	":"
	.string	"]"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC4:
	.string	"["
	.string	"["
	.string	":"
	.string	"L"
	.string	"a"
	.string	"o"
	.string	"o"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"L"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"B"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"k"
	.string	"="
	.string	"S"
	.string	"A"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"M"
	.string	":"
	.string	"]"
	.string	"]"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714LaoBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.type	_ZN6icu_6714LaoBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode, @function
_ZN6icu_6714LaoBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode:
.LFB2521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	208(%rbx), %r15
	leaq	408(%rbx), %r13
	leaq	608(%rbx), %r12
	leaq	808(%rbx), %r14
	subq	$136, %rsp
	movq	%rsi, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN6icu_6719LanguageBreakEngineC2Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rcx
	leaq	8(%rbx), %r9
	movq	%rcx, (%rbx)
	movq	%r9, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	16+_ZTVN6icu_6714LaoBreakEngineE(%rip), %rcx
	movq	%r15, %rdi
	movq	%rcx, (%rbx)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	-160(%rbp), %rsi
	leaq	-136(%rbp), %r8
	leaq	.LC3(%rip), %rcx
	movq	%r8, %rdx
	movq	%rcx, -136(%rbp)
	movl	$-1, %ecx
	movq	%rsi, 1008(%rbx)
	leaq	-128(%rbp), %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %r9
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L458
.L455:
	movq	%r8, %rdx
	movl	$-1, %ecx
	movq	%rbx, %rdi
	movl	$1, %esi
	leaq	.LC4(%rip), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movl	$3780, %edx
	movl	$3776, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6removeEii@PLT
	movl	$3758, %edx
	movl	$3713, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$3805, %edx
	movl	$3804, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$3780, %edx
	movl	$3776, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L459
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movq	%r9, %rdi
	movq	%r15, %rsi
	movq	%r8, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	-160(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	-168(%rbp), %r8
	jmp	.L455
.L459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2521:
	.size	_ZN6icu_6714LaoBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode, .-_ZN6icu_6714LaoBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.globl	_ZN6icu_6714LaoBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode
	.set	_ZN6icu_6714LaoBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode,_ZN6icu_6714LaoBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.section	.rodata.str2.8
	.align 8
.LC5:
	.string	"["
	.string	"["
	.string	":"
	.string	"M"
	.string	"y"
	.string	"m"
	.string	"r"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"L"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"B"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"k"
	.string	"="
	.string	"S"
	.string	"A"
	.string	":"
	.string	"]"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC6:
	.string	"["
	.string	"["
	.string	":"
	.string	"M"
	.string	"y"
	.string	"m"
	.string	"r"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"L"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"B"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"k"
	.string	"="
	.string	"S"
	.string	"A"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"M"
	.string	":"
	.string	"]"
	.string	"]"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718BurmeseBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.type	_ZN6icu_6718BurmeseBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode, @function
_ZN6icu_6718BurmeseBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode:
.LFB2529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	208(%rbx), %r13
	leaq	408(%rbx), %r15
	leaq	608(%rbx), %r14
	leaq	808(%rbx), %r12
	subq	$136, %rsp
	movq	%rsi, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN6icu_6719LanguageBreakEngineC2Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rcx
	leaq	8(%rbx), %r9
	movq	%rcx, (%rbx)
	movq	%r9, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	16+_ZTVN6icu_6718BurmeseBreakEngineE(%rip), %rcx
	movq	%r13, %rdi
	movq	%rcx, (%rbx)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	-160(%rbp), %rsi
	leaq	-136(%rbp), %r8
	leaq	.LC5(%rip), %rcx
	movq	%r8, %rdx
	movq	%rcx, -136(%rbp)
	movl	$-1, %ecx
	movq	%rsi, 1008(%rbx)
	leaq	-128(%rbp), %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %r9
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L464
.L461:
	movq	%r8, %rdx
	movl	$-1, %ecx
	movq	%rbx, %rdi
	movl	$1, %esi
	leaq	.LC6(%rip), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movl	$4138, %edx
	movl	$4096, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L465
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movq	%r9, %rdi
	movq	%r13, %rsi
	movq	%r8, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	-160(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	-168(%rbp), %r8
	jmp	.L461
.L465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2529:
	.size	_ZN6icu_6718BurmeseBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode, .-_ZN6icu_6718BurmeseBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.globl	_ZN6icu_6718BurmeseBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode
	.set	_ZN6icu_6718BurmeseBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode,_ZN6icu_6718BurmeseBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.section	.rodata.str2.8
	.align 8
.LC7:
	.string	"["
	.string	"["
	.string	":"
	.string	"K"
	.string	"h"
	.string	"m"
	.string	"r"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"L"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"B"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"k"
	.string	"="
	.string	"S"
	.string	"A"
	.string	":"
	.string	"]"
	.string	"]"
	.string	""
	.string	""
	.align 8
.LC8:
	.string	"["
	.string	"["
	.string	":"
	.string	"K"
	.string	"h"
	.string	"m"
	.string	"r"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"L"
	.string	"i"
	.string	"n"
	.string	"e"
	.string	"B"
	.string	"r"
	.string	"e"
	.string	"a"
	.string	"k"
	.string	"="
	.string	"S"
	.string	"A"
	.string	":"
	.string	"]"
	.string	"&"
	.string	"["
	.string	":"
	.string	"M"
	.string	":"
	.string	"]"
	.string	"]"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716KhmerBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.type	_ZN6icu_6716KhmerBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode, @function
_ZN6icu_6716KhmerBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode:
.LFB2537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	208(%rbx), %r14
	leaq	408(%rbx), %r12
	leaq	608(%rbx), %r15
	leaq	808(%rbx), %r13
	subq	$136, %rsp
	movq	%rsi, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN6icu_6719LanguageBreakEngineC2Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rcx
	leaq	8(%rbx), %r9
	movq	%rcx, (%rbx)
	movq	%r9, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	16+_ZTVN6icu_6716KhmerBreakEngineE(%rip), %rcx
	movq	%r14, %rdi
	movq	%rcx, (%rbx)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	-160(%rbp), %rsi
	leaq	-136(%rbp), %r8
	leaq	.LC7(%rip), %rcx
	movq	%r8, %rdx
	movq	%rcx, -136(%rbp)
	movl	$-1, %ecx
	movq	%rsi, 1008(%rbx)
	leaq	-128(%rbp), %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %r8
	movq	-168(%rbp), %r9
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L470
.L467:
	movq	%r8, %rdx
	movl	$-1, %ecx
	movq	%rbx, %rdi
	movl	$1, %esi
	leaq	.LC8(%rip), %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-152(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$32, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movl	$6067, %edx
	movl	$6016, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$6098, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6removeEi@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L471
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movq	%r9, %rdi
	movq	%r14, %rsi
	movq	%r8, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	-160(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	-168(%rbp), %r8
	jmp	.L467
.L471:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2537:
	.size	_ZN6icu_6716KhmerBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode, .-_ZN6icu_6716KhmerBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.globl	_ZN6icu_6716KhmerBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode
	.set	_ZN6icu_6716KhmerBreakEngineC1EPNS_17DictionaryMatcherER10UErrorCode,_ZN6icu_6716KhmerBreakEngineC2EPNS_17DictionaryMatcherER10UErrorCode
	.section	.rodata.str2.8
	.align 8
.LC9:
	.string	"["
	.string	"\\"
	.string	"u"
	.string	"a"
	.string	"c"
	.string	"0"
	.string	"0"
	.string	"-"
	.string	"\\"
	.string	"u"
	.string	"d"
	.string	"7"
	.string	"a"
	.string	"3"
	.string	"]"
	.string	""
	.string	""
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC10:
	.string	"["
	.string	":"
	.string	"H"
	.string	"a"
	.string	"n"
	.string	":"
	.string	"]"
	.string	""
	.string	""
	.section	.rodata.str2.8
	.align 8
.LC11:
	.string	"["
	.string	"["
	.string	":"
	.string	"K"
	.string	"a"
	.string	"t"
	.string	"a"
	.string	"k"
	.string	"a"
	.string	"n"
	.string	"a"
	.string	":"
	.string	"]"
	.string	"\\"
	.string	"u"
	.string	"f"
	.string	"f"
	.string	"9"
	.string	"e"
	.string	"\\"
	.string	"u"
	.string	"f"
	.string	"f"
	.string	"9"
	.string	"f"
	.string	"]"
	.string	""
	.string	""
	.section	.rodata.str2.2
	.align 2
.LC12:
	.string	"["
	.string	":"
	.string	"H"
	.string	"i"
	.string	"r"
	.string	"a"
	.string	"g"
	.string	"a"
	.string	"n"
	.string	"a"
	.string	":"
	.string	"]"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714CjkBreakEngineC2EPNS_17DictionaryMatcherENS_12LanguageTypeER10UErrorCode
	.type	_ZN6icu_6714CjkBreakEngineC2EPNS_17DictionaryMatcherENS_12LanguageTypeER10UErrorCode, @function
_ZN6icu_6714CjkBreakEngineC2EPNS_17DictionaryMatcherENS_12LanguageTypeER10UErrorCode:
.LFB2545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-264(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	808(%rbx), %r15
	subq	$280, %rsp
	movl	%edx, -300(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN6icu_6719LanguageBreakEngineC2Ev@PLT
	leaq	16+_ZTVN6icu_6721DictionaryBreakEngineE(%rip), %rax
	leaq	8(%rbx), %r11
	movq	%rax, (%rbx)
	movq	%r11, %rdi
	movq	%r11, -312(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	16+_ZTVN6icu_6714CjkBreakEngineE(%rip), %rax
	leaq	208(%rbx), %r10
	movq	%rax, (%rbx)
	movq	%r10, %rdi
	movq	%r10, -296(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	408(%rbx), %r9
	movq	%r9, %rdi
	movq	%r9, -288(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	608(%rbx), %r8
	movq	%r8, %rdi
	movq	%r8, -280(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, 1008(%rbx)
	movl	$-1, %ecx
	movq	%r14, %rdx
	leaq	-256(%rbp), %r12
	leaq	.LC9(%rip), %rax
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-296(%rbp), %r10
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC10(%rip), %rax
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-288(%rbp), %r9
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC11(%rip), %rax
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-280(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	$-1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	leaq	.LC12(%rip), %rax
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6711Normalizer215getNFKCInstanceER10UErrorCode@PLT
	movl	0(%r13), %edx
	movq	%rax, 1016(%rbx)
	testl	%edx, %edx
	jle	.L477
.L472:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L478
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	movl	-300(%rbp), %eax
	movq	-280(%rbp), %r8
	movq	-288(%rbp), %r9
	movq	-312(%rbp), %r11
	testl	%eax, %eax
	jne	.L474
	movq	-296(%rbp), %r10
	movq	%r11, %rdi
	movq	%r11, -280(%rbp)
	movq	%r10, %rsi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	-280(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L474:
	movq	%r12, %rdi
	movq	%r11, -296(%rbp)
	movq	%r8, -288(%rbp)
	movq	%r9, -280(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	-280(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	-288(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movq	%r12, %rdi
	movl	$65392, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	%r12, %rdi
	movl	$12540, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	-296(%rbp), %r11
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -280(%rbp)
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movq	-280(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	jmp	.L472
.L478:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2545:
	.size	_ZN6icu_6714CjkBreakEngineC2EPNS_17DictionaryMatcherENS_12LanguageTypeER10UErrorCode, .-_ZN6icu_6714CjkBreakEngineC2EPNS_17DictionaryMatcherENS_12LanguageTypeER10UErrorCode
	.globl	_ZN6icu_6714CjkBreakEngineC1EPNS_17DictionaryMatcherENS_12LanguageTypeER10UErrorCode
	.set	_ZN6icu_6714CjkBreakEngineC1EPNS_17DictionaryMatcherENS_12LanguageTypeER10UErrorCode,_ZN6icu_6714CjkBreakEngineC2EPNS_17DictionaryMatcherENS_12LanguageTypeER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0, @function
_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0:
.LFB3495:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movslq	%edx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%ecx, %rbx
	subq	$744, %rsp
	movq	%rdi, -680(%rbp)
	movl	%edx, -660(%rbp)
	movl	%ecx, -656(%rbp)
	movq	%r8, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$0, -628(%rbp)
	movq	%rax, -320(%rbp)
	movl	$2, %eax
	movw	%ax, -312(%rbp)
	testb	$4, 8(%r12)
	je	.L480
	movq	32(%r12), %rax
	cmpq	%rsi, %rax
	jle	.L740
.L480:
	movq	%r12, %rdi
	call	utext_setNativeIndex_67@PLT
	movq	%r12, %rdi
	call	utext_nativeLength_67@PLT
	cmpq	%rbx, %rax
	jl	.L741
.L482:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L483
	leaq	-628(%rbp), %r13
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movl	-628(%rbp), %eax
	testl	%eax, %eax
	jle	.L629
	movq	%r15, %rdi
	leaq	-320(%rbp), %r14
	xorl	%r15d, %r15d
	call	_ZN6icu_679UVector32D0Ev@PLT
	movl	-628(%rbp), %eax
	testl	%eax, %eax
	jle	.L629
.L618:
	xorl	%r12d, %r12d
.L616:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L742
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L629:
	.cfi_restore_state
	movslq	-656(%rbp), %rax
	leaq	-320(%rbp), %r14
	movq	%rax, -648(%rbp)
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	cmpq	-648(%rbp), %rax
	jge	.L485
	movq	%r12, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	utext_next32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	8(%r15), %eax
.L486:
	movswl	-312(%rbp), %edx
	testw	%dx, %dx
	js	.L487
.L743:
	sarl	$5, %edx
	cmpl	%eax, %edx
	jle	.L695
.L488:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L490
	cmpl	12(%r15), %esi
	jle	.L491
.L490:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	movl	8(%r15), %eax
	je	.L486
.L491:
	movq	24(%r15), %rdx
	cltq
	movl	%ebx, (%rdx,%rax,4)
	movl	8(%r15), %eax
	movswl	-312(%rbp), %edx
	addl	$1, %eax
	movl	%eax, 8(%r15)
	testw	%dx, %dx
	jns	.L743
.L487:
	cmpl	%eax, -308(%rbp)
	jg	.L488
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L485:
	movslq	8(%r15), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L494
	cmpl	12(%r15), %esi
	jle	.L495
.L494:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L481
	movslq	8(%r15), %rax
.L495:
	movq	24(%r15), %rdx
	movl	-656(%rbp), %ebx
	movl	%ebx, (%rdx,%rax,4)
	addl	$1, 8(%r15)
.L481:
	movq	-680(%rbp), %rax
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	1016(%rax), %rdi
	movq	(%rdi), %rax
	call	*88(%rax)
	testb	%al, %al
	jne	.L496
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$32, %edi
	movq	%rax, -256(%rbp)
	movl	$2, %eax
	movw	%ax, -248(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L497
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movl	-628(%rbp), %eax
	testl	%eax, %eax
	jg	.L744
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ebx
	movq	%r13, -656(%rbp)
	movq	-680(%rbp), %r13
	movq	%rax, -192(%rbp)
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, -704(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -696(%rbp)
	movswl	-312(%rbp), %eax
	movw	%bx, -184(%rbp)
	leaq	-192(%rbp), %rbx
	movl	$0, -648(%rbp)
	movq	%rbx, -712(%rbp)
	movq	%r15, -688(%rbp)
	testw	%ax, %ax
	js	.L503
	.p2align 4,,10
	.p2align 3
.L750:
	movl	-648(%rbp), %r15d
	sarl	$5, %eax
	cmpl	%eax, %r15d
	jge	.L505
.L751:
	movzwl	-184(%rbp), %edx
	movl	$2, %edi
	movl	%r15d, %esi
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	cmovne	%edi, %eax
	movq	%r14, %rdi
	movw	%ax, -184(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	%r12, -672(%rbp)
	movl	%eax, %r12d
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L747:
	sarl	$5, %eax
	cmpl	%eax, %r15d
	je	.L745
.L509:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	1016(%r13), %rdi
	movl	%eax, %r12d
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*120(%rax)
	testb	%al, %al
	jne	.L746
.L513:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	%r15d, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, %r15d
	movswl	-312(%rbp), %eax
	testw	%ax, %ax
	jns	.L747
	movl	-308(%rbp), %eax
	cmpl	%eax, %r15d
	jne	.L509
.L745:
	movq	-672(%rbp), %r12
	movl	%eax, %r8d
.L512:
	movq	1016(%r13), %rdi
	movq	-696(%rbp), %r15
	movq	%rbx, %rsi
	movl	%r8d, -672(%rbp)
	movq	-656(%rbp), %rcx
	movq	(%rdi), %rax
	movq	%r15, %rdx
	call	*24(%rax)
	movswl	-120(%rbp), %ecx
	movq	-704(%rbp), %rdi
	movq	%r15, %rsi
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-116(%rbp), %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	-660(%rbp), %r15d
	addl	-648(%rbp), %r15d
	cmpq	$0, -688(%rbp)
	movl	-672(%rbp), %r8d
	je	.L725
	movl	-648(%rbp), %ecx
	xorl	%r15d, %r15d
	testl	%ecx, %ecx
	js	.L725
	movq	-688(%rbp), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L725
	subl	%ecx, %eax
	testl	%eax, %eax
	jle	.L725
	movq	24(%rdi), %rax
	movslq	%ecx, %rdx
	movq	%rbx, -672(%rbp)
	movl	%r8d, -648(%rbp)
	movl	(%rax,%rdx,4), %r15d
	movl	%r15d, %ebx
	movq	-656(%rbp), %r15
	jmp	.L523
.L740:
	cmpq	%rbx, 16(%r12)
	jl	.L480
	movq	%rbx, %rdx
	movslq	28(%r12), %rcx
	subq	%rax, %rdx
	cmpq	%rdx, %rcx
	jl	.L480
	subq	%rax, %rsi
	movq	48(%r12), %rax
	leaq	-320(%rbp), %r14
	movl	-656(%rbp), %ecx
	leaq	-464(%rbp), %rdx
	subl	-660(%rbp), %ecx
	movq	%r14, %rdi
	leaq	(%rax,%rsi,2), %rax
	xorl	%esi, %esi
	movq	%rax, -464(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	xorl	%r15d, %r15d
	leaq	-628(%rbp), %r13
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L725:
	movq	%rbx, -672(%rbp)
	movl	%r15d, %ebx
	movq	-656(%rbp), %r15
	movl	%r8d, -648(%rbp)
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L748:
	sarl	$5, %edx
	cmpl	%edx, %eax
	jge	.L726
.L749:
	movl	%eax, %esi
	addl	$1, %esi
	js	.L520
	cmpl	12(%r12), %esi
	jle	.L521
.L520:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L522
	movslq	8(%r12), %rax
.L521:
	movq	24(%r12), %rdx
	movl	%ebx, (%rdx,%rax,4)
	addl	$1, 8(%r12)
.L522:
	movl	-628(%rbp), %r11d
	testl	%r11d, %r11d
	jg	.L726
.L523:
	movswl	-248(%rbp), %edx
	movslq	8(%r12), %rax
	testw	%dx, %dx
	jns	.L748
	movl	-244(%rbp), %edx
	cmpl	%edx, %eax
	jl	.L749
.L726:
	movl	-648(%rbp), %r8d
	movswl	-312(%rbp), %eax
	movq	-672(%rbp), %rbx
	movl	%r8d, -648(%rbp)
	testw	%ax, %ax
	jns	.L750
.L503:
	movl	-308(%rbp), %eax
	movl	-648(%rbp), %r15d
	cmpl	%eax, %r15d
	jl	.L751
.L505:
	movl	-660(%rbp), %ebx
	movq	-688(%rbp), %r15
	movq	-656(%rbp), %r13
	addl	%eax, %ebx
	testq	%r15, %r15
	je	.L525
	xorl	%ebx, %ebx
	testl	%eax, %eax
	js	.L525
	movl	8(%r15), %edx
	testl	%edx, %edx
	jle	.L525
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L525
	movq	24(%r15), %rdx
	cltq
	movl	(%rdx,%rax,4), %ebx
.L525:
	movslq	8(%r12), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L526
	cmpl	12(%r12), %esi
	jle	.L527
.L526:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L528
	movslq	8(%r12), %rax
.L527:
	movq	24(%r12), %rdx
	movl	%ebx, (%rdx,%rax,4)
	addl	$1, 8(%r12)
.L528:
	testq	%r15, %r15
	je	.L529
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
.L529:
	movq	-704(%rbp), %rbx
	movq	%r14, %rdi
	movq	%r12, %r15
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	-696(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-712(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L496:
	movl	$2147483647, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	movl	%eax, %r12d
	movswl	-312(%rbp), %eax
	testw	%ax, %ax
	js	.L530
	sarl	$5, %eax
.L531:
	cmpl	%eax, %r12d
	je	.L639
	testq	%r15, %r15
	je	.L533
	movq	%r15, -736(%rbp)
.L536:
	xorl	%r8d, %r8d
	movl	%r12d, -656(%rbp)
	xorl	%ebx, %ebx
	movq	-736(%rbp), %r12
	movq	%r13, -648(%rbp)
	movl	%r8d, %r13d
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L752:
	sarl	$5, %eax
	cmpl	%eax, %r13d
	je	.L727
.L544:
	movl	%r13d, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, %r13d
.L534:
	testq	%r15, %r15
	je	.L538
	xorl	%esi, %esi
	testl	%r13d, %r13d
	js	.L539
	movl	8(%r12), %eax
	testl	%eax, %eax
	jle	.L539
	subl	%r13d, %eax
	testl	%eax, %eax
	jle	.L539
	movq	24(%r12), %rdx
	movslq	%r13d, %rax
	movl	(%rdx,%rax,4), %esi
.L539:
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
.L540:
	movswl	-312(%rbp), %eax
	addl	$1, %ebx
	testw	%ax, %ax
	jns	.L752
	cmpl	-308(%rbp), %r13d
	jne	.L544
.L727:
	movl	-656(%rbp), %r12d
	movq	-648(%rbp), %r13
.L532:
	leaq	-624(%rbp), %rax
	leal	1(%r12), %esi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movl	%esi, -748(%rbp)
	movq	%rax, -648(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movslq	-616(%rbp), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L545
	cmpl	-612(%rbp), %esi
	jle	.L546
.L545:
	movq	-648(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L753
.L547:
	testl	%r12d, %r12d
	jle	.L548
	movl	$1, %ebx
	movl	%r12d, -656(%rbp)
	movl	-748(%rbp), %r15d
	movl	%ebx, %r12d
	movq	-648(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L553:
	movslq	-616(%rbp), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L549
	cmpl	-612(%rbp), %esi
	jle	.L550
.L549:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L551
	movslq	-616(%rbp), %rax
.L550:
	movq	-600(%rbp), %rdx
	movl	$-1, (%rdx,%rax,4)
	addl	$1, -616(%rbp)
.L551:
	addl	$1, %r12d
	cmpl	%r15d, %r12d
	jne	.L553
	leaq	-592(%rbp), %rax
	movl	-748(%rbp), %esi
	movq	%r13, %rdx
	movl	-656(%rbp), %r12d
	movq	%rax, %rdi
	movq	%rax, -720(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
.L628:
	movq	-720(%rbp), %r15
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L559:
	movslq	-584(%rbp), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L555
	cmpl	-580(%rbp), %esi
	jle	.L556
.L555:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L557
	movslq	-584(%rbp), %rax
.L556:
	movq	-568(%rbp), %rdx
	movl	$-1, (%rdx,%rax,4)
	addl	$1, -584(%rbp)
.L557:
	addl	$1, %ebx
	cmpl	%ebx, %r12d
	jge	.L559
	leaq	-560(%rbp), %rax
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -768(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movq	%rbx, %rdi
	movl	%r12d, %esi
	call	_ZN6icu_679UVector327setSizeEi@PLT
	leaq	-528(%rbp), %rax
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -776(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector327setSizeEi@PLT
	xorl	%eax, %eax
	movl	$18, %ecx
	movq	%r13, %rdx
	leaq	-464(%rbp), %rdi
	movq	%r14, %rsi
	rep stosq
	leaq	-464(%rbp), %rax
	movl	$878368812, -464(%rbp)
	movq	%rax, %rdi
	movq	%rax, -728(%rbp)
	movl	$144, -452(%rbp)
	call	utext_openUnicodeString_67@PLT
	testl	%r12d, %r12d
	jle	.L754
	leal	-1(%r12), %eax
	xorl	%ecx, %ecx
	movl	%r12d, -664(%rbp)
	xorl	%ebx, %ebx
	movq	%rax, -672(%rbp)
	movq	-680(%rbp), %rax
	movl	%ecx, %r12d
	movb	$0, -688(%rbp)
	addq	$208, %rax
	movq	%r13, -784(%rbp)
	movq	%rax, -760(%rbp)
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L755:
	subl	%ebx, %eax
	testl	%eax, %eax
	jle	.L560
	movq	-600(%rbp), %rax
	cmpl	$-1, (%rax,%rbx,4)
	jne	.L560
.L561:
	movl	%r12d, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, %r12d
	leaq	1(%rbx), %rax
	cmpq	-672(%rbp), %rbx
	je	.L584
	movq	%rax, %rbx
.L585:
	movl	-616(%rbp), %eax
	movl	%ebx, %r15d
	testl	%eax, %eax
	jg	.L755
.L560:
	movq	-728(%rbp), %r13
	movslq	%r12d, %rsi
	movq	%r13, %rdi
	call	utext_setNativeIndex_67@PLT
	xorl	%r8d, %r8d
	movl	$20, %edx
	movq	%r13, %rsi
	movq	-680(%rbp), %rax
	movq	-504(%rbp), %r9
	movl	-664(%rbp), %ecx
	movq	1008(%rax), %rdi
	movq	(%rdi), %rax
	pushq	$0
	pushq	-536(%rbp)
	call	*16(%rax)
	popq	%r8
	movl	%eax, %edx
	popq	%r9
	testl	%eax, %eax
	je	.L562
	movl	-520(%rbp), %edi
	testl	%edi, %edi
	jle	.L563
	movq	-504(%rbp), %rax
	cmpl	$1, (%rax)
	jne	.L563
.L564:
	testl	%edx, %edx
	jle	.L572
	leaq	0(,%rbx,4), %rax
	leal	-1(%rdx), %r8d
	xorl	%r13d, %r13d
	movl	%r12d, -696(%rbp)
	movq	%rbx, -704(%rbp)
	movq	%r13, %r12
	movq	-720(%rbp), %rbx
	movq	%r14, -712(%rbp)
	movl	%r15d, %r14d
	movq	%r8, %r15
	movq	%rax, -656(%rbp)
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L756:
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L646
	movq	-504(%rbp), %rcx
	movl	%r14d, %r13d
	addl	(%rcx,%r12,4), %r13d
	jns	.L570
.L571:
	leaq	1(%r12), %rdx
	cmpq	%r15, %r12
	je	.L728
.L757:
	movq	%rdx, %r12
.L573:
	movl	-616(%rbp), %edx
	movl	%r12d, %edi
	xorl	%esi, %esi
	testl	%edx, %edx
	jle	.L568
	movl	%edx, %ecx
	subl	%r14d, %ecx
	testl	%ecx, %ecx
	jle	.L568
	movq	-600(%rbp), %rcx
	movq	-656(%rbp), %rax
	movl	(%rcx,%rax), %esi
.L568:
	movl	-552(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L569
	subl	%edi, %ecx
	testl	%ecx, %ecx
	jle	.L569
	movq	-536(%rbp), %rcx
	addl	(%rcx,%r12,4), %esi
.L569:
	movl	-520(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L756
.L646:
	movl	%r14d, %r13d
.L570:
	testl	%edx, %edx
	jle	.L571
	subl	%r13d, %edx
	testl	%edx, %edx
	jle	.L571
	movq	-600(%rbp), %rcx
	movslq	%r13d, %rdx
	cmpl	%esi, (%rcx,%rdx,4)
	jbe	.L571
	movq	-648(%rbp), %rdi
	movl	%r13d, %edx
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	leaq	1(%r12), %rdx
	cmpq	%r15, %r12
	jne	.L757
.L728:
	movl	%r14d, %r15d
	movl	-696(%rbp), %r12d
	movq	-704(%rbp), %rbx
	movq	-712(%rbp), %r14
.L572:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	leal	-12449(%rax), %edx
	cmpl	$93, %edx
	setbe	%cl
	cmpl	$12539, %eax
	setne	%dl
	testb	%dl, %cl
	jne	.L566
	subl	$65382, %eax
	cmpl	$57, %eax
	jbe	.L566
	movb	$0, -688(%rbp)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L746:
	movq	-672(%rbp), %r12
	movl	%r15d, %r8d
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L566:
	cmpb	$0, -688(%rbp)
	je	.L574
.L735:
	movb	$1, -688(%rbp)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L563:
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%edx, -656(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	-760(%rbp), %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	-656(%rbp), %edx
	testb	%al, %al
	jne	.L564
.L625:
	movq	-768(%rbp), %rdi
	movl	$255, %esi
	movl	%edx, -656(%rbp)
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	-656(%rbp), %edx
	movl	$1, %esi
	movq	-776(%rbp), %rdi
	leal	1(%rdx), %r13d
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movl	%r13d, %edx
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L538:
	movslq	8(%r12), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L541
	cmpl	12(%r12), %esi
	jle	.L542
.L541:
	movq	-648(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L540
	movslq	8(%r12), %rax
.L542:
	movl	-660(%rbp), %ecx
	movq	24(%r12), %rdx
	addl	%r13d, %ecx
	movl	%ecx, (%rdx,%rax,4)
	addl	$1, 8(%r12)
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L562:
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%eax, -656(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	-760(%rbp), %rdi
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	-656(%rbp), %edx
	testb	%al, %al
	je	.L625
	jmp	.L572
.L741:
	movq	%r12, %rdi
	call	utext_nativeLength_67@PLT
	movl	%eax, -656(%rbp)
	jmp	.L482
.L753:
	movslq	-616(%rbp), %rax
.L546:
	movq	-600(%rbp), %rdx
	movl	$0, (%rdx,%rax,4)
	addl	$1, -616(%rbp)
	jmp	.L547
.L584:
	movq	-728(%rbp), %rdi
	movq	-784(%rbp), %r13
	movl	-664(%rbp), %r12d
	call	utext_close_67@PLT
	leaq	-496(%rbp), %rax
	movl	-748(%rbp), %esi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movl	-616(%rbp), %eax
	testl	%eax, %eax
	jle	.L588
.L621:
	subl	%r12d, %eax
	testl	%eax, %eax
	jle	.L589
	movq	-600(%rbp), %rdx
	movslq	%r12d, %rax
	cmpl	$-1, (%rdx,%rax,4)
	jne	.L589
	movslq	-488(%rbp), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L590
	cmpl	-484(%rbp), %esi
	jle	.L591
.L590:
	movq	-672(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L650
	movslq	-488(%rbp), %rax
.L591:
	movq	-472(%rbp), %rdx
	xorl	%r15d, %r15d
	movl	$1, %ebx
	movl	%r12d, (%rdx,%rax,4)
	addl	$1, -488(%rbp)
	jmp	.L587
.L589:
	testl	%r12d, %r12d
	jle	.L736
.L588:
	movslq	-488(%rbp), %rax
	movslq	%r12d, %rbx
	xorl	%r15d, %r15d
	movq	-672(%rbp), %r12
	movl	%eax, %esi
	addl	$1, %esi
	js	.L592
.L758:
	cmpl	-484(%rbp), %esi
	jle	.L593
.L592:
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L594
	movslq	-488(%rbp), %rax
.L593:
	movq	-472(%rbp), %rdx
	movl	%ebx, (%rdx,%rax,4)
	addl	$1, -488(%rbp)
.L594:
	movl	-584(%rbp), %eax
	leal	1(%r15), %edx
	testl	%eax, %eax
	jle	.L731
	subl	%ebx, %eax
	testl	%eax, %eax
	jle	.L731
	movq	-568(%rbp), %rax
	movslq	(%rax,%rbx,4), %rbx
	testl	%ebx, %ebx
	jle	.L731
	movslq	-488(%rbp), %rax
	movl	%edx, %r15d
	movl	%eax, %esi
	addl	$1, %esi
	jns	.L758
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L731:
	movl	%edx, %ebx
.L587:
	movq	-744(%rbp), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L596
	movl	$0, %edx
	jg	.L759
.L597:
	cmpl	%edx, -660(%rbp)
	jg	.L596
	xorl	%r12d, %r12d
	cmpl	$-1, %r15d
	je	.L604
	movl	-488(%rbp), %eax
	movl	%ebx, %r12d
	movl	%r15d, %ebx
.L603:
	movslq	%ebx, %r15
	movq	%r13, -680(%rbp)
	movq	-744(%rbp), %rdi
	movl	$-1, %edx
	salq	$2, %r15
	movq	%r14, -688(%rbp)
	movq	-736(%rbp), %r13
	movl	%r12d, %r14d
	movq	%r15, %r12
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L605:
	subl	%ebx, %eax
	testl	%eax, %eax
	jle	.L739
	movq	-472(%rbp), %rax
	movslq	(%rax,%r12), %rax
	testq	%r13, %r13
	je	.L760
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jns	.L761
.L610:
	cmpl	%edx, %r15d
	jg	.L762
.L611:
	subl	$1, %r14d
.L614:
	subl	$1, %ebx
	subq	$4, %r12
	cmpl	$-1, %ebx
	je	.L732
.L763:
	movl	-488(%rbp), %eax
	movl	%r15d, %edx
.L615:
	testl	%eax, %eax
	jg	.L605
.L739:
	movl	-660(%rbp), %r15d
	testq	%r13, %r13
	je	.L610
	movl	8(%r13), %ecx
	xorl	%r15d, %r15d
	testl	%ecx, %ecx
	jle	.L610
	xorl	%eax, %eax
.L620:
	movq	24(%r13), %rsi
	movl	(%rsi,%rax), %r15d
	cmpl	%edx, %r15d
	jle	.L611
	.p2align 4,,10
	.p2align 3
.L762:
	movslq	8(%rdi), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L612
	cmpl	12(%rdi), %esi
	jle	.L613
.L612:
	movq	-680(%rbp), %rdx
	movq	%rdi, -656(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	-656(%rbp), %rdi
	testb	%al, %al
	je	.L614
	movslq	8(%rdi), %rax
.L613:
	movq	24(%rdi), %rdx
	subl	$1, %ebx
	subq	$4, %r12
	movl	%r15d, (%rdx,%rax,4)
	addl	$1, 8(%rdi)
	cmpl	$-1, %ebx
	jne	.L763
.L732:
	movl	%r14d, %r12d
	movq	-688(%rbp), %r14
.L604:
	movq	-672(%rbp), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	movq	-776(%rbp), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	movq	-768(%rbp), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	movq	-720(%rbp), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	movq	-648(%rbp), %rdi
	call	_ZN6icu_679UVector32D1Ev@PLT
	jmp	.L501
.L596:
	movslq	-488(%rbp), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L599
	cmpl	-484(%rbp), %esi
	jle	.L600
.L599:
	movq	-672(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L764
	movslq	-488(%rbp), %rax
.L600:
	movq	-472(%rbp), %rdx
	movl	$0, (%rdx,%rax,4)
	movl	-488(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -488(%rbp)
	jmp	.L602
.L574:
	movq	%r14, %rdi
	movl	$1, %edx
	movl	%r12d, %esi
	movl	$1, %r13d
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%r15d, -656(%rbp)
	movq	%r14, %r15
	movl	%eax, %r14d
	movswl	-312(%rbp), %eax
	testw	%ax, %ax
	js	.L575
.L765:
	sarl	$5, %eax
.L576:
	cmpl	%eax, %r14d
	jge	.L577
	cmpl	$20, %r13d
	je	.L729
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	leal	-12449(%rax), %edx
	cmpl	$93, %edx
	ja	.L658
	cmpl	$12539, %eax
	je	.L658
.L579:
	movl	%r14d, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	addl	$1, %r13d
	call	_ZNK6icu_6713UnicodeString11moveIndex32Eii@PLT
	movl	%eax, %r14d
	movswl	-312(%rbp), %eax
	testw	%ax, %ax
	jns	.L765
.L575:
	movl	-308(%rbp), %eax
	jmp	.L576
.L658:
	subl	$65382, %eax
	cmpl	$57, %eax
	jbe	.L579
	movq	%r15, %r14
	movl	-656(%rbp), %r15d
.L581:
	movl	-616(%rbp), %eax
	testl	%eax, %eax
	jle	.L735
	movl	%eax, %edx
	xorl	%esi, %esi
	subl	%r15d, %edx
	testl	%edx, %edx
	jle	.L583
	movq	-600(%rbp), %rdx
	movl	(%rdx,%rbx,4), %esi
.L583:
	cmpl	$8, %r13d
	jg	.L766
	movslq	%r13d, %rdx
	leaq	_ZZN6icu_67L15getKatakanaCostEiE12katakanaCost(%rip), %rcx
	addl	(%rcx,%rdx,4), %esi
.L622:
	addl	%r15d, %r13d
	subl	%r13d, %eax
	testl	%eax, %eax
	jle	.L735
	movq	-600(%rbp), %rdx
	movslq	%r13d, %rax
	cmpl	%esi, (%rdx,%rax,4)
	jbe	.L735
	movq	-648(%rbp), %rdi
	movl	%r13d, %edx
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	movq	-720(%rbp), %rdi
	movl	%r13d, %edx
	movl	%r15d, %esi
	call	_ZN6icu_679UVector3212setElementAtEii@PLT
	jmp	.L735
.L744:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector32D0Ev@PLT
.L500:
	leaq	-256(%rbp), %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, -736(%rbp)
.L501:
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L616
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L616
.L764:
	movl	-488(%rbp), %eax
.L602:
	leal	1(%rbx), %r12d
	jmp	.L603
.L759:
	movq	-744(%rbp), %rcx
	subl	$1, %eax
	cltq
	movq	24(%rcx), %rdx
	movl	(%rdx,%rax,4), %edx
	jmp	.L597
.L760:
	movl	-660(%rbp), %ecx
	leal	(%rcx,%rax), %r15d
	jmp	.L610
.L530:
	movl	-308(%rbp), %eax
	jmp	.L531
.L548:
	leaq	-592(%rbp), %rax
	movl	-748(%rbp), %esi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, -720(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	testl	%r12d, %r12d
	je	.L628
	leaq	-560(%rbp), %rax
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -768(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector327setSizeEi@PLT
	leaq	-528(%rbp), %rax
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -776(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	leaq	-464(%rbp), %r12
	call	_ZN6icu_679UVector327setSizeEi@PLT
	xorl	%eax, %eax
	movl	$18, %ecx
	movq	%r13, %rdx
	leaq	-464(%rbp), %rdi
	movq	%r14, %rsi
	rep stosq
	movq	%r12, %rdi
	movl	$878368812, -464(%rbp)
	movl	$144, -452(%rbp)
	call	utext_openUnicodeString_67@PLT
	movq	%r12, %rdi
	call	utext_close_67@PLT
	leaq	-496(%rbp), %rax
	movl	-748(%rbp), %esi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
.L736:
	movl	$-1, %r15d
	xorl	%ebx, %ebx
	jmp	.L587
.L639:
	movq	%r15, -736(%rbp)
	jmp	.L532
.L497:
	movl	-628(%rbp), %esi
	testl	%esi, %esi
	jg	.L500
	movl	$7, -628(%rbp)
	jmp	.L500
.L533:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -736(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L738
	movq	%r13, %rsi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movl	-628(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L536
	movq	-736(%rbp), %rdi
	call	_ZN6icu_679UVector32D0Ev@PLT
	movl	-628(%rbp), %edx
	testl	%edx, %edx
	jg	.L618
	movq	$0, -736(%rbp)
	jmp	.L536
.L577:
	movq	%r15, %r14
	movl	-656(%rbp), %r15d
	cmpl	$20, %r13d
	je	.L735
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L729:
	movq	%r15, %r14
	jmp	.L735
.L766:
	addl	$8192, %esi
	jmp	.L622
.L650:
	xorl	%r15d, %r15d
	movl	$1, %ebx
	jmp	.L587
.L483:
	leaq	-320(%rbp), %r14
.L738:
	cmpl	$0, -628(%rbp)
	jg	.L618
	movl	$7, -628(%rbp)
	xorl	%r12d, %r12d
	jmp	.L616
.L742:
	call	__stack_chk_fail@PLT
.L754:
	movq	-728(%rbp), %rdi
	call	utext_close_67@PLT
	leaq	-496(%rbp), %rax
	movl	-748(%rbp), %esi
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN6icu_679UVector32C1EiR10UErrorCode@PLT
	movl	-616(%rbp), %eax
	testl	%eax, %eax
	jg	.L621
	jmp	.L736
.L761:
	movl	8(%r13), %esi
	testl	%esi, %esi
	jle	.L610
	subl	%eax, %esi
	testl	%esi, %esi
	jle	.L610
	salq	$2, %rax
	jmp	.L620
	.cfi_endproc
.LFE3495:
	.size	_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0, .-_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.type	_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E, @function
_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E:
.LFB2554:
	.cfi_startproc
	endbr64
	cmpl	%ecx, %edx
	jge	.L768
	jmp	_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E.part.0
	.p2align 4,,10
	.p2align 3
.L768:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2554:
	.size	_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E, .-_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.weak	_ZTSN6icu_6721DictionaryBreakEngineE
	.section	.rodata._ZTSN6icu_6721DictionaryBreakEngineE,"aG",@progbits,_ZTSN6icu_6721DictionaryBreakEngineE,comdat
	.align 32
	.type	_ZTSN6icu_6721DictionaryBreakEngineE, @object
	.size	_ZTSN6icu_6721DictionaryBreakEngineE, 33
_ZTSN6icu_6721DictionaryBreakEngineE:
	.string	"N6icu_6721DictionaryBreakEngineE"
	.weak	_ZTIN6icu_6721DictionaryBreakEngineE
	.section	.data.rel.ro._ZTIN6icu_6721DictionaryBreakEngineE,"awG",@progbits,_ZTIN6icu_6721DictionaryBreakEngineE,comdat
	.align 8
	.type	_ZTIN6icu_6721DictionaryBreakEngineE, @object
	.size	_ZTIN6icu_6721DictionaryBreakEngineE, 24
_ZTIN6icu_6721DictionaryBreakEngineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721DictionaryBreakEngineE
	.quad	_ZTIN6icu_6719LanguageBreakEngineE
	.weak	_ZTSN6icu_6715ThaiBreakEngineE
	.section	.rodata._ZTSN6icu_6715ThaiBreakEngineE,"aG",@progbits,_ZTSN6icu_6715ThaiBreakEngineE,comdat
	.align 16
	.type	_ZTSN6icu_6715ThaiBreakEngineE, @object
	.size	_ZTSN6icu_6715ThaiBreakEngineE, 27
_ZTSN6icu_6715ThaiBreakEngineE:
	.string	"N6icu_6715ThaiBreakEngineE"
	.weak	_ZTIN6icu_6715ThaiBreakEngineE
	.section	.data.rel.ro._ZTIN6icu_6715ThaiBreakEngineE,"awG",@progbits,_ZTIN6icu_6715ThaiBreakEngineE,comdat
	.align 8
	.type	_ZTIN6icu_6715ThaiBreakEngineE, @object
	.size	_ZTIN6icu_6715ThaiBreakEngineE, 24
_ZTIN6icu_6715ThaiBreakEngineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715ThaiBreakEngineE
	.quad	_ZTIN6icu_6721DictionaryBreakEngineE
	.weak	_ZTSN6icu_6714LaoBreakEngineE
	.section	.rodata._ZTSN6icu_6714LaoBreakEngineE,"aG",@progbits,_ZTSN6icu_6714LaoBreakEngineE,comdat
	.align 16
	.type	_ZTSN6icu_6714LaoBreakEngineE, @object
	.size	_ZTSN6icu_6714LaoBreakEngineE, 26
_ZTSN6icu_6714LaoBreakEngineE:
	.string	"N6icu_6714LaoBreakEngineE"
	.weak	_ZTIN6icu_6714LaoBreakEngineE
	.section	.data.rel.ro._ZTIN6icu_6714LaoBreakEngineE,"awG",@progbits,_ZTIN6icu_6714LaoBreakEngineE,comdat
	.align 8
	.type	_ZTIN6icu_6714LaoBreakEngineE, @object
	.size	_ZTIN6icu_6714LaoBreakEngineE, 24
_ZTIN6icu_6714LaoBreakEngineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714LaoBreakEngineE
	.quad	_ZTIN6icu_6721DictionaryBreakEngineE
	.weak	_ZTSN6icu_6718BurmeseBreakEngineE
	.section	.rodata._ZTSN6icu_6718BurmeseBreakEngineE,"aG",@progbits,_ZTSN6icu_6718BurmeseBreakEngineE,comdat
	.align 16
	.type	_ZTSN6icu_6718BurmeseBreakEngineE, @object
	.size	_ZTSN6icu_6718BurmeseBreakEngineE, 30
_ZTSN6icu_6718BurmeseBreakEngineE:
	.string	"N6icu_6718BurmeseBreakEngineE"
	.weak	_ZTIN6icu_6718BurmeseBreakEngineE
	.section	.data.rel.ro._ZTIN6icu_6718BurmeseBreakEngineE,"awG",@progbits,_ZTIN6icu_6718BurmeseBreakEngineE,comdat
	.align 8
	.type	_ZTIN6icu_6718BurmeseBreakEngineE, @object
	.size	_ZTIN6icu_6718BurmeseBreakEngineE, 24
_ZTIN6icu_6718BurmeseBreakEngineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718BurmeseBreakEngineE
	.quad	_ZTIN6icu_6721DictionaryBreakEngineE
	.weak	_ZTSN6icu_6716KhmerBreakEngineE
	.section	.rodata._ZTSN6icu_6716KhmerBreakEngineE,"aG",@progbits,_ZTSN6icu_6716KhmerBreakEngineE,comdat
	.align 16
	.type	_ZTSN6icu_6716KhmerBreakEngineE, @object
	.size	_ZTSN6icu_6716KhmerBreakEngineE, 28
_ZTSN6icu_6716KhmerBreakEngineE:
	.string	"N6icu_6716KhmerBreakEngineE"
	.weak	_ZTIN6icu_6716KhmerBreakEngineE
	.section	.data.rel.ro._ZTIN6icu_6716KhmerBreakEngineE,"awG",@progbits,_ZTIN6icu_6716KhmerBreakEngineE,comdat
	.align 8
	.type	_ZTIN6icu_6716KhmerBreakEngineE, @object
	.size	_ZTIN6icu_6716KhmerBreakEngineE, 24
_ZTIN6icu_6716KhmerBreakEngineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6716KhmerBreakEngineE
	.quad	_ZTIN6icu_6721DictionaryBreakEngineE
	.weak	_ZTSN6icu_6714CjkBreakEngineE
	.section	.rodata._ZTSN6icu_6714CjkBreakEngineE,"aG",@progbits,_ZTSN6icu_6714CjkBreakEngineE,comdat
	.align 16
	.type	_ZTSN6icu_6714CjkBreakEngineE, @object
	.size	_ZTSN6icu_6714CjkBreakEngineE, 26
_ZTSN6icu_6714CjkBreakEngineE:
	.string	"N6icu_6714CjkBreakEngineE"
	.weak	_ZTIN6icu_6714CjkBreakEngineE
	.section	.data.rel.ro._ZTIN6icu_6714CjkBreakEngineE,"awG",@progbits,_ZTIN6icu_6714CjkBreakEngineE,comdat
	.align 8
	.type	_ZTIN6icu_6714CjkBreakEngineE, @object
	.size	_ZTIN6icu_6714CjkBreakEngineE, 24
_ZTIN6icu_6714CjkBreakEngineE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714CjkBreakEngineE
	.quad	_ZTIN6icu_6721DictionaryBreakEngineE
	.weak	_ZTVN6icu_6721DictionaryBreakEngineE
	.section	.data.rel.ro._ZTVN6icu_6721DictionaryBreakEngineE,"awG",@progbits,_ZTVN6icu_6721DictionaryBreakEngineE,comdat
	.align 8
	.type	_ZTVN6icu_6721DictionaryBreakEngineE, @object
	.size	_ZTVN6icu_6721DictionaryBreakEngineE, 64
_ZTVN6icu_6721DictionaryBreakEngineE:
	.quad	0
	.quad	_ZTIN6icu_6721DictionaryBreakEngineE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_6721DictionaryBreakEngine7handlesEi
	.quad	_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.quad	_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6715ThaiBreakEngineE
	.section	.data.rel.ro.local._ZTVN6icu_6715ThaiBreakEngineE,"awG",@progbits,_ZTVN6icu_6715ThaiBreakEngineE,comdat
	.align 8
	.type	_ZTVN6icu_6715ThaiBreakEngineE, @object
	.size	_ZTVN6icu_6715ThaiBreakEngineE, 64
_ZTVN6icu_6715ThaiBreakEngineE:
	.quad	0
	.quad	_ZTIN6icu_6715ThaiBreakEngineE
	.quad	_ZN6icu_6715ThaiBreakEngineD1Ev
	.quad	_ZN6icu_6715ThaiBreakEngineD0Ev
	.quad	_ZNK6icu_6721DictionaryBreakEngine7handlesEi
	.quad	_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.quad	_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE
	.quad	_ZNK6icu_6715ThaiBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.weak	_ZTVN6icu_6714LaoBreakEngineE
	.section	.data.rel.ro.local._ZTVN6icu_6714LaoBreakEngineE,"awG",@progbits,_ZTVN6icu_6714LaoBreakEngineE,comdat
	.align 8
	.type	_ZTVN6icu_6714LaoBreakEngineE, @object
	.size	_ZTVN6icu_6714LaoBreakEngineE, 64
_ZTVN6icu_6714LaoBreakEngineE:
	.quad	0
	.quad	_ZTIN6icu_6714LaoBreakEngineE
	.quad	_ZN6icu_6714LaoBreakEngineD1Ev
	.quad	_ZN6icu_6714LaoBreakEngineD0Ev
	.quad	_ZNK6icu_6721DictionaryBreakEngine7handlesEi
	.quad	_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.quad	_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE
	.quad	_ZNK6icu_6714LaoBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.weak	_ZTVN6icu_6718BurmeseBreakEngineE
	.section	.data.rel.ro.local._ZTVN6icu_6718BurmeseBreakEngineE,"awG",@progbits,_ZTVN6icu_6718BurmeseBreakEngineE,comdat
	.align 8
	.type	_ZTVN6icu_6718BurmeseBreakEngineE, @object
	.size	_ZTVN6icu_6718BurmeseBreakEngineE, 64
_ZTVN6icu_6718BurmeseBreakEngineE:
	.quad	0
	.quad	_ZTIN6icu_6718BurmeseBreakEngineE
	.quad	_ZN6icu_6718BurmeseBreakEngineD1Ev
	.quad	_ZN6icu_6718BurmeseBreakEngineD0Ev
	.quad	_ZNK6icu_6721DictionaryBreakEngine7handlesEi
	.quad	_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.quad	_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE
	.quad	_ZNK6icu_6718BurmeseBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.weak	_ZTVN6icu_6716KhmerBreakEngineE
	.section	.data.rel.ro.local._ZTVN6icu_6716KhmerBreakEngineE,"awG",@progbits,_ZTVN6icu_6716KhmerBreakEngineE,comdat
	.align 8
	.type	_ZTVN6icu_6716KhmerBreakEngineE, @object
	.size	_ZTVN6icu_6716KhmerBreakEngineE, 64
_ZTVN6icu_6716KhmerBreakEngineE:
	.quad	0
	.quad	_ZTIN6icu_6716KhmerBreakEngineE
	.quad	_ZN6icu_6716KhmerBreakEngineD1Ev
	.quad	_ZN6icu_6716KhmerBreakEngineD0Ev
	.quad	_ZNK6icu_6721DictionaryBreakEngine7handlesEi
	.quad	_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.quad	_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE
	.quad	_ZNK6icu_6716KhmerBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.weak	_ZTVN6icu_6714CjkBreakEngineE
	.section	.data.rel.ro.local._ZTVN6icu_6714CjkBreakEngineE,"awG",@progbits,_ZTVN6icu_6714CjkBreakEngineE,comdat
	.align 8
	.type	_ZTVN6icu_6714CjkBreakEngineE, @object
	.size	_ZTVN6icu_6714CjkBreakEngineE, 64
_ZTVN6icu_6714CjkBreakEngineE:
	.quad	0
	.quad	_ZTIN6icu_6714CjkBreakEngineE
	.quad	_ZN6icu_6714CjkBreakEngineD1Ev
	.quad	_ZN6icu_6714CjkBreakEngineD0Ev
	.quad	_ZNK6icu_6721DictionaryBreakEngine7handlesEi
	.quad	_ZNK6icu_6721DictionaryBreakEngine10findBreaksEP5UTextiiRNS_9UVector32E
	.quad	_ZN6icu_6721DictionaryBreakEngine13setCharactersERKNS_10UnicodeSetE
	.quad	_ZNK6icu_6714CjkBreakEngine23divideUpDictionaryRangeEP5UTextiiRNS_9UVector32E
	.section	.rodata
	.align 32
	.type	_ZZN6icu_67L15getKatakanaCostEiE12katakanaCost, @object
	.size	_ZZN6icu_67L15getKatakanaCostEiE12katakanaCost, 36
_ZZN6icu_67L15getKatakanaCostEiE12katakanaCost:
	.long	8192
	.long	984
	.long	408
	.long	240
	.long	204
	.long	252
	.long	300
	.long	372
	.long	480
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	-1
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
