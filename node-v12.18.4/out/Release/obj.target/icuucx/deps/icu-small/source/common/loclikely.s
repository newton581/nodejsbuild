	.file	"loclikely.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Zzzz"
.LC1:
	.string	"ZZ"
	.text
	.p2align 4
	.type	_ZL14parseTagStringPKcPcPiS1_S2_S1_S2_P10UErrorCode, @function
_ZL14parseTagStringPKcPcPiS1_S2_S1_S2_P10UErrorCode:
.LFB2405:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r10
	movq	24(%rbp), %r8
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	movq	%rdi, -64(%rbp)
	movq	%r8, -72(%rbp)
	jg	.L1
	leaq	-64(%rbp), %r11
	movq	%rdx, %r12
	movl	(%rdx), %edx
	movq	%rcx, %r15
	movq	%r11, %rcx
	movq	%r11, -104(%rbp)
	movq	%rsi, %r13
	movq	%rdi, %rbx
	call	ulocimp_getLanguage_67@PLT
	movq	-72(%rbp), %r8
	movl	(%r12), %esi
	movq	%r13, %rdi
	movl	%eax, %edx
	movl	%eax, -72(%rbp)
	movq	%r8, %rcx
	movq	%r8, -96(%rbp)
	call	u_terminateChars_67@PLT
	movq	-96(%rbp), %r8
	movl	-72(%rbp), %edx
	movq	-104(%rbp), %r11
	movq	-80(%rbp), %r10
	movl	(%r8), %ecx
	movq	-88(%rbp), %r9
	testl	%ecx, %ecx
	jg	.L28
	movq	-64(%rbp), %rdi
	movl	%edx, (%r12)
	movzbl	(%rdi), %eax
	cmpb	$95, %al
	je	.L18
	cmpb	$45, %al
	je	.L18
.L4:
	movl	(%r14), %edx
	movq	%r11, %rcx
	movq	%r15, %rsi
	movq	%r10, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	ulocimp_getScript_67@PLT
	movq	-72(%rbp), %r8
	movl	(%r14), %esi
	movq	%r15, %rdi
	movl	%eax, %edx
	movl	%eax, %r12d
	movq	%r8, %rcx
	call	u_terminateChars_67@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %r10
	movl	(%r8), %edx
	testl	%edx, %edx
	jg	.L28
	movl	%r12d, (%r14)
	testl	%r12d, %r12d
	jg	.L7
	movq	-64(%rbp), %rdi
.L8:
	movl	(%r10), %edx
	movq	%r11, %rcx
	movq	%r9, %rsi
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	ulocimp_getCountry_67@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	-72(%rbp), %r9
	movl	%eax, %edx
	movl	%eax, %r12d
	movl	(%r10), %esi
	movq	%r8, %rcx
	movq	%r9, %rdi
	call	u_terminateChars_67@PLT
	movq	-88(%rbp), %r8
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L28
	movl	%r12d, (%r10)
	movq	%r10, -72(%rbp)
	testl	%r12d, %r12d
	jg	.L30
	movq	-64(%rbp), %rcx
	leaq	-1(%rcx), %rax
	subq	%rbx, %rax
	movq	%rax, %rdx
	movq	%rcx, %rax
	subq	%rbx, %rax
	testb	$-65, (%rcx)
	cmovne	%edx, %eax
.L14:
	.p2align 4,,10
	.p2align 3
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L31
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L30:
	.cfi_restore_state
	movl	%r12d, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r9, %rdi
	call	uprv_strnicmp_67@PLT
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	jne	.L28
	movl	$0, (%r10)
	.p2align 4,,10
	.p2align 3
.L28:
	movl	-64(%rbp), %eax
	subl	%ebx, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$1, %rdi
	movq	%rdi, -64(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%r12d, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movq	%r11, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	uprv_strnicmp_67@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	testl	%eax, %eax
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r11
	jne	.L9
	movl	$0, (%r14)
.L9:
	movq	-64(%rbp), %rdi
	movzbl	(%rdi), %eax
	cmpb	$95, %al
	je	.L19
	cmpb	$45, %al
	jne	.L8
.L19:
	addq	$1, %rdi
	movq	%rdi, -64(%rbp)
	jmp	.L8
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2405:
	.size	_ZL14parseTagStringPKcPcPiS1_S2_S1_S2_P10UErrorCode, .-_ZL14parseTagStringPKcPcPiS1_S2_S1_S2_P10UErrorCode
	.section	.rodata.str1.1
.LC2:
	.string	"_"
	.text
	.p2align 4
	.type	_ZL29createTagStringWithAlternatesPKciS0_iS0_iS0_iS0_RN6icu_678ByteSinkEP10UErrorCode, @function
_ZL29createTagStringWithAlternatesPKciS0_iS0_iS0_iS0_RN6icu_678ByteSinkEP10UErrorCode:
.LFB2403:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	48(%rbp), %r8
	movq	32(%rbp), %r9
	movq	40(%rbp), %r10
	movq	%rax, -248(%rbp)
	movl	(%r8), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L106
	cmpl	$5, %ecx
	movl	%ecx, %ebx
	setg	%dl
	cmpl	$3, %r13d
	setg	%al
	orb	%al, %dl
	jne	.L68
	movl	%esi, %r12d
	cmpl	$11, %esi
	jg	.L68
	testl	%esi, %esi
	jg	.L112
	testq	%r9, %r9
	je	.L40
	leaq	-236(%rbp), %rsi
	movq	%r8, %rcx
	movl	$12, %edx
	movq	%r9, %rdi
	movq	%r8, -256(%rbp)
	movq	%r10, -280(%rbp)
	movq	%rsi, -272(%rbp)
	movq	%r9, -264(%rbp)
	call	uloc_getLanguage_67@PLT
	movq	-256(%rbp), %r8
	movl	%eax, %r12d
	movl	(%r8), %edx
	cmpl	$11, %eax
	jg	.L33
	testl	%edx, %edx
	jg	.L106
	testl	%eax, %eax
	movq	-264(%rbp), %r9
	movq	-272(%rbp), %rsi
	movq	-280(%rbp), %r10
	jne	.L113
.L42:
	testl	%ebx, %ebx
	jle	.L46
	movslq	%r12d, %rax
	addl	$1, %r12d
	leaq	-224(%rbp), %r11
	xorl	%edx, %edx
	movb	$95, -224(%rbp,%rax)
	movslq	%r12d, %rdi
	addq	%r11, %rdi
.L71:
	movl	%edx, %eax
	addl	$1, %edx
	movzbl	(%r14,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	cmpl	%ebx, %edx
	jb	.L71
	addl	%ebx, %r12d
	testl	%r13d, %r13d
	jle	.L57
.L109:
	movslq	%r12d, %rdx
	.p2align 4,,10
	.p2align 3
.L50:
	addl	$1, %r12d
	movb	$95, -224(%rbp,%rdx)
	movslq	%r12d, %rdi
	addq	%r11, %rdi
	testl	%r13d, %r13d
	je	.L59
	xorl	%edx, %edx
.L58:
	movl	%edx, %eax
	addl	$1, %edx
	movzbl	(%r15,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	cmpl	%r13d, %edx
	jb	.L58
.L59:
	addl	%r13d, %r12d
	movl	$1, %ebx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	-224(%rbp), %r11
	movslq	%esi, %rdx
	movl	$157, %ecx
	movq	%rdi, %rsi
	movq	%r11, %rdi
	movq	%r8, -280(%rbp)
	movq	%r10, -272(%rbp)
	movq	%r9, -264(%rbp)
	movq	%rdx, -256(%rbp)
	call	__memmove_chk@PLT
	testl	%ebx, %ebx
	movq	-256(%rbp), %rdx
	movq	-264(%rbp), %r9
	movq	-272(%rbp), %r10
	movq	-280(%rbp), %r8
	movq	%rax, %r11
	jg	.L114
	leaq	-236(%rbp), %rsi
	testq	%r9, %r9
	je	.L70
.L46:
	movq	%r8, %rcx
	movl	$6, %edx
	movq	%r9, %rdi
	movq	%r8, -256(%rbp)
	movq	%r10, -280(%rbp)
	movq	%rsi, -272(%rbp)
	movq	%r9, -264(%rbp)
	call	uloc_getScript_67@PLT
	movq	-256(%rbp), %r8
	movl	(%r8), %edx
	cmpl	$5, %eax
	jle	.L115
.L33:
	testl	%edx, %edx
	jg	.L106
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$1, (%r8)
.L32:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	cmpl	$15, %edx
	jne	.L32
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L40:
	testl	%ecx, %ecx
	jg	.L69
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	leaq	-224(%rbp), %r11
.L70:
	testl	%r13d, %r13d
	jg	.L50
.L76:
	xorl	%ebx, %ebx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L115:
	testl	%edx, %edx
	jg	.L106
	testl	%eax, %eax
	movq	-264(%rbp), %r9
	movq	-272(%rbp), %rsi
	movq	-280(%rbp), %r10
	jg	.L117
.L54:
	testl	%r13d, %r13d
	jg	.L110
.L57:
	movq	%r8, %rcx
	movl	$4, %edx
	movq	%r9, %rdi
	movq	%r8, -256(%rbp)
	movq	%r10, -272(%rbp)
	movq	%rsi, -264(%rbp)
	call	uloc_getCountry_67@PLT
	movq	-256(%rbp), %r8
	movl	(%r8), %edx
	testl	%edx, %edx
	jg	.L106
	cmpl	$3, %eax
	jg	.L33
	testl	%eax, %eax
	movq	-264(%rbp), %rsi
	movq	-272(%rbp), %r10
	jg	.L61
.L77:
	xorl	%ebx, %ebx
	leaq	-224(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%r10), %rax
	movq	%r10, -256(%rbp)
	movl	%r12d, %edx
	movq	%r11, %rsi
	movq	%r10, %rdi
	call	*16(%rax)
	movl	24(%rbp), %eax
	testl	%eax, %eax
	jle	.L32
	movq	-256(%rbp), %r10
	movq	-248(%rbp), %rcx
	movq	(%r10), %rax
	cmpb	$64, (%rcx)
	movq	16(%rax), %rax
	je	.L66
	movq	%r10, %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	*%rax
	movq	-256(%rbp), %r10
	testb	%bl, %bl
	movq	(%r10), %rax
	jne	.L111
	movq	%r10, -256(%rbp)
	movq	%r10, %rdi
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	*16(%rax)
	movq	-256(%rbp), %r10
	movq	(%r10), %rax
.L111:
	movq	16(%rax), %rax
.L66:
	movl	24(%rbp), %edx
	movq	-248(%rbp), %rsi
	movq	%r10, %rdi
	call	*%rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L69:
	movb	$95, -224(%rbp)
	leaq	-223(%rbp), %rsi
	xorl	%eax, %eax
.L73:
	movl	%eax, %edx
	addl	$1, %eax
	movzbl	(%r14,%rdx), %ecx
	movb	%cl, (%rsi,%rdx)
	cmpl	%ebx, %eax
	jb	.L73
	leal	1(%rbx), %r12d
	testl	%r13d, %r13d
	jle	.L77
.L110:
	movslq	%r12d, %rdx
	leaq	-224(%rbp), %r11
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L114:
	addl	$1, %r12d
	movb	$95, -224(%rbp,%rdx)
	xorl	%edx, %edx
	movslq	%r12d, %rdi
	addq	%rax, %rdi
.L47:
	movl	%edx, %eax
	addl	$1, %edx
	movzbl	(%r14,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	cmpl	%ebx, %edx
	jb	.L47
	addl	%ebx, %r12d
	testl	%r13d, %r13d
	jg	.L109
	testq	%r9, %r9
	je	.L76
	leaq	-236(%rbp), %rsi
	jmp	.L57
.L117:
	movslq	%r12d, %rdx
	addl	$1, %r12d
	movb	$95, -224(%rbp,%rdx)
	movslq	%r12d, %rdx
	leaq	-224(%rbp,%rdx), %rbx
	xorl	%edx, %edx
.L55:
	movl	%edx, %ecx
	addl	$1, %edx
	movzbl	(%rsi,%rcx), %edi
	movb	%dil, (%rbx,%rcx)
	cmpl	%eax, %edx
	jb	.L55
	addl	%eax, %r12d
	jmp	.L54
.L113:
	leaq	-224(%rbp), %r11
	movslq	%eax, %rdx
	movl	$157, %ecx
	movq	%r8, -280(%rbp)
	movq	%r11, %rdi
	movq	%r10, -272(%rbp)
	movq	%rsi, -256(%rbp)
	call	__memmove_chk@PLT
	movq	-280(%rbp), %r8
	movq	-272(%rbp), %r10
	movq	-264(%rbp), %r9
	movq	-256(%rbp), %rsi
	jmp	.L42
.L61:
	movslq	%r12d, %rdx
	addl	$1, %r12d
	leaq	-224(%rbp), %r11
	movslq	%r12d, %rdi
	movb	$95, -224(%rbp,%rdx)
	xorl	%edx, %edx
	addq	%r11, %rdi
.L63:
	movl	%edx, %ecx
	addl	$1, %edx
	movzbl	(%rsi,%rcx), %r8d
	movb	%r8b, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L63
	addl	%eax, %r12d
	movl	$1, %ebx
	jmp	.L51
.L116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2403:
	.size	_ZL29createTagStringWithAlternatesPKciS0_iS0_iS0_iS0_RN6icu_678ByteSinkEP10UErrorCode, .-_ZL29createTagStringWithAlternatesPKciS0_iS0_iS0_iS0_RN6icu_678ByteSinkEP10UErrorCode
	.section	.rodata.str1.1
.LC3:
	.string	"und"
.LC4:
	.string	"likelySubtags"
	.text
	.p2align 4
	.type	_ZL17findLikelySubtagsPKcPciP10UErrorCode.part.0.constprop.0, @function
_ZL17findLikelySubtagsPKcPciP10UErrorCode.part.0.constprop.0:
.LFB3044:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-148(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	movq	%r15, %rdx
	.cfi_offset 13, -40
	movq	%rdi, %r13
	xorl	%edi, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC4(%rip), %rsi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -152(%rbp)
	movl	$0, -148(%rbp)
	call	ures_openDirect_67@PLT
	movq	%rax, %r12
	movl	-148(%rbp), %eax
	testl	%eax, %eax
	jle	.L152
	movl	%eax, (%r14)
	xorl	%r13d, %r13d
.L132:
	testq	%r12, %r12
	je	.L118
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L118:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L153
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	leaq	-115(%rbp), %rax
	xorl	%edx, %edx
	leaq	-128(%rbp), %r8
	movl	$0, -72(%rbp)
	movq	%rax, -128(%rbp)
	movl	$40, -120(%rbp)
	movw	%dx, -116(%rbp)
	testq	%r13, %r13
	je	.L120
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L154
	leaq	.LC3(%rip), %r13
.L120:
	leaq	-152(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	ures_getStringByKey_67@PLT
	movl	-148(%rbp), %edx
	testl	%edx, %edx
	jle	.L125
	xorl	%r13d, %r13d
	cmpl	$2, %edx
	je	.L126
	movl	%edx, (%r14)
.L126:
	cmpb	$0, -116(%rbp)
	je	.L132
.L156:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L154:
	cmpb	$95, %al
	jne	.L120
	leaq	-144(%rbp), %r9
	leaq	.LC3(%rip), %rsi
	movq	%r8, -168(%rbp)
	movq	%r9, %rdi
	movq	%r9, -176(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-168(%rbp), %r8
	movl	-136(%rbp), %edx
	movq	%r14, %rcx
	movq	-144(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-176(%rbp), %r9
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-168(%rbp), %r8
	movl	-136(%rbp), %edx
	movq	%r14, %rcx
	movq	-144(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%r14), %eax
	movq	-128(%rbp), %r13
	testl	%eax, %eax
	jle	.L120
	cmpb	$0, -116(%rbp)
	jne	.L155
.L122:
	testq	%r12, %r12
	je	.L123
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L123:
	xorl	%r13d, %r13d
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L125:
	movl	-152(%rbp), %edx
	cmpl	$156, %edx
	jle	.L127
	xorl	%r13d, %r13d
	cmpb	$0, -116(%rbp)
	movl	$5, (%r14)
	je	.L132
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L127:
	addl	$1, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	u_UCharsToChars_67@PLT
	cmpl	$2, -152(%rbp)
	jg	.L128
.L151:
	movq	%rbx, %r13
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$3, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	call	uprv_strnicmp_67@PLT
	testl	%eax, %eax
	jne	.L151
	movl	-152(%rbp), %eax
	cmpl	$3, %eax
	je	.L130
	cmpb	$95, 3(%rbx)
	jne	.L151
.L130:
	subl	$2, %eax
	leaq	3(%rbx), %rsi
	movq	%rbx, %rdi
	movslq	%eax, %rdx
	call	memmove@PLT
	jmp	.L151
.L155:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	jmp	.L122
.L153:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3044:
	.size	_ZL17findLikelySubtagsPKcPciP10UErrorCode.part.0.constprop.0, .-_ZL17findLikelySubtagsPKcPciP10UErrorCode.part.0.constprop.0
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2699:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2699:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2702:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L170
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L158
	cmpb	$0, 12(%rbx)
	jne	.L171
.L162:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L158:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L162
	.cfi_endproc
.LFE2702:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2705:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L174
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2705:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2708:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L177
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2708:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L183
.L179:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L184
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2710:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2711:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2711:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2712:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2712:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2713:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2713:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2714:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2714:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2715:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2715:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2716:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L200
	testl	%edx, %edx
	jle	.L200
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L203
.L192:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L192
	.cfi_endproc
.LFE2716:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L207
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L207
	testl	%r12d, %r12d
	jg	.L214
	cmpb	$0, 12(%rbx)
	jne	.L215
.L209:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L209
.L215:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L207:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2717:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L217
	movq	(%rdi), %r8
.L218:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L221
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L221
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2718:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2719:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L228
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2719:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2720:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2720:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2721:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2721:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2722:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2722:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2724:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2724:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2726:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2726:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.p2align 4
	.type	_ZL25createLikelySubtagsStringPKciS0_iS0_iS0_iRN6icu_678ByteSinkEP10UErrorCode, @function
_ZL25createLikelySubtagsStringPKciS0_iS0_iS0_iRN6icu_678ByteSinkEP10UErrorCode:
.LFB2406:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	40(%rbp), %r14
	movq	%r8, -488(%rbp)
	movq	%rdi, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	32(%rbp), %rax
	movq	%rdx, -472(%rbp)
	movl	(%r14), %r8d
	movq	%rax, -504(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L310
	movl	%esi, %r13d
	movl	%ecx, %r12d
	movl	%r9d, %ebx
	testl	%ecx, %ecx
	jle	.L290
	testl	%r9d, %r9d
	jg	.L320
	testl	%ecx, %ecx
	jle	.L290
	leaq	-448(%rbp), %rax
	leaq	-464(%rbp), %r15
	movq	%rax, -512(%rbp)
	leaq	-435(%rbp), %rax
	movq	%rax, -520(%rbp)
.L242:
	movq	-520(%rbp), %rax
	movq	-512(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movl	$0, -392(%rbp)
	movq	%rax, -448(%rbp)
	movl	$40, -440(%rbp)
	movw	%cx, -436(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L246
	cmpl	$11, %r13d
	jg	.L247
	cmpl	$5, %r12d
	jg	.L247
	testl	%r13d, %r13d
	jg	.L321
	leaq	-224(%rbp), %r8
	movl	$1, %ecx
	movl	$1, %edi
	movq	%r8, %rax
.L248:
	movb	$95, (%rax)
	addq	%r8, %rcx
	testl	%r12d, %r12d
	je	.L278
	movq	-472(%rbp), %r9
	xorl	%eax, %eax
.L277:
	movl	%eax, %edx
	addl	$1, %eax
	movzbl	(%r9,%rdx), %esi
	movb	%sil, (%rcx,%rdx)
	cmpl	%r12d, %eax
	jb	.L277
.L278:
	leal	(%r12,%rdi), %edx
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6718CharStringByteSink6AppendEPKci@PLT
.L276:
	movq	%r15, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L267
	movq	-448(%rbp), %rdi
	movq	%r14, %rdx
	leaq	-384(%rbp), %rsi
	call	_ZL17findLikelySubtagsPKcPciP10UErrorCode.part.0.constprop.0
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L267
	testq	%rax, %rax
	jne	.L322
	cmpb	$0, -436(%rbp)
	je	.L245
	movq	-448(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	-464(%rbp), %r15
	xorl	%edi, %edi
	leaq	-435(%rbp), %rax
	movl	$0, -392(%rbp)
	leaq	-448(%rbp), %rsi
	movw	%di, -436(%rbp)
	movq	%r15, %rdi
	movq	%rax, -520(%rbp)
	movq	%rax, -448(%rbp)
	movq	%rsi, -512(%rbp)
	movl	$40, -440(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	subq	$8, %rsp
	movl	%ebx, %r9d
	movl	%r12d, %ecx
	pushq	%r14
	movq	-472(%rbp), %rdx
	movl	%r13d, %esi
	movq	-488(%rbp), %r8
	movq	-480(%rbp), %rdi
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	call	_ZL29createTagStringWithAlternatesPKciS0_iS0_iS0_iS0_RN6icu_678ByteSinkEP10UErrorCode
	addq	$48, %rsp
	movq	%r15, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jle	.L323
.L267:
	cmpb	$0, -436(%rbp)
	jne	.L324
.L244:
	testl	%edx, %edx
	jg	.L310
	movl	$1, (%r14)
.L310:
	xorl	%eax, %eax
.L234:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L325
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	leaq	-448(%rbp), %rax
	leaq	-464(%rbp), %r15
	movq	%rax, -512(%rbp)
	leaq	-435(%rbp), %rax
	movq	%rax, -520(%rbp)
.L245:
	testl	%ebx, %ebx
	jle	.L254
	movq	-520(%rbp), %rax
	movq	-512(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	$0, -392(%rbp)
	movq	%rax, -448(%rbp)
	movl	$40, -440(%rbp)
	movw	%dx, -436(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L255
	cmpl	$3, %ebx
	jg	.L256
	cmpl	$11, %r13d
	jg	.L256
	testl	%r13d, %r13d
	jg	.L326
	leaq	-224(%rbp), %r8
	movl	$1, %ecx
	movl	$1, %edi
	movq	%r8, %rax
.L257:
	movb	$95, (%rax)
	addq	%r8, %rcx
	testl	%ebx, %ebx
	je	.L275
	movq	-488(%rbp), %r9
	xorl	%eax, %eax
.L274:
	movl	%eax, %edx
	addl	$1, %eax
	movzbl	(%r9,%rdx), %esi
	movb	%sil, (%rcx,%rdx)
	cmpl	%ebx, %eax
	jb	.L274
.L275:
	leal	(%rbx,%rdi), %edx
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6718CharStringByteSink6AppendEPKci@PLT
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L255:
	cmpl	$15, %eax
	jne	.L273
.L256:
	movl	$1, (%r14)
.L273:
	movq	%r15, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L267
	movq	-448(%rbp), %rdi
	movq	%r14, %rdx
	leaq	-384(%rbp), %rsi
	call	_ZL17findLikelySubtagsPKcPciP10UErrorCode.part.0.constprop.0
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L267
	testq	%rax, %rax
	jne	.L327
	cmpb	$0, -436(%rbp)
	je	.L254
	movq	-448(%rbp), %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L254:
	movq	-520(%rbp), %rax
	movq	%r15, %rdi
	movq	-512(%rbp), %rsi
	movl	$0, -392(%rbp)
	movl	$40, -440(%rbp)
	movq	%rax, -448(%rbp)
	xorl	%eax, %eax
	movw	%ax, -436(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L263
	cmpl	$11, %r13d
	jg	.L264
	testl	%r13d, %r13d
	jg	.L328
	xorl	%r13d, %r13d
	leaq	-224(%rbp), %r8
.L265:
	movl	%r13d, %edx
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6718CharStringByteSink6AppendEPKci@PLT
.L266:
	movq	%r15, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L267
	movq	-448(%rbp), %rdi
	movq	%r14, %rdx
	leaq	-384(%rbp), %rsi
	call	_ZL17findLikelySubtagsPKcPciP10UErrorCode.part.0.constprop.0
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L267
	testq	%rax, %rax
	jne	.L329
	cmpb	$0, -436(%rbp)
	je	.L310
	movq	-448(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L324:
	movq	-448(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	(%r14), %edx
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L323:
	movq	-448(%rbp), %rdi
	movq	%r14, %rdx
	leaq	-384(%rbp), %rsi
	call	_ZL17findLikelySubtagsPKcPciP10UErrorCode.part.0.constprop.0
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L267
	testq	%rax, %rax
	jne	.L330
	cmpb	$0, -436(%rbp)
	je	.L242
	movq	-448(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L246:
	cmpl	$15, %eax
	jne	.L276
.L247:
	movl	$1, (%r14)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L263:
	cmpl	$15, %eax
	jne	.L266
.L264:
	movl	$1, (%r14)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L321:
	movslq	%r13d, %rax
	leaq	-224(%rbp), %r8
	movl	$157, %ecx
	movq	-480(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, %rdx
	movq	%rax, -528(%rbp)
	call	__memmove_chk@PLT
	movq	-528(%rbp), %rdx
	leal	1(%r13), %edi
	movq	%rax, %r8
	movslq	%edi, %rcx
	leaq	(%rax,%rdx), %rax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L328:
	leaq	-224(%rbp), %r8
	movq	-480(%rbp), %rsi
	movslq	%r13d, %rdx
	movl	$157, %ecx
	movq	%r8, %rdi
	call	__memmove_chk@PLT
	movq	%rax, %r8
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L326:
	movslq	%r13d, %rax
	leaq	-224(%rbp), %r8
	movl	$157, %ecx
	movq	-480(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, %rdx
	movq	%rax, -528(%rbp)
	call	__memmove_chk@PLT
	movq	-528(%rbp), %rdx
	leal	1(%r13), %edi
	movq	%rax, %r8
	movslq	%edi, %rcx
	leaq	(%rax,%rdx), %rax
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L330:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%r14
	xorl	%edx, %edx
	pushq	-504(%rbp)
	pushq	%rax
	movl	24(%rbp), %eax
	pushq	%rax
	pushq	-496(%rbp)
.L318:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	_ZL29createTagStringWithAlternatesPKciS0_iS0_iS0_iS0_RN6icu_678ByteSinkEP10UErrorCode
	addq	$48, %rsp
	cmpb	$0, -436(%rbp)
	jne	.L331
.L269:
	movl	$1, %eax
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-448(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L322:
	subq	$8, %rsp
	movq	-488(%rbp), %r8
	movl	%ebx, %r9d
	xorl	%ecx, %ecx
	pushq	%r14
	xorl	%edx, %edx
	pushq	-504(%rbp)
	pushq	%rax
	movl	24(%rbp), %eax
	pushq	%rax
	pushq	-496(%rbp)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L327:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%r14
	pushq	-504(%rbp)
	pushq	%rax
	movl	24(%rbp), %eax
	pushq	%rax
	pushq	-496(%rbp)
.L316:
	movq	-472(%rbp), %rdx
	movl	%r12d, %ecx
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L329:
	subq	$8, %rsp
	movq	-488(%rbp), %r8
	movl	%ebx, %r9d
	pushq	%r14
	pushq	-504(%rbp)
	pushq	%rax
	movl	24(%rbp), %eax
	pushq	%rax
	pushq	-496(%rbp)
	jmp	.L316
.L325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2406:
	.size	_ZL25createLikelySubtagsStringPKciS0_iS0_iS0_iRN6icu_678ByteSinkEP10UErrorCode, .-_ZL25createLikelySubtagsStringPKciS0_iS0_iS0_iRN6icu_678ByteSinkEP10UErrorCode
	.p2align 4
	.globl	ulocimp_addLikelySubtags_67
	.type	ulocimp_addLikelySubtags_67, @function
ulocimp_addLikelySubtags_67:
.LFB2411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$157, %edx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uloc_canonicalize_67@PLT
	movl	(%rbx), %eax
	cmpl	$-124, %eax
	je	.L343
	cmpl	$15, %eax
	je	.L343
	testl	%eax, %eax
	jle	.L367
.L332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L368
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	leaq	-252(%rbp), %rax
	pushq	%rbx
	leaq	-246(%rbp), %r10
	movq	%r13, %rdi
	pushq	%rax
	leaq	-242(%rbp), %r15
	movq	%r10, %r9
	leaq	-236(%rbp), %r14
	leaq	-260(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r10, -280(%rbp)
	leaq	-256(%rbp), %r8
	movl	$12, -260(%rbp)
	movl	$6, -256(%rbp)
	movl	$4, -252(%rbp)
	call	_ZL14parseTagStringPKcPcPiS1_S2_S1_S2_P10UErrorCode
	movl	(%rbx), %edx
	popq	%rcx
	movq	-280(%rbp), %r10
	popq	%rsi
	testl	%edx, %edx
	jg	.L369
	cltq
	leaq	0(%r13,%rax), %rdi
	movzbl	(%rdi), %edx
	cmpb	$45, %dl
	je	.L351
	cmpb	$95, %dl
	jne	.L338
.L351:
	leaq	1(%r13,%rax), %rax
	.p2align 4,,10
	.p2align 3
.L360:
	movzbl	(%rax), %edx
	movq	%rax, %rdi
	addq	$1, %rax
	cmpb	$95, %dl
	je	.L360
	cmpb	$45, %dl
	je	.L360
.L338:
	movq	%r10, -288(%rbp)
	movq	%rdi, -280(%rbp)
	call	strlen@PLT
	movq	-280(%rbp), %rdi
	movq	-288(%rbp), %r10
	testl	%eax, %eax
	jle	.L341
	leal	-1(%rax), %ecx
	movq	%rdi, %rdx
	xorl	%esi, %esi
	leaq	1(%rdi,%rcx), %r8
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L370:
	cmpb	$95, %cl
	je	.L349
	cmpb	$64, %cl
	je	.L341
	cmpl	$8, %esi
	jg	.L343
	addl	$1, %esi
.L342:
	addq	$1, %rdx
	cmpq	%rdx, %r8
	je	.L341
.L344:
	movzbl	(%rdx), %ecx
	cmpb	$45, %cl
	jne	.L370
.L349:
	xorl	%esi, %esi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L369:
	cmpl	$15, %edx
	jne	.L332
	.p2align 4,,10
	.p2align 3
.L343:
	movl	$1, (%rbx)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L341:
	pushq	%rbx
	movl	-252(%rbp), %r9d
	movq	%r10, %r8
	movq	%r15, %rdx
	movl	-256(%rbp), %ecx
	movl	-260(%rbp), %esi
	pushq	%r12
	pushq	%rax
	pushq	%rdi
	movq	%r14, %rdi
	call	_ZL25createLikelySubtagsStringPKciS0_iS0_iS0_iRN6icu_678ByteSinkEP10UErrorCode
	addq	$32, %rsp
	testb	%al, %al
	jne	.L332
	movq	%r13, %rdx
.L345:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L345
	movl	%eax, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ebx
	addb	%al, %bl
	movq	(%r12), %rax
	sbbq	$3, %rdx
	subq	%r13, %rdx
	call	*16(%rax)
	jmp	.L332
.L368:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2411:
	.size	ulocimp_addLikelySubtags_67, .-ulocimp_addLikelySubtags_67
	.p2align 4
	.globl	uloc_isRightToLeft_67
	.type	uloc_isRightToLeft_67, @function
uloc_isRightToLeft_67:
.LFB2414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-72(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-164(%rbp), %r13
	movq	%r14, %rsi
	pushq	%r12
	movq	%r13, %rcx
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -164(%rbp)
	call	uloc_getScript_67@PLT
	movl	-164(%rbp), %edx
	testl	%edx, %edx
	setg	%cl
	cmpl	$-124, %edx
	sete	%dl
	orb	%dl, %cl
	jne	.L385
	testl	%eax, %eax
	jne	.L372
.L385:
	leaq	-64(%rbp), %r15
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%r12, %rdi
	movl	$0, -164(%rbp)
	movq	%r15, %rsi
	call	uloc_getLanguage_67@PLT
	movslq	%eax, %rbx
	movl	-164(%rbp), %eax
	cmpl	$-124, %eax
	je	.L395
	testl	%eax, %eax
	jg	.L395
	testl	%ebx, %ebx
	jle	.L377
	movq	%r15, %rsi
	leaq	_ZL15LANG_DIR_STRING(%rip), %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L377
	movzbl	(%rax,%rbx), %edx
	movl	$1, %eax
	cmpb	$43, %dl
	je	.L371
	xorl	%eax, %eax
	cmpb	$45, %dl
	je	.L371
.L377:
	leaq	-160(%rbp), %r15
	leaq	-131(%rbp), %rax
	movl	$0, -164(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-144(%rbp), %rsi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movw	%ax, -132(%rbp)
	movl	$0, -88(%rbp)
	movl	$40, -136(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	ulocimp_addLikelySubtags_67
	movq	%r15, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	-164(%rbp), %eax
	cmpl	$-124, %eax
	je	.L378
	testl	%eax, %eax
	jg	.L378
	movq	-144(%rbp), %rdi
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%r14, %rsi
	call	uloc_getScript_67@PLT
	movl	-164(%rbp), %edx
	cmpl	$-124, %edx
	sete	%cl
	testl	%edx, %edx
	setg	%dl
	orb	%dl, %cl
	jne	.L378
	testl	%eax, %eax
	jne	.L396
	.p2align 4,,10
	.p2align 3
.L378:
	cmpb	$0, -132(%rbp)
	jne	.L397
.L395:
	xorl	%eax, %eax
.L371:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L398
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	cmpb	$0, -132(%rbp)
	je	.L372
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%r14, %rsi
	movl	$4106, %edi
	call	u_getPropertyValueEnum_67@PLT
	movl	%eax, %edi
	call	uscript_isRightToLeft_67@PLT
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L397:
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L395
.L398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2414:
	.size	uloc_isRightToLeft_67, .-uloc_isRightToLeft_67
	.section	.rodata.str1.1
.LC5:
	.string	"rg"
.LC6:
	.string	"ZZZZ"
	.text
	.p2align 4
	.globl	ulocimp_getRegionForSupplementalData_67
	.type	ulocimp_getRegionForSupplementalData_67, @function
ulocimp_getRegionForSupplementalData_67:
.LFB2416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -172(%rbp)
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L429
.L399:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L430
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	leaq	-148(%rbp), %rax
	movq	%rdx, %r14
	movl	%ecx, %r15d
	movq	%r8, %r12
	leaq	-64(%rbp), %rbx
	movq	%rax, %r8
	movl	$8, %ecx
	movq	%rdi, %r13
	movq	%rbx, %rdx
	leaq	.LC5(%rip), %rsi
	movl	$0, -148(%rbp)
	movq	%rax, -184(%rbp)
	call	uloc_getKeywordValue_67@PLT
	movl	-148(%rbp), %edi
	testl	%edi, %edi
	jg	.L409
	cmpl	$6, %eax
	je	.L401
.L409:
	movq	%rbx, %rsi
	movq	%r12, %rcx
	movl	$8, %edx
	movq	%r13, %rdi
	call	uloc_getCountry_67@PLT
	movl	(%r12), %esi
	movl	%eax, %r8d
	testl	%esi, %esi
	jle	.L431
	xorl	%r8d, %r8d
.L403:
	movslq	%r8d, %rax
	movslq	%r15d, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%r8d, -168(%rbp)
	movb	$0, -64(%rbp,%rax)
	call	strncpy@PLT
	movq	%r12, %rcx
	movl	%r15d, %esi
	movq	%r14, %rdi
	movl	-168(%rbp), %r8d
	movl	%r8d, %edx
	call	u_terminateChars_67@PLT
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L401:
	movsbl	-64(%rbp), %edi
	testb	%dil, %dil
	je	.L405
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%rdx, -168(%rbp)
	call	uprv_toupper_67@PLT
	movq	-168(%rbp), %rdx
	movb	%al, (%rdx)
	movsbl	1(%rdx), %edi
	addq	$1, %rdx
	testb	%dil, %dil
	jne	.L406
.L405:
	cmpl	$1515870810, -62(%rbp)
	jne	.L409
	cmpb	$0, -58(%rbp)
	movl	$2, %r8d
	je	.L403
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L431:
	testl	%eax, %eax
	jne	.L403
	cmpb	$0, -172(%rbp)
	je	.L403
	leaq	-144(%rbp), %r8
	leaq	-115(%rbp), %rax
	xorl	%edx, %edx
	movl	$0, -148(%rbp)
	movq	%r8, %rdi
	leaq	-128(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movw	%dx, -116(%rbp)
	movq	%r8, -168(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	movq	-168(%rbp), %r8
	movq	-184(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	ulocimp_addLikelySubtags_67
	movq	-168(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	-148(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L432
.L410:
	xorl	%r8d, %r8d
.L411:
	cmpb	$0, -116(%rbp)
	je	.L403
	movq	-128(%rbp), %rdi
	movl	%r8d, -168(%rbp)
	call	uprv_free_67@PLT
	movl	-168(%rbp), %r8d
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L432:
	movq	-128(%rbp), %rdi
	movq	%r12, %rcx
	movl	$8, %edx
	movq	%rbx, %rsi
	call	uloc_getCountry_67@PLT
	movl	%eax, %r8d
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L410
	jmp	.L411
.L430:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2416:
	.size	ulocimp_getRegionForSupplementalData_67, .-ulocimp_getRegionForSupplementalData_67
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676Locale13isRightToLeftEv
	.type	_ZNK6icu_676Locale13isRightToLeftEv, @function
_ZNK6icu_676Locale13isRightToLeftEv:
.LFB2415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-72(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-164(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%r14, %rsi
	movl	$0, -164(%rbp)
	movq	%rax, %rdi
	movq	%rax, %r12
	call	uloc_getScript_67@PLT
	movl	-164(%rbp), %edx
	testl	%edx, %edx
	setg	%cl
	cmpl	$-124, %edx
	sete	%dl
	orb	%dl, %cl
	jne	.L447
	testl	%eax, %eax
	jne	.L434
.L447:
	leaq	-64(%rbp), %r15
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%r12, %rdi
	movl	$0, -164(%rbp)
	movq	%r15, %rsi
	call	uloc_getLanguage_67@PLT
	movslq	%eax, %rbx
	movl	-164(%rbp), %eax
	cmpl	$-124, %eax
	je	.L457
	testl	%eax, %eax
	jg	.L457
	testl	%ebx, %ebx
	jle	.L439
	movq	%r15, %rsi
	leaq	_ZL15LANG_DIR_STRING(%rip), %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L439
	movzbl	(%rax,%rbx), %edx
	movl	$1, %eax
	cmpb	$43, %dl
	je	.L433
	xorl	%eax, %eax
	cmpb	$45, %dl
	je	.L433
.L439:
	leaq	-160(%rbp), %r15
	leaq	-131(%rbp), %rax
	movl	$0, -164(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-144(%rbp), %rsi
	xorl	%eax, %eax
	movq	%r15, %rdi
	movw	%ax, -132(%rbp)
	movl	$0, -88(%rbp)
	movl	$40, -136(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	ulocimp_addLikelySubtags_67
	movq	%r15, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	-164(%rbp), %eax
	cmpl	$-124, %eax
	je	.L440
	testl	%eax, %eax
	jg	.L440
	movq	-144(%rbp), %rdi
	movq	%r13, %rcx
	movl	$8, %edx
	movq	%r14, %rsi
	call	uloc_getScript_67@PLT
	movl	-164(%rbp), %edx
	cmpl	$-124, %edx
	sete	%cl
	testl	%edx, %edx
	setg	%dl
	orb	%dl, %cl
	jne	.L440
	testl	%eax, %eax
	jne	.L458
	.p2align 4,,10
	.p2align 3
.L440:
	cmpb	$0, -132(%rbp)
	jne	.L459
.L457:
	xorl	%eax, %eax
.L433:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L460
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	cmpb	$0, -132(%rbp)
	je	.L434
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L434:
	movq	%r14, %rsi
	movl	$4106, %edi
	call	u_getPropertyValueEnum_67@PLT
	movl	%eax, %edi
	call	uscript_isRightToLeft_67@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L459:
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L457
.L460:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2415:
	.size	_ZNK6icu_676Locale13isRightToLeftEv, .-_ZNK6icu_676Locale13isRightToLeftEv
	.p2align 4
	.type	_ZL21_uloc_minimizeSubtagsPKcRN6icu_678ByteSinkEP10UErrorCode, @function
_ZL21_uloc_minimizeSubtagsPKcRN6icu_678ByteSinkEP10UErrorCode:
.LFB2408:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -248(%rbp)
	movl	(%rdx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-195(%rbp), %rax
	movl	$0, -152(%rbp)
	movq	%rax, -208(%rbp)
	movl	$40, -200(%rbp)
	movw	%r8w, -196(%rbp)
	movl	$12, -236(%rbp)
	movl	$6, -232(%rbp)
	movl	$4, -228(%rbp)
	testl	%r9d, %r9d
	jg	.L461
	leaq	-74(%rbp), %rax
	movq	%rdx, %rbx
	leaq	-78(%rbp), %r14
	movq	%rdi, %r15
	movq	%rax, %rcx
	pushq	%rbx
	leaq	-68(%rbp), %r13
	leaq	-236(%rbp), %rdx
	movq	%rax, -256(%rbp)
	movq	%r13, %rsi
	leaq	-228(%rbp), %rax
	movq	%r14, %r9
	pushq	%rax
	leaq	-232(%rbp), %r8
	call	_ZL14parseTagStringPKcPcPiS1_S2_S1_S2_P10UErrorCode
	movl	(%rbx), %edx
	popq	%rcx
	popq	%rsi
	testl	%edx, %edx
	jg	.L532
	cltq
	leaq	(%r15,%rax), %r10
	movzbl	(%r10), %edx
	cmpb	$95, %dl
	je	.L502
	cmpb	$45, %dl
	jne	.L465
.L502:
	leaq	1(%r15,%rax), %rax
	.p2align 4,,10
	.p2align 3
.L513:
	movzbl	(%rax), %edx
	movq	%rax, %r10
	addq	$1, %rax
	cmpb	$45, %dl
	je	.L513
	cmpb	$95, %dl
	je	.L513
.L465:
	movq	%r10, %rdi
	movq	%r10, -264(%rbp)
	call	strlen@PLT
	movq	-264(%rbp), %r10
	testl	%eax, %eax
	movq	%rax, -288(%rbp)
	movq	%rax, %rdi
	jle	.L468
	leal	-1(%rdi), %edx
	movq	%r10, %rax
	xorl	%ecx, %ecx
	leaq	1(%r10,%rdx), %rsi
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L534:
	cmpb	$95, %dl
	je	.L501
	cmpb	$64, %dl
	je	.L468
	cmpl	$8, %ecx
	jg	.L533
	addq	$1, %rax
	addl	$1, %ecx
	cmpq	%rsi, %rax
	je	.L468
.L472:
	movzbl	(%rax), %edx
	cmpb	$45, %dl
	jne	.L534
.L501:
	addq	$1, %rax
	xorl	%ecx, %ecx
	cmpq	%rsi, %rax
	jne	.L472
.L468:
	xorl	%r12d, %r12d
	leaq	-131(%rbp), %rax
	leaq	-144(%rbp), %rsi
	movq	%r10, -264(%rbp)
	movw	%r12w, -132(%rbp)
	leaq	-224(%rbp), %r12
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rsi, -272(%rbp)
	movl	$0, -88(%rbp)
	movl	$40, -136(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	subq	$8, %rsp
	movq	%r14, %r8
	movl	-228(%rbp), %r9d
	movl	-232(%rbp), %ecx
	movl	-236(%rbp), %esi
	pushq	%rbx
	movq	%r13, %rdi
	pushq	%r12
	movq	-256(%rbp), %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	call	_ZL29createTagStringWithAlternatesPKciS0_iS0_iS0_iS0_RN6icu_678ByteSinkEP10UErrorCode
	addq	$48, %rsp
	movq	%r12, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	leaq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	movq	-144(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	ulocimp_addLikelySubtags_67
	movq	%r12, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	cmpb	$0, -132(%rbp)
	movq	-264(%rbp), %r10
	jne	.L535
.L473:
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jle	.L474
	movzbl	-196(%rbp), %eax
	testb	%al, %al
	jne	.L525
	.p2align 4,,10
	.p2align 3
.L461:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L536
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	movzbl	-196(%rbp), %eax
	cmpl	$15, %edx
	je	.L471
	.p2align 4,,10
	.p2align 3
.L497:
	testb	%al, %al
	je	.L461
.L525:
	movq	-208(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L474:
	movq	-280(%rbp), %rax
	movq	-272(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movq	%r10, -264(%rbp)
	movq	%rax, -144(%rbp)
	movw	%r9w, -132(%rbp)
	movl	$0, -88(%rbp)
	movl	$40, -136(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	pushq	%rbx
	movl	-236(%rbp), %esi
	xorl	%edx, %edx
	pushq	%r12
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movq	%r13, %rdi
	pushq	$0
	call	_ZL25createLikelySubtagsStringPKciS0_iS0_iS0_iRN6icu_678ByteSinkEP10UErrorCode
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	(%rbx), %edx
	movq	-264(%rbp), %r10
	testl	%edx, %edx
	jle	.L475
.L527:
	cmpb	$0, -132(%rbp)
	jne	.L537
.L477:
	movzbl	-196(%rbp), %eax
	testl	%edx, %edx
	jg	.L497
.L471:
	movl	$1, (%rbx)
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L535:
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-264(%rbp), %r10
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L533:
	movzbl	-196(%rbp), %eax
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L475:
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	jne	.L538
.L478:
	cmpb	$0, -132(%rbp)
	jne	.L539
.L482:
	movl	-228(%rbp), %r8d
	movq	%r10, -264(%rbp)
	testl	%r8d, %r8d
	jg	.L540
.L484:
	movq	%r15, %rdx
.L495:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L495
	movl	%eax, %ecx
	movq	-248(%rbp), %rdi
	movq	%r15, %rsi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ebx
	addb	%al, %bl
	movq	(%rdi), %rax
	sbbq	$3, %rdx
	subq	%r15, %rdx
	call	*16(%rax)
.L483:
	cmpb	$0, -196(%rbp)
	je	.L461
	jmp	.L525
.L540:
	movq	-280(%rbp), %rax
	xorl	%edi, %edi
	movq	-272(%rbp), %rsi
	movl	$0, -88(%rbp)
	movw	%di, -132(%rbp)
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	movl	$40, -136(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	pushq	%rbx
	movl	-228(%rbp), %r9d
	xorl	%edx, %edx
	pushq	%r12
	movl	-236(%rbp), %esi
	xorl	%ecx, %ecx
	movq	%r14, %r8
	pushq	$0
	movq	%r13, %rdi
	pushq	$0
	call	_ZL25createLikelySubtagsStringPKciS0_iS0_iS0_iRN6icu_678ByteSinkEP10UErrorCode
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	(%rbx), %edx
	movq	-264(%rbp), %r10
	testl	%edx, %edx
	jg	.L527
	movl	-88(%rbp), %edx
	movq	-144(%rbp), %rsi
	movq	%r10, -264(%rbp)
	movq	-208(%rbp), %rdi
	call	uprv_strnicmp_67@PLT
	movq	-264(%rbp), %r10
	testl	%eax, %eax
	je	.L541
	cmpb	$0, -132(%rbp)
	jne	.L542
.L489:
	movl	-232(%rbp), %esi
	movq	%r10, -264(%rbp)
	testl	%esi, %esi
	jle	.L484
	movl	-228(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L484
	movq	-280(%rbp), %rax
	movq	-272(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movw	%dx, -132(%rbp)
	movq	%rax, -144(%rbp)
	movl	$0, -88(%rbp)
	movl	$40, -136(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	pushq	%rbx
	movq	-256(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	%r12
	movl	-232(%rbp), %ecx
	xorl	%r8d, %r8d
	movq	%r13, %rdi
	pushq	$0
	movl	-236(%rbp), %esi
	pushq	$0
	call	_ZL25createLikelySubtagsStringPKciS0_iS0_iS0_iRN6icu_678ByteSinkEP10UErrorCode
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	movl	(%rbx), %edx
	movq	-264(%rbp), %r10
	testl	%edx, %edx
	jg	.L527
	movl	-88(%rbp), %edx
	movq	-144(%rbp), %rsi
	movq	%r10, -264(%rbp)
	movq	-208(%rbp), %rdi
	call	uprv_strnicmp_67@PLT
	movq	-264(%rbp), %r10
	testl	%eax, %eax
	je	.L543
	cmpb	$0, -132(%rbp)
	je	.L484
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L537:
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	(%rbx), %edx
	jmp	.L477
.L538:
	movq	-144(%rbp), %rsi
	movq	-208(%rbp), %rdi
	movq	%r10, -264(%rbp)
	call	uprv_strnicmp_67@PLT
	movq	-264(%rbp), %r10
	testl	%eax, %eax
	jne	.L478
	movl	-288(%rbp), %eax
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	-248(%rbp)
	pushq	$0
	pushq	%rax
	pushq	%r10
.L530:
	movl	-236(%rbp), %esi
	movq	%r13, %rdi
	call	_ZL29createTagStringWithAlternatesPKciS0_iS0_iS0_iS0_RN6icu_678ByteSinkEP10UErrorCode
	addq	$48, %rsp
	cmpb	$0, -132(%rbp)
	je	.L483
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L483
.L539:
	movq	-144(%rbp), %rdi
	movq	%r10, -264(%rbp)
	call	uprv_free_67@PLT
	movq	-264(%rbp), %r10
	jmp	.L482
.L542:
	movq	-144(%rbp), %rdi
	movq	%r10, -264(%rbp)
	call	uprv_free_67@PLT
	movq	-264(%rbp), %r10
	jmp	.L489
.L541:
	movl	-288(%rbp), %eax
	subq	$8, %rsp
	movq	%r14, %r8
	xorl	%ecx, %ecx
	pushq	%rbx
	movl	-228(%rbp), %r9d
	xorl	%edx, %edx
	pushq	-248(%rbp)
	pushq	$0
	pushq	%rax
	pushq	%r10
	jmp	.L530
.L543:
	pushq	%rax
	movl	-288(%rbp), %eax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	-232(%rbp), %ecx
	movq	-256(%rbp), %rdx
	pushq	%rbx
	pushq	-248(%rbp)
	pushq	$0
	pushq	%rax
	pushq	%r10
	jmp	.L530
.L536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2408:
	.size	_ZL21_uloc_minimizeSubtagsPKcRN6icu_678ByteSinkEP10UErrorCode, .-_ZL21_uloc_minimizeSubtagsPKcRN6icu_678ByteSinkEP10UErrorCode
	.p2align 4
	.globl	ulocimp_minimizeSubtags_67
	.type	ulocimp_minimizeSubtags_67, @function
ulocimp_minimizeSubtags_67:
.LFB2413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-208(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	movq	%r14, %rsi
	.cfi_offset 12, -40
	movq	%rdx, %r12
	movl	$157, %edx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	uloc_canonicalize_67@PLT
	movl	(%r12), %eax
	cmpl	$-124, %eax
	je	.L549
	cmpl	$15, %eax
	je	.L549
	testl	%eax, %eax
	jle	.L551
.L544:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L552
	addq	$184, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZL21_uloc_minimizeSubtagsPKcRN6icu_678ByteSinkEP10UErrorCode
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L549:
	movl	$1, (%r12)
	jmp	.L544
.L552:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2413:
	.size	ulocimp_minimizeSubtags_67, .-ulocimp_minimizeSubtags_67
	.p2align 4
	.globl	uloc_minimizeSubtags_67
	.type	uloc_minimizeSubtags_67, @function
uloc_minimizeSubtags_67:
.LFB2412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L567
.L553:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L568
	addq	$232, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	leaq	-256(%rbp), %r8
	movq	%rcx, %r12
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%r8, %rdi
	leaq	-224(%rbp), %r14
	movq	%r8, -264(%rbp)
	movl	%edx, %r15d
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	movq	%r12, %rcx
	movl	$157, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	uloc_canonicalize_67@PLT
	movl	(%r12), %eax
	movq	-264(%rbp), %r8
	cmpl	$-124, %eax
	je	.L564
	cmpl	$15, %eax
	je	.L564
	testl	%eax, %eax
	jle	.L569
	movl	-232(%rbp), %r14d
	movzbl	-228(%rbp), %eax
.L557:
	testb	%al, %al
	movl	$-1, %eax
	cmove	%eax, %r14d
.L560:
	movq	%r8, %rdi
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L569:
	movq	%r12, %rdx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -264(%rbp)
	call	_ZL21_uloc_minimizeSubtagsPKcRN6icu_678ByteSinkEP10UErrorCode
	movl	(%r12), %edx
	movl	-232(%rbp), %r14d
	movzbl	-228(%rbp), %eax
	movq	-264(%rbp), %r8
	testl	%edx, %edx
	jg	.L557
	testb	%al, %al
	je	.L561
	movl	$15, (%r12)
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L564:
	movl	$1, (%r12)
	movl	-232(%rbp), %r14d
	movzbl	-228(%rbp), %eax
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L561:
	movq	%r12, %rcx
	movl	%r14d, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%r8, -264(%rbp)
	call	u_terminateChars_67@PLT
	movq	-264(%rbp), %r8
	jmp	.L560
.L568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2412:
	.size	uloc_minimizeSubtags_67, .-uloc_minimizeSubtags_67
	.p2align 4
	.globl	uloc_addLikelySubtags_67
	.type	uloc_addLikelySubtags_67, @function
uloc_addLikelySubtags_67:
.LFB2410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$296, %rsp
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L612
.L570:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L613
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	leaq	-288(%rbp), %r15
	movq	%rcx, %r12
	movq	%rsi, %r13
	movl	%edx, %r14d
	movq	%rdi, -312(%rbp)
	movq	%r15, %rdi
	leaq	-224(%rbp), %rbx
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	movq	%r12, %rcx
	movl	$157, %edx
	movq	%rbx, %rsi
	movq	-312(%rbp), %r8
	movq	%r8, %rdi
	call	uloc_canonicalize_67@PLT
	movl	(%r12), %eax
	cmpl	$-124, %eax
	je	.L583
	cmpl	$15, %eax
	je	.L583
	testl	%eax, %eax
	jle	.L614
.L575:
	movl	-264(%rbp), %ebx
	movzbl	-260(%rbp), %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L614:
	leaq	-292(%rbp), %rax
	pushq	%r12
	leaq	-246(%rbp), %r11
	movq	%rbx, %rdi
	pushq	%rax
	leaq	-242(%rbp), %r10
	movq	%r11, %r9
	leaq	-236(%rbp), %rsi
	movq	%r10, %rcx
	leaq	-300(%rbp), %rdx
	leaq	-296(%rbp), %r8
	movq	%rsi, -328(%rbp)
	movq	%r11, -320(%rbp)
	movq	%r10, -312(%rbp)
	movl	$12, -300(%rbp)
	movl	$6, -296(%rbp)
	movl	$4, -292(%rbp)
	call	_ZL14parseTagStringPKcPcPiS1_S2_S1_S2_P10UErrorCode
	movl	(%r12), %edx
	popq	%rcx
	movq	-312(%rbp), %r10
	movq	-320(%rbp), %r11
	testl	%edx, %edx
	popq	%rsi
	jg	.L615
	cltq
	leaq	(%rbx,%rax), %rdi
	movzbl	(%rdi), %edx
	cmpb	$95, %dl
	je	.L595
	cmpb	$45, %dl
	je	.L595
	.p2align 4,,10
	.p2align 3
.L578:
	movq	%r10, -336(%rbp)
	movq	%r11, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	strlen@PLT
	movq	-312(%rbp), %rdi
	movq	-320(%rbp), %r11
	testl	%eax, %eax
	movq	-336(%rbp), %r10
	jle	.L581
	leal	-1(%rax), %ecx
	movq	%rdi, %rdx
	xorl	%esi, %esi
	leaq	1(%rdi,%rcx), %r8
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L616:
	cmpb	$95, %cl
	je	.L593
	cmpb	$64, %cl
	je	.L581
	cmpl	$8, %esi
	jg	.L583
	addl	$1, %esi
.L582:
	addq	$1, %rdx
	cmpq	%rdx, %r8
	je	.L581
.L584:
	movzbl	(%rdx), %ecx
	cmpb	$45, %cl
	jne	.L616
.L593:
	xorl	%esi, %esi
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L595:
	leaq	1(%rbx,%rax), %rax
	.p2align 4,,10
	.p2align 3
.L604:
	movzbl	(%rax), %edx
	movq	%rax, %rdi
	addq	$1, %rax
	cmpb	$45, %dl
	je	.L604
	cmpb	$95, %dl
	je	.L604
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L615:
	cmpl	$15, %edx
	jne	.L575
	.p2align 4,,10
	.p2align 3
.L583:
	movl	$1, (%r12)
	movl	-264(%rbp), %ebx
	movzbl	-260(%rbp), %eax
.L574:
	testb	%al, %al
	movl	$-1, %eax
	cmove	%eax, %ebx
.L589:
	movq	%r15, %rdi
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L581:
	pushq	%r12
	movl	-292(%rbp), %r9d
	movq	%r11, %r8
	movq	%r10, %rdx
	movl	-296(%rbp), %ecx
	movl	-300(%rbp), %esi
	pushq	%r15
	pushq	%rax
	pushq	%rdi
	movq	-328(%rbp), %rdi
	call	_ZL25createLikelySubtagsStringPKciS0_iS0_iS0_iRN6icu_678ByteSinkEP10UErrorCode
	addq	$32, %rsp
	testb	%al, %al
	jne	.L585
	movq	%rbx, %rdx
.L586:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L586
	movl	%eax, %ecx
	movq	%rbx, %rsi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %edi
	addb	%al, %dil
	movq	%r15, %rdi
	sbbq	$3, %rdx
	subq	%rbx, %rdx
	call	_ZN6icu_6720CheckedArrayByteSink6AppendEPKci@PLT
.L585:
	movl	(%r12), %edx
	movl	-264(%rbp), %ebx
	movzbl	-260(%rbp), %eax
	testl	%edx, %edx
	jg	.L574
	testb	%al, %al
	je	.L590
	movl	$15, (%r12)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L590:
	movq	%r12, %rcx
	movl	%ebx, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	u_terminateChars_67@PLT
	jmp	.L589
.L613:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2410:
	.size	uloc_addLikelySubtags_67, .-uloc_addLikelySubtags_67
	.section	.rodata
	.align 32
	.type	_ZL15LANG_DIR_STRING, @object
	.size	_ZL15LANG_DIR_STRING, 57
_ZL15LANG_DIR_STRING:
	.string	"root-en-es-pt-zh-ja-ko-de-fr-it-ar+he+fa+ru-nl-pl-th-tr-"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
