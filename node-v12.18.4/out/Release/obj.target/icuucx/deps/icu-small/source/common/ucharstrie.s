	.file	"ucharstrie.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrieD2Ev
	.type	_ZN6icu_6710UCharsTrieD2Ev, @function
_ZN6icu_6710UCharsTrieD2Ev:
.LFB2319:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2319:
	.size	_ZN6icu_6710UCharsTrieD2Ev, .-_ZN6icu_6710UCharsTrieD2Ev
	.globl	_ZN6icu_6710UCharsTrieD1Ev
	.set	_ZN6icu_6710UCharsTrieD1Ev,_ZN6icu_6710UCharsTrieD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UCharsTrie7currentEv
	.type	_ZNK6icu_6710UCharsTrie7currentEv, @function
_ZNK6icu_6710UCharsTrie7currentEv:
.LFB2321:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L5
	movl	24(%rdi), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	js	.L8
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movzwl	(%rdx), %edx
	cmpl	$63, %edx
	jle	.L3
	sarl	$15, %edx
	movl	$3, %eax
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2321:
	.size	_ZNK6icu_6710UCharsTrie7currentEv, .-_ZNK6icu_6710UCharsTrie7currentEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie10branchNextEPKDsii
	.type	_ZN6icu_6710UCharsTrie10branchNextEPKDsii, @function
_ZN6icu_6710UCharsTrie10branchNextEPKDsii:
.LFB2324:
	.cfi_startproc
	endbr64
	movzwl	(%rsi), %r8d
	movzwl	2(%rsi), %eax
	testl	%edx, %edx
	jne	.L10
	movzwl	%r8w, %edx
	addq	$2, %rsi
	movzwl	%ax, %r8d
	movzwl	2(%rsi), %eax
.L10:
	addl	$1, %edx
	cmpl	$5, %edx
	jg	.L12
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L50:
	cmpl	$64511, %eax
	jle	.L14
	movzwl	4(%rsi), %edx
	cmpl	$65535, %eax
	je	.L49
	subl	$64512, %eax
	leaq	6(%rsi), %r9
	sall	$16, %eax
	orl	%edx, %eax
.L14:
	cltq
	movl	%r10d, %edx
	leaq	(%r9,%rax,2), %rsi
.L16:
	movzwl	(%rsi), %r8d
	movzwl	2(%rsi), %eax
	cmpl	$5, %edx
	jle	.L11
.L12:
	movl	%edx, %r10d
	leaq	4(%rsi), %r9
	sarl	%r10d
	cmpl	%ecx, %r8d
	jg	.L50
	subl	%r10d, %edx
	cmpl	$64511, %eax
	jle	.L40
	leaq	8(%rsi), %r8
	addq	$6, %rsi
	cmpl	$65535, %eax
	cmove	%r8, %rsi
	movzwl	(%rsi), %r8d
	movzwl	2(%rsi), %eax
	cmpl	$5, %edx
	jg	.L12
.L11:
	cmpl	%r8d, %ecx
	je	.L18
	leal	-1(%rdx), %r9d
	testb	$64, %ah
	jne	.L19
	addq	$4, %rsi
.L20:
	movzwl	(%rsi), %r8d
	cmpl	$1, %r9d
	jle	.L22
	movzwl	2(%rsi), %eax
	cmpl	%r8d, %ecx
	je	.L18
	subl	$2, %edx
	testb	$64, %ah
	jne	.L23
	addq	$4, %rsi
.L24:
	movzwl	(%rsi), %r8d
	cmpl	$1, %edx
	je	.L22
	movzwl	2(%rsi), %eax
	cmpl	%r8d, %ecx
	je	.L18
	testb	$64, %ah
	jne	.L26
	addq	$4, %rsi
.L27:
	movzwl	(%rsi), %r8d
	cmpl	$2, %edx
	je	.L22
	movzwl	2(%rsi), %eax
	cmpl	%ecx, %r8d
	je	.L18
	testb	$64, %ah
	jne	.L36
	addq	$4, %rsi
.L37:
	movzwl	(%rsi), %r8d
.L22:
	cmpl	%r8d, %ecx
	jne	.L39
	movzwl	2(%rsi), %edx
	leaq	2(%rsi), %rax
	movq	%rax, 16(%rdi)
	movl	$1, %eax
	cmpl	$63, %edx
	jle	.L9
	sarl	$15, %edx
	movl	$3, %eax
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r9, %rsi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L49:
	movzwl	6(%rsi), %eax
	sall	$16, %edx
	leaq	8(%rsi), %r9
	orl	%edx, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L18:
	testw	%ax, %ax
	js	.L51
	movzwl	%ax, %edx
	cmpw	$16383, %ax
	jbe	.L52
	movzwl	4(%rsi), %eax
	cmpl	$32767, %edx
	je	.L34
	subl	$16384, %edx
	addq	$6, %rsi
	sall	$16, %edx
	orl	%eax, %edx
.L33:
	movslq	%edx, %rdx
	movl	$1, %eax
	leaq	(%rsi,%rdx,2), %rsi
	movzwl	(%rsi), %edx
	cmpl	$63, %edx
	jle	.L31
	sarl	$15, %edx
	movl	$3, %eax
	movq	%rsi, 16(%rdi)
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	andw	$32767, %ax
	leaq	8(%rsi), %r8
	addq	$6, %rsi
	cmpw	$32767, %ax
	cmove	%r8, %rsi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L51:
	addq	$2, %rsi
	movl	$2, %eax
.L31:
	movq	%rsi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movq	$0, 16(%rdi)
	xorl	%eax, %eax
.L9:
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	andw	$32767, %ax
	leaq	8(%rsi), %r8
	addq	$6, %rsi
	cmpw	$32767, %ax
	cmove	%r8, %rsi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	andw	$32767, %ax
	leaq	8(%rsi), %r8
	addq	$6, %rsi
	cmpw	$32767, %ax
	cmove	%r8, %rsi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L36:
	andw	$32767, %ax
	leaq	6(%rsi), %rdx
	addq	$8, %rsi
	cmpw	$32767, %ax
	cmovne	%rdx, %rsi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L52:
	addq	$4, %rsi
	jmp	.L33
.L34:
	movzwl	6(%rsi), %edx
	sall	$16, %eax
	addq	$8, %rsi
	orl	%eax, %edx
	jmp	.L33
	.cfi_endproc
.LFE2324:
	.size	_ZN6icu_6710UCharsTrie10branchNextEPKDsii, .-_ZN6icu_6710UCharsTrie10branchNextEPKDsii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie8nextImplEPKDsi
	.type	_ZN6icu_6710UCharsTrie8nextImplEPKDsi, @function
_ZN6icu_6710UCharsTrie8nextImplEPKDsi:
.LFB2325:
	.cfi_startproc
	endbr64
	movl	%edx, %ecx
	movzwl	(%rsi), %edx
	leaq	2(%rsi), %r9
	movl	%edx, %eax
	cmpl	$47, %edx
	jle	.L54
	cmpl	$63, %edx
	jle	.L55
	testw	%dx, %dx
	js	.L56
	cmpl	$16447, %edx
	jle	.L59
	leaq	4(%rsi), %r8
	addq	$6, %rsi
	cmpl	$32703, %edx
	movq	%r8, %r9
	cmovg	%rsi, %r9
.L59:
	movl	%eax, %r8d
	movl	%eax, %edx
	andl	$63, %r8d
	andl	$63, %edx
	cmpw	$47, %r8w
	jbe	.L54
.L55:
	movzwl	(%r9), %eax
	cmpl	%ecx, %eax
	je	.L64
.L56:
	movq	$0, 16(%rdi)
	xorl	%eax, %eax
.L53:
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r9, %rsi
	jmp	_ZN6icu_6710UCharsTrie10branchNextEPKDsii
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	2(%r9), %rax
	subl	$49, %edx
	movq	%rax, 16(%rdi)
	movl	$1, %eax
	movl	%edx, 24(%rdi)
	cmpl	$-1, %edx
	jne	.L53
	movzwl	2(%r9), %edx
	cmpl	$63, %edx
	jle	.L53
	sarl	$15, %edx
	movl	$3, %eax
	subl	%edx, %eax
	ret
	.cfi_endproc
.LFE2325:
	.size	_ZN6icu_6710UCharsTrie8nextImplEPKDsi, .-_ZN6icu_6710UCharsTrie8nextImplEPKDsi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie16nextForCodePointEi
	.type	_ZN6icu_6710UCharsTrie16nextForCodePointEi, @function
_ZN6icu_6710UCharsTrie16nextForCodePointEi:
.LFB2323:
	.cfi_startproc
	endbr64
	movl	%esi, %r11d
	movq	16(%rdi), %rsi
	cmpl	$65535, %r11d
	jg	.L66
	testq	%rsi, %rsi
	je	.L96
	movl	24(%rdi), %eax
	testl	%eax, %eax
	js	.L69
	movzwl	(%rsi), %edx
	cmpl	%edx, %r11d
	jne	.L91
	subl	$1, %eax
	leaq	2(%rsi), %rdx
	movl	%eax, 24(%rdi)
	movq	%rdx, 16(%rdi)
	cmpl	$-1, %eax
	je	.L71
.L92:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%r11d, %edx
	sarl	$10, %edx
	subw	$10304, %dx
	movzwl	%dx, %edx
	testq	%rsi, %rsi
	je	.L96
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	24(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	js	.L74
	movzwl	(%rsi), %ecx
	cmpl	%ecx, %edx
	jne	.L70
	leaq	2(%rsi), %r8
	subl	$1, %eax
	movl	%eax, 24(%rdi)
	movq	%r8, 16(%rdi)
	cmpl	$-1, %eax
	je	.L97
.L75:
	andl	$1023, %r11d
	orl	$56320, %r11d
.L78:
	testl	%eax, %eax
	js	.L77
	movzwl	(%r8), %ecx
	cmpl	%r11d, %ecx
	jne	.L70
	subl	$1, %eax
	leaq	2(%r8), %rdx
	movl	%eax, 24(%rdi)
	movq	%rdx, 16(%rdi)
	cmpl	$-1, %eax
	je	.L98
.L72:
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	$0, 16(%rdi)
.L95:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore 6
	movq	$0, 16(%rdi)
.L96:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	movl	%r11d, %edx
	jmp	_ZN6icu_6710UCharsTrie8nextImplEPKDsi
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	_ZN6icu_6710UCharsTrie8nextImplEPKDsi
	testb	$1, %al
	je	.L95
	movq	16(%rdi), %r8
	testq	%r8, %r8
	je	.L95
	movl	%r11d, %edx
	movl	24(%rdi), %eax
	andl	$1023, %edx
	orb	$-36, %dh
	movl	%edx, %r11d
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L97:
	movzwl	2(%rsi), %edx
	cmpl	$63, %edx
	jle	.L75
	shrl	$15, %edx
	jne	.L95
	andl	$1023, %r11d
	orl	$56320, %r11d
.L77:
	movl	%r11d, %edx
	movq	%r8, %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UCharsTrie8nextImplEPKDsi
	.p2align 4,,10
	.p2align 3
.L71:
	movzwl	2(%rsi), %eax
	cmpl	$63, %eax
	jle	.L92
	sarl	$15, %eax
	movl	%eax, %edx
	movl	$3, %eax
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movzwl	2(%r8), %eax
	cmpl	$63, %eax
	jle	.L72
	sarl	$15, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %edx
	movl	$3, %eax
	subl	%edx, %eax
	ret
	.cfi_endproc
.LFE2323:
	.size	_ZN6icu_6710UCharsTrie16nextForCodePointEi, .-_ZN6icu_6710UCharsTrie16nextForCodePointEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie17firstForCodePointEi
	.type	_ZN6icu_6710UCharsTrie17firstForCodePointEi, @function
_ZN6icu_6710UCharsTrie17firstForCodePointEi:
.LFB2322:
	.cfi_startproc
	endbr64
	movl	%esi, %r11d
	movq	8(%rdi), %rsi
	cmpl	$65535, %r11d
	jg	.L100
	movl	$-1, 24(%rdi)
	movl	%r11d, %edx
	jmp	_ZN6icu_6710UCharsTrie8nextImplEPKDsi
	.p2align 4,,10
	.p2align 3
.L100:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r11d, %edx
	sarl	$10, %edx
	movl	$-1, 24(%rdi)
	subw	$10304, %dx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movzwl	%dx, %edx
	call	_ZN6icu_6710UCharsTrie8nextImplEPKDsi
	movl	%eax, %r8d
	xorl	%eax, %eax
	andl	$1, %r8d
	jne	.L111
.L99:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movq	16(%rdi), %rsi
	andl	$1023, %r11d
	orl	$56320, %r11d
	testq	%rsi, %rsi
	je	.L99
	movl	24(%rdi), %edx
	testl	%edx, %edx
	js	.L102
	movzwl	(%rsi), %ecx
	cmpl	%ecx, %r11d
	jne	.L103
	leaq	2(%rsi), %rax
	subl	$1, %edx
	movq	%rax, 16(%rdi)
	movl	$1, %eax
	movl	%edx, 24(%rdi)
	cmpl	$-1, %edx
	jne	.L99
	movzwl	2(%rsi), %edx
	cmpl	$63, %edx
	jle	.L99
	sarl	$15, %edx
	movl	$3, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	$0, 16(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	%r11d, %edx
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UCharsTrie8nextImplEPKDsi
	.cfi_endproc
.LFE2322:
	.size	_ZN6icu_6710UCharsTrie17firstForCodePointEi, .-_ZN6icu_6710UCharsTrie17firstForCodePointEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie4nextEi
	.type	_ZN6icu_6710UCharsTrie4nextEi, @function
_ZN6icu_6710UCharsTrie4nextEi:
.LFB2326:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	testq	%r8, %r8
	je	.L116
	movl	24(%rdi), %eax
	testl	%eax, %eax
	js	.L114
	movzwl	(%r8), %edx
	cmpl	%esi, %edx
	je	.L119
	movq	$0, 16(%rdi)
	xorl	%r9d, %r9d
.L112:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	subl	$1, %eax
	leaq	2(%r8), %rdx
	movl	$1, %r9d
	movl	%eax, 24(%rdi)
	movq	%rdx, 16(%rdi)
	cmpl	$-1, %eax
	jne	.L112
	movzwl	2(%r8), %eax
	cmpl	$63, %eax
	jle	.L112
	sarl	$15, %eax
	movl	$3, %r9d
	subl	%eax, %r9d
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L114:
	movl	%esi, %edx
	movq	%r8, %rsi
	jmp	_ZN6icu_6710UCharsTrie8nextImplEPKDsi
	.p2align 4,,10
	.p2align 3
.L116:
	xorl	%r9d, %r9d
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE2326:
	.size	_ZN6icu_6710UCharsTrie4nextEi, .-_ZN6icu_6710UCharsTrie4nextEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie4nextENS_14ConstChar16PtrEi
	.type	_ZN6icu_6710UCharsTrie4nextENS_14ConstChar16PtrEi, @function
_ZN6icu_6710UCharsTrie4nextENS_14ConstChar16PtrEi:
.LFB2327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	subq	$8, %rsp
	movq	(%rsi), %rdx
	testl	%ebx, %ebx
	js	.L194
	sete	%al
.L122:
	movq	16(%rdi), %rcx
	testb	%al, %al
	je	.L123
	testq	%rcx, %rcx
	je	.L124
	movl	24(%rdi), %eax
	testl	%eax, %eax
	js	.L191
.L127:
	movl	$1, %eax
.L120:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L124
	movl	24(%rdi), %eax
.L148:
	testl	%ebx, %ebx
	js	.L195
	je	.L135
	movzwl	(%rdx), %r8d
	leaq	2(%rdx), %r11
	leal	-1(%rbx), %esi
	testl	%eax, %eax
	js	.L196
	movl	%ebx, %r9d
	leal	-2(%rbx), %ebx
	subl	%eax, %ebx
	subl	%r9d, %eax
	movl	%eax, %edx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L197:
	addq	$2, %rcx
	leal	(%rdx,%rsi), %eax
	testl	%esi, %esi
	je	.L135
	subl	$1, %esi
	addq	$2, %r11
	movzwl	-2(%r11), %r8d
	cmpl	%ebx, %esi
	je	.L139
.L138:
	cmpw	%r8w, (%rcx)
	je	.L197
.L140:
	movq	$0, 16(%rdi)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	cmpw	$0, (%rdx)
	sete	%al
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L135:
	movl	%eax, 24(%rdi)
	movq	%rcx, 16(%rdi)
	testl	%eax, %eax
	jns	.L127
.L191:
	movzwl	(%rcx), %eax
	cmpl	$63, %eax
	jle	.L127
	sarl	$15, %eax
	addq	$8, %rsp
	movl	%eax, %edx
	movl	$3, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movzwl	(%rdx), %r8d
	leaq	2(%rdx), %r11
	movl	%r8d, %edx
	testl	%r8d, %r8d
	je	.L135
	testl	%eax, %eax
	jns	.L131
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L133:
	movzwl	(%r11), %r8d
	addq	$2, %r11
	addq	$2, %rcx
	subl	$1, %eax
	movl	%r8d, %edx
	testl	%r8d, %r8d
	je	.L135
	cmpl	$-1, %eax
	je	.L139
.L131:
	cmpw	%dx, (%rcx)
	je	.L133
	jmp	.L140
.L196:
	movl	%esi, %ebx
	.p2align 4,,10
	.p2align 3
.L139:
	movl	%eax, 24(%rdi)
	movzwl	(%rcx), %edx
	leaq	2(%rcx), %rsi
	.p2align 4,,10
	.p2align 3
.L142:
	cmpl	$47, %edx
	jle	.L198
.L143:
	cmpl	$63, %edx
	jle	.L199
	testb	$-128, %dh
	jne	.L140
	cmpl	$16447, %edx
	jle	.L149
	leaq	2(%rsi), %rax
	addq	$4, %rsi
	cmpl	$32703, %edx
	cmovle	%rax, %rsi
.L149:
	andl	$63, %edx
	cmpl	$47, %edx
	jg	.L143
.L198:
	movl	%r8d, %ecx
	call	_ZN6icu_6710UCharsTrie10branchNextEPKDsii
	testl	%eax, %eax
	je	.L124
	testl	%ebx, %ebx
	js	.L200
	je	.L120
	movzwl	(%r11), %r8d
	subl	$1, %ebx
	addq	$2, %r11
.L145:
	cmpl	$2, %eax
	je	.L140
	movq	16(%rdi), %rax
	movzwl	(%rax), %edx
	leaq	2(%rax), %rsi
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L200:
	movzwl	(%r11), %r8d
	leaq	2(%r11), %rdx
	testl	%r8d, %r8d
	je	.L120
	movq	%rdx, %r11
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L199:
	movzwl	(%rsi), %eax
	cmpl	%r8d, %eax
	jne	.L140
	leal	-49(%rdx), %eax
	leaq	2(%rsi), %rcx
	movq	%r11, %rdx
	jmp	.L148
	.cfi_endproc
.LFE2327:
	.size	_ZN6icu_6710UCharsTrie4nextENS_14ConstChar16PtrEi, .-_ZN6icu_6710UCharsTrie4nextENS_14ConstChar16PtrEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie15findUniqueValueEPKDsaRi
	.type	_ZN6icu_6710UCharsTrie15findUniqueValueEPKDsaRi, @function
_ZN6icu_6710UCharsTrie15findUniqueValueEPKDsaRi:
.LFB2329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	leaq	2(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	movsbl	%sil, %edx
	subq	$8, %rsp
	movzwl	(%rax), %esi
	.p2align 4,,10
	.p2align 3
.L202:
	cmpl	$47, %esi
	jg	.L203
.L226:
	testl	%esi, %esi
	jne	.L204
	movzwl	(%rdi), %esi
	addq	$2, %rdi
.L204:
	addl	$1, %esi
	movq	%rbx, %rcx
	call	_ZN6icu_6710UCharsTrie25findUniqueValueFromBranchEPKDsiaRi
	testq	%rax, %rax
	je	.L205
	movzwl	(%rax), %esi
	leaq	2(%rax), %rdi
	movl	$1, %edx
	cmpl	$47, %esi
	jle	.L226
.L203:
	cmpl	$63, %esi
	jg	.L207
	subl	$48, %esi
	movslq	%esi, %rsi
	leaq	2(%rdi,%rsi,2), %rax
	movzwl	(%rax), %esi
	leaq	2(%rax), %rdi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L207:
	movl	%esi, %ecx
	sarl	$15, %ecx
	je	.L208
	movl	%esi, %eax
	andl	$32767, %eax
	testl	$16384, %esi
	je	.L209
	movzwl	(%rdi), %r8d
	cmpl	$32767, %eax
	je	.L210
	subl	$16384, %eax
	sall	$16, %eax
	orl	%r8d, %eax
	.p2align 4,,10
	.p2align 3
.L209:
	testb	%dl, %dl
	je	.L213
	cmpl	%eax, (%rbx)
	jne	.L205
	testl	%ecx, %ecx
	jne	.L218
.L227:
	cmpl	$16447, %esi
	jle	.L216
	leaq	2(%rdi), %rax
	addq	$4, %rdi
	cmpl	$32703, %esi
	cmovle	%rax, %rdi
.L216:
	andl	$63, %esi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L213:
	movl	%eax, (%rbx)
	movl	$1, %edx
	testl	%ecx, %ecx
	je	.L227
.L218:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	cmpl	$16447, %esi
	jg	.L211
	movl	%esi, %eax
	sarl	$6, %eax
	subl	$1, %eax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L211:
	movzwl	(%rdi), %eax
	cmpl	$32703, %esi
	jg	.L212
	movl	%esi, %r8d
	andl	$32704, %r8d
	subl	$16448, %r8d
	sall	$10, %r8d
	orl	%r8d, %eax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L210:
	movzwl	2(%rdi), %eax
	sall	$16, %r8d
	orl	%r8d, %eax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L212:
	movzwl	2(%rdi), %r8d
	sall	$16, %eax
	orl	%r8d, %eax
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L205:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2329:
	.size	_ZN6icu_6710UCharsTrie15findUniqueValueEPKDsaRi, .-_ZN6icu_6710UCharsTrie15findUniqueValueEPKDsaRi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie25findUniqueValueFromBranchEPKDsiaRi
	.type	_ZN6icu_6710UCharsTrie25findUniqueValueFromBranchEPKDsiaRi, @function
_ZN6icu_6710UCharsTrie25findUniqueValueFromBranchEPKDsiaRi:
.LFB2328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	%edx, -60(%rbp)
	movb	%dl, -53(%rbp)
	cmpl	$5, %esi
	jle	.L229
	movsbl	%dl, %eax
	movl	%eax, -52(%rbp)
.L230:
	movzwl	2(%rbx), %eax
	movl	%r15d, %r12d
	leaq	4(%rbx), %r13
	sarl	%r12d
	movq	%r13, %rdx
	cmpl	$64511, %eax
	jle	.L231
	movzwl	4(%rbx), %ecx
	cmpl	$65535, %eax
	je	.L278
	subl	$64512, %eax
	leaq	6(%rbx), %rdx
	sall	$16, %eax
	orl	%ecx, %eax
.L231:
	cltq
	movq	%r14, %rcx
	movl	%r12d, %esi
	leaq	(%rdx,%rax,2), %rdi
	movl	-52(%rbp), %edx
	call	_ZN6icu_6710UCharsTrie25findUniqueValueFromBranchEPKDsiaRi
	testq	%rax, %rax
	je	.L261
	movzwl	2(%rbx), %eax
	subl	%r12d, %r15d
	cmpl	$64511, %eax
	jle	.L263
	leaq	8(%rbx), %rdx
	addq	$6, %rbx
	cmpl	$65535, %eax
	cmove	%rdx, %rbx
.L235:
	cmpl	$5, %r15d
	jg	.L230
.L229:
	movzwl	2(%rbx), %ecx
	movl	%ecx, %eax
	sarl	$15, %ecx
	movl	%eax, %esi
	movl	%eax, %edx
	andw	$32767, %si
	andl	$32767, %edx
	testb	$64, %ah
	jne	.L237
	addq	$4, %rbx
.L238:
	testl	%ecx, %ecx
	je	.L279
.L240:
	cmpb	$0, -60(%rbp)
	jne	.L243
	movl	%edx, (%r14)
.L277:
	movb	$1, -53(%rbp)
.L242:
	leal	-1(%r15), %eax
	cmpl	$1, %eax
	jle	.L245
	movzwl	2(%rbx), %ecx
	movl	%ecx, %eax
	sarl	$15, %ecx
	movl	%eax, %esi
	movl	%eax, %edx
	andw	$32767, %si
	andl	$32767, %edx
	testb	$64, %ah
	jne	.L246
	addq	$4, %rbx
.L247:
	testl	%ecx, %ecx
	je	.L280
.L249:
	cmpl	(%r14), %edx
	jne	.L261
.L250:
	cmpl	$3, %r15d
	je	.L245
	movzwl	2(%rbx), %ecx
	movl	%ecx, %eax
	sarl	$15, %ecx
	movl	%eax, %esi
	movl	%eax, %edx
	andw	$32767, %si
	andl	$32767, %edx
	testb	$64, %ah
	jne	.L251
	addq	$4, %rbx
.L252:
	testl	%ecx, %ecx
	je	.L281
.L254:
	cmpl	(%r14), %edx
	jne	.L261
.L255:
	cmpl	$5, %r15d
	jne	.L245
	movzwl	2(%rbx), %ecx
	movl	%ecx, %eax
	sarl	$15, %ecx
	movl	%eax, %esi
	movl	%eax, %edx
	andw	$32767, %si
	andl	$32767, %edx
	testb	$64, %ah
	jne	.L256
	addq	$4, %rbx
.L257:
	testl	%ecx, %ecx
	je	.L259
	cmpl	%edx, (%r14)
	jne	.L261
.L245:
	addq	$24, %rsp
	leaq	2(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	movq	%r13, %rbx
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L278:
	movzwl	6(%rbx), %eax
	sall	$16, %ecx
	leaq	8(%rbx), %rdx
	orl	%ecx, %eax
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L259:
	movslq	%edx, %rdx
	movsbl	-53(%rbp), %esi
	leaq	(%rbx,%rdx,2), %rdi
	movq	%r14, %rdx
	call	_ZN6icu_6710UCharsTrie15findUniqueValueEPKDsaRi
	testb	%al, %al
	jne	.L245
.L261:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movzwl	4(%rbx), %eax
	cmpw	$32767, %si
	je	.L282
	subl	$16384, %edx
	addq	$6, %rbx
	sall	$16, %edx
	orl	%eax, %edx
	testl	%ecx, %ecx
	jne	.L240
.L279:
	movslq	%edx, %rdx
	movsbl	-60(%rbp), %esi
	leaq	(%rbx,%rdx,2), %rdi
	movq	%r14, %rdx
	call	_ZN6icu_6710UCharsTrie15findUniqueValueEPKDsaRi
	testb	%al, %al
	jne	.L277
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L243:
	cmpl	%edx, (%r14)
	je	.L242
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L282:
	movzwl	6(%rbx), %edx
	sall	$16, %eax
	addq	$8, %rbx
	orl	%eax, %edx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L246:
	movzwl	4(%rbx), %eax
	cmpw	$32767, %si
	jne	.L248
	movzwl	6(%rbx), %edx
	sall	$16, %eax
	addq	$8, %rbx
	orl	%eax, %edx
	testl	%ecx, %ecx
	jne	.L249
.L280:
	movslq	%edx, %rdx
	movsbl	-53(%rbp), %esi
	leaq	(%rbx,%rdx,2), %rdi
	movq	%r14, %rdx
	call	_ZN6icu_6710UCharsTrie15findUniqueValueEPKDsaRi
	testb	%al, %al
	je	.L261
	movb	$1, -53(%rbp)
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L251:
	movzwl	4(%rbx), %eax
	cmpw	$32767, %si
	jne	.L253
	movzwl	6(%rbx), %edx
	sall	$16, %eax
	addq	$8, %rbx
	orl	%eax, %edx
	testl	%ecx, %ecx
	jne	.L254
.L281:
	movslq	%edx, %rdx
	movsbl	-53(%rbp), %esi
	leaq	(%rbx,%rdx,2), %rdi
	movq	%r14, %rdx
	call	_ZN6icu_6710UCharsTrie15findUniqueValueEPKDsaRi
	testb	%al, %al
	je	.L261
	movb	$1, -53(%rbp)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L256:
	movzwl	4(%rbx), %eax
	cmpw	$32767, %si
	je	.L258
	subl	$16384, %edx
	addq	$6, %rbx
	sall	$16, %edx
	orl	%eax, %edx
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L248:
	subl	$16384, %edx
	addq	$6, %rbx
	sall	$16, %edx
	orl	%eax, %edx
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L253:
	subl	$16384, %edx
	addq	$6, %rbx
	sall	$16, %edx
	orl	%eax, %edx
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L258:
	movzwl	6(%rbx), %edx
	sall	$16, %eax
	addq	$8, %rbx
	orl	%eax, %edx
	jmp	.L257
	.cfi_endproc
.LFE2328:
	.size	_ZN6icu_6710UCharsTrie25findUniqueValueFromBranchEPKDsiaRi, .-_ZN6icu_6710UCharsTrie25findUniqueValueFromBranchEPKDsiaRi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie19getNextBranchUCharsEPKDsiRNS_10AppendableE
	.type	_ZN6icu_6710UCharsTrie19getNextBranchUCharsEPKDsiRNS_10AppendableE, @function
_ZN6icu_6710UCharsTrie19getNextBranchUCharsEPKDsiRNS_10AppendableE:
.LFB2331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$5, %esi
	jle	.L284
.L285:
	movzwl	2(%rbx), %eax
	movl	%r12d, %r14d
	leaq	4(%rbx), %r15
	sarl	%r14d
	movq	%r15, %rdx
	cmpl	$64511, %eax
	jle	.L286
	movzwl	4(%rbx), %ecx
	cmpl	$65535, %eax
	je	.L298
	subl	$64512, %eax
	leaq	6(%rbx), %rdx
	sall	$16, %eax
	orl	%ecx, %eax
.L286:
	cltq
	movl	%r14d, %esi
	subl	%r14d, %r12d
	leaq	(%rdx,%rax,2), %rdi
	movq	%r13, %rdx
	call	_ZN6icu_6710UCharsTrie19getNextBranchUCharsEPKDsiRNS_10AppendableE
	movzwl	2(%rbx), %eax
	cmpl	$64511, %eax
	jle	.L295
	leaq	8(%rbx), %rdx
	addq	$6, %rbx
	cmpl	$65535, %eax
	cmove	%rdx, %rbx
.L288:
	cmpl	$5, %r12d
	jg	.L285
.L284:
	movzwl	(%rbx), %esi
.L293:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movzwl	2(%rbx), %eax
	testb	$64, %ah
	jne	.L290
	movzwl	4(%rbx), %esi
	addq	$4, %rbx
.L291:
	subl	$1, %r12d
	cmpl	$1, %r12d
	jg	.L293
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L292
	movzwl	6(%rbx), %esi
	addq	$6, %rbx
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%r15, %rbx
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L292:
	movzwl	8(%rbx), %esi
	addq	$8, %rbx
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L298:
	movzwl	6(%rbx), %eax
	sall	$16, %ecx
	leaq	8(%rbx), %rdx
	orl	%ecx, %eax
	jmp	.L286
	.cfi_endproc
.LFE2331:
	.size	_ZN6icu_6710UCharsTrie19getNextBranchUCharsEPKDsiRNS_10AppendableE, .-_ZN6icu_6710UCharsTrie19getNextBranchUCharsEPKDsiRNS_10AppendableE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UCharsTrie13getNextUCharsERNS_10AppendableE
	.type	_ZNK6icu_6710UCharsTrie13getNextUCharsERNS_10AppendableE, @function
_ZNK6icu_6710UCharsTrie13getNextUCharsERNS_10AppendableE:
.LFB2330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L307
	movq	%rsi, %r12
	movl	24(%rdi), %ecx
	movzwl	(%rax), %esi
	movl	%esi, %edx
	testl	%ecx, %ecx
	jns	.L310
	leaq	2(%rax), %r14
	cmpl	$63, %esi
	jle	.L302
	xorl	%r13d, %r13d
	testw	%si, %si
	js	.L299
	cmpl	$16447, %esi
	jle	.L303
	leaq	4(%rax), %r14
	addq	$6, %rax
	cmpl	$32703, %esi
	cmovg	%rax, %r14
.L303:
	movl	%edx, %esi
	andl	$63, %esi
.L302:
	movq	(%r12), %rax
	cmpl	$47, %esi
	jg	.L305
	testl	%esi, %esi
	jne	.L306
	movzwl	(%r14), %esi
	addq	$2, %r14
.L306:
	leal	1(%rsi), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	*48(%rax)
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrie19getNextBranchUCharsEPKDsiRNS_10AppendableE
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	$1, %r13d
	call	*24(%rax)
.L299:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movzwl	(%r14), %esi
	movq	%r12, %rdi
	movl	$1, %r13d
	call	*24(%rax)
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2330:
	.size	_ZNK6icu_6710UCharsTrie13getNextUCharsERNS_10AppendableE, .-_ZNK6icu_6710UCharsTrie13getNextUCharsERNS_10AppendableE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
