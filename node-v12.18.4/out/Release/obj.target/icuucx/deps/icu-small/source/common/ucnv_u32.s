	.file	"ucnv_u32.cpp"
	.text
	.p2align 4
	.type	T_UConverter_toUnicode_UTF32_BE, @function
T_UConverter_toUnicode_UTF32_BE:
.LFB2113:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r11
	movq	16(%rdi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rdi), %r9
	movq	24(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	64(%r11), %ecx
	movq	40(%rdi), %rbx
	testb	%cl, %cl
	jle	.L2
	movl	$1, %edx
	cmpq	%rbx, %r9
	jb	.L36
.L3:
	cmpq	%r8, %rax
	jnb	.L14
	testb	%dl, %dl
	je	.L14
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L37
.L14:
	popq	%rbx
	popq	%r12
	movq	%r9, 32(%rdi)
	popq	%r13
	popq	%r14
	movq	%rax, 16(%rdi)
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movb	$0, 64(%r11)
	movq	8(%rdi), %r12
	movsbl	%cl, %r10d
	movl	72(%r12), %r13d
	movl	$0, 72(%r12)
	leal	-1(%r13), %edx
	cmpb	$3, %cl
	jg	.L4
	cmpq	%r8, %rax
	jnb	.L8
	movzbl	(%rax), %ecx
	sall	$8, %edx
	movl	%r10d, %r14d
	leaq	65(%r11), %r12
	addq	$1, %rax
	movl	%ecx, %r13d
	orl	%ecx, %edx
	leal	1(%r10), %ecx
	movb	%r13b, 65(%r11,%r14)
	cmpl	$3, %r10d
	jne	.L15
	movl	$4, %r10d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L38:
	movzbl	2(%rax), %r10d
	sall	$8, %edx
	addq	$3, %rax
	movb	%r10b, 3(%r12)
	orl	%r10d, %edx
	movl	$4, %r10d
.L4:
	cmpl	$1114111, %edx
	ja	.L10
	movl	%edx, %ecx
	andl	$-2048, %ecx
	cmpl	$55296, %ecx
	je	.L10
	leaq	2(%r9), %rcx
	cmpl	$65535, %edx
	ja	.L11
	movw	%dx, (%r9)
	movq	%rcx, %r9
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	%rbx, %r9
	setnb	%cl
	movl	%ecx, %edx
	cmpq	%r8, %rax
	jnb	.L14
	testb	%cl, %cl
	jne	.L3
	movzbl	(%rax), %edx
	leaq	65(%r11), %r12
	addq	$1, %rax
	movl	$1, %ecx
	movb	%dl, 65(%r11)
.L15:
	cmpq	%r8, %rax
	jnb	.L9
	movzbl	(%rax), %r10d
	sall	$8, %edx
	movl	%ecx, %r14d
	leaq	1(%rax), %r15
	movl	%r10d, %r13d
	orl	%r10d, %edx
	leal	1(%rcx), %r10d
	movb	%r13b, (%r12,%r14)
	cmpl	$3, %ecx
	je	.L18
	cmpq	%r15, %r8
	jbe	.L19
	movzbl	1(%rax), %r14d
	sall	$8, %edx
	movl	%r14d, %r13d
	orl	%r14d, %edx
	leaq	2(%rax), %r14
	movb	%r13b, (%r12,%r10)
	cmpl	$2, %ecx
	je	.L20
	cmpq	%r14, %r8
	ja	.L38
	movq	%r14, %rax
	movl	$3, %ecx
.L9:
	movq	8(%rdi), %r12
	leal	1(%rdx), %r13d
.L8:
	cmpq	%r9, %rbx
	movl	%r13d, 72(%r12)
	movb	%cl, 64(%r12)
	setbe	%dl
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$15, (%rsi)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L10:
	movq	8(%rdi), %rdx
	movb	%r10b, 64(%rdx)
	movl	$12, (%rsi)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r15, %rax
	jmp	.L4
.L11:
	movl	%edx, %r10d
	andw	$1023, %dx
	shrl	$10, %r10d
	orw	$-9216, %dx
	subw	$10304, %r10w
	movw	%r10w, (%r9)
	cmpq	%rcx, %rbx
	jbe	.L12
	movw	%dx, 2(%r9)
	addq	$4, %r9
	jmp	.L2
.L19:
	movl	%r10d, %ecx
	movq	%r15, %rax
	jmp	.L9
.L20:
	movl	$4, %r10d
	movq	%r14, %rax
	jmp	.L4
.L12:
	movq	8(%rdi), %r8
	movq	%rcx, %r9
	movw	%dx, 144(%r8)
	movb	$1, 93(%r8)
	movl	$15, (%rsi)
	jmp	.L14
	.cfi_endproc
.LFE2113:
	.size	T_UConverter_toUnicode_UTF32_BE, .-T_UConverter_toUnicode_UTF32_BE
	.p2align 4
	.type	T_UConverter_toUnicode_UTF32_BE_OFFSET_LOGIC, @function
T_UConverter_toUnicode_UTF32_BE_OFFSET_LOGIC:
.LFB2114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	16(%rdi), %rax
	movq	32(%rdi), %r10
	movq	48(%rdi), %r12
	movzbl	64(%r8), %ecx
	movq	24(%rdi), %r9
	movq	40(%rdi), %r11
	testb	%cl, %cl
	jle	.L55
	movl	$1, %edx
	cmpq	%r11, %r10
	jb	.L77
.L41:
	cmpq	%r9, %rax
	jnb	.L53
	testb	%dl, %dl
	je	.L53
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L78
.L53:
	movq	%r12, 48(%rdi)
	movq	%r10, 32(%rdi)
	movq	%rax, 16(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movb	$0, 64(%r8)
	movq	8(%rdi), %r13
	movsbl	%cl, %ebx
	movl	72(%r13), %r14d
	movl	$0, 72(%r13)
	leal	-1(%r14), %edx
	cmpb	$3, %cl
	jg	.L76
	cmpq	%r9, %rax
	jnb	.L46
	movzbl	(%rax), %ecx
	sall	$8, %edx
	movl	%ebx, %r15d
	leaq	65(%r8), %r13
	addq	$1, %rax
	movl	%ecx, %r14d
	orl	%ecx, %edx
	leal	1(%rbx), %ecx
	movb	%r14b, 65(%r8,%r15)
	cmpl	$3, %ebx
	je	.L58
	movl	$0, -52(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$0, -52(%rbp)
.L40:
	cmpq	%r11, %r10
	setnb	%cl
	movl	%ecx, %edx
	cmpq	%r9, %rax
	jnb	.L53
	testb	%cl, %cl
	jne	.L41
	movzbl	(%rax), %edx
	leaq	65(%r8), %r13
	addq	$1, %rax
	movl	$1, %ecx
	movb	%dl, 65(%r8)
.L54:
	cmpq	%rax, %r9
	jbe	.L47
	leaq	1(%rax), %r15
	movzbl	(%rax), %ebx
	sall	$8, %edx
	movl	%ecx, %r14d
	movq	%r15, -48(%rbp)
	movzbl	(%rax), %r15d
	orl	%ebx, %edx
	leal	1(%rcx), %ebx
	movb	%r15b, 0(%r13,%r14)
	cmpl	$3, %ecx
	je	.L59
	cmpq	-48(%rbp), %r9
	jbe	.L60
	movzbl	1(%rax), %r14d
	sall	$8, %edx
	movl	%r14d, %r15d
	orl	%r14d, %edx
	leaq	2(%rax), %r14
	movb	%r15b, 0(%r13,%rbx)
	cmpl	$2, %ecx
	je	.L61
	cmpq	%r14, %r9
	jbe	.L62
	movzbl	2(%rax), %ebx
	sall	$8, %edx
	addq	$3, %rax
	movb	%bl, 3(%r13)
	orl	%ebx, %edx
	movl	$4, %ebx
.L42:
	cmpl	$1114111, %edx
	ja	.L48
	movl	%edx, %ecx
	andl	$-2048, %ecx
	cmpl	$55296, %ecx
	je	.L48
	leaq	2(%r10), %rcx
	leaq	4(%r12), %r14
	cmpl	$65535, %edx
	ja	.L49
	movw	%dx, (%r10)
	movl	-52(%rbp), %edx
	movq	%rcx, %r10
	movl	%edx, (%r12)
	movq	%r14, %r12
.L50:
	addl	%ebx, -52(%rbp)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$15, (%rsi)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$4, %ebx
.L76:
	movl	$0, -52(%rbp)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L48:
	movq	8(%rdi), %rdx
	movb	%bl, 64(%rdx)
	movl	$12, (%rsi)
	jmp	.L53
.L62:
	movq	%r14, %rax
	movl	$3, %ecx
.L47:
	movq	8(%rdi), %r13
	leal	1(%rdx), %r14d
.L46:
	cmpq	%r10, %r11
	movl	%r14d, 72(%r13)
	movb	%cl, 64(%r13)
	setbe	%dl
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L59:
	movq	-48(%rbp), %rax
	jmp	.L42
.L49:
	movl	%edx, %r13d
	movl	-52(%rbp), %r15d
	andw	$1023, %dx
	shrl	$10, %r13d
	orw	$-9216, %dx
	subw	$10304, %r13w
	movw	%r13w, (%r10)
	movl	%r15d, (%r12)
	cmpq	%rcx, %r11
	jbe	.L51
	movw	%dx, 2(%r10)
	addq	$8, %r12
	addq	$4, %r10
	movl	%r15d, -4(%r12)
	jmp	.L50
.L60:
	movq	-48(%rbp), %rax
	movl	%ebx, %ecx
	jmp	.L47
.L61:
	movl	$4, %ebx
	movq	%r14, %rax
	jmp	.L42
.L51:
	movq	8(%rdi), %r8
	movq	%r14, %r12
	movq	%rcx, %r10
	movw	%dx, 144(%r8)
	movb	$1, 93(%r8)
	movl	$15, (%rsi)
	jmp	.L53
	.cfi_endproc
.LFE2114:
	.size	T_UConverter_toUnicode_UTF32_BE_OFFSET_LOGIC, .-T_UConverter_toUnicode_UTF32_BE_OFFSET_LOGIC
	.p2align 4
	.type	T_UConverter_toUnicode_UTF32_LE, @function
T_UConverter_toUnicode_UTF32_LE:
.LFB2118:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	32(%rdi), %r9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	24(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	40(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movzbl	64(%rbx), %edx
	testb	%dl, %dl
	jle	.L80
	movl	$1, %ecx
	cmpq	%r12, %r9
	jb	.L113
.L81:
	cmpq	%r8, %rax
	jnb	.L92
	testb	%cl, %cl
	je	.L92
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L114
.L92:
	popq	%rbx
	popq	%r12
	movq	%r9, 32(%rdi)
	popq	%r13
	popq	%r14
	movq	%rax, 16(%rdi)
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movb	$0, 64(%rbx)
	movq	8(%rdi), %rcx
	movsbl	%dl, %r11d
	movl	72(%rcx), %r13d
	movl	$0, 72(%rcx)
	leal	-1(%r13), %r10d
	cmpb	$3, %dl
	jg	.L82
	cmpq	%r8, %rax
	jnb	.L86
	movzbl	(%rax), %edx
	leal	0(,%r11,8), %ecx
	leaq	65(%rbx), %r13
	addq	$1, %rax
	movl	%edx, %r14d
	sall	%cl, %edx
	movl	%r11d, %ecx
	orl	%edx, %r10d
	movb	%r14b, 65(%rbx,%rcx)
	leal	1(%r11), %edx
	cmpl	$3, %r11d
	jne	.L93
	movl	$4, %r11d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L115:
	movzbl	2(%rax), %edx
	movl	$4, %r11d
	addq	$3, %rax
	movl	%edx, %ecx
	movb	%dl, 3(%r13)
	sall	$24, %ecx
	orl	%ecx, %r10d
.L82:
	cmpl	$1114111, %r10d
	ja	.L88
	movl	%r10d, %edx
	andl	$-2048, %edx
	cmpl	$55296, %edx
	je	.L88
	leaq	2(%r9), %rdx
	cmpl	$65535, %r10d
	ja	.L89
	movw	%r10w, (%r9)
	movq	%rdx, %r9
	.p2align 4,,10
	.p2align 3
.L80:
	cmpq	%r12, %r9
	setnb	%dl
	movl	%edx, %ecx
	cmpq	%r8, %rax
	jnb	.L92
	testb	%dl, %dl
	jne	.L81
	movzbl	(%rax), %r10d
	leaq	65(%rbx), %r13
	addq	$1, %rax
	movl	$1, %edx
	movb	%r10b, 65(%rbx)
.L93:
	cmpq	%r8, %rax
	jnb	.L87
	movzbl	(%rax), %r11d
	leal	0(,%rdx,8), %ecx
	leaq	1(%rax), %r15
	movl	%r11d, %r14d
	sall	%cl, %r11d
	movl	%edx, %ecx
	orl	%r11d, %r10d
	movb	%r14b, 0(%r13,%rcx)
	leal	1(%rdx), %r11d
	cmpl	$3, %edx
	je	.L96
	cmpq	%r15, %r8
	jbe	.L97
	movzbl	1(%rax), %r14d
	leal	0(,%r11,8), %ecx
	movl	%r14d, %r15d
	sall	%cl, %r14d
	leaq	2(%rax), %rcx
	movb	%r15b, 0(%r13,%r11)
	orl	%r14d, %r10d
	cmpl	$2, %edx
	je	.L98
	cmpq	%rcx, %r8
	ja	.L115
	movq	%rcx, %rax
	movl	$3, %edx
.L87:
	movq	8(%rdi), %rcx
	leal	1(%r10), %r13d
.L86:
	cmpq	%r9, %r12
	movl	%r13d, 72(%rcx)
	movb	%dl, 64(%rcx)
	setbe	%cl
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$15, (%rsi)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L88:
	movq	8(%rdi), %rdx
	movb	%r11b, 64(%rdx)
	movl	$12, (%rsi)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r15, %rax
	jmp	.L82
.L89:
	movl	%r10d, %ecx
	andw	$1023, %r10w
	shrl	$10, %ecx
	orw	$-9216, %r10w
	subw	$10304, %cx
	movw	%cx, (%r9)
	cmpq	%rdx, %r12
	jbe	.L90
	movw	%r10w, 2(%r9)
	addq	$4, %r9
	jmp	.L80
.L97:
	movl	%r11d, %edx
	movq	%r15, %rax
	jmp	.L87
.L98:
	movl	$4, %r11d
	movq	%rcx, %rax
	jmp	.L82
.L90:
	movq	8(%rdi), %rcx
	movq	%rdx, %r9
	movw	%r10w, 144(%rcx)
	movb	$1, 93(%rcx)
	movl	$15, (%rsi)
	jmp	.L92
	.cfi_endproc
.LFE2118:
	.size	T_UConverter_toUnicode_UTF32_LE, .-T_UConverter_toUnicode_UTF32_LE
	.p2align 4
	.type	T_UConverter_toUnicode_UTF32_LE_OFFSET_LOGIC, @function
T_UConverter_toUnicode_UTF32_LE_OFFSET_LOGIC:
.LFB2119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r8
	movq	16(%rdi), %rax
	movq	32(%rdi), %r10
	movq	48(%rdi), %r13
	movzbl	64(%r8), %edx
	movq	24(%rdi), %r9
	movq	40(%rdi), %r12
	testb	%dl, %dl
	jle	.L132
	movl	$1, %ecx
	cmpq	%r12, %r10
	jb	.L154
.L118:
	cmpq	%r9, %rax
	jnb	.L130
	testb	%cl, %cl
	je	.L130
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L155
.L130:
	movq	%r13, 48(%rdi)
	movq	%r10, 32(%rdi)
	movq	%rax, 16(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movb	$0, 64(%r8)
	movq	8(%rdi), %rcx
	movsbl	%dl, %ebx
	movl	72(%rcx), %r14d
	movl	$0, 72(%rcx)
	leal	-1(%r14), %r11d
	cmpb	$3, %dl
	jg	.L153
	cmpq	%r9, %rax
	jnb	.L123
	movzbl	(%rax), %edx
	leal	0(,%rbx,8), %ecx
	leaq	65(%r8), %r14
	addq	$1, %rax
	movl	%edx, %r15d
	sall	%cl, %edx
	movl	%ebx, %ecx
	orl	%edx, %r11d
	movb	%r15b, 65(%r8,%rcx)
	leal	1(%rbx), %edx
	cmpl	$3, %ebx
	je	.L135
	movl	$0, -52(%rbp)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L132:
	movl	$0, -52(%rbp)
.L117:
	cmpq	%r12, %r10
	setnb	%dl
	movl	%edx, %ecx
	cmpq	%r9, %rax
	jnb	.L130
	testb	%dl, %dl
	jne	.L118
	movzbl	(%rax), %r11d
	leaq	65(%r8), %r14
	addq	$1, %rax
	movl	$1, %edx
	movb	%r11b, 65(%r8)
.L131:
	cmpq	%rax, %r9
	jbe	.L124
	movzbl	(%rax), %ebx
	leal	0(,%rdx,8), %ecx
	movzbl	(%rax), %r15d
	sall	%cl, %ebx
	leaq	1(%rax), %rcx
	movq	%rcx, -48(%rbp)
	movl	%edx, %ecx
	orl	%ebx, %r11d
	leal	1(%rdx), %ebx
	movb	%r15b, (%r14,%rcx)
	cmpl	$3, %edx
	je	.L136
	cmpq	-48(%rbp), %r9
	jbe	.L137
	movzbl	1(%rax), %r15d
	leal	0(,%rbx,8), %ecx
	sall	%cl, %r15d
	leaq	2(%rax), %rcx
	orl	%r15d, %r11d
	movzbl	1(%rax), %r15d
	movb	%r15b, (%r14,%rbx)
	cmpl	$2, %edx
	je	.L138
	cmpq	%rcx, %r9
	jbe	.L139
	movzbl	2(%rax), %edx
	movl	$4, %ebx
	addq	$3, %rax
	movl	%edx, %ecx
	movb	%dl, 3(%r14)
	sall	$24, %ecx
	orl	%ecx, %r11d
.L119:
	cmpl	$1114111, %r11d
	ja	.L125
	movl	%r11d, %edx
	andl	$-2048, %edx
	cmpl	$55296, %edx
	je	.L125
	leaq	2(%r10), %rdx
	leaq	4(%r13), %r14
	cmpl	$65535, %r11d
	ja	.L126
	movl	-52(%rbp), %ecx
	movw	%r11w, (%r10)
	movq	%rdx, %r10
	movl	%ecx, 0(%r13)
	movq	%r14, %r13
.L127:
	addl	%ebx, -52(%rbp)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$15, (%rsi)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$4, %ebx
.L153:
	movl	$0, -52(%rbp)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L125:
	movq	8(%rdi), %rdx
	movb	%bl, 64(%rdx)
	movl	$12, (%rsi)
	jmp	.L130
.L139:
	movq	%rcx, %rax
	movl	$3, %edx
.L124:
	movq	8(%rdi), %rcx
	leal	1(%r11), %r14d
.L123:
	cmpq	%r10, %r12
	movl	%r14d, 72(%rcx)
	movb	%dl, 64(%rcx)
	setbe	%cl
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L136:
	movq	-48(%rbp), %rax
	jmp	.L119
.L126:
	movl	%r11d, %ecx
	andw	$1023, %r11w
	shrl	$10, %ecx
	orw	$-9216, %r11w
	subw	$10304, %cx
	movw	%cx, (%r10)
	movl	-52(%rbp), %ecx
	movl	%ecx, 0(%r13)
	cmpq	%rdx, %r12
	jbe	.L128
	movw	%r11w, 2(%r10)
	addq	$8, %r13
	addq	$4, %r10
	movl	%ecx, -4(%r13)
	jmp	.L127
.L137:
	movq	-48(%rbp), %rax
	movl	%ebx, %edx
	jmp	.L124
.L138:
	movl	$4, %ebx
	movq	%rcx, %rax
	jmp	.L119
.L128:
	movq	8(%rdi), %rcx
	movq	%r14, %r13
	movq	%rdx, %r10
	movw	%r11w, 144(%rcx)
	movb	$1, 93(%rcx)
	movl	$15, (%rsi)
	jmp	.L130
	.cfi_endproc
.LFE2119:
	.size	T_UConverter_toUnicode_UTF32_LE_OFFSET_LOGIC, .-T_UConverter_toUnicode_UTF32_LE_OFFSET_LOGIC
	.p2align 4
	.type	_UTF32Reset, @function
_UTF32Reset:
.LFB2123:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	jle	.L157
.L159:
	movl	$1, 80(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$0, 76(%rdi)
	jne	.L159
	ret
	.cfi_endproc
.LFE2123:
	.size	_UTF32Reset, .-_UTF32Reset
	.p2align 4
	.type	_UTF32Open, @function
_UTF32Open:
.LFB2124:
	.cfi_startproc
	endbr64
	movabsq	$4294967296, %rax
	movq	%rax, 76(%rdi)
	ret
	.cfi_endproc
.LFE2124:
	.size	_UTF32Open, .-_UTF32Open
	.p2align 4
	.type	_UTF32ToUnicodeWithOffsets, @function
_UTF32ToUnicodeWithOffsets:
.LFB2125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.L167(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movl	$0, -52(%rbp)
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rbx
	movq	48(%rdi), %r12
	movl	76(%r13), %r14d
.L162:
	cmpq	%rdx, %rbx
	jbe	.L163
.L212:
	movl	(%rsi), %r10d
	movl	%r14d, %ecx
.L204:
	testl	%r10d, %r10d
	jg	.L165
	cmpl	$9, %r14d
	ja	.L204
	movslq	(%r15,%rcx,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L167:
	.long	.L170-.L167
	.long	.L169-.L167
	.long	.L169-.L167
	.long	.L169-.L167
	.long	.L204-.L167
	.long	.L169-.L167
	.long	.L169-.L167
	.long	.L169-.L167
	.long	.L168-.L167
	.long	.L166-.L167
	.text
.L166:
	movq	%rdx, 16(%rdi)
	testq	%r12, %r12
	je	.L211
	call	T_UConverter_toUnicode_UTF32_LE_OFFSET_LOGIC
.L181:
	movq	16(%rdi), %rdx
	cmpq	%rdx, %rbx
	ja	.L212
.L163:
	testq	%r12, %r12
	je	.L173
	movl	-52(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L173
	movq	48(%rdi), %r11
	cmpq	%r11, %r12
	jnb	.L173
.L193:
	leaq	-1(%r11), %rcx
	movq	%r12, %rax
	subq	%r12, %rcx
	movq	%rcx, %r10
	shrq	$2, %r10
	addq	$1, %r10
	cmpq	$11, %rcx
	jbe	.L182
	movq	%r10, %rcx
	movd	-52(%rbp), %xmm2
	shrq	$2, %rcx
	salq	$4, %rcx
	pshufd	$0, %xmm2, %xmm1
	addq	%r12, %rcx
	.p2align 4,,10
	.p2align 3
.L183:
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	paddd	%xmm1, %xmm0
	movups	%xmm0, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L183
	movq	%r10, %rax
	andq	$-4, %rax
	leaq	(%r12,%rax,4), %r12
	cmpq	%rax, %r10
	je	.L173
.L182:
	leaq	4(%r12), %rax
	movl	-52(%rbp), %ecx
	addl	%ecx, (%r12)
	cmpq	%rax, %r11
	jbe	.L173
	leaq	8(%r12), %rax
	addl	%ecx, 4(%r12)
	cmpq	%rax, %r11
	jbe	.L173
	addl	%ecx, 8(%r12)
.L173:
	movq	%rdx, 16(%rdi)
	cmpq	%rdx, %rbx
	je	.L213
.L185:
	movl	%r14d, 76(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L168:
	.cfi_restore_state
	movq	%rdx, 16(%rdi)
	testq	%r12, %r12
	je	.L214
	call	T_UConverter_toUnicode_UTF32_BE_OFFSET_LOGIC
	jmp	.L181
.L170:
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L194
	movl	$8, %r14d
	cmpb	$-1, %al
	jne	.L162
	movl	$5, %r14d
	jmp	.L174
.L169:
	movslq	%r14d, %rax
	leaq	_ZL8utf32BOM(%rip), %rcx
	movzbl	(%rcx,%rax), %eax
	cmpb	%al, (%rdx)
	jne	.L176
	addl	$1, %r14d
	addq	$1, %rdx
	cmpl	$4, %r14d
	je	.L215
	cmpl	$8, %r14d
	jne	.L162
	movl	%edx, %eax
	subl	16(%rdi), %eax
	movl	$9, %r14d
	movl	%eax, -52(%rbp)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$1, %r14d
.L174:
	addq	$1, %rdx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L176:
	movq	16(%rdi), %rcx
	movl	%r14d, %eax
	andl	$3, %eax
	subq	%rcx, %rdx
	cmpl	%edx, %eax
	jne	.L216
	movq	%rcx, %rdx
	movl	$8, %r14d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L211:
	call	T_UConverter_toUnicode_UTF32_LE
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L214:
	call	T_UConverter_toUnicode_UTF32_BE
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L216:
	andl	$4, %r14d
	subl	%edx, %eax
	movq	%rcx, -64(%rbp)
	movzbl	2(%rdi), %r10d
	leaq	_ZL8utf32BOM(%rip), %rcx
	movslq	%r14d, %r11
	cltq
	movb	$0, 2(%rdi)
	addq	%rcx, %r11
	movb	%r10b, -53(%rbp)
	movl	$8, %r14d
	addq	%r11, %rax
	movq	%r11, 16(%rdi)
	movq	%rax, 24(%rdi)
	call	T_UConverter_toUnicode_UTF32_BE
	movzbl	-53(%rbp), %r10d
	movq	-64(%rbp), %rcx
	movq	%rbx, 24(%rdi)
	movb	%r10b, 2(%rdi)
	movq	%rcx, %rdx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L213:
	cmpb	$0, 2(%rdi)
	je	.L185
	cmpl	$8, %r14d
	je	.L186
	cmpl	$9, %r14d
	je	.L187
	testl	%r14d, %r14d
	je	.L185
	movl	%r14d, %eax
	leaq	_ZL8utf32BOM(%rip), %rdx
	andl	$3, %r14d
	andl	$4, %eax
	movq	%rbx, %xmm0
	cltq
	punpcklqdq	%xmm0, %xmm0
	addq	%rdx, %rax
	movq	%rax, 16(%rdi)
	addq	%r14, %rax
	movl	$8, %r14d
	movq	%rax, 24(%rdi)
	call	T_UConverter_toUnicode_UTF32_BE
	movups	%xmm0, 16(%rdi)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L165:
	testq	%r12, %r12
	je	.L192
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	je	.L192
	movq	48(%rdi), %r11
	cmpq	%r11, %r12
	jb	.L193
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%rdx, 16(%rdi)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L215:
	movl	%edx, %eax
	subl	16(%rdi), %eax
	movl	$8, %r14d
	movl	%eax, -52(%rbp)
	jmp	.L162
.L186:
	call	T_UConverter_toUnicode_UTF32_BE
	jmp	.L185
.L187:
	call	T_UConverter_toUnicode_UTF32_LE
	jmp	.L185
	.cfi_endproc
.LFE2125:
	.size	_UTF32ToUnicodeWithOffsets, .-_UTF32ToUnicodeWithOffsets
	.p2align 4
	.type	T_UConverter_getNextUChar_UTF32_BE, @function
T_UConverter_getNextUChar_UTF32_BE:
.LFB2117:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	cmpq	%rdx, %rax
	jbe	.L231
	subq	%rdx, %rax
	cmpl	$3, %eax
	jle	.L232
	movl	(%rdx), %eax
	leaq	4(%rdx), %rcx
	movq	%rcx, 16(%rdi)
	bswap	%eax
	cmpl	$1114111, %eax
	ja	.L223
	movl	%eax, %ecx
	andl	$-2048, %ecx
	cmpl	$55296, %ecx
	je	.L223
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	movq	8(%rdi), %rax
	movl	(%rdx), %edx
	movl	%edx, 65(%rax)
	movq	8(%rdi), %rax
	movb	$4, 64(%rax)
	movl	$65535, %eax
	movl	$12, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$8, (%rsi)
	movl	$65535, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	movq	8(%rdi), %rcx
	movslq	%eax, %r8
	leaq	65(%rcx), %r10
	testq	%r8, %r8
	je	.L222
	xorl	%ecx, %ecx
.L221:
	movzbl	(%rdx,%rcx), %r9d
	movb	%r9b, (%r10,%rcx)
	addq	$1, %rcx
	cmpq	%r8, %rcx
	jb	.L221
	movq	8(%rdi), %rcx
.L222:
	addq	%r8, %rdx
	movb	%al, 64(%rcx)
	movl	$65535, %eax
	movq	%rdx, 16(%rdi)
	movl	$11, (%rsi)
	ret
	.cfi_endproc
.LFE2117:
	.size	T_UConverter_getNextUChar_UTF32_BE, .-T_UConverter_getNextUChar_UTF32_BE
	.p2align 4
	.type	T_UConverter_getNextUChar_UTF32_LE, @function
T_UConverter_getNextUChar_UTF32_LE:
.LFB2122:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	cmpq	%rdx, %rax
	jbe	.L247
	subq	%rdx, %rax
	cmpl	$3, %eax
	jle	.L248
	movl	(%rdx), %eax
	leaq	4(%rdx), %rcx
	movq	%rcx, 16(%rdi)
	cmpl	$1114111, %eax
	ja	.L239
	movl	%eax, %ecx
	andl	$-2048, %ecx
	cmpl	$55296, %ecx
	je	.L239
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	movq	8(%rdi), %rax
	movl	(%rdx), %edx
	movl	%edx, 65(%rax)
	movq	8(%rdi), %rax
	movb	$4, 64(%rax)
	movl	$65535, %eax
	movl	$12, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	movl	$8, (%rsi)
	movl	$65535, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	movq	8(%rdi), %rcx
	movslq	%eax, %r9
	leaq	65(%rcx), %r10
	testq	%r9, %r9
	je	.L238
	xorl	%ecx, %ecx
.L237:
	movzbl	(%rdx,%rcx), %r8d
	movb	%r8b, (%r10,%rcx)
	addq	$1, %rcx
	cmpq	%r9, %rcx
	jb	.L237
	movq	8(%rdi), %rcx
.L238:
	addq	%r9, %rdx
	movb	%al, 64(%rcx)
	movl	$65535, %eax
	movq	%rdx, 16(%rdi)
	movl	$11, (%rsi)
	ret
	.cfi_endproc
.LFE2122:
	.size	T_UConverter_getNextUChar_UTF32_LE, .-T_UConverter_getNextUChar_UTF32_LE
	.p2align 4
	.type	T_UConverter_fromUnicode_UTF32_BE_OFFSET_LOGIC, @function
T_UConverter_fromUnicode_UTF32_BE_OFFSET_LOGIC:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	24(%rdi), %r14
	cmpq	%r14, %r12
	jnb	.L249
	movq	40(%rdi), %r15
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	%rsi, %r13
	cmpl	$1, 80(%rdi)
	je	.L281
.L252:
	movl	84(%rdi), %edx
	movq	32(%rbx), %rax
	movq	48(%rbx), %rcx
	testl	%edx, %edx
	jne	.L282
.L253:
	cmpq	%r15, %rax
	setnb	%sil
	cmpq	%r14, %r12
	jnb	.L261
	testb	%sil, %sil
	jne	.L255
	movzwl	(%r12), %esi
	leaq	2(%r12), %rdi
	movl	%esi, %r8d
	andl	$-2048, %r8d
	cmpl	$55296, %r8d
	je	.L283
.L256:
	movl	%esi, %r8d
	movl	%esi, %r10d
	movl	%esi, %r9d
	sarl	$16, %r8d
	sarl	$8, %r10d
	andl	$31, %r8d
	cmpq	%r15, %rax
	jb	.L263
	movq	8(%rbx), %r11
	movsbq	91(%r11), %rsi
	leal	1(%rsi), %r12d
	movb	%r12b, 91(%r11)
	movb	$0, 104(%r11,%rsi)
	movl	$15, 0(%r13)
.L264:
	movq	8(%rbx), %r11
	movsbq	91(%r11), %rsi
	leal	1(%rsi), %r12d
	movb	%r12b, 91(%r11)
	movb	%r8b, 104(%r11,%rsi)
	movl	$15, 0(%r13)
.L266:
	movq	8(%rbx), %r11
	movsbq	91(%r11), %rsi
	leal	1(%rsi), %r12d
	movb	%r12b, 91(%r11)
	movb	%r10b, 104(%r11,%rsi)
	movl	$15, 0(%r13)
.L268:
	movq	8(%rbx), %r10
	movsbq	91(%r10), %rsi
	leal	1(%rsi), %r11d
	movb	%r11b, 91(%r10)
	movb	%r9b, 104(%r10,%rsi)
	movl	$15, 0(%r13)
.L269:
	xorl	%esi, %esi
	testb	%r8b, %r8b
	movq	%rdi, %r12
	setne	%sil
	leal	1(%rdx,%rsi), %edx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L255:
	cmpq	%r14, %r12
	jnb	.L261
	testb	%sil, %sil
	je	.L261
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L261
	movl	$15, 0(%r13)
.L261:
	movq	%rax, 32(%rbx)
	movq	%r12, 16(%rbx)
	movq	%rcx, 48(%rbx)
.L249:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movl	$0, 84(%rdi)
	xorl	%r8d, %r8d
.L254:
	movzwl	(%r12), %esi
	movl	%esi, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	je	.L284
	movq	8(%rbx), %rsi
	movl	%edx, 84(%rsi)
	movl	$12, 0(%r13)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L283:
	movl	%esi, %r8d
	andl	$-1024, %r8d
	cmpl	$55296, %r8d
	je	.L285
	movq	8(%rbx), %rdx
	movq	%rdi, %r12
	movl	%esi, 84(%rdx)
	movl	$12, 0(%r13)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L281:
	pushq	%rsi
	leaq	32(%rbx), %rcx
	leaq	_ZZ46T_UConverter_fromUnicode_UTF32_BE_OFFSET_LOGICE3bom(%rip), %rsi
	movq	%r15, %r8
	pushq	$-1
	leaq	48(%rbx), %r9
	movl	$4, %edx
	call	ucnv_fromUWriteBytes_67@PLT
	movq	8(%rbx), %rdi
	movl	$0, 80(%rdi)
	popq	%rcx
	popq	%rsi
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L285:
.L258:
	endbr64
	cmpq	%rdi, %r14
	ja	.L286
	movq	8(%rbx), %rdx
	movl	%esi, 84(%rdx)
	cmpb	$0, 2(%rbx)
	je	.L262
	movl	$12, 0(%r13)
.L262:
	movq	%rdi, %r12
	movq	%rax, 32(%rbx)
	movq	%r12, 16(%rbx)
	movq	%rcx, 48(%rbx)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L284:
	subl	$55296, %edx
	leaq	2(%r12), %rdi
	sall	$10, %edx
	leal	9216(%rsi,%rdx), %esi
	movl	%r8d, %edx
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	1(%rax), %r11
	movb	$0, (%rax)
	leaq	4(%rcx), %r12
	movl	%edx, (%rcx)
	cmpq	%r11, %r15
	jbe	.L287
	leaq	2(%rax), %r11
	movb	%r8b, 1(%rax)
	leaq	8(%rcx), %r12
	movl	%edx, 4(%rcx)
	cmpq	%r15, %r11
	jnb	.L288
	leaq	3(%rax), %r11
	movb	%r10b, 2(%rax)
	leaq	12(%rcx), %r10
	movl	%edx, 8(%rcx)
	cmpq	%r11, %r15
	jbe	.L270
	movb	%sil, 3(%rax)
	addq	$16, %rcx
	addq	$4, %rax
	movl	%edx, -4(%rcx)
	jmp	.L269
.L286:
	movl	%edx, %r8d
	movq	%rdi, %r12
	movl	%esi, %edx
	jmp	.L254
.L270:
	movq	%r10, %rcx
	movq	%r11, %rax
	jmp	.L268
.L288:
	movq	%r11, %rax
	movq	%r12, %rcx
	jmp	.L266
.L287:
	movq	%r11, %rax
	movq	%r12, %rcx
	jmp	.L264
	.cfi_endproc
.LFE2116:
	.size	T_UConverter_fromUnicode_UTF32_BE_OFFSET_LOGIC, .-T_UConverter_fromUnicode_UTF32_BE_OFFSET_LOGIC
	.p2align 4
	.type	T_UConverter_fromUnicode_UTF32_BE, @function
T_UConverter_fromUnicode_UTF32_BE:
.LFB2115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	24(%rdi), %r14
	cmpq	%r14, %r12
	jnb	.L289
	movq	40(%rdi), %r15
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	%rsi, %r13
	cmpl	$1, 80(%rdi)
	je	.L321
.L292:
	movl	84(%rdi), %eax
	movq	32(%rbx), %rdx
	testl	%eax, %eax
	jne	.L322
.L293:
	cmpq	%r15, %rdx
	setnb	%al
	cmpq	%r14, %r12
	jnb	.L301
	testb	%al, %al
	jne	.L295
	movzwl	(%r12), %eax
	leaq	2(%r12), %rcx
	movl	%eax, %esi
	andl	$-2048, %esi
	cmpl	$55296, %esi
	je	.L323
.L296:
	movl	%eax, %esi
	movl	%eax, %r8d
	movl	%eax, %edi
	sarl	$16, %esi
	sarl	$8, %r8d
	andl	$31, %esi
	cmpq	%r15, %rdx
	jb	.L303
	movq	8(%rbx), %r9
	movsbq	91(%r9), %rax
	leal	1(%rax), %r10d
	movb	%r10b, 91(%r9)
	movb	$0, 104(%r9,%rax)
	movl	$15, 0(%r13)
.L304:
	movq	8(%rbx), %r9
	movsbq	91(%r9), %rax
	leal	1(%rax), %r10d
	movb	%r10b, 91(%r9)
	movb	%sil, 104(%r9,%rax)
	movl	$15, 0(%r13)
.L306:
	movq	8(%rbx), %rsi
	movsbq	91(%rsi), %rax
	leal	1(%rax), %r9d
	movb	%r9b, 91(%rsi)
	movb	%r8b, 104(%rsi,%rax)
	movl	$15, 0(%r13)
.L308:
	movq	8(%rbx), %rsi
	movsbq	91(%rsi), %rax
	leal	1(%rax), %r8d
	movb	%r8b, 91(%rsi)
	movb	%dil, 104(%rsi,%rax)
	movl	$15, 0(%r13)
.L309:
	movq	%rcx, %r12
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L295:
	cmpq	%r14, %r12
	jnb	.L301
	testb	%al, %al
	je	.L301
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L301
	movl	$15, 0(%r13)
.L301:
	movq	%rdx, 32(%rbx)
	movq	%r12, 16(%rbx)
.L289:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movl	$0, 84(%rdi)
.L294:
	movzwl	(%r12), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	je	.L324
	movq	8(%rbx), %rcx
	movl	%eax, 84(%rcx)
	movl	$12, 0(%r13)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L323:
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	je	.L325
	movq	8(%rbx), %rsi
	movq	%rcx, %r12
	movl	%eax, 84(%rsi)
	movl	$12, 0(%r13)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L321:
	pushq	%rsi
	leaq	32(%rbx), %rcx
	movl	$4, %edx
	leaq	48(%rbx), %r9
	pushq	$-1
	movq	%r15, %r8
	leaq	_ZZ33T_UConverter_fromUnicode_UTF32_BEE3bom(%rip), %rsi
	call	ucnv_fromUWriteBytes_67@PLT
	movq	8(%rbx), %rdi
	movl	$0, 80(%rdi)
	popq	%rdx
	popq	%rcx
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L325:
.L298:
	endbr64
	cmpq	%rcx, %r14
	ja	.L326
	movq	8(%rbx), %rsi
	movl	%eax, 84(%rsi)
	cmpb	$0, 2(%rbx)
	je	.L302
	movl	$12, 0(%r13)
.L302:
	movq	%rcx, %r12
	movq	%rdx, 32(%rbx)
	movq	%r12, 16(%rbx)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L324:
	subl	$55296, %eax
	sall	$10, %eax
	leal	9216(%rcx,%rax), %eax
	leaq	2(%r12), %rcx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L303:
	leaq	1(%rdx), %r9
	movb	$0, (%rdx)
	cmpq	%r9, %r15
	jbe	.L327
	leaq	2(%rdx), %r9
	movb	%sil, 1(%rdx)
	cmpq	%r15, %r9
	jnb	.L328
	leaq	3(%rdx), %rsi
	movb	%r8b, 2(%rdx)
	cmpq	%rsi, %r15
	jbe	.L310
	movb	%al, 3(%rdx)
	addq	$4, %rdx
	jmp	.L309
.L326:
	movq	%rcx, %r12
	jmp	.L294
.L310:
	movq	%rsi, %rdx
	jmp	.L308
.L328:
	movq	%r9, %rdx
	jmp	.L306
.L327:
	movq	%r9, %rdx
	jmp	.L304
	.cfi_endproc
.LFE2115:
	.size	T_UConverter_fromUnicode_UTF32_BE, .-T_UConverter_fromUnicode_UTF32_BE
	.p2align 4
	.type	T_UConverter_fromUnicode_UTF32_LE_OFFSET_LOGIC, @function
T_UConverter_fromUnicode_UTF32_LE_OFFSET_LOGIC:
.LFB2121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	24(%rdi), %r14
	cmpq	%r14, %r12
	jnb	.L329
	movq	40(%rdi), %r15
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	%rsi, %r13
	cmpl	$1, 80(%rdi)
	je	.L361
.L332:
	movl	84(%rdi), %edx
	movq	32(%rbx), %rax
	movq	48(%rbx), %rcx
	testl	%edx, %edx
	jne	.L362
.L333:
	cmpq	%r15, %rax
	setnb	%sil
	cmpq	%r14, %r12
	jnb	.L341
	testb	%sil, %sil
	jne	.L335
	movzwl	(%r12), %esi
	leaq	2(%r12), %rdi
	movl	%esi, %r8d
	andl	$-2048, %r8d
	cmpl	$55296, %r8d
	je	.L363
.L336:
	movl	%esi, %r8d
	movl	%esi, %r10d
	sarl	$16, %r8d
	sarl	$8, %r10d
	andl	$31, %r8d
	cmpq	%r15, %rax
	jb	.L343
	movq	8(%rbx), %r11
	movsbq	91(%r11), %r9
	leal	1(%r9), %r12d
	movb	%r12b, 91(%r11)
	movb	%sil, 104(%r11,%r9)
	movl	$15, 0(%r13)
.L344:
	movq	8(%rbx), %r9
	movsbq	91(%r9), %rsi
	leal	1(%rsi), %r11d
	movb	%r11b, 91(%r9)
	movb	%r10b, 104(%r9,%rsi)
	movl	$15, 0(%r13)
.L346:
	movq	8(%rbx), %r9
	movsbq	91(%r9), %rsi
	leal	1(%rsi), %r10d
	movb	%r10b, 91(%r9)
	movb	%r8b, 104(%r9,%rsi)
	movl	$15, 0(%r13)
.L348:
	movq	8(%rbx), %r9
	movsbq	91(%r9), %rsi
	leal	1(%rsi), %r10d
	movb	%r10b, 91(%r9)
	movb	$0, 104(%r9,%rsi)
	movl	$15, 0(%r13)
.L349:
	xorl	%esi, %esi
	testb	%r8b, %r8b
	movq	%rdi, %r12
	setne	%sil
	leal	1(%rdx,%rsi), %edx
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L335:
	cmpq	%r14, %r12
	jnb	.L341
	testb	%sil, %sil
	je	.L341
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L341
	movl	$15, 0(%r13)
.L341:
	movq	%rax, 32(%rbx)
	movq	%r12, 16(%rbx)
	movq	%rcx, 48(%rbx)
.L329:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movl	$0, 84(%rdi)
	xorl	%r8d, %r8d
.L334:
	movzwl	(%r12), %esi
	movl	%esi, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	je	.L364
	movq	8(%rbx), %rsi
	movl	%edx, 84(%rsi)
	movl	$12, 0(%r13)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L363:
	movl	%esi, %r8d
	andl	$-1024, %r8d
	cmpl	$55296, %r8d
	je	.L365
	movq	8(%rbx), %rdx
	movq	%rdi, %r12
	movl	%esi, 84(%rdx)
	movl	$12, 0(%r13)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L361:
	pushq	%rsi
	leaq	32(%rbx), %rcx
	leaq	_ZZ46T_UConverter_fromUnicode_UTF32_LE_OFFSET_LOGICE3bom(%rip), %rsi
	movq	%r15, %r8
	pushq	$-1
	leaq	48(%rbx), %r9
	movl	$4, %edx
	call	ucnv_fromUWriteBytes_67@PLT
	movq	8(%rbx), %rdi
	movl	$0, 80(%rdi)
	popq	%rcx
	popq	%rsi
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L365:
.L338:
	endbr64
	cmpq	%rdi, %r14
	ja	.L366
	movq	8(%rbx), %rdx
	movl	%esi, 84(%rdx)
	cmpb	$0, 2(%rbx)
	je	.L342
	movl	$12, 0(%r13)
.L342:
	movq	%rdi, %r12
	movq	%rax, 32(%rbx)
	movq	%r12, 16(%rbx)
	movq	%rcx, 48(%rbx)
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L364:
	subl	$55296, %edx
	leaq	2(%r12), %rdi
	sall	$10, %edx
	leal	9216(%rsi,%rdx), %esi
	movl	%r8d, %edx
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	1(%rax), %r9
	movb	%sil, (%rax)
	leaq	4(%rcx), %rsi
	movl	%edx, (%rcx)
	cmpq	%r9, %r15
	jbe	.L367
	leaq	2(%rax), %rsi
	movb	%r10b, 1(%rax)
	leaq	8(%rcx), %r9
	movl	%edx, 4(%rcx)
	cmpq	%r15, %rsi
	jnb	.L368
	leaq	3(%rax), %rsi
	movb	%r8b, 2(%rax)
	leaq	12(%rcx), %r9
	movl	%edx, 8(%rcx)
	cmpq	%rsi, %r15
	jbe	.L350
	movb	$0, 3(%rax)
	addq	$16, %rcx
	addq	$4, %rax
	movl	%edx, -4(%rcx)
	jmp	.L349
.L366:
	movl	%edx, %r8d
	movq	%rdi, %r12
	movl	%esi, %edx
	jmp	.L334
.L350:
	movq	%r9, %rcx
	movq	%rsi, %rax
	jmp	.L348
.L368:
	movq	%rsi, %rax
	movq	%r9, %rcx
	jmp	.L346
.L367:
	movq	%r9, %rax
	movq	%rsi, %rcx
	jmp	.L344
	.cfi_endproc
.LFE2121:
	.size	T_UConverter_fromUnicode_UTF32_LE_OFFSET_LOGIC, .-T_UConverter_fromUnicode_UTF32_LE_OFFSET_LOGIC
	.p2align 4
	.type	T_UConverter_fromUnicode_UTF32_LE, @function
T_UConverter_fromUnicode_UTF32_LE:
.LFB2120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	24(%rdi), %r14
	cmpq	%r14, %r12
	jnb	.L369
	movq	40(%rdi), %r15
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	%rsi, %r13
	cmpl	$1, 80(%rdi)
	je	.L401
.L372:
	movl	84(%rdi), %eax
	movq	32(%rbx), %rdx
	testl	%eax, %eax
	jne	.L402
.L373:
	cmpq	%r15, %rdx
	setnb	%al
	cmpq	%r14, %r12
	jnb	.L381
	testb	%al, %al
	jne	.L375
	movzwl	(%r12), %eax
	leaq	2(%r12), %rcx
	movl	%eax, %esi
	andl	$-2048, %esi
	cmpl	$55296, %esi
	je	.L403
.L376:
	movl	%eax, %esi
	movl	%eax, %r8d
	sarl	$16, %esi
	sarl	$8, %r8d
	andl	$31, %esi
	cmpq	%r15, %rdx
	jb	.L383
	movq	8(%rbx), %r9
	movsbq	91(%r9), %rdi
	leal	1(%rdi), %r10d
	movb	%r10b, 91(%r9)
	movb	%al, 104(%r9,%rdi)
	movl	$15, 0(%r13)
.L384:
	movq	8(%rbx), %rdi
	movsbq	91(%rdi), %rax
	leal	1(%rax), %r9d
	movb	%r9b, 91(%rdi)
	movb	%r8b, 104(%rdi,%rax)
	movl	$15, 0(%r13)
.L386:
	movq	8(%rbx), %rdi
	movsbq	91(%rdi), %rax
	leal	1(%rax), %r8d
	movb	%r8b, 91(%rdi)
	movb	%sil, 104(%rdi,%rax)
	movl	$15, 0(%r13)
.L388:
	movq	8(%rbx), %rsi
	movsbq	91(%rsi), %rax
	leal	1(%rax), %edi
	movb	%dil, 91(%rsi)
	movb	$0, 104(%rsi,%rax)
	movl	$15, 0(%r13)
.L389:
	movq	%rcx, %r12
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L375:
	cmpq	%r14, %r12
	jnb	.L381
	testb	%al, %al
	je	.L381
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L381
	movl	$15, 0(%r13)
.L381:
	movq	%rdx, 32(%rbx)
	movq	%r12, 16(%rbx)
.L369:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	movl	$0, 84(%rdi)
.L374:
	movzwl	(%r12), %ecx
	movl	%ecx, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	je	.L404
	movq	8(%rbx), %rcx
	movl	%eax, 84(%rcx)
	movl	$12, 0(%r13)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L403:
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	je	.L405
	movq	8(%rbx), %rsi
	movq	%rcx, %r12
	movl	%eax, 84(%rsi)
	movl	$12, 0(%r13)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L401:
	pushq	%rsi
	leaq	32(%rbx), %rcx
	movl	$4, %edx
	leaq	48(%rbx), %r9
	pushq	$-1
	movq	%r15, %r8
	leaq	_ZZ33T_UConverter_fromUnicode_UTF32_LEE3bom(%rip), %rsi
	call	ucnv_fromUWriteBytes_67@PLT
	movq	8(%rbx), %rdi
	movl	$0, 80(%rdi)
	popq	%rdx
	popq	%rcx
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L405:
.L378:
	endbr64
	cmpq	%rcx, %r14
	ja	.L406
	movq	8(%rbx), %rsi
	movl	%eax, 84(%rsi)
	cmpb	$0, 2(%rbx)
	je	.L382
	movl	$12, 0(%r13)
.L382:
	movq	%rcx, %r12
	movq	%rdx, 32(%rbx)
	movq	%r12, 16(%rbx)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L404:
	subl	$55296, %eax
	sall	$10, %eax
	leal	9216(%rcx,%rax), %eax
	leaq	2(%r12), %rcx
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L383:
	leaq	1(%rdx), %rdi
	movb	%al, (%rdx)
	cmpq	%rdi, %r15
	jbe	.L407
	leaq	2(%rdx), %rax
	movb	%r8b, 1(%rdx)
	cmpq	%r15, %rax
	jnb	.L408
	leaq	3(%rdx), %rax
	movb	%sil, 2(%rdx)
	cmpq	%rax, %r15
	jbe	.L390
	movb	$0, 3(%rdx)
	addq	$4, %rdx
	jmp	.L389
.L406:
	movq	%rcx, %r12
	jmp	.L374
.L390:
	movq	%rax, %rdx
	jmp	.L388
.L408:
	movq	%rax, %rdx
	jmp	.L386
.L407:
	movq	%rdi, %rdx
	jmp	.L384
	.cfi_endproc
.LFE2120:
	.size	T_UConverter_fromUnicode_UTF32_LE, .-T_UConverter_fromUnicode_UTF32_LE
	.p2align 4
	.type	_UTF32GetNextUChar, @function
_UTF32GetNextUChar:
.LFB2126:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movl	76(%rdx), %eax
	cmpl	$8, %eax
	je	.L410
	cmpl	$9, %eax
	jne	.L440
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	cmpq	%rax, %rcx
	jnb	.L418
	subq	%rcx, %rax
	cmpl	$3, %eax
	jle	.L441
	movl	(%rcx), %eax
	leaq	4(%rcx), %r8
	movq	%r8, 16(%rdi)
	cmpl	$1114111, %eax
	ja	.L422
.L439:
	movl	%eax, %r8d
	andl	$-2048, %r8d
	cmpl	$55296, %r8d
	je	.L422
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	movl	$-9, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	cmpq	%rax, %rcx
	jnb	.L418
	subq	%rcx, %rax
	cmpl	$3, %eax
	jle	.L442
	movl	(%rcx), %eax
	leaq	4(%rcx), %r8
	movq	%r8, 16(%rdi)
	bswap	%eax
	cmpl	$1114111, %eax
	jbe	.L439
.L422:
	movl	(%rcx), %eax
	movl	%eax, 65(%rdx)
	movq	8(%rdi), %rax
	movb	$4, 64(%rax)
	movl	$65535, %eax
	movl	$12, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	movl	$8, (%rsi)
	movl	$65535, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	movslq	%eax, %r9
	leaq	65(%rdx), %r10
	testq	%r9, %r9
	je	.L421
	xorl	%edx, %edx
.L420:
	movzbl	(%rcx,%rdx), %r8d
	movb	%r8b, (%r10,%rdx)
	addq	$1, %rdx
	cmpq	%r9, %rdx
	jb	.L420
.L437:
	movq	8(%rdi), %rdx
.L421:
	addq	%r9, %rcx
	movb	%al, 64(%rdx)
	movl	$65535, %eax
	movq	%rcx, 16(%rdi)
	movl	$11, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	movslq	%eax, %r9
	leaq	65(%rdx), %r10
	testq	%r9, %r9
	je	.L421
	xorl	%edx, %edx
.L415:
	movzbl	(%rcx,%rdx), %r8d
	movb	%r8b, (%r10,%rdx)
	addq	$1, %rdx
	cmpq	%r9, %rdx
	jb	.L415
	jmp	.L437
	.cfi_endproc
.LFE2126:
	.size	_UTF32GetNextUChar, .-_UTF32GetNextUChar
	.globl	_UTF32Data_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_UTF32Data_67, @object
	.size	_UTF32Data_67, 296
_UTF32Data_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL16_UTF32StaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL10_UTF32Impl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL16_UTF32StaticData, @object
	.size	_ZL16_UTF32StaticData, 100
_ZL16_UTF32StaticData:
	.long	100
	.string	"UTF-32"
	.zero	53
	.long	1236
	.byte	0
	.byte	30
	.byte	4
	.byte	4
	.string	"\375\377"
	.zero	1
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL10_UTF32Impl, @object
	.size	_ZL10_UTF32Impl, 144
_ZL10_UTF32Impl:
	.long	30
	.zero	4
	.quad	0
	.quad	0
	.quad	_UTF32Open
	.quad	0
	.quad	_UTF32Reset
	.quad	_UTF32ToUnicodeWithOffsets
	.quad	_UTF32ToUnicodeWithOffsets
	.quad	T_UConverter_fromUnicode_UTF32_LE
	.quad	T_UConverter_fromUnicode_UTF32_LE_OFFSET_LOGIC
	.quad	_UTF32GetNextUChar
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ucnv_getNonSurrogateUnicodeSet_67
	.quad	0
	.quad	0
	.section	.rodata
	.align 8
	.type	_ZL8utf32BOM, @object
	.size	_ZL8utf32BOM, 8
_ZL8utf32BOM:
	.string	""
	.string	""
	.string	"\376\377\377\376"
	.zero	1
	.globl	_UTF32LEData_67
	.section	.data.rel.ro.local
	.align 32
	.type	_UTF32LEData_67, @object
	.size	_UTF32LEData_67, 296
_UTF32LEData_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_UTF32LEStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_UTF32LEImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_UTF32LEStaticData, @object
	.size	_ZL18_UTF32LEStaticData, 100
_ZL18_UTF32LEStaticData:
	.long	100
	.string	"UTF-32LE"
	.zero	51
	.long	1234
	.byte	0
	.byte	8
	.byte	4
	.byte	4
	.string	"\375\377"
	.zero	1
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL12_UTF32LEImpl, @object
	.size	_ZL12_UTF32LEImpl, 144
_ZL12_UTF32LEImpl:
	.long	8
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	T_UConverter_toUnicode_UTF32_LE
	.quad	T_UConverter_toUnicode_UTF32_LE_OFFSET_LOGIC
	.quad	T_UConverter_fromUnicode_UTF32_LE
	.quad	T_UConverter_fromUnicode_UTF32_LE_OFFSET_LOGIC
	.quad	T_UConverter_getNextUChar_UTF32_LE
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ucnv_getNonSurrogateUnicodeSet_67
	.quad	0
	.quad	0
	.section	.rodata
	.type	_ZZ46T_UConverter_fromUnicode_UTF32_LE_OFFSET_LOGICE3bom, @object
	.size	_ZZ46T_UConverter_fromUnicode_UTF32_LE_OFFSET_LOGICE3bom, 4
_ZZ46T_UConverter_fromUnicode_UTF32_LE_OFFSET_LOGICE3bom:
	.string	"\377\376"
	.string	""
	.type	_ZZ33T_UConverter_fromUnicode_UTF32_LEE3bom, @object
	.size	_ZZ33T_UConverter_fromUnicode_UTF32_LEE3bom, 4
_ZZ33T_UConverter_fromUnicode_UTF32_LEE3bom:
	.string	"\377\376"
	.string	""
	.globl	_UTF32BEData_67
	.section	.data.rel.ro.local
	.align 32
	.type	_UTF32BEData_67, @object
	.size	_UTF32BEData_67, 296
_UTF32BEData_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_UTF32BEStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_UTF32BEImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_UTF32BEStaticData, @object
	.size	_ZL18_UTF32BEStaticData, 100
_ZL18_UTF32BEStaticData:
	.long	100
	.string	"UTF-32BE"
	.zero	51
	.long	1232
	.byte	0
	.byte	7
	.byte	4
	.byte	4
	.string	""
	.string	""
	.ascii	"\377\375"
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL12_UTF32BEImpl, @object
	.size	_ZL12_UTF32BEImpl, 144
_ZL12_UTF32BEImpl:
	.long	7
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	T_UConverter_toUnicode_UTF32_BE
	.quad	T_UConverter_toUnicode_UTF32_BE_OFFSET_LOGIC
	.quad	T_UConverter_fromUnicode_UTF32_BE
	.quad	T_UConverter_fromUnicode_UTF32_BE_OFFSET_LOGIC
	.quad	T_UConverter_getNextUChar_UTF32_BE
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ucnv_getNonSurrogateUnicodeSet_67
	.quad	0
	.quad	0
	.section	.rodata
	.type	_ZZ46T_UConverter_fromUnicode_UTF32_BE_OFFSET_LOGICE3bom, @object
	.size	_ZZ46T_UConverter_fromUnicode_UTF32_BE_OFFSET_LOGICE3bom, 4
_ZZ46T_UConverter_fromUnicode_UTF32_BE_OFFSET_LOGICE3bom:
	.string	""
	.string	""
	.ascii	"\376\377"
	.type	_ZZ33T_UConverter_fromUnicode_UTF32_BEE3bom, @object
	.size	_ZZ33T_UConverter_fromUnicode_UTF32_BEE3bom, 4
_ZZ33T_UConverter_fromUnicode_UTF32_BEE3bom:
	.string	""
	.string	""
	.ascii	"\376\377"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
