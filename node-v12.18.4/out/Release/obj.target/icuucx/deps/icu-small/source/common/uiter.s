	.file	"uiter.cpp"
	.text
	.p2align 4
	.type	noopGetIndex, @function
noopGetIndex:
.LFB2288:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2288:
	.size	noopGetIndex, .-noopGetIndex
	.p2align 4
	.type	noopMove, @function
noopMove:
.LFB2289:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2289:
	.size	noopMove, .-noopMove
	.p2align 4
	.type	noopHasNext, @function
noopHasNext:
.LFB2290:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2290:
	.size	noopHasNext, .-noopHasNext
	.p2align 4
	.type	noopCurrent, @function
noopCurrent:
.LFB2291:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2291:
	.size	noopCurrent, .-noopCurrent
	.p2align 4
	.type	noopGetState, @function
noopGetState:
.LFB2292:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2292:
	.size	noopGetState, .-noopGetState
	.p2align 4
	.type	noopSetState, @function
noopSetState:
.LFB2293:
	.cfi_startproc
	endbr64
	movl	$16, (%rdx)
	ret
	.cfi_endproc
.LFE2293:
	.size	noopSetState, .-noopSetState
	.p2align 4
	.type	stringIteratorGetIndex, @function
stringIteratorGetIndex:
.LFB2294:
	.cfi_startproc
	endbr64
	cmpl	$4, %esi
	ja	.L9
	leaq	.L11(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L11:
	.long	.L15-.L11
	.long	.L14-.L11
	.long	.L13-.L11
	.long	.L16-.L11
	.long	.L10-.L11
	.text
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	20(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	8(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movl	12(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	16(%rdi), %eax
	ret
.L9:
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE2294:
	.size	stringIteratorGetIndex, .-stringIteratorGetIndex
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	stringIteratorMove, @function
stringIteratorMove:
.LFB2295:
	.cfi_startproc
	endbr64
	cmpl	$4, %edx
	ja	.L27
	leaq	.L20(%rip), %rcx
	movl	%edx, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L20:
	.long	.L24-.L20
	.long	.L23-.L20
	.long	.L22-.L20
	.long	.L28-.L20
	.long	.L19-.L20
	.text
	.p2align 4,,10
	.p2align 3
.L19:
	addl	8(%rdi), %esi
.L28:
	movl	12(%rdi), %eax
.L25:
	cmpl	%eax, %esi
	jl	.L26
	cmpl	%esi, 20(%rdi)
	movl	%esi, %eax
	cmovle	20(%rdi), %eax
.L26:
	movl	%eax, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	12(%rdi), %eax
	addl	20(%rdi), %esi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L24:
	movl	12(%rdi), %eax
	addl	%eax, %esi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L23:
	movl	12(%rdi), %eax
	addl	16(%rdi), %esi
	jmp	.L25
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	stringIteratorMove.cold, @function
stringIteratorMove.cold:
.LFSB2295:
.L27:
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE2295:
	.text
	.size	stringIteratorMove, .-stringIteratorMove
	.section	.text.unlikely
	.size	stringIteratorMove.cold, .-stringIteratorMove.cold
.LCOLDE0:
	.text
.LHOTE0:
	.p2align 4
	.type	stringIteratorHasNext, @function
stringIteratorHasNext:
.LFB2296:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %eax
	cmpl	%eax, 16(%rdi)
	setl	%al
	ret
	.cfi_endproc
.LFE2296:
	.size	stringIteratorHasNext, .-stringIteratorHasNext
	.p2align 4
	.type	stringIteratorHasPrevious, @function
stringIteratorHasPrevious:
.LFB2297:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	cmpl	%eax, 16(%rdi)
	setg	%al
	ret
	.cfi_endproc
.LFE2297:
	.size	stringIteratorHasPrevious, .-stringIteratorHasPrevious
	.p2align 4
	.type	stringIteratorCurrent, @function
stringIteratorCurrent:
.LFB2298:
	.cfi_startproc
	endbr64
	movslq	16(%rdi), %rax
	cmpl	20(%rdi), %eax
	jge	.L33
	movq	(%rdi), %rdx
	movzwl	(%rdx,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2298:
	.size	stringIteratorCurrent, .-stringIteratorCurrent
	.p2align 4
	.type	stringIteratorNext, @function
stringIteratorNext:
.LFB2299:
	.cfi_startproc
	endbr64
	movslq	16(%rdi), %rax
	cmpl	20(%rdi), %eax
	jge	.L36
	movq	(%rdi), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rdi)
	movzwl	(%rdx,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2299:
	.size	stringIteratorNext, .-stringIteratorNext
	.p2align 4
	.type	stringIteratorPrevious, @function
stringIteratorPrevious:
.LFB2300:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	cmpl	12(%rdi), %eax
	jle	.L39
	subl	$1, %eax
	movq	(%rdi), %rdx
	movl	%eax, 16(%rdi)
	cltq
	movzwl	(%rdx,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2300:
	.size	stringIteratorPrevious, .-stringIteratorPrevious
	.p2align 4
	.type	stringIteratorGetState, @function
stringIteratorGetState:
.LFB2301:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	ret
	.cfi_endproc
.LFE2301:
	.size	stringIteratorGetState, .-stringIteratorGetState
	.p2align 4
	.type	utf16BEIteratorCurrent, @function
utf16BEIteratorCurrent:
.LFB2305:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	cmpl	%eax, 20(%rdi)
	jle	.L43
	movq	(%rdi), %rcx
	addl	%eax, %eax
	movslq	%eax, %rdx
	movzbl	(%rcx,%rdx), %eax
	movzbl	1(%rcx,%rdx), %edx
	sall	$8, %eax
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2305:
	.size	utf16BEIteratorCurrent, .-utf16BEIteratorCurrent
	.p2align 4
	.type	utf16BEIteratorNext, @function
utf16BEIteratorNext:
.LFB2306:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	cmpl	%eax, 20(%rdi)
	jle	.L46
	movq	(%rdi), %rcx
	leal	1(%rax), %edx
	addl	%eax, %eax
	movl	%edx, 16(%rdi)
	movslq	%eax, %rdx
	movzbl	(%rcx,%rdx), %eax
	movzbl	1(%rcx,%rdx), %edx
	sall	$8, %eax
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2306:
	.size	utf16BEIteratorNext, .-utf16BEIteratorNext
	.p2align 4
	.type	utf16BEIteratorPrevious, @function
utf16BEIteratorPrevious:
.LFB2307:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	cmpl	%eax, 12(%rdi)
	jge	.L49
	subl	$1, %eax
	movq	(%rdi), %rcx
	movl	%eax, 16(%rdi)
	addl	%eax, %eax
	movslq	%eax, %rdx
	movzbl	(%rcx,%rdx), %eax
	movzbl	1(%rcx,%rdx), %edx
	sall	$8, %eax
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2307:
	.size	utf16BEIteratorPrevious, .-utf16BEIteratorPrevious
	.p2align 4
	.type	characterIteratorGetIndex, @function
characterIteratorGetIndex:
.LFB2310:
	.cfi_startproc
	endbr64
	cmpl	$4, %esi
	ja	.L51
	leaq	.L53(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L53:
	.long	.L57-.L53
	.long	.L56-.L53
	.long	.L55-.L53
	.long	.L58-.L53
	.long	.L52-.L53
	.text
	.p2align 4,,10
	.p2align 3
.L58:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movq	(%rdi), %rax
	movl	20(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	movq	(%rdi), %rax
	movl	8(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movq	(%rdi), %rax
	movl	16(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%rdi), %rax
	movl	12(%rax), %eax
	ret
.L51:
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE2310:
	.size	characterIteratorGetIndex, .-characterIteratorGetIndex
	.p2align 4
	.type	characterIteratorHasNext, @function
characterIteratorHasNext:
.LFB2312:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*56(%rax)
	.cfi_endproc
.LFE2312:
	.size	characterIteratorHasNext, .-characterIteratorHasNext
	.p2align 4
	.type	characterIteratorHasPrevious, @function
characterIteratorHasPrevious:
.LFB2313:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*184(%rax)
	.cfi_endproc
.LFE2313:
	.size	characterIteratorHasPrevious, .-characterIteratorHasPrevious
	.p2align 4
	.type	characterIteratorCurrent, @function
characterIteratorCurrent:
.LFB2314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*136(%rax)
	movzwl	%ax, %r12d
	cmpl	$65535, %r12d
	jne	.L61
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	testb	%al, %al
	movl	$-1, %eax
	cmove	%eax, %r12d
.L61:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2314:
	.size	characterIteratorCurrent, .-characterIteratorCurrent
	.p2align 4
	.type	characterIteratorNext, @function
characterIteratorNext:
.LFB2315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	testb	%al, %al
	je	.L67
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movzwl	%ax, %eax
.L65:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L65
	.cfi_endproc
.LFE2315:
	.size	characterIteratorNext, .-characterIteratorNext
	.p2align 4
	.type	characterIteratorPrevious, @function
characterIteratorPrevious:
.LFB2316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*184(%rax)
	testb	%al, %al
	je	.L71
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	call	*168(%rax)
	movzwl	%ax, %eax
.L69:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L69
	.cfi_endproc
.LFE2316:
	.size	characterIteratorPrevious, .-characterIteratorPrevious
	.p2align 4
	.type	characterIteratorGetState, @function
characterIteratorGetState:
.LFB2317:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	12(%rax), %eax
	ret
	.cfi_endproc
.LFE2317:
	.size	characterIteratorGetState, .-characterIteratorGetState
	.p2align 4
	.type	replaceableIteratorCurrent, @function
replaceableIteratorCurrent:
.LFB2320:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %esi
	cmpl	20(%rdi), %esi
	jge	.L76
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*72(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzwl	%ax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore 6
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2320:
	.size	replaceableIteratorCurrent, .-replaceableIteratorCurrent
	.p2align 4
	.type	replaceableIteratorNext, @function
replaceableIteratorNext:
.LFB2321:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %esi
	cmpl	20(%rdi), %esi
	jge	.L83
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %r8
	leal	1(%rsi), %eax
	movl	%eax, 16(%rdi)
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*72(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzwl	%ax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore 6
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2321:
	.size	replaceableIteratorNext, .-replaceableIteratorNext
	.p2align 4
	.type	replaceableIteratorPrevious, @function
replaceableIteratorPrevious:
.LFB2322:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %esi
	cmpl	12(%rdi), %esi
	jle	.L90
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %r8
	subl	$1, %esi
	movl	%esi, 16(%rdi)
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*72(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzwl	%ax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore 6
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2322:
	.size	replaceableIteratorPrevious, .-replaceableIteratorPrevious
	.section	.rodata
.LC2:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	" 000000000000\02000"
	.text
	.p2align 4
	.type	utf8IteratorGetIndex, @function
utf8IteratorGetIndex:
.LFB2324:
	.cfi_startproc
	endbr64
	cmpl	$4, %esi
	ja	.L96
	leaq	.L98(%rip), %rdx
	movl	%esi, %esi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L98:
	.long	.L132-.L98
	.long	.L100-.L98
	.long	.L97-.L98
	.long	.L132-.L98
	.long	.L97-.L98
	.text
	.p2align 4,,10
	.p2align 3
.L132:
	xorl	%eax, %eax
.L95:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jns	.L95
	movl	16(%rdi), %eax
	movq	(%rdi), %r9
	movl	24(%rdi), %esi
	movl	12(%rdi), %edx
	testl	%eax, %eax
	js	.L167
	cmpl	$1, %esi
	sbbl	$-1, %eax
.L123:
	movl	20(%rdi), %r10d
	cmpl	%r10d, %edx
	jge	.L124
	leaq	.LC2(%rip), %rbx
	leaq	.LC1(%rip), %r11
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L155:
	movl	%r13d, %edx
	movl	$1, %r12d
.L125:
	addl	%r12d, %eax
	cmpl	%edx, %r10d
	jle	.L124
.L131:
	movslq	%edx, %rcx
	leal	1(%rdx), %r13d
	movzbl	(%r9,%rcx), %esi
	testb	%sil, %sil
	jns	.L155
	cmpl	%r13d, %r10d
	je	.L126
	movzbl	%sil, %ecx
	cmpb	$-33, %sil
	jbe	.L127
	cmpl	$239, %ecx
	jg	.L128
	movslq	%r13d, %rdx
	movl	%esi, %ecx
	andl	$15, %esi
	movzbl	(%r9,%rdx), %r8d
	movsbl	(%r11,%rsi), %esi
	andl	$15, %ecx
	movl	%r8d, %edx
	andl	$63, %r8d
	shrb	$5, %dl
	btl	%edx, %esi
	jnc	.L155
.L129:
	leal	1(%r13), %edx
	cmpl	%edx, %r10d
	je	.L126
	sall	$6, %ecx
	movzbl	%r8b, %esi
	orl	%ecx, %esi
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L100:
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jns	.L95
	movl	12(%rdi), %r8d
	movq	(%rdi), %r9
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L133
	xorl	%esi, %esi
	leaq	.LC2(%rip), %r11
	leaq	.LC1(%rip), %r10
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L139:
	movl	%ebx, %esi
	movl	$1, %r12d
.L103:
	addl	%r12d, %eax
	cmpl	%esi, %r8d
	jle	.L168
.L109:
	movslq	%esi, %rdx
	leal	1(%rsi), %ebx
	movzbl	(%r9,%rdx), %edx
	testb	%dl, %dl
	jns	.L139
	cmpl	%ebx, %r8d
	je	.L104
	movzbl	%dl, %ecx
	cmpb	$-33, %dl
	jbe	.L105
	cmpl	$239, %ecx
	jg	.L106
	movslq	%ebx, %rsi
	movl	%edx, %ecx
	andl	$15, %edx
	movzbl	(%r9,%rsi), %esi
	movsbl	(%r10,%rdx), %r12d
	andl	$15, %ecx
	movl	%esi, %edx
	andl	$63, %esi
	shrb	$5, %dl
	btl	%edx, %r12d
	jnc	.L139
.L107:
	addl	$1, %ebx
	cmpl	%ebx, %r8d
	je	.L104
	movzbl	%sil, %edx
	sall	$6, %ecx
	movl	%ebx, %esi
	orl	%ecx, %edx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L105:
	cmpl	$193, %ecx
	jle	.L139
	andl	$31, %edx
	movl	%ebx, %esi
.L108:
	movslq	%ebx, %rcx
	movl	$1, %r12d
	movzbl	(%r9,%rcx), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L103
	sall	$6, %edx
	movzbl	%cl, %ecx
	leal	1(%rbx), %esi
	xorl	%r12d, %r12d
	orl	%ecx, %edx
	cmpl	$65536, %edx
	setge	%r12b
	addl	$1, %r12d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L127:
	cmpl	$193, %ecx
	jle	.L155
	andl	$31, %esi
	movl	%r13d, %edx
.L130:
	movslq	%edx, %rcx
	movl	$1, %r12d
	movzbl	(%r9,%rcx), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L125
	sall	$6, %esi
	movzbl	%cl, %ecx
	xorl	%r12d, %r12d
	addl	$1, %edx
	orl	%ecx, %esi
	cmpl	$65536, %esi
	setge	%r12b
	addl	$1, %r12d
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L126:
	addl	$1, %eax
.L124:
	popq	%rbx
	popq	%r12
	movl	%eax, 8(%rdi)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	addl	$1, %eax
.L102:
	movl	%ebx, 12(%rdi)
	cmpl	%ebx, 20(%rdi)
	jne	.L110
	movl	%eax, 8(%rdi)
.L110:
	cmpl	$1, 24(%rdi)
	popq	%rbx
	adcl	$-1, %eax
	popq	%r12
	popq	%r13
	movl	%eax, 16(%rdi)
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	subl	$240, %ecx
	cmpl	$4, %ecx
	jg	.L155
	movslq	%r13d, %rsi
	movzbl	(%r9,%rsi), %esi
	movq	%rsi, %r8
	shrq	$4, %r8
	andl	$15, %r8d
	movsbl	(%rbx,%r8), %r12d
	sarl	%cl, %r12d
	andl	$1, %r12d
	je	.L155
	addl	$2, %edx
	cmpl	%edx, %r10d
	je	.L126
	movslq	%edx, %r8
	movzbl	(%r9,%r8), %r8d
	addl	$-128, %r8d
	cmpb	$63, %r8b
	ja	.L125
	sall	$6, %ecx
	andl	$63, %esi
	movl	%edx, %r13d
	orl	%esi, %ecx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L106:
	subl	$240, %ecx
	cmpl	$4, %ecx
	jg	.L139
	movslq	%ebx, %rdx
	movzbl	(%r9,%rdx), %edx
	movq	%rdx, %r12
	shrq	$4, %r12
	andl	$15, %r12d
	movsbl	(%r11,%r12), %r12d
	sarl	%cl, %r12d
	andl	$1, %r12d
	je	.L139
	leal	2(%rsi), %ebx
	cmpl	%ebx, %r8d
	je	.L104
	movslq	%ebx, %rsi
	movzbl	(%r9,%rsi), %esi
	addl	$-128, %esi
	cmpb	$63, %sil
	ja	.L138
	sall	$6, %ecx
	andl	$63, %edx
	orl	%edx, %ecx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L167:
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L142
	xorl	%r11d, %r11d
	leaq	.LC2(%rip), %r10
	leaq	.LC1(%rip), %r8
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L147:
	movl	%ebx, %r11d
	movl	$1, %r12d
.L115:
	addl	%r12d, %eax
	cmpl	%r11d, %edx
	jle	.L169
.L121:
	movslq	%r11d, %rcx
	leal	1(%r11), %ebx
	movzbl	(%r9,%rcx), %ecx
	testb	%cl, %cl
	jns	.L147
	cmpl	%edx, %ebx
	je	.L116
	movzbl	%cl, %r12d
	cmpb	$-33, %cl
	jbe	.L117
	cmpl	$239, %r12d
	jg	.L118
	movslq	%ebx, %r11
	movl	%ecx, %r12d
	andl	$15, %ecx
	movzbl	(%r9,%r11), %r13d
	movsbl	(%r8,%rcx), %r11d
	andl	$15, %r12d
	movl	%r13d, %ecx
	andl	$63, %r13d
	shrb	$5, %cl
	btl	%ecx, %r11d
	jnc	.L147
.L119:
	leal	1(%rbx), %r11d
	cmpl	%edx, %r11d
	je	.L116
	movl	%r12d, %ebx
	movzbl	%r13b, %ecx
	sall	$6, %ebx
	orl	%ebx, %ecx
.L120:
	movslq	%r11d, %rbx
	movl	$1, %r12d
	movzbl	(%r9,%rbx), %ebx
	addl	$-128, %ebx
	cmpb	$63, %bl
	ja	.L115
	sall	$6, %ecx
	movzbl	%bl, %ebx
	xorl	%r12d, %r12d
	addl	$1, %r11d
	orl	%ebx, %ecx
	cmpl	$65536, %ecx
	setge	%r12b
	addl	$1, %r12d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L117:
	cmpl	$193, %r12d
	jle	.L147
	andl	$31, %ecx
	movl	%ebx, %r11d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L116:
	addl	$1, %eax
.L114:
	cmpl	$1, %esi
	movl	%eax, %ecx
	movl	%edx, 12(%rdi)
	adcl	$-1, %ecx
	movl	%ecx, 16(%rdi)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L118:
	leal	-240(%r12), %ecx
	cmpl	$4, %ecx
	jg	.L147
	movslq	%ebx, %r12
	movzbl	(%r9,%r12), %r14d
	movq	%r14, %r12
	shrq	$4, %r12
	andl	$15, %r12d
	movsbl	(%r10,%r12), %r12d
	sarl	%cl, %r12d
	andl	$1, %r12d
	je	.L147
	addl	$2, %r11d
	cmpl	%edx, %r11d
	je	.L116
	movslq	%r11d, %rbx
	movzbl	(%r9,%rbx), %r13d
	addl	$-128, %r13d
	cmpb	$63, %r13b
	ja	.L115
	movl	%ecx, %ebx
	movl	%r14d, %r12d
	sall	$6, %ebx
	andl	$63, %r12d
	orl	%ebx, %r12d
	movl	%r11d, %ebx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L133:
	xorl	%ebx, %ebx
	jmp	.L102
.L142:
	xorl	%edx, %edx
	jmp	.L114
.L138:
	movl	%ebx, %esi
	jmp	.L103
.L96:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	orl	$-1, %eax
	ret
.L168:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	%esi, %ebx
	jmp	.L102
.L169:
	movl	%r11d, %edx
	jmp	.L114
	.cfi_endproc
.LFE2324:
	.size	utf8IteratorGetIndex, .-utf8IteratorGetIndex
	.p2align 4
	.type	utf8IteratorHasNext, @function
utf8IteratorHasNext:
.LFB2326:
	.cfi_startproc
	endbr64
	movl	20(%rdi), %edx
	movl	$1, %eax
	cmpl	%edx, 12(%rdi)
	jl	.L170
	movl	24(%rdi), %eax
	testl	%eax, %eax
	setne	%al
.L170:
	ret
	.cfi_endproc
.LFE2326:
	.size	utf8IteratorHasNext, .-utf8IteratorHasNext
	.p2align 4
	.type	utf8IteratorHasPrevious, @function
utf8IteratorHasPrevious:
.LFB2327:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	testl	%eax, %eax
	setg	%al
	ret
	.cfi_endproc
.LFE2327:
	.size	utf8IteratorHasPrevious, .-utf8IteratorHasPrevious
	.p2align 4
	.type	utf8IteratorGetState, @function
utf8IteratorGetState:
.LFB2331:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %eax
	movl	24(%rdi), %edx
	addl	%eax, %eax
	testl	%edx, %edx
	je	.L174
	orl	$1, %eax
.L174:
	ret
	.cfi_endproc
.LFE2331:
	.size	utf8IteratorGetState, .-utf8IteratorGetState
	.section	.text.unlikely
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4
	.type	utf8IteratorMove, @function
utf8IteratorMove:
.LFB2325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, %edx
	ja	.L228
	leaq	.L179(%rip), %rcx
	movl	%edx, %edx
	movq	%rdi, %r13
	movl	%esi, %ebx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L179:
	.long	.L180-.L179
	.long	.L181-.L179
	.long	.L178-.L179
	.long	.L180-.L179
	.long	.L178-.L179
	.text
	.p2align 4,,10
	.p2align 3
.L181:
	movl	16(%rdi), %edi
	testl	%edi, %edi
	js	.L182
	addl	%edi, %ebx
.L180:
	testl	%ebx, %ebx
	jle	.L194
.L185:
	movl	8(%r13), %eax
	cmpl	%ebx, %eax
	jg	.L186
	testl	%eax, %eax
	jns	.L265
.L186:
	movl	16(%r13), %r12d
	testl	%r12d, %r12d
	js	.L187
	movl	%r12d, %edx
	sarl	%edx
	cmpl	%ebx, %edx
	jg	.L187
	movl	%ebx, %ecx
	subl	%r12d, %ecx
	testl	%eax, %eax
	js	.L190
	movl	%eax, %edx
	subl	%ebx, %edx
	cmpl	%ecx, %edx
	jge	.L190
	movl	20(%r13), %edx
	movl	%eax, 16(%r13)
	subl	%eax, %ebx
	movl	%eax, %r12d
	movl	$0, 24(%r13)
	movl	%ebx, %ecx
	movl	%edx, 12(%r13)
.L190:
	testl	%ecx, %ecx
	je	.L176
	movl	12(%r13), %edx
	movl	24(%r13), %eax
	movl	%r12d, %edi
	movl	%ecx, %ebx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L178:
	movl	8(%rdi), %eax
	testl	%eax, %eax
	js	.L183
	addl	%eax, %ebx
	testl	%ebx, %ebx
	jg	.L185
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$0, 24(%r13)
	xorl	%r12d, %r12d
	movq	$0, 12(%r13)
.L176:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movl	$0, 24(%r13)
	movq	0(%r13), %r15
	xorl	%edi, %edi
	xorl	%edx, %edx
	movq	$0, 12(%r13)
	movl	20(%r13), %esi
	xorl	%r12d, %r12d
	movl	$0, -60(%rbp)
.L189:
	leaq	.LC2(%rip), %r9
	leaq	.LC1(%rip), %r8
	jmp	.L199
.L267:
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L200:
	addl	$1, %r12d
	subl	$1, %ebx
	movl	%eax, %edx
.L206:
	testl	%ebx, %ebx
	je	.L198
.L199:
	cmpl	%edx, %esi
	jle	.L198
	leal	1(%rdx), %eax
	movslq	%edx, %rcx
	movl	%eax, -60(%rbp)
	movzbl	(%r15,%rcx), %ecx
	cmpl	%esi, %eax
	je	.L200
	testb	%cl, %cl
	jns	.L200
	movzbl	%cl, %r10d
	cmpb	$-33, %cl
	jbe	.L201
	cmpl	$239, %r10d
	jg	.L202
	movslq	%eax, %rdx
	movl	%ecx, %r10d
	andl	$15, %ecx
	movzbl	(%r15,%rdx), %edx
	movsbl	(%r8,%rcx), %r11d
	andl	$15, %r10d
	movl	%edx, %ecx
	andl	$63, %edx
	shrb	$5, %cl
	btl	%ecx, %r11d
	jnc	.L200
.L203:
	addl	$1, %eax
	movl	%eax, -60(%rbp)
	cmpl	%esi, %eax
	je	.L200
	sall	$6, %r10d
	movzbl	%dl, %ecx
	orl	%r10d, %ecx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L201:
	cmpl	$193, %r10d
	jle	.L200
	andl	$31, %ecx
.L204:
	movslq	%eax, %rdx
	movzbl	(%r15,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L200
	movzbl	%dl, %edx
	sall	$6, %ecx
	orl	%edx, %ecx
	leal	1(%rax), %edx
	movl	%edx, -60(%rbp)
	cmpl	$65535, %ecx
	jle	.L267
	cmpl	$1, %ebx
	je	.L207
	addl	$2, %r12d
	subl	$2, %ebx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L202:
	subl	$240, %r10d
	cmpl	$4, %r10d
	jg	.L200
	movslq	%eax, %rcx
	movzbl	(%r15,%rcx), %ecx
	movq	%rcx, %r11
	shrq	$4, %r11
	andl	$15, %r11d
	movsbl	(%r9,%r11), %r11d
	btl	%r10d, %r11d
	jnc	.L200
	leal	2(%rdx), %eax
	movl	%eax, -60(%rbp)
	cmpl	%esi, %eax
	je	.L230
	movslq	%eax, %rdx
	movzbl	(%r15,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L200
	sall	$6, %r10d
	andl	$63, %ecx
	orl	%ecx, %r10d
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L183:
	movl	20(%rdi), %edx
	movl	$-1, 16(%rdi)
	movl	$0, 24(%rdi)
	movl	%edx, 12(%rdi)
	testl	%esi, %esi
	jns	.L192
	movl	%esi, %eax
	negl	%eax
	cmpl	%eax, %edx
	jle	.L194
	movl	%edx, -60(%rbp)
	movq	(%rdi), %r15
	movl	$-1, %r12d
.L215:
	leaq	-60(%rbp), %r14
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L220:
	subl	$1, %r12d
	addl	$1, %ebx
.L222:
	testl	%ebx, %ebx
	je	.L264
.L216:
	testl	%edx, %edx
	jle	.L268
	subl	$1, %edx
	movslq	%edx, %rax
	movl	%edx, -60(%rbp)
	movzbl	(%r15,%rax), %ecx
	testb	%cl, %cl
	jns	.L220
	movq	%r14, %rdx
	movl	$-3, %r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	utf8_prevCharSafeBody_67@PLT
	movl	-60(%rbp), %edx
	cmpl	$65535, %eax
	jle	.L220
	cmpl	$-1, %ebx
	je	.L223
	subl	$2, %r12d
	addl	$2, %ebx
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L182:
	testl	%esi, %esi
	je	.L192
	movl	%esi, %eax
	movl	12(%r13), %edx
	negl	%eax
	cmpl	%edx, %eax
	jge	.L194
	movl	20(%r13), %eax
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	%esi, %ecx
	jg	.L269
	movl	8(%r13), %r12d
	movl	%eax, 12(%r13)
	movl	$0, 24(%r13)
	movl	%r12d, 16(%r13)
	testl	%r12d, %r12d
	jns	.L176
.L192:
	movl	$-2, %r12d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L265:
	movl	20(%r13), %edx
	movl	%eax, 16(%r13)
	movl	%eax, %r12d
	movl	$0, 24(%r13)
	movl	%edx, 12(%r13)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L207:
	movl	%ecx, 24(%r13)
	addl	$1, %r12d
	.p2align 4,,10
	.p2align 3
.L198:
	cmpl	%edx, %esi
	je	.L270
.L208:
	movl	%edx, 12(%r13)
	testl	%edi, %edi
	js	.L224
.L218:
	movl	%r12d, 16(%r13)
	jmp	.L176
.L210:
	movl	%edx, 12(%r13)
	.p2align 4,,10
	.p2align 3
.L224:
	cmpl	$1, %edx
	jg	.L192
.L219:
	movl	%edx, 16(%r13)
	movl	%edx, %r12d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L223:
	addl	$4, %edx
	movl	%eax, 24(%r13)
	subl	$1, %r12d
	movl	%edx, -60(%rbp)
.L264:
	movl	16(%r13), %edi
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L270:
	movl	8(%r13), %eax
	testl	%eax, %eax
	js	.L271
	testl	%edi, %edi
	js	.L272
.L212:
	movl	%edx, 12(%r13)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L271:
	testl	%edi, %edi
	js	.L210
	cmpl	$1, 24(%r13)
	movl	%r12d, %eax
	sbbl	$-1, %eax
	movl	%eax, 8(%r13)
	jmp	.L212
.L269:
	movl	24(%r13), %eax
.L196:
	movl	%edx, -60(%rbp)
	movq	0(%r13), %r15
	testl	%ebx, %ebx
	jle	.L197
	movl	20(%r13), %esi
	testl	%eax, %eax
	jne	.L273
	movl	%edi, %r12d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L273:
	movl	$0, 24(%r13)
	leal	1(%rdi), %r12d
	subl	$1, %ebx
	jne	.L189
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L268:
	movl	16(%r13), %eax
	movl	%edx, 12(%r13)
	testl	%eax, %eax
	jns	.L218
	jmp	.L219
.L230:
	movl	%esi, %eax
	jmp	.L200
.L272:
	cmpl	$1, 24(%r13)
	adcl	$-1, %eax
	movl	%eax, 16(%r13)
	movl	%eax, %edi
	jmp	.L208
.L197:
	testl	%eax, %eax
	je	.L233
	subl	$4, %edx
	movl	$0, 24(%r13)
	leal	-1(%rdi), %r12d
	addl	$1, %ebx
	movl	%edx, -60(%rbp)
.L214:
	testl	%ebx, %ebx
	js	.L215
	jmp	.L208
.L266:
	call	__stack_chk_fail@PLT
.L233:
	movl	%edi, %r12d
	jmp	.L214
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	utf8IteratorMove.cold, @function
utf8IteratorMove.cold:
.LFSB2325:
.L228:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	orl	$-1, %r12d
	jmp	.L176
	.cfi_endproc
.LFE2325:
	.text
	.size	utf8IteratorMove, .-utf8IteratorMove
	.section	.text.unlikely
	.size	utf8IteratorMove.cold, .-utf8IteratorMove.cold
.LCOLDE3:
	.text
.LHOTE3:
	.p2align 4
	.type	stringIteratorSetState, @function
stringIteratorSetState:
.LFB2302:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L274
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L282
.L274:
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	testq	%rdi, %rdi
	je	.L283
	cmpl	%esi, 12(%rdi)
	jg	.L277
	cmpl	20(%rdi), %esi
	jg	.L277
	movl	%esi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	movl	$8, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$1, (%rdx)
	ret
	.cfi_endproc
.LFE2302:
	.size	stringIteratorSetState, .-stringIteratorSetState
	.p2align 4
	.type	utf8IteratorCurrent, @function
utf8IteratorCurrent:
.LFB2328:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jne	.L302
	movl	12(%rdi), %ecx
	movl	20(%rdi), %esi
	cmpl	%esi, %ecx
	jge	.L291
	movq	(%rdi), %rdi
	movslq	%ecx, %rax
	movzbl	(%rdi,%rax), %eax
	movl	%eax, %edx
	testb	%al, %al
	js	.L303
.L284:
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	leal	1(%rcx), %r8d
	cmpl	%r8d, %esi
	je	.L298
	cmpl	$223, %eax
	jg	.L304
	cmpl	$193, %eax
	jle	.L298
	andl	$31, %edx
.L290:
	movslq	%r8d, %rax
	movzbl	(%rdi,%rax), %ecx
	movl	$65533, %eax
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L284
	movl	%edx, %eax
	movzbl	%cl, %edx
	sall	$6, %eax
	orl	%edx, %eax
	cmpl	$65535, %eax
	jle	.L284
	sarl	$10, %eax
	subw	$10304, %ax
	movzwl	%ax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	andl	$1023, %eax
	orb	$-36, %ah
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	cmpl	$239, %eax
	jg	.L288
	movl	%eax, %r9d
	movslq	%r8d, %rax
	andl	$15, %edx
	movzbl	(%rdi,%rax), %ecx
	leaq	.LC1(%rip), %rax
	andl	$15, %r9d
	movsbl	(%rax,%rdx), %r10d
	movl	$65533, %eax
	movl	%ecx, %edx
	andl	$63, %ecx
	shrb	$5, %dl
	btl	%edx, %r10d
	jnc	.L284
.L289:
	movl	%r9d, %eax
	movzbl	%cl, %edx
	addl	$1, %r8d
	sall	$6, %eax
	orl	%eax, %edx
	cmpl	%r8d, %esi
	jne	.L290
.L298:
	movl	$65533, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	leal	-240(%rax), %r9d
	movl	$65533, %eax
	cmpl	$4, %r9d
	jg	.L284
	movslq	%r8d, %r8
	movzbl	(%rdi,%r8), %r10d
	leaq	.LC2(%rip), %r8
	movq	%r10, %rdx
	shrq	$4, %rdx
	andl	$15, %edx
	movsbl	(%r8,%rdx), %edx
	btl	%r9d, %edx
	jnc	.L284
	leal	2(%rcx), %r8d
	cmpl	%r8d, %esi
	je	.L284
	movslq	%r8d, %rdx
	movzbl	(%rdi,%rdx), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L284
	sall	$6, %r9d
	andl	$63, %r10d
	orl	%r10d, %r9d
	jmp	.L289
.L291:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2328:
	.size	utf8IteratorCurrent, .-utf8IteratorCurrent
	.p2align 4
	.type	utf8IteratorNext, @function
utf8IteratorNext:
.LFB2329:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jne	.L337
	movl	12(%rdi), %edx
	movl	20(%rdi), %r8d
	cmpl	%r8d, %edx
	jge	.L323
	movq	(%rdi), %r9
	leal	1(%rdx), %esi
	movslq	%edx, %rax
	movl	%esi, 12(%rdi)
	movzbl	(%r9,%rax), %eax
	movl	%eax, %ecx
	testb	%al, %al
	js	.L338
.L309:
	movl	16(%rdi), %ecx
	testl	%ecx, %ecx
	js	.L316
	movl	8(%rdi), %esi
	leal	1(%rcx), %edx
	movl	%edx, 16(%rdi)
	testl	%esi, %esi
	js	.L339
.L317:
	cmpl	$65535, %eax
	jle	.L305
	movl	%eax, 24(%rdi)
	sarl	$10, %eax
	subw	$10304, %ax
.L335:
	movzwl	%ax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	cmpl	%esi, %r8d
	je	.L310
	cmpl	$223, %eax
	jle	.L311
	cmpl	$239, %eax
	jle	.L340
	subl	$240, %eax
	cmpl	$4, %eax
	jg	.L313
	movslq	%esi, %rsi
	movzbl	(%r9,%rsi), %r10d
	leaq	.LC2(%rip), %rsi
	movq	%r10, %rcx
	shrq	$4, %rcx
	andl	$15, %ecx
	movsbl	(%rsi,%rcx), %ecx
	btl	%eax, %ecx
	jnc	.L313
	leal	2(%rdx), %esi
	movl	%esi, 12(%rdi)
	cmpl	%esi, %r8d
	je	.L310
	movslq	%esi, %rdx
	movzbl	(%r9,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L313
	sall	$6, %eax
	andl	$63, %r10d
	orl	%r10d, %eax
	.p2align 4,,10
	.p2align 3
.L314:
	addl	$1, %esi
	movl	%esi, 12(%rdi)
	cmpl	%esi, %r8d
	je	.L310
	sall	$6, %eax
	movzbl	%dl, %ecx
	orl	%eax, %ecx
.L315:
	movslq	%esi, %rax
	movzbl	(%r9,%rax), %edx
	movl	$65533, %eax
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L309
	sall	$6, %ecx
	movzbl	%dl, %eax
	addl	$1, %esi
	movl	%esi, 12(%rdi)
	orl	%ecx, %eax
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L310:
	movl	16(%rdi), %edx
	testl	%edx, %edx
	js	.L341
.L321:
	movl	8(%rdi), %ecx
	addl	$1, %edx
	movl	$65533, %eax
	movl	%edx, 16(%rdi)
	testl	%ecx, %ecx
	js	.L342
.L305:
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	movl	16(%rdi), %edx
	andw	$1023, %ax
	movl	$0, 24(%rdi)
	orw	$-9216, %ax
	testl	%edx, %edx
	js	.L335
	addl	$1, %edx
	movzwl	%ax, %eax
	movl	%edx, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	cmpl	12(%rdi), %r8d
	jne	.L317
	addl	$2, %ecx
	cmpl	$65535, %eax
	cmovg	%ecx, %edx
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L316:
	cmpl	%esi, %r8d
	jne	.L317
	movl	8(%rdi), %edx
	testl	%edx, %edx
	js	.L317
	xorl	%ecx, %ecx
	cmpl	$65535, %eax
	setg	%cl
	subl	%ecx, %edx
	movl	%edx, 16(%rdi)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L311:
	cmpl	$193, %eax
	jle	.L313
	andl	$31, %ecx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L340:
	movslq	%esi, %rdx
	andl	$15, %ecx
	leaq	.LC1(%rip), %r10
	andl	$15, %eax
	movzbl	(%r9,%rdx), %edx
	movsbl	(%r10,%rcx), %r10d
	movl	%edx, %ecx
	andl	$63, %edx
	shrb	$5, %cl
	btl	%ecx, %r10d
	jc	.L314
.L313:
	movl	16(%rdi), %edx
	movl	$65533, %eax
	testl	%edx, %edx
	jns	.L321
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	cmpl	12(%rdi), %r8d
	jne	.L305
.L319:
	movl	%edx, 8(%rdi)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L341:
	movl	8(%rdi), %edx
	movl	$65533, %eax
	testl	%edx, %edx
	js	.L305
	movl	%edx, 16(%rdi)
	ret
.L323:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2329:
	.size	utf8IteratorNext, .-utf8IteratorNext
	.p2align 4
	.type	utf8IteratorSetState, @function
utf8IteratorSetState:
.LFB2332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L343
	movl	(%rdx), %ecx
	movq	%rdx, %rbx
	testl	%ecx, %ecx
	jle	.L361
.L343:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L362
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L363
	movl	12(%rdi), %eax
	movl	24(%rdi), %edx
	addl	%eax, %eax
	testl	%edx, %edx
	je	.L346
	orl	$1, %eax
.L346:
	cmpl	%eax, %esi
	je	.L343
	movl	%esi, %eax
	andl	$1, %esi
	shrl	%eax
	movl	%eax, -28(%rbp)
	cmpl	$3, %eax
	ja	.L347
	testb	%sil, %sil
	jne	.L349
.L347:
	cmpl	20(%r12), %eax
	jg	.L349
	cmpl	$1, %eax
	movl	$-1, %edx
	movl	%eax, 12(%r12)
	cmovle	%eax, %edx
	movl	%edx, 16(%r12)
	testl	%esi, %esi
	jne	.L351
	movl	$0, 24(%r12)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L349:
	movl	$8, (%rbx)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L363:
	movl	$1, (%rdx)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L351:
	subl	$1, %eax
	movq	(%r12), %rdi
	movl	%eax, -28(%rbp)
	cltq
	movzbl	(%rdi,%rax), %eax
	testb	%al, %al
	jns	.L349
	xorl	%esi, %esi
	movzbl	%al, %ecx
	leaq	-28(%rbp), %rdx
	movl	$-3, %r8d
	call	utf8_prevCharSafeBody_67@PLT
	cmpl	$65535, %eax
	jle	.L349
	movl	%eax, 24(%r12)
	jmp	.L343
.L362:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2332:
	.size	utf8IteratorSetState, .-utf8IteratorSetState
	.p2align 4
	.type	utf8IteratorPrevious, @function
utf8IteratorPrevious:
.LFB2330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	24(%rdi), %eax
	movl	12(%rdi), %edx
	testl	%eax, %eax
	jne	.L374
	testl	%edx, %edx
	jle	.L372
	movq	(%rdi), %rdi
	subl	$1, %edx
	movl	%edx, 12(%rbx)
	movslq	%edx, %rdx
	movzbl	(%rdi,%rdx), %eax
	testb	%al, %al
	js	.L375
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L369
.L376:
	subl	$1, %edx
	movl	%edx, 16(%rbx)
.L370:
	cmpl	$65535, %eax
	jle	.L364
	addl	$4, 12(%rbx)
	movl	%eax, 24(%rbx)
	andl	$1023, %eax
	orb	$-36, %ah
.L364:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	movl	12(%rbx), %edx
	cmpl	$1, %edx
	jg	.L370
	xorl	%ecx, %ecx
	cmpl	$65535, %eax
	setg	%cl
	addl	%ecx, %edx
	movl	%edx, 16(%rbx)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L374:
	subl	$4, %edx
	sarl	$10, %eax
	movl	$0, 24(%rdi)
	movl	%edx, 12(%rdi)
	movl	16(%rdi), %edx
	subw	$10304, %ax
	testl	%edx, %edx
	jle	.L366
	subl	$1, %edx
	movl	%edx, 16(%rdi)
.L366:
	addq	$8, %rsp
	movzwl	%ax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	leaq	12(%rbx), %rdx
	movl	$-3, %r8d
	movl	%eax, %ecx
	xorl	%esi, %esi
	call	utf8_prevCharSafeBody_67@PLT
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L376
	jmp	.L369
.L372:
	movl	$-1, %eax
	jmp	.L364
	.cfi_endproc
.LFE2330:
	.size	utf8IteratorPrevious, .-utf8IteratorPrevious
	.p2align 4
	.type	characterIteratorSetState, @function
characterIteratorSetState:
.LFB2318:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L377
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L388
.L377:
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	testq	%rdi, %rdi
	je	.L379
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L379
	cmpl	%esi, 16(%rdi)
	jg	.L381
	cmpl	20(%rdi), %esi
	jle	.L389
.L381:
	movl	$8, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	movq	(%rdi), %rax
	jmp	*120(%rax)
	.cfi_endproc
.LFE2318:
	.size	characterIteratorSetState, .-characterIteratorSetState
	.p2align 4
	.type	characterIteratorMove, @function
characterIteratorMove:
.LFB2311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$3, %edx
	je	.L391
	ja	.L396
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	movq	192(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movl	$-1, %eax
	cmpl	$4, %edx
	jne	.L390
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	addl	8(%rdi), %esi
	call	*120(%rax)
	movq	(%rbx), %rax
	movl	12(%rax), %eax
.L390:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	movq	(%rbx), %rax
	movl	12(%rax), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2311:
	.size	characterIteratorMove, .-characterIteratorMove
	.p2align 4
	.globl	uiter_setString_67
	.type	uiter_setString_67, @function
uiter_setString_67:
.LFB2303:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L412
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L399
	cmpl	$-1, %edx
	jl	.L399
	pxor	%xmm0, %xmm0
	leaq	stringIteratorMove(%rip), %rax
	leaq	stringIteratorGetIndex(%rip), %rcx
	movl	$0, 24(%rdi)
	movq	%rax, %xmm1
	movups	%xmm0, 8(%rdi)
	movq	%rcx, %xmm0
	leaq	stringIteratorHasPrevious(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	movq	%rsi, (%rdi)
	leaq	stringIteratorHasNext(%rip), %rcx
	movups	%xmm0, 32(%rdi)
	movq	%rcx, %xmm0
	leaq	stringIteratorNext(%rip), %rax
	leaq	stringIteratorCurrent(%rip), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, %xmm3
	leaq	stringIteratorPrevious(%rip), %rax
	movq	$0, 88(%rdi)
	movups	%xmm0, 48(%rdi)
	movq	%rcx, %xmm0
	leaq	stringIteratorGetState(%rip), %rcx
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 80(%rdi)
	leaq	stringIteratorSetState(%rip), %rax
	movups	%xmm0, 64(%rdi)
	movq	%rax, %xmm4
	movq	%rcx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 96(%rdi)
	je	.L400
.L415:
	movl	%edx, 8(%rbx)
	movl	%edx, 20(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	leaq	noopGetIndex(%rip), %rcx
	leaq	noopMove(%rip), %rdx
	movq	$0, (%rbx)
	movups	%xmm0, 8(%rbx)
	movq	%rdx, %xmm5
	movq	%rcx, %xmm0
	leaq	noopHasNext(%rip), %rdi
	punpcklqdq	%xmm5, %xmm0
	leaq	noopCurrent(%rip), %rax
	leaq	noopGetState(%rip), %rcx
	movl	$0, 24(%rbx)
	movups	%xmm0, 32(%rbx)
	movq	%rdi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 80(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	leaq	noopSetState(%rip), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, %xmm6
	movq	$0, 88(%rbx)
	movups	%xmm0, 64(%rbx)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %edx
	jmp	.L415
	.cfi_endproc
.LFE2303:
	.size	uiter_setString_67, .-uiter_setString_67
	.p2align 4
	.globl	uiter_setUTF16BE_67
	.type	uiter_setUTF16BE_67, @function
uiter_setUTF16BE_67:
.LFB2309:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L438
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L418
	cmpl	$-1, %edx
	je	.L419
	testl	%edx, %edx
	js	.L418
	testb	$1, %dl
	je	.L419
.L418:
	pxor	%xmm0, %xmm0
	leaq	noopGetIndex(%rip), %rcx
	leaq	noopMove(%rip), %rdx
	movq	$0, (%rbx)
	movups	%xmm0, 8(%rbx)
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	leaq	noopHasNext(%rip), %rcx
	punpcklqdq	%xmm1, %xmm0
	leaq	noopCurrent(%rip), %rax
	movl	$0, 24(%rbx)
	movups	%xmm0, 32(%rbx)
	movq	%rcx, %xmm0
	leaq	noopGetState(%rip), %rcx
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 80(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	leaq	noopSetState(%rip), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, %xmm2
	movq	$0, 88(%rbx)
	movups	%xmm0, 64(%rbx)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	stringIteratorMove(%rip), %rax
	sarl	%edx
	movq	%rsi, (%rbx)
	leaq	stringIteratorGetIndex(%rip), %rcx
	movq	%rax, %xmm3
	movups	%xmm0, 8(%rbx)
	leaq	stringIteratorHasPrevious(%rip), %rax
	movq	%rcx, %xmm0
	leaq	stringIteratorHasNext(%rip), %rdi
	movq	%rax, %xmm4
	movl	$0, 24(%rbx)
	punpcklqdq	%xmm3, %xmm0
	leaq	utf16BEIteratorNext(%rip), %rax
	leaq	utf16BEIteratorCurrent(%rip), %rcx
	movq	$0, 88(%rbx)
	movups	%xmm0, 32(%rbx)
	movq	%rdi, %xmm0
	movq	%rax, %xmm5
	leaq	utf16BEIteratorPrevious(%rip), %rax
	punpcklqdq	%xmm4, %xmm0
	movq	%rax, 80(%rbx)
	leaq	stringIteratorGetState(%rip), %rdi
	leaq	stringIteratorSetState(%rip), %rax
	movups	%xmm0, 48(%rbx)
	movq	%rcx, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rdi, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 96(%rbx)
	js	.L420
.L423:
	movl	%edx, 8(%rbx)
	movl	%edx, 20(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	movq	%rsi, %rdx
	testb	$1, %sil
	jne	.L422
	movq	%rsi, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %edx
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L424:
	addq	$2, %rdx
.L422:
	cmpb	$0, (%rdx)
	jne	.L424
	cmpb	$0, 1(%rdx)
	jne	.L424
	movq	%rdx, %rax
	subq	%rsi, %rax
	movq	%rax, %rdx
	shrq	$63, %rdx
	addq	%rax, %rdx
	sarq	%rdx
	jmp	.L423
	.cfi_endproc
.LFE2309:
	.size	uiter_setUTF16BE_67, .-uiter_setUTF16BE_67
	.p2align 4
	.globl	uiter_setCharacterIterator_67
	.type	uiter_setCharacterIterator_67, @function
uiter_setCharacterIterator_67:
.LFB2319:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L441
	testq	%rsi, %rsi
	je	.L443
	pxor	%xmm0, %xmm0
	leaq	characterIteratorMove(%rip), %rax
	leaq	characterIteratorGetIndex(%rip), %rcx
	movl	$0, 24(%rdi)
	movq	%rax, %xmm1
	movups	%xmm0, 8(%rdi)
	movq	%rcx, %xmm0
	leaq	characterIteratorHasPrevious(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	leaq	characterIteratorHasNext(%rip), %rdx
	movq	$0, 88(%rdi)
	movups	%xmm0, 32(%rdi)
	movq	%rdx, %xmm0
	leaq	characterIteratorNext(%rip), %rax
	leaq	characterIteratorCurrent(%rip), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, %xmm3
	leaq	characterIteratorPrevious(%rip), %rax
	movq	%rsi, (%rdi)
	movups	%xmm0, 48(%rdi)
	movq	%rcx, %xmm0
	leaq	characterIteratorGetState(%rip), %rdx
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 80(%rdi)
	leaq	characterIteratorSetState(%rip), %rax
	movups	%xmm0, 64(%rdi)
	movq	%rax, %xmm4
	movq	%rdx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 96(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	pxor	%xmm0, %xmm0
	leaq	noopMove(%rip), %rdx
	leaq	noopGetIndex(%rip), %rcx
	movq	$0, (%rdi)
	movq	%rdx, %xmm5
	movups	%xmm0, 8(%rdi)
	movq	%rcx, %xmm0
	leaq	noopHasNext(%rip), %rdx
	punpcklqdq	%xmm5, %xmm0
	leaq	noopCurrent(%rip), %rax
	leaq	noopGetState(%rip), %rcx
	movl	$0, 24(%rdi)
	movups	%xmm0, 32(%rdi)
	movq	%rdx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 80(%rdi)
	movups	%xmm0, 48(%rdi)
	movq	%rax, %xmm0
	leaq	noopSetState(%rip), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, %xmm6
	movq	$0, 88(%rdi)
	movups	%xmm0, 64(%rdi)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 96(%rdi)
.L441:
	ret
	.cfi_endproc
.LFE2319:
	.size	uiter_setCharacterIterator_67, .-uiter_setCharacterIterator_67
	.p2align 4
	.globl	uiter_setReplaceable_67
	.type	uiter_setReplaceable_67, @function
uiter_setReplaceable_67:
.LFB2323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L447
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L449
	pxor	%xmm0, %xmm0
	leaq	stringIteratorMove(%rip), %rax
	leaq	stringIteratorGetIndex(%rip), %rcx
	movl	$0, 24(%rbx)
	movq	%rax, %xmm1
	movups	%xmm0, 8(%rbx)
	movq	%rcx, %xmm0
	leaq	stringIteratorHasPrevious(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	leaq	stringIteratorHasNext(%rip), %rdx
	movq	$0, 88(%rbx)
	movups	%xmm0, 32(%rbx)
	movq	%rdx, %xmm0
	leaq	replaceableIteratorNext(%rip), %rax
	leaq	replaceableIteratorCurrent(%rip), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, %xmm3
	leaq	replaceableIteratorPrevious(%rip), %rax
	movq	%rsi, (%rbx)
	movups	%xmm0, 48(%rbx)
	movq	%rcx, %xmm0
	leaq	stringIteratorGetState(%rip), %rdx
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 80(%rbx)
	leaq	stringIteratorSetState(%rip), %rax
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm4
	movq	%rdx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 96(%rbx)
	movq	(%rsi), %rax
	call	*64(%rax)
	movl	%eax, 8(%rbx)
	movl	%eax, 20(%rbx)
.L447:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	leaq	noopMove(%rip), %rdx
	leaq	noopGetIndex(%rip), %rcx
	movq	$0, (%rbx)
	movups	%xmm0, 8(%rbx)
	movq	%rdx, %xmm5
	movq	%rcx, %xmm0
	leaq	noopHasNext(%rip), %rdx
	punpcklqdq	%xmm5, %xmm0
	leaq	noopCurrent(%rip), %rax
	leaq	noopGetState(%rip), %rcx
	movl	$0, 24(%rbx)
	movups	%xmm0, 32(%rbx)
	movq	%rdx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 80(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	leaq	noopSetState(%rip), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, %xmm6
	movq	$0, 88(%rbx)
	movups	%xmm0, 64(%rbx)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2323:
	.size	uiter_setReplaceable_67, .-uiter_setReplaceable_67
	.p2align 4
	.globl	uiter_setUTF8_67
	.type	uiter_setUTF8_67, @function
uiter_setUTF8_67:
.LFB2333:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L470
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L456
	cmpl	$-1, %edx
	jl	.L456
	pxor	%xmm0, %xmm0
	leaq	utf8IteratorMove(%rip), %rax
	leaq	utf8IteratorGetIndex(%rip), %rcx
	movl	$0, 24(%rdi)
	movq	%rax, %xmm1
	movups	%xmm0, 8(%rdi)
	movq	%rcx, %xmm0
	leaq	utf8IteratorHasPrevious(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	movq	%rsi, (%rdi)
	leaq	utf8IteratorHasNext(%rip), %rcx
	movups	%xmm0, 32(%rdi)
	movq	%rcx, %xmm0
	leaq	utf8IteratorNext(%rip), %rax
	leaq	utf8IteratorCurrent(%rip), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, %xmm3
	leaq	utf8IteratorPrevious(%rip), %rax
	movq	$0, 88(%rdi)
	movups	%xmm0, 48(%rdi)
	movq	%rcx, %xmm0
	leaq	utf8IteratorGetState(%rip), %rcx
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 80(%rdi)
	leaq	utf8IteratorSetState(%rip), %rax
	movups	%xmm0, 64(%rdi)
	movq	%rax, %xmm4
	movq	%rcx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 96(%rdi)
	jne	.L457
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %edx
.L457:
	cmpl	$2, %edx
	movl	$-1, %eax
	movl	%edx, 20(%rbx)
	cmovge	%eax, %edx
	movl	%edx, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	leaq	noopGetIndex(%rip), %rcx
	leaq	noopMove(%rip), %rdx
	movq	$0, (%rbx)
	movups	%xmm0, 8(%rbx)
	movq	%rdx, %xmm5
	movq	%rcx, %xmm0
	leaq	noopHasNext(%rip), %rdi
	punpcklqdq	%xmm5, %xmm0
	leaq	noopCurrent(%rip), %rax
	leaq	noopGetState(%rip), %rcx
	movl	$0, 24(%rbx)
	movups	%xmm0, 32(%rbx)
	movq	%rdi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 80(%rbx)
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	leaq	noopSetState(%rip), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, %xmm6
	movq	$0, 88(%rbx)
	movups	%xmm0, 64(%rbx)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2333:
	.size	uiter_setUTF8_67, .-uiter_setUTF8_67
	.p2align 4
	.globl	uiter_current32_67
	.type	uiter_current32_67, @function
uiter_current32_67:
.LFB2334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	*64(%rdi)
	movl	%eax, %r12d
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L480
.L473:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	testl	$1024, %r12d
	jne	.L475
	movl	$1, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	*40(%rbx)
	movq	%rbx, %rdi
	call	*64(%rbx)
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L476
	sall	$10, %r12d
	leal	-56613888(%rax,%r12), %r12d
.L476:
	movq	%rbx, %rdi
	movl	$1, %edx
	movl	$-1, %esi
	call	*40(%rbx)
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*80(%rbx)
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L481
	testl	%eax, %eax
	js	.L473
.L478:
	movq	%rbx, %rdi
	movl	$1, %edx
	movl	$1, %esi
	call	*40(%rbx)
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	sall	$10, %eax
	leal	-56613888(%r12,%rax), %r12d
	jmp	.L478
	.cfi_endproc
.LFE2334:
	.size	uiter_current32_67, .-uiter_current32_67
	.p2align 4
	.globl	uiter_next32_67
	.type	uiter_next32_67, @function
uiter_next32_67:
.LFB2335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	*72(%rdi)
	movl	%eax, %r12d
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L486
.L482:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*72(%rbx)
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L487
	testl	%eax, %eax
	js	.L482
	movq	%rbx, %rdi
	movl	$1, %edx
	movl	$-1, %esi
	call	*40(%rbx)
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	sall	$10, %r12d
	popq	%rbx
	leal	-56613888(%rax,%r12), %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2335:
	.size	uiter_next32_67, .-uiter_next32_67
	.p2align 4
	.globl	uiter_previous32_67
	.type	uiter_previous32_67, @function
uiter_previous32_67:
.LFB2336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	*80(%rdi)
	movl	%eax, %r12d
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L492
.L488:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	*80(%rbx)
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L493
	testl	%eax, %eax
	js	.L488
	movq	%rbx, %rdi
	movl	$1, %edx
	movl	$1, %esi
	call	*40(%rbx)
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	sall	$10, %eax
	popq	%rbx
	leal	-56613888(%r12,%rax), %r12d
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2336:
	.size	uiter_previous32_67, .-uiter_previous32_67
	.p2align 4
	.globl	uiter_getState_67
	.type	uiter_getState_67, @function
uiter_getState_67:
.LFB2337:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L494
	movq	96(%rdi), %rax
	testq	%rax, %rax
	je	.L494
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L494:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2337:
	.size	uiter_getState_67, .-uiter_getState_67
	.p2align 4
	.globl	uiter_setState_67
	.type	uiter_setState_67, @function
uiter_setState_67:
.LFB2338:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L500
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L507
.L500:
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	testq	%rdi, %rdi
	je	.L508
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.L509
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L509:
	movl	$16, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$1, (%rdx)
	ret
	.cfi_endproc
.LFE2338:
	.size	uiter_setState_67, .-uiter_setState_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
