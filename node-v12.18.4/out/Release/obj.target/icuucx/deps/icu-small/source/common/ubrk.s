	.file	"ubrk.cpp"
	.text
	.p2align 4
	.globl	ubrk_openBinaryRules_67
	.type	ubrk_openBinaryRules_67, @function
ubrk_openBinaryRules_67:
.LFB2458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L16
	movl	%esi, %r15d
	movq	%r8, %r12
	testl	%esi, %esi
	js	.L18
	movq	%rdi, -216(%rbp)
	movl	$640, %edi
	movq	%rdx, %r14
	movl	%ecx, %ebx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L5
	movq	-216(%rbp), %r9
	movl	%r15d, %edx
	movq	%r12, %rcx
	movq	%rax, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_6722RuleBasedBreakIteratorC1EPKhjR10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L19
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	_ZN6icu_6722RuleBasedBreakIteratorD0Ev@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L1
	xorl	%eax, %eax
	leaq	-208(%rbp), %r15
	movslq	%ebx, %rdx
	movq	%r14, %rsi
	leaq	-208(%rbp), %rdi
	movl	$18, %ecx
	rep stosq
	movq	%r15, %rdi
	movq	%r12, %rcx
	movl	$878368812, -208(%rbp)
	movl	$144, -196(%rbp)
	call	utext_openUChars_67@PLT
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*64(%rax)
	jmp	.L1
.L5:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L16
	movl	$7, (%r12)
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%r13d, %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$1, (%r8)
	xorl	%r13d, %r13d
	jmp	.L1
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2458:
	.size	ubrk_openBinaryRules_67, .-ubrk_openBinaryRules_67
	.p2align 4
	.globl	ubrk_safeClone_67
	.type	ubrk_safeClone_67, @function
ubrk_safeClone_67:
.LFB2459:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L37
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rcx, %rbx
	subq	$8, %rsp
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L36
	testq	%rdi, %rdi
	je	.L38
	testq	%rdx, %rdx
	je	.L29
	movl	(%rdx), %eax
	movl	$1, (%rdx)
	testl	%eax, %eax
	je	.L36
.L29:
	movq	(%rdi), %rax
	call	*32(%rax)
	cmpq	$1, %rax
	sbbl	%edx, %edx
	andl	$133, %edx
	subl	$126, %edx
	movl	%edx, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	$1, (%rcx)
.L36:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2459:
	.size	ubrk_safeClone_67, .-ubrk_safeClone_67
	.p2align 4
	.globl	ubrk_close_67
	.type	ubrk_close_67, @function
ubrk_close_67:
.LFB2460:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L39
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L39:
	ret
	.cfi_endproc
.LFE2460:
	.size	ubrk_close_67, .-ubrk_close_67
	.p2align 4
	.globl	ubrk_setText_67
	.type	ubrk_setText_67, @function
ubrk_setText_67:
.LFB2461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-192(%rbp), %r14
	movq	%rcx, %r13
	movl	$18, %ecx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-192(%rbp), %rdi
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	rep stosq
	movq	%r13, %rcx
	movq	%r14, %rdi
	movl	$878368812, -192(%rbp)
	movl	$144, -180(%rbp)
	call	utext_openUChars_67@PLT
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*64(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$168, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2461:
	.size	ubrk_setText_67, .-ubrk_setText_67
	.p2align 4
	.globl	ubrk_setUText_67
	.type	ubrk_setUText_67, @function
ubrk_setUText_67:
.LFB2462:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*64(%rax)
	.cfi_endproc
.LFE2462:
	.size	ubrk_setUText_67, .-ubrk_setUText_67
	.p2align 4
	.globl	ubrk_current_67
	.type	ubrk_current_67, @function
ubrk_current_67:
.LFB2463:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*112(%rax)
	.cfi_endproc
.LFE2463:
	.size	ubrk_current_67, .-ubrk_current_67
	.p2align 4
	.globl	ubrk_next_67
	.type	ubrk_next_67, @function
ubrk_next_67:
.LFB2464:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*104(%rax)
	.cfi_endproc
.LFE2464:
	.size	ubrk_next_67, .-ubrk_next_67
	.p2align 4
	.globl	ubrk_previous_67
	.type	ubrk_previous_67, @function
ubrk_previous_67:
.LFB2465:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*96(%rax)
	.cfi_endproc
.LFE2465:
	.size	ubrk_previous_67, .-ubrk_previous_67
	.p2align 4
	.globl	ubrk_first_67
	.type	ubrk_first_67, @function
ubrk_first_67:
.LFB2466:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*80(%rax)
	.cfi_endproc
.LFE2466:
	.size	ubrk_first_67, .-ubrk_first_67
	.p2align 4
	.globl	ubrk_last_67
	.type	ubrk_last_67, @function
ubrk_last_67:
.LFB2467:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*88(%rax)
	.cfi_endproc
.LFE2467:
	.size	ubrk_last_67, .-ubrk_last_67
	.p2align 4
	.globl	ubrk_preceding_67
	.type	ubrk_preceding_67, @function
ubrk_preceding_67:
.LFB2468:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*128(%rax)
	.cfi_endproc
.LFE2468:
	.size	ubrk_preceding_67, .-ubrk_preceding_67
	.p2align 4
	.globl	ubrk_following_67
	.type	ubrk_following_67, @function
ubrk_following_67:
.LFB2469:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*120(%rax)
	.cfi_endproc
.LFE2469:
	.size	ubrk_following_67, .-ubrk_following_67
	.p2align 4
	.globl	ubrk_getAvailable_67
	.type	ubrk_getAvailable_67, @function
ubrk_getAvailable_67:
.LFB2470:
	.cfi_startproc
	endbr64
	jmp	uloc_getAvailable_67@PLT
	.cfi_endproc
.LFE2470:
	.size	ubrk_getAvailable_67, .-ubrk_getAvailable_67
	.p2align 4
	.globl	ubrk_countAvailable_67
	.type	ubrk_countAvailable_67, @function
ubrk_countAvailable_67:
.LFB2471:
	.cfi_startproc
	endbr64
	jmp	uloc_countAvailable_67@PLT
	.cfi_endproc
.LFE2471:
	.size	ubrk_countAvailable_67, .-ubrk_countAvailable_67
	.p2align 4
	.globl	ubrk_isBoundary_67
	.type	ubrk_isBoundary_67, @function
ubrk_isBoundary_67:
.LFB2472:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*136(%rax)
	.cfi_endproc
.LFE2472:
	.size	ubrk_isBoundary_67, .-ubrk_isBoundary_67
	.p2align 4
	.globl	ubrk_getRuleStatus_67
	.type	ubrk_getRuleStatus_67, @function
ubrk_getRuleStatus_67:
.LFB2473:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*152(%rax)
	.cfi_endproc
.LFE2473:
	.size	ubrk_getRuleStatus_67, .-ubrk_getRuleStatus_67
	.p2align 4
	.globl	ubrk_getRuleStatusVec_67
	.type	ubrk_getRuleStatusVec_67, @function
ubrk_getRuleStatusVec_67:
.LFB2474:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*160(%rax)
	.cfi_endproc
.LFE2474:
	.size	ubrk_getRuleStatusVec_67, .-ubrk_getRuleStatusVec_67
	.p2align 4
	.globl	ubrk_getLocaleByType_67
	.type	ubrk_getLocaleByType_67, @function
ubrk_getLocaleByType_67:
.LFB2475:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L62
	jmp	_ZNK6icu_6713BreakIterator11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L58
	movl	$1, (%rdx)
.L58:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2475:
	.size	ubrk_getLocaleByType_67, .-ubrk_getLocaleByType_67
	.p2align 4
	.globl	ubrk_refreshUText_67
	.type	ubrk_refreshUText_67, @function
ubrk_refreshUText_67:
.LFB2476:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*176(%rax)
	.cfi_endproc
.LFE2476:
	.size	ubrk_refreshUText_67, .-ubrk_refreshUText_67
	.p2align 4
	.globl	ubrk_getBinaryRules_67
	.type	ubrk_getBinaryRules_67, @function
ubrk_getBinaryRules_67:
.LFB2477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L64
	movl	%edx, %r13d
	testq	%rsi, %rsi
	movq	%rsi, %r14
	movq	%rcx, %rbx
	sete	%dl
	testl	%r13d, %r13d
	setg	%al
	testb	%al, %dl
	jne	.L68
	testl	%r13d, %r13d
	js	.L68
	testq	%rdi, %rdi
	je	.L68
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6722RuleBasedBreakIteratorE(%rip), %rdx
	leaq	_ZTIN6icu_6713BreakIteratorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L68
	movq	(%rax), %rax
	leaq	-44(%rbp), %rsi
	call	*200(%rax)
	movl	-44(%rbp), %r12d
	testl	%r12d, %r12d
	js	.L83
	testq	%r14, %r14
	je	.L64
	cmpl	%r13d, %r12d
	jle	.L70
	movl	$15, (%rbx)
	.p2align 4,,10
	.p2align 3
.L64:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$8, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L70:
	movl	%r12d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L64
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2477:
	.size	ubrk_getBinaryRules_67, .-ubrk_getBinaryRules_67
	.p2align 4
	.globl	ubrk_open_67
	.type	ubrk_open_67, @function
ubrk_open_67:
.LFB2456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L103
	movq	%r8, %r12
	cmpl	$4, %edi
	ja	.L88
	movl	%edi, %edi
	movq	%rdx, %r14
	leaq	.L90(%rip), %rdx
	movl	%ecx, %ebx
	movslq	(%rdx,%rdi,4), %rax
	leaq	-288(%rbp), %r15
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	addq	%rdx, %rax
	xorl	%edx, %edx
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L90:
	.long	.L94-.L90
	.long	.L93-.L90
	.long	.L92-.L90
	.long	.L91-.L90
	.long	.L89-.L90
	.text
.L88:
	movl	$1, (%r8)
.L103:
	xorl	%r13d, %r13d
.L85:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$392, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L91:
	.cfi_restore_state
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
.L95:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L103
	testq	%r13, %r13
	je	.L105
	testq	%r14, %r14
	je	.L85
	xorl	%eax, %eax
	leaq	-432(%rbp), %r15
	movslq	%ebx, %rdx
	movq	%r14, %rsi
	leaq	-432(%rbp), %rdi
	movl	$18, %ecx
	rep stosq
	movq	%r15, %rdi
	movq	%r12, %rcx
	movl	$878368812, -432(%rbp)
	movl	$144, -420(%rbp)
	call	utext_openUChars_67@PLT
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*64(%rax)
	jmp	.L85
.L89:
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713BreakIterator19createTitleInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L95
.L94:
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L95
.L93:
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L95
.L92:
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713BreakIterator18createLineInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$7, (%r12)
	jmp	.L85
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2456:
	.size	ubrk_open_67, .-ubrk_open_67
	.p2align 4
	.globl	ubrk_openRules_67
	.type	ubrk_openRules_67, @function
ubrk_openRules_67:
.LFB2457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -276(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L110
	movl	(%r9), %ecx
	movq	%r9, %r12
	xorl	%r13d, %r13d
	testl	%ecx, %ecx
	jg	.L106
	leaq	-128(%rbp), %r15
	movq	%r8, %r14
	movq	%rdx, %rbx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6715RBBIRuleBuilder28createRuleBasedBreakIteratorERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode@PLT
	movq	%rax, %r13
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L112
	testq	%rbx, %rbx
	je	.L108
	movslq	-276(%rbp), %rdx
	xorl	%eax, %eax
	movq	%rbx, %rsi
	leaq	-272(%rbp), %r14
	leaq	-272(%rbp), %rdi
	movl	$18, %ecx
	rep stosq
	movq	%r12, %rcx
	movq	%r14, %rdi
	movl	$878368812, -272(%rbp)
	movl	$144, -260(%rbp)
	call	utext_openUChars_67@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	*64(%rax)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$248, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	xorl	%r13d, %r13d
.L108:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L110:
	xorl	%r13d, %r13d
	jmp	.L106
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2457:
	.size	ubrk_openRules_67, .-ubrk_openRules_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
