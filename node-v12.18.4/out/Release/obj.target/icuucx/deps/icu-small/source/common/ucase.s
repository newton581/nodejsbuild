	.file	"ucase.cpp"
	.text
	.p2align 4
	.type	_ZL24_enumPropertyStartsRangePKviij, @function
_ZL24_enumPropertyStartsRangePKviij:
.LFB3007:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*8(%rax)
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3007:
	.size	_ZL24_enumPropertyStartsRangePKviij, .-_ZL24_enumPropertyStartsRangePKviij
	.p2align 4
	.type	ucase_addCaseClosure_67.part.0, @function
ucase_addCaseClosure_67.part.0:
.LFB3837:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpl	$55295, %edi
	ja	.L5
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L70:
	movzwl	(%rdx,%rax,2), %ecx
	movl	%r12d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movswl	(%rdx,%rax), %eax
	testb	$8, %al
	je	.L36
	shrq	$3, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %rdx
	xorl	%r13d, %r13d
	andl	$8190, %eax
	addq	%rdx, %rax
	movzwl	(%rax), %r15d
	leaq	2(%rax), %rdi
	movq	%rdi, -56(%rbp)
	movl	%r15d, %eax
	movl	%r15d, %r14d
	andw	$256, %ax
	movw	%ax, -58(%rbp)
	je	.L14
.L17:
	btl	%r13d, %r15d
	jnc	.L15
	movl	%r13d, %ecx
	movl	$1, %eax
	leaq	_ZL11flagsOffset(%rip), %rdi
	sall	%cl, %eax
	movq	-56(%rbp), %rcx
	subl	$1, %eax
	andl	%r15d, %eax
	cltq
	movzbl	(%rdi,%rax), %eax
	movq	(%rbx), %rdi
	addl	%eax, %eax
	cltq
	leaq	(%rcx,%rax,2), %rax
	movzwl	(%rax), %r12d
	movl	%r12d, %esi
	movzwl	2(%rax), %r12d
	sall	$16, %esi
	orl	%esi, %r12d
	movl	%r12d, %esi
	call	*8(%rbx)
.L15:
	addl	$1, %r13d
	cmpl	$4, %r13d
	jne	.L17
	testb	$16, %r14b
	jne	.L21
	movl	%r14d, %ecx
	andw	$128, %cx
	testb	$64, %r14b
	je	.L25
	movq	%r14, %rax
	leaq	_ZL11flagsOffset(%rip), %r15
	andl	$63, %eax
	movzbl	(%r15,%rax), %eax
.L26:
	leaq	0(,%rax,4), %r12
	andl	$1020, %r12d
	addq	-56(%rbp), %r12
	movzwl	2(%r12), %r13d
	addq	$4, %r12
	andl	$15, %r13d
	testw	%cx, %cx
	jne	.L71
.L28:
	testl	%r13d, %r13d
	je	.L4
	xorl	%eax, %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rbx), %rdi
	call	*8(%rbx)
	cmpl	%r13d, %r14d
	jge	.L4
.L34:
	movl	%r14d, %eax
.L35:
	movslq	%eax, %rdx
	leal	1(%rax), %r14d
	movzwl	(%r12,%rdx,2), %esi
	leaq	(%rdx,%rdx), %rcx
	movl	%esi, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L33
	leal	2(%rax), %r14d
	movzwl	2(%r12,%rcx), %eax
	sall	$10, %esi
	movq	(%rbx), %rdi
	leal	-56613888(%rax,%rsi), %esi
	call	*8(%rbx)
	cmpl	%r13d, %r14d
	jl	.L34
.L4:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	btl	%r13d, %r15d
	jnc	.L18
	movl	%r13d, %ecx
	movl	$1, %eax
	leaq	_ZL11flagsOffset(%rip), %rdi
	sall	%cl, %eax
	movq	-56(%rbp), %rcx
	subl	$1, %eax
	andl	%r15d, %eax
	cltq
	movzbl	(%rdi,%rax), %eax
	movq	(%rbx), %rdi
	movzwl	(%rcx,%rax,2), %r12d
	movl	%r12d, %esi
	call	*8(%rbx)
.L18:
	addl	$1, %r13d
	cmpl	$4, %r13d
	jne	.L14
	testb	$16, %r14b
	jne	.L19
	movl	%r14d, %ecx
	andw	$128, %cx
	testb	$64, %r14b
	je	.L25
	movq	%r14, %rax
	leaq	_ZL11flagsOffset(%rip), %r15
	andl	$63, %eax
	movzbl	(%r15,%rax), %eax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$65535, %edi
	jbe	.L72
	cmpl	$1114111, %edi
	jbe	.L73
	movl	$6832, %edx
.L9:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rax
	movswl	(%rax,%rdx), %eax
.L36:
	testb	$3, %al
	je	.L4
	sarl	$7, %eax
	je	.L4
	movq	(%rbx), %rdi
	leal	(%r12,%rax), %esi
	movq	8(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r14, %rax
	leaq	_ZL11flagsOffset(%rip), %r15
	movq	-56(%rbp), %rdi
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	addl	%eax, %eax
	cltq
	leaq	(%rdi,%rax,2), %rsi
	movzwl	(%rsi), %eax
	sall	$16, %eax
	movl	%eax, %ecx
	movzwl	2(%rsi), %eax
	orl	%ecx, %eax
.L39:
	leal	(%rax,%r12), %esi
	subl	%eax, %r12d
	testw	$1024, %r14w
	movq	(%rbx), %rdi
	cmovne	%r12d, %esi
	call	*8(%rbx)
	movl	%r14d, %ecx
	andw	$128, %cx
	testb	$64, %r14b
	je	.L25
	movq	%r14, %rax
	andl	$63, %eax
	cmpw	$0, -58(%rbp)
	movzbl	(%r15,%rax), %eax
	jne	.L26
.L40:
	movq	-56(%rbp), %rdi
	leaq	(%rdi,%rax,2), %r12
	movzwl	(%r12), %r13d
	addq	$2, %r12
	andl	$15, %r13d
	testw	%cx, %cx
	je	.L28
	movq	%r14, %rdx
	andl	$127, %edx
	movzbl	(%r15,%rdx), %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r14, %rax
	leaq	_ZL11flagsOffset(%rip), %r15
	movq	-56(%rbp), %rdi
	andl	$15, %eax
	movzbl	(%r15,%rax), %eax
	movzwl	(%rdi,%rax,2), %eax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L73:
	cmpl	$919551, %edi
	jg	.L45
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L25:
	testw	%cx, %cx
	je	.L4
	movq	%r14, %rdx
	leaq	_ZL11flagsOffset(%rip), %rax
	xorl	%r13d, %r13d
	andl	$127, %edx
	cmpw	$0, -58(%rbp)
	movzbl	(%rax,%rdx), %eax
	jne	.L30
.L38:
	movq	-56(%rbp), %rdi
	leaq	(%rdi,%rax,2), %rdx
	movzwl	(%rdx), %eax
	movl	%eax, %r12d
.L31:
	andl	$15, %eax
	leaq	2(%rdx,%rax,2), %r15
	movl	%r12d, %eax
	sarl	$4, %eax
	andl	$15, %eax
	movl	%eax, %r14d
	je	.L32
	movl	%eax, %edx
	movq	%r15, %rsi
	movq	(%rbx), %rdi
	call	*24(%rbx)
	movslq	%r14d, %rax
	leaq	(%r15,%rax,2), %r15
.L32:
	movl	%r12d, %eax
	sarl	$12, %r12d
	sarl	$8, %eax
	movslq	%r12d, %r12
	andl	$15, %eax
	addq	%rax, %r12
	leaq	(%r15,%r12,2), %r12
	jmp	.L28
.L71:
	movq	%r14, %rdx
	andl	$127, %edx
	movzbl	(%r15,%rdx), %eax
.L30:
	movq	-56(%rbp), %r14
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %r14
	movzwl	(%r14), %eax
	movzwl	2(%r14), %r12d
	leaq	2(%r14), %rdx
	sall	$16, %eax
	orl	%r12d, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$24704, %edx
	jmp	.L9
	.cfi_endproc
.LFE3837:
	.size	ucase_addCaseClosure_67.part.0, .-ucase_addCaseClosure_67.part.0
	.p2align 4
	.type	_ZL14toUpperOrTitleiPFiPvaES_PPKDsia, @function
_ZL14toUpperOrTitleiPFiPvaES_PPKDsia:
.LFB3032:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$40, %rsp
	cmpl	$55295, %edi
	ja	.L75
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L133:
	movzwl	(%rdx,%rax,2), %ecx
	movl	%ebx, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %r15d
	testb	$8, %r15b
	je	.L109
	movq	%r15, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %r11
	shrq	$3, %rax
	andl	$8190, %eax
	addq	%r11, %rax
	movzwl	(%rax), %r10d
	leaq	2(%rax), %rdi
	movq	%rdi, -72(%rbp)
	testw	$16384, %r10w
	je	.L84
	cmpl	$2, %r8d
	jne	.L119
	movl	$304, %eax
	cmpl	$105, %ebx
	je	.L74
.L119:
	cmpl	$3, %r8d
	sete	%sil
	cmpl	$775, %ebx
	sete	%al
	testb	%al, %sil
	je	.L87
	testq	%r12, %r12
	je	.L87
	movl	$-1, %esi
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L138:
	movl	%eax, %esi
	sarl	$5, %esi
.L134:
	movslq	%esi, %rsi
	andl	$31, %eax
	movzwl	(%rdx,%rsi,2), %esi
	leal	(%rax,%rsi,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %eax
	testb	$8, %al
	je	.L136
	salq	$48, %rax
	shrq	$52, %rax
	movzwl	(%r11,%rax,2), %eax
	sarl	$7, %eax
.L136:
	andl	$96, %eax
	cmpl	$32, %eax
	je	.L95
	xorl	%esi, %esi
	cmpl	$96, %eax
	jne	.L87
.L96:
	movl	%r9d, -56(%rbp)
	movq	%r13, %rdi
	movl	%r10d, -52(%rbp)
	movq	%rdx, -64(%rbp)
	call	*%r12
	movl	-52(%rbp), %r10d
	movl	-56(%rbp), %r9d
	testl	%eax, %eax
	js	.L87
	cmpl	$55295, %eax
	movq	-64(%rbp), %rdx
	leaq	_ZL22ucase_props_exceptions(%rip), %r11
	jle	.L138
	cmpl	$65535, %eax
	jg	.L90
	cmpl	$56320, %eax
	movl	$320, %esi
	movl	$0, %edi
	cmovl	%esi, %edi
	movl	%eax, %esi
	sarl	$5, %esi
	addl	%edi, %esi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L84:
	testb	$-128, %r10b
	je	.L87
	movq	%r10, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$127, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r10w
	jne	.L97
	leaq	(%rdi,%rax,2), %rdi
	movzwl	(%rdi), %esi
.L98:
	movl	%esi, %eax
	movl	%esi, %edx
	sarl	$4, %eax
	andl	$15, %edx
	andl	$15, %eax
	leaq	1(%rdx,%rax), %rax
	leaq	(%rdi,%rax,2), %rdi
	movl	%esi, %eax
	sarl	$8, %eax
	andl	$15, %eax
	testb	%r9b, %r9b
	jne	.L99
	movslq	%eax, %rdx
	movl	%esi, %eax
	sarl	$12, %eax
	leaq	(%rdi,%rdx,2), %rdi
	andl	$15, %eax
.L99:
	testl	%eax, %eax
	jne	.L139
.L87:
	testb	$16, %r10b
	je	.L100
	movl	%r15d, %ecx
	andl	$3, %ecx
	cmpw	$1, %cx
	je	.L140
.L100:
	testb	%r9b, %r9b
	jne	.L104
	testb	$8, %r10b
	je	.L104
	movl	$7, %eax
.L105:
	andl	%r10d, %eax
	leaq	_ZL11flagsOffset(%rip), %rdx
	cltq
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r10w
	jne	.L106
	movq	-72(%rbp), %rdi
	movzwl	(%rdi,%rax,2), %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$3, %eax
	testb	$4, %r10b
	jne	.L105
.L82:
	movl	%ebx, %eax
	notl	%eax
.L74:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	cmpl	$1114111, %edi
	jbe	.L141
	movl	$6832, %eax
.L79:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movzwl	(%rdx,%rax), %r15d
.L109:
	movl	%r15d, %eax
	andl	$3, %eax
	cmpw	$1, %ax
	jne	.L82
	movswl	%r15w, %eax
	sarl	$7, %eax
	leal	(%rax,%rbx), %eax
.L83:
	cmpl	%ebx, %eax
	je	.L82
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	cmpl	$65535, %edi
	ja	.L77
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L106:
	movq	-72(%rbp), %r15
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %r15
	movzwl	(%r15), %eax
	sall	$16, %eax
	movl	%eax, %ecx
	movzwl	2(%r15), %eax
	orl	%ecx, %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L141:
	cmpl	$919551, %edi
	jg	.L112
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L90:
	cmpl	$1114111, %eax
	ja	.L115
	cmpl	$919551, %eax
	jle	.L142
	movl	$24704, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L97:
	salq	$2, %rax
	andl	$1020, %eax
	leaq	(%rdi,%rax), %rdx
	movzwl	(%rdx), %esi
	movzwl	2(%rdx), %eax
	leaq	2(%rdx), %rdi
	sall	$16, %esi
	orl	%eax, %esi
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$24704, %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L115:
	movl	$6832, %eax
.L92:
	movzwl	(%rdx,%rax), %eax
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%rdi, (%r14)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r10, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r10w
	jne	.L101
	movq	-72(%rbp), %rdi
	movzwl	(%rdi,%rax,2), %edx
.L102:
	leal	(%rdx,%rbx), %eax
	subl	%edx, %ebx
	testw	$1024, %r10w
	cmovne	%ebx, %eax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L142:
	movl	%eax, %esi
	sarl	$11, %esi
	addl	$2080, %esi
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %edi
	movl	%eax, %esi
	sarl	$5, %esi
	andl	$63, %esi
	addl	%edi, %esi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L95:
	movq	$0, (%r14)
	xorl	%eax, %eax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L101:
	movq	-72(%rbp), %r15
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %r15
	movzwl	(%r15), %edx
	movl	%edx, %eax
	movzwl	2(%r15), %edx
	sall	$16, %eax
	orl	%eax, %edx
	jmp	.L102
	.cfi_endproc
.LFE3032:
	.size	_ZL14toUpperOrTitleiPFiPvaES_PPKDsia, .-_ZL14toUpperOrTitleiPFiPvaES_PPKDsia
	.p2align 4
	.globl	ucase_addPropertyStarts_67
	.type	ucase_addPropertyStarts_67, @function
ucase_addPropertyStarts_67:
.LFB3008:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L145
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%rdi, %rcx
	leaq	_ZL24_enumPropertyStartsRangePKviij(%rip), %rdx
	xorl	%esi, %esi
	leaq	32+_ZL21ucase_props_singleton(%rip), %rdi
	jmp	utrie2_enum_67@PLT
	.cfi_endproc
.LFE3008:
	.size	ucase_addPropertyStarts_67, .-ucase_addPropertyStarts_67
	.p2align 4
	.globl	ucase_getTrie_67
	.type	ucase_getTrie_67, @function
ucase_getTrie_67:
.LFB3009:
	.cfi_startproc
	endbr64
	leaq	32+_ZL21ucase_props_singleton(%rip), %rax
	ret
	.cfi_endproc
.LFE3009:
	.size	ucase_getTrie_67, .-ucase_getTrie_67
	.p2align 4
	.globl	ucase_tolower_67
	.type	ucase_tolower_67, @function
ucase_tolower_67:
.LFB3010:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L148
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L176:
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movswl	(%rdx,%rax), %edx
	testb	$8, %dl
	je	.L159
	movq	%rdx, %rcx
	leaq	_ZL22ucase_props_exceptions(%rip), %rax
	shrq	$3, %rcx
	andl	$8190, %ecx
	addq	%rax, %rcx
	movzwl	(%rcx), %esi
	testb	$16, %sil
	jne	.L177
.L155:
	movl	%edi, %eax
	testb	$1, %sil
	je	.L147
	movzwl	2(%rcx), %eax
	testw	$256, %si
	jne	.L178
.L147:
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	andl	$2, %edx
	je	.L155
	movq	%rsi, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	addq	$2, %rcx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %si
	je	.L179
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rcx
	movzwl	(%rcx), %edx
	movl	%edx, %eax
	movzwl	2(%rcx), %edx
	sall	$16, %eax
	orl	%eax, %edx
.L157:
	leal	(%rdx,%rdi), %eax
	subl	%edx, %edi
	testw	$1024, %si
	cmovne	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	cmpl	$1114111, %edi
	jbe	.L180
	movl	$6832, %eax
.L152:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movswl	(%rdx,%rax), %edx
.L159:
	movl	%edi, %eax
	testb	$2, %dl
	je	.L147
	sarl	$7, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	cmpl	$65535, %edi
	ja	.L150
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L180:
	cmpl	$919551, %edi
	jg	.L162
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L178:
	sall	$16, %eax
	movl	%eax, %edi
	movzwl	4(%rcx), %eax
	orl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$24704, %eax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L179:
	movzwl	(%rcx,%rax,2), %edx
	jmp	.L157
	.cfi_endproc
.LFE3010:
	.size	ucase_tolower_67, .-ucase_tolower_67
	.p2align 4
	.globl	ucase_toupper_67
	.type	ucase_toupper_67, @function
ucase_toupper_67:
.LFB3011:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	cmpl	$55295, %edi
	ja	.L182
	movl	%edi, %edx
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$5, %edx
	movslq	%edx, %rdx
.L205:
	movzwl	(%rcx,%rdx,2), %esi
	movl	%eax, %edx
	andl	$31, %edx
	leal	(%rdx,%rsi,4), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	movswl	(%rcx,%rdx), %edx
	testb	$8, %dl
	je	.L194
	movq	%rdx, %rcx
	leaq	_ZL22ucase_props_exceptions(%rip), %rsi
	shrq	$3, %rcx
	andl	$8190, %ecx
	addq	%rsi, %rcx
	leaq	2(%rcx), %rsi
	movzwl	(%rcx), %ecx
	testb	$16, %cl
	jne	.L206
.L189:
	testb	$4, %cl
	je	.L200
	movq	%rcx, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$3, %eax
	movzbl	(%rdx,%rax), %eax
	andb	$1, %ch
	jne	.L193
	movzwl	(%rsi,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	andl	$3, %edx
	cmpw	$1, %dx
	jne	.L189
	movq	%rcx, %rdx
	leaq	_ZL11flagsOffset(%rip), %rdi
	andl	$15, %edx
	movzbl	(%rdi,%rdx), %edx
	testb	$1, %ch
	je	.L207
	salq	$2, %rdx
	andl	$1020, %edx
	addq	%rdx, %rsi
	movzwl	(%rsi), %edx
	movl	%edx, %edi
	movzwl	2(%rsi), %edx
	sall	$16, %edi
	orl	%edi, %edx
.L191:
	leal	(%rax,%rdx), %esi
	subl	%edx, %eax
	andb	$4, %ch
	cmove	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	cmpl	$1114111, %edi
	jbe	.L208
	movl	$6832, %edx
.L186:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	movswl	(%rcx,%rdx), %edx
.L194:
	movl	%edx, %ecx
	andl	$3, %ecx
	cmpw	$1, %cx
	je	.L209
.L200:
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	cmpl	$65535, %edi
	ja	.L184
	cmpl	$56320, %edi
	movl	$320, %edx
	movl	$0, %ecx
	cmovl	%edx, %ecx
	movl	%edi, %edx
	sarl	$5, %edx
	addl	%ecx, %edx
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	movslq	%edx, %rdx
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L209:
	sarl	$7, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	cmpl	$919551, %edi
	jg	.L197
	movl	%edi, %edx
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %esi
	movl	%edi, %edx
	sarl	$5, %edx
	andl	$63, %edx
	addl	%esi, %edx
	movslq	%edx, %rdx
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L193:
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rsi
	movzwl	(%rsi), %eax
	movzwl	2(%rsi), %edi
	sall	$16, %eax
	orl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	movl	$24704, %edx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L207:
	movzwl	(%rsi,%rdx,2), %edx
	jmp	.L191
	.cfi_endproc
.LFE3011:
	.size	ucase_toupper_67, .-ucase_toupper_67
	.p2align 4
	.globl	ucase_totitle_67
	.type	ucase_totitle_67, @function
ucase_totitle_67:
.LFB3012:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L211
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L235:
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movswl	(%rdx,%rax), %edx
	testb	$8, %dl
	je	.L224
	movq	%rdx, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %rcx
	shrq	$3, %rax
	andl	$8190, %eax
	addq	%rcx, %rax
	movzwl	(%rax), %ecx
	leaq	2(%rax), %rsi
	testb	$16, %cl
	jne	.L236
.L218:
	testb	$8, %cl
	jne	.L229
	movl	%edi, %eax
	testb	$4, %cl
	je	.L210
	movl	$3, %eax
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L236:
	andl	$3, %edx
	cmpw	$1, %dx
	jne	.L218
	movq	%rcx, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	testb	$1, %ch
	je	.L237
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rsi
	movzwl	(%rsi), %edx
	movl	%edx, %eax
	movzwl	2(%rsi), %edx
	sall	$16, %eax
	orl	%eax, %edx
.L220:
	leal	(%rdx,%rdi), %eax
	subl	%edx, %edi
	andb	$4, %ch
	cmovne	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	cmpl	$1114111, %edi
	jbe	.L238
	movl	$6832, %eax
.L215:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movswl	(%rdx,%rax), %edx
.L224:
	movl	%edx, %ecx
	movl	%edi, %eax
	andl	$3, %ecx
	cmpw	$1, %cx
	je	.L239
.L210:
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	cmpl	$65535, %edi
	ja	.L213
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$7, %eax
.L222:
	andl	%ecx, %eax
	leaq	_ZL11flagsOffset(%rip), %rdx
	cltq
	movzbl	(%rdx,%rax), %eax
	andb	$1, %ch
	jne	.L223
	movzwl	(%rsi,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rsi
	movzwl	(%rsi), %eax
	movzwl	2(%rsi), %edi
	sall	$16, %eax
	orl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	sarl	$7, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	cmpl	$919551, %edi
	jg	.L227
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L227:
	movl	$24704, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L237:
	movzwl	(%rsi,%rax,2), %edx
	jmp	.L220
	.cfi_endproc
.LFE3012:
	.size	ucase_totitle_67, .-ucase_totitle_67
	.p2align 4
	.globl	ucase_addCaseClosure_67
	.type	ucase_addCaseClosure_67, @function
ucase_addCaseClosure_67:
.LFB3013:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	cmpl	$304, %edi
	je	.L241
	jg	.L242
	cmpl	$73, %edi
	je	.L243
	cmpl	$105, %edi
	jne	.L245
	movq	8(%rsi), %rax
	movq	(%r8), %rdi
	movl	$73, %esi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L242:
	cmpl	$305, %edi
	jne	.L245
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r8, %rsi
	jmp	ucase_addCaseClosure_67.part.0
	.p2align 4,,10
	.p2align 3
.L241:
	movq	24(%rsi), %rax
	movq	(%r8), %rdi
	movl	$2, %edx
	leaq	_ZL4iDot(%rip), %rsi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L243:
	movq	8(%rsi), %rax
	movq	(%r8), %rdi
	movl	$105, %esi
	jmp	*%rax
	.cfi_endproc
.LFE3013:
	.size	ucase_addCaseClosure_67, .-ucase_addCaseClosure_67
	.p2align 4
	.globl	ucase_addStringCaseClosure_67
	.type	ucase_addStringCaseClosure_67, @function
ucase_addStringCaseClosure_67:
.LFB3015:
	.cfi_startproc
	endbr64
	leal	-2(%rsi), %eax
	cmpl	$1, %eax
	ja	.L265
	testq	%rdi, %rdi
	je	.L265
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$73, %r9d
	xorl	%r8d, %r8d
	leaq	10+_ZL18ucase_props_unfold(%rip), %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzwl	(%rdi), %ecx
.L249:
	leal	(%r9,%r8), %edx
	sarl	%edx
	leal	(%rdx,%rdx,4), %eax
	cltq
	leaq	(%r10,%rax,2), %r15
	movzwl	(%r15), %eax
	testl	%eax, %eax
	je	.L250
	movl	%ecx, %ebx
	subl	%eax, %ebx
	movl	%ebx, %eax
	jne	.L251
	movzwl	2(%r15), %r11d
	movzwl	2(%rdi), %eax
	testl	%r11d, %r11d
	je	.L250
	subl	%r11d, %eax
	jne	.L251
	cmpl	$2, %esi
	je	.L252
	movzwl	4(%r15), %r11d
	movzwl	4(%rdi), %eax
	testl	%r11d, %r11d
	je	.L250
	subl	%r11d, %eax
	jne	.L251
.L269:
	movl	$3, %r13d
	leaq	_ZL4iDot(%rip), %rbx
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L292:
	cmpl	$105, %r12d
	jne	.L261
	movq	(%r14), %rdi
	movl	$73, %esi
	call	*8(%r14)
	.p2align 4,,10
	.p2align 3
.L262:
	cmpl	$4, %r13d
	jne	.L255
.L254:
	movslq	%r13d, %rax
	leaq	(%rax,%rax), %rcx
	movzwl	(%r15,%rax,2), %eax
	testw	%ax, %ax
	je	.L255
	movzwl	%ax, %r12d
	andl	$64512, %eax
	leal	1(%r13), %edx
	cmpl	$55296, %eax
	je	.L291
	movl	%edx, %r13d
.L256:
	movq	(%r14), %rdi
	movl	%r12d, %esi
	call	*8(%r14)
	cmpl	$304, %r12d
	je	.L257
	jg	.L258
	cmpl	$73, %r12d
	jne	.L292
	movq	(%r14), %rdi
	movl	$105, %esi
	call	*8(%r14)
	cmpl	$4, %r13d
	je	.L254
	.p2align 4,,10
	.p2align 3
.L255:
	movl	$1, %eax
.L247:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpl	$305, %r12d
	je	.L262
.L261:
	movq	%r14, %rsi
	movl	%r12d, %edi
	call	ucase_addCaseClosure_67.part.0
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L251:
	testl	%eax, %eax
	jns	.L250
.L264:
	cmpl	%r8d, %edx
	jle	.L266
.L263:
	movl	%edx, %r9d
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L291:
	movzwl	2(%r15,%rcx), %eax
	sall	$10, %r12d
	addl	$2, %r13d
	leal	-56613888(%rax,%r12), %r12d
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L257:
	movq	(%r14), %rdi
	movl	$2, %edx
	movq	%rbx, %rsi
	call	*24(%r14)
	jmp	.L262
.L252:
	cmpw	$0, 4(%r15)
	je	.L269
	jmp	.L264
.L266:
	xorl	%eax, %eax
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L250:
	leal	1(%rdx), %r8d
	cmpl	%r9d, %r8d
	jge	.L266
	movl	%r9d, %edx
	jmp	.L263
	.cfi_endproc
.LFE3015:
	.size	ucase_addStringCaseClosure_67, .-ucase_addStringCaseClosure_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723FullCaseFoldingIteratorC2Ev
	.type	_ZN6icu_6723FullCaseFoldingIteratorC2Ev, @function
_ZN6icu_6723FullCaseFoldingIteratorC2Ev:
.LFB3017:
	.cfi_startproc
	endbr64
	movdqa	.LC0(%rip), %xmm0
	leaq	10+_ZL18ucase_props_unfold(%rip), %rax
	movl	$3, 24(%rdi)
	movq	%rax, (%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE3017:
	.size	_ZN6icu_6723FullCaseFoldingIteratorC2Ev, .-_ZN6icu_6723FullCaseFoldingIteratorC2Ev
	.globl	_ZN6icu_6723FullCaseFoldingIteratorC1Ev
	.set	_ZN6icu_6723FullCaseFoldingIteratorC1Ev,_ZN6icu_6723FullCaseFoldingIteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723FullCaseFoldingIterator4nextERNS_13UnicodeStringE
	.type	_ZN6icu_6723FullCaseFoldingIterator4nextERNS_13UnicodeStringE, @function
_ZN6icu_6723FullCaseFoldingIterator4nextERNS_13UnicodeStringE:
.LFB3019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$16, %rsp
	movslq	12(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	20(%rbx), %eax
	movl	%eax, %ecx
	imull	%edx, %ecx
	movslq	%ecx, %rcx
	leaq	(%rsi,%rcx,2), %r12
	movslq	24(%rbx), %rcx
	cmpl	%ecx, %edx
	jle	.L295
	cmpw	$0, (%r12,%rcx,2)
	je	.L295
.L296:
	cmpl	%eax, 8(%rbx)
	jle	.L301
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L298
	movslq	%ecx, %rax
	leaq	(%r12,%rax,2), %rax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L304:
	subq	$2, %rax
	subl	$1, %ecx
	je	.L298
.L299:
	cmpw	$0, -2(%rax)
	je	.L304
.L298:
	leaq	-32(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r12, -32(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movl	24(%rbx), %edx
	leal	1(%rdx), %eax
	movl	%eax, 24(%rbx)
	movslq	%edx, %rax
	leaq	(%rax,%rax), %rsi
	movzwl	(%r12,%rax,2), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L294
	addl	$2, %edx
	sall	$10, %eax
	movl	%edx, 24(%rbx)
	movzwl	2(%r12,%rsi), %edx
	leal	-56613888(%rdx,%rax), %eax
.L294:
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L305
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	leaq	(%r12,%rdx,2), %r12
	movl	16(%rbx), %edx
	addl	$1, %eax
	movl	%eax, 20(%rbx)
	movl	%edx, 24(%rbx)
	jmp	.L296
.L301:
	movl	$-1, %eax
	jmp	.L294
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3019:
	.size	_ZN6icu_6723FullCaseFoldingIterator4nextERNS_13UnicodeStringE, .-_ZN6icu_6723FullCaseFoldingIterator4nextERNS_13UnicodeStringE
	.p2align 4
	.globl	ucase_getType_67
	.type	ucase_getType_67, @function
ucase_getType_67:
.LFB3020:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L307
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L314:
	movzwl	(%rdx,%rax,2), %eax
	andl	$31, %edi
	leal	(%rdi,%rax,4), %eax
	cltq
	addq	%rax, %rax
.L308:
	movzwl	(%rdx,%rax), %eax
	andl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	cmpl	$65535, %edi
	ja	.L309
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L309:
	movl	$6832, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cmpl	$1114111, %edi
	ja	.L308
	movl	$24704, %eax
	cmpl	$919551, %edi
	jg	.L308
	movl	%edi, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L314
	.cfi_endproc
.LFE3020:
	.size	ucase_getType_67, .-ucase_getType_67
	.p2align 4
	.globl	ucase_getTypeOrIgnorable_67
	.type	ucase_getTypeOrIgnorable_67, @function
ucase_getTypeOrIgnorable_67:
.LFB3021:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L316
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L323:
	movzwl	(%rdx,%rax,2), %eax
	andl	$31, %edi
	leal	(%rdi,%rax,4), %eax
	cltq
	addq	%rax, %rax
.L317:
	movzwl	(%rdx,%rax), %eax
	andl	$7, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	cmpl	$65535, %edi
	ja	.L318
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L318:
	movl	$6832, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cmpl	$1114111, %edi
	ja	.L317
	movl	$24704, %eax
	cmpl	$919551, %edi
	jg	.L317
	movl	%edi, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L323
	.cfi_endproc
.LFE3021:
	.size	ucase_getTypeOrIgnorable_67, .-ucase_getTypeOrIgnorable_67
	.p2align 4
	.globl	ucase_isSoftDotted_67
	.type	ucase_isSoftDotted_67, @function
ucase_isSoftDotted_67:
.LFB3023:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L325
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L337:
	movzwl	(%rdx,%rax,2), %eax
	andl	$31, %edi
	leal	(%rdi,%rax,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %eax
	testb	$8, %al
	je	.L338
	salq	$48, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %rdx
	shrq	$52, %rax
	movzwl	(%rdx,%rax,2), %eax
	sarl	$7, %eax
.L338:
	andl	$96, %eax
	cmpl	$32, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	cmpl	$65535, %edi
	ja	.L327
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L327:
	cmpl	$1114111, %edi
	jbe	.L339
	movl	$6832, %eax
.L329:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movzwl	(%rdx,%rax), %eax
	andl	$96, %eax
	cmpl	$32, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	cmpl	$919551, %edi
	jg	.L335
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L335:
	movl	$24704, %eax
	jmp	.L329
	.cfi_endproc
.LFE3023:
	.size	ucase_isSoftDotted_67, .-ucase_isSoftDotted_67
	.p2align 4
	.globl	ucase_isCaseSensitive_67
	.type	ucase_isCaseSensitive_67, @function
ucase_isCaseSensitive_67:
.LFB3024:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L341
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L352:
	movzwl	(%rdx,%rax,2), %eax
	andl	$31, %edi
	leal	(%rdi,%rax,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %eax
	testb	$8, %al
	je	.L348
	salq	$48, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %rdx
	shrq	$52, %rax
	movzwl	(%rdx,%rax,2), %eax
	shrw	$11, %ax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	cmpl	$65535, %edi
	ja	.L343
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L343:
	cmpl	$1114111, %edi
	jbe	.L353
	movl	$6832, %eax
.L345:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movzwl	(%rdx,%rax), %eax
.L348:
	shrw	$4, %ax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	cmpl	$919551, %edi
	jg	.L351
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L351:
	movl	$24704, %eax
	jmp	.L345
	.cfi_endproc
.LFE3024:
	.size	ucase_isCaseSensitive_67, .-ucase_isCaseSensitive_67
	.p2align 4
	.globl	ucase_getCaseLocale_67
	.type	ucase_getCaseLocale_67, @function
ucase_getCaseLocale_67:
.LFB3025:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	cmpb	$101, %al
	je	.L466
	movl	$1, %r8d
	cmpb	$122, %al
	je	.L354
	cmpb	$96, %al
	jle	.L359
	cmpb	$116, %al
	je	.L459
	cmpb	$97, %al
	je	.L461
	cmpb	$108, %al
	je	.L463
	cmpb	$110, %al
	je	.L465
.L354:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	cmpb	$84, %al
	je	.L459
	cmpb	$65, %al
	je	.L461
	cmpb	$76, %al
	je	.L463
	cmpb	$69, %al
	je	.L467
	cmpb	$78, %al
	jne	.L354
.L465:
	movzbl	1(%rdi), %eax
	andl	$-33, %eax
	cmpb	$76, %al
	jne	.L354
	movzbl	2(%rdi), %eax
	movl	%eax, %edx
	andl	$-33, %edx
	cmpb	$68, %dl
	jne	.L387
	movzbl	3(%rdi), %eax
.L387:
	cmpb	$95, %al
	sete	%cl
	cmpb	$45, %al
	sete	%dl
	orb	%dl, %cl
	jne	.L421
	movl	$1, %r8d
	testb	%al, %al
	jne	.L354
.L421:
	movl	$5, %r8d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L466:
	movzbl	1(%rdi), %eax
	movl	$1, %r8d
	andl	$-33, %eax
	cmpb	$76, %al
	jne	.L354
.L455:
	movzbl	2(%rdi), %eax
	movl	%eax, %edx
	andl	$-33, %edx
	cmpb	$76, %dl
	jne	.L385
	movzbl	3(%rdi), %eax
.L385:
	cmpb	$95, %al
	sete	%cl
	cmpb	$45, %al
	sete	%dl
	orb	%dl, %cl
	jne	.L420
	movl	$1, %r8d
	testb	%al, %al
	jne	.L354
.L420:
	movl	$4, %r8d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L459:
	movzbl	1(%rdi), %eax
	leaq	2(%rdi), %rdx
	andl	$-33, %eax
	cmpb	$85, %al
	jne	.L375
	movzbl	2(%rdi), %eax
	leaq	3(%rdi), %rdx
	andl	$-33, %eax
.L375:
	movl	$1, %r8d
	cmpb	$82, %al
	jne	.L354
	movzbl	(%rdx), %eax
	cmpb	$95, %al
	sete	%cl
	cmpb	$45, %al
	sete	%dl
	orb	%dl, %cl
	jne	.L418
.L436:
	testb	%al, %al
	jne	.L354
.L418:
	movl	$2, %r8d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L461:
	movzbl	1(%rdi), %eax
	andl	$-33, %eax
	cmpb	$90, %al
	jne	.L354
	movzbl	2(%rdi), %eax
	movl	%eax, %edx
	andl	$-33, %edx
	cmpb	$69, %dl
	jne	.L378
	movzbl	3(%rdi), %eax
.L378:
	cmpb	$95, %al
	sete	%cl
	cmpb	$45, %al
	sete	%dl
	orb	%dl, %cl
	jne	.L418
	movl	$1, %r8d
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L463:
	movzbl	1(%rdi), %eax
	leaq	2(%rdi), %rdx
	andl	$-33, %eax
	cmpb	$73, %al
	je	.L468
.L382:
	movl	$1, %r8d
	cmpb	$84, %al
	jne	.L354
	movzbl	(%rdx), %eax
	cmpb	$95, %al
	sete	%cl
	cmpb	$45, %al
	sete	%dl
	orb	%dl, %cl
	jne	.L419
	testb	%al, %al
	jne	.L354
.L419:
	movl	$3, %r8d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L468:
	movzbl	2(%rdi), %eax
	leaq	3(%rdi), %rdx
	andl	$-33, %eax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L467:
	movzbl	1(%rdi), %eax
	andl	$-33, %eax
	cmpb	$76, %al
	jne	.L354
	jmp	.L455
	.cfi_endproc
.LFE3025:
	.size	ucase_getCaseLocale_67, .-ucase_getCaseLocale_67
	.p2align 4
	.globl	ucase_toFullLower_67
	.type	ucase_toFullLower_67, @function
ucase_toFullLower_67:
.LFB3031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%rcx, -64(%rbp)
	cmpl	$55295, %edi
	ja	.L470
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L597:
	movzwl	(%rdx,%rax,2), %ecx
	movl	%r15d, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %r14d
	testb	$8, %r14b
	je	.L541
	movq	%r14, %r13
	leaq	_ZL22ucase_props_exceptions(%rip), %rax
	shrq	$3, %r13
	andl	$8190, %r13d
	addq	%rax, %r13
	leaq	2(%r13), %rax
	movq	%rax, -80(%rbp)
	movzwl	0(%r13), %eax
	movw	%ax, -50(%rbp)
	testb	$64, %ah
	je	.L479
	cmpl	$3, %r8d
	je	.L610
	cmpl	$2, %r8d
	sete	%cl
	cmpl	$304, %r15d
	jne	.L502
	movl	$105, %eax
	testb	%cl, %cl
	jne	.L469
.L502:
	cmpl	$775, %r15d
	jne	.L559
	testb	%cl, %cl
	je	.L559
	testq	%rbx, %rbx
	je	.L519
	movl	$-1, %esi
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L611:
	movl	%eax, %ecx
	sarl	$5, %ecx
.L601:
	movslq	%ecx, %rcx
	andl	$31, %eax
	movzwl	(%rdx,%rcx,2), %ecx
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %eax
	testb	$8, %al
	je	.L603
	salq	$48, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %rdi
	shrq	$52, %rax
	movzwl	(%rdi,%rax,2), %eax
	sarl	$7, %eax
.L603:
	andl	$96, %eax
	xorl	%esi, %esi
	cmpl	$96, %eax
	jne	.L519
.L516:
	movq	%rdx, -72(%rbp)
	movq	%r12, %rdi
	call	*%rbx
	testl	%eax, %eax
	js	.L519
	cmpl	$73, %eax
	je	.L508
	cmpl	$55295, %eax
	movq	-72(%rbp), %rdx
	jle	.L611
	cmpl	$65535, %eax
	jg	.L511
	cmpl	$56320, %eax
	movl	$320, %ecx
	movl	$0, %esi
	cmovl	%ecx, %esi
	movl	%eax, %ecx
	sarl	$5, %ecx
	addl	%esi, %ecx
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L472:
	cmpl	$1114111, %edi
	jbe	.L612
	movl	$6832, %eax
.L474:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movzwl	(%rdx,%rax), %r14d
.L541:
	testb	$2, %r14b
	je	.L537
	movswl	%r14w, %eax
	sarl	$7, %eax
	leal	(%rax,%r15), %eax
.L478:
	cmpl	%r15d, %eax
	je	.L537
.L469:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	cmpl	$65535, %edi
	ja	.L472
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L479:
	movl	%eax, %ebx
	testb	$-128, %al
	je	.L519
	movq	%rbx, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$127, %eax
	movzbl	(%rdx,%rax), %eax
	andb	$1, %bh
	jne	.L531
	leaq	2(%r13), %rbx
	leaq	(%rbx,%rax,2), %rcx
	movzwl	(%rcx), %eax
.L532:
	andl	$15, %eax
	jne	.L613
.L519:
	movzwl	-50(%rbp), %ebx
	testb	$16, -50(%rbp)
	jne	.L614
.L533:
	testb	$1, %bl
	jne	.L615
.L537:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	notl	%eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	andl	$2, %r14d
	je	.L533
	movq	%rbx, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	andb	$1, %bh
	je	.L616
	salq	$2, %rax
	andl	$1020, %eax
	addq	-80(%rbp), %rax
	movzwl	(%rax), %edx
	sall	$16, %edx
	movl	%edx, %ecx
	movzwl	2(%rax), %edx
	orl	%ecx, %edx
.L535:
	movl	%r15d, %r9d
	leal	(%rdx,%r15), %eax
	subl	%edx, %r9d
	testw	$1024, -50(%rbp)
	cmovne	%r9d, %eax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L615:
	movzwl	2(%r13), %eax
	andb	$1, %bh
	je	.L478
	movzwl	4(%r13), %r14d
	sall	$16, %eax
	orl	%r14d, %eax
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L612:
	cmpl	$919551, %edi
	jg	.L544
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L610:
	leal	-73(%r15), %eax
	cmpl	$1, %eax
	setbe	%cl
	cmpl	$302, %r15d
	sete	%al
	orb	%al, %cl
	je	.L485
	testq	%rbx, %rbx
	jne	.L545
.L485:
	leal	-204(%r15), %eax
	cmpl	$1, %eax
	jbe	.L483
	cmpl	$296, %r15d
	je	.L483
.L484:
	cmpl	$304, %r15d
	je	.L608
	cmpl	$931, %r15d
	jne	.L519
	movl	$1, %esi
	testq	%rbx, %rbx
	jne	.L529
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L618:
	call	ucase_getTypeOrIgnorable_67
	xorl	%esi, %esi
	testb	$4, %al
	je	.L617
.L529:
	movq	%r12, %rdi
	call	*%rbx
	movl	%eax, %edi
	testl	%eax, %eax
	jns	.L618
.L557:
	movl	$-1, %esi
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L620:
	call	ucase_getTypeOrIgnorable_67
	xorl	%esi, %esi
	movl	%eax, %edx
	testb	$4, %al
	je	.L619
.L530:
	movq	%r12, %rdi
	call	*%rbx
	movl	%eax, %edi
	testl	%eax, %eax
	jns	.L620
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L559:
	cmpl	$73, %r15d
	jne	.L484
	testb	%cl, %cl
	je	.L484
	movl	$1, %esi
	testq	%rbx, %rbx
	jne	.L517
.L518:
	movl	$305, %eax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L621:
	movl	%eax, %ecx
	sarl	$5, %ecx
.L604:
	movslq	%ecx, %rcx
	andl	$31, %eax
	movzwl	(%rdx,%rcx,2), %ecx
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %eax
	testb	$8, %al
	je	.L606
	salq	$48, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %rdi
	shrq	$52, %rax
	movzwl	(%rdi,%rax,2), %eax
	sarl	$7, %eax
.L606:
	andl	$96, %eax
	xorl	%esi, %esi
	cmpl	$96, %eax
	jne	.L518
.L517:
	movq	%rdx, -64(%rbp)
	movq	%r12, %rdi
	call	*%rbx
	testl	%eax, %eax
	js	.L518
	cmpl	$775, %eax
	je	.L519
	cmpl	$55295, %eax
	movq	-64(%rbp), %rdx
	jle	.L621
	cmpl	$65535, %eax
	jg	.L522
	cmpl	$56320, %eax
	movl	$320, %ecx
	movl	$0, %esi
	cmovl	%ecx, %esi
	movl	%eax, %ecx
	sarl	$5, %ecx
	addl	%esi, %ecx
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L522:
	cmpl	$1114111, %eax
	jbe	.L622
	movl	$6832, %eax
.L524:
	movzwl	(%rdx,%rax), %eax
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L531:
	salq	$2, %rax
	leaq	2(%r13), %rbx
	andl	$1020, %eax
	leaq	(%rbx,%rax), %rdx
	movzwl	(%rdx), %eax
	leaq	2(%rdx), %rcx
	sall	$16, %eax
	movl	%eax, %esi
	movzwl	2(%rdx), %eax
	orl	%esi, %eax
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L544:
	movl	$24704, %eax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L613:
	movq	-64(%rbp), %rbx
	addq	$2, %rcx
	movq	%rcx, (%rbx)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L622:
	cmpl	$919551, %eax
	jg	.L556
	movl	%eax, %ecx
	sarl	$11, %ecx
	addl	$2080, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rdx,%rcx,2), %esi
	movl	%eax, %ecx
	sarl	$5, %ecx
	andl	$63, %ecx
	addl	%esi, %ecx
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L616:
	movq	-80(%rbp), %rbx
	movzwl	(%rbx,%rax,2), %edx
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L545:
	movl	$1, %esi
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L623:
	movl	%eax, %ecx
	sarl	$5, %ecx
.L598:
	movslq	%ecx, %rcx
	andl	$31, %eax
	movzwl	(%rdx,%rcx,2), %ecx
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %eax
	testb	$8, %al
	je	.L600
	salq	$48, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %rdi
	shrq	$52, %rax
	movzwl	(%rdi,%rax,2), %eax
	sarl	$7, %eax
.L600:
	andl	$96, %eax
	cmpl	$64, %eax
	je	.L483
	xorl	%esi, %esi
	cmpl	$96, %eax
	jne	.L485
.L481:
	movq	%rdx, -72(%rbp)
	movq	%r12, %rdi
	call	*%rbx
	testl	%eax, %eax
	js	.L485
	cmpl	$55295, %eax
	movq	-72(%rbp), %rdx
	jle	.L623
	cmpl	$65535, %eax
	jg	.L488
	cmpl	$56320, %eax
	movl	$320, %ecx
	movl	$0, %esi
	cmovl	%ecx, %esi
	movl	%eax, %ecx
	sarl	$5, %ecx
	addl	%esi, %ecx
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L483:
	cmpl	$205, %r15d
	je	.L494
	jg	.L495
	cmpl	$74, %r15d
	je	.L496
	cmpl	$204, %r15d
	jne	.L624
	movq	-64(%rbp), %rax
	leaq	_ZL9iDotGrave(%rip), %rbx
	movq	%rbx, (%rax)
	movl	$3, %eax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L624:
	cmpl	$73, %r15d
	jne	.L609
.L608:
	movq	-64(%rbp), %rax
	leaq	_ZL4iDot(%rip), %rbx
	movq	%rbx, (%rax)
	movl	$2, %eax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L511:
	cmpl	$1114111, %eax
	ja	.L551
	cmpl	$919551, %eax
	jg	.L552
	movl	%eax, %ecx
	sarl	$11, %ecx
	addl	$2080, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rdx,%rcx,2), %esi
	movl	%eax, %ecx
	sarl	$5, %ecx
	andl	$63, %ecx
	addl	%esi, %ecx
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L488:
	cmpl	$1114111, %eax
	ja	.L547
	cmpl	$919551, %eax
	jg	.L548
	movl	%eax, %ecx
	sarl	$11, %ecx
	addl	$2080, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rdx,%rcx,2), %esi
	movl	%eax, %ecx
	sarl	$5, %ecx
	andl	$63, %ecx
	addl	%esi, %ecx
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L508:
	movq	-64(%rbp), %rax
	movq	$0, (%rax)
.L609:
	xorl	%eax, %eax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L495:
	cmpl	$296, %r15d
	je	.L499
	cmpl	$302, %r15d
	jne	.L609
	movq	-64(%rbp), %rax
	leaq	_ZL10iOgonekDot(%rip), %rbx
	movq	%rbx, (%rax)
	movl	$2, %eax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L551:
	movl	$6832, %eax
.L513:
	movzwl	(%rdx,%rax), %eax
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L547:
	movl	$6832, %eax
.L490:
	movzwl	(%rdx,%rax), %eax
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L552:
	movl	$24704, %eax
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L556:
	movl	$24704, %eax
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L617:
	testl	%eax, %eax
	jne	.L519
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L548:
	movl	$24704, %eax
	jmp	.L490
.L619:
	movl	$962, %eax
	testl	%edx, %edx
	jne	.L469
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L499:
	movq	-64(%rbp), %rax
	leaq	_ZL9iDotTilde(%rip), %rbx
	movq	%rbx, (%rax)
	movl	$3, %eax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L496:
	movq	-64(%rbp), %rax
	leaq	_ZL4jDot(%rip), %rbx
	movq	%rbx, (%rax)
	movl	$2, %eax
	jmp	.L469
.L494:
	movq	-64(%rbp), %rax
	leaq	_ZL9iDotAcute(%rip), %rbx
	movq	%rbx, (%rax)
	movl	$3, %eax
	jmp	.L469
	.cfi_endproc
.LFE3031:
	.size	ucase_toFullLower_67, .-ucase_toFullLower_67
	.p2align 4
	.globl	ucase_toFullUpper_67
	.type	ucase_toFullUpper_67, @function
ucase_toFullUpper_67:
.LFB3033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$40, %rsp
	cmpl	$55295, %edi
	ja	.L626
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L678:
	movzwl	(%rdx,%rax,2), %ecx
	movl	%ebx, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %r15d
	testb	$8, %r15b
	je	.L656
	movq	%r15, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %r10
	shrq	$3, %rax
	andl	$8190, %eax
	addq	%r10, %rax
	movzwl	(%rax), %r9d
	leaq	2(%rax), %rdi
	movq	%rdi, -72(%rbp)
	testw	$16384, %r9w
	je	.L634
	cmpl	$2, %r8d
	jne	.L664
	movl	$304, %eax
	cmpl	$105, %ebx
	je	.L625
.L664:
	cmpl	$3, %r8d
	sete	%sil
	cmpl	$775, %ebx
	sete	%al
	testb	%al, %sil
	je	.L637
	testq	%r12, %r12
	je	.L637
	movl	$-1, %esi
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L683:
	movl	%eax, %esi
	sarl	$5, %esi
.L679:
	movslq	%esi, %rsi
	andl	$31, %eax
	movzwl	(%rdx,%rsi,2), %esi
	leal	(%rax,%rsi,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %eax
	testb	$8, %al
	je	.L681
	salq	$48, %rax
	shrq	$52, %rax
	movzwl	(%r10,%rax,2), %eax
	sarl	$7, %eax
.L681:
	andl	$96, %eax
	cmpl	$32, %eax
	je	.L645
	xorl	%esi, %esi
	cmpl	$96, %eax
	jne	.L637
.L646:
	movl	%r9d, -52(%rbp)
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	call	*%r12
	movl	-52(%rbp), %r9d
	testl	%eax, %eax
	js	.L637
	cmpl	$55295, %eax
	movq	-64(%rbp), %rdx
	leaq	_ZL22ucase_props_exceptions(%rip), %r10
	jle	.L683
	cmpl	$65535, %eax
	jg	.L640
	cmpl	$56320, %eax
	movl	$320, %esi
	movl	$0, %edi
	cmovl	%esi, %edi
	movl	%eax, %esi
	sarl	$5, %esi
	addl	%edi, %esi
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L626:
	cmpl	$65535, %edi
	ja	.L628
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L628:
	cmpl	$1114111, %edi
	jbe	.L684
	movl	$6832, %eax
.L630:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movzwl	(%rdx,%rax), %r15d
.L656:
	movl	%r15d, %eax
	andl	$3, %eax
	cmpw	$1, %ax
	je	.L685
.L632:
	movl	%ebx, %eax
	notl	%eax
.L625:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	testb	$-128, %r9b
	je	.L637
	movq	%r9, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$127, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r9w
	jne	.L647
	leaq	(%rdi,%rax,2), %rdi
	movzwl	(%rdi), %edx
.L648:
	movl	%edx, %eax
	sarl	$8, %eax
	andl	$15, %eax
	jne	.L686
.L637:
	testb	$16, %r9b
	jne	.L687
.L649:
	testb	$4, %r9b
	je	.L632
	movq	%r9, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$3, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r9w
	jne	.L654
	movq	-72(%rbp), %rdi
	movzwl	(%rdi,%rax,2), %eax
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L687:
	movl	%r15d, %ecx
	andl	$3, %ecx
	cmpw	$1, %cx
	jne	.L649
	movq	%r9, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r9w
	jne	.L650
	movq	-72(%rbp), %rdi
	movzwl	(%rdi,%rax,2), %edx
.L651:
	leal	(%rbx,%rdx), %eax
	subl	%edx, %ebx
	testw	$1024, %r9w
	cmovne	%ebx, %eax
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L685:
	movswl	%r15w, %eax
	sarl	$7, %eax
	leal	(%rax,%rbx), %eax
.L633:
	cmpl	%eax, %ebx
	je	.L632
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L684:
	.cfi_restore_state
	cmpl	$919551, %edi
	jg	.L659
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L654:
	movq	-72(%rbp), %r15
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %r15
	movzwl	(%r15), %eax
	movzwl	2(%r15), %ecx
	sall	$16, %eax
	orl	%ecx, %eax
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L640:
	cmpl	$1114111, %eax
	ja	.L662
	cmpl	$919551, %eax
	jle	.L688
	movl	$24704, %eax
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L647:
	salq	$2, %rax
	andl	$1020, %eax
	addq	-72(%rbp), %rax
	movzwl	(%rax), %edx
	leaq	2(%rax), %rdi
	movl	%edx, %esi
	movzwl	2(%rax), %edx
	sall	$16, %esi
	orl	%esi, %edx
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L659:
	movl	$24704, %eax
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L662:
	movl	$6832, %eax
.L642:
	movzwl	(%rdx,%rax), %eax
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L686:
	movl	%edx, %ecx
	sarl	$4, %edx
	andl	$15, %ecx
	andl	$15, %edx
	leaq	1(%rcx,%rdx), %rdx
	leaq	(%rdi,%rdx,2), %rdx
	movq	%rdx, (%r14)
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L688:
	movl	%eax, %esi
	sarl	$11, %esi
	addl	$2080, %esi
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %edi
	movl	%eax, %esi
	sarl	$5, %esi
	andl	$63, %esi
	addl	%edi, %esi
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L645:
	movq	$0, (%r14)
	xorl	%eax, %eax
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L650:
	movq	-72(%rbp), %r15
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %r15
	movzwl	(%r15), %edx
	movl	%edx, %eax
	movzwl	2(%r15), %edx
	sall	$16, %eax
	orl	%eax, %edx
	jmp	.L651
	.cfi_endproc
.LFE3033:
	.size	ucase_toFullUpper_67, .-ucase_toFullUpper_67
	.p2align 4
	.globl	ucase_toFullTitle_67
	.type	ucase_toFullTitle_67, @function
ucase_toFullTitle_67:
.LFB3034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edi, %ebx
	subq	$40, %rsp
	cmpl	$55295, %edi
	ja	.L690
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L744:
	movzwl	(%rdx,%rax,2), %ecx
	movl	%ebx, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %r15d
	testb	$8, %r15b
	je	.L720
	movq	%r15, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %r10
	shrq	$3, %rax
	andl	$8190, %eax
	addq	%r10, %rax
	movzwl	(%rax), %r9d
	leaq	2(%rax), %rdi
	movq	%rdi, -72(%rbp)
	testw	$16384, %r9w
	je	.L698
	cmpl	$2, %r8d
	jne	.L730
	movl	$304, %eax
	cmpl	$105, %ebx
	je	.L689
.L730:
	cmpl	$3, %r8d
	sete	%sil
	cmpl	$775, %ebx
	sete	%al
	testb	%al, %sil
	je	.L701
	testq	%r12, %r12
	je	.L701
	movl	$-1, %esi
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L749:
	movl	%eax, %esi
	sarl	$5, %esi
.L745:
	movslq	%esi, %rsi
	andl	$31, %eax
	movzwl	(%rdx,%rsi,2), %esi
	leal	(%rax,%rsi,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %eax
	testb	$8, %al
	je	.L747
	salq	$48, %rax
	shrq	$52, %rax
	movzwl	(%r10,%rax,2), %eax
	sarl	$7, %eax
.L747:
	andl	$96, %eax
	cmpl	$32, %eax
	je	.L709
	xorl	%esi, %esi
	cmpl	$96, %eax
	jne	.L701
.L710:
	movl	%r9d, -52(%rbp)
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	call	*%r12
	movl	-52(%rbp), %r9d
	testl	%eax, %eax
	js	.L701
	cmpl	$55295, %eax
	movq	-64(%rbp), %rdx
	leaq	_ZL22ucase_props_exceptions(%rip), %r10
	jle	.L749
	cmpl	$65535, %eax
	jg	.L704
	cmpl	$56320, %eax
	movl	$320, %esi
	movl	$0, %edi
	cmovl	%esi, %edi
	movl	%eax, %esi
	sarl	$5, %esi
	addl	%edi, %esi
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L698:
	testb	$-128, %r9b
	je	.L701
	movq	%r9, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$127, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r9w
	jne	.L711
	leaq	(%rdi,%rax,2), %rdi
	movzwl	(%rdi), %edx
.L712:
	movl	%edx, %eax
	sarl	$12, %eax
	andl	$15, %eax
	jne	.L750
.L701:
	testb	$16, %r9b
	je	.L713
	movl	%r15d, %ecx
	andl	$3, %ecx
	cmpw	$1, %cx
	je	.L751
.L713:
	testb	$8, %r9b
	jne	.L728
	movl	$3, %eax
	testb	$4, %r9b
	je	.L696
.L717:
	andl	%r9d, %eax
	leaq	_ZL11flagsOffset(%rip), %rdx
	cltq
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r9w
	jne	.L718
	movq	-72(%rbp), %rdi
	movzwl	(%rdi,%rax,2), %eax
.L697:
	cmpl	%eax, %ebx
	je	.L696
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	cmpl	$1114111, %edi
	jbe	.L752
	movl	$6832, %eax
.L694:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movzwl	(%rdx,%rax), %r15d
.L720:
	movl	%r15d, %eax
	andl	$3, %eax
	cmpw	$1, %ax
	je	.L753
.L696:
	movl	%ebx, %eax
	notl	%eax
.L689:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore_state
	cmpl	$65535, %edi
	ja	.L692
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L728:
	movl	$7, %eax
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L752:
	cmpl	$919551, %edi
	jg	.L723
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L753:
	movswl	%r15w, %eax
	sarl	$7, %eax
	leal	(%rax,%rbx), %eax
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L718:
	movq	-72(%rbp), %r15
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %r15
	movzwl	(%r15), %eax
	movzwl	2(%r15), %ecx
	sall	$16, %eax
	orl	%ecx, %eax
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L704:
	cmpl	$1114111, %eax
	ja	.L726
	cmpl	$919551, %eax
	jle	.L754
	movl	$24704, %eax
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L711:
	salq	$2, %rax
	andl	$1020, %eax
	addq	-72(%rbp), %rax
	movzwl	(%rax), %edx
	leaq	2(%rax), %rdi
	movl	%edx, %esi
	movzwl	2(%rax), %edx
	sall	$16, %esi
	orl	%esi, %edx
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L723:
	movl	$24704, %eax
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L726:
	movl	$6832, %eax
.L706:
	movzwl	(%rdx,%rax), %eax
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L750:
	movl	%edx, %ecx
	movl	%edx, %esi
	sarl	$4, %edx
	sarl	$8, %ecx
	andl	$15, %esi
	andl	$15, %edx
	andl	$15, %ecx
	leaq	1(%rsi,%rcx), %rcx
	addq	%rcx, %rdx
	leaq	(%rdi,%rdx,2), %rdx
	movq	%rdx, (%r14)
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L751:
	movq	%r9, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r9w
	jne	.L714
	movq	-72(%rbp), %rdi
	movzwl	(%rdi,%rax,2), %edx
.L715:
	leal	(%rbx,%rdx), %eax
	subl	%edx, %ebx
	testw	$1024, %r9w
	cmovne	%ebx, %eax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L754:
	movl	%eax, %esi
	sarl	$11, %esi
	addl	$2080, %esi
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %edi
	movl	%eax, %esi
	sarl	$5, %esi
	andl	$63, %esi
	addl	%edi, %esi
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L709:
	movq	$0, (%r14)
	xorl	%eax, %eax
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L714:
	movq	-72(%rbp), %r15
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %r15
	movzwl	(%r15), %edx
	movl	%edx, %eax
	movzwl	2(%r15), %edx
	sall	$16, %eax
	orl	%eax, %edx
	jmp	.L715
	.cfi_endproc
.LFE3034:
	.size	ucase_toFullTitle_67, .-ucase_toFullTitle_67
	.p2align 4
	.globl	ucase_fold_67
	.type	ucase_fold_67, @function
ucase_fold_67:
.LFB3035:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L756
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$5, %eax
	cltq
.L791:
	movzwl	(%rcx,%rax,2), %edx
	movl	%edi, %eax
	andl	$31, %eax
	leal	(%rax,%rdx,4), %eax
	cltq
	addq	%rax, %rax
	movswl	(%rcx,%rax), %edx
	testb	$8, %dl
	je	.L772
	movq	%rdx, %rcx
	leaq	_ZL22ucase_props_exceptions(%rip), %rax
	shrq	$3, %rcx
	andl	$8190, %ecx
	addq	%rax, %rcx
	movzwl	(%rcx), %r8d
	testw	%r8w, %r8w
	js	.L792
.L763:
	movl	%edi, %eax
	testw	$512, %r8w
	jne	.L755
	addq	$2, %rcx
	testb	$16, %r8b
	je	.L765
	andl	$2, %edx
	je	.L765
	movq	%r8, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r8w
	je	.L793
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rcx
	movzwl	(%rcx), %edx
	movl	%edx, %eax
	movzwl	2(%rcx), %edx
	sall	$16, %eax
	orl	%eax, %edx
.L767:
	leal	(%rdx,%rdi), %eax
	subl	%edx, %edi
	testw	$1024, %r8w
	cmovne	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L756:
	cmpl	$65535, %edi
	ja	.L758
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$5, %eax
	addl	%edx, %eax
	cltq
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L758:
	cmpl	$1114111, %edi
	jbe	.L794
	movl	$6832, %eax
.L760:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movswl	(%rdx,%rax), %edx
.L772:
	movl	%edi, %eax
	testb	$2, %dl
	je	.L755
	sarl	$7, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L755:
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	andl	$7, %esi
	jne	.L764
	cmpl	$73, %edi
	je	.L780
	cmpl	$304, %edi
	jne	.L763
	movl	$304, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	testb	$2, %r8b
	jne	.L795
	movl	%edi, %eax
	testb	$1, %r8b
	je	.L755
	xorl	%eax, %eax
	testw	$256, %r8w
	jne	.L771
.L796:
	movzwl	(%rcx,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L794:
	cmpl	$919551, %edi
	jg	.L775
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rcx,%rax,2), %edx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%edx, %eax
	cltq
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L795:
	movq	%r8, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$1, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r8w
	je	.L796
.L771:
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rcx
	movzwl	(%rcx), %eax
	sall	$16, %eax
	movl	%eax, %edi
	movzwl	2(%rcx), %eax
	orl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	cmpl	$73, %edi
	je	.L779
	cmpl	$304, %edi
	jne	.L763
.L780:
	movl	$105, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	movl	$24704, %eax
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L793:
	movzwl	(%rcx,%rax,2), %edx
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L779:
	movl	$305, %eax
	ret
	.cfi_endproc
.LFE3035:
	.size	ucase_fold_67, .-ucase_fold_67
	.p2align 4
	.globl	ucase_toFullFolding_67
	.type	ucase_toFullFolding_67, @function
ucase_toFullFolding_67:
.LFB3036:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L798
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$5, %eax
	cltq
.L843:
	movzwl	(%rcx,%rax,2), %r8d
	movl	%edi, %eax
	andl	$31, %eax
	leal	(%rax,%r8,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rcx,%rax), %r8d
	testb	$8, %r8b
	je	.L820
	movq	%r8, %rcx
	leaq	_ZL22ucase_props_exceptions(%rip), %rax
	shrq	$3, %rcx
	andl	$8190, %ecx
	addq	%rax, %rcx
	leaq	2(%rcx), %r9
	movzwl	(%rcx), %ecx
	testw	%cx, %cx
	js	.L844
	testb	$-128, %cl
	je	.L809
	movq	%rcx, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$127, %eax
	movzbl	(%rdx,%rax), %eax
	testb	$1, %ch
	jne	.L810
	leaq	(%r9,%rax,2), %r11
	movzwl	(%r11), %edx
.L811:
	movl	%edx, %eax
	sarl	$4, %eax
	andl	$15, %eax
	je	.L809
	andl	$15, %edx
	leaq	2(%r11,%rdx,2), %rdx
	movq	%rdx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L798:
	cmpl	$65535, %edi
	ja	.L800
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%ecx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	cltq
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L800:
	cmpl	$1114111, %edi
	jbe	.L845
	movl	$6832, %eax
.L802:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movzwl	(%rdx,%rax), %r8d
.L820:
	testb	$2, %r8b
	je	.L804
	movswl	%r8w, %eax
	sarl	$7, %eax
	addl	%edi, %eax
.L805:
	cmpl	%edi, %eax
	je	.L804
	ret
	.p2align 4,,10
	.p2align 3
.L844:
	andl	$7, %edx
	jne	.L807
	cmpl	$73, %edi
	je	.L826
	cmpl	$304, %edi
	je	.L846
	.p2align 4,,10
	.p2align 3
.L809:
	testb	$2, %ch
	jne	.L804
	testb	$16, %cl
	je	.L813
	andl	$2, %r8d
	jne	.L847
.L813:
	testb	$2, %cl
	jne	.L817
	xorl	%eax, %eax
	testb	$1, %cl
	jne	.L818
.L804:
	movl	%edi, %eax
	notl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	cmpl	$919551, %edi
	jg	.L823
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rcx,%rax,2), %r8d
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%r8d, %eax
	cltq
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L817:
	movq	%rcx, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$1, %eax
	movzbl	(%rdx,%rax), %eax
.L818:
	andb	$1, %ch
	jne	.L819
	movzwl	(%r9,%rax,2), %eax
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L807:
	cmpl	$73, %edi
	je	.L825
	cmpl	$304, %edi
	jne	.L809
.L826:
	movl	$105, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L819:
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %r9
	movzwl	(%r9), %eax
	sall	$16, %eax
	movl	%eax, %edx
	movzwl	2(%r9), %eax
	orl	%edx, %eax
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L810:
	salq	$2, %rax
	andl	$1020, %eax
	addq	%r9, %rax
	movzwl	(%rax), %edx
	leaq	2(%rax), %r11
	sall	$16, %edx
	movl	%edx, %r10d
	movzwl	2(%rax), %edx
	orl	%r10d, %edx
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L823:
	movl	$24704, %eax
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L847:
	movq	%rcx, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	testb	$1, %ch
	jne	.L814
	movzwl	(%r9,%rax,2), %edx
.L815:
	leal	(%rdx,%rdi), %eax
	subl	%edx, %edi
	andb	$4, %ch
	cmovne	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L846:
	leaq	_ZL4iDot(%rip), %rax
	movq	%rax, (%rsi)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %r9
	movzwl	(%r9), %edx
	movl	%edx, %eax
	movzwl	2(%r9), %edx
	sall	$16, %eax
	orl	%eax, %edx
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L825:
	movl	$305, %eax
	ret
	.cfi_endproc
.LFE3036:
	.size	ucase_toFullFolding_67, .-ucase_toFullFolding_67
	.p2align 4
	.globl	u_isULowercase_67
	.type	u_isULowercase_67, @function
u_isULowercase_67:
.LFB3037:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L849
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L856:
	movzwl	(%rdx,%rax,2), %eax
	andl	$31, %edi
	leal	(%rdi,%rax,4), %eax
	cltq
	addq	%rax, %rax
.L850:
	movzwl	(%rdx,%rax), %eax
	andl	$3, %eax
	cmpw	$1, %ax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L849:
	cmpl	$65535, %edi
	ja	.L851
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L851:
	movl	$6832, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cmpl	$1114111, %edi
	ja	.L850
	movl	$24704, %eax
	cmpl	$919551, %edi
	jg	.L850
	movl	%edi, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L856
	.cfi_endproc
.LFE3037:
	.size	u_isULowercase_67, .-u_isULowercase_67
	.p2align 4
	.globl	u_isUUppercase_67
	.type	u_isUUppercase_67, @function
u_isUUppercase_67:
.LFB3038:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L858
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L865:
	movzwl	(%rdx,%rax,2), %eax
	andl	$31, %edi
	leal	(%rdi,%rax,4), %eax
	cltq
	addq	%rax, %rax
.L859:
	movzwl	(%rdx,%rax), %eax
	andl	$3, %eax
	cmpw	$2, %ax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L858:
	cmpl	$65535, %edi
	ja	.L860
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L860:
	movl	$6832, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cmpl	$1114111, %edi
	ja	.L859
	movl	$24704, %eax
	cmpl	$919551, %edi
	jg	.L859
	movl	%edi, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L865
	.cfi_endproc
.LFE3038:
	.size	u_isUUppercase_67, .-u_isUUppercase_67
	.p2align 4
	.globl	u_tolower_67
	.type	u_tolower_67, @function
u_tolower_67:
.LFB3039:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L867
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L895:
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movswl	(%rdx,%rax), %edx
	testb	$8, %dl
	je	.L878
	movq	%rdx, %rcx
	leaq	_ZL22ucase_props_exceptions(%rip), %rax
	shrq	$3, %rcx
	andl	$8190, %ecx
	addq	%rax, %rcx
	movzwl	(%rcx), %esi
	testb	$16, %sil
	jne	.L896
.L874:
	movl	%edi, %eax
	testb	$1, %sil
	je	.L866
	movzwl	2(%rcx), %eax
	testw	$256, %si
	jne	.L897
.L866:
	ret
	.p2align 4,,10
	.p2align 3
.L867:
	cmpl	$65535, %edi
	ja	.L869
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L869:
	cmpl	$1114111, %edi
	jbe	.L898
	movl	$6832, %eax
.L871:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movswl	(%rdx,%rax), %edx
.L878:
	movl	%edi, %eax
	testb	$2, %dl
	je	.L866
	sarl	$7, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L896:
	andl	$2, %edx
	je	.L874
	movq	%rsi, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	addq	$2, %rcx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %si
	je	.L899
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rcx
	movzwl	(%rcx), %edx
	movl	%edx, %eax
	movzwl	2(%rcx), %edx
	sall	$16, %eax
	orl	%eax, %edx
.L876:
	leal	(%rdi,%rdx), %eax
	subl	%edx, %edi
	testw	$1024, %si
	cmovne	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L898:
	cmpl	$919551, %edi
	jg	.L881
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L897:
	sall	$16, %eax
	movl	%eax, %edi
	movzwl	4(%rcx), %eax
	orl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L881:
	movl	$24704, %eax
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L899:
	movzwl	(%rcx,%rax,2), %edx
	jmp	.L876
	.cfi_endproc
.LFE3039:
	.size	u_tolower_67, .-u_tolower_67
	.p2align 4
	.globl	u_toupper_67
	.type	u_toupper_67, @function
u_toupper_67:
.LFB3040:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	cmpl	$55295, %edi
	ja	.L901
	movl	%edi, %edx
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$5, %edx
	movslq	%edx, %rdx
.L924:
	movzwl	(%rcx,%rdx,2), %esi
	movl	%eax, %edx
	andl	$31, %edx
	leal	(%rdx,%rsi,4), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	movswl	(%rcx,%rdx), %edx
	testb	$8, %dl
	je	.L913
	movq	%rdx, %rcx
	leaq	_ZL22ucase_props_exceptions(%rip), %rsi
	shrq	$3, %rcx
	andl	$8190, %ecx
	addq	%rsi, %rcx
	leaq	2(%rcx), %rsi
	movzwl	(%rcx), %ecx
	testb	$16, %cl
	jne	.L925
.L908:
	testb	$4, %cl
	je	.L919
	movq	%rcx, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$3, %eax
	movzbl	(%rdx,%rax), %eax
	andb	$1, %ch
	jne	.L912
	movzwl	(%rsi,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L901:
	cmpl	$65535, %edi
	ja	.L903
	cmpl	$56320, %edi
	movl	$320, %edx
	movl	$0, %ecx
	cmovl	%edx, %ecx
	movl	%edi, %edx
	sarl	$5, %edx
	addl	%ecx, %edx
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	movslq	%edx, %rdx
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L903:
	cmpl	$1114111, %edi
	jbe	.L926
	movl	$6832, %edx
.L905:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	movswl	(%rcx,%rdx), %edx
.L913:
	movl	%edx, %ecx
	andl	$3, %ecx
	cmpw	$1, %cx
	je	.L927
.L919:
	ret
	.p2align 4,,10
	.p2align 3
.L925:
	andl	$3, %edx
	cmpw	$1, %dx
	jne	.L908
	movq	%rcx, %rdx
	leaq	_ZL11flagsOffset(%rip), %rdi
	andl	$15, %edx
	movzbl	(%rdi,%rdx), %edx
	testb	$1, %ch
	je	.L928
	salq	$2, %rdx
	andl	$1020, %edx
	addq	%rdx, %rsi
	movzwl	(%rsi), %edx
	movl	%edx, %edi
	movzwl	2(%rsi), %edx
	sall	$16, %edi
	orl	%edi, %edx
.L910:
	leal	(%rax,%rdx), %esi
	subl	%edx, %eax
	andb	$4, %ch
	cmove	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L927:
	sarl	$7, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L926:
	cmpl	$919551, %edi
	jg	.L916
	movl	%edi, %edx
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %esi
	movl	%edi, %edx
	sarl	$5, %edx
	andl	$63, %edx
	addl	%esi, %edx
	movslq	%edx, %rdx
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L912:
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rsi
	movzwl	(%rsi), %eax
	movzwl	2(%rsi), %edi
	sall	$16, %eax
	orl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L916:
	movl	$24704, %edx
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L928:
	movzwl	(%rsi,%rdx,2), %edx
	jmp	.L910
	.cfi_endproc
.LFE3040:
	.size	u_toupper_67, .-u_toupper_67
	.p2align 4
	.globl	u_totitle_67
	.type	u_totitle_67, @function
u_totitle_67:
.LFB3041:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L930
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
.L954:
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	andl	$31, %eax
	leal	(%rax,%rcx,4), %eax
	cltq
	addq	%rax, %rax
	movswl	(%rdx,%rax), %edx
	testb	$8, %dl
	je	.L943
	movq	%rdx, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %rcx
	shrq	$3, %rax
	andl	$8190, %eax
	addq	%rcx, %rax
	movzwl	(%rax), %ecx
	leaq	2(%rax), %rsi
	testb	$16, %cl
	jne	.L955
.L937:
	testb	$8, %cl
	jne	.L948
	movl	%edi, %eax
	testb	$4, %cl
	je	.L929
	movl	$3, %eax
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L955:
	andl	$3, %edx
	cmpw	$1, %dx
	jne	.L937
	movq	%rcx, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	testb	$1, %ch
	je	.L956
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rsi
	movzwl	(%rsi), %edx
	movl	%edx, %eax
	movzwl	2(%rsi), %edx
	sall	$16, %eax
	orl	%eax, %edx
.L939:
	leal	(%rdi,%rdx), %eax
	subl	%edx, %edi
	andb	$4, %ch
	cmovne	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L932:
	cmpl	$1114111, %edi
	jbe	.L957
	movl	$6832, %eax
.L934:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movswl	(%rdx,%rax), %edx
.L943:
	movl	%edx, %ecx
	movl	%edi, %eax
	andl	$3, %ecx
	cmpw	$1, %cx
	je	.L958
.L929:
	ret
	.p2align 4,,10
	.p2align 3
.L930:
	cmpl	$65535, %edi
	ja	.L932
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L948:
	movl	$7, %eax
.L941:
	andl	%ecx, %eax
	leaq	_ZL11flagsOffset(%rip), %rdx
	cltq
	movzbl	(%rdx,%rax), %eax
	andb	$1, %ch
	jne	.L942
	movzwl	(%rsi,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rsi
	movzwl	(%rsi), %eax
	movzwl	2(%rsi), %edi
	sall	$16, %eax
	orl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L958:
	sarl	$7, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L957:
	cmpl	$919551, %edi
	jg	.L946
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	cltq
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L946:
	movl	$24704, %eax
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L956:
	movzwl	(%rsi,%rax,2), %edx
	jmp	.L939
	.cfi_endproc
.LFE3041:
	.size	u_totitle_67, .-u_totitle_67
	.p2align 4
	.globl	u_foldCase_67
	.type	u_foldCase_67, @function
u_foldCase_67:
.LFB3042:
	.cfi_startproc
	endbr64
	cmpl	$55295, %edi
	ja	.L960
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$5, %eax
	cltq
.L995:
	movzwl	(%rcx,%rax,2), %edx
	movl	%edi, %eax
	andl	$31, %eax
	leal	(%rax,%rdx,4), %eax
	cltq
	addq	%rax, %rax
	movswl	(%rcx,%rax), %edx
	testb	$8, %dl
	je	.L976
	movq	%rdx, %rcx
	leaq	_ZL22ucase_props_exceptions(%rip), %rax
	shrq	$3, %rcx
	andl	$8190, %ecx
	addq	%rax, %rcx
	movzwl	(%rcx), %r8d
	testw	%r8w, %r8w
	js	.L996
.L967:
	movl	%edi, %eax
	testw	$512, %r8w
	jne	.L959
	addq	$2, %rcx
	testb	$16, %r8b
	je	.L969
	andl	$2, %edx
	je	.L969
	movq	%r8, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$15, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r8w
	je	.L997
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rcx
	movzwl	(%rcx), %edx
	movl	%edx, %eax
	movzwl	2(%rcx), %edx
	sall	$16, %eax
	orl	%eax, %edx
.L971:
	leal	(%rdi,%rdx), %eax
	subl	%edx, %edi
	testw	$1024, %r8w
	cmovne	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L960:
	cmpl	$65535, %edi
	ja	.L962
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$5, %eax
	addl	%edx, %eax
	cltq
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L962:
	cmpl	$1114111, %edi
	jbe	.L998
	movl	$6832, %eax
.L964:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movswl	(%rdx,%rax), %edx
.L976:
	movl	%edi, %eax
	testb	$2, %dl
	je	.L959
	sarl	$7, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L959:
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	andl	$7, %esi
	jne	.L968
	cmpl	$73, %edi
	je	.L984
	cmpl	$304, %edi
	jne	.L967
	movl	$304, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L969:
	testb	$2, %r8b
	jne	.L999
	movl	%edi, %eax
	testb	$1, %r8b
	je	.L959
	xorl	%eax, %eax
	testw	$256, %r8w
	jne	.L975
.L1000:
	movzwl	(%rcx,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L998:
	cmpl	$919551, %edi
	jg	.L979
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rcx
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rcx,%rax,2), %edx
	movl	%edi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%edx, %eax
	cltq
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L999:
	movq	%r8, %rax
	leaq	_ZL11flagsOffset(%rip), %rdx
	andl	$1, %eax
	movzbl	(%rdx,%rax), %eax
	testw	$256, %r8w
	je	.L1000
.L975:
	salq	$2, %rax
	andl	$1020, %eax
	addq	%rax, %rcx
	movzwl	(%rcx), %eax
	sall	$16, %eax
	movl	%eax, %edi
	movzwl	2(%rcx), %eax
	orl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L968:
	cmpl	$73, %edi
	je	.L983
	cmpl	$304, %edi
	jne	.L967
.L984:
	movl	$105, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L979:
	movl	$24704, %eax
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L997:
	movzwl	(%rcx,%rax,2), %edx
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L983:
	movl	$305, %eax
	ret
	.cfi_endproc
.LFE3042:
	.size	u_foldCase_67, .-u_foldCase_67
	.p2align 4
	.globl	ucase_hasBinaryProperty_67
	.type	ucase_hasBinaryProperty_67, @function
ucase_hasBinaryProperty_67:
.LFB3043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$22, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$33, %esi
	ja	.L1026
	leaq	.L1004(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1004:
	.long	.L1013-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1012-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1011-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1010-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1026-.L1004
	.long	.L1009-.L1004
	.long	.L1008-.L1004
	.long	.L1007-.L1004
	.long	.L1006-.L1004
	.long	.L1005-.L1004
	.long	.L1026-.L1004
	.long	.L1003-.L1004
	.text
	.p2align 4,,10
	.p2align 3
.L1026:
	xorl	%eax, %eax
.L1001:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1034
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1003:
	.cfi_restore_state
	leaq	-32(%rbp), %r12
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movq	%r12, %rcx
	movl	%edi, -36(%rbp)
	call	ucase_toFullLower_67
	movl	-36(%rbp), %edi
	testl	%eax, %eax
	js	.L1021
.L1023:
	movl	$1, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1013:
	call	ucase_getType_67
	cmpl	$1, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1012:
	cmpl	$55295, %edi
	jbe	.L1035
	cmpl	$65535, %edi
	ja	.L1016
	cmpl	$56320, %edi
	movl	$320, %eax
	movl	$0, %edx
	cmovl	%eax, %edx
	movl	%edi, %eax
	sarl	$5, %eax
	addl	%edx, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	cltq
.L1031:
	movzwl	(%rdx,%rax,2), %eax
	andl	$31, %edi
	leal	(%rdi,%rax,4), %eax
	cltq
	addq	%rax, %rax
	movzwl	(%rdx,%rax), %eax
	testb	$8, %al
	je	.L1032
	salq	$48, %rax
	leaq	_ZL22ucase_props_exceptions(%rip), %rdx
	shrq	$52, %rax
	movzwl	(%rdx,%rax,2), %eax
	sarl	$7, %eax
.L1032:
	andl	$96, %eax
	cmpl	$32, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1011:
	call	ucase_getType_67
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1010:
	call	ucase_isCaseSensitive_67
	movsbl	%al, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1009:
	call	ucase_getType_67
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1008:
	call	ucase_getTypeOrIgnorable_67
	sarl	$2, %eax
	movsbl	%al, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1007:
	leaq	-32(%rbp), %rcx
	movl	$1, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	ucase_toFullLower_67
	notl	%eax
	shrl	$31, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1006:
	leaq	-32(%rbp), %rcx
	movl	$1, %r9d
	movl	$1, %r8d
.L1033:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZL14toUpperOrTitleiPFiPvaES_PPKDsia
	notl	%eax
	shrl	$31, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1005:
	leaq	-32(%rbp), %rcx
	xorl	%r9d, %r9d
	movl	$1, %r8d
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1035:
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	sarl	$5, %eax
	cltq
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1021:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r9d
	movl	$1, %r8d
	movq	%r12, %rcx
	movl	%edi, -36(%rbp)
	call	_ZL14toUpperOrTitleiPFiPvaES_PPKDsia
	movl	-36(%rbp), %edi
	testl	%eax, %eax
	jns	.L1023
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%r12, %rcx
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1016:
	cmpl	$1114111, %edi
	ja	.L1028
	cmpl	$919551, %edi
	jg	.L1029
	movl	%edi, %eax
	leaq	_ZL21ucase_props_trieIndex(%rip), %rdx
	movl	%edi, %ecx
	sarl	$11, %eax
	sarl	$5, %ecx
	addl	$2080, %eax
	andl	$63, %ecx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%ecx, %eax
	cltq
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1028:
	movl	$6832, %edx
.L1018:
	leaq	_ZL21ucase_props_trieIndex(%rip), %rax
	movzwl	(%rax,%rdx), %eax
	jmp	.L1032
.L1029:
	movl	$24704, %edx
	jmp	.L1018
.L1034:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3043:
	.size	ucase_hasBinaryProperty_67, .-ucase_hasBinaryProperty_67
	.globl	_ZN6icu_679LatinCase11TO_UPPER_TRE
	.section	.rodata
	.align 32
	.type	_ZN6icu_679LatinCase11TO_UPPER_TRE, @object
	.size	_ZN6icu_679LatinCase11TO_UPPER_TRE, 384
_ZN6icu_679LatinCase11TO_UPPER_TRE:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\340\340\340\340\340\340\340\340\200\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\200"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\200\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340"
	.string	"\340\340\340\340\340\340\340y"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\200"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	""
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377\200"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	""
	.string	"\377"
	.string	"\377"
	.ascii	"\377\200"
	.globl	_ZN6icu_679LatinCase15TO_UPPER_NORMALE
	.align 32
	.type	_ZN6icu_679LatinCase15TO_UPPER_NORMALE, @object
	.size	_ZN6icu_679LatinCase15TO_UPPER_NORMALE, 384
_ZN6icu_679LatinCase15TO_UPPER_NORMALE:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\200"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\200\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340\340"
	.string	"\340\340\340\340\340\340\340y"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\200"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	""
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377\200"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	"\377"
	.string	""
	.string	"\377"
	.string	"\377"
	.ascii	"\377\200"
	.globl	_ZN6icu_679LatinCase14TO_LOWER_TR_LTE
	.align 32
	.type	_ZN6icu_679LatinCase14TO_LOWER_TR_LTE, @object
	.size	_ZN6icu_679LatinCase14TO_LOWER_TR_LTE, 384
_ZN6icu_679LatinCase14TO_LOWER_TR_LTE:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"        \200\200                "
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\200"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"            \200\200         "
	.string	"       \200"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\200"
	.string	"\001"
	.string	"\001"
	.string	"\200"
	.string	"\200"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	""
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\200\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\207\001"
	.string	"\001"
	.string	"\001"
	.ascii	"\200"
	.globl	_ZN6icu_679LatinCase15TO_LOWER_NORMALE
	.align 32
	.type	_ZN6icu_679LatinCase15TO_LOWER_NORMALE, @object
	.size	_ZN6icu_679LatinCase15TO_LOWER_NORMALE, 384
_ZN6icu_679LatinCase15TO_LOWER_NORMALE:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"                          "
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\200"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"                       "
	.string	"       \200"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\200"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	""
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\200\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\207\001"
	.string	"\001"
	.string	"\001"
	.ascii	"\200"
	.align 2
	.type	_ZL9iDotTilde, @object
	.size	_ZL9iDotTilde, 6
_ZL9iDotTilde:
	.value	105
	.value	775
	.value	771
	.align 2
	.type	_ZL9iDotAcute, @object
	.size	_ZL9iDotAcute, 6
_ZL9iDotAcute:
	.value	105
	.value	775
	.value	769
	.align 2
	.type	_ZL9iDotGrave, @object
	.size	_ZL9iDotGrave, 6
_ZL9iDotGrave:
	.value	105
	.value	775
	.value	768
	.align 2
	.type	_ZL10iOgonekDot, @object
	.size	_ZL10iOgonekDot, 6
_ZL10iOgonekDot:
	.value	303
	.value	775
	.zero	2
	.align 2
	.type	_ZL4jDot, @object
	.size	_ZL4jDot, 4
_ZL4jDot:
	.value	106
	.value	775
	.align 2
	.type	_ZL4iDot, @object
	.size	_ZL4iDot, 4
_ZL4iDot:
	.value	105
	.value	775
	.align 32
	.type	_ZL11flagsOffset, @object
	.size	_ZL11flagsOffset, 256
_ZL11flagsOffset:
	.string	""
	.ascii	"\001\001\002\001\002\002\003\001\002\002\003\002\003\003\004"
	.ascii	"\001\002\002\003\002\003\003\004\002\003\003\004\003\004\004"
	.ascii	"\005\001\002\002\003\002\003\003\004\002\003\003\004\003\004"
	.ascii	"\004\005\002\003\003\004\003\004\004\005\003\004\004\005\004"
	.ascii	"\005\005\006\001\002\002\003\002\003\003\004\002\003\003\004"
	.ascii	"\003\004\004\005\002\003\003\004\003\004\004\005\003\004\004"
	.ascii	"\005\004\005\005\006\002\003\003\004\003\004\004\005\003\004"
	.ascii	"\004\005\004\005\005\006\003\004\004\005\004\005\005\006\004"
	.ascii	"\005\005\006\005\006\006\007\001\002\002\003\002\003\003\004"
	.ascii	"\002\003\003\004\003\004\004\005\002\003\003\004\003\004\004"
	.ascii	"\005\003\004\004\005\004\005\005\006\002\003\003\004\003\004"
	.ascii	"\004\005\003\004\004\005\004\005\005\006\003\004\004\005\004"
	.ascii	"\005\005\006\004\005\005\006\005\006\006\007\002\003\003\004"
	.ascii	"\003\004\004\005\003\004\004\005\004\005\005\006\003\004\004"
	.ascii	"\005\004\005\005\006\004\005\005\006\005\006\006\007\003\004"
	.ascii	"\004\005\004\005\005\006\004\005\005\006\005\006\006\007\004"
	.ascii	"\005\005\006\005\006\006\007\005\006\006\007\006\007\007\b"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL21ucase_props_singleton, @object
	.size	_ZL21ucase_props_singleton, 120
_ZL21ucase_props_singleton:
	.quad	0
	.quad	_ZL19ucase_props_indexes
	.quad	_ZL22ucase_props_exceptions
	.quad	_ZL18ucase_props_unfold
	.quad	_ZL21ucase_props_trieIndex
	.quad	_ZL21ucase_props_trieIndex+6576
	.quad	0
	.long	3288
	.long	9068
	.value	392
	.value	3412
	.long	0
	.long	0
	.long	919552
	.long	12352
	.zero	4
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.value	0
	.quad	0
	.string	"\004"
	.zero	2
	.zero	4
	.section	.rodata
	.align 32
	.type	_ZL18ucase_props_unfold, @object
	.size	_ZL18ucase_props_unfold, 740
_ZL18ucase_props_unfold:
	.value	73
	.value	5
	.value	3
	.value	0
	.value	0
	.value	97
	.value	702
	.value	0
	.value	7834
	.value	0
	.value	102
	.value	102
	.value	0
	.value	-1280
	.value	0
	.value	102
	.value	102
	.value	105
	.value	-1277
	.value	0
	.value	102
	.value	102
	.value	108
	.value	-1276
	.value	0
	.value	102
	.value	105
	.value	0
	.value	-1279
	.value	0
	.value	102
	.value	108
	.value	0
	.value	-1278
	.value	0
	.value	104
	.value	817
	.value	0
	.value	7830
	.value	0
	.value	105
	.value	775
	.value	0
	.value	304
	.value	0
	.value	106
	.value	780
	.value	0
	.value	496
	.value	0
	.value	115
	.value	115
	.value	0
	.value	223
	.value	7838
	.value	115
	.value	116
	.value	0
	.value	-1275
	.value	-1274
	.value	116
	.value	776
	.value	0
	.value	7831
	.value	0
	.value	119
	.value	778
	.value	0
	.value	7832
	.value	0
	.value	121
	.value	778
	.value	0
	.value	7833
	.value	0
	.value	700
	.value	110
	.value	0
	.value	329
	.value	0
	.value	940
	.value	953
	.value	0
	.value	8116
	.value	0
	.value	942
	.value	953
	.value	0
	.value	8132
	.value	0
	.value	945
	.value	834
	.value	0
	.value	8118
	.value	0
	.value	945
	.value	834
	.value	953
	.value	8119
	.value	0
	.value	945
	.value	953
	.value	0
	.value	8115
	.value	8124
	.value	951
	.value	834
	.value	0
	.value	8134
	.value	0
	.value	951
	.value	834
	.value	953
	.value	8135
	.value	0
	.value	951
	.value	953
	.value	0
	.value	8131
	.value	8140
	.value	953
	.value	776
	.value	768
	.value	8146
	.value	0
	.value	953
	.value	776
	.value	769
	.value	912
	.value	8147
	.value	953
	.value	776
	.value	834
	.value	8151
	.value	0
	.value	953
	.value	834
	.value	0
	.value	8150
	.value	0
	.value	961
	.value	787
	.value	0
	.value	8164
	.value	0
	.value	965
	.value	776
	.value	768
	.value	8162
	.value	0
	.value	965
	.value	776
	.value	769
	.value	944
	.value	8163
	.value	965
	.value	776
	.value	834
	.value	8167
	.value	0
	.value	965
	.value	787
	.value	0
	.value	8016
	.value	0
	.value	965
	.value	787
	.value	768
	.value	8018
	.value	0
	.value	965
	.value	787
	.value	769
	.value	8020
	.value	0
	.value	965
	.value	787
	.value	834
	.value	8022
	.value	0
	.value	965
	.value	834
	.value	0
	.value	8166
	.value	0
	.value	969
	.value	834
	.value	0
	.value	8182
	.value	0
	.value	969
	.value	834
	.value	953
	.value	8183
	.value	0
	.value	969
	.value	953
	.value	0
	.value	8179
	.value	8188
	.value	974
	.value	953
	.value	0
	.value	8180
	.value	0
	.value	1381
	.value	1410
	.value	0
	.value	1415
	.value	0
	.value	1396
	.value	1381
	.value	0
	.value	-1260
	.value	0
	.value	1396
	.value	1387
	.value	0
	.value	-1259
	.value	0
	.value	1396
	.value	1389
	.value	0
	.value	-1257
	.value	0
	.value	1396
	.value	1398
	.value	0
	.value	-1261
	.value	0
	.value	1406
	.value	1398
	.value	0
	.value	-1258
	.value	0
	.value	7936
	.value	953
	.value	0
	.value	8064
	.value	8072
	.value	7937
	.value	953
	.value	0
	.value	8065
	.value	8073
	.value	7938
	.value	953
	.value	0
	.value	8066
	.value	8074
	.value	7939
	.value	953
	.value	0
	.value	8067
	.value	8075
	.value	7940
	.value	953
	.value	0
	.value	8068
	.value	8076
	.value	7941
	.value	953
	.value	0
	.value	8069
	.value	8077
	.value	7942
	.value	953
	.value	0
	.value	8070
	.value	8078
	.value	7943
	.value	953
	.value	0
	.value	8071
	.value	8079
	.value	7968
	.value	953
	.value	0
	.value	8080
	.value	8088
	.value	7969
	.value	953
	.value	0
	.value	8081
	.value	8089
	.value	7970
	.value	953
	.value	0
	.value	8082
	.value	8090
	.value	7971
	.value	953
	.value	0
	.value	8083
	.value	8091
	.value	7972
	.value	953
	.value	0
	.value	8084
	.value	8092
	.value	7973
	.value	953
	.value	0
	.value	8085
	.value	8093
	.value	7974
	.value	953
	.value	0
	.value	8086
	.value	8094
	.value	7975
	.value	953
	.value	0
	.value	8087
	.value	8095
	.value	8032
	.value	953
	.value	0
	.value	8096
	.value	8104
	.value	8033
	.value	953
	.value	0
	.value	8097
	.value	8105
	.value	8034
	.value	953
	.value	0
	.value	8098
	.value	8106
	.value	8035
	.value	953
	.value	0
	.value	8099
	.value	8107
	.value	8036
	.value	953
	.value	0
	.value	8100
	.value	8108
	.value	8037
	.value	953
	.value	0
	.value	8101
	.value	8109
	.value	8038
	.value	953
	.value	0
	.value	8102
	.value	8110
	.value	8039
	.value	953
	.value	0
	.value	8103
	.value	8111
	.value	8048
	.value	953
	.value	0
	.value	8114
	.value	0
	.value	8052
	.value	953
	.value	0
	.value	8130
	.value	0
	.value	8060
	.value	953
	.value	0
	.value	8178
	.zero	2
	.align 32
	.type	_ZL22ucase_props_exceptions, @object
	.size	_ZL22ucase_props_exceptions, 3342
_ZL22ucase_props_exceptions:
	.value	-14256
	.value	32
	.value	2
	.value	304
	.value	305
	.value	18448
	.value	32
	.value	2113
	.value	107
	.value	1
	.value	8490
	.value	2113
	.value	115
	.value	1
	.value	383
	.value	23632
	.value	32
	.value	2
	.value	304
	.value	305
	.value	2116
	.value	75
	.value	1
	.value	8490
	.value	2116
	.value	83
	.value	1
	.value	383
	.value	2054
	.value	956
	.value	924
	.value	2113
	.value	229
	.value	1
	.value	8491
	.value	2240
	.value	1
	.value	8736
	.value	115
	.value	115
	.value	83
	.value	83
	.value	83
	.value	115
	.value	7838
	.value	2116
	.value	197
	.value	1
	.value	8491
	.value	18448
	.value	1
	.value	-12720
	.value	199
	.value	2
	.value	73
	.value	305
	.value	2116
	.value	73
	.value	2
	.value	105
	.value	304
	.value	2176
	.value	8736
	.value	700
	.value	110
	.value	700
	.value	78
	.value	700
	.value	78
	.value	2054
	.value	115
	.value	83
	.value	2057
	.value	454
	.value	453
	.value	2061
	.value	454
	.value	452
	.value	453
	.value	2060
	.value	452
	.value	453
	.value	2057
	.value	457
	.value	456
	.value	2061
	.value	457
	.value	455
	.value	456
	.value	2060
	.value	455
	.value	456
	.value	2057
	.value	460
	.value	459
	.value	2061
	.value	460
	.value	458
	.value	459
	.value	2060
	.value	458
	.value	459
	.value	2176
	.value	8736
	.value	106
	.value	780
	.value	74
	.value	780
	.value	74
	.value	780
	.value	2057
	.value	499
	.value	498
	.value	2061
	.value	499
	.value	497
	.value	498
	.value	2060
	.value	497
	.value	498
	.value	2064
	.value	10795
	.value	2064
	.value	10792
	.value	2064
	.value	10815
	.value	2064
	.value	10783
	.value	2064
	.value	10780
	.value	2064
	.value	10782
	.value	2064
	.value	-23217
	.value	2064
	.value	-23221
	.value	2064
	.value	-23256
	.value	2064
	.value	-23228
	.value	2064
	.value	10743
	.value	2064
	.value	-23231
	.value	2064
	.value	10749
	.value	2064
	.value	10727
	.value	2064
	.value	-23229
	.value	2064
	.value	-23254
	.value	6160
	.value	-23275
	.value	2064
	.value	-23278
	.value	26624
	.value	14406
	.value	953
	.value	921
	.value	1
	.value	8126
	.value	2240
	.value	1
	.value	13104
	.value	953
	.value	776
	.value	769
	.value	921
	.value	776
	.value	769
	.value	921
	.value	776
	.value	769
	.value	8147
	.value	2113
	.value	946
	.value	1
	.value	976
	.value	2113
	.value	949
	.value	1
	.value	1013
	.value	2113
	.value	952
	.value	2
	.value	977
	.value	1012
	.value	2113
	.value	953
	.value	2
	.value	837
	.value	8126
	.value	2113
	.value	954
	.value	1
	.value	1008
	.value	2113
	.value	956
	.value	1
	.value	181
	.value	2113
	.value	960
	.value	1
	.value	982
	.value	2113
	.value	961
	.value	1
	.value	1009
	.value	18512
	.value	32
	.value	1
	.value	962
	.value	2113
	.value	966
	.value	1
	.value	981
	.value	2113
	.value	969
	.value	1
	.value	8486
	.value	2240
	.value	1
	.value	13104
	.value	965
	.value	776
	.value	769
	.value	933
	.value	776
	.value	769
	.value	933
	.value	776
	.value	769
	.value	8163
	.value	2116
	.value	914
	.value	1
	.value	976
	.value	2116
	.value	917
	.value	1
	.value	1013
	.value	2116
	.value	920
	.value	2
	.value	977
	.value	1012
	.value	2116
	.value	921
	.value	2
	.value	837
	.value	8126
	.value	2116
	.value	922
	.value	1
	.value	1008
	.value	2116
	.value	924
	.value	1
	.value	181
	.value	2116
	.value	928
	.value	1
	.value	982
	.value	2116
	.value	929
	.value	1
	.value	1009
	.value	2054
	.value	963
	.value	931
	.value	2116
	.value	931
	.value	1
	.value	962
	.value	2116
	.value	934
	.value	1
	.value	981
	.value	2116
	.value	937
	.value	1
	.value	8486
	.value	2054
	.value	946
	.value	914
	.value	2118
	.value	952
	.value	920
	.value	1
	.value	1012
	.value	2054
	.value	966
	.value	934
	.value	2054
	.value	960
	.value	928
	.value	2054
	.value	954
	.value	922
	.value	2054
	.value	961
	.value	929
	.value	2113
	.value	952
	.value	2
	.value	920
	.value	977
	.value	2054
	.value	949
	.value	917
	.value	2113
	.value	1074
	.value	1
	.value	7296
	.value	2113
	.value	1076
	.value	1
	.value	7297
	.value	2113
	.value	1086
	.value	1
	.value	7298
	.value	2113
	.value	1089
	.value	1
	.value	7299
	.value	2113
	.value	1090
	.value	2
	.value	7300
	.value	7301
	.value	2113
	.value	1098
	.value	1
	.value	7302
	.value	2116
	.value	1042
	.value	1
	.value	7296
	.value	2116
	.value	1044
	.value	1
	.value	7297
	.value	2116
	.value	1054
	.value	1
	.value	7298
	.value	2116
	.value	1057
	.value	1
	.value	7299
	.value	2116
	.value	1058
	.value	2
	.value	7300
	.value	7301
	.value	2116
	.value	1066
	.value	1
	.value	7302
	.value	2113
	.value	1123
	.value	1
	.value	7303
	.value	2116
	.value	1122
	.value	1
	.value	7303
	.value	2176
	.value	8736
	.value	1381
	.value	1410
	.value	1333
	.value	1362
	.value	1333
	.value	1410
	.value	2064
	.value	7264
	.value	2060
	.value	7312
	.value	4304
	.value	2060
	.value	7313
	.value	4305
	.value	2060
	.value	7314
	.value	4306
	.value	2060
	.value	7315
	.value	4307
	.value	2060
	.value	7316
	.value	4308
	.value	2060
	.value	7317
	.value	4309
	.value	2060
	.value	7318
	.value	4310
	.value	2060
	.value	7319
	.value	4311
	.value	2060
	.value	7320
	.value	4312
	.value	2060
	.value	7321
	.value	4313
	.value	2060
	.value	7322
	.value	4314
	.value	2060
	.value	7323
	.value	4315
	.value	2060
	.value	7324
	.value	4316
	.value	2060
	.value	7325
	.value	4317
	.value	2060
	.value	7326
	.value	4318
	.value	2060
	.value	7327
	.value	4319
	.value	2060
	.value	7328
	.value	4320
	.value	2060
	.value	7329
	.value	4321
	.value	2060
	.value	7330
	.value	4322
	.value	2060
	.value	7331
	.value	4323
	.value	2060
	.value	7332
	.value	4324
	.value	2060
	.value	7333
	.value	4325
	.value	2060
	.value	7334
	.value	4326
	.value	2060
	.value	7335
	.value	4327
	.value	2060
	.value	7336
	.value	4328
	.value	2060
	.value	7337
	.value	4329
	.value	2060
	.value	7338
	.value	4330
	.value	2060
	.value	7339
	.value	4331
	.value	2060
	.value	7340
	.value	4332
	.value	2060
	.value	7341
	.value	4333
	.value	2060
	.value	7342
	.value	4334
	.value	2060
	.value	7343
	.value	4335
	.value	2060
	.value	7344
	.value	4336
	.value	2060
	.value	7345
	.value	4337
	.value	2060
	.value	7346
	.value	4338
	.value	2060
	.value	7347
	.value	4339
	.value	2060
	.value	7348
	.value	4340
	.value	2060
	.value	7349
	.value	4341
	.value	2060
	.value	7350
	.value	4342
	.value	2060
	.value	7351
	.value	4343
	.value	2060
	.value	7352
	.value	4344
	.value	2060
	.value	7353
	.value	4345
	.value	2060
	.value	7354
	.value	4346
	.value	2060
	.value	7357
	.value	4349
	.value	2060
	.value	7358
	.value	4350
	.value	2060
	.value	7359
	.value	4351
	.value	2576
	.value	-26672
	.value	2576
	.value	8
	.value	2054
	.value	5104
	.value	5104
	.value	2054
	.value	5105
	.value	5105
	.value	2054
	.value	5106
	.value	5106
	.value	2054
	.value	5107
	.value	5107
	.value	2054
	.value	5108
	.value	5108
	.value	2054
	.value	5109
	.value	5109
	.value	2054
	.value	1074
	.value	1042
	.value	2054
	.value	1076
	.value	1044
	.value	2054
	.value	1086
	.value	1054
	.value	2054
	.value	1089
	.value	1057
	.value	2118
	.value	1090
	.value	1058
	.value	1
	.value	7301
	.value	2118
	.value	1090
	.value	1058
	.value	1
	.value	7300
	.value	2054
	.value	1098
	.value	1066
	.value	2054
	.value	1123
	.value	1122
	.value	2054
	.value	-22965
	.value	-22966
	.value	3088
	.value	3008
	.value	2064
	.value	-30204
	.value	2064
	.value	3814
	.value	2064
	.value	-30152
	.value	2113
	.value	7777
	.value	1
	.value	7835
	.value	2116
	.value	7776
	.value	1
	.value	7835
	.value	2176
	.value	8736
	.value	104
	.value	817
	.value	72
	.value	817
	.value	72
	.value	817
	.value	2176
	.value	8736
	.value	116
	.value	776
	.value	84
	.value	776
	.value	84
	.value	776
	.value	2176
	.value	8736
	.value	119
	.value	778
	.value	87
	.value	778
	.value	87
	.value	778
	.value	2176
	.value	8736
	.value	121
	.value	778
	.value	89
	.value	778
	.value	89
	.value	778
	.value	2176
	.value	8736
	.value	97
	.value	702
	.value	65
	.value	702
	.value	65
	.value	702
	.value	2054
	.value	7777
	.value	7776
	.value	3216
	.value	7615
	.value	32
	.value	115
	.value	115
	.value	2176
	.value	8736
	.value	965
	.value	787
	.value	933
	.value	787
	.value	933
	.value	787
	.value	2176
	.value	13104
	.value	965
	.value	787
	.value	768
	.value	933
	.value	787
	.value	768
	.value	933
	.value	787
	.value	768
	.value	2176
	.value	13104
	.value	965
	.value	787
	.value	769
	.value	933
	.value	787
	.value	769
	.value	933
	.value	787
	.value	769
	.value	2176
	.value	13104
	.value	965
	.value	787
	.value	834
	.value	933
	.value	787
	.value	834
	.value	933
	.value	787
	.value	834
	.value	2192
	.value	8
	.value	544
	.value	7936
	.value	953
	.value	7944
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7937
	.value	953
	.value	7945
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7938
	.value	953
	.value	7946
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7939
	.value	953
	.value	7947
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7940
	.value	953
	.value	7948
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7941
	.value	953
	.value	7949
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7942
	.value	953
	.value	7950
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7943
	.value	953
	.value	7951
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7936
	.value	953
	.value	7944
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7937
	.value	953
	.value	7945
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7938
	.value	953
	.value	7946
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7939
	.value	953
	.value	7947
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7940
	.value	953
	.value	7948
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7941
	.value	953
	.value	7949
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7942
	.value	953
	.value	7950
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7943
	.value	953
	.value	7951
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7968
	.value	953
	.value	7976
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7969
	.value	953
	.value	7977
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7970
	.value	953
	.value	7978
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7971
	.value	953
	.value	7979
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7972
	.value	953
	.value	7980
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7973
	.value	953
	.value	7981
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7974
	.value	953
	.value	7982
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	7975
	.value	953
	.value	7983
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7968
	.value	953
	.value	7976
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7969
	.value	953
	.value	7977
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7970
	.value	953
	.value	7978
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7971
	.value	953
	.value	7979
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7972
	.value	953
	.value	7980
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7973
	.value	953
	.value	7981
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7974
	.value	953
	.value	7982
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	7975
	.value	953
	.value	7983
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	8032
	.value	953
	.value	8040
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	8033
	.value	953
	.value	8041
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	8034
	.value	953
	.value	8042
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	8035
	.value	953
	.value	8043
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	8036
	.value	953
	.value	8044
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	8037
	.value	953
	.value	8045
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	8038
	.value	953
	.value	8046
	.value	921
	.value	2192
	.value	8
	.value	544
	.value	8039
	.value	953
	.value	8047
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	8032
	.value	953
	.value	8040
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	8033
	.value	953
	.value	8041
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	8034
	.value	953
	.value	8042
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	8035
	.value	953
	.value	8043
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	8036
	.value	953
	.value	8044
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	8037
	.value	953
	.value	8045
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	8038
	.value	953
	.value	8046
	.value	921
	.value	3216
	.value	8
	.value	544
	.value	8039
	.value	953
	.value	8047
	.value	921
	.value	2176
	.value	8736
	.value	8048
	.value	953
	.value	8122
	.value	921
	.value	8122
	.value	837
	.value	2192
	.value	9
	.value	544
	.value	945
	.value	953
	.value	913
	.value	921
	.value	2176
	.value	8736
	.value	940
	.value	953
	.value	902
	.value	921
	.value	902
	.value	837
	.value	2176
	.value	8736
	.value	945
	.value	834
	.value	913
	.value	834
	.value	913
	.value	834
	.value	2176
	.value	13104
	.value	945
	.value	834
	.value	953
	.value	913
	.value	834
	.value	921
	.value	913
	.value	834
	.value	837
	.value	3216
	.value	9
	.value	544
	.value	945
	.value	953
	.value	913
	.value	921
	.value	2118
	.value	953
	.value	921
	.value	1
	.value	837
	.value	2176
	.value	8736
	.value	8052
	.value	953
	.value	8138
	.value	921
	.value	8138
	.value	837
	.value	2192
	.value	9
	.value	544
	.value	951
	.value	953
	.value	919
	.value	921
	.value	2176
	.value	8736
	.value	942
	.value	953
	.value	905
	.value	921
	.value	905
	.value	837
	.value	2176
	.value	8736
	.value	951
	.value	834
	.value	919
	.value	834
	.value	919
	.value	834
	.value	2176
	.value	13104
	.value	951
	.value	834
	.value	953
	.value	919
	.value	834
	.value	921
	.value	919
	.value	834
	.value	837
	.value	3216
	.value	9
	.value	544
	.value	951
	.value	953
	.value	919
	.value	921
	.value	2176
	.value	13104
	.value	953
	.value	776
	.value	768
	.value	921
	.value	776
	.value	768
	.value	921
	.value	776
	.value	768
	.value	2240
	.value	1
	.value	13104
	.value	953
	.value	776
	.value	769
	.value	921
	.value	776
	.value	769
	.value	921
	.value	776
	.value	769
	.value	912
	.value	2176
	.value	8736
	.value	953
	.value	834
	.value	921
	.value	834
	.value	921
	.value	834
	.value	2176
	.value	13104
	.value	953
	.value	776
	.value	834
	.value	921
	.value	776
	.value	834
	.value	921
	.value	776
	.value	834
	.value	2176
	.value	13104
	.value	965
	.value	776
	.value	768
	.value	933
	.value	776
	.value	768
	.value	933
	.value	776
	.value	768
	.value	2240
	.value	1
	.value	13104
	.value	965
	.value	776
	.value	769
	.value	933
	.value	776
	.value	769
	.value	933
	.value	776
	.value	769
	.value	944
	.value	2176
	.value	8736
	.value	961
	.value	787
	.value	929
	.value	787
	.value	929
	.value	787
	.value	2176
	.value	8736
	.value	965
	.value	834
	.value	933
	.value	834
	.value	933
	.value	834
	.value	2176
	.value	13104
	.value	965
	.value	776
	.value	834
	.value	933
	.value	776
	.value	834
	.value	933
	.value	776
	.value	834
	.value	2176
	.value	8736
	.value	8060
	.value	953
	.value	8186
	.value	921
	.value	8186
	.value	837
	.value	2192
	.value	9
	.value	544
	.value	969
	.value	953
	.value	937
	.value	921
	.value	2176
	.value	8736
	.value	974
	.value	953
	.value	911
	.value	921
	.value	911
	.value	837
	.value	2176
	.value	8736
	.value	969
	.value	834
	.value	937
	.value	834
	.value	937
	.value	834
	.value	2176
	.value	13104
	.value	969
	.value	834
	.value	953
	.value	937
	.value	834
	.value	921
	.value	937
	.value	834
	.value	837
	.value	3216
	.value	9
	.value	544
	.value	969
	.value	953
	.value	937
	.value	921
	.value	3152
	.value	7517
	.value	1
	.value	937
	.value	3152
	.value	8383
	.value	1
	.value	75
	.value	3152
	.value	8262
	.value	1
	.value	197
	.value	3088
	.value	10743
	.value	3088
	.value	3814
	.value	3088
	.value	10727
	.value	3088
	.value	10795
	.value	3088
	.value	10792
	.value	3088
	.value	10780
	.value	3088
	.value	10749
	.value	3088
	.value	10783
	.value	3088
	.value	10782
	.value	3088
	.value	10815
	.value	3088
	.value	7264
	.value	2113
	.value	-22965
	.value	1
	.value	7304
	.value	2116
	.value	-22966
	.value	1
	.value	7304
	.value	3088
	.value	-30204
	.value	3088
	.value	-23256
	.value	3088
	.value	-23228
	.value	3088
	.value	-23217
	.value	3088
	.value	-23221
	.value	3088
	.value	-23231
	.value	3088
	.value	-23278
	.value	3088
	.value	-23254
	.value	3088
	.value	-23275
	.value	2064
	.value	928
	.value	3088
	.value	-23229
	.value	3088
	.value	-30152
	.value	3088
	.value	928
	.value	2054
	.value	5024
	.value	5024
	.value	2054
	.value	5025
	.value	5025
	.value	2054
	.value	5026
	.value	5026
	.value	2054
	.value	5027
	.value	5027
	.value	2054
	.value	5028
	.value	5028
	.value	2054
	.value	5029
	.value	5029
	.value	2054
	.value	5030
	.value	5030
	.value	2054
	.value	5031
	.value	5031
	.value	2054
	.value	5032
	.value	5032
	.value	2054
	.value	5033
	.value	5033
	.value	2054
	.value	5034
	.value	5034
	.value	2054
	.value	5035
	.value	5035
	.value	2054
	.value	5036
	.value	5036
	.value	2054
	.value	5037
	.value	5037
	.value	2054
	.value	5038
	.value	5038
	.value	2054
	.value	5039
	.value	5039
	.value	2054
	.value	5040
	.value	5040
	.value	2054
	.value	5041
	.value	5041
	.value	2054
	.value	5042
	.value	5042
	.value	2054
	.value	5043
	.value	5043
	.value	2054
	.value	5044
	.value	5044
	.value	2054
	.value	5045
	.value	5045
	.value	2054
	.value	5046
	.value	5046
	.value	2054
	.value	5047
	.value	5047
	.value	2054
	.value	5048
	.value	5048
	.value	2054
	.value	5049
	.value	5049
	.value	2054
	.value	5050
	.value	5050
	.value	2054
	.value	5051
	.value	5051
	.value	2054
	.value	5052
	.value	5052
	.value	2054
	.value	5053
	.value	5053
	.value	2054
	.value	5054
	.value	5054
	.value	2054
	.value	5055
	.value	5055
	.value	2054
	.value	5056
	.value	5056
	.value	2054
	.value	5057
	.value	5057
	.value	2054
	.value	5058
	.value	5058
	.value	2054
	.value	5059
	.value	5059
	.value	2054
	.value	5060
	.value	5060
	.value	2054
	.value	5061
	.value	5061
	.value	2054
	.value	5062
	.value	5062
	.value	2054
	.value	5063
	.value	5063
	.value	2054
	.value	5064
	.value	5064
	.value	2054
	.value	5065
	.value	5065
	.value	2054
	.value	5066
	.value	5066
	.value	2054
	.value	5067
	.value	5067
	.value	2054
	.value	5068
	.value	5068
	.value	2054
	.value	5069
	.value	5069
	.value	2054
	.value	5070
	.value	5070
	.value	2054
	.value	5071
	.value	5071
	.value	2054
	.value	5072
	.value	5072
	.value	2054
	.value	5073
	.value	5073
	.value	2054
	.value	5074
	.value	5074
	.value	2054
	.value	5075
	.value	5075
	.value	2054
	.value	5076
	.value	5076
	.value	2054
	.value	5077
	.value	5077
	.value	2054
	.value	5078
	.value	5078
	.value	2054
	.value	5079
	.value	5079
	.value	2054
	.value	5080
	.value	5080
	.value	2054
	.value	5081
	.value	5081
	.value	2054
	.value	5082
	.value	5082
	.value	2054
	.value	5083
	.value	5083
	.value	2054
	.value	5084
	.value	5084
	.value	2054
	.value	5085
	.value	5085
	.value	2054
	.value	5086
	.value	5086
	.value	2054
	.value	5087
	.value	5087
	.value	2054
	.value	5088
	.value	5088
	.value	2054
	.value	5089
	.value	5089
	.value	2054
	.value	5090
	.value	5090
	.value	2054
	.value	5091
	.value	5091
	.value	2054
	.value	5092
	.value	5092
	.value	2054
	.value	5093
	.value	5093
	.value	2054
	.value	5094
	.value	5094
	.value	2054
	.value	5095
	.value	5095
	.value	2054
	.value	5096
	.value	5096
	.value	2054
	.value	5097
	.value	5097
	.value	2054
	.value	5098
	.value	5098
	.value	2054
	.value	5099
	.value	5099
	.value	2054
	.value	5100
	.value	5100
	.value	2054
	.value	5101
	.value	5101
	.value	2054
	.value	5102
	.value	5102
	.value	2054
	.value	5103
	.value	5103
	.value	2176
	.value	8736
	.value	102
	.value	102
	.value	70
	.value	70
	.value	70
	.value	102
	.value	2176
	.value	8736
	.value	102
	.value	105
	.value	70
	.value	73
	.value	70
	.value	105
	.value	2176
	.value	8736
	.value	102
	.value	108
	.value	70
	.value	76
	.value	70
	.value	108
	.value	2176
	.value	13104
	.value	102
	.value	102
	.value	105
	.value	70
	.value	70
	.value	73
	.value	70
	.value	102
	.value	105
	.value	2176
	.value	13104
	.value	102
	.value	102
	.value	108
	.value	70
	.value	70
	.value	76
	.value	70
	.value	102
	.value	108
	.value	2240
	.value	1
	.value	8736
	.value	115
	.value	116
	.value	83
	.value	84
	.value	83
	.value	116
	.value	-1274
	.value	2240
	.value	1
	.value	8736
	.value	115
	.value	116
	.value	83
	.value	84
	.value	83
	.value	116
	.value	-1275
	.value	2176
	.value	8736
	.value	1396
	.value	1398
	.value	1348
	.value	1350
	.value	1348
	.value	1398
	.value	2176
	.value	8736
	.value	1396
	.value	1381
	.value	1348
	.value	1333
	.value	1348
	.value	1381
	.value	2176
	.value	8736
	.value	1396
	.value	1387
	.value	1348
	.value	1339
	.value	1348
	.value	1387
	.value	2176
	.value	8736
	.value	1406
	.value	1398
	.value	1358
	.value	1350
	.value	1358
	.value	1398
	.value	2176
	.value	8736
	.value	1396
	.value	1389
	.value	1348
	.value	1341
	.value	1348
	.value	1389
	.align 32
	.type	_ZL21ucase_props_trieIndex, @object
	.size	_ZL21ucase_props_trieIndex, 24712
_ZL21ucase_props_trieIndex:
	.value	822
	.value	830
	.value	838
	.value	846
	.value	860
	.value	868
	.value	876
	.value	884
	.value	892
	.value	900
	.value	907
	.value	915
	.value	923
	.value	931
	.value	939
	.value	947
	.value	953
	.value	961
	.value	969
	.value	977
	.value	985
	.value	993
	.value	1001
	.value	1009
	.value	1017
	.value	1025
	.value	1033
	.value	1041
	.value	1049
	.value	1057
	.value	1065
	.value	1073
	.value	1081
	.value	1089
	.value	1097
	.value	1105
	.value	1113
	.value	1121
	.value	1129
	.value	1137
	.value	1133
	.value	1141
	.value	1146
	.value	1154
	.value	1161
	.value	1169
	.value	1177
	.value	1185
	.value	1193
	.value	1201
	.value	1209
	.value	1217
	.value	853
	.value	861
	.value	1222
	.value	1230
	.value	1235
	.value	1243
	.value	1251
	.value	1259
	.value	1258
	.value	1266
	.value	1271
	.value	1279
	.value	1287
	.value	1294
	.value	1298
	.value	853
	.value	853
	.value	853
	.value	1305
	.value	1313
	.value	1321
	.value	1323
	.value	1331
	.value	1339
	.value	1343
	.value	1344
	.value	1352
	.value	1360
	.value	1368
	.value	1344
	.value	1376
	.value	1381
	.value	1368
	.value	1344
	.value	1389
	.value	1397
	.value	1343
	.value	1405
	.value	1413
	.value	1421
	.value	1429
	.value	853
	.value	1437
	.value	853
	.value	1445
	.value	1260
	.value	1453
	.value	1421
	.value	1343
	.value	1405
	.value	1460
	.value	1421
	.value	1468
	.value	1470
	.value	1352
	.value	1421
	.value	1343
	.value	853
	.value	1478
	.value	853
	.value	853
	.value	1484
	.value	1491
	.value	853
	.value	853
	.value	1495
	.value	1503
	.value	853
	.value	1507
	.value	1514
	.value	853
	.value	1521
	.value	1529
	.value	1536
	.value	1544
	.value	853
	.value	853
	.value	1549
	.value	1557
	.value	1565
	.value	1573
	.value	1581
	.value	1588
	.value	1596
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	1604
	.value	853
	.value	853
	.value	1620
	.value	1620
	.value	1612
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	1628
	.value	1628
	.value	1356
	.value	1356
	.value	853
	.value	1634
	.value	1642
	.value	853
	.value	1650
	.value	853
	.value	1658
	.value	853
	.value	1665
	.value	1671
	.value	853
	.value	853
	.value	853
	.value	1679
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	1686
	.value	853
	.value	1693
	.value	1701
	.value	853
	.value	1709
	.value	1717
	.value	853
	.value	1404
	.value	1720
	.value	1728
	.value	1734
	.value	1468
	.value	1742
	.value	853
	.value	1749
	.value	853
	.value	1754
	.value	853
	.value	1760
	.value	1768
	.value	1772
	.value	1780
	.value	1788
	.value	1796
	.value	1801
	.value	1804
	.value	1812
	.value	1828
	.value	1820
	.value	1844
	.value	1836
	.value	892
	.value	1852
	.value	892
	.value	1860
	.value	1863
	.value	892
	.value	1871
	.value	892
	.value	1879
	.value	1887
	.value	1895
	.value	1903
	.value	1911
	.value	1919
	.value	1927
	.value	1935
	.value	1943
	.value	1950
	.value	853
	.value	1958
	.value	1966
	.value	853
	.value	1974
	.value	1982
	.value	1990
	.value	1998
	.value	2006
	.value	2014
	.value	2022
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2025
	.value	2031
	.value	2037
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2045
	.value	2050
	.value	2054
	.value	2062
	.value	892
	.value	892
	.value	892
	.value	2070
	.value	2078
	.value	2085
	.value	853
	.value	2090
	.value	853
	.value	853
	.value	853
	.value	2098
	.value	853
	.value	1655
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	1342
	.value	2106
	.value	853
	.value	853
	.value	2113
	.value	853
	.value	853
	.value	2121
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2129
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	1760
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2135
	.value	853
	.value	2143
	.value	2148
	.value	2156
	.value	853
	.value	853
	.value	2164
	.value	2172
	.value	2180
	.value	892
	.value	2185
	.value	2193
	.value	2199
	.value	2207
	.value	2210
	.value	2218
	.value	2225
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2232
	.value	2240
	.value	853
	.value	2248
	.value	2255
	.value	853
	.value	1321
	.value	2260
	.value	2268
	.value	1665
	.value	853
	.value	2274
	.value	2282
	.value	2286
	.value	853
	.value	2294
	.value	2302
	.value	2310
	.value	853
	.value	2316
	.value	2320
	.value	2328
	.value	2344
	.value	2336
	.value	853
	.value	2352
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2360
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2368
	.value	1468
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2373
	.value	2381
	.value	2385
	.value	853
	.value	853
	.value	853
	.value	853
	.value	824
	.value	830
	.value	2393
	.value	2401
	.value	2408
	.value	1260
	.value	853
	.value	853
	.value	2416
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	3416
	.value	3416
	.value	3440
	.value	3504
	.value	3568
	.value	3628
	.value	3692
	.value	3756
	.value	3812
	.value	3876
	.value	3940
	.value	4004
	.value	4068
	.value	4132
	.value	4196
	.value	4260
	.value	4324
	.value	4388
	.value	4452
	.value	4516
	.value	4532
	.value	4584
	.value	4644
	.value	4708
	.value	4772
	.value	4836
	.value	3412
	.value	4888
	.value	4940
	.value	5004
	.value	5032
	.value	5084
	.value	2529
	.value	2577
	.value	2641
	.value	2704
	.value	392
	.value	392
	.value	2760
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	2801
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	2865
	.value	392
	.value	392
	.value	2918
	.value	2981
	.value	3045
	.value	3103
	.value	3158
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	392
	.value	3222
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2423
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	1717
	.value	853
	.value	853
	.value	853
	.value	2431
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2439
	.value	2445
	.value	2449
	.value	853
	.value	853
	.value	2453
	.value	2457
	.value	2463
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2471
	.value	2475
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2483
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2491
	.value	2495
	.value	2503
	.value	2507
	.value	853
	.value	2514
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2520
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2527
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	1343
	.value	2532
	.value	2539
	.value	1469
	.value	1468
	.value	2543
	.value	1340
	.value	853
	.value	2551
	.value	2558
	.value	853
	.value	2564
	.value	1468
	.value	2569
	.value	2577
	.value	853
	.value	853
	.value	2582
	.value	853
	.value	853
	.value	853
	.value	853
	.value	824
	.value	2590
	.value	1468
	.value	1470
	.value	2598
	.value	2605
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2532
	.value	2613
	.value	853
	.value	853
	.value	2621
	.value	2629
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2633
	.value	2641
	.value	853
	.value	853
	.value	2649
	.value	1200
	.value	853
	.value	853
	.value	2657
	.value	853
	.value	853
	.value	2663
	.value	2671
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2676
	.value	853
	.value	853
	.value	853
	.value	2684
	.value	2692
	.value	853
	.value	853
	.value	2700
	.value	2708
	.value	853
	.value	853
	.value	853
	.value	2711
	.value	1717
	.value	2719
	.value	2723
	.value	2731
	.value	853
	.value	2738
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2745
	.value	853
	.value	853
	.value	2368
	.value	2753
	.value	853
	.value	853
	.value	853
	.value	2759
	.value	2767
	.value	853
	.value	2771
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2777
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2783
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2790
	.value	853
	.value	2796
	.value	1404
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2684
	.value	2692
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	1655
	.value	853
	.value	2802
	.value	853
	.value	853
	.value	2810
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2815
	.value	1404
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2823
	.value	2831
	.value	2837
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2845
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2853
	.value	2861
	.value	2866
	.value	2872
	.value	2880
	.value	2888
	.value	2896
	.value	2857
	.value	2904
	.value	2912
	.value	2920
	.value	2927
	.value	2858
	.value	2853
	.value	2861
	.value	2856
	.value	2872
	.value	2859
	.value	2854
	.value	2935
	.value	2857
	.value	2943
	.value	2951
	.value	2959
	.value	2966
	.value	2946
	.value	2954
	.value	2962
	.value	2969
	.value	2949
	.value	2977
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2172
	.value	2985
	.value	2172
	.value	2992
	.value	2999
	.value	3007
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	3015
	.value	3023
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	3027
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2512
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	3035
	.value	853
	.value	3043
	.value	3051
	.value	3058
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2849
	.value	3066
	.value	3066
	.value	3072
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2553
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	1343
	.value	2172
	.value	2172
	.value	2172
	.value	853
	.value	853
	.value	853
	.value	853
	.value	2172
	.value	2172
	.value	2172
	.value	2172
	.value	2172
	.value	2172
	.value	2172
	.value	3080
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	853
	.value	821
	.value	821
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	10
	.value	90
	.value	122
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	186
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	4
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	249
	.value	-4047
	.value	329
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	393
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	1
	.value	0
	.value	0
	.value	4
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	457
	.value	0
	.value	4
	.value	4
	.value	0
	.value	1
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	506
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	90
	.value	90
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	0
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	569
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	729
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	0
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	15505
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	794
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	794
	.value	-79
	.value	826
	.value	905
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	1
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	985
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	-15470
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	1113
	.value	24977
	.value	26898
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	26386
	.value	146
	.value	-111
	.value	26258
	.value	26258
	.value	146
	.value	-111
	.value	1
	.value	10130
	.value	25874
	.value	26002
	.value	146
	.value	-111
	.value	26258
	.value	26514
	.value	12433
	.value	27026
	.value	26770
	.value	146
	.value	-111
	.value	20881
	.value	1
	.value	27026
	.value	27282
	.value	16657
	.value	27410
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	27922
	.value	146
	.value	-111
	.value	27922
	.value	1
	.value	1
	.value	146
	.value	-111
	.value	27922
	.value	146
	.value	-111
	.value	27794
	.value	27794
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	28050
	.value	146
	.value	-111
	.value	1
	.value	0
	.value	146
	.value	-111
	.value	1
	.value	7185
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1162
	.value	1211
	.value	1273
	.value	1322
	.value	1371
	.value	1433
	.value	1482
	.value	1531
	.value	1593
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	-10095
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	1641
	.value	1770
	.value	1819
	.value	1881
	.value	146
	.value	-111
	.value	-12398
	.value	-7150
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	-16622
	.value	1
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1930
	.value	146
	.value	-111
	.value	-20846
	.value	1962
	.value	1993
	.value	1993
	.value	146
	.value	-111
	.value	-24942
	.value	8850
	.value	9106
	.value	146
	.value	-111
	.value	146
	.value	-79
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	2025
	.value	2057
	.value	2089
	.value	-26863
	.value	-26351
	.value	1
	.value	-26223
	.value	-26223
	.value	1
	.value	-25839
	.value	1
	.value	-25967
	.value	2121
	.value	1
	.value	1
	.value	1
	.value	-26223
	.value	2153
	.value	1
	.value	-26479
	.value	1
	.value	2185
	.value	2217
	.value	1
	.value	-26703
	.value	-26991
	.value	2217
	.value	2249
	.value	2281
	.value	1
	.value	1
	.value	-26991
	.value	1
	.value	2313
	.value	-27247
	.value	1
	.value	1
	.value	-27375
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2345
	.value	1
	.value	1
	.value	-27887
	.value	1
	.value	2377
	.value	-27887
	.value	1
	.value	1
	.value	1
	.value	2409
	.value	-27887
	.value	-8815
	.value	-27759
	.value	-27759
	.value	-9071
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-28015
	.value	1
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2441
	.value	2473
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	5
	.value	5
	.value	37
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	4
	.value	4
	.value	4
	.value	20
	.value	4
	.value	20
	.value	4
	.value	5
	.value	5
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	84
	.value	84
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	2508
	.value	84
	.value	68
	.value	84
	.value	68
	.value	84
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	84
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	116
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	84
	.value	68
	.value	68
	.value	2525
	.value	68
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	4
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	100
	.value	100
	.value	100
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	4
	.value	4
	.value	146
	.value	-111
	.value	0
	.value	0
	.value	5
	.value	16657
	.value	16657
	.value	16657
	.value	0
	.value	14866
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4882
	.value	4
	.value	4754
	.value	4754
	.value	4754
	.value	0
	.value	8210
	.value	0
	.value	8082
	.value	8082
	.value	2601
	.value	4114
	.value	2810
	.value	4114
	.value	4114
	.value	2874
	.value	4114
	.value	4114
	.value	2938
	.value	3018
	.value	3098
	.value	4114
	.value	3162
	.value	4114
	.value	4114
	.value	4114
	.value	3226
	.value	3290
	.value	0
	.value	3354
	.value	4114
	.value	4114
	.value	3418
	.value	4114
	.value	4114
	.value	3482
	.value	4114
	.value	4114
	.value	-4847
	.value	-4719
	.value	-4719
	.value	-4719
	.value	3545
	.value	-4079
	.value	3753
	.value	-4079
	.value	-4079
	.value	3817
	.value	-4079
	.value	-4079
	.value	3881
	.value	3961
	.value	4041
	.value	-4079
	.value	4105
	.value	-4079
	.value	-4079
	.value	-4079
	.value	4169
	.value	4233
	.value	4297
	.value	4345
	.value	-4079
	.value	-4079
	.value	4409
	.value	-4079
	.value	-4079
	.value	4473
	.value	-4079
	.value	-4079
	.value	-8175
	.value	-8047
	.value	-8047
	.value	1042
	.value	4537
	.value	4585
	.value	2
	.value	2
	.value	2
	.value	4665
	.value	4713
	.value	-1007
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	4761
	.value	4809
	.value	913
	.value	-14799
	.value	4858
	.value	4937
	.value	0
	.value	146
	.value	-111
	.value	-878
	.value	146
	.value	-111
	.value	1
	.value	-16622
	.value	-16622
	.value	-16622
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	10258
	.value	4114
	.value	4114
	.value	4986
	.value	4114
	.value	5050
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	5114
	.value	4114
	.value	4114
	.value	5178
	.value	5242
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	5322
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	-4079
	.value	-4079
	.value	5385
	.value	-4079
	.value	5449
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	5513
	.value	-4079
	.value	-4079
	.value	5577
	.value	5641
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	5721
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-10223
	.value	-10223
	.value	-10223
	.value	-10223
	.value	-10223
	.value	-10223
	.value	-10191
	.value	-10223
	.value	-10191
	.value	-10223
	.value	-10223
	.value	-10223
	.value	-10223
	.value	-10223
	.value	-10223
	.value	-10223
	.value	146
	.value	-111
	.value	5786
	.value	5849
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	4
	.value	4
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	1938
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	-1903
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	0
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	1
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	5913
	.value	1
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	100
	.value	68
	.value	68
	.value	100
	.value	100
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	0
	.value	100
	.value	0
	.value	100
	.value	100
	.value	0
	.value	68
	.value	100
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	100
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	68
	.value	68
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	4
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	68
	.value	4
	.value	4
	.value	68
	.value	68
	.value	0
	.value	100
	.value	68
	.value	68
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	100
	.value	68
	.value	68
	.value	100
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	68
	.value	100
	.value	100
	.value	68
	.value	100
	.value	68
	.value	68
	.value	68
	.value	100
	.value	68
	.value	100
	.value	68
	.value	100
	.value	68
	.value	100
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	68
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	4
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	4
	.value	68
	.value	68
	.value	68
	.value	4
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	4
	.value	100
	.value	68
	.value	68
	.value	100
	.value	68
	.value	68
	.value	100
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	68
	.value	100
	.value	68
	.value	68
	.value	100
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	68
	.value	100
	.value	68
	.value	68
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	100
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	100
	.value	100
	.value	100
	.value	100
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	100
	.value	100
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	100
	.value	100
	.value	100
	.value	100
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	100
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	4
	.value	100
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	100
	.value	100
	.value	100
	.value	4
	.value	0
	.value	100
	.value	4
	.value	68
	.value	68
	.value	100
	.value	0
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	0
	.value	100
	.value	100
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	6042
	.value	0
	.value	6042
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	6042
	.value	0
	.value	0
	.value	6073
	.value	6121
	.value	6169
	.value	6217
	.value	6265
	.value	6313
	.value	6361
	.value	6409
	.value	6457
	.value	6505
	.value	6553
	.value	6601
	.value	6649
	.value	6697
	.value	6745
	.value	6793
	.value	6841
	.value	6889
	.value	6937
	.value	6985
	.value	7033
	.value	7081
	.value	7129
	.value	7177
	.value	7225
	.value	7273
	.value	7321
	.value	7369
	.value	7417
	.value	7465
	.value	7513
	.value	7561
	.value	7609
	.value	7657
	.value	7705
	.value	7753
	.value	7801
	.value	7849
	.value	7897
	.value	7945
	.value	7993
	.value	8041
	.value	8089
	.value	0
	.value	4
	.value	8137
	.value	8185
	.value	8233
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8314
	.value	8314
	.value	8314
	.value	8314
	.value	8314
	.value	8314
	.value	0
	.value	0
	.value	8345
	.value	8393
	.value	8441
	.value	8489
	.value	8537
	.value	8585
	.value	0
	.value	0
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	8282
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	4
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	68
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	100
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	100
	.value	0
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	100
	.value	4
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	96
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	96
	.value	100
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	96
	.value	96
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	8633
	.value	8681
	.value	8729
	.value	8777
	.value	8825
	.value	8905
	.value	8985
	.value	9033
	.value	9081
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	9130
	.value	0
	.value	0
	.value	9130
	.value	9130
	.value	9130
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	0
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	0
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	37
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	5
	.value	9161
	.value	1
	.value	1
	.value	1
	.value	9193
	.value	1
	.value	1
	.value	5
	.value	5
	.value	5
	.value	5
	.value	37
	.value	5
	.value	5
	.value	5
	.value	37
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	9225
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	33
	.value	1
	.value	1
	.value	1
	.value	1
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	0
	.value	68
	.value	100
	.value	100
	.value	68
	.value	100
	.value	68
	.value	68
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-79
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	9258
	.value	9321
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	9385
	.value	9513
	.value	9641
	.value	9769
	.value	9897
	.value	10025
	.value	1
	.value	1
	.value	10074
	.value	1
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-79
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	0
	.value	0
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	0
	.value	0
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	0
	.value	0
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	0
	.value	0
	.value	10153
	.value	1041
	.value	10281
	.value	1041
	.value	10457
	.value	1041
	.value	10633
	.value	1041
	.value	0
	.value	-1006
	.value	0
	.value	-1006
	.value	0
	.value	-1006
	.value	0
	.value	-1006
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	1041
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	-1006
	.value	9489
	.value	9489
	.value	11025
	.value	11025
	.value	11025
	.value	11025
	.value	12817
	.value	12817
	.value	16401
	.value	16401
	.value	14353
	.value	14353
	.value	16145
	.value	16145
	.value	0
	.value	0
	.value	10809
	.value	10921
	.value	11033
	.value	11145
	.value	11257
	.value	11369
	.value	11481
	.value	11593
	.value	11707
	.value	11819
	.value	11931
	.value	12043
	.value	12155
	.value	12267
	.value	12379
	.value	12491
	.value	12601
	.value	12713
	.value	12825
	.value	12937
	.value	13049
	.value	13161
	.value	13273
	.value	13385
	.value	13499
	.value	13611
	.value	13723
	.value	13835
	.value	13947
	.value	14059
	.value	14171
	.value	14283
	.value	14393
	.value	14505
	.value	14617
	.value	14729
	.value	14841
	.value	14953
	.value	15065
	.value	15177
	.value	15291
	.value	15403
	.value	15515
	.value	15627
	.value	15739
	.value	15851
	.value	15963
	.value	16075
	.value	1041
	.value	1041
	.value	16185
	.value	16313
	.value	16425
	.value	0
	.value	16553
	.value	16681
	.value	-1006
	.value	-1006
	.value	-9454
	.value	-9454
	.value	16859
	.value	4
	.value	16969
	.value	4
	.value	4
	.value	4
	.value	17049
	.value	17177
	.value	17289
	.value	0
	.value	17417
	.value	17545
	.value	-10990
	.value	-10990
	.value	-10990
	.value	-10990
	.value	17723
	.value	4
	.value	4
	.value	4
	.value	1041
	.value	1041
	.value	17833
	.value	18009
	.value	0
	.value	0
	.value	18217
	.value	18345
	.value	-1006
	.value	-1006
	.value	-12782
	.value	-12782
	.value	0
	.value	4
	.value	4
	.value	4
	.value	1041
	.value	1041
	.value	18521
	.value	18697
	.value	18905
	.value	913
	.value	19033
	.value	19161
	.value	-1006
	.value	-1006
	.value	-14318
	.value	-14318
	.value	-878
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	19337
	.value	19465
	.value	19577
	.value	0
	.value	19705
	.value	19833
	.value	-16366
	.value	-16366
	.value	-16110
	.value	-16110
	.value	20011
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	37
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	5
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	5
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	100
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	4
	.value	4
	.value	4
	.value	4
	.value	68
	.value	4
	.value	4
	.value	4
	.value	100
	.value	100
	.value	68
	.value	100
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	2
	.value	0
	.value	0
	.value	0
	.value	0
	.value	2
	.value	0
	.value	0
	.value	1
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	2
	.value	2
	.value	2
	.value	1
	.value	0
	.value	2
	.value	0
	.value	0
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	2
	.value	0
	.value	20122
	.value	0
	.value	2
	.value	0
	.value	20186
	.value	20250
	.value	2
	.value	2
	.value	0
	.value	1
	.value	2
	.value	2
	.value	3602
	.value	2
	.value	1
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1
	.value	0
	.value	0
	.value	1
	.value	1
	.value	2
	.value	2
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	2
	.value	1
	.value	1
	.value	33
	.value	33
	.value	0
	.value	0
	.value	0
	.value	0
	.value	-3567
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	2066
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	-2031
	.value	0
	.value	0
	.value	0
	.value	146
	.value	-111
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	3346
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	-3311
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	6162
	.value	0
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	-6127
	.value	0
	.value	146
	.value	-111
	.value	20314
	.value	20346
	.value	20378
	.value	20409
	.value	20441
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	20474
	.value	20506
	.value	20538
	.value	20570
	.value	1
	.value	146
	.value	-111
	.value	1
	.value	146
	.value	-111
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	37
	.value	5
	.value	20602
	.value	20602
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	1
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	68
	.value	68
	.value	68
	.value	146
	.value	-111
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	20633
	.value	0
	.value	20633
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	20633
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	100
	.value	100
	.value	96
	.value	96
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	20666
	.value	20729
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	0
	.value	68
	.value	4
	.value	4
	.value	4
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	4
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	5
	.value	5
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	1
	.value	1
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	5
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	20794
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	4
	.value	4
	.value	4
	.value	146
	.value	-111
	.value	20826
	.value	1
	.value	0
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	6161
	.value	1
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	20858
	.value	20890
	.value	20922
	.value	20954
	.value	20858
	.value	1
	.value	20986
	.value	21018
	.value	21050
	.value	21082
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	0
	.value	0
	.value	146
	.value	-111
	.value	-6126
	.value	21114
	.value	21146
	.value	146
	.value	-111
	.value	146
	.value	-111
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	146
	.value	-111
	.value	0
	.value	5
	.value	5
	.value	1
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	96
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	96
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	0
	.value	68
	.value	68
	.value	100
	.value	0
	.value	0
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	0
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	21177
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	4
	.value	5
	.value	5
	.value	5
	.value	5
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	21209
	.value	21257
	.value	21305
	.value	21353
	.value	21401
	.value	21449
	.value	21497
	.value	21545
	.value	21593
	.value	21641
	.value	21689
	.value	21737
	.value	21785
	.value	21833
	.value	21881
	.value	21929
	.value	23513
	.value	23561
	.value	23609
	.value	23657
	.value	23705
	.value	23753
	.value	23801
	.value	23849
	.value	23897
	.value	23945
	.value	23993
	.value	24041
	.value	24089
	.value	24137
	.value	24185
	.value	24233
	.value	24281
	.value	24329
	.value	24377
	.value	24425
	.value	24473
	.value	24521
	.value	24569
	.value	24617
	.value	24665
	.value	24713
	.value	24761
	.value	24809
	.value	24857
	.value	24905
	.value	24953
	.value	25001
	.value	21977
	.value	22025
	.value	22073
	.value	22121
	.value	22169
	.value	22217
	.value	22265
	.value	22313
	.value	22361
	.value	22409
	.value	22457
	.value	22505
	.value	22553
	.value	22601
	.value	22649
	.value	22697
	.value	22745
	.value	22793
	.value	22841
	.value	22889
	.value	22937
	.value	22985
	.value	23033
	.value	23081
	.value	23129
	.value	23177
	.value	23225
	.value	23273
	.value	23321
	.value	23369
	.value	23417
	.value	23465
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	25049
	.value	25177
	.value	25305
	.value	25433
	.value	25609
	.value	25785
	.value	25945
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	26105
	.value	26233
	.value	26361
	.value	26489
	.value	26617
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	4
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	5138
	.value	0
	.value	0
	.value	0
	.value	0
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	-5103
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	100
	.value	4
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	8210
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	-8175
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	68
	.value	68
	.value	68
	.value	100
	.value	68
	.value	100
	.value	100
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	100
	.value	100
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	96
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	100
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	96
	.value	100
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	96
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	4
	.value	4
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	96
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	4114
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	-4079
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	96
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	100
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	100
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	100
	.value	4
	.value	100
	.value	100
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	96
	.value	96
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	96
	.value	96
	.value	100
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	96
	.value	96
	.value	96
	.value	96
	.value	96
	.value	96
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	33
	.value	33
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	0
	.value	33
	.value	33
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	33
	.value	33
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	0
	.value	2
	.value	2
	.value	0
	.value	0
	.value	2
	.value	0
	.value	0
	.value	2
	.value	2
	.value	0
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	1
	.value	1
	.value	0
	.value	1
	.value	0
	.value	1
	.value	33
	.value	33
	.value	1
	.value	1
	.value	1
	.value	1
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	33
	.value	33
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	2
	.value	0
	.value	0
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	33
	.value	33
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	0
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	1
	.value	0
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	1
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	68
	.value	68
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	100
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	4370
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	-4335
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	68
	.value	100
	.value	4
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.value	4
	.zero	40
	.align 32
	.type	_ZL19ucase_props_indexes, @object
	.size	_ZL19ucase_props_indexes, 64
_ZL19ucase_props_indexes:
	.long	16
	.long	28874
	.long	24728
	.long	1671
	.long	370
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	3
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	73
	.long	5
	.long	3
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
