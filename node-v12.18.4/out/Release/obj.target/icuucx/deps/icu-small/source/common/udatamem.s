	.file	"udatamem.cpp"
	.text
	.p2align 4
	.globl	UDataMemory_init_67
	.type	UDataMemory_init_67, @function
UDataMemory_init_67:
.LFB2074:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rdi)
	movups	%xmm0, (%rdi)
	movl	$-1, 48(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE2074:
	.size	UDataMemory_init_67, .-UDataMemory_init_67
	.p2align 4
	.globl	UDatamemory_assign_67
	.type	UDatamemory_assign_67, @function
UDatamemory_assign_67:
.LFB2075:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm0
	movzbl	24(%rdi), %eax
	movups	%xmm0, (%rdi)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	32(%rsi), %xmm2
	movups	%xmm2, 32(%rdi)
	movq	48(%rsi), %rdx
	movb	%al, 24(%rdi)
	movq	%rdx, 48(%rdi)
	ret
	.cfi_endproc
.LFE2075:
	.size	UDatamemory_assign_67, .-UDatamemory_assign_67
	.p2align 4
	.globl	UDataMemory_createNewInstance_67
	.type	UDataMemory_createNewInstance_67, @function
UDataMemory_createNewInstance_67:
.LFB2076:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$56, %edi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L12
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rax)
	movups	%xmm0, 16(%rax)
	movl	$-1, 48(%rax)
	movb	$1, 24(%rax)
	movups	%xmm0, (%rax)
	movups	%xmm0, 32(%rax)
.L4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L12:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L4
	.cfi_endproc
.LFE2076:
	.size	UDataMemory_createNewInstance_67, .-UDataMemory_createNewInstance_67
	.p2align 4
	.globl	UDataMemory_normalizeDataPointer_67
	.type	UDataMemory_normalizeDataPointer_67, @function
UDataMemory_normalizeDataPointer_67:
.LFB2077:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testq	%rdi, %rdi
	je	.L15
	cmpw	$10202, 2(%rdi)
	je	.L13
	addq	$8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%eax, %eax
.L13:
	ret
	.cfi_endproc
.LFE2077:
	.size	UDataMemory_normalizeDataPointer_67, .-UDataMemory_normalizeDataPointer_67
	.p2align 4
	.globl	UDataMemory_setData_67
	.type	UDataMemory_setData_67, @function
UDataMemory_setData_67:
.LFB2078:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L18
	cmpw	$10202, 2(%rsi)
	je	.L18
	addq	$8, %rsi
.L18:
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE2078:
	.size	UDataMemory_setData_67, .-UDataMemory_setData_67
	.p2align 4
	.globl	udata_close_67
	.type	udata_close_67, @function
udata_close_67:
.LFB2079:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L29
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	uprv_unmapFile_67@PLT
	cmpb	$0, 24(%r12)
	je	.L24
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	$0, 48(%r12)
	pxor	%xmm0, %xmm0
	movl	$-1, 48(%r12)
	movups	%xmm0, (%r12)
	movups	%xmm0, 16(%r12)
	movups	%xmm0, 32(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2079:
	.size	udata_close_67, .-udata_close_67
	.p2align 4
	.globl	udata_getMemory_67
	.type	udata_getMemory_67, @function
udata_getMemory_67:
.LFB2080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	testq	%rdi, %rdi
	je	.L34
	movq	8(%rdi), %r12
	testq	%r12, %r12
	je	.L32
	movq	%r12, %rdi
	call	udata_getHeaderSize_67@PLT
	movzwl	%ax, %eax
	addq	%rax, %r12
.L32:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L32
	.cfi_endproc
.LFE2080:
	.size	udata_getMemory_67, .-udata_getMemory_67
	.p2align 4
	.globl	udata_getLength_67
	.type	udata_getLength_67, @function
udata_getLength_67:
.LFB2081:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L42
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L42
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	48(%rdi), %ebx
	testl	%ebx, %ebx
	js	.L43
	movq	%r8, %rdi
	call	udata_getHeaderSize_67@PLT
	movzwl	%ax, %eax
	subl	%eax, %ebx
	movl	%ebx, %eax
.L39:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L39
.L42:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2081:
	.size	udata_getLength_67, .-udata_getLength_67
	.p2align 4
	.globl	udata_getRawMemory_67
	.type	udata_getRawMemory_67, @function
udata_getRawMemory_67:
.LFB2082:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L50
	movq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2082:
	.size	udata_getRawMemory_67, .-udata_getRawMemory_67
	.p2align 4
	.globl	UDataMemory_isLoaded_67
	.type	UDataMemory_isLoaded_67, @function
UDataMemory_isLoaded_67:
.LFB2083:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE2083:
	.size	UDataMemory_isLoaded_67, .-UDataMemory_isLoaded_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
