	.file	"bytestrie.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrieD2Ev
	.type	_ZN6icu_679BytesTrieD2Ev, @function
_ZN6icu_679BytesTrieD2Ev:
.LFB2113:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2113:
	.size	_ZN6icu_679BytesTrieD2Ev, .-_ZN6icu_679BytesTrieD2Ev
	.globl	_ZN6icu_679BytesTrieD1Ev
	.set	_ZN6icu_679BytesTrieD1Ev,_ZN6icu_679BytesTrieD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie9readValueEPKhi
	.type	_ZN6icu_679BytesTrie9readValueEPKhi, @function
_ZN6icu_679BytesTrie9readValueEPKhi:
.LFB2115:
	.cfi_startproc
	endbr64
	cmpl	$80, %esi
	jg	.L4
	leal	-16(%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movzbl	(%rdi), %eax
	cmpl	$107, %esi
	jg	.L6
	subl	$81, %esi
	sall	$8, %esi
	orl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$125, %esi
	jle	.L9
	movzbl	1(%rdi), %edx
	movzbl	2(%rdi), %ecx
	cmpl	$126, %esi
	je	.L10
	movl	(%rdi), %esi
	movl	%esi, %eax
	bswap	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movzwl	(%rdi), %eax
	subl	$108, %esi
	sall	$16, %esi
	rolw	$8, %ax
	movzwl	%ax, %eax
	orl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	sall	$16, %eax
	sall	$8, %edx
	orl	%edx, %eax
	orl	%ecx, %eax
	ret
	.cfi_endproc
.LFE2115:
	.size	_ZN6icu_679BytesTrie9readValueEPKhi, .-_ZN6icu_679BytesTrie9readValueEPKhi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie11jumpByDeltaEPKh
	.type	_ZN6icu_679BytesTrie11jumpByDeltaEPKh, @function
_ZN6icu_679BytesTrie11jumpByDeltaEPKh:
.LFB2116:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %edx
	cmpl	$191, %edx
	jg	.L12
	addq	$1, %rdi
.L13:
	movslq	%edx, %rdx
	leaq	(%rdi,%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movzbl	1(%rdi), %eax
	cmpl	$239, %edx
	jg	.L14
	subl	$192, %edx
	addq	$2, %rdi
	sall	$8, %edx
	orl	%eax, %edx
	movslq	%edx, %rdx
	leaq	(%rdi,%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	$253, %edx
	jle	.L17
	movzbl	2(%rdi), %ecx
	movzbl	3(%rdi), %esi
	cmpl	$254, %edx
	je	.L18
	movl	1(%rdi), %edx
	addq	$5, %rdi
	bswap	%edx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L17:
	movzwl	1(%rdi), %ecx
	subl	$240, %edx
	addq	$3, %rdi
	movl	%edx, %eax
	rolw	$8, %cx
	sall	$16, %eax
	movzwl	%cx, %edx
	orl	%eax, %edx
	movslq	%edx, %rdx
	leaq	(%rdi,%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	sall	$16, %eax
	sall	$8, %ecx
	addq	$4, %rdi
	movl	%eax, %edx
	orl	%ecx, %edx
	orl	%esi, %edx
	jmp	.L13
	.cfi_endproc
.LFE2116:
	.size	_ZN6icu_679BytesTrie11jumpByDeltaEPKh, .-_ZN6icu_679BytesTrie11jumpByDeltaEPKh
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679BytesTrie7currentEv
	.type	_ZNK6icu_679BytesTrie7currentEv, @function
_ZNK6icu_679BytesTrie7currentEv:
.LFB2117:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L21
	movl	24(%rdi), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	js	.L24
.L19:
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movzbl	(%rdx), %edx
	cmpb	$31, %dl
	jbe	.L19
	andl	$1, %edx
	movl	$3, %eax
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2117:
	.size	_ZNK6icu_679BytesTrie7currentEv, .-_ZNK6icu_679BytesTrie7currentEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie10branchNextEPKhii
	.type	_ZN6icu_679BytesTrie10branchNextEPKhii, @function
_ZN6icu_679BytesTrie10branchNextEPKhii:
.LFB2118:
	.cfi_startproc
	endbr64
	movzbl	(%rsi), %r8d
	movzbl	1(%rsi), %r9d
	testl	%edx, %edx
	jne	.L26
	movzbl	%r8b, %edx
	addq	$1, %rsi
	movzbl	%r9b, %r8d
	movzbl	1(%rsi), %r9d
.L26:
	addl	$1, %edx
	cmpl	$5, %edx
	jg	.L28
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	$191, %eax
	jle	.L30
	movzbl	2(%rsi), %edx
	cmpl	$239, %eax
	jg	.L31
	subl	$192, %eax
	leaq	3(%rsi), %r10
	sall	$8, %eax
	orl	%edx, %eax
.L30:
	movslq	%eax, %rsi
	movl	%r11d, %edx
	addq	%r10, %rsi
.L34:
	movzbl	(%rsi), %r8d
	movzbl	1(%rsi), %r9d
	cmpl	$5, %edx
	jle	.L27
.L28:
	movl	%edx, %r11d
	leaq	2(%rsi), %r10
	movzbl	%r9b, %eax
	sarl	%r11d
	cmpl	%ecx, %r8d
	jg	.L67
	subl	%r11d, %edx
	cmpl	$191, %eax
	jle	.L61
	cmpl	$239, %eax
	jg	.L35
	addq	$3, %rsi
	movzbl	(%rsi), %r8d
	movzbl	1(%rsi), %r9d
	cmpl	$5, %edx
	jg	.L28
	.p2align 4,,10
	.p2align 3
.L27:
	movzbl	%r9b, %r11d
	cmpl	%ecx, %r8d
	je	.L37
	leal	-1(%rdx), %r8d
	leaq	2(%rsi), %r10
	cmpl	$161, %r11d
	jg	.L68
.L38:
	movzbl	(%r10), %eax
	cmpl	$1, %r8d
	jle	.L41
	movzbl	1(%r10), %r11d
	movl	%r11d, %r9d
	cmpl	%eax, %ecx
	je	.L62
	subl	$2, %edx
	leaq	2(%r10), %rsi
	cmpl	$161, %r11d
	jg	.L69
.L42:
	movzbl	(%rsi), %eax
	cmpl	$1, %edx
	je	.L63
	movzbl	1(%rsi), %r11d
	movl	%r11d, %r9d
	cmpl	%eax, %ecx
	je	.L37
	leaq	2(%rsi), %r8
	cmpl	$161, %r11d
	jg	.L70
.L45:
	movzbl	(%r8), %eax
	cmpl	$2, %edx
	je	.L64
	movzbl	1(%r8), %r11d
	movq	%r8, %rsi
	movl	%r11d, %r9d
	cmpl	%ecx, %eax
	je	.L37
	leaq	2(%r8), %r10
	cmpl	$161, %r11d
	jg	.L71
.L57:
	movzbl	(%r10), %eax
.L41:
	cmpl	%eax, %ecx
	jne	.L60
	leaq	1(%r10), %rax
	movq	%rax, 16(%rdi)
	movzbl	1(%r10), %edx
	movl	$1, %eax
	cmpb	$31, %dl
	jbe	.L25
	andl	$1, %edx
	movl	$3, %eax
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r10, %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$4, %rsi
	cmpl	$253, %eax
	jle	.L34
	andl	$1, %r9d
	leal	3(%r9), %esi
	movslq	%esi, %rsi
	addq	%r10, %rsi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L31:
	cmpl	$253, %eax
	jg	.L32
	movzwl	2(%rsi), %edx
	subl	$240, %eax
	leaq	4(%rsi), %r10
	sall	$16, %eax
	rolw	$8, %dx
	movzwl	%dx, %edx
	orl	%edx, %eax
	jmp	.L30
.L62:
	movq	%r10, %rsi
.L37:
	andl	$1, %r9d
	je	.L49
	addq	$1, %rsi
	movl	$2, %eax
.L50:
	movq	%rsi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movzbl	3(%rsi), %r8d
	movzbl	4(%rsi), %r9d
	cmpl	$254, %eax
	je	.L72
	movl	2(%rsi), %eax
	leaq	6(%rsi), %r10
	bswap	%eax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L68:
	cmpl	$215, %r11d
	jle	.L39
	cmpl	$251, %r11d
	jg	.L73
	leaq	4(%rsi), %r10
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L69:
	cmpl	$215, %r11d
	jle	.L43
	cmpl	$251, %r11d
	jg	.L74
	leaq	4(%r10), %rsi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L70:
	cmpl	$215, %r11d
	jle	.L46
	cmpl	$251, %r11d
	jle	.L47
	movl	%r11d, %eax
	sarl	%eax
	andl	$1, %eax
	addl	$3, %eax
	cltq
	addq	%rax, %r8
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L71:
	cmpl	$215, %r11d
	jle	.L75
	cmpl	$251, %r11d
	jg	.L59
	leaq	4(%r8), %r10
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L49:
	movl	%r11d, %eax
	sarl	%eax
	cmpl	$161, %r11d
	jbe	.L76
	movzbl	2(%rsi), %ecx
	cmpl	$107, %eax
	jg	.L53
	subl	$81, %eax
	addq	$3, %rsi
	sall	$8, %eax
	orl	%ecx, %eax
.L52:
	cltq
	addq	%rax, %rsi
	movl	$1, %eax
	movzbl	(%rsi), %edx
	cmpb	$31, %dl
	jbe	.L50
	andl	$1, %edx
	movl	$3, %eax
	movq	%rsi, 16(%rdi)
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	movq	$0, 16(%rdi)
	xorl	%eax, %eax
.L25:
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	3(%rsi), %r10
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	3(%r10), %rsi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	3(%rsi), %r8
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L72:
	movl	%edx, %eax
	sall	$8, %r8d
	leaq	5(%rsi), %r10
	sall	$16, %eax
	orl	%r8d, %eax
	orl	%r9d, %eax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	3(%r8), %r10
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L76:
	addq	$2, %rsi
	subl	$16, %eax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L73:
	movl	%r11d, %eax
	sarl	%eax
	andl	$1, %eax
	addl	$3, %eax
	cltq
	addq	%rax, %r10
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L74:
	movl	%r11d, %eax
	sarl	%eax
	andl	$1, %eax
	addl	$3, %eax
	cltq
	addq	%rax, %rsi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	4(%rsi), %r8
	jmp	.L45
.L59:
	movl	%r11d, %eax
	sarl	%eax
	andl	$1, %eax
	addl	$3, %eax
	cltq
	addq	%rax, %r10
	jmp	.L57
.L53:
	cmpl	$125, %eax
	jle	.L77
	movzbl	3(%rsi), %r8d
	movzbl	4(%rsi), %edx
	cmpl	$126, %eax
	je	.L78
	movl	2(%rsi), %eax
	addq	$6, %rsi
	bswap	%eax
	jmp	.L52
.L77:
	movzwl	2(%rsi), %edx
	subl	$108, %eax
	addq	$4, %rsi
	sall	$16, %eax
	rolw	$8, %dx
	movzwl	%dx, %edx
	orl	%edx, %eax
	jmp	.L52
.L63:
	movq	%rsi, %r10
	jmp	.L41
.L64:
	movq	%r8, %r10
	jmp	.L41
.L78:
	movl	%ecx, %eax
	sall	$8, %r8d
	addq	$5, %rsi
	sall	$16, %eax
	orl	%r8d, %eax
	orl	%edx, %eax
	jmp	.L52
	.cfi_endproc
.LFE2118:
	.size	_ZN6icu_679BytesTrie10branchNextEPKhii, .-_ZN6icu_679BytesTrie10branchNextEPKhii
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie8nextImplEPKhi
	.type	_ZN6icu_679BytesTrie8nextImplEPKhi, @function
_ZN6icu_679BytesTrie8nextImplEPKhi:
.LFB2119:
	.cfi_startproc
	endbr64
	movl	%edx, %ecx
	.p2align 4,,10
	.p2align 3
.L85:
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %eax
	cmpl	$15, %edx
	jle	.L92
.L81:
	cmpl	$31, %edx
	jle	.L93
	testb	$1, %al
	jne	.L83
	cmpl	$161, %edx
	jle	.L91
	cmpl	$215, %edx
	jg	.L86
	addq	$2, %rsi
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %eax
	cmpl	$15, %edx
	jg	.L81
.L92:
	movq	%r8, %rsi
	jmp	_ZN6icu_679BytesTrie10branchNextEPKhii
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r8, %rsi
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L86:
	addq	$3, %rsi
	cmpl	$251, %edx
	jle	.L85
	sarl	%edx
	andl	$1, %edx
	leal	3(%rdx), %esi
	movslq	%esi, %rsi
	addq	%r8, %rsi
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L93:
	movzbl	1(%rsi), %eax
	cmpl	%ecx, %eax
	je	.L94
.L83:
	movq	$0, 16(%rdi)
	xorl	%eax, %eax
.L79:
	ret
.L94:
	leaq	2(%rsi), %rax
	subl	$17, %edx
	movq	%rax, 16(%rdi)
	movl	$1, %eax
	movl	%edx, 24(%rdi)
	cmpl	$-1, %edx
	jne	.L79
	movzbl	2(%rsi), %edx
	cmpb	$31, %dl
	jbe	.L79
	andl	$1, %edx
	movl	$3, %eax
	subl	%edx, %eax
	ret
	.cfi_endproc
.LFE2119:
	.size	_ZN6icu_679BytesTrie8nextImplEPKhi, .-_ZN6icu_679BytesTrie8nextImplEPKhi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie4nextEi
	.type	_ZN6icu_679BytesTrie4nextEi, @function
_ZN6icu_679BytesTrie4nextEi:
.LFB2120:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	movq	16(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L100
	leal	256(%rdx), %eax
	testl	%edx, %edx
	cmovs	%eax, %edx
	movl	24(%rdi), %eax
	testl	%eax, %eax
	js	.L98
	movzbl	(%rsi), %ecx
	cmpl	%edx, %ecx
	je	.L103
	movq	$0, 16(%rdi)
	xorl	%r8d, %r8d
.L95:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	subl	$1, %eax
	leaq	1(%rsi), %rdx
	movl	$1, %r8d
	movl	%eax, 24(%rdi)
	movq	%rdx, 16(%rdi)
	cmpl	$-1, %eax
	jne	.L95
	movzbl	1(%rsi), %eax
	cmpb	$31, %al
	jbe	.L95
	andl	$1, %eax
	movl	$3, %r8d
	subl	%eax, %r8d
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L98:
	jmp	_ZN6icu_679BytesTrie8nextImplEPKhi
	.p2align 4,,10
	.p2align 3
.L100:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2120:
	.size	_ZN6icu_679BytesTrie4nextEi, .-_ZN6icu_679BytesTrie4nextEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie4nextEPKci
	.type	_ZN6icu_679BytesTrie4nextEPKci, @function
_ZN6icu_679BytesTrie4nextEPKci:
.LFB2121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	testl	%edx, %edx
	js	.L180
	sete	%dl
.L106:
	movq	16(%rdi), %rax
	testb	%dl, %dl
	je	.L107
	testq	%rax, %rax
	je	.L108
	movl	24(%rdi), %edx
	testl	%edx, %edx
	js	.L177
.L111:
	movl	$1, %eax
.L104:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L108
	movl	24(%rdi), %edx
.L132:
	testl	%r12d, %r12d
	js	.L181
	je	.L120
	movsbl	(%rsi), %ecx
	leaq	1(%rsi), %rbx
	leal	-1(%r12), %esi
	testl	%edx, %edx
	js	.L182
	movl	%r12d, %r8d
	leal	-2(%r12), %r12d
	subl	%edx, %r12d
	subl	%r8d, %edx
	movl	%edx, %r8d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L183:
	addq	$1, %rax
	leal	(%r8,%rsi), %edx
	testl	%esi, %esi
	je	.L120
	subl	$1, %esi
	addq	$1, %rbx
	movsbl	-1(%rbx), %ecx
	cmpl	%r12d, %esi
	je	.L121
.L123:
	movzbl	(%rax), %edx
	cmpl	%ecx, %edx
	je	.L183
.L125:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	movq	$0, 16(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	cmpb	$0, (%rsi)
	sete	%dl
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L120:
	movl	%edx, 24(%rdi)
	movq	%rax, 16(%rdi)
	testl	%edx, %edx
	jns	.L111
.L177:
	movzbl	(%rax), %eax
	cmpb	$31, %al
	jbe	.L111
	movl	%eax, %edx
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	andl	$1, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movsbl	(%rsi), %ecx
	leaq	1(%rsi), %r8
	testl	%ecx, %ecx
	je	.L120
	movslq	%edx, %r9
	leaq	2(%rsi,%r9), %rbx
	leal	(%rdx,%rax), %esi
	testl	%edx, %edx
	jns	.L116
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L118:
	movsbl	(%r8), %ecx
	addq	$1, %rax
	movl	%esi, %edx
	addq	$1, %r8
	subl	%eax, %edx
	testl	%ecx, %ecx
	je	.L120
	cmpq	%rbx, %r8
	je	.L121
.L116:
	movzbl	(%rax), %edx
	cmpl	%ecx, %edx
	je	.L118
	jmp	.L125
.L182:
	movl	%esi, %r12d
	.p2align 4,,10
	.p2align 3
.L121:
	movl	%edx, 24(%rdi)
	.p2align 4,,10
	.p2align 3
.L117:
	movzbl	(%rax), %edx
	leaq	1(%rax), %r8
	movl	%edx, %esi
	cmpl	$15, %edx
	jle	.L185
.L127:
	cmpl	$31, %edx
	jle	.L186
	andl	$1, %esi
	jne	.L125
	cmpl	$161, %edx
	jle	.L138
	cmpl	$215, %edx
	jg	.L133
	movzbl	2(%rax), %edx
	addq	$2, %rax
	leaq	1(%rax), %r8
	movl	%edx, %esi
	cmpl	$15, %edx
	jg	.L127
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r8, %rsi
	call	_ZN6icu_679BytesTrie10branchNextEPKhii
	testl	%eax, %eax
	je	.L108
	testl	%r12d, %r12d
	js	.L187
	je	.L104
	movsbl	(%rbx), %ecx
	subl	$1, %r12d
	addq	$1, %rbx
.L129:
	cmpl	$2, %eax
	je	.L125
	movq	16(%rdi), %rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r8, %rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L133:
	addq	$3, %rax
	cmpl	$251, %edx
	jle	.L117
	movl	%edx, %eax
	sarl	%eax
	andl	$1, %eax
	addl	$3, %eax
	cltq
	addq	%r8, %rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L187:
	movsbl	(%rbx), %ecx
	leaq	1(%rbx), %rdx
	testl	%ecx, %ecx
	je	.L104
	movq	%rdx, %rbx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L186:
	movzbl	1(%rax), %esi
	cmpl	%ecx, %esi
	jne	.L125
	addq	$2, %rax
	subl	$17, %edx
	movq	%rbx, %rsi
	jmp	.L132
.L184:
	movq	%r8, %rbx
	jmp	.L121
	.cfi_endproc
.LFE2121:
	.size	_ZN6icu_679BytesTrie4nextEPKci, .-_ZN6icu_679BytesTrie4nextEPKci
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie15findUniqueValueEPKhaRi
	.type	_ZN6icu_679BytesTrie15findUniqueValueEPKhaRi, @function
_ZN6icu_679BytesTrie15findUniqueValueEPKhaRi:
.LFB2123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	movsbl	%sil, %edx
	.p2align 4,,10
	.p2align 3
.L189:
	movzbl	(%rdi), %esi
	leaq	1(%rdi), %r8
	movl	%esi, %eax
	cmpl	$15, %esi
	jg	.L190
.L210:
	testl	%esi, %esi
	jne	.L191
	movzbl	1(%rdi), %esi
	leaq	2(%rdi), %r8
.L191:
	movq	%r8, %rdi
	addl	$1, %esi
	movq	%rbx, %rcx
	call	_ZN6icu_679BytesTrie25findUniqueValueFromBranchEPKhiaRi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L192
	movzbl	(%rdi), %esi
	movl	$1, %edx
	leaq	1(%rdi), %r8
	movl	%esi, %eax
	cmpl	$15, %esi
	jle	.L210
.L190:
	cmpl	$31, %esi
	jg	.L194
	subl	$16, %esi
	movslq	%esi, %rsi
	leaq	1(%r8,%rsi), %rdi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L194:
	movl	%esi, %r10d
	andl	$1, %eax
	sarl	%r10d
	leal	-16(%r10), %ecx
	cmpl	$161, %esi
	jbe	.L196
	movzbl	1(%rdi), %r11d
	cmpl	$107, %r10d
	jg	.L197
	leal	-81(%r10), %ecx
	sall	$8, %ecx
	orl	%r11d, %ecx
.L196:
	testb	%dl, %dl
	je	.L200
	cmpl	%ecx, (%rbx)
	jne	.L192
	testb	%al, %al
	jne	.L188
.L211:
	cmpl	$161, %esi
	jle	.L205
	cmpl	$215, %esi
	jg	.L203
	addq	$2, %rdi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L200:
	movl	%ecx, (%rbx)
	movl	$1, %edx
	testb	%al, %al
	je	.L211
.L188:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	%r8, %rdi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L197:
	cmpl	$125, %r10d
	jg	.L198
	movzwl	1(%rdi), %r9d
	leal	-108(%r10), %ecx
	sall	$16, %ecx
	rolw	$8, %r9w
	movzwl	%r9w, %r9d
	orl	%r9d, %ecx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L203:
	addq	$3, %rdi
	cmpl	$251, %esi
	jle	.L189
	andl	$1, %r10d
	leal	3(%r10), %eax
	cltq
	leaq	(%r8,%rax), %rdi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L198:
	movzbl	2(%rdi), %r12d
	movzbl	3(%rdi), %r9d
	cmpl	$126, %r10d
	je	.L212
	movl	1(%rdi), %ecx
	bswap	%ecx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L192:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movl	%r11d, %ecx
	sall	$8, %r12d
	sall	$16, %ecx
	orl	%r12d, %ecx
	orl	%r9d, %ecx
	jmp	.L196
	.cfi_endproc
.LFE2123:
	.size	_ZN6icu_679BytesTrie15findUniqueValueEPKhaRi, .-_ZN6icu_679BytesTrie15findUniqueValueEPKhaRi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie25findUniqueValueFromBranchEPKhiaRi
	.type	_ZN6icu_679BytesTrie25findUniqueValueFromBranchEPKhiaRi, @function
_ZN6icu_679BytesTrie25findUniqueValueFromBranchEPKhiaRi:
.LFB2122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	%edx, -56(%rbp)
	movb	%dl, -49(%rbp)
	cmpl	$5, %esi
	jle	.L214
	movsbl	%dl, %r14d
.L215:
	leaq	1(%r13), %rdi
	movl	%r15d, %ebx
	call	_ZN6icu_679BytesTrie11jumpByDeltaEPKh
	sarl	%ebx
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%rax, %rdi
	movl	%ebx, %esi
	call	_ZN6icu_679BytesTrie25findUniqueValueFromBranchEPKhiaRi
	testq	%rax, %rax
	je	.L229
	movzbl	1(%r13), %edx
	subl	%ebx, %r15d
	leaq	2(%r13), %rcx
	movl	%edx, %eax
	cmpl	$191, %edx
	jle	.L259
	cmpl	$239, %edx
	jg	.L219
	addq	$3, %r13
.L218:
	cmpl	$5, %r15d
	jg	.L215
.L214:
	movzbl	1(%r13), %eax
	leaq	2(%r13), %rbx
	movl	%eax, %ecx
	sarl	%eax
	andl	$1, %ecx
	leal	-16(%rax), %edx
	cmpl	$80, %eax
	jg	.L278
.L226:
	testb	%cl, %cl
	je	.L279
.L228:
	cmpb	$0, -56(%rbp)
	jne	.L231
	movl	%edx, (%r12)
	movb	%cl, -49(%rbp)
.L230:
	leal	-1(%r15), %eax
	cmpl	$1, %eax
	jle	.L232
	movzbl	1(%rbx), %eax
	leaq	2(%rbx), %r13
	movl	%eax, %esi
	sarl	%eax
	andl	$1, %esi
	leal	-16(%rax), %edx
	cmpl	$80, %eax
	jle	.L238
	movzbl	2(%rbx), %edi
	cmpl	$107, %eax
	jle	.L234
	cmpl	$125, %eax
	jle	.L235
	movzbl	3(%rbx), %r8d
	movzbl	4(%rbx), %ecx
	cmpl	$126, %eax
	je	.L280
	movl	2(%rbx), %edx
	bswap	%edx
.L239:
	andl	$1, %eax
	addl	$3, %eax
	cltq
	addq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L238:
	testb	%sil, %sil
	je	.L281
	cmpl	(%r12), %edx
	jne	.L229
.L241:
	cmpl	$3, %r15d
	je	.L260
	movzbl	1(%r13), %eax
	leaq	2(%r13), %r14
	movl	%eax, %esi
	sarl	%eax
	andl	$1, %esi
	leal	-16(%rax), %edx
	cmpl	$80, %eax
	jle	.L247
	movzbl	2(%r13), %edi
	cmpl	$107, %eax
	jle	.L243
	cmpl	$125, %eax
	jle	.L244
	movzbl	3(%r13), %r8d
	movzbl	4(%r13), %ecx
	cmpl	$126, %eax
	je	.L282
	movl	2(%r13), %edx
	bswap	%edx
.L248:
	andl	$1, %eax
	addl	$3, %eax
	cltq
	addq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L247:
	testb	%sil, %sil
	je	.L283
	cmpl	(%r12), %edx
	jne	.L229
.L250:
	cmpl	$5, %r15d
	jne	.L261
	movzbl	1(%r14), %eax
	leaq	2(%r14), %rbx
	movl	%eax, %ecx
	movl	%eax, %esi
	andl	$1, %ecx
	sarl	%esi
	cmpl	$161, %eax
	ja	.L251
	leal	-16(%rsi), %eax
.L252:
	testb	%cl, %cl
	je	.L257
.L285:
	cmpl	%eax, (%r12)
	jne	.L229
.L232:
	addq	$24, %rsp
	leaq	1(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movq	%rcx, %r13
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L219:
	addq	$4, %r13
	cmpl	$253, %edx
	jle	.L218
	andl	$1, %eax
	leal	3(%rax), %r8d
	movslq	%r8d, %r8
	leaq	(%rcx,%r8), %r13
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L231:
	cmpl	%edx, (%r12)
	je	.L230
.L229:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movzbl	2(%r13), %edi
	cmpl	$107, %eax
	jle	.L222
	cmpl	$125, %eax
	jle	.L223
	movzbl	3(%r13), %esi
	movzbl	4(%r13), %r10d
	cmpl	$126, %eax
	je	.L284
	movl	2(%r13), %edx
	bswap	%edx
.L227:
	andl	$1, %eax
	addl	$3, %eax
	cltq
	addq	%rax, %rbx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L279:
	movslq	%edx, %rdx
	movsbl	-56(%rbp), %esi
	leaq	(%rbx,%rdx), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_679BytesTrie15findUniqueValueEPKhaRi
	testb	%al, %al
	je	.L229
	movb	$1, -49(%rbp)
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L222:
	leal	-81(%rax), %edx
	leaq	3(%r13), %rbx
	sall	$8, %edx
	orl	%edi, %edx
	testb	%cl, %cl
	jne	.L228
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L251:
	movzbl	2(%r14), %edi
	cmpl	$107, %esi
	jg	.L253
	leal	-81(%rsi), %eax
	leaq	3(%r14), %rbx
	sall	$8, %eax
	orl	%edi, %eax
	testb	%cl, %cl
	jne	.L285
.L257:
	cltq
	movsbl	-49(%rbp), %esi
	movq	%r12, %rdx
	leaq	(%rbx,%rax), %rdi
	call	_ZN6icu_679BytesTrie15findUniqueValueEPKhaRi
	testb	%al, %al
	jne	.L232
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L281:
	movslq	%edx, %rdx
	movsbl	-49(%rbp), %esi
	leaq	0(%r13,%rdx), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_679BytesTrie15findUniqueValueEPKhaRi
	testb	%al, %al
	je	.L229
	movb	$1, -49(%rbp)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L283:
	movslq	%edx, %rdx
	movsbl	-49(%rbp), %esi
	leaq	(%r14,%rdx), %rdi
	movq	%r12, %rdx
	call	_ZN6icu_679BytesTrie15findUniqueValueEPKhaRi
	testb	%al, %al
	je	.L229
	movb	$1, -49(%rbp)
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L234:
	leal	-81(%rax), %edx
	leaq	3(%rbx), %r13
	sall	$8, %edx
	orl	%edi, %edx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L243:
	leal	-81(%rax), %edx
	leaq	3(%r13), %r14
	sall	$8, %edx
	orl	%edi, %edx
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L253:
	cmpl	$125, %esi
	jg	.L254
	movzwl	2(%r14), %eax
	subl	$108, %esi
	leaq	4(%r14), %rbx
	sall	$16, %esi
	rolw	$8, %ax
	movzwl	%ax, %eax
	orl	%esi, %eax
	jmp	.L252
.L223:
	movzwl	2(%r13), %edx
	subl	$108, %eax
	leaq	4(%r13), %rbx
	sall	$16, %eax
	rolw	$8, %dx
	movzwl	%dx, %edx
	orl	%eax, %edx
	jmp	.L226
.L235:
	movzwl	2(%rbx), %edx
	subl	$108, %eax
	leaq	4(%rbx), %r13
	sall	$16, %eax
	rolw	$8, %dx
	movzwl	%dx, %edx
	orl	%eax, %edx
	jmp	.L238
.L244:
	leal	-108(%rax), %edx
	movzwl	2(%r13), %eax
	leaq	4(%r13), %r14
	sall	$16, %edx
	rolw	$8, %ax
	movzwl	%ax, %eax
	orl	%eax, %edx
	jmp	.L247
.L254:
	movzbl	3(%r14), %r8d
	movzbl	4(%r14), %edx
	cmpl	$126, %esi
	je	.L286
	movl	2(%r14), %eax
	bswap	%eax
.L256:
	andl	$1, %esi
	leal	3(%rsi), %edx
	movslq	%edx, %rdx
	addq	%rdx, %rbx
	jmp	.L252
.L284:
	movl	%edi, %edx
	sall	$8, %esi
	sall	$16, %edx
	orl	%esi, %edx
	orl	%r10d, %edx
	jmp	.L227
.L280:
	movl	%edi, %edx
	sall	$8, %r8d
	sall	$16, %edx
	orl	%r8d, %edx
	orl	%ecx, %edx
	jmp	.L239
.L282:
	movl	%edi, %edx
	sall	$8, %r8d
	sall	$16, %edx
	orl	%r8d, %edx
	orl	%ecx, %edx
	jmp	.L248
.L286:
	movl	%edi, %eax
	sall	$8, %r8d
	sall	$16, %eax
	orl	%r8d, %eax
	orl	%edx, %eax
	jmp	.L256
.L260:
	movq	%r13, %rbx
	jmp	.L232
.L261:
	movq	%r14, %rbx
	jmp	.L232
	.cfi_endproc
.LFE2122:
	.size	_ZN6icu_679BytesTrie25findUniqueValueFromBranchEPKhiaRi, .-_ZN6icu_679BytesTrie25findUniqueValueFromBranchEPKhiaRi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie18getNextBranchBytesEPKhiRNS_8ByteSinkE
	.type	_ZN6icu_679BytesTrie18getNextBranchBytesEPKhiRNS_8ByteSinkE, @function
_ZN6icu_679BytesTrie18getNextBranchBytesEPKhiRNS_8ByteSinkE:
.LFB2125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$5, %esi
	jle	.L288
.L289:
	movzbl	1(%rbx), %eax
	movl	%r12d, %r15d
	leaq	2(%rbx), %r14
	sarl	%r15d
	movq	%r14, %rdi
	cmpl	$191, %eax
	jle	.L290
	movzbl	2(%rbx), %edx
	cmpl	$239, %eax
	jg	.L291
	subl	$192, %eax
	leaq	3(%rbx), %rdi
	sall	$8, %eax
	orl	%edx, %eax
.L290:
	cltq
	movq	%r13, %rdx
	movl	%r15d, %esi
	subl	%r15d, %r12d
	addq	%rax, %rdi
	call	_ZN6icu_679BytesTrie18getNextBranchBytesEPKhiRNS_8ByteSinkE
	movzbl	1(%rbx), %edx
	movl	%edx, %eax
	cmpl	$191, %edx
	jle	.L303
	cmpl	$239, %edx
	jg	.L295
	addq	$3, %rbx
.L294:
	cmpl	$5, %r12d
	jg	.L289
.L288:
	leaq	-57(%rbp), %r14
.L300:
	movzbl	(%rbx), %eax
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%al, -57(%rbp)
	movq	0(%r13), %rax
	call	*16(%rax)
	movzbl	1(%rbx), %eax
	leaq	2(%rbx), %rdx
	cmpl	$161, %eax
	jle	.L304
	cmpl	$215, %eax
	jg	.L298
	addq	$3, %rbx
.L297:
	subl	$1, %r12d
	cmpl	$1, %r12d
	jg	.L300
	movzbl	(%rbx), %eax
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	%al, -57(%rbp)
	movq	0(%r13), %rax
	call	*16(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L307
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movq	%rdx, %rbx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L303:
	movq	%r14, %rbx
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L298:
	addq	$4, %rbx
	cmpl	$251, %eax
	jle	.L297
	sarl	%eax
	movl	%eax, %ebx
	andl	$1, %ebx
	addl	$3, %ebx
	movslq	%ebx, %rbx
	addq	%rdx, %rbx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L295:
	addq	$4, %rbx
	cmpl	$253, %edx
	jle	.L294
	andl	$1, %eax
	leal	3(%rax), %ebx
	movslq	%ebx, %rbx
	addq	%r14, %rbx
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L291:
	cmpl	$253, %eax
	jle	.L308
	movzbl	3(%rbx), %ecx
	movzbl	4(%rbx), %esi
	cmpl	$254, %eax
	je	.L309
	movl	2(%rbx), %eax
	leaq	6(%rbx), %rdi
	bswap	%eax
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L308:
	movzwl	2(%rbx), %edx
	subl	$240, %eax
	sall	$16, %eax
	rolw	$8, %dx
	movl	%eax, %edi
	movzwl	%dx, %eax
	orl	%edi, %eax
	leaq	4(%rbx), %rdi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L309:
	movl	%edx, %eax
	sall	$8, %ecx
	leaq	5(%rbx), %rdi
	sall	$16, %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	jmp	.L290
.L307:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2125:
	.size	_ZN6icu_679BytesTrie18getNextBranchBytesEPKhiRNS_8ByteSinkE, .-_ZN6icu_679BytesTrie18getNextBranchBytesEPKhiRNS_8ByteSinkE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679BytesTrie12getNextBytesERNS_8ByteSinkE
	.type	_ZNK6icu_679BytesTrie12getNextBytesERNS_8ByteSinkE, @function
_ZNK6icu_679BytesTrie12getNextBytesERNS_8ByteSinkE:
.LFB2124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L320
	movl	24(%rdi), %edx
	movzbl	(%rax), %ecx
	movq	%rsi, %r8
	testl	%edx, %edx
	jns	.L324
	movzbl	%cl, %edx
	leaq	1(%rax), %rdi
	cmpl	$31, %edx
	jle	.L313
	xorl	%r12d, %r12d
	andl	$1, %ecx
	jne	.L310
	cmpl	$161, %edx
	jle	.L314
	cmpl	$215, %edx
	jle	.L325
	cmpl	$251, %edx
	jg	.L316
	leaq	3(%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L314:
	movzbl	(%rdi), %edx
	addq	$1, %rdi
.L313:
	cmpl	$15, %edx
	jg	.L317
	testl	%edx, %edx
	jne	.L318
	movzbl	(%rdi), %edx
	addq	$1, %rdi
.L318:
	leal	1(%rdx), %r12d
	movq	%r8, %rdx
	movl	%r12d, %esi
	call	_ZN6icu_679BytesTrie18getNextBranchBytesEPKhiRNS_8ByteSinkE
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L324:
	movb	%cl, -25(%rbp)
.L323:
	movq	(%r8), %rax
	leaq	-25(%rbp), %rsi
	movl	$1, %edx
	movq	%r8, %rdi
	movl	$1, %r12d
	call	*16(%rax)
.L310:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L326
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	movzbl	(%rdi), %eax
	movb	%al, -25(%rbp)
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L320:
	xorl	%r12d, %r12d
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L325:
	leaq	2(%rax), %rdi
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L316:
	sarl	%edx
	andl	$1, %edx
	leal	3(%rdx), %eax
	cltq
	addq	%rax, %rdi
	jmp	.L314
.L326:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2124:
	.size	_ZNK6icu_679BytesTrie12getNextBytesERNS_8ByteSinkE, .-_ZNK6icu_679BytesTrie12getNextBytesERNS_8ByteSinkE
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie6appendERNS_8ByteSinkEi
	.type	_ZN6icu_679BytesTrie6appendERNS_8ByteSinkEi, @function
_ZN6icu_679BytesTrie6appendERNS_8ByteSinkEi:
.LFB2126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movb	%sil, -9(%rbp)
	leaq	-9(%rbp), %rsi
	call	*16(%rax)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L330
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L330:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2126:
	.size	_ZN6icu_679BytesTrie6appendERNS_8ByteSinkEi, .-_ZN6icu_679BytesTrie6appendERNS_8ByteSinkEi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
