	.file	"ustrcase.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_125utf16_caseContextIteratorEPva, @function
_ZN6icu_6712_GLOBAL__N_125utf16_caseContextIteratorEPva:
.LFB3133:
	.cfi_startproc
	endbr64
	testb	%sil, %sil
	js	.L22
	jne	.L23
	cmpb	$0, 28(%rdi)
	movl	12(%rdi), %eax
	js	.L3
.L5:
	movl	16(%rdi), %ecx
	cmpl	%eax, %ecx
	jle	.L8
	movq	(%rdi), %r9
	movslq	%eax, %rdx
	leal	1(%rax), %esi
	leaq	(%rdx,%rdx), %r10
	movl	%esi, 12(%rdi)
	movzwl	(%r9,%rdx,2), %r8d
	movl	%r8d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1
	cmpl	%esi, %ecx
	jne	.L24
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	24(%rdi), %eax
	movb	%sil, 28(%rdi)
	movl	%eax, 12(%rdi)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L22:
	movl	20(%rdi), %eax
	movb	%sil, 28(%rdi)
	movl	%eax, 12(%rdi)
.L3:
	movl	8(%rdi), %esi
	cmpl	%eax, %esi
	jge	.L8
	leal	-1(%rax), %edx
	movq	(%rdi), %r9
	movslq	%edx, %rcx
	movl	%edx, 12(%rdi)
	movzwl	(%r9,%rcx,2), %r8d
	leaq	(%rcx,%rcx), %r10
	movl	%r8d, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L1
	cmpl	%edx, %esi
	jge	.L1
	movzwl	-2(%r9,%r10), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L1
	subl	$2, %eax
	sall	$10, %edx
	movl	%eax, 12(%rdi)
	leal	-56613888(%r8,%rdx), %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	movzwl	2(%r9,%r10), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L1
	addl	$2, %eax
	sall	$10, %r8d
	movl	%eax, 12(%rdi)
	leal	-56613888(%rdx,%r8), %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$-1, %r8d
	jmp	.L1
	.cfi_endproc
.LFE3133:
	.size	_ZN6icu_6712_GLOBAL__N_125utf16_caseContextIteratorEPva, .-_ZN6icu_6712_GLOBAL__N_125utf16_caseContextIteratorEPva
	.p2align 4
	.type	_ZL8_cmpFoldPKDsiS0_ijPiS1_P10UErrorCode.isra.0.part.0, @function
_ZL8_cmpFoldPKDsiS0_ijPiS1_P10UErrorCode.isra.0.part.0:
.LFB4136:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movl	%r8d, -308(%rbp)
	movq	%r9, -360(%rbp)
	movq	%rax, -416(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testq	%r9, %r9
	je	.L26
	movl	$0, (%r9)
	movl	$0, (%rax)
.L26:
	xorl	%r14d, %r14d
	cmpl	$-1, %esi
	je	.L27
	movq	-320(%rbp), %rax
	movslq	%esi, %rsi
	leaq	(%rax,%rsi,2), %r14
.L27:
	xorl	%ebx, %ebx
	cmpl	$-1, %ecx
	je	.L28
	movq	-328(%rbp), %rax
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,2), %rbx
.L28:
	movl	-308(%rbp), %eax
	movq	-328(%rbp), %rcx
	movq	%r14, %r8
	movl	$-1, %r15d
	movq	-320(%rbp), %r9
	movq	%rbx, -336(%rbp)
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	andl	$4096, %eax
	movq	%rcx, -400(%rbp)
	movq	%rcx, %rbx
	movq	%rcx, %r14
	movl	%eax, -312(%rbp)
	leaq	-296(%rbp), %rax
	movq	%r9, %rdi
	movq	%r9, %r10
	movq	%rax, -352(%rbp)
	movl	$-1, %r11d
	movq	%r9, -408(%rbp)
	movl	%r15d, %r9d
	movq	%rdi, %r15
.L29:
	cmpl	$-1, %r9d
	je	.L35
.L30:
	cmpl	$-1, %r11d
	je	.L164
.L36:
	cmpl	%r11d, %r9d
	je	.L165
	testl	%r9d, %r9d
	js	.L87
	testl	%r11d, %r11d
	js	.L88
	movl	%r9d, %eax
	movl	%r9d, %edi
	andl	$-2048, %eax
	movl	%eax, -368(%rbp)
	cmpl	$55296, %eax
	je	.L166
.L47:
	movl	%r11d, %eax
	movl	%r11d, -344(%rbp)
	andl	$-2048, %eax
	movl	%eax, -376(%rbp)
	cmpl	$55296, %eax
	je	.L167
.L49:
	testl	%r12d, %r12d
	je	.L51
.L55:
	testl	%r13d, %r13d
	jne	.L161
	movl	-308(%rbp), %edx
	movq	-352(%rbp), %rsi
	movq	%r10, -392(%rbp)
	movl	-344(%rbp), %edi
	movq	%r8, -384(%rbp)
	movl	%r11d, -372(%rbp)
	movl	%r9d, -368(%rbp)
	call	ucase_toFullFolding_67@PLT
	movl	-368(%rbp), %r9d
	movl	-372(%rbp), %r11d
	testl	%eax, %eax
	movq	-384(%rbp), %r8
	movq	-392(%rbp), %r10
	movl	%eax, %r13d
	jns	.L168
.L161:
	movq	%r15, %rdi
	movq	%r14, %rax
	movl	%r9d, %r15d
	movq	%rbx, %rcx
	movq	%r8, %r14
	movq	-336(%rbp), %rbx
	movq	%rax, %r8
	movq	%rdi, %r9
	cmpl	$55295, %r15d
	jle	.L58
	cmpl	$55295, %r11d
	jle	.L58
	testl	$32768, -308(%rbp)
	jne	.L169
.L58:
	movl	%r15d, %eax
	subl	%r11d, %eax
.L43:
	movq	-360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L25
	movq	-408(%rbp), %rdx
	subq	-320(%rbp), %rdx
	sarq	%rdx
	movq	-416(%rbp), %rbx
	movl	%edx, (%rdi)
	movq	-400(%rbp), %rdx
	subq	-328(%rbp), %rdx
	sarq	%rdx
	movl	%edx, (%rbx)
.L25:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L170
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	subl	$2, %r12d
	movslq	%r12d, %rax
	leaq	(%rax,%rax,2), %rdx
	movq	-288(%rbp,%rdx,8), %r10
.L34:
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	movq	-280(%rbp,%rax), %r15
	movq	-272(%rbp,%rax), %r8
.L35:
	cmpq	%r8, %r15
	je	.L31
.L158:
	movzwl	(%r15), %eax
	testl	%eax, %eax
	jne	.L32
	testq	%r8, %r8
	je	.L31
	movl	-312(%rbp), %edx
	testl	%edx, %edx
	je	.L32
.L31:
	testl	%r12d, %r12d
	je	.L30
	leal	-1(%r12), %edx
	movslq	%edx, %rax
	leaq	(%rax,%rax,2), %rsi
	movq	-288(%rbp,%rsi,8), %r10
	testq	%r10, %r10
	je	.L171
	movl	%edx, %r12d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L165:
	cmpl	$-1, %r9d
	jne	.L172
	xorl	%eax, %eax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L172:
	testl	%r12d, %r12d
	jne	.L173
	movq	%r15, %rax
.L44:
	testq	%rax, %rax
	je	.L163
	testl	%r13d, %r13d
	je	.L84
	movq	-232(%rbp), %rdx
	movl	$-1, %r11d
	movl	$-1, %r9d
	cmpq	-336(%rbp), %rbx
	jne	.L35
.L46:
	movq	-400(%rbp), %rsi
	testq	%rdx, %rdx
	cmove	-408(%rbp), %rax
	cmovne	%rdx, %rsi
	movq	%rax, -408(%rbp)
	movq	%rsi, -400(%rbp)
.L163:
	movl	$-1, %r11d
	movl	$-1, %r9d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L168:
	cmpl	$55296, -376(%rbp)
	je	.L174
.L64:
	movq	-336(%rbp), %rax
	movq	%r14, %xmm0
	movq	%rbx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	cmpl	$31, %r13d
	jle	.L175
	cmpl	$65535, %r13d
	jle	.L176
	movl	%r13d, %eax
	andw	$1023, %r13w
	leaq	-128(%rbp), %r14
	movl	$4, %ecx
	sarl	$10, %eax
	orw	$-9216, %r13w
	subw	$10304, %ax
	movw	%r13w, -126(%rbp)
	movw	%ax, -128(%rbp)
.L67:
	leaq	(%r14,%rcx), %rax
	movl	$-1, %r11d
	movl	$1, %r13d
	movq	%r14, %rbx
	movq	%rax, -336(%rbp)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L167:
	testl	$1024, %r11d
	jne	.L50
	cmpq	-336(%rbp), %rbx
	je	.L49
	movzwl	(%rbx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L49
	movl	%r11d, %edx
	sall	$10, %edx
	leal	-56613888(%rax,%rdx), %eax
	movl	%eax, -344(%rbp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L166:
	testl	$1024, %r9d
	jne	.L48
	cmpq	%r8, %r15
	je	.L47
	movzwl	(%r15), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L47
	movl	%r9d, %edx
	sall	$10, %edx
	leal	-56613888(%rax,%rdx), %edi
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L51:
	movl	-308(%rbp), %edx
	movq	-352(%rbp), %rsi
	movq	%r10, -424(%rbp)
	movq	%r8, -392(%rbp)
	movl	%r11d, -384(%rbp)
	movl	%r9d, -372(%rbp)
	call	ucase_toFullFolding_67@PLT
	movl	-372(%rbp), %r9d
	movl	-384(%rbp), %r11d
	testl	%eax, %eax
	movq	-392(%rbp), %r8
	movq	-424(%rbp), %r10
	movl	%eax, %edx
	js	.L55
	cmpl	$55296, -368(%rbp)
	je	.L177
.L59:
	movq	%r10, %xmm0
	movq	%r15, %xmm2
	movq	%r8, -272(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -288(%rbp)
	cmpl	$31, %edx
	jle	.L178
	cmpl	$65535, %edx
	jg	.L63
	movw	%dx, -192(%rbp)
	movl	$2, %r8d
	leaq	-192(%rbp), %r15
.L62:
	addq	%r15, %r8
	movl	$-1, %r9d
	movl	$1, %r12d
	movq	%r15, %r10
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L164:
	movl	-312(%rbp), %edx
	movq	-336(%rbp), %rcx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L180:
	subl	$2, %r13d
	movslq	%r13d, %rax
	leaq	(%rax,%rax,2), %rcx
	movq	-240(%rbp,%rcx,8), %r14
.L40:
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	movq	-232(%rbp,%rax), %rbx
	movq	-224(%rbp,%rax), %rcx
.L41:
	cmpq	%rcx, %rbx
	je	.L37
	movzwl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L38
	testq	%rcx, %rcx
	je	.L37
	testl	%edx, %edx
	je	.L38
.L37:
	testl	%r13d, %r13d
	je	.L179
	leal	-1(%r13), %ecx
	movslq	%ecx, %rax
	leaq	(%rax,%rax,2), %rsi
	movq	-240(%rbp,%rsi,8), %r14
	testq	%r14, %r14
	je	.L180
	movl	%ecx, %r13d
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%rcx, -336(%rbp)
	addq	$2, %rbx
	movl	%eax, %r11d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$2, %r15
	movl	%eax, %r9d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	-4(%r15), %rax
	cmpq	%rax, %r10
	ja	.L47
	movzwl	-4(%r15), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L47
	sall	$10, %eax
	leal	-56613888(%r9,%rax), %edi
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	-4(%rbx), %rax
	movl	%r11d, -344(%rbp)
	cmpq	%rax, %r14
	ja	.L49
	movzwl	-4(%rbx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L49
	sall	$10, %eax
	leal	-56613888(%r11,%rax), %eax
	movl	%eax, -344(%rbp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L169:
	cmpl	$56319, %r15d
	jg	.L69
	cmpq	%r14, %rdi
	je	.L69
	movzwl	(%rdi), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L70
.L69:
	movl	%r15d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L181
.L71:
	subl	$10240, %r15d
.L70:
	cmpl	$56319, %r11d
	jg	.L72
	cmpq	%rbx, %rcx
	je	.L72
	movzwl	(%rcx), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L58
.L72:
	movl	%r11d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L182
.L73:
	subl	$10240, %r11d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L176:
	movw	%r13w, -128(%rbp)
	movl	$2, %ecx
	leaq	-128(%rbp), %r14
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-280(%rbp), %rax
	movl	$-1, %r11d
	movl	$-1, %r9d
	cmpq	%r8, %r15
	jne	.L158
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%rcx, -336(%rbp)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L174:
	andl	$1024, %r11d
	jne	.L65
	addq	$2, %rbx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-296(%rbp), %rsi
	leaq	-128(%rbp), %r14
	movl	%r13d, %edx
	movq	%r10, -368(%rbp)
	movq	%r14, %rdi
	movq	%r8, -344(%rbp)
	movl	%r9d, -336(%rbp)
	call	u_memcpy_67@PLT
	movslq	%r13d, %rcx
	movl	-336(%rbp), %r9d
	movq	-344(%rbp), %r8
	movq	-368(%rbp), %r10
	addq	%rcx, %rcx
	jmp	.L67
.L65:
	movzwl	-4(%r15), %r9d
	subq	$2, -400(%rbp)
	subq	$2, %r15
	jmp	.L64
.L84:
	movq	%rbx, %rdx
	jmp	.L46
.L177:
	andl	$1024, %r9d
	jne	.L60
	addq	$2, %r15
	jmp	.L59
.L63:
	movl	%edx, %eax
	andw	$1023, %dx
	movl	$4, %r8d
	sarl	$10, %eax
	orw	$-9216, %dx
	leaq	-192(%rbp), %r15
	subw	$10304, %ax
	movw	%dx, -190(%rbp)
	movw	%ax, -192(%rbp)
	jmp	.L62
.L178:
	movq	-296(%rbp), %rsi
	leaq	-192(%rbp), %r15
	movl	%r11d, -368(%rbp)
	movq	%r15, %rdi
	movl	%edx, -344(%rbp)
	call	u_memcpy_67@PLT
	movslq	-344(%rbp), %r8
	movl	-368(%rbp), %r11d
	addq	%r8, %r8
	jmp	.L62
.L60:
	movzwl	-4(%rbx), %r11d
	subq	$2, -400(%rbp)
	subq	$2, %rbx
	jmp	.L59
.L182:
	leaq	-2(%rcx), %rax
	cmpq	%rax, %r8
	je	.L73
	movzwl	-4(%rcx), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L73
	jmp	.L58
.L181:
	leaq	-2(%r9), %rax
	cmpq	%rax, %r10
	je	.L71
	movzwl	-4(%r9), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L71
	jmp	.L70
.L87:
	movl	$-1, %eax
	jmp	.L43
.L88:
	movl	$1, %eax
	jmp	.L43
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4136:
	.size	_ZL8_cmpFoldPKDsiS0_ijPiS1_P10UErrorCode.isra.0.part.0, .-_ZL8_cmpFoldPKDsiS0_ijPiS1_P10UErrorCode.isra.0.part.0
	.p2align 4
	.type	ustrcase_mapWithOverlap_67.part.0, @function
ustrcase_mapWithOverlap_67.part.0:
.LFB4140:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$648, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %r15d
	movq	24(%rbp), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rbp), %rbx
	cmpl	$-1, %r15d
	je	.L212
.L184:
	testq	%r12, %r12
	je	.L198
	cmpq	%r12, %r9
	jb	.L186
	movslq	%r13d, %rax
	leaq	(%r12,%rax,2), %rax
	cmpq	%rax, %r9
	jb	.L187
	cmpq	%r12, %r9
	je	.L186
.L200:
	movq	%r12, %rcx
.L185:
	subq	$8, %rsp
	movl	%r14d, %edi
	movl	%r13d, %r8d
	pushq	%rbx
	pushq	$0
	pushq	%r15
	call	*%r10
	movl	%eax, %r14d
	addq	$32, %rsp
.L192:
	movq	%rbx, %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	u_terminateUChars_67@PLT
.L183:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L213
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movslq	%r15d, %rax
	leaq	(%r9,%rax,2), %rax
	cmpq	%rax, %r12
	jnb	.L200
.L187:
	cmpl	$300, %r13d
	jg	.L214
	subq	$8, %rsp
	leaq	-656(%rbp), %r11
	movl	%r14d, %edi
	movl	%r13d, %r8d
	pushq	%rbx
	movq	%r11, %rcx
	pushq	$0
	pushq	%r15
	movq	%r11, -664(%rbp)
	call	*%r10
	movl	(%rbx), %edx
	movl	%eax, %r14d
	addq	$32, %rsp
	testl	%edx, %edx
	jg	.L192
	movq	-664(%rbp), %r11
	testl	%eax, %eax
	jle	.L192
	cmpl	%eax, %r13d
	jl	.L192
	movl	%eax, %edx
	movq	%r11, %rsi
	movq	%r12, %rdi
	call	u_memmove_67@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L198:
	xorl	%ecx, %ecx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%r9, %rdi
	movq	%r10, -688(%rbp)
	movq	%rdx, -680(%rbp)
	movl	%esi, -672(%rbp)
	movq	%r9, -664(%rbp)
	call	u_strlen_67@PLT
	movq	-688(%rbp), %r10
	movq	-680(%rbp), %rdx
	movl	-672(%rbp), %esi
	movq	-664(%rbp), %r9
	movl	%eax, %r15d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L214:
	leal	(%r13,%r13), %edi
	movq	%r10, -688(%rbp)
	movslq	%edi, %rdi
	movq	%r9, -680(%rbp)
	movq	%rdx, -672(%rbp)
	movl	%esi, -664(%rbp)
	call	uprv_malloc_67@PLT
	movl	-664(%rbp), %esi
	movq	-672(%rbp), %rdx
	testq	%rax, %rax
	movq	-680(%rbp), %r9
	movq	-688(%rbp), %r10
	je	.L215
	subq	$8, %rsp
	movl	%r14d, %edi
	movl	%r13d, %r8d
	movq	%rax, %rcx
	pushq	%rbx
	pushq	$0
	pushq	%r15
	movq	%rax, -664(%rbp)
	call	*%r10
	movq	-664(%rbp), %r11
	movl	%eax, %r14d
	addq	$32, %rsp
	cmpq	%r11, %r12
	je	.L192
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L216
.L194:
	movq	%r11, %rdi
	call	uprv_free_67@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L216:
	cmpl	%r14d, %r13d
	jl	.L194
	testl	%r14d, %r14d
	jle	.L194
	movq	%r11, %rsi
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	%r11, -664(%rbp)
	call	u_memmove_67@PLT
	movq	-664(%rbp), %r11
	jmp	.L194
.L213:
	call	__stack_chk_fail@PLT
.L215:
	movl	$7, (%rbx)
	xorl	%eax, %eax
	jmp	.L183
	.cfi_endproc
.LFE4140:
	.size	ustrcase_mapWithOverlap_67.part.0, .-ustrcase_mapWithOverlap_67.part.0
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_17toLowerEijPDsiPKDsP12UCaseContextiiPNS_5EditsER10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_17toLowerEijPDsiPKDsP12UCaseContextiiPNS_5EditsER10UErrorCode:
.LFB3134:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rbp), %rax
	movl	%ecx, -68(%rbp)
	movq	%rdx, -128(%rbp)
	leaq	_ZN6icu_679LatinCase15TO_LOWER_NORMALE(%rip), %rdx
	movq	32(%rbp), %r11
	movq	%r9, -168(%rbp)
	movl	16(%rbp), %r9d
	movl	%edi, -144(%rbp)
	movl	%esi, -156(%rbp)
	movq	%rax, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -80(%rbp)
	cmpl	$1, %edi
	je	.L218
	testl	%edi, %edi
	js	.L219
	leal	-2(%rdi), %eax
	cmpl	$1, %eax
	leaq	_ZN6icu_679LatinCase14TO_LOWER_TR_LTE(%rip), %rax
	cmova	%rdx, %rax
	movq	%rax, -80(%rbp)
.L218:
	movq	%r11, -96(%rbp)
	xorl	%r14d, %r14d
	movl	%r9d, 16(%rbp)
	movq	%r10, -88(%rbp)
	call	ucase_getTrie_67@PLT
	movslq	16(%rbp), %r9
	movq	-88(%rbp), %r10
	movq	%rax, -136(%rbp)
	movl	-156(%rbp), %eax
	movq	-96(%rbp), %r11
	movl	%r9d, %r15d
	andl	$16384, %eax
	movl	%eax, -72(%rbp)
	movl	24(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -140(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -152(%rbp)
.L254:
	cmpl	24(%rbp), %r15d
	jge	.L220
	movl	-140(%rbp), %eax
	movslq	%r15d, %rbx
	movslq	%r9d, %rdx
	movq	-136(%rbp), %r9
	subl	%r15d, %eax
	leaq	1(%rbx,%rax), %r8
	movl	%r14d, %eax
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L354:
	movq	-80(%rbp), %rdi
	movzwl	%r13w, %ecx
	movsbl	(%rdi,%rcx), %r12d
	cmpb	$-128, %r12b
	je	.L223
	testb	%r12b, %r12b
	je	.L224
.L225:
	subl	%edx, %r15d
	testl	%r15d, %r15d
	jle	.L228
	testq	%r11, %r11
	je	.L229
	movq	%r11, %rdi
	movl	%r15d, %esi
	movl	%edx, 16(%rbp)
	movq	%r10, -120(%rbp)
	movl	%eax, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movslq	16(%rbp), %rdx
	movq	-120(%rbp), %r10
	movl	-112(%rbp), %eax
	movq	-104(%rbp), %r8
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r11
.L229:
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	jne	.L228
	movl	$2147483647, %ecx
	subl	%eax, %ecx
	cmpl	%ecx, %r15d
	jg	.L350
	leal	(%r15,%rax), %ecx
	cmpl	%ecx, -68(%rbp)
	jge	.L352
	movl	%ecx, %eax
.L228:
	cmpl	%eax, -68(%rbp)
	jle	.L353
	movq	-128(%rbp), %rdi
	movslq	%eax, %rdx
	addl	%r13d, %r12d
	movw	%r12w, (%rdi,%rdx,2)
.L231:
	addl	$1, %eax
	movslq	%r14d, %rdx
	testq	%r11, %r11
	je	.L224
	movl	$1, %edx
	movq	%r11, %rdi
	movl	$1, %esi
	movl	%eax, -112(%rbp)
	movq	%r10, -120(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movq	-88(%rbp), %r11
	movq	-96(%rbp), %r9
	movslq	%r14d, %rdx
	movq	-104(%rbp), %r8
	movl	-112(%rbp), %eax
	movq	-120(%rbp), %r10
.L224:
	addq	$1, %rbx
	cmpq	%rbx, %r8
	je	.L347
.L221:
	movzwl	(%r10,%rbx,2), %r13d
	movl	%ebx, %r15d
	leal	1(%rbx), %r14d
	cmpw	$382, %r13w
	jbe	.L354
	movzwl	%r13w, %edi
	cmpw	$-10241, %r13w
	ja	.L226
	movl	%edi, %esi
	movq	(%r9), %rcx
	sarl	$5, %esi
	movslq	%esi, %rsi
	movzwl	(%rcx,%rsi,2), %r12d
	movl	%r13d, %esi
	andl	$31, %esi
	leal	(%rsi,%r12,4), %esi
	movslq	%esi, %rsi
	movswl	(%rcx,%rsi,2), %r12d
	testb	$8, %r12b
	jne	.L348
	testb	$2, %r12b
	je	.L224
	sarl	$7, %r12d
	jne	.L225
	addq	$1, %rbx
	cmpq	%rbx, %r8
	jne	.L221
	.p2align 4,,10
	.p2align 3
.L347:
	movl	%r14d, %r15d
	movslq	%edx, %r9
	movl	%eax, %r14d
.L220:
	subl	%r9d, %r15d
	testl	%r15d, %r15d
	jle	.L217
	testq	%r11, %r11
	je	.L255
	movl	%r15d, %esi
	movq	%r11, %rdi
	movl	%r9d, 16(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movslq	16(%rbp), %r9
	movq	-80(%rbp), %r10
.L255:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jne	.L217
	movl	$2147483647, %eax
	subl	%r14d, %eax
	cmpl	%eax, %r15d
	jg	.L350
	leal	(%r15,%r14), %ebx
	cmpl	%ebx, -68(%rbp)
	jge	.L355
	movl	%ebx, %r14d
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L356
	addq	$136, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	cmpl	$2147483647, %eax
	jne	.L231
	testq	%r11, %r11
	je	.L350
	movl	$1, %edx
	movl	$1, %esi
	movq	%r11, %rdi
	call	_ZN6icu_675Edits10addReplaceEii@PLT
.L350:
	movq	-176(%rbp), %rax
	xorl	%r14d, %r14d
	movl	$8, (%rax)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L352:
	leaq	(%r10,%rdx,2), %rsi
	movq	-128(%rbp), %rdx
	cltq
	movq	%r11, -120(%rbp)
	movq	%r8, -112(%rbp)
	leaq	(%rdx,%rax,2), %rdi
	movl	%r15d, %edx
	movq	%r9, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	u_memcpy_67@PLT
	movl	-96(%rbp), %ecx
	movq	-88(%rbp), %r10
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %r11
	movl	%ecx, %eax
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L226:
	movl	%r14d, %r15d
	movslq	%eax, %r14
	movl	%edi, %eax
	movl	%ebx, %r13d
	andl	$-1024, %eax
	movl	%edx, %r9d
	cmpl	$55296, %eax
	jne	.L235
	cmpl	%r15d, 24(%rbp)
	jg	.L357
.L235:
	movl	-144(%rbp), %r8d
	movq	%r11, -96(%rbp)
	movl	%r9d, 16(%rbp)
	movq	%r10, -88(%rbp)
	testl	%r8d, %r8d
	js	.L236
	movq	-168(%rbp), %rax
	movq	-152(%rbp), %rcx
	leaq	_ZN6icu_6712_GLOBAL__N_125utf16_caseContextIteratorEPva(%rip), %rsi
	movl	%r13d, 20(%rax)
	movq	%rax, %rdx
	movl	%r15d, 24(%rax)
	call	ucase_toFullLower_67@PLT
	movq	-88(%rbp), %r10
	movslq	16(%rbp), %r9
	movq	-96(%rbp), %r11
	movl	%eax, %ebx
.L237:
	testl	%ebx, %ebx
	js	.L254
	movl	%r13d, %edx
	subl	%r9d, %edx
	testl	%edx, %edx
	jle	.L239
	testq	%r11, %r11
	je	.L240
	movl	%edx, %esi
	movq	%r11, %rdi
	movl	%r9d, 16(%rbp)
	movq	%r10, -104(%rbp)
	movl	%edx, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movslq	16(%rbp), %r9
	movq	-104(%rbp), %r10
	movl	-96(%rbp), %edx
	movq	-88(%rbp), %r11
.L240:
	movl	-72(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L239
	movl	$2147483647, %eax
	subl	%r14d, %eax
	cmpl	%eax, %edx
	jg	.L350
	leal	(%rdx,%r14), %r12d
	cmpl	%r12d, -68(%rbp)
	jge	.L358
	movl	%r12d, %r14d
.L239:
	movl	%r15d, %esi
	subl	%r13d, %esi
	cmpl	$31, %ebx
	jg	.L359
	movl	$-1, %r12d
.L243:
	movq	-64(%rbp), %r13
	testq	%r11, %r11
	je	.L244
	movq	%r11, %rdi
	movl	%ebx, %edx
	movq	%r10, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movq	-96(%rbp), %r10
	movq	-88(%rbp), %r11
.L244:
	movl	$2147483647, %eax
	subl	%r14d, %eax
	cmpl	%eax, %ebx
	jg	.L350
	cmpl	%r14d, -68(%rbp)
	jle	.L245
	cmpl	$-1, %r12d
	je	.L246
	cmpl	$65535, %r12d
	jg	.L247
	movq	-128(%rbp), %rdx
	movslq	%r14d, %rax
	movslq	%r15d, %r9
	addl	$1, %r14d
	movw	%r12w, (%rdx,%rax,2)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L236:
	movl	-156(%rbp), %edx
	movq	-152(%rbp), %rsi
	call	ucase_toFullFolding_67@PLT
	movq	-96(%rbp), %r11
	movslq	16(%rbp), %r9
	movq	-88(%rbp), %r10
	movl	%eax, %ebx
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L223:
	movzwl	%r13w, %edi
	movl	%r14d, %r15d
	movl	%ebx, %r13d
	movslq	%eax, %r14
	movl	%edx, %r9d
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L348:
	movl	%r14d, %r15d
	movl	%ebx, %r13d
	movslq	%eax, %r14
	movl	%edx, %r9d
	jmp	.L235
.L357:
	movslq	%r15d, %rax
	movzwl	(%r10,%rax,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L235
	sall	$10, %edi
	leal	2(%r13), %r15d
	leal	-56613888(%rax,%rdi), %edi
	jmp	.L235
.L359:
	cmpl	%r14d, -68(%rbp)
	jle	.L241
	cmpl	$65535, %ebx
	jg	.L272
	movq	-128(%rbp), %rdx
	movslq	%r14d, %rax
	addl	$1, %r14d
	movw	%bx, (%rdx,%rax,2)
	testq	%r11, %r11
	je	.L274
	movq	%r11, %rdi
	movl	$1, %edx
	movq	%r10, -96(%rbp)
	movq	%r11, -88(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movq	-88(%rbp), %r11
	movq	-96(%rbp), %r10
	jmp	.L274
.L219:
	testb	$7, -156(%rbp)
	leaq	_ZN6icu_679LatinCase14TO_LOWER_TR_LTE(%rip), %rax
	cmove	-80(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L218
.L245:
	addl	%ebx, %r14d
.L274:
	movslq	%r15d, %r9
	jmp	.L254
.L358:
	movq	-128(%rbp), %rax
	leaq	(%r10,%r9,2), %rsi
	movq	%r11, -96(%rbp)
	movq	%r10, -88(%rbp)
	leaq	(%rax,%r14,2), %rdi
	movl	%r12d, %r14d
	call	u_memcpy_67@PLT
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r11
	jmp	.L239
.L246:
	leal	(%rbx,%r14), %eax
	cmpl	%eax, -68(%rbp)
	jl	.L273
	testl	%ebx, %ebx
	je	.L274
	movq	-128(%rbp), %rdi
	movslq	%r14d, %rax
	leaq	16(%r13), %rsi
	leal	-1(%rbx), %ecx
	leaq	(%rdi,%rax,2), %rdx
	leaq	16(%rdi,%rax,2), %rax
	cmpq	%rsi, %rdx
	setnb	%sil
	cmpq	%rax, %r13
	setnb	%al
	orb	%al, %sil
	je	.L249
	cmpl	$6, %ecx
	jbe	.L249
	movdqu	0(%r13), %xmm0
	movl	%ebx, %eax
	shrl	$3, %eax
	movups	%xmm0, (%rdx)
	cmpl	$1, %eax
	je	.L250
	movdqu	16(%r13), %xmm1
	movups	%xmm1, 16(%rdx)
	cmpl	$2, %eax
	je	.L250
	movdqu	32(%r13), %xmm2
	movups	%xmm2, 32(%rdx)
.L250:
	movl	%ebx, %esi
	movl	%ebx, %edx
	andl	$-8, %esi
	andl	$7, %edx
	movl	%esi, %eax
	leal	(%rsi,%r14), %r8d
	leaq	0(%r13,%rax,2), %rax
	cmpl	%esi, %ebx
	je	.L252
	movzwl	(%rax), %esi
	movq	-128(%rbp), %rbx
	leal	1(%r8), %edi
	movslq	%r8d, %r8
	movw	%si, (%rbx,%r8,2)
	cmpl	$1, %edx
	je	.L252
	movzwl	2(%rax), %r8d
	movslq	%edi, %rdi
	leaq	(%rdi,%rdi), %rsi
	movw	%r8w, (%rbx,%rdi,2)
	cmpl	$2, %edx
	je	.L252
	movzwl	4(%rax), %edi
	movw	%di, 2(%rbx,%rsi)
	cmpl	$3, %edx
	je	.L252
	movzwl	6(%rax), %edi
	movw	%di, 4(%rbx,%rsi)
	cmpl	$4, %edx
	je	.L252
	movzwl	8(%rax), %edi
	movw	%di, 6(%rbx,%rsi)
	cmpl	$5, %edx
	je	.L252
	movzwl	10(%rax), %edi
	movw	%di, 8(%rbx,%rsi)
	cmpl	$6, %edx
	je	.L252
	movzwl	12(%rax), %eax
	movw	%ax, 10(%rbx,%rsi)
.L252:
	leal	1(%r14,%rcx), %r14d
	movslq	%r15d, %r9
	jmp	.L254
.L241:
	cmpl	$65535, %ebx
	jg	.L272
	movl	%ebx, %r12d
	movl	$1, %ebx
	jmp	.L243
.L247:
	cmpl	$1114111, %r12d
	jg	.L248
	leal	1(%r14), %eax
	cmpl	%eax, -68(%rbp)
	jle	.L248
	movl	%r12d, %eax
	movq	-128(%rbp), %rcx
	movslq	%r14d, %rdx
	movslq	%r15d, %r9
	sarl	$10, %eax
	addl	$2, %r14d
	subw	$10304, %ax
	movw	%ax, (%rcx,%rdx,2)
	movl	%r12d, %eax
	andw	$1023, %ax
	orw	$-9216, %ax
	movw	%ax, 2(%rcx,%rdx,2)
	jmp	.L254
.L248:
	addl	%ebx, %r14d
	movslq	%r15d, %r9
	jmp	.L254
.L272:
	movl	%ebx, %r12d
	movl	$2, %ebx
	jmp	.L243
.L273:
	movslq	%r15d, %r9
	movl	%eax, %r14d
	jmp	.L254
.L355:
	movq	-128(%rbp), %rax
	movslq	%r14d, %r14
	leaq	(%r10,%r9,2), %rsi
	movl	%r15d, %edx
	leaq	(%rax,%r14,2), %rdi
	movl	%ebx, %r14d
	call	u_memcpy_67@PLT
	jmp	.L217
.L249:
	movl	%ecx, %esi
	xorl	%eax, %eax
.L253:
	movzwl	0(%r13,%rax,2), %edi
	movw	%di, (%rdx,%rax,2)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rsi, %rdi
	jne	.L253
	jmp	.L252
.L356:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3134:
	.size	_ZN6icu_6712_GLOBAL__N_17toLowerEijPDsiPKDsP12UCaseContextiiPNS_5EditsER10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_17toLowerEijPDsiPKDsP12UCaseContextiiPNS_5EditsER10UErrorCode
	.p2align 4
	.globl	ustrcase_internalFold_67
	.type	ustrcase_internalFold_67, @function
ustrcase_internalFold_67:
.LFB3143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	movl	$-1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	movl	16(%rbp), %eax
	.cfi_offset 14, -24
	movq	24(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	32(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%r8d, %ebx
	movq	%r9, %r8
	xorl	%r9d, %r9d
	pushq	%r13
	movl	%ebx, %ecx
	pushq	%r14
	pushq	%rax
	pushq	$0
	call	_ZN6icu_6712_GLOBAL__N_17toLowerEijPDsiPKDsP12UCaseContextiiPNS_5EditsER10UErrorCode
	addq	$32, %rsp
	movl	%eax, %r12d
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L367
.L360:
	leaq	-32(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	cmpl	%r12d, %ebx
	jge	.L362
	movl	$15, 0(%r13)
	leaq	-32(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L360
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	leaq	-32(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3143:
	.size	ustrcase_internalFold_67, .-ustrcase_internalFold_67
	.p2align 4
	.globl	ustrcase_internalToTitle_67
	.type	ustrcase_internalToTitle_67, @function
ustrcase_internalToTitle_67:
.LFB3136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movl	%edi, -168(%rbp)
	movl	%esi, -148(%rbp)
	movq	%rax, -176(%rbp)
	movq	32(%rbp), %rax
	movq	%rcx, -184(%rbp)
	movq	%rax, -192(%rbp)
	movl	(%rax), %eax
	movl	%r8d, -164(%rbp)
	movq	%r9, -120(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movl	$0, -128(%rbp)
	testl	%eax, %eax
	jg	.L368
	movl	%esi, %eax
	andl	$1536, %eax
	cmpl	$1536, %eax
	je	.L542
	movl	16(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	$0, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	movq	%r9, -96(%rbp)
	movl	%eax, -80(%rbp)
	testl	%eax, %eax
	jle	.L371
	movq	(%rdx), %rax
	movq	%rdx, %r12
	movq	%rdx, %rdi
	xorl	%r15d, %r15d
	call	*80(%rax)
	movl	%eax, %ebx
	movl	-148(%rbp), %eax
	movl	%eax, %ecx
	movl	%eax, %edx
	andl	$512, %ecx
	andl	$1024, %edx
	movl	%ecx, -124(%rbp)
	movl	%eax, %ecx
	andl	$256, %eax
	movl	%eax, -152(%rbp)
	andl	$16384, %ecx
	movq	%r12, %rax
	movl	%r15d, %r12d
	movl	%ebx, %r15d
	movl	%edx, -144(%rbp)
	movq	%rax, %rbx
	movl	%ecx, -140(%rbp)
	cmpl	$-1, %r15d
	je	.L376
.L543:
	cmpl	16(%rbp), %r15d
	jg	.L376
	cmpl	%r12d, %r15d
	jg	.L426
	cmpl	16(%rbp), %r15d
	je	.L373
.L374:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	%r15d, %r12d
	call	*104(%rax)
	movl	%eax, %r15d
	cmpl	$-1, %r15d
	jne	.L543
.L376:
	cmpl	%r12d, 16(%rbp)
	jle	.L373
	movl	16(%rbp), %r15d
.L426:
	movq	-120(%rbp), %rsi
	movslq	%r12d, %rax
	leal	1(%r12), %r13d
	addq	%rax, %rax
	leaq	(%rsi,%rax), %rdx
	movzwl	(%rdx), %r14d
	movq	%rdx, -160(%rbp)
	movl	%r14d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L378
	cmpl	%r15d, %r13d
	jne	.L544
.L378:
	movl	-124(%rbp), %eax
	testl	%eax, %eax
	je	.L545
.L379:
	cmpl	%r13d, %r12d
	jl	.L546
.L420:
	cmpl	%r15d, 16(%rbp)
	jg	.L374
.L373:
	movq	-192(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L371
.L368:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L547
	movl	-128(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	leaq	-96(%rbp), %r9
	movl	%r14d, %edi
	leaq	-104(%rbp), %rcx
	movl	%r12d, -76(%rbp)
	movl	-168(%rbp), %r8d
	movq	%r9, %rdx
	movl	%r13d, -72(%rbp)
	leaq	_ZN6icu_6712_GLOBAL__N_125utf16_caseContextIteratorEPva(%rip), %rsi
	movq	%r9, -136(%rbp)
	call	ucase_toFullTitle_67@PLT
	movl	%r13d, %esi
	movq	-136(%rbp), %r9
	movl	%eax, %r14d
	subl	%r12d, %esi
	movq	-104(%rbp), %rax
	testl	%r14d, %r14d
	js	.L548
	cmpl	$31, %r14d
	jle	.L434
	movl	-128(%rbp), %ecx
	movl	-164(%rbp), %edi
	cmpl	%edi, %ecx
	jge	.L397
	cmpl	$65535, %r14d
	jg	.L436
	movq	-184(%rbp), %rdi
	movslq	%ecx, %rax
	movw	%r14w, (%rdi,%rax,2)
	movq	-176(%rbp), %rdi
	leal	1(%rcx), %eax
	movl	%eax, -128(%rbp)
	testq	%rdi, %rdi
	je	.L394
	movl	$1, %edx
	movq	%r9, -136(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movq	-136(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L394:
	movl	-128(%rbp), %r8d
	testl	%r8d, %r8d
	js	.L399
	leal	1(%r12), %eax
	cmpl	%r15d, %eax
	jge	.L413
	cmpl	$5, -168(%rbp)
	je	.L549
.L413:
	cmpl	%r15d, %r13d
	jge	.L420
	movl	-152(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L421
	movq	-192(%rbp), %r12
	movslq	-128(%rbp), %rax
	movq	-184(%rbp), %rsi
	movl	-164(%rbp), %ecx
	movq	-120(%rbp), %r8
	movl	-168(%rbp), %edi
	pushq	%r12
	movq	%rax, %r14
	pushq	-176(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	movl	-148(%rbp), %esi
	subl	%eax, %ecx
	pushq	%r15
	pushq	%r13
	call	_ZN6icu_6712_GLOBAL__N_17toLowerEijPDsiPKDsP12UCaseContextiiPNS_5EditsER10UErrorCode
	addq	$32, %rsp
	addl	%eax, %r14d
	movl	(%r12), %eax
	movl	%r14d, -128(%rbp)
	cmpl	$15, %eax
	je	.L550
	testl	%eax, %eax
	jle	.L420
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L545:
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	jne	.L430
	movq	%rbx, %rax
	movl	%r12d, -136(%rbp)
	movl	%r13d, %ebx
	movl	%r14d, %r13d
	movq	%rax, %r14
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L553:
	cmpl	$4, %esi
	je	.L551
	cmpl	%r15d, %ebx
	je	.L537
.L554:
	movq	-120(%rbp), %rcx
	movslq	%ebx, %rax
	leal	1(%rbx), %esi
	leaq	(%rax,%rax), %rdi
	movzwl	(%rcx,%rax,2), %r13d
	movl	%r13d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L384
	cmpl	%r15d, %esi
	jne	.L552
.L384:
	movl	%ebx, -136(%rbp)
	movl	%esi, %ebx
.L385:
	movl	%r13d, %edi
	call	u_charType_67@PLT
	movl	$251792942, %ecx
	btl	%eax, %ecx
	movsbl	%al, %esi
	jnc	.L553
.L536:
	movq	%r14, %rax
	movl	%r13d, %r14d
	movl	%ebx, %r13d
	movq	%rax, %rbx
.L381:
	cmpl	%r12d, -136(%rbp)
	jg	.L425
.L431:
	movl	-136(%rbp), %r12d
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L551:
	movl	%r13d, %edi
	call	ucase_getType_67@PLT
	testl	%eax, %eax
	jne	.L536
	cmpl	%r15d, %ebx
	jne	.L554
.L537:
	movq	%r14, %rbx
	movl	%r13d, %r14d
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L552:
	movzwl	2(%rcx,%rdi), %eax
	movl	%eax, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L384
	movl	%r13d, %ecx
	leal	2(%rbx), %esi
	sall	$10, %ecx
	leal	-56613888(%rax,%rcx), %r13d
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L544:
	movzwl	2(%rsi,%rax), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L378
	movl	%r14d, %r9d
	leal	2(%r12), %r13d
	sall	$10, %r9d
	leal	-56613888(%rax,%r9), %r14d
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L430:
	movl	%r12d, %eax
	movl	%r12d, -136(%rbp)
	movl	%r13d, %r12d
	movq	-120(%rbp), %r13
	movq	%rbx, -200(%rbp)
	movl	%eax, %ebx
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L386:
	movl	%r12d, %ebx
	movl	%esi, %r12d
.L380:
	movl	%r14d, %edi
	call	ucase_getType_67@PLT
	testl	%eax, %eax
	jne	.L538
	cmpl	%r15d, %r12d
	je	.L539
	movslq	%r12d, %rax
	leal	1(%r12), %esi
	movzwl	0(%r13,%rax,2), %r14d
	leaq	(%rax,%rax), %rdi
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L386
	cmpl	%r15d, %esi
	je	.L386
	movzwl	2(%r13,%rdi), %eax
	movl	%eax, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L386
	movl	%r14d, %ecx
	leal	2(%r12), %esi
	sall	$10, %ecx
	leal	-56613888(%rax,%rcx), %r14d
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L548:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L393
	movq	%r9, -200(%rbp)
	movq	%rax, -160(%rbp)
	movl	%esi, -136(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movq	-200(%rbp), %r9
	movq	-160(%rbp), %rax
	movl	-136(%rbp), %esi
.L393:
	movl	-140(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L394
	movl	-128(%rbp), %edi
	movl	-164(%rbp), %edx
	movl	%r14d, %ecx
	notl	%ecx
	cmpl	%edx, %edi
	jge	.L433
	cmpl	$65535, %ecx
	jg	.L433
	movslq	%edi, %rax
	movq	-184(%rbp), %rdi
	movw	%cx, (%rdi,%rax,2)
	leal	1(%rax), %eax
	movl	%eax, -128(%rbp)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L434:
	movl	$-1, %ecx
.L396:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L395
	movl	%r14d, %edx
	movq	%r9, -200(%rbp)
	movl	%ecx, -160(%rbp)
	movq	%rax, -136(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movq	-136(%rbp), %rax
	movl	-160(%rbp), %ecx
	movq	-200(%rbp), %r9
.L395:
	movl	-128(%rbp), %edi
	movl	$2147483647, %edx
	subl	%edi, %edx
	cmpl	%r14d, %edx
	jl	.L399
	movl	-164(%rbp), %esi
	cmpl	%esi, %edi
	jge	.L400
	cmpl	$-1, %ecx
	je	.L401
	cmpl	$65535, %ecx
	jg	.L402
	movq	-184(%rbp), %rsi
	movslq	%edi, %rax
	movw	%cx, (%rsi,%rax,2)
	leal	1(%rdi), %eax
	movl	%eax, -128(%rbp)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L402:
	cmpl	$1114111, %ecx
	jle	.L555
	.p2align 4,,10
	.p2align 3
.L400:
	addl	%r14d, -128(%rbp)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L539:
	movl	-136(%rbp), %r12d
	movq	-200(%rbp), %rbx
.L383:
	cmpl	%r15d, %r12d
	jge	.L420
	movl	%r15d, -136(%rbp)
	movl	%r15d, %r13d
.L425:
	movl	-136(%rbp), %edx
	movq	-176(%rbp), %rdi
	subl	%r12d, %edx
	testq	%rdi, %rdi
	je	.L387
	movl	%edx, %esi
	movl	%edx, -200(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movl	-200(%rbp), %edx
.L387:
	movl	-140(%rbp), %r12d
	testl	%r12d, %r12d
	jne	.L388
	movl	$2147483647, %eax
	subl	-128(%rbp), %eax
	cmpl	%eax, %edx
	jg	.L399
	movslq	-128(%rbp), %rax
	leal	(%rdx,%rax), %r12d
	cmpl	%r12d, -164(%rbp)
	jge	.L556
	movl	%r12d, -128(%rbp)
.L388:
	movl	-128(%rbp), %r11d
	testl	%r11d, %r11d
	jns	.L431
.L399:
	movq	-192(%rbp), %rax
	movl	$0, -128(%rbp)
	movl	$8, (%rax)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L421:
	movq	-176(%rbp), %rax
	movl	%r15d, %r12d
	subl	%r13d, %r12d
	testq	%rax, %rax
	je	.L423
	movl	%r12d, %esi
	movq	%rax, %rdi
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
.L423:
	movl	-140(%rbp), %edx
	testl	%edx, %edx
	jne	.L420
	movl	-128(%rbp), %esi
	movl	$2147483647, %eax
	subl	%esi, %eax
	cmpl	%eax, %r12d
	jg	.L399
	leal	(%r12,%rsi), %r14d
	cmpl	%r14d, -164(%rbp)
	jge	.L557
	movl	%r14d, -128(%rbp)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L538:
	movl	%r12d, %r13d
	movl	-136(%rbp), %r12d
	movl	%ebx, -136(%rbp)
	movq	-200(%rbp), %rbx
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L433:
	movl	%esi, %r14d
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L549:
	movq	-120(%rbp), %rsi
	movslq	%r12d, %r11
	leaq	(%r11,%r11), %rdx
	movzwl	(%rsi,%r11,2), %eax
	andl	$-33, %eax
	cmpw	$73, %ax
	jne	.L413
	leaq	2(%rsi,%rdx), %r14
	movzwl	(%r14), %eax
	cmpw	$106, %ax
	je	.L558
	cmpw	$74, %ax
	jne	.L413
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L418
	movl	$1, %esi
	movq	%r9, -136(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movq	-136(%rbp), %r9
.L418:
	movl	-140(%rbp), %esi
	testl	%esi, %esi
	jne	.L438
	movslq	-128(%rbp), %rax
	cmpl	$2147483647, %eax
	je	.L399
	leal	1(%rax), %r12d
	cmpl	%r12d, -164(%rbp)
	jge	.L559
.L419:
	movl	%r12d, -128(%rbp)
	addl	$1, %r13d
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L401:
	leal	(%rdi,%r14), %edx
	cmpl	%edx, -164(%rbp)
	jl	.L437
	testl	%r14d, %r14d
	je	.L394
	movq	-184(%rbp), %r11
	movslq	%edi, %rdx
	leaq	16(%rax), %rdi
	leal	-1(%r14), %esi
	leaq	(%r11,%rdx,2), %rcx
	leaq	16(%r11,%rdx,2), %rdx
	cmpq	%rdi, %rcx
	setnb	%dil
	cmpq	%rdx, %rax
	setnb	%dl
	orb	%dl, %dil
	je	.L404
	cmpl	$6, %esi
	jbe	.L404
	movl	%r14d, %edi
	xorl	%edx, %edx
	shrl	$3, %edi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L406:
	movdqu	(%rax,%rdx), %xmm1
	movups	%xmm1, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdi, %rdx
	jne	.L406
	movl	%r14d, %ecx
	movl	%r14d, %edx
	andl	$-8, %ecx
	andl	$7, %edx
	movl	%ecx, %edi
	leaq	(%rax,%rdi,2), %rax
	movl	-128(%rbp), %edi
	addl	%ecx, %edi
	cmpl	%r14d, %ecx
	je	.L408
	movzwl	(%rax), %r8d
	movq	-184(%rbp), %r10
	leal	1(%rdi), %ecx
	movslq	%edi, %rdi
	movw	%r8w, (%r10,%rdi,2)
	cmpl	$1, %edx
	je	.L408
	movzwl	2(%rax), %r8d
	movslq	%ecx, %rcx
	leaq	(%rcx,%rcx), %rdi
	movw	%r8w, (%r10,%rcx,2)
	cmpl	$2, %edx
	je	.L408
	movzwl	4(%rax), %ecx
	movw	%cx, 2(%r10,%rdi)
	cmpl	$3, %edx
	je	.L408
	movzwl	6(%rax), %ecx
	movw	%cx, 4(%r10,%rdi)
	cmpl	$4, %edx
	je	.L408
	movzwl	8(%rax), %ecx
	movw	%cx, 6(%r10,%rdi)
	cmpl	$5, %edx
	je	.L408
	movzwl	10(%rax), %ecx
	movw	%cx, 8(%r10,%rdi)
	cmpl	$6, %edx
	je	.L408
	movzwl	12(%rax), %eax
	movw	%ax, 10(%r10,%rdi)
.L408:
	movl	-128(%rbp), %eax
	leal	1(%rax,%rsi), %eax
	movl	%eax, -128(%rbp)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L397:
	cmpl	$65535, %r14d
	jg	.L436
	movl	%r14d, %ecx
	movl	$1, %r14d
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L436:
	movl	%r14d, %ecx
	movl	$2, %r14d
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L550:
	movq	-192(%rbp), %rax
	movl	$0, (%rax)
	jmp	.L420
.L437:
	movl	%edx, -128(%rbp)
	jmp	.L394
.L555:
	movl	%edi, %esi
	leal	1(%rdi), %eax
	cmpl	%eax, -164(%rbp)
	jle	.L400
	movl	%ecx, %eax
	movslq	%edi, %rdx
	movq	-184(%rbp), %rdi
	addl	$2, %esi
	sarl	$10, %eax
	movl	%esi, -128(%rbp)
	subw	$10304, %ax
	movw	%ax, (%rdi,%rdx,2)
	movl	%ecx, %eax
	andw	$1023, %ax
	orw	$-9216, %ax
	movw	%ax, 2(%rdi,%rdx,2)
	jmp	.L394
.L371:
	movl	-128(%rbp), %ebx
	cmpl	%ebx, -164(%rbp)
	jge	.L424
	movq	-192(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L368
.L542:
	movq	-192(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L368
.L558:
	movslq	-128(%rbp), %rax
	cmpl	%eax, -164(%rbp)
	jg	.L560
	cmpl	$2147483647, -128(%rbp)
	je	.L399
.L416:
	movq	-176(%rbp), %rdi
	addl	$1, -128(%rbp)
	testq	%rdi, %rdi
	je	.L417
	movl	$1, %edx
	movl	$1, %esi
	movq	%r9, -136(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movq	-136(%rbp), %r9
.L417:
	addl	$1, %r13d
	jmp	.L413
.L556:
	movq	-184(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdi
	movq	-160(%rbp), %rsi
	call	u_memcpy_67@PLT
	movl	%r12d, -128(%rbp)
	jmp	.L388
.L557:
	movq	-120(%rbp), %rax
	movslq	%r13d, %r13
	movq	-184(%rbp), %rcx
	movl	%r12d, %edx
	leaq	(%rax,%r13,2), %rsi
	movslq	-128(%rbp), %rax
	leaq	(%rcx,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movl	%r14d, -128(%rbp)
	jmp	.L420
.L424:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L368
	movq	-192(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	jmp	.L368
.L560:
	movq	-184(%rbp), %rsi
	movl	$74, %edi
	movw	%di, (%rsi,%rax,2)
	jmp	.L416
.L404:
	movl	%esi, %r8d
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L409:
	movzwl	(%rax,%rdx,2), %edi
	movw	%di, (%rcx,%rdx,2)
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	%r8, %rdi
	jne	.L409
	jmp	.L408
.L438:
	movl	-128(%rbp), %r12d
	jmp	.L419
.L559:
	movq	-184(%rbp), %rsi
	movl	$1, %edx
	movq	%r9, -136(%rbp)
	leaq	(%rsi,%rax,2), %rdi
	movq	%r14, %rsi
	call	u_memcpy_67@PLT
	movq	-136(%rbp), %r9
	jmp	.L419
.L547:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3136:
	.size	ustrcase_internalToTitle_67, .-ustrcase_internalToTitle_67
	.p2align 4
	.globl	_ZN6icu_6710GreekUpper13getLetterDataEi
	.type	_ZN6icu_6710GreekUpper13getLetterDataEi, @function
_ZN6icu_6710GreekUpper13getLetterDataEi:
.LFB3137:
	.cfi_startproc
	endbr64
	leal	-880(%rdi), %eax
	cmpl	$7606, %eax
	ja	.L565
	leal	-1024(%rdi), %edx
	cmpl	$6911, %edx
	jbe	.L565
	cmpl	$1023, %edi
	jle	.L567
	cmpl	$8191, %edi
	jle	.L568
	cmpl	$8486, %edi
	movl	$5033, %edx
	movl	$0, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	leaq	_ZN6icu_6710GreekUpperL8data0370E(%rip), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	subl	$7936, %edi
	leaq	_ZN6icu_6710GreekUpperL8data1F00E(%rip), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	ret
	.cfi_endproc
.LFE3137:
	.size	_ZN6icu_6710GreekUpper13getLetterDataEi, .-_ZN6icu_6710GreekUpper13getLetterDataEi
	.p2align 4
	.globl	_ZN6icu_6710GreekUpper16getDiacriticDataEi
	.type	_ZN6icu_6710GreekUpper16getDiacriticDataEi, @function
_ZN6icu_6710GreekUpper16getDiacriticDataEi:
.LFB3138:
	.cfi_startproc
	endbr64
	subl	$768, %edi
	xorl	%eax, %eax
	cmpl	$69, %edi
	ja	.L569
	leaq	CSWTCH.121(%rip), %rax
	movl	(%rax,%rdi,4), %eax
.L569:
	ret
	.cfi_endproc
.LFE3138:
	.size	_ZN6icu_6710GreekUpper16getDiacriticDataEi, .-_ZN6icu_6710GreekUpper16getDiacriticDataEi
	.p2align 4
	.globl	_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKDsii
	.type	_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKDsii, @function
_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKDsii:
.LFB3139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L575:
	cmpl	%r12d, %ebx
	jge	.L576
	movslq	%ebx, %rax
	leal	1(%rbx), %edx
	movzwl	0(%r13,%rax,2), %edi
	leaq	(%rax,%rax), %rcx
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L578
	cmpl	%edx, %r12d
	jne	.L587
.L578:
	movl	%edx, %ebx
.L574:
	call	ucase_getTypeOrIgnorable_67@PLT
	testb	$4, %al
	jne	.L575
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	.cfi_restore_state
	movzwl	2(%r13,%rcx), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L578
	sall	$10, %edi
	addl	$2, %ebx
	leal	-56613888(%rax,%rdi), %edi
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L576:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3139:
	.size	_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKDsii, .-_ZN6icu_6710GreekUpper23isFollowedByCasedLetterEPKDsii
	.p2align 4
	.globl	_ZN6icu_6710GreekUpper7toUpperEjPDsiPKDsiPNS_5EditsER10UErrorCode
	.type	_ZN6icu_6710GreekUpper7toUpperEjPDsiPKDsiPNS_5EditsER10UErrorCode, @function
_ZN6icu_6710GreekUpper7toUpperEjPDsiPKDsiPNS_5EditsER10UErrorCode:
.LFB3140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rsi, -128(%rbp)
	movl	%edx, -108(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%r8d, -68(%rbp)
	movq	%r9, -96(%rbp)
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L663
	andl	$16384, %edi
	leaq	-64(%rbp), %rax
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	movl	%edi, -112(%rbp)
	movl	%r11d, %r15d
	movl	$0, -72(%rbp)
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L590:
	movslq	%r15d, %r14
	leal	1(%r15), %esi
	leaq	(%r14,%r14), %rax
	movq	-80(%rbp), %r14
	movq	%rax, -104(%rbp)
	addq	%rax, %r14
	movzwl	(%r14), %r13d
	movl	%r13d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L664
	cmpl	%esi, -68(%rbp)
	jne	.L792
.L664:
	movl	%esi, %ebx
.L591:
	movl	%r13d, %edi
	movl	%esi, -88(%rbp)
	call	ucase_getTypeOrIgnorable_67@PLT
	movl	-88(%rbp), %esi
	testb	$4, %al
	je	.L592
	movl	-72(%rbp), %r9d
	andl	$1, %r9d
.L593:
	leal	-880(%r13), %ecx
	leal	-1024(%r13), %eax
	cmpl	$7606, %ecx
	seta	%dil
	cmpl	$6911, %eax
	setbe	%al
	orb	%al, %dil
	movl	%edi, %r11d
	jne	.L594
	cmpl	$1023, %r13d
	jle	.L793
	cmpl	$8191, %r13d
	jle	.L794
	cmpl	$8486, %r13d
	je	.L795
.L594:
	movq	-120(%rbp), %rcx
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%r13d, %edi
	movl	$4, %r8d
	movl	%r9d, -88(%rbp)
	call	ucase_toFullUpper_67@PLT
	movl	%ebx, %esi
	movl	-88(%rbp), %r9d
	subl	%r15d, %esi
	testl	%eax, %eax
	movq	-64(%rbp), %r15
	movl	%eax, %r13d
	js	.L796
	cmpl	$31, %eax
	jle	.L675
	cmpl	%r12d, -108(%rbp)
	jle	.L643
	cmpl	$65535, %eax
	jg	.L677
	movq	-128(%rbp), %rcx
	movq	-96(%rbp), %rdi
	movslq	%r12d, %rax
	addl	$1, %r12d
	movw	%r13w, (%rcx,%rax,2)
	testq	%rdi, %rdi
	je	.L639
	movl	$1, %edx
	movl	%r9d, -88(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movl	-88(%rbp), %r9d
	.p2align 4,,10
	.p2align 3
.L639:
	movl	%r9d, -72(%rbp)
	movl	%ebx, %r15d
.L625:
	cmpl	%ebx, -68(%rbp)
	jg	.L590
.L588:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L797
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	movq	-104(%rbp), %rbx
	movq	-80(%rbp), %rax
	movzwl	2(%rax,%rbx), %eax
	movl	%esi, %ebx
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L591
	sall	$10, %r13d
	leal	2(%r15), %ebx
	leal	-56613888(%rax,%r13), %r13d
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L592:
	xorl	%r9d, %r9d
	testl	%eax, %eax
	setne	%r9b
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L796:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L638
	movl	%r9d, -72(%rbp)
	movl	%esi, -88(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movl	-72(%rbp), %r9d
	movl	-88(%rbp), %esi
.L638:
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jne	.L639
	movl	%r13d, %r14d
	notl	%r14d
	cmpl	%r12d, -108(%rbp)
	jle	.L674
	cmpl	$65535, %r14d
	jle	.L790
.L674:
	movl	%esi, %r13d
.L640:
	movl	$2147483647, %eax
	subl	%r12d, %eax
	cmpl	%r13d, %eax
	jl	.L657
	cmpl	%r12d, -108(%rbp)
	jle	.L645
	cmpl	$-1, %r14d
	je	.L646
	cmpl	$65535, %r14d
	jle	.L790
	cmpl	$1114111, %r14d
	jg	.L645
	leal	1(%r12), %eax
	cmpl	%eax, -108(%rbp)
	jle	.L645
	movl	%r14d, %eax
	movq	-128(%rbp), %rcx
	andw	$1023, %r14w
	movslq	%r12d, %rdx
	sarl	$10, %eax
	orw	$-9216, %r14w
	addl	$2, %r12d
	subw	$10304, %ax
	movw	%r14w, 2(%rcx,%rdx,2)
	movw	%ax, (%rcx,%rdx,2)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L645:
	addl	%r13d, %r12d
	testl	%r12d, %r12d
	jns	.L639
.L657:
	movq	-144(%rbp), %rax
	xorl	%r12d, %r12d
	movl	$8, (%rax)
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L793:
	movslq	%ecx, %rcx
	leaq	_ZN6icu_6710GreekUpperL8data0370E(%rip), %rax
	movzwl	(%rax,%rcx,2), %ecx
.L596:
	testl	%ecx, %ecx
	je	.L594
	movl	%ecx, %r13d
	movl	%ecx, %eax
	shrl	$13, %r13d
	andl	$1023, %eax
	andl	$1, %r13d
	testb	$16, %ch
	jne	.L798
.L599:
	cmpl	-68(%rbp), %ebx
	jge	.L603
.L602:
	movslq	%ebx, %r8
.L605:
	movq	-80(%rbp), %rdi
	movl	%r8d, %ebx
	movzwl	(%rdi,%r8,2), %edx
	subl	$768, %edx
	cmpl	$69, %edx
	ja	.L603
	leaq	CSWTCH.121(%rip), %rdi
	movl	(%rdi,%rdx,4), %edx
	testl	%edx, %edx
	je	.L603
	orl	%edx, %ecx
	andl	$8192, %edx
	cmpl	$1, %edx
	sbbl	$-1, %r13d
	addl	$1, %ebx
	addq	$1, %r8
	cmpl	%r8d, -68(%rbp)
	jg	.L605
	.p2align 4,,10
	.p2align 3
.L603:
	movl	%ecx, %r8d
	movl	%r9d, %edi
	andl	$53248, %r8d
	orl	$2, %edi
	cmpl	$20480, %r8d
	cmove	%edi, %r9d
	cmpl	$919, %eax
	je	.L660
	testb	$-128, %ch
	je	.L669
.L612:
	cmpl	$921, %eax
	jne	.L614
	movl	%r9d, -72(%rbp)
	andl	$-98305, %ecx
	xorl	%edi, %edi
	movl	$938, %eax
.L613:
	cmpq	$0, -96(%rbp)
	je	.L799
.L615:
	movzwl	(%r14), %edx
	cmpl	%eax, %edx
	setne	%r8b
	testl	%r13d, %r13d
	setne	%dl
	orl	%r8d, %edx
	testl	$98304, %ecx
	jne	.L617
	movl	%edx, %r8d
	movl	$1, %edx
.L618:
	testb	%dil, %dil
	je	.L620
	cmpl	%ebx, %esi
	jge	.L671
	movq	-80(%rbp), %rdi
	movslq	%esi, %rdx
	cmpw	$769, (%rdi,%rdx,2)
	setne	%dl
	orl	%edx, %r8d
.L621:
	leal	1(%rsi), %edx
	subl	%r15d, %edx
.L620:
	movl	%ebx, %esi
	addl	%r13d, %edx
	subl	%r15d, %esi
	cmpl	%edx, %esi
	setne	%dil
	orb	%r8b, %dil
	movq	-96(%rbp), %rdi
	je	.L622
	testq	%rdi, %rdi
	je	.L616
	movl	%ecx, -132(%rbp)
	movl	%eax, -104(%rbp)
	movb	%r11b, -88(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movzbl	-88(%rbp), %r11d
	movl	-104(%rbp), %eax
	movl	-132(%rbp), %ecx
.L616:
	cmpl	%r12d, -108(%rbp)
	jle	.L626
.L802:
	movq	-128(%rbp), %rsi
	movslq	%r12d, %rdx
	movw	%ax, (%rsi,%rdx,2)
.L627:
	andl	$98304, %ecx
	leal	1(%r12), %edx
	je	.L659
	cmpl	%edx, -108(%rbp)
	jg	.L800
	cmpl	$2147483647, %edx
	je	.L657
.L629:
	leal	2(%r12), %edx
.L659:
	testb	%r11b, %r11b
	je	.L631
	cmpl	-108(%rbp), %edx
	jge	.L632
	movq	-128(%rbp), %rcx
	movslq	%edx, %rax
	movl	$769, %esi
	movw	%si, (%rcx,%rax,2)
.L633:
	addl	$1, %edx
.L631:
	testl	%r13d, %r13d
	je	.L672
	movq	-128(%rbp), %rcx
	movslq	%edx, %rax
	movl	%edx, %r12d
	addl	%edx, %r13d
	leaq	(%rcx,%rax,2), %rax
	movl	-108(%rbp), %ecx
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L801:
	movl	$921, %edx
	movw	%dx, (%rax)
.L635:
	addl	$1, %r12d
	movl	%r13d, %edx
	addq	$2, %rax
	subl	%r12d, %edx
	testl	%edx, %edx
	jle	.L673
.L636:
	cmpl	%r12d, %ecx
	jg	.L801
	cmpl	$2147483647, %r12d
	jne	.L635
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L622:
	testq	%rdi, %rdi
	je	.L624
	movl	%ecx, -132(%rbp)
	movl	%eax, -104(%rbp)
	movb	%r11b, -88(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movl	-132(%rbp), %ecx
	movl	-104(%rbp), %eax
	movzbl	-88(%rbp), %r11d
.L624:
	movl	-112(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L616
.L673:
	movl	%ebx, %r15d
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L617:
	movl	$1, %r8d
	cmpl	%ebx, %esi
	jge	.L619
	movq	-80(%rbp), %rsi
	movq	-104(%rbp), %r10
	cmpw	$776, 2(%rsi,%r10)
	setne	%r8b
	orl	%edx, %r8d
.L619:
	leal	2(%r15), %esi
	movl	$2, %edx
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L675:
	movl	$-1, %r14d
.L642:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L640
	movl	%r13d, %edx
	movl	%r9d, -88(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movl	-88(%rbp), %r9d
	jmp	.L640
.L679:
	xorl	%r13d, %r13d
	movl	$937, %eax
	movl	$5033, %ecx
	.p2align 4,,10
	.p2align 3
.L669:
	xorl	%edi, %edi
	cmpq	$0, -96(%rbp)
	movl	%r9d, -72(%rbp)
	jne	.L615
.L799:
	movl	-112(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L615
	cmpl	%r12d, -108(%rbp)
	jg	.L802
.L626:
	cmpl	$2147483647, %r12d
	jne	.L627
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L798:
	testb	$2, -72(%rbp)
	je	.L599
	cmpl	$921, %eax
	je	.L680
	cmpl	$933, %eax
	jne	.L599
.L680:
	orb	$-128, %ch
	cmpl	-68(%rbp), %ebx
	jl	.L602
	cmpl	$919, %eax
	jne	.L612
	.p2align 4,,10
	.p2align 3
.L660:
	testb	$64, %ch
	jne	.L803
.L608:
	movl	%r9d, -72(%rbp)
	xorl	%edi, %edi
	movl	$919, %eax
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L794:
	leal	-7936(%r13), %eax
	leaq	_ZN6icu_6710GreekUpperL8data1F00E(%rip), %rcx
	cltq
	movzwl	(%rcx,%rax,2), %ecx
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L790:
	movq	-128(%rbp), %rcx
	movslq	%r12d, %rax
	addl	$1, %r12d
	movw	%r14w, (%rcx,%rax,2)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L671:
	movl	%edi, %r8d
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L614:
	cmpl	$933, %eax
	jne	.L669
	movl	%r9d, -72(%rbp)
	andl	$-98305, %ecx
	xorl	%edi, %edi
	movl	$939, %eax
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L646:
	leal	0(%r13,%r12), %eax
	cmpl	%eax, -108(%rbp)
	jl	.L678
	testl	%r13d, %r13d
	jle	.L639
	movq	-128(%rbp), %rdi
	movslq	%r12d, %rax
	leaq	16(%r15), %rsi
	leal	-1(%r13), %ecx
	leaq	(%rdi,%rax,2), %rdx
	leaq	16(%rdi,%rax,2), %rax
	cmpq	%rsi, %rdx
	setnb	%sil
	cmpq	%rax, %r15
	setnb	%al
	orb	%al, %sil
	je	.L651
	cmpl	$6, %ecx
	jbe	.L651
	movl	%r13d, %esi
	xorl	%eax, %eax
	shrl	$3, %esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L653:
	movdqu	(%r15,%rax), %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L653
	movl	%r13d, %esi
	movl	%r13d, %edx
	andl	$-8, %esi
	andl	$7, %edx
	movl	%esi, %eax
	leal	(%rsi,%r12), %edi
	leaq	(%r15,%rax,2), %rax
	cmpl	%r13d, %esi
	je	.L655
	movzwl	(%rax), %r8d
	movq	-128(%rbp), %r10
	leal	1(%rdi), %esi
	movslq	%edi, %rdi
	movw	%r8w, (%r10,%rdi,2)
	cmpl	$1, %edx
	je	.L655
	movzwl	2(%rax), %r8d
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi), %rdi
	movw	%r8w, (%r10,%rsi,2)
	cmpl	$2, %edx
	je	.L655
	movzwl	4(%rax), %esi
	movw	%si, 2(%r10,%rdi)
	cmpl	$3, %edx
	je	.L655
	movzwl	6(%rax), %esi
	movw	%si, 4(%r10,%rdi)
	cmpl	$4, %edx
	je	.L655
	movzwl	8(%rax), %esi
	movw	%si, 6(%r10,%rdi)
	cmpl	$5, %edx
	je	.L655
	movzwl	10(%rax), %esi
	movw	%si, 8(%r10,%rdi)
	cmpl	$6, %edx
	je	.L655
	movzwl	12(%rax), %eax
	movw	%ax, 10(%r10,%rdi)
.L655:
	leal	1(%r12,%rcx), %r12d
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L795:
	cmpl	-68(%rbp), %ebx
	jge	.L679
	movl	$937, %eax
	xorl	%r13d, %r13d
	movl	$5033, %ecx
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L677:
	movl	%r13d, %r14d
	movl	$2, %r13d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L643:
	cmpl	$65535, %eax
	jg	.L677
	movl	%eax, %r14d
	movl	$1, %r13d
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L803:
	movl	-72(%rbp), %eax
	andl	$1, %eax
	orl	%r13d, %eax
	jne	.L608
	movl	%r9d, -72(%rbp)
	movl	%esi, -132(%rbp)
	movb	%r11b, -153(%rbp)
	movl	%ebx, -136(%rbp)
	movl	%ecx, -148(%rbp)
	movq	%r14, -88(%rbp)
	movq	-80(%rbp), %r14
	movl	%r13d, -152(%rbp)
	movl	-68(%rbp), %r13d
	.p2align 4,,10
	.p2align 3
.L611:
	cmpl	%ebx, %r13d
	jle	.L788
	movslq	%ebx, %rax
	leal	1(%rbx), %ecx
	movzwl	(%r14,%rax,2), %edi
	leaq	(%rax,%rax), %rsi
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L667
	cmpl	%ecx, %r13d
	jne	.L804
.L667:
	movl	%ecx, %ebx
.L610:
	call	ucase_getTypeOrIgnorable_67@PLT
	testb	$4, %al
	jne	.L611
	movq	-88(%rbp), %r14
	movl	-72(%rbp), %r9d
	movl	-152(%rbp), %r13d
	movl	-132(%rbp), %esi
	movzbl	-153(%rbp), %r11d
	movl	-136(%rbp), %ebx
	movl	-148(%rbp), %ecx
	testl	%eax, %eax
	jne	.L608
.L609:
	xorl	%r13d, %r13d
	cmpl	%ebx, %r15d
	movl	$919, %eax
	movl	%r9d, -72(%rbp)
	setne	%r11b
	movl	$905, %r8d
	cmove	%r8d, %eax
	movl	%r11d, %edi
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L632:
	cmpl	$2147483647, %edx
	jne	.L633
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L678:
	movl	%eax, %r12d
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L800:
	movq	-128(%rbp), %rax
	movslq	%edx, %rdx
	movl	$776, %edi
	movw	%di, (%rax,%rdx,2)
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L672:
	movl	%ebx, %r15d
	movl	%edx, %r12d
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L804:
	movzwl	2(%r14,%rsi), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L667
	sall	$10, %edi
	addl	$2, %ebx
	leal	-56613888(%rax,%rdi), %edi
	jmp	.L610
.L788:
	movq	-88(%rbp), %r14
	movl	-72(%rbp), %r9d
	movl	-132(%rbp), %esi
	movl	-136(%rbp), %ebx
	movl	-148(%rbp), %ecx
	jmp	.L609
.L651:
	movl	%ecx, %edi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L656:
	movzwl	(%r15,%rax,2), %esi
	movw	%si, (%rdx,%rax,2)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rdi, %rsi
	jne	.L656
	jmp	.L655
.L663:
	xorl	%r12d, %r12d
	jmp	.L588
.L797:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3140:
	.size	_ZN6icu_6710GreekUpper7toUpperEjPDsiPKDsiPNS_5EditsER10UErrorCode, .-_ZN6icu_6710GreekUpper7toUpperEjPDsiPKDsiPNS_5EditsER10UErrorCode
	.p2align 4
	.globl	ustrcase_internalToLower_67
	.type	ustrcase_internalToLower_67, @function
ustrcase_internalToLower_67:
.LFB3141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rdx
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%r8d, %ebx
	movq	%r9, %r8
	subq	$48, %rsp
	movl	16(%rbp), %eax
	movq	32(%rbp), %r13
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	movq	24(%rbp), %r14
	movq	%r9, -80(%rbp)
	movl	%ebx, %ecx
	pushq	%r13
	leaq	-80(%rbp), %r9
	pushq	%r14
	pushq	%rax
	pushq	$0
	movups	%xmm0, -72(%rbp)
	movq	$0, -56(%rbp)
	movl	%eax, -64(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_17toLowerEijPDsiPKDsP12UCaseContextiiPNS_5EditsER10UErrorCode
	addq	$32, %rsp
	movl	%eax, %r12d
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L813
.L805:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L814
	leaq	-32(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L813:
	.cfi_restore_state
	cmpl	%r12d, %ebx
	jge	.L807
	movl	$15, 0(%r13)
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L807:
	testq	%r14, %r14
	je	.L805
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	jmp	.L805
.L814:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3141:
	.size	ustrcase_internalToLower_67, .-ustrcase_internalToLower_67
	.p2align 4
	.globl	ustrcase_internalToUpper_67
	.type	ustrcase_internalToUpper_67, @function
ustrcase_internalToUpper_67:
.LFB3142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	32(%rbp), %rax
	movl	%r8d, -116(%rbp)
	movl	%edi, -140(%rbp)
	movq	24(%rbp), %r13
	movq	%rcx, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%rax, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, %edi
	je	.L948
	movl	16(%rbp), %eax
	pxor	%xmm0, %xmm0
	movq	%r9, -96(%rbp)
	leaq	_ZN6icu_679LatinCase11TO_UPPER_TRE(%rip), %rdx
	cmpl	$2, -140(%rbp)
	movups	%xmm0, -88(%rbp)
	movl	%eax, -80(%rbp)
	leaq	_ZN6icu_679LatinCase15TO_UPPER_NORMALE(%rip), %rax
	cmove	%rdx, %rax
	movq	$0, -72(%rbp)
	andl	$16384, %r12d
	xorl	%r15d, %r15d
	movq	%rax, -192(%rbp)
	call	ucase_getTrie_67@PLT
	movl	%r12d, -144(%rbp)
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
	movq	%rax, -184(%rbp)
	movl	16(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -196(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -208(%rbp)
.L850:
	cmpl	%r12d, 16(%rbp)
	jle	.L819
	movl	-196(%rbp), %eax
	movslq	%r12d, %r8
	movq	%r13, %rcx
	subl	%r12d, %eax
	movq	%r8, %r12
	leaq	1(%r8,%rax), %rax
	movq	%rax, -160(%rbp)
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L951:
	movq	-192(%rbp), %rsi
	movzwl	%bx, %eax
	movsbl	(%rsi,%rax), %r14d
	cmpb	$-128, %r14b
	je	.L822
	testb	%r14b, %r14b
	je	.L823
.L824:
	subl	%r9d, %edx
	movl	%edx, %r13d
	testl	%edx, %edx
	jle	.L827
	testq	%rcx, %rcx
	je	.L828
	movq	%rcx, %rdi
	movl	%edx, %esi
	movl	%r9d, -176(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movslq	-176(%rbp), %r9
	movq	-168(%rbp), %rcx
.L828:
	movl	-144(%rbp), %esi
	testl	%esi, %esi
	jne	.L827
	movl	$2147483647, %eax
	subl	%r15d, %eax
	cmpl	%eax, %r13d
	jg	.L946
	leal	0(%r13,%r15), %r10d
	cmpl	%r10d, -116(%rbp)
	jge	.L949
	movl	%r10d, %r15d
.L827:
	cmpl	%r15d, -116(%rbp)
	jle	.L950
	movq	-136(%rbp), %rax
	movslq	%r15d, %rdx
	addl	%r14d, %ebx
	movw	%bx, (%rax,%rdx,2)
.L830:
	movl	-120(%rbp), %ebx
	addl	$1, %r15d
	movslq	%ebx, %r9
	testq	%rcx, %rcx
	je	.L823
	movq	%rcx, %rdi
	movl	$1, %edx
	movl	$1, %esi
	movq	%rcx, -168(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movq	-168(%rbp), %rcx
	movslq	%ebx, %r9
.L823:
	addq	$1, %r12
	cmpq	%r12, -160(%rbp)
	je	.L941
.L820:
	movq	-128(%rbp), %rax
	movl	%r12d, %edx
	movzwl	(%rax,%r12,2), %ebx
	leal	1(%r12), %eax
	movl	%eax, -120(%rbp)
	cmpw	$382, %bx
	jbe	.L951
	movzwl	%bx, %edi
	cmpw	$-10241, %bx
	ja	.L825
	movq	-184(%rbp), %rax
	movq	(%rax), %rsi
	movl	%edi, %eax
	sarl	$5, %eax
	cltq
	movzwl	(%rsi,%rax,2), %r10d
	movl	%ebx, %eax
	andl	$31, %eax
	leal	(%rax,%r10,4), %eax
	cltq
	movswl	(%rsi,%rax,2), %eax
	testb	$8, %al
	jne	.L942
	movl	%eax, %esi
	andl	$3, %esi
	cmpw	$1, %si
	jne	.L823
	sarl	$7, %eax
	movl	%eax, %r14d
	jne	.L824
	addq	$1, %r12
	cmpq	%r12, -160(%rbp)
	jne	.L820
	.p2align 4,,10
	.p2align 3
.L941:
	movl	-120(%rbp), %r12d
	movq	%rcx, %r13
.L819:
	subl	%r9d, %r12d
	testl	%r12d, %r12d
	jle	.L945
	testq	%r13, %r13
	je	.L851
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%r9d, -120(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movl	-120(%rbp), %r9d
.L851:
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	jne	.L945
	movl	$2147483647, %eax
	subl	%r15d, %eax
	cmpl	%eax, %r12d
	jg	.L946
	leal	(%r12,%r15), %ebx
	cmpl	%ebx, -116(%rbp)
	jge	.L855
.L944:
	movq	-152(%rbp), %rax
	movl	%ebx, %r15d
	movl	(%rax), %eax
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L950:
	cmpl	$2147483647, %r15d
	jne	.L830
	testq	%rcx, %rcx
	je	.L946
	movl	$1, %edx
	movl	$1, %esi
	movq	%rcx, %rdi
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	.p2align 4,,10
	.p2align 3
.L946:
	movq	-152(%rbp), %rax
	xorl	%r15d, %r15d
	movl	$8, (%rax)
.L815:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L952
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L825:
	.cfi_restore_state
	movl	%edi, %eax
	movl	%r12d, %r14d
	movl	%edx, -76(%rbp)
	movl	-120(%rbp), %r12d
	andl	$-1024, %eax
	movq	%rcx, %r13
	cmpl	$55296, %eax
	jne	.L834
	cmpl	%r12d, 16(%rbp)
	jg	.L953
.L834:
	movl	-140(%rbp), %r8d
	leaq	-96(%rbp), %rdx
	movl	%r9d, -120(%rbp)
	leaq	_ZN6icu_6712_GLOBAL__N_125utf16_caseContextIteratorEPva(%rip), %rsi
	movq	-208(%rbp), %rcx
	movl	%r12d, -72(%rbp)
	call	ucase_toFullUpper_67@PLT
	movslq	-120(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L850
	movl	%r14d, %edx
	subl	%r9d, %edx
	testl	%edx, %edx
	jle	.L836
	testq	%r13, %r13
	je	.L837
	movl	%edx, %esi
	movq	%r13, %rdi
	movl	%r9d, -160(%rbp)
	movl	%edx, -120(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movslq	-160(%rbp), %r9
	movl	-120(%rbp), %edx
.L837:
	movl	-144(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L836
	movl	$2147483647, %eax
	subl	%r15d, %eax
	cmpl	%eax, %edx
	jg	.L946
	leal	(%rdx,%r15), %ecx
	cmpl	%ecx, -116(%rbp)
	jge	.L954
	movl	%ecx, %r15d
.L836:
	movl	%r12d, %esi
	subl	%r14d, %esi
	cmpl	$31, %ebx
	jle	.L873
.L956:
	cmpl	%r15d, -116(%rbp)
	jle	.L838
	cmpl	$65535, %ebx
	jg	.L870
	movq	-136(%rbp), %rax
	leal	1(%r15), %r14d
	movslq	%r15d, %r15
	movw	%bx, (%rax,%r15,2)
	testq	%r13, %r13
	je	.L869
	movl	$1, %edx
	movq	%r13, %rdi
	movl	%r14d, %r15d
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movslq	%r12d, %r9
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L949:
	movq	-128(%rbp), %rax
	movslq	%r15d, %r15
	movl	%r13d, %edx
	movq	%rcx, -176(%rbp)
	movl	%r10d, -168(%rbp)
	leaq	(%rax,%r9,2), %rsi
	movq	-136(%rbp), %rax
	leaq	(%rax,%r15,2), %rdi
	call	u_memcpy_67@PLT
	movl	-168(%rbp), %r10d
	movq	-176(%rbp), %rcx
	movl	%r10d, %r15d
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L822:
	movl	-120(%rbp), %r12d
	movl	%edx, %r14d
	movq	%rcx, %r13
	movzwl	%bx, %edi
.L826:
	movl	%r14d, -76(%rbp)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L942:
	movl	-120(%rbp), %r12d
	movl	%edx, %r14d
	movq	%rcx, %r13
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L873:
	movl	$-1, %ecx
.L839:
	movq	-104(%rbp), %r14
	testq	%r13, %r13
	je	.L840
	movl	%ebx, %edx
	movq	%r13, %rdi
	movl	%ecx, -120(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movl	-120(%rbp), %ecx
.L840:
	movl	$2147483647, %eax
	subl	%r15d, %eax
	cmpl	%eax, %ebx
	jg	.L946
	cmpl	%r15d, -116(%rbp)
	jle	.L841
	cmpl	$-1, %ecx
	je	.L842
	cmpl	$65535, %ecx
	jg	.L843
	movq	-136(%rbp), %rbx
	movslq	%r15d, %rax
	movslq	%r12d, %r9
	addl	$1, %r15d
	movw	%cx, (%rbx,%rax,2)
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L953:
	movq	-128(%rbp), %rbx
	movslq	%r12d, %rax
	movzwl	(%rbx,%rax,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L834
	sall	$10, %edi
	leal	2(%r14), %r12d
	leal	-56613888(%rax,%rdi), %edi
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L948:
	movq	-152(%rbp), %rbx
	movl	%r8d, %edx
	movq	%r9, %rcx
	movl	%r12d, %edi
	subq	$8, %rsp
	movl	16(%rbp), %r8d
	movq	-136(%rbp), %rsi
	movq	%r13, %r9
	pushq	%rbx
	call	_ZN6icu_6710GreekUpper7toUpperEjPDsiPKDsiPNS_5EditsER10UErrorCode
	popq	%rdi
	popq	%r8
	movl	%eax, %r15d
	movl	(%rbx), %eax
.L817:
	testl	%eax, %eax
	jg	.L815
	cmpl	-116(%rbp), %r15d
	jle	.L856
	movq	-152(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L843:
	cmpl	$1114111, %ecx
	jle	.L955
	.p2align 4,,10
	.p2align 3
.L841:
	addl	%ebx, %r15d
	movslq	%r12d, %r9
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L856:
	testq	%r13, %r13
	je	.L815
	movq	-152(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L954:
	movq	-128(%rbp), %rax
	movslq	%r15d, %r15
	movl	%ecx, -120(%rbp)
	leaq	(%rax,%r9,2), %rsi
	movq	-136(%rbp), %rax
	leaq	(%rax,%r15,2), %rdi
	call	u_memcpy_67@PLT
	movl	-120(%rbp), %ecx
	movl	%r12d, %esi
	subl	%r14d, %esi
	movl	%ecx, %r15d
	cmpl	$31, %ebx
	jle	.L873
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L842:
	leal	(%rbx,%r15), %eax
	cmpl	%eax, -116(%rbp)
	jl	.L871
	testl	%ebx, %ebx
	je	.L872
	movq	-136(%rbp), %rdi
	movslq	%r15d, %rax
	leaq	16(%r14), %rcx
	leal	-1(%rbx), %esi
	leaq	(%rdi,%rax,2), %rdx
	leaq	16(%rdi,%rax,2), %rax
	cmpq	%rcx, %rdx
	setnb	%cl
	cmpq	%rax, %r14
	setnb	%al
	orb	%al, %cl
	je	.L845
	cmpl	$6, %esi
	jbe	.L845
	movdqu	(%r14), %xmm1
	movl	%ebx, %eax
	shrl	$3, %eax
	movups	%xmm1, (%rdx)
	cmpl	$1, %eax
	je	.L846
	movdqu	16(%r14), %xmm2
	movups	%xmm2, 16(%rdx)
	cmpl	$2, %eax
	je	.L846
	movdqu	32(%r14), %xmm3
	movups	%xmm3, 32(%rdx)
.L846:
	movl	%ebx, %edi
	movl	%ebx, %edx
	andl	$-8, %edi
	andl	$7, %edx
	movl	%edi, %eax
	leal	(%rdi,%r15), %ecx
	leaq	(%r14,%rax,2), %rax
	cmpl	%ebx, %edi
	je	.L848
	movzwl	(%rax), %r8d
	movq	-136(%rbp), %rbx
	leal	1(%rcx), %edi
	movslq	%ecx, %rcx
	movw	%r8w, (%rbx,%rcx,2)
	cmpl	$1, %edx
	je	.L848
	movzwl	2(%rax), %r8d
	movslq	%edi, %rcx
	leaq	(%rcx,%rcx), %rdi
	movw	%r8w, (%rbx,%rcx,2)
	cmpl	$2, %edx
	je	.L848
	movzwl	4(%rax), %ecx
	movw	%cx, 2(%rbx,%rdi)
	cmpl	$3, %edx
	je	.L848
	movzwl	6(%rax), %ecx
	movw	%cx, 4(%rbx,%rdi)
	cmpl	$4, %edx
	je	.L848
	movzwl	8(%rax), %ecx
	movw	%cx, 6(%rbx,%rdi)
	cmpl	$5, %edx
	je	.L848
	movzwl	10(%rax), %ecx
	movw	%cx, 8(%rbx,%rdi)
	cmpl	$6, %edx
	je	.L848
	movzwl	12(%rax), %eax
	movw	%ax, 10(%rbx,%rdi)
.L848:
	leal	1(%r15,%rsi), %r15d
	movslq	%r12d, %r9
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L945:
	movq	-152(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L838:
	cmpl	$65535, %ebx
	jle	.L957
	.p2align 4,,10
	.p2align 3
.L870:
	movl	%ebx, %ecx
	movl	$2, %ebx
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L957:
	movl	%ebx, %ecx
	movl	$1, %ebx
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L871:
	movslq	%r12d, %r9
	movl	%eax, %r15d
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L955:
	leal	1(%r15), %eax
	cmpl	%eax, -116(%rbp)
	jle	.L841
	movl	%ecx, %eax
	movq	-136(%rbp), %rbx
	movslq	%r15d, %rdx
	movslq	%r12d, %r9
	sarl	$10, %eax
	addl	$2, %r15d
	subw	$10304, %ax
	movw	%ax, (%rbx,%rdx,2)
	movl	%ecx, %eax
	andw	$1023, %ax
	orw	$-9216, %ax
	movw	%ax, 2(%rbx,%rdx,2)
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L869:
	movslq	%r12d, %r9
	movl	%r14d, %r15d
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L855:
	movq	-128(%rbp), %rax
	movslq	%r9d, %r11
	movslq	%r15d, %r9
	movl	%r12d, %edx
	leaq	(%rax,%r11,2), %rsi
	movq	-136(%rbp), %rax
	leaq	(%rax,%r9,2), %rdi
	call	u_memcpy_67@PLT
	jmp	.L944
.L845:
	movl	%esi, %edi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L849:
	movzwl	(%r14,%rax,2), %ecx
	movw	%cx, (%rdx,%rax,2)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdi, %rcx
	jne	.L849
	jmp	.L848
.L872:
	movslq	%r12d, %r9
	jmp	.L850
.L952:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3142:
	.size	ustrcase_internalToUpper_67, .-ustrcase_internalToUpper_67
	.p2align 4
	.globl	ustrcase_map_67
	.type	ustrcase_map_67, @function
ustrcase_map_67:
.LFB3144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rbp), %r15
	movl	16(%rbp), %eax
	movq	24(%rbp), %rbx
	movq	32(%rbp), %rcx
	movl	(%r15), %r10d
	testl	%r10d, %r10d
	jg	.L958
	movl	%r8d, %r12d
	testl	%r8d, %r8d
	js	.L961
	movl	%edi, %r14d
	testq	%r13, %r13
	jne	.L968
	testl	%r8d, %r8d
	jle	.L968
.L961:
	movl	$1, (%r15)
.L958:
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L968:
	.cfi_restore_state
	testq	%r9, %r9
	je	.L961
	cmpl	$-1, %eax
	jl	.L961
	je	.L979
.L963:
	testq	%r13, %r13
	je	.L964
	cmpq	%r9, %r13
	jbe	.L980
.L965:
	movslq	%eax, %rdi
	leaq	(%r9,%rdi,2), %rdi
	cmpq	%rdi, %r13
	jb	.L961
.L964:
	testq	%rcx, %rcx
	je	.L966
	testl	$8192, %esi
	je	.L981
.L966:
	subq	$8, %rsp
	movl	%r14d, %edi
	movl	%r12d, %r8d
	pushq	%r15
	pushq	%rcx
	movq	%r13, %rcx
	pushq	%rax
	call	*%rbx
	movq	%r15, %rcx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%eax, %edx
	addq	$32, %rsp
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L980:
	.cfi_restore_state
	movslq	%r12d, %rdi
	leaq	0(%r13,%rdi,2), %rdi
	cmpq	%rdi, %r9
	jb	.L961
	cmpq	%r9, %r13
	jne	.L964
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L979:
	movq	%r9, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movl	%esi, -60(%rbp)
	movq	%r9, -56(%rbp)
	call	u_strlen_67@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movl	-60(%rbp), %esi
	movq	-56(%rbp), %r9
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L981:
	movq	%rcx, %rdi
	movl	%eax, -64(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movl	%esi, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN6icu_675Edits5resetEv@PLT
	movl	-64(%rbp), %eax
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rdx
	movl	-60(%rbp), %esi
	movq	-56(%rbp), %rcx
	jmp	.L966
	.cfi_endproc
.LFE3144:
	.size	ustrcase_map_67, .-ustrcase_map_67
	.p2align 4
	.globl	ustrcase_mapWithOverlap_67
	.type	ustrcase_mapWithOverlap_67, @function
ustrcase_mapWithOverlap_67:
.LFB3145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	32(%rbp), %rax
	movl	16(%rbp), %r10d
	movq	24(%rbp), %r11
	cmpl	$0, (%rax)
	jg	.L982
	testl	%r8d, %r8d
	js	.L984
	testq	%rcx, %rcx
	jne	.L989
	testl	%r8d, %r8d
	jle	.L989
.L984:
	movl	$1, (%rax)
.L982:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L989:
	.cfi_restore_state
	testq	%r9, %r9
	je	.L984
	cmpl	$-1, %r10d
	jl	.L984
	movq	%rax, 32(%rbp)
	movq	%r11, 24(%rbp)
	movl	%r10d, 16(%rbp)
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ustrcase_mapWithOverlap_67.part.0
	.cfi_endproc
.LFE3145:
	.size	ustrcase_mapWithOverlap_67, .-ustrcase_mapWithOverlap_67
	.p2align 4
	.globl	u_strFoldCase_67
	.type	u_strFoldCase_67, @function
u_strFoldCase_67:
.LFB3146:
	.cfi_startproc
	endbr64
	movl	(%r9), %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L1006
	testl	%esi, %esi
	js	.L996
	movl	%r8d, %r10d
	testq	%rdi, %rdi
	jne	.L1001
	testl	%esi, %esi
	jle	.L1001
.L996:
	movl	$1, (%r9)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	testq	%rdx, %rdx
	je	.L996
	cmpl	$-1, %ecx
	jl	.L996
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	ustrcase_internalFold_67(%rip), %rax
	movl	%esi, %r8d
	movl	%r10d, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r9
	movq	%rdx, %r9
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rcx
	movq	%rdi, %rcx
	movl	$1, %edi
	call	ustrcase_mapWithOverlap_67.part.0
	addq	$32, %rsp
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1006:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE3146:
	.size	u_strFoldCase_67, .-u_strFoldCase_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap4foldEjPKDsiPDsiPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap4foldEjPKDsiPDsiPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap4foldEjPKDsiPDsiPNS_5EditsER10UErrorCode:
.LFB3147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	16(%rbp), %r10
	movl	(%r10), %r11d
	testl	%r11d, %r11d
	jg	.L1008
	movl	%r8d, %r12d
	testl	%r8d, %r8d
	js	.L1011
	movl	%edi, %r14d
	movq	%rcx, %r13
	movq	%r9, %r15
	testq	%rcx, %rcx
	jne	.L1020
	testl	%r8d, %r8d
	jle	.L1020
.L1011:
	movl	$1, (%r10)
.L1008:
	leaq	-32(%rbp), %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1020:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L1011
	cmpl	$-1, %edx
	jl	.L1011
	je	.L1034
.L1013:
	testq	%r13, %r13
	je	.L1014
	cmpq	%rsi, %r13
	jbe	.L1035
.L1015:
	movslq	%edx, %rax
	leaq	(%rsi,%rax,2), %rax
	cmpq	%rax, %r13
	jb	.L1011
.L1014:
	testq	%r15, %r15
	je	.L1016
	testl	$8192, %r14d
	je	.L1036
.L1016:
	pushq	%r10
	movq	%rsi, %r8
	xorl	%r9d, %r9d
	movl	%r12d, %ecx
	pushq	%r15
	movl	%r14d, %esi
	movl	$-1, %edi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	movq	%r10, -40(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_17toLowerEijPDsiPKDsP12UCaseContextiiPNS_5EditsER10UErrorCode
	movq	-40(%rbp), %r10
	addq	$32, %rsp
	movl	%eax, %edx
	movl	(%r10), %eax
	testl	%eax, %eax
	jle	.L1037
.L1017:
	leaq	-32(%rbp), %rsp
	movl	%r12d, %esi
	movq	%r13, %rdi
	movq	%r10, %rcx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L1035:
	.cfi_restore_state
	movslq	%r12d, %rax
	leaq	0(%r13,%rax,2), %rax
	cmpq	%rax, %rsi
	jb	.L1011
	cmpq	%rsi, %r13
	jne	.L1014
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1037:
	cmpl	%edx, %r12d
	jl	.L1038
	testq	%r15, %r15
	je	.L1017
	movq	%r10, %rsi
	movq	%r15, %rdi
	movl	%edx, -48(%rbp)
	movq	%r10, -40(%rbp)
	call	_ZNK6icu_675Edits11copyErrorToER10UErrorCode@PLT
	movl	-48(%rbp), %edx
	movq	-40(%rbp), %r10
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1038:
	movl	$15, (%r10)
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	%rsi, %rdi
	movq	%r10, -48(%rbp)
	movq	%rsi, -40(%rbp)
	call	u_strlen_67@PLT
	movq	-48(%rbp), %r10
	movq	-40(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	%r15, %rdi
	movq	%r10, -56(%rbp)
	movl	%edx, -48(%rbp)
	movq	%rsi, -40(%rbp)
	call	_ZN6icu_675Edits5resetEv@PLT
	movq	-56(%rbp), %r10
	movl	-48(%rbp), %edx
	movq	-40(%rbp), %rsi
	jmp	.L1016
	.cfi_endproc
.LFE3147:
	.size	_ZN6icu_677CaseMap4foldEjPKDsiPDsiPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap4foldEjPKDsiPDsiPNS_5EditsER10UErrorCode
	.p2align 4
	.globl	u_strcmpFold_67
	.type	u_strcmpFold_67, @function
u_strcmpFold_67:
.LFB3149:
	.cfi_startproc
	endbr64
	movl	(%r9), %r9d
	testl	%r9d, %r9d
	jg	.L1042
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	_ZL8_cmpFoldPKDsiS0_ijPiS1_P10UErrorCode.isra.0.part.0
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1042:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3149:
	.size	u_strcmpFold_67, .-u_strcmpFold_67
	.p2align 4
	.globl	u_strCaseCompare_67
	.type	u_strCaseCompare_67, @function
u_strCaseCompare_67:
.LFB3150:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L1052
	movl	(%r9), %r10d
	testl	%r10d, %r10d
	jg	.L1055
	testq	%rdi, %rdi
	je	.L1049
	cmpl	$-1, %esi
	jl	.L1049
	testq	%rdx, %rdx
	je	.L1049
	cmpl	$-1, %ecx
	jl	.L1049
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	orl	$65536, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	_ZL8_cmpFoldPKDsiS0_ijPiS1_P10UErrorCode.isra.0.part.0
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1055:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L1049:
	movl	$1, (%r9)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1052:
	ret
	.cfi_endproc
.LFE3150:
	.size	u_strCaseCompare_67, .-u_strCaseCompare_67
	.p2align 4
	.globl	u_strcasecmp_67
	.type	u_strcasecmp_67, @function
u_strcasecmp_67:
.LFB3151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	orl	$65536, %edx
	xorl	%r9d, %r9d
	movl	$-1, %ecx
	movl	%edx, %r8d
	movq	%rsi, %rdx
	movl	$-1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	_ZL8_cmpFoldPKDsiS0_ijPiS1_P10UErrorCode.isra.0.part.0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3151:
	.size	u_strcasecmp_67, .-u_strcasecmp_67
	.p2align 4
	.globl	u_memcasecmp_67
	.type	u_memcasecmp_67, @function
u_memcasecmp_67:
.LFB3152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	orl	$65536, %ecx
	movl	%edx, %esi
	movl	%ecx, %r8d
	xorl	%r9d, %r9d
	movl	%edx, %ecx
	movq	%r10, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	_ZL8_cmpFoldPKDsiS0_ijPiS1_P10UErrorCode.isra.0.part.0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3152:
	.size	u_memcasecmp_67, .-u_memcasecmp_67
	.p2align 4
	.globl	u_strncasecmp_67
	.type	u_strncasecmp_67, @function
u_strncasecmp_67:
.LFB3153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	orl	$69632, %ecx
	movl	%edx, %esi
	movl	%ecx, %r8d
	xorl	%r9d, %r9d
	movl	%edx, %ecx
	movq	%r10, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	_ZL8_cmpFoldPKDsiS0_ijPiS1_P10UErrorCode.isra.0.part.0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3153:
	.size	u_strncasecmp_67, .-u_strncasecmp_67
	.p2align 4
	.globl	u_caseInsensitivePrefixMatch_67
	.type	u_caseInsensitivePrefixMatch_67, @function
u_caseInsensitivePrefixMatch_67:
.LFB3154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	24(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1063
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZL8_cmpFoldPKDsiS0_ijPiS1_P10UErrorCode.isra.0.part.0
	.p2align 4,,10
	.p2align 3
.L1063:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3154:
	.size	u_caseInsensitivePrefixMatch_67, .-u_caseInsensitivePrefixMatch_67
	.section	.rodata
	.align 32
	.type	CSWTCH.121, @object
	.size	CSWTCH.121, 280
CSWTCH.121:
	.long	16384
	.long	16384
	.long	16384
	.long	16384
	.long	131072
	.long	0
	.long	131072
	.long	0
	.long	65536
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	16384
	.long	0
	.long	131072
	.long	131072
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	16384
	.long	131072
	.long	81920
	.long	8192
	.align 32
	.type	_ZN6icu_6710GreekUpperL8data1F00E, @object
	.size	_ZN6icu_6710GreekUpperL8data1F00E, 512
_ZN6icu_6710GreekUpperL8data1F00E:
	.value	5009
	.value	5009
	.value	21393
	.value	21393
	.value	21393
	.value	21393
	.value	21393
	.value	21393
	.value	5009
	.value	5009
	.value	21393
	.value	21393
	.value	21393
	.value	21393
	.value	21393
	.value	21393
	.value	5013
	.value	5013
	.value	21397
	.value	21397
	.value	21397
	.value	21397
	.value	0
	.value	0
	.value	5013
	.value	5013
	.value	21397
	.value	21397
	.value	21397
	.value	21397
	.value	0
	.value	0
	.value	5015
	.value	5015
	.value	21399
	.value	21399
	.value	21399
	.value	21399
	.value	21399
	.value	21399
	.value	5015
	.value	5015
	.value	21399
	.value	21399
	.value	21399
	.value	21399
	.value	21399
	.value	21399
	.value	5017
	.value	5017
	.value	21401
	.value	21401
	.value	21401
	.value	21401
	.value	21401
	.value	21401
	.value	5017
	.value	5017
	.value	21401
	.value	21401
	.value	21401
	.value	21401
	.value	21401
	.value	21401
	.value	5023
	.value	5023
	.value	21407
	.value	21407
	.value	21407
	.value	21407
	.value	0
	.value	0
	.value	5023
	.value	5023
	.value	21407
	.value	21407
	.value	21407
	.value	21407
	.value	0
	.value	0
	.value	5029
	.value	5029
	.value	21413
	.value	21413
	.value	21413
	.value	21413
	.value	21413
	.value	21413
	.value	0
	.value	5029
	.value	0
	.value	21413
	.value	0
	.value	21413
	.value	0
	.value	21413
	.value	5033
	.value	5033
	.value	21417
	.value	21417
	.value	21417
	.value	21417
	.value	21417
	.value	21417
	.value	5033
	.value	5033
	.value	21417
	.value	21417
	.value	21417
	.value	21417
	.value	21417
	.value	21417
	.value	21393
	.value	21393
	.value	21397
	.value	21397
	.value	21399
	.value	21399
	.value	21401
	.value	21401
	.value	21407
	.value	21407
	.value	21413
	.value	21413
	.value	21417
	.value	21417
	.value	0
	.value	0
	.value	13201
	.value	13201
	.value	29585
	.value	29585
	.value	29585
	.value	29585
	.value	29585
	.value	29585
	.value	13201
	.value	13201
	.value	29585
	.value	29585
	.value	29585
	.value	29585
	.value	29585
	.value	29585
	.value	13207
	.value	13207
	.value	29591
	.value	29591
	.value	29591
	.value	29591
	.value	29591
	.value	29591
	.value	13207
	.value	13207
	.value	29591
	.value	29591
	.value	29591
	.value	29591
	.value	29591
	.value	29591
	.value	13225
	.value	13225
	.value	29609
	.value	29609
	.value	29609
	.value	29609
	.value	29609
	.value	29609
	.value	13225
	.value	13225
	.value	29609
	.value	29609
	.value	29609
	.value	29609
	.value	29609
	.value	29609
	.value	5009
	.value	5009
	.value	29585
	.value	13201
	.value	29585
	.value	0
	.value	21393
	.value	29585
	.value	5009
	.value	5009
	.value	21393
	.value	21393
	.value	13201
	.value	0
	.value	5017
	.value	0
	.value	0
	.value	0
	.value	29591
	.value	13207
	.value	29591
	.value	0
	.value	21399
	.value	29591
	.value	21397
	.value	21397
	.value	21399
	.value	21399
	.value	13207
	.value	0
	.value	0
	.value	0
	.value	5017
	.value	5017
	.value	-11367
	.value	-11367
	.value	0
	.value	0
	.value	21401
	.value	-11367
	.value	5017
	.value	5017
	.value	21401
	.value	21401
	.value	0
	.value	0
	.value	0
	.value	0
	.value	5029
	.value	5029
	.value	-11355
	.value	-11355
	.value	929
	.value	929
	.value	21413
	.value	-11355
	.value	5029
	.value	5029
	.value	21413
	.value	21413
	.value	929
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	29609
	.value	13225
	.value	29609
	.value	0
	.value	21417
	.value	29609
	.value	21407
	.value	21407
	.value	21417
	.value	21417
	.value	13225
	.value	0
	.value	0
	.value	0
	.align 32
	.type	_ZN6icu_6710GreekUpperL8data0370E, @object
	.size	_ZN6icu_6710GreekUpperL8data0370E, 288
_ZN6icu_6710GreekUpperL8data0370E:
	.value	880
	.value	880
	.value	882
	.value	882
	.value	0
	.value	0
	.value	886
	.value	886
	.value	0
	.value	0
	.value	890
	.value	1021
	.value	1022
	.value	1023
	.value	0
	.value	895
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	21393
	.value	0
	.value	21397
	.value	21399
	.value	21401
	.value	0
	.value	21407
	.value	0
	.value	21413
	.value	21417
	.value	-11367
	.value	5009
	.value	914
	.value	915
	.value	916
	.value	5013
	.value	918
	.value	5015
	.value	920
	.value	5017
	.value	922
	.value	923
	.value	924
	.value	925
	.value	926
	.value	5023
	.value	928
	.value	929
	.value	0
	.value	931
	.value	932
	.value	5029
	.value	934
	.value	935
	.value	936
	.value	5033
	.value	-27751
	.value	-27739
	.value	21393
	.value	21397
	.value	21399
	.value	21401
	.value	-11355
	.value	5009
	.value	914
	.value	915
	.value	916
	.value	5013
	.value	918
	.value	5015
	.value	920
	.value	5017
	.value	922
	.value	923
	.value	924
	.value	925
	.value	926
	.value	5023
	.value	928
	.value	929
	.value	931
	.value	931
	.value	932
	.value	5029
	.value	934
	.value	935
	.value	936
	.value	5033
	.value	-27751
	.value	-27739
	.value	21407
	.value	21413
	.value	21417
	.value	975
	.value	914
	.value	920
	.value	978
	.value	17362
	.value	-31790
	.value	934
	.value	928
	.value	975
	.value	984
	.value	984
	.value	986
	.value	986
	.value	988
	.value	988
	.value	990
	.value	990
	.value	992
	.value	992
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	922
	.value	929
	.value	1017
	.value	895
	.value	1012
	.value	5013
	.value	0
	.value	1015
	.value	1015
	.value	1017
	.value	1018
	.value	1018
	.value	1020
	.value	1021
	.value	1022
	.value	1023
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
