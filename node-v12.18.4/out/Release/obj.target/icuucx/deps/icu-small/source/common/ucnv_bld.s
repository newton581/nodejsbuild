	.file	"ucnv_bld.cpp"
	.text
	.p2align 4
	.type	_ZL15isCnvAcceptablePvPKcS1_PK9UDataInfo, @function
_ZL15isCnvAcceptablePvPKcS1_PK9UDataInfo:
.LFB2840:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpw	$19, (%rcx)
	jbe	.L1
	cmpw	$0, 4(%rcx)
	jne	.L1
	cmpb	$2, 6(%rcx)
	je	.L9
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	cmpw	$28259, 8(%rcx)
	jne	.L1
	cmpw	$29814, 10(%rcx)
	jne	.L1
	cmpb	$6, 12(%rcx)
	sete	%al
	ret
	.cfi_endproc
.LFE2840:
	.size	_ZL15isCnvAcceptablePvPKcS1_PK9UDataInfo, .-_ZL15isCnvAcceptablePvPKcS1_PK9UDataInfo
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"locale="
.LC1:
	.string	"version="
.LC2:
	.string	"swaplfnl"
	.text
	.p2align 4
	.type	_ZL21parseConverterOptionsPKcP20UConverterNamePiecesP18UConverterLoadArgsP10UErrorCode, @function
_ZL21parseConverterOptionsPKcP20UConverterNamePiecesP18UConverterLoadArgsP10UErrorCode:
.LFB2851:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsi, %r9
	movq	%rdi, 24(%rdx)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	220(%rsi), %ecx
	leaq	60(%rsi), %rbx
	movq	%rbx, 32(%rdx)
	movl	%ecx, 12(%rdx)
	movzbl	(%rdi), %ecx
	cmpb	$44, %cl
	je	.L34
	testb	%cl, %cl
	leaq	59(%rdi), %rdi
	setne	%r8b
	testb	%r8b, %r8b
	jne	.L15
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L64:
	cmpb	$44, %cl
	je	.L13
	cmpq	%rax, %rdi
	je	.L63
.L15:
	addq	$1, %rax
	movb	%cl, (%rsi)
	addq	$1, %rsi
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L64
.L13:
	movb	$0, (%rsi)
	leaq	.LC0(%rip), %r10
	leaq	.LC1(%rip), %r11
	movq	%r9, 24(%rdx)
	movzbl	(%rax), %ecx
	leaq	.LC2(%rip), %r12
	leaq	216(%r9), %r8
	testb	%cl, %cl
	je	.L10
	.p2align 4,,10
	.p2align 3
.L16:
	cmpb	$44, %cl
	movq	%r10, %rdi
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %rax
	movl	$7, %ecx
	movq	%rax, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L65
	movl	$8, %ecx
	movq	%rax, %rsi
	movq	%r11, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	jne	.L30
	movzbl	8(%rax), %ecx
	testb	%cl, %cl
	je	.L66
	leal	-48(%rcx), %esi
	cmpb	$9, %sil
	ja	.L32
	movl	220(%r9), %edi
	movsbl	%cl, %esi
	addq	$9, %rax
	leal	-48(%rsi), %ecx
	andl	$-16, %edi
	orl	%edi, %ecx
	movl	%ecx, 220(%r9)
	movl	%ecx, 12(%rdx)
	movzbl	(%rax), %ecx
.L26:
	testb	%cl, %cl
	jne	.L16
.L10:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	$8, %ecx
	movq	%rax, %rsi
	movq	%r12, %rdi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	jne	.L33
	movl	220(%r9), %ecx
	addq	$8, %rax
	orl	$16, %ecx
	movl	%ecx, 220(%r9)
	movl	%ecx, 12(%rdx)
	movzbl	(%rax), %ecx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L68:
	cmpb	$44, %cl
	je	.L67
.L33:
	movq	%rax, %rsi
	movzbl	(%rax), %ecx
	addq	$1, %rax
	testb	%cl, %cl
	jne	.L68
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L65:
	movzbl	7(%rax), %ecx
	cmpb	$44, %cl
	je	.L24
	testb	%cl, %cl
	je	.L24
	addq	$8, %rax
	movq	%rbx, %rsi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L70:
	testb	%cl, %cl
	je	.L29
	addq	$1, %rax
	cmpq	%r8, %rsi
	je	.L69
.L25:
	movb	%cl, (%rsi)
	movzbl	(%rax), %ecx
	addq	$1, %rsi
	cmpb	$44, %cl
	jne	.L70
.L29:
	movb	$0, (%rsi)
	movzbl	(%rax), %ecx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$8, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L67:
	movzbl	1(%rsi), %ecx
	jmp	.L26
.L69:
	popq	%rbx
	popq	%r12
	movl	$1, 0(%r13)
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	$0, 60(%r9)
	ret
.L24:
	.cfi_restore_state
	addq	$7, %rax
	movq	%rbx, %rsi
	movb	$0, (%rsi)
	movzbl	(%rax), %ecx
	jmp	.L26
.L63:
	popq	%rbx
	popq	%r12
	movl	$1, 0(%r13)
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	$0, (%r9)
	ret
.L66:
	.cfi_restore_state
	movl	220(%r9), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	andl	$-16, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%eax, 220(%r9)
	movl	%eax, 12(%rdx)
	ret
.L34:
	.cfi_restore_state
	movq	%r9, %rsi
	jmp	.L13
	.cfi_endproc
.LFE2851:
	.size	_ZL21parseConverterOptionsPKcP20UConverterNamePiecesP18UConverterLoadArgsP10UErrorCode, .-_ZL21parseConverterOptionsPKcP20UConverterNamePiecesP18UConverterLoadArgsP10UErrorCode
	.p2align 4
	.type	_ZL23createConverterFromFileP18UConverterLoadArgsP10UErrorCode.part.0, @function
_ZL23createConverterFromFileP18UConverterLoadArgsP10UErrorCode.part.0:
.LFB3838:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	xorl	%r8d, %r8d
	leaq	_ZL15isCnvAcceptablePvPKcS1_PK9UDataInfo(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	_ZL9DATA_TYPE(%rip), %rsi
	subq	$24, %rsp
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rdi
	call	udata_openChoice_67@PLT
	movl	(%rbx), %edi
	movq	%rax, %r15
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L71
	movq	%r15, %rdi
	call	udata_getMemory_67@PLT
	movl	(%rbx), %esi
	movq	%rax, %r13
	testl	%esi, %esi
	jg	.L73
	movsbq	69(%rax), %rdx
	movq	%rdx, %rax
	cmpw	$33, %dx
	ja	.L74
	leaq	_ZL13converterData(%rip), %rdx
	movq	(%rdx,%rax,8), %r14
	testq	%r14, %r14
	je	.L74
	cmpb	$0, 25(%r14)
	je	.L74
	cmpl	$1, 4(%r14)
	jne	.L74
	cmpl	$100, 0(%r13)
	jne	.L74
	movl	$296, %edi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L87
	movq	(%r14), %rdx
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	%r14, %rsi
	andq	$-8, %rdi
	movq	%r15, %xmm0
	movq	%r13, %xmm1
	movq	%rdx, (%rax)
	subq	%rdi, %rcx
	movq	288(%r14), %rdx
	punpcklqdq	%xmm1, %xmm0
	subq	%rcx, %rsi
	addl	$296, %ecx
	movq	%rdx, 288(%rax)
	shrl	$3, %ecx
	rep movsq
	movb	$0, 24(%rax)
	movups	%xmm0, 8(%rax)
	movq	32(%rax), %rdx
	movq	8(%rdx), %r8
	testq	%r8, %r8
	je	.L77
	movl	0(%r13), %edx
	movq	%rax, -56(%rbp)
	movq	%rbx, %rcx
	movq	%rax, %rdi
	movq	%r12, %rsi
	addq	%r13, %rdx
	call	*%r8
	movl	(%rbx), %ecx
	movq	-56(%rbp), %rax
	testl	%ecx, %ecx
	jg	.L88
.L71:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movl	$13, (%rbx)
.L73:
	movq	%r15, %rdi
	call	udata_close_67@PLT
	xorl	%eax, %eax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%rax, %rdi
	call	uprv_free_67@PLT
	xorl	%eax, %eax
.L77:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L73
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L73
	.cfi_endproc
.LFE3838:
	.size	_ZL23createConverterFromFileP18UConverterLoadArgsP10UErrorCode.part.0, .-_ZL23createConverterFromFileP18UConverterLoadArgsP10UErrorCode.part.0
	.p2align 4
	.type	ucnv_unloadSharedDataIfReady_67.part.0, @function
ucnv_unloadSharedDataIfReady_67.part.0:
.LFB3841:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	subq	$8, %rsp
	call	umtx_lock_67@PLT
	testq	%r12, %r12
	je	.L91
	movl	4(%r12), %eax
	testl	%eax, %eax
	je	.L95
	subl	$1, %eax
	movl	%eax, 4(%r12)
	je	.L95
.L91:
	addq	$8, %rsp
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	cmpb	$0, 24(%r12)
	jne	.L91
	movq	32(%r12), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L96
	movq	%r12, %rdi
	call	*%rax
.L96:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L97
	call	udata_close_67@PLT
.L97:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	jmp	.L91
	.cfi_endproc
.LFE3841:
	.size	ucnv_unloadSharedDataIfReady_67.part.0, .-ucnv_unloadSharedDataIfReady_67.part.0
	.p2align 4
	.globl	ucnv_enableCleanup_67
	.type	ucnv_enableCleanup_67, @function
ucnv_enableCleanup_67:
.LFB2839:
	.cfi_startproc
	endbr64
	leaq	_ZL12ucnv_cleanupv(%rip), %rsi
	movl	$18, %edi
	jmp	ucln_common_registerCleanup_67@PLT
	.cfi_endproc
.LFE2839:
	.size	ucnv_enableCleanup_67, .-ucnv_enableCleanup_67
	.p2align 4
	.globl	ucnv_load_67
	.type	ucnv_load_67, @function
ucnv_load_67:
.LFB2847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L112
	movq	%rsi, %r12
	movl	(%rsi), %esi
	testl	%esi, %esi
	jg	.L112
	movq	16(%rdi), %rax
	movq	%rdi, %r14
	testq	%rax, %rax
	je	.L113
	cmpb	$0, (%rax)
	jne	.L127
.L113:
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L117
	movq	24(%r14), %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L128
	addl	$1, 4(%rax)
.L109:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L117
.L112:
	xorl	%r13d, %r13d
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r12, %rsi
	call	_ZL23createConverterFromFileP18UConverterLoadArgsP10UErrorCode.part.0
	movq	%rax, %r13
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZL23createConverterFromFileP18UConverterLoadArgsP10UErrorCode.part.0
	movl	(%r12), %ecx
	movq	%rax, %r13
	testl	%ecx, %ecx
	jg	.L112
	testq	%rax, %rax
	je	.L112
	cmpb	$0, 8(%r14)
	jne	.L109
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	movl	$0, -44(%rbp)
	leaq	-44(%rbp), %r12
	testq	%rdi, %rdi
	je	.L130
.L118:
	movq	16(%r13), %rax
	movb	$1, 24(%r13)
	movq	%r12, %rcx
	movq	%r13, %rdx
	leaq	4(%rax), %rsi
	call	uhash_put_67@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r12, %rdi
	call	ucnv_io_countKnownConverters_67@PLT
	movq	uhash_compareChars_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashChars_67@GOTPCREL(%rip), %rdi
	xorl	%edx, %edx
	movzwl	%ax, %ecx
	movq	%r12, %r8
	addl	%ecx, %ecx
	call	uhash_openSize_67@PLT
	leaq	_ZL12ucnv_cleanupv(%rip), %rsi
	movl	$18, %edi
	movq	%rax, _ZL21SHARED_DATA_HASHTABLE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L109
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	jmp	.L118
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2847:
	.size	ucnv_load_67, .-ucnv_load_67
	.p2align 4
	.globl	ucnv_unload_67
	.type	ucnv_unload_67, @function
ucnv_unload_67:
.LFB2848:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L150
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jne	.L134
.L137:
	cmpb	$0, 24(%r12)
	je	.L154
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	subl	$1, %eax
	movl	%eax, 4(%rdi)
	je	.L137
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	32(%r12), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L138
	movq	%r12, %rdi
	call	*%rax
.L138:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L139
	call	udata_close_67@PLT
.L139:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2848:
	.size	ucnv_unload_67, .-ucnv_unload_67
	.p2align 4
	.globl	ucnv_unloadSharedDataIfReady_67
	.type	ucnv_unloadSharedDataIfReady_67, @function
ucnv_unloadSharedDataIfReady_67:
.LFB2849:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L177
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 25(%rdi)
	jne	.L180
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movl	4(%r12), %eax
	testl	%eax, %eax
	jne	.L157
.L160:
	cmpb	$0, 24(%r12)
	je	.L181
.L161:
	addq	$8, %rsp
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	subl	$1, %eax
	movl	%eax, 4(%r12)
	jne	.L161
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L181:
	movq	32(%r12), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L162
	movq	%r12, %rdi
	call	*%rax
.L162:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L163
	call	udata_close_67@PLT
.L163:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	jmp	.L161
	.cfi_endproc
.LFE2849:
	.size	ucnv_unloadSharedDataIfReady_67, .-ucnv_unloadSharedDataIfReady_67
	.p2align 4
	.globl	ucnv_incrementRefCount_67
	.type	ucnv_incrementRefCount_67, @function
ucnv_incrementRefCount_67:
.LFB2850:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L188
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 25(%rdi)
	jne	.L191
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	addl	$1, 4(%rbx)
	addq	$8, %rsp
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	ret
	.cfi_endproc
.LFE2850:
	.size	ucnv_incrementRefCount_67, .-ucnv_incrementRefCount_67
	.section	.rodata.str1.1
.LC3:
	.string	"lmbcs6"
.LC4:
	.string	"UTF-8"
	.text
	.p2align 4
	.globl	ucnv_loadSharedData_67
	.type	ucnv_loadSharedData_67, @function
ucnv_loadSharedData_67:
.LFB2852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, -405(%rbp)
	movl	$0, -404(%rbp)
	testl	%r8d, %r8d
	jg	.L193
	movq	%rsi, %r13
	movq	%rdx, %r12
	movq	%rcx, %rbx
	testq	%rsi, %rsi
	je	.L223
	testq	%rdx, %rdx
	je	.L195
.L197:
	leaq	60(%r13), %rax
	movb	$0, 0(%r13)
	movb	$0, 60(%r13)
	movl	$0, 220(%r13)
	movq	%rdi, 24(%r12)
	movq	%rax, 32(%r12)
	movl	$0, 12(%r12)
	testq	%rdi, %rdi
	je	.L203
	movzbl	(%rdi), %eax
	cmpb	$85, %al
	je	.L224
	cmpb	$117, %al
	je	.L225
.L200:
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	call	_ZL21parseConverterOptionsPKcP20UConverterNamePiecesP18UConverterLoadArgsP10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L193
	movq	24(%r12), %rdi
	leaq	-404(%rbp), %rdx
	leaq	-405(%rbp), %rsi
	call	ucnv_io_getConverterName_67@PLT
	movq	%rax, %r8
	movq	%rax, 24(%r12)
	movl	-404(%rbp), %eax
	testq	%r8, %r8
	je	.L204
	testl	%eax, %eax
	jle	.L226
.L204:
	movq	%r13, 24(%r12)
	movq	%r13, %r8
.L207:
	leaq	-128(%rbp), %r13
	movq	%r8, %rsi
	movl	$17, %r15d
	xorl	%r14d, %r14d
	movq	%r13, %rdi
	call	ucnv_io_stripASCIIForCompare_67@PLT
	leaq	.LC3(%rip), %rsi
	movl	$34, %r8d
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L228:
	movl	-424(%rbp), %r8d
	je	.L227
	movl	%r15d, %r14d
	leal	(%r14,%r8), %eax
	shrl	%eax
	cmpl	%r15d, %eax
	je	.L212
.L229:
	movl	%eax, %edx
	leaq	_ZL11cnvNameType(%rip), %rcx
	movl	%eax, %r15d
	salq	$4, %rdx
	movq	(%rcx,%rdx), %rsi
.L208:
	movq	%r13, %rdi
	movl	%r8d, -424(%rbp)
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L228
	movl	%r15d, %r8d
	leal	(%r14,%r8), %eax
	shrl	%eax
	cmpl	%r15d, %eax
	jne	.L229
.L212:
	movl	$1, 4(%r12)
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	movq	$0, 16(%r12)
	call	umtx_lock_67@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	ucnv_load_67
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	movq	%rax, -424(%rbp)
	call	umtx_unlock_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L193
	movq	-424(%rbp), %rax
	testq	%rax, %rax
	jne	.L192
	.p2align 4,,10
	.p2align 3
.L193:
	xorl	%eax, %eax
.L192:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L230
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	leaq	_ZL11cnvNameType(%rip), %rax
	salq	$4, %r15
	movslq	8(%rax,%r15), %rdx
	leaq	_ZL13converterData(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	jne	.L192
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	-352(%rbp), %r13
	testq	%rdx, %rdx
	jne	.L231
.L195:
	pxor	%xmm0, %xmm0
	leaq	-400(%rbp), %r12
	movq	$0, -368(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movl	$40, -400(%rbp)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L224:
	cmpb	$84, 1(%rdi)
	jne	.L200
	cmpb	$70, 2(%rdi)
	jne	.L200
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L226:
	cmpl	$-122, %eax
	jne	.L206
	movl	$-122, (%rbx)
.L206:
	cmpb	$0, -405(%rbp)
	je	.L207
	cmpq	%r13, %r8
	je	.L207
	movq	%r8, %rdi
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZL21parseConverterOptionsPKcP20UConverterNamePiecesP18UConverterLoadArgsP10UErrorCode
	movq	24(%r12), %r8
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$5, (%rcx)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L225:
	cmpb	$116, 1(%rdi)
	jne	.L200
	cmpb	$102, 2(%rdi)
	jne	.L200
.L201:
	movzbl	3(%rdi), %eax
	cmpb	$45, %al
	je	.L232
	cmpb	$56, %al
	jne	.L200
	cmpb	$0, 4(%rdi)
	jne	.L200
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	.LC4(%rip), %rax
	movq	%rax, 24(%r12)
	leaq	_UTF8Data_67(%rip), %rax
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L232:
	cmpb	$56, 4(%rdi)
	jne	.L200
	cmpb	$0, 5(%rdi)
	jne	.L200
	jmp	.L203
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2852:
	.size	ucnv_loadSharedData_67, .-ucnv_loadSharedData_67
	.p2align 4
	.globl	ucnv_createConverter_67
	.type	ucnv_createConverter_67, @function
ucnv_createConverter_67:
.LFB2853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$288, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -320(%rbp)
	movq	$0, -288(%rbp)
	movl	$40, -320(%rbp)
	movaps	%xmm0, -304(%rbp)
	testl	%r8d, %r8d
	jle	.L279
.L234:
	xorl	%eax, %eax
.L233:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L280
	addq	$288, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	leaq	-320(%rbp), %r14
	movq	%rdx, %r12
	movq	%rdx, %rcx
	movq	%rdi, %rbx
	movq	%r14, %rdx
	movq	%rsi, %rdi
	leaq	-272(%rbp), %rsi
	call	ucnv_loadSharedData_67
	movl	(%r12), %esi
	movq	%rax, %r13
	testl	%esi, %esi
	jg	.L281
	movl	$1, %edx
	testq	%rbx, %rbx
	je	.L282
.L245:
	leaq	8(%rbx), %rdi
	movq	%rbx, %rcx
	xorl	%eax, %eax
	movq	$0, (%rbx)
	movq	$0, 280(%rbx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$288, %ecx
	shrl	$3, %ecx
	rep stosq
	movb	%dl, 61(%rbx)
	movq	%r13, 48(%rbx)
	movl	-308(%rbp), %eax
	cmpb	$0, -312(%rbp)
	movl	%eax, 56(%rbx)
	je	.L283
.L246:
	movq	32(%r13), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L255
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movl	(%r12), %ecx
	movq	%rbx, %rax
	testl	%ecx, %ecx
	jle	.L233
	cmpb	$0, -312(%rbp)
	jne	.L234
	movq	%rbx, %rdi
	call	ucnv_close_67@PLT
	xorl	%eax, %eax
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L283:
	movq	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %xmm0
	leaq	136(%rbx), %rcx
	movl	$-1, 208(%rbx)
	movhps	UCNV_TO_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %xmm0
	movups	%xmm0, (%rbx)
	movl	40(%r13), %eax
	movl	%eax, 72(%rbx)
	movq	16(%r13), %rax
	movzbl	71(%rax), %eax
	movb	%al, 88(%rbx)
	movq	16(%r13), %rax
	movzbl	80(%rax), %eax
	movb	%al, 94(%rbx)
	movq	16(%r13), %rax
	movsbq	76(%rax), %rax
	movq	%rcx, 40(%rbx)
	movb	%al, 89(%rbx)
	movq	16(%r13), %rdx
	leaq	72(%rdx), %rsi
	cmpq	$8, %rax
	jnb	.L247
	testb	$4, %al
	jne	.L284
	testq	%rax, %rax
	je	.L248
	movzbl	72(%rdx), %edx
	movb	%dl, (%rcx)
	testb	$2, %al
	jne	.L285
.L248:
	movl	$1, 284(%rbx)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L282:
	movl	$288, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L286
	xorl	%edx, %edx
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%rbx, %rax
.L244:
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L233
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L281:
	testq	%rax, %rax
	je	.L234
	cmpb	$0, 25(%rax)
	je	.L234
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movl	4(%r13), %eax
	testl	%eax, %eax
	je	.L240
	subl	$1, %eax
	movl	%eax, 4(%r13)
	je	.L240
.L241:
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	%rbx, %rax
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L247:
	movq	72(%rdx), %rdx
	leaq	144(%rbx), %rdi
	andq	$-8, %rdi
	movq	%rdx, 136(%rbx)
	movq	-8(%rsi,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%rax, %rcx
	shrq	$3, %rcx
	rep movsq
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L240:
	cmpb	$0, 24(%r13)
	jne	.L241
	movq	32(%r13), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L242
	movq	%r13, %rdi
	call	*%rax
.L242:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L243
	call	udata_close_67@PLT
.L243:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	jmp	.L241
.L284:
	movl	72(%rdx), %edx
	movl	%edx, (%rcx)
	movl	-4(%rsi,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L248
.L285:
	movzwl	-2(%rsi,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L248
.L280:
	call	__stack_chk_fail@PLT
.L286:
	movl	$7, (%r12)
	testq	%r13, %r13
	je	.L234
	cmpb	$0, 25(%r13)
	je	.L234
	movq	%r13, %rdi
	call	ucnv_unloadSharedDataIfReady_67.part.0
	xorl	%eax, %eax
	jmp	.L244
	.cfi_endproc
.LFE2853:
	.size	ucnv_createConverter_67, .-ucnv_createConverter_67
	.section	.rodata.str1.1
.LC5:
	.string	""
	.text
	.p2align 4
	.globl	ucnv_createAlgorithmicConverter_67
	.type	ucnv_createAlgorithmicConverter_67, @function
ucnv_createAlgorithmicConverter_67:
.LFB2855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%r8, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -48(%rbp)
	movl	$40, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	cmpl	$33, %esi
	ja	.L290
	movslq	%esi, %rsi
	leaq	_ZL13converterData(%rip), %rax
	movq	(%rax,%rsi,8), %r14
	testq	%r14, %r14
	je	.L290
	cmpb	$0, 25(%r14)
	jne	.L290
	movq	%rdx, -48(%rbp)
	movl	(%r8), %edx
	movq	%rdi, %r13
	movl	%ecx, %r9d
	leaq	.LC5(%rip), %rax
	movl	%ecx, -68(%rbp)
	movq	%rdi, %r12
	movq	%rax, -56(%rbp)
	testl	%edx, %edx
	jg	.L287
	testq	%rdi, %rdi
	je	.L317
	movq	$0, (%rdi)
	leaq	8(%rdi), %rdi
	movq	%r13, %rcx
	xorl	%eax, %eax
	movq	$0, 272(%rdi)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$288, %ecx
	shrl	$3, %ecx
	rep stosq
	movb	$1, 61(%r13)
	movq	%r14, 48(%r13)
	movl	%r9d, 56(%r13)
.L293:
	movq	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %xmm0
	leaq	136(%r12), %rcx
	movl	$-1, 208(%r12)
	movhps	UCNV_TO_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %xmm0
	movups	%xmm0, (%r12)
	movl	40(%r14), %eax
	movl	%eax, 72(%r12)
	movq	16(%r14), %rax
	movzbl	71(%rax), %eax
	movb	%al, 88(%r12)
	movq	16(%r14), %rax
	movzbl	80(%rax), %eax
	movb	%al, 94(%r12)
	movq	16(%r14), %rax
	movsbq	76(%rax), %rax
	movq	%rcx, 40(%r12)
	movb	%al, 89(%r12)
	movq	16(%r14), %rdx
	leaq	72(%rdx), %rsi
	cmpq	$8, %rax
	jnb	.L295
	testb	$4, %al
	jne	.L318
	testq	%rax, %rax
	je	.L296
	movzbl	72(%rdx), %edx
	movb	%dl, (%rcx)
	testb	$2, %al
	je	.L296
	movzwl	-2(%rsi,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L296
.L292:
	movl	$7, (%rbx)
	cmpb	$0, 25(%r14)
	je	.L287
	movq	%r14, %rdi
	call	ucnv_unloadSharedDataIfReady_67.part.0
	.p2align 4,,10
	.p2align 3
.L287:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L319
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L317:
	movl	$288, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L292
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movl	-68(%rbp), %esi
	movzbl	-72(%rbp), %edx
	andq	$-8, %rdi
	movq	$0, (%rax)
	movq	$0, 280(%rax)
	subq	%rdi, %rcx
	movq	%r13, %rax
	addl	$288, %ecx
	shrl	$3, %ecx
	rep stosq
	movb	$0, 61(%r12)
	movq	%r14, 48(%r12)
	movl	%esi, 56(%r12)
	testb	%dl, %dl
	je	.L293
.L294:
	movq	32(%r14), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L287
	leaq	-80(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	*%rax
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L287
	cmpb	$0, -72(%rbp)
	jne	.L287
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ucnv_close_67@PLT
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L295:
	movq	72(%rdx), %rdx
	leaq	144(%r12), %rdi
	andq	$-8, %rdi
	movq	%rdx, 136(%r12)
	movq	-8(%rsi,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%rax, %rcx
	shrq	$3, %rcx
	rep movsq
.L296:
	movl	$1, 284(%r12)
	jmp	.L294
.L318:
	movl	72(%rdx), %edx
	movl	%edx, (%rcx)
	movl	-4(%rsi,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L296
.L319:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2855:
	.size	ucnv_createAlgorithmicConverter_67, .-ucnv_createAlgorithmicConverter_67
	.p2align 4
	.globl	ucnv_createConverterFromSharedData_67
	.type	ucnv_createConverterFromSharedData_67, @function
ucnv_createConverterFromSharedData_67:
.LFB2857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %esi
	testl	%esi, %esi
	jg	.L363
	movq	%rdx, %r14
	movq	%rcx, %rbx
	movq	%rdi, %r12
	movl	$1, %edx
	testq	%rdi, %rdi
	je	.L364
.L330:
	leaq	8(%r12), %rdi
	movq	%r12, %rcx
	xorl	%eax, %eax
	movq	$0, (%r12)
	movq	$0, 280(%r12)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$288, %ecx
	shrl	$3, %ecx
	rep stosq
	movb	%dl, 61(%r12)
	movq	%r13, 48(%r12)
	movl	12(%r14), %eax
	movl	%eax, 56(%r12)
	cmpb	$0, 8(%r14)
	je	.L365
	movq	32(%r13), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L320
.L369:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rax
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L320
	cmpb	$0, 8(%r14)
	je	.L366
.L320:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movq	%rdi, %r12
	testq	%r13, %r13
	je	.L320
	cmpb	$0, 25(%r13)
	je	.L320
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movl	4(%r13), %eax
	testl	%eax, %eax
	jne	.L323
.L326:
	cmpb	$0, 24(%r13)
	je	.L367
.L327:
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	movq	%r15, %r12
	call	umtx_unlock_67@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L365:
	movq	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %xmm0
	movl	$-1, 208(%r12)
	movhps	UCNV_TO_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %xmm0
	movups	%xmm0, (%r12)
	movl	40(%r13), %eax
	movl	%eax, 72(%r12)
	movq	16(%r13), %rax
	movzbl	71(%rax), %eax
	movb	%al, 88(%r12)
	movq	16(%r13), %rax
	movzbl	80(%rax), %eax
	movb	%al, 94(%r12)
	movq	16(%r13), %rax
	movsbq	76(%rax), %rcx
	leaq	136(%r12), %rax
	movq	%rax, 40(%r12)
	movb	%cl, 89(%r12)
	movq	16(%r13), %rdx
	leaq	72(%rdx), %rsi
	cmpq	$8, %rcx
	jnb	.L332
	testb	$4, %cl
	jne	.L368
	testq	%rcx, %rcx
	je	.L333
	movzbl	72(%rdx), %edx
	movb	%dl, (%rax)
	testb	$2, %cl
	je	.L333
	movzwl	-2(%rsi,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	.p2align 4,,10
	.p2align 3
.L333:
	movl	$1, 284(%r12)
	movq	32(%r13), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L369
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L364:
	movl	$288, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L370
	xorl	%edx, %edx
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L332:
	movq	72(%rdx), %rdx
	leaq	144(%r12), %rdi
	andq	$-8, %rdi
	movq	%rdx, 136(%r12)
	movq	-8(%rsi,%rcx), %rdx
	movq	%rdx, -8(%rax,%rcx)
	subq	%rdi, %rax
	addq	%rax, %rcx
	subq	%rax, %rsi
	shrq	$3, %rcx
	rep movsq
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L323:
	subl	$1, %eax
	movl	%eax, 4(%r13)
	jne	.L327
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L366:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ucnv_close_67@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L367:
	movq	32(%r13), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L328
	movq	%r13, %rdi
	call	*%rax
.L328:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L329
	call	udata_close_67@PLT
.L329:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	jmp	.L327
.L368:
	movl	72(%rdx), %edx
	movl	%edx, (%rax)
	movl	-4(%rsi,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L333
.L370:
	movl	$7, (%rbx)
	testq	%r13, %r13
	je	.L320
	cmpb	$0, 25(%r13)
	je	.L320
	movq	%r13, %rdi
	call	ucnv_unloadSharedDataIfReady_67.part.0
	jmp	.L320
	.cfi_endproc
.LFE2857:
	.size	ucnv_createConverterFromSharedData_67, .-ucnv_createConverterFromSharedData_67
	.p2align 4
	.globl	ucnv_canCreateConverter_67
	.type	ucnv_canCreateConverter_67, @function
ucnv_canCreateConverter_67:
.LFB2854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	movaps	%xmm0, -608(%rbp)
	movq	$0, -576(%rbp)
	movl	$40, -608(%rbp)
	movaps	%xmm0, -592(%rbp)
	testl	%eax, %eax
	jle	.L394
.L372:
	testl	%eax, %eax
	setle	%al
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L395
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movq	%rsi, %rbx
	leaq	-608(%rbp), %r13
	leaq	-560(%rbp), %rsi
	movb	$1, -600(%rbp)
	movq	%rbx, %rcx
	movq	%r13, %rdx
	call	ucnv_loadSharedData_67
	leaq	-336(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%rax, %r12
	movq	%rax, %rsi
	call	ucnv_createConverterFromSharedData_67
	testq	%r12, %r12
	je	.L393
	cmpb	$0, 25(%r12)
	je	.L393
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movl	4(%r12), %eax
	testl	%eax, %eax
	jne	.L375
.L378:
	cmpb	$0, 24(%r12)
	je	.L396
.L379:
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L393:
	movl	(%rbx), %eax
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L375:
	subl	$1, %eax
	movl	%eax, 4(%r12)
	jne	.L379
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L396:
	movq	32(%r12), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L380
	movq	%r12, %rdi
	call	*%rax
.L380:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L381
	call	udata_close_67@PLT
.L381:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	jmp	.L379
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2854:
	.size	ucnv_canCreateConverter_67, .-ucnv_canCreateConverter_67
	.p2align 4
	.globl	ucnv_createConverterFromPackage_67
	.type	ucnv_createConverterFromPackage_67, @function
ucnv_createConverterFromPackage_67:
.LFB2856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -320(%rbp)
	movq	$0, -288(%rbp)
	movl	$40, -320(%rbp)
	movaps	%xmm0, -304(%rbp)
	testl	%r8d, %r8d
	jg	.L403
	leaq	-320(%rbp), %r13
	movq	%rdx, %rbx
	movq	%rdx, %rcx
	movq	%rdi, %r12
	movq	%r13, %rdx
	movq	%rsi, %rdi
	leaq	-272(%rbp), %rsi
	movb	$0, -272(%rbp)
	movb	$0, -212(%rbp)
	movl	$0, -52(%rbp)
	call	_ZL21parseConverterOptionsPKcP20UConverterNamePiecesP18UConverterLoadArgsP10UErrorCode
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L403
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	$1, -316(%rbp)
	movq	%r12, -304(%rbp)
	call	_ZL23createConverterFromFileP18UConverterLoadArgsP10UErrorCode.part.0
	movl	(%rbx), %ecx
	movq	%rax, %rsi
	testl	%ecx, %ecx
	jg	.L403
	movq	%r13, %rdx
	movq	%rbx, %rcx
	xorl	%edi, %edi
	call	ucnv_createConverterFromSharedData_67
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L397
	movq	%rax, %rdi
	call	ucnv_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L403:
	xorl	%eax, %eax
.L397:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L404
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L404:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2856:
	.size	ucnv_createConverterFromPackage_67, .-ucnv_createConverterFromPackage_67
	.p2align 4
	.type	_ZL27initAvailableConvertersListR10UErrorCode, @function
_ZL27initAvailableConvertersListR10UErrorCode:
.LFB2859:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL12ucnv_cleanupv(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$18, %edi
	subq	$920, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ucln_common_registerCleanup_67@PLT
	movq	%rbx, %rdi
	call	ucnv_openAllNames_67@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	uenum_count_67@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L439
.L405:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L440
	addq	$920, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movslq	%eax, %rdi
	movl	%eax, %r13d
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, _ZL20gAvailableConverters(%rip)
	testq	%rax, %rax
	je	.L441
	pxor	%xmm0, %xmm0
	leaq	-292(%rbp), %rax
	leaq	.LC4(%rip), %rcx
	movl	$0, -916(%rbp)
	movaps	%xmm0, -912(%rbp)
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	-916(%rbp), %r14
	leaq	-912(%rbp), %r15
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, %rcx
	movq	$0, -896(%rbp)
	leaq	_UTF8Data_67(%rip), %rsi
	leaq	-640(%rbp), %rdi
	movq	%r15, %rdx
	movups	%xmm0, -888(%rbp)
	movl	$40, -912(%rbp)
	movb	$0, -352(%rbp)
	movb	$0, -292(%rbp)
	movl	$0, -132(%rbp)
	movl	$0, -900(%rbp)
	call	ucnv_createConverterFromSharedData_67
	movl	-916(%rbp), %ecx
	movq	%rax, %rdi
	movl	$0, %eax
	testl	%ecx, %ecx
	cmovg	%rax, %rdi
	xorl	%ebx, %ebx
	call	ucnv_close_67@PLT
	xorl	%esi, %esi
	leaq	-864(%rbp), %rax
	movw	%si, _ZL24gAvailableConverterCount(%rip)
	movq	%rax, -952(%rbp)
	testl	%r13d, %r13d
	jg	.L409
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L420:
	addl	$1, %ebx
	cmpl	%ebx, %r13d
	je	.L421
.L409:
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$0, -916(%rbp)
	call	uenum_next_67@PLT
	movl	-916(%rbp), %edx
	pxor	%xmm0, %xmm0
	movq	$0, -880(%rbp)
	movaps	%xmm0, -912(%rbp)
	movaps	%xmm0, -896(%rbp)
	movl	$40, -912(%rbp)
	testl	%edx, %edx
	jg	.L420
	movq	-952(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rax, %rdi
	movb	$1, -904(%rbp)
	movq	%rax, -944(%rbp)
	call	ucnv_loadSharedData_67
	leaq	-352(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%rax, -936(%rbp)
	call	ucnv_createConverterFromSharedData_67
	movq	-936(%rbp), %r9
	movq	-944(%rbp), %r8
	testq	%r9, %r9
	je	.L412
	cmpb	$0, 25(%r9)
	jne	.L442
.L412:
	movl	-916(%rbp), %eax
	testl	%eax, %eax
	jg	.L420
	movzwl	_ZL24gAvailableConverterCount(%rip), %eax
	addl	$1, %ebx
	leal	1(%rax), %edx
	movw	%dx, _ZL24gAvailableConverterCount(%rip)
	movq	_ZL20gAvailableConverters(%rip), %rdx
	movq	%r8, (%rdx,%rax,8)
	cmpl	%ebx, %r13d
	jne	.L409
.L421:
	movq	%r12, %rdi
	call	uenum_close_67@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	movq	%r9, -944(%rbp)
	movq	%r8, -936(%rbp)
	call	umtx_lock_67@PLT
	movq	-944(%rbp), %r9
	movq	-936(%rbp), %r8
	movl	4(%r9), %eax
	testl	%eax, %eax
	je	.L416
	subl	$1, %eax
	movl	%eax, 4(%r9)
	je	.L416
.L417:
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	movq	%r8, -936(%rbp)
	call	umtx_unlock_67@PLT
	movq	-936(%rbp), %r8
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L416:
	cmpb	$0, 24(%r9)
	jne	.L417
	movq	32(%r9), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L418
	movq	%r8, -944(%rbp)
	movq	%r9, %rdi
	movq	%r9, -936(%rbp)
	call	*%rax
	movq	-944(%rbp), %r8
	movq	-936(%rbp), %r9
.L418:
	movq	8(%r9), %rdi
	testq	%rdi, %rdi
	je	.L419
	movq	%r9, -944(%rbp)
	movq	%r8, -936(%rbp)
	call	udata_close_67@PLT
	movq	-944(%rbp), %r9
	movq	-936(%rbp), %r8
.L419:
	movq	%r9, %rdi
	movq	%r8, -936(%rbp)
	call	uprv_free_67@PLT
	movq	-936(%rbp), %r8
	jmp	.L417
.L440:
	call	__stack_chk_fail@PLT
.L441:
	movl	$7, (%rbx)
	jmp	.L405
	.cfi_endproc
.LFE2859:
	.size	_ZL27initAvailableConvertersListR10UErrorCode, .-_ZL27initAvailableConvertersListR10UErrorCode
	.p2align 4
	.globl	ucnv_flushCache_67
	.type	ucnv_flushCache_67, @function
ucnv_flushCache_67:
.LFB2858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	u_flushDefaultConverter_67@PLT
	cmpq	$0, _ZL21SHARED_DATA_HASHTABLE(%rip)
	je	.L443
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	xorl	%r14d, %r14d
	leaq	-60(%rbp), %r12
	call	umtx_lock_67@PLT
.L452:
	movl	$-1, -60(%rbp)
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	xorl	%ebx, %ebx
.L445:
	movq	%r12, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	je	.L446
.L464:
	movq	8(%rax), %r15
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	movl	4(%r15), %edx
	testl	%edx, %edx
	je	.L463
	movq	%r12, %rsi
	addl	$1, %ebx
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L464
.L446:
	testl	%ebx, %ebx
	movl	%r14d, %eax
	movl	$1, %r14d
	setne	%dl
	xorl	$1, %eax
	testb	%al, %dl
	jne	.L452
	leaq	_ZL13cnvCacheMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L443:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L465
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	%rax, %rsi
	addl	$1, %r13d
	call	uhash_removeElement_67@PLT
	movl	4(%r15), %eax
	movb	$0, 24(%r15)
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	testl	%eax, %eax
	jne	.L445
	movq	32(%r15), %rax
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L449
	movq	%r15, %rdi
	call	*%rax
.L449:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L450
	call	udata_close_67@PLT
.L450:
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	jmp	.L445
.L465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2858:
	.size	ucnv_flushCache_67, .-ucnv_flushCache_67
	.p2align 4
	.type	_ZL12ucnv_cleanupv, @function
_ZL12ucnv_cleanupv:
.LFB2838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ucnv_flushCache_67
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L468
	call	uhash_count_67@PLT
	testl	%eax, %eax
	je	.L475
.L468:
	movq	_ZL20gAvailableConverters(%rip), %rdi
	xorl	%eax, %eax
	movw	%ax, _ZL24gAvailableConverterCount(%rip)
	testq	%rdi, %rdi
	je	.L470
	call	uprv_free_67@PLT
	movq	$0, _ZL20gAvailableConverters(%rip)
.L470:
	movl	$0, _ZL28gAvailableConvertersInitOnce(%rip)
	mfence
	cmpq	$0, _ZL21SHARED_DATA_HASHTABLE(%rip)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	.cfi_restore_state
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	call	uhash_close_67@PLT
	movq	$0, _ZL21SHARED_DATA_HASHTABLE(%rip)
	jmp	.L468
	.cfi_endproc
.LFE2838:
	.size	_ZL12ucnv_cleanupv, .-_ZL12ucnv_cleanupv
	.p2align 4
	.globl	ucnv_bld_countAvailableConverters_67
	.type	ucnv_bld_countAvailableConverters_67, @function
ucnv_bld_countAvailableConverters_67:
.LFB2861:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	testl	%edx, %edx
	jle	.L492
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZL28gAvailableConvertersInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L493
.L478:
	movl	4+_ZL28gAvailableConvertersInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L479
	movl	%eax, (%rbx)
.L477:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore_state
	leaq	_ZL28gAvailableConvertersInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L478
	movq	%rbx, %rdi
	call	_ZL27initAvailableConvertersListR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL28gAvailableConvertersInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL28gAvailableConvertersInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L479:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L477
	movzwl	_ZL24gAvailableConverterCount(%rip), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2861:
	.size	ucnv_bld_countAvailableConverters_67, .-ucnv_bld_countAvailableConverters_67
	.p2align 4
	.globl	ucnv_bld_getAvailableConverter_67
	.type	ucnv_bld_getAvailableConverter_67, @function
ucnv_bld_getAvailableConverter_67:
.LFB2862:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L511
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movzwl	%di, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	_ZL28gAvailableConvertersInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L512
.L496:
	movl	4+_ZL28gAvailableConvertersInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L497
	movl	%eax, (%rbx)
.L495:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	.cfi_restore_state
	leaq	_ZL28gAvailableConvertersInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L496
	movq	%rbx, %rdi
	call	_ZL27initAvailableConvertersListR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZL28gAvailableConvertersInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL28gAvailableConvertersInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L497:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L495
	cmpw	%r12w, _ZL24gAvailableConverterCount(%rip)
	jbe	.L498
	movq	_ZL20gAvailableConverters(%rip), %rax
	movq	(%rax,%r13,8), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	movl	$8, (%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2862:
	.size	ucnv_bld_getAvailableConverter_67, .-ucnv_bld_getAvailableConverter_67
	.p2align 4
	.globl	ucnv_getDefaultName_67
	.type	ucnv_getDefaultName_67, @function
ucnv_getDefaultName_67:
.LFB2863:
	.cfi_startproc
	endbr64
	leaq	.LC4(%rip), %rax
	ret
	.cfi_endproc
.LFE2863:
	.size	ucnv_getDefaultName_67, .-ucnv_getDefaultName_67
	.p2align 4
	.globl	ucnv_setDefaultName_67
	.type	ucnv_setDefaultName_67, @function
ucnv_setDefaultName_67:
.LFB2864:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2864:
	.size	ucnv_setDefaultName_67, .-ucnv_setDefaultName_67
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"ucnv_swap(): data format %02x.%02x.%02x.%02x (format version %02x.%02x) is not recognized as an ICU .cnv conversion table\n"
	.align 8
.LC7:
	.string	"ucnv_swap(): too few bytes (%d after header) for an ICU .cnv conversion table\n"
	.align 8
.LC8:
	.string	"ucnv_swap(): error swapping converter name\n"
	.align 8
.LC9:
	.string	"ucnv_swap(): too few bytes (%d after headers) for an ICU MBCS .cnv conversion table\n"
	.align 8
.LC10:
	.string	"ucnv_swap(): unsupported _MBCSHeader.version %d.%d\n"
	.align 8
.LC11:
	.string	"ucnv_swap(): unsupported combination of makeconv --small with SBCS\n"
	.align 8
.LC12:
	.string	"ucnv_swap(): unsupported MBCS output type 0x%x\n"
	.align 8
.LC13:
	.string	"ucnv_swap(): too few bytes (%d after headers) for an ICU MBCS .cnv conversion table with extension data\n"
	.align 8
.LC14:
	.string	"ucnv_swap(): unknown conversionType=%d!=UCNV_MBCS\n"
	.text
	.p2align 4
	.globl	ucnv_swap_67
	.type	ucnv_swap_67, @function
ucnv_swap_67:
.LFB2865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	call	udata_swapDataHeader_67@PLT
	movl	%eax, -52(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L515
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L515
	cmpw	$28259, 12(%rbx)
	movzbl	16(%rbx), %eax
	movzbl	17(%rbx), %ecx
	jne	.L517
	cmpw	$29814, 14(%rbx)
	jne	.L517
	cmpb	$6, %al
	jne	.L517
	cmpb	$1, %cl
	jbe	.L517
	movslq	-52(%rbp), %rax
	addq	%rax, %rbx
	addq	%rax, %r13
	testl	%r15d, %r15d
	js	.L591
	subl	-52(%rbp), %r15d
	movl	%r15d, -64(%rbp)
	cmpl	$99, %r15d
	jle	.L523
	movl	(%rbx), %edi
	call	*16(%r14)
	movl	%eax, -56(%rbp)
	cmpl	%eax, -64(%rbp)
	jb	.L523
	cmpq	%r13, %rbx
	je	.L555
	movl	-56(%rbp), %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	memcpy@PLT
.L555:
	leaq	4(%rbx), %r15
	movq	%r12, %r8
	movq	%r13, %rcx
	movl	$4, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*56(%r14)
	leaq	64(%r13), %rcx
	leaq	64(%rbx), %rsi
	movq	%r12, %r8
	movl	$4, %edx
	movq	%r14, %rdi
	call	*56(%r14)
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	4(%r13), %rcx
	movq	%r12, %r8
	movq	%r15, %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	*72(%r14)
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L592
	movzbl	69(%rbx), %eax
	cmpb	$2, %al
	jne	.L521
	movl	-64(%rbp), %r15d
	subl	-56(%rbp), %r15d
	cmpl	$39, %r15d
	ja	.L593
.L526:
	movl	%r15d, %edx
	leaq	.LC9(%rip), %rsi
.L589:
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	movl	$8, (%r12)
	xorl	%eax, %eax
.L515:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	movzbl	13(%rbx), %r11d
	movzbl	12(%rbx), %edx
	pushq	%rcx
	leaq	.LC6(%rip), %rsi
	pushq	%rax
	movzbl	15(%rbx), %r9d
	movq	%r14, %rdi
	xorl	%eax, %eax
	movzbl	14(%rbx), %r8d
	movl	%r11d, %ecx
	call	udata_printError_67@PLT
	movl	$16, (%r12)
	popq	%rcx
	xorl	%eax, %eax
	popq	%rsi
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	movl	(%rbx), %edi
	call	*16(%r14)
	movl	%eax, %ecx
	movzbl	69(%rbx), %eax
	leaq	0(%r13,%rcx), %rsi
	movl	%ecx, -56(%rbp)
	leaq	(%rbx,%rcx), %r9
	movq	%rsi, -64(%rbp)
	cmpb	$2, %al
	jne	.L521
.L527:
	movzbl	(%r9), %eax
	movzbl	1(%r9), %ecx
	cmpb	$4, %al
	je	.L594
	cmpb	$5, %al
	jne	.L530
	cmpb	$2, %cl
	jbe	.L530
	movq	%r9, -72(%rbp)
	movl	32(%r9), %edi
	call	*16(%r14)
	movq	-72(%rbp), %r9
	testl	$65408, %eax
	jne	.L595
	movl	%eax, %edi
	shrl	$6, %eax
	movl	%eax, %r13d
	andl	$63, %edi
	andl	$1, %r13d
	movl	%edi, -120(%rbp)
	movb	%r13b, -104(%rbp)
.L529:
	movl	(%r9), %eax
	movl	4(%r9), %edi
	movq	%r9, -72(%rbp)
	movl	%eax, -92(%rbp)
	call	*16(%r14)
	movq	-72(%rbp), %r9
	movl	%eax, -128(%rbp)
	movl	8(%r9), %edi
	call	*16(%r14)
	movq	-72(%rbp), %r9
	movl	%eax, -96(%rbp)
	movl	12(%r9), %edi
	call	*16(%r14)
	movq	-72(%rbp), %r9
	movl	%eax, -132(%rbp)
	movl	16(%r9), %edi
	call	*16(%r14)
	movq	-72(%rbp), %r9
	movl	%eax, -112(%rbp)
	movl	20(%r9), %edi
	call	*16(%r14)
	movq	-72(%rbp), %r9
	movl	%eax, -84(%rbp)
	movl	24(%r9), %edi
	call	*16(%r14)
	movq	-72(%rbp), %r9
	movl	%eax, -88(%rbp)
	movl	28(%r9), %edi
	call	*16(%r14)
	movl	-88(%rbp), %r10d
	movq	-72(%rbp), %r9
	movl	%eax, -80(%rbp)
	testb	%r10b, %r10b
	jne	.L533
	testb	%r13b, %r13b
	jne	.L596
.L533:
	movzbl	%r10b, %r13d
	cmpb	$14, %r10b
	ja	.L534
	movl	$1, %eax
	movl	%r13d, %ecx
	salq	%cl, %rax
	testl	$21263, %eax
	je	.L534
	movl	$0, -136(%rbp)
	cmpb	$14, %r10b
	je	.L536
	testb	%r10b, %r10b
	je	.L536
	movl	-92(%rbp), %eax
	cmpb	$2, %ah
	jbe	.L536
	movl	%eax, %esi
	shrl	$16, %eax
	andl	$16711680, %esi
	je	.L536
	movzbl	%al, %eax
	sall	$8, %eax
	orb	$-1, %al
	addl	$1, %eax
	sarl	$6, %eax
	addl	%eax, %eax
	movl	%eax, -136(%rbp)
.L536:
	movl	%r10d, %eax
	shrl	$8, %eax
	movl	%eax, -88(%rbp)
	jne	.L537
	movzbl	-104(%rbp), %edi
	movl	-136(%rbp), %eax
	movq	$0, -72(%rbp)
	addl	-84(%rbp), %eax
	movl	%eax, %ecx
	addl	-80(%rbp), %ecx
	testb	%dil, %dil
	cmove	%ecx, %eax
	movl	%eax, -92(%rbp)
.L538:
	testl	%r15d, %r15d
	js	.L552
	cmpl	%r15d, -92(%rbp)
	jg	.L526
	movq	-64(%rbp), %rdi
	cmpq	%r9, %rdi
	je	.L542
	movslq	-92(%rbp), %rdx
	movq	%r9, %rsi
	movl	%r10d, -152(%rbp)
	movq	%r9, -144(%rbp)
	call	memcpy@PLT
	movl	-152(%rbp), %r10d
	movq	-144(%rbp), %r9
.L542:
	movl	-120(%rbp), %r15d
	movq	-64(%rbp), %rax
	leaq	4(%r9), %rsi
	movq	%r9, -120(%rbp)
	movl	%r10d, -144(%rbp)
	movq	%r12, %r8
	movq	%r14, %rdi
	sall	$2, %r15d
	leaq	4(%rax), %rcx
	leal	-4(%r15), %edx
	call	*56(%r14)
	movq	-64(%rbp), %rax
	movq	-120(%rbp), %r9
	movl	%r15d, %esi
	movl	-144(%rbp), %r10d
	leaq	(%rax,%rsi), %r11
	addq	%r9, %rsi
	cmpb	$14, %r10b
	je	.L597
	movl	-128(%rbp), %edx
	movl	%r10d, -144(%rbp)
	movq	%r11, %rcx
	movq	%r12, %r8
	movq	%r9, -152(%rbp)
	movq	%r14, %rdi
	sall	$10, %edx
	movl	%edx, -120(%rbp)
	call	*56(%r14)
	movl	-120(%rbp), %edx
	movl	%r15d, %esi
	movq	-64(%rbp), %r15
	movq	-152(%rbp), %r9
	movq	%r12, %r8
	movq	%r14, %rdi
	addl	%edx, %esi
	movl	-96(%rbp), %edx
	leaq	(%r15,%rsi), %rcx
	movq	%r9, -120(%rbp)
	addq	%r9, %rsi
	sall	$3, %edx
	call	*56(%r14)
	movl	-132(%rbp), %esi
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	%r15, -64(%rbp)
	movq	-120(%rbp), %r9
	leaq	(%r15,%rsi), %rcx
	movl	-112(%rbp), %r15d
	movl	%r15d, %eax
	subl	%esi, %eax
	addq	%r9, %rsi
	movl	%eax, %edx
	call	*48(%r14)
	movl	%r15d, %esi
	movq	-120(%rbp), %r9
	movq	-64(%rbp), %r15
	movl	-144(%rbp), %r10d
	movq	%rsi, %rax
	leaq	(%r15,%rsi), %rcx
	addq	%r9, %rsi
	testb	%r10b, %r10b
	jne	.L545
	movl	-84(%rbp), %edx
	addl	-80(%rbp), %edx
	movq	%r9, -104(%rbp)
	movq	%r12, %r8
	subl	%eax, %edx
	movq	%r14, %rdi
	call	*48(%r14)
	movq	-104(%rbp), %r9
.L544:
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	jne	.L598
.L552:
	movl	-56(%rbp), %eax
	addl	-52(%rbp), %eax
	addl	-92(%rbp), %eax
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L523:
	movl	-64(%rbp), %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L521:
	movsbl	%al, %edx
	leaq	.LC14(%rip), %rsi
.L590:
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	movl	$16, (%r12)
	xorl	%eax, %eax
	jmp	.L515
.L595:
	movzbl	1(%r9), %ecx
	movzbl	(%r9), %eax
.L530:
	movzbl	%al, %edx
	leaq	.LC10(%rip), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	movl	$16, (%r12)
	xorl	%eax, %eax
	jmp	.L515
.L592:
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	xorl	%eax, %eax
	jmp	.L515
.L594:
	testb	%cl, %cl
	je	.L530
	movb	$0, -104(%rbp)
	xorl	%r13d, %r13d
	movl	$8, -120(%rbp)
	jmp	.L529
.L593:
	movl	-56(%rbp), %eax
	leaq	(%rbx,%rax), %r9
	addq	%r13, %rax
	movq	%rax, -64(%rbp)
	jmp	.L527
.L537:
	testl	%r15d, %r15d
	js	.L539
	addl	$127, %eax
	movl	%r15d, %edx
	leaq	.LC13(%rip), %rsi
	cmpl	%r15d, %eax
	jge	.L589
.L539:
	movl	-88(%rbp), %eax
	movl	%r10d, -152(%rbp)
	movq	%r9, -144(%rbp)
	leaq	(%r9,%rax), %rdi
	movl	124(%rdi), %esi
	movq	%rdi, -72(%rbp)
	movq	%r14, %rdi
	call	udata_readInt32_67@PLT
	addl	-88(%rbp), %eax
	movl	-152(%rbp), %r10d
	movl	%eax, -92(%rbp)
	movq	-144(%rbp), %r9
	jmp	.L538
.L596:
	xorl	%eax, %eax
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	udata_printError_67@PLT
	movl	$16, (%r12)
	xorl	%eax, %eax
	jmp	.L515
.L534:
	movl	%r13d, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L590
.L598:
	movl	-88(%rbp), %eax
	movq	-64(%rbp), %r13
	movq	%r14, %rdi
	leaq	(%r9,%rax), %r15
	addq	%rax, %r13
	movq	-72(%rbp), %rax
	movl	4(%rax), %esi
	call	udata_readInt32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	movq	-72(%rbp), %rax
	movl	8(%rax), %esi
	call	udata_readInt32_67@PLT
	leaq	0(%r13,%rbx), %rcx
	movq	%r12, %r8
	movq	%r14, %rdi
	leal	0(,%rax,4), %edx
	leaq	(%r15,%rbx), %rsi
	call	*56(%r14)
	movq	-72(%rbp), %rax
	movq	%r14, %rdi
	movl	12(%rax), %esi
	call	udata_readInt32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	movq	-72(%rbp), %rax
	movl	16(%rax), %esi
	call	udata_readInt32_67@PLT
	leaq	0(%r13,%rbx), %rcx
	movq	%r12, %r8
	movq	%r14, %rdi
	leal	(%rax,%rax), %edx
	leaq	(%r15,%rbx), %rsi
	call	*48(%r14)
	movq	-72(%rbp), %rax
	movq	%r14, %rdi
	movl	20(%rax), %esi
	call	udata_readInt32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	movq	-72(%rbp), %rax
	movl	28(%rax), %esi
	call	udata_readInt32_67@PLT
	leaq	0(%r13,%rbx), %rcx
	movq	%r12, %r8
	movq	%r14, %rdi
	leaq	(%r15,%rbx), %rsi
	leal	(%rax,%rax), %edx
	movl	%eax, -64(%rbp)
	call	*48(%r14)
	movq	-72(%rbp), %rbx
	movq	%r14, %rdi
	movl	24(%rbx), %esi
	call	udata_readInt32_67@PLT
	movl	-64(%rbp), %r11d
	movq	%r12, %r8
	movq	%r14, %rdi
	movl	%eax, %eax
	leal	0(,%r11,4), %edx
	leaq	0(%r13,%rax), %rcx
	leaq	(%r15,%rax), %rsi
	call	*56(%r14)
	movl	40(%rbx), %esi
	movq	%r14, %rdi
	movq	%rbx, -72(%rbp)
	call	udata_readInt32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	movq	-72(%rbp), %rax
	movl	48(%rax), %esi
	call	udata_readInt32_67@PLT
	leaq	0(%r13,%rbx), %rcx
	movq	%r12, %r8
	movq	%r14, %rdi
	leal	(%rax,%rax), %edx
	leaq	(%r15,%rbx), %rsi
	call	*48(%r14)
	movq	-72(%rbp), %rbx
	movq	%r14, %rdi
	movl	52(%rbx), %esi
	call	udata_readInt32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	movq	-72(%rbp), %rax
	movl	56(%rax), %esi
	call	udata_readInt32_67@PLT
	leaq	0(%r13,%rbx), %rcx
	movq	%r12, %r8
	movq	%r14, %rdi
	leal	(%rax,%rax), %edx
	leaq	(%r15,%rbx), %rsi
	call	*48(%r14)
	movq	-72(%rbp), %rbx
	movq	%r14, %rdi
	movl	60(%rbx), %esi
	call	udata_readInt32_67@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	movq	-72(%rbp), %rax
	movl	64(%rax), %esi
	call	udata_readInt32_67@PLT
	leaq	0(%r13,%rbx), %rcx
	movq	%r12, %r8
	movq	%r14, %rdi
	leal	0(,%rax,4), %edx
	leaq	(%r15,%rbx), %rsi
	call	*56(%r14)
	movq	-72(%rbp), %rax
	movq	%r14, %rdi
	movl	(%rax), %esi
	call	udata_readInt32_67@PLT
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	leal	0(,%rax,4), %edx
	movq	%r14, %rdi
	call	*56(%r14)
	jmp	.L552
.L545:
	movzbl	79(%rbx), %eax
	movq	%r9, -128(%rbp)
	movq	%r12, %r8
	movq	%r14, %rdi
	movl	%r10d, -120(%rbp)
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%ebx, %ebx
	andl	$-2048, %ebx
	addl	$2176, %ebx
	movl	%ebx, %edx
	call	*48(%r14)
	movl	-112(%rbp), %eax
	movq	-128(%rbp), %r9
	movq	%r12, %r8
	movq	-64(%rbp), %rdi
	movl	-84(%rbp), %edx
	leal	(%rax,%rbx), %esi
	movq	%r9, -112(%rbp)
	leaq	(%rdi,%rsi), %rcx
	subl	%esi, %edx
	movq	%r14, %rdi
	addq	%r9, %rsi
	call	*56(%r14)
	cmpb	$0, -104(%rbp)
	movl	$0, %eax
	cmove	-80(%rbp), %eax
	cmpl	$3, %r13d
	movq	-112(%rbp), %r9
	movl	%eax, -80(%rbp)
	je	.L548
	movl	-120(%rbp), %r10d
	testb	$-4, %r10b
	je	.L599
	movl	%r10d, %eax
	andl	$251, %eax
	cmpl	$8, %eax
	je	.L550
.L551:
	movl	-136(%rbp), %edx
	testl	%edx, %edx
	je	.L544
	movl	-80(%rbp), %esi
	movl	-84(%rbp), %eax
	movq	%r9, -80(%rbp)
	movq	%r12, %r8
	movq	%r14, %rdi
	addl	%esi, %eax
	movq	-64(%rbp), %rsi
	leaq	(%rsi,%rax), %rcx
	leaq	(%r9,%rax), %rsi
	call	*48(%r14)
	movq	-80(%rbp), %r9
	jmp	.L544
.L597:
	movq	%r9, -80(%rbp)
	movq	%rsi, %rdi
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r12, %r8
	repnz scasb
	movq	%r14, %rdi
	movq	%rcx, %rax
	movq	%r11, %rcx
	notq	%rax
	leaq	-1(%rax), %rdx
	call	*72(%r14)
	movq	-80(%rbp), %r9
	jmp	.L544
.L599:
	subl	$1, %r13d
	jne	.L551
.L550:
	movl	-84(%rbp), %eax
	movq	-64(%rbp), %rsi
	movq	%r9, -104(%rbp)
	movq	%r12, %r8
	movl	-80(%rbp), %edx
	movq	%r14, %rdi
	leaq	(%rsi,%rax), %rcx
	leaq	(%r9,%rax), %rsi
	call	*48(%r14)
	movq	-104(%rbp), %r9
	jmp	.L551
.L548:
	movl	-84(%rbp), %eax
	movq	-64(%rbp), %rsi
	movq	%r9, -104(%rbp)
	movq	%r12, %r8
	movl	-80(%rbp), %edx
	movq	%r14, %rdi
	leaq	(%rsi,%rax), %rcx
	leaq	(%r9,%rax), %rsi
	call	*56(%r14)
	movq	-104(%rbp), %r9
	jmp	.L551
	.cfi_endproc
.LFE2865:
	.size	ucnv_swap_67, .-ucnv_swap_67
	.section	.rodata
	.type	_ZL9DATA_TYPE, @object
	.size	_ZL9DATA_TYPE, 4
_ZL9DATA_TYPE:
	.string	"cnv"
	.local	_ZL28gAvailableConvertersInitOnce
	.comm	_ZL28gAvailableConvertersInitOnce,8,8
	.local	_ZL24gAvailableConverterCount
	.comm	_ZL24gAvailableConverterCount,2,2
	.local	_ZL20gAvailableConverters
	.comm	_ZL20gAvailableConverters,8,8
	.local	_ZL13cnvCacheMutex
	.comm	_ZL13cnvCacheMutex,56,32
	.local	_ZL21SHARED_DATA_HASHTABLE
	.comm	_ZL21SHARED_DATA_HASHTABLE,8,8
	.section	.rodata.str1.1
.LC15:
	.string	"bocu1"
.LC16:
	.string	"cesu8"
.LC17:
	.string	"hz"
.LC18:
	.string	"imapmailboxname"
.LC19:
	.string	"iscii"
.LC20:
	.string	"iso2022"
.LC21:
	.string	"iso88591"
.LC22:
	.string	"lmbcs1"
.LC23:
	.string	"lmbcs11"
.LC24:
	.string	"lmbcs16"
.LC25:
	.string	"lmbcs17"
.LC26:
	.string	"lmbcs18"
.LC27:
	.string	"lmbcs19"
.LC28:
	.string	"lmbcs2"
.LC29:
	.string	"lmbcs3"
.LC30:
	.string	"lmbcs4"
.LC31:
	.string	"lmbcs5"
.LC32:
	.string	"lmbcs8"
.LC33:
	.string	"scsu"
.LC34:
	.string	"usascii"
.LC35:
	.string	"utf16"
.LC36:
	.string	"utf16be"
.LC37:
	.string	"utf16le"
.LC38:
	.string	"utf16oppositeendian"
.LC39:
	.string	"utf16platformendian"
.LC40:
	.string	"utf32"
.LC41:
	.string	"utf32be"
.LC42:
	.string	"utf32le"
.LC43:
	.string	"utf32oppositeendian"
.LC44:
	.string	"utf32platformendian"
.LC45:
	.string	"utf7"
.LC46:
	.string	"utf8"
.LC47:
	.string	"x11compoundtext"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL11cnvNameType, @object
	.size	_ZL11cnvNameType, 544
_ZL11cnvNameType:
	.quad	.LC15
	.long	28
	.zero	4
	.quad	.LC16
	.long	31
	.zero	4
	.quad	.LC17
	.long	23
	.zero	4
	.quad	.LC18
	.long	32
	.zero	4
	.quad	.LC19
	.long	25
	.zero	4
	.quad	.LC20
	.long	10
	.zero	4
	.quad	.LC21
	.long	3
	.zero	4
	.quad	.LC22
	.long	11
	.zero	4
	.quad	.LC23
	.long	18
	.zero	4
	.quad	.LC24
	.long	19
	.zero	4
	.quad	.LC25
	.long	20
	.zero	4
	.quad	.LC26
	.long	21
	.zero	4
	.quad	.LC27
	.long	22
	.zero	4
	.quad	.LC28
	.long	12
	.zero	4
	.quad	.LC29
	.long	13
	.zero	4
	.quad	.LC30
	.long	14
	.zero	4
	.quad	.LC31
	.long	15
	.zero	4
	.quad	.LC3
	.long	16
	.zero	4
	.quad	.LC32
	.long	17
	.zero	4
	.quad	.LC33
	.long	24
	.zero	4
	.quad	.LC34
	.long	26
	.zero	4
	.quad	.LC35
	.long	29
	.zero	4
	.quad	.LC36
	.long	5
	.zero	4
	.quad	.LC37
	.long	6
	.zero	4
	.quad	.LC38
	.long	5
	.zero	4
	.quad	.LC39
	.long	6
	.zero	4
	.quad	.LC40
	.long	30
	.zero	4
	.quad	.LC41
	.long	7
	.zero	4
	.quad	.LC42
	.long	8
	.zero	4
	.quad	.LC43
	.long	7
	.zero	4
	.quad	.LC44
	.long	8
	.zero	4
	.quad	.LC45
	.long	27
	.zero	4
	.quad	.LC46
	.long	4
	.zero	4
	.quad	.LC47
	.long	33
	.zero	4
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL13converterData, @object
	.size	_ZL13converterData, 272
_ZL13converterData:
	.quad	0
	.quad	0
	.quad	_MBCSData_67
	.quad	_Latin1Data_67
	.quad	_UTF8Data_67
	.quad	_UTF16BEData_67
	.quad	_UTF16LEData_67
	.quad	_UTF32BEData_67
	.quad	_UTF32LEData_67
	.quad	0
	.quad	_ISO2022Data_67
	.quad	_LMBCSData1_67
	.quad	_LMBCSData2_67
	.quad	_LMBCSData3_67
	.quad	_LMBCSData4_67
	.quad	_LMBCSData5_67
	.quad	_LMBCSData6_67
	.quad	_LMBCSData8_67
	.quad	_LMBCSData11_67
	.quad	_LMBCSData16_67
	.quad	_LMBCSData17_67
	.quad	_LMBCSData18_67
	.quad	_LMBCSData19_67
	.quad	_HZData_67
	.quad	_SCSUData_67
	.quad	_ISCIIData_67
	.quad	_ASCIIData_67
	.quad	_UTF7Data_67
	.quad	_Bocu1Data_67
	.quad	_UTF16Data_67
	.quad	_UTF32Data_67
	.quad	_CESU8Data_67
	.quad	_IMAPData_67
	.quad	_CompoundTextData_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
