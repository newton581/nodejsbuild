	.file	"ustr_cnv.cpp"
	.text
	.p2align 4
	.globl	u_getDefaultConverter_67
	.type	u_getDefaultConverter_67, @function
u_getDefaultConverter_67:
.LFB2785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	je	.L2
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZL17gDefaultConverter(%rip), %r12
	testq	%r12, %r12
	je	.L7
	movq	$0, _ZL17gDefaultConverter(%rip)
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L1:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L2:
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	ucnv_open_67@PLT
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	ucnv_close_67@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2785:
	.size	u_getDefaultConverter_67, .-u_getDefaultConverter_67
	.p2align 4
	.globl	u_releaseDefaultConverter_67
	.type	u_releaseDefaultConverter_67, @function
u_releaseDefaultConverter_67:
.LFB2786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	je	.L17
.L9:
	testq	%r12, %r12
	je	.L8
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ucnv_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L10
	call	ucnv_reset_67@PLT
.L10:
	call	ucnv_enableCleanup_67@PLT
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	je	.L18
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r12, _ZL17gDefaultConverter(%rip)
	addq	$8, %rsp
	xorl	%edi, %edi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.cfi_endproc
.LFE2786:
	.size	u_releaseDefaultConverter_67, .-u_releaseDefaultConverter_67
	.p2align 4
	.globl	u_flushDefaultConverter_67
	.type	u_flushDefaultConverter_67, @function
u_flushDefaultConverter_67:
.LFB2787:
	.cfi_startproc
	endbr64
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	je	.L19
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	umtx_lock_67@PLT
	movq	_ZL17gDefaultConverter(%rip), %r12
	testq	%r12, %r12
	je	.L25
	movq	$0, _ZL17gDefaultConverter(%rip)
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ucnv_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	addq	$8, %rsp
	xorl	%edi, %edi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.cfi_endproc
.LFE2787:
	.size	u_flushDefaultConverter_67, .-u_flushDefaultConverter_67
	.p2align 4
	.globl	u_uastrncpy_67
	.type	u_uastrncpy_67, @function
u_uastrncpy_67:
.LFB2789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	%edx, %rbx
	subq	$48, %rsp
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	movq	%rdi, -48(%rbp)
	movl	$0, -52(%rbp)
	je	.L27
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZL17gDefaultConverter(%rip), %r13
	testq	%r13, %r13
	je	.L59
	xorl	%edi, %edi
	leaq	-52(%rbp), %r14
	movq	$0, _ZL17gDefaultConverter(%rip)
	call	umtx_unlock_67@PLT
	movl	-52(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L29
.L32:
	xorl	%r8d, %r8d
	movw	%r8w, (%r12)
.L35:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L27:
	leaq	-52(%rbp), %r14
	xorl	%edi, %edi
	movq	%r14, %rsi
	call	ucnv_open_67@PLT
	movl	-52(%rbp), %r9d
	movq	%rax, %r13
	testl	%r9d, %r9d
	jg	.L61
	testq	%rax, %rax
	je	.L32
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r13, %rdi
	call	ucnv_reset_67@PLT
	movq	-72(%rbp), %r8
	testq	%r8, %r8
	je	.L34
	testl	%ebx, %ebx
	je	.L34
	leal	-1(%rbx), %esi
	xorl	%edx, %edx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L36:
	leal	1(%rdx), %ecx
	leaq	1(%rdx), %rax
	cmpq	%rdx, %rsi
	je	.L62
	movq	%rax, %rdx
.L37:
	cmpb	$0, (%r8,%rdx)
	movslq	%edx, %rax
	jne	.L36
	addq	%rax, %r8
.L34:
	pushq	%r14
	leaq	(%r12,%rbx,2), %rbx
	leaq	-48(%rbp), %rsi
	xorl	%r9d, %r9d
	pushq	$1
	leaq	-72(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	ucnv_toUnicode_67@PLT
	movq	%r13, %rdi
	call	ucnv_reset_67@PLT
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	popq	%rsi
	popq	%rdi
	je	.L63
.L38:
	movq	%r13, %rdi
	call	ucnv_close_67@PLT
.L40:
	movl	-52(%rbp), %eax
	cmpl	$15, %eax
	je	.L41
	testl	%eax, %eax
	jle	.L41
	xorl	%ecx, %ecx
	movw	%cx, (%r12)
.L41:
	movq	-48(%rbp), %rax
	cmpq	%rax, %rbx
	jbe	.L35
	xorl	%edx, %edx
	movw	%dx, (%rax)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%rax, %rdi
	call	ucnv_close_67@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r13, %rdi
	call	ucnv_reset_67@PLT
	call	ucnv_enableCleanup_67@PLT
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	je	.L64
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L62:
	movslq	%ecx, %rcx
	addq	%rcx, %r8
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%edi, %edi
	movq	%r13, _ZL17gDefaultConverter(%rip)
	call	umtx_unlock_67@PLT
	jmp	.L40
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2789:
	.size	u_uastrncpy_67, .-u_uastrncpy_67
	.p2align 4
	.globl	u_uastrcpy_67
	.type	u_uastrcpy_67, @function
u_uastrcpy_67:
.LFB2790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	movl	$0, -44(%rbp)
	je	.L66
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZL17gDefaultConverter(%rip), %r13
	testq	%r13, %r13
	je	.L83
	xorl	%edi, %edi
	leaq	-44(%rbp), %rbx
	movq	$0, _ZL17gDefaultConverter(%rip)
	call	umtx_unlock_67@PLT
	movl	-44(%rbp), %esi
	testl	%esi, %esi
	jle	.L68
.L71:
	xorl	%eax, %eax
	movw	%ax, (%r12)
.L75:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L66:
	leaq	-44(%rbp), %rbx
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	ucnv_open_67@PLT
	movl	-44(%rbp), %ecx
	movq	%rax, %r13
	testl	%ecx, %ecx
	jg	.L85
	testq	%rax, %rax
	je	.L71
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rbx, %r9
	movq	%r14, %rcx
	movl	$268435455, %edx
	movl	%eax, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	ucnv_toUChars_67@PLT
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	je	.L86
.L72:
	movq	%r13, %rdi
	call	ucnv_close_67@PLT
.L74:
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jg	.L71
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rax, %rdi
	call	ucnv_close_67@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r13, %rdi
	call	ucnv_reset_67@PLT
	call	ucnv_enableCleanup_67@PLT
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	je	.L87
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L87:
	xorl	%edi, %edi
	movq	%r13, _ZL17gDefaultConverter(%rip)
	call	umtx_unlock_67@PLT
	jmp	.L74
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2790:
	.size	u_uastrcpy_67, .-u_uastrcpy_67
	.p2align 4
	.globl	u_austrncpy_67
	.type	u_austrncpy_67, @function
u_austrncpy_67:
.LFB2792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	%edx, %rbx
	subq	$48, %rsp
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	movq	%rdi, -48(%rbp)
	movl	$0, -52(%rbp)
	je	.L89
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZL17gDefaultConverter(%rip), %r13
	testq	%r13, %r13
	je	.L121
	xorl	%edi, %edi
	leaq	-52(%rbp), %r14
	movq	$0, _ZL17gDefaultConverter(%rip)
	call	umtx_unlock_67@PLT
	movl	-52(%rbp), %esi
	testl	%esi, %esi
	jle	.L91
.L94:
	movb	$0, (%r12)
.L97:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L89:
	leaq	-52(%rbp), %r14
	xorl	%edi, %edi
	movq	%r14, %rsi
	call	ucnv_open_67@PLT
	movl	-52(%rbp), %ecx
	movq	%rax, %r13
	testl	%ecx, %ecx
	jg	.L123
	testq	%rax, %rax
	je	.L94
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r13, %rdi
	call	ucnv_reset_67@PLT
	movq	-72(%rbp), %r8
	testq	%r8, %r8
	je	.L96
	testl	%ebx, %ebx
	je	.L96
	leal	-1(%rbx), %esi
	xorl	%edx, %edx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L98:
	leal	1(%rdx), %ecx
	leaq	1(%rdx), %rax
	cmpq	%rdx, %rsi
	je	.L124
	movq	%rax, %rdx
.L99:
	cmpw	$0, (%r8,%rdx,2)
	movslq	%edx, %rax
	jne	.L98
	leaq	(%r8,%rax,2), %r8
.L96:
	pushq	%r14
	addq	%r12, %rbx
	xorl	%r9d, %r9d
	leaq	-72(%rbp), %rcx
	pushq	$1
	movq	%rbx, %rdx
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	ucnv_fromUnicode_67@PLT
	movq	%r13, %rdi
	call	ucnv_reset_67@PLT
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	popq	%rax
	popq	%rdx
	je	.L125
.L100:
	movq	%r13, %rdi
	call	ucnv_close_67@PLT
.L102:
	movl	-52(%rbp), %eax
	cmpl	$15, %eax
	je	.L103
	testl	%eax, %eax
	jle	.L103
	movb	$0, (%r12)
.L103:
	movq	-48(%rbp), %rax
	cmpq	%rax, %rbx
	jbe	.L97
	movb	$0, (%rax)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%rax, %rdi
	call	ucnv_close_67@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%r13, %rdi
	call	ucnv_reset_67@PLT
	call	ucnv_enableCleanup_67@PLT
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	je	.L126
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L124:
	movslq	%ecx, %rcx
	leaq	(%r8,%rcx,2), %r8
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L126:
	xorl	%edi, %edi
	movq	%r13, _ZL17gDefaultConverter(%rip)
	call	umtx_unlock_67@PLT
	jmp	.L102
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2792:
	.size	u_austrncpy_67, .-u_austrncpy_67
	.p2align 4
	.globl	u_austrcpy_67
	.type	u_austrcpy_67, @function
u_austrcpy_67:
.LFB2793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	movl	$0, -44(%rbp)
	je	.L128
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZL17gDefaultConverter(%rip), %r13
	testq	%r13, %r13
	je	.L145
	xorl	%edi, %edi
	leaq	-44(%rbp), %rbx
	movq	$0, _ZL17gDefaultConverter(%rip)
	call	umtx_unlock_67@PLT
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jle	.L130
.L133:
	movb	$0, (%r12)
.L137:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L128:
	leaq	-44(%rbp), %rbx
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	ucnv_open_67@PLT
	movq	%rax, %r13
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L147
	testq	%r13, %r13
	je	.L133
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%rbx, %r9
	movl	$-1, %r8d
	movq	%r14, %rcx
	movq	%r12, %rsi
	movl	$268435455, %edx
	movq	%r13, %rdi
	call	ucnv_fromUChars_67@PLT
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	movslq	%eax, %rbx
	je	.L148
.L134:
	movq	%r13, %rdi
	call	ucnv_close_67@PLT
.L136:
	movb	$0, (%r12,%rbx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r13, %rdi
	call	ucnv_close_67@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r13, %rdi
	call	ucnv_reset_67@PLT
	call	ucnv_enableCleanup_67@PLT
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	cmpq	$0, _ZL17gDefaultConverter(%rip)
	je	.L149
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L149:
	xorl	%edi, %edi
	movq	%r13, _ZL17gDefaultConverter(%rip)
	call	umtx_unlock_67@PLT
	jmp	.L136
.L146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2793:
	.size	u_austrcpy_67, .-u_austrcpy_67
	.local	_ZL17gDefaultConverter
	.comm	_ZL17gDefaultConverter,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
