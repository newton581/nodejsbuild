	.file	"uresbund.cpp"
	.text
	.p2align 4
	.type	_ZL21ures_loc_countLocalesP12UEnumerationP10UErrorCode, @function
_ZL21ures_loc_countLocalesP12UEnumerationP10UErrorCode:
.LFB3232:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L3
	movl	192(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3232:
	.size	_ZL21ures_loc_countLocalesP12UEnumerationP10UErrorCode, .-_ZL21ures_loc_countLocalesP12UEnumerationP10UErrorCode
	.p2align 4
	.type	ures_loc_resetLocales, @function
ures_loc_resetLocales:
.LFB3234:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L5
	movl	$-1, 188(%rax)
.L5:
	ret
	.cfi_endproc
.LFE3234:
	.size	ures_loc_resetLocales, .-ures_loc_resetLocales
	.p2align 4
	.type	_ZL10entryCloseP18UResourceDataEntry, @function
_ZL10entryCloseP18UResourceDataEntry:
.LFB3181:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	_ZL9resbMutex(%rip), %rdi
	subq	$8, %rsp
	call	umtx_lock_67@PLT
	testq	%rbx, %rbx
	je	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rbx, %rax
	movq	16(%rbx), %rbx
	subl	$1, 108(%rax)
	testq	%rbx, %rbx
	jne	.L12
.L11:
	addq	$8, %rsp
	leaq	_ZL9resbMutex(%rip), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.cfi_endproc
.LFE3181:
	.size	_ZL10entryCloseP18UResourceDataEntry, .-_ZL10entryCloseP18UResourceDataEntry
	.p2align 4
	.type	_ZL18ures_appendResPathP15UResourceBundlePKciP10UErrorCode, @function
_ZL18ures_appendResPathP15UResourceBundlePKciP10UErrorCode:
.LFB3182:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	movslq	168(%rdi), %r14
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L19
	addl	%r14d, %edx
	movl	%edx, 168(%r13)
	cmpl	$62, %edx
	jle	.L21
	addl	$1, %edx
	leaq	104(%r13), %r15
	movslq	%edx, %rsi
	cmpq	%r15, %rdi
	je	.L27
	call	uprv_realloc_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L26
	movq	%rax, 32(%r13)
.L21:
	addq	$8, %rsp
	addq	%r14, %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	strcpy@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	leaq	104(%r13), %rdi
	movb	$0, 104(%r13)
	movq	%rdi, 32(%r13)
	movl	%edx, 168(%r13)
	cmpl	$62, %edx
	jle	.L21
	addl	$1, %edx
	movq	%rdi, %r15
	movslq	%edx, %rsi
.L27:
	movq	%rsi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 32(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L26
	movq	%r15, %rsi
	call	strcpy@PLT
	movq	%rax, %rdi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$7, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3182:
	.size	_ZL18ures_appendResPathP15UResourceBundlePKciP10UErrorCode, .-_ZL18ures_appendResPathP15UResourceBundlePKciP10UErrorCode
	.p2align 4
	.type	_ZL11createCacheR10UErrorCode, @function
_ZL11createCacheR10UErrorCode:
.LFB3161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	leaq	_ZL14compareEntries8UElementS_(%rip), %rsi
	xorl	%edx, %edx
	leaq	_ZL9hashEntry8UElement(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uhash_open_67@PLT
	leaq	_ZL12ures_cleanupv(%rip), %rsi
	movl	$24, %edi
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%rax, _ZL5cache(%rip)
	jmp	ucln_common_registerCleanup_67@PLT
	.cfi_endproc
.LFE3161:
	.size	_ZL11createCacheR10UErrorCode, .-_ZL11createCacheR10UErrorCode
	.p2align 4
	.type	_ZL14compareEntries8UElementS_, @function
_ZL14compareEntries8UElementS_:
.LFB3154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %r8
	movq	(%rsi), %r9
	movq	8(%rdi), %r12
	movq	8(%rsi), %r13
	movq	%r8, %rdi
	movq	%r9, %rsi
	call	uhash_compareChars_67@PLT
	testb	%al, %al
	jne	.L41
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	uhash_compareChars_67@PLT
	popq	%r12
	popq	%r13
	testb	%al, %al
	popq	%rbp
	.cfi_def_cfa 7, 8
	setne	%al
	ret
	.cfi_endproc
.LFE3154:
	.size	_ZL14compareEntries8UElementS_, .-_ZL14compareEntries8UElementS_
	.p2align 4
	.type	_ZL9hashEntry8UElement, @function
_ZL9hashEntry8UElement:
.LFB3153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r8
	movq	8(%rdi), %r12
	movq	%r8, %rdi
	call	uhash_hashChars_67@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	uhash_hashChars_67@PLT
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	addl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3153:
	.size	_ZL9hashEntry8UElement, .-_ZL9hashEntry8UElement
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"root"
	.text
	.p2align 4
	.type	_ZL15getFallbackDataPK15UResourceBundlePPKcPP18UResourceDataEntryPjP10UErrorCode.isra.0, @function
_ZL15getFallbackDataPK15UResourceBundlePPKcPP18UResourceDataEntryPjP10UErrorCode.isra.0:
.LFB4243:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -72(%rbp)
	movq	%r9, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$-1, -60(%rbp)
	movl	$-1, (%r8)
	testq	%rdi, %rdi
	je	.L45
	movq	%rdx, %r14
	movl	112(%rdi), %edx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%r8, %r13
	testl	%edx, %edx
	je	.L46
	cmpb	$1, (%rsi)
	je	.L71
.L45:
	movq	-88(%rbp), %rax
	xorl	%r9d, %r9d
	movl	$2, (%rax)
.L44:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$56, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	xorl	%r15d, %r15d
.L47:
	leaq	-60(%rbp), %rdx
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L55:
	cmpl	$-1, 0(%r13)
	jne	.L51
.L56:
	movq	%rbx, %r12
.L52:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	je	.L54
	movl	112(%rbx), %eax
	testl	%eax, %eax
	jne	.L55
	movl	72(%rbx), %esi
	leaq	40(%rbx), %rdi
	movq	%r14, %rcx
	movq	%rdx, -80(%rbp)
	addl	$1, %r15d
	call	res_getTableItemByKey_67@PLT
	movq	-80(%rbp), %rdx
	cmpl	$-1, %eax
	movl	%eax, 0(%r13)
	je	.L56
.L51:
	movq	%rbx, %r12
.L53:
	leaq	40(%r12), %r9
	cmpl	$1, %r15d
	jle	.L50
	movq	%r9, -80(%rbp)
	call	uloc_getDefault_67@PLT
	movq	(%r12), %r13
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	movq	-80(%rbp), %r9
	testl	%eax, %eax
	je	.L57
	movl	$5, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L57
	movq	-88(%rbp), %rax
	movl	$-128, (%rax)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	40(%rdi), %r9
	movl	72(%rdi), %esi
	leaq	-60(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r9, %rdi
	movq	%r9, -80(%rbp)
	call	res_getTableItemByKey_67@PLT
	movq	-80(%rbp), %r9
	movl	%eax, 0(%r13)
	cmpb	$1, (%rbx)
	je	.L73
	cmpl	$-1, %eax
	je	.L45
.L50:
	movq	-72(%rbp), %rax
	movq	%r12, (%rax)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L73:
	cmpl	$-1, %eax
	jne	.L50
	movl	$1, %r15d
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-88(%rbp), %rax
	movl	$-127, (%rax)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L54:
	cmpl	$-1, 0(%r13)
	jne	.L53
	jmp	.L45
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4243:
	.size	_ZL15getFallbackDataPK15UResourceBundlePPKcPP18UResourceDataEntryPjP10UErrorCode.isra.0, .-_ZL15getFallbackDataPK15UResourceBundlePPKcPP18UResourceDataEntryPjP10UErrorCode.isra.0
	.p2align 4
	.type	_ZL12ures_cleanupv, @function
_ZL12ures_cleanupv:
.LFB3160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, _ZL5cache(%rip)
	je	.L75
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZL5cache(%rip), %rdi
	testq	%rdi, %rdi
	je	.L76
	leaq	-44(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$-1, -44(%rbp)
	xorl	%r13d, %r13d
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L109:
	movq	8(%rax), %r12
	movq	_ZL5cache(%rip), %rdi
	movl	108(%r12), %eax
	testl	%eax, %eax
	je	.L108
.L78:
	movq	%rbx, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L109
	testb	%r13b, %r13b
	je	.L76
	movq	_ZL5cache(%rip), %rdi
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	_ZL5cache(%rip), %rdi
	call	uhash_close_67@PLT
	movq	$0, _ZL5cache(%rip)
.L75:
	movl	$0, _ZL14gCacheInitOnce(%rip)
	mfence
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	call	uhash_removeElement_67@PLT
	leaq	40(%r12), %rdi
	call	res_unload_67@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L79
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L79
	call	uprv_free_67@PLT
.L79:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L80
	call	uprv_free_67@PLT
.L80:
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.L81
	subl	$1, 108(%rax)
.L81:
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L82
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L83
	subl	$1, 108(%rdx)
.L82:
	movq	%r12, %rdi
	movl	$1, %r13d
	call	uprv_free_67@PLT
	movq	_ZL5cache(%rip), %rdi
	jmp	.L78
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3160:
	.size	_ZL12ures_cleanupv, .-_ZL12ures_cleanupv
	.p2align 4
	.type	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0, @function
_ZL16ures_closeBundleP15UResourceBundlea.constprop.0:
.LFB4269:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L130
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L114
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%rbx, %rax
	movq	16(%rbx), %rbx
	subl	$1, 108(%rax)
	testq	%rbx, %rbx
	jne	.L115
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L114:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L116
	call	uprv_free_67@PLT
.L116:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L117
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L117
	call	uprv_free_67@PLT
.L117:
	cmpl	$19700503, 180(%r12)
	movq	$0, 32(%r12)
	movl	$0, 168(%r12)
	je	.L133
.L111:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	cmpl	$19641227, 184(%r12)
	jne	.L111
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L130:
	ret
	.cfi_endproc
.LFE4269:
	.size	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0, .-_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	.p2align 4
	.type	ures_copyResb_67.part.0, @function
ures_copyResb_67.part.0:
.LFB4260:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L176
	cmpl	$19700503, 180(%rdi)
	movq	%rdi, %rbx
	movl	$1, %r15d
	jne	.L138
	cmpl	$19641227, 184(%rdi)
	setne	%r15b
.L138:
	movq	8(%rbx), %r13
	testq	%r13, %r13
	je	.L139
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r13, %rax
	movq	16(%r13), %r13
	subl	$1, 108(%rax)
	testq	%r13, %r13
	jne	.L140
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L139:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L141
	call	uprv_free_67@PLT
.L141:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L142
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L142
	call	uprv_free_67@PLT
.L142:
	movq	$0, 32(%rbx)
	movl	$0, 168(%rbx)
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rbx)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rbx)
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rbx)
	movdqu	48(%r12), %xmm3
	movups	%xmm3, 48(%rbx)
	movdqu	64(%r12), %xmm4
	movups	%xmm4, 64(%rbx)
	movdqu	80(%r12), %xmm5
	movups	%xmm5, 80(%rbx)
	movdqu	96(%r12), %xmm6
	movups	%xmm6, 96(%rbx)
	movdqu	112(%r12), %xmm7
	movups	%xmm7, 112(%rbx)
	movdqu	128(%r12), %xmm0
	movups	%xmm0, 128(%rbx)
	movdqu	144(%r12), %xmm1
	movups	%xmm1, 144(%rbx)
	movdqu	160(%r12), %xmm2
	movups	%xmm2, 160(%rbx)
	movdqu	176(%r12), %xmm3
	movups	%xmm3, 176(%rbx)
	movq	192(%r12), %rax
	movq	$0, 32(%rbx)
	movq	%rax, 192(%rbx)
	movq	32(%r12), %rsi
	movl	$0, 168(%rbx)
	testq	%rsi, %rsi
	je	.L143
	movl	168(%r12), %edx
	movq	%r14, %rcx
	movq	%rbx, %rdi
	call	_ZL18ures_appendResPathP15UResourceBundlePKciP10UErrorCode
.L143:
	testb	%r15b, %r15b
	je	.L144
	movq	$0, 180(%rbx)
	movq	%rbx, %r12
.L145:
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L134
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	16(%rbx), %rax
	addl	$1, 108(%rbx)
	testq	%rax, %rax
	je	.L147
	.p2align 4,,10
	.p2align 3
.L146:
	addl	$1, 108(%rax)
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L146
.L147:
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L134:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movl	$200, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L177
	movdqu	(%r12), %xmm4
	movdqu	16(%r12), %xmm5
	movdqu	32(%r12), %xmm6
	movdqu	48(%r12), %xmm7
	movups	%xmm4, (%rax)
	movdqu	64(%r12), %xmm4
	movq	32(%r12), %rsi
	movups	%xmm5, 16(%rax)
	movdqu	80(%r12), %xmm5
	movups	%xmm6, 32(%rax)
	movdqu	96(%r12), %xmm6
	movups	%xmm7, 48(%rax)
	movdqu	112(%r12), %xmm7
	movups	%xmm4, 64(%rax)
	movdqu	128(%r12), %xmm4
	movups	%xmm5, 80(%rax)
	movdqu	144(%r12), %xmm5
	movups	%xmm6, 96(%rax)
	movdqu	160(%r12), %xmm6
	movups	%xmm7, 112(%rax)
	movdqu	176(%r12), %xmm7
	movups	%xmm4, 128(%rax)
	movups	%xmm5, 144(%rax)
	movups	%xmm6, 160(%rax)
	movups	%xmm7, 176(%rax)
	movq	192(%r12), %rax
	movq	$0, 32(%rbx)
	movq	%rax, 192(%rbx)
	movl	$0, 168(%rbx)
	testq	%rsi, %rsi
	je	.L144
	movl	168(%r12), %edx
	movq	%r14, %rcx
	movq	%rbx, %rdi
	call	_ZL18ures_appendResPathP15UResourceBundlePKciP10UErrorCode
	.p2align 4,,10
	.p2align 3
.L144:
	movabsq	$84358427638012695, %rax
	movq	%rbx, %r12
	movq	%rax, 180(%rbx)
	jmp	.L145
.L177:
	movl	$7, (%r14)
	xorl	%r12d, %r12d
	jmp	.L134
	.cfi_endproc
.LFE4260:
	.size	ures_copyResb_67.part.0, .-ures_copyResb_67.part.0
	.p2align 4
	.type	_ZL21ures_loc_closeLocalesP12UEnumeration, @function
_ZL21ures_loc_closeLocalesP12UEnumeration:
.LFB3231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r12
	movq	208(%r12), %rbx
	testq	%rbx, %rbx
	je	.L179
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%rbx, %rax
	movq	16(%rbx), %rbx
	subl	$1, 108(%rax)
	testq	%rbx, %rbx
	jne	.L180
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L179:
	movq	216(%r12), %rdi
	testq	%rdi, %rdi
	je	.L181
	call	uprv_free_67@PLT
.L181:
	movq	232(%r12), %rdi
	testq	%rdi, %rdi
	je	.L182
	leaq	304(%r12), %rax
	cmpq	%rax, %rdi
	je	.L182
	call	uprv_free_67@PLT
.L182:
	cmpl	$19700503, 380(%r12)
	movq	$0, 232(%r12)
	movl	$0, 368(%r12)
	je	.L195
.L183:
	movq	%r12, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	cmpl	$19641227, 384(%r12)
	jne	.L183
	leaq	200(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L183
	.cfi_endproc
.LFE3231:
	.size	_ZL21ures_loc_closeLocalesP12UEnumeration, .-_ZL21ures_loc_closeLocalesP12UEnumeration
	.section	.rodata.str1.1
.LC1:
	.string	"pool"
.LC2:
	.string	"%%ALIAS"
	.text
	.p2align 4
	.type	_ZL10init_entryPKcS0_P10UErrorCode, @function
_ZL10init_entryPKcS0_P10UErrorCode:
.LFB3164:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -312(%rbp)
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -64(%rbp)
	movl	$0, -296(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%r8d, %r8d
	jg	.L196
	movq	%rdi, %r13
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L274
	cmpb	$0, (%rdi)
	leaq	.LC0(%rip), %rax
	cmove	%rax, %r13
.L199:
	movq	_ZL5cache(%rip), %rdi
	movq	%r13, %xmm0
	leaq	-288(%rbp), %rsi
	movhps	-312(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	call	uhash_get_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L275
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%rbx, %rax
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L229
	movl	112(%rax), %edx
	addl	$1, 108(%rax)
	testl	%edx, %edx
	jne	.L276
.L196:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L277
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L196
	movl	%edx, (%r12)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L274:
	call	uloc_getDefault_67@PLT
	movq	%rax, %r13
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L275:
	movl	$120, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L278
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	andq	$-8, %rdi
	movq	$0, 112(%rax)
	movq	%rbx, %rax
	subq	%rdi, %rcx
	addl	$120, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%r13, %rdi
	call	strlen@PLT
	cmpl	$2, %eax
	jg	.L202
	leaq	104(%r14), %rdi
	movq	%rdi, (%r14)
.L203:
	movq	%r13, %rsi
	call	strcpy@PLT
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L222
	cmpq	$0, -312(%rbp)
	je	.L206
	movq	-312(%rbp), %rdi
	call	uprv_strdup_67@PLT
	movq	%rax, 8(%r14)
	testq	%rax, %rax
	je	.L279
.L207:
	movq	(%r14), %rdx
	leaq	40(%r14), %r13
	movq	%rax, %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	res_load_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L208
	cmpl	$7, %eax
	je	.L222
	movl	$-128, (%r12)
	movl	$-128, 112(%r14)
.L210:
	movq	_ZL5cache(%rip), %rdi
	movq	%r14, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L280
	movq	%r13, %rdi
	call	res_unload_67@PLT
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L224
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L224
	call	uprv_free_67@PLT
.L224:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L225
	call	uprv_free_67@PLT
.L225:
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.L226
	subl	$1, 108(%rax)
.L226:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L227
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L228
	subl	$1, 108(%rdx)
.L227:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L229
.L202:
	addl	$1, %eax
	movslq	%eax, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, (%r14)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L203
	movl	$7, (%r12)
.L222:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	xorl	%eax, %eax
	jmp	.L196
.L208:
	cmpb	$0, 98(%r14)
	jne	.L281
.L211:
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	res_getResource_67@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L210
	movq	%r13, %rdi
	leaq	-296(%rbp), %rdx
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L210
	movl	-296(%rbp), %eax
	testl	%eax, %eax
	jle	.L210
	leaq	-160(%rbp), %r15
	leal	1(%rax), %edx
	movq	%r15, %rsi
	call	u_UCharsToChars_67@PLT
	movq	-312(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZL10init_entryPKcS0_P10UErrorCode
	movq	%rax, 24(%r14)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L206:
	movq	8(%r14), %rax
	jmp	.L207
.L280:
	movq	_ZL5cache(%rip), %rdi
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	%r14, %rbx
	leaq	-292(%rbp), %rcx
	movl	$0, -292(%rbp)
	call	uhash_put_67@PLT
	movl	-292(%rbp), %eax
	testl	%eax, %eax
	jle	.L229
	movl	%eax, (%r12)
	movq	%r13, %rdi
	call	res_unload_67@PLT
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L219
	leaq	104(%r14), %rax
	cmpq	%rax, %rdi
	je	.L219
	call	uprv_free_67@PLT
.L219:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L220
	call	uprv_free_67@PLT
.L220:
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.L221
	subl	$1, 108(%rax)
.L221:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L222
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L223
	subl	$1, 108(%rdx)
	jmp	.L222
.L281:
	movq	8(%r14), %rsi
	movq	%r12, %rdx
	leaq	.LC1(%rip), %rdi
	call	_ZL10init_entryPKcS0_P10UErrorCode
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L282
	movq	%rax, 32(%r14)
.L215:
	movl	%edx, 112(%r14)
	jmp	.L210
.L282:
	testq	%rax, %rax
	je	.L213
	movl	112(%rax), %esi
	testl	%esi, %esi
	jne	.L213
	cmpb	$0, 97(%rax)
	jne	.L214
.L213:
	movl	$3, (%r12)
	movl	$3, %edx
	movq	%rax, 32(%r14)
	jmp	.L215
.L214:
	movq	48(%rax), %rdx
	movq	48(%r14), %rcx
	movq	%rax, 32(%r14)
	movl	32(%rdx), %esi
	cmpl	%esi, 32(%rcx)
	je	.L283
	movl	$3, (%r12)
	movl	$3, 112(%r14)
	jmp	.L210
.L279:
	movl	$7, (%r12)
	movq	%r14, %rdi
	movq	%rax, -312(%rbp)
	call	uprv_free_67@PLT
	movq	-312(%rbp), %rax
	jmp	.L196
.L283:
	movzbl	4(%rdx), %ecx
	movq	56(%rax), %rax
	leaq	4(%rdx,%rcx,4), %rdx
	movq	%rax, 80(%r14)
	movq	%rdx, 64(%r14)
	jmp	.L211
.L277:
	call	__stack_chk_fail@PLT
.L278:
	movl	$7, (%r12)
	xorl	%eax, %eax
	jmp	.L196
	.cfi_endproc
.LFE3164:
	.size	_ZL10init_entryPKcS0_P10UErrorCode, .-_ZL10init_entryPKcS0_P10UErrorCode
	.p2align 4
	.type	_ZL17findFirstExistingPKcPcPaS2_S2_P10UErrorCode, @function
_ZL17findFirstExistingPKcPcPaS2_S2_P10UErrorCode:
.LFB3166:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$40, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%r8, -64(%rbp)
	call	uloc_getDefault_67@PLT
	movb	$1, (%r15)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L285:
	movq	-72(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZL10init_entryPKcS0_P10UErrorCode
	movq	%rax, %r12
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L296
	movq	%r13, %rdx
.L288:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L288
	movl	%eax, %ecx
	movq	%r13, %rdi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	movq	%r14, %rsi
	sbbq	$3, %rdx
	subq	%r13, %rdx
	call	strncmp@PLT
	testl	%eax, %eax
	movq	-64(%rbp), %rax
	sete	(%rax)
	movl	112(%r12), %r15d
	testl	%r15d, %r15d
	je	.L290
	subl	$1, 108(%r12)
	xorl	%r12d, %r12d
	movl	$-128, (%rbx)
.L291:
	movq	%r13, %rsi
	leaq	.LC0(%rip), %rdi
	movl	$5, %ecx
	repz cmpsb
	movl	$95, %esi
	movq	%r13, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	movq	-80(%rbp), %rax
	sete	(%rax)
	call	strrchr@PLT
	testq	%rax, %rax
	je	.L306
	movb	$0, (%rax)
	movq	-56(%rbp), %rax
	testl	%r15d, %r15d
	sete	%cl
	movb	$1, (%rax)
	cmpb	$0, 0(%r13)
	je	.L307
	testl	%r15d, %r15d
	jne	.L285
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L306:
	movq	-56(%rbp), %rax
	movb	$0, (%rax)
.L284:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	strcpy@PLT
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L307:
	movl	$6581877, 0(%r13)
	cmpb	$0, (%rax)
	je	.L284
	testb	%cl, %cl
	je	.L285
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L296:
	xorl	%r12d, %r12d
	jmp	.L284
	.cfi_endproc
.LFE3166:
	.size	_ZL17findFirstExistingPKcPcPaS2_S2_P10UErrorCode, .-_ZL17findFirstExistingPKcPcPaS2_S2_P10UErrorCode
	.section	.rodata.str1.1
.LC3:
	.string	"%%ParentIsRoot"
.LC4:
	.string	"%%Parent"
	.text
	.p2align 4
	.type	_ZL21loadParentsExceptRootRP18UResourceDataEntryPciaS2_P10UErrorCode.part.0.constprop.0, @function
_ZL21loadParentsExceptRootRP18UResourceDataEntryPciaS2_P10UErrorCode.part.0.constprop.0:
.LFB4270:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 16(%rdi)
	jne	.L327
	movq	%rsi, %r13
	leaq	.LC0(%rip), %r15
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L316:
	movb	$0, (%rax)
	movq	(%rbx), %rdi
	cmpq	$0, 16(%rdi)
	jne	.L327
.L309:
	movzbl	96(%rdi), %r12d
	testb	%r12b, %r12b
	jne	.L327
	addq	$40, %rdi
	leaq	.LC3(%rip), %rsi
	call	res_getResource_67@PLT
	cmpl	$-1, %eax
	jne	.L327
	movq	(%rbx), %rax
	leaq	.LC4(%rip), %rsi
	leaq	-60(%rbp), %r14
	leaq	40(%rax), %rdi
	call	res_getResource_67@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L312
	movq	(%rbx), %rax
	leaq	-60(%rbp), %r14
	movl	$0, -60(%rbp)
	movq	%r14, %rdx
	leaq	40(%rax), %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L312
	movl	-60(%rbp), %edx
	leal	-1(%rdx), %eax
	cmpl	$155, %eax
	jbe	.L328
.L312:
	movq	(%rbx), %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	movl	$0, -60(%rbp)
	movq	8(%rax), %rsi
	call	_ZL10init_entryPKcS0_P10UErrorCode
	movl	-60(%rbp), %edx
	testl	%edx, %edx
	jg	.L329
	movq	(%rbx), %rdx
	movl	$95, %esi
	movq	%r13, %rdi
	movq	%rax, 16(%rdx)
	movq	%rax, (%rbx)
	call	strrchr@PLT
	testq	%rax, %rax
	jne	.L316
.L327:
	movl	$1, %r12d
.L308:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L330
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	addl	$1, %edx
	movq	%r13, %rsi
	call	u_UCharsToChars_67@PLT
	movl	$5, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L312
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L329:
	movq	-72(%rbp), %rax
	movl	%edx, (%rax)
	jmp	.L308
.L330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4270:
	.size	_ZL21loadParentsExceptRootRP18UResourceDataEntryPciaS2_P10UErrorCode.part.0.constprop.0, .-_ZL21loadParentsExceptRootRP18UResourceDataEntryPciaS2_P10UErrorCode.part.0.constprop.0
	.p2align 4
	.type	_ZL9entryOpenPKcS0_12UResOpenTypeP10UErrorCode, @function
_ZL9entryOpenPKcS0_12UResOpenTypeP10UErrorCode:
.LFB3178:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -384(%rbp)
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -339(%rbp)
	movl	$0, -336(%rbp)
	movq	$0, -328(%rbp)
	movb	$0, -338(%rbp)
	movb	$1, -337(%rbp)
	testl	%r8d, %r8d
	jle	.L386
.L332:
	xorl	%r12d, %r12d
.L331:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	addq	$344, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	movl	_ZL14gCacheInitOnce(%rip), %eax
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rcx, %rbx
	cmpl	$2, %eax
	jne	.L388
.L333:
	movl	4+_ZL14gCacheInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L334
	movl	%eax, (%rbx)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	_ZL14gCacheInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L333
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	_ZL14compareEntries8UElementS_(%rip), %rsi
	leaq	_ZL9hashEntry8UElement(%rip), %rdi
	call	uhash_open_67@PLT
	movl	$24, %edi
	leaq	_ZL12ures_cleanupv(%rip), %rsi
	movq	%rax, _ZL5cache(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZL14gCacheInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL14gCacheInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L334:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L332
	leaq	-224(%rbp), %r15
	movq	%r12, %rsi
	movl	$156, %edx
	movq	%r15, %rdi
	leaq	-338(%rbp), %r14
	call	strncpy@PLT
	leaq	_ZL9resbMutex(%rip), %rdi
	movb	$0, -68(%rbp)
	call	umtx_lock_67@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-336(%rbp), %r9
	leaq	-339(%rbp), %r8
	leaq	-337(%rbp), %rcx
	movq	%r9, -376(%rbp)
	movq	%r8, -368(%rbp)
	movq	%rcx, -360(%rbp)
	call	_ZL17findFirstExistingPKcPcPaS2_S2_P10UErrorCode
	cmpl	$7, -336(%rbp)
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %r8
	movq	-376(%rbp), %r9
	movq	%rax, %r12
	je	.L344
	testq	%rax, %rax
	je	.L337
	cmpb	$0, -337(%rbp)
	movl	(%rbx), %r8d
	movq	%rax, -328(%rbp)
	movzbl	-338(%rbp), %eax
	je	.L345
.L385:
	testb	%al, %al
	jne	.L342
	testl	%r8d, %r8d
	jg	.L336
	leaq	-320(%rbp), %rdx
	leaq	-328(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r15, %rsi
	call	_ZL21loadParentsExceptRootRP18UResourceDataEntryPciaS2_P10UErrorCode.part.0.constprop.0
	testb	%al, %al
	je	.L346
	movzbl	-338(%rbp), %eax
	movl	(%rbx), %r8d
	.p2align 4,,10
	.p2align 3
.L345:
	testb	%al, %al
	jne	.L342
	movq	-328(%rbp), %r9
	leaq	.LC0(%rip), %r10
	movl	$5, %ecx
	movq	%r10, %rdi
	movq	(%r9), %rsi
	movq	16(%r9), %rdx
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L349
	testq	%rdx, %rdx
	je	.L389
	.p2align 4,,10
	.p2align 3
.L354:
	addl	$1, 108(%rdx)
	movq	%rdx, %rax
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L354
	movq	%rax, -328(%rbp)
.L342:
	testl	%r8d, %r8d
	jg	.L336
	movl	-336(%rbp), %eax
	testl	%eax, %eax
	je	.L356
	movl	%eax, (%rbx)
.L356:
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L337:
	movl	-384(%rbp), %eax
	testl	%eax, %eax
	jne	.L343
	movzbl	-339(%rbp), %eax
	orb	-338(%rbp), %al
	je	.L390
.L343:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movb	$0, -220(%rbp)
	movl	$1953460082, -224(%rbp)
	call	_ZL17findFirstExistingPKcPcPaS2_S2_P10UErrorCode
	cmpl	$7, -336(%rbp)
	movq	%rax, %r12
	je	.L344
	testq	%rax, %rax
	jne	.L391
	movl	$2, (%rbx)
	.p2align 4,,10
	.p2align 3
.L336:
	xorl	%r12d, %r12d
.L392:
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L391:
	movq	%rax, -328(%rbp)
	movl	$-127, -336(%rbp)
.L348:
	cmpb	$0, -338(%rbp)
	movl	(%rbx), %r8d
	jne	.L342
	movq	16(%rax), %rdx
.L349:
	testq	%rdx, %rdx
	jne	.L354
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L344:
	movl	$7, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L392
.L346:
	movl	(%rbx), %r8d
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L390:
	movq	%r9, -376(%rbp)
	movq	%rcx, -368(%rbp)
	movq	%r8, -360(%rbp)
	call	uloc_getDefault_67@PLT
	movl	$157, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	__strcpy_chk@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	-376(%rbp), %r9
	movq	-360(%rbp), %r8
	movq	-368(%rbp), %rcx
	movq	%r9, -384(%rbp)
	movq	%r8, -376(%rbp)
	movq	%rcx, -360(%rbp)
	call	_ZL17findFirstExistingPKcPcPaS2_S2_P10UErrorCode
	cmpl	$7, -336(%rbp)
	movq	%rax, %r12
	je	.L344
	testq	%rax, %rax
	movq	-360(%rbp), %rcx
	movl	$-127, -336(%rbp)
	movq	-376(%rbp), %r8
	movq	-384(%rbp), %r9
	je	.L343
	cmpb	$0, -337(%rbp)
	movl	(%rbx), %r8d
	movq	%rax, -328(%rbp)
	movb	$1, -339(%rbp)
	movzbl	-338(%rbp), %eax
	je	.L345
	jmp	.L385
.L389:
	cmpb	$0, 96(%r12)
	jne	.L342
	testl	%r8d, %r8d
	jg	.L336
	movq	8(%r9), %rsi
	movq	%r10, %rdi
	leaq	-332(%rbp), %rdx
	movl	$0, -332(%rbp)
	call	_ZL10init_entryPKcS0_P10UErrorCode
	movl	-332(%rbp), %edx
	testl	%edx, %edx
	jg	.L393
	movq	-328(%rbp), %rdx
	movq	%rax, -328(%rbp)
	movq	%rax, 16(%rdx)
	jmp	.L348
.L393:
	movl	%edx, (%rbx)
	jmp	.L336
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3178:
	.size	_ZL9entryOpenPKcS0_12UResOpenTypeP10UErrorCode, .-_ZL9entryOpenPKcS0_12UResOpenTypeP10UErrorCode
	.p2align 4
	.type	_ZL15entryOpenDirectPKcS0_P10UErrorCode, @function
_ZL15entryOpenDirectPKcS0_P10UErrorCode:
.LFB3179:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L511
.L395:
	xorl	%r14d, %r14d
.L394:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L512
	addq	$472, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	movl	_ZL14gCacheInitOnce(%rip), %eax
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %rbx
	cmpl	$2, %eax
	jne	.L513
.L396:
	movl	4+_ZL14gCacheInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L397
	movl	%eax, (%rbx)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	_ZL14gCacheInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L396
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	_ZL14compareEntries8UElementS_(%rip), %rsi
	leaq	_ZL9hashEntry8UElement(%rip), %rdi
	call	uhash_open_67@PLT
	movl	$24, %edi
	leaq	_ZL12ures_cleanupv(%rip), %rsi
	movq	%rax, _ZL5cache(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZL14gCacheInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL14gCacheInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L397:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L395
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZL10init_entryPKcS0_P10UErrorCode
	movl	(%rbx), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	jle	.L514
	movq	$0, -472(%rbp)
	xorl	%r14d, %r14d
.L400:
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L514:
	movl	112(%rax), %eax
	testl	%eax, %eax
	jne	.L515
	leaq	.LC0(%rip), %r15
	movl	$5, %ecx
	movq	%r12, %rsi
	movq	%r14, -472(%rbp)
	movq	%r15, %rdi
	movq	16(%r14), %rax
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L401
	testq	%rax, %rax
	je	.L516
	.p2align 4,,10
	.p2align 3
.L439:
	addl	$1, 108(%rax)
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L439
	movq	%rdx, -472(%rbp)
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L515:
	subl	$1, 108(%r14)
	xorl	%r14d, %r14d
	movq	$0, -472(%rbp)
	jmp	.L400
.L516:
	cmpb	$0, 96(%r14)
	jne	.L400
	movq	%r12, %rdi
	call	strlen@PLT
	cmpq	$156, %rax
	ja	.L400
	leaq	-224(%rbp), %r13
	movq	%r12, %rsi
	leaq	1(%rax), %rdx
	movl	$157, %ecx
	movq	%r13, %rdi
	call	__memcpy_chk@PLT
	movl	$95, %esi
	movq	%r13, %rdi
	call	strrchr@PLT
	testq	%rax, %rax
	je	.L403
	movb	$0, (%rax)
	movl	$5, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L517
.L403:
	movq	(%r14), %rsi
	movl	$5, %ecx
	movq	%r15, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L441
	movq	%r14, %rax
.L442:
	movq	16(%rax), %rax
	.p2align 4,,10
	.p2align 3
.L401:
	testq	%rax, %rax
	jne	.L439
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L441:
	cmpq	$0, 16(%r14)
	je	.L518
.L444:
	movq	-472(%rbp), %rax
	jmp	.L442
.L512:
	call	__stack_chk_fail@PLT
.L517:
	xorl	%edx, %edx
	leaq	-472(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rsi
	call	_ZL21loadParentsExceptRootRP18UResourceDataEntryPciaS2_P10UErrorCode.part.0.constprop.0
	testb	%al, %al
	je	.L437
	movq	-472(%rbp), %r12
	leaq	.LC0(%rip), %rsi
	movq	(%r12), %rdi
	call	strcmp@PLT
	movl	%eax, %r9d
	movl	(%rbx), %eax
	testl	%r9d, %r9d
	je	.L405
	cmpq	$0, 16(%r12)
	jne	.L405
	testl	%eax, %eax
	jle	.L406
.L438:
	xorl	%r14d, %r14d
	jmp	.L400
.L523:
	movq	%r13, %rdi
	movl	%eax, -484(%rbp)
	call	res_unload_67@PLT
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L425
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L425
	call	uprv_free_67@PLT
.L425:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L426
	call	uprv_free_67@PLT
.L426:
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.L427
	subl	$1, 108(%rax)
.L427:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L509
.L429:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L429
	subl	$1, 108(%rdx)
.L509:
	movq	%r15, %rdi
	call	uprv_free_67@PLT
.L409:
	movl	-484(%rbp), %eax
	testl	%eax, %eax
	jg	.L519
	movq	-472(%rbp), %rax
	movq	%r12, -472(%rbp)
	movq	%r12, 16(%rax)
.L437:
	movl	(%rbx), %eax
.L405:
	testl	%eax, %eax
	jg	.L438
	jmp	.L444
.L518:
	movq	%r14, %r12
.L406:
	xorl	%r13d, %r13d
	movq	8(%r12), %rsi
	leaq	-320(%rbp), %rdi
	movl	$21, %ecx
	movl	%r13d, %eax
	pxor	%xmm0, %xmm0
	movl	$0, -484(%rbp)
	rep stosl
	movq	_ZL5cache(%rip), %rdi
	movq	%rsi, -504(%rbp)
	movq	%rsi, -456(%rbp)
	leaq	-464(%rbp), %rsi
	movl	$0, -480(%rbp)
	movq	%r15, -464(%rbp)
	movaps	%xmm0, -336(%rbp)
	call	uhash_get_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L520
.L407:
	movq	%r12, %rax
.L435:
	movq	%rax, %r12
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L435
	movl	112(%r12), %eax
	addl	$1, 108(%r12)
	testl	%eax, %eax
	je	.L409
	cmpl	$0, -484(%rbp)
	jg	.L409
	movl	%eax, -484(%rbp)
	jmp	.L409
.L520:
	movl	$120, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L521
	movq	%rax, %rdi
	movl	$30, %ecx
	movl	%r13d, %eax
	rep stosl
	movl	$5, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L510
	cmpl	$0, -484(%rbp)
	movl	$1953460082, (%rax)
	movb	$0, 4(%rax)
	jg	.L509
	movq	-504(%rbp), %rax
	testq	%rax, %rax
	je	.L413
	movq	%rax, %rdi
	call	uprv_strdup_67@PLT
	movq	%rax, 8(%r15)
	testq	%rax, %rax
	je	.L510
.L413:
	leaq	-484(%rbp), %rax
	movq	8(%r15), %rsi
	movq	(%r15), %rdx
	leaq	40(%r15), %r13
	movq	%rax, %rcx
	movq	%r13, %rdi
	movq	%rax, -512(%rbp)
	call	res_load_67@PLT
	movl	-484(%rbp), %eax
	testl	%eax, %eax
	jle	.L414
	cmpl	$7, %eax
	je	.L509
	movl	$-128, -484(%rbp)
	movl	$-128, 112(%r15)
.L416:
	movq	_ZL5cache(%rip), %rdi
	movq	%r15, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L522
	movq	%r13, %rdi
	call	res_unload_67@PLT
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L430
	leaq	104(%r15), %rax
	cmpq	%rax, %rdi
	je	.L430
	call	uprv_free_67@PLT
.L430:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L431
	call	uprv_free_67@PLT
.L431:
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.L432
	subl	$1, 108(%rax)
.L432:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L433
.L434:
	movq	%rax, %rdx
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L434
	subl	$1, 108(%rdx)
.L433:
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	jmp	.L407
.L522:
	movq	_ZL5cache(%rip), %rdi
	leaq	-476(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r15, %rsi
	movl	$0, -476(%rbp)
	call	uhash_put_67@PLT
	movl	-476(%rbp), %eax
	testl	%eax, %eax
	jg	.L523
	movq	%r15, %r12
	jmp	.L407
.L414:
	cmpb	$0, 98(%r15)
	jne	.L524
.L417:
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	res_getResource_67@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L416
	movq	%r13, %rdi
	leaq	-480(%rbp), %rdx
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L416
	movl	-480(%rbp), %eax
	testl	%eax, %eax
	jle	.L416
	leaq	-336(%rbp), %r12
	leal	1(%rax), %edx
	movq	%r12, %rsi
	call	u_UCharsToChars_67@PLT
	movq	-512(%rbp), %rdx
	movq	-504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZL10init_entryPKcS0_P10UErrorCode
	movq	%rax, 24(%r15)
	jmp	.L416
.L524:
	movq	8(%r15), %rsi
	movq	-512(%rbp), %rdx
	leaq	.LC1(%rip), %rdi
	call	_ZL10init_entryPKcS0_P10UErrorCode
	cmpl	$0, -484(%rbp)
	jle	.L525
.L418:
	movl	-484(%rbp), %edx
	movq	%rax, 32(%r15)
	testl	%edx, %edx
	jle	.L526
	movl	%edx, 112(%r15)
	jmp	.L416
.L510:
	movl	$7, -484(%rbp)
	jmp	.L509
.L519:
	movl	%eax, (%rbx)
	movl	(%rbx), %eax
	jmp	.L405
.L521:
	movl	$7, -484(%rbp)
	jmp	.L409
.L526:
	movq	48(%rax), %rdx
	movq	48(%r15), %rcx
	movl	32(%rdx), %esi
	cmpl	%esi, 32(%rcx)
	je	.L527
	movl	$3, -484(%rbp)
	movl	$3, 112(%r15)
	jmp	.L416
.L525:
	testq	%rax, %rax
	je	.L419
	cmpl	$0, 112(%rax)
	jne	.L419
	cmpb	$0, 97(%rax)
	jne	.L418
.L419:
	movl	$3, -484(%rbp)
	jmp	.L418
.L527:
	movzbl	4(%rdx), %ecx
	movq	56(%rax), %rax
	leaq	4(%rdx,%rcx,4), %rdx
	movq	%rax, 80(%r15)
	movq	%rdx, 64(%r15)
	jmp	.L417
	.cfi_endproc
.LFE3179:
	.size	_ZL15entryOpenDirectPKcS0_P10UErrorCode, .-_ZL15entryOpenDirectPKcS0_P10UErrorCode
	.p2align 4
	.type	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0, @function
_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0:
.LFB4271:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L542
	movq	%rdi, %r12
	movl	%edx, %r13d
	movq	%rsi, %rdi
	movq	%rcx, %rbx
	cmpl	$2, %edx
	je	.L531
	leaq	-208(%rbp), %r14
	movl	$157, %edx
	movq	%r14, %rsi
	call	uloc_getBaseName_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L532
	cmpl	$-124, %eax
	je	.L532
	movq	%r14, %rsi
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZL9entryOpenPKcS0_12UResOpenTypeP10UErrorCode
	movq	%rax, %r14
.L533:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L542
	testq	%r14, %r14
	je	.L543
	movl	$200, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L544
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	$0, 192(%rax)
	andq	$-8, %rdi
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$200, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%r14, 8(%r12)
	leaq	40(%r12), %rdi
	movabsq	$84358427638012695, %rax
	movq	%rax, 180(%r12)
	xorl	%eax, %eax
	movq	%r14, 24(%r12)
	movdqu	40(%r14), %xmm0
	movups	%xmm0, 40(%r12)
	movdqu	56(%r14), %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	72(%r14), %xmm2
	movups	%xmm2, 32(%rdi)
	movdqu	88(%r14), %xmm3
	movups	%xmm3, 48(%rdi)
	cmpl	$2, %r13d
	je	.L537
	cmpb	$0, 96(%r12)
	sete	%al
.L537:
	movl	72(%r12), %esi
	movb	%al, 176(%r12)
	movb	$1, 177(%r12)
	movl	%esi, 172(%r12)
	call	res_countArrayItems_67@PLT
	movl	$-1, 188(%r12)
	movl	%eax, 192(%r12)
.L528:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L545
	addq	$176, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	movl	$2, (%rbx)
	.p2align 4,,10
	.p2align 3
.L542:
	xorl	%r12d, %r12d
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L531:
	movq	%rcx, %rdx
	movq	%r12, %rdi
	call	_ZL15entryOpenDirectPKcS0_P10UErrorCode
	movq	%rax, %r14
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L532:
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L528
.L545:
	call	__stack_chk_fail@PLT
.L544:
	movq	%r14, %rdi
	call	_ZL10entryCloseP18UResourceDataEntry
	movl	$7, (%rbx)
	jmp	.L528
	.cfi_endproc
.LFE4271:
	.size	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0, .-_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	.section	.rodata.str1.1
.LC5:
	.string	"LOCALE"
.LC6:
	.string	"ICUDATA"
.LC7:
	.string	"/"
	.text
	.p2align 4
	.type	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0, @function
_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0:
.LFB4264:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$584, %rsp
	movq	32(%rbp), %rax
	movq	%rdi, -584(%rbp)
	movq	%rdx, -568(%rbp)
	movq	24(%rbp), %r12
	movl	%ecx, -592(%rbp)
	movq	%rax, -576(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, %eax
	shrl	$28, %eax
	cmpl	$3, %eax
	je	.L663
	testq	%r12, %r12
	je	.L664
	movq	8(%r12), %r15
	testq	%r15, %r15
	je	.L597
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r15, %rax
	movq	16(%r15), %r15
	subl	$1, 108(%rax)
	testq	%r15, %r15
	jne	.L598
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L597:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L599
	call	uprv_free_67@PLT
.L599:
	leaq	104(%r12), %r15
	cmpq	%r12, %r14
	je	.L596
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L600
	cmpq	%r15, %rdi
	je	.L600
	call	uprv_free_67@PLT
.L600:
	movq	$0, 32(%r12)
	movl	$0, 168(%r12)
.L596:
	movq	%rbx, 8(%r12)
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	16(%rbx), %rax
	addl	$1, 108(%rbx)
	testq	%rax, %rax
	je	.L605
	.p2align 4,,10
	.p2align 3
.L601:
	addl	$1, 108(%rax)
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L601
.L605:
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-568(%rbp), %rax
	xorl	%edx, %edx
	movl	$-1, 188(%r12)
	movw	%dx, 176(%r12)
	movq	%rax, (%r12)
	movq	24(%r14), %rax
	movq	32(%r14), %rsi
	movq	%rax, 24(%r12)
	testq	%rsi, %rsi
	je	.L604
	cmpq	%r12, %r14
	je	.L604
	movl	168(%r14), %edx
	movq	-576(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZL18ures_appendResPathP15UResourceBundlePKciP10UErrorCode
.L604:
	movq	-568(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L606
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	-576(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r14, %rcx
	call	_ZL18ures_appendResPathP15UResourceBundlePKciP10UErrorCode
	movq	32(%r12), %rax
	movslq	168(%r12), %rdx
	movq	%r14, %rcx
	cmpb	$47, -1(%rax,%rdx)
	jne	.L660
.L607:
	movl	$64, %edx
	cmpq	%r15, %rax
	je	.L665
.L610:
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r12, %r14
	call	memset@PLT
	leaq	40(%r12), %rdi
	movl	$64, %edx
	movq	$0, 16(%r12)
	movl	%r13d, 172(%r12)
	movq	-584(%rbp), %rsi
	call	memmove@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	res_countArrayItems_67@PLT
	movl	%eax, 192(%r12)
.L546:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L666
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	movl	-592(%rbp), %eax
	testl	%eax, %eax
	jns	.L608
	movq	32(%r12), %rax
.L667:
	movl	$64, %edx
	cmpq	%r15, %rax
	jne	.L610
.L665:
	movslq	168(%r12), %rax
	subq	%rax, %rdx
	addq	%rax, %r15
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L608:
	movl	-592(%rbp), %esi
	leaq	-320(%rbp), %r14
	movl	$10, %edx
	movq	%r14, %rdi
	call	T_CString_integerToString_67@PLT
	movq	-576(%rbp), %rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	movq	%rbx, %rcx
	call	_ZL18ures_appendResPathP15UResourceBundlePKciP10UErrorCode
	movq	32(%r12), %rax
	movslq	168(%r12), %rdx
	cmpb	$47, -1(%rax,%rdx)
	je	.L607
	movq	%rbx, %rcx
.L660:
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZL18ures_appendResPathP15UResourceBundlePKciP10UErrorCode
	movq	32(%r12), %rax
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L663:
	cmpl	$256, 16(%rbp)
	jne	.L668
	movq	-576(%rbp), %rax
	movq	%r12, %r14
	movl	$24, (%rax)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L668:
	leaq	-552(%rbp), %rdx
	movl	$0, -552(%rbp)
	call	res_getAlias_67@PLT
	movl	-552(%rbp), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	jle	.L549
	movq	32(%r14), %rdi
	addl	$1, %edx
	xorl	%r13d, %r13d
	movl	%edx, -552(%rbp)
	testq	%rdi, %rdi
	je	.L550
	movl	%edx, -584(%rbp)
	call	strlen@PLT
	movl	-584(%rbp), %edx
	leal	1(%rax), %r13d
.L550:
	cmpl	%r13d, %edx
	cmovge	%edx, %r13d
	cmpl	$200, %r13d
	jle	.L615
	movslq	%r13d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -600(%rbp)
	testq	%rax, %rax
	je	.L662
	movl	-552(%rbp), %edx
	movq	%rax, %rsi
.L551:
	movq	%r15, %rdi
	call	u_UCharsToChars_67@PLT
	movq	-600(%rbp), %rax
	cmpb	$47, (%rax)
	je	.L669
	movq	%rax, %rdi
	movl	$47, %esi
	call	strchr@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L561
	movb	$0, (%rax)
	movq	-600(%rbp), %rsi
	movl	$2, %edx
	leaq	-548(%rbp), %rcx
	movq	$0, -544(%rbp)
	movl	$0, -548(%rbp)
	call	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	leaq	1(%r15), %rcx
	movq	%rax, -616(%rbp)
	movl	-548(%rbp), %eax
	movq	%rcx, -608(%rbp)
	testl	%eax, %eax
	jg	.L563
.L611:
	movq	-616(%rbp), %rax
	movq	-608(%rbp), %rdi
	movq	8(%rax), %rbx
	leaq	-320(%rbp), %rax
	movq	%rax, -624(%rbp)
	movq	%rax, -536(%rbp)
	call	strlen@PLT
	cmpq	$255, %rax
	jbe	.L616
	leaq	1(%rax), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -592(%rbp)
	testq	%rax, %rax
	je	.L579
	movq	-536(%rbp), %r13
	movq	%rax, %rdi
.L578:
	movq	-608(%rbp), %rsi
	movq	%rbx, %r15
	call	strcpy@PLT
	movq	-616(%rbp), %r14
	leaq	-544(%rbp), %rax
	movq	-576(%rbp), %rbx
	movq	%rax, -584(%rbp)
	movq	%r13, %rax
	movq	%r14, %r13
.L587:
	movl	72(%r15), %r10d
	cmpb	$0, (%rax)
	movl	%r10d, %r14d
	je	.L670
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L671
.L583:
	movq	-584(%rbp), %rcx
	leaq	40(%r15), %rdi
	movl	%r14d, %esi
	leaq	-536(%rbp), %rdx
	movq	%rdi, -568(%rbp)
	call	res_findResource_67@PLT
	movl	%eax, %r14d
	cmpl	$-1, %eax
	je	.L618
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L619
	testq	%r13, %r13
	movq	-568(%rbp), %rdi
	je	.L672
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movq	%r13, %r9
	movq	%r15, %r8
	pushq	%rbx
	movq	-544(%rbp), %rdx
	movl	$-1, %ecx
	movl	%r14d, %esi
	addl	$1, %eax
	pushq	%r12
	pushq	%rax
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	movq	%rax, %r13
.L584:
	testq	%r13, %r13
	je	.L586
	movl	172(%r13), %r14d
	movq	8(%r13), %r15
.L586:
	movq	-536(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L673
	movl	(%rbx), %esi
	movq	%r13, %r12
	testl	%esi, %esi
	jle	.L583
.L671:
	cmpl	$-1, %r14d
	movl	%r14d, %r10d
	setne	%r14b
.L582:
	movq	-608(%rbp), %rsi
	movq	-592(%rbp), %rdi
	movl	%r10d, -568(%rbp)
	movq	16(%r15), %r15
	call	strcpy@PLT
	movq	-592(%rbp), %rax
	movl	-568(%rbp), %r10d
	testq	%r15, %r15
	movq	%rax, -536(%rbp)
	je	.L623
	testb	%r14b, %r14b
	je	.L587
.L623:
	movq	%r13, %r14
	cmpl	$-1, %r10d
	jne	.L589
	movq	-576(%rbp), %rax
	movq	%r12, %r14
	movl	$2, (%rax)
.L589:
	movq	-592(%rbp), %rdi
	movq	-624(%rbp), %rax
	leaq	-528(%rbp), %rbx
	cmpq	%rax, %rdi
	je	.L577
	call	uprv_free_67@PLT
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L669:
	leaq	1(%rax), %r8
	movl	$47, %esi
	movq	%r8, %rdi
	movq	%r8, -584(%rbp)
	call	strchr@PLT
	movq	-584(%rbp), %r8
	testq	%rax, %rax
	je	.L674
	movb	$0, (%rax)
	addq	$1, %rax
	movq	%rax, -608(%rbp)
.L556:
	movl	$7, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L557
	movq	24(%r14), %rax
	movq	8(%rbx), %r8
	movq	(%rax), %rsi
.L558:
	leaq	-548(%rbp), %rcx
	movl	$2, %edx
	movq	%r8, %rdi
	movq	$0, -544(%rbp)
	movl	$0, -548(%rbp)
	call	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	movq	%rax, -616(%rbp)
	movl	-548(%rbp), %eax
	testl	%eax, %eax
	jle	.L611
.L563:
	movq	-576(%rbp), %rcx
	movq	%r12, %r14
	leaq	-528(%rbp), %rbx
	movl	%eax, (%rcx)
.L577:
	movq	-600(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L591
	movq	%rax, %rdi
	call	uprv_free_67@PLT
.L591:
	movq	-616(%rbp), %rdi
	cmpq	%rdi, %r14
	je	.L546
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L664:
	movl	$200, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L662
	movabsq	$84358427638012695, %rax
	leaq	104(%r12), %r15
	movq	$0, 32(%r12)
	movq	%rax, 180(%r12)
	movl	$0, 168(%r12)
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L615:
	leaq	-528(%rbp), %rax
	movl	$200, %r13d
	movq	%rax, -600(%rbp)
	movq	%rax, %rsi
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L619:
	movq	%r12, %r13
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L618:
	movl	%eax, %r10d
	xorl	%r14d, %r14d
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L673:
	cmpl	$-1, %r14d
	movl	%r14d, %r10d
	movq	%r13, %r12
	setne	%r14b
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L549:
	movq	-576(%rbp), %rax
	movq	%r12, %r14
	movl	$1, (%rax)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L672:
	movl	$1, (%rbx)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L561:
	movq	$0, -544(%rbp)
	movq	-600(%rbp), %rsi
	leaq	-548(%rbp), %rcx
	movl	$2, %edx
	movl	$0, -548(%rbp)
.L659:
	call	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	movq	%rax, -616(%rbp)
	movl	-548(%rbp), %eax
	testl	%eax, %eax
	jg	.L563
	movq	32(%r14), %rsi
	movq	%rsi, -536(%rbp)
	testq	%rsi, %rsi
	je	.L564
	movq	-600(%rbp), %rbx
	movq	%rbx, %rdi
	call	strcpy@PLT
	movq	-616(%rbp), %rax
	leaq	-544(%rbp), %rcx
	leaq	-536(%rbp), %rdx
	movq	%rbx, -536(%rbp)
	movl	172(%rax), %esi
	leaq	40(%rax), %rdi
	call	res_findResource_67@PLT
	movl	%eax, %r14d
.L565:
	movq	-568(%rbp), %rax
	testq	%rax, %rax
	je	.L566
	movq	%rax, %rdi
	leaq	-528(%rbp), %rbx
	call	strlen@PLT
	addl	$1, %eax
	movl	%eax, -552(%rbp)
	movslq	%eax, %rdx
	cmpl	%r13d, %eax
	jle	.L567
	cmpq	%rbx, -600(%rbp)
	je	.L675
	movq	-600(%rbp), %rdi
	movq	%rdx, %rsi
	call	uprv_realloc_67@PLT
	movq	%rax, -600(%rbp)
.L569:
	cmpq	$0, -600(%rbp)
	je	.L570
	movslq	-552(%rbp), %rdx
.L567:
	movq	-600(%rbp), %r15
	movq	-568(%rbp), %rsi
	movq	%r15, %rdi
	call	memcpy@PLT
	movq	-616(%rbp), %rax
	movl	%r14d, %esi
	leaq	-544(%rbp), %rcx
	leaq	-536(%rbp), %rdx
	movq	%r15, -536(%rbp)
	leaq	40(%rax), %rdi
	call	res_findResource_67@PLT
	movl	%eax, %r14d
.L572:
	movq	-576(%rbp), %rax
	cmpl	$-1, %r14d
	je	.L575
	testq	%rax, %rax
	je	.L576
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L576
	movq	-616(%rbp), %r9
	subq	$8, %rsp
	movl	$-1, %ecx
	movl	%r14d, %esi
	movq	-544(%rbp), %rdx
	movq	8(%r9), %r8
	pushq	%rax
	leaq	40(%r9), %rdi
	movl	16(%rbp), %eax
	pushq	%r12
	addl	$1, %eax
	pushq	%rax
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	movq	%rax, %r12
.L576:
	movq	%r12, %r14
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L557:
	movq	%r8, %rsi
	movl	$8, %ecx
	leaq	.LC6(%rip), %rdi
	movq	-608(%rbp), %rbx
	repz cmpsb
	movl	$47, %esi
	movq	%rbx, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r8
	movq	%r8, -584(%rbp)
	call	strchr@PLT
	movq	-584(%rbp), %r8
	testq	%rax, %rax
	je	.L560
	movb	$0, (%rax)
	addq	$1, %rax
	movq	%rbx, %rsi
	movq	%rax, -608(%rbp)
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L616:
	movq	-624(%rbp), %rax
	movq	%rax, -592(%rbp)
	movq	%rax, %r13
	movq	%rax, %rdi
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L670:
	cmpl	$-1, %r10d
	setne	%r14b
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L674:
	movq	-600(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movq	-584(%rbp), %r8
	addq	%r15, %rax
	movq	%rax, -608(%rbp)
	jmp	.L556
.L564:
	movq	-616(%rbp), %rax
	movl	172(%rax), %r14d
	jmp	.L565
.L575:
	movl	$2, (%rax)
	jmp	.L576
.L560:
	movl	$0, -548(%rbp)
	movq	%rbx, %rsi
	movl	$2, %edx
	movq	%r8, %rdi
	movq	$0, -544(%rbp)
	leaq	-548(%rbp), %rcx
	jmp	.L659
.L675:
	movq	%rdx, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -600(%rbp)
	jmp	.L569
.L566:
	cmpl	$-1, -592(%rbp)
	leaq	-528(%rbp), %rbx
	je	.L572
	movl	%r14d, %eax
	movq	-616(%rbp), %rcx
	shrl	$28, %eax
	leal	-4(%rax), %edx
	leaq	40(%rcx), %rdi
	cmpl	$1, %edx
	jbe	.L622
	cmpl	$2, %eax
	je	.L622
	movl	-592(%rbp), %edx
	movl	%r14d, %esi
	call	res_getArrayItem_67@PLT
	movl	%eax, %r14d
	jmp	.L572
.L570:
	movq	-616(%rbp), %rdi
	xorl	%r14d, %r14d
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	-576(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L546
.L622:
	movl	-592(%rbp), %edx
	movl	%r14d, %esi
	leaq	-536(%rbp), %rcx
	leaq	-528(%rbp), %rbx
	call	res_getTableItemByIndex_67@PLT
	movl	%eax, %r14d
	jmp	.L572
.L666:
	call	__stack_chk_fail@PLT
.L662:
	movq	-576(%rbp), %rax
	xorl	%r14d, %r14d
	movl	$7, (%rax)
	jmp	.L546
.L579:
	movq	-576(%rbp), %rax
	movq	-616(%rbp), %rdi
	xorl	%r14d, %r14d
	movl	$7, (%rax)
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	jmp	.L546
	.cfi_endproc
.LFE4264:
	.size	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0, .-_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	.p2align 4
	.type	ures_getByKeyWithFallback_67.part.0, @function
ures_getByKeyWithFallback_67.part.0:
.LFB4266:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -200(%rbp)
	movl	172(%rdi), %r15d
	movq	%rdi, -184(%rbp)
	movq	%rsi, -176(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%r15d, %eax
	shrl	$28, %eax
	leal	-4(%rax), %edx
	movl	%eax, %r9d
	cmpl	$1, %edx
	jbe	.L716
	cmpl	$2, %eax
	je	.L716
	movl	$17, (%rcx)
.L742:
	movq	-200(%rbp), %r12
	xorl	%r15d, %r15d
.L704:
	movq	%r15, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
.L676:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L743
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	.cfi_restore_state
	movq	-176(%rbp), %rsi
	leaq	-115(%rbp), %rax
	leaq	-128(%rbp), %rbx
	movl	%r9d, -212(%rbp)
	leaq	-144(%rbp), %r12
	xorl	%r9d, %r9d
	movq	%rax, -128(%rbp)
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	movw	%r9w, -116(%rbp)
	movq	%rbx, -232(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	movl	$0, -160(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	leaq	-160(%rbp), %rax
	movl	-136(%rbp), %edx
	movq	%rbx, %rdi
	movq	-144(%rbp), %rsi
	movq	%rax, %rcx
	movq	%rax, -208(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-160(%rbp), %r10d
	testl	%r10d, %r10d
	jg	.L679
	movq	-128(%rbp), %rbx
	movq	-184(%rbp), %rax
	cmpb	$0, (%rbx)
	leaq	40(%rax), %r13
	je	.L680
	cmpl	$-1, %r15d
	je	.L679
	leaq	-152(%rbp), %rax
	movl	-212(%rbp), %r9d
	movq	%rax, -168(%rbp)
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L717:
	movl	$47, %esi
	movq	%rbx, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L683
	movb	$0, (%rax)
	leaq	1(%rax), %r14
.L684:
	movq	-168(%rbp), %rdx
	movl	%r15d, %esi
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%rbx, -144(%rbp)
	call	res_getTableItemByKey_67@PLT
	movl	%eax, %r9d
	movl	%eax, %r15d
	shrl	$28, %r9d
	cmpb	$0, (%r14)
	je	.L680
	cmpl	$-1, %r15d
	je	.L679
	movq	%r14, %rbx
.L681:
	leal	-4(%r9), %eax
	andl	$-6, %eax
	je	.L717
	cmpl	$2, %r9d
	je	.L717
.L679:
	cmpb	$0, -116(%rbp)
	jne	.L744
	movq	-176(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-184(%rbp), %rax
	movq	8(%rax), %r14
.L686:
	movq	-224(%rbp), %rax
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	movq	%r15, %rbx
	movq	%rax, -128(%rbp)
	movq	-184(%rbp), %rax
	movw	%r8w, -116(%rbp)
	movq	$0, -152(%rbp)
	movq	32(%rax), %rcx
	movl	168(%rax), %eax
	movq	%rcx, -224(%rbp)
	movl	%eax, -212(%rbp)
	leaq	-152(%rbp), %rax
	movq	%rax, -168(%rbp)
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L746:
	movl	112(%r14), %edi
	testl	%edi, %edi
	je	.L745
.L691:
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.L746
	movq	-192(%rbp), %rax
	movq	-200(%rbp), %r12
	movq	%rbx, %r15
	movl	$2, (%rax)
.L705:
	cmpb	$0, -116(%rbp)
	je	.L704
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L683:
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	(%rbx,%rax), %r14
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L680:
	cmpb	$0, -116(%rbp)
	jne	.L747
.L710:
	movq	-176(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-184(%rbp), %rax
	movq	8(%rax), %r14
	cmpl	$-1, %r15d
	je	.L686
	movq	-192(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L742
	subq	$8, %rsp
	movl	%r15d, %esi
	movq	%r14, %r8
	movl	$-1, %ecx
	pushq	%rax
	movq	-184(%rbp), %r9
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	pushq	-200(%rbp)
	movq	-176(%rbp), %rdx
	pushq	$0
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	movq	%rax, %r12
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L747:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L745:
	movl	72(%r14), %eax
	movl	$0, -72(%rbp)
	movl	%eax, %r13d
	movq	-128(%rbp), %rax
	movb	$0, (%rax)
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	jle	.L692
	movq	-192(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movl	%eax, %edx
	movq	-232(%rbp), %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L692:
	movq	-176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-192(%rbp), %r15
	movq	-144(%rbp), %rsi
	movl	-136(%rbp), %edx
	movq	-232(%rbp), %rdi
	movq	%r15, %rcx
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L748
	movq	-128(%rbp), %rax
	leaq	40(%r14), %r15
	movq	%r15, %rcx
	movq	%r14, %r15
	movq	%rax, -152(%rbp)
	movq	-176(%rbp), %rax
	movq	%rcx, %r14
	movq	%rax, -160(%rbp)
	movq	%r12, %rax
	movq	%rbx, %r12
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L700:
	movq	-208(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	res_findResource_67@PLT
	movl	%eax, %esi
	shrl	$28, %eax
	cmpl	$3, %eax
	movq	-152(%rbp), %rax
	je	.L749
	cmpb	$0, (%rax)
	jne	.L700
	movq	%rbx, %rax
	movq	%r14, %rcx
	movq	%r12, %rbx
	movl	%esi, %r13d
	movq	%r15, %r14
	movq	%rax, %r12
	cmpl	$-1, %esi
	je	.L691
	movq	%rcx, %r10
	movq	%rbx, %r15
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L749:
	cmpb	$0, (%rax)
	je	.L739
	movq	-192(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L697
	subq	$8, %rsp
	movq	-184(%rbp), %r9
	xorl	%edx, %edx
	movq	%r15, %r8
	pushq	%rax
	movl	$-1, %ecx
	movq	%r14, %rdi
	pushq	%r12
	pushq	$0
	movl	%esi, -212(%rbp)
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	movl	-212(%rbp), %esi
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L739
	movq	8(%rax), %r15
	movl	172(%rax), %r13d
	movq	32(%rax), %rax
	leaq	40(%r15), %r14
	movq	%rax, -224(%rbp)
	movl	168(%r12), %eax
	movl	%eax, -212(%rbp)
	movq	-152(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L700
	.p2align 4,,10
	.p2align 3
.L739:
	movq	%r14, %r10
	movl	%esi, %r13d
	movq	%r15, %r14
	movq	%r12, %r15
.L689:
	movq	%r10, -168(%rbp)
	call	uloc_getDefault_67@PLT
	movq	(%r14), %r12
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	movq	-168(%rbp), %r10
	movl	%eax, %r9d
	movl	$-127, %eax
	testl	%r9d, %r9d
	jne	.L750
.L701:
	movq	-192(%rbp), %rcx
	movq	%r14, %r8
	movl	%r13d, %esi
	movq	%r10, %rdi
	subq	$8, %rsp
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %rdx
	movl	%eax, (%rcx)
	pushq	%rcx
	movl	$-1, %ecx
	pushq	-200(%rbp)
	pushq	$0
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	movq	%rax, %r12
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L697:
	testq	%r12, %r12
	je	.L739
	movq	32(%r12), %rax
	movq	8(%r12), %r15
	movl	172(%r12), %r13d
	movq	%rax, -224(%rbp)
	movl	168(%r12), %eax
	leaq	40(%r15), %r14
	movl	%eax, -212(%rbp)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L744:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-176(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-184(%rbp), %rax
	movq	8(%rax), %r14
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L750:
	movl	$5, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	sete	%al
	movzbl	%al, %eax
	addl	$-128, %eax
	jmp	.L701
.L748:
	movq	%rbx, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	cmpb	$0, -116(%rbp)
	jne	.L751
.L694:
	movq	-200(%rbp), %r12
	jmp	.L676
.L751:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L694
.L743:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4266:
	.size	ures_getByKeyWithFallback_67.part.0, .-ures_getByKeyWithFallback_67.part.0
	.p2align 4
	.type	_ZN12_GLOBAL__N_123getAllItemsWithFallbackEPK15UResourceBundleRN6icu_6717ResourceDataValueERNS3_12ResourceSinkER10UErrorCode, @function
_ZN12_GLOBAL__N_123getAllItemsWithFallbackEPK15UResourceBundleRN6icu_6717ResourceDataValueERNS3_12ResourceSinkER10UErrorCode:
.LFB3211:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L752
	movdqu	40(%rdi), %xmm0
	movq	8(%rdi), %rax
	movq	%rsi, %r12
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movq	%rcx, %r13
	movups	%xmm0, 8(%rsi)
	movdqu	56(%rdi), %xmm1
	movq	16(%rax), %r15
	movq	(%rdx), %rax
	movups	%xmm1, 24(%rsi)
	movdqu	72(%rdi), %xmm2
	movq	24(%rax), %rax
	movups	%xmm2, 40(%rsi)
	movdqu	88(%rdi), %xmm3
	movups	%xmm3, 56(%rsi)
	movl	172(%rdi), %edx
	movq	(%rdi), %rsi
	testq	%r15, %r15
	je	.L756
	movl	112(%r15), %ecx
	testl	%ecx, %ecx
	jle	.L769
.L756:
	movl	%edx, 72(%r12)
	movq	%r13, %r8
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	*%rax
.L752:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L770
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L769:
	.cfi_restore_state
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movl	%edx, 72(%r12)
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	*%rax
	xorl	%eax, %eax
	leaq	-464(%rbp), %rdi
	movl	$25, %ecx
	movq	%rdi, -488(%rbp)
	rep stosq
	leaq	-424(%rbp), %rdi
	movq	%r15, -456(%rbp)
	movq	%r15, -440(%rbp)
	movdqu	40(%r15), %xmm4
	movups	%xmm4, -424(%rbp)
	movdqu	56(%r15), %xmm5
	movups	%xmm5, -408(%rbp)
	movdqu	72(%r15), %xmm6
	movups	%xmm6, -392(%rbp)
	movdqu	88(%r15), %xmm7
	movl	-392(%rbp), %esi
	movb	$1, -287(%rbp)
	movups	%xmm7, -376(%rbp)
	cmpb	$0, -368(%rbp)
	sete	-288(%rbp)
	movl	%esi, -292(%rbp)
	call	res_countArrayItems_67@PLT
	leaq	_ZL9resbMutex(%rip), %rdi
	movl	$-1, -276(%rbp)
	movl	%eax, -272(%rbp)
	call	umtx_lock_67@PLT
	movq	16(%r15), %rax
	addl	$1, 108(%r15)
	testq	%rax, %rax
	je	.L760
	.p2align 4,,10
	.p2align 3
.L757:
	addl	$1, 108(%rax)
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L757
.L760:
	leaq	_ZL9resbMutex(%rip), %rdi
	leaq	-256(%rbp), %r15
	call	umtx_unlock_67@PLT
	movq	32(%rbx), %rsi
	xorl	%eax, %eax
	movl	$25, %ecx
	movl	$0, -468(%rbp)
	movq	%r15, %rdi
	rep stosq
	movq	-488(%rbp), %rax
	testq	%rsi, %rsi
	je	.L758
	cmpb	$0, (%rsi)
	jne	.L771
.L758:
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN12_GLOBAL__N_123getAllItemsWithFallbackEPK15UResourceBundleRN6icu_6717ResourceDataValueERNS3_12ResourceSinkER10UErrorCode
.L759:
	movq	%r15, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	-488(%rbp), %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L771:
	movq	%r15, %rdx
	leaq	-468(%rbp), %rcx
	movq	%rax, %rdi
	call	ures_getByKeyWithFallback_67.part.0
	movl	-468(%rbp), %edx
	testl	%edx, %edx
	jg	.L759
	jmp	.L758
.L770:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3211:
	.size	_ZN12_GLOBAL__N_123getAllItemsWithFallbackEPK15UResourceBundleRN6icu_6717ResourceDataValueERNS3_12ResourceSinkER10UErrorCode, .-_ZN12_GLOBAL__N_123getAllItemsWithFallbackEPK15UResourceBundleRN6icu_6717ResourceDataValueERNS3_12ResourceSinkER10UErrorCode
	.p2align 4
	.type	ures_loc_nextLocale, @function
ures_loc_nextLocale:
.LFB3233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L775
	movl	192(%r12), %eax
	movq	%rdx, %r13
	movl	188(%r12), %edx
	subl	$1, %eax
	cmpl	%eax, %edx
	jge	.L775
	movq	$0, -64(%rbp)
	leaq	200(%r12), %r14
	testq	%r13, %r13
	je	.L783
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L783
	movl	172(%r12), %esi
	addl	$1, %edx
	movl	%edx, 188(%r12)
	movl	%esi, %eax
	shrl	$28, %eax
	cmpl	$-268435457, %esi
	ja	.L777
	leaq	.L779(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L779:
	.long	.L778-.L779
	.long	.L778-.L779
	.long	.L781-.L779
	.long	.L777-.L779
	.long	.L781-.L779
	.long	.L781-.L779
	.long	.L778-.L779
	.long	.L778-.L779
	.long	.L780-.L779
	.long	.L780-.L779
	.long	.L777-.L779
	.long	.L777-.L779
	.long	.L777-.L779
	.long	.L777-.L779
	.long	.L778-.L779
	.text
	.p2align 4,,10
	.p2align 3
.L781:
	leaq	40(%r12), %r15
	leaq	-64(%rbp), %rcx
	movq	%r15, %rdi
	call	res_getTableItemByIndex_67@PLT
	movl	0(%r13), %edx
	movl	%eax, %esi
	testl	%edx, %edx
	jg	.L783
.L795:
	subq	$8, %rsp
	movq	8(%r12), %r8
	movq	-64(%rbp), %rdx
	movq	%r12, %r9
	movl	188(%r12), %ecx
	pushq	%r13
	movq	%r15, %rdi
	pushq	%r14
	pushq	$0
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	movq	%rax, %r14
.L777:
	testq	%r14, %r14
	jne	.L783
	.p2align 4,,10
	.p2align 3
.L775:
	xorl	%eax, %eax
	xorl	%r12d, %r12d
.L774:
	testq	%rbx, %rbx
	je	.L772
	movl	%eax, (%rbx)
.L772:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L796
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	leaq	40(%r12), %r15
	movq	%r15, %rdi
	call	res_getArrayItem_67@PLT
	movl	%eax, %esi
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L795
	.p2align 4,,10
	.p2align 3
.L783:
	movq	(%r14), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L778:
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	ures_copyResb_67.part.0
	movq	%rax, %r14
	jmp	.L777
.L796:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3233:
	.size	ures_loc_nextLocale, .-ures_loc_nextLocale
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3545:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3545:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3548:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L810
	ret
	.p2align 4,,10
	.p2align 3
.L810:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L798
	cmpb	$0, 12(%rbx)
	jne	.L811
.L802:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L798:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L802
	.cfi_endproc
.LFE3548:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3551:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L814
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3551:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3554:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L817
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3554:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L823
.L819:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L824
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L823:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L824:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3556:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3557:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3557:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3558:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3558:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3559:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3559:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3560:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3560:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3561:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3561:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3562:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L840
	testl	%edx, %edx
	jle	.L840
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L843
.L832:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L840:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L843:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L832
	.cfi_endproc
.LFE3562:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L847
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L847
	testl	%r12d, %r12d
	jg	.L854
	cmpb	$0, 12(%rbx)
	jne	.L855
.L849:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L849
.L855:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L847:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3563:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L857
	movq	(%rdi), %r8
.L858:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L857:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L861
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L861
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L861:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3564:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3565:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L868
	ret
	.p2align 4,,10
	.p2align 3
.L868:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3565:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3566:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3566:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3567:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3567:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3568:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3568:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3570:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3570:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3572:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3572:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.p2align 4
	.globl	ures_initStackObject_67
	.type	ures_initStackObject_67, @function
ures_initStackObject_67:
.LFB3169:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%rdi, %rdx
	leaq	8(%rdi), %rdi
	xorl	%eax, %eax
	movq	$0, 184(%rdi)
	movq	%rdx, %rcx
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$200, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, 180(%rdx)
	ret
	.cfi_endproc
.LFE3169:
	.size	ures_initStackObject_67, .-ures_initStackObject_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720StackUResourceBundleC2Ev
	.type	_ZN6icu_6720StackUResourceBundleC2Ev, @function
_ZN6icu_6720StackUResourceBundleC2Ev:
.LFB3171:
	.cfi_startproc
	endbr64
	movq	$0, (%rdi)
	movq	%rdi, %rdx
	leaq	8(%rdi), %rdi
	xorl	%eax, %eax
	movq	$0, 184(%rdi)
	movq	%rdx, %rcx
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$200, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	$0, 180(%rdx)
	ret
	.cfi_endproc
.LFE3171:
	.size	_ZN6icu_6720StackUResourceBundleC2Ev, .-_ZN6icu_6720StackUResourceBundleC2Ev
	.globl	_ZN6icu_6720StackUResourceBundleC1Ev
	.set	_ZN6icu_6720StackUResourceBundleC1Ev,_ZN6icu_6720StackUResourceBundleC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720StackUResourceBundleD2Ev
	.type	_ZN6icu_6720StackUResourceBundleD2Ev, @function
_ZN6icu_6720StackUResourceBundleD2Ev:
.LFB3174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L877
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	.p2align 4,,10
	.p2align 3
.L878:
	movq	%rbx, %rax
	movq	16(%rbx), %rbx
	subl	$1, 108(%rax)
	testq	%rbx, %rbx
	jne	.L878
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L877:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L879
	call	uprv_free_67@PLT
.L879:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L880
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L880
	call	uprv_free_67@PLT
.L880:
	cmpl	$19700503, 180(%r12)
	movq	$0, 32(%r12)
	movl	$0, 168(%r12)
	je	.L893
.L876:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	cmpl	$19641227, 184(%r12)
	jne	.L876
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3174:
	.size	_ZN6icu_6720StackUResourceBundleD2Ev, .-_ZN6icu_6720StackUResourceBundleD2Ev
	.globl	_ZN6icu_6720StackUResourceBundleD1Ev
	.set	_ZN6icu_6720StackUResourceBundleD1Ev,_ZN6icu_6720StackUResourceBundleD2Ev
	.p2align 4
	.globl	ures_close_67
	.type	ures_close_67, @function
ures_close_67:
.LFB3185:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L913
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L897
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	.p2align 4,,10
	.p2align 3
.L898:
	movq	%rbx, %rax
	movq	16(%rbx), %rbx
	subl	$1, 108(%rax)
	testq	%rbx, %rbx
	jne	.L898
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L897:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L899
	call	uprv_free_67@PLT
.L899:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L900
	leaq	104(%r12), %rax
	cmpq	%rax, %rdi
	je	.L900
	call	uprv_free_67@PLT
.L900:
	cmpl	$19700503, 180(%r12)
	movq	$0, 32(%r12)
	movl	$0, 168(%r12)
	je	.L916
.L894:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L916:
	.cfi_restore_state
	cmpl	$19641227, 184(%r12)
	jne	.L894
	popq	%rbx
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L913:
	ret
	.cfi_endproc
.LFE3185:
	.size	ures_close_67, .-ures_close_67
	.p2align 4
	.globl	ures_copyResb_67
	.type	ures_copyResb_67, @function
ures_copyResb_67:
.LFB3187:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	movq	%rdi, %rax
	testl	%ecx, %ecx
	setg	%dil
	cmpq	%rsi, %rax
	sete	%cl
	orb	%cl, %dil
	jne	.L918
	testq	%rsi, %rsi
	jne	.L925
.L918:
	ret
	.p2align 4,,10
	.p2align 3
.L925:
	movq	%rax, %rdi
	jmp	ures_copyResb_67.part.0
	.cfi_endproc
.LFE3187:
	.size	ures_copyResb_67, .-ures_copyResb_67
	.p2align 4
	.globl	ures_getString_67
	.type	ures_getString_67, @function
ures_getString_67:
.LFB3188:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L929
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L926
	testq	%rdi, %rdi
	je	.L935
	movl	172(%rdi), %r8d
	movq	%rsi, %rdx
	addq	$40, %rdi
	movl	%r8d, %esi
	call	res_getStringNoTrace_67@PLT
	testq	%rax, %rax
	je	.L936
.L926:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L936:
	.cfi_restore_state
	movl	$17, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L935:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L929:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3188:
	.size	ures_getString_67, .-ures_getString_67
	.section	.rodata.str1.1
.LC8:
	.string	""
	.text
	.p2align 4
	.globl	ures_getUTF8String_67
	.type	ures_getUTF8String_67, @function
ures_getUTF8String_67:
.LFB3190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L938
	movq	%r8, %r12
	testq	%rdi, %rdi
	je	.L968
	movq	%rsi, %r14
	movl	172(%rdi), %esi
	movq	%rdx, %r13
	addq	$40, %rdi
	leaq	-44(%rbp), %rdx
	movl	%ecx, %ebx
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L940
	movl	(%r12), %eax
	movl	-44(%rbp), %r8d
	testl	%eax, %eax
	jg	.L938
	testq	%r13, %r13
	je	.L942
	movl	0(%r13), %esi
	testl	%esi, %esi
	js	.L943
	jle	.L944
	testq	%r14, %r14
	je	.L943
.L944:
	testl	%r8d, %r8d
	jne	.L949
	movl	$0, 0(%r13)
.L950:
	leaq	.LC8(%rip), %rax
	testb	%bl, %bl
	jne	.L969
	.p2align 4,,10
	.p2align 3
.L937:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L970
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore_state
	xorl	%esi, %esi
	testl	%r8d, %r8d
	je	.L950
.L949:
	cmpl	%esi, %r8d
	jg	.L971
	testb	%bl, %bl
	jne	.L947
	cmpl	$715827882, %r8d
	jg	.L947
	leal	1(%r8,%r8,2), %eax
	cmpl	%esi, %eax
	jge	.L947
	subl	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %r14
	movl	%eax, %esi
.L947:
	movq	%r12, %r9
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	u_strToUTF8_67@PLT
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L968:
	movl	$1, (%r8)
.L938:
	xorl	%eax, %eax
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L940:
	movl	$17, (%r12)
	xorl	%eax, %eax
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L943:
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L971:
	movq	%r12, %r9
	movq	%r13, %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	u_strToUTF8_67@PLT
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L969:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	u_terminateChars_67@PLT
	movq	%r14, %rax
	jmp	.L937
.L970:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3190:
	.size	ures_getUTF8String_67, .-ures_getUTF8String_67
	.p2align 4
	.globl	ures_getBinary_67
	.type	ures_getBinary_67, @function
ures_getBinary_67:
.LFB3191:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L975
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L972
	testq	%rdi, %rdi
	je	.L981
	movl	172(%rdi), %r8d
	movq	%rsi, %rdx
	addq	$40, %rdi
	movl	%r8d, %esi
	call	res_getBinaryNoTrace_67@PLT
	testq	%rax, %rax
	je	.L982
.L972:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L982:
	.cfi_restore_state
	movl	$17, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L981:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L975:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3191:
	.size	ures_getBinary_67, .-ures_getBinary_67
	.p2align 4
	.globl	ures_getIntVector_67
	.type	ures_getIntVector_67, @function
ures_getIntVector_67:
.LFB3192:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L986
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L983
	testq	%rdi, %rdi
	je	.L992
	movl	172(%rdi), %r8d
	movq	%rsi, %rdx
	addq	$40, %rdi
	movl	%r8d, %esi
	call	res_getIntVectorNoTrace_67@PLT
	testq	%rax, %rax
	je	.L993
.L983:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L993:
	.cfi_restore_state
	movl	$17, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L992:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L986:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3192:
	.size	ures_getIntVector_67, .-ures_getIntVector_67
	.p2align 4
	.globl	ures_getInt_67
	.type	ures_getInt_67, @function
ures_getInt_67:
.LFB3193:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L999
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L999
	testq	%rdi, %rdi
	je	.L1000
	movl	172(%rdi), %eax
	movl	%eax, %edx
	shrl	$28, %edx
	cmpl	$7, %edx
	jne	.L1001
	sall	$4, %eax
	sarl	$4, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1001:
	movl	$17, (%rsi)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	movl	$-1, %eax
	ret
.L1000:
	movl	$1, (%rsi)
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3193:
	.size	ures_getInt_67, .-ures_getInt_67
	.p2align 4
	.globl	ures_getUInt_67
	.type	ures_getUInt_67, @function
ures_getUInt_67:
.LFB3194:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	testq	%rsi, %rsi
	je	.L1002
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L1002
	testq	%rdi, %rdi
	je	.L1009
	movl	172(%rdi), %eax
	movl	%eax, %edx
	andl	$268435455, %eax
	shrl	$28, %edx
	cmpl	$7, %edx
	jne	.L1010
.L1002:
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	movl	$17, (%rsi)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1009:
	movl	$1, (%rsi)
	ret
	.cfi_endproc
.LFE3194:
	.size	ures_getUInt_67, .-ures_getUInt_67
	.p2align 4
	.globl	ures_getType_67
	.type	ures_getType_67, @function
ures_getType_67:
.LFB3195:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1012
	movl	172(%rdi), %edi
	jmp	res_getPublicType_67@PLT
.L1012:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3195:
	.size	ures_getType_67, .-ures_getType_67
	.p2align 4
	.globl	ures_getKey_67
	.type	ures_getKey_67, @function
ures_getKey_67:
.LFB3196:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1015
	movq	(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1015:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3196:
	.size	ures_getKey_67, .-ures_getKey_67
	.p2align 4
	.globl	ures_getSize_67
	.type	ures_getSize_67, @function
ures_getSize_67:
.LFB3197:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1018
	movl	192(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1018:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3197:
	.size	ures_getSize_67, .-ures_getSize_67
	.p2align 4
	.globl	ures_resetIterator_67
	.type	ures_resetIterator_67, @function
ures_resetIterator_67:
.LFB3199:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1019
	movl	$-1, 188(%rdi)
.L1019:
	ret
	.cfi_endproc
.LFE3199:
	.size	ures_resetIterator_67, .-ures_resetIterator_67
	.p2align 4
	.globl	ures_hasNext_67
	.type	ures_hasNext_67, @function
ures_hasNext_67:
.LFB3200:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1026
	movl	192(%rdi), %eax
	subl	$1, %eax
	cmpl	%eax, 188(%rdi)
	setl	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1026:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3200:
	.size	ures_hasNext_67, .-ures_hasNext_67
	.p2align 4
	.globl	ures_getNextResource_67
	.type	ures_getNextResource_67, @function
ures_getNextResource_67:
.LFB3202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	testq	%rdx, %rdx
	je	.L1038
	movl	(%rdx), %ecx
	movq	%rdx, %r13
	testl	%ecx, %ecx
	jg	.L1038
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L1041
	movl	192(%rdi), %eax
	movl	188(%rdi), %edx
	subl	$1, %eax
	cmpl	%eax, %edx
	je	.L1042
	movl	172(%rdi), %esi
	addl	$1, %edx
	movl	%edx, 188(%rdi)
	movl	%esi, %eax
	shrl	$28, %eax
	cmpl	$-268435457, %esi
	ja	.L1038
	leaq	.L1034(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1034:
	.long	.L1033-.L1034
	.long	.L1033-.L1034
	.long	.L1036-.L1034
	.long	.L1038-.L1034
	.long	.L1036-.L1034
	.long	.L1036-.L1034
	.long	.L1033-.L1034
	.long	.L1033-.L1034
	.long	.L1035-.L1034
	.long	.L1035-.L1034
	.long	.L1038-.L1034
	.long	.L1038-.L1034
	.long	.L1038-.L1034
	.long	.L1038-.L1034
	.long	.L1033-.L1034
	.text
	.p2align 4,,10
	.p2align 3
.L1036:
	leaq	40(%rdi), %r15
	leaq	-48(%rbp), %rcx
	movq	%r15, %rdi
	call	res_getTableItemByIndex_67@PLT
	movl	0(%r13), %edx
	movl	%eax, %esi
	testl	%edx, %edx
	jle	.L1040
	.p2align 4,,10
	.p2align 3
.L1038:
	movq	%r12, %rax
.L1027:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1043
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1033:
	.cfi_restore_state
	cmpq	%r12, %rdi
	je	.L1038
	movq	%rdi, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	ures_copyResb_67.part.0
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1035:
	leaq	40(%rdi), %r15
	movq	%r15, %rdi
	call	res_getArrayItem_67@PLT
	movl	%eax, %esi
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1038
.L1040:
	subq	$8, %rsp
	movq	8(%r14), %r8
	movq	-48(%rbp), %rdx
	movq	%r14, %r9
	movl	188(%r14), %ecx
	pushq	%r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	$0
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1042:
	movl	$8, 0(%r13)
	movq	%rsi, %rax
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1041:
	movl	$1, (%rdx)
	movq	%rsi, %rax
	jmp	.L1027
.L1043:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3202:
	.size	ures_getNextResource_67, .-ures_getNextResource_67
	.p2align 4
	.globl	ures_getByIndex_67
	.type	ures_getByIndex_67, @function
ures_getByIndex_67:
.LFB3203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testq	%rcx, %rcx
	je	.L1055
	movq	%rcx, %r13
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jg	.L1055
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L1058
	movl	%esi, %r15d
	testl	%esi, %esi
	js	.L1049
	cmpl	%esi, 192(%rdi)
	jle	.L1049
	movl	172(%rdi), %esi
	movl	%esi, %eax
	shrl	$28, %eax
	cmpl	$-268435457, %esi
	ja	.L1055
	leaq	.L1051(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1051:
	.long	.L1050-.L1051
	.long	.L1050-.L1051
	.long	.L1053-.L1051
	.long	.L1055-.L1051
	.long	.L1053-.L1051
	.long	.L1053-.L1051
	.long	.L1050-.L1051
	.long	.L1050-.L1051
	.long	.L1052-.L1051
	.long	.L1052-.L1051
	.long	.L1055-.L1051
	.long	.L1055-.L1051
	.long	.L1055-.L1051
	.long	.L1055-.L1051
	.long	.L1050-.L1051
	.text
	.p2align 4,,10
	.p2align 3
.L1052:
	leaq	40(%rdi), %rbx
	movl	%r15d, %edx
	movq	%rbx, %rdi
	call	res_getArrayItem_67@PLT
	movl	%eax, %esi
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L1057
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	%r12, %rax
.L1044:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1059
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1049:
	.cfi_restore_state
	movl	$2, 0(%r13)
	movq	%r12, %rax
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1058:
	movl	$1, 0(%r13)
	movq	%rdx, %rax
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1050:
	cmpq	%r12, %rdi
	je	.L1055
	movq	%rdi, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	ures_copyResb_67.part.0
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1053:
	leaq	40(%rdi), %rbx
	movl	%r15d, %edx
	leaq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	call	res_getTableItemByIndex_67@PLT
	movl	0(%r13), %edx
	movl	%eax, %esi
	testl	%edx, %edx
	jg	.L1055
.L1057:
	subq	$8, %rsp
	movq	8(%r14), %r8
	movq	-64(%rbp), %rdx
	movq	%r14, %r9
	pushq	%r13
	movl	%r15d, %ecx
	movq	%rbx, %rdi
	pushq	%r12
	pushq	$0
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	jmp	.L1044
.L1059:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3203:
	.size	ures_getByIndex_67, .-ures_getByIndex_67
	.p2align 4
	.globl	ures_getNextString_67
	.type	ures_getNextString_67, @function
ures_getNextString_67:
.LFB3201:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L1078
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$16, %rsp
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jg	.L1080
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L1100
	movl	192(%rdi), %eax
	movl	188(%rdi), %ecx
	subl	$1, %eax
	cmpl	%eax, %ecx
	je	.L1101
	movq	%rsi, %r13
	movl	172(%rdi), %esi
	leal	1(%rcx), %r8d
	movl	%r8d, 188(%rdi)
	movl	%esi, %eax
	shrl	$28, %eax
	cmpl	$-268435457, %esi
	ja	.L1080
	leaq	.L1065(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1065:
	.long	.L1067-.L1065
	.long	.L1064-.L1065
	.long	.L1068-.L1065
	.long	.L1069-.L1065
	.long	.L1068-.L1065
	.long	.L1068-.L1065
	.long	.L1067-.L1065
	.long	.L1064-.L1065
	.long	.L1066-.L1065
	.long	.L1066-.L1065
	.long	.L1080-.L1065
	.long	.L1080-.L1065
	.long	.L1080-.L1065
	.long	.L1080-.L1065
	.long	.L1064-.L1065
	.text
	.p2align 4,,10
	.p2align 3
.L1080:
	xorl	%eax, %eax
.L1060:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1068:
	.cfi_restore_state
	leaq	40(%rdi), %r14
	movq	%rdx, %rcx
	movl	%r8d, %edx
	movq	%r14, %rdi
	call	res_getTableItemByIndex_67@PLT
.L1099:
	movl	%eax, %esi
	shrl	$28, %eax
	movq	%r13, %rdx
	movq	%r14, %rdi
	cmpl	$3, %eax
	je	.L1102
.L1088:
	addq	$16, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	res_getStringNoTrace_67@PLT
	.p2align 4,,10
	.p2align 3
.L1064:
	.cfi_restore_state
	movl	$17, (%rbx)
	xorl	%eax, %eax
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1067:
	leaq	40(%rdi), %rdi
	movq	%r13, %rdx
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1066:
	leaq	40(%rdi), %r14
	movl	%r8d, %edx
	movq	%r14, %rdi
	call	res_getArrayItem_67@PLT
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1101:
	movl	$8, (%rbx)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1078:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1100:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$1, (%rbx)
	xorl	%eax, %eax
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1069:
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	%r8d, %esi
.L1096:
	movq	%r12, %rdi
	call	ures_getByIndex_67
	movl	(%rbx), %edx
	movq	%rax, %r12
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1076
	testq	%r12, %r12
	je	.L1103
	movl	172(%r12), %esi
	leaq	40(%r12), %rdi
	movq	%r13, %rdx
	call	res_getStringNoTrace_67@PLT
	testq	%rax, %rax
	je	.L1104
.L1076:
	movq	%r12, %rdi
	movq	%rax, -40(%rbp)
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	-40(%rbp), %rax
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1102:
	movl	188(%r12), %esi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	jmp	.L1096
.L1104:
	movl	$17, (%rbx)
	jmp	.L1076
.L1103:
	movl	$1, (%rbx)
	movq	%r12, %rax
	jmp	.L1076
	.cfi_endproc
.LFE3201:
	.size	ures_getNextString_67, .-ures_getNextString_67
	.p2align 4
	.globl	ures_getStringByIndex_67
	.type	ures_getStringByIndex_67, @function
ures_getStringByIndex_67:
.LFB3204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testq	%rcx, %rcx
	je	.L1125
	movq	%rcx, %rbx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jg	.L1105
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L1140
	movl	%esi, %r12d
	testl	%esi, %esi
	js	.L1108
	cmpl	%esi, 192(%rdi)
	jle	.L1108
	movl	172(%rdi), %esi
	movl	%esi, %eax
	shrl	$28, %eax
	cmpl	$-268435457, %esi
	ja	.L1109
	movq	%rdx, %r14
	leaq	.L1111(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1111:
	.long	.L1113-.L1111
	.long	.L1110-.L1111
	.long	.L1114-.L1111
	.long	.L1115-.L1111
	.long	.L1114-.L1111
	.long	.L1114-.L1111
	.long	.L1113-.L1111
	.long	.L1110-.L1111
	.long	.L1112-.L1111
	.long	.L1112-.L1111
	.long	.L1109-.L1111
	.long	.L1109-.L1111
	.long	.L1109-.L1111
	.long	.L1109-.L1111
	.long	.L1110-.L1111
	.text
	.p2align 4,,10
	.p2align 3
.L1115:
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rbx, %rcx
	movq	%r13, %rdi
	call	ures_getByIndex_67
	movl	(%rbx), %edx
	movq	%rax, %r12
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1122
	testq	%r12, %r12
	je	.L1141
	movl	172(%r12), %esi
	leaq	40(%r12), %rdi
	movq	%r14, %rdx
	call	res_getStringNoTrace_67@PLT
	testq	%rax, %rax
	je	.L1142
.L1122:
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	-72(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1143
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1108:
	.cfi_restore_state
	movl	$2, (%rbx)
	xorl	%eax, %eax
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1114:
	leaq	40(%rdi), %r15
	leaq	-64(%rbp), %rcx
	movl	%r12d, %edx
	movq	%r15, %rdi
	call	res_getTableItemByIndex_67@PLT
.L1139:
	movl	%eax, %esi
	shrl	$28, %eax
	cmpl	$3, %eax
	je	.L1115
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	res_getStringNoTrace_67@PLT
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1110:
	movl	$17, (%rbx)
	xorl	%eax, %eax
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1113:
	leaq	40(%rdi), %rdi
	movq	%r14, %rdx
	call	res_getStringNoTrace_67@PLT
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1109:
	movl	$5, (%rbx)
	xorl	%eax, %eax
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1112:
	leaq	40(%rdi), %r15
	movl	%r12d, %edx
	movq	%r15, %rdi
	call	res_getArrayItem_67@PLT
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1125:
	xorl	%eax, %eax
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1140:
	movl	$1, (%rbx)
	jmp	.L1105
.L1142:
	movl	$17, (%rbx)
	jmp	.L1122
.L1141:
	movl	$1, (%rbx)
	movq	%r12, %rax
	jmp	.L1122
.L1143:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3204:
	.size	ures_getStringByIndex_67, .-ures_getStringByIndex_67
	.p2align 4
	.globl	ures_getUTF8StringByIndex_67
	.type	ures_getUTF8StringByIndex_67, @function
ures_getUTF8StringByIndex_67:
.LFB3205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testl	%r10d, %r10d
	jg	.L1144
	movq	%rdi, %r13
	movq	%r9, %r12
	testq	%rdi, %rdi
	je	.L1203
	movl	%esi, %r15d
	testl	%esi, %esi
	js	.L1148
	cmpl	192(%rdi), %esi
	jge	.L1148
	movl	172(%rdi), %esi
	movl	%esi, %eax
	shrl	$28, %eax
	cmpl	$-268435457, %esi
	ja	.L1149
	movq	%rdx, %rbx
	leaq	.L1151(%rip), %rdx
	movq	%rcx, %r14
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1151:
	.long	.L1153-.L1151
	.long	.L1150-.L1151
	.long	.L1154-.L1151
	.long	.L1155-.L1151
	.long	.L1154-.L1151
	.long	.L1154-.L1151
	.long	.L1153-.L1151
	.long	.L1150-.L1151
	.long	.L1152-.L1151
	.long	.L1152-.L1151
	.long	.L1149-.L1151
	.long	.L1149-.L1151
	.long	.L1149-.L1151
	.long	.L1149-.L1151
	.long	.L1150-.L1151
	.text
	.p2align 4,,10
	.p2align 3
.L1154:
	leaq	40(%rdi), %rdi
	leaq	-64(%rbp), %rcx
	movl	%r15d, %edx
	movl	%r8d, -92(%rbp)
	movq	%rdi, -88(%rbp)
	call	res_getTableItemByIndex_67@PLT
.L1202:
	movl	-92(%rbp), %r10d
	movl	%eax, %esi
	shrl	$28, %eax
	movq	-88(%rbp), %rdi
	cmpl	$3, %eax
	movl	%r10d, -88(%rbp)
	je	.L1200
	leaq	-68(%rbp), %rdx
	call	res_getStringNoTrace_67@PLT
	movl	-88(%rbp), %r10d
	movq	%rax, %rcx
	movl	(%r12), %eax
.L1156:
	testl	%eax, %eax
	jg	.L1177
	movl	-68(%rbp), %r8d
	testq	%r14, %r14
	je	.L1165
	movl	(%r14), %esi
	testl	%esi, %esi
	js	.L1166
	jle	.L1167
	testq	%rbx, %rbx
	je	.L1166
.L1167:
	testl	%r8d, %r8d
	jne	.L1171
	movl	$0, (%r14)
.L1172:
	leaq	.LC8(%rip), %rax
	testb	%r10b, %r10b
	je	.L1144
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	u_terminateChars_67@PLT
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1204
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1148:
	.cfi_restore_state
	movl	$2, (%r12)
	xorl	%eax, %eax
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1150:
	movl	$17, (%r9)
	xorl	%eax, %eax
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1152:
	leaq	40(%rdi), %rdi
	movl	%r15d, %edx
	movl	%r8d, -92(%rbp)
	movq	%rdi, -88(%rbp)
	call	res_getArrayItem_67@PLT
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1153:
	leaq	-68(%rbp), %rdx
	leaq	40(%rdi), %rdi
	movl	%r8d, -88(%rbp)
	call	res_getStringNoTrace_67@PLT
	movl	-88(%rbp), %r10d
	movq	%rax, %rcx
	movl	(%r12), %eax
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1149:
	movl	$5, (%r12)
	xorl	%eax, %eax
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1165:
	xorl	%esi, %esi
	testl	%r8d, %r8d
	je	.L1172
.L1171:
	cmpl	%esi, %r8d
	jg	.L1205
	testb	%r10b, %r10b
	jne	.L1169
	cmpl	$715827882, %r8d
	jg	.L1169
	leal	1(%r8,%r8,2), %eax
	cmpl	%esi, %eax
	jge	.L1169
	subl	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rbx
	movl	%eax, %esi
.L1169:
	movq	%r12, %r9
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	u_strToUTF8_67@PLT
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1166:
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1205:
	movq	%r12, %r9
	movq	%r14, %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	u_strToUTF8_67@PLT
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1203:
	movl	$1, (%r9)
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1155:
	movl	%r8d, -88(%rbp)
.L1200:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	%r15d, %esi
	call	ures_getByIndex_67
	xorl	%ecx, %ecx
	movl	-88(%rbp), %r10d
	movq	%rax, %r13
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1163
	testq	%r13, %r13
	je	.L1206
	movl	172(%r13), %esi
	leaq	-68(%rbp), %rdx
	leaq	40(%r13), %rdi
	movl	%r10d, -88(%rbp)
	call	res_getStringNoTrace_67@PLT
	movl	-88(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L1207
.L1163:
	movq	%r13, %rdi
	movl	%r10d, -92(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movl	(%r12), %eax
	movq	-88(%rbp), %rcx
	movl	-92(%rbp), %r10d
	jmp	.L1156
.L1207:
	movl	$17, (%r12)
	jmp	.L1163
.L1206:
	movl	$1, (%r12)
	movq	%r13, %rcx
	jmp	.L1163
.L1204:
	call	__stack_chk_fail@PLT
.L1177:
	xorl	%eax, %eax
	jmp	.L1144
	.cfi_endproc
.LFE3205:
	.size	ures_getUTF8StringByIndex_67, .-ures_getUTF8StringByIndex_67
	.p2align 4
	.globl	ures_findResource_67
	.type	ures_findResource_67, @function
ures_findResource_67:
.LFB3206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1209
	movl	(%rdx), %esi
	movq	%rdx, %rbx
	testl	%esi, %esi
	jle	.L1239
.L1209:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1240
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1239:
	.cfi_restore_state
	movq	%rdi, %r14
	call	strlen@PLT
	leal	1(%rax), %r15d
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1241
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	xorl	%r15d, %r15d
	call	memcpy@PLT
	cmpb	$47, 0(%r13)
	movq	%r13, %r14
	je	.L1242
.L1211:
	movl	$47, %esi
	movq	%r14, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L1213
	movb	$0, (%rax)
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	movl	(%rbx), %ecx
	movq	-88(%rbp), %r8
	movq	%rax, %r14
	testl	%ecx, %ecx
	jle	.L1243
.L1215:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1243:
	leaq	1(%r8), %rax
	movq	%r12, -112(%rbp)
	movq	%r14, %r9
	leaq	-64(%rbp), %r15
	movq	%rax, -72(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rax, -104(%rbp)
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	-96(%rbp), %r9
	subq	$8, %rsp
	movq	-64(%rbp), %rdx
	movl	$-1, %ecx
	movq	-88(%rbp), %rdi
	movq	8(%r9), %r8
	pushq	%rbx
	pushq	%r12
	pushq	$0
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	movq	%rax, %r9
	movq	-72(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L1218
.L1219:
	movq	%r9, -112(%rbp)
.L1220:
	movq	-104(%rbp), %rdx
	leaq	40(%r9), %rdi
	movq	%r15, %rcx
	movq	%r9, -96(%rbp)
	movl	172(%r9), %esi
	movq	%rdi, -88(%rbp)
	call	res_findResource_67@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L1216
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L1244
	movq	-72(%rbp), %rax
	movq	%r12, %r9
	cmpb	$0, (%rax)
	jne	.L1219
.L1218:
	movq	%r9, %r12
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	%r14, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1242:
	leaq	1(%r13), %r15
	movl	$47, %esi
	movq	%r15, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L1245
	movb	$0, (%rax)
	leaq	1(%rax), %r14
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	%r14, %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	movq	%rax, %r14
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1215
	cmpq	%r14, %r12
	je	.L1222
	testq	%r14, %r14
	je	.L1222
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	ures_copyResb_67.part.0
	movq	%rax, %r12
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1245:
	movl	$1, (%rbx)
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1216:
	movl	$2, (%rbx)
	movq	-112(%rbp), %r12
	jmp	.L1222
.L1240:
	call	__stack_chk_fail@PLT
.L1241:
	movl	$7, (%rbx)
	jmp	.L1209
	.cfi_endproc
.LFE3206:
	.size	ures_findResource_67, .-ures_findResource_67
	.p2align 4
	.globl	ures_findSubResource_67
	.type	ures_findSubResource_67, @function
ures_findSubResource_67:
.LFB3207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L1247
	movq	%rcx, %rbx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jg	.L1247
	movq	%rdx, -88(%rbp)
	movq	%rdi, %r15
	leaq	-64(%rbp), %r14
	leaq	-72(%rbp), %r13
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1259:
	subq	$8, %rsp
	movq	8(%r15), %r8
	movq	-64(%rbp), %rdx
	movq	%r15, %r9
	pushq	%rbx
	movl	$-1, %ecx
	movq	%r12, %rdi
	pushq	-80(%rbp)
	pushq	$0
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	movq	%rax, %r15
	movq	-72(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L1250
.L1251:
	movq	%r15, -88(%rbp)
.L1252:
	movl	172(%r15), %esi
	leaq	40(%r15), %r12
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	res_findResource_67@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L1248
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L1259
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r15
	cmpb	$0, (%rax)
	jne	.L1251
.L1250:
	movq	%r15, -80(%rbp)
.L1247:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1260
	movq	-80(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1248:
	.cfi_restore_state
	movq	-88(%rbp), %rax
	movl	$2, (%rbx)
	movq	%rax, -80(%rbp)
	jmp	.L1247
.L1260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3207:
	.size	ures_findSubResource_67, .-ures_findSubResource_67
	.p2align 4
	.globl	ures_getStringByKeyWithFallback_67
	.type	ures_getStringByKeyWithFallback_67, @function
ures_getStringByKeyWithFallback_67:
.LFB3208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	movl	$25, %ecx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	rep stosq
	testq	%rbx, %rbx
	je	.L1262
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1262
	testq	%r8, %r8
	je	.L1297
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%r8, %rdi
	call	ures_getByKeyWithFallback_67.part.0
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1298
	movl	-84(%rbp), %esi
	leaq	-260(%rbp), %rdx
	leaq	-216(%rbp), %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1266
.L1296:
	movq	-248(%rbp), %r12
.L1265:
	testq	%r12, %r12
	je	.L1267
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	.p2align 4,,10
	.p2align 3
.L1268:
	movq	%r12, %rax
	movq	16(%r12), %r12
	subl	$1, 108(%rax)
	testq	%r12, %r12
	jne	.L1268
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L1267:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1269
	call	uprv_free_67@PLT
.L1269:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1270
	leaq	-152(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1270
	call	uprv_free_67@PLT
.L1270:
	cmpl	$19700503, -76(%rbp)
	movl	$0, -88(%rbp)
	movq	$0, -224(%rbp)
	je	.L1299
.L1271:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1275
	movl	-260(%rbp), %eax
	cmpl	$3, %eax
	je	.L1300
.L1273:
	testq	%r14, %r14
	je	.L1261
	movl	%eax, (%r14)
.L1261:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1301
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1297:
	.cfi_restore_state
	movl	$1, (%rbx)
	.p2align 4,,10
	.p2align 3
.L1262:
	xorl	%r13d, %r13d
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1299:
	cmpl	$19641227, -72(%rbp)
	jne	.L1271
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1300:
	cmpw	$8709, 0(%r13)
	jne	.L1273
	cmpw	$8709, 2(%r13)
	jne	.L1273
	cmpw	$8709, 4(%r13)
	jne	.L1273
	movl	$2, (%rbx)
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1275:
	xorl	%r13d, %r13d
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1266:
	movl	$17, (%rbx)
	jmp	.L1296
.L1301:
	call	__stack_chk_fail@PLT
.L1298:
	movq	-248(%rbp), %r12
	xorl	%r13d, %r13d
	jmp	.L1265
	.cfi_endproc
.LFE3208:
	.size	ures_getStringByKeyWithFallback_67, .-ures_getStringByKeyWithFallback_67
	.p2align 4
	.globl	ures_getByKeyWithFallback_67
	.type	ures_getByKeyWithFallback_67, @function
ures_getByKeyWithFallback_67:
.LFB3210:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L1303
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1303
	testq	%rdi, %rdi
	je	.L1308
	jmp	ures_getByKeyWithFallback_67.part.0
	.p2align 4,,10
	.p2align 3
.L1308:
	movl	$1, (%rcx)
.L1303:
	movq	%rdx, %rax
	ret
	.cfi_endproc
.LFE3210:
	.size	ures_getByKeyWithFallback_67, .-ures_getByKeyWithFallback_67
	.p2align 4
	.globl	ures_getValueWithFallback_67
	.type	ures_getValueWithFallback_67, @function
ures_getValueWithFallback_67:
.LFB3212:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L1320
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L1314
	cmpb	$0, (%rsi)
	movq	%rcx, %rbx
	je	.L1313
	testq	%rdi, %rdi
	je	.L1314
	movq	%r8, %rcx
	call	ures_getByKeyWithFallback_67.part.0
	movq	%rax, %rdi
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1309
.L1313:
	movdqu	40(%rdi), %xmm0
	movups	%xmm0, 8(%rbx)
	movdqu	56(%rdi), %xmm1
	movups	%xmm1, 24(%rbx)
	movdqu	72(%rdi), %xmm2
	movups	%xmm2, 40(%rbx)
	movdqu	88(%rdi), %xmm3
	movups	%xmm3, 56(%rbx)
	movl	172(%rdi), %eax
	movl	%eax, 72(%rbx)
.L1309:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1320:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L1314:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$1, (%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3212:
	.size	ures_getValueWithFallback_67, .-ures_getValueWithFallback_67
	.p2align 4
	.globl	ures_getAllItemsWithFallback_67
	.type	ures_getAllItemsWithFallback_67, @function
ures_getAllItemsWithFallback_67:
.LFB3213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$288, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1323
	movq	%rcx, %r12
	testq	%rsi, %rsi
	je	.L1333
	leaq	-240(%rbp), %r14
	xorl	%eax, %eax
	movq	%rdi, %r8
	cmpb	$0, (%rsi)
	movl	$25, %ecx
	movq	%r14, %rdi
	movq	%rdx, %r13
	rep stosq
	je	.L1326
	testq	%r8, %r8
	je	.L1334
	movq	%r8, %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	call	ures_getByKeyWithFallback_67.part.0
	movq	%rax, %r8
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1332
.L1326:
	leaq	-320(%rbp), %r15
	movq	%r8, %rdi
	movq	%r12, %rcx
	movq	%r13, %rdx
	leaq	16+_ZTVN6icu_6717ResourceDataValueE(%rip), %rax
	movq	%r15, %rsi
	movl	$-1, -248(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZN12_GLOBAL__N_123getAllItemsWithFallbackEPK15UResourceBundleRN6icu_6717ResourceDataValueERNS3_12ResourceSinkER10UErrorCode
	movq	%r15, %rdi
	call	_ZN6icu_6717ResourceDataValueD1Ev@PLT
.L1332:
	movq	%r14, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
.L1323:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1335
	addq	$288, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1333:
	.cfi_restore_state
	movl	$1, (%rcx)
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1334:
	movl	$1, (%r12)
	jmp	.L1332
.L1335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3213:
	.size	ures_getAllItemsWithFallback_67, .-ures_getAllItemsWithFallback_67
	.p2align 4
	.globl	ures_getByKey_67
	.type	ures_getByKey_67, @function
ures_getByKey_67:
.LFB3214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$-1, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%rcx, %rcx
	je	.L1337
	movl	(%rcx), %r8d
	movq	%rcx, %rbx
	testl	%r8d, %r8d
	jg	.L1337
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L1350
	movq	%rsi, %r13
	movl	172(%rdi), %esi
	movl	%esi, %eax
	shrl	$28, %eax
	leal	-4(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1345
	cmpl	$2, %eax
	je	.L1345
	movl	$17, (%rcx)
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1351
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1345:
	.cfi_restore_state
	leaq	-64(%rbp), %r15
	leaq	40(%r14), %rdi
	leaq	-76(%rbp), %rdx
	movq	%r15, %rcx
	movq	%rdi, -88(%rbp)
	call	res_getTableItemByKey_67@PLT
	movq	-88(%rbp), %rdi
	cmpl	$-1, %eax
	movl	%eax, -80(%rbp)
	je	.L1352
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L1337
	subq	$8, %rsp
	movq	8(%r14), %r8
	movq	-64(%rbp), %rdx
	movq	%r14, %r9
	pushq	%rbx
	movl	$-1, %ecx
	movl	%eax, %esi
	pushq	%r12
	pushq	$0
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	movq	%rax, %r12
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1352:
	cmpb	$1, 176(%r14)
	movq	%r13, -64(%rbp)
	je	.L1353
.L1342:
	movl	$2, (%rbx)
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1350:
	movl	$1, (%rcx)
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1353:
	movq	8(%r14), %rdi
	leaq	-72(%rbp), %rcx
	movq	%rbx, %r9
	leaq	-80(%rbp), %r8
	leaq	176(%r14), %rsi
	movq	%r15, %rdx
	call	_ZL15getFallbackDataPK15UResourceBundlePPKcPP18UResourceDataEntryPjP10UErrorCode.isra.0
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1342
	subq	$8, %rsp
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rdx
	movq	%r14, %r9
	pushq	%rbx
	movl	-80(%rbp), %esi
	movl	$-1, %ecx
	movq	%rax, %rdi
	pushq	%r12
	pushq	$0
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	movq	%rax, %r12
	jmp	.L1337
.L1351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3214:
	.size	ures_getByKey_67, .-ures_getByKey_67
	.section	.rodata.str1.1
.LC9:
	.string	"res_index"
.LC10:
	.string	"InstalledLocales"
	.text
	.p2align 4
	.type	ures_openAvailableLocales_67.part.0, @function
ures_openAvailableLocales_67.part.0:
.LFB4267:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movl	$400, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	uprv_malloc_67@PLT
	movl	$56, %edi
	movq	%rax, %r12
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1360
	testq	%r12, %r12
	je	.L1360
	movdqa	_ZL12gLocalesEnum(%rip), %xmm0
	movdqa	16+_ZL12gLocalesEnum(%rip), %xmm1
	leaq	8(%r12), %rdi
	movq	%r12, %rcx
	movdqa	32+_ZL12gLocalesEnum(%rip), %xmm2
	andq	$-8, %rdi
	leaq	208(%r12), %rdx
	leaq	.LC9(%rip), %rsi
	movups	%xmm0, (%rax)
	subq	%rdi, %rcx
	andq	$-8, %rdx
	movups	%xmm1, 16(%rax)
	addl	$200, %ecx
	movups	%xmm2, 32(%rax)
	movq	48+_ZL12gLocalesEnum(%rip), %rax
	shrl	$3, %ecx
	movq	%rax, 48(%r13)
	xorl	%eax, %eax
	movq	$0, (%r12)
	movq	$0, 192(%r12)
	rep stosq
	movq	%rdx, %rdi
	movl	%r12d, %edx
	movq	$0, 180(%r12)
	movq	$0, 200(%r12)
	subl	%edi, %edx
	leal	400(%rdx), %ecx
	movq	$0, 392(%r12)
	movl	$2, %edx
	shrl	$3, %ecx
	rep stosq
	movq	%r14, %rdi
	movq	%rbx, %rcx
	movq	$0, 380(%r12)
	call	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	movq	%rbx, %rcx
	movq	%r12, %rdx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	ures_getByKey_67
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1358
	movq	%r12, 8(%r13)
.L1359:
	movq	%r14, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
.L1354:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1358:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	uprv_free_67@PLT
	jmp	.L1359
.L1360:
	movl	$7, (%rbx)
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	jmp	.L1354
	.cfi_endproc
.LFE4267:
	.size	ures_openAvailableLocales_67.part.0, .-ures_openAvailableLocales_67.part.0
	.p2align 4
	.globl	ures_getStringByKey_67
	.type	ures_getStringByKey_67, @function
ures_getStringByKey_67:
.LFB3215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$-1, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%rcx, %rcx
	je	.L1381
	movl	(%rcx), %r8d
	movq	%rcx, %rbx
	testl	%r8d, %r8d
	jg	.L1362
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L1398
	movq	%rsi, %r13
	movl	172(%rdi), %esi
	movq	%rdx, %r14
	movl	%esi, %edx
	shrl	$28, %edx
	leal	-4(%rdx), %ecx
	cmpl	$1, %ecx
	jbe	.L1385
	cmpl	$2, %edx
	jne	.L1365
.L1385:
	leaq	-64(%rbp), %r15
	leaq	40(%r12), %rdi
	movl	$0, -76(%rbp)
	leaq	-76(%rbp), %rdx
	movq	%r15, %rcx
	movq	%rdi, -88(%rbp)
	call	res_getTableItemByKey_67@PLT
	movq	-88(%rbp), %rdi
	cmpl	$-1, %eax
	movl	%eax, -80(%rbp)
	movl	%eax, %esi
	je	.L1399
	shrl	$28, %eax
	cmpl	$3, %eax
	je	.L1376
	cmpl	$6, %eax
	je	.L1377
	testl	%eax, %eax
	je	.L1377
.L1371:
	movl	$17, (%rbx)
.L1381:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1400
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1376:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movq	%r13, %rsi
	call	ures_getByKey_67
	movl	(%rbx), %edx
	movq	%rax, %r12
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1378
	testq	%r12, %r12
	je	.L1401
	movl	172(%r12), %esi
	leaq	40(%r12), %rdi
	movq	%r14, %rdx
	call	res_getStringNoTrace_67@PLT
	testq	%rax, %rax
	je	.L1402
.L1378:
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	-88(%rbp), %rax
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1365:
	movl	$17, (%rbx)
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1399:
	cmpb	$1, 176(%r12)
	movq	%r13, -64(%rbp)
	je	.L1403
.L1368:
	movl	$2, (%rbx)
	xorl	%eax, %eax
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1377:
	movq	%r14, %rdx
	call	res_getStringNoTrace_67@PLT
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	8(%r12), %rdi
	leaq	-72(%rbp), %rcx
	movq	%rbx, %r9
	leaq	-80(%rbp), %r8
	leaq	176(%r12), %rsi
	movq	%r15, %rdx
	call	_ZL15getFallbackDataPK15UResourceBundlePPKcPP18UResourceDataEntryPjP10UErrorCode.isra.0
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1368
	movl	-80(%rbp), %esi
	movl	%esi, %edx
	shrl	$28, %edx
	cmpl	$3, %edx
	je	.L1376
	cmpl	$6, %edx
	je	.L1370
	testl	%edx, %edx
	jne	.L1371
.L1370:
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	res_getStringNoTrace_67@PLT
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1398:
	movl	$1, (%rcx)
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1402:
	movl	$17, (%rbx)
	jmp	.L1378
	.p2align 4,,10
	.p2align 3
.L1401:
	movl	$1, (%rbx)
	movq	%r12, %rax
	jmp	.L1378
.L1400:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3215:
	.size	ures_getStringByKey_67, .-ures_getStringByKey_67
	.p2align 4
	.globl	ures_getUTF8StringByKey_67
	.type	ures_getUTF8StringByKey_67, @function
ures_getUTF8StringByKey_67:
.LFB3216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$-1, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testl	%r10d, %r10d
	jg	.L1404
	movq	%rdi, %r13
	movq	%r9, %r12
	testq	%rdi, %rdi
	je	.L1463
	movq	%rsi, %r10
	movl	172(%rdi), %esi
	movq	%rdx, %r15
	movq	%rcx, %r14
	movl	%r8d, %ebx
	movl	%esi, %edx
	shrl	$28, %edx
	leal	-4(%rdx), %ecx
	cmpl	$1, %ecx
	jbe	.L1437
	cmpl	$2, %edx
	jne	.L1408
.L1437:
	leaq	-64(%rbp), %r11
	leaq	40(%r13), %rdi
	movq	%r10, -120(%rbp)
	movq	%r11, %rcx
	leaq	-76(%rbp), %rdx
	movq	%r11, -112(%rbp)
	movq	%rdi, -104(%rbp)
	movl	$0, -76(%rbp)
	call	res_getTableItemByKey_67@PLT
	movq	-104(%rbp), %rdi
	movq	-112(%rbp), %r11
	cmpl	$-1, %eax
	movl	%eax, -80(%rbp)
	movq	-120(%rbp), %r10
	movl	%eax, %esi
	je	.L1464
	shrl	$28, %eax
	cmpl	$3, %eax
	je	.L1418
	cmpl	$6, %eax
	je	.L1419
	testl	%eax, %eax
	je	.L1419
.L1414:
	movl	$17, (%r12)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1465
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1418:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rcx
	movq	%r10, %rsi
	call	ures_getByKey_67
	movl	(%r12), %edx
	xorl	%r13d, %r13d
	movq	%rax, %r8
	testl	%edx, %edx
	jg	.L1420
	testq	%rax, %rax
	je	.L1466
	movl	172(%rax), %esi
	leaq	-84(%rbp), %rdx
	leaq	40(%rax), %rdi
	movq	%rax, -104(%rbp)
	call	res_getStringNoTrace_67@PLT
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1467
.L1420:
	movq	%r8, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
.L1415:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1468
	movl	-84(%rbp), %r8d
	testq	%r14, %r14
	je	.L1423
	movl	(%r14), %esi
	testl	%esi, %esi
	js	.L1424
	jle	.L1425
	testq	%r15, %r15
	je	.L1424
.L1425:
	testl	%r8d, %r8d
	jne	.L1429
	movl	$0, (%r14)
.L1430:
	leaq	.LC8(%rip), %rax
	testb	%bl, %bl
	je	.L1404
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	u_terminateChars_67@PLT
	movq	%r15, %rax
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1423:
	xorl	%esi, %esi
	testl	%r8d, %r8d
	je	.L1430
.L1429:
	cmpl	%esi, %r8d
	jg	.L1469
	testb	%bl, %bl
	jne	.L1427
	cmpl	$715827882, %r8d
	jg	.L1427
	leal	1(%r8,%r8,2), %eax
	cmpl	%esi, %eax
	jge	.L1427
	subl	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %r15
	movl	%eax, %esi
.L1427:
	movq	%r12, %r9
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	u_strToUTF8_67@PLT
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1408:
	movl	$17, (%r9)
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1424:
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1464:
	cmpb	$1, 176(%r13)
	movq	%r10, -64(%rbp)
	je	.L1470
.L1411:
	movl	$2, (%r12)
	xorl	%eax, %eax
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1419:
	leaq	-84(%rbp), %rdx
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r13
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1469:
	movq	%r12, %r9
	movq	%r13, %rcx
	movq	%r14, %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	u_strToUTF8_67@PLT
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1470:
	movq	8(%r13), %rdi
	leaq	-72(%rbp), %rcx
	movq	%r12, %r9
	leaq	-80(%rbp), %r8
	leaq	176(%r13), %rsi
	movq	%r11, %rdx
	movq	%r10, -104(%rbp)
	call	_ZL15getFallbackDataPK15UResourceBundlePPKcPP18UResourceDataEntryPjP10UErrorCode.isra.0
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L1411
	movl	-80(%rbp), %esi
	movq	-104(%rbp), %r10
	movl	%esi, %edx
	shrl	$28, %edx
	cmpl	$3, %edx
	je	.L1418
	cmpl	$6, %edx
	je	.L1413
	testl	%edx, %edx
	jne	.L1414
.L1413:
	leaq	-84(%rbp), %rdx
	movq	%rax, %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r13
	jmp	.L1415
	.p2align 4,,10
	.p2align 3
.L1463:
	movl	$1, (%r9)
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1467:
	movl	$17, (%r12)
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1466:
	movl	$1, (%r12)
	movq	%rax, %r13
	jmp	.L1420
.L1465:
	call	__stack_chk_fail@PLT
.L1468:
	xorl	%eax, %eax
	jmp	.L1404
	.cfi_endproc
.LFE3216:
	.size	ures_getUTF8StringByKey_67, .-ures_getUTF8StringByKey_67
	.p2align 4
	.globl	ures_getLocaleInternal_67
	.type	ures_getLocaleInternal_67, @function
ures_getLocaleInternal_67:
.LFB3217:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1471
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L1471
	testq	%rdi, %rdi
	je	.L1476
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1471:
	ret
	.p2align 4,,10
	.p2align 3
.L1476:
	movl	$1, (%rsi)
	ret
	.cfi_endproc
.LFE3217:
	.size	ures_getLocaleInternal_67, .-ures_getLocaleInternal_67
	.p2align 4
	.globl	ures_getLocale_67
	.type	ures_getLocale_67, @function
ures_getLocale_67:
.LFB3218:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1477
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L1477
	testq	%rdi, %rdi
	je	.L1482
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1477:
	ret
	.p2align 4,,10
	.p2align 3
.L1482:
	movl	$1, (%rsi)
	ret
	.cfi_endproc
.LFE3218:
	.size	ures_getLocale_67, .-ures_getLocale_67
	.p2align 4
	.globl	ures_getLocaleByType_67
	.type	ures_getLocaleByType_67, @function
ures_getLocaleByType_67:
.LFB3219:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1483
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L1483
	testq	%rdi, %rdi
	je	.L1491
	testl	%esi, %esi
	je	.L1486
	cmpl	$1, %esi
	je	.L1487
.L1491:
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L1483:
	ret
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE3219:
	.size	ures_getLocaleByType_67, .-ures_getLocaleByType_67
	.p2align 4
	.globl	ures_getName_67
	.type	ures_getName_67, @function
ures_getName_67:
.LFB3220:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1494
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1494:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3220:
	.size	ures_getName_67, .-ures_getName_67
	.p2align 4
	.globl	ures_open_67
	.type	ures_open_67, @function
ures_open_67:
.LFB3222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1506
	leaq	-208(%rbp), %r13
	movq	%rdx, %rbx
	movq	%rdx, %rcx
	movq	%rdi, %r12
	movl	$157, %edx
	movq	%rsi, %rdi
	movq	%r13, %rsi
	call	uloc_getBaseName_67@PLT
	movl	(%rbx), %eax
	cmpl	$-124, %eax
	je	.L1498
	testl	%eax, %eax
	jg	.L1498
	movq	%r13, %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZL9entryOpenPKcS0_12UResOpenTypeP10UErrorCode
	movq	%rax, %r13
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1506
	testq	%r13, %r13
	je	.L1507
	movl	$200, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1508
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	$0, 192(%rax)
	andq	$-8, %rdi
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$200, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%r13, 8(%r12)
	leaq	40(%r12), %rdi
	movabsq	$84358427638012695, %rax
	movq	%rax, 180(%r12)
	movq	%r13, 24(%r12)
	movdqu	40(%r13), %xmm0
	movups	%xmm0, 40(%r12)
	movdqu	56(%r13), %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	72(%r13), %xmm2
	movups	%xmm2, 32(%rdi)
	movdqu	88(%r13), %xmm3
	movups	%xmm3, 48(%rdi)
	movl	72(%r12), %esi
	cmpb	$0, 96(%r12)
	movb	$1, 177(%r12)
	movl	%esi, 172(%r12)
	sete	176(%r12)
	call	res_countArrayItems_67@PLT
	movl	$-1, 188(%r12)
	movl	%eax, 192(%r12)
.L1495:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1509
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1498:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1507:
	movl	$2, (%rbx)
	.p2align 4,,10
	.p2align 3
.L1506:
	xorl	%r12d, %r12d
	jmp	.L1495
.L1509:
	call	__stack_chk_fail@PLT
.L1508:
	movq	%r13, %rdi
	call	_ZL10entryCloseP18UResourceDataEntry
	movl	$7, (%rbx)
	jmp	.L1495
	.cfi_endproc
.LFE3222:
	.size	ures_open_67, .-ures_open_67
	.p2align 4
	.globl	ures_openNoDefault_67
	.type	ures_openNoDefault_67, @function
ures_openNoDefault_67:
.LFB3223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1521
	leaq	-208(%rbp), %r13
	movq	%rdx, %rbx
	movq	%rdx, %rcx
	movq	%rdi, %r12
	movl	$157, %edx
	movq	%rsi, %rdi
	movq	%r13, %rsi
	call	uloc_getBaseName_67@PLT
	movl	(%rbx), %eax
	cmpl	$-124, %eax
	je	.L1513
	testl	%eax, %eax
	jg	.L1513
	movq	%r13, %rsi
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZL9entryOpenPKcS0_12UResOpenTypeP10UErrorCode
	movq	%rax, %r13
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1521
	testq	%r13, %r13
	je	.L1522
	movl	$200, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1523
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	$0, 192(%rax)
	andq	$-8, %rdi
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$200, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%r13, 8(%r12)
	leaq	40(%r12), %rdi
	movabsq	$84358427638012695, %rax
	movq	%rax, 180(%r12)
	movq	%r13, 24(%r12)
	movdqu	40(%r13), %xmm0
	movups	%xmm0, 40(%r12)
	movdqu	56(%r13), %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	72(%r13), %xmm2
	movups	%xmm2, 32(%rdi)
	movdqu	88(%r13), %xmm3
	movups	%xmm3, 48(%rdi)
	movl	72(%r12), %esi
	cmpb	$0, 96(%r12)
	movb	$1, 177(%r12)
	movl	%esi, 172(%r12)
	sete	176(%r12)
	call	res_countArrayItems_67@PLT
	movl	$-1, 188(%r12)
	movl	%eax, 192(%r12)
.L1510:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1524
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1513:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1522:
	movl	$2, (%rbx)
	.p2align 4,,10
	.p2align 3
.L1521:
	xorl	%r12d, %r12d
	jmp	.L1510
.L1524:
	call	__stack_chk_fail@PLT
.L1523:
	movq	%r13, %rdi
	call	_ZL10entryCloseP18UResourceDataEntry
	movl	$7, (%rbx)
	jmp	.L1510
	.cfi_endproc
.LFE3223:
	.size	ures_openNoDefault_67, .-ures_openNoDefault_67
	.p2align 4
	.globl	ures_openDirect_67
	.type	ures_openDirect_67, @function
ures_openDirect_67:
.LFB3224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L1532
	movq	%rdx, %rbx
	call	_ZL15entryOpenDirectPKcS0_P10UErrorCode
	movl	(%rbx), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jg	.L1532
	testq	%rax, %rax
	je	.L1533
	movl	$200, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1534
	leaq	8(%rax), %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	$0, 192(%rax)
	andq	$-8, %rdi
	xorl	%eax, %eax
	subq	%rdi, %rcx
	addl	$200, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%r13, 8(%r12)
	leaq	40(%r12), %rdi
	movabsq	$84358427638012695, %rax
	movq	%rax, 180(%r12)
	movl	$256, %eax
	movq	%r13, 24(%r12)
	movdqu	40(%r13), %xmm0
	movups	%xmm0, 40(%r12)
	movdqu	56(%r13), %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	72(%r13), %xmm2
	movups	%xmm2, 32(%rdi)
	movdqu	88(%r13), %xmm3
	movups	%xmm3, 48(%rdi)
	movl	72(%r12), %esi
	movw	%ax, 176(%r12)
	movl	%esi, 172(%r12)
	call	res_countArrayItems_67@PLT
	movl	$-1, 188(%r12)
	movl	%eax, 192(%r12)
.L1525:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1533:
	.cfi_restore_state
	movl	$2, (%rbx)
.L1532:
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1534:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZL10entryCloseP18UResourceDataEntry
	movl	$7, (%rbx)
	jmp	.L1525
	.cfi_endproc
.LFE3224:
	.size	ures_openDirect_67, .-ures_openDirect_67
	.p2align 4
	.globl	ures_openFillIn_67
	.type	ures_openFillIn_67, @function
ures_openFillIn_67:
.LFB3225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$176, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1535
	movq	%rdi, %rbx
	movq	%rcx, %r12
	testq	%rdi, %rdi
	je	.L1539
	leaq	-208(%rbp), %r14
	movq	%rdx, %rdi
	movq	%rsi, %r13
	movl	$157, %edx
	movq	%r14, %rsi
	call	uloc_getBaseName_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1539
	cmpl	$-124, %eax
	je	.L1539
	movq	%r13, %rdi
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZL9entryOpenPKcS0_12UResOpenTypeP10UErrorCode
	movq	%rax, %r13
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L1535
	testq	%r13, %r13
	je	.L1564
	cmpl	$19700503, 180(%rbx)
	movl	$1, %r14d
	je	.L1565
.L1543:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L1544
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	%r12, %rax
	movq	16(%r12), %r12
	subl	$1, 108(%rax)
	testq	%r12, %r12
	jne	.L1545
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L1544:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1546
	call	uprv_free_67@PLT
.L1546:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1547
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1547
	call	uprv_free_67@PLT
.L1547:
	leaq	8(%rbx), %rdi
	movq	%rbx, %rcx
	xorl	%eax, %eax
	movq	$0, (%rbx)
	movabsq	$84358427638012695, %rdx
	andq	$-8, %rdi
	movq	$0, 192(%rbx)
	subq	%rdi, %rcx
	addl	$200, %ecx
	shrl	$3, %ecx
	testb	%r14b, %r14b
	rep stosq
	cmove	%rdx, %rax
	movq	%r13, 8(%rbx)
	leaq	40(%rbx), %rdi
	movq	%r13, 24(%rbx)
	movq	%rax, 180(%rbx)
	movdqu	40(%r13), %xmm0
	movups	%xmm0, 40(%rbx)
	movdqu	56(%r13), %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	72(%r13), %xmm2
	movups	%xmm2, 32(%rdi)
	movdqu	88(%r13), %xmm3
	movups	%xmm3, 48(%rdi)
	movl	72(%rbx), %esi
	cmpb	$0, 96(%rbx)
	movb	$1, 177(%rbx)
	movl	%esi, 172(%rbx)
	sete	176(%rbx)
	call	res_countArrayItems_67@PLT
	movl	$-1, 188(%rbx)
	movl	%eax, 192(%rbx)
.L1535:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1566
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1539:
	.cfi_restore_state
	movl	$1, (%r12)
	jmp	.L1535
	.p2align 4,,10
	.p2align 3
.L1565:
	cmpl	$19641227, 184(%rbx)
	setne	%r14b
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1564:
	movl	$2, (%r12)
	jmp	.L1535
.L1566:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3225:
	.size	ures_openFillIn_67, .-ures_openFillIn_67
	.p2align 4
	.globl	ures_openDirectFillIn_67
	.type	ures_openDirectFillIn_67, @function
ures_openDirectFillIn_67:
.LFB3226:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L1592
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L1595
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	call	_ZL15entryOpenDirectPKcS0_P10UErrorCode
	movl	(%r12), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jg	.L1567
	testq	%rax, %rax
	je	.L1596
	cmpl	$19700503, 180(%rbx)
	movl	$1, %r14d
	jne	.L1573
	cmpl	$19641227, 184(%rbx)
	setne	%r14b
.L1573:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L1574
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	.p2align 4,,10
	.p2align 3
.L1575:
	movq	%r12, %rax
	movq	16(%r12), %r12
	subl	$1, 108(%rax)
	testq	%r12, %r12
	jne	.L1575
	leaq	_ZL9resbMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L1574:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1576
	call	uprv_free_67@PLT
.L1576:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1577
	leaq	104(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1577
	call	uprv_free_67@PLT
.L1577:
	leaq	8(%rbx), %rdi
	movq	%rbx, %rcx
	xorl	%eax, %eax
	movq	$0, (%rbx)
	movabsq	$84358427638012695, %rdx
	andq	$-8, %rdi
	movq	$0, 192(%rbx)
	subq	%rdi, %rcx
	addl	$200, %ecx
	shrl	$3, %ecx
	testb	%r14b, %r14b
	rep stosq
	cmove	%rdx, %rax
	movq	%r13, 8(%rbx)
	leaq	40(%rbx), %rdi
	movq	%r13, 24(%rbx)
	movq	%rax, 180(%rbx)
	movdqu	40(%r13), %xmm0
	movl	$256, %eax
	movups	%xmm0, 40(%rbx)
	movdqu	56(%r13), %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	72(%r13), %xmm2
	movups	%xmm2, 32(%rdi)
	movdqu	88(%r13), %xmm3
	movups	%xmm3, 48(%rdi)
	movl	72(%rbx), %esi
	movw	%ax, 176(%rbx)
	movl	%esi, 172(%rbx)
	call	res_countArrayItems_67@PLT
	movl	$-1, 188(%rbx)
	movl	%eax, 192(%rbx)
.L1567:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1592:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L1595:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$1, (%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1596:
	.cfi_restore_state
	movl	$2, (%r12)
	jmp	.L1567
	.cfi_endproc
.LFE3226:
	.size	ures_openDirectFillIn_67, .-ures_openDirectFillIn_67
	.p2align 4
	.globl	ures_countArrayItems_67
	.type	ures_countArrayItems_67, @function
ures_countArrayItems_67:
.LFB3227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movl	$25, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-240(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	rep stosq
	testq	%rdx, %rdx
	je	.L1602
	movl	(%rdx), %eax
	movq	%rdx, %rbx
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jg	.L1597
	testq	%r8, %r8
	je	.L1605
	movq	%rdx, %rcx
	movq	%r8, %rdi
	movq	%r12, %rdx
	call	ures_getByKey_67
	cmpq	$0, -200(%rbp)
	je	.L1600
	movl	-68(%rbp), %esi
	leaq	-200(%rbp), %rdi
	call	res_countArrayItems_67@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
.L1597:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1606
	addq	$216, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1602:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1605:
	movl	$1, (%rdx)
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1600:
	movl	$2, (%rbx)
	movq	%r12, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	jmp	.L1597
.L1606:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3227:
	.size	ures_countArrayItems_67, .-ures_countArrayItems_67
	.section	.rodata.str1.1
.LC11:
	.string	"Version"
	.text
	.p2align 4
	.globl	ures_getVersionNumberInternal_67
	.type	ures_getVersionNumberInternal_67, @function
ures_getVersionNumberInternal_67:
.LFB3228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1630
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L1645
.L1607:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1646
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1645:
	.cfi_restore_state
	movl	172(%rdi), %esi
	leaq	.LC11(%rip), %r13
	movl	$0, -88(%rbp)
	movl	$0, -84(%rbp)
	movl	%esi, %eax
	movl	$-1, -80(%rbp)
	shrl	$28, %eax
	movq	$0, -72(%rbp)
	leal	-4(%rax), %edx
	movq	%r13, -64(%rbp)
	cmpl	$1, %edx
	jbe	.L1631
	cmpl	$2, %eax
	jne	.L1610
.L1631:
	leaq	40(%rbx), %r15
	leaq	-64(%rbp), %r14
	movl	$0, -76(%rbp)
	leaq	-76(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	call	res_getTableItemByKey_67@PLT
	movl	%eax, -80(%rbp)
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L1647
	shrl	$28, %eax
	cmpl	$3, %eax
	je	.L1621
	cmpl	$6, %eax
	je	.L1622
	testl	%eax, %eax
	je	.L1622
.L1616:
	movl	$17, -88(%rbp)
.L1617:
	movl	-84(%rbp), %edx
	movl	$1, %r13d
	testl	%edx, %edx
	cmovg	-84(%rbp), %r13d
	leal	1(%r13), %edi
	movslq	%edi, %rdi
.L1625:
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1630
	movl	-84(%rbp), %edx
	testl	%edx, %edx
	jle	.L1627
	movq	%r12, %rdi
	movslq	%r13d, %r13
	call	u_UCharsToChars_67@PLT
	movq	16(%rbx), %rax
	movb	$0, (%rax,%r13)
	movq	16(%rbx), %r12
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1630:
	xorl	%r12d, %r12d
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1621:
	leaq	-88(%rbp), %rcx
.L1644:
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	ures_getByKey_67
	movl	-88(%rbp), %ecx
	movq	%rax, %r13
	testl	%ecx, %ecx
	jg	.L1623
	testq	%rax, %rax
	je	.L1648
	movl	172(%rax), %esi
	leaq	-84(%rbp), %rdx
	leaq	40(%rax), %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1649
.L1623:
	movq	%r13, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1622:
	leaq	-84(%rbp), %rdx
	movq	%r15, %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r12
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1627:
	movl	$48, %eax
	movq	%rsi, %r12
	movw	%ax, (%rsi)
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1610:
	movl	$17, -88(%rbp)
	movl	$1, %r13d
	movl	$2, %edi
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1647:
	cmpb	$1, 176(%rbx)
	movq	%r13, -64(%rbp)
	je	.L1650
.L1613:
	movl	$2, -88(%rbp)
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1650:
	movq	8(%rbx), %rdi
	leaq	-88(%rbp), %r13
	leaq	-72(%rbp), %rcx
	movq	%r14, %rdx
	leaq	176(%rbx), %rsi
	movq	%r13, %r9
	leaq	-80(%rbp), %r8
	call	_ZL15getFallbackDataPK15UResourceBundlePPKcPP18UResourceDataEntryPjP10UErrorCode.isra.0
	movl	-88(%rbp), %esi
	testl	%esi, %esi
	jg	.L1613
	movl	-80(%rbp), %esi
	movl	%esi, %edx
	shrl	$28, %edx
	cmpl	$3, %edx
	je	.L1614
	cmpl	$6, %edx
	je	.L1615
	testl	%edx, %edx
	jne	.L1616
.L1615:
	leaq	-84(%rbp), %rdx
	movq	%rax, %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r12
	jmp	.L1617
.L1649:
	movl	$17, -88(%rbp)
	jmp	.L1623
.L1648:
	movl	$1, -88(%rbp)
	xorl	%r12d, %r12d
	jmp	.L1623
.L1614:
	movq	%r13, %rcx
	jmp	.L1644
.L1646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3228:
	.size	ures_getVersionNumberInternal_67, .-ures_getVersionNumberInternal_67
	.p2align 4
	.globl	ures_getVersionNumber_67
	.type	ures_getVersionNumber_67, @function
ures_getVersionNumber_67:
.LFB3229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1674
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L1689
.L1651:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1690
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1689:
	.cfi_restore_state
	movl	172(%rdi), %esi
	leaq	.LC11(%rip), %r13
	movl	$0, -88(%rbp)
	movl	$0, -84(%rbp)
	movl	%esi, %eax
	movl	$-1, -80(%rbp)
	shrl	$28, %eax
	movq	$0, -72(%rbp)
	leal	-4(%rax), %edx
	movq	%r13, -64(%rbp)
	cmpl	$1, %edx
	jbe	.L1675
	cmpl	$2, %eax
	jne	.L1654
.L1675:
	leaq	40(%rbx), %r15
	leaq	-64(%rbp), %r14
	movl	$0, -76(%rbp)
	leaq	-76(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	call	res_getTableItemByKey_67@PLT
	movl	%eax, -80(%rbp)
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L1691
	shrl	$28, %eax
	cmpl	$3, %eax
	je	.L1665
	cmpl	$6, %eax
	je	.L1666
	testl	%eax, %eax
	je	.L1666
.L1660:
	movl	$17, -88(%rbp)
.L1661:
	movl	-84(%rbp), %edx
	movl	$1, %r13d
	testl	%edx, %edx
	cmovg	-84(%rbp), %r13d
	leal	1(%r13), %edi
	movslq	%edi, %rdi
.L1669:
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1674
	movl	-84(%rbp), %edx
	testl	%edx, %edx
	jle	.L1671
	movq	%r12, %rdi
	movslq	%r13d, %r13
	call	u_UCharsToChars_67@PLT
	movq	16(%rbx), %rax
	movb	$0, (%rax,%r13)
	movq	16(%rbx), %r12
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1674:
	xorl	%r12d, %r12d
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1665:
	leaq	-88(%rbp), %rcx
.L1688:
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	ures_getByKey_67
	movl	-88(%rbp), %ecx
	movq	%rax, %r13
	testl	%ecx, %ecx
	jg	.L1667
	testq	%rax, %rax
	je	.L1692
	movl	172(%rax), %esi
	leaq	-84(%rbp), %rdx
	leaq	40(%rax), %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1693
.L1667:
	movq	%r13, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1666:
	leaq	-84(%rbp), %rdx
	movq	%r15, %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r12
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1671:
	movl	$48, %eax
	movq	%rsi, %r12
	movw	%ax, (%rsi)
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1654:
	movl	$17, -88(%rbp)
	movl	$2, %edi
	movl	$1, %r13d
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1691:
	cmpb	$1, 176(%rbx)
	movq	%r13, -64(%rbp)
	je	.L1694
.L1657:
	movl	$2, -88(%rbp)
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	8(%rbx), %rdi
	leaq	-88(%rbp), %r13
	leaq	-72(%rbp), %rcx
	movq	%r14, %rdx
	leaq	176(%rbx), %rsi
	movq	%r13, %r9
	leaq	-80(%rbp), %r8
	call	_ZL15getFallbackDataPK15UResourceBundlePPKcPP18UResourceDataEntryPjP10UErrorCode.isra.0
	movl	-88(%rbp), %esi
	testl	%esi, %esi
	jg	.L1657
	movl	-80(%rbp), %esi
	movl	%esi, %edx
	shrl	$28, %edx
	cmpl	$3, %edx
	je	.L1658
	cmpl	$6, %edx
	je	.L1659
	testl	%edx, %edx
	jne	.L1660
.L1659:
	leaq	-84(%rbp), %rdx
	movq	%rax, %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r12
	jmp	.L1661
.L1693:
	movl	$17, -88(%rbp)
	jmp	.L1667
.L1692:
	movl	$1, -88(%rbp)
	xorl	%r12d, %r12d
	jmp	.L1667
.L1658:
	movq	%r13, %rcx
	jmp	.L1688
.L1690:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3229:
	.size	ures_getVersionNumber_67, .-ures_getVersionNumber_67
	.p2align 4
	.globl	ures_getVersion_67
	.type	ures_getVersion_67, @function
ures_getVersion_67:
.LFB3230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1695
	movq	%rsi, %r14
	movq	16(%rdi), %rsi
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L1707
.L1699:
	movq	%r14, %rdi
	call	u_versionFromString_67@PLT
.L1695:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1708
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1707:
	.cfi_restore_state
	leaq	-48(%rbp), %rcx
	leaq	.LC11(%rip), %rsi
	movl	$0, -48(%rbp)
	movl	$1, %r12d
	leaq	-44(%rbp), %rdx
	movl	$0, -44(%rbp)
	call	ures_getStringByKey_67
	movl	-44(%rbp), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	cmovg	-44(%rbp), %r12d
	leal	1(%r12), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1699
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jle	.L1700
	movq	%r13, %rdi
	movslq	%r12d, %r12
	call	u_UCharsToChars_67@PLT
	movq	16(%rbx), %rax
	movb	$0, (%rax,%r12)
	movq	16(%rbx), %rsi
	jmp	.L1699
	.p2align 4,,10
	.p2align 3
.L1700:
	movl	$48, %eax
	movw	%ax, (%rsi)
	jmp	.L1699
.L1708:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3230:
	.size	ures_getVersion_67, .-ures_getVersion_67
	.p2align 4
	.globl	ures_openAvailableLocales_67
	.type	ures_openAvailableLocales_67, @function
ures_openAvailableLocales_67:
.LFB3235:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1710
	jmp	ures_openAvailableLocales_67.part.0
	.p2align 4,,10
	.p2align 3
.L1710:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3235:
	.size	ures_openAvailableLocales_67, .-ures_openAvailableLocales_67
	.section	.rodata.str1.1
.LC12:
	.string	"default"
.LC13:
	.string	"="
	.text
	.p2align 4
	.globl	ures_getFunctionalEquivalent_67
	.type	ures_getFunctionalEquivalent_67, @function
ures_getFunctionalEquivalent_67:
.LFB3237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$3640, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	pxor	%xmm0, %xmm0
	movq	16(%rbp), %rax
	movq	32(%rbp), %rbx
	movl	(%rbx), %r10d
	movq	%rax, -7680(%rbp)
	movl	24(%rbp), %eax
	movq	%rdi, -7712(%rbp)
	leaq	-7216(%rbp), %rdi
	movq	%rcx, -7688(%rbp)
	movl	$126, %ecx
	movl	%esi, -7716(%rbp)
	movq	%rdx, -7672(%rbp)
	movq	%r8, -7704(%rbp)
	movl	%eax, -7720(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -7232(%rbp)
	movl	$0, -7656(%rbp)
	movaps	%xmm0, -6208(%rbp)
	movaps	%xmm0, -5184(%rbp)
	movaps	%xmm0, -4160(%rbp)
	movaps	%xmm0, -3136(%rbp)
	rep stosq
	leaq	-6192(%rbp), %rdi
	movl	$126, %ecx
	movaps	%xmm0, -2112(%rbp)
	rep stosq
	leaq	-5168(%rbp), %rdi
	movl	$126, %ecx
	movaps	%xmm0, -1088(%rbp)
	rep stosq
	leaq	-4144(%rbp), %rdi
	movl	$126, %ecx
	rep stosq
	leaq	-3120(%rbp), %rdi
	movl	$126, %ecx
	rep stosq
	leaq	-2096(%rbp), %rdi
	movl	$126, %ecx
	rep stosq
	leaq	-1072(%rbp), %rdi
	movl	$126, %ecx
	rep stosq
	testl	%r10d, %r10d
	jle	.L1839
.L1711:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1840
	addq	$7736, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1839:
	.cfi_restore_state
	leaq	-7232(%rbp), %rsi
	leaq	-7656(%rbp), %r13
	movq	%r9, %rdi
	movq	%r9, %r12
	movq	%rsi, -7736(%rbp)
	movq	%rsi, %r15
	movq	%rsi, %rdx
	movq	%r13, %r8
	movq	-7704(%rbp), %rsi
	movl	$1023, %ecx
	call	uloc_getKeywordValue_67@PLT
	movl	$8, %ecx
	leaq	.LC12(%rip), %rdi
	movq	%r15, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1841
.L1713:
	leaq	-4160(%rbp), %r14
	movq	%r12, %rdi
	movq	%r13, %rcx
	movl	$1023, %edx
	movq	%r14, %rsi
	movq	%r14, -7744(%rbp)
	leaq	-2112(%rbp), %r12
	movq	%r14, %r15
	call	uloc_getBaseName_67@PLT
	xorl	%eax, %eax
	movl	$25, %ecx
	movq	%r14, %rsi
	leaq	-7648(%rbp), %rdi
	movl	$1024, %edx
	leaq	-3136(%rbp), %r14
	movq	%rdi, -7696(%rbp)
	rep stosq
	leaq	-7440(%rbp), %rdi
	movl	$25, %ecx
	movq	%rdi, -7728(%rbp)
	rep stosq
	movq	%r12, %rdi
	call	__strcpy_chk@PLT
	movl	$1024, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	__strcpy_chk@PLT
	cmpq	$0, -7680(%rbp)
	je	.L1714
	movl	-7656(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L1715
	movq	-7672(%rbp), %rdi
	movq	%r13, %rsi
	call	ures_openAvailableLocales_67.part.0
	movl	-7656(%rbp), %edi
	movq	%rax, %r15
	movq	-7680(%rbp), %rax
	movb	$1, (%rax)
	testl	%edi, %edi
	jle	.L1718
.L1716:
	movq	%r15, %rdi
	call	uenum_close_67@PLT
.L1714:
	movl	-7656(%rbp), %eax
	testl	%eax, %eax
	jle	.L1719
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1720:
	testl	%eax, %eax
	jle	.L1722
	movl	%eax, (%rbx)
.L1723:
	movl	$0, -7656(%rbp)
	testq	%r15, %r15
	je	.L1727
	movq	24(%r15), %rax
	movl	$1024, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	__strcpy_chk@PLT
.L1727:
	movq	%r13, %rcx
	movl	$1024, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	uloc_getParent_67@PLT
	movq	%r15, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	cmpb	$0, -6208(%rbp)
	jne	.L1728
	cmpb	$0, -3136(%rbp)
	je	.L1728
	movl	$5, %ecx
	movq	%r14, %rsi
	leaq	.LC0(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1728
	movq	$0, -7680(%rbp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1728
.L1719:
	movq	-7672(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rcx
	movq	%r12, %rsi
	movl	$0, -7656(%rbp)
	call	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	movq	%rax, %r15
	movl	-7656(%rbp), %eax
	leal	128(%rax), %edx
	cmpl	$1, %edx
	ja	.L1720
	movq	-7680(%rbp), %rax
	testq	%rax, %rax
	je	.L1723
	movb	$0, (%rax)
	jmp	.L1723
.L1722:
	jne	.L1723
	movq	-7688(%rbp), %rsi
	movq	-7696(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	ures_getByKey_67
	movl	-7656(%rbp), %esi
	testl	%esi, %esi
	jne	.L1723
	movq	-7696(%rbp), %rdi
	movq	%r13, %rcx
	leaq	-7652(%rbp), %rdx
	leaq	.LC12(%rip), %rsi
	call	ures_getStringByKey_67
	movl	-7656(%rbp), %ecx
	movq	%rax, %rdi
	testl	%ecx, %ecx
	jg	.L1723
	movl	-7652(%rbp), %edx
	testl	%edx, %edx
	je	.L1723
	movq	%rax, -7752(%rbp)
	call	u_strlen_67@PLT
	leaq	-6208(%rbp), %r9
	movq	-7752(%rbp), %rdi
	movq	%r9, %rsi
	movl	%eax, %edx
	movq	%r9, -7680(%rbp)
	call	u_UCharsToChars_67@PLT
	leaq	-5184(%rbp), %rdi
	movl	$1024, %edx
	movq	%r12, %rsi
	call	__strcpy_chk@PLT
	cmpb	$0, -7232(%rbp)
	movq	-7680(%rbp), %r9
	jne	.L1723
	movq	-7736(%rbp), %rdi
	movl	$1024, %edx
	movq	%r9, %rsi
	call	__strcpy_chk@PLT
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1728:
	movq	-7744(%rbp), %r15
	movl	$1024, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	__strcpy_chk@PLT
	movl	$1024, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	__strcpy_chk@PLT
	leaq	-1088(%rbp), %rax
	movq	%rax, -7680(%rbp)
	jmp	.L1740
	.p2align 4,,10
	.p2align 3
.L1843:
	movl	%eax, (%rbx)
.L1730:
	movl	$1024, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$0, -7656(%rbp)
	call	__strcpy_chk@PLT
	movq	%r14, %rdi
	movq	%r13, %rcx
	movl	$1023, %edx
	movq	%r12, %rsi
	call	uloc_getParent_67@PLT
	movq	%r15, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
.L1786:
	cmpb	$0, -3136(%rbp)
	je	.L1739
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jg	.L1739
.L1740:
	movq	-7672(%rbp), %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$0, -7656(%rbp)
	call	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	movq	%rax, %r15
	movl	-7656(%rbp), %eax
	testl	%eax, %eax
	jg	.L1843
	jne	.L1730
	movq	-7696(%rbp), %rdx
	movq	-7688(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	ures_getByKey_67
	movl	-7656(%rbp), %eax
	testl	%eax, %eax
	jne	.L1730
	movq	-7728(%rbp), %rdx
	movq	-7736(%rbp), %rsi
	movq	%r13, %rcx
	movq	-7696(%rbp), %rdi
	call	ures_getByKey_67
	movl	-7656(%rbp), %eax
	testl	%eax, %eax
	je	.L1731
	movzbl	-1088(%rbp), %r9d
.L1732:
	movl	$1024, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	%r9b, -7752(%rbp)
	movl	$0, -7656(%rbp)
	call	__strcpy_chk@PLT
	movq	%r13, %rcx
	movl	$1023, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	uloc_getParent_67@PLT
	movq	%r15, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movzbl	-7752(%rbp), %r9d
	testb	%r9b, %r9b
	je	.L1786
.L1738:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1752
	cmpb	$0, -7720(%rbp)
	je	.L1844
	leaq	-5184(%rbp), %rsi
	movq	%rsi, %rdx
.L1753:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L1753
	movl	%eax, %ecx
	leaq	-1088(%rbp), %r12
	shrl	$16, %ecx
	testl	$32896, %eax
	movq	%r12, %r15
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	subq	%rsi, %rdx
.L1755:
	movl	(%r15), %ecx
	addq	$4, %r15
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L1755
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%r15), %rcx
	cmove	%rcx, %r15
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %r15
	subq	%r12, %r15
	cmpq	%r15, %rdx
	ja	.L1757
	movq	-7736(%rbp), %rdi
	leaq	-6208(%rbp), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L1757
	leaq	1(%r15), %rdx
	movl	$1024, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	$0, -7232(%rbp)
	call	__memcpy_chk@PLT
	.p2align 4,,10
	.p2align 3
.L1752:
	movq	-7696(%rbp), %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	-7728(%rbp), %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L1845
	movq	-7712(%rbp), %rax
	xorl	%r12d, %r12d
	movb	$0, (%rax)
.L1769:
	movl	-7716(%rbp), %esi
	movq	-7712(%rbp), %rdi
	movq	%rbx, %rcx
	movl	%r12d, %edx
	call	u_terminateChars_67@PLT
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1841:
	movb	$0, -7232(%rbp)
	jmp	.L1713
	.p2align 4,,10
	.p2align 3
.L1847:
	movq	%r12, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1846
.L1718:
	movq	%r15, %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	uenum_next_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L1847
	xorl	%eax, %eax
.L1717:
	movq	-7680(%rbp), %rdx
	movb	%al, (%rdx)
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1715:
	movq	-7680(%rbp), %rax
	xorl	%r15d, %r15d
	movb	$1, (%rax)
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1842:
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1739:
	leaq	-6208(%rbp), %rax
	movq	-7736(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -7680(%rbp)
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L1771
	movl	(%rbx), %eax
.L1750:
	testl	%eax, %eax
	jg	.L1752
	movl	$2, (%rbx)
	leaq	-1088(%rbp), %rsi
	movl	$1024, %edx
	movq	%r14, %rdi
	call	__strcpy_chk@PLT
	cmpb	$0, -7232(%rbp)
	jne	.L1759
	cmpb	$0, -7720(%rbp)
	jne	.L1752
.L1770:
	movq	%r14, %rdx
.L1763:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L1763
	movl	%eax, %ecx
	movq	%r14, %r12
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	$64, %ecx
	movl	%eax, %esi
	addb	%al, %sil
	movq	-7704(%rbp), %rsi
	sbbq	$3, %rdx
	leaq	1(%rdx), %rdi
	movw	%cx, (%rdx)
	movq	%r14, %rdx
	subq	%rdi, %rdx
	addq	$1024, %rdx
	call	__stpcpy_chk@PLT
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	subq	%rax, %r12
	movq	%rax, %rdi
	leaq	1024(%r12), %rcx
	call	__memcpy_chk@PLT
	movq	-7680(%rbp), %rsi
	leaq	1023(%r12), %rdx
	movq	%rax, %rdi
	addq	$1, %rdi
	call	__strcpy_chk@PLT
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1845:
	movq	%r14, %r13
.L1766:
	movl	0(%r13), %edx
	addq	$4, %r13
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L1766
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r13), %rdx
	cmove	%rdx, %r13
	movl	%eax, %esi
	addb	%al, %sil
	movl	-7716(%rbp), %esi
	sbbq	$3, %r13
	subq	%r14, %r13
	movl	%r13d, %edi
	movl	%r13d, %r12d
	call	uprv_min_67@PLT
	testl	%eax, %eax
	jg	.L1848
.L1768:
	testq	%r13, %r13
	jne	.L1769
	movl	$2, (%rbx)
	xorl	%r12d, %r12d
	jmp	.L1769
	.p2align 4,,10
	.p2align 3
.L1771:
	movq	-7680(%rbp), %rsi
	movq	-7736(%rbp), %rdi
	movl	$1024, %edx
	call	__strcpy_chk@PLT
	movq	-7744(%rbp), %r15
	movl	$1024, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	__strcpy_chk@PLT
	movl	$1024, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	__strcpy_chk@PLT
	leaq	-1088(%rbp), %rax
	movq	%rax, -7744(%rbp)
	jmp	.L1751
	.p2align 4,,10
	.p2align 3
.L1849:
	movl	%eax, (%rbx)
.L1742:
	movl	$1024, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$0, -7656(%rbp)
	call	__strcpy_chk@PLT
	movq	%r14, %rdi
	movq	%r13, %rcx
	movl	$1023, %edx
	movq	%r12, %rsi
	call	uloc_getParent_67@PLT
	movq	%r15, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
.L1785:
	cmpb	$0, -3136(%rbp)
	movl	(%rbx), %eax
	je	.L1750
	testl	%eax, %eax
	jg	.L1752
.L1751:
	movq	-7672(%rbp), %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$0, -7656(%rbp)
	call	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	movq	%rax, %r15
	movl	-7656(%rbp), %eax
	testl	%eax, %eax
	jg	.L1849
	jne	.L1742
	movq	-7696(%rbp), %rdx
	movq	-7688(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	ures_getByKey_67
	movl	-7656(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L1742
	movq	-7728(%rbp), %rdx
	movq	-7736(%rbp), %rsi
	movq	%r13, %rcx
	movq	-7696(%rbp), %rdi
	call	ures_getByKey_67
	movl	-7656(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L1743
	movzbl	-1088(%rbp), %r9d
.L1744:
	movl	$1024, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	%r9b, -7752(%rbp)
	movl	$0, -7656(%rbp)
	call	__strcpy_chk@PLT
	movq	%r13, %rcx
	movl	$1023, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	uloc_getParent_67@PLT
	movq	%r15, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movzbl	-7752(%rbp), %r9d
	testb	%r9b, %r9b
	jne	.L1738
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1846:
	movl	$1, %eax
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	-7680(%rbp), %rdi
	movl	$1024, %edx
	movq	%r12, %rsi
	call	__stpcpy_chk@PLT
	movzbl	-1088(%rbp), %r9d
	leaq	-5184(%rbp), %r10
	movq	%r10, %rcx
	testb	%r9b, %r9b
	je	.L1850
.L1781:
	movl	(%rcx), %esi
	addq	$4, %rcx
	leal	-16843009(%rsi), %edx
	notl	%esi
	andl	%esi, %edx
	andl	$-2139062144, %edx
	je	.L1781
	movl	%edx, %esi
	shrl	$16, %esi
	testl	$32896, %edx
	cmove	%esi, %edx
	leaq	2(%rcx), %rsi
	cmove	%rsi, %rcx
	movl	%edx, %esi
	addb	%dl, %sil
	sbbq	$3, %rcx
	subq	-7680(%rbp), %rax
	subq	%r10, %rcx
	cmpq	%rax, %rcx
	ja	.L1783
.L1736:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$1024, %edx
	movl	$0, -7656(%rbp)
	call	__strcpy_chk@PLT
	movq	%r14, %rdi
	movq	%r13, %rcx
	movl	$1023, %edx
	movq	%r12, %rsi
	call	uloc_getParent_67@PLT
	movq	%r15, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L1757:
	leaq	1(%r15), %rdx
	movl	$1024, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movzbl	-7232(%rbp), %r13d
	call	__memcpy_chk@PLT
	testb	%r13b, %r13b
	je	.L1752
.L1759:
	movq	%r14, %rdi
.L1761:
	movl	(%rdi), %edx
	addq	$4, %rdi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L1761
	movl	%eax, %edx
	movq	%r14, %r12
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rdi), %rdx
	cmove	%rdx, %rdi
	movq	%r14, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	movl	$64, %esi
	sbbq	$3, %rdi
	movw	%si, (%rdi)
	addq	$1, %rdi
	movq	-7704(%rbp), %rsi
	subq	%rdi, %rdx
	addq	$1024, %rdx
	call	__stpcpy_chk@PLT
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	subq	%rax, %r12
	movq	%rax, %rdi
	leaq	1024(%r12), %rcx
	call	__memcpy_chk@PLT
	movq	-7736(%rbp), %rsi
	leaq	1023(%r12), %rdx
	movq	%rax, %rdi
	addq	$1, %rdi
	call	__strcpy_chk@PLT
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1848:
	movq	-7712(%rbp), %rdi
	movslq	%eax, %rdx
	movq	%r14, %rsi
	call	strncpy@PLT
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1844:
	leaq	-1088(%rbp), %rsi
	movl	$1024, %edx
	movq	%r14, %rdi
	call	__strcpy_chk@PLT
	cmpb	$0, -7232(%rbp)
	jne	.L1759
	leaq	-6208(%rbp), %rax
	movq	%rax, -7680(%rbp)
	jmp	.L1770
.L1850:
	movq	-7680(%rbp), %rax
	movl	$114, %r9d
	movq	%r10, %rdx
	movl	$1953460082, (%rax)
	movb	$0, 4(%rax)
.L1734:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L1734
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rdx
	subq	%r10, %rdx
	cmpq	$4, %rdx
	jbe	.L1736
.L1783:
	movq	-7696(%rbp), %rdi
	leaq	-7652(%rbp), %rdx
	movq	%r13, %rcx
	leaq	.LC12(%rip), %rsi
	movb	%r9b, -7752(%rbp)
	movq	%r10, -7760(%rbp)
	call	ures_getStringByKey_67
	movzbl	-7752(%rbp), %r9d
	movq	%rax, %rdi
	movl	-7656(%rbp), %eax
	testl	%eax, %eax
	jg	.L1732
	movl	-7652(%rbp), %eax
	movq	-7760(%rbp), %r10
	testl	%eax, %eax
	je	.L1732
	movb	%r9b, -7760(%rbp)
	movq	%r10, -7768(%rbp)
	movq	%rdi, -7752(%rbp)
	call	u_strlen_67@PLT
	movq	-7752(%rbp), %rdi
	leaq	-6208(%rbp), %rsi
	movl	%eax, %edx
	call	u_UCharsToChars_67@PLT
	movq	-7768(%rbp), %r10
	movl	$1024, %edx
	movq	-7680(%rbp), %rsi
	movq	%r10, %rdi
	call	__strcpy_chk@PLT
	movzbl	-7760(%rbp), %r9d
	jmp	.L1732
.L1743:
	movq	-7744(%rbp), %rdi
	movl	$1024, %edx
	movq	%r12, %rsi
	call	__stpcpy_chk@PLT
	movzbl	-1088(%rbp), %r9d
	leaq	-5184(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdx
	testb	%r9b, %r9b
	je	.L1851
.L1777:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L1777
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	movq	%rsi, %rax
	sbbq	$3, %rdx
	subq	-7744(%rbp), %rax
	subq	%r10, %rdx
	cmpq	%rax, %rdx
	jbe	.L1736
.L1779:
	movq	-7696(%rbp), %rdi
	leaq	-7652(%rbp), %rdx
	movq	%r13, %rcx
	leaq	.LC12(%rip), %rsi
	movb	%r9b, -7752(%rbp)
	movq	%r10, -7760(%rbp)
	call	ures_getStringByKey_67
	movl	-7656(%rbp), %r8d
	movzbl	-7752(%rbp), %r9d
	movq	%rax, %rdi
	testl	%r8d, %r8d
	jg	.L1744
	cmpl	$0, -7652(%rbp)
	movq	-7760(%rbp), %r10
	je	.L1744
	movb	%r9b, -7760(%rbp)
	movq	%r10, -7768(%rbp)
	movq	%rax, -7752(%rbp)
	call	u_strlen_67@PLT
	movq	-7680(%rbp), %rsi
	movq	-7752(%rbp), %rdi
	movl	%eax, %edx
	call	u_UCharsToChars_67@PLT
	movq	-7768(%rbp), %r10
	movl	$1024, %edx
	movq	-7744(%rbp), %rsi
	movq	%r10, %rdi
	call	__strcpy_chk@PLT
	movzbl	-7760(%rbp), %r9d
	jmp	.L1744
.L1851:
	movq	-7744(%rbp), %rax
	movl	$114, %r9d
	movl	$1953460082, (%rax)
	movb	$0, 4(%rax)
.L1746:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L1746
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rdx
	subq	%r10, %rdx
	cmpq	$4, %rdx
	jbe	.L1736
	jmp	.L1779
.L1840:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3237:
	.size	ures_getFunctionalEquivalent_67, .-ures_getFunctionalEquivalent_67
	.section	.rodata.str1.1
.LC14:
	.string	"private-"
	.text
	.p2align 4
	.globl	ures_getKeywordValues_67
	.type	ures_getKeywordValues_67, @function
ures_getKeywordValues_67:
.LFB3238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$2584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$25, %ecx
	movl	(%rdx), %r11d
	movq	%rdi, -6664(%rbp)
	leaq	-6624(%rbp), %r15
	movq	%rsi, -6680(%rbp)
	movq	%r15, %rdi
	movq	%rdx, -6648(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	rep stosq
	leaq	-6416(%rbp), %rdi
	movl	$25, %ecx
	movq	%rdi, -6688(%rbp)
	rep stosq
	testl	%r11d, %r11d
	jg	.L1855
	movq	-6664(%rbp), %rdi
	movq	%rdx, %rbx
	movq	%rdx, %rsi
	call	ures_openAvailableLocales_67.part.0
	movl	(%rbx), %r10d
	movq	%rax, -6656(%rbp)
	testl	%r10d, %r10d
	jg	.L1855
	leaq	-6640(%rbp), %rax
	xorl	%r9d, %r9d
	movl	$0, -6692(%rbp)
	movq	%rax, -6672(%rbp)
	leaq	-2112(%rbp), %rax
	movw	%r9w, -2112(%rbp)
	movl	$0, -6696(%rbp)
	movq	%rax, -6712(%rbp)
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1871:
	movq	%r12, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
.L1856:
	movq	-6672(%rbp), %rsi
	movq	-6648(%rbp), %rdx
	movq	-6656(%rbp), %rdi
	call	uenum_next_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1857
	movq	-6664(%rbp), %rdi
	movl	$2, %edx
	leaq	-6636(%rbp), %r13
	movl	$0, -6636(%rbp)
	movq	%r13, %rcx
	call	_ZL17ures_openWithTypeP15UResourceBundlePKcS2_12UResOpenTypeP10UErrorCode.constprop.0
	movq	-6680(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rax, %r12
	movq	%rax, %rdi
	call	ures_getByKey_67
	testq	%r12, %r12
	je	.L1871
	movl	-6636(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L1871
	.p2align 4,,10
	.p2align 3
.L1859:
	movl	-6432(%rbp), %eax
	movl	-6436(%rbp), %edx
	movq	$0, -6632(%rbp)
	subl	$1, %eax
	cmpl	%eax, %edx
	je	.L1907
	movl	-6452(%rbp), %esi
	addl	$1, %edx
	movl	%edx, -6436(%rbp)
	movl	%esi, %eax
	shrl	$28, %eax
	cmpl	$-268435457, %esi
	ja	.L1879
	leaq	.L1865(%rip), %rbx
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1865:
	.long	.L1864-.L1865
	.long	.L1864-.L1865
	.long	.L1867-.L1865
	.long	.L1879-.L1865
	.long	.L1867-.L1865
	.long	.L1867-.L1865
	.long	.L1864-.L1865
	.long	.L1864-.L1865
	.long	.L1866-.L1865
	.long	.L1866-.L1865
	.long	.L1879-.L1865
	.long	.L1879-.L1865
	.long	.L1879-.L1865
	.long	.L1879-.L1865
	.long	.L1864-.L1865
	.text
	.p2align 4,,10
	.p2align 3
.L1864:
	movq	-6688(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	ures_copyResb_67.part.0
.L1863:
	testq	%rax, %rax
	je	.L1871
	movl	-6636(%rbp), %edx
	testl	%edx, %edx
	jg	.L1871
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.L1859
	cmpb	$0, (%r14)
	je	.L1859
	movl	$8, %ecx
	movq	%r14, %rsi
	leaq	.LC12(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1859
	movl	$8, %ecx
	movq	%r14, %rsi
	leaq	.LC14(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1859
	movl	-6692(%rbp), %eax
	testl	%eax, %eax
	je	.L1873
	subl	$1, %eax
	leaq	-6208(%rbp), %rbx
	movq	%r12, -6704(%rbp)
	leaq	-6200(%rbp,%rax,8), %rax
	movq	%rbx, %r12
	movq	%rax, %rbx
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1909:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L1908
.L1875:
	movq	(%r12), %rdi
	movq	%r14, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L1909
	movq	-6704(%rbp), %r12
	jmp	.L1859
	.p2align 4,,10
	.p2align 3
.L1867:
	leaq	-6584(%rbp), %r14
	leaq	-6632(%rbp), %rcx
	movq	%r14, %rdi
	call	res_getTableItemByIndex_67@PLT
	movl	-6636(%rbp), %edi
	movl	%eax, %esi
	testl	%edi, %edi
	jg	.L1871
.L1906:
	subq	$8, %rsp
	movl	-6436(%rbp), %ecx
	movq	%r15, %r9
	movq	%r14, %rdi
	movq	-6616(%rbp), %r8
	movq	-6632(%rbp), %rdx
	pushq	%r13
	pushq	-6688(%rbp)
	pushq	$0
	call	_ZL16init_resb_resultPK12ResourceDatajPKciP18UResourceDataEntryPK15UResourceBundleiPS6_P10UErrorCode.part.0
	addq	$32, %rsp
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	-6688(%rbp), %rax
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1866:
	leaq	-6584(%rbp), %r14
	movq	%r14, %rdi
	call	res_getArrayItem_67@PLT
	movl	-6636(%rbp), %ecx
	movl	%eax, %esi
	testl	%ecx, %ecx
	jle	.L1906
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1907:
	movl	$8, -6636(%rbp)
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1908:
	cmpl	$510, -6692(%rbp)
	movq	-6704(%rbp), %r12
	jle	.L1873
.L1877:
	movq	-6648(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L1859
	.p2align 4,,10
	.p2align 3
.L1873:
	movq	%r14, %rdi
	call	strlen@PLT
	movl	-6696(%rbp), %ecx
	leal	(%rcx,%rax), %ebx
	cmpl	$2045, %ebx
	jg	.L1877
	movslq	-6696(%rbp), %rcx
	leaq	1(%rax), %rdx
	movq	%r14, %rsi
	addq	-6712(%rbp), %rcx
	movq	%rcx, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
	movslq	-6692(%rbp), %rax
	movq	%rax, %rdx
	movq	%rcx, -6208(%rbp,%rax,8)
	leal	1(%rbx), %eax
	movslq	%ebx, %rbx
	movl	%eax, -6696(%rbp)
	leal	1(%rdx), %eax
	movb	$0, -2112(%rbp,%rbx)
	movl	%eax, -6692(%rbp)
	jmp	.L1859
	.p2align 4,,10
	.p2align 3
.L1857:
	movslq	-6696(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, %rbx
	movb	$0, -2112(%rbp,%rax)
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	-6688(%rbp), %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	-6656(%rbp), %rdi
	call	uenum_close_67@PLT
	movq	-6648(%rbp), %rdx
	movq	-6712(%rbp), %rdi
	leal	1(%rbx), %esi
	call	uloc_openKeywordList_67@PLT
.L1852:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1910
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1855:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	movq	-6688(%rbp), %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
	xorl	%eax, %eax
	jmp	.L1852
.L1910:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3238:
	.size	ures_getKeywordValues_67, .-ures_getKeywordValues_67
	.p2align 4
	.globl	ures_getVersionByKey_67
	.type	ures_getVersionByKey_67, @function
ures_getVersionByKey_67:
.LFB3239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$-1, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testl	%r8d, %r8d
	jg	.L1911
	movq	%rdi, %r12
	movq	%rcx, %rbx
	testq	%rdi, %rdi
	je	.L1946
	movq	%rsi, %r14
	movl	172(%rdi), %esi
	movq	%rdx, %r13
	movl	%esi, %eax
	shrl	$28, %eax
	leal	-4(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1933
	cmpl	$2, %eax
	jne	.L1915
.L1933:
	leaq	-64(%rbp), %r15
	leaq	40(%r12), %rdi
	movl	$0, -76(%rbp)
	leaq	-76(%rbp), %rdx
	movq	%r15, %rcx
	movq	%rdi, -104(%rbp)
	call	res_getTableItemByKey_67@PLT
	movq	-104(%rbp), %rdi
	cmpl	$-1, %eax
	movl	%eax, -80(%rbp)
	movl	%eax, %esi
	je	.L1947
	shrl	$28, %eax
	cmpl	$3, %eax
	je	.L1925
	cmpl	$6, %eax
	je	.L1926
	testl	%eax, %eax
	je	.L1926
.L1915:
	movl	$17, (%rbx)
.L1911:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1948
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1925:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rbx, %rcx
	call	ures_getByKey_67
	movl	(%rbx), %edx
	xorl	%r12d, %r12d
	movq	%rax, %r14
	testl	%edx, %edx
	jg	.L1927
	testq	%rax, %rax
	je	.L1949
	movl	172(%rax), %esi
	leaq	-84(%rbp), %rdx
	leaq	40(%rax), %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1950
.L1927:
	movq	%r14, %rdi
	call	_ZL16ures_closeBundleP15UResourceBundlea.constprop.0
.L1922:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L1911
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	u_versionFromUString_67@PLT
	jmp	.L1911
	.p2align 4,,10
	.p2align 3
.L1947:
	cmpb	$1, 176(%r12)
	movq	%r14, -64(%rbp)
	je	.L1951
.L1918:
	movl	$2, (%rbx)
	jmp	.L1911
	.p2align 4,,10
	.p2align 3
.L1926:
	leaq	-84(%rbp), %rdx
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r12
	jmp	.L1922
	.p2align 4,,10
	.p2align 3
.L1951:
	movq	8(%r12), %rdi
	leaq	-72(%rbp), %rcx
	movq	%rbx, %r9
	leaq	-80(%rbp), %r8
	leaq	176(%r12), %rsi
	movq	%r15, %rdx
	call	_ZL15getFallbackDataPK15UResourceBundlePPKcPP18UResourceDataEntryPjP10UErrorCode.isra.0
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1918
	movl	-80(%rbp), %esi
	movl	%esi, %edx
	shrl	$28, %edx
	cmpl	$3, %edx
	je	.L1925
	cmpl	$6, %edx
	je	.L1920
	testl	%edx, %edx
	jne	.L1915
.L1920:
	leaq	-84(%rbp), %rdx
	movq	%rax, %rdi
	call	res_getStringNoTrace_67@PLT
	movq	%rax, %r12
	jmp	.L1922
	.p2align 4,,10
	.p2align 3
.L1946:
	movl	$1, (%rcx)
	jmp	.L1911
	.p2align 4,,10
	.p2align 3
.L1950:
	movl	$17, (%rbx)
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L1949:
	movl	$1, (%rbx)
	movq	%rax, %r12
	jmp	.L1927
.L1948:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3239:
	.size	ures_getVersionByKey_67, .-ures_getVersionByKey_67
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL12gLocalesEnum, @object
	.size	_ZL12gLocalesEnum, 56
_ZL12gLocalesEnum:
	.quad	0
	.quad	0
	.quad	_ZL21ures_loc_closeLocalesP12UEnumeration
	.quad	_ZL21ures_loc_countLocalesP12UEnumerationP10UErrorCode
	.quad	uenum_unextDefault_67
	.quad	ures_loc_nextLocale
	.quad	ures_loc_resetLocales
	.local	_ZL9resbMutex
	.comm	_ZL9resbMutex,56,32
	.local	_ZL14gCacheInitOnce
	.comm	_ZL14gCacheInitOnce,8,8
	.local	_ZL5cache
	.comm	_ZL5cache,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
