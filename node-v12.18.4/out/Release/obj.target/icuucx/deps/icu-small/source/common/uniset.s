	.file	"uniset.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet17getDynamicClassIDEv
	.type	_ZNK6icu_6710UnicodeSet17getDynamicClassIDEv, @function
_ZNK6icu_6710UnicodeSet17getDynamicClassIDEv:
.LFB2383:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6710UnicodeSet16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2383:
	.size	_ZNK6icu_6710UnicodeSet17getDynamicClassIDEv, .-_ZNK6icu_6710UnicodeSet17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet8hashCodeEv
	.type	_ZNK6icu_6710UnicodeSet8hashCodeEv, @function
_ZNK6icu_6710UnicodeSet8hashCodeEv:
.LFB2429:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	testl	%eax, %eax
	jle	.L3
	movq	16(%rdi), %rcx
	leal	-1(%rax), %esi
	leaq	4(%rcx), %rdx
	leaq	(%rdx,%rsi,4), %rsi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$4, %rdx
.L5:
	imull	$1000003, %eax, %eax
	addl	(%rcx), %eax
	movq	%rdx, %rcx
	cmpq	%rdx, %rsi
	jne	.L7
.L3:
	ret
	.cfi_endproc
.LFE2429:
	.size	_ZNK6icu_6710UnicodeSet8hashCodeEv, .-_ZNK6icu_6710UnicodeSet8hashCodeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet4sizeEv
	.type	_ZNK6icu_6710UnicodeSet4sizeEv, @function
_ZNK6icu_6710UnicodeSet4sizeEv:
.LFB2430:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	cmpl	$1, %eax
	jle	.L14
	movq	16(%rdi), %rsi
	cmpl	$7, %eax
	jle	.L15
	movl	%ecx, %edx
	pxor	%xmm1, %xmm1
	pcmpeqd	%xmm3, %xmm3
	movq	%rsi, %rax
	shrl	$2, %edx
	movdqa	.LC0(%rip), %xmm4
	salq	$5, %rdx
	addq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L11:
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm5
	addq	$32, %rax
	movdqa	%xmm2, %xmm0
	shufps	$136, %xmm5, %xmm2
	shufps	$221, %xmm5, %xmm0
	paddd	%xmm3, %xmm0
	psubd	%xmm2, %xmm0
	paddd	%xmm4, %xmm0
	paddd	%xmm0, %xmm1
	cmpq	%rdx, %rax
	jne	.L11
	movdqa	%xmm1, %xmm0
	movl	%ecx, %edx
	psrldq	$8, %xmm0
	andl	$-4, %edx
	paddd	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	psrldq	$4, %xmm0
	paddd	%xmm0, %xmm1
	movd	%xmm1, %eax
	testb	$3, %cl
	je	.L9
.L10:
	leal	(%rdx,%rdx), %r8d
	movslq	%r8d, %r8
	addl	4(%rsi,%r8,4), %eax
	subl	(%rsi,%r8,4), %eax
	leal	1(%rdx), %r8d
	cmpl	%r8d, %ecx
	jle	.L9
	addl	%r8d, %r8d
	addl	$2, %edx
	movslq	%r8d, %r8
	addl	4(%rsi,%r8,4), %eax
	subl	(%rsi,%r8,4), %eax
	cmpl	%edx, %ecx
	jle	.L9
	addl	%edx, %edx
	movslq	%edx, %rdx
	addl	4(%rsi,%rdx,4), %eax
	subl	(%rsi,%rdx,4), %eax
.L9:
	movq	80(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L8
	addl	8(%rdx), %eax
.L8:
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
	jmp	.L9
.L15:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L10
	.cfi_endproc
.LFE2430:
	.size	_ZNK6icu_6710UnicodeSet4sizeEv, .-_ZNK6icu_6710UnicodeSet4sizeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet7isEmptyEv
	.type	_ZNK6icu_6710UnicodeSet7isEmptyEv, @function
_ZNK6icu_6710UnicodeSet7isEmptyEv:
.LFB2431:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$1, 28(%rdi)
	je	.L25
.L20:
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	80(%rdi), %rdx
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L20
	movl	8(%rdx), %eax
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE2431:
	.size	_ZNK6icu_6710UnicodeSet7isEmptyEv, .-_ZNK6icu_6710UnicodeSet7isEmptyEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet13getRangeCountEv
	.type	_ZNK6icu_6710UnicodeSet13getRangeCountEv, @function
_ZNK6icu_6710UnicodeSet13getRangeCountEv:
.LFB2474:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	ret
	.cfi_endproc
.LFE2474:
	.size	_ZNK6icu_6710UnicodeSet13getRangeCountEv, .-_ZNK6icu_6710UnicodeSet13getRangeCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet13getRangeStartEi
	.type	_ZNK6icu_6710UnicodeSet13getRangeStartEi, @function
_ZNK6icu_6710UnicodeSet13getRangeStartEi:
.LFB2475:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	addl	%esi, %esi
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %eax
	ret
	.cfi_endproc
.LFE2475:
	.size	_ZNK6icu_6710UnicodeSet13getRangeStartEi, .-_ZNK6icu_6710UnicodeSet13getRangeStartEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet11getRangeEndEi
	.type	_ZNK6icu_6710UnicodeSet11getRangeEndEi, @function
_ZNK6icu_6710UnicodeSet11getRangeEndEi:
.LFB2476:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	addl	%esi, %esi
	movslq	%esi, %rsi
	movl	4(%rax,%rsi,4), %eax
	subl	$1, %eax
	ret
	.cfi_endproc
.LFE2476:
	.size	_ZNK6icu_6710UnicodeSet11getRangeEndEi, .-_ZNK6icu_6710UnicodeSet11getRangeEndEi
	.p2align 4
	.type	_ZN6icu_67L18cloneUnicodeStringEP8UElementS1_, @function
_ZN6icu_67L18cloneUnicodeStringEP8UElementS1_:
.LFB2385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$64, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L30
	movq	0(%r13), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L30:
	movq	%rbx, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2385:
	.size	_ZN6icu_67L18cloneUnicodeStringEP8UElementS1_, .-_ZN6icu_67L18cloneUnicodeStringEP8UElementS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetD2Ev
	.type	_ZN6icu_6710UnicodeSetD2Ev, @function
_ZN6icu_6710UnicodeSetD2Ev:
.LFB2421:
	.cfi_startproc
	endbr64
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-280(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movups	%xmm0, (%rdi)
	movq	16(%rdi), %rdi
	leaq	96(%r12), %r13
	cmpq	%r13, %rdi
	je	.L36
	call	uprv_free_67@PLT
.L36:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	call	*8(%rax)
.L37:
	movq	48(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L38
	call	uprv_free_67@PLT
.L38:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L39
	movq	(%rdi), %rax
	call	*8(%rax)
.L39:
	movq	88(%r12), %r13
	testq	%r13, %r13
	je	.L40
	movq	%r13, %rdi
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L40:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
.L41:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeFilterD2Ev@PLT
	.cfi_endproc
.LFE2421:
	.size	_ZN6icu_6710UnicodeSetD2Ev, .-_ZN6icu_6710UnicodeSetD2Ev
	.globl	_ZN6icu_6710UnicodeSetD1Ev
	.set	_ZN6icu_6710UnicodeSetD1Ev,_ZN6icu_6710UnicodeSetD2Ev
	.p2align 4
	.type	_ZN6icu_67L20compareUnicodeStringE8UElementS0_, @function
_ZN6icu_67L20compareUnicodeStringE8UElementS0_:
.LFB2386:
	.cfi_startproc
	endbr64
	movzwl	8(%rsi), %r10d
	testw	%r10w, %r10w
	js	.L56
	movswl	%r10w, %ecx
	sarl	$5, %ecx
.L57:
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L58
	movswl	%ax, %edx
	sarl	$5, %edx
.L59:
	testb	$1, %r10b
	je	.L60
	notl	%eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L62
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L62:
	andl	$2, %r10d
	leaq	10(%rsi), %rcx
	jne	.L64
	movq	24(%rsi), %rcx
.L64:
	xorl	%esi, %esi
	jmp	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	movl	12(%rdi), %edx
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L56:
	movl	12(%rsi), %ecx
	jmp	.L57
	.cfi_endproc
.LFE2386:
	.size	_ZN6icu_67L20compareUnicodeStringE8UElementS0_, .-_ZN6icu_67L20compareUnicodeStringE8UElementS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet7compactEv
	.type	_ZN6icu_6710UnicodeSet7compactEv, @function
_ZN6icu_6710UnicodeSet7compactEv:
.LFB2478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpq	$0, 40(%rdi)
	je	.L83
.L69:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L69
	testb	$1, 32(%rdi)
	jne	.L69
	movq	48(%rdi), %rdi
	leaq	96(%r12), %rbx
	cmpq	%rbx, %rdi
	je	.L71
	call	uprv_free_67@PLT
	movq	$0, 48(%r12)
	movl	$0, 56(%r12)
.L71:
	movq	16(%r12), %r13
	cmpq	%r13, %rbx
	je	.L73
	movslq	28(%r12), %rax
	cmpl	$25, %eax
	jle	.L84
	leal	7(%rax), %edx
	cmpl	24(%r12), %edx
	jge	.L73
	leaq	0(,%rax,4), %rsi
	movq	%r13, %rdi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L73
	movq	%rax, 16(%r12)
	movl	28(%r12), %eax
	movl	%eax, 24(%r12)
	.p2align 4,,10
	.p2align 3
.L73:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L69
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jne	.L69
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	$0, 80(%r12)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	0(,%rax,4), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movq	%rbx, 16(%r12)
	movl	$25, 24(%r12)
	jmp	.L73
	.cfi_endproc
.LFE2478:
	.size	_ZN6icu_6710UnicodeSet7compactEv, .-_ZN6icu_6710UnicodeSet7compactEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSeteqERKS0_
	.type	_ZNK6icu_6710UnicodeSeteqERKS0_, @function
_ZNK6icu_6710UnicodeSeteqERKS0_:
.LFB2428:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	xorl	%r8d, %r8d
	cmpl	28(%rsi), %eax
	jne	.L104
	testl	%eax, %eax
	jle	.L87
	leal	-1(%rax), %r9d
	movq	16(%rdi), %r8
	movq	16(%rsi), %rcx
	xorl	%eax, %eax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	1(%rax), %rdx
	cmpq	%rax, %r9
	je	.L87
	movq	%rdx, %rax
.L88:
	movl	(%rcx,%rax,4), %edx
	cmpl	%edx, (%r8,%rax,4)
	je	.L107
	xorl	%r8d, %r8d
.L104:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movq	80(%rdi), %rdi
	movq	80(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L89
	movl	8(%rdi), %eax
	testl	%eax, %eax
	setne	%dl
	testq	%rsi, %rsi
	je	.L108
	movl	8(%rsi), %eax
	testl	%eax, %eax
	setne	%al
	xorl	%r8d, %r8d
	cmpb	%al, %dl
	jne	.L104
.L93:
	movl	8(%rdi), %edx
	movl	$1, %r8d
	testl	%edx, %edx
	je	.L104
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN6icu_677UVectoreqERKS0_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testb	%al, %al
	setne	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore 6
	movl	$1, %r8d
	testq	%rsi, %rsi
	je	.L104
	movl	8(%rsi), %ecx
	testl	%ecx, %ecx
	sete	%r8b
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L108:
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jne	.L104
	jmp	.L93
	.cfi_endproc
.LFE2428:
	.size	_ZNK6icu_6710UnicodeSeteqERKS0_, .-_ZNK6icu_6710UnicodeSeteqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet17matchesIndexValueEh
	.type	_ZNK6icu_6710UnicodeSet17matchesIndexValueEh, @function
_ZNK6icu_6710UnicodeSet17matchesIndexValueEh:
.LFB2441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movzbl	%sil, %ebx
	subq	$8, %rsp
	movl	28(%rdi), %eax
	movl	%eax, %r8d
	shrl	$31, %r8d
	addl	%eax, %r8d
	sarl	%r8d
	cmpl	$1, %eax
	jle	.L110
	movq	16(%rdi), %rsi
	xorl	%ecx, %ecx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L123:
	cmpl	%ebx, %edi
	jg	.L112
.L122:
	movzbl	%al, %eax
	cmpl	%ebx, %eax
	jge	.L114
.L112:
	addq	$1, %rcx
	cmpl	%ecx, %r8d
	jle	.L110
.L115:
	movl	4(%rsi,%rcx,8), %eax
	movl	(%rsi,%rcx,8), %edx
	subl	$1, %eax
	movzbl	%dl, %edi
	xorl	%eax, %edx
	andl	$-256, %edx
	je	.L123
	cmpl	%ebx, %edi
	jg	.L122
.L114:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L117
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L117
	xorl	%r13d, %r13d
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L124:
	movq	80(%r12), %rdi
	addl	$1, %r13d
	cmpl	%r13d, 8(%rdi)
	jle	.L117
.L118:
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movzbl	%al, %eax
	cmpl	%ebx, %eax
	jne	.L124
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L117:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2441:
	.size	_ZNK6icu_6710UnicodeSet17matchesIndexValueEh, .-_ZNK6icu_6710UnicodeSet17matchesIndexValueEh
	.set	.LTHUNK2,_ZNK6icu_6710UnicodeSet17matchesIndexValueEh
	.p2align 4
	.globl	_ZThn8_NK6icu_6710UnicodeSet17matchesIndexValueEh
	.type	_ZThn8_NK6icu_6710UnicodeSet17matchesIndexValueEh, @function
_ZThn8_NK6icu_6710UnicodeSet17matchesIndexValueEh:
.LFB3103:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK2
	.cfi_endproc
.LFE3103:
	.size	_ZThn8_NK6icu_6710UnicodeSet17matchesIndexValueEh, .-_ZThn8_NK6icu_6710UnicodeSet17matchesIndexValueEh
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet16removeAllStringsEv
	.type	_ZN6icu_6710UnicodeSet16removeAllStringsEv, @function
_ZN6icu_6710UnicodeSet16removeAllStringsEv:
.LFB2457:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	movq	%rdi, %rax
	je	.L139
.L136:
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	cmpq	$0, 88(%rdi)
	jne	.L136
	movq	80(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L136
	movl	8(%rdi), %edx
	testl	%edx, %edx
	je	.L136
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rax, -8(%rbp)
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movq	-8(%rbp), %rax
	movq	64(%rax), %rdi
	testq	%rdi, %rdi
	je	.L127
	call	uprv_free_67@PLT
	movq	-8(%rbp), %rax
	movq	$0, 64(%rax)
	movl	$0, 72(%rax)
.L127:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2457:
	.size	_ZN6icu_6710UnicodeSet16removeAllStringsEv, .-_ZN6icu_6710UnicodeSet16removeAllStringsEv
	.align 2
	.p2align 4
	.type	_ZN6icu_6710UnicodeSet5clearEv.part.0, @function
_ZN6icu_6710UnicodeSet5clearEv.part.0:
.LFB3080:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movl	$1114112, (%rax)
	movl	$1, 28(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L141
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L141:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
.L142:
	movb	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3080:
	.size	_ZN6icu_6710UnicodeSet5clearEv.part.0, .-_ZN6icu_6710UnicodeSet5clearEv.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia.part.0, @function
_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia.part.0:
.LFB3086:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movl	$1, %r11d
	leal	-1(%r8), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	movl	(%rsi), %r9d
	movl	(%r12), %edx
	cmpb	$1, %al
	jbe	.L164
.L151:
	movq	48(%rbx), %rsi
	movl	$1, %ecx
	movl	$1, %eax
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L152:
	movl	%eax, %r10d
	leaq	(%rsi,%r8,4), %rdi
	cmpl	%r9d, %edx
	jge	.L153
.L165:
	movl	%edx, (%rdi)
	movslq	%ecx, %rdx
	movslq	%r10d, %r8
	addl	$1, %eax
	movl	(%r12,%rdx,4), %edx
	addl	$1, %ecx
	movl	%eax, %r10d
	leaq	(%rsi,%r8,4), %rdi
	cmpl	%r9d, %edx
	jl	.L165
.L153:
	jle	.L155
	movl	%r9d, (%rdi)
	movslq	%r11d, %rdi
	addl	$1, %eax
	addl	$1, %r11d
	movl	0(%r13,%rdi,4), %r9d
	movslq	%r10d, %r8
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L155:
	cmpl	$1114112, %edx
	je	.L156
	movslq	%ecx, %rdx
	movslq	%r11d, %rdi
	addl	$1, %ecx
	addl	$1, %r11d
	movl	(%r12,%rdx,4), %edx
	movl	0(%r13,%rdi,4), %r9d
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L156:
	movl	$1114112, (%rdi)
	movq	64(%rbx), %rdi
	movl	56(%rbx), %edx
	movl	%eax, 28(%rbx)
	movl	24(%rbx), %eax
	movq	%rsi, 16(%rbx)
	movq	%r12, 48(%rbx)
	movl	%edx, 24(%rbx)
	movl	%eax, 56(%rbx)
	testq	%rdi, %rdi
	je	.L150
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L150:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	testl	%r9d, %r9d
	jne	.L159
	movl	4(%rsi), %r9d
	jmp	.L151
.L159:
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	jmp	.L151
	.cfi_endproc
.LFE3086:
	.size	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia.part.0, .-_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6710UnicodeSet3addEPKiia.part.0, @function
_ZN6icu_6710UnicodeSet3addEPKiia.part.0:
.LFB3089:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	xorl	%eax, %eax
	movl	$1, %r8d
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	movq	48(%rbx), %r11
	movl	(%rsi), %edi
	movl	$1, %esi
	movl	(%r12), %ecx
	.p2align 4,,10
	.p2align 3
.L212:
	cmpb	$2, %dl
	je	.L168
	jg	.L169
	testb	%dl, %dl
	je	.L170
	cmpb	$1, %dl
	jne	.L212
.L171:
	leaq	0(,%r9,4), %rdx
	cmpl	%edi, %ecx
	jge	.L187
.L216:
	movl	%ecx, (%r11,%rdx)
	movslq	%esi, %rdx
	addl	$1, %eax
	addl	$1, %esi
	movl	(%r12,%rdx,4), %ecx
	movslq	%eax, %r9
	.p2align 4,,10
	.p2align 3
.L170:
	salq	$2, %r9
	cmpl	%edi, %ecx
	jge	.L174
.L217:
	movslq	%esi, %rdx
	leaq	(%r12,%rdx,4), %rdx
	testl	%eax, %eax
	je	.L175
	cmpl	%ecx, -4(%r11,%r9)
	jl	.L175
	subl	$1, %eax
	movl	(%rdx), %ecx
	movslq	%eax, %r9
	cmpl	%ecx, (%r11,%r9,4)
	cmovge	(%r11,%r9,4), %ecx
.L176:
	addl	$1, %esi
	leaq	0(,%r9,4), %rdx
	cmpl	%edi, %ecx
	jl	.L216
.L187:
	jg	.L214
	cmpl	$1114112, %ecx
	je	.L215
	movslq	%esi, %rdx
	addl	$1, %esi
	movl	(%r12,%rdx,4), %ecx
	movslq	%r8d, %rdx
	addl	$1, %r8d
	movl	(%r10,%rdx,4), %edi
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	0(,%r9,4), %rdx
	cmpl	%edi, %ecx
	jle	.L190
.L218:
	movl	%edi, (%r11,%rdx)
	movslq	%r8d, %rdx
	addl	$1, %eax
	addl	$1, %r8d
	movl	(%r10,%rdx,4), %edi
	movslq	%eax, %r9
	salq	$2, %r9
	cmpl	%edi, %ecx
	jl	.L217
.L174:
	jle	.L178
	movslq	%r8d, %rdx
	leaq	(%r10,%rdx,4), %rdx
	testl	%eax, %eax
	je	.L179
	cmpl	%edi, -4(%r11,%r9)
	jl	.L179
	subl	$1, %eax
	movl	(%rdx), %edi
	movslq	%eax, %r9
	cmpl	%edi, (%r11,%r9,4)
	cmovge	(%r11,%r9,4), %edi
.L180:
	addl	$1, %r8d
	leaq	0(,%r9,4), %rdx
	cmpl	%edi, %ecx
	jg	.L218
.L190:
	jge	.L191
	movslq	%esi, %rdx
	addl	$1, %eax
	leaq	(%r11,%r9,4), %r9
	addl	$1, %esi
	movl	(%r12,%rdx,4), %ecx
	cmpl	%edi, %ecx
	jge	.L219
.L184:
	cmpl	$1114112, %edi
	je	.L185
	movl	%edi, (%r9)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L169:
	cmpb	$3, %dl
	jne	.L212
.L173:
	addl	$1, %eax
	leaq	(%r11,%r9,4), %r9
	cmpl	%edi, %ecx
	jl	.L184
.L219:
	cmpl	$1114112, %ecx
	je	.L185
	movl	%ecx, (%r9)
.L186:
	movslq	%esi, %rdx
	movslq	%eax, %r9
	addl	$1, %esi
	movl	(%r12,%rdx,4), %ecx
	movslq	%r8d, %rdx
	addl	$1, %r8d
	movl	(%r10,%rdx,4), %edi
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L178:
	cmpl	$1114112, %ecx
	je	.L181
	movslq	%esi, %rdx
	leaq	(%r12,%rdx,4), %rdx
	testl	%eax, %eax
	je	.L182
	cmpl	%ecx, -4(%r11,%r9)
	jl	.L182
	subl	$1, %eax
	movl	(%rdx), %ecx
	movslq	%eax, %r9
	cmpl	%ecx, (%r11,%r9,4)
	cmovge	(%r11,%r9,4), %ecx
.L183:
	addl	$1, %esi
.L214:
	movslq	%r8d, %rdx
	addl	$1, %r8d
	movl	(%r10,%rdx,4), %edi
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L175:
	addl	$1, %eax
	movl	%ecx, (%r11,%r9)
	movl	(%rdx), %ecx
	movslq	%eax, %r9
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L191:
	cmpl	$1114112, %ecx
	je	.L215
	movslq	%esi, %rdx
	addl	$1, %esi
	movl	(%r12,%rdx,4), %ecx
	movslq	%r8d, %rdx
	addl	$1, %r8d
	movl	(%r10,%rdx,4), %edi
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L179:
	addl	$1, %eax
	movl	%edi, (%r11,%r9)
	movl	(%rdx), %edi
	movslq	%eax, %r9
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L182:
	addl	$1, %eax
	movl	%ecx, (%r11,%r9)
	movl	(%rdx), %ecx
	movslq	%eax, %r9
	jmp	.L183
.L181:
	addl	$1, %eax
	addq	%r11, %r9
.L185:
	movl	$1114112, (%r9)
	movq	64(%rbx), %rdi
	movl	56(%rbx), %edx
	movl	%eax, 28(%rbx)
	movl	24(%rbx), %eax
	movq	%r11, 16(%rbx)
	movq	%r12, 48(%rbx)
	movl	%edx, 24(%rbx)
	movl	%eax, 56(%rbx)
	testq	%rdi, %rdi
	je	.L166
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L166:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L215:
	.cfi_restore_state
	addl	$1, %eax
	leaq	(%r11,%rdx), %r9
	jmp	.L185
	.cfi_endproc
.LFE3089:
	.size	_ZN6icu_6710UnicodeSet3addEPKiia.part.0, .-_ZN6icu_6710UnicodeSet3addEPKiia.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0, @function
_ZN6icu_6710UnicodeSet6retainEPKiia.part.0:
.LFB3091:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movl	$1, %r8d
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r11
	movl	(%rsi), %esi
	movq	48(%rdi), %r13
	movl	$1, %edi
	movl	(%r11), %eax
	.p2align 4,,10
	.p2align 3
.L254:
	cmpb	$2, %dl
	je	.L222
	jg	.L223
	testb	%dl, %dl
	je	.L224
	cmpb	$1, %dl
	jne	.L254
.L225:
	cmpl	%esi, %eax
	jge	.L234
.L256:
	movslq	%ecx, %rax
	addl	$1, %ecx
	movl	(%r11,%rax,4), %eax
	.p2align 4,,10
	.p2align 3
.L224:
	cmpl	%esi, %eax
	jge	.L228
.L257:
	movslq	%ecx, %rax
	addl	$1, %ecx
	movl	(%r11,%rax,4), %eax
	cmpl	%esi, %eax
	jl	.L256
.L234:
	movl	%r8d, %r10d
	leaq	0(%r13,%r9,4), %rdx
	jle	.L235
	movl	%esi, (%rdx)
	movslq	%edi, %rdx
	addl	$1, %r8d
	addl	$1, %edi
	movl	(%r12,%rdx,4), %esi
	movslq	%r10d, %r9
	.p2align 4,,10
	.p2align 3
.L227:
	movl	%r8d, %r10d
	leaq	0(%r13,%r9,4), %rdx
	cmpl	%esi, %eax
	jge	.L232
.L259:
	movl	%eax, (%rdx)
	movslq	%ecx, %rax
	addl	$1, %r8d
	addl	$1, %ecx
	movl	(%r11,%rax,4), %eax
	movslq	%r10d, %r9
.L222:
	cmpl	%esi, %eax
	jle	.L236
.L258:
	movslq	%edi, %rdx
	addl	$1, %edi
	movl	(%r12,%rdx,4), %esi
	cmpl	%esi, %eax
	jl	.L257
.L228:
	jle	.L230
	movslq	%edi, %rdx
	addl	$1, %edi
	movl	(%r12,%rdx,4), %esi
	cmpl	%esi, %eax
	jg	.L258
.L236:
	movl	%r8d, %r10d
	leaq	0(%r13,%r9,4), %rdx
	jge	.L237
	movl	%eax, (%rdx)
	movslq	%ecx, %rax
	movslq	%r10d, %r9
	addl	$1, %r8d
	movl	(%r11,%rax,4), %eax
	addl	$1, %ecx
	movl	%r8d, %r10d
	leaq	0(%r13,%r9,4), %rdx
	cmpl	%esi, %eax
	jl	.L259
.L232:
	jle	.L233
	movl	%esi, (%rdx)
	movslq	%edi, %rdx
	addl	$1, %r8d
	addl	$1, %edi
	movl	(%r12,%rdx,4), %esi
	movslq	%r10d, %r9
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L223:
	cmpb	$3, %dl
	je	.L227
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L235:
	cmpl	$1114112, %eax
	je	.L231
	movslq	%ecx, %rax
	movslq	%edi, %rdx
	addl	$1, %ecx
	addl	$1, %edi
	movl	(%r11,%rax,4), %eax
	movl	(%r12,%rdx,4), %esi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L237:
	cmpl	$1114112, %eax
	je	.L231
	movslq	%ecx, %rax
	movslq	%edi, %rdx
	addl	$1, %ecx
	addl	$1, %edi
	movl	(%r11,%rax,4), %eax
	movl	(%r12,%rdx,4), %esi
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L233:
	cmpl	$1114112, %eax
	je	.L231
	movl	%eax, (%rdx)
	movslq	%ecx, %rax
	movslq	%edi, %rdx
	addl	$1, %ecx
	movl	(%r11,%rax,4), %eax
	movl	(%r12,%rdx,4), %esi
	addl	$1, %edi
	addl	$1, %r8d
	movslq	%r10d, %r9
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L230:
	movl	%r8d, %r10d
	leaq	0(%r13,%r9,4), %rdx
	cmpl	$1114112, %eax
	je	.L231
	movl	%eax, (%rdx)
	movslq	%ecx, %rax
	movslq	%edi, %rdx
	addl	$1, %ecx
	movl	(%r11,%rax,4), %eax
	movl	(%r12,%rdx,4), %esi
	addl	$1, %edi
	addl	$1, %r8d
	movslq	%r10d, %r9
	jmp	.L227
.L231:
	movl	$1114112, (%rdx)
	movq	64(%rbx), %rdi
	movl	24(%rbx), %eax
	movl	56(%rbx), %edx
	movl	%r8d, 28(%rbx)
	movq	%r13, 16(%rbx)
	movq	%r11, 48(%rbx)
	movl	%edx, 24(%rbx)
	movl	%eax, 56(%rbx)
	testq	%rdi, %rdi
	je	.L220
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L220:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3091:
	.size	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0, .-_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet11containsAllERKS0_
	.type	_ZNK6icu_6710UnicodeSet11containsAllERKS0_, @function
_ZNK6icu_6710UnicodeSet11containsAllERKS0_:
.LFB2436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	28(%rsi), %eax
	movl	%eax, %r12d
	shrl	$31, %r12d
	addl	%eax, %r12d
	sarl	%r12d
	cmpl	$1, %eax
	jle	.L261
	movq	16(%rdi), %r10
	movq	16(%rsi), %rbx
	xorl	%ecx, %ecx
	movl	(%r10), %r13d
	.p2align 4,,10
	.p2align 3
.L266:
	movl	(%rbx,%rcx,8), %r8d
	cmpl	%r13d, %r8d
	jl	.L270
	movl	28(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L263
	movslq	%eax, %rdx
	cmpl	-4(%r10,%rdx,4), %r8d
	jge	.L263
	xorl	%r9d, %r9d
.L265:
	movl	%eax, %edx
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L280:
	movslq	%edx, %r11
	cmpl	(%r10,%r11,4), %r8d
	jge	.L279
.L264:
	movl	%edx, %eax
	leal	(%rdx,%r9), %edx
	sarl	%edx
	cmpl	%r9d, %edx
	jne	.L280
.L263:
	testb	$1, %al
	je	.L270
	cltq
	movl	(%r10,%rax,4), %eax
	cmpl	%eax, 4(%rbx,%rcx,8)
	jle	.L281
.L270:
	xorl	%eax, %eax
.L260:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	addq	$1, %rcx
	cmpl	%ecx, %r12d
	jg	.L266
.L261:
	movq	80(%rsi), %rsi
	movl	$1, %eax
	testq	%rsi, %rsi
	je	.L260
	movl	8(%rsi), %edx
	testl	%edx, %edx
	je	.L260
	movq	80(%rdi), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L260
	call	_ZNK6icu_677UVector11containsAllERKS0_@PLT
	testb	%al, %al
	setne	%al
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L279:
	movl	%edx, %r9d
	jmp	.L265
	.cfi_endproc
.LFE2436:
	.size	_ZNK6icu_6710UnicodeSet11containsAllERKS0_, .-_ZNK6icu_6710UnicodeSet11containsAllERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet8containsEii
	.type	_ZNK6icu_6710UnicodeSet8containsEii, @function
_ZNK6icu_6710UnicodeSet8containsEii:
.LFB2434:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	xorl	%r9d, %r9d
	cmpl	(%r8), %esi
	jl	.L282
	movl	28(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L284
	movslq	%eax, %rcx
	cmpl	-4(%r8,%rcx,4), %esi
	jge	.L284
	xorl	%edi, %edi
.L286:
	movl	%eax, %ecx
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L292:
	movslq	%ecx, %r9
	cmpl	(%r8,%r9,4), %esi
	jge	.L291
.L285:
	movl	%ecx, %eax
	leal	(%rcx,%rdi), %ecx
	sarl	%ecx
	cmpl	%edi, %ecx
	jne	.L292
.L284:
	xorl	%r9d, %r9d
	testb	$1, %al
	je	.L282
	cltq
	cmpl	%edx, (%r8,%rax,4)
	setg	%r9b
.L282:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	movl	%ecx, %edi
	jmp	.L286
	.cfi_endproc
.LFE2434:
	.size	_ZNK6icu_6710UnicodeSet8containsEii, .-_ZNK6icu_6710UnicodeSet8containsEii
	.p2align 4
	.globl	_ZThn8_N6icu_6710UnicodeSetD0Ev
	.type	_ZThn8_N6icu_6710UnicodeSetD0Ev, @function
_ZThn8_N6icu_6710UnicodeSetD0Ev:
.LFB3099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	leaq	-280(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	punpcklqdq	%xmm1, %xmm0
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	88(%rbx), %r13
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	movq	8(%rdi), %rdi
	cmpq	%r13, %rdi
	je	.L294
	call	uprv_free_67@PLT
.L294:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L295
	movq	(%rdi), %rax
	call	*8(%rax)
.L295:
	movq	40(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L296
	call	uprv_free_67@PLT
.L296:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L297
	movq	(%rdi), %rax
	call	*8(%rax)
.L297:
	movq	80(%rbx), %r13
	testq	%r13, %r13
	je	.L298
	movq	%r13, %rdi
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L298:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L299
	call	uprv_free_67@PLT
	movq	$0, 56(%rbx)
	movl	$0, 64(%rbx)
.L299:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeFilterD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3099:
	.size	_ZThn8_N6icu_6710UnicodeSetD0Ev, .-_ZThn8_N6icu_6710UnicodeSetD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet8containsEi
	.type	_ZNK6icu_6710UnicodeSet8containsEi, @function
_ZNK6icu_6710UnicodeSet8containsEi:
.LFB2432:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %r8
	testq	%r8, %r8
	je	.L314
.L316:
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	16(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L332:
	movq	48(%rax), %r8
	leaq	8(%rax), %rdi
	testq	%r8, %r8
	jne	.L316
	.p2align 4,,10
	.p2align 3
.L314:
	movq	88(%rdi), %rax
	testq	%rax, %rax
	jne	.L332
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	jg	.L313
	movq	16(%rdi), %r8
	cmpl	(%r8), %esi
	jl	.L313
	movl	28(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L331
	movslq	%eax, %rcx
	xorl	%edx, %edx
	cmpl	-4(%r8,%rcx,4), %esi
	jl	.L322
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L321:
	movslq	%eax, %rdi
	cmpl	(%r8,%rdi,4), %esi
	jge	.L333
.L322:
	movl	%eax, %ecx
	leal	(%rax,%rdx), %eax
	sarl	%eax
	cmpl	%edx, %eax
	jne	.L321
	movl	%ecx, %eax
.L331:
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	movl	%eax, %edx
	movl	%ecx, %eax
	jmp	.L322
	.cfi_endproc
.LFE2432:
	.size	_ZNK6icu_6710UnicodeSet8containsEi, .-_ZNK6icu_6710UnicodeSet8containsEi
	.align 2
	.p2align 4
	.type	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition.part.0, @function
_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition.part.0:
.LFB3097:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L373
.L335:
	xorl	%r15d, %r15d
	testl	%r12d, %r12d
	je	.L334
	movq	88(%r13), %rdi
	testq	%rdi, %rdi
	je	.L337
	movl	%ebx, %ecx
	movl	%r12d, %edx
	movq	%r14, %rsi
	call	_ZNK6icu_6720UnicodeSetStringSpan4spanEPKDsi17USetSpanCondition@PLT
	movl	%eax, %r15d
.L334:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L374
	addq	$424, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	80(%r13), %rdx
	testq	%rdx, %rdx
	je	.L338
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L375
.L338:
	testl	%ebx, %ebx
	movl	$1, %ecx
	cmove	%ebx, %ecx
	xorl	%r15d, %r15d
	movl	%ecx, -456(%rbp)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L377:
	movq	(%rdi), %rax
	call	*16(%rax)
	movsbl	%al, %eax
.L343:
	cmpl	%eax, -456(%rbp)
	jne	.L334
	movl	%ebx, %r15d
	cmpl	%ebx, %r12d
	jle	.L334
.L350:
	movslq	%r15d, %rax
	leal	1(%r15), %ebx
	movzwl	(%r14,%rax,2), %esi
	leaq	(%rax,%rax), %rcx
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L341
	cmpl	%ebx, %r12d
	jne	.L376
.L341:
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L377
	movq	88(%r13), %rdi
	testq	%rdi, %rdi
	je	.L344
	addq	$8, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi
	movsbl	%al, %eax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L376:
	movzwl	2(%r14,%rcx), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L341
	sall	$10, %esi
	leal	2(%r15), %ebx
	leal	-56613888(%rax,%rsi), %esi
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L344:
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	jg	.L343
	movq	16(%r13), %r9
	cmpl	(%r9), %esi
	jl	.L343
	movl	28(%r13), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L372
	movslq	%eax, %rdi
	xorl	%ecx, %ecx
	cmpl	-4(%r9,%rdi,4), %esi
	jl	.L349
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L348:
	movslq	%eax, %r8
	cmpl	(%r9,%r8,4), %esi
	jge	.L378
.L349:
	movl	%eax, %edi
	leal	(%rax,%rcx), %eax
	sarl	%eax
	cmpl	%ecx, %eax
	jne	.L348
	movl	%edi, %eax
.L372:
	andl	$1, %eax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%rsi, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %r12d
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L375:
	xorl	%ecx, %ecx
	testl	%ebx, %ebx
	leaq	-448(%rbp), %rdi
	movq	%r13, %rsi
	setne	%cl
	movq	%rdi, -456(%rbp)
	addl	$41, %ecx
	call	_ZN6icu_6720UnicodeSetStringSpanC1ERKNS_10UnicodeSetERKNS_7UVectorEj@PLT
	movl	-196(%rbp), %eax
	movq	-456(%rbp), %rdi
	testl	%eax, %eax
	je	.L340
	movl	%ebx, %ecx
	movl	%r12d, %edx
	movq	%r14, %rsi
	call	_ZNK6icu_6720UnicodeSetStringSpan4spanEPKDsi17USetSpanCondition@PLT
	movq	-456(%rbp), %rdi
	movl	%eax, %r15d
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L340:
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L378:
	movl	%eax, %ecx
	movl	%edi, %eax
	jmp	.L349
.L374:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3097:
	.size	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition.part.0, .-_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetD0Ev
	.type	_ZN6icu_6710UnicodeSetD0Ev, @function
_ZN6icu_6710UnicodeSetD0Ev:
.LFB2423:
	.cfi_startproc
	endbr64
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-280(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movups	%xmm0, (%rdi)
	movq	16(%rdi), %rdi
	leaq	96(%r12), %r13
	cmpq	%r13, %rdi
	je	.L380
	call	uprv_free_67@PLT
.L380:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L381
	movq	(%rdi), %rax
	call	*8(%rax)
.L381:
	movq	48(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L382
	call	uprv_free_67@PLT
.L382:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L383
	movq	(%rdi), %rax
	call	*8(%rax)
.L383:
	movq	88(%r12), %r13
	testq	%r13, %r13
	je	.L384
	movq	%r13, %rdi
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L384:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L385
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
.L385:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeFilterD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2423:
	.size	_ZN6icu_6710UnicodeSetD0Ev, .-_ZN6icu_6710UnicodeSetD0Ev
	.p2align 4
	.globl	_ZThn8_N6icu_6710UnicodeSetD1Ev
	.type	_ZThn8_N6icu_6710UnicodeSetD1Ev, @function
_ZThn8_N6icu_6710UnicodeSetD1Ev:
.LFB3101:
	.cfi_startproc
	endbr64
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-280(%rax), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movups	%xmm0, -8(%rdi)
	movq	8(%rdi), %rdi
	leaq	88(%rbx), %r12
	cmpq	%r12, %rdi
	je	.L400
	call	uprv_free_67@PLT
.L400:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L401
	movq	(%rdi), %rax
	call	*8(%rax)
.L401:
	movq	40(%rbx), %rdi
	cmpq	%rdi, %r12
	je	.L402
	call	uprv_free_67@PLT
.L402:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L403
	movq	(%rdi), %rax
	call	*8(%rax)
.L403:
	movq	80(%rbx), %r12
	testq	%r12, %r12
	je	.L404
	movq	%r12, %rdi
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L404:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L405
	call	uprv_free_67@PLT
	movq	$0, 56(%rbx)
	movl	$0, 64(%rbx)
.L405:
	leaq	-8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeFilterD2Ev@PLT
	.cfi_endproc
.LFE3101:
	.size	_ZThn8_N6icu_6710UnicodeSetD1Ev, .-_ZThn8_N6icu_6710UnicodeSetD1Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet5clearEv
	.type	_ZN6icu_6710UnicodeSet5clearEv, @function
_ZN6icu_6710UnicodeSet5clearEv:
.LFB2473:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	movq	%rdi, %rax
	je	.L433
.L430:
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	cmpq	$0, 88(%rdi)
	jne	.L430
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rdi), %rdx
	movl	$1114112, (%rdx)
	movl	$1, 28(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L421
	movq	%rax, -8(%rbp)
	call	uprv_free_67@PLT
	movq	-8(%rbp), %rax
	movq	$0, 64(%rax)
	movl	$0, 72(%rax)
.L421:
	movq	80(%rax), %rdi
	testq	%rdi, %rdi
	je	.L422
	movq	%rax, -8(%rbp)
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movq	-8(%rbp), %rax
.L422:
	movb	$0, 32(%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2473:
	.size	_ZN6icu_6710UnicodeSet5clearEv, .-_ZN6icu_6710UnicodeSet5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet6removeEii
	.type	_ZN6icu_6710UnicodeSet6removeEii, @function
_ZN6icu_6710UnicodeSet6removeEii:
.LFB2462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1114111, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	cmovg	%ecx, %esi
	testl	%esi, %esi
	cmovs	%eax, %esi
	cmpl	$1114111, %edx
	cmovg	%ecx, %edx
	testl	%edx, %edx
	cmovs	%eax, %edx
	cmpl	%edx, %esi
	jle	.L446
.L435:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L447
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	.cfi_restore_state
	addl	$1, %edx
	cmpq	$0, 40(%rdi)
	movl	$1114112, -44(%rbp)
	movl	%esi, -52(%rbp)
	movl	%edx, -48(%rbp)
	jne	.L435
	cmpq	$0, 88(%rdi)
	jne	.L435
	testb	$1, 32(%rdi)
	jne	.L435
	movl	28(%rdi), %edx
	movl	$1114113, %ebx
	leal	2(%rdx), %eax
	cmpl	$1114113, %eax
	cmovg	%ebx, %eax
	cmpl	56(%rdi), %eax
	jle	.L437
	cmpl	$22, %edx
	jle	.L448
	cmpl	$2498, %edx
	jg	.L440
	leal	(%rax,%rax,4), %ebx
.L439:
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L449
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L443
	call	uprv_free_67@PLT
.L443:
	movq	%r13, 48(%r12)
	movl	%ebx, 56(%r12)
.L437:
	leaq	-52(%rbp), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
	jmp	.L435
.L448:
	leal	25(%rax), %ebx
	jmp	.L439
.L440:
	addl	%eax, %eax
	cmpl	$1114113, %eax
	cmovle	%eax, %ebx
	jmp	.L439
.L447:
	call	__stack_chk_fail@PLT
.L449:
	cmpq	$0, 40(%r12)
	je	.L450
.L442:
	movb	$1, 32(%r12)
	jmp	.L435
.L450:
	cmpq	$0, 88(%r12)
	jne	.L442
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L442
	.cfi_endproc
.LFE2462:
	.size	_ZN6icu_6710UnicodeSet6removeEii, .-_ZN6icu_6710UnicodeSet6removeEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet6retainEii
	.type	_ZN6icu_6710UnicodeSet6retainEii, @function
_ZN6icu_6710UnicodeSet6retainEii:
.LFB2460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1114111, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	cmovg	%ecx, %esi
	testl	%esi, %esi
	cmovs	%eax, %esi
	cmpl	$1114111, %edx
	cmovg	%ecx, %edx
	testl	%edx, %edx
	cmovs	%eax, %edx
	movq	40(%rdi), %rax
	cmpl	%esi, %edx
	jge	.L472
	testq	%rax, %rax
	je	.L473
.L461:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L474
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L461
	movq	16(%rdi), %rax
	movl	$1114112, (%rax)
	movl	$1, 28(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L462
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
.L462:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L463
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
.L463:
	movb	$0, 32(%r12)
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L472:
	addl	$1, %edx
	movl	$1114112, -44(%rbp)
	movl	%esi, -52(%rbp)
	movl	%edx, -48(%rbp)
	testq	%rax, %rax
	jne	.L461
	cmpq	$0, 88(%rdi)
	jne	.L461
	testb	$1, 32(%rdi)
	jne	.L461
	movl	28(%rdi), %edx
	movl	$1114113, %ebx
	leal	2(%rdx), %eax
	cmpl	$1114113, %eax
	cmovg	%ebx, %eax
	cmpl	56(%rdi), %eax
	jle	.L454
	cmpl	$22, %edx
	jle	.L475
	cmpl	$2498, %edx
	jg	.L457
	leal	(%rax,%rax,4), %ebx
.L456:
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L476
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L460
	call	uprv_free_67@PLT
.L460:
	movq	%r13, 48(%r12)
	movl	%ebx, 56(%r12)
.L454:
	leaq	-52(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
	jmp	.L461
.L475:
	leal	25(%rax), %ebx
	jmp	.L456
.L457:
	addl	%eax, %eax
	cmpl	$1114113, %eax
	cmovle	%eax, %ebx
	jmp	.L456
.L474:
	call	__stack_chk_fail@PLT
.L476:
	cmpq	$0, 40(%r12)
	je	.L477
.L459:
	movb	$1, 32(%r12)
	jmp	.L461
.L477:
	cmpq	$0, 88(%r12)
	jne	.L459
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L459
	.cfi_endproc
.LFE2460:
	.size	_ZN6icu_6710UnicodeSet6retainEii, .-_ZN6icu_6710UnicodeSet6retainEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet9removeAllERKS0_
	.type	_ZN6icu_6710UnicodeSet9removeAllERKS0_, @function
_ZN6icu_6710UnicodeSet9removeAllERKS0_:
.LFB2471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	$0, 40(%rdi)
	je	.L497
.L480:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L480
	testb	$1, 32(%rdi)
	jne	.L480
	movl	28(%rdi), %eax
	movl	$1114113, %r13d
	addl	28(%rsi), %eax
	movq	%rsi, %rbx
	cmpl	$1114113, %eax
	movl	%r13d, %edx
	movq	16(%rsi), %r14
	cmovle	%eax, %edx
	cmpl	56(%rdi), %edx
	jle	.L482
	cmpl	$24, %eax
	jle	.L498
	cmpl	$2500, %eax
	jg	.L485
	leal	(%rdx,%rdx,4), %r13d
.L484:
	movslq	%r13d, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L499
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L489
	call	uprv_free_67@PLT
.L489:
	movq	%r15, 48(%r12)
	movl	%r13d, 56(%r12)
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
.L488:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L480
	movl	8(%rdi), %edx
	testl	%edx, %edx
	je	.L480
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L480
	movl	8(%rsi), %eax
	testl	%eax, %eax
	je	.L480
	call	_ZN6icu_677UVector9removeAllERKS0_@PLT
	jmp	.L480
.L498:
	leal	25(%rdx), %r13d
	jmp	.L484
.L485:
	addl	%edx, %edx
	cmpl	$1114113, %edx
	cmovle	%edx, %r13d
	jmp	.L484
.L499:
	cmpq	$0, 40(%r12)
	je	.L500
.L487:
	movb	$1, 32(%r12)
	jmp	.L488
.L500:
	cmpq	$0, 88(%r12)
	jne	.L487
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L487
	.cfi_endproc
.LFE2471:
	.size	_ZN6icu_6710UnicodeSet9removeAllERKS0_, .-_ZN6icu_6710UnicodeSet9removeAllERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet9retainAllERKS0_
	.type	_ZN6icu_6710UnicodeSet9retainAllERKS0_, @function
_ZN6icu_6710UnicodeSet9retainAllERKS0_:
.LFB2470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	$0, 40(%rdi)
	je	.L521
.L503:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L503
	testb	$1, 32(%rdi)
	jne	.L503
	movl	28(%rdi), %eax
	movl	$1114113, %r13d
	addl	28(%rsi), %eax
	movq	%rsi, %rbx
	cmpl	$1114113, %eax
	movl	%r13d, %edx
	movq	16(%rsi), %r14
	cmovle	%eax, %edx
	cmpl	56(%rdi), %edx
	jle	.L505
	cmpl	$24, %eax
	jle	.L522
	cmpl	$2500, %eax
	jg	.L508
	leal	(%rdx,%rdx,4), %r13d
.L507:
	movslq	%r13d, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L523
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L512
	call	uprv_free_67@PLT
.L512:
	movq	%r15, 48(%r12)
	movl	%r13d, 56(%r12)
	.p2align 4,,10
	.p2align 3
.L505:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
.L511:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L503
	movl	8(%rdi), %edx
	testl	%edx, %edx
	je	.L503
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L513
	movl	8(%rsi), %eax
	testl	%eax, %eax
	je	.L513
	call	_ZN6icu_677UVector9retainAllERKS0_@PLT
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L513:
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L503
.L522:
	leal	25(%rdx), %r13d
	jmp	.L507
.L508:
	addl	%edx, %edx
	cmpl	$1114113, %edx
	cmovle	%edx, %r13d
	jmp	.L507
.L523:
	cmpq	$0, 40(%r12)
	je	.L524
.L510:
	movb	$1, 32(%r12)
	jmp	.L511
.L524:
	cmpq	$0, 88(%r12)
	jne	.L510
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L510
	.cfi_endproc
.LFE2470:
	.size	_ZN6icu_6710UnicodeSet9retainAllERKS0_, .-_ZN6icu_6710UnicodeSet9retainAllERKS0_
	.align 2
	.p2align 4
	.type	_ZN6icu_6710UnicodeSet8copyFromERKS0_a.part.0, @function
_ZN6icu_6710UnicodeSet8copyFromERKS0_a.part.0:
.LFB3096:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movslq	28(%rsi), %rdx
	movq	16(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%edx, 28(%rdi)
	movq	16(%rdi), %rdi
	salq	$2, %rdx
	call	memcpy@PLT
	testb	%r13b, %r13b
	sete	%r13b
	cmpq	$0, 40(%r12)
	je	.L526
	testb	%r13b, %r13b
	jne	.L576
.L526:
	movq	80(%r12), %rsi
	movq	80(%rbx), %rdi
	testq	%rsi, %rsi
	je	.L529
	movl	8(%rsi), %r9d
	testl	%r9d, %r9d
	je	.L529
	movl	$0, -44(%rbp)
	leaq	-44(%rbp), %r14
	testq	%rdi, %rdi
	je	.L577
.L530:
	movq	%r14, %rcx
	leaq	_ZN6icu_67L18cloneUnicodeStringEP8UElementS1_(%rip), %rdx
	call	_ZN6icu_677UVector6assignERKS0_PFvP8UElementS4_ER10UErrorCode@PLT
	movl	-44(%rbp), %edi
	testl	%edi, %edi
	jg	.L533
.L534:
	cmpq	$0, 88(%r12)
	je	.L537
	testb	%r13b, %r13b
	jne	.L578
.L537:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L539
.L580:
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L539:
	movq	64(%r12), %r13
	testq	%r13, %r13
	jne	.L544
.L525:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L579
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L534
	movl	8(%rdi), %esi
	testl	%esi, %esi
	je	.L534
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L576:
	movl	$872, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L527
	movl	28(%rbx), %ecx
	movq	16(%rbx), %rdx
	movq	%rax, %rdi
	movq	40(%r12), %rsi
	call	_ZN6icu_676BMPSetC1ERKS0_PKii@PLT
	movq	%r14, 40(%rbx)
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L544:
	movl	72(%r12), %r12d
	leal	1(%r12), %edi
	movslq	%edi, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L525
	movl	%r12d, 72(%rbx)
	movl	%r12d, %edx
	movq	%r13, %rsi
	call	u_memcpy_67@PLT
	movslq	72(%rbx), %rdx
	movq	64(%rbx), %rax
	xorl	%ecx, %ecx
	movw	%cx, (%rax,%rdx,2)
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L578:
	movl	$392, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L538
	movq	80(%rbx), %rdx
	movq	88(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6720UnicodeSetStringSpanC1ERKS0_RKNS_7UVectorE@PLT
	movq	64(%rbx), %rdi
	movq	%r13, 88(%rbx)
	testq	%rdi, %rdi
	jne	.L580
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L532:
	call	_ZN6icu_677UVectorD0Ev@PLT
	movq	$0, 80(%rbx)
.L533:
	cmpq	$0, 40(%rbx)
	je	.L581
.L535:
	movb	$1, 32(%rbx)
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L577:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L531
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %r8
	movq	%rax, -56(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movq	-56(%rbp), %rdi
	movl	-44(%rbp), %r8d
	movq	%rdi, 80(%rbx)
	testl	%r8d, %r8d
	jg	.L532
	movq	80(%r12), %rsi
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L581:
	cmpq	$0, 88(%rbx)
	jne	.L535
	movq	16(%rbx), %rax
	movq	64(%rbx), %rdi
	movl	$1114112, (%rax)
	movl	$1, 28(%rbx)
	testq	%rdi, %rdi
	je	.L536
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L536:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L535
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L535
.L579:
	call	__stack_chk_fail@PLT
.L527:
	cmpq	$0, 88(%rbx)
	movq	$0, 40(%rbx)
	jne	.L535
.L540:
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L535
.L538:
	cmpq	$0, 40(%rbx)
	movq	$0, 88(%rbx)
	jne	.L535
	jmp	.L540
.L531:
	movq	$0, 80(%rbx)
	movl	$7, -44(%rbp)
	jmp	.L533
	.cfi_endproc
.LFE3096:
	.size	_ZN6icu_6710UnicodeSet8copyFromERKS0_a.part.0, .-_ZN6icu_6710UnicodeSet8copyFromERKS0_a.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet6addAllERKS0_
	.type	_ZN6icu_6710UnicodeSet6addAllERKS0_, @function
_ZN6icu_6710UnicodeSet6addAllERKS0_:
.LFB2469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	28(%rsi), %eax
	testl	%eax, %eax
	jle	.L583
	movq	16(%rsi), %r14
	testq	%r14, %r14
	je	.L583
	cmpq	$0, 40(%rdi)
	je	.L636
.L583:
	movq	80(%r13), %rdi
	testq	%rdi, %rdi
	je	.L591
	movl	8(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L591
	xorl	%ebx, %ebx
	leaq	-60(%rbp), %r14
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L592:
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	js	.L596
.L595:
	movq	80(%r13), %rdi
	addl	$1, %ebx
	cmpl	%ebx, 8(%rdi)
	jle	.L591
.L609:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	80(%r12), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	jne	.L592
.L596:
	cmpq	$0, 40(%r12)
	jne	.L595
	cmpq	$0, 88(%r12)
	jne	.L595
	testb	$1, 32(%r12)
	jne	.L595
	cmpq	$0, 80(%r12)
	movl	$0, -60(%rbp)
	je	.L637
.L597:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L638
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rax, -72(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-72(%rbp), %r8
	movq	%r14, %rcx
	movq	80(%r12), %rdi
	leaq	_ZN6icu_67L20compareUnicodeStringE8UElementS0_(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN6icu_677UVector12sortedInsertEPvPFa8UElementS2_ER10UErrorCode@PLT
	movl	-60(%rbp), %eax
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	jle	.L595
	cmpq	$0, 40(%r12)
	je	.L639
.L607:
	movb	$1, 32(%r12)
	movq	(%r8), %rax
	movq	%r8, %rdi
	addl	$1, %ebx
	call	*8(%rax)
	movq	80(%r13), %rdi
	cmpl	%ebx, 8(%rdi)
	jg	.L609
	.p2align 4,,10
	.p2align 3
.L591:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L640
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L583
	testb	$1, 32(%rdi)
	jne	.L583
	addl	28(%rdi), %eax
	movl	$1114113, %ebx
	cmpl	$1114113, %eax
	movl	%ebx, %edx
	cmovle	%eax, %edx
	cmpl	56(%rdi), %edx
	jle	.L584
	cmpl	$24, %eax
	jle	.L641
	cmpl	$2500, %eax
	jg	.L587
	leal	(%rdx,%rdx,4), %ebx
.L586:
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L642
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L590
	call	uprv_free_67@PLT
.L590:
	movq	%r15, 48(%r12)
	movl	%ebx, 56(%r12)
.L584:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEPKiia.part.0
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L637:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L598
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %r8
	movq	%rax, -72(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movq	-72(%rbp), %rdi
	movl	-60(%rbp), %edx
	movq	%rdi, 80(%r12)
	testl	%edx, %edx
	jle	.L597
	call	_ZN6icu_677UVectorD0Ev@PLT
	movq	$0, 80(%r12)
.L599:
	cmpq	$0, 40(%r12)
	je	.L643
.L606:
	movb	$1, 32(%r12)
	jmp	.L595
.L639:
	cmpq	$0, 88(%r12)
	jne	.L607
	movq	16(%r12), %rax
	movq	64(%r12), %rdi
	movl	$1114112, (%rax)
	movl	$1, 28(%r12)
	testq	%rdi, %rdi
	je	.L608
	movq	%r8, -72(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %r8
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
.L608:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L607
	movq	%r8, -72(%rbp)
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movq	-72(%rbp), %r8
	jmp	.L607
.L643:
	cmpq	$0, 88(%r12)
	jne	.L606
	movq	16(%r12), %rax
	movq	64(%r12), %rdi
	movl	$1114112, (%rax)
	movl	$1, 28(%r12)
	testq	%rdi, %rdi
	je	.L603
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
.L603:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L606
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L606
.L641:
	leal	25(%rdx), %ebx
	jmp	.L586
.L587:
	addl	%edx, %edx
	cmpl	$1114113, %edx
	cmovle	%edx, %ebx
	jmp	.L586
.L640:
	call	__stack_chk_fail@PLT
.L638:
	cmpq	$0, 40(%r12)
	jne	.L606
	cmpq	$0, 88(%r12)
	jne	.L606
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L606
.L642:
	cmpq	$0, 40(%r12)
	je	.L644
.L589:
	movb	$1, 32(%r12)
	jmp	.L583
.L598:
	movq	$0, 80(%r12)
	movl	$7, -60(%rbp)
	jmp	.L599
.L644:
	cmpq	$0, 88(%r12)
	jne	.L589
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L589
	.cfi_endproc
.LFE2469:
	.size	_ZN6icu_6710UnicodeSet6addAllERKS0_, .-_ZN6icu_6710UnicodeSet6addAllERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet13addMatchSetToERS0_
	.type	_ZNK6icu_6710UnicodeSet13addMatchSetToERS0_, @function
_ZNK6icu_6710UnicodeSet13addMatchSetToERS0_:
.LFB2444:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%r8, %rsi
	jmp	_ZN6icu_6710UnicodeSet6addAllERKS0_
	.cfi_endproc
.LFE2444:
	.size	_ZNK6icu_6710UnicodeSet13addMatchSetToERS0_, .-_ZNK6icu_6710UnicodeSet13addMatchSetToERS0_
	.p2align 4
	.globl	_ZThn8_NK6icu_6710UnicodeSet13addMatchSetToERS0_
	.type	_ZThn8_NK6icu_6710UnicodeSet13addMatchSetToERS0_, @function
_ZThn8_NK6icu_6710UnicodeSet13addMatchSetToERS0_:
.LFB3098:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	leaq	-8(%r8), %rsi
	jmp	_ZN6icu_6710UnicodeSet6addAllERKS0_
	.cfi_endproc
.LFE3098:
	.size	_ZThn8_NK6icu_6710UnicodeSet13addMatchSetToERS0_, .-_ZThn8_NK6icu_6710UnicodeSet13addMatchSetToERS0_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2764:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2764:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2767:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L660
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L648
	cmpb	$0, 12(%rbx)
	jne	.L661
.L652:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L648:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L652
	.cfi_endproc
.LFE2767:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2770:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L664
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2770:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2773:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L667
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L667:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2773:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L673
.L669:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L674
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L674:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2775:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2776:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2776:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2777:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2777:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2778:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2778:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2779:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2779:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2780:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2780:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2781:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L690
	testl	%edx, %edx
	jle	.L690
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L693
.L682:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L693:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L682
	.cfi_endproc
.LFE2781:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L697
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L697
	testl	%r12d, %r12d
	jg	.L704
	cmpb	$0, 12(%rbx)
	jne	.L705
.L699:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L704:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L699
.L705:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L697:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2782:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L707
	movq	(%rdi), %r8
.L708:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L711
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L711
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L711:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2783:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2784:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L718
	ret
	.p2align 4,,10
	.p2align 3
.L718:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2784:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2785:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2785:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2786:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2786:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2787:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2787:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2789:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2789:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2791:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2791:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711SymbolTableD2Ev
	.type	_ZN6icu_6711SymbolTableD2Ev, @function
_ZN6icu_6711SymbolTableD2Ev:
.LFB2379:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2379:
	.size	_ZN6icu_6711SymbolTableD2Ev, .-_ZN6icu_6711SymbolTableD2Ev
	.globl	_ZN6icu_6711SymbolTableD1Ev
	.set	_ZN6icu_6711SymbolTableD1Ev,_ZN6icu_6711SymbolTableD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711SymbolTableD0Ev
	.type	_ZN6icu_6711SymbolTableD0Ev, @function
_ZN6icu_6711SymbolTableD0Ev:
.LFB2381:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE2381:
	.size	_ZN6icu_6711SymbolTableD0Ev, .-_ZN6icu_6711SymbolTableD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet16getStaticClassIDEv
	.type	_ZN6icu_6710UnicodeSet16getStaticClassIDEv, @function
_ZN6icu_6710UnicodeSet16getStaticClassIDEv:
.LFB2382:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6710UnicodeSet16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2382:
	.size	_ZN6icu_6710UnicodeSet16getStaticClassIDEv, .-_ZN6icu_6710UnicodeSet16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet10hasStringsEv
	.type	_ZNK6icu_6710UnicodeSet10hasStringsEv, @function
_ZNK6icu_6710UnicodeSet10hasStringsEv:
.LFB2387:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L727
	movl	8(%rdx), %eax
	testl	%eax, %eax
	setne	%al
.L727:
	ret
	.cfi_endproc
.LFE2387:
	.size	_ZNK6icu_6710UnicodeSet10hasStringsEv, .-_ZNK6icu_6710UnicodeSet10hasStringsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet11stringsSizeEv
	.type	_ZNK6icu_6710UnicodeSet11stringsSizeEv, @function
_ZNK6icu_6710UnicodeSet11stringsSizeEv:
.LFB2388:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L731
	movl	8(%rdx), %eax
.L731:
	ret
	.cfi_endproc
.LFE2388:
	.size	_ZNK6icu_6710UnicodeSet11stringsSizeEv, .-_ZNK6icu_6710UnicodeSet11stringsSizeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet15stringsContainsERKNS_13UnicodeStringE
	.type	_ZNK6icu_6710UnicodeSet15stringsContainsERKNS_13UnicodeStringE, @function
_ZNK6icu_6710UnicodeSet15stringsContainsERKNS_13UnicodeStringE:
.LFB2389:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L739
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	notl	%eax
	shrl	$31, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2389:
	.size	_ZNK6icu_6710UnicodeSet15stringsContainsERKNS_13UnicodeStringE, .-_ZNK6icu_6710UnicodeSet15stringsContainsERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetC2Ev
	.type	_ZN6icu_6710UnicodeSetC2Ev, @function
_ZN6icu_6710UnicodeSetC2Ev:
.LFB2400:
	.cfi_startproc
	endbr64
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, 32(%rdi)
	leaq	-280(%rax), %rdx
	movq	%rax, %xmm1
	leaq	96(%rdi), %rax
	movl	$0, 56(%rdi)
	movq	%rdx, %xmm0
	movq	%rax, 16(%rdi)
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%rdi)
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 64(%rdi)
	movl	$0, 72(%rdi)
	movl	$1114112, 96(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 80(%rdi)
	ret
	.cfi_endproc
.LFE2400:
	.size	_ZN6icu_6710UnicodeSetC2Ev, .-_ZN6icu_6710UnicodeSetC2Ev
	.globl	_ZN6icu_6710UnicodeSetC1Ev
	.set	_ZN6icu_6710UnicodeSetC1Ev,_ZN6icu_6710UnicodeSetC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetC2ERKS0_a
	.type	_ZN6icu_6710UnicodeSetC2ERKS0_a, @function
_ZN6icu_6710UnicodeSetC2ERKS0_a:
.LFB2418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	96(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movq	%r15, 16(%rdi)
	leaq	-280(%rax), %rcx
	movq	%rax, %xmm1
	movabsq	$4294967321, %rax
	movb	$0, 32(%rdi)
	movq	%rcx, %xmm0
	movq	%rax, 24(%rdi)
	movl	28(%rsi), %eax
	punpcklqdq	%xmm1, %xmm0
	movl	$0, 56(%rdi)
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 64(%rdi)
	movl	$0, 72(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 80(%rdi)
	cmpl	$25, %eax
	jle	.L746
	movl	$1114113, %r14d
	cmpl	$1114113, %eax
	movl	%r14d, %edx
	cmovle	%eax, %edx
	cmpl	$2500, %eax
	jle	.L779
	addl	%edx, %edx
	cmpl	$1114113, %edx
	cmovle	%edx, %r14d
.L748:
	movslq	%r14d, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L780
	movq	16(%rbx), %r8
	movslq	28(%rbx), %rdx
	movq	%rax, %rdi
	movq	%r8, %rsi
	salq	$2, %rdx
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	cmpq	%r8, %r15
	je	.L752
	movq	%r8, %rdi
	call	uprv_free_67@PLT
.L752:
	movq	%r13, 16(%rbx)
	movq	%r13, %r15
	movl	%r14d, 24(%rbx)
.L746:
	movslq	28(%r12), %rdx
	movq	16(%r12), %rsi
	movq	%r15, %rdi
	movl	%edx, 28(%rbx)
	salq	$2, %rdx
	call	memcpy@PLT
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L753
	movl	8(%rax), %r8d
	testl	%r8d, %r8d
	je	.L753
	movl	$40, %edi
	movl	$0, -60(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L754
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	leaq	-60(%rbp), %r14
	movq	%rax, %rdi
	movl	$1, %ecx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %r8
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movl	-60(%rbp), %edi
	movq	%r13, 80(%rbx)
	testl	%edi, %edi
	jg	.L781
	movq	80(%r12), %rsi
	movq	%r14, %rcx
	leaq	_ZN6icu_67L18cloneUnicodeStringEP8UElementS1_(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_677UVector6assignERKS0_PFvP8UElementS4_ER10UErrorCode@PLT
	movl	-60(%rbp), %esi
	testl	%esi, %esi
	jg	.L756
	.p2align 4,,10
	.p2align 3
.L753:
	movq	64(%r12), %r13
	testq	%r13, %r13
	je	.L745
	movq	64(%rbx), %rdi
	movl	72(%r12), %r12d
	testq	%rdi, %rdi
	je	.L760
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L760:
	leal	1(%r12), %edi
	movslq	%edi, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L745
	movl	%r12d, 72(%rbx)
	movl	%r12d, %edx
	movq	%r13, %rsi
	call	u_memcpy_67@PLT
	movslq	72(%rbx), %rdx
	movq	64(%rbx), %rax
	xorl	%ecx, %ecx
	movw	%cx, (%rax,%rdx,2)
.L745:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L782
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L779:
	.cfi_restore_state
	leal	(%rdx,%rdx,4), %r14d
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L781:
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorD0Ev@PLT
	movq	$0, 80(%rbx)
.L756:
	cmpq	$0, 40(%rbx)
	je	.L783
.L757:
	movb	$1, 32(%rbx)
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L783:
	cmpq	$0, 88(%rbx)
	jne	.L757
	movq	16(%rbx), %rax
	movq	64(%rbx), %rdi
	movl	$1114112, (%rax)
	movl	$1, 28(%rbx)
	testq	%rdi, %rdi
	je	.L758
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L758:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L757
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L757
.L782:
	call	__stack_chk_fail@PLT
.L780:
	cmpq	$0, 40(%rbx)
	jne	.L757
	cmpq	$0, 88(%rbx)
	jne	.L757
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L757
.L754:
	movq	$0, 80(%rbx)
	movl	$7, -60(%rbp)
	jmp	.L756
	.cfi_endproc
.LFE2418:
	.size	_ZN6icu_6710UnicodeSetC2ERKS0_a, .-_ZN6icu_6710UnicodeSetC2ERKS0_a
	.globl	_ZN6icu_6710UnicodeSetC1ERKS0_a
	.set	_ZN6icu_6710UnicodeSetC1ERKS0_a,_ZN6icu_6710UnicodeSetC2ERKS0_a
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet13cloneAsThawedEv
	.type	_ZNK6icu_6710UnicodeSet13cloneAsThawedEv, @function
_ZNK6icu_6710UnicodeSet13cloneAsThawedEv:
.LFB2427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$200, %edi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L784
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	leaq	96(%r12), %r15
	movb	$0, 32(%r12)
	leaq	-280(%rax), %rcx
	movq	%rax, %xmm1
	movabsq	$4294967321, %rax
	movq	%r15, 16(%r12)
	movq	%rcx, %xmm0
	movq	%rax, 24(%r12)
	movl	28(%rbx), %eax
	movl	$0, 56(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	pxor	%xmm0, %xmm0
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	movups	%xmm0, 40(%r12)
	movups	%xmm0, 80(%r12)
	cmpl	$25, %eax
	jle	.L787
	movl	$1114113, %r14d
	cmpl	$1114113, %eax
	movl	%r14d, %edx
	cmovle	%eax, %edx
	cmpl	$2500, %eax
	jle	.L821
	addl	%edx, %edx
	cmpl	$1114113, %edx
	cmovle	%edx, %r14d
.L789:
	movslq	%r14d, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L822
	movq	16(%r12), %r8
	movslq	28(%r12), %rdx
	movq	%rax, %rdi
	movq	%r8, %rsi
	salq	$2, %rdx
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	cmpq	%r8, %r15
	je	.L792
	movq	%r8, %rdi
	call	uprv_free_67@PLT
.L792:
	movq	%r13, 16(%r12)
	movq	%r13, %r15
	movl	%r14d, 24(%r12)
.L787:
	movslq	28(%rbx), %rdx
	movq	16(%rbx), %rsi
	movq	%r15, %rdi
	movl	%edx, 28(%r12)
	salq	$2, %rdx
	call	memcpy@PLT
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L793
	movl	8(%rax), %r8d
	testl	%r8d, %r8d
	je	.L793
	movl	$40, %edi
	movl	$0, -60(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L794
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	leaq	-60(%rbp), %r14
	movq	%rax, %rdi
	movl	$1, %ecx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %r8
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movl	-60(%rbp), %edi
	movq	%r13, 80(%r12)
	testl	%edi, %edi
	jg	.L823
	movq	80(%rbx), %rsi
	movq	%r14, %rcx
	leaq	_ZN6icu_67L18cloneUnicodeStringEP8UElementS1_(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN6icu_677UVector6assignERKS0_PFvP8UElementS4_ER10UErrorCode@PLT
	movl	-60(%rbp), %esi
	testl	%esi, %esi
	jg	.L796
	.p2align 4,,10
	.p2align 3
.L793:
	movq	64(%rbx), %r13
	testq	%r13, %r13
	je	.L784
	movq	64(%r12), %rdi
	movl	72(%rbx), %r14d
	testq	%rdi, %rdi
	je	.L800
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
.L800:
	leal	1(%r14), %edi
	movslq	%edi, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L784
	movl	%r14d, 72(%r12)
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	u_memcpy_67@PLT
	movslq	72(%r12), %rdx
	movq	64(%r12), %rax
	xorl	%ecx, %ecx
	movw	%cx, (%rax,%rdx,2)
.L784:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L824
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L821:
	.cfi_restore_state
	leal	(%rdx,%rdx,4), %r14d
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L823:
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorD0Ev@PLT
	movq	$0, 80(%r12)
.L796:
	cmpq	$0, 40(%r12)
	je	.L825
.L797:
	movb	$1, 32(%r12)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L825:
	cmpq	$0, 88(%r12)
	jne	.L797
	movq	16(%r12), %rax
	movq	64(%r12), %rdi
	movl	$1114112, (%rax)
	movl	$1, 28(%r12)
	testq	%rdi, %rdi
	je	.L798
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
.L798:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L797
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L797
.L824:
	call	__stack_chk_fail@PLT
.L822:
	cmpq	$0, 40(%r12)
	jne	.L797
	cmpq	$0, 88(%r12)
	jne	.L797
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L797
.L794:
	movq	$0, 80(%r12)
	movl	$7, -60(%rbp)
	jmp	.L796
	.cfi_endproc
.LFE2427:
	.size	_ZNK6icu_6710UnicodeSet13cloneAsThawedEv, .-_ZNK6icu_6710UnicodeSet13cloneAsThawedEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet13findCodePointEi
	.type	_ZNK6icu_6710UnicodeSet13findCodePointEi, @function
_ZNK6icu_6710UnicodeSet13findCodePointEi:
.LFB2433:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r9
	xorl	%r8d, %r8d
	cmpl	%esi, (%r9)
	jg	.L826
	movl	28(%rdi), %r8d
	subl	$1, %r8d
	testl	%r8d, %r8d
	jle	.L826
	movslq	%r8d, %rax
	cmpl	%esi, -4(%r9,%rax,4)
	jle	.L826
	xorl	%edx, %edx
.L829:
	movl	%r8d, %eax
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L833:
	movslq	%eax, %rcx
	cmpl	%esi, (%r9,%rcx,4)
	jle	.L832
.L828:
	movl	%eax, %r8d
	leal	(%rdx,%rax), %eax
	sarl	%eax
	cmpl	%eax, %edx
	jne	.L833
.L826:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	movl	%eax, %edx
	jmp	.L829
	.cfi_endproc
.LFE2433:
	.size	_ZNK6icu_6710UnicodeSet13findCodePointEi, .-_ZNK6icu_6710UnicodeSet13findCodePointEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE
	.type	_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE, @function
_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE:
.LFB2435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movzwl	8(%rsi), %ecx
	movq	%rsi, %r12
	testw	%cx, %cx
	js	.L835
	movswl	%cx, %edx
	xorl	%eax, %eax
	sarl	$5, %edx
	jne	.L837
.L834:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_restore_state
	movl	12(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L834
.L837:
	cmpl	$2, %edx
	jg	.L845
	cmpl	$1, %edx
	jne	.L841
	andl	$2, %ecx
	jne	.L851
	movq	24(%r12), %r12
.L843:
	movzwl	(%r12), %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6710UnicodeSet8containsEi
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_restore_state
	movq	80(%r13), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L834
	movq	%r12, %rsi
	xorl	%edx, %edx
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	popq	%r12
	popq	%r13
	notl	%eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrl	$31, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L841:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %esi
	cmpl	$65535, %eax
	jle	.L845
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6710UnicodeSet8containsEi
	.p2align 4,,10
	.p2align 3
.L851:
	.cfi_restore_state
	addq	$10, %r12
	jmp	.L843
	.cfi_endproc
.LFE2435:
	.size	_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE, .-_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet11containsAllERKNS_13UnicodeStringE
	.type	_ZNK6icu_6710UnicodeSet11containsAllERKNS_13UnicodeStringE, @function
_ZNK6icu_6710UnicodeSet11containsAllERKNS_13UnicodeStringE:
.LFB2437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzwl	8(%rsi), %eax
	movq	%rsi, %rbx
	testw	%ax, %ax
	js	.L853
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L861
.L868:
	leaq	10(%rbx), %r12
	testb	$2, %al
	je	.L866
.L855:
	testl	%edx, %edx
	jle	.L857
	movq	40(%rdi), %r8
	testq	%r8, %r8
	je	.L857
	movslq	%edx, %rdx
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	leaq	(%r12,%rdx,2), %rdx
	call	_ZNK6icu_676BMPSet4spanEPKDsS2_17USetSpanCondition@PLT
	movswl	8(%rbx), %edx
	subq	%r12, %rax
	sarq	%rax
	testw	%dx, %dx
	jns	.L867
.L859:
	movl	12(%rbx), %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmpl	%eax, %edx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L857:
	.cfi_restore_state
	movl	$1, %ecx
	movq	%r12, %rsi
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition.part.0
	movswl	8(%rbx), %edx
	testw	%dx, %dx
	js	.L859
.L867:
	sarl	$5, %edx
	popq	%rbx
	popq	%r12
	cmpl	%eax, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	movq	24(%rbx), %r12
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L853:
	movl	12(%rsi), %edx
	testb	$17, %al
	je	.L868
.L861:
	xorl	%r12d, %r12d
	jmp	.L855
	.cfi_endproc
.LFE2437:
	.size	_ZNK6icu_6710UnicodeSet11containsAllERKNS_13UnicodeStringE, .-_ZNK6icu_6710UnicodeSet11containsAllERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet12containsNoneEii
	.type	_ZNK6icu_6710UnicodeSet12containsNoneEii, @function
_ZNK6icu_6710UnicodeSet12containsNoneEii:
.LFB2438:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	movl	(%r8), %eax
	cmpl	%eax, %esi
	jl	.L870
	movl	28(%rdi), %ecx
	subl	$1, %ecx
	testl	%ecx, %ecx
	jle	.L871
	movslq	%ecx, %rax
	cmpl	-4(%r8,%rax,4), %esi
	jge	.L871
	xorl	%edi, %edi
.L873:
	movl	%ecx, %eax
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L878:
	movslq	%eax, %r9
	cmpl	(%r8,%r9,4), %esi
	jge	.L877
.L872:
	movl	%eax, %ecx
	leal	(%rax,%rdi), %eax
	sarl	%eax
	cmpl	%edi, %eax
	jne	.L878
.L871:
	xorl	%eax, %eax
	testb	$1, %cl
	je	.L879
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	movslq	%ecx, %rcx
	movl	(%r8,%rcx,4), %eax
.L870:
	cmpl	%eax, %edx
	setl	%al
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	movl	%eax, %edi
	jmp	.L873
	.cfi_endproc
.LFE2438:
	.size	_ZNK6icu_6710UnicodeSet12containsNoneEii, .-_ZNK6icu_6710UnicodeSet12containsNoneEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet12containsNoneERKS0_
	.type	_ZNK6icu_6710UnicodeSet12containsNoneERKS0_, @function
_ZNK6icu_6710UnicodeSet12containsNoneERKS0_:
.LFB2439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	28(%rsi), %eax
	movl	%eax, %r12d
	shrl	$31, %r12d
	addl	%eax, %r12d
	sarl	%r12d
	cmpl	$1, %eax
	jle	.L881
	movq	16(%rdi), %r11
	movq	16(%rsi), %rbx
	xorl	%ecx, %ecx
	movl	(%r11), %r13d
	.p2align 4,,10
	.p2align 3
.L887:
	movl	(%rbx,%rcx,8), %r8d
	cmpl	%r13d, %r8d
	jl	.L889
	movl	28(%rdi), %eax
	leal	-1(%rax), %edx
	testl	%edx, %edx
	jle	.L883
	movslq	%edx, %rax
	cmpl	-4(%r11,%rax,4), %r8d
	jge	.L883
	xorl	%r9d, %r9d
.L885:
	movl	%edx, %eax
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L901:
	movslq	%eax, %r10
	cmpl	(%r11,%r10,4), %r8d
	jge	.L900
.L884:
	movl	%eax, %edx
	leal	(%rax,%r9), %eax
	sarl	%eax
	cmpl	%r9d, %eax
	jne	.L901
.L883:
	testb	$1, %dl
	je	.L902
.L891:
	xorl	%eax, %eax
.L880:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L902:
	.cfi_restore_state
	movslq	%edx, %rdx
	movl	(%r11,%rdx,4), %eax
.L882:
	cmpl	%eax, 4(%rbx,%rcx,8)
	jg	.L891
	addq	$1, %rcx
	cmpl	%ecx, %r12d
	jg	.L887
.L881:
	movq	80(%rdi), %rdi
	movl	$1, %eax
	testq	%rdi, %rdi
	je	.L880
	movq	80(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L880
	movl	8(%rsi), %edx
	testl	%edx, %edx
	je	.L880
	call	_ZNK6icu_677UVector12containsNoneERKS0_@PLT
	testb	%al, %al
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L889:
	.cfi_restore_state
	movl	%r13d, %eax
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L900:
	movl	%eax, %r9d
	jmp	.L885
	.cfi_endproc
.LFE2439:
	.size	_ZNK6icu_6710UnicodeSet12containsNoneERKS0_, .-_ZNK6icu_6710UnicodeSet12containsNoneERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet12containsNoneERKNS_13UnicodeStringE
	.type	_ZNK6icu_6710UnicodeSet12containsNoneERKNS_13UnicodeStringE, @function
_ZNK6icu_6710UnicodeSet12containsNoneERKNS_13UnicodeStringE:
.LFB2440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzwl	8(%rsi), %eax
	movq	%rsi, %rbx
	testw	%ax, %ax
	js	.L904
	movswl	%ax, %edx
	sarl	$5, %edx
	testb	$17, %al
	jne	.L912
.L919:
	leaq	10(%rbx), %r12
	testb	$2, %al
	je	.L917
.L906:
	testl	%edx, %edx
	jle	.L908
	movq	40(%rdi), %r8
	testq	%r8, %r8
	je	.L908
	movslq	%edx, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r8, %rdi
	leaq	(%r12,%rdx,2), %rdx
	call	_ZNK6icu_676BMPSet4spanEPKDsS2_17USetSpanCondition@PLT
	movswl	8(%rbx), %edx
	subq	%r12, %rax
	sarq	%rax
	testw	%dx, %dx
	jns	.L918
.L910:
	movl	12(%rbx), %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmpl	%eax, %edx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition.part.0
	movswl	8(%rbx), %edx
	testw	%dx, %dx
	js	.L910
.L918:
	sarl	$5, %edx
	popq	%rbx
	popq	%r12
	cmpl	%eax, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L917:
	.cfi_restore_state
	movq	24(%rbx), %r12
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L904:
	movl	12(%rsi), %edx
	testb	$17, %al
	je	.L919
.L912:
	xorl	%r12d, %r12d
	jmp	.L906
	.cfi_endproc
.LFE2440:
	.size	_ZNK6icu_6710UnicodeSet12containsNoneERKNS_13UnicodeStringE, .-_ZNK6icu_6710UnicodeSet12containsNoneERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet9matchRestERKNS_11ReplaceableEiiRKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet9matchRestERKNS_11ReplaceableEiiRKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet9matchRestERKNS_11ReplaceableEiiRKNS_13UnicodeStringE:
.LFB2443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movswl	8(%rcx), %eax
	testw	%ax, %ax
	js	.L921
	sarl	$5, %eax
.L922:
	cmpl	%edx, %r13d
	jge	.L923
	subl	%r13d, %edx
	cmpl	%eax, %edx
	cmovle	%edx, %eax
	movl	%eax, -52(%rbp)
	cmpl	$1, %eax
	jle	.L920
	movl	$2, %r15d
	movl	$1, %r12d
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L943:
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L926:
	movl	$-1, %esi
	cmpl	%r12d, %ecx
	jbe	.L927
	andl	$2, %edx
	leaq	10(%rbx), %rdx
	jne	.L929
	movq	24(%rbx), %rdx
.L929:
	movzwl	(%rdx,%r15), %esi
.L927:
	cmpw	%ax, %si
	jne	.L937
	addl	$1, %r12d
	addq	$2, %r15
	cmpl	%r12d, -52(%rbp)
	je	.L920
.L931:
	movq	(%r14), %rax
	leal	0(%r13,%r12), %esi
	movq	%r14, %rdi
	call	*72(%rax)
	movzwl	8(%rbx), %edx
	testw	%dx, %dx
	jns	.L943
	movl	12(%rbx), %ecx
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L937:
	movl	$0, -52(%rbp)
.L920:
	movl	-52(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	movl	%r13d, %edi
	subl	%edx, %edi
	movl	%edi, %edx
	cmpl	%eax, %edi
	cmovg	%eax, %edx
	movl	%edx, %edi
	movl	%edx, -52(%rbp)
	leal	-1(%rax), %edx
	cmpl	$1, %edi
	jle	.L920
	movl	%r13d, %ecx
	leal	-1(%r13), %r12d
	movslq	%edx, %rdx
	notl	%r13d
	subl	%edi, %ecx
	leaq	-2(%rdx,%rdx), %r15
	addl	%eax, %r13d
	movl	%ecx, -56(%rbp)
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L944:
	movswl	%dx, %esi
	sarl	$5, %esi
.L933:
	leal	0(%r13,%r12), %r9d
	movl	$-1, %r8d
	cmpl	%r9d, %esi
	jbe	.L934
	andl	$2, %edx
	leaq	10(%rbx), %rdx
	jne	.L936
	movq	24(%rbx), %rdx
.L936:
	movzwl	(%rdx,%r15), %r8d
.L934:
	cmpw	%ax, %r8w
	jne	.L937
	subl	$1, %r12d
	subq	$2, %r15
	cmpl	-56(%rbp), %r12d
	je	.L920
.L938:
	movq	(%r14), %rax
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*72(%rax)
	movzwl	8(%rbx), %edx
	testw	%dx, %dx
	jns	.L944
	movl	12(%rbx), %esi
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L921:
	movl	12(%rcx), %eax
	jmp	.L922
	.cfi_endproc
.LFE2443:
	.size	_ZN6icu_6710UnicodeSet9matchRestERKNS_11ReplaceableEiiRKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet9matchRestERKNS_11ReplaceableEiiRKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia
	.type	_ZN6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia, @function
_ZN6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia:
.LFB2442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %eax
	movl	%r8d, -56(%rbp)
	movb	%r8b, -57(%rbp)
	movl	%eax, -52(%rbp)
	cmpl	%ecx, %eax
	je	.L993
	movq	80(%rdi), %rax
	movq	%rsi, %r14
	movq	%rdx, %r13
	movl	%ecx, %r15d
	testq	%rax, %rax
	je	.L948
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.L948
	movl	-52(%rbp), %esi
	movq	(%r14), %rax
	movq	%r14, %rdi
	cmpl	%r15d, %esi
	setl	-58(%rbp)
	call	*72(%rax)
	movq	80(%r12), %rdi
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jle	.L948
	xorl	%ebx, %ebx
	movq	%r14, -80(%rbp)
	movl	$0, -64(%rbp)
	movl	%ebx, %r14d
	movl	%eax, %ebx
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L996:
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	testw	%dx, %dx
	js	.L953
.L997:
	movswl	%dx, %edi
	sarl	$5, %edi
.L954:
	movl	$-1, %esi
	cmpl	%r9d, %edi
	jbe	.L955
	andl	$2, %edx
	leaq	10(%rcx), %rdx
	jne	.L957
	movq	24(%rcx), %rdx
.L957:
	cltq
	movzwl	(%rdx,%rax,2), %esi
.L955:
	cmpw	%si, %bx
	jnb	.L974
	cmpb	$0, -58(%rbp)
	jne	.L958
.L974:
	cmpw	%si, %bx
	je	.L994
.L960:
	movq	80(%r12), %rdi
	addl	$1, %r14d
	cmpl	8(%rdi), %r14d
	jge	.L995
.L968:
	movl	%r14d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movzwl	8(%rax), %edx
	movq	%rax, %rcx
	cmpl	%r15d, -52(%rbp)
	jl	.L996
	testw	%dx, %dx
	js	.L951
	movswl	%dx, %eax
	sarl	$5, %eax
.L952:
	subl	$1, %eax
	movl	%eax, %r9d
	testw	%dx, %dx
	jns	.L997
.L953:
	movl	12(%rcx), %edi
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L994:
	movl	0(%r13), %esi
	movq	-80(%rbp), %rdi
	movl	%r15d, %edx
	movq	%rcx, -72(%rbp)
	call	_ZN6icu_6710UnicodeSet9matchRestERKNS_11ReplaceableEiiRKNS_13UnicodeStringE
	cmpb	$0, -57(%rbp)
	movq	-72(%rbp), %rcx
	je	.L961
	movl	0(%r13), %edx
	movl	%r15d, %esi
	subl	%edx, %esi
	subl	%r15d, %edx
	cmpl	%r15d, -52(%rbp)
	cmovl	%esi, %edx
	cmpl	%eax, %edx
	je	.L964
.L961:
	movswl	8(%rcx), %edx
	testw	%dx, %dx
	js	.L965
	sarl	$5, %edx
.L966:
	cmpl	%edx, %eax
	jne	.L960
	movl	-64(%rbp), %esi
	cmpl	%eax, %esi
	cmovl	%eax, %esi
	movl	%esi, -64(%rbp)
	cmpl	%eax, %esi
	jle	.L960
	cmpb	$0, -58(%rbp)
	je	.L960
.L958:
	movl	-64(%rbp), %eax
	movq	-80(%rbp), %r14
	testl	%eax, %eax
	jne	.L998
.L948:
	movsbl	-56(%rbp), %r8d
	addq	$40, %rsp
	movl	%r15d, %ecx
	movq	%r13, %rdx
	popq	%rbx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia@PLT
	.p2align 4,,10
	.p2align 3
.L951:
	.cfi_restore_state
	movl	12(%rax), %eax
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L965:
	movl	12(%rcx), %edx
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L993:
	movl	$65535, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L945
	cmpb	$0, -56(%rbp)
	movl	$2, %eax
	je	.L945
.L964:
	movl	$1, %eax
.L945:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L995:
	.cfi_restore_state
	movl	-64(%rbp), %ebx
	movq	-80(%rbp), %r14
	testl	%ebx, %ebx
	je	.L948
	movl	0(%r13), %eax
	cmpl	%r15d, -52(%rbp)
	jl	.L969
	negl	%ebx
	movl	%ebx, -64(%rbp)
.L969:
	addl	-64(%rbp), %eax
	movl	%eax, 0(%r13)
	movl	$2, %eax
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L998:
	movl	0(%r13), %eax
	jmp	.L969
	.cfi_endproc
.LFE2442:
	.size	_ZN6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia, .-_ZN6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia
	.set	.LTHUNK3,_ZN6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia
	.p2align 4
	.globl	_ZThn8_N6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia
	.type	_ZThn8_N6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia, @function
_ZThn8_N6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia:
.LFB3114:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK3
	.cfi_endproc
.LFE3114:
	.size	_ZThn8_N6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia, .-_ZThn8_N6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet7indexOfEi
	.type	_ZNK6icu_6710UnicodeSet7indexOfEi, @function
_ZNK6icu_6710UnicodeSet7indexOfEi:
.LFB2445:
	.cfi_startproc
	endbr64
	cmpl	$1114111, %esi
	ja	.L1004
	movq	16(%rdi), %rdx
	movl	(%rdx), %ecx
	cmpl	%ecx, %esi
	jl	.L1004
	addq	$4, %rdx
	xorl	%edi, %edi
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1001:
	addq	$8, %rdx
	subl	%ecx, %eax
	movl	-4(%rdx), %ecx
	addl	%eax, %edi
	cmpl	%ecx, %esi
	jl	.L1004
.L1002:
	movl	(%rdx), %eax
	cmpl	%eax, %esi
	jge	.L1001
	leal	(%rsi,%rdi), %eax
	subl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1004:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2445:
	.size	_ZNK6icu_6710UnicodeSet7indexOfEi, .-_ZNK6icu_6710UnicodeSet7indexOfEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet6charAtEi
	.type	_ZNK6icu_6710UnicodeSet6charAtEi, @function
_ZNK6icu_6710UnicodeSet6charAtEi:
.LFB2446:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L1011
	movl	28(%rdi), %edx
	andl	$-2, %edx
	jle	.L1011
	movq	16(%rdi), %rax
	subl	$1, %edx
	shrl	%edx
	leaq	8(%rax,%rdx,8), %rdi
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1008:
	addq	$8, %rax
	subl	%edx, %esi
	cmpq	%rdi, %rax
	je	.L1011
.L1009:
	movl	(%rax), %ecx
	movl	4(%rax), %edx
	subl	%ecx, %edx
	cmpl	%esi, %edx
	jle	.L1008
	leal	(%rcx,%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1011:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2446:
	.size	_ZNK6icu_6710UnicodeSet6charAtEi, .-_ZNK6icu_6710UnicodeSet6charAtEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet4_addERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet4_addERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet4_addERKNS_13UnicodeStringE:
.LFB2451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 40(%rdi)
	je	.L1042
.L1013:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1043
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1042:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	movq	%rdi, %rbx
	jne	.L1013
	testb	$1, 32(%rdi)
	jne	.L1013
	cmpq	$0, 80(%rdi)
	movl	$0, -44(%rbp)
	movq	%rsi, %r12
	je	.L1044
.L1015:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1017
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	80(%rbx), %rdi
	leaq	-44(%rbp), %rcx
	movq	%r13, %rsi
	leaq	_ZN6icu_67L20compareUnicodeStringE8UElementS0_(%rip), %rdx
	call	_ZN6icu_677UVector12sortedInsertEPvPFa8UElementS2_ER10UErrorCode@PLT
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L1013
	cmpq	$0, 40(%rbx)
	je	.L1045
.L1026:
	movb	$1, 32(%rbx)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1044:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1016
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	leaq	-44(%rbp), %r8
	movq	%rax, %rdi
	movl	$1, %ecx
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movl	-44(%rbp), %edx
	movq	%r13, 80(%rbx)
	testl	%edx, %edx
	jle	.L1015
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorD0Ev@PLT
	movq	$0, 80(%rbx)
.L1017:
	cmpq	$0, 40(%rbx)
	je	.L1046
.L1021:
	movb	$1, 32(%rbx)
	jmp	.L1013
.L1045:
	cmpq	$0, 88(%rbx)
	jne	.L1026
	movq	16(%rbx), %rax
	movq	64(%rbx), %rdi
	movl	$1114112, (%rax)
	movl	$1, 28(%rbx)
	testq	%rdi, %rdi
	je	.L1027
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L1027:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1026
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L1026
.L1046:
	cmpq	$0, 88(%rbx)
	jne	.L1021
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1021
.L1043:
	call	__stack_chk_fail@PLT
.L1016:
	movq	$0, 80(%rbx)
	movl	$7, -44(%rbp)
	jmp	.L1017
	.cfi_endproc
.LFE2451:
	.size	_ZN6icu_6710UnicodeSet4_addERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet4_addERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet13complementAllERKS0_
	.type	_ZN6icu_6710UnicodeSet13complementAllERKS0_, @function
_ZN6icu_6710UnicodeSet13complementAllERKS0_:
.LFB2472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	$0, 40(%rdi)
	je	.L1069
.L1048:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1069:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L1048
	testb	$1, 32(%rdi)
	jne	.L1048
	movl	28(%rdi), %eax
	movl	$1114113, %r13d
	addl	28(%rsi), %eax
	movq	%rsi, %rbx
	cmpl	$1114113, %eax
	movl	%r13d, %edx
	movq	16(%rsi), %r14
	cmovle	%eax, %edx
	cmpl	56(%rdi), %edx
	jle	.L1049
	cmpl	$24, %eax
	jle	.L1070
	cmpl	$2500, %eax
	jg	.L1052
	leal	(%rdx,%rdx,4), %r13d
.L1051:
	movslq	%r13d, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1071
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1056
	call	uprv_free_67@PLT
.L1056:
	movq	%r15, 48(%r12)
	movl	%r13d, 56(%r12)
	.p2align 4,,10
	.p2align 3
.L1049:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia.part.0
.L1055:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1048
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L1048
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1060:
	movl	%r14d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	80(%r12), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1059
	movq	%rax, %rsi
	call	_ZN6icu_677UVector13removeElementEPv@PLT
	testb	%al, %al
	jne	.L1058
.L1059:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet4_addERKNS_13UnicodeStringE
.L1058:
	movq	80(%rbx), %rdi
	addl	$1, %r14d
	cmpl	%r14d, 8(%rdi)
	jg	.L1060
	jmp	.L1048
.L1070:
	leal	25(%rdx), %r13d
	jmp	.L1051
.L1052:
	addl	%edx, %edx
	cmpl	$1114113, %edx
	cmovle	%edx, %r13d
	jmp	.L1051
.L1071:
	cmpq	$0, 40(%r12)
	je	.L1072
.L1054:
	movb	$1, 32(%r12)
	jmp	.L1055
.L1072:
	cmpq	$0, 88(%r12)
	jne	.L1054
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1054
	.cfi_endproc
.LFE2472:
	.size	_ZN6icu_6710UnicodeSet13complementAllERKS0_, .-_ZN6icu_6710UnicodeSet13complementAllERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet11getSingleCPERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet11getSingleCPERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet11getSingleCPERKNS_13UnicodeStringE:
.LFB2452:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L1074
	movswl	%dx, %eax
	sarl	$5, %eax
.L1075:
	cmpl	$2, %eax
	jg	.L1082
	cmpl	$1, %eax
	jne	.L1077
	andl	$2, %edx
	jne	.L1085
	movq	24(%rdi), %rdi
	movzwl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1085:
	addq	$10, %rdi
	movzwl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1077:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	jle	.L1086
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1074:
	.cfi_restore 6
	movl	12(%rdi), %eax
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1086:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	$-1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1082:
	.cfi_restore 6
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2452:
	.size	_ZN6icu_6710UnicodeSet11getSingleCPERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet11getSingleCPERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet6retainEi
	.type	_ZN6icu_6710UnicodeSet6retainEi, @function
_ZN6icu_6710UnicodeSet6retainEi:
.LFB2461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	movl	$1114111, %eax
	movl	$1114112, -44(%rbp)
	cmovg	%eax, %esi
	movl	$0, %eax
	testl	%esi, %esi
	cmovs	%eax, %esi
	movl	%esi, -52(%rbp)
	addl	$1, %esi
	cmpq	$0, 40(%rdi)
	movl	%esi, -48(%rbp)
	je	.L1098
.L1088:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1099
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1098:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L1088
	testb	$1, 32(%rdi)
	jne	.L1088
	movl	28(%rdi), %edx
	movl	$1114113, %ebx
	leal	2(%rdx), %eax
	cmpl	$1114113, %eax
	cmovg	%ebx, %eax
	cmpl	56(%rdi), %eax
	jle	.L1089
	cmpl	$22, %edx
	jle	.L1100
	cmpl	$2498, %edx
	jg	.L1092
	leal	(%rax,%rax,4), %ebx
.L1091:
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1101
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1095
	call	uprv_free_67@PLT
.L1095:
	movq	%r13, 48(%r12)
	movl	%ebx, 56(%r12)
.L1089:
	leaq	-52(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
	jmp	.L1088
.L1100:
	leal	25(%rax), %ebx
	jmp	.L1091
.L1092:
	addl	%eax, %eax
	cmpl	$1114113, %eax
	cmovle	%eax, %ebx
	jmp	.L1091
.L1099:
	call	__stack_chk_fail@PLT
.L1101:
	cmpq	$0, 40(%r12)
	je	.L1102
.L1094:
	movb	$1, 32(%r12)
	jmp	.L1088
.L1102:
	cmpq	$0, 88(%r12)
	jne	.L1094
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1094
	.cfi_endproc
.LFE2461:
	.size	_ZN6icu_6710UnicodeSet6retainEi, .-_ZN6icu_6710UnicodeSet6retainEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet6removeEi
	.type	_ZN6icu_6710UnicodeSet6removeEi, @function
_ZN6icu_6710UnicodeSet6removeEi:
.LFB2463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	movl	$1114111, %eax
	movl	$1114112, -44(%rbp)
	cmovg	%eax, %esi
	movl	$0, %eax
	testl	%esi, %esi
	cmovs	%eax, %esi
	movl	%esi, -52(%rbp)
	addl	$1, %esi
	cmpq	$0, 40(%rdi)
	movl	%esi, -48(%rbp)
	je	.L1114
.L1104:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1115
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1114:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L1104
	testb	$1, 32(%rdi)
	jne	.L1104
	movl	28(%rdi), %edx
	movl	$1114113, %ebx
	leal	2(%rdx), %eax
	cmpl	$1114113, %eax
	cmovg	%ebx, %eax
	cmpl	56(%rdi), %eax
	jle	.L1105
	cmpl	$22, %edx
	jle	.L1116
	cmpl	$2498, %edx
	jg	.L1108
	leal	(%rax,%rax,4), %ebx
.L1107:
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1117
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1111
	call	uprv_free_67@PLT
.L1111:
	movq	%r13, 48(%r12)
	movl	%ebx, 56(%r12)
.L1105:
	leaq	-52(%rbp), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
	jmp	.L1104
.L1116:
	leal	25(%rax), %ebx
	jmp	.L1107
.L1108:
	addl	%eax, %eax
	cmpl	$1114113, %eax
	cmovle	%eax, %ebx
	jmp	.L1107
.L1115:
	call	__stack_chk_fail@PLT
.L1117:
	cmpq	$0, 40(%r12)
	je	.L1118
.L1110:
	movb	$1, 32(%r12)
	jmp	.L1104
.L1118:
	cmpq	$0, 88(%r12)
	jne	.L1110
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1110
	.cfi_endproc
.LFE2463:
	.size	_ZN6icu_6710UnicodeSet6removeEi, .-_ZN6icu_6710UnicodeSet6removeEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet9getStringEi
	.type	_ZNK6icu_6710UnicodeSet9getStringEi, @function
_ZNK6icu_6710UnicodeSet9getStringEi:
.LFB2477:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rdi
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.cfi_endproc
.LFE2477:
	.size	_ZNK6icu_6710UnicodeSet9getStringEi, .-_ZNK6icu_6710UnicodeSet9getStringEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet9serializeEPtiR10UErrorCode
	.type	_ZNK6icu_6710UnicodeSet9serializeEPtiR10UErrorCode, @function
_ZNK6icu_6710UnicodeSet9serializeEPtiR10UErrorCode:
.LFB2482:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r9d
	xorl	%r8d, %r8d
	testl	%r9d, %r9d
	jg	.L1169
	testl	%edx, %edx
	js	.L1122
	jle	.L1123
	testq	%rsi, %rsi
	jne	.L1123
.L1122:
	movl	$1, (%rcx)
	xorl	%r8d, %r8d
.L1169:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1123:
	movl	28(%rdi), %r8d
	movl	%r8d, %r9d
	subl	$1, %r9d
	je	.L1171
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%r9d, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	16(%rdi), %rdi
	cmpl	$65535, -4(%rdi,%rax,4)
	jle	.L1126
	cmpl	$65535, (%rdi)
	jle	.L1127
	leal	(%r9,%r9), %r10d
	xorl	%r9d, %r9d
.L1128:
	cmpl	$32767, %r10d
	jg	.L1146
	cmpl	%r10d, %r9d
	jge	.L1172
	leal	2(%r10), %r8d
	cmpl	%edx, %r8d
	jg	.L1150
	movl	%r10d, %eax
	movw	%r9w, 2(%rsi)
	leaq	2(%rsi), %r11
	leaq	4(%rsi), %rdx
	orw	$-32768, %ax
	movw	%ax, (%rsi)
	testl	%r9d, %r9d
	je	.L1136
.L1148:
	leal	-1(%r9), %esi
	cmpl	$6, %esi
	jbe	.L1154
	movl	%r9d, %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1139:
	movdqu	(%rdi,%rax,2), %xmm0
	movdqu	16(%rdi,%rax,2), %xmm5
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm5, %xmm0
	punpckhwd	%xmm5, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpcklwd	%xmm2, %xmm0
	movups	%xmm0, 2(%r11,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1139
	movl	%r9d, %eax
	andl	$-8, %eax
	movl	%eax, %r11d
	leaq	(%rdx,%r11,2), %rcx
	leaq	(%rdi,%r11,4), %r11
	cmpl	%eax, %r9d
	je	.L1141
.L1137:
	movl	(%r11), %ebx
	movw	%bx, (%rcx)
	leal	1(%rax), %ebx
	cmpl	%r9d, %ebx
	jge	.L1141
	movl	4(%r11), %ebx
	movw	%bx, 2(%rcx)
	leal	2(%rax), %ebx
	cmpl	%r9d, %ebx
	jge	.L1141
	movl	8(%r11), %ebx
	movw	%bx, 4(%rcx)
	leal	3(%rax), %ebx
	cmpl	%r9d, %ebx
	jge	.L1141
	movl	12(%r11), %ebx
	movw	%bx, 6(%rcx)
	leal	4(%rax), %ebx
	cmpl	%r9d, %ebx
	jge	.L1141
	movl	16(%r11), %ebx
	movw	%bx, 8(%rcx)
	leal	5(%rax), %ebx
	cmpl	%r9d, %ebx
	jge	.L1141
	movl	20(%r11), %ebx
	addl	$6, %eax
	movw	%bx, 10(%rcx)
	cmpl	%r9d, %eax
	jge	.L1141
	movl	24(%r11), %eax
	movw	%ax, 12(%rcx)
.L1141:
	movl	%esi, %eax
	addq	$1, %rax
	leaq	(%rdi,%rax,4), %rdi
	leaq	(%rdx,%rax,2), %rdx
.L1140:
	cmpl	%r10d, %r9d
	jge	.L1120
.L1136:
	leal	-1(%r10), %eax
	subl	%r9d, %eax
	movl	%eax, %esi
	shrl	%esi
	addl	$1, %esi
	cmpl	$13, %eax
	jbe	.L1142
	movl	%esi, %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	salq	$5, %rcx
	.p2align 4,,10
	.p2align 3
.L1144:
	movdqu	16(%rdi,%rax), %xmm2
	movdqu	(%rdi,%rax), %xmm1
	movdqa	%xmm2, %xmm4
	movdqa	%xmm1, %xmm0
	psrad	$16, %xmm4
	psrad	$16, %xmm0
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm4, %xmm0
	punpckhwd	%xmm4, %xmm3
	movdqa	%xmm0, %xmm4
	punpckhwd	%xmm3, %xmm4
	punpcklwd	%xmm3, %xmm0
	movdqa	%xmm1, %xmm3
	punpcklwd	%xmm2, %xmm1
	punpckhwd	%xmm2, %xmm3
	punpcklwd	%xmm4, %xmm0
	movdqa	%xmm1, %xmm2
	punpcklwd	%xmm3, %xmm1
	punpckhwd	%xmm3, %xmm2
	punpcklwd	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm2
	punpckhwd	%xmm1, %xmm0
	movups	%xmm2, (%rdx,%rax)
	movups	%xmm0, 16(%rdx,%rax)
	addq	$32, %rax
	cmpq	%rcx, %rax
	jne	.L1144
	movl	%esi, %ecx
	andl	$-8, %ecx
	movl	%ecx, %eax
	leal	(%r9,%rcx,2), %r9d
	salq	$2, %rax
	addq	%rax, %rdx
	addq	%rax, %rdi
	cmpl	%ecx, %esi
	je	.L1120
.L1142:
	movl	(%rdi), %eax
	movl	%eax, %ecx
	movw	%ax, 2(%rdx)
	leal	2(%r9), %eax
	sarl	$16, %ecx
	movw	%cx, (%rdx)
	cmpl	%r10d, %eax
	jge	.L1120
	movl	4(%rdi), %eax
	movl	%eax, %ecx
	movw	%ax, 6(%rdx)
	leal	4(%r9), %eax
	sarl	$16, %ecx
	movw	%cx, 4(%rdx)
	cmpl	%r10d, %eax
	jge	.L1120
	movl	8(%rdi), %eax
	movl	%eax, %ecx
	movw	%ax, 10(%rdx)
	leal	6(%r9), %eax
	sarl	$16, %ecx
	movw	%cx, 8(%rdx)
	cmpl	%r10d, %eax
	jge	.L1120
	movl	12(%rdi), %eax
	movl	%eax, %ecx
	movw	%ax, 14(%rdx)
	leal	8(%r9), %eax
	sarl	$16, %ecx
	movw	%cx, 12(%rdx)
	cmpl	%r10d, %eax
	jge	.L1120
	movl	16(%rdi), %eax
	movl	%eax, %ecx
	movw	%ax, 18(%rdx)
	leal	10(%r9), %eax
	sarl	$16, %ecx
	movw	%cx, 16(%rdx)
	cmpl	%r10d, %eax
	jge	.L1120
	movl	20(%rdi), %eax
	addl	$12, %r9d
	movl	%eax, %ecx
	movw	%ax, 22(%rdx)
	sarl	$16, %ecx
	movw	%cx, 20(%rdx)
	cmpl	%r10d, %r9d
	jge	.L1120
	movl	24(%rdi), %eax
	movl	%eax, %ecx
	movw	%ax, 26(%rdx)
	sarl	$16, %ecx
	movw	%cx, 24(%rdx)
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1171:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	testl	%edx, %edx
	je	.L1125
	xorl	%eax, %eax
	movl	$1, %r8d
	movw	%ax, (%rsi)
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1126:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	%r9d, %r10d
	cmpl	$32767, %r9d
	jg	.L1146
.L1147:
	cmpl	%r8d, %edx
	jl	.L1150
	movw	%r10w, (%rsi)
	leaq	2(%rsi), %rdx
	testl	%r9d, %r9d
	jle	.L1155
	movq	%rsi, %r11
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1150:
	movl	$15, (%rcx)
.L1120:
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1146:
	.cfi_restore_state
	movl	$8, (%rcx)
	xorl	%r8d, %r8d
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1125:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$15, (%rcx)
	movl	$1, %r8d
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1127:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	testl	%r9d, %r9d
	jle	.L1173
	subl	$2, %r8d
	movl	$1, %eax
	addq	$1, %r8
	.p2align 4,,10
	.p2align 3
.L1131:
	movl	%eax, %r10d
	cmpq	%r8, %rax
	je	.L1174
	addq	$1, %rax
	cmpl	$65535, -4(%rdi,%rax,4)
	jle	.L1131
	subl	%r10d, %r9d
	movl	%r9d, %r8d
	movl	%r10d, %r9d
.L1129:
	leal	(%r9,%r8,2), %r10d
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1174:
	subl	%eax, %r9d
	movl	%r9d, %r8d
	movl	%eax, %r9d
	jmp	.L1129
.L1154:
	movq	%rdi, %r11
	movq	%rdx, %rcx
	xorl	%eax, %eax
	jmp	.L1137
.L1155:
	xorl	%r9d, %r9d
	jmp	.L1140
.L1173:
	movl	%r9d, %r8d
	xorl	%r9d, %r9d
	jmp	.L1129
.L1172:
	leal	1(%r10), %r8d
	jmp	.L1147
	.cfi_endproc
.LFE2482:
	.size	_ZNK6icu_6710UnicodeSet9serializeEPtiR10UErrorCode, .-_ZNK6icu_6710UnicodeSet9serializeEPtiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet15allocateStringsER10UErrorCode
	.type	_ZN6icu_6710UnicodeSet15allocateStringsER10UErrorCode, @function
_ZN6icu_6710UnicodeSet15allocateStringsER10UErrorCode:
.LFB2483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rsi), %edx
	testl	%edx, %edx
	jle	.L1181
.L1175:
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1181:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	$40, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1177
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r8
	movl	$1, %ecx
	movl	$1, %r13d
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movl	(%rbx), %eax
	movq	%r14, 80(%r12)
	testl	%eax, %eax
	jle	.L1175
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	_ZN6icu_677UVectorD0Ev@PLT
	movl	%r13d, %eax
	movq	$0, 80(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1177:
	.cfi_restore_state
	movq	$0, 80(%r12)
	movl	$7, (%rbx)
	jmp	.L1175
	.cfi_endproc
.LFE2483:
	.size	_ZN6icu_6710UnicodeSet15allocateStringsER10UErrorCode, .-_ZN6icu_6710UnicodeSet15allocateStringsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet12nextCapacityEi
	.type	_ZN6icu_6710UnicodeSet12nextCapacityEi, @function
_ZN6icu_6710UnicodeSet12nextCapacityEi:
.LFB2484:
	.cfi_startproc
	endbr64
	cmpl	$24, %edi
	jle	.L1186
	cmpl	$2500, %edi
	jg	.L1185
	leal	(%rdi,%rdi,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1185:
	addl	%edi, %edi
	movl	$1114113, %eax
	cmpl	$1114113, %edi
	cmovle	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1186:
	leal	25(%rdi), %eax
	ret
	.cfi_endproc
.LFE2484:
	.size	_ZN6icu_6710UnicodeSet12nextCapacityEi, .-_ZN6icu_6710UnicodeSet12nextCapacityEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	.type	_ZN6icu_6710UnicodeSet14ensureCapacityEi, @function
_ZN6icu_6710UnicodeSet14ensureCapacityEi:
.LFB2485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$1114113, %esi
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	$1114113, %r13d
	movl	%r13d, %eax
	pushq	%r12
	cmovle	%esi, %eax
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	%eax, 24(%rdi)
	jge	.L1187
	movq	%rdi, %rbx
	cmpl	$24, %esi
	jle	.L1197
	cmpl	$2500, %esi
	jg	.L1191
	leal	(%rax,%rax,4), %r13d
.L1190:
	movslq	%r13d, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1198
	movq	16(%rbx), %r14
	movslq	28(%rbx), %rdx
	movq	%rax, %rdi
	salq	$2, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	leaq	96(%rbx), %rax
	cmpq	%rax, %r14
	je	.L1194
	movq	%r14, %rdi
	call	uprv_free_67@PLT
.L1194:
	movq	%r12, 16(%rbx)
	movl	$1, %r8d
	movl	%r13d, 24(%rbx)
.L1187:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1197:
	.cfi_restore_state
	leal	25(%rax), %r13d
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1191:
	addl	%eax, %eax
	cmpl	$1114113, %eax
	cmovle	%eax, %r13d
	jmp	.L1190
.L1198:
	cmpq	$0, 40(%rbx)
	je	.L1199
.L1193:
	movb	$1, 32(%rbx)
	xorl	%r8d, %r8d
	jmp	.L1187
.L1199:
	cmpq	$0, 88(%rbx)
	jne	.L1193
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1193
	.cfi_endproc
.LFE2485:
	.size	_ZN6icu_6710UnicodeSet14ensureCapacityEi, .-_ZN6icu_6710UnicodeSet14ensureCapacityEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetC2EPKtiNS0_14ESerializationER10UErrorCode
	.type	_ZN6icu_6710UnicodeSetC2EPKtiNS0_14ESerializationER10UErrorCode, @function
_ZN6icu_6710UnicodeSetC2EPKtiNS0_14ESerializationER10UErrorCode:
.LFB2480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movq	%rax, %xmm6
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-280(%rax), %rdi
	leaq	96(%rbx), %rax
	movq	%rdi, %xmm0
	punpcklqdq	%xmm6, %xmm0
	subq	$24, %rsp
	movq	%rax, 16(%rbx)
	movabsq	$4294967321, %rax
	movq	%rax, 24(%rbx)
	movl	(%r8), %eax
	movups	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	movb	$0, 32(%rbx)
	movl	$0, 56(%rbx)
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
	movups	%xmm0, 40(%rbx)
	movups	%xmm0, 80(%rbx)
	testl	%eax, %eax
	jg	.L1227
	testq	%rsi, %rsi
	movq	%rsi, %r12
	sete	%sil
	testl	%edx, %edx
	setle	%al
	orb	%al, %sil
	jne	.L1221
	testl	%ecx, %ecx
	jne	.L1221
	movzwl	(%r12), %r13d
	movl	$1, %r15d
	movl	%r13d, %eax
	testw	%r13w, %r13w
	jns	.L1206
	movzwl	2(%r12), %r13d
	movl	$2, %r15d
.L1206:
	andl	$32767, %eax
	movq	%rbx, %rdi
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	leal	0(%r13,%rcx), %r14d
	movl	%ecx, -52(%rbp)
	leal	1(%r14), %esi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1200
	movslq	%r13d, %rsi
	movq	16(%rbx), %rdx
	movl	-52(%rbp), %ecx
	salq	$2, %rsi
	testl	%r13d, %r13d
	je	.L1228
	leal	-1(%r13), %eax
	cmpl	$6, %eax
	jbe	.L1219
	movl	%r13d, %edi
	movslq	%r15d, %rax
	pxor	%xmm1, %xmm1
	shrl	$3, %edi
	leaq	(%r12,%rax,2), %r8
	xorl	%eax, %eax
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1211:
	movdqu	(%r8,%rax), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm0
	punpcklwd	%xmm1, %xmm2
	movups	%xmm0, 16(%rdx,%rax,2)
	movups	%xmm2, (%rdx,%rax,2)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L1211
	movl	%r13d, %eax
	andl	$-8, %eax
	testb	$7, %r13b
	je	.L1212
.L1210:
	leal	(%r15,%rax), %edi
	movslq	%eax, %r8
	movslq	%edi, %rdi
	movzwl	(%r12,%rdi,2), %edi
	movl	%edi, (%rdx,%r8,4)
	leal	1(%rax), %edi
	cmpl	%edi, %r13d
	jle	.L1212
	movslq	%edi, %r8
	addl	%r15d, %edi
	movslq	%edi, %rdi
	movzwl	(%r12,%rdi,2), %edi
	movl	%edi, (%rdx,%r8,4)
	leal	2(%rax), %edi
	cmpl	%edi, %r13d
	jle	.L1212
	movslq	%edi, %r8
	addl	%r15d, %edi
	movslq	%edi, %rdi
	movzwl	(%r12,%rdi,2), %edi
	movl	%edi, (%rdx,%r8,4)
	leal	3(%rax), %edi
	cmpl	%edi, %r13d
	jle	.L1212
	movslq	%edi, %r8
	addl	%r15d, %edi
	movslq	%edi, %rdi
	movzwl	(%r12,%rdi,2), %edi
	movl	%edi, (%rdx,%r8,4)
	leal	4(%rax), %edi
	cmpl	%edi, %r13d
	jle	.L1212
	movslq	%edi, %r8
	addl	%r15d, %edi
	movslq	%edi, %rdi
	movzwl	(%r12,%rdi,2), %edi
	movl	%edi, (%rdx,%r8,4)
	leal	5(%rax), %edi
	cmpl	%edi, %r13d
	jle	.L1212
	movslq	%edi, %r8
	addl	%r15d, %edi
	addl	$6, %eax
	movslq	%edi, %rdi
	movzwl	(%r12,%rdi,2), %edi
	movl	%edi, (%rdx,%r8,4)
	cmpl	%eax, %r13d
	jle	.L1212
	movslq	%eax, %rdi
	addl	%r15d, %eax
	cltq
	movzwl	(%r12,%rax,2), %eax
	movl	%eax, (%rdx,%rdi,4)
.L1212:
	cmpl	%r14d, %r13d
	jge	.L1216
.L1209:
	movl	%r13d, %r8d
	cmpl	%r14d, %r13d
	leal	0(%r13,%r15), %edi
	movl	$1, %eax
	notl	%r8d
	cmovge	%eax, %ecx
	addl	%r14d, %r8d
	cmpl	$7, %r8d
	jbe	.L1220
	cmpl	%r14d, %r13d
	jge	.L1220
	subl	$1, %ecx
	movslq	%edi, %rax
	pxor	%xmm3, %xmm3
	addq	%rdx, %rsi
	movl	%ecx, %r11d
	leaq	(%r12,%rax,2), %r10
	leaq	2(%r12,%rax,2), %r9
	xorl	%eax, %eax
	shrl	$3, %r11d
	salq	$5, %r11
	.p2align 4,,10
	.p2align 3
.L1215:
	movdqu	(%r10,%rax), %xmm0
	movdqu	16(%r10,%rax), %xmm5
	movdqu	16(%r9,%rax), %xmm7
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm5, %xmm0
	punpckhwd	%xmm5, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	movdqu	(%r9,%rax), %xmm1
	punpcklwd	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	punpcklwd	%xmm7, %xmm1
	punpckhwd	%xmm7, %xmm2
	movdqa	%xmm1, %xmm4
	punpckhwd	%xmm2, %xmm4
	punpcklwd	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm4, %xmm1
	punpcklwd	%xmm3, %xmm2
	punpckhwd	%xmm3, %xmm0
	pslld	$16, %xmm2
	pslld	$16, %xmm0
	movdqa	%xmm1, %xmm4
	punpcklwd	%xmm3, %xmm4
	punpckhwd	%xmm3, %xmm1
	paddd	%xmm4, %xmm2
	paddd	%xmm1, %xmm0
	movups	%xmm2, (%rsi,%rax)
	movups	%xmm0, 16(%rsi,%rax)
	addq	$32, %rax
	cmpq	%rax, %r11
	jne	.L1215
	andl	$-8, %ecx
	leal	(%rcx,%r13), %eax
.L1213:
	movl	%eax, %ecx
	movslq	%eax, %r9
	subl	%r13d, %ecx
	leal	(%rdi,%rcx,2), %esi
	movslq	%esi, %rsi
	movzwl	(%r12,%rsi,2), %ecx
	movzwl	2(%r12,%rsi,2), %esi
	sall	$16, %ecx
	addl	%esi, %ecx
	movl	%ecx, (%rdx,%r9,4)
	leal	1(%rax), %ecx
	cmpl	%ecx, %r14d
	jle	.L1217
	movl	%ecx, %esi
	movslq	%ecx, %rcx
	subl	%r13d, %esi
	leal	(%rdi,%rsi,2), %r9d
	movslq	%r9d, %r9
	movzwl	(%r12,%r9,2), %esi
	movzwl	2(%r12,%r9,2), %r9d
	sall	$16, %esi
	addl	%r9d, %esi
	movl	%esi, (%rdx,%rcx,4)
	leal	2(%rax), %ecx
	cmpl	%ecx, %r14d
	jle	.L1217
	movl	%ecx, %esi
	movslq	%ecx, %rcx
	subl	%r13d, %esi
	leal	(%rdi,%rsi,2), %r9d
	movslq	%r9d, %r9
	movzwl	(%r12,%r9,2), %esi
	movzwl	2(%r12,%r9,2), %r9d
	sall	$16, %esi
	addl	%r9d, %esi
	movl	%esi, (%rdx,%rcx,4)
	leal	3(%rax), %ecx
	cmpl	%ecx, %r14d
	jle	.L1217
	movl	%ecx, %esi
	movslq	%ecx, %rcx
	subl	%r13d, %esi
	leal	(%rdi,%rsi,2), %r9d
	movslq	%r9d, %r9
	movzwl	(%r12,%r9,2), %esi
	movzwl	2(%r12,%r9,2), %r9d
	sall	$16, %esi
	addl	%r9d, %esi
	movl	%esi, (%rdx,%rcx,4)
	leal	4(%rax), %ecx
	cmpl	%ecx, %r14d
	jle	.L1217
	movl	%ecx, %esi
	movslq	%ecx, %rcx
	subl	%r13d, %esi
	leal	(%rdi,%rsi,2), %r9d
	movslq	%r9d, %r9
	movzwl	(%r12,%r9,2), %esi
	movzwl	2(%r12,%r9,2), %r9d
	sall	$16, %esi
	addl	%r9d, %esi
	movl	%esi, (%rdx,%rcx,4)
	leal	5(%rax), %ecx
	cmpl	%ecx, %r14d
	jle	.L1217
	movl	%ecx, %esi
	movslq	%ecx, %rcx
	subl	%r13d, %esi
	leal	(%rdi,%rsi,2), %r9d
	movslq	%r9d, %r9
	movzwl	(%r12,%r9,2), %esi
	movzwl	2(%r12,%r9,2), %r9d
	sall	$16, %esi
	addl	%r9d, %esi
	movl	%esi, (%rdx,%rcx,4)
	leal	6(%rax), %ecx
	cmpl	%ecx, %r14d
	jle	.L1217
	movl	%ecx, %esi
	addl	$7, %eax
	movslq	%ecx, %rcx
	subl	%r13d, %esi
	leal	(%rdi,%rsi,2), %r9d
	movslq	%r9d, %r9
	movzwl	(%r12,%r9,2), %esi
	movzwl	2(%r12,%r9,2), %r9d
	sall	$16, %esi
	addl	%r9d, %esi
	movl	%esi, (%rdx,%rcx,4)
	cmpl	%eax, %r14d
	jle	.L1217
	movl	%eax, %ecx
	cltq
	subl	%r13d, %ecx
	leal	(%rdi,%rcx,2), %ecx
	movslq	%ecx, %rcx
	movzwl	2(%r12,%rcx,2), %esi
	movzwl	(%r12,%rcx,2), %ecx
	sall	$16, %ecx
	addl	%esi, %ecx
	movl	%ecx, (%rdx,%rax,4)
.L1217:
	cmpl	%r14d, %r13d
	movl	$0, %eax
	cmovge	%eax, %r8d
	leal	1(%r13,%r8), %r13d
	movslq	%r13d, %rsi
	salq	$2, %rsi
.L1216:
	cmpl	$1114112, -4(%rdx,%rsi)
	je	.L1218
.L1208:
	movl	$1114112, (%rdx,%rsi)
	addl	$1, %r13d
.L1218:
	movl	%r13d, 28(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1221:
	.cfi_restore_state
	movl	$1, (%r8)
.L1227:
	movl	$1114112, 96(%rbx)
	movb	$1, 32(%rbx)
.L1200:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1228:
	.cfi_restore_state
	testl	%r14d, %r14d
	jle	.L1208
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1219:
	xorl	%eax, %eax
	jmp	.L1210
.L1220:
	movl	%r13d, %eax
	jmp	.L1213
	.cfi_endproc
.LFE2480:
	.size	_ZN6icu_6710UnicodeSetC2EPKtiNS0_14ESerializationER10UErrorCode, .-_ZN6icu_6710UnicodeSetC2EPKtiNS0_14ESerializationER10UErrorCode
	.globl	_ZN6icu_6710UnicodeSetC1EPKtiNS0_14ESerializationER10UErrorCode
	.set	_ZN6icu_6710UnicodeSetC1EPKtiNS0_14ESerializationER10UErrorCode,_ZN6icu_6710UnicodeSetC2EPKtiNS0_14ESerializationER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet3addEi
	.type	_ZN6icu_6710UnicodeSet3addEi, @function
_ZN6icu_6710UnicodeSet3addEi:
.LFB2449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$0, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1114111, %ebx
	subq	$8, %rsp
	cmpl	$1114111, %esi
	movq	16(%rdi), %rdx
	cmovle	%esi, %ebx
	testl	%ebx, %ebx
	cmovs	%eax, %ebx
	cmpl	(%rdx), %ebx
	jl	.L1242
	movl	28(%rdi), %eax
	leal	-1(%rax), %r12d
	testl	%r12d, %r12d
	jle	.L1231
	movslq	%r12d, %rax
	cmpl	-4(%rdx,%rax,4), %ebx
	jge	.L1231
	xorl	%ecx, %ecx
.L1233:
	movl	%r12d, %eax
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1258:
	movslq	%eax, %rdi
	cmpl	(%rdx,%rdi,4), %ebx
	jge	.L1257
.L1232:
	movl	%eax, %r12d
	leal	(%rax,%rcx), %eax
	sarl	%eax
	cmpl	%ecx, %eax
	jne	.L1258
.L1231:
	testb	$1, %r12b
	je	.L1230
.L1234:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1242:
	.cfi_restore_state
	xorl	%r12d, %r12d
.L1230:
	cmpq	$0, 40(%r13)
	jne	.L1234
	cmpq	$0, 88(%r13)
	jne	.L1234
	testb	$1, 32(%r13)
	jne	.L1234
	movslq	%r12d, %r15
	leaq	0(,%r15,4), %r14
	leaq	(%rdx,%r14), %rcx
	movl	(%rcx), %eax
	subl	$1, %eax
	cmpl	%ebx, %eax
	je	.L1259
	testl	%r12d, %r12d
	jle	.L1241
	leaq	-4(%rdx,%r14), %rax
	cmpl	%ebx, (%rax)
	je	.L1260
.L1241:
	movl	28(%r13), %eax
	movq	%r13, %rdi
	leal	2(%rax), %esi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1234
	movl	28(%r13), %edx
	movq	16(%r13), %rsi
	subl	%r12d, %edx
	addq	%r14, %rsi
	movslq	%edx, %rdx
	leaq	8(%rsi), %rdi
	salq	$2, %rdx
	call	memmove@PLT
	movq	16(%r13), %rax
	movl	%ebx, (%rax,%r15,4)
	addl	$1, %ebx
	movl	%ebx, 4(%rax,%r14)
	addl	$2, 28(%r13)
.L1238:
	movq	64(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1234
	call	uprv_free_67@PLT
	movq	$0, 64(%r13)
	movl	$0, 72(%r13)
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1257:
	movl	%eax, %ecx
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1259:
	movl	%ebx, (%rcx)
	cmpl	$1114110, %esi
	jg	.L1261
.L1237:
	testl	%r12d, %r12d
	jle	.L1238
	leaq	-4(%rdx,%r14), %rdi
	cmpl	%ebx, (%rdi)
	jne	.L1238
	movslq	28(%r13), %rax
	leaq	8(%rdi), %rsi
	leaq	(%rdx,%rax,4), %rax
	cmpq	%rax, %rsi
	jnb	.L1240
	subq	$9, %rax
	subq	%rdi, %rax
	shrq	$2, %rax
	leaq	4(,%rax,4), %rdx
	call	memmove@PLT
.L1240:
	subl	$2, 28(%r13)
	jmp	.L1238
.L1260:
	addl	$1, %ebx
	movl	%ebx, (%rax)
	jmp	.L1238
.L1261:
	movl	28(%r13), %eax
	movq	%r13, %rdi
	leal	1(%rax), %esi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1234
	movslq	28(%r13), %rax
	movq	16(%r13), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 28(%r13)
	movl	$1114112, (%rdx,%rax,4)
	jmp	.L1237
	.cfi_endproc
.LFE2449:
	.size	_ZN6icu_6710UnicodeSet3addEi, .-_ZN6icu_6710UnicodeSet3addEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE:
.LFB2450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movzwl	8(%rsi), %edx
	movq	%rdi, %r12
	testw	%dx, %dx
	js	.L1263
	movswl	%dx, %eax
	sarl	$5, %eax
.L1264:
	testl	%eax, %eax
	je	.L1277
	cmpq	$0, 40(%r12)
	je	.L1279
.L1277:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1263:
	.cfi_restore_state
	movl	12(%rsi), %eax
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1279:
	cmpq	$0, 88(%r12)
	jne	.L1277
	testb	$1, 32(%r12)
	jne	.L1277
	cmpl	$2, %eax
	jg	.L1275
	cmpl	$1, %eax
	jne	.L1271
	andl	$2, %edx
	je	.L1272
	addq	$10, %r13
.L1273:
	movzwl	0(%r13), %esi
.L1274:
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1271:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %esi
	cmpl	$65535, %eax
	jg	.L1274
.L1275:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1270
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	jns	.L1277
.L1270:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6710UnicodeSet4_addERKNS_13UnicodeStringE
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1277
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	jmp	.L1277
.L1272:
	movq	24(%r13), %r13
	jmp	.L1273
	.cfi_endproc
.LFE2450:
	.size	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet10createFromERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet10createFromERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet10createFromERKNS_13UnicodeStringE:
.LFB2458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$200, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1280
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, 32(%r12)
	leaq	-280(%rax), %rcx
	movq	%rax, %xmm1
	leaq	96(%r12), %rax
	movl	$1114112, 96(%r12)
	movq	%rcx, %xmm0
	movq	%rax, 16(%r12)
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, (%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 40(%r12)
	movups	%xmm0, 80(%r12)
	movzwl	8(%r13), %edx
	movl	$0, 56(%r12)
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	testw	%dx, %dx
	js	.L1283
	movswl	%dx, %eax
	sarl	$5, %eax
.L1284:
	testl	%eax, %eax
	je	.L1280
	cmpl	$2, %eax
	jg	.L1293
	cmpl	$1, %eax
	jne	.L1289
	andl	$2, %edx
	je	.L1290
	addq	$10, %r13
.L1291:
	movzwl	0(%r13), %esi
.L1292:
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi
.L1280:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1283:
	.cfi_restore_state
	movl	12(%r13), %eax
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1289:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %esi
	cmpl	$65535, %eax
	jg	.L1292
.L1293:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1288
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	jns	.L1280
.L1288:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6710UnicodeSet4_addERKNS_13UnicodeStringE
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1280
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	jmp	.L1280
.L1290:
	movq	24(%r13), %r13
	jmp	.L1291
	.cfi_endproc
.LFE2458:
	.size	_ZN6icu_6710UnicodeSet10createFromERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet10createFromERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet10complementEv
	.type	_ZN6icu_6710UnicodeSet10complementEv, @function
_ZN6icu_6710UnicodeSet10complementEv:
.LFB2467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpq	$0, 40(%rdi)
	je	.L1312
.L1301:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1312:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L1301
	testb	$1, 32(%rdi)
	jne	.L1301
	movq	16(%rdi), %rdi
	movl	28(%r12), %esi
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L1313
	addl	$1, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1301
	movq	16(%r12), %rsi
	movslq	28(%r12), %rdx
	leaq	4(%rsi), %rdi
	salq	$2, %rdx
	call	memmove@PLT
	movq	16(%r12), %rax
	movl	$0, (%rax)
	addl	$1, 28(%r12)
.L1304:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1301
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1313:
	leal	-1(%rsi), %edx
	leaq	4(%rdi), %rsi
	movslq	%edx, %rdx
	salq	$2, %rdx
	call	memmove@PLT
	subl	$1, 28(%r12)
	jmp	.L1304
	.cfi_endproc
.LFE2467:
	.size	_ZN6icu_6710UnicodeSet10complementEv, .-_ZN6icu_6710UnicodeSet10complementEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet3addEii
	.type	_ZN6icu_6710UnicodeSet3addEii, @function
_ZN6icu_6710UnicodeSet3addEii:
.LFB2448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1114111, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	cmovg	%ecx, %esi
	testl	%esi, %esi
	cmovs	%eax, %esi
	cmpl	$1114111, %edx
	cmovg	%ecx, %edx
	testl	%edx, %edx
	cmovs	%eax, %edx
	cmpl	%edx, %esi
	jge	.L1315
	movslq	28(%rdi), %rax
	addl	$1, %edx
	testb	$1, %al
	je	.L1316
	cmpl	$1, %eax
	je	.L1337
	movq	16(%rdi), %rdi
	movslq	%eax, %rcx
	movl	-8(%rdi,%rcx,4), %ecx
	cmpl	%ecx, %esi
	jge	.L1317
.L1316:
	cmpq	$0, 40(%r12)
	movl	$1114112, -44(%rbp)
	movl	%esi, -52(%rbp)
	movl	%edx, -48(%rbp)
	je	.L1352
.L1327:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1353
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1315:
	.cfi_restore_state
	jne	.L1327
	call	_ZN6icu_6710UnicodeSet3addEi
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1337:
	movl	$-2, %ecx
.L1317:
	cmpq	$0, 40(%r12)
	jne	.L1327
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1354
.L1320:
	movl	$1114112, -44(%rbp)
	movl	%esi, -52(%rbp)
	movl	%edx, -48(%rbp)
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1352:
	movq	88(%r12), %rdi
.L1335:
	testq	%rdi, %rdi
	jne	.L1327
	testb	$1, 32(%r12)
	jne	.L1327
	leal	2(%rax), %edx
	movl	$1114113, %ebx
	cmpl	$1114113, %edx
	cmovg	%ebx, %edx
	cmpl	56(%r12), %edx
	jle	.L1328
	cmpl	$22, %eax
	jle	.L1355
	cmpl	$2498, %eax
	jg	.L1331
	leal	(%rdx,%rdx,4), %ebx
.L1330:
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1356
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1334
	call	uprv_free_67@PLT
.L1334:
	movq	%r13, 48(%r12)
	movl	%ebx, 56(%r12)
.L1328:
	leaq	-52(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEPKiia.part.0
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1354:
	testb	$1, 32(%r12)
	jne	.L1320
	movq	16(%r12), %rdi
	salq	$2, %rax
	cmpl	%ecx, %esi
	je	.L1357
	cmpl	$1114112, %edx
	movl	%esi, -4(%rdi,%rax)
	movl	28(%r12), %eax
	movl	%edx, -68(%rbp)
	je	.L1324
	leal	2(%rax), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1323
	movslq	28(%r12), %rax
	movq	16(%r12), %rcx
	movl	-68(%rbp), %edx
	leal	1(%rax), %esi
	movl	%esi, 28(%r12)
	movl	%edx, (%rcx,%rax,4)
	movslq	28(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 28(%r12)
	movl	$1114112, (%rcx,%rax,4)
.L1323:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1327
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	jmp	.L1327
.L1355:
	leal	25(%rdx), %ebx
	jmp	.L1330
.L1331:
	addl	%edx, %edx
	cmpl	$1114113, %edx
	cmovle	%edx, %ebx
	jmp	.L1330
.L1324:
	leal	1(%rax), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1323
	movslq	28(%r12), %rax
	movq	16(%r12), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 28(%r12)
	movl	$1114112, (%rdx,%rax,4)
	jmp	.L1323
.L1357:
	movl	%edx, -8(%rdi,%rax)
	cmpl	$1114112, %edx
	jne	.L1323
	subl	$1, 28(%r12)
	jmp	.L1323
.L1353:
	call	__stack_chk_fail@PLT
.L1356:
	cmpq	$0, 40(%r12)
	je	.L1358
.L1333:
	movb	$1, 32(%r12)
	jmp	.L1327
.L1358:
	cmpq	$0, 88(%r12)
	jne	.L1333
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1333
	.cfi_endproc
.LFE2448:
	.size	_ZN6icu_6710UnicodeSet3addEii, .-_ZN6icu_6710UnicodeSet3addEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet8copyFromERKS0_a
	.type	_ZN6icu_6710UnicodeSet8copyFromERKS0_a, @function
_ZN6icu_6710UnicodeSet8copyFromERKS0_a:
.LFB2425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	cmpq	%rsi, %rdi
	je	.L1361
	cmpq	$0, 40(%rdi)
	je	.L1376
.L1361:
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1376:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L1361
	movq	%rsi, %r13
	testb	$1, 32(%rsi)
	jne	.L1377
	movl	28(%rsi), %esi
	movl	%edx, -20(%rbp)
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1361
	movl	-20(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsbl	%dl, %edx
	call	_ZN6icu_6710UnicodeSet8copyFromERKS0_a.part.0
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1377:
	movq	16(%rdi), %rax
	movl	$1114112, (%rax)
	movl	$1, 28(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1364
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
.L1364:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1365
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
.L1365:
	movb	$1, 32(%r12)
	jmp	.L1361
	.cfi_endproc
.LFE2425:
	.size	_ZN6icu_6710UnicodeSet8copyFromERKS0_a, .-_ZN6icu_6710UnicodeSet8copyFromERKS0_a
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetaSERKS0_
	.type	_ZN6icu_6710UnicodeSetaSERKS0_, @function
_ZN6icu_6710UnicodeSetaSERKS0_:
.LFB2424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	cmpq	%rsi, %rdi
	je	.L1380
	cmpq	$0, 40(%rdi)
	je	.L1395
.L1380:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1395:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L1380
	movq	%rsi, %r13
	testb	$1, 32(%rsi)
	jne	.L1396
	movl	28(%rsi), %esi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1380
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet8copyFromERKS0_a.part.0
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	16(%rdi), %rax
	movl	$1114112, (%rax)
	movl	$1, 28(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1383
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
.L1383:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1384
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
.L1384:
	movb	$1, 32(%r12)
	jmp	.L1380
	.cfi_endproc
.LFE2424:
	.size	_ZN6icu_6710UnicodeSetaSERKS0_, .-_ZN6icu_6710UnicodeSetaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet5cloneEv
	.type	_ZNK6icu_6710UnicodeSet5cloneEv, @function
_ZNK6icu_6710UnicodeSet5cloneEv:
.LFB2426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$200, %edi
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1397
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, 32(%r12)
	leaq	-280(%rax), %rcx
	movq	%rax, %xmm1
	leaq	96(%r12), %rax
	movl	$0, 56(%r12)
	movq	%rcx, %xmm0
	movq	%rax, 16(%r12)
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, (%r12)
	pxor	%xmm0, %xmm0
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	movups	%xmm0, 40(%r12)
	movups	%xmm0, 80(%r12)
	cmpq	%r13, %r12
	je	.L1397
	testb	$1, 32(%r13)
	jne	.L1406
	movl	28(%r13), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1397
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet8copyFromERKS0_a.part.0
.L1397:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1406:
	.cfi_restore_state
	movl	$1114112, 96(%r12)
	movb	$1, 32(%r12)
	jmp	.L1397
	.cfi_endproc
.LFE2426:
	.size	_ZNK6icu_6710UnicodeSet5cloneEv, .-_ZNK6icu_6710UnicodeSet5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetC2ERKS0_
	.type	_ZN6icu_6710UnicodeSetC2ERKS0_, @function
_ZN6icu_6710UnicodeSetC2ERKS0_:
.LFB2415:
	.cfi_startproc
	endbr64
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, 32(%rdi)
	leaq	-280(%rax), %rcx
	movq	%rax, %xmm1
	leaq	96(%rdi), %rax
	movl	$0, 56(%rdi)
	movq	%rcx, %xmm0
	movq	%rax, 16(%rdi)
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%rdi)
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 64(%rdi)
	movl	$0, 72(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 80(%rdi)
	cmpq	%rsi, %rdi
	je	.L1416
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	testb	$1, 32(%rsi)
	jne	.L1419
	movl	28(%rsi), %esi
	movq	%rdi, -24(%rbp)
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1407
	movq	-24(%rbp), %rdi
	addq	$24, %rsp
	movq	%r12, %rsi
	xorl	%edx, %edx
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet8copyFromERKS0_a.part.0
	.p2align 4,,10
	.p2align 3
.L1416:
	ret
	.p2align 4,,10
	.p2align 3
.L1419:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$1114112, 96(%rdi)
	movb	$1, 32(%rdi)
.L1407:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2415:
	.size	_ZN6icu_6710UnicodeSetC2ERKS0_, .-_ZN6icu_6710UnicodeSetC2ERKS0_
	.globl	_ZN6icu_6710UnicodeSetC1ERKS0_
	.set	_ZN6icu_6710UnicodeSetC1ERKS0_,_ZN6icu_6710UnicodeSetC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetC2Eii
	.type	_ZN6icu_6710UnicodeSetC2Eii, @function
_ZN6icu_6710UnicodeSetC2Eii:
.LFB2403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$1114111, %esi
	movl	$1114111, %ecx
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	cmovg	%ecx, %esi
	movq	%rax, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	-280(%rax), %rbx
	leaq	96(%rdi), %rax
	movq	%rax, 16(%rdi)
	movq	%rbx, %xmm0
	movabsq	$4294967321, %rax
	movq	%rax, 24(%rdi)
	xorl	%eax, %eax
	testl	%esi, %esi
	punpcklqdq	%xmm1, %xmm0
	cmovs	%eax, %esi
	cmpl	$1114111, %edx
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	cmovg	%ecx, %edx
	movb	$0, 32(%rdi)
	movl	$0, 56(%rdi)
	testl	%edx, %edx
	movq	$0, 64(%rdi)
	cmovs	%eax, %edx
	movl	$0, 72(%rdi)
	movl	$1114112, 96(%rdi)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 80(%rdi)
	cmpl	%edx, %esi
	jge	.L1421
	movl	%esi, 96(%rdi)
	leal	1(%rdx), %ebx
	cmpl	$1114111, %edx
	jne	.L1435
	movl	$2, %esi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1424
	movslq	28(%r12), %rax
	movq	16(%r12), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 28(%r12)
	movl	$1114112, (%rdx,%rax,4)
.L1424:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1420
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1421:
	je	.L1436
.L1420:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1436:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet3addEi
	.p2align 4,,10
	.p2align 3
.L1435:
	.cfi_restore_state
	movl	$3, %esi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	testb	%al, %al
	je	.L1424
	movslq	28(%r12), %rax
	movq	16(%r12), %rdx
	leal	1(%rax), %ecx
	movl	%ecx, 28(%r12)
	movl	%ebx, (%rdx,%rax,4)
	movslq	28(%r12), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 28(%r12)
	movl	$1114112, (%rdx,%rax,4)
	jmp	.L1424
	.cfi_endproc
.LFE2403:
	.size	_ZN6icu_6710UnicodeSetC2Eii, .-_ZN6icu_6710UnicodeSetC2Eii
	.globl	_ZN6icu_6710UnicodeSetC1Eii
	.set	_ZN6icu_6710UnicodeSetC1Eii,_ZN6icu_6710UnicodeSetC2Eii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet6addAllERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet6addAllERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet6addAllERKNS_13UnicodeStringE:
.LFB2453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L1438
	.p2align 4,,10
	.p2align 3
.L1471:
	sarl	$5, %eax
	cmpl	%eax, %r14d
	jge	.L1440
.L1472:
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	$1114111, %r12d
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	16(%r15), %rdx
	cmpl	$1114111, %eax
	movl	%eax, %r8d
	cmovle	%eax, %r12d
	movl	$0, %eax
	testl	%r12d, %r12d
	cmovs	%eax, %r12d
	cmpl	(%rdx), %r12d
	jl	.L1456
	movl	28(%r15), %eax
	leal	-1(%rax), %ebx
	testl	%ebx, %ebx
	jle	.L1442
	movslq	%ebx, %rax
	cmpl	-4(%rdx,%rax,4), %r12d
	jge	.L1442
	xorl	%esi, %esi
.L1444:
	movl	%ebx, %eax
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1469:
	movslq	%eax, %rdi
	cmpl	(%rdx,%rdi,4), %r12d
	jge	.L1468
.L1443:
	movl	%eax, %ebx
	leal	(%rax,%rsi), %eax
	sarl	%eax
	cmpl	%esi, %eax
	jne	.L1469
.L1442:
	testb	$1, %bl
	jne	.L1445
	cmpq	$0, 40(%r15)
	je	.L1470
.L1445:
	cmpl	$65535, %r8d
	ja	.L1454
	movl	$1, %eax
.L1450:
	addl	%eax, %r14d
	movswl	8(%r13), %eax
	testw	%ax, %ax
	jns	.L1471
.L1438:
	movl	12(%r13), %eax
	cmpl	%eax, %r14d
	jl	.L1472
.L1440:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1454:
	.cfi_restore_state
	movl	$2, %eax
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1456:
	xorl	%ebx, %ebx
	cmpq	$0, 40(%r15)
	jne	.L1445
.L1470:
	cmpq	$0, 88(%r15)
	jne	.L1445
	testb	$1, 32(%r15)
	jne	.L1445
	movslq	%ebx, %r10
	leaq	0(,%r10,4), %r9
	leaq	(%rdx,%r9), %rsi
	movl	(%rsi), %eax
	subl	$1, %eax
	cmpl	%eax, %r12d
	je	.L1473
	testl	%ebx, %ebx
	jle	.L1453
	leaq	-4(%rdx,%r9), %rax
	cmpl	(%rax), %r12d
	je	.L1474
.L1453:
	movl	28(%r15), %eax
	movq	%r15, %rdi
	movl	%r8d, -56(%rbp)
	movq	%r10, -72(%rbp)
	leal	2(%rax), %esi
	movq	%r9, -64(%rbp)
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	movl	-56(%rbp), %r8d
	testb	%al, %al
	je	.L1445
	movl	28(%r15), %edx
	movq	-64(%rbp), %r9
	movl	%r8d, -76(%rbp)
	subl	%ebx, %edx
	movq	%r9, %rsi
	addq	16(%r15), %rsi
	movq	%r9, -56(%rbp)
	movslq	%edx, %rdx
	leaq	8(%rsi), %rdi
	salq	$2, %rdx
	call	memmove@PLT
	movq	16(%r15), %rax
	movq	-72(%rbp), %r10
	movq	-56(%rbp), %r9
	movl	-76(%rbp), %r8d
	movl	%r12d, (%rax,%r10,4)
	addl	$1, %r12d
	movl	%r12d, 4(%rax,%r9)
	addl	$2, 28(%r15)
.L1451:
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1445
	movl	%r8d, -56(%rbp)
	call	uprv_free_67@PLT
	movq	$0, 64(%r15)
	movl	-56(%rbp), %r8d
	movl	$0, 72(%r15)
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1468:
	movl	%eax, %esi
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1473:
	movl	%r12d, (%rsi)
	cmpl	$1114110, %r8d
	jg	.L1475
.L1448:
	testl	%ebx, %ebx
	jle	.L1451
	leaq	-4(%rdx,%r9), %rdi
	cmpl	(%rdi), %r12d
	jne	.L1451
	movslq	28(%r15), %rax
	leaq	8(%rdi), %r9
	movq	%rax, %rsi
	leaq	(%rdx,%rax,4), %rax
	cmpq	%rax, %r9
	jnb	.L1452
	subq	$9, %rax
	movq	%r9, %rsi
	movl	%r8d, -56(%rbp)
	subq	%rdi, %rax
	shrq	$2, %rax
	leaq	4(,%rax,4), %rdx
	call	memmove@PLT
	movl	28(%r15), %esi
	movl	-56(%rbp), %r8d
.L1452:
	subl	$2, %esi
	movl	%esi, 28(%r15)
	jmp	.L1451
.L1474:
	addl	$1, %r12d
	movl	%r12d, (%rax)
	jmp	.L1451
.L1475:
	movl	28(%r15), %eax
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	movl	%r8d, -56(%rbp)
	leal	1(%rax), %esi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	movl	-56(%rbp), %r8d
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L1454
	movslq	28(%r15), %rax
	movq	16(%r15), %rdx
	leal	1(%rax), %esi
	movl	%esi, 28(%r15)
	movl	$1114112, (%rdx,%rax,4)
	jmp	.L1448
	.cfi_endproc
.LFE2453:
	.size	_ZN6icu_6710UnicodeSet6addAllERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet6addAllERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet13complementAllERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet13complementAllERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet13complementAllERKNS_13UnicodeStringE:
.LFB2455:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, -224(%rbp)
	leaq	-280(%rax), %rdx
	movq	%rax, %xmm1
	leaq	-160(%rbp), %rax
	movl	$0, -200(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rdx, %xmm0
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -232(%rbp)
	movswl	8(%rcx), %eax
	movaps	%xmm0, -256(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movl	$1114112, -160(%rbp)
	movups	%xmm0, -216(%rbp)
	movaps	%xmm0, -176(%rbp)
	testw	%ax, %ax
	js	.L1477
	.p2align 4,,10
	.p2align 3
.L1533:
	sarl	$5, %eax
	cmpl	%eax, %r15d
	jge	.L1479
.L1534:
	movq	%rcx, %rdi
	movl	%r15d, %esi
	movq	%rcx, -264(%rbp)
	movl	$1114111, %r12d
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	-240(%rbp), %rdx
	movq	-264(%rbp), %rcx
	cmpl	$1114111, %eax
	movl	%eax, %r9d
	cmovle	%eax, %r12d
	movl	$0, %eax
	testl	%r12d, %r12d
	cmovs	%eax, %r12d
	cmpl	(%rdx), %r12d
	jl	.L1510
	movl	-228(%rbp), %eax
	leal	-1(%rax), %ebx
	testl	%ebx, %ebx
	jle	.L1481
	movslq	%ebx, %rax
	cmpl	-4(%rdx,%rax,4), %r12d
	jge	.L1481
	xorl	%esi, %esi
.L1483:
	movl	%ebx, %eax
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1531:
	movslq	%eax, %rdi
	cmpl	(%rdx,%rdi,4), %r12d
	jge	.L1530
.L1482:
	movl	%eax, %ebx
	leal	(%rax,%rsi), %eax
	sarl	%eax
	cmpl	%esi, %eax
	jne	.L1531
.L1481:
	testb	$1, %bl
	jne	.L1484
.L1480:
	cmpq	$0, -216(%rbp)
	je	.L1532
.L1484:
	cmpl	$65535, %r9d
	ja	.L1493
.L1538:
	movl	$1, %eax
.L1489:
	addl	%eax, %r15d
	movswl	8(%rcx), %eax
	testw	%ax, %ax
	jns	.L1533
.L1477:
	movl	12(%rcx), %eax
	cmpl	%eax, %r15d
	jl	.L1534
.L1479:
	cmpq	$0, 40(%r13)
	je	.L1495
.L1496:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1535
	addq	$264, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1532:
	.cfi_restore_state
	cmpq	$0, -168(%rbp)
	jne	.L1484
	testb	$1, -224(%rbp)
	jne	.L1484
	movslq	%ebx, %r11
	leaq	0(,%r11,4), %r10
	leaq	(%rdx,%r10), %rsi
	movl	(%rsi), %eax
	subl	$1, %eax
	cmpl	%eax, %r12d
	je	.L1536
	testl	%ebx, %ebx
	jle	.L1492
	leaq	-4(%rdx,%r10), %rax
	cmpl	(%rax), %r12d
	je	.L1537
.L1492:
	movl	-228(%rbp), %eax
	movq	%r14, %rdi
	movq	%rcx, -272(%rbp)
	movl	%r9d, -264(%rbp)
	leal	2(%rax), %esi
	movq	%r11, -288(%rbp)
	movq	%r10, -280(%rbp)
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	movl	-264(%rbp), %r9d
	movq	-272(%rbp), %rcx
	testb	%al, %al
	je	.L1484
	movl	-228(%rbp), %edx
	movq	-280(%rbp), %r10
	movq	%rcx, -296(%rbp)
	movl	%r9d, -272(%rbp)
	subl	%ebx, %edx
	movq	%r10, %rsi
	addq	-240(%rbp), %rsi
	movq	%r10, -264(%rbp)
	movslq	%edx, %rdx
	leaq	8(%rsi), %rdi
	salq	$2, %rdx
	call	memmove@PLT
	movq	-240(%rbp), %rax
	movq	-288(%rbp), %r11
	movq	-264(%rbp), %r10
	movq	-296(%rbp), %rcx
	movl	%r12d, (%rax,%r11,4)
	addl	$1, %r12d
	movl	-272(%rbp), %r9d
	movl	%r12d, 4(%rax,%r10)
	addl	$2, -228(%rbp)
.L1490:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1484
	movq	%rcx, -272(%rbp)
	movl	%r9d, -264(%rbp)
	call	uprv_free_67@PLT
	movl	-264(%rbp), %r9d
	movq	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movq	-272(%rbp), %rcx
	cmpl	$65535, %r9d
	jbe	.L1538
	.p2align 4,,10
	.p2align 3
.L1493:
	movl	$2, %eax
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1510:
	xorl	%ebx, %ebx
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1530:
	movl	%eax, %esi
	jmp	.L1483
	.p2align 4,,10
	.p2align 3
.L1495:
	cmpq	$0, 88(%r13)
	jne	.L1496
	testb	$1, 32(%r13)
	jne	.L1496
	movl	28(%r13), %eax
	movl	$1114113, %ebx
	addl	-228(%rbp), %eax
	cmpl	$1114113, %eax
	movl	%ebx, %edx
	movq	-240(%rbp), %r12
	cmovle	%eax, %edx
	cmpl	56(%r13), %edx
	jle	.L1497
	cmpl	$24, %eax
	jle	.L1539
	cmpl	$2500, %eax
	jg	.L1500
	leal	(%rdx,%rdx,4), %ebx
.L1499:
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1540
	movq	48(%r13), %rdi
	leaq	96(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1504
	call	uprv_free_67@PLT
.L1504:
	movq	%r15, 48(%r13)
	movl	%ebx, 56(%r13)
	.p2align 4,,10
	.p2align 3
.L1497:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia.part.0
.L1503:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1496
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L1496
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1508:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	80(%r13), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1507
	movq	%rax, %rsi
	call	_ZN6icu_677UVector13removeElementEPv@PLT
	testb	%al, %al
	jne	.L1506
.L1507:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet4_addERKNS_13UnicodeStringE
.L1506:
	movq	-176(%rbp), %rdi
	addl	$1, %ebx
	cmpl	%ebx, 8(%rdi)
	jg	.L1508
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1536:
	movl	%r12d, (%rsi)
	cmpl	$1114110, %r9d
	jg	.L1541
.L1487:
	testl	%ebx, %ebx
	jle	.L1490
	leaq	-4(%rdx,%r10), %rdi
	cmpl	(%rdi), %r12d
	jne	.L1490
	movslq	-228(%rbp), %rax
	leaq	8(%rdi), %rsi
	movq	%rax, %r10
	leaq	(%rdx,%rax,4), %rax
	cmpq	%rax, %rsi
	jnb	.L1491
	subq	$9, %rax
	movq	%rcx, -272(%rbp)
	subq	%rdi, %rax
	movl	%r9d, -264(%rbp)
	shrq	$2, %rax
	leaq	4(,%rax,4), %rdx
	call	memmove@PLT
	movl	-228(%rbp), %r10d
	movq	-272(%rbp), %rcx
	movl	-264(%rbp), %r9d
.L1491:
	subl	$2, %r10d
	movl	%r10d, -228(%rbp)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1537:
	addl	$1, %r12d
	movl	%r12d, (%rax)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1541:
	movl	-228(%rbp), %eax
	movq	%r14, %rdi
	movq	%rcx, -280(%rbp)
	movq	%r10, -272(%rbp)
	leal	1(%rax), %esi
	movl	%r9d, -264(%rbp)
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	movl	-264(%rbp), %r9d
	movq	-272(%rbp), %r10
	testb	%al, %al
	movq	-280(%rbp), %rcx
	je	.L1493
	movslq	-228(%rbp), %rax
	movq	-240(%rbp), %rdx
	leal	1(%rax), %esi
	movl	%esi, -228(%rbp)
	movl	$1114112, (%rdx,%rax,4)
	jmp	.L1487
.L1500:
	addl	%edx, %edx
	cmpl	$1114113, %edx
	cmovle	%edx, %ebx
	jmp	.L1499
.L1539:
	leal	25(%rdx), %ebx
	jmp	.L1499
.L1535:
	call	__stack_chk_fail@PLT
.L1540:
	cmpq	$0, 40(%r13)
	je	.L1542
.L1502:
	movb	$1, 32(%r13)
	jmp	.L1503
.L1542:
	cmpq	$0, 88(%r13)
	jne	.L1502
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1502
	.cfi_endproc
.LFE2455:
	.size	_ZN6icu_6710UnicodeSet13complementAllERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet13complementAllERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet9retainAllERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet9retainAllERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet9retainAllERKNS_13UnicodeStringE:
.LFB2454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, -224(%rbp)
	leaq	-280(%rax), %rdx
	movq	%rax, %xmm1
	leaq	-160(%rbp), %rax
	movl	$0, -200(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rdx, %xmm0
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -232(%rbp)
	movswl	8(%r15), %eax
	movaps	%xmm0, -256(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movl	$1114112, -160(%rbp)
	movups	%xmm0, -216(%rbp)
	movaps	%xmm0, -176(%rbp)
	testw	%ax, %ax
	js	.L1544
	.p2align 4,,10
	.p2align 3
.L1596:
	sarl	$5, %eax
	cmpl	%eax, %ecx
	jge	.L1546
.L1597:
	movl	%ecx, %esi
	movq	%r15, %rdi
	movl	%ecx, -264(%rbp)
	movl	$1114111, %r12d
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	-240(%rbp), %rdx
	movl	-264(%rbp), %ecx
	cmpl	$1114111, %eax
	movl	%eax, %r9d
	cmovle	%eax, %r12d
	movl	$0, %eax
	testl	%r12d, %r12d
	cmovs	%eax, %r12d
	cmpl	(%rdx), %r12d
	jl	.L1575
	movl	-228(%rbp), %eax
	leal	-1(%rax), %ebx
	testl	%ebx, %ebx
	jle	.L1548
	movslq	%ebx, %rax
	cmpl	-4(%rdx,%rax,4), %r12d
	jge	.L1548
	xorl	%esi, %esi
.L1550:
	movl	%ebx, %eax
	jmp	.L1549
	.p2align 4,,10
	.p2align 3
.L1594:
	movslq	%eax, %rdi
	cmpl	(%rdx,%rdi,4), %r12d
	jge	.L1593
.L1549:
	movl	%eax, %ebx
	leal	(%rax,%rsi), %eax
	sarl	%eax
	cmpl	%esi, %eax
	jne	.L1594
.L1548:
	testb	$1, %bl
	jne	.L1551
.L1547:
	cmpq	$0, -216(%rbp)
	je	.L1595
.L1551:
	cmpl	$65535, %r9d
	ja	.L1560
	movl	$1, %eax
	addl	%eax, %ecx
.L1600:
	movswl	8(%r15), %eax
	testw	%ax, %ax
	jns	.L1596
.L1544:
	movl	12(%r15), %eax
	cmpl	%eax, %ecx
	jl	.L1597
.L1546:
	cmpq	$0, 40(%r13)
	je	.L1598
.L1563:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1599
	addq	$248, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1560:
	.cfi_restore_state
	movl	$2, %eax
	addl	%eax, %ecx
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1595:
	cmpq	$0, -168(%rbp)
	jne	.L1551
	testb	$1, -224(%rbp)
	jne	.L1551
	movslq	%ebx, %r11
	leaq	0(,%r11,4), %r10
	leaq	(%rdx,%r10), %rsi
	movl	(%rsi), %eax
	subl	$1, %eax
	cmpl	%eax, %r12d
	je	.L1601
	testl	%ebx, %ebx
	jle	.L1559
	leaq	-4(%rdx,%r10), %rax
	cmpl	(%rax), %r12d
	je	.L1602
.L1559:
	movl	-228(%rbp), %eax
	movq	%r14, %rdi
	movl	%r9d, -268(%rbp)
	movl	%ecx, -264(%rbp)
	leal	2(%rax), %esi
	movq	%r11, -288(%rbp)
	movq	%r10, -280(%rbp)
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	movl	-264(%rbp), %ecx
	movl	-268(%rbp), %r9d
	testb	%al, %al
	je	.L1551
	movl	-228(%rbp), %edx
	movq	-280(%rbp), %r10
	movl	%r9d, -272(%rbp)
	movl	%ecx, -268(%rbp)
	subl	%ebx, %edx
	movq	%r10, %rsi
	addq	-240(%rbp), %rsi
	movq	%r10, -264(%rbp)
	movslq	%edx, %rdx
	leaq	8(%rsi), %rdi
	salq	$2, %rdx
	call	memmove@PLT
	movq	-240(%rbp), %rax
	movq	-288(%rbp), %r11
	movq	-264(%rbp), %r10
	movl	-272(%rbp), %r9d
	movl	%r12d, (%rax,%r11,4)
	addl	$1, %r12d
	movl	-268(%rbp), %ecx
	movl	%r12d, 4(%rax,%r10)
	addl	$2, -228(%rbp)
.L1557:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1551
	movl	%r9d, -268(%rbp)
	movl	%ecx, -264(%rbp)
	call	uprv_free_67@PLT
	movl	-268(%rbp), %r9d
	movq	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movl	-264(%rbp), %ecx
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1575:
	xorl	%ebx, %ebx
	jmp	.L1547
	.p2align 4,,10
	.p2align 3
.L1593:
	movl	%eax, %esi
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1598:
	cmpq	$0, 88(%r13)
	jne	.L1563
	testb	$1, 32(%r13)
	jne	.L1563
	movl	28(%r13), %eax
	movl	$1114113, %ebx
	addl	-228(%rbp), %eax
	cmpl	$1114113, %eax
	movl	%ebx, %edx
	movq	-240(%rbp), %r12
	cmovle	%eax, %edx
	cmpl	56(%r13), %edx
	jle	.L1565
	cmpl	$24, %eax
	jle	.L1603
	cmpl	$2500, %eax
	jg	.L1568
	leal	(%rdx,%rdx,4), %ebx
.L1567:
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1604
	movq	48(%r13), %rdi
	leaq	96(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1572
	call	uprv_free_67@PLT
.L1572:
	movq	%r15, 48(%r13)
	movl	%ebx, 56(%r13)
	.p2align 4,,10
	.p2align 3
.L1565:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
.L1571:
	movq	80(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1563
	movl	8(%rdi), %edx
	testl	%edx, %edx
	je	.L1563
	movq	-176(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1573
	movl	8(%rsi), %eax
	testl	%eax, %eax
	je	.L1573
	call	_ZN6icu_677UVector9retainAllERKS0_@PLT
	jmp	.L1563
	.p2align 4,,10
	.p2align 3
.L1601:
	movl	%r12d, (%rsi)
	cmpl	$1114110, %r9d
	jg	.L1605
.L1554:
	testl	%ebx, %ebx
	jle	.L1557
	leaq	-4(%rdx,%r10), %rdi
	cmpl	(%rdi), %r12d
	jne	.L1557
	movslq	-228(%rbp), %rax
	leaq	8(%rdi), %rsi
	movq	%rax, %r10
	leaq	(%rdx,%rax,4), %rax
	cmpq	%rax, %rsi
	jnb	.L1558
	subq	$9, %rax
	movl	%r9d, -268(%rbp)
	subq	%rdi, %rax
	movl	%ecx, -264(%rbp)
	shrq	$2, %rax
	leaq	4(,%rax,4), %rdx
	call	memmove@PLT
	movl	-228(%rbp), %r10d
	movl	-268(%rbp), %r9d
	movl	-264(%rbp), %ecx
.L1558:
	subl	$2, %r10d
	movl	%r10d, -228(%rbp)
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1602:
	addl	$1, %r12d
	movl	%r12d, (%rax)
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1605:
	movl	-228(%rbp), %eax
	movq	%r14, %rdi
	movq	%r10, -280(%rbp)
	movl	%r9d, -268(%rbp)
	leal	1(%rax), %esi
	movl	%ecx, -264(%rbp)
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	movl	-264(%rbp), %ecx
	movl	-268(%rbp), %r9d
	testb	%al, %al
	movq	-280(%rbp), %r10
	je	.L1560
	movslq	-228(%rbp), %rax
	movq	-240(%rbp), %rdx
	leal	1(%rax), %esi
	movl	%esi, -228(%rbp)
	movl	$1114112, (%rdx,%rax,4)
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1573:
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L1563
.L1568:
	addl	%edx, %edx
	cmpl	$1114113, %edx
	cmovle	%edx, %ebx
	jmp	.L1567
.L1603:
	leal	25(%rdx), %ebx
	jmp	.L1567
.L1599:
	call	__stack_chk_fail@PLT
.L1604:
	cmpq	$0, 40(%r13)
	je	.L1606
.L1570:
	movb	$1, 32(%r13)
	jmp	.L1571
.L1606:
	cmpq	$0, 88(%r13)
	jne	.L1570
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1570
	.cfi_endproc
.LFE2454:
	.size	_ZN6icu_6710UnicodeSet9retainAllERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet9retainAllERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet9removeAllERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet9removeAllERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet9removeAllERKNS_13UnicodeStringE:
.LFB2456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, -224(%rbp)
	leaq	-280(%rax), %rdx
	movq	%rax, %xmm1
	leaq	-160(%rbp), %rax
	movl	$0, -200(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rdx, %xmm0
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -232(%rbp)
	movswl	8(%r15), %eax
	movaps	%xmm0, -256(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movl	$1114112, -160(%rbp)
	movups	%xmm0, -216(%rbp)
	movaps	%xmm0, -176(%rbp)
	testw	%ax, %ax
	js	.L1608
	.p2align 4,,10
	.p2align 3
.L1659:
	sarl	$5, %eax
	cmpl	%eax, %ecx
	jge	.L1610
.L1660:
	movl	%ecx, %esi
	movq	%r15, %rdi
	movl	%ecx, -264(%rbp)
	movl	$1114111, %r12d
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	-240(%rbp), %rdx
	movl	-264(%rbp), %ecx
	cmpl	$1114111, %eax
	movl	%eax, %r9d
	cmovle	%eax, %r12d
	movl	$0, %eax
	testl	%r12d, %r12d
	cmovs	%eax, %r12d
	cmpl	(%rdx), %r12d
	jl	.L1638
	movl	-228(%rbp), %eax
	leal	-1(%rax), %ebx
	testl	%ebx, %ebx
	jle	.L1612
	movslq	%ebx, %rax
	cmpl	-4(%rdx,%rax,4), %r12d
	jge	.L1612
	xorl	%esi, %esi
.L1614:
	movl	%ebx, %eax
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1657:
	movslq	%eax, %rdi
	cmpl	(%rdx,%rdi,4), %r12d
	jge	.L1656
.L1613:
	movl	%eax, %ebx
	leal	(%rax,%rsi), %eax
	sarl	%eax
	cmpl	%esi, %eax
	jne	.L1657
.L1612:
	testb	$1, %bl
	jne	.L1615
.L1611:
	cmpq	$0, -216(%rbp)
	je	.L1658
.L1615:
	cmpl	$65535, %r9d
	ja	.L1624
	movl	$1, %eax
	addl	%eax, %ecx
.L1663:
	movswl	8(%r15), %eax
	testw	%ax, %ax
	jns	.L1659
.L1608:
	movl	12(%r15), %eax
	cmpl	%eax, %ecx
	jl	.L1660
.L1610:
	cmpq	$0, 40(%r13)
	je	.L1661
.L1627:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1662
	addq	$248, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1624:
	.cfi_restore_state
	movl	$2, %eax
	addl	%eax, %ecx
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1658:
	cmpq	$0, -168(%rbp)
	jne	.L1615
	testb	$1, -224(%rbp)
	jne	.L1615
	movslq	%ebx, %r11
	leaq	0(,%r11,4), %r10
	leaq	(%rdx,%r10), %rsi
	movl	(%rsi), %eax
	subl	$1, %eax
	cmpl	%eax, %r12d
	je	.L1664
	testl	%ebx, %ebx
	jle	.L1623
	leaq	-4(%rdx,%r10), %rax
	cmpl	(%rax), %r12d
	je	.L1665
.L1623:
	movl	-228(%rbp), %eax
	movq	%r14, %rdi
	movl	%r9d, -268(%rbp)
	movl	%ecx, -264(%rbp)
	leal	2(%rax), %esi
	movq	%r11, -288(%rbp)
	movq	%r10, -280(%rbp)
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	movl	-264(%rbp), %ecx
	movl	-268(%rbp), %r9d
	testb	%al, %al
	je	.L1615
	movl	-228(%rbp), %edx
	movq	-280(%rbp), %r10
	movl	%r9d, -272(%rbp)
	movl	%ecx, -268(%rbp)
	subl	%ebx, %edx
	movq	%r10, %rsi
	addq	-240(%rbp), %rsi
	movq	%r10, -264(%rbp)
	movslq	%edx, %rdx
	leaq	8(%rsi), %rdi
	salq	$2, %rdx
	call	memmove@PLT
	movq	-240(%rbp), %rax
	movq	-288(%rbp), %r11
	movq	-264(%rbp), %r10
	movl	-272(%rbp), %r9d
	movl	%r12d, (%rax,%r11,4)
	addl	$1, %r12d
	movl	-268(%rbp), %ecx
	movl	%r12d, 4(%rax,%r10)
	addl	$2, -228(%rbp)
.L1621:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1615
	movl	%r9d, -268(%rbp)
	movl	%ecx, -264(%rbp)
	call	uprv_free_67@PLT
	movl	-268(%rbp), %r9d
	movq	$0, -192(%rbp)
	movl	$0, -184(%rbp)
	movl	-264(%rbp), %ecx
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1638:
	xorl	%ebx, %ebx
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1656:
	movl	%eax, %esi
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1661:
	cmpq	$0, 88(%r13)
	jne	.L1627
	testb	$1, 32(%r13)
	jne	.L1627
	movl	28(%r13), %eax
	movl	$1114113, %ebx
	addl	-228(%rbp), %eax
	cmpl	$1114113, %eax
	movl	%ebx, %edx
	movq	-240(%rbp), %r12
	cmovle	%eax, %edx
	cmpl	56(%r13), %edx
	jle	.L1629
	cmpl	$24, %eax
	jle	.L1666
	cmpl	$2500, %eax
	jg	.L1632
	leal	(%rdx,%rdx,4), %ebx
.L1631:
	movslq	%ebx, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1667
	movq	48(%r13), %rdi
	leaq	96(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1636
	call	uprv_free_67@PLT
.L1636:
	movq	%r15, 48(%r13)
	movl	%ebx, 56(%r13)
	.p2align 4,,10
	.p2align 3
.L1629:
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
.L1635:
	movq	80(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1627
	movl	8(%rdi), %edx
	testl	%edx, %edx
	je	.L1627
	movq	-176(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1627
	movl	8(%rsi), %eax
	testl	%eax, %eax
	je	.L1627
	call	_ZN6icu_677UVector9removeAllERKS0_@PLT
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1664:
	movl	%r12d, (%rsi)
	cmpl	$1114110, %r9d
	jg	.L1668
.L1618:
	testl	%ebx, %ebx
	jle	.L1621
	leaq	-4(%rdx,%r10), %rdi
	cmpl	(%rdi), %r12d
	jne	.L1621
	movslq	-228(%rbp), %rax
	leaq	8(%rdi), %rsi
	movq	%rax, %r10
	leaq	(%rdx,%rax,4), %rax
	cmpq	%rax, %rsi
	jnb	.L1622
	subq	$9, %rax
	movl	%r9d, -268(%rbp)
	subq	%rdi, %rax
	movl	%ecx, -264(%rbp)
	shrq	$2, %rax
	leaq	4(,%rax,4), %rdx
	call	memmove@PLT
	movl	-228(%rbp), %r10d
	movl	-268(%rbp), %r9d
	movl	-264(%rbp), %ecx
.L1622:
	subl	$2, %r10d
	movl	%r10d, -228(%rbp)
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1665:
	addl	$1, %r12d
	movl	%r12d, (%rax)
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1668:
	movl	-228(%rbp), %eax
	movq	%r14, %rdi
	movq	%r10, -280(%rbp)
	movl	%r9d, -268(%rbp)
	leal	1(%rax), %esi
	movl	%ecx, -264(%rbp)
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	movl	-264(%rbp), %ecx
	movl	-268(%rbp), %r9d
	testb	%al, %al
	movq	-280(%rbp), %r10
	je	.L1624
	movslq	-228(%rbp), %rax
	movq	-240(%rbp), %rdx
	leal	1(%rax), %esi
	movl	%esi, -228(%rbp)
	movl	$1114112, (%rdx,%rax,4)
	jmp	.L1618
.L1632:
	addl	%edx, %edx
	cmpl	$1114113, %edx
	cmovle	%edx, %ebx
	jmp	.L1631
.L1666:
	leal	25(%rdx), %ebx
	jmp	.L1631
.L1662:
	call	__stack_chk_fail@PLT
.L1667:
	cmpq	$0, 40(%r13)
	je	.L1669
.L1634:
	movb	$1, 32(%r13)
	jmp	.L1635
.L1669:
	cmpq	$0, 88(%r13)
	jne	.L1634
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1634
	.cfi_endproc
.LFE2456:
	.size	_ZN6icu_6710UnicodeSet9removeAllERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet9removeAllERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet13createFromAllERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet13createFromAllERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet13createFromAllERKNS_13UnicodeStringE:
.LFB2459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$200, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1670
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, 32(%r12)
	xorl	%r15d, %r15d
	leaq	-280(%rax), %rcx
	movq	%rax, %xmm1
	leaq	96(%r12), %rax
	movl	$1114112, 96(%r12)
	movq	%rcx, %xmm0
	movq	%rax, 16(%r12)
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%r12)
	movups	%xmm0, (%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 40(%r12)
	movups	%xmm0, 80(%r12)
	movswl	8(%r14), %eax
	movl	$0, 56(%r12)
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	testw	%ax, %ax
	js	.L1672
	.p2align 4,,10
	.p2align 3
.L1707:
	sarl	$5, %eax
	cmpl	%eax, %r15d
	jge	.L1670
.L1708:
	movl	%r15d, %esi
	movq	%r14, %rdi
	movl	$1114111, %r13d
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	16(%r12), %rdx
	cmpl	$1114111, %eax
	movl	%eax, %r8d
	cmovle	%eax, %r13d
	movl	$0, %eax
	testl	%r13d, %r13d
	cmovs	%eax, %r13d
	cmpl	(%rdx), %r13d
	jl	.L1689
	movl	28(%r12), %eax
	leal	-1(%rax), %ebx
	testl	%ebx, %ebx
	jle	.L1675
	movslq	%ebx, %rax
	cmpl	-4(%rdx,%rax,4), %r13d
	jge	.L1675
	xorl	%esi, %esi
.L1677:
	movl	%ebx, %eax
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1705:
	movslq	%eax, %rdi
	cmpl	(%rdx,%rdi,4), %r13d
	jge	.L1704
.L1676:
	movl	%eax, %ebx
	leal	(%rax,%rsi), %eax
	sarl	%eax
	cmpl	%esi, %eax
	jne	.L1705
.L1675:
	testb	$1, %bl
	jne	.L1678
.L1674:
	cmpq	$0, 40(%r12)
	je	.L1706
.L1678:
	cmpl	$65535, %r8d
	ja	.L1687
.L1711:
	movl	$1, %eax
.L1683:
	addl	%eax, %r15d
	movswl	8(%r14), %eax
	testw	%ax, %ax
	jns	.L1707
.L1672:
	movl	12(%r14), %eax
	cmpl	%eax, %r15d
	jl	.L1708
.L1670:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1706:
	.cfi_restore_state
	cmpq	$0, 88(%r12)
	jne	.L1678
	testb	$1, 32(%r12)
	jne	.L1678
	movslq	%ebx, %r10
	leaq	0(,%r10,4), %r9
	leaq	(%rdx,%r9), %rsi
	movl	(%rsi), %eax
	subl	$1, %eax
	cmpl	%eax, %r13d
	je	.L1709
	testl	%ebx, %ebx
	jle	.L1686
	leaq	-4(%rdx,%r9), %rax
	cmpl	(%rax), %r13d
	je	.L1710
.L1686:
	movl	28(%r12), %eax
	movq	%r12, %rdi
	movl	%r8d, -56(%rbp)
	movq	%r10, -72(%rbp)
	leal	2(%rax), %esi
	movq	%r9, -64(%rbp)
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	movl	-56(%rbp), %r8d
	testb	%al, %al
	je	.L1678
	movl	28(%r12), %edx
	movq	-64(%rbp), %r9
	movl	%r8d, -76(%rbp)
	subl	%ebx, %edx
	movq	%r9, %rsi
	addq	16(%r12), %rsi
	movq	%r9, -56(%rbp)
	movslq	%edx, %rdx
	leaq	8(%rsi), %rdi
	salq	$2, %rdx
	call	memmove@PLT
	movq	16(%r12), %rax
	movq	-72(%rbp), %r10
	movq	-56(%rbp), %r9
	movl	-76(%rbp), %r8d
	movl	%r13d, (%rax,%r10,4)
	addl	$1, %r13d
	movl	%r13d, 4(%rax,%r9)
	addl	$2, 28(%r12)
.L1684:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1678
	movl	%r8d, -56(%rbp)
	call	uprv_free_67@PLT
	movl	-56(%rbp), %r8d
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	cmpl	$65535, %r8d
	jbe	.L1711
	.p2align 4,,10
	.p2align 3
.L1687:
	movl	$2, %eax
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1689:
	xorl	%ebx, %ebx
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1704:
	movl	%eax, %esi
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L1709:
	movl	%r13d, (%rsi)
	cmpl	$1114110, %r8d
	jg	.L1712
.L1681:
	testl	%ebx, %ebx
	jle	.L1684
	leaq	-4(%rdx,%r9), %rdi
	cmpl	(%rdi), %r13d
	jne	.L1684
	movslq	28(%r12), %rax
	leaq	8(%rdi), %r9
	movq	%rax, %rsi
	leaq	(%rdx,%rax,4), %rax
	cmpq	%rax, %r9
	jnb	.L1685
	subq	$9, %rax
	movq	%r9, %rsi
	movl	%r8d, -56(%rbp)
	subq	%rdi, %rax
	shrq	$2, %rax
	leaq	4(,%rax,4), %rdx
	call	memmove@PLT
	movl	28(%r12), %esi
	movl	-56(%rbp), %r8d
.L1685:
	subl	$2, %esi
	movl	%esi, 28(%r12)
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1710:
	addl	$1, %r13d
	movl	%r13d, (%rax)
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1712:
	movl	28(%r12), %eax
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	movl	%r8d, -56(%rbp)
	leal	1(%rax), %esi
	call	_ZN6icu_6710UnicodeSet14ensureCapacityEi
	movl	-56(%rbp), %r8d
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L1687
	movslq	28(%r12), %rax
	movq	16(%r12), %rdx
	leal	1(%rax), %esi
	movl	%esi, 28(%r12)
	movl	$1114112, (%rdx,%rax,4)
	jmp	.L1681
	.cfi_endproc
.LFE2459:
	.size	_ZN6icu_6710UnicodeSet13createFromAllERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet13createFromAllERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet20ensureBufferCapacityEi
	.type	_ZN6icu_6710UnicodeSet20ensureBufferCapacityEi, @function
_ZN6icu_6710UnicodeSet20ensureBufferCapacityEi:
.LFB2486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	$1114113, %r12d
	pushq	%rbx
	movl	%r12d, %eax
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpl	$1114113, %esi
	cmovle	%esi, %eax
	cmpl	%eax, 56(%rdi)
	jge	.L1713
	movq	%rdi, %rbx
	cmpl	$24, %esi
	jle	.L1723
	cmpl	$2500, %esi
	jg	.L1717
	leal	(%rax,%rax,4), %r12d
.L1716:
	movslq	%r12d, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1724
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1720
	call	uprv_free_67@PLT
.L1720:
	movq	%r13, 48(%rbx)
	movl	$1, %r8d
	movl	%r12d, 56(%rbx)
.L1713:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1723:
	.cfi_restore_state
	leal	25(%rax), %r12d
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1717:
	addl	%eax, %eax
	cmpl	$1114113, %eax
	cmovle	%eax, %r12d
	jmp	.L1716
.L1724:
	cmpq	$0, 40(%rbx)
	je	.L1725
.L1719:
	movb	$1, 32(%rbx)
	xorl	%r8d, %r8d
	jmp	.L1713
.L1725:
	cmpq	$0, 88(%rbx)
	jne	.L1719
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1719
	.cfi_endproc
.LFE2486:
	.size	_ZN6icu_6710UnicodeSet20ensureBufferCapacityEi, .-_ZN6icu_6710UnicodeSet20ensureBufferCapacityEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet10complementEii
	.type	_ZN6icu_6710UnicodeSet10complementEii, @function
_ZN6icu_6710UnicodeSet10complementEii:
.LFB2465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 40(%rdi)
	je	.L1740
.L1728:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1741
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1740:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L1728
	testb	$1, 32(%rdi)
	jne	.L1728
	movl	$1114111, %ecx
	cmpl	$1114111, %esi
	cmovg	%ecx, %esi
	testl	%esi, %esi
	cmovs	%eax, %esi
	cmpl	$1114111, %edx
	cmovg	%ecx, %edx
	testl	%edx, %edx
	cmovs	%eax, %edx
	cmpl	%edx, %esi
	jle	.L1742
.L1730:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1728
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L1742:
	movl	28(%rdi), %eax
	addl	$1, %edx
	movl	%esi, -36(%rbp)
	movl	$1114112, -28(%rbp)
	leal	2(%rax), %esi
	movl	%edx, -32(%rbp)
	call	_ZN6icu_6710UnicodeSet20ensureBufferCapacityEi
	testb	%al, %al
	je	.L1730
	leaq	-36(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia.part.0
	jmp	.L1730
.L1741:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2465:
	.size	_ZN6icu_6710UnicodeSet10complementEii, .-_ZN6icu_6710UnicodeSet10complementEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet10complementERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet10complementERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet10complementERKNS_13UnicodeStringE:
.LFB2468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movzwl	8(%rsi), %edx
	movq	%rdi, %r12
	testw	%dx, %dx
	js	.L1744
	movswl	%dx, %eax
	sarl	$5, %eax
.L1745:
	testl	%eax, %eax
	je	.L1759
	cmpq	$0, 40(%r12)
	je	.L1763
.L1759:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1744:
	.cfi_restore_state
	movl	12(%rsi), %eax
	jmp	.L1745
	.p2align 4,,10
	.p2align 3
.L1763:
	cmpq	$0, 88(%r12)
	jne	.L1759
	testb	$1, 32(%r12)
	jne	.L1759
	cmpl	$2, %eax
	jg	.L1756
	cmpl	$1, %eax
	jne	.L1752
	andl	$2, %edx
	je	.L1753
	addq	$10, %r13
.L1754:
	movzwl	0(%r13), %esi
.L1755:
	movl	%esi, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEii
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1752:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %esi
	cmpl	$65535, %eax
	jg	.L1755
.L1756:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1751
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	js	.L1751
	movq	80(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_677UVector13removeElementEPv@PLT
.L1757:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1759
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1751:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet4_addERKNS_13UnicodeStringE
	jmp	.L1757
.L1753:
	movq	24(%r13), %r13
	jmp	.L1754
	.cfi_endproc
.LFE2468:
	.size	_ZN6icu_6710UnicodeSet10complementERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet10complementERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet10complementEi
	.type	_ZN6icu_6710UnicodeSet10complementEi, @function
_ZN6icu_6710UnicodeSet10complementEi:
.LFB2466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 40(%rdi)
	je	.L1777
.L1766:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1778
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1777:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L1766
	testb	$1, 32(%rdi)
	jne	.L1766
	cmpl	$1114111, %esi
	movl	$1114111, %eax
	movl	$1114112, -28(%rbp)
	cmovg	%eax, %esi
	movl	$0, %eax
	testl	%esi, %esi
	cmovs	%eax, %esi
	movl	28(%rdi), %eax
	movl	%esi, -36(%rbp)
	addl	$1, %esi
	movl	%esi, -32(%rbp)
	leal	2(%rax), %esi
	call	_ZN6icu_6710UnicodeSet20ensureBufferCapacityEi
	testb	%al, %al
	je	.L1768
	leaq	-36(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia.part.0
	.p2align 4,,10
	.p2align 3
.L1768:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1766
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	jmp	.L1766
.L1778:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2466:
	.size	_ZN6icu_6710UnicodeSet10complementEi, .-_ZN6icu_6710UnicodeSet10complementEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet3setEii
	.type	_ZN6icu_6710UnicodeSet3setEii, @function
_ZN6icu_6710UnicodeSet3setEii:
.LFB2447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 40(%rdi)
	je	.L1801
.L1781:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1802
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1801:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L1781
	movq	16(%rdi), %rax
	movl	$1114112, (%rax)
	movl	$1, 28(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1783
	movl	%edx, -56(%rbp)
	movl	%esi, -52(%rbp)
	call	uprv_free_67@PLT
	movl	-56(%rbp), %edx
	movl	-52(%rbp), %esi
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
.L1783:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1784
	movl	%edx, -56(%rbp)
	movl	%esi, -52(%rbp)
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	movl	-56(%rbp), %edx
	movl	-52(%rbp), %esi
.L1784:
	cmpq	$0, 40(%r12)
	movb	$0, 32(%r12)
	jne	.L1781
	cmpq	$0, 88(%r12)
	jne	.L1781
	cmpl	$1114111, %esi
	movl	$1114111, %ecx
	cmovg	%ecx, %esi
	xorl	%eax, %eax
	testl	%esi, %esi
	cmovs	%eax, %esi
	cmpl	$1114111, %edx
	cmovg	%ecx, %edx
	testl	%edx, %edx
	cmovs	%eax, %edx
	cmpl	%edx, %esi
	jle	.L1803
.L1785:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1781
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	jmp	.L1781
	.p2align 4,,10
	.p2align 3
.L1803:
	movl	28(%r12), %eax
	addl	$1, %edx
	movl	%esi, -36(%rbp)
	movq	%r12, %rdi
	movl	$1114112, -28(%rbp)
	leal	2(%rax), %esi
	movl	%edx, -32(%rbp)
	call	_ZN6icu_6710UnicodeSet20ensureBufferCapacityEi
	testb	%al, %al
	je	.L1785
	leaq	-36(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia.part.0
	jmp	.L1785
.L1802:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2447:
	.size	_ZN6icu_6710UnicodeSet3setEii, .-_ZN6icu_6710UnicodeSet3setEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet6removeERKNS_13UnicodeStringE
	.type	_ZN6icu_6710UnicodeSet6removeERKNS_13UnicodeStringE, @function
_ZN6icu_6710UnicodeSet6removeERKNS_13UnicodeStringE:
.LFB2464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movzwl	8(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testw	%dx, %dx
	js	.L1805
	movswl	%dx, %eax
	sarl	$5, %eax
.L1806:
	testl	%eax, %eax
	je	.L1808
	cmpq	$0, 40(%r12)
	je	.L1833
.L1808:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1834
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1805:
	.cfi_restore_state
	movl	12(%rsi), %eax
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1833:
	cmpq	$0, 88(%r12)
	jne	.L1808
	testb	$1, 32(%r12)
	jne	.L1808
	cmpl	$2, %eax
	jg	.L1817
	cmpl	$1, %eax
	jne	.L1812
	andl	$2, %edx
	je	.L1813
	addq	$10, %r13
.L1814:
	movzwl	0(%r13), %eax
	movl	$1114112, -28(%rbp)
	movl	%eax, -36(%rbp)
	addl	$1, %eax
	movl	%eax, -32(%rbp)
.L1815:
	cmpq	$0, 88(%r12)
	jne	.L1808
	testb	$1, 32(%r12)
	jne	.L1808
	movl	28(%r12), %eax
	movq	%r12, %rdi
	leal	2(%rax), %esi
	call	_ZN6icu_6710UnicodeSet20ensureBufferCapacityEi
	testb	%al, %al
	je	.L1808
	leaq	-36(%rbp), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1817:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1808
	movq	%r13, %rsi
	call	_ZN6icu_677UVector13removeElementEPv@PLT
	testb	%al, %al
	je	.L1808
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1808
	call	uprv_free_67@PLT
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1812:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpl	$65535, %eax
	jle	.L1817
	cmpl	$1114111, %eax
	movl	$1114111, %edx
	movl	$1114112, -28(%rbp)
	cmovg	%edx, %eax
	movl	%eax, -36(%rbp)
	addl	$1, %eax
	cmpq	$0, 40(%r12)
	movl	%eax, -32(%rbp)
	jne	.L1808
	jmp	.L1815
	.p2align 4,,10
	.p2align 3
.L1813:
	movq	24(%r13), %r13
	jmp	.L1814
.L1834:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2464:
	.size	_ZN6icu_6710UnicodeSet6removeERKNS_13UnicodeStringE, .-_ZN6icu_6710UnicodeSet6removeERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet11swapBuffersEv
	.type	_ZN6icu_6710UnicodeSet11swapBuffersEv, @function
_ZN6icu_6710UnicodeSet11swapBuffersEv:
.LFB2487:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	48(%rdi), %rdx
	movq	%rax, 48(%rdi)
	movl	24(%rdi), %eax
	movq	%rdx, 16(%rdi)
	movl	56(%rdi), %edx
	movl	%eax, 56(%rdi)
	movl	%edx, 24(%rdi)
	ret
	.cfi_endproc
.LFE2487:
	.size	_ZN6icu_6710UnicodeSet11swapBuffersEv, .-_ZN6icu_6710UnicodeSet11swapBuffersEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet10setToBogusEv
	.type	_ZN6icu_6710UnicodeSet10setToBogusEv, @function
_ZN6icu_6710UnicodeSet10setToBogusEv:
.LFB2488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 40(%rdi)
	je	.L1846
.L1837:
	movb	$1, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1846:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L1837
	movq	16(%rdi), %rax
	movl	$1114112, (%rax)
	movl	$1, 28(%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1838
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L1838:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1837
	call	_ZN6icu_677UVector17removeAllElementsEv@PLT
	jmp	.L1837
	.cfi_endproc
.LFE2488:
	.size	_ZN6icu_6710UnicodeSet10setToBogusEv, .-_ZN6icu_6710UnicodeSet10setToBogusEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia
	.type	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia, @function
_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia:
.LFB2490:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	je	.L1860
	ret
	.p2align 4,,10
	.p2align 3
.L1860:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	cmpq	$0, 88(%rdi)
	je	.L1861
.L1847:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1861:
	.cfi_restore_state
	testb	$1, 32(%rdi)
	jne	.L1847
	addl	28(%rdi), %edx
	movl	$1114113, %ebx
	cmpl	$1114113, %edx
	movl	%ebx, %eax
	cmovle	%edx, %eax
	cmpl	56(%rdi), %eax
	jle	.L1849
	cmpl	$24, %edx
	jle	.L1862
	cmpl	$2500, %edx
	jg	.L1852
	leal	(%rax,%rax,4), %ebx
.L1851:
	movslq	%ebx, %rdi
	movl	%ecx, -44(%rbp)
	salq	$2, %rdi
	movq	%rsi, -40(%rbp)
	call	uprv_malloc_67@PLT
	movq	-40(%rbp), %rsi
	movl	-44(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1863
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1855
	movl	%ecx, -44(%rbp)
	movq	%rsi, -40(%rbp)
	call	uprv_free_67@PLT
	movl	-44(%rbp), %ecx
	movq	-40(%rbp), %rsi
.L1855:
	movq	%r13, 48(%r12)
	movl	%ebx, 56(%r12)
.L1849:
	addq	$24, %rsp
	movq	%r12, %rdi
	movsbl	%cl, %edx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia.part.0
.L1862:
	.cfi_restore_state
	leal	25(%rax), %ebx
	jmp	.L1851
.L1852:
	addl	%eax, %eax
	cmpl	$1114113, %eax
	cmovle	%eax, %ebx
	jmp	.L1851
.L1863:
	cmpq	$0, 40(%r12)
	je	.L1864
.L1854:
	movb	$1, 32(%r12)
	jmp	.L1847
.L1864:
	cmpq	$0, 88(%r12)
	jne	.L1854
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1854
	.cfi_endproc
.LFE2490:
	.size	_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia, .-_ZN6icu_6710UnicodeSet11exclusiveOrEPKiia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet3addEPKiia
	.type	_ZN6icu_6710UnicodeSet3addEPKiia, @function
_ZN6icu_6710UnicodeSet3addEPKiia:
.LFB2491:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	je	.L1878
	ret
	.p2align 4,,10
	.p2align 3
.L1878:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	cmpq	$0, 88(%rdi)
	je	.L1879
.L1865:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1879:
	.cfi_restore_state
	testb	$1, 32(%rdi)
	jne	.L1865
	testq	%rsi, %rsi
	je	.L1865
	addl	28(%rdi), %edx
	movl	$1114113, %ebx
	cmpl	$1114113, %edx
	movl	%ebx, %eax
	cmovle	%edx, %eax
	cmpl	56(%rdi), %eax
	jle	.L1867
	cmpl	$24, %edx
	jle	.L1880
	cmpl	$2500, %edx
	jg	.L1870
	leal	(%rax,%rax,4), %ebx
.L1869:
	movslq	%ebx, %rdi
	movl	%ecx, -44(%rbp)
	salq	$2, %rdi
	movq	%rsi, -40(%rbp)
	call	uprv_malloc_67@PLT
	movq	-40(%rbp), %rsi
	movl	-44(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1881
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1873
	movl	%ecx, -44(%rbp)
	movq	%rsi, -40(%rbp)
	call	uprv_free_67@PLT
	movl	-44(%rbp), %ecx
	movq	-40(%rbp), %rsi
.L1873:
	movq	%r13, 48(%r12)
	movl	%ebx, 56(%r12)
.L1867:
	addq	$24, %rsp
	movq	%r12, %rdi
	movsbl	%cl, %edx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet3addEPKiia.part.0
.L1880:
	.cfi_restore_state
	leal	25(%rax), %ebx
	jmp	.L1869
.L1870:
	addl	%eax, %eax
	cmpl	$1114113, %eax
	cmovle	%eax, %ebx
	jmp	.L1869
.L1881:
	cmpq	$0, 40(%r12)
	je	.L1882
.L1872:
	movb	$1, 32(%r12)
	jmp	.L1865
.L1882:
	cmpq	$0, 88(%r12)
	jne	.L1872
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1872
	.cfi_endproc
.LFE2491:
	.size	_ZN6icu_6710UnicodeSet3addEPKiia, .-_ZN6icu_6710UnicodeSet3addEPKiia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet6retainEPKiia
	.type	_ZN6icu_6710UnicodeSet6retainEPKiia, @function
_ZN6icu_6710UnicodeSet6retainEPKiia:
.LFB2492:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	je	.L1896
	ret
	.p2align 4,,10
	.p2align 3
.L1896:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	cmpq	$0, 88(%rdi)
	je	.L1897
.L1883:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1897:
	.cfi_restore_state
	testb	$1, 32(%rdi)
	jne	.L1883
	addl	28(%rdi), %edx
	movl	$1114113, %eax
	cmpl	$1114113, %edx
	movl	%eax, %ebx
	cmovle	%edx, %ebx
	cmpl	56(%rdi), %ebx
	jle	.L1885
	cmpl	$24, %edx
	jle	.L1898
	cmpl	$2500, %edx
	jg	.L1888
	leal	(%rbx,%rbx,4), %ebx
.L1887:
	movslq	%ebx, %rdi
	movl	%ecx, -44(%rbp)
	salq	$2, %rdi
	movq	%rsi, -40(%rbp)
	call	uprv_malloc_67@PLT
	movq	-40(%rbp), %rsi
	movl	-44(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1899
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1891
	movl	%ecx, -44(%rbp)
	movq	%rsi, -40(%rbp)
	call	uprv_free_67@PLT
	movl	-44(%rbp), %ecx
	movq	-40(%rbp), %rsi
.L1891:
	movq	%r13, 48(%r12)
	movl	%ebx, 56(%r12)
.L1885:
	addq	$24, %rsp
	movq	%r12, %rdi
	movsbl	%cl, %edx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet6retainEPKiia.part.0
.L1898:
	.cfi_restore_state
	addl	$25, %ebx
	jmp	.L1887
.L1888:
	addl	%ebx, %ebx
	cmpl	$1114113, %ebx
	cmovg	%eax, %ebx
	jmp	.L1887
.L1899:
	cmpq	$0, 40(%r12)
	je	.L1900
.L1890:
	movb	$1, 32(%r12)
	jmp	.L1883
.L1900:
	cmpq	$0, 88(%r12)
	jne	.L1890
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L1890
	.cfi_endproc
.LFE2492:
	.size	_ZN6icu_6710UnicodeSet6retainEPKiia, .-_ZN6icu_6710UnicodeSet6retainEPKiia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringERKS1_a
	.type	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringERKS1_a, @function
_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringERKS1_a:
.LFB2493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movb	%dl, -65(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-58(%rbp), %rax
	movq	%rax, -80(%rbp)
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L1928:
	sarl	$5, %eax
	cmpl	%eax, %ebx
	jge	.L1901
.L1929:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpb	$0, -65(%rbp)
	movl	%eax, %r15d
	jne	.L1905
.L1911:
	cmpl	$58, %r15d
	jg	.L1906
	cmpl	$35, %r15d
	jg	.L1927
.L1908:
	movl	%r15d, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L1926
.L1913:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L1912:
	xorl	%eax, %eax
	cmpl	$65535, %r15d
	seta	%al
	leal	1(%rbx,%rax), %ebx
.L1915:
	movswl	8(%r12), %eax
	testw	%ax, %ax
	jns	.L1928
	movl	12(%r12), %eax
	cmpl	%eax, %ebx
	jl	.L1929
.L1901:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1930
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1927:
	.cfi_restore_state
	movabsq	$288265904121184256, %rsi
	movq	%r13, %rax
	movl	%r15d, %ecx
	salq	%cl, %rax
	testq	%rsi, %rax
	je	.L1908
.L1926:
	movq	-80(%rbp), %rsi
	movl	$92, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L1906:
	leal	-91(%r15), %ecx
	cmpl	$34, %ecx
	ja	.L1908
	movabsq	$21474836495, %rdx
	movq	%r13, %rax
	salq	%cl, %rax
	testq	%rdx, %rax
	je	.L1908
	jmp	.L1926
	.p2align 4,,10
	.p2align 3
.L1905:
	movl	%eax, %edi
	call	_ZN6icu_6711ICU_Utility13isUnprintableEi@PLT
	testb	%al, %al
	je	.L1911
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi@PLT
	testb	%al, %al
	je	.L1911
	jmp	.L1912
.L1930:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2493:
	.size	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringERKS1_a, .-_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringERKS1_a
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia
	.type	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia, @function
_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia:
.LFB2494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L1932
.L1938:
	cmpl	$58, %r12d
	jg	.L1933
	cmpl	$35, %r12d
	jg	.L1951
.L1935:
	movl	%r12d, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L1950
.L1940:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L1931:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1952
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1951:
	.cfi_restore_state
	movabsq	$288265904121184256, %rax
	btq	%r12, %rax
	jnc	.L1935
.L1950:
	movl	$92, %eax
	leaq	-26(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%ax, -26(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1933:
	leal	-91(%r12), %eax
	cmpl	$34, %eax
	ja	.L1935
	movabsq	$21474836495, %rdx
	btq	%rax, %rdx
	jnc	.L1935
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L1932:
	movl	%esi, %edi
	call	_ZN6icu_6711ICU_Utility13isUnprintableEi@PLT
	testb	%al, %al
	je	.L1938
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi@PLT
	testb	%al, %al
	je	.L1938
	jmp	.L1931
.L1952:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2494:
	.size	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia, .-_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet16_generatePatternERNS_13UnicodeStringEa
	.type	_ZNK6icu_6710UnicodeSet16_generatePatternERNS_13UnicodeStringEa, @function
_ZNK6icu_6710UnicodeSet16_generatePatternERNS_13UnicodeStringEa:
.LFB2497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-58(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$91, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r14, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%rdi, -80(%rbp)
	movq	%r12, %rdi
	movb	%dl, -69(%rbp)
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%r13w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	28(%r15), %eax
	movl	%eax, %r13d
	shrl	$31, %r13d
	addl	%eax, %r13d
	movl	%r13d, %ecx
	sarl	%ecx
	movl	%ecx, -68(%rbp)
	cmpl	$3, %eax
	jle	.L1954
	movq	16(%r15), %rdx
	movl	(%rdx), %r11d
	testl	%r11d, %r11d
	je	.L1996
.L1955:
	movq	%r14, -88(%rbp)
	movsbl	%bl, %ebx
	xorl	%r13d, %r13d
	jmp	.L1965
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	-80(%rbp), %rax
	movq	16(%rax), %rdx
.L1965:
	movl	(%rdx,%r13,8), %r14d
	movl	4(%rdx,%r13,8), %r8d
	movq	%r12, %rdi
	movl	%ebx, %edx
	leal	-1(%r8), %r15d
	movl	%r14d, %esi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia
	cmpl	%r14d, %r15d
	je	.L1963
	leal	1(%r14), %eax
	cmpl	%r15d, %eax
	je	.L1964
	movq	-88(%rbp), %rsi
	movl	$45, %r8d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$1, %ecx
	movw	%r8w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L1964:
	movl	%ebx, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia
.L1963:
	addq	$1, %r13
	cmpl	%r13d, -68(%rbp)
	jg	.L1997
	movq	-88(%rbp), %r14
.L1961:
	movq	-80(%rbp), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1958
	movl	8(%rax), %esi
	testl	%esi, %esi
	jle	.L1958
	movl	$0, -68(%rbp)
	.p2align 4,,10
	.p2align 3
.L1980:
	movl	$123, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%cx, -58(%rbp)
	movl	$1, %ecx
	xorl	%r13d, %r13d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-80(%rbp), %rax
	movl	-68(%rbp), %esi
	movq	80(%rax), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rbx
	jmp	.L1979
	.p2align 4,,10
	.p2align 3
.L1999:
	sarl	$5, %eax
	cmpl	%eax, %r13d
	jge	.L1968
.L2000:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	cmpb	$0, -69(%rbp)
	movl	%eax, %r15d
	jne	.L1969
.L1975:
	cmpl	$58, %r15d
	jg	.L1970
	cmpl	$35, %r15d
	jg	.L1998
.L1972:
	movl	%r15d, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L1995
.L1977:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L1976:
	xorl	%eax, %eax
	cmpl	$65535, %r15d
	seta	%al
	leal	1(%r13,%rax), %r13d
.L1979:
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	jns	.L1999
	movl	12(%rbx), %eax
	cmpl	%eax, %r13d
	jl	.L2000
.L1968:
	movl	$125, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-80(%rbp), %rax
	addl	$1, -68(%rbp)
	movl	-68(%rbp), %ebx
	movq	80(%rax), %rax
	cmpl	8(%rax), %ebx
	jl	.L1980
.L1958:
	movl	$93, %edi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movl	$1, %ecx
	movw	%di, -58(%rbp)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L2001
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1998:
	.cfi_restore_state
	movl	%r15d, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movabsq	$288265904121184256, %rcx
	testq	%rcx, %rax
	je	.L1972
.L1995:
	movl	$92, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%dx, -58(%rbp)
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L1970:
	leal	-91(%r15), %ecx
	cmpl	$34, %ecx
	ja	.L1972
	movabsq	$21474836495, %rdx
	movl	$1, %eax
	salq	%cl, %rax
	testq	%rdx, %rax
	je	.L1972
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L1969:
	movl	%eax, %edi
	call	_ZN6icu_6711ICU_Utility13isUnprintableEi@PLT
	testb	%al, %al
	je	.L1975
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi@PLT
	testb	%al, %al
	je	.L1975
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L1954:
	cmpl	$1, %eax
	jle	.L1961
	movq	-80(%rbp), %rax
	movq	16(%rax), %rdx
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L1996:
	leal	-2(%rcx,%rcx), %eax
	cltq
	cmpl	$1114112, 4(%rdx,%rax,4)
	jne	.L1955
	movl	$94, %r10d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movw	%r10w, -58(%rbp)
	movsbl	%bl, %r13d
	movl	$1, %r15d
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r14, -96(%rbp)
	movl	$4, %ebx
	.p2align 4,,10
	.p2align 3
.L1962:
	movq	-80(%rbp), %rax
	movq	%r12, %rdi
	movq	16(%rax), %rdx
	movl	(%rdx,%rbx), %eax
	movl	4(%rdx,%rbx), %r8d
	movl	%r13d, %edx
	movl	%eax, %esi
	movl	%eax, -88(%rbp)
	leal	-1(%r8), %r14d
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia
	movl	-88(%rbp), %eax
	cmpl	%eax, %r14d
	je	.L1959
	addl	$1, %eax
	cmpl	%r14d, %eax
	je	.L1960
	movq	-96(%rbp), %rsi
	movl	$45, %r9d
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$1, %ecx
	movw	%r9w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
.L1960:
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia
.L1959:
	addl	$1, %r15d
	addq	$8, %rbx
	cmpl	%r15d, -68(%rbp)
	jg	.L1962
	movq	-96(%rbp), %r14
	jmp	.L1961
.L2001:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2497:
	.size	_ZNK6icu_6710UnicodeSet16_generatePatternERNS_13UnicodeStringEa, .-_ZNK6icu_6710UnicodeSet16_generatePatternERNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet10_toPatternERNS_13UnicodeStringEa
	.type	_ZNK6icu_6710UnicodeSet10_toPatternERNS_13UnicodeStringEa, @function
_ZNK6icu_6710UnicodeSet10_toPatternERNS_13UnicodeStringEa:
.LFB2495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	64(%rdi), %r8
	movb	%dl, -49(%rbp)
	testq	%r8, %r8
	je	.L2003
	movl	72(%rdi), %edi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L2004
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2008:
	movl	%r14d, %edi
	call	_ZN6icu_6711ICU_Utility13isUnprintableEi@PLT
	testb	%al, %al
	je	.L2011
	andl	$1, %ebx
	jne	.L2039
.L2012:
	movq	%r13, %rdi
	movl	%r14d, %esi
	xorl	%ebx, %ebx
	call	_ZN6icu_6711ICU_Utility17escapeUnprintableERNS_13UnicodeStringEi@PLT
	movl	72(%r12), %edi
	cmpl	%r15d, %edi
	jle	.L2025
.L2041:
	movq	64(%r12), %r8
	movl	%r15d, %eax
.L2004:
	movslq	%eax, %rcx
	leal	1(%rax), %r15d
	movzwl	(%r8,%rcx,2), %r14d
	leaq	(%rcx,%rcx), %r9
	movl	%r14d, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L2007
	cmpl	%edi, %r15d
	jne	.L2040
.L2007:
	cmpb	$0, -49(%rbp)
	jne	.L2008
.L2011:
	movq	%r13, %rdi
	movl	%r14d, %esi
	addl	$1, %ebx
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movl	72(%r12), %edi
	cmpl	$92, %r14d
	movl	$0, %eax
	cmovne	%eax, %ebx
	cmpl	%r15d, %edi
	jg	.L2041
.L2025:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2040:
	.cfi_restore_state
	movzwl	2(%r8,%r9), %ecx
	movl	%ecx, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L2007
	movl	%r14d, %esi
	leal	2(%rax), %r15d
	sall	$10, %esi
	leal	-56613888(%rcx,%rsi), %r14d
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L2039:
	movzwl	8(%r13), %eax
	movl	%eax, %r8d
	andl	$1, %r8d
	testw	%ax, %ax
	js	.L2013
	movswl	%ax, %ecx
	sarl	$5, %ecx
	leal	-1(%rcx), %edi
	testb	%r8b, %r8b
	jne	.L2014
	cmpl	%edi, %ecx
	jbe	.L2012
.L2018:
	andl	$31, %eax
	sall	$5, %edi
	orl	%edi, %eax
	movw	%ax, 8(%r13)
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2013:
	movl	12(%r13), %ecx
	leal	-1(%rcx), %edi
	testb	%r8b, %r8b
	je	.L2016
.L2014:
	testl	%edi, %edi
	je	.L2042
.L2016:
	cmpl	%edi, %ecx
	jbe	.L2012
	cmpl	$1023, %edi
	jle	.L2018
	orl	$-32, %eax
	movl	%edi, 12(%r13)
	movw	%ax, 8(%r13)
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2003:
	addq	$24, %rsp
	movsbl	%dl, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6710UnicodeSet16_generatePatternERNS_13UnicodeStringEa
.L2042:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	jmp	.L2012
	.cfi_endproc
.LFE2495:
	.size	_ZNK6icu_6710UnicodeSet10_toPatternERNS_13UnicodeStringEa, .-_ZNK6icu_6710UnicodeSet10_toPatternERNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa
	.type	_ZNK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa, @function
_ZNK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa:
.LFB2496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	movzwl	8(%rsi), %eax
	testb	$1, %al
	je	.L2044
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L2045:
	addq	$8, %rsp
	movsbl	%bl, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6710UnicodeSet10_toPatternERNS_13UnicodeStringEa
	.p2align 4,,10
	.p2align 3
.L2044:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L2046
	movswl	%ax, %edx
	sarl	$5, %edx
.L2047:
	testl	%edx, %edx
	je	.L2045
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L2045
	.p2align 4,,10
	.p2align 3
.L2046:
	movl	12(%rsi), %edx
	jmp	.L2047
	.cfi_endproc
.LFE2496:
	.size	_ZNK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa, .-_ZNK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa
	.p2align 4
	.globl	_ZThn8_NK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa
	.type	_ZThn8_NK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa, @function
_ZThn8_NK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa:
.LFB3100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	-8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	movzwl	8(%rsi), %eax
	testb	$1, %al
	je	.L2053
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
.L2054:
	addq	$8, %rsp
	movsbl	%bl, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6710UnicodeSet10_toPatternERNS_13UnicodeStringEa
	.p2align 4,,10
	.p2align 3
.L2053:
	.cfi_restore_state
	testw	%ax, %ax
	js	.L2055
	movswl	%ax, %edx
	sarl	$5, %edx
.L2056:
	testl	%edx, %edx
	je	.L2054
	andl	$31, %eax
	movw	%ax, 8(%r12)
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2055:
	movl	12(%rsi), %edx
	jmp	.L2056
	.cfi_endproc
.LFE3100:
	.size	_ZThn8_NK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa, .-_ZThn8_NK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet14releasePatternEv
	.type	_ZN6icu_6710UnicodeSet14releasePatternEv, @function
_ZN6icu_6710UnicodeSet14releasePatternEv:
.LFB2498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2061
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L2061:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2498:
	.size	_ZN6icu_6710UnicodeSet14releasePatternEv, .-_ZN6icu_6710UnicodeSet14releasePatternEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet10setPatternEPKDsi
	.type	_ZN6icu_6710UnicodeSet10setPatternEPKDsi, @function
_ZN6icu_6710UnicodeSet10setPatternEPKDsi:
.LFB2499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2068
	call	uprv_free_67@PLT
	movq	$0, 64(%rbx)
	movl	$0, 72(%rbx)
.L2068:
	leal	1(%r13), %edi
	movslq	%edi, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 64(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2067
	movl	%r13d, 72(%rbx)
	movl	%r13d, %edx
	movq	%r12, %rsi
	call	u_memcpy_67@PLT
	movslq	72(%rbx), %rdx
	movq	64(%rbx), %rax
	xorl	%ecx, %ecx
	movw	%cx, (%rax,%rdx,2)
.L2067:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2499:
	.size	_ZN6icu_6710UnicodeSet10setPatternEPKDsi, .-_ZN6icu_6710UnicodeSet10setPatternEPKDsi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet6freezeEv
	.type	_ZN6icu_6710UnicodeSet6freezeEv, @function
_ZN6icu_6710UnicodeSet6freezeEv:
.LFB2500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	cmpq	$0, 40(%rdi)
	movq	%rdi, %r12
	je	.L2094
.L2090:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2094:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L2090
	testb	$1, 32(%rdi)
	jne	.L2090
	call	_ZN6icu_6710UnicodeSet7compactEv
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L2081
	movl	8(%rax), %edx
	testl	%edx, %edx
	je	.L2081
	movl	$392, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2082
	movq	80(%r12), %rdx
	movq	%rax, %rdi
	movl	$63, %ecx
	movq	%r12, %rsi
	call	_ZN6icu_6720UnicodeSetStringSpanC1ERKNS_10UnicodeSetERKNS_7UVectorEj@PLT
	movl	252(%r13), %eax
	movq	%r13, 88(%r12)
	testl	%eax, %eax
	jne	.L2090
	movq	%r13, %rdi
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, 88(%r12)
.L2084:
	movl	$872, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2085
	movl	28(%r12), %edx
	movq	16(%r12), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_676BMPSetC1EPKii@PLT
	movq	%r13, 40(%r12)
	jmp	.L2090
	.p2align 4,,10
	.p2align 3
.L2081:
	cmpq	$0, 88(%r12)
	jne	.L2090
	jmp	.L2084
.L2085:
	movq	$0, 40(%r12)
	cmpq	$0, 88(%r12)
	je	.L2086
.L2087:
	movb	$1, 32(%r12)
	jmp	.L2090
.L2082:
	movq	$0, 88(%r12)
	cmpq	$0, 40(%r12)
	jne	.L2087
.L2086:
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv.part.0
	jmp	.L2087
	.cfi_endproc
.LFE2500:
	.size	_ZN6icu_6710UnicodeSet6freezeEv, .-_ZN6icu_6710UnicodeSet6freezeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition
	.type	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition, @function
_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition:
.LFB2501:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L2096
	movq	40(%rdi), %r8
	testq	%r8, %r8
	je	.L2096
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%r8, %rdi
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	call	_ZNK6icu_676BMPSet4spanEPKDsS2_17USetSpanCondition@PLT
	movq	-8(%rbp), %rsi
	leave
	.cfi_def_cfa 7, 8
	subq	%rsi, %rax
	sarq	%rax
	ret
	.p2align 4,,10
	.p2align 3
.L2096:
	.cfi_restore 6
	jmp	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition.part.0
	.cfi_endproc
.LFE2501:
	.size	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition, .-_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition
	.type	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition, @function
_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition:
.LFB2502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movslq	%edx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r15d, %r15d
	jle	.L2106
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2107
	leaq	(%rsi,%r15,2), %rdx
	call	_ZNK6icu_676BMPSet8spanBackEPKDsS2_17USetSpanCondition@PLT
	subq	%r14, %rax
	movq	%rax, %r12
	sarq	%r12
.L2105:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2148
	addq	$424, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2106:
	.cfi_restore_state
	movl	$0, %r12d
	je	.L2105
	movq	%rsi, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L2105
.L2107:
	movq	88(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2109
	movl	%ebx, %ecx
	movl	%r15d, %edx
	movq	%r14, %rsi
	call	_ZNK6icu_6720UnicodeSetStringSpan8spanBackEPKDsi17USetSpanCondition@PLT
	movl	%eax, %r12d
	jmp	.L2105
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	80(%r13), %rdx
	testq	%rdx, %rdx
	je	.L2110
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L2110
	xorl	%ecx, %ecx
	testl	%ebx, %ebx
	leaq	-448(%rbp), %rdi
	movq	%r13, %rsi
	setne	%cl
	movq	%rdi, -456(%rbp)
	addl	$25, %ecx
	call	_ZN6icu_6720UnicodeSetStringSpanC1ERKNS_10UnicodeSetERKNS_7UVectorEj@PLT
	movl	-196(%rbp), %eax
	movq	-456(%rbp), %rdi
	testl	%eax, %eax
	je	.L2112
	movl	%ebx, %ecx
	movl	%r15d, %edx
	movq	%r14, %rsi
	call	_ZNK6icu_6720UnicodeSetStringSpan8spanBackEPKDsi17USetSpanCondition@PLT
	movq	-456(%rbp), %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	jmp	.L2105
	.p2align 4,,10
	.p2align 3
.L2112:
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
.L2110:
	testl	%ebx, %ebx
	movl	$1, %ecx
	movl	%r15d, %r12d
	cmovne	%ecx, %ebx
	jmp	.L2122
	.p2align 4,,10
	.p2align 3
.L2150:
	movq	(%rdi), %rax
	call	*16(%rax)
	movsbl	%al, %eax
.L2115:
	cmpl	%eax, %ebx
	jne	.L2105
	movl	%r15d, %r12d
	testl	%r15d, %r15d
	jle	.L2105
.L2122:
	leal	-1(%r12), %r15d
	movslq	%r15d, %rax
	movzwl	(%r14,%rax,2), %esi
	leaq	(%rax,%rax), %rdx
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L2113
	testl	%r15d, %r15d
	jg	.L2149
.L2113:
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L2150
	movq	88(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2116
	addq	$8, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi
	movsbl	%al, %eax
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2149:
	movzwl	-2(%r14,%rdx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L2113
	sall	$10, %eax
	leal	-2(%r12), %r15d
	leal	-56613888(%rsi,%rax), %esi
	jmp	.L2113
	.p2align 4,,10
	.p2align 3
.L2116:
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	jg	.L2115
	movq	16(%r13), %r8
	cmpl	(%r8), %esi
	jl	.L2115
	movl	28(%r13), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2147
	movslq	%eax, %rcx
	xorl	%edx, %edx
	cmpl	-4(%r8,%rcx,4), %esi
	jl	.L2121
.L2147:
	andl	$1, %eax
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2120:
	movslq	%eax, %rdi
	cmpl	(%r8,%rdi,4), %esi
	jge	.L2151
.L2121:
	movl	%eax, %ecx
	leal	(%rax,%rdx), %eax
	sarl	%eax
	cmpl	%edx, %eax
	jne	.L2120
	movl	%ecx, %eax
	jmp	.L2147
	.p2align 4,,10
	.p2align 3
.L2151:
	movl	%eax, %edx
	movl	%ecx, %eax
	jmp	.L2121
.L2148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2502:
	.size	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition, .-_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	" 000000000000\02000"
	.section	.rodata
.LC2:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition
	.type	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition, @function
_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition:
.LFB2503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$424, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L2153
	movq	40(%rdi), %rdi
	movl	%edx, %r14d
	testq	%rdi, %rdi
	je	.L2154
	call	_ZNK6icu_676BMPSet8spanUTF8EPKhi17USetSpanCondition@PLT
	movl	%eax, %r13d
	subl	%ebx, %r13d
.L2152:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2208
	addq	$424, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2153:
	.cfi_restore_state
	movl	$0, %r13d
	je	.L2152
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L2152
.L2154:
	movq	88(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2156
	movl	%r12d, %ecx
	movl	%r14d, %edx
	movq	%rbx, %rsi
	call	_ZNK6icu_6720UnicodeSetStringSpan8spanUTF8EPKhi17USetSpanCondition@PLT
	movl	%eax, %r13d
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2156:
	movq	80(%r15), %rdx
	testq	%rdx, %rdx
	je	.L2157
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L2157
	xorl	%ecx, %ecx
	testl	%r12d, %r12d
	leaq	-448(%rbp), %rdi
	movq	%r15, %rsi
	setne	%cl
	movq	%rdi, -456(%rbp)
	addl	$37, %ecx
	call	_ZN6icu_6720UnicodeSetStringSpanC1ERKNS_10UnicodeSetERKNS_7UVectorEj@PLT
	movl	-192(%rbp), %eax
	movq	-456(%rbp), %rdi
	testl	%eax, %eax
	je	.L2159
	movl	%r12d, %ecx
	movl	%r14d, %edx
	movq	%rbx, %rsi
	call	_ZNK6icu_6720UnicodeSetStringSpan8spanUTF8EPKhi17USetSpanCondition@PLT
	movq	-456(%rbp), %rdi
	movl	%eax, %r13d
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2159:
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
.L2157:
	testl	%r12d, %r12d
	movl	$1, %eax
	cmove	%r12d, %eax
	xorl	%r13d, %r13d
	movl	%eax, -456(%rbp)
	jmp	.L2177
	.p2align 4,,10
	.p2align 3
.L2160:
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2169
.L2166:
	movq	(%rdi), %rax
	movl	%r10d, %esi
	call	*16(%rax)
	movsbl	%al, %eax
.L2170:
	cmpl	%eax, -456(%rbp)
	jne	.L2152
	movl	%r12d, %r13d
	cmpl	%r12d, %r14d
	jle	.L2152
.L2177:
	movslq	%r13d, %rax
	leal	1(%r13), %r12d
	movzbl	(%rbx,%rax), %r10d
	movl	%r10d, %esi
	testb	%r10b, %r10b
	jns	.L2160
	cmpl	%r14d, %r12d
	je	.L2183
	cmpb	$-33, %r10b
	jbe	.L2162
	cmpb	$-17, %r10b
	ja	.L2163
	movslq	%r12d, %rax
	andl	$15, %esi
	leaq	.LC1(%rip), %rcx
	andl	$15, %r10d
	movzbl	(%rbx,%rax), %eax
	movsbl	(%rcx,%rsi), %esi
	movl	%eax, %ecx
	andl	$63, %eax
	shrb	$5, %cl
	btl	%ecx, %esi
	jnc	.L2161
.L2164:
	addl	$1, %r12d
	cmpl	%r14d, %r12d
	je	.L2161
	sall	$6, %r10d
	movzbl	%al, %esi
	orl	%r10d, %esi
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L2183:
	movl	%r14d, %r12d
.L2161:
	movq	40(%r15), %rdi
	movl	$65533, %r10d
	testq	%rdi, %rdi
	jne	.L2166
	movq	88(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2168
.L2167:
	addq	$8, %rdi
	movl	%r10d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi
	movsbl	%al, %eax
	jmp	.L2170
	.p2align 4,,10
	.p2align 3
.L2169:
	movq	88(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L2167
	xorl	%eax, %eax
	cmpl	$1114111, %r10d
	jg	.L2170
.L2168:
	movq	16(%r15), %rsi
	xorl	%eax, %eax
	cmpl	%r10d, (%rsi)
	jg	.L2170
	movl	28(%r15), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2207
	movslq	%eax, %rdi
	xorl	%ecx, %ecx
	cmpl	%r10d, -4(%rsi,%rdi,4)
	jg	.L2176
.L2207:
	andl	$1, %eax
	jmp	.L2170
	.p2align 4,,10
	.p2align 3
.L2175:
	movslq	%eax, %r11
	cmpl	%r10d, (%rsi,%r11,4)
	jle	.L2209
.L2176:
	movl	%eax, %edi
	leal	(%rax,%rcx), %eax
	sarl	%eax
	cmpl	%ecx, %eax
	jne	.L2175
	movl	%edi, %eax
	jmp	.L2207
	.p2align 4,,10
	.p2align 3
.L2162:
	cmpb	$-63, %r10b
	jbe	.L2161
	andl	$31, %esi
.L2165:
	movslq	%r12d, %rax
	movzbl	(%rbx,%rax), %r10d
	addl	$-128, %r10d
	cmpb	$63, %r10b
	ja	.L2161
	sall	$6, %esi
	movzbl	%r10b, %r10d
	addl	$1, %r12d
	orl	%esi, %r10d
	jmp	.L2160
	.p2align 4,,10
	.p2align 3
.L2163:
	leal	-240(%r10), %esi
	cmpl	$4, %esi
	jg	.L2161
	movslq	%r12d, %rax
	leaq	.LC2(%rip), %rcx
	movzbl	(%rbx,%rax), %r10d
	movq	%r10, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%rcx,%rax), %eax
	btl	%esi, %eax
	jnc	.L2161
	leal	2(%r13), %r12d
	cmpl	%r14d, %r12d
	je	.L2183
	movslq	%r12d, %rax
	movzbl	(%rbx,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L2161
	sall	$6, %esi
	andl	$63, %r10d
	orl	%esi, %r10d
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2209:
	movl	%eax, %ecx
	movl	%edi, %eax
	jmp	.L2176
.L2208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2503:
	.size	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition, .-_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UnicodeSet12spanBackUTF8EPKci17USetSpanCondition
	.type	_ZNK6icu_6710UnicodeSet12spanBackUTF8EPKci17USetSpanCondition, @function
_ZNK6icu_6710UnicodeSet12spanBackUTF8EPKci17USetSpanCondition:
.LFB2504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$424, %rsp
	.cfi_offset 3, -56
	movl	%edx, -452(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L2211
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2212
	call	_ZNK6icu_676BMPSet12spanBackUTF8EPKhi17USetSpanCondition@PLT
	movl	%eax, %r12d
.L2210:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2247
	addq	$424, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2211:
	.cfi_restore_state
	je	.L2210
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, -452(%rbp)
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L2210
.L2212:
	movq	88(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2214
	movl	%r12d, %edx
	movl	%r13d, %ecx
	movq	%r15, %rsi
	call	_ZNK6icu_6720UnicodeSetStringSpan12spanBackUTF8EPKhi17USetSpanCondition@PLT
	movl	%eax, %r12d
	jmp	.L2210
	.p2align 4,,10
	.p2align 3
.L2214:
	movq	80(%r14), %rdx
	testq	%rdx, %rdx
	je	.L2215
	movl	8(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L2215
	xorl	%ecx, %ecx
	testl	%r13d, %r13d
	leaq	-448(%rbp), %rbx
	movq	%r14, %rsi
	setne	%cl
	movq	%rbx, %rdi
	addl	$21, %ecx
	call	_ZN6icu_6720UnicodeSetStringSpanC1ERKNS_10UnicodeSetERKNS_7UVectorEj@PLT
	movl	-192(%rbp), %eax
	testl	%eax, %eax
	je	.L2217
	movl	-452(%rbp), %edx
	movq	%rbx, %rdi
	movl	%r13d, %ecx
	movq	%r15, %rsi
	call	_ZNK6icu_6720UnicodeSetStringSpan12spanBackUTF8EPKhi17USetSpanCondition@PLT
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	jmp	.L2210
	.p2align 4,,10
	.p2align 3
.L2217:
	movq	%rbx, %rdi
	call	_ZN6icu_6720UnicodeSetStringSpanD1Ev@PLT
	movl	-452(%rbp), %r12d
.L2215:
	testl	%r13d, %r13d
	movl	$1, %ebx
	cmove	%r13d, %ebx
	leaq	-452(%rbp), %r13
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2249:
	movq	(%rdi), %rax
	call	*16(%rax)
	movsbl	%al, %eax
.L2220:
	cmpl	%eax, %ebx
	jne	.L2210
	movl	-452(%rbp), %r12d
	testl	%r12d, %r12d
	jle	.L2210
.L2227:
	leal	-1(%r12), %eax
	movl	%eax, -452(%rbp)
	cltq
	movzbl	(%r15,%rax), %esi
	testb	%sil, %sil
	js	.L2248
.L2218:
	movq	40(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L2249
	movq	88(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2221
	addq	$8, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi
	movsbl	%al, %eax
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2248:
	movl	%esi, %ecx
	movl	$-3, %r8d
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	utf8_prevCharSafeBody_67@PLT
	movl	%eax, %esi
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2221:
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	jg	.L2220
	movq	16(%r14), %r8
	cmpl	(%r8), %esi
	jl	.L2220
	movl	28(%r14), %eax
	subl	$1, %eax
	testl	%eax, %eax
	jle	.L2246
	movslq	%eax, %rcx
	xorl	%edx, %edx
	cmpl	-4(%r8,%rcx,4), %esi
	jl	.L2226
.L2246:
	andl	$1, %eax
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2225:
	movslq	%eax, %rdi
	cmpl	(%r8,%rdi,4), %esi
	jge	.L2250
.L2226:
	movl	%eax, %ecx
	leal	(%rax,%rdx), %eax
	sarl	%eax
	cmpl	%edx, %eax
	jne	.L2225
	movl	%ecx, %eax
	jmp	.L2246
	.p2align 4,,10
	.p2align 3
.L2250:
	movl	%eax, %edx
	movl	%ecx, %eax
	jmp	.L2226
.L2247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2504:
	.size	_ZNK6icu_6710UnicodeSet12spanBackUTF8EPKci17USetSpanCondition, .-_ZNK6icu_6710UnicodeSet12spanBackUTF8EPKci17USetSpanCondition
	.weak	_ZTSN6icu_6711SymbolTableE
	.section	.rodata._ZTSN6icu_6711SymbolTableE,"aG",@progbits,_ZTSN6icu_6711SymbolTableE,comdat
	.align 16
	.type	_ZTSN6icu_6711SymbolTableE, @object
	.size	_ZTSN6icu_6711SymbolTableE, 23
_ZTSN6icu_6711SymbolTableE:
	.string	"N6icu_6711SymbolTableE"
	.weak	_ZTIN6icu_6711SymbolTableE
	.section	.data.rel.ro._ZTIN6icu_6711SymbolTableE,"awG",@progbits,_ZTIN6icu_6711SymbolTableE,comdat
	.align 8
	.type	_ZTIN6icu_6711SymbolTableE, @object
	.size	_ZTIN6icu_6711SymbolTableE, 16
_ZTIN6icu_6711SymbolTableE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_6711SymbolTableE
	.weak	_ZTSN6icu_6710UnicodeSetE
	.section	.rodata._ZTSN6icu_6710UnicodeSetE,"aG",@progbits,_ZTSN6icu_6710UnicodeSetE,comdat
	.align 16
	.type	_ZTSN6icu_6710UnicodeSetE, @object
	.size	_ZTSN6icu_6710UnicodeSetE, 22
_ZTSN6icu_6710UnicodeSetE:
	.string	"N6icu_6710UnicodeSetE"
	.weak	_ZTIN6icu_6710UnicodeSetE
	.section	.data.rel.ro._ZTIN6icu_6710UnicodeSetE,"awG",@progbits,_ZTIN6icu_6710UnicodeSetE,comdat
	.align 8
	.type	_ZTIN6icu_6710UnicodeSetE, @object
	.size	_ZTIN6icu_6710UnicodeSetE, 24
_ZTIN6icu_6710UnicodeSetE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6710UnicodeSetE
	.quad	_ZTIN6icu_6713UnicodeFilterE
	.weak	_ZTVN6icu_6711SymbolTableE
	.section	.data.rel.ro._ZTVN6icu_6711SymbolTableE,"awG",@progbits,_ZTVN6icu_6711SymbolTableE,comdat
	.align 8
	.type	_ZTVN6icu_6711SymbolTableE, @object
	.size	_ZTVN6icu_6711SymbolTableE, 56
_ZTVN6icu_6711SymbolTableE:
	.quad	0
	.quad	_ZTIN6icu_6711SymbolTableE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6710UnicodeSetE
	.section	.data.rel.ro._ZTVN6icu_6710UnicodeSetE,"awG",@progbits,_ZTVN6icu_6710UnicodeSetE,comdat
	.align 8
	.type	_ZTVN6icu_6710UnicodeSetE, @object
	.size	_ZTVN6icu_6710UnicodeSetE, 344
_ZTVN6icu_6710UnicodeSetE:
	.quad	0
	.quad	_ZTIN6icu_6710UnicodeSetE
	.quad	_ZN6icu_6710UnicodeSetD1Ev
	.quad	_ZN6icu_6710UnicodeSetD0Ev
	.quad	_ZNK6icu_6710UnicodeSet17getDynamicClassIDEv
	.quad	_ZNK6icu_6710UnicodeSet5cloneEv
	.quad	_ZNK6icu_6713UnicodeFilter9toMatcherEv
	.quad	_ZNK6icu_6714UnicodeFunctor10toReplacerEv
	.quad	_ZN6icu_6713UnicodeFilter7setDataEPKNS_23TransliterationRuleDataE
	.quad	_ZNK6icu_6710UnicodeSet8containsEi
	.quad	_ZN6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia
	.quad	_ZNK6icu_6710UnicodeSeteqERKS0_
	.quad	_ZNK6icu_6710UnicodeSet8hashCodeEv
	.quad	_ZNK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa
	.quad	_ZNK6icu_6710UnicodeSet4sizeEv
	.quad	_ZNK6icu_6710UnicodeSet7isEmptyEv
	.quad	_ZNK6icu_6710UnicodeSet8containsEii
	.quad	_ZNK6icu_6710UnicodeSet11containsAllERKS0_
	.quad	_ZNK6icu_6710UnicodeSet13addMatchSetToERS0_
	.quad	_ZN6icu_6710UnicodeSet3addEii
	.quad	_ZN6icu_6710UnicodeSet6retainEii
	.quad	_ZN6icu_6710UnicodeSet6removeEii
	.quad	_ZN6icu_6710UnicodeSet10complementEv
	.quad	_ZN6icu_6710UnicodeSet10complementEii
	.quad	_ZN6icu_6710UnicodeSet6addAllERKS0_
	.quad	_ZN6icu_6710UnicodeSet9retainAllERKS0_
	.quad	_ZN6icu_6710UnicodeSet9removeAllERKS0_
	.quad	_ZN6icu_6710UnicodeSet13complementAllERKS0_
	.quad	_ZN6icu_6710UnicodeSet5clearEv
	.quad	_ZN6icu_6710UnicodeSet16removeAllStringsEv
	.quad	_ZNK6icu_6710UnicodeSet13getRangeCountEv
	.quad	_ZNK6icu_6710UnicodeSet13getRangeStartEi
	.quad	_ZNK6icu_6710UnicodeSet11getRangeEndEi
	.quad	_ZN6icu_6710UnicodeSet7compactEv
	.quad	_ZNK6icu_6710UnicodeSet17matchesIndexValueEh
	.quad	-8
	.quad	_ZTIN6icu_6710UnicodeSetE
	.quad	_ZThn8_N6icu_6710UnicodeSetD1Ev
	.quad	_ZThn8_N6icu_6710UnicodeSetD0Ev
	.quad	_ZThn8_N6icu_6710UnicodeSet7matchesERKNS_11ReplaceableERiia
	.quad	_ZThn8_NK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa
	.quad	_ZThn8_NK6icu_6710UnicodeSet17matchesIndexValueEh
	.quad	_ZThn8_NK6icu_6710UnicodeSet13addMatchSetToERS0_
	.local	_ZZN6icu_6710UnicodeSet16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6710UnicodeSet16getStaticClassIDEvE7classID,1,1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1
	.long	1
	.long	1
	.long	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
