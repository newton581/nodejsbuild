	.file	"umutablecptrie.cpp"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj, @function
_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj:
.LFB2157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -56(%rbp)
	cmpl	$1114111, %esi
	ja	.L33
	movq	%rdi, %r12
	movl	%esi, %ebx
	movq	%rdx, %r13
	movq	%rcx, %r15
	cmpl	48(%rdi), %esi
	jge	.L68
	movl	40(%rdi), %r11d
	testq	%rdx, %rdx
	je	.L6
	movl	%r9d, -72(%rbp)
	movl	%r11d, %esi
	movq	%rcx, %rdi
	movl	%r10d, -60(%rbp)
	call	*%rdx
	movl	-72(%rbp), %r9d
	movl	-60(%rbp), %r10d
	movl	%eax, %r11d
.L6:
	movl	%ebx, %r8d
	xorl	%eax, %eax
	sarl	$4, %r8d
	movslq	%r8d, %r8
.L30:
	movq	(%r12), %rdx
	cmpb	$0, 64(%r12,%r8)
	movl	(%rdx,%r8,4), %r14d
	jne	.L7
	testb	%al, %al
	je	.L8
	cmpl	%r14d, %r10d
	je	.L9
	testq	%r13, %r13
	je	.L65
	movl	%r11d, %eax
	cmpl	%r14d, 40(%r12)
	je	.L11
	movl	%r9d, -80(%rbp)
	movl	%r14d, %esi
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	movl	%r11d, -60(%rbp)
	call	*%r13
	movl	-80(%rbp), %r9d
	movq	-72(%rbp), %r8
	movl	-60(%rbp), %r11d
.L11:
	cmpl	%eax, %r9d
	jne	.L65
.L9:
	addl	$16, %ebx
	movl	%r14d, %r10d
	andl	$-16, %ebx
.L14:
	addq	$1, %r8
	movl	$1, %eax
	cmpl	%ebx, 48(%r12)
	jg	.L30
	movl	52(%r12), %eax
	cmpl	%eax, 40(%r12)
	je	.L31
	movl	%eax, %r11d
	testq	%r13, %r13
	je	.L31
	movl	%r9d, -56(%rbp)
	movl	%eax, %esi
	movq	%r15, %rdi
	call	*%r13
	movl	-56(%rbp), %r9d
	movl	%eax, %r11d
.L31:
	cmpl	%r9d, %r11d
	je	.L66
.L65:
	leal	-1(%rbx), %r9d
.L1:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	%ebx, %edx
	andl	$15, %edx
	addl	%edx, %r14d
	movq	16(%r12), %rdx
	movslq	%r14d, %r14
	movl	(%rdx,%r14,4), %edx
	leaq	0(,%r14,4), %rcx
	testb	%al, %al
	je	.L15
	cmpl	%edx, %r10d
	je	.L37
	testq	%r13, %r13
	je	.L65
	movl	%r11d, %r10d
	cmpl	40(%r12), %edx
	je	.L18
	movl	%r9d, -88(%rbp)
	movl	%edx, %esi
	movq	%r15, %rdi
	movq	%r8, -80(%rbp)
	movl	%r11d, -72(%rbp)
	movl	%edx, -60(%rbp)
	call	*%r13
	movl	-88(%rbp), %r9d
	movq	-80(%rbp), %r8
	movl	-72(%rbp), %r11d
	movl	-60(%rbp), %edx
	movl	%eax, %r10d
.L18:
	cmpl	%r10d, %r9d
	jne	.L65
	addl	$1, %ebx
	testb	$15, %bl
	je	.L64
.L22:
	leaq	4(,%r14,4), %r14
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L25:
	movl	%r9d, -64(%rbp)
	movl	%ecx, %esi
	movq	%r15, %rdi
	movl	%r10d, -88(%rbp)
	movq	%r8, -80(%rbp)
	movl	%r11d, -72(%rbp)
	movl	%ecx, -60(%rbp)
	call	*%r13
	movl	-60(%rbp), %ecx
	movl	-72(%rbp), %r11d
	movq	-80(%rbp), %r8
	movl	-88(%rbp), %r10d
	movl	-64(%rbp), %r9d
.L26:
	cmpl	%eax, %r10d
	jne	.L1
	movl	%ecx, %edx
.L24:
	addl	$1, %ebx
	addq	$4, %r14
	testb	$15, %bl
	je	.L41
.L27:
	movq	16(%r12), %rax
	leal	-1(%rbx), %r9d
	movl	(%rax,%r14), %ecx
	cmpl	%edx, %ecx
	je	.L24
	cmpl	%ecx, 40(%r12)
	jne	.L25
	movl	%r11d, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L8:
	movl	%r11d, %r9d
	cmpl	%r14d, 40(%r12)
	je	.L13
	movl	%r14d, %r9d
	testq	%r13, %r13
	je	.L13
	movq	%r8, -72(%rbp)
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	%r11d, -60(%rbp)
	call	*%r13
	movq	-72(%rbp), %r8
	movl	-60(%rbp), %r11d
	movl	%eax, %r9d
.L13:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L9
	movl	%r9d, (%rax)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%r10d, %r9d
.L64:
	movl	%edx, %r10d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%r11d, %r10d
	cmpl	40(%r12), %edx
	je	.L21
	movl	%edx, %r10d
	testq	%r13, %r13
	je	.L21
	movq	%rcx, -88(%rbp)
	movl	%edx, %esi
	movq	%r15, %rdi
	movq	%r8, -80(%rbp)
	movl	%r11d, -72(%rbp)
	movl	%edx, -60(%rbp)
	call	*%r13
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %r8
	movl	-72(%rbp), %r11d
	movl	-60(%rbp), %edx
	movl	%eax, %r10d
.L21:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L16
	movl	%r10d, (%rax)
.L16:
	addl	$1, %ebx
	testb	$15, %bl
	je	.L41
	testq	%r13, %r13
	jne	.L22
	addq	16(%r12), %rcx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L69:
	addl	$1, %ebx
	addq	$4, %rcx
	testb	$15, %bl
	je	.L41
.L23:
	leal	-1(%rbx), %r9d
	cmpl	4(%rcx), %edx
	jne	.L1
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L37:
	movl	%r9d, %r10d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L68:
	testq	%r8, %r8
	je	.L66
	movl	52(%rdi), %esi
	testq	%rdx, %rdx
	je	.L5
	movq	%rcx, %rdi
	call	*%rdx
	movl	%eax, %esi
.L5:
	movq	-56(%rbp), %rax
	movl	%esi, (%rax)
.L66:
	movl	$1114111, %r9d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$-1, %r9d
	jmp	.L1
	.cfi_endproc
.LFE2157:
	.size	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj, .-_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie12getDataBlockEi, @function
_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie12getDataBlockEi:
.LFB2111:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%esi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	cmpb	$1, 64(%rdi,%r14)
	je	.L89
	movl	28(%rdi), %r13d
	movl	24(%rdi), %eax
	movq	%r14, %r12
	cmpl	$4095, %r14d
	jg	.L73
	leal	64(%r13), %r15d
	cmpl	%eax, %r15d
	jle	.L74
	cmpl	$131071, %eax
	jle	.L79
	cmpl	$1114111, %eax
	jg	.L76
	movl	$1114112, -56(%rbp)
	movl	$4456448, %edi
.L75:
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L76
	movq	16(%rbx), %r9
	movslq	28(%rbx), %rdx
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	movq	%r9, %rsi
	salq	$2, %rdx
	movq	%r9, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r9
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	movq	-64(%rbp), %r8
	movl	-56(%rbp), %eax
	movq	%r8, 16(%rbx)
	movl	%eax, 24(%rbx)
.L74:
	movl	%r15d, 28(%rbx)
	testl	%r13d, %r13d
	js	.L70
	andl	$-4, %r12d
	movq	(%rbx), %rcx
	movq	16(%rbx), %rdi
	movslq	%r13d, %rsi
	movslq	%r12d, %r10
	salq	$2, %rsi
	leaq	0(,%r10,4), %r8
	leaq	(%rdi,%rsi), %rax
	leaq	(%rcx,%r8), %r9
	movl	(%r9), %edx
	movl	%edx, (%rax)
	movl	%edx, 4(%rax)
	movl	%edx, 8(%rax)
	movl	%edx, 12(%rax)
	movl	%edx, 16(%rax)
	movl	%edx, 20(%rax)
	movl	%edx, 24(%rax)
	movl	%edx, 28(%rax)
	movl	%edx, 32(%rax)
	movl	%edx, 36(%rax)
	movl	%edx, 40(%rax)
	movl	%edx, 44(%rax)
	movl	%edx, 48(%rax)
	movl	%edx, 52(%rax)
	movl	%edx, 56(%rax)
	movl	%edx, 60(%rax)
	leaq	64(%rdi,%rsi), %rax
	movb	$1, 64(%rbx,%r10)
	movl	%r13d, (%r9)
	leaq	4(%rcx,%r8), %r9
	movl	(%r9), %edx
	movl	%edx, (%rax)
	movl	%edx, 4(%rax)
	movl	%edx, 8(%rax)
	movl	%edx, 12(%rax)
	movl	%edx, 16(%rax)
	movl	%edx, 20(%rax)
	movl	%edx, 24(%rax)
	movl	%edx, 28(%rax)
	movl	%edx, 32(%rax)
	movl	%edx, 36(%rax)
	movl	%edx, 40(%rax)
	movl	%edx, 44(%rax)
	movl	%edx, 48(%rax)
	movl	%edx, 52(%rax)
	movl	%edx, 56(%rax)
	movl	%edx, 60(%rax)
	leal	1(%r12), %eax
	cltq
	movb	$1, 64(%rbx,%rax)
	leal	16(%r13), %eax
	movl	%eax, (%r9)
	leaq	8(%rcx,%r8), %r9
	leaq	128(%rdi,%rsi), %rax
	movl	(%r9), %edx
	leaq	12(%rcx,%r8), %r8
	movl	%edx, (%rax)
	movl	%edx, 4(%rax)
	movl	%edx, 8(%rax)
	movl	%edx, 12(%rax)
	movl	%edx, 16(%rax)
	movl	%edx, 20(%rax)
	movl	%edx, 24(%rax)
	movl	%edx, 28(%rax)
	movl	%edx, 32(%rax)
	movl	%edx, 36(%rax)
	movl	%edx, 40(%rax)
	movl	%edx, 44(%rax)
	movl	%edx, 48(%rax)
	movl	%edx, 52(%rax)
	movl	%edx, 56(%rax)
	movl	%edx, 60(%rax)
	leal	2(%r12), %eax
	cltq
	movb	$1, 64(%rbx,%rax)
	leal	32(%r13), %eax
	addl	$48, %r13d
	movl	%eax, (%r9)
	movl	(%r8), %edx
	leaq	192(%rdi,%rsi), %rax
	movl	%edx, (%rax)
	movl	%edx, 4(%rax)
	movl	%edx, 8(%rax)
	movl	%edx, 12(%rax)
	movl	%edx, 16(%rax)
	movl	%edx, 20(%rax)
	movl	%edx, 24(%rax)
	movl	%edx, 28(%rax)
	movl	%edx, 32(%rax)
	movl	%edx, 36(%rax)
	movl	%edx, 40(%rax)
	movl	%edx, 44(%rax)
	movl	%edx, 48(%rax)
	movl	%edx, 52(%rax)
	movl	%edx, 56(%rax)
	movl	%edx, 60(%rax)
	leal	3(%r12), %eax
	cltq
	movb	$1, 64(%rbx,%rax)
	movl	%r13d, (%r8)
	movl	(%rcx,%r14,4), %r13d
.L70:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	leal	16(%r13), %r12d
	cmpl	%eax, %r12d
	jle	.L77
	cmpl	$131071, %eax
	jle	.L81
	cmpl	$1114111, %eax
	jg	.L76
	movl	$4456448, %edi
	movl	$1114112, %r15d
.L78:
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L76
	movq	16(%rbx), %r8
	movslq	28(%rbx), %rdx
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	movq	%r8, %rsi
	salq	$2, %rdx
	movq	%r8, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rcx
	movl	%r15d, 24(%rbx)
	movq	%rcx, 16(%rbx)
.L77:
	movl	%r12d, 28(%rbx)
	testl	%r13d, %r13d
	js	.L70
	movq	(%rbx), %rdx
	movq	16(%rbx), %rcx
	movslq	%r13d, %rax
	movd	(%rdx,%r14,4), %xmm1
	leaq	(%rcx,%rax,4), %rax
	pshufd	$0, %xmm1, %xmm0
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movb	$1, 64(%rbx,%r14)
	movl	%r13d, (%rdx,%r14,4)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L89:
	movq	(%rdi), %rax
	movl	(%rax,%r14,4), %r13d
	jmp	.L70
.L79:
	movl	$131072, -56(%rbp)
	movl	$524288, %edi
	jmp	.L75
.L81:
	movl	$524288, %edi
	movl	$131072, %r15d
	jmp	.L78
.L76:
	movl	$-1, %r13d
	jmp	.L70
	.cfi_endproc
.LFE2111:
	.size	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie12getDataBlockEi, .-_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie12getDataBlockEi
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie3setEijR10UErrorCode.part.0, @function
_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie3setEijR10UErrorCode.part.0:
.LFB2788:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	48(%rdi), %r12d
	movl	%edx, -52(%rbp)
	cmpl	%r12d, %esi
	jl	.L91
	leal	512(%rsi), %r10d
	sarl	$4, %r12d
	movl	%r10d, %eax
	andl	$-512, %eax
	movl	%eax, -56(%rbp)
	sarl	$4, %eax
	movl	%eax, %r15d
	cmpl	8(%rdi), %eax
	jg	.L92
	movq	(%rdi), %r8
.L93:
	movl	%r15d, %r10d
	movl	$4, %edi
	movslq	%r12d, %rdx
	movl	$1, %r11d
	subl	%r12d, %r10d
	cmpl	%r15d, %r12d
	leaq	0(,%rdx,4), %rax
	movl	%r10d, %esi
	leaq	0(,%rsi,4), %rcx
	cmovge	%rdi, %rcx
	leaq	64(%rdx), %rdi
	leaq	(%rbx,%rdi), %r9
	addq	%rax, %rcx
	addq	%r8, %rax
	addq	%r8, %rcx
	cmpl	%r15d, %r12d
	cmovge	%r11, %rsi
	addq	%rdi, %rsi
	addq	%rbx, %rsi
	cmpq	%rsi, %rax
	setnb	%dil
	cmpq	%r9, %rcx
	setbe	%sil
	orl	%edi, %esi
	leaq	40(%rbx), %rdi
	cmpq	%rdi, %rcx
	leaq	44(%rbx), %rdi
	setbe	%cl
	cmpq	%rdi, %rax
	setnb	%dil
	orl	%edi, %ecx
	testb	%cl, %sil
	je	.L103
	leal	-1(%r15), %ecx
	subl	%r12d, %ecx
	cmpl	$14, %ecx
	seta	%sil
	cmpl	%r15d, %r12d
	setl	%cl
	testb	%cl, %sil
	je	.L103
	cmpl	%r15d, %r12d
	movl	$1, %ecx
	movd	40(%rbx), %xmm2
	movq	%r9, %rdx
	cmovl	%r10d, %ecx
	pxor	%xmm1, %xmm1
	pshufd	$0, %xmm2, %xmm0
	movl	%ecx, %esi
	shrl	$4, %esi
	salq	$4, %rsi
	addq	%r9, %rsi
	.p2align 4,,10
	.p2align 3
.L97:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	addq	$64, %rax
	movups	%xmm0, -64(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rsi, %rdx
	jne	.L97
	movl	%ecx, %eax
	andl	$-16, %eax
	addl	%eax, %r12d
	cmpl	%eax, %ecx
	je	.L99
	movslq	%r12d, %rax
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	1(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	2(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	3(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	4(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	5(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	6(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	7(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	8(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	9(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	10(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	11(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	12(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	leal	13(%r12), %eax
	cmpl	%eax, %r15d
	jle	.L99
	cltq
	addl	$14, %r12d
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r8,%rax,4)
	cmpl	%r12d, %r15d
	jle	.L99
	movslq	%r12d, %r12
	movb	$0, 64(%rbx,%r12)
	movl	40(%rbx), %eax
	movl	%eax, (%r8,%r12,4)
	.p2align 4,,10
	.p2align 3
.L99:
	movl	-56(%rbp), %eax
	movl	%eax, 48(%rbx)
.L91:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	sarl	$4, %esi
	call	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie12getDataBlockEi
	testl	%eax, %eax
	js	.L100
	andl	$15, %r13d
	movl	-52(%rbp), %edi
	addl	%eax, %r13d
	movq	16(%rbx), %rax
	movslq	%r13d, %r13
	movl	%edi, (%rax,%r13,4)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movb	$0, 64(%rbx,%rdx)
	movl	40(%rbx), %eax
	movl	%eax, (%r8,%rdx,4)
	addq	$1, %rdx
	cmpl	%edx, %r15d
	jg	.L103
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$7, (%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movl	$278528, %edi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L100
	movq	(%rbx), %r11
	movl	$278528, %ecx
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	leal	0(,%r12,4), %edx
	movq	%r11, %rsi
	movslq	%edx, %rdx
	movq	%r11, -72(%rbp)
	call	__memcpy_chk@PLT
	movq	-72(%rbp), %r11
	movq	%r11, %rdi
	call	uprv_free_67@PLT
	movq	-64(%rbp), %r8
	movl	$69632, 8(%rbx)
	movq	%r8, (%rbx)
	jmp	.L93
	.cfi_endproc
.LFE2788:
	.size	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie3setEijR10UErrorCode.part.0, .-_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie3setEijR10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie8setRangeEiijR10UErrorCode.part.0, @function
_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie8setRangeEiijR10UErrorCode.part.0:
.LFB2789:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leal	1(%rdx), %r12d
	movl	%esi, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r12d, %ebx
	andl	$15, %ebx
	subq	$24, %rsp
	movq	%r8, -56(%rbp)
	andl	$15, %edx
	jne	.L144
.L116:
	movl	%r12d, %edx
	andl	$-16, %edx
	cmpl	%r13d, %edx
	jle	.L127
	movd	%r15d, %xmm1
	movq	(%r14), %r8
	movl	%r13d, %esi
	pshufd	$0, %xmm1, %xmm0
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L146:
	addl	$16, %esi
	movl	%r15d, (%rdi)
	cmpl	%esi, %edx
	jle	.L145
.L130:
	movl	%esi, %eax
	sarl	$4, %eax
	cltq
	cmpb	$0, 64(%r14,%rax)
	leaq	(%r8,%rax,4), %rdi
	je	.L146
	movl	(%rdi), %edi
	movq	16(%r14), %rax
	addl	$16, %esi
	leaq	(%rax,%rdi,4), %rax
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	cmpl	%esi, %edx
	jg	.L130
.L145:
	subl	$1, %edx
	subl	%r13d, %edx
	andl	$-16, %edx
	leal	16(%rdx,%r13), %r13d
.L127:
	testl	%ebx, %ebx
	jne	.L147
.L111:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	sarl	$4, %esi
	movl	%edx, -60(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie12getDataBlockEi
	movslq	-60(%rbp), %rdx
	testl	%eax, %eax
	js	.L131
	movq	16(%r14), %r8
	addl	$15, %r13d
	cltq
	andl	$-16, %r13d
	leaq	(%r8,%rax,4), %rdi
	leaq	(%rdi,%rdx,4), %rsi
	cmpl	%r13d, %r12d
	jl	.L115
	leaq	64(%rdi), %r9
	cmpq	%rsi, %r9
	jbe	.L116
	addq	$63, %rdi
	subq	%rsi, %rdi
	movq	%rdi, %r10
	shrq	$2, %r10
	addq	$1, %r10
	cmpq	$11, %rdi
	jbe	.L117
	addq	%rdx, %rax
	movq	%r10, %rdx
	movd	%r15d, %xmm3
	shrq	$2, %rdx
	leaq	(%r8,%rax,4), %rax
	pshufd	$0, %xmm3, %xmm0
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L119:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L119
	movq	%r10, %rax
	andq	$-4, %rax
	leaq	(%rsi,%rax,4), %rsi
	cmpq	%rax, %r10
	je	.L116
.L117:
	leaq	4(%rsi), %rax
	movl	%r15d, (%rsi)
	cmpq	%rax, %r9
	jbe	.L116
	leaq	8(%rsi), %rax
	movl	%r15d, 4(%rsi)
	cmpq	%rax, %r9
	jbe	.L116
	movl	%r15d, 8(%rsi)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L147:
	movl	%r13d, %esi
	movq	%r14, %rdi
	sarl	$4, %esi
	call	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie12getDataBlockEi
	testl	%eax, %eax
	js	.L131
	movq	16(%r14), %rdx
	cltq
	leaq	(%rdx,%rax,4), %rsi
	movslq	%ebx, %rdx
	salq	$2, %rdx
	leaq	(%rsi,%rdx), %rdi
	cmpq	%rdi, %rsi
	jnb	.L111
	subq	$1, %rdx
	movq	%rsi, %rax
	movq	%rdx, %rcx
	shrq	$2, %rcx
	addq	$1, %rcx
	cmpq	$11, %rdx
	jbe	.L132
	movq	%rcx, %rdx
	movd	%r15d, %xmm2
	shrq	$2, %rdx
	pshufd	$0, %xmm2, %xmm0
	salq	$4, %rdx
	addq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L134:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L134
	movq	%rcx, %rax
	andq	$-4, %rax
	leaq	(%rsi,%rax,4), %rsi
	cmpq	%rcx, %rax
	je	.L111
.L132:
	leaq	4(%rsi), %rax
	movl	%r15d, (%rsi)
	cmpq	%rax, %rdi
	jbe	.L111
	leaq	8(%rsi), %rax
	movl	%r15d, 4(%rsi)
	cmpq	%rax, %rdi
	jbe	.L111
	movl	%r15d, 8(%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L115:
	.cfi_restore_state
	movslq	%ebx, %rcx
	leaq	(%rdi,%rcx,4), %rdi
	cmpq	%rsi, %rdi
	jbe	.L111
	leaq	-1(%rdi), %r9
	subq	%rsi, %r9
	movq	%r9, %rcx
	shrq	$2, %rcx
	addq	$1, %rcx
	cmpq	$11, %r9
	jbe	.L132
	addq	%rdx, %rax
	movq	%rcx, %rdx
	movd	%r15d, %xmm4
	shrq	$2, %rdx
	leaq	(%r8,%rax,4), %rax
	pshufd	$0, %xmm4, %xmm0
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L124:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L124
	movq	%rcx, %rax
	andq	$-4, %rax
	leaq	(%rsi,%rax,4), %rsi
	cmpq	%rax, %rcx
	jne	.L132
	jmp	.L111
.L131:
	movq	-56(%rbp), %rax
	movl	$7, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2789:
	.size	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie8setRangeEiijR10UErrorCode.part.0, .-_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie8setRangeEiijR10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie11compactTrieEiR10UErrorCode, @function
_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie11compactTrieEiR10UErrorCode:
.LFB2149:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$3448, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -7536(%rbp)
	movl	48(%rdi), %edx
	movq	%rdi, %r13
	movl	%esi, -7380(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	sarl	$4, %eax
	cmpl	$1114111, %edx
	jle	.L907
	movq	(%rdi), %r9
	cmpb	$0, 69695(%rdi)
	movl	278524(%r9), %edx
	jne	.L908
	movl	%edx, 52(%r13)
.L460:
	cltq
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L909:
	cmpl	%esi, %edx
	jne	.L156
.L155:
	subq	$1, %rax
	testl	%eax, %eax
	jle	.L150
.L153:
	cmpb	$0, 63(%r13,%rax)
	movl	-4(%r9,%rax,4), %esi
	movl	%eax, %r8d
	je	.L909
	movq	16(%r13), %rcx
	movl	%esi, %edi
	leaq	0(,%rdi,4), %rsi
	cmpl	(%rcx,%rdi,4), %edx
	jne	.L156
	cmpl	4(%rcx,%rsi), %edx
	jne	.L156
	cmpl	8(%rcx,%rsi), %edx
	jne	.L156
	cmpl	12(%rcx,%rsi), %edx
	jne	.L156
	cmpl	16(%rcx,%rsi), %edx
	jne	.L156
	cmpl	20(%rcx,%rsi), %edx
	jne	.L156
	cmpl	24(%rcx,%rsi), %edx
	jne	.L156
	cmpl	28(%rcx,%rsi), %edx
	jne	.L156
	cmpl	32(%rcx,%rsi), %edx
	jne	.L156
	cmpl	36(%rcx,%rsi), %edx
	jne	.L156
	cmpl	40(%rcx,%rsi), %edx
	jne	.L156
	cmpl	44(%rcx,%rsi), %edx
	jne	.L156
	cmpl	48(%rcx,%rsi), %edx
	jne	.L156
	cmpl	52(%rcx,%rsi), %edx
	jne	.L156
	cmpl	56(%rcx,%rsi), %edx
	jne	.L156
	cmpl	60(%rcx,%rsi), %edx
	je	.L155
	.p2align 4,,10
	.p2align 3
.L156:
	sall	$4, %r8d
	leal	511(%r8), %eax
	andl	$-512, %eax
	movl	%eax, -7540(%rbp)
	cmpl	$1114112, %eax
	jne	.L910
	movl	$69632, -7384(%rbp)
	movl	40(%r13), %eax
	movl	%eax, 52(%r13)
.L159:
	movl	-7540(%rbp), %eax
	movl	%eax, 48(%r13)
	jmp	.L161
.L907:
	movl	52(%rdi), %edx
	movq	(%rdi), %r9
	testl	%eax, %eax
	jg	.L460
.L150:
	movl	$0, -7384(%rbp)
	movl	-7380(%rbp), %edi
	movl	$0, -7540(%rbp)
	sall	$4, %edi
.L160:
	movl	-7380(%rbp), %r15d
	movslq	-7384(%rbp), %rcx
	leaq	52(%r13), %rbx
	movl	%r15d, %esi
	leaq	64(%rcx), %r8
	subl	%ecx, %esi
	leaq	0(%r13,%r8), %rdx
	movl	%esi, %r10d
	leaq	(%r10,%rcx), %rax
	addq	%r10, %r8
	leaq	(%r9,%rax,4), %r11
	addq	%r13, %r8
	leaq	(%r9,%rcx,4), %rax
	cmpq	%r8, %rax
	setnb	%r10b
	cmpq	%r11, %rdx
	setnb	%r8b
	orl	%r8d, %r10d
	leaq	56(%r13), %r8
	cmpq	%r8, %rax
	setnb	%r8b
	cmpq	%rbx, %r11
	setbe	%r11b
	orl	%r11d, %r8d
	testb	%r8b, %r10b
	je	.L849
	leal	-1(%r15), %r8d
	subl	%ecx, %r8d
	cmpl	$14, %r8d
	jbe	.L849
	shrl	$4, %esi
	movd	52(%r13), %xmm4
	pxor	%xmm1, %xmm1
	movl	%esi, %ecx
	salq	$6, %rcx
	pshufd	$0, %xmm4, %xmm0
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L163:
	movups	%xmm1, (%rdx)
	addq	$64, %rax
	addq	$16, %rdx
	movups	%xmm0, -64(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rax, %rcx
	jne	.L163
.L901:
	movq	0(%r13), %r9
.L164:
	movl	%edi, 48(%r13)
	sarl	$4, %edi
	movl	%edi, -7384(%rbp)
.L161:
	xorl	%eax, %eax
	leaq	-6976(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L167:
	movl	%eax, %edx
	sarl	$4, %edx
	movslq	%edx, %rdx
	cmpb	$0, 64(%r13,%rdx)
	movl	(%r9,%rdx,4), %ecx
	je	.L166
	movl	%eax, %edx
	andl	$15, %edx
	addl	%edx, %ecx
	movq	16(%r13), %rdx
	movl	(%rdx,%rcx,4), %ecx
.L166:
	movl	%ecx, (%r12,%rax,4)
	addq	$1, %rax
	cmpq	$128, %rax
	jne	.L167
	movl	-7380(%rbp), %eax
	movl	$-1, %ebx
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$64, -7392(%rbp)
	movl	$4, %r15d
	addl	$1, %eax
	movq	%r12, -7408(%rbp)
	movl	$148, -7492(%rbp)
	movl	%eax, -7400(%rbp)
	.p2align 4,,10
	.p2align 3
.L200:
	movslq	%esi, %rdi
	leaq	(%r9,%rdi,4), %r8
	movzbl	64(%r13,%rdi), %edx
	movl	(%r8), %eax
	cmpl	%esi, -7380(%rbp)
	je	.L168
	leal	(%r15,%rsi), %r12d
	cmpb	$1, %dl
	je	.L911
	cmpl	$1, %r15d
	jle	.L173
	leal	1(%rsi), %edx
	movslq	%edx, %rdx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L912:
	addq	$1, %rdx
	cmpl	%edx, %r12d
	jle	.L173
.L176:
	cmpl	%eax, (%r9,%rdx,4)
	je	.L912
	movq	%r13, %rdi
	call	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie12getDataBlockEi
	testl	%eax, %eax
	js	.L202
	.p2align 4,,10
	.p2align 3
.L185:
	movl	-7392(%rbp), %edi
	addl	%edi, -7492(%rbp)
	movl	%r12d, %esi
.L174:
	cmpl	-7384(%rbp), %r12d
	jge	.L199
.L915:
	movq	0(%r13), %r9
	jmp	.L200
.L908:
	leal	15(%rdx), %ecx
	movq	16(%rdi), %rdx
	movl	(%rdx,%rcx,4), %edx
	movl	%edx, 52(%r13)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L168:
	movl	-7400(%rbp), %r12d
	cmpb	$1, %dl
	je	.L536
	movl	$16, -7392(%rbp)
	movl	$1, %r15d
.L173:
	cmpl	$-1, %ebx
	je	.L177
	movslq	%ebx, %rdx
	cmpl	-7240(%rbp,%rdx,4), %eax
	je	.L902
.L177:
	testl	%r14d, %r14d
	je	.L179
	leal	-1(%r14), %r10d
	xorl	%edx, %edx
	leaq	-7376(%rbp), %rcx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	1(%rdx), %r11
	cmpq	%rdx, %r10
	je	.L913
	movq	%r11, %rdx
.L183:
	movl	%edx, %ebx
	cmpl	136(%rcx,%rdx,4), %eax
	jne	.L182
	movslq	%edx, %rdx
.L902:
	addl	%r15d, -7112(%rbp,%rdx,4)
	movl	-7368(%rbp,%rdx,4), %edx
	cmpl	$-2, %edx
	je	.L914
.L186:
	testl	%edx, %edx
	js	.L185
	movb	$2, 64(%r13,%rdi)
	movl	%r12d, %esi
	movl	%edx, (%r8)
	cmpl	-7384(%rbp), %r12d
	jl	.L915
.L199:
	movl	-7492(%rbp), %r15d
	movq	-7408(%rbp), %r12
	testl	%r15d, %r15d
	js	.L202
	leal	0(,%r15,4), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -7552(%rbp)
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L202
	movq	-6976(%rbp), %rax
	leaq	8(%rbx), %rdi
	movq	%r12, %rsi
	andq	$-8, %rdi
	movq	%rax, (%rbx)
	movq	-6472(%rbp), %rax
	movq	%rax, 504(%rbx)
	movq	%rbx, %rax
	subq	%rdi, %rax
	subq	%rax, %rsi
	addl	$512, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	leal	-63(%r15), %ecx
	testl	%r14d, %r14d
	je	.L469
	leal	-1(%r14), %r8d
	xorl	%eax, %eax
	movq	$-1, %r9
	xorl	%esi, %esi
	leaq	-7376(%rbp), %rdi
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L470:
	movq	%rdx, %rax
.L205:
	movl	264(%rdi,%rax,4), %edx
	cmpl	%esi, %edx
	jle	.L204
	movl	%edx, %esi
	movslq	%eax, %r9
.L204:
	leaq	1(%rax), %rdx
	cmpq	%rax, %r8
	jne	.L470
	movl	-7368(%rbp,%r9,4), %eax
	movl	%eax, -7560(%rbp)
.L203:
	movq	0(%r13), %rax
	movl	$0, (%rax)
	movl	$64, 16(%rax)
	cmpl	$4095, %ecx
	jle	.L471
	cmpl	$32767, %ecx
	jg	.L916
	movl	$32767, -7384(%rbp)
	movl	$-32768, %r14d
	movl	$50021, %ebx
	movl	$50020, %r15d
	movl	$15, -7476(%rbp)
	movl	$200084, %r8d
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L913:
	cmpl	$32, %r14d
	je	.L184
.L179:
	movslq	%r14d, %rdx
	movl	%r14d, %ebx
	addl	$1, %r14d
	movl	%esi, -7368(%rbp,%rdx,4)
	movl	%eax, -7240(%rbp,%rdx,4)
	movl	%r15d, -7112(%rbp,%rdx,4)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L911:
	movl	-7392(%rbp), %ecx
	subl	$1, %ecx
	movslq	%ecx, %rcx
	salq	$2, %rcx
.L456:
	movq	16(%r13), %rdx
	leaq	(%rdx,%rax,4), %rdx
	movl	(%rdx), %eax
	addq	$4, %rdx
	addq	%rdx, %rcx
	cmpq	%rcx, %rdx
	jb	.L171
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L917:
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jbe	.L170
.L171:
	cmpl	(%rdx), %eax
	je	.L917
.L170:
	cmpq	%rdx, %rcx
	je	.L918
	movl	-7392(%rbp), %esi
	addl	%esi, -7492(%rbp)
	movl	%r12d, %esi
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L184:
	testl	%esi, %esi
	je	.L459
.L187:
	movl	-7380(%rbp), %ebx
	xorl	%edx, %edx
	movl	$4, %ecx
	movl	$1, %r11d
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L828:
	cmpl	%edx, %ebx
	cmove	%r11d, %ecx
.L191:
	movslq	%edx, %r10
	cmpb	$0, 64(%r13,%r10)
	jne	.L193
	cmpl	(%r9,%r10,4), %eax
	je	.L919
.L193:
	addl	%ecx, %edx
	cmpl	%esi, %edx
	jne	.L828
.L197:
	testl	%r14d, %r14d
	je	.L466
	leaq	-7376(%rbp), %rcx
.L459:
	xorl	%edx, %edx
	movl	$-1, %ebx
	movl	$69632, %r8d
	.p2align 4,,10
	.p2align 3
.L190:
	movl	264(%rcx,%rdx,4), %edi
	cmpl	%r8d, %edi
	jge	.L189
	movl	%edi, %r8d
	movl	%edx, %ebx
.L189:
	addq	$1, %rdx
	cmpl	%edx, %r14d
	jg	.L190
.L188:
	movslq	%ebx, %rdx
	movl	%esi, -7368(%rbp,%rdx,4)
	movl	%eax, -7240(%rbp,%rdx,4)
	movl	%r15d, -7112(%rbp,%rdx,4)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L918:
	movb	$0, 64(%r13,%rdi)
	movl	%eax, (%r8)
	jmp	.L173
.L536:
	movl	$16, -7392(%rbp)
	movl	$60, %ecx
	movl	$1, %r15d
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L914:
	testl	%esi, %esi
	jne	.L187
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L919:
	leal	(%rcx,%r15), %ebx
	movl	%ebx, -7416(%rbp)
	testl	%r14d, %r14d
	je	.L467
	leal	-1(%r14), %r11d
	xorl	%esi, %esi
	movl	$-1, %ebx
	movl	$69632, %r10d
	leaq	-7376(%rbp), %rcx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%r9, %rsi
.L196:
	movl	264(%rcx,%rsi,4), %r9d
	cmpl	%r9d, %r10d
	jle	.L195
	movl	%r9d, %r10d
	movl	%esi, %ebx
.L195:
	leaq	1(%rsi), %r9
	cmpq	%rsi, %r11
	jne	.L468
.L194:
	movslq	%ebx, %rcx
	movl	%eax, -7240(%rbp,%rcx,4)
	movl	-7416(%rbp), %eax
	movl	%edx, -7368(%rbp,%rcx,4)
	movl	%eax, -7112(%rbp,%rcx,4)
	jmp	.L186
.L202:
	movq	-7536(%rbp), %rax
	movl	$0, -7384(%rbp)
	movl	$7, (%rax)
.L148:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L920
	movl	-7384(%rbp), %eax
	addq	$7544, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L471:
	.cfi_restore_state
	movl	$4095, -7384(%rbp)
	movl	$-4096, %r14d
	movl	$6007, %ebx
	movl	$6006, %r15d
	movl	$12, -7476(%rbp)
	movl	$24028, %r8d
.L206:
	xorl	%edi, %edi
	movq	%r8, -7392(%rbp)
	call	uprv_free_67@PLT
	movq	-7392(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L207
	movq	-7392(%rbp), %r8
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r8, %rdx
	call	memset@PLT
	movq	-7552(%rbp), %r10
	xorl	%r9d, %r9d
	movl	%r15d, -7392(%rbp)
	movq	%r13, -7400(%rbp)
	movl	-7384(%rbp), %r11d
	leaq	256(%r10), %r8
.L208:
	movl	-256(%r8), %eax
	addl	$1, %r9d
	leaq	-256(%r8), %rsi
	leaq	-252(%r8), %rcx
	.p2align 4,,10
	.p2align 3
.L211:
	leal	(%rax,%rax,8), %edx
	addq	$4, %rcx
	leal	(%rax,%rdx,4), %eax
	addl	-4(%rcx), %eax
	cmpq	%rcx, %r8
	jne	.L211
	xorl	%edx, %edx
	movl	%eax, %r13d
	movzbl	-7476(%rbp), %ecx
	divl	-7392(%rbp)
	sall	%cl, %r13d
	leal	1(%rdx), %eax
	movq	%rax, %rdi
	movl	(%r12,%rax,4), %eax
	movl	%edi, %edx
	movl	%eax, %ecx
	testl	%eax, %eax
	jne	.L212
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L214:
	leal	(%rdi,%rdx), %eax
	cltd
	idivl	%ebx
	movslq	%edx, %rax
	movl	(%r12,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.L216
.L212:
	movl	%ecx, %eax
	andl	%r14d, %eax
	cmpl	%eax, %r13d
	jne	.L214
	andl	%r11d, %ecx
	leal	-1(%rcx), %eax
	xorl	%ecx, %ecx
	cltq
	leaq	(%r10,%rax,4), %rax
	.p2align 4,,10
	.p2align 3
.L215:
	movl	(%rsi,%rcx,4), %r15d
	cmpl	%r15d, (%rax,%rcx,4)
	jne	.L214
	movl	%ecx, %r15d
	addq	$1, %rcx
	cmpq	$64, %rcx
	jne	.L215
	cmpl	$63, %r15d
	jne	.L214
	addq	$4, %r8
	cmpl	$65, %r9d
	jne	.L208
.L210:
	movq	-7400(%rbp), %r13
	movl	48(%r13), %eax
	movl	%eax, %edi
	sarl	$4, %edi
	movl	%edi, -7528(%rbp)
	cmpl	$143, %eax
	jle	.L217
	movl	-7492(%rbp), %eax
	movl	%ebx, -7568(%rbp)
	movl	%ebx, %r10d
	movq	%r12, %rbx
	movl	$8, -7392(%rbp)
	movq	-7552(%rbp), %r11
	subl	$15, %eax
	movl	$128, -7520(%rbp)
	movl	%eax, -7544(%rbp)
	movq	0(%r13), %rax
	movl	$0, -7416(%rbp)
	movq	%rax, -7488(%rbp)
	movl	-7380(%rbp), %eax
	movl	$4, -7408(%rbp)
	subl	$1, %eax
	movq	%r13, -7512(%rbp)
	movl	$64, -7400(%rbp)
	shrl	$2, %eax
	movq	%rax, -7576(%rbp)
	.p2align 4,,10
	.p2align 3
.L218:
	movslq	-7392(%rbp), %rax
	movq	-7512(%rbp), %rdi
	movzbl	64(%rdi,%rax), %edx
	movq	-7488(%rbp), %rdi
	leaq	(%rdi,%rax,4), %r15
	movl	(%r15), %r13d
	testb	%dl, %dl
	je	.L921
	leaq	0(,%r13,4), %rsi
	cmpb	$1, %dl
	je	.L922
	movl	(%rdi,%r13,4), %eax
.L904:
	movl	%eax, (%r15)
.L247:
	movl	-7408(%rbp), %edi
	addl	%edi, -7392(%rbp)
	movl	-7392(%rbp), %eax
	cmpl	%eax, -7528(%rbp)
	jle	.L923
	cmpl	%eax, -7380(%rbp)
	jne	.L218
	movl	-7544(%rbp), %edi
	cmpl	$4095, %edi
	jle	.L475
	cmpl	$32767, %edi
	jle	.L476
	cmpl	$131072, %edi
	movl	$200002, %edx
	movl	$1500006, %eax
	movl	$800012, %r12d
	cmovl	%edx, %eax
	movl	$131071, %edx
	setge	%dil
	movzbl	%dil, %edi
	movl	%eax, -7504(%rbp)
	movl	$6000028, %eax
	leal	17(,%rdi,4), %edi
	cmovge	%rax, %r12
	movl	$2097151, %eax
	movl	%edi, -7476(%rbp)
	cmovl	%edx, %eax
	movl	$200003, %edx
	movl	%eax, -7384(%rbp)
	movl	$1500007, %eax
	cmovl	%edx, %eax
	movl	%eax, %r10d
.L221:
	cmpl	-7568(%rbp), %r10d
	jle	.L223
	movq	%rbx, %rdi
	movl	%r10d, -7408(%rbp)
	movq	%r11, -7400(%rbp)
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L207
	movq	-7512(%rbp), %rax
	movl	-7408(%rbp), %r10d
	movq	-7400(%rbp), %r11
	movq	(%rax), %rax
	movl	%r10d, -7568(%rbp)
	movq	%rax, -7488(%rbp)
.L223:
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	%r10d, -7408(%rbp)
	movq	%r11, -7400(%rbp)
	call	memset@PLT
	movl	-7520(%rbp), %eax
	movq	-7400(%rbp), %r11
	movl	$0, -7400(%rbp)
	movl	-7384(%rbp), %r13d
	movl	-7408(%rbp), %r10d
	subl	$15, %eax
	movq	%r11, -7432(%rbp)
	leaq	60(%r11), %r12
	movl	%eax, -7496(%rbp)
	notl	%r13d
	.p2align 4,,10
	.p2align 3
.L224:
	movl	-60(%r12), %esi
	movl	-56(%r12), %r9d
	movl	-52(%r12), %edi
	movl	-48(%r12), %r8d
	leal	(%rsi,%rsi,8), %eax
	movl	-44(%r12), %r11d
	movl	-40(%r12), %ecx
	leal	(%rsi,%rax,4), %eax
	addl	$1, -7400(%rbp)
	addl	%r9d, %eax
	movl	%ecx, -7408(%rbp)
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	addl	%edi, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	addl	%r8d, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	addl	%r11d, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	addl	%ecx, %eax
	movl	-36(%r12), %ecx
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, -7416(%rbp)
	addl	%ecx, %eax
	movl	-32(%r12), %ecx
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, -7424(%rbp)
	addl	%ecx, %eax
	movl	-28(%r12), %ecx
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, -7440(%rbp)
	addl	%ecx, %eax
	movl	-24(%r12), %ecx
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, -7444(%rbp)
	addl	%ecx, %eax
	movl	-20(%r12), %ecx
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, -7448(%rbp)
	addl	%ecx, %eax
	movl	-16(%r12), %ecx
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, -7456(%rbp)
	addl	%ecx, %eax
	movl	-12(%r12), %ecx
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, -7464(%rbp)
	addl	%ecx, %eax
	movl	-8(%r12), %ecx
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, -7468(%rbp)
	addl	%ecx, %eax
	movl	-4(%r12), %ecx
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	movl	%ecx, -7472(%rbp)
	addl	%ecx, %eax
	movl	(%r12), %ecx
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	xorl	%edx, %edx
	movl	%ecx, -7480(%rbp)
	addl	%ecx, %eax
	movzbl	-7476(%rbp), %ecx
	movl	%eax, %r14d
	divl	-7504(%rbp)
	sall	%cl, %r14d
	leal	1(%rdx), %eax
	movq	%rax, %rcx
	movl	(%rbx,%rax,4), %eax
	movl	%ecx, %edx
	movl	%eax, %r15d
	testl	%eax, %eax
	jne	.L229
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L227:
	leal	(%rcx,%rdx), %eax
	cltd
	idivl	%r10d
	movslq	%edx, %rax
	movl	(%rbx,%rax,4), %r15d
	testl	%r15d, %r15d
	je	.L228
.L229:
	movl	%r15d, %eax
	andl	%r13d, %eax
	cmpl	%eax, %r14d
	jne	.L227
	andl	-7384(%rbp), %r15d
	movq	-7432(%rbp), %rax
	subl	$1, %r15d
	movslq	%r15d, %r15
	leaq	(%rax,%r15,4), %rax
	cmpl	(%rax), %esi
	jne	.L227
	cmpl	4(%rax), %r9d
	jne	.L227
	cmpl	8(%rax), %edi
	jne	.L227
	cmpl	12(%rax), %r8d
	jne	.L227
	cmpl	16(%rax), %r11d
	jne	.L227
	movl	-7408(%rbp), %r15d
	cmpl	20(%rax), %r15d
	jne	.L227
	movl	-7416(%rbp), %r15d
	cmpl	24(%rax), %r15d
	jne	.L227
	movl	-7424(%rbp), %r15d
	cmpl	28(%rax), %r15d
	jne	.L227
	movl	-7440(%rbp), %r15d
	cmpl	32(%rax), %r15d
	jne	.L227
	movl	-7444(%rbp), %r15d
	cmpl	36(%rax), %r15d
	jne	.L227
	movl	-7448(%rbp), %r15d
	cmpl	40(%rax), %r15d
	jne	.L227
	movl	-7456(%rbp), %r15d
	cmpl	44(%rax), %r15d
	jne	.L227
	movl	-7464(%rbp), %r15d
	cmpl	48(%rax), %r15d
	jne	.L227
	movl	-7468(%rbp), %r15d
	cmpl	52(%rax), %r15d
	jne	.L227
	movl	-7472(%rbp), %r15d
	cmpl	56(%rax), %r15d
	jne	.L227
	movl	-7480(%rbp), %r15d
	cmpl	60(%rax), %r15d
	jne	.L227
.L226:
	movl	-7496(%rbp), %edi
	addq	$4, %r12
	cmpl	%edi, -7400(%rbp)
	jne	.L224
	movl	-7520(%rbp), %eax
	movq	-7432(%rbp), %r11
	movl	$1, -7408(%rbp)
	movl	$16, -7400(%rbp)
	movl	%eax, -7416(%rbp)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L228:
	movslq	%edx, %rdx
	orl	-7400(%rbp), %r14d
	movl	%r14d, (%rbx,%rdx,4)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L216:
	movslq	%edx, %rdx
	orl	%r9d, %r13d
	addq	$4, %r8
	movl	%r13d, (%r12,%rdx,4)
	cmpl	$65, %r9d
	jne	.L208
	jmp	.L210
.L922:
	movq	-7512(%rbp), %rax
	movl	-7400(%rbp), %edi
	movl	$1, %edx
	movq	16(%rax), %rax
	addq	%rax, %rsi
	movq	%rax, -7440(%rbp)
	movl	(%rsi), %eax
	.p2align 4,,10
	.p2align 3
.L265:
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,4), %eax
	addl	(%rsi,%rdx,4), %eax
	addq	$1, %rdx
	cmpl	%edx, %edi
	jg	.L265
	leal	-1(%r10), %edi
	xorl	%edx, %edx
	movl	%eax, %r12d
	movzbl	-7476(%rbp), %ecx
	divl	%edi
	movl	%edi, -7444(%rbp)
	sall	%cl, %r12d
	leal	1(%rdx), %eax
	movq	%rax, %r9
	movl	(%rbx,%rax,4), %eax
	testl	%eax, %eax
	je	.L266
	movl	-7400(%rbp), %edi
	movl	-7384(%rbp), %ecx
	movl	%r9d, %edx
	movq	%r13, -7432(%rbp)
	movq	%r15, -7424(%rbp)
	notl	%ecx
	leal	-1(%rdi), %r8d
	movq	%r8, %rdi
	movl	%ecx, %r13d
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L267:
	leal	(%r9,%rdx), %eax
	cltd
	idivl	%r10d
	movslq	%edx, %rax
	movl	(%rbx,%rax,4), %eax
	testl	%eax, %eax
	je	.L924
.L271:
	movl	%eax, %ecx
	andl	%r13d, %ecx
	cmpl	%ecx, %r12d
	jne	.L267
	andl	-7384(%rbp), %eax
	subl	$1, %eax
	cltq
	leaq	(%r11,%rax,4), %r14
	movl	-7400(%rbp), %eax
	testl	%eax, %eax
	jle	.L488
	xorl	%ecx, %ecx
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L489:
	movq	%rax, %rcx
.L269:
	movl	(%rsi,%rcx,4), %eax
	cmpl	%eax, (%r14,%rcx,4)
	jne	.L267
	movl	%edi, %r15d
	leaq	1(%rcx), %rax
	subl	%ecx, %r15d
	cmpq	%rcx, %r8
	jne	.L489
.L268:
	testl	%r15d, %r15d
	jne	.L267
	movslq	%edx, %rdx
	movl	-7384(%rbp), %eax
	movq	-7424(%rbp), %r15
	andl	(%rbx,%rdx,4), %eax
	movq	-7432(%rbp), %r13
	leal	-1(%rax), %edx
	je	.L266
	movl	%edx, (%r15)
	jmp	.L247
.L921:
	movl	-7400(%rbp), %esi
	movl	%r13d, %eax
	cmpl	$1, %esi
	jle	.L231
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L232:
	leal	(%rax,%rax,8), %ecx
	addl	$1, %edx
	leal	(%rax,%rcx,4), %eax
	addl	%r13d, %eax
	cmpl	%edx, %esi
	jne	.L232
.L231:
	leal	-1(%r10), %edi
	xorl	%edx, %edx
	movl	%eax, %esi
	movzbl	-7476(%rbp), %ecx
	divl	%edi
	movl	%edi, -7444(%rbp)
	sall	%cl, %esi
	leal	1(%rdx), %eax
	movq	%rax, %rcx
	movl	(%rbx,%rax,4), %eax
	testl	%eax, %eax
	je	.L233
	movl	-7384(%rbp), %r12d
	movslq	-7400(%rbp), %r8
	movl	%ecx, %edx
	movl	%eax, %r9d
	movl	%r12d, %edi
	salq	$2, %r8
	notl	%edi
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L234:
	leal	(%rcx,%rdx), %eax
	cltd
	idivl	%r10d
	movslq	%edx, %rax
	movl	(%rbx,%rax,4), %r9d
	testl	%r9d, %r9d
	je	.L233
.L238:
	movl	%r9d, %eax
	andl	%edi, %eax
	cmpl	%eax, %esi
	jne	.L234
	andl	%r12d, %r9d
	leal	-1(%r9), %eax
	cltq
	leaq	(%r11,%rax,4), %r9
	leaq	(%r9,%r8), %rax
	cmpq	%rax, %r9
	jb	.L236
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L925:
	addq	$4, %r9
	cmpq	%r9, %rax
	jbe	.L235
.L236:
	cmpl	%r13d, (%r9)
	je	.L925
.L235:
	cmpq	%r9, %rax
	jne	.L234
	movq	-7576(%rbp), %rdi
	movslq	%edx, %rdx
	movl	-7384(%rbp), %eax
	movl	-7520(%rbp), %r9d
	andl	(%rbx,%rdx,4), %eax
	leaq	1(%rdi), %r8
	movl	-7400(%rbp), %edi
	movl	-7392(%rbp), %esi
	subl	$1, %eax
	cmpl	%esi, -7560(%rbp)
	sete	%r12b
	subl	%edi, %r9d
	subl	$2, %edi
	salq	$4, %r8
	addq	$2, %rdi
	addq	-7488(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L244:
	testl	%eax, %eax
	js	.L233
	testb	%r12b, %r12b
	je	.L239
	cmpl	%eax, -7416(%rbp)
	jle	.L904
	movl	-7392(%rbp), %ecx
	cmpl	%ecx, -7380(%rbp)
	jg	.L904
	movq	-7488(%rbp), %rdx
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L926:
	addq	$16, %rdx
	cmpq	%rdx, %r8
	je	.L904
.L242:
	cmpl	%eax, (%rdx)
	jne	.L926
	addl	$1, %eax
	cmpl	%r9d, %eax
	jle	.L246
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L243:
	addl	$1, %eax
	cmpl	%eax, %r9d
	jl	.L233
.L246:
	movslq	%eax, %rdx
	leaq	(%r11,%rdx,4), %rcx
	cmpl	%r13d, (%rcx)
	jne	.L243
	movl	$1, %edx
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L927:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	je	.L244
.L245:
	leal	(%rax,%rdx), %esi
	cmpl	%r13d, (%rcx,%rdx,4)
	je	.L927
	movl	%esi, %eax
	addl	$1, %eax
	cmpl	%eax, %r9d
	jge	.L246
	.p2align 4,,10
	.p2align 3
.L233:
	movl	-7400(%rbp), %eax
	leal	-1(%rax), %r14d
	movslq	-7520(%rbp), %rax
	movl	%eax, %ecx
	subl	%r14d, %ecx
	cmpl	%eax, %ecx
	jl	.L250
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L249:
	leal	-1(%rax), %edx
	subq	$1, %rax
	cmpl	%eax, %ecx
	jge	.L903
.L250:
	movl	%eax, %edx
	cmpl	%r13d, -4(%r11,%rax,4)
	je	.L249
.L903:
	movl	-7520(%rbp), %ecx
	subl	%edx, %ecx
.L248:
	movl	-7400(%rbp), %eax
	movl	%edx, (%r15)
	cmpl	%ecx, %eax
	jle	.L482
	subl	%ecx, %eax
	movl	%eax, %esi
	leal	-1(%rax), %eax
	cmpl	$2, %eax
	jbe	.L483
	movslq	-7520(%rbp), %rax
	movl	%esi, %edx
	movd	%r13d, %xmm5
	pshufd	$0, %xmm5, %xmm0
	shrl	$2, %edx
	leaq	(%r11,%rax,4), %rax
	movups	%xmm0, (%rax)
	cmpl	$1, %edx
	je	.L253
	movups	%xmm0, 16(%rax)
	cmpl	$2, %edx
	je	.L253
	movups	%xmm0, 32(%rax)
	cmpl	$3, %edx
	je	.L253
	movups	%xmm0, 48(%rax)
	cmpl	$4, %edx
	je	.L253
	movups	%xmm0, 64(%rax)
	cmpl	$5, %edx
	je	.L253
	movups	%xmm0, 80(%rax)
	cmpl	$6, %edx
	je	.L253
	movups	%xmm0, 96(%rax)
	cmpl	$7, %edx
	je	.L253
	movups	%xmm0, 112(%rax)
	cmpl	$8, %edx
	je	.L253
	movups	%xmm0, 128(%rax)
	cmpl	$9, %edx
	je	.L253
	movups	%xmm0, 144(%rax)
	cmpl	$10, %edx
	je	.L253
	movups	%xmm0, 160(%rax)
	cmpl	$11, %edx
	je	.L253
	movups	%xmm0, 176(%rax)
	cmpl	$12, %edx
	je	.L253
	movups	%xmm0, 192(%rax)
	cmpl	$13, %edx
	je	.L253
	movups	%xmm0, 208(%rax)
	cmpl	$14, %edx
	je	.L253
	movups	%xmm0, 224(%rax)
	cmpl	$15, %edx
	je	.L253
	movups	%xmm0, 240(%rax)
.L253:
	movl	-7520(%rbp), %eax
	movl	%esi, %edx
	andl	$-4, %edx
	addl	%edx, %ecx
	addl	%edx, %eax
	cmpl	%esi, %edx
	je	.L254
.L252:
	cltq
	movl	-7400(%rbp), %edi
	movl	%r13d, (%r11,%rax,4)
	leaq	0(,%rax,4), %rdx
	leal	1(%rcx), %eax
	cmpl	%eax, %edi
	jle	.L254
	addl	$2, %ecx
	movl	%r13d, 4(%r11,%rdx)
	cmpl	%ecx, %edi
	jle	.L254
	movl	%r13d, 8(%r11,%rdx)
.L254:
	addl	-7520(%rbp), %esi
	movl	%esi, -7456(%rbp)
.L251:
	movl	-7400(%rbp), %edi
	movl	-7520(%rbp), %eax
	subl	%edi, %eax
	subl	%edi, %esi
	addl	$1, %eax
	movl	%esi, -7448(%rbp)
	cmpl	%esi, %eax
	jg	.L492
	movslq	%eax, %rsi
	addl	%edi, %eax
	movl	-7384(%rbp), %r9d
	movl	%r14d, %edi
	movq	%rsi, -7424(%rbp)
	movl	%eax, -7432(%rbp)
	notl	%r9d
	.p2align 4,,10
	.p2align 3
.L255:
	movq	-7424(%rbp), %rcx
	movl	-7432(%rbp), %r8d
	movl	%ecx, %esi
	movl	(%r11,%rcx,4), %eax
	addl	$1, %esi
	movl	%esi, -7440(%rbp)
	leaq	(%r11,%rcx,4), %rsi
	addq	$1, %rcx
	movq	%rcx, -7424(%rbp)
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L256:
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,4), %eax
	addl	(%r11,%rdx,4), %eax
	addq	$1, %rdx
	cmpl	%edx, %r8d
	jg	.L256
	xorl	%edx, %edx
	movl	%eax, %r12d
	movzbl	-7476(%rbp), %ecx
	divl	-7444(%rbp)
	sall	%cl, %r12d
	leal	1(%rdx), %eax
	movq	%rax, %r8
	movl	(%rbx,%rax,4), %eax
	movl	%r8d, %edx
	testl	%eax, %eax
	jne	.L263
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L259:
	leal	(%r8,%rdx), %eax
	cltd
	idivl	%r10d
	movslq	%edx, %rax
	movl	(%rbx,%rax,4), %eax
	testl	%eax, %eax
	je	.L262
.L263:
	movl	%eax, %ecx
	andl	%r9d, %ecx
	cmpl	%ecx, %r12d
	jne	.L259
	andl	-7384(%rbp), %eax
	subl	$1, %eax
	cltq
	leaq	(%r11,%rax,4), %r13
	movl	-7400(%rbp), %eax
	testl	%eax, %eax
	jle	.L486
	xorl	%ecx, %ecx
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%rax, %rcx
.L261:
	movl	(%rsi,%rcx,4), %eax
	cmpl	%eax, 0(%r13,%rcx,4)
	jne	.L259
	movl	%r14d, %r15d
	leaq	1(%rcx), %rax
	subl	%ecx, %r15d
	cmpq	%rcx, %rdi
	jne	.L487
.L260:
	testl	%r15d, %r15d
	jne	.L259
	.p2align 4,,10
	.p2align 3
.L258:
	addl	$1, -7432(%rbp)
	movl	-7424(%rbp), %esi
	cmpl	%esi, -7448(%rbp)
	jge	.L255
	jmp	.L492
.L924:
	movq	-7424(%rbp), %r15
	movq	-7432(%rbp), %r13
.L266:
	movl	-7400(%rbp), %eax
	leal	-1(%rax), %r14d
	movl	%r14d, %ecx
	testl	%r14d, %r14d
	jle	.L272
	movslq	-7520(%rbp), %rax
	movslq	%r14d, %rdx
	movq	%rax, %r9
	subq	%rdx, %rax
	leaq	(%r11,%rax,4), %rdi
	.p2align 4,,10
	.p2align 3
.L276:
	movl	%r9d, %r8d
	xorl	%eax, %eax
	subl	%ecx, %r8d
	.p2align 4,,10
	.p2align 3
.L275:
	movl	(%rsi,%rax,4), %edx
	cmpl	%edx, (%rdi,%rax,4)
	jne	.L273
	addq	$1, %rax
	movl	%ecx, %edx
	subl	%eax, %edx
	testl	%edx, %edx
	jg	.L275
.L274:
	movl	-7400(%rbp), %eax
	movl	%r8d, (%r15)
	cmpl	%ecx, %eax
	jle	.L490
	subl	%ecx, %eax
	movl	%eax, %r12d
	movslq	%ecx, %rax
	leaq	4(%rax,%r13), %rdx
	leaq	0(,%rdx,4), %r15
	movslq	-7520(%rbp), %rdx
	leaq	0(,%rdx,4), %r13
	movq	-7440(%rbp), %rdx
	leaq	16(%r11,%r13), %r8
	leaq	(%r11,%r13), %rdi
	leaq	-16(%rdx,%r15), %rdx
	cmpq	%r8, %rdx
	movq	-7440(%rbp), %r8
	setnb	%r9b
	addq	%r15, %r8
	cmpq	%r8, %rdi
	setnb	%r8b
	orb	%r8b, %r9b
	je	.L278
	leal	-1(%r12), %r8d
	cmpl	$3, %r8d
	jbe	.L278
	movl	%r12d, %r8d
	xorl	%eax, %eax
	shrl	$2, %r8d
	salq	$4, %r8
.L279:
	movdqu	(%rdx,%rax), %xmm3
	movups	%xmm3, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %r8
	jne	.L279
	movl	%r12d, %eax
	andl	$-4, %eax
	leal	(%rcx,%rax), %edi
	movl	-7520(%rbp), %ecx
	leal	(%rcx,%rax), %edx
	cmpl	%r12d, %eax
	je	.L281
	movslq	%edi, %rax
	movl	-7400(%rbp), %r9d
	leal	1(%rdx), %r8d
	leal	1(%rdi), %ecx
	movl	(%rsi,%rax,4), %eax
	movslq	%edx, %rdx
	movl	%eax, (%r11,%rdx,4)
	cmpl	%ecx, %r9d
	jle	.L281
	movslq	%ecx, %rax
	movslq	%r8d, %rdx
	leaq	0(,%rax,4), %rcx
	movl	(%rsi,%rax,4), %eax
	leaq	0(,%rdx,4), %r8
	movl	%eax, (%r11,%rdx,4)
	leal	2(%rdi), %eax
	cmpl	%eax, %r9d
	jle	.L281
	movl	4(%rsi,%rcx), %eax
	movl	%eax, 4(%r11,%r8)
.L281:
	addl	-7520(%rbp), %r12d
	movl	%r12d, -7456(%rbp)
	movl	%r12d, %esi
.L277:
	movl	-7400(%rbp), %edi
	movl	-7520(%rbp), %eax
	subl	%edi, %eax
	subl	%edi, %esi
	addl	$1, %eax
	movl	%esi, -7448(%rbp)
	cmpl	%esi, %eax
	jg	.L492
	movslq	%eax, %rsi
	addl	%edi, %eax
	movl	-7384(%rbp), %r9d
	movl	%r14d, %edi
	movq	%rsi, -7424(%rbp)
	movl	%eax, -7432(%rbp)
	notl	%r9d
	.p2align 4,,10
	.p2align 3
.L283:
	movq	-7424(%rbp), %rcx
	movl	-7432(%rbp), %r8d
	movl	%ecx, %esi
	movl	(%r11,%rcx,4), %eax
	addl	$1, %esi
	movl	%esi, -7440(%rbp)
	leaq	(%r11,%rcx,4), %rsi
	addq	$1, %rcx
	movq	%rcx, -7424(%rbp)
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L284:
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,4), %eax
	addl	(%r11,%rdx,4), %eax
	addq	$1, %rdx
	cmpl	%edx, %r8d
	jg	.L284
	xorl	%edx, %edx
	movl	%eax, %r12d
	movzbl	-7476(%rbp), %ecx
	divl	-7444(%rbp)
	sall	%cl, %r12d
	leal	1(%rdx), %eax
	movq	%rax, %r8
	movl	(%rbx,%rax,4), %eax
	movl	%r8d, %edx
	testl	%eax, %eax
	jne	.L291
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L287:
	leal	(%r8,%rdx), %eax
	cltd
	idivl	%r10d
	movslq	%edx, %rax
	movl	(%rbx,%rax,4), %eax
	testl	%eax, %eax
	je	.L290
.L291:
	movl	%eax, %ecx
	andl	%r9d, %ecx
	cmpl	%ecx, %r12d
	jne	.L287
	andl	-7384(%rbp), %eax
	movl	-7400(%rbp), %r15d
	subl	$1, %eax
	cltq
	leaq	(%r11,%rax,4), %r13
	testl	%r15d, %r15d
	jle	.L493
	xorl	%ecx, %ecx
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%rax, %rcx
.L289:
	movl	(%rsi,%rcx,4), %eax
	cmpl	%eax, 0(%r13,%rcx,4)
	jne	.L287
	movl	%r14d, %r15d
	leaq	1(%rcx), %rax
	subl	%ecx, %r15d
	cmpq	%rcx, %rdi
	jne	.L494
.L288:
	testl	%r15d, %r15d
	jne	.L287
	.p2align 4,,10
	.p2align 3
.L286:
	addl	$1, -7432(%rbp)
	movl	-7424(%rbp), %esi
	cmpl	%esi, -7448(%rbp)
	jge	.L283
.L492:
	movl	-7456(%rbp), %eax
	movl	%eax, -7520(%rbp)
	jmp	.L247
.L475:
	movl	$6006, -7504(%rbp)
	movl	$24028, %r12d
	movl	$6007, %r10d
	movl	$4095, -7384(%rbp)
	movl	$12, -7476(%rbp)
	jmp	.L223
.L207:
	movq	-7536(%rbp), %rax
	xorl	%r12d, %r12d
	movl	$0, -7384(%rbp)
	movl	$7, (%rax)
.L209:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	jmp	.L148
.L923:
	movq	-7536(%rbp), %rax
	movq	-7512(%rbp), %r13
	movq	%rbx, %r12
	movl	$0, -7384(%rbp)
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L209
	movq	16(%r13), %rdi
	call	uprv_free_67@PLT
	movq	-7552(%rbp), %rax
	movq	%rax, 16(%r13)
	movl	-7492(%rbp), %eax
	movl	%eax, 24(%r13)
	movl	-7520(%rbp), %eax
	movl	%eax, 28(%r13)
	cmpl	$262159, %eax
	jle	.L293
	movq	-7536(%rbp), %rax
	movl	$8, (%rax)
	jmp	.L209
.L476:
	movl	$50020, -7504(%rbp)
	movl	$200084, %r12d
	movl	$50021, %r10d
	movl	$32767, -7384(%rbp)
	movl	$15, -7476(%rbp)
	jmp	.L221
.L916:
	cmpl	$131072, %ecx
	movl	$-131072, %edx
	movl	$-2097152, %eax
	movl	$1500006, %r12d
	cmovl	%edx, %eax
	movl	%r12d, %r15d
	movl	$800012, %edx
	movl	%eax, %r14d
	movl	$200002, %eax
	cmovl	%eax, %r15d
	movl	$6000028, %eax
	cmovl	%rdx, %rax
	movl	$131071, %edx
	movq	%rax, %r8
	movl	$2097151, %eax
	cmovl	%edx, %eax
	movl	$200003, %edx
	movl	%eax, -7384(%rbp)
	setge	%al
	movzbl	%al, %eax
	leal	17(,%rax,4), %eax
	movl	%eax, -7476(%rbp)
	movl	$1500007, %eax
	cmovl	%edx, %eax
	movl	%eax, %ebx
	jmp	.L206
.L849:
	movl	%r15d, %edx
.L746:
	movb	$0, 64(%r13,%rcx)
	movl	52(%r13), %eax
	movl	%eax, (%r9,%rcx,4)
	addq	$1, %rcx
	cmpl	%ecx, %edx
	jg	.L746
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L273:
	addq	$4, %rdi
	subl	$1, %ecx
	jne	.L276
	movl	-7520(%rbp), %r8d
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L290:
	movslq	%edx, %rdx
	orl	-7440(%rbp), %r12d
	movl	%r12d, (%rbx,%rdx,4)
	jmp	.L286
.L910:
	movl	-7380(%rbp), %edi
	movl	%eax, %ebx
	sarl	$4, %ebx
	sall	$4, %edi
	movl	%ebx, -7384(%rbp)
	cmpl	%edi, %eax
	jge	.L159
	cmpl	%ebx, -7380(%rbp)
	jle	.L164
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L466:
	movl	$-1, %ebx
	jmp	.L188
.L217:
	movq	-7536(%rbp), %rax
	movl	$0, -7384(%rbp)
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L209
	movq	16(%r13), %rdi
	call	uprv_free_67@PLT
	movq	-7552(%rbp), %rax
	movl	$128, 28(%r13)
	movl	%ebx, -7568(%rbp)
	movq	%rax, 16(%r13)
	movl	-7492(%rbp), %eax
	movl	%eax, 24(%r13)
.L293:
	movl	-7560(%rbp), %eax
	movl	$1048575, %esi
	testl	%eax, %eax
	js	.L294
	movslq	%eax, %rdx
	movq	0(%r13), %rax
	movq	-7552(%rbp), %rbx
	movslq	(%rax,%rdx,4), %rax
	movq	%rax, %rsi
	movl	(%rbx,%rax,4), %eax
	movl	%eax, 40(%r13)
.L294:
	movl	-7380(%rbp), %ebx
	movl	48(%r13), %eax
	movl	%esi, 32(%r13)
	sarl	$2, %ebx
	sarl	$6, %eax
	movl	%ebx, -7384(%rbp)
	cmpl	%eax, %ebx
	jge	.L928
	movl	-7380(%rbp), %eax
	movq	0(%r13), %rdx
	xorl	%ecx, %ecx
	movl	$-1, %r8d
	leaq	-6464(%rbp), %rbx
	leal	-1(%rax), %edi
	shrl	$2, %edi
	jmp	.L299
.L497:
	movl	$-1, %r8d
.L297:
	leal	16(%rax), %esi
	addq	$16, %rdx
	movl	%esi, -12(%rdx)
	leal	32(%rax), %esi
	addl	$48, %eax
	movl	%esi, -8(%rdx)
	movl	%eax, -4(%rdx)
	cmpq	%rcx, %rdi
	je	.L298
	movl	32(%r13), %esi
	addq	$1, %rcx
.L299:
	movl	(%rdx), %eax
	movl	%ecx, %r9d
	movw	%ax, (%rbx,%rcx,2)
	cmpl	%esi, %eax
	jne	.L497
	cmpl	$-1, %r8d
	je	.L498
	movl	12(%r13), %esi
	testl	%esi, %esi
	jns	.L297
	subl	%r8d, %r9d
	cmpl	$31, %r9d
	jne	.L297
	movl	%r8d, 12(%r13)
	jmp	.L297
.L493:
	movl	-7400(%rbp), %r15d
	jmp	.L288
.L262:
	movslq	%edx, %rdx
	orl	-7440(%rbp), %r12d
	movl	%r12d, (%rbx,%rdx,4)
	jmp	.L258
.L486:
	movl	-7400(%rbp), %r15d
	jmp	.L260
.L498:
	movl	%ecx, %r8d
	jmp	.L297
.L298:
	movl	$24028, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	memset@PLT
	movl	-7384(%rbp), %eax
	leaq	-6400(%rbp), %r11
	movl	$2928614291, %r10d
	leal	-31(%rax), %r14d
.L300:
	movzwl	-64(%r11), %ecx
	addl	$1, %r15d
	leaq	-64(%r11), %rsi
	leaq	-62(%r11), %rax
	.p2align 4,,10
	.p2align 3
.L302:
	leal	(%rcx,%rcx,8), %edx
	addq	$2, %rax
	leal	(%rcx,%rdx,4), %edx
	movzwl	-2(%rax), %ecx
	addl	%edx, %ecx
	cmpq	%rax, %r11
	jne	.L302
	movl	%ecx, %eax
	movl	%ecx, %edi
	imulq	$1464550953, %rax, %rax
	sall	$12, %edi
	shrq	$43, %rax
	imull	$6006, %eax, %eax
	subl	%eax, %ecx
	leal	1(%rcx), %eax
	movl	(%r12,%rax,4), %edx
	movq	%rax, %rcx
	testl	%edx, %edx
	jne	.L303
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L305:
	addl	%ecx, %eax
	movq	%rax, %rdx
	imulq	%r10, %rax
	shrq	$44, %rax
	imull	$6007, %eax, %eax
	subl	%eax, %edx
	movslq	%edx, %r8
	movl	(%r12,%r8,4), %edx
	movq	%r8, %rax
	testl	%edx, %edx
	je	.L307
.L303:
	movl	%edx, %r8d
	andl	$-4096, %r8d
	cmpl	%r8d, %edi
	jne	.L305
	andl	$4095, %edx
	subl	$1, %edx
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,2), %r8
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L306:
	movzwl	(%rsi,%rdx,2), %r9d
	cmpw	%r9w, (%r8,%rdx,2)
	jne	.L305
	movl	%edx, %r9d
	addq	$1, %rdx
	cmpq	$32, %rdx
	jne	.L306
	cmpl	$31, %r9d
	jne	.L305
.L304:
	addq	$2, %r11
	cmpl	%r14d, %r15d
	jne	.L300
	movl	-7380(%rbp), %esi
	movl	12(%r13), %eax
	movl	48(%r13), %edi
	cmpl	$4096, %esi
	movl	%eax, -7392(%rbp)
	movl	$0, %eax
	cmove	%esi, %eax
	sarl	$4, %edi
	movl	%edi, -7432(%rbp)
	movl	%eax, -7380(%rbp)
	cmpl	%eax, %edi
	jle	.L501
	movq	0(%r13), %rsi
	movslq	%eax, %r15
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	movb	$0, -7476(%rbp)
	movq	%rsi, -7400(%rbp)
	leaq	128(%rsi,%r15,4), %rdi
.L310:
	leaq	-128(%rdi), %r10
	movl	32(%r13), %r8d
	movl	%r15d, %r11d
	movl	$1, %ecx
	movq	%r10, %rax
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L312:
	movl	(%rax), %edx
	orl	%edx, %esi
	cmpl	%edx, %r8d
	cmovne	%r9d, %ecx
	addq	$4, %rax
	cmpq	%rax, %rdi
	jne	.L312
	testb	%cl, %cl
	je	.L313
	movl	-7392(%rbp), %eax
	movb	$0, 64(%r13,%r15)
	testl	%eax, %eax
	js	.L930
.L314:
	addq	$32, %r15
	subq	$-128, %rdi
	cmpl	%r15d, -7432(%rbp)
	jg	.L310
.L309:
	movl	-7432(%rbp), %eax
	subl	-7380(%rbp), %eax
	sarl	$5, %eax
	leal	31(%rax), %edx
	sarl	$5, %edx
	addl	-7384(%rbp), %edx
	movl	%edx, -7392(%rbp)
	addl	%r14d, %edx
	leal	(%rdx,%rax), %r14d
	leal	2(%r14,%r14), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 56(%r13)
	testq	%rax, %rax
	je	.L906
	movl	-7384(%rbp), %r8d
	movq	-6464(%rbp), %rdx
	addl	%r8d, %r8d
	movslq	%r8d, %rdi
	movq	%rdi, -7552(%rbp)
	leaq	8(%rax), %rdi
	movq	%rdx, (%rax)
	movl	%r8d, %edx
	andq	$-8, %rdi
	movq	-8(%rbx,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	movq	%rax, %rcx
	subq	%rax, %rbx
	leal	-30(%r14), %eax
	addl	%r8d, %ecx
	movq	%rbx, %rsi
	shrl	$3, %ecx
	rep movsq
	cmpl	$4095, %eax
	jle	.L502
	cmpl	$32767, %eax
	jle	.L503
	cmpl	$131072, %eax
	movl	$800012, %ecx
	movl	$6000028, %edx
	movl	$1500007, %eax
	cmovl	%rcx, %rdx
	movl	$131071, %ecx
	setge	%bl
	movzbl	%bl, %ebx
	movq	%rdx, %r15
	movl	$2097151, %edx
	leal	17(,%rbx,4), %ebx
	cmovl	%ecx, %edx
	movl	%ebx, -7448(%rbp)
	movl	%edx, -7384(%rbp)
	movl	$200003, %edx
	cmovl	%edx, %eax
	movl	%eax, %ebx
.L325:
	cmpl	%ebx, -7568(%rbp)
	jge	.L327
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	movq	%r15, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L906
.L327:
	xorl	%esi, %esi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	memset@PLT
	cmpb	$0, -7476(%rbp)
	jne	.L931
	movl	$0, -7444(%rbp)
	movl	$0, -7424(%rbp)
	movl	$0, -7468(%rbp)
	movl	$0, -7440(%rbp)
	movq	$0, -7416(%rbp)
.L329:
	movl	12(%r13), %eax
	movslq	-7380(%rbp), %rdi
	movl	%eax, -7464(%rbp)
	cmpl	%edi, -7432(%rbp)
	jle	.L333
	movl	%eax, -7480(%rbp)
	movl	-7440(%rbp), %eax
	leaq	-4416(%rbp), %rsi
	movq	%rdi, -7400(%rbp)
	subl	$1, %eax
	movq	%r13, -7456(%rbp)
	movl	%eax, -7496(%rbp)
	movl	-7444(%rbp), %eax
	movq	%rsi, -7528(%rbp)
	leal	-1(%rax), %edi
	movq	%rsi, -7488(%rbp)
	movl	-7392(%rbp), %esi
	movl	%edi, -7512(%rbp)
.L417:
	movq	-7400(%rbp), %r14
	movq	-7456(%rbp), %r11
	movl	-7464(%rbp), %eax
	movzbl	64(%r11,%r14), %edx
	movl	%r14d, %r10d
	movl	%r14d, %r13d
	testb	%dl, %dl
	sete	%cl
	shrl	$31, %eax
	andb	%al, %cl
	movb	%cl, -7492(%rbp)
	je	.L334
	movl	$0, -7464(%rbp)
	movq	(%r11), %r9
	movq	%r11, %rcx
	cmpl	$65535, 32(%rcx)
	leaq	(%r9,%r14,4), %r11
	movl	(%r11), %eax
	jg	.L336
.L335:
	movq	-7456(%rbp), %rcx
	leal	1(%r10), %edx
	movslq	%edx, %rdx
	movq	56(%rcx), %r8
	movq	-7400(%rbp), %rcx
	leaq	(%r9,%rdx,4), %rdx
	leaq	128(%r9,%rcx,4), %r10
.L339:
	leal	(%rax,%rax,8), %ecx
	addq	$4, %rdx
	leal	(%rax,%rcx,4), %eax
	addl	-4(%rdx), %eax
	cmpq	%rdx, %r10
	jne	.L339
	movzbl	-7448(%rbp), %ecx
	movl	%eax, %r15d
	xorl	%edx, %edx
	sall	%cl, %r15d
	leal	-1(%rbx), %ecx
	divl	%ecx
	movl	%ecx, -7520(%rbp)
	leal	1(%rdx), %eax
	movq	%rax, %r14
	movl	(%r12,%rax,4), %eax
	testl	%eax, %eax
	je	.L340
	movl	-7384(%rbp), %ecx
	movl	%r14d, %edx
	notl	%ecx
	movl	%ecx, -7408(%rbp)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L341:
	leal	(%r14,%rdx), %eax
	cltd
	idivl	%ebx
	movslq	%edx, %rax
	movl	(%r12,%rax,4), %eax
	testl	%eax, %eax
	je	.L340
.L344:
	movl	-7408(%rbp), %ecx
	andl	%eax, %ecx
	cmpl	%ecx, %r15d
	jne	.L341
	andl	-7384(%rbp), %eax
	subl	$1, %eax
	cltq
	leaq	(%r8,%rax,2), %r10
	xorl	%eax, %eax
.L342:
	movzwl	(%r10,%rax,2), %ecx
	cmpl	(%r11,%rax,4), %ecx
	jne	.L341
	movl	%eax, %ecx
	addq	$1, %rax
	cmpq	$32, %rax
	jne	.L342
	cmpl	$31, %ecx
	jne	.L341
	movslq	%edx, %rdx
	movl	-7384(%rbp), %eax
	andl	(%r12,%rdx,4), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, -7408(%rbp)
	je	.L340
.L337:
	movl	-7480(%rbp), %r8d
	testl	%r8d, %r8d
	jns	.L414
	cmpb	$0, -7492(%rbp)
	jne	.L932
.L414:
	movq	-7488(%rbp), %rax
	movzwl	-7408(%rbp), %ecx
	addq	$32, -7400(%rbp)
	movw	%cx, (%rax)
	addq	$2, %rax
	movq	-7400(%rbp), %rcx
	movq	%rax, -7488(%rbp)
	cmpl	%ecx, -7432(%rbp)
	jg	.L417
	movq	-7456(%rbp), %r13
.L415:
	movl	-7432(%rbp), %eax
	movl	-7480(%rbp), %edx
	subl	$1, %eax
	subl	-7380(%rbp), %eax
	shrl	$5, %eax
	addl	$1, %eax
	movl	%eax, -7408(%rbp)
	testl	%edx, %edx
	jns	.L420
	movl	$32767, 12(%r13)
.L420:
	cmpl	$32798, %esi
	jg	.L419
	movq	56(%r13), %rdi
	movl	$32, %r14d
	movl	%esi, %r15d
	movq	-7552(%rbp), %rax
	movl	$0, -7380(%rbp)
	addq	%rdi, %rax
	movq	%r13, -7456(%rbp)
	movq	%rdi, %r13
	movq	%rax, -7432(%rbp)
	movslq	-7392(%rbp), %rax
	leaq	(%rdi,%rax,2), %rax
	movq	%rax, -7464(%rbp)
.L450:
	movslq	-7380(%rbp), %rax
	movl	-7408(%rbp), %r10d
	movq	-7528(%rbp), %rdi
	leaq	(%rax,%rax), %rcx
	subl	%eax, %r10d
	movq	%rax, -7424(%rbp)
	leaq	(%rdi,%rcx), %rsi
	cmpl	%r14d, %r10d
	jl	.L421
	movzwl	(%rsi), %eax
	leaq	2(%rdi,%rcx), %rdx
	leaq	64(%rdi,%rcx), %rcx
.L422:
	leal	(%rax,%rax,8), %edi
	addq	$2, %rdx
	leal	(%rax,%rdi,4), %edi
	movzwl	-2(%rdx), %eax
	addl	%edi, %eax
	cmpq	%rcx, %rdx
	jne	.L422
	leal	-1(%rbx), %edi
	xorl	%edx, %edx
	movl	%eax, %r8d
	movzbl	-7448(%rbp), %ecx
	divl	%edi
	movl	-7384(%rbp), %edi
	sall	%cl, %r8d
	notl	%edi
	leal	1(%rdx), %eax
	movq	%rax, %rcx
	movl	(%r12,%rax,4), %eax
	movl	%ecx, %edx
	testl	%eax, %eax
	jne	.L429
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L425:
	leal	(%rcx,%rdx), %eax
	cltd
	idivl	%ebx
	movslq	%edx, %rax
	movl	(%r12,%rax,4), %eax
	testl	%eax, %eax
	je	.L428
.L429:
	movl	%eax, %r9d
	andl	%edi, %r9d
	cmpl	%r9d, %r8d
	jne	.L425
	andl	-7384(%rbp), %eax
	subl	$1, %eax
	cltq
	leaq	0(%r13,%rax,2), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L426:
	movzwl	(%rsi,%rax,2), %r11d
	cmpw	%r11w, (%r9,%rax,2)
	jne	.L425
	movl	%eax, %r10d
	addq	$1, %rax
	cmpq	$32, %rax
	jne	.L426
	cmpl	$31, %r10d
	jne	.L425
	movslq	%edx, %rdx
	addl	%r14d, -7380(%rbp)
	movl	-7384(%rbp), %eax
	andl	(%r12,%rdx,4), %eax
	leal	-1(%rax), %edi
	movl	%edi, -7400(%rbp)
	je	.L424
.L441:
	movq	-7432(%rbp), %rax
	movzwl	-7400(%rbp), %edi
	addq	$2, %rax
	movw	%di, -2(%rax)
	movl	-7380(%rbp), %edi
	movq	%rax, -7432(%rbp)
	cmpl	%edi, -7408(%rbp)
	jg	.L450
	movl	%r15d, -7392(%rbp)
	movq	-7456(%rbp), %r13
.L332:
	movq	-7416(%rbp), %rdi
	call	uprv_free_67@PLT
	movl	-7392(%rbp), %eax
	movl	%eax, -7384(%rbp)
	jmp	.L296
.L929:
	movslq	%eax, %r8
.L307:
	orl	%r15d, %edi
	movl	%edi, (%r12,%r8,4)
	jmp	.L304
.L488:
	movl	-7400(%rbp), %r15d
	jmp	.L268
.L467:
	movl	$-1, %ebx
	jmp	.L194
.L928:
	movl	$32767, 12(%r13)
.L296:
	movl	-7540(%rbp), %eax
	movl	%eax, 48(%r13)
	jmp	.L209
.L272:
	movl	-7520(%rbp), %r8d
	subl	%r14d, %r8d
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L313:
	cmpl	$65535, %esi
	jbe	.L933
	movb	$3, 64(%r13,%r15)
	addl	$36, %r14d
	movb	$1, -7476(%rbp)
	jmp	.L314
.L334:
	movl	-7464(%rbp), %eax
	notl	%eax
	shrl	$31, %eax
	movl	%eax, -7492(%rbp)
	movl	-7480(%rbp), %eax
	movl	%eax, -7408(%rbp)
	testb	%dl, %dl
	je	.L337
	movq	-7400(%rbp), %rax
	movq	(%r11), %r9
	leaq	(%r9,%rax,4), %r11
	movl	(%r11), %eax
	cmpb	$1, %dl
	jne	.L338
	movl	%eax, -7408(%rbp)
	jmp	.L337
.L930:
	cmpl	$65535, %esi
	ja	.L315
	movl	$0, -7392(%rbp)
	addl	$32, %r14d
	jmp	.L314
.L428:
	addl	%r14d, -7380(%rbp)
.L424:
	movl	-7392(%rbp), %eax
	xorl	%ecx, %ecx
	movl	%eax, -7400(%rbp)
	cmpl	%r15d, %eax
	jne	.L934
.L435:
	cmpl	%ecx, %r14d
	jle	.L532
	movslq	%r15d, %rax
	movq	-7528(%rbp), %rsi
	movl	%ecx, -7440(%rbp)
	leaq	0(%r13,%rax,2), %rdi
	leal	-1(%r14), %eax
	subl	%ecx, %eax
	leaq	2(%rax,%rax), %rdx
	movslq	%ecx, %rax
	addq	-7424(%rbp), %rax
	leaq	(%rsi,%rax,2), %rsi
	call	memcpy@PLT
	movl	-7440(%rbp), %ecx
	leal	(%r15,%r14), %eax
	subl	%ecx, %eax
	movl	%eax, -7424(%rbp)
	movl	%eax, %esi
.L439:
	movl	-7392(%rbp), %edi
	leal	-32(%r15), %eax
	leal	-31(%r15), %r10d
	cmpl	%eax, %edi
	leal	-32(%rsi), %eax
	cmovg	%edi, %r10d
	cmpl	%r10d, %eax
	jl	.L534
	leal	-1(%rbx), %eax
	movl	%r14d, -7468(%rbp)
	movl	%eax, -7440(%rbp)
	movslq	%r10d, %rax
	leaq	64(%r13,%rax,2), %r9
	leal	-31(%rsi), %eax
	movl	%eax, -7444(%rbp)
	movl	-7384(%rbp), %eax
	movl	%eax, %r8d
	movl	%eax, %r14d
	notl	%r8d
.L442:
	movzwl	-64(%r9), %eax
	addl	$1, %r10d
	leaq	-64(%r9), %rsi
	leaq	-62(%r9), %rdx
	.p2align 4,,10
	.p2align 3
.L443:
	leal	(%rax,%rax,8), %ecx
	addq	$2, %rdx
	leal	(%rax,%rcx,4), %ecx
	movzwl	-2(%rdx), %eax
	addl	%ecx, %eax
	cmpq	%r9, %rdx
	jne	.L443
	xorl	%edx, %edx
	movl	%eax, %r11d
	movzbl	-7448(%rbp), %ecx
	divl	-7440(%rbp)
	sall	%cl, %r11d
	leal	1(%rdx), %eax
	movq	%rax, %rdi
	movl	(%r12,%rax,4), %eax
	movl	%edi, %edx
	movl	%eax, %ecx
	testl	%eax, %eax
	jne	.L449
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L446:
	leal	(%rdi,%rdx), %eax
	cltd
	idivl	%ebx
	movslq	%edx, %rax
	movl	(%r12,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.L448
.L449:
	movl	%ecx, %eax
	andl	%r8d, %eax
	cmpl	%eax, %r11d
	jne	.L446
	andl	%r14d, %ecx
	leal	-1(%rcx), %eax
	xorl	%ecx, %ecx
	cltq
	leaq	0(%r13,%rax,2), %rax
	.p2align 4,,10
	.p2align 3
.L447:
	movzwl	(%rsi,%rcx,2), %r15d
	cmpw	%r15w, (%rax,%rcx,2)
	jne	.L446
	movl	%ecx, %r15d
	addq	$1, %rcx
	cmpq	$32, %rcx
	jne	.L447
	cmpl	$31, %r15d
	jne	.L446
.L445:
	addq	$2, %r9
	cmpl	-7444(%rbp), %r10d
	jne	.L442
	movl	-7468(%rbp), %r14d
	movl	-7424(%rbp), %r15d
	jmp	.L441
.L448:
	movslq	%edx, %rdx
	orl	%r10d, %r11d
	movl	%r11d, (%r12,%rdx,4)
	jmp	.L445
.L933:
	movq	-7400(%rbp), %rsi
	leal	1(%r11), %edx
	movl	-128(%rdi), %eax
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx,4), %rdx
.L317:
	leal	(%rax,%rax,8), %ecx
	addq	$4, %rdx
	leal	(%rax,%rcx,4), %eax
	addl	-4(%rdx), %eax
	cmpq	%rdx, %rdi
	jne	.L317
	movl	%eax, %edx
	movl	%eax, %r11d
	imulq	$1464550953, %rdx, %rdx
	sall	$12, %r11d
	shrq	$43, %rdx
	imull	$6006, %edx, %edx
	subl	%edx, %eax
	addl	$1, %eax
	movl	(%r12,%rax,4), %edx
	movq	%rax, %rcx
	testl	%edx, %edx
	je	.L318
	cltq
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L319:
	addl	%ecx, %eax
	movl	$2928614291, %esi
	movq	%rax, %rdx
	imulq	%rsi, %rax
	shrq	$44, %rax
	imull	$6007, %eax, %eax
	subl	%eax, %edx
	movslq	%edx, %rax
	movslq	%eax, %rdx
	movl	(%r12,%rdx,4), %edx
	testl	%edx, %edx
	je	.L318
.L322:
	movl	%edx, %esi
	andl	$-4096, %esi
	cmpl	%esi, %r11d
	jne	.L319
	andl	$4095, %edx
	subl	$1, %edx
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,2), %r8
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L320:
	movzwl	(%r8,%rdx,2), %esi
	cmpl	(%r10,%rdx,4), %esi
	jne	.L319
	movl	%edx, %esi
	addq	$1, %rdx
	cmpq	$32, %rdx
	jne	.L320
	cmpl	$31, %esi
	jne	.L319
	movl	(%r12,%rax,4), %eax
	andl	$4095, %eax
	leal	-1(%rax), %edx
	je	.L318
	movb	$1, 64(%r13,%r15)
	movl	%edx, -128(%rdi)
	jmp	.L314
.L421:
	movl	%r15d, %r14d
	movl	-7392(%rbp), %r11d
	subl	%r10d, %r14d
	cmpl	%r14d, %r11d
	jg	.L527
	leal	-1(%r10), %r9d
	movq	-7464(%rbp), %rcx
	movq	%r9, %r8
.L434:
	testl	%r10d, %r10d
	jle	.L528
	xorl	%eax, %eax
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L935:
	movl	%r8d, %edi
	leaq	1(%rax), %rdx
	subl	%eax, %edi
	cmpq	%rax, %r9
	je	.L430
	movq	%rdx, %rax
.L432:
	movzwl	(%rsi,%rax,2), %edi
	cmpw	%di, (%rcx,%rax,2)
	je	.L935
.L431:
	addl	$1, %r11d
	addq	$2, %rcx
	cmpl	%r11d, %r14d
	jge	.L434
.L527:
	movl	-7408(%rbp), %eax
	movl	%r10d, %r14d
	movl	%eax, -7380(%rbp)
	jmp	.L424
.L528:
	movl	%r10d, %edi
.L430:
	testl	%edi, %edi
	jne	.L431
	testl	%r11d, %r11d
	js	.L527
	movq	-7432(%rbp), %rax
	movl	%r15d, -7392(%rbp)
	movq	-7456(%rbp), %r13
	movw	%r11w, (%rax)
	jmp	.L332
.L906:
	movq	-7536(%rbp), %rax
	movl	$0, -7384(%rbp)
	movl	$7, (%rax)
	jmp	.L296
.L932:
	movq	-7456(%rbp), %rax
	movl	-7408(%rbp), %edx
	addq	$32, -7400(%rbp)
	movq	-7400(%rbp), %rcx
	movl	%edx, 12(%rax)
	movq	-7488(%rbp), %rax
	movw	%dx, (%rax)
	addq	$2, %rax
	movq	%rax, -7488(%rbp)
	cmpl	%ecx, -7432(%rbp)
	jle	.L526
	movl	%edx, -7480(%rbp)
	jmp	.L417
.L338:
	cmpb	$2, %dl
	je	.L335
.L336:
	movq	-7456(%rbp), %rax
	movslq	%esi, %r10
	movq	56(%rax), %r8
	movq	-7400(%rbp), %rax
	leaq	(%r9,%rax,4), %rdx
	leaq	(%r10,%r10), %rax
	leaq	(%r8,%rax), %r9
	movq	%rax, -7560(%rbp)
	leaq	72(%r8,%rax), %r14
	movq	%r9, %r15
	movq	%r9, %rax
.L373:
	movl	(%rdx), %r13d
	movl	4(%rdx), %ecx
	addq	$18, %rax
	addq	$32, %rdx
	movl	-24(%rdx), %r11d
	movw	%r13w, -16(%rax)
	shrl	$2, %r13d
	movw	%cx, -14(%rax)
	shrl	$4, %ecx
	andl	$49152, %r13d
	andl	$12288, %ecx
	movw	%r11w, -12(%rax)
	orl	%r13d, %ecx
	movl	%r11d, %r13d
	movl	-20(%rdx), %r11d
	shrl	$6, %r13d
	andl	$3072, %r13d
	movw	%r11w, -10(%rax)
	orl	%r13d, %ecx
	movl	%r11d, %r13d
	movl	-16(%rdx), %r11d
	shrl	$8, %r13d
	andl	$768, %r13d
	movw	%r11w, -8(%rax)
	orl	%r13d, %ecx
	movl	%r11d, %r13d
	shrl	$10, %r13d
	andl	$192, %r13d
	orl	%r13d, %ecx
	movl	-12(%rdx), %r13d
	movl	%r13d, %r11d
	movw	%r13w, -6(%rax)
	movl	-8(%rdx), %r13d
	shrl	$12, %r11d
	andl	$48, %r11d
	movw	%r13w, -4(%rax)
	orl	%r11d, %ecx
	movl	%r13d, %r11d
	shrl	$14, %r11d
	andl	$12, %r11d
	orl	%ecx, %r11d
	movl	-4(%rdx), %ecx
	movw	%cx, -2(%rax)
	shrl	$16, %ecx
	andl	$3, %ecx
	orl	%r11d, %ecx
	movw	%cx, -18(%rax)
	cmpq	%rax, %r14
	jne	.L373
	movl	-7444(%rbp), %eax
	leal	1(%rsi), %edx
	movslq	%edx, %rdx
	leal	(%rax,%rsi), %ecx
	movzwl	(%r9), %eax
.L374:
	leal	(%rax,%rax,8), %r11d
	leal	(%rax,%r11,4), %r11d
	movzwl	(%r8,%rdx,2), %eax
	addq	$1, %rdx
	addl	%r11d, %eax
	cmpl	%edx, %ecx
	jg	.L374
	movzbl	-7468(%rbp), %ecx
	movl	%eax, %edx
	sall	%cl, %edx
	movq	-7416(%rbp), %rcx
	movl	%edx, %r11d
	xorl	%edx, %edx
	divl	-7496(%rbp)
	leal	1(%rdx), %r14d
	movslq	%r14d, %rax
	movl	%r14d, %edx
	movl	(%rcx,%rax,4), %eax
	movl	-7424(%rbp), %ecx
	notl	%ecx
	testl	%eax, %eax
	je	.L379
	movq	%r12, -7408(%rbp)
	movl	%ecx, %r12d
	movl	%esi, -7472(%rbp)
	movq	-7416(%rbp), %rsi
	movl	%ebx, -7504(%rbp)
	movl	%r11d, %ebx
	movq	%r10, -7520(%rbp)
	movl	-7440(%rbp), %r10d
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L377:
	leal	(%r14,%rdx), %eax
	cltd
	idivl	%r10d
	movslq	%edx, %rax
	movl	(%rsi,%rax,4), %eax
	testl	%eax, %eax
	je	.L936
.L380:
	movl	%eax, %ecx
	andl	%r12d, %ecx
	cmpl	%ecx, %ebx
	jne	.L377
	andl	-7424(%rbp), %eax
	subl	$1, %eax
	cltq
	leaq	(%r8,%rax,2), %r13
	movl	-7444(%rbp), %eax
	testl	%eax, %eax
	je	.L884
	xorl	%eax, %eax
	jmp	.L378
.L519:
	movq	%rcx, %rax
.L378:
	movzwl	(%r9,%rax,2), %ecx
	cmpw	%cx, 0(%r13,%rax,2)
	jne	.L377
	leaq	1(%rax), %rcx
	cmpq	%rdi, %rax
	jne	.L519
	cmpl	%eax, -7512(%rbp)
	jne	.L377
.L884:
	movq	-7408(%rbp), %r12
	movl	-7472(%rbp), %esi
	movl	-7504(%rbp), %ebx
	movq	-7520(%rbp), %r10
.L376:
	testl	%edx, %edx
	js	.L381
	movq	-7416(%rbp), %rcx
	movslq	%edx, %rdx
	movl	-7424(%rbp), %eax
	andl	(%rcx,%rdx,4), %eax
	leal	-1(%rax), %edx
	je	.L381
	orb	$-128, %dh
	movl	%edx, -7408(%rbp)
	jmp	.L337
.L936:
	movq	-7408(%rbp), %r12
	movl	-7472(%rbp), %esi
	movl	-7504(%rbp), %ebx
	movq	-7520(%rbp), %r10
.L379:
	notl	%edx
	jmp	.L376
.L315:
	movb	%cl, -7476(%rbp)
	addl	$36, %r14d
	movl	$0, -7392(%rbp)
	jmp	.L314
.L278:
	movq	%rax, %rdx
	negq	%rdx
	leaq	0(%r13,%rdx,4), %rdx
	addq	%r11, %rdx
.L282:
	movl	(%rsi,%rax,4), %ecx
	movl	%ecx, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, -7400(%rbp)
	jg	.L282
	jmp	.L281
.L318:
	movb	$2, 64(%r13,%r15)
	addl	$32, %r14d
	jmp	.L314
.L502:
	movl	$12, -7448(%rbp)
	movl	$24028, %r15d
	movl	$6007, %ebx
	movl	$4095, -7384(%rbp)
	jmp	.L327
.L381:
	movl	-7392(%rbp), %eax
	movl	%eax, %ecx
	orb	$-128, %ch
	movl	%ecx, -7408(%rbp)
	cmpl	%esi, %eax
	jne	.L937
.L388:
	leal	36(%rsi), %eax
	movl	%eax, -7472(%rbp)
.L393:
	movl	-7392(%rbp), %ecx
	leal	-32(%rsi), %edx
	leal	-31(%rsi), %eax
	cmpl	%edx, %ecx
	movl	-7472(%rbp), %edx
	cmovg	%ecx, %eax
	movl	%eax, -7504(%rbp)
	movl	%eax, %ecx
	leal	-32(%rdx), %eax
	cmpl	%ecx, %eax
	jl	.L395
	leal	-1(%rbx), %eax
	movl	-7384(%rbp), %r11d
	movl	%eax, -7560(%rbp)
	movslq	%ecx, %rax
	leaq	64(%r8,%rax,2), %r13
	leal	-31(%rdx), %eax
	notl	%r11d
	movl	%eax, -7520(%rbp)
.L396:
	addl	$1, -7504(%rbp)
	movzwl	-64(%r13), %eax
	leaq	-64(%r13), %r9
	leaq	-62(%r13), %rcx
	.p2align 4,,10
	.p2align 3
.L397:
	leal	(%rax,%rax,8), %edx
	addq	$2, %rcx
	leal	(%rax,%rdx,4), %edx
	movzwl	-2(%rcx), %eax
	addl	%edx, %eax
	cmpq	%rcx, %r13
	jne	.L397
	xorl	%edx, %edx
	movl	%eax, %r14d
	movzbl	-7448(%rbp), %ecx
	divl	-7560(%rbp)
	sall	%cl, %r14d
	leal	1(%rdx), %eax
	movq	%rax, %r10
	movl	(%r12,%rax,4), %eax
	movl	%r10d, %edx
	testl	%eax, %eax
	jne	.L403
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L400:
	leal	(%r10,%rdx), %eax
	cltd
	idivl	%ebx
	movslq	%edx, %rax
	movl	(%r12,%rax,4), %eax
	testl	%eax, %eax
	je	.L402
.L403:
	movl	%eax, %ecx
	andl	%r11d, %ecx
	cmpl	%ecx, %r14d
	jne	.L400
	andl	-7384(%rbp), %eax
	xorl	%ecx, %ecx
	subl	$1, %eax
	cltq
	leaq	(%r8,%rax,2), %rax
	.p2align 4,,10
	.p2align 3
.L401:
	movzwl	(%r9,%rcx,2), %r15d
	cmpw	%r15w, (%rax,%rcx,2)
	jne	.L400
	movl	%ecx, %r15d
	addq	$1, %rcx
	cmpq	$32, %rcx
	jne	.L401
	cmpl	$31, %r15d
	jne	.L400
.L399:
	movl	-7520(%rbp), %ecx
	addq	$2, %r13
	cmpl	%ecx, -7504(%rbp)
	jne	.L396
.L395:
	cmpb	$0, -7476(%rbp)
	jne	.L938
.L905:
	movl	-7472(%rbp), %esi
	jmp	.L337
.L402:
	movslq	%edx, %rdx
	orl	-7504(%rbp), %r14d
	movl	%r14d, (%r12,%rdx,4)
	jmp	.L399
.L490:
	movl	-7520(%rbp), %eax
	movl	%eax, -7456(%rbp)
	movl	%eax, %esi
	jmp	.L277
.L239:
	testl	%eax, %eax
	jns	.L904
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L483:
	movl	-7520(%rbp), %eax
	jmp	.L252
.L938:
	movl	-7444(%rbp), %ecx
	movl	-7392(%rbp), %eax
	subl	%ecx, %esi
	cmpl	%esi, %eax
	leal	1(%rsi), %r15d
	movl	%ecx, %esi
	cmovg	%eax, %r15d
	movl	-7472(%rbp), %eax
	subl	%ecx, %eax
	cmpl	%r15d, %eax
	jl	.L905
	leal	1(%r15), %edx
	leal	1(%rsi,%rax), %eax
	movl	-7424(%rbp), %r9d
	movq	%r12, -7568(%rbp)
	movslq	%edx, %rcx
	movl	%eax, -7560(%rbp)
	movslq	%r15d, %rdx
	addl	%esi, %r15d
	movq	%rcx, -7504(%rbp)
	movq	-7416(%rbp), %r10
	leaq	(%r8,%rdx,2), %r14
	notl	%r9d
	movl	%ebx, -7544(%rbp)
	movl	-7440(%rbp), %ebx
.L405:
	movq	-7504(%rbp), %rdx
	movzwl	(%r14), %eax
	movl	%edx, -7520(%rbp)
.L406:
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,4), %ecx
	movzwl	(%r8,%rdx,2), %eax
	addq	$1, %rdx
	addl	%ecx, %eax
	cmpl	%edx, %r15d
	jg	.L406
	xorl	%edx, %edx
	movl	%eax, %r11d
	movzbl	-7468(%rbp), %ecx
	divl	-7496(%rbp)
	sall	%cl, %r11d
	leal	1(%rdx), %esi
	movslq	%esi, %rax
	movl	%esi, %edx
	movl	(%r10,%rax,4), %eax
	testl	%eax, %eax
	jne	.L412
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L409:
	leal	(%rdx,%rsi), %eax
	cltd
	idivl	%ebx
	movslq	%edx, %rax
	movl	(%r10,%rax,4), %eax
	testl	%eax, %eax
	je	.L411
.L412:
	movl	%eax, %ecx
	andl	%r9d, %ecx
	cmpl	%ecx, %r11d
	jne	.L409
	andl	-7424(%rbp), %eax
	movl	-7444(%rbp), %r12d
	subl	$1, %eax
	cltq
	leaq	(%r8,%rax,2), %r13
	testl	%r12d, %r12d
	je	.L408
	xorl	%eax, %eax
	jmp	.L410
.L525:
	movq	%rcx, %rax
.L410:
	movzwl	(%r14,%rax,2), %ecx
	cmpw	%cx, 0(%r13,%rax,2)
	jne	.L409
	leaq	1(%rax), %rcx
	cmpq	%rax, %rdi
	jne	.L525
	cmpl	%eax, -7512(%rbp)
	jne	.L409
.L408:
	testl	%edx, %edx
	jns	.L413
	movl	%edx, %eax
	orl	-7520(%rbp), %r11d
	notl	%eax
	cltq
	movl	%r11d, (%r10,%rax,4)
.L413:
	addq	$1, -7504(%rbp)
	addq	$2, %r14
	addl	$1, %r15d
	cmpl	%r15d, -7560(%rbp)
	jne	.L405
	movq	-7568(%rbp), %r12
	movl	-7544(%rbp), %ebx
	jmp	.L905
.L411:
	notl	%edx
	jmp	.L408
.L937:
	leaq	-70(%r8,%r10,2), %r11
	movl	$35, %edx
.L386:
	movl	%esi, %r13d
	xorl	%eax, %eax
	subl	%edx, %r13d
.L385:
	movzwl	(%r9,%rax,2), %ecx
	cmpw	%cx, (%r11,%rax,2)
	jne	.L383
	addq	$1, %rax
	movl	%edx, %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jg	.L385
	movslq	%edx, %r11
	orl	$32768, %r13d
	leaq	16(%r10,%r10), %rcx
	movl	$35, %r14d
	leaq	8(%r11,%r10), %rax
	movl	%r13d, -7408(%rbp)
	subl	%edx, %r14d
	addq	%rax, %rax
	leaq	-16(%rax), %r13
	cmpq	%rcx, %r13
	setge	%cl
	cmpq	%rax, -7560(%rbp)
	setge	%al
	orb	%al, %cl
	je	.L454
	cmpl	$6, %r14d
	jbe	.L454
	movl	$36, %r10d
	leaq	(%r8,%r13), %rax
	subl	%edx, %r10d
	movdqu	(%rax), %xmm6
	movl	%r10d, %ecx
	shrl	$3, %ecx
	movups	%xmm6, (%r9)
	cmpl	$1, %ecx
	je	.L392
	movdqu	16(%rax), %xmm7
	movups	%xmm7, 16(%r9)
	cmpl	$2, %ecx
	je	.L392
	movdqu	32(%rax), %xmm4
	movups	%xmm4, 32(%r9)
	cmpl	$3, %ecx
	je	.L392
	movdqu	48(%rax), %xmm5
	movups	%xmm5, 48(%r9)
.L392:
	movl	%r10d, %ecx
	andl	$-8, %ecx
	leal	(%rdx,%rcx), %eax
	leal	(%rsi,%rcx), %r11d
	cmpl	%r10d, %ecx
	je	.L390
	leal	(%rsi,%rax), %ecx
	leal	1(%rax), %r13d
	movslq	%ecx, %rcx
	leal	1(%r11), %r10d
	movzwl	(%r8,%rcx,2), %r9d
	movslq	%r11d, %rcx
	movw	%r9w, (%r8,%rcx,2)
	cmpl	$35, %eax
	je	.L390
	leal	0(%r13,%rsi), %ecx
	movslq	%r10d, %r9
	leal	2(%rax), %r11d
	movslq	%ecx, %rcx
	leaq	(%r9,%r9), %r10
	movzwl	(%r8,%rcx,2), %ecx
	movw	%cx, (%r8,%r9,2)
	cmpl	$34, %eax
	je	.L390
	leal	(%rsi,%r11), %ecx
	leal	3(%rax), %r9d
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	movw	%cx, 2(%r8,%r10)
	cmpl	$33, %eax
	je	.L390
	leal	(%rsi,%r9), %ecx
	leal	4(%rax), %r11d
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	movw	%cx, 4(%r8,%r10)
	cmpl	$32, %eax
	je	.L390
	leal	(%rsi,%r11), %ecx
	leal	5(%rax), %r9d
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	movw	%cx, 6(%r8,%r10)
	cmpl	$31, %eax
	je	.L390
	leal	(%rsi,%r9), %ecx
	leal	6(%rax), %r11d
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	movw	%cx, 8(%r8,%r10)
	cmpl	$30, %eax
	je	.L390
	leal	(%rsi,%r11), %eax
	cltq
	movzwl	(%r8,%rax,2), %eax
	movw	%ax, 10(%r8,%r10)
.L390:
	leal	36(%rsi), %eax
	subl	%edx, %eax
	movl	%eax, -7472(%rbp)
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L383:
	addq	$2, %r11
	subl	$1, %edx
	jne	.L386
	movl	%esi, %eax
	orb	$-128, %ah
	movl	%eax, -7408(%rbp)
	jmp	.L388
.L340:
	cmpl	%esi, -7392(%rbp)
	jne	.L939
	movslq	-7392(%rbp), %r10
	movl	$32, %r11d
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movl	%r10d, -7408(%rbp)
.L345:
	movslq	%eax, %rdx
	addq	-7400(%rbp), %rdx
	leaq	(%r8,%r10,2), %rcx
	movl	%r11d, %r15d
	leaq	(%r9,%rdx,4), %rdx
	shrl	$3, %r15d
	movdqu	(%rdx), %xmm0
	movdqu	16(%rdx), %xmm6
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm6, %xmm0
	punpckhwd	%xmm6, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpcklwd	%xmm2, %xmm0
	movups	%xmm0, (%rcx)
	cmpl	$1, %r15d
	je	.L351
	movdqu	32(%rdx), %xmm0
	movdqu	48(%rdx), %xmm5
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm5, %xmm0
	punpckhwd	%xmm5, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpcklwd	%xmm2, %xmm0
	movups	%xmm0, 16(%rcx)
	cmpl	$2, %r15d
	je	.L351
	movdqu	64(%rdx), %xmm0
	movdqu	80(%rdx), %xmm7
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm7, %xmm0
	punpckhwd	%xmm7, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpcklwd	%xmm2, %xmm0
	movups	%xmm0, 32(%rcx)
	cmpl	$3, %r15d
	je	.L351
	movdqu	112(%rdx), %xmm2
	movdqu	96(%rdx), %xmm0
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpcklwd	%xmm2, %xmm0
	movups	%xmm0, 48(%rcx)
.L352:
	movl	%esi, %eax
	leal	-31(%rsi), %edx
	subl	%r14d, %eax
	movl	-7392(%rbp), %r14d
	leal	32(%rax), %ecx
	movl	%ecx, -7472(%rbp)
	leal	-32(%rsi), %ecx
	cmpl	%ecx, %r14d
	cmovg	%r14d, %edx
	movl	%edx, -7504(%rbp)
	cmpl	%edx, %eax
	jl	.L354
	addl	$1, %eax
	movl	-7384(%rbp), %r15d
	movslq	%edx, %rdx
	movl	%eax, -7560(%rbp)
	leaq	64(%r8,%rdx,2), %r14
	notl	%r15d
.L355:
	addl	$1, -7504(%rbp)
	movzwl	-64(%r14), %eax
	leaq	-64(%r14), %r9
	leaq	-62(%r14), %rdx
.L356:
	leal	(%rax,%rax,8), %ecx
	addq	$2, %rdx
	leal	(%rax,%rcx,4), %ecx
	movzwl	-2(%rdx), %eax
	addl	%ecx, %eax
	cmpq	%rdx, %r14
	jne	.L356
	xorl	%edx, %edx
	movl	%eax, %r10d
	movzbl	-7448(%rbp), %ecx
	divl	-7520(%rbp)
	sall	%cl, %r10d
	leal	1(%rdx), %eax
	movq	%rax, %rcx
	movl	(%r12,%rax,4), %eax
	movl	%ecx, %edx
	testl	%eax, %eax
	jne	.L362
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L359:
	leal	(%rcx,%rdx), %eax
	cltd
	idivl	%ebx
	movslq	%edx, %rax
	movl	(%r12,%rax,4), %eax
	testl	%eax, %eax
	je	.L361
.L362:
	movl	%eax, %r11d
	andl	%r15d, %r11d
	cmpl	%r11d, %r10d
	jne	.L359
	andl	-7384(%rbp), %eax
	subl	$1, %eax
	cltq
	leaq	(%r8,%rax,2), %r13
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L360:
	movzwl	(%r9,%rax,2), %r11d
	cmpw	%r11w, 0(%r13,%rax,2)
	jne	.L359
	movl	%eax, %r11d
	addq	$1, %rax
	cmpq	$32, %rax
	jne	.L360
	cmpl	$31, %r11d
	jne	.L359
.L358:
	movl	-7560(%rbp), %ecx
	addq	$2, %r14
	cmpl	%ecx, -7504(%rbp)
	jne	.L355
.L354:
	cmpb	$0, -7476(%rbp)
	je	.L905
	movl	-7444(%rbp), %ecx
	movl	-7392(%rbp), %edx
	subl	%ecx, %esi
	cmpl	%esi, %edx
	leal	1(%rsi), %eax
	movl	%ecx, %esi
	cmovg	%edx, %eax
	movl	-7472(%rbp), %edx
	subl	%ecx, %edx
	cmpl	%eax, %edx
	jl	.L905
	leal	1(%rax), %r15d
	movslq	%eax, %rcx
	leal	(%rsi,%rax), %r9d
	leal	1(%rsi,%rdx), %eax
	movslq	%r15d, %r15
	leaq	(%r8,%rcx,2), %r14
	movl	%eax, -7560(%rbp)
	movl	-7424(%rbp), %eax
	notl	%eax
	movl	%eax, -7504(%rbp)
.L364:
	movl	%r15d, -7520(%rbp)
	movzwl	(%r14), %eax
	movq	%r15, %rdx
.L365:
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,4), %ecx
	movzwl	(%r8,%rdx,2), %eax
	addq	$1, %rdx
	addl	%ecx, %eax
	cmpl	%edx, %r9d
	jg	.L365
	xorl	%edx, %edx
	movl	%eax, %esi
	movzbl	-7468(%rbp), %ecx
	divl	-7496(%rbp)
	sall	%cl, %esi
	leal	1(%rdx), %ecx
	movq	-7416(%rbp), %rdx
	movslq	%ecx, %rax
	movl	(%rdx,%rax,4), %eax
	movl	%ecx, %edx
	testl	%eax, %eax
	jne	.L371
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L368:
	leal	(%rcx,%rdx), %eax
	movq	-7416(%rbp), %r11
	cltd
	idivl	-7440(%rbp)
	movslq	%edx, %rax
	movl	(%r11,%rax,4), %eax
	testl	%eax, %eax
	je	.L370
.L371:
	movl	-7504(%rbp), %r10d
	andl	%eax, %r10d
	cmpl	%r10d, %esi
	jne	.L368
	andl	-7424(%rbp), %eax
	subl	$1, %eax
	cltq
	leaq	(%r8,%rax,2), %r13
	movl	-7444(%rbp), %eax
	testl	%eax, %eax
	je	.L367
	xorl	%eax, %eax
	jmp	.L369
.L518:
	movq	%r10, %rax
.L369:
	movzwl	(%r14,%rax,2), %r11d
	cmpw	%r11w, 0(%r13,%rax,2)
	jne	.L368
	leaq	1(%rax), %r10
	cmpq	%rdi, %rax
	jne	.L518
	cmpl	-7512(%rbp), %eax
	jne	.L368
.L367:
	testl	%edx, %edx
	jns	.L372
	movl	%edx, %eax
	movq	-7416(%rbp), %rcx
	orl	-7520(%rbp), %esi
	notl	%eax
	cltq
	movl	%esi, (%rcx,%rax,4)
.L372:
	addq	$1, %r15
	addq	$2, %r14
	addl	$1, %r9d
	cmpl	%r9d, -7560(%rbp)
	jne	.L364
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L361:
	movslq	%edx, %rdx
	orl	-7504(%rbp), %r10d
	movl	%r10d, (%r12,%rdx,4)
	jmp	.L358
.L370:
	notl	%edx
	jmp	.L367
.L351:
	movl	%r11d, %ecx
	andl	$-8, %ecx
	addl	%ecx, %eax
	leal	(%rsi,%rcx), %edx
	cmpl	%ecx, %r11d
	je	.L352
.L350:
	leal	0(%r13,%rax), %ecx
	movslq	%edx, %rdx
	leal	1(%rax), %r11d
	movslq	%ecx, %rcx
	leaq	(%rdx,%rdx), %r10
	movl	(%r9,%rcx,4), %ecx
	movw	%cx, (%r8,%rdx,2)
	cmpl	$31, %eax
	je	.L352
	leal	(%r11,%r13), %edx
	leal	2(%rax), %ecx
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edx
	movw	%dx, 2(%r8,%r10)
	cmpl	$30, %eax
	je	.L352
	leal	(%rcx,%r13), %edx
	leal	3(%rax), %r11d
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edx
	movw	%dx, 4(%r8,%r10)
	cmpl	$29, %eax
	je	.L352
	leal	(%r11,%r13), %edx
	leal	4(%rax), %ecx
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edx
	movw	%dx, 6(%r8,%r10)
	cmpl	$28, %eax
	je	.L352
	leal	(%rcx,%r13), %edx
	leal	5(%rax), %r11d
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edx
	movw	%dx, 8(%r8,%r10)
	cmpl	$27, %eax
	je	.L352
	leal	(%r11,%r13), %edx
	leal	6(%rax), %ecx
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edx
	movw	%dx, 10(%r8,%r10)
	cmpl	$26, %eax
	je	.L352
	leal	(%rcx,%r13), %eax
	cltq
	movl	(%r9,%rax,4), %eax
	movw	%ax, 12(%r8,%r10)
	jmp	.L352
.L526:
	movl	%edx, -7480(%rbp)
	movq	-7456(%rbp), %r13
	jmp	.L415
.L482:
	movl	-7520(%rbp), %eax
	movl	%eax, -7456(%rbp)
	movl	%eax, %esi
	jmp	.L251
.L481:
	movl	%eax, %edx
	xorl	%ecx, %ecx
	jmp	.L248
.L333:
	movl	-7464(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L940
	cmpl	$32798, -7392(%rbp)
	jle	.L332
.L419:
	movq	-7536(%rbp), %rax
	movl	$0, -7392(%rbp)
	movl	$8, (%rax)
	jmp	.L332
.L503:
	movl	$32767, -7384(%rbp)
	movl	$200084, %r15d
	movl	$50021, %ebx
	movl	$15, -7448(%rbp)
	jmp	.L325
.L931:
	leal	-34(%r14), %ecx
	cmpl	$4095, %ecx
	jle	.L506
	cmpl	$32767, %ecx
	jle	.L507
	cmpl	$131072, %ecx
	movl	$800012, %edx
	movl	$6000028, %eax
	cmovl	%rdx, %rax
	movl	$131071, %edx
	movq	%rax, %r14
	movl	$2097151, %eax
	cmovl	%edx, %eax
	movl	$200003, %edx
	movl	%eax, -7424(%rbp)
	setge	%al
	movzbl	%al, %eax
	leal	17(,%rax,4), %eax
	movl	%eax, -7468(%rbp)
	movl	$1500007, %eax
	cmovl	%edx, %eax
	movl	%eax, -7440(%rbp)
.L330:
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -7416(%rbp)
	testq	%rax, %rax
	je	.L331
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movl	$36, -7444(%rbp)
	jmp	.L329
.L934:
	leal	-1(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L941
	movslq	%r15d, %rax
	movslq	%ecx, %rdx
	subq	%rdx, %rax
	leaq	0(%r13,%rax,2), %rdi
.L451:
	movl	%r15d, %eax
	leal	-1(%rcx), %r8d
	subl	%ecx, %eax
	movq	%r8, %r9
	movl	%eax, -7400(%rbp)
	xorl	%eax, %eax
	jmp	.L438
.L942:
	leaq	1(%rax), %rdx
	cmpq	%r8, %rax
	je	.L435
	movq	%rdx, %rax
.L438:
	movzwl	(%rsi,%rax,2), %edx
	cmpw	%dx, (%rdi,%rax,2)
	je	.L942
	movl	%r9d, %ecx
	addq	$2, %rdi
	testl	%r9d, %r9d
	jne	.L451
	movl	%r15d, -7400(%rbp)
	jmp	.L435
.L501:
	movb	$0, -7476(%rbp)
	xorl	%r14d, %r14d
	jmp	.L309
.L506:
	movl	$4095, -7424(%rbp)
	movl	$24028, %r14d
	movl	$12, -7468(%rbp)
	movl	$6007, -7440(%rbp)
	jmp	.L330
.L331:
	movq	-7536(%rbp), %rax
	movl	$0, -7392(%rbp)
	movl	$7, (%rax)
	jmp	.L332
.L507:
	movl	$32767, -7424(%rbp)
	movl	$200084, %r14d
	movl	$15, -7468(%rbp)
	movl	$50021, -7440(%rbp)
	jmp	.L330
.L941:
	movl	%r15d, %eax
	subl	%ecx, %eax
	movl	%eax, -7400(%rbp)
	jmp	.L435
.L534:
	movl	%esi, %r15d
	jmp	.L441
.L532:
	movl	%r15d, -7424(%rbp)
	movl	%r15d, %esi
	jmp	.L439
.L940:
	cmpl	$32798, -7392(%rbp)
	movl	$32767, 12(%r13)
	jle	.L332
	jmp	.L419
.L920:
	call	__stack_chk_fail@PLT
.L469:
	movl	$-1, -7560(%rbp)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L939:
	movslq	%esi, %r10
	movl	$31, %eax
	leaq	-62(%r8,%r10,2), %r14
.L349:
	movl	%esi, %ecx
	xorl	%edx, %edx
	subl	%eax, %ecx
	movl	%ecx, -7408(%rbp)
.L348:
	movzwl	(%r14,%rdx,2), %ecx
	cmpl	(%r11,%rdx,4), %ecx
	jne	.L346
	addq	$1, %rdx
	movl	%eax, %ecx
	subl	%edx, %ecx
	testl	%ecx, %ecx
	jg	.L348
	movl	$31, %edx
	movl	$32, %r11d
	movl	%eax, %r14d
	subl	%eax, %edx
	subl	%eax, %r11d
	cmpl	$6, %edx
	ja	.L345
	movl	%esi, %edx
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L346:
	addq	$2, %r14
	subl	$1, %eax
	jne	.L349
	movl	%esi, -7408(%rbp)
	movl	$32, %r11d
	xorl	%r14d, %r14d
	jmp	.L345
.L454:
	addq	%r14, %r10
	addq	%r10, %r10
	leaq	2(%r8,%r10), %rax
.L391:
	movzwl	(%r15,%r11,2), %ecx
	addq	$2, %r15
	movw	%cx, -2(%r15)
	cmpq	%r15, %rax
	jne	.L391
	jmp	.L390
	.cfi_endproc
.LFE2149:
	.size	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie11compactTrieEiR10UErrorCode, .-_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie11compactTrieEiR10UErrorCode
	.p2align 4
	.globl	umutablecptrie_open_67
	.type	umutablecptrie_open_67, @function
umutablecptrie_open_67:
.LFB2151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L943
	movl	%edi, %r13d
	movl	$69696, %edi
	movl	%esi, %r14d
	movq	%rdx, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L945
	movl	(%rbx), %edx
	movq	$0, (%rax)
	movabsq	$-4294967296, %rax
	movq	%rax, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movl	$-1, 32(%r12)
	movl	%r13d, 36(%r12)
	movl	%r13d, 40(%r12)
	movl	%r14d, 44(%r12)
	movl	$0, 48(%r12)
	movl	%r13d, 52(%r12)
	movq	$0, 56(%r12)
	testl	%edx, %edx
	jle	.L956
	movl	(%rbx), %eax
	xorl	%edi, %edi
.L950:
	testl	%eax, %eax
	jle	.L943
.L949:
	call	uprv_free_67@PLT
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	movq	56(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L943:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L956:
	.cfi_restore_state
	movl	$16384, %edi
	call	uprv_malloc_67@PLT
	movl	$65536, %edi
	movq	%rax, (%r12)
	call	uprv_malloc_67@PLT
	movq	(%r12), %rdi
	movq	%rax, 16(%r12)
	testq	%rdi, %rdi
	je	.L954
	testq	%rax, %rax
	je	.L954
	movl	$4096, 8(%r12)
	movl	(%rbx), %eax
	movl	$16384, 24(%r12)
	jmp	.L950
.L945:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L943
	movl	$7, (%rbx)
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L954:
	movl	$7, (%rbx)
	jmp	.L949
	.cfi_endproc
.LFE2151:
	.size	umutablecptrie_open_67, .-umutablecptrie_open_67
	.p2align 4
	.globl	umutablecptrie_clone_67
	.type	umutablecptrie_clone_67, @function
umutablecptrie_clone_67:
.LFB2152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L972
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L972
	movl	$69696, %edi
	movq	%rsi, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L959
	movq	32(%rbx), %rdx
	movq	$0, (%rax)
	movl	$0, 8(%rax)
	movl	12(%rbx), %eax
	movq	%rdx, 32(%r12)
	movq	40(%rbx), %rdx
	movl	%eax, 12(%r12)
	movl	48(%rbx), %eax
	movq	%rdx, 40(%r12)
	movq	48(%rbx), %rdx
	movq	$0, 16(%r12)
	movq	%rdx, 48(%r12)
	movl	0(%r13), %edx
	movq	$0, 24(%r12)
	movq	$0, 56(%r12)
	testl	%edx, %edx
	jg	.L960
	cmpl	$65537, %eax
	movl	$278528, %edx
	movl	$16384, %edi
	movl	$69632, %eax
	cmovge	%rdx, %rdi
	movl	$4096, %r14d
	cmovge	%eax, %r14d
	call	uprv_malloc_67@PLT
	movq	%rax, (%r12)
	movl	24(%rbx), %eax
	leal	0(,%rax,4), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	(%r12), %r15
	movq	%rax, 16(%r12)
	testq	%r15, %r15
	je	.L970
	testq	%rax, %rax
	je	.L970
	movl	24(%rbx), %eax
	movl	%r14d, 8(%r12)
	leaq	64(%r12), %rdi
	leaq	64(%rbx), %rsi
	movl	48(%r12), %r14d
	movl	%eax, 24(%r12)
	sarl	$4, %r14d
	movslq	%r14d, %rdx
	call	memcpy@PLT
	movq	(%rbx), %rsi
	leal	0(,%r14,4), %edx
	movq	%r15, %rdi
	movslq	%edx, %rdx
	call	memcpy@PLT
	movslq	28(%rbx), %rdx
	movq	16(%r12), %rdi
	movq	16(%rbx), %rsi
	salq	$2, %rdx
	call	memcpy@PLT
	movl	28(%rbx), %eax
	movl	%eax, 28(%r12)
	movl	0(%r13), %eax
.L965:
	testl	%eax, %eax
	jle	.L957
.L964:
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	movq	56(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L972:
	xorl	%r12d, %r12d
.L957:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L960:
	.cfi_restore_state
	movl	0(%r13), %eax
	jmp	.L965
.L959:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L957
	movl	$7, 0(%r13)
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L970:
	movl	$7, 0(%r13)
	jmp	.L964
	.cfi_endproc
.LFE2152:
	.size	umutablecptrie_clone_67, .-umutablecptrie_clone_67
	.p2align 4
	.globl	umutablecptrie_close_67
	.type	umutablecptrie_close_67, @function
umutablecptrie_close_67:
.LFB2153:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L973
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	16(%r12), %rdi
	call	uprv_free_67@PLT
	movq	56(%r12), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L973:
	ret
	.cfi_endproc
.LFE2153:
	.size	umutablecptrie_close_67, .-umutablecptrie_close_67
	.p2align 4
	.globl	umutablecptrie_fromUCPMap_67
	.type	umutablecptrie_fromUCPMap_67, @function
umutablecptrie_fromUCPMap_67:
.LFB2154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L978
	movq	%rdi, %r12
	movq	%rsi, %r14
	testq	%rdi, %rdi
	je	.L1018
	movl	$-1, %esi
	call	ucpmap_get_67@PLT
	movl	$1114111, %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	ucpmap_get_67@PLT
	movl	$69696, %edi
	movl	%eax, -68(%rbp)
	movl	%eax, %r13d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L981
	movl	(%r14), %edi
	movq	$0, (%rax)
	movabsq	$-4294967296, %rax
	movq	%rax, 8(%r15)
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	movl	$-1, 32(%r15)
	movl	%r13d, 36(%r15)
	movl	%r13d, 40(%r15)
	movl	%ebx, 44(%r15)
	movl	$0, 48(%r15)
	movl	%r13d, 52(%r15)
	movq	$0, 56(%r15)
	testl	%edi, %edi
	jle	.L1019
	movl	(%r14), %eax
	xorl	%edi, %edi
.L986:
	testl	%eax, %eax
	jg	.L985
	leaq	-60(%rbp), %rax
	movq	%r12, -88(%rbp)
	xorl	%r13d, %r13d
	movq	%rax, -80(%rbp)
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1020:
	testl	%eax, %eax
	jg	.L989
	cmpl	$1114111, %ebx
	jg	.L992
	movq	%r14, %rcx
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie3setEijR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L989:
	leal	1(%rbx), %r13d
.L1001:
	subq	$8, %rsp
	pushq	-80(%rbp)
	movq	-88(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	%r13d, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	ucpmap_getRange_67@PLT
	popq	%rcx
	popq	%rsi
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L988
	movl	-60(%rbp), %r12d
	cmpl	%r12d, -68(%rbp)
	je	.L989
	movl	(%r14), %eax
	cmpl	%ebx, %r13d
	je	.L1020
	testl	%eax, %eax
	jg	.L989
	cmpl	$1114111, %r13d
	seta	%dl
	cmpl	$1114111, %ebx
	seta	%al
	orb	%al, %dl
	jne	.L992
	cmpl	%ebx, %r13d
	jg	.L992
	movl	48(%r15), %r8d
	cmpl	%r8d, %ebx
	jl	.L993
	leal	512(%rbx), %eax
	sarl	$4, %r8d
	andl	$-512, %eax
	movl	%eax, -72(%rbp)
	sarl	$4, %eax
	movl	%eax, %r11d
	cmpl	8(%r15), %eax
	jg	.L994
	movq	(%r15), %r10
.L995:
	movl	%r11d, %r9d
	movl	$4, %edi
	movslq	%r8d, %rax
	subl	%r8d, %r9d
	cmpl	%r11d, %r8d
	leaq	0(,%rax,4), %rsi
	movl	%r9d, %ecx
	leaq	0(,%rcx,4), %rdx
	cmovge	%rdi, %rdx
	addq	%rsi, %rdx
	addq	%r10, %rsi
	leaq	(%r10,%rdx), %rdi
	leaq	64(%rax), %rdx
	movq	%rsi, -96(%rbp)
	leaq	(%r15,%rdx), %rsi
	movq	%rdi, -112(%rbp)
	movq	%rsi, -120(%rbp)
	leaq	40(%r15), %rsi
	cmpq	%rsi, %rdi
	leaq	44(%r15), %rsi
	setbe	%dil
	cmpq	%rsi, -96(%rbp)
	setnb	%sil
	orl	%esi, %edi
	leal	-1(%r9), %esi
	cmpl	$14, %esi
	seta	%sil
	cmpl	%r11d, %r8d
	setl	-104(%rbp)
	andb	-104(%rbp), %sil
	testb	%sil, %dil
	je	.L1010
	cmpl	%r11d, %r8d
	movl	$1, %esi
	movq	-120(%rbp), %rdi
	cmovge	%rsi, %rcx
	movq	-96(%rbp), %rsi
	addq	%rdx, %rcx
	addq	%r15, %rcx
	cmpq	%rcx, %rsi
	setnb	%cl
	cmpq	%rdi, -112(%rbp)
	setbe	%dl
	orb	%dl, %cl
	je	.L1010
	cmpl	%r11d, %r8d
	movl	$1, %ecx
	movq	%rsi, %rax
	movq	%rdi, %rdx
	cmovl	%r9d, %ecx
	movd	40(%r15), %xmm2
	pxor	%xmm1, %xmm1
	movl	%ecx, %esi
	pshufd	$0, %xmm2, %xmm0
	shrl	$4, %esi
	salq	$4, %rsi
	addq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L998:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	addq	$64, %rax
	movups	%xmm0, -64(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rsi, %rdx
	jne	.L998
	movl	%ecx, %eax
	andl	$-16, %eax
	addl	%eax, %r8d
	cmpl	%eax, %ecx
	je	.L1000
	movslq	%r8d, %rax
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %edx
	movl	%edx, (%r10,%rax,4)
	leal	1(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %ecx
	leaq	0(,%rdx,4), %rax
	movl	%ecx, (%r10,%rdx,4)
	leal	2(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 4(%r10,%rax)
	leal	3(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 8(%r10,%rax)
	leal	4(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 12(%r10,%rax)
	leal	5(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 16(%r10,%rax)
	leal	6(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 20(%r10,%rax)
	leal	7(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 24(%r10,%rax)
	leal	8(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 28(%r10,%rax)
	leal	9(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 32(%r10,%rax)
	leal	10(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 36(%r10,%rax)
	leal	11(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 40(%r10,%rax)
	leal	12(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 44(%r10,%rax)
	leal	13(%r8), %edx
	cmpl	%edx, %r11d
	jle	.L1000
	movslq	%edx, %rdx
	addl	$14, %r8d
	movb	$0, 64(%r15,%rdx)
	movl	40(%r15), %edx
	movl	%edx, 48(%r10,%rax)
	cmpl	%r8d, %r11d
	jle	.L1000
	movslq	%r8d, %r8
	movb	$0, 64(%r15,%r8)
	movl	40(%r15), %edx
	movl	%edx, 52(%r10,%rax)
	.p2align 4,,10
	.p2align 3
.L1000:
	movl	-72(%rbp), %eax
	movl	%eax, 48(%r15)
.L993:
	movq	%r14, %r8
	movl	%r12d, %ecx
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie8setRangeEiijR10UErrorCode.part.0
	jmp	.L989
.L981:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L978
	movl	$7, (%r14)
	.p2align 4,,10
	.p2align 3
.L978:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1021
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1019:
	.cfi_restore_state
	movl	$16384, %edi
	call	uprv_malloc_67@PLT
	movl	$65536, %edi
	movq	%rax, (%r15)
	call	uprv_malloc_67@PLT
	movq	(%r15), %rdi
	movq	%rax, 16(%r15)
	testq	%rax, %rax
	je	.L1007
	testq	%rdi, %rdi
	je	.L1007
	movl	$4096, 8(%r15)
	movl	(%r14), %eax
	movl	$16384, 24(%r15)
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L992:
	movl	$1, (%r14)
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L988:
	movl	(%r14), %edx
	testl	%edx, %edx
	jle	.L978
	movq	(%r15), %rdi
.L985:
	call	uprv_free_67@PLT
	movq	16(%r15), %rdi
	call	uprv_free_67@PLT
	movq	56(%r15), %rdi
	call	uprv_free_67@PLT
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1010:
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %edx
	movl	%edx, (%r10,%rax,4)
	addq	$1, %rax
	cmpl	%eax, %r11d
	jg	.L1010
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1018:
	movl	$1, (%rsi)
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1007:
	movl	$7, (%r14)
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L994:
	movl	$278528, %edi
	movl	%eax, -120(%rbp)
	movl	%r8d, -96(%rbp)
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L996
	movl	-96(%rbp), %r8d
	movq	(%r15), %r9
	movl	$278528, %ecx
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	leal	0(,%r8,4), %edx
	movq	%r9, %rsi
	movl	%r8d, -112(%rbp)
	movslq	%edx, %rdx
	movq	%r9, -104(%rbp)
	call	__memcpy_chk@PLT
	movq	-104(%rbp), %r9
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	movq	-96(%rbp), %r10
	movl	-120(%rbp), %r11d
	movl	$69632, 8(%r15)
	movl	-112(%rbp), %r8d
	movq	%r10, (%r15)
	jmp	.L995
.L996:
	movl	$7, (%r14)
	jmp	.L989
.L1021:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2154:
	.size	umutablecptrie_fromUCPMap_67, .-umutablecptrie_fromUCPMap_67
	.p2align 4
	.globl	umutablecptrie_fromUCPTrie_67
	.type	umutablecptrie_fromUCPTrie_67, @function
umutablecptrie_fromUCPTrie_67:
.LFB2155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L1022
	movq	%rdi, %r12
	movq	%rsi, %r14
	testq	%rdi, %rdi
	je	.L1027
	movzbl	31(%rdi), %eax
	cmpb	$1, %al
	je	.L1025
	cmpb	$2, %al
	je	.L1026
	testb	%al, %al
	je	.L1067
.L1027:
	movl	$1, (%r14)
.L1022:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1068
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1067:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movslq	20(%rdi), %rdx
	movzwl	-2(%rax,%rdx,2), %ebx
	movzwl	-4(%rax,%rdx,2), %eax
	movl	%eax, -68(%rbp)
.L1028:
	movl	$69696, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1030
	movq	$0, (%rax)
	movl	(%r14), %edi
	movabsq	$-4294967296, %rax
	movq	%rax, 8(%r15)
	movl	-68(%rbp), %eax
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	movl	$-1, 32(%r15)
	movl	%eax, 36(%r15)
	movl	%eax, 40(%r15)
	movl	%ebx, 44(%r15)
	movl	$0, 48(%r15)
	movl	%eax, 52(%r15)
	movq	$0, 56(%r15)
	testl	%edi, %edi
	jle	.L1069
	movl	(%r14), %eax
	xorl	%edi, %edi
.L1035:
	testl	%eax, %eax
	jg	.L1034
	leaq	-60(%rbp), %rax
	movq	%r12, -88(%rbp)
	xorl	%r13d, %r13d
	movq	%rax, -80(%rbp)
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1070:
	testl	%eax, %eax
	jg	.L1038
	cmpl	$1114111, %ebx
	jg	.L1041
	movq	%r14, %rcx
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie3setEijR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1038:
	leal	1(%rbx), %r13d
.L1050:
	subq	$8, %rsp
	pushq	-80(%rbp)
	movq	-88(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	%r13d, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	ucptrie_getRange_67@PLT
	popq	%rcx
	popq	%rsi
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L1037
	movl	-60(%rbp), %r12d
	cmpl	%r12d, -68(%rbp)
	je	.L1038
	movl	(%r14), %eax
	cmpl	%ebx, %r13d
	je	.L1070
	testl	%eax, %eax
	jg	.L1038
	cmpl	$1114111, %r13d
	seta	%dl
	cmpl	$1114111, %ebx
	seta	%al
	orb	%al, %dl
	jne	.L1041
	cmpl	%ebx, %r13d
	jg	.L1041
	movl	48(%r15), %r8d
	cmpl	%r8d, %ebx
	jl	.L1042
	leal	512(%rbx), %eax
	sarl	$4, %r8d
	andl	$-512, %eax
	movl	%eax, -72(%rbp)
	sarl	$4, %eax
	movl	%eax, %r9d
	cmpl	8(%r15), %eax
	jg	.L1043
	movq	(%r15), %r10
.L1044:
	movl	%r9d, %edi
	movl	$4, %r11d
	movslq	%r8d, %rax
	subl	%r8d, %edi
	cmpl	%r9d, %r8d
	leaq	0(,%rax,4), %rcx
	movl	%edi, %edx
	leaq	0(,%rdx,4), %rsi
	cmovge	%r11, %rsi
	leaq	(%r10,%rcx), %r11
	addq	%rcx, %rsi
	leaq	64(%rax), %rcx
	movq	%rcx, -104(%rbp)
	addq	%r15, %rcx
	addq	%r10, %rsi
	movq	%rcx, -120(%rbp)
	leaq	40(%r15), %rcx
	cmpq	%rcx, %rsi
	leaq	44(%r15), %rcx
	movq	%rsi, -112(%rbp)
	setbe	%sil
	cmpq	%rcx, %r11
	setnb	%cl
	orl	%ecx, %esi
	leal	-1(%rdi), %ecx
	cmpl	$14, %ecx
	seta	%cl
	cmpl	%r9d, %r8d
	setl	-96(%rbp)
	andb	-96(%rbp), %cl
	testb	%cl, %sil
	je	.L1059
	cmpl	%r9d, %r8d
	movl	$1, %ecx
	movq	-120(%rbp), %rsi
	cmovge	%rcx, %rdx
	addq	-104(%rbp), %rdx
	addq	%r15, %rdx
	cmpq	%rdx, %r11
	setnb	%dl
	cmpq	%rsi, -112(%rbp)
	setbe	%cl
	orb	%cl, %dl
	je	.L1059
	cmpl	%r9d, %r8d
	movl	$1, %ecx
	movq	%rsi, %rdx
	movq	%r11, %rax
	cmovl	%edi, %ecx
	movd	40(%r15), %xmm2
	pxor	%xmm1, %xmm1
	movl	%ecx, %esi
	pshufd	$0, %xmm2, %xmm0
	shrl	$4, %esi
	salq	$4, %rsi
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L1047:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	addq	$64, %rax
	movups	%xmm0, -64(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rsi, %rdx
	jne	.L1047
	movl	%ecx, %eax
	andl	$-16, %eax
	addl	%eax, %r8d
	cmpl	%eax, %ecx
	je	.L1049
	movslq	%r8d, %rax
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %edx
	movl	%edx, (%r10,%rax,4)
	leal	1(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %ecx
	leaq	0(,%rax,4), %rdx
	movl	%ecx, (%r10,%rax,4)
	leal	2(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 4(%r10,%rdx)
	leal	3(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 8(%r10,%rdx)
	leal	4(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 12(%r10,%rdx)
	leal	5(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 16(%r10,%rdx)
	leal	6(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 20(%r10,%rdx)
	leal	7(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 24(%r10,%rdx)
	leal	8(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 28(%r10,%rdx)
	leal	9(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 32(%r10,%rdx)
	leal	10(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 36(%r10,%rdx)
	leal	11(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 40(%r10,%rdx)
	leal	12(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 44(%r10,%rdx)
	leal	13(%r8), %eax
	cmpl	%eax, %r9d
	jle	.L1049
	cltq
	addl	$14, %r8d
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %eax
	movl	%eax, 48(%r10,%rdx)
	cmpl	%r8d, %r9d
	jle	.L1049
	movslq	%r8d, %r8
	movb	$0, 64(%r15,%r8)
	movl	40(%r15), %eax
	movl	%eax, 52(%r10,%rdx)
	.p2align 4,,10
	.p2align 3
.L1049:
	movl	-72(%rbp), %eax
	movl	%eax, 48(%r15)
.L1042:
	movq	%r14, %r8
	movl	%r12d, %ecx
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie8setRangeEiijR10UErrorCode.part.0
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	8(%rdi), %rdx
	movslq	20(%rdi), %rax
	movzbl	-1(%rdx,%rax), %ebx
	movzbl	-2(%rdx,%rax), %eax
	movl	%eax, -68(%rbp)
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1025:
	movslq	20(%rdi), %rax
	movq	8(%rdi), %rdx
	subq	$1, %rax
	movl	(%rdx,%rax,4), %ebx
	movl	-4(%rdx,%rax,4), %eax
	movl	%eax, -68(%rbp)
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1041:
	movl	$1, (%r14)
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1037:
	movl	(%r14), %edx
	testl	%edx, %edx
	jle	.L1022
	movq	(%r15), %rdi
.L1034:
	call	uprv_free_67@PLT
	movq	16(%r15), %rdi
	call	uprv_free_67@PLT
	movq	56(%r15), %rdi
	call	uprv_free_67@PLT
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1069:
	movl	$16384, %edi
	call	uprv_malloc_67@PLT
	movl	$65536, %edi
	movq	%rax, (%r15)
	call	uprv_malloc_67@PLT
	movq	(%r15), %rdi
	movq	%rax, 16(%r15)
	testq	%rax, %rax
	je	.L1056
	testq	%rdi, %rdi
	je	.L1056
	movl	$4096, 8(%r15)
	movl	(%r14), %eax
	movl	$16384, 24(%r15)
	jmp	.L1035
.L1030:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L1022
	movl	$7, (%r14)
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1059:
	movb	$0, 64(%r15,%rax)
	movl	40(%r15), %edx
	movl	%edx, (%r10,%rax,4)
	addq	$1, %rax
	cmpl	%eax, %r9d
	jg	.L1059
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1043:
	movl	$278528, %edi
	movl	%eax, -120(%rbp)
	movl	%r8d, -96(%rbp)
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L1045
	movl	-96(%rbp), %r8d
	movq	(%r15), %r11
	movl	$278528, %ecx
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	leal	0(,%r8,4), %edx
	movq	%r11, %rsi
	movl	%r8d, -112(%rbp)
	movslq	%edx, %rdx
	movq	%r11, -104(%rbp)
	call	__memcpy_chk@PLT
	movq	-104(%rbp), %r11
	movq	%r11, %rdi
	call	uprv_free_67@PLT
	movq	-96(%rbp), %r10
	movl	-120(%rbp), %r9d
	movl	$69632, 8(%r15)
	movl	-112(%rbp), %r8d
	movq	%r10, (%r15)
	jmp	.L1044
.L1045:
	movl	$7, (%r14)
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1056:
	movl	$7, (%r14)
	jmp	.L1034
.L1068:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2155:
	.size	umutablecptrie_fromUCPTrie_67, .-umutablecptrie_fromUCPTrie_67
	.p2align 4
	.globl	umutablecptrie_get_67
	.type	umutablecptrie_get_67, @function
umutablecptrie_get_67:
.LFB2156:
	.cfi_startproc
	endbr64
	cmpl	$1114111, %esi
	ja	.L1075
	cmpl	48(%rdi), %esi
	jge	.L1076
	movl	%esi, %edx
	movq	(%rdi), %rax
	sarl	$4, %edx
	movslq	%edx, %rdx
	cmpb	$0, 64(%rdi,%rdx)
	movl	(%rax,%rdx,4), %eax
	je	.L1071
	andl	$15, %esi
	leal	(%rsi,%rax), %edx
	movq	16(%rdi), %rax
	movl	(%rax,%rdx,4), %eax
.L1071:
	ret
	.p2align 4,,10
	.p2align 3
.L1076:
	movl	52(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1075:
	movl	44(%rdi), %eax
	ret
	.cfi_endproc
.LFE2156:
	.size	umutablecptrie_get_67, .-umutablecptrie_get_67
	.p2align 4
	.globl	umutablecptrie_getRange_67
	.type	umutablecptrie_getRange_67, @function
umutablecptrie_getRange_67:
.LFB2158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	pushq	16(%rbp)
	pushq	%r9
	movq	%r8, %r9
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	%esi, %edx
	movq	%rdi, %rsi
	leaq	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj(%rip), %rdi
	call	ucptrie_internalGetRange_67@PLT
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2158:
	.size	umutablecptrie_getRange_67, .-umutablecptrie_getRange_67
	.p2align 4
	.globl	umutablecptrie_set_67
	.type	umutablecptrie_set_67, @function
umutablecptrie_set_67:
.LFB2159:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1101
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	cmpl	$1114111, %esi
	jbe	.L1081
	movl	$1, (%rcx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1081:
	.cfi_restore_state
	movl	48(%rdi), %r15d
	movq	%rdi, %rbx
	movl	%edx, %r14d
	cmpl	%r15d, %esi
	jl	.L1082
	leal	512(%rsi), %r10d
	sarl	$4, %r15d
	movl	%r10d, %eax
	andl	$-512, %eax
	movl	%eax, -52(%rbp)
	sarl	$4, %eax
	movl	%eax, %r8d
	cmpl	8(%rdi), %eax
	jg	.L1083
	movq	(%rdi), %r9
.L1084:
	movl	%r8d, %r11d
	movl	$4, %edi
	movslq	%r15d, %rdx
	subl	%r15d, %r11d
	cmpl	%r8d, %r15d
	leaq	0(,%rdx,4), %rax
	movl	%r11d, %esi
	leaq	0(,%rsi,4), %rcx
	cmovge	%rdi, %rcx
	leaq	64(%rdx), %rdi
	leaq	(%rbx,%rdi), %r10
	addq	%rax, %rcx
	movq	%r10, -64(%rbp)
	addq	%r9, %rax
	movl	$1, %r10d
	addq	%r9, %rcx
	cmpl	%r8d, %r15d
	cmovge	%r10, %rsi
	movq	-64(%rbp), %r10
	addq	%rdi, %rsi
	addq	%rbx, %rsi
	cmpq	%rsi, %rax
	setnb	%dil
	cmpq	%r10, %rcx
	setbe	%sil
	orl	%edi, %esi
	leaq	40(%rbx), %rdi
	cmpq	%rdi, %rcx
	leaq	44(%rbx), %rdi
	setbe	%cl
	cmpq	%rdi, %rax
	setnb	%dil
	orl	%edi, %ecx
	testb	%cl, %sil
	je	.L1093
	leal	-1(%r8), %ecx
	subl	%r15d, %ecx
	cmpl	$14, %ecx
	seta	%sil
	cmpl	%r8d, %r15d
	setl	%cl
	testb	%cl, %sil
	je	.L1093
	cmpl	%r8d, %r15d
	movl	$1, %esi
	movd	40(%rbx), %xmm2
	movq	%r10, %rdx
	cmovl	%r11d, %esi
	pxor	%xmm1, %xmm1
	pshufd	$0, %xmm2, %xmm0
	movl	%esi, %ecx
	shrl	$4, %ecx
	salq	$4, %rcx
	addq	%r10, %rcx
	.p2align 4,,10
	.p2align 3
.L1087:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	addq	$64, %rax
	movups	%xmm0, -64(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rcx, %rdx
	jne	.L1087
	movl	%esi, %eax
	andl	$-16, %eax
	addl	%eax, %r15d
	cmpl	%eax, %esi
	je	.L1089
	movslq	%r15d, %rax
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	1(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	2(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	3(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	4(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	5(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	6(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	7(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	8(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	9(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	10(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	11(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	12(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	leal	13(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L1089
	cltq
	addl	$14, %r15d
	movb	$0, 64(%rbx,%rax)
	movl	40(%rbx), %edx
	movl	%edx, (%r9,%rax,4)
	cmpl	%r15d, %r8d
	jle	.L1089
	movslq	%r15d, %r15
	movb	$0, 64(%rbx,%r15)
	movl	40(%rbx), %eax
	movl	%eax, (%r9,%r15,4)
	.p2align 4,,10
	.p2align 3
.L1089:
	movl	-52(%rbp), %eax
	movl	%eax, 48(%rbx)
.L1082:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	sarl	$4, %esi
	call	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie12getDataBlockEi
	testl	%eax, %eax
	js	.L1090
	andl	$15, %r12d
	addl	%eax, %r12d
	movq	16(%rbx), %rax
	movslq	%r12d, %r12
	movl	%r14d, (%rax,%r12,4)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1101:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1093:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movb	$0, 64(%rbx,%rdx)
	movl	40(%rbx), %eax
	movl	%eax, (%r9,%rdx,4)
	addq	$1, %rdx
	cmpl	%edx, %r8d
	jg	.L1093
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1090:
	movl	$7, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1083:
	.cfi_restore_state
	movl	$278528, %edi
	movl	%eax, -64(%rbp)
	call	uprv_malloc_67@PLT
	movl	-64(%rbp), %r8d
	testq	%rax, %rax
	je	.L1090
	movq	(%rbx), %r11
	movl	$278528, %ecx
	movq	%rax, %rdi
	movl	%r8d, -56(%rbp)
	leal	0(,%r15,4), %edx
	movq	%rax, -64(%rbp)
	movq	%r11, %rsi
	movslq	%edx, %rdx
	movq	%r11, -72(%rbp)
	call	__memcpy_chk@PLT
	movq	-72(%rbp), %r11
	movq	%r11, %rdi
	call	uprv_free_67@PLT
	movq	-64(%rbp), %r9
	movl	$69632, 8(%rbx)
	movl	-56(%rbp), %r8d
	movq	%r9, (%rbx)
	jmp	.L1084
	.cfi_endproc
.LFE2159:
	.size	umutablecptrie_set_67, .-umutablecptrie_set_67
	.p2align 4
	.globl	umutablecptrie_setRange_67
	.type	umutablecptrie_setRange_67, @function
umutablecptrie_setRange_67:
.LFB2160:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L1130
	ret
	.p2align 4,,10
	.p2align 3
.L1130:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$1114111, %edx
	seta	%dl
	cmpl	%r13d, %esi
	setg	%al
	orb	%al, %dl
	jne	.L1116
	cmpl	$1114111, %esi
	ja	.L1116
	movl	48(%rdi), %ebx
	movq	%rdi, %r12
	movl	%ecx, %r15d
	cmpl	%ebx, %r13d
	jl	.L1108
	leal	512(%r13), %eax
	sarl	$4, %ebx
	andl	$-512, %eax
	movl	%eax, -52(%rbp)
	sarl	$4, %eax
	movl	%eax, %r9d
	cmpl	8(%rdi), %eax
	jg	.L1109
	movq	(%rdi), %r10
.L1110:
	movl	%r9d, %edi
	movslq	%ebx, %rdx
	subl	%ebx, %edi
	cmpl	%r9d, %ebx
	leaq	0(,%rdx,4), %rax
	movl	%edi, %esi
	movl	%edi, -72(%rbp)
	movl	$4, %edi
	leaq	0(,%rsi,4), %rcx
	cmovge	%rdi, %rcx
	leaq	64(%rdx), %rdi
	leaq	(%r12,%rdi), %r11
	addq	%rax, %rcx
	movq	%r11, -64(%rbp)
	addq	%r10, %rax
	movl	$1, %r11d
	addq	%r10, %rcx
	cmpl	%r9d, %ebx
	cmovge	%r11, %rsi
	movq	-64(%rbp), %r11
	addq	%rdi, %rsi
	addq	%r12, %rsi
	cmpq	%rsi, %rax
	setnb	%dil
	cmpq	%r11, %rcx
	setbe	%sil
	orl	%edi, %esi
	leaq	40(%r12), %rdi
	cmpq	%rdi, %rcx
	leaq	44(%r12), %rdi
	setbe	%cl
	cmpq	%rdi, %rax
	setnb	%dil
	orl	%edi, %ecx
	testb	%cl, %sil
	je	.L1119
	leal	-1(%r9), %ecx
	subl	%ebx, %ecx
	cmpl	$14, %ecx
	seta	%sil
	cmpl	%r9d, %ebx
	setl	%cl
	testb	%cl, %sil
	je	.L1119
	cmpl	%r9d, %ebx
	movl	$1, %ecx
	cmovl	-72(%rbp), %ecx
	movq	%r11, %rdx
	movd	40(%r12), %xmm2
	pxor	%xmm1, %xmm1
	movl	%ecx, %esi
	shrl	$4, %esi
	pshufd	$0, %xmm2, %xmm0
	salq	$4, %rsi
	addq	%r11, %rsi
	.p2align 4,,10
	.p2align 3
.L1113:
	movups	%xmm1, (%rdx)
	addq	$16, %rdx
	addq	$64, %rax
	movups	%xmm0, -64(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	cmpq	%rsi, %rdx
	jne	.L1113
	movl	%ecx, %eax
	andl	$-16, %eax
	addl	%eax, %ebx
	cmpl	%eax, %ecx
	je	.L1115
	movslq	%ebx, %rax
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	1(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	2(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	3(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	4(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	5(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	6(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	7(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	8(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	9(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	10(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	11(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	12(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	leal	13(%rbx), %eax
	cmpl	%eax, %r9d
	jle	.L1115
	cltq
	addl	$14, %ebx
	movb	$0, 64(%r12,%rax)
	movl	40(%r12), %edx
	movl	%edx, (%r10,%rax,4)
	cmpl	%ebx, %r9d
	jle	.L1115
	movslq	%ebx, %rbx
	movb	$0, 64(%r12,%rbx)
	movl	40(%r12), %eax
	movl	%eax, (%r10,%rbx,4)
	.p2align 4,,10
	.p2align 3
.L1115:
	movl	-52(%rbp), %eax
	movl	%eax, 48(%r12)
.L1108:
	addq	$40, %rsp
	movl	%r15d, %ecx
	movl	%r13d, %edx
	movl	%r14d, %esi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie8setRangeEiijR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L1116:
	.cfi_restore_state
	movl	$1, (%r8)
.L1104:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1119:
	.cfi_restore_state
	movb	$0, 64(%r12,%rdx)
	movl	40(%r12), %eax
	movl	%eax, (%r10,%rdx,4)
	addq	$1, %rdx
	cmpl	%edx, %r9d
	jg	.L1119
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1109:
	movl	$278528, %edi
	movq	%r8, -64(%rbp)
	movl	%eax, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	je	.L1111
	movq	(%r12), %r11
	movl	$278528, %ecx
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	leal	0(,%rbx,4), %edx
	movq	%rax, -64(%rbp)
	movq	%r11, %rsi
	movslq	%edx, %rdx
	movq	%r11, -72(%rbp)
	call	__memcpy_chk@PLT
	movq	-72(%rbp), %r11
	movq	%r11, %rdi
	call	uprv_free_67@PLT
	movq	-64(%rbp), %r10
	movl	-56(%rbp), %r9d
	movl	$69632, 8(%r12)
	movq	-80(%rbp), %r8
	movq	%r10, (%r12)
	jmp	.L1110
.L1111:
	movl	$7, (%r8)
	jmp	.L1104
	.cfi_endproc
.LFE2160:
	.size	umutablecptrie_setRange_67, .-umutablecptrie_setRange_67
	.p2align 4
	.globl	umutablecptrie_buildImmutable_67
	.type	umutablecptrie_buildImmutable_67, @function
umutablecptrie_buildImmutable_67:
.LFB2161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r15d
	testl	%r15d, %r15d
	jg	.L1131
	movl	%esi, %r8d
	movq	%rcx, %r15
	cmpl	$1, %esi
	ja	.L1199
	movl	%edx, %r13d
	cmpl	$2, %edx
	ja	.L1199
	movq	%rdi, %rbx
	testl	%edx, %edx
	je	.L1135
	cmpl	$2, %edx
	jne	.L1137
	movl	48(%rdi), %edx
	andl	$255, 52(%rdi)
	movabsq	$1095216660735, %rax
	andq	%rax, 40(%rdi)
	xorl	%eax, %eax
	sarl	$4, %edx
	leal	-1(%rdx), %ecx
	testl	%edx, %edx
	jg	.L1149
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1290:
	movq	%rdx, %rax
.L1149:
	cmpb	$0, 64(%rbx,%rax)
	jne	.L1147
	movq	(%rbx), %rdx
	leaq	(%rdx,%rax,4), %rdx
	andl	$255, (%rdx)
.L1147:
	leaq	1(%rax), %rdx
	cmpq	%rcx, %rax
	jne	.L1290
.L1148:
	movl	28(%rbx), %r12d
	testl	%r12d, %r12d
	jle	.L1137
	movq	16(%rbx), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1150:
	andl	$255, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 28(%rbx)
	jg	.L1150
.L1137:
	cmpl	$1, %r8d
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movl	%r8d, -64(%rbp)
	sbbl	%esi, %esi
	andl	$3840, %esi
	addl	$256, %esi
	cmpl	$1, %r8d
	sbbl	%ecx, %ecx
	andl	$61440, %ecx
	addl	$4096, %ecx
	movl	%ecx, -56(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_120MutableCodePointTrie11compactTrieEiR10UErrorCode
	movl	(%r15), %r11d
	movl	-56(%rbp), %ecx
	movl	-64(%rbp), %r8d
	movl	%eax, %r14d
	testl	%r11d, %r11d
	jg	.L1291
	movslq	28(%rbx), %r10
	movq	16(%rbx), %rsi
	movq	%r10, %rax
	leaq	0(,%r10,4), %r9
	cmpl	$1, %r13d
	je	.L1292
	leal	(%r14,%r14), %edx
	testl	%r13d, %r13d
	jne	.L1293
	movl	%r14d, %edi
	movl	44(%rbx), %r11d
	xorl	%eax, %edi
	andl	$1, %edi
	je	.L1155
	addl	$1, %eax
	movl	%eax, 28(%rbx)
	movl	%r11d, (%rsi,%r10,4)
	movslq	28(%rbx), %r9
	movl	44(%rbx), %r11d
	movq	%r9, %rax
	salq	$2, %r9
.L1155:
	movl	52(%rbx), %edi
	cmpl	%r11d, -4(%rsi,%r9)
	je	.L1294
.L1156:
	addl	$1, %eax
	movl	%eax, 28(%rbx)
	movl	%edi, (%rsi,%r9)
	movslq	28(%rbx), %rax
	leal	1(%rax), %edi
	movl	%edi, 28(%rbx)
	movl	44(%rbx), %edi
	movl	%edi, (%rsi,%rax,4)
	movl	28(%rbx), %eax
.L1157:
	leal	(%rdx,%rax,2), %edi
.L1158:
	addl	$48, %edi
	movl	%r8d, -68(%rbp)
	movslq	%edi, %rdi
	movl	%edx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movl	-56(%rbp), %ecx
	movslq	-64(%rbp), %rdx
	testq	%rax, %rax
	movl	-68(%rbp), %r8d
	movq	%rax, %r12
	je	.L1295
	movl	28(%rbx), %r15d
	pxor	%xmm0, %xmm0
	leaq	48(%r12), %rdi
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 24(%rax)
	movl	%r14d, 16(%rax)
	movl	%r15d, 20(%rax)
	movl	48(%rbx), %eax
	movb	%r8b, 30(%r12)
	leal	4095(%rax), %esi
	movl	%eax, 24(%r12)
	sarl	$12, %esi
	movb	%r13b, 31(%r12)
	movw	%si, 28(%r12)
	movl	12(%rbx), %esi
	movq	%rdi, (%r12)
	movw	%si, 38(%r12)
	movl	32(%rbx), %esi
	movl	%esi, 40(%r12)
	movl	40(%rbx), %esi
	movl	%esi, 44(%r12)
	movq	56(%rbx), %rsi
	movq	%rsi, -56(%rbp)
	cmpl	%eax, %ecx
	jl	.L1170
	testl	%r14d, %r14d
	jle	.L1197
	leal	-1(%r14), %r8d
	movq	(%rbx), %rsi
	cmpl	$7, %r8d
	jbe	.L1198
	movl	%r8d, %r9d
	movq	%rsi, %rax
	movq	%rdi, %rcx
	shrl	$3, %r9d
	salq	$7, %r9
	addq	%rsi, %r9
	.p2align 4,,10
	.p2align 3
.L1174:
	movdqu	(%rax), %xmm0
	movdqu	16(%rax), %xmm5
	subq	$-128, %rax
	addq	$16, %rcx
	movdqu	-96(%rax), %xmm1
	movdqu	-80(%rax), %xmm6
	movdqu	-48(%rax), %xmm7
	movdqu	-32(%rax), %xmm2
	shufps	$136, %xmm5, %xmm0
	movdqu	-16(%rax), %xmm4
	shufps	$136, %xmm6, %xmm1
	shufps	$136, %xmm1, %xmm0
	movdqu	-64(%rax), %xmm1
	shufps	$136, %xmm4, %xmm2
	shufps	$136, %xmm7, %xmm1
	shufps	$136, %xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, -16(%rcx)
	cmpq	%r9, %rax
	jne	.L1174
	movl	%r8d, %ecx
	andl	$-8, %ecx
	movl	%ecx, %r9d
	leal	0(,%rcx,4), %eax
	leaq	(%rdi,%r9,2), %r9
.L1172:
	movslq	%eax, %r10
	leal	1(%rcx), %r11d
	movl	(%rsi,%r10,4), %r10d
	movw	%r10w, (%r9)
	leal	4(%rax), %r10d
	cmpl	%r11d, %r14d
	jle	.L1175
	movslq	%r10d, %r10
	leal	2(%rcx), %r11d
	movl	(%rsi,%r10,4), %r10d
	movw	%r10w, 2(%r9)
	leal	8(%rax), %r10d
	cmpl	%r11d, %r14d
	jle	.L1175
	movslq	%r10d, %r10
	leal	3(%rcx), %r11d
	movl	(%rsi,%r10,4), %r10d
	movw	%r10w, 4(%r9)
	leal	12(%rax), %r10d
	cmpl	%r11d, %r14d
	jle	.L1175
	movslq	%r10d, %r10
	leal	4(%rcx), %r11d
	movl	(%rsi,%r10,4), %r10d
	movw	%r10w, 6(%r9)
	leal	16(%rax), %r10d
	cmpl	%r11d, %r14d
	jle	.L1175
	movslq	%r10d, %r10
	leal	20(%rax), %r11d
	movl	(%rsi,%r10,4), %r10d
	movw	%r10w, 8(%r9)
	leal	5(%rcx), %r10d
	cmpl	%r10d, %r14d
	jle	.L1175
	movslq	%r11d, %r11
	addl	$24, %eax
	movl	(%rsi,%r11,4), %r10d
	movw	%r10w, 10(%r9)
	leal	6(%rcx), %r10d
	cmpl	%r10d, %r14d
	jle	.L1175
	cltq
	addl	$7, %ecx
	leaq	0(,%rax,4), %r10
	movl	(%rsi,%rax,4), %eax
	movw	%ax, 12(%r9)
	cmpl	%ecx, %r14d
	jle	.L1175
	movl	16(%rsi,%r10), %eax
	movw	%ax, 14(%r9)
.L1175:
	movl	%r8d, %eax
	leaq	2(%rdi,%rax,2), %rcx
.L1171:
	movq	16(%rbx), %rsi
	addq	%rdx, %rdi
	cmpl	$1, %r13d
	je	.L1176
	cmpl	$2, %r13d
	je	.L1177
	movq	%rcx, 8(%r12)
	testl	%r15d, %r15d
	jle	.L1179
	movl	%r15d, %edi
	cmpl	$7, %r15d
	jle	.L1180
	movl	%r15d, %edx
	xorl	%eax, %eax
	shrl	$3, %edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1182:
	movdqu	(%rsi,%rax,2), %xmm0
	movdqu	16(%rsi,%rax,2), %xmm3
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm3, %xmm0
	punpckhwd	%xmm3, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpcklwd	%xmm2, %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L1182
	movl	%r15d, %eax
	andl	$-8, %eax
	movl	%eax, %edx
	subl	%eax, %r15d
	leaq	(%rsi,%rdx,4), %rsi
	leaq	(%rcx,%rdx,2), %rcx
	cmpl	%edi, %eax
	je	.L1179
.L1180:
	movl	(%rsi), %eax
	movw	%ax, (%rcx)
	cmpl	$1, %r15d
	je	.L1179
	movl	4(%rsi), %eax
	movw	%ax, 2(%rcx)
	cmpl	$2, %r15d
	je	.L1179
	movl	8(%rsi), %eax
	movw	%ax, 4(%rcx)
	cmpl	$3, %r15d
	je	.L1179
	movl	12(%rsi), %eax
	movw	%ax, 6(%rcx)
	cmpl	$4, %r15d
	je	.L1179
	movl	16(%rsi), %eax
	movw	%ax, 8(%rcx)
	cmpl	$5, %r15d
	je	.L1179
	movl	20(%rsi), %eax
	movw	%ax, 10(%rcx)
	cmpl	$6, %r15d
	je	.L1179
	movl	24(%rsi), %eax
	movw	%ax, 12(%rcx)
	.p2align 4,,10
	.p2align 3
.L1179:
	movabsq	$-4294967296, %rax
	movl	$-1, 12(%rbx)
	movq	-56(%rbp), %rdi
	movq	%rax, 28(%rbx)
	movl	36(%rbx), %eax
	movl	$0, 48(%rbx)
	movl	%eax, 40(%rbx)
	movl	%eax, 52(%rbx)
	call	uprv_free_67@PLT
	movq	$0, 56(%rbx)
.L1131:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1292:
	.cfi_restore_state
	testb	$1, %r14b
	jne	.L1296
.L1154:
	movl	-4(%rsi,%r9), %r10d
	movl	44(%rbx), %edi
	leal	(%r14,%r14), %edx
	movl	52(%rbx), %r11d
	cmpl	%edi, %r10d
	je	.L1297
.L1191:
	addl	$1, %eax
	addq	%rsi, %r9
	cmpl	%r11d, %r10d
	je	.L1160
	movl	%eax, 28(%rbx)
	movl	%r11d, (%r9)
	movslq	28(%rbx), %rdi
	leal	1(%rdi), %eax
	leaq	(%rsi,%rdi,4), %r9
	movl	44(%rbx), %edi
.L1160:
	movl	%eax, 28(%rbx)
	movl	%edi, (%r9)
	movl	28(%rbx), %eax
.L1159:
	leal	(%rdx,%rax,4), %edi
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1199:
	movl	$1, (%r15)
	xorl	%r12d, %r12d
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1135:
	movl	48(%rdi), %edx
	andl	$65535, 52(%rdi)
	movabsq	$281470681808895, %rax
	andq	%rax, 40(%rdi)
	xorl	%eax, %eax
	sarl	$4, %edx
	leal	-1(%rdx), %ecx
	testl	%edx, %edx
	jg	.L1142
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1298:
	movq	%rdx, %rax
.L1142:
	cmpb	$0, 64(%rbx,%rax)
	jne	.L1140
	movq	(%rbx), %rdx
	leaq	(%rdx,%rax,4), %rdx
	andl	$65535, (%rdx)
.L1140:
	leaq	1(%rax), %rdx
	cmpq	%rcx, %rax
	jne	.L1298
.L1141:
	movl	28(%rbx), %r14d
	testl	%r14d, %r14d
	jle	.L1137
	movq	16(%rbx), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1144:
	andl	$65535, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 28(%rbx)
	jg	.L1144
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1177:
	movq	%rdi, 8(%r12)
	testl	%r15d, %r15d
	jle	.L1179
	movslq	%r15d, %rcx
	leaq	48(%r12,%rdx), %rax
	leaq	48(%rdx,%rcx), %rdx
	addq	%r12, %rdx
	cmpq	%rdx, %rsi
	leaq	(%rsi,%rcx,4), %rdx
	setnb	%r9b
	cmpq	%rdx, %rax
	setnb	%dl
	orb	%dl, %r9b
	leal	-1(%r15), %edx
	je	.L1185
	cmpl	$14, %edx
	jbe	.L1185
	movl	%r15d, %ecx
	movq	%rax, %rdx
	movdqa	.LC0(%rip), %xmm3
	movq	%rsi, %rax
	shrl	$4, %ecx
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L1187:
	movdqu	(%rax), %xmm0
	movdqu	16(%rax), %xmm5
	addq	$16, %rdx
	addq	$64, %rax
	movdqu	-16(%rax), %xmm7
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm5, %xmm0
	punpckhwd	%xmm5, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	movdqu	-32(%rax), %xmm1
	punpcklwd	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	punpcklwd	%xmm7, %xmm1
	pand	%xmm3, %xmm0
	punpckhwd	%xmm7, %xmm2
	movdqa	%xmm1, %xmm4
	punpckhwd	%xmm2, %xmm4
	punpcklwd	%xmm2, %xmm1
	punpcklwd	%xmm4, %xmm1
	pand	%xmm3, %xmm1
	packuswb	%xmm1, %xmm0
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L1187
	movl	%r15d, %eax
	movl	%r15d, %r9d
	andl	$-16, %eax
	andl	$15, %r9d
	movl	%eax, %edx
	leaq	(%rsi,%rdx,4), %rcx
	addq	%rdx, %rdi
	cmpl	%r15d, %eax
	je	.L1179
	movl	(%rcx), %eax
	movb	%al, (%rdi)
	cmpl	$1, %r9d
	je	.L1179
	movl	4(%rcx), %eax
	movb	%al, 1(%rdi)
	cmpl	$2, %r9d
	je	.L1179
	movl	8(%rcx), %eax
	movb	%al, 2(%rdi)
	cmpl	$3, %r9d
	je	.L1179
	movl	12(%rcx), %eax
	movb	%al, 3(%rdi)
	cmpl	$4, %r9d
	je	.L1179
	movl	16(%rcx), %eax
	movb	%al, 4(%rdi)
	cmpl	$5, %r9d
	je	.L1179
	movl	20(%rcx), %eax
	movb	%al, 5(%rdi)
	cmpl	$6, %r9d
	je	.L1179
	movl	24(%rcx), %eax
	movb	%al, 6(%rdi)
	cmpl	$7, %r9d
	je	.L1179
	movl	28(%rcx), %eax
	movb	%al, 7(%rdi)
	cmpl	$8, %r9d
	je	.L1179
	movl	32(%rcx), %eax
	movb	%al, 8(%rdi)
	cmpl	$9, %r9d
	je	.L1179
	movl	36(%rcx), %eax
	movb	%al, 9(%rdi)
	cmpl	$10, %r9d
	je	.L1179
	movl	40(%rcx), %eax
	movb	%al, 10(%rdi)
	cmpl	$11, %r9d
	je	.L1179
	movl	44(%rcx), %eax
	movb	%al, 11(%rdi)
	cmpl	$12, %r9d
	je	.L1179
	movl	48(%rcx), %eax
	movb	%al, 12(%rdi)
	cmpl	$13, %r9d
	je	.L1179
	movl	52(%rcx), %eax
	movb	%al, 13(%rdi)
	cmpl	$14, %r9d
	je	.L1179
	movl	56(%rcx), %eax
	movb	%al, 14(%rdi)
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	%rdi, 8(%r12)
	movslq	%r15d, %rdx
	salq	$2, %rdx
	call	memcpy@PLT
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1293:
	leal	(%rdx,%rax), %edi
	movl	52(%rbx), %r11d
	movl	%edi, %r12d
	andl	$3, %r12d
	movl	%r12d, -56(%rbp)
	je	.L1299
	addl	$1, %eax
	cmpl	$3, -56(%rbp)
	leaq	(%rsi,%r9), %rdi
	movl	%eax, %r12d
	jne	.L1166
	cmpl	%r11d, -4(%rsi,%r9)
	je	.L1300
.L1162:
	movl	%eax, 28(%rbx)
	movl	-56(%rbp), %eax
	movl	%r11d, (%rsi,%r10,4)
	addl	$1, %eax
	andl	$3, %eax
	.p2align 4,,10
	.p2align 3
.L1168:
	movslq	28(%rbx), %rdi
	movl	52(%rbx), %r9d
	addl	$1, %eax
	andl	$3, %eax
	leal	1(%rdi), %r10d
	movl	%r10d, 28(%rbx)
	movl	%r9d, (%rsi,%rdi,4)
	cmpl	$2, %eax
	jne	.L1168
.L1167:
	movslq	28(%rbx), %rax
	movl	52(%rbx), %r11d
	leal	1(%rax), %r12d
	leaq	(%rsi,%rax,4), %rdi
.L1164:
	movl	%r12d, 28(%rbx)
	movl	%r11d, (%rdi)
	movslq	28(%rbx), %rax
	leal	1(%rax), %edi
	movl	%edi, 28(%rbx)
	movl	44(%rbx), %edi
	movl	%edi, (%rsi,%rax,4)
	movl	28(%rbx), %edi
	addl	%edx, %edi
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	%rdx, -64(%rbp)
	movslq	%r14d, %r14
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %rdi
	leaq	(%rax,%r14,2), %rcx
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	56(%rbx), %rdx
	movslq	%r14d, %rdi
	movl	$-18, %r10d
	addl	$1, %r14d
	movw	%r10w, (%rdx,%rdi,2)
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1299:
	movl	44(%rbx), %r12d
	addl	$1, %eax
	cmpl	%r12d, -4(%rsi,%r9)
	jne	.L1162
	cmpl	%r11d, -8(%rsi,%r9)
	jne	.L1162
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1294:
	cmpl	%edi, -8(%rsi,%r9)
	jne	.L1156
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1291:
	movl	$-1, 12(%rbx)
	movq	56(%rbx), %rdi
	xorl	%r12d, %r12d
	movabsq	$-4294967296, %rax
	movq	%rax, 28(%rbx)
	movl	36(%rbx), %eax
	movl	$0, 48(%rbx)
	movl	%eax, 40(%rbx)
	movl	%eax, 52(%rbx)
	call	uprv_free_67@PLT
	movq	$0, 56(%rbx)
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	%rdi, %rcx
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	%rdi, %r9
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1166:
	cmpl	$2, -56(%rbp)
	je	.L1164
	movl	%eax, 28(%rbx)
	movl	%r11d, (%rsi,%r10,4)
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1185:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1190:
	movl	(%rsi,%rax,4), %ecx
	movb	%cl, (%rdi,%rax)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L1190
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1297:
	cmpl	%r11d, -8(%rsi,%r9)
	jne	.L1191
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1300:
	movl	%eax, 28(%rbx)
	movl	44(%rbx), %eax
	movl	%eax, (%rdi)
	movl	28(%rbx), %edi
	addl	%edx, %edi
	jmp	.L1158
.L1295:
	movl	$4294967295, %eax
	movl	$7, (%r15)
	movq	56(%rbx), %rdi
	salq	$32, %rax
	movl	$-1, 12(%rbx)
	movq	%rax, 28(%rbx)
	movl	36(%rbx), %eax
	movl	$0, 48(%rbx)
	movl	%eax, 40(%rbx)
	movl	%eax, 52(%rbx)
	call	uprv_free_67@PLT
	movq	$0, 56(%rbx)
	jmp	.L1131
	.cfi_endproc
.LFE2161:
	.size	umutablecptrie_buildImmutable_67, .-umutablecptrie_buildImmutable_67
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
