	.file	"unifiedcache.cpp"
	.text
	.p2align 4
	.globl	ucache_hashKeys_67
	.type	ucache_hashKeys_67, @function
ucache_hashKeys_67:
.LFB3279:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE3279:
	.size	ucache_hashKeys_67, .-ucache_hashKeys_67
	.p2align 4
	.globl	ucache_compareKeys_67
	.type	ucache_compareKeys_67, @function
ucache_compareKeys_67:
.LFB3280:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE3280:
	.size	ucache_compareKeys_67, .-ucache_compareKeys_67
	.p2align 4
	.globl	ucache_deleteKey_67
	.type	ucache_deleteKey_67, @function
ucache_deleteKey_67:
.LFB3281:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L4:
	ret
	.cfi_endproc
.LFE3281:
	.size	ucache_deleteKey_67, .-ucache_deleteKey_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CacheKeyBaseD2Ev
	.type	_ZN6icu_6712CacheKeyBaseD2Ev, @function
_ZN6icu_6712CacheKeyBaseD2Ev:
.LFB3283:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712CacheKeyBaseE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3283:
	.size	_ZN6icu_6712CacheKeyBaseD2Ev, .-_ZN6icu_6712CacheKeyBaseD2Ev
	.globl	_ZN6icu_6712CacheKeyBaseD1Ev
	.set	_ZN6icu_6712CacheKeyBaseD1Ev,_ZN6icu_6712CacheKeyBaseD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712CacheKeyBaseD0Ev
	.type	_ZN6icu_6712CacheKeyBaseD0Ev, @function
_ZN6icu_6712CacheKeyBaseD0Ev:
.LFB3285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712CacheKeyBaseE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3285:
	.size	_ZN6icu_6712CacheKeyBaseD0Ev, .-_ZN6icu_6712CacheKeyBaseD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712UnifiedCacheC2ER10UErrorCode
	.type	_ZN6icu_6712UnifiedCacheC2ER10UErrorCode, @function
_ZN6icu_6712UnifiedCacheC2ER10UErrorCode:
.LFB3301:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movdqa	.LC0(%rip), %xmm0
	leaq	16+_ZTVN6icu_6712UnifiedCacheE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	$100, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movups	%xmm0, 16(%rdi)
	testl	%ecx, %ecx
	jle	.L18
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L12
	leaq	16+_ZTVN6icu_6712SharedObjectE(%rip), %rdx
	movq	$0, 16(%rax)
	movq	%r12, %rcx
	leaq	ucache_compareKeys_67(%rip), %rsi
	movq	%rdx, (%rax)
	leaq	ucache_hashKeys_67(%rip), %rdi
	xorl	%edx, %edx
	movl	$0, 12(%rax)
	movq	%rax, 48(%rbx)
	movl	$1, 8(%rax)
	movl	$1, 12(%rax)
	mfence
	movq	48(%rbx), %rax
	movq	%rbx, 16(%rax)
	call	uhash_open_67@PLT
	movl	(%r12), %edx
	movq	%rax, 8(%rbx)
	testl	%edx, %edx
	jle	.L19
.L9:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	leaq	ucache_deleteKey_67(%rip), %rsi
	popq	%r12
	.cfi_restore 12
	movq	%rax, %rdi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uhash_setKeyDeleter_67@PLT
.L12:
	.cfi_restore_state
	movq	$0, 48(%rbx)
	movl	$7, (%r12)
	jmp	.L9
	.cfi_endproc
.LFE3301:
	.size	_ZN6icu_6712UnifiedCacheC2ER10UErrorCode, .-_ZN6icu_6712UnifiedCacheC2ER10UErrorCode
	.globl	_ZN6icu_6712UnifiedCacheC1ER10UErrorCode
	.set	_ZN6icu_6712UnifiedCacheC1ER10UErrorCode,_ZN6icu_6712UnifiedCacheC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712UnifiedCache17setEvictionPolicyEiiR10UErrorCode
	.type	_ZN6icu_6712UnifiedCache17setEvictionPolicyEiiR10UErrorCode, @function
_ZN6icu_6712UnifiedCache17setEvictionPolicyEiiR10UErrorCode:
.LFB3303:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L38
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	js	.L27
	movl	%edx, %r14d
	testl	%edx, %edx
	js	.L27
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	movq	_ZL11gCacheMutex(%rip), %r13
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L25
	movq	%r13, %rdi
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L41
.L25:
	movl	%r15d, 28(%rbx)
	movl	%r14d, 32(%rbx)
	testq	%r12, %r12
	je	.L20
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	$1, (%rcx)
.L20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L41:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE3303:
	.size	_ZN6icu_6712UnifiedCache17setEvictionPolicyEiiR10UErrorCode, .-_ZN6icu_6712UnifiedCache17setEvictionPolicyEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache11unusedCountEv
	.type	_ZNK6icu_6712UnifiedCache11unusedCountEv, @function
_ZNK6icu_6712UnifiedCache11unusedCountEv:
.LFB3304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	_ZL11gCacheMutex(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%r13, %r13
	je	.L43
	movq	%r14, %rdi
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L55
.L43:
	movq	8(%rbx), %rdi
	call	uhash_count_67@PLT
	subl	24(%rbx), %eax
	movl	%eax, %r12d
	testq	%r13, %r13
	je	.L42
	movq	%r14, %rdi
	call	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
.L42:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE3304:
	.size	_ZNK6icu_6712UnifiedCache11unusedCountEv, .-_ZNK6icu_6712UnifiedCache11unusedCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache16autoEvictedCountEv
	.type	_ZNK6icu_6712UnifiedCache16autoEvictedCountEv, @function
_ZNK6icu_6712UnifiedCache16autoEvictedCountEv:
.LFB3305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	_ZL11gCacheMutex(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L57
	movq	%r13, %rdi
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L69
.L57:
	movq	40(%rbx), %r14
	testq	%r12, %r12
	je	.L56
	movq	%r13, %rdi
	call	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
.L56:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE3305:
	.size	_ZNK6icu_6712UnifiedCache16autoEvictedCountEv, .-_ZNK6icu_6712UnifiedCache16autoEvictedCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache8keyCountEv
	.type	_ZNK6icu_6712UnifiedCache8keyCountEv, @function
_ZNK6icu_6712UnifiedCache8keyCountEv:
.LFB3306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	_ZL11gCacheMutex(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%r13, %r13
	je	.L71
	movq	%r14, %rdi
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L83
.L71:
	movq	8(%rbx), %rdi
	call	uhash_count_67@PLT
	movl	%eax, %r12d
	testq	%r13, %r13
	je	.L70
	movq	%r14, %rdi
	call	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
.L70:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L83:
	.cfi_restore_state
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE3306:
	.size	_ZNK6icu_6712UnifiedCache8keyCountEv, .-_ZNK6icu_6712UnifiedCache8keyCountEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache5flushEv
	.type	_ZNK6icu_6712UnifiedCache5flushEv, @function
_ZNK6icu_6712UnifiedCache5flushEv:
.LFB3307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	_ZL11gCacheMutex(%rip), %rax
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, -72(%rbp)
	je	.L85
	movq	%rax, %rdi
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L118
.L85:
	leaq	16(%rbx), %r15
	.p2align 4,,10
	.p2align 3
.L96:
	movq	8(%rbx), %rdi
	call	uhash_count_67@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	jle	.L97
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L121:
	cmpl	$1, 8(%r12)
	je	.L119
.L91:
	addl	$1, %r14d
	cmpl	%r14d, -52(%rbp)
	je	.L90
.L95:
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L120
.L89:
	movq	16(%rsi), %rax
	movq	8(%rsi), %r12
	cmpq	%r12, 48(%rbx)
	jne	.L99
	movl	8(%rax), %edx
	testl	%edx, %edx
	je	.L91
.L99:
	cmpb	$0, 12(%rax)
	jne	.L121
.L93:
	movq	8(%rbx), %rdi
	call	uhash_removeElement_67@PLT
	subl	$1, 8(%r12)
	je	.L98
.L116:
	movl	$1, %r13d
	addl	$1, %r14d
	cmpl	%r14d, -52(%rbp)
	jne	.L95
.L90:
	testb	%r13b, %r13b
	jne	.L96
.L97:
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L122
	movq	-72(%rbp), %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movl	$-1, 16(%rbx)
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L89
	testb	%r13b, %r13b
	jne	.L96
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	subl	$1, 20(%rbx)
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	testl	%eax, %eax
	jne	.L94
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	$1, %r13d
	call	*8(%rax)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	testl	%eax, %eax
	jne	.L91
	movq	-64(%rbp), %rsi
	movq	8(%rsi), %r12
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	movq	$0, 16(%r12)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L122:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L118:
	.cfi_restore_state
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE3307:
	.size	_ZNK6icu_6712UnifiedCache5flushEv, .-_ZNK6icu_6712UnifiedCache5flushEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712UnifiedCacheD2Ev
	.type	_ZN6icu_6712UnifiedCacheD2Ev, @function
_ZN6icu_6712UnifiedCacheD2Ev:
.LFB3310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712UnifiedCacheE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rax, (%rdi)
	call	_ZNK6icu_6712UnifiedCache5flushEv
	movq	_ZL11gCacheMutex(%rip), %rax
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, -56(%rbp)
	je	.L124
	movq	%rax, %rdi
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L148
.L124:
	movq	8(%r13), %rdi
	leaq	16(%r13), %r14
	xorl	%r12d, %r12d
	call	uhash_count_67@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jg	.L132
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L130:
	addl	$1, %r12d
	cmpl	%r12d, %ebx
	je	.L129
.L132:
	movq	8(%r13), %rdi
	movq	%r14, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L149
.L128:
	movq	8(%rsi), %r15
	movq	8(%r13), %rdi
	call	uhash_removeElement_67@PLT
	subl	$1, 8(%r15)
	jne	.L130
	subl	$1, 20(%r13)
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	testl	%eax, %eax
	jne	.L131
	movq	(%r15), %rax
	addl	$1, %r12d
	movq	%r15, %rdi
	call	*8(%rax)
	cmpl	%r12d, %ebx
	jne	.L132
	.p2align 4,,10
	.p2align 3
.L129:
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L127
	movq	-56(%rbp), %rdi
	call	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
.L127:
	movq	8(%r13), %rdi
	call	uhash_close_67@PLT
	movq	48(%r13), %rdi
	movq	$0, 8(%r13)
	testq	%rdi, %rdi
	je	.L133
	movq	(%rdi), %rax
	call	*8(%rax)
.L133:
	movq	$0, 48(%r13)
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716UnifiedCacheBaseD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movl	$-1, 16(%r13)
	movq	8(%r13), %rdi
	movq	%r14, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L128
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L131:
	movq	$0, 16(%r15)
	jmp	.L130
.L148:
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE3310:
	.size	_ZN6icu_6712UnifiedCacheD2Ev, .-_ZN6icu_6712UnifiedCacheD2Ev
	.globl	_ZN6icu_6712UnifiedCacheD1Ev
	.set	_ZN6icu_6712UnifiedCacheD1Ev,_ZN6icu_6712UnifiedCacheD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712UnifiedCacheD0Ev
	.type	_ZN6icu_6712UnifiedCacheD0Ev, @function
_ZN6icu_6712UnifiedCacheD0Ev:
.LFB3312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6712UnifiedCacheD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3312:
	.size	_ZN6icu_6712UnifiedCacheD0Ev, .-_ZN6icu_6712UnifiedCacheD0Ev
	.p2align 4
	.type	unifiedcache_cleanup, @function
unifiedcache_cleanup:
.LFB3278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movl	$0, _ZL14gCacheInitOnce(%rip)
	mfence
	movq	_ZL6gCache(%rip), %r12
	testq	%r12, %r12
	je	.L153
	movq	(%r12), %rax
	leaq	_ZN6icu_6712UnifiedCacheD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L154
	call	_ZN6icu_6712UnifiedCacheD1Ev
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L153:
	movq	_ZL25gInProgressValueAddedCond(%rip), %rdi
	movq	$0, _ZL6gCache(%rip)
	movq	$0, _ZL11gCacheMutex(%rip)
	call	_ZNSt18condition_variableD1Ev@PLT
	movl	$1, %eax
	movq	$0, _ZL25gInProgressValueAddedCond(%rip)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	call	*%rax
	jmp	.L153
	.cfi_endproc
.LFE3278:
	.size	unifiedcache_cleanup, .-unifiedcache_cleanup
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode
	.type	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode, @function
_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode:
.LFB3299:
	.cfi_startproc
	endbr64
	movl	(%rdi), %esi
	testl	%esi, %esi
	jle	.L181
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	_ZL14gCacheInitOnce(%rip), %eax
	movq	%rdi, %rbx
	cmpl	$2, %eax
	jne	.L182
.L161:
	movl	4+_ZL14gCacheInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L168
	movl	%eax, (%rbx)
.L160:
	xorl	%eax, %eax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	_ZL14gCacheInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L161
	leaq	unifiedcache_cleanup(%rip), %rsi
	movl	$23, %edi
	call	ucln_common_registerCleanup_67@PLT
	pxor	%xmm0, %xmm0
	leaq	_ZZZN6icu_67L9cacheInitER10UErrorCodeENKUlvE_clEvE7storage(%rip), %rax
	leaq	_ZZZN6icu_67L9cacheInitER10UErrorCodeENKUlvE0_clEvE7storage(%rip), %rdi
	movq	%rax, _ZL11gCacheMutex(%rip)
	movups	%xmm0, _ZZZN6icu_67L9cacheInitER10UErrorCodeENKUlvE_clEvE7storage(%rip)
	movups	%xmm0, 16+_ZZZN6icu_67L9cacheInitER10UErrorCodeENKUlvE_clEvE7storage(%rip)
	movq	$0, 32+_ZZZN6icu_67L9cacheInitER10UErrorCodeENKUlvE_clEvE7storage(%rip)
	call	_ZNSt18condition_variableC1Ev@PLT
	leaq	_ZZZN6icu_67L9cacheInitER10UErrorCodeENKUlvE0_clEvE7storage(%rip), %rax
	movl	$56, %edi
	movq	%rax, _ZL25gInProgressValueAddedCond(%rip)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L162
	movdqa	.LC0(%rip), %xmm0
	movl	(%rbx), %ecx
	leaq	16+_ZTVN6icu_6712UnifiedCacheE(%rip), %rax
	movq	$0, 8(%r12)
	movq	%rax, (%r12)
	movl	$100, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movups	%xmm0, 16(%r12)
	testl	%ecx, %ecx
	jle	.L183
.L163:
	movq	%r12, _ZL6gCache(%rip)
.L169:
	movq	(%r12), %rax
	leaq	_ZN6icu_6712UnifiedCacheD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L166
	call	_ZN6icu_6712UnifiedCacheD1Ev
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L167:
	movq	$0, _ZL6gCache(%rip)
	movl	(%rbx), %eax
.L165:
	leaq	_ZL14gCacheInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL14gCacheInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L168:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L160
	movq	_ZL6gCache(%rip), %rax
.L159:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L164
	leaq	16+_ZTVN6icu_6712SharedObjectE(%rip), %rcx
	movq	$0, 16(%rax)
	xorl	%edx, %edx
	leaq	ucache_hashKeys_67(%rip), %rdi
	movq	%rcx, (%rax)
	leaq	ucache_compareKeys_67(%rip), %rsi
	movq	%rbx, %rcx
	movl	$0, 12(%rax)
	movq	%rax, 48(%r12)
	movl	$1, 8(%rax)
	movl	$1, 12(%rax)
	mfence
	movq	48(%r12), %rax
	movq	%r12, 16(%rax)
	call	uhash_open_67@PLT
	movl	(%rbx), %edx
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	testl	%edx, %edx
	jg	.L163
	leaq	ucache_deleteKey_67(%rip), %rsi
	call	uhash_setKeyDeleter_67@PLT
	movl	(%rbx), %eax
	movq	%r12, _ZL6gCache(%rip)
	testl	%eax, %eax
	jle	.L165
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L166:
	call	*%rax
	jmp	.L167
.L162:
	movl	$7, (%rbx)
	jmp	.L167
.L164:
	movq	$0, 48(%r12)
	movl	$7, (%rbx)
	movq	%r12, _ZL6gCache(%rip)
	jmp	.L169
	.cfi_endproc
.LFE3299:
	.size	_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode, .-_ZN6icu_6712UnifiedCache11getInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache12_nextElementEv
	.type	_ZNK6icu_6712UnifiedCache12_nextElementEv, @function
_ZNK6icu_6712UnifiedCache12_nextElementEv:
.LFB3313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	%r12, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	je	.L187
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movl	$-1, 16(%rbx)
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uhash_nextElement_67@PLT
	.cfi_endproc
.LFE3313:
	.size	_ZNK6icu_6712UnifiedCache12_nextElementEv, .-_ZNK6icu_6712UnifiedCache12_nextElementEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache6_flushEa
	.type	_ZNK6icu_6712UnifiedCache6_flushEa, @function
_ZNK6icu_6712UnifiedCache6_flushEa:
.LFB3314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	call	uhash_count_67@PLT
	movl	%eax, -52(%rbp)
	testl	%eax, %eax
	jle	.L196
	movb	$0, -53(%rbp)
	leaq	16(%r15), %r14
	xorl	%ebx, %ebx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L209:
	movq	16(%rsi), %rax
	cmpq	%r12, 48(%r15)
	jne	.L198
	movl	8(%rax), %edx
	testl	%edx, %edx
	je	.L192
.L198:
	cmpb	$0, 12(%rax)
	je	.L191
	cmpl	$1, 8(%r12)
	je	.L207
	.p2align 4,,10
	.p2align 3
.L192:
	addl	$1, %ebx
	cmpl	%ebx, -52(%rbp)
	je	.L188
.L195:
	movq	8(%r15), %rdi
	movq	%r14, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L208
.L190:
	movq	8(%rsi), %r12
	testb	%r13b, %r13b
	je	.L209
.L191:
	movq	8(%r15), %rdi
	call	uhash_removeElement_67@PLT
	subl	$1, 8(%r12)
	movb	$1, -53(%rbp)
	jne	.L192
	subl	$1, 20(%r15)
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	testl	%eax, %eax
	jne	.L194
	movq	(%r12), %rax
	movq	%r12, %rdi
	addl	$1, %ebx
	call	*8(%rax)
	cmpl	%ebx, -52(%rbp)
	jne	.L195
.L188:
	movzbl	-53(%rbp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movl	$-1, 16(%r15)
	movq	8(%r15), %rdi
	movq	%r14, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L190
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L194:
	movq	$0, 16(%r12)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	testl	%eax, %eax
	jne	.L192
	movq	-64(%rbp), %rsi
	movq	8(%rsi), %r12
	jmp	.L191
.L196:
	movb	$0, -53(%rbp)
	jmp	.L188
	.cfi_endproc
.LFE3314:
	.size	_ZNK6icu_6712UnifiedCache6_flushEa, .-_ZNK6icu_6712UnifiedCache6_flushEa
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache27_computeCountOfItemsToEvictEv
	.type	_ZNK6icu_6712UnifiedCache27_computeCountOfItemsToEvictEv, @function
_ZNK6icu_6712UnifiedCache27_computeCountOfItemsToEvictEv:
.LFB3315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	uhash_count_67@PLT
	movl	24(%rbx), %edx
	movl	32(%rbx), %ecx
	imull	%edx, %ecx
	subl	%edx, %eax
	movslq	%ecx, %rdx
	sarl	$31, %ecx
	imulq	$1374389535, %rdx, %rdx
	sarq	$37, %rdx
	subl	%ecx, %edx
	movl	28(%rbx), %ecx
	cmpl	%ecx, %edx
	cmovl	%ecx, %edx
	subl	%edx, %eax
	movl	$0, %edx
	cmovs	%edx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3315:
	.size	_ZNK6icu_6712UnifiedCache27_computeCountOfItemsToEvictEv, .-_ZNK6icu_6712UnifiedCache27_computeCountOfItemsToEvictEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache17_runEvictionSliceEv
	.type	_ZNK6icu_6712UnifiedCache17_runEvictionSliceEv, @function
_ZNK6icu_6712UnifiedCache17_runEvictionSliceEv:
.LFB3316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rdi
	call	uhash_count_67@PLT
	movl	24(%r14), %edx
	movl	32(%r14), %ecx
	imull	%edx, %ecx
	subl	%edx, %eax
	movl	%eax, %r12d
	movl	28(%r14), %eax
	movslq	%ecx, %rdx
	sarl	$31, %ecx
	imulq	$1374389535, %rdx, %rdx
	sarq	$37, %rdx
	subl	%ecx, %edx
	cmpl	%eax, %edx
	cmovl	%eax, %edx
	subl	%edx, %r12d
	testl	%r12d, %r12d
	jg	.L238
.L212:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	leaq	16(%r14), %r13
	movl	$10, %ebx
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L218:
	movq	8(%r14), %rdi
	call	uhash_removeElement_67@PLT
	subl	$1, 8(%r15)
	je	.L239
.L220:
	addq	$1, 40(%r14)
	subl	$1, %r12d
	je	.L212
.L216:
	subl	$1, %ebx
	je	.L212
.L221:
	movq	8(%r14), %rdi
	movq	%r13, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L240
.L214:
	movq	16(%rsi), %rax
	movq	8(%rsi), %r15
	movl	8(%rax), %edx
	testl	%edx, %edx
	jne	.L223
	cmpq	%r15, 48(%r14)
	je	.L216
.L223:
	cmpb	$0, 12(%rax)
	je	.L218
	cmpl	$1, 8(%r15)
	jne	.L216
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	testl	%eax, %eax
	jne	.L216
	movq	-56(%rbp), %rsi
	movq	8(%r14), %rdi
	movq	8(%rsi), %r15
	call	uhash_removeElement_67@PLT
	subl	$1, 8(%r15)
	jne	.L220
	.p2align 4,,10
	.p2align 3
.L239:
	subl	$1, 20(%r14)
	movq	%r15, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	testl	%eax, %eax
	jne	.L219
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L240:
	movl	$-1, 16(%r14)
	movq	8(%r14), %rdi
	movq	%r13, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L214
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	$0, 16(%r15)
	jmp	.L220
	.cfi_endproc
.LFE3316:
	.size	_ZNK6icu_6712UnifiedCache17_runEvictionSliceEv, .-_ZNK6icu_6712UnifiedCache17_runEvictionSliceEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache24handleUnreferencedObjectEv
	.type	_ZNK6icu_6712UnifiedCache24handleUnreferencedObjectEv, @function
_ZNK6icu_6712UnifiedCache24handleUnreferencedObjectEv:
.LFB3308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	movq	_ZL11gCacheMutex(%rip), %r13
	testq	%rbx, %rbx
	je	.L242
	movq	%r13, %rdi
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L251
.L242:
	subl	$1, 24(%r12)
	movq	%r12, %rdi
	call	_ZNK6icu_6712UnifiedCache17_runEvictionSliceEv
	testq	%rbx, %rbx
	je	.L241
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L251:
	.cfi_restore_state
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE3308:
	.size	_ZNK6icu_6712UnifiedCache24handleUnreferencedObjectEv, .-_ZNK6icu_6712UnifiedCache24handleUnreferencedObjectEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache7_putNewERKNS_12CacheKeyBaseEPKNS_12SharedObjectE10UErrorCodeRS7_
	.type	_ZNK6icu_6712UnifiedCache7_putNewERKNS_12CacheKeyBaseEPKNS_12SharedObjectE10UErrorCodeRS7_, @function
_ZNK6icu_6712UnifiedCache7_putNewERKNS_12CacheKeyBaseEPKNS_12SharedObjectE10UErrorCodeRS7_:
.LFB3317:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L259
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%r8, %rbx
	call	*32(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L262
	movl	8(%r13), %edx
	movl	%r14d, 8(%rax)
	testl	%edx, %edx
	jne	.L256
	movb	$1, 12(%rax)
	movq	%r12, 16(%r13)
	addl	$1, 20(%r12)
	addl	$1, 24(%r12)
.L256:
	movq	8(%r12), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	call	uhash_put_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L252
	addl	$1, 8(%r13)
.L252:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$7, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3317:
	.size	_ZNK6icu_6712UnifiedCache7_putNewERKNS_12CacheKeyBaseEPKNS_12SharedObjectE10UErrorCodeRS7_, .-_ZNK6icu_6712UnifiedCache7_putNewERKNS_12CacheKeyBaseEPKNS_12SharedObjectE10UErrorCodeRS7_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache5_pollERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode
	.type	_ZNK6icu_6712UnifiedCache5_pollERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode, @function
_ZNK6icu_6712UnifiedCache5_pollERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode:
.LFB3319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	_ZL11gCacheMutex(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -72(%rbp)
	movq	%rdi, -80(%rbp)
	testq	%rdi, %rdi
	je	.L307
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rsi, %r12
	movq	%rdx, %r14
	movq	%rcx, %r13
	je	.L265
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L308
.L265:
	movq	8(%r15), %rdi
	movq	%r12, %rsi
	movb	$1, -72(%rbp)
	call	uhash_find_67@PLT
	testq	%rax, %rax
	je	.L266
	leaq	-80(%rbp), %rbx
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L309:
	leaq	12(%rsi), %rcx
	movl	$1, %edi
	lock xaddl	%edi, (%rcx)
	testl	%edi, %edi
	jne	.L268
	addl	$1, 24(%r15)
.L268:
	cmpq	%rsi, 48(%r15)
	sete	%sil
	andl	%esi, %edx
	lock subl	$1, (%rcx)
	jne	.L284
	subl	$1, 24(%r15)
.L284:
	testb	%dl, %dl
	je	.L269
.L310:
	movq	_ZL25gInProgressValueAddedCond(%rip), %rdi
	movq	%rbx, %rsi
	call	_ZNSt18condition_variable4waitERSt11unique_lockISt5mutexE@PLT
	movq	8(%r15), %rdi
	movq	%r12, %rsi
	call	uhash_find_67@PLT
	testq	%rax, %rax
	je	.L266
.L270:
	movq	16(%rax), %rdx
	movq	8(%rax), %rsi
	movl	8(%rdx), %edi
	testl	%edi, %edi
	sete	%dl
	testq	%rsi, %rsi
	jne	.L309
	cmpq	$0, 48(%r15)
	sete	%cl
	andl	%ecx, %edx
	testb	%dl, %dl
	jne	.L310
.L269:
	movq	16(%rax), %rdx
	movl	8(%rdx), %edx
	movl	%edx, 0(%r13)
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L272
	lock subl	$1, 12(%rdx)
	jne	.L272
	subl	$1, 24(%r15)
.L272:
	movq	8(%rax), %rax
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L305
	movl	$1, %edx
	lock xaddl	%edx, 12(%rax)
	testl	%edx, %edx
	jne	.L305
	addl	$1, 24(%r15)
	.p2align 4,,10
	.p2align 3
.L305:
	cmpb	$0, -72(%rbp)
	movl	$1, %r12d
	je	.L263
.L312:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L263
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L263
	call	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L266:
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L306
	movq	(%r12), %rax
	movq	48(%r15), %rbx
	movq	%r12, %rdi
	call	*32(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L311
	movl	8(%rbx), %ecx
	movl	$0, 8(%rax)
	testl	%ecx, %ecx
	jne	.L277
	movb	$1, 12(%rax)
	movq	%r15, 16(%rbx)
	addl	$1, 20(%r15)
	addl	$1, 24(%r15)
.L277:
	movq	8(%r15), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	call	uhash_put_67@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L278
.L306:
	xorl	%r12d, %r12d
.L274:
	cmpb	$0, -72(%rbp)
	jne	.L312
.L263:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L313
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	addl	$1, 8(%rbx)
	jmp	.L306
.L311:
	movl	$7, 0(%r13)
	xorl	%r12d, %r12d
	jmp	.L274
.L313:
	call	__stack_chk_fail@PLT
.L308:
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
.L307:
	movl	$1, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE3319:
	.size	_ZNK6icu_6712UnifiedCache5_pollERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode, .-_ZNK6icu_6712UnifiedCache5_pollERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache15_registerMasterEPKNS_12CacheKeyBaseEPKNS_12SharedObjectE
	.type	_ZNK6icu_6712UnifiedCache15_registerMasterEPKNS_12CacheKeyBaseEPKNS_12SharedObjectE, @function
_ZNK6icu_6712UnifiedCache15_registerMasterEPKNS_12CacheKeyBaseEPKNS_12SharedObjectE:
.LFB3321:
	.cfi_startproc
	endbr64
	movb	$1, 12(%rsi)
	movq	%rdi, 16(%rdx)
	addl	$1, 20(%rdi)
	addl	$1, 24(%rdi)
	ret
	.cfi_endproc
.LFE3321:
	.size	_ZNK6icu_6712UnifiedCache15_registerMasterEPKNS_12CacheKeyBaseEPKNS_12SharedObjectE, .-_ZNK6icu_6712UnifiedCache15_registerMasterEPKNS_12CacheKeyBaseEPKNS_12SharedObjectE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache4_putEPK12UHashElementPKNS_12SharedObjectE10UErrorCode
	.type	_ZNK6icu_6712UnifiedCache4_putEPK12UHashElementPKNS_12SharedObjectE10UErrorCode, @function
_ZNK6icu_6712UnifiedCache4_putEPK12UHashElementPKNS_12SharedObjectE10UErrorCode:
.LFB3322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rsi), %rcx
	movl	8(%rdx), %eax
	movq	8(%rsi), %r12
	movl	%r8d, 8(%rcx)
	testl	%eax, %eax
	jne	.L316
	movb	$1, 12(%rcx)
	movq	%rdi, 16(%rdx)
	addl	$1, 20(%rdi)
	addl	$1, 24(%rdi)
.L316:
	addl	$1, %eax
	movl	%eax, 8(%rdx)
	movq	%rdx, 8(%rsi)
	subl	$1, 8(%r12)
	je	.L320
.L317:
	movq	_ZL25gInProgressValueAddedCond(%rip), %rdi
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt18condition_variable10notify_allEv@PLT
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	subl	$1, 20(%rdi)
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	testl	%eax, %eax
	jne	.L318
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L318:
	movq	$0, 16(%r12)
	jmp	.L317
	.cfi_endproc
.LFE3322:
	.size	_ZNK6icu_6712UnifiedCache4_putEPK12UHashElementPKNS_12SharedObjectE10UErrorCode, .-_ZNK6icu_6712UnifiedCache4_putEPK12UHashElementPKNS_12SharedObjectE10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache6_fetchEPK12UHashElementRPKNS_12SharedObjectER10UErrorCode
	.type	_ZNK6icu_6712UnifiedCache6_fetchEPK12UHashElementRPKNS_12SharedObjectER10UErrorCode, @function
_ZNK6icu_6712UnifiedCache6_fetchEPK12UHashElementRPKNS_12SharedObjectER10UErrorCode:
.LFB3323:
	.cfi_startproc
	endbr64
	movq	16(%rsi), %rax
	movl	8(%rax), %eax
	movl	%eax, (%rcx)
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L323
	lock subl	$1, 12(%rax)
	je	.L328
.L323:
	movq	8(%rsi), %rax
	movq	%rax, (%rdx)
	testq	%rax, %rax
	je	.L321
	movl	$1, %edx
	lock xaddl	%edx, 12(%rax)
	testl	%edx, %edx
	je	.L329
.L321:
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	subl	$1, 24(%rdi)
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L329:
	addl	$1, 24(%rdi)
	ret
	.cfi_endproc
.LFE3323:
	.size	_ZNK6icu_6712UnifiedCache6_fetchEPK12UHashElementRPKNS_12SharedObjectER10UErrorCode, .-_ZNK6icu_6712UnifiedCache6_fetchEPK12UHashElementRPKNS_12SharedObjectER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache11_inProgressEPK12UHashElement
	.type	_ZNK6icu_6712UnifiedCache11_inProgressEPK12UHashElement, @function
_ZNK6icu_6712UnifiedCache11_inProgressEPK12UHashElement:
.LFB3324:
	.cfi_startproc
	endbr64
	movq	16(%rsi), %rax
	movq	8(%rsi), %rcx
	movl	8(%rax), %eax
	testl	%eax, %eax
	sete	%al
	testq	%rcx, %rcx
	je	.L331
	leaq	12(%rcx), %rdx
	movl	$1, %esi
	lock xaddl	%esi, (%rdx)
	testl	%esi, %esi
	je	.L335
.L332:
	cmpq	%rcx, 48(%rdi)
	sete	%cl
	andl	%ecx, %eax
	lock subl	$1, (%rdx)
	jne	.L336
	subl	$1, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	cmpq	$0, 48(%rdi)
	sete	%dl
	andl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	addl	$1, 24(%rdi)
	jmp	.L332
	.cfi_endproc
.LFE3324:
	.size	_ZNK6icu_6712UnifiedCache11_inProgressEPK12UHashElement, .-_ZNK6icu_6712UnifiedCache11_inProgressEPK12UHashElement
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache18_putIfAbsentAndGetERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode
	.type	_ZNK6icu_6712UnifiedCache18_putIfAbsentAndGetERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode, @function
_ZNK6icu_6712UnifiedCache18_putIfAbsentAndGetERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode:
.LFB3318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	_ZL11gCacheMutex(%rip), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L338
	movq	%r15, %rdi
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L377
.L338:
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	call	uhash_find_67@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L339
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6712UnifiedCache11_inProgressEPK12UHashElement
	testb	%al, %al
	je	.L378
	movl	0(%r13), %eax
	movq	(%rbx), %rdx
	movq	16(%r9), %rcx
	movq	8(%r9), %r12
	movl	%eax, 8(%rcx)
	movl	8(%rdx), %eax
	testl	%eax, %eax
	jne	.L358
	movb	$1, 12(%rcx)
	movq	%r14, 16(%rdx)
	addl	$1, 20(%r14)
	addl	$1, 24(%r14)
.L358:
	addl	$1, %eax
	movl	%eax, 8(%rdx)
	movq	%rdx, 8(%r9)
	subl	$1, 8(%r12)
	je	.L379
.L353:
	movq	_ZL25gInProgressValueAddedCond(%rip), %rdi
	call	_ZNSt18condition_variable10notify_allEv@PLT
.L352:
	movq	%r14, %rdi
	call	_ZNK6icu_6712UnifiedCache17_runEvictionSliceEv
.L376:
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L337
	movq	%r15, %rdi
	call	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
.L337:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L380
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	subl	$1, 20(%r14)
	movq	%r12, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	testl	%eax, %eax
	jne	.L354
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L339:
	movq	(%r12), %rax
	movq	(%rbx), %rbx
	movl	$0, -60(%rbp)
	movq	%r12, %rdi
	movl	0(%r13), %r13d
	call	*32(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L352
	movl	8(%rbx), %edx
	movl	%r13d, 8(%rsi)
	testl	%edx, %edx
	jne	.L350
	movb	$1, 12(%rsi)
	movq	%r14, 16(%rbx)
	addl	$1, 20(%r14)
	addl	$1, 24(%r14)
.L350:
	movq	8(%r14), %rdi
	leaq	-60(%rbp), %rcx
	movq	%rbx, %rdx
	call	uhash_put_67@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L352
	addl	$1, 8(%rbx)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L378:
	movq	16(%r9), %rax
	movl	8(%rax), %eax
	movl	%eax, 0(%r13)
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L342
	lock subl	$1, 12(%rax)
	je	.L381
.L342:
	movq	8(%r9), %rax
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L376
	movl	$1, %edx
	lock xaddl	%edx, 12(%rax)
	testl	%edx, %edx
	jne	.L376
	addl	$1, 24(%r14)
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L354:
	movq	$0, 16(%r12)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L381:
	subl	$1, 24(%r14)
	jmp	.L342
.L380:
	call	__stack_chk_fail@PLT
.L377:
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE3318:
	.size	_ZNK6icu_6712UnifiedCache18_putIfAbsentAndGetERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode, .-_ZNK6icu_6712UnifiedCache18_putIfAbsentAndGetERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode
	.type	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode, @function
_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode:
.LFB3320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movq	%r8, %rcx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_6712UnifiedCache5_pollERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode
	testb	%al, %al
	jne	.L406
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L382
	movq	(%r15), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	*48(%rax)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L407
.L388:
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6712UnifiedCache18_putIfAbsentAndGetERKNS_12CacheKeyBaseERPKNS_12SharedObjectER10UErrorCode
.L406:
	movq	(%rbx), %rdi
	cmpq	%rdi, 48(%r13)
	jne	.L382
	testq	%rdi, %rdi
	jne	.L408
.L382:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	call	_ZNK6icu_6712SharedObject9removeRefEv@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	movq	48(%r13), %rdi
	testq	%rdi, %rdi
	je	.L388
	movq	%rdi, (%rbx)
	call	_ZNK6icu_6712SharedObject6addRefEv@PLT
	jmp	.L388
	.cfi_endproc
.LFE3320:
	.size	_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode, .-_ZNK6icu_6712UnifiedCache4_getERKNS_12CacheKeyBaseERPKNS_12SharedObjectEPKvR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache11_inProgressEPKNS_12SharedObjectE10UErrorCode
	.type	_ZNK6icu_6712UnifiedCache11_inProgressEPKNS_12SharedObjectE10UErrorCode, @function
_ZNK6icu_6712UnifiedCache11_inProgressEPKNS_12SharedObjectE10UErrorCode:
.LFB3325:
	.cfi_startproc
	endbr64
	cmpq	%rsi, 48(%rdi)
	sete	%al
	testl	%edx, %edx
	sete	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE3325:
	.size	_ZNK6icu_6712UnifiedCache11_inProgressEPKNS_12SharedObjectE10UErrorCode, .-_ZNK6icu_6712UnifiedCache11_inProgressEPKNS_12SharedObjectE10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache12_isEvictableEPK12UHashElement
	.type	_ZNK6icu_6712UnifiedCache12_isEvictableEPK12UHashElement, @function
_ZNK6icu_6712UnifiedCache12_isEvictableEPK12UHashElement:
.LFB3326:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	16(%rsi), %rdx
	movq	8(%rsi), %rdi
	cmpq	%rdi, 48(%r8)
	jne	.L417
	movl	8(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L422
.L417:
	cmpb	$0, 12(%rdx)
	movl	$1, %eax
	jne	.L425
.L422:
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	xorl	%eax, %eax
	cmpl	$1, 8(%rdi)
	jne	.L422
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3326:
	.size	_ZNK6icu_6712UnifiedCache12_isEvictableEPK12UHashElement, .-_ZNK6icu_6712UnifiedCache12_isEvictableEPK12UHashElement
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache13removeSoftRefEPKNS_12SharedObjectE
	.type	_ZNK6icu_6712UnifiedCache13removeSoftRefEPKNS_12SharedObjectE, @function
_ZNK6icu_6712UnifiedCache13removeSoftRefEPKNS_12SharedObjectE:
.LFB3327:
	.cfi_startproc
	endbr64
	subl	$1, 8(%rsi)
	je	.L433
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	subl	$1, 20(%rdi)
	movq	%rsi, %rdi
	call	_ZNK6icu_6712SharedObject11getRefCountEv@PLT
	testl	%eax, %eax
	jne	.L428
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movq	$0, 16(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3327:
	.size	_ZNK6icu_6712UnifiedCache13removeSoftRefEPKNS_12SharedObjectE, .-_ZNK6icu_6712UnifiedCache13removeSoftRefEPKNS_12SharedObjectE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache13removeHardRefEPKNS_12SharedObjectE
	.type	_ZNK6icu_6712UnifiedCache13removeHardRefEPKNS_12SharedObjectE, @function
_ZNK6icu_6712UnifiedCache13removeHardRefEPKNS_12SharedObjectE:
.LFB3328:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L434
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rsi)
	subl	$1, %eax
	jne	.L434
	subl	$1, 24(%rdi)
.L434:
	ret
	.cfi_endproc
.LFE3328:
	.size	_ZNK6icu_6712UnifiedCache13removeHardRefEPKNS_12SharedObjectE, .-_ZNK6icu_6712UnifiedCache13removeHardRefEPKNS_12SharedObjectE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712UnifiedCache10addHardRefEPKNS_12SharedObjectE
	.type	_ZNK6icu_6712UnifiedCache10addHardRefEPKNS_12SharedObjectE, @function
_ZNK6icu_6712UnifiedCache10addHardRefEPKNS_12SharedObjectE:
.LFB3329:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L438
	movl	$1, %edx
	lock xaddl	%edx, 12(%rsi)
	leal	1(%rdx), %eax
	testl	%edx, %edx
	je	.L442
.L438:
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	addl	$1, 24(%rdi)
	ret
	.cfi_endproc
.LFE3329:
	.size	_ZNK6icu_6712UnifiedCache10addHardRefEPKNS_12SharedObjectE, .-_ZNK6icu_6712UnifiedCache10addHardRefEPKNS_12SharedObjectE
	.weak	_ZTSN6icu_6712CacheKeyBaseE
	.section	.rodata._ZTSN6icu_6712CacheKeyBaseE,"aG",@progbits,_ZTSN6icu_6712CacheKeyBaseE,comdat
	.align 16
	.type	_ZTSN6icu_6712CacheKeyBaseE, @object
	.size	_ZTSN6icu_6712CacheKeyBaseE, 24
_ZTSN6icu_6712CacheKeyBaseE:
	.string	"N6icu_6712CacheKeyBaseE"
	.weak	_ZTIN6icu_6712CacheKeyBaseE
	.section	.data.rel.ro._ZTIN6icu_6712CacheKeyBaseE,"awG",@progbits,_ZTIN6icu_6712CacheKeyBaseE,comdat
	.align 8
	.type	_ZTIN6icu_6712CacheKeyBaseE, @object
	.size	_ZTIN6icu_6712CacheKeyBaseE, 24
_ZTIN6icu_6712CacheKeyBaseE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712CacheKeyBaseE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6712UnifiedCacheE
	.section	.rodata._ZTSN6icu_6712UnifiedCacheE,"aG",@progbits,_ZTSN6icu_6712UnifiedCacheE,comdat
	.align 16
	.type	_ZTSN6icu_6712UnifiedCacheE, @object
	.size	_ZTSN6icu_6712UnifiedCacheE, 24
_ZTSN6icu_6712UnifiedCacheE:
	.string	"N6icu_6712UnifiedCacheE"
	.weak	_ZTIN6icu_6712UnifiedCacheE
	.section	.data.rel.ro._ZTIN6icu_6712UnifiedCacheE,"awG",@progbits,_ZTIN6icu_6712UnifiedCacheE,comdat
	.align 8
	.type	_ZTIN6icu_6712UnifiedCacheE, @object
	.size	_ZTIN6icu_6712UnifiedCacheE, 24
_ZTIN6icu_6712UnifiedCacheE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712UnifiedCacheE
	.quad	_ZTIN6icu_6716UnifiedCacheBaseE
	.weak	_ZTVN6icu_6712CacheKeyBaseE
	.section	.data.rel.ro._ZTVN6icu_6712CacheKeyBaseE,"awG",@progbits,_ZTVN6icu_6712CacheKeyBaseE,comdat
	.align 8
	.type	_ZTVN6icu_6712CacheKeyBaseE, @object
	.size	_ZTVN6icu_6712CacheKeyBaseE, 80
_ZTVN6icu_6712CacheKeyBaseE:
	.quad	0
	.quad	_ZTIN6icu_6712CacheKeyBaseE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6712UnifiedCacheE
	.section	.data.rel.ro._ZTVN6icu_6712UnifiedCacheE,"awG",@progbits,_ZTVN6icu_6712UnifiedCacheE,comdat
	.align 8
	.type	_ZTVN6icu_6712UnifiedCacheE, @object
	.size	_ZTVN6icu_6712UnifiedCacheE, 48
_ZTVN6icu_6712UnifiedCacheE:
	.quad	0
	.quad	_ZTIN6icu_6712UnifiedCacheE
	.quad	_ZN6icu_6712UnifiedCacheD1Ev
	.quad	_ZN6icu_6712UnifiedCacheD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6712UnifiedCache24handleUnreferencedObjectEv
	.local	_ZZZN6icu_67L9cacheInitER10UErrorCodeENKUlvE0_clEvE7storage
	.comm	_ZZZN6icu_67L9cacheInitER10UErrorCodeENKUlvE0_clEvE7storage,48,8
	.local	_ZZZN6icu_67L9cacheInitER10UErrorCodeENKUlvE_clEvE7storage
	.comm	_ZZZN6icu_67L9cacheInitER10UErrorCodeENKUlvE_clEvE7storage,40,8
	.local	_ZL14gCacheInitOnce
	.comm	_ZL14gCacheInitOnce,8,8
	.local	_ZL25gInProgressValueAddedCond
	.comm	_ZL25gInProgressValueAddedCond,8,8
	.local	_ZL11gCacheMutex
	.comm	_ZL11gCacheMutex,8,8
	.local	_ZL6gCache
	.comm	_ZL6gCache,8,8
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.weakref	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t,pthread_mutex_unlock
	.weakref	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t,pthread_mutex_lock
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	-1
	.long	0
	.long	0
	.long	1000
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
