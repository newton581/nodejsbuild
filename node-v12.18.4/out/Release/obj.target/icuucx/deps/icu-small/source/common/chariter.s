	.file	"chariter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharacterIterator12firstPostIncEv
	.type	_ZN6icu_6717CharacterIterator12firstPostIncEv, @function
_ZN6icu_6717CharacterIterator12firstPostIncEv:
.LFB1341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*192(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE1341:
	.size	_ZN6icu_6717CharacterIterator12firstPostIncEv, .-_ZN6icu_6717CharacterIterator12firstPostIncEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharacterIterator14first32PostIncEv
	.type	_ZN6icu_6717CharacterIterator14first32PostIncEv, @function
_ZN6icu_6717CharacterIterator14first32PostIncEv:
.LFB1342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*192(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	48(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE1342:
	.size	_ZN6icu_6717CharacterIterator14first32PostIncEv, .-_ZN6icu_6717CharacterIterator14first32PostIncEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ForwardCharacterIteratorD2Ev
	.type	_ZN6icu_6724ForwardCharacterIteratorD2Ev, @function
_ZN6icu_6724ForwardCharacterIteratorD2Ev:
.LFB1309:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6724ForwardCharacterIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE1309:
	.size	_ZN6icu_6724ForwardCharacterIteratorD2Ev, .-_ZN6icu_6724ForwardCharacterIteratorD2Ev
	.globl	_ZN6icu_6724ForwardCharacterIteratorD1Ev
	.set	_ZN6icu_6724ForwardCharacterIteratorD1Ev,_ZN6icu_6724ForwardCharacterIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ForwardCharacterIteratorD0Ev
	.type	_ZN6icu_6724ForwardCharacterIteratorD0Ev, @function
_ZN6icu_6724ForwardCharacterIteratorD0Ev:
.LFB1311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724ForwardCharacterIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE1311:
	.size	_ZN6icu_6724ForwardCharacterIteratorD0Ev, .-_ZN6icu_6724ForwardCharacterIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ForwardCharacterIteratorC2Ev
	.type	_ZN6icu_6724ForwardCharacterIteratorC2Ev, @function
_ZN6icu_6724ForwardCharacterIteratorC2Ev:
.LFB1313:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6724ForwardCharacterIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE1313:
	.size	_ZN6icu_6724ForwardCharacterIteratorC2Ev, .-_ZN6icu_6724ForwardCharacterIteratorC2Ev
	.globl	_ZN6icu_6724ForwardCharacterIteratorC1Ev
	.set	_ZN6icu_6724ForwardCharacterIteratorC1Ev,_ZN6icu_6724ForwardCharacterIteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724ForwardCharacterIteratorC2ERKS0_
	.type	_ZN6icu_6724ForwardCharacterIteratorC2ERKS0_, @function
_ZN6icu_6724ForwardCharacterIteratorC2ERKS0_:
.LFB1319:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6724ForwardCharacterIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE1319:
	.size	_ZN6icu_6724ForwardCharacterIteratorC2ERKS0_, .-_ZN6icu_6724ForwardCharacterIteratorC2ERKS0_
	.globl	_ZN6icu_6724ForwardCharacterIteratorC1ERKS0_
	.set	_ZN6icu_6724ForwardCharacterIteratorC1ERKS0_,_ZN6icu_6724ForwardCharacterIteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharacterIteratorC2Ev
	.type	_ZN6icu_6717CharacterIteratorC2Ev, @function
_ZN6icu_6717CharacterIteratorC2Ev:
.LFB1322:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharacterIteratorE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE1322:
	.size	_ZN6icu_6717CharacterIteratorC2Ev, .-_ZN6icu_6717CharacterIteratorC2Ev
	.globl	_ZN6icu_6717CharacterIteratorC1Ev
	.set	_ZN6icu_6717CharacterIteratorC1Ev,_ZN6icu_6717CharacterIteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharacterIteratorC2Ei
	.type	_ZN6icu_6717CharacterIteratorC2Ei, @function
_ZN6icu_6717CharacterIteratorC2Ei:
.LFB1325:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharacterIteratorE(%rip), %rax
	movl	%esi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 12(%rdi)
	testl	%esi, %esi
	js	.L13
	movl	%esi, 20(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$0, 20(%rdi)
	movl	$0, 8(%rdi)
	ret
	.cfi_endproc
.LFE1325:
	.size	_ZN6icu_6717CharacterIteratorC2Ei, .-_ZN6icu_6717CharacterIteratorC2Ei
	.globl	_ZN6icu_6717CharacterIteratorC1Ei
	.set	_ZN6icu_6717CharacterIteratorC1Ei,_ZN6icu_6717CharacterIteratorC2Ei
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharacterIteratorC2Eii
	.type	_ZN6icu_6717CharacterIteratorC2Eii, @function
_ZN6icu_6717CharacterIteratorC2Eii:
.LFB1328:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717CharacterIteratorE(%rip), %rax
	movl	%esi, 8(%rdi)
	movq	%rax, (%rdi)
	movl	%edx, 12(%rdi)
	movl	$0, 16(%rdi)
	testl	%esi, %esi
	js	.L16
	movl	%esi, 20(%rdi)
.L17:
	testl	%edx, %edx
	js	.L20
	cmpl	%esi, %edx
	jle	.L15
	movl	%esi, 12(%rdi)
.L15:
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$0, 12(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$0, 20(%rdi)
	xorl	%esi, %esi
	movl	$0, 8(%rdi)
	jmp	.L17
	.cfi_endproc
.LFE1328:
	.size	_ZN6icu_6717CharacterIteratorC2Eii, .-_ZN6icu_6717CharacterIteratorC2Eii
	.globl	_ZN6icu_6717CharacterIteratorC1Eii
	.set	_ZN6icu_6717CharacterIteratorC1Eii,_ZN6icu_6717CharacterIteratorC2Eii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharacterIteratorC2Eiiii
	.type	_ZN6icu_6717CharacterIteratorC2Eiiii, @function
_ZN6icu_6717CharacterIteratorC2Eiiii:
.LFB1331:
	.cfi_startproc
	endbr64
	movd	%edx, %xmm1
	movd	%ecx, %xmm2
	movd	%esi, %xmm0
	movd	%r8d, %xmm3
	punpckldq	%xmm2, %xmm1
	leaq	16+_ZTVN6icu_6717CharacterIteratorE(%rip), %rax
	punpckldq	%xmm3, %xmm0
	movq	%rax, (%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rdi)
	testl	%esi, %esi
	jns	.L22
	movl	$0, 8(%rdi)
	xorl	%esi, %esi
.L22:
	testl	%edx, %edx
	js	.L29
	cmpl	%esi, %edx
	jg	.L30
.L24:
	cmpl	%edx, %ecx
	jge	.L25
	movl	%edx, 20(%rdi)
	movl	%edx, %ecx
.L26:
	cmpl	%edx, %r8d
	jge	.L27
.L31:
	movl	%edx, 12(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	%esi, %ecx
	jle	.L26
	movl	%esi, 20(%rdi)
	movl	%esi, %ecx
	cmpl	%edx, %r8d
	jl	.L31
.L27:
	cmpl	%ecx, %r8d
	jle	.L21
	movl	%ecx, 12(%rdi)
.L21:
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movl	%esi, 16(%rdi)
	movl	%esi, %edx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$0, 16(%rdi)
	xorl	%edx, %edx
	jmp	.L24
	.cfi_endproc
.LFE1331:
	.size	_ZN6icu_6717CharacterIteratorC2Eiiii, .-_ZN6icu_6717CharacterIteratorC2Eiiii
	.globl	_ZN6icu_6717CharacterIteratorC1Eiiii
	.set	_ZN6icu_6717CharacterIteratorC1Eiiii,_ZN6icu_6717CharacterIteratorC2Eiiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharacterIteratorD2Ev
	.type	_ZN6icu_6717CharacterIteratorD2Ev, @function
_ZN6icu_6717CharacterIteratorD2Ev:
.LFB1334:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6724ForwardCharacterIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE1334:
	.size	_ZN6icu_6717CharacterIteratorD2Ev, .-_ZN6icu_6717CharacterIteratorD2Ev
	.globl	_ZN6icu_6717CharacterIteratorD1Ev
	.set	_ZN6icu_6717CharacterIteratorD1Ev,_ZN6icu_6717CharacterIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharacterIteratorD0Ev
	.type	_ZN6icu_6717CharacterIteratorD0Ev, @function
_ZN6icu_6717CharacterIteratorD0Ev:
.LFB1336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724ForwardCharacterIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE1336:
	.size	_ZN6icu_6717CharacterIteratorD0Ev, .-_ZN6icu_6717CharacterIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharacterIteratorC2ERKS0_
	.type	_ZN6icu_6717CharacterIteratorC2ERKS0_, @function
_ZN6icu_6717CharacterIteratorC2ERKS0_:
.LFB1338:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	leaq	16+_ZTVN6icu_6717CharacterIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE1338:
	.size	_ZN6icu_6717CharacterIteratorC2ERKS0_, .-_ZN6icu_6717CharacterIteratorC2ERKS0_
	.globl	_ZN6icu_6717CharacterIteratorC1ERKS0_
	.set	_ZN6icu_6717CharacterIteratorC1ERKS0_,_ZN6icu_6717CharacterIteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CharacterIteratoraSERKS0_
	.type	_ZN6icu_6717CharacterIteratoraSERKS0_, @function
_ZN6icu_6717CharacterIteratoraSERKS0_:
.LFB1340:
	.cfi_startproc
	endbr64
	movdqu	8(%rsi), %xmm0
	movq	%rdi, %rax
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE1340:
	.size	_ZN6icu_6717CharacterIteratoraSERKS0_, .-_ZN6icu_6717CharacterIteratoraSERKS0_
	.weak	_ZTSN6icu_6724ForwardCharacterIteratorE
	.section	.rodata._ZTSN6icu_6724ForwardCharacterIteratorE,"aG",@progbits,_ZTSN6icu_6724ForwardCharacterIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6724ForwardCharacterIteratorE, @object
	.size	_ZTSN6icu_6724ForwardCharacterIteratorE, 36
_ZTSN6icu_6724ForwardCharacterIteratorE:
	.string	"N6icu_6724ForwardCharacterIteratorE"
	.weak	_ZTIN6icu_6724ForwardCharacterIteratorE
	.section	.data.rel.ro._ZTIN6icu_6724ForwardCharacterIteratorE,"awG",@progbits,_ZTIN6icu_6724ForwardCharacterIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6724ForwardCharacterIteratorE, @object
	.size	_ZTIN6icu_6724ForwardCharacterIteratorE, 24
_ZTIN6icu_6724ForwardCharacterIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724ForwardCharacterIteratorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6717CharacterIteratorE
	.section	.rodata._ZTSN6icu_6717CharacterIteratorE,"aG",@progbits,_ZTSN6icu_6717CharacterIteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6717CharacterIteratorE, @object
	.size	_ZTSN6icu_6717CharacterIteratorE, 29
_ZTSN6icu_6717CharacterIteratorE:
	.string	"N6icu_6717CharacterIteratorE"
	.weak	_ZTIN6icu_6717CharacterIteratorE
	.section	.data.rel.ro._ZTIN6icu_6717CharacterIteratorE,"awG",@progbits,_ZTIN6icu_6717CharacterIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6717CharacterIteratorE, @object
	.size	_ZTIN6icu_6717CharacterIteratorE, 24
_ZTIN6icu_6717CharacterIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717CharacterIteratorE
	.quad	_ZTIN6icu_6724ForwardCharacterIteratorE
	.weak	_ZTVN6icu_6724ForwardCharacterIteratorE
	.section	.data.rel.ro._ZTVN6icu_6724ForwardCharacterIteratorE,"awG",@progbits,_ZTVN6icu_6724ForwardCharacterIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6724ForwardCharacterIteratorE, @object
	.size	_ZTVN6icu_6724ForwardCharacterIteratorE, 80
_ZTVN6icu_6724ForwardCharacterIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6724ForwardCharacterIteratorE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6717CharacterIteratorE
	.section	.data.rel.ro._ZTVN6icu_6717CharacterIteratorE,"awG",@progbits,_ZTVN6icu_6717CharacterIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6717CharacterIteratorE, @object
	.size	_ZTVN6icu_6717CharacterIteratorE, 232
_ZTVN6icu_6717CharacterIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6717CharacterIteratorE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_6717CharacterIterator12firstPostIncEv
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_6717CharacterIterator14first32PostIncEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
