	.file	"ustrfmt.cpp"
	.text
	.p2align 4
	.globl	uprv_itou_67
	.type	uprv_itou_67, @function
uprv_itou_67:
.LFB2053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r10d
	movl	$1, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rdx, %r9
	movl	%eax, %r10d
.L4:
	xorl	%edx, %edx
	movl	%r10d, %eax
	divl	%ecx
	leal	48(%rdx), %r12d
	leal	55(%rdx), %r11d
	cmpl	$9, %edx
	cmovle	%r12d, %r11d
	leaq	1(%r9), %rdx
	movl	%r9d, %r12d
	movw	%r11w, -2(%rdi,%r9,2)
	cmpl	%r9d, %esi
	jle	.L18
	cmpl	%r10d, %ecx
	jbe	.L17
.L18:
	cmpl	%r12d, %r8d
	jle	.L6
	leal	-1(%r8), %eax
	movl	%r8d, %ecx
	subl	%r9d, %eax
	subl	%r9d, %ecx
	cmpl	$6, %eax
	jbe	.L7
	movl	%ecx, %edx
	movslq	%r12d, %rax
	movdqa	.LC0(%rip), %xmm0
	shrl	$3, %edx
	leaq	(%rdi,%rax,2), %rax
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L9:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L9
	movl	%ecx, %eax
	andl	$-8, %eax
	addl	%eax, %r12d
	cmpl	%eax, %ecx
	je	.L12
.L7:
	movslq	%r12d, %rdx
	movl	$48, %r10d
	leal	1(%r12), %eax
	movw	%r10w, (%rdi,%rdx,2)
	cmpl	%eax, %r8d
	jle	.L12
	cltq
	movl	$48, %r9d
	leal	2(%r12), %edx
	movw	%r9w, (%rdi,%rax,2)
	cmpl	%r8d, %edx
	jge	.L12
	movslq	%edx, %rdx
	movl	$48, %ecx
	leal	3(%r12), %eax
	movw	%cx, (%rdi,%rdx,2)
	cmpl	%r8d, %eax
	jge	.L12
	cltq
	movl	$48, %r11d
	leal	4(%r12), %edx
	movw	%r11w, (%rdi,%rax,2)
	cmpl	%r8d, %edx
	jge	.L12
	movslq	%edx, %rdx
	movl	$48, %r10d
	leal	5(%r12), %eax
	movw	%r10w, (%rdi,%rdx,2)
	cmpl	%r8d, %eax
	jge	.L12
	cltq
	movl	$48, %r9d
	addl	$6, %r12d
	movw	%r9w, (%rdi,%rax,2)
	cmpl	%r12d, %r8d
	jle	.L12
	movslq	%r12d, %r12
	movl	$48, %ecx
	movw	%cx, (%rdi,%r12,2)
.L12:
	cmpl	%r8d, %esi
	jle	.L22
.L10:
	movslq	%r8d, %rax
	xorl	%edx, %edx
	movl	%r8d, %r12d
	movw	%dx, (%rdi,%rax,2)
.L13:
	movl	%r12d, %r9d
	sarl	%r9d
	cmpl	$1, %r12d
	je	.L1
.L14:
	leal	-1(%r12), %eax
	cltq
	leaq	(%rdi,%rax,2), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L16:
	movzwl	(%rdx), %ecx
	movzwl	(%rdi,%rax,2), %esi
	subq	$2, %rdx
	movw	%si, 2(%rdx)
	movw	%cx, (%rdi,%rax,2)
	addq	$1, %rax
	cmpl	%eax, %r9d
	jg	.L16
.L1:
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	%r8d, %r9d
	movl	%r8d, %r12d
	shrl	$31, %r9d
	addl	%r8d, %r9d
	sarl	%r9d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	%r12d, %esi
	jle	.L13
	movl	%r12d, %r8d
	jmp	.L10
	.cfi_endproc
.LFE2053:
	.size	uprv_itou_67, .-uprv_itou_67
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.value	48
	.value	48
	.value	48
	.value	48
	.value	48
	.value	48
	.value	48
	.value	48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
