	.file	"ucol_swp.cpp"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"ucol_swap(formatVersion=3): too few bytes (%d after header) for collation data\n"
	.align 8
.LC1:
	.string	"ucol_swap(formatVersion=3): magic 0x%08x or format version %02x.%02x is not a collation binary\n"
	.align 8
.LC2:
	.string	"ucol_swap(formatVersion=3): endianness %d or charset %d does not match the swapper\n"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_118swapFormatVersion3EPK12UDataSwapperPKviPvP10UErrorCode.part.0, @function
_ZN12_GLOBAL__N_118swapFormatVersion3EPK12UDataSwapperPKviPvP10UErrorCode.part.0:
.LFB3494:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	-224(%rbp), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movl	$21, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	rep stosq
	cmpl	$-1, %r14d
	je	.L54
	cmpl	$167, %r14d
	jle	.L6
	movl	(%rsi), %esi
	movq	%r12, %rdi
	call	udata_readInt32_67@PLT
	movl	%eax, -224(%rbp)
	cmpl	%eax, %r14d
	jl	.L6
.L3:
	movl	16(%r15), %edi
	call	*16(%r12)
	movzbl	80(%r15), %ecx
	movl	%eax, -208(%rbp)
	movl	%eax, %edx
	cmpl	$537069080, %eax
	jne	.L8
	cmpb	$3, %cl
	je	.L9
.L8:
	movzbl	81(%r15), %r8d
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	udata_printError_67@PLT
	movl	$16, (%rbx)
	xorl	%eax, %eax
.L1:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L55
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	xorl	%eax, %eax
	movl	%r14d, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	udata_printError_67@PLT
	movl	$8, (%rbx)
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L54:
	movl	(%rsi), %esi
	movq	%r12, %rdi
	call	udata_readInt32_67@PLT
	movl	%eax, -224(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	movzwl	(%r12), %eax
	cmpw	%ax, 65(%r15)
	jne	.L56
	cmpl	$-1, %r14d
	je	.L12
	cmpq	%r13, %r15
	je	.L13
	movslq	-224(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	memcpy@PLT
.L13:
	movl	4(%r15), %edi
	call	*16(%r12)
	movl	8(%r15), %edi
	movl	%eax, -220(%rbp)
	movl	%eax, -264(%rbp)
	call	*16(%r12)
	movl	12(%r15), %edi
	movl	%eax, -240(%rbp)
	movl	%eax, -216(%rbp)
	call	*16(%r12)
	movl	20(%r15), %edi
	movl	%eax, -268(%rbp)
	movl	%eax, -212(%rbp)
	call	*16(%r12)
	movl	24(%r15), %edi
	movl	%eax, -204(%rbp)
	movl	%eax, %r14d
	call	*16(%r12)
	movl	28(%r15), %edi
	movl	%eax, -200(%rbp)
	movl	%eax, -260(%rbp)
	call	*16(%r12)
	movl	32(%r15), %edi
	movl	%eax, -276(%rbp)
	movl	%eax, -196(%rbp)
	call	*16(%r12)
	movl	36(%r15), %edi
	movl	%eax, -280(%rbp)
	movl	%eax, -192(%rbp)
	call	*16(%r12)
	movl	40(%r15), %edi
	movl	%eax, -232(%rbp)
	movl	%eax, -188(%rbp)
	call	*16(%r12)
	movl	44(%r15), %edi
	movl	%eax, -272(%rbp)
	movl	%eax, -184(%rbp)
	call	*16(%r12)
	movl	48(%r15), %esi
	movq	%r12, %rdi
	movl	%eax, -180(%rbp)
	call	udata_readInt32_67@PLT
	movl	60(%r15), %esi
	movq	%r12, %rdi
	movl	%eax, -244(%rbp)
	movl	%eax, -176(%rbp)
	call	udata_readInt32_67@PLT
	movl	84(%r15), %edi
	movl	%eax, -248(%rbp)
	movl	%eax, -164(%rbp)
	call	*16(%r12)
	movl	88(%r15), %edi
	movl	%eax, -252(%rbp)
	movl	%eax, -140(%rbp)
	call	*16(%r12)
	movq	%rbx, %r8
	movq	%r13, %rcx
	movl	$64, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, -256(%rbp)
	movl	%eax, -136(%rbp)
	call	*56(%r12)
	leaq	84(%r13), %rcx
	leaq	84(%r15), %rsi
	movq	%rbx, %r8
	movl	$8, %edx
	movq	%r12, %rdi
	call	*56(%r12)
	movzbl	2(%r12), %eax
	movl	-264(%rbp), %r11d
	movl	-260(%rbp), %r10d
	movb	%al, 65(%r13)
	movzbl	3(%r12), %eax
	testl	%r11d, %r11d
	movb	%al, 66(%r13)
	jne	.L57
.L14:
	testl	%r10d, %r10d
	je	.L15
	testl	%r14d, %r14d
	jne	.L58
.L15:
	movl	-232(%rbp), %eax
	testl	%eax, %eax
	jne	.L18
.L20:
	testl	%r14d, %r14d
	jne	.L21
.L19:
	movl	-244(%rbp), %eax
	testl	%eax, %eax
	je	.L22
	movl	-272(%rbp), %esi
	sall	$2, %eax
	movq	%rbx, %r8
	movq	%r12, %rdi
	movl	%eax, %edx
	leaq	0(%r13,%rsi), %rcx
	addq	%r15, %rsi
	call	*56(%r12)
.L22:
	movl	-240(%rbp), %eax
	testl	%eax, %eax
	je	.L23
	movl	-268(%rbp), %edx
	movl	%eax, %esi
	movq	%rbx, %r8
	movq	%r12, %rdi
	leaq	0(%r13,%rsi), %rcx
	addq	%r15, %rsi
	subl	%eax, %edx
	call	*56(%r12)
.L23:
	movl	-248(%rbp), %eax
	testl	%eax, %eax
	je	.L24
	movzbl	67(%r15), %edx
	movl	-268(%rbp), %esi
	movq	%rbx, %r8
	movq	%r12, %rdi
	imull	%edx, %eax
	leaq	0(%r13,%rsi), %rcx
	addq	%r15, %rsi
	leal	(%rax,%rax), %edx
	call	*48(%r12)
.L24:
	movl	-252(%rbp), %eax
	testl	%eax, %eax
	je	.L25
	movl	%eax, %r14d
	leaq	(%r15,%r14), %rsi
	movzwl	(%rsi), %edi
	movq	%rsi, -240(%rbp)
	call	*8(%r12)
	movzwl	2(%r15,%r14), %edi
	movw	%ax, -232(%rbp)
	call	*8(%r12)
	leaq	0(%r13,%r14), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdi
	movzwl	-232(%rbp), %edx
	movzwl	%ax, %eax
	movq	-240(%rbp), %rsi
	leal	2(%rax,%rdx,2), %edx
	addl	%edx, %edx
	call	*48(%r12)
.L25:
	movl	-256(%rbp), %eax
	testl	%eax, %eax
	je	.L12
	leaq	(%r15,%rax), %rsi
	movq	%rax, -232(%rbp)
	movzwl	(%rsi), %edi
	movq	%rsi, -240(%rbp)
	call	*8(%r12)
	movzwl	%ax, %r14d
	movq	-232(%rbp), %rax
	movzwl	2(%r15,%rax), %edi
	call	*8(%r12)
	movq	-232(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r12, %rdi
	movzwl	%ax, %eax
	leal	2(%r14,%rax), %edx
	leaq	0(%r13,%rsi), %rcx
	movq	-240(%rbp), %rsi
	addl	%edx, %edx
	call	*48(%r12)
.L12:
	movl	-224(%rbp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L56:
	movzbl	66(%r15), %ecx
	movsbl	65(%r15), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	udata_printError_67@PLT
	movl	$3, (%rbx)
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%r11d, %esi
	movl	%r10d, %edx
	movq	%rbx, %r8
	movq	%r12, %rdi
	leaq	0(%r13,%rsi), %rcx
	subl	%r11d, %edx
	addq	%r15, %rsi
	call	*56(%r12)
	movl	-260(%rbp), %r10d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L18:
	movl	-276(%rbp), %eax
	movl	-232(%rbp), %esi
	movq	%rbx, %r8
	movq	%r12, %rdi
	leaq	0(%r13,%rax), %rcx
	leal	(%rsi,%rsi), %edx
	leaq	(%r15,%rax), %rsi
	call	*48(%r12)
	movl	-280(%rbp), %esi
	movq	%rbx, %r8
	movq	%r12, %rdi
	movl	-232(%rbp), %eax
	leaq	0(%r13,%rsi), %rcx
	addq	%r15, %rsi
	leal	0(,%rax,4), %edx
	call	*56(%r12)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L58:
	movl	-276(%rbp), %esi
	movl	%r14d, %edx
	movq	%rbx, %r8
	movq	%r12, %rdi
	subl	%r10d, %edx
	leaq	0(%r13,%r10), %rcx
	movl	%esi, %eax
	subl	%r10d, %eax
	testl	%esi, %esi
	leaq	(%r15,%r10), %rsi
	cmovne	%eax, %edx
	call	*56(%r12)
	movl	-232(%rbp), %edx
	testl	%edx, %edx
	jne	.L18
.L21:
	movl	-272(%rbp), %edx
	movl	%r14d, %esi
	movq	%rbx, %r8
	movq	%r12, %rdi
	leaq	0(%r13,%rsi), %rcx
	addq	%r15, %rsi
	subl	%r14d, %edx
	call	utrie_swap_67@PLT
	jmp	.L19
.L55:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3494:
	.size	_ZN12_GLOBAL__N_118swapFormatVersion3EPK12UDataSwapperPKviPvP10UErrorCode.part.0, .-_ZN12_GLOBAL__N_118swapFormatVersion3EPK12UDataSwapperPKviPvP10UErrorCode.part.0
	.p2align 4
	.globl	ucol_looksLikeCollationBinary_67
	.type	ucol_looksLikeCollationBinary_67, @function
ucol_looksLikeCollationBinary_67:
.LFB2760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	sete	%dl
	cmpl	$-1, %r12d
	setl	%al
	orb	%al, %dl
	jne	.L66
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L66
	leaq	-212(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%rsi, %rbx
	movl	$-1, %edx
	movl	$0, -212(%rbp)
	call	udata_swapDataHeader_67@PLT
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	jg	.L63
	cmpw	$17237, 12(%rbx)
	je	.L70
.L63:
	leaq	-208(%rbp), %rdx
	movl	$21, %ecx
	xorl	%eax, %eax
	movq	%rdx, %rdi
	rep stosq
	cmpl	$-1, %r12d
	je	.L71
	cmpl	$167, %r12d
	jg	.L72
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%eax, %eax
.L59:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L73
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movl	(%rbx), %esi
	movq	%r13, %rdi
	call	udata_readInt32_67@PLT
	movl	%eax, -208(%rbp)
	cmpl	%r12d, %eax
	jg	.L66
.L65:
	movl	16(%rbx), %edi
	call	*16(%r13)
	cmpl	$537069080, %eax
	jne	.L66
	cmpb	$3, 80(%rbx)
	jne	.L66
	movzwl	0(%r13), %eax
	cmpw	%ax, 65(%rbx)
	sete	%al
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L70:
	cmpw	$27759, 14(%rbx)
	movl	$1, %eax
	jne	.L63
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L71:
	movl	(%rbx), %esi
	movq	%r13, %rdi
	call	udata_readInt32_67@PLT
	jmp	.L65
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2760:
	.size	ucol_looksLikeCollationBinary_67, .-ucol_looksLikeCollationBinary_67
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"ucol_swap(): data format %02x.%02x.%02x.%02x (format version %02x.%02x) is not recognized as collation data\n"
	.align 8
.LC4:
	.string	"ucol_swap(formatVersion=4): too few bytes (%d after header) for collation data\n"
	.align 8
.LC5:
	.string	"ucol_swap(formatVersion=4): unknown data at IX_RESERVED8_OFFSET\n"
	.align 8
.LC6:
	.string	"ucol_swap(formatVersion=4): unknown data at IX_RESERVED10_OFFSET\n"
	.align 8
.LC7:
	.string	"ucol_swap(formatVersion=4): unknown data at IX_RESERVED18_OFFSET\n"
	.text
	.p2align 4
	.globl	ucol_swap_67
	.type	ucol_swap_67, @function
ucol_swap_67:
.LFB2763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L75
.L129:
	xorl	%eax, %eax
.L74:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L130
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	%r8, %rbx
	movq	%rcx, %r13
	movq	%rdi, %r14
	movq	%rsi, %r12
	movl	%edx, %r15d
	call	udata_swapDataHeader_67@PLT
	movl	(%rbx), %ecx
	movl	%eax, %r10d
	testl	%ecx, %ecx
	jg	.L131
	cmpw	$17237, 12(%r12)
	movzbl	16(%r12), %eax
	jne	.L83
	cmpw	$27759, 14(%r12)
	jne	.L83
	leal	-3(%rax), %edx
	cmpb	$2, %dl
	ja	.L83
	movslq	%r10d, %rcx
	movl	%r15d, %edx
	subl	%r10d, %edx
	addq	%rcx, %r12
	leaq	0(%r13,%rcx), %rdi
	testl	%r15d, %r15d
	movq	%rdi, -168(%rbp)
	cmovns	%edx, %r15d
	cmpb	$3, %al
	je	.L86
	cmpl	$7, %r15d
	jbe	.L127
	movl	(%r12), %esi
	movq	%r14, %rdi
	movl	%r10d, -148(%rbp)
	call	udata_readInt32_67@PLT
	testl	%r15d, %r15d
	movl	-148(%rbp), %r10d
	movl	%eax, -152(%rbp)
	js	.L92
	sall	$2, %eax
	cmpl	%eax, %r15d
	jl	.L127
.L92:
	cmpl	$1, -152(%rbp)
	jle	.L132
	leaq	-144(%rbp), %rax
	movl	$2, %r13d
	movq	%rax, -160(%rbp)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L133:
	cmpl	%eax, -152(%rbp)
	jle	.L94
.L91:
	movl	-4(%r12,%r13,4), %esi
	movq	%r14, %rdi
	movl	%r10d, -148(%rbp)
	call	udata_readInt32_67@PLT
	movq	-160(%rbp), %rdi
	movl	-148(%rbp), %r10d
	movl	%eax, -4(%rdi,%r13,4)
	movl	%r13d, %eax
	addq	$1, %r13
	cmpl	$19, %eax
	jle	.L133
.L94:
	cmpl	$19, -152(%rbp)
	jg	.L134
.L90:
	movl	-152(%rbp), %r13d
	movl	$19, %eax
	movq	-160(%rbp), %rcx
	movl	$255, %esi
	movl	%r10d, -172(%rbp)
	subl	%r13d, %eax
	leaq	4(,%rax,4), %rdx
	movslq	%r13d, %rax
	leaq	(%rcx,%rax,4), %rdi
	call	memset@PLT
	cmpl	$5, %r13d
	leal	0(,%r13,4), %eax
	movl	-172(%rbp), %r10d
	movl	%eax, -148(%rbp)
	jle	.L96
	leal	-1(%r13), %eax
	cltq
	movl	-144(%rbp,%rax,4), %eax
	movl	%eax, -148(%rbp)
.L96:
	testl	%r15d, %r15d
	js	.L128
	cmpl	-148(%rbp), %r15d
	jl	.L127
	movq	-168(%rbp), %rdi
	cmpq	%rdi, %r12
	je	.L100
	movslq	-148(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r10d, -160(%rbp)
	call	memcpy@PLT
	movl	-160(%rbp), %r10d
.L100:
	movl	-152(%rbp), %edx
	movq	%rbx, %r8
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-168(%rbp), %r15
	movl	%r10d, -160(%rbp)
	sall	$2, %edx
	movq	%r15, %rcx
	call	*56(%r14)
	movslq	-124(%rbp), %rax
	movl	-120(%rbp), %edx
	movl	-160(%rbp), %r10d
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L101
	leaq	(%r15,%rax), %rcx
	leaq	(%r12,%rax), %rsi
	movq	%rbx, %r8
	movq	%r14, %rdi
	movl	%r10d, -152(%rbp)
	call	*56(%r14)
	movl	-152(%rbp), %r10d
.L101:
	movl	-112(%rbp), %r13d
	movslq	-116(%rbp), %rax
	movl	%r13d, %edx
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L102
	movq	-168(%rbp), %rcx
	leaq	(%r12,%rax), %rsi
	movq	%rbx, %r8
	movq	%r14, %rdi
	movl	%r10d, -152(%rbp)
	addq	%rax, %rcx
	call	utrie2_swap_67@PLT
	movl	-152(%rbp), %r10d
.L102:
	movslq	-108(%rbp), %rax
	movl	%eax, %edx
	subl	%r13d, %edx
	testl	%edx, %edx
	jg	.L135
	movl	-104(%rbp), %r13d
	movl	%r13d, %edx
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L104
	movq	-168(%rbp), %rcx
	movl	%r10d, -152(%rbp)
	movq	%rbx, %r8
	movq	%r14, %rdi
	leaq	(%r12,%rax), %rsi
	addq	%rax, %rcx
	call	*64(%r14)
	movl	-152(%rbp), %r10d
.L104:
	movslq	-100(%rbp), %rax
	movl	%eax, %edx
	subl	%r13d, %edx
	testl	%edx, %edx
	jg	.L136
	movl	-96(%rbp), %r13d
	movl	%r13d, %edx
	subl	%eax, %edx
	testl	%edx, %edx
	jle	.L106
	movq	-168(%rbp), %rcx
	movl	%r10d, -152(%rbp)
	movq	%rbx, %r8
	movq	%r14, %rdi
	leaq	(%r12,%rax), %rsi
	addq	%rax, %rcx
	call	*56(%r14)
	movl	-152(%rbp), %r10d
.L106:
	movslq	-92(%rbp), %r9
	movl	%r9d, %edx
	subl	%r13d, %edx
	testl	%edx, %edx
	jle	.L107
	movq	-168(%rbp), %rcx
	movslq	%r13d, %rax
	movq	%rbx, %r8
	movq	%r14, %rdi
	movl	%r9d, -160(%rbp)
	leaq	(%r12,%rax), %rsi
	movl	%r10d, -152(%rbp)
	addq	%rax, %rcx
	call	*56(%r14)
	movslq	-160(%rbp), %r9
	movl	-152(%rbp), %r10d
.L107:
	movl	-88(%rbp), %r13d
	movl	%r13d, %edx
	subl	%r9d, %edx
	testl	%edx, %edx
	jle	.L108
	movq	-168(%rbp), %rax
	movl	%r10d, -152(%rbp)
	movq	%rbx, %r8
	movq	%r14, %rdi
	leaq	(%r12,%r9), %rsi
	leaq	(%rax,%r9), %rcx
	call	*48(%r14)
	movl	-152(%rbp), %r10d
.L108:
	movslq	-84(%rbp), %r9
	movl	%r9d, %edx
	subl	%r13d, %edx
	testl	%edx, %edx
	jle	.L109
	movq	-168(%rbp), %rcx
	movslq	%r13d, %rax
	movq	%rbx, %r8
	movq	%r14, %rdi
	movl	%r9d, -160(%rbp)
	leaq	(%r12,%rax), %rsi
	movl	%r10d, -152(%rbp)
	addq	%rax, %rcx
	call	*48(%r14)
	movslq	-160(%rbp), %r9
	movl	-152(%rbp), %r10d
.L109:
	movl	-80(%rbp), %r13d
	movl	%r13d, %edx
	subl	%r9d, %edx
	testl	%edx, %edx
	jle	.L110
	movq	-168(%rbp), %rax
	movl	%r10d, -152(%rbp)
	movq	%rbx, %r8
	movq	%r14, %rdi
	leaq	(%r12,%r9), %rsi
	leaq	(%rax,%r9), %rcx
	call	*48(%r14)
	movl	-152(%rbp), %r10d
.L110:
	movl	-76(%rbp), %edx
	subl	%r13d, %edx
	testl	%edx, %edx
	jle	.L111
	movq	-168(%rbp), %rcx
	movslq	%r13d, %rax
	movq	%rbx, %r8
	movq	%r14, %rdi
	movl	%r10d, -152(%rbp)
	leaq	(%r12,%rax), %rsi
	addq	%rax, %rcx
	call	*48(%r14)
	movl	-152(%rbp), %r10d
.L111:
	movl	-68(%rbp), %edx
	subl	-72(%rbp), %edx
	testl	%edx, %edx
	jg	.L112
.L128:
	movl	(%rbx), %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L83:
	movzbl	17(%r12), %esi
	movzbl	12(%r12), %edx
	movq	%r14, %rdi
	movzbl	13(%r12), %ecx
	pushq	%rsi
	leaq	.LC3(%rip), %rsi
	pushq	%rax
	movzbl	15(%r12), %r9d
	xorl	%eax, %eax
	movzbl	14(%r12), %r8d
	call	udata_printError_67@PLT
	movl	$16, (%rbx)
	popq	%rax
	popq	%rdx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L131:
	testq	%r14, %r14
	movl	$0, (%rbx)
	sete	%dl
	testq	%r12, %r12
	sete	%al
	orb	%al, %dl
	jne	.L80
	cmpl	$-1, %r15d
	jl	.L80
	testl	%r15d, %r15d
	jle	.L116
	testq	%r13, %r13
	je	.L80
.L116:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	leaq	-40(%rbp), %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	movl	%r15d, %edx
	popq	%rbx
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12_GLOBAL__N_118swapFormatVersion3EPK12UDataSwapperPKviPvP10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%eax, %eax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L86:
	testq	%r14, %r14
	je	.L80
	cmpl	$-1, %r15d
	jl	.L80
	testl	%r15d, %r15d
	jle	.L117
	cmpq	$0, -168(%rbp)
	je	.L80
.L117:
	movq	-168(%rbp), %rcx
	movq	%rbx, %r8
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%r10d, -152(%rbp)
	call	_ZN12_GLOBAL__N_118swapFormatVersion3EPK12UDataSwapperPKviPvP10UErrorCode.part.0
	movl	-152(%rbp), %r10d
	movl	%eax, -148(%rbp)
	movl	(%rbx), %eax
.L113:
	testl	%eax, %eax
	jg	.L129
	movl	-148(%rbp), %eax
	addl	%r10d, %eax
	jmp	.L74
.L112:
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$16, (%rbx)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L127:
	movl	%r15d, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$8, (%rbx)
	jmp	.L129
.L134:
	movl	-68(%rbp), %eax
	movl	%eax, -148(%rbp)
	jmp	.L96
.L135:
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$16, (%rbx)
	jmp	.L129
.L136:
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$16, (%rbx)
	jmp	.L129
.L132:
	leaq	-144(%rbp), %rax
	movq	%rax, -160(%rbp)
	jmp	.L90
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2763:
	.size	ucol_swap_67, .-ucol_swap_67
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"ucol_swapInverseUCA(): data format %02x.%02x.%02x.%02x (format version %02x.%02x) is not an inverse UCA collation file\n"
	.align 8
.LC9:
	.string	"ucol_swapInverseUCA(): too few bytes (%d after header) for inverse UCA collation data\n"
	.text
	.p2align 4
	.globl	ucol_swapInverseUCA_67
	.type	ucol_swapInverseUCA_67, @function
ucol_swapInverseUCA_67:
.LFB2764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	call	udata_swapDataHeader_67@PLT
	testq	%r12, %r12
	je	.L148
	movl	(%r12), %ecx
	movl	%eax, %r9d
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L137
	cmpw	$28233, 12(%rbx)
	movzbl	16(%rbx), %eax
	movzbl	17(%rbx), %edx
	jne	.L139
	cmpw	$17270, 14(%rbx)
	jne	.L139
	cmpb	$2, %al
	jne	.L139
	testb	%dl, %dl
	je	.L139
	movslq	%r9d, %rcx
	addq	%rcx, %rbx
	testl	%r15d, %r15d
	js	.L152
	movl	%r15d, %edx
	subl	%r9d, %edx
	cmpl	$31, %edx
	jle	.L144
	movl	(%rbx), %esi
	movq	%r13, %rdi
	movl	%r9d, -64(%rbp)
	movl	%edx, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	udata_readInt32_67@PLT
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rcx
	movl	-64(%rbp), %r9d
	movl	%eax, %r10d
	cmpl	%eax, %edx
	jb	.L144
	addq	%rcx, %r14
	cmpq	%r14, %rbx
	je	.L147
	movl	%r10d, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%r9d, -60(%rbp)
	movl	%r10d, -56(%rbp)
	call	memcpy@PLT
	movl	-60(%rbp), %r9d
	movl	-56(%rbp), %r10d
.L147:
	movl	%r9d, -72(%rbp)
	movl	4(%rbx), %edi
	movl	%r10d, -68(%rbp)
	call	*16(%r13)
	movl	8(%rbx), %edi
	movl	%eax, -64(%rbp)
	call	*16(%r13)
	movl	12(%rbx), %edi
	movl	%eax, -56(%rbp)
	call	*16(%r13)
	movl	16(%rbx), %edi
	movl	%eax, %r15d
	call	*16(%r13)
	movq	%r12, %r8
	movq	%r14, %rcx
	movl	$20, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	%eax, -60(%rbp)
	call	*56(%r13)
	movl	-64(%rbp), %r11d
	leaq	(%r14,%r15), %rcx
	leaq	(%rbx,%r15), %rsi
	movq	%r12, %r8
	movq	%r13, %rdi
	leal	(%r11,%r11,2), %edx
	sall	$2, %edx
	call	*56(%r13)
	movl	-60(%rbp), %esi
	movl	-56(%rbp), %edx
	movq	%r12, %r8
	movq	%r13, %rdi
	leaq	(%r14,%rsi), %rcx
	addl	%edx, %edx
	addq	%rbx, %rsi
	call	*48(%r13)
	movl	-72(%rbp), %r9d
	movl	-68(%rbp), %r10d
.L142:
	leal	(%r9,%r10), %eax
.L137:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movzbl	12(%rbx), %r10d
	movzbl	13(%rbx), %ecx
	pushq	%rdx
	movq	%r13, %rdi
	pushq	%rax
	movzbl	15(%rbx), %r9d
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movzbl	14(%rbx), %r8d
	movl	%r10d, %edx
	call	udata_printError_67@PLT
	popq	%rax
	movl	$16, (%r12)
	xorl	%eax, %eax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	(%rbx), %esi
	movq	%r13, %rdi
	movl	%r9d, -56(%rbp)
	call	udata_readInt32_67@PLT
	movl	-56(%rbp), %r9d
	movl	%eax, %r10d
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L144:
	xorl	%eax, %eax
	movl	%r15d, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	udata_printError_67@PLT
	movl	$8, (%r12)
	xorl	%eax, %eax
	jmp	.L137
	.cfi_endproc
.LFE2764:
	.size	ucol_swapInverseUCA_67, .-ucol_swapInverseUCA_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
