	.file	"bytesinkutil.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CharStringByteSinkD2Ev
	.type	_ZN6icu_6718CharStringByteSinkD2Ev, @function
_ZN6icu_6718CharStringByteSinkD2Ev:
.LFB2350:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6718CharStringByteSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678ByteSinkD2Ev@PLT
	.cfi_endproc
.LFE2350:
	.size	_ZN6icu_6718CharStringByteSinkD2Ev, .-_ZN6icu_6718CharStringByteSinkD2Ev
	.globl	_ZN6icu_6718CharStringByteSinkD1Ev
	.set	_ZN6icu_6718CharStringByteSinkD1Ev,_ZN6icu_6718CharStringByteSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CharStringByteSinkD0Ev
	.type	_ZN6icu_6718CharStringByteSinkD0Ev, @function
_ZN6icu_6718CharStringByteSinkD0Ev:
.LFB2352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718CharStringByteSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2352:
	.size	_ZN6icu_6718CharStringByteSinkD0Ev, .-_ZN6icu_6718CharStringByteSinkD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CharStringByteSink6AppendEPKci
	.type	_ZN6icu_6718CharStringByteSink6AppendEPKci, @function
_ZN6icu_6718CharStringByteSink6AppendEPKci:
.LFB2353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rcx
	movl	$0, -12(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L8
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2353:
	.size	_ZN6icu_6718CharStringByteSink6AppendEPKci, .-_ZN6icu_6718CharStringByteSink6AppendEPKci
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CharStringByteSink15GetAppendBufferEiiPciPi
	.type	_ZN6icu_6718CharStringByteSink15GetAppendBufferEiiPciPi, @function
_ZN6icu_6718CharStringByteSink15GetAppendBufferEiiPciPi:
.LFB2354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%r9, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jle	.L14
	movl	%r8d, %r12d
	cmpl	%r8d, %esi
	jg	.L14
	movq	8(%rdi), %rdi
	movq	%rcx, %r13
	leaq	-44(%rbp), %r8
	movq	%r9, %rcx
	movl	$0, -44(%rbp)
	call	_ZN6icu_6710CharString15getAppendBufferEiiRiR10UErrorCode@PLT
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jle	.L9
	movl	%r12d, (%rbx)
	movq	%r13, %rax
.L9:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L16
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	$0, (%rbx)
	xorl	%eax, %eax
	jmp	.L9
.L16:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2354:
	.size	_ZN6icu_6718CharStringByteSink15GetAppendBufferEiiPciPi, .-_ZN6icu_6718CharStringByteSink15GetAppendBufferEiiPciPi
	.align 2
	.p2align 4
	.type	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode.part.0, @function
_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode.part.0:
.LFB2848:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, -324(%rbp)
	movq	%r8, -312(%rbp)
	movq	%r9, -320(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L37
	leaq	-264(%rbp), %rax
	movl	%edx, %r15d
	xorl	%ebx, %ebx
	movq	%rsi, %r12
	movq	%rax, -288(%rbp)
	movq	%rcx, %r14
	xorl	%r13d, %r13d
	leaq	-256(%rbp), %rax
	movq	%rax, -296(%rbp)
	leaq	-260(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	%r15d, %eax
	subl	%ebx, %eax
	cmpl	$715827881, %eax
	jg	.L19
	.p2align 4,,10
	.p2align 3
.L49:
	leal	(%rax,%rax,2), %edx
.L20:
	movq	(%r14), %rax
	movl	$200, %r8d
	movl	$4, %esi
	movq	-288(%rbp), %r9
	movq	-296(%rbp), %rcx
	movq	%r14, %rdi
	call	*24(%rax)
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	movl	-264(%rbp), %eax
	subl	$3, %eax
	movl	%eax, -264(%rbp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L46:
	movb	%al, (%rdx)
	movl	%ecx, %r8d
	cmpl	%r9d, %r15d
	jle	.L22
.L47:
	movl	-264(%rbp), %eax
	movl	%r9d, %ebx
.L21:
	cmpl	%eax, %r8d
	jge	.L39
	movslq	%ebx, %rax
	leal	1(%rbx), %r9d
	leaq	(%rax,%rax), %rcx
	movzwl	(%r12,%rax,2), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L23
	movzwl	2(%r12,%rcx), %edx
	sall	$10, %eax
	leal	2(%rbx), %r9d
	leal	-56613888(%rdx,%rax), %eax
.L23:
	movslq	%r8d, %rdx
	leal	1(%r8), %ecx
	addq	%rsi, %rdx
	cmpl	$127, %eax
	jbe	.L46
	movl	%eax, %r10d
	movslq	%ecx, %rdi
	shrl	$6, %r10d
	addq	%rsi, %rdi
	cmpl	$2047, %eax
	ja	.L26
	orl	$-64, %r10d
	movb	%r10b, (%rdx)
.L27:
	andl	$63, %eax
	leal	1(%rcx), %r8d
	orl	$-128, %eax
	movb	%al, (%rdi)
	cmpl	%r9d, %r15d
	jg	.L47
.L22:
	movl	$2147483647, %eax
	subl	%r13d, %eax
	cmpl	%r8d, %eax
	jl	.L48
.L30:
	movq	(%r14), %rax
	leaq	_ZN6icu_6718CharStringByteSink6AppendEPKci(%rip), %rdi
	movl	%r9d, -280(%rbp)
	movq	16(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L32
	movq	8(%r14), %rdi
	movl	%r8d, %edx
	movq	-304(%rbp), %rcx
	movl	$0, -260(%rbp)
	movl	%r8d, -276(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L45:
	movl	-276(%rbp), %r8d
	movl	-280(%rbp), %r9d
	addl	%r8d, %r13d
	cmpl	%r9d, %r15d
	jle	.L18
	movl	%r9d, %ebx
	movl	%r15d, %eax
	subl	%ebx, %eax
	cmpl	$715827881, %eax
	jle	.L49
.L19:
	leal	(%rax,%rax), %edx
	cmpl	$1073741823, %eax
	movl	$2147483647, %eax
	cmovge	%eax, %edx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L26:
	movl	%eax, %r11d
	shrl	$12, %r11d
	cmpl	$65535, %eax
	ja	.L28
	orl	$-32, %r11d
	movb	%r11b, (%rdx)
.L29:
	andl	$63, %r10d
	addl	$1, %ecx
	orl	$-128, %r10d
	movb	%r10b, (%rdi)
	movslq	%ecx, %rdi
	addq	%rsi, %rdi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	movl	%eax, %ecx
	andl	$63, %r11d
	shrl	$18, %ecx
	orl	$-128, %r11d
	orl	$-16, %ecx
	movb	%cl, (%rdx)
	leal	2(%r8), %ecx
	movb	%r11b, (%rdi)
	movslq	%ecx, %rdi
	addq	%rsi, %rdi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$2147483647, %eax
	movl	%ebx, %r9d
	subl	%r13d, %eax
	cmpl	%r8d, %eax
	jge	.L30
.L48:
	movq	-320(%rbp), %rax
	movl	$8, (%rax)
	xorl	%eax, %eax
.L17:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L50
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	%r8d, -276(%rbp)
	movl	%r8d, %edx
	movq	%r14, %rdi
	call	*%rax
	jmp	.L45
.L37:
	xorl	%r13d, %r13d
.L18:
	movq	-312(%rbp), %rdi
	movl	$1, %eax
	testq	%rdi, %rdi
	je	.L17
	movl	-324(%rbp), %esi
	movl	%r13d, %edx
	movb	%al, -276(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	movzbl	-276(%rbp), %eax
	jmp	.L17
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2848:
	.size	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode.part.0, .-_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode.part.0
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2591:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2591:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2594:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L64
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L52
	cmpb	$0, 12(%rbx)
	jne	.L65
.L56:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L52:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L56
	.cfi_endproc
.LFE2594:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2597:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L68
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2597:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2600:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L71
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2600:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L77
.L73:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L78
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2602:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2603:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2603:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2604:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2604:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2605:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2605:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2606:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2606:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2607:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2607:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2608:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L94
	testl	%edx, %edx
	jle	.L94
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L97
.L86:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L86
	.cfi_endproc
.LFE2608:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L101
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L101
	testl	%r12d, %r12d
	jg	.L108
	cmpb	$0, 12(%rbx)
	jne	.L109
.L103:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L103
.L109:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L101:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2609:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L111
	movq	(%rdi), %r8
.L112:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L115
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L115
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L115:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2610:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2611:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L122
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2611:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2612:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2612:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2613:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2613:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2614:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2614:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2616:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2616:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2618:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2618:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB2338:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L129
	jmp	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L129:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2338:
	.size	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ByteSinkUtil12appendChangeEPKhS2_PKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZN6icu_6712ByteSinkUtil12appendChangeEPKhS2_PKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZN6icu_6712ByteSinkUtil12appendChangeEPKhS2_PKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB2339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movl	%ecx, %edx
	movq	%r8, %rcx
	movq	%r9, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %r9
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L130
	movq	%rdi, %rax
	movq	%rsi, %rdi
	subq	%rax, %rdi
	cmpq	$2147483647, %rdi
	jle	.L132
	movl	$8, (%r9)
.L130:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movq	%r10, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6712ByteSinkUtil12appendChangeEiPKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode.part.0
	.cfi_endproc
.LFE2339:
	.size	_ZN6icu_6712ByteSinkUtil12appendChangeEPKhS2_PKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZN6icu_6712ByteSinkUtil12appendChangeEPKhS2_PKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE
	.type	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE, @function
_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE:
.LFB2340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rcx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$127, %esi
	ja	.L136
	movb	%sil, -28(%rbp)
	movl	$1, %r13d
.L137:
	testq	%rdi, %rdi
	je	.L142
	movl	%r13d, %edx
	movl	%r8d, %esi
	call	_ZN6icu_675Edits10addReplaceEii@PLT
.L142:
	movq	(%r12), %rax
	leaq	_ZN6icu_6718CharStringByteSink6AppendEPKci(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L143
	movq	8(%r12), %rdi
	leaq	-32(%rbp), %rcx
	leaq	-28(%rbp), %rsi
	movl	%r13d, %edx
	movl	$0, -32(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L135:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movl	%esi, %eax
	shrl	$6, %eax
	cmpl	$2047, %esi
	ja	.L138
	orl	$-64, %eax
	movl	$2, %r13d
	movl	$1, %edx
	movb	%al, -28(%rbp)
.L139:
	andl	$63, %esi
	movslq	%edx, %rax
	orl	$-128, %esi
	movb	%sil, -28(%rbp,%rax)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L138:
	movl	%esi, %edx
	shrl	$12, %edx
	cmpl	$65535, %esi
	jbe	.L151
	movl	%esi, %r9d
	andl	$63, %edx
	movl	$4, %r13d
	movl	$2, %ecx
	orl	$-128, %edx
	shrl	$18, %r9d
	movb	%dl, -27(%rbp)
	orl	$-16, %r9d
	movl	$3, %edx
.L141:
	andl	$63, %eax
	movb	%r9b, -28(%rbp)
	orl	$-128, %eax
	movb	%al, -28(%rbp,%rcx)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	-28(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	*%rax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L151:
	movl	%edx, %r9d
	movl	$3, %r13d
	movl	$2, %edx
	movl	$1, %ecx
	orl	$-32, %r9d
	jmp	.L141
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2340:
	.size	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE, .-_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ByteSinkUtil14appendTwoBytesEiRNS_8ByteSinkE
	.type	_ZN6icu_6712ByteSinkUtil14appendTwoBytesEiRNS_8ByteSinkE, @function
_ZN6icu_6712ByteSinkUtil14appendTwoBytesEiRNS_8ByteSinkE:
.LFB2343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_6718CharStringByteSink6AppendEPKci(%rip), %rdx
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%edi, %eax
	andl	$63, %edi
	sarl	$6, %eax
	orl	$-128, %edi
	orl	$-64, %eax
	movb	%dil, -9(%rbp)
	movb	%al, -10(%rbp)
	movq	(%rsi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L153
	movq	8(%r8), %rdi
	leaq	-16(%rbp), %rcx
	leaq	-10(%rbp), %rsi
	movl	$2, %edx
	movl	$0, -16(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L152:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	leaq	-10(%rbp), %rsi
	movl	$2, %edx
	movq	%r8, %rdi
	call	*%rax
	jmp	.L152
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2343:
	.size	_ZN6icu_6712ByteSinkUtil14appendTwoBytesEiRNS_8ByteSinkE, .-_ZN6icu_6712ByteSinkUtil14appendTwoBytesEiRNS_8ByteSinkE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE
	.type	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE, @function
_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE:
.LFB2344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%ecx, %ebx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L159
	movq	%r8, %rdi
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
.L159:
	andb	$64, %bh
	jne	.L158
	movq	0(%r13), %rax
	leaq	_ZN6icu_6718CharStringByteSink6AppendEPKci(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L161
	movq	8(%r13), %rdi
	leaq	-44(%rbp), %rcx
	movl	%r12d, %edx
	movq	%r14, %rsi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L158:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L158
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2344:
	.size	_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE, .-_ZN6icu_6712ByteSinkUtil23appendNonEmptyUnchangedEPKhiRNS_8ByteSinkEjPNS_5EditsE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode
	.type	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode, @function
_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode:
.LFB2345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L168
	movq	%rsi, %rbx
	movq	%rdi, %r12
	subq	%rdi, %rbx
	cmpq	$2147483647, %rbx
	jg	.L182
	testl	%ebx, %ebx
	jg	.L171
.L181:
	movl	$1, %eax
.L168:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L183
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movl	$8, (%r9)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%rdx, %r13
	testq	%r8, %r8
	je	.L172
	movl	%ebx, %esi
	movq	%r8, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movl	-52(%rbp), %ecx
.L172:
	andb	$64, %ch
	jne	.L181
	movq	0(%r13), %rax
	leaq	_ZN6icu_6718CharStringByteSink6AppendEPKci(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L174
	movq	8(%r13), %rdi
	leaq	-44(%rbp), %rcx
	movl	%ebx, %edx
	movq	%r12, %rsi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	$1, %eax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L174:
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L181
.L183:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2345:
	.size	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode, .-_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718CharStringByteSinkC2EPNS_10CharStringE
	.type	_ZN6icu_6718CharStringByteSinkC2EPNS_10CharStringE, @function
_ZN6icu_6718CharStringByteSinkC2EPNS_10CharStringE:
.LFB2347:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6718CharStringByteSinkE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE2347:
	.size	_ZN6icu_6718CharStringByteSinkC2EPNS_10CharStringE, .-_ZN6icu_6718CharStringByteSinkC2EPNS_10CharStringE
	.globl	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE
	.set	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE,_ZN6icu_6718CharStringByteSinkC2EPNS_10CharStringE
	.weak	_ZTSN6icu_6718CharStringByteSinkE
	.section	.rodata._ZTSN6icu_6718CharStringByteSinkE,"aG",@progbits,_ZTSN6icu_6718CharStringByteSinkE,comdat
	.align 16
	.type	_ZTSN6icu_6718CharStringByteSinkE, @object
	.size	_ZTSN6icu_6718CharStringByteSinkE, 30
_ZTSN6icu_6718CharStringByteSinkE:
	.string	"N6icu_6718CharStringByteSinkE"
	.weak	_ZTIN6icu_6718CharStringByteSinkE
	.section	.data.rel.ro._ZTIN6icu_6718CharStringByteSinkE,"awG",@progbits,_ZTIN6icu_6718CharStringByteSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6718CharStringByteSinkE, @object
	.size	_ZTIN6icu_6718CharStringByteSinkE, 24
_ZTIN6icu_6718CharStringByteSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718CharStringByteSinkE
	.quad	_ZTIN6icu_678ByteSinkE
	.weak	_ZTVN6icu_6718CharStringByteSinkE
	.section	.data.rel.ro._ZTVN6icu_6718CharStringByteSinkE,"awG",@progbits,_ZTVN6icu_6718CharStringByteSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6718CharStringByteSinkE, @object
	.size	_ZTVN6icu_6718CharStringByteSinkE, 56
_ZTVN6icu_6718CharStringByteSinkE:
	.quad	0
	.quad	_ZTIN6icu_6718CharStringByteSinkE
	.quad	_ZN6icu_6718CharStringByteSinkD1Ev
	.quad	_ZN6icu_6718CharStringByteSinkD0Ev
	.quad	_ZN6icu_6718CharStringByteSink6AppendEPKci
	.quad	_ZN6icu_6718CharStringByteSink15GetAppendBufferEiiPciPi
	.quad	_ZN6icu_678ByteSink5FlushEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
