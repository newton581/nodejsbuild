	.file	"uscript_props.cpp"
	.text
	.p2align 4
	.globl	uscript_getSampleString_67
	.type	uscript_getSampleString_67, @function
uscript_getSampleString_67:
.LFB2282:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	movq	%rsi, %r8
	movl	%edx, %esi
	testl	%eax, %eax
	jg	.L1
	testl	%edx, %edx
	js	.L3
	jle	.L4
	testq	%r8, %r8
	jne	.L4
.L3:
	movl	$1, (%rcx)
.L1:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%edx, %edx
	cmpl	$192, %edi
	ja	.L5
	movslq	%edi, %rdi
	leaq	_ZN12_GLOBAL__N_1L12SCRIPT_PROPSE(%rip), %rax
	movl	(%rax,%rdi,4), %eax
	movl	%eax, %edx
	andl	$2097151, %edx
	je	.L5
	testl	$2031616, %eax
	jne	.L24
	testl	%esi, %esi
	jle	.L25
	movw	%dx, (%r8)
	movl	$1, %edx
.L5:
	movq	%r8, %rdi
	jmp	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	$1, %esi
	jle	.L22
	movl	%edx, %eax
	andw	$1023, %dx
	sarl	$10, %eax
	orw	$-9216, %dx
	subw	$10304, %ax
	movw	%dx, 2(%r8)
	movw	%ax, (%r8)
.L22:
	movl	$2, %edx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$1, %edx
	jmp	.L5
	.cfi_endproc
.LFE2282:
	.size	uscript_getSampleString_67, .-uscript_getSampleString_67
	.p2align 4
	.globl	_Z33uscript_getSampleUnicodeString_6711UScriptCode
	.type	_Z33uscript_getSampleUnicodeString_6711UScriptCode, @function
_Z33uscript_getSampleUnicodeString_6711UScriptCode:
.LFB2283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	cmpl	$192, %esi
	ja	.L26
	movslq	%esi, %rsi
	leaq	_ZN12_GLOBAL__N_1L12SCRIPT_PROPSE(%rip), %rax
	movl	(%rax,%rsi,4), %esi
	andl	$2097151, %esi
	je	.L26
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L26:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2283:
	.size	_Z33uscript_getSampleUnicodeString_6711UScriptCode, .-_Z33uscript_getSampleUnicodeString_6711UScriptCode
	.p2align 4
	.globl	uscript_getUsage_67
	.type	uscript_getUsage_67, @function
uscript_getUsage_67:
.LFB2284:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$192, %edi
	ja	.L32
	movslq	%edi, %rdi
	leaq	_ZN12_GLOBAL__N_1L12SCRIPT_PROPSE(%rip), %rax
	movl	(%rax,%rdi,4), %eax
	sarl	$21, %eax
	andl	$7, %eax
.L32:
	ret
	.cfi_endproc
.LFE2284:
	.size	uscript_getUsage_67, .-uscript_getUsage_67
	.p2align 4
	.globl	uscript_isRightToLeft_67
	.type	uscript_isRightToLeft_67, @function
uscript_isRightToLeft_67:
.LFB2285:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$192, %edi
	ja	.L35
	movslq	%edi, %rdi
	leaq	_ZN12_GLOBAL__N_1L12SCRIPT_PROPSE(%rip), %rax
	movzbl	3(%rax,%rdi,4), %eax
	andl	$1, %eax
.L35:
	ret
	.cfi_endproc
.LFE2285:
	.size	uscript_isRightToLeft_67, .-uscript_isRightToLeft_67
	.p2align 4
	.globl	uscript_breaksBetweenLetters_67
	.type	uscript_breaksBetweenLetters_67, @function
uscript_breaksBetweenLetters_67:
.LFB2286:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$192, %edi
	ja	.L38
	movslq	%edi, %rdi
	leaq	_ZN12_GLOBAL__N_1L12SCRIPT_PROPSE(%rip), %rax
	movl	(%rax,%rdi,4), %eax
	shrl	$25, %eax
	andl	$1, %eax
.L38:
	ret
	.cfi_endproc
.LFE2286:
	.size	uscript_breaksBetweenLetters_67, .-uscript_breaksBetweenLetters_67
	.p2align 4
	.globl	uscript_isCased_67
	.type	uscript_isCased_67, @function
uscript_isCased_67:
.LFB2287:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$192, %edi
	ja	.L41
	movslq	%edi, %rdi
	leaq	_ZN12_GLOBAL__N_1L12SCRIPT_PROPSE(%rip), %rax
	movl	(%rax,%rdi,4), %eax
	shrl	$26, %eax
	andl	$1, %eax
.L41:
	ret
	.cfi_endproc
.LFE2287:
	.size	uscript_isCased_67, .-uscript_isCased_67
	.section	.rodata
	.align 32
	.type	_ZN12_GLOBAL__N_1L12SCRIPT_PROPSE, @object
	.size	_ZN12_GLOBAL__N_1L12SCRIPT_PROPSE, 772
_ZN12_GLOBAL__N_1L12SCRIPT_PROPSE:
	.long	10485824
	.long	10486536
	.long	27264552
	.long	77595953
	.long	10488213
	.long	44052741
	.long	73405380
	.long	71304162
	.long	77595695
	.long	71369748
	.long	10488069
	.long	10490528
	.long	10490067
	.long	4260656
	.long	77595561
	.long	10488469
	.long	10488341
	.long	44063575
	.long	10529792
	.long	27264464
	.long	44052555
	.long	10488981
	.long	44052651
	.long	44046208
	.long	44043941
	.long	77594700
	.long	10489109
	.long	4200486
	.long	44044288
	.long	4200079
	.long	4260608
	.long	10488597
	.long	4200096
	.long	10489221
	.long	23070480
	.long	10488725
	.long	10488853
	.long	27264908
	.long	44043799
	.long	10489664
	.long	6296768
	.long	39887496
	.long	4200195
	.long	4200227
	.long	4200259
	.long	4200291
	.long	2107406
	.long	21039104
	.long	6297856
	.long	4259840
	.long	4260992
	.long	4260944
	.long	39852368
	.long	4260736
	.long	0
	.long	4200960
	.long	71314432
	.long	21039616
	.long	6334464
	.long	39852416
	.long	6303024
	.long	4260768
	.long	6298373
	.long	6298560
	.long	0
	.long	4263941
	.long	6334976
	.long	0
	.long	0
	.long	0
	.long	0
	.long	4272467
	.long	0
	.long	44063575
	.long	44063575
	.long	4287260
	.long	88149153
	.long	0
	.long	6334852
	.long	6334730
	.long	0
	.long	0
	.long	6298624
	.long	4261447
	.long	23070784
	.long	0
	.long	21039488
	.long	23070666
	.long	21040128
	.long	4260715
	.long	4237376
	.long	21039360
	.long	6385408
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	6333769
	.long	0
	.long	4268032
	.long	0
	.long	2162128
	.long	4260512
	.long	44052555
	.long	39852576
	.long	4260480
	.long	21039392
	.long	6298714
	.long	4237616
	.long	6334594
	.long	4315216
	.long	6298499
	.long	0
	.long	6335424
	.long	21039168
	.long	21039872
	.long	6361347
	.long	10529792
	.long	4264067
	.long	21039832
	.long	21039968
	.long	21040015
	.long	0
	.long	21039936
	.long	20973568
	.long	39889536
	.long	0
	.long	0
	.long	6334112
	.long	6333648
	.long	0
	.long	21039712
	.long	4287206
	.long	4308000
	.long	4261120
	.long	4264725
	.long	0
	.long	0
	.long	21096450
	.long	21039520
	.long	21039765
	.long	21039254
	.long	21039219
	.long	4264638
	.long	71375028
	.long	0
	.long	0
	.long	4287055
	.long	37859780
	.long	4264323
	.long	4264144
	.long	4265600
	.long	37847593
	.long	0
	.long	4277248
	.long	4264456
	.long	4265092
	.long	4261175
	.long	4264274
	.long	37820183
	.long	21039348
	.long	4265486
	.long	4264591
	.long	4266688
	.long	4265358
	.long	90302729
	.long	4267022
	.long	4267122
	.long	6362130
	.long	73467061
	.long	44063575
	.long	10490130
	.long	0
	.long	4267280
	.long	4266588
	.long	4266507
	.long	4265995
	.long	6364529
	.long	4267749
	.long	71396928
	.long	23137554
	.long	21040962
	.long	21040921
	.long	21041137
	.long	6414600
	.long	4266446
	.long	6415073
	.long	21041087
	.long	4266252
	.long	37850213
	.long	21040776
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
