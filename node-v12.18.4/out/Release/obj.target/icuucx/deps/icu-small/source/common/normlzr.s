	.file	"normlzr.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Normalizer17getDynamicClassIDEv
	.type	_ZNK6icu_6710Normalizer17getDynamicClassIDEv, @function
_ZNK6icu_6710Normalizer17getDynamicClassIDEv:
.LFB3126:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6710Normalizer16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3126:
	.size	_ZNK6icu_6710Normalizer17getDynamicClassIDEv, .-_ZNK6icu_6710Normalizer17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710NormalizerD2Ev
	.type	_ZN6icu_6710NormalizerD2Ev, @function
_ZN6icu_6710NormalizerD2Ev:
.LFB3144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710NormalizerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	call	*8(%rax)
.L4:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	call	*8(%rax)
.L5:
	leaq	48(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3144:
	.size	_ZN6icu_6710NormalizerD2Ev, .-_ZN6icu_6710NormalizerD2Ev
	.globl	_ZN6icu_6710NormalizerD1Ev
	.set	_ZN6icu_6710NormalizerD1Ev,_ZN6icu_6710NormalizerD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode.part.0, @function
_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode.part.0:
.LFB4041:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	%esi, %edi
	movl	$2, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%si, -120(%rbp)
	movq	%rax, -128(%rbp)
	cmpq	%rcx, %r12
	je	.L29
	movq	%r8, %rsi
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movq	%rax, %r15
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L24
.L28:
	leaq	-128(%rbp), %r8
.L19:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	leaq	-128(%rbp), %r8
	andl	$32, %r14d
	movq	%r13, %rdx
	movq	%r8, -176(%rbp)
	jne	.L31
.L16:
	movq	(%r15), %rax
	movq	%rdx, -168(%rbp)
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*24(%rax)
	movq	-176(%rbp), %r8
	movq	-168(%rbp), %rdx
.L17:
	cmpq	%r8, %rdx
	jne	.L19
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L19
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-168(%rbp), %r8
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r8, %rsi
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	jg	.L28
	leaq	-128(%rbp), %r8
	andl	$32, %r14d
	movq	%r8, -176(%rbp)
	movq	%r8, %rdx
	je	.L16
.L31:
	movq	%rbx, %rdi
	movq	%rdx, -168(%rbp)
	leaq	-160(%rbp), %r14
	call	uniset_getUnicode32Instance_67@PLT
	movq	-168(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rcx
	movq	%r15, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rbx, %rcx
	movq	%rax, -144(%rbp)
	call	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %r8
	jmp	.L17
.L30:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4041:
	.size	_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode.part.0, .-_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710NormalizerD0Ev
	.type	_ZN6icu_6710NormalizerD0Ev, @function
_ZN6icu_6710NormalizerD0Ev:
.LFB3146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710NormalizerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L33
	movq	(%rdi), %rax
	call	*8(%rax)
.L33:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L34
	movq	(%rdi), %rax
	call	*8(%rax)
.L34:
	leaq	48(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3146:
	.size	_ZN6icu_6710NormalizerD0Ev, .-_ZN6icu_6710NormalizerD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer16getStaticClassIDEv
	.type	_ZN6icu_6710Normalizer16getStaticClassIDEv, @function
_ZN6icu_6710Normalizer16getStaticClassIDEv:
.LFB3125:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6710Normalizer16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3125:
	.size	_ZN6icu_6710Normalizer16getStaticClassIDEv, .-_ZN6icu_6710Normalizer16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710NormalizerC2ERKNS_13UnicodeStringE18UNormalizationMode
	.type	_ZN6icu_6710NormalizerC2ERKNS_13UnicodeStringE18UNormalizationMode, @function
_ZN6icu_6710NormalizerC2ERKNS_13UnicodeStringE18UNormalizationMode:
.LFB3128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6710NormalizerE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	movl	%edx, 24(%rdi)
	movl	$0, 28(%rdi)
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L44
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE@PLT
.L44:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	%r12, 32(%rbx)
	movl	24(%rbx), %edi
	movq	$0, 40(%rbx)
	leaq	-44(%rbp), %r12
	movq	%rax, 48(%rbx)
	movq	%r12, %rsi
	movw	%dx, 56(%rbx)
	movl	$0, 112(%rbx)
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movq	%rax, 16(%rbx)
	testb	$32, 28(%rbx)
	je	.L45
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L46
	movq	(%rdi), %rax
	call	*8(%rax)
.L46:
	movq	%r12, %rdi
	call	uniset_getUnicode32Instance_67@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L47
	movq	16(%rbx), %rdx
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rcx
	movq	%r13, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
.L47:
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
.L45:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L43
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode@PLT
	movq	%rax, 16(%rbx)
.L43:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L63:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3128:
	.size	_ZN6icu_6710NormalizerC2ERKNS_13UnicodeStringE18UNormalizationMode, .-_ZN6icu_6710NormalizerC2ERKNS_13UnicodeStringE18UNormalizationMode
	.globl	_ZN6icu_6710NormalizerC1ERKNS_13UnicodeStringE18UNormalizationMode
	.set	_ZN6icu_6710NormalizerC1ERKNS_13UnicodeStringE18UNormalizationMode,_ZN6icu_6710NormalizerC2ERKNS_13UnicodeStringE18UNormalizationMode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710NormalizerC2ERKNS_17CharacterIteratorE18UNormalizationMode
	.type	_ZN6icu_6710NormalizerC2ERKNS_17CharacterIteratorE18UNormalizationMode, @function
_ZN6icu_6710NormalizerC2ERKNS_17CharacterIteratorE18UNormalizationMode:
.LFB3134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-44(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6710NormalizerE(%rip), %rax
	movl	%edx, 24(%rbx)
	movq	%rax, (%rbx)
	movq	(%rsi), %rax
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movl	$0, 28(%rbx)
	call	*64(%rax)
	movl	$2, %edx
	movl	24(%rbx), %edi
	movq	%r12, %rsi
	movq	%rax, 32(%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	$0, 40(%rbx)
	movq	%rax, 48(%rbx)
	movw	%dx, 56(%rbx)
	movl	$0, 112(%rbx)
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movq	%rax, 16(%rbx)
	testb	$32, 28(%rbx)
	je	.L65
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L66
	movq	(%rdi), %rax
	call	*8(%rax)
.L66:
	movq	%r12, %rdi
	call	uniset_getUnicode32Instance_67@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L67
	movq	16(%rbx), %rdx
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rcx
	movq	%r13, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
.L67:
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
.L65:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L64
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode@PLT
	movq	%rax, 16(%rbx)
.L64:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L80:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3134:
	.size	_ZN6icu_6710NormalizerC2ERKNS_17CharacterIteratorE18UNormalizationMode, .-_ZN6icu_6710NormalizerC2ERKNS_17CharacterIteratorE18UNormalizationMode
	.globl	_ZN6icu_6710NormalizerC1ERKNS_17CharacterIteratorE18UNormalizationMode
	.set	_ZN6icu_6710NormalizerC1ERKNS_17CharacterIteratorE18UNormalizationMode,_ZN6icu_6710NormalizerC2ERKNS_17CharacterIteratorE18UNormalizationMode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer4initEv
	.type	_ZN6icu_6710Normalizer4initEv, @function
_ZN6icu_6710Normalizer4initEv:
.LFB3142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-44(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rsi
	subq	$24, %rsp
	movl	24(%rdi), %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movq	%rax, 16(%rbx)
	testb	$32, 28(%rbx)
	je	.L82
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L83
	movq	(%rdi), %rax
	call	*8(%rax)
.L83:
	movq	%r12, %rdi
	call	uniset_getUnicode32Instance_67@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L84
	movq	16(%rbx), %rdx
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rcx
	movq	%r13, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
.L84:
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
.L82:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L81
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode@PLT
	movq	%rax, 16(%rbx)
.L81:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L97:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3142:
	.size	_ZN6icu_6710Normalizer4initEv, .-_ZN6icu_6710Normalizer4initEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710NormalizerC2ENS_14ConstChar16PtrEi18UNormalizationMode
	.type	_ZN6icu_6710NormalizerC2ENS_14ConstChar16PtrEi18UNormalizationMode, @function
_ZN6icu_6710NormalizerC2ENS_14ConstChar16PtrEi18UNormalizationMode:
.LFB3131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6710NormalizerE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	(%rsi), %rax
	movq	$0, 16(%rdi)
	movl	%ecx, 24(%rdi)
	movl	$0, 28(%rdi)
	movl	$32, %edi
	movq	%rax, -48(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L99
	leaq	-48(%rbp), %rsi
	movl	%r13d, %edx
	movq	%rax, %rdi
	call	_ZN6icu_6722UCharCharacterIteratorC1ENS_14ConstChar16PtrEi@PLT
.L99:
	movq	%rbx, 32(%r12)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r12, %rdi
	movq	$0, 40(%r12)
	movq	%rax, 48(%r12)
	movl	$2, %eax
	movw	%ax, 56(%r12)
	movl	$0, 112(%r12)
	call	_ZN6icu_6710Normalizer4initEv
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L105:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3131:
	.size	_ZN6icu_6710NormalizerC2ENS_14ConstChar16PtrEi18UNormalizationMode, .-_ZN6icu_6710NormalizerC2ENS_14ConstChar16PtrEi18UNormalizationMode
	.globl	_ZN6icu_6710NormalizerC1ENS_14ConstChar16PtrEi18UNormalizationMode
	.set	_ZN6icu_6710NormalizerC1ENS_14ConstChar16PtrEi18UNormalizationMode,_ZN6icu_6710NormalizerC2ENS_14ConstChar16PtrEi18UNormalizationMode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710NormalizerC2ERKS0_
	.type	_ZN6icu_6710NormalizerC2ERKS0_, @function
_ZN6icu_6710NormalizerC2ERKS0_:
.LFB3140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710NormalizerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	%rax, (%rdi)
	movq	24(%rsi), %rax
	movq	$0, 8(%rdi)
	movq	%rax, 24(%rdi)
	movq	$0, 16(%rdi)
	movq	32(%rsi), %rdi
	movq	(%rdi), %rax
	call	*64(%rax)
	leaq	48(%r12), %rdi
	leaq	48(%rbx), %rsi
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	112(%rbx), %eax
	movq	%r12, %rdi
	popq	%rbx
	movl	%eax, 112(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710Normalizer4initEv
	.cfi_endproc
.LFE3140:
	.size	_ZN6icu_6710NormalizerC2ERKS0_, .-_ZN6icu_6710NormalizerC2ERKS0_
	.globl	_ZN6icu_6710NormalizerC1ERKS0_
	.set	_ZN6icu_6710NormalizerC1ERKS0_,_ZN6icu_6710NormalizerC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Normalizer5cloneEv
	.type	_ZNK6icu_6710Normalizer5cloneEv, @function
_ZNK6icu_6710Normalizer5cloneEv:
.LFB3147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$120, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L108
	leaq	16+_ZTVN6icu_6710NormalizerE(%rip), %rax
	movq	32(%rbx), %rdi
	movq	$0, 8(%r12)
	movq	%rax, (%r12)
	movq	24(%rbx), %rax
	movq	$0, 16(%r12)
	movq	%rax, 24(%r12)
	movq	(%rdi), %rax
	call	*64(%rax)
	leaq	48(%r12), %rdi
	leaq	48(%rbx), %rsi
	movq	%rax, 32(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 40(%r12)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	112(%rbx), %eax
	movq	%r12, %rdi
	movl	%eax, 112(%r12)
	call	_ZN6icu_6710Normalizer4initEv
.L108:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3147:
	.size	_ZNK6icu_6710Normalizer5cloneEv, .-_ZNK6icu_6710Normalizer5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Normalizer8hashCodeEv
	.type	_ZNK6icu_6710Normalizer8hashCodeEv, @function
_ZNK6icu_6710Normalizer8hashCodeEv:
.LFB3148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	28(%r12), %ebx
	addl	24(%r12), %eax
	leaq	48(%r12), %rdi
	addl	%eax, %ebx
	call	_ZNK6icu_6713UnicodeString10doHashCodeEv@PLT
	addl	%ebx, %eax
	popq	%rbx
	addl	112(%r12), %eax
	addl	40(%r12), %eax
	addl	44(%r12), %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3148:
	.size	_ZNK6icu_6710Normalizer8hashCodeEv, .-_ZNK6icu_6710Normalizer8hashCodeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710NormalizereqERKS0_
	.type	_ZNK6icu_6710NormalizereqERKS0_, @function
_ZNK6icu_6710NormalizereqERKS0_:
.LFB3149:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L139
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	24(%rsi), %rax
	cmpq	%rax, 24(%rdi)
	je	.L118
.L119:
	xorl	%eax, %eax
.L116:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	movq	32(%rsi), %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	testb	%al, %al
	je	.L119
	movswl	56(%rbx), %ecx
	movzwl	56(%r12), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L120
	testw	%ax, %ax
	js	.L121
	movswl	%ax, %edx
	sarl	$5, %edx
.L122:
	testw	%cx, %cx
	js	.L123
	sarl	$5, %ecx
.L124:
	testb	%sil, %sil
	jne	.L119
	cmpl	%edx, %ecx
	jne	.L119
	leaq	48(%rbx), %rsi
	leaq	48(%r12), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L120:
	testb	%sil, %sil
	je	.L119
	movl	112(%rbx), %eax
	cmpl	%eax, 112(%r12)
	jne	.L119
	movl	44(%rbx), %eax
	cmpl	%eax, 44(%r12)
	sete	%al
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	60(%r12), %edx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L123:
	movl	60(%rbx), %ecx
	jmp	.L124
	.cfi_endproc
.LFE3149:
	.size	_ZNK6icu_6710NormalizereqERKS0_, .-_ZNK6icu_6710NormalizereqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode
	.type	_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode, @function
_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode:
.LFB3150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%r8, %rbx
	subq	$8, %rsp
	testb	$1, 8(%rdi)
	jne	.L143
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jle	.L144
.L143:
	movq	%rcx, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L148
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rbx, %r8
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode.part.0
	.cfi_endproc
.LFE3150:
	.size	_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode, .-_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer7composeERKNS_13UnicodeStringEaiRS1_R10UErrorCode
	.type	_ZN6icu_6710Normalizer7composeERKNS_13UnicodeStringEaiRS1_R10UErrorCode, @function
_ZN6icu_6710Normalizer7composeERKNS_13UnicodeStringEaiRS1_R10UErrorCode:
.LFB3151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%r8, %rbx
	subq	$8, %rsp
	testb	%sil, %sil
	setne	%sil
	movzbl	%sil, %esi
	addl	$4, %esi
	testb	$1, 8(%rdi)
	jne	.L151
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jle	.L152
.L151:
	movq	%rcx, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L157
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rbx, %r8
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode.part.0
	.cfi_endproc
.LFE3151:
	.size	_ZN6icu_6710Normalizer7composeERKNS_13UnicodeStringEaiRS1_R10UErrorCode, .-_ZN6icu_6710Normalizer7composeERKNS_13UnicodeStringEaiRS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer9decomposeERKNS_13UnicodeStringEaiRS1_R10UErrorCode
	.type	_ZN6icu_6710Normalizer9decomposeERKNS_13UnicodeStringEaiRS1_R10UErrorCode, @function
_ZN6icu_6710Normalizer9decomposeERKNS_13UnicodeStringEaiRS1_R10UErrorCode:
.LFB3152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%r8, %rbx
	subq	$8, %rsp
	testb	%sil, %sil
	setne	%sil
	movzbl	%sil, %esi
	addl	$2, %esi
	testb	$1, 8(%rdi)
	jne	.L160
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jle	.L161
.L160:
	movq	%rcx, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L166
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rbx, %r8
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710Normalizer9normalizeERKNS_13UnicodeStringE18UNormalizationModeiRS1_R10UErrorCode.part.0
	.cfi_endproc
.LFE3152:
	.size	_ZN6icu_6710Normalizer9decomposeERKNS_13UnicodeStringEaiRS1_R10UErrorCode, .-_ZN6icu_6710Normalizer9decomposeERKNS_13UnicodeStringEaiRS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer10quickCheckERKNS_13UnicodeStringE18UNormalizationModeiR10UErrorCode
	.type	_ZN6icu_6710Normalizer10quickCheckERKNS_13UnicodeStringE18UNormalizationModeiR10UErrorCode, @function
_ZN6icu_6710Normalizer10quickCheckERKNS_13UnicodeStringE18UNormalizationModeiR10UErrorCode:
.LFB3153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movl	%esi, %edi
	pushq	%r13
	movq	%rcx, %rsi
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%rax, %r13
	movl	$2, %eax
	testl	%edx, %edx
	jg	.L167
	andl	$32, %ebx
	jne	.L173
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*104(%rax)
.L167:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L174
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rbx
	call	uniset_getUnicode32Instance_67@PLT
	movq	%r13, -56(%rbp)
	leaq	-64(%rbp), %r13
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZNK6icu_6719FilteredNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r13, %rdi
	movl	%eax, -68(%rbp)
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
	movl	-68(%rbp), %eax
	jmp	.L167
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3153:
	.size	_ZN6icu_6710Normalizer10quickCheckERKNS_13UnicodeStringE18UNormalizationModeiR10UErrorCode, .-_ZN6icu_6710Normalizer10quickCheckERKNS_13UnicodeStringE18UNormalizationModeiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer12isNormalizedERKNS_13UnicodeStringE18UNormalizationModeiR10UErrorCode
	.type	_ZN6icu_6710Normalizer12isNormalizedERKNS_13UnicodeStringE18UNormalizationModeiR10UErrorCode, @function
_ZN6icu_6710Normalizer12isNormalizedERKNS_13UnicodeStringE18UNormalizationModeiR10UErrorCode:
.LFB3154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	%esi, %edi
	movq	%rcx, %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movl	(%r12), %edx
	movq	%rax, %r14
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L175
	andl	$32, %ebx
	jne	.L181
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	*88(%rax)
.L175:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L182
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rbx
	call	uniset_getUnicode32Instance_67@PLT
	movq	%r14, -56(%rbp)
	leaq	-64(%rbp), %r14
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rbx, -64(%rbp)
	movq	%rax, -48(%rbp)
	call	_ZNK6icu_6719FilteredNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r14, %rdi
	movb	%al, -65(%rbp)
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
	movzbl	-65(%rbp), %eax
	jmp	.L175
.L182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3154:
	.size	_ZN6icu_6710Normalizer12isNormalizedERKNS_13UnicodeStringE18UNormalizationModeiR10UErrorCode, .-_ZN6icu_6710Normalizer12isNormalizedERKNS_13UnicodeStringE18UNormalizationModeiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer11concatenateERKNS_13UnicodeStringES3_RS1_18UNormalizationModeiR10UErrorCode
	.type	_ZN6icu_6710Normalizer11concatenateERKNS_13UnicodeStringES3_RS1_18UNormalizationModeiR10UErrorCode, @function
_ZN6icu_6710Normalizer11concatenateERKNS_13UnicodeStringES3_RS1_18UNormalizationModeiR10UErrorCode:
.LFB3155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, 8(%rdi)
	jne	.L184
	movq	%rsi, %r13
	testb	$1, 8(%rsi)
	je	.L202
.L184:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L187
	movl	$1, (%rbx)
.L187:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movl	(%r9), %r9d
	testl	%r9d, %r9d
	jg	.L184
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movq	%rax, -128(%rbp)
	movw	%si, -120(%rbp)
	cmpq	%rdx, %r13
	je	.L204
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	movl	%r8d, -172(%rbp)
	leaq	-128(%rbp), %r15
	movl	%ecx, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	-168(%rbp), %ecx
	movq	%rbx, %rsi
	movl	%ecx, %edi
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movl	-172(%rbp), %r8d
	movq	%rax, %r14
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L205
.L193:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L204:
	leaq	-128(%rbp), %r15
	movq	%rdi, %rsi
	movl	%r8d, -172(%rbp)
	movq	%r15, %rdi
	movl	%ecx, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	-168(%rbp), %ecx
	movq	%rbx, %rsi
	movl	%ecx, %edi
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	jg	.L193
	movl	-172(%rbp), %r8d
	movq	%r15, %rsi
.L195:
	andl	$32, %r8d
	jne	.L206
	movq	(%r14), %rax
	movq	%rsi, -168(%rbp)
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	*48(%rax)
	movq	-168(%rbp), %rsi
.L191:
	cmpq	%r15, %rsi
	jne	.L193
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L193
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%r12, %rsi
	leaq	-128(%rbp), %r15
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%rbx, %rdi
	movq	%rsi, -168(%rbp)
	call	uniset_getUnicode32Instance_67@PLT
	movq	-168(%rbp), %rsi
	movq	%r14, -152(%rbp)
	movq	%rbx, %rcx
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rdx
	leaq	-160(%rbp), %r14
	movq	%rax, -144(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%r14, %rdi
	movq	%r13, %rdx
	call	_ZNK6icu_6719FilteredNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
	movq	-168(%rbp), %rsi
	jmp	.L191
.L203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3155:
	.size	_ZN6icu_6710Normalizer11concatenateERKNS_13UnicodeStringES3_RS1_18UNormalizationModeiR10UErrorCode, .-_ZN6icu_6710Normalizer11concatenateERKNS_13UnicodeStringES3_RS1_18UNormalizationModeiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer5resetEv
	.type	_ZN6icu_6710Normalizer5resetEv, @function
_ZN6icu_6710Normalizer5resetEv:
.LFB3159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*192(%rax)
	movzwl	56(%rbx), %edx
	movl	$0, 112(%rbx)
	movl	%eax, 44(%rbx)
	movl	%eax, 40(%rbx)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3159:
	.size	_ZN6icu_6710Normalizer5resetEv, .-_ZN6icu_6710Normalizer5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer12setIndexOnlyEi
	.type	_ZN6icu_6710Normalizer12setIndexOnlyEi, @function
_ZN6icu_6710Normalizer12setIndexOnlyEi:
.LFB3160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	movq	32(%rbx), %rax
	movzwl	56(%rbx), %edx
	movl	12(%rax), %eax
	movl	$0, 112(%rbx)
	movl	%eax, 44(%rbx)
	movl	%eax, 40(%rbx)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3160:
	.size	_ZN6icu_6710Normalizer12setIndexOnlyEi, .-_ZN6icu_6710Normalizer12setIndexOnlyEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Normalizer8getIndexEv
	.type	_ZNK6icu_6710Normalizer8getIndexEv, @function
_ZNK6icu_6710Normalizer8getIndexEv:
.LFB3163:
	.cfi_startproc
	endbr64
	movswl	56(%rdi), %eax
	movl	112(%rdi), %ecx
	testw	%ax, %ax
	js	.L216
	movl	44(%rdi), %edx
	sarl	$5, %eax
	cmpl	%eax, %ecx
	movl	%edx, %eax
	cmovl	40(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	movl	60(%rdi), %eax
	movl	44(%rdi), %edx
	cmpl	%eax, %ecx
	movl	%edx, %eax
	cmovl	40(%rdi), %eax
	ret
	.cfi_endproc
.LFE3163:
	.size	_ZNK6icu_6710Normalizer8getIndexEv, .-_ZNK6icu_6710Normalizer8getIndexEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Normalizer10startIndexEv
	.type	_ZNK6icu_6710Normalizer10startIndexEv, @function
_ZNK6icu_6710Normalizer10startIndexEv:
.LFB3164:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movl	16(%rax), %eax
	ret
	.cfi_endproc
.LFE3164:
	.size	_ZNK6icu_6710Normalizer10startIndexEv, .-_ZNK6icu_6710Normalizer10startIndexEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Normalizer8endIndexEv
	.type	_ZNK6icu_6710Normalizer8endIndexEv, @function
_ZNK6icu_6710Normalizer8endIndexEv:
.LFB3165:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movl	20(%rax), %eax
	ret
	.cfi_endproc
.LFE3165:
	.size	_ZNK6icu_6710Normalizer8endIndexEv, .-_ZNK6icu_6710Normalizer8endIndexEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer7setModeE18UNormalizationMode
	.type	_ZN6icu_6710Normalizer7setModeE18UNormalizationMode, @function
_ZN6icu_6710Normalizer7setModeE18UNormalizationMode:
.LFB3166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-44(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%esi, 24(%rbx)
	movq	%r12, %rsi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movq	%rax, 16(%rbx)
	testb	$32, 28(%rbx)
	je	.L222
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L223
	movq	(%rdi), %rax
	call	*8(%rax)
.L223:
	movq	%r12, %rdi
	call	uniset_getUnicode32Instance_67@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L224
	movq	16(%rbx), %rdx
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rcx
	movq	%r13, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
.L224:
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
.L222:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L221
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode@PLT
	movq	%rax, 16(%rbx)
.L221:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L237:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3166:
	.size	_ZN6icu_6710Normalizer7setModeE18UNormalizationMode, .-_ZN6icu_6710Normalizer7setModeE18UNormalizationMode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Normalizer8getUModeEv
	.type	_ZNK6icu_6710Normalizer8getUModeEv, @function
_ZNK6icu_6710Normalizer8getUModeEv:
.LFB3167:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE3167:
	.size	_ZNK6icu_6710Normalizer8getUModeEv, .-_ZNK6icu_6710Normalizer8getUModeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer9setOptionEia
	.type	_ZN6icu_6710Normalizer9setOptionEia, @function
_ZN6icu_6710Normalizer9setOptionEia:
.LFB3168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-44(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	28(%rdi), %eax
	movl	$0, -44(%rbp)
	movl	%eax, %edx
	orl	%esi, %edx
	notl	%esi
	andl	%eax, %esi
	testb	%r8b, %r8b
	cmovne	%edx, %esi
	movl	%esi, 28(%rdi)
	movl	24(%rdi), %edi
	movq	%r12, %rsi
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movq	%rax, 16(%rbx)
	testb	$32, 28(%rbx)
	je	.L242
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L243
	movq	(%rdi), %rax
	call	*8(%rax)
.L243:
	movq	%r12, %rdi
	call	uniset_getUnicode32Instance_67@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L244
	movq	16(%rbx), %rdx
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rcx
	movq	%r13, 16(%rax)
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
.L244:
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
.L242:
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jle	.L239
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode@PLT
	movq	%rax, 16(%rbx)
.L239:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L257:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3168:
	.size	_ZN6icu_6710Normalizer9setOptionEia, .-_ZN6icu_6710Normalizer9setOptionEia
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710Normalizer9getOptionEi
	.type	_ZNK6icu_6710Normalizer9getOptionEi, @function
_ZNK6icu_6710Normalizer9getOptionEi:
.LFB3169:
	.cfi_startproc
	endbr64
	testl	%esi, 28(%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE3169:
	.size	_ZNK6icu_6710Normalizer9getOptionEi, .-_ZNK6icu_6710Normalizer9getOptionEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer7setTextERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6710Normalizer7setTextERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6710Normalizer7setTextERKNS_13UnicodeStringER10UErrorCode:
.LFB3170:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L272
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L261
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6723StringCharacterIteratorC1ERKNS_13UnicodeStringE@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L262
	movq	(%rdi), %rax
	call	*8(%rax)
.L262:
	movq	(%r14), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, 32(%rbx)
	movq	%r14, %rdi
	call	*192(%rax)
	movzwl	56(%rbx), %edx
	movl	$0, 112(%rbx)
	movl	%eax, 44(%rbx)
	movl	%eax, 40(%rbx)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 56(%rbx)
.L259:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L261:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L259
	.cfi_endproc
.LFE3170:
	.size	_ZN6icu_6710Normalizer7setTextERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6710Normalizer7setTextERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer7setTextERKNS_17CharacterIteratorER10UErrorCode
	.type	_ZN6icu_6710Normalizer7setTextERKNS_17CharacterIteratorER10UErrorCode, @function
_ZN6icu_6710Normalizer7setTextERKNS_17CharacterIteratorER10UErrorCode:
.LFB3171:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L283
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	(%rsi), %rax
	call	*64(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L286
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L276
	movq	(%rdi), %rax
	call	*8(%rax)
.L276:
	movq	0(%r13), %rax
	movq	%r13, 32(%rbx)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	*192(%rax)
	movzwl	56(%rbx), %edx
	movl	$0, 112(%rbx)
	movl	%eax, 44(%rbx)
	movl	%eax, 40(%rbx)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3171:
	.size	_ZN6icu_6710Normalizer7setTextERKNS_17CharacterIteratorER10UErrorCode, .-_ZN6icu_6710Normalizer7setTextERKNS_17CharacterIteratorER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer7setTextENS_14ConstChar16PtrEiR10UErrorCode
	.type	_ZN6icu_6710Normalizer7setTextENS_14ConstChar16PtrEiR10UErrorCode, @function
_ZN6icu_6710Normalizer7setTextENS_14ConstChar16PtrEiR10UErrorCode:
.LFB3172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L302
.L287:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rdi, %rbx
	movl	$32, %edi
	movl	%edx, %r14d
	movq	%rcx, %r12
	movq	%rax, -48(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L289
	leaq	-48(%rbp), %rsi
	movl	%r14d, %edx
	movq	%rax, %rdi
	call	_ZN6icu_6722UCharCharacterIteratorC1ENS_14ConstChar16PtrEi@PLT
.L289:
	testq	%r13, %r13
	je	.L304
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L291
	movq	(%rdi), %rax
	call	*8(%rax)
.L291:
	movq	0(%r13), %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, 32(%rbx)
	movq	%r13, %rdi
	call	*192(%rax)
	movzwl	56(%rbx), %edx
	movl	$0, 112(%rbx)
	movl	%eax, 44(%rbx)
	movl	%eax, 40(%rbx)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 56(%rbx)
	jmp	.L287
.L303:
	call	__stack_chk_fail@PLT
.L304:
	movl	$7, (%r12)
	jmp	.L287
	.cfi_endproc
.LFE3172:
	.size	_ZN6icu_6710Normalizer7setTextENS_14ConstChar16PtrEiR10UErrorCode, .-_ZN6icu_6710Normalizer7setTextENS_14ConstChar16PtrEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer7getTextERNS_13UnicodeStringE
	.type	_ZN6icu_6710Normalizer7getTextERNS_13UnicodeStringE, @function
_ZN6icu_6710Normalizer7getTextERNS_13UnicodeStringE:
.LFB3173:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*208(%rax)
	.cfi_endproc
.LFE3173:
	.size	_ZN6icu_6710Normalizer7getTextERNS_13UnicodeStringE, .-_ZN6icu_6710Normalizer7getTextERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer11clearBufferEv
	.type	_ZN6icu_6710Normalizer11clearBufferEv, @function
_ZN6icu_6710Normalizer11clearBufferEv:
.LFB3174:
	.cfi_startproc
	endbr64
	movzwl	56(%rdi), %edx
	movl	$0, 112(%rdi)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 56(%rdi)
	ret
	.cfi_endproc
.LFE3174:
	.size	_ZN6icu_6710Normalizer11clearBufferEv, .-_ZN6icu_6710Normalizer11clearBufferEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer13nextNormalizeEv
	.type	_ZN6icu_6710Normalizer13nextNormalizeEv, @function
_ZN6icu_6710Normalizer13nextNormalizeEv:
.LFB3175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movzwl	56(%rdi), %edx
	movl	44(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, 112(%rdi)
	movl	%edx, %eax
	movl	%esi, 40(%rdi)
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 56(%rdi)
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movl	%eax, %r12d
	testb	%al, %al
	je	.L309
	movq	32(%rbx), %rdi
	leaq	-128(%rbp), %r13
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L328:
	movq	16(%rbx), %r14
	movq	32(%rbx), %rdi
	movq	(%r14), %rax
	movq	120(%rax), %r15
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%r14, %rdi
	movl	%eax, %r12d
	movl	%eax, %esi
	call	*%r15
	testb	%al, %al
	jne	.L327
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L314:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	testb	%al, %al
	jne	.L328
.L312:
	movq	32(%rbx), %rax
	movq	16(%rbx), %rdi
	leaq	48(%rbx), %rdx
	movq	%r13, %rsi
	movl	$0, -132(%rbp)
	leaq	-132(%rbp), %rcx
	xorl	%r12d, %r12d
	movl	12(%rax), %eax
	movl	%eax, 44(%rbx)
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	jle	.L329
.L315:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L309:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L330
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	movswl	56(%rbx), %eax
	shrl	$5, %eax
	setne	%r12b
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L327:
	movq	32(%rbx), %rdi
	movl	$1, %edx
	movl	$-1, %esi
	movq	(%rdi), %rax
	call	*200(%rax)
	jmp	.L312
.L330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3175:
	.size	_ZN6icu_6710Normalizer13nextNormalizeEv, .-_ZN6icu_6710Normalizer13nextNormalizeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer7currentEv
	.type	_ZN6icu_6710Normalizer7currentEv, @function
_ZN6icu_6710Normalizer7currentEv:
.LFB3156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movswl	56(%rdi), %eax
	movl	112(%rdi), %esi
	testw	%ax, %ax
	js	.L332
	sarl	$5, %eax
.L333:
	cmpl	%eax, %esi
	jl	.L334
	movq	%rbx, %rdi
	call	_ZN6icu_6710Normalizer13nextNormalizeEv
	testb	%al, %al
	je	.L335
	movl	112(%rbx), %esi
.L334:
	addq	$8, %rsp
	leaq	48(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movl	60(%rdi), %eax
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L335:
	addq	$8, %rsp
	movl	$65535, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3156:
	.size	_ZN6icu_6710Normalizer7currentEv, .-_ZN6icu_6710Normalizer7currentEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer4nextEv
	.type	_ZN6icu_6710Normalizer4nextEv, @function
_ZN6icu_6710Normalizer4nextEv:
.LFB3157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movswl	56(%rdi), %eax
	movl	112(%rdi), %esi
	testw	%ax, %ax
	js	.L338
	sarl	$5, %eax
.L339:
	cmpl	%eax, %esi
	jl	.L340
	movq	%rbx, %rdi
	call	_ZN6icu_6710Normalizer13nextNormalizeEv
	movl	%eax, %r8d
	movl	$65535, %eax
	testb	%r8b, %r8b
	je	.L337
	movl	112(%rbx), %esi
.L340:
	leaq	48(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	cmpl	$65535, %eax
	seta	%dl
	addl	$1, %edx
	addl	%edx, 112(%rbx)
.L337:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movl	60(%rdi), %eax
	jmp	.L339
	.cfi_endproc
.LFE3157:
	.size	_ZN6icu_6710Normalizer4nextEv, .-_ZN6icu_6710Normalizer4nextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer5firstEv
	.type	_ZN6icu_6710Normalizer5firstEv, @function
_ZN6icu_6710Normalizer5firstEv:
.LFB3161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*192(%rax)
	movl	%eax, 44(%rbx)
	movl	%eax, 40(%rbx)
	movzwl	56(%rbx), %eax
	testb	$1, %al
	je	.L348
	movl	$2, %eax
	movl	$0, 112(%rbx)
	movw	%ax, 56(%rbx)
.L349:
	movq	%rbx, %rdi
	call	_ZN6icu_6710Normalizer13nextNormalizeEv
	movl	%eax, %r8d
	movl	$65535, %eax
	testb	%r8b, %r8b
	je	.L347
	movl	112(%rbx), %esi
	leaq	48(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	cmpl	$65535, %eax
	seta	%dl
	addl	$1, %edx
	addl	%edx, 112(%rbx)
.L347:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	andl	$31, %eax
	movl	$0, 112(%rbx)
	movw	%ax, 56(%rbx)
	jmp	.L349
	.cfi_endproc
.LFE3161:
	.size	_ZN6icu_6710Normalizer5firstEv, .-_ZN6icu_6710Normalizer5firstEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer17previousNormalizeEv
	.type	_ZN6icu_6710Normalizer17previousNormalizeEv, @function
_ZN6icu_6710Normalizer17previousNormalizeEv:
.LFB3176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$104, %rsp
	movzwl	56(%rdi), %edx
	movl	40(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, 112(%rdi)
	movl	%edx, %eax
	movl	%esi, 44(%rdi)
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 56(%rdi)
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*120(%rax)
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	call	*184(%rax)
	movl	%eax, %r12d
	testb	%al, %al
	je	.L356
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	leaq	-112(%rbp), %r13
	movq	%rax, -112(%rbp)
	movw	%dx, -104(%rbp)
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L359:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	call	*176(%rax)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	%eax, %ecx
	movl	%eax, %r12d
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	movq	16(%rbx), %rdi
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*120(%rax)
	testb	%al, %al
	jne	.L362
.L363:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	call	*184(%rax)
	testb	%al, %al
	jne	.L359
.L362:
	movq	32(%rbx), %rax
	movq	16(%rbx), %rdi
	leaq	48(%rbx), %rdx
	leaq	-116(%rbp), %rcx
	movl	$0, -116(%rbp)
	movq	%r13, %rsi
	movl	12(%rax), %eax
	movl	%eax, 40(%rbx)
	movq	(%rdi), %rax
	call	*24(%rax)
	movswl	56(%rbx), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	js	.L375
	movl	%eax, 112(%rbx)
	movl	-116(%rbp), %eax
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jle	.L376
.L365:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L356:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L377
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	shrl	$5, %edx
	setne	%r12b
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L375:
	movl	60(%rbx), %eax
	xorl	%r12d, %r12d
	movl	%eax, 112(%rbx)
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	jg	.L365
	jmp	.L376
.L377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3176:
	.size	_ZN6icu_6710Normalizer17previousNormalizeEv, .-_ZN6icu_6710Normalizer17previousNormalizeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer8previousEv
	.type	_ZN6icu_6710Normalizer8previousEv, @function
_ZN6icu_6710Normalizer8previousEv:
.LFB3158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	112(%rdi), %esi
	testl	%esi, %esi
	jg	.L379
	call	_ZN6icu_6710Normalizer17previousNormalizeEv
	movl	%eax, %r8d
	movl	$65535, %eax
	testb	%r8b, %r8b
	je	.L378
	movl	112(%rbx), %esi
.L379:
	subl	$1, %esi
	leaq	48(%rbx), %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	cmpl	$65535, %eax
	seta	%dl
	addl	$1, %edx
	subl	%edx, 112(%rbx)
.L378:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3158:
	.size	_ZN6icu_6710Normalizer8previousEv, .-_ZN6icu_6710Normalizer8previousEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Normalizer4lastEv
	.type	_ZN6icu_6710Normalizer4lastEv, @function
_ZN6icu_6710Normalizer4lastEv:
.LFB3162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*192(%rax)
	movzwl	56(%rbx), %edx
	movl	$0, 112(%rbx)
	movq	%rbx, %rdi
	movl	%eax, 44(%rbx)
	movl	%eax, 40(%rbx)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 56(%rbx)
	call	_ZN6icu_6710Normalizer17previousNormalizeEv
	movl	%eax, %r8d
	movl	$65535, %eax
	testb	%r8b, %r8b
	je	.L386
	movl	112(%rbx), %eax
	leaq	48(%rbx), %rdi
	leal	-1(%rax), %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	xorl	%edx, %edx
	cmpl	$65535, %eax
	seta	%dl
	addl	$1, %edx
	subl	%edx, 112(%rbx)
.L386:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3162:
	.size	_ZN6icu_6710Normalizer4lastEv, .-_ZN6icu_6710Normalizer4lastEv
	.weak	_ZTSN6icu_6710NormalizerE
	.section	.rodata._ZTSN6icu_6710NormalizerE,"aG",@progbits,_ZTSN6icu_6710NormalizerE,comdat
	.align 16
	.type	_ZTSN6icu_6710NormalizerE, @object
	.size	_ZTSN6icu_6710NormalizerE, 22
_ZTSN6icu_6710NormalizerE:
	.string	"N6icu_6710NormalizerE"
	.weak	_ZTIN6icu_6710NormalizerE
	.section	.data.rel.ro._ZTIN6icu_6710NormalizerE,"awG",@progbits,_ZTIN6icu_6710NormalizerE,comdat
	.align 8
	.type	_ZTIN6icu_6710NormalizerE, @object
	.size	_ZTIN6icu_6710NormalizerE, 24
_ZTIN6icu_6710NormalizerE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6710NormalizerE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6710NormalizerE
	.section	.data.rel.ro.local._ZTVN6icu_6710NormalizerE,"awG",@progbits,_ZTVN6icu_6710NormalizerE,comdat
	.align 8
	.type	_ZTVN6icu_6710NormalizerE, @object
	.size	_ZTVN6icu_6710NormalizerE, 40
_ZTVN6icu_6710NormalizerE:
	.quad	0
	.quad	_ZTIN6icu_6710NormalizerE
	.quad	_ZN6icu_6710NormalizerD1Ev
	.quad	_ZN6icu_6710NormalizerD0Ev
	.quad	_ZNK6icu_6710Normalizer17getDynamicClassIDEv
	.local	_ZZN6icu_6710Normalizer16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6710Normalizer16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
