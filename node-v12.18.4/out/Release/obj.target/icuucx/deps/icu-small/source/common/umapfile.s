	.file	"umapfile.cpp"
	.text
	.p2align 4
	.globl	uprv_mapFile_67
	.type	uprv_mapFile_67, @function
uprv_mapFile_67:
.LFB2015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$160, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L2
.L4:
	xorl	%eax, %eax
.L1:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L14
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%rsi, %r12
	movq	%rdi, %rbx
	call	UDataMemory_init_67@PLT
	leaq	-192(%rbp), %rdx
	movq	%r12, %rsi
	movl	$1, %edi
	call	__xstat@PLT
	testl	%eax, %eax
	jne	.L4
	movq	-144(%rbp), %r13
	testq	%r13, %r13
	jle	.L4
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	open@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L4
	movslq	%r13d, %r13
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movl	%eax, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r13, %rsi
	call	mmap@PLT
	movl	%r12d, %edi
	movq	%rax, %r14
	call	close@PLT
	cmpq	$-1, %r14
	je	.L4
	addq	%r14, %r13
	movq	%r14, 8(%rbx)
	movl	$1, %eax
	movq	%r13, 40(%rbx)
	movq	%r14, 32(%rbx)
	jmp	.L1
.L14:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2015:
	.size	uprv_mapFile_67, .-uprv_mapFile_67
	.p2align 4
	.globl	uprv_unmapFile_67
	.type	uprv_unmapFile_67, @function
uprv_unmapFile_67:
.LFB2016:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L24
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L15
	movq	32(%rdi), %rdi
	subq	%rdi, %rsi
	call	munmap@PLT
	movq	$0, 8(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
.L15:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2016:
	.size	uprv_unmapFile_67, .-uprv_unmapFile_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
