	.file	"uvectr64.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679UVector6417getDynamicClassIDEv
	.type	_ZNK6icu_679UVector6417getDynamicClassIDEv, @function
_ZNK6icu_679UVector6417getDynamicClassIDEv:
.LFB2087:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_679UVector6416getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2087:
	.size	_ZNK6icu_679UVector6417getDynamicClassIDEv, .-_ZNK6icu_679UVector6417getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector64D2Ev
	.type	_ZN6icu_679UVector64D2Ev, @function
_ZN6icu_679UVector64D2Ev:
.LFB2099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679UVector64E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	movq	$0, 24(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2099:
	.size	_ZN6icu_679UVector64D2Ev, .-_ZN6icu_679UVector64D2Ev
	.globl	_ZN6icu_679UVector64D1Ev
	.set	_ZN6icu_679UVector64D1Ev,_ZN6icu_679UVector64D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector64D0Ev
	.type	_ZN6icu_679UVector64D0Ev, @function
_ZN6icu_679UVector64D0Ev:
.LFB2101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679UVector64E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	movq	$0, 24(%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2101:
	.size	_ZN6icu_679UVector64D0Ev, .-_ZN6icu_679UVector64D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector6416getStaticClassIDEv
	.type	_ZN6icu_679UVector6416getStaticClassIDEv, @function
_ZN6icu_679UVector6416getStaticClassIDEv:
.LFB2086:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_679UVector6416getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2086:
	.size	_ZN6icu_679UVector6416getStaticClassIDEv, .-_ZN6icu_679UVector6416getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector64C2ER10UErrorCode
	.type	_ZN6icu_679UVector64C2ER10UErrorCode, @function
_ZN6icu_679UVector64C2ER10UErrorCode:
.LFB2092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679UVector64E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	$64, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.L12
	movl	$8, 12(%rbx)
.L8:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L8
	.cfi_endproc
.LFE2092:
	.size	_ZN6icu_679UVector64C2ER10UErrorCode, .-_ZN6icu_679UVector64C2ER10UErrorCode
	.globl	_ZN6icu_679UVector64C1ER10UErrorCode
	.set	_ZN6icu_679UVector64C1ER10UErrorCode,_ZN6icu_679UVector64C2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector64C2EiR10UErrorCode
	.type	_ZN6icu_679UVector64C2EiR10UErrorCode, @function
_ZN6icu_679UVector64C2EiR10UErrorCode:
.LFB2095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679UVector64E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	testl	%esi, %esi
	jle	.L18
	movslq	%esi, %rdi
	movl	%esi, %r12d
	salq	$3, %rdi
	cmpl	$268435455, %esi
	jle	.L14
	movl	$8, %edi
	xorl	%esi, %esi
	call	uprv_min_67@PLT
	movslq	%eax, %rdi
	movq	%rdi, %r12
	salq	$3, %rdi
.L14:
	call	uprv_malloc_67@PLT
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.L20
	movl	%r12d, 12(%rbx)
.L13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movl	$64, %edi
	movl	$8, %r12d
	jmp	.L14
.L20:
	movl	$7, 0(%r13)
	jmp	.L13
	.cfi_endproc
.LFE2095:
	.size	_ZN6icu_679UVector64C2EiR10UErrorCode, .-_ZN6icu_679UVector64C2EiR10UErrorCode
	.globl	_ZN6icu_679UVector64C1EiR10UErrorCode
	.set	_ZN6icu_679UVector64C1EiR10UErrorCode,_ZN6icu_679UVector64C2EiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector645_initEiR10UErrorCode
	.type	_ZN6icu_679UVector645_initEiR10UErrorCode, @function
_ZN6icu_679UVector645_initEiR10UErrorCode:
.LFB2097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movl	16(%rdi), %esi
	testl	%ebx, %ebx
	jle	.L31
	testl	%esi, %esi
	jle	.L25
.L23:
	cmpl	%esi, %ebx
	cmovg	%esi, %ebx
.L25:
	cmpl	$268435455, %ebx
	jg	.L26
	movslq	%ebx, %rdi
	salq	$3, %rdi
.L24:
	call	uprv_malloc_67@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L32
	movl	%ebx, 12(%r12)
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	$8, %edi
	call	uprv_min_67@PLT
	movslq	%eax, %rdi
	movq	%rdi, %rbx
	salq	$3, %rdi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$8, %ebx
	movl	$64, %edi
	testl	%esi, %esi
	jg	.L23
	jmp	.L24
.L32:
	movl	$7, 0(%r13)
	jmp	.L21
	.cfi_endproc
.LFE2097:
	.size	_ZN6icu_679UVector645_initEiR10UErrorCode, .-_ZN6icu_679UVector645_initEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector64eqERKS0_
	.type	_ZN6icu_679UVector64eqERKS0_, @function
_ZN6icu_679UVector64eqERKS0_:
.LFB2103:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	xorl	%r8d, %r8d
	cmpl	8(%rsi), %eax
	jne	.L33
	testl	%eax, %eax
	jle	.L37
	movq	24(%rsi), %rcx
	movq	24(%rdi), %rdi
	leal	-1(%rax), %esi
	xorl	%eax, %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	1(%rax), %rdx
	cmpq	%rsi, %rax
	je	.L37
	movq	%rdx, %rax
.L35:
	movq	(%rcx,%rax,8), %rdx
	cmpq	%rdx, (%rdi,%rax,8)
	je	.L40
	xorl	%r8d, %r8d
.L33:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2103:
	.size	_ZN6icu_679UVector64eqERKS0_, .-_ZN6icu_679UVector64eqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector6412setElementAtEli
	.type	_ZN6icu_679UVector6412setElementAtEli, @function
_ZN6icu_679UVector6412setElementAtEli:
.LFB2104:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	js	.L41
	cmpl	%edx, 8(%rdi)
	jle	.L41
	movq	24(%rdi), %rax
	movslq	%edx, %rdx
	movq	%rsi, (%rax,%rdx,8)
.L41:
	ret
	.cfi_endproc
.LFE2104:
	.size	_ZN6icu_679UVector6412setElementAtEli, .-_ZN6icu_679UVector6412setElementAtEli
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode
	.type	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode, @function
_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode:
.LFB2105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	subq	$24, %rsp
	testl	%ebx, %ebx
	js	.L43
	movslq	8(%rdi), %rax
	movq	%rdi, %r12
	cmpl	%ebx, %eax
	jl	.L43
	movl	12(%rdi), %edx
	leal	1(%rax), %r14d
	movq	%rsi, %r13
	cmpl	%edx, %r14d
	jle	.L59
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L43
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jle	.L47
	cmpl	%eax, %r14d
	jg	.L60
	cmpl	$1073741823, %edx
	jg	.L49
	addl	%edx, %edx
	cmpl	%edx, %r14d
	cmovl	%edx, %r14d
	cmpl	%eax, %r14d
	cmovg	%eax, %r14d
.L53:
	cmpl	$268435455, %r14d
	jg	.L49
	movq	24(%r12), %rdi
	movslq	%r14d, %rsi
	movq	%rcx, -56(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L61
	movq	%rax, 24(%r12)
	movslq	8(%r12), %rax
	movl	%r14d, 12(%r12)
	leal	1(%rax), %r14d
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L59:
	movq	24(%rdi), %r15
.L46:
	cmpl	%eax, %ebx
	jge	.L52
	movl	%eax, %edx
	salq	$3, %rax
	subl	%ebx, %edx
	subl	$1, %edx
	movq	%rdx, %rdi
	leaq	8(,%rdx,8), %rdx
	negq	%rdi
	salq	$3, %rdi
	leaq	-8(%rax,%rdi), %rsi
	addq	%rax, %rdi
	addq	%r15, %rsi
	addq	%r15, %rdi
	call	memmove@PLT
.L52:
	movq	%r13, (%r15,%rbx,8)
	movl	%r14d, 8(%r12)
.L43:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	cmpl	$1073741823, %edx
	jg	.L49
	addl	%edx, %edx
	cmpl	%edx, %r14d
	cmovl	%edx, %r14d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$1, (%rcx)
	jmp	.L43
.L60:
	movl	$15, (%rcx)
	jmp	.L43
.L61:
	movl	$7, (%rcx)
	jmp	.L43
	.cfi_endproc
.LFE2105:
	.size	_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode, .-_ZN6icu_679UVector6415insertElementAtEliR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector6417removeAllElementsEv
	.type	_ZN6icu_679UVector6417removeAllElementsEv, @function
_ZN6icu_679UVector6417removeAllElementsEv:
.LFB2106:
	.cfi_startproc
	endbr64
	movl	$0, 8(%rdi)
	ret
	.cfi_endproc
.LFE2106:
	.size	_ZN6icu_679UVector6417removeAllElementsEv, .-_ZN6icu_679UVector6417removeAllElementsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode
	.type	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode, @function
_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode:
.LFB2107:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L78
	testl	%esi, %esi
	js	.L81
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movl	12(%rdi), %ebx
	cmpl	%ebx, %esi
	jle	.L63
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jle	.L66
	cmpl	%eax, %esi
	jg	.L82
	cmpl	$1073741823, %ebx
	jg	.L68
	addl	%ebx, %ebx
	cmpl	%ebx, %esi
	cmovl	%ebx, %esi
	cmpl	%eax, %esi
	cmovle	%esi, %eax
	movl	%eax, %ebx
.L70:
	cmpl	$268435455, %ebx
	jg	.L68
	movq	24(%r13), %rdi
	movslq	%ebx, %rsi
	movq	%rdx, -24(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-24(%rbp), %rdx
	testq	%rax, %rax
	je	.L83
	movl	%ebx, 12(%r13)
	movq	%rax, 24(%r13)
	movl	$1, %eax
.L63:
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 13
	movl	$1, (%rdx)
	xorl	%eax, %eax
.L78:
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 13, -24
	cmpl	$1073741823, %ebx
	jg	.L68
	addl	%ebx, %ebx
	cmpl	%ebx, %esi
	cmovge	%esi, %ebx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$1, (%rdx)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movl	$15, (%rdx)
	xorl	%eax, %eax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$7, (%rdx)
	jmp	.L63
	.cfi_endproc
.LFE2107:
	.size	_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode, .-_ZN6icu_679UVector6414expandCapacityEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector6414setMaxCapacityEi
	.type	_ZN6icu_679UVector6414setMaxCapacityEi, @function
_ZN6icu_679UVector6414setMaxCapacityEi:
.LFB2108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%esi, %esi
	js	.L93
	cmpl	$268435455, %esi
	jg	.L84
	movl	%esi, 16(%rdi)
	cmpl	%esi, 12(%rdi)
	jle	.L84
	testl	%esi, %esi
	je	.L84
	movslq	%esi, %rsi
	movq	24(%rdi), %rdi
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L84
	movq	%rax, 24(%rbx)
	movl	16(%rbx), %eax
	movl	%eax, 12(%rbx)
	cmpl	8(%rbx), %eax
	jge	.L84
	movl	%eax, 8(%rbx)
.L84:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movl	$0, 16(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2108:
	.size	_ZN6icu_679UVector6414setMaxCapacityEi, .-_ZN6icu_679UVector6414setMaxCapacityEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector647setSizeEi
	.type	_ZN6icu_679UVector647setSizeEi, @function
_ZN6icu_679UVector647setSizeEi:
.LFB2109:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L114
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movslq	8(%rdi), %rdx
	cmpl	%esi, %edx
	jge	.L102
	movl	12(%rdi), %eax
	cmpl	%eax, %esi
	jle	.L98
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jle	.L117
	cmpl	%edx, %esi
	jg	.L94
	cmpl	$1073741823, %eax
	jg	.L94
	leal	(%rax,%rax), %r13d
	cmpl	%r13d, %ebx
	cmovge	%ebx, %r13d
	cmpl	%edx, %r13d
	cmovg	%edx, %r13d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L98:
	movq	24(%rdi), %rax
.L106:
	leal	-1(%rbx), %ecx
	leaq	(%rax,%rdx,8), %rdi
	subl	%edx, %ecx
	cmpl	%edx, %ebx
	leaq	8(,%rcx,8), %r8
	movl	$8, %ecx
	cmovle	%rcx, %r8
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
.L102:
	movl	%ebx, 8(%r12)
.L94:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpl	$1073741823, %eax
	jg	.L94
	addl	%eax, %eax
	cmpl	%eax, %ebx
	cmovge	%ebx, %eax
	movl	%eax, %r13d
.L103:
	cmpl	$268435455, %r13d
	jg	.L94
	movslq	%r13d, %rsi
	movq	24(%r12), %rdi
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L94
	movslq	8(%r12), %rdx
	movq	%rax, 24(%r12)
	movl	%r13d, 12(%r12)
	cmpl	%edx, %ebx
	jg	.L106
	jmp	.L102
	.cfi_endproc
.LFE2109:
	.size	_ZN6icu_679UVector647setSizeEi, .-_ZN6icu_679UVector647setSizeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector646assignERKS0_R10UErrorCode
	.type	_ZN6icu_679UVector646assignERKS0_R10UErrorCode, @function
_ZN6icu_679UVector646assignERKS0_R10UErrorCode:
.LFB2102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movl	8(%rsi), %esi
	testl	%esi, %esi
	js	.L119
	movl	12(%rdi), %eax
	movq	%rdi, %rbx
	cmpl	%eax, %esi
	jle	.L120
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L118
	movl	16(%rbx), %r14d
	testl	%r14d, %r14d
	jle	.L123
	cmpl	%r14d, %esi
	jg	.L150
	cmpl	$1073741823, %eax
	jg	.L125
	addl	%eax, %eax
	cmpl	%eax, %esi
	cmovl	%eax, %esi
	cmpl	%r14d, %esi
	cmovle	%esi, %r14d
.L135:
	cmpl	$268435455, %r14d
	jg	.L125
	movq	24(%rbx), %rdi
	movslq	%r14d, %rsi
	movq	%rdx, -40(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-40(%rbp), %rdx
	testq	%rax, %rax
	je	.L151
	movq	%rax, 24(%rbx)
	movl	8(%r12), %esi
	movl	%r14d, 12(%rbx)
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%rbx, %rdi
	call	_ZN6icu_679UVector647setSizeEi
	movl	8(%r12), %edi
	testl	%edi, %edi
	jle	.L118
	movq	24(%r12), %rdx
	movq	24(%rbx), %rcx
	leal	-1(%rdi), %eax
	leaq	15(%rdx), %rsi
	subq	%rcx, %rsi
	cmpq	$30, %rsi
	jbe	.L128
	cmpl	$3, %eax
	jbe	.L128
	movl	%edi, %esi
	xorl	%eax, %eax
	shrl	%esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L130:
	movdqu	(%rdx,%rax), %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L130
	movl	%edi, %eax
	andl	$-2, %eax
	andl	$1, %edi
	je	.L118
	movq	(%rdx,%rax,8), %rdx
	movq	%rdx, (%rcx,%rax,8)
.L118:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L118
.L125:
	movl	$1, (%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	cmpl	$1073741823, %eax
	jg	.L125
	addl	%eax, %eax
	cmpl	%eax, %esi
	cmovge	%esi, %eax
	movl	%eax, %r14d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L128:
	movl	%eax, %esi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L134:
	movq	(%rdx,%rax,8), %rdi
	movq	%rdi, (%rcx,%rax,8)
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	%rdi, %rsi
	jne	.L134
	jmp	.L118
.L151:
	movl	$7, (%rdx)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$15, (%rdx)
	jmp	.L118
	.cfi_endproc
.LFE2102:
	.size	_ZN6icu_679UVector646assignERKS0_R10UErrorCode, .-_ZN6icu_679UVector646assignERKS0_R10UErrorCode
	.weak	_ZTSN6icu_679UVector64E
	.section	.rodata._ZTSN6icu_679UVector64E,"aG",@progbits,_ZTSN6icu_679UVector64E,comdat
	.align 16
	.type	_ZTSN6icu_679UVector64E, @object
	.size	_ZTSN6icu_679UVector64E, 20
_ZTSN6icu_679UVector64E:
	.string	"N6icu_679UVector64E"
	.weak	_ZTIN6icu_679UVector64E
	.section	.data.rel.ro._ZTIN6icu_679UVector64E,"awG",@progbits,_ZTIN6icu_679UVector64E,comdat
	.align 8
	.type	_ZTIN6icu_679UVector64E, @object
	.size	_ZTIN6icu_679UVector64E, 24
_ZTIN6icu_679UVector64E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_679UVector64E
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_679UVector64E
	.section	.data.rel.ro.local._ZTVN6icu_679UVector64E,"awG",@progbits,_ZTVN6icu_679UVector64E,comdat
	.align 8
	.type	_ZTVN6icu_679UVector64E, @object
	.size	_ZTVN6icu_679UVector64E, 40
_ZTVN6icu_679UVector64E:
	.quad	0
	.quad	_ZTIN6icu_679UVector64E
	.quad	_ZN6icu_679UVector64D1Ev
	.quad	_ZN6icu_679UVector64D0Ev
	.quad	_ZNK6icu_679UVector6417getDynamicClassIDEv
	.local	_ZZN6icu_679UVector6416getStaticClassIDEvE7classID
	.comm	_ZZN6icu_679UVector6416getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
