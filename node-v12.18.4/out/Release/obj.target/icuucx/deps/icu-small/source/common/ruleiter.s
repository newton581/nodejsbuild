	.file	"ruleiter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RuleCharacterIteratorC2ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE
	.type	_ZN6icu_6721RuleCharacterIteratorC2ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE, @function
_ZN6icu_6721RuleCharacterIteratorC2ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE:
.LFB1321:
	.cfi_startproc
	endbr64
	movq	%rsi, %xmm0
	movq	%rcx, %xmm1
	movq	%rdx, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 24(%rdi)
	movl	$0, 32(%rdi)
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE1321:
	.size	_ZN6icu_6721RuleCharacterIteratorC2ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE, .-_ZN6icu_6721RuleCharacterIteratorC2ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE
	.globl	_ZN6icu_6721RuleCharacterIteratorC1ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE
	.set	_ZN6icu_6721RuleCharacterIteratorC1ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE,_ZN6icu_6721RuleCharacterIteratorC2ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721RuleCharacterIterator5atEndEv
	.type	_ZNK6icu_6721RuleCharacterIterator5atEndEv, @function
_ZNK6icu_6721RuleCharacterIterator5atEndEv:
.LFB1323:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 24(%rdi)
	je	.L8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	8(%rdi), %rax
	movq	(%rdi), %rdx
	movl	8(%rax), %ecx
	movswl	8(%rdx), %eax
	testw	%ax, %ax
	js	.L5
	sarl	$5, %eax
.L6:
	cmpl	%eax, %ecx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	12(%rdx), %eax
	jmp	.L6
	.cfi_endproc
.LFE1323:
	.size	_ZNK6icu_6721RuleCharacterIterator5atEndEv, .-_ZNK6icu_6721RuleCharacterIterator5atEndEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode
	.type	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode, @function
_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -168(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L51
	movl	%esi, %eax
	movb	$0, (%rdx)
	movl	%esi, %r14d
	movq	%rdi, %r13
	andl	$1, %eax
	movl	%esi, %ebx
	movq	%rdx, %r12
	andl	$4, %r14d
	movl	%eax, -156(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -152(%rbp)
.L11:
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L12
	movl	32(%r13), %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r15d
.L13:
	xorl	%esi, %esi
	movq	24(%r13), %rdx
	cmpl	$65535, %r15d
	seta	%sil
	addl	$1, %esi
	testq	%rdx, %rdx
	je	.L64
	movswl	8(%rdx), %eax
	addl	32(%r13), %esi
	movl	%esi, 32(%r13)
	testw	%ax, %ax
	js	.L19
	sarl	$5, %eax
	cmpl	%eax, %esi
	je	.L65
.L21:
	testl	%r14d, %r14d
	je	.L37
	movl	%r15d, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L11
.L37:
	cmpl	$92, %r15d
	je	.L66
.L9:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$136, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	movq	0(%r13), %rdi
	movq	8(%r13), %rdx
	movswl	8(%rdi), %eax
	movl	8(%rdx), %r8d
	testw	%ax, %ax
	js	.L14
	sarl	$5, %eax
.L15:
	cmpl	%eax, %r8d
	jge	.L52
	movl	%r8d, %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r15d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	movl	12(%rdi), %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L19:
	movl	12(%rdx), %eax
	cmpl	%eax, %esi
	jne	.L21
.L65:
	movq	$0, 24(%r13)
	cmpl	$36, %r15d
	jne	.L21
.L69:
	movl	-156(%rbp), %edx
	testl	%edx, %edx
	je	.L21
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.L21
	movq	(%rsi), %rax
	movq	0(%r13), %rdx
	movq	32(%rax), %r11
	movzwl	8(%rdx), %eax
	testw	%ax, %ax
	js	.L25
	movswl	%ax, %r8d
	sarl	$5, %r8d
.L26:
	movq	8(%r13), %rcx
	movq	-152(%rbp), %rdi
	call	*%r11
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L27
	sarl	$5, %eax
.L28:
	testl	%eax, %eax
	je	.L29
	movq	16(%r13), %rdi
	movl	$0, 32(%r13)
	movq	-152(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, 24(%r13)
	testq	%rax, %rax
	je	.L68
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L31
	sarl	$5, %edx
.L32:
	testl	%edx, %edx
	jne	.L33
	movq	$0, 24(%r13)
.L33:
	movq	-152(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L64:
	movq	8(%r13), %rdx
	movq	0(%r13), %rdi
	movl	8(%rdx), %r8d
.L16:
	addl	%r8d, %esi
	movl	%esi, 8(%rdx)
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L23
	sarl	$5, %eax
.L24:
	cmpl	%eax, %esi
	jle	.L22
	movl	%eax, 8(%rdx)
.L22:
	cmpl	$36, %r15d
	jne	.L21
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L66:
	andl	$2, %ebx
	je	.L9
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	24(%r13), %rcx
	movl	$0, -132(%rbp)
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	testq	%rcx, %rcx
	je	.L38
	leaq	-128(%rbp), %r14
	movl	32(%r13), %r8d
	movl	$12, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
.L39:
	leaq	-132(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString10unescapeAtERi@PLT
	movq	24(%r13), %rcx
	movl	%eax, %r15d
	movl	-132(%rbp), %eax
	testq	%rcx, %rcx
	je	.L40
	movswl	8(%rcx), %edx
	addl	32(%r13), %eax
	movl	%eax, 32(%r13)
	testw	%dx, %dx
	js	.L41
	sarl	$5, %edx
.L42:
	cmpl	%edx, %eax
	jne	.L44
	movq	$0, 24(%r13)
.L44:
	movb	$1, (%r12)
	testl	%r15d, %r15d
	js	.L70
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L23:
	movl	12(%rdi), %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	movl	12(%rdx), %r8d
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L40:
	movq	8(%r13), %rcx
	movq	0(%r13), %rsi
	addl	8(%rcx), %eax
	movl	%eax, 8(%rcx)
	movswl	8(%rsi), %edx
	testw	%dx, %dx
	js	.L45
	sarl	$5, %edx
.L46:
	cmpl	%edx, %eax
	jle	.L44
	movl	%edx, 8(%rcx)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L38:
	movq	8(%r13), %rax
	leaq	-128(%rbp), %r14
	movq	0(%r13), %rcx
	xorl	%edx, %edx
	movl	$12, %r9d
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	8(%rax), %r8d
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L41:
	movl	12(%rcx), %edx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$-1, %r15d
	movl	$2, %esi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L27:
	movl	-116(%rbp), %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L31:
	movl	12(%rax), %edx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L45:
	movl	12(%rsi), %edx
	jmp	.L46
.L51:
	movl	$-1, %r15d
	jmp	.L9
.L29:
	movq	-152(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L9
.L70:
	movq	-168(%rbp), %rax
	movq	%r14, %rdi
	movl	$-1, %r15d
	movl	$65540, (%rax)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L9
.L67:
	call	__stack_chk_fail@PLT
.L68:
	movq	-168(%rbp), %rax
	movq	-152(%rbp), %rdi
	orl	$-1, %r15d
	movl	$65554, (%rax)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L9
	.cfi_endproc
.LFE1324:
	.size	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode, .-_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721RuleCharacterIterator6getPosERNS0_3PosE
	.type	_ZNK6icu_6721RuleCharacterIterator6getPosERNS0_3PosE, @function
_ZNK6icu_6721RuleCharacterIterator6getPosERNS0_3PosE:
.LFB1325:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	%rax, (%rsi)
	movq	8(%rdi), %rax
	movl	8(%rax), %eax
	movl	%eax, 8(%rsi)
	movl	32(%rdi), %eax
	movl	%eax, 12(%rsi)
	ret
	.cfi_endproc
.LFE1325:
	.size	_ZNK6icu_6721RuleCharacterIterator6getPosERNS0_3PosE, .-_ZNK6icu_6721RuleCharacterIterator6getPosERNS0_3PosE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RuleCharacterIterator6setPosERKNS0_3PosE
	.type	_ZN6icu_6721RuleCharacterIterator6setPosERKNS0_3PosE, @function
_ZN6icu_6721RuleCharacterIterator6setPosERKNS0_3PosE:
.LFB1326:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	8(%rsi), %edx
	movq	%rax, 24(%rdi)
	movq	8(%rdi), %rax
	movl	%edx, 8(%rax)
	movl	12(%rsi), %eax
	movl	%eax, 32(%rdi)
	ret
	.cfi_endproc
.LFE1326:
	.size	_ZN6icu_6721RuleCharacterIterator6setPosERKNS0_3PosE, .-_ZN6icu_6721RuleCharacterIterator6setPosERKNS0_3PosE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RuleCharacterIterator11skipIgnoredEi
	.type	_ZN6icu_6721RuleCharacterIterator11skipIgnoredEi, @function
_ZN6icu_6721RuleCharacterIterator11skipIgnoredEi:
.LFB1327:
	.cfi_startproc
	endbr64
	andl	$4, %esi
	je	.L103
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rdi
.L84:
	testq	%rdi, %rdi
	je	.L106
	movl	32(%r12), %esi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %ebx
	movl	%ebx, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	je	.L73
.L107:
	movl	$1, %edx
	cmpl	$65535, %ebx
	jbe	.L80
.L87:
	movl	$2, %edx
.L80:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L81
	movswl	8(%rdi), %eax
	addl	32(%r12), %edx
	movl	%edx, 32(%r12)
	testw	%ax, %ax
	js	.L82
	sarl	$5, %eax
.L83:
	cmpl	%eax, %edx
	jne	.L84
	movq	$0, 24(%r12)
.L106:
	movq	8(%r12), %rcx
	movq	(%r12), %rdi
.L75:
	movswl	8(%rdi), %eax
	movl	8(%rcx), %esi
	testw	%ax, %ax
	js	.L77
.L108:
	sarl	$5, %eax
.L78:
	cmpl	%eax, %esi
	jge	.L79
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %ebx
	movl	%ebx, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L107
.L73:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	8(%r12), %rcx
	movq	(%r12), %rdi
	addl	8(%rcx), %edx
	movl	%edx, 8(%rcx)
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L85
	sarl	$5, %eax
.L86:
	cmpl	%eax, %edx
	jle	.L75
	movl	%eax, 8(%rcx)
	movswl	8(%rdi), %eax
	movl	8(%rcx), %esi
	testw	%ax, %ax
	jns	.L108
.L77:
	movl	12(%rdi), %eax
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L82:
	movl	12(%rdi), %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	12(%rdi), %eax
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$-1, %edi
	call	_ZN6icu_6712PatternProps12isWhiteSpaceEi@PLT
	testb	%al, %al
	jne	.L87
	jmp	.L73
	.cfi_endproc
.LFE1327:
	.size	_ZN6icu_6721RuleCharacterIterator11skipIgnoredEi, .-_ZN6icu_6721RuleCharacterIterator11skipIgnoredEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721RuleCharacterIterator9lookaheadERNS_13UnicodeStringEi
	.type	_ZNK6icu_6721RuleCharacterIterator9lookaheadERNS_13UnicodeStringEi, @function
_ZNK6icu_6721RuleCharacterIterator9lookaheadERNS_13UnicodeStringEi:
.LFB1328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movl	$2147483647, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	24(%rdi), %rcx
	testl	%edx, %edx
	movswl	8(%rsi), %edx
	cmovs	%eax, %r9d
	testq	%rcx, %rcx
	je	.L111
	movl	32(%rdi), %r8d
	testw	%dx, %dx
	js	.L115
.L118:
	sarl	$5, %edx
.L116:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	(%rdi), %rcx
	movl	8(%rax), %r8d
	testw	%dx, %dx
	jns	.L118
.L115:
	movl	12(%r12), %edx
	jmp	.L116
	.cfi_endproc
.LFE1328:
	.size	_ZNK6icu_6721RuleCharacterIterator9lookaheadERNS_13UnicodeStringEi, .-_ZNK6icu_6721RuleCharacterIterator9lookaheadERNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RuleCharacterIterator9jumpaheadEi
	.type	_ZN6icu_6721RuleCharacterIterator9jumpaheadEi, @function
_ZN6icu_6721RuleCharacterIterator9jumpaheadEi:
.LFB1329:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L120
	movswl	8(%rdx), %eax
	addl	32(%rdi), %esi
	movl	%esi, 32(%rdi)
	testw	%ax, %ax
	js	.L121
	sarl	$5, %eax
	cmpl	%eax, %esi
	je	.L128
.L119:
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	movl	12(%rdx), %eax
	cmpl	%eax, %esi
	jne	.L119
.L128:
	movq	$0, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	movq	8(%rdi), %rdx
	movq	(%rdi), %rcx
	addl	8(%rdx), %esi
	movl	%esi, 8(%rdx)
	movswl	8(%rcx), %eax
	testw	%ax, %ax
	js	.L125
	sarl	$5, %eax
.L126:
	cmpl	%eax, %esi
	jle	.L119
	movl	%eax, 8(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	movl	12(%rcx), %eax
	jmp	.L126
	.cfi_endproc
.LFE1329:
	.size	_ZN6icu_6721RuleCharacterIterator9jumpaheadEi, .-_ZN6icu_6721RuleCharacterIterator9jumpaheadEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6721RuleCharacterIterator8_currentEv
	.type	_ZNK6icu_6721RuleCharacterIterator8_currentEv, @function
_ZNK6icu_6721RuleCharacterIterator8_currentEv:
.LFB1330:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %r8
	testq	%r8, %r8
	je	.L130
	movl	32(%rdi), %esi
	movq	%r8, %rdi
	jmp	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	.p2align 4,,10
	.p2align 3
.L130:
	movq	8(%rdi), %rax
	movq	(%rdi), %rdi
	movl	8(%rax), %esi
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L131
	sarl	$5, %eax
.L132:
	cmpl	%eax, %esi
	jge	.L133
	jmp	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	.p2align 4,,10
	.p2align 3
.L131:
	movl	12(%rdi), %eax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L133:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE1330:
	.size	_ZNK6icu_6721RuleCharacterIterator8_currentEv, .-_ZNK6icu_6721RuleCharacterIterator8_currentEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721RuleCharacterIterator8_advanceEi
	.type	_ZN6icu_6721RuleCharacterIterator8_advanceEi, @function
_ZN6icu_6721RuleCharacterIterator8_advanceEi:
.LFB1331:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L135
	movswl	8(%rdx), %eax
	addl	32(%rdi), %esi
	movl	%esi, 32(%rdi)
	testw	%ax, %ax
	js	.L136
	sarl	$5, %eax
	cmpl	%eax, %esi
	je	.L143
.L134:
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	movl	12(%rdx), %eax
	cmpl	%eax, %esi
	jne	.L134
.L143:
	movq	$0, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	movq	8(%rdi), %rdx
	movq	(%rdi), %rcx
	addl	8(%rdx), %esi
	movl	%esi, 8(%rdx)
	movswl	8(%rcx), %eax
	testw	%ax, %ax
	js	.L140
	sarl	$5, %eax
.L141:
	cmpl	%eax, %esi
	jle	.L134
	movl	%eax, 8(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	movl	12(%rcx), %eax
	jmp	.L141
	.cfi_endproc
.LFE1331:
	.size	_ZN6icu_6721RuleCharacterIterator8_advanceEi, .-_ZN6icu_6721RuleCharacterIterator8_advanceEi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
