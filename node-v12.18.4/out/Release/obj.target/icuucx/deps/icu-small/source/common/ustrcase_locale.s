	.file	"ustrcase_locale.cpp"
	.text
	.p2align 4
	.globl	ustrcase_getCaseLocale_67
	.type	ustrcase_getCaseLocale_67, @function
ustrcase_getCaseLocale_67:
.LFB3013:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L11
	cmpb	$0, (%rdi)
	jne	.L12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	jmp	ucase_getCaseLocale_67@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uloc_getDefault_67@PLT
	cmpb	$0, (%rax)
	movq	%rax, %rdi
	je	.L3
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ucase_getCaseLocale_67@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3013:
	.size	ustrcase_getCaseLocale_67, .-ustrcase_getCaseLocale_67
	.p2align 4
	.globl	u_strToLower_67
	.type	u_strToLower_67, @function
u_strToLower_67:
.LFB3014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	testq	%r8, %r8
	je	.L18
.L14:
	cmpb	$0, (%r8)
	movl	$1, %r10d
	je	.L15
	movq	%r8, %rdi
	call	ucase_getCaseLocale_67@PLT
	movl	%eax, %r10d
.L15:
	subq	$8, %rsp
	movq	%r13, %r9
	movl	%ebx, %r8d
	movq	%r12, %rcx
	pushq	%r15
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%r10d, %edi
	pushq	ustrcase_internalToLower_67@GOTPCREL(%rip)
	pushq	%r14
	call	ustrcase_mapWithOverlap_67@PLT
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	call	uloc_getDefault_67@PLT
	movq	%rax, %r8
	jmp	.L14
	.cfi_endproc
.LFE3014:
	.size	u_strToLower_67, .-u_strToLower_67
	.p2align 4
	.globl	u_strToUpper_67
	.type	u_strToUpper_67, @function
u_strToUpper_67:
.LFB3015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	testq	%r8, %r8
	je	.L24
.L20:
	cmpb	$0, (%r8)
	movl	$1, %r10d
	je	.L21
	movq	%r8, %rdi
	call	ucase_getCaseLocale_67@PLT
	movl	%eax, %r10d
.L21:
	subq	$8, %rsp
	movq	%r13, %r9
	movl	%ebx, %r8d
	movq	%r12, %rcx
	pushq	%r15
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%r10d, %edi
	pushq	ustrcase_internalToUpper_67@GOTPCREL(%rip)
	pushq	%r14
	call	ustrcase_mapWithOverlap_67@PLT
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	call	uloc_getDefault_67@PLT
	movq	%rax, %r8
	jmp	.L20
	.cfi_endproc
.LFE3015:
	.size	u_strToUpper_67, .-u_strToUpper_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap7toLowerEPKcjPKDsiPDsiPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap7toLowerEPKcjPKDsiPDsiPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap7toLowerEPKcjPKDsiPDsiPNS_5EditsER10UErrorCode:
.LFB3016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L30
.L26:
	cmpb	$0, (%rdi)
	movl	$1, %r10d
	je	.L27
	call	ucase_getCaseLocale_67@PLT
	movl	%eax, %r10d
.L27:
	pushq	24(%rbp)
	movq	%r14, %r9
	movl	%ebx, %r8d
	movq	%r13, %rcx
	pushq	16(%rbp)
	movl	%r12d, %esi
	xorl	%edx, %edx
	movl	%r10d, %edi
	pushq	ustrcase_internalToLower_67@GOTPCREL(%rip)
	pushq	%r15
	call	ustrcase_map_67@PLT
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	call	uloc_getDefault_67@PLT
	movq	%rax, %rdi
	jmp	.L26
	.cfi_endproc
.LFE3016:
	.size	_ZN6icu_677CaseMap7toLowerEPKcjPKDsiPDsiPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap7toLowerEPKcjPKDsiPDsiPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap7toUpperEPKcjPKDsiPDsiPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap7toUpperEPKcjPKDsiPDsiPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap7toUpperEPKcjPKDsiPDsiPNS_5EditsER10UErrorCode:
.LFB3017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L36
.L32:
	cmpb	$0, (%rdi)
	movl	$1, %r10d
	je	.L33
	call	ucase_getCaseLocale_67@PLT
	movl	%eax, %r10d
.L33:
	pushq	24(%rbp)
	movq	%r14, %r9
	movl	%ebx, %r8d
	movq	%r13, %rcx
	pushq	16(%rbp)
	movl	%r12d, %esi
	xorl	%edx, %edx
	movl	%r10d, %edi
	pushq	ustrcase_internalToUpper_67@GOTPCREL(%rip)
	pushq	%r15
	call	ustrcase_map_67@PLT
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	call	uloc_getDefault_67@PLT
	movq	%rax, %rdi
	jmp	.L32
	.cfi_endproc
.LFE3017:
	.size	_ZN6icu_677CaseMap7toUpperEPKcjPKDsiPDsiPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap7toUpperEPKcjPKDsiPDsiPNS_5EditsER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
