	.file	"ustr_wcs.cpp"
	.text
	.p2align 4
	.globl	u_strToWCS_67
	.type	u_strToWCS_67, @function
u_strToWCS_67:
.LFB2094:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L1
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L1
	testq	%rcx, %rcx
	jne	.L8
	testl	%r8d, %r8d
	jne	.L3
.L8:
	cmpl	$-1, %r8d
	jl	.L3
	testl	%esi, %esi
	js	.L3
	testq	%rdi, %rdi
	jne	.L5
	testl	%esi, %esi
	jg	.L3
.L5:
	jmp	u_strToUTF32_67@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$1, (%r9)
.L1:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2094:
	.size	u_strToWCS_67, .-u_strToWCS_67
	.p2align 4
	.globl	u_strFromWCS_67
	.type	u_strFromWCS_67, @function
u_strFromWCS_67:
.LFB2095:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L20
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L20
	testq	%rcx, %rcx
	jne	.L27
	testl	%r8d, %r8d
	jne	.L22
.L27:
	cmpl	$-1, %r8d
	jl	.L22
	testl	%esi, %esi
	js	.L22
	testq	%rdi, %rdi
	jne	.L24
	testl	%esi, %esi
	jg	.L22
.L24:
	jmp	u_strFromUTF32_67@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1, (%r9)
.L20:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2095:
	.size	u_strFromWCS_67, .-u_strFromWCS_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
