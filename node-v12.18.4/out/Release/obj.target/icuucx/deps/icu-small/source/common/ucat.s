	.file	"ucat.cpp"
	.text
	.p2align 4
	.globl	u_catopen_67
	.type	u_catopen_67, @function
u_catopen_67:
.LFB2325:
	.cfi_startproc
	endbr64
	jmp	ures_open_67@PLT
	.cfi_endproc
.LFE2325:
	.size	u_catopen_67, .-u_catopen_67
	.p2align 4
	.globl	u_catclose_67
	.type	u_catclose_67, @function
u_catclose_67:
.LFB2326:
	.cfi_startproc
	endbr64
	jmp	ures_close_67@PLT
	.cfi_endproc
.LFE2326:
	.size	u_catclose_67, .-u_catclose_67
	.p2align 4
	.globl	u_catgets_67
	.type	u_catgets_67, @function
u_catgets_67:
.LFB2327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L8
	movl	(%r9), %ecx
	movq	%r9, %rbx
	testl	%ecx, %ecx
	jle	.L11
.L8:
	testq	%r13, %r13
	je	.L7
	movq	%r12, %rdi
	call	u_strlen_67@PLT
	movl	%eax, 0(%r13)
.L7:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	leaq	-80(%rbp), %r8
	movq	%rdi, %r14
	movl	%edx, %r15d
	movl	$10, %edx
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	T_CString_integerToString_67@PLT
	movq	-88(%rbp), %r8
	movl	%r15d, %esi
	leal	1(%rax), %edi
	movslq	%eax, %rdx
	movslq	%edi, %rdi
	movb	$37, -80(%rbp,%rdx)
	movl	$10, %edx
	addq	%r8, %rdi
	call	T_CString_integerToString_67@PLT
	movq	-88(%rbp), %r8
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	ures_getStringByKey_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L8
	movq	%rax, %r12
	jmp	.L7
.L12:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2327:
	.size	u_catgets_67, .-u_catgets_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
