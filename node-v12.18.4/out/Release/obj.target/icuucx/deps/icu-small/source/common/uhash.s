	.file	"uhash.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	_ZL11_uhash_findPK10UHashtable8UElementi, @function
_ZL11_uhash_findPK10UHashtable8UElementi:
.LFB2080:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$2147483647, %edx
	movq	%rdi, %r10
	movq	$-1, %r9
	movl	%edx, %eax
	xorl	$67108864, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	cltd
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movl	52(%rdi), %esi
	movq	(%rdi), %rcx
	idivl	%esi
	movl	%edx, %r8d
	movslq	%edx, %rbx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L2:
	testl	%r13d, %r13d
	js	.L17
.L4:
	testl	%r12d, %r12d
	jne	.L6
	movl	%r15d, %eax
	leal	-1(%rsi), %edi
	cltd
	idivl	%edi
	leal	1(%rdx), %r12d
.L6:
	leal	(%rbx,%r12), %eax
	cltd
	idivl	%esi
	movslq	%edx, %rbx
	cmpl	%ebx, %r8d
	je	.L18
.L9:
	movslq	%ebx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rcx,%rax,8), %r14
	movl	(%r14), %r13d
	cmpl	%r13d, %r15d
	jne	.L2
	movl	%r8d, -64(%rbp)
	movq	16(%r14), %rsi
	movq	%rcx, -72(%rbp)
	movq	-80(%rbp), %rdi
	movl	%r9d, -60(%rbp)
	movq	%r10, -56(%rbp)
	call	*16(%r10)
	testb	%al, %al
	jne	.L1
	movq	-56(%rbp), %r10
	movslq	-60(%rbp), %r9
	movq	-72(%rbp), %rcx
	movl	-64(%rbp), %r8d
	movl	52(%r10), %esi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	$-2147483647, %r13d
	je	.L5
	testl	%r9d, %r9d
	cmovs	%rbx, %r9
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L18:
	testl	%r9d, %r9d
	jns	.L13
	cmpl	$-2147483647, %r13d
	jne	.L12
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rcx,%rax,8), %r14
.L1:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	testl	%r9d, %r9d
	js	.L1
.L13:
	leaq	(%r9,%r9,2), %rax
	leaq	(%rcx,%rax,8), %r14
	jmp	.L1
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZL11_uhash_findPK10UHashtable8UElementi.cold, @function
_ZL11_uhash_findPK10UHashtable8UElementi.cold:
.LFSB2080:
.L12:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE2080:
	.text
	.size	_ZL11_uhash_findPK10UHashtable8UElementi, .-_ZL11_uhash_findPK10UHashtable8UElementi
	.section	.text.unlikely
	.size	_ZL11_uhash_findPK10UHashtable8UElementi.cold, .-_ZL11_uhash_findPK10UHashtable8UElementi.cold
.LCOLDE0:
	.text
.LHOTE0:
	.p2align 4
	.type	_ZL13_uhash_rehashP10UHashtableP10UErrorCode, @function
_ZL13_uhash_rehashP10UHashtableP10UErrorCode:
.LFB2081:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movsbl	72(%rdi), %eax
	movl	48(%rdi), %edx
	cmpl	56(%rdi), %edx
	jle	.L20
	addl	$1, %eax
	cmpl	$28, %eax
	jg	.L19
	movl	(%rbx), %edx
	movq	(%r12), %r13
	movl	52(%r12), %r15d
	testl	%edx, %edx
	jle	.L36
.L25:
	movq	%r13, (%r12)
	movl	%r15d, 52(%r12)
.L19:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	cmpl	60(%rdi), %edx
	jge	.L19
	subl	$1, %eax
	js	.L19
	movl	(%rbx), %edx
	movq	(%r12), %r13
	movl	52(%r12), %r15d
	testl	%edx, %edx
	jg	.L25
.L36:
	movb	%al, 72(%r12)
	leaq	_ZL6PRIMES(%rip), %rdx
	cltq
	movslq	(%rdx,%rax,4), %rax
	movl	%eax, 52(%r12)
	leaq	(%rax,%rax,2), %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L37
	movslq	52(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%rax,%rsi,8), %rsi
	cmpq	%rsi, %rax
	jnb	.L28
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%rdx, 16(%rax)
	addq	$24, %rax
	movq	%rdx, -16(%rax)
	movl	$-2147483647, -24(%rax)
	cmpq	%rax, %rsi
	ja	.L26
.L28:
	pxor	%xmm0, %xmm0
	movss	68(%r12), %xmm1
	movl	$0, 48(%r12)
	cvtsi2ssl	%ecx, %xmm0
	mulss	%xmm0, %xmm1
	mulss	64(%r12), %xmm0
	cvttss2sil	%xmm1, %eax
	movl	%eax, 60(%r12)
	cvttss2sil	%xmm0, %eax
	movl	%eax, 56(%r12)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L25
	testl	%r15d, %r15d
	jle	.L31
	movslq	%r15d, %rax
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	leaq	-48(%r13,%rax), %r14
	leaq	-24(%r13,%rax), %rbx
	leal	-1(%r15), %eax
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	subq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L32:
	movl	(%rbx), %edx
	testl	%edx, %edx
	js	.L30
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rax)
	movl	(%rbx), %edx
	movl	%edx, (%rax)
	addl	$1, 48(%r12)
.L30:
	subq	$24, %rbx
	cmpq	%r14, %rbx
	jne	.L32
.L31:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
.L37:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L25
	.cfi_endproc
.LFE2081:
	.size	_ZL13_uhash_rehashP10UHashtableP10UErrorCode, .-_ZL13_uhash_rehashP10UHashtableP10UErrorCode
	.p2align 4
	.type	_ZL13_uhash_removeP10UHashtable8UElement, @function
_ZL13_uhash_removeP10UHashtable8UElement:
.LFB2082:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movq	%rax, %rbx
	xorl	%eax, %eax
	movl	(%rbx), %edx
	testl	%edx, %edx
	jns	.L65
.L40:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L66
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	32(%r12), %rax
	subl	$1, 48(%r12)
	movq	8(%rbx), %r13
	testq	%rax, %rax
	je	.L41
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	*%rax
.L41:
	movq	40(%r12), %rax
	testq	%rax, %rax
	je	.L42
	testq	%r13, %r13
	je	.L45
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*%rax
.L42:
	pxor	%xmm0, %xmm0
	movl	$-2147483648, (%rbx)
	movl	60(%r12), %ecx
	movq	%r13, %rax
	movups	%xmm0, 8(%rbx)
	cmpl	%ecx, 48(%r12)
	jge	.L40
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -56(%rbp)
	movl	$0, -44(%rbp)
	call	_ZL13_uhash_rehashP10UHashtableP10UErrorCode
	movq	-56(%rbp), %rax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%r13d, %r13d
	jmp	.L42
.L66:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2082:
	.size	_ZL13_uhash_removeP10UHashtable8UElement, .-_ZL13_uhash_removeP10UHashtable8UElement
	.p2align 4
	.globl	uhash_open_67
	.type	uhash_open_67, @function
uhash_open_67:
.LFB2084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%rsi, -40(%rbp)
	movl	(%rcx), %esi
	testl	%esi, %esi
	jg	.L67
	movq	%rdi, %r13
	movl	$80, %edi
	movq	%rdx, %rbx
	movq	%rcx, %r14
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L78
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L70
	movl	$4, %edx
	movq	%rbx, 24(%rax)
	movq	%r13, %xmm0
	movl	$3048, %edi
	movq	$0, 32(%rax)
	movhps	-40(%rbp), %xmm0
	movq	$0, 40(%rax)
	movq	$1056964608, 64(%rax)
	movw	%dx, 72(%rax)
	movl	$127, 52(%rax)
	movups	%xmm0, 8(%rax)
	call	uprv_malloc_67@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L79
	movslq	52(%r12), %rcx
	xorl	%esi, %esi
	movq	%rcx, %rdx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rdi
	cmpq	%rdi, %rax
	jnb	.L74
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%rsi, 16(%rax)
	addq	$24, %rax
	movq	%rsi, -16(%rax)
	movl	$-2147483647, -24(%rax)
	cmpq	%rax, %rdi
	ja	.L73
.L74:
	pxor	%xmm0, %xmm0
	movss	68(%r12), %xmm1
	movl	$0, 48(%r12)
	cvtsi2ssl	%edx, %xmm0
	movb	$1, 73(%r12)
	mulss	%xmm0, %xmm1
	mulss	64(%r12), %xmm0
	cvttss2sil	%xmm1, %eax
	movl	%eax, 60(%r12)
	cvttss2sil	%xmm0, %eax
	movl	%eax, 56(%r12)
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L72
.L67:
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L79:
	.cfi_restore_state
	movl	$7, (%r14)
	movb	$1, 73(%r12)
.L72:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L70:
	movb	$1, 73(%rax)
	jmp	.L72
.L78:
	movl	$7, (%r14)
	jmp	.L67
	.cfi_endproc
.LFE2084:
	.size	uhash_open_67, .-uhash_open_67
	.p2align 4
	.globl	uhash_openSize_67
	.type	uhash_openSize_67, @function
uhash_openSize_67:
.LFB2085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$7, %ebx
	subq	$24, %rsp
	movq	%rsi, -64(%rbp)
	leaq	_ZL6PRIMES(%rip), %rsi
	movq	%rdi, -56(%rbp)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$1, %rax
	cmpq	$28, %rax
	je	.L90
	movslq	(%rsi,%rax,4), %rbx
.L82:
	movl	%eax, %r13d
	cmpl	%ebx, %ecx
	jg	.L94
.L81:
	movl	(%r15), %ecx
	xorl	%r14d, %r14d
	testl	%ecx, %ecx
	jg	.L80
	movl	$80, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L95
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L85
	movq	-56(%rbp), %xmm0
	movq	%r12, 24(%rax)
	leaq	(%rbx,%rbx,2), %rdi
	movq	$0, 32(%rax)
	salq	$3, %rdi
	movq	$0, 40(%rax)
	movhps	-64(%rbp), %xmm0
	movb	$0, 73(%rax)
	movq	$1056964608, 64(%rax)
	movb	%r13b, 72(%rax)
	movl	%ebx, 52(%rax)
	movups	%xmm0, 8(%rax)
	call	uprv_malloc_67@PLT
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L96
	movslq	52(%r14), %rsi
	xorl	%ecx, %ecx
	movq	%rsi, %rdx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%rax,%rsi,8), %rsi
	cmpq	%rsi, %rax
	jnb	.L89
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%rcx, 16(%rax)
	addq	$24, %rax
	movq	%rcx, -16(%rax)
	movl	$-2147483647, -24(%rax)
	cmpq	%rax, %rsi
	ja	.L88
.L89:
	pxor	%xmm0, %xmm0
	movss	68(%r14), %xmm1
	movb	$1, 73(%r14)
	cvtsi2ssl	%edx, %xmm0
	movl	$0, 48(%r14)
	mulss	%xmm0, %xmm1
	mulss	64(%r14), %xmm0
	cvttss2sil	%xmm1, %eax
	movl	%eax, 60(%r14)
	cvttss2sil	%xmm0, %eax
	movl	%eax, 56(%r14)
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L87
.L80:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	$2147483647, %ebx
	movl	$28, %r13d
	jmp	.L81
.L96:
	movl	$7, (%r15)
	movb	$1, 73(%r14)
.L87:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	uprv_free_67@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L85:
	movb	$1, 73(%rax)
	jmp	.L87
.L95:
	movl	$7, (%r15)
	jmp	.L80
	.cfi_endproc
.LFE2085:
	.size	uhash_openSize_67, .-uhash_openSize_67
	.p2align 4
	.globl	uhash_init_67
	.type	uhash_init_67, @function
uhash_init_67:
.LFB2086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%r8), %r9d
	movq	%rsi, -24(%rbp)
	movq	%rdx, -32(%rbp)
	testl	%r9d, %r9d
	jg	.L98
	movl	$4, %edx
	movq	%rcx, 24(%rdi)
	movq	%rdi, %rbx
	movq	%r8, %r12
	movq	$0, 32(%rdi)
	movq	%rsi, %xmm0
	movq	$0, 40(%rdi)
	movhps	-32(%rbp), %xmm0
	movq	$1056964608, 64(%rdi)
	movw	%dx, 72(%rdi)
	movl	$127, 52(%rdi)
	movups	%xmm0, 8(%rdi)
	movl	$3048, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L106
	movslq	52(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%rsi, %rdx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%rax,%rsi,8), %rsi
	cmpq	%rsi, %rax
	jnb	.L101
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rcx, 16(%rax)
	addq	$24, %rax
	movq	%rcx, -16(%rax)
	movl	$-2147483647, -24(%rax)
	cmpq	%rax, %rsi
	ja	.L102
.L101:
	pxor	%xmm0, %xmm0
	movss	68(%rbx), %xmm1
	movl	$0, 48(%rbx)
	cvtsi2ssl	%edx, %xmm0
	mulss	%xmm0, %xmm1
	mulss	64(%rbx), %xmm0
	cvttss2sil	%xmm1, %eax
	movl	%eax, 60(%rbx)
	cvttss2sil	%xmm0, %eax
	movl	%eax, 56(%rbx)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L98
	addq	$16, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L106:
	.cfi_restore_state
	movl	$7, (%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2086:
	.size	uhash_init_67, .-uhash_init_67
	.p2align 4
	.globl	uhash_initSize_67
	.type	uhash_initSize_67, @function
uhash_initSize_67:
.LFB2087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	_ZL6PRIMES(%rip), %rdi
	subq	$16, %rsp
	movq	%rdx, -32(%rbp)
	movl	$7, %edx
	movq	%rsi, -24(%rbp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L119:
	addq	$1, %rax
	cmpq	$28, %rax
	je	.L116
	movslq	(%rdi,%rax,4), %rdx
.L109:
	movl	%eax, %esi
	cmpl	%edx, %r8d
	jg	.L119
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L114
.L110:
	movq	-24(%rbp), %xmm0
	movq	%rcx, 24(%rbx)
	leaq	(%rdx,%rdx,2), %rdi
	movq	$0, 32(%rbx)
	salq	$3, %rdi
	movq	$0, 40(%rbx)
	movhps	-32(%rbp), %xmm0
	movb	$0, 73(%rbx)
	movq	$1056964608, 64(%rbx)
	movb	%sil, 72(%rbx)
	movl	%edx, 52(%rbx)
	movups	%xmm0, 8(%rbx)
	call	uprv_malloc_67@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L120
	movslq	52(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%rax,%rsi,8), %rsi
	cmpq	%rsi, %rax
	jnb	.L115
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%rdx, 16(%rax)
	addq	$24, %rax
	movq	%rdx, -16(%rax)
	movl	$-2147483647, -24(%rax)
	cmpq	%rax, %rsi
	ja	.L113
.L115:
	pxor	%xmm0, %xmm0
	movss	68(%rbx), %xmm1
	movl	$0, 48(%rbx)
	cvtsi2ssl	%ecx, %xmm0
	mulss	%xmm0, %xmm1
	mulss	64(%rbx), %xmm0
	cvttss2sil	%xmm1, %eax
	movl	%eax, 60(%rbx)
	cvttss2sil	%xmm0, %eax
	movl	%eax, 56(%rbx)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L114
	addq	$16, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movl	(%r12), %edi
	movl	$2147483647, %edx
	movl	$28, %esi
	testl	%edi, %edi
	jle	.L110
.L114:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L120:
	.cfi_restore_state
	movl	$7, (%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2087:
	.size	uhash_initSize_67, .-uhash_initSize_67
	.p2align 4
	.globl	uhash_close_67
	.type	uhash_close_67, @function
uhash_close_67:
.LFB2088:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L149
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L124
	cmpq	$0, 32(%r13)
	movl	$-1, %r12d
	je	.L152
.L127:
	movl	52(%r13), %edx
	leal	1(%r12), %eax
	cmpl	%eax, %edx
	jle	.L126
	.p2align 4,,10
	.p2align 3
.L154:
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rdi,%rcx,8), %rbx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L153:
	addq	$24, %rbx
	cmpl	%eax, %edx
	je	.L126
.L129:
	movl	(%rbx), %ecx
	movl	%eax, %r12d
	addl	$1, %eax
	testl	%ecx, %ecx
	js	.L153
	movq	32(%r13), %rax
	testq	%rax, %rax
	je	.L130
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L130
	call	*%rax
.L130:
	movq	40(%r13), %rax
	testq	%rax, %rax
	je	.L131
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L131
	call	*%rax
.L131:
	movl	52(%r13), %edx
	leal	1(%r12), %eax
	movq	0(%r13), %rdi
	cmpl	%eax, %edx
	jg	.L154
	.p2align 4,,10
	.p2align 3
.L126:
	call	uprv_free_67@PLT
	movq	$0, 0(%r13)
.L124:
	cmpb	$0, 73(%r13)
	jne	.L155
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L155:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
.L152:
	.cfi_restore_state
	cmpq	$0, 40(%r13)
	jne	.L127
	jmp	.L126
.L149:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE2088:
	.size	uhash_close_67, .-uhash_close_67
	.p2align 4
	.globl	uhash_setKeyHasher_67
	.type	uhash_setKeyHasher_67, @function
uhash_setKeyHasher_67:
.LFB2089:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rsi, 8(%rdi)
	ret
	.cfi_endproc
.LFE2089:
	.size	uhash_setKeyHasher_67, .-uhash_setKeyHasher_67
	.p2align 4
	.globl	uhash_setKeyComparator_67
	.type	uhash_setKeyComparator_67, @function
uhash_setKeyComparator_67:
.LFB2090:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	%rsi, 16(%rdi)
	ret
	.cfi_endproc
.LFE2090:
	.size	uhash_setKeyComparator_67, .-uhash_setKeyComparator_67
	.p2align 4
	.globl	uhash_setValueComparator_67
	.type	uhash_setValueComparator_67, @function
uhash_setValueComparator_67:
.LFB2091:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE2091:
	.size	uhash_setValueComparator_67, .-uhash_setValueComparator_67
	.p2align 4
	.globl	uhash_setKeyDeleter_67
	.type	uhash_setKeyDeleter_67, @function
uhash_setKeyDeleter_67:
.LFB2092:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE2092:
	.size	uhash_setKeyDeleter_67, .-uhash_setKeyDeleter_67
	.p2align 4
	.globl	uhash_setValueDeleter_67
	.type	uhash_setValueDeleter_67, @function
uhash_setValueDeleter_67:
.LFB2093:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	%rsi, 40(%rdi)
	ret
	.cfi_endproc
.LFE2093:
	.size	uhash_setValueDeleter_67, .-uhash_setValueDeleter_67
	.p2align 4
	.globl	uhash_setResizePolicy_67
	.type	uhash_setResizePolicy_67, @function
uhash_setResizePolicy_67:
.LFB2094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	_ZL25RESIZE_POLICY_RATIO_TABLE(%rip), %rax
	movslq	%esi, %rdx
	addl	$1, %esi
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	52(%rdi), %r15d
	movss	(%rax,%rdx,4), %xmm1
	movss	(%rax,%rsi,4), %xmm2
	movl	48(%rdi), %esi
	cvtsi2ssl	%r15d, %xmm0
	movss	%xmm1, 68(%rdi)
	movsbl	72(%rdi), %ecx
	movss	%xmm2, 64(%rdi)
	mulss	%xmm0, %xmm1
	mulss	%xmm2, %xmm0
	cvttss2sil	%xmm1, %edx
	cvttss2sil	%xmm0, %eax
	movl	%edx, 60(%rdi)
	movl	%eax, 56(%rdi)
	cmpl	%esi, %eax
	jge	.L162
	leal	1(%rcx), %eax
	cmpl	$28, %eax
	jg	.L161
.L164:
	movb	%al, 72(%rbx)
	leaq	_ZL6PRIMES(%rip), %rdx
	cltq
	movq	(%rbx), %r13
	movslq	(%rdx,%rax,4), %rax
	movl	%eax, 52(%rbx)
	leaq	(%rax,%rax,2), %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L165
	movslq	52(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%rsi, %rdx
	leaq	(%rsi,%rsi,2), %rsi
	leaq	(%rax,%rsi,8), %rsi
	cmpq	%rsi, %rax
	jnb	.L166
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%rcx, 16(%rax)
	addq	$24, %rax
	movq	%rcx, -16(%rax)
	movl	$-2147483647, -24(%rax)
	cmpq	%rax, %rsi
	ja	.L167
.L166:
	pxor	%xmm0, %xmm0
	movss	68(%rbx), %xmm1
	movl	$0, 48(%rbx)
	cvtsi2ssl	%edx, %xmm0
	mulss	%xmm0, %xmm1
	mulss	64(%rbx), %xmm0
	cvttss2sil	%xmm1, %eax
	movl	%eax, 60(%rbx)
	cvttss2sil	%xmm0, %eax
	movl	%eax, 56(%rbx)
	testl	%r15d, %r15d
	jle	.L168
	movslq	%r15d, %rax
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	leaq	-48(%r13,%rax), %r14
	leaq	-24(%r13,%rax), %r12
	leal	-1(%r15), %eax
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	subq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L171:
	movl	(%r12), %edx
	testl	%edx, %edx
	js	.L170
	movq	16(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	8(%r12), %rdx
	movq	%rdx, 8(%rax)
	movl	(%r12), %edx
	movl	%edx, (%rax)
	addl	$1, 48(%rbx)
.L170:
	subq	$24, %r12
	cmpq	%r14, %r12
	jne	.L171
.L168:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
.L165:
	.cfi_restore_state
	movq	%r13, (%rbx)
	movl	%r15d, 52(%rbx)
.L161:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	cmpl	%esi, %edx
	jle	.L161
	movl	%ecx, %eax
	subl	$1, %eax
	jns	.L164
	jmp	.L161
	.cfi_endproc
.LFE2094:
	.size	uhash_setResizePolicy_67, .-uhash_setResizePolicy_67
	.p2align 4
	.globl	uhash_count_67
	.type	uhash_count_67, @function
uhash_count_67:
.LFB2095:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %eax
	ret
	.cfi_endproc
.LFE2095:
	.size	uhash_count_67, .-uhash_count_67
	.p2align 4
	.globl	uhash_get_67
	.type	uhash_get_67, @function
uhash_get_67:
.LFB2096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	popq	%r12
	popq	%r13
	movq	8(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2096:
	.size	uhash_get_67, .-uhash_get_67
	.p2align 4
	.globl	uhash_iget_67
	.type	uhash_iget_67, @function
uhash_iget_67:
.LFB2097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	popq	%r12
	popq	%r13
	movq	8(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2097:
	.size	uhash_iget_67, .-uhash_iget_67
	.p2align 4
	.globl	uhash_geti_67
	.type	uhash_geti_67, @function
uhash_geti_67:
.LFB2098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	popq	%r12
	popq	%r13
	movl	8(%rax), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2098:
	.size	uhash_geti_67, .-uhash_geti_67
	.p2align 4
	.globl	uhash_igeti_67
	.type	uhash_igeti_67, @function
uhash_igeti_67:
.LFB2099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	popq	%r12
	popq	%r13
	movl	8(%rax), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2099:
	.size	uhash_igeti_67, .-uhash_igeti_67
	.p2align 4
	.globl	uhash_put_67
	.type	uhash_put_67, @function
uhash_put_67:
.LFB2100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %esi
	testl	%esi, %esi
	jg	.L193
	testq	%rdx, %rdx
	je	.L230
	movl	56(%rdi), %eax
	movq	%rcx, %rbx
	cmpl	%eax, 48(%rdi)
	jg	.L189
.L192:
	movq	%r14, %rdi
	call	*8(%r12)
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	movl	%eax, -56(%rbp)
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movl	(%rax), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	movl	-56(%rbp), %edx
	jns	.L191
	movl	48(%r12), %ecx
	leal	1(%rcx), %eax
	movl	%eax, 48(%r12)
	cmpl	52(%r12), %eax
	je	.L231
.L191:
	movl	%edx, %ebx
	movq	32(%r12), %rdx
	movq	8(%r15), %rax
	andl	$2147483647, %ebx
	testq	%rdx, %rdx
	je	.L194
	movq	16(%r15), %rdi
	cmpq	%rdi, %r14
	je	.L194
	testq	%rdi, %rdi
	je	.L194
	movq	%rax, -56(%rbp)
	call	*%rdx
	movq	-56(%rbp), %rax
.L194:
	movq	40(%r12), %rdx
	testq	%rdx, %rdx
	je	.L195
	cmpq	%rax, %r13
	je	.L198
	testq	%rax, %rax
	je	.L198
	movq	%rax, %rdi
	call	*%rdx
	xorl	%eax, %eax
.L195:
	movq	%r13, %xmm0
	movq	%r14, %xmm1
	movl	%ebx, (%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movl	%ecx, 48(%r12)
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L193:
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.L196
	testq	%r14, %r14
	je	.L196
	movq	%r14, %rdi
	call	*%rax
.L196:
	movq	40(%r12), %rax
	testq	%rax, %rax
	je	.L197
	testq	%r13, %r13
	je	.L197
	movq	%r13, %rdi
	call	*%rax
.L197:
	xorl	%eax, %eax
.L185:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	%rcx, %rsi
	call	_ZL13_uhash_rehashP10UHashtableP10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L192
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L198:
	xorl	%eax, %eax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%r14, %rsi
	call	_ZL13_uhash_removeP10UHashtable8UElement
	jmp	.L185
	.cfi_endproc
.LFE2100:
	.size	uhash_put_67, .-uhash_put_67
	.p2align 4
	.globl	uhash_iput_67
	.type	uhash_iput_67, @function
uhash_iput_67:
.LFB2101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %esi
	testl	%esi, %esi
	jg	.L240
	testq	%rdx, %rdx
	je	.L277
	movl	56(%rdi), %eax
	movq	%rcx, %rbx
	cmpl	%eax, 48(%rdi)
	jg	.L236
.L239:
	movq	%r13, %rdi
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	movl	%eax, -56(%rbp)
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movl	(%rax), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	movl	-56(%rbp), %edx
	jns	.L238
	movl	48(%r12), %ecx
	leal	1(%rcx), %eax
	movl	%eax, 48(%r12)
	cmpl	52(%r12), %eax
	je	.L278
.L238:
	movl	%edx, %ebx
	movq	32(%r12), %rdx
	movq	8(%r15), %rax
	andl	$2147483647, %ebx
	testq	%rdx, %rdx
	je	.L241
	movq	16(%r15), %rdi
	cmpq	%r13, %rdi
	je	.L241
	testq	%rdi, %rdi
	je	.L241
	movq	%rax, -56(%rbp)
	call	*%rdx
	movq	-56(%rbp), %rax
.L241:
	movq	40(%r12), %rdx
	testq	%rdx, %rdx
	je	.L242
	testq	%rax, %rax
	je	.L245
	cmpq	%rax, %r14
	je	.L245
	movq	%rax, %rdi
	call	*%rdx
	xorl	%eax, %eax
.L242:
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movl	%ebx, (%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movl	%ecx, 48(%r12)
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L240:
	movq	32(%r12), %rax
	testq	%r13, %r13
	je	.L243
	testq	%rax, %rax
	je	.L243
	movq	%r13, %rdi
	call	*%rax
.L243:
	movq	40(%r12), %rax
	testq	%rax, %rax
	je	.L244
	testq	%r14, %r14
	je	.L244
	movq	%r14, %rdi
	call	*%rax
.L244:
	xorl	%eax, %eax
.L232:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	%rcx, %rsi
	call	_ZL13_uhash_rehashP10UHashtableP10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L239
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L245:
	xorl	%eax, %eax
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r13, %rsi
	call	_ZL13_uhash_removeP10UHashtable8UElement
	jmp	.L232
	.cfi_endproc
.LFE2101:
	.size	uhash_iput_67, .-uhash_iput_67
	.p2align 4
	.globl	uhash_puti_67
	.type	uhash_puti_67, @function
uhash_puti_67:
.LFB2102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rcx), %esi
	testl	%esi, %esi
	jg	.L285
	movl	56(%rdi), %eax
	movq	%rcx, %r14
	cmpl	%eax, 48(%rdi)
	jg	.L281
.L284:
	movq	%r12, %rdi
	call	*8(%rbx)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	movl	%eax, -56(%rbp)
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movl	(%rax), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	movl	-56(%rbp), %edx
	jns	.L283
	movl	48(%rbx), %ecx
	leal	1(%rcx), %eax
	movl	%eax, 48(%rbx)
	cmpl	52(%rbx), %eax
	je	.L323
.L283:
	andl	$2147483647, %edx
	movq	8(%r15), %rax
	movl	%edx, %r14d
	movq	32(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L286
	movq	16(%r15), %rdi
	cmpq	%rdi, %r12
	je	.L286
	testq	%rdi, %rdi
	je	.L286
	movq	%rax, -56(%rbp)
	call	*%rdx
	movq	-56(%rbp), %rax
.L286:
	movq	40(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L287
	cmpq	%r13, %rax
	je	.L291
	testq	%rax, %rax
	je	.L291
	movq	%rax, %rdi
	call	*%rdx
	xorl	%eax, %eax
.L287:
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movl	%r14d, (%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	movl	%ecx, 48(%rbx)
	movl	$7, (%r14)
	.p2align 4,,10
	.p2align 3
.L285:
	movq	32(%rbx), %rax
	testq	%r12, %r12
	je	.L289
	testq	%rax, %rax
	je	.L289
	movq	%r12, %rdi
	call	*%rax
.L289:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L290
	testq	%r13, %r13
	je	.L290
	movq	%r13, %rdi
	call	*%rax
.L290:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	movq	%rcx, %rsi
	call	_ZL13_uhash_rehashP10UHashtableP10UErrorCode
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L284
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L291:
	xorl	%eax, %eax
	jmp	.L287
	.cfi_endproc
.LFE2102:
	.size	uhash_puti_67, .-uhash_puti_67
	.p2align 4
	.globl	uhash_iputi_67
	.type	uhash_iputi_67, @function
uhash_iputi_67:
.LFB2103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rcx), %esi
	testl	%esi, %esi
	jg	.L330
	movl	56(%rdi), %eax
	movq	%rcx, %r14
	cmpl	%eax, 48(%rdi)
	jg	.L326
.L329:
	movq	%r12, %rdi
	call	*8(%rbx)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	movl	%eax, -56(%rbp)
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movl	(%rax), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	movl	-56(%rbp), %edx
	jns	.L328
	movl	48(%rbx), %ecx
	leal	1(%rcx), %eax
	movl	%eax, 48(%rbx)
	cmpl	52(%rbx), %eax
	je	.L368
.L328:
	andl	$2147483647, %edx
	movq	8(%r15), %rax
	movl	%edx, %r14d
	movq	32(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L331
	movq	16(%r15), %rdi
	cmpq	%r12, %rdi
	je	.L331
	testq	%rdi, %rdi
	je	.L331
	movq	%rax, -56(%rbp)
	call	*%rdx
	movq	-56(%rbp), %rax
.L331:
	movq	40(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L332
	testq	%rax, %rax
	je	.L336
	cmpq	%r13, %rax
	je	.L336
	movq	%rax, %rdi
	call	*%rdx
	xorl	%eax, %eax
.L332:
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	movl	%r14d, (%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movl	%ecx, 48(%rbx)
	movl	$7, (%r14)
	.p2align 4,,10
	.p2align 3
.L330:
	movq	32(%rbx), %rax
	testq	%r12, %r12
	je	.L334
	testq	%rax, %rax
	je	.L334
	movq	%r12, %rdi
	call	*%rax
.L334:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L335
	testq	%r13, %r13
	je	.L335
	movq	%r13, %rdi
	call	*%rax
.L335:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	%rcx, %rsi
	call	_ZL13_uhash_rehashP10UHashtableP10UErrorCode
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L329
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L336:
	xorl	%eax, %eax
	jmp	.L332
	.cfi_endproc
.LFE2103:
	.size	uhash_iputi_67, .-uhash_iputi_67
	.p2align 4
	.globl	uhash_remove_67
	.type	uhash_remove_67, @function
uhash_remove_67:
.LFB2104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movq	%rax, %rbx
	xorl	%eax, %eax
	movl	(%rbx), %edx
	testl	%edx, %edx
	jns	.L396
.L371:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L397
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movq	32(%r12), %rax
	subl	$1, 48(%r12)
	movq	8(%rbx), %r13
	testq	%rax, %rax
	je	.L372
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L372
	call	*%rax
.L372:
	movq	40(%r12), %rax
	testq	%rax, %rax
	je	.L373
	testq	%r13, %r13
	je	.L376
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*%rax
.L373:
	pxor	%xmm0, %xmm0
	movl	$-2147483648, (%rbx)
	movl	60(%r12), %ecx
	movq	%r13, %rax
	movups	%xmm0, 8(%rbx)
	cmpl	%ecx, 48(%r12)
	jge	.L371
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -56(%rbp)
	movl	$0, -44(%rbp)
	call	_ZL13_uhash_rehashP10UHashtableP10UErrorCode
	movq	-56(%rbp), %rax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L376:
	xorl	%r13d, %r13d
	jmp	.L373
.L397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2104:
	.size	uhash_remove_67, .-uhash_remove_67
	.p2align 4
	.globl	uhash_iremove_67
	.type	uhash_iremove_67, @function
uhash_iremove_67:
.LFB2105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movq	%rax, %rbx
	xorl	%eax, %eax
	movl	(%rbx), %edx
	testl	%edx, %edx
	jns	.L425
.L400:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L426
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	movq	32(%r12), %rax
	subl	$1, 48(%r12)
	movq	8(%rbx), %r13
	testq	%rax, %rax
	je	.L401
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L401
	call	*%rax
.L401:
	movq	40(%r12), %rax
	testq	%rax, %rax
	je	.L402
	testq	%r13, %r13
	je	.L405
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*%rax
.L402:
	pxor	%xmm0, %xmm0
	movl	$-2147483648, (%rbx)
	movl	60(%r12), %ecx
	movq	%r13, %rax
	movups	%xmm0, 8(%rbx)
	cmpl	%ecx, 48(%r12)
	jge	.L400
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, -56(%rbp)
	movl	$0, -44(%rbp)
	call	_ZL13_uhash_rehashP10UHashtableP10UErrorCode
	movq	-56(%rbp), %rax
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L405:
	xorl	%r13d, %r13d
	jmp	.L402
.L426:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2105:
	.size	uhash_iremove_67, .-uhash_iremove_67
	.p2align 4
	.globl	uhash_removei_67
	.type	uhash_removei_67, @function
uhash_removei_67:
.LFB2106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movl	(%rax), %edx
	testl	%edx, %edx
	jns	.L454
.L429:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L455
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L454:
	.cfi_restore_state
	movq	8(%rax), %r13
	movq	%rax, %rbx
	movq	32(%r12), %rax
	subl	$1, 48(%r12)
	testq	%rax, %rax
	je	.L430
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L430
	call	*%rax
.L430:
	movq	40(%r12), %rax
	testq	%rax, %rax
	je	.L431
	testq	%r13, %r13
	je	.L434
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*%rax
.L431:
	pxor	%xmm0, %xmm0
	movl	$-2147483648, (%rbx)
	movl	60(%r12), %eax
	movups	%xmm0, 8(%rbx)
	cmpl	%eax, 48(%r12)
	jge	.L429
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZL13_uhash_rehashP10UHashtableP10UErrorCode
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L434:
	xorl	%r13d, %r13d
	jmp	.L431
.L455:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2106:
	.size	uhash_removei_67, .-uhash_removei_67
	.p2align 4
	.globl	uhash_iremovei_67
	.type	uhash_iremovei_67, @function
uhash_iremovei_67:
.LFB2107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movl	(%rax), %edx
	testl	%edx, %edx
	jns	.L483
.L458:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L484
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movq	8(%rax), %r13
	movq	%rax, %rbx
	movq	32(%r12), %rax
	subl	$1, 48(%r12)
	testq	%rax, %rax
	je	.L459
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L459
	call	*%rax
.L459:
	movq	40(%r12), %rax
	testq	%rax, %rax
	je	.L460
	testq	%r13, %r13
	je	.L463
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*%rax
.L460:
	pxor	%xmm0, %xmm0
	movl	$-2147483648, (%rbx)
	movl	60(%r12), %eax
	movups	%xmm0, 8(%rbx)
	cmpl	%eax, 48(%r12)
	jge	.L458
	leaq	-44(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -44(%rbp)
	call	_ZL13_uhash_rehashP10UHashtableP10UErrorCode
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L463:
	xorl	%r13d, %r13d
	jmp	.L460
.L484:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2107:
	.size	uhash_iremovei_67, .-uhash_iremovei_67
	.p2align 4
	.globl	uhash_removeAll_67
	.type	uhash_removeAll_67, @function
uhash_removeAll_67:
.LFB2108:
	.cfi_startproc
	endbr64
	movl	48(%rdi), %esi
	testl	%esi, %esi
	je	.L506
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	$-1, %r12d
	pushq	%rbx
	leal	1(%r12), %eax
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	52(%r13), %edx
	cmpl	%edx, %eax
	jge	.L485
	.p2align 4,,10
	.p2align 3
.L510:
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	movq	0(%r13), %rcx
	leaq	(%rcx,%rsi,8), %rbx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L509:
	addq	$24, %rbx
	cmpl	%eax, %edx
	je	.L485
.L488:
	movl	(%rbx), %ecx
	movl	%eax, %r12d
	addl	$1, %eax
	testl	%ecx, %ecx
	js	.L509
	movq	32(%r13), %rax
	subl	$1, 48(%r13)
	movq	8(%rbx), %r14
	testq	%rax, %rax
	je	.L489
	movq	16(%rbx), %rdi
	cmpq	%rdi, %r15
	je	.L489
	testq	%rdi, %rdi
	je	.L489
	call	*%rax
.L489:
	cmpq	%r14, %r15
	movq	40(%r13), %rax
	setne	%cl
	testq	%r14, %r14
	setne	%dl
	testb	%dl, %cl
	je	.L490
	testq	%rax, %rax
	je	.L490
	movq	%r14, %rdi
	call	*%rax
.L490:
	movl	52(%r13), %edx
	pxor	%xmm0, %xmm0
	leal	1(%r12), %eax
	movl	$-2147483648, (%rbx)
	movups	%xmm0, 8(%rbx)
	cmpl	%edx, %eax
	jl	.L510
.L485:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE2108:
	.size	uhash_removeAll_67, .-uhash_removeAll_67
	.p2align 4
	.globl	uhash_find_67
	.type	uhash_find_67, @function
uhash_find_67:
.LFB2109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	call	*8(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	popq	%r12
	popq	%r13
	movl	(%rax), %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%edx, %edx
	movl	$0, %edx
	cmovs	%rdx, %rax
	ret
	.cfi_endproc
.LFE2109:
	.size	uhash_find_67, .-uhash_find_67
	.p2align 4
	.globl	uhash_nextElement_67
	.type	uhash_nextElement_67, @function
uhash_nextElement_67:
.LFB2110:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	movl	52(%rdi), %ecx
	leal	1(%rax), %edx
	cmpl	%edx, %ecx
	jle	.L518
	movslq	%edx, %rax
	leaq	(%rax,%rax,2), %r8
	movq	(%rdi), %rax
	leaq	(%rax,%r8,8), %rax
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L516:
	addl	$1, %edx
	addq	$24, %rax
	cmpl	%edx, %ecx
	je	.L518
.L517:
	movl	(%rax), %edi
	testl	%edi, %edi
	js	.L516
	movl	%edx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2110:
	.size	uhash_nextElement_67, .-uhash_nextElement_67
	.p2align 4
	.globl	uhash_removeElement_67
	.type	uhash_removeElement_67, @function
uhash_removeElement_67:
.LFB2111:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	js	.L544
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	32(%rdi), %rdx
	subl	$1, 48(%rdi)
	movq	8(%rsi), %rax
	testq	%rdx, %rdx
	je	.L522
	movq	16(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L522
	movq	%rax, -24(%rbp)
	call	*%rdx
	movq	-24(%rbp), %rax
.L522:
	movq	40(%r12), %rdx
	testq	%rdx, %rdx
	je	.L523
	testq	%rax, %rax
	je	.L525
	movq	%rax, %rdi
	call	*%rdx
	xorl	%eax, %eax
.L523:
	pxor	%xmm0, %xmm0
	movl	$-2147483648, (%rbx)
	movups	%xmm0, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L544:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	xorl	%eax, %eax
	jmp	.L523
	.cfi_endproc
.LFE2111:
	.size	uhash_removeElement_67, .-uhash_removeElement_67
	.p2align 4
	.globl	uhash_hashUChars_67
	.type	uhash_hashUChars_67, @function
uhash_hashUChars_67:
.LFB2112:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L550
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	u_strlen_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	%eax, %esi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ustr_hashUCharsN_67@PLT
	.p2align 4,,10
	.p2align 3
.L550:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2112:
	.size	uhash_hashUChars_67, .-uhash_hashUChars_67
	.p2align 4
	.globl	uhash_hashChars_67
	.type	uhash_hashChars_67, @function
uhash_hashChars_67:
.LFB2113:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L555
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	strlen@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ustr_hashCharsN_67@PLT
	.p2align 4,,10
	.p2align 3
.L555:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2113:
	.size	uhash_hashChars_67, .-uhash_hashChars_67
	.p2align 4
	.globl	uhash_hashIChars_67
	.type	uhash_hashIChars_67, @function
uhash_hashIChars_67:
.LFB2114:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L560
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	strlen@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ustr_hashICharsN_67@PLT
	.p2align 4,,10
	.p2align 3
.L560:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2114:
	.size	uhash_hashIChars_67, .-uhash_hashIChars_67
	.section	.text.unlikely
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4
	.globl	uhash_equals_67
	.type	uhash_equals_67, @function
uhash_equals_67:
.LFB2115:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L580
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L564
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L564
	movq	16(%rsi), %rcx
	cmpq	%rcx, 16(%rdi)
	je	.L581
.L564:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore_state
	movq	24(%rdi), %rdx
	cmpq	%rdx, 24(%rsi)
	jne	.L575
	testq	%rdx, %rdx
	je	.L575
	movl	48(%rdi), %r15d
	cmpl	48(%rsi), %r15d
	jne	.L564
	testl	%r15d, %r15d
	jle	.L567
	movl	$-1, %ebx
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L572:
	movl	52(%r12), %edx
	addl	$1, %ebx
	cmpl	%edx, %ebx
	jge	.L568
	movslq	%ebx, %rax
	leaq	(%rax,%rax,2), %rcx
	movq	(%r12), %rax
	leaq	(%rax,%rcx,8), %rax
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L583:
	addl	$1, %ebx
	addq	$24, %rax
	cmpl	%edx, %ebx
	je	.L582
.L570:
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	js	.L583
	movq	16(%rax), %rsi
	movq	8(%rax), %r8
	movq	%rsi, %rdi
	movq	%r8, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	*8(%r13)
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	movl	%eax, %edx
	call	_ZL11_uhash_findPK10UHashtable8UElementi
	movq	-64(%rbp), %r8
	movq	8(%rax), %rsi
	movq	%r8, %rdi
	call	*24(%r12)
	testb	%al, %al
	je	.L564
	addl	$1, %r14d
	cmpl	%r14d, %r15d
	jne	.L572
.L567:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
.L582:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	jmp	.L568
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	uhash_equals_67.cold, @function
uhash_equals_67.cold:
.LFSB2115:
.L568:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	16, %rsi
	ud2
	.cfi_endproc
.LFE2115:
	.text
	.size	uhash_equals_67, .-uhash_equals_67
	.section	.text.unlikely
	.size	uhash_equals_67.cold, .-uhash_equals_67.cold
.LCOLDE1:
	.text
.LHOTE1:
	.p2align 4
	.globl	uhash_compareUChars_67
	.type	uhash_compareUChars_67, @function
uhash_compareUChars_67:
.LFB2116:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L588
	testq	%rdi, %rdi
	je	.L589
	testq	%rsi, %rsi
	jne	.L594
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L587:
	movzwl	(%rsi), %edx
	cmpw	%ax, %dx
	jne	.L586
	addq	$2, %rdi
	addq	$2, %rsi
.L594:
	movzwl	(%rdi), %eax
	movzwl	(%rsi), %edx
	testw	%ax, %ax
	jne	.L587
.L586:
	cmpw	%dx, %ax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L589:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2116:
	.size	uhash_compareUChars_67, .-uhash_compareUChars_67
	.p2align 4
	.globl	uhash_compareChars_67
	.type	uhash_compareChars_67, @function
uhash_compareChars_67:
.LFB2117:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L599
	testq	%rdi, %rdi
	je	.L600
	testq	%rsi, %rsi
	jne	.L605
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L598:
	movzbl	(%rsi), %edx
	cmpb	%al, %dl
	jne	.L597
	addq	$1, %rdi
	addq	$1, %rsi
.L605:
	movzbl	(%rdi), %eax
	movzbl	(%rsi), %edx
	testb	%al, %al
	jne	.L598
.L597:
	cmpb	%dl, %al
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2117:
	.size	uhash_compareChars_67, .-uhash_compareChars_67
	.p2align 4
	.globl	uhash_compareIChars_67
	.type	uhash_compareIChars_67, @function
uhash_compareIChars_67:
.LFB2118:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L611
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L612
	movq	%rsi, %r13
	testq	%rsi, %rsi
	jne	.L621
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L610:
	call	uprv_asciitolower_67@PLT
	movsbl	0(%r13), %edi
	movl	%eax, %r12d
	call	uprv_asciitolower_67@PLT
	cmpb	%al, %r12b
	jne	.L622
	addq	$1, %rbx
	addq	$1, %r13
.L621:
	movsbl	(%rbx), %edi
	testb	%dil, %dil
	jne	.L610
.L608:
	cmpb	%dil, 0(%r13)
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movzbl	(%rbx), %edi
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2118:
	.size	uhash_compareIChars_67, .-uhash_compareIChars_67
	.p2align 4
	.globl	uhash_hashLong_67
	.type	uhash_hashLong_67, @function
uhash_hashLong_67:
.LFB2119:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE2119:
	.size	uhash_hashLong_67, .-uhash_hashLong_67
	.p2align 4
	.globl	uhash_compareLong_67
	.type	uhash_compareLong_67, @function
uhash_compareLong_67:
.LFB2120:
	.cfi_startproc
	endbr64
	cmpl	%esi, %edi
	sete	%al
	ret
	.cfi_endproc
.LFE2120:
	.size	uhash_compareLong_67, .-uhash_compareLong_67
	.section	.rodata
	.align 16
	.type	_ZL25RESIZE_POLICY_RATIO_TABLE, @object
	.size	_ZL25RESIZE_POLICY_RATIO_TABLE, 24
_ZL25RESIZE_POLICY_RATIO_TABLE:
	.long	0
	.long	1056964608
	.long	1036831949
	.long	1056964608
	.long	0
	.long	1065353216
	.align 32
	.type	_ZL6PRIMES, @object
	.size	_ZL6PRIMES, 116
_ZL6PRIMES:
	.long	7
	.long	13
	.long	31
	.long	61
	.long	127
	.long	251
	.long	509
	.long	1021
	.long	2039
	.long	4093
	.long	8191
	.long	16381
	.long	32749
	.long	65521
	.long	131071
	.long	262139
	.long	524287
	.long	1048573
	.long	2097143
	.long	4194301
	.long	8388593
	.long	16777213
	.long	33554393
	.long	67108859
	.long	134217689
	.long	268435399
	.long	536870909
	.long	1073741789
	.long	2147483647
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
