	.file	"rbbirb.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIRuleBuilderC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6715RBBIRuleBuilderC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, @function
_ZN6icu_6715RBBIRuleBuilderC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode:
.LFB3166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715RBBIRuleBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	movq	%rsi, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	pxor	%xmm0, %xmm0
	leaq	112(%rbx), %rax
	xorl	%ecx, %ecx
	movq	%r13, 16(%rbx)
	movq	%r12, 24(%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 136(%rbx)
	movq	%rax, 144(%rbx)
	movw	%cx, 152(%rbx)
	movb	$0, 154(%rbx)
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 120(%rbx)
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 176(%rbx)
	testq	%r12, %r12
	je	.L2
	movq	$0, 64(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movups	%xmm0, 16(%r12)
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
.L2:
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L26
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L5
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
.L5:
	movq	%r12, 168(%rbx)
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L6
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
.L6:
	movq	%r12, 184(%rbx)
	movl	$3192, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L7
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715RBBIRuleScannerC1EPNS_15RBBIRuleBuilderE@PLT
.L7:
	movq	%r12, 104(%rbx)
	movl	$48, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L8
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6714RBBISetBuilderC1EPNS_15RBBIRuleBuilderE@PLT
	movl	0(%r13), %eax
	movq	%r12, 160(%rbx)
	testl	%eax, %eax
	jg	.L1
	cmpq	$0, 104(%rbx)
	je	.L11
	cmpq	$0, 168(%rbx)
	je	.L11
	cmpq	$0, 184(%rbx)
	jne	.L1
.L11:
	movl	$7, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	movq	$0, 160(%rbx)
	cmpl	$0, 0(%r13)
	jle	.L11
	jmp	.L1
	.cfi_endproc
.LFE3166:
	.size	_ZN6icu_6715RBBIRuleBuilderC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, .-_ZN6icu_6715RBBIRuleBuilderC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6715RBBIRuleBuilderC1ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.set	_ZN6icu_6715RBBIRuleBuilderC1ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode,_ZN6icu_6715RBBIRuleBuilderC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIRuleBuilder14optimizeTablesEv
	.type	_ZN6icu_6715RBBIRuleBuilder14optimizeTablesEv, @function
_ZN6icu_6715RBBIRuleBuilder14optimizeTablesEv:
.LFB3176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L30:
	movq	$3, -48(%rbp)
	xorl	%r13d, %r13d
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L37:
	movq	160(%r12), %rdi
	movq	-48(%rbp), %rsi
	movl	%ebx, %r13d
	call	_ZN6icu_6714RBBISetBuilder15mergeCategoriesESt4pairIiiE@PLT
	movq	176(%r12), %rdi
	movl	-44(%rbp), %esi
	call	_ZN6icu_6716RBBITableBuilder12removeColumnEi@PLT
.L29:
	movq	176(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6716RBBITableBuilder21findDuplCharClassFromEPSt4pairIiiE@PLT
	movl	%eax, %ebx
	testb	%al, %al
	jne	.L37
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$1, %r13d
.L28:
	movq	176(%r12), %rdi
	call	_ZN6icu_6716RBBITableBuilder21removeDuplicateStatesEv@PLT
	testl	%eax, %eax
	jg	.L32
	testb	%r13b, %r13b
	jne	.L30
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3176:
	.size	_ZN6icu_6715RBBIRuleBuilder14optimizeTablesEv, .-_ZN6icu_6715RBBIRuleBuilder14optimizeTablesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIRuleBuilderD2Ev
	.type	_ZN6icu_6715RBBIRuleBuilderD2Ev, @function
_ZN6icu_6715RBBIRuleBuilderD2Ev:
.LFB3169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715RBBIRuleBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%rax, %rdi
	addl	$1, %ebx
	call	_ZN6icu_678RBBINodeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L41:
	movq	168(%r13), %rdi
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L79
	movq	168(%r13), %rdi
	testq	%rdi, %rdi
	je	.L42
	movq	(%rdi), %rax
	call	*8(%rax)
.L42:
	movq	160(%r13), %r12
	testq	%r12, %r12
	je	.L43
	movq	%r12, %rdi
	call	_ZN6icu_6714RBBISetBuilderD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L43:
	movq	176(%r13), %r12
	testq	%r12, %r12
	je	.L44
	movq	%r12, %rdi
	call	_ZN6icu_6716RBBITableBuilderD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L44:
	movq	112(%r13), %r12
	testq	%r12, %r12
	je	.L45
	movq	%r12, %rdi
	call	_ZN6icu_678RBBINodeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L45:
	movq	120(%r13), %r12
	testq	%r12, %r12
	je	.L46
	movq	%r12, %rdi
	call	_ZN6icu_678RBBINodeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L46:
	movq	128(%r13), %r12
	testq	%r12, %r12
	je	.L47
	movq	%r12, %rdi
	call	_ZN6icu_678RBBINodeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L47:
	movq	136(%r13), %r12
	testq	%r12, %r12
	je	.L48
	movq	%r12, %rdi
	call	_ZN6icu_678RBBINodeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L48:
	movq	104(%r13), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	(%rdi), %rax
	call	*8(%rax)
.L49:
	movq	184(%r13), %rdi
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rax
	call	*8(%rax)
.L50:
	addq	$8, %rsp
	leaq	40(%r13), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE3169:
	.size	_ZN6icu_6715RBBIRuleBuilderD2Ev, .-_ZN6icu_6715RBBIRuleBuilderD2Ev
	.globl	_ZN6icu_6715RBBIRuleBuilderD1Ev
	.set	_ZN6icu_6715RBBIRuleBuilderD1Ev,_ZN6icu_6715RBBIRuleBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIRuleBuilderD0Ev
	.type	_ZN6icu_6715RBBIRuleBuilderD0Ev, @function
_ZN6icu_6715RBBIRuleBuilderD0Ev:
.LFB3171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6715RBBIRuleBuilderD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3171:
	.size	_ZN6icu_6715RBBIRuleBuilderD0Ev, .-_ZN6icu_6715RBBIRuleBuilderD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6715RBBIRuleBuilder11flattenDataEv.part.0, @function
_ZN6icu_6715RBBIRuleBuilder11flattenDataEv.part.0:
.LFB4238:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	40(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6715RBBIRuleScanner10stripRulesERKNS_13UnicodeStringE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	176(%rbx), %rdi
	call	_ZNK6icu_6716RBBITableBuilder12getTableSizeEv@PLT
	movq	176(%rbx), %rdi
	addl	$7, %eax
	andl	$-8, %eax
	movl	%eax, -148(%rbp)
	call	_ZNK6icu_6716RBBITableBuilder16getSafeTableSizeEv@PLT
	movq	160(%rbx), %rdi
	addl	$7, %eax
	andl	$-8, %eax
	movl	%eax, -152(%rbp)
	call	_ZN6icu_6714RBBISetBuilder11getTrieSizeEv@PLT
	addl	$7, %eax
	andl	$-8, %eax
	movl	%eax, -156(%rbp)
	movq	184(%rbx), %rax
	movl	8(%rax), %eax
	leal	7(,%rax,4), %r12d
	movswl	48(%rbx), %eax
	andl	$-8, %r12d
	testw	%ax, %ax
	js	.L83
	sarl	$5, %eax
.L84:
	leal	9(%rax,%rax), %edx
	movl	-148(%rbp), %eax
	movl	-152(%rbp), %r14d
	movl	%edx, %ecx
	addl	$80, %eax
	andl	$-8, %ecx
	addl	%eax, %r14d
	movl	%eax, -168(%rbp)
	leal	(%r14,%r12), %eax
	addl	-156(%rbp), %eax
	movl	%ecx, -160(%rbp)
	addl	%ecx, %eax
	movslq	%eax, %rdx
	movl	%eax, -164(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -176(%rbp)
	call	uprv_malloc_67@PLT
	movq	-176(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L94
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	160(%rbx), %rdi
	movabsq	$21474881952, %rax
	movq	%rax, (%r15)
	movl	-164(%rbp), %eax
	movl	%eax, 8(%r15)
	call	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv@PLT
	movq	160(%rbx), %rdi
	movl	%r14d, 32(%r15)
	movl	%eax, 12(%r15)
	movl	-148(%rbp), %eax
	movl	$80, 16(%r15)
	movl	%eax, 20(%r15)
	movl	-168(%rbp), %eax
	movl	%eax, 24(%r15)
	movl	-152(%rbp), %eax
	movl	%eax, 28(%r15)
	call	_ZN6icu_6714RBBISetBuilder11getTrieSizeEv@PLT
	movl	%r12d, 52(%r15)
	movl	%eax, 36(%r15)
	movl	-156(%rbp), %eax
	addl	32(%r15), %eax
	addl	%eax, %r12d
	movl	%eax, 48(%r15)
	movswl	48(%rbx), %eax
	movl	%r12d, 40(%r15)
	testw	%ax, %ax
	js	.L87
	sarl	$5, %eax
.L88:
	movl	16(%r15), %esi
	movq	176(%rbx), %rdi
	addl	%eax, %eax
	pxor	%xmm0, %xmm0
	movl	%eax, 44(%r15)
	addq	%r15, %rsi
	movups	%xmm0, 56(%r15)
	movq	$0, 72(%r15)
	call	_ZN6icu_6716RBBITableBuilder11exportTableEPv@PLT
	movl	24(%r15), %esi
	movq	176(%rbx), %rdi
	addq	%r15, %rsi
	call	_ZN6icu_6716RBBITableBuilder15exportSafeTableEPv@PLT
	movl	32(%r15), %esi
	movq	160(%rbx), %rdi
	addq	%r15, %rsi
	call	_ZN6icu_6714RBBISetBuilder13serializeTrieEPh@PLT
	movq	184(%rbx), %rdi
	movl	48(%r15), %eax
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jle	.L89
	xorl	%r12d, %r12d
	leaq	(%r15,%rax), %r14
	.p2align 4,,10
	.p2align 3
.L90:
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	movq	184(%rbx), %rdi
	movl	%eax, (%r14,%r12,4)
	addq	$1, %r12
	cmpl	%r12d, 8(%rdi)
	jg	.L90
.L89:
	movl	-160(%rbp), %edx
	movl	40(%r15), %eax
	movq	%r13, %rdi
	leaq	-136(%rbp), %rsi
	movq	16(%rbx), %rcx
	sarl	%edx
	addq	%r15, %rax
	addl	$1, %edx
	movq	%rax, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
.L82:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$136, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movl	52(%rbx), %eax
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L87:
	movl	52(%rbx), %eax
	jmp	.L88
.L95:
	call	__stack_chk_fail@PLT
.L94:
	movq	16(%rbx), %rax
	movl	$7, (%rax)
	jmp	.L82
	.cfi_endproc
.LFE4238:
	.size	_ZN6icu_6715RBBIRuleBuilder11flattenDataEv.part.0, .-_ZN6icu_6715RBBIRuleBuilder11flattenDataEv.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIRuleBuilder11flattenDataEv
	.type	_ZN6icu_6715RBBIRuleBuilder11flattenDataEv, @function
_ZN6icu_6715RBBIRuleBuilder11flattenDataEv:
.LFB3173:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L97
	jmp	_ZN6icu_6715RBBIRuleBuilder11flattenDataEv.part.0
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3173:
	.size	_ZN6icu_6715RBBIRuleBuilder11flattenDataEv, .-_ZN6icu_6715RBBIRuleBuilder11flattenDataEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIRuleBuilder5buildER10UErrorCode
	.type	_ZN6icu_6715RBBIRuleBuilder5buildER10UErrorCode, @function
_ZN6icu_6715RBBIRuleBuilder5buildER10UErrorCode:
.LFB3175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L99
	movq	%rdi, %r12
	movq	104(%rdi), %rdi
	movq	%rsi, %rbx
	call	_ZN6icu_6715RBBIRuleScanner5parseEv@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L99
	movq	160(%r12), %rdi
	call	_ZN6icu_6714RBBISetBuilder11buildRangesEv@PLT
	movl	$48, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L100
	movq	%rax, %rdi
	leaq	112(%r12), %rdx
	movq	%rbx, %rcx
	movq	%r12, %rsi
	call	_ZN6icu_6716RBBITableBuilderC1EPNS_15RBBIRuleBuilderEPPNS_8RBBINodeER10UErrorCode@PLT
	movq	%r13, 176(%r12)
	movq	%r13, %rdi
	leaq	-64(%rbp), %r14
	call	_ZN6icu_6716RBBITableBuilder17buildForwardTableEv@PLT
	.p2align 4,,10
	.p2align 3
.L103:
	movq	$3, -64(%rbp)
	xorl	%r15d, %r15d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L113:
	movq	160(%r12), %rdi
	movq	-64(%rbp), %rsi
	movl	%r13d, %r15d
	call	_ZN6icu_6714RBBISetBuilder15mergeCategoriesESt4pairIiiE@PLT
	movq	176(%r12), %rdi
	movl	-60(%rbp), %esi
	call	_ZN6icu_6716RBBITableBuilder12removeColumnEi@PLT
.L102:
	movq	176(%r12), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6716RBBITableBuilder21findDuplCharClassFromEPSt4pairIiiE@PLT
	movl	%eax, %r13d
	testb	%al, %al
	jne	.L113
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$1, %r15d
.L101:
	movq	176(%r12), %rdi
	call	_ZN6icu_6716RBBITableBuilder21removeDuplicateStatesEv@PLT
	testl	%eax, %eax
	jg	.L107
	testb	%r15b, %r15b
	jne	.L103
	movq	176(%r12), %rdi
	movq	%rbx, %rsi
	xorl	%r13d, %r13d
	call	_ZN6icu_6716RBBITableBuilder21buildSafeReverseTableER10UErrorCode@PLT
	movq	160(%r12), %rdi
	call	_ZN6icu_6714RBBISetBuilder9buildTrieEv@PLT
	movq	16(%r12), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L104
	movq	%r12, %rdi
	call	_ZN6icu_6715RBBIRuleBuilder11flattenDataEv.part.0
	movq	%rax, %r13
.L104:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L98
	.p2align 4,,10
	.p2align 3
.L99:
	xorl	%r13d, %r13d
.L98:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L100:
	movq	$0, 176(%r12)
	movl	$7, (%rbx)
	jmp	.L98
	.cfi_endproc
.LFE3175:
	.size	_ZN6icu_6715RBBIRuleBuilder5buildER10UErrorCode, .-_ZN6icu_6715RBBIRuleBuilder5buildER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIRuleBuilder28createRuleBasedBreakIteratorERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6715RBBIRuleBuilder28createRuleBasedBreakIteratorERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, @function
_ZN6icu_6715RBBIRuleBuilder28createRuleBasedBreakIteratorERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode:
.LFB3174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%r8, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$208, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6715RBBIRuleBuilderE(%rip), %rax
	movq	%rdi, -208(%rbp)
	leaq	-200(%rbp), %rdi
	movq	%rax, -240(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rax
	xorl	%edi, %edi
	movq	%rbx, -224(%rbp)
	movq	%r12, -216(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movw	%di, -88(%rbp)
	movb	$0, -86(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -120(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	testq	%r12, %r12
	je	.L116
	movq	$0, 64(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movups	%xmm0, 16(%r12)
	movups	%xmm0, 32(%r12)
	movups	%xmm0, 48(%r12)
.L116:
	movl	(%rbx), %esi
	leaq	-240(%rbp), %r13
	testl	%esi, %esi
	jle	.L150
.L149:
	xorl	%r12d, %r12d
.L126:
	movq	%r13, %rdi
	call	_ZN6icu_6715RBBIRuleBuilderD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L151
	addq	$208, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L118
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
.L118:
	movl	$40, %edi
	movq	%r12, -72(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L119
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
.L119:
	movl	$3192, %edi
	movq	%r12, -56(%rbp)
	leaq	-240(%rbp), %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L120
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6715RBBIRuleScannerC1EPNS_15RBBIRuleBuilderE@PLT
.L120:
	movl	$48, %edi
	movq	%r12, -136(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L121
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6714RBBISetBuilderC1EPNS_15RBBIRuleBuilderE@PLT
	movl	(%rbx), %ecx
	movq	%r12, -80(%rbp)
	testl	%ecx, %ecx
	jg	.L149
	cmpq	$0, -136(%rbp)
	je	.L124
	cmpq	$0, -72(%rbp)
	je	.L124
	cmpq	$0, -56(%rbp)
	je	.L124
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6715RBBIRuleBuilder5buildER10UErrorCode
	movq	%rax, %r14
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L149
	movl	$640, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L127
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722RuleBasedBreakIteratorC1EPNS_14RBBIDataHeaderER10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L126
	movq	%r12, %rdi
	call	_ZN6icu_6722RuleBasedBreakIteratorD0Ev@PLT
	jmp	.L149
.L121:
	cmpl	$0, (%rbx)
	movq	$0, -80(%rbp)
	jg	.L149
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$7, (%rbx)
	jmp	.L149
.L151:
	call	__stack_chk_fail@PLT
.L127:
	cmpl	$0, (%rbx)
	jg	.L149
	movl	$7, (%rbx)
	jmp	.L126
	.cfi_endproc
.LFE3174:
	.size	_ZN6icu_6715RBBIRuleBuilder28createRuleBasedBreakIteratorERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, .-_ZN6icu_6715RBBIRuleBuilder28createRuleBasedBreakIteratorERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6715RBBIRuleBuilderE
	.section	.rodata._ZTSN6icu_6715RBBIRuleBuilderE,"aG",@progbits,_ZTSN6icu_6715RBBIRuleBuilderE,comdat
	.align 16
	.type	_ZTSN6icu_6715RBBIRuleBuilderE, @object
	.size	_ZTSN6icu_6715RBBIRuleBuilderE, 27
_ZTSN6icu_6715RBBIRuleBuilderE:
	.string	"N6icu_6715RBBIRuleBuilderE"
	.weak	_ZTIN6icu_6715RBBIRuleBuilderE
	.section	.data.rel.ro._ZTIN6icu_6715RBBIRuleBuilderE,"awG",@progbits,_ZTIN6icu_6715RBBIRuleBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_6715RBBIRuleBuilderE, @object
	.size	_ZTIN6icu_6715RBBIRuleBuilderE, 24
_ZTIN6icu_6715RBBIRuleBuilderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715RBBIRuleBuilderE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTVN6icu_6715RBBIRuleBuilderE
	.section	.data.rel.ro.local._ZTVN6icu_6715RBBIRuleBuilderE,"awG",@progbits,_ZTVN6icu_6715RBBIRuleBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_6715RBBIRuleBuilderE, @object
	.size	_ZTVN6icu_6715RBBIRuleBuilderE, 32
_ZTVN6icu_6715RBBIRuleBuilderE:
	.quad	0
	.quad	_ZTIN6icu_6715RBBIRuleBuilderE
	.quad	_ZN6icu_6715RBBIRuleBuilderD1Ev
	.quad	_ZN6icu_6715RBBIRuleBuilderD0Ev
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
