	.file	"bmpset.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_676BMPSetD2Ev
	.type	_ZN6icu_676BMPSetD2Ev, @function
_ZN6icu_676BMPSetD2Ev:
.LFB2321:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2321:
	.size	_ZN6icu_676BMPSetD2Ev, .-_ZN6icu_676BMPSetD2Ev
	.globl	_ZN6icu_676BMPSetD1Ev
	.set	_ZN6icu_676BMPSetD1Ev,_ZN6icu_676BMPSetD2Ev
	.p2align 4
	.type	_ZN6icu_67L12set32x64BitsEPjii, @function
_ZN6icu_67L12set32x64BitsEPjii:
.LFB2324:
	.cfi_startproc
	movl	%esi, %ecx
	movl	%esi, %eax
	movl	$1, %r8d
	addl	$1, %esi
	sarl	$6, %ecx
	andl	$63, %eax
	sall	%cl, %r8d
	cmpl	%edx, %esi
	je	.L161
	movl	%edx, %r9d
	andl	$63, %edx
	sarl	$6, %r9d
	cmpl	%r9d, %ecx
	je	.L162
	testl	%eax, %eax
	je	.L11
	movl	$63, %esi
	movl	$64, %r10d
	subl	%eax, %esi
	subl	%eax, %r10d
	cmpl	$2, %esi
	jbe	.L12
	movslq	%eax, %rsi
	movd	%r8d, %xmm5
	movl	%r10d, %r11d
	leaq	(%rdi,%rsi,4), %rsi
	pshufd	$0, %xmm5, %xmm0
	shrl	$2, %r11d
	movdqu	(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, (%rsi)
	cmpl	$1, %r11d
	je	.L13
	movdqu	16(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 16(%rsi)
	cmpl	$2, %r11d
	je	.L13
	movdqu	32(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 32(%rsi)
	cmpl	$3, %r11d
	je	.L13
	movdqu	48(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 48(%rsi)
	cmpl	$4, %r11d
	je	.L13
	movdqu	64(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 64(%rsi)
	cmpl	$5, %r11d
	je	.L13
	movdqu	80(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 80(%rsi)
	cmpl	$6, %r11d
	je	.L13
	movdqu	96(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 96(%rsi)
	cmpl	$7, %r11d
	je	.L13
	movdqu	112(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 112(%rsi)
	cmpl	$8, %r11d
	je	.L13
	movdqu	128(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 128(%rsi)
	cmpl	$9, %r11d
	je	.L13
	movdqu	144(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 144(%rsi)
	cmpl	$10, %r11d
	je	.L13
	movdqu	160(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 160(%rsi)
	cmpl	$11, %r11d
	je	.L13
	movdqu	176(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 176(%rsi)
	cmpl	$12, %r11d
	je	.L13
	movdqu	192(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 192(%rsi)
	cmpl	$13, %r11d
	je	.L13
	movdqu	208(%rsi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 208(%rsi)
	cmpl	$14, %r11d
	je	.L13
	movdqu	224(%rsi), %xmm7
	por	%xmm7, %xmm0
	movups	%xmm0, 224(%rsi)
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%r10d, %esi
	andl	$-4, %esi
	addl	%esi, %eax
	cmpl	%esi, %r10d
	je	.L14
.L12:
	movslq	%eax, %r10
	leal	1(%rax), %esi
	orl	%r8d, (%rdi,%r10,4)
	cmpl	$63, %eax
	je	.L14
	movslq	%esi, %rsi
	leal	2(%rax), %r10d
	orl	%r8d, (%rdi,%rsi,4)
	cmpl	$62, %eax
	je	.L14
	movslq	%r10d, %rax
	orl	%r8d, (%rdi,%rax,4)
.L14:
	addl	$1, %ecx
.L11:
	cmpl	%r9d, %ecx
	jge	.L15
	movl	$-1, %eax
	sall	%cl, %eax
	cmpl	$31, %r9d
	jg	.L16
	movl	$-1, %esi
	movl	%r9d, %ecx
	sall	%cl, %esi
	notl	%esi
	andl	%esi, %eax
.L16:
	movdqu	(%rdi), %xmm1
	movd	%eax, %xmm3
	movdqu	240(%rdi), %xmm4
	pshufd	$0, %xmm3, %xmm0
	por	%xmm0, %xmm1
	movups	%xmm1, (%rdi)
	movdqu	16(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 16(%rdi)
	movdqu	32(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 32(%rdi)
	movdqu	48(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 48(%rdi)
	movdqu	64(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 64(%rdi)
	movdqu	80(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 80(%rdi)
	movdqu	96(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 96(%rdi)
	movdqu	112(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 112(%rdi)
	movdqu	128(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 128(%rdi)
	movdqu	144(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 144(%rdi)
	movdqu	160(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 160(%rdi)
	movdqu	176(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 176(%rdi)
	movdqu	192(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 192(%rdi)
	movdqu	208(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 208(%rdi)
	movdqu	224(%rdi), %xmm1
	por	%xmm0, %xmm1
	por	%xmm4, %xmm0
	movups	%xmm1, 224(%rdi)
	movups	%xmm0, 240(%rdi)
.L15:
	movl	%r9d, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	cmpl	$32, %r9d
	movl	$-2147483648, %ecx
	cmove	%ecx, %eax
	testl	%edx, %edx
	je	.L3
	leal	-1(%rdx), %ecx
	cmpl	$2, %ecx
	jbe	.L22
	movdqu	(%rdi), %xmm1
	movd	%eax, %xmm2
	movl	%edx, %ecx
	pshufd	$0, %xmm2, %xmm0
	shrl	$2, %ecx
	por	%xmm0, %xmm1
	movups	%xmm1, (%rdi)
	cmpl	$1, %ecx
	je	.L19
	movdqu	16(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 16(%rdi)
	cmpl	$2, %ecx
	je	.L19
	movdqu	32(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 32(%rdi)
	cmpl	$3, %ecx
	je	.L19
	movdqu	48(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 48(%rdi)
	cmpl	$4, %ecx
	je	.L19
	movdqu	64(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 64(%rdi)
	cmpl	$5, %ecx
	je	.L19
	movdqu	80(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 80(%rdi)
	cmpl	$6, %ecx
	je	.L19
	movdqu	96(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 96(%rdi)
	cmpl	$7, %ecx
	je	.L19
	movdqu	112(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 112(%rdi)
	cmpl	$8, %ecx
	je	.L19
	movdqu	128(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 128(%rdi)
	cmpl	$9, %ecx
	je	.L19
	movdqu	144(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 144(%rdi)
	cmpl	$10, %ecx
	je	.L19
	movdqu	160(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 160(%rdi)
	cmpl	$11, %ecx
	je	.L19
	movdqu	176(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 176(%rdi)
	cmpl	$12, %ecx
	je	.L19
	movdqu	192(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 192(%rdi)
	cmpl	$13, %ecx
	je	.L19
	movdqu	208(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 208(%rdi)
	cmpl	$14, %ecx
	je	.L19
	movdqu	224(%rdi), %xmm7
	por	%xmm7, %xmm0
	movups	%xmm0, 224(%rdi)
	.p2align 4,,10
	.p2align 3
.L19:
	movl	%edx, %ecx
	andl	$-4, %ecx
	testl	$-61, %edx
	je	.L163
.L18:
	movslq	%ecx, %rsi
	orl	%eax, (%rdi,%rsi,4)
	leal	1(%rcx), %esi
	cmpl	%esi, %edx
	jle	.L3
	movslq	%esi, %rsi
	addl	$2, %ecx
	orl	%eax, (%rdi,%rsi,4)
	cmpl	%ecx, %edx
	jle	.L3
	movslq	%ecx, %rcx
	orl	%eax, (%rdi,%rcx,4)
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	cmpl	%edx, %eax
	jge	.L3
	leal	-1(%rdx), %ecx
	movl	%edx, %r9d
	subl	%eax, %ecx
	subl	%eax, %r9d
	cmpl	$2, %ecx
	jbe	.L8
	movslq	%eax, %rcx
	movd	%r8d, %xmm6
	movl	%r9d, %esi
	leaq	(%rdi,%rcx,4), %rcx
	pshufd	$0, %xmm6, %xmm0
	shrl	$2, %esi
	movdqu	(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, (%rcx)
	cmpl	$1, %esi
	je	.L9
	movdqu	16(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 16(%rcx)
	cmpl	$2, %esi
	je	.L9
	movdqu	32(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 32(%rcx)
	cmpl	$3, %esi
	je	.L9
	movdqu	48(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 48(%rcx)
	cmpl	$4, %esi
	je	.L9
	movdqu	64(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 64(%rcx)
	cmpl	$5, %esi
	je	.L9
	movdqu	80(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 80(%rcx)
	cmpl	$6, %esi
	je	.L9
	movdqu	96(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 96(%rcx)
	cmpl	$7, %esi
	je	.L9
	movdqu	112(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 112(%rcx)
	cmpl	$8, %esi
	je	.L9
	movdqu	128(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 128(%rcx)
	cmpl	$9, %esi
	je	.L9
	movdqu	144(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 144(%rcx)
	cmpl	$10, %esi
	je	.L9
	movdqu	160(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 160(%rcx)
	cmpl	$11, %esi
	je	.L9
	movdqu	176(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 176(%rcx)
	cmpl	$12, %esi
	je	.L9
	movdqu	192(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 192(%rcx)
	cmpl	$13, %esi
	je	.L9
	movdqu	208(%rcx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 208(%rcx)
	cmpl	$14, %esi
	je	.L9
	movdqu	224(%rcx), %xmm7
	por	%xmm7, %xmm0
	movups	%xmm0, 224(%rcx)
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%r9d, %ecx
	andl	$-4, %ecx
	addl	%ecx, %eax
	cmpl	%r9d, %ecx
	je	.L3
.L8:
	movslq	%eax, %rsi
	leal	1(%rax), %ecx
	orl	%r8d, (%rdi,%rsi,4)
	cmpl	%edx, %ecx
	jge	.L3
	movslq	%ecx, %rcx
	addl	$2, %eax
	orl	%r8d, (%rdi,%rcx,4)
	cmpl	%edx, %eax
	jge	.L3
.L161:
	cltq
	orl	%r8d, (%rdi,%rax,4)
	ret
.L163:
	ret
.L22:
	xorl	%ecx, %ecx
	jmp	.L18
	.cfi_endproc
.LFE2324:
	.size	_ZN6icu_67L12set32x64BitsEPjii, .-_ZN6icu_67L12set32x64BitsEPjii
	.align 2
	.p2align 4
	.globl	_ZN6icu_676BMPSetD0Ev
	.type	_ZN6icu_676BMPSetD0Ev, @function
_ZN6icu_676BMPSetD0Ev:
.LFB2323:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2323:
	.size	_ZN6icu_676BMPSetD0Ev, .-_ZN6icu_676BMPSetD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676BMPSet8containsEi
	.type	_ZNK6icu_676BMPSet8containsEi, @function
_ZNK6icu_676BMPSet8containsEi:
.LFB2328:
	.cfi_startproc
	endbr64
	cmpl	$255, %esi
	jbe	.L184
	cmpl	$2047, %esi
	jbe	.L185
	leal	-57344(%rsi), %eax
	cmpl	$8191, %eax
	jbe	.L181
	cmpl	$55295, %esi
	jbe	.L181
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	jbe	.L186
.L165:
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	movl	%esi, %eax
	sarl	$6, %esi
	andl	$63, %eax
	movl	%esi, %ecx
	movl	268(%rdi,%rax,4), %eax
	shrl	%cl, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	movslq	%esi, %rsi
	movzbl	8(%rdi,%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	movl	%esi, %eax
	movl	%esi, %ecx
	sarl	$6, %eax
	sarl	$12, %ecx
	andl	$63, %eax
	movl	524(%rdi,%rax,4), %eax
	shrl	%cl, %eax
	andl	$65537, %eax
	cmpl	$1, %eax
	jbe	.L165
	movslq	%ecx, %rcx
	movq	856(%rdi), %r8
	leaq	(%rdi,%rcx,4), %rdx
	movl	780(%rdx), %ecx
	movslq	%ecx, %rdi
	movl	%ecx, %eax
	cmpl	(%r8,%rdi,4), %esi
	jl	.L175
	movl	784(%rdx), %eax
	cmpl	%ecx, %eax
	jle	.L175
	movslq	%eax, %rdx
	cmpl	-4(%r8,%rdx,4), %esi
	jge	.L175
.L174:
	movl	%eax, %edx
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L188:
	movslq	%edx, %rdi
	cmpl	(%r8,%rdi,4), %esi
	jge	.L187
.L173:
	movl	%edx, %eax
	leal	(%rdx,%rcx), %edx
	sarl	%edx
	cmpl	%ecx, %edx
	jne	.L188
.L175:
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	movslq	832(%rdi), %rdx
	movq	856(%rdi), %r8
	movq	%rdx, %rcx
	movl	%edx, %eax
	cmpl	(%r8,%rdx,4), %esi
	jl	.L175
	movl	848(%rdi), %eax
	cmpl	%edx, %eax
	jle	.L175
	movslq	%eax, %rdx
	cmpl	-4(%r8,%rdx,4), %esi
	jge	.L175
.L177:
	movl	%eax, %edx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L190:
	movslq	%edx, %rdi
	cmpl	(%r8,%rdi,4), %esi
	jge	.L189
.L176:
	movl	%edx, %eax
	leal	(%rdx,%rcx), %edx
	sarl	%edx
	cmpl	%ecx, %edx
	jne	.L190
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L187:
	movl	%edx, %ecx
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L189:
	movl	%edx, %ecx
	jmp	.L177
	.cfi_endproc
.LFE2328:
	.size	_ZNK6icu_676BMPSet8containsEi, .-_ZNK6icu_676BMPSet8containsEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_676BMPSetC2ERKS0_PKii
	.type	_ZN6icu_676BMPSetC2ERKS0_PKii, @function
_ZN6icu_676BMPSetC2ERKS0_PKii:
.LFB2318:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676BMPSetE(%rip), %rax
	movq	%rdx, 856(%rdi)
	movq	%rax, (%rdi)
	movzbl	264(%rsi), %eax
	movl	%ecx, 864(%rdi)
	movb	%al, 264(%rdi)
	movdqu	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
	movdqu	24(%rsi), %xmm1
	movups	%xmm1, 24(%rdi)
	movdqu	40(%rsi), %xmm2
	movups	%xmm2, 40(%rdi)
	movdqu	56(%rsi), %xmm3
	movups	%xmm3, 56(%rdi)
	movdqu	72(%rsi), %xmm4
	movups	%xmm4, 72(%rdi)
	movdqu	88(%rsi), %xmm5
	movups	%xmm5, 88(%rdi)
	movdqu	104(%rsi), %xmm6
	movups	%xmm6, 104(%rdi)
	movdqu	120(%rsi), %xmm7
	movups	%xmm7, 120(%rdi)
	movdqu	136(%rsi), %xmm0
	movups	%xmm0, 136(%rdi)
	movdqu	152(%rsi), %xmm1
	movups	%xmm1, 152(%rdi)
	movdqu	168(%rsi), %xmm2
	movups	%xmm2, 168(%rdi)
	movdqu	184(%rsi), %xmm3
	movups	%xmm3, 184(%rdi)
	movdqu	200(%rsi), %xmm4
	movups	%xmm4, 200(%rdi)
	movdqu	216(%rsi), %xmm5
	movups	%xmm5, 216(%rdi)
	movdqu	232(%rsi), %xmm6
	movups	%xmm6, 232(%rdi)
	movdqu	248(%rsi), %xmm7
	movups	%xmm7, 248(%rdi)
	movdqu	268(%rsi), %xmm0
	movups	%xmm0, 268(%rdi)
	movdqu	284(%rsi), %xmm1
	movups	%xmm1, 284(%rdi)
	movdqu	300(%rsi), %xmm2
	movups	%xmm2, 300(%rdi)
	movdqu	316(%rsi), %xmm3
	movups	%xmm3, 316(%rdi)
	movdqu	332(%rsi), %xmm4
	movups	%xmm4, 332(%rdi)
	movdqu	348(%rsi), %xmm5
	movups	%xmm5, 348(%rdi)
	movdqu	364(%rsi), %xmm6
	movups	%xmm6, 364(%rdi)
	movdqu	380(%rsi), %xmm7
	movups	%xmm7, 380(%rdi)
	movdqu	396(%rsi), %xmm0
	movups	%xmm0, 396(%rdi)
	movdqu	412(%rsi), %xmm1
	movups	%xmm1, 412(%rdi)
	movdqu	428(%rsi), %xmm2
	movups	%xmm2, 428(%rdi)
	movdqu	444(%rsi), %xmm3
	movups	%xmm3, 444(%rdi)
	movdqu	460(%rsi), %xmm4
	movups	%xmm4, 460(%rdi)
	movdqu	476(%rsi), %xmm5
	movups	%xmm5, 476(%rdi)
	movdqu	492(%rsi), %xmm6
	movups	%xmm6, 492(%rdi)
	movdqu	508(%rsi), %xmm7
	movups	%xmm7, 508(%rdi)
	movdqu	524(%rsi), %xmm0
	movups	%xmm0, 524(%rdi)
	movdqu	540(%rsi), %xmm1
	movups	%xmm1, 540(%rdi)
	movdqu	556(%rsi), %xmm2
	movups	%xmm2, 556(%rdi)
	movdqu	572(%rsi), %xmm3
	movups	%xmm3, 572(%rdi)
	movdqu	588(%rsi), %xmm4
	movups	%xmm4, 588(%rdi)
	movdqu	604(%rsi), %xmm5
	movups	%xmm5, 604(%rdi)
	movdqu	620(%rsi), %xmm6
	movups	%xmm6, 620(%rdi)
	movdqu	636(%rsi), %xmm7
	movups	%xmm7, 636(%rdi)
	movdqu	652(%rsi), %xmm0
	movups	%xmm0, 652(%rdi)
	movdqu	668(%rsi), %xmm1
	movups	%xmm1, 668(%rdi)
	movdqu	684(%rsi), %xmm2
	movups	%xmm2, 684(%rdi)
	movdqu	700(%rsi), %xmm3
	movups	%xmm3, 700(%rdi)
	movdqu	716(%rsi), %xmm4
	movups	%xmm4, 716(%rdi)
	movdqu	732(%rsi), %xmm5
	movups	%xmm5, 732(%rdi)
	movdqu	748(%rsi), %xmm6
	movups	%xmm6, 748(%rdi)
	movdqu	764(%rsi), %xmm7
	movups	%xmm7, 764(%rdi)
	movdqu	780(%rsi), %xmm0
	movups	%xmm0, 780(%rdi)
	movdqu	796(%rsi), %xmm1
	movups	%xmm1, 796(%rdi)
	movdqu	812(%rsi), %xmm2
	movups	%xmm2, 812(%rdi)
	movdqu	828(%rsi), %xmm3
	movups	%xmm3, 828(%rdi)
	movq	844(%rsi), %rdx
	movq	%rdx, 844(%rdi)
	ret
	.cfi_endproc
.LFE2318:
	.size	_ZN6icu_676BMPSetC2ERKS0_PKii, .-_ZN6icu_676BMPSetC2ERKS0_PKii
	.globl	_ZN6icu_676BMPSetC1ERKS0_PKii
	.set	_ZN6icu_676BMPSetC1ERKS0_PKii,_ZN6icu_676BMPSetC2ERKS0_PKii
	.align 2
	.p2align 4
	.globl	_ZN6icu_676BMPSet8initBitsEv
	.type	_ZN6icu_676BMPSet8initBitsEv, @function
_ZN6icu_676BMPSet8initBitsEv:
.LFB2325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	856(%rdi), %rdi
	movl	864(%rbx), %r8d
	.p2align 4,,10
	.p2align 3
.L199:
	movslq	%esi, %rax
	leal	1(%rsi), %edx
	leaq	0(,%rax,4), %rcx
	movl	(%rdi,%rax,4), %eax
	cmpl	%edx, %r8d
	jle	.L212
	movl	4(%rdi,%rcx), %ecx
	addl	$2, %esi
.L193:
	cmpl	$255, %eax
	jg	.L198
	addl	$1, %eax
	cltq
	.p2align 4,,10
	.p2align 3
.L196:
	movl	%eax, %edx
	movb	$1, 7(%rbx,%rax)
	addq	$1, %rax
	cmpl	%edx, %ecx
	jle	.L217
	cmpl	$255, %edx
	jle	.L196
.L217:
	cmpl	$256, %ecx
	jle	.L199
.L198:
	movl	$1, %edx
	xorl	%eax, %eax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L233:
	movl	4(%rdi,%rax,4), %r12d
	leal	2(%rax), %r13d
	addl	$2, %edx
	addq	$2, %rax
	cmpl	$128, %r12d
	jg	.L200
.L195:
	movslq	%eax, %rcx
	movl	%edx, %r13d
	cmpl	%edx, %r8d
	jg	.L233
	movl	$1114112, %r12d
.L200:
	movl	(%rdi,%rcx,4), %eax
	movl	$128, %esi
	movl	$2048, %r14d
	leaq	268(%rbx), %rdi
	cmpl	$128, %eax
	cmovge	%eax, %esi
	cmpl	$2047, %eax
	jle	.L203
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L234:
	movl	4(%rdx,%r8), %r12d
	addl	$2, %r13d
	cmpl	$2047, %esi
	jg	.L201
.L203:
	cmpl	$2048, %r12d
	movl	%r14d, %edx
	cmovle	%r12d, %edx
	call	_ZN6icu_67L12set32x64BitsEPjii
	cmpl	$2048, %r12d
	jg	.L214
	movq	856(%rbx), %rdx
	movslq	%r13d, %rax
	leal	1(%r13), %ecx
	leaq	0(,%rax,4), %r8
	movl	(%rdx,%rax,4), %esi
	cmpl	%ecx, 864(%rbx)
	jg	.L234
	movl	%ecx, %r13d
	movl	$1114112, %r12d
	cmpl	$2047, %esi
	jle	.L203
.L201:
	cmpl	$65535, %esi
	jg	.L192
.L205:
	movl	$2048, %r14d
	movl	$65536, %r15d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L236:
	movl	4(%rdx,%rdi), %r12d
	addl	$2, %r13d
	cmpl	$65535, %esi
	jg	.L192
.L207:
	cmpl	$65536, %r12d
	cmovg	%r15d, %r12d
	cmpl	%esi, %r14d
	cmovge	%r14d, %esi
	cmpl	%esi, %r12d
	jle	.L208
	testb	$63, %sil
	je	.L209
	movl	%esi, %r14d
	sarl	$12, %esi
	movl	$65537, %eax
	sarl	$6, %r14d
	movl	%esi, %ecx
	movl	%r14d, %edx
	addl	$1, %r14d
	sall	%cl, %eax
	andl	$63, %edx
	sall	$6, %r14d
	orl	%eax, 524(%rbx,%rdx,4)
	cmpl	%r14d, %r12d
	jg	.L235
.L208:
	cmpl	$65536, %r12d
	je	.L192
	movq	856(%rbx), %rdx
	movslq	%r13d, %rax
	leal	1(%r13), %ecx
	leaq	0(,%rax,4), %rdi
	movl	(%rdx,%rax,4), %esi
	cmpl	%ecx, 864(%rbx)
	jg	.L236
	movl	%ecx, %r13d
	movl	$1114112, %r12d
	cmpl	$65535, %esi
	jle	.L207
.L192:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	movl	%edx, %esi
	movl	$1114112, %ecx
	jmp	.L193
.L235:
	movl	%r14d, %esi
	.p2align 4,,10
	.p2align 3
.L209:
	movl	%r12d, %eax
	andl	$-64, %eax
	cmpl	%esi, %eax
	jg	.L237
.L210:
	testb	$63, %r12b
	je	.L208
	movl	%r12d, %eax
	sarl	$12, %r12d
	movl	$65537, %edx
	sarl	$6, %eax
	movl	%r12d, %ecx
	movl	%eax, %esi
	leal	1(%rax), %r12d
	sall	%cl, %edx
	andl	$63, %esi
	sall	$6, %r12d
	orl	%edx, 524(%rbx,%rsi,4)
	movl	%r12d, %r14d
	jmp	.L208
.L237:
	movl	%r12d, %edx
	sarl	$6, %esi
	leaq	524(%rbx), %rdi
	sarl	$6, %edx
	call	_ZN6icu_67L12set32x64BitsEPjii
	jmp	.L210
.L214:
	movl	$2048, %esi
	jmp	.L205
	.cfi_endproc
.LFE2325:
	.size	_ZN6icu_676BMPSet8initBitsEv, .-_ZN6icu_676BMPSet8initBitsEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676BMPSetC2EPKii
	.type	_ZN6icu_676BMPSetC2EPKii, @function
_ZN6icu_676BMPSetC2EPKii:
.LFB2315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676BMPSetE(%rip), %rax
	leal	-1(%rdx), %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$16, %rdi
	movl	%ebx, %ecx
	leaq	532(%rbx), %r8
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	xorl	%eax, %eax
	movl	%edx, 848(%rdi)
	xorl	%edx, %edx
	movq	$0, -8(%rdi)
	movq	$0, 240(%rdi)
	movq	%rsi, 840(%rdi)
	andq	$-8, %rdi
	subl	%edi, %ecx
	addl	$264, %ecx
	shrl	$3, %ecx
	rep stosq
	leaq	276(%rbx), %rdi
	movl	%ebx, %ecx
	movq	$0, 268(%rbx)
	movq	$0, 516(%rbx)
	andq	$-8, %rdi
	subl	%edi, %ecx
	addl	$524, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%r8, %rdi
	movl	%ebx, %ecx
	movq	$0, 524(%rbx)
	movq	$0, 772(%rbx)
	andq	$-8, %rdi
	subl	%edi, %ecx
	addl	$780, %ecx
	shrl	$3, %ecx
	rep stosq
	movslq	%r10d, %rdi
	leaq	-4(%rsi,%rdi,4), %r11
	movl	(%rsi), %eax
	cmpl	$2048, %eax
	jg	.L239
	testl	%r10d, %r10d
	jle	.L264
	xorl	%ecx, %ecx
	cmpl	$2048, (%r11)
	movl	%r10d, %eax
	jg	.L243
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L242:
	movslq	%eax, %rdi
	cmpl	$2048, (%rsi,%rdi,4)
	jle	.L265
.L243:
	movl	%eax, %edx
	leal	(%rax,%rcx), %eax
	sarl	%eax
	cmpl	%ecx, %eax
	jne	.L242
	movslq	%edx, %rax
	movl	(%rsi,%rax,4), %eax
.L239:
	movl	%edx, 780(%rbx)
	leaq	780(%rbx), %rdi
	movl	$4096, %ecx
	.p2align 4,,10
	.p2align 3
.L248:
	movl	(%rdi), %edx
	cmpl	%ecx, %eax
	jg	.L244
	cmpl	%edx, %r10d
	jle	.L257
	cmpl	%ecx, (%r11)
	jle	.L257
	movl	%r10d, %eax
	movl	%edx, %r8d
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L267:
	movslq	%eax, %r9
	cmpl	%ecx, (%rsi,%r9,4)
	jle	.L266
.L245:
	movl	%eax, %edx
	leal	(%rax,%r8), %eax
	sarl	%eax
	cmpl	%r8d, %eax
	jne	.L267
.L244:
	addl	$4096, %ecx
	movl	%edx, 4(%rdi)
	addq	$4, %rdi
	cmpl	$69632, %ecx
	je	.L247
	movslq	%edx, %rax
	movl	(%rsi,%rax,4), %eax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L257:
	movl	%r10d, %edx
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L266:
	movl	%eax, %r8d
	movl	%edx, %eax
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L247:
	movslq	840(%rbx), %rax
	movl	%r10d, 848(%rbx)
	cmpl	$65533, (%rsi,%rax,4)
	movq	%rax, %rcx
	movl	%eax, %edx
	jg	.L249
	movl	844(%rbx), %edx
	cmpl	%eax, %edx
	jle	.L249
	movslq	%edx, %rax
	cmpl	$65533, -4(%rsi,%rax,4)
	jle	.L249
.L251:
	movl	%edx, %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L269:
	movslq	%eax, %rdi
	cmpl	$65533, (%rsi,%rdi,4)
	jle	.L268
.L250:
	movl	%eax, %edx
	leal	(%rax,%rcx), %eax
	sarl	%eax
	cmpl	%ecx, %eax
	jne	.L269
.L249:
	andl	$1, %edx
	movq	%rbx, %rdi
	movb	%dl, 264(%rbx)
	call	_ZN6icu_676BMPSet8initBitsEv
	cmpb	$0, 264(%rbx)
	jne	.L270
	movdqa	.LC2(%rip), %xmm0
	movdqu	652(%rbx), %xmm1
	movdqu	764(%rbx), %xmm6
	pand	%xmm0, %xmm1
	movups	%xmm1, 652(%rbx)
	movdqu	668(%rbx), %xmm1
	pand	%xmm0, %xmm1
	movups	%xmm1, 668(%rbx)
	movdqu	684(%rbx), %xmm1
	pand	%xmm0, %xmm1
	movups	%xmm1, 684(%rbx)
	movdqu	700(%rbx), %xmm1
	pand	%xmm0, %xmm1
	movups	%xmm1, 700(%rbx)
	movdqu	716(%rbx), %xmm1
	pand	%xmm0, %xmm1
	movups	%xmm1, 716(%rbx)
	movdqu	732(%rbx), %xmm1
	pand	%xmm0, %xmm1
	movups	%xmm1, 732(%rbx)
	movdqu	748(%rbx), %xmm1
	pand	%xmm0, %xmm1
	pand	%xmm6, %xmm0
	movups	%xmm1, 748(%rbx)
	movups	%xmm0, 764(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	movdqa	.LC0(%rip), %xmm0
	movdqu	268(%rbx), %xmm1
	movdqu	508(%rbx), %xmm3
	por	%xmm0, %xmm1
	movups	%xmm1, 268(%rbx)
	movdqu	284(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 284(%rbx)
	movdqu	300(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 300(%rbx)
	movdqu	316(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 316(%rbx)
	movdqu	332(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 332(%rbx)
	movdqu	348(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 348(%rbx)
	movdqu	364(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 364(%rbx)
	movdqu	380(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 380(%rbx)
	movdqu	396(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 396(%rbx)
	movdqu	412(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 412(%rbx)
	movdqu	428(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 428(%rbx)
	movdqu	444(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 444(%rbx)
	movdqu	460(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 460(%rbx)
	movdqu	476(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 476(%rbx)
	movdqu	492(%rbx), %xmm1
	por	%xmm0, %xmm1
	por	%xmm3, %xmm0
	movups	%xmm1, 492(%rbx)
	movups	%xmm0, 508(%rbx)
	movdqa	.LC1(%rip), %xmm0
	movdqu	524(%rbx), %xmm1
	movdqu	636(%rbx), %xmm4
	movdqu	652(%rbx), %xmm2
	por	%xmm0, %xmm1
	movups	%xmm1, 524(%rbx)
	movdqu	540(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 540(%rbx)
	movdqu	556(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 556(%rbx)
	movdqu	572(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 572(%rbx)
	movdqu	588(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 588(%rbx)
	movdqu	604(%rbx), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 604(%rbx)
	movdqu	620(%rbx), %xmm1
	por	%xmm0, %xmm1
	por	%xmm4, %xmm0
	movups	%xmm1, 620(%rbx)
	movdqa	.LC2(%rip), %xmm1
	movups	%xmm0, 636(%rbx)
	movdqa	.LC3(%rip), %xmm0
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 652(%rbx)
	movdqu	668(%rbx), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 668(%rbx)
	movdqu	684(%rbx), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 684(%rbx)
	movdqu	700(%rbx), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 700(%rbx)
	movdqu	716(%rbx), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 716(%rbx)
	movdqu	732(%rbx), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 732(%rbx)
	movdqu	748(%rbx), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 748(%rbx)
	movdqu	764(%rbx), %xmm5
	pand	%xmm5, %xmm1
	por	%xmm1, %xmm0
	movups	%xmm0, 764(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	movl	(%rsi,%rdi,4), %eax
	movl	%r10d, %edx
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L265:
	movl	%eax, %ecx
	movl	%edx, %eax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L268:
	movl	%eax, %ecx
	jmp	.L251
	.cfi_endproc
.LFE2315:
	.size	_ZN6icu_676BMPSetC2EPKii, .-_ZN6icu_676BMPSetC2EPKii
	.globl	_ZN6icu_676BMPSetC1EPKii
	.set	_ZN6icu_676BMPSetC1EPKii,_ZN6icu_676BMPSetC2EPKii
	.align 2
	.p2align 4
	.globl	_ZN6icu_676BMPSet15overrideIllegalEv
	.type	_ZN6icu_676BMPSet15overrideIllegalEv, @function
_ZN6icu_676BMPSet15overrideIllegalEv:
.LFB2326:
	.cfi_startproc
	endbr64
	cmpb	$0, 264(%rdi)
	je	.L272
	movdqa	.LC0(%rip), %xmm0
	movdqu	268(%rdi), %xmm1
	movdqu	508(%rdi), %xmm3
	por	%xmm0, %xmm1
	movups	%xmm1, 268(%rdi)
	movdqu	284(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 284(%rdi)
	movdqu	300(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 300(%rdi)
	movdqu	316(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 316(%rdi)
	movdqu	332(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 332(%rdi)
	movdqu	348(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 348(%rdi)
	movdqu	364(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 364(%rdi)
	movdqu	380(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 380(%rdi)
	movdqu	396(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 396(%rdi)
	movdqu	412(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 412(%rdi)
	movdqu	428(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 428(%rdi)
	movdqu	444(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 444(%rdi)
	movdqu	460(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 460(%rdi)
	movdqu	476(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 476(%rdi)
	movdqu	492(%rdi), %xmm1
	por	%xmm0, %xmm1
	por	%xmm3, %xmm0
	movups	%xmm1, 492(%rdi)
	movups	%xmm0, 508(%rdi)
	movdqa	.LC1(%rip), %xmm0
	movdqu	524(%rdi), %xmm1
	movdqu	636(%rdi), %xmm4
	movdqu	652(%rdi), %xmm2
	por	%xmm0, %xmm1
	movups	%xmm1, 524(%rdi)
	movdqu	540(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 540(%rdi)
	movdqu	556(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 556(%rdi)
	movdqu	572(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 572(%rdi)
	movdqu	588(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 588(%rdi)
	movdqu	604(%rdi), %xmm1
	por	%xmm0, %xmm1
	movups	%xmm1, 604(%rdi)
	movdqu	620(%rdi), %xmm1
	por	%xmm0, %xmm1
	por	%xmm4, %xmm0
	movups	%xmm1, 620(%rdi)
	movdqa	.LC2(%rip), %xmm1
	movups	%xmm0, 636(%rdi)
	movdqa	.LC3(%rip), %xmm0
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 652(%rdi)
	movdqu	668(%rdi), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 668(%rdi)
	movdqu	684(%rdi), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 684(%rdi)
	movdqu	700(%rdi), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 700(%rdi)
	movdqu	716(%rdi), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 716(%rdi)
	movdqu	732(%rdi), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 732(%rdi)
	movdqu	748(%rdi), %xmm2
	pand	%xmm1, %xmm2
	por	%xmm0, %xmm2
	movups	%xmm2, 748(%rdi)
	movdqu	764(%rdi), %xmm5
	pand	%xmm5, %xmm1
	por	%xmm1, %xmm0
	movups	%xmm0, 764(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	movdqa	.LC2(%rip), %xmm0
	movdqu	652(%rdi), %xmm1
	movdqu	764(%rdi), %xmm6
	pand	%xmm0, %xmm1
	movups	%xmm1, 652(%rdi)
	movdqu	668(%rdi), %xmm1
	pand	%xmm0, %xmm1
	movups	%xmm1, 668(%rdi)
	movdqu	684(%rdi), %xmm1
	pand	%xmm0, %xmm1
	movups	%xmm1, 684(%rdi)
	movdqu	700(%rdi), %xmm1
	pand	%xmm0, %xmm1
	movups	%xmm1, 700(%rdi)
	movdqu	716(%rdi), %xmm1
	pand	%xmm0, %xmm1
	movups	%xmm1, 716(%rdi)
	movdqu	732(%rdi), %xmm1
	pand	%xmm0, %xmm1
	movups	%xmm1, 732(%rdi)
	movdqu	748(%rdi), %xmm1
	pand	%xmm0, %xmm1
	pand	%xmm6, %xmm0
	movups	%xmm1, 748(%rdi)
	movups	%xmm0, 764(%rdi)
	ret
	.cfi_endproc
.LFE2326:
	.size	_ZN6icu_676BMPSet15overrideIllegalEv, .-_ZN6icu_676BMPSet15overrideIllegalEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676BMPSet13findCodePointEiii
	.type	_ZNK6icu_676BMPSet13findCodePointEiii, @function
_ZNK6icu_676BMPSet13findCodePointEiii:
.LFB2327:
	.cfi_startproc
	endbr64
	movq	856(%rdi), %r8
	movslq	%edx, %rdi
	movl	%edx, %eax
	cmpl	%esi, (%r8,%rdi,4)
	jg	.L274
	movl	%ecx, %eax
	cmpl	%ecx, %edx
	jge	.L274
	movslq	%ecx, %rdi
	cmpl	%esi, -4(%r8,%rdi,4)
	jg	.L276
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L283:
	movslq	%ecx, %rdi
	cmpl	%esi, (%r8,%rdi,4)
	jle	.L282
.L276:
	movl	%ecx, %eax
	leal	(%rdx,%rcx), %ecx
	sarl	%ecx
	cmpl	%ecx, %edx
	jne	.L283
.L274:
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	movl	%ecx, %edx
	movl	%eax, %ecx
	jmp	.L276
	.cfi_endproc
.LFE2327:
	.size	_ZNK6icu_676BMPSet13findCodePointEiii, .-_ZNK6icu_676BMPSet13findCodePointEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676BMPSet4spanEPKDsS2_17USetSpanCondition
	.type	_ZNK6icu_676BMPSet4spanEPKDsS2_17USetSpanCondition, @function
_ZNK6icu_676BMPSet4spanEPKDsS2_17USetSpanCondition:
.LFB2329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	testl	%ecx, %ecx
	je	.L285
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L354:
	movslq	%esi, %rsi
	cmpb	$0, 8(%rdi,%rsi)
	je	.L284
.L288:
	addq	$2, %rax
	cmpq	%rax, %rdx
	jbe	.L284
.L303:
	movzwl	(%rax), %esi
	movl	%esi, %ecx
	cmpw	$255, %si
	jbe	.L354
	cmpw	$2047, %si
	ja	.L289
	andl	$63, %ecx
	sarl	$6, %esi
	movl	268(%rdi,%rcx,4), %ecx
	btl	%esi, %ecx
	jc	.L288
.L284:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movslq	%esi, %rsi
	cmpb	$0, 8(%rdi,%rsi)
	jne	.L284
.L305:
	addq	$2, %rax
	cmpq	%rax, %rdx
	jbe	.L284
.L285:
	movzwl	(%rax), %esi
	movl	%esi, %ecx
	cmpw	$255, %si
	jbe	.L355
	cmpw	$2047, %si
	ja	.L306
	andl	$63, %ecx
	sarl	$6, %esi
	movl	268(%rdi,%rcx,4), %ecx
	btl	%esi, %ecx
	jnc	.L305
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L306:
	leal	10240(%rsi), %r8d
	cmpw	$2047, %r8w
	jbe	.L307
	movl	%esi, %r8d
	sarl	$12, %ecx
	sarl	$6, %r8d
	andl	$63, %r8d
	movl	524(%rdi,%r8,4), %r8d
	shrl	%cl, %r8d
	andl	$65537, %r8d
	cmpl	$1, %r8d
	ja	.L308
	testl	%r8d, %r8d
	je	.L305
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L289:
	leal	10240(%rsi), %r8d
	cmpw	$2047, %r8w
	jbe	.L290
	movl	%esi, %r8d
	sarl	$12, %ecx
	sarl	$6, %r8d
	andl	$63, %r8d
	movl	524(%rdi,%r8,4), %r8d
	shrl	%cl, %r8d
	andl	$65537, %r8d
	cmpl	$1, %r8d
	ja	.L291
	testl	%r8d, %r8d
	jne	.L288
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L307:
	movq	856(%rdi), %r11
	cmpw	$-9217, %si
	ja	.L312
	leaq	2(%rax), %r8
	cmpq	%rdx, %r8
	je	.L312
	movzwl	2(%rax), %ecx
	leal	9216(%rcx), %r9d
	cmpw	$1023, %r9w
	jbe	.L356
.L312:
	movslq	832(%rdi), %rcx
	movq	%rcx, %r9
	movl	%ecx, %r8d
	cmpl	%esi, (%r11,%rcx,4)
	jg	.L316
	movl	836(%rdi), %r8d
	cmpl	%ecx, %r8d
	jle	.L316
	movslq	%r8d, %rcx
	cmpl	%esi, -4(%r11,%rcx,4)
	jle	.L316
.L318:
	movl	%r8d, %ecx
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L358:
	movslq	%ecx, %r10
	cmpl	%esi, (%r11,%r10,4)
	jle	.L357
.L317:
	movl	%ecx, %r8d
	leal	(%rcx,%r9), %ecx
	sarl	%ecx
	cmpl	%r9d, %ecx
	jne	.L358
	.p2align 4,,10
	.p2align 3
.L316:
	andl	$1, %r8d
	je	.L305
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L290:
	movq	856(%rdi), %r11
	cmpw	$-9217, %si
	ja	.L295
	leaq	2(%rax), %r8
	cmpq	%rdx, %r8
	je	.L295
	movzwl	2(%rax), %ecx
	leal	9216(%rcx), %r9d
	cmpw	$1023, %r9w
	jbe	.L296
.L295:
	movslq	832(%rdi), %rcx
	movq	%rcx, %r9
	movl	%ecx, %r8d
	cmpl	%esi, (%r11,%rcx,4)
	jg	.L297
	movl	836(%rdi), %r8d
	cmpl	%ecx, %r8d
	jle	.L297
	movslq	%r8d, %rcx
	cmpl	%esi, -4(%r11,%rcx,4)
	jle	.L297
.L299:
	movl	%r8d, %ecx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L360:
	movslq	%ecx, %r10
	cmpl	%esi, (%r11,%r10,4)
	jle	.L359
.L298:
	movl	%ecx, %r8d
	leal	(%rcx,%r9), %ecx
	sarl	%ecx
	cmpl	%r9d, %ecx
	jne	.L360
	.p2align 4,,10
	.p2align 3
.L297:
	andl	$1, %r8d
	jne	.L288
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L308:
	movslq	%ecx, %rcx
	movq	856(%rdi), %r11
	leaq	(%rdi,%rcx,4), %rcx
	movslq	780(%rcx), %r10
	movq	%r10, %r9
	movl	%r10d, %r8d
	cmpl	%esi, (%r11,%r10,4)
	jg	.L316
	movl	784(%rcx), %r8d
	cmpl	%r10d, %r8d
	jle	.L316
	movslq	%r8d, %rcx
	cmpl	%esi, -4(%r11,%rcx,4)
	jle	.L316
.L311:
	movl	%r8d, %ecx
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L362:
	movslq	%ecx, %r10
	cmpl	%esi, (%r11,%r10,4)
	jle	.L361
.L310:
	movl	%ecx, %r8d
	leal	(%rcx,%r9), %ecx
	sarl	%ecx
	cmpl	%r9d, %ecx
	jne	.L362
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L291:
	movslq	%ecx, %rcx
	movq	856(%rdi), %r11
	leaq	(%rdi,%rcx,4), %rcx
	movslq	780(%rcx), %r10
	movq	%r10, %r9
	movl	%r10d, %r8d
	cmpl	%esi, (%r11,%r10,4)
	jg	.L297
	movl	784(%rcx), %r8d
	cmpl	%r10d, %r8d
	jle	.L297
	movslq	%r8d, %rcx
	cmpl	%esi, -4(%r11,%rcx,4)
	jle	.L297
.L294:
	movl	%r8d, %ecx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L364:
	movslq	%ecx, %r10
	cmpl	%esi, (%r11,%r10,4)
	jle	.L363
.L293:
	movl	%ecx, %r8d
	leal	(%rcx,%r9), %ecx
	sarl	%ecx
	cmpl	%r9d, %ecx
	jne	.L364
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L359:
	movl	%ecx, %r9d
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L357:
	movl	%ecx, %r9d
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L361:
	movl	%ecx, %r9d
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L363:
	movl	%ecx, %r9d
	jmp	.L294
.L356:
	movl	844(%rdi), %r9d
	sall	$10, %esi
	leal	-56613888(%rsi,%rcx), %ebx
	movslq	%r9d, %rcx
	movl	%r9d, %esi
	cmpl	(%r11,%rcx,4), %ebx
	jl	.L313
	movl	848(%rdi), %esi
	cmpl	%r9d, %esi
	jle	.L313
	movslq	%esi, %rcx
	cmpl	-4(%r11,%rcx,4), %ebx
	jge	.L313
.L315:
	movl	%esi, %ecx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L366:
	movslq	%ecx, %r10
	cmpl	(%r11,%r10,4), %ebx
	jge	.L365
.L314:
	movl	%ecx, %esi
	leal	(%rcx,%r9), %ecx
	sarl	%ecx
	cmpl	%r9d, %ecx
	jne	.L366
.L313:
	andl	$1, %esi
	jne	.L284
	movq	%r8, %rax
	jmp	.L305
.L296:
	movl	844(%rdi), %r9d
	sall	$10, %esi
	leal	-56613888(%rsi,%rcx), %ebx
	movslq	%r9d, %rcx
	movl	%r9d, %esi
	cmpl	(%r11,%rcx,4), %ebx
	jl	.L300
	movl	848(%rdi), %esi
	cmpl	%r9d, %esi
	jle	.L300
	movslq	%esi, %rcx
	cmpl	-4(%r11,%rcx,4), %ebx
	jge	.L300
.L302:
	movl	%esi, %ecx
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L368:
	movslq	%ecx, %r10
	cmpl	(%r11,%r10,4), %ebx
	jge	.L367
.L301:
	movl	%ecx, %esi
	leal	(%rcx,%r9), %ecx
	sarl	%ecx
	cmpl	%r9d, %ecx
	jne	.L368
.L300:
	andl	$1, %esi
	je	.L284
	movq	%r8, %rax
	jmp	.L288
.L365:
	movl	%ecx, %r9d
	jmp	.L315
.L367:
	movl	%ecx, %r9d
	jmp	.L302
	.cfi_endproc
.LFE2329:
	.size	_ZNK6icu_676BMPSet4spanEPKDsS2_17USetSpanCondition, .-_ZNK6icu_676BMPSet4spanEPKDsS2_17USetSpanCondition
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676BMPSet8spanBackEPKDsS2_17USetSpanCondition
	.type	_ZNK6icu_676BMPSet8spanBackEPKDsS2_17USetSpanCondition, @function
_ZNK6icu_676BMPSet8spanBackEPKDsS2_17USetSpanCondition:
.LFB2330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	testl	%ecx, %ecx
	je	.L370
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L438:
	movslq	%r8d, %r8
	cmpb	$0, 8(%rdi,%r8)
	je	.L424
.L373:
	cmpq	%rsi, %rax
	je	.L369
	movq	%rax, %rdx
.L389:
	movzwl	-2(%rdx), %r8d
	leaq	-2(%rdx), %rax
	movl	%r8d, %ecx
	cmpw	$255, %r8w
	jbe	.L438
	cmpw	$2047, %r8w
	ja	.L374
	andl	$63, %ecx
	sarl	$6, %r8d
	movl	268(%rdi,%rcx,4), %ecx
	btl	%r8d, %ecx
	jc	.L373
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rdx, %rax
.L369:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movslq	%r8d, %r8
	cmpb	$0, 8(%rdi,%r8)
	jne	.L424
.L391:
	cmpq	%rsi, %rax
	je	.L369
	movq	%rax, %rdx
.L370:
	movzwl	-2(%rdx), %r8d
	leaq	-2(%rdx), %rax
	movl	%r8d, %ecx
	cmpw	$255, %r8w
	jbe	.L439
	cmpw	$2047, %r8w
	ja	.L392
	andl	$63, %ecx
	sarl	$6, %r8d
	movl	268(%rdi,%rcx,4), %ecx
	btl	%r8d, %ecx
	jnc	.L391
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L392:
	leal	10240(%r8), %r9d
	cmpw	$2047, %r9w
	jbe	.L393
	movl	%r8d, %r9d
	sarl	$12, %ecx
	sarl	$6, %r9d
	andl	$63, %r9d
	movl	524(%rdi,%r9,4), %r9d
	shrl	%cl, %r9d
	andl	$65537, %r9d
	cmpl	$1, %r9d
	ja	.L394
	testl	%r9d, %r9d
	je	.L391
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L374:
	leal	10240(%r8), %r9d
	cmpw	$2047, %r9w
	jbe	.L375
	movl	%r8d, %r9d
	sarl	$12, %ecx
	sarl	$6, %r9d
	andl	$63, %r9d
	movl	524(%rdi,%r9,4), %r9d
	shrl	%cl, %r9d
	andl	$65537, %r9d
	cmpl	$1, %r9d
	ja	.L376
	testl	%r9d, %r9d
	jne	.L373
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L393:
	movq	856(%rdi), %rbx
	cmpw	$-9217, %r8w
	jbe	.L398
	cmpq	%rsi, %rax
	je	.L398
	movzwl	-4(%rdx), %ecx
	leal	10240(%rcx), %r9d
	cmpw	$1023, %r9w
	jbe	.L440
.L398:
	movslq	832(%rdi), %rcx
	movq	%rcx, %r10
	movl	%ecx, %r9d
	cmpl	%r8d, (%rbx,%rcx,4)
	jg	.L403
	movl	836(%rdi), %r9d
	cmpl	%ecx, %r9d
	jle	.L403
	movslq	%r9d, %rcx
	cmpl	%r8d, -4(%rbx,%rcx,4)
	jle	.L403
.L405:
	movl	%r9d, %ecx
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L442:
	movslq	%ecx, %r11
	cmpl	%r8d, (%rbx,%r11,4)
	jle	.L441
.L404:
	movl	%ecx, %r9d
	leal	(%rcx,%r10), %ecx
	sarl	%ecx
	cmpl	%r10d, %ecx
	jne	.L442
	.p2align 4,,10
	.p2align 3
.L403:
	andl	$1, %r9d
	je	.L391
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L375:
	movq	856(%rdi), %rbx
	cmpw	$-9217, %r8w
	jbe	.L380
	cmpq	%rax, %rsi
	je	.L380
	movzwl	-4(%rdx), %ecx
	leal	10240(%rcx), %r9d
	cmpw	$1023, %r9w
	jbe	.L381
.L380:
	movslq	832(%rdi), %rcx
	movq	%rcx, %r10
	movl	%ecx, %r9d
	cmpl	%r8d, (%rbx,%rcx,4)
	jg	.L382
	movl	836(%rdi), %r9d
	cmpl	%ecx, %r9d
	jle	.L382
	movslq	%r9d, %rcx
	cmpl	%r8d, -4(%rbx,%rcx,4)
	jle	.L382
.L384:
	movl	%r9d, %ecx
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L444:
	movslq	%ecx, %r11
	cmpl	%r8d, (%rbx,%r11,4)
	jle	.L443
.L383:
	movl	%ecx, %r9d
	leal	(%rcx,%r10), %ecx
	sarl	%ecx
	cmpl	%r10d, %ecx
	jne	.L444
	.p2align 4,,10
	.p2align 3
.L382:
	andl	$1, %r9d
	jne	.L373
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L394:
	movslq	%ecx, %rcx
	movq	856(%rdi), %rbx
	leaq	(%rdi,%rcx,4), %rcx
	movslq	780(%rcx), %r11
	movq	%r11, %r10
	movl	%r11d, %r9d
	cmpl	%r8d, (%rbx,%r11,4)
	jg	.L403
	movl	784(%rcx), %r9d
	cmpl	%r11d, %r9d
	jle	.L403
	movslq	%r9d, %rcx
	cmpl	%r8d, -4(%rbx,%rcx,4)
	jle	.L403
.L397:
	movl	%r9d, %ecx
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L446:
	movslq	%ecx, %r11
	cmpl	%r8d, (%rbx,%r11,4)
	jle	.L445
.L396:
	movl	%ecx, %r9d
	leal	(%rcx,%r10), %ecx
	sarl	%ecx
	cmpl	%r10d, %ecx
	jne	.L446
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L376:
	movslq	%ecx, %rcx
	movq	856(%rdi), %rbx
	leaq	(%rdi,%rcx,4), %rcx
	movslq	780(%rcx), %r11
	movq	%r11, %r10
	movl	%r11d, %r9d
	cmpl	%r8d, (%rbx,%r11,4)
	jg	.L382
	movl	784(%rcx), %r9d
	cmpl	%r11d, %r9d
	jle	.L382
	movslq	%r9d, %rcx
	cmpl	%r8d, -4(%rbx,%rcx,4)
	jle	.L382
.L379:
	movl	%r9d, %ecx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L448:
	movslq	%ecx, %r11
	cmpl	%r8d, (%rbx,%r11,4)
	jle	.L447
.L378:
	movl	%ecx, %r9d
	leal	(%rcx,%r10), %ecx
	sarl	%ecx
	cmpl	%r10d, %ecx
	jne	.L448
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L443:
	movl	%ecx, %r10d
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L441:
	movl	%ecx, %r10d
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L445:
	movl	%ecx, %r10d
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L447:
	movl	%ecx, %r10d
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L440:
	movslq	844(%rdi), %rax
	sall	$10, %ecx
	leal	-56613888(%r8,%rcx), %r10d
	movq	%rax, %r9
	movl	%eax, %ecx
	cmpl	(%rbx,%rax,4), %r10d
	jl	.L399
	movl	848(%rdi), %ecx
	cmpl	%eax, %ecx
	jle	.L399
	movslq	%ecx, %rax
	cmpl	-4(%rbx,%rax,4), %r10d
	jge	.L399
.L401:
	movl	%ecx, %eax
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L450:
	movslq	%eax, %r8
	cmpl	(%rbx,%r8,4), %r10d
	jge	.L449
.L400:
	movl	%eax, %ecx
	leal	(%rax,%r9), %eax
	sarl	%eax
	cmpl	%r9d, %eax
	jne	.L450
.L399:
	andl	$1, %ecx
	jne	.L424
	leaq	-4(%rdx), %rax
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L381:
	movslq	844(%rdi), %rax
	sall	$10, %ecx
	leal	-56613888(%r8,%rcx), %r10d
	movq	%rax, %r9
	movl	%eax, %ecx
	cmpl	(%rbx,%rax,4), %r10d
	jl	.L385
	movl	848(%rdi), %ecx
	cmpl	%eax, %ecx
	jle	.L385
	movslq	%ecx, %rax
	cmpl	-4(%rbx,%rax,4), %r10d
	jge	.L385
.L387:
	movl	%ecx, %eax
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L452:
	movslq	%eax, %r8
	cmpl	(%rbx,%r8,4), %r10d
	jge	.L451
.L386:
	movl	%eax, %ecx
	leal	(%rax,%r9), %eax
	sarl	%eax
	cmpl	%r9d, %eax
	jne	.L452
.L385:
	andl	$1, %ecx
	je	.L424
	leaq	-4(%rdx), %rax
	jmp	.L373
.L449:
	movl	%eax, %r9d
	jmp	.L401
.L451:
	movl	%eax, %r9d
	jmp	.L387
	.cfi_endproc
.LFE2330:
	.size	_ZNK6icu_676BMPSet8spanBackEPKDsS2_17USetSpanCondition, .-_ZNK6icu_676BMPSet8spanBackEPKDsS2_17USetSpanCondition
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676BMPSet8spanUTF8EPKhi17USetSpanCondition
	.type	_ZNK6icu_676BMPSet8spanUTF8EPKhi17USetSpanCondition, @function
_ZNK6icu_676BMPSet8spanUTF8EPKhi17USetSpanCondition:
.LFB2331:
	.cfi_startproc
	endbr64
	movzbl	(%rsi), %eax
	movslq	%edx, %r8
	movl	%ecx, %r10d
	addq	%rsi, %r8
	testb	%al, %al
	js	.L454
	testl	%ecx, %ecx
	je	.L455
	cmpb	$0, 8(%rdi,%rax)
	je	.L520
	.p2align 4,,10
	.p2align 3
.L525:
	addq	$1, %rsi
	cmpq	%rsi, %r8
	je	.L511
	movzbl	(%rsi), %eax
	testb	%al, %al
	js	.L457
	cmpb	$0, 8(%rdi,%rax)
	jne	.L525
.L520:
	movq	%rsi, %r8
.L511:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	addq	$1, %rsi
	cmpq	%rsi, %r8
	je	.L511
	movzbl	(%rsi), %eax
	testb	%al, %al
	js	.L457
.L455:
	cmpb	$0, 8(%rdi,%rax)
	je	.L526
	movq	%rsi, %r8
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L457:
	movl	%r8d, %edx
	subl	%esi, %edx
.L454:
	movzbl	-1(%r8), %eax
	testl	%r10d, %r10d
	movl	$1, %r11d
	movq	%r8, %r9
	cmove	%r10d, %r11d
	testb	%al, %al
	js	.L527
.L459:
	cmpq	%r9, %rsi
	jnb	.L511
	movzbl	(%rsi), %eax
	testb	%al, %al
	js	.L523
	testl	%r10d, %r10d
	je	.L515
	cmpb	$0, 8(%rdi,%rax)
	je	.L520
	.p2align 4,,10
	.p2align 3
.L528:
	addq	$1, %rsi
	cmpq	%r9, %rsi
	je	.L511
	movzbl	(%rsi), %eax
	testb	%al, %al
	js	.L523
	cmpb	$0, 8(%rdi,%rax)
	jne	.L528
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L529:
	addq	$1, %rsi
	cmpq	%r9, %rsi
	je	.L511
	movzbl	(%rsi), %eax
	testb	%al, %al
	js	.L523
.L515:
	cmpb	$0, 8(%rdi,%rax)
	je	.L529
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L523:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
.L465:
	cmpb	$-33, %al
	jbe	.L468
	movzbl	1(%rsi), %ebx
	addl	$-128, %ebx
	cmpb	$-17, %al
	ja	.L469
	cmpb	$63, %bl
	ja	.L470
	movzbl	2(%rsi), %ecx
	leal	-128(%rcx), %edx
	cmpb	$63, %dl
	jbe	.L530
	.p2align 4,,10
	.p2align 3
.L470:
	movsbl	264(%rdi), %eax
	cmpl	%r11d, %eax
	jne	.L497
	addq	$1, %rsi
.L476:
	cmpq	%r9, %rsi
	jnb	.L453
	movzbl	(%rsi), %eax
	testb	%al, %al
	js	.L465
	testl	%r10d, %r10d
	je	.L466
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L531:
	addq	$1, %rsi
	cmpq	%r9, %rsi
	je	.L453
	movzbl	(%rsi), %eax
	testb	%al, %al
	js	.L465
.L467:
	cmpb	$0, 8(%rdi,%rax)
	jne	.L531
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L468:
	cmpb	$-65, %al
	jbe	.L470
	movzbl	1(%rsi), %ebx
	leal	-128(%rbx), %edx
	cmpb	$63, %dl
	ja	.L470
	movzbl	%dl, %edx
	movl	%eax, %ecx
	movl	268(%rdi,%rdx,4), %edx
	shrl	%cl, %edx
	andl	$1, %edx
	cmpl	%r11d, %edx
	je	.L532
.L497:
	movq	%rsi, %r8
.L453:
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	cmpb	$-65, %al
	ja	.L460
	cmpl	$1, %edx
	jle	.L459
	movzbl	-2(%r8), %eax
	cmpb	$-33, %al
	ja	.L461
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L459
	cmpl	$2, %edx
	jle	.L459
	cmpb	$-17, -3(%r8)
	jbe	.L459
	movsbl	264(%rdi), %eax
	leaq	-3(%r8), %r9
	cmpl	%r11d, %eax
	cmovne	%r9, %r8
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpb	$63, %bl
	ja	.L470
	movzbl	2(%rsi), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L470
	movzbl	3(%rsi), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L470
	subl	$240, %eax
	movzbl	%dl, %edx
	movzbl	%cl, %ecx
	sall	$18, %eax
	sall	$6, %ecx
	orl	%eax, %edx
	movzbl	%bl, %eax
	sall	$12, %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	leal	-65536(%rax), %edx
	cmpl	$1048575, %edx
	jbe	.L533
	movsbl	264(%rdi), %ecx
.L481:
	cmpl	%ecx, %r11d
	jne	.L497
	addq	$4, %rsi
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movsbl	264(%rdi), %eax
	leaq	-1(%r8), %r9
	cmpl	%r11d, %eax
	cmovne	%r9, %r8
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	addq	$1, %rsi
	cmpq	%r9, %rsi
	je	.L453
	movzbl	(%rsi), %eax
	testb	%al, %al
	js	.L465
.L466:
	cmpb	$0, 8(%rdi,%rax)
	je	.L534
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L530:
	movzbl	%bl, %r12d
	movzbl	%bl, %ebx
	movl	%eax, %ecx
	andl	$15, %eax
	movl	524(%rdi,%rbx,4), %ebx
	andl	$15, %ecx
	shrl	%cl, %ebx
	andl	$65537, %ebx
	cmpl	$1, %ebx
	ja	.L471
	cmpl	%r11d, %ebx
	jne	.L497
.L472:
	addq	$3, %rsi
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movsbl	264(%rdi), %eax
	leaq	-2(%r8), %r9
	cmpl	%r11d, %eax
	cmovne	%r9, %r8
	jmp	.L459
.L532:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	addq	$2, %rsi
	jmp	.L476
.L533:
	movslq	844(%rdi), %rdx
	movq	856(%rdi), %r13
	movq	%rdx, %rbx
	movl	%edx, %ecx
	cmpl	0(%r13,%rdx,4), %eax
	jl	.L478
	movl	848(%rdi), %ecx
	cmpl	%edx, %ecx
	jle	.L478
	movslq	%ecx, %rdx
	cmpl	-4(%r13,%rdx,4), %eax
	jge	.L478
.L480:
	movl	%ecx, %edx
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L536:
	movslq	%edx, %r12
	cmpl	0(%r13,%r12,4), %eax
	jge	.L535
.L479:
	movl	%edx, %ecx
	leal	(%rdx,%rbx), %edx
	sarl	%edx
	cmpl	%ebx, %edx
	jne	.L536
.L478:
	andl	$1, %ecx
	jmp	.L481
.L471:
	sall	$12, %eax
	movzbl	%cl, %ecx
	movzbl	%dl, %edx
	movq	856(%rdi), %r13
	orl	%eax, %edx
	leaq	(%rdi,%rcx,4), %rax
	sall	$6, %r12d
	movslq	780(%rax), %rbx
	orl	%edx, %r12d
	movq	%rbx, %rcx
	movl	%ebx, %edx
	cmpl	0(%r13,%rbx,4), %r12d
	jl	.L473
	movl	784(%rax), %edx
	cmpl	%ebx, %edx
	jle	.L473
	movslq	%edx, %rax
	cmpl	-4(%r13,%rax,4), %r12d
	jge	.L473
.L475:
	movl	%edx, %eax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L538:
	movslq	%eax, %rbx
	cmpl	0(%r13,%rbx,4), %r12d
	jge	.L537
.L474:
	movl	%eax, %edx
	leal	(%rax,%rcx), %eax
	sarl	%eax
	cmpl	%ecx, %eax
	jne	.L538
.L473:
	andl	$1, %edx
	cmpl	%r11d, %edx
	jne	.L497
	jmp	.L472
.L535:
	movl	%edx, %ebx
	jmp	.L480
.L537:
	movl	%eax, %ecx
	jmp	.L475
	.cfi_endproc
.LFE2331:
	.size	_ZNK6icu_676BMPSet8spanUTF8EPKhi17USetSpanCondition, .-_ZNK6icu_676BMPSet8spanUTF8EPKhi17USetSpanCondition
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676BMPSet12spanBackUTF8EPKhi17USetSpanCondition
	.type	_ZNK6icu_676BMPSet12spanBackUTF8EPKhi17USetSpanCondition, @function
_ZNK6icu_676BMPSet12spanBackUTF8EPKhi17USetSpanCondition:
.LFB2332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-52(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$1, %ebx
	subq	$24, %rsp
	testl	%ecx, %ecx
	cmove	%ecx, %ebx
.L562:
	leal	-1(%r9), %edx
	movslq	%edx, %rax
	movl	%edx, -52(%rbp)
	addq	%r14, %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	js	.L540
	xorl	%edi, %edi
	testl	%r13d, %r13d
	je	.L551
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L542:
	testl	%edx, %edx
	je	.L545
	movzbl	-1(%rax), %ecx
	leal	-1(%rdx), %esi
	subq	$1, %rax
	movl	$1, %edi
	testb	%cl, %cl
	js	.L546
	movl	%esi, %edx
.L547:
	cmpb	$0, 8(%r12,%rcx)
	jne	.L542
.L579:
	testb	%dil, %dil
	je	.L549
	movl	%edx, -52(%rbp)
.L549:
	movl	-52(%rbp), %eax
	leal	1(%rax), %r9d
.L539:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L545
	movzbl	-1(%rax), %ecx
	leal	-1(%rdx), %esi
	subq	$1, %rax
	movl	$1, %edi
	testb	%cl, %cl
	js	.L546
	movl	%esi, %edx
.L551:
	cmpb	$0, 8(%r12,%rcx)
	je	.L548
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L546:
	movl	%esi, -52(%rbp)
	movl	%edx, %r9d
.L540:
	xorl	%esi, %esi
	movl	$-3, %r8d
	movq	%r15, %rdx
	movq	%r14, %rdi
	movl	%r9d, -56(%rbp)
	call	utf8_prevCharSafeBody_67@PLT
	movl	-56(%rbp), %r9d
	cmpl	$2047, %eax
	jg	.L552
	movl	%eax, %edx
	sarl	$6, %eax
	andl	$63, %edx
	movl	%eax, %ecx
	movl	268(%r12,%rdx,4), %edx
	shrl	%cl, %edx
	andl	$1, %edx
	cmpl	%ebx, %edx
	jne	.L539
.L553:
	movl	-52(%rbp), %r9d
	testl	%r9d, %r9d
	jg	.L562
	.p2align 4,,10
	.p2align 3
.L545:
	xorl	%r9d, %r9d
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L552:
	cmpl	$65535, %eax
	jg	.L554
	movl	%eax, %edx
	movl	%eax, %ecx
	sarl	$6, %edx
	sarl	$12, %ecx
	andl	$63, %edx
	movl	524(%r12,%rdx,4), %edx
	shrl	%cl, %edx
	andl	$65537, %edx
	cmpl	$1, %edx
	ja	.L555
	cmpl	%ebx, %edx
	je	.L553
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L555:
	movslq	%ecx, %rcx
	movq	856(%r12), %r8
	leaq	(%r12,%rcx,4), %rdx
	movslq	780(%rdx), %rdi
	movq	%rdi, %rsi
	movl	%edi, %ecx
	cmpl	(%r8,%rdi,4), %eax
	jl	.L559
	movl	784(%rdx), %ecx
	cmpl	%edi, %ecx
	jle	.L559
	movslq	%ecx, %rdx
	cmpl	-4(%r8,%rdx,4), %eax
	jge	.L559
.L558:
	movl	%ecx, %edx
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L581:
	movslq	%edx, %rdi
	cmpl	(%r8,%rdi,4), %eax
	jge	.L580
.L557:
	movl	%edx, %ecx
	leal	(%rdx,%rsi), %edx
	sarl	%edx
	cmpl	%esi, %edx
	jne	.L581
	.p2align 4,,10
	.p2align 3
.L559:
	andl	$1, %ecx
	cmpl	%ebx, %ecx
	je	.L553
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L554:
	movslq	844(%r12), %rdx
	movq	856(%r12), %r8
	movq	%rdx, %rsi
	movl	%edx, %ecx
	cmpl	(%r8,%rdx,4), %eax
	jl	.L559
	movl	848(%r12), %ecx
	cmpl	%edx, %ecx
	jle	.L559
	movslq	%ecx, %rdx
	cmpl	-4(%r8,%rdx,4), %eax
	jge	.L559
.L561:
	movl	%ecx, %edx
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L583:
	movslq	%edx, %rdi
	cmpl	(%r8,%rdi,4), %eax
	jge	.L582
.L560:
	movl	%edx, %ecx
	leal	(%rdx,%rsi), %edx
	sarl	%edx
	cmpl	%esi, %edx
	jne	.L583
	jmp	.L559
.L582:
	movl	%edx, %esi
	jmp	.L561
.L580:
	movl	%edx, %esi
	jmp	.L558
	.cfi_endproc
.LFE2332:
	.size	_ZNK6icu_676BMPSet12spanBackUTF8EPKhi17USetSpanCondition, .-_ZNK6icu_676BMPSet12spanBackUTF8EPKhi17USetSpanCondition
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_676BMPSetE
	.section	.rodata._ZTSN6icu_676BMPSetE,"aG",@progbits,_ZTSN6icu_676BMPSetE,comdat
	.align 16
	.type	_ZTSN6icu_676BMPSetE, @object
	.size	_ZTSN6icu_676BMPSetE, 17
_ZTSN6icu_676BMPSetE:
	.string	"N6icu_676BMPSetE"
	.weak	_ZTIN6icu_676BMPSetE
	.section	.data.rel.ro._ZTIN6icu_676BMPSetE,"awG",@progbits,_ZTIN6icu_676BMPSetE,comdat
	.align 8
	.type	_ZTIN6icu_676BMPSetE, @object
	.size	_ZTIN6icu_676BMPSetE, 24
_ZTIN6icu_676BMPSetE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676BMPSetE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTVN6icu_676BMPSetE
	.section	.data.rel.ro.local._ZTVN6icu_676BMPSetE,"awG",@progbits,_ZTVN6icu_676BMPSetE,comdat
	.align 8
	.type	_ZTVN6icu_676BMPSetE, @object
	.size	_ZTVN6icu_676BMPSetE, 40
_ZTVN6icu_676BMPSetE:
	.quad	0
	.quad	_ZTIN6icu_676BMPSetE
	.quad	_ZN6icu_676BMPSetD1Ev
	.quad	_ZN6icu_676BMPSetD0Ev
	.quad	_ZNK6icu_676BMPSet8containsEi
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	3
	.long	3
	.long	3
	.long	3
	.align 16
.LC1:
	.long	1
	.long	1
	.long	1
	.long	1
	.align 16
.LC2:
	.long	-536879105
	.long	-536879105
	.long	-536879105
	.long	-536879105
	.align 16
.LC3:
	.long	8192
	.long	8192
	.long	8192
	.long	8192
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
