	.file	"ucln_cmn.cpp"
	.text
	.p2align 4
	.globl	u_cleanup_67
	.type	u_cleanup_67, @function
u_cleanup_67:
.LFB2731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	_ZL20gLibCleanupFunctions(%rip), %rbx
	leaq	64(%rbx), %r12
	call	umtx_lock_67@PLT
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L2
	call	*%rax
	movq	$0, (%rbx)
.L2:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L3
	leaq	_ZL23gCommonCleanupFunctions(%rip), %rbx
	leaq	208(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L4
	call	*%rax
	movq	$0, (%rbx)
.L4:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L5
	call	cmemory_cleanup_67@PLT
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	utrace_cleanup_67@PLT
	.cfi_endproc
.LFE2731:
	.size	u_cleanup_67, .-u_cleanup_67
	.p2align 4
	.globl	ucln_cleanupOne_67
	.type	ucln_cleanupOne_67, @function
ucln_cleanupOne_67:
.LFB2732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	_ZL20gLibCleanupFunctions(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movslq	%edi, %rbx
	movq	(%r12,%rbx,8), %rax
	testq	%rax, %rax
	je	.L16
	call	*%rax
	movq	$0, (%r12,%rbx,8)
.L16:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2732:
	.size	ucln_cleanupOne_67, .-ucln_cleanupOne_67
	.p2align 4
	.globl	ucln_common_registerCleanup_67
	.type	ucln_common_registerCleanup_67, @function
ucln_common_registerCleanup_67:
.LFB2733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movslq	%edi, %rbx
	cmpl	$25, %ebx
	je	.L26
	jbe	.L27
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	leaq	_ZL23gCommonCleanupFunctions(%rip), %rax
	xorl	%edi, %edi
	movq	%r12, (%rax,%rbx,8)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	movq	%rsi, 200+_ZL23gCommonCleanupFunctions(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2733:
	.size	ucln_common_registerCleanup_67, .-ucln_common_registerCleanup_67
	.p2align 4
	.globl	ucln_registerCleanup_67
	.type	ucln_registerCleanup_67, @function
ucln_registerCleanup_67:
.LFB2734:
	.cfi_startproc
	endbr64
	cmpl	$7, %edi
	ja	.L28
	movslq	%edi, %rdi
	leaq	_ZL20gLibCleanupFunctions(%rip), %rax
	movq	%rsi, (%rax,%rdi,8)
.L28:
	ret
	.cfi_endproc
.LFE2734:
	.size	ucln_registerCleanup_67, .-ucln_registerCleanup_67
	.p2align 4
	.globl	ucln_lib_cleanup_67
	.type	ucln_lib_cleanup_67, @function
ucln_lib_cleanup_67:
.LFB2735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	_ZL20gLibCleanupFunctions(%rip), %rbx
	leaq	64(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L31
	call	*%rax
	movq	$0, (%rbx)
.L31:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L32
	leaq	_ZL23gCommonCleanupFunctions(%rip), %rbx
	leaq	208(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L34:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L33
	call	*%rax
	movq	$0, (%rbx)
.L33:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L34
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2735:
	.size	ucln_lib_cleanup_67, .-ucln_lib_cleanup_67
	.local	_ZL20gLibCleanupFunctions
	.comm	_ZL20gLibCleanupFunctions,64,32
	.local	_ZL23gCommonCleanupFunctions
	.comm	_ZL23gCommonCleanupFunctions,208,32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
