	.file	"ucnv_u16.cpp"
	.text
	.p2align 4
	.type	_UTF16BEGetNextUChar, @function
_UTF16BEGetNextUChar:
.LFB2115:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	cmpl	$7, 76(%r8)
	jle	.L9
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	cmpq	%rcx, %rax
	jnb	.L11
	leaq	2(%rax), %r9
	cmpq	%rcx, %r9
	ja	.L12
	movzwl	(%rax), %edx
	rolw	$8, %dx
	movl	%edx, %r11d
	movzwl	%dx, %r10d
	andl	$63488, %r11d
	cmpl	$55296, %r11d
	je	.L13
.L5:
	movq	%r9, 16(%rdi)
.L1:
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	andb	$4, %dh
	jne	.L6
	leaq	4(%rax), %r11
	cmpq	%rcx, %r11
	ja	.L7
	movzbl	2(%rax), %edx
	movl	%edx, %ecx
	movzbl	3(%rax), %edx
	sall	$8, %ecx
	orl	%ecx, %edx
	movzwl	%dx, %ecx
	andl	$64512, %edx
	cmpl	$56320, %edx
	je	.L14
.L6:
	movb	$2, 64(%r8)
	movzbl	(%rax), %edx
	movl	$65535, %r10d
	movb	%dl, 65(%r8)
	movzbl	1(%rax), %eax
	movb	%al, 66(%r8)
	movl	$12, (%rsi)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$65535, %r10d
	movl	$8, (%rsi)
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movzbl	(%rax), %edx
	movl	$65535, %r10d
	addq	$1, %rax
	movb	%dl, 65(%r8)
	movq	8(%rdi), %rdx
	movb	$1, 64(%rdx)
	movq	%rax, 16(%rdi)
	movl	%r10d, %eax
	movl	$11, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movb	%dl, 64(%r8)
	movzbl	(%rax), %edx
	movb	%dl, 65(%r8)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rcx
	jbe	.L8
	movzbl	1(%rax), %edx
	movb	%dl, 66(%r8)
	cmpq	%r9, %rcx
	jbe	.L8
	movzbl	2(%rax), %eax
	movb	%al, 67(%r8)
.L8:
	movl	$11, (%rsi)
	movq	%rcx, %r9
	movl	$65535, %r10d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L14:
	sall	$10, %r10d
	movq	%r11, %r9
	leal	-56613888(%rcx,%r10), %r10d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$-9, %r10d
	jmp	.L1
	.cfi_endproc
.LFE2115:
	.size	_UTF16BEGetNextUChar, .-_UTF16BEGetNextUChar
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"UTF-16BE"
.LC1:
	.string	"UTF-16BE,version=1"
	.text
	.p2align 4
	.type	_UTF16BEGetName, @function
_UTF16BEGetName:
.LFB2118:
	.cfi_startproc
	endbr64
	testb	$15, 56(%rdi)
	leaq	.LC1(%rip), %rdx
	leaq	.LC0(%rip), %rax
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE2118:
	.size	_UTF16BEGetName, .-_UTF16BEGetName
	.p2align 4
	.type	_UTF16LEGetNextUChar, @function
_UTF16LEGetNextUChar:
.LFB2121:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	cmpl	$7, 76(%rcx)
	jle	.L26
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	cmpq	%rdx, %rax
	jnb	.L27
	leaq	2(%rax), %r8
	cmpq	%rdx, %r8
	ja	.L28
	movzwl	(%rax), %r9d
	movl	%r9d, %r11d
	andl	$63488, %r11d
	cmpl	$55296, %r11d
	je	.L29
.L22:
	movq	%r8, 16(%rdi)
.L18:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	testw	$1024, %r9w
	jne	.L23
	leaq	4(%rax), %r10
	cmpq	%rdx, %r10
	ja	.L24
	movzbl	3(%rax), %edx
	sall	$8, %edx
	movl	%edx, %r11d
	movzbl	2(%rax), %edx
	orl	%r11d, %edx
	movzwl	%dx, %r11d
	andl	$64512, %edx
	cmpl	$56320, %edx
	je	.L30
.L23:
	movb	$2, 64(%rcx)
	movzbl	(%rax), %edx
	movl	$65535, %r9d
	movb	%dl, 65(%rcx)
	movzbl	1(%rax), %eax
	movb	%al, 66(%rcx)
	movl	$12, (%rsi)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$65535, %r9d
	movl	$8, (%rsi)
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movzbl	(%rax), %edx
	movl	$65535, %r9d
	addq	$1, %rax
	movb	%dl, 65(%rcx)
	movq	8(%rdi), %rdx
	movb	$1, 64(%rdx)
	movq	%rax, 16(%rdi)
	movl	%r9d, %eax
	movl	$11, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rdx, %r9
	subq	%rax, %r9
	movb	%r9b, 64(%rcx)
	movzbl	(%rax), %r9d
	movb	%r9b, 65(%rcx)
	leaq	1(%rax), %r9
	cmpq	%r9, %rdx
	jbe	.L25
	movzbl	1(%rax), %r9d
	movb	%r9b, 66(%rcx)
	cmpq	%r8, %rdx
	jbe	.L25
	movzbl	2(%rax), %eax
	movb	%al, 67(%rcx)
.L25:
	movl	$11, (%rsi)
	movq	%rdx, %r8
	movl	$65535, %r9d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L30:
	sall	$10, %r9d
	movq	%r10, %r8
	leal	-56613888(%r11,%r9), %r9d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$-9, %r9d
	jmp	.L18
	.cfi_endproc
.LFE2121:
	.size	_UTF16LEGetNextUChar, .-_UTF16LEGetNextUChar
	.section	.rodata.str1.1
.LC2:
	.string	"UTF-16LE"
.LC3:
	.string	"UTF-16LE,version=1"
	.text
	.p2align 4
	.type	_UTF16LEGetName, @function
_UTF16LEGetName:
.LFB2124:
	.cfi_startproc
	endbr64
	testb	$15, 56(%rdi)
	leaq	.LC3(%rip), %rdx
	leaq	.LC2(%rip), %rax
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE2124:
	.size	_UTF16LEGetName, .-_UTF16LEGetName
	.p2align 4
	.type	_UTF16Reset, @function
_UTF16Reset:
.LFB2125:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	jle	.L35
.L37:
	movl	$1, 80(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$0, 76(%rdi)
	jne	.L37
	ret
	.cfi_endproc
.LFE2125:
	.size	_UTF16Reset, .-_UTF16Reset
	.section	.rodata.str1.1
.LC4:
	.string	"UTF-16"
.LC5:
	.string	"UTF-16,version=1"
.LC6:
	.string	"UTF-16,version=2"
	.text
	.p2align 4
	.type	_UTF16GetName, @function
_UTF16GetName:
.LFB2127:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	leaq	.LC4(%rip), %r8
	andl	$15, %eax
	je	.L38
	cmpl	$1, %eax
	leaq	.LC5(%rip), %r8
	leaq	.LC6(%rip), %rax
	cmovne	%rax, %r8
.L38:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE2127:
	.size	_UTF16GetName, .-_UTF16GetName
	.p2align 4
	.type	_UTF16ToUnicodeWithOffsets, @function
_UTF16ToUnicodeWithOffsets:
.LFB2131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	48(%rdi), %rax
	movq	8(%rdi), %r13
	movl	$0, -52(%rbp)
	movq	16(%rdi), %rdx
	movq	24(%rdi), %r12
	movq	%rax, -64(%rbp)
	movl	76(%r13), %r15d
	.p2align 4,,10
	.p2align 3
.L44:
	cmpq	%rdx, %r12
	jbe	.L45
.L103:
	movl	(%rbx), %ecx
.L99:
	testl	%ecx, %ecx
	jg	.L46
	cmpl	$8, %r15d
	je	.L47
	jg	.L48
	testl	%r15d, %r15d
	je	.L49
	cmpl	$1, %r15d
	jne	.L99
	movzbl	65(%r13), %r10d
	movzbl	(%rdx), %ecx
	movq	48(%r13), %rax
	cmpb	$-2, %r10b
	je	.L101
	cmpb	$-2, %cl
	jne	.L59
	cmpb	$-1, %r10b
	je	.L102
.L59:
	leaq	_UTF16v2Data_67(%rip), %rsi
	cmpq	%rsi, %rax
	je	.L83
	leaq	_UTF16Data_67(%rip), %rdi
	cmpq	%rdi, %rax
	je	.L83
.L62:
	movq	16(%r14), %rcx
	cmpq	%rdx, %rcx
	je	.L65
	movb	$0, 64(%r13)
.L65:
	leaq	_UTF16LEData_67(%rip), %rsi
	xorl	%r15d, %r15d
	movq	%rcx, %rdx
	cmpq	%rsi, %rax
	sete	%r15b
	addl	$8, %r15d
.L64:
	movl	%r15d, 76(%r13)
	cmpq	%rdx, %r12
	ja	.L103
	.p2align 4,,10
	.p2align 3
.L45:
	cmpq	$0, -64(%rbp)
	je	.L67
	movl	-52(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L67
	movq	48(%r14), %rdi
	cmpq	%rdi, -64(%rbp)
	jnb	.L67
.L78:
	movq	-64(%rbp), %r8
	movl	$1, %esi
	movq	%r8, %r10
	leaq	1(%r8), %rax
	movq	%r8, %rcx
	notq	%r10
	addq	%rdi, %r10
	movq	%r10, %r11
	shrq	$2, %r11
	addq	$1, %r11
	cmpq	%rax, %rdi
	cmovb	%rsi, %r11
	jb	.L68
	cmpq	$11, %r10
	jbe	.L68
	movq	%r11, %r10
	movd	-52(%rbp), %xmm2
	shrq	$2, %r10
	salq	$4, %r10
	pshufd	$0, %xmm2, %xmm1
	addq	%r8, %r10
	.p2align 4,,10
	.p2align 3
.L70:
	movdqu	(%rcx), %xmm0
	addq	$16, %rcx
	paddd	%xmm1, %xmm0
	movups	%xmm0, -16(%rcx)
	cmpq	%r10, %rcx
	jne	.L70
	movq	-64(%rbp), %rax
	movq	%r11, %rcx
	andq	$-4, %rcx
	leaq	(%rax,%rcx,4), %rax
	movq	%rax, -64(%rbp)
	cmpq	%rcx, %r11
	je	.L67
.L68:
	movq	-64(%rbp), %rax
	movl	-52(%rbp), %esi
	leaq	4(%rax), %rcx
	addl	%esi, (%rax)
	cmpq	%rcx, %rdi
	jbe	.L67
	leaq	8(%rax), %rcx
	addl	%esi, 4(%rax)
	cmpq	%rcx, %rdi
	jbe	.L67
	addl	%esi, 8(%rax)
.L67:
	movq	%rdx, 16(%r14)
	cmpq	%rdx, %r12
	je	.L104
.L56:
	movl	%r15d, 76(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	cmpl	$9, %r15d
	jne	.L99
	movq	%rdx, 16(%r14)
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_UTF16LEToUnicodeWithOffsets
	movq	16(%r14), %rdx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%rdx, 16(%r14)
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_UTF16BEToUnicodeWithOffsets
	movq	16(%r14), %rdx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	movzbl	(%rdx), %eax
	movl	$1, %r15d
	movb	$1, 64(%r13)
	addq	$1, %rdx
	movb	%al, 65(%r13)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L83:
	movl	56(%r13), %r10d
	andl	$15, %r10d
	cmpl	$1, %r10d
	jne	.L62
	movl	$8, %eax
.L60:
	addq	$1, %rdx
	movb	%cl, 66(%r13)
	movb	$2, 64(%r13)
	movq	%rdx, 16(%r14)
	movl	%eax, 76(%r13)
	movl	$18, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	cmpb	$-1, %cl
	jne	.L59
	leaq	_UTF16LEData_67(%rip), %rdi
	cmpq	%rdi, %rax
	je	.L79
	movl	$8, %r15d
.L61:
	addq	$1, %rdx
	movb	$0, 64(%r13)
	movl	%edx, %eax
	subl	16(%r14), %eax
	movl	%eax, -52(%rbp)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$9, %eax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L46:
	cmpq	$0, -64(%rbp)
	je	.L77
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	je	.L77
	movq	48(%r14), %rdi
	cmpq	%rdi, -64(%rbp)
	jb	.L78
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%rdx, 16(%r14)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	_UTF16BEData_67(%rip), %rdi
	cmpq	%rdi, %rax
	je	.L80
	movl	$9, %r15d
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L104:
	cmpb	$0, 2(%r14)
	je	.L56
	cmpl	$8, %r15d
	je	.L72
	cmpl	$9, %r15d
	jne	.L56
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_UTF16LEToUnicodeWithOffsets
	jmp	.L56
.L80:
	movl	$8, %eax
	movl	$-2, %ecx
	jmp	.L60
.L72:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_UTF16BEToUnicodeWithOffsets
	jmp	.L56
	.cfi_endproc
.LFE2131:
	.size	_UTF16ToUnicodeWithOffsets, .-_UTF16ToUnicodeWithOffsets
	.p2align 4
	.type	_UTF16BEToUnicodeWithOffsets, @function
_UTF16BEToUnicodeWithOffsets:
.LFB2114:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r11
	cmpl	$7, 76(%r11)
	jle	.L207
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	24(%rdi), %rbx
	subq	%rax, %rbx
	movl	%ebx, %r14d
	testl	%ebx, %ebx
	je	.L208
	movq	32(%rdi), %r8
	movq	40(%rdi), %rcx
	cmpq	%r8, %rcx
	jbe	.L145
	subq	%r8, %rcx
	movq	48(%rdi), %r10
	movq	%rcx, %rdx
	sarq	%rdx
	movl	%edx, -52(%rbp)
	movl	72(%r11), %edx
	testl	%edx, %edx
	jne	.L144
	movsbl	64(%r11), %r9d
	testl	%r9d, %r9d
	jne	.L147
	movl	$1, %r13d
	xorl	%edx, %edx
.L113:
	movl	%r14d, %ebx
	andl	$-2, %ebx
	cmpl	%ecx, %r14d
	cmovb	%ebx, %ecx
	testl	%ecx, %ecx
	je	.L127
	testb	%r13b, %r13b
	je	.L127
	subl	%ecx, %r14d
	shrl	%ecx
	subl	%ecx, -52(%rbp)
	testq	%r10, %r10
	jne	.L128
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L209:
	movw	%dx, (%r8)
	addq	$4, %r10
	addq	$2, %r8
	movq	%rbx, %rax
	movl	%r9d, -4(%r10)
	addl	$2, %r9d
.L136:
	subl	$1, %ecx
	je	.L134
.L128:
	movzbl	(%rax), %r12d
	movzbl	1(%rax), %edx
	leaq	2(%rax), %rbx
	sall	$8, %r12d
	orl	%r12d, %edx
	andl	$63488, %r12d
	cmpl	$55296, %r12d
	jne	.L209
	testb	$4, %dh
	jne	.L131
	cmpl	$1, %ecx
	jbe	.L131
	movzbl	2(%rax), %r12d
	movzbl	3(%rax), %r13d
	leal	-1(%rcx), %r15d
	sall	$8, %r12d
	orl	%r12d, %r13d
	andl	$64512, %r12d
	cmpl	$56320, %r12d
	jne	.L149
	movw	%dx, (%r8)
	addq	$4, %rax
	movl	%r15d, %ecx
	addq	$8, %r10
	movw	%r13w, 2(%r8)
	addq	$4, %r8
	movl	%r9d, -8(%r10)
	movl	%r9d, -4(%r10)
	addl	$4, %r9d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L208:
	movl	72(%r11), %edx
	testl	%edx, %edx
	je	.L105
	movq	32(%rdi), %r8
	movq	40(%rdi), %rcx
	cmpq	%r8, %rcx
	jbe	.L145
	subq	%r8, %rcx
	movq	48(%rdi), %r10
	sarq	%rcx
	movl	%ecx, -52(%rbp)
.L144:
	movb	%dl, 65(%r11)
	movl	$1, %ecx
	movb	$1, 64(%r11)
	movl	$0, 72(%r11)
.L112:
	leaq	65(%r11), %r12
	leal	1(%r14), %r13d
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L114:
	cmpl	$4, %ecx
	je	.L210
.L118:
	testl	%ebx, %ebx
	je	.L211
.L125:
	movq	%rax, %r14
	movzbl	(%rax), %r9d
	movl	%ecx, %edx
	addl	$1, %ecx
	addq	$1, %rax
	movb	%r9b, (%r12,%rdx)
	movl	%r13d, %r9d
	subl	%ebx, %r9d
	subl	$1, %ebx
	cmpl	$2, %ecx
	jne	.L114
	movzbl	65(%r11), %r14d
	movzbl	66(%r11), %edx
	sall	$8, %r14d
	orl	%r14d, %edx
	andl	$63488, %r14d
	cmpl	$55296, %r14d
	jne	.L212
	testb	$4, %dh
	je	.L118
	movl	-52(%rbp), %ecx
	xorl	%r13d, %r13d
	movl	$2, %r12d
	addl	%ecx, %ecx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L145:
	movl	$15, (%rsi)
.L105:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	jmp	_UTF16ToUnicodeWithOffsets
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movw	%dx, (%r8)
	movq	%rbx, %rax
	addq	$2, %r8
.L130:
	subl	$1, %ecx
	je	.L134
.L133:
	movzbl	(%rax), %r12d
	movzbl	1(%rax), %edx
	leaq	2(%rax), %rbx
	sall	$8, %r12d
	orl	%r12d, %edx
	andl	$63488, %r12d
	cmpl	$55296, %r12d
	jne	.L213
	testb	$4, %dh
	jne	.L131
	cmpl	$1, %ecx
	jbe	.L131
	movzbl	2(%rax), %r12d
	movzbl	3(%rax), %r13d
	leal	-1(%rcx), %r15d
	sall	$8, %r12d
	orl	%r12d, %r13d
	andl	$64512, %r12d
	cmpl	$56320, %r12d
	jne	.L149
	movw	%dx, (%r8)
	addq	$4, %rax
	movl	%r15d, %ecx
	addq	$4, %r8
	movw	%r13w, -2(%r8)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L127:
	testw	%dx, %dx
	je	.L134
.L137:
	movl	%edx, %ecx
	movb	$2, 64(%r11)
	rolw	$8, %cx
	movw	%cx, 65(%r11)
	testb	$4, %dh
	jne	.L138
	cmpl	$1, %r14d
	jbe	.L134
	movzbl	(%rax), %ecx
	movzbl	1(%rax), %ebx
	sall	$8, %ecx
	orl	%ecx, %ebx
	andl	$64512, %ecx
	cmpl	$56320, %ecx
	je	.L214
.L138:
	movl	$12, (%rsi)
.L142:
	movq	%rax, 16(%rdi)
	movq	%r8, 32(%rdi)
	movq	%r10, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L150:
	.cfi_restore_state
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L134:
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L142
	testl	%r14d, %r14d
	je	.L142
	movl	-52(%rbp), %edx
	testl	%edx, %edx
	jne	.L143
	movl	$15, (%rsi)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L211:
	movl	%ecx, %r12d
	movl	-52(%rbp), %ecx
	movl	$1, %r13d
	xorl	%edx, %edx
	addl	%ecx, %ecx
.L117:
	movb	%r12b, 64(%r11)
	movl	%ebx, %r14d
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L210:
	movzbl	67(%r11), %edx
	movzbl	68(%r11), %ecx
	sall	$8, %edx
	orl	%edx, %ecx
	andl	$64512, %edx
	cmpl	$56320, %edx
	jne	.L120
	movzbl	65(%r11), %edx
	sall	$8, %edx
	movl	%edx, %r12d
	movzbl	66(%r11), %edx
	orl	%r12d, %edx
	cmpl	$1, -52(%rbp)
	movw	%dx, (%r8)
	jbe	.L121
	movw	%cx, 2(%r8)
	leaq	4(%r8), %rdx
	testq	%r10, %r10
	je	.L122
	movq	$-1, (%r10)
	addq	$8, %r10
.L122:
	subl	$2, -52(%rbp)
	movl	-52(%rbp), %ecx
	movq	%rdx, %r8
	movl	$1, %r13d
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	addl	%ecx, %ecx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L131:
	testl	%ecx, %ecx
	je	.L150
	leal	-1(%rcx), %r15d
	movq	%rbx, %rax
.L132:
	addl	%ecx, -52(%rbp)
	leal	(%r14,%r15,2), %r14d
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L143:
	movsbq	64(%r11), %rdx
	addq	$1, %rax
	leal	1(%rdx), %ecx
	movb	%cl, 64(%r11)
	movzbl	-1(%rax), %ecx
	movb	%cl, 65(%r11,%rdx)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$12, (%rsi)
	movq	16(%rdi), %rsi
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jle	.L123
	subq	$1, %r14
.L124:
	movb	$2, 64(%r11)
	movq	%r14, 16(%rdi)
	movq	%r8, 32(%rdi)
	movq	%r10, 48(%rdi)
	jmp	.L105
.L214:
	movw	%dx, (%r8)
	addq	$2, %rax
	leaq	2(%r8), %rcx
	testq	%r10, %r10
	je	.L140
	movl	%r9d, (%r10)
	addq	$4, %r10
.L140:
	movw	%bx, 144(%r11)
	movq	%rcx, %r8
	movb	$1, 93(%r11)
	movb	$0, 64(%r11)
	movl	$15, (%rsi)
	jmp	.L142
.L149:
	movq	%rbx, %rax
	jmp	.L132
.L212:
	movw	%dx, (%r8)
	leaq	2(%r8), %r12
	testq	%r10, %r10
	je	.L116
	movl	$-1, (%r10)
	addq	$4, %r10
.L116:
	subl	$1, -52(%rbp)
	movl	-52(%rbp), %ecx
	movq	%r12, %r8
	movl	$1, %r13d
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	addl	%ecx, %ecx
	jmp	.L117
.L121:
	movb	$1, 93(%r11)
	addq	$2, %r8
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	movw	%cx, 144(%r11)
	movl	$1, %r13d
	xorl	%ecx, %ecx
	movl	$15, (%rsi)
	movl	$0, -52(%rbp)
	jmp	.L117
.L123:
	movzbl	67(%r11), %eax
	orb	$1, %ah
	movl	%eax, 72(%r11)
	jmp	.L124
.L147:
	movl	%r9d, %ecx
	jmp	.L112
	.cfi_endproc
.LFE2114:
	.size	_UTF16BEToUnicodeWithOffsets, .-_UTF16BEToUnicodeWithOffsets
	.p2align 4
	.type	_UTF16LEToUnicodeWithOffsets, @function
_UTF16LEToUnicodeWithOffsets:
.LFB2120:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r11
	cmpl	$7, 76(%r11)
	jle	.L318
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	24(%rdi), %rbx
	subq	%rax, %rbx
	movl	%ebx, %r13d
	testl	%ebx, %ebx
	je	.L319
	movq	32(%rdi), %r8
	movq	40(%rdi), %rdx
	cmpq	%r8, %rdx
	jbe	.L255
	subq	%r8, %rdx
	movq	48(%rdi), %r10
	movq	%rdx, %rcx
	sarq	%rcx
	movl	%ecx, -52(%rbp)
	movl	72(%r11), %ecx
	testl	%ecx, %ecx
	jne	.L254
	movsbl	64(%r11), %r9d
	testl	%r9d, %r9d
	jne	.L257
	movl	$1, %r14d
	xorl	%ecx, %ecx
.L223:
	movl	%r13d, %ebx
	andl	$-2, %ebx
	cmpl	%edx, %r13d
	cmovb	%ebx, %edx
	testl	%edx, %edx
	je	.L237
	testb	%r14b, %r14b
	je	.L237
	subl	%edx, %r13d
	shrl	%edx
	subl	%edx, -52(%rbp)
	testq	%r10, %r10
	jne	.L238
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L320:
	movw	%cx, (%r8)
	addq	$4, %r10
	addq	$2, %r8
	movq	%rbx, %rax
	movl	%r9d, -4(%r10)
	addl	$2, %r9d
.L246:
	subl	$1, %edx
	je	.L244
.L238:
	movzbl	1(%rax), %r12d
	movzbl	(%rax), %ecx
	leaq	2(%rax), %rbx
	sall	$8, %r12d
	orl	%r12d, %ecx
	andl	$63488, %r12d
	cmpl	$55296, %r12d
	jne	.L320
	testb	$4, %ch
	jne	.L241
	cmpl	$1, %edx
	jbe	.L241
	movzbl	3(%rax), %r12d
	movl	(%rax), %r15d
	leal	-1(%rdx), %r14d
	sall	$8, %r12d
	andl	$64512, %r12d
	cmpl	$56320, %r12d
	jne	.L259
	movl	%r15d, (%r8)
	addq	$4, %rax
	movl	%r14d, %edx
	addq	$8, %r10
	movl	%r9d, -8(%r10)
	addq	$4, %r8
	movl	%r9d, -4(%r10)
	addl	$4, %r9d
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L319:
	movl	72(%r11), %ecx
	testl	%ecx, %ecx
	je	.L215
	movq	32(%rdi), %r8
	movq	40(%rdi), %rdx
	cmpq	%r8, %rdx
	jbe	.L255
	subq	%r8, %rdx
	movq	48(%rdi), %r10
	sarq	%rdx
	movl	%edx, -52(%rbp)
.L254:
	movb	%cl, 65(%r11)
	movl	$1, %edx
	movb	$1, 64(%r11)
	movl	$0, 72(%r11)
.L222:
	leaq	65(%r11), %r12
	addl	$1, %r13d
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L224:
	cmpl	$4, %edx
	je	.L321
.L228:
	testl	%ebx, %ebx
	je	.L322
.L235:
	movq	%rax, %r14
	movzbl	(%rax), %r9d
	movl	%edx, %ecx
	addl	$1, %edx
	addq	$1, %rax
	movb	%r9b, (%r12,%rcx)
	movl	%r13d, %r9d
	subl	%ebx, %r9d
	subl	$1, %ebx
	cmpl	$2, %edx
	jne	.L224
	movzbl	66(%r11), %r14d
	movzbl	65(%r11), %ecx
	sall	$8, %r14d
	orl	%r14d, %ecx
	andl	$63488, %r14d
	cmpl	$55296, %r14d
	jne	.L323
	testb	$4, %ch
	je	.L228
	movl	-52(%rbp), %edx
	xorl	%r14d, %r14d
	movl	$2, %r12d
	addl	%edx, %edx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L255:
	movl	$15, (%rsi)
.L215:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	jmp	_UTF16ToUnicodeWithOffsets
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movw	%cx, (%r8)
	movq	%rbx, %rax
	addq	$2, %r8
.L240:
	subl	$1, %edx
	je	.L244
.L243:
	movzbl	1(%rax), %r12d
	movzbl	(%rax), %ecx
	leaq	2(%rax), %rbx
	sall	$8, %r12d
	orl	%r12d, %ecx
	andl	$63488, %r12d
	cmpl	$55296, %r12d
	jne	.L324
	testb	$4, %ch
	jne	.L241
	cmpl	$1, %edx
	jbe	.L241
	movzbl	3(%rax), %r12d
	movl	(%rax), %r15d
	leal	-1(%rdx), %r14d
	sall	$8, %r12d
	andl	$64512, %r12d
	cmpl	$56320, %r12d
	jne	.L259
	movl	%r15d, (%r8)
	addq	$4, %rax
	movl	%r14d, %edx
	addq	$4, %r8
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L237:
	testw	%cx, %cx
	je	.L244
.L247:
	movw	%cx, 65(%r11)
	movb	$2, 64(%r11)
	testb	$4, %ch
	jne	.L248
	cmpl	$1, %r13d
	jbe	.L244
	movzbl	1(%rax), %edx
	movzbl	(%rax), %ebx
	sall	$8, %edx
	orl	%edx, %ebx
	andl	$64512, %edx
	cmpl	$56320, %edx
	je	.L325
.L248:
	movl	$12, (%rsi)
.L252:
	movq	%rax, 16(%rdi)
	movq	%r8, 32(%rdi)
	movq	%r10, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L260:
	.cfi_restore_state
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L244:
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L252
	testl	%r13d, %r13d
	je	.L252
	movl	-52(%rbp), %edx
	testl	%edx, %edx
	jne	.L253
	movl	$15, (%rsi)
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L322:
	movl	%edx, %r12d
	movl	-52(%rbp), %edx
	movl	$1, %r14d
	xorl	%ecx, %ecx
	addl	%edx, %edx
.L227:
	movb	%r12b, 64(%r11)
	movl	%ebx, %r13d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L321:
	movzbl	68(%r11), %edx
	movzbl	67(%r11), %ecx
	sall	$8, %edx
	orl	%edx, %ecx
	andl	$64512, %edx
	cmpl	$56320, %edx
	jne	.L230
	movzbl	66(%r11), %edx
	sall	$8, %edx
	movl	%edx, %r12d
	movzbl	65(%r11), %edx
	orl	%r12d, %edx
	cmpl	$1, -52(%rbp)
	movw	%dx, (%r8)
	jbe	.L231
	movw	%cx, 2(%r8)
	leaq	4(%r8), %r12
	testq	%r10, %r10
	je	.L232
	movq	$-1, (%r10)
	addq	$8, %r10
.L232:
	subl	$2, -52(%rbp)
.L317:
	movl	-52(%rbp), %edx
	movq	%r12, %r8
	movl	$1, %r14d
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
	addl	%edx, %edx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L241:
	testl	%edx, %edx
	je	.L260
	leal	-1(%rdx), %r14d
	movq	%rbx, %rax
.L242:
	addl	%edx, -52(%rbp)
	leal	0(%r13,%r14,2), %r13d
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L253:
	movsbq	64(%r11), %rdx
	addq	$1, %rax
	leal	1(%rdx), %ecx
	movb	%cl, 64(%r11)
	movzbl	-1(%rax), %ecx
	movb	%cl, 65(%r11,%rdx)
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$12, (%rsi)
	movq	16(%rdi), %rsi
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jle	.L233
	subq	$1, %r14
.L234:
	movb	$2, 64(%r11)
	movq	%r14, 16(%rdi)
	movq	%r8, 32(%rdi)
	movq	%r10, 48(%rdi)
	jmp	.L215
.L325:
	movw	%cx, (%r8)
	addq	$2, %rax
	leaq	2(%r8), %rdx
	testq	%r10, %r10
	je	.L250
	movl	%r9d, (%r10)
	addq	$4, %r10
.L250:
	movw	%bx, 144(%r11)
	movq	%rdx, %r8
	movb	$1, 93(%r11)
	movb	$0, 64(%r11)
	movl	$15, (%rsi)
	jmp	.L252
.L259:
	movq	%rbx, %rax
	jmp	.L242
.L323:
	movw	%cx, (%r8)
	leaq	2(%r8), %r12
	testq	%r10, %r10
	je	.L226
	movl	$-1, (%r10)
	addq	$4, %r10
.L226:
	subl	$1, -52(%rbp)
	jmp	.L317
.L231:
	movb	$1, 93(%r11)
	addq	$2, %r8
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	movw	%cx, 144(%r11)
	movl	$1, %r14d
	xorl	%ecx, %ecx
	movl	$15, (%rsi)
	movl	$0, -52(%rbp)
	jmp	.L227
.L233:
	movzbl	67(%r11), %eax
	orb	$1, %ah
	movl	%eax, 72(%r11)
	jmp	.L234
.L257:
	movl	%r9d, %edx
	jmp	.L222
	.cfi_endproc
.LFE2120:
	.size	_UTF16LEToUnicodeWithOffsets, .-_UTF16LEToUnicodeWithOffsets
	.p2align 4
	.type	_UTF16BEFromUnicodeWithOffsets, @function
_UTF16BEFromUnicodeWithOffsets:
.LFB2113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	24(%rdi), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	subq	%rbx, %r10
	sarq	%r10
	testl	%r10d, %r10d
	je	.L326
	movq	8(%rdi), %r15
	movq	40(%rdi), %r8
	movq	%rdi, %r12
	movq	%rsi, %r13
	movl	%r10d, %r14d
	cmpl	$1, 80(%r15)
	je	.L402
.L328:
	movq	32(%r12), %rax
	movq	%rax, -80(%rbp)
	cmpq	%r8, %rax
	jnb	.L403
	movq	48(%r12), %rsi
	movl	84(%r15), %ecx
	subq	%rax, %r8
	movl	%r8d, %edi
	movq	%rsi, -72(%rbp)
	movl	%ecx, %edx
	testw	%cx, %cx
	je	.L356
	movzwl	(%rbx), %r9d
	movl	%r9d, %edi
	andl	$64512, %edi
	cmpl	$56320, %edi
	jne	.L357
	cmpl	$3, %r8d
	ja	.L404
.L357:
	xorl	%ecx, %ecx
.L331:
	movzwl	%dx, %edi
	testb	$4, %dh
	je	.L354
.L345:
	movl	$12, 0(%r13)
	movl	%edi, 84(%r15)
.L347:
	movq	%rax, 32(%r12)
	movq	-72(%rbp), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, 48(%r12)
.L326:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L405
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movl	$15, 0(%r13)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L356:
	xorl	%ecx, %ecx
.L330:
	leal	(%r14,%r14), %edx
	movl	%edi, %r8d
	andl	$-2, %r8d
	cmpl	%edi, %edx
	cmova	%r8d, %edx
	subl	%edx, %edi
	shrl	%edx
	testq	%rsi, %rsi
	movl	%edi, -92(%rbp)
	movl	%edx, %esi
	movl	%edx, -88(%rbp)
	je	.L406
	testl	%edx, %edx
	je	.L335
	movq	%rbx, %r10
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L408:
	movb	%dh, (%rax)
	movq	-80(%rbp), %rax
	movq	%r8, %r10
	movb	%dil, 1(%rax)
	movq	-80(%rbp), %rax
	movl	%ecx, %edi
	movq	-72(%rbp), %rdx
	addq	$2, %rax
	leaq	8(%rdx), %r9
	movq	%rax, -80(%rbp)
	movl	%ecx, (%rdx)
	addl	$1, %ecx
	movq	%r9, -72(%rbp)
	movl	%edi, 4(%rdx)
.L342:
	subl	$1, %esi
	je	.L407
.L343:
	movzwl	(%r10), %edi
	leaq	2(%r10), %r8
	movl	%edi, %r9d
	movl	%edi, %edx
	andl	$63488, %r9d
	cmpl	$55296, %r9d
	jne	.L408
	testb	$4, %dh
	jne	.L338
	cmpl	$1, %esi
	je	.L339
	movzwl	2(%r10), %r11d
	movq	%r10, %rbx
	movl	%r11d, %r10d
	andl	$64512, %r10d
	cmpl	$56320, %r10d
	jne	.L339
	movb	%dh, (%rax)
	movq	-80(%rbp), %rax
	addq	$4, %rbx
	subl	$1, %esi
	movq	%rbx, %r10
	movl	%r11d, %ebx
	movb	%dil, 1(%rax)
	movq	-80(%rbp), %rax
	movb	%bh, 2(%rax)
	movq	-80(%rbp), %rax
	movb	%r11b, 3(%rax)
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rax
	leaq	16(%rdx), %rdi
	movl	%ecx, 8(%rdx)
	addq	$4, %rax
	movl	%ecx, 12(%rdx)
	movq	%rax, -80(%rbp)
	movl	%ecx, (%rdx)
	movl	%ecx, 4(%rdx)
	addl	$2, %ecx
	movq	%rdi, -72(%rbp)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L406:
	testl	%edx, %edx
	je	.L335
	movl	%ecx, %r10d
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L410:
	movb	%ch, (%rax)
	movq	-80(%rbp), %rax
	movq	%r8, %rbx
	movb	%cl, 1(%rax)
	movq	-80(%rbp), %rax
	addq	$2, %rax
	movq	%rax, -80(%rbp)
.L337:
	subl	$1, %esi
	je	.L409
.L340:
	movzwl	(%rbx), %ecx
	leaq	2(%rbx), %r8
	movl	%ecx, %r9d
	movl	%ecx, %edx
	andl	$63488, %r9d
	cmpl	$55296, %r9d
	jne	.L410
	testb	$4, %ch
	jne	.L394
	cmpl	$1, %esi
	je	.L396
	movzwl	2(%rbx), %r11d
	movl	%r11d, %r9d
	andl	$64512, %r9d
	cmpl	$56320, %r9d
	jne	.L396
	movb	%ch, (%rax)
	movq	-80(%rbp), %rax
	addq	$4, %rbx
	subl	$1, %esi
	movb	%cl, 1(%rax)
	movq	-80(%rbp), %rax
	movl	%r11d, %ecx
	movb	%ch, 2(%rax)
	movq	-80(%rbp), %rax
	movb	%r11b, 3(%rax)
	movq	-80(%rbp), %rax
	addq	$4, %rax
	movq	%rax, -80(%rbp)
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L402:
	pushq	%rsi
	leaq	48(%rdi), %r9
	leaq	32(%rdi), %rcx
	movl	$2, %edx
	pushq	$-1
	leaq	_ZZ30_UTF16BEFromUnicodeWithOffsetsE3bom(%rip), %rsi
	movq	%r15, %rdi
	movq	%r10, -88(%rbp)
	call	ucnv_fromUWriteBytes_67@PLT
	movl	$0, 80(%r15)
	movq	40(%r12), %r8
	popq	%r9
	popq	%r10
	movq	-88(%rbp), %r10
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L409:
	movl	%r10d, %ecx
.L335:
	cmpl	%r14d, -88(%rbp)
	je	.L353
	movl	-92(%rbp), %edx
	testl	%edx, %edx
	je	.L353
	movzwl	(%rbx), %edx
	leaq	2(%rbx), %rdi
	movl	%edx, %esi
	movl	%edx, %r8d
	andl	$63488, %esi
	rolw	$8, %r8w
	cmpl	$55296, %esi
	je	.L359
	movw	%r8w, -60(%rbp)
	movq	%rdi, %rbx
	movl	$2, %edx
.L344:
	pushq	%r13
	movq	40(%r12), %r8
	leaq	-80(%rbp), %r10
	leaq	-60(%rbp), %rsi
	pushq	%rcx
	movq	%r15, %rdi
	leaq	-72(%rbp), %r9
	movq	%r10, %rcx
	call	ucnv_fromUWriteBytes_67@PLT
	movq	-80(%rbp), %rax
	movl	40(%r12), %esi
	popq	%rdi
	popq	%r8
	subl	%eax, %esi
	movl	%esi, -92(%rbp)
.L353:
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L347
	cmpq	%rbx, 24(%r12)
	jbe	.L347
	movl	-92(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L347
	movl	$15, 0(%r13)
	jmp	.L347
.L396:
	movl	%ecx, %edi
	movl	%r10d, %ecx
.L339:
	movq	%r8, %rbx
.L354:
	cmpq	%rbx, 24(%r12)
	jbe	.L401
	movzwl	(%rbx), %eax
	movzwl	(%rbx), %esi
	andl	$-1024, %eax
	rolw	$8, %si
	cmpl	$56320, %eax
	je	.L411
	movl	$12, 0(%r13)
.L401:
	movl	%edi, 84(%r15)
	movq	-80(%rbp), %rax
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r10, %rbx
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L404:
	movb	%ch, (%rax)
	movq	-80(%rbp), %rax
	addq	$2, %rbx
	leal	-1(%r10), %r14d
	leal	-4(%r8), %edi
	xorl	%esi, %esi
	movb	%cl, 1(%rax)
	movq	-80(%rbp), %rax
	movl	%r9d, %ecx
	movb	%ch, 2(%rax)
	movq	-80(%rbp), %rax
	movb	%r9b, 3(%rax)
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	addq	$4, %rax
	movq	%rax, -80(%rbp)
	testq	%rdx, %rdx
	je	.L332
	leaq	16(%rdx), %rsi
	movq	$-1, (%rdx)
	movl	$-1, 8(%rdx)
	movq	%rsi, -72(%rbp)
	movl	$-1, 12(%rdx)
.L332:
	movl	$0, 84(%r15)
	movl	$1, %ecx
	jmp	.L330
.L411:
	movl	%edi, %eax
	movb	%dl, -59(%rbp)
	addq	$2, %rbx
	movl	$4, %edx
	movb	%ah, -60(%rbp)
	movw	%si, -58(%rbp)
	movl	$0, 84(%r15)
	jmp	.L344
.L359:
	movq	%rdi, %rbx
	jmp	.L331
.L394:
	movl	%ecx, %edi
.L338:
	movq	%r8, %rbx
	jmp	.L345
.L405:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2113:
	.size	_UTF16BEFromUnicodeWithOffsets, .-_UTF16BEFromUnicodeWithOffsets
	.p2align 4
	.type	_UTF16LEFromUnicodeWithOffsets, @function
_UTF16LEFromUnicodeWithOffsets:
.LFB2119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	24(%rdi), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	subq	%rbx, %r10
	sarq	%r10
	testl	%r10d, %r10d
	je	.L412
	movq	8(%rdi), %r15
	movq	40(%rdi), %r8
	movq	%rdi, %r12
	movq	%rsi, %r13
	movl	%r10d, %r14d
	cmpl	$1, 80(%r15)
	je	.L488
.L414:
	movq	32(%r12), %rax
	movq	%rax, -80(%rbp)
	cmpq	%r8, %rax
	jnb	.L489
	movq	48(%r12), %rsi
	movl	84(%r15), %ecx
	subq	%rax, %r8
	movl	%r8d, %edi
	movq	%rsi, -72(%rbp)
	movl	%ecx, %edx
	testw	%cx, %cx
	je	.L442
	movzwl	(%rbx), %r9d
	movl	%r9d, %edi
	andl	$64512, %edi
	cmpl	$56320, %edi
	jne	.L443
	cmpl	$3, %r8d
	ja	.L490
.L443:
	xorl	%ecx, %ecx
	movzwl	%dx, %edi
.L417:
	testb	$4, %dh
	je	.L440
.L431:
	movl	$12, 0(%r13)
	movl	%edi, 84(%r15)
.L433:
	movq	%rax, 32(%r12)
	movq	-72(%rbp), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, 48(%r12)
.L412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L491
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	movl	$15, 0(%r13)
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L442:
	xorl	%ecx, %ecx
.L416:
	leal	(%r14,%r14), %edx
	movl	%edi, %r8d
	andl	$-2, %r8d
	cmpl	%edi, %edx
	cmova	%r8d, %edx
	subl	%edx, %edi
	shrl	%edx
	testq	%rsi, %rsi
	movl	%edi, -92(%rbp)
	movl	%edx, %esi
	movl	%edx, -88(%rbp)
	je	.L492
	testl	%edx, %edx
	jne	.L429
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L493:
	movb	%dil, (%rax)
	movq	-80(%rbp), %rax
	movl	%ecx, %edi
	movq	%r8, %rbx
	movb	%dh, 1(%rax)
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	addq	$2, %rax
	leaq	8(%rdx), %r9
	movq	%rax, -80(%rbp)
	movl	%ecx, (%rdx)
	addl	$1, %ecx
	movq	%r9, -72(%rbp)
	movl	%edi, 4(%rdx)
.L428:
	subl	$1, %esi
	je	.L421
.L429:
	movzwl	(%rbx), %edi
	leaq	2(%rbx), %r8
	movl	%edi, %r9d
	movl	%edi, %edx
	andl	$63488, %r9d
	cmpl	$55296, %r9d
	jne	.L493
	testb	$4, %dh
	jne	.L424
	cmpl	$1, %esi
	je	.L425
	movzwl	2(%rbx), %r11d
	movl	%r11d, %r10d
	andl	$64512, %r10d
	cmpl	$56320, %r10d
	jne	.L425
	movb	%dil, (%rax)
	movq	-80(%rbp), %rax
	addq	$4, %rbx
	subl	$1, %esi
	movb	%dh, 1(%rax)
	movq	-80(%rbp), %rax
	movl	%r11d, %edx
	movb	%r11b, 2(%rax)
	movq	-80(%rbp), %rax
	movb	%dh, 3(%rax)
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rax
	leaq	16(%rdx), %rdi
	movl	%ecx, 8(%rdx)
	addq	$4, %rax
	movl	%ecx, 12(%rdx)
	movq	%rax, -80(%rbp)
	movl	%ecx, (%rdx)
	movl	%ecx, 4(%rdx)
	addl	$2, %ecx
	movq	%rdi, -72(%rbp)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L492:
	testl	%edx, %edx
	je	.L421
	movl	%ecx, %r10d
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L495:
	movb	%cl, (%rax)
	movq	-80(%rbp), %rax
	movq	%r8, %rbx
	movb	%ch, 1(%rax)
	movq	-80(%rbp), %rax
	addq	$2, %rax
	movq	%rax, -80(%rbp)
.L423:
	subl	$1, %esi
	je	.L494
.L426:
	movzwl	(%rbx), %ecx
	leaq	2(%rbx), %r8
	movl	%ecx, %r9d
	movl	%ecx, %edx
	andl	$63488, %r9d
	cmpl	$55296, %r9d
	jne	.L495
	testb	$4, %ch
	jne	.L480
	cmpl	$1, %esi
	je	.L482
	movzwl	2(%rbx), %r11d
	movl	%r11d, %r9d
	andl	$64512, %r9d
	cmpl	$56320, %r9d
	jne	.L482
	movb	%cl, (%rax)
	movq	-80(%rbp), %rax
	addq	$4, %rbx
	subl	$1, %esi
	movb	%ch, 1(%rax)
	movq	-80(%rbp), %rax
	movl	%r11d, %ecx
	movb	%r11b, 2(%rax)
	movq	-80(%rbp), %rax
	movb	%ch, 3(%rax)
	movq	-80(%rbp), %rax
	addq	$4, %rax
	movq	%rax, -80(%rbp)
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L488:
	pushq	%rsi
	leaq	48(%rdi), %r9
	leaq	32(%rdi), %rcx
	movl	$2, %edx
	pushq	$-1
	leaq	_ZZ30_UTF16LEFromUnicodeWithOffsetsE3bom(%rip), %rsi
	movq	%r15, %rdi
	movq	%r10, -88(%rbp)
	call	ucnv_fromUWriteBytes_67@PLT
	movl	$0, 80(%r15)
	movq	40(%r12), %r8
	popq	%r9
	popq	%r10
	movq	-88(%rbp), %r10
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L494:
	movl	%r10d, %ecx
.L421:
	cmpl	%r14d, -88(%rbp)
	je	.L439
	movl	-92(%rbp), %edx
	testl	%edx, %edx
	je	.L439
	movzwl	(%rbx), %edi
	leaq	2(%rbx), %r8
	movl	%edi, %esi
	movl	%edi, %edx
	andl	$63488, %esi
	cmpl	$55296, %esi
	je	.L445
	movw	%di, -60(%rbp)
	movq	%r8, %rbx
	movl	$2, %edx
.L430:
	pushq	%r13
	movq	40(%r12), %r8
	leaq	-80(%rbp), %r10
	leaq	-60(%rbp), %rsi
	pushq	%rcx
	movq	%r15, %rdi
	leaq	-72(%rbp), %r9
	movq	%r10, %rcx
	call	ucnv_fromUWriteBytes_67@PLT
	movq	-80(%rbp), %rax
	movl	40(%r12), %esi
	popq	%rdi
	popq	%r8
	subl	%eax, %esi
	movl	%esi, -92(%rbp)
.L439:
	movl	0(%r13), %esi
	testl	%esi, %esi
	jg	.L433
	cmpq	%rbx, 24(%r12)
	jbe	.L433
	movl	-92(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L433
	movl	$15, 0(%r13)
	jmp	.L433
.L482:
	movl	%ecx, %edi
	movl	%r10d, %ecx
.L425:
	movq	%r8, %rbx
.L440:
	cmpq	%rbx, 24(%r12)
	jbe	.L487
	movzwl	(%rbx), %eax
	movl	%eax, %esi
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L496
	movl	$12, 0(%r13)
.L487:
	movl	%edi, 84(%r15)
	movq	-80(%rbp), %rax
	jmp	.L433
.L496:
	movl	%edi, %eax
	movb	%dl, -60(%rbp)
	addq	$2, %rbx
	movl	$4, %edx
	movb	%ah, -59(%rbp)
	movw	%si, -58(%rbp)
	movl	$0, 84(%r15)
	jmp	.L430
.L445:
	movq	%r8, %rbx
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L490:
	movb	%cl, (%rax)
	movq	-80(%rbp), %rax
	addq	$2, %rbx
	leal	-1(%r10), %r14d
	leal	-4(%r8), %edi
	xorl	%esi, %esi
	movb	%ch, 1(%rax)
	movq	-80(%rbp), %rax
	movl	%r9d, %ecx
	movb	%r9b, 2(%rax)
	movq	-80(%rbp), %rax
	movb	%ch, 3(%rax)
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
	addq	$4, %rax
	movq	%rax, -80(%rbp)
	testq	%rdx, %rdx
	je	.L418
	leaq	16(%rdx), %rsi
	movq	$-1, (%rdx)
	movl	$-1, 8(%rdx)
	movq	%rsi, -72(%rbp)
	movl	$-1, 12(%rdx)
.L418:
	movl	$0, 84(%r15)
	movl	$1, %ecx
	jmp	.L416
.L480:
	movl	%ecx, %edi
.L424:
	movq	%r8, %rbx
	jmp	.L431
.L491:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2119:
	.size	_UTF16LEFromUnicodeWithOffsets, .-_UTF16LEFromUnicodeWithOffsets
	.p2align 4
	.type	_UTF16Open, @function
_UTF16Open:
.LFB2126:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	andl	$15, %eax
	cmpl	$2, %eax
	ja	.L498
	je	.L501
.L499:
	movabsq	$4294967296, %rax
	movq	%rax, 76(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	cmpb	$0, 8(%rsi)
	jne	.L499
	leaq	_UTF16v2Data_67(%rip), %rax
	movq	%rax, 48(%rdi)
	movq	40(%rdi), %rax
	movl	$65023, (%rax)
	jmp	.L499
	.cfi_endproc
.LFE2126:
	.size	_UTF16Open, .-_UTF16Open
	.p2align 4
	.type	_UTF16BEOpen, @function
_UTF16BEOpen:
.LFB2117:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testb	$14, %al
	jne	.L503
	andl	$15, %eax
	jne	.L504
	movl	$8, 76(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	movl	$1, (%rdx)
.L502:
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	movl	$0, 76(%rdi)
	cmpl	$1, %eax
	jne	.L502
	movl	$1, 80(%rdi)
	ret
	.cfi_endproc
.LFE2117:
	.size	_UTF16BEOpen, .-_UTF16BEOpen
	.p2align 4
	.type	_UTF16LEReset, @function
_UTF16LEReset:
.LFB2122:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	andl	$15, %eax
	cmpl	$1, %esi
	jle	.L507
.L511:
	cmpl	$1, %eax
	je	.L515
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	movl	$1, 80(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	xorl	%edx, %edx
	testl	%eax, %eax
	sete	%dl
	sall	$3, %edx
	movl	%edx, 76(%rdi)
	cmpl	$1, %esi
	jne	.L511
	ret
	.cfi_endproc
.LFE2122:
	.size	_UTF16LEReset, .-_UTF16LEReset
	.p2align 4
	.type	_UTF16BEReset, @function
_UTF16BEReset:
.LFB2116:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	andl	$15, %eax
	cmpl	$1, %esi
	jle	.L517
.L521:
	cmpl	$1, %eax
	je	.L525
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	movl	$1, 80(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	xorl	%edx, %edx
	testl	%eax, %eax
	sete	%dl
	sall	$3, %edx
	movl	%edx, 76(%rdi)
	cmpl	$1, %esi
	jne	.L521
	ret
	.cfi_endproc
.LFE2116:
	.size	_UTF16BEReset, .-_UTF16BEReset
	.p2align 4
	.type	_UTF16LEOpen, @function
_UTF16LEOpen:
.LFB2123:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testb	$14, %al
	jne	.L527
	andl	$15, %eax
	jne	.L528
	movl	$8, 76(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	movl	$1, (%rdx)
.L526:
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	movl	$0, 76(%rdi)
	cmpl	$1, %eax
	jne	.L526
	movl	$1, 80(%rdi)
	ret
	.cfi_endproc
.LFE2123:
	.size	_UTF16LEOpen, .-_UTF16LEOpen
	.p2align 4
	.type	_UTF16GetNextUChar, @function
_UTF16GetNextUChar:
.LFB2132:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movl	76(%rdx), %eax
	cmpl	$8, %eax
	je	.L531
	cmpl	$9, %eax
	jne	.L547
	movq	16(%rdi), %rcx
	movq	24(%rdi), %r8
	cmpq	%r8, %rcx
	jnb	.L540
	leaq	2(%rcx), %r9
	cmpq	%r9, %r8
	jb	.L546
	movzwl	(%rcx), %eax
	movl	%eax, %r11d
	andl	$63488, %r11d
	cmpl	$55296, %r11d
	je	.L548
.L542:
	movq	%r9, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	movl	$-9, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	movq	16(%rdi), %rcx
	movq	24(%rdi), %r9
	cmpq	%r9, %rcx
	jnb	.L540
	leaq	2(%rcx), %r10
	cmpq	%r10, %r9
	jb	.L546
	movzwl	(%rcx), %r8d
	rolw	$8, %r8w
	movl	%r8d, %r11d
	movzwl	%r8w, %eax
	andl	$63488, %r11d
	cmpl	$55296, %r11d
	je	.L549
.L536:
	movq	%r10, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	movl	$8, (%rsi)
	movl	$65535, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	testb	$4, %ah
	jne	.L543
	leaq	4(%rcx), %r10
	cmpq	%r10, %r8
	jb	.L544
	movzbl	3(%rcx), %r8d
	movl	%r8d, %r11d
	movzbl	2(%rcx), %r8d
	sall	$8, %r11d
	orl	%r11d, %r8d
	movzwl	%r8w, %r11d
	andl	$64512, %r8d
	cmpl	$56320, %r8d
	jne	.L543
	sall	$10, %eax
	movq	%r10, %r9
	leal	-56613888(%r11,%rax), %eax
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L549:
	testw	$1024, %r8w
	jne	.L537
	leaq	4(%rcx), %r11
	cmpq	%r11, %r9
	jb	.L538
	movzbl	2(%rcx), %r8d
	movl	%r8d, %r9d
	movzbl	3(%rcx), %r8d
	sall	$8, %r9d
	orl	%r9d, %r8d
	movzwl	%r8w, %r9d
	andl	$64512, %r8d
	cmpl	$56320, %r8d
	jne	.L537
	sall	$10, %eax
	movq	%r11, %r10
	leal	-56613888(%r9,%rax), %eax
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L546:
	movzbl	(%rcx), %eax
	addq	$1, %rcx
	movb	%al, 65(%rdx)
	movq	8(%rdi), %rax
	movb	$1, 64(%rax)
	movl	$65535, %eax
	movq	%rcx, 16(%rdi)
	movl	$11, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	movb	$2, 64(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, 65(%rdx)
	movzbl	1(%rcx), %eax
	movb	%al, 66(%rdx)
	movl	$65535, %eax
	movl	$12, (%rsi)
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L543:
	movb	$2, 64(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, 65(%rdx)
	movzbl	1(%rcx), %eax
	movb	%al, 66(%rdx)
	movl	$65535, %eax
	movl	$12, (%rsi)
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L538:
	movq	%r9, %rax
	subq	%rcx, %rax
	movb	%al, 64(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, 65(%rdx)
	leaq	1(%rcx), %rax
	cmpq	%rax, %r9
	jbe	.L539
	movzbl	1(%rcx), %eax
	movb	%al, 66(%rdx)
	cmpq	%r10, %r9
	jbe	.L539
	movzbl	2(%rcx), %eax
	movb	%al, 67(%rdx)
.L539:
	movl	$11, (%rsi)
	movq	%r9, %r10
	movl	$65535, %eax
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%r8, %rax
	subq	%rcx, %rax
	movb	%al, 64(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, 65(%rdx)
	leaq	1(%rcx), %rax
	cmpq	%rax, %r8
	jbe	.L545
	movzbl	1(%rcx), %eax
	movb	%al, 66(%rdx)
	cmpq	%r9, %r8
	jbe	.L545
	movzbl	2(%rcx), %eax
	movb	%al, 67(%rdx)
.L545:
	movl	$11, (%rsi)
	movq	%r8, %r9
	movl	$65535, %eax
	jmp	.L542
	.cfi_endproc
.LFE2132:
	.size	_UTF16GetNextUChar, .-_UTF16GetNextUChar
	.globl	_UTF16v2Data_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_UTF16v2Data_67, @object
	.size	_UTF16v2Data_67, 296
_UTF16v2Data_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_UTF16v2StaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_UTF16v2Impl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_UTF16v2StaticData, @object
	.size	_ZL18_UTF16v2StaticData, 100
_ZL18_UTF16v2StaticData:
	.long	100
	.string	"UTF-16,version=2"
	.zero	43
	.long	1204
	.byte	0
	.byte	29
	.byte	2
	.byte	2
	.string	"\377\375"
	.zero	1
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL12_UTF16v2Impl, @object
	.size	_ZL12_UTF16v2Impl, 144
_ZL12_UTF16v2Impl:
	.long	29
	.zero	4
	.quad	0
	.quad	0
	.quad	_UTF16Open
	.quad	0
	.quad	_UTF16Reset
	.quad	_UTF16ToUnicodeWithOffsets
	.quad	_UTF16ToUnicodeWithOffsets
	.quad	_UTF16BEFromUnicodeWithOffsets
	.quad	_UTF16BEFromUnicodeWithOffsets
	.quad	_UTF16GetNextUChar
	.quad	0
	.quad	_UTF16GetName
	.quad	0
	.quad	0
	.quad	ucnv_getNonSurrogateUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_UTF16Data_67
	.section	.data.rel.ro.local
	.align 32
	.type	_UTF16Data_67, @object
	.size	_UTF16Data_67, 296
_UTF16Data_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL16_UTF16StaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL10_UTF16Impl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL16_UTF16StaticData, @object
	.size	_ZL16_UTF16StaticData, 100
_ZL16_UTF16StaticData:
	.long	100
	.string	"UTF-16"
	.zero	53
	.long	1204
	.byte	0
	.byte	29
	.byte	2
	.byte	2
	.string	"\375\377"
	.zero	1
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL10_UTF16Impl, @object
	.size	_ZL10_UTF16Impl, 144
_ZL10_UTF16Impl:
	.long	29
	.zero	4
	.quad	0
	.quad	0
	.quad	_UTF16Open
	.quad	0
	.quad	_UTF16Reset
	.quad	_UTF16ToUnicodeWithOffsets
	.quad	_UTF16ToUnicodeWithOffsets
	.quad	_UTF16LEFromUnicodeWithOffsets
	.quad	_UTF16LEFromUnicodeWithOffsets
	.quad	_UTF16GetNextUChar
	.quad	0
	.quad	_UTF16GetName
	.quad	0
	.quad	0
	.quad	ucnv_getNonSurrogateUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_UTF16LEData_67
	.section	.data.rel.ro.local
	.align 32
	.type	_UTF16LEData_67, @object
	.size	_UTF16LEData_67, 296
_UTF16LEData_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_UTF16LEStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_UTF16LEImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_UTF16LEStaticData, @object
	.size	_ZL18_UTF16LEStaticData, 100
_ZL18_UTF16LEStaticData:
	.long	100
	.string	"UTF-16LE"
	.zero	51
	.long	1202
	.byte	0
	.byte	6
	.byte	2
	.byte	2
	.string	"\375\377"
	.zero	1
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL12_UTF16LEImpl, @object
	.size	_ZL12_UTF16LEImpl, 144
_ZL12_UTF16LEImpl:
	.long	6
	.zero	4
	.quad	0
	.quad	0
	.quad	_UTF16LEOpen
	.quad	0
	.quad	_UTF16LEReset
	.quad	_UTF16LEToUnicodeWithOffsets
	.quad	_UTF16LEToUnicodeWithOffsets
	.quad	_UTF16LEFromUnicodeWithOffsets
	.quad	_UTF16LEFromUnicodeWithOffsets
	.quad	_UTF16LEGetNextUChar
	.quad	0
	.quad	_UTF16LEGetName
	.quad	0
	.quad	0
	.quad	ucnv_getNonSurrogateUnicodeSet_67
	.quad	0
	.quad	0
	.section	.rodata
	.type	_ZZ30_UTF16LEFromUnicodeWithOffsetsE3bom, @object
	.size	_ZZ30_UTF16LEFromUnicodeWithOffsetsE3bom, 2
_ZZ30_UTF16LEFromUnicodeWithOffsetsE3bom:
	.ascii	"\377\376"
	.globl	_UTF16BEData_67
	.section	.data.rel.ro.local
	.align 32
	.type	_UTF16BEData_67, @object
	.size	_UTF16BEData_67, 296
_UTF16BEData_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL18_UTF16BEStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL12_UTF16BEImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL18_UTF16BEStaticData, @object
	.size	_ZL18_UTF16BEStaticData, 100
_ZL18_UTF16BEStaticData:
	.long	100
	.string	"UTF-16BE"
	.zero	51
	.long	1200
	.byte	0
	.byte	5
	.byte	2
	.byte	2
	.string	"\377\375"
	.zero	1
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL12_UTF16BEImpl, @object
	.size	_ZL12_UTF16BEImpl, 144
_ZL12_UTF16BEImpl:
	.long	5
	.zero	4
	.quad	0
	.quad	0
	.quad	_UTF16BEOpen
	.quad	0
	.quad	_UTF16BEReset
	.quad	_UTF16BEToUnicodeWithOffsets
	.quad	_UTF16BEToUnicodeWithOffsets
	.quad	_UTF16BEFromUnicodeWithOffsets
	.quad	_UTF16BEFromUnicodeWithOffsets
	.quad	_UTF16BEGetNextUChar
	.quad	0
	.quad	_UTF16BEGetName
	.quad	0
	.quad	0
	.quad	ucnv_getNonSurrogateUnicodeSet_67
	.quad	0
	.quad	0
	.section	.rodata
	.type	_ZZ30_UTF16BEFromUnicodeWithOffsetsE3bom, @object
	.size	_ZZ30_UTF16BEFromUnicodeWithOffsetsE3bom, 2
_ZZ30_UTF16BEFromUnicodeWithOffsetsE3bom:
	.ascii	"\376\377"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
