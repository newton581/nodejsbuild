	.file	"locutil.cpp"
	.text
	.p2align 4
	.type	service_cleanup, @function
service_cleanup:
.LFB3094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZL19LocaleUtility_cache(%rip), %r12
	testq	%r12, %r12
	je	.L2
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3
	call	uhash_close_67@PLT
.L3:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	$0, _ZL19LocaleUtility_cache(%rip)
.L2:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3094:
	.size	service_cleanup, .-service_cleanup
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3406:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3406:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3409:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L25
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L13
	cmpb	$0, 12(%rbx)
	jne	.L26
.L17:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L17
	.cfi_endproc
.LFE3409:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3412:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L29
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3412:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3415:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L32
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3415:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L38
.L34:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L39
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3417:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3418:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3418:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3419:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3419:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3420:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3420:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3421:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3421:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3422:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3422:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3423:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L55
	testl	%edx, %edx
	jle	.L55
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L58
.L47:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L47
	.cfi_endproc
.LFE3423:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L62
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L62
	testl	%r12d, %r12d
	jg	.L69
	cmpb	$0, 12(%rbx)
	jne	.L70
.L64:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L64
.L70:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L62:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3424:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L72
	movq	(%rdi), %r8
.L73:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L76
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L76
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3425:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3426:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L83
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3426:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3427:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3427:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3428:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3428:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3429:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3429:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3431:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3431:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3433:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3433:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleUtility21canonicalLocaleStringEPKNS_13UnicodeStringERS1_
	.type	_ZN6icu_6713LocaleUtility21canonicalLocaleStringEPKNS_13UnicodeStringERS1_, @function
_ZN6icu_6713LocaleUtility21canonicalLocaleStringEPKNS_13UnicodeStringERS1_:
.LFB3096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L126
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movswl	8(%r12), %ecx
	testw	%cx, %cx
	js	.L92
	sarl	$5, %ecx
.L93:
	xorl	%edx, %edx
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movswl	8(%r12), %ecx
	movl	%eax, %ebx
	testw	%cx, %cx
	js	.L94
	sarl	$5, %ecx
.L95:
	xorl	%edx, %edx
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %r14d
	movzwl	8(%r12), %eax
	testl	%r14d, %r14d
	js	.L122
	cmpl	%ebx, %r14d
	jge	.L122
.L98:
	testw	%ax, %ax
	js	.L102
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L103:
	xorl	%edx, %edx
	movl	$95, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	movl	%eax, %r13d
	leaq	10(%r12), %rax
	cmovs	%r14d, %r13d
	movq	%rax, -56(%rbp)
	xorl	%ebx, %ebx
	leal	-1(%r13), %r15d
	testl	%r13d, %r13d
	jg	.L113
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	movswl	%ax, %edx
	sarl	$5, %edx
.L108:
	cmpl	%edi, %edx
	jbe	.L109
	testb	$2, %al
	je	.L110
	movq	-56(%rbp), %rax
.L111:
	movzwl	(%rax,%rbx,2), %edx
	leal	-65(%rdx), %eax
	cmpw	$25, %ax
	ja	.L109
	addl	$32, %edx
	movq	%r12, %rdi
	movzwl	%dx, %edx
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
.L109:
	leaq	1(%rbx), %rax
	cmpq	%rbx, %r15
	je	.L112
	movq	%rax, %rbx
.L113:
	movzwl	8(%r12), %eax
	movl	%ebx, %edi
	movl	%ebx, %esi
	testw	%ax, %ax
	jns	.L128
	movl	12(%r12), %edx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L92:
	movl	12(%r12), %ecx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L110:
	movq	24(%r12), %rax
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L127:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L112:
	movslq	%r13d, %r15
	leaq	10(%r12), %rbx
	addq	%r15, %r15
	cmpl	%r14d, %r13d
	jl	.L120
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L129:
	movswl	%ax, %edx
	sarl	$5, %edx
.L115:
	cmpl	%r13d, %edx
	jbe	.L116
	testb	$2, %al
	je	.L117
	movq	%rbx, %rax
.L118:
	movzwl	(%rax,%r15), %edx
	leal	-97(%rdx), %eax
	cmpw	$25, %ax
	ja	.L116
	subl	$32, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movzwl	%dx, %edx
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
.L116:
	addl	$1, %r13d
	addq	$2, %r15
	cmpl	%r14d, %r13d
	je	.L91
.L120:
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	jns	.L129
	movl	12(%r12), %edx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L117:
	movq	24(%r12), %rax
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L91:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	testl	%ebx, %ebx
	jns	.L121
	testw	%ax, %ax
	js	.L99
	movswl	%ax, %r14d
	sarl	$5, %r14d
	movl	%r14d, %ecx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L94:
	movl	12(%r12), %ecx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L99:
	movl	12(%r12), %r14d
	movl	%r14d, %ecx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L102:
	movl	12(%r12), %ecx
	jmp	.L103
.L121:
	movl	%ebx, %r14d
	jmp	.L98
	.cfi_endproc
.LFE3096:
	.size	_ZN6icu_6713LocaleUtility21canonicalLocaleStringEPKNS_13UnicodeStringERS1_, .-_ZN6icu_6713LocaleUtility21canonicalLocaleStringEPKNS_13UnicodeStringERS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleUtility12isFallbackOfERKNS_13UnicodeStringES3_
	.type	_ZN6icu_6713LocaleUtility12isFallbackOfERKNS_13UnicodeStringES3_, @function
_ZN6icu_6713LocaleUtility12isFallbackOfERKNS_13UnicodeStringES3_:
.LFB3100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movswl	8(%rsi), %r9d
	movq	%rsi, %rbx
	testw	%r9w, %r9w
	js	.L131
	sarl	$5, %r9d
.L132:
	movzwl	8(%r12), %eax
	movl	%eax, %edx
	andl	$1, %edx
	testw	%ax, %ax
	js	.L133
	movswl	%ax, %ecx
	sarl	$5, %ecx
	testb	%dl, %dl
	jne	.L136
.L134:
	testl	%ecx, %ecx
	je	.L136
	leaq	10(%r12), %rsi
	testb	$2, %al
	jne	.L138
	movq	24(%r12), %rsi
.L138:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	testl	%eax, %eax
	jne	.L136
	movzwl	8(%rbx), %ecx
	testw	%cx, %cx
	js	.L139
	movswl	8(%r12), %eax
	movswl	%cx, %edx
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L141
.L154:
	sarl	$5, %eax
.L142:
	movl	$1, %r8d
	cmpl	%edx, %eax
	je	.L130
	cmpl	%eax, %edx
	jbe	.L136
	andl	$2, %ecx
	jne	.L153
	movq	24(%rbx), %rbx
.L144:
	cltq
	cmpw	$95, (%rbx,%rax,2)
	popq	%rbx
	sete	%r8b
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movl	12(%r12), %ecx
	testb	%dl, %dl
	jne	.L136
	testl	%ecx, %ecx
	jns	.L134
.L136:
	xorl	%r8d, %r8d
.L130:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movswl	8(%r12), %eax
	movl	12(%rbx), %edx
	testw	%ax, %ax
	jns	.L154
.L141:
	movl	12(%r12), %eax
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L131:
	movl	12(%rsi), %r9d
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L153:
	addq	$10, %rbx
	jmp	.L144
	.cfi_endproc
.LFE3100:
	.size	_ZN6icu_6713LocaleUtility12isFallbackOfERKNS_13UnicodeStringES3_, .-_ZN6icu_6713LocaleUtility12isFallbackOfERKNS_13UnicodeStringES3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleUtility18initLocaleFromNameERKNS_13UnicodeStringERNS_6LocaleE
	.type	_ZN6icu_6713LocaleUtility18initLocaleFromNameERKNS_13UnicodeStringERNS_6LocaleE, @function
_ZN6icu_6713LocaleUtility18initLocaleFromNameERKNS_13UnicodeStringERNS_6LocaleE:
.LFB3097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 3, -56
	movswl	8(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %cl
	jne	.L156
	movq	%rdi, %r15
	testw	%cx, %cx
	js	.L157
	movswl	%cx, %eax
	sarl	$5, %eax
.L158:
	cmpl	$127, %eax
	jg	.L156
	xorl	%r14d, %r14d
	leaq	-192(%rbp), %r13
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L170:
	sarl	$5, %ecx
.L168:
	cmpl	%ecx, %r14d
	movl	%ecx, %edx
	movl	$64, %esi
	movq	%r15, %rdi
	cmovle	%r14d, %edx
	subl	%edx, %ecx
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	$128, %r8d
	movslq	%r14d, %rcx
	movl	%eax, %ebx
	subl	%r14d, %r8d
	addq	%r13, %rcx
	testl	%eax, %eax
	js	.L169
	movl	%eax, %edx
	movl	%r14d, %esi
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	subl	%r14d, %edx
	leal	1(%rbx), %r14d
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movslq	%ebx, %rax
	movswl	8(%r15), %ecx
	movb	$64, -192(%rbp,%rax)
.L159:
	testw	%cx, %cx
	jns	.L170
	movl	12(%r15), %ecx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r12, %rdi
	call	_ZN6icu_676Locale10setToBogusEv@PLT
.L160:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$376, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movl	12(%rdi), %eax
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L169:
	xorl	%r9d, %r9d
	movl	$2147483647, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	leaq	-416(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676Locale14createFromNameEPKc@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_676LocaleaSEOS0_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L160
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3097:
	.size	_ZN6icu_6713LocaleUtility18initLocaleFromNameERKNS_13UnicodeStringERNS_6LocaleE, .-_ZN6icu_6713LocaleUtility18initLocaleFromNameERKNS_13UnicodeStringERNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleUtility18initNameFromLocaleERKNS_6LocaleERNS_13UnicodeStringE
	.type	_ZN6icu_6713LocaleUtility18initNameFromLocaleERKNS_6LocaleERNS_13UnicodeStringE, @function
_ZN6icu_6713LocaleUtility18initNameFromLocaleERKNS_6LocaleERNS_13UnicodeStringE:
.LFB3098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 216(%rdi)
	je	.L173
	movq	%rsi, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L174:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	40(%rdi), %rsi
	leaq	-96(%rbp), %r13
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movswl	-88(%rbp), %ecx
	testw	%cx, %cx
	js	.L175
	sarl	$5, %ecx
.L176:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L175:
	movl	-84(%rbp), %ecx
	jmp	.L176
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3098:
	.size	_ZN6icu_6713LocaleUtility18initNameFromLocaleERKNS_6LocaleERNS_13UnicodeStringE, .-_ZN6icu_6713LocaleUtility18initNameFromLocaleERKNS_6LocaleERNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleUtility23getAvailableLocaleNamesERKNS_13UnicodeStringE
	.type	_ZN6icu_6713LocaleUtility23getAvailableLocaleNamesERKNS_13UnicodeStringE, @function
_ZN6icu_6713LocaleUtility23getAvailableLocaleNamesERKNS_13UnicodeStringE:
.LFB3099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -196(%rbp)
	movl	_ZL21LocaleUtilityInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L248
.L181:
	movl	4+_ZL21LocaleUtilityInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L188
	movl	%eax, -196(%rbp)
.L188:
	movq	_ZL19LocaleUtility_cache(%rip), %rbx
	testq	%rbx, %rbx
	je	.L191
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	uhash_get_67@PLT
	xorl	%edi, %edi
	movq	%rax, %r15
	call	umtx_unlock_67@PLT
	testq	%r15, %r15
	je	.L249
.L180:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$184, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	leaq	_ZL21LocaleUtilityInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L181
	leaq	service_cleanup(%rip), %rsi
	movl	$4, %edi
	call	ucln_common_registerCleanup_67@PLT
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L182
	movl	-196(%rbp), %r11d
	movq	$0, (%rax)
	testl	%r11d, %r11d
	jle	.L251
	movq	%rax, _ZL19LocaleUtility_cache(%rip)
.L187:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L206:
	movq	$0, _ZL19LocaleUtility_cache(%rip)
.L208:
	movl	-196(%rbp), %eax
	leaq	_ZL21LocaleUtilityInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL21LocaleUtilityInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L191:
	xorl	%r15d, %r15d
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L249:
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L191
	movl	-196(%rbp), %r8d
	movq	$0, (%rax)
	testl	%r8d, %r8d
	jle	.L252
.L212:
	movq	%r13, %r15
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L252:
	leaq	8(%rax), %r14
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	leaq	-196(%rbp), %rax
	xorl	%ecx, %ecx
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %r8
	movq	%rax, -216(%rbp)
	call	uhash_init_67@PLT
	movl	-196(%rbp), %edi
	testl	%edi, %edi
	jg	.L212
	movq	%r14, 0(%r13)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	uhash_setKeyDeleter_67@PLT
	movl	-196(%rbp), %esi
	testl	%esi, %esi
	jg	.L212
	xorl	%edx, %edx
	leaq	-179(%rbp), %rax
	leaq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movw	%dx, -180(%rbp)
	movq	-216(%rbp), %rdx
	movq	%rax, -192(%rbp)
	movl	$0, -136(%rbp)
	movl	$40, -184(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	-136(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L192
	movq	-192(%rbp), %r15
.L192:
	movq	-216(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-128(%rbp), %r14
	call	ures_openAvailableLocales_67@PLT
	movq	%rax, -224(%rbp)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L194
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L194:
	movq	0(%r13), %rdi
	movq	-216(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	uhash_put_67@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L195:
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rdi
	xorl	%esi, %esi
	call	uenum_unext_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L253
	movl	-196(%rbp), %eax
	testl	%eax, %eax
	jg	.L254
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L200
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	uhash_close_67@PLT
.L201:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L202:
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L204
.L247:
	movq	%rax, %rdi
	call	uenum_close_67@PLT
.L204:
	cmpb	$0, -180(%rbp)
	je	.L180
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L251:
	leaq	8(%rax), %r14
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	xorl	%ecx, %ecx
	leaq	-196(%rbp), %r8
	movq	%r14, %rdi
	call	uhash_init_67@PLT
	movl	-196(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L184
	movq	%r13, _ZL19LocaleUtility_cache(%rip)
	movq	0(%r13), %rdi
.L185:
	testq	%rdi, %rdi
	je	.L187
	call	uhash_close_67@PLT
	jmp	.L187
.L184:
	movq	%r14, 0(%r13)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	call	uhash_setKeyDeleter_67@PLT
	movl	-196(%rbp), %r9d
	movq	%r13, _ZL19LocaleUtility_cache(%rip)
	movq	0(%r13), %rdi
	testl	%r9d, %r9d
	jg	.L185
	movq	uhash_deleteHashtable_67@GOTPCREL(%rip), %rsi
	call	uhash_setValueDeleter_67@PLT
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L254:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L197
	call	uhash_close_67@PLT
.L197:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	jne	.L247
	jmp	.L204
.L200:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L203
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L203:
	movq	-216(%rbp), %rcx
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r13, %r15
	call	uhash_put_67@PLT
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	jmp	.L202
.L250:
	call	__stack_chk_fail@PLT
.L182:
	cmpl	$0, -196(%rbp)
	movq	$0, _ZL19LocaleUtility_cache(%rip)
	jg	.L206
	movl	$7, -196(%rbp)
	jmp	.L208
	.cfi_endproc
.LFE3099:
	.size	_ZN6icu_6713LocaleUtility23getAvailableLocaleNamesERKNS_13UnicodeStringE, .-_ZN6icu_6713LocaleUtility23getAvailableLocaleNamesERKNS_13UnicodeStringE
	.local	_ZL19LocaleUtility_cache
	.comm	_ZL19LocaleUtility_cache,8,8
	.local	_ZL21LocaleUtilityInitOnce
	.comm	_ZL21LocaleUtilityInitOnce,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
