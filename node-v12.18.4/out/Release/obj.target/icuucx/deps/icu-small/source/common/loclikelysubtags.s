	.file	"loclikelysubtags.cpp"
	.text
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3519:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3519:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3522:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	cmpb	$0, 12(%rbx)
	jne	.L16
.L7:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L7
	.cfi_endproc
.LFE3522:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3525:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3525:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3528:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L22
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3528:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3530:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L28
.L24:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L29
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3530:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3531:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3531:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3532:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3532:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3533:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3533:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3534:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3534:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3535:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3535:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3536:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L45
	testl	%edx, %edx
	jle	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L48
.L37:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L37
	.cfi_endproc
.LFE3536:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L52
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L52
	testl	%r12d, %r12d
	jg	.L59
	cmpb	$0, 12(%rbx)
	jne	.L60
.L54:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L54
.L60:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3537:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L62
	movq	(%rdi), %r8
.L63:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L66
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L66
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3538:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3539:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3539:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3540:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3540:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3541:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3541:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3542:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3542:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3544:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3544:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3546:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3546:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718LocaleDistanceDataC2EOS0_
	.type	_ZN6icu_6718LocaleDistanceDataC2EOS0_, @function
_ZN6icu_6718LocaleDistanceDataC2EOS0_:
.LFB3203:
	.cfi_startproc
	endbr64
	movl	32(%rsi), %eax
	movdqu	16(%rsi), %xmm2
	pxor	%xmm0, %xmm0
	movdqu	(%rsi), %xmm1
	movups	%xmm0, 16(%rsi)
	movl	%eax, 32(%rdi)
	movq	40(%rsi), %rax
	movups	%xmm1, (%rdi)
	movq	%rax, 40(%rdi)
	movups	%xmm2, 16(%rdi)
	ret
	.cfi_endproc
.LFE3203:
	.size	_ZN6icu_6718LocaleDistanceDataC2EOS0_, .-_ZN6icu_6718LocaleDistanceDataC2EOS0_
	.globl	_ZN6icu_6718LocaleDistanceDataC1EOS0_
	.set	_ZN6icu_6718LocaleDistanceDataC1EOS0_,_ZN6icu_6718LocaleDistanceDataC2EOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718LocaleDistanceDataD2Ev
	.type	_ZN6icu_6718LocaleDistanceDataD2Ev, @function
_ZN6icu_6718LocaleDistanceDataD2Ev:
.LFB3206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	-8(%rdi), %rax
	leaq	(%rax,%rax,2), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
.L90:
	cmpq	%rbx, %rdi
	je	.L82
.L86:
	subq	$48, %rbx
	cmpq	$0, 24(%rbx)
	je	.L90
	movq	%rbx, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movq	24(%r12), %rdi
	cmpq	%rdi, %rbx
	jne	.L86
.L82:
	popq	%rbx
	subq	$8, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydaEPv@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3206:
	.size	_ZN6icu_6718LocaleDistanceDataD2Ev, .-_ZN6icu_6718LocaleDistanceDataD2Ev
	.globl	_ZN6icu_6718LocaleDistanceDataD1Ev
	.set	_ZN6icu_6718LocaleDistanceDataD1Ev,_ZN6icu_6718LocaleDistanceDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714XLikelySubtagsC2ERNS_18XLikelySubtagsDataE
	.type	_ZN6icu_6714XLikelySubtagsC2ERNS_18XLikelySubtagsDataE, @function
_ZN6icu_6714XLikelySubtagsC2ERNS_18XLikelySubtagsDataE:
.LFB3225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	$97, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movq	88(%rsi), %rax
	movq	$0, 88(%rsi)
	movq	%rax, 8(%rdi)
	movq	104(%rsi), %rax
	movq	$0, 104(%rsi)
	movq	%rax, 16(%rdi)
	movq	112(%rsi), %rax
	movq	$0, 112(%rsi)
	movq	%rax, 24(%rdi)
	movq	120(%rsi), %rax
	movq	$0, 32(%rdi)
	movq	%rax, 40(%rdi)
	movq	%rax, 48(%rdi)
	movq	128(%rsi), %rax
	movl	$-1, 56(%rdi)
	movq	%rax, 296(%rdi)
	movq	144(%rsi), %rax
	movq	%rax, 304(%rdi)
	movq	152(%rsi), %rax
	movq	%rax, 312(%rdi)
	movq	160(%rsi), %rax
	movq	%rax, 320(%rdi)
	movq	168(%rsi), %rax
	movq	%rax, 328(%rdi)
	movl	176(%rsi), %eax
	movl	%eax, 336(%rdi)
	movq	184(%rsi), %rax
	movups	%xmm0, 160(%rsi)
	movq	%rax, 344(%rdi)
	movq	%r13, %rdi
	movq	$0, (%rsi)
	movq	$0, 128(%rsi)
	movl	$42, %esi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movl	56(%rbx), %eax
	movq	48(%rbx), %rdx
	movq	%r13, %rdi
	subq	40(%rbx), %rdx
	movl	$42, %esi
	addl	$2, %eax
	salq	$59, %rax
	orq	%rdx, %rax
	movq	%rax, 64(%rbx)
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movl	56(%rbx), %eax
	movq	48(%rbx), %rdx
	movq	%r13, %rdi
	subq	40(%rbx), %rdx
	movl	$42, %esi
	addl	$2, %eax
	salq	$59, %rax
	orq	%rdx, %rax
	movq	%rax, 72(%rbx)
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movq	48(%rbx), %rdi
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	sarl	%esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	movl	$-1, 56(%rbx)
	movl	%eax, 80(%rbx)
	movq	40(%rbx), %rax
	movq	%rax, 48(%rbx)
	.p2align 4,,10
	.p2align 3
.L95:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	cmpl	$1, %eax
	je	.L92
	movq	40(%rbx), %rax
	addq	$1, %r12
	movl	$-1, 56(%rbx)
	movq	%rax, 48(%rbx)
	cmpq	$123, %r12
	jne	.L95
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movl	56(%rbx), %eax
	movq	40(%rbx), %rcx
	movq	48(%rbx), %rdx
	addl	$2, %eax
	salq	$59, %rax
	subq	%rcx, %rdx
	orq	%rdx, %rax
	movq	%rax, -688(%rbx,%r12,8)
	addq	$1, %r12
	movq	%rcx, 48(%rbx)
	movl	$-1, 56(%rbx)
	cmpq	$123, %r12
	jne	.L95
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3225:
	.size	_ZN6icu_6714XLikelySubtagsC2ERNS_18XLikelySubtagsDataE, .-_ZN6icu_6714XLikelySubtagsC2ERNS_18XLikelySubtagsDataE
	.globl	_ZN6icu_6714XLikelySubtagsC1ERNS_18XLikelySubtagsDataE
	.set	_ZN6icu_6714XLikelySubtagsC1ERNS_18XLikelySubtagsDataE,_ZN6icu_6714XLikelySubtagsC2ERNS_18XLikelySubtagsDataE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714XLikelySubtagsD2Ev
	.type	_ZN6icu_6714XLikelySubtagsD2Ev, @function
_ZN6icu_6714XLikelySubtagsD2Ev:
.LFB3228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	call	ures_close_67@PLT
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L104
	cmpb	$0, 12(%r13)
	jne	.L130
.L105:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L104:
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L106
	movq	-8(%rdi), %rax
	leaq	(%rax,%rax,2), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	cmpq	%rbx, %rdi
	je	.L107
	.p2align 4,,10
	.p2align 3
.L111:
	subq	$48, %rbx
	cmpq	$0, 24(%rbx)
	je	.L108
	movq	%rbx, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movq	296(%r12), %rdi
	cmpq	%rdi, %rbx
	jne	.L111
.L107:
	subq	$8, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L106:
	movq	320(%r12), %rdi
	call	uprv_free_67@PLT
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	-8(%rdi), %rax
	leaq	(%rax,%rax,2), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	cmpq	%rbx, %rdi
	je	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	subq	$48, %rbx
	cmpq	$0, 24(%rbx)
	je	.L114
	movq	%rbx, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movq	328(%r12), %rdi
	cmpq	%rdi, %rbx
	jne	.L117
.L113:
	subq	$8, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L112:
	leaq	32(%r12), %rdi
	call	_ZN6icu_679BytesTrieD1Ev@PLT
	movq	24(%r12), %rdi
	call	uhash_close_67@PLT
	movq	16(%r12), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uhash_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	cmpq	%rdi, %rbx
	jne	.L111
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L114:
	cmpq	%rdi, %rbx
	jne	.L117
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L130:
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L105
	.cfi_endproc
.LFE3228:
	.size	_ZN6icu_6714XLikelySubtagsD2Ev, .-_ZN6icu_6714XLikelySubtagsD2Ev
	.globl	_ZN6icu_6714XLikelySubtagsD1Ev
	.set	_ZN6icu_6714XLikelySubtagsD1Ev,_ZN6icu_6714XLikelySubtagsD2Ev
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_17cleanupEv, @function
_ZN6icu_6712_GLOBAL__N_17cleanupEv:
.LFB3219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZN6icu_6712_GLOBAL__N_114gLikelySubtagsE(%rip), %r12
	testq	%r12, %r12
	je	.L132
	movq	%r12, %rdi
	call	_ZN6icu_6714XLikelySubtagsD1Ev
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L132:
	movq	$0, _ZN6icu_6712_GLOBAL__N_114gLikelySubtagsE(%rip)
	movl	$1, %eax
	movl	$0, _ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip)
	mfence
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3219:
	.size	_ZN6icu_6712_GLOBAL__N_17cleanupEv, .-_ZN6icu_6712_GLOBAL__N_17cleanupEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	.type	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci, @function
_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci:
.LFB3236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	1(%rsi,%rdx), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	(%rsi,%rdx), %r12d
	testb	%r12b, %r12b
	jne	.L142
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%rbx, %rdi
	addq	$1, %r13
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	testb	$1, %al
	je	.L150
.L142:
	movzbl	%r12b, %esi
	movzbl	0(%r13), %r12d
	testb	%r12b, %r12b
	jne	.L151
	orl	$-128, %esi
	movq	%rbx, %rdi
	movzbl	%sil, %esi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	cmpl	$2, %eax
	je	.L143
.L152:
	movl	$1, %r8d
	cmpl	$3, %eax
	je	.L137
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	setne	%r8b
	negl	%r8d
.L137:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movl	$42, %esi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	cmpl	$2, %eax
	jne	.L152
.L143:
	movq	16(%rbx), %rdi
	movzbl	(%rdi), %esi
	addq	$8, %rsp
	addq	$1, %rdi
	popq	%rbx
	popq	%r12
	sarl	%esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-1, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3236:
	.size	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci, .-_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"und"
.LC2:
	.string	"Zzzz"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714XLikelySubtags8maximizeEPKcS2_S2_
	.type	_ZNK6icu_6714XLikelySubtags8maximizeEPKcS2_S2_, @function
_ZNK6icu_6714XLikelySubtags8maximizeEPKcS2_S2_:
.LFB3233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movl	$4, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movq	%rdx, %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	.LC1(%rip), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	.LC0(%rip), %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	xorl	%r8d, %r8d
	testb	%al, %al
	je	.L154
	movsbl	(%rdx), %r8d
	movq	%rdx, %r12
.L154:
	movl	$5, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r9, %rsi
	movzbl	(%r14), %eax
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	movsbl	%dl, %edx
	subl	$90, %eax
	jne	.L155
	movzbl	1(%r14), %eax
	subl	$90, %eax
	je	.L213
.L155:
	testl	%edx, %edx
	je	.L156
.L218:
	testl	%eax, %eax
	jne	.L214
.L157:
	leaq	.LC0(%rip), %r14
.L158:
	movq	48(%r15), %rdx
	movq	40(%r15), %rax
	subl	$97, %r8d
	movq	$0, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	56(%r15), %edx
	movq	%rax, -88(%rbp)
	movl	%edx, -72(%rbp)
	cmpl	$25, %r8d
	jbe	.L215
.L161:
	leaq	-96(%rbp), %r8
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r9, -112(%rbp)
	movq	%r8, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %r8
.L162:
	movq	-88(%rbp), %rsi
	testl	%eax, %eax
	js	.L163
	movzbl	(%r12), %edx
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdi
	cmpb	$1, %dl
	sbbl	%r10d, %r10d
	andl	$-4, %r10d
	addl	$6, %r10d
	cmpb	$1, %dl
	sbbl	%ebx, %ebx
	addl	$2, %ecx
	subq	%rsi, %rdi
	notl	%ebx
	salq	$59, %rcx
	andl	$4, %ebx
	orq	%rdi, %rcx
	testl	%eax, %eax
	je	.L216
	movzbl	(%r9), %esi
	testb	%sil, %sil
	cmovne	%r10d, %ebx
	cmpl	$1, %eax
	je	.L170
.L171:
	cmpb	$0, (%r14)
	je	.L178
	orl	$1, %ebx
.L179:
	cltq
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	296(%r15), %rax
	testb	%dl, %dl
	je	.L196
.L190:
	movl	$1, %edx
.L189:
	testb	$4, %bl
	jne	.L186
	movq	(%rax), %r12
.L186:
	testb	$2, %bl
	jne	.L187
	movq	8(%rax), %r9
.L187:
	testl	%edx, %edx
	jne	.L188
	movq	16(%rax), %r14
.L188:
	movq	%r12, %xmm0
	movq	%r9, %xmm1
	movq	%r8, -104(%rbp)
	movq	%r14, %rdi
	movq	%r14, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 24(%r13)
	movups	%xmm0, 0(%r13)
.L212:
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	-104(%rbp), %r8
	movl	%ebx, 36(%r13)
	movl	%eax, 32(%r13)
	movl	$0, 40(%r13)
	movq	%r8, %rdi
	call	_ZN6icu_679BytesTrieD1Ev@PLT
.L153:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movzbl	2(%r14), %eax
	testl	%edx, %edx
	jne	.L218
.L156:
	leaq	.LC0(%rip), %r9
	testl	%eax, %eax
	je	.L157
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L214:
	cmpb	$0, (%r9)
	je	.L158
	cmpb	$0, (%r14)
	je	.L158
	testb	%r8b, %r8b
	jne	.L219
	movq	40(%r15), %rax
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	48(%r15), %rax
	movq	%rax, -80(%rbp)
	movl	56(%r15), %eax
	movl	%eax, -72(%rbp)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L178:
	cltq
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	296(%r15), %rax
	testb	%dl, %dl
	leaq	.LC1(%rip), %rdx
	cmove	%rdx, %r12
	testl	%ebx, %ebx
	jne	.L184
	movq	16(%rax), %rdi
	movdqu	(%rax), %xmm2
	movq	%r8, -104(%rbp)
	movl	36(%rax), %ebx
	movq	$0, 24(%r13)
	movq	%rdi, 16(%r13)
	movups	%xmm2, 0(%r13)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L163:
	movq	64(%r15), %rax
	movq	%r8, %rdi
	movq	%r9, -112(%rbp)
	movl	$4, %ebx
	movq	%r8, -104(%rbp)
	movq	%rax, %rdx
	shrq	$59, %rdx
	subl	$2, %edx
	movl	%edx, -72(%rbp)
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	xorl	%edx, %edx
	addq	%rax, %rsi
	movq	%rsi, -80(%rbp)
	movq	%r9, %rsi
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	movl	$6, %r10d
	testl	%eax, %eax
	js	.L220
.L167:
	movl	-72(%rbp), %ecx
	cmpb	$0, (%r9)
	cmovne	%r10d, %ebx
	movq	-80(%rbp), %rdx
	subq	-88(%rbp), %rdx
	addl	$2, %ecx
	salq	$59, %rcx
	orq	%rdx, %rcx
.L174:
	testl	%eax, %eax
	jle	.L170
	movzbl	(%r12), %edx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L215:
	cmpb	$0, 1(%r12)
	je	.L161
	movslq	%r8d, %r8
	movq	88(%r15,%r8,8), %rdx
	testq	%rdx, %rdx
	je	.L161
	movq	%rdx, %rcx
	leaq	-96(%rbp), %r8
	movq	%r12, %rsi
	movq	%r9, -112(%rbp)
	shrq	$59, %rcx
	movq	%r8, %rdi
	movq	%r8, -104(%rbp)
	subl	$2, %ecx
	movl	%ecx, -72(%rbp)
	movabsq	$576460752303423487, %rcx
	andq	%rcx, %rdx
	addq	%rdx, %rax
	movl	$1, %edx
	movq	%rax, -80(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L196:
	movl	$1, %edx
	leaq	.LC1(%rip), %r12
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%r9, %xmm3
	movq	%r14, 16(%r13)
	movq	%r12, %xmm0
	movq	%r14, %rdi
	movq	$0, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, 36(%r13)
	movl	%eax, 32(%r13)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L170:
	xorl	%edx, %edx
	movq	%r8, %rdi
	movq	%r14, %rsi
	movq	%r9, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %rcx
	testl	%eax, %eax
	movq	-120(%rbp), %r9
	js	.L180
.L176:
	cmpb	$0, (%r14)
	movzbl	(%r12), %edx
	je	.L178
	orl	$1, %ebx
.L181:
	cltq
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	296(%r15), %rax
	testb	%dl, %dl
	jne	.L190
	leaq	.LC1(%rip), %r12
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L184:
	movl	%ebx, %edx
	andl	$1, %edx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L220:
	movq	-88(%rbp), %rdx
	movl	$6, %ebx
.L168:
	movq	72(%r15), %rax
	movq	%r8, %rdi
	movq	%r14, %rsi
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rax, %rcx
	shrq	$59, %rcx
	subl	$2, %ecx
	movl	%ecx, -72(%rbp)
	movabsq	$576460752303423487, %rcx
	andq	%rcx, %rax
	addq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, -80(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	testl	%eax, %eax
	jns	.L176
	orl	$1, %ebx
.L191:
	movl	80(%r15), %eax
	movzbl	(%r12), %edx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L180:
	orl	$1, %ebx
	testq	%rcx, %rcx
	je	.L191
	movq	%rcx, %rax
	xorl	%edx, %edx
	movq	%r8, %rdi
	movq	%r9, -112(%rbp)
	shrq	$59, %rax
	leaq	.LC0(%rip), %rsi
	movq	%r8, -104(%rbp)
	subl	$2, %eax
	movl	%eax, -72(%rbp)
	movabsq	$576460752303423487, %rax
	andq	%rax, %rcx
	addq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movzbl	(%r12), %edx
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	jmp	.L179
.L217:
	call	__stack_chk_fail@PLT
.L216:
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r8, %rdi
	movl	%r10d, -124(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r9
	testl	%eax, %eax
	movq	-120(%rbp), %rcx
	movl	-124(%rbp), %r10d
	jns	.L167
	movq	-88(%rbp), %rdx
	testq	%rcx, %rcx
	je	.L221
	movq	%rcx, %rax
	movq	%r8, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%r9, -120(%rbp)
	shrq	$59, %rax
	movl	%r10d, -112(%rbp)
	subl	$2, %eax
	movq	%r8, -104(%rbp)
	movl	%eax, -72(%rbp)
	movabsq	$576460752303423487, %rax
	andq	%rax, %rcx
	leaq	(%rdx,%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, -80(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movl	-72(%rbp), %ebx
	movl	-112(%rbp), %r10d
	movq	-80(%rbp), %rdx
	subq	-88(%rbp), %rdx
	leal	2(%rbx), %ecx
	movq	-120(%rbp), %r9
	movq	-104(%rbp), %r8
	movl	%r10d, %ebx
	salq	$59, %rcx
	orq	%rdx, %rcx
	jmp	.L174
.L221:
	movl	%r10d, %ebx
	jmp	.L168
	.cfi_endproc
.LFE3233:
	.size	_ZNK6icu_6714XLikelySubtags8maximizeEPKcS2_S2_, .-_ZNK6icu_6714XLikelySubtags8maximizeEPKcS2_S2_
	.section	.rodata.str1.1
.LC3:
	.string	"XA"
.LC4:
	.string	"XB"
.LC5:
	.string	"XC"
.LC6:
	.string	"PSACCENT"
.LC7:
	.string	"PSBIDI"
.LC8:
	.string	"PSCRACK"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714XLikelySubtags16makeMaximizedLsrEPKcS2_S2_S2_R10UErrorCode
	.type	_ZNK6icu_6714XLikelySubtags16makeMaximizedLsrEPKcS2_S2_S2_R10UErrorCode, @function
_ZNK6icu_6714XLikelySubtags16makeMaximizedLsrEPKcS2_S2_S2_R10UErrorCode:
.LFB3232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	16(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%r8), %eax
	cmpb	$88, %al
	je	.L258
.L223:
	cmpb	$80, (%r9)
	jne	.L227
	cmpb	$83, 1(%r9)
	je	.L259
.L227:
	movq	16(%r13), %rdi
	movq	%r15, %rsi
	movq	%r10, -72(%rbp)
	call	uhash_get_67@PLT
	movq	24(%r13), %rdi
	movq	%rbx, %rsi
	testq	%rax, %rax
	movq	%rax, %r12
	cmove	%r15, %r12
	call	uhash_get_67@PLT
	movq	-72(%rbp), %r10
	movq	%r13, %rsi
	movq	%r14, %rdi
	testq	%rax, %rax
	movq	%r12, %rdx
	cmovne	%rax, %rbx
	movq	%r10, %rcx
	movq	%rbx, %r8
	call	_ZNK6icu_6714XLikelySubtags8maximizeEPKcS2_S2_
.L222:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movzbl	1(%r8), %ecx
	testb	%cl, %cl
	je	.L223
	cmpb	$0, 2(%r8)
	jne	.L223
	cmpb	$66, %cl
	je	.L224
	cmpb	$67, %cl
	je	.L225
	cmpb	$65, %cl
	jne	.L223
	subq	$8, %rsp
	movl	$7, %r9d
	pushq	%rdx
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$9, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r9, %rsi
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	movsbl	%cl, %ecx
	testb	%al, %al
	jne	.L261
	testl	%ecx, %ecx
	je	.L241
	movl	$7, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r9, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L253
	movl	$6, %ecx
	leaq	.LC4(%rip), %rbx
.L234:
	subq	$8, %rsp
	movl	%ecx, %r9d
	pushq	%rdx
.L256:
	movq	%r10, %rcx
	movl	$43, %esi
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_673LSRC1EcPKcS2_S2_iR10UErrorCode@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L261:
	testl	%ecx, %ecx
	je	.L240
	movl	$7, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r9, %rsi
	repz cmpsb
	movl	$7, %ecx
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L234
	movq	%r9, %rsi
	movl	$8, %ecx
	leaq	.LC8(%rip), %rdi
	movl	$7, %r9d
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L227
.L236:
	subq	$8, %rsp
	pushq	%rdx
.L257:
	movq	%r15, %rdx
	movq	%rbx, %r8
	movq	%r10, %rcx
	movl	$44, %esi
	movq	%r14, %rdi
	call	_ZN6icu_673LSRC1EcPKcS2_S2_iR10UErrorCode@PLT
	popq	%rax
	popq	%rdx
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L240:
	movl	$7, %r9d
.L232:
	subq	$8, %rsp
	pushq	%rdx
.L255:
	movq	%rbx, %r8
	movq	%r14, %rdi
	movq	%r10, %rcx
	movq	%r15, %rdx
	movl	$39, %esi
	call	_ZN6icu_673LSRC1EcPKcS2_S2_iR10UErrorCode@PLT
	popq	%rdi
	popq	%r8
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$6, %r9d
	leaq	.LC3(%rip), %rbx
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L225:
	subq	$8, %rsp
	movl	$7, %r9d
	pushq	%rdx
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L224:
	subq	$8, %rsp
	movl	$7, %r9d
	pushq	%rdx
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L253:
	movl	$8, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%r9, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L227
	movl	$6, %r9d
	leaq	.LC5(%rip), %rbx
	jmp	.L236
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3232:
	.size	_ZNK6icu_6714XLikelySubtags16makeMaximizedLsrEPKcS2_S2_S2_R10UErrorCode, .-_ZNK6icu_6714XLikelySubtags16makeMaximizedLsrEPKcS2_S2_S2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode
	.type	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode, @function
_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode:
.LFB3230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdx), %rax
	cmpb	$64, (%rax)
	jne	.L263
	cmpb	$120, 1(%rax)
	je	.L303
.L263:
	movzbl	26(%rdx), %r9d
	movslq	32(%rdx), %rax
	leaq	26(%rdx), %rbx
	leaq	20(%rdx), %r11
	addq	208(%rdx), %rax
	leaq	8(%rdx), %r15
	cmpb	$88, %r9b
	je	.L304
.L265:
	cmpb	$80, (%rax)
	jne	.L268
	cmpb	$83, 1(%rax)
	je	.L305
.L268:
	movq	16(%r13), %rdi
	movq	%r15, %rsi
	movq	%r11, -72(%rbp)
	call	uhash_get_67@PLT
	movq	24(%r13), %rdi
	movq	%rbx, %rsi
	testq	%rax, %rax
	movq	%rax, %r12
	cmove	%r15, %r12
	call	uhash_get_67@PLT
	movq	-72(%rbp), %r11
	movq	%r13, %rsi
	movq	%r14, %rdi
	testq	%rax, %rax
	movq	%r12, %rdx
	cmovne	%rax, %rbx
	movq	%r11, %rcx
	movq	%rbx, %r8
	call	_ZNK6icu_6714XLikelySubtags8maximizeEPKcS2_S2_
.L262:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L306
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movzbl	27(%rdx), %ecx
	testb	%cl, %cl
	je	.L265
	cmpb	$0, 28(%rdx)
	jne	.L265
	cmpb	$66, %cl
	je	.L266
	cmpb	$67, %cl
	je	.L267
	cmpb	$65, %cl
	jne	.L265
	subq	$8, %rsp
	movl	$7, %r9d
	pushq	%r10
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L303:
	cmpb	$61, 2(%rax)
	jne	.L263
	movq	%rax, (%rdi)
	leaq	.LC0(%rip), %rdi
	movq	%rdi, 8(%r14)
	movq	%rdi, 16(%r14)
	movq	$0, 24(%r14)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	$7, 36(%r14)
	movl	%eax, 32(%r14)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L305:
	movl	$9, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	movsbl	%dl, %edx
	testb	%r9b, %r9b
	jne	.L307
	testl	%edx, %edx
	je	.L283
	movl	$7, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L297
	movl	$6, %r9d
	leaq	.LC4(%rip), %rbx
.L276:
	subq	$8, %rsp
	pushq	%r10
.L301:
	movq	%r11, %rcx
	movl	$43, %esi
	movq	%rbx, %r8
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_673LSRC1EcPKcS2_S2_iR10UErrorCode@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L307:
	testl	%edx, %edx
	je	.L284
	movl	$7, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	movl	$7, %r9d
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L276
	movq	%rax, %rsi
	movl	$8, %ecx
	leaq	.LC8(%rip), %rdi
	movl	$7, %r9d
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L268
.L279:
	subq	$8, %rsp
	pushq	%r10
.L302:
	movq	%r15, %rdx
	movq	%rbx, %r8
	movq	%r11, %rcx
	movl	$44, %esi
	movq	%r14, %rdi
	call	_ZN6icu_673LSRC1EcPKcS2_S2_iR10UErrorCode@PLT
	popq	%rax
	popq	%rdx
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L284:
	movl	$7, %r9d
.L273:
	subq	$8, %rsp
	pushq	%r10
.L300:
	movq	%rbx, %r8
	movq	%r14, %rdi
	movq	%r11, %rcx
	movq	%r15, %rdx
	movl	$39, %esi
	call	_ZN6icu_673LSRC1EcPKcS2_S2_iR10UErrorCode@PLT
	popq	%rdi
	popq	%r8
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$6, %r9d
	leaq	.LC3(%rip), %rbx
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L267:
	subq	$8, %rsp
	movl	$7, %r9d
	pushq	%r10
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L266:
	subq	$8, %rsp
	movl	$7, %r9d
	pushq	%r10
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%rax, %rsi
	movl	$8, %ecx
	leaq	.LC8(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L268
	movl	$6, %r9d
	leaq	.LC5(%rip), %rbx
	jmp	.L279
.L306:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3230:
	.size	_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode, .-_ZNK6icu_6714XLikelySubtags20makeMaximizedLsrFromERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714XLikelySubtags14getLikelyIndexEPKcS2_
	.type	_ZNK6icu_6714XLikelySubtags14getLikelyIndexEPKcS2_, @function
_ZNK6icu_6714XLikelySubtags14getLikelyIndexEPKcS2_:
.LFB3235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %ecx
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	.LC1(%rip), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	repz cmpsb
	movl	$5, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%r13, %rsi
	seta	%dl
	sbbb	$0, %dl
	repz cmpsb
	movl	56(%rbx), %esi
	movq	48(%rbx), %rdi
	movq	40(%rbx), %rcx
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testb	%dl, %dl
	je	.L309
	movsbl	(%r8), %edx
	movq	$0, -96(%rbp)
	movq	%rcx, -88(%rbp)
	subl	$97, %edx
	testl	%eax, %eax
	leaq	.LC0(%rip), %rax
	movl	%esi, -72(%rbp)
	movq	%rdi, -80(%rbp)
	cmove	%rax, %r13
	cmpl	$25, %edx
	ja	.L311
	cmpb	$0, 1(%r8)
	jne	.L332
.L311:
	leaq	-96(%rbp), %r15
	xorl	%edx, %edx
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movl	%eax, %r12d
.L312:
	movq	-88(%rbp), %rcx
	testl	%r12d, %r12d
	js	.L313
	je	.L333
	cmpl	$1, %r12d
	je	.L317
.L318:
	movq	%r15, %rdi
	call	_ZN6icu_679BytesTrieD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L334
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	-88(%rbp), %rcx
.L319:
	movq	72(%rbx), %rax
	movq	%rax, %rdx
	shrq	$59, %rdx
	subl	$2, %edx
	movl	%edx, -72(%rbp)
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movq	%rax, -80(%rbp)
.L317:
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movl	%eax, %r12d
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L309:
	movq	$0, -96(%rbp)
	leaq	.LC0(%rip), %r8
	movq	%rcx, -88(%rbp)
	movq	%rdi, -80(%rbp)
	movl	%esi, -72(%rbp)
	testl	%eax, %eax
	jne	.L311
	leaq	.LC0(%rip), %r13
	movq	%r13, %r8
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L313:
	movq	64(%rbx), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	shrq	$59, %rdx
	subl	$2, %edx
	movl	%edx, -72(%rbp)
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	xorl	%edx, %edx
	addq	%rcx, %rax
	movq	%rax, -80(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L335
.L316:
	testl	%r12d, %r12d
	jle	.L317
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L332:
	movslq	%edx, %rdx
	movq	88(%rbx,%rdx,8), %rax
	testq	%rax, %rax
	je	.L311
	movq	%rax, %rdx
	leaq	-96(%rbp), %r15
	movq	%r8, %rsi
	shrq	$59, %rdx
	movq	%r15, %rdi
	subl	$2, %edx
	movl	%edx, -72(%rbp)
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	movl	$1, %edx
	addq	%rax, %rcx
	movq	%rcx, -80(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movl	%eax, %r12d
	jmp	.L312
.L334:
	call	__stack_chk_fail@PLT
.L333:
	movl	-72(%rbp), %eax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rcx, -112(%rbp)
	movq	-80(%rbp), %r14
	movl	%eax, -100(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movl	%eax, %r12d
	testl	%eax, %eax
	jns	.L316
	movl	-100(%rbp), %eax
	movq	-112(%rbp), %rcx
	addl	$2, %eax
	subq	%rcx, %r14
	movq	-88(%rbp), %rcx
	salq	$59, %rax
	orq	%r14, %rax
	je	.L319
	movq	%rax, %rdx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	shrq	$59, %rdx
	subl	$2, %edx
	movl	%edx, -72(%rbp)
	movabsq	$576460752303423487, %rdx
	andq	%rdx, %rax
	xorl	%edx, %edx
	addq	%rcx, %rax
	movq	%rax, -80(%rbp)
	call	_ZN6icu_6714XLikelySubtags8trieNextERNS_9BytesTrieEPKci
	movl	%eax, %r12d
	jmp	.L316
	.cfi_endproc
.LFE3235:
	.size	_ZNK6icu_6714XLikelySubtags14getLikelyIndexEPKcS2_, .-_ZNK6icu_6714XLikelySubtags14getLikelyIndexEPKcS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714XLikelySubtags13compareLikelyERKNS_3LSRES3_i
	.type	_ZNK6icu_6714XLikelySubtags13compareLikelyERKNS_3LSRES3_i, @function
_ZNK6icu_6714XLikelySubtags13compareLikelyERKNS_3LSRES3_i:
.LFB3234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	movq	(%rsi), %r15
	movq	(%rdx), %rsi
	movq	%r15, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L346
	movq	8(%r12), %rdi
	movq	8(%r13), %rsi
	movq	%rdi, -56(%rbp)
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L338
	testl	%ebx, %ebx
	js	.L339
	testb	$2, %bl
	je	.L353
.L339:
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rsi
	call	_ZNK6icu_6714XLikelySubtags14getLikelyIndexEPKcS2_
	movq	8(%r12), %rdi
	leal	0(,%rax,4), %ebx
.L340:
	cltq
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	296(%r14), %rax
	movq	8(%rax), %rsi
.L352:
	call	strcmp@PLT
	movl	%eax, %r8d
	movl	%ebx, %eax
	andl	$-2, %ebx
	orl	$1, %eax
	testl	%r8d, %r8d
	cmovne	%ebx, %eax
.L336:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movq	16(%r12), %rdi
	movq	16(%r13), %rsi
	movq	%rdi, -56(%rbp)
	call	strcmp@PLT
	movl	%eax, %r8d
	movl	%ebx, %eax
	andl	$-2, %eax
	testl	%r8d, %r8d
	je	.L336
	testl	%ebx, %ebx
	movq	-56(%rbp), %rdi
	js	.L343
	testb	$2, %bl
	je	.L343
	movl	%ebx, %eax
	sarl	$2, %eax
.L344:
	cltq
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	296(%r14), %rax
	movq	16(%rax), %rsi
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L353:
	movl	%ebx, %eax
	movq	-56(%rbp), %rdi
	sarl	$2, %eax
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L343:
	movq	%rdi, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6714XLikelySubtags14getLikelyIndexEPKcS2_
	movq	16(%r12), %rdi
	leal	2(,%rax,4), %ebx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$-4, %eax
	jmp	.L336
	.cfi_endproc
.LFE3234:
	.size	_ZNK6icu_6714XLikelySubtags13compareLikelyERKNS_3LSRES3_i, .-_ZNK6icu_6714XLikelySubtags13compareLikelyERKNS_3LSRES3_i
	.section	.text._ZN6icu_6718XLikelySubtagsData11readStringsERKNS_13ResourceTableEPKcRNS_13ResourceValueERNS_11LocalMemoryIiEERiR10UErrorCode,"axG",@progbits,_ZN6icu_6718XLikelySubtagsData11readStringsERKNS_13ResourceTableEPKcRNS_13ResourceValueERNS_11LocalMemoryIiEERiR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6718XLikelySubtagsData11readStringsERKNS_13ResourceTableEPKcRNS_13ResourceValueERNS_11LocalMemoryIiEERiR10UErrorCode
	.type	_ZN6icu_6718XLikelySubtagsData11readStringsERKNS_13ResourceTableEPKcRNS_13ResourceValueERNS_11LocalMemoryIiEERiR10UErrorCode, @function
_ZN6icu_6718XLikelySubtagsData11readStringsERKNS_13ResourceTableEPKcRNS_13ResourceValueERNS_11LocalMemoryIiEERiR10UErrorCode:
.LFB3218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$1, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$200, %rsp
	movq	%rdi, -192(%rbp)
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%r9, -184(%rbp)
	movq	%rbx, %rdx
	movq	16(%rbp), %r12
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L354
	movq	(%rbx), %rax
	leaq	-160(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	*80(%rax)
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L356
	movslq	-144(%rbp), %rdi
	movq	-184(%rbp), %rax
	movl	%edi, (%rax)
	testl	%edi, %edi
	jne	.L377
	.p2align 4,,10
	.p2align 3
.L354:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L378
	addq	$200, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L377:
	jle	.L358
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -200(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L358
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	movq	-184(%rbp), %rax
	movq	%r14, 0(%r13)
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L359
	movq	-192(%rbp), %rax
	leaq	-172(%rbp), %rcx
	xorl	%r13d, %r13d
	leaq	-128(%rbp), %r14
	movq	%rcx, -208(%rbp)
	leaq	-168(%rbp), %rcx
	addq	$8, %rax
	movq	%rcx, -216(%rbp)
	movq	%rax, -224(%rbp)
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L362:
	movzwl	-120(%rbp), %eax
	testb	$17, %al
	jne	.L368
	leaq	-118(%rbp), %r9
	testb	$2, %al
	cmove	-104(%rbp), %r9
.L363:
	movq	-224(%rbp), %rdi
	movq	%r9, %rsi
	movq	%r9, -232(%rbp)
	call	uhash_geti_67@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L361
	movq	-192(%rbp), %rax
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	88(%rax), %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-192(%rbp), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	88(%rax), %rdi
	movl	56(%rdi), %r8d
	movl	%r8d, -236(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	-236(%rbp), %r8d
	movq	-232(%rbp), %r9
	movq	%r12, %rcx
	movq	-224(%rbp), %rdi
	movl	%r8d, %edx
	movq	%r9, %rsi
	call	uhash_puti_67@PLT
	movl	-236(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L361:
	movq	-200(%rbp), %rax
	movq	%r14, %rdi
	movl	%r8d, (%rax,%r13,4)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L356
	movq	-184(%rbp), %rax
	addq	$1, %r13
	cmpl	%r13d, (%rax)
	jle	.L359
.L360:
	movq	%rbx, %rdx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE@PLT
	movq	(%rbx), %rax
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	-208(%rbp), %rsi
	movl	$0, -172(%rbp)
	call	*32(%rax)
	movl	-172(%rbp), %ecx
	movq	-216(%rbp), %rdx
	movq	%r14, %rdi
	movl	$1, %esi
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	(%r12), %edx
	xorl	%r8d, %r8d
	testl	%edx, %edx
	jg	.L361
	movq	-192(%rbp), %rax
	cmpb	$0, 96(%rax)
	je	.L362
	movl	$30, (%r12)
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L368:
	xorl	%r9d, %r9d
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L359:
	movl	$1, %r14d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L358:
	movl	$7, (%r12)
	xorl	%r14d, %r14d
	jmp	.L354
.L378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3218:
	.size	_ZN6icu_6718XLikelySubtagsData11readStringsERKNS_13ResourceTableEPKcRNS_13ResourceValueERNS_11LocalMemoryIiEERiR10UErrorCode, .-_ZN6icu_6718XLikelySubtagsData11readStringsERKNS_13ResourceTableEPKcRNS_13ResourceValueERNS_11LocalMemoryIiEERiR10UErrorCode
	.section	.rodata._ZN6icu_6718XLikelySubtagsData4loadER10UErrorCode.str1.1,"aMS",@progbits,1
.LC9:
	.string	"langInfo"
.LC10:
	.string	"likely"
.LC11:
	.string	"languageAliases"
.LC12:
	.string	"regionAliases"
.LC13:
	.string	"lsrs"
.LC14:
	.string	"trie"
.LC15:
	.string	"match"
.LC16:
	.string	"regionToPartitions"
.LC17:
	.string	"partitions"
.LC18:
	.string	"paradigms"
.LC19:
	.string	"distances"
	.section	.text._ZN6icu_6718XLikelySubtagsData4loadER10UErrorCode,"axG",@progbits,_ZN6icu_6718XLikelySubtagsData4loadER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6718XLikelySubtagsData4loadER10UErrorCode
	.type	_ZN6icu_6718XLikelySubtagsData4loadER10UErrorCode, @function
_ZN6icu_6718XLikelySubtagsData4loadER10UErrorCode:
.LFB3217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	xorl	%edi, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC9(%rip), %rsi
	subq	$632, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ures_openDirect_67@PLT
	movl	(%rbx), %edi
	movq	%rax, (%r12)
	testl	%edi, %edi
	jle	.L547
.L379:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L548
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_restore_state
	leaq	-256(%rbp), %r15
	leaq	-400(%rbp), %r13
	movq	%r15, %rdi
	leaq	-496(%rbp), %r14
	call	_ZN6icu_6720StackUResourceBundleC1Ev@PLT
	movq	(%r12), %rdi
	movq	%rbx, %r8
	movq	%r13, %rcx
	leaq	16+_ZTVN6icu_6717ResourceDataValueE(%rip), %rax
	movq	%r15, %rdx
	leaq	.LC10(%rip), %rsi
	movl	$-1, -328(%rbp)
	movq	%rax, -400(%rbp)
	call	ures_getValueWithFallback_67@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZNK6icu_6717ResourceDataValue8getTableER10UErrorCode@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L549
.L381:
	movq	%r13, %rdi
	call	_ZN6icu_6717ResourceDataValueD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6720StackUResourceBundleD1Ev@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L549:
	movq	%r13, %rdx
	leaq	.LC11(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L463
	leaq	-448(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -544(%rbp)
	call	_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L383
	movl	-432(%rbp), %eax
	movq	$0, -536(%rbp)
	movl	%eax, -568(%rbp)
	testl	%eax, %eax
	je	.L382
	jle	.L385
	movslq	%eax, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -536(%rbp)
	testq	%rax, %rax
	je	.L385
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movl	-568(%rbp), %eax
	leaq	-504(%rbp), %rcx
	xorl	%esi, %esi
	leaq	8(%r12), %rdx
	movq	%rcx, -560(%rbp)
	leaq	-320(%rbp), %rcx
	subl	$1, %eax
	movq	%r12, -552(%rbp)
	movq	%rax, -576(%rbp)
	leaq	-512(%rbp), %rax
	movq	%r15, -608(%rbp)
	movq	%rax, %r12
	movq	%rcx, %r15
	movq	%r14, -616(%rbp)
	movq	%rsi, %r14
	movq	%rdx, -584(%rbp)
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L389:
	movzwl	-312(%rbp), %eax
	testb	$17, %al
	jne	.L465
	leaq	-310(%rbp), %r9
	testb	$2, %al
	cmove	-296(%rbp), %r9
.L390:
	movq	-584(%rbp), %rdi
	movq	%r9, %rsi
	movq	%r9, -592(%rbp)
	call	uhash_geti_67@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L388
	movq	-552(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	88(%rax), %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-552(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	88(%rax), %rdi
	movl	56(%rdi), %r8d
	movl	%r8d, -600(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	-600(%rbp), %r8d
	movq	-592(%rbp), %r9
	movq	%rbx, %rcx
	movq	-584(%rbp), %rdi
	movl	%r8d, %edx
	movq	%r9, %rsi
	call	uhash_puti_67@PLT
	movl	-600(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L388:
	movq	-536(%rbp), %rax
	movq	%r15, %rdi
	movl	%r8d, (%rax,%r14,4)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L391
	leaq	1(%r14), %rax
	cmpq	%r14, -576(%rbp)
	je	.L550
	movq	%rax, %r14
.L386:
	movq	-544(%rbp), %rdi
	movq	%r13, %rdx
	movl	%r14d, %esi
	call	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$0, -512(%rbp)
	call	_ZNK6icu_6717ResourceDataValue9getStringERiR10UErrorCode@PLT
	movl	-512(%rbp), %ecx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-560(%rbp), %rdx
	movq	%rax, -504(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	(%rbx), %edx
	xorl	%r8d, %r8d
	testl	%edx, %edx
	jg	.L388
	movq	-552(%rbp), %rax
	cmpb	$0, 96(%rax)
	je	.L389
	movl	$30, (%rbx)
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L463:
	movq	$0, -536(%rbp)
	movl	$0, -568(%rbp)
.L382:
	movq	%r13, %rdx
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	jne	.L551
	movq	$0, -552(%rbp)
	movl	$0, -576(%rbp)
.L392:
	movq	%r13, %rdx
	leaq	.LC13(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE@PLT
	testb	%al, %al
	je	.L401
	leaq	-448(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -544(%rbp)
	call	_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode@PLT
	movl	(%rbx), %eax
	xorl	%r10d, %r10d
	testl	%eax, %eax
	jg	.L387
	movl	-432(%rbp), %eax
	testl	%eax, %eax
	je	.L401
	jle	.L404
	movslq	%eax, %rcx
	movl	%eax, -624(%rbp)
	leaq	0(,%rcx,4), %rdi
	movq	%rcx, -648(%rbp)
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	movq	%rax, -584(%rbp)
	je	.L404
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movl	-624(%rbp), %eax
	leaq	-504(%rbp), %rcx
	xorl	%esi, %esi
	leaq	8(%r12), %rdx
	movq	%rcx, -560(%rbp)
	leaq	-320(%rbp), %rcx
	subl	$1, %eax
	movq	%r12, -592(%rbp)
	movq	%rax, -600(%rbp)
	leaq	-512(%rbp), %rax
	movq	%r15, -640(%rbp)
	movq	%rax, %r12
	movq	%rcx, %r15
	movq	%r14, -656(%rbp)
	movq	%rsi, %r14
	movq	%rax, -664(%rbp)
	movq	%rdx, -608(%rbp)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L407:
	movzwl	-312(%rbp), %eax
	testb	$17, %al
	jne	.L474
	leaq	-310(%rbp), %r9
	testb	$2, %al
	cmove	-296(%rbp), %r9
.L408:
	movq	-608(%rbp), %rdi
	movq	%r9, %rsi
	movq	%r9, -616(%rbp)
	call	uhash_geti_67@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L406
	movq	-592(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	88(%rax), %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-592(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	88(%rax), %rdi
	movl	56(%rdi), %r8d
	movl	%r8d, -632(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	-632(%rbp), %r8d
	movq	-616(%rbp), %r9
	movq	%rbx, %rcx
	movq	-608(%rbp), %rdi
	movl	%r8d, %edx
	movq	%r9, %rsi
	call	uhash_puti_67@PLT
	movl	-632(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L406:
	movq	-584(%rbp), %rax
	movq	%r15, %rdi
	movl	%r8d, (%rax,%r14,4)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L537
	leaq	1(%r14), %rax
	cmpq	%r14, -600(%rbp)
	je	.L552
	movq	%rax, %r14
.L405:
	movq	-544(%rbp), %rdi
	movq	%r13, %rdx
	movl	%r14d, %esi
	call	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$0, -512(%rbp)
	call	_ZNK6icu_6717ResourceDataValue9getStringERiR10UErrorCode@PLT
	movl	-512(%rbp), %ecx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-560(%rbp), %rdx
	movq	%rax, -504(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	(%rbx), %r11d
	xorl	%r8d, %r8d
	testl	%r11d, %r11d
	jg	.L406
	movq	-592(%rbp), %rax
	cmpb	$0, 96(%rax)
	je	.L407
	movl	$30, (%rbx)
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L551:
	leaq	-448(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -544(%rbp)
	call	_ZNK6icu_6717ResourceDataValue8getArrayER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L393
	movl	-432(%rbp), %eax
	movq	$0, -552(%rbp)
	movl	%eax, -576(%rbp)
	testl	%eax, %eax
	je	.L392
	jle	.L395
	movslq	%eax, %rdi
	salq	$2, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -552(%rbp)
	testq	%rax, %rax
	je	.L395
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movl	-576(%rbp), %eax
	leaq	-504(%rbp), %rcx
	xorl	%esi, %esi
	leaq	8(%r12), %rdx
	movq	%rcx, -560(%rbp)
	leaq	-320(%rbp), %rcx
	subl	$1, %eax
	movq	%r12, -584(%rbp)
	movq	%rax, -592(%rbp)
	leaq	-512(%rbp), %rax
	movq	%r15, -624(%rbp)
	movq	%rax, %r12
	movq	%rcx, %r15
	movq	%r14, -632(%rbp)
	movq	%rsi, %r14
	movq	%rdx, -600(%rbp)
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L398:
	movzwl	-312(%rbp), %eax
	testb	$17, %al
	jne	.L470
	leaq	-310(%rbp), %r9
	testb	$2, %al
	cmove	-296(%rbp), %r9
.L399:
	movq	-600(%rbp), %rdi
	movq	%r9, %rsi
	movq	%r9, -608(%rbp)
	call	uhash_geti_67@PLT
	movl	%eax, %r8d
	testl	%eax, %eax
	jne	.L397
	movq	-584(%rbp), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	88(%rax), %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-584(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	88(%rax), %rdi
	movl	56(%rdi), %r8d
	movl	%r8d, -616(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	-616(%rbp), %r8d
	movq	-608(%rbp), %r9
	movq	%rbx, %rcx
	movq	-600(%rbp), %rdi
	movl	%r8d, %edx
	movq	%r9, %rsi
	call	uhash_puti_67@PLT
	movl	-616(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L397:
	movq	-552(%rbp), %rax
	movq	%r15, %rdi
	movl	%r8d, (%rax,%r14,4)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L400
	leaq	1(%r14), %rax
	cmpq	%r14, -592(%rbp)
	je	.L553
	movq	%rax, %r14
.L396:
	movq	-544(%rbp), %rdi
	movq	%r13, %rdx
	movl	%r14d, %esi
	call	_ZNK6icu_6713ResourceArray8getValueEiRNS_13ResourceValueE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$0, -512(%rbp)
	call	_ZNK6icu_6717ResourceDataValue9getStringERiR10UErrorCode@PLT
	movl	-512(%rbp), %ecx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-560(%rbp), %rdx
	movq	%rax, -504(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	(%rbx), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jg	.L397
	movq	-584(%rbp), %rax
	cmpb	$0, 96(%rax)
	je	.L398
	movl	$30, (%rbx)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L383:
	movq	$0, -552(%rbp)
	xorl	%r10d, %r10d
	movq	$0, -536(%rbp)
.L387:
	movq	%r10, %rdi
	call	uprv_free_67@PLT
	movq	-552(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-536(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L401:
	movl	-576(%rbp), %eax
	orl	-568(%rbp), %eax
	xorl	%r10d, %r10d
	testb	$1, %al
	je	.L546
.L410:
	movl	$3, (%rbx)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L393:
	movq	$0, -552(%rbp)
	xorl	%r10d, %r10d
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L465:
	xorl	%r9d, %r9d
	jmp	.L390
.L391:
	movq	$0, -552(%rbp)
	movq	-608(%rbp), %r15
	xorl	%r10d, %r10d
	jmp	.L387
.L550:
	movq	-552(%rbp), %r12
	movq	-608(%rbp), %r15
	movq	-616(%rbp), %r14
	jmp	.L382
.L385:
	movl	$7, (%rbx)
	xorl	%r10d, %r10d
	movq	$0, -552(%rbp)
	movq	$0, -536(%rbp)
	jmp	.L387
.L552:
	movl	-576(%rbp), %eax
	orl	-568(%rbp), %eax
	andl	$1, %eax
	movl	-624(%rbp), %esi
	movq	-584(%rbp), %r10
	movl	%eax, %ecx
	movq	-592(%rbp), %r12
	imulq	$1431655766, -648(%rbp), %rax
	movl	%esi, %edx
	movq	-640(%rbp), %r15
	movq	-656(%rbp), %r14
	sarl	$31, %edx
	shrq	$32, %rax
	subl	%edx, %eax
	leal	(%rax,%rax,2), %eax
	subl	%eax, %esi
	orl	%esi, %ecx
	movl	%ecx, -584(%rbp)
	jne	.L410
	movq	%r13, %rdx
	leaq	.LC14(%rip), %rsi
	movq	%r14, %rdi
	movq	%r10, -592(%rbp)
	call	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE@PLT
	movq	-592(%rbp), %r10
	testb	%al, %al
	je	.L546
	leaq	-528(%rbp), %r14
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%r10, -592(%rbp)
	movq	%r14, %rsi
	call	_ZNK6icu_6717ResourceDataValue9getBinaryERiR10UErrorCode@PLT
	movl	(%rbx), %r9d
	movq	-592(%rbp), %r10
	movq	%rax, 120(%r12)
	testl	%r9d, %r9d
	jg	.L387
	movq	(%r12), %rdi
	leaq	-524(%rbp), %r8
	movq	%r13, %rcx
	movq	%r15, %rdx
	leaq	.LC15(%rip), %rsi
	movl	$0, -524(%rbp)
	call	ures_getValueWithFallback_67@PLT
	movl	-524(%rbp), %eax
	movq	$0, -512(%rbp)
	movq	$0, -504(%rbp)
	movq	-592(%rbp), %r10
	movl	$0, -520(%rbp)
	testl	%eax, %eax
	movl	$0, -516(%rbp)
	jle	.L554
	cmpl	$2, %eax
	je	.L422
	movl	%eax, (%rbx)
.L423:
	movq	-504(%rbp), %rdi
	movq	%r10, -544(%rbp)
	call	uprv_free_67@PLT
	movq	-512(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-544(%rbp), %r10
	jmp	.L387
.L546:
	movl	$2, (%rbx)
	jmp	.L387
.L470:
	xorl	%r9d, %r9d
	jmp	.L399
.L400:
	movq	-624(%rbp), %r15
	xorl	%r10d, %r10d
	jmp	.L387
.L553:
	movq	-584(%rbp), %r12
	movq	-624(%rbp), %r15
	movq	-632(%rbp), %r14
	jmp	.L392
.L395:
	movl	$7, (%rbx)
	xorl	%r10d, %r10d
	movq	$0, -552(%rbp)
	jmp	.L387
.L474:
	xorl	%r9d, %r9d
	jmp	.L408
.L537:
	movq	-584(%rbp), %r10
	movq	-640(%rbp), %r15
	jmp	.L387
.L404:
	movl	$7, (%rbx)
	xorl	%r10d, %r10d
	jmp	.L387
.L548:
	call	__stack_chk_fail@PLT
.L422:
	movq	uhash_compareChars_67@GOTPCREL(%rip), %r14
	movl	-568(%rbp), %ecx
	movb	$1, 96(%r12)
	movq	%rbx, %r8
	movq	uhash_hashChars_67@GOTPCREL(%rip), %rdi
	movq	%r10, -560(%rbp)
	sarl	%ecx
	movq	%r14, %rdx
	movq	%r14, %rsi
	call	uhash_openSize_67@PLT
	xorl	%edi, %edi
	movq	%rax, 104(%r12)
	call	uhash_close_67@PLT
	xorl	%r8d, %r8d
	cmpl	$0, -568(%rbp)
	movq	-560(%rbp), %r10
	je	.L429
	movq	%r13, -592(%rbp)
	movq	%rbx, %r13
	movq	%r12, %rbx
	movq	%r8, %r12
	movq	%r10, -560(%rbp)
	jmp	.L424
.L556:
	movq	-536(%rbp), %rcx
	leal	1(%r12), %eax
	movq	88(%rbx), %rsi
	xorl	%edx, %edx
	movslq	(%rcx,%rax,4), %rax
	testl	%eax, %eax
	jle	.L428
	addq	(%rsi), %rax
	movq	%rax, %rdx
.L428:
	movq	-536(%rbp), %rax
	movslq	(%rax,%r12,4), %rax
	testl	%eax, %eax
	jle	.L541
	addq	(%rsi), %rax
	movq	%rax, %rsi
.L427:
	movq	104(%rbx), %rdi
	movq	%r13, %rcx
	addq	$2, %r12
	call	uhash_put_67@PLT
	cmpl	%r12d, -568(%rbp)
	jle	.L555
.L424:
	cmpb	$0, 96(%rbx)
	jne	.L556
	xorl	%edx, %edx
.L541:
	xorl	%esi, %esi
	jmp	.L427
.L555:
	movq	%rbx, %r12
	movq	-560(%rbp), %r10
	movq	%r13, %rbx
	movq	-592(%rbp), %r13
.L429:
	movl	-576(%rbp), %ecx
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%r14, %rsi
	movq	uhash_hashChars_67@GOTPCREL(%rip), %rdi
	movq	%r10, -560(%rbp)
	sarl	%ecx
	call	uhash_openSize_67@PLT
	xorl	%edi, %edi
	movq	%rax, 112(%r12)
	call	uhash_close_67@PLT
	cmpl	$0, -576(%rbp)
	movq	-560(%rbp), %r10
	je	.L425
	xorl	%r14d, %r14d
	jmp	.L426
.L558:
	movq	-552(%rbp), %rcx
	leal	1(%r14), %eax
	movq	88(%r12), %rsi
	xorl	%edx, %edx
	movslq	(%rcx,%rax,4), %rax
	testl	%eax, %eax
	jle	.L432
	addq	(%rsi), %rax
	movq	%rax, %rdx
.L432:
	movq	-552(%rbp), %rax
	movslq	(%rax,%r14,4), %rax
	testl	%eax, %eax
	jle	.L542
	addq	(%rsi), %rax
	movq	%rax, %rsi
.L431:
	movq	112(%r12), %rdi
	movq	%rbx, %rcx
	addq	$2, %r14
	call	uhash_put_67@PLT
	cmpl	%r14d, -576(%rbp)
	jle	.L557
.L426:
	cmpb	$0, 96(%r12)
	jne	.L558
	xorl	%edx, %edx
.L542:
	xorl	%esi, %esi
	jmp	.L431
.L557:
	movq	-560(%rbp), %r10
.L425:
	cmpl	$0, (%rbx)
	jg	.L423
	movl	-624(%rbp), %eax
	movl	$3, %ecx
	movq	%r10, -568(%rbp)
	cltd
	idivl	%ecx
	movslq	%eax, %r14
	movl	%eax, 136(%r12)
	imulq	$48, %r14, %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -560(%rbp)
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	-568(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L433
	movq	%r14, (%rax)
	leaq	8(%rax), %rsi
	testq	%r14, %r14
	je	.L434
	movq	-560(%rbp), %rcx
	leaq	.LC1(%rip), %rdi
	movq	%rsi, %rax
	movq	%rdi, %xmm0
	addq	%rcx, %rdx
	leaq	.LC0(%rip), %rcx
	movq	%rcx, %xmm1
	punpcklqdq	%xmm1, %xmm0
.L435:
	movq	%rcx, 16(%rax)
	addq	$48, %rax
	movups	%xmm0, -48(%rax)
	movq	$0, -24(%rax)
	movl	$0, -16(%rax)
	movl	$0, -12(%rax)
	movl	$0, -8(%rax)
	cmpq	%rax, %rdx
	jne	.L435
.L434:
	xorl	%r14d, %r14d
	movq	%r13, %rax
	movq	%rbx, -560(%rbp)
	movq	%r10, %rbx
	movq	%r14, %r13
	movq	%rsi, 128(%r12)
	movq	%rax, %r14
	jmp	.L440
.L560:
	leal	2(%r13), %eax
	movq	88(%r12), %rsi
	movl	%r13d, %ecx
	cltq
	movslq	(%rbx,%rax,4), %rax
	testl	%eax, %eax
	jle	.L483
	addq	(%rsi), %rax
	movq	%rax, %rdi
.L437:
	leal	1(%rcx), %eax
	xorl	%ecx, %ecx
	movslq	(%rbx,%rax,4), %rax
	testl	%eax, %eax
	jle	.L438
	addq	(%rsi), %rax
	movq	%rax, %rcx
.L438:
	movslq	(%rbx,%r13,4), %rax
	testl	%eax, %eax
	jle	.L543
	addq	(%rsi), %rax
.L436:
	movq	%rcx, %xmm2
	movq	%rax, %xmm0
	movq	%rdi, -432(%rbp)
	movq	$0, -424(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -448(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	-544(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -412(%rbp)
	salq	$4, %rdi
	addq	128(%r12), %rdi
	movl	%eax, -416(%rbp)
	call	_ZN6icu_673LSRaSEOS0_@PLT
	cmpq	$0, -424(%rbp)
	je	.L439
	movq	-544(%rbp), %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
.L439:
	addq	$3, %r13
	cmpl	%r13d, -624(%rbp)
	jle	.L559
.L440:
	cmpb	$0, 96(%r12)
	jne	.L560
	xorl	%edi, %edi
	xorl	%ecx, %ecx
.L543:
	xorl	%eax, %eax
	jmp	.L436
.L483:
	xorl	%edi, %edi
	jmp	.L437
.L554:
	movq	-544(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZNK6icu_6717ResourceDataValue8getTableER10UErrorCode@PLT
	cmpl	$0, (%rbx)
	movq	-592(%rbp), %r10
	jg	.L423
	movq	-544(%rbp), %rdi
	movq	%r13, %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE@PLT
	movq	-592(%rbp), %r10
	testb	%al, %al
	je	.L418
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r10, -592(%rbp)
	call	_ZNK6icu_6717ResourceDataValue9getBinaryERiR10UErrorCode@PLT
	cmpl	$0, (%rbx)
	movq	-592(%rbp), %r10
	movq	%rax, 144(%r12)
	jg	.L423
.L418:
	movq	-544(%rbp), %rdi
	movq	%r13, %rdx
	leaq	.LC16(%rip), %rsi
	movq	%r10, -592(%rbp)
	call	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE@PLT
	movq	-592(%rbp), %r10
	testb	%al, %al
	je	.L417
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r10, -592(%rbp)
	call	_ZNK6icu_6717ResourceDataValue9getBinaryERiR10UErrorCode@PLT
	cmpl	$0, (%rbx)
	movq	-592(%rbp), %r10
	movq	%rax, 152(%r12)
	jg	.L423
	cmpl	$1676, -528(%rbp)
	jle	.L420
.L417:
	pushq	%rsi
	movq	-664(%rbp), %r8
	movq	%r12, %rdi
	movq	%r13, %rcx
	pushq	%rbx
	movq	-544(%rbp), %rsi
	leaq	-520(%rbp), %r9
	leaq	.LC17(%rip), %rdx
	movq	%r10, -592(%rbp)
	call	_ZN6icu_6718XLikelySubtagsData11readStringsERKNS_13ResourceTableEPKcRNS_13ResourceValueERNS_11LocalMemoryIiEERiR10UErrorCode
	popq	%rdi
	movq	-592(%rbp), %r10
	testb	%al, %al
	popq	%r8
	je	.L423
	pushq	%rax
	movq	-560(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	pushq	%rbx
	movq	-544(%rbp), %rsi
	leaq	.LC18(%rip), %rdx
	leaq	-516(%rbp), %r9
	call	_ZN6icu_6718XLikelySubtagsData11readStringsERKNS_13ResourceTableEPKcRNS_13ResourceValueERNS_11LocalMemoryIiEERiR10UErrorCode
	popq	%rdx
	movq	-592(%rbp), %r10
	testb	%al, %al
	popq	%rcx
	je	.L423
	movl	-516(%rbp), %eax
	movl	$3, %ecx
	cltd
	idivl	%ecx
	testl	%edx, %edx
	jne	.L420
	movq	-544(%rbp), %rdi
	movq	%r13, %rdx
	leaq	.LC19(%rip), %rsi
	movq	%r10, -560(%rbp)
	call	_ZNK6icu_6713ResourceTable9findValueEPKcRNS_13ResourceValueE@PLT
	movq	-560(%rbp), %r10
	testb	%al, %al
	je	.L422
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6717ResourceDataValue12getIntVectorERiR10UErrorCode@PLT
	cmpl	$0, (%rbx)
	movq	-560(%rbp), %r10
	movq	%rax, 184(%r12)
	jg	.L423
	cmpl	$3, -528(%rbp)
	jg	.L422
.L420:
	movl	$3, (%rbx)
	jmp	.L423
.L559:
	movslq	-520(%rbp), %rax
	movq	%rbx, %r10
	movq	%r14, %r13
	movq	-560(%rbp), %rbx
	testl	%eax, %eax
	jg	.L561
.L441:
	movl	-516(%rbp), %eax
	testl	%eax, %eax
	jle	.L423
	movl	$3, %ecx
	cltd
	movq	%r10, -560(%rbp)
	idivl	%ecx
	movslq	%eax, %r14
	movl	%eax, 176(%r12)
	imulq	$48, %r14, %rdi
	addq	$8, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	-560(%rbp), %r10
	testq	%rax, %rax
	je	.L545
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rcx
	movq	%r14, (%rax)
	addq	$8, %rax
	movq	%rcx, %xmm0
	movq	%rdx, %xmm3
	movq	%rax, -560(%rbp)
	punpcklqdq	%xmm3, %xmm0
	jmp	.L448
.L446:
	movq	%rdx, 16(%rax)
	addq	$48, %rax
	movq	$0, -24(%rax)
	movl	$0, -16(%rax)
	movl	$0, -12(%rax)
	movl	$0, -8(%rax)
	movups	%xmm0, -48(%rax)
.L448:
	subq	$1, %r14
	cmpq	$-1, %r14
	jne	.L446
	movq	-560(%rbp), %rbx
	xorl	%r14d, %r14d
	movq	%r13, -576(%rbp)
	movq	%r10, -568(%rbp)
	movq	%r14, %r13
	movq	%r12, %r14
	movq	%rbx, %r12
	movl	-584(%rbp), %ebx
	jmp	.L447
.L562:
	leal	2(%rbx), %eax
	movq	88(%r14), %r8
	cltq
	movslq	(%r9,%rax,4), %rax
	testl	%eax, %eax
	jle	.L486
	addq	(%r8), %rax
	movq	%rax, %rdi
.L451:
	leal	1(%rbx), %eax
	xorl	%esi, %esi
	movslq	(%r9,%rax,4), %rax
	testl	%eax, %eax
	jle	.L452
	addq	(%r8), %rax
	movq	%rax, %rsi
.L452:
	movslq	(%r9,%r13), %rax
	testl	%eax, %eax
	jle	.L544
	addq	(%r8), %rax
.L450:
	movq	%rsi, %xmm4
	movq	%rax, %xmm0
	movq	%rdi, -432(%rbp)
	movq	$0, -424(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -448(%rbp)
	call	_ZN6icu_673LSR14indexForRegionEPKc@PLT
	movq	-544(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -412(%rbp)
	movl	%eax, -416(%rbp)
	call	_ZN6icu_673LSRaSEOS0_@PLT
	cmpq	$0, -424(%rbp)
	je	.L453
	movq	-544(%rbp), %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
.L453:
	addl	$3, %ebx
	addq	$48, %r12
	addq	$12, %r13
.L447:
	movq	-504(%rbp), %r9
	cmpl	%ebx, -516(%rbp)
	jle	.L449
	cmpb	$0, 96(%r14)
	jne	.L562
	xorl	%edi, %edi
	xorl	%esi, %esi
.L544:
	xorl	%eax, %eax
	jmp	.L450
.L486:
	xorl	%edi, %edi
	jmp	.L451
.L449:
	movq	-560(%rbp), %rax
	movq	-568(%rbp), %r10
	movq	-576(%rbp), %r13
	movq	%rax, 168(%r14)
	jmp	.L423
.L561:
	leaq	0(,%rax,8), %rdi
	movq	%r10, -560(%rbp)
	call	uprv_malloc_67@PLT
	movq	-560(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, 160(%r12)
	movq	%rax, %rcx
	je	.L545
	movl	-520(%rbp), %edi
	movq	-512(%rbp), %rsi
	xorl	%edx, %edx
	jmp	.L443
.L563:
	testl	%eax, %eax
	jle	.L484
	movq	88(%r12), %r8
	addq	(%r8), %rax
.L444:
	movq	%rax, (%rcx,%rdx,8)
	addq	$1, %rdx
.L443:
	cmpl	%edx, %edi
	jle	.L441
	cmpb	$0, 96(%r12)
	movslq	(%rsi,%rdx,4), %rax
	jne	.L563
.L484:
	xorl	%eax, %eax
	jmp	.L444
.L433:
	movq	$0, 128(%r12)
.L545:
	movl	$7, (%rbx)
	jmp	.L423
	.cfi_endproc
.LFE3217:
	.size	_ZN6icu_6718XLikelySubtagsData4loadER10UErrorCode, .-_ZN6icu_6718XLikelySubtagsData4loadER10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714XLikelySubtags17initLikelySubtagsER10UErrorCode
	.type	_ZN6icu_6714XLikelySubtags17initLikelySubtagsER10UErrorCode, @function
_ZN6icu_6714XLikelySubtags17initLikelySubtagsER10UErrorCode:
.LFB3220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-232(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$208, %rsp
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rcx
	movq	uhash_compareUChars_67@GOTPCREL(%rip), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	uhash_hashUChars_67@GOTPCREL(%rip), %rsi
	movb	$0, -144(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -152(%rbp)
	call	uhash_init_67@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L624
.L565:
	leaq	-240(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movl	$0, -104(%rbp)
	movq	%r13, %rdi
	movl	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -120(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN6icu_6718XLikelySubtagsData4loadER10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L625
.L567:
	movq	-240(%rbp), %rdi
	call	ures_close_67@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L584
	movq	-8(%rdi), %rax
	leaq	(%rax,%rax,2), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	cmpq	%rbx, %rdi
	je	.L585
	.p2align 4,,10
	.p2align 3
.L589:
	subq	$48, %rbx
	cmpq	$0, 24(%rbx)
	je	.L586
	movq	%rbx, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rdi, %rbx
	jne	.L589
.L585:
	subq	$8, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L584:
	movq	-80(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L590
	movq	-8(%rax), %rdx
	leaq	(%rdx,%rdx,2), %rbx
	salq	$4, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L591
	.p2align 4,,10
	.p2align 3
.L595:
	subq	$48, %rbx
	cmpq	$0, 24(%rbx)
	je	.L592
	movq	%rbx, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movq	-72(%rbp), %rax
	cmpq	%rbx, %rax
	jne	.L595
.L591:
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L590:
	movq	-128(%rbp), %rdi
	call	uhash_close_67@PLT
	movq	-136(%rbp), %rdi
	call	uhash_close_67@PLT
	movq	%r12, %rdi
	call	uhash_close_67@PLT
	movq	-152(%rbp), %r12
	testq	%r12, %r12
	je	.L564
	cmpb	$0, 12(%r12)
	jne	.L626
.L597:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L564:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L627
	addq	$208, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L586:
	cmpq	%rdi, %rbx
	jne	.L589
	subq	$8, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L592:
	cmpq	%rax, %rbx
	jne	.L595
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L568
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6714XLikelySubtagsC1ERNS_18XLikelySubtagsDataE
	leaq	_ZN6icu_6712_GLOBAL__N_17cleanupEv(%rip), %rsi
	movl	$8, %edi
	movq	%r14, _ZN6icu_6712_GLOBAL__N_114gLikelySubtagsE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movq	-240(%rbp), %rdi
	call	ures_close_67@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L569
	movq	-8(%rdi), %rax
	leaq	(%rax,%rax,2), %rbx
	salq	$4, %rbx
	addq	%rdi, %rbx
	cmpq	%rbx, %rdi
	je	.L570
	.p2align 4,,10
	.p2align 3
.L574:
	subq	$48, %rbx
	cmpq	$0, 24(%rbx)
	je	.L571
	movq	%rbx, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rdi, %rbx
	jne	.L574
.L570:
	subq	$8, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L569:
	movq	-80(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L590
	movq	-8(%rax), %rdx
	leaq	(%rdx,%rdx,2), %rbx
	salq	$4, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L591
.L580:
	subq	$48, %rbx
	cmpq	$0, 24(%rbx)
	je	.L577
.L628:
	movq	%rbx, %rdi
	call	_ZN6icu_673LSR11deleteOwnedEv@PLT
	movq	-72(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L591
	subq	$48, %rbx
	cmpq	$0, 24(%rbx)
	jne	.L628
.L577:
	cmpq	%rax, %rbx
	jne	.L580
	leaq	-8(%rbx), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L571:
	cmpq	%rdi, %rbx
	jne	.L574
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L624:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L566
	leaq	13(%rax), %rdx
	movl	$0, 56(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movl	$40, 8(%rax)
	movw	%dx, 12(%rax)
	movq	%rax, -152(%rbp)
	jmp	.L565
.L627:
	call	__stack_chk_fail@PLT
.L568:
	movq	$0, _ZN6icu_6712_GLOBAL__N_114gLikelySubtagsE(%rip)
	movl	$7, (%rbx)
	jmp	.L567
.L566:
	movq	$0, -152(%rbp)
	movl	$7, (%rbx)
	jmp	.L565
	.cfi_endproc
.LFE3220:
	.size	_ZN6icu_6714XLikelySubtags17initLikelySubtagsER10UErrorCode, .-_ZN6icu_6714XLikelySubtags17initLikelySubtagsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714XLikelySubtags12getSingletonER10UErrorCode
	.type	_ZN6icu_6714XLikelySubtags12getSingletonER10UErrorCode, @function
_ZN6icu_6714XLikelySubtags12getSingletonER10UErrorCode:
.LFB3221:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L640
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L645
.L631:
	movl	4+_ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L632
	movl	%eax, (%rbx)
.L632:
	movq	_ZN6icu_6712_GLOBAL__N_114gLikelySubtagsE(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	_ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L631
	movq	%rbx, %rdi
	call	_ZN6icu_6714XLikelySubtags17initLikelySubtagsER10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_6712_GLOBAL__N_19gInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L632
	.cfi_endproc
.LFE3221:
	.size	_ZN6icu_6714XLikelySubtags12getSingletonER10UErrorCode, .-_ZN6icu_6714XLikelySubtags12getSingletonER10UErrorCode
	.local	_ZN6icu_6712_GLOBAL__N_19gInitOnceE
	.comm	_ZN6icu_6712_GLOBAL__N_19gInitOnceE,8,8
	.local	_ZN6icu_6712_GLOBAL__N_114gLikelySubtagsE
	.comm	_ZN6icu_6712_GLOBAL__N_114gLikelySubtagsE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
