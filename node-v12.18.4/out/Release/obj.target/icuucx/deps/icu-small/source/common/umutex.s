	.file	"umutex.cpp"
	.text
	.section	.text._ZZSt9call_onceIRFvvEJEEvRSt9once_flagOT_DpOT0_ENUlvE0_4_FUNEv,"axG",@progbits,_ZZSt9call_onceIRFvvEJEEvRSt9once_flagOT_DpOT0_ENUlvE0_4_FUNEv,comdat
	.p2align 4
	.weak	_ZZSt9call_onceIRFvvEJEEvRSt9once_flagOT_DpOT0_ENUlvE0_4_FUNEv
	.type	_ZZSt9call_onceIRFvvEJEEvRSt9once_flagOT_DpOT0_ENUlvE0_4_FUNEv, @function
_ZZSt9call_onceIRFvvEJEEvRSt9once_flagOT_DpOT0_ENUlvE0_4_FUNEv:
.LFB3057:
	.cfi_startproc
	endbr64
	movq	_ZSt15__once_callable@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	jmp	*(%rax)
	.cfi_endproc
.LFE3057:
	.size	_ZZSt9call_onceIRFvvEJEEvRSt9once_flagOT_DpOT0_ENUlvE0_4_FUNEv, .-_ZZSt9call_onceIRFvvEJEEvRSt9once_flagOT_DpOT0_ENUlvE0_4_FUNEv
	.text
	.p2align 4
	.type	umtx_init, @function
umtx_init:
.LFB2735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	_ZZZ9umtx_initENKUlvE_clEvE7storage(%rip), %rax
	leaq	_ZZZ9umtx_initENKUlvE0_clEvE7storage(%rip), %rdi
	movq	%rax, _ZN6icu_6712_GLOBAL__N_19initMutexE(%rip)
	movq	$0, 32+_ZZZ9umtx_initENKUlvE_clEvE7storage(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movups	%xmm0, _ZZZ9umtx_initENKUlvE_clEvE7storage(%rip)
	movups	%xmm0, 16+_ZZZ9umtx_initENKUlvE_clEvE7storage(%rip)
	call	_ZNSt18condition_variableC1Ev@PLT
	leaq	_ZZZ9umtx_initENKUlvE0_clEvE7storage(%rip), %rax
	leaq	umtx_cleanup(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	$25, %edi
	movq	%rax, _ZN6icu_6712_GLOBAL__N_113initConditionE(%rip)
	jmp	ucln_common_registerCleanup_67@PLT
	.cfi_endproc
.LFE2735:
	.size	umtx_init, .-umtx_init
	.p2align 4
	.type	umtx_cleanup, @function
umtx_cleanup:
.LFB2734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN6icu_6712_GLOBAL__N_113initConditionE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt18condition_variableD1Ev@PLT
	movq	_ZN6icu_676UMutex9gListHeadE(%rip), %rax
	testq	%rax, %rax
	je	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movq	40(%rax), %rdx
	movq	%rax, %rdx
	movq	$0, 40(%rax)
	mfence
	movq	48(%rax), %rax
	movq	$0, 48(%rdx)
	testq	%rax, %rax
	jne	.L7
.L6:
	leaq	_ZN6icu_6712_GLOBAL__N_18initFlagE(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, _ZN6icu_676UMutex9gListHeadE(%rip)
	movq	%rax, _ZN6icu_6712_GLOBAL__N_19pInitFlagE(%rip)
	movl	$1, %eax
	movl	$0, _ZN6icu_6712_GLOBAL__N_18initFlagE(%rip)
	ret
	.cfi_endproc
.LFE2734:
	.size	umtx_cleanup, .-umtx_cleanup
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UMutex8getMutexEv
	.type	_ZN6icu_676UMutex8getMutexEv, @function
_ZN6icu_676UMutex8getMutexEv:
.LFB2748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L25
.L13:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	leaq	umtx_init(%rip), %rax
	leaq	-48(%rbp), %rdx
	movq	%rdi, %rbx
	movq	_ZN6icu_6712_GLOBAL__N_19pInitFlagE(%rip), %rdi
	movq	%rax, -48(%rbp)
	movq	_ZSt15__once_callable@gottpoff(%rip), %rax
	leaq	_ZZSt9call_onceIRFvvEJEEvRSt9once_flagOT_DpOT0_ENUlvE0_4_FUNEv(%rip), %rcx
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rdx, %fs:(%rax)
	movq	_ZSt11__once_call@gottpoff(%rip), %rax
	movq	%rcx, %fs:(%rax)
	je	.L22
	movq	__once_proxy@GOTPCREL(%rip), %rsi
	call	_ZL20__gthrw_pthread_oncePiPFvvE@PLT
	testl	%eax, %eax
	jne	.L24
	movq	_ZN6icu_6712_GLOBAL__N_19initMutexE(%rip), %r13
	movq	%r13, %rdi
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L24
	movq	40(%rbx), %rax
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L27
.L20:
	movq	%r13, %rdi
	call	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L27:
	movq	$0, 32(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movups	%xmm0, 16(%rbx)
	movq	%rbx, 40(%rbx)
	mfence
	movq	40(%rbx), %rax
	movq	%rax, %r12
	movq	_ZN6icu_676UMutex9gListHeadE(%rip), %rax
	movq	%rbx, _ZN6icu_676UMutex9gListHeadE(%rip)
	movq	%rax, 48(%rbx)
	jmp	.L20
.L22:
	movl	$-1, %eax
.L24:
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2748:
	.size	_ZN6icu_676UMutex8getMutexEv, .-_ZN6icu_676UMutex8getMutexEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UMutex7cleanupEv
	.type	_ZN6icu_676UMutex7cleanupEv, @function
_ZN6icu_676UMutex7cleanupEv:
.LFB2749:
	.cfi_startproc
	endbr64
	movq	_ZN6icu_676UMutex9gListHeadE(%rip), %rax
	testq	%rax, %rax
	je	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movq	40(%rax), %rdx
	movq	%rax, %rdx
	movq	$0, 40(%rax)
	mfence
	movq	48(%rax), %rax
	movq	$0, 48(%rdx)
	testq	%rax, %rax
	jne	.L30
.L29:
	movq	$0, _ZN6icu_676UMutex9gListHeadE(%rip)
	ret
	.cfi_endproc
.LFE2749:
	.size	_ZN6icu_676UMutex7cleanupEv, .-_ZN6icu_676UMutex7cleanupEv
	.p2align 4
	.globl	umtx_lock_67
	.type	umtx_lock_67, @function
umtx_lock_67:
.LFB2750:
	.cfi_startproc
	endbr64
	leaq	_ZN6icu_6712_GLOBAL__N_111globalMutexE(%rip), %rax
	testq	%rdi, %rdi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmove	%rax, %rdi
	movq	40(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rax, %rax
	je	.L37
.L47:
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, %rdi
	je	.L35
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L48
.L35:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	call	_ZN6icu_676UMutex8getMutexEv
	jmp	.L47
.L48:
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE2750:
	.size	umtx_lock_67, .-umtx_lock_67
	.p2align 4
	.globl	umtx_unlock_67
	.type	umtx_unlock_67, @function
umtx_unlock_67:
.LFB2751:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	leaq	_ZN6icu_6712_GLOBAL__N_111globalMutexE(%rip), %rax
	cmove	%rax, %rdi
	movq	40(%rdi), %rdi
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L49
	jmp	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	ret
	.cfi_endproc
.LFE2751:
	.size	umtx_unlock_67, .-umtx_unlock_67
	.p2align 4
	.globl	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE
	.type	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE, @function
_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE:
.LFB2752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZSt9call_onceIRFvvEJEEvRSt9once_flagOT_DpOT0_ENUlvE0_4_FUNEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-48(%rbp), %r12
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	_ZN6icu_6712_GLOBAL__N_19pInitFlagE(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	umtx_init(%rip), %rax
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, -48(%rbp)
	movq	_ZSt15__once_callable@gottpoff(%rip), %rax
	movq	%r12, %fs:(%rax)
	movq	_ZSt11__once_call@gottpoff(%rip), %rax
	movq	%rdx, %fs:(%rax)
	je	.L61
	movq	__once_proxy@GOTPCREL(%rip), %rsi
	call	_ZL20__gthrw_pthread_oncePiPFvvE@PLT
	testl	%eax, %eax
	jne	.L68
	movq	_ZN6icu_6712_GLOBAL__N_19initMutexE(%rip), %rdi
	movb	$0, -40(%rbp)
	movq	%rdi, -48(%rbp)
	testq	%rdi, %rdi
	je	.L69
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L68
	movb	$1, -40(%rbp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L57
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	movq	_ZN6icu_6712_GLOBAL__N_113initConditionE(%rip), %rdi
	movq	%r12, %rsi
	call	_ZNSt18condition_variable4waitERSt11unique_lockISt5mutexE@PLT
.L57:
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.L71
	xorl	%r12d, %r12d
	cmpb	$0, -40(%rbp)
	jne	.L72
.L52:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$32, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movl	$1, %r12d
	movl	$1, (%rbx)
	cmpb	$0, -40(%rbp)
	je	.L52
.L72:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
	jmp	.L52
.L61:
	movl	$-1, %eax
.L68:
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
.L69:
	movl	$1, %edi
	call	_ZSt20__throw_system_errori@PLT
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2752:
	.size	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE, .-_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE
	.p2align 4
	.globl	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE
	.type	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE, @function
_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE:
.LFB2753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	_ZN6icu_6712_GLOBAL__N_19initMutexE(%rip), %r13
	testq	%r13, %r13
	je	.L88
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L76
	movq	%r13, %rdi
	call	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t@PLT
	testl	%eax, %eax
	jne	.L89
.L76:
	movl	$2, (%rbx)
	testq	%r12, %r12
	je	.L77
	movq	%r13, %rdi
	call	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t@PLT
.L77:
	movq	_ZN6icu_6712_GLOBAL__N_113initConditionE(%rip), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt18condition_variable10notify_allEv@PLT
.L88:
	.cfi_restore_state
	movl	$1, %edi
	call	_ZSt20__throw_system_errori@PLT
.L89:
	movl	%eax, %edi
	call	_ZSt20__throw_system_errori@PLT
	.cfi_endproc
.LFE2753:
	.size	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE, .-_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE
	.p2align 4
	.globl	u_setMutexFunctions_67
	.type	u_setMutexFunctions_67, @function
u_setMutexFunctions_67:
.LFB2754:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jle	.L92
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$16, (%r9)
	ret
	.cfi_endproc
.LFE2754:
	.size	u_setMutexFunctions_67, .-u_setMutexFunctions_67
	.p2align 4
	.globl	u_setAtomicIncDecFunctions_67
	.type	u_setAtomicIncDecFunctions_67, @function
u_setAtomicIncDecFunctions_67:
.LFB2755:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L95
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$16, (%rcx)
	ret
	.cfi_endproc
.LFE2755:
	.size	u_setAtomicIncDecFunctions_67, .-u_setAtomicIncDecFunctions_67
	.globl	_ZN6icu_676UMutex9gListHeadE
	.bss
	.align 8
	.type	_ZN6icu_676UMutex9gListHeadE, @object
	.size	_ZN6icu_676UMutex9gListHeadE, 8
_ZN6icu_676UMutex9gListHeadE:
	.zero	8
	.local	_ZZZ9umtx_initENKUlvE0_clEvE7storage
	.comm	_ZZZ9umtx_initENKUlvE0_clEvE7storage,48,8
	.local	_ZZZ9umtx_initENKUlvE_clEvE7storage
	.comm	_ZZZ9umtx_initENKUlvE_clEvE7storage,40,8
	.section	.data.rel.local,"aw"
	.align 8
	.type	_ZN6icu_6712_GLOBAL__N_19pInitFlagE, @object
	.size	_ZN6icu_6712_GLOBAL__N_19pInitFlagE, 8
_ZN6icu_6712_GLOBAL__N_19pInitFlagE:
	.quad	_ZN6icu_6712_GLOBAL__N_18initFlagE
	.local	_ZN6icu_6712_GLOBAL__N_18initFlagE
	.comm	_ZN6icu_6712_GLOBAL__N_18initFlagE,4,4
	.local	_ZN6icu_6712_GLOBAL__N_111globalMutexE
	.comm	_ZN6icu_6712_GLOBAL__N_111globalMutexE,56,32
	.local	_ZN6icu_6712_GLOBAL__N_113initConditionE
	.comm	_ZN6icu_6712_GLOBAL__N_113initConditionE,8,8
	.local	_ZN6icu_6712_GLOBAL__N_19initMutexE
	.comm	_ZN6icu_6712_GLOBAL__N_19initMutexE,8,8
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.weakref	_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t,pthread_mutex_unlock
	.weakref	_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t,pthread_mutex_lock
	.weakref	_ZL20__gthrw_pthread_oncePiPFvvE,pthread_once
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
