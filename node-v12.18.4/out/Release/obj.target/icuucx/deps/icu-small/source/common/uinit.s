	.file	"uinit.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_67L13uinit_cleanupEv, @function
_ZN6icu_67L13uinit_cleanupEv:
.LFB2725:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	movl	$0, _ZN6icu_67L12gICUInitOnceE(%rip)
	mfence
	ret
	.cfi_endproc
.LFE2725:
	.size	_ZN6icu_67L13uinit_cleanupEv, .-_ZN6icu_67L13uinit_cleanupEv
	.p2align 4
	.globl	u_init_67
	.type	u_init_67, @function
u_init_67:
.LFB2727:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L18
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L12gICUInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L19
.L6:
	movl	4+_ZN6icu_67L12gICUInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L3
	movl	%eax, (%rbx)
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	leaq	_ZN6icu_67L12gICUInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L6
	movq	%rbx, %rdi
	call	ucnv_io_countKnownConverters_67@PLT
	movl	$22, %edi
	leaq	_ZN6icu_67L13uinit_cleanupEv(%rip), %rsi
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L12gICUInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12gICUInitOnceE(%rip)
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	.cfi_endproc
.LFE2727:
	.size	u_init_67, .-u_init_67
	.local	_ZN6icu_67L12gICUInitOnceE
	.comm	_ZN6icu_67L12gICUInitOnceE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
