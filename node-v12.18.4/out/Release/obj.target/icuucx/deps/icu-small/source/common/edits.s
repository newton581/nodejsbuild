	.file	"edits.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_675Edits12addUnchangedEi.part.0, @function
_ZN6icu_675Edits12addUnchangedEi.part.0:
.LFB2810:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	12(%rdi), %ebx
	movq	(%rdi), %r15
	testl	%ebx, %ebx
	jle	.L2
	movslq	%ebx, %rax
	leaq	-2(%r15,%rax,2), %rdx
	movzwl	(%rdx), %eax
	cmpl	$4094, %eax
	jle	.L31
.L2:
	cmpl	$4095, %r14d
	jle	.L7
.L6:
	leaq	28(%r12), %rax
	movl	%r14d, %ecx
	movq	%rax, -56(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L33:
	movslq	%ebx, %r13
	addq	%r13, %r13
.L9:
	addl	$1, %ebx
	movl	$4095, %eax
	movl	%ebx, 12(%r12)
	movw	%ax, (%r15,%r13)
.L12:
	subl	$4096, %ecx
	cmpl	$4095, %ecx
	jle	.L32
.L16:
	movl	12(%r12), %ebx
	movq	(%r12), %r15
.L17:
	movl	8(%r12), %eax
	cmpl	%ebx, %eax
	jg	.L33
	cmpq	%r15, -56(%rbp)
	je	.L25
	cmpl	$2147483647, %eax
	je	.L13
	leal	(%rax,%rax), %r9d
	cmpl	$1073741823, %eax
	movl	$2147483647, %edx
	cmovge	%edx, %r9d
.L10:
	movl	%r9d, %edx
	subl	%eax, %edx
	cmpl	$4, %edx
	jle	.L13
	movslq	%r9d, %rdi
	movl	%ecx, -68(%rbp)
	addq	%rdi, %rdi
	movl	%r9d, -64(%rbp)
	call	uprv_malloc_67@PLT
	movl	-64(%rbp), %r9d
	movl	-68(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L34
	movslq	12(%r12), %r13
	movq	(%r12), %r10
	movq	%rax, %rdi
	movl	%r9d, -72(%rbp)
	movl	%ecx, -68(%rbp)
	movq	%r13, %rbx
	addq	%r13, %r13
	movq	%r10, %rsi
	movq	%r10, -64(%rbp)
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %r10
	cmpq	%r10, -56(%rbp)
	movl	-68(%rbp), %ecx
	movl	-72(%rbp), %r9d
	je	.L15
	movq	%r10, %rdi
	movl	%r9d, -68(%rbp)
	movl	%ecx, -64(%rbp)
	call	uprv_free_67@PLT
	movslq	12(%r12), %r13
	movl	-68(%rbp), %r9d
	movl	-64(%rbp), %ecx
	movq	%r13, %rbx
	addq	%r13, %r13
.L15:
	movq	%r15, (%r12)
	movl	%r9d, 8(%r12)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$8, 24(%r12)
	subl	$4096, %ecx
	cmpl	$4095, %ecx
	jg	.L16
	.p2align 4,,10
	.p2align 3
.L32:
	leal	-4096(%r14), %eax
	movl	$0, %r8d
	movl	%eax, %esi
	andl	$-4096, %esi
	negl	%esi
	cmpl	$4095, %r14d
	cmovg	%esi, %r8d
	leal	(%r8,%rax), %r14d
.L7:
	testl	%r14d, %r14d
	jg	.L35
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	$2000, %r9d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$4095, %ecx
	subl	%eax, %ecx
	cmpl	%esi, %ecx
	jge	.L36
	movl	$4095, %esi
	subl	%ecx, %r14d
	movw	%si, (%rdx)
	cmpl	$4095, %r14d
	jg	.L6
	movl	8(%r12), %eax
	subl	$1, %r14d
	cmpl	%eax, %ebx
	jge	.L18
	.p2align 4,,10
	.p2align 3
.L37:
	movslq	%ebx, %r13
	addq	%r13, %r13
.L19:
	addl	$1, %ebx
	movl	%ebx, 12(%r12)
	movw	%r14w, (%r15,%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	12(%r12), %ebx
	movl	8(%r12), %eax
	subl	$1, %r14d
	movq	(%r12), %r15
	cmpl	%eax, %ebx
	jl	.L37
.L18:
	leaq	28(%r12), %r8
	cmpq	%r8, %r15
	je	.L27
	cmpl	$2147483647, %eax
	je	.L22
	leal	(%rax,%rax), %ecx
	cmpl	$1073741823, %eax
	movl	$2147483647, %edx
	cmovge	%edx, %ecx
.L20:
	movl	%ecx, %edi
	movq	%r8, -56(%rbp)
	subl	%eax, %edi
	cmpl	$4, %edi
	jle	.L22
	movslq	%ecx, %rdi
	movl	%ecx, -64(%rbp)
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L38
	movslq	12(%r12), %r13
	movq	(%r12), %r9
	movq	%rax, %rdi
	movl	%ecx, -68(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r13, %rbx
	addq	%r13, %r13
	movq	%r9, %rsi
	movq	%r9, -56(%rbp)
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movl	-68(%rbp), %ecx
	cmpq	%r9, %r8
	je	.L24
	movq	%r9, %rdi
	movl	%ecx, -56(%rbp)
	call	uprv_free_67@PLT
	movslq	12(%r12), %r13
	movl	-56(%rbp), %ecx
	movq	%r13, %rbx
	addq	%r13, %r13
.L24:
	movq	%r15, (%r12)
	movl	%ecx, 8(%r12)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L36:
	addl	%esi, %eax
	movw	%ax, (%rdx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	movl	$8, 24(%r12)
	jmp	.L1
.L27:
	movl	$2000, %ecx
	jmp	.L20
.L38:
	movl	$7, 24(%r12)
	jmp	.L1
.L34:
	movl	$7, 24(%r12)
	jmp	.L12
	.cfi_endproc
.LFE2810:
	.size	_ZN6icu_675Edits12addUnchangedEi.part.0, .-_ZN6icu_675Edits12addUnchangedEi.part.0
	.align 2
	.p2align 4
	.type	_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode.part.0, @function
_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode.part.0:
.LFB2816:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	24(%rdi), %esi
	movl	28(%rdi), %r8d
	testb	%dl, %dl
	je	.L40
	movl	32(%rdi), %eax
.L41:
	cmpl	%r15d, %eax
	jle	.L42
	sarl	%eax
	movl	(%rcx), %r13d
	cmpl	%eax, %r15d
	jl	.L43
	.p2align 4,,10
	.p2align 3
.L84:
	testl	%r13d, %r13d
	jg	.L44
	cmpb	$0, 22(%rdi)
	movl	16(%rdi), %eax
	movslq	8(%rdi), %rsi
	js	.L45
	je	.L46
	testl	%eax, %eax
	jg	.L152
	movl	24(%rdi), %edx
	addl	%edx, 32(%rdi)
	cmpb	$0, 23(%rdi)
	movl	28(%rdi), %edx
	je	.L48
	addl	%edx, 36(%rdi)
.L48:
	addl	%edx, 40(%rdi)
.L46:
	movb	$-1, 22(%rdi)
.L45:
	testl	%eax, %eax
	jle	.L49
	movq	(%rdi), %rcx
	movslq	%esi, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	andl	$511, %edx
	cmpl	%eax, %edx
	jge	.L153
	movl	$0, 16(%rdi)
.L49:
	testl	%esi, %esi
	jle	.L154
	leal	-1(%rsi), %ecx
	movq	(%rdi), %r9
	movl	32(%rdi), %ebx
	movslq	%ecx, %rax
	movl	%ecx, 8(%rdi)
	movl	40(%rdi), %r14d
	leaq	(%rax,%rax), %rdx
	movzwl	(%r9,%rax,2), %eax
	movl	%ebx, -44(%rbp)
	movl	%eax, %r10d
	cmpl	$4095, %eax
	jle	.L155
	movl	36(%rdi), %ebx
	movzbl	21(%rdi), %r11d
	movb	$1, 23(%rdi)
	movl	%ebx, -48(%rbp)
	cmpl	$28671, %eax
	jg	.L56
	movl	%r10d, %edx
	movl	%eax, %r8d
	sarl	$9, %eax
	andl	$511, %edx
	sarl	$12, %r8d
	andl	$7, %eax
	addl	$1, %edx
	testb	%r11b, %r11b
	je	.L57
	imull	%edx, %r8d
	imull	%eax, %edx
	movl	%r8d, 24(%rdi)
	movl	%edx, 28(%rdi)
.L73:
	testl	%ecx, %ecx
	jle	.L58
	movslq	%ecx, %rcx
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L156:
	movl	%eax, %r10d
	andl	$511, %esi
	sarl	$9, %eax
	addl	$1, %esi
	sarl	$12, %r10d
	andl	$7, %eax
	imull	%esi, %r10d
	imull	%esi, %eax
	addl	%r10d, %r8d
	addl	%eax, %edx
	movl	%r8d, 24(%rdi)
	movl	%edx, 28(%rdi)
.L76:
	subq	$1, %rcx
	testl	%ecx, %ecx
	jle	.L58
.L59:
	movzwl	-2(%r9,%rcx,2), %eax
	movl	%ecx, %r11d
	movl	%eax, %esi
	cmpl	$4095, %eax
	jle	.L58
	leal	-1(%rcx), %r10d
	movl	%r10d, 8(%rdi)
	cmpl	$28671, %eax
	jle	.L156
	cmpl	$32767, %eax
	jg	.L76
	sarl	$6, %eax
	andl	$63, %eax
	cmpl	$60, %eax
	jle	.L77
	movzwl	(%r9,%rcx,2), %ebx
	cmpl	$61, %eax
	jne	.L78
	movl	%ebx, %eax
	addl	$1, %r11d
	andl	$32767, %eax
.L77:
	addl	%eax, %r8d
	movl	%esi, %eax
	andl	$63, %esi
	andl	$63, %eax
	movl	%r8d, 24(%rdi)
	cmpw	$60, %ax
	jbe	.L79
	movslq	%r11d, %r11
	leaq	(%r11,%r11), %rbx
	movzwl	(%r9,%r11,2), %r11d
	cmpw	$61, %ax
	jne	.L80
	movl	%r11d, %esi
	andl	$32767, %esi
.L79:
	addl	%esi, %edx
	subq	$1, %rcx
	movl	%r10d, 8(%rdi)
	movl	%edx, 28(%rdi)
	testl	%ecx, %ecx
	jg	.L59
	.p2align 4,,10
	.p2align 3
.L58:
	movl	-44(%rbp), %eax
	subl	%r8d, %eax
	movl	%eax, 32(%rdi)
	movl	-48(%rbp), %eax
	subl	%edx, %eax
	movl	%eax, 36(%rdi)
	movl	%r14d, %eax
	subl	%edx, %eax
	movl	%eax, 40(%rdi)
	.p2align 4,,10
	.p2align 3
.L44:
	testb	%r12b, %r12b
	je	.L81
	movl	32(%rdi), %eax
	cmpl	%eax, %r15d
	jge	.L83
.L158:
	movl	16(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L84
	movslq	8(%rdi), %r9
	movq	(%rdi), %rdx
	testb	%r12b, %r12b
	movl	28(%rdi), %esi
	movl	24(%rdi), %r8d
	movzwl	(%rdx,%r9,2), %edx
	movl	40(%rdi), %r14d
	movl	%esi, %ebx
	movl	32(%rdi), %r11d
	movl	36(%rdi), %r10d
	movl	%edx, %r9d
	cmovne	%r8d, %ebx
	movl	%r14d, -44(%rbp)
	andl	$511, %r9d
	leal	1(%r9), %edx
	movl	%eax, %r9d
	subl	%ecx, %edx
	movl	%edx, %r14d
	imull	%ebx, %r14d
	subl	%r14d, %r9d
	cmpl	%r9d, %r15d
	jge	.L157
	imull	%edx, %r8d
	movl	-44(%rbp), %r9d
	movl	$0, 16(%rdi)
	imull	%esi, %edx
	subl	%r8d, %r11d
	subl	%edx, %r10d
	subl	%edx, %r9d
	movl	%r11d, 32(%rdi)
	movl	%r10d, 36(%rdi)
	movl	%r9d, 40(%rdi)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L81:
	movl	40(%rdi), %eax
	cmpl	%eax, %r15d
	jl	.L158
	.p2align 4,,10
	.p2align 3
.L83:
	xorl	%eax, %eax
.L39:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	$0, 24(%rdi)
	movw	%dx, 22(%rdi)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L42:
	addl	%esi, %eax
	cmpl	%eax, %r15d
	jl	.L83
	movl	(%rcx), %r13d
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L160:
	movl	32(%rdi), %eax
	movl	%ecx, %r8d
.L117:
	leal	(%rax,%r8), %edx
	cmpl	%edx, %r15d
	jl	.L83
	movl	16(%rdi), %esi
	cmpl	$1, %esi
	jg	.L159
.L118:
	testl	%r13d, %r13d
	jg	.L121
.L162:
	cmpb	$0, 22(%rdi)
	movl	16(%rdi), %eax
	jle	.L91
	movl	24(%rdi), %edx
	addl	%edx, 32(%rdi)
	cmpb	$0, 23(%rdi)
	movl	28(%rdi), %edx
	je	.L92
	addl	%edx, 36(%rdi)
.L92:
	addl	%edx, 40(%rdi)
.L93:
	testl	%eax, %eax
	jle	.L96
	cmpl	$1, %eax
	je	.L97
	subl	$1, %eax
	movl	24(%rdi), %ecx
	movl	28(%rdi), %r9d
	movl	%eax, 16(%rdi)
.L95:
	testb	%r12b, %r12b
	jne	.L160
	movl	40(%rdi), %eax
	movl	%r9d, %r8d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L40:
	movl	40(%rdi), %eax
	movl	%r8d, %esi
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L56:
	leal	-2(%rsi), %ebx
	movslq	%ebx, %rbx
	cmpl	$32767, %eax
	jle	.L161
	.p2align 4,,10
	.p2align 3
.L67:
	movzwl	(%r9,%rbx,2), %r8d
	leal	1(%rbx), %eax
	movl	%ebx, %ecx
	subq	$1, %rbx
	movl	%r8d, %edx
	cmpl	$32767, %r8d
	jg	.L67
	sarl	$6, %r8d
	andl	$63, %r8d
	cmpl	$60, %r8d
	jle	.L68
	cltq
	movzwl	(%r9,%rax,2), %esi
	leaq	(%rax,%rax), %r10
	cmpl	$61, %r8d
	jne	.L69
	movl	%esi, %r8d
	leal	2(%rcx), %eax
	andl	$32767, %r8d
.L68:
	movl	%edx, %esi
	movl	%r8d, 24(%rdi)
	andl	$63, %edx
	andl	$63, %esi
	cmpw	$60, %si
	jbe	.L70
	cltq
	leaq	(%rax,%rax), %r10
	movzwl	(%r9,%rax,2), %eax
	cmpw	$61, %si
	jne	.L71
	movl	%eax, %edx
	andl	$32767, %edx
.L70:
	movl	%edx, 28(%rdi)
	movl	%ecx, 8(%rdi)
.L66:
	testb	%r11b, %r11b
	je	.L58
	movl	8(%rdi), %ecx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L152:
	subl	$1, %esi
	movb	$-1, 22(%rdi)
	movl	%esi, 8(%rdi)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L43:
	pxor	%xmm0, %xmm0
	movb	$0, 22(%rdi)
	movl	$0, 40(%rdi)
	movl	$0, 16(%rdi)
	movl	$0, 8(%rdi)
	movups	%xmm0, 24(%rdi)
	testl	%r13d, %r13d
	jle	.L162
.L121:
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movl	$0, 16(%rdi)
.L96:
	movl	8(%rdi), %edx
	movl	12(%rdi), %r10d
	cmpl	%r10d, %edx
	jl	.L98
	xorl	%eax, %eax
	movq	$0, 24(%rdi)
	movw	%ax, 22(%rdi)
	movl	$1, %eax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L91:
	je	.L94
	testl	%eax, %eax
	jg	.L163
.L94:
	movb	$1, 22(%rdi)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L98:
	movq	(%rdi), %r8
	movslq	%edx, %r11
	leal	1(%rdx), %esi
	movl	%esi, 8(%rdi)
	leaq	(%r11,%r11), %rbx
	movzwl	(%r8,%r11,2), %r9d
	movl	%r9d, %eax
	cmpl	$4095, %r9d
	jg	.L99
	leal	1(%r9), %ecx
	movb	$0, 23(%rdi)
	movl	%ecx, 24(%rdi)
	cmpl	%esi, %r10d
	jle	.L100
	subl	$2, %r10d
	leal	2(%rdx), %eax
	subl	%edx, %r10d
	cltq
	leaq	3(%r11,%r10), %rsi
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L164:
	movl	%eax, 8(%rdi)
	leal	1(%rdx,%rcx), %ecx
	addq	$1, %rax
	movl	%ecx, 24(%rdi)
	cmpq	%rax, %rsi
	je	.L100
.L101:
	movzwl	-2(%r8,%rax,2), %edx
	cmpl	$4095, %edx
	jle	.L164
.L100:
	movl	%ecx, 28(%rdi)
	movl	%ecx, %r9d
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L99:
	movb	$1, 23(%rdi)
	movzbl	21(%rdi), %r11d
	cmpl	$28671, %r9d
	jg	.L102
	movl	%r9d, %ecx
	andl	$511, %eax
	sarl	$9, %r9d
	sarl	$12, %ecx
	andl	$7, %r9d
	addl	$1, %eax
	testb	%r11b, %r11b
	je	.L103
	imull	%eax, %ecx
	imull	%eax, %r9d
	movl	%ecx, 24(%rdi)
	movl	%r9d, 28(%rdi)
.L109:
	cmpl	%r10d, %esi
	jl	.L104
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L165:
	movl	%eax, %esi
	andl	$511, %edx
	sarl	$9, %eax
	addl	$1, %edx
	sarl	$12, %esi
	andl	$7, %eax
	imull	%edx, %esi
	imull	%edx, %eax
	addl	%esi, %ecx
	movl	%r11d, %esi
	addl	%eax, %r9d
	movl	%ecx, 24(%rdi)
	movl	%r9d, 28(%rdi)
.L111:
	cmpl	%esi, %r10d
	jle	.L95
.L104:
	movslq	%esi, %rax
	leaq	(%rax,%rax), %rbx
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %edx
	cmpl	$4095, %eax
	jle	.L95
	leal	1(%rsi), %r11d
	movl	%r11d, 8(%rdi)
	cmpl	$28671, %eax
	jle	.L165
	sarl	$6, %eax
	andl	$63, %eax
	cmpl	$60, %eax
	jle	.L122
	movzwl	2(%r8,%rbx), %r11d
	cmpl	$61, %eax
	jne	.L113
	addl	$2, %esi
	movl	%r11d, %eax
	movl	%esi, 8(%rdi)
	andl	$32767, %eax
.L112:
	addl	%eax, %ecx
	movl	%edx, %eax
	andl	$63, %edx
	andl	$63, %eax
	movl	%ecx, 24(%rdi)
	cmpw	$60, %ax
	jbe	.L114
	movslq	%esi, %r11
	leaq	(%r11,%r11), %rbx
	movzwl	(%r8,%r11,2), %r11d
	cmpw	$61, %ax
	jne	.L115
	addl	$1, %esi
	movl	%r11d, %edx
	movl	%esi, 8(%rdi)
	andl	$32767, %edx
.L114:
	addl	%edx, %r9d
	movl	%r9d, 28(%rdi)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L122:
	movl	%r11d, %esi
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L113:
	movzwl	4(%r8,%rbx), %ebx
	sall	$15, %r11d
	sall	$30, %eax
	addl	$3, %esi
	andl	$1073709056, %r11d
	andl	$1073741824, %eax
	movl	%esi, 8(%rdi)
	andl	$32767, %ebx
	orl	%ebx, %r11d
	orl	%r11d, %eax
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L115:
	movzwl	2(%r8,%rbx), %eax
	sall	$15, %r11d
	sall	$30, %edx
	addl	$2, %esi
	andl	$1073709056, %r11d
	andl	$1073741824, %edx
	movl	%esi, 8(%rdi)
	andl	$32767, %eax
	orl	%eax, %r11d
	orl	%r11d, %edx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L153:
	addl	$1, %eax
	movl	%eax, 16(%rdi)
	movl	24(%rdi), %eax
	subl	%eax, 32(%rdi)
	cmpb	$0, 23(%rdi)
	movl	28(%rdi), %eax
	je	.L51
	subl	%eax, 36(%rdi)
.L51:
	subl	%eax, 40(%rdi)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L163:
	addl	$1, 8(%rdi)
	movl	24(%rdi), %ecx
	movb	$1, 22(%rdi)
	movl	28(%rdi), %r9d
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L155:
	addl	$1, %eax
	movb	$0, 23(%rdi)
	movl	%eax, 24(%rdi)
	testl	%ecx, %ecx
	je	.L54
	leal	-2(%rsi), %ecx
	subq	$3, %rsi
	movslq	%ecx, %rdx
	movl	%ecx, %ecx
	subq	%rcx, %rsi
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L166:
	movl	%edx, 8(%rdi)
	leal	1(%rcx,%rax), %eax
	subq	$1, %rdx
	movl	%eax, 24(%rdi)
	cmpq	%rdx, %rsi
	je	.L54
.L55:
	movzwl	(%r9,%rdx,2), %ecx
	cmpl	$4095, %ecx
	jle	.L166
.L54:
	movl	-44(%rbp), %edx
	subl	%eax, %r14d
	movl	%eax, 28(%rdi)
	movl	%r14d, 40(%rdi)
	subl	%eax, %edx
	movl	%edx, 32(%rdi)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L102:
	sarl	$6, %r9d
	movl	%r9d, %ecx
	andl	$63, %ecx
	cmpl	$60, %ecx
	jle	.L105
	movzwl	2(%r8,%rbx), %r9d
	cmpl	$61, %ecx
	jne	.L106
	leal	2(%rdx), %esi
	movl	%r9d, %ecx
	movl	%esi, 8(%rdi)
	andl	$32767, %ecx
.L105:
	movl	%eax, %edx
	movl	%eax, %r9d
	movl	%ecx, 24(%rdi)
	andl	$63, %edx
	andl	$63, %r9d
	cmpw	$60, %dx
	jbe	.L107
	movslq	%esi, %rax
	leaq	(%rax,%rax), %rbx
	movzwl	(%r8,%rax,2), %eax
	cmpw	$61, %dx
	jne	.L108
	addl	$1, %esi
	movl	%eax, %r9d
	movl	%esi, 8(%rdi)
	andl	$32767, %r9d
.L107:
	movl	%r9d, 28(%rdi)
	testb	%r11b, %r11b
	jne	.L109
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L159:
	movl	%r8d, %edx
	imull	%esi, %edx
	addl	%eax, %edx
	cmpl	%edx, %r15d
	jl	.L167
	imull	%esi, %ecx
	movl	$0, 16(%rdi)
	imull	%esi, %r9d
	movl	%ecx, 24(%rdi)
	movl	%r9d, 28(%rdi)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L103:
	movl	%ecx, 24(%rdi)
	movl	%r9d, 28(%rdi)
	cmpl	$1, %eax
	je	.L95
	movl	%eax, 16(%rdi)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L78:
	sall	$15, %ebx
	sall	$30, %eax
	addl	$2, %r11d
	andl	$1073709056, %ebx
	andl	$1073741824, %eax
	movl	%ebx, -52(%rbp)
	movzwl	2(%r9,%rcx,2), %ebx
	andl	$32767, %ebx
	orl	-52(%rbp), %ebx
	orl	%ebx, %eax
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L80:
	movzwl	2(%r9,%rbx), %eax
	sall	$15, %r11d
	sall	$30, %esi
	andl	$1073709056, %r11d
	andl	$1073741824, %esi
	andl	$32767, %eax
	orl	%eax, %r11d
	orl	%r11d, %esi
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L108:
	movzwl	2(%r8,%rbx), %edx
	sall	$15, %eax
	sall	$30, %r9d
	addl	$2, %esi
	andl	$1073709056, %eax
	andl	$1073741824, %r9d
	movl	%esi, 8(%rdi)
	andl	$32767, %edx
	orl	%edx, %eax
	orl	%eax, %r9d
	movl	%r9d, 28(%rdi)
	testb	%r11b, %r11b
	jne	.L109
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L106:
	movzwl	4(%r8,%rbx), %esi
	sall	$15, %r9d
	sall	$30, %ecx
	andl	$1073709056, %r9d
	andl	$1073741824, %ecx
	andl	$32767, %esi
	orl	%esi, %r9d
	leal	3(%rdx), %esi
	movl	%esi, 8(%rdi)
	orl	%r9d, %ecx
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%r8d, 24(%rdi)
	movl	%eax, 28(%rdi)
	cmpl	$1, %edx
	je	.L60
	movl	$1, 16(%rdi)
.L60:
	movl	-44(%rbp), %edx
	subl	%eax, %r14d
	movl	%r14d, 40(%rdi)
	subl	%r8d, %edx
	movl	%edx, 32(%rdi)
	movl	-48(%rbp), %edx
	subl	%eax, %edx
	movl	%edx, 36(%rdi)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L161:
	movl	%eax, %r8d
	sarl	$6, %r8d
	andl	$63, %r8d
	cmpl	$60, %r8d
	jle	.L62
	cmpl	$61, %r8d
	jne	.L63
	movl	%esi, 8(%rdi)
	movl	%eax, %r8d
	movl	%esi, %ecx
	andl	$32767, %r8d
.L62:
	movl	%r10d, %esi
	movl	%r10d, %edx
	movl	%r8d, 24(%rdi)
	andl	$63, %esi
	andl	$63, %edx
	cmpw	$60, %si
	jbe	.L64
	movslq	%ecx, %rax
	leaq	(%rax,%rax), %r10
	movzwl	(%r9,%rax,2), %eax
	cmpw	$61, %si
	jne	.L65
	addl	$1, %ecx
	movl	%eax, %edx
	movl	%ecx, 8(%rdi)
	andl	$32767, %edx
.L64:
	movl	%edx, 28(%rdi)
	jmp	.L66
.L71:
	movzwl	2(%r9,%r10), %esi
	sall	$15, %eax
	sall	$30, %edx
	andl	$1073709056, %eax
	andl	$1073741824, %edx
	andl	$32767, %esi
	orl	%esi, %eax
	orl	%eax, %edx
	jmp	.L70
.L69:
	movzwl	2(%r9,%r10), %eax
	sall	$30, %r8d
	andl	$1073741824, %r8d
	movl	%eax, %r10d
	movl	%esi, %eax
	sall	$15, %eax
	andl	$32767, %r10d
	andl	$1073709056, %eax
	orl	%r10d, %eax
	orl	%eax, %r8d
	leal	3(%rcx), %eax
	jmp	.L68
.L65:
	movzwl	2(%r9,%r10), %esi
	sall	$15, %eax
	sall	$30, %edx
	addl	$2, %ecx
	andl	$1073709056, %eax
	andl	$1073741824, %edx
	movl	%ecx, 8(%rdi)
	andl	$32767, %esi
	orl	%esi, %eax
	orl	%eax, %edx
	jmp	.L64
.L63:
	movzwl	2(%r9,%rdx), %edx
	sall	$15, %eax
	sall	$30, %r8d
	leal	1(%rsi), %ecx
	andl	$1073741824, %r8d
	movl	%ecx, 8(%rdi)
	andl	$32767, %edx
	orl	%edx, %eax
	orl	%eax, %r8d
	jmp	.L62
.L157:
	subl	%r15d, %eax
	movl	-44(%rbp), %r9d
	subl	$1, %eax
	cltd
	idivl	%ebx
	addl	$1, %eax
	imull	%eax, %esi
	imull	%eax, %r8d
	addl	%ecx, %eax
	movl	%eax, 16(%rdi)
	xorl	%eax, %eax
	subl	%esi, %r10d
	subl	%esi, %r9d
	subl	%r8d, %r11d
	movl	%r10d, 36(%rdi)
	movl	%r11d, 32(%rdi)
	movl	%r9d, 40(%rdi)
	jmp	.L39
.L167:
	subl	%eax, %r15d
	movl	%r15d, %eax
	cltd
	idivl	%r8d
	movl	%r9d, %edx
	subl	%eax, %esi
	imull	%eax, %edx
	addl	%edx, 36(%rdi)
	imull	%eax, %ecx
	addl	%edx, 40(%rdi)
	xorl	%eax, %eax
	addl	%ecx, 32(%rdi)
	movl	%esi, 16(%rdi)
	jmp	.L39
	.cfi_endproc
.LFE2816:
	.size	_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode.part.0, .-_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits12releaseArrayEv
	.type	_ZN6icu_675Edits12releaseArrayEv, @function
_ZN6icu_675Edits12releaseArrayEv:
.LFB2311:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	addq	$28, %rdi
	cmpq	%rdi, %r8
	je	.L168
	movq	%r8, %rdi
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	ret
	.cfi_endproc
.LFE2311:
	.size	_ZN6icu_675Edits12releaseArrayEv, .-_ZN6icu_675Edits12releaseArrayEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits9copyArrayERKS0_
	.type	_ZN6icu_675Edits9copyArrayERKS0_, @function
_ZN6icu_675Edits9copyArrayERKS0_:
.LFB2312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jg	.L177
	movslq	12(%rdi), %rdx
	movq	%rsi, %rbx
	cmpl	8(%rdi), %edx
	jg	.L178
	testl	%edx, %edx
	jg	.L179
.L172:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	(%r12), %rdi
	movq	(%rbx), %rsi
	addq	%rdx, %rdx
	call	memcpy@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movl	$0, 12(%rdi)
	movq	%r12, %rax
	movq	$0, 16(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	leaq	(%rdx,%rdx), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L180
	movq	(%r12), %rdi
	leaq	28(%r12), %rax
	cmpq	%rax, %rdi
	je	.L175
	call	uprv_free_67@PLT
.L175:
	movslq	12(%r12), %rdx
	movq	%r13, (%r12)
	movl	%edx, 8(%r12)
	testl	%edx, %edx
	jle	.L172
	jmp	.L179
.L180:
	movdqa	.LC0(%rip), %xmm0
	movups	%xmm0, 12(%r12)
	jmp	.L172
	.cfi_endproc
.LFE2312:
	.size	_ZN6icu_675Edits9copyArrayERKS0_, .-_ZN6icu_675Edits9copyArrayERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits9moveArrayERS0_
	.type	_ZN6icu_675Edits9moveArrayERS0_, @function
_ZN6icu_675Edits9moveArrayERS0_:
.LFB2313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jg	.L194
	movq	(%rdi), %rdi
	leaq	28(%r12), %r13
	movq	%rsi, %rbx
	cmpq	%r13, %rdi
	je	.L184
	call	uprv_free_67@PLT
.L184:
	movslq	12(%r12), %rax
	cmpl	$100, %eax
	jg	.L195
	movq	%r13, (%r12)
	movl	$100, 8(%r12)
	testl	%eax, %eax
	jg	.L196
.L183:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movl	$0, 12(%rdi)
	movq	%r12, %rax
	movq	$0, 16(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	(%rbx), %rax
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, (%r12)
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	leaq	28(%rbx), %rax
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$0, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	addq	%rax, %rax
	movq	(%rbx), %rsi
	cmpl	$8, %eax
	jnb	.L186
	testb	$4, %al
	jne	.L197
	testl	%eax, %eax
	je	.L183
	movzbl	(%rsi), %edx
	movb	%dl, 28(%r12)
	testb	$2, %al
	je	.L183
	movl	%eax, %edx
	movzwl	-2(%rsi,%rdx), %eax
	movw	%ax, -2(%r13,%rdx)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L186:
	movq	(%rsi), %rdx
	leaq	8(%r13), %rdi
	andq	$-8, %rdi
	movq	%rdx, 28(%r12)
	movl	%eax, %edx
	movq	-8(%rsi,%rdx), %rcx
	movq	%rcx, -8(%r13,%rdx)
	subq	%rdi, %r13
	leal	(%rax,%r13), %ecx
	subq	%r13, %rsi
	movl	%ecx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L183
.L197:
	movl	(%rsi), %edx
	movl	%edx, 28(%r12)
	movl	%eax, %edx
	movl	-4(%rsi,%rdx), %eax
	movl	%eax, -4(%r13,%rdx)
	jmp	.L183
	.cfi_endproc
.LFE2313:
	.size	_ZN6icu_675Edits9moveArrayERS0_, .-_ZN6icu_675Edits9moveArrayERS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_675EditsaSERKS0_
	.type	_ZN6icu_675EditsaSERKS0_, @function
_ZN6icu_675EditsaSERKS0_:
.LFB2314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	24(%rsi), %eax
	movdqu	12(%rsi), %xmm1
	movslq	12(%rsi), %rdx
	movups	%xmm1, 12(%rdi)
	testl	%eax, %eax
	jg	.L205
	movq	%rsi, %rbx
	cmpl	8(%rdi), %edx
	jg	.L206
	testl	%edx, %edx
	jg	.L207
.L200:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	(%r12), %rdi
	movq	(%rbx), %rsi
	addq	%rdx, %rdx
	call	memcpy@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movl	$0, 12(%rdi)
	movq	%r12, %rax
	movq	$0, 16(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	leaq	(%rdx,%rdx), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L208
	movq	(%r12), %rdi
	leaq	28(%r12), %rax
	cmpq	%rax, %rdi
	je	.L203
	call	uprv_free_67@PLT
.L203:
	movslq	12(%r12), %rdx
	movq	%r13, (%r12)
	movl	%edx, 8(%r12)
	testl	%edx, %edx
	jle	.L200
	jmp	.L207
.L208:
	movdqa	.LC0(%rip), %xmm0
	movups	%xmm0, 12(%r12)
	jmp	.L200
	.cfi_endproc
.LFE2314:
	.size	_ZN6icu_675EditsaSERKS0_, .-_ZN6icu_675EditsaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_675EditsaSEOS0_
	.type	_ZN6icu_675EditsaSEOS0_, @function
_ZN6icu_675EditsaSEOS0_:
.LFB2315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	24(%rsi), %edx
	movdqu	12(%rsi), %xmm1
	movslq	12(%rsi), %rax
	movups	%xmm1, 12(%rdi)
	testl	%edx, %edx
	jg	.L222
	movq	(%rdi), %rdi
	leaq	28(%r12), %r13
	movq	%rsi, %rbx
	cmpq	%r13, %rdi
	je	.L212
	call	uprv_free_67@PLT
	movslq	12(%r12), %rax
.L212:
	cmpl	$100, %eax
	jg	.L223
	movq	%r13, (%r12)
	movl	$100, 8(%r12)
	testl	%eax, %eax
	jg	.L224
.L211:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movl	$0, 12(%rdi)
	movq	%r12, %rax
	movq	$0, 16(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movq	(%rbx), %rax
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, (%r12)
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	leaq	28(%rbx), %rax
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$0, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	addq	%rax, %rax
	movq	(%rbx), %rsi
	cmpl	$8, %eax
	jnb	.L214
	testb	$4, %al
	jne	.L225
	testl	%eax, %eax
	je	.L211
	movzbl	(%rsi), %edx
	movb	%dl, 28(%r12)
	testb	$2, %al
	je	.L211
	movl	%eax, %edx
	movzwl	-2(%rsi,%rdx), %eax
	movw	%ax, -2(%r13,%rdx)
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L214:
	movq	(%rsi), %rdx
	leaq	8(%r13), %rdi
	andq	$-8, %rdi
	movq	%rdx, 28(%r12)
	movl	%eax, %edx
	movq	-8(%rsi,%rdx), %rcx
	movq	%rcx, -8(%r13,%rdx)
	subq	%rdi, %r13
	leal	(%rax,%r13), %ecx
	subq	%r13, %rsi
	movl	%ecx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L211
.L225:
	movl	(%rsi), %edx
	movl	%edx, 28(%r12)
	movl	%eax, %edx
	movl	-4(%rsi,%rdx), %eax
	movl	%eax, -4(%r13,%rdx)
	jmp	.L211
	.cfi_endproc
.LFE2315:
	.size	_ZN6icu_675EditsaSEOS0_, .-_ZN6icu_675EditsaSEOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_675EditsD2Ev
	.type	_ZN6icu_675EditsD2Ev, @function
_ZN6icu_675EditsD2Ev:
.LFB2317:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	addq	$28, %rdi
	cmpq	%rdi, %r8
	je	.L226
	movq	%r8, %rdi
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L226:
	ret
	.cfi_endproc
.LFE2317:
	.size	_ZN6icu_675EditsD2Ev, .-_ZN6icu_675EditsD2Ev
	.globl	_ZN6icu_675EditsD1Ev
	.set	_ZN6icu_675EditsD1Ev,_ZN6icu_675EditsD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits5resetEv
	.type	_ZN6icu_675Edits5resetEv, @function
_ZN6icu_675Edits5resetEv:
.LFB2319:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2319:
	.size	_ZN6icu_675Edits5resetEv, .-_ZN6icu_675Edits5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits12addUnchangedEi
	.type	_ZN6icu_675Edits12addUnchangedEi, @function
_ZN6icu_675Edits12addUnchangedEi:
.LFB2320:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jg	.L229
	testl	%esi, %esi
	je	.L229
	js	.L232
	jmp	_ZN6icu_675Edits12addUnchangedEi.part.0
	.p2align 4,,10
	.p2align 3
.L229:
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	movl	$1, 24(%rdi)
	ret
	.cfi_endproc
.LFE2320:
	.size	_ZN6icu_675Edits12addUnchangedEi, .-_ZN6icu_675Edits12addUnchangedEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits6appendEi
	.type	_ZN6icu_675Edits6appendEi, @function
_ZN6icu_675Edits6appendEi:
.LFB2322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	12(%rdi), %r12d
	movl	8(%rdi), %eax
	movq	(%rdi), %r14
	cmpl	%eax, %r12d
	jge	.L234
	movslq	%r12d, %r13
	addq	%r13, %r13
.L235:
	addl	$1, %r12d
	movl	%r12d, 12(%rbx)
	movw	%r15w, (%r14,%r13)
.L233:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	leaq	28(%rdi), %r8
	cmpq	%r14, %r8
	je	.L242
	cmpl	$2147483647, %eax
	je	.L239
	leal	(%rax,%rax), %ecx
	cmpl	$1073741823, %eax
	movl	$2147483647, %edx
	cmovge	%edx, %ecx
.L236:
	movl	%ecx, %edx
	movq	%r8, -56(%rbp)
	subl	%eax, %edx
	cmpl	$4, %edx
	jle	.L239
	movslq	%ecx, %rdi
	movl	%ecx, -64(%rbp)
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L245
	movslq	12(%rbx), %r13
	movq	(%rbx), %r9
	movq	%rax, %rdi
	movl	%ecx, -68(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r13, %r12
	addq	%r13, %r13
	movq	%r9, %rsi
	movq	%r9, -56(%rbp)
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movl	-68(%rbp), %ecx
	cmpq	%r9, %r8
	je	.L241
	movq	%r9, %rdi
	movl	%ecx, -56(%rbp)
	call	uprv_free_67@PLT
	movslq	12(%rbx), %r13
	movl	-56(%rbp), %ecx
	movq	%r13, %r12
	addq	%r13, %r13
.L241:
	movq	%r14, (%rbx)
	movl	%ecx, 8(%rbx)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L239:
	movl	$8, 24(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movl	$2000, %ecx
	jmp	.L236
.L245:
	movl	$7, 24(%rbx)
	jmp	.L233
	.cfi_endproc
.LFE2322:
	.size	_ZN6icu_675Edits6appendEi, .-_ZN6icu_675Edits6appendEi
	.align 2
	.p2align 4
	.type	_ZN6icu_675Edits10addReplaceEii.part.0, @function
_ZN6icu_675Edits10addReplaceEii.part.0:
.LFB2811:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	subl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	addl	$1, 20(%rdi)
	testl	%eax, %eax
	je	.L247
	movl	16(%rdi), %edx
	jle	.L248
	testl	%edx, %edx
	js	.L249
	movl	$2147483647, %ecx
	subl	%edx, %ecx
	cmpl	%ecx, %eax
	jle	.L249
.L250:
	movl	$8, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	testl	%edx, %edx
	jns	.L249
	movl	$-2147483648, %ecx
	subl	%edx, %ecx
	cmpl	%ecx, %eax
	jl	.L250
	.p2align 4,,10
	.p2align 3
.L249:
	addl	%edx, %eax
	movl	%eax, 16(%r12)
.L247:
	leal	-1(%rbx), %eax
	cmpl	$5, %eax
	ja	.L252
	cmpl	$7, %r13d
	jle	.L292
.L252:
	cmpl	$60, %ebx
	jg	.L254
	cmpl	$60, %r13d
	jg	.L254
	sall	$6, %ebx
	movl	%ebx, %edx
	orl	%r13d, %edx
	movl	%edx, %esi
	orl	$28672, %esi
.L291:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_675Edits6appendEi
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movl	8(%r12), %eax
	movl	12(%r12), %ecx
	movq	(%r12), %r14
	movl	%eax, %edx
	subl	%ecx, %edx
	cmpl	$4, %edx
	jle	.L255
	movslq	%ecx, %r15
	addq	%r15, %r15
.L256:
	leal	1(%rcx), %eax
	movslq	%eax, %rdx
	addq	%rdx, %rdx
	leaq	(%r14,%rdx), %rdi
	cmpl	$60, %ebx
	jle	.L293
	movl	%ebx, %r8d
	leal	2(%rcx), %eax
	orw	$-32768, %r8w
	cmpl	$32767, %ebx
	jg	.L262
	movw	%r8w, (%rdi)
	movl	$32576, %esi
.L263:
	cmpl	$60, %r13d
	jg	.L264
	movl	%r13d, %ebx
	orl	%esi, %ebx
.L265:
	movw	%bx, (%r14,%r15)
	movl	%eax, 12(%r12)
.L246:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movl	%ebx, %esi
	movl	%r13d, %edx
	movslq	12(%r12), %rax
	sall	$12, %esi
	sall	$9, %edx
	orl	%edx, %esi
	testl	%eax, %eax
	jle	.L291
	movq	(%r12), %rdx
	leaq	-2(%rdx,%rax,2), %rcx
	movzwl	(%rcx), %eax
	leal	-4096(%rax), %edi
	movl	%eax, %edx
	cmpl	$24574, %edi
	ja	.L291
	andl	$-512, %eax
	cmpl	%eax, %esi
	jne	.L291
	movl	%edx, %eax
	andw	$511, %ax
	cmpw	$511, %ax
	je	.L291
	addl	$1, %edx
	movw	%dx, (%rcx)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L293:
	sall	$6, %ebx
	orb	$112, %bh
	movl	%ebx, %esi
.L261:
	movl	%r13d, %r8d
	leal	1(%rax), %ecx
	orw	$-32768, %r8w
	cmpl	$32767, %r13d
	jg	.L266
	orl	$61, %esi
	movw	%r8w, (%rdi)
	movl	%ecx, %eax
	movl	%esi, %ebx
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	28(%r12), %r9
	cmpq	%r14, %r9
	je	.L267
	cmpl	$2147483647, %eax
	je	.L250
	leal	(%rax,%rax), %r8d
	cmpl	$1073741823, %eax
	movl	$2147483647, %edx
	cmovge	%edx, %r8d
.L257:
	movl	%r8d, %ecx
	movq	%r9, -56(%rbp)
	subl	%eax, %ecx
	cmpl	$4, %ecx
	jle	.L250
	movslq	%r8d, %rdi
	movl	%r8d, -60(%rbp)
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movl	-60(%rbp), %r8d
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L294
	movslq	12(%r12), %r15
	movq	(%r12), %r10
	movq	%rax, %rdi
	movl	%r8d, -64(%rbp)
	movq	%r9, -72(%rbp)
	movl	%r15d, -60(%rbp)
	addq	%r15, %r15
	movq	%r10, %rsi
	movq	%r15, %rdx
	movq	%r10, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %r10
	movq	-72(%rbp), %r9
	movl	-60(%rbp), %ecx
	movl	-64(%rbp), %r8d
	cmpq	%r10, %r9
	je	.L259
	movq	%r10, %rdi
	movl	%r8d, -56(%rbp)
	call	uprv_free_67@PLT
	movslq	12(%r12), %r15
	movl	-56(%rbp), %r8d
	movq	%r15, %rcx
	addq	%r15, %r15
.L259:
	movq	%r14, (%r12)
	movl	%r8d, 8(%r12)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L262:
	movl	%ebx, %esi
	movl	%ebx, %eax
	sarl	$30, %esi
	sarl	$15, %eax
	addl	$62, %esi
	orw	$-32768, %ax
	sall	$6, %esi
	movw	%ax, (%rdi)
	leal	3(%rcx), %eax
	movw	%r8w, 2(%r14,%rdx)
	orl	$28672, %esi
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L266:
	movl	%r13d, %ebx
	movl	%r13d, %edx
	movslq	%ecx, %rcx
	addl	$2, %eax
	sarl	$30, %ebx
	sarl	$15, %edx
	addl	$62, %ebx
	orw	$-32768, %dx
	movw	%dx, (%rdi)
	orl	%esi, %ebx
	movw	%r8w, (%r14,%rcx,2)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L267:
	movl	$2000, %r8d
	jmp	.L257
.L294:
	movl	$7, 24(%r12)
	jmp	.L246
.L264:
	movslq	%eax, %rdi
	addq	%rdi, %rdi
	addq	%r14, %rdi
	jmp	.L261
	.cfi_endproc
.LFE2811:
	.size	_ZN6icu_675Edits10addReplaceEii.part.0, .-_ZN6icu_675Edits10addReplaceEii.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits10addReplaceEii
	.type	_ZN6icu_675Edits10addReplaceEii, @function
_ZN6icu_675Edits10addReplaceEii:
.LFB2321:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jg	.L346
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	testl	%esi, %esi
	js	.L320
	movl	%edx, %r13d
	testl	%edx, %edx
	js	.L320
	movl	%esi, %eax
	orl	%edx, %eax
	je	.L295
	movl	%edx, %eax
	addl	$1, 20(%rdi)
	subl	%esi, %eax
	testl	%eax, %eax
	je	.L299
	movl	16(%rdi), %edx
	jle	.L300
	testl	%edx, %edx
	js	.L301
	movl	$2147483647, %ecx
	subl	%edx, %ecx
	cmpl	%ecx, %eax
	jle	.L301
.L302:
	movl	$8, 24(%r12)
.L295:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	testl	%edx, %edx
	jns	.L301
	movl	$-2147483648, %ecx
	subl	%edx, %ecx
	cmpl	%ecx, %eax
	jl	.L302
	.p2align 4,,10
	.p2align 3
.L301:
	addl	%edx, %eax
	movl	%eax, 16(%r12)
.L299:
	leal	-1(%rbx), %eax
	cmpl	$5, %eax
	ja	.L303
	cmpl	$7, %r13d
	jle	.L350
.L303:
	cmpl	$60, %ebx
	jg	.L305
	cmpl	$60, %r13d
	jg	.L305
	sall	$6, %ebx
	movl	%ebx, %eax
	orl	%r13d, %eax
	orb	$112, %ah
	movl	%eax, %esi
.L349:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_675Edits6appendEi
	.p2align 4,,10
	.p2align 3
.L346:
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movl	8(%r12), %eax
	movl	12(%r12), %ecx
	movq	(%r12), %r14
	movl	%eax, %edx
	subl	%ecx, %edx
	cmpl	$4, %edx
	jle	.L306
	movslq	%ecx, %r15
	addq	%r15, %r15
.L307:
	leal	1(%rcx), %edx
	movslq	%edx, %rax
	leaq	(%rax,%rax), %r8
	leaq	(%r14,%r8), %rdi
	cmpl	$60, %ebx
	jle	.L351
	movl	%ebx, %r9d
	leal	2(%rcx), %edx
	orw	$-32768, %r9w
	cmpl	$32767, %ebx
	jg	.L313
	movw	%r9w, (%rdi)
	movl	$32576, %esi
.L314:
	cmpl	$60, %r13d
	jg	.L315
	movl	%r13d, %ebx
	orl	%esi, %ebx
.L316:
	movw	%bx, (%r14,%r15)
	movl	%edx, 12(%r12)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L350:
	sall	$12, %ebx
	movl	%r13d, %eax
	sall	$9, %eax
	movl	%ebx, %esi
	orl	%eax, %esi
	movslq	12(%r12), %rax
	testl	%eax, %eax
	jle	.L349
	movq	(%r12), %rdx
	leaq	-2(%rdx,%rax,2), %rcx
	movzwl	(%rcx), %eax
	leal	-4096(%rax), %edi
	movl	%eax, %edx
	cmpl	$24574, %edi
	ja	.L349
	andl	$-512, %eax
	cmpl	%eax, %esi
	jne	.L349
	movl	%edx, %eax
	andw	$511, %ax
	cmpw	$511, %ax
	je	.L349
	addl	$1, %edx
	movw	%dx, (%rcx)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L351:
	sall	$6, %ebx
	orb	$112, %bh
	movl	%ebx, %esi
.L312:
	movl	%r13d, %r8d
	leal	1(%rdx), %ecx
	orw	$-32768, %r8w
	cmpl	$32767, %r13d
	jg	.L317
	orl	$61, %esi
	movw	%r8w, (%rdi)
	movl	%ecx, %edx
	movl	%esi, %ebx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L306:
	leaq	28(%r12), %r9
	cmpq	%r14, %r9
	je	.L318
	cmpl	$2147483647, %eax
	je	.L302
	leal	(%rax,%rax), %r8d
	cmpl	$1073741823, %eax
	movl	$2147483647, %edx
	cmovge	%edx, %r8d
.L308:
	movl	%r8d, %ecx
	movq	%r9, -56(%rbp)
	subl	%eax, %ecx
	cmpl	$4, %ecx
	jle	.L302
	movslq	%r8d, %rdi
	movl	%r8d, -60(%rbp)
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movl	-60(%rbp), %r8d
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L352
	movslq	12(%r12), %r15
	movq	(%r12), %r10
	movq	%rax, %rdi
	movl	%r8d, -64(%rbp)
	movq	%r9, -72(%rbp)
	movl	%r15d, -60(%rbp)
	addq	%r15, %r15
	movq	%r10, %rsi
	movq	%r15, %rdx
	movq	%r10, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %r10
	movq	-72(%rbp), %r9
	movl	-60(%rbp), %ecx
	movl	-64(%rbp), %r8d
	cmpq	%r10, %r9
	je	.L310
	movq	%r10, %rdi
	movl	%r8d, -56(%rbp)
	call	uprv_free_67@PLT
	movslq	12(%r12), %r15
	movl	-56(%rbp), %r8d
	movq	%r15, %rcx
	addq	%r15, %r15
.L310:
	movq	%r14, (%r12)
	movl	%r8d, 8(%r12)
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L317:
	movl	%r13d, %ebx
	movl	%r13d, %eax
	movslq	%ecx, %rcx
	addl	$2, %edx
	sarl	$30, %ebx
	sarl	$15, %eax
	addl	$62, %ebx
	orw	$-32768, %ax
	movw	%ax, (%rdi)
	orl	%esi, %ebx
	movw	%r8w, (%r14,%rcx,2)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L313:
	movl	%ebx, %esi
	movl	%ebx, %eax
	leal	3(%rcx), %edx
	sarl	$30, %esi
	sarl	$15, %eax
	addl	$62, %esi
	orw	$-32768, %ax
	sall	$6, %esi
	movw	%ax, (%rdi)
	orl	$28672, %esi
	movw	%r9w, 2(%r14,%r8)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L318:
	movl	$2000, %r8d
	jmp	.L308
.L315:
	movslq	%edx, %rdi
	addq	%rdi, %rdi
	addq	%r14, %rdi
	jmp	.L312
.L352:
	movl	$7, 24(%r12)
	jmp	.L295
	.cfi_endproc
.LFE2321:
	.size	_ZN6icu_675Edits10addReplaceEii, .-_ZN6icu_675Edits10addReplaceEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits9growArrayEv
	.type	_ZN6icu_675Edits9growArrayEv, @function
_ZN6icu_675Edits9growArrayEv:
.LFB2323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	28(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %eax
	cmpq	%r14, (%rdi)
	je	.L360
	cmpl	$2147483647, %eax
	je	.L357
	leal	(%rax,%rax), %r12d
	cmpl	$1073741823, %eax
	movl	$2147483647, %edx
	cmovge	%edx, %r12d
.L354:
	movl	%r12d, %ecx
	subl	%eax, %ecx
	cmpl	$4, %ecx
	jg	.L363
.L357:
	movl	$8, 24(%rbx)
	xorl	%eax, %eax
.L353:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	movslq	%r12d, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L364
	movq	(%rbx), %r15
	movslq	12(%rbx), %rdx
	movq	%rax, %rdi
	addq	%rdx, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	cmpq	%r15, %r14
	je	.L359
	movq	%r15, %rdi
	call	uprv_free_67@PLT
.L359:
	movq	%r13, (%rbx)
	movl	$1, %eax
	movl	%r12d, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movl	$2000, %r12d
	jmp	.L354
.L364:
	movl	$7, 24(%rbx)
	xorl	%eax, %eax
	jmp	.L353
	.cfi_endproc
.LFE2323:
	.size	_ZN6icu_675Edits9growArrayEv, .-_ZN6icu_675Edits9growArrayEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675Edits11copyErrorToER10UErrorCode
	.type	_ZNK6icu_675Edits11copyErrorToER10UErrorCode, @function
_ZNK6icu_675Edits11copyErrorToER10UErrorCode:
.LFB2324:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jg	.L365
	movl	24(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L365
	movl	%edx, (%rsi)
	movl	$1, %eax
.L365:
	ret
	.cfi_endproc
.LFE2324:
	.size	_ZNK6icu_675Edits11copyErrorToER10UErrorCode, .-_ZNK6icu_675Edits11copyErrorToER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits8IteratorC2EPKtiaa
	.type	_ZN6icu_675Edits8IteratorC2EPKtiaa, @function
_ZN6icu_675Edits8IteratorC2EPKtiaa:
.LFB2327:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rdi)
	movl	$0, 8(%rdi)
	movl	%edx, 12(%rdi)
	movl	$0, 16(%rdi)
	movb	%cl, 20(%rdi)
	movb	%r8b, 21(%rdi)
	movw	%ax, 22(%rdi)
	movl	$0, 40(%rdi)
	movups	%xmm0, 24(%rdi)
	ret
	.cfi_endproc
.LFE2327:
	.size	_ZN6icu_675Edits8IteratorC2EPKtiaa, .-_ZN6icu_675Edits8IteratorC2EPKtiaa
	.globl	_ZN6icu_675Edits8IteratorC1EPKtiaa
	.set	_ZN6icu_675Edits8IteratorC1EPKtiaa,_ZN6icu_675Edits8IteratorC2EPKtiaa
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits8Iterator10readLengthEi
	.type	_ZN6icu_675Edits8Iterator10readLengthEi, @function
_ZN6icu_675Edits8Iterator10readLengthEi:
.LFB2329:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	cmpl	$60, %esi
	jle	.L370
	movslq	8(%rdi), %rcx
	movq	(%rdi), %rsi
	movq	%rcx, %rdx
	leaq	(%rcx,%rcx), %r8
	movzwl	(%rsi,%rcx,2), %ecx
	cmpl	$61, %eax
	jne	.L372
	addl	$1, %edx
	movl	%ecx, %eax
	movl	%edx, 8(%rdi)
	andl	$32767, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	movzwl	2(%rsi,%r8), %esi
	sall	$15, %ecx
	addl	$2, %edx
	sall	$30, %eax
	andl	$1073709056, %ecx
	movl	%edx, 8(%rdi)
	andl	$1073741824, %eax
	andl	$32767, %esi
	orl	%esi, %ecx
	orl	%ecx, %eax
.L370:
	ret
	.cfi_endproc
.LFE2329:
	.size	_ZN6icu_675Edits8Iterator10readLengthEi, .-_ZN6icu_675Edits8Iterator10readLengthEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits8Iterator17updateNextIndexesEv
	.type	_ZN6icu_675Edits8Iterator17updateNextIndexesEv, @function
_ZN6icu_675Edits8Iterator17updateNextIndexesEv:
.LFB2330:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	addl	%eax, 32(%rdi)
	cmpb	$0, 23(%rdi)
	movl	28(%rdi), %eax
	je	.L375
	addl	%eax, 36(%rdi)
.L375:
	addl	%eax, 40(%rdi)
	ret
	.cfi_endproc
.LFE2330:
	.size	_ZN6icu_675Edits8Iterator17updateNextIndexesEv, .-_ZN6icu_675Edits8Iterator17updateNextIndexesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits8Iterator21updatePreviousIndexesEv
	.type	_ZN6icu_675Edits8Iterator21updatePreviousIndexesEv, @function
_ZN6icu_675Edits8Iterator21updatePreviousIndexesEv:
.LFB2331:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	subl	%eax, 32(%rdi)
	cmpb	$0, 23(%rdi)
	movl	28(%rdi), %eax
	je	.L377
	subl	%eax, 36(%rdi)
.L377:
	subl	%eax, 40(%rdi)
	ret
	.cfi_endproc
.LFE2331:
	.size	_ZN6icu_675Edits8Iterator21updatePreviousIndexesEv, .-_ZN6icu_675Edits8Iterator21updatePreviousIndexesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits8Iterator6noNextEv
	.type	_ZN6icu_675Edits8Iterator6noNextEv, @function
_ZN6icu_675Edits8Iterator6noNextEv:
.LFB2332:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movq	$0, 24(%rdi)
	movw	%ax, 22(%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2332:
	.size	_ZN6icu_675Edits8Iterator6noNextEv, .-_ZN6icu_675Edits8Iterator6noNextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits8Iterator4nextEaR10UErrorCode
	.type	_ZN6icu_675Edits8Iterator4nextEaR10UErrorCode, @function
_ZN6icu_675Edits8Iterator4nextEaR10UErrorCode:
.LFB2333:
	.cfi_startproc
	endbr64
	movl	(%rdx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L425
	cmpb	$0, 22(%rdi)
	movl	16(%rdi), %eax
	jle	.L381
	movl	24(%rdi), %edx
	addl	%edx, 32(%rdi)
	cmpb	$0, 23(%rdi)
	movl	28(%rdi), %edx
	jne	.L428
.L382:
	addl	%edx, 40(%rdi)
.L383:
	testl	%eax, %eax
	jle	.L385
	cmpl	$1, %eax
	je	.L386
	subl	$1, %eax
	movl	%eax, 16(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	je	.L384
	testl	%eax, %eax
	jle	.L384
	addl	$1, 8(%rdi)
	movl	$1, %eax
	movb	$1, 22(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	movl	$0, 16(%rdi)
.L385:
	movslq	8(%rdi), %rax
	movl	12(%rdi), %r9d
	cmpl	%r9d, %eax
	jge	.L429
	movq	(%rdi), %r8
	leal	1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	movzwl	(%r8,%rax,2), %edx
	cmpl	$4095, %edx
	jg	.L388
	leal	1(%rdx), %r10d
	movb	$0, 23(%rdi)
	movl	%r10d, 24(%rdi)
	cmpl	%ecx, %r9d
	jle	.L389
	movslq	%ecx, %rax
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L430:
	leal	1(%rax), %ecx
	leal	1(%rdx,%r10), %r10d
	addq	$1, %rax
	movl	%ecx, 8(%rdi)
	movl	%r10d, 24(%rdi)
	cmpl	%eax, %r9d
	jle	.L389
.L391:
	movzwl	(%r8,%rax,2), %edx
	movl	%eax, %ecx
	cmpl	$4095, %edx
	jle	.L430
	movl	%r10d, 28(%rdi)
	testb	%sil, %sil
	je	.L426
	addl	%r10d, 32(%rdi)
	addl	%r10d, 40(%rdi)
	cmpl	%eax, %r9d
	jle	.L409
	addl	$1, %ecx
	movl	%ecx, 8(%rdi)
.L388:
	movb	$1, 23(%rdi)
	movzbl	21(%rdi), %esi
	cmpl	$28671, %edx
	jg	.L394
	movl	%edx, %eax
	movl	%edx, %r10d
	andl	$511, %edx
	sarl	$9, %eax
	sarl	$12, %r10d
	addl	$1, %edx
	andl	$7, %eax
	testb	%sil, %sil
	je	.L395
	imull	%edx, %r10d
	imull	%edx, %eax
	movl	%r10d, 24(%rdi)
	movl	%eax, 28(%rdi)
.L402:
	cmpl	%ecx, %r9d
	jle	.L426
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L431:
	andl	$511, %edx
	movl	%eax, %ecx
	sarl	$9, %eax
	leal	1(%rdx), %r11d
	movl	%eax, %edx
	sarl	$12, %ecx
	andl	$7, %edx
	imull	%r11d, %ecx
	imull	%r11d, %edx
	addl	%ebx, %ecx
	addl	%r10d, %edx
	movl	%ecx, 24(%rdi)
	movl	%esi, %ecx
	movl	%edx, 28(%rdi)
.L404:
	cmpl	%ecx, %r9d
	jle	.L397
.L396:
	movslq	%ecx, %rax
	leaq	(%rax,%rax), %r11
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %edx
	cmpl	$4095, %eax
	jle	.L397
	leal	1(%rcx), %esi
	movl	24(%rdi), %ebx
	movl	28(%rdi), %r10d
	movl	%esi, 8(%rdi)
	cmpl	$28671, %eax
	jle	.L431
	sarl	$6, %eax
	andl	$63, %eax
	cmpl	$60, %eax
	jle	.L412
	movzwl	2(%r8,%r11), %esi
	cmpl	$61, %eax
	jne	.L406
	addl	$2, %ecx
	movl	%esi, %eax
	movl	%ecx, 8(%rdi)
	andl	$32767, %eax
.L405:
	addl	%ebx, %eax
	movl	%eax, 24(%rdi)
	movl	%edx, %eax
	andl	$63, %edx
	andl	$63, %eax
	cmpw	$60, %ax
	jbe	.L407
	movslq	%ecx, %rsi
	leaq	(%rsi,%rsi), %r11
	movzwl	(%r8,%rsi,2), %esi
	cmpw	$61, %ax
	jne	.L408
	addl	$1, %ecx
	movl	%esi, %edx
	movl	%ecx, 8(%rdi)
	andl	$32767, %edx
.L407:
	addl	%r10d, %edx
	movl	%edx, 28(%rdi)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	movq	$0, 24(%rdi)
	movw	%ax, 22(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	addl	%edx, 36(%rdi)
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L384:
	movb	$1, 22(%rdi)
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	movl	%esi, %ecx
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L408:
	movzwl	2(%r8,%r11), %eax
	sall	$15, %esi
	sall	$30, %edx
	addl	$2, %ecx
	andl	$1073709056, %esi
	andl	$1073741824, %edx
	movl	%ecx, 8(%rdi)
	andl	$32767, %eax
	orl	%eax, %esi
	orl	%esi, %edx
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L406:
	movzwl	4(%r8,%r11), %r11d
	sall	$15, %esi
	sall	$30, %eax
	addl	$3, %ecx
	andl	$1073709056, %esi
	andl	$1073741824, %eax
	movl	%ecx, 8(%rdi)
	andl	$32767, %r11d
	orl	%r11d, %esi
	orl	%esi, %eax
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	%edx, %eax
	sarl	$6, %eax
	andl	$63, %eax
	cmpl	$60, %eax
	jle	.L398
	movslq	%ecx, %r10
	leaq	(%r10,%r10), %r11
	movzwl	(%r8,%r10,2), %r10d
	cmpl	$61, %eax
	jne	.L399
	addl	$1, %ecx
	movl	%r10d, %eax
	movl	%ecx, 8(%rdi)
	andl	$32767, %eax
.L398:
	andl	$63, %edx
	movl	%eax, 24(%rdi)
	cmpl	$60, %edx
	jle	.L400
	movslq	%ecx, %rax
	leaq	(%rax,%rax), %r10
	movzwl	(%r8,%rax,2), %eax
	cmpl	$61, %edx
	jne	.L401
	addl	$1, %ecx
	movl	%eax, %edx
	movl	%ecx, 8(%rdi)
	andl	$32767, %edx
.L400:
	movl	%edx, 28(%rdi)
	testb	%sil, %sil
	jne	.L402
.L426:
	movl	$1, %eax
.L425:
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	movl	%r10d, 28(%rdi)
	testb	%sil, %sil
	je	.L426
	addl	%r10d, 32(%rdi)
	addl	%r10d, 40(%rdi)
.L409:
	movb	$0, 22(%rdi)
	xorl	%eax, %eax
	movq	$0, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	movl	%r10d, 24(%rdi)
	movl	%eax, 28(%rdi)
	cmpl	$1, %edx
	je	.L426
	movl	%edx, 16(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	movzwl	2(%r8,%r11), %r11d
	sall	$15, %r10d
	sall	$30, %eax
	addl	$2, %ecx
	andl	$1073709056, %r10d
	andl	$1073741824, %eax
	movl	%ecx, 8(%rdi)
	andl	$32767, %r11d
	orl	%r11d, %r10d
	orl	%r10d, %eax
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L401:
	movzwl	2(%r8,%r10), %r10d
	sall	$15, %eax
	sall	$30, %edx
	addl	$2, %ecx
	andl	$1073709056, %eax
	andl	$1073741824, %edx
	movl	%ecx, 8(%rdi)
	andl	$32767, %r10d
	orl	%r10d, %eax
	orl	%eax, %edx
	jmp	.L400
	.cfi_endproc
.LFE2333:
	.size	_ZN6icu_675Edits8Iterator4nextEaR10UErrorCode, .-_ZN6icu_675Edits8Iterator4nextEaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits14mergeAndAppendERKS0_S2_R10UErrorCode
	.type	_ZN6icu_675Edits14mergeAndAppendERKS0_S2_R10UErrorCode, @function
_ZN6icu_675Edits14mergeAndAppendERKS0_S2_R10UErrorCode:
.LFB2325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L434
	movl	24(%rdi), %eax
	movq	%rcx, %r14
	testl	%eax, %eax
	jle	.L435
.L539:
	movl	%eax, (%r14)
.L434:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L541
	addq	$136, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L435:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	(%rdx), %rcx
	movl	$0, -104(%rbp)
	xorl	%r9d, %r9d
	movq	$0, -96(%rbp)
	movq	%r14, %rdi
	xorl	%r11d, %r11d
	xorl	%ebx, %ebx
	movq	%rax, -160(%rbp)
	movl	12(%rsi), %eax
	movl	%r9d, %r14d
	xorl	%r10d, %r10d
	movl	$0, -72(%rbp)
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	movl	%eax, -152(%rbp)
	movl	12(%rdx), %eax
	movq	%rdi, %r9
	pxor	%xmm0, %xmm0
	movl	$0, -140(%rbp)
	movl	$0, -144(%rbp)
	movl	$0, -148(%rbp)
	movq	%rcx, -112(%rbp)
	xorl	%ecx, %ecx
	movl	%eax, -100(%rbp)
	movl	$1, %eax
	movups	%xmm0, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L436:
	testl	%ebx, %ebx
	jne	.L437
	movq	%r9, %rbx
.L525:
	testb	%al, %al
	je	.L438
.L546:
	movsbl	-92(%rbp), %esi
	leaq	-112(%rbp), %rdi
	movq	%rbx, %rdx
	movl	%r10d, -128(%rbp)
	movq	%r8, -136(%rbp)
	movb	%r11b, -121(%rbp)
	movl	%ecx, -120(%rbp)
	call	_ZN6icu_675Edits8Iterator4nextEaR10UErrorCode
	movl	-120(%rbp), %ecx
	movl	-128(%rbp), %r10d
	testb	%al, %al
	movzbl	-121(%rbp), %r11d
	movq	-136(%rbp), %r8
	je	.L438
	movl	-88(%rbp), %edx
	movl	-84(%rbp), %ecx
	testl	%edx, %edx
	je	.L542
	movq	%rbx, %r9
	movl	%edx, %ebx
.L437:
	testl	%r12d, %r12d
	je	.L483
	movzbl	-89(%rbp), %edx
	testb	%r11b, %r11b
	je	.L543
.L464:
	addl	%r13d, %r14d
	testb	%dl, %dl
	jne	.L544
	cmpl	%ebx, %r12d
	jle	.L545
	addl	%ecx, %r15d
.L477:
	subl	%ebx, %r12d
	xorl	%r13d, %r13d
	movq	%r9, %rbx
	testb	%al, %al
	jne	.L546
.L438:
	movq	%rbx, %r9
	testl	%r12d, %r12d
	jne	.L547
	xorl	%ebx, %ebx
	xorl	%eax, %eax
.L483:
	movl	(%r9), %edi
	testl	%edi, %edi
	jg	.L443
	movl	-148(%rbp), %edi
	testl	%edi, %edi
	je	.L444
	cmpl	$1, %edi
	je	.L444
	subl	$1, %edi
	movl	-140(%rbp), %r12d
	movl	-144(%rbp), %r13d
	movl	%edi, -148(%rbp)
.L445:
	testl	%r12d, %r12d
	je	.L548
.L448:
	testl	%ebx, %ebx
	je	.L533
	movl	%r12d, -140(%rbp)
	movzbl	-89(%rbp), %edx
	movl	%r13d, -144(%rbp)
	testb	%r11b, %r11b
	jne	.L464
.L543:
	testb	%dl, %dl
	je	.L549
	addl	%ecx, %r15d
	cmpl	%ebx, %r12d
	jge	.L550
	addl	%r13d, %r14d
.L478:
	subl	%r12d, %ebx
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L544:
	addl	%ecx, %r15d
	cmpl	%ebx, %r12d
	je	.L551
	jge	.L477
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L444:
	movl	-152(%rbp), %r11d
	cmpl	%r10d, %r11d
	jg	.L552
	movq	%r9, %rax
	movl	%r14d, %r9d
	movq	%rax, %r14
	testl	%ebx, %ebx
	jne	.L530
	movl	%r9d, %eax
	orl	%r15d, %eax
	je	.L486
	.p2align 4,,10
	.p2align 3
.L485:
	movq	%r8, %rdi
	movl	%r15d, %edx
	movl	%r9d, %esi
	movq	%r8, -120(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii
	movl	(%r14), %eax
	movq	-120(%rbp), %r8
	testl	%eax, %eax
	jg	.L434
.L486:
	movl	24(%r8), %eax
	testl	%eax, %eax
	jg	.L539
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L548:
	addl	%r13d, %r14d
	cmpl	%ebx, -88(%rbp)
	je	.L457
	cmpb	$0, -89(%rbp)
	je	.L457
	movl	$0, -140(%rbp)
	movl	%r13d, -144(%rbp)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L549:
	movl	%r14d, %esi
	movl	24(%r8), %edx
	orl	%r15d, %esi
	jne	.L553
.L466:
	cmpl	%r13d, %ecx
	movl	%r13d, %ebx
	cmovle	%ecx, %ebx
	testl	%ebx, %ebx
	je	.L470
	testl	%edx, %edx
	jg	.L470
	testl	%ebx, %ebx
	js	.L554
	movq	%r8, %rdi
	movl	%ebx, %esi
	movl	%ecx, -128(%rbp)
	movq	%r9, -168(%rbp)
	movl	%r10d, -136(%rbp)
	movb	%al, -121(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN6icu_675Edits12addUnchangedEi.part.0
	movq	-168(%rbp), %r9
	movl	-136(%rbp), %r10d
	movl	-128(%rbp), %ecx
	movzbl	-121(%rbp), %eax
	movq	-120(%rbp), %r8
.L470:
	subl	%ebx, %r13d
	subl	%ebx, %ecx
	xorl	%r11d, %r11d
	xorl	%r15d, %r15d
	movl	%ecx, %ebx
	movl	%r13d, %r12d
	xorl	%r14d, %r14d
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L457:
	movl	24(%r8), %esi
	testl	%esi, %esi
	jg	.L536
	testl	%r14d, %r14d
	js	.L498
	testl	%r15d, %r15d
	js	.L498
	movl	%r15d, %edi
	orl	%r14d, %edi
	movl	%edi, -140(%rbp)
	je	.L495
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r8, %rdi
	movq	%r9, -176(%rbp)
	movl	%r10d, -168(%rbp)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movb	%r11b, -136(%rbp)
	movl	%ecx, -128(%rbp)
	movb	%al, -121(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii.part.0
	movl	%r13d, -144(%rbp)
	movq	-120(%rbp), %r8
	movl	$0, -140(%rbp)
	movzbl	-121(%rbp), %eax
	movl	-128(%rbp), %ecx
	movzbl	-136(%rbp), %r11d
	movl	-168(%rbp), %r10d
	movq	-176(%rbp), %r9
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L552:
	movq	-160(%rbp), %r13
	movslq	%r10d, %rsi
	leal	1(%r10), %edx
	leaq	(%rsi,%rsi), %rdi
	movzwl	0(%r13,%rsi,2), %r12d
	movl	%r12d, %esi
	cmpl	$4095, %r12d
	jg	.L447
	leal	1(%r12), %r13d
	cmpl	%edx, %r11d
	jle	.L490
	movq	-160(%rbp), %rdi
	movslq	%edx, %rdx
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L555:
	leal	1(%rdx), %r10d
	addq	$1, %rdx
	leal	1(%r13,%rsi), %r13d
	cmpl	%edx, %r11d
	jle	.L449
.L450:
	movzwl	(%rdi,%rdx,2), %esi
	movl	%edx, %r10d
	cmpl	$4095, %esi
	jle	.L555
.L449:
	movl	$0, -148(%rbp)
	movl	%r13d, %r12d
	xorl	%r11d, %r11d
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L542:
	addl	%ecx, %r15d
	testl	%r12d, %r12d
	je	.L439
	testb	%r11b, %r11b
	jne	.L525
.L439:
	movl	24(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L538
	testl	%r14d, %r14d
	js	.L497
	testl	%r15d, %r15d
	js	.L497
	movl	%r14d, %esi
	orl	%r15d, %esi
	je	.L538
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r8, %rdi
	movl	%r10d, -168(%rbp)
	movb	%r11b, -136(%rbp)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movl	%ecx, -128(%rbp)
	movb	%al, -121(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii.part.0
	movq	-120(%rbp), %r8
	movl	-128(%rbp), %ecx
	movzbl	-121(%rbp), %eax
	movzbl	-136(%rbp), %r11d
	movl	-168(%rbp), %r10d
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L447:
	cmpl	$28671, %r12d
	jg	.L452
	movl	%r12d, %r13d
	sarl	$9, %r12d
	movl	%edx, %r10d
	movl	$0, %edx
	sarl	$12, %r13d
	andl	$7, %r12d
	andl	$511, %esi
	movl	$1, %r11d
	leal	1(%rsi), %edi
	cmovne	%edi, %edx
	movl	%edx, -148(%rbp)
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L498:
	movl	$1, 24(%r8)
.L536:
	movl	$0, -140(%rbp)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movl	%r13d, -144(%rbp)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L553:
	testl	%edx, %edx
	jg	.L467
	testl	%r14d, %r14d
	js	.L499
	testl	%r15d, %r15d
	js	.L499
	movl	%r15d, %edx
	movq	%r8, %rdi
	movl	%r14d, %esi
	movq	%r9, -168(%rbp)
	movl	%r10d, -136(%rbp)
	movl	%ecx, -128(%rbp)
	movb	%al, -121(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii.part.0
	movq	-120(%rbp), %r8
	movq	-168(%rbp), %r9
	movl	-136(%rbp), %r10d
	movl	-128(%rbp), %ecx
	movl	24(%r8), %edx
	movzbl	-121(%rbp), %eax
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L551:
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r8, %rdi
	movq	%r9, -176(%rbp)
	movl	%r10d, -168(%rbp)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movb	%r11b, -136(%rbp)
	movl	%ecx, -128(%rbp)
	movb	%al, -121(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii
.L537:
	movq	-176(%rbp), %r9
	movzbl	-121(%rbp), %eax
	movl	-128(%rbp), %ecx
	movzbl	-136(%rbp), %r11d
	movl	-168(%rbp), %r10d
	movq	-120(%rbp), %r8
	movq	%r9, %rbx
	jmp	.L525
.L497:
	movl	$1, 24(%r8)
.L538:
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r9, %rax
	movl	%r14d, %r9d
	movq	%rax, %r14
	testl	%ebx, %ebx
	jne	.L434
	movl	%r9d, %eax
	orl	%r15d, %eax
	jne	.L485
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L550:
	movl	%r12d, %r13d
	leal	(%r14,%rbx), %esi
	movl	%r15d, %edx
	movq	%r8, %rdi
	subl	%ebx, %r13d
	movq	%r9, -176(%rbp)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movl	%r10d, -168(%rbp)
	movl	%r13d, %r12d
	movb	%r11b, -136(%rbp)
	movl	%ecx, -128(%rbp)
	movb	%al, -121(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L547:
	movl	(%r9), %edx
	movq	%r9, %r14
	testl	%edx, %edx
	jg	.L434
.L530:
	movl	24(%r8), %eax
	testl	%eax, %eax
	jg	.L539
	movl	$1, (%r14)
	jmp	.L434
.L499:
	movl	$1, 24(%r8)
.L467:
	cmpl	%r13d, %ecx
	movl	%r13d, %ebx
	cmovle	%ecx, %ebx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L545:
	leal	(%r15,%r12), %edx
	movl	%r14d, %esi
	movq	%r8, %rdi
	subl	%r12d, %ebx
	movq	%r9, -168(%rbp)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movl	%r10d, -136(%rbp)
	movb	%r11b, -128(%rbp)
	movb	%al, -121(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN6icu_675Edits10addReplaceEii
	movq	-120(%rbp), %r8
	movzbl	-121(%rbp), %eax
	movl	%ebx, %ecx
	movzbl	-128(%rbp), %r11d
	movl	-136(%rbp), %r10d
	movq	-168(%rbp), %r9
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L452:
	sarl	$6, %r12d
	movl	%r12d, %r13d
	andl	$63, %r13d
	cmpl	$60, %r13d
	jle	.L492
	movq	-160(%rbp), %rdx
	movzwl	2(%rdx,%rdi), %edx
	cmpl	$61, %r13d
	jne	.L454
	movl	%edx, %r13d
	addl	$2, %r10d
	andl	$32767, %r13d
.L453:
	movl	%esi, %edx
	movl	%esi, %r12d
	andl	$63, %edx
	andl	$63, %r12d
	cmpw	$60, %dx
	jbe	.L455
	movq	-160(%rbp), %rdi
	movslq	%r10d, %rdx
	leaq	(%rdx,%rdx), %rsi
	movzwl	(%rdi,%rdx,2), %edx
	cmpl	$61, %r12d
	jne	.L456
	movl	%edx, %r12d
	addl	$1, %r10d
	andl	$32767, %r12d
.L455:
	movl	$0, -148(%rbp)
	movl	$1, %r11d
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L554:
	movl	$1, 24(%r8)
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L495:
	movl	%r13d, -144(%rbp)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L436
.L492:
	movl	%edx, %r10d
	jmp	.L453
.L454:
	movq	-160(%rbp), %r11
	sall	$15, %edx
	sall	$30, %r13d
	addl	$3, %r10d
	andl	$1073709056, %edx
	andl	$1073741824, %r13d
	movzwl	4(%r11,%rdi), %edi
	andl	$32767, %edi
	orl	%edi, %edx
	orl	%edx, %r13d
	jmp	.L453
.L456:
	movzwl	2(%rdi,%rsi), %esi
	sall	$15, %edx
	sall	$30, %r12d
	addl	$2, %r10d
	andl	$1073709056, %edx
	andl	$1073741824, %r12d
	andl	$32767, %esi
	orl	%esi, %edx
	orl	%edx, %r12d
	jmp	.L455
.L490:
	movl	$0, -148(%rbp)
	movl	%r13d, %r12d
	movl	%edx, %r10d
	xorl	%r11d, %r11d
	jmp	.L448
.L541:
	call	__stack_chk_fail@PLT
.L533:
	movq	%r9, %r14
	jmp	.L530
	.cfi_endproc
.LFE2325:
	.size	_ZN6icu_675Edits14mergeAndAppendERKS0_S2_R10UErrorCode, .-_ZN6icu_675Edits14mergeAndAppendERKS0_S2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits8Iterator8previousER10UErrorCode
	.type	_ZN6icu_675Edits8Iterator8previousER10UErrorCode, @function
_ZN6icu_675Edits8Iterator8previousER10UErrorCode:
.LFB2334:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L608
	cmpb	$0, 22(%rdi)
	movl	16(%rdi), %eax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movslq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	js	.L558
	je	.L559
	testl	%eax, %eax
	jg	.L611
	movl	24(%rdi), %edx
	addl	%edx, 32(%rdi)
	cmpb	$0, 23(%rdi)
	movl	28(%rdi), %edx
	je	.L561
	addl	%edx, 36(%rdi)
.L561:
	addl	%edx, 40(%rdi)
.L559:
	movb	$-1, 22(%rdi)
.L558:
	testl	%eax, %eax
	jle	.L562
	movq	(%rdi), %rcx
	movslq	%r12d, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	andl	$511, %edx
	cmpl	%eax, %edx
	jge	.L612
	movl	$0, 16(%rdi)
.L562:
	testl	%r12d, %r12d
	jle	.L613
	leal	-1(%r12), %esi
	movq	(%rdi), %r8
	movl	32(%rdi), %r10d
	movslq	%esi, %rax
	movl	%esi, 8(%rdi)
	movl	40(%rdi), %r9d
	leaq	(%rax,%rax), %rcx
	movzwl	(%r8,%rax,2), %eax
	movl	%eax, %r14d
	cmpl	$4095, %eax
	jle	.L614
	movzbl	21(%rdi), %edx
	movl	36(%rdi), %ebx
	movb	$1, 23(%rdi)
	cmpl	$28671, %eax
	jg	.L569
	movl	%r14d, %ecx
	movl	%eax, %r11d
	sarl	$9, %eax
	andl	$511, %ecx
	sarl	$12, %r11d
	andl	$7, %eax
	addl	$1, %ecx
	testb	%dl, %dl
	je	.L570
	imull	%ecx, %r11d
	imull	%eax, %ecx
	movl	%r11d, 24(%rdi)
	movl	%ecx, 28(%rdi)
.L586:
	testl	%esi, %esi
	jle	.L571
	movslq	%esi, %rsi
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L615:
	movl	%eax, %r12d
	andl	$511, %edx
	sarl	$9, %eax
	addl	$1, %edx
	sarl	$12, %r12d
	andl	$7, %eax
	imull	%edx, %r12d
	imull	%edx, %eax
	addl	%r12d, %r11d
	addl	%eax, %ecx
	movl	%r11d, 24(%rdi)
	movl	%ecx, 28(%rdi)
.L589:
	subq	$1, %rsi
	testl	%esi, %esi
	jle	.L571
.L572:
	movzwl	-2(%r8,%rsi,2), %eax
	movl	%esi, %r13d
	movl	%eax, %edx
	cmpl	$4095, %eax
	jle	.L571
	leal	-1(%rsi), %r12d
	movl	%r12d, 8(%rdi)
	cmpl	$28671, %eax
	jle	.L615
	cmpl	$32767, %eax
	jg	.L589
	sarl	$6, %eax
	andl	$63, %eax
	cmpl	$60, %eax
	jle	.L590
	movzwl	(%r8,%rsi,2), %r14d
	cmpl	$61, %eax
	jne	.L591
	movl	%r14d, %eax
	addl	$1, %r13d
	andl	$32767, %eax
.L590:
	movl	%edx, %r14d
	addl	%eax, %r11d
	andl	$63, %edx
	andl	$63, %r14d
	movl	%r11d, 24(%rdi)
	cmpw	$60, %r14w
	jbe	.L592
	movslq	%r13d, %rax
	leaq	(%rax,%rax), %r13
	movzwl	(%r8,%rax,2), %eax
	cmpw	$61, %r14w
	jne	.L593
	movl	%eax, %edx
	andl	$32767, %edx
.L592:
	addl	%edx, %ecx
	movl	%r12d, 8(%rdi)
	movl	%ecx, 28(%rdi)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	addl	$1, %eax
	movl	%eax, 16(%rdi)
	movl	24(%rdi), %eax
	subl	%eax, 32(%rdi)
	cmpb	$0, 23(%rdi)
	movl	28(%rdi), %eax
	je	.L564
	subl	%eax, 36(%rdi)
.L564:
	subl	%eax, 40(%rdi)
	movl	$1, %eax
.L556:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	addl	$1, %eax
	movb	$0, 23(%rdi)
	movl	%eax, 24(%rdi)
	testl	%esi, %esi
	je	.L567
	leal	-2(%r12), %ecx
	subq	$3, %r12
	movslq	%ecx, %rdx
	movl	%ecx, %ecx
	subq	%rcx, %r12
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L616:
	movl	%edx, 8(%rdi)
	leal	1(%rcx,%rax), %eax
	subq	$1, %rdx
	movl	%eax, 24(%rdi)
	cmpq	%rdx, %r12
	je	.L567
.L568:
	movzwl	(%r8,%rdx,2), %ecx
	cmpl	$4095, %ecx
	jle	.L616
.L567:
	subl	%eax, %r10d
	subl	%eax, %r9d
	movl	%eax, 28(%rdi)
	movl	$1, %eax
	movl	%r10d, 32(%rdi)
	movl	%r9d, 40(%rdi)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L613:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	movq	$0, 24(%rdi)
	popq	%r13
	popq	%r14
	movw	%ax, 22(%rdi)
	xorl	%eax, %eax
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	.cfi_restore_state
	subl	%ecx, %ebx
	subl	%r11d, %r10d
	subl	%ecx, %r9d
	movl	$1, %eax
	movl	%ebx, 36(%rdi)
	popq	%rbx
	movl	%r10d, 32(%rdi)
	popq	%r12
	movl	%r9d, 40(%rdi)
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	leal	-2(%r12), %r13d
	movslq	%r13d, %r13
	cmpl	$32767, %eax
	jle	.L617
	.p2align 4,,10
	.p2align 3
.L580:
	movzwl	(%r8,%r13,2), %r11d
	leal	1(%r13), %eax
	movl	%r13d, %esi
	subq	$1, %r13
	movl	%r11d, %ecx
	cmpl	$32767, %r11d
	jg	.L580
	sarl	$6, %r11d
	andl	$63, %r11d
	cmpl	$60, %r11d
	jle	.L581
	cltq
	movzwl	(%r8,%rax,2), %r12d
	leaq	(%rax,%rax), %r13
	cmpl	$61, %r11d
	jne	.L582
	movl	%r12d, %r11d
	leal	2(%rsi), %eax
	andl	$32767, %r11d
.L581:
	movl	%ecx, %r12d
	movl	%r11d, 24(%rdi)
	andl	$63, %ecx
	andl	$63, %r12d
	cmpw	$60, %r12w
	jbe	.L583
	cltq
	leaq	(%rax,%rax), %r13
	movzwl	(%r8,%rax,2), %eax
	cmpw	$61, %r12w
	jne	.L584
	movl	%eax, %ecx
	andl	$32767, %ecx
.L583:
	movl	%ecx, 28(%rdi)
	movl	%esi, 8(%rdi)
.L579:
	testb	%dl, %dl
	je	.L571
	movl	8(%rdi), %esi
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L611:
	subl	$1, %r12d
	movb	$-1, 22(%rdi)
	movl	$1, %eax
	movl	%r12d, 8(%rdi)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L593:
	movzwl	2(%r8,%r13), %r13d
	sall	$15, %eax
	sall	$30, %edx
	andl	$1073709056, %eax
	andl	$1073741824, %edx
	andl	$32767, %r13d
	orl	%r13d, %eax
	orl	%eax, %edx
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L591:
	movzwl	2(%r8,%rsi,2), %r15d
	sall	$15, %r14d
	sall	$30, %eax
	addl	$2, %r13d
	andl	$1073709056, %r14d
	andl	$1073741824, %eax
	andl	$32767, %r15d
	orl	%r15d, %r14d
	orl	%r14d, %eax
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L570:
	movl	%r11d, 24(%rdi)
	movl	%eax, 28(%rdi)
	cmpl	$1, %ecx
	je	.L573
	movl	$1, 16(%rdi)
.L573:
	subl	%eax, %ebx
	subl	%eax, %r9d
	subl	%r11d, %r10d
	movl	$1, %eax
	movl	%r10d, 32(%rdi)
	movl	%ebx, 36(%rdi)
	movl	%r9d, 40(%rdi)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L617:
	movl	%eax, %r11d
	sarl	$6, %r11d
	andl	$63, %r11d
	cmpl	$60, %r11d
	jle	.L575
	cmpl	$61, %r11d
	jne	.L576
	movl	%r12d, 8(%rdi)
	movl	%eax, %r11d
	movl	%r12d, %esi
	andl	$32767, %r11d
.L575:
	movl	%r14d, %eax
	movl	%r14d, %ecx
	movl	%r11d, 24(%rdi)
	andl	$63, %eax
	andl	$63, %ecx
	cmpw	$60, %ax
	jbe	.L577
	movslq	%esi, %r12
	leaq	(%r12,%r12), %r13
	movzwl	(%r8,%r12,2), %r12d
	cmpw	$61, %ax
	jne	.L578
	addl	$1, %esi
	movl	%r12d, %ecx
	movl	%esi, 8(%rdi)
	andl	$32767, %ecx
.L577:
	movl	%ecx, 28(%rdi)
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L584:
	movzwl	2(%r8,%r13), %r12d
	sall	$15, %eax
	sall	$30, %ecx
	andl	$1073709056, %eax
	andl	$1073741824, %ecx
	andl	$32767, %r12d
	orl	%r12d, %eax
	orl	%eax, %ecx
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L582:
	movzwl	2(%r8,%r13), %eax
	sall	$15, %r12d
	sall	$30, %r11d
	andl	$1073709056, %r12d
	andl	$1073741824, %r11d
	andl	$32767, %eax
	orl	%eax, %r12d
	leal	3(%rsi), %eax
	orl	%r12d, %r11d
	jmp	.L581
.L576:
	movzwl	2(%r8,%rcx), %ecx
	sall	$15, %eax
	sall	$30, %r11d
	leal	1(%r12), %esi
	andl	$1073741824, %r11d
	movl	%esi, 8(%rdi)
	andl	$32767, %ecx
	orl	%ecx, %eax
	orl	%eax, %r11d
	jmp	.L575
.L578:
	movzwl	2(%r8,%r13), %eax
	sall	$15, %r12d
	sall	$30, %ecx
	addl	$2, %esi
	andl	$1073709056, %r12d
	andl	$1073741824, %ecx
	movl	%esi, 8(%rdi)
	andl	$32767, %eax
	orl	%eax, %r12d
	orl	%r12d, %ecx
	jmp	.L577
	.cfi_endproc
.LFE2334:
	.size	_ZN6icu_675Edits8Iterator8previousER10UErrorCode, .-_ZN6icu_675Edits8Iterator8previousER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode
	.type	_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode, @function
_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode:
.LFB2335:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L619
	testl	%esi, %esi
	js	.L619
	movsbl	%dl, %edx
	jmp	_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L619:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2335:
	.size	_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode, .-_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits8Iterator31destinationIndexFromSourceIndexEiR10UErrorCode
	.type	_ZN6icu_675Edits8Iterator31destinationIndexFromSourceIndexEiR10UErrorCode, @function
_ZN6icu_675Edits8Iterator31destinationIndexFromSourceIndexEiR10UErrorCode:
.LFB2336:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L627
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	testl	%esi, %esi
	js	.L624
	movq	%rdx, %rcx
	movl	$1, %edx
	call	_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode.part.0
	testl	%eax, %eax
	js	.L624
	movl	40(%rdi), %eax
	jne	.L620
	movl	32(%rdi), %edx
	cmpl	%ebx, %edx
	je	.L620
	cmpb	$0, 23(%rdi)
	je	.L625
	addl	28(%rdi), %eax
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L624:
	xorl	%eax, %eax
.L620:
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	subl	%edx, %ebx
	addl	%ebx, %eax
	jmp	.L620
	.cfi_endproc
.LFE2336:
	.size	_ZN6icu_675Edits8Iterator31destinationIndexFromSourceIndexEiR10UErrorCode, .-_ZN6icu_675Edits8Iterator31destinationIndexFromSourceIndexEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_675Edits8Iterator31sourceIndexFromDestinationIndexEiR10UErrorCode
	.type	_ZN6icu_675Edits8Iterator31sourceIndexFromDestinationIndexEiR10UErrorCode, @function
_ZN6icu_675Edits8Iterator31sourceIndexFromDestinationIndexEiR10UErrorCode:
.LFB2337:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L637
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	testl	%esi, %esi
	js	.L634
	movq	%rdx, %rcx
	xorl	%edx, %edx
	call	_ZN6icu_675Edits8Iterator9findIndexEiaR10UErrorCode.part.0
	testl	%eax, %eax
	js	.L634
	movl	32(%rdi), %eax
	jne	.L630
	movl	40(%rdi), %edx
	cmpl	%ebx, %edx
	je	.L630
	cmpb	$0, 23(%rdi)
	je	.L635
	addl	24(%rdi), %eax
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L634:
	xorl	%eax, %eax
.L630:
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L635:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	subl	%edx, %ebx
	addl	%ebx, %eax
	jmp	.L630
	.cfi_endproc
.LFE2337:
	.size	_ZN6icu_675Edits8Iterator31sourceIndexFromDestinationIndexEiR10UErrorCode, .-_ZN6icu_675Edits8Iterator31sourceIndexFromDestinationIndexEiR10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC2:
	.string	"{"
	.string	" "
	.string	"s"
	.string	"r"
	.string	"c"
	.string	"["
	.string	""
	.string	""
	.align 2
.LC3:
	.string	"."
	.string	"."
	.string	""
	.string	""
	.align 2
.LC4:
	.string	"]"
	.string	" "
	.string	"\335! "
	.string	"d"
	.string	"e"
	.string	"s"
	.string	"t"
	.string	"["
	.string	""
	.string	""
	.align 2
.LC5:
	.string	"]"
	.string	" "
	.string	"a\" "
	.string	"d"
	.string	"e"
	.string	"s"
	.string	"t"
	.string	"["
	.string	""
	.string	""
	.align 2
.LC6:
	.string	"]"
	.string	","
	.string	" "
	.string	"r"
	.string	"e"
	.string	"p"
	.string	"l"
	.string	"["
	.string	""
	.string	""
	.align 2
.LC7:
	.string	"]"
	.string	" "
	.string	"}"
	.string	""
	.string	""
	.section	.rodata.str2.8,"aMS",@progbits,2
	.align 8
.LC8:
	.string	"]"
	.string	" "
	.string	"("
	.string	"n"
	.string	"o"
	.string	"-"
	.string	"c"
	.string	"h"
	.string	"a"
	.string	"n"
	.string	"g"
	.string	"e"
	.string	")"
	.string	" "
	.string	"}"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_675Edits8Iterator8toStringERNS_13UnicodeStringE
	.type	_ZNK6icu_675Edits8Iterator8toStringERNS_13UnicodeStringE, @function
_ZNK6icu_675Edits8Iterator8toStringERNS_13UnicodeStringE:
.LFB2338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC2(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC2(%rip), %rax
	movl	32(%rbx), %esi
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$10, %edx
	leaq	.LC3(%rip), %r13
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$1, %ecx
	movl	24(%rbx), %esi
	movl	$10, %edx
	addl	32(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	xorl	%edx, %edx
	cmpb	$0, 23(%rbx)
	movl	$-1, %ecx
	je	.L641
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC4(%rip), %rax
.L642:
	movl	40(%rbx), %esi
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$10, %edx
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$1, %ecx
	movl	28(%rbx), %esi
	movl	$10, %edx
	addl	40(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	xorl	%edx, %edx
	cmpb	$0, 23(%rbx)
	movl	$-1, %ecx
	je	.L643
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC6(%rip), %rax
	movl	36(%rbx), %esi
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$10, %edx
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$1, %ecx
	movl	28(%rbx), %esi
	movq	%r12, %rdi
	addl	36(%rbx), %esi
	movl	$10, %edx
	call	_ZN6icu_6711ICU_Utility12appendNumberERNS_13UnicodeStringEiii@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC7(%rip), %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC7(%rip), %rax
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC5(%rip), %rax
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L643:
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	.LC8(%rip), %rax
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2338:
	.size	_ZNK6icu_675Edits8Iterator8toStringERNS_13UnicodeStringE, .-_ZNK6icu_675Edits8Iterator8toStringERNS_13UnicodeStringE
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	7
	.align 16
.LC1:
	.long	100
	.long	0
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
