	.file	"cstring.cpp"
	.text
	.p2align 4
	.globl	uprv_isASCIILetter_67
	.type	uprv_isASCIILetter_67, @function
uprv_isASCIILetter_67:
.LFB2053:
	.cfi_startproc
	endbr64
	andl	$-33, %edi
	subl	$65, %edi
	cmpb	$25, %dil
	setbe	%al
	ret
	.cfi_endproc
.LFE2053:
	.size	uprv_isASCIILetter_67, .-uprv_isASCIILetter_67
	.p2align 4
	.globl	uprv_toupper_67
	.type	uprv_toupper_67, @function
uprv_toupper_67:
.LFB2054:
	.cfi_startproc
	endbr64
	leal	-97(%rdi), %edx
	leal	-32(%rdi), %eax
	cmpb	$26, %dl
	cmovnb	%edi, %eax
	ret
	.cfi_endproc
.LFE2054:
	.size	uprv_toupper_67, .-uprv_toupper_67
	.p2align 4
	.globl	uprv_asciitolower_67
	.type	uprv_asciitolower_67, @function
uprv_asciitolower_67:
.LFB2055:
	.cfi_startproc
	endbr64
	leal	-65(%rdi), %edx
	leal	32(%rdi), %eax
	cmpb	$26, %dl
	cmovnb	%edi, %eax
	ret
	.cfi_endproc
.LFE2055:
	.size	uprv_asciitolower_67, .-uprv_asciitolower_67
	.p2align 4
	.globl	uprv_ebcdictolower_67
	.type	uprv_ebcdictolower_67, @function
uprv_ebcdictolower_67:
.LFB2056:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	andl	$-17, %eax
	addl	$63, %eax
	cmpb	$8, %al
	jbe	.L8
	leal	30(%rdi), %edx
	movl	%edi, %eax
	cmpb	$7, %dl
	ja	.L7
.L8:
	leal	-64(%rdi), %eax
.L7:
	ret
	.cfi_endproc
.LFE2056:
	.size	uprv_ebcdictolower_67, .-uprv_ebcdictolower_67
	.p2align 4
	.globl	T_CString_toLowerCase_67
	.type	T_CString_toLowerCase_67, @function
T_CString_toLowerCase_67:
.LFB2057:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	%rdi, %rcx
	testq	%rdi, %rdi
	je	.L18
.L12:
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	leal	-65(%rdx), %esi
	cmpb	$25, %sil
	ja	.L13
.L19:
	addl	$32, %edx
	addq	$1, %rcx
	movb	%dl, -2(%rcx)
	movzbl	-1(%rcx), %edx
	leal	-65(%rdx), %esi
	cmpb	$25, %sil
	jbe	.L19
.L13:
	testb	%dl, %dl
	jne	.L12
.L18:
	ret
	.cfi_endproc
.LFE2057:
	.size	T_CString_toLowerCase_67, .-T_CString_toLowerCase_67
	.p2align 4
	.globl	T_CString_toUpperCase_67
	.type	T_CString_toUpperCase_67, @function
T_CString_toUpperCase_67:
.LFB2058:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	%rdi, %rcx
	testq	%rdi, %rdi
	je	.L27
.L21:
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	leal	-97(%rdx), %esi
	cmpb	$25, %sil
	ja	.L22
.L28:
	subl	$32, %edx
	addq	$1, %rcx
	movb	%dl, -2(%rcx)
	movzbl	-1(%rcx), %edx
	leal	-97(%rdx), %esi
	cmpb	$25, %sil
	jbe	.L28
.L22:
	testb	%dl, %dl
	jne	.L21
.L27:
	ret
	.cfi_endproc
.LFE2058:
	.size	T_CString_toUpperCase_67, .-T_CString_toUpperCase_67
	.p2align 4
	.globl	T_CString_integerToString_67
	.type	T_CString_integerToString_67, @function
T_CString_integerToString_67:
.LFB2059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jns	.L38
	cmpl	$10, %edx
	jne	.L38
	movb	$45, (%rdi)
	negl	%esi
	addq	$1, %rdi
	movl	$1, %ebx
.L32:
	movl	%esi, %eax
	xorl	%edx, %edx
	movl	$28, %ecx
	movb	$0, -35(%rbp)
	divl	%r8d
	leaq	-64(%rbp), %r9
	movl	%ecx, %r12d
	cmpb	$9, %dl
	ja	.L33
	.p2align 4,,10
	.p2align 3
.L40:
	addl	$48, %edx
	movb	%dl, (%r9,%rcx)
	subq	$1, %rcx
	cmpl	%esi, %r8d
	ja	.L34
.L35:
	movl	%eax, %esi
	xorl	%edx, %edx
	movl	%ecx, %r12d
	movl	%esi, %eax
	divl	%r8d
	cmpb	$9, %dl
	jbe	.L40
.L33:
	addl	$55, %edx
	movb	%dl, (%r9,%rcx)
	subq	$1, %rcx
	cmpl	%esi, %r8d
	jbe	.L35
.L34:
	movslq	%r12d, %rsi
	addq	%r9, %rsi
	call	strcpy@PLT
	leal	29(%rbx), %eax
	subl	%r12d, %eax
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L41
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L32
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2059:
	.size	T_CString_integerToString_67, .-T_CString_integerToString_67
	.p2align 4
	.globl	T_CString_int64ToString_67
	.type	T_CString_int64ToString_67, @function
T_CString_int64ToString_67:
.LFB2060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jns	.L51
	cmpl	$10, %edx
	jne	.L51
	movb	$45, (%rdi)
	negq	%rsi
	addq	$1, %rdi
	movl	$1, %ebx
.L45:
	movb	$0, -35(%rbp)
	movl	%edx, %r8d
	movl	$28, %ecx
	leaq	-64(%rbp), %r9
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L53:
	addl	$48, %edx
	movb	%dl, (%r9,%rcx)
	subq	$1, %rcx
	cmpq	%rsi, %r8
	ja	.L47
.L48:
	movq	%rax, %rsi
.L49:
	movq	%rsi, %rax
	xorl	%edx, %edx
	movl	%ecx, %r12d
	divq	%r8
	cmpb	$9, %dl
	jbe	.L53
	addl	$55, %edx
	movb	%dl, (%r9,%rcx)
	subq	$1, %rcx
	cmpq	%rsi, %r8
	jbe	.L48
.L47:
	movslq	%r12d, %rsi
	addq	%r9, %rsi
	call	strcpy@PLT
	leal	29(%rbx), %eax
	subl	%r12d, %eax
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L54
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	xorl	%ebx, %ebx
	jmp	.L45
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2060:
	.size	T_CString_int64ToString_67, .-T_CString_int64ToString_67
	.p2align 4
	.globl	T_CString_stringToInteger_67
	.type	T_CString_stringToInteger_67, @function
T_CString_stringToInteger_67:
.LFB2061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rsi
	call	strtoul@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L58
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L58:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2061:
	.size	T_CString_stringToInteger_67, .-T_CString_stringToInteger_67
	.p2align 4
	.globl	uprv_stricmp_67
	.type	uprv_stricmp_67, @function
uprv_stricmp_67:
.LFB2062:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L75
	testq	%rsi, %rsi
	je	.L70
	movzbl	(%rdi), %edx
	movzbl	(%rsi), %ecx
	movl	$1, %r8d
	testb	%dl, %dl
	jne	.L62
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L76:
	leal	-65(%rdx), %r9d
	leal	32(%rdx), %eax
	cmpb	$26, %r9b
	leal	-65(%rcx), %r9d
	cmovnb	%edx, %eax
	leal	32(%rcx), %edx
	cmpb	$26, %r9b
	cmovnb	%ecx, %edx
	movzbl	%al, %eax
	movzbl	%dl, %edx
	subl	%edx, %eax
	jne	.L59
	movzbl	(%rdi,%r8), %edx
	movzbl	(%rsi,%r8), %ecx
	addq	$1, %r8
	testb	%dl, %dl
	je	.L67
.L62:
	testb	%cl, %cl
	jne	.L76
.L70:
	movl	$1, %eax
.L59:
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	setne	%al
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%eax, %eax
	testb	%cl, %cl
	setne	%al
	negl	%eax
	ret
	.cfi_endproc
.LFE2062:
	.size	uprv_stricmp_67, .-uprv_stricmp_67
	.p2align 4
	.globl	uprv_strnicmp_67
	.type	uprv_strnicmp_67, @function
uprv_strnicmp_67:
.LFB2063:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L88
	testq	%rsi, %rsi
	je	.L86
	leal	-1(%rdx), %r10d
	testl	%edx, %edx
	je	.L85
	xorl	%ecx, %ecx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L80:
	testb	%dl, %dl
	je	.L86
	leal	-65(%rax), %r9d
	leal	32(%rax), %r8d
	cmpb	$26, %r9b
	leal	-65(%rdx), %r9d
	cmovb	%r8d, %eax
	leal	32(%rdx), %r8d
	cmpb	$26, %r9b
	cmovb	%r8d, %edx
	movzbl	%al, %eax
	movzbl	%dl, %edx
	subl	%edx, %eax
	jne	.L77
	leaq	1(%rcx), %rdx
	cmpq	%r10, %rcx
	je	.L89
	movq	%rdx, %rcx
.L83:
	movzbl	(%rdi,%rcx), %eax
	movzbl	(%rsi,%rcx), %edx
	testb	%al, %al
	jne	.L80
	xorl	%eax, %eax
	testb	%dl, %dl
	setne	%al
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$1, %eax
.L77:
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	setne	%al
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	ret
.L85:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2063:
	.size	uprv_strnicmp_67, .-uprv_strnicmp_67
	.p2align 4
	.globl	uprv_strdup_67
	.type	uprv_strdup_67, @function
uprv_strdup_67:
.LFB2064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	strlen@PLT
	leaq	1(%rax), %r13
	movq	%r13, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L90
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
.L90:
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2064:
	.size	uprv_strdup_67, .-uprv_strdup_67
	.p2align 4
	.globl	uprv_strndup_67
	.type	uprv_strndup_67, @function
uprv_strndup_67:
.LFB2065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movslq	%esi, %rbx
	subq	$8, %rsp
	testl	%ebx, %ebx
	js	.L106
	leal	1(%rbx), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L96
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movb	$0, (%rax,%rbx)
	movq	%rax, %r8
.L96:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	call	strlen@PLT
	leaq	1(%rax), %r13
	movq	%r13, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L96
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	addq	$8, %rsp
	movq	%rax, %r8
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2065:
	.size	uprv_strndup_67, .-uprv_strndup_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
