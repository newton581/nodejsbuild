	.file	"rbbisetb.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714RBBISetBuilderC2EPNS_15RBBIRuleBuilderE
	.type	_ZN6icu_6714RBBISetBuilderC2EPNS_15RBBIRuleBuilderE, @function
_ZN6icu_6714RBBISetBuilderC2EPNS_15RBBIRuleBuilderE:
.LFB3144:
	.cfi_startproc
	endbr64
	movq	16(%rsi), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rdi)
	movq	$0, 32(%rdi)
	movq	%rax, 8(%rdi)
	movb	$0, 40(%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE3144:
	.size	_ZN6icu_6714RBBISetBuilderC2EPNS_15RBBIRuleBuilderE, .-_ZN6icu_6714RBBISetBuilderC2EPNS_15RBBIRuleBuilderE
	.globl	_ZN6icu_6714RBBISetBuilderC1EPNS_15RBBIRuleBuilderE
	.set	_ZN6icu_6714RBBISetBuilderC1EPNS_15RBBIRuleBuilderE,_ZN6icu_6714RBBISetBuilderC2EPNS_15RBBIRuleBuilderE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714RBBISetBuilderD2Ev
	.type	_ZN6icu_6714RBBISetBuilderD2Ev, @function
_ZN6icu_6714RBBISetBuilderD2Ev:
.LFB3147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %r12
	testq	%r12, %r12
	jne	.L7
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	testq	%rbx, %rbx
	je	.L4
.L6:
	movq	%rbx, %r12
.L7:
	movq	16(%r12), %rdi
	movq	24(%r12), %rbx
	testq	%rdi, %rdi
	jne	.L18
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	testq	%rbx, %rbx
	jne	.L6
.L4:
	movq	24(%r13), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	utrie2_close_67@PLT
	.cfi_endproc
.LFE3147:
	.size	_ZN6icu_6714RBBISetBuilderD2Ev, .-_ZN6icu_6714RBBISetBuilderD2Ev
	.globl	_ZN6icu_6714RBBISetBuilderD1Ev
	.set	_ZN6icu_6714RBBISetBuilderD1Ev,_ZN6icu_6714RBBISetBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714RBBISetBuilder9buildTrieEv
	.type	_ZN6icu_6714RBBISetBuilder9buildTrieEv, @function
_ZN6icu_6714RBBISetBuilder9buildTrieEv:
.LFB3150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rdi), %rdx
	xorl	%edi, %edi
	call	utrie2_open_67@PLT
	movq	16(%r12), %rbx
	movq	%rax, 24(%r12)
	testq	%rbx, %rbx
	jne	.L21
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L27:
	movl	8(%rbx), %ecx
	movl	4(%rbx), %edx
	movl	$1, %r8d
	movl	(%rbx), %esi
	movq	24(%r12), %rdi
	call	utrie2_setRange32_67@PLT
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L19
.L21:
	movq	8(%r12), %r9
	movl	(%r9), %eax
	testl	%eax, %eax
	jle	.L27
.L19:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3150:
	.size	_ZN6icu_6714RBBISetBuilder9buildTrieEv, .-_ZN6icu_6714RBBISetBuilder9buildTrieEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714RBBISetBuilder15mergeCategoriesESt4pairIiiE
	.type	_ZN6icu_6714RBBISetBuilder15mergeCategoriesESt4pairIiiE, @function
_ZN6icu_6714RBBISetBuilder15mergeCategoriesESt4pairIiiE:
.LFB3151:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	shrq	$32, %rax
	movq	%rax, %r8
	movq	16(%rdi), %rax
	testq	%rax, %rax
	jne	.L32
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	jle	.L31
	subl	$1, %edx
	movl	%edx, 8(%rax)
.L31:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L29
.L32:
	movl	8(%rax), %edx
	movl	%edx, %ecx
	andb	$-65, %ch
	cmpl	%r8d, %ecx
	jne	.L30
	andl	$16384, %edx
	orl	%esi, %edx
	movl	%edx, 8(%rax)
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L32
.L29:
	subl	$1, 36(%rdi)
	ret
	.cfi_endproc
.LFE3151:
	.size	_ZN6icu_6714RBBISetBuilder15mergeCategoriesESt4pairIiiE, .-_ZN6icu_6714RBBISetBuilder15mergeCategoriesESt4pairIiiE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714RBBISetBuilder11getTrieSizeEv
	.type	_ZN6icu_6714RBBISetBuilder11getTrieSizeEv, @function
_ZN6icu_6714RBBISetBuilder11getTrieSizeEv:
.LFB3152:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L44
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	utrie2_freeze_67@PLT
	movq	8(%rbx), %rcx
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	utrie2_serialize_67@PLT
	movq	8(%rbx), %rdx
	movl	%eax, 32(%rbx)
	cmpl	$15, (%rdx)
	jne	.L37
	movl	$0, (%rdx)
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3152:
	.size	_ZN6icu_6714RBBISetBuilder11getTrieSizeEv, .-_ZN6icu_6714RBBISetBuilder11getTrieSizeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714RBBISetBuilder13serializeTrieEPh
	.type	_ZN6icu_6714RBBISetBuilder13serializeTrieEPh, @function
_ZN6icu_6714RBBISetBuilder13serializeTrieEPh:
.LFB3153:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rcx
	movl	32(%rdi), %edx
	movq	24(%rdi), %rdi
	jmp	utrie2_serialize_67@PLT
	.cfi_endproc
.LFE3153:
	.size	_ZN6icu_6714RBBISetBuilder13serializeTrieEPh, .-_ZN6icu_6714RBBISetBuilder13serializeTrieEPh
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714RBBISetBuilder12addValToSetsEPNS_7UVectorEj
	.type	_ZN6icu_6714RBBISetBuilder12addValToSetsEPNS_7UVectorEj, @function
_ZN6icu_6714RBBISetBuilder12addValToSetsEPNS_7UVectorEj:
.LFB3154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rsi), %eax
	movq	%rdi, -64(%rbp)
	testl	%eax, %eax
	jle	.L46
	movzwl	%dx, %eax
	movq	%rsi, %r13
	xorl	%r12d, %r12d
	movl	%eax, -52(%rbp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L48
	movl	$9, %esi
	movq	%rax, %rdi
	call	_ZN6icu_678RBBINodeC1ENS0_8NodeTypeE@PLT
	movq	16(%rbx), %rcx
	movq	%r14, 24(%r15)
	movq	%rcx, 16(%r15)
	movq	%r15, 8(%rcx)
	movq	%r15, 8(%r14)
	movq	%r15, 16(%rbx)
	movq	%rbx, 8(%r15)
.L50:
	addl	$1, %r12d
	cmpl	8(%r13), %r12d
	jge	.L46
.L52:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	$160, %edi
	movq	%rax, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L48
	movq	%rax, %rdi
	movl	$3, %esi
	call	_ZN6icu_678RBBINodeC1ENS0_8NodeTypeE@PLT
	movl	-52(%rbp), %eax
	cmpq	$0, 16(%rbx)
	movl	%eax, 124(%r14)
	jne	.L49
	movq	%r14, 16(%rbx)
	addl	$1, %r12d
	movq	%rbx, 8(%r14)
	cmpl	8(%r13), %r12d
	jl	.L52
.L46:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L48:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movq	8(%rax), %rax
	movl	$7, (%rax)
	jmp	.L50
	.cfi_endproc
.LFE3154:
	.size	_ZN6icu_6714RBBISetBuilder12addValToSetsEPNS_7UVectorEj, .-_ZN6icu_6714RBBISetBuilder12addValToSetsEPNS_7UVectorEj
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714RBBISetBuilder11addValToSetEPNS_8RBBINodeEj
	.type	_ZN6icu_6714RBBISetBuilder11addValToSetEPNS_8RBBINodeEj, @function
_ZN6icu_6714RBBISetBuilder11addValToSetEPNS_8RBBINodeEj:
.LFB3155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$160, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L56
	movl	$3, %esi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN6icu_678RBBINodeC1ENS0_8NodeTypeE@PLT
	movzwl	%r14w, %edx
	cmpq	$0, 16(%r12)
	movl	%edx, 124(%rbx)
	je	.L61
	movl	$160, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L56
	movq	%rax, %rdi
	movl	$9, %esi
	call	_ZN6icu_678RBBINodeC1ENS0_8NodeTypeE@PLT
	movq	16(%r12), %rax
	movq	%rbx, 24(%r14)
	movq	%rax, 16(%r14)
	movq	%r14, 8(%rax)
	movq	%r14, 8(%rbx)
	movq	%r14, 16(%r12)
	movq	%r12, 8(%r14)
.L55:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%rbx, 16(%r12)
	movq	%r12, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L56:
	.cfi_restore_state
	movq	8(%r13), %rax
	movl	$7, (%rax)
	jmp	.L55
	.cfi_endproc
.LFE3155:
	.size	_ZN6icu_6714RBBISetBuilder11addValToSetEPNS_8RBBINodeEj, .-_ZN6icu_6714RBBISetBuilder11addValToSetEPNS_8RBBINodeEj
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv
	.type	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv, @function
_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv:
.LFB3156:
	.cfi_startproc
	endbr64
	movl	36(%rdi), %eax
	addl	$3, %eax
	ret
	.cfi_endproc
.LFE3156:
	.size	_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv, .-_ZNK6icu_6714RBBISetBuilder20getNumCharCategoriesEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714RBBISetBuilder6sawBOFEv
	.type	_ZNK6icu_6714RBBISetBuilder6sawBOFEv, @function
_ZNK6icu_6714RBBISetBuilder6sawBOFEv:
.LFB3157:
	.cfi_startproc
	endbr64
	movzbl	40(%rdi), %eax
	ret
	.cfi_endproc
.LFE3157:
	.size	_ZNK6icu_6714RBBISetBuilder6sawBOFEv, .-_ZNK6icu_6714RBBISetBuilder6sawBOFEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714RBBISetBuilder12getFirstCharEi
	.type	_ZNK6icu_6714RBBISetBuilder12getFirstCharEi, @function
_ZNK6icu_6714RBBISetBuilder12getFirstCharEi:
.LFB3158:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	jne	.L67
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L66:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L68
.L67:
	cmpl	%esi, 8(%rax)
	jne	.L66
	movl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3158:
	.size	_ZNK6icu_6714RBBISetBuilder12getFirstCharEi, .-_ZNK6icu_6714RBBISetBuilder12getFirstCharEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RangeDescriptorC2ERKS0_R10UErrorCode
	.type	_ZN6icu_6715RangeDescriptorC2ERKS0_R10UErrorCode, @function
_ZN6icu_6715RangeDescriptorC2ERKS0_R10UErrorCode:
.LFB3160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %eax
	movq	(%rsi), %rdx
	movq	$0, 24(%rdi)
	movl	0(%r13), %ebx
	movq	%rdx, (%rdi)
	movl	%eax, 8(%rdi)
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L71
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
.L71:
	movq	%r15, 16(%r12)
	testl	%ebx, %ebx
	jle	.L72
	movl	%ebx, 0(%r13)
.L70:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L70
	testq	%r15, %r15
	je	.L82
	movq	16(%r14), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L70
	xorl	%ebx, %ebx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L83:
	movq	16(%r12), %r15
.L76:
	movl	%ebx, %esi
	addl	$1, %ebx
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	16(%r14), %rdi
	cmpl	%ebx, 8(%rdi)
	jg	.L83
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L70
	.cfi_endproc
.LFE3160:
	.size	_ZN6icu_6715RangeDescriptorC2ERKS0_R10UErrorCode, .-_ZN6icu_6715RangeDescriptorC2ERKS0_R10UErrorCode
	.globl	_ZN6icu_6715RangeDescriptorC1ERKS0_R10UErrorCode
	.set	_ZN6icu_6715RangeDescriptorC1ERKS0_R10UErrorCode,_ZN6icu_6715RangeDescriptorC2ERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RangeDescriptorC2ER10UErrorCode
	.type	_ZN6icu_6715RangeDescriptorC2ER10UErrorCode, @function
_ZN6icu_6715RangeDescriptorC2ER10UErrorCode:
.LFB3163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	(%rsi), %r14d
	movq	$0, (%rdi)
	movl	$0, 8(%rdi)
	movq	$0, 24(%rdi)
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L85
	movq	%rax, %r12
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	%r12, 16(%rbx)
	testl	%r14d, %r14d
	jle	.L84
.L86:
	movl	%r14d, 0(%r13)
.L84:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	movq	$0, 16(%rbx)
	testl	%r14d, %r14d
	jg	.L86
	cmpl	$0, 0(%r13)
	jg	.L84
	movl	$7, 0(%r13)
	jmp	.L84
	.cfi_endproc
.LFE3163:
	.size	_ZN6icu_6715RangeDescriptorC2ER10UErrorCode, .-_ZN6icu_6715RangeDescriptorC2ER10UErrorCode
	.globl	_ZN6icu_6715RangeDescriptorC1ER10UErrorCode
	.set	_ZN6icu_6715RangeDescriptorC1ER10UErrorCode,_ZN6icu_6715RangeDescriptorC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RangeDescriptorD2Ev
	.type	_ZN6icu_6715RangeDescriptorD2Ev, @function
_ZN6icu_6715RangeDescriptorD2Ev:
.LFB3166:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L90
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L90:
	ret
	.cfi_endproc
.LFE3166:
	.size	_ZN6icu_6715RangeDescriptorD2Ev, .-_ZN6icu_6715RangeDescriptorD2Ev
	.globl	_ZN6icu_6715RangeDescriptorD1Ev
	.set	_ZN6icu_6715RangeDescriptorD1Ev,_ZN6icu_6715RangeDescriptorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RangeDescriptor5splitEiR10UErrorCode
	.type	_ZN6icu_6715RangeDescriptor5splitEiR10UErrorCode, @function
_ZN6icu_6715RangeDescriptor5splitEiR10UErrorCode:
.LFB3168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$32, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movl	%esi, -52(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L93
	movq	%rax, %r12
	movq	(%r14), %rdx
	movl	8(%r14), %eax
	movl	$40, %edi
	movq	$0, 24(%r12)
	movl	(%rbx), %r15d
	movq	%rdx, (%r12)
	movl	%eax, 8(%r12)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L94
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
.L94:
	movq	%r13, 16(%r12)
	testl	%r15d, %r15d
	jle	.L95
	movl	%r15d, (%rbx)
.L98:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L102
	movq	(%rdi), %rax
	call	*8(%rax)
.L102:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L98
	testq	%r13, %r13
	je	.L111
	movq	16(%r14), %rdi
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jle	.L99
	xorl	%r15d, %r15d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L100:
	movq	16(%r12), %r13
.L101:
	movl	%r15d, %esi
	addl	$1, %r15d
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	16(%r14), %rdi
	cmpl	%r15d, 8(%rdi)
	jg	.L100
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L98
.L99:
	movl	-52(%rbp), %eax
	movl	%eax, (%r12)
	subl	$1, %eax
	movl	%eax, 4(%r14)
	movq	24(%r14), %rax
	movq	%rax, 24(%r12)
	movq	%r12, 24(%r14)
.L92:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L93:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L92
.L111:
	movl	$7, (%rbx)
	jmp	.L98
	.cfi_endproc
.LFE3168:
	.size	_ZN6icu_6715RangeDescriptor5splitEiR10UErrorCode, .-_ZN6icu_6715RangeDescriptor5splitEiR10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC0:
	.string	"d"
	.string	"i"
	.string	"c"
	.string	"t"
	.string	"i"
	.string	"o"
	.string	"n"
	.string	"a"
	.string	"r"
	.string	"y"
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RangeDescriptor17setDictionaryFlagEv
	.type	_ZN6icu_6715RangeDescriptor17setDictionaryFlagEv, @function
_ZN6icu_6715RangeDescriptor17setDictionaryFlagEv:
.LFB3169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rdi
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L112
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %r13
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L114:
	movq	16(%r12), %rdi
	addl	$1, %ebx
	cmpl	8(%rdi), %ebx
	jge	.L112
.L118:
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L114
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L114
	cmpl	$2, (%rax)
	jne	.L114
	movswl	56(%rax), %edx
	leaq	48(%rax), %rdi
	testw	%dx, %dx
	js	.L115
	sarl	$5, %edx
.L116:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	jne	.L114
	orl	$16384, 8(%r12)
.L112:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movl	60(%rax), %edx
	jmp	.L116
	.cfi_endproc
.LFE3169:
	.size	_ZN6icu_6715RangeDescriptor17setDictionaryFlagEv, .-_ZN6icu_6715RangeDescriptor17setDictionaryFlagEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714RBBISetBuilder11buildRangesEv
	.type	_ZN6icu_6714RBBISetBuilder11buildRangesEv, @function
_ZN6icu_6714RBBISetBuilder11buildRangesEv:
.LFB3149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L128
	movq	8(%rbx), %r15
	movq	$0, (%rax)
	movl	$40, %edi
	movq	%rax, %r12
	movl	$0, 8(%rax)
	movq	$0, 24(%rax)
	movl	(%r15), %r14d
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L129
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_677UVectorC1ER10UErrorCode@PLT
	movq	%r13, 16(%r12)
	testl	%r14d, %r14d
	jle	.L131
.L132:
	movl	%r14d, (%r15)
.L131:
	movabsq	$4785070309113856, %rax
	movq	%r12, 16(%rbx)
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L127
	movl	$0, -216(%rbp)
.L147:
	movq	(%rbx), %rax
	movl	-216(%rbp), %esi
	movq	168(%rax), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L133
	movq	32(%rax), %r13
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movq	16(%rbx), %r15
	movl	%eax, -212(%rbp)
	testl	%eax, %eax
	jle	.L134
	xorl	%r12d, %r12d
.L135:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%eax, -200(%rbp)
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	-200(%rbp), %r8d
	movl	%eax, %r14d
	movl	4(%r15), %eax
	cmpl	%eax, %r8d
	jle	.L136
	.p2align 4,,10
	.p2align 3
.L137:
	movq	24(%r15), %r15
	movl	4(%r15), %eax
	cmpl	%r8d, %eax
	jl	.L137
.L136:
	cmpl	%r8d, (%r15)
	jl	.L177
	cmpl	%eax, %r14d
	jge	.L144
	movq	8(%rbx), %rdx
	leal	1(%r14), %esi
	movq	%r15, %rdi
	call	_ZN6icu_6715RangeDescriptor5splitEiR10UErrorCode
	movq	8(%rbx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L127
.L144:
	movq	16(%r15), %rdi
	movq	-208(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	cmpl	$-1, %eax
	je	.L178
.L143:
	xorl	%eax, %eax
	cmpl	%r14d, 4(%r15)
	movq	24(%r15), %r15
	sete	%al
	addl	%eax, %r12d
.L140:
	cmpl	-212(%rbp), %r12d
	jl	.L135
.L134:
	addl	$1, -216(%rbp)
	jmp	.L147
.L158:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	movl	%r8d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6715RangeDescriptor5splitEiR10UErrorCode
	movq	8(%rbx), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L127
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L178:
	movq	8(%rbx), %rdx
	movq	16(%r15), %rdi
	movq	-208(%rbp), %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movq	8(%rbx), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L143
	jmp	.L127
.L133:
	movq	16(%rbx), %r13
	testq	%r13, %r13
	je	.L156
.L151:
	movl	8(%r13), %eax
.L150:
	testl	%eax, %eax
	je	.L180
.L155:
	movq	24(%r13), %r13
	testq	%r13, %r13
	je	.L156
	movq	16(%rbx), %r12
	cmpq	%r13, %r12
	jne	.L154
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L152:
	movq	24(%r12), %r12
	cmpq	%r13, %r12
	je	.L151
.L154:
	movq	16(%r12), %rsi
	movq	16(%r13), %rdi
	call	_ZNK6icu_677UVector6equalsERKS0_@PLT
	testb	%al, %al
	je	.L152
	movl	8(%r12), %eax
	movl	%eax, 8(%r13)
	jmp	.L150
.L180:
	movl	36(%rbx), %eax
	movq	%r13, %rdi
	leal	1(%rax), %edx
	addl	$3, %eax
	movl	%edx, 36(%rbx)
	movl	%eax, 8(%r13)
	call	_ZN6icu_6715RangeDescriptor17setDictionaryFlagEv
	movl	36(%rbx), %eax
	movq	16(%r13), %rsi
	movq	%rbx, %rdi
	leal	2(%rax), %edx
	call	_ZN6icu_6714RBBISetBuilder12addValToSetsEPNS_7UVectorEj
	jmp	.L155
.L156:
	leaq	-192(%rbp), %r14
	leaq	_ZZN6icu_6714RBBISetBuilder11buildRangesEvE10eofUString(%rip), %rsi
	xorl	%r13d, %r13d
	movq	%r14, %rdi
	leaq	-128(%rbp), %r15
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	leaq	_ZZN6icu_6714RBBISetBuilder11buildRangesEvE10bofUString(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDs@PLT
	jmp	.L149
.L159:
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE@PLT
	testb	%al, %al
	jne	.L181
.L160:
	addl	$1, %r13d
.L149:
	movq	(%rbx), %rax
	movl	%r13d, %esi
	movq	168(%rax), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L158
	movq	32(%rax), %r8
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r8, -200(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsERKNS_13UnicodeStringE@PLT
	movq	-200(%rbp), %r8
	testb	%al, %al
	je	.L159
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714RBBISetBuilder11addValToSetEPNS_8RBBINodeEj
	movq	-200(%rbp), %r8
	jmp	.L159
.L181:
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6714RBBISetBuilder11addValToSetEPNS_8RBBINodeEj
	movb	$1, 40(%rbx)
	jmp	.L160
.L129:
	movq	$0, 16(%r12)
	testl	%r14d, %r14d
	jg	.L132
	cmpl	$0, (%r15)
	jg	.L131
	movl	$7, (%r15)
	jmp	.L131
.L128:
	movq	8(%rbx), %rax
	movq	$0, 16(%rbx)
	movl	$7, (%rax)
	jmp	.L127
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3149:
	.size	_ZN6icu_6714RBBISetBuilder11buildRangesEv, .-_ZN6icu_6714RBBISetBuilder11buildRangesEv
	.section	.rodata
	.align 8
	.type	_ZZN6icu_6714RBBISetBuilder11buildRangesEvE10bofUString, @object
	.size	_ZZN6icu_6714RBBISetBuilder11buildRangesEvE10bofUString, 8
_ZZN6icu_6714RBBISetBuilder11buildRangesEvE10bofUString:
	.value	98
	.value	111
	.value	102
	.value	0
	.align 8
	.type	_ZZN6icu_6714RBBISetBuilder11buildRangesEvE10eofUString, @object
	.size	_ZZN6icu_6714RBBISetBuilder11buildRangesEvE10eofUString, 8
_ZZN6icu_6714RBBISetBuilder11buildRangesEvE10eofUString:
	.value	101
	.value	111
	.value	102
	.value	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
