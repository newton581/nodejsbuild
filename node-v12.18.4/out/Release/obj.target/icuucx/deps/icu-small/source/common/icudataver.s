	.file	"icudataver.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"icuver"
.LC1:
	.string	"DataVersion"
	.text
	.p2align 4
	.globl	u_getDataVersion_67
	.type	u_getDataVersion_67, @function
u_getDataVersion_67:
.LFB2319:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	testq	%rdi, %rdi
	jne	.L15
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	%rsi, %r12
	movq	%rsi, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	ures_openDirect_67@PLT
	movq	%rax, %r14
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L16
.L3:
	addq	$8, %rsp
	movq	%r14, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	ures_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	%r12, %rcx
	movq	%r13, %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	ures_getVersionByKey_67@PLT
	jmp	.L3
	.cfi_endproc
.LFE2319:
	.size	u_getDataVersion_67, .-u_getDataVersion_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
