	.file	"ucnvsel.cpp"
	.text
	.p2align 4
	.type	ucnvsel_count_encodings, @function
ucnvsel_count_encodings:
.LFB3095:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1
	movq	8(%rdi), %rax
	movswl	8(%rax), %eax
.L1:
	ret
	.cfi_endproc
.LFE3095:
	.size	ucnvsel_count_encodings, .-ucnvsel_count_encodings
	.p2align 4
	.type	ucnvsel_reset_iterator, @function
ucnvsel_reset_iterator:
.LFB3097:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L5
	movq	8(%rdi), %rax
	xorl	%edx, %edx
	movw	%dx, 10(%rax)
.L5:
	ret
	.cfi_endproc
.LFE3097:
	.size	ucnvsel_reset_iterator, .-ucnvsel_reset_iterator
	.p2align 4
	.type	ucnvsel_next_encoding, @function
ucnvsel_next_encoding:
.LFB3096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L7
	movq	8(%rdi), %rax
	movzwl	10(%rax), %edx
	cmpw	%dx, 8(%rax)
	jle	.L7
	movq	16(%rax), %rcx
	movq	%rsi, %rbx
	movq	(%rax), %rsi
	movswq	%dx, %rdi
	addl	$1, %edx
	movswq	(%rsi,%rdi,2), %rsi
	movq	24(%rcx), %rcx
	movq	(%rcx,%rsi,8), %r12
	movw	%dx, 10(%rax)
	testq	%rbx, %rbx
	je	.L7
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, (%rbx)
.L7:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3096:
	.size	ucnvsel_next_encoding, .-ucnvsel_next_encoding
	.p2align 4
	.type	ucnvsel_close_selector_iterator, @function
ucnvsel_close_selector_iterator:
.LFB3094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	(%rax), %rdi
	call	uprv_free_67@PLT
	movq	8(%r12), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3094:
	.size	ucnvsel_close_selector_iterator, .-ucnvsel_close_selector_iterator
	.p2align 4
	.type	_ZL13selectForMaskPK18UConverterSelectorPjP10UErrorCode, @function
_ZL13selectForMaskPK18UConverterSelectorPjP10UErrorCode:
.LFB3100:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L43
	movq	$0, (%rax)
	movl	$56, %edi
	movl	$0, 8(%rax)
	movq	%rbx, 16(%rax)
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L44
	movdqa	_ZL16defaultEncodings(%rip), %xmm0
	movdqa	16+_ZL16defaultEncodings(%rip), %xmm1
	movdqa	32+_ZL16defaultEncodings(%rip), %xmm2
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	48+_ZL16defaultEncodings(%rip), %rax
	movq	%rax, 48(%r12)
	movl	32(%rbx), %eax
	movl	%eax, %edx
	leal	62(%rax), %r14d
	addl	$31, %edx
	cmovns	%edx, %r14d
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	sarl	$5, %r14d
	testl	%eax, %eax
	jle	.L25
	.p2align 4,,10
	.p2align 3
.L22:
	movl	(%r15,%rsi,4), %eax
	testl	%eax, %eax
	je	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	leal	-1(%rax), %edx
	addl	$1, %ecx
	andl	%edx, %eax
	jne	.L24
.L23:
	addq	$1, %rsi
	cmpl	%esi, %r14d
	jg	.L22
	movswq	%cx, %rax
	testw	%cx, %cx
	jg	.L45
.L25:
	movq	%r13, 8(%r12)
	xorl	%edi, %edi
	xorl	%r13d, %r13d
.L21:
	call	uprv_free_67@PLT
.L19:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L45:
	.cfi_restore_state
	leaq	(%rax,%rax), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L46
	movl	32(%rbx), %edi
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L30:
	movl	(%r15,%r11,4), %edx
	leal	32(%rax), %esi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L47:
	testb	$1, %dl
	je	.L28
	movswq	8(%r13), %rcx
	leal	1(%rcx), %r10d
	movw	%ax, (%r9,%rcx,2)
	movw	%r10w, 8(%r13)
.L28:
	addl	$1, %eax
	shrl	%edx
	cmpw	%si, %ax
	je	.L27
.L29:
	movswl	%ax, %ecx
	cmpl	%edi, %ecx
	jl	.L47
.L27:
	addq	$1, %r11
	cmpl	%r11d, %r14d
	jg	.L30
	jmp	.L25
.L46:
	movq	-56(%rbp), %rax
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	movl	$7, (%rax)
	jmp	.L21
.L44:
	movq	-56(%rbp), %rax
	xorl	%edi, %edi
	movl	$7, (%rax)
	jmp	.L21
.L43:
	movq	-56(%rbp), %rax
	xorl	%r12d, %r12d
	movl	$7, (%rax)
	jmp	.L19
	.cfi_endproc
.LFE3100:
	.size	_ZL13selectForMaskPK18UConverterSelectorPjP10UErrorCode, .-_ZL13selectForMaskPK18UConverterSelectorPjP10UErrorCode
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"ucnvsel_swap(): data format %02x.%02x.%02x.%02x is not recognized as UConverterSelector data\n"
	.align 8
.LC1:
	.string	"ucnvsel_swap(): format version %02x is not supported\n"
	.align 8
.LC2:
	.string	"ucnvsel_swap(): too few bytes (%d after header) for UConverterSelector data\n"
	.align 8
.LC3:
	.string	"ucnvsel_swap(): too few bytes (%d after header) for all of UConverterSelector data\n"
	.text
	.p2align 4
	.type	_ZL12ucnvsel_swapPK12UDataSwapperPKviPvP10UErrorCode, @function
_ZL12ucnvsel_swapPK12UDataSwapperPKviPvP10UErrorCode:
.LFB3092:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$120, %rsp
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	udata_swapDataHeader_67@PLT
	xorl	%r8d, %r8d
	movl	%eax, -140(%rbp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L48
	cmpw	$21315, 12(%r15)
	jne	.L50
	cmpw	$27749, 14(%r15)
	jne	.L50
	movzbl	16(%r15), %edx
	cmpb	$1, %dl
	jne	.L62
	testl	%r12d, %r12d
	js	.L53
	subl	-140(%rbp), %r12d
	cmpl	$63, %r12d
	jle	.L63
.L53:
	movslq	-140(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -160(%rbp)
	addq	%rax, %r15
	leaq	-128(%rbp), %rax
	movq	%rax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L54:
	movl	(%r15,%r13), %esi
	movq	%r14, %rdi
	call	udata_readInt32_67@PLT
	movq	-136(%rbp), %rcx
	movl	%eax, (%rcx,%r13)
	addq	$4, %r13
	cmpq	$64, %r13
	jne	.L54
	movl	-68(%rbp), %r9d
	testl	%r12d, %r12d
	js	.L55
	cmpl	%r9d, %r12d
	jl	.L64
	movq	-152(%rbp), %r13
	addq	-160(%rbp), %r13
	cmpq	%r13, %r15
	je	.L57
	movslq	%r9d, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%r9d, -136(%rbp)
	call	memcpy@PLT
	movl	-136(%rbp), %r9d
.L57:
	movl	%r9d, -152(%rbp)
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	movl	$64, %edx
	movq	%r14, %rdi
	call	*56(%r14)
	movl	-128(%rbp), %r12d
	leaq	64(%r13), %rcx
	leaq	64(%r15), %rsi
	movq	%rbx, %r8
	movq	%r14, %rdi
	movl	%r12d, %edx
	addl	$64, %r12d
	call	utrie2_swap_67@PLT
	movl	-124(%rbp), %eax
	movslq	%r12d, %rsi
	movq	%rbx, %r8
	leaq	0(%r13,%rsi), %rcx
	movq	%r14, %rdi
	addq	%r15, %rsi
	leal	0(,%rax,4), %edx
	movl	%edx, -136(%rbp)
	call	*56(%r14)
	movl	-136(%rbp), %edx
	movq	%rbx, %r8
	movq	%r14, %rdi
	leal	(%r12,%rdx), %esi
	movl	-116(%rbp), %edx
	movslq	%esi, %rsi
	leaq	0(%r13,%rsi), %rcx
	addq	%r15, %rsi
	call	*72(%r14)
	movl	-152(%rbp), %r9d
.L55:
	movl	-140(%rbp), %r8d
	addl	%r9d, %r8d
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$120, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movzbl	14(%r15), %r8d
	movzbl	13(%r15), %ecx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movzbl	12(%r15), %edx
	movzbl	15(%r15), %r9d
	leaq	.LC0(%rip), %rsi
	call	udata_printError_67@PLT
	movl	$3, (%rbx)
	xorl	%r8d, %r8d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	%r8d, -136(%rbp)
	call	udata_printError_67@PLT
	movl	$16, (%rbx)
	movl	-136(%rbp), %r8d
	jmp	.L48
.L63:
	movl	%r12d, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	%r8d, -136(%rbp)
	call	udata_printError_67@PLT
	movl	$8, (%rbx)
	movl	-136(%rbp), %r8d
	jmp	.L48
.L64:
	movl	%r12d, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$8, (%rbx)
	xorl	%r8d, %r8d
	jmp	.L48
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3092:
	.size	_ZL12ucnvsel_swapPK12UDataSwapperPKviPvP10UErrorCode, .-_ZL12ucnvsel_swapPK12UDataSwapperPKviPvP10UErrorCode
	.p2align 4
	.globl	ucnvsel_open_67
	.type	ucnvsel_open_67, @function
ucnvsel_open_67:
.LFB3089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -92(%rbp)
	movl	(%r8), %r11d
	movq	%rdx, -152(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	testl	%r11d, %r11d
	jg	.L66
	movq	%r8, %r13
	testl	%esi, %esi
	js	.L68
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	jne	.L69
	testl	%esi, %esi
	jne	.L68
.L69:
	movl	$56, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L143
	movl	-92(%rbp), %r10d
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rax)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	testl	%r10d, %r10d
	je	.L144
.L71:
	movslq	-92(%rbp), %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, 24(%rdx)
	testq	%rax, %rax
	je	.L109
	movl	-92(%rbp), %r9d
	movq	$0, (%rax)
	testl	%r9d, %r9d
	jle	.L74
	testq	%rbx, %rbx
	je	.L145
	movl	-92(%rbp), %eax
	movq	%rbx, %r14
	xorl	%r15d, %r15d
	subl	$1, %eax
	leaq	8(%rbx,%rax,8), %r12
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%r14), %rdi
	addq	$8, %r14
	call	strlen@PLT
	leal	1(%r15,%rax), %r15d
	cmpq	%r12, %r14
	jne	.L78
.L76:
	movl	%r15d, %eax
	andl	$3, %eax
	movl	%eax, -104(%rbp)
	je	.L142
	movl	$4, %eax
	subl	-104(%rbp), %eax
	movl	%eax, -104(%rbp)
	addl	%eax, %r15d
.L142:
	movq	-88(%rbp), %rax
	movslq	%r15d, %rdi
	movl	%r15d, 36(%rax)
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L109
	movq	-88(%rbp), %rax
	movq	24(%rax), %r12
	movl	-92(%rbp), %eax
	testq	%rbx, %rbx
	je	.L146
	subl	$1, %eax
	movq	%r13, -112(%rbp)
	xorl	%r15d, %r15d
	movq	%rbx, %r13
	leaq	8(,%rax,8), %rax
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r14, (%r12,%r15)
	movq	0(%r13,%r15), %rsi
	movq	%r14, %rdi
	call	strcpy@PLT
	movq	(%r12,%r15), %rdi
	addq	$8, %r15
	call	strlen@PLT
	leaq	1(%r14,%rax), %r14
	cmpq	%r15, %rbx
	jne	.L87
.L141:
	movl	-104(%rbp), %r8d
	movq	-112(%rbp), %r13
	testl	%r8d, %r8d
	je	.L90
	movl	-104(%rbp), %eax
	subl	$1, %eax
	cltq
	addq	$1, %rax
	movl	%eax, %esi
	testl	%eax, %eax
	je	.L90
	xorl	%eax, %eax
.L88:
	movl	%eax, %edx
	addl	$1, %eax
	movb	$0, (%r14,%rdx)
	cmpl	%esi, %eax
	jb	.L88
.L90:
	movl	-92(%rbp), %eax
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movl	%eax, 32(%rdx)
	leal	62(%rax), %edi
	addl	$31, %eax
	cmovns	%eax, %edi
	movb	$1, 49(%rdx)
	sarl	$5, %edi
	call	upvec_open_67@PLT
	movl	0(%r13), %edi
	movq	%rax, %r14
	testl	%edi, %edi
	jg	.L91
	movq	-88(%rbp), %rax
	movl	32(%rax), %ecx
	movl	%ecx, %edx
	leal	62(%rcx), %eax
	movl	%ecx, -160(%rbp)
	addl	$31, %edx
	cmovns	%edx, %eax
	xorl	%ebx, %ebx
	sarl	$5, %eax
	movl	%eax, -156(%rbp)
	movl	%eax, %r12d
	testl	%ecx, %ecx
	jle	.L95
	.p2align 4,,10
	.p2align 3
.L92:
	subq	$8, %rsp
	movl	%ebx, %ecx
	movl	$1114113, %esi
	movl	$1114113, %edx
	pushq	%r13
	movl	$-1, %r9d
	movq	%r14, %rdi
	addl	$1, %ebx
	movl	$-1, %r8d
	call	upvec_setValue_67@PLT
	popq	%rcx
	popq	%rsi
	cmpl	%ebx, %r12d
	jg	.L92
	movq	-88(%rbp), %rax
	movl	32(%rax), %edx
	testl	%edx, %edx
	jle	.L95
	leaq	-64(%rbp), %rax
	movq	%r14, -136(%rbp)
	xorl	%r15d, %r15d
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-88(%rbp), %rax
	movq	%r13, %rsi
	movq	24(%rax), %rax
	movq	(%rax,%r15,8), %rdi
	call	ucnv_open_67@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %r14
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L137
	xorl	%esi, %esi
	movl	$1, %edi
	call	uset_open_67@PLT
	movl	-96(%rbp), %edx
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	ucnv_getUnicodeSet_67@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L147
	movl	%r15d, %eax
	movl	%r15d, %ecx
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	sarl	$5, %eax
	movl	%eax, -92(%rbp)
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, -104(%rbp)
	call	uset_getItemCount_67@PLT
	movl	%eax, %ebx
	leaq	-68(%rbp), %rax
	movq	%rax, -112(%rbp)
	leaq	-60(%rbp), %rax
	movq	%rax, -120(%rbp)
	testl	%ebx, %ebx
	jle	.L100
	.p2align 4,,10
	.p2align 3
.L97:
	subq	$8, %rsp
	pushq	-120(%rbp)
	movq	-112(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	-144(%rbp), %rcx
	xorl	%r8d, %r8d
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$0, -60(%rbp)
	call	uset_getItem_67@PLT
	popq	%r11
	popq	%rax
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jle	.L148
	addl	$1, %r14d
	cmpl	%r14d, %ebx
	jne	.L97
.L100:
	movq	-128(%rbp), %rdi
	call	ucnv_close_67@PLT
	movq	%r12, %rdi
	call	uset_close_67@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L137
	movq	-88(%rbp), %rax
	addq	$1, %r15
	cmpl	%r15d, 32(%rax)
	jg	.L102
	movq	-136(%rbp), %r14
.L95:
	cmpq	$0, -152(%rbp)
	je	.L94
	movq	-152(%rbp), %rdi
	call	uset_getItemCount_67@PLT
	movl	%eax, -92(%rbp)
	testl	%eax, %eax
	jle	.L94
	leaq	-64(%rbp), %r12
	leaq	-60(%rbp), %rax
	movl	-156(%rbp), %r15d
	xorl	%ebx, %ebx
	movq	%rax, -104(%rbp)
	movq	%r12, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L104:
	subq	$8, %rsp
	movq	-152(%rbp), %rdi
	xorl	%r8d, %r8d
	movl	%ebx, %esi
	pushq	%r13
	movq	-104(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
	movq	-112(%rbp), %rdx
	call	uset_getItem_67@PLT
	movl	-160(%rbp), %r8d
	popq	%rsi
	popq	%rdi
	testl	%r8d, %r8d
	jle	.L105
	.p2align 4,,10
	.p2align 3
.L103:
	subq	$8, %rsp
	movl	-60(%rbp), %edx
	movl	-64(%rbp), %esi
	movl	%r12d, %ecx
	pushq	%r13
	movl	$-1, %r9d
	movq	%r14, %rdi
	addl	$1, %r12d
	movl	$-1, %r8d
	call	upvec_setValue_67@PLT
	popq	%rdx
	popq	%rcx
	cmpl	%r12d, %r15d
	jg	.L103
.L105:
	addl	$1, %ebx
	cmpl	%ebx, -92(%rbp)
	jne	.L104
.L94:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	upvec_compactToUTrie2WithRowIndexes_67@PLT
	movq	-88(%rbp), %rbx
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, (%rbx)
	leaq	16(%rbx), %rsi
	call	upvec_cloneArray_67@PLT
	movb	$1, 48(%rbx)
	movq	%rax, 8(%rbx)
	movl	-156(%rbp), %eax
	imull	16(%rbx), %eax
	movl	%eax, 16(%rbx)
.L91:
	movq	%r14, %rdi
	call	upvec_close_67@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L73
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	movq	-88(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	subq	$8, %rsp
	movl	-104(%rbp), %r9d
	movl	-92(%rbp), %ecx
	addl	$1, %r14d
	movl	-64(%rbp), %edx
	movl	-68(%rbp), %esi
	pushq	%r13
	movl	$-1, %r8d
	movq	-136(%rbp), %rdi
	call	upvec_setValue_67@PLT
	popq	%r9
	popq	%r10
	cmpl	%ebx, %r14d
	jne	.L97
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$0, 36(%rdx)
	xorl	%edi, %edi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	jne	.L90
.L109:
	movl	$7, 0(%r13)
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-88(%rbp), %rax
	cmpb	$0, 49(%rax)
	movq	24(%rax), %rdi
	jne	.L150
.L108:
	call	uprv_free_67@PLT
	movq	-88(%rbp), %rax
	cmpb	$0, 48(%rax)
	jne	.L151
.L106:
	movq	-88(%rbp), %rbx
	movq	(%rbx), %rdi
	call	utrie2_close_67@PLT
	movq	40(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	%rbx, %rdi
	call	uprv_free_67@PLT
	movq	$0, -88(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$1, 0(%r13)
	movq	$0, -88(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L151:
	movq	8(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L150:
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	-88(%rbp), %rax
	movq	24(%rax), %rdi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L144:
	call	ucnv_countAvailable_67@PLT
	xorl	%ebx, %ebx
	movl	%eax, -92(%rbp)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L137:
	movq	-136(%rbp), %r14
	movq	%r14, %rdi
	call	upvec_close_67@PLT
	movl	0(%r13), %eax
	testl	%eax, %eax
	jle	.L66
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L146:
	leal	-1(%rax), %ebx
	movq	%r13, -112(%rbp)
	movq	%r12, %r13
	xorl	%r15d, %r15d
	movq	%rbx, %r12
	movq	-88(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r14, 0(%r13,%r15,8)
	movl	%r15d, %edi
	call	ucnv_getAvailableName_67@PLT
	movq	%rax, %rsi
	movq	24(%rbx), %rax
	movq	(%rax,%r15,8), %rdi
	call	strcpy@PLT
	movq	24(%rbx), %r13
	movq	0(%r13,%r15,8), %rdi
	call	strlen@PLT
	leaq	1(%r14,%rax), %r14
	movq	%r15, %rax
	addq	$1, %r15
	cmpq	%r12, %rax
	jne	.L86
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L145:
	movl	-92(%rbp), %r14d
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L77:
	movl	%r12d, %edi
	addl	$1, %r12d
	call	ucnv_getAvailableName_67@PLT
	movq	%rax, %rdi
	call	strlen@PLT
	leal	1(%r15,%rax), %r15d
	cmpl	%r12d, %r14d
	jne	.L77
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-128(%rbp), %rdi
	movq	-136(%rbp), %r14
	call	ucnv_close_67@PLT
	jmp	.L91
.L149:
	call	__stack_chk_fail@PLT
.L143:
	movl	$7, 0(%r13)
	jmp	.L66
	.cfi_endproc
.LFE3089:
	.size	ucnvsel_open_67, .-ucnvsel_open_67
	.p2align 4
	.globl	ucnvsel_close_67
	.type	ucnvsel_close_67, @function
ucnvsel_close_67:
.LFB3090:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L152
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 49(%r12)
	movq	24(%rdi), %rdi
	jne	.L159
	call	uprv_free_67@PLT
	cmpb	$0, 48(%r12)
	jne	.L160
.L155:
	movq	(%r12), %rdi
	call	utrie2_close_67@PLT
	movq	40(%r12), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	24(%r12), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, 48(%r12)
	je	.L155
.L160:
	movq	8(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE3090:
	.size	ucnvsel_close_67, .-ucnvsel_close_67
	.p2align 4
	.globl	ucnvsel_serialize_67
	.type	ucnvsel_serialize_67, @function
ucnvsel_serialize_67:
.LFB3091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L184
	movl	%edx, %r15d
	movq	%rcx, %r12
	testl	%edx, %edx
	js	.L164
	movq	%rdi, %r14
	movq	%rsi, %rbx
	je	.L165
	testq	%rsi, %rsi
	je	.L164
	testb	$3, %sil
	jne	.L164
.L165:
	movq	(%r14), %rdi
	xorl	%edx, %edx
	movq	%r12, %rcx
	xorl	%esi, %esi
	call	utrie2_serialize_67@PLT
	movl	%eax, %edx
	movl	(%r12), %eax
	cmpl	$15, %eax
	je	.L170
	testl	%eax, %eax
	jg	.L184
.L170:
	movl	16+_ZL8dataInfo(%rip), %eax
	movl	36(%r14), %ecx
	pxor	%xmm0, %xmm0
	movl	$0, (%r12)
	movdqa	_ZL8dataInfo(%rip), %xmm3
	movq	$0, -64(%rbp)
	movl	%eax, -124(%rbp)
	movl	16(%r14), %eax
	movl	$0, -56(%rbp)
	leal	96(%rdx,%rax,4), %r8d
	movups	%xmm3, -140(%rbp)
	addl	%ecx, %r8d
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpl	%r8d, %r15d
	jge	.L168
	movl	$15, (%r12)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L164:
	movl	$1, (%r12)
.L184:
	xorl	%r8d, %r8d
.L161:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	subq	$-128, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movd	%ecx, %xmm5
	movd	%eax, %xmm6
	movq	-128(%rbp), %rsi
	leal	-32(%r8), %eax
	movd	32(%r14), %xmm2
	movd	%edx, %xmm1
	movl	%eax, -52(%rbp)
	movq	%r12, %rcx
	movdqa	-64(%rbp), %xmm7
	punpckldq	%xmm6, %xmm1
	movq	%rsi, 16(%rbx)
	addq	$96, %rbx
	punpckldq	%xmm5, %xmm2
	movq	$0, -72(%rbx)
	movl	$668598304, -144(%rbp)
	movdqa	-144(%rbp), %xmm4
	punpcklqdq	%xmm2, %xmm1
	movups	%xmm1, -64(%rbx)
	movups	%xmm4, -96(%rbx)
	movups	%xmm0, -48(%rbx)
	movups	%xmm0, -32(%rbx)
	movups	%xmm7, -16(%rbx)
	movq	(%r14), %rdi
	movq	%rbx, %rsi
	movl	%r8d, -152(%rbp)
	movaps	%xmm1, -112(%rbp)
	movl	%edx, -148(%rbp)
	call	utrie2_serialize_67@PLT
	movl	16(%r14), %eax
	movslq	-148(%rbp), %rdx
	movq	8(%r14), %rsi
	leal	0(,%rax,4), %edi
	addq	%rdx, %rbx
	movslq	%edi, %r12
	movq	%rbx, %rdi
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	24(%r14), %rax
	movslq	36(%r14), %rdx
	leaq	(%rbx,%r12), %rdi
	movq	(%rax), %rsi
	call	memcpy@PLT
	movl	-152(%rbp), %r8d
	jmp	.L161
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3091:
	.size	ucnvsel_serialize_67, .-ucnvsel_serialize_67
	.p2align 4
	.globl	ucnvsel_openFromSerialized_67
	.type	ucnvsel_openFromSerialized_67, @function
ucnvsel_openFromSerialized_67:
.LFB3093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L186
	movl	%esi, %ebx
	movq	%rdx, %r13
	testl	%esi, %esi
	jle	.L188
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L188
	testb	$3, %dil
	jne	.L188
	cmpl	$31, %esi
	jle	.L213
	cmpw	$10202, 2(%rdi)
	jne	.L191
	cmpw	$21315, 12(%rdi)
	jne	.L191
	cmpw	$27749, 14(%rdi)
	jne	.L191
	cmpb	$1, 16(%rdi)
	jne	.L214
	xorl	%r15d, %r15d
	cmpw	$0, 8(%rdi)
	jne	.L215
.L194:
	movzwl	(%r12), %edx
	leal	63(%rdx), %ecx
	cmpl	%ebx, %ecx
	jge	.L212
	addq	%rdx, %r12
	subl	%edx, %ebx
	cmpl	%ebx, 60(%r12)
	jg	.L212
	movl	$56, %edi
	leaq	64(%r12), %rbx
	call	uprv_malloc_67@PLT
	movslq	8(%r12), %rdi
	movq	%rax, %r14
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	testq	%r14, %r14
	je	.L209
	testq	%rax, %rax
	je	.L209
	pxor	%xmm0, %xmm0
	movl	4(%r12), %edx
	movq	%r15, 40(%r14)
	movq	%rbx, %rsi
	movups	%xmm0, 16(%r14)
	movq	%r13, %r8
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%rax, 24(%r14)
	movq	8(%r12), %rax
	movl	%edx, 16(%r14)
	movl	(%r12), %edx
	movq	$0, 48(%r14)
	movq	%rax, 32(%r14)
	movups	%xmm0, (%r14)
	call	utrie2_openFromSerialized_67@PLT
	movq	%rax, (%r14)
	movslq	(%r12), %rax
	addq	%rax, %rbx
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L216
	movl	16(%r14), %eax
	movl	32(%r14), %edx
	movq	%rbx, 8(%r14)
	sall	$2, %eax
	cltq
	addq	%rax, %rbx
	testl	%edx, %edx
	jle	.L186
	movq	24(%r14), %rax
	subl	$1, %edx
	leaq	8(%rax), %r12
	leaq	(%r12,%rdx,8), %r13
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L217:
	addq	$8, %r12
.L205:
	movq	%rbx, (%rax)
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	1(%rbx,%rax), %rbx
	movq	%r12, %rax
	cmpq	%r12, %r13
	jne	.L217
.L186:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movl	$3, 0(%r13)
	xorl	%r14d, %r14d
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$1, 0(%r13)
	xorl	%r14d, %r14d
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L213:
	movl	$8, (%rdx)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$16, (%rdx)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L215:
	movq	%rdx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	udata_openSwapperForInputData_67@PLT
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZL12ucnvsel_swapPK12UDataSwapperPKviPvP10UErrorCode
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L218
	cmpl	%eax, %ebx
	jl	.L219
	movslq	%eax, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L220
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %r8
	movq	%rax, %rcx
	movq	%r14, %rdi
	movq	%r15, %r12
	call	_ZL12ucnvsel_swapPK12UDataSwapperPKviPvP10UErrorCode
	movq	%r14, %rdi
	call	udata_closeSwapper_67@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L194
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	uprv_free_67@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	uprv_free_67@PLT
	movl	$8, 0(%r13)
	jmp	.L186
.L216:
	cmpb	$0, 49(%r14)
	movq	24(%r14), %rdi
	jne	.L221
.L203:
	call	uprv_free_67@PLT
	cmpb	$0, 48(%r14)
	jne	.L222
.L204:
	movq	(%r14), %rdi
	call	utrie2_close_67@PLT
	movq	40(%r14), %rdi
	call	uprv_free_67@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	uprv_free_67@PLT
	jmp	.L186
.L219:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	udata_closeSwapper_67@PLT
	movl	$8, 0(%r13)
	jmp	.L186
.L218:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	udata_closeSwapper_67@PLT
	jmp	.L186
.L222:
	movq	8(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L204
.L221:
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	24(%r14), %rdi
	jmp	.L203
.L220:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	udata_closeSwapper_67@PLT
	movl	$7, 0(%r13)
	jmp	.L186
.L209:
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	uprv_free_67@PLT
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	uprv_free_67@PLT
	movl	$7, 0(%r13)
	jmp	.L186
	.cfi_endproc
.LFE3093:
	.size	ucnvsel_openFromSerialized_67, .-ucnvsel_openFromSerialized_67
	.p2align 4
	.globl	ucnvsel_selectForString_67
	.type	ucnvsel_selectForString_67, @function
ucnvsel_selectForString_67:
.LFB3101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %eax
	movq	%rcx, -80(%rbp)
	testl	%eax, %eax
	jg	.L223
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L225
	movq	%rsi, %rbx
	movslq	%edx, %r15
	testq	%rsi, %rsi
	jne	.L226
	testl	%r15d, %r15d
	jne	.L225
.L226:
	movl	32(%r13), %r14d
	movl	%r14d, %eax
	leal	62(%r14), %r12d
	addl	$31, %eax
	cmovns	%eax, %r12d
	sarl	$5, %r12d
	leal	0(,%r12,4), %edx
	movslq	%edx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	je	.L273
	movl	$-1, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	%rax, %r8
	testq	%rbx, %rbx
	je	.L232
	xorl	%edi, %edi
	testl	%r15d, %r15d
	js	.L229
	leaq	(%rbx,%r15,2), %rdi
.L229:
	testl	%r14d, %r14d
	movl	$1, %eax
	cmovg	%r12d, %eax
	movl	%eax, %esi
	movl	%eax, %ecx
	movl	%eax, -72(%rbp)
	andl	$-4, %esi
	shrl	$2, %ecx
	movslq	%esi, %rax
	salq	$4, %rcx
	movl	%esi, -68(%rbp)
	leaq	0(,%rax,4), %r15
	movq	%rcx, -56(%rbp)
	leaq	(%r8,%r15), %rax
	movq	%rax, -88(%rbp)
	leal	1(%rsi), %eax
	movl	%eax, -92(%rbp)
	cltq
	salq	$2, %rax
	movq	%rax, -104(%rbp)
	addq	%r8, %rax
	movq	%rax, -112(%rbp)
	leal	2(%rsi), %eax
	movl	%eax, -96(%rbp)
	cltq
	salq	$2, %rax
	movq	%rax, -128(%rbp)
	addq	%r8, %rax
	movq	%rax, -120(%rbp)
	leaq	16(%r8), %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L243:
	cmpq	%rdi, %rbx
	setne	%al
	testq	%rdi, %rdi
	je	.L274
.L231:
	testb	%al, %al
	je	.L232
	movzwl	(%rbx), %eax
	movq	0(%r13), %r10
	leaq	2(%rbx), %r9
	movl	%eax, %r11d
	movq	(%r10), %rsi
	movl	%eax, %edx
	andl	$64512, %r11d
	cmpl	$55296, %r11d
	je	.L233
	sarl	$5, %eax
.L272:
	cltq
	andl	$31, %edx
	movq	%r9, %rbx
	movzwl	(%rsi,%rax,2), %eax
	leal	(%rdx,%rax,4), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
.L234:
	movq	8(%r13), %rdx
	salq	$2, %rax
	testl	%r14d, %r14d
	jle	.L232
	leaq	(%rdx,%rax), %rsi
	leaq	16(%rdx,%rax), %rax
	cmpq	%rax, %r8
	setnb	%dl
	cmpq	-64(%rbp), %rsi
	setnb	%al
	orb	%al, %dl
	movl	$0, %eax
	je	.L246
	cmpl	$128, %r14d
	jle	.L246
	movq	-56(%rbp), %rcx
	pxor	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L240:
	movdqu	(%rsi,%rax), %xmm0
	movdqu	(%r8,%rax), %xmm2
	pand	%xmm2, %xmm0
	movups	%xmm0, (%r8,%rax)
	addq	$16, %rax
	por	%xmm0, %xmm1
	cmpq	%rax, %rcx
	jne	.L240
	movdqa	%xmm1, %xmm0
	movq	%rcx, -56(%rbp)
	movl	-72(%rbp), %r11d
	psrldq	$8, %xmm0
	por	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	psrldq	$4, %xmm0
	por	%xmm0, %xmm1
	movd	%xmm1, %edx
	cmpl	%r11d, -68(%rbp)
	je	.L242
	movq	-88(%rbp), %r10
	movl	(%rsi,%r15), %eax
	andl	(%r10), %eax
	movl	%eax, (%r10)
	orl	%eax, %edx
	cmpl	-92(%rbp), %r12d
	jle	.L242
	movq	-112(%rbp), %r10
	movq	-104(%rbp), %rax
	movl	(%rsi,%rax), %eax
	andl	(%r10), %eax
	movl	%eax, (%r10)
	orl	%eax, %edx
	cmpl	-96(%rbp), %r12d
	jle	.L242
	movq	-128(%rbp), %rax
	movl	(%rsi,%rax), %eax
	movq	-120(%rbp), %rsi
	andl	(%rsi), %eax
	movl	%eax, (%rsi)
	orl	%eax, %edx
.L242:
	testl	%edx, %edx
	jne	.L243
.L232:
	movq	-80(%rbp), %rdx
	addq	$88, %rsp
	movq	%r13, %rdi
	movq	%r8, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZL13selectForMaskPK18UConverterSelectorPjP10UErrorCode
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	cmpq	%rdi, %r9
	je	.L235
	movzwl	2(%rbx), %r11d
	movl	%r11d, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L236
.L235:
	sarl	$5, %eax
	addl	$320, %eax
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L274:
	cmpw	$0, (%rbx)
	setne	%al
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L246:
	movq	-56(%rbp), %rcx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L239:
	movl	(%rsi,%rax,4), %r9d
	andl	(%r8,%rax,4), %r9d
	movl	%r9d, (%r8,%rax,4)
	addq	$1, %rax
	orl	%r9d, %edx
	cmpl	%eax, %r12d
	jg	.L239
	movq	%rcx, -56(%rbp)
	jmp	.L242
.L225:
	movq	-80(%rbp), %rax
	movl	$1, (%rax)
.L223:
	addq	$88, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L236:
	.cfi_restore_state
	sall	$10, %eax
	addq	$4, %rbx
	leal	-56613888(%r11,%rax), %eax
	cmpl	%eax, 44(%r10)
	jle	.L275
	movl	%eax, %edx
	movl	%eax, %r9d
	andl	$31, %eax
	sarl	$11, %edx
	sarl	$5, %r9d
	addl	$2080, %edx
	andl	$63, %r9d
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	addl	%r9d, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %eax
	cltq
	addq	%rax, %rax
.L238:
	movzwl	(%rsi,%rax), %eax
	jmp	.L234
.L275:
	movslq	48(%r10), %rax
	addq	%rax, %rax
	jmp	.L238
.L273:
	movq	-80(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L223
	.cfi_endproc
.LFE3101:
	.size	ucnvsel_selectForString_67, .-ucnvsel_selectForString_67
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	" 000000000000\02000"
	.text
	.p2align 4
	.globl	ucnvsel_selectForUTF8_67
	.type	ucnvsel_selectForUTF8_67, @function
ucnvsel_selectForUTF8_67:
.LFB3102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %eax
	movq	%rcx, -112(%rbp)
	testl	%eax, %eax
	jg	.L276
	movq	%rdi, %r15
	testq	%rdi, %rdi
	je	.L278
	movq	%rsi, %rbx
	movl	%edx, %r13d
	testl	%edx, %edx
	je	.L279
	testq	%rsi, %rsi
	je	.L278
.L279:
	movl	32(%r15), %r8d
	movl	%r8d, %eax
	leal	62(%r8), %r14d
	movl	%r8d, -60(%rbp)
	addl	$31, %eax
	cmovns	%eax, %r14d
	sarl	$5, %r14d
	leal	0(,%r14,4), %edx
	movslq	%edx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %rdx
	movl	-60(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L326
	movl	$-1, %esi
	movq	%rax, %rdi
	movl	%r8d, -56(%rbp)
	call	memset@PLT
	testl	%r13d, %r13d
	movl	-56(%rbp), %r8d
	js	.L327
	testq	%rbx, %rbx
	jne	.L282
.L283:
	movq	-112(%rbp), %rdx
	addq	$120, %rsp
	movq	%r12, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZL13selectForMaskPK18UConverterSelectorPjP10UErrorCode
.L327:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	strlen@PLT
	movl	-56(%rbp), %r8d
	movl	%eax, %r13d
.L282:
	movslq	%r13d, %rcx
	movl	$1, %eax
	addq	%rbx, %rcx
	testl	%r8d, %r8d
	cmovg	%r14d, %eax
	movl	%eax, %r13d
	movl	%eax, -60(%rbp)
	andl	$-4, %eax
	movslq	%eax, %r9
	shrl	$2, %r13d
	movq	%r9, %rdi
	salq	$4, %r13
	movl	%r9d, -64(%rbp)
	leal	1(%rdi), %eax
	movq	%r13, -56(%rbp)
	salq	$2, %r9
	movl	%eax, -68(%rbp)
	cltq
	leaq	(%r12,%r9), %r11
	movq	%r9, %r10
	salq	$2, %rax
	movq	%rax, -80(%rbp)
	addq	%r12, %rax
	movq	%rax, -88(%rbp)
	leal	2(%rdi), %eax
	movl	%eax, -72(%rbp)
	cltq
	salq	$2, %rax
	movq	%rax, -96(%rbp)
	addq	%r12, %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L292:
	cmpq	%rbx, %rcx
	je	.L283
	movzbl	(%rbx), %r9d
	movq	(%r15), %rdi
	leaq	1(%rbx), %rdx
	testb	%r9b, %r9b
	js	.L284
	movq	8(%rdi), %rax
	movq	%rdx, %rbx
	movzwl	(%rax,%r9,2), %esi
.L285:
	movq	8(%r15), %rdi
	salq	$2, %rsi
	testl	%r8d, %r8d
	jle	.L283
	leaq	16(%rdi,%rsi), %rax
	leaq	(%rdi,%rsi), %rdx
	cmpq	%rax, %r12
	leaq	16(%r12), %rax
	setnb	%r9b
	cmpq	%rax, %rdx
	setnb	%al
	orb	%al, %r9b
	je	.L294
	cmpl	$128, %r8d
	jle	.L294
	movq	-56(%rbp), %r13
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L289:
	movdqu	(%rdx,%rax), %xmm0
	movdqu	(%r12,%rax), %xmm2
	pand	%xmm2, %xmm0
	movups	%xmm0, (%r12,%rax)
	addq	$16, %rax
	por	%xmm0, %xmm1
	cmpq	%r13, %rax
	jne	.L289
	movdqa	%xmm1, %xmm0
	movq	%r13, -56(%rbp)
	movl	-64(%rbp), %r13d
	psrldq	$8, %xmm0
	por	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	psrldq	$4, %xmm0
	por	%xmm0, %xmm1
	movd	%xmm1, %eax
	cmpl	%r13d, -60(%rbp)
	je	.L291
	movl	(%rdx,%r10), %r9d
	andl	(%r11), %r9d
	movl	%r9d, (%r11)
	orl	%r9d, %eax
	cmpl	%r14d, -68(%rbp)
	jge	.L291
	movq	-80(%rbp), %r9
	movl	(%rdx,%r9), %edx
	movq	-88(%rbp), %r9
	andl	(%r9), %edx
	movl	%edx, (%r9)
	orl	%edx, %eax
	cmpl	-72(%rbp), %r14d
	jle	.L291
	addq	-96(%rbp), %rdi
	movl	(%rdi,%rsi), %edx
	movq	-104(%rbp), %rdi
	andl	(%rdi), %edx
	movl	%edx, (%rdi)
	orl	%edx, %eax
.L291:
	testl	%eax, %eax
	jne	.L292
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L284:
	leal	32(%r9), %eax
	movzbl	%r9b, %esi
	cmpb	$15, %al
	jbe	.L328
	addl	$62, %r9d
	cmpb	$29, %r9b
	ja	.L287
	cmpq	%rcx, %rdx
	jnb	.L287
	movzbl	1(%rbx), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L287
	movq	(%rdi), %rdx
	addl	$1888, %esi
	movzbl	%al, %eax
	addq	$2, %rbx
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %esi
	addl	%esi, %eax
	cltq
	movzwl	(%rdx,%rax,2), %esi
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L328:
	leaq	2(%rbx), %rax
	cmpq	%rax, %rcx
	jbe	.L287
	movzbl	1(%rbx), %eax
	andl	$15, %r9d
	leaq	.LC4(%rip), %r13
	movsbl	0(%r13,%r9), %r9d
	movl	%eax, %r13d
	sarl	$5, %r13d
	btl	%r13d, %r9d
	jnc	.L287
	movzbl	2(%rbx), %r9d
	addl	$-128, %r9d
	cmpb	$63, %r9b
	jbe	.L329
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%r11, -152(%rbp)
	movq	%r10, -144(%rbp)
	movl	%r8d, -132(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	utrie2_internalU8NextIndex_67@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rcx
	movl	%eax, %ebx
	sarl	$3, %eax
	movq	-152(%rbp), %r11
	movq	-144(%rbp), %r10
	andl	$7, %ebx
	cltq
	movl	-132(%rbp), %r8d
	addq	%rdx, %rbx
	movq	(%r15), %rdx
	movq	(%rdx), %rdx
	movzwl	(%rdx,%rax,2), %esi
	jmp	.L285
.L329:
	subl	$224, %esi
	addl	%eax, %eax
	movq	(%rdi), %rdx
	addq	$3, %rbx
	sall	$7, %esi
	andl	$126, %eax
	addl	%esi, %eax
	movzbl	%r9b, %esi
	andl	$31, %r9d
	sarl	$5, %esi
	addl	%esi, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	leal	(%r9,%rax,4), %eax
	cltq
	movzwl	(%rdx,%rax,2), %esi
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L294:
	movq	-56(%rbp), %r13
	xorl	%esi, %esi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L288:
	movl	(%rdx,%rsi,4), %edi
	andl	(%r12,%rsi,4), %edi
	movl	%edi, (%r12,%rsi,4)
	addq	$1, %rsi
	orl	%edi, %eax
	cmpl	%esi, %r14d
	jg	.L288
	movq	%r13, -56(%rbp)
	jmp	.L291
.L278:
	movq	-112(%rbp), %rax
	movl	$1, (%rax)
.L276:
	addq	$120, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L326:
	.cfi_restore_state
	movq	-112(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L276
	.cfi_endproc
.LFE3102:
	.size	ucnvsel_selectForUTF8_67, .-ucnvsel_selectForUTF8_67
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL16defaultEncodings, @object
	.size	_ZL16defaultEncodings, 56
_ZL16defaultEncodings:
	.quad	0
	.quad	0
	.quad	ucnvsel_close_selector_iterator
	.quad	ucnvsel_count_encodings
	.quad	uenum_unextDefault_67
	.quad	ucnvsel_next_encoding
	.quad	ucnvsel_reset_iterator
	.section	.rodata
	.align 16
	.type	_ZL8dataInfo, @object
	.size	_ZL8dataInfo, 20
_ZL8dataInfo:
	.value	20
	.value	0
	.byte	0
	.byte	0
	.byte	2
	.byte	0
	.ascii	"CSel"
	.string	"\001"
	.zero	2
	.string	""
	.zero	3
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
