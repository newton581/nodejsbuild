	.file	"localeprioritylist.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_122compareLocaleAndWeightEPKvS2_S2_, @function
_ZN6icu_6712_GLOBAL__N_122compareLocaleAndWeightEPKvS2_S2_:
.LFB2384:
	.cfi_startproc
	endbr64
	movl	8(%rdx), %eax
	subl	8(%rsi), %eax
	jne	.L1
	movl	12(%rsi), %eax
	subl	12(%rdx), %eax
.L1:
	ret
	.cfi_endproc
.LFE2384:
	.size	_ZN6icu_6712_GLOBAL__N_122compareLocaleAndWeightEPKvS2_S2_, .-_ZN6icu_6712_GLOBAL__N_122compareLocaleAndWeightEPKvS2_S2_
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_114compareLocalesE8UElementS1_, @function
_ZN6icu_6712_GLOBAL__N_114compareLocalesE8UElementS1_:
.LFB2382:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_676LocaleeqERKS0_@PLT
	.cfi_endproc
.LFE2382:
	.size	_ZN6icu_6712_GLOBAL__N_114compareLocalesE8UElementS1_, .-_ZN6icu_6712_GLOBAL__N_114compareLocalesE8UElementS1_
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_110hashLocaleE8UElement, @function
_ZN6icu_6712_GLOBAL__N_110hashLocaleE8UElement:
.LFB2381:
	.cfi_startproc
	endbr64
	jmp	_ZNK6icu_676Locale8hashCodeEv@PLT
	.cfi_endproc
.LFE2381:
	.size	_ZN6icu_6712_GLOBAL__N_110hashLocaleE8UElement, .-_ZN6icu_6712_GLOBAL__N_110hashLocaleE8UElement
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2663:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2663:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2666:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L7
	cmpb	$0, 12(%rbx)
	jne	.L20
.L11:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L11
	.cfi_endproc
.LFE2666:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2669:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L23
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2669:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2672:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L26
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2672:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L32
.L28:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L33
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2674:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2675:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2675:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2676:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2676:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2677:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2677:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2678:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2678:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2679:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2679:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2680:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L49
	testl	%edx, %edx
	jle	.L49
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L52
.L41:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L41
	.cfi_endproc
.LFE2680:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L56
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L56
	testl	%r12d, %r12d
	jg	.L63
	cmpb	$0, 12(%rbx)
	jne	.L64
.L58:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L58
.L64:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L56:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2681:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L66
	movq	(%rdi), %r8
.L67:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L70
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L70
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2682:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2683:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L77
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2683:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2684:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2684:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2685:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2685:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2686:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2686:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2688:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2688:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2690:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2690:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718LocalePriorityListD2Ev
	.type	_ZN6icu_6718LocalePriorityListD2Ev, @function
_ZN6icu_6718LocalePriorityListD2Ev:
.LFB2398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %r13
	testq	%r13, %r13
	je	.L85
	movl	8(%rdi), %edx
	testl	%edx, %edx
	jle	.L86
	xorl	%ebx, %ebx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movq	(%rdi), %rax
	addq	$1, %rbx
	call	*8(%rax)
	movl	8(%r12), %edx
	movq	(%r12), %r13
	cmpl	%ebx, %edx
	jle	.L88
.L97:
	movq	0(%r13), %rcx
.L90:
	movq	%rbx, %rax
	salq	$4, %rax
	movq	(%rcx,%rax), %rdi
	testq	%rdi, %rdi
	jne	.L98
	addq	$1, %rbx
	cmpl	%ebx, %edx
	jg	.L90
.L86:
	cmpb	$0, 12(%r13)
	jne	.L99
.L92:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L85:
	movq	24(%r12), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uhash_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L85
	cmpb	$0, 12(%r13)
	je	.L92
.L99:
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L92
	.cfi_endproc
.LFE2398:
	.size	_ZN6icu_6718LocalePriorityListD2Ev, .-_ZN6icu_6718LocalePriorityListD2Ev
	.globl	_ZN6icu_6718LocalePriorityListD1Ev
	.set	_ZN6icu_6718LocalePriorityListD1Ev,_ZN6icu_6718LocalePriorityListD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718LocalePriorityList8localeAtEi
	.type	_ZNK6icu_6718LocalePriorityList8localeAtEi, @function
_ZNK6icu_6718LocalePriorityList8localeAtEi:
.LFB2400:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	salq	$4, %rsi
	addq	(%rax), %rsi
	movq	(%rsi), %rax
	ret
	.cfi_endproc
.LFE2400:
	.size	_ZNK6icu_6718LocalePriorityList8localeAtEi, .-_ZNK6icu_6718LocalePriorityList8localeAtEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718LocalePriorityList14orphanLocaleAtEi
	.type	_ZN6icu_6718LocalePriorityList14orphanLocaleAtEi, @function
_ZN6icu_6718LocalePriorityList14orphanLocaleAtEi:
.LFB2401:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L101
	movq	(%r8), %rax
	movslq	%esi, %rsi
	salq	$4, %rsi
	addq	%rsi, %rax
	movq	(%rax), %r8
	movq	$0, (%rax)
.L101:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE2401:
	.size	_ZN6icu_6718LocalePriorityList14orphanLocaleAtEi, .-_ZN6icu_6718LocalePriorityList14orphanLocaleAtEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718LocalePriorityList3addERKNS_6LocaleEiR10UErrorCode
	.type	_ZN6icu_6718LocalePriorityList3addERKNS_6LocaleEiR10UErrorCode, @function
_ZN6icu_6718LocalePriorityList3addERKNS_6LocaleEiR10UErrorCode:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L111
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	movq	%rsi, %r14
	movl	%edx, %r13d
	movq	%rcx, %r12
	testq	%rdi, %rdi
	je	.L146
	call	uhash_geti_67@PLT
	testl	%eax, %eax
	je	.L147
	movq	(%rbx), %r15
	subl	$1, %eax
	cltq
	salq	$4, %rax
	addq	(%r15), %rax
	movq	(%rax), %r8
	movl	$0, 8(%rax)
	movq	$0, (%rax)
	addl	$1, 12(%rbx)
	testl	%r13d, %r13d
	jle	.L117
.L128:
	testq	%r8, %r8
	je	.L118
.L119:
	movl	8(%rbx), %r14d
	cmpl	8(%r15), %r14d
	jne	.L122
	cmpl	$49, %r14d
	jle	.L130
	leal	0(,%r14,4), %r9d
	movslq	%r9d, %rdi
	salq	$4, %rdi
.L123:
	movq	%r8, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L124
	testl	%r14d, %r14d
	movq	(%r15), %rsi
	movslq	-56(%rbp), %r9
	jle	.L125
	cmpl	%r14d, 8(%r15)
	cmovle	8(%r15), %r14d
	movq	%rax, %rdi
	cmpl	%r9d, %r14d
	movslq	%r14d, %rdx
	cmovg	%r9, %rdx
	salq	$4, %rdx
	call	memcpy@PLT
	movq	(%r15), %rsi
	movq	-64(%rbp), %r8
	movl	-56(%rbp), %r9d
	movq	%rax, %rcx
.L125:
	cmpb	$0, 12(%r15)
	jne	.L148
.L126:
	movq	%rcx, (%r15)
	movl	%r9d, 8(%r15)
	movb	$1, 12(%r15)
	movl	8(%rbx), %r14d
.L122:
	movq	24(%rbx), %rdi
	movq	%r8, %rsi
	leal	1(%r14), %edx
	movq	%r12, %rcx
	movq	%r8, -56(%rbp)
	call	uhash_puti_67@PLT
	movl	(%r12), %eax
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jg	.L131
	movslq	8(%rbx), %rax
	movq	(%rbx), %rcx
	movq	%rax, %rdx
	salq	$4, %rax
	addq	(%rcx), %rax
	leal	1(%rdx), %ecx
	movq	%r8, (%rax)
	movl	%r13d, 8(%rax)
	movl	%ecx, 8(%rbx)
	movl	%edx, 12(%rax)
	cmpl	$999, %r13d
	jg	.L145
	movb	$1, 16(%rbx)
.L145:
	movl	$1, %r12d
.L106:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	testl	%edx, %edx
	jle	.L145
	movq	uhash_compareLong_67@GOTPCREL(%rip), %rdx
	leaq	_ZN6icu_6712_GLOBAL__N_110hashLocaleE8UElement(%rip), %rdi
	leaq	_ZN6icu_6712_GLOBAL__N_114compareLocalesE8UElementS1_(%rip), %rsi
	call	uhash_open_67@PLT
	movl	(%r12), %edx
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	testl	%edx, %edx
	jg	.L111
	movq	%r14, %rsi
	call	uhash_geti_67@PLT
	testl	%eax, %eax
	je	.L118
	movq	(%rbx), %r15
	subl	$1, %eax
	cltq
	salq	$4, %rax
	addq	(%r15), %rax
	movq	(%rax), %r8
	movl	$0, 8(%rax)
	movq	$0, (%rax)
	addl	$1, 12(%rbx)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L111:
	xorl	%r12d, %r12d
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L147:
	testl	%r13d, %r13d
	jle	.L145
.L118:
	movq	%r14, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L115
	cmpb	$0, 216(%rax)
	je	.L144
	cmpb	$0, 216(%r14)
	jne	.L144
.L115:
	movl	$7, (%r12)
	xorl	%r12d, %r12d
.L121:
	testq	%r8, %r8
	jne	.L127
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L131:
	xorl	%r12d, %r12d
.L127:
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L130:
	movl	$1600, %edi
	movl	$100, %r9d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%rbx), %r15
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L117:
	movq	24(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r8, -56(%rbp)
	movl	$1, %r12d
	call	uhash_removei_67@PLT
	movq	-56(%rbp), %r8
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%rsi, %rdi
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %r8
	movl	-56(%rbp), %r9d
	jmp	.L126
.L124:
	movl	$7, (%r12)
	xorl	%r12d, %r12d
	jmp	.L127
	.cfi_endproc
.LFE2402:
	.size	_ZN6icu_6718LocalePriorityList3addERKNS_6LocaleEiR10UErrorCode, .-_ZN6icu_6718LocalePriorityList3addERKNS_6LocaleEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718LocalePriorityList4sortER10UErrorCode
	.type	_ZN6icu_6718LocalePriorityList4sortER10UErrorCode, @function
_ZN6icu_6718LocalePriorityList4sortER10UErrorCode:
.LFB2403:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L152
	movl	8(%rdi), %r10d
	movl	%r10d, %eax
	subl	12(%rdi), %eax
	cmpl	$1, %eax
	jle	.L152
	cmpb	$0, 16(%rdi)
	je	.L152
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movl	$16, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	_ZN6icu_6712_GLOBAL__N_122compareLocaleAndWeightEPKvS2_S2_(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%rsi
	movq	(%rax), %rdi
	movl	%r10d, %esi
	call	uprv_sortArray_67@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2403:
	.size	_ZN6icu_6718LocalePriorityList4sortER10UErrorCode, .-_ZN6icu_6718LocalePriorityList4sortER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718LocalePriorityListC2ENS_11StringPieceER10UErrorCode
	.type	_ZN6icu_6718LocalePriorityListC2ENS_11StringPieceER10UErrorCode, @function
_ZN6icu_6718LocalePriorityListC2ENS_11StringPieceER10UErrorCode:
.LFB2392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -368(%rbp)
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	testl	%r8d, %r8d
	jle	.L215
.L155:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L215:
	.cfi_restore_state
	movl	$336, %edi
	movq	%rsi, %rbx
	movq	%rdx, %r12
	movq	%rcx, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L158
	movq	-368(%rbp), %rcx
	leaq	16(%rax), %rdx
	movl	$20, 8(%rax)
	movq	%rdx, (%rax)
	movslq	%r12d, %rdx
	movabsq	$576478348784435200, %r12
	movb	$0, 12(%rax)
	leaq	(%rbx,%rdx), %r14
	movq	%rax, (%rcx)
	leaq	-352(%rbp), %rax
	movq	%rax, -376(%rbp)
	.p2align 4,,10
	.p2align 3
.L159:
	cmpq	%rbx, %r14
	ja	.L162
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L217:
	addq	$1, %rbx
	cmpq	%rbx, %r14
	je	.L161
.L162:
	cmpb	$32, (%rbx)
	je	.L217
.L160:
	cmpq	%r14, %rbx
	je	.L161
	cmpb	$44, (%rbx)
	je	.L218
	cmpq	%r14, %rbx
	jnb	.L165
	movq	%rbx, %rax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L166:
	addq	$1, %rax
	cmpq	%rax, %r14
	je	.L219
.L168:
	movzbl	(%rax), %edx
	cmpb	$59, %dl
	ja	.L166
	btq	%rdx, %r12
	jnc	.L166
.L167:
	movq	%rax, %rsi
	subq	%rbx, %rsi
	movq	%rsi, -360(%rbp)
	cmpl	%ebx, %eax
	je	.L165
	movq	-376(%rbp), %rdi
	xorl	%esi, %esi
	movl	-360(%rbp), %edx
	movq	%r13, %rcx
	movw	%si, -340(%rbp)
	movq	%rbx, %rsi
	leaq	-339(%rbp), %rax
	movq	%rax, -352(%rbp)
	movl	$0, -296(%rbp)
	movl	$40, -344(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L171
	movq	-352(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-288(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	cmpb	$0, -72(%rbp)
	jne	.L178
	movslq	-360(%rbp), %rax
	addq	%rax, %rbx
	cmpq	%r14, %rbx
	jb	.L175
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L220:
	addq	$1, %rbx
	cmpq	%rbx, %r14
	je	.L204
.L175:
	cmpb	$32, (%rbx)
	je	.L220
.L173:
	cmpq	%rbx, %r14
	je	.L204
	movzbl	(%rbx), %eax
	movl	$1000, %edx
	cmpb	$59, %al
	je	.L221
.L176:
	cmpb	$44, %al
	jne	.L178
	movq	-368(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rsi
	addq	$1, %rbx
	call	_ZN6icu_6718LocalePriorityList3addERKNS_6LocaleEiR10UErrorCode
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	cmpb	$0, -340(%rbp)
	je	.L159
	movq	-352(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L218:
	addq	$1, %rbx
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$1000, %edx
.L174:
	movq	-368(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rsi
	call	_ZN6icu_6718LocalePriorityList3addERKNS_6LocaleEiR10UErrorCode
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	cmpb	$0, -340(%rbp)
	jne	.L222
	.p2align 4,,10
	.p2align 3
.L161:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L155
	movq	-368(%rbp), %rcx
	movl	8(%rcx), %esi
	movl	%esi, %eax
	subl	12(%rcx), %eax
	cmpl	$1, %eax
	jle	.L155
	cmpb	$0, 16(%rcx)
	je	.L155
	movq	(%rcx), %rax
	subq	$8, %rsp
	movl	$16, %edx
	xorl	%r9d, %r9d
	pushq	%r13
	xorl	%r8d, %r8d
	leaq	_ZN6icu_6712_GLOBAL__N_122compareLocaleAndWeightEPKvS2_S2_(%rip), %rcx
	movq	(%rax), %rdi
	call	uprv_sortArray_67@PLT
	popq	%rax
	popq	%rdx
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%r14, %rax
	jmp	.L167
.L221:
	leaq	1(%rbx), %r9
	cmpq	%r14, %r9
	jb	.L179
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L223:
	addq	$1, %r9
	cmpq	%r9, %r14
	je	.L178
.L179:
	cmpb	$32, (%r9)
	je	.L223
.L177:
	cmpq	%r9, %r14
	je	.L178
	cmpb	$113, (%r9)
	jne	.L178
	addq	$1, %r9
	cmpq	%r14, %r9
	jb	.L181
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L224:
	addq	$1, %r9
	cmpq	%r9, %r14
	je	.L178
.L181:
	cmpb	$32, (%r9)
	je	.L224
.L180:
	cmpq	%r9, %r14
	je	.L178
	cmpb	$61, (%r9)
	jne	.L178
	addq	$1, %r9
	cmpq	%r14, %r9
	jb	.L183
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L225:
	addq	$1, %r9
	cmpq	%r9, %r14
	je	.L178
.L183:
	cmpb	$32, (%r9)
	je	.L225
.L182:
	cmpq	%r9, %r14
	je	.L178
	movzbl	(%r9), %eax
	leal	-48(%rax), %edx
	cmpb	$1, %dl
	ja	.L178
	movsbl	%al, %edx
	leaq	1(%r9), %rbx
	subl	$48, %edx
	imull	$1000, %edx, %edx
	cmpq	%rbx, %r14
	je	.L174
	cmpb	$46, 1(%r9)
	jne	.L185
	leaq	2(%r9), %rbx
	movl	$100, %ecx
	cmpq	%r14, %rbx
	jne	.L186
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L227:
	movzbl	%al, %eax
	imull	%ecx, %eax
	addl	%eax, %edx
	movslq	%ecx, %rax
	sarl	$31, %ecx
	imulq	$1717986919, %rax, %rax
	sarq	$34, %rax
	subl	%ecx, %eax
	movl	%eax, %ecx
.L192:
	addq	$1, %rbx
	cmpq	%rbx, %r14
	je	.L188
.L186:
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L188
	testl	%ecx, %ecx
	jg	.L227
	jne	.L192
	cmpb	$5, %al
	movl	$-1, %ecx
	sbbl	$-1, %edx
	jmp	.L192
.L185:
	cmpq	%rbx, %r14
	ja	.L194
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L228:
	addq	$1, %rbx
	cmpq	%rbx, %r14
	je	.L174
.L194:
	cmpb	$32, (%rbx)
	je	.L228
.L184:
	cmpq	%rbx, %r14
	je	.L174
.L195:
	movzbl	(%rbx), %eax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L178:
	movl	$1, 0(%r13)
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
.L171:
	cmpb	$0, -340(%rbp)
	je	.L155
	movq	-352(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L155
.L188:
	cmpl	$1000, %edx
	jg	.L178
	cmpq	%rbx, %r14
	ja	.L194
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$1, 0(%r13)
	jmp	.L155
.L222:
	movq	-352(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L161
.L226:
	movq	%r14, %rbx
	jmp	.L184
.L158:
	movq	-368(%rbp), %rax
	movl	$7, 0(%r13)
	movq	$0, (%rax)
	jmp	.L155
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2392:
	.size	_ZN6icu_6718LocalePriorityListC2ENS_11StringPieceER10UErrorCode, .-_ZN6icu_6718LocalePriorityListC2ENS_11StringPieceER10UErrorCode
	.globl	_ZN6icu_6718LocalePriorityListC1ENS_11StringPieceER10UErrorCode
	.set	_ZN6icu_6718LocalePriorityListC1ENS_11StringPieceER10UErrorCode,_ZN6icu_6718LocalePriorityListC2ENS_11StringPieceER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
