	.file	"ucharstrieiterator.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie8IteratorC2ENS_14ConstChar16PtrEiR10UErrorCode
	.type	_ZN6icu_6710UCharsTrie8IteratorC2ENS_14ConstChar16PtrEiR10UErrorCode, @function
_ZN6icu_6710UCharsTrie8IteratorC2ENS_14ConstChar16PtrEiR10UErrorCode:
.LFB2348:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movl	%edx, 104(%rdi)
	movl	(%rcx), %edx
	movq	$-1, 24(%rdi)
	movq	%rax, (%rdi)
	movq	%rax, 8(%rdi)
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 40(%rdi)
	movl	$2, %eax
	movb	$0, 32(%rdi)
	movw	%ax, 48(%rdi)
	movl	$0, 108(%rdi)
	movq	$0, 112(%rdi)
	testl	%edx, %edx
	jle	.L9
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movq	%r13, 112(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3:
	.cfi_restore_state
	movq	$0, 112(%rbx)
	movl	$7, (%r12)
	jmp	.L1
	.cfi_endproc
.LFE2348:
	.size	_ZN6icu_6710UCharsTrie8IteratorC2ENS_14ConstChar16PtrEiR10UErrorCode, .-_ZN6icu_6710UCharsTrie8IteratorC2ENS_14ConstChar16PtrEiR10UErrorCode
	.globl	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode
	.set	_ZN6icu_6710UCharsTrie8IteratorC1ENS_14ConstChar16PtrEiR10UErrorCode,_ZN6icu_6710UCharsTrie8IteratorC2ENS_14ConstChar16PtrEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie8IteratorC2ERKS0_iR10UErrorCode
	.type	_ZN6icu_6710UCharsTrie8IteratorC2ERKS0_iR10UErrorCode, @function
_ZN6icu_6710UCharsTrie8IteratorC2ERKS0_iR10UErrorCode:
.LFB2351:
	.cfi_startproc
	endbr64
	movq	16(%rsi), %rax
	movdqu	8(%rsi), %xmm0
	movb	$0, 32(%rdi)
	movl	(%rcx), %r8d
	movl	%edx, 104(%rdi)
	movq	%rax, 16(%rdi)
	movl	24(%rsi), %eax
	movl	$2, %esi
	movw	%si, 48(%rdi)
	movl	%eax, 24(%rdi)
	movl	%eax, 28(%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 40(%rdi)
	movl	$0, 108(%rdi)
	movq	$0, 112(%rdi)
	movups	%xmm0, (%rdi)
	testl	%r8d, %r8d
	jle	.L24
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L13
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movl	(%r12), %eax
	movq	%r13, 112(%rbx)
	testl	%eax, %eax
	jg	.L10
	movl	24(%rbx), %r12d
	testl	%r12d, %r12d
	js	.L10
	movl	104(%rbx), %eax
	addl	$1, %r12d
	testl	%eax, %eax
	jle	.L16
	cmpl	%eax, %r12d
	cmovg	%eax, %r12d
.L16:
	movq	8(%rbx), %r13
	leaq	40(%rbx), %rdi
	movl	%r12d, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movslq	%r12d, %rax
	subl	%r12d, 24(%rbx)
	addq	%rax, %rax
	addq	%rax, 8(%rbx)
.L10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	cmpl	$0, (%r12)
	movq	$0, 112(%rbx)
	jg	.L10
	movl	$7, (%r12)
	jmp	.L10
	.cfi_endproc
.LFE2351:
	.size	_ZN6icu_6710UCharsTrie8IteratorC2ERKS0_iR10UErrorCode, .-_ZN6icu_6710UCharsTrie8IteratorC2ERKS0_iR10UErrorCode
	.globl	_ZN6icu_6710UCharsTrie8IteratorC1ERKS0_iR10UErrorCode
	.set	_ZN6icu_6710UCharsTrie8IteratorC1ERKS0_iR10UErrorCode,_ZN6icu_6710UCharsTrie8IteratorC2ERKS0_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie8IteratorD2Ev
	.type	_ZN6icu_6710UCharsTrie8IteratorD2Ev, @function
_ZN6icu_6710UCharsTrie8IteratorD2Ev:
.LFB2354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L26
	movq	(%rdi), %rax
	call	*8(%rax)
.L26:
	addq	$8, %rsp
	leaq	40(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE2354:
	.size	_ZN6icu_6710UCharsTrie8IteratorD2Ev, .-_ZN6icu_6710UCharsTrie8IteratorD2Ev
	.globl	_ZN6icu_6710UCharsTrie8IteratorD1Ev
	.set	_ZN6icu_6710UCharsTrie8IteratorD1Ev,_ZN6icu_6710UCharsTrie8IteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie8Iterator5resetEv
	.type	_ZN6icu_6710UCharsTrie8Iterator5resetEv, @function
_ZN6icu_6710UCharsTrie8Iterator5resetEv:
.LFB2356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	28(%rdi), %eax
	movq	16(%rdi), %rcx
	movl	104(%rdi), %edx
	movb	$0, 32(%rdi)
	movl	%eax, 24(%rdi)
	leal	1(%rax), %ebx
	movq	%rcx, 8(%rdi)
	testl	%edx, %edx
	jle	.L32
	cmpl	%edx, %ebx
	cmovg	%edx, %ebx
.L32:
	movzwl	48(%r12), %edx
	testl	%ebx, %ebx
	jne	.L33
	testb	$1, %dl
	jne	.L45
.L33:
	testw	%dx, %dx
	js	.L35
	movswl	%dx, %esi
	sarl	$5, %esi
.L36:
	cmpl	%ebx, %esi
	jbe	.L34
	cmpl	$1023, %ebx
	jg	.L37
	movl	%ebx, %esi
	andl	$31, %edx
	sall	$5, %esi
	orl	%esi, %edx
	movw	%dx, 48(%r12)
.L34:
	movslq	%ebx, %rdx
	subl	%ebx, %eax
	movq	112(%r12), %rdi
	xorl	%esi, %esi
	leaq	(%rcx,%rdx,2), %rdx
	movl	%eax, 24(%r12)
	movq	%rdx, 8(%r12)
	call	_ZN6icu_679UVector327setSizeEi@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	52(%r12), %esi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	orl	$-32, %edx
	movl	%ebx, 52(%r12)
	movw	%dx, 48(%r12)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	40(%r12), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	8(%r12), %rcx
	movl	24(%r12), %eax
	jmp	.L34
	.cfi_endproc
.LFE2356:
	.size	_ZN6icu_6710UCharsTrie8Iterator5resetEv, .-_ZN6icu_6710UCharsTrie8Iterator5resetEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710UCharsTrie8Iterator7hasNextEv
	.type	_ZNK6icu_6710UCharsTrie8Iterator7hasNextEv, @function
_ZNK6icu_6710UCharsTrie8Iterator7hasNextEv:
.LFB2357:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	movl	$1, %eax
	je	.L49
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movq	112(%rdi), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE2357:
	.size	_ZNK6icu_6710UCharsTrie8Iterator7hasNextEv, .-_ZNK6icu_6710UCharsTrie8Iterator7hasNextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie8Iterator10branchNextEPKDsiR10UErrorCode
	.type	_ZN6icu_6710UCharsTrie8Iterator10branchNextEPKDsiR10UErrorCode, @function
_ZN6icu_6710UCharsTrie8Iterator10branchNextEPKDsiR10UErrorCode:
.LFB2359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	112(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$5, %edx
	jg	.L66
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L94:
	sarl	$5, %r13d
.L59:
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L60
	cmpl	12(%rcx), %esi
	jle	.L85
.L60:
	movq	%r10, %rdx
	movq	%rcx, %rdi
	movq	%r8, -96(%rbp)
	movl	%r9d, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r10
	testb	%al, %al
	movl	-88(%rbp), %r9d
	movq	-96(%rbp), %r8
	jne	.L62
	movq	112(%r14), %r15
.L63:
	movzwl	2(%rbx), %eax
	cmpl	$64511, %eax
	jle	.L64
	movzwl	4(%rbx), %edx
	cmpl	$65535, %eax
	je	.L93
	subl	$64512, %eax
	leaq	6(%rbx), %r8
	sall	$16, %eax
	orl	%edx, %eax
.L64:
	cltq
	leaq	(%r8,%rax,2), %rbx
	cmpl	$5, %r9d
	jle	.L51
.L86:
	movl	%r9d, %r12d
.L66:
	movzwl	2(%rbx), %eax
	leaq	4(%rbx), %r8
	movq	%r8, %r13
	cmpl	$64511, %eax
	jle	.L52
	leaq	8(%rbx), %rdx
	leaq	6(%rbx), %r13
	cmpl	$65535, %eax
	cmove	%rdx, %r13
.L52:
	movslq	8(%r15), %rax
	movq	(%r14), %r9
	movl	%eax, %esi
	addl	$1, %esi
	js	.L54
	cmpl	12(%r15), %esi
	jle	.L84
.L54:
	movq	%r10, %rdx
	movq	%r15, %rdi
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r9
	testb	%al, %al
	movq	-88(%rbp), %r8
	jne	.L56
	movq	112(%r14), %rcx
.L57:
	movswl	48(%r14), %r13d
	movl	%r12d, %r9d
	sarl	%r9d
	testw	%r13w, %r13w
	jns	.L94
	movl	52(%r14), %r13d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L62:
	movslq	8(%rcx), %rax
	movq	112(%r14), %r15
.L61:
	subl	%r9d, %r12d
	movq	24(%rcx), %rdx
	sall	$16, %r12d
	orl	%r12d, %r13d
	movl	%r13d, (%rdx,%rax,4)
	addl	$1, 8(%rcx)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L56:
	movslq	8(%r15), %rax
	movq	112(%r14), %rcx
.L55:
	movq	24(%r15), %rdx
	subq	%r9, %r13
	sarq	%r13
	movl	%r13d, (%rdx,%rax,4)
	addl	$1, 8(%r15)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r15, %rcx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rcx, %r15
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L93:
	movzwl	6(%rbx), %eax
	sall	$16, %edx
	leaq	8(%rbx), %r8
	orl	%edx, %eax
	cltq
	leaq	(%r8,%rax,2), %rbx
	cmpl	$5, %r9d
	jg	.L86
	.p2align 4,,10
	.p2align 3
.L51:
	movzwl	(%rbx), %eax
	movzwl	2(%rbx), %r13d
	movw	%ax, -72(%rbp)
	movl	%r13d, %eax
	sarl	$15, %r13d
	movl	%eax, %edx
	movl	%eax, %r12d
	andw	$32767, %dx
	andl	$32767, %r12d
	testb	$64, %ah
	jne	.L67
	addq	$4, %rbx
.L68:
	movslq	8(%r15), %rax
	movq	(%r14), %r11
	movl	%eax, %esi
	addl	$1, %esi
	js	.L70
	cmpl	12(%r15), %esi
	jle	.L87
.L70:
	movq	%r10, %rdx
	movq	%r15, %rdi
	movq	%r11, -96(%rbp)
	movl	%r9d, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	-80(%rbp), %r10
	movl	-88(%rbp), %r9d
	testb	%al, %al
	movq	-96(%rbp), %r11
	jne	.L72
	movswl	48(%r14), %r15d
	movq	112(%r14), %rcx
	testw	%r15w, %r15w
	js	.L74
.L97:
	sarl	$5, %r15d
.L75:
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L76
	cmpl	12(%rcx), %esi
	jle	.L77
.L76:
	movq	%r10, %rdx
	movq	%rcx, %rdi
	movl	%r9d, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L78
	movq	-80(%rbp), %rcx
	movl	-88(%rbp), %r9d
	movslq	8(%rcx), %rax
.L77:
	subl	$1, %r9d
	movq	24(%rcx), %rdx
	sall	$16, %r9d
	orl	%r9d, %r15d
	movl	%r15d, (%rdx,%rax,4)
	addl	$1, 8(%rcx)
.L78:
	movzwl	-72(%rbp), %eax
	xorl	%edx, %edx
	leaq	-58(%rbp), %rsi
	leaq	40(%r14), %rdi
	movl	$1, %ecx
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	testl	%r13d, %r13d
	jne	.L95
	movslq	%r12d, %r12
	leaq	(%rbx,%r12,2), %rax
.L50:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L96
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movzwl	4(%rbx), %eax
	cmpw	$32767, %dx
	je	.L69
	subl	$16384, %r12d
	addq	$6, %rbx
	sall	$16, %r12d
	orl	%eax, %r12d
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L72:
	movslq	8(%r15), %rax
	movq	112(%r14), %rcx
.L71:
	movq	%rbx, %rdx
	movq	24(%r15), %rsi
	subq	%r11, %rdx
	sarq	%rdx
	movl	%edx, (%rsi,%rax,4)
	addl	$1, 8(%r15)
	movswl	48(%r14), %r15d
	testw	%r15w, %r15w
	jns	.L97
.L74:
	movl	52(%r14), %r15d
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r15, %rcx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L69:
	movzwl	6(%rbx), %r12d
	sall	$16, %eax
	addq	$8, %rbx
	orl	%eax, %r12d
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L95:
	movq	$0, 8(%r14)
	xorl	%eax, %eax
	movl	%r12d, 108(%r14)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L92:
	movl	%edx, %r9d
	jmp	.L51
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2359:
	.size	_ZN6icu_6710UCharsTrie8Iterator10branchNextEPKDsiR10UErrorCode, .-_ZN6icu_6710UCharsTrie8Iterator10branchNextEPKDsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode
	.type	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode, @function
_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode:
.LFB2358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L98
	movq	8(%rdi), %r12
	movq	%rdi, %r15
	movq	%rsi, %rbx
	testq	%r12, %r12
	je	.L160
.L100:
	movl	24(%r15), %eax
	leaq	40(%r15), %r13
	testl	%eax, %eax
	js	.L109
.L127:
	movq	$0, 8(%r15)
	movl	$-1, 108(%r15)
.L159:
	movl	$1, %eax
.L98:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L161
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	cmpl	$47, %edx
	jg	.L162
.L134:
	testl	%edx, %edx
	jne	.L129
	movzwl	(%r14), %edx
	addq	$2, %r14
.L129:
	addl	$1, %edx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator10branchNextEPKDsiR10UErrorCode
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L159
.L109:
	movzwl	(%r12), %edx
	leaq	2(%r12), %r14
	movl	%edx, %ecx
	cmpl	$63, %edx
	jle	.L110
	cmpb	$0, 32(%r15)
	je	.L111
	cmpl	$16447, %edx
	jle	.L112
	leaq	4(%r12), %r14
	addq	$6, %r12
	cmpl	$32703, %edx
	cmovg	%r12, %r14
.L112:
	movb	$0, 32(%r15)
	movl	%ecx, %edx
	andl	$63, %edx
.L110:
	movl	104(%r15), %ecx
	testl	%ecx, %ecx
	jle	.L124
	movswl	48(%r15), %eax
	testw	%ax, %ax
	js	.L125
	sarl	$5, %eax
.L126:
	cmpl	%eax, %ecx
	je	.L127
	cmpl	$47, %edx
	jle	.L134
	leal	-47(%rdx), %r12d
	leal	(%rax,%r12), %edx
	cmpl	%edx, %ecx
	jl	.L163
.L133:
	movl	%r12d, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movslq	%r12d, %rdx
	leaq	(%r14,%rdx,2), %r12
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L125:
	movl	52(%r15), %eax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L162:
	leal	-47(%rdx), %r12d
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L160:
	movq	112(%rdi), %rdi
	movl	8(%rdi), %edx
	testl	%edx, %edx
	je	.L98
	jg	.L164
	movb	$1, -80(%rbp)
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	$0, -68(%rbp)
	movl	$0, -72(%rbp)
.L101:
	movq	(%r15), %r14
	leal	-2(%rdx), %esi
	cmpl	$1, %edx
	jg	.L165
.L102:
	call	_ZN6icu_679UVector327setSizeEi@PLT
	movzwl	48(%r15), %eax
	leaq	40(%r15), %rdi
	testb	$1, %al
	je	.L103
	cmpb	$0, -80(%rbp)
	jne	.L166
.L103:
	testw	%ax, %ax
	js	.L105
	movswl	%ax, %edx
	sarl	$5, %edx
.L106:
	cmpl	%r12d, %edx
	jbe	.L104
	testl	$64512, -72(%rbp)
	jne	.L107
	andl	$31, %eax
	sall	$5, %r12d
	orl	%r12d, %eax
	movw	%ax, 48(%r15)
.L104:
	cmpl	$1, -68(%rbp)
	jbe	.L108
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UCharsTrie8Iterator10branchNextEPKDsiR10UErrorCode
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L100
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L111:
	movl	%edx, %eax
	shrl	$15, %eax
	jne	.L167
	cmpl	$16447, %edx
	jg	.L118
	sarl	$6, %edx
	subl	$1, %edx
.L119:
	movl	%edx, 108(%r15)
	movl	104(%r15), %edx
	testl	%edx, %edx
	jle	.L121
	movswl	48(%r15), %eax
	testw	%ax, %ax
	js	.L122
	sarl	$5, %eax
.L123:
	cmpl	%eax, %edx
	je	.L117
.L121:
	movq	%r12, 8(%r15)
	movl	$1, %eax
	movb	$1, 32(%r15)
	jmp	.L98
.L167:
	movl	%ecx, %eax
	andw	$32767, %dx
	andl	$32767, %eax
	andb	$64, %ch
	je	.L115
	movzwl	2(%r12), %ecx
	cmpw	$32767, %dx
	je	.L116
	subl	$16384, %eax
	sall	$16, %eax
	orl	%ecx, %eax
.L115:
	movl	%eax, 108(%r15)
.L117:
	movq	$0, 8(%r15)
	movl	$1, %eax
	jmp	.L98
.L107:
	orl	$-32, %eax
	movl	%r12d, 52(%r15)
	movw	%ax, 48(%r15)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L108:
	movzwl	(%r14), %eax
	leaq	-58(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	leaq	2(%r14), %r12
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L105:
	movl	52(%r15), %edx
	jmp	.L106
.L163:
	subl	%eax, %ecx
	leaq	40(%r15), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$1, %eax
	movq	$0, 8(%r15)
	movl	$-1, 108(%r15)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L165:
	movq	24(%rdi), %rdx
	movslq	%esi, %rax
	movslq	(%rdx,%rax,4), %rax
	leaq	(%r14,%rax,2), %r14
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L164:
	movq	24(%rdi), %rcx
	leal	-1(%rdx), %eax
	cltq
	movl	(%rcx,%rax,4), %eax
	movl	%eax, %esi
	movl	%eax, -72(%rbp)
	shrl	$16, %esi
	andl	$65535, %eax
	movl	%esi, -68(%rbp)
	movl	%esi, %r13d
	movl	%eax, %r12d
	sete	-80(%rbp)
	jmp	.L101
.L118:
	movzwl	2(%r12), %esi
	cmpl	$32703, %edx
	jg	.L120
	movl	%edx, %eax
	andl	$32704, %eax
	subl	$16448, %eax
	sall	$10, %eax
	orl	%esi, %eax
	movl	%eax, %edx
	jmp	.L119
.L166:
	movq	%rdi, -80(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	-80(%rbp), %rdi
	jmp	.L104
.L116:
	movzwl	4(%r12), %eax
	sall	$16, %ecx
	orl	%ecx, %eax
	jmp	.L115
.L122:
	movl	52(%r15), %eax
	jmp	.L123
.L120:
	movzwl	4(%r12), %eax
	sall	$16, %esi
	movl	%esi, %edx
	orl	%eax, %edx
	jmp	.L119
.L161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2358:
	.size	_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode, .-_ZN6icu_6710UCharsTrie8Iterator4nextER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
