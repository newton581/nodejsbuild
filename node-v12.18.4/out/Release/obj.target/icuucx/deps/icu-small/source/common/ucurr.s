	.file	"ucurr.cpp"
	.text
	.p2align 4
	.type	_ZL13deleteUnicodePv, @function
_ZL13deleteUnicodePv:
.LFB3242:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE3242:
	.size	_ZL13deleteUnicodePv, .-_ZL13deleteUnicodePv
	.p2align 4
	.type	_ZL22currencyNameComparatorPKvS0_, @function
_ZL22currencyNameComparatorPKvS0_:
.LFB3251:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %r8d
	movl	16(%rsi), %r9d
	movl	%r8d, %eax
	cmpl	%r8d, %r9d
	cmovle	%r9d, %eax
	testl	%eax, %eax
	jle	.L5
	movq	8(%rsi), %rcx
	movq	8(%rdi), %rdi
	leal	-1(%rax), %esi
	xorl	%eax, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L12:
	ja	.L9
	leaq	1(%rax), %rdx
	cmpq	%rax, %rsi
	je	.L5
	movq	%rdx, %rax
.L7:
	movzwl	(%rcx,%rax,2), %edx
	cmpw	%dx, (%rdi,%rax,2)
	jnb	.L12
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	cmpl	%r8d, %r9d
	movl	$-1, %edx
	setl	%al
	cmovg	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3251:
	.size	_ZL22currencyNameComparatorPKvS0_, .-_ZL22currencyNameComparatorPKvS0_
	.p2align 4
	.type	ucurr_countCurrencyList, @function
ucurr_countCurrencyList:
.LFB3276:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	(%rax), %edx
	cmpl	$2147483647, %edx
	je	.L14
	leaq	8+_ZL13gCurrencyList(%rip), %rax
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L17:
	movl	(%rax), %ecx
	andl	%edx, %ecx
	cmpl	%ecx, %edx
	sete	%cl
	addq	$16, %rax
	movzbl	%cl, %ecx
	addl	%ecx, %r8d
	cmpq	$0, -8(%rax)
	jne	.L17
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	16+_ZL13gCurrencyList(%rip), %rax
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$16, %rax
	addl	$1, %r8d
	cmpq	$0, -16(%rax)
	jne	.L18
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3276:
	.size	ucurr_countCurrencyList, .-ucurr_countCurrencyList
	.p2align 4
	.type	ucurr_nextCurrencyList, @function
ucurr_nextCurrencyList:
.LFB3277:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	_ZL13gCurrencyList(%rip), %r10
	movl	4(%r8), %eax
	movq	%rax, %rdx
	salq	$4, %rax
	leaq	8(%r10,%rax), %rcx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L35:
	movl	(%r8), %eax
	leal	1(%rdx), %edi
	movl	%edi, 4(%r8)
	cmpl	$2147483647, %eax
	je	.L22
	movl	(%rcx), %r9d
	addq	$16, %rcx
	andl	%eax, %r9d
	cmpl	%r9d, %eax
	je	.L22
	movl	%edi, %edx
.L23:
	cmpl	$302, %edx
	jbe	.L35
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L20
	movl	$0, (%rsi)
.L20:
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	testq	%rsi, %rsi
	je	.L24
	movl	$3, (%rsi)
.L24:
	salq	$4, %rdx
	movq	(%r10,%rdx), %rax
	ret
	.cfi_endproc
.LFE3277:
	.size	ucurr_nextCurrencyList, .-ucurr_nextCurrencyList
	.p2align 4
	.type	ucurr_resetCurrencyList, @function
ucurr_resetCurrencyList:
.LFB3278:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movl	$0, 4(%rax)
	ret
	.cfi_endproc
.LFE3278:
	.size	ucurr_resetCurrencyList, .-ucurr_resetCurrencyList
	.p2align 4
	.type	_ZL18deleteIsoCodeEntryPv, @function
_ZL18deleteIsoCodeEntryPv:
.LFB3241:
	.cfi_startproc
	endbr64
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3241:
	.size	_ZL18deleteIsoCodeEntryPv, .-_ZL18deleteIsoCodeEntryPv
	.p2align 4
	.type	ucurr_closeCurrencyList, @function
ucurr_closeCurrencyList:
.LFB3279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3279:
	.size	ucurr_closeCurrencyList, .-ucurr_closeCurrencyList
	.p2align 4
	.type	_ZL18searchCurrencyNamePK18CurrencyNameStructiPKDsiPiS4_S4_, @function
_ZL18searchCurrencyNamePK18CurrencyNameStructiPKDsiPiS4_S4_:
.LFB3257:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movl	%ecx, -60(%rbp)
	movl	$-1, (%rax)
	leal	-1(%rcx), %eax
	movl	$0, (%r9)
	movq	%rax, -56(%rbp)
	testl	%ecx, %ecx
	jle	.L40
	movq	%rdx, %r12
	movq	%r8, %r13
	movq	%r9, %r14
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L73:
	movzwl	(%r12,%rdx,2), %r10d
	leaq	(%rdx,%rdx), %r9
	cmpl	%ebx, %esi
	jl	.L40
	movl	%esi, %r8d
	movl	%ebx, %ecx
	movl	%edx, %r11d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	movq	8(%r15), %r15
	cmpw	(%r15,%r9), %r10w
	ja	.L87
	jnb	.L92
	leal	-1(%rax), %r8d
	cmpl	%r8d, %ecx
	jg	.L40
.L43:
	leal	(%r8,%rcx), %eax
	sarl	%eax
	movslq	%eax, %r15
	leaq	(%r15,%r15,2), %r15
	leaq	(%rdi,%r15,8), %r15
	cmpl	%edx, 16(%r15)
	jg	.L44
.L87:
	leal	1(%rax), %ecx
	cmpl	%r8d, %ecx
	jle	.L43
.L40:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	cmpl	%ebx, %eax
	jle	.L55
	movl	%eax, %r15d
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L52:
	movq	8(%r8), %r8
	cmpw	(%r8,%r9), %r10w
	ja	.L88
	movl	%ecx, %r15d
	cmpl	%r15d, %ebx
	jge	.L55
.L49:
	leal	(%r15,%rbx), %ecx
	sarl	%ecx
	movslq	%ecx, %r8
	leaq	(%r8,%r8,2), %r8
	leaq	(%rdi,%r8,8), %r8
	cmpl	%r11d, 16(%r8)
	jg	.L52
.L88:
	leal	1(%rcx), %ebx
	cmpl	%r15d, %ebx
	jl	.L49
	.p2align 4,,10
	.p2align 3
.L55:
	cmpl	%esi, %eax
	jge	.L50
.L93:
	leal	(%rsi,%rax), %ecx
	sarl	%ecx
	movslq	%ecx, %r8
	leaq	(%r8,%r8,2), %r8
	leaq	(%rdi,%r8,8), %r8
	cmpl	%r11d, 16(%r8)
	jl	.L89
	movq	8(%r8), %r8
	cmpw	(%r8,%r9), %r10w
	jnb	.L89
	movl	%ecx, %esi
	cmpl	%esi, %eax
	jl	.L93
.L50:
	movslq	%esi, %rax
	movl	0(%r13), %r8d
	leaq	(%rax,%rax,2), %rax
	movq	8(%rdi,%rax,8), %rax
	cmpw	(%rax,%r9), %r10w
	movslq	%ebx, %rax
	leaq	(%rax,%rax,2), %rcx
	sbbl	$0, %esi
	leal	1(%rdx), %eax
	leaq	0(,%rcx,8), %r9
	cmpl	%eax, 16(%rdi,%rcx,8)
	jne	.L94
	cmpl	%r8d, %r11d
	jl	.L60
	movl	%eax, 0(%r13)
.L60:
	movl	%eax, (%r14)
	movq	16(%rbp), %rax
	movl	%ebx, (%rax)
.L61:
	movl	%esi, %eax
	subl	%ebx, %eax
	cmpl	$9, %eax
	jle	.L95
	leaq	1(%rdx), %rax
	cmpq	%rdx, -56(%rbp)
	je	.L40
	movq	%rax, %rdx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L89:
	leal	1(%rcx), %eax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L95:
	movl	0(%r13), %ecx
	cmpl	%ebx, %esi
	jl	.L40
	leal	1(%rsi), %eax
	leaq	8(%rdi,%r9), %r15
	movl	-60(%rbp), %r8d
	movq	%r14, %r9
	movl	%eax, -56(%rbp)
	movl	%ebx, %r14d
	movq	%r12, %rbx
	movl	%ecx, %r12d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L96:
	cmpl	%r10d, %r8d
	jl	.L67
	movslq	%r10d, %rdx
	movq	(%r15), %rdi
	movq	%rbx, %rsi
	movq	%r9, -72(%rbp)
	addq	%rdx, %rdx
	movl	%r8d, -64(%rbp)
	movl	%r10d, -60(%rbp)
	call	memcmp@PLT
	movl	-60(%rbp), %r10d
	movl	-64(%rbp), %r8d
	testl	%eax, %eax
	movq	-72(%rbp), %r9
	jne	.L67
	cmpl	%r10d, 0(%r13)
	movl	%r10d, %eax
	cmovge	0(%r13), %eax
	movl	%eax, 0(%r13)
	movq	16(%rbp), %rax
	movl	%r14d, (%rax)
	movl	%r10d, (%r9)
.L66:
	addl	$1, %r14d
	addq	$24, %r15
	cmpl	-56(%rbp), %r14d
	je	.L40
.L72:
	movl	8(%r15), %r10d
	cmpl	(%r9), %r10d
	jg	.L96
.L67:
	cmpl	%r10d, %r8d
	movl	%r10d, %edx
	cmovle	%r8d, %edx
	cmpl	%edx, %r12d
	jge	.L66
	movq	(%r15), %r10
	movslq	%r12d, %rax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L97:
	leal	1(%rax), %edi
	cmpl	%eax, 0(%r13)
	jg	.L91
	movl	%edi, 0(%r13)
.L91:
	addq	$1, %rax
	cmpl	%eax, %edx
	jle	.L66
.L71:
	movzwl	(%rbx,%rax,2), %edi
	cmpw	%di, (%r10,%rax,2)
	je	.L97
	jmp	.L66
.L94:
	cmpl	%r8d, %r11d
	jl	.L61
	movl	%eax, 0(%r13)
	jmp	.L61
	.cfi_endproc
.LFE3257:
	.size	_ZL18searchCurrencyNamePK18CurrencyNameStructiPKDsiPiS4_S4_, .-_ZL18searchCurrencyNamePK18CurrencyNameStructiPKDsiPiS4_S4_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"icudt67l-curr"
	.text
	.p2align 4
	.type	_ZL13_findMetaDataPKDsR10UErrorCode.part.0, @function
_ZL13_findMetaDataPKDsR10UErrorCode.part.0:
.LFB4295:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	.LC0(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	leaq	_ZL13CURRENCY_DATA(%rip), %rsi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ures_openDirect_67@PLT
	movq	%rbx, %rcx
	leaq	_ZL13CURRENCY_META(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rdx
	call	ures_getByKey_67@PLT
	movl	(%rbx), %edi
	movq	%rax, %r12
	testl	%edi, %edi
	jg	.L106
	leaq	-44(%rbp), %r14
	movq	%r13, %rdi
	movl	$3, %edx
	movl	$0, -52(%rbp)
	movq	%r14, %rsi
	call	u_UCharsToChars_67@PLT
	movq	%r14, %rsi
	leaq	-52(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movb	$0, -41(%rbp)
	call	ures_getByKey_67@PLT
	movl	-52(%rbp), %esi
	movq	%rax, %r13
	testl	%esi, %esi
	jle	.L101
	movq	%rax, %rdi
	call	ures_close_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	_ZL12DEFAULT_META(%rip), %rsi
	call	ures_getByKey_67@PLT
	movl	(%rbx), %ecx
	movq	%rax, %r13
	testl	%ecx, %ecx
	jg	.L102
.L101:
	movq	%rbx, %rdx
	leaq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	ures_getIntVector_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L102
	cmpl	$4, -48(%rbp)
	je	.L107
	movl	$3, (%rbx)
.L102:
	movq	%r12, %rdi
	call	ures_close_67@PLT
	movq	%r13, %rdi
	call	ures_close_67@PLT
	leaq	_ZL16LAST_RESORT_DATA(%rip), %rax
.L98:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L108
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	%rax, %rdi
	call	ures_close_67@PLT
	leaq	_ZL16LAST_RESORT_DATA(%rip), %rax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	ures_close_67@PLT
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movq	-72(%rbp), %rax
	jmp	.L98
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4295:
	.size	_ZL13_findMetaDataPKDsR10UErrorCode.part.0, .-_ZL13_findMetaDataPKDsR10UErrorCode.part.0
	.p2align 4
	.type	currency_cleanup, @function
currency_cleanup:
.LFB3246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	_ZL9currCache(%rip), %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L110
	movl	168(%r12), %eax
	movq	160(%r12), %r14
	testl	%eax, %eax
	jle	.L111
	subl	$1, %eax
	leaq	8(%r14), %r13
	leaq	(%rax,%rax,2), %rax
	leaq	32(%r14,%rax,8), %r15
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L112:
	addq	$24, %r13
	cmpq	%r15, %r13
	je	.L111
.L113:
	testb	$1, 12(%r13)
	je	.L112
	movq	0(%r13), %rdi
	addq	$24, %r13
	call	uprv_free_67@PLT
	cmpq	%r15, %r13
	jne	.L113
.L111:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	movl	184(%r12), %eax
	movq	176(%r12), %r14
	testl	%eax, %eax
	jle	.L114
	subl	$1, %eax
	leaq	8(%r14), %r13
	leaq	(%rax,%rax,2), %rax
	leaq	32(%r14,%rax,8), %r15
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L115:
	addq	$24, %r13
	cmpq	%r13, %r15
	je	.L114
.L116:
	testb	$1, 12(%r13)
	je	.L115
	movq	0(%r13), %rdi
	addq	$24, %r13
	call	uprv_free_67@PLT
	cmpq	%r13, %r15
	jne	.L116
.L114:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	movq	$0, (%rbx)
.L110:
	addq	$8, %rbx
	leaq	80+_ZL9currCache(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L117
	movq	_ZL9gIsoCodes(%rip), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	uhash_close_67@PLT
	movq	$0, _ZL9gIsoCodes(%rip)
.L118:
	movl	$0, _ZL17gIsoCodesInitOnce(%rip)
	mfence
	movq	_ZL17gCurrSymbolsEquiv(%rip), %r12
	testq	%r12, %r12
	je	.L119
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L120
	call	uhash_close_67@PLT
.L120:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L119:
	movq	$0, _ZL17gCurrSymbolsEquiv(%rip)
	movl	$1, %eax
	movl	$0, _ZL25gCurrSymbolsEquivInitOnce(%rip)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3246:
	.size	currency_cleanup, .-currency_cleanup
	.p2align 4
	.type	_ZL16deleteCacheEntryP22CurrencyNameCacheEntry, @function
_ZL16deleteCacheEntryP22CurrencyNameCacheEntry:
.LFB3265:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	168(%rdi), %eax
	movq	160(%rdi), %r13
	testl	%eax, %eax
	jle	.L144
	subl	$1, %eax
	leaq	8(%r13), %rbx
	leaq	(%rax,%rax,2), %rax
	leaq	32(%r13,%rax,8), %r14
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L145:
	addq	$24, %rbx
	cmpq	%r14, %rbx
	je	.L144
.L146:
	testb	$1, 12(%rbx)
	je	.L145
	movq	(%rbx), %rdi
	addq	$24, %rbx
	call	uprv_free_67@PLT
	cmpq	%r14, %rbx
	jne	.L146
.L144:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movl	184(%r12), %eax
	movq	176(%r12), %r13
	testl	%eax, %eax
	jle	.L147
	subl	$1, %eax
	leaq	8(%r13), %rbx
	leaq	(%rax,%rax,2), %rax
	leaq	32(%r13,%rax,8), %r14
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L148:
	addq	$24, %rbx
	cmpq	%rbx, %r14
	je	.L147
.L149:
	testb	$1, 12(%rbx)
	je	.L148
	movq	(%rbx), %rdi
	addq	$24, %rbx
	call	uprv_free_67@PLT
	cmpq	%rbx, %r14
	jne	.L149
.L147:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3265:
	.size	_ZL16deleteCacheEntryP22CurrencyNameCacheEntry, .-_ZL16deleteCacheEntryP22CurrencyNameCacheEntry
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3596:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3596:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3599:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L172
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L160
	cmpb	$0, 12(%rbx)
	jne	.L173
.L164:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L160:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L164
	.cfi_endproc
.LFE3599:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3602:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L176
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3602:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3605:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L179
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3605:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L185
.L181:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L186
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L186:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3607:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3608:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3608:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3609:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3609:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3610:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3610:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3611:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3611:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3612:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3612:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3613:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L202
	testl	%edx, %edx
	jle	.L202
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L205
.L194:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L194
	.cfi_endproc
.LFE3613:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L209
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L209
	testl	%r12d, %r12d
	jg	.L216
	cmpb	$0, 12(%rbx)
	jne	.L217
.L211:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L211
.L217:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L209:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3614:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L219
	movq	(%rdi), %r8
.L220:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L223
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L223
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L223:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3615:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3616:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L230
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3616:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3617:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3617:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3618:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3618:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3619:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3619:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3621:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3621:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3623:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3623:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713EquivIterator4nextEv
	.type	_ZN6icu_6713EquivIterator4nextEv, @function
_ZN6icu_6713EquivIterator4nextEv:
.LFB3236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	16(%rdi), %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L245
	movq	8(%rbx), %rsi
	movswl	8(%rax), %edx
	movq	%rax, %r12
	movswl	8(%rsi), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L239
	testw	%dx, %dx
	js	.L240
	sarl	$5, %edx
.L241:
	testw	%ax, %ax
	js	.L242
	sarl	$5, %eax
.L243:
	testb	%cl, %cl
	jne	.L244
	cmpl	%edx, %eax
	je	.L253
.L244:
	movq	%r12, 16(%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L239:
	testb	%cl, %cl
	je	.L244
.L245:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movl	12(%rsi), %eax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L240:
	movl	12(%r12), %edx
	jmp	.L241
	.cfi_endproc
.LFE3236:
	.size	_ZN6icu_6713EquivIterator4nextEv, .-_ZN6icu_6713EquivIterator4nextEv
	.section	.rodata.str1.1
.LC1:
	.string	"currency"
.LC2:
	.string	"id"
	.text
	.p2align 4
	.globl	ucurr_forLocale_67
	.type	ucurr_forLocale_67, @function
ucurr_forLocale_67:
.LFB3247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L293
	movl	%edx, %r13d
	movq	%rcx, %r12
	testl	%edx, %edx
	js	.L257
	movq	%rdi, %r15
	movq	%rsi, %r14
	testq	%rsi, %rsi
	jne	.L258
	testl	%edx, %edx
	jle	.L258
.L257:
	movl	$1, (%r12)
.L293:
	xorl	%eax, %eax
.L254:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L294
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	leaq	-228(%rbp), %r9
	movl	$4, %ecx
	movq	%r15, %rdi
	movl	$0, -236(%rbp)
	leaq	-236(%rbp), %rbx
	movq	%r9, %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r9, -248(%rbp)
	movq	%rbx, %r8
	call	uloc_getKeywordValue_67@PLT
	movl	-236(%rbp), %edx
	movl	%eax, -232(%rbp)
	testl	%edx, %edx
	jg	.L259
	cmpl	$3, %eax
	movq	-248(%rbp), %r9
	je	.L295
.L259:
	leaq	-224(%rbp), %r9
	xorl	%esi, %esi
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	%r9, %rdx
	movl	$157, %ecx
	movq	%r9, -248(%rbp)
	call	ulocimp_getRegionForSupplementalData_67@PLT
	movl	(%r12), %eax
	movq	-248(%rbp), %r9
	testl	%eax, %eax
	jg	.L293
	movq	%r9, %rdi
	movl	$95, %esi
	movq	%r9, -248(%rbp)
	call	strchr@PLT
	movq	-248(%rbp), %r9
	testq	%rax, %rax
	je	.L264
	movb	$0, (%rax)
.L264:
	cmpb	$0, -224(%rbp)
	jne	.L265
	movl	$2, -236(%rbp)
	xorl	%ebx, %ebx
	movl	$2, %edx
.L266:
	movq	%r9, %rdi
	movl	$95, %esi
	movq	%r9, -248(%rbp)
	movl	%edx, -256(%rbp)
	call	strchr@PLT
	movq	-248(%rbp), %r9
	testq	%rax, %rax
	je	.L296
	movq	%r12, %rcx
	movq	%r9, %rsi
	movq	%r15, %rdi
	movl	$157, %edx
	movq	%r9, -248(%rbp)
	call	uloc_getParent_67@PLT
	movq	%r12, %rcx
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	-248(%rbp), %r9
	movl	$-128, (%r12)
	movq	%r9, %rdi
	call	ucurr_forLocale_67
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L296:
	movl	-256(%rbp), %edx
.L269:
	movl	%edx, (%r12)
.L270:
	movl	-232(%rbp), %r8d
	testl	%edx, %edx
	jle	.L297
.L271:
	movq	%r12, %rcx
	movl	%r8d, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	u_terminateUChars_67@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%rbx, %rdx
	leaq	_ZL13CURRENCY_DATA(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	movq	%r9, -256(%rbp)
	movl	$0, -236(%rbp)
	call	ures_openDirect_67@PLT
	movq	%rbx, %rcx
	leaq	_ZL12CURRENCY_MAP(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rdx
	movq	%rax, -248(%rbp)
	call	ures_getByKey_67@PLT
	movq	-256(%rbp), %r9
	movq	-248(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%r9, -264(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, -256(%rbp)
	call	ures_getByIndex_67@PLT
	leaq	-232(%rbp), %rdx
	movq	%rbx, %rcx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -248(%rbp)
	call	ures_getStringByKey_67@PLT
	movq	-248(%rbp), %rdi
	movq	%rax, %rbx
	call	ures_close_67@PLT
	movq	-256(%rbp), %r8
	movq	%r8, %rdi
	call	ures_close_67@PLT
	movl	-236(%rbp), %edx
	movq	-264(%rbp), %r9
	testl	%edx, %edx
	jg	.L266
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.L269
	testl	%edx, %edx
	jne	.L269
	movl	%eax, %edx
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L297:
	cmpl	%r13d, %r8d
	jge	.L271
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	u_strcpy_67@PLT
	movl	-232(%rbp), %r8d
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L295:
	movl	$3, %esi
	movq	%r9, %rdi
	call	uprv_isInvariantString_67@PLT
	testb	%al, %al
	je	.L259
	movl	-232(%rbp), %edx
	movq	-248(%rbp), %r9
	cmpl	%r13d, %edx
	jl	.L298
.L261:
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	u_terminateUChars_67@PLT
	jmp	.L254
.L298:
	movq	%r9, %rdi
	movq	%r9, -248(%rbp)
	call	T_CString_toUpperCase_67@PLT
	movq	-248(%rbp), %r9
	movl	-232(%rbp), %edx
	movq	%r14, %rsi
	movq	%r9, %rdi
	call	u_charsToUChars_67@PLT
	movl	-232(%rbp), %edx
	jmp	.L261
.L294:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3247:
	.size	ucurr_forLocale_67, .-ucurr_forLocale_67
	.section	.rodata.str1.1
.LC3:
	.string	"/"
	.text
	.p2align 4
	.globl	ucurr_getName_67
	.type	ucurr_getName_67, @function
ucurr_getName_67:
.LFB3249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -352(%rbp)
	movl	(%r9), %ecx
	movq	%r8, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L299
	movq	%r9, %rbx
	cmpl	$2, %edx
	ja	.L302
	leaq	-324(%rbp), %r14
	leaq	-224(%rbp), %r13
	movl	%edx, -360(%rbp)
	movq	%rdi, %r12
	movq	%r14, %rcx
	movq	%rsi, %rdi
	movl	$157, %edx
	movq	%r13, %rsi
	movl	$0, -324(%rbp)
	call	uloc_getName_67@PLT
	movl	-324(%rbp), %eax
	testl	%eax, %eax
	jg	.L302
	cmpl	$-124, %eax
	je	.L302
	leaq	-228(%rbp), %r15
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	u_UCharsToChars_67@PLT
	movq	%r15, %rdi
	movb	$0, -225(%rbp)
	call	T_CString_toUpperCase_67@PLT
	movq	%r13, %rsi
	movq	%r14, %rdx
	leaq	.LC0(%rip), %rdi
	movl	$0, -324(%rbp)
	call	ures_open_67@PLT
	movl	-360(%rbp), %r10d
	movq	%rax, %r13
	cmpl	$2, %r10d
	je	.L327
.L304:
	movq	%r14, %rcx
	movq	%r13, %rdx
	leaq	_ZL10CURRENCIES(%rip), %rsi
	movq	%r13, %rdi
	movl	%r10d, -360(%rbp)
	call	ures_getByKey_67@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	ures_getByKeyWithFallback_67@PLT
	movl	-360(%rbp), %r10d
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	-344(%rbp), %rdx
	movl	%r10d, %esi
	call	ures_getStringByIndex_67@PLT
.L307:
	movl	-324(%rbp), %edx
	testl	%edx, %edx
	jle	.L328
	cmpq	$0, -352(%rbp)
	je	.L312
	movq	-352(%rbp), %rax
	movb	$0, (%rax)
.L312:
	movq	%r12, %rdi
	call	u_strlen_67@PLT
	movq	-344(%rbp), %rcx
	movl	%eax, (%rcx)
	movq	%r12, %rax
	movl	$-127, (%rbx)
.L313:
	testq	%r13, %r13
	je	.L299
	movq	%r13, %rdi
	movq	%rax, -344(%rbp)
	call	ures_close_67@PLT
	movq	-344(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L299:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L329
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%eax, %eax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L328:
	cmpl	$-127, %edx
	je	.L309
	cmpl	$-128, %edx
	je	.L330
.L310:
	movq	-352(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L313
	movb	$0, (%rbx)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L327:
	leaq	-320(%rbp), %r10
	leaq	-291(%rbp), %rax
	movl	$0, -248(%rbp)
	movq	%r10, %rdi
	leaq	-304(%rbp), %r9
	movq	%rax, -304(%rbp)
	xorl	%eax, %eax
	leaq	_ZL17CURRENCIES_NARROW(%rip), %rsi
	movq	%r10, -368(%rbp)
	movq	%r9, -360(%rbp)
	movw	%ax, -292(%rbp)
	movl	$40, -296(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-360(%rbp), %r9
	movl	-312(%rbp), %edx
	movq	%r14, %rcx
	movq	-320(%rbp), %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-368(%rbp), %r10
	leaq	.LC3(%rip), %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-360(%rbp), %r9
	movl	-312(%rbp), %edx
	movq	%r14, %rcx
	movq	-320(%rbp), %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-368(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-360(%rbp), %r9
	movl	-312(%rbp), %edx
	movq	%r14, %rcx
	movq	-320(%rbp), %rsi
	movq	%r9, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-344(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	-304(%rbp), %rsi
	call	ures_getStringByKeyWithFallback_67@PLT
	cmpl	$2, -324(%rbp)
	movl	$2, %r10d
	jne	.L305
	movl	$-128, (%rbx)
	xorl	%r10d, %r10d
	movl	$0, -324(%rbp)
.L305:
	cmpb	$0, -292(%rbp)
	jne	.L331
.L306:
	testq	%rax, %rax
	jne	.L307
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L330:
	cmpl	$-127, (%rbx)
	je	.L310
.L309:
	movl	%edx, (%rbx)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-304(%rbp), %rdi
	movq	%rax, -368(%rbp)
	movl	%r10d, -360(%rbp)
	call	uprv_free_67@PLT
	movq	-368(%rbp), %rax
	movl	-360(%rbp), %r10d
	jmp	.L306
.L329:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3249:
	.size	ucurr_getName_67, .-ucurr_getName_67
	.section	.rodata.str1.1
.LC4:
	.string	"other"
	.text
	.p2align 4
	.globl	ucurr_getPluralName_67
	.type	ucurr_getPluralName_67, @function
ucurr_getPluralName_67:
.LFB3250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -256(%rbp)
	movl	(%r9), %r10d
	movq	%rcx, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L332
	movq	%rsi, %r14
	movq	%r9, %rbx
	leaq	-232(%rbp), %r15
	movq	%rdi, %r13
	leaq	-224(%rbp), %r9
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%r8, %r12
	movl	$157, %edx
	movq	%r9, %rsi
	movl	$0, -232(%rbp)
	movq	%r9, -264(%rbp)
	call	uloc_getName_67@PLT
	movl	-232(%rbp), %eax
	testl	%eax, %eax
	jg	.L334
	cmpl	$-124, %eax
	je	.L334
	leaq	-228(%rbp), %r8
	movl	$3, %edx
	movq	%r13, %rdi
	movq	%r8, %rsi
	movq	%r8, -272(%rbp)
	call	u_UCharsToChars_67@PLT
	movq	-264(%rbp), %r9
	movq	%r15, %rdx
	leaq	.LC0(%rip), %rdi
	movb	$0, -225(%rbp)
	movl	$0, -232(%rbp)
	movq	%r9, %rsi
	call	ures_open_67@PLT
	movq	%r15, %rcx
	leaq	_ZL15CURRENCYPLURALS(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rdx
	call	ures_getByKey_67@PLT
	movq	-272(%rbp), %r8
	movq	%r15, %rcx
	movq	%rax, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	ures_getByKeyWithFallback_67@PLT
	movq	-248(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, -248(%rbp)
	call	ures_getStringByKeyWithFallback_67@PLT
	movl	-232(%rbp), %ecx
	movq	-248(%rbp), %rdi
	testl	%ecx, %ecx
	jle	.L336
	movq	%r12, %rdx
	movq	%r15, %rcx
	leaq	.LC4(%rip), %rsi
	movq	%rdi, -248(%rbp)
	movl	$0, -232(%rbp)
	call	ures_getStringByKeyWithFallback_67@PLT
	movl	-232(%rbp), %edx
	movq	-248(%rbp), %rdi
	testl	%edx, %edx
	jg	.L345
.L336:
	movq	%rax, -248(%rbp)
	call	ures_close_67@PLT
	movl	-232(%rbp), %edx
	testl	%edx, %edx
	jle	.L346
	movq	%r13, %rdi
	call	u_strlen_67@PLT
	movl	%eax, (%r12)
	movq	%r13, %rax
	movl	$-127, (%rbx)
	.p2align 4,,10
	.p2align 3
.L332:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L347
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%eax, %eax
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L346:
	cmpl	$-127, %edx
	movq	-248(%rbp), %rax
	je	.L338
	cmpl	$-128, %edx
	jne	.L332
	cmpl	$-127, (%rbx)
	je	.L332
.L338:
	movl	%edx, (%rbx)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L345:
	call	ures_close_67@PLT
	movq	%rbx, %r9
	movq	%r12, %r8
	movl	$1, %edx
	movq	-256(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	ucurr_getName_67
	jmp	.L332
.L347:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3250:
	.size	ucurr_getPluralName_67, .-ucurr_getPluralName_67
	.p2align 4
	.globl	uprv_getStaticCurrencyName_67
	.type	uprv_getStaticCurrencyName_67, @function
uprv_getStaticCurrencyName_67:
.LFB3271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-44(%rbp), %r8
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	ucurr_getName_67
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L354
.L348:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L355
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	-44(%rbp), %ebx
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L350
	sarl	$5, %edx
.L351:
	movl	%ebx, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L350:
	movl	12(%r12), %edx
	jmp	.L351
.L355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3271:
	.size	uprv_getStaticCurrencyName_67, .-uprv_getStaticCurrencyName_67
	.p2align 4
	.globl	ucurr_getDefaultFractionDigits_67
	.type	ucurr_getDefaultFractionDigits_67, @function
ucurr_getDefaultFractionDigits_67:
.LFB3272:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L368
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	testq	%rdi, %rdi
	je	.L358
	cmpw	$0, (%rdi)
	jne	.L359
.L358:
	movl	$1, (%rsi)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZL13_findMetaDataPKDsR10UErrorCode.part.0
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	(%rax), %eax
	ret
	.cfi_endproc
.LFE3272:
	.size	ucurr_getDefaultFractionDigits_67, .-ucurr_getDefaultFractionDigits_67
	.p2align 4
	.globl	ucurr_getDefaultFractionDigitsForUsage_67
	.type	ucurr_getDefaultFractionDigitsForUsage_67, @function
ucurr_getDefaultFractionDigitsForUsage_67:
.LFB3273:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L392
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%esi, %esi
	je	.L371
	cmpl	$1, %esi
	je	.L372
	movl	$16, (%rdx)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L377
	cmpw	$0, (%rdi)
	je	.L377
	movq	%rdx, %rsi
	call	_ZL13_findMetaDataPKDsR10UErrorCode.part.0
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	movl	$2, %eax
	movl	$1, (%rdx)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L377
	cmpw	$0, (%rdi)
	je	.L377
	movq	%rdx, %rsi
	call	_ZL13_findMetaDataPKDsR10UErrorCode.part.0
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	8(%rax), %eax
	ret
	.cfi_endproc
.LFE3273:
	.size	ucurr_getDefaultFractionDigitsForUsage_67, .-ucurr_getDefaultFractionDigitsForUsage_67
	.p2align 4
	.globl	ucurr_getRoundingIncrement_67
	.type	ucurr_getRoundingIncrement_67, @function
ucurr_getRoundingIncrement_67:
.LFB3274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L394
	cmpw	$0, (%rdi)
	jne	.L395
.L394:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L403
.L396:
	addq	$8, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	call	_ZL13_findMetaDataPKDsR10UErrorCode.part.0
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L396
	movslq	(%rax), %rdx
	cmpl	$9, %edx
	jbe	.L397
	movl	$3, (%rbx)
	addq	$8, %rsp
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movl	$1, (%rbx)
	jmp	.L396
.L397:
	movl	4(%rax), %eax
	cmpl	$1, %eax
	jle	.L396
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm0
	leaq	_ZL5POW10(%rip), %rax
	cvtsi2sdl	(%rax,%rdx,4), %xmm1
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	divsd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE3274:
	.size	ucurr_getRoundingIncrement_67, .-ucurr_getRoundingIncrement_67
	.p2align 4
	.globl	ucurr_getRoundingIncrementForUsage_67
	.type	ucurr_getRoundingIncrementForUsage_67, @function
ucurr_getRoundingIncrementForUsage_67:
.LFB3275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	testq	%rdi, %rdi
	je	.L405
	cmpw	$0, (%rdi)
	jne	.L406
.L405:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L419
.L407:
	pxor	%xmm0, %xmm0
.L404:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	movl	%esi, %r12d
	movq	%rdx, %rsi
	call	_ZL13_findMetaDataPKDsR10UErrorCode.part.0
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L407
	testl	%r12d, %r12d
	je	.L408
	cmpl	$1, %r12d
	je	.L409
	movl	$16, (%rbx)
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	movl	$1, (%rbx)
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L408:
	movslq	(%rax), %rdx
	movl	4(%rax), %eax
	cmpl	$9, %edx
	jbe	.L413
.L420:
	movl	$3, (%rbx)
	pxor	%xmm0, %xmm0
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	movslq	8(%rax), %rdx
	movl	12(%rax), %eax
	cmpl	$9, %edx
	ja	.L420
.L413:
	cmpl	$1, %eax
	jle	.L407
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm0
	leaq	_ZL5POW10(%rip), %rax
	cvtsi2sdl	(%rax,%rdx,4), %xmm1
	divsd	%xmm1, %xmm0
	jmp	.L404
	.cfi_endproc
.LFE3275:
	.size	ucurr_getRoundingIncrementForUsage_67, .-ucurr_getRoundingIncrementForUsage_67
	.section	.rodata.str1.1
.LC8:
	.string	"from"
.LC9:
	.string	"to"
	.text
	.p2align 4
	.globl	ucurr_isAvailable_67
	.type	ucurr_isAvailable_67, @function
ucurr_isAvailable_67:
.LFB3284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movl	(%rsi), %esi
	movq	%rdi, -120(%rbp)
	movsd	%xmm0, -96(%rbp)
	movsd	%xmm1, -112(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testl	%esi, %esi
	jle	.L458
.L422:
	xorl	%eax, %eax
.L421:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L459
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movl	_ZL17gIsoCodesInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L460
.L423:
	movl	4+_ZL17gIsoCodesInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L438
	movq	-88(%rbp), %rbx
	movl	%eax, (%rbx)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L460:
	leaq	_ZL17gIsoCodesInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L423
	leaq	currency_cleanup(%rip), %rsi
	movl	$11, %edi
	call	ucln_common_registerCleanup_67@PLT
	movq	-88(%rbp), %rbx
	movq	uhash_compareUChars_67@GOTPCREL(%rip), %rsi
	xorl	%edx, %edx
	movq	uhash_hashUChars_67@GOTPCREL(%rip), %rdi
	movq	%rbx, %rcx
	call	uhash_open_67@PLT
	movq	%rax, -152(%rbp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L461
.L424:
	leaq	_ZL17gIsoCodesInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL17gIsoCodesInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L438:
	movq	-88(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L422
	movq	-120(%rbp), %rsi
	movq	_ZL9gIsoCodes(%rip), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L422
	movsd	-96(%rbp), %xmm3
	comisd	-112(%rbp), %xmm3
	ja	.L462
	comisd	16(%rax), %xmm3
	ja	.L422
	movsd	8(%rax), %xmm0
	comisd	-112(%rbp), %xmm0
	movl	$1, %eax
	jbe	.L421
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L462:
	movq	-88(%rbp), %rax
	movl	$1, (%rax)
	xorl	%eax, %eax
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L461:
	movq	-152(%rbp), %rdi
	leaq	_ZL18deleteIsoCodeEntryPv(%rip), %rsi
	leaq	-68(%rbp), %rbx
	call	uhash_setValueDeleter_67@PLT
	movq	%rbx, %rdx
	leaq	_ZL13CURRENCY_DATA(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	movl	$0, -68(%rbp)
	call	ures_openDirect_67@PLT
	movq	%rbx, %rcx
	leaq	_ZL12CURRENCY_MAP(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rdx
	call	ures_getByKey_67@PLT
	movq	%rax, -184(%rbp)
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	jle	.L463
	movq	-88(%rbp), %rbx
	movl	%eax, (%rbx)
.L426:
	movq	-184(%rbp), %rdi
	call	ures_close_67@PLT
	movq	-88(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L430
	movq	-152(%rbp), %rbx
	movq	%rbx, _ZL9gIsoCodes(%rip)
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L463:
	movl	$0, -164(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -160(%rbp)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L427:
	movq	-88(%rbp), %rcx
	movl	%eax, (%rcx)
.L428:
	movq	%r15, %rdi
	call	ures_close_67@PLT
	addl	$1, -164(%rbp)
.L436:
	movq	-184(%rbp), %r15
	movq	%r15, %rdi
	call	ures_getSize_67@PLT
	movl	-164(%rbp), %esi
	cmpl	%esi, %eax
	jle	.L426
	movq	%r15, %rdi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	call	ures_getByIndex_67@PLT
	movq	%rax, %r15
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	jg	.L427
	xorl	%edx, %edx
	leaq	-60(%rbp), %rax
	movq	%r15, -144(%rbp)
	movq	%rax, -176(%rbp)
	movl	%edx, %r15d
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L433:
	call	ures_close_67@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, %rcx
	leaq	.LC9(%rip), %rsi
	movl	$0, -68(%rbp)
	call	ures_getByKey_67@PLT
	movl	-68(%rbp), %edx
	movq	%rax, %rdi
	movq	.LC7(%rip), %rax
	movq	%rax, %xmm0
	testl	%edx, %edx
	jle	.L464
.L434:
	movsd	%xmm0, -136(%rbp)
	call	ures_close_67@PLT
	movq	%r14, %rdi
	call	ures_close_67@PLT
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movsd	-104(%rbp), %xmm2
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movsd	-136(%rbp), %xmm0
	movq	-128(%rbp), %rsi
	movl	$0, -68(%rbp)
	movq	-152(%rbp), %rdi
	unpcklpd	%xmm0, %xmm2
	movq	%rsi, (%r12)
	movups	%xmm2, 8(%r12)
	call	uhash_put_67@PLT
.L432:
	addl	$1, %r15d
.L435:
	movq	-144(%rbp), %r14
	movq	%r14, %rdi
	call	ures_getSize_67@PLT
	cmpl	%r15d, %eax
	jle	.L456
	movq	%rbx, %rcx
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	ures_getByIndex_67@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L465
	movq	%rbx, %rcx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movl	$0, -64(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L432
	movq	-160(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	ures_getString_67@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdi
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, -128(%rbp)
	call	ures_getByKey_67@PLT
	movl	-68(%rbp), %ecx
	movq	%rax, %rdi
	movq	.LC6(%rip), %rax
	movq	%rax, -104(%rbp)
	testl	%ecx, %ecx
	jg	.L433
	movq	-176(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rdi, -136(%rbp)
	movl	$0, -60(%rbp)
	call	ures_getIntVector_67@PLT
	pxor	%xmm6, %xmm6
	movq	-136(%rbp), %rdi
	movslq	(%rax), %rdx
	movl	4(%rax), %eax
	salq	$32, %rdx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm6
	movsd	%xmm6, -104(%rbp)
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L464:
	movq	-176(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rdi, -136(%rbp)
	movl	$0, -60(%rbp)
	call	ures_getIntVector_67@PLT
	pxor	%xmm0, %xmm0
	movq	-136(%rbp), %rdi
	movslq	(%rax), %rdx
	movl	4(%rax), %eax
	salq	$32, %rdx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L456:
	movq	-144(%rbp), %r15
	jmp	.L428
.L465:
	movq	-88(%rbp), %rax
	movl	$7, (%rax)
	.p2align 4,,10
	.p2align 3
.L430:
	movq	-152(%rbp), %rdi
	call	uhash_close_67@PLT
	movq	-88(%rbp), %rax
	movl	(%rax), %eax
	jmp	.L424
.L459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3284:
	.size	ucurr_isAvailable_67, .-ucurr_isAvailable_67
	.p2align 4
	.globl	ucurr_openISOCurrencies_67
	.type	ucurr_openISOCurrencies_67, @function
ucurr_openISOCurrencies_67:
.LFB3286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	%edi, %ebx
	movl	$56, %edi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L471
	movdqa	_ZL17gEnumCurrencyList(%rip), %xmm0
	movdqa	16+_ZL17gEnumCurrencyList(%rip), %xmm1
	movl	$8, %edi
	movdqa	32+_ZL17gEnumCurrencyList(%rip), %xmm2
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	48+_ZL17gEnumCurrencyList(%rip), %rax
	movq	%rax, 48(%r12)
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L472
	movl	%ebx, (%rax)
	movl	$0, 4(%rax)
	movq	%rax, 8(%r12)
.L466:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L471:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L466
.L472:
	movl	$7, 0(%r13)
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L466
	.cfi_endproc
.LFE3286:
	.size	ucurr_openISOCurrencies_67, .-ucurr_openISOCurrencies_67
	.p2align 4
	.globl	ucurr_countCurrencies_67
	.type	ucurr_countCurrencies_67, @function
ucurr_countCurrencies_67:
.LFB3287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movsd	%xmm0, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L473
	movl	(%rsi), %ecx
	movq	%rsi, %rbx
	testl	%ecx, %ecx
	jle	.L502
.L473:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L503
	addq	$248, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_restore_state
	leaq	-224(%rbp), %r13
	leaq	-236(%rbp), %r12
	movl	$157, %ecx
	movq	%rdi, -256(%rbp)
	movq	%r12, %r8
	movq	%r13, %rdx
	leaq	.LC1(%rip), %rsi
	movl	$0, -236(%rbp)
	call	uloc_getKeywordValue_67@PLT
	movq	%r13, %rdx
	movq	%rbx, %r8
	movl	$157, %ecx
	movq	-256(%rbp), %rdi
	xorl	%esi, %esi
	call	ulocimp_getRegionForSupplementalData_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L473
	movl	$95, %esi
	movq	%r13, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L476
	movb	$0, (%rax)
.L476:
	movq	%r12, %rdx
	leaq	_ZL13CURRENCY_DATA(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	call	ures_openDirect_67@PLT
	movq	%r12, %rcx
	leaq	_ZL12CURRENCY_MAP(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, %r14
	call	ures_getByKey_67@PLT
	movq	%r14, %rdi
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%rax, %rdx
	xorl	%r14d, %r14d
	call	ures_getByKey_67@PLT
	movq	%rax, %r15
	movl	-236(%rbp), %eax
	testl	%eax, %eax
	jle	.L504
.L477:
	movq	%r15, %rdi
	call	ures_close_67@PLT
	movl	(%rbx), %edx
	movl	-236(%rbp), %eax
	testl	%edx, %edx
	je	.L486
	testl	%eax, %eax
	je	.L493
.L486:
	movl	%eax, (%rbx)
.L487:
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%eax, %r14d
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L493:
	movl	%edx, %eax
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L504:
	leaq	-232(%rbp), %rax
	movl	$0, -256(%rbp)
	movq	%rax, -280(%rbp)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L478:
	movsd	-248(%rbp), %xmm2
	comisd	-272(%rbp), %xmm2
	sbbl	$-1, %r14d
.L482:
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movq	-264(%rbp), %rdi
	call	ures_close_67@PLT
	addl	$1, -256(%rbp)
.L484:
	movq	%r15, %rdi
	call	ures_getSize_67@PLT
	movl	-256(%rbp), %esi
	cmpl	%esi, %eax
	jle	.L477
	xorl	%edx, %edx
	movq	%r12, %rcx
	movq	%r15, %rdi
	call	ures_getByIndex_67@PLT
	xorl	%edx, %edx
	movq	%r12, %rcx
	leaq	.LC8(%rip), %rsi
	movl	$0, -232(%rbp)
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getByKey_67@PLT
	movq	-280(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	ures_getIntVector_67@PLT
	pxor	%xmm1, %xmm1
	movq	%r13, %rdi
	movslq	(%rax), %rdx
	movl	4(%rax), %eax
	salq	$32, %rdx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm1
	movsd	%xmm1, -272(%rbp)
	call	ures_getSize_67@PLT
	cmpl	$2, %eax
	jle	.L478
	xorl	%edx, %edx
	movq	%r12, %rcx
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	movl	$0, -228(%rbp)
	call	ures_getByKey_67@PLT
	movq	%r12, %rdx
	leaq	-228(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	call	ures_getIntVector_67@PLT
	movsd	-248(%rbp), %xmm4
	comisd	-272(%rbp), %xmm4
	movl	(%rax), %edx
	movq	-288(%rbp), %rdi
	movl	4(%rax), %eax
	jb	.L479
	salq	$32, %rdx
	pxor	%xmm0, %xmm0
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	xorl	%eax, %eax
	comisd	%xmm4, %xmm0
	seta	%al
	addl	%eax, %r14d
.L479:
	call	ures_close_67@PLT
	jmp	.L482
.L503:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3287:
	.size	ucurr_countCurrencies_67, .-ucurr_countCurrencies_67
	.p2align 4
	.globl	ucurr_forLocaleAndDate_67
	.type	ucurr_forLocaleAndDate_67, @function
ucurr_forLocaleAndDate_67:
.LFB3288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -252(%rbp)
	movq	%rdx, -264(%rbp)
	movsd	%xmm0, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -240(%rbp)
	testq	%r8, %r8
	je	.L505
	movl	(%r8), %esi
	movq	%r8, %r13
	testl	%esi, %esi
	jle	.L552
.L505:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L553
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	testq	%rdx, %rdx
	movl	%ecx, %r12d
	setne	%dl
	testl	%ecx, %ecx
	setne	%cl
	andl	%ecx, %edx
	testl	%r12d, %r12d
	sete	%cl
	orb	%cl, %dl
	movb	%dl, -253(%rbp)
	je	.L507
	leaq	-224(%rbp), %r14
	leaq	-236(%rbp), %rbx
	movl	$157, %ecx
	movq	%rdi, -272(%rbp)
	movq	%rbx, %r8
	movq	%r14, %rdx
	leaq	.LC1(%rip), %rsi
	movl	$0, -236(%rbp)
	call	uloc_getKeywordValue_67@PLT
	movl	$157, %ecx
	movq	%r13, %r8
	movq	%r14, %rdx
	movq	-272(%rbp), %rdi
	xorl	%esi, %esi
	movl	%eax, -240(%rbp)
	call	ulocimp_getRegionForSupplementalData_67@PLT
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L551
	movl	$95, %esi
	movq	%r14, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L510
	movb	$0, (%rax)
.L510:
	movq	%rbx, %rdx
	leaq	_ZL13CURRENCY_DATA(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	call	ures_openDirect_67@PLT
	movq	%rbx, %rcx
	leaq	_ZL12CURRENCY_MAP(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, %r15
	call	ures_getByKey_67@PLT
	movq	%r15, %rdi
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	ures_getByKey_67@PLT
	movl	-236(%rbp), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	jg	.L533
	movl	-252(%rbp), %eax
	testl	%eax, %eax
	jle	.L513
	movq	%r15, %rdi
	call	ures_getSize_67@PLT
	cmpl	-252(%rbp), %eax
	jl	.L513
	xorl	%esi, %esi
	leaq	-240(%rbp), %rax
	movq	%r13, -312(%rbp)
	movq	$0, -280(%rbp)
	movl	%esi, %r13d
	movl	$0, -292(%rbp)
	movq	%rax, -288(%rbp)
	movl	%r12d, -296(%rbp)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L514:
	movsd	-248(%rbp), %xmm2
	comisd	-272(%rbp), %xmm2
	jnb	.L554
.L549:
	movq	%r14, %rdi
	call	ures_close_67@PLT
	movq	%r12, %rdi
	call	ures_close_67@PLT
.L516:
	addl	$1, %r13d
.L524:
	movq	%r15, %rdi
	call	ures_getSize_67@PLT
	cmpl	%r13d, %eax
	jle	.L534
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	ures_getByIndex_67@PLT
	movq	-288(%rbp), %rdx
	movq	%rbx, %rcx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r14
	movq	%rax, %rdi
	call	ures_getStringByKey_67@PLT
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%r14, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, -280(%rbp)
	movl	$0, -232(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rdx
	leaq	-232(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getIntVector_67@PLT
	pxor	%xmm1, %xmm1
	movq	%r14, %rdi
	movslq	(%rax), %rdx
	movl	4(%rax), %eax
	salq	$32, %rdx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm1
	movsd	%xmm1, -272(%rbp)
	call	ures_getSize_67@PLT
	cmpl	$2, %eax
	jle	.L514
	xorl	%edx, %edx
	movq	%rbx, %rcx
	leaq	.LC9(%rip), %rsi
	movq	%r14, %rdi
	movl	$0, -228(%rbp)
	call	ures_getByKey_67@PLT
	leaq	-228(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
	call	ures_getIntVector_67@PLT
	movsd	-248(%rbp), %xmm4
	comisd	-272(%rbp), %xmm4
	movq	-304(%rbp), %rdi
	jb	.L548
	movslq	(%rax), %rdx
	movl	4(%rax), %eax
	pxor	%xmm0, %xmm0
	salq	$32, %rdx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	comisd	-248(%rbp), %xmm0
	jbe	.L548
	addl	$1, -292(%rbp)
	movl	-292(%rbp), %eax
	cmpl	%eax, -252(%rbp)
	je	.L519
	.p2align 4,,10
	.p2align 3
.L548:
	call	ures_close_67@PLT
	movq	%r14, %rdi
	call	ures_close_67@PLT
	movq	%r12, %rdi
	call	ures_close_67@PLT
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L507:
	movl	$1, (%r8)
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L513:
	movq	%r15, %rdi
	call	ures_close_67@PLT
.L551:
	xorl	%eax, %eax
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L554:
	addl	$1, -292(%rbp)
	movl	-292(%rbp), %eax
	cmpl	%eax, -252(%rbp)
	jne	.L549
	movq	%r12, %rbx
	movq	%r14, %rdi
	movl	-296(%rbp), %r12d
	movq	-312(%rbp), %r13
	call	ures_close_67@PLT
	movq	%rbx, %rdi
	call	ures_close_67@PLT
.L511:
	movq	%r15, %rdi
	call	ures_close_67@PLT
	movl	0(%r13), %edx
	movl	-236(%rbp), %eax
	testl	%edx, %edx
	je	.L526
	testl	%eax, %eax
	jne	.L526
	movl	%edx, %eax
.L527:
	movl	-240(%rbp), %edx
	testl	%eax, %eax
	jg	.L528
	cmpl	%r12d, %edx
	jge	.L551
	cmpb	$0, -253(%rbp)
	je	.L551
	movq	-280(%rbp), %rsi
	movq	-264(%rbp), %rdi
	call	u_strcpy_67@PLT
	movl	-240(%rbp), %edx
.L528:
	movq	-264(%rbp), %rdi
	movq	%r13, %rcx
	movl	%r12d, %esi
	call	u_terminateUChars_67@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L526:
	movl	%eax, 0(%r13)
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L533:
	movb	$0, -253(%rbp)
	movq	$0, -280(%rbp)
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L534:
	movb	$0, -253(%rbp)
	movl	-296(%rbp), %r12d
	movq	-312(%rbp), %r13
	jmp	.L511
.L519:
	movq	%r12, %rbx
	movq	-312(%rbp), %r13
	movl	-296(%rbp), %r12d
	call	ures_close_67@PLT
	movq	%r14, %rdi
	call	ures_close_67@PLT
	movq	%rbx, %rdi
	call	ures_close_67@PLT
	jmp	.L511
.L553:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3288:
	.size	ucurr_forLocaleAndDate_67, .-ucurr_forLocaleAndDate_67
	.section	.rodata.str1.1
.LC10:
	.string	"supplementalData"
.LC11:
	.string	"CurrencyMap"
.LC12:
	.string	"und"
	.text
	.p2align 4
	.globl	ucurr_getKeywordValuesForLocale_67
	.type	ucurr_getKeywordValuesForLocale_67, @function
ucurr_getKeywordValuesForLocale_67:
.LFB3289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movl	$4, %ecx
	pushq	%rbx
	subq	$968, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -1000(%rbp)
	movq	%rsi, %rdi
	movl	$1, %esi
	movb	%dl, -928(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-60(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -960(%rbp)
	call	ulocimp_getRegionForSupplementalData_67@PLT
	movq	%r12, %rdi
	call	ulist_createEmptyList_67@PLT
	movq	%r12, %rdi
	movq	%rax, -936(%rbp)
	movq	%rax, %rbx
	call	ulist_createEmptyList_67@PLT
	movl	$56, %edi
	movq	%rax, -920(%rbp)
	call	uprv_malloc_67@PLT
	movl	(%r12), %r8d
	movq	%rax, -984(%rbp)
	movq	%rax, %rcx
	testl	%r8d, %r8d
	jg	.L556
	testq	%rax, %rax
	je	.L557
	movdqa	_ZL20defaultKeywordValues(%rip), %xmm0
	movdqa	16+_ZL20defaultKeywordValues(%rip), %xmm1
	movq	%r12, %rdx
	leaq	.LC10(%rip), %rsi
	movdqa	32+_ZL20defaultKeywordValues(%rip), %xmm2
	leaq	.LC0(%rip), %rdi
	leaq	-688(%rbp), %r14
	movups	%xmm0, (%rax)
	leaq	-480(%rbp), %r13
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	48+_ZL20defaultKeywordValues(%rip), %rax
	movq	%rbx, 8(%rcx)
	leaq	-896(%rbp), %rbx
	movq	%rax, 48(%rcx)
	call	ures_openDirect_67@PLT
	movq	%r12, %rcx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, -968(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rbx, %rdi
	call	ures_initStackObject_67@PLT
	movq	%r14, %rdi
	call	ures_initStackObject_67@PLT
	movq	%r13, %rdi
	call	ures_initStackObject_67@PLT
	leaq	-272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -944(%rbp)
	call	ures_initStackObject_67@PLT
	leaq	-900(%rbp), %rcx
	movl	(%r12), %eax
	movq	%rcx, -992(%rbp)
.L575:
	cmpb	$0, -928(%rbp)
	je	.L621
	movq	%r13, -952(%rbp)
	movq	-968(%rbp), %r15
	testl	%eax, %eax
	jg	.L615
.L627:
	movq	%r15, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L568
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	ures_getNextResource_67@PLT
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L615
	movq	%rbx, %rdi
	call	ures_getKey_67@PLT
	movq	-960(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L622
	movq	%r13, %r8
	movq	-952(%rbp), %r13
.L566:
	movb	$1, -952(%rbp)
.L567:
	movq	-968(%rbp), %rdi
	movq	%r8, %rsi
	movq	%r12, %rcx
	movq	%r14, %rdx
	call	ures_getByKey_67@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L588
	movq	%rbx, -976(%rbp)
	movq	-992(%rbp), %rbx
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L582:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L618
.L574:
	movq	%r14, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L623
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	ures_getNextResource_67@PLT
	movq	%r13, %rdi
	call	ures_getType_67@PLT
	cmpl	$2, %eax
	jne	.L582
	movl	$96, %edi
	call	uprv_malloc_67@PLT
	movl	$96, -900(%rbp)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L624
	movq	%r12, %r9
	movl	$1, %r8d
	movq	%rbx, %rcx
	movq	%rax, %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	ures_getUTF8StringByKey_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L618
	movq	-944(%rbp), %rdx
	movq	%r12, %rcx
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	ures_getByKey_67@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L581
	cmpb	$0, -952(%rbp)
	movl	$0, (%r12)
	je	.L581
	movq	%r15, %rdi
	call	strlen@PLT
	movq	-936(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	ulist_containsString_67@PLT
	testb	%al, %al
	je	.L625
.L581:
	movq	%r15, %rdi
	call	strlen@PLT
	movq	-920(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	ulist_containsString_67@PLT
	orb	-928(%rbp), %al
	je	.L626
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L574
	.p2align 4,,10
	.p2align 3
.L618:
	movq	-976(%rbp), %rbx
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L622:
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L627
.L615:
	movq	-952(%rbp), %r13
.L588:
	movq	-936(%rbp), %rdi
	call	ulist_deleteList_67@PLT
	movq	-984(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	$0, -984(%rbp)
.L586:
	movq	-944(%rbp), %rdi
	call	ures_close_67@PLT
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movq	%r14, %rdi
	call	ures_close_67@PLT
	movq	%rbx, %rdi
	call	ures_close_67@PLT
	movq	-968(%rbp), %rdi
	call	ures_close_67@PLT
	movq	-920(%rbp), %rdi
	call	ulist_deleteList_67@PLT
.L555:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L628
	movq	-984(%rbp), %rax
	addq	$968, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	movq	-920(%rbp), %rdi
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%r15, %rsi
	call	ulist_addItemEndList_67@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L625:
	movq	-936(%rbp), %rdi
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%r15, %rsi
	call	ulist_addItemEndList_67@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L623:
	movq	-976(%rbp), %rbx
	movl	(%r12), %eax
	jmp	.L575
.L568:
	movl	(%r12), %eax
	movq	-952(%rbp), %r13
	testl	%eax, %eax
	jg	.L588
	movq	-936(%rbp), %rdi
	call	ulist_getListSize_67@PLT
	testl	%eax, %eax
	je	.L629
.L583:
	movq	-984(%rbp), %rax
	movq	8(%rax), %rdi
	call	ulist_resetList_67@PLT
	jmp	.L586
.L556:
	cmpq	$0, -984(%rbp)
	je	.L557
	movq	%rax, %rdi
	call	uprv_free_67@PLT
.L560:
	movq	-936(%rbp), %rdi
	call	ulist_deleteList_67@PLT
	movq	-920(%rbp), %rdi
	call	ulist_deleteList_67@PLT
	movq	$0, -984(%rbp)
	jmp	.L555
.L629:
	movq	-984(%rbp), %rdi
	call	uenum_close_67@PLT
	movq	-1000(%rbp), %rdi
	movq	%r12, %rcx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	ucurr_getKeywordValuesForLocale_67
	movq	%rax, -984(%rbp)
	jmp	.L583
.L624:
	movl	$7, (%r12)
	movq	-976(%rbp), %rbx
	movl	$7, %eax
	jmp	.L575
.L557:
	movl	$7, (%r12)
	jmp	.L560
.L628:
	call	__stack_chk_fail@PLT
.L621:
	testl	%eax, %eax
	jg	.L588
	movq	-968(%rbp), %r15
	movq	%r15, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L564
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	ures_getNextResource_67@PLT
	cmpl	$0, (%r12)
	jg	.L588
	movq	%rbx, %rdi
	call	ures_getKey_67@PLT
	movq	-960(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -952(%rbp)
	call	strcmp@PLT
	movq	-952(%rbp), %r8
	testl	%eax, %eax
	je	.L566
	movb	$0, -952(%rbp)
	jmp	.L567
.L564:
	cmpl	$0, (%r12)
	jg	.L588
	movq	-920(%rbp), %rdi
	call	ulist_resetList_67@PLT
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%rax, %rdi
	call	strlen@PLT
	movq	-936(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	ulist_containsString_67@PLT
	testb	%al, %al
	je	.L630
.L584:
	movq	-920(%rbp), %rdi
	call	ulist_getNext_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L631
	jmp	.L583
.L630:
	movl	$96, %edi
	call	uprv_malloc_67@PLT
	movq	%r15, %rdi
	movq	%rax, -928(%rbp)
	call	strlen@PLT
	movq	-928(%rbp), %r8
	movl	$96, %ecx
	movq	%r15, %rsi
	leaq	1(%rax), %rdx
	movq	%r8, %rdi
	call	__memcpy_chk@PLT
	movq	-936(%rbp), %rdi
	movl	$1, %edx
	movq	%r12, %rcx
	movq	%rax, %rsi
	call	ulist_addItemEndList_67@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L584
	jmp	.L583
	.cfi_endproc
.LFE3289:
	.size	ucurr_getKeywordValuesForLocale_67, .-ucurr_getKeywordValuesForLocale_67
	.section	.rodata.str1.1
.LC13:
	.string	"currencyNumericCodes"
.LC14:
	.string	"codeMap"
	.text
	.p2align 4
	.globl	ucurr_getNumericCode_67
	.type	ucurr_getNumericCode_67, @function
ucurr_getNumericCode_67:
.LFB3290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L635
	movq	%rdi, %r12
	call	u_strlen_67@PLT
	cmpl	$3, %eax
	je	.L642
.L635:
	xorl	%eax, %eax
.L632:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L643
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	leaq	-48(%rbp), %r14
	leaq	.LC13(%rip), %rsi
	xorl	%edi, %edi
	movl	$0, -48(%rbp)
	movq	%r14, %rdx
	call	ures_openDirect_67@PLT
	movq	%r14, %rcx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getByKey_67@PLT
	movl	-48(%rbp), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L644
.L636:
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	ures_close_67@PLT
	movl	-52(%rbp), %eax
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L644:
	leaq	-44(%rbp), %r15
	movl	$3, %edx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	u_UCharsToChars_67@PLT
	movq	%r15, %rdi
	movb	$0, -41(%rbp)
	call	T_CString_toUpperCase_67@PLT
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	ures_getByKey_67@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	ures_getInt_67@PLT
	movl	-48(%rbp), %edx
	testl	%edx, %edx
	movl	$0, %edx
	cmovg	%edx, %eax
	jmp	.L636
.L643:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3290:
	.size	ucurr_getNumericCode_67, .-ucurr_getNumericCode_67
	.p2align 4
	.type	_ZL20initCurrSymbolsEquivv, @function
_ZL20initCurrSymbolsEquivv:
.LFB3283:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$11, %edi
	leaq	currency_cleanup(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -324(%rbp)
	call	ucln_common_registerCleanup_67@PLT
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L645
	movl	-324(%rbp), %r8d
	movq	$0, (%rax)
	movq	%rax, %rbx
	testl	%r8d, %r8d
	jle	.L834
.L653:
	movq	%rbx, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L645:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L835
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L834:
	.cfi_restore_state
	leaq	8(%rax), %r12
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	leaq	-324(%rbp), %rax
	xorl	%ecx, %ecx
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r8
	movq	%rax, -376(%rbp)
	call	uhash_init_67@PLT
	movl	-324(%rbp), %edi
	testl	%edi, %edi
	jle	.L836
.L648:
	movq	(%rbx), %rdi
.L649:
	testq	%rdi, %rdi
	je	.L653
	call	uhash_close_67@PLT
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%r12, (%rbx)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	uhash_setKeyDeleter_67@PLT
	movl	-324(%rbp), %esi
	movq	(%rbx), %rdi
	testl	%esi, %esi
	jg	.L649
	leaq	_ZL13deleteUnicodePv(%rip), %rsi
	leaq	-192(%rbp), %r14
	call	uhash_setValueDeleter_67@PLT
	movl	-324(%rbp), %ecx
	movq	%r14, %r15
	leaq	_ZN6icu_677unisetsL16kCurrencyEntriesE(%rip), %rax
	movq	%rax, -344(%rbp)
	leaq	-256(%rbp), %r13
	movq	%rbx, %r14
	testl	%ecx, %ecx
	jg	.L648
.L717:
	movq	-344(%rbp), %rbx
	movq	%r15, %rdi
	movl	4(%rbx), %esi
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movl	(%rbx), %edi
	call	_ZN6icu_677unisets3getENS0_3KeyE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L824
	movq	%r13, %rdi
	leaq	-128(%rbp), %r12
	movq	%r14, %rbx
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	leaq	-320(%rbp), %rax
	movq	%rax, -352(%rbp)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L837:
	movzbl	-184(%rbp), %eax
	andl	$1, %eax
.L657:
	testb	%al, %al
	je	.L662
.L715:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L663:
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L655
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIterator9getStringEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movswl	-120(%rbp), %edx
	testb	$1, %dl
	jne	.L837
	testw	%dx, %dx
	js	.L658
	sarl	$5, %edx
.L659:
	movzwl	-184(%rbp), %ecx
	testw	%cx, %cx
	js	.L660
	movswl	%cx, %eax
	sarl	$5, %eax
.L661:
	andl	$1, %ecx
	jne	.L662
	cmpl	%edx, %eax
	je	.L838
.L662:
	movl	-324(%rbp), %edx
	testl	%edx, %edx
	jle	.L839
.L825:
	movq	%r15, %r14
.L711:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
.L654:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L716:
	movl	-324(%rbp), %eax
	testl	%eax, %eax
	jg	.L648
	movq	%rbx, _ZL17gCurrSymbolsEquiv(%rip)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L839:
	movswl	-120(%rbp), %eax
	movswl	-184(%rbp), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L665
	testw	%dx, %dx
	js	.L666
	sarl	$5, %edx
.L667:
	testw	%ax, %ax
	js	.L668
	sarl	$5, %eax
.L669:
	cmpl	%edx, %eax
	jne	.L670
	testb	%cl, %cl
	je	.L840
.L670:
	movq	-352(%rbp), %rdi
	movq	%rbx, -320(%rbp)
	movq	%r15, -304(%rbp)
	movq	%r15, -312(%rbp)
	movq	%rbx, -288(%rbp)
	movq	%r12, -272(%rbp)
	movq	%r12, -280(%rbp)
	call	_ZN6icu_6713EquivIterator4nextEv
	leaq	-288(%rbp), %rdi
	movq	%rax, -368(%rbp)
	movq	%rax, %r14
	call	_ZN6icu_6713EquivIterator4nextEv
	movq	%rax, -360(%rbp)
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L671
	testq	%r14, %r14
	jne	.L701
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L845:
	testw	%dx, %dx
	js	.L673
	sarl	$5, %edx
.L674:
	testw	%ax, %ax
	js	.L675
	sarl	$5, %eax
.L676:
	cmpl	%edx, %eax
	jne	.L677
	testb	%cl, %cl
	je	.L841
.L677:
	movswl	-184(%rbp), %eax
	movswl	8(%r8), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L678
	testw	%dx, %dx
	js	.L679
	sarl	$5, %edx
.L680:
	testw	%ax, %ax
	js	.L681
	sarl	$5, %eax
.L682:
	cmpl	%edx, %eax
	jne	.L683
	testb	%cl, %cl
	je	.L842
.L683:
	movq	-320(%rbp), %rax
	movq	-304(%rbp), %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L692
	movq	-312(%rbp), %rsi
	movswl	8(%rax), %edx
	movswl	8(%rsi), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L686
	testw	%dx, %dx
	js	.L687
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L689
.L847:
	sarl	$5, %eax
.L690:
	cmpl	%edx, %eax
	jne	.L691
	testb	%cl, %cl
	je	.L843
.L691:
	movq	%r14, -304(%rbp)
.L685:
	movq	-288(%rbp), %rax
	movq	-272(%rbp), %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L700
	movq	-280(%rbp), %rsi
	movswl	8(%rax), %edx
	movswl	8(%rsi), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	jne	.L694
	testw	%dx, %dx
	js	.L695
	sarl	$5, %edx
.L696:
	testw	%ax, %ax
	js	.L697
	sarl	$5, %eax
.L698:
	testb	%cl, %cl
	jne	.L699
	cmpl	%edx, %eax
	je	.L844
.L699:
	movq	%r8, -272(%rbp)
	testq	%r14, %r14
	je	.L700
.L701:
	movswl	-120(%rbp), %eax
	movswl	8(%r14), %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	je	.L845
.L672:
	testb	%cl, %cl
	je	.L677
.L831:
	movl	-324(%rbp), %eax
	testl	%eax, %eax
	jle	.L715
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L844:
	movq	%r8, %rdi
	movq	%r8, -384(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-384(%rbp), %r8
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L694:
	testb	%cl, %cl
	je	.L699
.L700:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L723
	movq	-360(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L723:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -360(%rbp)
	testq	%rax, %rax
	je	.L704
	movq	-368(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L705:
	testq	%r14, %r14
	jne	.L846
	movq	-360(%rbp), %rdi
	movq	%r15, %r14
	movq	(%rdi), %rax
	call	*8(%rax)
.L724:
	movl	$7, -324(%rbp)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L842:
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L678:
	testb	%cl, %cl
	je	.L683
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L843:
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L686:
	testb	%cl, %cl
	je	.L691
.L692:
	xorl	%r14d, %r14d
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L675:
	movl	-116(%rbp), %eax
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L673:
	movl	12(%r14), %edx
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L681:
	movl	-180(%rbp), %eax
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L679:
	movl	12(%r8), %edx
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L660:
	movl	-180(%rbp), %eax
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L658:
	movl	-116(%rbp), %edx
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L687:
	movl	12(%r14), %edx
	testw	%ax, %ax
	jns	.L847
.L689:
	movl	12(%rsi), %eax
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L697:
	movl	12(%rsi), %eax
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L695:
	movl	12(%r8), %edx
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L840:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%cl
	.p2align 4,,10
	.p2align 3
.L665:
	testb	%cl, %cl
	jne	.L831
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L841:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r8, -384(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-384(%rbp), %r8
	testb	%al, %al
	setne	%cl
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L655:
	movq	%r13, %rdi
	movq	%rbx, %r14
	leaq	40+_ZN6icu_677unisetsL16kCurrencyEntriesE(%rip), %rbx
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, -344(%rbp)
	movq	-344(%rbp), %rax
	cmpq	%rbx, %rax
	jne	.L717
	movq	%r14, %rbx
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L668:
	movl	-116(%rbp), %eax
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L666:
	movl	-180(%rbp), %edx
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L838:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L671:
	movq	-368(%rbp), %rax
	orq	-360(%rbp), %rax
	movl	$64, %edi
	jne	.L702
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L708
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L708:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -360(%rbp)
	testq	%rax, %rax
	je	.L704
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L846:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L712
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-368(%rbp), %r8
.L712:
	movq	-376(%rbp), %rcx
	movq	(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	uhash_put_67@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L713
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L713:
	movq	-376(%rbp), %rcx
	movq	-360(%rbp), %rdx
	movq	%r14, %rsi
	movq	(%rbx), %rdi
	call	uhash_put_67@PLT
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L702:
	cmpq	$0, -360(%rbp)
	je	.L848
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L708
	movq	-360(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L848:
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L723
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L824:
	movq	%r14, %rbx
	movq	%r15, %r14
	jmp	.L654
.L835:
	call	__stack_chk_fail@PLT
.L704:
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%rax, %r14
	testq	%r15, %r15
	je	.L724
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	jmp	.L724
	.cfi_endproc
.LFE3283:
	.size	_ZL20initCurrSymbolsEquivv, .-_ZL20initCurrSymbolsEquivv
	.section	.rodata.str1.1
.LC15:
	.string	"en_GB"
	.text
	.p2align 4
	.type	_ZL20collectCurrencyNamesPKcPP18CurrencyNameStructPiS3_S4_R10UErrorCode, @function
_ZL20collectCurrencyNamesPKcPP18CurrencyNameStructPiS3_S4_R10UErrorCode:
.LFB3254:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$648, %rsp
	movq	%rdi, -600(%rbp)
	movq	%rsi, -496(%rbp)
	movq	%rcx, -664(%rbp)
	movq	%r9, -672(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZL25gCurrSymbolsEquivInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L851
	leaq	_ZL25gCurrSymbolsEquivInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	jne	.L976
.L851:
	movq	_ZL17gCurrSymbolsEquiv(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	-600(%rbp), %rdi
	leaq	-476(%rbp), %rcx
	movl	$157, %edx
	movl	$0, -476(%rbp)
	movq	%rax, -560(%rbp)
	leaq	-384(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -228(%rbp)
	movq	$0, -240(%rbp)
	movl	$0, -232(%rbp)
	movq	%rcx, -576(%rbp)
	movq	%rax, -648(%rbp)
	movaps	%xmm0, -384(%rbp)
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm0, -352(%rbp)
	movaps	%xmm0, -336(%rbp)
	movaps	%xmm0, -320(%rbp)
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm0, -256(%rbp)
	call	uloc_getName_67@PLT
	movl	-476(%rbp), %eax
	cmpl	$-124, %eax
	je	.L924
	testl	%eax, %eax
	jle	.L853
.L924:
	movq	-672(%rbp), %rax
	movl	$1, (%rax)
.L853:
	movl	$0, (%rbx)
	leaq	-224(%rbp), %rax
	movq	-648(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movl	$0, (%r14)
	movl	$157, %edx
	movq	%rax, %rdi
	movq	$0, -80(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movq	%rax, -584(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	__strcpy_chk@PLT
	movl	_ZL25gCurrSymbolsEquivInitOnce(%rip), %eax
	cmpl	$2, %eax
	je	.L856
	leaq	_ZL25gCurrSymbolsEquivInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	jne	.L977
.L856:
	movq	_ZL17gCurrSymbolsEquiv(%rip), %rax
	leaq	-464(%rbp), %r15
	movq	%r15, -528(%rbp)
	movq	%rax, -488(%rbp)
	leaq	-460(%rbp), %rax
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, -568(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, -512(%rbp)
	.p2align 4,,10
	.p2align 3
.L878:
	movq	-584(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%r13d, %r13d
	leaq	.LC0(%rip), %rdi
	movl	$0, -464(%rbp)
	call	ures_open_67@PLT
	movq	%r15, %rcx
	xorl	%edx, %edx
	leaq	_ZL10CURRENCIES(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	ures_getSize_67@PLT
	movl	%eax, -544(%rbp)
	testl	%eax, %eax
	jle	.L869
	movq	%rbx, -536(%rbp)
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L861:
	movq	-536(%rbp), %rax
	addl	$1, %edx
	movq	%r12, %rdi
	addl	$1, %r13d
	movl	%edx, (%r14)
	addl	$1, (%rax)
	call	ures_close_67@PLT
	cmpl	%r13d, -544(%rbp)
	je	.L978
.L858:
	movq	-520(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r15, %rcx
	movl	%r13d, %esi
	call	ures_getByIndex_67@PLT
	movq	-504(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r15, %rcx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getStringByIndex_67@PLT
	movl	(%r14), %ebx
	cmpq	$0, -488(%rbp)
	leal	1(%rbx), %edx
	movl	%edx, (%r14)
	je	.L861
	movq	-512(%rbp), %rbx
	movl	-460(%rbp), %ecx
	movl	$1, %esi
	movq	%rax, -456(%rbp)
	movq	-568(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%rbx, %r8
	xorl	%ebx, %ebx
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L980:
	testw	%dx, %dx
	js	.L864
	sarl	$5, %edx
.L865:
	testw	%ax, %ax
	js	.L866
	sarl	$5, %eax
.L867:
	cmpl	%edx, %eax
	jne	.L868
	testb	%cl, %cl
	je	.L979
.L868:
	addl	$1, %ebx
.L918:
	movq	-488(%rbp), %rax
	movq	%r8, %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L862
	movswl	8(%rax), %edx
	movswl	-440(%rbp), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	testb	$1, %dl
	je	.L980
.L863:
	testb	%cl, %cl
	je	.L868
.L862:
	addl	%ebx, (%r14)
	movq	-512(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r14), %edx
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L866:
	movl	-436(%rbp), %eax
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L864:
	movl	12(%r8), %edx
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L978:
	movq	-536(%rbp), %rbx
.L869:
	movq	-504(%rbp), %r13
	movq	-552(%rbp), %rdi
	xorl	%edx, %edx
	leaq	_ZL15CURRENCYPLURALS(%rip), %rsi
	movl	$0, -460(%rbp)
	movq	%r13, %rcx
	call	ures_getByKey_67@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getSize_67@PLT
	movl	%eax, -536(%rbp)
	testl	%eax, %eax
	jle	.L859
	xorl	%esi, %esi
	movq	%r14, -544(%rbp)
	movq	%r13, %r14
	movq	%r15, -592(%rbp)
	movl	%esi, %r15d
	.p2align 4,,10
	.p2align 3
.L860:
	movl	%r15d, %esi
	movq	%r14, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ures_getByIndex_67@PLT
	addl	$1, %r15d
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getSize_67@PLT
	addl	%eax, (%rbx)
	movq	%r13, %rdi
	call	ures_close_67@PLT
	cmpl	%r15d, -536(%rbp)
	jne	.L860
	movq	-544(%rbp), %r14
	movq	-592(%rbp), %r15
.L859:
	movq	%r12, %rdi
	call	ures_close_67@PLT
	movq	-520(%rbp), %rdi
	call	ures_close_67@PLT
	movq	-552(%rbp), %rdi
	call	ures_close_67@PLT
	cmpb	$0, -224(%rbp)
	je	.L981
	movq	-584(%rbp), %rsi
	movl	$6, %ecx
	leaq	.LC15(%rip), %rdi
	movl	$0, -456(%rbp)
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L872
	movl	$3223600, -221(%rbp)
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L979:
	movq	-512(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -592(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-592(%rbp), %r8
	testb	%al, %al
	setne	%cl
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L981:
	movslq	(%rbx), %rax
	leaq	(%rax,%rax,2), %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	-496(%rbp), %rcx
	movq	%rax, (%rcx)
	movslq	(%r14), %rax
	leaq	(%rax,%rax,2), %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	-664(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-672(%rbp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jle	.L982
.L849:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L983
	addq	$648, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L872:
	.cfi_restore_state
	movq	-584(%rbp), %rdx
.L874:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L874
	movl	%eax, %ecx
	movq	-584(%rbp), %rsi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	movq	%rsi, %rdi
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	movq	-568(%rbp), %rcx
	sbbq	$3, %rdx
	subq	%rsi, %rdx
	call	uloc_getParent_67@PLT
	jmp	.L878
.L977:
	call	_ZL20initCurrSymbolsEquivv
	leaq	_ZL25gCurrSymbolsEquivInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L856
.L976:
	call	_ZL20initCurrSymbolsEquivv
	leaq	_ZL25gCurrSymbolsEquivInitOnce(%rip), %rdi
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L982:
	movq	uhash_compareChars_67@GOTPCREL(%rip), %r13
	movq	uhash_hashChars_67@GOTPCREL(%rip), %r12
	movl	$0, (%rbx)
	xorl	%edx, %edx
	movl	$0, (%r14)
	leaq	-472(%rbp), %rax
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -632(%rbp)
	movl	$0, -472(%rbp)
	movl	$0, -468(%rbp)
	call	uhash_open_67@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	leaq	-468(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, -640(%rbp)
	call	uhash_open_67@PLT
	movq	%r14, -680(%rbp)
	movq	-600(%rbp), %r15
	movq	%rax, -608(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, -488(%rbp)
	leaq	-448(%rbp), %rax
	movl	$0, -568(%rbp)
	movq	%rax, -624(%rbp)
	.p2align 4,,10
	.p2align 3
.L916:
	movq	-576(%rbp), %r14
	movq	-648(%rbp), %rsi
	leaq	.LC0(%rip), %rdi
	movl	$0, -476(%rbp)
	movq	%r14, %rdx
	call	ures_open_67@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	leaq	_ZL10CURRENCIES(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -656(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rax, %rdi
	movq	%rax, -592(%rbp)
	call	ures_getSize_67@PLT
	movl	$0, -512(%rbp)
	movl	%eax, -600(%rbp)
	testl	%eax, %eax
	jle	.L900
	movq	%rbx, -544(%rbp)
	movq	-664(%rbp), %r14
	movq	%r15, -536(%rbp)
	movq	-680(%rbp), %rbx
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L882:
	movq	-584(%rbp), %rdi
	movq	%rax, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L884
.L974:
	movq	%r13, %rdi
	call	ures_close_67@PLT
	addl	$1, -512(%rbp)
	movl	-512(%rbp), %eax
	cmpl	%eax, -600(%rbp)
	je	.L984
.L879:
	movq	-576(%rbp), %r15
	movl	-512(%rbp), %esi
	xorl	%edx, %edx
	movq	-592(%rbp), %rdi
	movq	%r15, %rcx
	call	ures_getByIndex_67@PLT
	movq	-504(%rbp), %rdx
	movq	%r15, %rcx
	xorl	%esi, %esi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	ures_getStringByIndex_67@PLT
	movq	%r13, %rdi
	movq	%rax, -520(%rbp)
	call	ures_getKey_67@PLT
	movl	-568(%rbp), %edi
	movq	%rax, %r12
	testl	%edi, %edi
	jne	.L882
.L884:
	movq	-632(%rbp), %rcx
	movq	-584(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r12, %rsi
	call	uhash_put_67@PLT
	movslq	(%rbx), %rax
	movq	(%r14), %rdx
	movq	%r12, %xmm0
	movhps	-520(%rbp), %xmm0
	cmpq	$0, -560(%rbp)
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rax
	movl	$0, 20(%rax)
	movups	%xmm0, (%rax)
	movslq	(%rbx), %rax
	leal	1(%rax), %ecx
	leaq	(%rax,%rax,2), %rax
	movl	%ecx, (%rbx)
	movl	-460(%rbp), %ecx
	movl	%ecx, 16(%rdx,%rax,8)
	jne	.L985
.L886:
	movq	-576(%rbp), %rcx
	movq	-504(%rbp), %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	ures_getStringByIndex_67@PLT
	movq	-496(%rbp), %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, %r10
	movq	-544(%rbp), %rax
	movslq	-460(%rbp), %r15
	movq	-488(%rbp), %r9
	movq	-536(%rbp), %r8
	movq	%r10, %rdx
	movq	%r10, -552(%rbp)
	movslq	(%rax), %rax
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	addq	(%rcx), %rax
	movl	%r15d, %ecx
	movq	%r12, (%rax)
	movl	$0, -456(%rbp)
	call	u_strToUpper_67@PLT
	movl	$0, -456(%rbp)
	cmpl	%eax, %r15d
	movslq	%eax, %rdi
	movl	%eax, -520(%rbp)
	cmovge	%r15, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	-552(%rbp), %r10
	movl	%r15d, %ecx
	movl	-520(%rbp), %esi
	movq	-488(%rbp), %r9
	movq	-536(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	movq	%r10, %rdx
	call	u_strToUpper_67@PLT
	movl	-456(%rbp), %esi
	movq	-520(%rbp), %rdi
	testl	%esi, %esi
	jle	.L899
	movq	-552(%rbp), %r10
	movl	%r15d, %edx
	movq	%r10, %rsi
	call	u_memcpy_67@PLT
	movq	-520(%rbp), %rdi
.L899:
	movq	-544(%rbp), %rsi
	movq	-496(%rbp), %rax
	movq	(%rax), %rdx
	movslq	(%rsi), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rax
	movl	$1, 20(%rax)
	movq	%rdi, 8(%rax)
	movslq	(%rsi), %rax
	movl	$6, %edi
	leal	1(%rax), %ecx
	leaq	(%rax,%rax,2), %rax
	movl	%ecx, (%rsi)
	movl	-460(%rbp), %ecx
	movl	%ecx, 16(%rdx,%rax,8)
	movslq	(%rbx), %rax
	leaq	(%rax,%rax,2), %rdx
	movq	(%r14), %rax
	leaq	(%rax,%rdx,8), %rdx
	movq	%r12, (%rdx)
	movq	%rdx, -520(%rbp)
	call	uprv_malloc_67@PLT
	movq	-520(%rbp), %rdx
	movq	(%r14), %rcx
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movslq	(%rbx), %rax
	movl	$3, %edx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	8(%rax), %rsi
	call	u_charsToUChars_67@PLT
	movslq	(%rbx), %rax
	movq	(%r14), %rdx
	leaq	(%rax,%rax,2), %rax
	movl	$1, 20(%rdx,%rax,8)
	movslq	(%rbx), %rax
	leal	1(%rax), %ecx
	leaq	(%rax,%rax,2), %rax
	movl	%ecx, (%rbx)
	movl	$3, 16(%rdx,%rax,8)
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L985:
	movq	-624(%rbp), %r15
	movq	-520(%rbp), %rax
	movl	$1, %esi
	movq	-488(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L988:
	testw	%dx, %dx
	js	.L889
	sarl	$5, %edx
.L890:
	testw	%ax, %ax
	js	.L891
	sarl	$5, %eax
.L892:
	cmpl	%edx, %eax
	jne	.L893
	testb	%sil, %sil
	je	.L986
.L893:
	movslq	(%rbx), %rax
	movq	(%r14), %rdx
	movzwl	8(%r15), %esi
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rax
	movq	%r12, (%rax)
	testb	$17, %sil
	jne	.L987
	andl	$2, %esi
	leaq	10(%r15), %rsi
	jne	.L895
	movq	24(%r15), %rsi
.L895:
	movq	%rsi, 8(%rax)
	movl	$0, 20(%rax)
	movslq	(%rbx), %rax
	leal	1(%rax), %esi
	leaq	(%rax,%rax,2), %rax
	movl	%esi, (%rbx)
	leaq	(%rdx,%rax,8), %rdx
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L896
	sarl	$5, %eax
.L897:
	movl	%eax, 16(%rdx)
.L898:
	movq	-560(%rbp), %rax
	movq	%r15, %rsi
	movq	(%rax), %rdi
	call	uhash_get_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L887
	movswl	8(%rax), %edx
	movswl	-440(%rbp), %eax
	movl	%eax, %esi
	andl	$1, %esi
	testb	$1, %dl
	je	.L988
.L888:
	testb	%sil, %sil
	je	.L893
.L887:
	movq	-624(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L896:
	movl	12(%r15), %eax
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L891:
	movl	-436(%rbp), %eax
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L889:
	movl	12(%r15), %edx
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L987:
	xorl	%esi, %esi
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L984:
	movq	-536(%rbp), %r15
	movq	-544(%rbp), %rbx
.L900:
	movq	-528(%rbp), %rcx
	movq	-656(%rbp), %rdi
	xorl	%edx, %edx
	leaq	_ZL15CURRENCYPLURALS(%rip), %rsi
	movl	$0, -464(%rbp)
	call	ures_getByKey_67@PLT
	movq	%rax, %rdi
	movq	%rax, -600(%rbp)
	call	ures_getSize_67@PLT
	movl	%eax, -612(%rbp)
	testl	%eax, %eax
	jle	.L880
	movl	$0, -544(%rbp)
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L903:
	movq	-608(%rbp), %rdi
	movq	%rax, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L905
.L909:
	movq	%r13, %rdi
	call	ures_close_67@PLT
	addl	$1, -544(%rbp)
	movl	-544(%rbp), %eax
	cmpl	%eax, -612(%rbp)
	je	.L880
.L881:
	movq	-528(%rbp), %rcx
	movl	-544(%rbp), %esi
	xorl	%edx, %edx
	movq	-600(%rbp), %rdi
	call	ures_getByIndex_67@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getKey_67@PLT
	movl	-568(%rbp), %edx
	movq	%rax, -520(%rbp)
	testl	%edx, %edx
	jne	.L903
	movq	-640(%rbp), %rcx
	movq	-608(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %rdx
	call	uhash_put_67@PLT
.L904:
	movq	%r13, %rdi
	call	ures_getSize_67@PLT
	xorl	%r11d, %r11d
	movl	%eax, -552(%rbp)
	testl	%eax, %eax
	jle	.L909
	movq	%r13, -536(%rbp)
	movl	%r11d, %r13d
	.p2align 4,,10
	.p2align 3
.L907:
	movq	-528(%rbp), %rcx
	movq	-504(%rbp), %rdx
	movl	%r13d, %esi
	movq	-536(%rbp), %rdi
	call	ures_getStringByIndex_67@PLT
	movq	-496(%rbp), %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, %r12
	movslq	(%rbx), %rax
	movslq	-460(%rbp), %r14
	movq	%r15, %r8
	movq	-488(%rbp), %r9
	movq	%r12, %rdx
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	addq	(%rcx), %rax
	movq	-520(%rbp), %rcx
	movq	%rcx, (%rax)
	movl	%r14d, %ecx
	movl	$0, -456(%rbp)
	call	u_strToUpper_67@PLT
	movl	$0, -456(%rbp)
	cmpl	%eax, %r14d
	movslq	%eax, %rdi
	movl	%eax, -512(%rbp)
	cmovge	%r14, %rdi
	addq	%rdi, %rdi
	call	uprv_malloc_67@PLT
	movq	%r15, %r8
	movl	%r14d, %ecx
	movq	%r12, %rdx
	movl	-512(%rbp), %esi
	movq	-488(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, -512(%rbp)
	call	u_strToUpper_67@PLT
	movl	-456(%rbp), %eax
	movq	-512(%rbp), %rdi
	testl	%eax, %eax
	jle	.L908
	movl	%r14d, %edx
	movq	%r12, %rsi
	call	u_memcpy_67@PLT
	movq	-512(%rbp), %rdi
.L908:
	movq	-496(%rbp), %rax
	addl	$1, %r13d
	movq	(%rax), %rdx
	movslq	(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdx,%rax,8), %rax
	movl	$1, 20(%rax)
	movq	%rdi, 8(%rax)
	movslq	(%rbx), %rax
	leal	1(%rax), %ecx
	leaq	(%rax,%rax,2), %rax
	movl	%ecx, (%rbx)
	movl	-460(%rbp), %ecx
	movl	%ecx, 16(%rdx,%rax,8)
	cmpl	%r13d, -552(%rbp)
	jne	.L907
	movq	-536(%rbp), %r13
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L905:
	movq	-520(%rbp), %rsi
	movq	-640(%rbp), %rcx
	movq	-608(%rbp), %rdi
	movq	%rsi, %rdx
	call	uhash_put_67@PLT
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L880:
	movq	-600(%rbp), %rdi
	call	ures_close_67@PLT
	movq	-592(%rbp), %rdi
	call	ures_close_67@PLT
	movq	-656(%rbp), %rdi
	call	ures_close_67@PLT
	cmpb	$0, -384(%rbp)
	je	.L989
	movq	-648(%rbp), %rsi
	movl	$6, %ecx
	leaq	.LC15(%rip), %rdi
	movl	$0, -456(%rbp)
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L910
	movl	$3223600, -381(%rbp)
.L911:
	addl	$1, -568(%rbp)
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L986:
	movq	-624(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%sil
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L989:
	movq	-584(%rbp), %rdi
	movq	-680(%rbp), %r14
	call	uhash_close_67@PLT
	movq	-608(%rbp), %rdi
	call	uhash_close_67@PLT
	movq	-496(%rbp), %rax
	movslq	(%rbx), %rsi
	leaq	_ZL22currencyNameComparatorPKvS0_(%rip), %rcx
	movl	$24, %edx
	movq	(%rax), %rdi
	call	qsort@PLT
	movq	-664(%rbp), %rax
	movslq	(%r14), %rsi
	leaq	_ZL22currencyNameComparatorPKvS0_(%rip), %rcx
	movl	$24, %edx
	movq	(%rax), %rdi
	call	qsort@PLT
	movl	-472(%rbp), %eax
	testl	%eax, %eax
	jg	.L975
	movl	-468(%rbp), %eax
	testl	%eax, %eax
	jle	.L849
.L975:
	movq	-672(%rbp), %rbx
	movl	%eax, (%rbx)
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L910:
	movq	-648(%rbp), %rdx
.L912:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L912
	movl	%eax, %ecx
	movq	-648(%rbp), %rsi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	movq	%rsi, %rdi
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	movq	-488(%rbp), %rcx
	sbbq	$3, %rdx
	subq	%rsi, %rdx
	call	uloc_getParent_67@PLT
	jmp	.L911
.L983:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3254:
	.size	_ZL20collectCurrencyNamesPKcPP18CurrencyNameStructPiS3_S4_R10UErrorCode, .-_ZL20collectCurrencyNamesPKcPP18CurrencyNameStructPiS3_S4_R10UErrorCode
	.p2align 4
	.type	_ZL13getCacheEntryPKcR10UErrorCode, @function
_ZL13getCacheEntryPKcR10UErrorCode:
.LFB3267:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	_ZL19gCurrencyCacheMutex(%rip), %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movl	$0, -60(%rbp)
	movq	$0, -48(%rbp)
	call	umtx_lock_67@PLT
	movq	_ZL9currCache(%rip), %r12
	testq	%r12, %r12
	je	.L991
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L992
.L991:
	movq	8+_ZL9currCache(%rip), %r12
	testq	%r12, %r12
	je	.L993
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L992
.L993:
	movq	16+_ZL9currCache(%rip), %r12
	testq	%r12, %r12
	je	.L994
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L992
.L994:
	movq	24+_ZL9currCache(%rip), %r12
	testq	%r12, %r12
	je	.L995
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L992
.L995:
	movq	32+_ZL9currCache(%rip), %r12
	testq	%r12, %r12
	je	.L996
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L992
.L996:
	movq	40+_ZL9currCache(%rip), %r12
	testq	%r12, %r12
	je	.L997
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L992
.L997:
	movq	48+_ZL9currCache(%rip), %r12
	testq	%r12, %r12
	je	.L998
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L992
.L998:
	movq	56+_ZL9currCache(%rip), %r12
	testq	%r12, %r12
	je	.L999
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L992
.L999:
	movq	64+_ZL9currCache(%rip), %r12
	testq	%r12, %r12
	je	.L1000
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L992
.L1000:
	movq	72+_ZL9currCache(%rip), %r12
	testq	%r12, %r12
	je	.L1001
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L992
.L1001:
	leaq	_ZL19gCurrencyCacheMutex(%rip), %rdi
	xorl	%r12d, %r12d
	call	umtx_unlock_67@PLT
	leaq	-48(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	movq	%rbx, %r9
	leaq	-56(%rbp), %rsi
	leaq	-60(%rbp), %r8
	movq	%r13, %rdi
	call	_ZL20collectCurrencyNamesPKcPP18CurrencyNameStructPiS3_S4_R10UErrorCode
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L990
	leaq	_ZL19gCurrencyCacheMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZL9currCache(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1003
	movq	%r13, %rdi
	call	strcmp@PLT
	movslq	%eax, %r12
	testl	%r12d, %r12d
	je	.L1004
.L1003:
	movq	8+_ZL9currCache(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1005
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1027
.L1005:
	movq	16+_ZL9currCache(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1006
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1028
.L1006:
	movq	24+_ZL9currCache(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1007
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1029
.L1007:
	movq	32+_ZL9currCache(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1008
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1030
.L1008:
	movq	40+_ZL9currCache(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1009
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1031
.L1009:
	movq	48+_ZL9currCache(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1010
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1032
.L1010:
	movq	56+_ZL9currCache(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1011
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1033
.L1011:
	movq	64+_ZL9currCache(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1012
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1034
.L1012:
	movq	72+_ZL9currCache(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1013
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1035
.L1013:
	movsbq	_ZL22currentCacheEntryIndex(%rip), %rax
	leaq	_ZL9currCache(%rip), %r14
	movq	(%r14,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L1016
	subl	$1, 188(%rdi)
	je	.L1143
.L1016:
	movl	$192, %edi
	call	uprv_malloc_67@PLT
	movq	%r13, %rsi
	movl	$157, %edx
	movq	%rax, %r12
	movsbq	_ZL22currentCacheEntryIndex(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%r12, (%r14,%rax,8)
	call	__strcpy_chk@PLT
	movq	-56(%rbp), %rax
	movl	$11, %edi
	movl	$2, 188(%r12)
	leaq	currency_cleanup(%rip), %rsi
	movq	%rax, 160(%r12)
	movl	-64(%rbp), %eax
	movl	%eax, 168(%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 176(%r12)
	movl	-60(%rbp), %eax
	movl	%eax, 184(%r12)
	leal	1(%rbx), %eax
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$1717986919, %rdx, %rdx
	sarl	$31, %ecx
	sarq	$34, %rdx
	subl	%ecx, %edx
	leal	(%rdx,%rdx,4), %edx
	addl	%edx, %edx
	subl	%edx, %eax
	movb	%al, _ZL22currentCacheEntryIndex(%rip)
	call	ucln_common_registerCleanup_67@PLT
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L992:
	addl	$1, 188(%r12)
.L1142:
	leaq	_ZL19gCurrencyCacheMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
.L990:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1144
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1032:
	.cfi_restore_state
	movl	$6, %r12d
.L1004:
	movl	-64(%rbp), %eax
	movq	-56(%rbp), %r14
	testl	%eax, %eax
	jle	.L1023
	subl	$1, %eax
	leaq	8(%r14), %rbx
	leaq	(%rax,%rax,2), %rax
	leaq	32(%r14,%rax,8), %r13
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1018:
	addq	$24, %rbx
	cmpq	%rbx, %r13
	je	.L1023
.L1019:
	testb	$1, 12(%rbx)
	je	.L1018
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L1018
.L1023:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	movl	-60(%rbp), %eax
	movq	-48(%rbp), %r14
	testl	%eax, %eax
	jle	.L1020
	subl	$1, %eax
	leaq	8(%r14), %rbx
	leaq	(%rax,%rax,2), %rax
	leaq	32(%r14,%rax,8), %r13
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1021:
	addq	$24, %rbx
	cmpq	%rbx, %r13
	je	.L1020
.L1022:
	testb	$1, 12(%rbx)
	je	.L1021
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L1021
.L1020:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	leaq	_ZL9currCache(%rip), %rax
	movq	(%rax,%r12,8), %r12
	addl	$1, 188(%r12)
	jmp	.L1142
.L1143:
	call	_ZL16deleteCacheEntryP22CurrencyNameCacheEntry
	jmp	.L1016
.L1028:
	movl	$2, %r12d
	jmp	.L1004
.L1027:
	movl	$1, %r12d
	jmp	.L1004
.L1029:
	movl	$3, %r12d
	jmp	.L1004
.L1033:
	movl	$7, %r12d
	jmp	.L1004
.L1031:
	movl	$5, %r12d
	jmp	.L1004
.L1030:
	movl	$4, %r12d
	jmp	.L1004
.L1035:
	movl	$9, %r12d
	jmp	.L1004
.L1034:
	movl	$8, %r12d
	jmp	.L1004
.L1144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3267:
	.size	_ZL13getCacheEntryPKcR10UErrorCode, .-_ZL13getCacheEntryPKcR10UErrorCode
	.p2align 4
	.globl	uprv_parseCurrency_67
	.type	uprv_parseCurrency_67, @function
uprv_parseCurrency_67:
.LFB3269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rdx
	movl	%ecx, -500(%rbp)
	movq	%r9, -512(%rbp)
	movl	(%rdx), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L1161
.L1145:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1162
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1161:
	.cfi_restore_state
	movq	%rsi, %r13
	movq	%rdx, %rsi
	movq	%rdx, -520(%rbp)
	movq	%rdi, %rbx
	movq	%r8, %r12
	call	_ZL13getCacheEntryPKcR10UErrorCode
	movq	-520(%rbp), %rdx
	movq	%rax, %r15
	movl	(%rdx), %r9d
	testl	%r9d, %r9d
	jg	.L1145
	movl	168(%rax), %r10d
	movq	160(%rax), %rax
	movq	%rax, -528(%rbp)
	movl	184(%r15), %eax
	movl	%eax, -556(%rbp)
	movq	176(%r15), %rax
	movq	%rax, -552(%rbp)
	movl	8(%r14), %eax
	movl	%eax, -520(%rbp)
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L1147
	sarl	$5, %eax
.L1148:
	movl	-520(%rbp), %esi
	movl	$100, %edx
	movq	%r13, %rdi
	leaq	-464(%rbp), %r11
	movq	%r11, %rcx
	movq	%r11, -544(%rbp)
	subl	%esi, %eax
	movl	%r10d, -504(%rbp)
	cmpl	$100, %eax
	cmovg	%edx, %eax
	xorl	%r8d, %r8d
	movl	%eax, %edx
	movl	%eax, -536(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-544(%rbp), %r11
	movl	-536(%rbp), %eax
	leaq	-256(%rbp), %r13
	movq	%rbx, %r8
	movq	%r11, %rdx
	leaq	-484(%rbp), %r9
	movl	$100, %esi
	movq	%r13, %rdi
	movq	%r11, -536(%rbp)
	movl	$0, -484(%rbp)
	movl	%eax, %ecx
	call	u_strToUpper_67@PLT
	subq	$8, %rsp
	movq	%r12, %r8
	movq	%r13, %rdx
	movl	%eax, %ebx
	leaq	-476(%rbp), %rax
	movl	$0, (%r12)
	movl	-504(%rbp), %r10d
	pushq	%rax
	movq	-528(%rbp), %rdi
	leaq	-480(%rbp), %r9
	movl	%ebx, %ecx
	movl	$0, -480(%rbp)
	movl	%r10d, %esi
	movl	$-1, -476(%rbp)
	call	_ZL18searchCurrencyNamePK18CurrencyNameStructiPKDsiPiS4_S4_
	cmpb	$1, -500(%rbp)
	popq	%rdi
	movl	$0, -472(%rbp)
	movl	$-1, -468(%rbp)
	popq	%r8
	je	.L1149
	subq	$8, %rsp
	leaq	-468(%rbp), %rax
	movl	%ebx, %ecx
	movq	%r12, %r8
	pushq	%rax
	movq	-536(%rbp), %r11
	leaq	-472(%rbp), %r9
	movl	-556(%rbp), %esi
	movq	-552(%rbp), %rdi
	movq	%r11, %rdx
	call	_ZL18searchCurrencyNamePK18CurrencyNameStructiPKDsiPiS4_S4_
	movl	-480(%rbp), %eax
	movl	-472(%rbp), %ecx
	popq	%rdx
	popq	%rsi
	cmpl	%ecx, %eax
	jge	.L1150
.L1155:
	movslq	-468(%rbp), %rax
	cmpl	$-1, %eax
	je	.L1154
	movq	-552(%rbp), %rbx
	leaq	(%rax,%rax,2), %rax
	movl	$4, %edx
	movq	-512(%rbp), %rsi
	movq	(%rbx,%rax,8), %rdi
	call	u_charsToUChars_67@PLT
	movl	-520(%rbp), %eax
	addl	-472(%rbp), %eax
	movl	%eax, 8(%r14)
	.p2align 4,,10
	.p2align 3
.L1154:
	leaq	_ZL19gCurrencyCacheMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	subl	$1, 188(%r15)
	je	.L1163
.L1156:
	leaq	_ZL19gCurrencyCacheMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1147:
	movl	12(%r13), %eax
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1149:
	movl	-480(%rbp), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	js	.L1154
.L1150:
	movslq	-476(%rbp), %rdx
	cmpl	$-1, %edx
	je	.L1153
	movq	-528(%rbp), %rbx
	leaq	(%rdx,%rdx,2), %rax
	movq	-512(%rbp), %rsi
	movl	$4, %edx
	movq	(%rbx,%rax,8), %rdi
	call	u_charsToUChars_67@PLT
	movl	-520(%rbp), %eax
	addl	-480(%rbp), %eax
	movl	%eax, 8(%r14)
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1153:
	cmpl	%eax, %ecx
	jne	.L1154
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	%r15, %rdi
	call	_ZL16deleteCacheEntryP22CurrencyNameCacheEntry
	jmp	.L1156
.L1162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3269:
	.size	uprv_parseCurrency_67, .-uprv_parseCurrency_67
	.p2align 4
	.globl	_Z18uprv_currencyLeadsPKcRN6icu_6710UnicodeSetER10UErrorCode
	.type	_Z18uprv_currencyLeadsPKcRN6icu_6710UnicodeSetER10UErrorCode, @function
_Z18uprv_currencyLeadsPKcRN6icu_6710UnicodeSetER10UErrorCode:
.LFB3270:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jle	.L1182
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZL13getCacheEntryPKcR10UErrorCode
	movl	(%rbx), %ecx
	movq	%rax, %r12
	testl	%ecx, %ecx
	jg	.L1164
	movl	184(%rax), %edx
	xorl	%ebx, %ebx
	testl	%edx, %edx
	jg	.L1168
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1171:
	movq	%r13, %rdi
	addq	$1, %rbx
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	cmpl	%ebx, 184(%r12)
	jle	.L1172
.L1168:
	movq	176(%r12), %rdx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	8(%rdx), %rcx
	movzwl	(%rcx), %esi
	movl	%esi, %edi
	movl	%esi, %eax
	andl	$63488, %edi
	cmpl	$55296, %edi
	jne	.L1171
	testb	$4, %ah
	jne	.L1171
	cmpl	$1, 16(%rdx)
	je	.L1171
	movzwl	2(%rcx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L1171
	sall	$10, %esi
	leal	-56613888(%rax,%rsi), %esi
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1164:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1172:
	.cfi_restore_state
	movl	168(%r12), %eax
	testl	%eax, %eax
	jle	.L1169
	xorl	%ebx, %ebx
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	%r13, %rdi
	addq	$1, %rbx
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	cmpl	%ebx, 168(%r12)
	jle	.L1169
.L1170:
	movq	160(%r12), %rdx
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rdx,%rax,8), %rdx
	movq	8(%rdx), %rcx
	movzwl	(%rcx), %esi
	movl	%esi, %edi
	movl	%esi, %eax
	andl	$63488, %edi
	cmpl	$55296, %edi
	jne	.L1173
	testb	$4, %ah
	jne	.L1173
	cmpl	$1, 16(%rdx)
	je	.L1173
	movzwl	2(%rcx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L1173
	sall	$10, %esi
	leal	-56613888(%rax,%rsi), %esi
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1169:
	leaq	_ZL19gCurrencyCacheMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	subl	$1, 188(%r12)
	je	.L1183
.L1174:
	addq	$8, %rsp
	leaq	_ZL19gCurrencyCacheMutex(%rip), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L1183:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZL16deleteCacheEntryP22CurrencyNameCacheEntry
	jmp	.L1174
	.cfi_endproc
.LFE3270:
	.size	_Z18uprv_currencyLeadsPKcRN6icu_6710UnicodeSetER10UErrorCode, .-_Z18uprv_currencyLeadsPKcRN6icu_6710UnicodeSetER10UErrorCode
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL20defaultKeywordValues, @object
	.size	_ZL20defaultKeywordValues, 56
_ZL20defaultKeywordValues:
	.quad	0
	.quad	0
	.quad	ulist_close_keyword_values_iterator_67
	.quad	ulist_count_keyword_values_67
	.quad	uenum_unextDefault_67
	.quad	ulist_next_keyword_value_67
	.quad	ulist_reset_keyword_values_iterator_67
	.align 32
	.type	_ZL17gEnumCurrencyList, @object
	.size	_ZL17gEnumCurrencyList, 56
_ZL17gEnumCurrencyList:
	.quad	0
	.quad	0
	.quad	ucurr_closeCurrencyList
	.quad	ucurr_countCurrencyList
	.quad	uenum_unextDefault_67
	.quad	ucurr_nextCurrencyList
	.quad	ucurr_resetCurrencyList
	.section	.rodata.str1.1
.LC16:
	.string	"ADP"
.LC17:
	.string	"AED"
.LC18:
	.string	"AFA"
.LC19:
	.string	"AFN"
.LC20:
	.string	"ALK"
.LC21:
	.string	"ALL"
.LC22:
	.string	"AMD"
.LC23:
	.string	"ANG"
.LC24:
	.string	"AOA"
.LC25:
	.string	"AOK"
.LC26:
	.string	"AON"
.LC27:
	.string	"AOR"
.LC28:
	.string	"ARA"
.LC29:
	.string	"ARL"
.LC30:
	.string	"ARM"
.LC31:
	.string	"ARP"
.LC32:
	.string	"ARS"
.LC33:
	.string	"ATS"
.LC34:
	.string	"AUD"
.LC35:
	.string	"AWG"
.LC36:
	.string	"AZM"
.LC37:
	.string	"AZN"
.LC38:
	.string	"BAD"
.LC39:
	.string	"BAM"
.LC40:
	.string	"BAN"
.LC41:
	.string	"BBD"
.LC42:
	.string	"BDT"
.LC43:
	.string	"BEC"
.LC44:
	.string	"BEF"
.LC45:
	.string	"BEL"
.LC46:
	.string	"BGL"
.LC47:
	.string	"BGM"
.LC48:
	.string	"BGN"
.LC49:
	.string	"BGO"
.LC50:
	.string	"BHD"
.LC51:
	.string	"BIF"
.LC52:
	.string	"BMD"
.LC53:
	.string	"BND"
.LC54:
	.string	"BOB"
.LC55:
	.string	"BOL"
.LC56:
	.string	"BOP"
.LC57:
	.string	"BOV"
.LC58:
	.string	"BRB"
.LC59:
	.string	"BRC"
.LC60:
	.string	"BRE"
.LC61:
	.string	"BRL"
.LC62:
	.string	"BRN"
.LC63:
	.string	"BRR"
.LC64:
	.string	"BRZ"
.LC65:
	.string	"BSD"
.LC66:
	.string	"BTN"
.LC67:
	.string	"BUK"
.LC68:
	.string	"BWP"
.LC69:
	.string	"BYB"
.LC70:
	.string	"BYN"
.LC71:
	.string	"BYR"
.LC72:
	.string	"BZD"
.LC73:
	.string	"CAD"
.LC74:
	.string	"CDF"
.LC75:
	.string	"CHE"
.LC76:
	.string	"CHF"
.LC77:
	.string	"CHW"
.LC78:
	.string	"CLE"
.LC79:
	.string	"CLF"
.LC80:
	.string	"CLP"
.LC81:
	.string	"CNH"
.LC82:
	.string	"CNX"
.LC83:
	.string	"CNY"
.LC84:
	.string	"COP"
.LC85:
	.string	"COU"
.LC86:
	.string	"CRC"
.LC87:
	.string	"CSD"
.LC88:
	.string	"CSK"
.LC89:
	.string	"CUC"
.LC90:
	.string	"CUP"
.LC91:
	.string	"CVE"
.LC92:
	.string	"CYP"
.LC93:
	.string	"CZK"
.LC94:
	.string	"DDM"
.LC95:
	.string	"DEM"
.LC96:
	.string	"DJF"
.LC97:
	.string	"DKK"
.LC98:
	.string	"DOP"
.LC99:
	.string	"DZD"
.LC100:
	.string	"ECS"
.LC101:
	.string	"ECV"
.LC102:
	.string	"EEK"
.LC103:
	.string	"EGP"
.LC104:
	.string	"EQE"
.LC105:
	.string	"ERN"
.LC106:
	.string	"ESA"
.LC107:
	.string	"ESB"
.LC108:
	.string	"ESP"
.LC109:
	.string	"ETB"
.LC110:
	.string	"EUR"
.LC111:
	.string	"FIM"
.LC112:
	.string	"FJD"
.LC113:
	.string	"FKP"
.LC114:
	.string	"FRF"
.LC115:
	.string	"GBP"
.LC116:
	.string	"GEK"
.LC117:
	.string	"GEL"
.LC118:
	.string	"GHC"
.LC119:
	.string	"GHS"
.LC120:
	.string	"GIP"
.LC121:
	.string	"GMD"
.LC122:
	.string	"GNF"
.LC123:
	.string	"GNS"
.LC124:
	.string	"GQE"
.LC125:
	.string	"GRD"
.LC126:
	.string	"GTQ"
.LC127:
	.string	"GWE"
.LC128:
	.string	"GWP"
.LC129:
	.string	"GYD"
.LC130:
	.string	"HKD"
.LC131:
	.string	"HNL"
.LC132:
	.string	"HRD"
.LC133:
	.string	"HRK"
.LC134:
	.string	"HTG"
.LC135:
	.string	"HUF"
.LC136:
	.string	"IDR"
.LC137:
	.string	"IEP"
.LC138:
	.string	"ILP"
.LC139:
	.string	"ILR"
.LC140:
	.string	"ILS"
.LC141:
	.string	"INR"
.LC142:
	.string	"IQD"
.LC143:
	.string	"IRR"
.LC144:
	.string	"ISJ"
.LC145:
	.string	"ISK"
.LC146:
	.string	"ITL"
.LC147:
	.string	"JMD"
.LC148:
	.string	"JOD"
.LC149:
	.string	"JPY"
.LC150:
	.string	"KES"
.LC151:
	.string	"KGS"
.LC152:
	.string	"KHR"
.LC153:
	.string	"KMF"
.LC154:
	.string	"KPW"
.LC155:
	.string	"KRH"
.LC156:
	.string	"KRO"
.LC157:
	.string	"KRW"
.LC158:
	.string	"KWD"
.LC159:
	.string	"KYD"
.LC160:
	.string	"KZT"
.LC161:
	.string	"LAK"
.LC162:
	.string	"LBP"
.LC163:
	.string	"LKR"
.LC164:
	.string	"LRD"
.LC165:
	.string	"LSL"
.LC166:
	.string	"LSM"
.LC167:
	.string	"LTL"
.LC168:
	.string	"LTT"
.LC169:
	.string	"LUC"
.LC170:
	.string	"LUF"
.LC171:
	.string	"LUL"
.LC172:
	.string	"LVL"
.LC173:
	.string	"LVR"
.LC174:
	.string	"LYD"
.LC175:
	.string	"MAD"
.LC176:
	.string	"MAF"
.LC177:
	.string	"MCF"
.LC178:
	.string	"MDC"
.LC179:
	.string	"MDL"
.LC180:
	.string	"MGA"
.LC181:
	.string	"MGF"
.LC182:
	.string	"MKD"
.LC183:
	.string	"MKN"
.LC184:
	.string	"MLF"
.LC185:
	.string	"MMK"
.LC186:
	.string	"MNT"
.LC187:
	.string	"MOP"
.LC188:
	.string	"MRO"
.LC189:
	.string	"MRU"
.LC190:
	.string	"MTL"
.LC191:
	.string	"MTP"
.LC192:
	.string	"MUR"
.LC193:
	.string	"MVP"
.LC194:
	.string	"MVR"
.LC195:
	.string	"MWK"
.LC196:
	.string	"MXN"
.LC197:
	.string	"MXP"
.LC198:
	.string	"MXV"
.LC199:
	.string	"MYR"
.LC200:
	.string	"MZE"
.LC201:
	.string	"MZM"
.LC202:
	.string	"MZN"
.LC203:
	.string	"NAD"
.LC204:
	.string	"NGN"
.LC205:
	.string	"NIC"
.LC206:
	.string	"NIO"
.LC207:
	.string	"NLG"
.LC208:
	.string	"NOK"
.LC209:
	.string	"NPR"
.LC210:
	.string	"NZD"
.LC211:
	.string	"OMR"
.LC212:
	.string	"PAB"
.LC213:
	.string	"PEI"
.LC214:
	.string	"PEN"
.LC215:
	.string	"PES"
.LC216:
	.string	"PGK"
.LC217:
	.string	"PHP"
.LC218:
	.string	"PKR"
.LC219:
	.string	"PLN"
.LC220:
	.string	"PLZ"
.LC221:
	.string	"PTE"
.LC222:
	.string	"PYG"
.LC223:
	.string	"QAR"
.LC224:
	.string	"RHD"
.LC225:
	.string	"ROL"
.LC226:
	.string	"RON"
.LC227:
	.string	"RSD"
.LC228:
	.string	"RUB"
.LC229:
	.string	"RUR"
.LC230:
	.string	"RWF"
.LC231:
	.string	"SAR"
.LC232:
	.string	"SBD"
.LC233:
	.string	"SCR"
.LC234:
	.string	"SDD"
.LC235:
	.string	"SDG"
.LC236:
	.string	"SDP"
.LC237:
	.string	"SEK"
.LC238:
	.string	"SGD"
.LC239:
	.string	"SHP"
.LC240:
	.string	"SIT"
.LC241:
	.string	"SKK"
.LC242:
	.string	"SLL"
.LC243:
	.string	"SOS"
.LC244:
	.string	"SRD"
.LC245:
	.string	"SRG"
.LC246:
	.string	"SSP"
.LC247:
	.string	"STD"
.LC248:
	.string	"STN"
.LC249:
	.string	"SUR"
.LC250:
	.string	"SVC"
.LC251:
	.string	"SYP"
.LC252:
	.string	"SZL"
.LC253:
	.string	"THB"
.LC254:
	.string	"TJR"
.LC255:
	.string	"TJS"
.LC256:
	.string	"TMM"
.LC257:
	.string	"TMT"
.LC258:
	.string	"TND"
.LC259:
	.string	"TOP"
.LC260:
	.string	"TPE"
.LC261:
	.string	"TRL"
.LC262:
	.string	"TRY"
.LC263:
	.string	"TTD"
.LC264:
	.string	"TWD"
.LC265:
	.string	"TZS"
.LC266:
	.string	"UAH"
.LC267:
	.string	"UAK"
.LC268:
	.string	"UGS"
.LC269:
	.string	"UGX"
.LC270:
	.string	"USD"
.LC271:
	.string	"USN"
.LC272:
	.string	"USS"
.LC273:
	.string	"UYI"
.LC274:
	.string	"UYP"
.LC275:
	.string	"UYU"
.LC276:
	.string	"UZS"
.LC277:
	.string	"VEB"
.LC278:
	.string	"VEF"
.LC279:
	.string	"VND"
.LC280:
	.string	"VNN"
.LC281:
	.string	"VUV"
.LC282:
	.string	"WST"
.LC283:
	.string	"XAF"
.LC284:
	.string	"XAG"
.LC285:
	.string	"XAU"
.LC286:
	.string	"XBA"
.LC287:
	.string	"XBB"
.LC288:
	.string	"XBC"
.LC289:
	.string	"XBD"
.LC290:
	.string	"XCD"
.LC291:
	.string	"XDR"
.LC292:
	.string	"XEU"
.LC293:
	.string	"XFO"
.LC294:
	.string	"XFU"
.LC295:
	.string	"XOF"
.LC296:
	.string	"XPD"
.LC297:
	.string	"XPF"
.LC298:
	.string	"XPT"
.LC299:
	.string	"XRE"
.LC300:
	.string	"XSU"
.LC301:
	.string	"XTS"
.LC302:
	.string	"XUA"
.LC303:
	.string	"XXX"
.LC304:
	.string	"YDD"
.LC305:
	.string	"YER"
.LC306:
	.string	"YUD"
.LC307:
	.string	"YUM"
.LC308:
	.string	"YUN"
.LC309:
	.string	"YUR"
.LC310:
	.string	"ZAL"
.LC311:
	.string	"ZAR"
.LC312:
	.string	"ZMK"
.LC313:
	.string	"ZMW"
.LC314:
	.string	"ZRN"
.LC315:
	.string	"ZRZ"
.LC316:
	.string	"ZWD"
.LC317:
	.string	"ZWL"
.LC318:
	.string	"ZWR"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL13gCurrencyList, @object
	.size	_ZL13gCurrencyList, 4864
_ZL13gCurrencyList:
	.quad	.LC16
	.long	5
	.zero	4
	.quad	.LC17
	.long	9
	.zero	4
	.quad	.LC18
	.long	5
	.zero	4
	.quad	.LC19
	.long	9
	.zero	4
	.quad	.LC20
	.long	5
	.zero	4
	.quad	.LC21
	.long	9
	.zero	4
	.quad	.LC22
	.long	9
	.zero	4
	.quad	.LC23
	.long	9
	.zero	4
	.quad	.LC24
	.long	9
	.zero	4
	.quad	.LC25
	.long	5
	.zero	4
	.quad	.LC26
	.long	5
	.zero	4
	.quad	.LC27
	.long	5
	.zero	4
	.quad	.LC28
	.long	5
	.zero	4
	.quad	.LC29
	.long	5
	.zero	4
	.quad	.LC30
	.long	5
	.zero	4
	.quad	.LC31
	.long	5
	.zero	4
	.quad	.LC32
	.long	9
	.zero	4
	.quad	.LC33
	.long	5
	.zero	4
	.quad	.LC34
	.long	9
	.zero	4
	.quad	.LC35
	.long	9
	.zero	4
	.quad	.LC36
	.long	5
	.zero	4
	.quad	.LC37
	.long	9
	.zero	4
	.quad	.LC38
	.long	5
	.zero	4
	.quad	.LC39
	.long	9
	.zero	4
	.quad	.LC40
	.long	5
	.zero	4
	.quad	.LC41
	.long	9
	.zero	4
	.quad	.LC42
	.long	9
	.zero	4
	.quad	.LC43
	.long	6
	.zero	4
	.quad	.LC44
	.long	5
	.zero	4
	.quad	.LC45
	.long	6
	.zero	4
	.quad	.LC46
	.long	5
	.zero	4
	.quad	.LC47
	.long	5
	.zero	4
	.quad	.LC48
	.long	9
	.zero	4
	.quad	.LC49
	.long	5
	.zero	4
	.quad	.LC50
	.long	9
	.zero	4
	.quad	.LC51
	.long	9
	.zero	4
	.quad	.LC52
	.long	9
	.zero	4
	.quad	.LC53
	.long	9
	.zero	4
	.quad	.LC54
	.long	9
	.zero	4
	.quad	.LC55
	.long	5
	.zero	4
	.quad	.LC56
	.long	5
	.zero	4
	.quad	.LC57
	.long	10
	.zero	4
	.quad	.LC58
	.long	5
	.zero	4
	.quad	.LC59
	.long	5
	.zero	4
	.quad	.LC60
	.long	5
	.zero	4
	.quad	.LC61
	.long	9
	.zero	4
	.quad	.LC62
	.long	5
	.zero	4
	.quad	.LC63
	.long	5
	.zero	4
	.quad	.LC64
	.long	5
	.zero	4
	.quad	.LC65
	.long	9
	.zero	4
	.quad	.LC66
	.long	9
	.zero	4
	.quad	.LC67
	.long	5
	.zero	4
	.quad	.LC68
	.long	9
	.zero	4
	.quad	.LC69
	.long	5
	.zero	4
	.quad	.LC70
	.long	9
	.zero	4
	.quad	.LC71
	.long	5
	.zero	4
	.quad	.LC72
	.long	9
	.zero	4
	.quad	.LC73
	.long	9
	.zero	4
	.quad	.LC74
	.long	9
	.zero	4
	.quad	.LC75
	.long	10
	.zero	4
	.quad	.LC76
	.long	9
	.zero	4
	.quad	.LC77
	.long	10
	.zero	4
	.quad	.LC78
	.long	5
	.zero	4
	.quad	.LC79
	.long	10
	.zero	4
	.quad	.LC80
	.long	9
	.zero	4
	.quad	.LC81
	.long	10
	.zero	4
	.quad	.LC82
	.long	6
	.zero	4
	.quad	.LC83
	.long	9
	.zero	4
	.quad	.LC84
	.long	9
	.zero	4
	.quad	.LC85
	.long	10
	.zero	4
	.quad	.LC86
	.long	9
	.zero	4
	.quad	.LC87
	.long	5
	.zero	4
	.quad	.LC88
	.long	5
	.zero	4
	.quad	.LC89
	.long	9
	.zero	4
	.quad	.LC90
	.long	9
	.zero	4
	.quad	.LC91
	.long	9
	.zero	4
	.quad	.LC92
	.long	5
	.zero	4
	.quad	.LC93
	.long	9
	.zero	4
	.quad	.LC94
	.long	5
	.zero	4
	.quad	.LC95
	.long	5
	.zero	4
	.quad	.LC96
	.long	9
	.zero	4
	.quad	.LC97
	.long	9
	.zero	4
	.quad	.LC98
	.long	9
	.zero	4
	.quad	.LC99
	.long	9
	.zero	4
	.quad	.LC100
	.long	5
	.zero	4
	.quad	.LC101
	.long	6
	.zero	4
	.quad	.LC102
	.long	5
	.zero	4
	.quad	.LC103
	.long	9
	.zero	4
	.quad	.LC104
	.long	5
	.zero	4
	.quad	.LC105
	.long	9
	.zero	4
	.quad	.LC106
	.long	6
	.zero	4
	.quad	.LC107
	.long	6
	.zero	4
	.quad	.LC108
	.long	5
	.zero	4
	.quad	.LC109
	.long	9
	.zero	4
	.quad	.LC110
	.long	9
	.zero	4
	.quad	.LC111
	.long	5
	.zero	4
	.quad	.LC112
	.long	9
	.zero	4
	.quad	.LC113
	.long	9
	.zero	4
	.quad	.LC114
	.long	5
	.zero	4
	.quad	.LC115
	.long	9
	.zero	4
	.quad	.LC116
	.long	5
	.zero	4
	.quad	.LC117
	.long	9
	.zero	4
	.quad	.LC118
	.long	5
	.zero	4
	.quad	.LC119
	.long	9
	.zero	4
	.quad	.LC120
	.long	9
	.zero	4
	.quad	.LC121
	.long	9
	.zero	4
	.quad	.LC122
	.long	9
	.zero	4
	.quad	.LC123
	.long	5
	.zero	4
	.quad	.LC124
	.long	5
	.zero	4
	.quad	.LC125
	.long	5
	.zero	4
	.quad	.LC126
	.long	9
	.zero	4
	.quad	.LC127
	.long	5
	.zero	4
	.quad	.LC128
	.long	5
	.zero	4
	.quad	.LC129
	.long	9
	.zero	4
	.quad	.LC130
	.long	9
	.zero	4
	.quad	.LC131
	.long	9
	.zero	4
	.quad	.LC132
	.long	5
	.zero	4
	.quad	.LC133
	.long	9
	.zero	4
	.quad	.LC134
	.long	9
	.zero	4
	.quad	.LC135
	.long	9
	.zero	4
	.quad	.LC136
	.long	9
	.zero	4
	.quad	.LC137
	.long	5
	.zero	4
	.quad	.LC138
	.long	5
	.zero	4
	.quad	.LC139
	.long	5
	.zero	4
	.quad	.LC140
	.long	9
	.zero	4
	.quad	.LC141
	.long	9
	.zero	4
	.quad	.LC142
	.long	9
	.zero	4
	.quad	.LC143
	.long	9
	.zero	4
	.quad	.LC144
	.long	5
	.zero	4
	.quad	.LC145
	.long	9
	.zero	4
	.quad	.LC146
	.long	5
	.zero	4
	.quad	.LC147
	.long	9
	.zero	4
	.quad	.LC148
	.long	9
	.zero	4
	.quad	.LC149
	.long	9
	.zero	4
	.quad	.LC150
	.long	9
	.zero	4
	.quad	.LC151
	.long	9
	.zero	4
	.quad	.LC152
	.long	9
	.zero	4
	.quad	.LC153
	.long	9
	.zero	4
	.quad	.LC154
	.long	9
	.zero	4
	.quad	.LC155
	.long	5
	.zero	4
	.quad	.LC156
	.long	5
	.zero	4
	.quad	.LC157
	.long	9
	.zero	4
	.quad	.LC158
	.long	9
	.zero	4
	.quad	.LC159
	.long	9
	.zero	4
	.quad	.LC160
	.long	9
	.zero	4
	.quad	.LC161
	.long	9
	.zero	4
	.quad	.LC162
	.long	9
	.zero	4
	.quad	.LC163
	.long	9
	.zero	4
	.quad	.LC164
	.long	9
	.zero	4
	.quad	.LC165
	.long	9
	.zero	4
	.quad	.LC166
	.long	5
	.zero	4
	.quad	.LC167
	.long	5
	.zero	4
	.quad	.LC168
	.long	5
	.zero	4
	.quad	.LC169
	.long	6
	.zero	4
	.quad	.LC170
	.long	5
	.zero	4
	.quad	.LC171
	.long	6
	.zero	4
	.quad	.LC172
	.long	5
	.zero	4
	.quad	.LC173
	.long	5
	.zero	4
	.quad	.LC174
	.long	9
	.zero	4
	.quad	.LC175
	.long	9
	.zero	4
	.quad	.LC176
	.long	5
	.zero	4
	.quad	.LC177
	.long	5
	.zero	4
	.quad	.LC178
	.long	5
	.zero	4
	.quad	.LC179
	.long	9
	.zero	4
	.quad	.LC180
	.long	9
	.zero	4
	.quad	.LC181
	.long	5
	.zero	4
	.quad	.LC182
	.long	9
	.zero	4
	.quad	.LC183
	.long	5
	.zero	4
	.quad	.LC184
	.long	5
	.zero	4
	.quad	.LC185
	.long	9
	.zero	4
	.quad	.LC186
	.long	9
	.zero	4
	.quad	.LC187
	.long	9
	.zero	4
	.quad	.LC188
	.long	5
	.zero	4
	.quad	.LC189
	.long	9
	.zero	4
	.quad	.LC190
	.long	5
	.zero	4
	.quad	.LC191
	.long	5
	.zero	4
	.quad	.LC192
	.long	9
	.zero	4
	.quad	.LC193
	.long	5
	.zero	4
	.quad	.LC194
	.long	9
	.zero	4
	.quad	.LC195
	.long	9
	.zero	4
	.quad	.LC196
	.long	9
	.zero	4
	.quad	.LC197
	.long	5
	.zero	4
	.quad	.LC198
	.long	10
	.zero	4
	.quad	.LC199
	.long	9
	.zero	4
	.quad	.LC200
	.long	5
	.zero	4
	.quad	.LC201
	.long	5
	.zero	4
	.quad	.LC202
	.long	9
	.zero	4
	.quad	.LC203
	.long	9
	.zero	4
	.quad	.LC204
	.long	9
	.zero	4
	.quad	.LC205
	.long	5
	.zero	4
	.quad	.LC206
	.long	9
	.zero	4
	.quad	.LC207
	.long	5
	.zero	4
	.quad	.LC208
	.long	9
	.zero	4
	.quad	.LC209
	.long	9
	.zero	4
	.quad	.LC210
	.long	9
	.zero	4
	.quad	.LC211
	.long	9
	.zero	4
	.quad	.LC212
	.long	9
	.zero	4
	.quad	.LC213
	.long	5
	.zero	4
	.quad	.LC214
	.long	9
	.zero	4
	.quad	.LC215
	.long	5
	.zero	4
	.quad	.LC216
	.long	9
	.zero	4
	.quad	.LC217
	.long	9
	.zero	4
	.quad	.LC218
	.long	9
	.zero	4
	.quad	.LC219
	.long	9
	.zero	4
	.quad	.LC220
	.long	5
	.zero	4
	.quad	.LC221
	.long	5
	.zero	4
	.quad	.LC222
	.long	9
	.zero	4
	.quad	.LC223
	.long	9
	.zero	4
	.quad	.LC224
	.long	5
	.zero	4
	.quad	.LC225
	.long	5
	.zero	4
	.quad	.LC226
	.long	9
	.zero	4
	.quad	.LC227
	.long	9
	.zero	4
	.quad	.LC228
	.long	9
	.zero	4
	.quad	.LC229
	.long	5
	.zero	4
	.quad	.LC230
	.long	9
	.zero	4
	.quad	.LC231
	.long	9
	.zero	4
	.quad	.LC232
	.long	9
	.zero	4
	.quad	.LC233
	.long	9
	.zero	4
	.quad	.LC234
	.long	5
	.zero	4
	.quad	.LC235
	.long	9
	.zero	4
	.quad	.LC236
	.long	5
	.zero	4
	.quad	.LC237
	.long	9
	.zero	4
	.quad	.LC238
	.long	9
	.zero	4
	.quad	.LC239
	.long	9
	.zero	4
	.quad	.LC240
	.long	5
	.zero	4
	.quad	.LC241
	.long	5
	.zero	4
	.quad	.LC242
	.long	9
	.zero	4
	.quad	.LC243
	.long	9
	.zero	4
	.quad	.LC244
	.long	9
	.zero	4
	.quad	.LC245
	.long	5
	.zero	4
	.quad	.LC246
	.long	9
	.zero	4
	.quad	.LC247
	.long	5
	.zero	4
	.quad	.LC248
	.long	9
	.zero	4
	.quad	.LC249
	.long	5
	.zero	4
	.quad	.LC250
	.long	5
	.zero	4
	.quad	.LC251
	.long	9
	.zero	4
	.quad	.LC252
	.long	9
	.zero	4
	.quad	.LC253
	.long	9
	.zero	4
	.quad	.LC254
	.long	5
	.zero	4
	.quad	.LC255
	.long	9
	.zero	4
	.quad	.LC256
	.long	5
	.zero	4
	.quad	.LC257
	.long	9
	.zero	4
	.quad	.LC258
	.long	9
	.zero	4
	.quad	.LC259
	.long	9
	.zero	4
	.quad	.LC260
	.long	5
	.zero	4
	.quad	.LC261
	.long	5
	.zero	4
	.quad	.LC262
	.long	9
	.zero	4
	.quad	.LC263
	.long	9
	.zero	4
	.quad	.LC264
	.long	9
	.zero	4
	.quad	.LC265
	.long	9
	.zero	4
	.quad	.LC266
	.long	9
	.zero	4
	.quad	.LC267
	.long	5
	.zero	4
	.quad	.LC268
	.long	5
	.zero	4
	.quad	.LC269
	.long	9
	.zero	4
	.quad	.LC270
	.long	9
	.zero	4
	.quad	.LC271
	.long	10
	.zero	4
	.quad	.LC272
	.long	10
	.zero	4
	.quad	.LC273
	.long	10
	.zero	4
	.quad	.LC274
	.long	5
	.zero	4
	.quad	.LC275
	.long	9
	.zero	4
	.quad	.LC276
	.long	9
	.zero	4
	.quad	.LC277
	.long	5
	.zero	4
	.quad	.LC278
	.long	9
	.zero	4
	.quad	.LC279
	.long	9
	.zero	4
	.quad	.LC280
	.long	5
	.zero	4
	.quad	.LC281
	.long	9
	.zero	4
	.quad	.LC282
	.long	9
	.zero	4
	.quad	.LC283
	.long	9
	.zero	4
	.quad	.LC284
	.long	10
	.zero	4
	.quad	.LC285
	.long	10
	.zero	4
	.quad	.LC286
	.long	10
	.zero	4
	.quad	.LC287
	.long	10
	.zero	4
	.quad	.LC288
	.long	10
	.zero	4
	.quad	.LC289
	.long	10
	.zero	4
	.quad	.LC290
	.long	9
	.zero	4
	.quad	.LC291
	.long	10
	.zero	4
	.quad	.LC292
	.long	6
	.zero	4
	.quad	.LC293
	.long	10
	.zero	4
	.quad	.LC294
	.long	10
	.zero	4
	.quad	.LC295
	.long	9
	.zero	4
	.quad	.LC296
	.long	10
	.zero	4
	.quad	.LC297
	.long	9
	.zero	4
	.quad	.LC298
	.long	10
	.zero	4
	.quad	.LC299
	.long	6
	.zero	4
	.quad	.LC300
	.long	10
	.zero	4
	.quad	.LC301
	.long	10
	.zero	4
	.quad	.LC302
	.long	10
	.zero	4
	.quad	.LC303
	.long	10
	.zero	4
	.quad	.LC304
	.long	5
	.zero	4
	.quad	.LC305
	.long	9
	.zero	4
	.quad	.LC306
	.long	5
	.zero	4
	.quad	.LC307
	.long	5
	.zero	4
	.quad	.LC308
	.long	5
	.zero	4
	.quad	.LC309
	.long	5
	.zero	4
	.quad	.LC310
	.long	6
	.zero	4
	.quad	.LC311
	.long	9
	.zero	4
	.quad	.LC312
	.long	5
	.zero	4
	.quad	.LC313
	.long	9
	.zero	4
	.quad	.LC314
	.long	5
	.zero	4
	.quad	.LC315
	.long	5
	.zero	4
	.quad	.LC316
	.long	5
	.zero	4
	.quad	.LC317
	.long	5
	.zero	4
	.quad	.LC318
	.long	5
	.zero	4
	.quad	0
	.long	0
	.zero	4
	.local	_ZL19gCurrencyCacheMutex
	.comm	_ZL19gCurrencyCacheMutex,56,32
	.local	_ZL22currentCacheEntryIndex
	.comm	_ZL22currentCacheEntryIndex,1,1
	.local	_ZL9currCache
	.comm	_ZL9currCache,80,32
	.local	_ZL25gCurrSymbolsEquivInitOnce
	.comm	_ZL25gCurrSymbolsEquivInitOnce,8,8
	.local	_ZL17gCurrSymbolsEquiv
	.comm	_ZL17gCurrSymbolsEquiv,8,8
	.local	_ZL17gIsoCodesInitOnce
	.comm	_ZL17gIsoCodesInitOnce,8,8
	.local	_ZL9gIsoCodes
	.comm	_ZL9gIsoCodes,8,8
	.section	.rodata
	.align 16
	.type	_ZL15CURRENCYPLURALS, @object
	.size	_ZL15CURRENCYPLURALS, 16
_ZL15CURRENCYPLURALS:
	.string	"CurrencyPlurals"
	.align 16
	.type	_ZL17CURRENCIES_NARROW, @object
	.size	_ZL17CURRENCIES_NARROW, 18
_ZL17CURRENCIES_NARROW:
	.string	"Currencies%narrow"
	.align 8
	.type	_ZL10CURRENCIES, @object
	.size	_ZL10CURRENCIES, 11
_ZL10CURRENCIES:
	.string	"Currencies"
	.align 8
	.type	_ZL12DEFAULT_META, @object
	.size	_ZL12DEFAULT_META, 8
_ZL12DEFAULT_META:
	.string	"DEFAULT"
	.align 8
	.type	_ZL12CURRENCY_MAP, @object
	.size	_ZL12CURRENCY_MAP, 12
_ZL12CURRENCY_MAP:
	.string	"CurrencyMap"
	.align 8
	.type	_ZL13CURRENCY_META, @object
	.size	_ZL13CURRENCY_META, 13
_ZL13CURRENCY_META:
	.string	"CurrencyMeta"
	.align 16
	.type	_ZL13CURRENCY_DATA, @object
	.size	_ZL13CURRENCY_DATA, 17
_ZL13CURRENCY_DATA:
	.string	"supplementalData"
	.align 32
	.type	_ZL5POW10, @object
	.size	_ZL5POW10, 40
_ZL5POW10:
	.long	1
	.long	10
	.long	100
	.long	1000
	.long	10000
	.long	100000
	.long	1000000
	.long	10000000
	.long	100000000
	.long	1000000000
	.align 16
	.type	_ZL16LAST_RESORT_DATA, @object
	.size	_ZL16LAST_RESORT_DATA, 16
_ZL16LAST_RESORT_DATA:
	.long	2
	.long	0
	.long	2
	.long	0
	.align 32
	.type	_ZN6icu_677unisetsL16kCurrencyEntriesE, @object
	.size	_ZN6icu_677unisetsL16kCurrencyEntriesE, 40
_ZN6icu_677unisetsL16kCurrencyEntriesE:
	.long	16
	.long	36
	.long	17
	.long	163
	.long	18
	.long	8377
	.long	19
	.long	165
	.long	20
	.long	8361
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC6:
	.long	4294967295
	.long	-1048577
	.align 8
.LC7:
	.long	4294967295
	.long	2146435071
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
