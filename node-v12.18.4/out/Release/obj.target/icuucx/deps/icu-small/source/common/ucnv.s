	.file	"ucnv.cpp"
	.text
	.p2align 4
	.type	_ZL6_resetP10UConverter21UConverterResetChoicea, @function
_ZL6_resetP10UConverter21UConverterResetChoicea:
.LFB2127:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1
	movq	%rdi, %r12
	movl	%esi, %r13d
	testb	%dl, %dl
	je	.L4
	cmpl	$2, %esi
	jne	.L5
.L9:
	movq	(%r12), %rax
	cmpq	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %rax
	je	.L4
	subq	$8, %rsp
	movl	$56, %ecx
	leaq	-84(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	24(%r12), %rdi
	movw	%cx, -80(%rbp)
	leaq	-80(%rbp), %rsi
	xorl	%ecx, %ecx
	pushq	%rdx
	movl	$3, %r9d
	xorl	%edx, %edx
	movb	$1, -78(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movq	%r12, -72(%rbp)
	movl	$0, -84(%rbp)
	call	*%rax
	popq	%rsi
	popq	%rdi
.L4:
	movq	48(%r12), %rax
	cmpl	$2, %r13d
	jne	.L11
.L13:
	movq	$0, 80(%r12)
	xorl	%edx, %edx
	movw	%dx, 91(%r12)
	movl	$-1, 208(%r12)
	movb	$0, 281(%r12)
.L12:
	movq	32(%rax), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L1
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*%rax
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movl	40(%rax), %edx
	movb	$0, 64(%r12)
	movl	$0, 76(%r12)
	movl	%edx, 72(%r12)
	movb	$0, 93(%r12)
	movb	$0, 90(%r12)
	movb	$0, 282(%r12)
	cmpl	$1, %r13d
	je	.L12
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L5:
	movq	8(%rdi), %rax
	cmpq	UCNV_TO_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %rax
	je	.L8
	movl	$56, %r8d
	movq	%rdi, -72(%rbp)
	leaq	-80(%rbp), %rsi
	xorl	%ecx, %ecx
	movw	%r8w, -80(%rbp)
	movq	32(%rdi), %rdi
	leaq	-84(%rbp), %r9
	xorl	%edx, %edx
	movb	$1, -78(%rbp)
	movl	$3, %r8d
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movl	$0, -84(%rbp)
	call	*%rax
.L8:
	cmpl	$1, %r13d
	jne	.L9
	movq	48(%r12), %rax
	cmpl	$2, %r13d
	je	.L13
	jmp	.L11
.L22:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2127:
	.size	_ZL6_resetP10UConverter21UConverterResetChoicea, .-_ZL6_resetP10UConverter21UConverterResetChoicea
	.p2align 4
	.type	_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode, @function
_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode:
.LFB2144:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	8(%rdi), %r12
	movq	48(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	32(%rdi), %r11
	movq	%rax, -104(%rbp)
	movq	48(%r12), %rax
	movq	32(%rax), %rax
	testq	%r13, %r13
	je	.L84
	movq	56(%rax), %rdi
	xorl	%r14d, %r14d
	movq	%rdi, -160(%rbp)
	testq	%rdi, %rdi
	je	.L85
.L25:
	movzbl	282(%r12), %eax
	testb	%al, %al
	js	.L86
.L62:
	movb	$0, -145(%rbp)
	movl	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -112(%rbp)
.L26:
	movl	(%r15), %eax
	movq	%r15, %r9
.L27:
	testl	%eax, %eax
	jle	.L28
	movb	$0, -146(%rbp)
	movq	16(%rbx), %r15
.L29:
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L58:
	testq	%r13, %r13
	je	.L33
	movq	32(%rbx), %rdx
	subq	%r11, %rdx
	sarq	%rdx
	testl	%edx, %edx
	jg	.L87
.L34:
	movq	%r15, %rax
	subq	-104(%rbp), %rax
	addl	%r14d, %eax
	testl	%r14d, %r14d
	cmovns	%eax, %r14d
.L33:
	movzbl	282(%r12), %eax
	testb	%al, %al
	js	.L88
.L40:
	movl	(%r9), %eax
	movq	32(%rbx), %r11
	testl	%eax, %eax
	jg	.L64
	cmpq	%r15, 24(%rbx)
	ja	.L43
	cmpq	$0, -112(%rbp)
	jne	.L89
	cmpb	$0, 2(%rbx)
	je	.L23
	cmpb	$0, 64(%r12)
	jle	.L46
	movl	$11, (%r9)
	movq	%r15, -104(%rbp)
.L47:
	movsbq	64(%r12), %rax
	leaq	96(%r12), %rdx
	movb	%al, 90(%r12)
	movq	%rax, %rcx
	testl	%eax, %eax
	jg	.L90
.L51:
	movl	284(%r12), %r8d
	movb	$0, 64(%r12)
	cmpl	$1, %r8d
	jne	.L57
	cmpl	$10, (%r9)
	jne	.L57
	movl	$0, 284(%r12)
	xorl	%r8d, %r8d
.L57:
	movq	%r9, -128(%rbp)
	movl	%eax, %ecx
	movq	32(%r12), %rdi
	movq	%rbx, %rsi
	movq	%r11, -136(%rbp)
	movl	%eax, -120(%rbp)
	call	*8(%r12)
	movq	16(%rbx), %r15
	movl	-120(%rbp), %eax
	movl	$1, 284(%r12)
	movq	-128(%rbp), %r9
	movq	-136(%rbp), %r11
	movl	$1, %r8d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L88:
	cmpq	$0, -112(%rbp)
	je	.L91
	movl	$5, (%r9)
	movq	32(%rbx), %r11
	movl	$5, %eax
	movq	%r15, -104(%rbp)
.L42:
	testb	%r8b, %r8b
	jne	.L49
	cmpl	$15, %eax
	je	.L49
	leal	-10(%rax), %edx
	cmpl	$2, %edx
	jbe	.L47
	subl	$18, %eax
	cmpl	$1, %eax
	jbe	.L47
	.p2align 4,,10
	.p2align 3
.L49:
	cmpq	$0, -112(%rbp)
	je	.L23
	movq	24(%rbx), %r13
	subq	%r15, %r13
	testl	%r13d, %r13d
	jg	.L92
.L52:
	movzbl	-145(%rbp), %eax
	movq	-112(%rbp), %xmm0
	movb	%al, 2(%rbx)
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, 16(%rbx)
.L23:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movslq	%edx, %rdx
	leaq	0(%r13,%rdx,4), %rcx
	testl	%r14d, %r14d
	js	.L35
	movl	%r14d, %edx
	subl	%eax, %edx
	testl	%edx, %edx
	jne	.L36
.L37:
	movq	%rcx, 48(%rbx)
	movq	%rcx, %r13
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	65(%r12), %rsi
	cmpl	$8, %eax
	jnb	.L53
	testb	$4, %al
	jne	.L94
	testl	%eax, %eax
	je	.L51
	movzbl	65(%r12), %edi
	movb	%dil, 96(%r12)
	testb	$2, %al
	je	.L51
	movl	%eax, %ecx
	movzwl	-2(%rsi,%rcx), %esi
	movw	%si, -2(%rdx,%rcx)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r15, -104(%rbp)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L53:
	movq	65(%r12), %rdi
	movq	%rdi, 96(%r12)
	movl	%eax, %edi
	movq	-8(%rsi,%rdi), %r8
	movq	%r8, -8(%rdx,%rdi)
	leaq	104(%r12), %rdi
	movq	%rdx, %r8
	andq	$-8, %rdi
	subq	%rdi, %r8
	addl	%r8d, %ecx
	subq	%r8, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L92:
	movslq	%r13d, %rdx
	leaq	250(%r12), %rdi
	movq	%r15, %rsi
	negl	%r13d
	call	memcpy@PLT
	movb	%r13b, 282(%r12)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L85:
	movq	48(%rax), %rax
	movl	$-1, %r14d
	movq	%rax, -160(%rbp)
	movzbl	282(%r12), %eax
	testb	%al, %al
	jns	.L62
.L86:
	movq	24(%rbx), %rsi
	movsbl	%al, %edx
	leaq	-96(%rbp), %rdi
	movl	$31, %ecx
	movl	%edx, %eax
	movq	%r11, -120(%rbp)
	movq	%rsi, -144(%rbp)
	movzbl	2(%rbx), %esi
	negl	%eax
	movslq	%eax, %rdx
	movb	%sil, -145(%rbp)
	leaq	250(%r12), %rsi
	call	__memcpy_chk@PLT
	movl	%r14d, -152(%rbp)
	movq	-120(%rbp), %r11
	movl	$-1, %r14d
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movsbq	282(%r12), %rax
	movb	$0, 2(%rbx)
	subq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rdi, 24(%rbx)
	movq	%rax, -112(%rbp)
	movb	$0, 282(%r12)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L36:
	jg	.L83
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L39:
	movl	0(%r13), %eax
	testl	%eax, %eax
	js	.L38
	addl	%edx, %eax
	movl	%eax, 0(%r13)
.L38:
	addq	$4, %r13
.L83:
	cmpq	%rcx, %r13
	jb	.L39
	movq	%rcx, 48(%rbx)
	movq	%rcx, %r13
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%r11, -128(%rbp)
	movq	-160(%rbp), %rax
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r9, -120(%rbp)
	call	*%rax
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r11
	movq	16(%rbx), %r15
	movl	(%r9), %eax
	testl	%eax, %eax
	jle	.L30
.L31:
	movb	$0, -146(%rbp)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L84:
	movq	48(%rax), %rax
	xorl	%r14d, %r14d
	movq	%rax, -160(%rbp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L91:
	movq	24(%rbx), %rsi
	movsbl	%al, %edx
	leaq	-96(%rbp), %rdi
	movl	$31, %ecx
	movl	%edx, %eax
	movq	%r9, -120(%rbp)
	movq	%rsi, -144(%rbp)
	movzbl	2(%rbx), %esi
	negl	%eax
	movslq	%eax, %rdx
	movb	%r8b, -104(%rbp)
	movb	%sil, -145(%rbp)
	leaq	250(%r12), %rsi
	call	__memcpy_chk@PLT
	movl	%r14d, -152(%rbp)
	movzbl	-104(%rbp), %r8d
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	-120(%rbp), %r9
	movsbq	282(%r12), %rax
	movb	$0, 2(%rbx)
	movq	%r15, -112(%rbp)
	subq	%rax, %rdi
	movq	%rdi, 24(%rbx)
	movsbl	282(%r12), %eax
	movb	$0, 282(%r12)
	movq	16(%rbx), %r15
	addl	%r14d, %eax
	movl	$-1, %r14d
	cmovns	%eax, %r14d
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L35:
	cmpq	%rcx, %r13
	jnb	.L37
	movq	%r13, %rax
	movq	%r13, %rdi
	movl	$255, %esi
	movb	%r8b, -128(%rbp)
	notq	%rax
	movq	%r9, -136(%rbp)
	addq	%rcx, %rax
	movq	%rcx, -120(%rbp)
	shrq	$2, %rax
	leaq	4(,%rax,4), %rdx
	call	memset@PLT
	movq	-120(%rbp), %rcx
	movzbl	-128(%rbp), %r8d
	movq	-136(%rbp), %r9
	movq	%rcx, 48(%rbx)
	movq	%rcx, %r13
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L30:
	cmpb	$0, 2(%rbx)
	je	.L31
	cmpq	%r15, 24(%rbx)
	jne	.L31
	cmpb	$0, 64(%r12)
	movb	$1, -146(%rbp)
	je	.L29
	jmp	.L31
.L89:
	movzbl	-145(%rbp), %esi
	movq	-112(%rbp), %xmm0
	movq	$0, -112(%rbp)
	movl	-152(%rbp), %r14d
	movb	%sil, 2(%rbx)
	movhps	-144(%rbp), %xmm0
	movaps	%xmm0, 16(%rbx)
.L43:
	movq	%r15, -104(%rbp)
	jmp	.L27
.L46:
	cmpb	$0, -146(%rbp)
	je	.L43
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L94:
	movl	65(%r12), %edi
	movl	%eax, %ecx
	movl	%edi, 96(%r12)
	movl	-4(%rsi,%rcx), %esi
	movl	%esi, -4(%rdx,%rcx)
	jmp	.L51
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2144:
	.size	_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode, .-_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode
	.p2align 4
	.type	_ZL24_fromUnicodeWithCallbackP25UConverterFromUnicodeArgsP10UErrorCode, @function
_ZL24_fromUnicodeWithCallbackP25UConverterFromUnicodeArgsP10UErrorCode:
.LFB2141:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	8(%rdi), %r12
	movq	48(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	32(%rdi), %r10
	movq	%rax, -104(%rbp)
	movq	48(%r12), %rax
	movq	32(%rax), %rax
	testq	%r13, %r13
	je	.L150
	movq	72(%rax), %rdi
	xorl	%r15d, %r15d
	movq	%rdi, -144(%rbp)
	testq	%rdi, %rdi
	je	.L151
.L97:
	movzbl	281(%r12), %eax
	testb	%al, %al
	js	.L152
.L130:
	movb	$0, -125(%rbp)
	movl	$0, -148(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -112(%rbp)
.L98:
	movl	(%r14), %eax
	movq	%r14, %r11
	movl	%eax, %edx
	movl	%r15d, %eax
	movq	%r10, %r15
	movl	%eax, %r10d
.L99:
	testl	%edx, %edx
	jle	.L100
	movb	$0, -126(%rbp)
	movq	16(%rbx), %r14
.L101:
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%r15, %r14
	xorl	%ecx, %ecx
	movq	%r11, %r15
	testq	%r13, %r13
	je	.L105
	movq	32(%rbx), %rax
	subq	%r14, %rax
	testl	%eax, %eax
	jg	.L153
.L106:
	testl	%r10d, %r10d
	js	.L105
	movq	%r9, %rax
	subq	-104(%rbp), %rax
	sarq	%rax
	addl	%eax, %r10d
.L105:
	movzbl	281(%r12), %eax
	testb	%al, %al
	js	.L154
.L112:
	movl	(%r15), %edx
	movq	32(%rbx), %r14
	testl	%edx, %edx
	jg	.L132
	cmpq	%r9, 24(%rbx)
	ja	.L147
	cmpq	$0, -112(%rbp)
	jne	.L155
	cmpb	$0, 2(%rbx)
	je	.L95
	movl	84(%r12), %ecx
	testl	%ecx, %ecx
	je	.L118
	movl	$11, (%r15)
	movq	%r9, -104(%rbp)
.L119:
	movl	84(%r12), %r8d
	cmpl	$65535, %r8d
	ja	.L156
	movl	%r8d, %edx
	movl	$1, %esi
	movl	$1, %ecx
.L125:
	movb	%sil, 92(%r12)
	xorl	%r9d, %r9d
	cmpl	$10, (%r15)
	movq	%rbx, %rsi
	setne	%r9b
	subq	$8, %rsp
	movw	%dx, 140(%r12)
	movq	24(%r12), %rdi
	movl	%r10d, -124(%rbp)
	leaq	140(%r12), %rdx
	movl	$0, 84(%r12)
	movl	%ecx, -120(%rbp)
	pushq	%r15
	call	*(%r12)
	popq	%rax
	movq	16(%rbx), %r9
	movl	$1, %r8d
	movl	-120(%rbp), %ecx
	movl	-124(%rbp), %r10d
	popq	%rdx
	testq	%r13, %r13
	je	.L105
	movq	32(%rbx), %rax
	subq	%r14, %rax
	testl	%eax, %eax
	jle	.L106
.L153:
	cltq
	leaq	0(%r13,%rax,4), %r14
	testl	%r10d, %r10d
	js	.L107
	movl	%r10d, %edx
	subl	%ecx, %edx
	testl	%edx, %edx
	jne	.L108
.L109:
	movq	%r14, 48(%rbx)
	movq	%r14, %r13
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L154:
	cmpq	$0, -112(%rbp)
	je	.L157
	movl	$5, (%r15)
	movq	32(%rbx), %r14
	movl	$5, %edx
	movq	%r9, -104(%rbp)
.L114:
	testb	%r8b, %r8b
	jne	.L121
	cmpl	$15, %edx
	je	.L121
	subl	$10, %edx
	cmpl	$2, %edx
	jbe	.L119
	.p2align 4,,10
	.p2align 3
.L121:
	cmpq	$0, -112(%rbp)
	je	.L95
	movq	24(%rbx), %r13
	subq	%r9, %r13
	sarq	%r13
	testl	%r13d, %r13d
	jg	.L158
.L124:
	movzbl	-125(%rbp), %eax
	movq	-112(%rbp), %xmm0
	movb	%al, 2(%rbx)
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, 16(%rbx)
.L95:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movl	%r8d, %ecx
	movl	%r8d, %edx
	movl	$2, %esi
	andw	$1023, %cx
	sarl	$10, %edx
	orw	$-9216, %cx
	subw	$10304, %dx
	movw	%cx, 142(%r12)
	movl	$2, %ecx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%r9, -104(%rbp)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L158:
	movl	%r13d, %edx
	leaq	212(%r12), %rdi
	movq	%r9, %rsi
	negl	%r13d
	call	u_memcpy_67@PLT
	movb	%r13b, 281(%r12)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L151:
	movq	64(%rax), %rax
	movl	$-1, %r15d
	movq	%rax, -144(%rbp)
	movzbl	281(%r12), %eax
	testb	%al, %al
	jns	.L130
.L152:
	movq	24(%rbx), %rsi
	movsbl	%al, %edx
	leaq	-96(%rbp), %rdi
	movl	$38, %ecx
	negl	%edx
	movq	%r10, -120(%rbp)
	movq	%rsi, -136(%rbp)
	movzbl	2(%rbx), %esi
	addl	%edx, %edx
	movslq	%edx, %rdx
	movb	%sil, -125(%rbp)
	leaq	212(%r12), %rsi
	call	__memcpy_chk@PLT
	movl	%r15d, -148(%rbp)
	movq	-120(%rbp), %r10
	movl	$-1, %r15d
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movsbq	281(%r12), %rax
	movb	$0, 2(%rbx)
	addq	%rax, %rax
	subq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rdi, 24(%rbx)
	movq	%rax, -112(%rbp)
	movb	$0, 281(%r12)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L108:
	jg	.L149
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L111:
	movl	0(%r13), %eax
	testl	%eax, %eax
	js	.L110
	addl	%edx, %eax
	movl	%eax, 0(%r13)
.L110:
	addq	$4, %r13
.L149:
	cmpq	%r14, %r13
	jb	.L111
	movq	%r14, 48(%rbx)
	movq	%r14, %r13
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L100:
	movl	%r10d, -124(%rbp)
	movq	%r11, %rsi
	movq	%rbx, %rdi
	movq	-144(%rbp), %rax
	movq	%r11, -120(%rbp)
	call	*%rax
	movq	-120(%rbp), %r11
	movl	-124(%rbp), %r10d
	movq	16(%rbx), %r14
	movl	(%r11), %edi
	testl	%edi, %edi
	jle	.L102
.L103:
	movb	$0, -126(%rbp)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L150:
	movq	64(%rax), %rax
	xorl	%r15d, %r15d
	movq	%rax, -144(%rbp)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L157:
	movq	24(%rbx), %rsi
	movsbl	%al, %edx
	leaq	-96(%rbp), %rdi
	movl	$38, %ecx
	negl	%edx
	movq	%r9, -112(%rbp)
	movq	%rsi, -136(%rbp)
	movzbl	2(%rbx), %esi
	addl	%edx, %edx
	movslq	%edx, %rdx
	movb	%r8b, -120(%rbp)
	movb	%sil, -125(%rbp)
	leaq	212(%r12), %rsi
	movl	%r10d, -104(%rbp)
	call	__memcpy_chk@PLT
	movl	-104(%rbp), %r10d
	movzbl	-120(%rbp), %r8d
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movsbq	281(%r12), %rax
	movb	$0, 2(%rbx)
	addq	%rax, %rax
	movl	%r10d, -148(%rbp)
	subq	%rax, %rdi
	movq	%rdi, 24(%rbx)
	movsbl	281(%r12), %eax
	movb	$0, 281(%r12)
	movq	16(%rbx), %r9
	addl	%r10d, %eax
	movl	$-1, %r10d
	cmovns	%eax, %r10d
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L107:
	cmpq	%r14, %r13
	jnb	.L109
	movq	%r13, %rax
	movq	%r13, %rdi
	movl	$255, %esi
	movb	%r8b, -124(%rbp)
	notq	%rax
	movq	%r9, -160(%rbp)
	movq	%r14, %r13
	addq	%r14, %rax
	movl	%r10d, -120(%rbp)
	shrq	$2, %rax
	leaq	4(,%rax,4), %rdx
	call	memset@PLT
	movq	%r14, 48(%rbx)
	movl	-120(%rbp), %r10d
	movzbl	-124(%rbp), %r8d
	movq	-160(%rbp), %r9
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L102:
	cmpb	$0, 2(%rbx)
	je	.L103
	cmpq	%r14, 24(%rbx)
	jne	.L103
	movl	84(%r12), %esi
	movb	$1, -126(%rbp)
	testl	%esi, %esi
	je	.L101
	jmp	.L103
.L118:
	cmpb	$0, -126(%rbp)
	movq	%r15, %r11
	movq	%r14, %r15
	movq	%r9, %r14
	jne	.L160
.L115:
	movq	%r14, -104(%rbp)
	jmp	.L99
.L147:
	movq	%r15, %r11
	movq	%r14, %r15
	movq	%r9, %r14
	jmp	.L115
.L155:
	movq	-112(%rbp), %xmm0
	movzbl	-125(%rbp), %eax
	movq	%r15, %r11
	movq	$0, -112(%rbp)
	movq	%r14, %r15
	movl	-148(%rbp), %r10d
	movq	%r9, %r14
	movhps	-136(%rbp), %xmm0
	movb	%al, 2(%rbx)
	movaps	%xmm0, 16(%rbx)
	jmp	.L115
.L160:
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	jmp	.L95
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2141:
	.size	_ZL24_fromUnicodeWithCallbackP25UConverterFromUnicodeArgsP10UErrorCode, .-_ZL24_fromUnicodeWithCallbackP25UConverterFromUnicodeArgsP10UErrorCode
	.p2align 4
	.globl	ucnv_open_67
	.type	ucnv_open_67, @function
ucnv_open_67:
.LFB2115:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	testq	%rsi, %rsi
	je	.L161
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L161
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	ucnv_createConverter_67@PLT
	.p2align 4,,10
	.p2align 3
.L161:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2115:
	.size	ucnv_open_67, .-ucnv_open_67
	.p2align 4
	.globl	ucnv_openPackage_67
	.type	ucnv_openPackage_67, @function
ucnv_openPackage_67:
.LFB2116:
	.cfi_startproc
	endbr64
	jmp	ucnv_createConverterFromPackage_67@PLT
	.cfi_endproc
.LFE2116:
	.size	ucnv_openPackage_67, .-ucnv_openPackage_67
	.p2align 4
	.globl	ucnv_openU_67
	.type	ucnv_openU_67, @function
ucnv_openU_67:
.LFB2117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L175
	movl	(%rsi), %edx
	movq	%rsi, %r12
	testl	%edx, %edx
	jg	.L175
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L176
	call	u_strlen_67@PLT
	cmpl	$59, %eax
	jg	.L177
	movq	%r13, %rsi
	leaq	-96(%rbp), %rdi
	call	u_austrcpy_67@PLT
	movq	%rax, %rsi
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L175
	movq	%r12, %rdx
	xorl	%edi, %edi
	call	ucnv_createConverter_67@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$1, (%r12)
.L175:
	xorl	%eax, %eax
.L167:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L178
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	%rsi, %rdx
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	ucnv_createConverter_67@PLT
	jmp	.L167
.L178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2117:
	.size	ucnv_openU_67, .-ucnv_openU_67
	.p2align 4
	.globl	ucnv_openCCSID_67
	.type	ucnv_openCCSID_67, @function
ucnv_openCCSID_67:
.LFB2119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L184
	movq	%rdx, %r12
	movl	(%rdx), %edx
	testl	%edx, %edx
	jg	.L179
	testl	%esi, %esi
	je	.L187
	movb	$0, -96(%rbp)
	leaq	-96(%rbp), %r13
	movq	%r13, %r8
.L182:
	movl	%edi, %esi
	movl	$10, %edx
	movq	%r8, %rdi
	call	T_CString_integerToString_67@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	ucnv_createConverter_67@PLT
.L179:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L188
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movl	$762143337, -96(%rbp)
	leaq	-92(%rbp), %r8
	leaq	-96(%rbp), %r13
	movb	$0, -92(%rbp)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L184:
	xorl	%eax, %eax
	jmp	.L179
.L188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2119:
	.size	ucnv_openCCSID_67, .-ucnv_openCCSID_67
	.p2align 4
	.globl	ucnv_safeClone_67
	.type	ucnv_safeClone_67, @function
ucnv_safeClone_67:
.LFB2120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movl	$56, %r9d
	movl	$56, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%r9w, -176(%rbp)
	movb	$1, -174(%rbp)
	movw	%r10w, -112(%rbp)
	movb	$1, -110(%rbp)
	movups	%xmm0, -168(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	testq	%rcx, %rcx
	je	.L230
	movl	(%rcx), %r8d
	movq	%rcx, %r13
	testl	%r8d, %r8d
	jg	.L230
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L231
	movq	48(%rdi), %rax
	movq	%rsi, %r14
	movq	%rdx, %r15
	movq	32(%rax), %rax
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L194
	movl	$0, -184(%rbp)
	leaq	-184(%rbp), %rdx
	xorl	%esi, %esi
	call	*%rax
	movl	0(%r13), %edi
	testl	%edi, %edi
	jg	.L230
	movl	-184(%rbp), %edx
	testq	%r15, %r15
	je	.L232
.L196:
	movl	(%r15), %eax
	movl	%eax, -188(%rbp)
	testl	%eax, %eax
	jle	.L233
	testq	%r14, %r14
	je	.L234
.L198:
	leaq	7(%r14), %rcx
	movslq	%edx, %r9
	movslq	%eax, %rdi
	andq	$-8, %rcx
	movq	%rcx, %rsi
	subq	%r14, %rsi
	leaq	(%r9,%rsi), %r10
	cmpq	%rdi, %r10
	jle	.L235
	movl	$1, -188(%rbp)
	cmpl	$1, %edx
	jg	.L199
	movq	$0, -200(%rbp)
	movq	%r14, %r12
.L202:
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	memset@PLT
	movq	(%rbx), %rax
	leaq	8(%r12), %rdi
	movq	%r12, %rcx
	andq	$-8, %rdi
	movq	%rbx, %rsi
	subq	%rdi, %rcx
	movq	%rax, (%r12)
	movq	280(%rbx), %rax
	subq	%rcx, %rsi
	addl	$288, %ecx
	movq	%rax, 280(%r12)
	shrl	$3, %ecx
	leaq	136(%rbx), %rax
	rep movsq
	xorl	%esi, %esi
	movw	%si, 61(%r12)
	cmpq	%rax, 40(%rbx)
	je	.L236
	movl	$64, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.L212
	movq	40(%rbx), %rdx
	movdqu	(%rdx), %xmm1
	movups	%xmm1, (%rax)
	movdqu	16(%rdx), %xmm2
	movups	%xmm2, 16(%rax)
	movdqu	32(%rdx), %xmm3
	movups	%xmm3, 32(%rax)
	movdqu	48(%rdx), %xmm4
	movups	%xmm4, 48(%rax)
.L206:
	movq	48(%rbx), %rax
	movq	32(%rax), %rax
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L208
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	*%rax
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L209
.L208:
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L209
	movq	48(%rbx), %rdi
	cmpb	$0, 25(%rdi)
	jne	.L237
	cmpq	%r12, %r14
	je	.L238
.L213:
	leaq	-180(%rbp), %r13
	movq	32(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-176(%rbp), %rsi
	movq	%r13, %r9
	movq	%r12, -104(%rbp)
	movl	$5, %r8d
	movq	%r12, -168(%rbp)
	movl	$0, -180(%rbp)
	call	*8(%rbx)
	subq	$8, %rsp
	movq	24(%rbx), %rdi
	xorl	%edx, %edx
	pushq	%r13
	leaq	-112(%rbp), %rsi
	movl	$5, %r9d
	xorl	%r8d, %r8d
	movl	$0, -180(%rbp)
	xorl	%ecx, %ecx
	call	*(%rbx)
	popq	%rax
	popq	%rdx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L209:
	movq	-200(%rbp), %rax
	testq	%rax, %rax
	je	.L212
	movq	40(%rax), %rdi
	addq	$136, %rax
	cmpq	%rax, %rdi
	je	.L212
	call	uprv_free_67@PLT
.L212:
	movq	-200(%rbp), %rdi
	call	uprv_free_67@PLT
.L230:
	xorl	%r12d, %r12d
.L189:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movl	$288, -184(%rbp)
	movl	$288, %edx
	testq	%r15, %r15
	jne	.L196
.L232:
	movl	$1, -188(%rbp)
	movl	$1, %eax
	leaq	-188(%rbp), %r15
	testq	%r14, %r14
	jne	.L198
.L234:
	movslq	%edx, %r9
.L203:
	xorl	%r14d, %r14d
.L199:
	movq	%r9, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L240
	movslq	-184(%rbp), %r9
	movl	$-126, 0(%r13)
	movq	%rax, -200(%rbp)
	movl	%r9d, (%r15)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L235:
	subl	%esi, %eax
	movq	%rcx, %r12
	movl	%eax, -188(%rbp)
	cmpl	%edx, %eax
	jl	.L241
	testq	%rcx, %rcx
	je	.L203
	movq	$0, -200(%rbp)
	movq	%rcx, %r14
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L236:
	leaq	136(%r12), %rax
	movq	%rax, 40(%r12)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L237:
	call	ucnv_incrementRefCount_67@PLT
	cmpq	%r12, %r14
	jne	.L213
.L238:
	movb	$1, 61(%r12)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$1, (%rcx)
	xorl	%r12d, %r12d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L241:
	movq	%rcx, %r14
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L233:
	movl	%edx, (%r15)
	xorl	%r12d, %r12d
	jmp	.L189
.L239:
	call	__stack_chk_fail@PLT
.L240:
	movl	$7, 0(%r13)
	jmp	.L189
	.cfi_endproc
.LFE2120:
	.size	ucnv_safeClone_67, .-ucnv_safeClone_67
	.p2align 4
	.globl	ucnv_close_67
	.type	ucnv_close_67, @function
ucnv_close_67:
.LFB2121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -84(%rbp)
	testq	%rdi, %rdi
	je	.L242
	movq	8(%rdi), %rax
	cmpq	UCNV_TO_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %rax
	movq	%rdi, %r12
	je	.L244
	movl	$56, %edi
	movb	$1, -78(%rbp)
	leaq	-80(%rbp), %rsi
	xorl	%ecx, %ecx
	movw	%di, -80(%rbp)
	leaq	-84(%rbp), %r9
	movq	32(%r12), %rdi
	xorl	%edx, %edx
	movq	$0, -64(%rbp)
	movl	$4, %r8d
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movq	%r12, -72(%rbp)
	call	*%rax
.L244:
	movq	(%r12), %rax
	cmpq	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %rax
	je	.L245
	movl	$56, %edx
	subq	$8, %rsp
	movq	24(%r12), %rdi
	xorl	%ecx, %ecx
	movw	%dx, -80(%rbp)
	leaq	-84(%rbp), %rdx
	leaq	-80(%rbp), %rsi
	movl	$4, %r9d
	pushq	%rdx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movb	$1, -78(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movq	%r12, -72(%rbp)
	movl	$0, -84(%rbp)
	call	*%rax
	popq	%rcx
	popq	%rsi
.L245:
	movq	48(%r12), %rax
	movq	32(%rax), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L246
	movq	%r12, %rdi
	call	*%rax
.L246:
	movq	40(%r12), %rdi
	leaq	136(%r12), %rax
	cmpq	%rax, %rdi
	je	.L247
	call	uprv_free_67@PLT
.L247:
	movq	48(%r12), %rdi
	cmpb	$0, 25(%rdi)
	jne	.L255
.L248:
	cmpb	$0, 61(%r12)
	je	.L256
.L242:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L255:
	call	ucnv_unloadSharedDataIfReady_67@PLT
	jmp	.L248
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2121:
	.size	ucnv_close_67, .-ucnv_close_67
	.p2align 4
	.globl	ucnv_getAvailableName_67
	.type	ucnv_getAvailableName_67, @function
ucnv_getAvailableName_67:
.LFB2122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$65535, %edi
	ja	.L258
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	ucnv_bld_getAvailableConverter_67@PLT
	movl	-12(%rbp), %edx
	testl	%edx, %edx
	movl	$0, %edx
	cmovg	%rdx, %rax
.L258:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L264
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L264:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2122:
	.size	ucnv_getAvailableName_67, .-ucnv_getAvailableName_67
	.p2align 4
	.globl	ucnv_countAvailable_67
	.type	ucnv_countAvailable_67, @function
ucnv_countAvailable_67:
.LFB2123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rdi
	movl	$0, -12(%rbp)
	call	ucnv_bld_countAvailableConverters_67@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L268
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzwl	%ax, %eax
	ret
.L268:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2123:
	.size	ucnv_countAvailable_67, .-ucnv_countAvailable_67
	.p2align 4
	.globl	ucnv_getSubstChars_67
	.type	ucnv_getSubstChars_67, @function
ucnv_getSubstChars_67:
.LFB2124:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	movq	%rdi, %r8
	testl	%eax, %eax
	jg	.L269
	movzbl	89(%rdi), %eax
	testb	%al, %al
	jle	.L281
	cmpb	(%rdx), %al
	jle	.L272
	movl	$8, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	movsbq	%al, %rcx
	movq	40(%rdi), %r9
	cmpl	$8, %ecx
	jnb	.L273
	testb	$4, %cl
	jne	.L282
	testl	%ecx, %ecx
	je	.L274
	movzbl	(%r9), %eax
	movb	%al, (%rsi)
	testb	$2, %cl
	je	.L280
	movl	%ecx, %ecx
	movzwl	-2(%r9,%rcx), %eax
	movw	%ax, -2(%rsi,%rcx)
	movzbl	89(%rdi), %eax
	.p2align 4,,10
	.p2align 3
.L274:
	movb	%al, (%rdx)
.L269:
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	movb	$0, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	movq	(%r9), %rax
	movq	%rax, (%rsi)
	movl	%ecx, %eax
	movq	-8(%r9,%rax), %rdi
	movq	%rdi, -8(%rsi,%rax)
	leaq	8(%rsi), %rdi
	movq	%rsi, %rax
	movq	%r9, %rsi
	andq	$-8, %rdi
	subq	%rdi, %rax
	addl	%eax, %ecx
	subq	%rax, %rsi
	shrl	$3, %ecx
	rep movsq
.L280:
	movzbl	89(%r8), %eax
	movb	%al, (%rdx)
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L282:
	movl	(%r9), %eax
	movl	%ecx, %ecx
	movl	%eax, (%rsi)
	movl	-4(%r9,%rcx), %eax
	movl	%eax, -4(%rsi,%rcx)
	movzbl	89(%rdi), %eax
	movb	%al, (%rdx)
	jmp	.L269
	.cfi_endproc
.LFE2124:
	.size	ucnv_getSubstChars_67, .-ucnv_getSubstChars_67
	.p2align 4
	.globl	ucnv_setSubstChars_67
	.type	ucnv_setSubstChars_67, @function
ucnv_setSubstChars_67:
.LFB2125:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movl	(%rcx), %edi
	movsbq	%dl, %rax
	testl	%edi, %edi
	jg	.L283
	movq	48(%r8), %rdi
	movq	16(%rdi), %rdi
	cmpb	%dl, 71(%rdi)
	jl	.L285
	cmpb	%dl, 70(%rdi)
	jle	.L286
.L285:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	movq	40(%r8), %rcx
	cmpq	$8, %rax
	jnb	.L287
	testb	$4, %al
	jne	.L297
	testq	%rax, %rax
	je	.L288
	movzbl	(%rsi), %edi
	movb	%dil, (%rcx)
	testb	$2, %al
	je	.L288
	movzwl	-2(%rsi,%rax), %esi
	movw	%si, -2(%rcx,%rax)
	.p2align 4,,10
	.p2align 3
.L288:
	movb	%dl, 89(%r8)
	movb	$0, 94(%r8)
.L283:
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	movq	(%rsi), %rdi
	movq	%rdi, (%rcx)
	movq	-8(%rsi,%rax), %rdi
	movq	%rdi, -8(%rcx,%rax)
	leaq	8(%rcx), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%rax, %rcx
	shrq	$3, %rcx
	rep movsq
	jmp	.L288
.L297:
	movl	(%rsi), %edi
	movl	%edi, (%rcx)
	movl	-4(%rsi,%rax), %esi
	movl	%esi, -4(%rcx,%rax)
	jmp	.L288
	.cfi_endproc
.LFE2125:
	.size	ucnv_setSubstChars_67, .-ucnv_setSubstChars_67
	.p2align 4
	.globl	ucnv_reset_67
	.type	ucnv_reset_67, @function
ucnv_reset_67:
.LFB2128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L298
	movq	8(%rdi), %rax
	cmpq	UCNV_TO_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %rax
	movq	%rdi, %r12
	je	.L301
	movl	$56, %r8d
	movq	%rdi, -72(%rbp)
	leaq	-80(%rbp), %rsi
	xorl	%ecx, %ecx
	movw	%r8w, -80(%rbp)
	movq	32(%rdi), %rdi
	leaq	-84(%rbp), %r9
	xorl	%edx, %edx
	movb	$1, -78(%rbp)
	movl	$3, %r8d
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movl	$0, -84(%rbp)
	call	*%rax
.L301:
	movq	(%r12), %rax
	cmpq	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %rax
	je	.L302
	subq	$8, %rsp
	movl	$56, %ecx
	leaq	-84(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	24(%r12), %rdi
	movw	%cx, -80(%rbp)
	leaq	-80(%rbp), %rsi
	xorl	%ecx, %ecx
	pushq	%rdx
	movl	$3, %r9d
	xorl	%edx, %edx
	movb	$1, -78(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movq	%r12, -72(%rbp)
	movl	$0, -84(%rbp)
	call	*%rax
	popq	%rsi
	popq	%rdi
.L302:
	movq	48(%r12), %rax
	movl	40(%rax), %edx
	movb	$0, 64(%r12)
	movq	$0, 80(%r12)
	movl	%edx, 72(%r12)
	xorl	%edx, %edx
	movl	$0, 90(%r12)
	movw	%dx, 281(%r12)
	movq	32(%rax), %rax
	movl	$0, 76(%r12)
	movl	$-1, 208(%r12)
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L298
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*%rax
.L298:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L309
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L309:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2128:
	.size	ucnv_reset_67, .-ucnv_reset_67
	.p2align 4
	.globl	ucnv_resetToUnicode_67
	.type	ucnv_resetToUnicode_67, @function
ucnv_resetToUnicode_67:
.LFB2129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L310
	movq	8(%rdi), %rax
	cmpq	UCNV_TO_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %rax
	movq	%rdi, %r12
	je	.L313
	movl	$56, %edx
	movq	%rdi, -72(%rbp)
	leaq	-80(%rbp), %rsi
	xorl	%ecx, %ecx
	movw	%dx, -80(%rbp)
	movq	32(%rdi), %rdi
	leaq	-84(%rbp), %r9
	xorl	%edx, %edx
	movb	$1, -78(%rbp)
	movl	$3, %r8d
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movl	$0, -84(%rbp)
	call	*%rax
.L313:
	movq	48(%r12), %rax
	movl	40(%rax), %edx
	movb	$0, 64(%r12)
	movb	$0, 93(%r12)
	movb	$0, 90(%r12)
	movb	$0, 282(%r12)
	movq	32(%rax), %rax
	movl	%edx, 72(%r12)
	movl	$0, 76(%r12)
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L310
	movl	$1, %esi
	movq	%r12, %rdi
	call	*%rax
.L310:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L320
	addq	$88, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L320:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2129:
	.size	ucnv_resetToUnicode_67, .-ucnv_resetToUnicode_67
	.p2align 4
	.globl	ucnv_resetFromUnicode_67
	.type	ucnv_resetFromUnicode_67, @function
ucnv_resetFromUnicode_67:
.LFB2130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L321
	movq	(%rdi), %rax
	cmpq	UCNV_FROM_U_CALLBACK_SUBSTITUTE_67@GOTPCREL(%rip), %rax
	movq	%rdi, %r12
	je	.L324
	movl	$56, %edx
	subq	$8, %rsp
	movq	%rdi, -72(%rbp)
	xorl	%ecx, %ecx
	movw	%dx, -80(%rbp)
	leaq	-84(%rbp), %rdx
	movq	24(%rdi), %rdi
	leaq	-80(%rbp), %rsi
	pushq	%rdx
	movl	$3, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movb	$1, -78(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movl	$0, -84(%rbp)
	call	*%rax
	popq	%rcx
	popq	%rsi
.L324:
	movq	$0, 80(%r12)
	xorl	%eax, %eax
	movw	%ax, 91(%r12)
	movq	48(%r12), %rax
	movb	$0, 281(%r12)
	movl	$-1, 208(%r12)
	movq	32(%rax), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L321
	movl	$2, %esi
	movq	%r12, %rdi
	call	*%rax
.L321:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L331:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2130:
	.size	ucnv_resetFromUnicode_67, .-ucnv_resetFromUnicode_67
	.p2align 4
	.globl	ucnv_getMaxCharSize_67
	.type	ucnv_getMaxCharSize_67, @function
ucnv_getMaxCharSize_67:
.LFB2131:
	.cfi_startproc
	endbr64
	movzbl	88(%rdi), %eax
	ret
	.cfi_endproc
.LFE2131:
	.size	ucnv_getMaxCharSize_67, .-ucnv_getMaxCharSize_67
	.p2align 4
	.globl	ucnv_getMinCharSize_67
	.type	ucnv_getMinCharSize_67, @function
ucnv_getMinCharSize_67:
.LFB2132:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	16(%rax), %rax
	movzbl	70(%rax), %eax
	ret
	.cfi_endproc
.LFE2132:
	.size	ucnv_getMinCharSize_67, .-ucnv_getMinCharSize_67
	.p2align 4
	.globl	ucnv_getName_67
	.type	ucnv_getName_67, @function
ucnv_getName_67:
.LFB2133:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L342
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rax
	movq	32(%rax), %rdx
	movq	96(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L336
	call	*%rdx
	testq	%rax, %rax
	je	.L345
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	movq	48(%rbx), %rax
.L336:
	movq	16(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	addq	$4, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2133:
	.size	ucnv_getName_67, .-ucnv_getName_67
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IBM"
	.text
	.p2align 4
	.globl	ucnv_getCCSID_67
	.type	ucnv_getCCSID_67, @function
ucnv_getCCSID_67:
.LFB2134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L350
	movq	48(%rdi), %rdx
	movq	%rdi, %r12
	movq	16(%rdx), %rax
	movl	64(%rax), %r13d
	testl	%r13d, %r13d
	jne	.L346
	movq	32(%rdx), %rdx
	movq	%rsi, %rbx
	movq	96(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L348
	call	*%rdx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L364
.L349:
	movq	%rbx, %rdx
	leaq	.LC0(%rip), %rsi
	call	ucnv_getStandardName_67@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L346
	testq	%rax, %rax
	jne	.L365
.L346:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movq	48(%r12), %rax
	movq	16(%rax), %rax
.L348:
	leaq	4(%rax), %rdi
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L365:
	movl	$45, %esi
	movq	%rax, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L346
	leaq	1(%rax), %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol@PLT
	movl	%eax, %r13d
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L350:
	movl	$-1, %r13d
	jmp	.L346
	.cfi_endproc
.LFE2134:
	.size	ucnv_getCCSID_67, .-ucnv_getCCSID_67
	.p2align 4
	.globl	ucnv_getPlatform_67
	.type	ucnv_getPlatform_67, @function
ucnv_getPlatform_67:
.LFB2135:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L368
	movq	48(%rdi), %rax
	movq	16(%rax), %rax
	movsbl	68(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2135:
	.size	ucnv_getPlatform_67, .-ucnv_getPlatform_67
	.p2align 4
	.globl	ucnv_getToUCallBack_67
	.type	ucnv_getToUCallBack_67, @function
ucnv_getToUCallBack_67:
.LFB2136:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rax, (%rsi)
	movq	32(%rdi), %rax
	movq	%rax, (%rdx)
	ret
	.cfi_endproc
.LFE2136:
	.size	ucnv_getToUCallBack_67, .-ucnv_getToUCallBack_67
	.p2align 4
	.globl	ucnv_getFromUCallBack_67
	.type	ucnv_getFromUCallBack_67, @function
ucnv_getFromUCallBack_67:
.LFB2137:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	movq	24(%rdi), %rax
	movq	%rax, (%rdx)
	ret
	.cfi_endproc
.LFE2137:
	.size	ucnv_getFromUCallBack_67, .-ucnv_getFromUCallBack_67
	.p2align 4
	.globl	ucnv_setToUCallBack_67
	.type	ucnv_setToUCallBack_67, @function
ucnv_setToUCallBack_67:
.LFB2138:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L371
	testq	%rcx, %rcx
	je	.L373
	movq	8(%rdi), %rax
	movq	%rax, (%rcx)
.L373:
	movq	%rsi, 8(%rdi)
	testq	%r8, %r8
	je	.L374
	movq	32(%rdi), %rax
	movq	%rax, (%r8)
.L374:
	movq	%rdx, 32(%rdi)
.L371:
	ret
	.cfi_endproc
.LFE2138:
	.size	ucnv_setToUCallBack_67, .-ucnv_setToUCallBack_67
	.p2align 4
	.globl	ucnv_setFromUCallBack_67
	.type	ucnv_setFromUCallBack_67, @function
ucnv_setFromUCallBack_67:
.LFB2139:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L381
	testq	%rcx, %rcx
	je	.L383
	movq	(%rdi), %rax
	movq	%rax, (%rcx)
.L383:
	movq	%rsi, (%rdi)
	testq	%r8, %r8
	je	.L384
	movq	24(%rdi), %rax
	movq	%rax, (%r8)
.L384:
	movq	%rdx, 24(%rdi)
.L381:
	ret
	.cfi_endproc
.LFE2139:
	.size	ucnv_setFromUCallBack_67, .-ucnv_setFromUCallBack_67
	.p2align 4
	.globl	ucnv_fromUnicode_67
	.type	ucnv_fromUnicode_67, @function
ucnv_fromUnicode_67:
.LFB2143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	24(%rbp), %rsi
	movl	16(%rbp), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L391
	movl	(%rsi), %r11d
	testl	%r11d, %r11d
	jg	.L391
	movq	%rcx, %r12
	testq	%rbx, %rbx
	sete	%cl
	testq	%r12, %r12
	sete	%al
	orb	%al, %cl
	jne	.L397
	testq	%rdi, %rdi
	je	.L397
	cmpq	$-2147483648, %r8
	movq	$-2147483648, %rcx
	movq	(%r12), %r13
	movq	(%rbx), %rax
	cmovbe	%r8, %rcx
	leaq	2147483647(%rcx), %r11
	addq	$2147483646, %rcx
	cmpq	%r11, %r8
	cmove	%rcx, %r8
	cmpq	%r13, %r8
	jb	.L397
	cmpq	%rdx, %rax
	ja	.L397
	movq	%r8, %rcx
	subq	%r13, %rcx
	cmpq	$2147483647, %rcx
	jbe	.L413
	cmpq	%r13, %r8
	ja	.L397
.L413:
	movq	%rdx, %r11
	subq	%rax, %r11
	cmpq	$2147483647, %r11
	jbe	.L414
	cmpq	%rdx, %rax
	jb	.L397
.L414:
	andl	$1, %ecx
	jne	.L397
	movsbl	91(%rdi), %r14d
	testb	%r14b, %r14b
	jg	.L460
.L400:
	testb	%r10b, %r10b
	jne	.L409
	cmpq	%r13, %r8
	jne	.L409
	cmpb	$0, 281(%rdi)
	jns	.L391
.L409:
	movq	%rdi, %xmm0
	movq	%r13, %xmm1
	movq	%rax, %xmm2
	movl	$56, %eax
	punpcklqdq	%xmm1, %xmm0
	movq	%r9, %xmm3
	leaq	-112(%rbp), %rdi
	movw	%ax, -112(%rbp)
	movups	%xmm0, -104(%rbp)
	movq	%r8, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movb	%r10b, -110(%rbp)
	movups	%xmm0, -88(%rbp)
	movq	%rdx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -72(%rbp)
	call	_ZL24_fromUnicodeWithCallbackP25UConverterFromUnicodeArgsP10UErrorCode
	movq	-96(%rbp), %rax
	movq	%rax, (%r12)
	movq	-80(%rbp), %rax
	movq	%rax, (%rbx)
	.p2align 4,,10
	.p2align 3
.L391:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L461
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L397:
	.cfi_restore_state
	movl	$1, (%rsi)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r13, -120(%rbp)
	movq	%r9, %r11
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L401:
	movzbl	104(%rdi,%rcx), %r13d
	movl	%ecx, %r15d
	cmpq	%rax, %rdx
	je	.L462
	addq	$1, %rax
	movb	%r13b, -1(%rax)
	testq	%r11, %r11
	je	.L408
	movl	$-1, (%r11)
	addq	$4, %r11
.L408:
	addq	$1, %rcx
	cmpl	%ecx, %r14d
	jg	.L401
	testq	%r11, %r11
	movb	$0, 91(%rdi)
	movq	-120(%rbp), %r13
	movq	%rax, (%rbx)
	cmovne	%r11, %r9
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L462:
	movslq	%ecx, %rax
	leaq	104(%rdi), %r10
	leaq	120(%rdi,%rax), %r9
	leaq	104(%rdi,%rax), %r8
	cmpq	%r9, %r10
	leaq	120(%rdi), %r9
	setnb	%r10b
	cmpq	%r9, %r8
	setnb	%r9b
	orb	%r9b, %r10b
	je	.L412
	leal	-1(%r14), %r9d
	subl	%ecx, %r9d
	cmpl	$14, %r9d
	seta	%r10b
	cmpl	%ecx, %r14d
	setg	%r9b
	testb	%r9b, %r10b
	je	.L412
	movl	%r14d, %r9d
	movl	$1, %eax
	movdqu	(%r8), %xmm4
	subl	%ecx, %r9d
	cmpl	%ecx, %r14d
	cmovle	%eax, %r9d
	movups	%xmm4, 104(%rdi)
	movl	%r9d, %eax
	shrl	$4, %eax
	cmpl	$1, %eax
	je	.L405
	movdqu	16(%r8), %xmm5
	movups	%xmm5, 120(%rdi)
	cmpl	$2, %eax
	je	.L405
	movdqu	32(%r8), %xmm6
	movups	%xmm6, 136(%rdi)
	cmpl	$3, %eax
	je	.L405
	movdqu	48(%r8), %xmm7
	movups	%xmm7, 152(%rdi)
	cmpl	$4, %eax
	je	.L405
	movdqu	64(%r8), %xmm7
	movups	%xmm7, 168(%rdi)
	cmpl	$5, %eax
	je	.L405
	movdqu	80(%r8), %xmm6
	movups	%xmm6, 184(%rdi)
	cmpl	$6, %eax
	je	.L405
	movdqu	96(%r8), %xmm5
	movups	%xmm5, 200(%rdi)
.L405:
	movl	%r9d, %eax
	andl	$-16, %eax
	leal	(%r15,%rax), %r8d
	cmpl	%eax, %r9d
	je	.L407
	movslq	%r8d, %r10
	leal	1(%r8), %r9d
	leal	1(%rax), %r12d
	movzbl	104(%rdi,%r10), %r11d
	movslq	%eax, %r10
	movb	%r11b, 104(%rdi,%r10)
	cmpl	%r9d, %r14d
	jle	.L407
	movslq	%r9d, %r9
	leal	2(%r8), %r10d
	leal	2(%rax), %r11d
	movslq	%r12d, %r12
	movzbl	104(%rdi,%r9), %r9d
	movb	%r9b, 104(%rdi,%r12)
	cmpl	%r10d, %r14d
	jle	.L407
	movslq	%r10d, %r10
	leal	3(%r8), %r9d
	leal	3(%rax), %r12d
	movslq	%r11d, %r11
	movzbl	104(%rdi,%r10), %r10d
	movb	%r10b, 104(%rdi,%r11)
	cmpl	%r9d, %r14d
	jle	.L407
	movslq	%r9d, %r9
	leal	4(%r8), %r10d
	leal	4(%rax), %r11d
	movslq	%r12d, %r12
	movzbl	104(%rdi,%r9), %r9d
	movb	%r9b, 104(%rdi,%r12)
	cmpl	%r10d, %r14d
	jle	.L407
	movslq	%r10d, %r10
	leal	5(%r8), %r9d
	leal	5(%rax), %r12d
	movslq	%r11d, %r11
	movzbl	104(%rdi,%r10), %r10d
	movb	%r10b, 104(%rdi,%r11)
	cmpl	%r9d, %r14d
	jle	.L407
	movslq	%r9d, %r9
	leal	6(%r8), %r10d
	leal	6(%rax), %r11d
	movslq	%r12d, %r12
	movzbl	104(%rdi,%r9), %r9d
	movb	%r9b, 104(%rdi,%r12)
	cmpl	%r10d, %r14d
	jle	.L407
	movslq	%r10d, %r10
	leal	7(%r8), %r9d
	leal	7(%rax), %r12d
	movslq	%r11d, %r11
	movzbl	104(%rdi,%r10), %r10d
	movb	%r10b, 104(%rdi,%r11)
	cmpl	%r9d, %r14d
	jle	.L407
	movslq	%r9d, %r9
	leal	8(%r8), %r10d
	leal	8(%rax), %r11d
	movslq	%r12d, %r12
	movzbl	104(%rdi,%r9), %r9d
	movb	%r9b, 104(%rdi,%r12)
	cmpl	%r10d, %r14d
	jle	.L407
	movslq	%r10d, %r10
	leal	9(%r8), %r9d
	leal	9(%rax), %r12d
	movslq	%r11d, %r11
	movzbl	104(%rdi,%r10), %r10d
	movb	%r10b, 104(%rdi,%r11)
	cmpl	%r9d, %r14d
	jle	.L407
	movslq	%r9d, %r9
	leal	10(%r8), %r10d
	leal	10(%rax), %r11d
	movslq	%r12d, %r12
	movzbl	104(%rdi,%r9), %r9d
	movb	%r9b, 104(%rdi,%r12)
	cmpl	%r10d, %r14d
	jle	.L407
	movslq	%r10d, %r10
	leal	11(%r8), %r9d
	leal	11(%rax), %r12d
	movslq	%r11d, %r11
	movzbl	104(%rdi,%r10), %r10d
	movb	%r10b, 104(%rdi,%r11)
	cmpl	%r9d, %r14d
	jle	.L407
	movslq	%r9d, %r9
	leal	12(%r8), %r10d
	leal	12(%rax), %r11d
	movslq	%r12d, %r12
	movzbl	104(%rdi,%r9), %r9d
	movb	%r9b, 104(%rdi,%r12)
	cmpl	%r10d, %r14d
	jle	.L407
	movslq	%r10d, %r10
	leal	13(%r8), %r9d
	leal	13(%rax), %r12d
	movslq	%r11d, %r11
	movzbl	104(%rdi,%r10), %r10d
	movb	%r10b, 104(%rdi,%r11)
	cmpl	%r9d, %r14d
	jle	.L407
	movslq	%r9d, %r9
	movslq	%r12d, %r12
	addl	$14, %r8d
	addl	$14, %eax
	movzbl	104(%rdi,%r9), %r9d
	movb	%r9b, 104(%rdi,%r12)
	cmpl	%r8d, %r14d
	jle	.L407
	movslq	%r8d, %r8
	cltq
	movzbl	104(%rdi,%r8), %r8d
	movb	%r8b, 104(%rdi,%rax)
	.p2align 4,,10
	.p2align 3
.L407:
	movl	%r14d, %eax
	subl	%ecx, %eax
	cmpl	%r15d, %r14d
	movl	$1, %ecx
	cmovle	%ecx, %eax
	movb	%al, 91(%rdi)
	movq	%rdx, (%rbx)
	movl	$15, (%rsi)
	jmp	.L391
.L412:
	movq	%rdi, %r9
	subq	%rax, %r9
	.p2align 4,,10
	.p2align 3
.L404:
	movzbl	104(%rdi,%rax), %r8d
	movb	%r8b, 104(%r9,%rax)
	addq	$1, %rax
	cmpl	%eax, %r14d
	jg	.L404
	jmp	.L407
.L461:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2143:
	.size	ucnv_fromUnicode_67, .-ucnv_fromUnicode_67
	.p2align 4
	.globl	ucnv_setSubstString_67
	.type	ucnv_setSubstString_67, @function
ucnv_setSubstString_67:
.LFB2126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	-2112(%rbp), %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	leaq	-2164(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1024, -2164(%rbp)
	call	ucnv_safeClone_67
	movl	(%r15), %r8d
	movq	%rax, %r11
	testl	%r8d, %r8d
	jg	.L464
	movq	UCNV_FROM_U_CALLBACK_STOP_67@GOTPCREL(%rip), %rax
	movq	$0, 24(%r11)
	movq	%rax, (%r11)
	leaq	-2144(%rbp), %rax
	movq	%rax, -2160(%rbp)
	movq	%r13, -2152(%rbp)
	cmpl	$-1, %r12d
	jge	.L501
.L465:
	movl	$1, (%r15)
	xorl	%r14d, %r14d
.L464:
	movq	%r11, %rdi
	call	ucnv_close_67
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L463
	movq	48(%rbx), %rax
	movq	32(%rax), %rdx
	cmpq	$0, 104(%rdx)
	je	.L491
	movq	16(%rax), %rax
	cmpb	$2, 69(%rax)
	je	.L473
.L476:
	cmpl	$32, %r12d
	jg	.L502
	testl	%r12d, %r12d
	js	.L503
.L477:
	leal	(%r12,%r12), %r14d
	cmpl	$4, %r14d
	jg	.L504
.L478:
	testl	%r14d, %r14d
	jne	.L505
	movb	$0, 89(%rbx)
.L482:
	movb	$0, 94(%rbx)
.L463:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L506
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	ucnv_MBCSGetType_67@PLT
	cmpl	$9, %eax
	je	.L476
.L491:
	leaq	-2144(%rbp), %r13
	cmpl	$4, %r14d
	jle	.L478
.L504:
	movq	40(%rbx), %rdi
	leaq	136(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L507
.L479:
	movslq	%r14d, %rdx
	movq	%r13, %rsi
	negl	%r12d
	call	memcpy@PLT
	leaq	-2144(%rbp), %rax
	cmpq	%rax, %r13
	cmove	%r14d, %r12d
	movb	%r12b, 89(%rbx)
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L501:
	testq	%r13, %r13
	jne	.L466
	testl	%r12d, %r12d
	jne	.L465
.L466:
	movq	%r11, %rdi
	movl	$1, %edx
	movl	$2, %esi
	movq	%r11, -2192(%rbp)
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movq	-2160(%rbp), %rax
	cmpl	$-1, %r12d
	movq	-2192(%rbp), %r11
	movq	%rax, -2184(%rbp)
	movl	%r12d, %eax
	je	.L508
.L467:
	testl	%eax, %eax
	jle	.L488
	movq	-2152(%rbp), %rdx
	cltq
	leaq	(%rdx,%rax,2), %r14
	movq	-2160(%rbp), %rdx
	cmpq	$-2147483648, %rdx
	ja	.L509
	movl	$32, -2212(%rbp)
	movl	$32, %eax
.L469:
	pushq	%r15
	addq	%rax, %rdx
	leaq	-2160(%rbp), %rax
	movq	%r11, %rdi
	pushq	$1
	movq	%rax, %rsi
	leaq	-2152(%rbp), %rcx
	xorl	%r9d, %r9d
	movq	%r14, %r8
	movq	%rax, -2192(%rbp)
	movq	%r11, -2208(%rbp)
	movq	%rcx, -2200(%rbp)
	call	ucnv_fromUnicode_67
	movl	-2160(%rbp), %eax
	popq	%rsi
	subl	-2184(%rbp), %eax
	cmpl	$15, (%r15)
	movq	-2208(%rbp), %r11
	popq	%rdi
	jne	.L468
	leaq	-64(%rbp), %rsi
	leaq	-1088(%rbp), %rcx
	movq	%rbx, -2224(%rbp)
	movq	%rsi, -2208(%rbp)
	movq	%rcx, %rbx
	movq	%r13, -2232(%rbp)
	movq	%r14, %r13
	movl	%eax, %r14d
	movl	%r12d, -2216(%rbp)
	movq	%r11, %r12
	.p2align 4,,10
	.p2align 3
.L470:
	pushq	%r15
	movq	-2200(%rbp), %rcx
	xorl	%r9d, %r9d
	movq	%r13, %r8
	pushq	$1
	movq	-2208(%rbp), %rdx
	movq	%r12, %rdi
	movl	$0, (%r15)
	movq	-2192(%rbp), %rsi
	movq	%rbx, -2160(%rbp)
	call	ucnv_fromUnicode_67
	movl	-2160(%rbp), %eax
	popq	%rdx
	popq	%rcx
	addl	%r14d, %eax
	subl	%ebx, %eax
	cmpl	$15, (%r15)
	movl	%eax, %r14d
	je	.L470
	movq	%r12, %r11
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r13
	movl	-2216(%rbp), %r12d
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L503:
	movq	%r13, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %r12d
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L502:
	movl	$15, (%r15)
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L488:
	movl	$32, -2212(%rbp)
	xorl	%eax, %eax
.L468:
	movl	-2212(%rbp), %esi
	movq	-2184(%rbp), %rdi
	movq	%r15, %rcx
	movl	%eax, %edx
	movq	%r11, -2192(%rbp)
	call	u_terminateChars_67@PLT
	movq	-2192(%rbp), %r11
	movl	%eax, %r14d
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L507:
	movl	$64, %edi
	movq	%rdx, -2184(%rbp)
	call	uprv_malloc_67@PLT
	movq	-2184(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, 40(%rbx)
	movq	%rax, %rdi
	je	.L510
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L509:
	movq	%rdx, %rax
	movl	$32, %ecx
	notq	%rax
	cmpl	$32, %eax
	cmovle	%eax, %ecx
	movl	%ecx, -2212(%rbp)
	movslq	%ecx, %rax
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L508:
	movq	-2152(%rbp), %rdi
	call	u_strlen_67@PLT
	movq	-2192(%rbp), %r11
	jmp	.L467
.L506:
	call	__stack_chk_fail@PLT
.L510:
	movq	%rdx, 40(%rbx)
	movl	$7, (%r15)
	jmp	.L463
.L505:
	movq	40(%rbx), %rdi
	jmp	.L479
	.cfi_endproc
.LFE2126:
	.size	ucnv_setSubstString_67, .-ucnv_setSubstString_67
	.p2align 4
	.globl	ucnv_toUnicode_67
	.type	ucnv_toUnicode_67, @function
ucnv_toUnicode_67:
.LFB2146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	24(%rbp), %rsi
	movl	16(%rbp), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L511
	movl	(%rsi), %r10d
	testl	%r10d, %r10d
	jg	.L511
	testq	%rbx, %rbx
	sete	%r10b
	testq	%r12, %r12
	sete	%al
	orb	%al, %r10b
	jne	.L517
	testq	%rdi, %rdi
	je	.L517
	cmpq	$-2147483648, %rdx
	movq	$-2147483648, %r10
	movq	(%r12), %r15
	movq	(%rbx), %rax
	cmovbe	%rdx, %r10
	leaq	2147483647(%r10), %r11
	addq	$2147483646, %r10
	cmpq	%r11, %rdx
	cmove	%r10, %rdx
	cmpq	%r8, %r15
	ja	.L517
	cmpq	%rax, %rdx
	jb	.L517
	movq	%r8, %r10
	subq	%r15, %r10
	cmpq	$2147483647, %r10
	jbe	.L533
	cmpq	%r8, %r15
	jb	.L517
.L533:
	movq	%rdx, %r10
	subq	%rax, %r10
	cmpq	$2147483647, %r10
	jbe	.L534
	cmpq	%rax, %rdx
	ja	.L517
.L534:
	andl	$1, %r10d
	jne	.L517
	movzbl	93(%rdi), %r10d
	testb	%r10b, %r10b
	jg	.L604
.L520:
	testb	%cl, %cl
	jne	.L530
	cmpq	%r8, %r15
	jne	.L530
	cmpb	$0, 282(%rdi)
	jns	.L511
.L530:
	movq	%rdi, %xmm0
	movq	%r15, %xmm1
	leaq	-112(%rbp), %rdi
	movb	%cl, -110(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	movq	%r9, %xmm3
	movl	$56, %eax
	movups	%xmm0, -104(%rbp)
	movq	%r8, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movw	%ax, -112(%rbp)
	movups	%xmm0, -88(%rbp)
	movq	%rdx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -72(%rbp)
	call	_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode
	movq	-96(%rbp), %rax
	movq	%rax, (%r12)
	movq	-80(%rbp), %rax
	movq	%rax, (%rbx)
	.p2align 4,,10
	.p2align 3
.L511:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L605
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	movl	$1, (%rsi)
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L604:
	movsbl	%r10b, %r14d
	xorl	%r11d, %r11d
	movq	%r9, %r10
	movq	%r12, -144(%rbp)
	leaq	144(%rdi), %r13
	movl	%r14d, -116(%rbp)
	movq	%r13, -128(%rbp)
	movq	%r15, -136(%rbp)
	movl	%r14d, %r15d
	.p2align 4,,10
	.p2align 3
.L521:
	movl	%r11d, %r14d
	movzwl	0(%r13), %r12d
	leal	1(%r11), %r11d
	cmpq	%rax, %rdx
	je	.L606
	addq	$2, %rax
	movw	%r12w, -2(%rax)
	testq	%r10, %r10
	je	.L529
	movl	$-1, (%r10)
	addq	$4, %r10
.L529:
	addq	$2, %r13
	cmpl	%r11d, %r15d
	jne	.L521
	testq	%r10, %r10
	movb	$0, 93(%rdi)
	movq	-136(%rbp), %r15
	movq	%rax, (%rbx)
	movq	-144(%rbp), %r12
	cmovne	%r10, %r9
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L606:
	movslq	%r14d, %rdx
	leaq	160(%rdi), %r9
	leaq	160(%rdx,%rdx), %r8
	leaq	-16(%rdi,%r8), %rcx
	cmpq	%r9, %rcx
	setnb	%r9b
	addq	%rdi, %r8
	cmpq	%r8, -128(%rbp)
	setnb	%r8b
	orb	%r8b, %r9b
	je	.L524
	movl	-116(%rbp), %r11d
	leal	-1(%r11), %r8d
	subl	%r14d, %r8d
	cmpl	$6, %r8d
	seta	%r9b
	cmpl	%r14d, %r11d
	setg	%r8b
	testb	%r8b, %r9b
	je	.L524
	movl	%r11d, %r8d
	movl	$1, %edx
	movdqu	(%rcx), %xmm4
	subl	%r14d, %r8d
	cmpl	%r14d, %r11d
	cmovle	%edx, %r8d
	movups	%xmm4, 144(%rdi)
	movl	%r8d, %edx
	shrl	$3, %edx
	cmpl	$1, %edx
	je	.L525
	movdqu	16(%rcx), %xmm5
	movups	%xmm5, 160(%rdi)
	cmpl	$2, %edx
	je	.L525
	movdqu	32(%rcx), %xmm6
	movups	%xmm6, 176(%rdi)
	cmpl	$3, %edx
	je	.L525
	movdqu	48(%rcx), %xmm7
	movups	%xmm7, 192(%rdi)
	cmpl	$4, %edx
	je	.L525
	movdqu	64(%rcx), %xmm7
	movups	%xmm7, 208(%rdi)
	cmpl	$5, %edx
	je	.L525
	movdqu	80(%rcx), %xmm6
	movups	%xmm6, 224(%rdi)
	cmpl	$6, %edx
	je	.L525
	movdqu	96(%rcx), %xmm5
	movups	%xmm5, 240(%rdi)
	cmpl	$7, %edx
	je	.L525
	movdqu	112(%rcx), %xmm4
	movups	%xmm4, 256(%rdi)
	cmpl	$8, %edx
	je	.L525
	movdqu	128(%rcx), %xmm6
	movups	%xmm6, 272(%rdi)
	cmpl	$9, %edx
	je	.L525
	movdqu	144(%rcx), %xmm7
	movups	%xmm7, 288(%rdi)
	cmpl	$10, %edx
	je	.L525
	movdqu	160(%rcx), %xmm5
	movups	%xmm5, 304(%rdi)
	cmpl	$11, %edx
	je	.L525
	movdqu	176(%rcx), %xmm6
	movups	%xmm6, 320(%rdi)
	cmpl	$12, %edx
	je	.L525
	movdqu	192(%rcx), %xmm7
	movups	%xmm7, 336(%rdi)
	cmpl	$13, %edx
	je	.L525
	movdqu	208(%rcx), %xmm4
	movups	%xmm4, 352(%rdi)
	cmpl	$14, %edx
	je	.L525
	movdqu	224(%rcx), %xmm6
	movups	%xmm6, 368(%rdi)
	.p2align 4,,10
	.p2align 3
.L525:
	movl	%r8d, %edx
	andl	$-8, %edx
	leal	(%r14,%rdx), %ecx
	cmpl	%edx, %r8d
	je	.L527
	movq	-128(%rbp), %r15
	movslq	%ecx, %r9
	movl	-116(%rbp), %r13d
	leal	1(%rcx), %r8d
	leal	1(%rdx), %r11d
	movzwl	(%r15,%r9,2), %r10d
	movslq	%edx, %r9
	movw	%r10w, (%r15,%r9,2)
	cmpl	%r8d, %r13d
	jle	.L527
	movslq	%r8d, %r8
	leal	2(%rcx), %r9d
	leal	2(%rdx), %r10d
	movslq	%r11d, %r11
	movzwl	(%r15,%r8,2), %r8d
	movw	%r8w, (%r15,%r11,2)
	cmpl	%r9d, %r13d
	jle	.L527
	movslq	%r9d, %r9
	leal	3(%rcx), %r8d
	leal	3(%rdx), %r11d
	movslq	%r10d, %r10
	movzwl	(%r15,%r9,2), %r9d
	movw	%r9w, (%r15,%r10,2)
	cmpl	%r8d, %r13d
	jle	.L527
	movslq	%r8d, %r8
	leal	4(%rcx), %r9d
	leal	4(%rdx), %r10d
	movslq	%r11d, %r11
	movzwl	(%r15,%r8,2), %r8d
	movw	%r8w, (%r15,%r11,2)
	cmpl	%r9d, %r13d
	jle	.L527
	movslq	%r9d, %r9
	movslq	%r10d, %r10
	leal	5(%rcx), %r8d
	addl	$5, %edx
	movzwl	(%r15,%r9,2), %r9d
	movw	%r9w, (%r15,%r10,2)
	cmpl	%r8d, %r13d
	jle	.L527
	movslq	%r8d, %r8
	movslq	%edx, %rdx
	addl	$6, %ecx
	movzwl	(%r15,%r8,2), %r8d
	addq	%rdx, %rdx
	movw	%r8w, 144(%rdi,%rdx)
	cmpl	%ecx, %r13d
	jle	.L527
	movslq	%ecx, %rcx
	movzwl	(%r15,%rcx,2), %ecx
	movw	%cx, 2(%r15,%rdx)
.L527:
	movl	-116(%rbp), %ecx
	movl	%ecx, %edx
	subl	%r14d, %edx
	cmpl	%r14d, %ecx
	movl	$1, %ecx
	cmovle	%ecx, %edx
	movb	%dl, 93(%rdi)
	movq	%rax, (%rbx)
	movl	$15, (%rsi)
	jmp	.L511
.L524:
	movq	%rdx, %rcx
	movl	-116(%rbp), %r9d
	negq	%rcx
	leaq	(%rdi,%rcx,2), %r8
	.p2align 4,,10
	.p2align 3
.L528:
	movzwl	144(%rdi,%rdx,2), %ecx
	movw	%cx, 144(%r8,%rdx,2)
	addq	$1, %rdx
	cmpl	%edx, %r9d
	jg	.L528
	jmp	.L527
.L605:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2146:
	.size	ucnv_toUnicode_67, .-ucnv_toUnicode_67
	.p2align 4
	.globl	ucnv_fromUChars_67
	.type	ucnv_fromUChars_67, @function
ucnv_fromUChars_67:
.LFB2147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1160, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -1160(%rbp)
	movq	%rcx, -1168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L624
	movq	%r9, %r12
	movl	(%r9), %r9d
	testl	%r9d, %r9d
	jg	.L607
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L609
	movl	%edx, %r13d
	testl	%edx, %edx
	js	.L609
	testq	%rsi, %rsi
	sete	%dl
	testl	%r13d, %r13d
	setg	%al
	testb	%al, %dl
	jne	.L609
	cmpl	$-1, %r8d
	jl	.L609
	testl	%r8d, %r8d
	je	.L610
	testq	%rcx, %rcx
	je	.L609
	movl	$1, %edx
	movl	$2, %esi
	movl	%r8d, -1176(%rbp)
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movslq	-1176(%rbp), %r8
	movq	-1160(%rbp), %r15
	cmpl	$-1, %r8d
	je	.L638
.L612:
	testl	%r8d, %r8d
	jle	.L626
	movq	-1168(%rbp), %rdx
	leaq	(%rdx,%r8,2), %rbx
	movq	-1160(%rbp), %rdx
	testl	%r13d, %r13d
	je	.L614
	cmpq	$-2147483648, %rdx
	ja	.L639
.L615:
	movslq	%r13d, %rax
	addq	%rax, %rdx
.L614:
	pushq	%r12
	leaq	-1168(%rbp), %rcx
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	pushq	$1
	leaq	-1160(%rbp), %rsi
	movq	%r14, %rdi
	call	ucnv_fromUnicode_67
	movl	-1160(%rbp), %r8d
	popq	%rdx
	popq	%rcx
	subl	%r15d, %r8d
	cmpl	$15, (%r12)
	je	.L640
	.p2align 4,,10
	.p2align 3
.L613:
	movq	%r12, %rcx
	movl	%r8d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	u_terminateChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L607:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L641
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L624:
	xorl	%eax, %eax
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L639:
	movq	%rdx, %rax
	notq	%rax
	cmpl	%eax, %r13d
	cmovg	%eax, %r13d
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L610:
	movl	$1, %edx
	movl	$2, %esi
	movl	%r8d, -1176(%rbp)
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movq	-1160(%rbp), %r15
	movl	-1176(%rbp), %r8d
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L638:
	movq	-1168(%rbp), %rdi
	call	u_strlen_67@PLT
	movslq	%eax, %r8
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L640:
	cmpq	$-2147483648, %rbx
	movq	$-2147483648, %rax
	leaq	-1(%rbx), %rdx
	cmovbe	%rbx, %rax
	leaq	-64(%rbp), %r9
	addq	$2147483647, %rax
	cmpq	%rax, %rbx
	movq	-1168(%rbp), %rax
	cmove	%rdx, %rbx
	leaq	-1088(%rbp), %rdx
.L622:
	movq	%rdx, -1160(%rbp)
	movl	$0, (%r12)
	cmpq	%rax, %rbx
	jb	.L616
	movq	%rbx, %rcx
	subq	%rax, %rcx
	cmpq	$2147483647, %rcx
	jbe	.L628
	cmpq	%rax, %rbx
	ja	.L616
.L628:
	andl	$1, %ecx
	jne	.L616
	movsbl	91(%r14), %ecx
	movq	%rdx, %r10
	testb	%cl, %cl
	jg	.L642
.L620:
	movq	%rax, %xmm1
	movq	%r14, %xmm0
	movq	%r10, %xmm2
	movq	%r12, %rsi
	punpcklqdq	%xmm1, %xmm0
	movl	$56, %eax
	leaq	-1152(%rbp), %rdi
	movq	%rdx, -1192(%rbp)
	movups	%xmm0, -1144(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movl	%r8d, -1180(%rbp)
	movq	%r9, -1112(%rbp)
	movq	%r9, -1176(%rbp)
	movw	%ax, -1152(%rbp)
	movb	$1, -1150(%rbp)
	movq	$0, -1104(%rbp)
	movups	%xmm0, -1128(%rbp)
	call	_ZL24_fromUnicodeWithCallbackP25UConverterFromUnicodeArgsP10UErrorCode
	movq	-1120(%rbp), %rcx
	movq	-1192(%rbp), %rdx
	movl	-1180(%rbp), %r8d
	movq	-1136(%rbp), %rax
	movq	%rcx, -1160(%rbp)
	subq	%rdx, %rcx
	movq	-1176(%rbp), %r9
	addl	%ecx, %r8d
	cmpl	$15, (%r12)
	movq	%rax, -1168(%rbp)
	je	.L622
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L616:
	movl	$1, (%r12)
	jmp	.L613
.L642:
	subl	$1, %ecx
	leaq	104(%r14), %rsi
	leaq	1(%rdx,%rcx), %r10
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L621:
	movzbl	(%rsi), %edi
	addq	$1, %rcx
	addq	$1, %rsi
	movb	%dil, -1(%rcx)
	cmpq	%r10, %rcx
	jne	.L621
	movb	$0, 91(%r14)
	movq	%r10, -1160(%rbp)
	jmp	.L620
.L626:
	xorl	%r8d, %r8d
	jmp	.L613
.L641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2147:
	.size	ucnv_fromUChars_67, .-ucnv_fromUChars_67
	.p2align 4
	.globl	ucnv_toUChars_67
	.type	ucnv_toUChars_67, @function
ucnv_toUChars_67:
.LFB2148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -2184(%rbp)
	movq	%rcx, -2192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L667
	movq	%r9, %r12
	movl	(%r9), %r9d
	testl	%r9d, %r9d
	jg	.L643
	movq	%rdi, %r15
	testq	%rdi, %rdi
	je	.L645
	movl	%edx, %r14d
	testl	%edx, %edx
	js	.L645
	testq	%rsi, %rsi
	sete	%dl
	testl	%r14d, %r14d
	setg	%al
	testb	%al, %dl
	jne	.L645
	movslq	%r8d, %r13
	cmpl	$-1, %r13d
	jl	.L645
	testl	%r13d, %r13d
	je	.L646
	testq	%rcx, %rcx
	je	.L645
	movl	$1, %edx
	movl	$1, %esi
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movq	-2184(%rbp), %rax
	movq	%rax, -2200(%rbp)
	cmpl	$-1, %r13d
	jne	.L648
	movq	-2192(%rbp), %rdi
	call	strlen@PLT
	movslq	%eax, %r13
.L648:
	testl	%r13d, %r13d
	jle	.L669
	movq	-2192(%rbp), %rbx
	movq	-2200(%rbp), %rcx
	addq	%r13, %rbx
	movq	%rcx, %rdx
	testl	%r14d, %r14d
	je	.L650
	movq	%rcx, %rax
	notq	%rax
	shrq	%rax
	cmpq	$-2147483648, %rcx
	movq	%rax, %rdx
	movl	$1073741823, %eax
	cmova	%edx, %eax
	cmpl	%eax, %r14d
	cmovg	%eax, %r14d
	movslq	%r14d, %rax
	leaq	(%rcx,%rax,2), %rdx
.L650:
	pushq	%r12
	leaq	-2192(%rbp), %rcx
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	pushq	$1
	leaq	-2184(%rbp), %rsi
	movq	%r15, %rdi
	call	ucnv_toUnicode_67
	movq	-2184(%rbp), %r13
	popq	%rdx
	subq	-2200(%rbp), %r13
	popq	%rcx
	sarq	%r13
	cmpl	$15, (%r12)
	je	.L728
.L649:
	movq	-2200(%rbp), %rdi
	movq	%r12, %rcx
	movl	%r13d, %edx
	movl	%r14d, %esi
	call	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L643:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L729
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L667:
	xorl	%eax, %eax
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L646:
	movl	$1, %edx
	movl	$1, %esi
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movq	-2184(%rbp), %rax
	movq	%rax, -2200(%rbp)
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L728:
	leaq	-64(%rbp), %rdx
	movq	$-2147483648, %rax
	leaq	-65(%rbp), %rcx
	movq	-2192(%rbp), %r11
	cmpq	$-2147483648, %rdx
	leaq	-2112(%rbp), %r9
	movl	%r14d, -2212(%rbp)
	movq	%r15, %r14
	cmovbe	%rdx, %rax
	movq	%r9, %r15
	addq	$2147483647, %rax
	cmpq	%rax, %rdx
	cmove	%rcx, %rdx
	movq	%rdx, %rax
	movq	%rdx, -2208(%rbp)
	subq	%r9, %rax
	movq	%rax, %rsi
	andl	$1, %eax
	sarq	%rsi
	movq	%rax, -2232(%rbp)
	movq	%rsi, -2224(%rbp)
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%r15, -2184(%rbp)
	movl	$0, (%r12)
	cmpq	%r11, %rbx
	jb	.L652
	movq	%rbx, %rax
	movl	$2147483648, %esi
	subq	%r11, %rax
	cmpq	%rsi, %rax
	jb	.L674
	cmpq	%r11, %rbx
	ja	.L652
.L674:
	cmpq	$1073741823, -2224(%rbp)
	ja	.L652
	cmpq	$0, -2232(%rbp)
	jne	.L652
	movsbl	93(%r14), %ecx
	movq	%r15, %rax
	testb	%cl, %cl
	jg	.L730
.L656:
	movq	%r11, %xmm1
	movq	%r14, %xmm0
	movq	%rax, %xmm2
	movq	%r12, %rsi
	movq	-2208(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	leaq	-2176(%rbp), %rdi
	movb	$1, -2174(%rbp)
	movups	%xmm0, -2168(%rbp)
	movq	%rbx, %xmm0
	movq	%rax, -2136(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movl	$56, %eax
	movw	%ax, -2176(%rbp)
	movq	$0, -2128(%rbp)
	movups	%xmm0, -2152(%rbp)
	call	_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode
	movq	-2144(%rbp), %rax
	movq	-2160(%rbp), %r11
	movq	%rax, -2184(%rbp)
	subq	%r15, %rax
	sarq	%rax
	movq	%r11, -2192(%rbp)
	addl	%eax, %r13d
	cmpl	$15, (%r12)
	je	.L665
	movl	-2212(%rbp), %r14d
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L652:
	movl	$1, (%r12)
	movl	-2212(%rbp), %r14d
	jmp	.L649
.L730:
	movq	-2208(%rbp), %r9
	leaq	144(%r14), %r8
	movl	$1, %edi
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L664:
	leaq	1(%rdi), %rdx
	cmpq	%rax, %r9
	je	.L731
	movq	%rdx, %rdi
.L657:
	movzwl	142(%r14,%rdi,2), %edx
	addq	$2, %rax
	movl	%edi, %esi
	movw	%dx, -2(%rax)
	cmpl	%edi, %ecx
	jg	.L664
	movb	$0, 93(%r14)
	movq	%rax, -2184(%rbp)
	jmp	.L656
.L669:
	xorl	%r13d, %r13d
	jmp	.L649
.L731:
	movl	%ecx, %edx
	subl	%edi, %edx
	movslq	%esi, %rdi
	leaq	160(%rdi,%rdi), %r9
	movl	%edx, -2216(%rbp)
	leaq	-16(%r14,%r9), %r10
	addq	%r14, %r9
	cmpq	%r9, %r8
	leaq	160(%r14), %r9
	setnb	-2240(%rbp)
	cmpq	%r9, %r10
	movzbl	-2240(%rbp), %edx
	setnb	%r9b
	orb	%r9b, %dl
	je	.L658
	movl	-2216(%rbp), %edx
	leal	-1(%rdx), %r9d
	cmpl	$6, %r9d
	jbe	.L658
	cmpl	%esi, %ecx
	movl	$1, %r9d
	movdqu	(%r10), %xmm3
	cmovg	%edx, %r9d
	movups	%xmm3, 144(%r14)
	movl	%r9d, %edi
	shrl	$3, %edi
	cmpl	$1, %edi
	je	.L659
	movdqu	16(%r10), %xmm4
	movups	%xmm4, 160(%r14)
	cmpl	$2, %edi
	je	.L659
	movdqu	32(%r10), %xmm5
	movups	%xmm5, 176(%r14)
	cmpl	$3, %edi
	je	.L659
	movdqu	48(%r10), %xmm6
	movups	%xmm6, 192(%r14)
	cmpl	$4, %edi
	je	.L659
	movdqu	64(%r10), %xmm7
	movups	%xmm7, 208(%r14)
	cmpl	$5, %edi
	je	.L659
	movdqu	80(%r10), %xmm7
	movups	%xmm7, 224(%r14)
	cmpl	$6, %edi
	je	.L659
	movdqu	96(%r10), %xmm5
	movups	%xmm5, 240(%r14)
	cmpl	$7, %edi
	je	.L659
	movdqu	112(%r10), %xmm6
	movups	%xmm6, 256(%r14)
	cmpl	$8, %edi
	je	.L659
	movdqu	128(%r10), %xmm7
	movups	%xmm7, 272(%r14)
	cmpl	$9, %edi
	je	.L659
	movdqu	144(%r10), %xmm3
	movups	%xmm3, 288(%r14)
	cmpl	$10, %edi
	je	.L659
	movdqu	160(%r10), %xmm4
	movups	%xmm4, 304(%r14)
	cmpl	$11, %edi
	je	.L659
	movdqu	176(%r10), %xmm5
	movups	%xmm5, 320(%r14)
	cmpl	$12, %edi
	je	.L659
	movdqu	192(%r10), %xmm6
	movups	%xmm6, 336(%r14)
	cmpl	$13, %edi
	je	.L659
	movdqu	208(%r10), %xmm7
	movups	%xmm7, 352(%r14)
	cmpl	$14, %edi
	je	.L659
	movdqu	224(%r10), %xmm3
	movups	%xmm3, 368(%r14)
.L659:
	movl	%r9d, %edi
	andl	$-8, %edi
	leal	(%rsi,%rdi), %r10d
	cmpl	%r9d, %edi
	je	.L661
	movslq	%edi, %rdi
	leal	1(%r10), %edx
	leaq	(%rdi,%rdi), %r9
	movq	%r9, -2240(%rbp)
	movslq	%r10d, %r9
	movzwl	(%r8,%r9,2), %r9d
	movw	%r9w, (%r8,%rdi,2)
	cmpl	%edx, %ecx
	jle	.L661
	movslq	%edx, %r9
	movq	-2240(%rbp), %rdx
	leaq	(%r9,%r9), %rdi
	movzwl	(%r8,%r9,2), %r9d
	movw	%r9w, 2(%r8,%rdx)
	leal	2(%r10), %r9d
	cmpl	%r9d, %ecx
	jle	.L661
	movzwl	2(%r8,%rdi), %r9d
	movw	%r9w, 4(%r8,%rdx)
	leal	3(%r10), %r9d
	cmpl	%r9d, %ecx
	jle	.L661
	movzwl	4(%r8,%rdi), %r9d
	movw	%r9w, 6(%r8,%rdx)
	leal	4(%r10), %r9d
	cmpl	%r9d, %ecx
	jle	.L661
	movzwl	6(%r8,%rdi), %r9d
	movw	%r9w, 8(%r8,%rdx)
	leal	5(%r10), %r9d
	cmpl	%r9d, %ecx
	jle	.L661
	movzwl	8(%r8,%rdi), %r9d
	movw	%r9w, 10(%r8,%rdx)
	leal	6(%r10), %r9d
	cmpl	%r9d, %ecx
	jle	.L661
	movzwl	10(%r8,%rdi), %edi
	movw	%di, 12(%r8,%rdx)
.L661:
	cmpl	%esi, %ecx
	movl	$1, %ecx
	cmovg	-2216(%rbp), %ecx
	subq	%r15, %rax
	sarq	%rax
	movb	%cl, 93(%r14)
	addl	%eax, %r13d
	jmp	.L665
.L658:
	imulq	$-2, %rdi, %r9
	movl	-2216(%rbp), %edx
	addq	%r14, %r9
.L662:
	movzwl	144(%r14,%rdi,2), %r8d
	movw	%r8w, 144(%r9,%rdi,2)
	addq	$1, %rdi
	cmpl	%edi, %ecx
	jg	.L662
	movl	%edx, -2216(%rbp)
	jmp	.L661
.L729:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2148:
	.size	ucnv_toUChars_67, .-ucnv_toUChars_67
	.p2align 4
	.globl	ucnv_getNextUChar_67
	.type	ucnv_getNextUChar_67, @function
ucnv_getNextUChar_67:
.LFB2149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L763
	movl	(%rcx), %r8d
	movq	%rcx, %rbx
	movl	$65535, %r15d
	testl	%r8d, %r8d
	jg	.L732
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L736
	movq	%rsi, %r14
	testq	%rsi, %rsi
	je	.L736
	movq	(%rsi), %rcx
	movq	%rdx, %r13
	cmpq	%rdx, %rcx
	ja	.L736
	movq	%rdx, %rax
	subq	%rcx, %rax
	cmpq	$2147483647, %rax
	jbe	.L773
	cmpq	%rdx, %rcx
	jb	.L736
.L773:
	movzbl	93(%r12), %edx
	testb	%dl, %dl
	jle	.L738
	movzwl	144(%r12), %r15d
	movsbl	%dl, %r9d
	movl	%r15d, %r8d
	andl	$-1024, %r8d
	cmpb	$1, %dl
	je	.L765
	cmpl	$55296, %r8d
	je	.L801
.L765:
	movl	$2, %esi
	movl	$1, %eax
	movl	$1, %r10d
.L739:
	subl	%eax, %edx
	movb	%dl, 93(%r12)
	testb	%dl, %dl
	jg	.L761
.L741:
	cmpl	$55296, %r8d
	jne	.L732
	cmpl	%r9d, %r10d
	jl	.L732
	movq	%r12, %xmm0
	movq	%rcx, %xmm1
	movl	(%rbx), %esi
	leaq	-58(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movl	$56, %edx
	movb	$1, -126(%rbp)
	movl	%r15d, %r8d
	movq	$0, -80(%rbp)
	movq	%r13, -104(%rbp)
	movq	%rax, -88(%rbp)
	movw	%dx, -128(%rbp)
	movw	%r15w, -60(%rbp)
	movq	%rax, -96(%rbp)
	movups	%xmm0, -120(%rbp)
	testl	%esi, %esi
	jle	.L802
	movl	$1, %r9d
	movl	$1, %ebx
	movl	$1, %r11d
	movl	$65535, %r15d
.L756:
	movsbl	93(%r12), %eax
	movl	%eax, %r13d
	testl	%eax, %eax
	jle	.L757
	addl	%eax, %eax
	leaq	144(%r12), %rsi
	movl	%r9d, -156(%rbp)
	movslq	%eax, %rdx
	movslq	%ebx, %rax
	movb	%r11b, -152(%rbp)
	leaq	(%rsi,%rax,2), %rdi
	movq	%rcx, -144(%rbp)
	movl	%r8d, -136(%rbp)
	call	memmove@PLT
	movslq	-156(%rbp), %r9
	movzbl	-152(%rbp), %r11d
	movq	-144(%rbp), %rcx
	movl	-136(%rbp), %r8d
.L757:
	movw	%r8w, 144(%r12)
	addl	%r11d, %r13d
	movb	%r13b, 93(%r12)
	cmpl	$1, %ebx
	je	.L752
	movzwl	-60(%rbp,%r9,2), %eax
	movw	%ax, 146(%r12)
.L752:
	movq	%rcx, (%r14)
.L732:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L803
	addq	$120, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L736:
	.cfi_restore_state
	movl	$1, (%rbx)
	movl	$65535, %r15d
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L763:
	movl	$65535, %r15d
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L738:
	movq	%r12, %xmm0
	leaq	-60(%rbp), %rdx
	leaq	-58(%rbp), %rax
	movb	$1, -126(%rbp)
	movq	%rcx, %xmm2
	movl	$56, %edi
	movq	%r13, -104(%rbp)
	cmpb	$0, 64(%r12)
	punpcklqdq	%xmm2, %xmm0
	movq	$0, -80(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movw	%di, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	jne	.L800
	movq	48(%r12), %rax
	movq	32(%rax), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L800
	leaq	-128(%rbp), %rdi
	movq	%rdx, -144(%rbp)
	movq	%rbx, %rsi
	movq	%rdi, -136(%rbp)
	call	*%rax
	movq	-136(%rbp), %rdi
	movq	-144(%rbp), %rdx
	movl	%eax, %r15d
	movq	-112(%rbp), %rax
	movq	%rax, (%r14)
	movl	(%rbx), %eax
	cmpl	$8, %eax
	je	.L804
	testl	%eax, %eax
	jg	.L748
	testl	%r15d, %r15d
	jns	.L732
.L748:
	movq	%rbx, %rsi
	movq	%rdx, -136(%rbp)
	call	_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode
	movl	(%rbx), %eax
	movq	-136(%rbp), %rdx
	cmpl	$15, %eax
	je	.L805
	movq	-96(%rbp), %r8
	subq	%rdx, %r8
	sarq	%r8
	testl	%eax, %eax
	jle	.L749
	movq	-112(%rbp), %rcx
	movl	$1, %r9d
	xorl	%r10d, %r10d
	movl	$65535, %r15d
.L753:
	cmpl	%r8d, %r10d
	jge	.L752
	movl	%r8d, %ebx
	movzwl	-60(%rbp,%r10,2), %r8d
	subl	%r10d, %ebx
	movl	%ebx, %r11d
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L801:
	movzwl	146(%r12), %esi
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L806
	subl	$1, %edx
	movl	$55296, %r8d
	movl	$2, %esi
	movl	$1, %r10d
	movb	%dl, 93(%r12)
.L761:
	movsbl	%dl, %edx
	leaq	144(%r12), %rdi
	movl	%r9d, -156(%rbp)
	addl	%edx, %edx
	addq	%rdi, %rsi
	movq	%rcx, -152(%rbp)
	movslq	%edx, %rdx
	movl	%r10d, -144(%rbp)
	movl	%r8d, -136(%rbp)
	call	memmove@PLT
	movl	-156(%rbp), %r9d
	movq	-152(%rbp), %rcx
	movl	-144(%rbp), %r10d
	movl	-136(%rbp), %r8d
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L800:
	leaq	-128(%rbp), %rdi
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L802:
	movzwl	%r15w, %r15d
	movl	$1, %r8d
	movl	%r15d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L752
.L758:
	movzbl	93(%r12), %edx
	testb	%dl, %dl
	jle	.L754
	movzwl	144(%r12), %esi
	movl	$2, %r9d
	movl	$1, %r10d
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L753
	sall	$10, %r15d
	subl	$1, %edx
	movb	%dl, 93(%r12)
	leal	-56613888(%rsi,%r15), %r15d
	testb	%dl, %dl
	je	.L753
	addq	%rdx, %rdx
	leaq	146(%r12), %rsi
	leaq	144(%r12), %rdi
	movl	%r9d, -156(%rbp)
	andl	$510, %edx
	movq	%rcx, -152(%rbp)
	movl	%r8d, -144(%rbp)
	movl	%r10d, -136(%rbp)
	call	memmove@PLT
	movslq	-136(%rbp), %r10
	movl	-144(%rbp), %r8d
	movq	-152(%rbp), %rcx
	movslq	-156(%rbp), %r9
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L805:
	movq	-96(%rbp), %r8
	movl	$0, (%rbx)
	subq	%rdx, %r8
	sarq	%r8
.L749:
	testl	%r8d, %r8d
	jne	.L751
	movl	$8, (%rbx)
	movq	-112(%rbp), %rcx
	movl	$65535, %r15d
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L806:
	sall	$10, %r15d
	movl	$2, %eax
	movl	$2, %r10d
	leal	-56613888(%rsi,%r15), %r15d
	movl	$4, %esi
	movl	%r15d, %r8d
	andl	$-1024, %r8d
	jmp	.L739
.L754:
	movl	$2, %r9d
	movl	$1, %r10d
	cmpq	%rcx, %r13
	jbe	.L753
	leaq	-56(%rbp), %rax
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, -88(%rbp)
	call	_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode
	movl	(%rbx), %edx
	cmpl	$15, %edx
	jne	.L755
	movl	$0, (%rbx)
	xorl	%edx, %edx
.L755:
	movq	-96(%rbp), %rdi
	leaq	-60(%rbp), %rax
	movq	-112(%rbp), %rcx
	subq	%rax, %rdi
	movq	%rdi, %rax
	sarq	%rax
	movl	%eax, %r8d
	testl	%edx, %edx
	jg	.L770
	cmpl	$2, %eax
	je	.L807
.L770:
	movl	$2, %r9d
	movl	$1, %r10d
	jmp	.L753
.L804:
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	$65535, %r15d
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	jmp	.L732
.L803:
	call	__stack_chk_fail@PLT
.L807:
	movzwl	-58(%rbp), %eax
	movl	%eax, %edx
	movl	%eax, %r8d
	andl	$64512, %edx
	cmpl	$56320, %edx
	je	.L808
	movl	$2, %r9d
	movl	$1, %ebx
	movl	$1, %r11d
	jmp	.L756
.L808:
	sall	$10, %r15d
	leal	-56613888(%rax,%r15), %r15d
	jmp	.L752
.L751:
	movzwl	-60(%rbp), %r15d
	movq	-112(%rbp), %rcx
	movl	%r15d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L770
	jmp	.L758
	.cfi_endproc
.LFE2149:
	.size	ucnv_getNextUChar_67, .-ucnv_getNextUChar_67
	.p2align 4
	.globl	ucnv_convertEx_67
	.type	ucnv_convertEx_67, @function
ucnv_convertEx_67:
.LFB2150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$2312, %rsp
	.cfi_offset 3, -56
	movq	32(%rbp), %rax
	movl	56(%rbp), %r8d
	movq	%rcx, -2288(%rbp)
	movq	64(%rbp), %rbx
	movq	16(%rbp), %rcx
	movq	%rdx, -2304(%rbp)
	movq	24(%rbp), %r11
	movl	48(%rbp), %esi
	movq	%rax, -2264(%rbp)
	movq	40(%rbp), %rax
	movq	%r9, -2272(%rbp)
	movb	%r8b, -2273(%rbp)
	movq	%rax, -2296(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L809
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jg	.L809
	testq	%r12, %r12
	movq	%rdi, %r14
	movq	%rdx, %rdi
	sete	%dl
	testq	%r15, %r15
	sete	%al
	orb	%al, %dl
	jne	.L811
	testq	%r14, %r14
	je	.L811
	movq	(%r15), %r9
	testq	%rdi, %rdi
	je	.L811
	testq	%r9, %r9
	je	.L811
	movq	(%rdi), %r10
	testq	%r10, %r10
	je	.L811
	testq	%r13, %r13
	je	.L811
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	setne	%dl
	cmpq	%rdi, %r9
	seta	%al
	testb	%al, %dl
	jne	.L811
	cmpq	-2288(%rbp), %r10
	ja	.L811
	testq	%rdi, %rdi
	je	.L815
	movq	%rdi, %rax
	subq	%r9, %rax
	cmpq	$2147483647, %rax
	jbe	.L815
	cmpq	%rdi, %r9
	jb	.L811
.L815:
	movq	-2288(%rbp), %rdx
	movq	%rdx, %rax
	subq	%r10, %rax
	cmpq	$2147483647, %rax
	jbe	.L870
	cmpq	%rdx, %r10
	jb	.L811
.L870:
	testq	%rcx, %rcx
	je	.L980
	movq	-2296(%rbp), %rax
	cmpq	%rax, %rcx
	jnb	.L811
	testq	%r11, %r11
	je	.L811
	movq	(%r11), %rdx
	testq	%rdx, %rdx
	je	.L811
	movq	-2264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L811
	movq	(%rdi), %r13
	testq	%rax, %rax
	je	.L811
	testq	%r13, %r13
	je	.L811
.L819:
	cmpq	$0, -2272(%rbp)
	je	.L981
.L820:
	testb	%sil, %sil
	jne	.L982
	movzbl	91(%r14), %eax
	testb	%al, %al
	jg	.L983
.L822:
	movq	48(%r12), %rdi
	movq	48(%r14), %rsi
	movq	16(%rdi), %rax
	cmpb	$4, 69(%rax)
	je	.L984
.L831:
	movq	$0, -2328(%rbp)
	movq	16(%rsi), %rax
	cmpb	$4, 69(%rax)
	je	.L985
.L833:
	movq	%r10, %xmm0
	movl	$56, %esi
	movl	$56, %edi
	movq	-2272(%rbp), %rax
	movhps	-2288(%rbp), %xmm0
	movq	%r9, %xmm7
	movb	$0, -2174(%rbp)
	movq	%rax, -2216(%rbp)
	movq	-2296(%rbp), %rax
	movaps	%xmm0, -2144(%rbp)
	movq	%r12, %xmm0
	movq	%rax, -2200(%rbp)
	leaq	-2176(%rbp), %rax
	punpcklqdq	%xmm7, %xmm0
	movq	$0, -2128(%rbp)
	movw	%si, -2176(%rbp)
	movb	%r8b, -2238(%rbp)
	movq	$0, -2192(%rbp)
	movw	%di, -2240(%rbp)
	movq	%rax, -2312(%rbp)
	movq	%r14, -2168(%rbp)
	movq	%r14, -2320(%rbp)
	movq	%rcx, %r14
	movq	%r15, -2336(%rbp)
	movq	%r12, %r15
	movq	%rbx, %r12
	movq	%r11, %rbx
	movups	%xmm0, -2232(%rbp)
	.p2align 4,,10
	.p2align 3
.L834:
	cmpq	%r13, %rdx
	jb	.L835
.L992:
	movl	(%r12), %esi
	testl	%esi, %esi
	jle	.L986
.L835:
	movq	%rdx, %xmm0
	movq	%r13, %xmm1
	movq	-2312(%rbp), %rdi
	movq	%r12, %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -2160(%rbp)
	call	_ZL24_fromUnicodeWithCallbackP25UConverterFromUnicodeArgsP10UErrorCode
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L987
.L836:
	movq	-2264(%rbp), %rax
	movq	%r14, (%rax)
	movq	%r14, (%rbx)
	movsbl	93(%r15), %eax
	testb	%al, %al
	jg	.L988
	movq	-2272(%rbp), %rax
	cmpq	%rax, -2224(%rbp)
	je	.L989
.L847:
	cmpq	$0, -2328(%rbp)
	leaq	-2240(%rbp), %r13
	je	.L854
	movq	-2320(%rbp), %rax
	movl	208(%rax), %ecx
	testl	%ecx, %ecx
	js	.L990
.L854:
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r14, -2208(%rbp)
	call	_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode
	movq	-2264(%rbp), %rax
	movq	-2208(%rbp), %r13
	movq	%r13, (%rax)
	movl	(%r12), %eax
	cmpl	$15, %eax
	je	.L991
	testl	%eax, %eax
	jg	.L977
	cmpb	$0, -2273(%rbp)
	jne	.L979
	cmpq	%r14, %r13
	je	.L977
	movq	(%rbx), %rdx
	cmpq	%r13, %rdx
	jnb	.L992
	jmp	.L835
.L997:
	movslq	%eax, %rcx
	leaq	104(%r14), %r8
	leaq	120(%r14,%rcx), %r9
	leaq	104(%r14,%rcx), %rsi
	cmpq	%r8, %r9
	leaq	120(%r14), %r8
	setbe	%r9b
	cmpq	%r8, %rsi
	setnb	%r8b
	orb	%r8b, %r9b
	je	.L868
	leal	-1(%rdx), %r8d
	subl	%eax, %r8d
	cmpl	$14, %r8d
	seta	%r9b
	cmpl	%eax, %edx
	setg	%r8b
	testb	%r8b, %r9b
	je	.L868
	movl	%edx, %r8d
	movl	$1, %ecx
	movdqu	(%rsi), %xmm3
	subl	%eax, %r8d
	cmpl	%eax, %edx
	cmovle	%ecx, %r8d
	movups	%xmm3, 104(%r14)
	movl	%r8d, %ecx
	shrl	$4, %ecx
	cmpl	$1, %ecx
	je	.L825
	movdqu	16(%rsi), %xmm6
	movups	%xmm6, 120(%r14)
	cmpl	$2, %ecx
	je	.L825
	movdqu	32(%rsi), %xmm7
	movups	%xmm7, 136(%r14)
	cmpl	$3, %ecx
	je	.L825
	movdqu	48(%rsi), %xmm3
	movups	%xmm3, 152(%r14)
	cmpl	$4, %ecx
	je	.L825
	movdqu	64(%rsi), %xmm5
	movups	%xmm5, 168(%r14)
	cmpl	$5, %ecx
	je	.L825
	movdqu	80(%rsi), %xmm6
	movups	%xmm6, 184(%r14)
	cmpl	$6, %ecx
	je	.L825
	movdqu	96(%rsi), %xmm7
	movups	%xmm7, 200(%r14)
.L825:
	movl	%r8d, %ecx
	andl	$-16, %ecx
	leal	(%rdi,%rcx), %esi
	cmpl	%ecx, %r8d
	je	.L827
	movslq	%esi, %r9
	leal	1(%rsi), %r8d
	leal	1(%rcx), %r10d
	movzbl	104(%r14,%r9), %r11d
	movslq	%ecx, %r9
	movb	%r11b, 104(%r14,%r9)
	cmpl	%r8d, %edx
	jle	.L827
	movslq	%r8d, %r8
	leal	2(%rsi), %r9d
	leal	2(%rcx), %r11d
	movzbl	104(%r14,%r8), %r12d
	movslq	%r10d, %r8
	movb	%r12b, 104(%r14,%r8)
	cmpl	%r9d, %edx
	jle	.L827
	movslq	%r9d, %r9
	leal	3(%rsi), %r8d
	leal	3(%rcx), %r10d
	movslq	%r11d, %r11
	movzbl	104(%r14,%r9), %r9d
	movb	%r9b, 104(%r14,%r11)
	cmpl	%r8d, %edx
	jle	.L827
	movslq	%r8d, %r8
	leal	4(%rsi), %r9d
	leal	4(%rcx), %r11d
	movslq	%r10d, %r10
	movzbl	104(%r14,%r8), %r8d
	movb	%r8b, 104(%r14,%r10)
	cmpl	%r9d, %edx
	jle	.L827
	movslq	%r9d, %r9
	leal	5(%rsi), %r8d
	leal	5(%rcx), %r10d
	movslq	%r11d, %r11
	movzbl	104(%r14,%r9), %r9d
	movb	%r9b, 104(%r14,%r11)
	cmpl	%r8d, %edx
	jle	.L827
	movslq	%r8d, %r8
	leal	6(%rsi), %r9d
	leal	6(%rcx), %r11d
	movslq	%r10d, %r10
	movzbl	104(%r14,%r8), %r8d
	movb	%r8b, 104(%r14,%r10)
	cmpl	%r9d, %edx
	jle	.L827
	movslq	%r9d, %r9
	leal	7(%rsi), %r8d
	leal	7(%rcx), %r10d
	movslq	%r11d, %r11
	movzbl	104(%r14,%r9), %r9d
	movb	%r9b, 104(%r14,%r11)
	cmpl	%r8d, %edx
	jle	.L827
	movslq	%r8d, %r8
	leal	8(%rsi), %r9d
	leal	8(%rcx), %r11d
	movslq	%r10d, %r10
	movzbl	104(%r14,%r8), %r8d
	movb	%r8b, 104(%r14,%r10)
	cmpl	%r9d, %edx
	jle	.L827
	movslq	%r9d, %r9
	leal	9(%rsi), %r8d
	leal	9(%rcx), %r10d
	movslq	%r11d, %r11
	movzbl	104(%r14,%r9), %r9d
	movb	%r9b, 104(%r14,%r11)
	cmpl	%r8d, %edx
	jle	.L827
	movslq	%r8d, %r8
	leal	10(%rsi), %r9d
	leal	10(%rcx), %r11d
	movslq	%r10d, %r10
	movzbl	104(%r14,%r8), %r8d
	movb	%r8b, 104(%r14,%r10)
	cmpl	%r9d, %edx
	jle	.L827
	movslq	%r9d, %r9
	leal	11(%rsi), %r8d
	leal	11(%rcx), %r10d
	movslq	%r11d, %r11
	movzbl	104(%r14,%r9), %r9d
	movb	%r9b, 104(%r14,%r11)
	cmpl	%r8d, %edx
	jle	.L827
	movslq	%r8d, %r8
	leal	12(%rsi), %r9d
	leal	12(%rcx), %r11d
	movslq	%r10d, %r10
	movzbl	104(%r14,%r8), %r8d
	movb	%r8b, 104(%r14,%r10)
	cmpl	%r9d, %edx
	jle	.L827
	movslq	%r9d, %r9
	leal	13(%rsi), %r8d
	leal	13(%rcx), %r10d
	movslq	%r11d, %r11
	movzbl	104(%r14,%r9), %r9d
	movb	%r9b, 104(%r14,%r11)
	cmpl	%r8d, %edx
	jle	.L827
	movslq	%r8d, %r8
	movslq	%r10d, %r10
	addl	$14, %esi
	addl	$14, %ecx
	movzbl	104(%r14,%r8), %r8d
	movb	%r8b, 104(%r14,%r10)
	cmpl	%esi, %edx
	jle	.L827
	movslq	%esi, %rsi
	movslq	%ecx, %rcx
	movzbl	104(%r14,%rsi), %esi
	movb	%sil, 104(%r14,%rcx)
.L827:
	movl	%edx, %ecx
	subl	%eax, %ecx
	cmpl	%edi, %edx
	movl	$1, %edx
	movl	%ecx, %eax
	movq	-2288(%rbp), %rcx
	cmovle	%edx, %eax
	movb	%al, 91(%r14)
	movq	-2304(%rbp), %rax
	movq	%rcx, (%rax)
	movl	$15, (%rbx)
	.p2align 4,,10
	.p2align 3
.L809:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L993
	addq	$2312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	.cfi_restore_state
	movl	$1, (%rbx)
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L986:
	movq	-2320(%rbp), %rax
	cmpb	$0, 281(%rax)
	js	.L835
	cmpb	$0, -2174(%rbp)
	je	.L836
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L988:
	leaq	144(%r15), %rdi
	movq	-2296(%rbp), %r8
	movq	%r14, %r13
	xorl	%esi, %esi
	movq	%rdi, %r9
	.p2align 4,,10
	.p2align 3
.L846:
	movl	%esi, %edx
	movzwl	(%r9), %ecx
	leal	1(%rsi), %esi
	cmpq	%r13, %r8
	je	.L994
	addq	$2, %r13
	addq	$2, %r9
	movw	%cx, -2(%r13)
	cmpl	%esi, %eax
	jne	.L846
	movq	-2264(%rbp), %rax
	movb	$0, 93(%r15)
	movq	%r13, (%rax)
	movq	(%rbx), %rdx
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L990:
	cmpb	$0, 282(%r15)
	jne	.L854
	cmpl	$-127, %esi
	jne	.L851
	movl	$0, (%r12)
.L851:
	leaq	-2240(%rbp), %r13
	movq	-2328(%rbp), %rax
	movq	-2312(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	*%rax
	movl	(%r12), %eax
	cmpl	$15, %eax
	je	.L977
	testl	%eax, %eax
	jle	.L852
	cmpb	$0, 64(%r15)
	jg	.L854
	movq	-2264(%rbp), %rax
	leaq	2(%r14), %rdx
	movq	%rdx, %r13
	movq	%rdx, (%rax)
	movq	%rdx, (%rbx)
	jmp	.L834
.L996:
	movl	$0, (%r12)
	.p2align 4,,10
	.p2align 3
.L979:
	movq	(%rbx), %rdx
.L859:
	movq	-2272(%rbp), %rax
	cmpq	%rax, -2224(%rbp)
	jne	.L834
	cmpb	$0, 282(%r15)
	js	.L834
	cmpb	$0, 93(%r15)
	jne	.L834
	movb	$1, -2174(%rbp)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L991:
	cmpb	$0, -2273(%rbp)
	movq	(%rbx), %rdx
	movl	$0, (%r12)
	je	.L834
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L989:
	cmpb	$0, 282(%r15)
	js	.L847
	cmpb	$0, 64(%r15)
	jne	.L847
	cmpb	$0, -2273(%rbp)
	je	.L977
	cmpb	$0, -2174(%rbp)
	je	.L847
	movq	-2336(%rbp), %r15
	movq	%r12, %rbx
.L849:
	movq	-2224(%rbp), %rax
	movq	-2304(%rbp), %rcx
	movl	(%rbx), %edx
	movq	%rax, (%r15)
	movq	-2144(%rbp), %rax
	movq	%rax, (%rcx)
	testl	%edx, %edx
	jg	.L809
	cmpq	%rax, -2288(%rbp)
	je	.L862
	movb	$0, (%rax)
	cmpl	$-124, (%rbx)
	jne	.L809
	movl	$0, (%rbx)
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L994:
	movslq	%edx, %rsi
	movl	%eax, %ecx
	leaq	160(%rsi,%rsi), %r9
	subl	%edx, %ecx
	leaq	-16(%r15,%r9), %r10
	addq	%r15, %r9
	cmpq	%r9, %rdi
	leaq	160(%r15), %r9
	setnb	%r11b
	cmpq	%r9, %r10
	setnb	%r9b
	orb	%r9b, %r11b
	je	.L840
	leal	-1(%rcx), %r9d
	cmpl	$6, %r9d
	seta	%r11b
	cmpl	%edx, %eax
	setg	%r9b
	testb	%r9b, %r11b
	je	.L840
	cmpl	%edx, %eax
	movl	$1, %r9d
	movdqu	(%r10), %xmm2
	cmovg	%ecx, %r9d
	movups	%xmm2, 144(%r15)
	movl	%r9d, %esi
	shrl	$3, %esi
	cmpl	$1, %esi
	je	.L841
	movdqu	16(%r10), %xmm3
	movups	%xmm3, 160(%r15)
	cmpl	$2, %esi
	je	.L841
	movdqu	32(%r10), %xmm4
	movups	%xmm4, 176(%r15)
	cmpl	$3, %esi
	je	.L841
	movdqu	48(%r10), %xmm5
	movups	%xmm5, 192(%r15)
	cmpl	$4, %esi
	je	.L841
	movdqu	64(%r10), %xmm6
	movups	%xmm6, 208(%r15)
	cmpl	$5, %esi
	je	.L841
	movdqu	80(%r10), %xmm7
	movups	%xmm7, 224(%r15)
	cmpl	$6, %esi
	je	.L841
	movdqu	96(%r10), %xmm6
	movups	%xmm6, 240(%r15)
	cmpl	$7, %esi
	je	.L841
	movdqu	112(%r10), %xmm5
	movups	%xmm5, 256(%r15)
	cmpl	$8, %esi
	je	.L841
	movdqu	128(%r10), %xmm4
	movups	%xmm4, 272(%r15)
	cmpl	$9, %esi
	je	.L841
	movdqu	144(%r10), %xmm7
	movups	%xmm7, 288(%r15)
	cmpl	$10, %esi
	je	.L841
	movdqu	160(%r10), %xmm6
	movups	%xmm6, 304(%r15)
	cmpl	$11, %esi
	je	.L841
	movdqu	176(%r10), %xmm3
	movups	%xmm3, 320(%r15)
	cmpl	$12, %esi
	je	.L841
	movdqu	192(%r10), %xmm5
	movups	%xmm5, 336(%r15)
	cmpl	$13, %esi
	je	.L841
	movdqu	208(%r10), %xmm4
	movups	%xmm4, 352(%r15)
	cmpl	$14, %esi
	je	.L841
	movdqu	224(%r10), %xmm5
	movups	%xmm5, 368(%r15)
	.p2align 4,,10
	.p2align 3
.L841:
	movl	%r9d, %esi
	andl	$-8, %esi
	leal	(%rsi,%rdx), %r10d
	cmpl	%r9d, %esi
	je	.L843
	movslq	%r10d, %r11
	movslq	%esi, %rsi
	leal	1(%r10), %r9d
	movzwl	(%rdi,%r11,2), %r11d
	addq	%rsi, %rsi
	movw	%r11w, 144(%r15,%rsi)
	cmpl	%eax, %r9d
	jge	.L843
	movslq	%r9d, %r9
	addq	%r9, %r9
	movzwl	144(%r15,%r9), %r11d
	movw	%r11w, 2(%rdi,%rsi)
	leal	2(%r10), %r11d
	cmpl	%r11d, %eax
	jle	.L843
	movzwl	2(%rdi,%r9), %r11d
	movw	%r11w, 4(%rdi,%rsi)
	leal	3(%r10), %r11d
	cmpl	%r11d, %eax
	jle	.L843
	movzwl	4(%rdi,%r9), %r11d
	movw	%r11w, 6(%rdi,%rsi)
	leal	4(%r10), %r11d
	cmpl	%r11d, %eax
	jle	.L843
	movzwl	6(%rdi,%r9), %r11d
	movw	%r11w, 8(%rdi,%rsi)
	leal	5(%r10), %r11d
	cmpl	%r11d, %eax
	jle	.L843
	movzwl	8(%rdi,%r9), %r11d
	addl	$6, %r10d
	movw	%r11w, 10(%rdi,%rsi)
	cmpl	%r10d, %eax
	jle	.L843
	movzwl	10(%rdi,%r9), %r9d
	movw	%r9w, 12(%rdi,%rsi)
.L843:
	cmpl	%edx, %eax
	movl	$1, %eax
	cmovg	%ecx, %eax
	movb	%al, 93(%r15)
	movq	-2264(%rbp), %rax
	movq	%r13, (%rax)
	movq	(%rbx), %rdx
	movl	$0, (%r12)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L852:
	cmpl	$-127, %eax
	je	.L995
	cmpb	$0, -2273(%rbp)
	je	.L977
	cmpb	$0, 64(%r15)
	jle	.L855
	movl	$11, (%r12)
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r14, -2208(%rbp)
	call	_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode
	movq	-2264(%rbp), %rax
	movq	-2208(%rbp), %r13
	movq	%r13, (%rax)
	movl	(%r12), %eax
	cmpl	$15, %eax
	je	.L996
	testl	%eax, %eax
	jle	.L979
.L977:
	movq	-2336(%rbp), %r15
.L837:
	movq	-2224(%rbp), %rax
	movq	-2304(%rbp), %rbx
	movq	%rax, (%r15)
	movq	-2144(%rbp), %rax
	movq	%rax, (%rbx)
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L980:
	testb	%r8b, %r8b
	je	.L811
	leaq	-64(%rbp), %rax
	leaq	-2112(%rbp), %r13
	movq	%rax, -2296(%rbp)
	leaq	-2248(%rbp), %rax
	movq	%r13, %rdx
	movq	%r13, %rcx
	movq	%r13, -2248(%rbp)
	leaq	-2256(%rbp), %r11
	movq	%r13, -2256(%rbp)
	movq	%rax, -2264(%rbp)
	jmp	.L819
.L840:
	movq	%rsi, %rdi
	negq	%rdi
	leaq	(%r15,%rdi,2), %r9
	.p2align 4,,10
	.p2align 3
.L844:
	movzwl	144(%r15,%rsi,2), %edi
	movw	%di, 144(%r9,%rsi,2)
	addq	$1, %rsi
	cmpl	%esi, %eax
	jg	.L844
	jmp	.L843
.L987:
	movq	-2160(%rbp), %rax
	movq	-2336(%rbp), %r15
	movq	%rax, (%rbx)
	jmp	.L837
.L995:
	movl	$0, (%r12)
	jmp	.L854
.L982:
	movl	$1, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	%r8d, -2336(%rbp)
	movq	%r11, -2328(%rbp)
	movq	%rcx, -2320(%rbp)
	movq	%r9, -2312(%rbp)
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movl	$1, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movq	-2320(%rbp), %rcx
	movq	-2264(%rbp), %rax
	movq	-2304(%rbp), %rdx
	movq	-2328(%rbp), %r11
	movq	%rcx, (%rax)
	movq	-2312(%rbp), %r9
	movq	(%rdx), %r10
	movl	-2336(%rbp), %r8d
	movq	%rcx, (%r11)
	movq	%rcx, %rdx
	movq	(%rax), %r13
	jmp	.L822
.L984:
	movq	32(%rsi), %rax
	movq	136(%rax), %rax
	movq	%rax, -2328(%rbp)
	testq	%rax, %rax
	je	.L831
.L832:
	movq	-2296(%rbp), %rdi
	leaq	64(%rcx), %rax
	movq	%rdi, %rsi
	subq	%rcx, %rsi
	cmpq	$64, %rsi
	cmovle	%rdi, %rax
	movq	%rax, -2296(%rbp)
	jmp	.L833
.L981:
	movq	%r9, %rdi
	movl	%r8d, -2340(%rbp)
	movl	%esi, -2280(%rbp)
	movq	%r11, -2336(%rbp)
	movq	%rcx, -2328(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%r10, -2312(%rbp)
	movq	%r9, -2272(%rbp)
	call	strlen@PLT
	movq	-2272(%rbp), %r9
	movl	-2340(%rbp), %r8d
	movl	-2280(%rbp), %esi
	movq	-2336(%rbp), %r11
	addq	%r9, %rax
	movq	-2328(%rbp), %rcx
	movq	-2320(%rbp), %rdx
	movq	%rax, -2272(%rbp)
	movq	-2312(%rbp), %r10
	jmp	.L820
.L983:
	movq	-2288(%rbp), %r13
	movsbl	%al, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L828:
	movzbl	104(%r14,%rax), %esi
	movl	%eax, %edi
	cmpq	%r10, %r13
	je	.L997
	addq	$1, %rax
	addq	$1, %r10
	movb	%sil, -1(%r10)
	cmpl	%eax, %edx
	jg	.L828
	movq	-2304(%rbp), %rax
	movb	$0, 91(%r14)
	movq	(%r11), %rdx
	movq	%r10, (%rax)
	movq	-2264(%rbp), %rax
	movq	(%rax), %r13
	testb	%r8b, %r8b
	jne	.L822
	cmpb	$0, 281(%r14)
	js	.L822
	cmpq	%r13, %rdx
	jne	.L822
	cmpb	$0, 93(%r12)
	jne	.L822
	cmpb	$0, 282(%r12)
	js	.L822
	cmpq	-2272(%rbp), %r9
	jne	.L822
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L985:
	movq	32(%rdi), %rax
	movq	128(%rax), %rax
	movq	%rax, -2328(%rbp)
	testq	%rax, %rax
	jne	.L832
	jmp	.L833
.L862:
	movl	$-124, (%rbx)
	jmp	.L809
.L993:
	call	__stack_chk_fail@PLT
.L868:
	movq	%r14, %r8
	subq	%rcx, %r8
.L824:
	movzbl	104(%r14,%rcx), %esi
	movb	%sil, 104(%r8,%rcx)
	addq	$1, %rcx
	cmpl	%ecx, %edx
	jg	.L824
	jmp	.L827
.L855:
	movq	-2320(%rbp), %r14
	movq	%r12, %rbx
	movq	%r15, %r12
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	$1, %esi
	movq	-2336(%rbp), %r15
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	jmp	.L849
	.cfi_endproc
.LFE2150:
	.size	ucnv_convertEx_67, .-ucnv_convertEx_67
	.p2align 4
	.type	_ZL20ucnv_internalConvertP10UConverterS0_PciPKciP10UErrorCode, @function
_ZL20ucnv_internalConvertP10UConverterS0_PciPKciP10UErrorCode:
.LFB2151:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$3352, %rsp
	movq	%rdi, -3328(%rbp)
	movq	16(%rbp), %r14
	movq	%rdx, -3368(%rbp)
	movl	%ecx, -3356(%rbp)
	movq	%r8, -3304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%r9d, %rax
	addq	%r8, %rax
	movq	%rax, -3344(%rbp)
	testl	%r9d, %r9d
	jns	.L1000
	movq	%r8, %rdi
	call	strlen@PLT
	addq	%rbx, %rax
	movq	%rax, -3344(%rbp)
.L1000:
	cmpq	-3344(%rbp), %rbx
	je	.L1130
	movq	-3368(%rbp), %rbx
	movl	-3356(%rbp), %edx
	leaq	-3136(%rbp), %r15
	movq	%r15, -3280(%rbp)
	movq	%r15, -3288(%rbp)
	movq	%rbx, -3272(%rbp)
	testl	%edx, %edx
	jg	.L1131
	cmpl	$15, (%r14)
	je	.L1056
	movl	-3356(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L1049
.L1056:
	movl	$0, -3316(%rbp)
	leaq	-1088(%rbp), %r13
.L1004:
	cmpq	$0, -3328(%rbp)
	sete	%dl
	testq	%r12, %r12
	sete	%al
	orl	%eax, %edx
	leaq	-3200(%rbp), %rax
	movb	%dl, -3317(%rbp)
	movq	%rax, -3336(%rbp)
	.p2align 4,,10
	.p2align 3
.L1047:
	cmpb	$0, -3317(%rbp)
	movl	$0, (%r14)
	movq	%r13, -3272(%rbp)
	jne	.L1011
	movq	-3304(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L1011
	movq	-3344(%rbp), %rax
	testq	%rax, %rax
	je	.L1010
	cmpq	%rcx, %rax
	jb	.L1011
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1010
	subq	%rcx, %rax
	movl	$2147483648, %ebx
	cmpq	%rbx, %rax
	jb	.L1010
	cmpq	%rcx, %rdx
	ja	.L1011
.L1010:
	movq	-3288(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1011
	movq	-3280(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1011
	movq	-3344(%rbp), %rax
	movq	%rax, -3312(%rbp)
	testq	%rax, %rax
	je	.L1132
.L1013:
	movq	-3328(%rbp), %rsi
	movq	%r13, %rdi
	movsbl	91(%rsi), %eax
	testb	%al, %al
	jg	.L1133
.L1014:
	movq	48(%r12), %rax
	movq	-3328(%rbp), %rsi
	movq	16(%rax), %r8
	movq	48(%rsi), %rsi
	cmpb	$4, 69(%r8)
	je	.L1134
.L1016:
	movq	16(%rsi), %rsi
	movq	%r13, -3376(%rbp)
	movq	$0, -3352(%rbp)
	cmpb	$4, 69(%rsi)
	jne	.L1017
	movq	32(%rax), %rax
	movq	128(%rax), %rax
	movq	%rax, %rsi
	movq	%rax, -3352(%rbp)
	leaq	-3072(%rbp), %rax
	testq	%rsi, %rsi
	cmove	%r13, %rax
	movq	%rax, -3376(%rbp)
.L1017:
	movl	$56, %esi
	movq	%r12, %xmm0
	movq	%rcx, %xmm6
	movq	-3328(%rbp), %rax
	movq	%rdi, -3168(%rbp)
	movl	$56, %edi
	punpcklqdq	%xmm6, %xmm0
	movq	%rax, -3192(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -3392(%rbp)
	movq	%rax, -3160(%rbp)
	movq	-3312(%rbp), %rax
	movb	$0, -3198(%rbp)
	movq	%rax, -3240(%rbp)
	movq	-3376(%rbp), %rax
	movq	$0, -3152(%rbp)
	movw	%si, -3200(%rbp)
	movb	$1, -3262(%rbp)
	movq	$0, -3216(%rbp)
	movq	%rax, -3224(%rbp)
	movw	%di, -3264(%rbp)
	movups	%xmm0, -3256(%rbp)
	.p2align 4,,10
	.p2align 3
.L1018:
	cmpq	%rdx, %rbx
	ja	.L1019
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L1135
.L1019:
	movq	%rdx, %xmm0
	movq	%rbx, %xmm1
	movq	-3336(%rbp), %rdi
	movq	%r14, %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -3184(%rbp)
	call	_ZL24_fromUnicodeWithCallbackP25UConverterFromUnicodeArgsP10UErrorCode
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L1136
.L1020:
	movsbl	93(%r12), %eax
	movq	%r15, -3280(%rbp)
	movq	%r15, -3288(%rbp)
	testb	%al, %al
	jg	.L1137
	movq	-3312(%rbp), %rax
	cmpq	-3248(%rbp), %rax
	je	.L1138
.L1031:
	cmpq	$0, -3352(%rbp)
	leaq	-3264(%rbp), %r8
	je	.L1039
	movq	-3328(%rbp), %rax
	movl	208(%rax), %eax
	testl	%eax, %eax
	js	.L1139
.L1039:
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r15, -3232(%rbp)
	call	_ZL22_toUnicodeWithCallbackP23UConverterToUnicodeArgsP10UErrorCode
	movq	-3232(%rbp), %rbx
	movl	(%r14), %ecx
	movq	%rbx, -3280(%rbp)
	cmpl	$15, %ecx
	je	.L1140
	testl	%ecx, %ecx
	jg	.L1141
.L1044:
	movq	-3288(%rbp), %rdx
	movq	-3312(%rbp), %rax
	cmpq	-3248(%rbp), %rax
	jne	.L1018
	cmpb	$0, 282(%r12)
	js	.L1018
	cmpb	$0, 93(%r12)
	jne	.L1018
	movb	$1, -3198(%rbp)
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1131:
	subq	$8, %rsp
	leaq	-3280(%rbp), %rax
	movslq	%edx, %rcx
	movq	%r12, %rsi
	leaq	-1088(%rbp), %r13
	pushq	%r14
	addq	%rbx, %rcx
	movq	-3344(%rbp), %r9
	pushq	$1
	movq	-3328(%rbp), %rdi
	leaq	-3272(%rbp), %rdx
	leaq	-3304(%rbp), %r8
	pushq	$0
	pushq	%r13
	pushq	%rax
	leaq	-3288(%rbp), %rax
	pushq	%rax
	pushq	%r15
	call	ucnv_convertEx_67
	movl	-3272(%rbp), %eax
	addq	$64, %rsp
	subl	%ebx, %eax
	cmpl	$15, (%r14)
	movl	%eax, -3316(%rbp)
	je	.L1004
.L998:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1142
	movl	-3316(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1011:
	.cfi_restore_state
	movl	$1, (%r14)
.L1008:
	movl	-3316(%rbp), %edx
	movl	-3356(%rbp), %esi
	movq	%r14, %rcx
	movq	-3368(%rbp), %rdi
	call	u_terminateChars_67@PLT
	movl	%eax, -3316(%rbp)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	-3328(%rbp), %rax
	cmpb	$0, 281(%rax)
	js	.L1019
	cmpb	$0, -3198(%rbp)
	je	.L1020
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	-3376(%rbp), %r8
	leaq	144(%r12), %rcx
	movl	$1, %esi
	movq	%r15, %rbx
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1030:
	leaq	1(%rsi), %rdi
	cmpq	%rbx, %r8
	je	.L1143
	movq	%rdi, %rsi
.L1023:
	movzwl	142(%r12,%rsi,2), %edi
	addq	$2, %rbx
	movl	%esi, %edx
	movw	%di, -2(%rbx)
	cmpl	%esi, %eax
	jg	.L1030
	movb	$0, 93(%r12)
	movq	%r15, %rdx
	movq	%rbx, -3280(%rbp)
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1139:
	cmpb	$0, 282(%r12)
	jne	.L1039
	cmpl	$-127, %ecx
	jne	.L1034
	movl	$0, (%r14)
.L1034:
	leaq	-3264(%rbp), %r8
	movq	-3352(%rbp), %rax
	movq	-3336(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r8, -3384(%rbp)
	movq	%r8, %rsi
	call	*%rax
	movl	(%r14), %eax
	movq	-3384(%rbp), %r8
	cmpl	$15, %eax
	je	.L1144
	testl	%eax, %eax
	jle	.L1037
	cmpb	$0, 64(%r12)
	jg	.L1039
	leaq	-3134(%rbp), %rbx
	movq	%rbx, -3280(%rbp)
	movq	%rbx, %rdx
	movq	%rbx, -3288(%rbp)
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1140:
	movl	$0, (%r14)
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1138:
	cmpb	$0, 282(%r12)
	js	.L1031
	cmpb	$0, 64(%r12)
	jne	.L1031
	cmpb	$0, -3198(%rbp)
	je	.L1031
	movq	%rax, -3304(%rbp)
	movq	-3168(%rbp), %rax
	movq	%rax, -3272(%rbp)
	.p2align 4,,10
	.p2align 3
.L1041:
	cmpq	-3392(%rbp), %rax
	je	.L1045
	movb	$0, (%rax)
	movl	(%r14), %ecx
	cmpl	$-124, %ecx
	je	.L1046
	movq	-3272(%rbp), %rax
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1143:
	movl	%eax, %edi
	subl	%esi, %edi
	movl	%edi, %esi
	movslq	%edx, %rdi
	leaq	160(%rdi,%rdi), %r9
	leaq	-16(%r12,%r9), %r8
	addq	%r12, %r9
	cmpq	%r9, %rcx
	leaq	160(%r12), %r9
	setnb	%r10b
	cmpq	%r9, %r8
	setnb	%r9b
	orb	%r9b, %r10b
	je	.L1024
	leal	-1(%rsi), %r9d
	cmpl	$6, %r9d
	jbe	.L1024
	cmpl	%edx, %eax
	movl	$1, %r9d
	movdqu	(%r8), %xmm2
	cmovg	%esi, %r9d
	movups	%xmm2, 144(%r12)
	movl	%r9d, %edi
	shrl	$3, %edi
	cmpl	$1, %edi
	je	.L1025
	movdqu	16(%r8), %xmm3
	movups	%xmm3, 160(%r12)
	cmpl	$2, %edi
	je	.L1025
	movdqu	32(%r8), %xmm4
	movups	%xmm4, 176(%r12)
	cmpl	$3, %edi
	je	.L1025
	movdqu	48(%r8), %xmm5
	movups	%xmm5, 192(%r12)
	cmpl	$4, %edi
	je	.L1025
	movdqu	64(%r8), %xmm6
	movups	%xmm6, 208(%r12)
	cmpl	$5, %edi
	je	.L1025
	movdqu	80(%r8), %xmm7
	movups	%xmm7, 224(%r12)
	cmpl	$6, %edi
	je	.L1025
	movdqu	96(%r8), %xmm7
	movups	%xmm7, 240(%r12)
	cmpl	$7, %edi
	je	.L1025
	movdqu	112(%r8), %xmm5
	movups	%xmm5, 256(%r12)
	cmpl	$8, %edi
	je	.L1025
	movdqu	128(%r8), %xmm4
	movups	%xmm4, 272(%r12)
	cmpl	$9, %edi
	je	.L1025
	movdqu	144(%r8), %xmm7
	movups	%xmm7, 288(%r12)
	cmpl	$10, %edi
	je	.L1025
	movdqu	160(%r8), %xmm6
	movups	%xmm6, 304(%r12)
	cmpl	$11, %edi
	je	.L1025
	movdqu	176(%r8), %xmm3
	movups	%xmm3, 320(%r12)
	cmpl	$12, %edi
	je	.L1025
	movdqu	192(%r8), %xmm5
	movups	%xmm5, 336(%r12)
	cmpl	$13, %edi
	je	.L1025
	movdqu	208(%r8), %xmm4
	movups	%xmm4, 352(%r12)
	cmpl	$14, %edi
	je	.L1025
	movdqu	224(%r8), %xmm7
	movups	%xmm7, 368(%r12)
	.p2align 4,,10
	.p2align 3
.L1025:
	movl	%r9d, %r8d
	andl	$-8, %r8d
	leal	(%rdx,%r8), %edi
	cmpl	%r8d, %r9d
	je	.L1027
	movslq	%edi, %r11
	movslq	%r8d, %r8
	leal	1(%rdi), %r9d
	movzwl	(%rcx,%r11,2), %r11d
	leaq	(%r8,%r8), %r10
	movw	%r11w, (%rcx,%r8,2)
	cmpl	%eax, %r9d
	jge	.L1027
	movslq	%r9d, %r9
	leaq	(%r9,%r9), %r8
	movzwl	(%rcx,%r9,2), %r9d
	movw	%r9w, 2(%rcx,%r10)
	leal	2(%rdi), %r9d
	cmpl	%r9d, %eax
	jle	.L1027
	movzwl	2(%rcx,%r8), %r9d
	movw	%r9w, 4(%rcx,%r10)
	leal	3(%rdi), %r9d
	cmpl	%r9d, %eax
	jle	.L1027
	movzwl	4(%rcx,%r8), %r9d
	movw	%r9w, 6(%rcx,%r10)
	leal	4(%rdi), %r9d
	cmpl	%r9d, %eax
	jle	.L1027
	movzwl	6(%rcx,%r8), %r9d
	movw	%r9w, 8(%rcx,%r10)
	leal	5(%rdi), %r9d
	cmpl	%r9d, %eax
	jle	.L1027
	movzwl	8(%rcx,%r8), %r9d
	addl	$6, %edi
	movw	%r9w, 10(%rcx,%r10)
	cmpl	%edi, %eax
	jle	.L1027
	movzwl	10(%rcx,%r8), %edi
	movw	%di, 12(%rcx,%r10)
.L1027:
	cmpl	%edx, %eax
	movl	$1, %eax
	movq	%rbx, -3280(%rbp)
	movq	%r15, %rdx
	cmovle	%eax, %esi
	movb	%sil, 93(%r12)
	movl	$0, (%r14)
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1037:
	cmpl	$-127, %eax
	je	.L1145
	cmpb	$0, 64(%r12)
	jle	.L1040
	movl	$11, (%r14)
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1130:
	movl	-3356(%rbp), %esi
	movq	-3368(%rbp), %rdi
	movq	%r14, %rcx
	xorl	%edx, %edx
	call	u_terminateChars_67@PLT
	movl	%eax, -3316(%rbp)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	%rdi, %rcx
	negq	%rcx
	leaq	(%r12,%rcx,2), %r8
	.p2align 4,,10
	.p2align 3
.L1028:
	movzwl	144(%r12,%rdi,2), %ecx
	movw	%cx, 144(%r8,%rdi,2)
	addq	$1, %rdi
	cmpl	%edi, %eax
	jg	.L1028
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1136:
	movq	-3184(%rbp), %rax
	movq	%rax, -3288(%rbp)
	movq	-3248(%rbp), %rax
.L1021:
	movq	%rax, -3304(%rbp)
	movq	-3168(%rbp), %rax
	movq	%rax, -3272(%rbp)
.L1042:
	subq	%r13, %rax
	addl	%eax, -3316(%rbp)
	cmpl	$15, %ecx
	je	.L1047
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1145:
	movl	$0, (%r14)
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1133:
	subl	$1, %eax
	addq	$104, %rsi
	leaq	1(%r13,%rax), %rdi
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L1015:
	movzbl	(%rsi), %r8d
	addq	$1, %rax
	addq	$1, %rsi
	movb	%r8b, -1(%rax)
	cmpq	%rdi, %rax
	jne	.L1015
	movq	-3328(%rbp), %rax
	movq	%rdi, -3272(%rbp)
	movb	$0, 91(%rax)
	jmp	.L1014
.L1040:
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movq	-3328(%rbp), %rdi
	xorl	%edx, %edx
	movl	$2, %esi
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movq	-3248(%rbp), %rax
	movl	(%r14), %ecx
	movq	%rax, -3304(%rbp)
	movq	-3168(%rbp), %rax
	movq	%rax, -3272(%rbp)
	testl	%ecx, %ecx
	jle	.L1041
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1049:
	movl	$0, -3316(%rbp)
	jmp	.L998
.L1132:
	movq	%rcx, %rdi
	movq	%rdx, -3352(%rbp)
	movq	%rcx, -3312(%rbp)
	call	strlen@PLT
	movq	-3312(%rbp), %rcx
	movq	-3352(%rbp), %rdx
	addq	%rcx, %rax
	movq	%rax, -3312(%rbp)
	jmp	.L1013
.L1134:
	movq	32(%rsi), %r8
	leaq	-3072(%rbp), %r10
	movq	%r10, -3376(%rbp)
	movq	136(%r8), %r11
	movq	%r11, -3352(%rbp)
	testq	%r11, %r11
	jne	.L1017
	jmp	.L1016
.L1141:
	movq	-3248(%rbp), %rax
	jmp	.L1021
.L1045:
	addl	$1024, -3316(%rbp)
	movl	$-124, (%r14)
	jmp	.L1008
.L1046:
	movl	-3316(%rbp), %eax
	addl	-3272(%rbp), %eax
	movl	$0, (%r14)
	subl	%r13d, %eax
	movl	%eax, -3316(%rbp)
	jmp	.L1008
.L1144:
	movq	-3248(%rbp), %rax
	movq	%rax, -3304(%rbp)
	movl	-3316(%rbp), %eax
	addl	-3168(%rbp), %eax
	subl	%r13d, %eax
	movl	%eax, -3316(%rbp)
	jmp	.L1047
.L1142:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2151:
	.size	_ZL20ucnv_internalConvertP10UConverterS0_PciPKciP10UErrorCode, .-_ZL20ucnv_internalConvertP10UConverterS0_PciPKciP10UErrorCode
	.section	.rodata.str1.1
.LC1:
	.string	""
	.text
	.p2align 4
	.type	_ZL23ucnv_convertAlgorithmica14UConverterTypeP10UConverterPciPKciP10UErrorCode.part.0, @function
_ZL23ucnv_convertAlgorithmica14UConverterTypeP10UConverterPciPKciP10UErrorCode.part.0:
.LFB2819:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rbp), %r8d
	movq	24(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L1147
	cmpl	$-1, 16(%rbp)
	movl	%edi, %r10d
	movq	%rdx, %r12
	movq	%r9, %rbx
	jne	.L1148
	cmpb	$0, (%r9)
	jne	.L1148
.L1147:
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	u_terminateChars_67@PLT
.L1146:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1158
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1148:
	.cfi_restore_state
	xorl	%ecx, %ecx
	leaq	-352(%rbp), %rdi
	movq	%r15, %r8
	movl	%r10d, -360(%rbp)
	leaq	.LC1(%rip), %rdx
	call	ucnv_createAlgorithmicConverter_67@PLT
	movl	(%r15), %ecx
	movq	%rax, %r11
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L1146
	movl	-360(%rbp), %r10d
	movl	$1, %edx
	movq	%r11, -360(%rbp)
	testb	%r10b, %r10b
	jne	.L1159
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movq	-360(%rbp), %r11
	movq	%r11, %rsi
.L1151:
	subq	$8, %rsp
	movl	16(%rbp), %r9d
	movq	%r13, %rdx
	movq	%rbx, %r8
	pushq	%r15
	movl	%r14d, %ecx
	movq	%r12, %rdi
	movq	%r11, -368(%rbp)
	call	_ZL20ucnv_internalConvertP10UConverterS0_PciPKciP10UErrorCode
	movq	-368(%rbp), %r11
	movl	%eax, -360(%rbp)
	movq	%r11, %rdi
	call	ucnv_close_67
	popq	%rax
	movl	-360(%rbp), %eax
	popq	%rdx
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1159:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZL6_resetP10UConverter21UConverterResetChoicea
	movq	-360(%rbp), %r11
	movq	%r12, %rsi
	movq	%r11, %r12
	jmp	.L1151
.L1158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2819:
	.size	_ZL23ucnv_convertAlgorithmica14UConverterTypeP10UConverterPciPKciP10UErrorCode.part.0, .-_ZL23ucnv_convertAlgorithmica14UConverterTypeP10UConverterPciPKciP10UErrorCode.part.0
	.p2align 4
	.globl	ucnv_convert_67
	.type	ucnv_convert_67, @function
ucnv_convert_67:
.LFB2152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$616, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L1180
	movl	(%r12), %r10d
	testl	%r10d, %r10d
	jg	.L1180
	testq	%r8, %r8
	movq	%rdx, %rbx
	sete	%dl
	cmpl	$-1, %r9d
	setl	%al
	orb	%al, %dl
	jne	.L1164
	movl	%ecx, %r13d
	testl	%ecx, %ecx
	js	.L1164
	movq	%rdi, %r15
	jle	.L1165
	testq	%rbx, %rbx
	je	.L1164
.L1165:
	testl	%r9d, %r9d
	je	.L1166
	cmpl	$-1, %r9d
	jne	.L1167
	cmpb	$0, (%r8)
	jne	.L1167
.L1166:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	u_terminateChars_67@PLT
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1164:
	movl	$1, (%r12)
.L1180:
	xorl	%eax, %eax
.L1160:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1181
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1167:
	.cfi_restore_state
	leaq	-640(%rbp), %rdi
	movq	%r12, %rdx
	movl	%r9d, -652(%rbp)
	movq	%r8, -648(%rbp)
	call	ucnv_createConverter_67@PLT
	movl	(%r12), %esi
	movq	%rax, %r14
	testl	%esi, %esi
	jg	.L1180
	movq	%r15, %rsi
	leaq	-352(%rbp), %rdi
	movq	%r12, %rdx
	call	ucnv_createConverter_67@PLT
	movl	(%r12), %ecx
	movq	-648(%rbp), %r8
	movl	-652(%rbp), %r9d
	movq	%rax, %r15
	testl	%ecx, %ecx
	jg	.L1182
	subq	$8, %rsp
	movq	%rbx, %rdx
	movl	%r13d, %ecx
	movq	%r14, %rsi
	pushq	%r12
	movq	%rax, %rdi
	call	_ZL20ucnv_internalConvertP10UConverterS0_PciPKciP10UErrorCode
	movq	%r14, %rdi
	movl	%eax, -648(%rbp)
	call	ucnv_close_67
	movq	%r15, %rdi
	call	ucnv_close_67
	popq	%rax
	movl	-648(%rbp), %eax
	popq	%rdx
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	%r14, %rdi
	call	ucnv_close_67
	xorl	%eax, %eax
	jmp	.L1160
.L1181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2152:
	.size	ucnv_convert_67, .-ucnv_convert_67
	.p2align 4
	.globl	ucnv_toAlgorithmic_67
	.type	ucnv_toAlgorithmic_67, @function
ucnv_toAlgorithmic_67:
.LFB2154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %r10
	testq	%r10, %r10
	je	.L1183
	movl	(%r10), %r11d
	testl	%r11d, %r11d
	jg	.L1183
	testq	%rsi, %rsi
	je	.L1185
	testq	%r8, %r8
	je	.L1185
	cmpl	$-1, %r9d
	jl	.L1185
	testl	%ecx, %ecx
	js	.L1185
	jle	.L1186
	testq	%rdx, %rdx
	je	.L1185
.L1186:
	pushq	%r10
	pushq	%r9
	movq	%r8, %r9
	movl	%ecx, %r8d
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movl	%edi, %esi
	movl	$1, %edi
	call	_ZL23ucnv_convertAlgorithmica14UConverterTypeP10UConverterPciPKciP10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
.L1183:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1185:
	.cfi_restore_state
	movl	$1, (%r10)
	xorl	%eax, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2154:
	.size	ucnv_toAlgorithmic_67, .-ucnv_toAlgorithmic_67
	.p2align 4
	.globl	ucnv_fromAlgorithmic_67
	.type	ucnv_fromAlgorithmic_67, @function
ucnv_fromAlgorithmic_67:
.LFB2155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %r10
	testq	%r10, %r10
	je	.L1196
	movl	(%r10), %r11d
	testl	%r11d, %r11d
	jg	.L1196
	testq	%rdi, %rdi
	je	.L1198
	testq	%r8, %r8
	je	.L1198
	cmpl	$-1, %r9d
	jl	.L1198
	testl	%ecx, %ecx
	js	.L1198
	jle	.L1199
	testq	%rdx, %rdx
	je	.L1198
.L1199:
	pushq	%r10
	pushq	%r9
	movq	%r8, %r9
	movl	%ecx, %r8d
	movq	%rdx, %rcx
	movq	%rdi, %rdx
	xorl	%edi, %edi
	call	_ZL23ucnv_convertAlgorithmica14UConverterTypeP10UConverterPciPKciP10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
.L1196:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1198:
	.cfi_restore_state
	movl	$1, (%r10)
	xorl	%eax, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2155:
	.size	ucnv_fromAlgorithmic_67, .-ucnv_fromAlgorithmic_67
	.p2align 4
	.globl	ucnv_getType_67
	.type	ucnv_getType_67, @function
ucnv_getType_67:
.LFB2156:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	16(%rax), %rax
	movsbl	69(%rax), %eax
	cmpb	$2, %al
	je	.L1211
	ret
	.p2align 4,,10
	.p2align 3
.L1211:
	jmp	ucnv_MBCSGetType_67@PLT
	.cfi_endproc
.LFE2156:
	.size	ucnv_getType_67, .-ucnv_getType_67
	.p2align 4
	.globl	ucnv_getStarters_67
	.type	ucnv_getStarters_67, @function
ucnv_getStarters_67:
.LFB2157:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L1212
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L1212
	movq	48(%rdi), %rax
	movq	32(%rax), %rax
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L1214
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1214:
	movl	$1, (%rdx)
.L1212:
	ret
	.cfi_endproc
.LFE2157:
	.size	ucnv_getStarters_67, .-ucnv_getStarters_67
	.section	.rodata.str1.1
.LC2:
	.string	"ibm-897_P100-1995"
.LC3:
	.string	"ibm-942_P120-1999"
.LC4:
	.string	"ibm-943_P130-1999"
.LC5:
	.string	"ibm-946_P100-1995"
.LC6:
	.string	"ibm-33722_P120-1999"
.LC7:
	.string	"ibm-1041_P100-1995"
.LC8:
	.string	"ibm-944_P100-1995"
.LC9:
	.string	"ibm-949_P110-1999"
.LC10:
	.string	"ibm-1363_P110-1997"
.LC11:
	.string	"ISO_2022,locale=ko,version=0"
.LC12:
	.string	"ibm-1088_P100-1995"
	.text
	.p2align 4
	.globl	ucnv_fixFileSeparator_67
	.type	ucnv_fixFileSeparator_67, @function
ucnv_fixFileSeparator_67:
.LFB2159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testq	%rsi, %rsi
	sete	%dl
	testl	%r12d, %r12d
	setle	%al
	orb	%al, %dl
	jne	.L1218
	movq	%rdi, %r13
	testq	%rdi, %rdi
	je	.L1218
	movq	48(%rdi), %rax
	movq	%rsi, %rbx
	movq	32(%rax), %rdx
	movq	96(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1221
	call	*%rdx
	testq	%rax, %rax
	je	.L1243
.L1222:
	movl	$18, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	movsbq	%dl, %rdx
	testl	%edx, %edx
	je	.L1223
	movl	$18, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1227
	movl	$18, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1228
	movl	$18, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1229
	movl	$20, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1230
	movl	$19, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1231
	movl	$18, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1232
	movl	$18, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1233
	movl	$19, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1234
	movl	$29, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1235
	movq	%rax, %rsi
	movl	$19, %ecx
	movl	$10, %edx
	leaq	.LC12(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1223
.L1218:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1235:
	.cfi_restore_state
	movl	$9, %edx
	.p2align 4,,10
	.p2align 3
.L1223:
	leaq	_ZL19ambiguousConverters(%rip), %rax
	salq	$4, %rdx
	movzwl	8(%rax,%rdx), %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1226:
	cmpw	%dx, (%rbx,%rax,2)
	jne	.L1225
	movl	$92, %ecx
	movw	%cx, (%rbx,%rax,2)
.L1225:
	addq	$1, %rax
	cmpl	%eax, %r12d
	jg	.L1226
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore_state
	movq	48(%r13), %rax
.L1221:
	movq	16(%rax), %rax
	addq	$4, %rax
	jmp	.L1222
.L1227:
	movl	$1, %edx
	jmp	.L1223
.L1228:
	movl	$2, %edx
	jmp	.L1223
.L1229:
	movl	$3, %edx
	jmp	.L1223
.L1230:
	movl	$4, %edx
	jmp	.L1223
.L1231:
	movl	$5, %edx
	jmp	.L1223
.L1232:
	movl	$6, %edx
	jmp	.L1223
.L1233:
	movl	$7, %edx
	jmp	.L1223
.L1234:
	movl	$8, %edx
	jmp	.L1223
	.cfi_endproc
.LFE2159:
	.size	ucnv_fixFileSeparator_67, .-ucnv_fixFileSeparator_67
	.p2align 4
	.globl	ucnv_isAmbiguous_67
	.type	ucnv_isAmbiguous_67, @function
ucnv_isAmbiguous_67:
.LFB2160:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1266
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rax
	movq	32(%rax), %rdx
	movq	96(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1247
	call	*%rdx
	testq	%rax, %rax
	je	.L1267
.L1248:
	movl	$18, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1259
	movl	$18, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1259
	movl	$18, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1259
	movl	$18, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1259
	movl	$20, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1259
	movl	$19, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1259
	movl	$18, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1259
	movl	$18, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1259
	movl	$19, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1259
	movl	$29, %ecx
	leaq	.LC11(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1259
	movq	%rax, %rsi
	movl	$19, %ecx
	leaq	.LC12(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1259
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1267:
	.cfi_restore_state
	movq	48(%rbx), %rax
.L1247:
	movq	16(%rax), %rax
	addq	$4, %rax
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1259:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1266:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2160:
	.size	ucnv_isAmbiguous_67, .-ucnv_isAmbiguous_67
	.p2align 4
	.globl	ucnv_setFallback_67
	.type	ucnv_setFallback_67, @function
ucnv_setFallback_67:
.LFB2161:
	.cfi_startproc
	endbr64
	movb	%sil, 63(%rdi)
	ret
	.cfi_endproc
.LFE2161:
	.size	ucnv_setFallback_67, .-ucnv_setFallback_67
	.p2align 4
	.globl	ucnv_usesFallback_67
	.type	ucnv_usesFallback_67, @function
ucnv_usesFallback_67:
.LFB2162:
	.cfi_startproc
	endbr64
	movzbl	63(%rdi), %eax
	ret
	.cfi_endproc
.LFE2162:
	.size	ucnv_usesFallback_67, .-ucnv_usesFallback_67
	.p2align 4
	.globl	ucnv_getInvalidChars_67
	.type	ucnv_getInvalidChars_67, @function
ucnv_getInvalidChars_67:
.LFB2163:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L1270
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1270
	testq	%rsi, %rsi
	sete	%r8b
	testq	%rdi, %rdi
	sete	%al
	orb	%al, %r8b
	jne	.L1281
	testq	%rdx, %rdx
	je	.L1281
	movzbl	90(%rdi), %eax
	cmpb	%al, (%rdx)
	jl	.L1285
	movb	%al, (%rdx)
	testb	%al, %al
	jle	.L1270
	movsbq	%al, %rdx
	leaq	96(%rdi), %rcx
	cmpl	$8, %edx
	jnb	.L1277
	testb	$4, %dl
	jne	.L1286
	testl	%edx, %edx
	je	.L1270
	movzbl	96(%rdi), %eax
	movb	%al, (%rsi)
	testb	$2, %dl
	je	.L1270
	movl	%edx, %eax
	movzwl	-2(%rcx,%rax), %edx
	movw	%dx, -2(%rsi,%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	96(%rdi), %rax
	movq	%rax, (%rsi)
	movl	%edx, %eax
	movq	-8(%rcx,%rax), %rdi
	movq	%rdi, -8(%rsi,%rax)
	leaq	8(%rsi), %rdi
	movq	%rsi, %rax
	movq	%rcx, %rsi
	andq	$-8, %rdi
	subq	%rdi, %rax
	subq	%rax, %rsi
	addl	%edx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
.L1270:
	ret
	.p2align 4,,10
	.p2align 3
.L1281:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1285:
	movl	$8, (%rcx)
	ret
.L1286:
	movl	96(%rdi), %eax
	movl	%eax, (%rsi)
	movl	%edx, %eax
	movl	-4(%rcx,%rax), %edx
	movl	%edx, -4(%rsi,%rax)
	ret
	.cfi_endproc
.LFE2163:
	.size	ucnv_getInvalidChars_67, .-ucnv_getInvalidChars_67
	.p2align 4
	.globl	ucnv_getInvalidUChars_67
	.type	ucnv_getInvalidUChars_67, @function
ucnv_getInvalidUChars_67:
.LFB2164:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	testq	%rcx, %rcx
	je	.L1287
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L1287
	testq	%rsi, %rsi
	sete	%sil
	testq	%rdi, %rdi
	sete	%al
	orb	%al, %sil
	jne	.L1294
	testq	%rdx, %rdx
	je	.L1294
	movzbl	92(%rdi), %eax
	cmpb	%al, (%rdx)
	jl	.L1295
	movb	%al, (%rdx)
	testb	%al, %al
	jle	.L1287
	leaq	140(%rdi), %rsi
	movsbl	%al, %edx
	movq	%r8, %rdi
	jmp	u_memcpy_67@PLT
	.p2align 4,,10
	.p2align 3
.L1287:
	ret
	.p2align 4,,10
	.p2align 3
.L1294:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1295:
	movl	$8, (%rcx)
	ret
	.cfi_endproc
.LFE2164:
	.size	ucnv_getInvalidUChars_67, .-ucnv_getInvalidUChars_67
	.section	.rodata.str1.1
.LC13:
	.string	"UTF-16BE"
.LC14:
	.string	"UTF-32LE"
.LC15:
	.string	"UTF-16LE"
.LC16:
	.string	"UTF-8"
.LC17:
	.string	"UTF-32BE"
.LC18:
	.string	"SCSU"
.LC19:
	.string	"BOCU-1"
.LC20:
	.string	"UTF-7"
.LC21:
	.string	"UTF-EBCDIC"
	.text
	.p2align 4
	.globl	ucnv_detectUnicodeSignature_67
	.type	ucnv_detectUnicodeSignature_67, @function
ucnv_detectUnicodeSignature_67:
.LFB2165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$-1515870811, -29(%rbp)
	movb	$-91, -25(%rbp)
	testq	%rcx, %rcx
	je	.L1316
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L1296
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L1318
	cmpl	$-1, %esi
	jl	.L1318
	movq	%rdx, %r12
	leaq	-36(%rbp), %rax
	testq	%rdx, %rdx
	cmove	%rax, %r12
	cmpl	$-1, %esi
	je	.L1323
	testl	%esi, %esi
	jle	.L1302
.L1330:
	movzbl	(%rbx), %eax
	cmpl	$1, %esi
	jle	.L1303
	movzbl	1(%rbx), %edx
	movb	%dl, -28(%rbp)
	cmpl	$2, %esi
	jle	.L1303
	movzbl	2(%rbx), %edx
	movb	%dl, -27(%rbp)
	cmpl	$3, %esi
	jle	.L1303
	movzbl	3(%rbx), %edx
	movb	%dl, -26(%rbp)
	cmpl	$4, %esi
	jle	.L1303
	movzbl	4(%rbx), %edx
	movb	%dl, -25(%rbp)
.L1303:
	cmpb	$-2, %al
	je	.L1324
	cmpb	$-1, %al
	je	.L1325
	cmpb	$-17, %al
	je	.L1326
	testb	%al, %al
	jne	.L1308
	cmpb	$0, -28(%rbp)
	jne	.L1302
	cmpb	$-2, -27(%rbp)
	je	.L1327
	.p2align 4,,10
	.p2align 3
.L1302:
	movl	$0, (%r12)
	xorl	%eax, %eax
.L1296:
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1328
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1308:
	.cfi_restore_state
	cmpb	$14, %al
	jne	.L1309
	cmpb	$-2, -28(%rbp)
	jne	.L1302
	cmpb	$-1, -27(%rbp)
	jne	.L1302
	movl	$3, (%r12)
	leaq	.LC18(%rip), %rax
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1309:
	cmpb	$-5, %al
	jne	.L1310
	cmpb	$-18, -28(%rbp)
	jne	.L1302
	cmpb	$40, -27(%rbp)
	jne	.L1302
	movl	$3, (%r12)
	leaq	.LC19(%rip), %rax
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1310:
	cmpb	$43, %al
	jne	.L1311
	cmpb	$47, -28(%rbp)
	jne	.L1302
	cmpb	$118, -27(%rbp)
	jne	.L1302
	movzbl	-26(%rbp), %eax
	cmpb	$56, %al
	je	.L1329
	leal	-56(%rax), %edx
	cmpb	$1, %dl
	jbe	.L1313
	andl	$-5, %eax
	cmpb	$43, %al
	jne	.L1302
.L1313:
	movl	$4, (%r12)
	leaq	.LC20(%rip), %rax
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1311:
	cmpb	$-35, %al
	jne	.L1302
	cmpb	$115, -28(%rbp)
	jne	.L1302
	cmpb	$102, -27(%rbp)
	jne	.L1302
	cmpb	$115, -26(%rbp)
	jne	.L1302
	movl	$4, (%r12)
	leaq	.LC21(%rip), %rax
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1323:
	call	strlen@PLT
	movl	%eax, %esi
	testl	%esi, %esi
	jg	.L1330
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1326:
	cmpb	$-69, -28(%rbp)
	jne	.L1302
	cmpb	$-65, -27(%rbp)
	jne	.L1302
	movl	$3, (%r12)
	leaq	.LC16(%rip), %rax
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1324:
	cmpb	$-1, -28(%rbp)
	jne	.L1302
	movl	$2, (%r12)
	leaq	.LC13(%rip), %rax
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1325:
	cmpb	$-2, -28(%rbp)
	jne	.L1302
	movzbl	-27(%rbp), %eax
	orb	-26(%rbp), %al
	jne	.L1306
	movl	$4, (%r12)
	leaq	.LC14(%rip), %rax
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1318:
	movl	$1, (%rcx)
	xorl	%eax, %eax
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1316:
	xorl	%eax, %eax
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1327:
	cmpb	$-1, -26(%rbp)
	jne	.L1302
	movl	$4, (%r12)
	leaq	.LC17(%rip), %rax
	jmp	.L1296
.L1306:
	movl	$2, (%r12)
	leaq	.LC15(%rip), %rax
	jmp	.L1296
.L1329:
	cmpb	$45, -25(%rbp)
	jne	.L1313
	movl	$5, (%r12)
	leaq	.LC20(%rip), %rax
	jmp	.L1296
.L1328:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2165:
	.size	ucnv_detectUnicodeSignature_67, .-ucnv_detectUnicodeSignature_67
	.p2align 4
	.globl	ucnv_fromUCountPending_67
	.type	ucnv_fromUCountPending_67, @function
ucnv_fromUCountPending_67:
.LFB2166:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1338
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L1338
	testq	%rdi, %rdi
	je	.L1340
	movl	208(%rdi), %edx
	movsbl	281(%rdi), %eax
	testl	%edx, %edx
	jns	.L1341
	testb	%al, %al
	js	.L1342
	movl	84(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setg	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1341:
	cmpl	$65535, %edx
	setg	%dl
	movzbl	%dl, %edx
	leal	1(%rdx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1342:
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L1338:
	movl	$-1, %eax
	ret
.L1340:
	movl	$1, (%rsi)
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2166:
	.size	ucnv_fromUCountPending_67, .-ucnv_fromUCountPending_67
	.p2align 4
	.globl	ucnv_toUCountPending_67
	.type	ucnv_toUCountPending_67, @function
ucnv_toUCountPending_67:
.LFB2167:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1349
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L1349
	testq	%rdi, %rdi
	je	.L1351
	movsbl	282(%rdi), %eax
	testb	%al, %al
	jg	.L1343
	jne	.L1352
	movsbl	64(%rdi), %eax
	movl	$0, %edx
	testb	%al, %al
	cmovle	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1349:
	movl	$-1, %eax
.L1343:
	ret
	.p2align 4,,10
	.p2align 3
.L1352:
	negl	%eax
	ret
.L1351:
	movl	$1, (%rsi)
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2167:
	.size	ucnv_toUCountPending_67, .-ucnv_toUCountPending_67
	.p2align 4
	.globl	ucnv_isFixedWidth_67
	.type	ucnv_isFixedWidth_67, @function
ucnv_isFixedWidth_67:
.LFB2168:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L1361
	testq	%rdi, %rdi
	je	.L1363
	movq	48(%rdi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rax), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movsbl	69(%rax), %ecx
	cmpb	$2, %cl
	je	.L1364
.L1357:
	cmpl	$30, %ecx
	ja	.L1359
	movl	$1, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	salq	%cl, %rdx
	testl	$1140851075, %edx
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1363:
	.cfi_restore 6
	movl	$1, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L1361:
	ret
	.p2align 4,,10
	.p2align 3
.L1364:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	ucnv_MBCSGetType_67@PLT
	movl	%eax, %ecx
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1359:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2168:
	.size	ucnv_isFixedWidth_67, .-ucnv_isFixedWidth_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL19ambiguousConverters, @object
	.size	_ZL19ambiguousConverters, 176
_ZL19ambiguousConverters:
	.quad	.LC2
	.value	165
	.zero	6
	.quad	.LC3
	.value	165
	.zero	6
	.quad	.LC4
	.value	165
	.zero	6
	.quad	.LC5
	.value	165
	.zero	6
	.quad	.LC6
	.value	165
	.zero	6
	.quad	.LC7
	.value	165
	.zero	6
	.quad	.LC8
	.value	8361
	.zero	6
	.quad	.LC9
	.value	8361
	.zero	6
	.quad	.LC10
	.value	8361
	.zero	6
	.quad	.LC11
	.value	8361
	.zero	6
	.quad	.LC12
	.value	8361
	.zero	6
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
