	.file	"locbased.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711LocaleBased9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_6711LocaleBased9getLocaleE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_6711LocaleBased9getLocaleE18ULocDataLocaleTypeR10UErrorCode:
.LFB2324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	(%rcx), %edi
	testl	%edi, %edi
	jg	.L2
	testl	%edx, %edx
	je	.L3
	cmpl	$1, %edx
	jne	.L4
	movq	(%rax), %rsi
.L5:
	testq	%rsi, %rsi
	leaq	.LC0(%rip), %rax
	cmove	%rax, %rsi
.L2:
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movl	$1, (%rcx)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	movq	8(%rax), %rsi
	jmp	.L5
	.cfi_endproc
.LFE2324:
	.size	_ZNK6icu_6711LocaleBased9getLocaleE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_6711LocaleBased9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711LocaleBased11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_6711LocaleBased11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_6711LocaleBased11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode:
.LFB2325:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L9
	testl	%esi, %esi
	je	.L11
	cmpl	$1, %esi
	je	.L14
	movl	$1, (%rdx)
.L9:
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE2325:
	.size	_ZNK6icu_6711LocaleBased11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_6711LocaleBased11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711LocaleBased12setLocaleIDsEPKcS2_
	.type	_ZN6icu_6711LocaleBased12setLocaleIDsEPKcS2_, @function
_ZN6icu_6711LocaleBased12setLocaleIDsEPKcS2_:
.LFB2326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L16
	movq	(%rdi), %rdi
	movl	$157, %edx
	call	strncpy@PLT
	movq	(%rbx), %rax
	movb	$0, 156(%rax)
.L16:
	testq	%r12, %r12
	je	.L15
	movq	8(%rbx), %rdi
	movl	$157, %edx
	movq	%r12, %rsi
	call	strncpy@PLT
	movq	8(%rbx), %rax
	movb	$0, 156(%rax)
.L15:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2326:
	.size	_ZN6icu_6711LocaleBased12setLocaleIDsEPKcS2_, .-_ZN6icu_6711LocaleBased12setLocaleIDsEPKcS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711LocaleBased12setLocaleIDsERKNS_6LocaleES3_
	.type	_ZN6icu_6711LocaleBased12setLocaleIDsERKNS_6LocaleES3_, @function
_ZN6icu_6711LocaleBased12setLocaleIDsERKNS_6LocaleES3_:
.LFB2327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	40(%rsi), %rsi
	movq	(%rdi), %rdi
	call	strcpy@PLT
	movq	40(%r12), %rsi
	movq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	strcpy@PLT
	.cfi_endproc
.LFE2327:
	.size	_ZN6icu_6711LocaleBased12setLocaleIDsERKNS_6LocaleES3_, .-_ZN6icu_6711LocaleBased12setLocaleIDsERKNS_6LocaleES3_
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
