	.file	"unames.cpp"
	.text
	.p2align 4
	.type	_ZN6icu_67L12isAcceptableEPvPKcS2_PK9UDataInfo, @function
_ZN6icu_67L12isAcceptableEPvPKcS2_PK9UDataInfo:
.LFB2768:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpw	$19, (%rcx)
	jbe	.L1
	cmpw	$0, 4(%rcx)
	jne	.L1
	cmpw	$28277, 8(%rcx)
	je	.L8
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpw	$28001, 10(%rcx)
	jne	.L1
	cmpb	$1, 12(%rcx)
	sete	%al
	ret
	.cfi_endproc
.LFE2768:
	.size	_ZN6icu_67L12isAcceptableEPvPKcS2_PK9UDataInfo, .-_ZN6icu_67L12isAcceptableEPvPKcS2_PK9UDataInfo
	.p2align 4
	.type	_ZN6icu_67L10expandNameEPNS_10UCharNamesEPKht15UCharNameChoicePct, @function
_ZN6icu_67L10expandNameEPNS_10UCharNamesEPKht15UCharNameChoicePct:
.LFB2771:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	18(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	andl	$-3, %ecx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzwl	16(%rdi), %eax
	movw	%ax, -42(%rbp)
	movl	(%rdi), %eax
	movq	%rax, -56(%rbp)
	jne	.L10
	movl	%edx, %r10d
.L19:
	testw	%r10w, %r10w
	je	.L30
	movzwl	-42(%rbp), %r12d
	xorl	%eax, %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L71:
	cmpb	$59, %cl
	je	.L11
	testw	%r9w, %r9w
	je	.L21
	movb	%cl, (%r8)
	subl	$1, %r9d
	addq	$1, %r8
.L21:
	addl	$1, %eax
	movl	%ebx, %r10d
	movq	%r11, %rsi
.L22:
	testw	%r10w, %r10w
	je	.L11
.L12:
	movzbl	(%rsi), %ecx
	leal	-1(%r10), %ebx
	leaq	1(%rsi), %r11
	movl	%ecx, %edx
	cmpl	%r12d, %ecx
	jge	.L71
	movzbl	%cl, %ecx
	movzwl	(%r14,%rcx,2), %ecx
	cmpw	$-2, %cx
	je	.L72
	movl	%ebx, %r10d
	movq	%r11, %rsi
.L23:
	cmpw	$-1, %cx
	jne	.L24
	cmpb	$59, %dl
	je	.L25
	testw	%r9w, %r9w
	je	.L26
	movb	%dl, (%r8)
	subl	$1, %r9d
	addq	$1, %r8
.L26:
	addl	$1, %eax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L24:
	addq	-56(%rbp), %rcx
	addq	%rdi, %rcx
	leaq	1(%rcx), %r15
	movzbl	(%rcx), %ecx
	testb	%cl, %cl
	je	.L22
	leal	1(%rax), %ebx
	movq	%r15, %rdx
	subl	%r15d, %ebx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L64:
	movb	%cl, (%r8)
	leaq	1(%r8), %r11
	leal	(%rbx,%rdx), %r8d
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	subl	$1, %r9d
	testb	%cl, %cl
	je	.L73
	movq	%r11, %r8
.L28:
	testw	%r9w, %r9w
	jne	.L64
	leal	1(%rax), %ecx
	subl	%r15d, %ecx
	.p2align 4,,10
	.p2align 3
.L27:
	leal	(%rcx,%rdx), %eax
	addq	$1, %rdx
	cmpb	$0, -1(%rdx)
	jne	.L27
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L72:
	movzwl	(%rsi), %ecx
	subl	$2, %r10d
	addq	$2, %rsi
	rolw	$8, %cx
	movzwl	%cx, %ecx
	movzwl	(%r14,%rcx,2), %ecx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L25:
	testw	%ax, %ax
	jne	.L11
	cmpl	$2, %r13d
	jne	.L11
	cmpw	$59, -42(%rbp)
	jbe	.L36
	cmpw	$-1, 136(%rdi)
	je	.L36
.L11:
	testw	%r9w, %r9w
	je	.L9
	movb	$0, (%r8)
.L9:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movl	%r8d, %eax
	movq	%r11, %r8
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L10:
	cmpw	$59, -42(%rbp)
	ja	.L74
.L13:
	cmpl	$4, %r13d
	movl	$2, %r10d
	movl	%edx, %eax
	cmovne	%r13d, %r10d
	leal	-1(%r10), %r11d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rcx, %rsi
.L15:
	movl	%r11d, %edx
	testw	%ax, %ax
	je	.L75
	subl	$1, %eax
	cmpb	$59, (%rsi)
	leaq	1(%rsi), %rcx
	jne	.L18
	movq	%rcx, %rsi
.L17:
	testl	%edx, %edx
	jle	.L76
	movl	%edx, %r10d
	movq	%rsi, %rcx
	leal	-1(%rdx), %r11d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%eax, %eax
	cmpw	$-1, 136(%rdi)
	jne	.L11
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L75:
	testl	%r11d, %r11d
	jle	.L11
	leal	-2(%r10), %edx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%eax, %eax
	jmp	.L22
.L76:
	movl	%eax, %r10d
	jmp	.L19
.L30:
	xorl	%eax, %eax
	jmp	.L11
	.cfi_endproc
.LFE2771:
	.size	_ZN6icu_67L10expandNameEPNS_10UCharNamesEPKht15UCharNameChoicePct, .-_ZN6icu_67L10expandNameEPNS_10UCharNamesEPKht15UCharNameChoicePct
	.p2align 4
	.type	_ZN6icu_67L18expandGroupLengthsEPKhPtS2_, @function
_ZN6icu_67L18expandGroupLengthsEPKhPtS2_:
.LFB2777:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L87:
	movzbl	%r8b, %r12d
	sall	$4, %ecx
	andl	$15, %r8d
	andl	$48, %ecx
	sarl	$4, %r12d
	orl	%r12d, %ecx
	addl	$12, %ecx
.L81:
	movw	%r9w, (%rsi)
	addl	%ecx, %r9d
	movw	%cx, (%rdx)
	movzbl	%r8b, %ecx
	cmpb	$11, %r8b
	jbe	.L84
	movl	%ebx, %edi
	movq	%r11, %rdx
	movq	%r10, %rsi
.L83:
	cmpw	$31, %di
	ja	.L85
.L78:
	addq	$1, %rax
	leaq	2(%rsi), %r10
	leal	1(%rdi), %ebx
	movzbl	-1(%rax), %r8d
	leaq	2(%rdx), %r11
	cmpw	$11, %cx
	ja	.L87
	cmpb	$-65, %r8b
	ja	.L88
	movl	%r8d, %ecx
	andl	$15, %r8d
	shrl	$4, %ecx
	andl	$15, %ecx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L88:
	andl	$63, %r8d
	movl	%ebx, %edi
	movw	%r9w, (%rsi)
	xorl	%ecx, %ecx
	addl	$12, %r8d
	movq	%r10, %rsi
	movw	%r8w, (%rdx)
	addl	%r8d, %r9d
	movq	%r11, %rdx
	cmpw	$31, %di
	jbe	.L78
.L85:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movw	%r9w, 2(%rsi)
	addl	$2, %edi
	addl	%ecx, %r9d
	addq	$4, %rdx
	movw	%cx, -2(%rdx)
	addq	$4, %rsi
	jmp	.L83
	.cfi_endproc
.LFE2777:
	.size	_ZN6icu_67L18expandGroupLengthsEPKhPtS2_, .-_ZN6icu_67L18expandGroupLengthsEPKhPtS2_
	.p2align 4
	.type	_ZN6icu_67L17writeFactorSuffixEPKttPKcjPtPS3_S5_Pct, @function
_ZN6icu_67L17writeFactorSuffixEPKttPKcjPtPS3_S5_Pct:
.LFB2783:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%r9, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	movq	16(%rbp), %r11
	movzwl	32(%rbp), %ecx
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	movq	%rdx, %rsi
	movl	%r12d, %r13d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	24(%rbp), %rdi
	subw	$1, %r13w
	je	.L136
	movzwl	%r13w, %r13d
	leal	-2(%r12), %edx
	movzwl	%dx, %edx
	movq	%r13, %r14
	leaq	(%r13,%r13), %r9
	subq	%rdx, %r14
	addq	%r14, %r14
	.p2align 4,,10
	.p2align 3
.L91:
	movzwl	(%rbx,%r9), %r12d
	xorl	%edx, %edx
	divl	%r12d
	movw	%dx, (%r8,%r9)
	movq	%r9, %rdx
	subq	$2, %r9
	cmpq	%r14, %rdx
	jne	.L91
.L90:
	movw	%ax, (%r8)
	movq	%r8, %r9
	leaq	(%rbx,%r13,2), %r12
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L105:
	testq	%r10, %r10
	je	.L92
	movq	%rsi, (%r10)
	addq	$8, %r10
.L92:
	movzwl	(%r9), %eax
	testw	%ax, %ax
	je	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$1, %rsi
	cmpb	$0, -1(%rsi)
	jne	.L94
	subw	$1, %ax
	jne	.L94
.L93:
	testq	%r11, %r11
	je	.L96
	movq	%rsi, (%r11)
	addq	$8, %r11
.L96:
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %r14
	movq	%r14, %rsi
	testb	%al, %al
	je	.L97
	leal	1(%r8), %r13d
	subl	%r14d, %r13d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L134:
	movb	%al, (%rdi)
	leaq	1(%rdi), %rdx
	leal	0(%r13,%rsi), %edi
	movzbl	(%rsi), %eax
	addq	$1, %rsi
	subl	$1, %ecx
	testb	%al, %al
	je	.L137
	movq	%rdx, %rdi
.L99:
	testw	%cx, %cx
	jne	.L134
	leal	1(%r8), %eax
	subl	%r14d, %eax
	.p2align 4,,10
	.p2align 3
.L98:
	leal	(%rax,%rsi), %r8d
	addq	$1, %rsi
	cmpb	$0, -1(%rsi)
	jne	.L98
.L97:
	cmpq	%rbx, %r12
	je	.L101
.L138:
	movzwl	(%rbx), %eax
	subl	$1, %eax
	subw	(%r9), %ax
	je	.L102
	.p2align 4,,10
	.p2align 3
.L103:
	addq	$1, %rsi
	cmpb	$0, -1(%rsi)
	jne	.L103
	subw	$1, %ax
	jne	.L103
.L102:
	addq	$2, %rbx
	addq	$2, %r9
	jmp	.L105
.L137:
	movl	%edi, %r8d
	movq	%rdx, %rdi
	cmpq	%rbx, %r12
	jne	.L138
.L101:
	testw	%cx, %cx
	je	.L89
	movb	$0, (%rdi)
.L89:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L136:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L90
	.cfi_endproc
.LFE2783:
	.size	_ZN6icu_67L17writeFactorSuffixEPKttPKcjPtPS3_S5_Pct, .-_ZN6icu_67L17writeFactorSuffixEPKttPKcjPtPS3_S5_Pct
	.p2align 4
	.type	_ZN6icu_67L14unames_cleanupEv, @function
_ZN6icu_67L14unames_cleanupEv:
.LFB2767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN6icu_67L14uCharNamesDataE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L140
	call	udata_close_67@PLT
	movq	$0, _ZN6icu_67L14uCharNamesDataE(%rip)
.L140:
	cmpq	$0, _ZN6icu_67L10uCharNamesE(%rip)
	je	.L141
	movq	$0, _ZN6icu_67L10uCharNamesE(%rip)
.L141:
	movl	$1, %eax
	movl	$0, _ZN6icu_67L18gCharNamesInitOnceE(%rip)
	mfence
	movl	$0, _ZN6icu_67L14gMaxNameLengthE(%rip)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2767:
	.size	_ZN6icu_67L14unames_cleanupEv, .-_ZN6icu_67L14unames_cleanupEv
	.p2align 4
	.type	_ZN6icu_67L10getAlgNameEPNS_16AlgorithmicRangeEj15UCharNameChoicePct.part.0, @function
_ZN6icu_67L10getAlgNameEPNS_16AlgorithmicRangeEj15UCharNameChoicePct.part.0:
.LFB3593:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rdi), %eax
	testb	%al, %al
	je	.L147
	cmpb	$1, %al
	je	.L148
	xorl	%eax, %eax
	testw	%cx, %cx
	jne	.L184
	.p2align 4,,10
	.p2align 3
.L146:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L185
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movzbl	12(%rdi), %r10d
	leaq	13(%rdi), %rbx
	movq	%rbx, %r9
	testb	%r10b, %r10b
	je	.L167
	movl	$1, %ecx
	subl	%ebx, %ecx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L180:
	leal	(%rcx,%r9), %eax
	addq	$1, %r9
	movb	%r10b, (%rdx)
	leaq	1(%rdx), %r11
	movzbl	-1(%r9), %r10d
	subl	$1, %r8d
	testb	%r10b, %r10b
	je	.L150
	movq	%r11, %rdx
.L152:
	testw	%r8w, %r8w
	jne	.L180
	movl	$1, %ecx
	subl	%ebx, %ecx
	.p2align 4,,10
	.p2align 3
.L151:
	leal	(%rcx,%r9), %eax
	addq	$1, %r9
	cmpb	$0, -1(%r9)
	jne	.L151
	movzbl	9(%rdi), %r10d
	movq	%rdx, %r11
.L154:
	testw	%r10w, %r10w
	je	.L160
	leal	-1(%r10), %ecx
	movl	%r10d, %edx
	movzwl	%cx, %ecx
	addq	%r11, %rcx
	.p2align 4,,10
	.p2align 3
.L161:
	subl	$1, %edx
	cmpw	%r8w, %dx
	jnb	.L157
	movl	%esi, %r9d
	andl	$15, %r9d
	leal	48(%r9), %r11d
	leal	55(%r9), %edi
	cmpb	$9, %r9b
	cmovle	%r11d, %edi
	movb	%dil, (%rcx)
.L157:
	shrl	$4, %esi
	subq	$1, %rcx
	testw	%dx, %dx
	jne	.L161
.L160:
	addl	%r10d, %eax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L148:
	movzbl	9(%rdi), %eax
	leaq	12(%rdi), %r11
	movq	%rax, %r12
	leaq	(%r11,%rax,2), %rax
	leaq	1(%rax), %r13
	movzbl	(%rax), %eax
	movq	%r13, %r10
	testb	%al, %al
	je	.L169
	movl	$1, %r9d
	subl	%r13d, %r9d
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L182:
	leal	(%r9,%r10), %ebx
	addq	$1, %r10
	movb	%al, (%rdx)
	leaq	1(%rdx), %rcx
	movzbl	-1(%r10), %eax
	subl	$1, %r8d
	movq	%rcx, %rdx
	testb	%al, %al
	je	.L162
.L164:
	testw	%r8w, %r8w
	jne	.L182
	movl	$1, %eax
	subl	%r13d, %eax
	.p2align 4,,10
	.p2align 3
.L163:
	leal	(%rax,%r10), %ebx
	addq	$1, %r10
	cmpb	$0, -1(%r10)
	jne	.L163
.L162:
	subq	$8, %rsp
	movzwl	%r8w, %r8d
	subl	(%rdi), %esi
	xorl	%r9d, %r9d
	pushq	%r8
	movl	%esi, %ecx
	leaq	-64(%rbp), %r8
	movzbl	%r12b, %esi
	pushq	%rdx
	movq	%r11, %rdi
	movq	%r10, %rdx
	pushq	$0
	call	_ZN6icu_67L17writeFactorSuffixEPKttPKcjPtPS3_S5_Pct
	addq	$32, %rsp
	addl	%ebx, %eax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L184:
	movb	$0, (%rdx)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%rdx, %r11
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L150:
	movzbl	9(%rdi), %r10d
	cmpw	%r8w, %r10w
	jnb	.L154
	movb	$0, (%r11,%r10)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L169:
	xorl	%ebx, %ebx
	jmp	.L162
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3593:
	.size	_ZN6icu_67L10getAlgNameEPNS_16AlgorithmicRangeEj15UCharNameChoicePct.part.0, .-_ZN6icu_67L10getAlgNameEPNS_16AlgorithmicRangeEj15UCharNameChoicePct.part.0
	.p2align 4
	.type	_ZN6icu_67L12enumAlgNamesEPNS_16AlgorithmicRangeEiiPFaPvi15UCharNameChoicePKciES2_S3_.part.0, @function
_ZN6icu_67L12enumAlgNamesEPNS_16AlgorithmicRangeEiiPFaPvi15UCharNameChoicePKciES2_S3_.part.0:
.LFB3594:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$424, %rsp
	movl	%edx, -412(%rbp)
	movq	%rcx, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	8(%rdi), %eax
	testb	%al, %al
	je	.L187
	cmpb	$1, %al
	jne	.L207
	movzbl	9(%rdi), %eax
	leaq	12(%rdi), %r15
	movq	%rax, %r11
	leaq	(%r15,%rax,2), %rax
	leaq	1(%rax), %rdx
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L219
	leaq	-256(%rbp), %r14
	movl	$1, %ecx
	movq	%r14, %r10
	subl	%edx, %ecx
	.p2align 4,,10
	.p2align 3
.L205:
	leal	(%rcx,%rdx), %esi
	addq	$1, %rdx
	movb	%al, (%r10)
	addq	$1, %r10
	movzbl	-1(%rdx), %eax
	testb	%al, %al
	jne	.L205
	movl	$200, %eax
	movw	%si, -416(%rbp)
	subl	%esi, %eax
	movzwl	%ax, %eax
.L204:
	subq	$8, %rsp
	movl	%ebx, %ecx
	subl	(%rdi), %ecx
	movzbl	%r11b, %esi
	pushq	%rax
	leaq	-320(%rbp), %rax
	leaq	-384(%rbp), %r9
	movq	%r15, %rdi
	pushq	%r10
	leaq	-400(%rbp), %r8
	pushq	%rax
	movb	%r11b, -426(%rbp)
	movq	%r10, -424(%rbp)
	movq	%rax, -448(%rbp)
	call	_ZN6icu_67L17writeFactorSuffixEPKttPKcjPtPS3_S5_Pct
	addq	$32, %rsp
	movq	%r14, %rcx
	movl	%r13d, %edx
	movzwl	-416(%rbp), %edi
	movl	%ebx, %esi
	leal	(%rax,%rdi), %r8d
	movq	-408(%rbp), %rax
	movq	%r12, %rdi
	movzwl	%r8w, %r8d
	call	*%rax
	movq	-424(%rbp), %r10
	movzbl	-426(%rbp), %r11d
	testb	%al, %al
	je	.L215
	movzbl	%r11b, %eax
	movq	%r10, -456(%rbp)
	leal	-1(%rax), %edi
	movw	%ax, -424(%rbp)
	movzwl	%di, %eax
	movw	%di, -426(%rbp)
	leaq	(%r15,%rax,2), %rsi
	leaq	-312(%rbp,%rax,8), %r9
	movzwl	%di, %eax
	movq	%r12, -464(%rbp)
	movq	%rsi, -440(%rbp)
	movl	%ebx, %r12d
	movq	%r9, %rbx
	movl	%eax, -432(%rbp)
	.p2align 4,,10
	.p2align 3
.L216:
	addl	$1, %r12d
	cmpl	%r12d, -412(%rbp)
	jle	.L207
	movslq	-432(%rbp), %rax
	movzwl	-426(%rbp), %ecx
	movzwl	-400(%rbp,%rax,2), %edi
	leal	1(%rdi), %edx
	movq	-440(%rbp), %rdi
	cmpw	(%rdi), %dx
	jb	.L212
	.p2align 4,,10
	.p2align 3
.L208:
	xorl	%edx, %edx
	subl	$1, %ecx
	movw	%dx, -400(%rbp,%rax,2)
	movq	-384(%rbp,%rax,8), %rdx
	movq	%rdx, -320(%rbp,%rax,8)
	movzwl	%cx, %eax
	movzwl	-400(%rbp,%rax,2), %edi
	leal	1(%rdi), %edx
	cmpw	(%r15,%rax,2), %dx
	jnb	.L208
.L212:
	movw	%dx, -400(%rbp,%rax,2)
	movq	-320(%rbp,%rax,8), %rdx
	.p2align 4,,10
	.p2align 3
.L209:
	addq	$1, %rdx
	cmpb	$0, -1(%rdx)
	jne	.L209
	cmpw	$0, -424(%rbp)
	movq	%rdx, -320(%rbp,%rax,8)
	je	.L221
	movq	-448(%rbp), %r11
	movq	-456(%rbp), %r10
	movzwl	-416(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L211:
	movq	(%r11), %rax
	movzbl	(%rax), %ecx
	leaq	1(%rax), %rdx
	testb	%cl, %cl
	je	.L213
	movq	%r10, %rax
	.p2align 4,,10
	.p2align 3
.L214:
	addq	$1, %rdx
	addq	$1, %rax
	movb	%cl, -1(%rax)
	movzbl	-1(%rdx), %ecx
	leal	(%r8,%rax), %esi
	subl	%r10d, %esi
	testb	%cl, %cl
	jne	.L214
	movzwl	%si, %r8d
	movq	%rax, %r10
.L213:
	addq	$8, %r11
	cmpq	%r11, %rbx
	jne	.L211
.L210:
	movb	$0, (%r10)
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	-464(%rbp), %rdi
	movq	-408(%rbp), %rax
	call	*%rax
	testb	%al, %al
	jne	.L216
.L215:
	xorl	%eax, %eax
	jmp	.L186
.L207:
	movl	$1, %eax
.L186:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L242
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L187:
	.cfi_restore_state
	leaq	-256(%rbp), %r14
	movl	$200, %ecx
	movq	%r14, %rdx
	call	_ZN6icu_67L10getAlgNameEPNS_16AlgorithmicRangeEj15UCharNameChoicePct.part.0
	movzwl	%ax, %r8d
	testw	%r8w, %r8w
	je	.L207
	movl	%r8d, -416(%rbp)
	movq	%r14, %rcx
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	-408(%rbp), %rax
	movq	%r12, %rdi
	call	*%rax
	movl	-416(%rbp), %r8d
	testb	%al, %al
	je	.L215
	cmpb	$0, -256(%rbp)
	movq	%r14, %r15
	je	.L218
.L193:
	addq	$1, %r15
	cmpb	$0, (%r15)
	jne	.L193
	movq	%r14, %rax
	movl	%r13d, %r14d
	movq	%r12, %r13
	movq	%r15, %r12
	movq	%rax, %r15
.L203:
	addl	$1, %ebx
	cmpl	-412(%rbp), %ebx
	jge	.L207
	movzbl	-1(%r12), %eax
	leaq	-1(%r12), %rdx
	leal	-65(%rax), %ecx
	cmpb	$4, %cl
	jbe	.L195
	leal	-48(%rax), %ecx
	cmpb	$8, %cl
	ja	.L196
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L199:
	cmpb	$70, %al
	jne	.L241
	movb	$48, (%rdx)
.L241:
	movzbl	-1(%rdx), %eax
	subq	$1, %rdx
	leal	-48(%rax), %ecx
	cmpb	$8, %cl
	jbe	.L195
	leal	-65(%rax), %ecx
	cmpb	$4, %cl
	jbe	.L195
.L196:
	cmpb	$57, %al
	jne	.L199
	movb	$65, (%rdx)
.L198:
	movl	%r8d, -416(%rbp)
	movq	%r15, %rcx
	movl	%r14d, %edx
	movl	%ebx, %esi
	movq	-408(%rbp), %rax
	movq	%r13, %rdi
	call	*%rax
	movl	-416(%rbp), %r8d
	testb	%al, %al
	jne	.L203
	xorl	%eax, %eax
	jmp	.L186
.L221:
	movq	-456(%rbp), %r10
	movzwl	-416(%rbp), %r8d
	jmp	.L210
.L195:
	addl	$1, %eax
	movb	%al, (%rdx)
	jmp	.L198
.L219:
	xorl	%ecx, %ecx
	leaq	-256(%rbp), %r14
	movl	$200, %eax
	movw	%cx, -416(%rbp)
	movq	%r14, %r10
	jmp	.L204
.L218:
	movl	%r13d, %r14d
	movq	%r12, %r13
	movq	%r15, %r12
	jmp	.L203
.L242:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3594:
	.size	_ZN6icu_67L12enumAlgNamesEPNS_16AlgorithmicRangeEiiPFaPvi15UCharNameChoicePKciES2_S3_.part.0, .-_ZN6icu_67L12enumAlgNamesEPNS_16AlgorithmicRangeEiiPFaPvi15UCharNameChoicePKciES2_S3_.part.0
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"unames/makeTokenMap() finds variant character 0x%02x used (input charset family %d)\n"
	.text
	.p2align 4
	.type	_ZL12makeTokenMapPK12UDataSwapperPstPhP10UErrorCode, @function
_ZL12makeTokenMapPK12UDataSwapperPstPhP10UErrorCode:
.LFB2800:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L243
	movzbl	3(%rdi), %eax
	movq	%rdi, %r13
	movq	%rcx, %r12
	cmpb	%al, 1(%rdi)
	jne	.L245
	movdqa	.LC0(%rip), %xmm0
	movaps	%xmm0, (%rcx)
	movdqa	.LC1(%rip), %xmm0
	movaps	%xmm0, 16(%rcx)
	movdqa	.LC2(%rip), %xmm0
	movaps	%xmm0, 32(%rcx)
	movdqa	.LC3(%rip), %xmm0
	movaps	%xmm0, 48(%rcx)
	movdqa	.LC4(%rip), %xmm0
	movaps	%xmm0, 64(%rcx)
	movdqa	.LC5(%rip), %xmm0
	movaps	%xmm0, 80(%rcx)
	movdqa	.LC6(%rip), %xmm0
	movaps	%xmm0, 96(%rcx)
	movdqa	.LC7(%rip), %xmm0
	movaps	%xmm0, 112(%rcx)
	movdqa	.LC8(%rip), %xmm0
	movaps	%xmm0, 128(%rcx)
	movdqa	.LC9(%rip), %xmm0
	movaps	%xmm0, 144(%rcx)
	movdqa	.LC10(%rip), %xmm0
	movaps	%xmm0, 160(%rcx)
	movdqa	.LC11(%rip), %xmm0
	movaps	%xmm0, 176(%rcx)
	movdqa	.LC12(%rip), %xmm0
	movaps	%xmm0, 192(%rcx)
	movdqa	.LC13(%rip), %xmm0
	movaps	%xmm0, 208(%rcx)
	movdqa	.LC14(%rip), %xmm0
	movaps	%xmm0, 224(%rcx)
	movdqa	.LC15(%rip), %xmm0
	movaps	%xmm0, 240(%rcx)
.L243:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L245:
	.cfi_restore_state
	xorl	%eax, %eax
	movq	%rsi, %r9
	movl	$32, %ecx
	movq	%r12, %rdi
	rep stosq
	cmpw	$256, %dx
	movl	$32, %ecx
	leaq	-320(%rbp), %rsi
	movq	%rsi, %rdi
	movl	$256, %r14d
	rep stosq
	cmovbe	%edx, %r14d
	cmpw	$1, %dx
	jbe	.L243
	leaq	-321(%rbp), %rax
	movl	$1, %ebx
	movq	%rax, -360(%rbp)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L246:
	addq	$1, %rbx
	cmpw	%bx, %r14w
	jbe	.L260
.L249:
	cmpw	$-1, (%r9,%rbx,2)
	jne	.L246
	movq	%r9, -352(%rbp)
	movl	$1, %edx
	movq	%r13, %rdi
	movq	-360(%rbp), %rcx
	movq	%r8, -344(%rbp)
	leaq	-322(%rbp), %rsi
	movb	%bl, -322(%rbp)
	call	*72(%r13)
	movq	-344(%rbp), %r8
	movq	-352(%rbp), %r9
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L261
	movzbl	-321(%rbp), %eax
	movzbl	-322(%rbp), %edx
	addq	$1, %rbx
	movb	%al, (%r12,%rdx)
	movb	$1, -320(%rbp,%rax)
	cmpw	%bx, %r14w
	ja	.L249
.L260:
	movl	$1, %ecx
	movl	$1, %eax
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L250:
	addq	$1, %rcx
	cmpw	%cx, %r14w
	jbe	.L243
.L253:
	cmpb	$0, (%r12,%rcx)
	jne	.L250
	movzwl	%ax, %edx
	cmpb	$0, -320(%rbp,%rdx)
	je	.L262
	.p2align 4,,10
	.p2align 3
.L252:
	addl	$1, %eax
	movzwl	%ax, %edx
	cmpb	$0, -320(%rbp,%rdx)
	jne	.L252
.L262:
	movb	%al, (%r12,%rcx)
	addl	$1, %eax
	jmp	.L250
.L261:
	movzbl	1(%r13), %ecx
	movzwl	%bx, %edx
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rsi
	call	udata_printError_67@PLT
	jmp	.L243
.L259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2800:
	.size	_ZL12makeTokenMapPK12UDataSwapperPstPhP10UErrorCode, .-_ZL12makeTokenMapPK12UDataSwapperPstPhP10UErrorCode
	.p2align 4
	.type	_ZN6icu_67L17calcNameSetLengthEPKttPKhPaPjPS3_S3_.constprop.0, @function
_ZN6icu_67L17calcNameSetLengthEPKttPKhPaPjPS3_S3_.constprop.0:
.LFB3601:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	$1, %ebx
	movq	%rdx, -48(%rbp)
	leaq	_ZN6icu_67L8gNameSetE(%rip), %rdx
	movq	(%r8), %rax
	cmpq	%r9, %rax
	jne	.L264
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L291:
	movq	%rcx, %rax
	movl	%ebx, %r14d
	addl	$1, %r12d
	shrq	$3, %rax
	sall	%cl, %r14d
	andl	$28, %eax
	orl	%r14d, (%rdx,%rax)
	movq	%r10, %rax
.L267:
	cmpq	%r10, %r9
	je	.L265
.L264:
	movzbl	(%rax), %r11d
	leaq	1(%rax), %r10
	movl	%r11d, %ecx
	cmpb	$59, %r11b
	je	.L277
	cmpw	%si, %r11w
	jnb	.L291
	movzbl	%r11b, %r14d
	movzwl	(%rdi,%r14,2), %r15d
	cmpw	$-2, %r15w
	jne	.L268
	movzbl	1(%rax), %r11d
	sall	$8, %ecx
	leaq	2(%rax), %r10
	orl	%ecx, %r11d
	movzwl	%r11w, %r14d
	movzwl	(%rdi,%r14,2), %r15d
.L268:
	cmpw	$-1, %r15w
	je	.L292
	testq	%r13, %r13
	je	.L270
	addq	%r13, %r14
	movsbl	(%r14), %eax
	testl	%eax, %eax
	je	.L293
.L271:
	addl	%eax, %r12d
.L274:
	movq	%r10, %rax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r10, %rax
.L265:
	movq	%rax, (%r8)
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movq	%r11, %rax
	movl	%ebx, %r15d
	movl	%r11d, %ecx
	addl	$1, %r12d
	shrq	$3, %rax
	sall	%cl, %r15d
	andl	$28, %eax
	orl	%r15d, (%rdx,%rax)
	movq	%r10, %rax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L293:
	addq	-48(%rbp), %r15
	movzbl	(%r15), %ecx
	leaq	1(%r15), %r11
	testb	%cl, %cl
	je	.L272
	movw	%si, -50(%rbp)
	movl	%ebx, %r15d
	subl	%r11d, %r15d
	.p2align 4,,10
	.p2align 3
.L273:
	movq	%rcx, %rax
	movl	%ebx, %esi
	shrq	$3, %rax
	sall	%cl, %esi
	andl	$28, %eax
	orl	%esi, (%rdx,%rax)
	leal	(%r15,%r11), %eax
	movzbl	(%r11), %ecx
	addq	$1, %r11
	testb	%cl, %cl
	jne	.L273
	movzwl	-50(%rbp), %esi
	movl	%eax, %ecx
	addl	%eax, %r12d
.L272:
	movb	%cl, (%r14)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L270:
	addq	-48(%rbp), %r15
	movzbl	(%r15), %ecx
	leaq	1(%r15), %r11
	testb	%cl, %cl
	je	.L274
	movl	%ebx, %r14d
	subl	%r11d, %r14d
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%rcx, %rax
	movl	%ebx, %r15d
	shrq	$3, %rax
	sall	%cl, %r15d
	andl	$28, %eax
	orl	%r15d, (%rdx,%rax)
	leal	(%r14,%r11), %eax
	movzbl	(%r11), %ecx
	addq	$1, %r11
	testb	%cl, %cl
	jne	.L275
	jmp	.L271
	.cfi_endproc
.LFE3601:
	.size	_ZN6icu_67L17calcNameSetLengthEPKttPKhPaPjPS3_S3_.constprop.0, .-_ZN6icu_67L17calcNameSetLengthEPKttPKhPaPjPS3_S3_.constprop.0
	.p2align 4
	.type	_ZN6icu_67L19calcNameSetsLengthsEP10UErrorCode.part.0, @function
_ZN6icu_67L19calcNameSetsLengthsEP10UErrorCode.part.0:
.LFB3597:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$248, %rsp
	movq	_ZN6icu_67L10uCharNamesE(%rip), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	12(%r8), %eax
	addq	%r8, %rax
	movl	(%rax), %r15d
	addq	$4, %rax
	testl	%r15d, %r15d
	je	.L295
	leaq	_ZN6icu_67L8gNameSetE(%rip), %r9
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L309:
	movzbl	8(%rax), %edx
	testb	%dl, %dl
	je	.L296
	cmpb	$1, %dl
	jne	.L298
	movzbl	9(%rax), %edx
	leaq	12(%rax), %rdi
	leaq	(%rdi,%rdx,2), %r11
	movq	%rdx, %rsi
	movzbl	(%r11), %ecx
	leaq	1(%r11), %rdx
	testb	%cl, %cl
	je	.L324
	movl	%r14d, %r12d
	subl	%edx, %r12d
	.p2align 4,,10
	.p2align 3
.L302:
	movq	%rcx, %r10
	movl	%r14d, %r13d
	shrq	$3, %r10
	sall	%cl, %r13d
	andl	$28, %r10d
	orl	%r13d, (%r9,%r10)
	leal	(%r12,%rdx), %r10d
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	testb	%cl, %cl
	jne	.L302
	movslq	%r10d, %rdx
	leaq	1(%r11,%rdx), %rdx
.L301:
	testb	%sil, %sil
	je	.L303
	leal	-1(%rsi), %ecx
	leaq	14(%rax,%rcx,2), %rcx
	movq	%rcx, -232(%rbp)
	.p2align 4,,10
	.p2align 3
.L308:
	movzwl	(%rdi), %r13d
	testl	%r13d, %r13d
	je	.L304
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L307:
	movzbl	(%rdx), %ecx
	leaq	1(%rdx), %rsi
	testb	%cl, %cl
	je	.L325
	movl	%r14d, %r12d
	movq	%rax, -248(%rbp)
	subl	%esi, %r12d
	movl	%r12d, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%rcx, %r12
	movl	%r14d, %eax
	shrq	$3, %r12
	sall	%cl, %eax
	andl	$28, %r12d
	orl	%eax, (%r9,%r12)
	movl	-240(%rbp), %eax
	leal	(%rax,%rsi), %r12d
	movzbl	(%rsi), %ecx
	addq	$1, %rsi
	testb	%cl, %cl
	jne	.L306
	movslq	%r12d, %rcx
	movq	-248(%rbp), %rax
	leaq	1(%rdx,%rcx), %rdx
.L305:
	cmpl	%r12d, %r11d
	cmovl	%r12d, %r11d
	subl	$1, %r13d
	jne	.L307
	addl	%r11d, %r10d
.L304:
	addq	$2, %rdi
	cmpq	%rdi, -232(%rbp)
	jne	.L308
.L303:
	cmpl	%r10d, %ebx
	cmovl	%r10d, %ebx
.L298:
	movzwl	10(%rax), %edx
	addq	%rdx, %rax
	subl	$1, %r15d
	jne	.L309
.L295:
	leaq	_ZN6icu_67L12charCatNamesE(%rip), %rsi
	movl	$117, %ecx
	movl	$1, %edi
	movq	(%rsi), %rax
	leaq	264(%rsi), %r10
	leaq	_ZN6icu_67L8gNameSetE(%rip), %r9
	addq	$1, %rax
	testb	%cl, %cl
	je	.L326
	.p2align 4,,10
	.p2align 3
.L356:
	movl	%edi, %r11d
	subl	%eax, %r11d
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%rcx, %rdx
	movl	%edi, %r14d
	shrq	$3, %rdx
	sall	%cl, %r14d
	andl	$28, %edx
	orl	%r14d, (%r9,%rdx)
	leal	(%r11,%rax), %edx
	movzbl	(%rax), %ecx
	addq	$1, %rax
	testb	%cl, %cl
	jne	.L311
	leal	9(%rdx), %eax
	cmpl	%eax, %ebx
	cmovl	%eax, %ebx
	addq	$8, %rsi
	cmpq	%rsi, %r10
	je	.L312
.L357:
	movq	(%rsi), %rax
	movzbl	(%rax), %ecx
	movq	(%rsi), %rax
	addq	$1, %rax
	testb	%cl, %cl
	jne	.L356
.L326:
	movl	$9, %eax
	cmpl	%eax, %ebx
	cmovl	%eax, %ebx
	addq	$8, %rsi
	cmpq	%rsi, %r10
	jne	.L357
	.p2align 4,,10
	.p2align 3
.L312:
	leaq	18(%r8), %rax
	movzwl	16(%r8), %r14d
	movq	%rax, -264(%rbp)
	movl	(%r8), %eax
	movq	%r14, %rdi
	movq	%r14, %r12
	addq	%r8, %rax
	movq	%rax, -232(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L314
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	_ZN6icu_67L10uCharNamesE(%rip), %r14
	movl	4(%r14), %eax
	addq	%r14, %rax
	leaq	2(%rax), %r15
	movzwl	(%rax), %eax
	testl	%eax, %eax
	je	.L315
.L320:
	subl	$1, %eax
	movq	%r14, -280(%rbp)
	leaq	3(%rax,%rax,2), %rax
	leaq	(%r15,%rax,2), %rax
	movq	%rax, -272(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -248(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -240(%rbp)
	movl	%r12d, %eax
	movl	%ebx, %r12d
	movl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L318:
	movzwl	2(%r15), %edi
	movzwl	4(%r15), %eax
	xorl	%r14d, %r14d
	movq	-280(%rbp), %rsi
	movq	-248(%rbp), %rdx
	sall	$16, %edi
	orl	%eax, %edi
	movl	8(%rsi), %eax
	movslq	%edi, %rdi
	addq	%rax, %rdi
	addq	%rsi, %rdi
	movq	-240(%rbp), %rsi
	call	_ZN6icu_67L18expandGroupLengthsEPKhPtS2_
	movq	-264(%rbp), %rdi
	movq	%rax, -256(%rbp)
	.p2align 4,,10
	.p2align 3
.L317:
	movq	-240(%rbp), %rax
	movzwl	(%rax,%r14), %r9d
	movq	-248(%rbp), %rax
	addq	-256(%rbp), %r9
	movzwl	(%rax,%r14), %eax
	movq	%r9, -216(%rbp)
	testw	%ax, %ax
	je	.L316
	movq	-232(%rbp), %rdx
	addq	%rax, %r9
	movq	%r13, %rcx
	movl	%ebx, %esi
	leaq	-216(%rbp), %r8
	call	_ZN6icu_67L17calcNameSetLengthEPKttPKhPaPjPS3_S3_.constprop.0
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	cmpq	-216(%rbp), %r9
	je	.L316
	movq	-232(%rbp), %rdx
	movq	%r13, %rcx
	movl	%ebx, %esi
	call	_ZN6icu_67L17calcNameSetLengthEPKttPKhPaPjPS3_S3_.constprop.0
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
.L316:
	addq	$2, %r14
	cmpq	$64, %r14
	jne	.L317
	addq	$6, %r15
	cmpq	-272(%rbp), %r15
	jne	.L318
	movl	%r12d, %ebx
	testq	%r13, %r13
	je	.L319
.L315:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
.L319:
	movl	%ebx, _ZN6icu_67L14gMaxNameLengthE(%rip)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L358
	addq	$248, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movq	%rsi, %rdx
	xorl	%r12d, %r12d
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L296:
	movzbl	12(%rax), %ecx
	leaq	13(%rax), %rdx
	testb	%cl, %cl
	je	.L323
	movl	%r14d, %edi
	subl	%edx, %edi
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%rcx, %rsi
	movl	%r14d, %r11d
	shrq	$3, %rsi
	sall	%cl, %r11d
	andl	$28, %esi
	orl	%r11d, (%r9,%rsi)
	leal	(%rdi,%rdx), %esi
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	testb	%cl, %cl
	jne	.L300
	movzbl	9(%rax), %edx
	addl	%esi, %edx
	cmpl	%edx, %ebx
	cmovl	%edx, %ebx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L324:
	xorl	%r10d, %r10d
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L323:
	movzbl	9(%rax), %edx
	xorl	%esi, %esi
	addl	%esi, %edx
	cmpl	%edx, %ebx
	cmovl	%edx, %ebx
	jmp	.L298
.L358:
	call	__stack_chk_fail@PLT
.L314:
	movq	_ZN6icu_67L10uCharNamesE(%rip), %r14
	movl	4(%r14), %eax
	addq	%r14, %rax
	leaq	2(%rax), %r15
	movzwl	(%rax), %eax
	testl	%eax, %eax
	jne	.L320
	jmp	.L319
	.cfi_endproc
.LFE3597:
	.size	_ZN6icu_67L19calcNameSetsLengthsEP10UErrorCode.part.0, .-_ZN6icu_67L19calcNameSetsLengthsEP10UErrorCode.part.0
	.p2align 4
	.type	_ZN6icu_67L7getNameEPNS_10UCharNamesEj15UCharNameChoicePct, @function
_ZN6icu_67L7getNameEPNS_10UCharNamesEj15UCharNameChoicePct:
.LFB2779:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rdi), %eax
	addq	%rdi, %rax
	movl	%esi, %edi
	shrl	$5, %edi
	movzwl	(%rax), %ecx
	leaq	2(%rax), %r8
	movl	%edi, %r10d
.L362:
	movzwl	%r9w, %edx
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L369:
	leal	(%rcx,%rdx), %eax
	sarl	%eax
	movl	%eax, %r9d
	leal	(%rax,%rax,2), %eax
	cltq
	cmpw	(%r8,%rax,2), %r10w
	jnb	.L362
	movzwl	%r9w, %ecx
.L361:
	leal	-1(%rcx), %eax
	cmpl	%edx, %eax
	jg	.L369
	leal	(%rdx,%rdx,2), %eax
	cltq
	leaq	(%r8,%rax,2), %rax
	cmpw	%di, (%rax)
	je	.L370
	xorl	%eax, %eax
	testw	%r14w, %r14w
	jne	.L371
.L359:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L372
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	movb	$0, (%rbx)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L370:
	movzwl	2(%rax), %edi
	movzwl	4(%rax), %eax
	andl	$31, %esi
	leaq	-128(%rbp), %rdx
	movl	%esi, %r15d
	leaq	-208(%rbp), %rsi
	sall	$16, %edi
	orl	%eax, %edi
	movl	8(%r12), %eax
	movslq	%edi, %rdi
	addq	%rax, %rdi
	addq	%r12, %rdi
	call	_ZN6icu_67L18expandGroupLengthsEPKhPtS2_
	movslq	%r15d, %rsi
	movzwl	%r14w, %r9d
	movq	%rbx, %r8
	movzwl	-128(%rbp,%rsi,2), %edx
	movzwl	-208(%rbp,%rsi,2), %esi
	movl	%r13d, %ecx
	movq	%r12, %rdi
	addq	%rax, %rsi
	call	_ZN6icu_67L10expandNameEPNS_10UCharNamesEPKht15UCharNameChoicePct
	jmp	.L359
.L372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2779:
	.size	_ZN6icu_67L7getNameEPNS_10UCharNamesEj15UCharNameChoicePct, .-_ZN6icu_67L7getNameEPNS_10UCharNamesEj15UCharNameChoicePct
	.p2align 4
	.type	_ZL13charSetToUSetPjPK9USetAdder.isra.0.constprop.0, @function
_ZL13charSetToUSetPjPK9USetAdder.isra.0.constprop.0:
.LFB3603:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	_ZN6icu_67L8gNameSetE(%rip), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$824, %rsp
	movl	_ZN6icu_67L14gMaxNameLengthE(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -836(%rbp)
	testl	%esi, %esi
	je	.L407
.L374:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L382:
	movl	%eax, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	movl	(%r14,%rdx,4), %edx
	btl	%eax, %edx
	jnc	.L381
	movslq	%r13d, %rdx
	addl	$1, %r13d
	movb	%al, -320(%rbp,%rdx)
.L381:
	addl	$1, %eax
	cmpl	$256, %eax
	jne	.L382
	leaq	-320(%rbp), %rax
	leaq	-832(%rbp), %r14
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -856(%rbp)
	call	u_charsToUChars_67@PLT
	testl	%r13d, %r13d
	je	.L373
	subl	$1, %r13d
	xorl	%r15d, %r15d
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%rdx, %r15
.L385:
	movzwl	(%r14,%r15,2), %esi
	testw	%si, %si
	jne	.L383
	movq	-856(%rbp), %rax
	cmpb	$0, (%rax,%r15)
	jne	.L384
.L383:
	movq	(%rbx), %rdi
	call	*(%r12)
.L384:
	leaq	1(%r15), %rdx
	cmpq	%r15, %r13
	jne	.L408
.L373:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	addq	$824, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L407:
	.cfi_restore_state
	movl	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L375
	leaq	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	jne	.L410
.L375:
	movl	4+_ZN6icu_67L18gCharNamesInitOnceE(%rip), %edx
	testl	%edx, %edx
	jg	.L373
.L378:
	movl	-836(%rbp), %eax
	testl	%eax, %eax
	jg	.L373
	leaq	_ZZN6icu_67L19calcNameSetsLengthsEP10UErrorCodeE8extChars(%rip), %rdx
	leaq	_ZN6icu_67L8gNameSetE(%rip), %r14
	movl	$1, %esi
	leaq	19(%rdx), %rdi
	.p2align 4,,10
	.p2align 3
.L380:
	movzbl	(%rdx), %ecx
	movl	%esi, %r10d
	addq	$1, %rdx
	movq	%rcx, %rax
	sall	%cl, %r10d
	shrq	$5, %rax
	andl	$7, %eax
	orl	%r10d, (%r14,%rax,4)
	cmpq	%rdx, %rdi
	jne	.L380
	call	_ZN6icu_67L19calcNameSetsLengthsEP10UErrorCode.part.0
	testb	%al, %al
	jne	.L374
	jmp	.L373
.L410:
	leaq	_ZN6icu_67L12isAcceptableEPvPKcS2_PK9UDataInfo(%rip), %rcx
	xorl	%edi, %edi
	leaq	-836(%rbp), %r9
	xorl	%r8d, %r8d
	leaq	_ZN6icu_67L9DATA_NAMEE(%rip), %rdx
	leaq	_ZN6icu_67L9DATA_TYPEE(%rip), %rsi
	call	udata_openChoice_67@PLT
	movl	-836(%rbp), %ecx
	movq	%rax, _ZN6icu_67L14uCharNamesDataE(%rip)
	movq	%rax, %rdi
	testl	%ecx, %ecx
	jle	.L376
	movq	$0, _ZN6icu_67L14uCharNamesDataE(%rip)
.L377:
	leaq	_ZN6icu_67L14unames_cleanupEv(%rip), %rsi
	movl	$16, %edi
	call	ucln_common_registerCleanup_67@PLT
	movl	-836(%rbp), %eax
	leaq	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L18gCharNamesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L378
.L376:
	call	udata_getMemory_67@PLT
	movq	%rax, _ZN6icu_67L10uCharNamesE(%rip)
	jmp	.L377
.L409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3603:
	.size	_ZL13charSetToUSetPjPK9USetAdder.isra.0.constprop.0, .-_ZL13charSetToUSetPjPK9USetAdder.isra.0.constprop.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC17:
	.string	"unknown"
.LC18:
	.string	"noncharacter"
.LC19:
	.string	"trail surrogate"
.LC20:
	.string	"lead surrogate"
	.text
	.p2align 4
	.type	_ZN6icu_67L10getExtNameEjPct.constprop.0, @function
_ZN6icu_67L10getExtNameEjPct.constprop.0:
.LFB3599:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	1(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edi, %ebx
	cmpl	$64975, %edi
	jle	.L412
	cmpl	$65007, %edi
	jg	.L468
.L413:
	cmpl	$1114111, %ebx
	jle	.L469
	movl	%ebx, %edi
	call	u_charType_67@PLT
	cmpb	$18, %al
	je	.L435
.L416:
	cmpb	$32, %al
	jbe	.L470
	movb	$60, (%r14)
	leaq	.LC17(%rip), %rdi
	movl	$117, %ecx
.L434:
	movl	$199, %edx
	movl	$1, %eax
.L423:
	movq	%r13, %r9
	movl	%eax, %r8d
	addl	$1, %eax
	movl	%edx, %esi
	movb	%cl, (%r9)
	movzwl	%ax, %ecx
	addq	$1, %r13
	subl	$1, %edx
	movzbl	-1(%rdi,%rcx), %ecx
	testb	%cl, %cl
	je	.L471
	testw	%dx, %dx
	jne	.L423
	.p2align 4,,10
	.p2align 3
.L422:
	movl	%eax, %r8d
	addl	$1, %eax
	movzwl	%ax, %edx
	cmpb	$0, -1(%rdi,%rdx)
	jne	.L422
	addl	$2, %r8d
.L437:
	testl	%ebx, %ebx
	je	.L440
	xorl	%esi, %esi
.L426:
	movl	%ebx, %edi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L428:
	addl	$1, %eax
	sarl	$4, %edi
	jne	.L428
	cmpl	$4, %eax
	movl	$4, %edi
	cmovge	%eax, %edi
	movl	%edi, %r10d
.L427:
	testw	%si, %si
	je	.L425
.L436:
	leal	-1(%rdi), %ecx
	movslq	%ecx, %rcx
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L432:
	subq	$1, %rcx
	testw	%si, %si
	je	.L425
.L429:
	movl	%r12d, %edx
	andl	$15, %edx
	cmpb	$9, %dl
	leal	48(%rdx), %r9d
	leal	55(%rdx), %eax
	cmovbe	%r9d, %eax
	subl	$1, %esi
	sarl	$4, %r12d
	movb	%al, 0(%r13,%rcx)
	jne	.L432
	testl	%ecx, %ecx
	jg	.L432
	movslq	%edi, %rax
	addl	%r10d, %r8d
	addq	%r13, %rax
	testw	%si, %si
	je	.L433
	movb	$62, (%rax)
.L433:
	popq	%rbx
	leal	1(%r8), %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	movl	%edi, %eax
	andl	$65534, %eax
	cmpl	$65534, %eax
	je	.L413
	movl	%ebx, %edi
	call	u_charType_67@PLT
	cmpb	$18, %al
	jne	.L416
.L435:
	leaq	.LC19(%rip), %rdi
.L415:
	movb	$60, (%r14)
	movzbl	(%rdi), %ecx
	jmp	.L434
.L440:
	movl	$4, %r10d
.L425:
	addl	%r10d, %r8d
	popq	%rbx
	popq	%r12
	leal	1(%r8), %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	addl	$2, %r8d
	testw	%dx, %dx
	je	.L437
	addq	$2, %r9
	movb	$45, 0(%r13)
	subl	$2, %esi
	movq	%r9, %r13
	testl	%ebx, %ebx
	jne	.L426
	movl	$4, %r10d
	movl	$4, %edi
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L412:
	call	u_charType_67@PLT
	cmpb	$18, %al
	jne	.L416
	movl	%ebx, %eax
	leaq	.LC20(%rip), %rdi
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L415
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L470:
	movzbl	%al, %eax
	leaq	_ZN6icu_67L12charCatNamesE(%rip), %rdx
	movb	$60, (%r14)
	movq	(%rdx,%rax,8), %rdi
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	jne	.L434
	movb	$45, 1(%r14)
	leaq	2(%r14), %r13
	testl	%ebx, %ebx
	je	.L442
	movl	$2, %r8d
	movl	$198, %esi
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L469:
	leaq	.LC18(%rip), %rdi
	jmp	.L415
.L442:
	movl	$4, %r10d
	movl	$4, %edi
	movl	$198, %esi
	movl	$2, %r8d
	jmp	.L436
	.cfi_endproc
.LFE3599:
	.size	_ZN6icu_67L10getExtNameEjPct.constprop.0, .-_ZN6icu_67L10getExtNameEjPct.constprop.0
	.p2align 4
	.type	_ZN6icu_67L14enumGroupNamesEPNS_10UCharNamesEPKtiiPFaPvi15UCharNameChoicePKciES4_S5_, @function
_ZN6icu_67L14enumGroupNamesEPNS_10UCharNamesEPKtiiPFaPvi15UCharNameChoicePKciES4_S5_:
.LFB2780:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	leaq	-336(%rbp), %rdx
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$424, %rsp
	movzwl	4(%rsi), %eax
	movl	16(%rbp), %r12d
	movl	%ecx, -420(%rbp)
	movq	%r9, -440(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movzwl	2(%rsi), %edi
	leaq	-416(%rbp), %rsi
	sall	$16, %edi
	orl	%eax, %edi
	movl	8(%rbx), %eax
	movslq	%edi, %rdi
	addq	%rax, %rdi
	addq	%rbx, %rdi
	call	_ZN6icu_67L18expandGroupLengthsEPKhPtS2_
	movq	%rax, -456(%rbp)
	testq	%r14, %r14
	je	.L473
	cmpl	$2, %r12d
	leaq	-256(%rbp), %r13
	sete	-432(%rbp)
	cmpl	-420(%rbp), %r15d
	jg	.L499
	movq	%r14, -448(%rbp)
	movq	-456(%rbp), %r14
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L545:
	cmpb	$0, -432(%rbp)
	je	.L476
	movq	%r13, %rsi
	movl	%r15d, %edi
	call	_ZN6icu_67L10getExtNameEjPct.constprop.0
	movzwl	%ax, %edx
	movb	$0, -256(%rbp,%rdx)
.L476:
	testw	%ax, %ax
	jne	.L531
.L477:
	addl	$1, %r15d
	cmpl	%r15d, -420(%rbp)
	jl	.L499
.L479:
	movl	%r15d, %eax
	movl	$200, %r9d
	movq	%r13, %r8
	movl	%r12d, %ecx
	andl	$31, %eax
	movq	%rbx, %rdi
	movzwl	-416(%rbp,%rax,2), %esi
	movzwl	-336(%rbp,%rax,2), %edx
	addq	%r14, %rsi
	call	_ZN6icu_67L10expandNameEPNS_10UCharNamesEPKht15UCharNameChoicePct
	testw	%ax, %ax
	je	.L545
.L531:
	movzwl	%ax, %r8d
	movq	%r13, %rcx
	movl	%r12d, %edx
	movl	%r15d, %esi
	movq	-440(%rbp), %rdi
	movq	-448(%rbp), %rax
	call	*%rax
	testb	%al, %al
	jne	.L477
.L472:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L546
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L473:
	movq	-440(%rbp), %rax
	movq	(%rax), %rdi
	cmpl	-420(%rbp), %r15d
	jg	.L499
	movl	(%rbx), %eax
	movzwl	16(%rbx), %r14d
	movl	%r12d, 16(%rbp)
	leaq	18(%rbx), %r13
	movzbl	(%rdi), %r10d
	movq	%rax, -432(%rbp)
	movl	%r12d, %eax
	andl	$-3, %eax
	cmpl	$4, %r12d
	movw	%r14w, -422(%rbp)
	movl	%eax, -448(%rbp)
	movl	$2, %eax
	cmovne	%r12d, %eax
	movq	%rdi, %r12
	movl	%eax, -460(%rbp)
	subl	$1, %eax
	movl	%eax, -464(%rbp)
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L480:
	cmpw	$59, -422(%rbp)
	jbe	.L502
	cmpw	$-1, 136(%rbx)
	je	.L502
.L503:
	movl	%r10d, %eax
.L481:
	testb	%al, %al
	je	.L547
.L490:
	addl	$1, %r15d
	cmpl	%r15d, -420(%rbp)
	jl	.L499
.L497:
	movl	%r15d, %eax
	andl	$31, %eax
	movzwl	-416(%rbp,%rax,2), %ecx
	movzwl	-336(%rbp,%rax,2), %edi
	movl	-448(%rbp), %eax
	addq	-456(%rbp), %rcx
	testl	%eax, %eax
	jne	.L480
.L488:
	movl	%r10d, %eax
	testw	%di, %di
	je	.L481
	movq	%r12, %rsi
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L548:
	cmpb	$59, %dl
	je	.L481
.L543:
	leaq	1(%rsi), %rdx
	cmpb	%al, %r8b
	jne	.L490
	movzbl	1(%rsi), %eax
	movl	%r9d, %edi
	movq	%rdx, %rsi
	movq	%r11, %rcx
.L495:
	testw	%r9w, %r9w
	je	.L481
.L482:
	movzbl	(%rcx), %edx
	leal	-1(%rdi), %r9d
	leaq	1(%rcx), %r11
	movl	%edx, %r8d
	cmpl	%r14d, %edx
	jge	.L548
	movzbl	%dl, %edx
	movzwl	0(%r13,%rdx,2), %edx
	cmpw	$-2, %dx
	jne	.L492
	movzwl	(%rcx), %edx
	leaq	2(%rcx), %r11
	leal	-2(%rdi), %r9d
	rolw	$8, %dx
	movzwl	%dx, %edx
	movzwl	0(%r13,%rdx,2), %edx
.L492:
	cmpw	$-1, %dx
	jne	.L493
	cmpb	$59, %r8b
	jne	.L543
	cmpq	%rsi, %r12
	jne	.L481
	cmpl	$2, 16(%rbp)
	jne	.L481
	cmpw	$59, -422(%rbp)
	jbe	.L507
	cmpw	$-1, 136(%rbx)
	jne	.L481
.L507:
	movl	%r9d, %edi
	movq	%r11, %rcx
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L493:
	addq	-432(%rbp), %rdx
	addq	%rbx, %rdx
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L549:
	addq	$1, %rsi
	cmpb	%al, %cl
	jne	.L490
.L496:
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	movzbl	(%rsi), %eax
	testb	%cl, %cl
	jne	.L549
	movl	%r9d, %edi
	movq	%r11, %rcx
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L502:
	movl	-460(%rbp), %esi
	movl	-464(%rbp), %r8d
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%rdx, %rcx
.L484:
	movl	%r8d, %eax
	testw	%di, %di
	je	.L550
	subl	$1, %edi
	cmpb	$59, (%rcx)
	leaq	1(%rcx), %rdx
	jne	.L487
	movq	%rdx, %rcx
.L486:
	testl	%eax, %eax
	jle	.L488
	movl	%eax, %esi
	movq	%rcx, %rdx
	leal	-1(%rax), %r8d
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L550:
	testl	%r8d, %r8d
	jle	.L503
	leal	-2(%rsi), %eax
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L547:
	movq	-440(%rbp), %rbx
	movl	%r15d, 8(%rbx)
	jmp	.L472
.L546:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2780:
	.size	_ZN6icu_67L14enumGroupNamesEPNS_10UCharNamesEPKtiiPFaPvi15UCharNameChoicePKciES4_S5_, .-_ZN6icu_67L14enumGroupNamesEPNS_10UCharNamesEPKtiiPFaPvi15UCharNameChoicePKciES4_S5_
	.p2align 4
	.type	_ZN6icu_67L9enumNamesEPNS_10UCharNamesEiiPFaPvi15UCharNameChoicePKciES2_S3_, @function
_ZN6icu_67L9enumNamesEPNS_10UCharNamesEiiPFaPvi15UCharNameChoicePKciES2_S3_:
.LFB2782:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movl	%edx, -300(%rbp)
	movl	%r9d, -260(%rbp)
	movl	%esi, -296(%rbp)
	movq	%rcx, -272(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movl	%eax, %esi
	shrl	$5, %eax
	sarl	$5, %esi
	movl	%eax, %r8d
	movl	%esi, -288(%rbp)
	leal	-1(%rdx), %esi
	movl	%esi, -304(%rbp)
	sarl	$5, %esi
	movl	%esi, -292(%rbp)
	movw	%si, -262(%rbp)
	movl	4(%rdi), %esi
	addq	%rdi, %rsi
	movzwl	(%rsi), %edx
	leaq	2(%rsi), %r9
	movl	%edx, %edi
.L554:
	movzwl	%r10w, %ecx
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L654:
	leal	(%rcx,%rdx), %eax
	sarl	%eax
	movl	%eax, %r10d
	leal	(%rax,%rax,2), %eax
	cltq
	cmpw	(%r9,%rax,2), %r8w
	jnb	.L554
	movzwl	%r10w, %edx
.L553:
	leal	-1(%rdx), %eax
	cmpl	%eax, %ecx
	jl	.L654
	leal	(%rcx,%rcx,2), %eax
	cmpl	$2, -260(%rbp)
	cltq
	sete	-263(%rbp)
	movzbl	-263(%rbp), %ecx
	leaq	(%r9,%rax,2), %rbx
	movzwl	(%rbx), %eax
	cmpw	-288(%rbp), %ax
	jbe	.L587
	testb	%cl, %cl
	jne	.L655
.L587:
	movl	-296(%rbp), %r15d
.L555:
	movzwl	-292(%rbp), %edi
	cmpw	%di, -288(%rbp)
	je	.L656
	movl	4(%r14), %edx
	addq	%r14, %rdx
	movzwl	(%rdx), %ecx
	leal	(%rcx,%rcx,2), %ecx
	movslq	%ecx, %rcx
	leaq	2(%rdx,%rcx,2), %rsi
	movq	%rsi, -280(%rbp)
	cmpw	%ax, -288(%rbp)
	je	.L657
.L563:
	movl	-288(%rbp), %esi
	cmpw	%ax, %si
	jbe	.L564
	leaq	6(%rbx), %r9
	cmpq	-280(%rbp), %r9
	jnb	.L588
	movzwl	6(%rbx), %eax
	cmpw	%si, %ax
	jbe	.L568
	cmpb	$0, -263(%rbp)
	jne	.L658
.L568:
	movq	%r9, %rbx
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L657:
	testb	$31, %r15b
	jne	.L659
.L564:
	cmpq	%rbx, -280(%rbp)
	jbe	.L566
.L567:
	leaq	-256(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	%r14, %rax
	movl	%r15d, %r14d
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L584:
	movzwl	(%rbx), %eax
	cmpw	-262(%rbp), %ax
	jnb	.L572
.L660:
	movzwl	%ax, %r11d
	movl	-260(%rbp), %eax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	subq	$8, %rsp
	sall	$5, %r11d
	movq	-272(%rbp), %r8
	movq	%r12, %r9
	pushq	%rax
	leal	31(%r11), %ecx
	movl	%r11d, %edx
	movl	%r11d, %r14d
	call	_ZN6icu_67L14enumGroupNamesEPNS_10UCharNamesEPKtiiPFaPvi15UCharNameChoicePKciES4_S5_
	popq	%rsi
	popq	%rdi
	testb	%al, %al
	je	.L573
	addq	$6, %rbx
	cmpq	-280(%rbp), %rbx
	jnb	.L649
	movzwl	-6(%rbx), %r9d
	movzwl	(%rbx), %r13d
	addl	$1, %r9d
	cmpl	%r9d, %r13d
	jle	.L584
	cmpb	$0, -263(%rbp)
	je	.L584
	movq	-272(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L584
	movl	-300(%rbp), %edi
	sall	$5, %r13d
	movl	%r13d, %eax
	cmpl	%r13d, %edi
	cmovle	%edi, %eax
	sall	$5, %r9d
	cmpl	%r9d, %eax
	jle	.L584
	movl	%r14d, -296(%rbp)
	movq	-288(%rbp), %r14
	movl	%eax, %r13d
	movq	%rbx, -312(%rbp)
	movl	%r9d, %ebx
	movq	%r15, -320(%rbp)
	movq	%rsi, %r15
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%r14, %rsi
	movl	%ebx, %edi
	call	_ZN6icu_67L10getExtNameEjPct.constprop.0
	movzwl	%ax, %edx
	movzwl	%ax, %r8d
	movb	$0, -256(%rbp,%rdx)
	testw	%ax, %ax
	je	.L576
	movq	%r14, %rcx
	movl	$2, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	*%r15
	testb	%al, %al
	je	.L551
.L576:
	addl	$1, %ebx
	cmpl	%ebx, %r13d
	jne	.L575
	movq	-312(%rbp), %rbx
	movl	-296(%rbp), %r14d
	movq	-320(%rbp), %r15
	movzwl	(%rbx), %eax
	cmpw	-262(%rbp), %ax
	jb	.L660
	.p2align 4,,10
	.p2align 3
.L572:
	movq	%r15, %rdi
	movl	%r14d, %r15d
	cmpq	%rbx, -280(%rbp)
	jbe	.L566
	cmpw	-292(%rbp), %ax
	je	.L578
.L583:
	movl	$1, %eax
.L551:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L661
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L649:
	.cfi_restore_state
	movl	%r14d, %r15d
.L566:
	cmpq	%rbx, -280(%rbp)
	jne	.L583
	cmpb	$0, -263(%rbp)
	je	.L583
	movzwl	-6(%rbx), %eax
	addl	$1, %eax
	sall	$5, %eax
	cmpl	%eax, %r15d
	cmovl	%eax, %r15d
.L580:
	cmpq	$0, -272(%rbp)
	je	.L583
	movl	-300(%rbp), %eax
	movl	$1114112, %r14d
	movq	-272(%rbp), %rbx
	leaq	-256(%rbp), %r13
	cmpl	$1114112, %eax
	cmovle	%eax, %r14d
	cmpl	%r15d, %r14d
	jle	.L583
	.p2align 4,,10
	.p2align 3
.L581:
	movq	%r13, %rsi
	movl	%r15d, %edi
	call	_ZN6icu_67L10getExtNameEjPct.constprop.0
	movzwl	%ax, %edx
	movzwl	%ax, %r8d
	movb	$0, -256(%rbp,%rdx)
	testw	%ax, %ax
	je	.L582
	movq	%r13, %rcx
	movl	$2, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*%rbx
	testb	%al, %al
	je	.L551
.L582:
	addl	$1, %r15d
	cmpl	%r15d, %r14d
	jne	.L581
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L656:
	cmpw	%ax, -288(%rbp)
	je	.L662
	cmpl	$2, -260(%rbp)
	jne	.L583
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L659:
	movl	-296(%rbp), %eax
	subq	$8, %rsp
	movq	%r12, %r9
	movq	%rbx, %rsi
	movq	-272(%rbp), %r8
	movl	%r15d, %edx
	movq	%r14, %rdi
	addq	$6, %rbx
	andl	$2097120, %eax
	leal	31(%rax), %ecx
	movl	-260(%rbp), %eax
	pushq	%rax
	call	_ZN6icu_67L14enumGroupNamesEPNS_10UCharNamesEPKtiiPFaPvi15UCharNameChoicePKciES4_S5_
	popq	%r8
	popq	%r9
	testb	%al, %al
	jne	.L564
	.p2align 4,,10
	.p2align 3
.L573:
	xorl	%eax, %eax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L655:
	movl	-300(%rbp), %ecx
	movzwl	%ax, %r11d
	sall	$5, %r11d
	cmpl	%r11d, %ecx
	movl	%r11d, %r15d
	cmovle	%ecx, %r15d
	movq	-272(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L556
	movl	-296(%rbp), %esi
	cmpl	%esi, %r15d
	jle	.L555
	movq	%rbx, -280(%rbp)
	leaq	-256(%rbp), %r13
	movl	%esi, %ebx
	movq	%r14, -312(%rbp)
	movq	%rcx, %r14
	.p2align 4,,10
	.p2align 3
.L560:
	movq	%r13, %rsi
	movl	%ebx, %edi
	call	_ZN6icu_67L10getExtNameEjPct.constprop.0
	movzwl	%ax, %edx
	movzwl	%ax, %r8d
	movb	$0, -256(%rbp,%rdx)
	testw	%ax, %ax
	je	.L558
	movq	%r13, %rcx
	movl	$2, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	*%r14
	testb	%al, %al
	je	.L551
.L558:
	addl	$1, %ebx
	cmpl	%ebx, %r15d
	jne	.L560
	movq	-280(%rbp), %rbx
	movq	-312(%rbp), %r14
	movzwl	(%rbx), %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L588:
	movq	%r9, %rbx
	jmp	.L566
.L662:
	movl	-260(%rbp), %eax
	subq	$8, %rsp
	movq	%r12, %r9
	movl	%r15d, %edx
	movq	-272(%rbp), %r8
	movl	-304(%rbp), %ecx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	pushq	%rax
	call	_ZN6icu_67L14enumGroupNamesEPNS_10UCharNamesEPKtiiPFaPvi15UCharNameChoicePKciES4_S5_
	popq	%r10
	popq	%r11
	jmp	.L551
.L556:
	movzwl	-292(%rbp), %ecx
	cmpw	%cx, -288(%rbp)
	je	.L583
	leal	(%rdi,%rdi,2), %edx
	movslq	%edx, %rdx
	leaq	2(%rsi,%rdx,2), %rdi
	movq	%rdi, -280(%rbp)
	jmp	.L563
.L578:
	movl	-260(%rbp), %eax
	subq	$8, %rsp
	movq	%r12, %r9
	movq	%rbx, %rsi
	movl	-304(%rbp), %ecx
	movq	-272(%rbp), %r8
	pushq	%rax
	movl	%ecx, %edx
	andl	$-32, %edx
	call	_ZN6icu_67L14enumGroupNamesEPNS_10UCharNamesEPKtiiPFaPvi15UCharNameChoicePKciES4_S5_
	popq	%rdx
	popq	%rcx
	jmp	.L551
.L658:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L568
	movl	-300(%rbp), %esi
	sall	$5, %eax
	movl	%r15d, %ebx
	leaq	-256(%rbp), %r13
	cmpl	%eax, %esi
	cmovle	%esi, %eax
	cmpl	%r15d, %eax
	jle	.L568
	movq	%r9, -288(%rbp)
	movl	%r15d, -296(%rbp)
	movl	%eax, %r15d
	movq	%r14, -312(%rbp)
	movl	%ebx, %r14d
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L569:
	movq	%r13, %rsi
	movl	%r14d, %edi
	call	_ZN6icu_67L10getExtNameEjPct.constprop.0
	movzwl	%ax, %edx
	movzwl	%ax, %r8d
	movb	$0, -256(%rbp,%rdx)
	testw	%ax, %ax
	je	.L570
	movq	%r13, %rcx
	movl	$2, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*%rbx
	testb	%al, %al
	je	.L551
.L570:
	addl	$1, %r14d
	cmpl	%r14d, %r15d
	jne	.L569
	movq	-288(%rbp), %r9
	movl	-296(%rbp), %r15d
	movq	-312(%rbp), %r14
	jmp	.L568
.L661:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2782:
	.size	_ZN6icu_67L9enumNamesEPNS_10UCharNamesEiiPFaPvi15UCharNameChoicePKciES2_S3_, .-_ZN6icu_67L9enumNamesEPNS_10UCharNamesEiiPFaPvi15UCharNameChoicePKciES2_S3_
	.p2align 4
	.globl	u_charName_67
	.type	u_charName_67, @function
u_charName_67:
.LFB2793:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L759
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L663
	movl	%esi, %r15d
	cmpl	$3, %esi
	jg	.L665
	movl	%ecx, %r14d
	testl	%ecx, %ecx
	js	.L665
	movl	%edi, %r13d
	movq	%rdx, %rbx
	jle	.L666
	testq	%rdx, %rdx
	jne	.L666
.L665:
	movl	$1, (%r12)
.L663:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	cmpl	$1114111, %r13d
	ja	.L672
	movl	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L668
	leaq	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L668
	leaq	_ZN6icu_67L9DATA_NAMEE(%rip), %rdx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	leaq	_ZN6icu_67L12isAcceptableEPvPKcS2_PK9UDataInfo(%rip), %rcx
	leaq	_ZN6icu_67L9DATA_TYPEE(%rip), %rsi
	call	udata_openChoice_67@PLT
	movl	(%r12), %edx
	movq	%rax, _ZN6icu_67L14uCharNamesDataE(%rip)
	testl	%edx, %edx
	jle	.L669
	movq	$0, _ZN6icu_67L14uCharNamesDataE(%rip)
.L670:
	movl	$16, %edi
	leaq	_ZN6icu_67L14unames_cleanupEv(%rip), %rsi
	call	ucln_common_registerCleanup_67@PLT
	movl	(%r12), %eax
	leaq	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L18gCharNamesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L671:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L672
	movq	_ZN6icu_67L10uCharNamesE(%rip), %r9
	movl	12(%r9), %edi
	addq	%r9, %rdi
	movl	(%rdi), %eax
	addq	$4, %rdi
	testl	%eax, %eax
	je	.L673
	.p2align 4,,10
	.p2align 3
.L674:
	cmpl	(%rdi), %r13d
	jb	.L675
	cmpl	4(%rdi), %r13d
	jbe	.L763
.L675:
	movzwl	10(%rdi), %edx
	addq	%rdx, %rdi
	subl	$1, %eax
	jne	.L674
.L673:
	movzwl	%r14w, %r8d
	cmpl	$2, %r15d
	je	.L764
	movl	%r15d, %edx
	movq	%rbx, %rcx
	movl	%r13d, %esi
	movq	%r9, %rdi
	call	_ZN6icu_67L7getNameEPNS_10UCharNamesEj15UCharNameChoicePct
	movzwl	%ax, %edx
.L678:
	movq	%r12, %rcx
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L668:
	movl	4+_ZN6icu_67L18gCharNamesInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L671
	movl	%eax, (%r12)
	.p2align 4,,10
	.p2align 3
.L672:
	movq	%r12, %rcx
	xorl	%edx, %edx
.L762:
	addq	$24, %rsp
	movl	%r14d, %esi
	movq	%rbx, %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	u_terminateChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L759:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	andl	$-3, %r15d
	jne	.L765
	movq	%rbx, %rdx
	movzwl	%r14w, %ecx
	movl	%r13d, %esi
	call	_ZN6icu_67L10getAlgNameEPNS_16AlgorithmicRangeEj15UCharNameChoicePct.part.0
	movzwl	%ax, %edx
	jmp	.L678
.L764:
	movl	$2, %edx
	movq	%rbx, %rcx
	movl	%r13d, %esi
	movq	%r9, %rdi
	call	_ZN6icu_67L7getNameEPNS_10UCharNamesEj15UCharNameChoicePct
	movzwl	%ax, %edx
	testl	%edx, %edx
	jne	.L678
	cmpl	$64975, %r13d
	jle	.L679
	cmpl	$65007, %r13d
	jle	.L713
	movl	%r13d, %eax
	andl	$65534, %eax
	cmpl	$65534, %eax
	je	.L713
.L679:
	movl	%r13d, %edi
	movl	%edx, -52(%rbp)
	call	u_charType_67@PLT
	movl	-52(%rbp), %edx
	cmpb	$18, %al
	je	.L766
	cmpb	$32, %al
	jbe	.L767
	leaq	.LC17(%rip), %r9
	testw	%r14w, %r14w
	je	.L705
	movb	$60, (%rbx)
	leaq	1(%rbx), %rsi
	leal	-1(%r14), %eax
	movl	$117, %r8d
	leaq	.LC17(%rip), %r9
.L703:
	movl	$1, %ecx
	jmp	.L692
.L768:
	addl	$1, %ecx
	movb	%r8b, (%rsi)
	leaq	1(%rsi), %rdi
	subl	$1, %eax
	movzwl	%cx, %esi
	movzbl	-1(%r9,%rsi), %r8d
	movq	%rdi, %rsi
	testb	%r8b, %r8b
	je	.L691
.L692:
	testw	%ax, %ax
	jne	.L768
	.p2align 4,,10
	.p2align 3
.L690:
	addl	$1, %ecx
	movzwl	%cx, %eax
	cmpb	$0, -1(%r9,%rax)
	jne	.L690
	xorl	%eax, %eax
.L691:
	leal	1(%rcx), %edi
.L688:
	testw	%ax, %ax
	je	.L769
	movb	$45, (%rsi)
	leaq	1(%rsi), %r8
	subl	$1, %eax
	testl	%r13d, %r13d
	je	.L719
.L687:
	movl	%r13d, %ecx
	.p2align 4,,10
	.p2align 3
.L695:
	addl	$1, %edx
	sarl	$4, %ecx
	jne	.L695
.L694:
	cmpl	$4, %edx
	movl	$4, %r10d
	cmovge	%edx, %r10d
	testw	%ax, %ax
	je	.L686
	leal	-1(%r10), %esi
	movslq	%esi, %rsi
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L699:
	subq	$1, %rsi
	testw	%ax, %ax
	je	.L686
.L696:
	movl	%r13d, %ecx
	andl	$15, %ecx
	cmpb	$9, %cl
	leal	48(%rcx), %r9d
	leal	55(%rcx), %edx
	cmovbe	%r9d, %edx
	subl	$1, %eax
	sarl	$4, %r13d
	movb	%dl, (%r8,%rsi)
	jne	.L699
	testl	%esi, %esi
	jg	.L699
	movslq	%r10d, %rdx
	addq	%rdx, %r8
	leal	(%rdi,%r10), %edx
	testw	%ax, %ax
	je	.L700
	movb	$62, (%r8)
.L700:
	addl	$1, %edx
	movzwl	%dx, %edx
	jmp	.L678
.L765:
	xorl	%edx, %edx
	testw	%r14w, %r14w
	je	.L678
	movb	$0, (%rbx)
	jmp	.L678
.L669:
	movq	%rax, %rdi
	call	udata_getMemory_67@PLT
	movq	%rax, _ZN6icu_67L10uCharNamesE(%rip)
	jmp	.L670
.L718:
	movl	$4, %r10d
.L686:
	leal	(%rdi,%r10), %edx
	jmp	.L700
.L713:
	movl	$30, %eax
.L680:
	andl	$63, %eax
	leaq	_ZN6icu_67L12charCatNamesE(%rip), %rcx
	movq	(%rcx,%rax,8), %r9
	testw	%r14w, %r14w
	jne	.L770
.L708:
	cmpb	$0, (%r9)
	je	.L684
.L705:
	movq	%rbx, %rsi
	movl	$1, %ecx
	jmp	.L690
.L769:
	testl	%r13d, %r13d
	je	.L718
	movq	%rsi, %r8
	jmp	.L687
.L766:
	movl	%r13d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	setne	%al
	addl	$31, %eax
	jmp	.L680
.L770:
	movb	$60, (%rbx)
	leaq	1(%rbx), %rsi
	movzbl	(%r9), %r8d
	leal	-1(%r14), %eax
	jmp	.L703
.L719:
	xorl	%edx, %edx
	jmp	.L694
.L684:
	testl	%r13d, %r13d
	je	.L715
	movq	%rbx, %r8
	movl	$2, %edi
	xorl	%eax, %eax
	jmp	.L687
.L767:
	movzbl	%al, %eax
	leaq	_ZN6icu_67L12charCatNamesE(%rip), %rcx
	movq	(%rcx,%rax,8), %r9
	testw	%r14w, %r14w
	je	.L708
	movb	$60, (%rbx)
	movzbl	(%r9), %r8d
	leaq	1(%rbx), %rsi
	leal	-1(%r14), %eax
	movl	$2, %edi
	testb	%r8b, %r8b
	je	.L688
	jmp	.L703
.L715:
	movl	$2, %edi
	movl	$4, %r10d
	jmp	.L686
	.cfi_endproc
.LFE2793:
	.size	u_charName_67, .-u_charName_67
	.p2align 4
	.globl	u_getISOComment_67
	.type	u_getISOComment_67, @function
u_getISOComment_67:
.LFB2794:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	movl	%edx, %esi
	testq	%rcx, %rcx
	je	.L771
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L771
	testl	%edx, %edx
	js	.L773
	jle	.L774
	testq	%rdi, %rdi
	jne	.L774
.L773:
	movl	$1, (%rcx)
.L771:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L774:
	xorl	%edx, %edx
	jmp	u_terminateChars_67@PLT
	.cfi_endproc
.LFE2794:
	.size	u_getISOComment_67, .-u_getISOComment_67
	.p2align 4
	.globl	u_charFromName_67
	.type	u_charFromName_67, @function
u_charFromName_67:
.LFB2795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, -540(%rbp)
	movq	%rdx, -536(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movq	$0, -192(%rbp)
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm0, -288(%rbp)
	movaps	%xmm0, -272(%rbp)
	movaps	%xmm0, -256(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rdx, %rdx
	je	.L890
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L890
	cmpl	$3, -540(%rbp)
	jg	.L788
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L788
	cmpb	$0, (%rsi)
	je	.L788
	movl	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L891
.L790:
	movl	4+_ZN6icu_67L18gCharNamesInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L793
	movq	-536(%rbp), %rcx
	movl	$65535, %r12d
	movl	%eax, (%rcx)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L788:
	movq	-536(%rbp), %rax
	movl	$1, (%rax)
.L890:
	movl	$65535, %r12d
.L784:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L892
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L891:
	.cfi_restore_state
	leaq	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L790
	movq	-536(%rbp), %rbx
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	leaq	_ZN6icu_67L12isAcceptableEPvPKcS2_PK9UDataInfo(%rip), %rcx
	leaq	_ZN6icu_67L9DATA_NAMEE(%rip), %rdx
	leaq	_ZN6icu_67L9DATA_TYPEE(%rip), %rsi
	movq	%rbx, %r9
	call	udata_openChoice_67@PLT
	movl	(%rbx), %edi
	movq	%rax, _ZN6icu_67L14uCharNamesDataE(%rip)
	testl	%edi, %edi
	jle	.L791
	movq	$0, _ZN6icu_67L14uCharNamesDataE(%rip)
.L792:
	movl	$16, %edi
	leaq	_ZN6icu_67L14unames_cleanupEv(%rip), %rsi
	call	ucln_common_registerCleanup_67@PLT
	movq	-536(%rbp), %rax
	leaq	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %rdi
	movl	(%rax), %eax
	movl	%eax, 4+_ZN6icu_67L18gCharNamesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L793:
	movq	-536(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L890
	xorl	%ebx, %ebx
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L893:
	movsbl	%dil, %r13d
	movl	%r13d, %edi
	call	uprv_toupper_67@PLT
	movl	%r13d, %edi
	movb	%al, -304(%rbp,%rbx)
	call	uprv_asciitolower_67@PLT
	movb	%al, -176(%rbp,%rbx)
	addq	$1, %rbx
	cmpq	$120, %rbx
	je	.L795
.L796:
	movzbl	(%r12,%rbx), %edi
	movl	%ebx, %edx
	testb	%dil, %dil
	jne	.L893
	movl	%ebx, %eax
	movb	$0, -176(%rbp,%rax)
	cmpb	$60, -176(%rbp)
	movb	$0, -304(%rbp,%rax)
	je	.L894
	movq	_ZN6icu_67L10uCharNamesE(%rip), %rax
	movl	12(%rax), %r13d
	movq	%rax, -584(%rbp)
	addq	%rax, %r13
	movl	-540(%rbp), %eax
	movl	0(%r13), %r11d
	addq	$4, %r13
	andl	$-3, %eax
	movl	%eax, -544(%rbp)
	leaq	-304(%rbp), %rax
	movl	%r11d, %r15d
	movq	%rax, -552(%rbp)
	leaq	-496(%rbp), %rax
	movq	%rax, -568(%rbp)
	testl	%r11d, %r11d
	je	.L840
	.p2align 4,,10
	.p2align 3
.L841:
	movl	-544(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L815
	movzbl	8(%r13), %eax
	testb	%al, %al
	je	.L816
	cmpb	$1, %al
	jne	.L815
	movzbl	9(%r13), %eax
	leaq	12(%r13), %r14
	movq	-552(%rbp), %rbx
	movq	%rax, %r10
	leaq	(%r14,%rax,2), %rdx
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L895:
	addq	$1, %rbx
	cmpb	-1(%rbx), %al
	jne	.L815
.L828:
	movzbl	(%rdx), %eax
	addq	$1, %rdx
	testb	%al, %al
	jne	.L895
	movl	4(%r13), %r11d
	subq	$8, %rsp
	movl	0(%r13), %r12d
	movzbl	%r10b, %esi
	leaq	-432(%rbp), %rax
	pushq	$64
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movl	%r11d, -572(%rbp)
	leaq	-368(%rbp), %r11
	movq	-568(%rbp), %r9
	leaq	-512(%rbp), %r8
	pushq	%r11
	pushq	%rax
	movq	%r11, -560(%rbp)
	movb	%r10b, -573(%rbp)
	call	_ZN6icu_67L17writeFactorSuffixEPKttPKcjPtPS3_S5_Pct
	movq	-560(%rbp), %r11
	addq	$32, %rsp
	movq	%rbx, %rdi
	movq	%r11, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L825
	movl	-572(%rbp), %r11d
	addl	$1, %r12d
	addl	$1, %r11d
	cmpl	%r12d, %r11d
	jle	.L815
	movzbl	-573(%rbp), %esi
	movq	%r13, -560(%rbp)
	leal	-1(%rsi), %edi
	movzwl	%di, %eax
	leaq	(%r14,%rax,2), %r10
	movq	%rax, %r9
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L830:
	movzwl	-512(%rbp,%r9,2), %eax
	movl	%edi, %ecx
	leal	1(%rax), %edx
	movq	%r8, %rax
	cmpw	(%r10), %dx
	jb	.L835
	.p2align 4,,10
	.p2align 3
.L831:
	xorl	%edx, %edx
	subl	$1, %ecx
	movw	%dx, -512(%rbp,%rax,2)
	movq	-496(%rbp,%rax,8), %rdx
	movq	%rdx, -432(%rbp,%rax,8)
	movzwl	%cx, %eax
	movzwl	-512(%rbp,%rax,2), %edx
	addl	$1, %edx
	cmpw	(%r14,%rax,2), %dx
	jnb	.L831
.L835:
	movw	%dx, -512(%rbp,%rax,2)
	movq	-432(%rbp,%rax,8), %rdx
	.p2align 4,,10
	.p2align 3
.L832:
	addq	$1, %rdx
	cmpb	$0, -1(%rdx)
	jne	.L832
	movq	%rdx, -432(%rbp,%rax,8)
	movq	%rbx, %r13
	testw	%si, %si
	je	.L833
	xorl	%edx, %edx
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L897:
	movl	$100, %edx
	cmpw	%dx, %si
	jbe	.L896
.L834:
	movzwl	%dx, %eax
	movq	-432(%rbp,%rax,8), %rax
	leaq	1(%rax), %rcx
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L889
	.p2align 4,,10
	.p2align 3
.L836:
	addq	$1, %r13
	cmpb	%al, -1(%r13)
	jne	.L897
	movzbl	(%rcx), %eax
	addq	$1, %rcx
	testb	%al, %al
	jne	.L836
	.p2align 4,,10
	.p2align 3
.L889:
	addl	$1, %edx
	cmpw	%dx, %si
	ja	.L834
	.p2align 4,,10
	.p2align 3
.L896:
	cmpw	$98, %dx
	jbe	.L833
.L839:
	addl	$1, %r12d
	cmpl	%r12d, %r11d
	jne	.L830
	movq	-560(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L815:
	movzwl	10(%r13), %eax
	addq	%rax, %r13
	subl	$1, %r15d
	jne	.L841
.L840:
	movq	-552(%rbp), %rax
	xorl	%ecx, %ecx
	movl	$1114112, %edx
	xorl	%esi, %esi
	movl	-540(%rbp), %r9d
	movq	-584(%rbp), %rdi
	leaq	-528(%rbp), %r8
	movl	$65535, -520(%rbp)
	movq	%rax, -528(%rbp)
	call	_ZN6icu_67L9enumNamesEPNS_10UCharNamesEiiPFaPvi15UCharNameChoicePKciES2_S3_
	movl	-520(%rbp), %r12d
	cmpl	$65535, %r12d
	jne	.L784
	movq	-536(%rbp), %rax
	movl	$12, (%rax)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L833:
	cmpb	$0, 0(%r13)
	jne	.L839
	movq	-560(%rbp), %r13
.L825:
	cmpl	$65535, %r12d
	jne	.L784
	movzwl	10(%r13), %eax
	addq	%rax, %r13
	subl	$1, %r15d
	jne	.L841
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L816:
	movq	-552(%rbp), %rax
	leaq	12(%r13), %rdx
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L898:
	addq	$1, %rax
	cmpb	%sil, %cl
	jne	.L815
.L820:
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	movzbl	(%rax), %esi
	testb	%cl, %cl
	jne	.L898
	movzbl	9(%r13), %edx
	testw	%dx, %dx
	je	.L849
	subl	$1, %edx
	xorl	%r12d, %r12d
	movzwl	%dx, %edx
	movl	%r12d, %edi
	leaq	1(%rax,%rdx), %rsi
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L900:
	sall	$4, %edi
	movl	%edi, %r8d
	movsbl	%dl, %edi
	leal	-48(%rdi), %edx
	movl	%edx, %edi
	orl	%r8d, %edi
.L823:
	cmpq	%rsi, %rax
	je	.L899
.L824:
	movq	%rax, %rcx
	movzbl	(%rax), %edx
	addq	$1, %rax
	leal	-48(%rdx), %r8d
	cmpb	$9, %r8b
	jbe	.L900
	leal	-65(%rdx), %r8d
	cmpb	$5, %r8b
	ja	.L815
	sall	$4, %edi
	movsbl	%dl, %r8d
	movl	%edi, %r9d
	leal	-55(%r8), %edi
	orl	%r9d, %edi
	jmp	.L823
	.p2align 4,,10
	.p2align 3
.L899:
	movzbl	1(%rcx), %esi
	movl	%edi, %r12d
.L821:
	testb	%sil, %sil
	jne	.L815
	cmpl	%r12d, 0(%r13)
	ja	.L815
	cmpl	4(%r13), %r12d
	ja	.L815
	jmp	.L825
.L903:
	cmpl	$64975, %r12d
	jle	.L807
	movb	$30, -540(%rbp)
	cmpl	$65007, %r12d
	jle	.L808
	movl	%r12d, %eax
	andl	$65534, %eax
	cmpl	$65534, %eax
	je	.L808
.L807:
	movl	%r12d, %edi
	call	u_charType_67@PLT
	movb	%al, -540(%rbp)
	cmpb	$18, %al
	jne	.L808
	movl	%r12d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	setne	%r13b
	leal	31(%r13), %eax
	movb	%al, -540(%rbp)
.L808:
	movb	$0, -176(%rbp,%rbx)
	xorl	%r15d, %r15d
	leaq	-175(%rbp), %r14
	leaq	_ZN6icu_67L12charCatNamesE(%rip), %r13
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L810:
	addq	$1, %r15
	cmpq	$33, %r15
	je	.L795
.L811:
	movq	0(%r13,%r15,8), %rsi
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L810
	movzbl	-540(%rbp), %r13d
	cmpl	%r15d, %r13d
	je	.L784
	.p2align 4,,10
	.p2align 3
.L795:
	movq	-536(%rbp), %rax
	movl	$65535, %r12d
	movl	$12, (%rax)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L894:
	cmpl	$2, -540(%rbp)
	jne	.L795
	leal	-1(%rbx), %eax
	cmpb	$62, -176(%rbp,%rax)
	movq	%rax, %rsi
	movl	%eax, %ebx
	je	.L801
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L902:
	leal	-1(%rbx), %ecx
	cmpb	$45, -176(%rbp,%rcx)
	movq	%rcx, %rax
	je	.L901
	movl	%eax, %ebx
.L801:
	cmpl	$2, %ebx
	ja	.L902
	jne	.L795
	cmpb	$45, -174(%rbp)
	jne	.L795
	subl	$3, %edx
	movl	$2, %eax
	movl	$3, %ebx
.L842:
	subl	$2, %edx
	cmpl	$7, %edx
	ja	.L795
	movb	$0, -176(%rbp,%rax)
	cmpl	%ebx, %esi
	jbe	.L803
	movl	%ebx, %eax
	xorl	%r12d, %r12d
	leaq	-176(%rbp,%rax), %rdx
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L904:
	sall	$4, %r12d
	leal	-48(%r12,%rax), %r12d
.L805:
	cmpl	$1114111, %r12d
	jg	.L795
	addl	$1, %ebx
	addq	$1, %rdx
	cmpl	%ebx, %esi
	jbe	.L903
.L806:
	movsbl	(%rdx), %eax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L904
	leal	-97(%rax), %ecx
	cmpb	$5, %cl
	ja	.L795
	sall	$4, %r12d
	leal	-87(%r12,%rax), %r12d
	jmp	.L805
.L901:
	subl	%ebx, %edx
	jmp	.L842
.L849:
	xorl	%r12d, %r12d
	jmp	.L821
.L791:
	movq	%rax, %rdi
	call	udata_getMemory_67@PLT
	movq	%rax, _ZN6icu_67L10uCharNamesE(%rip)
	jmp	.L792
.L892:
	call	__stack_chk_fail@PLT
.L803:
	xorl	%edi, %edi
	xorl	%r12d, %r12d
	call	u_charType_67@PLT
	movb	%al, -540(%rbp)
	cmpb	$18, %al
	jne	.L808
	movb	$32, -540(%rbp)
	jmp	.L808
	.cfi_endproc
.LFE2795:
	.size	u_charFromName_67, .-u_charFromName_67
	.p2align 4
	.globl	u_enumCharNames_67
	.type	u_enumCharNames_67, @function
u_enumCharNames_67:
.LFB2796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -56(%rbp)
	testq	%r9, %r9
	je	.L905
	movl	(%r9), %r10d
	testl	%r10d, %r10d
	jg	.L905
	movl	%r8d, %ebx
	cmpl	$3, %r8d
	jg	.L929
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L929
	movl	%esi, -52(%rbp)
	movl	%edi, %r12d
	movq	%rcx, %r15
	cmpl	$1114112, %esi
	jbe	.L911
	movl	$1114112, -52(%rbp)
	movl	$1114112, -56(%rbp)
.L911:
	cmpl	-52(%rbp), %r12d
	jb	.L951
.L905:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L951:
	.cfi_restore_state
	movl	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L952
.L912:
	movl	4+_ZN6icu_67L18gCharNamesInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L915
	movl	%eax, (%r9)
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L929:
	movl	$1, (%r9)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L952:
	.cfi_restore_state
	leaq	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %rdi
	movq	%r9, -64(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L912
	leaq	_ZN6icu_67L9DATA_NAMEE(%rip), %rdx
	xorl	%r8d, %r8d
	leaq	_ZN6icu_67L12isAcceptableEPvPKcS2_PK9UDataInfo(%rip), %rcx
	xorl	%edi, %edi
	leaq	_ZN6icu_67L9DATA_TYPEE(%rip), %rsi
	call	udata_openChoice_67@PLT
	movq	-64(%rbp), %r9
	movq	%rax, _ZN6icu_67L14uCharNamesDataE(%rip)
	movl	(%r9), %edx
	testl	%edx, %edx
	jle	.L913
	movq	$0, _ZN6icu_67L14uCharNamesDataE(%rip)
.L914:
	leaq	_ZN6icu_67L14unames_cleanupEv(%rip), %rsi
	movl	$16, %edi
	movq	%r9, -64(%rbp)
	call	ucln_common_registerCleanup_67@PLT
	movq	-64(%rbp), %r9
	leaq	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %rdi
	movl	(%r9), %eax
	movl	%eax, 4+_ZN6icu_67L18gCharNamesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	-64(%rbp), %r9
.L915:
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L905
	movq	_ZN6icu_67L10uCharNamesE(%rip), %rdi
	movl	12(%rdi), %r13d
	addq	%rdi, %r13
	movl	0(%r13), %r10d
	addq	$4, %r13
	testl	%r10d, %r10d
	je	.L916
	testl	$-3, %ebx
	jne	.L917
	.p2align 4,,10
	.p2align 3
.L924:
	movl	0(%r13), %edx
	cmpl	%edx, %r12d
	jnb	.L918
	movl	%r10d, -64(%rbp)
	movq	_ZN6icu_67L10uCharNamesE(%rip), %rdi
	cmpl	-52(%rbp), %edx
	jnb	.L916
	movl	%ebx, %r9d
	movq	%r15, %r8
	movq	%r14, %rcx
	movl	%r12d, %esi
	call	_ZN6icu_67L9enumNamesEPNS_10UCharNamesEiiPFaPvi15UCharNameChoicePKciES2_S3_
	testb	%al, %al
	je	.L905
	movl	0(%r13), %r12d
	movl	-64(%rbp), %r10d
.L918:
	movl	4(%r13), %edx
	cmpl	%r12d, %edx
	jb	.L921
	movl	%r10d, -64(%rbp)
	addl	$1, %edx
	movl	%ebx, %r9d
	movq	%r15, %r8
	movq	%r14, %rcx
	cmpl	-52(%rbp), %edx
	jnb	.L922
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_67L12enumAlgNamesEPNS_16AlgorithmicRangeEiiPFaPvi15UCharNameChoicePKciES2_S3_.part.0
	testb	%al, %al
	je	.L905
	movl	4(%r13), %eax
	movl	-64(%rbp), %r10d
	leal	1(%rax), %r12d
.L921:
	movzwl	10(%r13), %eax
	addq	%rax, %r13
	subl	$1, %r10d
	jne	.L924
.L923:
	movq	_ZN6icu_67L10uCharNamesE(%rip), %rdi
.L916:
	movl	-56(%rbp), %edx
	addq	$24, %rsp
	movl	%ebx, %r9d
	movq	%r15, %r8
	popq	%rbx
	movq	%r14, %rcx
	movl	%r12d, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_67L9enumNamesEPNS_10UCharNamesEiiPFaPvi15UCharNameChoicePKciES2_S3_
	.p2align 4,,10
	.p2align 3
.L917:
	.cfi_restore_state
	movl	0(%r13), %edx
	cmpl	%r12d, %edx
	jbe	.L925
	movq	_ZN6icu_67L10uCharNamesE(%rip), %rdi
	cmpl	-52(%rbp), %edx
	jnb	.L916
	movl	%ebx, %r9d
	movq	%r15, %r8
	movq	%r14, %rcx
	movl	%r12d, %esi
	movl	%r10d, -64(%rbp)
	call	_ZN6icu_67L9enumNamesEPNS_10UCharNamesEiiPFaPvi15UCharNameChoicePKciES2_S3_
	testb	%al, %al
	je	.L905
	movl	0(%r13), %r12d
	movl	-64(%rbp), %r10d
.L925:
	movl	4(%r13), %edi
	cmpl	%r12d, %edi
	jb	.L928
	leal	1(%rdi), %r12d
	cmpl	-52(%rbp), %r12d
	jnb	.L905
.L928:
	movzwl	10(%r13), %eax
	addq	%rax, %r13
	subl	$1, %r10d
	jne	.L917
	jmp	.L923
.L922:
	movl	-56(%rbp), %edx
	addq	$24, %rsp
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_67L12enumAlgNamesEPNS_16AlgorithmicRangeEiiPFaPvi15UCharNameChoicePKciES2_S3_.part.0
.L913:
	.cfi_restore_state
	movq	%rax, %rdi
	movq	%r9, -64(%rbp)
	call	udata_getMemory_67@PLT
	movq	-64(%rbp), %r9
	movq	%rax, _ZN6icu_67L10uCharNamesE(%rip)
	jmp	.L914
	.cfi_endproc
.LFE2796:
	.size	u_enumCharNames_67, .-u_enumCharNames_67
	.p2align 4
	.globl	uprv_getMaxCharNameLength_67
	.type	uprv_getMaxCharNameLength_67, @function
uprv_getMaxCharNameLength_67:
.LFB2797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movl	_ZN6icu_67L14gMaxNameLengthE(%rip), %r12d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	testl	%r12d, %r12d
	je	.L973
.L953:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L974
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L973:
	.cfi_restore_state
	movl	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L955
	leaq	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	jne	.L975
.L955:
	movl	4+_ZN6icu_67L18gCharNamesInitOnceE(%rip), %edx
	testl	%edx, %edx
	jg	.L953
.L958:
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L953
	leaq	_ZZN6icu_67L19calcNameSetsLengthsEP10UErrorCodeE8extChars(%rip), %rdx
	leaq	_ZN6icu_67L8gNameSetE(%rip), %rsi
	movl	$1, %edi
	leaq	19(%rdx), %r8
	.p2align 4,,10
	.p2align 3
.L961:
	movzbl	(%rdx), %ecx
	movl	%edi, %r10d
	addq	$1, %rdx
	movq	%rcx, %rax
	sall	%cl, %r10d
	shrq	$5, %rax
	andl	$7, %eax
	orl	%r10d, (%rsi,%rax,4)
	cmpq	%rdx, %r8
	jne	.L961
	call	_ZN6icu_67L19calcNameSetsLengthsEP10UErrorCode.part.0
	testb	%al, %al
	cmovne	_ZN6icu_67L14gMaxNameLengthE(%rip), %r12d
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L975:
	leaq	_ZN6icu_67L12isAcceptableEPvPKcS2_PK9UDataInfo(%rip), %rcx
	leaq	-28(%rbp), %r9
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	leaq	_ZN6icu_67L9DATA_NAMEE(%rip), %rdx
	leaq	_ZN6icu_67L9DATA_TYPEE(%rip), %rsi
	call	udata_openChoice_67@PLT
	movl	-28(%rbp), %ecx
	movq	%rax, _ZN6icu_67L14uCharNamesDataE(%rip)
	testl	%ecx, %ecx
	jle	.L956
	movq	$0, _ZN6icu_67L14uCharNamesDataE(%rip)
.L957:
	leaq	_ZN6icu_67L14unames_cleanupEv(%rip), %rsi
	movl	$16, %edi
	call	ucln_common_registerCleanup_67@PLT
	movl	-28(%rbp), %eax
	leaq	_ZN6icu_67L18gCharNamesInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L18gCharNamesInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L956:
	movq	%rax, %rdi
	call	udata_getMemory_67@PLT
	movq	%rax, _ZN6icu_67L10uCharNamesE(%rip)
	jmp	.L957
.L974:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2797:
	.size	uprv_getMaxCharNameLength_67, .-uprv_getMaxCharNameLength_67
	.p2align 4
	.globl	uprv_getCharNameCharacters_67
	.type	uprv_getCharNameCharacters_67, @function
uprv_getCharNameCharacters_67:
.LFB2799:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rsi
	jmp	_ZL13charSetToUSetPjPK9USetAdder.isra.0.constprop.0
	.cfi_endproc
.LFE2799:
	.size	uprv_getCharNameCharacters_67, .-uprv_getCharNameCharacters_67
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"uchar_swapNames(): data format %02x.%02x.%02x.%02x (format version %02x) is not recognized as unames.icu\n"
	.align 8
.LC22:
	.string	"uchar_swapNames(): too few bytes (%d after header) for unames.icu\n"
	.align 8
.LC23:
	.string	"out of memory swapping %u unames.icu tokens\n"
	.align 8
.LC24:
	.string	"uchar_swapNames(token strings) failed\n"
	.align 8
.LC25:
	.string	"uchar_swapNames(): too few bytes (%d after header) for unames.icu algorithmic range %u\n"
	.align 8
.LC26:
	.string	"uchar_swapNames(prefix string of algorithmic range %u) failed\n"
	.align 8
.LC27:
	.string	"uchar_swapNames(): unknown type %u of algorithmic range %u\n"
	.text
	.p2align 4
	.globl	uchar_swapNames_67
	.type	uchar_swapNames_67, @function
uchar_swapNames_67:
.LFB2801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1816, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	udata_swapDataHeader_67@PLT
	movl	%eax, -1764(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L977
	movl	(%r14), %r10d
	testl	%r10d, %r10d
	jg	.L977
	cmpw	$28277, 12(%rbx)
	movzbl	16(%rbx), %eax
	jne	.L979
	cmpw	$28001, 14(%rbx)
	jne	.L979
	cmpb	$1, %al
	jne	.L979
	movslq	-1764(%rbp), %rdx
	addq	%rdx, %rbx
	testl	%r13d, %r13d
	js	.L1058
	subl	-1764(%rbp), %r13d
	movl	%r13d, -1768(%rbp)
	cmpl	$19, %r13d
	jle	.L985
	movq	%rdx, -1776(%rbp)
	movl	12(%rbx), %edi
	call	*16(%r12)
	cmpl	%eax, -1768(%rbp)
	movq	-1776(%rbp), %rdx
	movl	%eax, -1808(%rbp)
	jb	.L985
	leaq	(%r15,%rdx), %rax
	movq	%rax, -1800(%rbp)
	cmpq	%rax, %rbx
	je	.L1024
	movslq	-1768(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	memcpy@PLT
.L1024:
	movl	(%rbx), %edi
	call	*16(%r12)
	movl	4(%rbx), %edi
	movl	%eax, -1816(%rbp)
	call	*16(%r12)
	movl	8(%rbx), %edi
	movl	%eax, -1812(%rbp)
	call	*16(%r12)
	movq	%r14, %r8
	movl	$16, %edx
	movq	%rbx, %rsi
	movq	-1800(%rbp), %r13
	movq	%r12, %rdi
	movl	%eax, -1820(%rbp)
	movq	%r13, %rcx
	call	*56(%r12)
	movzwl	16(%rbx), %edi
	call	*8(%r12)
	leaq	16(%r13), %rcx
	movq	%r12, %rdi
	leaq	16(%rbx), %rsi
	movl	%eax, %r15d
	movw	%ax, -1784(%rbp)
	movq	%r14, %r8
	movl	$2, %edx
	call	*48(%r12)
	movl	$512, %r13d
	movl	%r15d, %eax
	cmpw	$512, %r15w
	cmovbe	%eax, %r13d
	movzwl	%r13w, %edi
	movl	%edi, -1776(%rbp)
	testw	%r15w, %r15w
	je	.L1028
	xorl	%r13d, %r13d
	movq	%r14, -1792(%rbp)
	leaq	-1600(%rbp), %r15
	movq	%r13, %r14
	movl	%edi, %r13d
	.p2align 4,,10
	.p2align 3
.L988:
	movswl	18(%rbx,%r14,2), %esi
	movq	%r12, %rdi
	call	udata_readInt16_67@PLT
	movw	%ax, (%r15,%r14,2)
	addq	$1, %r14
	cmpl	%r14d, %r13d
	ja	.L988
	cmpl	$512, -1776(%rbp)
	movq	-1792(%rbp), %r14
	je	.L996
.L987:
	movl	-1776(%rbp), %r13d
	movl	$511, %eax
	subl	%r13d, %eax
	leal	1(%r13), %edx
	leaq	(%r15,%r13,2), %rcx
	cmpl	$512, %edx
	leaq	2(%rax,%rax), %rax
	movl	$2, %edx
	cmova	%rdx, %rax
	cmpl	$8, %eax
	jnb	.L992
	testb	$4, %al
	jne	.L1059
	testl	%eax, %eax
	je	.L996
	movb	$0, (%rcx)
	testb	$2, %al
	jne	.L1060
	.p2align 4,,10
	.p2align 3
.L996:
	movzwl	-1784(%rbp), %edi
	leaq	-576(%rbp), %r13
	movq	%r14, %r8
	movq	%r15, %rsi
	movq	%r13, %rcx
	movl	%edi, %edx
	movl	%edi, -1792(%rbp)
	movq	%r12, %rdi
	call	_ZL12makeTokenMapPK12UDataSwapperPstPhP10UErrorCode
	movl	$256, %edx
	movq	%r12, %rdi
	movq	%r14, %r8
	movzwl	-1784(%rbp), %eax
	leaq	-320(%rbp), %rcx
	leaq	-1088(%rbp), %rsi
	movq	%rcx, -1776(%rbp)
	cmpw	$256, %ax
	cmovnb	%eax, %edx
	subw	$256, %dx
	movzwl	%dx, %edx
	call	_ZL12makeTokenMapPK12UDataSwapperPstPhP10UErrorCode
	movl	(%r14), %edi
	testl	%edi, %edi
	jle	.L1061
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L977:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1062
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L979:
	.cfi_restore_state
	subq	$8, %rsp
	movzbl	13(%rbx), %ecx
	movzbl	12(%rbx), %edx
	movq	%r12, %rdi
	pushq	%rax
	movzbl	15(%rbx), %r9d
	xorl	%eax, %eax
	leaq	.LC21(%rip), %rsi
	movzbl	14(%rbx), %r8d
	call	udata_printError_67@PLT
	movl	$16, (%r14)
	xorl	%eax, %eax
	popq	%r8
	popq	%r9
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1058:
	movl	12(%rbx), %edi
	call	*16(%r12)
	movl	%eax, %r13d
	movl	%eax, %eax
	movl	(%rbx,%rax), %edi
	addl	$4, %r13d
	call	*16(%r12)
	movl	%eax, %r14d
	testl	%eax, %eax
	je	.L982
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L983:
	movl	%r13d, %eax
	addl	$1, %r15d
	movzwl	10(%rbx,%rax), %edi
	call	*8(%r12)
	movzwl	%ax, %eax
	addl	%eax, %r13d
	cmpl	%r15d, %r14d
	jne	.L983
.L982:
	movl	-1764(%rbp), %eax
	addl	%r13d, %eax
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1061:
	movl	-1792(%rbp), %eax
	leal	(%rax,%rax), %r15d
	movslq	%r15d, %rax
	movq	%rax, %rdi
	movq	%rax, -1840(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, -1832(%rbp)
	testq	%rax, %rax
	je	.L1063
	movl	-1792(%rbp), %esi
	testl	%esi, %esi
	je	.L1002
	movl	-1792(%rbp), %eax
	movq	%r13, %r15
	leaq	18(%rbx), %r13
	movq	%rbx, -1848(%rbp)
	movq	%r14, %rbx
	movq	%r13, %r14
	movq	-1832(%rbp), %r13
	subl	$1, %eax
	addq	%r15, %rax
	movq	%rax, -1784(%rbp)
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1053:
	addq	$1, %r15
	addq	$2, %r14
	cmpq	%r15, -1776(%rbp)
	je	.L1064
.L1001:
	movzbl	(%r15), %eax
	movq	%rbx, %r8
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	0(%r13,%rax,2), %rcx
	call	*48(%r12)
	cmpq	%r15, -1784(%rbp)
	jne	.L1053
	movq	%rbx, %r14
	movq	-1848(%rbp), %rbx
.L1002:
	movq	-1800(%rbp), %r15
	movq	-1832(%rbp), %r13
	movq	-1840(%rbp), %rdx
	movq	%r13, %rsi
	leaq	18(%r15), %rdi
	call	memcpy@PLT
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movl	-1816(%rbp), %eax
	movq	%r14, %r8
	movq	%r12, %rdi
	movl	-1812(%rbp), %edx
	leaq	(%r15,%rax), %rcx
	leaq	(%rbx,%rax), %rsi
	subl	%eax, %edx
	call	udata_swapInvStringBlock_67@PLT
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L1065
	movl	-1812(%rbp), %r13d
	leaq	(%rbx,%r13), %r15
	movzwl	(%r15), %edi
	call	*8(%r12)
	movq	%r14, %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-1800(%rbp), %rcx
	movzwl	%ax, %eax
	leal	(%rax,%rax,2), %eax
	addq	%r13, %rcx
	leal	2(%rax,%rax), %edx
	call	*48(%r12)
	movzbl	3(%r12), %eax
	cmpb	%al, 1(%r12)
	je	.L1006
	movl	-1820(%rbp), %r15d
	movl	-1808(%rbp), %r13d
	movq	%r15, %rax
	leaq	(%rbx,%r15), %rdi
	addq	-1800(%rbp), %r15
	subl	%eax, %r13d
	cmpl	$32, %r13d
	jbe	.L1006
	leaq	-1680(%rbp), %rax
	leaq	-1760(%rbp), %rsi
	movq	%rbx, -1776(%rbp)
	movq	%r15, %rbx
	movq	%r12, -1784(%rbp)
	movq	%rax, %r15
	movl	%r13d, %r12d
	movq	%rsi, %r13
	movq	%r14, -1792(%rbp)
	movq	%rdi, %r14
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_67L18expandGroupLengthsEPKhPtS2_
	movzwl	-1698(%rbp), %edx
	movzwl	-1618(%rbp), %edi
	movq	%rax, %rsi
	subq	%r14, %rsi
	addl	%edi, %edx
	addq	%rsi, %rbx
	addl	%edx, %esi
	subl	%esi, %r12d
	testl	%edx, %edx
	je	.L1030
	.p2align 4,,10
	.p2align 3
.L1009:
	movzbl	(%rax), %ecx
	movzbl	-576(%rbp,%rcx), %esi
	cmpw	$-2, -1600(%rbp,%rcx,2)
	movb	%sil, (%rbx)
	je	.L1011
	addq	$1, %rax
	addq	$1, %rbx
	subl	$1, %edx
	jne	.L1009
.L1030:
	movq	%rax, %r14
.L1008:
	cmpl	$32, %r12d
	ja	.L1010
	movq	-1776(%rbp), %rbx
	movq	-1784(%rbp), %r12
	movq	-1792(%rbp), %r14
.L1006:
	movl	-1808(%rbp), %r13d
	leaq	(%rbx,%r13), %r15
	movl	(%r15), %edi
	call	*16(%r12)
	movl	$4, %edx
	movq	%r15, %rsi
	movq	%r14, %r8
	movq	-1800(%rbp), %rdi
	movl	%eax, -1792(%rbp)
	leaq	(%rdi,%r13), %rcx
	movq	%r12, %rdi
	call	*56(%r12)
	movl	-1808(%rbp), %esi
	movl	-1792(%rbp), %edx
	leal	4(%rsi), %r13d
	testl	%edx, %edx
	je	.L982
	cmpl	%r13d, -1768(%rbp)
	movl	$0, -1776(%rbp)
	jb	.L1014
	movq	%rbx, -1784(%rbp)
.L1015:
	movq	-1784(%rbp), %rax
	movl	%r13d, %r15d
	leaq	(%rax,%r15), %rbx
	addq	-1800(%rbp), %r15
	movzwl	10(%rbx), %edi
	call	*8(%r12)
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$8, %edx
	movzwl	%ax, %eax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	addl	%eax, %r13d
	call	*56(%r12)
	leaq	10(%r15), %rcx
	leaq	10(%rbx), %rsi
	movq	%r14, %r8
	movl	$2, %edx
	movq	%r12, %rdi
	call	*48(%r12)
	movzbl	8(%rbx), %eax
	testb	%al, %al
	je	.L1016
	cmpb	$1, %al
	je	.L1017
	movl	-1776(%rbp), %ecx
	movzbl	%al, %edx
	leaq	.LC27(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	udata_printError_67@PLT
	movl	$16, (%r14)
	xorl	%eax, %eax
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1011:
	leaq	2(%rax), %r14
	movzbl	1(%rax), %eax
	leaq	2(%rbx), %rsi
	movzbl	-320(%rbp,%rax), %eax
	movb	%al, 1(%rbx)
	subl	$2, %edx
	je	.L1031
	movq	%r14, %rax
	movq	%rsi, %rbx
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L985:
	movl	-1768(%rbp), %edx
	xorl	%eax, %eax
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	call	udata_printError_67@PLT
	movl	$8, (%r14)
	xorl	%eax, %eax
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%rsi, %rbx
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L992:
	leaq	8(%rcx), %rdi
	movl	%eax, %edx
	movq	$0, (%rcx)
	movq	$0, -8(%rcx,%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	leal	(%rax,%rcx), %edx
	xorl	%eax, %eax
	shrl	$3, %edx
	movl	%edx, %ecx
	rep stosq
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1017:
	movzbl	9(%rbx), %edx
	leaq	12(%rbx), %rsi
	addq	$12, %r15
	movq	%r14, %r8
	movq	%rsi, -1808(%rbp)
	movq	%r15, %rcx
	movq	%r12, %rdi
	movb	%dl, -1812(%rbp)
	addl	%edx, %edx
	call	*48(%r12)
	movzbl	-1812(%rbp), %eax
	movq	-1808(%rbp), %rsi
	addq	%rax, %rax
	addq	%rax, %rsi
	addq	%rax, %r15
	movq	-1784(%rbp), %rax
	leal	0(%r13,%rax), %edx
	subl	%esi, %edx
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1066:
	leal	-1(%rdx), %ecx
	cmpb	$0, (%rsi,%rcx)
	movq	%rcx, %rax
	je	.L1020
	movl	%eax, %edx
.L1021:
	testl	%edx, %edx
	jne	.L1066
	xorl	%edx, %edx
.L1020:
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	*72(%r12)
.L1019:
	addl	$1, -1776(%rbp)
	movl	-1776(%rbp), %eax
	cmpl	%eax, -1792(%rbp)
	je	.L982
	cmpl	%r13d, -1768(%rbp)
	jnb	.L1015
.L1014:
	movl	-1776(%rbp), %ecx
	movl	-1768(%rbp), %edx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC25(%rip), %rsi
	call	udata_printError_67@PLT
	movl	$8, (%r14)
	xorl	%eax, %eax
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1016:
	addq	$12, %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	12(%r15), %rcx
	movq	%r14, %r8
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	*72(%r12)
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L1019
	movl	-1776(%rbp), %edx
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	udata_printError_67@PLT
	xorl	%eax, %eax
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1028:
	movl	$0, -1776(%rbp)
	leaq	-1600(%rbp), %r15
	jmp	.L987
.L1065:
	xorl	%eax, %eax
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	udata_printError_67@PLT
	xorl	%eax, %eax
	jmp	.L977
.L1064:
	cmpl	$256, -1792(%rbp)
	movq	%rbx, %r14
	movq	-1848(%rbp), %rbx
	jbe	.L1002
	leaq	530(%rbx), %rsi
	movq	%rbx, -1776(%rbp)
	movq	%r12, %r15
	movq	-1832(%rbp), %rbx
	movl	$256, %r13d
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L1005:
	movzbl	%r13b, %eax
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r14, %r8
	movzbl	-320(%rbp,%rax), %eax
	movq	%r15, %rdi
	addl	$1, %r13d
	addq	$2, %r12
	andl	$-256, %edx
	addq	%rdx, %rax
	movl	$2, %edx
	leaq	(%rbx,%rax,2), %rcx
	call	*48(%r15)
	cmpl	-1792(%rbp), %r13d
	jne	.L1005
	movq	-1776(%rbp), %rbx
	movq	%r15, %r12
	jmp	.L1002
.L1059:
	movl	%eax, %eax
	movl	$0, (%rcx)
	movl	$0, -4(%rcx,%rax)
	jmp	.L996
.L1060:
	movl	%eax, %eax
	movw	$0, -2(%rcx,%rax)
	jmp	.L996
.L1062:
	call	__stack_chk_fail@PLT
.L1063:
	movl	-1792(%rbp), %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	udata_printError_67@PLT
	movl	$7, (%r14)
	xorl	%eax, %eax
	jmp	.L977
	.cfi_endproc
.LFE2801:
	.size	uchar_swapNames_67, .-uchar_swapNames_67
	.section	.rodata
	.align 16
	.type	_ZZN6icu_67L19calcNameSetsLengthsEP10UErrorCodeE8extChars, @object
	.size	_ZZN6icu_67L19calcNameSetsLengthsEP10UErrorCodeE8extChars, 20
_ZZN6icu_67L19calcNameSetsLengthsEP10UErrorCodeE8extChars:
	.string	"0123456789ABCDEF<>-"
	.section	.rodata.str1.1
.LC28:
	.string	"unassigned"
.LC29:
	.string	"uppercase letter"
.LC30:
	.string	"lowercase letter"
.LC31:
	.string	"titlecase letter"
.LC32:
	.string	"modifier letter"
.LC33:
	.string	"other letter"
.LC34:
	.string	"non spacing mark"
.LC35:
	.string	"enclosing mark"
.LC36:
	.string	"combining spacing mark"
.LC37:
	.string	"decimal digit number"
.LC38:
	.string	"letter number"
.LC39:
	.string	"other number"
.LC40:
	.string	"space separator"
.LC41:
	.string	"line separator"
.LC42:
	.string	"paragraph separator"
.LC43:
	.string	"control"
.LC44:
	.string	"format"
.LC45:
	.string	"private use area"
.LC46:
	.string	"surrogate"
.LC47:
	.string	"dash punctuation"
.LC48:
	.string	"start punctuation"
.LC49:
	.string	"end punctuation"
.LC50:
	.string	"connector punctuation"
.LC51:
	.string	"other punctuation"
.LC52:
	.string	"math symbol"
.LC53:
	.string	"currency symbol"
.LC54:
	.string	"modifier symbol"
.LC55:
	.string	"other symbol"
.LC56:
	.string	"initial punctuation"
.LC57:
	.string	"final punctuation"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZN6icu_67L12charCatNamesE, @object
	.size	_ZN6icu_67L12charCatNamesE, 264
_ZN6icu_67L12charCatNamesE:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC18
	.quad	.LC20
	.quad	.LC19
	.local	_ZN6icu_67L8gNameSetE
	.comm	_ZN6icu_67L8gNameSetE,32,32
	.local	_ZN6icu_67L14gMaxNameLengthE
	.comm	_ZN6icu_67L14gMaxNameLengthE,4,4
	.local	_ZN6icu_67L18gCharNamesInitOnceE
	.comm	_ZN6icu_67L18gCharNamesInitOnceE,8,8
	.local	_ZN6icu_67L10uCharNamesE
	.comm	_ZN6icu_67L10uCharNamesE,8,8
	.local	_ZN6icu_67L14uCharNamesDataE
	.comm	_ZN6icu_67L14uCharNamesDataE,8,8
	.section	.rodata
	.type	_ZN6icu_67L9DATA_TYPEE, @object
	.size	_ZN6icu_67L9DATA_TYPEE, 4
_ZN6icu_67L9DATA_TYPEE:
	.string	"icu"
	.type	_ZN6icu_67L9DATA_NAMEE, @object
	.size	_ZN6icu_67L9DATA_NAMEE, 7
_ZN6icu_67L9DATA_NAMEE:
	.string	"unames"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.align 16
.LC1:
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.align 16
.LC2:
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.align 16
.LC3:
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.align 16
.LC4:
	.byte	64
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	72
	.byte	73
	.byte	74
	.byte	75
	.byte	76
	.byte	77
	.byte	78
	.byte	79
	.align 16
.LC5:
	.byte	80
	.byte	81
	.byte	82
	.byte	83
	.byte	84
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	90
	.byte	91
	.byte	92
	.byte	93
	.byte	94
	.byte	95
	.align 16
.LC6:
	.byte	96
	.byte	97
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
	.byte	103
	.byte	104
	.byte	105
	.byte	106
	.byte	107
	.byte	108
	.byte	109
	.byte	110
	.byte	111
	.align 16
.LC7:
	.byte	112
	.byte	113
	.byte	114
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	121
	.byte	122
	.byte	123
	.byte	124
	.byte	125
	.byte	126
	.byte	127
	.align 16
.LC8:
	.byte	-128
	.byte	-127
	.byte	-126
	.byte	-125
	.byte	-124
	.byte	-123
	.byte	-122
	.byte	-121
	.byte	-120
	.byte	-119
	.byte	-118
	.byte	-117
	.byte	-116
	.byte	-115
	.byte	-114
	.byte	-113
	.align 16
.LC9:
	.byte	-112
	.byte	-111
	.byte	-110
	.byte	-109
	.byte	-108
	.byte	-107
	.byte	-106
	.byte	-105
	.byte	-104
	.byte	-103
	.byte	-102
	.byte	-101
	.byte	-100
	.byte	-99
	.byte	-98
	.byte	-97
	.align 16
.LC10:
	.byte	-96
	.byte	-95
	.byte	-94
	.byte	-93
	.byte	-92
	.byte	-91
	.byte	-90
	.byte	-89
	.byte	-88
	.byte	-87
	.byte	-86
	.byte	-85
	.byte	-84
	.byte	-83
	.byte	-82
	.byte	-81
	.align 16
.LC11:
	.byte	-80
	.byte	-79
	.byte	-78
	.byte	-77
	.byte	-76
	.byte	-75
	.byte	-74
	.byte	-73
	.byte	-72
	.byte	-71
	.byte	-70
	.byte	-69
	.byte	-68
	.byte	-67
	.byte	-66
	.byte	-65
	.align 16
.LC12:
	.byte	-64
	.byte	-63
	.byte	-62
	.byte	-61
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	-54
	.byte	-53
	.byte	-52
	.byte	-51
	.byte	-50
	.byte	-49
	.align 16
.LC13:
	.byte	-48
	.byte	-47
	.byte	-46
	.byte	-45
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	-40
	.byte	-39
	.byte	-38
	.byte	-37
	.byte	-36
	.byte	-35
	.byte	-34
	.byte	-33
	.align 16
.LC14:
	.byte	-32
	.byte	-31
	.byte	-30
	.byte	-29
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	-22
	.byte	-21
	.byte	-20
	.byte	-19
	.byte	-18
	.byte	-17
	.align 16
.LC15:
	.byte	-16
	.byte	-15
	.byte	-14
	.byte	-13
	.byte	-12
	.byte	-11
	.byte	-10
	.byte	-9
	.byte	-8
	.byte	-7
	.byte	-6
	.byte	-5
	.byte	-4
	.byte	-3
	.byte	-2
	.byte	-1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
