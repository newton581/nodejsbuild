	.file	"utrie2_builder.cpp"
	.text
	.p2align 4
	.type	_ZL12getDataBlockP9UNewTrie2ia, @function
_ZL12getDataBlockP9UNewTrie2ia:
.LFB2753:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$-1024, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$55296, %eax
	jne	.L19
	testb	%dl, %dl
	jne	.L16
.L19:
	movl	%esi, %eax
	movslq	144160(%rbx), %rcx
	sarl	$11, %eax
	cltq
	leaq	(%rbx,%rax,4), %rdi
	movl	(%rdi), %eax
	cmpl	%ecx, %eax
	je	.L28
	testl	%eax, %eax
	js	.L7
.L2:
	sarl	$5, %esi
	andl	$63, %esi
	addl	%eax, %esi
	movslq	%esi, %rsi
	leaq	(%rbx,%rsi,4), %r13
	movl	2176(%r13), %r14d
	cmpl	144164(%rbx), %r14d
	je	.L8
	movl	%r14d, %eax
	movl	%r14d, %r12d
	sarl	$5, %eax
	cltq
	cmpl	$1, 144176(%rbx,%rax,4)
	je	.L1
.L8:
	movl	144156(%rbx), %r12d
	testl	%r12d, %r12d
	je	.L9
	movl	%r12d, %ecx
	movq	144128(%rbx), %r15
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	144176(%rbx,%rcx,4), %eax
	negl	%eax
	movl	%eax, 144156(%rbx)
.L10:
	movslq	%r14d, %rdx
	movslq	%r12d, %rax
	leaq	(%r15,%rdx,4), %rdx
	leaq	(%r15,%rax,4), %rax
	movdqu	(%rdx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rdx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rdx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rdx), %xmm4
	movups	%xmm4, 64(%rax)
	movdqu	80(%rdx), %xmm5
	movups	%xmm5, 80(%rax)
	movdqu	96(%rdx), %xmm6
	movups	%xmm6, 96(%rax)
	movdqu	112(%rdx), %xmm7
	movups	%xmm7, 112(%rax)
	testl	%r12d, %r12d
	js	.L29
	movl	$1, 144176(%rbx,%rcx,4)
	movl	2176(%r13), %ecx
	movl	%ecx, %eax
	sarl	$5, %eax
	cltq
	leaq	(%rbx,%rax,4), %rdx
	subl	$1, 144176(%rdx)
	jne	.L15
	movl	144156(%rbx), %eax
	negl	%eax
	movl	%eax, 144176(%rdx)
	movl	%ecx, 144156(%rbx)
.L15:
	movl	%r12d, 2176(%r13)
.L1:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	144152(%rbx), %r12d
	movl	144148(%rbx), %eax
	leal	32(%r12), %ecx
	cmpl	%eax, %ecx
	jle	.L30
	cmpl	$131071, %eax
	jle	.L18
	cmpl	$1115263, %eax
	jg	.L7
	movl	$1115264, -64(%rbp)
	movl	$4461056, %edi
.L13:
	movl	%ecx, -60(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L7
	movq	144128(%rbx), %r9
	movslq	144152(%rbx), %rdx
	movq	%rax, %rdi
	movq	%r9, %rsi
	salq	$2, %rdx
	movq	%r9, -56(%rbp)
	call	memcpy@PLT
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	movl	-64(%rbp), %eax
	movq	%r15, 144128(%rbx)
	movl	-60(%rbp), %ecx
	movl	%eax, 144148(%rbx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L30:
	movq	144128(%rbx), %r15
.L12:
	movl	%ecx, 144152(%rbx)
	movl	%r12d, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$2048, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L28:
	movl	144144(%rbx), %eax
	leal	64(%rax), %edx
	cmpl	$35488, %edx
	jg	.L7
	leaq	2176(%rbx), %r8
	movl	%edx, 144144(%rbx)
	movslq	%eax, %rdx
	leaq	(%r8,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rdx
	movdqu	(%rcx), %xmm0
	movups	%xmm0, (%rdx)
	movdqu	16(%rcx), %xmm1
	movups	%xmm1, 16(%rdx)
	movdqu	32(%rcx), %xmm2
	movups	%xmm2, 32(%rdx)
	movdqu	48(%rcx), %xmm3
	movups	%xmm3, 48(%rdx)
	movdqu	64(%rcx), %xmm4
	movups	%xmm4, 64(%rdx)
	movdqu	80(%rcx), %xmm5
	movups	%xmm5, 80(%rdx)
	movdqu	96(%rcx), %xmm6
	movups	%xmm6, 96(%rdx)
	movdqu	112(%rcx), %xmm7
	movups	%xmm7, 112(%rdx)
	movdqu	128(%rcx), %xmm0
	movups	%xmm0, 128(%rdx)
	movdqu	144(%rcx), %xmm1
	movups	%xmm1, 144(%rdx)
	movdqu	160(%rcx), %xmm2
	movups	%xmm2, 160(%rdx)
	movdqu	176(%rcx), %xmm3
	movups	%xmm3, 176(%rdx)
	movdqu	192(%rcx), %xmm4
	movups	%xmm4, 192(%rdx)
	movdqu	208(%rcx), %xmm5
	movups	%xmm5, 208(%rdx)
	movdqu	224(%rcx), %xmm6
	movups	%xmm6, 224(%rdx)
	movdqu	240(%rcx), %xmm7
	movups	%xmm7, 240(%rdx)
	testl	%eax, %eax
	js	.L7
	movl	%eax, (%rdi)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$131072, -64(%rbp)
	movl	$524288, %edi
	jmp	.L13
.L29:
	movl	$0, 144176(%rbx,%rcx,4)
.L7:
	movl	$-1, %r12d
	jmp	.L1
	.cfi_endproc
.LFE2753:
	.size	_ZL12getDataBlockP9UNewTrie2ia, .-_ZL12getDataBlockP9UNewTrie2ia
	.p2align 4
	.type	utrie2_setRange32_67.part.0, @function
utrie2_setRange32_67.part.0:
.LFB3441:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	72(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L32
	cmpb	$0, 144172(%rbx)
	jne	.L32
	movl	%ecx, %r13d
	movl	%esi, %r15d
	movl	%r8d, %ecx
	testb	%r8b, %r8b
	je	.L108
.L35:
	movl	%r15d, %r12d
	leal	1(%rdx), %r14d
	andl	$31, %r12d
	jne	.L109
	movl	%r14d, %eax
	movl	144136(%rbx), %edx
	andl	$31, %eax
	movl	%eax, -52(%rbp)
.L40:
	andl	$-32, %r14d
	movl	$-1, %r11d
	cmpl	%edx, %r13d
	jne	.L54
	movl	144164(%rbx), %r11d
.L54:
	cmpl	%r15d, %r14d
	jle	.L55
	testb	%r8b, %r8b
	movd	%r13d, %xmm3
	movl	%r15d, %r12d
	movl	%ecx, %r15d
	setne	%r10b
	movq	%rbx, %rcx
	pshufd	$0, %xmm3, %xmm0
	movl	%r10d, %ebx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L112:
	movl	%r12d, %edx
	movslq	144164(%rcx), %rsi
	sarl	$5, %edx
	cmpl	$55296, %eax
	je	.L110
	movl	%r12d, %r10d
	andl	$63, %edx
	sarl	$11, %r10d
	movslq	%r10d, %r10
	movslq	(%rcx,%r10,4), %rdi
	leal	(%rdx,%rdi), %eax
	cltq
	cmpl	%esi, 2176(%rcx,%rax,4)
	je	.L69
	cmpl	%edi, 144160(%rcx)
	je	.L111
.L64:
	testl	%edi, %edi
	js	.L65
	movl	%r12d, %edx
	movslq	144164(%rcx), %rsi
	sarl	$5, %edx
.L60:
	andl	$63, %edx
	addl	%edi, %edx
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx,4), %rdi
	movslq	2176(%rdi), %rax
	cmpl	%esi, %eax
	je	.L66
	movl	%eax, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	cmpl	$1, 144176(%rcx,%rdx,4)
	je	.L67
	movq	144128(%rcx), %rsi
	movslq	%eax, %rdx
	cmpl	(%rsi,%rdx,4), %r13d
	je	.L69
	testb	%r15b, %r15b
	jne	.L70
.L69:
	addl	$32, %r12d
	movl	%r12d, %eax
	cmpl	%r12d, %r14d
	jle	.L101
.L114:
	movl	144136(%rcx), %edx
.L56:
	movl	%r12d, %eax
	andl	$-1024, %eax
	cmpl	%edx, %r13d
	je	.L112
	cmpl	$55296, %eax
	je	.L113
	movl	%r12d, %r10d
	sarl	$11, %r10d
	movslq	%r10d, %r10
	movslq	(%rcx,%r10,4), %rdi
	cmpl	%edi, 144160(%rcx)
	jne	.L64
.L111:
	movl	144144(%rcx), %eax
	leal	64(%rax), %edx
	cmpl	$35488, %edx
	jg	.L65
	movl	%edx, 144144(%rcx)
	movslq	%eax, %rsi
	leaq	2176(%rcx), %rdx
	leaq	(%rdx,%rsi,4), %rsi
	leaq	(%rdx,%rdi,4), %rdx
	movdqu	(%rdx), %xmm1
	movups	%xmm1, (%rsi)
	movdqu	16(%rdx), %xmm2
	movups	%xmm2, 16(%rsi)
	movdqu	32(%rdx), %xmm3
	movups	%xmm3, 32(%rsi)
	movdqu	48(%rdx), %xmm4
	movups	%xmm4, 48(%rsi)
	movdqu	64(%rdx), %xmm5
	movups	%xmm5, 64(%rsi)
	movdqu	80(%rdx), %xmm6
	movups	%xmm6, 80(%rsi)
	movdqu	96(%rdx), %xmm7
	movups	%xmm7, 96(%rsi)
	movdqu	112(%rdx), %xmm1
	movups	%xmm1, 112(%rsi)
	movdqu	128(%rdx), %xmm2
	movups	%xmm2, 128(%rsi)
	movdqu	144(%rdx), %xmm3
	movups	%xmm3, 144(%rsi)
	movdqu	160(%rdx), %xmm4
	movups	%xmm4, 160(%rsi)
	movdqu	176(%rdx), %xmm5
	movups	%xmm5, 176(%rsi)
	movdqu	192(%rdx), %xmm6
	movups	%xmm6, 192(%rsi)
	movdqu	208(%rdx), %xmm7
	movups	%xmm7, 208(%rsi)
	movdqu	224(%rdx), %xmm1
	movups	%xmm1, 224(%rsi)
	movdqu	240(%rdx), %xmm2
	movups	%xmm2, 240(%rsi)
	testl	%eax, %eax
	js	.L65
	movl	%r12d, %edx
	movl	%eax, (%rcx,%r10,4)
	movslq	144164(%rcx), %rsi
	movl	%eax, %edi
	sarl	$5, %edx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L108:
	cmpl	%r13d, 144136(%rbx)
	jne	.L35
.L31:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	144128(%rcx), %rdx
	cmpl	(%rdx,%rsi,4), %r13d
	je	.L69
.L70:
	testl	%r11d, %r11d
	js	.L75
	movl	%r11d, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	addl	$1, 144176(%rcx,%rdx,4)
	movl	%eax, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	leaq	(%rcx,%rdx,4), %rsi
	subl	$1, 144176(%rsi)
	jne	.L76
	movl	144156(%rcx), %edx
	negl	%edx
	movl	%edx, 144176(%rsi)
	movl	%eax, 144156(%rcx)
.L76:
	addl	$32, %r12d
	movl	%r11d, 2176(%rdi)
	movl	%r12d, %eax
	cmpl	%r12d, %r14d
	jg	.L114
.L101:
	movq	%rcx, %rbx
	movl	%eax, %r15d
.L55:
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	je	.L31
	movl	$1, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%r9, -64(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZL12getDataBlockP9UNewTrie2ia
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	js	.L77
	movq	144128(%rbx), %rdx
	cltq
	movl	-72(%rbp), %r8d
	leaq	(%rdx,%rax,4), %rax
	movslq	-52(%rbp), %rdx
	salq	$2, %rdx
	leaq	(%rax,%rdx), %rsi
	testb	%r8b, %r8b
	jne	.L115
	cmpq	%rsi, %rax
	jnb	.L31
	movl	144136(%rbx), %edx
	.p2align 4,,10
	.p2align 3
.L84:
	cmpl	(%rax), %edx
	jne	.L83
	movl	%r13d, (%rax)
.L83:
	addq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L84
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	$2175, %eax
	jle	.L86
	testb	%bl, %bl
	jne	.L70
.L86:
	movq	144128(%rcx), %rdx
	leaq	(%rdx,%rax,4), %rax
	testb	%r15b, %r15b
	je	.L72
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L110:
	leal	320(%rdx), %eax
	cltq
	cmpl	%esi, 2176(%rcx,%rax,4)
	je	.L69
.L105:
	movl	$2048, %edi
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L113:
	movl	%r12d, %edx
	movslq	144164(%rcx), %rsi
	sarl	$5, %edx
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$30, (%r9)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movl	144136(%rcx), %esi
	leaq	128(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L74:
	cmpl	(%rax), %esi
	jne	.L73
	movl	%r13d, (%rax)
.L73:
	addq	$4, %rax
	cmpq	%rdx, %rax
	jne	.L74
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$5, (%r9)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movl	$1, %edx
	movl	%r12d, %esi
	movq	%rcx, %rdi
	movq	%r9, -64(%rbp)
	movl	%r8d, -76(%rbp)
	movq	%rcx, -72(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZL12getDataBlockP9UNewTrie2ia
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r11d
	js	.L77
	movq	-72(%rbp), %rcx
	movdqa	-96(%rbp), %xmm0
	cltq
	movl	-76(%rbp), %r8d
	movq	144128(%rcx), %rdx
	leaq	(%rdx,%rax,4), %rax
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$1, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%r9, -72(%rbp)
	movl	%r8d, -64(%rbp)
	movb	%cl, -52(%rbp)
	call	_ZL12getDataBlockP9UNewTrie2ia
	movzbl	-52(%rbp), %ecx
	movl	-64(%rbp), %r8d
	testl	%eax, %eax
	movq	-72(%rbp), %r9
	js	.L77
	movl	%r14d, %edi
	movslq	%eax, %r11
	addl	$31, %r15d
	movslq	%r12d, %r12
	andl	$31, %edi
	andl	$-32, %r15d
	movl	144136(%rbx), %edx
	movl	%edi, -52(%rbp)
	movq	144128(%rbx), %rdi
	leaq	(%rdi,%r11,4), %rsi
	leaq	(%rsi,%r12,4), %rax
	cmpl	%r15d, %r14d
	jl	.L38
	leaq	128(%rsi), %r10
	testb	%r8b, %r8b
	je	.L39
	cmpq	%rax, %r10
	jbe	.L40
	addq	$127, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rdx
	shrq	$2, %rdx
	addq	$1, %rdx
	cmpq	$11, %rsi
	jbe	.L41
	addq	%r11, %r12
	movd	%r13d, %xmm4
	leaq	(%rdi,%r12,4), %rsi
	movq	%rdx, %rdi
	pshufd	$0, %xmm4, %xmm0
	shrq	$2, %rdi
	salq	$4, %rdi
	addq	%rsi, %rdi
	.p2align 4,,10
	.p2align 3
.L42:
	movups	%xmm0, (%rsi)
	addq	$16, %rsi
	cmpq	%rsi, %rdi
	jne	.L42
	movq	%rdx, %rsi
	andq	$-4, %rsi
	leaq	(%rax,%rsi,4), %rax
	cmpq	%rsi, %rdx
	je	.L104
.L41:
	leaq	4(%rax), %rdx
	movl	%r13d, (%rax)
	cmpq	%rdx, %r10
	jbe	.L104
	leaq	8(%rax), %rdx
	movl	%r13d, 4(%rax)
	cmpq	%rdx, %r10
	jbe	.L104
	movl	%r13d, 8(%rax)
.L104:
	movl	144136(%rbx), %edx
	jmp	.L40
.L77:
	movl	$7, (%r9)
	jmp	.L31
.L39:
	cmpq	%rax, %r10
	jbe	.L40
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	%edx, (%rax)
	jne	.L44
	movl	%r13d, (%rax)
.L44:
	addq	$4, %rax
	cmpq	%rax, %r10
	ja	.L45
	jmp	.L104
.L115:
	cmpq	%rsi, %rax
	jnb	.L31
	subq	$1, %rdx
	movq	%rax, %rcx
	movq	%rdx, %rdi
	shrq	$2, %rdi
	addq	$1, %rdi
	cmpq	$11, %rdx
	jbe	.L79
	movq	%rdi, %rdx
	movd	%r13d, %xmm6
	shrq	$2, %rdx
	pshufd	$0, %xmm6, %xmm0
	salq	$4, %rdx
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L81:
	movups	%xmm0, (%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rdx
	jne	.L81
	movq	%rdi, %rdx
	andq	$-4, %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rdi, %rdx
	je	.L31
.L79:
	leaq	4(%rax), %rdx
	movl	%r13d, (%rax)
	cmpq	%rdx, %rsi
	jbe	.L31
	leaq	8(%rax), %rdx
	movl	%r13d, 4(%rax)
	cmpq	%rdx, %rsi
	jbe	.L31
.L107:
	movl	%r13d, 8(%rax)
	jmp	.L31
.L38:
	movslq	-52(%rbp), %rcx
	leaq	(%rsi,%rcx,4), %rcx
	testb	%r8b, %r8b
	je	.L106
	cmpq	%rax, %rcx
	jbe	.L31
	leaq	-1(%rcx), %rdx
	subq	%rax, %rdx
	movq	%rdx, %r8
	shrq	$2, %r8
	addq	$1, %r8
	cmpq	$11, %rdx
	jbe	.L48
	movq	%r8, %rsi
	addq	%r11, %r12
	movd	%r13d, %xmm5
	shrq	$2, %rsi
	leaq	(%rdi,%r12,4), %rdx
	pshufd	$0, %xmm5, %xmm0
	salq	$4, %rsi
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L50:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L50
	movq	%r8, %rdx
	andq	$-4, %rdx
	leaq	(%rax,%rdx,4), %rax
	cmpq	%rdx, %r8
	je	.L31
.L48:
	leaq	4(%rax), %rdx
	movl	%r13d, (%rax)
	cmpq	%rdx, %rcx
	jbe	.L31
	leaq	8(%rax), %rdx
	movl	%r13d, 4(%rax)
	cmpq	%rdx, %rcx
	ja	.L107
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L53:
	cmpl	%edx, (%rax)
	jne	.L52
	movl	%r13d, (%rax)
.L52:
	addq	$4, %rax
.L106:
	cmpq	%rax, %rcx
	ja	.L53
	jmp	.L31
	.cfi_endproc
.LFE3441:
	.size	utrie2_setRange32_67.part.0, .-utrie2_setRange32_67.part.0
	.p2align 4
	.type	utrie2_open_67.part.0, @function
utrie2_open_67.part.0:
.LFB3439:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	movl	$80, %edi
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -52(%rbp)
	call	uprv_malloc_67@PLT
	movl	$283584, %edi
	movq	%rax, %r15
	call	uprv_malloc_67@PLT
	movl	$65536, %edi
	movq	%rax, %r12
	call	uprv_malloc_67@PLT
	testq	%r15, %r15
	sete	%dl
	testq	%r12, %r12
	movq	%rax, %rbx
	sete	%al
	orb	%al, %dl
	jne	.L128
	testq	%rbx, %rbx
	movl	-52(%rbp), %esi
	je	.L128
	pxor	%xmm0, %xmm0
	movd	%r13d, %xmm2
	movq	%rbx, %rdx
	movq	%rbx, 144128(%r12)
	movups	%xmm0, 32(%r15)
	leaq	512(%rbx), %rax
	movups	%xmm0, 64(%r15)
	movl	%r13d, 36(%r15)
	movl	%esi, 40(%r15)
	movl	$1114112, 44(%r15)
	movq	%r12, 72(%r15)
	movl	$16384, 144148(%r12)
	movl	%r13d, 144136(%r12)
	movl	%esi, 144140(%r12)
	movl	$1114112, 144168(%r12)
	movl	$0, 144156(%r12)
	movb	$0, 144172(%r12)
	movups	%xmm0, (%r15)
	movups	%xmm0, 16(%r15)
	movups	%xmm0, 48(%r15)
	pshufd	$0, %xmm2, %xmm0
	.p2align 4,,10
	.p2align 3
.L120:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L120
	movd	%esi, %xmm3
	movups	%xmm0, 768(%rbx)
	leaq	2192(%r12), %rax
	leaq	10496(%r12), %rdx
	pshufd	$0, %xmm3, %xmm1
	movups	%xmm0, 784(%rbx)
	movups	%xmm0, 800(%rbx)
	movups	%xmm0, 816(%rbx)
	movups	%xmm0, 832(%rbx)
	movups	%xmm0, 848(%rbx)
	movups	%xmm0, 864(%rbx)
	movups	%xmm0, 880(%rbx)
	movups	%xmm0, 896(%rbx)
	movups	%xmm0, 912(%rbx)
	movups	%xmm0, 928(%rbx)
	movups	%xmm0, 944(%rbx)
	movups	%xmm0, 960(%rbx)
	movups	%xmm0, 976(%rbx)
	movups	%xmm0, 992(%rbx)
	movups	%xmm0, 1008(%rbx)
	movups	%xmm1, 512(%rbx)
	movups	%xmm1, 528(%rbx)
	movups	%xmm1, 544(%rbx)
	movups	%xmm1, 560(%rbx)
	movups	%xmm1, 576(%rbx)
	movups	%xmm1, 592(%rbx)
	movups	%xmm1, 608(%rbx)
	movups	%xmm1, 624(%rbx)
	movups	%xmm1, 640(%rbx)
	movups	%xmm1, 656(%rbx)
	movups	%xmm1, 672(%rbx)
	movups	%xmm1, 688(%rbx)
	movups	%xmm1, 704(%rbx)
	movups	%xmm1, 720(%rbx)
	movups	%xmm1, 736(%rbx)
	movups	%xmm1, 752(%rbx)
	movl	$192, 144164(%r12)
	movdqa	.LC0(%rip), %xmm0
	movl	$256, 144152(%r12)
	movups	%xmm0, 2176(%r12)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 144176(%r12)
	movdqa	.LC2(%rip), %xmm0
	movups	%xmm0, 144192(%r12)
	movdqa	.LC3(%rip), %xmm0
	.p2align 4,,10
	.p2align 3
.L121:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L121
	leaq	10504(%r12), %rdi
	movq	$-1, 10496(%r12)
	movq	$-1, 12792(%r12)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	2304(%rax), %ecx
	movq	$-1, %rax
	shrl	$3, %ecx
	rep stosq
	leaq	128(%r12), %rax
	movups	%xmm0, 12800(%r12)
	addq	$2176, %r12
	movups	%xmm0, 10640(%r12)
	movups	%xmm0, 10656(%r12)
	movups	%xmm0, 10672(%r12)
	movups	%xmm0, 10688(%r12)
	movups	%xmm0, 10704(%r12)
	movups	%xmm0, 10720(%r12)
	movups	%xmm0, 10736(%r12)
	movups	%xmm0, 10752(%r12)
	movups	%xmm0, 10768(%r12)
	movups	%xmm0, 10784(%r12)
	movups	%xmm0, 10800(%r12)
	movups	%xmm0, 10816(%r12)
	movups	%xmm0, 10832(%r12)
	movups	%xmm0, 10848(%r12)
	movups	%xmm0, 10864(%r12)
	movdqa	.LC4(%rip), %xmm0
	movl	$2656, 141984(%r12)
	movups	%xmm0, -2176(%r12)
	movdqa	.LC5(%rip), %xmm0
	movl	$2720, 141968(%r12)
	movups	%xmm0, -2160(%r12)
	movdqa	.LC6(%rip), %xmm0
	movups	%xmm0, -2144(%r12)
	movdqa	.LC7(%rip), %xmm0
	movups	%xmm0, -2128(%r12)
	movdqa	.LC8(%rip), %xmm0
	movups	%xmm0, -2112(%r12)
	movdqa	.LC9(%rip), %xmm0
	movups	%xmm0, -2096(%r12)
	movdqa	.LC10(%rip), %xmm0
	movups	%xmm0, -2080(%r12)
	movdqa	.LC11(%rip), %xmm0
	movups	%xmm0, -2064(%r12)
	movdqa	.LC12(%rip), %xmm0
	.p2align 4,,10
	.p2align 3
.L122:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %r12
	jne	.L122
	movl	$128, %ebx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$1, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZL12getDataBlockP9UNewTrie2ia
	testl	%eax, %eax
	js	.L137
	movq	144128(%r12), %rdx
	cltq
	movl	%r13d, (%rdx,%rax,4)
.L123:
	addl	$32, %ebx
	cmpl	$2048, %ebx
	je	.L116
.L127:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L123
	movq	72(%r15), %r12
	testq	%r12, %r12
	je	.L124
	cmpb	$0, 144172(%r12)
	je	.L125
.L124:
	addl	$32, %ebx
	movl	$30, (%r14)
	cmpl	$2048, %ebx
	jne	.L127
.L116:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movl	$7, (%r14)
	jmp	.L123
.L128:
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	movq	%rbx, %rdi
	call	uprv_free_67@PLT
	movl	$7, (%r14)
	jmp	.L116
	.cfi_endproc
.LFE3439:
	.size	utrie2_open_67.part.0, .-utrie2_open_67.part.0
	.p2align 4
	.type	_ZL13copyEnumRangePKviij, @function
_ZL13copyEnumRangePKviij:
.LFB2743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	cmpl	%ecx, 36(%rdi)
	je	.L138
	cmpb	$1, 12(%rbx)
	movl	%edx, %r12d
	movl	8(%rbx), %eax
	adcl	$-1, %r12d
	xorl	%r13d, %r13d
	cmpl	%esi, %r12d
	jne	.L141
	testl	%eax, %eax
	jg	.L138
	cmpl	$1114111, %r12d
	ja	.L147
	movq	72(%rdi), %r14
	testq	%r14, %r14
	je	.L144
	movzbl	144172(%r14), %r13d
	testb	%r13b, %r13b
	jne	.L144
	movl	$1, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	%ecx, -36(%rbp)
	call	_ZL12getDataBlockP9UNewTrie2ia
	movl	-36(%rbp), %ecx
	testl	%eax, %eax
	js	.L155
	andl	$31, %r12d
	movq	144128(%r14), %rdx
	addl	%r12d, %eax
	cltq
	movl	%ecx, (%rdx,%rax,4)
	movl	8(%rbx), %edx
	testl	%edx, %edx
	setle	%r13b
.L138:
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	testl	%eax, %eax
	jg	.L138
	cmpl	%esi, %r12d
	setl	%dl
	cmpl	$1114111, %esi
	seta	%al
	orb	%al, %dl
	jne	.L147
	cmpl	$1114111, %r12d
	ja	.L147
	leaq	8(%rbx), %r9
	movl	$1, %r8d
	movl	%r12d, %edx
	call	utrie2_setRange32_67.part.0
	movl	8(%rbx), %eax
	testl	%eax, %eax
	setle	%r13b
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$1, 8(%rbx)
	xorl	%r13d, %r13d
	addq	$16, %rsp
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movl	$30, 8(%rbx)
	xorl	%r13d, %r13d
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$7, 8(%rbx)
	jmp	.L138
	.cfi_endproc
.LFE2743:
	.size	_ZL13copyEnumRangePKviij, .-_ZL13copyEnumRangePKviij
	.p2align 4
	.globl	utrie2_open_67
	.type	utrie2_open_67, @function
utrie2_open_67:
.LFB2740:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L157
	jmp	utrie2_open_67.part.0
	.p2align 4,,10
	.p2align 3
.L157:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2740:
	.size	utrie2_open_67, .-utrie2_open_67
	.p2align 4
	.globl	utrie2_clone_67
	.type	utrie2_clone_67, @function
utrie2_clone_67:
.LFB2742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L158
	movq	%rdi, %rbx
	movq	%rsi, %r14
	testq	%rdi, %rdi
	je	.L160
	cmpq	$0, 56(%rdi)
	je	.L184
.L161:
	movl	$80, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L185
	movdqu	(%rbx), %xmm0
	movdqu	16(%rbx), %xmm1
	movdqu	32(%rbx), %xmm2
	movdqu	48(%rbx), %xmm3
	movdqu	64(%rbx), %xmm4
	cmpq	$0, 56(%rbx)
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	je	.L163
	movslq	64(%rbx), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 56(%r12)
	testq	%rax, %rax
	je	.L186
	movq	56(%rbx), %r13
	movslq	64(%rbx), %rdx
	movb	$1, 68(%r12)
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	%rax, %rcx
	movq	(%rbx), %rax
	addq	%rcx, %rax
	subq	%r13, %rax
	movq	%rax, (%r12)
	testq	%rdx, %rdx
	je	.L166
	subq	%r13, %rdx
	addq	%rcx, %rdx
	movq	%rdx, 8(%r12)
.L166:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L158
	subq	%r13, %rax
	addq	%rcx, %rax
	movq	%rax, 16(%r12)
.L158:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	cmpq	$0, 72(%rdi)
	jne	.L161
.L160:
	movl	$1, (%r14)
	xorl	%r12d, %r12d
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$283584, %edi
	movq	72(%rbx), %r13
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L183
	movl	144148(%r13), %eax
	leal	0(,%rax,4), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 144128(%r15)
	testq	%rax, %rax
	je	.L187
	movl	144148(%r13), %eax
	leaq	8(%r15), %rdi
	movq	%r15, %rcx
	movq	%r13, %rsi
	andq	$-8, %rdi
	leaq	2176(%r15), %r8
	leaq	2176(%r13), %r9
	movl	%eax, 144148(%r15)
	movq	0(%r13), %rax
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	$2176, %ecx
	movq	%rax, (%r15)
	movq	2168(%r13), %rax
	shrl	$3, %ecx
	movq	%rax, 2168(%r15)
	rep movsq
	movq	%r9, %rsi
	movq	%r8, %rdi
	movl	$281408, %ecx
	movslq	144144(%r13), %rdx
	movq	%rdx, %r14
	salq	$2, %rdx
	call	__memcpy_chk@PLT
	movslq	144152(%r13), %rdx
	movl	144160(%r13), %eax
	movl	%r14d, 144144(%r15)
	movq	144128(%r15), %rdi
	movq	144128(%r13), %rsi
	salq	$2, %rdx
	movl	%eax, 144160(%r15)
	call	memcpy@PLT
	movl	144164(%r13), %eax
	movl	%eax, 144164(%r15)
	movl	144152(%r13), %eax
	movl	%eax, 144152(%r15)
	movzbl	144172(%r13), %r14d
	testb	%r14b, %r14b
	je	.L170
	movl	$0, 144156(%r15)
.L171:
	movl	144136(%r13), %eax
	movl	%eax, 144136(%r15)
	movl	144140(%r13), %eax
	movl	%eax, 144140(%r15)
	movl	144168(%r13), %eax
	movb	%r14b, 144172(%r15)
	movl	%eax, 144168(%r15)
	movq	%r15, 72(%r12)
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L170:
	movslq	%eax, %rdx
	leaq	144176(%r13), %rsi
	movl	$139408, %ecx
	movq	%rdx, %rax
	leaq	144176(%r15), %rdi
	shrq	$5, %rax
	leaq	0(,%rax,4), %rdx
	call	__memcpy_chk@PLT
	movl	144156(%r13), %eax
	movl	%eax, 144156(%r15)
	jmp	.L171
.L186:
	cmpq	$0, 72(%r12)
	jne	.L158
.L165:
	movl	$7, (%r14)
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L158
.L185:
	movl	$7, (%r14)
	jmp	.L158
.L187:
	movq	%r15, %rdi
	call	uprv_free_67@PLT
.L183:
	movq	$0, 72(%r12)
	cmpq	$0, 56(%r12)
	je	.L165
	jmp	.L158
	.cfi_endproc
.LFE2742:
	.size	utrie2_clone_67, .-utrie2_clone_67
	.p2align 4
	.globl	utrie2_cloneAsThawed_67
	.type	utrie2_cloneAsThawed_67, @function
utrie2_cloneAsThawed_67:
.LFB2744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L216
	movq	%rdi, %r12
	movq	%rsi, %r13
	testq	%rdi, %rdi
	je	.L191
	cmpq	$0, 56(%rdi)
	movq	72(%rdi), %rax
	je	.L217
	testq	%rax, %rax
	je	.L194
.L193:
	cmpb	$0, 144172(%rax)
	je	.L218
.L194:
	movl	40(%r12), %esi
	movl	36(%r12), %edi
	movq	%r13, %rdx
	call	utrie2_open_67.part.0
	movq	%rax, -80(%rbp)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L216
	xorl	%esi, %esi
	leaq	-80(%rbp), %rcx
	leaq	_ZL13copyEnumRangePKviij(%rip), %rdx
	movq	%r12, %rdi
	movb	$0, -68(%rbp)
	movl	$55296, %ebx
	movl	%eax, -72(%rbp)
	call	utrie2_enum_67@PLT
	movl	-72(%rbp), %esi
	movq	-80(%rbp), %r8
	movl	%esi, 0(%r13)
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L196:
	movl	(%rdx,%rax,4), %r15d
.L197:
	cmpl	%r15d, 36(%r12)
	je	.L198
	testl	%esi, %esi
	jg	.L198
	movq	72(%r8), %rcx
	testq	%rcx, %rcx
	je	.L199
	cmpb	$0, 144172(%rcx)
	jne	.L199
	xorl	%edx, %edx
	movq	%rcx, %rdi
	movl	%ebx, %esi
	movq	%rcx, -88(%rbp)
	call	_ZL12getDataBlockP9UNewTrie2ia
	movq	-88(%rbp), %rcx
	testl	%eax, %eax
	js	.L219
	addl	%eax, %r14d
	movq	144128(%rcx), %rax
	movl	0(%r13), %esi
	movslq	%r14d, %r14
	movq	-80(%rbp), %r8
	movl	%r15d, (%rax,%r14,4)
	.p2align 4,,10
	.p2align 3
.L198:
	addl	$1, %ebx
	cmpl	$56320, %ebx
	je	.L220
.L202:
	movl	%ebx, %eax
	movq	(%r12), %rcx
	movl	%ebx, %r14d
	movq	16(%r12), %rdx
	sarl	$5, %eax
	andl	$31, %r14d
	cltq
	movzwl	(%rcx,%rax,2), %eax
	leal	(%r14,%rax,4), %eax
	cltq
	testq	%rdx, %rdx
	jne	.L196
	movzwl	(%rcx,%rax,2), %r15d
	jmp	.L197
.L220:
	testl	%esi, %esi
	jle	.L188
	movq	%r8, %rdi
	call	utrie2_close_67@PLT
.L216:
	xorl	%r8d, %r8d
.L188:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	addq	$56, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L217:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L193
.L191:
	movl	$1, 0(%r13)
	xorl	%r8d, %r8d
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$30, 0(%r13)
	movl	$30, %esi
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$7, 0(%r13)
	movq	-80(%rbp), %r8
	movl	$7, %esi
	jmp	.L198
.L218:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	utrie2_clone_67
	movq	%rax, %r8
	jmp	.L188
.L221:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2744:
	.size	utrie2_cloneAsThawed_67, .-utrie2_cloneAsThawed_67
	.p2align 4
	.globl	utrie2_set32_67
	.type	utrie2_set32_67, @function
utrie2_set32_67:
.LFB2755:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L232
	cmpl	$1114111, %esi
	ja	.L234
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	72(%rdi), %r13
	testq	%r13, %r13
	je	.L225
	cmpb	$0, 144172(%r13)
	je	.L226
.L225:
	movl	$30, (%rcx)
.L222:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	%edx, %r12d
	movq	%r13, %rdi
	movl	$1, %edx
	movq	%rcx, -32(%rbp)
	movl	%esi, -20(%rbp)
	call	_ZL12getDataBlockP9UNewTrie2ia
	movl	-20(%rbp), %esi
	movq	-32(%rbp), %rcx
	testl	%eax, %eax
	js	.L235
	andl	$31, %esi
	addl	%eax, %esi
	movq	144128(%r13), %rax
	movslq	%esi, %rsi
	movl	%r12d, (%rax,%rsi,4)
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movl	$7, (%rcx)
	jmp	.L222
	.cfi_endproc
.LFE2755:
	.size	utrie2_set32_67, .-utrie2_set32_67
	.p2align 4
	.globl	utrie2_set32ForLeadSurrogateCodeUnit_67
	.type	utrie2_set32ForLeadSurrogateCodeUnit_67, @function
utrie2_set32ForLeadSurrogateCodeUnit_67:
.LFB2756:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L246
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L248
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	72(%rdi), %r13
	testq	%r13, %r13
	je	.L239
	cmpb	$0, 144172(%r13)
	je	.L240
.L239:
	movl	$30, (%rcx)
.L236:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	%edx, %r12d
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%rcx, -32(%rbp)
	movl	%esi, -20(%rbp)
	call	_ZL12getDataBlockP9UNewTrie2ia
	movl	-20(%rbp), %esi
	movq	-32(%rbp), %rcx
	testl	%eax, %eax
	js	.L249
	andl	$31, %esi
	addl	%eax, %esi
	movq	144128(%r13), %rax
	movslq	%esi, %rsi
	movl	%r12d, (%rax,%rsi,4)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L249:
	movl	$7, (%rcx)
	jmp	.L236
	.cfi_endproc
.LFE2756:
	.size	utrie2_set32ForLeadSurrogateCodeUnit_67, .-utrie2_set32ForLeadSurrogateCodeUnit_67
	.p2align 4
	.globl	utrie2_setRange32_67
	.type	utrie2_setRange32_67, @function
utrie2_setRange32_67:
.LFB2759:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L250
	cmpl	$1114111, %edx
	seta	%r10b
	cmpl	%edx, %esi
	setg	%al
	orb	%al, %r10b
	jne	.L254
	cmpl	$1114111, %esi
	ja	.L254
	movsbl	%r8b, %r8d
	jmp	utrie2_setRange32_67.part.0
	.p2align 4,,10
	.p2align 3
.L250:
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	movl	$1, (%r9)
	ret
	.cfi_endproc
.LFE2759:
	.size	utrie2_setRange32_67, .-utrie2_setRange32_67
	.p2align 4
	.globl	utrie2_freeze_67
	.type	utrie2_freeze_67, @function
utrie2_freeze_67:
.LFB2768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r11d
	movl	%esi, -52(%rbp)
	testl	%r11d, %r11d
	jg	.L255
	testq	%rdi, %rdi
	movq	%rdx, %r15
	movq	%rdi, %rbx
	sete	%dl
	cmpl	$1, %esi
	seta	%al
	orb	%al, %dl
	movb	%dl, -56(%rbp)
	je	.L257
.L260:
	movl	$1, (%r15)
.L255:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movq	72(%rdi), %r12
	testq	%r12, %r12
	je	.L555
	cmpb	$0, 144172(%r12)
	je	.L261
.L339:
	movl	44(%rbx), %r14d
	cmpl	$65536, %r14d
	jg	.L556
	movl	-52(%rbp), %edi
	movl	$2112, %r9d
	testl	%edi, %edi
	jne	.L342
	movl	$2112, %r13d
.L341:
	movl	144164(%r12), %eax
	addl	%r13d, %eax
	cmpl	$65535, %eax
	jg	.L344
	cmpb	$0, -56(%rbp)
	jne	.L344
	movl	%r13d, %r9d
.L345:
	movl	144152(%r12), %eax
	leal	(%rax,%r13), %edx
	cmpl	$262140, %edx
	jg	.L344
	movl	-52(%rbp), %ecx
	leal	16(%r9,%r9), %edx
	testl	%ecx, %ecx
	jne	.L347
	leal	(%rdx,%rax,2), %edx
.L348:
	movslq	%edx, %rdi
	movl	%r9d, -56(%rbp)
	movl	%edx, -64(%rbp)
	call	uprv_malloc_67@PLT
	movl	-64(%rbp), %edx
	movl	-56(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, 56(%rbx)
	je	.L557
	movb	$1, 68(%rbx)
	movl	144152(%r12), %esi
	movl	%edx, 64(%rbx)
	movl	$-1, %edx
	movl	%r9d, 24(%rbx)
	movl	%esi, 28(%rbx)
	cmpl	$65536, %r14d
	jle	.L350
	movzwl	144160(%r12), %edx
.L350:
	movzwl	144164(%r12), %edi
	movzwl	-52(%rbp), %r15d
	movw	%dx, 32(%rbx)
	movl	%r13d, %ecx
	leal	-4(%r13,%rsi), %r8d
	movw	%dx, 10(%rax)
	movl	%r14d, %edx
	movd	%r13d, %xmm6
	addl	%r13d, %edi
	sarl	$11, %edx
	movl	%r8d, 48(%rbx)
	movl	%esi, %r8d
	movw	%di, 34(%rbx)
	sarl	$2, %r8d
	pshufd	$0, %xmm6, %xmm3
	movl	$1416784178, (%rax)
	movw	%r15w, 4(%rax)
	movw	%r9w, 6(%rax)
	movw	%di, 12(%rax)
	movw	%dx, 14(%rax)
	leaq	16(%rax), %rdx
	movw	%r8w, 8(%rax)
	leaq	2176(%r12), %r8
	movq	%rdx, (%rbx)
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L351:
	movdqu	2176(%r12,%rdx,2), %xmm0
	movdqu	2192(%r12,%rdx,2), %xmm1
	paddd	%xmm3, %xmm0
	paddd	%xmm3, %xmm1
	psrld	$2, %xmm1
	psrld	$2, %xmm0
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, 16(%rax,%rdx)
	addq	$16, %rdx
	cmpq	$4160, %rdx
	jne	.L351
	movd	%ecx, %xmm0
	leal	128(%rcx), %edx
	movdqu	2208(%r12), %xmm7
	movdqu	2240(%r12), %xmm6
	punpcklwd	%xmm0, %xmm0
	movw	%dx, 4176(%rax)
	movdqu	2192(%r12), %xmm1
	leaq	4240(%rax), %rdi
	pshufd	$0, %xmm0, %xmm2
	movw	%dx, 4178(%rax)
	movdqu	2224(%r12), %xmm0
	movdqu	2304(%r12), %xmm5
	shufps	$136, %xmm7, %xmm1
	movdqa	%xmm1, %xmm4
	movdqu	2272(%r12), %xmm7
	movzwl	2384(%r12), %edx
	shufps	$136, %xmm6, %xmm0
	punpcklwd	%xmm0, %xmm1
	punpckhwd	%xmm0, %xmm4
	movdqu	2336(%r12), %xmm6
	movdqa	%xmm1, %xmm0
	punpcklwd	%xmm4, %xmm1
	punpckhwd	%xmm4, %xmm0
	addl	%ecx, %edx
	punpcklwd	%xmm0, %xmm1
	movdqu	2288(%r12), %xmm0
	paddw	%xmm2, %xmm1
	movups	%xmm1, 4180(%rax)
	shufps	$136, %xmm5, %xmm0
	movdqu	2256(%r12), %xmm1
	shufps	$136, %xmm7, %xmm1
	movdqa	%xmm1, %xmm4
	punpcklwd	%xmm0, %xmm1
	movdqu	2368(%r12), %xmm7
	punpckhwd	%xmm0, %xmm4
	movdqa	%xmm1, %xmm0
	punpckhwd	%xmm4, %xmm0
	punpcklwd	%xmm4, %xmm1
	punpcklwd	%xmm0, %xmm1
	movdqu	2320(%r12), %xmm0
	paddw	%xmm2, %xmm1
	movups	%xmm1, 4196(%rax)
	shufps	$136, %xmm6, %xmm0
	movdqa	%xmm0, %xmm4
	movdqu	2352(%r12), %xmm1
	movw	%dx, 4228(%rax)
	movzwl	2392(%r12), %edx
	shufps	$136, %xmm7, %xmm1
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm4
	addl	%ecx, %edx
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm4, %xmm0
	movw	%dx, 4230(%rax)
	punpckhwd	%xmm4, %xmm1
	movzwl	2400(%r12), %edx
	punpcklwd	%xmm1, %xmm0
	addl	%ecx, %edx
	paddw	%xmm2, %xmm0
	movw	%dx, 4232(%rax)
	movzwl	2408(%r12), %edx
	movups	%xmm0, 4212(%rax)
	addl	%ecx, %edx
	movw	%dx, 4234(%rax)
	movzwl	2416(%r12), %edx
	addl	%ecx, %edx
	addw	2424(%r12), %cx
	movw	%dx, 4236(%rax)
	movw	%cx, 4238(%rax)
	cmpl	$65536, %r14d
	jle	.L353
	leal	-65536(%r14), %edx
	leaq	128(%r12), %r10
	sarl	$11, %edx
	leal	2112(%rdx), %r14d
	je	.L355
	leal	-1(%rdx), %ecx
	movl	%edx, %r15d
	movslq	%ecx, %r11
	cmpl	$6, %r11d
	jle	.L382
	movl	%edx, %r9d
	xorl	%ecx, %ecx
	shrl	$3, %r9d
	salq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L358:
	movdqu	128(%r12,%rcx,2), %xmm0
	movdqu	144(%r12,%rcx,2), %xmm6
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm6, %xmm0
	punpckhwd	%xmm6, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpcklwd	%xmm2, %xmm0
	movups	%xmm0, 4240(%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %r9
	jne	.L358
	movl	%edx, %ecx
	andl	$-8, %ecx
	movl	%ecx, %eax
	subl	%ecx, %edx
	leaq	(%r10,%rax,4), %r10
	leaq	(%rdi,%rax,2), %rax
	cmpl	%r15d, %ecx
	je	.L359
	leal	-1(%rdx), %ecx
.L356:
	movl	(%r10), %r9d
	movw	%r9w, (%rax)
	testl	%ecx, %ecx
	je	.L359
	movl	4(%r10), %ecx
	movw	%cx, 2(%rax)
	cmpl	$2, %edx
	je	.L359
	movl	8(%r10), %ecx
	movw	%cx, 4(%rax)
	cmpl	$3, %edx
	je	.L359
	movl	12(%r10), %ecx
	movw	%cx, 6(%rax)
	cmpl	$4, %edx
	je	.L359
	movl	16(%r10), %ecx
	movw	%cx, 8(%rax)
	cmpl	$5, %edx
	je	.L359
	movl	20(%r10), %ecx
	movw	%cx, 10(%rax)
	cmpl	$6, %edx
	je	.L359
	movl	24(%r10), %edx
	movw	%dx, 12(%rax)
.L359:
	leaq	2(%rdi,%r11,2), %rdi
.L355:
	movl	144144(%r12), %edx
	movslq	%r14d, %rax
	salq	$2, %rax
	subl	%r14d, %edx
	addq	%rax, %r8
	testl	%edx, %edx
	jle	.L353
	leal	-1(%rdx), %ecx
	movl	%edx, %r11d
	movslq	%ecx, %r10
	cmpl	$6, %r10d
	jle	.L383
	movl	%edx, %r9d
	leaq	2176(%r12,%rax), %rcx
	xorl	%eax, %eax
	shrl	$3, %r9d
	salq	$4, %r9
	.p2align 4,,10
	.p2align 3
.L362:
	movdqu	(%rcx,%rax,2), %xmm0
	movdqu	16(%rcx,%rax,2), %xmm1
	paddd	%xmm3, %xmm0
	paddd	%xmm3, %xmm1
	psrld	$2, %xmm1
	psrld	$2, %xmm0
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %r9
	jne	.L362
	movl	%edx, %ecx
	andl	$-8, %ecx
	movl	%ecx, %eax
	subl	%ecx, %edx
	leaq	(%r8,%rax,4), %r8
	leaq	(%rdi,%rax,2), %rax
	cmpl	%r11d, %ecx
	je	.L363
	leal	-1(%rdx), %ecx
.L360:
	movl	(%r8), %r9d
	addl	%r13d, %r9d
	shrl	$2, %r9d
	movw	%r9w, (%rax)
	testl	%ecx, %ecx
	je	.L363
	movl	4(%r8), %ecx
	addl	%r13d, %ecx
	shrl	$2, %ecx
	movw	%cx, 2(%rax)
	cmpl	$2, %edx
	je	.L363
	movl	8(%r8), %ecx
	addl	%r13d, %ecx
	shrl	$2, %ecx
	movw	%cx, 4(%rax)
	cmpl	$3, %edx
	je	.L363
	movl	12(%r8), %ecx
	addl	%r13d, %ecx
	shrl	$2, %ecx
	movw	%cx, 6(%rax)
	cmpl	$4, %edx
	je	.L363
	movl	16(%r8), %ecx
	addl	%r13d, %ecx
	shrl	$2, %ecx
	movw	%cx, 8(%rax)
	cmpl	$5, %edx
	je	.L363
	movl	20(%r8), %ecx
	addl	%r13d, %ecx
	shrl	$2, %ecx
	movw	%cx, 10(%rax)
	cmpl	$6, %edx
	je	.L363
	addl	24(%r8), %r13d
	shrl	$2, %r13d
	movw	%r13w, 12(%rax)
.L363:
	leaq	2(%rdi,%r10,2), %rdi
.L353:
	cmpl	$1, -52(%rbp)
	movq	144128(%r12), %r8
	je	.L364
	movq	%rdi, 8(%rbx)
	movq	$0, 16(%rbx)
	testl	%esi, %esi
	jle	.L365
	movl	%esi, %r9d
	cmpl	$7, %esi
	jle	.L384
	movl	%esi, %edx
	xorl	%eax, %eax
	shrl	$3, %edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L368:
	movdqu	(%r8,%rax,2), %xmm0
	movdqu	16(%r8,%rax,2), %xmm6
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm6, %xmm0
	punpckhwd	%xmm6, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpcklwd	%xmm2, %xmm0
	movups	%xmm0, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L368
	movl	%esi, %edx
	andl	$-8, %edx
	movl	%edx, %ecx
	subl	%edx, %esi
	leaq	(%r8,%rcx,4), %rax
	leaq	(%rdi,%rcx,2), %rdi
	cmpl	%r9d, %edx
	je	.L365
.L366:
	movl	(%rax), %edx
	movw	%dx, (%rdi)
	cmpl	$1, %esi
	je	.L365
	movl	4(%rax), %edx
	movw	%dx, 2(%rdi)
	cmpl	$2, %esi
	je	.L365
	movl	8(%rax), %edx
	movw	%dx, 4(%rdi)
	cmpl	$3, %esi
	je	.L365
	movl	12(%rax), %edx
	movw	%dx, 6(%rdi)
	cmpl	$4, %esi
	je	.L365
	movl	16(%rax), %edx
	movw	%dx, 8(%rdi)
	cmpl	$5, %esi
	je	.L365
	movl	20(%rax), %edx
	movw	%dx, 10(%rdi)
	cmpl	$6, %esi
	je	.L365
	movl	24(%rax), %eax
	movw	%ax, 12(%rdi)
.L365:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	movq	$0, 72(%rbx)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L556:
	movl	-52(%rbp), %esi
	movl	144144(%r12), %r9d
	testl	%esi, %esi
	je	.L343
	cmpl	$65535, %r9d
	jg	.L344
.L342:
	xorl	%r13d, %r13d
	cmpl	$65535, 144164(%r12)
	jle	.L345
.L344:
	movl	$8, (%r15)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movl	$1114111, %esi
	call	utrie2_get32_67@PLT
	movl	$-1, %ecx
	movl	144136(%r12), %r13d
	movl	144160(%r12), %r14d
	movl	%eax, %esi
	movl	144164(%r12), %r11d
	movl	%ecx, %edx
	movl	%eax, -100(%rbp)
	leaq	-256(%r12), %rdi
	cmpl	%r13d, %esi
	movq	144128(%r12), %rax
	movq	%rbx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	cmove	%r11d, %edx
	cmove	%r14d, %ecx
	leaq	2172(%r12), %r10
	movq	%rax, -64(%rbp)
	movl	$1114112, %eax
.L266:
	movl	(%r10), %r9d
	cmpl	%ecx, %r9d
	je	.L558
	cmpl	%r9d, %r14d
	jne	.L559
	cmpl	%r13d, %esi
	jne	.L547
	subl	$2048, %eax
	movl	%r14d, %ecx
.L269:
	subq	$4, %r10
	testl	%eax, %eax
	jg	.L266
	movl	(%r15), %eax
	movq	-80(%rbp), %rbx
	movl	$65536, %esi
	movl	$0, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	-80(%rbp), %eax
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L559:
	movslq	%r9d, %rbx
	salq	$2, %rbx
	leaq	(%r12,%rbx), %rdi
	addq	-72(%rbp), %rbx
.L271:
	movl	2428(%rdi), %r8d
	cmpl	%edx, %r8d
	je	.L560
	cmpl	%r8d, %r11d
	jne	.L275
	cmpl	%r13d, %esi
	jne	.L547
	subl	$32, %eax
	movl	%r11d, %edx
.L274:
	subq	$4, %rdi
	cmpq	%rdi, %rbx
	jne	.L271
	movl	%r9d, %ecx
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L275:
	movq	-64(%rbp), %rcx
	movslq	%r8d, %rdx
	leaq	(%rcx,%rdx,4), %rdx
	leal	-32(%rax), %ecx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L562:
	subl	$1, %eax
	subq	$4, %rdx
	cmpl	%ecx, %eax
	je	.L561
.L276:
	cmpl	124(%rdx), %esi
	je	.L562
.L547:
	addl	$2047, %eax
	movq	-80(%rbp), %rbx
	movl	%eax, %edi
	movl	(%r15), %eax
	andl	$-2048, %edi
	movl	%edi, -80(%rbp)
	movl	%eax, -72(%rbp)
	cmpl	$1114112, %edi
	je	.L277
	cmpl	$65536, %edi
	movl	$65536, %eax
	cmovge	%edi, %eax
	movl	%eax, %esi
	movl	%edi, %eax
.L267:
	movl	-72(%rbp), %r10d
	movl	%eax, 144168(%r12)
	movl	%eax, 44(%rbx)
	testl	%r10d, %r10d
	jg	.L255
	movl	36(%rbx), %ecx
	movq	%r15, %r9
	movl	$1, %r8d
	movq	%rbx, %rdi
	movl	$1114111, %edx
	call	utrie2_setRange32_67.part.0
	movl	(%r15), %eax
	movl	%eax, -72(%rbp)
	testl	%eax, %eax
	jg	.L255
	movq	144128(%r12), %rax
	movq	%rax, -64(%rbp)
.L280:
	movdqa	.LC0(%rip), %xmm0
	movabsq	$687194767488, %rax
	cmpl	$192, 144152(%r12)
	movq	%rax, 144192(%r12)
	movups	%xmm0, 144176(%r12)
	jle	.L281
	movq	-64(%rbp), %rax
	movq	%rbx, -88(%rbp)
	movl	$192, %r8d
	movl	$192, %r11d
	movq	%r15, -96(%rbp)
	movl	$2, %r13d
	movl	$64, %ecx
	addq	$4, %rax
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L282:
	movl	%r11d, %eax
	sarl	$5, %eax
	cltq
	leaq	(%r12,%rax,4), %r14
	movl	144176(%r14), %r9d
	testl	%r9d, %r9d
	jle	.L298
	movl	%r8d, %r10d
	subl	%ecx, %r10d
	js	.L291
	movq	-64(%rbp), %rsi
	movslq	%r11d, %rax
	xorl	%edi, %edi
	leaq	(%rsi,%rax,4), %r9
	.p2align 4,,10
	.p2align 3
.L296:
	testl	%ecx, %ecx
	jle	.L292
	movl	%ecx, %edx
	xorl	%eax, %eax
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L563:
	addq	$4, %rax
	subl	$1, %edx
	je	.L294
.L295:
	movl	(%r9,%rax), %ebx
	cmpl	%ebx, (%rsi,%rax)
	je	.L563
.L293:
	addl	$4, %edi
	addq	$16, %rsi
	cmpl	%edi, %r10d
	jge	.L296
.L291:
	leal	-4(%rcx), %edi
	movl	%edi, %ebx
	testl	%edi, %edi
	jle	.L372
	movq	-64(%rbp), %rbx
	movslq	%r11d, %rax
	movslq	%edi, %rdx
	leaq	(%rbx,%rax,4), %r10
	movslq	%r8d, %rax
	subq	%rdx, %rax
	leaq	(%rbx,%rax,4), %rsi
	leal	-5(%rcx), %eax
	leal	-8(%rcx), %ebx
	andl	$-4, %eax
	subl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L371:
	movl	%r8d, %r15d
	leal	-1(%rdi), %r9d
	xorl	%eax, %eax
	subl	%edi, %r15d
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L564:
	movq	%rdx, %rax
.L301:
	movl	(%r10,%rax,4), %edx
	cmpl	%edx, (%rsi,%rax,4)
	jne	.L299
	leaq	1(%rax), %rdx
	cmpq	%r9, %rax
	jne	.L564
.L300:
	testl	%r13d, %r13d
	jle	.L306
	movl	%r15d, 144176(%r14)
	addl	$32, %r15d
	cmpl	$1, %r13d
	je	.L306
	movl	%r15d, 144180(%r14)
.L306:
	movl	%ecx, %eax
	addl	%edi, %r11d
	subl	%edi, %eax
	testl	%eax, %eax
	jle	.L290
	movslq	%r8d, %rdi
	movslq	%r11d, %r10
	leal	-1(%rax), %r9d
	leaq	0(,%rdi,4), %rdx
	leaq	0(,%r10,4), %rsi
	leaq	16(%rdx), %r14
	leaq	16(,%r10,4), %rbx
	cmpq	%rsi, %r14
	setle	%r14b
	cmpq	%rdx, %rbx
	setle	%bl
	orb	%bl, %r14b
	je	.L307
	cmpl	$3, %r9d
	jbe	.L307
	movq	-64(%rbp), %rbx
	shrl	$2, %eax
	movl	%eax, %edi
	xorl	%eax, %eax
	addq	%rbx, %rsi
	addq	%rbx, %rdx
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L308:
	movdqu	(%rsi,%rax), %xmm5
	movups	%xmm5, (%rdx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L308
.L309:
	leal	1(%r11,%r9), %r11d
	leal	1(%r8,%r9), %r8d
	.p2align 4,,10
	.p2align 3
.L290:
	cmpl	144152(%r12), %r11d
	jge	.L565
	cmpl	$2176, %r11d
	movl	$1, %eax
	cmove	%eax, %r13d
	movl	$32, %eax
	cmove	%eax, %ecx
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L555:
	xorl	%eax, %eax
	cmpq	$0, 8(%rdi)
	sete	%al
	cmpl	%eax, -52(%rbp)
	jne	.L260
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L343:
	cmpl	$65535, %r9d
	jg	.L344
	cmpl	$63359, %r9d
	movl	%r9d, %r13d
	setg	-56(%rbp)
	jmp	.L341
.L277:
	movl	40(%rbx), %eax
	movl	$1114112, 144168(%r12)
	movl	$1114112, 44(%rbx)
	movl	%eax, -100(%rbp)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L299:
	subl	$4, %edi
	addq	$16, %rsi
	cmpl	%ebx, %edi
	jne	.L371
.L372:
	testl	%ebx, %ebx
	jg	.L385
	cmpl	%r8d, %r11d
	jg	.L385
	testl	%r13d, %r13d
	jle	.L379
	movl	%r11d, 144176(%r14)
	leal	32(%r11), %edx
	movl	%r13d, %r11d
	subl	$1, %r11d
	je	.L311
	movl	%edx, 144180(%r14)
.L311:
	movl	%r11d, %eax
	sall	$5, %eax
	leal	(%rax,%rdx), %r11d
	movl	%r11d, %r8d
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L292:
	jne	.L293
	.p2align 4,,10
	.p2align 3
.L294:
	testl	%r13d, %r13d
	jle	.L298
	movl	%edi, 144176(%r14)
	addl	$32, %edi
	cmpl	$1, %r13d
	je	.L298
	movl	%edi, 144180(%r14)
.L298:
	addl	%ecx, %r11d
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L560:
	subl	$32, %eax
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L565:
	leal	1(%r8), %eax
	movq	-88(%rbp), %rbx
	movl	%r8d, %edi
	movq	-96(%rbp), %r15
	movl	%eax, -88(%rbp)
	movl	144144(%r12), %esi
	movslq	%r8d, %rax
	andl	$3, %edi
	salq	$2, %rax
	movq	%rax, -96(%rbp)
	testl	%esi, %esi
	jle	.L284
.L375:
	xorl	%edx, %edx
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L312:
	movl	12800(%r12), %eax
	sarl	$5, %eax
	cltq
	movl	144176(%r12,%rax,4), %eax
	movl	%eax, 12800(%r12)
	cmpl	$2657, %esi
	jle	.L284
	movl	$2657, %eax
.L313:
	leaq	(%r12,%rax,4), %rcx
	leal	1(%rax), %edx
	movl	2176(%rcx), %eax
	sarl	$5, %eax
	cltq
	movl	144176(%r12,%rax,4), %eax
	movl	%eax, 2176(%rcx)
	cmpl	%esi, %edx
	jge	.L284
.L285:
	cmpl	$2080, %edx
	je	.L312
	movslq	%edx, %rax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L284:
	movl	144164(%r12), %eax
	sarl	$5, %eax
	cltq
	movl	144176(%r12,%rax,4), %eax
	movl	%eax, 144164(%r12)
	testl	%edi, %edi
	je	.L286
	leal	1(%r8), %eax
	movq	-64(%rbp), %rcx
	cltq
	.p2align 4,,10
	.p2align 3
.L314:
	movl	144136(%r12), %edx
	movl	%eax, %r8d
	movl	%edx, -4(%rcx,%rax,4)
	addq	$1, %rax
	testb	$3, %r8b
	jne	.L314
	leal	1(%r8), %eax
	movl	%eax, -88(%rbp)
	movslq	%r8d, %rax
	salq	$2, %rax
	movq	%rax, -96(%rbp)
.L286:
	cmpl	$65536, -80(%rbp)
	movl	%r8d, 144152(%r12)
	jg	.L566
.L315:
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdi
	movl	-100(%rbp), %esi
	movq	-64(%rbp), %rcx
	movl	%eax, 144152(%r12)
	movq	-64(%rbp), %rax
	movl	%esi, (%rax,%rdi)
	movslq	144152(%r12), %rax
	testb	$3, %al
	je	.L340
	.p2align 4,,10
	.p2align 3
.L338:
	leal	1(%rax), %edx
	movl	%edx, 144152(%r12)
	movl	36(%rbx), %edx
	movl	%edx, (%rcx,%rax,4)
	movslq	144152(%r12), %rax
	testb	$3, %al
	jne	.L338
.L340:
	movb	$1, 144172(%r12)
	movl	-72(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L339
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L566:
	movdqa	.LC4(%rip), %xmm0
	movl	144168(%r12), %eax
	movl	$2048, 144304(%r12)
	leaq	2176(%r12), %r9
	movl	144144(%r12), %r10d
	movups	%xmm0, 144176(%r12)
	leal	-65536(%rax), %esi
	movdqa	.LC5(%rip), %xmm0
	sarl	$11, %esi
	movups	%xmm0, 144192(%r12)
	addl	$2112, %esi
	movdqa	.LC6(%rip), %xmm0
	movups	%xmm0, 144208(%r12)
	movdqa	.LC7(%rip), %xmm0
	movups	%xmm0, 144224(%r12)
	movdqa	.LC8(%rip), %xmm0
	movups	%xmm0, 144240(%r12)
	movdqa	.LC9(%rip), %xmm0
	movups	%xmm0, 144256(%r12)
	movdqa	.LC10(%rip), %xmm0
	movups	%xmm0, 144272(%r12)
	movdqa	.LC11(%rip), %xmm0
	movups	%xmm0, 144288(%r12)
	cmpl	$2656, %r10d
	jle	.L316
	leaq	4(%r12), %rax
	movl	$2656, %r11d
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L317:
	movslq	%r11d, %rax
	leaq	(%r9,%rax,4), %rdx
	cmpl	$63, %esi
	jle	.L318
	movq	%r9, %rcx
	leal	-63(%rsi), %r8d
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L370:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L321:
	movl	(%rdx,%rax), %r14d
	cmpl	%r14d, (%rcx,%rax)
	jne	.L319
	addq	$4, %rax
	cmpq	$256, %rax
	jne	.L321
	movl	%r11d, %eax
	addl	$64, %r11d
	sarl	$6, %eax
	cltq
	movl	%edi, 144176(%r12,%rax,4)
.L334:
	cmpl	%r10d, %r11d
	jl	.L317
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L335:
	movl	(%rdx), %eax
	addq	$4, %rdx
	sarl	$6, %eax
	cltq
	movl	144176(%r12,%rax,4), %eax
	movl	%eax, -4(%rdx)
	cmpq	%rdx, %r9
	jne	.L335
	movl	144160(%r12), %eax
	sarl	$6, %eax
	cltq
	movl	144176(%r12,%rax,4), %eax
	movl	%eax, 144160(%r12)
	testb	$3, %sil
	je	.L336
	leal	1(%rsi), %eax
	cltq
	.p2align 4,,10
	.p2align 3
.L337:
	movl	$262140, 2172(%r12,%rax,4)
	movl	%eax, %esi
	addq	$1, %rax
	testb	$3, %sil
	jne	.L337
.L336:
	movl	%esi, 144144(%r12)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L319:
	addl	$1, %edi
	addq	$4, %rcx
	cmpl	%edi, %r8d
	jne	.L370
.L318:
	movslq	%esi, %r14
	movl	$63, %edi
	leaq	1924(%r12,%r14,4), %r8
	.p2align 4,,10
	.p2align 3
.L325:
	movl	%esi, %r13d
	xorl	%eax, %eax
	subl	%edi, %r13d
	.p2align 4,,10
	.p2align 3
.L324:
	movl	(%rdx,%rax,4), %ecx
	cmpl	%ecx, (%r8,%rax,4)
	jne	.L322
	addq	$1, %rax
	movl	%edi, %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jg	.L324
	movl	%r11d, %eax
	sarl	$6, %eax
.L327:
	addl	%edi, %r11d
	cltq
	movl	$64, %r8d
	movl	$63, %ecx
	movl	%r13d, 144176(%r12,%rax,4)
	leal	3(%r11), %eax
	subl	%edi, %r8d
	subl	%edi, %ecx
	cmpl	%esi, %eax
	leal	3(%rsi), %eax
	setl	%dl
	cmpl	%r11d, %eax
	setl	%al
	orb	%al, %dl
	je	.L329
	cmpl	$3, %ecx
	jbe	.L329
	movslq	%r11d, %rax
	movl	%r8d, %edi
	leaq	2176(%r12,%rax,4), %rdx
	shrl	$2, %edi
	leaq	2176(%r12,%r14,4), %rax
	movdqu	(%rdx), %xmm5
	movups	%xmm5, (%rax)
	cmpl	$1, %edi
	je	.L330
	movdqu	16(%rdx), %xmm5
	movups	%xmm5, 16(%rax)
	cmpl	$2, %edi
	je	.L330
	movdqu	32(%rdx), %xmm5
	movups	%xmm5, 32(%rax)
	cmpl	$3, %edi
	je	.L330
	movdqu	48(%rdx), %xmm5
	movups	%xmm5, 48(%rax)
	cmpl	$4, %edi
	je	.L330
	movdqu	64(%rdx), %xmm5
	movups	%xmm5, 64(%rax)
	cmpl	$5, %edi
	je	.L330
	movdqu	80(%rdx), %xmm4
	movups	%xmm4, 80(%rax)
	cmpl	$6, %edi
	je	.L330
	movdqu	96(%rdx), %xmm5
	movups	%xmm5, 96(%rax)
	cmpl	$7, %edi
	je	.L330
	movdqu	112(%rdx), %xmm4
	movups	%xmm4, 112(%rax)
	cmpl	$8, %edi
	je	.L330
	movdqu	128(%rdx), %xmm5
	movups	%xmm5, 128(%rax)
	cmpl	$9, %edi
	je	.L330
	movdqu	144(%rdx), %xmm4
	movups	%xmm4, 144(%rax)
	cmpl	$10, %edi
	je	.L330
	movdqu	160(%rdx), %xmm5
	movups	%xmm5, 160(%rax)
	cmpl	$11, %edi
	je	.L330
	movdqu	176(%rdx), %xmm6
	movups	%xmm6, 176(%rax)
	cmpl	$12, %edi
	je	.L330
	movdqu	192(%rdx), %xmm7
	movups	%xmm7, 192(%rax)
	cmpl	$13, %edi
	je	.L330
	movdqu	208(%rdx), %xmm4
	movups	%xmm4, 208(%rax)
	cmpl	$14, %edi
	je	.L330
	movdqu	224(%rdx), %xmm5
	movups	%xmm5, 224(%rax)
	cmpl	$15, %edi
	je	.L330
	movdqu	240(%rdx), %xmm6
	movups	%xmm6, 240(%rax)
.L332:
	leal	1(%r11,%rcx), %r11d
	leal	1(%rsi,%rcx), %esi
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L322:
	addq	$4, %r8
	subl	$1, %edi
	jne	.L325
	movl	%r11d, %eax
	sarl	$6, %eax
	cmpl	%r11d, %esi
	jl	.L567
	cltq
	movl	%r11d, 144176(%r12,%rax,4)
	addl	$64, %r11d
	movl	%r11d, %esi
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L558:
	subl	$2048, %eax
	jmp	.L269
.L561:
	movl	%r8d, %edx
	jmp	.L274
.L347:
	leal	(%rdx,%rax,4), %edx
	jmp	.L348
.L307:
	movq	-64(%rbp), %rax
	movq	-112(%rbp), %rbx
	movl	%r9d, %edx
	subq	%r10, %rdi
	addq	%r10, %rdx
	leaq	(%rax,%r10,4), %rax
	leaq	(%rbx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L310:
	movl	(%rax), %esi
	movl	%esi, (%rax,%rdi,4)
	addq	$4, %rax
	cmpq	%rdx, %rax
	jne	.L310
	jmp	.L309
.L330:
	movl	%r8d, %eax
	movl	%r8d, %r13d
	andl	$-4, %eax
	andl	$3, %r13d
	leal	(%r11,%rax), %edi
	leal	(%rsi,%rax), %edx
	cmpl	%eax, %r8d
	je	.L332
	movslq	%edi, %rdi
	movslq	%edx, %rdx
	leaq	(%r12,%rdi,4), %rdi
	leaq	(%r12,%rdx,4), %rax
	movl	2176(%rdi), %r8d
	movl	%r8d, 2176(%rax)
	cmpl	$1, %r13d
	je	.L332
	movl	2180(%rdi), %edx
	movl	%edx, 2180(%rax)
	cmpl	$2, %r13d
	je	.L332
	movl	2184(%rdi), %edx
	movl	%edx, 2184(%rax)
	jmp	.L332
.L364:
	movq	$0, 8(%rbx)
	movslq	%esi, %rdx
	movq	%r8, %rsi
	movq	%rdi, 16(%rbx)
	salq	$2, %rdx
	call	memcpy@PLT
	movq	144128(%r12), %r8
	jmp	.L365
.L379:
	movl	%r11d, %r8d
	jmp	.L290
.L281:
	movl	144144(%r12), %esi
	testl	%esi, %esi
	jle	.L374
	movq	$768, -96(%rbp)
	movl	$192, %r8d
	xorl	%edi, %edi
	movl	$193, -88(%rbp)
	jmp	.L375
.L384:
	movq	%r8, %rax
	jmp	.L366
.L329:
	movslq	%r11d, %rdx
	leal	-1(%r8), %edi
	movq	-80(%rbp), %r8
	addq	%rdx, %rdi
	leaq	(%r12,%rdx,4), %rax
	subq	%rdx, %r14
	leaq	(%r8,%rdi,4), %rdi
.L333:
	movl	2176(%rax), %edx
	movl	%edx, 2176(%rax,%r14,4)
	addq	$4, %rax
	cmpq	%rax, %rdi
	jne	.L333
	jmp	.L332
.L385:
	movl	%r8d, %r15d
	movl	%ebx, %edi
	subl	%ebx, %r15d
	jmp	.L300
.L383:
	movq	%rdi, %rax
	jmp	.L360
.L382:
	movq	%rdi, %rax
	jmp	.L356
.L374:
	movl	144164(%r12), %eax
	movq	$768, -96(%rbp)
	movl	$192, %r8d
	movl	$193, -88(%rbp)
	sarl	$5, %eax
	cltq
	movl	144176(%r12,%rax,4), %eax
	movl	%eax, 144164(%r12)
	jmp	.L286
.L567:
	movl	%esi, %r13d
	jmp	.L327
.L557:
	movl	$7, (%r15)
	jmp	.L255
	.cfi_endproc
.LFE2768:
	.size	utrie2_freeze_67, .-utrie2_freeze_67
	.p2align 4
	.globl	utrie2_fromUTrie_67
	.type	utrie2_fromUTrie_67, @function
utrie2_fromUTrie_67:
.LFB2745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L589
	movq	%rdi, %r12
	movq	%rdx, %r13
	testq	%rdi, %rdi
	je	.L590
	movl	32(%rdi), %edi
	call	utrie2_open_67.part.0
	movq	%rax, -80(%rbp)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L589
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-80(%rbp), %rcx
	movb	$1, -68(%rbp)
	leaq	_ZL13copyEnumRangePKviij(%rip), %rdx
	movl	%eax, -72(%rbp)
	movl	$55296, %ebx
	call	utrie_enum_67@PLT
	movl	-72(%rbp), %esi
	movq	-80(%rbp), %rdi
	movl	%esi, 0(%r13)
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L573:
	movl	(%rdx,%rax,4), %r15d
.L574:
	cmpl	%r15d, 32(%r12)
	je	.L575
	testl	%esi, %esi
	jg	.L575
	movq	72(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L576
	cmpb	$0, 144172(%rcx)
	jne	.L576
	xorl	%edx, %edx
	movq	%rcx, %rdi
	movl	%ebx, %esi
	movq	%rcx, -88(%rbp)
	call	_ZL12getDataBlockP9UNewTrie2ia
	movq	-88(%rbp), %rcx
	testl	%eax, %eax
	js	.L591
	addl	%eax, %r14d
	movq	144128(%rcx), %rax
	movl	0(%r13), %esi
	movslq	%r14d, %r14
	movq	-80(%rbp), %rdi
	movl	%r15d, (%rax,%r14,4)
	.p2align 4,,10
	.p2align 3
.L575:
	addl	$1, %ebx
	cmpl	$56320, %ebx
	je	.L592
.L579:
	movl	%ebx, %eax
	movq	(%r12), %rcx
	movl	%ebx, %r14d
	movq	8(%r12), %rdx
	sarl	$5, %eax
	andl	$31, %r14d
	cltq
	movzwl	(%rcx,%rax,2), %eax
	leal	(%r14,%rax,4), %eax
	cltq
	testq	%rdx, %rdx
	jne	.L573
	movzwl	(%rcx,%rax,2), %r15d
	jmp	.L574
.L592:
	testl	%esi, %esi
	jle	.L593
.L580:
	call	utrie2_close_67@PLT
.L589:
	xorl	%eax, %eax
.L568:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L594
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	.cfi_restore_state
	movl	$30, 0(%r13)
	movl	$30, %esi
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L591:
	movl	$7, 0(%r13)
	movq	-80(%rbp), %rdi
	movl	$7, %esi
	jmp	.L575
.L593:
	xorl	%esi, %esi
	cmpq	$0, 8(%r12)
	movq	%r13, %rdx
	setne	%sil
	call	utrie2_freeze_67
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L581
	movq	-80(%rbp), %rax
	jmp	.L568
.L590:
	movl	$1, (%rdx)
	xorl	%eax, %eax
	jmp	.L568
.L581:
	movq	-80(%rbp), %rdi
	jmp	.L580
.L594:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2745:
	.size	utrie2_fromUTrie_67, .-utrie2_fromUTrie_67
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	32
	.long	64
	.long	96
	.align 16
.LC1:
	.long	1
	.long	1
	.long	1
	.long	1
	.align 16
.LC2:
	.long	0
	.long	0
	.long	34845
	.long	0
	.align 16
.LC3:
	.long	192
	.long	192
	.long	192
	.long	192
	.align 16
.LC4:
	.long	0
	.long	64
	.long	128
	.long	192
	.align 16
.LC5:
	.long	256
	.long	320
	.long	384
	.long	448
	.align 16
.LC6:
	.long	512
	.long	576
	.long	640
	.long	704
	.align 16
.LC7:
	.long	768
	.long	832
	.long	896
	.long	960
	.align 16
.LC8:
	.long	1024
	.long	1088
	.long	1152
	.long	1216
	.align 16
.LC9:
	.long	1280
	.long	1344
	.long	1408
	.long	1472
	.align 16
.LC10:
	.long	1536
	.long	1600
	.long	1664
	.long	1728
	.align 16
.LC11:
	.long	1792
	.long	1856
	.long	1920
	.long	1984
	.align 16
.LC12:
	.long	2656
	.long	2656
	.long	2656
	.long	2656
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
