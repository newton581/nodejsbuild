	.file	"ucnv_cnv.cpp"
	.text
	.p2align 4
	.globl	ucnv_getCompleteUnicodeSet_67
	.type	ucnv_getCompleteUnicodeSet_67, @function
ucnv_getCompleteUnicodeSet_67:
.LFB2113:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	movq	16(%rsi), %rcx
	movl	$1114111, %edx
	xorl	%esi, %esi
	movq	(%rax), %rdi
	jmp	*%rcx
	.cfi_endproc
.LFE2113:
	.size	ucnv_getCompleteUnicodeSet_67, .-ucnv_getCompleteUnicodeSet_67
	.p2align 4
	.globl	ucnv_getNonSurrogateUnicodeSet_67
	.type	ucnv_getNonSurrogateUnicodeSet_67, @function
ucnv_getNonSurrogateUnicodeSet_67:
.LFB2114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$55295, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$8, %rsp
	movq	(%rbx), %rdi
	call	*16(%rbx)
	movq	16(%rbx), %rax
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	movl	$1114111, %edx
	movl	$57344, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2114:
	.size	ucnv_getNonSurrogateUnicodeSet_67, .-ucnv_getNonSurrogateUnicodeSet_67
	.p2align 4
	.globl	ucnv_fromUWriteBytes_67
	.type	ucnv_fromUWriteBytes_67, @function
ucnv_fromUWriteBytes_67:
.LFB2115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%rcx), %rdi
	movl	16(%rbp), %r11d
	cmpq	%r8, %rdi
	setnb	%bl
	testl	%edx, %edx
	setle	%al
	orl	%eax, %ebx
	testq	%r9, %r9
	je	.L9
	movq	(%r9), %rax
	testq	%rax, %rax
	je	.L9
	testb	%bl, %bl
	jne	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movsb
	subl	$1, %edx
	addq	$4, %rax
	movl	%r11d, -4(%rax)
	testl	%edx, %edx
	jle	.L11
	cmpq	%rdi, %r8
	ja	.L12
.L11:
	movq	%rax, (%r9)
.L7:
	movq	%rdi, (%rcx)
	testl	%edx, %edx
	jle	.L5
	testq	%r10, %r10
	je	.L19
	leaq	16(%rsi), %rax
	leaq	104(%r10), %r8
	movb	%dl, 91(%r10)
	cmpq	%rax, %r8
	leaq	120(%r10), %rax
	setnb	%cl
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %cl
	leal	-1(%rdx), %ecx
	je	.L16
	cmpl	$14, %ecx
	jbe	.L16
	movl	%edx, %ecx
	xorl	%eax, %eax
	shrl	$4, %ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L17:
	movdqu	(%rsi,%rax), %xmm0
	movups	%xmm0, 104(%r10,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L17
	movl	%edx, %ecx
	movl	%edx, %edi
	andl	$-16, %ecx
	movl	%ecx, %eax
	subl	%ecx, %edi
	addq	%rax, %rsi
	addq	%r8, %rax
	cmpl	%ecx, %edx
	je	.L19
	movzbl	(%rsi), %edx
	movb	%dl, (%rax)
	cmpl	$1, %edi
	je	.L19
	movzbl	1(%rsi), %edx
	movb	%dl, 1(%rax)
	cmpl	$2, %edi
	je	.L19
	movzbl	2(%rsi), %edx
	movb	%dl, 2(%rax)
	cmpl	$3, %edi
	je	.L19
	movzbl	3(%rsi), %edx
	movb	%dl, 3(%rax)
	cmpl	$4, %edi
	je	.L19
	movzbl	4(%rsi), %edx
	movb	%dl, 4(%rax)
	cmpl	$5, %edi
	je	.L19
	movzbl	5(%rsi), %edx
	movb	%dl, 5(%rax)
	cmpl	$6, %edi
	je	.L19
	movzbl	6(%rsi), %edx
	movb	%dl, 6(%rax)
	cmpl	$7, %edi
	je	.L19
	movzbl	7(%rsi), %edx
	movb	%dl, 7(%rax)
	cmpl	$8, %edi
	je	.L19
	movzbl	8(%rsi), %edx
	movb	%dl, 8(%rax)
	cmpl	$9, %edi
	je	.L19
	movzbl	9(%rsi), %edx
	movb	%dl, 9(%rax)
	cmpl	$10, %edi
	je	.L19
	movzbl	10(%rsi), %edx
	movb	%dl, 10(%rax)
	cmpl	$11, %edi
	je	.L19
	movzbl	11(%rsi), %edx
	movb	%dl, 11(%rax)
	cmpl	$12, %edi
	je	.L19
	movzbl	12(%rsi), %edx
	movb	%dl, 12(%rax)
	cmpl	$13, %edi
	je	.L19
	movzbl	13(%rsi), %edx
	movb	%dl, 13(%rax)
	cmpl	$14, %edi
	je	.L19
	movzbl	14(%rsi), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L19:
	movq	24(%rbp), %rax
	movl	$15, (%rax)
.L5:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	testb	%bl, %bl
	jne	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	movsb
	subl	$1, %edx
	testl	%edx, %edx
	jle	.L7
	cmpq	%rdi, %r8
	ja	.L8
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, 104(%r10,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rcx, %rdx
	jne	.L21
	jmp	.L19
	.cfi_endproc
.LFE2115:
	.size	ucnv_fromUWriteBytes_67, .-ucnv_fromUWriteBytes_67
	.p2align 4
	.globl	ucnv_toUWriteUChars_67
	.type	ucnv_toUWriteUChars_67, @function
ucnv_toUWriteUChars_67:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%rcx), %rdi
	movl	16(%rbp), %r11d
	cmpq	%r8, %rdi
	setnb	%bl
	testl	%edx, %edx
	setle	%al
	orl	%eax, %ebx
	testq	%r9, %r9
	je	.L84
	movq	(%r9), %rax
	testq	%rax, %rax
	je	.L84
	testb	%bl, %bl
	jne	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	movsw
	subl	$1, %edx
	addq	$4, %rax
	movl	%r11d, -4(%rax)
	testl	%edx, %edx
	jle	.L86
	cmpq	%rdi, %r8
	ja	.L87
.L86:
	movq	%rax, (%r9)
.L82:
	movq	%rdi, (%rcx)
	testl	%edx, %edx
	jle	.L80
	testq	%r10, %r10
	je	.L94
	leaq	16(%rsi), %rax
	leaq	144(%r10), %rdi
	movb	%dl, 93(%r10)
	cmpq	%rax, %rdi
	leaq	160(%r10), %rax
	setnb	%cl
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %cl
	leal	-1(%rdx), %eax
	je	.L91
	cmpl	$6, %eax
	jbe	.L91
	movl	%edx, %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L92:
	movdqu	(%rsi,%rax), %xmm0
	movups	%xmm0, 144(%r10,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L92
	movl	%edx, %r8d
	movl	%edx, %ecx
	andl	$-8, %r8d
	movl	%r8d, %eax
	subl	%r8d, %ecx
	addq	%rax, %rax
	addq	%rax, %rsi
	addq	%rdi, %rax
	cmpl	%r8d, %edx
	je	.L94
	movzwl	(%rsi), %edx
	movw	%dx, (%rax)
	cmpl	$1, %ecx
	je	.L94
	movzwl	2(%rsi), %edx
	movw	%dx, 2(%rax)
	cmpl	$2, %ecx
	je	.L94
	movzwl	4(%rsi), %edx
	movw	%dx, 4(%rax)
	cmpl	$3, %ecx
	je	.L94
	movzwl	6(%rsi), %edx
	movw	%dx, 6(%rax)
	cmpl	$4, %ecx
	je	.L94
	movzwl	8(%rsi), %edx
	movw	%dx, 8(%rax)
	cmpl	$5, %ecx
	je	.L94
	movzwl	10(%rsi), %edx
	movw	%dx, 10(%rax)
	cmpl	$6, %ecx
	je	.L94
	movzwl	12(%rsi), %edx
	movw	%dx, 12(%rax)
.L94:
	movq	24(%rbp), %rax
	movl	$15, (%rax)
.L80:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	testb	%bl, %bl
	jne	.L82
	.p2align 4,,10
	.p2align 3
.L83:
	movsw
	subl	$1, %edx
	testl	%edx, %edx
	jle	.L82
	cmpq	%rdi, %r8
	ja	.L83
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L91:
	movl	%eax, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L96:
	movzwl	(%rsi,%rax,2), %ecx
	movw	%cx, 144(%r10,%rax,2)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L96
	jmp	.L94
	.cfi_endproc
.LFE2116:
	.size	ucnv_toUWriteUChars_67, .-ucnv_toUWriteUChars_67
	.p2align 4
	.globl	ucnv_toUWriteCodePoint_67
	.type	ucnv_toUWriteCodePoint_67, @function
ucnv_toUWriteCodePoint_67:
.LFB2117:
	.cfi_startproc
	endbr64
	movq	(%rdx), %rax
	cmpq	%rcx, %rax
	jnb	.L132
	leaq	2(%rax), %r11
	cmpl	$65535, %esi
	jg	.L133
	movw	%si, (%rax)
	movq	%r11, %rcx
	testq	%r8, %r8
	je	.L155
.L146:
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L155
	movl	%r9d, (%rax)
	cmpq	%rcx, %r11
	jnb	.L159
	movl	%r9d, 4(%rax)
	addq	$8, %rax
	movq	%rax, (%r8)
	movq	%rcx, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	testl	%esi, %esi
	js	.L157
	testq	%rdi, %rdi
	je	.L139
	cmpl	$65535, %esi
	jle	.L152
	movl	%esi, %r10d
	andw	$1023, %si
	movl	$2, %eax
	orw	$-9216, %si
	sarl	$10, %r10d
	movw	%si, 146(%rdi)
	subw	$10304, %r10w
.L141:
	movw	%r10w, 144(%rdi)
	movb	%al, 93(%rdi)
.L139:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rax
	movl	$15, (%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore 6
	movl	%esi, %r10d
	andw	$1023, %si
	sarl	$10, %r10d
	orw	$-9216, %si
	subw	$10304, %r10w
	movw	%r10w, (%rax)
	movl	%esi, %r10d
	cmpq	%rcx, %r11
	jb	.L160
	movzwl	%si, %esi
	testq	%r8, %r8
	je	.L161
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L148
	movl	%r9d, (%rax)
	addq	$4, %rax
	movq	%rax, (%r8)
.L148:
	movq	%r11, (%rdx)
	testq	%rdi, %rdi
	je	.L139
.L152:
	movl	%esi, %r10d
.L142:
	movl	$1, %eax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L157:
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	movw	%si, 2(%rax)
	leaq	4(%rax), %rcx
	testq	%r8, %r8
	jne	.L146
.L155:
	movq	%rcx, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	addq	$4, %rax
	movq	%rax, (%r8)
	movq	%rcx, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r11, (%rdx)
	testq	%rdi, %rdi
	jne	.L142
	jmp	.L139
	.cfi_endproc
.LFE2117:
	.size	ucnv_toUWriteCodePoint_67, .-ucnv_toUWriteCodePoint_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
