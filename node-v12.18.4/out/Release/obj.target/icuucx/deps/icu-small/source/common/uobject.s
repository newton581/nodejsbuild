	.file	"uobject.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UObjectD2Ev
	.type	_ZN6icu_677UObjectD2Ev, @function
_ZN6icu_677UObjectD2Ev:
.LFB2058:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2058:
	.size	_ZN6icu_677UObjectD2Ev, .-_ZN6icu_677UObjectD2Ev
	.globl	_ZN6icu_677UObjectD1Ev
	.set	_ZN6icu_677UObjectD1Ev,_ZN6icu_677UObjectD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UObject17getDynamicClassIDEv
	.type	_ZNK6icu_677UObject17getDynamicClassIDEv, @function
_ZNK6icu_677UObject17getDynamicClassIDEv:
.LFB2061:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2061:
	.size	_ZNK6icu_677UObject17getDynamicClassIDEv, .-_ZNK6icu_677UObject17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UObjectD0Ev
	.type	_ZN6icu_677UObjectD0Ev, @function
_ZN6icu_677UObjectD0Ev:
.LFB2060:
	.cfi_startproc
	endbr64
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2060:
	.size	_ZN6icu_677UObjectD0Ev, .-_ZN6icu_677UObjectD0Ev
	.p2align 4
	.globl	_ZN6icu_677UMemorynwEm
	.type	_ZN6icu_677UMemorynwEm, @function
_ZN6icu_677UMemorynwEm:
.LFB2053:
	.cfi_startproc
	endbr64
	jmp	uprv_malloc_67@PLT
	.cfi_endproc
.LFE2053:
	.size	_ZN6icu_677UMemorynwEm, .-_ZN6icu_677UMemorynwEm
	.p2align 4
	.globl	_ZN6icu_677UMemorydlEPv
	.type	_ZN6icu_677UMemorydlEPv, @function
_ZN6icu_677UMemorydlEPv:
.LFB2054:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L6
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	ret
	.cfi_endproc
.LFE2054:
	.size	_ZN6icu_677UMemorydlEPv, .-_ZN6icu_677UMemorydlEPv
	.p2align 4
	.globl	_ZN6icu_677UMemorynaEm
	.type	_ZN6icu_677UMemorynaEm, @function
_ZN6icu_677UMemorynaEm:
.LFB2524:
	.cfi_startproc
	endbr64
	jmp	uprv_malloc_67@PLT
	.cfi_endproc
.LFE2524:
	.size	_ZN6icu_677UMemorynaEm, .-_ZN6icu_677UMemorynaEm
	.p2align 4
	.globl	_ZN6icu_677UMemorydaEPv
	.type	_ZN6icu_677UMemorydaEPv, @function
_ZN6icu_677UMemorydaEPv:
.LFB2526:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L9
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	ret
	.cfi_endproc
.LFE2526:
	.size	_ZN6icu_677UMemorydaEPv, .-_ZN6icu_677UMemorydaEPv
	.p2align 4
	.globl	uprv_deleteUObject_67
	.type	uprv_deleteUObject_67, @function
uprv_deleteUObject_67:
.LFB2062:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	leaq	_ZN6icu_677UObjectD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L13
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	jmp	*%rax
	.cfi_endproc
.LFE2062:
	.size	uprv_deleteUObject_67, .-uprv_deleteUObject_67
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_677UObjectE
	.section	.rodata._ZTSN6icu_677UObjectE,"aG",@progbits,_ZTSN6icu_677UObjectE,comdat
	.align 16
	.type	_ZTSN6icu_677UObjectE, @object
	.size	_ZTSN6icu_677UObjectE, 18
_ZTSN6icu_677UObjectE:
	.string	"N6icu_677UObjectE"
	.weak	_ZTIN6icu_677UObjectE
	.section	.data.rel.ro._ZTIN6icu_677UObjectE,"awG",@progbits,_ZTIN6icu_677UObjectE,comdat
	.align 8
	.type	_ZTIN6icu_677UObjectE, @object
	.size	_ZTIN6icu_677UObjectE, 24
_ZTIN6icu_677UObjectE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_677UObjectE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTVN6icu_677UObjectE
	.section	.data.rel.ro.local._ZTVN6icu_677UObjectE,"awG",@progbits,_ZTVN6icu_677UObjectE,comdat
	.align 8
	.type	_ZTVN6icu_677UObjectE, @object
	.size	_ZTVN6icu_677UObjectE, 40
_ZTVN6icu_677UObjectE:
	.quad	0
	.quad	_ZTIN6icu_677UObjectE
	.quad	_ZN6icu_677UObjectD1Ev
	.quad	_ZN6icu_677UObjectD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
