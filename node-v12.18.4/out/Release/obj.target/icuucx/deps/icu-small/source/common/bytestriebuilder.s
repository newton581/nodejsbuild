	.file	"bytestriebuilder.cpp"
	.text
	.section	.text._ZNK6icu_6716BytesTrieBuilder23matchNodesCanHaveValuesEv,"axG",@progbits,_ZNK6icu_6716BytesTrieBuilder23matchNodesCanHaveValuesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6716BytesTrieBuilder23matchNodesCanHaveValuesEv
	.type	_ZNK6icu_6716BytesTrieBuilder23matchNodesCanHaveValuesEv, @function
_ZNK6icu_6716BytesTrieBuilder23matchNodesCanHaveValuesEv:
.LFB1172:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE1172:
	.size	_ZNK6icu_6716BytesTrieBuilder23matchNodesCanHaveValuesEv, .-_ZNK6icu_6716BytesTrieBuilder23matchNodesCanHaveValuesEv
	.section	.text._ZNK6icu_6716BytesTrieBuilder31getMaxBranchLinearSubNodeLengthEv,"axG",@progbits,_ZNK6icu_6716BytesTrieBuilder31getMaxBranchLinearSubNodeLengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6716BytesTrieBuilder31getMaxBranchLinearSubNodeLengthEv
	.type	_ZNK6icu_6716BytesTrieBuilder31getMaxBranchLinearSubNodeLengthEv, @function
_ZNK6icu_6716BytesTrieBuilder31getMaxBranchLinearSubNodeLengthEv:
.LFB1173:
	.cfi_startproc
	endbr64
	movl	$5, %eax
	ret
	.cfi_endproc
.LFE1173:
	.size	_ZNK6icu_6716BytesTrieBuilder31getMaxBranchLinearSubNodeLengthEv, .-_ZNK6icu_6716BytesTrieBuilder31getMaxBranchLinearSubNodeLengthEv
	.section	.text._ZNK6icu_6716BytesTrieBuilder17getMinLinearMatchEv,"axG",@progbits,_ZNK6icu_6716BytesTrieBuilder17getMinLinearMatchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6716BytesTrieBuilder17getMinLinearMatchEv
	.type	_ZNK6icu_6716BytesTrieBuilder17getMinLinearMatchEv, @function
_ZNK6icu_6716BytesTrieBuilder17getMinLinearMatchEv:
.LFB1174:
	.cfi_startproc
	endbr64
	movl	$16, %eax
	ret
	.cfi_endproc
.LFE1174:
	.size	_ZNK6icu_6716BytesTrieBuilder17getMinLinearMatchEv, .-_ZNK6icu_6716BytesTrieBuilder17getMinLinearMatchEv
	.section	.text._ZNK6icu_6716BytesTrieBuilder23getMaxLinearMatchLengthEv,"axG",@progbits,_ZNK6icu_6716BytesTrieBuilder23getMaxLinearMatchLengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6716BytesTrieBuilder23getMaxLinearMatchLengthEv
	.type	_ZNK6icu_6716BytesTrieBuilder23getMaxLinearMatchLengthEv, @function
_ZNK6icu_6716BytesTrieBuilder23getMaxLinearMatchLengthEv:
.LFB1175:
	.cfi_startproc
	endbr64
	movl	$16, %eax
	ret
	.cfi_endproc
.LFE1175:
	.size	_ZNK6icu_6716BytesTrieBuilder23getMaxLinearMatchLengthEv, .-_ZNK6icu_6716BytesTrieBuilder23getMaxLinearMatchLengthEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716BytesTrieBuilder22getElementStringLengthEi
	.type	_ZNK6icu_6716BytesTrieBuilder22getElementStringLengthEi, @function
_ZNK6icu_6716BytesTrieBuilder22getElementStringLengthEi:
.LFB2424:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movslq	%esi, %rsi
	movslq	(%rax,%rsi,8), %rdx
	movq	16(%rdi), %rax
	movq	(%rax), %rcx
	testl	%edx, %edx
	js	.L7
	movzbl	(%rcx,%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%edx, %eax
	negl	%edx
	notl	%eax
	movslq	%edx, %rdx
	cltq
	movzbl	(%rcx,%rdx), %edx
	movzbl	(%rcx,%rax), %eax
	sall	$8, %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE2424:
	.size	_ZNK6icu_6716BytesTrieBuilder22getElementStringLengthEi, .-_ZNK6icu_6716BytesTrieBuilder22getElementStringLengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716BytesTrieBuilder14getElementUnitEii
	.type	_ZNK6icu_6716BytesTrieBuilder14getElementUnitEii, @function
_ZNK6icu_6716BytesTrieBuilder14getElementUnitEii:
.LFB2425:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movslq	%esi, %rsi
	movq	16(%rdi), %r8
	movslq	%edx, %rdx
	movl	(%rax,%rsi,8), %ecx
	movl	$1, %eax
	subl	%ecx, %eax
	leal	1(%rcx), %esi
	testl	%ecx, %ecx
	cmovns	%esi, %eax
	cltq
	addq	(%r8), %rax
	movzbl	(%rax,%rdx), %eax
	ret
	.cfi_endproc
.LFE2425:
	.size	_ZNK6icu_6716BytesTrieBuilder14getElementUnitEii, .-_ZNK6icu_6716BytesTrieBuilder14getElementUnitEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716BytesTrieBuilder15getElementValueEi
	.type	_ZNK6icu_6716BytesTrieBuilder15getElementValueEi, @function
_ZNK6icu_6716BytesTrieBuilder15getElementValueEi:
.LFB2426:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movslq	%esi, %rsi
	movl	4(%rax,%rsi,8), %eax
	ret
	.cfi_endproc
.LFE2426:
	.size	_ZNK6icu_6716BytesTrieBuilder15getElementValueEi, .-_ZNK6icu_6716BytesTrieBuilder15getElementValueEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716BytesTrieBuilder21getLimitOfLinearMatchEiii
	.type	_ZNK6icu_6716BytesTrieBuilder21getLimitOfLinearMatchEiii, @function
_ZNK6icu_6716BytesTrieBuilder21getLimitOfLinearMatchEiii:
.LFB2427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %rax
	leaq	(%rax,%rdx,8), %r11
	movl	(%rax,%rsi,8), %edx
	movq	16(%rdi), %rax
	movq	(%rax), %r9
	testl	%edx, %edx
	js	.L14
	movslq	%edx, %rax
	movzbl	(%r9,%rax), %r10d
.L15:
	movl	$1, %r8d
	movl	$1, %ebx
	movl	%r8d, %eax
	leal	1(%rdx), %r8d
	subl	%edx, %eax
	testl	%edx, %edx
	cmovs	%eax, %r8d
	addl	$1, %ecx
	movslq	%ecx, %rax
	movslq	%r8d, %r8
	addq	%r9, %r8
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L22:
	movl	(%r11), %ecx
	movl	%ebx, %edx
	movzbl	(%r8,%rax), %esi
	subl	%ecx, %edx
	leal	1(%rcx), %edi
	testl	%ecx, %ecx
	cmovns	%edi, %edx
	leaq	(%r9,%rax), %rcx
	addq	$1, %rax
	movslq	%edx, %rdx
	cmpb	%sil, (%rcx,%rdx)
	jne	.L13
.L19:
	movl	%eax, %r12d
	cmpl	%eax, %r10d
	jg	.L22
.L13:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	%edx, %eax
	notl	%eax
	cltq
	movzbl	(%r9,%rax), %r10d
	movl	%edx, %eax
	negl	%eax
	cltq
	sall	$8, %r10d
	movzbl	(%r9,%rax), %eax
	orl	%eax, %r10d
	jmp	.L15
	.cfi_endproc
.LFE2427:
	.size	_ZNK6icu_6716BytesTrieBuilder21getLimitOfLinearMatchEiii, .-_ZNK6icu_6716BytesTrieBuilder21getLimitOfLinearMatchEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716BytesTrieBuilder17countElementUnitsEiii
	.type	_ZNK6icu_6716BytesTrieBuilder17countElementUnitsEiii, @function
_ZNK6icu_6716BytesTrieBuilder17countElementUnitsEiii:
.LFB2428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%ecx, %r9
	movl	$1, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %rbx
	movq	16(%rdi), %rax
	movslq	%esi, %rdi
	movl	(%rbx,%rdi,8), %edi
	addq	(%rax), %r9
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%r10d, %eax
	leal	1(%rsi), %ecx
	leal	1(%rdi), %esi
	subl	%edi, %eax
	testl	%edi, %edi
	cmovns	%esi, %eax
	cltq
	movzbl	(%r9,%rax), %r11d
	cmpl	%edx, %ecx
	jge	.L26
	movslq	%ecx, %rcx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$1, %rcx
	cmpl	%ecx, %edx
	jle	.L26
.L30:
	movl	(%rbx,%rcx,8), %edi
	movl	%r10d, %eax
	movl	%ecx, %esi
	subl	%edi, %eax
	leal	1(%rdi), %r8d
	testl	%edi, %edi
	cmovns	%r8d, %eax
	cltq
	cmpb	(%r9,%rax), %r11b
	je	.L35
	addl	$1, %r12d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L26:
	leal	1(%r12), %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2428:
	.size	_ZNK6icu_6716BytesTrieBuilder17countElementUnitsEiii, .-_ZNK6icu_6716BytesTrieBuilder17countElementUnitsEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716BytesTrieBuilder23skipElementsBySomeUnitsEiii
	.type	_ZNK6icu_6716BytesTrieBuilder23skipElementsBySomeUnitsEiii, @function
_ZNK6icu_6716BytesTrieBuilder23skipElementsBySomeUnitsEiii:
.LFB2429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r9d
	movslq	%edx, %rsi
	movl	$1, %r10d
	movslq	%r9d, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%ecx, %ebx
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	movl	(%rcx,%rdx,8), %edx
	addq	(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%r10d, %eax
	leal	1(%rdx), %r8d
	leal	1(%r9), %edi
	subl	%edx, %eax
	testl	%edx, %edx
	movslq	%edi, %rdi
	cmovns	%r8d, %eax
	cltq
	movzbl	(%rsi,%rax), %r11d
	.p2align 4,,10
	.p2align 3
.L41:
	movl	(%rcx,%rdi,8), %edx
	movl	%r10d, %eax
	movl	%edi, %r9d
	subl	%edx, %eax
	leal	1(%rdx), %r8d
	testl	%edx, %edx
	cmovns	%r8d, %eax
	addq	$1, %rdi
	cltq
	cmpb	(%rsi,%rax), %r11b
	je	.L41
	subl	$1, %ebx
	testl	%ebx, %ebx
	jg	.L42
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2429:
	.size	_ZNK6icu_6716BytesTrieBuilder23skipElementsBySomeUnitsEiii, .-_ZNK6icu_6716BytesTrieBuilder23skipElementsBySomeUnitsEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716BytesTrieBuilder26indexOfElementWithNextUnitEiiDs
	.type	_ZNK6icu_6716BytesTrieBuilder26indexOfElementWithNextUnitEiiDs, @function
_ZNK6icu_6716BytesTrieBuilder26indexOfElementWithNextUnitEiiDs:
.LFB2430:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movslq	%edx, %rdx
	movq	24(%rdi), %r11
	movslq	%esi, %rsi
	movl	$1, %r10d
	addq	(%rax), %rdx
	movq	%rdx, %r8
	.p2align 4,,10
	.p2align 3
.L49:
	movl	(%r11,%rsi,8), %edx
	movl	%r10d, %eax
	movl	%esi, %r9d
	subl	%edx, %eax
	leal	1(%rdx), %edi
	testl	%edx, %edx
	cmovns	%edi, %eax
	addq	$1, %rsi
	cltq
	cmpb	(%r8,%rax), %cl
	je	.L49
	movl	%r9d, %eax
	ret
	.cfi_endproc
.LFE2430:
	.size	_ZNK6icu_6716BytesTrieBuilder26indexOfElementWithNextUnitEiiDs, .-_ZNK6icu_6716BytesTrieBuilder26indexOfElementWithNextUnitEiiDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilderD2Ev
	.type	_ZN6icu_6716BytesTrieBuilderD2Ev, @function
_ZN6icu_6716BytesTrieBuilderD2Ev:
.LFB2415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6716BytesTrieBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L52
	cmpb	$0, 12(%r13)
	jne	.L62
.L53:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L52:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L54
	call	_ZN6icu_677UMemorydaEPv@PLT
.L54:
	movq	40(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717StringTrieBuilderD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L53
	.cfi_endproc
.LFE2415:
	.size	_ZN6icu_6716BytesTrieBuilderD2Ev, .-_ZN6icu_6716BytesTrieBuilderD2Ev
	.globl	_ZN6icu_6716BytesTrieBuilderD1Ev
	.set	_ZN6icu_6716BytesTrieBuilderD1Ev,_ZN6icu_6716BytesTrieBuilderD2Ev
	.section	.text._ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD2Ev,"axG",@progbits,_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD2Ev
	.type	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD2Ev, @function
_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD2Ev:
.LFB3003:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3003:
	.size	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD2Ev, .-_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD2Ev
	.weak	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD1Ev
	.set	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD1Ev,_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD2Ev
	.section	.text._ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD0Ev,"axG",@progbits,_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD0Ev
	.type	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD0Ev, @function
_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD0Ev:
.LFB3005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3005:
	.size	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD0Ev, .-_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716BytesTrieBuilder17BTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE
	.type	_ZNK6icu_6716BytesTrieBuilder17BTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE, @function
_ZNK6icu_6716BytesTrieBuilder17BTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE:
.LFB2438:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L73
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZNK6icu_6717StringTrieBuilder15LinearMatchNodeeqERKNS0_4NodeE@PLT
	testb	%al, %al
	je	.L66
	movslq	24(%rbx), %rdx
	movq	40(%r12), %rsi
	movq	40(%rbx), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
.L66:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2438:
	.size	_ZNK6icu_6716BytesTrieBuilder17BTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE, .-_ZNK6icu_6716BytesTrieBuilder17BTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilderD0Ev
	.type	_ZN6icu_6716BytesTrieBuilderD0Ev, @function
_ZN6icu_6716BytesTrieBuilderD0Ev:
.LFB2417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6716BytesTrieBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L77
	cmpb	$0, 12(%r13)
	jne	.L87
.L78:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L77:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZN6icu_677UMemorydaEPv@PLT
.L79:
	movq	40(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6717StringTrieBuilderD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L78
	.cfi_endproc
.LFE2417:
	.size	_ZN6icu_6716BytesTrieBuilderD0Ev, .-_ZN6icu_6716BytesTrieBuilderD0Ev
	.p2align 4
	.type	compareElementStrings, @function
compareElementStrings:
.LFB2419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movslq	(%rsi), %rcx
	movq	(%rdi), %rsi
	testl	%ecx, %ecx
	js	.L89
	leal	1(%rcx), %edi
	movslq	(%rdx), %rdx
	movzbl	(%rsi,%rcx), %eax
	movslq	%edi, %rdi
	addq	%rsi, %rdi
	testl	%edx, %edx
	js	.L91
.L96:
	movzbl	(%rsi,%rdx), %r8d
	leal	1(%rdx), %ecx
.L92:
	movl	%eax, %ebx
	movslq	%ecx, %rdx
	subl	%r8d, %ebx
	addq	%rdx, %rsi
	testl	%ebx, %ebx
	cmovg	%r8d, %eax
	movslq	%eax, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	cmove	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movl	%ecx, %edi
	negl	%ecx
	movslq	(%rdx), %rdx
	notl	%edi
	movslq	%ecx, %rcx
	movslq	%edi, %rax
	movzbl	(%rsi,%rcx), %ecx
	addl	$2, %edi
	movzbl	(%rsi,%rax), %eax
	movslq	%edi, %rdi
	addq	%rsi, %rdi
	sall	$8, %eax
	orl	%ecx, %eax
	testl	%edx, %edx
	jns	.L96
.L91:
	movl	%edx, %ecx
	negl	%edx
	notl	%ecx
	movslq	%edx, %rdx
	movslq	%ecx, %r8
	movzbl	(%rsi,%rdx), %edx
	addl	$2, %ecx
	movzbl	(%rsi,%r8), %r8d
	sall	$8, %r8d
	orl	%edx, %r8d
	jmp	.L92
	.cfi_endproc
.LFE2419:
	.size	compareElementStrings, .-compareElementStrings
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716BytesTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE
	.type	_ZNK6icu_6716BytesTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE, @function
_ZNK6icu_6716BytesTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE:
.LFB2440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rax
	movq	16(%rdi), %r8
	movl	$48, %edi
	movl	(%rax,%rsi,8), %ecx
	movl	%ecx, %eax
	leal	1(%rcx), %r13d
	notl	%eax
	addl	$2, %eax
	testl	%ecx, %ecx
	cmovns	%r13d, %eax
	movslq	%edx, %r13
	cltq
	addq	%r13, %rax
	addq	(%r8), %rax
	movq	%rax, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L97
	leal	(%r14,%r14,8), %eax
	leal	(%r14,%rax,4), %edx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L101
	movl	8(%rbx), %eax
.L101:
	movq	%r13, %xmm1
	movl	%r14d, 24(%r12)
	movl	%r14d, %esi
	movq	%r13, %rdi
	leal	298634171(%rax,%rdx), %r15d
	leaq	16+_ZTVN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE(%rip), %rax
	movl	$0, 12(%r12)
	movq	%rbx, %xmm0
	movl	%r15d, 8(%r12)
	punpcklqdq	%xmm1, %xmm0
	movb	$0, 16(%r12)
	movl	$0, 20(%r12)
	movq	%rax, (%r12)
	movups	%xmm0, 32(%r12)
	call	ustr_hashCharsN_67@PLT
	movl	%eax, %r8d
	leal	(%r15,%r15,8), %eax
	leal	(%r15,%rax,4), %eax
	addl	%r8d, %eax
	movl	%eax, 8(%r12)
.L97:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2440:
	.size	_ZNK6icu_6716BytesTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE, .-_ZNK6icu_6716BytesTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder5writeEi
	.type	_ZN6icu_6716BytesTrieBuilder5writeEi, @function
_ZN6icu_6716BytesTrieBuilder5writeEi:
.LFB2442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdi), %r14
	movl	52(%rdi), %eax
	testq	%r14, %r14
	je	.L108
	movl	48(%rdi), %r12d
	leal	1(%rax), %r15d
	movq	%rdi, %rbx
	movl	%esi, %r13d
	cmpl	%r12d, %r15d
	jg	.L111
.L110:
	subl	%r15d, %r12d
	movl	%r15d, 52(%rbx)
	movslq	%r12d, %r12
	movb	%r13b, (%r14,%r12)
	movl	52(%rbx), %eax
.L108:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	addl	%r12d, %r12d
	cmpl	%r12d, %r15d
	jge	.L111
	movslq	%r12d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L118
	movslq	52(%rbx), %rdx
	movl	48(%rbx), %esi
	movl	%r12d, %edi
	movq	40(%rbx), %r8
	subl	%edx, %edi
	subl	%edx, %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	movq	%r8, -56(%rbp)
	addq	%r8, %rsi
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	%r14, 40(%rbx)
	movl	%r12d, 48(%rbx)
	jmp	.L110
.L118:
	movq	40(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	$0, 40(%rbx)
	movl	52(%rbx), %eax
	movl	$0, 48(%rbx)
	jmp	.L108
	.cfi_endproc
.LFE2442:
	.size	_ZN6icu_6716BytesTrieBuilder5writeEi, .-_ZN6icu_6716BytesTrieBuilder5writeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder17writeElementUnitsEiii
	.type	_ZN6icu_6716BytesTrieBuilder17writeElementUnitsEiii, @function
_ZN6icu_6716BytesTrieBuilder17writeElementUnitsEiii:
.LFB2444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	movq	40(%rdi), %r8
	movl	%edx, -52(%rbp)
	movl	(%rax,%rsi,8), %eax
	movl	%eax, %r13d
	leal	1(%rax), %esi
	notl	%r13d
	addl	$2, %r13d
	testl	%eax, %eax
	movl	52(%rdi), %eax
	cmovns	%esi, %r13d
	leal	(%rcx,%rax), %r14d
	testq	%r8, %r8
	je	.L119
	movq	16(%rdi), %rax
	movl	48(%rdi), %r12d
	movq	%rdi, %rbx
	movl	%ecx, %r15d
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
	cmpl	%r12d, %r14d
	jle	.L123
	.p2align 4,,10
	.p2align 3
.L124:
	addl	%r12d, %r12d
	cmpl	%r12d, %r14d
	jge	.L124
	movslq	%r12d, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L131
	movslq	52(%rbx), %rdx
	movl	48(%rbx), %esi
	movl	%r12d, %edi
	movq	%rax, -80(%rbp)
	movq	40(%rbx), %r10
	subl	%edx, %edi
	subl	%edx, %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	movq	%r10, -72(%rbp)
	addq	%r10, %rsi
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r10
	movq	%r10, %rdi
	call	uprv_free_67@PLT
	movq	-80(%rbp), %r8
	movl	%r12d, 48(%rbx)
	movq	%r8, 40(%rbx)
.L123:
	movl	%r14d, 52(%rbx)
	subl	%r14d, %r12d
	movslq	-52(%rbp), %r14
	movslq	%r13d, %r13
	movq	-64(%rbp), %rsi
	movslq	%r12d, %r12
	movslq	%r15d, %rdx
	addq	%r14, %r13
	leaq	(%r8,%r12), %rdi
	addq	%r13, %rsi
	call	memcpy@PLT
	movl	52(%rbx), %eax
.L119:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L131:
	.cfi_restore_state
	movq	40(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	$0, 40(%rbx)
	movl	52(%rbx), %eax
	movl	$0, 48(%rbx)
	jmp	.L119
	.cfi_endproc
.LFE2444:
	.size	_ZN6icu_6716BytesTrieBuilder17writeElementUnitsEiii, .-_ZN6icu_6716BytesTrieBuilder17writeElementUnitsEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder17writeValueAndTypeEaii
	.type	_ZN6icu_6716BytesTrieBuilder17writeValueAndTypeEaii, @function
_ZN6icu_6716BytesTrieBuilder17writeValueAndTypeEaii:
.LFB2446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	leaq	_ZN6icu_6716BytesTrieBuilder5writeEi(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L133
	movl	52(%rdi), %eax
	movq	40(%rdi), %r14
	leal	1(%rax), %ecx
	testq	%r14, %r14
	je	.L138
	movl	48(%rdi), %ebx
	cmpl	%ebx, %ecx
	jle	.L135
	.p2align 4,,10
	.p2align 3
.L136:
	addl	%ebx, %ebx
	cmpl	%ebx, %ecx
	jge	.L136
	movslq	%ebx, %rdi
	movl	%r8d, -60(%rbp)
	movl	%ecx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movl	-56(%rbp), %ecx
	movl	-60(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L145
	movslq	52(%r12), %rdx
	movl	48(%r12), %esi
	movl	%ebx, %edi
	movl	%r8d, -64(%rbp)
	movq	40(%r12), %r9
	movl	%ecx, -60(%rbp)
	subl	%edx, %edi
	subl	%edx, %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	movq	%r9, -56(%rbp)
	addq	%r9, %rsi
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	-56(%rbp), %r9
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	movq	%r14, 40(%r12)
	movl	-64(%rbp), %r8d
	movl	%ebx, 48(%r12)
	movl	-60(%rbp), %ecx
.L135:
	subl	%ecx, %ebx
	movl	%ecx, 52(%r12)
	movslq	%ebx, %rbx
	movb	%r15b, (%r14,%rbx)
	movl	52(%r12), %eax
.L138:
	testb	%r13b, %r13b
	je	.L132
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%edx, %edx
	movl	%r8d, %esi
	movq	136(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movl	%r8d, -56(%rbp)
	movl	%ecx, %esi
	call	*%rax
	movl	-56(%rbp), %r8d
	jmp	.L138
.L145:
	movq	40(%r12), %rdi
	movl	%r8d, -56(%rbp)
	call	uprv_free_67@PLT
	movl	52(%r12), %eax
	movl	-56(%rbp), %r8d
	movq	$0, 40(%r12)
	movl	$0, 48(%r12)
	jmp	.L138
	.cfi_endproc
.LFE2446:
	.size	_ZN6icu_6716BytesTrieBuilder17writeValueAndTypeEaii, .-_ZN6icu_6716BytesTrieBuilder17writeValueAndTypeEaii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNode5writeERNS_17StringTrieBuilderE
	.type	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNode5writeERNS_17StringTrieBuilderE, @function
_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNode5writeERNS_17StringTrieBuilderE:
.LFB2439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movl	24(%rbx), %r14d
	movl	52(%r12), %ecx
	movq	40(%r12), %r15
	addl	%r14d, %ecx
	testq	%r15, %r15
	je	.L147
	movq	40(%rbx), %rax
	movl	48(%r12), %r13d
	movq	%rax, -56(%rbp)
	cmpl	%r13d, %ecx
	jle	.L148
	.p2align 4,,10
	.p2align 3
.L149:
	addl	%r13d, %r13d
	cmpl	%r13d, %ecx
	jge	.L149
	movslq	%r13d, %rdi
	movl	%ecx, -64(%rbp)
	call	uprv_malloc_67@PLT
	movl	-64(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L168
	movslq	52(%r12), %rdx
	movl	48(%r12), %esi
	movl	%r13d, %edi
	movl	%ecx, -68(%rbp)
	movq	40(%r12), %r9
	subl	%edx, %edi
	subl	%edx, %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	movq	%r9, -64(%rbp)
	addq	%r9, %rsi
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	movq	%r15, 40(%r12)
	movl	-68(%rbp), %ecx
	movl	%r13d, 48(%r12)
.L148:
	subl	%ecx, %r13d
	movl	%ecx, 52(%r12)
	movq	-56(%rbp), %rsi
	movslq	%r14d, %rdx
	movslq	%r13d, %r13
	leaq	(%r15,%r13), %rdi
	call	memcpy@PLT
.L147:
	movq	(%r12), %rax
	leaq	_ZNK6icu_6716BytesTrieBuilder17getMinLinearMatchEv(%rip), %rcx
	movq	96(%rax), %rdx
	movq	120(%rax), %r13
	movl	$16, %eax
	cmpq	%rcx, %rdx
	jne	.L169
.L151:
	addl	24(%rbx), %eax
	leal	-1(%rax), %r14d
	leaq	_ZN6icu_6716BytesTrieBuilder5writeEi(%rip), %rax
	cmpq	%rax, %r13
	jne	.L152
	movl	52(%r12), %eax
	movq	40(%r12), %r15
	leal	1(%rax), %ecx
	testq	%r15, %r15
	je	.L157
	movl	48(%r12), %r13d
	cmpl	%r13d, %ecx
	jle	.L154
	.p2align 4,,10
	.p2align 3
.L155:
	addl	%r13d, %r13d
	cmpl	%r13d, %ecx
	jge	.L155
	movslq	%r13d, %rdi
	movl	%ecx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movl	-56(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L170
	movslq	52(%r12), %rdx
	movl	48(%r12), %esi
	movl	%r13d, %edi
	movl	%ecx, -64(%rbp)
	movq	40(%r12), %r8
	subl	%edx, %edi
	subl	%edx, %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	movq	%r8, -56(%rbp)
	addq	%r8, %rsi
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	-56(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	%r15, 40(%r12)
	movl	-64(%rbp), %ecx
	movl	%r13d, 48(%r12)
.L154:
	subl	%ecx, %r13d
	movl	%ecx, 52(%r12)
	movslq	%r13d, %r13
	movb	%r14b, (%r15,%r13)
	movl	52(%r12), %eax
.L157:
	movl	%eax, 12(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*%r13
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L151
.L168:
	movq	40(%r12), %rdi
	call	uprv_free_67@PLT
	movq	$0, 40(%r12)
	movl	$0, 48(%r12)
	jmp	.L147
.L170:
	movq	40(%r12), %rdi
	call	uprv_free_67@PLT
	movl	52(%r12), %eax
	movq	$0, 40(%r12)
	movl	$0, 48(%r12)
	jmp	.L157
	.cfi_endproc
.LFE2439:
	.size	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNode5writeERNS_17StringTrieBuilderE, .-_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNode5writeERNS_17StringTrieBuilderE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder12writeDeltaToEi
	.type	_ZN6icu_6716BytesTrieBuilder12writeDeltaToEi, @function
_ZN6icu_6716BytesTrieBuilder12writeDeltaToEi:
.LFB2447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	52(%rdi), %eax
	movl	%eax, %r13d
	subl	%esi, %r13d
	cmpl	$191, %r13d
	jg	.L172
	movq	(%rdi), %rdx
	leaq	_ZN6icu_6716BytesTrieBuilder5writeEi(%rip), %rcx
	movq	120(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L173
	movq	40(%rdi), %r15
	leal	1(%rax), %r14d
	testq	%r15, %r15
	je	.L171
	movl	48(%rdi), %ebx
	cmpl	%ebx, %r14d
	jle	.L175
	.p2align 4,,10
	.p2align 3
.L176:
	addl	%ebx, %ebx
	cmpl	%ebx, %r14d
	jge	.L176
	movslq	%ebx, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L205
	movslq	52(%r12), %rdx
	movl	48(%r12), %esi
	movl	%ebx, %edi
	movq	40(%r12), %r8
	subl	%edx, %edi
	subl	%edx, %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	movq	%r8, -72(%rbp)
	addq	%r8, %rsi
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	%r15, 40(%r12)
	movl	%ebx, 48(%r12)
.L175:
	subl	%r14d, %ebx
	movl	%r14d, 52(%r12)
	movslq	%ebx, %rbx
	movb	%r13b, (%r15,%rbx)
	movl	52(%r12), %eax
.L171:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L206
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movl	%r13d, %ecx
	sarl	$8, %ecx
	cmpl	$12287, %r13d
	jg	.L180
	subl	$64, %ecx
	movl	$2, %r15d
	movl	$1, %edx
	movb	%cl, -61(%rbp)
.L181:
	movb	%r13b, -61(%rbp,%rdx)
	movq	40(%r12), %r13
	leal	(%rax,%r15), %r14d
	testq	%r13, %r13
	je	.L171
	movl	48(%r12), %ebx
	cmpl	%ebx, %r14d
	jg	.L187
.L186:
	subl	%r14d, %ebx
	movl	%r14d, 52(%r12)
	leaq	-61(%rbp), %rsi
	movslq	%ebx, %rbx
	addq	%r13, %rbx
	testl	%r15d, %r15d
	je	.L190
	xorl	%eax, %eax
.L189:
	movl	%eax, %edx
	addl	$1, %eax
	movzbl	(%rsi,%rdx), %ecx
	movb	%cl, (%rbx,%rdx)
	cmpl	%r15d, %eax
	jb	.L189
.L190:
	movl	52(%r12), %eax
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L180:
	cmpl	$917503, %r13d
	jle	.L207
	cmpl	$16777215, %r13d
	jg	.L184
	movb	$-2, -61(%rbp)
	movl	$4, %r15d
	movl	$3, %edx
.L183:
	movb	%cl, -60(%rbp)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L187:
	addl	%ebx, %ebx
	cmpl	%ebx, %r14d
	jge	.L187
	movslq	%ebx, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L205
	movslq	52(%r12), %rdx
	movl	48(%r12), %esi
	movl	%ebx, %edi
	movq	40(%r12), %r8
	subl	%edx, %edi
	subl	%edx, %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	movq	%r8, -72(%rbp)
	addq	%r8, %rsi
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	%r13, 40(%r12)
	movl	%ebx, 48(%r12)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L207:
	movl	%r13d, %edx
	movl	$3, %r15d
	sarl	$16, %edx
	subl	$16, %edx
	movb	%dl, -61(%rbp)
	movl	$2, %edx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$40, %rsp
	movl	%r13d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movb	$-1, -61(%rbp)
	movl	$5, %r15d
	movl	$4, %edx
	jmp	.L183
.L206:
	call	__stack_chk_fail@PLT
.L205:
	movq	40(%r12), %rdi
	call	uprv_free_67@PLT
	movl	52(%r12), %eax
	movq	$0, 40(%r12)
	movl	$0, 48(%r12)
	jmp	.L171
	.cfi_endproc
.LFE2447:
	.size	_ZN6icu_6716BytesTrieBuilder12writeDeltaToEi, .-_ZN6icu_6716BytesTrieBuilder12writeDeltaToEi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2684:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2684:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2687:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L221
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L209
	cmpb	$0, 12(%rbx)
	jne	.L222
.L213:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L209:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L213
	.cfi_endproc
.LFE2687:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2690:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L225
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2690:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2693:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L228
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2693:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L234
.L230:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L235
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L235:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2695:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2696:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2696:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2697:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2697:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2698:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2698:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2699:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2699:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2700:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2700:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2701:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L251
	testl	%edx, %edx
	jle	.L251
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L254
.L243:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L243
	.cfi_endproc
.LFE2701:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L258
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L258
	testl	%r12d, %r12d
	jg	.L265
	cmpb	$0, 12(%rbx)
	jne	.L266
.L260:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L260
.L266:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L258:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2702:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L268
	movq	(%rdi), %r8
.L269:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L272
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L272
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L272:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2703:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2704:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L279
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2704:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2705:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2705:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2706:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2706:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2707:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2707:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2709:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2709:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2711:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2711:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieElement5setToENS_11StringPieceEiRNS_10CharStringER10UErrorCode
	.type	_ZN6icu_6716BytesTrieElement5setToENS_11StringPieceEiRNS_10CharStringER10UErrorCode, @function
_ZN6icu_6716BytesTrieElement5setToENS_11StringPieceEiRNS_10CharStringER10UErrorCode:
.LFB2409:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L290
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	cmpl	$65535, %edx
	jg	.L293
	movl	56(%r8), %eax
	movq	%rdi, %r14
	movq	%rsi, %r15
	movq	%r8, %r13
	movl	%eax, -52(%rbp)
	cmpl	$255, %edx
	jg	.L294
.L288:
	movsbl	%bl, %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	%ecx, -56(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-56(%rbp), %ecx
	movl	-52(%rbp), %eax
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%ecx, 4(%r14)
	movq	%r12, %rcx
	movl	%eax, (%r14)
	addq	$24, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L290:
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$8, (%r9)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	movsbl	%dh, %esi
	movq	%r8, %rdi
	movq	%r9, %rdx
	movl	%ecx, -56(%rbp)
	notl	-52(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-56(%rbp), %ecx
	jmp	.L288
	.cfi_endproc
.LFE2409:
	.size	_ZN6icu_6716BytesTrieElement5setToENS_11StringPieceEiRNS_10CharStringER10UErrorCode, .-_ZN6icu_6716BytesTrieElement5setToENS_11StringPieceEiRNS_10CharStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716BytesTrieElement15compareStringToERKS0_RKNS_10CharStringE
	.type	_ZNK6icu_6716BytesTrieElement15compareStringToERKS0_RKNS_10CharStringE, @function
_ZNK6icu_6716BytesTrieElement15compareStringToERKS0_RKNS_10CharStringE:
.LFB2410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movslq	(%rdi), %rax
	movq	(%rdx), %rsi
	testl	%eax, %eax
	js	.L296
	leal	1(%rax), %edi
	movzbl	(%rsi,%rax), %edx
	movslq	(%rcx), %rax
	movslq	%edi, %rdi
	addq	%rsi, %rdi
	testl	%eax, %eax
	js	.L298
.L303:
	movzbl	(%rsi,%rax), %r8d
	leal	1(%rax), %ecx
.L299:
	movl	%edx, %ebx
	movslq	%ecx, %rax
	subl	%r8d, %ebx
	addq	%rax, %rsi
	testl	%ebx, %ebx
	cmovg	%r8d, %edx
	movslq	%edx, %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	cmove	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movl	%eax, %edi
	negl	%eax
	notl	%edi
	cltq
	movslq	%edi, %rdx
	movzbl	(%rsi,%rax), %eax
	addl	$2, %edi
	movzbl	(%rsi,%rdx), %edx
	movslq	%edi, %rdi
	addq	%rsi, %rdi
	sall	$8, %edx
	orl	%eax, %edx
	movslq	(%rcx), %rax
	testl	%eax, %eax
	jns	.L303
.L298:
	movl	%eax, %ecx
	negl	%eax
	notl	%ecx
	cltq
	movslq	%ecx, %r8
	movzbl	(%rsi,%rax), %eax
	addl	$2, %ecx
	movzbl	(%rsi,%r8), %r8d
	sall	$8, %r8d
	orl	%eax, %r8d
	jmp	.L299
	.cfi_endproc
.LFE2410:
	.size	_ZNK6icu_6716BytesTrieElement15compareStringToERKS0_RKNS_10CharStringE, .-_ZNK6icu_6716BytesTrieElement15compareStringToERKS0_RKNS_10CharStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilderC2ER10UErrorCode
	.type	_ZN6icu_6716BytesTrieBuilderC2ER10UErrorCode, @function
_ZN6icu_6716BytesTrieBuilderC2ER10UErrorCode:
.LFB2412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN6icu_6717StringTrieBuilderC2Ev@PLT
	movl	(%r12), %ecx
	leaq	16+_ZTVN6icu_6716BytesTrieBuilderE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movups	%xmm0, 16(%rbx)
	testl	%ecx, %ecx
	jle	.L308
.L304:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L306
	leaq	13(%rax), %rdx
	movl	$0, 56(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movl	$40, 8(%rax)
	movw	%dx, 12(%rax)
	movq	%rax, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L306:
	.cfi_restore_state
	movq	$0, 16(%rbx)
	movl	$7, (%r12)
	jmp	.L304
	.cfi_endproc
.LFE2412:
	.size	_ZN6icu_6716BytesTrieBuilderC2ER10UErrorCode, .-_ZN6icu_6716BytesTrieBuilderC2ER10UErrorCode
	.globl	_ZN6icu_6716BytesTrieBuilderC1ER10UErrorCode
	.set	_ZN6icu_6716BytesTrieBuilderC1ER10UErrorCode,_ZN6icu_6716BytesTrieBuilderC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder3addENS_11StringPieceEiR10UErrorCode
	.type	_ZN6icu_6716BytesTrieBuilder3addENS_11StringPieceEiR10UErrorCode, @function
_ZN6icu_6716BytesTrieBuilder3addENS_11StringPieceEiR10UErrorCode:
.LFB2418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	(%r8), %edi
	testl	%edi, %edi
	jg	.L310
	movl	52(%r12), %eax
	movq	%r8, %r13
	testl	%eax, %eax
	jg	.L328
	movslq	36(%r12), %r8
	movq	%rsi, %r14
	movq	%rdx, %rbx
	cmpl	32(%r12), %r8d
	jne	.L329
	testl	%r8d, %r8d
	je	.L322
	leal	0(,%r8,4), %r15d
	movabsq	$1152921504606846975, %rax
	movslq	%r15d, %rdi
	cmpq	%rax, %rdi
	jbe	.L330
	movq	$-1, %rdi
.L315:
	movl	%ecx, -56(%rbp)
	call	_ZN6icu_677UMemorynaEm@PLT
	movl	-56(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L331
	movslq	36(%r12), %rdx
	movq	24(%r12), %rdi
	testl	%edx, %edx
	jle	.L317
	movq	%rdi, %rsi
	salq	$3, %rdx
	movq	%rax, %rdi
	movl	%ecx, -56(%rbp)
	call	memcpy@PLT
	movq	24(%r12), %rdi
	movl	-56(%rbp), %ecx
	movq	%rax, %r9
.L317:
	testq	%rdi, %rdi
	je	.L318
	movl	%ecx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN6icu_677UMemorydaEPv@PLT
	movl	-64(%rbp), %ecx
	movq	-56(%rbp), %r9
.L318:
	movslq	36(%r12), %r8
	movl	0(%r13), %eax
	movq	%r9, 24(%r12)
	movl	%r15d, 32(%r12)
	leal	1(%r8), %edx
	movl	%edx, 36(%r12)
	testl	%eax, %eax
	jle	.L313
.L310:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movl	$30, (%r8)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L329:
	leal	1(%r8), %eax
	movq	24(%r12), %r9
	movl	%eax, 36(%r12)
.L313:
	cmpl	$65535, %ebx
	jg	.L332
	movq	16(%r12), %rdi
	movl	56(%rdi), %r15d
	cmpl	$255, %ebx
	jg	.L333
.L321:
	leaq	(%r9,%r8,8), %r8
	movsbl	%bl, %esi
	movq	%r13, %rdx
	movl	%ecx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-64(%rbp), %r8
	movl	-72(%rbp), %ecx
	movl	%ebx, %edx
	movq	-56(%rbp), %rdi
	movq	%r14, %rsi
	movl	%ecx, 4(%r8)
	movq	%r13, %rcx
	movl	%r15d, (%r8)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L322:
	movl	$8192, %edi
	movl	$1024, %r15d
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L332:
	movl	$8, 0(%r13)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L333:
	movsbl	%bh, %esi
	movq	%r13, %rdx
	movl	%ecx, -76(%rbp)
	notl	%r15d
	movq	%r9, -72(%rbp)
	movl	%r8d, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movl	-76(%rbp), %ecx
	movq	-72(%rbp), %r9
	movslq	-64(%rbp), %r8
	movq	-56(%rbp), %rdi
	jmp	.L321
.L331:
	movl	$7, 0(%r13)
	jmp	.L310
.L330:
	salq	$3, %rdi
	jmp	.L315
	.cfi_endproc
.LFE2418:
	.size	_ZN6icu_6716BytesTrieBuilder3addENS_11StringPieceEiR10UErrorCode, .-_ZN6icu_6716BytesTrieBuilder3addENS_11StringPieceEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode
	.type	_ZN6icu_6716BytesTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode, @function
_ZN6icu_6716BytesTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode:
.LFB2420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L335
	cmpq	$0, 40(%rdi)
	movl	52(%rdi), %eax
	movq	%rdi, %rbx
	movl	%esi, %r13d
	movq	%rdx, %r12
	je	.L336
	testl	%eax, %eax
	jg	.L353
.L336:
	testl	%eax, %eax
	jne	.L339
	movl	36(%rbx), %esi
	testl	%esi, %esi
	jne	.L340
	movl	$8, (%r12)
	.p2align 4,,10
	.p2align 3
.L335:
	xorl	%eax, %eax
.L334:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L358
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movq	16(%rbx), %rcx
.L349:
	movl	$0, 52(%rbx)
	movl	$1024, %r14d
	cmpl	$1024, 56(%rcx)
	cmovge	56(%rcx), %r14d
	cmpl	48(%rbx), %r14d
	jle	.L350
	movq	40(%rbx), %rdi
	call	uprv_free_67@PLT
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L359
	movl	%r14d, 48(%rbx)
.L350:
	movl	36(%rbx), %edx
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode@PLT
	cmpq	$0, 40(%rbx)
	je	.L360
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L335
.L353:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L361
	movq	40(%rbx), %rcx
	movl	48(%rbx), %edx
	subl	52(%rbx), %edx
	movl	$-1, 24(%rax)
	movslq	%edx, %rdx
	movq	%rcx, (%rax)
	addq	%rcx, %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 40(%rbx)
	movl	$0, 48(%rbx)
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L340:
	subq	$8, %rsp
	movq	24(%rbx), %rdi
	movq	16(%rbx), %r8
	xorl	%r9d, %r9d
	pushq	%r12
	leaq	compareElementStrings(%rip), %rcx
	movl	$8, %edx
	call	uprv_sortArray_67@PLT
	movl	(%r12), %esi
	popq	%rdx
	popq	%rcx
	testl	%esi, %esi
	jg	.L335
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rcx
	movslq	(%rsi), %rax
	movq	(%rcx), %rdx
	testl	%eax, %eax
	js	.L342
	movzbl	(%rdx,%rax), %r8d
	leal	1(%rax), %edi
.L343:
	movslq	%edi, %rax
	movl	%r8d, -88(%rbp)
	addq	%rax, %rdx
	cmpl	$1, 36(%rbx)
	movq	%rdx, -96(%rbp)
	jle	.L349
	leaq	-96(%rbp), %rdi
	xorl	%eax, %eax
	leaq	-80(%rbp), %r15
	movq	%rdi, -104(%rbp)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L363:
	movzbl	(%rcx,%rax), %edi
	leal	1(%rax), %esi
.L346:
	movslq	%esi, %rax
	movl	%edi, -72(%rbp)
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	addq	%rax, %rcx
	movq	%rcx, -80(%rbp)
	call	_ZN6icu_67eqERKNS_11StringPieceES2_@PLT
	testb	%al, %al
	jne	.L362
	movq	-80(%rbp), %rax
	movq	16(%rbx), %rcx
	leal	1(%r14), %edx
	movq	%rax, -96(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, -88(%rbp)
	movq	%r14, %rax
	cmpl	%edx, 36(%rbx)
	jle	.L349
	movq	24(%rbx), %rsi
.L348:
	leaq	1(%rax), %r14
	movq	(%rcx), %rcx
	movslq	(%rsi,%r14,8), %rax
	testl	%eax, %eax
	jns	.L363
	movl	%eax, %esi
	negl	%eax
	notl	%esi
	cltq
	movslq	%esi, %rdi
	movzbl	(%rcx,%rax), %eax
	addl	$2, %esi
	movzbl	(%rcx,%rdi), %edi
	sall	$8, %edi
	orl	%eax, %edi
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L360:
	movl	$7, (%r12)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L362:
	movl	$1, (%r12)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L342:
	movl	%eax, %edi
	negl	%eax
	notl	%edi
	cltq
	movslq	%edi, %r8
	movzbl	(%rdx,%rax), %eax
	addl	$2, %edi
	movzbl	(%rdx,%r8), %r8d
	sall	$8, %r8d
	orl	%eax, %r8d
	jmp	.L343
.L358:
	call	__stack_chk_fail@PLT
.L361:
	movl	$7, (%r12)
	jmp	.L334
.L359:
	movl	$7, (%r12)
	movl	$0, 48(%rbx)
	jmp	.L335
	.cfi_endproc
.LFE2420:
	.size	_ZN6icu_6716BytesTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode, .-_ZN6icu_6716BytesTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder16buildStringPieceE22UStringTrieBuildOptionR10UErrorCode
	.type	_ZN6icu_6716BytesTrieBuilder16buildStringPieceE22UStringTrieBuildOptionR10UErrorCode, @function
_ZN6icu_6716BytesTrieBuilder16buildStringPieceE22UStringTrieBuildOptionR10UErrorCode:
.LFB2421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L365
	movq	40(%rdi), %rax
	movq	%rdx, %r12
	movq	%rdi, %rbx
	movl	52(%rdi), %edx
	movl	%esi, %r13d
	testq	%rax, %rax
	je	.L366
	testl	%edx, %edx
	jg	.L367
.L366:
	testl	%edx, %edx
	jne	.L368
	movl	36(%rbx), %esi
	testl	%esi, %esi
	jne	.L369
	movl	$8, (%r12)
	.p2align 4,,10
	.p2align 3
.L365:
	xorl	%edx, %edx
	xorl	%eax, %eax
.L382:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L389
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	16(%rbx), %rcx
.L378:
	movl	$0, 52(%rbx)
	movl	$1024, %r14d
	cmpl	$1024, 56(%rcx)
	cmovge	56(%rcx), %r14d
	cmpl	48(%rbx), %r14d
	jle	.L379
	movq	40(%rbx), %rdi
	call	uprv_free_67@PLT
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L390
	movl	%r14d, 48(%rbx)
.L379:
	movl	36(%rbx), %edx
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode@PLT
	cmpq	$0, 40(%rbx)
	je	.L391
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L365
	movl	52(%rbx), %edx
	movq	40(%rbx), %rax
.L367:
	movl	48(%rbx), %ecx
	subl	%edx, %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L369:
	subq	$8, %rsp
	movq	24(%rbx), %rdi
	movq	16(%rbx), %r8
	xorl	%r9d, %r9d
	pushq	%r12
	leaq	compareElementStrings(%rip), %rcx
	movl	$8, %edx
	call	uprv_sortArray_67@PLT
	movl	(%r12), %esi
	popq	%rdx
	popq	%rcx
	testl	%esi, %esi
	jg	.L365
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rcx
	movslq	(%rsi), %rax
	movq	(%rcx), %rdx
	testl	%eax, %eax
	js	.L371
	movzbl	(%rdx,%rax), %r8d
	leal	1(%rax), %edi
.L372:
	movslq	%edi, %rax
	movl	%r8d, -88(%rbp)
	addq	%rax, %rdx
	cmpl	$1, 36(%rbx)
	movq	%rdx, -96(%rbp)
	jle	.L378
	leaq	-96(%rbp), %rdi
	xorl	%eax, %eax
	leaq	-80(%rbp), %r15
	movq	%rdi, -104(%rbp)
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L393:
	movzbl	(%rcx,%rax), %edi
	leal	1(%rax), %esi
.L375:
	movslq	%esi, %rax
	movl	%edi, -72(%rbp)
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	addq	%rax, %rcx
	movq	%rcx, -80(%rbp)
	call	_ZN6icu_67eqERKNS_11StringPieceES2_@PLT
	testb	%al, %al
	jne	.L392
	movq	-80(%rbp), %rax
	movq	16(%rbx), %rcx
	leal	1(%r14), %edx
	movq	%rax, -96(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, -88(%rbp)
	movq	%r14, %rax
	cmpl	%edx, 36(%rbx)
	jle	.L378
	movq	24(%rbx), %rsi
.L377:
	leaq	1(%rax), %r14
	movq	(%rcx), %rcx
	movslq	(%rsi,%r14,8), %rax
	testl	%eax, %eax
	jns	.L393
	movl	%eax, %esi
	negl	%eax
	notl	%esi
	cltq
	movslq	%esi, %rdi
	movzbl	(%rcx,%rax), %eax
	addl	$2, %esi
	movzbl	(%rcx,%rdi), %edi
	sall	$8, %edi
	orl	%eax, %edi
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L391:
	movl	$7, (%r12)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L392:
	movl	$1, (%r12)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L371:
	movl	%eax, %edi
	negl	%eax
	notl	%edi
	cltq
	movslq	%edi, %r8
	movzbl	(%rdx,%rax), %eax
	addl	$2, %edi
	movzbl	(%rdx,%r8), %r8d
	sall	$8, %r8d
	orl	%eax, %r8d
	jmp	.L372
.L389:
	call	__stack_chk_fail@PLT
.L390:
	movl	$7, (%r12)
	movl	$0, 48(%rbx)
	jmp	.L365
	.cfi_endproc
.LFE2421:
	.size	_ZN6icu_6716BytesTrieBuilder16buildStringPieceE22UStringTrieBuildOptionR10UErrorCode, .-_ZN6icu_6716BytesTrieBuilder16buildStringPieceE22UStringTrieBuildOptionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder10buildBytesE22UStringTrieBuildOptionR10UErrorCode
	.type	_ZN6icu_6716BytesTrieBuilder10buildBytesE22UStringTrieBuildOptionR10UErrorCode, @function
_ZN6icu_6716BytesTrieBuilder10buildBytesE22UStringTrieBuildOptionR10UErrorCode:
.LFB2422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L394
	cmpq	$0, 40(%rdi)
	movl	52(%rdi), %eax
	movq	%rdi, %rbx
	movl	%esi, %r13d
	movq	%rdx, %r12
	je	.L397
	testl	%eax, %eax
	jg	.L394
.L397:
	testl	%eax, %eax
	jne	.L399
	movl	36(%rbx), %esi
	testl	%esi, %esi
	jne	.L400
	movl	$8, (%r12)
	.p2align 4,,10
	.p2align 3
.L394:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L414
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	movq	16(%rbx), %rcx
.L409:
	movl	$0, 52(%rbx)
	movl	$1024, %r14d
	cmpl	$1024, 56(%rcx)
	cmovge	56(%rcx), %r14d
	cmpl	48(%rbx), %r14d
	jle	.L410
	movq	40(%rbx), %rdi
	call	uprv_free_67@PLT
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 40(%rbx)
	testq	%rax, %rax
	je	.L415
	movl	%r14d, 48(%rbx)
.L410:
	movl	36(%rbx), %edx
	movq	%r12, %rcx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode@PLT
	cmpq	$0, 40(%rbx)
	jne	.L394
	movl	$7, (%r12)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L400:
	subq	$8, %rsp
	movq	24(%rbx), %rdi
	movq	16(%rbx), %r8
	xorl	%r9d, %r9d
	pushq	%r12
	leaq	compareElementStrings(%rip), %rcx
	movl	$8, %edx
	call	uprv_sortArray_67@PLT
	movl	(%r12), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L394
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rcx
	movslq	(%rsi), %rax
	movq	(%rcx), %rdx
	testl	%eax, %eax
	js	.L402
	movzbl	(%rdx,%rax), %r8d
	leal	1(%rax), %edi
.L403:
	movslq	%edi, %rax
	movl	%r8d, -88(%rbp)
	addq	%rax, %rdx
	cmpl	$1, 36(%rbx)
	movq	%rdx, -96(%rbp)
	jle	.L409
	leaq	-96(%rbp), %rdx
	xorl	%eax, %eax
	leaq	-80(%rbp), %r15
	movq	%rdx, -104(%rbp)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L417:
	movzbl	(%rcx,%rax), %edi
	leal	1(%rax), %esi
.L406:
	movslq	%esi, %rax
	movl	%edi, -72(%rbp)
	movq	-104(%rbp), %rdi
	movq	%r15, %rsi
	addq	%rax, %rcx
	movq	%rcx, -80(%rbp)
	call	_ZN6icu_67eqERKNS_11StringPieceES2_@PLT
	testb	%al, %al
	jne	.L416
	movq	-80(%rbp), %rax
	movq	16(%rbx), %rcx
	leal	1(%r14), %edx
	movq	%rax, -96(%rbp)
	movl	-72(%rbp), %eax
	movl	%eax, -88(%rbp)
	movq	%r14, %rax
	cmpl	%edx, 36(%rbx)
	jle	.L409
	movq	24(%rbx), %rsi
.L408:
	leaq	1(%rax), %r14
	movq	(%rcx), %rcx
	movslq	(%rsi,%r14,8), %rax
	testl	%eax, %eax
	jns	.L417
	movl	%eax, %esi
	negl	%eax
	notl	%esi
	cltq
	movslq	%esi, %rdi
	movzbl	(%rcx,%rax), %eax
	addl	$2, %esi
	movzbl	(%rcx,%rdi), %edi
	sall	$8, %edi
	orl	%eax, %edi
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L416:
	movl	$1, (%r12)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L402:
	movl	%eax, %edi
	negl	%eax
	notl	%edi
	cltq
	movslq	%edi, %r8
	movzbl	(%rdx,%rax), %eax
	addl	$2, %edi
	movzbl	(%rdx,%r8), %r8d
	sall	$8, %r8d
	orl	%eax, %r8d
	jmp	.L403
.L414:
	call	__stack_chk_fail@PLT
.L415:
	movl	$7, (%r12)
	movl	$0, 48(%rbx)
	jmp	.L394
	.cfi_endproc
.LFE2422:
	.size	_ZN6icu_6716BytesTrieBuilder10buildBytesE22UStringTrieBuildOptionR10UErrorCode, .-_ZN6icu_6716BytesTrieBuilder10buildBytesE22UStringTrieBuildOptionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder5clearEv
	.type	_ZN6icu_6716BytesTrieBuilder5clearEv, @function
_ZN6icu_6716BytesTrieBuilder5clearEv:
.LFB2423:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	%rdi, %rax
	movl	$0, 56(%rdx)
	movq	(%rdx), %rdx
	movb	$0, (%rdx)
	movl	$0, 36(%rdi)
	movl	$0, 52(%rdi)
	ret
	.cfi_endproc
.LFE2423:
	.size	_ZN6icu_6716BytesTrieBuilder5clearEv, .-_ZN6icu_6716BytesTrieBuilder5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeC2EPKciPNS_17StringTrieBuilder4NodeE
	.type	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeC2EPKciPNS_17StringTrieBuilder4NodeE, @function
_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeC2EPKciPNS_17StringTrieBuilder4NodeE:
.LFB2436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	(%rdx,%rdx,8), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	%edx, %esi
	leal	(%rdx,%rax,4), %edx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L420
	movl	8(%rcx), %eax
.L420:
	leal	298634171(%rax,%rdx), %r12d
	movb	$0, 16(%rbx)
	leaq	16+_ZTVN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE(%rip), %rax
	movq	%rcx, %xmm0
	movl	%r12d, 8(%rbx)
	movq	%rdi, %xmm1
	movl	$0, 12(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movl	$0, 20(%rbx)
	movl	%esi, 24(%rbx)
	movq	%rax, (%rbx)
	movups	%xmm0, 32(%rbx)
	call	ustr_hashCharsN_67@PLT
	movl	%eax, %r8d
	leal	(%r12,%r12,8), %eax
	leal	(%r12,%rax,4), %eax
	addl	%r8d, %eax
	movl	%eax, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2436:
	.size	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeC2EPKciPNS_17StringTrieBuilder4NodeE, .-_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeC2EPKciPNS_17StringTrieBuilder4NodeE
	.globl	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeC1EPKciPNS_17StringTrieBuilder4NodeE
	.set	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeC1EPKciPNS_17StringTrieBuilder4NodeE,_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeC2EPKciPNS_17StringTrieBuilder4NodeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder14ensureCapacityEi
	.type	_ZN6icu_6716BytesTrieBuilder14ensureCapacityEi, @function
_ZN6icu_6716BytesTrieBuilder14ensureCapacityEi:
.LFB2441:
	.cfi_startproc
	endbr64
	cmpq	$0, 40(%rdi)
	je	.L429
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	48(%rdi), %ebx
	cmpl	%esi, %ebx
	jge	.L425
	.p2align 4,,10
	.p2align 3
.L427:
	addl	%ebx, %ebx
	cmpl	%ebx, %esi
	jge	.L427
	movslq	%ebx, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L436
	movslq	52(%r12), %rdx
	movl	48(%r12), %esi
	movl	%ebx, %edi
	movq	40(%r12), %r14
	subl	%edx, %edi
	subl	%edx, %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	addq	%rax, %rdi
	addq	%r14, %rsi
	call	memcpy@PLT
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	movq	%r13, 40(%r12)
	movl	$1, %eax
	movl	%ebx, 48(%r12)
.L425:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
.L436:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	40(%r12), %rdi
	call	uprv_free_67@PLT
	xorl	%eax, %eax
	movq	$0, 40(%r12)
	movl	$0, 48(%r12)
	jmp	.L425
	.cfi_endproc
.LFE2441:
	.size	_ZN6icu_6716BytesTrieBuilder14ensureCapacityEi, .-_ZN6icu_6716BytesTrieBuilder14ensureCapacityEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder5writeEPKci
	.type	_ZN6icu_6716BytesTrieBuilder5writeEPKci, @function
_ZN6icu_6716BytesTrieBuilder5writeEPKci:
.LFB2443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	52(%rdi), %eax
	movq	40(%rdi), %r14
	movq	%rsi, -56(%rbp)
	leal	(%rax,%rdx), %r13d
	testq	%r14, %r14
	je	.L437
	movl	48(%rdi), %r12d
	movq	%rdi, %rbx
	movl	%edx, %r15d
	cmpl	%r12d, %r13d
	jle	.L439
	.p2align 4,,10
	.p2align 3
.L440:
	addl	%r12d, %r12d
	cmpl	%r12d, %r13d
	jge	.L440
	movslq	%r12d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L447
	movslq	52(%rbx), %rdx
	movl	48(%rbx), %esi
	movl	%r12d, %edi
	movq	40(%rbx), %r8
	subl	%edx, %edi
	subl	%edx, %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	movq	%r8, -64(%rbp)
	addq	%r8, %rsi
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	%r14, 40(%rbx)
	movl	%r12d, 48(%rbx)
.L439:
	movl	%r13d, 52(%rbx)
	subl	%r13d, %r12d
	movq	-56(%rbp), %rsi
	movslq	%r15d, %rdx
	movslq	%r12d, %r12
	leaq	(%r14,%r12), %rdi
	call	memcpy@PLT
	movl	52(%rbx), %eax
.L437:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L447:
	.cfi_restore_state
	movq	40(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	$0, 40(%rbx)
	movl	52(%rbx), %eax
	movl	$0, 48(%rbx)
	jmp	.L437
	.cfi_endproc
.LFE2443:
	.size	_ZN6icu_6716BytesTrieBuilder5writeEPKci, .-_ZN6icu_6716BytesTrieBuilder5writeEPKci
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716BytesTrieBuilder18writeValueAndFinalEia
	.type	_ZN6icu_6716BytesTrieBuilder18writeValueAndFinalEia, @function
_ZN6icu_6716BytesTrieBuilder18writeValueAndFinalEia:
.LFB2445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$64, %esi
	ja	.L449
	movq	(%rdi), %rax
	movsbl	%dl, %edx
	leal	32(%rsi,%rsi), %r13d
	orl	%edx, %r13d
	leaq	_ZN6icu_6716BytesTrieBuilder5writeEi(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L450
	movl	52(%rdi), %eax
	movq	40(%rdi), %r14
	leal	1(%rax), %r15d
	testq	%r14, %r14
	je	.L448
	movl	48(%rdi), %ebx
	cmpl	%ebx, %r15d
	jle	.L452
	.p2align 4,,10
	.p2align 3
.L453:
	addl	%ebx, %ebx
	cmpl	%ebx, %r15d
	jge	.L453
	movslq	%ebx, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L468
	movslq	52(%r12), %rdx
	movl	48(%r12), %esi
	movl	%ebx, %edi
	movq	40(%r12), %r8
	subl	%edx, %edi
	subl	%edx, %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	movq	%r8, -72(%rbp)
	addq	%r8, %rsi
	addq	%rax, %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	movq	%r14, 40(%r12)
	movl	%ebx, 48(%r12)
.L452:
	movl	%ebx, %eax
	movl	%r15d, 52(%r12)
	subl	%r15d, %eax
	cltq
	movb	%r13b, (%r14,%rax)
	movl	52(%r12), %eax
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L449:
	cmpl	$16777215, %esi
	jbe	.L456
	bswap	%esi
	movl	%esi, -60(%rbp)
	movl	$-2, %eax
	movl	$5, %r8d
.L457:
	orl	%eax, %edx
	leaq	-61(%rbp), %rsi
	movq	%r12, %rdi
	movb	%dl, -61(%rbp)
	movl	%r8d, %edx
	call	_ZN6icu_6716BytesTrieBuilder5writeEPKci
.L448:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L469
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	movl	%esi, %edi
	sarl	$8, %edi
	cmpl	$6911, %esi
	jle	.L470
	movl	%esi, %eax
	sarl	$16, %eax
	cmpl	$1179647, %esi
	jg	.L460
	leal	-40(%rax,%rax), %eax
	movl	$3, %r8d
	movl	$2, %ecx
	movl	$1, %r9d
.L461:
	movb	%dil, -61(%rbp,%r9)
.L459:
	movb	%sil, -61(%rbp,%rcx)
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L470:
	leal	-94(%rdi,%rdi), %eax
	movl	$2, %r8d
	movl	$1, %ecx
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L450:
	movl	%r13d, %esi
	call	*%rax
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L460:
	movb	%al, -60(%rbp)
	movl	$3, %ecx
	movl	$-4, %eax
	movl	$4, %r8d
	movl	$2, %r9d
	jmp	.L461
.L469:
	call	__stack_chk_fail@PLT
.L468:
	movq	40(%r12), %rdi
	call	uprv_free_67@PLT
	movl	52(%r12), %eax
	movq	$0, 40(%r12)
	movl	$0, 48(%r12)
	jmp	.L448
	.cfi_endproc
.LFE2445:
	.size	_ZN6icu_6716BytesTrieBuilder18writeValueAndFinalEia, .-_ZN6icu_6716BytesTrieBuilder18writeValueAndFinalEia
	.weak	_ZTSN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE
	.section	.rodata._ZTSN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE,"aG",@progbits,_ZTSN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE,comdat
	.align 32
	.type	_ZTSN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE, @object
	.size	_ZTSN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE, 47
_ZTSN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE:
	.string	"N6icu_6716BytesTrieBuilder17BTLinearMatchNodeE"
	.weak	_ZTIN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE
	.section	.data.rel.ro._ZTIN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE,"awG",@progbits,_ZTIN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE,comdat
	.align 8
	.type	_ZTIN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE, @object
	.size	_ZTIN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE, 24
_ZTIN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE
	.quad	_ZTIN6icu_6717StringTrieBuilder15LinearMatchNodeE
	.weak	_ZTSN6icu_6716BytesTrieBuilderE
	.section	.rodata._ZTSN6icu_6716BytesTrieBuilderE,"aG",@progbits,_ZTSN6icu_6716BytesTrieBuilderE,comdat
	.align 16
	.type	_ZTSN6icu_6716BytesTrieBuilderE, @object
	.size	_ZTSN6icu_6716BytesTrieBuilderE, 28
_ZTSN6icu_6716BytesTrieBuilderE:
	.string	"N6icu_6716BytesTrieBuilderE"
	.weak	_ZTIN6icu_6716BytesTrieBuilderE
	.section	.data.rel.ro._ZTIN6icu_6716BytesTrieBuilderE,"awG",@progbits,_ZTIN6icu_6716BytesTrieBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_6716BytesTrieBuilderE, @object
	.size	_ZTIN6icu_6716BytesTrieBuilderE, 24
_ZTIN6icu_6716BytesTrieBuilderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6716BytesTrieBuilderE
	.quad	_ZTIN6icu_6717StringTrieBuilderE
	.weak	_ZTVN6icu_6716BytesTrieBuilderE
	.section	.data.rel.ro._ZTVN6icu_6716BytesTrieBuilderE,"awG",@progbits,_ZTVN6icu_6716BytesTrieBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_6716BytesTrieBuilderE, @object
	.size	_ZTVN6icu_6716BytesTrieBuilderE, 176
_ZTVN6icu_6716BytesTrieBuilderE:
	.quad	0
	.quad	_ZTIN6icu_6716BytesTrieBuilderE
	.quad	_ZN6icu_6716BytesTrieBuilderD1Ev
	.quad	_ZN6icu_6716BytesTrieBuilderD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6716BytesTrieBuilder22getElementStringLengthEi
	.quad	_ZNK6icu_6716BytesTrieBuilder14getElementUnitEii
	.quad	_ZNK6icu_6716BytesTrieBuilder15getElementValueEi
	.quad	_ZNK6icu_6716BytesTrieBuilder21getLimitOfLinearMatchEiii
	.quad	_ZNK6icu_6716BytesTrieBuilder17countElementUnitsEiii
	.quad	_ZNK6icu_6716BytesTrieBuilder23skipElementsBySomeUnitsEiii
	.quad	_ZNK6icu_6716BytesTrieBuilder26indexOfElementWithNextUnitEiiDs
	.quad	_ZNK6icu_6716BytesTrieBuilder23matchNodesCanHaveValuesEv
	.quad	_ZNK6icu_6716BytesTrieBuilder31getMaxBranchLinearSubNodeLengthEv
	.quad	_ZNK6icu_6716BytesTrieBuilder17getMinLinearMatchEv
	.quad	_ZNK6icu_6716BytesTrieBuilder23getMaxLinearMatchLengthEv
	.quad	_ZNK6icu_6716BytesTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE
	.quad	_ZN6icu_6716BytesTrieBuilder5writeEi
	.quad	_ZN6icu_6716BytesTrieBuilder17writeElementUnitsEiii
	.quad	_ZN6icu_6716BytesTrieBuilder18writeValueAndFinalEia
	.quad	_ZN6icu_6716BytesTrieBuilder17writeValueAndTypeEaii
	.quad	_ZN6icu_6716BytesTrieBuilder12writeDeltaToEi
	.weak	_ZTVN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE
	.section	.data.rel.ro._ZTVN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE,"awG",@progbits,_ZTVN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE,comdat
	.align 8
	.type	_ZTVN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE, @object
	.size	_ZTVN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE, 64
_ZTVN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE:
	.quad	0
	.quad	_ZTIN6icu_6716BytesTrieBuilder17BTLinearMatchNodeE
	.quad	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD1Ev
	.quad	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNodeD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6716BytesTrieBuilder17BTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE
	.quad	_ZN6icu_6717StringTrieBuilder15LinearMatchNode19markRightEdgesFirstEi
	.quad	_ZN6icu_6716BytesTrieBuilder17BTLinearMatchNode5writeERNS_17StringTrieBuilderE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
