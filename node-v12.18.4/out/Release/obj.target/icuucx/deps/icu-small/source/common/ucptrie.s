	.file	"ucptrie.cpp"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj, @function
_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj:
.LFB2083:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -92(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%r8, -120(%rbp)
	cmpl	$1114111, %esi
	ja	.L61
	movzbl	31(%rdi), %r15d
	movq	%rdi, %rbx
	movq	%rcx, %r13
	cmpl	%esi, 24(%rdi)
	jle	.L99
	movl	44(%rdi), %esi
	movl	%esi, -76(%rbp)
	testq	%rdx, %rdx
	je	.L9
	movl	%r9d, -52(%rbp)
	movq	%rcx, %rdi
	call	*%rdx
	movl	-52(%rbp), %r9d
	movl	%eax, -76(%rbp)
.L9:
	movq	(%rbx), %rax
	movl	-76(%rbp), %r11d
	xorl	%r10d, %r10d
	movq	%r13, -88(%rbp)
	movl	-92(%rbp), %r8d
	movl	$-1, %esi
	movl	%r9d, %r12d
	movq	%rbx, %r14
	movq	%rax, -104(%rbp)
	movl	%r10d, %eax
	movl	$-1, %edx
	movl	%r11d, %r10d
	movl	%eax, %r11d
.L54:
	movzbl	30(%r14), %ecx
	cmpl	$65535, %r8d
	jg	.L10
	testb	%cl, %cl
	je	.L11
	movl	%r8d, %eax
	sarl	$14, %eax
	cmpl	$4095, %r8d
	jle	.L100
.L14:
	addl	$64, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L10:
	movl	%r8d, %eax
	sarl	$14, %eax
	testb	%cl, %cl
	jne	.L14
	addl	$1020, %eax
.L15:
	movq	(%r14), %rdi
	cltq
	movl	%r8d, %ecx
	sarl	$9, %ecx
	movzwl	(%rdi,%rax,2), %eax
	andl	$31, %ecx
	addl	%ecx, %eax
	cltq
	movzwl	(%rdi,%rax,2), %edi
	movl	%edi, -128(%rbp)
	movl	%edi, %eax
	cmpl	%edi, %edx
	je	.L101
.L16:
	cmpw	38(%r14), %ax
	jne	.L18
	testb	%r11b, %r11b
	je	.L19
	cmpl	%r10d, -76(%rbp)
	je	.L20
.L97:
	leal	-1(%r8), %r14d
.L1:
	addq	$104, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	andw	$-32768, %ax
	movl	%r8d, %edx
	movl	$16, -108(%rbp)
	movzwl	%ax, %eax
	sarl	$4, %edx
	movl	$32, -124(%rbp)
	movl	%eax, -96(%rbp)
	movl	-128(%rbp), %eax
	andl	$31, %edx
	movl	%edx, -80(%rbp)
	movl	%eax, -112(%rbp)
.L60:
	movl	-112(%rbp), %eax
	movl	%r10d, %edx
	movq	%r14, %rbx
	andl	$32767, %eax
	movl	%eax, -132(%rbp)
	movl	-108(%rbp), %eax
	leal	-1(%rax), %r9d
.L53:
	movl	-96(%rbp), %eax
	testl	%eax, %eax
	jne	.L21
	movl	-80(%rbp), %eax
	movq	-104(%rbp), %rcx
	addl	-112(%rbp), %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	movl	%eax, -64(%rbp)
	cmpl	-64(%rbp), %esi
	je	.L102
.L23:
	movl	-64(%rbp), %eax
	cmpl	%eax, 40(%rbx)
	je	.L103
	movl	%r8d, %ecx
	andl	%r9d, %ecx
	leal	(%rcx,%rax), %r14d
	movq	8(%rbx), %rax
	cmpb	$1, %r15b
	je	.L28
	cmpb	$2, %r15b
	je	.L29
	movl	$-1, %esi
	testb	%r15b, %r15b
	je	.L104
.L30:
	testb	%r11b, %r11b
	je	.L31
	cmpl	%esi, %r12d
	je	.L66
	cmpq	$0, -72(%rbp)
	je	.L97
	movl	-76(%rbp), %eax
	movl	%eax, -52(%rbp)
	cmpl	%esi, 44(%rbx)
	je	.L34
	movl	%r9d, -144(%rbp)
	movq	-88(%rbp), %rdi
	movb	%r11b, -137(%rbp)
	movq	-72(%rbp), %rax
	movl	%edx, -136(%rbp)
	movl	%r8d, -60(%rbp)
	movl	%esi, -56(%rbp)
	call	*%rax
	movl	-144(%rbp), %r9d
	movzbl	-137(%rbp), %r11d
	movl	-136(%rbp), %edx
	movl	-60(%rbp), %r8d
	movl	%eax, -52(%rbp)
	movl	-56(%rbp), %esi
.L34:
	cmpl	-52(%rbp), %edx
	jne	.L97
	leal	1(%r8), %r13d
	testl	%r13d, %r9d
	je	.L105
.L38:
	movslq	%r14d, %rcx
	movq	%rbx, %rax
	movl	%r13d, %ebx
	addq	$1, %rcx
	movq	%rax, %r13
	movq	%rcx, %r14
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$-1, %r12d
	testb	%r15b, %r15b
	je	.L106
.L43:
	cmpl	%esi, %r12d
	je	.L44
	cmpl	%r12d, 44(%r13)
	je	.L107
	movq	-88(%rbp), %rdi
	movq	-72(%rbp), %rax
	movl	%r11d, -60(%rbp)
	movl	%r12d, %esi
	movl	%r9d, -56(%rbp)
	call	*%rax
	movl	-56(%rbp), %r9d
	movl	-60(%rbp), %r11d
	cmpl	%eax, -52(%rbp)
	jne	.L108
.L44:
	addl	$1, %ebx
	addq	$1, %r14
	testl	%ebx, %r9d
	je	.L109
	movl	%r12d, %esi
.L47:
	movq	8(%r13), %rax
	leal	-1(%rbx), %r11d
	cmpb	$1, %r15b
	je	.L40
	cmpb	$2, %r15b
	jne	.L110
	movzbl	(%rax,%r14), %r12d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L106:
	movzwl	(%rax,%r14,2), %r12d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L40:
	movl	(%rax,%r14,4), %r12d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L107:
	movl	-76(%rbp), %eax
	cmpl	%eax, -52(%rbp)
	je	.L44
.L108:
	movl	%r11d, %r14d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	movl	-80(%rbp), %edi
	movq	-104(%rbp), %r10
	movl	%edi, %eax
	movl	%edi, %ecx
	andl	$7, %edi
	sarl	$3, %ecx
	andl	$-8, %eax
	addl	-132(%rbp), %eax
	addl	%ecx, %eax
	movslq	%eax, %rcx
	leal	1(%rax,%rdi), %eax
	movzwl	(%r10,%rcx,2), %r14d
	cltq
	leal	2(%rdi,%rdi), %ecx
	movzwl	(%r10,%rax,2), %eax
	sall	%cl, %r14d
	andl	$196608, %r14d
	orl	%r14d, %eax
	movl	%eax, -64(%rbp)
	cmpl	-64(%rbp), %esi
	jne	.L23
.L102:
	movl	-108(%rbp), %edi
	movl	%r8d, %eax
	subl	-92(%rbp), %eax
	cmpl	%edi, %eax
	jl	.L23
	addl	%edi, %r8d
.L24:
	addl	$1, -80(%rbp)
	movl	-80(%rbp), %eax
	cmpl	-124(%rbp), %eax
	jl	.L53
	movl	%edx, %r10d
	movq	%rbx, %r14
	movl	-128(%rbp), %edx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L103:
	testb	%r11b, %r11b
	je	.L26
	cmpl	%edx, -76(%rbp)
	jne	.L97
.L27:
	movl	-108(%rbp), %eax
	movl	-64(%rbp), %esi
	movl	$1, %r11d
	addl	%eax, %r8d
	negl	%eax
	andl	%eax, %r8d
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%r13, %rax
	movl	%ebx, %r13d
	movq	%rax, %rbx
.L48:
	movl	-52(%rbp), %edx
	movl	-64(%rbp), %esi
	movl	%r13d, %r8d
	movl	$1, %r11d
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L101:
	movl	%r8d, %ecx
	subl	-92(%rbp), %ecx
	cmpl	$511, %ecx
	jle	.L16
	addl	$512, %r8d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L31:
	movl	-76(%rbp), %eax
	movl	%eax, -52(%rbp)
	cmpl	%esi, 44(%rbx)
	je	.L37
	movq	-72(%rbp), %rax
	movl	%esi, -52(%rbp)
	testq	%rax, %rax
	je	.L37
	movl	%r9d, -136(%rbp)
	movq	-88(%rbp), %rdi
	movl	%r8d, -60(%rbp)
	movl	%esi, -56(%rbp)
	call	*%rax
	movl	-136(%rbp), %r9d
	movl	-60(%rbp), %r8d
	movl	%eax, -52(%rbp)
	movl	-56(%rbp), %esi
.L37:
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L32
	movl	-52(%rbp), %edx
	movl	%edx, (%rax)
.L32:
	leal	1(%r8), %eax
	movl	%eax, %r13d
	testl	%eax, %r9d
	je	.L70
	cmpq	$0, -72(%rbp)
	jne	.L38
	leal	1(%r14), %edi
	leal	-1(%r8), %r11d
	movq	8(%rbx), %rdx
	subl	%r14d, %eax
	movslq	%edi, %rdi
	subl	%r14d, %r11d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$-1, %r12d
	testb	%r15b, %r15b
	je	.L111
.L51:
	cmpl	%esi, %r12d
	jne	.L1
	addl	%eax, %ecx
	addq	$1, %rdi
	testl	%ecx, %r9d
	je	.L112
.L39:
	movl	%edi, %ecx
	leal	(%r11,%rdi), %r14d
	cmpb	$1, %r15b
	je	.L49
	cmpb	$2, %r15b
	jne	.L113
	movzbl	(%rdx,%rdi), %r12d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L26:
	movl	-76(%rbp), %eax
	movq	-120(%rbp), %rdi
	movl	44(%rbx), %r12d
	movl	%eax, %edx
	testq	%rdi, %rdi
	je	.L27
	movl	%eax, (%rdi)
	movl	%eax, %edx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L104:
	movslq	%r14d, %rsi
	movzwl	(%rax,%rsi,2), %esi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L29:
	movslq	%r14d, %rsi
	movzbl	(%rax,%rsi), %esi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L28:
	movslq	%r14d, %rsi
	movl	(%rax,%rsi,4), %esi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L19:
	movl	-76(%rbp), %eax
	movq	-120(%rbp), %rdx
	movl	44(%r14), %r12d
	movl	%eax, %r10d
	testq	%rdx, %rdx
	je	.L20
	movl	%eax, (%rdx)
.L20:
	addl	$512, %r8d
	movl	40(%r14), %esi
	movl	-128(%rbp), %edx
	movl	$1, %r11d
	andl	$-512, %r8d
.L17:
	cmpl	%r8d, 24(%r14)
	jg	.L54
	movl	20(%r14), %eax
	movq	-88(%rbp), %r13
	movl	%r10d, %r11d
	movq	%r14, %rbx
	movq	8(%r14), %rcx
	subl	$2, %eax
	cmpb	$1, %r15b
	je	.L55
	cmpb	$2, %r15b
	je	.L56
	movl	$-1, %edx
	testb	%r15b, %r15b
	je	.L114
.L57:
	cmpl	%edx, 44(%rbx)
	je	.L58
	movq	-72(%rbp), %rax
	movl	%edx, -76(%rbp)
	testq	%rax, %rax
	je	.L58
	movl	%r11d, -56(%rbp)
	movl	%edx, %esi
	movq	%r13, %rdi
	movl	%r8d, -52(%rbp)
	call	*%rax
	movl	-56(%rbp), %r11d
	movl	-52(%rbp), %r8d
	movl	%eax, -76(%rbp)
.L58:
	cmpl	-76(%rbp), %r11d
	jne	.L97
.L96:
	movl	$1114111, %r14d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%edx, -52(%rbp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L99:
	testq	%r8, %r8
	je	.L96
	movl	20(%rdi), %eax
	movq	8(%rdi), %rdx
	subl	$2, %eax
	cmpb	$1, %r15b
	je	.L5
	cmpb	$2, %r15b
	je	.L6
	movl	$-1, %esi
	testb	%r15b, %r15b
	je	.L115
.L7:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L8
	movq	%r13, %rdi
	call	*%rax
	movl	%eax, %esi
.L8:
	movq	-120(%rbp), %rax
	movl	$1114111, %r14d
	movl	%esi, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movl	%r8d, %eax
	movl	%edx, -128(%rbp)
	sarl	$6, %eax
	movl	$0, -96(%rbp)
	movl	%eax, -80(%rbp)
	movl	$64, -108(%rbp)
	movl	$1024, -124(%rbp)
	movl	$0, -112(%rbp)
	jmp	.L60
.L100:
	movl	%r8d, %eax
	movl	%edx, -128(%rbp)
	sarl	$6, %eax
	movl	$0, -96(%rbp)
	movl	%eax, -80(%rbp)
	movl	$64, -108(%rbp)
	movl	$64, -124(%rbp)
	movl	$0, -112(%rbp)
	jmp	.L60
.L49:
	movl	(%rdx,%rdi,4), %r12d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L111:
	movzwl	(%rdx,%rdi,2), %r12d
	jmp	.L51
.L6:
	cltq
	movzbl	(%rdx,%rax), %esi
	jmp	.L7
.L115:
	cltq
	movzwl	(%rdx,%rax,2), %esi
	jmp	.L7
.L5:
	cltq
	movl	(%rdx,%rax,4), %esi
	jmp	.L7
.L70:
	movl	%esi, %r12d
	movl	-52(%rbp), %edx
	movl	-64(%rbp), %esi
	movl	%eax, %r8d
	movl	$1, %r11d
	jmp	.L24
.L105:
	movl	%esi, %r12d
	movl	%r13d, %r8d
	movl	-64(%rbp), %esi
	jmp	.L24
.L112:
	movl	%ecx, %r13d
	jmp	.L48
.L61:
	movl	$-1, %r14d
	jmp	.L1
.L55:
	cltq
	movl	(%rcx,%rax,4), %edx
	jmp	.L57
.L114:
	cltq
	movzwl	(%rcx,%rax,2), %edx
	jmp	.L57
.L56:
	cltq
	movzbl	(%rcx,%rax), %edx
	jmp	.L57
	.cfi_endproc
.LFE2083:
	.size	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj, .-_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj
	.p2align 4
	.globl	ucptrie_openFromBinary_67
	.type	ucptrie_openFromBinary_67, @function
ucptrie_openFromBinary_67:
.LFB2073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L116
	testl	%ecx, %ecx
	jle	.L118
	movq	%rdx, %rbx
	testb	$3, %dl
	jne	.L118
	leal	1(%rdi), %eax
	cmpl	$2, %eax
	ja	.L118
	leal	1(%rsi), %eax
	movl	%esi, %r12d
	cmpl	$3, %eax
	ja	.L118
	cmpl	$15, %ecx
	jle	.L121
	cmpl	$1416784179, (%rdx)
	jne	.L121
	movzwl	4(%rdx), %edx
	movl	%edx, %r13d
	movl	%edx, %r10d
	movl	%edx, %eax
	movl	%edx, %esi
	sarl	$6, %r10d
	andl	$7, %r13d
	shrb	$7, %al
	cmpl	$2, %r13d
	setg	%r11b
	orb	%r11b, %al
	jne	.L121
	andl	$56, %esi
	jne	.L121
	andl	$3, %r10d
	cmpl	$-1, %edi
	je	.L122
	cmpl	%r10d, %edi
	setne	%al
	cmpl	$-1, %r12d
	je	.L136
.L133:
	cmpl	%r13d, %r12d
	setne	%sil
	orl	%esi, %eax
.L123:
	testb	%al, %al
	jne	.L121
.L132:
	movl	%edx, %r14d
	movzwl	8(%rbx), %eax
	pxor	%xmm0, %xmm0
	sall	$8, %edx
	sall	$4, %r14d
	movaps	%xmm0, -80(%rbp)
	movl	%edx, %r13d
	movzwl	12(%rbx), %edx
	andl	$983040, %r14d
	movzwl	6(%rbx), %esi
	andl	$983040, %r13d
	movb	%dil, -82(%rbp)
	orl	%eax, %r14d
	movzwl	10(%rbx), %eax
	orl	%edx, %r13d
	movb	%r12b, -81(%rbp)
	movl	%esi, -96(%rbp)
	movq	%rsi, %r15
	movw	%ax, -74(%rbp)
	movzwl	14(%rbx), %eax
	movl	%r14d, -92(%rbp)
	sall	$9, %eax
	movl	%r13d, -72(%rbp)
	movl	%eax, -88(%rbp)
	addl	$4095, %eax
	sarl	$12, %eax
	movaps	%xmm0, -112(%rbp)
	movw	%ax, -84(%rbp)
	leal	16(%rsi,%rsi), %eax
	testl	%r12d, %r12d
	jne	.L124
	leal	(%rax,%r14,2), %esi
.L125:
	cmpl	%ecx, %esi
	movq	%r8, -128(%rbp)
	movl	%esi, -116(%rbp)
	jg	.L121
	movl	$48, %edi
	movq	%r9, -136(%rbp)
	call	uprv_malloc_67@PLT
	movq	-136(%rbp), %r9
	movl	-116(%rbp), %esi
	testq	%rax, %rax
	movq	-128(%rbp), %r8
	je	.L142
	movq	-72(%rbp), %rcx
	addq	$16, %rbx
	cmpl	%r13d, %r14d
	movdqu	-104(%rbp), %xmm1
	movdqu	-88(%rbp), %xmm2
	leaq	(%rbx,%r15,2), %rdx
	movq	%rbx, (%rax)
	movq	%rcx, 40(%rax)
	leal	-2(%r14), %ecx
	cmovle	%ecx, %r13d
	movups	%xmm1, 8(%rax)
	movups	%xmm2, 24(%rax)
	movq	%rdx, 8(%rax)
	movslq	%r13d, %r13
	cmpl	$1, %r12d
	je	.L129
	cmpl	$2, %r12d
	jne	.L143
	movzbl	(%rdx,%r13), %edx
	movl	%edx, 44(%rax)
.L131:
	testq	%r8, %r8
	je	.L116
	movl	%esi, (%r8)
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L144
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movl	$1, (%r9)
	xorl	%eax, %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$3, (%r9)
	xorl	%eax, %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L124:
	leal	(%rax,%r14,4), %esi
	addl	%r14d, %eax
	cmpl	$1, %r12d
	cmovne	%eax, %esi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L122:
	movl	%r10d, %edi
	cmpl	$-1, %r12d
	jne	.L133
	movl	%r13d, %r12d
	movl	%r10d, %edi
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L143:
	movzwl	(%rdx,%r13,2), %edx
	movl	%edx, 44(%rax)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L129:
	movl	(%rdx,%r13,4), %edx
	movl	%edx, 44(%rax)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L136:
	movl	%r13d, %r12d
	jmp	.L123
.L144:
	call	__stack_chk_fail@PLT
.L142:
	movl	$7, (%r9)
	jmp	.L116
	.cfi_endproc
.LFE2073:
	.size	ucptrie_openFromBinary_67, .-ucptrie_openFromBinary_67
	.p2align 4
	.globl	ucptrie_close_67
	.type	ucptrie_close_67, @function
ucptrie_close_67:
.LFB2074:
	.cfi_startproc
	endbr64
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2074:
	.size	ucptrie_close_67, .-ucptrie_close_67
	.p2align 4
	.globl	ucptrie_getType_67
	.type	ucptrie_getType_67, @function
ucptrie_getType_67:
.LFB2075:
	.cfi_startproc
	endbr64
	movsbl	30(%rdi), %eax
	ret
	.cfi_endproc
.LFE2075:
	.size	ucptrie_getType_67, .-ucptrie_getType_67
	.p2align 4
	.globl	ucptrie_getValueWidth_67
	.type	ucptrie_getValueWidth_67, @function
ucptrie_getValueWidth_67:
.LFB2076:
	.cfi_startproc
	endbr64
	movsbl	31(%rdi), %eax
	ret
	.cfi_endproc
.LFE2076:
	.size	ucptrie_getValueWidth_67, .-ucptrie_getValueWidth_67
	.p2align 4
	.globl	ucptrie_internalSmallIndex_67
	.type	ucptrie_internalSmallIndex_67, @function
ucptrie_internalSmallIndex_67:
.LFB2077:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	sarl	$14, %edx
	cmpb	$0, 30(%rdi)
	leal	64(%rdx), %eax
	jne	.L150
	leal	1020(%rdx), %eax
.L150:
	movq	(%rdi), %rdi
	cltq
	movl	%esi, %r8d
	sarl	$4, %r8d
	movzwl	(%rdi,%rax,2), %edx
	movl	%esi, %eax
	sarl	$9, %eax
	andl	$31, %eax
	addl	%edx, %eax
	movl	%r8d, %edx
	cltq
	andl	$31, %edx
	movzwl	(%rdi,%rax,2), %eax
	testw	%ax, %ax
	js	.L151
	addl	%edx, %eax
	andl	$15, %esi
	cltq
	movzwl	(%rdi,%rax,2), %eax
	addl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	movl	%r8d, %ecx
	andl	$32767, %eax
	andl	$7, %r8d
	andl	$15, %esi
	andl	$24, %ecx
	addl	%eax, %ecx
	movl	%edx, %eax
	sarl	$3, %eax
	leal	(%rcx,%rax), %edx
	leal	2(%r8,%r8), %ecx
	movslq	%edx, %rax
	movzwl	(%rdi,%rax,2), %eax
	sall	%cl, %eax
	andl	$196608, %eax
	movl	%eax, %ecx
	leal	1(%rdx,%r8), %eax
	cltq
	movzwl	(%rdi,%rax,2), %eax
	orl	%ecx, %eax
	addl	%esi, %eax
	ret
	.cfi_endproc
.LFE2077:
	.size	ucptrie_internalSmallIndex_67, .-ucptrie_internalSmallIndex_67
	.p2align 4
	.globl	ucptrie_internalSmallU8Index_67
	.type	ucptrie_internalSmallU8Index_67, @function
ucptrie_internalSmallU8Index_67:
.LFB2078:
	.cfi_startproc
	endbr64
	movzbl	%dl, %eax
	sall	$12, %esi
	sall	$6, %eax
	orl	%eax, %esi
	movzbl	%cl, %eax
	orl	%esi, %eax
	cmpl	%eax, 24(%rdi)
	jle	.L160
	movl	%eax, %ecx
	sarl	$14, %ecx
	cmpb	$0, 30(%rdi)
	leal	64(%rcx), %edx
	jne	.L157
	leal	1020(%rcx), %edx
.L157:
	movq	(%rdi), %rsi
	movslq	%edx, %rdx
	movl	%eax, %edi
	sarl	$4, %edi
	movzwl	(%rsi,%rdx,2), %ecx
	movl	%eax, %edx
	movl	%edi, %r8d
	sarl	$9, %edx
	andl	$31, %r8d
	andl	$31, %edx
	addl	%ecx, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	testw	%dx, %dx
	js	.L158
	addl	%r8d, %edx
	andl	$15, %eax
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	movl	%edi, %ecx
	andl	$32767, %edx
	andl	$7, %edi
	andl	$15, %eax
	andl	$24, %ecx
	addl	%edx, %ecx
	movl	%r8d, %edx
	sarl	$3, %edx
	addl	%ecx, %edx
	movslq	%edx, %rcx
	leal	1(%rdx,%rdi), %edx
	movzwl	(%rsi,%rcx,2), %r8d
	movslq	%edx, %rdx
	leal	2(%rdi,%rdi), %ecx
	movzwl	(%rsi,%rdx,2), %edx
	sall	%cl, %r8d
	andl	$196608, %r8d
	orl	%r8d, %edx
	addl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	movl	20(%rdi), %eax
	subl	$2, %eax
	ret
	.cfi_endproc
.LFE2078:
	.size	ucptrie_internalSmallU8Index_67, .-ucptrie_internalSmallU8Index_67
	.p2align 4
	.globl	ucptrie_internalU8PrevIndex_67
	.type	ucptrie_internalU8PrevIndex_67, @function
ucptrie_internalU8PrevIndex_67:
.LFB2079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rcx, %rbx
	subq	%rdx, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$7, %rbx
	jle	.L163
	leaq	-7(%rcx), %rdi
	movl	$7, %ebx
.L163:
	leaq	-28(%rbp), %rdx
	movl	$-1, %r8d
	movl	%r9d, %ecx
	xorl	%esi, %esi
	movl	%ebx, -28(%rbp)
	call	utf8_prevCharSafeBody_67@PLT
	subl	-28(%rbp), %ebx
	movl	%ebx, -28(%rbp)
	cmpl	$65535, %eax
	ja	.L164
	movl	%eax, %edx
	movq	(%r12), %rcx
	andl	$63, %eax
	sarl	$6, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	addl	%edx, %eax
.L165:
	sall	$3, %eax
	orl	%ebx, %eax
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L171
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	cmpl	$1114111, %eax
	ja	.L166
	cmpl	%eax, 24(%r12)
	jg	.L167
	movl	20(%r12), %esi
	leal	-2(%rsi), %eax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L166:
	movl	20(%r12), %eax
	subl	$1, %eax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L167:
	movl	%eax, %esi
	movq	%r12, %rdi
	call	ucptrie_internalSmallIndex_67
	jmp	.L165
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2079:
	.size	ucptrie_internalU8PrevIndex_67, .-ucptrie_internalU8PrevIndex_67
	.p2align 4
	.globl	ucptrie_get_67
	.type	ucptrie_get_67, @function
ucptrie_get_67:
.LFB2081:
	.cfi_startproc
	endbr64
	cmpl	$127, %esi
	jbe	.L184
	cmpb	$1, 30(%rdi)
	sbbl	%eax, %eax
	andl	$61440, %eax
	addl	$4095, %eax
	cmpl	%eax, %esi
	ja	.L175
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%eax, %esi
.L184:
	movzbl	31(%rdi), %eax
	movq	8(%rdi), %rdx
	cmpb	$1, %al
	je	.L186
.L193:
	cmpb	$2, %al
	je	.L188
	movl	$-1, %r8d
	testb	%al, %al
	je	.L192
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	cmpl	$1114111, %esi
	ja	.L176
	cmpl	%esi, 24(%rdi)
	jg	.L177
	movl	20(%rdi), %eax
	leal	-2(%rax), %esi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L192:
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	movslq	%esi, %rsi
	movl	(%rdx,%rsi,4), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	movl	20(%rdi), %eax
	movq	8(%rdi), %rdx
	leal	-1(%rax), %esi
	movzbl	31(%rdi), %eax
	cmpb	$1, %al
	jne	.L193
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L177:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	ucptrie_internalSmallIndex_67
	movq	-8(%rbp), %rdi
	movl	%eax, %esi
	movzbl	31(%rdi), %eax
	movq	8(%rdi), %rdx
	cmpb	$1, %al
	je	.L178
	cmpb	$2, %al
	je	.L179
	movl	$-1, %r8d
	testb	%al, %al
	je	.L194
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	movslq	%esi, %rsi
	movl	(%rdx,%rsi,4), %r8d
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %r8d
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %r8d
	leave
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2081:
	.size	ucptrie_get_67, .-ucptrie_get_67
	.p2align 4
	.globl	ucptrie_internalGetRange_67
	.type	ucptrie_internalGetRange_67, @function
ucptrie_internalGetRange_67:
.LFB2084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movl	%r8d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	movq	%r9, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rbp), %rcx
	movq	24(%rbp), %r8
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	testl	%eax, %eax
	jne	.L196
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	*%rbx
.L195:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L208
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	leaq	-64(%rbp), %rsi
	testq	%r8, %r8
	movl	$57344, %r15d
	movq	%r13, %rdi
	cmove	%rsi, %r8
	cmpl	$2, %eax
	movl	$56320, %esi
	movl	%r10d, -92(%rbp)
	movl	$57343, %r14d
	movl	$56319, %eax
	cmovne	%esi, %r15d
	movq	%rcx, -80(%rbp)
	cmovne	%eax, %r14d
	movq	%r8, -88(%rbp)
	movl	%r12d, %esi
	movq	%r9, -72(%rbp)
	call	*%rbx
	cmpl	$55294, %eax
	jle	.L195
	cmpl	%r14d, %r12d
	jg	.L195
	movq	-88(%rbp), %r8
	movl	-92(%rbp), %r10d
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	cmpl	(%r8), %r10d
	je	.L209
	cmpl	$55295, %r12d
	jle	.L206
	movl	%r10d, (%r8)
	cmpl	%r14d, %eax
	jg	.L210
.L202:
	movl	%r10d, -72(%rbp)
	leaq	-60(%rbp), %r8
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	*%rbx
	movl	-72(%rbp), %r10d
	cmpl	-60(%rbp), %r10d
	cmovne	%r14d, %eax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L209:
	cmpl	%r14d, %eax
	jl	.L202
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L210:
	movl	%r14d, %eax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L206:
	movl	$55295, %eax
	jmp	.L195
.L208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2084:
	.size	ucptrie_internalGetRange_67, .-ucptrie_internalGetRange_67
	.p2align 4
	.globl	ucptrie_getRange_67
	.type	ucptrie_getRange_67, @function
ucptrie_getRange_67:
.LFB2085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r10d
	movq	%r9, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L212
	movq	%r15, %rdx
	call	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj
.L211:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L224
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	leaq	-64(%rbp), %rax
	testq	%r8, %r8
	movl	$57343, %ebx
	movl	%r10d, -84(%rbp)
	cmove	%rax, %r8
	cmpl	$2, %edx
	movl	$56320, %eax
	movq	%r15, %rdx
	movl	$57344, %r14d
	movq	%r9, -72(%rbp)
	cmovne	%eax, %r14d
	movl	$56319, %eax
	movq	%r8, -80(%rbp)
	cmovne	%eax, %ebx
	call	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj
	cmpl	$55294, %eax
	jle	.L211
	cmpl	%ebx, %r12d
	jg	.L211
	movq	-80(%rbp), %r8
	movl	-84(%rbp), %r10d
	movq	-72(%rbp), %rcx
	cmpl	(%r8), %r10d
	je	.L225
	cmpl	$55295, %r12d
	jle	.L222
	movl	%r10d, (%r8)
	cmpl	%ebx, %eax
	jg	.L226
.L218:
	leaq	-60(%rbp), %r8
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	%r10d, -72(%rbp)
	call	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj
	movl	-72(%rbp), %r10d
	cmpl	-60(%rbp), %r10d
	cmovne	%ebx, %eax
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L225:
	cmpl	%ebx, %eax
	jl	.L218
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L226:
	movl	%ebx, %eax
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L222:
	movl	$55295, %eax
	jmp	.L211
.L224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2085:
	.size	ucptrie_getRange_67, .-ucptrie_getRange_67
	.p2align 4
	.globl	ucptrie_toBinary_67
	.type	ucptrie_toBinary_67, @function
ucptrie_toBinary_67:
.LFB2086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L227
	movsbl	30(%rdi), %eax
	movq	%rdi, %rbx
	cmpl	$1, %eax
	ja	.L229
	movzbl	31(%rdi), %r13d
	testl	%edx, %edx
	js	.L229
	cmpb	$2, %r13b
	ja	.L229
	testl	%edx, %edx
	jne	.L248
.L230:
	movl	16(%rbx), %r9d
	movl	20(%rbx), %edi
	leal	16(%r9,%r9), %r8d
	cmpb	$1, %r13b
	je	.L231
	leal	(%r8,%rdi,2), %r12d
	leal	(%r8,%rdi), %r10d
	cmpb	$2, %r13b
	cmove	%r10d, %r12d
.L233:
	cmpl	%edx, %r12d
	jle	.L234
	movl	$15, (%rcx)
.L227:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L229
	testb	$3, %sil
	je	.L230
.L229:
	movl	$1, (%rcx)
	xorl	%r12d, %r12d
	addq	$8, %rsp
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movl	40(%rbx), %ecx
	movl	%edi, %edx
	sall	$6, %eax
	movw	%di, 8(%rsi)
	sarl	$4, %edx
	movl	$1416784179, (%rsi)
	leaq	16(%rsi), %rdi
	movl	%ecx, %r10d
	andw	$-4096, %dx
	movw	%r9w, 6(%rsi)
	sarl	$8, %r10d
	andw	$3840, %r10w
	orl	%r10d, %edx
	movsbw	%r13b, %r10w
	orl	%r10d, %edx
	orl	%edx, %eax
	leal	-16(%r8), %edx
	movw	%ax, 4(%rsi)
	movzwl	38(%rbx), %eax
	movslq	%edx, %rdx
	movw	%cx, 12(%rsi)
	movw	%ax, 10(%rsi)
	movl	24(%rbx), %eax
	sarl	$9, %eax
	movw	%ax, 14(%rsi)
	movq	(%rbx), %rsi
	call	memcpy@PLT
	movq	%rax, %rdi
	movl	16(%rbx), %eax
	addl	%eax, %eax
	cltq
	addq	%rax, %rdi
	cmpb	$1, %r13b
	je	.L235
	cmpb	$2, %r13b
	je	.L236
	movl	20(%rbx), %eax
	movq	8(%rbx), %rsi
	leal	(%rax,%rax), %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L231:
	leal	(%r8,%rdi,4), %r12d
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L236:
	movslq	20(%rbx), %rdx
	movq	8(%rbx), %rsi
	call	memcpy@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L235:
	movl	20(%rbx), %eax
	movq	8(%rbx), %rsi
	leal	0(,%rax,4), %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	jmp	.L227
	.cfi_endproc
.LFE2086:
	.size	ucptrie_toBinary_67, .-ucptrie_toBinary_67
	.p2align 4
	.globl	ucpmap_get_67
	.type	ucpmap_get_67, @function
ucpmap_get_67:
.LFB2087:
	.cfi_startproc
	endbr64
	cmpl	$127, %esi
	jbe	.L261
	cmpb	$1, 30(%rdi)
	sbbl	%eax, %eax
	andl	$61440, %eax
	addl	$4095, %eax
	cmpl	%eax, %esi
	ja	.L252
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%eax, %esi
.L261:
	movzbl	31(%rdi), %eax
	movq	8(%rdi), %rdx
	cmpb	$1, %al
	je	.L263
.L270:
	cmpb	$2, %al
	je	.L265
	movl	$-1, %r8d
	testb	%al, %al
	je	.L269
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	cmpl	$1114111, %esi
	ja	.L253
	cmpl	24(%rdi), %esi
	jl	.L254
	movl	20(%rdi), %eax
	leal	-2(%rax), %esi
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L269:
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	movslq	%esi, %rsi
	movl	(%rdx,%rsi,4), %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	movl	20(%rdi), %eax
	movq	8(%rdi), %rdx
	leal	-1(%rax), %esi
	movzbl	31(%rdi), %eax
	cmpb	$1, %al
	jne	.L270
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L254:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	ucptrie_internalSmallIndex_67
	movq	-8(%rbp), %rdi
	movl	%eax, %esi
	movzbl	31(%rdi), %eax
	movq	8(%rdi), %rdx
	cmpb	$1, %al
	je	.L255
	cmpb	$2, %al
	je	.L256
	movl	$-1, %r8d
	testb	%al, %al
	je	.L271
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movslq	%esi, %rsi
	movl	(%rdx,%rsi,4), %r8d
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %r8d
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movslq	%esi, %rsi
	movzbl	(%rdx,%rsi), %r8d
	leave
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2087:
	.size	ucpmap_get_67, .-ucpmap_get_67
	.p2align 4
	.globl	ucpmap_getRange_67
	.type	ucpmap_getRange_67, @function
ucpmap_getRange_67:
.LFB2088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r10d
	movq	%r9, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L273
	movq	%r15, %rdx
	call	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj
.L272:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L285
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	leaq	-64(%rbp), %rax
	testq	%r8, %r8
	movl	$57343, %ebx
	movl	%r10d, -84(%rbp)
	cmove	%rax, %r8
	cmpl	$2, %edx
	movl	$56320, %eax
	movq	%r15, %rdx
	movl	$57344, %r14d
	movq	%r9, -72(%rbp)
	cmovne	%eax, %r14d
	movl	$56319, %eax
	movq	%r8, -80(%rbp)
	cmovne	%eax, %ebx
	call	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj
	cmpl	$55294, %eax
	jle	.L272
	cmpl	%ebx, %r12d
	jg	.L272
	movq	-80(%rbp), %r8
	movl	-84(%rbp), %r10d
	movq	-72(%rbp), %rcx
	cmpl	(%r8), %r10d
	je	.L286
	cmpl	$55295, %r12d
	jle	.L283
	movl	%r10d, (%r8)
	cmpl	%ebx, %eax
	jg	.L287
.L279:
	leaq	-60(%rbp), %r8
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r13, %rdi
	movl	%r10d, -72(%rbp)
	call	_ZN12_GLOBAL__N_18getRangeEPKviPFjS1_jES1_Pj
	movl	-72(%rbp), %r10d
	cmpl	-60(%rbp), %r10d
	cmovne	%ebx, %eax
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L286:
	cmpl	%ebx, %eax
	jl	.L279
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L287:
	movl	%ebx, %eax
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$55295, %eax
	jmp	.L272
.L285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2088:
	.size	ucpmap_getRange_67, .-ucpmap_getRange_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
