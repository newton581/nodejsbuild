	.file	"dtintrv.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712DateInterval17getDynamicClassIDEv
	.type	_ZNK6icu_6712DateInterval17getDynamicClassIDEv, @function
_ZNK6icu_6712DateInterval17getDynamicClassIDEv:
.LFB8:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712DateInterval16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE8:
	.size	_ZNK6icu_6712DateInterval17getDynamicClassIDEv, .-_ZNK6icu_6712DateInterval17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712DateIntervaleqERKS0_
	.type	_ZNK6icu_6712DateIntervaleqERKS0_, @function
_ZNK6icu_6712DateIntervaleqERKS0_:
.LFB27:
	.cfi_startproc
	endbr64
	movsd	8(%rdi), %xmm0
	ucomisd	8(%rsi), %xmm0
	jp	.L6
	jne	.L6
	movsd	16(%rdi), %xmm0
	ucomisd	16(%rsi), %xmm0
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE27:
	.size	_ZNK6icu_6712DateIntervaleqERKS0_, .-_ZNK6icu_6712DateIntervaleqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712DateIntervalD2Ev
	.type	_ZN6icu_6712DateIntervalD2Ev, @function
_ZN6icu_6712DateIntervalD2Ev:
.LFB16:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712DateIntervalE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE16:
	.size	_ZN6icu_6712DateIntervalD2Ev, .-_ZN6icu_6712DateIntervalD2Ev
	.globl	_ZN6icu_6712DateIntervalD1Ev
	.set	_ZN6icu_6712DateIntervalD1Ev,_ZN6icu_6712DateIntervalD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712DateIntervalD0Ev
	.type	_ZN6icu_6712DateIntervalD0Ev, @function
_ZN6icu_6712DateIntervalD0Ev:
.LFB18:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712DateIntervalE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE18:
	.size	_ZN6icu_6712DateIntervalD0Ev, .-_ZN6icu_6712DateIntervalD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6712DateInterval5cloneEv
	.type	_ZNK6icu_6712DateInterval5cloneEv, @function
_ZNK6icu_6712DateInterval5cloneEv:
.LFB26:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L11
	leaq	16+_ZTVN6icu_6712DateIntervalE(%rip), %rdx
	movq	%rdx, (%rax)
	cmpq	%rbx, %rax
	je	.L11
	movupd	8(%rbx), %xmm0
	movups	%xmm0, 8(%rax)
.L11:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26:
	.size	_ZNK6icu_6712DateInterval5cloneEv, .-_ZNK6icu_6712DateInterval5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712DateInterval16getStaticClassIDEv
	.type	_ZN6icu_6712DateInterval16getStaticClassIDEv, @function
_ZN6icu_6712DateInterval16getStaticClassIDEv:
.LFB7:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6712DateInterval16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE7:
	.size	_ZN6icu_6712DateInterval16getStaticClassIDEv, .-_ZN6icu_6712DateInterval16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712DateIntervalC2Edd
	.type	_ZN6icu_6712DateIntervalC2Edd, @function
_ZN6icu_6712DateIntervalC2Edd:
.LFB13:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712DateIntervalE(%rip), %rax
	unpcklpd	%xmm1, %xmm0
	movq	%rax, (%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE13:
	.size	_ZN6icu_6712DateIntervalC2Edd, .-_ZN6icu_6712DateIntervalC2Edd
	.globl	_ZN6icu_6712DateIntervalC1Edd
	.set	_ZN6icu_6712DateIntervalC1Edd,_ZN6icu_6712DateIntervalC2Edd
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712DateIntervalC2ERKS0_
	.type	_ZN6icu_6712DateIntervalC2ERKS0_, @function
_ZN6icu_6712DateIntervalC2ERKS0_:
.LFB23:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712DateIntervalE(%rip), %rax
	movq	%rax, (%rdi)
	cmpq	%rsi, %rdi
	je	.L18
	movupd	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
.L18:
	ret
	.cfi_endproc
.LFE23:
	.size	_ZN6icu_6712DateIntervalC2ERKS0_, .-_ZN6icu_6712DateIntervalC2ERKS0_
	.globl	_ZN6icu_6712DateIntervalC1ERKS0_
	.set	_ZN6icu_6712DateIntervalC1ERKS0_,_ZN6icu_6712DateIntervalC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712DateIntervalaSERKS0_
	.type	_ZN6icu_6712DateIntervalaSERKS0_, @function
_ZN6icu_6712DateIntervalaSERKS0_:
.LFB25:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	cmpq	%rsi, %rdi
	je	.L21
	movupd	8(%rsi), %xmm0
	movups	%xmm0, 8(%rdi)
.L21:
	ret
	.cfi_endproc
.LFE25:
	.size	_ZN6icu_6712DateIntervalaSERKS0_, .-_ZN6icu_6712DateIntervalaSERKS0_
	.weak	_ZTSN6icu_6712DateIntervalE
	.section	.rodata._ZTSN6icu_6712DateIntervalE,"aG",@progbits,_ZTSN6icu_6712DateIntervalE,comdat
	.align 16
	.type	_ZTSN6icu_6712DateIntervalE, @object
	.size	_ZTSN6icu_6712DateIntervalE, 24
_ZTSN6icu_6712DateIntervalE:
	.string	"N6icu_6712DateIntervalE"
	.weak	_ZTIN6icu_6712DateIntervalE
	.section	.data.rel.ro._ZTIN6icu_6712DateIntervalE,"awG",@progbits,_ZTIN6icu_6712DateIntervalE,comdat
	.align 8
	.type	_ZTIN6icu_6712DateIntervalE, @object
	.size	_ZTIN6icu_6712DateIntervalE, 24
_ZTIN6icu_6712DateIntervalE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712DateIntervalE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6712DateIntervalE
	.section	.data.rel.ro.local._ZTVN6icu_6712DateIntervalE,"awG",@progbits,_ZTVN6icu_6712DateIntervalE,comdat
	.align 8
	.type	_ZTVN6icu_6712DateIntervalE, @object
	.size	_ZTVN6icu_6712DateIntervalE, 56
_ZTVN6icu_6712DateIntervalE:
	.quad	0
	.quad	_ZTIN6icu_6712DateIntervalE
	.quad	_ZN6icu_6712DateIntervalD1Ev
	.quad	_ZN6icu_6712DateIntervalD0Ev
	.quad	_ZNK6icu_6712DateInterval17getDynamicClassIDEv
	.quad	_ZNK6icu_6712DateIntervaleqERKS0_
	.quad	_ZNK6icu_6712DateInterval5cloneEv
	.local	_ZZN6icu_6712DateInterval16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6712DateInterval16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
