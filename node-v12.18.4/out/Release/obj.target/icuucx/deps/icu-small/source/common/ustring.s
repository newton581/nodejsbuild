	.file	"ustring.cpp"
	.text
	.p2align 4
	.type	_ZL13_matchFromSetPKDsS0_a, @function
_ZL13_matchFromSetPKDsS0_a:
.LFB2067:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testw	%ax, %ax
	je	.L24
	xorl	%ecx, %ecx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L3:
	leal	1(%rcx), %r8d
	addq	$1, %rcx
	movzwl	(%rsi,%rcx,2), %eax
	testw	%ax, %ax
	je	.L2
.L5:
	andl	$63488, %eax
	movl	%ecx, %r9d
	cmpl	$55296, %eax
	jne	.L3
	leal	1(%rcx), %eax
	cltq
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%eax, %r8d
	addq	$1, %rax
	cmpw	$0, -2(%rsi,%rax,2)
	jne	.L4
.L8:
	movzwl	(%rdi), %ecx
	movl	$-1, %r12d
	testw	%cx, %cx
	je	.L1
	leal	-1(%r8), %eax
	xorl	%r12d, %r12d
	leaq	2(%rsi,%rax,2), %r10
.L9:
	movl	%ecx, %eax
	leal	1(%r12), %ebx
	movzwl	%cx, %r11d
	andl	$63488, %eax
	cmpl	$55296, %eax
	je	.L10
	testb	%dl, %dl
	je	.L11
	testl	%r8d, %r8d
	je	.L35
	movq	%rsi, %rax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L58:
	addq	$2, %rax
	cmpq	%rax, %r10
	je	.L35
.L14:
	cmpw	%cx, (%rax)
	jne	.L58
.L1:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	testl	%r8d, %r8d
	je	.L1
	movq	%rsi, %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$2, %rax
	cmpq	%rax, %r10
	je	.L1
.L15:
	cmpw	%cx, (%rax)
	jne	.L59
.L35:
	movslq	%ebx, %rax
	movl	%ebx, %r12d
	movzwl	(%rdi,%rax,2), %ecx
	testw	%cx, %cx
	jne	.L9
	notl	%ebx
	movl	%ebx, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	andb	$4, %ch
	jne	.L16
	movslq	%ebx, %rax
	movzwl	(%rdi,%rax,2), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L60
.L16:
	movl	%r9d, %eax
	testb	%dl, %dl
	jne	.L19
.L57:
	cmpl	%r8d, %eax
	jl	.L61
.L21:
	xorl	%eax, %eax
	cmpl	$65535, %r11d
	setg	%al
	addl	$1, %eax
	subl	%eax, %ebx
	movl	%ebx, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movzwl	2(%rsi,%r14), %r13d
	movl	%r13d, %r14d
	andl	$-1024, %r14d
	cmpl	$56320, %r14d
	je	.L62
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%r12d, %eax
.L18:
	cmpl	%ecx, %r11d
	je	.L21
.L19:
	cmpl	%r8d, %eax
	jge	.L35
	movslq	%eax, %rcx
	leal	1(%rax), %r12d
	leaq	(%rcx,%rcx), %r14
	movzwl	(%rsi,%rcx,2), %ecx
	movl	%ecx, %r13d
	andl	$-1024, %r13d
	cmpl	$55296, %r13d
	jne	.L31
	cmpl	%r8d, %r12d
	je	.L31
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L65:
	movzwl	2(%rsi,%r14), %r13d
	movl	%r13d, %r14d
	andl	$-1024, %r14d
	cmpl	$56320, %r14d
	je	.L64
	.p2align 4,,10
	.p2align 3
.L34:
	movl	%r12d, %eax
.L22:
	cmpl	%ecx, %r11d
	je	.L35
	cmpl	%r8d, %eax
	jge	.L21
.L61:
	movslq	%eax, %rcx
	leal	1(%rax), %r12d
	leaq	(%rcx,%rcx), %r14
	movzwl	(%rsi,%rcx,2), %ecx
	movl	%ecx, %r13d
	andl	$-1024, %r13d
	cmpl	$55296, %r13d
	jne	.L34
	cmpl	%r8d, %r12d
	je	.L34
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L62:
	sall	$10, %ecx
	addl	$2, %eax
	leal	-56613888(%r13,%rcx), %ecx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L64:
	sall	$10, %ecx
	addl	$2, %eax
	leal	-56613888(%r13,%rcx), %ecx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L60:
	sall	$10, %r11d
	leal	2(%r12), %ebx
	leal	-56613888(%rax,%r11), %r11d
	movl	%r9d, %eax
	testb	%dl, %dl
	je	.L57
	jmp	.L19
.L24:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%r8d, %r9d
	jmp	.L8
	.cfi_endproc
.LFE2067:
	.size	_ZL13_matchFromSetPKDsS0_a, .-_ZL13_matchFromSetPKDsS0_a
	.p2align 4
	.type	_ZL15_charPtr_charAtiPv, @function
_ZL15_charPtr_charAtiPv:
.LFB2094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movl	$1, %edx
	addq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-10(%rbp), %r8
	movq	%r8, %rsi
	call	u_charsToUChars_67@PLT
	movzwl	-10(%rbp), %eax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L69
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2094:
	.size	_ZL15_charPtr_charAtiPv, .-_ZL15_charPtr_charAtiPv
	.p2align 4
	.type	u_unescapeAt_67.constprop.0, @function
u_unescapeAt_67.constprop.0:
.LFB2568:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rdx, -72(%rbp)
	movl	(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ecx, -80(%rbp)
	cmpl	%esi, %ecx
	jge	.L71
	testl	%ecx, %ecx
	js	.L71
	leal	1(%rcx), %eax
	leaq	-60(%rbp), %r13
	movl	%esi, %r12d
	movl	%eax, (%rdi)
	movslq	%ecx, %rdi
	movq	%r13, %rsi
	addq	%rdx, %rdi
	movl	$1, %edx
	call	u_charsToUChars_67@PLT
	movzwl	-60(%rbp), %eax
	cmpw	$117, %ax
	je	.L72
	cmpw	$120, %ax
	je	.L73
	cmpw	$85, %ax
	je	.L112
	leal	-48(%rax), %edx
	cmpw	$7, %dx
	ja	.L78
	movl	(%rbx), %edi
	leal	-48(%rax), %r8d
	movzbl	%r8b, %r15d
	cmpl	%edi, %r12d
	jle	.L70
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$1, %edx
	movslq	%edi, %rdi
	movq	%r13, %rsi
	addq	-72(%rbp), %rdi
	call	u_charsToUChars_67@PLT
	movzwl	-60(%rbp), %eax
	leal	-48(%rax), %edx
	cmpw	$7, %dx
	ja	.L86
	subl	$48, %eax
	leal	0(,%r15,8), %r8d
	addl	$1, %r14d
	movzbl	%al, %eax
	orl	%r8d, %eax
	cmpb	$2, %r14b
	movl	$2, %r14d
	movl	%eax, %r15d
	movl	(%rbx), %eax
	setg	%dl
	leal	1(%rax), %edi
	cmpl	%edi, %r12d
	movl	%edi, (%rbx)
	setle	%al
	orb	%al, %dl
	je	.L80
.L86:
	cmpl	$1114111, %r15d
	ja	.L71
	movslq	(%rbx), %rax
	cmpl	%eax, %r12d
	jle	.L70
	movl	%r15d, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L113
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movb	$0, -75(%rbp)
	movl	(%rbx), %edi
	movb	$8, -73(%rbp)
	movb	$8, -74(%rbp)
.L77:
	cmpl	%edi, %r12d
	jle	.L71
.L94:
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L115:
	leal	-65(%rsi), %edx
	cmpw	$5, %dx
	jbe	.L82
	leal	-97(%rsi), %edx
	cmpw	$5, %dx
	ja	.L83
	leal	-87(%rsi), %edx
.L84:
	movl	(%rbx), %eax
	movl	%r15d, %r8d
	movsbl	%dl, %edx
	addl	$1, %r14d
	sall	$4, %r8d
	orl	%r8d, %edx
	leal	1(%rax), %edi
	movl	%edi, (%rbx)
	movl	%edx, %r15d
	cmpl	%edi, %r12d
	jle	.L83
	cmpb	%r14b, -73(%rbp)
	jle	.L83
.L85:
	movl	$1, %edx
	movq	%r13, %rsi
	movslq	%edi, %rdi
	addq	-72(%rbp), %rdi
	call	u_charsToUChars_67@PLT
	movzwl	-60(%rbp), %esi
	leal	-48(%rsi), %edx
	cmpw	$9, %dx
	ja	.L115
	leal	-48(%rsi), %edx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L82:
	leal	-55(%rsi), %edx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L83:
	cmpb	%r14b, -74(%rbp)
	jg	.L71
	cmpb	$0, -75(%rbp)
	je	.L86
	cmpw	$125, %si
	jne	.L71
	addl	$1, (%rbx)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L73:
	movslq	(%rbx), %rax
	cmpl	%eax, %r12d
	jle	.L71
	movq	-72(%rbp), %rcx
	movl	$1, %edx
	movq	%r13, %rsi
	leaq	(%rcx,%rax), %rdi
	call	u_charsToUChars_67@PLT
	cmpw	$123, -60(%rbp)
	je	.L76
	movb	$0, -75(%rbp)
	movl	(%rbx), %edi
	movb	$2, -73(%rbp)
	movb	$1, -74(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-72(%rbp), %rcx
	leal	1(%rax), %edx
	leaq	-62(%rbp), %rsi
	movl	%edx, -60(%rbp)
	movl	$1, %edx
	leaq	(%rcx,%rax), %rdi
	call	u_charsToUChars_67@PLT
	movzwl	-62(%rbp), %eax
	cmpw	$92, %ax
	je	.L116
.L87:
	movzwl	%ax, %edx
	andl	$64512, %eax
	cmpl	$56320, %eax
	jne	.L70
	movl	-60(%rbp), %eax
	sall	$10, %r15d
	leal	-56613888(%rdx,%r15), %r15d
	movl	%eax, (%rbx)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	movl	-80(%rbp), %eax
	movl	$-1, %r15d
	movl	%eax, (%rbx)
	jmp	.L70
.L78:
	cmpw	$97, %ax
	je	.L96
	cmpw	$96, %ax
	jbe	.L90
	cmpw	$98, %ax
	je	.L97
	cmpw	$101, %ax
	je	.L98
	cmpw	$100, %ax
	jbe	.L90
	cmpw	$102, %ax
	je	.L99
	cmpw	$101, %ax
	je	.L91
	cmpw	$110, %ax
	je	.L100
	cmpw	$109, %ax
	jbe	.L91
	cmpw	$114, %ax
	je	.L101
	cmpw	$113, %ax
	jbe	.L91
	cmpw	$116, %ax
	je	.L102
	cmpw	$115, %ax
	jbe	.L91
	cmpw	$118, %ax
	je	.L117
.L91:
	movzwl	%ax, %r14d
	movl	%r14d, %r15d
	.p2align 4,,10
	.p2align 3
.L92:
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L70
	movslq	(%rbx), %rax
	cmpl	%eax, %r12d
	jle	.L70
	movq	-72(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addq	%rax, %rdi
	call	u_charsToUChars_67@PLT
	movzwl	-60(%rbp), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L70
	addl	$1, (%rbx)
	movl	%r14d, %ebx
	sall	$10, %ebx
	leal	-56613888(%rax,%rbx), %r15d
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L90:
	movzwl	%ax, %r14d
	movl	%r14d, %r15d
	cmpw	$99, %ax
	jne	.L92
	movslq	(%rbx), %rax
	movl	$99, %r15d
	cmpl	%eax, %r12d
	jle	.L70
	leal	1(%rax), %edx
	movq	-72(%rbp), %rcx
	movq	%r13, %rsi
	movl	%edx, (%rbx)
	movl	$1, %edx
	leaq	(%rcx,%rax), %rdi
	call	u_charsToUChars_67@PLT
	movzwl	-60(%rbp), %r15d
	movl	%r15d, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L118
.L93:
	andl	$31, %r15d
	jmp	.L70
.L76:
	movl	(%rbx), %eax
	movb	$1, -75(%rbp)
	movb	$8, -73(%rbp)
	leal	1(%rax), %edi
	movb	$1, -74(%rbp)
	movl	%edi, (%rbx)
	jmp	.L77
.L116:
	cmpl	-60(%rbp), %r12d
	jle	.L70
	movq	-72(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	u_unescapeAt_67.constprop.0
	jmp	.L87
.L118:
	movslq	(%rbx), %rax
	cmpl	%eax, %r12d
	jle	.L93
	movq	-72(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addq	%rax, %rdi
	call	u_charsToUChars_67@PLT
	movzwl	-60(%rbp), %eax
	movl	%eax, %edx
	andl	$64512, %edx
	cmpl	$56320, %edx
	jne	.L93
	sall	$10, %r15d
	addl	$1, (%rbx)
	movl	%r15d, %r8d
	leal	9216(%rax,%r8), %r15d
	jmp	.L93
.L114:
	call	__stack_chk_fail@PLT
.L99:
	movl	$6, %eax
.L89:
	addl	$1, %eax
	leaq	_ZL12UNESCAPE_MAP(%rip), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %r15d
	jmp	.L70
.L96:
	xorl	%eax, %eax
	jmp	.L89
.L98:
	movl	$4, %eax
	jmp	.L89
.L97:
	movl	$2, %eax
	jmp	.L89
.L117:
	movl	$14, %eax
	jmp	.L89
.L102:
	movl	$12, %eax
	jmp	.L89
.L101:
	movl	$10, %eax
	jmp	.L89
.L100:
	movl	$8, %eax
	jmp	.L89
.L72:
	movl	(%rbx), %edi
	cmpl	%edi, %r12d
	jle	.L71
	movb	$4, -74(%rbp)
	movb	$4, -73(%rbp)
	movb	$0, -75(%rbp)
	jmp	.L94
	.cfi_endproc
.LFE2568:
	.size	u_unescapeAt_67.constprop.0, .-u_unescapeAt_67.constprop.0
	.p2align 4
	.type	u_strFindLast_67.part.0, @function
u_strFindLast_67.part.0:
.LFB2567:
	.cfi_startproc
	cmpl	$-1, %ecx
	je	.L178
.L120:
	movq	%rdi, %r8
	testl	%ecx, %ecx
	je	.L175
	movslq	%ecx, %r8
	addq	%r8, %r8
	leaq	(%rdx,%r8), %r11
	movzwl	-2(%r11), %r10d
	subl	$1, %ecx
	je	.L179
.L123:
	cmpl	$-1, %esi
	je	.L180
.L130:
	cmpl	%esi, %ecx
	jge	.L143
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	leaq	-2(%rdi,%r8), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 3, -32
	leaq	(%rdi,%rsi,2), %rbx
	cmpq	%r9, %rbx
	je	.L145
	subq	$2, %r11
	movq	%rbx, %rcx
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L134:
	cmpq	%rcx, %r9
	je	.L145
.L133:
	movzwl	-2(%rcx), %esi
	subq	$2, %rcx
	cmpw	%si, %r10w
	jne	.L134
	movq	%r11, %rax
	movq	%rcx, %r8
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L135:
	subq	$2, %r8
	subq	$2, %rax
	movzwl	(%rax), %r14d
	cmpw	%r14w, (%r8)
	jne	.L134
.L137:
	cmpq	%rax, %rdx
	jne	.L135
	movzwl	(%r8), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L136
	cmpq	%r8, %rdi
	jne	.L181
.L136:
	andl	$64512, %esi
	cmpl	$55296, %esi
	jne	.L119
	cmpq	%r8, %rbx
	jne	.L182
.L119:
	popq	%rbx
	movq	%r8, %rax
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 14
	movl	%r10d, %eax
	andl	$63488, %eax
	cmpl	$55296, %eax
	je	.L123
	cmpl	$-1, %esi
	je	.L183
	testl	%esi, %esi
	jle	.L143
	movslq	%esi, %rsi
	leaq	(%rdi,%rsi,2), %r8
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L184:
	cmpq	%r8, %rdi
	je	.L143
.L129:
	subq	$2, %r8
	cmpw	(%r8), %r10w
	jne	.L184
.L175:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	cmpw	$0, (%rdx)
	je	.L138
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L122:
	addq	$2, %rax
	cmpw	$0, (%rax)
	jne	.L122
	subq	%rdx, %rax
	movq	%rax, %rcx
	sarq	%rcx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 14, -24
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r14
	movq	%r8, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movzwl	-2(%r8), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L136
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L182:
	movzwl	2(%rcx), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L119
	jmp	.L134
.L183:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 14
	xorl	%r8d, %r8d
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L125:
	testw	%ax, %ax
	je	.L175
.L126:
	addq	$2, %rdi
.L127:
	movzwl	(%rdi), %eax
	cmpw	%ax, %r10w
	jne	.L125
	movq	%rdi, %r8
	testw	%r10w, %r10w
	jne	.L126
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L180:
	cmpw	$0, (%rdi)
	je	.L142
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L132:
	addq	$2, %rax
	cmpw	$0, (%rax)
	jne	.L132
	subq	%rdi, %rax
	movq	%rax, %rsi
	sarq	%rsi
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L143:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
.L138:
	movq	%rdi, %r8
	jmp	.L175
.L142:
	xorl	%esi, %esi
	jmp	.L130
	.cfi_endproc
.LFE2567:
	.size	u_strFindLast_67.part.0, .-u_strFindLast_67.part.0
	.p2align 4
	.type	u_strFindFirst_67.part.0, @function
u_strFindFirst_67.part.0:
.LFB2566:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testl	%esi, %esi
	jns	.L186
	testl	%ecx, %ecx
	jns	.L186
	movzwl	(%rdx), %r11d
	movq	%rdi, %r10
	testw	%r11w, %r11w
	je	.L185
	movzwl	2(%rdx), %ebx
	movzwl	(%rdi), %r9d
	testw	%bx, %bx
	jne	.L188
	movl	%r11d, %eax
	andl	$63488, %eax
	cmpl	$55296, %eax
	je	.L188
	cmpw	%r9w, %r11w
	jne	.L192
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L298:
	movzwl	2(%rdi), %r9d
	addq	$2, %rdi
	cmpw	%r9w, %r11w
	je	.L225
.L192:
	testw	%r9w, %r9w
	jne	.L298
	.p2align 4,,10
	.p2align 3
.L235:
	xorl	%r10d, %r10d
.L185:
	popq	%rbx
	movq	%r10, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	cmpl	$-1, %ecx
	je	.L299
.L197:
	movq	%rdi, %r10
	testl	%ecx, %ecx
	je	.L185
	movzwl	(%rdx), %r9d
	subl	$1, %ecx
	jne	.L199
	movl	%r9d, %eax
	andl	$63488, %eax
	cmpl	$55296, %eax
	je	.L199
	cmpl	$-1, %esi
	je	.L300
	testl	%esi, %esi
	jle	.L235
	movslq	%esi, %rsi
	leaq	(%rdi,%rsi,2), %rax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L188:
	leaq	2(%rdi), %r8
	testw	%r9w, %r9w
	je	.L235
	leaq	2(%rdx), %r12
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L193:
	movzwl	(%r8), %r9d
	addq	$2, %r8
	testw	%r9w, %r9w
	je	.L235
.L190:
	leaq	-2(%r8), %r10
	cmpw	%r9w, %r11w
	jne	.L193
	movq	%r8, %rdx
	movl	%ebx, %ecx
	movq	%r12, %rsi
	testw	%bx, %bx
	jne	.L194
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L302:
	cmpw	%cx, %ax
	jne	.L193
	movzwl	2(%rsi), %ecx
	addq	$2, %rsi
	addq	$2, %rdx
	testw	%cx, %cx
	je	.L196
.L194:
	movzwl	(%rdx), %eax
	testw	%ax, %ax
	jne	.L302
	xorl	%r10d, %r10d
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L199:
	movslq	%ecx, %rax
	leaq	2(%rdx), %rbx
	addq	%rax, %rax
	leaq	(%rbx,%rax), %r11
	cmpl	$-1, %esi
	je	.L303
	cmpl	%ecx, %esi
	jle	.L235
	movslq	%esi, %rsi
	leaq	(%rdi,%rsi,2), %r13
	movq	%r13, %r12
	subq	%rax, %r12
	cmpq	%r12, %rdi
	je	.L235
	movq	%rdi, %rdx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L212:
	cmpq	%rdx, %r12
	je	.L235
.L211:
	movq	%rdx, %r10
	movzwl	(%rdx), %r8d
	addq	$2, %rdx
	cmpw	%r8w, %r9w
	jne	.L212
	movq	%rdx, %rcx
	movq	%rbx, %rax
	cmpq	%r11, %rbx
	je	.L304
	.p2align 4,,10
	.p2align 3
.L213:
	movzwl	(%rcx), %esi
	cmpw	(%rax), %si
	jne	.L212
	addq	$2, %rax
	addq	$2, %rcx
	cmpq	%rax, %r11
	jne	.L213
.L215:
	andl	$64512, %r8d
	leaq	-2(%rdx), %rax
	cmpl	$56320, %r8d
	jne	.L214
	cmpq	%rax, %rdi
	jne	.L305
.L214:
	andl	$64512, %esi
	cmpl	$55296, %esi
	jne	.L185
	cmpq	%rax, %r13
	je	.L185
	movzwl	(%rcx), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L185
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L299:
	cmpw	$0, (%rdx)
	je	.L225
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L198:
	addq	$2, %rax
	cmpw	$0, (%rax)
	jne	.L198
	subq	%rdx, %rax
	movq	%rax, %rcx
	sarq	%rcx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L306:
	addq	$2, %r10
	cmpq	%r10, %rax
	je	.L235
.L204:
	cmpw	(%r10), %r9w
	jne	.L306
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L303:
	movzwl	(%rdi), %r8d
	leaq	2(%rdi), %rsi
	testw	%r8w, %r8w
	jne	.L206
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L207:
	movzwl	(%rsi), %r8d
	addq	$2, %rsi
	testw	%r8w, %r8w
	je	.L235
.L206:
	leaq	-2(%rsi), %r10
	cmpw	%r8w, %r9w
	jne	.L207
	movq	%rsi, %rcx
	movq	%rbx, %rax
	cmpq	%r11, %rbx
	jne	.L208
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L308:
	cmpw	(%rax), %dx
	jne	.L207
	addq	$2, %rax
	addq	$2, %rcx
	cmpq	%rax, %r11
	je	.L210
.L208:
	movzwl	(%rcx), %edx
	testw	%dx, %dx
	jne	.L308
	jmp	.L235
.L301:
	movl	%r11d, %eax
	.p2align 4,,10
	.p2align 3
.L196:
	andl	$64512, %r9d
	cmpl	$56320, %r9d
	jne	.L195
	cmpq	%r10, %rdi
	jne	.L309
.L195:
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L185
	movzwl	(%rdx), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L185
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%rdi, %r10
	jmp	.L185
.L307:
	movl	%r9d, %edx
	.p2align 4,,10
	.p2align 3
.L210:
	andl	$64512, %r8d
	cmpl	$56320, %r8d
	jne	.L209
	cmpq	%r10, %rdi
	jne	.L310
.L209:
	andl	$64512, %edx
	cmpl	$55296, %edx
	jne	.L185
	movzwl	(%rcx), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L185
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L305:
	movzwl	-4(%rdx), %r8d
	andl	$-1024, %r8d
	cmpl	$55296, %r8d
	jne	.L214
	jmp	.L212
.L300:
	movzwl	(%rdi), %eax
	cmpw	%ax, %r9w
	jne	.L202
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L311:
	movzwl	2(%rdi), %eax
	addq	$2, %rdi
	cmpw	%ax, %r9w
	je	.L225
.L202:
	testw	%ax, %ax
	jne	.L311
	jmp	.L235
.L309:
	movzwl	-4(%r8), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L195
	jmp	.L193
.L304:
	movl	%r9d, %esi
	jmp	.L215
.L310:
	movzwl	-4(%rsi), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L209
	jmp	.L207
	.cfi_endproc
.LFE2566:
	.size	u_strFindFirst_67.part.0, .-u_strFindFirst_67.part.0
	.p2align 4
	.globl	u_strFindFirst_67
	.type	u_strFindFirst_67, @function
u_strFindFirst_67:
.LFB2055:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L314
	cmpl	$-1, %ecx
	jl	.L314
	testq	%rdi, %rdi
	je	.L315
	cmpl	$-1, %esi
	jl	.L315
	jmp	u_strFindFirst_67.part.0
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2055:
	.size	u_strFindFirst_67, .-u_strFindFirst_67
	.p2align 4
	.globl	u_strstr_67
	.type	u_strstr_67, @function
u_strstr_67:
.LFB2056:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testq	%rsi, %rsi
	je	.L361
	testq	%rdi, %rdi
	je	.L361
	movzwl	(%rsi), %r11d
	testw	%r11w, %r11w
	je	.L361
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzwl	2(%rsi), %r12d
	movzwl	(%rdi), %ecx
	testw	%r12w, %r12w
	jne	.L318
	movl	%r11d, %edx
	andl	$63488, %edx
	cmpl	$55296, %edx
	jne	.L319
.L318:
	leaq	2(%rax), %rdx
	testw	%cx, %cx
	je	.L332
	addq	$2, %rsi
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L323:
	movzwl	(%rdx), %ecx
	addq	$2, %rdx
	testw	%cx, %cx
	je	.L332
.L320:
	leaq	-2(%rdx), %rbx
	cmpw	%cx, %r11w
	jne	.L323
	movq	%rdx, %rdi
	movl	%r12d, %r10d
	movq	%rsi, %r9
	testw	%r12w, %r12w
	jne	.L324
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L366:
	cmpw	%r10w, %r8w
	jne	.L323
	movzwl	2(%r9), %r10d
	addq	$2, %r9
	addq	$2, %rdi
	testw	%r10w, %r10w
	je	.L326
.L324:
	movzwl	(%rdi), %r8d
	testw	%r8w, %r8w
	jne	.L366
.L332:
	xorl	%eax, %eax
.L341:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
.L365:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	%r11d, %r8d
	.p2align 4,,10
	.p2align 3
.L326:
	andl	$64512, %ecx
	cmpl	$56320, %ecx
	jne	.L325
	cmpq	%rbx, %rax
	je	.L325
	movzwl	-4(%rdx), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L323
	.p2align 4,,10
	.p2align 3
.L325:
	andl	$64512, %r8d
	cmpl	$55296, %r8d
	je	.L327
.L364:
	movq	%rbx, %rax
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L319:
	cmpw	%r11w, %cx
	jne	.L322
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L367:
	movzwl	2(%rax), %ecx
	addq	$2, %rax
	cmpw	%cx, %r11w
	je	.L341
.L322:
	testw	%cx, %cx
	jne	.L367
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L327:
	movzwl	(%rdi), %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L364
	jmp	.L323
	.cfi_endproc
.LFE2056:
	.size	u_strstr_67, .-u_strstr_67
	.p2align 4
	.globl	u_strchr_67
	.type	u_strchr_67, @function
u_strchr_67:
.LFB2057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$63488, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movw	%si, -4(%rbp)
	cmpl	$55296, %eax
	je	.L377
	movzwl	(%rdi), %eax
	movl	%esi, %edx
	cmpw	%ax, %si
	jne	.L371
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L378:
	movzwl	2(%rdi), %eax
	addq	$2, %rdi
	cmpw	%ax, %dx
	je	.L373
.L371:
	testw	%ax, %ax
	jne	.L378
.L374:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L374
	leaq	-4(%rbp), %rdx
	movl	$1, %ecx
	movl	$-1, %esi
	call	u_strFindFirst_67.part.0
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_restore_state
	leave
	.cfi_def_cfa 7, 8
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE2057:
	.size	u_strchr_67, .-u_strchr_67
	.p2align 4
	.globl	u_strchr32_67
	.type	u_strchr32_67, @function
u_strchr32_67:
.LFB2058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$65535, %esi
	jbe	.L396
	cmpl	$1114111, %esi
	ja	.L393
	movl	%esi, %ecx
	movzwl	(%rdi), %edx
	andw	$1023, %si
	sarl	$10, %ecx
	orw	$-9216, %si
	subw	$10304, %cx
	testw	%dx, %dx
	jne	.L385
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L386:
	movzwl	2(%rdi), %edx
	addq	$2, %rdi
	testw	%dx, %dx
	je	.L393
.L385:
	movq	%rdi, %rax
	cmpw	%dx, %cx
	jne	.L386
	cmpw	%si, 2(%rdi)
	jne	.L386
	.p2align 4,,10
	.p2align 3
.L379:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L397
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movl	%esi, %eax
	movw	%si, -10(%rbp)
	movl	%esi, %edx
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L398
	movzwl	(%rdi), %eax
	cmpw	%si, %ax
	jne	.L383
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L399:
	movzwl	2(%rdi), %eax
	addq	$2, %rdi
	cmpw	%ax, %dx
	je	.L389
.L383:
	testw	%ax, %ax
	jne	.L399
	.p2align 4,,10
	.p2align 3
.L393:
	xorl	%eax, %eax
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L398:
	testq	%rdi, %rdi
	je	.L393
	leaq	-10(%rbp), %rdx
	movl	$1, %ecx
	movl	$-1, %esi
	call	u_strFindFirst_67.part.0
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L389:
	movq	%rdi, %rax
	jmp	.L379
.L397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2058:
	.size	u_strchr32_67, .-u_strchr32_67
	.p2align 4
	.globl	u_memchr_67
	.type	u_memchr_67, @function
u_memchr_67:
.LFB2059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movw	%si, -4(%rbp)
	testl	%r8d, %r8d
	jle	.L405
	movl	%esi, %edx
	andl	$63488, %esi
	cmpl	$55296, %esi
	je	.L408
	leaq	(%rdi,%r8,2), %rcx
	movq	%rdi, %rax
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L409:
	addq	$2, %rax
	cmpq	%rax, %rcx
	je	.L405
.L403:
	cmpw	(%rax), %dx
	jne	.L409
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L405
	leaq	-4(%rbp), %rdx
	movl	$1, %ecx
	movl	%r8d, %esi
	call	u_strFindFirst_67.part.0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2059:
	.size	u_memchr_67, .-u_memchr_67
	.p2align 4
	.globl	u_memchr32_67
	.type	u_memchr32_67, @function
u_memchr32_67:
.LFB2060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$65535, %esi
	jbe	.L432
	cmpl	$1, %r8d
	jle	.L421
	cmpl	$1114111, %esi
	ja	.L421
	movl	%esi, %eax
	andw	$1023, %si
	leaq	-2(%rdi,%r8,2), %rdx
	sarl	$10, %eax
	orw	$-9216, %si
	subw	$10304, %ax
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L416:
	addq	$2, %rdi
	cmpq	%rdi, %rdx
	je	.L421
.L417:
	cmpw	%ax, (%rdi)
	jne	.L416
	cmpw	%si, 2(%rdi)
	jne	.L416
	movq	%rdi, %rax
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L432:
	movw	%si, -10(%rbp)
	movl	%esi, %edx
	testl	%r8d, %r8d
	jle	.L421
	andl	$-2048, %esi
	cmpl	$55296, %esi
	je	.L433
	leaq	(%rdi,%r8,2), %rcx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L414:
	cmpw	(%rax), %dx
	je	.L410
	addq	$2, %rax
	cmpq	%rax, %rcx
	jne	.L414
	.p2align 4,,10
	.p2align 3
.L421:
	xorl	%eax, %eax
.L410:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L434
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L421
	leaq	-10(%rbp), %rdx
	movl	$1, %ecx
	movl	%r8d, %esi
	call	u_strFindFirst_67.part.0
	jmp	.L410
.L434:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2060:
	.size	u_memchr32_67, .-u_memchr32_67
	.p2align 4
	.globl	u_strFindLast_67
	.type	u_strFindLast_67, @function
u_strFindLast_67:
.LFB2061:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L437
	cmpl	$-1, %ecx
	jl	.L437
	testq	%rdi, %rdi
	je	.L438
	cmpl	$-1, %esi
	jl	.L438
	jmp	u_strFindLast_67.part.0
	.p2align 4,,10
	.p2align 3
.L437:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2061:
	.size	u_strFindLast_67, .-u_strFindLast_67
	.p2align 4
	.globl	u_strrstr_67
	.type	u_strrstr_67, @function
u_strrstr_67:
.LFB2062:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	testq	%rsi, %rsi
	je	.L441
	testq	%rdi, %rdi
	je	.L442
	movl	$-1, %ecx
	movl	$-1, %esi
	jmp	u_strFindLast_67.part.0
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2062:
	.size	u_strrstr_67, .-u_strrstr_67
	.p2align 4
	.globl	u_strrchr_67
	.type	u_strrchr_67, @function
u_strrchr_67:
.LFB2063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movw	%si, -4(%rbp)
	andl	$63488, %esi
	cmpl	$55296, %esi
	jne	.L449
	testq	%rdi, %rdi
	je	.L450
	leaq	-4(%rbp), %rdx
	movl	$1, %ecx
	movl	$-1, %esi
	call	u_strFindLast_67.part.0
.L443:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L446:
	testw	%dx, %dx
	je	.L443
.L448:
	addq	$2, %rdi
.L444:
	movzwl	(%rdi), %edx
	cmpw	%dx, %cx
	jne	.L446
	movq	%rdi, %rax
	testw	%cx, %cx
	jne	.L448
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	leave
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2063:
	.size	u_strrchr_67, .-u_strrchr_67
	.p2align 4
	.globl	u_strrchr32_67
	.type	u_strrchr32_67, @function
u_strrchr32_67:
.LFB2064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$65535, %esi
	jbe	.L476
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	ja	.L454
	movl	%esi, %r8d
	movzwl	(%rdi), %ecx
	andw	$1023, %si
	sarl	$10, %r8d
	orw	$-9216, %si
	subw	$10304, %r8w
	testw	%cx, %cx
	jne	.L462
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L463:
	testw	%dx, %dx
	je	.L454
.L464:
	addq	$2, %rdi
	movl	%edx, %ecx
.L462:
	movzwl	2(%rdi), %edx
	cmpw	%cx, %r8w
	jne	.L463
	cmpw	%si, %dx
	jne	.L463
	movq	%rdi, %rax
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L476:
	movw	%si, -10(%rbp)
	movl	%esi, %ecx
	andl	$-2048, %esi
	cmpl	$55296, %esi
	jne	.L466
	testq	%rdi, %rdi
	je	.L467
	leaq	-10(%rbp), %rdx
	movl	$1, %ecx
	movl	$-1, %esi
	call	u_strFindLast_67.part.0
	.p2align 4,,10
	.p2align 3
.L454:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L477
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L458:
	testw	%dx, %dx
	je	.L454
.L460:
	addq	$2, %rdi
.L456:
	movzwl	(%rdi), %edx
	cmpw	%dx, %cx
	jne	.L458
	movq	%rdi, %rax
	testw	%cx, %cx
	jne	.L460
	jmp	.L454
.L467:
	xorl	%eax, %eax
	jmp	.L454
.L477:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2064:
	.size	u_strrchr32_67, .-u_strrchr32_67
	.p2align 4
	.globl	u_memrchr_67
	.type	u_memrchr_67, @function
u_memrchr_67:
.LFB2065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movw	%si, -4(%rbp)
	testl	%r8d, %r8d
	jle	.L483
	movl	%esi, %edx
	andl	$63488, %esi
	leaq	(%rdi,%r8,2), %rax
	cmpl	$55296, %esi
	jne	.L481
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L487:
	cmpq	%rax, %rdi
	je	.L483
.L481:
	subq	$2, %rax
	cmpw	(%rax), %dx
	jne	.L487
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L483
	leaq	-4(%rbp), %rdx
	movl	$1, %ecx
	movl	%r8d, %esi
	call	u_strFindLast_67.part.0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2065:
	.size	u_memrchr_67, .-u_memrchr_67
	.p2align 4
	.globl	u_memrchr32_67
	.type	u_memrchr32_67, @function
u_memrchr32_67:
.LFB2066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$65535, %esi
	jbe	.L509
	cmpl	$1114111, %esi
	ja	.L499
	cmpl	$1, %r8d
	jle	.L499
	movl	%esi, %edx
	andw	$1023, %si
	leaq	-2(%rdi,%r8,2), %rax
	sarl	$10, %edx
	orw	$-9216, %si
	subw	$10304, %dx
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L494:
	subq	$2, %rax
	cmpq	%rax, %rdi
	je	.L499
.L495:
	cmpw	%si, (%rax)
	jne	.L494
	cmpw	%dx, -2(%rax)
	jne	.L494
	subq	$2, %rax
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L509:
	movw	%si, -10(%rbp)
	movl	%esi, %edx
	testl	%r8d, %r8d
	jle	.L499
	andl	$-2048, %esi
	leaq	(%rdi,%r8,2), %rax
	cmpl	$55296, %esi
	je	.L510
	.p2align 4,,10
	.p2align 3
.L492:
	subq	$2, %rax
	cmpw	(%rax), %dx
	je	.L488
	cmpq	%rax, %rdi
	jne	.L492
	.p2align 4,,10
	.p2align 3
.L499:
	xorl	%eax, %eax
.L488:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L511
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L499
	leaq	-10(%rbp), %rdx
	movl	$1, %ecx
	movl	%r8d, %esi
	call	u_strFindLast_67.part.0
	jmp	.L488
.L511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2066:
	.size	u_memrchr32_67, .-u_memrchr32_67
	.p2align 4
	.globl	u_strpbrk_67
	.type	u_strpbrk_67, @function
u_strpbrk_67:
.LFB2068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZL13_matchFromSetPKDsS0_a
	xorl	%r8d, %r8d
	testl	%eax, %eax
	js	.L512
	cltq
	leaq	(%rdi,%rax,2), %r8
.L512:
	movq	%r8, %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2068:
	.size	u_strpbrk_67, .-u_strpbrk_67
	.p2align 4
	.globl	u_strcspn_67
	.type	u_strcspn_67, @function
u_strcspn_67:
.LFB2069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZL13_matchFromSetPKDsS0_a
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %r8d
	sarl	$31, %eax
	xorl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2069:
	.size	u_strcspn_67, .-u_strcspn_67
	.p2align 4
	.globl	u_strspn_67
	.type	u_strspn_67, @function
u_strspn_67:
.LFB2070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZL13_matchFromSetPKDsS0_a
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %r8d
	sarl	$31, %eax
	xorl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2070:
	.size	u_strspn_67, .-u_strspn_67
	.p2align 4
	.globl	u_strtok_r_67
	.type	u_strtok_r_67, @function
u_strtok_r_67:
.LFB2071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	testq	%rdi, %rdi
	je	.L524
	movq	%rdi, (%rdx)
.L525:
	xorl	%edx, %edx
	call	_ZL13_matchFromSetPKDsS0_a
	movl	%eax, %r8d
	sarl	$31, %eax
	xorl	%r8d, %eax
	cltq
	leaq	(%rdi,%rax,2), %r12
	cmpw	$0, (%r12)
	je	.L528
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZL13_matchFromSetPKDsS0_a
	testl	%eax, %eax
	js	.L529
	cltq
	xorl	%edx, %edx
	leaq	(%r12,%rax,2), %rax
	movw	%dx, (%rax)
	addq	$2, %rax
	movq	%rax, (%rbx)
.L523:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	movq	$0, (%rbx)
.L532:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L525
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L529:
	cmpq	$0, (%rbx)
	je	.L532
	movq	$0, (%rbx)
	jmp	.L523
	.cfi_endproc
.LFE2071:
	.size	u_strtok_r_67, .-u_strtok_r_67
	.p2align 4
	.globl	u_strcat_67
	.type	u_strcat_67, @function
u_strcat_67:
.LFB2072:
	.cfi_startproc
	endbr64
	cmpw	$0, (%rdi)
	movq	%rdi, %rax
	movq	%rdi, %rcx
	je	.L534
	.p2align 4,,10
	.p2align 3
.L535:
	addq	$2, %rcx
	cmpw	$0, (%rcx)
	jne	.L535
.L534:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L536:
	movzwl	(%rsi,%rdx), %r8d
	movw	%r8w, (%rcx,%rdx)
	addq	$2, %rdx
	testw	%r8w, %r8w
	jne	.L536
	ret
	.cfi_endproc
.LFE2072:
	.size	u_strcat_67, .-u_strcat_67
	.p2align 4
	.globl	u_strncat_67
	.type	u_strncat_67, @function
u_strncat_67:
.LFB2073:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testl	%edx, %edx
	jle	.L542
	cmpw	$0, (%rdi)
	movq	%rdi, %rcx
	je	.L546
	.p2align 4,,10
	.p2align 3
.L543:
	addq	$2, %rcx
	cmpw	$0, (%rcx)
	jne	.L543
.L546:
	movzwl	(%rsi), %edi
	movw	%di, (%rcx)
	testw	%di, %di
	je	.L558
	addq	$2, %rcx
	cmpl	$1, %edx
	je	.L547
	subl	$2, %edx
	leaq	2(%rsi,%rdx,2), %rdi
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L550:
	addq	$2, %rcx
	cmpq	%rsi, %rdi
	je	.L547
.L549:
	movzwl	2(%rsi), %edx
	addq	$2, %rsi
	movw	%dx, (%rcx)
	testw	%dx, %dx
	jne	.L550
.L542:
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	xorl	%edx, %edx
	movw	%dx, (%rcx)
	ret
.L558:
	ret
	.cfi_endproc
.LFE2073:
	.size	u_strncat_67, .-u_strncat_67
	.p2align 4
	.globl	u_strcmp_67
	.type	u_strcmp_67, @function
u_strcmp_67:
.LFB2074:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L560:
	movzwl	(%rdi,%rax), %edx
	movzwl	(%rsi,%rax), %ecx
	addq	$2, %rax
	cmpw	%cx, %dx
	jne	.L562
	testw	%dx, %dx
	jne	.L560
.L562:
	movzwl	%dx, %eax
	subl	%ecx, %eax
	ret
	.cfi_endproc
.LFE2074:
	.size	u_strcmp_67, .-u_strcmp_67
	.p2align 4
	.globl	uprv_strCompare_67
	.type	uprv_strCompare_67, @function
uprv_strCompare_67:
.LFB2075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testl	%esi, %esi
	jns	.L566
	testl	%ecx, %ecx
	jns	.L566
	cmpq	%rdx, %rdi
	je	.L596
	movzwl	(%rdi), %r11d
	movzwl	(%rdx), %ebx
	movq	%rdx, %r10
	movq	%rdi, %r8
	cmpw	%bx, %r11w
	je	.L569
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L633:
	movzwl	2(%r8), %r11d
	movzwl	2(%r10), %ebx
	addq	$2, %r8
	addq	$2, %r10
	cmpw	%bx, %r11w
	jne	.L592
.L569:
	testw	%r11w, %r11w
	jne	.L633
.L596:
	xorl	%eax, %eax
.L565:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	testb	%r8b, %r8b
	je	.L570
	cmpq	%rdx, %rdi
	je	.L596
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi), %rax
	leaq	(%rdi,%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L596
	movq	%rdx, %r10
	movq	%rdi, %r8
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L634:
	testw	%r11w, %r11w
	je	.L596
	addq	$2, %r8
	addq	$2, %r10
	cmpq	%r8, %rsi
	je	.L596
.L572:
	movzwl	(%r8), %r11d
	movzwl	(%r10), %ebx
	cmpw	%bx, %r11w
	je	.L634
	leaq	(%rdx,%rax), %r12
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L570:
	testl	%esi, %esi
	js	.L635
	testl	%ecx, %ecx
	jns	.L576
	cmpw	$0, (%rdx)
	je	.L598
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L578:
	addq	$2, %rax
	cmpw	$0, (%rax)
	jne	.L578
	subq	%rdx, %rax
	movq	%rax, %rcx
	sarq	%rcx
.L576:
	cmpl	%esi, %ecx
	jle	.L577
	movslq	%esi, %rax
	leaq	(%rdi,%rax,2), %r12
	movl	$-1, %eax
.L579:
	cmpq	%rdx, %rdi
	je	.L565
	cmpq	%rdi, %r12
	je	.L565
	movq	%rdx, %r10
	movq	%rdi, %r8
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L636:
	addq	$2, %r8
	addq	$2, %r10
	cmpq	%r8, %r12
	je	.L565
.L582:
	movzwl	(%r8), %r11d
	movzwl	(%r10), %ebx
	cmpw	%bx, %r11w
	je	.L636
	movslq	%esi, %rsi
	movslq	%ecx, %rcx
	leaq	(%rdi,%rsi,2), %rsi
	leaq	(%rdx,%rcx,2), %r12
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L592:
	xorl	%r12d, %r12d
	xorl	%esi, %esi
.L568:
	cmpw	$-10241, %r11w
	seta	%cl
	cmpw	$-10241, %bx
	seta	%al
	testb	%al, %cl
	movzwl	%r11w, %eax
	movzwl	%bx, %ecx
	je	.L583
	testb	%r9b, %r9b
	jne	.L637
.L583:
	subl	%ecx, %eax
.L639:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L635:
	.cfi_restore_state
	cmpw	$0, (%rdi)
	je	.L597
	movq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L575:
	addq	$2, %rsi
	cmpw	$0, (%rsi)
	jne	.L575
	subq	%rdi, %rsi
	sarq	%rsi
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L637:
	cmpw	$-9217, %r11w
	jbe	.L638
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L585
	cmpq	%rdi, %r8
	je	.L585
	movzwl	-2(%r8), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L586
	.p2align 4,,10
	.p2align 3
.L585:
	subw	$10240, %r11w
	movzwl	%r11w, %eax
.L586:
	cmpw	$-9217, %bx
	ja	.L587
	leaq	2(%r10), %rdx
	cmpq	%rdx, %r12
	je	.L588
	movzwl	2(%r10), %edx
	movzwl	%bx, %ecx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L583
.L588:
	subw	$10240, %bx
	movzwl	%bx, %ecx
	subl	%ecx, %eax
	jmp	.L639
.L598:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L577:
	cmpl	%ecx, %esi
	je	.L640
	movslq	%ecx, %rax
	leaq	(%rdi,%rax,2), %r12
	movl	$1, %eax
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L638:
	leaq	2(%r8), %rcx
	cmpq	%rcx, %rsi
	je	.L585
	movzwl	2(%r8), %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L585
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L587:
	movl	%ebx, %esi
	movzwl	%bx, %ecx
	andl	$64512, %esi
	cmpl	$56320, %esi
	jne	.L588
	cmpq	%rdx, %r10
	je	.L588
	movzwl	-2(%r10), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L588
	subl	%ecx, %eax
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L640:
	movslq	%esi, %rax
	leaq	(%rdi,%rax,2), %r12
	xorl	%eax, %eax
	jmp	.L579
.L597:
	xorl	%esi, %esi
	jmp	.L576
	.cfi_endproc
.LFE2075:
	.size	uprv_strCompare_67, .-uprv_strCompare_67
	.p2align 4
	.globl	u_strCompareIter_67
	.type	u_strCompareIter_67, @function
u_strCompareIter_67:
.LFB2076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	sete	%dl
	cmpq	%rsi, %rdi
	sete	%al
	orb	%al, %dl
	jne	.L646
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L646
	movq	%rdi, %r15
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	*40(%rdi)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	*40(%r13)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L674:
	cmpl	$-1, %ebx
	je	.L646
.L647:
	movq	%r15, %rdi
	call	*72(%r15)
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	*72(%r13)
	movl	%eax, %r12d
	cmpl	%eax, %ebx
	je	.L674
	cmpl	$55295, %ebx
	setg	%dl
	cmpl	$55295, %eax
	setg	%al
	testb	%al, %dl
	je	.L648
	testb	%r14b, %r14b
	je	.L648
	cmpl	$56319, %ebx
	jg	.L653
	movq	%r15, %rdi
	call	*64(%r15)
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L652
.L653:
	movl	%ebx, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L675
.L651:
	subl	$10240, %ebx
.L652:
	cmpl	$56319, %r12d
	jg	.L657
	movq	%r13, %rdi
	call	*64(%r13)
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L648
.L657:
	movl	%r12d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L676
.L656:
	subl	$10240, %r12d
.L648:
	addq	$8, %rsp
	movl	%ebx, %eax
	subl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L675:
	.cfi_restore_state
	movq	%r15, %rdi
	call	*80(%r15)
	movq	%r15, %rdi
	call	*80(%r15)
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L651
	jmp	.L652
.L676:
	movq	%r13, %rdi
	call	*80(%r13)
	movq	%r13, %rdi
	call	*80(%r13)
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L656
	jmp	.L648
	.cfi_endproc
.LFE2076:
	.size	u_strCompareIter_67, .-u_strCompareIter_67
	.p2align 4
	.globl	u_strCompare_67
	.type	u_strCompare_67, @function
u_strCompare_67:
.LFB2077:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L677
	cmpl	$-1, %esi
	jl	.L677
	testq	%rdx, %rdx
	je	.L677
	cmpl	$-1, %ecx
	jl	.L677
	movsbl	%r8b, %r9d
	xorl	%r8d, %r8d
	jmp	uprv_strCompare_67
	.p2align 4,,10
	.p2align 3
.L677:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2077:
	.size	u_strCompare_67, .-u_strCompare_67
	.p2align 4
	.globl	u_strcmpCodePointOrder_67
	.type	u_strcmpCodePointOrder_67, @function
u_strcmpCodePointOrder_67:
.LFB2078:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L694
	movzwl	(%rdi), %edx
	movzwl	(%rsi), %ecx
	movq	%rsi, %r9
	movq	%rdi, %r8
	cmpw	%dx, %cx
	je	.L684
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L728:
	movzwl	2(%r8), %edx
	movzwl	2(%r9), %ecx
	addq	$2, %r8
	addq	$2, %r9
	cmpw	%cx, %dx
	jne	.L683
.L684:
	testw	%dx, %dx
	jne	.L728
.L694:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	cmpw	$-10241, %dx
	jbe	.L727
	cmpw	$-10241, %cx
	jbe	.L727
	cmpw	$-9217, %dx
	ja	.L686
	movzwl	2(%r8), %edi
	movzwl	%dx, %eax
	andl	$-1024, %edi
	cmpl	$56320, %edi
	je	.L687
.L688:
	subw	$10240, %dx
	movzwl	%dx, %eax
.L687:
	cmpw	$-9217, %cx
	jbe	.L729
	movl	%ecx, %edi
	movzwl	%cx, %edx
	andl	$64512, %edi
	cmpl	$56320, %edi
	jne	.L689
	cmpq	%r9, %rsi
	jne	.L730
.L689:
	subw	$10240, %cx
	movzwl	%cx, %edx
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	movzwl	%dx, %eax
	movzwl	%cx, %edx
.L685:
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	movl	%edx, %r10d
	movzwl	%dx, %eax
	andl	$64512, %r10d
	cmpl	$56320, %r10d
	jne	.L688
	cmpq	%r8, %rdi
	je	.L688
	movzwl	-2(%r8), %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L688
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L729:
	movzwl	2(%r9), %esi
	movzwl	%cx, %edx
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L689
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L730:
	movzwl	-2(%r9), %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	jne	.L689
	jmp	.L685
	.cfi_endproc
.LFE2078:
	.size	u_strcmpCodePointOrder_67, .-u_strcmpCodePointOrder_67
	.p2align 4
	.globl	u_strncmp_67
	.type	u_strncmp_67, @function
u_strncmp_67:
.LFB2079:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L731
	movzwl	(%rdi), %eax
	movzwl	(%rsi), %ecx
	movl	%eax, %r8d
	subl	%ecx, %eax
	jne	.L731
	leal	-1(%rdx), %r10d
	xorl	%edx, %edx
	addq	%r10, %r10
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L741:
	cmpq	%r10, %rdx
	je	.L731
	movzwl	2(%rdi,%rdx), %ecx
	movzwl	2(%rsi,%rdx), %r9d
	addq	$2, %rdx
	movl	%ecx, %r8d
	subl	%r9d, %ecx
	jne	.L740
.L733:
	testw	%r8w, %r8w
	jne	.L741
.L731:
	ret
	.p2align 4,,10
	.p2align 3
.L740:
	movl	%ecx, %eax
	ret
	.cfi_endproc
.LFE2079:
	.size	u_strncmp_67, .-u_strncmp_67
	.p2align 4
	.globl	u_strncmpCodePointOrder_67
	.type	u_strncmpCodePointOrder_67, @function
u_strncmpCodePointOrder_67:
.LFB2080:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jns	.L743
	cmpq	%rsi, %rdi
	je	.L762
	movzwl	(%rdi), %edx
	movzwl	(%rsi), %r10d
	movq	%rsi, %r8
	movq	%rdi, %rcx
	cmpw	%r10w, %dx
	je	.L746
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L794:
	movzwl	2(%rcx), %edx
	movzwl	2(%r8), %r10d
	addq	$2, %rcx
	addq	$2, %r8
	cmpw	%r10w, %dx
	jne	.L758
.L746:
	testw	%dx, %dx
	jne	.L794
.L762:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L743:
	cmpq	%rsi, %rdi
	je	.L762
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx), %rax
	leaq	(%rdi,%rax), %r11
	cmpq	%r11, %rdi
	je	.L762
	movq	%rsi, %r8
	movq	%rdi, %rcx
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L795:
	testw	%dx, %dx
	je	.L762
	addq	$2, %rcx
	addq	$2, %r8
	cmpq	%rcx, %r11
	je	.L762
.L748:
	movzwl	(%rcx), %edx
	movzwl	(%r8), %r10d
	cmpw	%r10w, %dx
	je	.L795
	leaq	(%rsi,%rax), %r9
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L758:
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
.L745:
	cmpw	$-10241, %dx
	jbe	.L793
	cmpw	$-10241, %r10w
	jbe	.L793
	cmpw	$-9217, %dx
	ja	.L750
	leaq	2(%rcx), %rax
	cmpq	%rax, %r11
	je	.L751
	movzwl	2(%rcx), %ecx
	movzwl	%dx, %eax
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L752
.L751:
	subw	$10240, %dx
	movzwl	%dx, %eax
.L752:
	cmpw	$-9217, %r10w
	ja	.L753
	leaq	2(%r8), %rdx
	cmpq	%rdx, %r9
	je	.L754
	movzwl	2(%r8), %ecx
	movzwl	%r10w, %edx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L749
.L754:
	subw	$10240, %r10w
	movzwl	%r10w, %edx
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L793:
	movzwl	%dx, %eax
	movzwl	%r10w, %edx
.L749:
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L753:
	movl	%r10d, %ecx
	movzwl	%r10w, %edx
	andl	$64512, %ecx
	cmpl	$56320, %ecx
	jne	.L754
	cmpq	%r8, %rsi
	je	.L754
	movzwl	-2(%r8), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L754
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L750:
	movl	%edx, %r11d
	movzwl	%dx, %eax
	andl	$64512, %r11d
	cmpl	$56320, %r11d
	jne	.L751
	cmpq	%rcx, %rdi
	je	.L751
	movzwl	-2(%rcx), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L751
	jmp	.L752
	.cfi_endproc
.LFE2080:
	.size	u_strncmpCodePointOrder_67, .-u_strncmpCodePointOrder_67
	.p2align 4
	.globl	u_strcpy_67
	.type	u_strcpy_67, @function
u_strcpy_67:
.LFB2081:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L797:
	movzwl	(%rsi,%rdx), %ecx
	movw	%cx, (%rax,%rdx)
	addq	$2, %rdx
	testw	%cx, %cx
	jne	.L797
	ret
	.cfi_endproc
.LFE2081:
	.size	u_strcpy_67, .-u_strcpy_67
	.p2align 4
	.globl	u_strncpy_67
	.type	u_strncpy_67, @function
u_strncpy_67:
.LFB2082:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testl	%edx, %edx
	jle	.L800
	leal	-1(%rdx), %edi
	xorl	%edx, %edx
	movzwl	(%rsi,%rdx,2), %ecx
	movw	%cx, (%rax,%rdx,2)
	testw	%cx, %cx
	je	.L800
	.p2align 4,,10
	.p2align 3
.L806:
	leaq	1(%rdx), %rcx
	cmpq	%rdx, %rdi
	je	.L800
	movq	%rcx, %rdx
	movzwl	(%rsi,%rdx,2), %ecx
	movw	%cx, (%rax,%rdx,2)
	testw	%cx, %cx
	jne	.L806
.L800:
	ret
	.cfi_endproc
.LFE2082:
	.size	u_strncpy_67, .-u_strncpy_67
	.p2align 4
	.globl	u_strlen_67
	.type	u_strlen_67, @function
u_strlen_67:
.LFB2083:
	.cfi_startproc
	endbr64
	cmpw	$0, (%rdi)
	je	.L810
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L809:
	addq	$2, %rax
	cmpw	$0, (%rax)
	jne	.L809
	subq	%rdi, %rax
	sarq	%rax
	ret
	.p2align 4,,10
	.p2align 3
.L810:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2083:
	.size	u_strlen_67, .-u_strlen_67
	.p2align 4
	.globl	u_countChar32_67
	.type	u_countChar32_67, @function
u_countChar32_67:
.LFB2084:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L820
	cmpl	$-1, %esi
	jl	.L820
	je	.L837
	xorl	%r8d, %r8d
	testl	%esi, %esi
	jne	.L816
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L817:
	subl	$1, %esi
	addq	$2, %rdi
	testl	%esi, %esi
	je	.L812
.L816:
	movzwl	(%rdi), %eax
	addl	$1, %r8d
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L817
	cmpl	$1, %esi
	je	.L817
	movzwl	2(%rdi), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L817
	subl	$2, %esi
	addq	$4, %rdi
	testl	%esi, %esi
	jne	.L816
.L812:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L837:
	movzwl	(%rdi), %eax
	leaq	2(%rdi), %rdx
	xorl	%r8d, %r8d
	testw	%ax, %ax
	je	.L812
	andl	$64512, %eax
	addl	$1, %r8d
	cmpl	$55296, %eax
	je	.L838
	.p2align 4,,10
	.p2align 3
.L823:
	movq	%rdx, %rdi
.L819:
	movzwl	(%rdi), %eax
	leaq	2(%rdi), %rdx
	testw	%ax, %ax
	je	.L812
	andl	$64512, %eax
	addl	$1, %r8d
	cmpl	$55296, %eax
	jne	.L823
.L838:
	movzwl	2(%rdi), %eax
	addq	$4, %rdi
	andl	$-1024, %eax
	cmpl	$56320, %eax
	cmovne	%rdx, %rdi
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L820:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2084:
	.size	u_countChar32_67, .-u_countChar32_67
	.p2align 4
	.globl	u_strHasMoreChar32Than_67
	.type	u_strHasMoreChar32Than_67, @function
u_strHasMoreChar32Than_67:
.LFB2085:
	.cfi_startproc
	endbr64
	movl	$1, %r8d
	testl	%edx, %edx
	js	.L839
	testq	%rdi, %rdi
	je	.L863
	cmpl	$-1, %esi
	jl	.L863
	je	.L871
	leal	1(%rsi), %eax
	sarl	%eax
	cmpl	%edx, %eax
	jg	.L839
	movl	%esi, %r9d
	xorl	%r8d, %r8d
	subl	%edx, %r9d
	testl	%r9d, %r9d
	jle	.L839
	movslq	%esi, %rsi
	leaq	(%rdi,%rsi,2), %rsi
	cmpq	%rsi, %rdi
	je	.L839
	testl	%edx, %edx
	je	.L861
	subl	$1, %edx
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L862:
	movq	%rcx, %rdi
.L846:
	movl	%edx, %eax
	cmpq	%rsi, %rdi
	je	.L863
.L848:
	subl	$1, %edx
	testl	%eax, %eax
	je	.L861
.L845:
	movzwl	(%rdi), %eax
	leaq	2(%rdi), %rcx
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L862
	cmpq	%rcx, %rsi
	je	.L862
	movzwl	2(%rdi), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L872
	movl	%edx, %eax
	movq	%rcx, %rdi
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L861:
	movl	$1, %r8d
.L839:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L872:
	addq	$4, %rdi
	subl	$1, %r9d
	jne	.L846
.L863:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L871:
	movzwl	(%rdi), %eax
	leaq	2(%rdi), %rcx
	testw	%ax, %ax
	je	.L863
	testl	%edx, %edx
	jne	.L842
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L854:
	movq	%rcx, %rdi
.L843:
	movzwl	(%rdi), %eax
	subl	$1, %edx
	leaq	2(%rdi), %rcx
	testw	%ax, %ax
	je	.L863
	testl	%edx, %edx
	je	.L861
.L842:
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L854
	movzwl	2(%rdi), %eax
	addq	$4, %rdi
	andl	$-1024, %eax
	cmpl	$56320, %eax
	cmovne	%rcx, %rdi
	jmp	.L843
	.cfi_endproc
.LFE2085:
	.size	u_strHasMoreChar32Than_67, .-u_strHasMoreChar32Than_67
	.p2align 4
	.globl	u_memcpy_67
	.type	u_memcpy_67, @function
u_memcpy_67:
.LFB2086:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jg	.L879
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	memcpy@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2086:
	.size	u_memcpy_67, .-u_memcpy_67
	.p2align 4
	.globl	u_memmove_67
	.type	u_memmove_67, @function
u_memmove_67:
.LFB2087:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jg	.L886
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	memmove@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2087:
	.size	u_memmove_67, .-u_memmove_67
	.p2align 4
	.globl	u_memset_67
	.type	u_memset_67, @function
u_memset_67:
.LFB2088:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testl	%edx, %edx
	jle	.L888
	movslq	%edx, %rdx
	leaq	(%rdi,%rdx,2), %rdi
	cmpq	%rdi, %rax
	jnb	.L888
	movq	%rax, %rcx
	movq	%rax, %rdx
	notq	%rcx
	addq	%rdi, %rcx
	movq	%rcx, %r8
	shrq	%r8
	addq	$1, %r8
	cmpq	$13, %rcx
	jbe	.L889
	movq	%r8, %rcx
	movd	%esi, %xmm0
	shrq	$3, %rcx
	punpcklwd	%xmm0, %xmm0
	salq	$4, %rcx
	pshufd	$0, %xmm0, %xmm0
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L890:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L890
	movq	%r8, %rcx
	andq	$-8, %rcx
	leaq	(%rax,%rcx,2), %rdx
	cmpq	%rcx, %r8
	je	.L894
.L889:
	leaq	2(%rdx), %rcx
	movw	%si, (%rdx)
	cmpq	%rcx, %rdi
	jbe	.L888
	leaq	4(%rdx), %rcx
	movw	%si, 2(%rdx)
	cmpq	%rcx, %rdi
	jbe	.L888
	leaq	6(%rdx), %rcx
	movw	%si, 4(%rdx)
	cmpq	%rcx, %rdi
	jbe	.L888
	leaq	8(%rdx), %rcx
	movw	%si, 6(%rdx)
	cmpq	%rcx, %rdi
	jbe	.L888
	leaq	10(%rdx), %rcx
	movw	%si, 8(%rdx)
	cmpq	%rcx, %rdi
	jbe	.L888
	leaq	12(%rdx), %rcx
	movw	%si, 10(%rdx)
	cmpq	%rcx, %rdi
	jbe	.L888
	movw	%si, 12(%rdx)
.L888:
	ret
	.p2align 4,,10
	.p2align 3
.L894:
	ret
	.cfi_endproc
.LFE2088:
	.size	u_memset_67, .-u_memset_67
	.p2align 4
	.globl	u_memcmp_67
	.type	u_memcmp_67, @function
u_memcmp_67:
.LFB2089:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L895
	movslq	%edx, %rdx
	leaq	(%rdi,%rdx,2), %rcx
	cmpq	%rcx, %rdi
	jb	.L897
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L901:
	addq	$2, %rdi
	addq	$2, %rsi
	cmpq	%rdi, %rcx
	jbe	.L895
.L897:
	movzwl	(%rdi), %eax
	movzwl	(%rsi), %edx
	subl	%edx, %eax
	je	.L901
.L895:
	ret
	.cfi_endproc
.LFE2089:
	.size	u_memcmp_67, .-u_memcmp_67
	.p2align 4
	.globl	u_memcmpCodePointOrder_67
	.type	u_memcmpCodePointOrder_67, @function
u_memcmpCodePointOrder_67:
.LFB2090:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jns	.L903
	cmpq	%rsi, %rdi
	je	.L922
	movzwl	(%rdi), %ecx
	movzwl	(%rsi), %r10d
	movq	%rsi, %r9
	movq	%rdi, %rdx
	cmpw	%cx, %r10w
	je	.L906
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L955:
	movzwl	2(%rdx), %ecx
	movzwl	2(%r9), %r10d
	addq	$2, %rdx
	addq	$2, %r9
	cmpw	%r10w, %cx
	jne	.L919
.L906:
	testw	%cx, %cx
	jne	.L955
.L922:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L903:
	movslq	%edx, %rdx
	leaq	(%rdx,%rdx), %rax
	leaq	(%rdi,%rax), %r11
	cmpq	%rsi, %rdi
	je	.L922
	cmpq	%r11, %rdi
	je	.L922
	movq	%rsi, %r9
	movq	%rdi, %rdx
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L956:
	addq	$2, %rdx
	addq	$2, %r9
	cmpq	%rdx, %r11
	je	.L922
.L908:
	movzwl	(%rdx), %ecx
	movzwl	(%r9), %r10d
	cmpw	%r10w, %cx
	je	.L956
	leaq	(%rsi,%rax), %r8
	cmpw	$-10241, %cx
	ja	.L951
.L954:
	movzwl	%cx, %eax
	movzwl	%r10w, %edx
.L909:
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L919:
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
	cmpw	$-10241, %cx
	jbe	.L954
.L951:
	cmpw	$-10241, %r10w
	jbe	.L954
	cmpw	$-9217, %cx
	ja	.L910
	leaq	2(%rdx), %rax
	cmpq	%rax, %r11
	je	.L911
	movzwl	2(%rdx), %edx
	movzwl	%cx, %eax
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L912
.L911:
	subw	$10240, %cx
	movzwl	%cx, %eax
.L912:
	cmpw	$-9217, %r10w
	ja	.L913
	leaq	2(%r9), %rdx
	cmpq	%rdx, %r8
	je	.L914
	movzwl	2(%r9), %ecx
	movzwl	%r10w, %edx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L909
.L914:
	subw	$10240, %r10w
	movzwl	%r10w, %edx
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L910:
	movl	%ecx, %r11d
	movzwl	%cx, %eax
	andl	$64512, %r11d
	cmpl	$56320, %r11d
	jne	.L911
	cmpq	%rdx, %rdi
	je	.L911
	movzwl	-2(%rdx), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L911
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L913:
	movl	%r10d, %ecx
	movzwl	%r10w, %edx
	andl	$64512, %ecx
	cmpl	$56320, %ecx
	jne	.L914
	cmpq	%r9, %rsi
	je	.L914
	movzwl	-2(%r9), %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L914
	jmp	.L909
	.cfi_endproc
.LFE2090:
	.size	u_memcmpCodePointOrder_67, .-u_memcmpCodePointOrder_67
	.p2align 4
	.globl	u_unescapeAt_67
	.type	u_unescapeAt_67, @function
u_unescapeAt_67:
.LFB2093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	movl	%eax, -76(%rbp)
	cmpl	%edx, %eax
	jge	.L958
	movq	%rdi, %r13
	movl	%eax, %edi
	testl	%eax, %eax
	js	.L958
	leal	1(%rax), %eax
	movq	%rcx, %r12
	movl	%eax, (%rsi)
	movq	%rcx, %rsi
	call	*%r13
	cmpw	$117, %ax
	je	.L959
	cmpw	$120, %ax
	je	.L960
	cmpw	$85, %ax
	je	.L999
	leal	-48(%rax), %edx
	cmpw	$7, %dx
	ja	.L965
	subl	$48, %eax
	movl	(%rbx), %edi
	movzbl	%al, %r15d
	cmpl	%edi, -68(%rbp)
	jle	.L957
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L967:
	movq	%r12, %rsi
	call	*%r13
	leal	-48(%rax), %edx
	cmpw	$7, %dx
	ja	.L973
	subl	$48, %eax
	leal	0(,%r15,8), %r8d
	addl	$1, %r14d
	movzbl	%al, %eax
	orl	%r8d, %eax
	cmpb	$2, %r14b
	movl	$2, %r14d
	movl	%eax, %r15d
	movl	(%rbx), %eax
	setg	%dl
	leal	1(%rax), %edi
	cmpl	-68(%rbp), %edi
	setge	%al
	movl	%edi, (%rbx)
	orb	%al, %dl
	je	.L967
.L973:
	cmpl	$1114111, %r15d
	ja	.L958
	movl	(%rbx), %edi
	cmpl	-68(%rbp), %edi
	jge	.L957
	movl	%r15d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L1000
	.p2align 4,,10
	.p2align 3
.L957:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1001
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	movb	$0, -71(%rbp)
	movl	(%rbx), %edi
	movb	$8, -69(%rbp)
	movb	$8, -70(%rbp)
.L964:
	cmpl	%edi, -68(%rbp)
	jle	.L958
.L981:
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1002:
	leal	-65(%rax), %ecx
	cmpw	$5, %cx
	jbe	.L969
	leal	-97(%rax), %ecx
	cmpw	$5, %cx
	ja	.L970
	leal	-87(%rax), %ecx
.L971:
	movl	(%rbx), %esi
	movl	%r15d, %r8d
	movsbl	%cl, %ecx
	addl	$1, %r14d
	sall	$4, %r8d
	orl	%r8d, %ecx
	leal	1(%rsi), %edi
	movl	%edi, (%rbx)
	movl	%ecx, %r15d
	cmpb	%r14b, -69(%rbp)
	jle	.L970
	cmpl	-68(%rbp), %edi
	jge	.L970
.L972:
	movq	%r12, %rsi
	call	*%r13
	leal	-48(%rax), %ecx
	cmpw	$9, %cx
	ja	.L1002
	leal	-48(%rax), %ecx
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L969:
	leal	-55(%rax), %ecx
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L970:
	cmpb	%r14b, -70(%rbp)
	jg	.L958
	cmpb	$0, -71(%rbp)
	je	.L973
	cmpw	$125, %ax
	jne	.L958
	addl	$1, (%rbx)
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L960:
	movl	(%rbx), %edi
	cmpl	-68(%rbp), %edi
	jge	.L958
	movq	%r12, %rsi
	call	*%r13
	cmpw	$123, %ax
	je	.L963
	movb	$0, -71(%rbp)
	movl	(%rbx), %edi
	movb	$2, -69(%rbp)
	movb	$1, -70(%rbp)
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1000:
	leal	1(%rdi), %eax
	movq	%r12, %rsi
	movl	%eax, -60(%rbp)
	call	*%r13
	cmpw	$92, %ax
	je	.L1003
.L974:
	movzwl	%ax, %edx
	andl	$64512, %eax
	cmpl	$56320, %eax
	jne	.L957
	movl	-60(%rbp), %eax
	sall	$10, %r15d
	leal	-56613888(%rdx,%r15), %r15d
	movl	%eax, (%rbx)
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L958:
	movl	-76(%rbp), %eax
	movl	$-1, %r15d
	movl	%eax, (%rbx)
	jmp	.L957
.L965:
	cmpw	$97, %ax
	je	.L983
	cmpw	$96, %ax
	jbe	.L977
	cmpw	$98, %ax
	je	.L984
	cmpw	$101, %ax
	je	.L985
	cmpw	$100, %ax
	jbe	.L977
	cmpw	$102, %ax
	je	.L986
	cmpw	$101, %ax
	je	.L978
	cmpw	$110, %ax
	je	.L987
	cmpw	$109, %ax
	jbe	.L978
	cmpw	$114, %ax
	je	.L988
	cmpw	$113, %ax
	jbe	.L978
	cmpw	$116, %ax
	je	.L989
	cmpw	$115, %ax
	jbe	.L978
	cmpw	$118, %ax
	je	.L1004
.L978:
	movzwl	%ax, %r14d
	movl	%r14d, %r15d
	.p2align 4,,10
	.p2align 3
.L979:
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L957
	movl	(%rbx), %edi
	cmpl	-68(%rbp), %edi
	jge	.L957
	movq	%r12, %rsi
	call	*%r13
	movzwl	%ax, %edx
	andl	$64512, %eax
	cmpl	$56320, %eax
	jne	.L957
	addl	$1, (%rbx)
	movl	%r14d, %ebx
	sall	$10, %ebx
	leal	-56613888(%rdx,%rbx), %r15d
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L977:
	movzwl	%ax, %r14d
	movl	%r14d, %r15d
	cmpw	$99, %ax
	jne	.L979
	movl	(%rbx), %edi
	movl	$99, %r15d
	cmpl	-68(%rbp), %edi
	jge	.L957
	leal	1(%rdi), %eax
	movq	%r12, %rsi
	movl	%eax, (%rbx)
	call	*%r13
	movl	%eax, %r15d
	movl	%r15d, %eax
	andl	$64512, %eax
	cmpl	$55296, %eax
	je	.L1005
.L980:
	andl	$31, %r15d
	jmp	.L957
.L963:
	movl	(%rbx), %eax
	movb	$1, -71(%rbp)
	movb	$8, -69(%rbp)
	leal	1(%rax), %edi
	movb	$1, -70(%rbp)
	movl	%edi, (%rbx)
	jmp	.L964
.L1003:
	movl	-68(%rbp), %eax
	cmpl	%eax, -60(%rbp)
	jge	.L957
	leaq	-60(%rbp), %rsi
	movq	%r12, %rcx
	movl	%eax, %edx
	movq	%r13, %rdi
	call	u_unescapeAt_67
	jmp	.L974
.L1005:
	movl	(%rbx), %edi
	cmpl	-68(%rbp), %edi
	jge	.L980
	movq	%r12, %rsi
	call	*%r13
	movl	%eax, %edx
	andl	$64512, %edx
	cmpl	$56320, %edx
	jne	.L980
	sall	$10, %r15d
	addl	$1, (%rbx)
	movl	%r15d, %r8d
	leal	9216(%rax,%r8), %r15d
	jmp	.L980
.L1001:
	call	__stack_chk_fail@PLT
.L986:
	movl	$6, %eax
.L976:
	addl	$1, %eax
	leaq	_ZL12UNESCAPE_MAP(%rip), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %r15d
	jmp	.L957
.L983:
	xorl	%eax, %eax
	jmp	.L976
.L985:
	movl	$4, %eax
	jmp	.L976
.L984:
	movl	$2, %eax
	jmp	.L976
.L1004:
	movl	$14, %eax
	jmp	.L976
.L989:
	movl	$12, %eax
	jmp	.L976
.L988:
	movl	$10, %eax
	jmp	.L976
.L987:
	movl	$8, %eax
	jmp	.L976
.L959:
	movl	(%rbx), %edi
	cmpl	%edi, -68(%rbp)
	jle	.L958
	movb	$4, -69(%rbp)
	movb	$4, -70(%rbp)
	movb	$0, -71(%rbp)
	jmp	.L981
	.cfi_endproc
.LFE2093:
	.size	u_unescapeAt_67, .-u_unescapeAt_67
	.p2align 4
	.globl	u_unescape_67
	.type	u_unescape_67, @function
u_unescape_67:
.LFB2096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-60(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rdi), %eax
	testb	%al, %al
	jne	.L1007
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1028:
	movq	%r14, %rdi
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L1009
.L1007:
	leaq	1(%rdi), %r14
	cmpb	$92, %al
	jne	.L1028
	movl	$0, -60(%rbp)
	cmpq	%r8, %rdi
	je	.L1011
	movq	%rdi, %rbx
	subq	%r8, %rbx
	testq	%r12, %r12
	je	.L1012
	movl	-68(%rbp), %edx
	movl	$0, %eax
	movq	%r8, %rdi
	subl	%r15d, %edx
	cmovs	%eax, %edx
	movslq	%r15d, %rax
	leaq	(%r12,%rax,2), %rsi
	cmpl	%ebx, %edx
	cmovg	%ebx, %edx
	call	u_charsToUChars_67@PLT
.L1012:
	addl	%ebx, %r15d
.L1011:
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	u_unescapeAt_67.constprop.0
	movl	%eax, %edx
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	je	.L1051
	movslq	%eax, %rdi
	addq	%r14, %rdi
	testq	%r12, %r12
	je	.L1017
	movl	-68(%rbp), %eax
	subl	%r15d, %eax
	cmpl	$65535, %edx
	ja	.L1052
	leal	1(%r15), %esi
	testl	%eax, %eax
	jle	.L1049
	movslq	%r15d, %r15
	movw	%dx, (%r12,%r15,2)
	movl	%esi, %r15d
	.p2align 4,,10
	.p2align 3
.L1020:
	movzbl	(%rdi), %eax
	movq	%rdi, %r8
	testb	%al, %al
	jne	.L1007
.L1009:
	movl	%r15d, %eax
	cmpq	%rdi, %r8
	je	.L1008
	movq	%rdi, %rbx
	subq	%r8, %rbx
	testq	%r12, %r12
	je	.L1023
	movl	-68(%rbp), %edx
	movl	$0, %eax
	movq	%r8, %rdi
	subl	%r15d, %edx
	cmovs	%eax, %edx
	movslq	%r15d, %rax
	leaq	(%r12,%rax,2), %rsi
	cmpl	%ebx, %edx
	cmovg	%ebx, %edx
	call	u_charsToUChars_67@PLT
.L1023:
	leal	(%r15,%rbx), %eax
.L1008:
	testq	%r12, %r12
	je	.L1006
	cmpl	-68(%rbp), %eax
	jge	.L1006
	movslq	%eax, %rdx
	xorl	%ecx, %ecx
	movw	%cx, (%r12,%rdx,2)
.L1006:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1053
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1052:
	.cfi_restore_state
	leal	2(%r15), %esi
	cmpl	$1, %eax
	jle	.L1049
	movl	%edx, %eax
	andw	$1023, %dx
	movslq	%r15d, %r15
	sarl	$10, %eax
	orw	$-9216, %dx
	subw	$10304, %ax
	movw	%dx, 2(%r12,%r15,2)
	movw	%ax, (%r12,%r15,2)
	movl	%esi, %r15d
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1049:
	movl	%esi, %r15d
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1017:
	movl	%r15d, %eax
	xorl	%r15d, %r15d
	cmpl	$65535, %edx
	seta	%r15b
	leal	1(%r15,%rax), %r15d
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1051:
	testq	%r12, %r12
	je	.L1006
	movl	-68(%rbp), %esi
	testl	%esi, %esi
	jle	.L1006
	xorl	%edx, %edx
	movw	%dx, (%r12)
	jmp	.L1006
.L1050:
	xorl	%eax, %eax
	jmp	.L1008
.L1053:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2096:
	.size	u_unescape_67, .-u_unescape_67
	.p2align 4
	.globl	u_asciiToUpper_67
	.type	u_asciiToUpper_67, @function
u_asciiToUpper_67:
.LFB2097:
	.cfi_startproc
	endbr64
	leal	-97(%rdi), %edx
	leal	-32(%rdi), %eax
	cmpw	$26, %dx
	cmovnb	%edi, %eax
	ret
	.cfi_endproc
.LFE2097:
	.size	u_asciiToUpper_67, .-u_asciiToUpper_67
	.p2align 4
	.globl	u_terminateUChars_67
	.type	u_terminateUChars_67, @function
u_terminateUChars_67:
.LFB2098:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	testq	%rcx, %rcx
	je	.L1058
	movl	(%rcx), %edx
	testl	%edx, %edx
	jg	.L1058
	testl	%eax, %eax
	jns	.L1062
.L1058:
	ret
	.p2align 4,,10
	.p2align 3
.L1062:
	cmpl	%esi, %eax
	jge	.L1060
	movslq	%eax, %rsi
	xorl	%r8d, %r8d
	movw	%r8w, (%rdi,%rsi,2)
	cmpl	$-124, %edx
	jne	.L1058
	movl	$0, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1060:
	movl	$-124, %esi
	movl	$15, %edx
	cmove	%esi, %edx
	movl	%edx, (%rcx)
	ret
	.cfi_endproc
.LFE2098:
	.size	u_terminateUChars_67, .-u_terminateUChars_67
	.p2align 4
	.globl	u_terminateChars_67
	.type	u_terminateChars_67, @function
u_terminateChars_67:
.LFB2099:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	testq	%rcx, %rcx
	je	.L1065
	movl	(%rcx), %edx
	testl	%edx, %edx
	jg	.L1065
	testl	%eax, %eax
	jns	.L1069
.L1065:
	ret
	.p2align 4,,10
	.p2align 3
.L1069:
	cmpl	%esi, %eax
	jge	.L1067
	movslq	%eax, %rdx
	movb	$0, (%rdi,%rdx)
	cmpl	$-124, (%rcx)
	jne	.L1065
	movl	$0, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1067:
	movl	$-124, %esi
	movl	$15, %edx
	cmove	%esi, %edx
	movl	%edx, (%rcx)
	ret
	.cfi_endproc
.LFE2099:
	.size	u_terminateChars_67, .-u_terminateChars_67
	.p2align 4
	.globl	u_terminateUChar32s_67
	.type	u_terminateUChar32s_67, @function
u_terminateUChar32s_67:
.LFB2100:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	testq	%rcx, %rcx
	je	.L1072
	movl	(%rcx), %edx
	testl	%edx, %edx
	jg	.L1072
	testl	%eax, %eax
	jns	.L1076
.L1072:
	ret
	.p2align 4,,10
	.p2align 3
.L1076:
	cmpl	%esi, %eax
	jge	.L1074
	movslq	%eax, %rsi
	movl	$0, (%rdi,%rsi,4)
	cmpl	$-124, %edx
	jne	.L1072
	movl	$0, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1074:
	movl	$-124, %esi
	movl	$15, %edx
	cmove	%esi, %edx
	movl	%edx, (%rcx)
	ret
	.cfi_endproc
.LFE2100:
	.size	u_terminateUChar32s_67, .-u_terminateUChar32s_67
	.p2align 4
	.globl	u_terminateWChars_67
	.type	u_terminateWChars_67, @function
u_terminateWChars_67:
.LFB2101:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	testq	%rcx, %rcx
	je	.L1079
	movl	(%rcx), %edx
	testl	%edx, %edx
	jg	.L1079
	testl	%eax, %eax
	jns	.L1083
.L1079:
	ret
	.p2align 4,,10
	.p2align 3
.L1083:
	cmpl	%esi, %eax
	jge	.L1081
	movslq	%eax, %rsi
	movl	$0, (%rdi,%rsi,4)
	cmpl	$-124, %edx
	jne	.L1079
	movl	$0, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L1081:
	movl	$-124, %esi
	movl	$15, %edx
	cmove	%esi, %edx
	movl	%edx, (%rcx)
	ret
	.cfi_endproc
.LFE2101:
	.size	u_terminateWChars_67, .-u_terminateWChars_67
	.p2align 4
	.globl	ustr_hashUCharsN_67
	.type	ustr_hashUCharsN_67, @function
ustr_hashUCharsN_67:
.LFB2102:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1084
	movl	%esi, %ecx
	leal	-1(%rsi), %edx
	movslq	%esi, %rsi
	subl	$32, %ecx
	leaq	(%rdi,%rsi,2), %rsi
	cmovns	%ecx, %edx
	sarl	$5, %edx
	addl	$1, %edx
	cmpq	%rsi, %rdi
	jnb	.L1084
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	.p2align 4,,10
	.p2align 3
.L1086:
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,4), %ecx
	movzwl	(%rdi), %eax
	addq	%rdx, %rdi
	addl	%ecx, %eax
	cmpq	%rdi, %rsi
	ja	.L1086
.L1084:
	ret
	.cfi_endproc
.LFE2102:
	.size	ustr_hashUCharsN_67, .-ustr_hashUCharsN_67
	.p2align 4
	.globl	ustr_hashCharsN_67
	.type	ustr_hashCharsN_67, @function
ustr_hashCharsN_67:
.LFB2103:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1091
	movl	%esi, %edx
	leal	-1(%rsi), %ecx
	movslq	%esi, %rsi
	subl	$32, %edx
	cmovns	%edx, %ecx
	leaq	(%rdi,%rsi), %rdx
	sarl	$5, %ecx
	addl	$1, %ecx
	cmpq	%rdx, %rdi
	jnb	.L1091
	movslq	%ecx, %rcx
	.p2align 4,,10
	.p2align 3
.L1093:
	leal	(%rax,%rax,8), %esi
	leal	(%rax,%rsi,4), %esi
	movzbl	(%rdi), %eax
	addq	%rcx, %rdi
	addl	%esi, %eax
	cmpq	%rdi, %rdx
	ja	.L1093
.L1091:
	ret
	.cfi_endproc
.LFE2103:
	.size	ustr_hashCharsN_67, .-ustr_hashCharsN_67
	.p2align 4
	.globl	ustr_hashICharsN_67
	.type	ustr_hashICharsN_67, @function
ustr_hashICharsN_67:
.LFB2104:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1106
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	subl	$32, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leal	-1(%rsi), %r12d
	movslq	%esi, %rsi
	cmovns	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	(%rdi,%rsi), %rbx
	sarl	$5, %r12d
	addl	$1, %r12d
	cmpq	%rbx, %rdi
	jnb	.L1098
	movslq	%r12d, %r12
	.p2align 4,,10
	.p2align 3
.L1100:
	movsbl	(%r14), %edi
	leal	(%rax,%rax,8), %edx
	addq	%r12, %r14
	leal	(%rax,%rdx,4), %r13d
	call	uprv_asciitolower_67@PLT
	movzbl	%al, %eax
	addl	%r13d, %eax
	cmpq	%r14, %rbx
	ja	.L1100
.L1098:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE2104:
	.size	ustr_hashICharsN_67, .-ustr_hashICharsN_67
	.section	.rodata
	.align 32
	.type	_ZL12UNESCAPE_MAP, @object
	.size	_ZL12UNESCAPE_MAP, 32
_ZL12UNESCAPE_MAP:
	.value	97
	.value	7
	.value	98
	.value	8
	.value	101
	.value	27
	.value	102
	.value	12
	.value	110
	.value	10
	.value	114
	.value	13
	.value	116
	.value	9
	.value	118
	.value	11
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
