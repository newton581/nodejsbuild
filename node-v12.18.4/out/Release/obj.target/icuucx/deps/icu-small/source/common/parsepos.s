	.file	"parsepos.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713ParsePosition17getDynamicClassIDEv
	.type	_ZNK6icu_6713ParsePosition17getDynamicClassIDEv, @function
_ZNK6icu_6713ParsePosition17getDynamicClassIDEv:
.LFB27:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713ParsePosition16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE27:
	.size	_ZNK6icu_6713ParsePosition17getDynamicClassIDEv, .-_ZNK6icu_6713ParsePosition17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ParsePositionD2Ev
	.type	_ZN6icu_6713ParsePositionD2Ev, @function
_ZN6icu_6713ParsePositionD2Ev:
.LFB29:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE29:
	.size	_ZN6icu_6713ParsePositionD2Ev, .-_ZN6icu_6713ParsePositionD2Ev
	.globl	_ZN6icu_6713ParsePositionD1Ev
	.set	_ZN6icu_6713ParsePositionD1Ev,_ZN6icu_6713ParsePositionD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ParsePositionD0Ev
	.type	_ZN6icu_6713ParsePositionD0Ev, @function
_ZN6icu_6713ParsePositionD0Ev:
.LFB31:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE31:
	.size	_ZN6icu_6713ParsePositionD0Ev, .-_ZN6icu_6713ParsePositionD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ParsePosition16getStaticClassIDEv
	.type	_ZN6icu_6713ParsePosition16getStaticClassIDEv, @function
_ZN6icu_6713ParsePosition16getStaticClassIDEv:
.LFB26:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713ParsePosition16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE26:
	.size	_ZN6icu_6713ParsePosition16getStaticClassIDEv, .-_ZN6icu_6713ParsePosition16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713ParsePosition5cloneEv
	.type	_ZNK6icu_6713ParsePosition5cloneEv, @function
_ZNK6icu_6713ParsePosition5cloneEv:
.LFB32:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$16, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L7
	movq	8(%rbx), %rdx
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
.L7:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE32:
	.size	_ZNK6icu_6713ParsePosition5cloneEv, .-_ZNK6icu_6713ParsePosition5cloneEv
	.weak	_ZTSN6icu_6713ParsePositionE
	.section	.rodata._ZTSN6icu_6713ParsePositionE,"aG",@progbits,_ZTSN6icu_6713ParsePositionE,comdat
	.align 16
	.type	_ZTSN6icu_6713ParsePositionE, @object
	.size	_ZTSN6icu_6713ParsePositionE, 25
_ZTSN6icu_6713ParsePositionE:
	.string	"N6icu_6713ParsePositionE"
	.weak	_ZTIN6icu_6713ParsePositionE
	.section	.data.rel.ro._ZTIN6icu_6713ParsePositionE,"awG",@progbits,_ZTIN6icu_6713ParsePositionE,comdat
	.align 8
	.type	_ZTIN6icu_6713ParsePositionE, @object
	.size	_ZTIN6icu_6713ParsePositionE, 24
_ZTIN6icu_6713ParsePositionE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713ParsePositionE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6713ParsePositionE
	.section	.data.rel.ro.local._ZTVN6icu_6713ParsePositionE,"awG",@progbits,_ZTVN6icu_6713ParsePositionE,comdat
	.align 8
	.type	_ZTVN6icu_6713ParsePositionE, @object
	.size	_ZTVN6icu_6713ParsePositionE, 40
_ZTVN6icu_6713ParsePositionE:
	.quad	0
	.quad	_ZTIN6icu_6713ParsePositionE
	.quad	_ZN6icu_6713ParsePositionD1Ev
	.quad	_ZN6icu_6713ParsePositionD0Ev
	.quad	_ZNK6icu_6713ParsePosition17getDynamicClassIDEv
	.local	_ZZN6icu_6713ParsePosition16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6713ParsePosition16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
