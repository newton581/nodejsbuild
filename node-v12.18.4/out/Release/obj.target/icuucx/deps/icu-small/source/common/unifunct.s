	.file	"unifunct.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714UnicodeFunctor9toMatcherEv
	.type	_ZNK6icu_6714UnicodeFunctor9toMatcherEv, @function
_ZNK6icu_6714UnicodeFunctor9toMatcherEv:
.LFB9:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE9:
	.size	_ZNK6icu_6714UnicodeFunctor9toMatcherEv, .-_ZNK6icu_6714UnicodeFunctor9toMatcherEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714UnicodeFunctor10toReplacerEv
	.type	_ZNK6icu_6714UnicodeFunctor10toReplacerEv, @function
_ZNK6icu_6714UnicodeFunctor10toReplacerEv:
.LFB10:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10:
	.size	_ZNK6icu_6714UnicodeFunctor10toReplacerEv, .-_ZNK6icu_6714UnicodeFunctor10toReplacerEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714UnicodeFunctor16getStaticClassIDEv
	.type	_ZN6icu_6714UnicodeFunctor16getStaticClassIDEv, @function
_ZN6icu_6714UnicodeFunctor16getStaticClassIDEv:
.LFB4:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6714UnicodeFunctor16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE4:
	.size	_ZN6icu_6714UnicodeFunctor16getStaticClassIDEv, .-_ZN6icu_6714UnicodeFunctor16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714UnicodeFunctorD2Ev
	.type	_ZN6icu_6714UnicodeFunctorD2Ev, @function
_ZN6icu_6714UnicodeFunctorD2Ev:
.LFB6:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714UnicodeFunctorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE6:
	.size	_ZN6icu_6714UnicodeFunctorD2Ev, .-_ZN6icu_6714UnicodeFunctorD2Ev
	.globl	_ZN6icu_6714UnicodeFunctorD1Ev
	.set	_ZN6icu_6714UnicodeFunctorD1Ev,_ZN6icu_6714UnicodeFunctorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714UnicodeFunctorD0Ev
	.type	_ZN6icu_6714UnicodeFunctorD0Ev, @function
_ZN6icu_6714UnicodeFunctorD0Ev:
.LFB8:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714UnicodeFunctorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE8:
	.size	_ZN6icu_6714UnicodeFunctorD0Ev, .-_ZN6icu_6714UnicodeFunctorD0Ev
	.weak	_ZTSN6icu_6714UnicodeFunctorE
	.section	.rodata._ZTSN6icu_6714UnicodeFunctorE,"aG",@progbits,_ZTSN6icu_6714UnicodeFunctorE,comdat
	.align 16
	.type	_ZTSN6icu_6714UnicodeFunctorE, @object
	.size	_ZTSN6icu_6714UnicodeFunctorE, 26
_ZTSN6icu_6714UnicodeFunctorE:
	.string	"N6icu_6714UnicodeFunctorE"
	.weak	_ZTIN6icu_6714UnicodeFunctorE
	.section	.data.rel.ro._ZTIN6icu_6714UnicodeFunctorE,"awG",@progbits,_ZTIN6icu_6714UnicodeFunctorE,comdat
	.align 8
	.type	_ZTIN6icu_6714UnicodeFunctorE, @object
	.size	_ZTIN6icu_6714UnicodeFunctorE, 24
_ZTIN6icu_6714UnicodeFunctorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714UnicodeFunctorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6714UnicodeFunctorE
	.section	.data.rel.ro._ZTVN6icu_6714UnicodeFunctorE,"awG",@progbits,_ZTVN6icu_6714UnicodeFunctorE,comdat
	.align 8
	.type	_ZTVN6icu_6714UnicodeFunctorE, @object
	.size	_ZTVN6icu_6714UnicodeFunctorE, 72
_ZTVN6icu_6714UnicodeFunctorE:
	.quad	0
	.quad	_ZTIN6icu_6714UnicodeFunctorE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6714UnicodeFunctor9toMatcherEv
	.quad	_ZNK6icu_6714UnicodeFunctor10toReplacerEv
	.quad	__cxa_pure_virtual
	.local	_ZZN6icu_6714UnicodeFunctor16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6714UnicodeFunctor16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
