	.file	"uloc_keytype.cpp"
	.text
	.p2align 4
	.type	uloc_key_type_cleanup, @function
uloc_key_type_cleanup:
.LFB3363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	_ZL13gLocExtKeyMap(%rip), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	uhash_close_67@PLT
	movq	$0, _ZL13gLocExtKeyMap(%rip)
.L2:
	movq	_ZL21gLocExtKeyDataEntries(%rip), %r12
	testq	%r12, %r12
	je	.L3
	movl	(%r12), %eax
	movq	8(%r12), %rdi
	testl	%eax, %eax
	jle	.L4
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%rdi,%rbx,8), %r13
	testq	%r13, %r13
	je	.L5
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L6
	call	uhash_close_67@PLT
.L6:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	(%r12), %eax
	movq	8(%r12), %rdi
.L5:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L7
.L4:
	cmpb	$0, 20(%r12)
	jne	.L47
.L8:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L3:
	movq	_ZL18gLocExtTypeEntries(%rip), %r12
	movq	$0, _ZL21gLocExtKeyDataEntries(%rip)
	testq	%r12, %r12
	je	.L9
	movl	(%r12), %eax
	movq	8(%r12), %r8
	testl	%eax, %eax
	jle	.L10
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	(%r12), %eax
	addq	$1, %rbx
	movq	8(%r12), %r8
	cmpl	%ebx, %eax
	jg	.L14
.L10:
	cmpb	$0, 20(%r12)
	jne	.L48
.L15:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L9:
	movq	_ZL18gKeyTypeStringPool(%rip), %r12
	movq	$0, _ZL18gLocExtTypeEntries(%rip)
	testq	%r12, %r12
	je	.L16
	movl	(%r12), %eax
	movq	8(%r12), %rdi
	testl	%eax, %eax
	jle	.L17
	xorl	%ebx, %ebx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	(%r12), %eax
	movq	8(%r12), %rdi
.L18:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L17
.L20:
	movq	(%rdi,%rbx,8), %r13
	testq	%r13, %r13
	je	.L18
	cmpb	$0, 12(%r13)
	je	.L19
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L17:
	cmpb	$0, 20(%r12)
	jne	.L49
.L21:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L16:
	movq	$0, _ZL18gKeyTypeStringPool(%rip)
	movl	$1, %eax
	movl	$0, _ZL21gLocExtKeyMapInitOnce(%rip)
	mfence
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L14
	cmpb	$0, 20(%r12)
	je	.L15
.L48:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L49:
	call	uprv_free_67@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L47:
	call	uprv_free_67@PLT
	jmp	.L8
	.cfi_endproc
.LFE3363:
	.size	uloc_key_type_cleanup, .-uloc_key_type_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"keyTypeData"
.LC1:
	.string	"keyMap"
.LC2:
	.string	"typeMap"
.LC3:
	.string	"typeAlias"
.LC4:
	.string	"bcpTypeAlias"
.LC5:
	.string	"timezone"
.LC6:
	.string	"CODEPOINTS"
.LC7:
	.string	"REORDER_CODE"
.LC8:
	.string	"RG_KEY_VALUE"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB9:
	.text
.LHOTB9:
	.p2align 4
	.type	_ZL22initFromResourceBundleR10UErrorCode, @function
_ZL22initFromResourceBundleR10UErrorCode:
.LFB3364:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	uloc_key_type_cleanup(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$5, %edi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	ucln_common_registerCleanup_67@PLT
	movq	uhash_compareIChars_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashIChars_67@GOTPCREL(%rip), %rdi
	xorl	%edx, %edx
	movq	%r15, %rcx
	call	uhash_open_67@PLT
	movq	%r15, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	movq	%rax, _ZL13gLocExtKeyMap(%rip)
	call	ures_openDirect_67@PLT
	movq	%r15, %rcx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -392(%rbp)
	call	ures_getByKey_67@PLT
	movq	%r15, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, -360(%rbp)
	call	ures_getByKey_67@PLT
	movl	(%r15), %esi
	movq	%rax, -384(%rbp)
	testl	%esi, %esi
	jle	.L341
.L52:
	movq	-384(%rbp), %rax
	testq	%rax, %rax
	je	.L170
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L170:
	movq	-360(%rbp), %rax
	testq	%rax, %rax
	je	.L171
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L171:
	movq	-392(%rbp), %rax
	testq	%rax, %rax
	je	.L50
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L50:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L342
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L341:
	.cfi_restore_state
	movq	-392(%rbp), %r14
	leaq	-216(%rbp), %rbx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rcx
	movq	%rbx, -344(%rbp)
	movl	$0, -216(%rbp)
	movq	%r14, %rdi
	call	ures_getByKey_67@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rbx, %rcx
	leaq	.LC4(%rip), %rsi
	movq	%rax, -368(%rbp)
	movl	$0, -216(%rbp)
	call	ures_getByKey_67@PLT
	movl	$88, %edi
	movq	%rax, -376(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L53
	leaq	24(%rax), %rdx
	movl	$0, (%rax)
	movl	$88, %edi
	movq	%rdx, 8(%rax)
	movl	$8, 16(%rax)
	movb	$0, 20(%rax)
	movq	%rax, _ZL18gKeyTypeStringPool(%rip)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L54
	leaq	24(%rax), %rdx
	movl	$0, (%rax)
	movl	$88, %edi
	movq	%rdx, 8(%rax)
	movl	$8, 16(%rax)
	movb	$0, 20(%rax)
	movq	%rax, _ZL21gLocExtKeyDataEntries(%rip)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L55
	leaq	24(%rax), %rdx
	movl	$0, (%rax)
	movq	%rdx, 8(%rax)
	movl	$8, 16(%rax)
	movb	$0, 20(%rax)
	movq	%rax, _ZL18gLocExtTypeEntries(%rip)
	movq	$0, -336(%rbp)
	.p2align 4,,10
	.p2align 3
.L158:
	movq	-360(%rbp), %rbx
	movq	%rbx, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L56
	movq	-336(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	call	ures_getNextResource_67@PLT
	movl	(%r15), %ecx
	movq	%rax, -336(%rbp)
	movq	%rax, %rbx
	testl	%ecx, %ecx
	jg	.L56
	movq	%rax, %rdi
	call	ures_getKey_67@PLT
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movl	$0, -212(%rbp)
	movq	%rax, -312(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -192(%rbp)
	movl	$2, %eax
	movw	%ax, -184(%rbp)
	leaq	-212(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -248(%rbp)
	call	ures_getString_67@PLT
	movl	(%r15), %edx
	testl	%edx, %edx
	jle	.L343
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L59:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L61
	movswl	-184(%rbp), %eax
	shrl	$5, %eax
	je	.L186
	movq	_ZL18gKeyTypeStringPool(%rip), %r12
	movl	16(%r12), %ebx
	cmpl	(%r12), %ebx
	jne	.L63
	cmpl	$8, %ebx
	je	.L64
	leal	(%rbx,%rbx), %r14d
	testl	%r14d, %r14d
	jg	.L344
.L177:
	movl	$7, (%r15)
.L61:
	movq	-328(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L56:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L178
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L178:
	movq	-376(%rbp), %rax
	testq	%rax, %rax
	je	.L168
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L168:
	movq	-368(%rbp), %rax
	testq	%rax, %rax
	je	.L52
	movq	%rax, %rdi
	call	ures_close_67@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-312(%rbp), %rax
	movq	%rax, -320(%rbp)
.L62:
	movq	-312(%rbp), %rsi
	movl	$9, %ecx
	leaq	.LC5(%rip), %rdi
	repz cmpsb
	movq	uhash_compareIChars_67@GOTPCREL(%rip), %rsi
	movq	uhash_hashIChars_67@GOTPCREL(%rip), %rdi
	movq	%r15, %rcx
	seta	%al
	sbbb	$0, %al
	xorl	%edx, %edx
	movsbl	%al, %eax
	movl	%eax, -280(%rbp)
	call	uhash_open_67@PLT
	movl	(%r15), %r13d
	movq	%rax, -264(%rbp)
	testl	%r13d, %r13d
	jg	.L61
	cmpq	$0, -368(%rbp)
	je	.L73
	movq	-344(%rbp), %rcx
	movq	-312(%rbp), %rsi
	xorl	%edx, %edx
	movl	$0, -216(%rbp)
	movq	-368(%rbp), %rdi
	call	ures_getByKey_67@PLT
	movl	-216(%rbp), %r12d
	movq	%rax, %r14
	testl	%r12d, %r12d
	jle	.L72
.L73:
	xorl	%r14d, %r14d
.L72:
	cmpq	$0, -376(%rbp)
	je	.L76
	movq	-344(%rbp), %rcx
	movq	-320(%rbp), %rsi
	xorl	%edx, %edx
	movl	$0, -216(%rbp)
	movq	-376(%rbp), %rdi
	call	ures_getByKey_67@PLT
	movl	-216(%rbp), %ebx
	movq	%rax, -304(%rbp)
	testl	%ebx, %ebx
	jle	.L75
.L76:
	movq	$0, -304(%rbp)
.L75:
	movq	-344(%rbp), %rcx
	movq	-312(%rbp), %rsi
	xorl	%edx, %edx
	movl	$0, -216(%rbp)
	movq	-384(%rbp), %rdi
	call	ures_getByKey_67@PLT
	movl	-216(%rbp), %r11d
	movq	%rax, -272(%rbp)
	testl	%r11d, %r11d
	jg	.L296
	leaq	-208(%rbp), %rax
	movq	$0, -232(%rbp)
	movl	$0, -276(%rbp)
	movq	%rax, -256(%rbp)
	.p2align 4,,10
	.p2align 3
.L77:
	movq	-272(%rbp), %rbx
	movq	%rbx, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L78
	movq	-232(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	ures_getNextResource_67@PLT
	movl	(%r15), %r10d
	movq	%rax, -232(%rbp)
	testl	%r10d, %r10d
	jg	.L78
	movq	%rax, %rdi
	call	ures_getKey_67@PLT
	movl	$11, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L345
	movl	$13, %ecx
	movq	%r12, %rsi
	leaq	.LC7(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L346
	movl	$13, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L347
	movl	-280(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L85
	movl	$58, %esi
	movq	%r12, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L348
.L85:
	movq	-248(%rbp), %rsi
	movq	-232(%rbp), %rdi
	movl	$2, %ecx
	movq	%r15, %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -120(%rbp)
	movq	%rax, -128(%rbp)
	movl	$0, -212(%rbp)
	call	ures_getString_67@PLT
	movl	(%r15), %esi
	testl	%esi, %esi
	jle	.L349
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
.L98:
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L108
	movswl	-120(%rbp), %eax
	shrl	$5, %eax
	je	.L188
	movq	_ZL18gKeyTypeStringPool(%rip), %r13
	movl	16(%r13), %ebx
	cmpl	0(%r13), %ebx
	jne	.L101
	cmpl	$8, %ebx
	je	.L102
	leal	(%rbx,%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L350
.L175:
	movl	$7, (%r15)
.L108:
	movq	-288(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L78:
	movq	-232(%rbp), %rax
	testq	%rax, %rax
	je	.L144
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L144:
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L154
	movq	_ZL21gLocExtKeyDataEntries(%rip), %r13
	movl	16(%r13), %ebx
	cmpl	0(%r13), %ebx
	jne	.L146
	cmpl	$8, %ebx
	je	.L147
	leal	(%rbx,%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L351
.L173:
	movl	$7, (%r15)
.L154:
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L159
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L159:
	movq	-304(%rbp), %rax
	testq	%rax, %rax
	je	.L160
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L160:
	testq	%r14, %r14
	je	.L61
	movq	%r14, %rdi
	call	ures_close_67@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L345:
	orl	$1, -276(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L346:
	orl	$2, -276(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L347:
	orl	$4, -276(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%r12, -240(%rbp)
.L100:
	movq	_ZL18gLocExtTypeEntries(%rip), %r13
	movl	16(%r13), %ebx
	cmpl	0(%r13), %ebx
	jne	.L109
	cmpl	$8, %ebx
	je	.L110
	leal	(%rbx,%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L175
	movslq	%r8d, %rdi
	movl	%r8d, -296(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L175
	testl	%ebx, %ebx
	movl	-296(%rbp), %r8d
	jg	.L181
	cmpb	$0, 20(%r13)
	jne	.L352
.L112:
	movq	%rcx, 8(%r13)
	movl	%r8d, 16(%r13)
	movb	$1, 20(%r13)
.L109:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L106
	movslq	0(%r13), %rax
	movq	%r12, %xmm0
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	-264(%rbp), %rdi
	leal	1(%rax), %edx
	movl	%edx, 0(%r13)
	movq	8(%r13), %rdx
	movq	-240(%rbp), %r13
	movq	%rbx, (%rdx,%rax,8)
	movq	%rbx, %rdx
	movq	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	uhash_put_67@PLT
	cmpq	%r12, %r13
	je	.L114
	movq	-264(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	uhash_put_67@PLT
.L114:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L108
	testq	%r14, %r14
	jne	.L353
.L134:
	movq	-304(%rbp), %r13
	testq	%r13, %r13
	jne	.L354
.L142:
	movq	-288(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L354:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	ures_resetIterator_67@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%r13, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L136
.L355:
	movl	(%r15), %edi
	testl	%edi, %edi
	jg	.L141
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	ures_getNextResource_67@PLT
	movq	-256(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getString_67@PLT
	movl	(%r15), %esi
	movq	%rax, %rcx
	testl	%esi, %esi
	jg	.L141
	movl	-208(%rbp), %r8d
	movq	-240(%rbp), %rsi
	xorl	%edi, %edi
	movl	$-1, %edx
	call	uprv_compareInvAscii_67@PLT
	testl	%eax, %eax
	jne	.L140
	movq	%r12, %rdi
	call	ures_getKey_67@PLT
	movq	-264(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%rax, %rsi
	call	uhash_put_67@PLT
	movq	%r13, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	jne	.L355
.L136:
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L141
	testq	%r12, %r12
	je	.L142
	movq	%r12, %rdi
	call	ures_close_67@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%rax, -208(%rbp)
	movl	-212(%rbp), %ecx
	leaq	-128(%rbp), %rax
	movl	$1, %esi
	movq	-256(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -288(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L175
	movl	$32, %r8d
.L182:
	cmpl	%ebx, 16(%r13)
	cmovle	16(%r13), %ebx
	movq	%rcx, %rdi
	movl	%r8d, -240(%rbp)
	movq	8(%r13), %rsi
	cmpl	%r8d, %ebx
	cmovg	%r8d, %ebx
	movslq	%ebx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 20(%r13)
	movl	-240(%rbp), %r8d
	movq	%rax, %rcx
	jne	.L356
.L105:
	movq	%rcx, 8(%r13)
	movl	%r8d, 16(%r13)
	movb	$1, 20(%r13)
.L101:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L106
	leaq	13(%rax), %rax
	movl	$0, 56(%rbx)
	movq	%rbx, %rdi
	movq	-288(%rbp), %rsi
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	movw	%ax, 12(%rbx)
	movl	$40, 8(%rbx)
	movslq	0(%r13), %rax
	leal	1(%rax), %edx
	movl	%edx, 0(%r13)
	movq	8(%r13), %rdx
	movq	%rbx, (%rdx,%rax,8)
	movq	%r15, %rdx
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L108
	movq	(%rbx), %rax
	movq	%rax, -240(%rbp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L353:
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	ures_resetIterator_67@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%r14, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	je	.L357
.L116:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L119
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	ures_getNextResource_67@PLT
	movq	-248(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	ures_getString_67@PLT
	movl	(%r15), %r11d
	movq	%rax, %rcx
	testl	%r11d, %r11d
	jg	.L119
	movl	-212(%rbp), %r8d
	xorl	%edi, %edi
	movl	$-1, %edx
	movq	%r12, %rsi
	call	uprv_compareInvAscii_67@PLT
	testl	%eax, %eax
	jne	.L133
	movq	%r13, %rdi
	call	ures_getKey_67@PLT
	movl	-280(%rbp), %r10d
	movq	%rax, %r8
	testl	%r10d, %r10d
	jne	.L122
	movl	$58, %esi
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	strchr@PLT
	movq	-296(%rbp), %r8
	testq	%rax, %rax
	jne	.L358
.L122:
	movq	-264(%rbp), %rdi
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	uhash_put_67@PLT
	movq	%r14, %rdi
	call	ures_hasNext_67@PLT
	testb	%al, %al
	jne	.L116
.L357:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L119
	testq	%r13, %r13
	je	.L134
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L358:
	movq	_ZL18gKeyTypeStringPool(%rip), %r9
	movl	16(%r9), %edx
	cmpl	(%r9), %edx
	jne	.L123
	cmpl	$8, %edx
	je	.L124
	leal	(%rdx,%rdx), %r10d
	testl	%r10d, %r10d
	jg	.L359
.L174:
	movl	$7, (%r15)
	.p2align 4,,10
	.p2align 3
.L119:
	testq	%r13, %r13
	je	.L108
	movq	%r13, %rdi
	call	ures_close_67@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L348:
	movq	_ZL18gKeyTypeStringPool(%rip), %r13
	movl	16(%r13), %ebx
	cmpl	0(%r13), %ebx
	jne	.L86
	cmpl	$8, %ebx
	je	.L87
	leal	(%rbx,%rbx), %r8d
	testl	%r8d, %r8d
	jg	.L360
.L176:
	movl	$7, (%r15)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$256, %edi
	movq	%r9, -400(%rbp)
	movl	%edx, -352(%rbp)
	movq	%r8, -296(%rbp)
	call	uprv_malloc_67@PLT
	movq	-296(%rbp), %r8
	movl	-352(%rbp), %edx
	testq	%rax, %rax
	movq	-400(%rbp), %r9
	movq	%rax, %rcx
	je	.L174
	movl	$32, %r10d
.L180:
	cmpl	%edx, 16(%r9)
	cmovle	16(%r9), %edx
	movq	%rcx, %rdi
	movq	%r8, -400(%rbp)
	movq	8(%r9), %rsi
	movl	%r10d, -352(%rbp)
	cmpl	%r10d, %edx
	movq	%r9, -296(%rbp)
	cmovg	%r10d, %edx
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movq	-296(%rbp), %r9
	movq	-400(%rbp), %r8
	movl	-352(%rbp), %r10d
	movq	%rax, %rcx
	cmpb	$0, 20(%r9)
	jne	.L361
.L127:
	movq	%rcx, 8(%r9)
	movl	%r10d, 16(%r9)
	movb	$1, 20(%r9)
.L123:
	movq	-256(%rbp), %rdi
	movq	%r8, %rsi
	movq	%r9, -296(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-296(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L128
	leaq	13(%rax), %rax
	xorl	%r8d, %r8d
	movl	-200(%rbp), %edx
	movq	%r15, %rcx
	movq	-208(%rbp), %rsi
	movq	%rax, (%rdi)
	movl	$0, 56(%rdi)
	movl	$40, 8(%rdi)
	movw	%r8w, 12(%rdi)
	movq	%r9, -352(%rbp)
	movq	%rdi, -296(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-352(%rbp), %r9
	movq	-296(%rbp), %rdi
	movslq	(%r9), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r9)
	movq	8(%r9), %rdx
	movl	(%r15), %r9d
	movq	%rdi, (%rdx,%rax,8)
	testl	%r9d, %r9d
	jg	.L119
	movq	(%rdi), %r8
	movslq	56(%rdi), %rax
	addq	%r8, %rax
	cmpq	%r8, %rax
	je	.L122
	.p2align 4,,10
	.p2align 3
.L132:
	cmpb	$58, (%r8)
	jne	.L131
	movb	$47, (%r8)
.L131:
	addq	$1, %r8
	cmpq	%r8, %rax
	jne	.L132
	movq	(%rdi), %r8
	jmp	.L122
.L147:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L173
	movl	$32, %ecx
.L179:
	cmpl	%ebx, 16(%r13)
	cmovle	16(%r13), %ebx
	movq	%r12, %rdi
	movl	%ecx, -232(%rbp)
	movq	8(%r13), %rsi
	cmpl	%ebx, %ecx
	cmovle	%ecx, %ebx
	movslq	%ebx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 20(%r13)
	movl	-232(%rbp), %ecx
	jne	.L362
.L150:
	movq	%r12, 8(%r13)
	movl	%ecx, 16(%r13)
	movb	$1, 20(%r13)
.L146:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	movslq	0(%r13), %rax
	leal	1(%rax), %edx
	movl	%edx, 0(%r13)
	movq	8(%r13), %rdx
	testq	%r12, %r12
	je	.L151
	movl	-276(%rbp), %ebx
	movq	%r12, (%rdx,%rax,8)
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %r13
	movl	%ebx, 24(%r12)
	movq	-264(%rbp), %rbx
	movq	%rax, %xmm0
	movq	%r13, %xmm2
	movq	_ZL13gLocExtKeyMap(%rip), %rdi
	movq	%rax, %rsi
	movq	%rbx, 16(%r12)
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, %rbx
	movups	%xmm0, (%r12)
	call	uhash_put_67@PLT
	cmpq	%rbx, %r13
	je	.L153
	movq	-320(%rbp), %rsi
	movq	_ZL13gLocExtKeyMap(%rip), %rdi
	movq	%r15, %rcx
	movq	%r12, %rdx
	call	uhash_put_67@PLT
.L153:
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L154
	movq	-272(%rbp), %rax
	testq	%rax, %rax
	je	.L155
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L155:
	movq	-304(%rbp), %rax
	testq	%rax, %rax
	je	.L156
	movq	%rax, %rdi
	call	ures_close_67@PLT
.L156:
	testq	%r14, %r14
	je	.L157
	movq	%r14, %rdi
	call	ures_close_67@PLT
.L157:
	movq	-328(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L175
	movl	$32, %r8d
.L181:
	cmpl	%ebx, 16(%r13)
	cmovle	16(%r13), %ebx
	movq	%rcx, %rdi
	movl	%r8d, -296(%rbp)
	movq	8(%r13), %rsi
	cmpl	%ebx, %r8d
	cmovle	%r8d, %ebx
	movslq	%ebx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 20(%r13)
	movl	-296(%rbp), %r8d
	movq	%rax, %rcx
	je	.L112
.L352:
	movq	8(%r13), %rdi
	movq	%rcx, -352(%rbp)
	movl	%r8d, -296(%rbp)
	call	uprv_free_67@PLT
	movq	-352(%rbp), %rcx
	movl	-296(%rbp), %r8d
	jmp	.L112
.L87:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L176
	movl	$32, %r8d
.L183:
	cmpl	%ebx, 16(%r13)
	cmovle	16(%r13), %ebx
	movq	%rcx, %rdi
	movl	%r8d, -240(%rbp)
	movq	8(%r13), %rsi
	cmpl	%r8d, %ebx
	cmovg	%r8d, %ebx
	movslq	%ebx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 20(%r13)
	movl	-240(%rbp), %r8d
	movq	%rax, %rcx
	jne	.L363
.L90:
	movq	%rcx, 8(%r13)
	movl	%r8d, 16(%r13)
	movb	$1, 20(%r13)
.L86:
	movq	-256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L91
	leaq	13(%rax), %rax
	xorl	%edi, %edi
	movl	-200(%rbp), %edx
	movq	%r15, %rcx
	movq	-208(%rbp), %rsi
	movw	%di, 12(%rbx)
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movl	$0, 56(%rbx)
	movl	$40, 8(%rbx)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movslq	0(%r13), %rax
	movl	(%r15), %r8d
	leal	1(%rax), %edx
	movl	%edx, 0(%r13)
	movq	8(%r13), %rdx
	movq	%rbx, (%rdx,%rax,8)
	testl	%r8d, %r8d
	jg	.L78
	movq	(%rbx), %r12
	movslq	56(%rbx), %rax
	addq	%r12, %rax
	cmpq	%r12, %rax
	je	.L85
	.p2align 4,,10
	.p2align 3
.L96:
	cmpb	$58, (%r12)
	jne	.L95
	movb	$47, (%r12)
.L95:
	addq	$1, %r12
	cmpq	%r12, %rax
	jne	.L96
	movq	(%rbx), %r12
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L343:
	movq	%rax, -208(%rbp)
	movl	-212(%rbp), %ecx
	movl	$1, %esi
	leaq	-192(%rbp), %rax
	leaq	-208(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L59
.L64:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L177
	movl	$32, %r14d
.L184:
	cmpl	%ebx, 16(%r12)
	cmovle	16(%r12), %ebx
	movq	%r13, %rdi
	movq	8(%r12), %rsi
	cmpl	%r14d, %ebx
	cmovg	%r14d, %ebx
	movslq	%ebx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 20(%r12)
	jne	.L364
.L67:
	movq	%r13, 8(%r12)
	movl	%r14d, 16(%r12)
	movb	$1, 20(%r12)
.L63:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L68
	leaq	13(%rax), %rax
	xorl	%r14d, %r14d
	movl	$0, 56(%rbx)
	movq	-328(%rbp), %rsi
	movw	%r14w, 12(%rbx)
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movl	$40, 8(%rbx)
	movslq	(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r12)
	movq	8(%r12), %rdx
	movq	%rbx, (%rdx,%rax,8)
	movq	%r15, %rdx
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L61
	movq	(%rbx), %rax
	movq	%rax, -320(%rbp)
	jmp	.L62
.L350:
	movslq	%r8d, %rdi
	movl	%r8d, -240(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L175
	testl	%ebx, %ebx
	movl	-240(%rbp), %r8d
	jg	.L182
	cmpb	$0, 20(%r13)
	je	.L105
.L356:
	movq	8(%r13), %rdi
	movl	%r8d, -296(%rbp)
	movq	%rcx, -240(%rbp)
	call	uprv_free_67@PLT
	movl	-296(%rbp), %r8d
	movq	-240(%rbp), %rcx
	jmp	.L105
.L359:
	movslq	%r10d, %rdi
	movq	%r9, -408(%rbp)
	salq	$3, %rdi
	movl	%edx, -400(%rbp)
	movq	%r8, -352(%rbp)
	movl	%r10d, -296(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L174
	movl	-400(%rbp), %edx
	movl	-296(%rbp), %r10d
	movq	-352(%rbp), %r8
	movq	-408(%rbp), %r9
	testl	%edx, %edx
	jg	.L180
	cmpb	$0, 20(%r9)
	je	.L127
.L361:
	movq	8(%r9), %rdi
	movl	%r10d, -408(%rbp)
	movq	%rcx, -400(%rbp)
	movq	%r8, -352(%rbp)
	movq	%r9, -296(%rbp)
	call	uprv_free_67@PLT
	movl	-408(%rbp), %r10d
	movq	-400(%rbp), %rcx
	movq	-352(%rbp), %r8
	movq	-296(%rbp), %r9
	jmp	.L127
.L351:
	movslq	%ecx, %rdi
	movl	%ecx, -232(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L173
	testl	%ebx, %ebx
	movl	-232(%rbp), %ecx
	jg	.L179
	cmpb	$0, 20(%r13)
	je	.L150
.L362:
	movq	8(%r13), %rdi
	movl	%ecx, -232(%rbp)
	call	uprv_free_67@PLT
	movl	-232(%rbp), %ecx
	jmp	.L150
.L360:
	movslq	%r8d, %rdi
	movl	%r8d, -240(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L176
	testl	%ebx, %ebx
	movl	-240(%rbp), %r8d
	jg	.L183
	cmpb	$0, 20(%r13)
	je	.L90
.L363:
	movq	8(%r13), %rdi
	movl	%r8d, -288(%rbp)
	movq	%rcx, -240(%rbp)
	call	uprv_free_67@PLT
	movl	-288(%rbp), %r8d
	movq	-240(%rbp), %rcx
	jmp	.L90
.L344:
	movslq	%r14d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L177
	testl	%ebx, %ebx
	jg	.L184
	cmpb	$0, 20(%r12)
	je	.L67
.L364:
	movq	8(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L141:
	testq	%r12, %r12
	je	.L108
	movq	%r12, %rdi
	call	ures_close_67@PLT
	jmp	.L108
.L106:
	movslq	0(%r13), %rax
	leal	1(%rax), %edx
	movl	%edx, 0(%r13)
	movq	8(%r13), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L175
.L151:
	movq	$0, (%rdx,%rax,8)
	jmp	.L173
.L342:
	call	__stack_chk_fail@PLT
.L68:
	movslq	(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r12)
	movq	8(%r12), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L177
.L128:
	movslq	(%r9), %rax
	leal	1(%rax), %edx
	movl	%edx, (%r9)
	movq	8(%r9), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L174
.L53:
	movq	$0, _ZL18gKeyTypeStringPool(%rip)
	movl	$7, (%r15)
	jmp	.L178
.L54:
	movq	$0, _ZL21gLocExtKeyDataEntries(%rip)
	movl	$7, (%r15)
	jmp	.L178
.L55:
	movq	$0, _ZL18gLocExtTypeEntries(%rip)
	movl	$7, (%r15)
	jmp	.L178
.L91:
	movslq	0(%r13), %rax
	leal	1(%rax), %edx
	movl	%edx, 0(%r13)
	movq	8(%r13), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L176
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZL22initFromResourceBundleR10UErrorCode.cold, @function
_ZL22initFromResourceBundleR10UErrorCode.cold:
.LFSB3364:
.L296:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE3364:
	.text
	.size	_ZL22initFromResourceBundleR10UErrorCode, .-_ZL22initFromResourceBundleR10UErrorCode
	.section	.text.unlikely
	.size	_ZL22initFromResourceBundleR10UErrorCode.cold, .-_ZL22initFromResourceBundleR10UErrorCode.cold
.LCOLDE9:
	.text
.LHOTE9:
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3609:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3609:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3612:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L378
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L366
	cmpb	$0, 12(%rbx)
	jne	.L379
.L370:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L366:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L370
	.cfi_endproc
.LFE3612:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3615:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L382
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3615:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3618:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L385
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3618:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L391
.L387:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L392
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L392:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3620:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3621:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3621:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3622:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3622:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3623:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3623:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3624:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3624:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3625:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3625:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3626:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L408
	testl	%edx, %edx
	jle	.L408
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L411
.L400:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L400
	.cfi_endproc
.LFE3626:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L415
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L415
	testl	%r12d, %r12d
	jg	.L422
	cmpb	$0, 12(%rbx)
	jne	.L423
.L417:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L417
.L423:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L415:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3627:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L425
	movq	(%rdi), %r8
.L426:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L429
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L429
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L429:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3628:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3629:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L436
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3629:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3630:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3630:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3631:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3631:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3632:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3632:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3634:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3634:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3636:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3636:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.p2align 4
	.globl	ulocimp_toBcpKey_67
	.type	ulocimp_toBcpKey_67, @function
ulocimp_toBcpKey_67:
.LFB3369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZL21gLocExtKeyMapInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L456
.L443:
	movl	4+_ZL21gLocExtKeyMapInitOnce(%rip), %edx
	testl	%edx, %edx
	jle	.L444
.L445:
	xorl	%eax, %eax
.L442:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L457
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	leaq	_ZL21gLocExtKeyMapInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L443
	leaq	-28(%rbp), %rdi
	call	_ZL22initFromResourceBundleR10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZL21gLocExtKeyMapInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gLocExtKeyMapInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L444:
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L445
	movq	_ZL13gLocExtKeyMap(%rip), %rdi
	movq	%r12, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L442
	movq	8(%rax), %rax
	jmp	.L442
.L457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3369:
	.size	ulocimp_toBcpKey_67, .-ulocimp_toBcpKey_67
	.p2align 4
	.globl	ulocimp_toLegacyKey_67
	.type	ulocimp_toLegacyKey_67, @function
ulocimp_toLegacyKey_67:
.LFB3370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZL21gLocExtKeyMapInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L472
.L459:
	movl	4+_ZL21gLocExtKeyMapInitOnce(%rip), %edx
	testl	%edx, %edx
	jle	.L460
.L461:
	xorl	%eax, %eax
.L458:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L473
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	leaq	_ZL21gLocExtKeyMapInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L459
	leaq	-28(%rbp), %rdi
	call	_ZL22initFromResourceBundleR10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZL21gLocExtKeyMapInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gLocExtKeyMapInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L460:
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L461
	movq	_ZL13gLocExtKeyMap(%rip), %rdi
	movq	%r12, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L458
	movq	(%rax), %rax
	jmp	.L458
.L473:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3370:
	.size	ulocimp_toLegacyKey_67, .-ulocimp_toLegacyKey_67
	.p2align 4
	.globl	ulocimp_toBcpType_67
	.type	ulocimp_toBcpType_67, @function
ulocimp_toBcpType_67:
.LFB3371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L475
	movb	$0, (%rdx)
.L475:
	testq	%r13, %r13
	je	.L476
	movb	$0, 0(%r13)
.L476:
	movl	$0, -60(%rbp)
	movl	_ZL21gLocExtKeyMapInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L559
.L477:
	movl	4+_ZL21gLocExtKeyMapInitOnce(%rip), %edx
	testl	%edx, %edx
	jle	.L478
.L479:
	xorl	%eax, %eax
.L474:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L560
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L559:
	.cfi_restore_state
	leaq	_ZL21gLocExtKeyMapInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L477
	leaq	-60(%rbp), %rdi
	call	_ZL22initFromResourceBundleR10UErrorCode
	movl	-60(%rbp), %eax
	leaq	_ZL21gLocExtKeyMapInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gLocExtKeyMapInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L478:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L479
	movq	_ZL13gLocExtKeyMap(%rip), %rdi
	movq	%r14, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L479
	testq	%rbx, %rbx
	je	.L482
	movb	$1, (%rbx)
.L482:
	movq	16(%r14), %rdi
	movq	%r12, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L483
	movq	8(%rax), %rax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L483:
	movl	24(%r14), %r8d
	testl	%r8d, %r8d
	je	.L479
	testb	$1, %r8b
	jne	.L485
	testb	$2, %r8b
	jne	.L561
.L486:
	andl	$4, %r8d
	je	.L479
	movzbl	(%r12), %eax
	testb	%al, %al
	je	.L479
	xorl	%ebx, %ebx
.L500:
	cmpq	$1, %rbx
	jbe	.L562
	movzbl	(%r12,%rbx), %eax
	andl	$-33, %eax
	cmpb	$90, %al
	jne	.L479
.L501:
	movl	%ebx, %edx
	addq	$1, %rbx
	movzbl	(%r12,%rbx), %eax
	testb	%al, %al
	jne	.L500
	cmpl	$5, %edx
	je	.L498
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L485:
	movsbl	(%r12), %edi
	testb	%dil, %dil
	je	.L486
	movl	%edi, %edx
	movq	%r12, %rax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L492:
	cmpb	$45, %dl
	je	.L563
	movl	%edx, %esi
	andl	$-33, %esi
	subl	$65, %esi
	cmpb	$5, %sil
	jbe	.L504
	subl	$48, %edx
	cmpb	$9, %dl
	jbe	.L504
.L489:
	testb	$2, %r8b
	je	.L486
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%r12, %r15
	xorl	%ebx, %ebx
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L565:
	subl	$3, %ebx
	cmpl	$5, %ebx
	ja	.L557
	xorl	%ebx, %ebx
.L495:
	movsbl	1(%r15), %edi
	addq	$1, %r15
	testb	%dil, %dil
	je	.L564
.L497:
	cmpb	$45, %dil
	je	.L565
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L557
	addl	$1, %ebx
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L504:
	addl	$1, %ecx
.L490:
	movzbl	1(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L492
	subl	$4, %ecx
	cmpl	$2, %ecx
	ja	.L489
.L498:
	testq	%r13, %r13
	je	.L558
	movb	$1, 0(%r13)
.L558:
	movq	%r12, %rax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L563:
	subl	$4, %ecx
	cmpl	$2, %ecx
	ja	.L489
	xorl	%ecx, %ecx
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L564:
	subl	$3, %ebx
	cmpl	$5, %ebx
	jbe	.L498
	.p2align 4,,10
	.p2align 3
.L557:
	movl	24(%r14), %r8d
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L561:
	movsbl	(%r12), %edi
	testb	%dil, %dil
	jne	.L487
	jmp	.L486
.L562:
	movsbl	%al, %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L479
	jmp	.L501
.L560:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3371:
	.size	ulocimp_toBcpType_67, .-ulocimp_toBcpType_67
	.p2align 4
	.globl	ulocimp_toLegacyType_67
	.type	ulocimp_toLegacyType_67, @function
ulocimp_toLegacyType_67:
.LFB3372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L567
	movb	$0, (%rdx)
.L567:
	testq	%r13, %r13
	je	.L568
	movb	$0, 0(%r13)
.L568:
	movl	$0, -60(%rbp)
	movl	_ZL21gLocExtKeyMapInitOnce(%rip), %eax
	cmpl	$2, %eax
	jne	.L651
.L569:
	movl	4+_ZL21gLocExtKeyMapInitOnce(%rip), %edx
	testl	%edx, %edx
	jle	.L570
.L571:
	xorl	%eax, %eax
.L566:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L652
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	leaq	_ZL21gLocExtKeyMapInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L569
	leaq	-60(%rbp), %rdi
	call	_ZL22initFromResourceBundleR10UErrorCode
	movl	-60(%rbp), %eax
	leaq	_ZL21gLocExtKeyMapInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL21gLocExtKeyMapInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L570:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L571
	movq	_ZL13gLocExtKeyMap(%rip), %rdi
	movq	%r14, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L571
	testq	%rbx, %rbx
	je	.L574
	movb	$1, (%rbx)
.L574:
	movq	16(%r14), %rdi
	movq	%r12, %rsi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L575
	movq	(%rax), %rax
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L575:
	movl	24(%r14), %r8d
	testl	%r8d, %r8d
	je	.L571
	testb	$1, %r8b
	jne	.L577
	testb	$2, %r8b
	jne	.L653
.L578:
	andl	$4, %r8d
	je	.L571
	movzbl	(%r12), %eax
	testb	%al, %al
	je	.L571
	xorl	%ebx, %ebx
.L592:
	cmpq	$1, %rbx
	jbe	.L654
	movzbl	(%r12,%rbx), %eax
	andl	$-33, %eax
	cmpb	$90, %al
	jne	.L571
.L593:
	movl	%ebx, %edx
	addq	$1, %rbx
	movzbl	(%r12,%rbx), %eax
	testb	%al, %al
	jne	.L592
	cmpl	$5, %edx
	je	.L590
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L577:
	movsbl	(%r12), %edi
	testb	%dil, %dil
	je	.L578
	movl	%edi, %edx
	movq	%r12, %rax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L584:
	cmpb	$45, %dl
	je	.L655
	movl	%edx, %esi
	andl	$-33, %esi
	subl	$65, %esi
	cmpb	$5, %sil
	jbe	.L596
	subl	$48, %edx
	cmpb	$9, %dl
	jbe	.L596
.L581:
	testb	$2, %r8b
	je	.L578
	.p2align 4,,10
	.p2align 3
.L579:
	movq	%r12, %r15
	xorl	%ebx, %ebx
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L657:
	subl	$3, %ebx
	cmpl	$5, %ebx
	ja	.L649
	xorl	%ebx, %ebx
.L587:
	movsbl	1(%r15), %edi
	addq	$1, %r15
	testb	%dil, %dil
	je	.L656
.L589:
	cmpb	$45, %dil
	je	.L657
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L649
	addl	$1, %ebx
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L596:
	addl	$1, %ecx
.L582:
	movzbl	1(%rax), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L584
	subl	$4, %ecx
	cmpl	$2, %ecx
	ja	.L581
.L590:
	testq	%r13, %r13
	je	.L650
	movb	$1, 0(%r13)
.L650:
	movq	%r12, %rax
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L655:
	subl	$4, %ecx
	cmpl	$2, %ecx
	ja	.L581
	xorl	%ecx, %ecx
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L656:
	subl	$3, %ebx
	cmpl	$5, %ebx
	jbe	.L590
	.p2align 4,,10
	.p2align 3
.L649:
	movl	24(%r14), %r8d
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L653:
	movsbl	(%r12), %edi
	testb	%dil, %dil
	jne	.L579
	jmp	.L578
.L654:
	movsbl	%al, %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L571
	jmp	.L593
.L652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3372:
	.size	ulocimp_toLegacyType_67, .-ulocimp_toLegacyType_67
	.local	_ZL18gLocExtTypeEntries
	.comm	_ZL18gLocExtTypeEntries,8,8
	.local	_ZL21gLocExtKeyDataEntries
	.comm	_ZL21gLocExtKeyDataEntries,8,8
	.local	_ZL18gKeyTypeStringPool
	.comm	_ZL18gKeyTypeStringPool,8,8
	.local	_ZL21gLocExtKeyMapInitOnce
	.comm	_ZL21gLocExtKeyMapInitOnce,8,8
	.local	_ZL13gLocExtKeyMap
	.comm	_ZL13gLocExtKeyMap,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
