	.file	"utf_impl.cpp"
	.text
	.section	.rodata
.LC0:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	" 000000000000\02000"
	.text
	.p2align 4
	.globl	utf8_nextCharSafeBody_67
	.type	utf8_nextCharSafeBody_67, @function
utf8_nextCharSafeBody_67:
.LFB28:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	(%rsi), %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	cmpl	%edx, %r10d
	je	.L29
	cmpl	$244, %ecx
	jg	.L29
	cmpl	$239, %ecx
	jle	.L3
	movslq	%r10d, %rax
	leaq	.LC0(%rip), %r9
	andl	$7, %ecx
	movzbl	(%rdi,%rax), %r11d
	movl	%r11d, %eax
	sarl	$4, %eax
	cltq
	movsbl	(%r9,%rax), %eax
	btl	%ecx, %eax
	jnc	.L29
	leal	1(%r10), %eax
	cmpl	%eax, %edx
	je	.L23
	movslq	%eax, %r9
	movzbl	(%rdi,%r9), %r9d
	addl	$-128, %r9d
	cmpb	$63, %r9b
	ja	.L24
	leal	2(%r10), %ebx
	cmpl	%ebx, %edx
	je	.L19
	movslq	%ebx, %rax
	movzbl	(%rdi,%rax), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	jbe	.L38
	movl	%ebx, %r10d
	movl	$65535, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$223, %ecx
	jle	.L8
	movslq	%r10d, %rax
	andl	$15, %ecx
	movzbl	(%rdi,%rax), %eax
	cmpb	$-2, %r8b
	je	.L9
	movzbl	%al, %r9d
	leaq	.LC1(%rip), %r11
	movslq	%ecx, %rax
	movsbl	(%r11,%rax), %r11d
	movl	%r9d, %eax
	sarl	$5, %eax
	btl	%eax, %r11d
	jc	.L39
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$21, %eax
.L2:
	testb	%r8b, %r8b
	jns	.L14
	cmpb	$-3, %r8b
	movl	$65533, %ecx
	movl	$-1, %eax
	cmove	%ecx, %eax
.L14:
	movl	%r10d, (%rsi)
.L1:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	cmpl	$193, %ecx
	jle	.L29
	movslq	%r10d, %rax
	movzbl	(%rdi,%rax), %edx
	movl	$21, %eax
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L2
	subl	$192, %ecx
	addl	$1, %r10d
	movzbl	%dl, %eax
	sall	$6, %ecx
	movl	%r10d, (%rsi)
	orl	%ecx, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	movl	%eax, %r10d
	movl	$159, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L23:
	movl	%edx, %r10d
	movl	$159, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L39:
	leal	1(%r10), %eax
	cmpl	%eax, %edx
	je	.L23
	movslq	%eax, %rdx
	movzbl	(%rdi,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L24
	movl	%r9d, %eax
	movzbl	%dl, %edx
	sall	$12, %ecx
	addl	$2, %r10d
	sall	$6, %eax
	andl	$4032, %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	cmpl	$64975, %eax
	jle	.L14
	testb	%r8b, %r8b
	jle	.L14
	cmpl	$65007, %eax
	jle	.L26
	movl	%eax, %edx
	andl	$65534, %edx
	cmpl	$65534, %edx
	jne	.L14
.L26:
	movl	$65535, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L38:
	sall	$12, %r11d
	movzbl	%al, %eax
	sall	$18, %ecx
	addl	$3, %r10d
	andl	$258048, %r11d
	orl	%r11d, %eax
	orl	%ecx, %eax
	movl	%eax, %r11d
	movzbl	%r9b, %eax
	sall	$6, %eax
	orl	%r11d, %eax
	cmpl	$64975, %eax
	jle	.L14
	testb	%r8b, %r8b
	jle	.L14
	cmpl	$65007, %eax
	jle	.L5
	movl	%eax, %edx
	andl	$65534, %edx
	cmpl	$65534, %edx
	jne	.L14
.L5:
	cmpl	$1114111, %eax
	jg	.L14
	movl	$1114111, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L9:
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L11
	testl	%ecx, %ecx
	jne	.L32
	cmpb	$31, %al
	ja	.L32
.L11:
	movl	$-1, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L19:
	movl	%edx, %r10d
	movl	$65535, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L32:
	leal	1(%r10), %r8d
	cmpl	%r8d, %edx
	je	.L27
	movslq	%r8d, %rdx
	movzbl	(%rdi,%rdx), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	jbe	.L40
	movl	%r8d, %r10d
	jmp	.L11
.L40:
	movzbl	%al, %eax
	movzbl	%dl, %edx
	addl	$2, %r10d
	sall	$12, %ecx
	sall	$6, %eax
	movl	%r10d, (%rsi)
	orl	%edx, %eax
	orl	%ecx, %eax
	jmp	.L1
.L27:
	movl	%edx, %r10d
	jmp	.L11
	.cfi_endproc
.LFE28:
	.size	utf8_nextCharSafeBody_67, .-utf8_nextCharSafeBody_67
	.p2align 4
	.globl	utf8_appendCharSafeBody_67
	.type	utf8_appendCharSafeBody_67, @function
utf8_appendCharSafeBody_67:
.LFB29:
	.cfi_startproc
	endbr64
	cmpl	$2047, %ecx
	ja	.L42
	leal	1(%rsi), %r9d
	cmpl	%edx, %r9d
	jl	.L57
.L43:
	testq	%r8, %r8
	je	.L46
	movb	$1, (%r8)
	movl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	cmpl	$65535, %ecx
	ja	.L45
	leal	2(%rsi), %r9d
	cmpl	%edx, %r9d
	jge	.L43
	movl	%ecx, %eax
	andl	$-2048, %eax
	cmpl	$55296, %eax
	je	.L43
	movl	%ecx, %eax
	movslq	%esi, %rdx
	movslq	%r9d, %r9
	sarl	$12, %eax
	orl	$-32, %eax
	movb	%al, (%rdi,%rdx)
	movl	%ecx, %eax
	leal	1(%rsi), %edx
	andl	$63, %ecx
	sarl	$6, %eax
	movslq	%edx, %rdx
	orl	$-128, %ecx
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, (%rdi,%rdx)
	leal	3(%rsi), %eax
	movb	%cl, (%rdi,%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	$1114111, %ecx
	ja	.L43
	leal	3(%rsi), %r9d
	cmpl	%edx, %r9d
	jge	.L43
	movl	%ecx, %eax
	movslq	%esi, %rdx
	movslq	%r9d, %r9
	sarl	$18, %eax
	orl	$-16, %eax
	movb	%al, (%rdi,%rdx)
	movl	%ecx, %eax
	leal	1(%rsi), %edx
	sarl	$12, %eax
	movslq	%edx, %rdx
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, (%rdi,%rdx)
	movl	%ecx, %eax
	leal	2(%rsi), %edx
	andl	$63, %ecx
	sarl	$6, %eax
	movslq	%edx, %rdx
	orl	$-128, %ecx
	andl	$63, %eax
	orl	$-128, %eax
	movb	%al, (%rdi,%rdx)
	leal	4(%rsi), %eax
	movb	%cl, (%rdi,%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	subl	%esi, %edx
	movl	%esi, %eax
	testl	%edx, %edx
	jle	.L41
	movslq	%esi, %rax
	addq	%rax, %rdi
	cmpl	$3, %edx
	movl	$3, %eax
	cmovg	%eax, %edx
	leaq	_ZL15utf8_errorValue(%rip), %rax
	subl	$1, %edx
	movslq	%edx, %rdx
	movl	(%rax,%rdx,4), %edx
	cmpl	$127, %edx
	ja	.L47
	movb	%dl, (%rdi)
	movl	$1, %eax
	addl	%esi, %eax
.L41:
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%ecx, %eax
	andl	$63, %ecx
	movslq	%esi, %rdx
	movslq	%r9d, %r9
	sarl	$6, %eax
	orl	$-128, %ecx
	orl	$-64, %eax
	movb	%al, (%rdi,%rdx)
	leal	2(%rsi), %eax
	movb	%cl, (%rdi,%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	movl	%edx, %ecx
	shrl	$6, %ecx
	cmpl	$2047, %edx
	jbe	.L58
	movl	%edx, %eax
	shrl	$12, %eax
	cmpl	$65535, %edx
	ja	.L51
	movl	%eax, %r9d
	movl	$2, %r8d
	movl	$3, %eax
	movl	$1, %r10d
	orl	$-32, %r9d
.L52:
	andl	$63, %ecx
	movb	%r9b, (%rdi)
	orl	$-128, %ecx
	movb	%cl, (%rdi,%r10)
.L50:
	andl	$63, %edx
	addl	%esi, %eax
	orl	$-128, %edx
	movb	%dl, (%rdi,%r8)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L58:
	orl	$-64, %ecx
	movl	$2, %eax
	movl	$1, %r8d
	movb	%cl, (%rdi)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L51:
	movl	%edx, %r9d
	andl	$63, %eax
	movl	$3, %r8d
	movl	$2, %r10d
	orl	$-128, %eax
	shrl	$18, %r9d
	movb	%al, 1(%rdi)
	orl	$-16, %r9d
	movl	$4, %eax
	jmp	.L52
	.cfi_endproc
.LFE29:
	.size	utf8_appendCharSafeBody_67, .-utf8_appendCharSafeBody_67
	.p2align 4
	.globl	utf8_prevCharSafeBody_67
	.type	utf8_prevCharSafeBody_67, @function
utf8_prevCharSafeBody_67:
.LFB30:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r9d
	cmpb	$-64, %cl
	jge	.L111
	cmpl	%esi, %r9d
	jg	.L118
.L111:
	testb	%r8b, %r8b
	jns	.L119
.L113:
	movl	$65533, %eax
	cmpb	$-3, %r8b
	jne	.L120
.L112:
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	leal	-1(%r9), %r10d
	movslq	%r10d, %rax
	movzbl	(%rdi,%rax), %eax
	leal	62(%rax), %r11d
	cmpb	$50, %r11b
	jbe	.L121
	cmpb	$-64, %al
	jge	.L111
	cmpl	%r10d, %esi
	jge	.L111
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$63, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leal	-2(%r9), %ebx
	movslq	%ebx, %r10
	movzbl	(%rdi,%r10), %r10d
	leal	32(%r10), %r11d
	cmpb	$20, %r11b
	jbe	.L122
	cmpb	$-64, %r10b
	jge	.L60
	cmpl	%ebx, %esi
	jge	.L60
	subl	$3, %r9d
	movslq	%r9d, %rsi
	movzbl	(%rdi,%rsi), %esi
	leal	16(%rsi), %edi
	cmpb	$4, %dil
	ja	.L60
	movl	%r10d, %edi
	leaq	.LC0(%rip), %rbx
	movl	%esi, %r11d
	andl	$7, %esi
	sarl	$4, %edi
	andl	$7, %r11d
	movslq	%edi, %rdi
	movsbl	(%rbx,%rdi), %edi
	btl	%r11d, %edi
	jnc	.L60
	sall	$12, %r10d
	movl	%r9d, (%rdx)
	sall	$18, %esi
	movl	%r10d, %edx
	sall	$6, %eax
	andl	$258048, %edx
	andl	$4032, %eax
	orl	%esi, %edx
	orl	%edx, %ecx
	orl	%ecx, %eax
	testb	%r8b, %r8b
	jle	.L59
	cmpl	$64975, %eax
	jle	.L59
	cmpl	$65007, %eax
	jle	.L73
	movl	%eax, %edx
	andl	$65534, %edx
	cmpl	$65534, %edx
	jne	.L59
.L73:
	cmpl	$1114111, %eax
	movl	$1114111, %edx
	cmovl	%edx, %eax
.L59:
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore 3
	.cfi_restore 6
	movl	$21, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	cmpb	$-33, %al
	jbe	.L123
	cmpb	$-17, %al
	jbe	.L124
	sarl	$4, %ecx
	andl	$7, %eax
	movl	$1, %edi
	andl	$15, %ecx
	leaq	.LC0(%rip), %rsi
	movsbl	(%rsi,%rcx), %esi
	movl	%eax, %ecx
	sall	%cl, %edi
	movl	%edi, %eax
	andl	%esi, %eax
.L65:
	testl	%eax, %eax
	je	.L111
	movl	%r10d, (%rdx)
	movl	$159, %eax
	testb	%r8b, %r8b
	js	.L113
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L124:
	shrb	$5, %cl
	movl	$1, %esi
	andl	$15, %eax
	sall	%cl, %esi
	leaq	.LC1(%rip), %rcx
	movsbl	(%rcx,%rax), %eax
	andl	%esi, %eax
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpb	$-17, %r10b
	ja	.L67
	andl	$15, %r10d
	cmpb	$-2, %r8b
	je	.L68
	leaq	.LC1(%rip), %rdi
	movzbl	%r10b, %esi
	andl	$15, %r10d
	movsbl	(%rdi,%r10), %r9d
	movl	%eax, %edi
	sarl	$5, %edi
	btl	%edi, %r9d
	jnc	.L60
	sall	$6, %eax
	sall	$12, %esi
	movl	%ebx, (%rdx)
	andl	$4032, %eax
	orl	%esi, %eax
	orl	%ecx, %eax
	cmpl	$64975, %eax
	jle	.L59
	testb	%r8b, %r8b
	jle	.L59
	cmpl	$65007, %eax
	jle	.L69
	movl	%eax, %edx
	andl	$65534, %edx
	cmpl	$65534, %edx
	jne	.L59
.L69:
	movl	$65535, %eax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	testb	%r8b, %r8b
	jns	.L125
.L72:
	movl	$65533, %eax
	cmpb	$-3, %r8b
	je	.L59
.L70:
	movl	$-1, %eax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$21, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore 3
	.cfi_restore 6
	subl	$192, %eax
	andl	$63, %ecx
	movl	%r10d, (%rdx)
	sall	$6, %eax
	orl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	shrq	$4, %rax
	leaq	.LC0(%rip), %rcx
	andl	$7, %r10d
	andl	$15, %eax
	movsbl	(%rcx,%rax), %eax
	btl	%r10d, %eax
	jnc	.L60
	movl	%ebx, (%rdx)
	testb	%r8b, %r8b
	js	.L72
	jmp	.L69
.L68:
	leal	-128(%rax), %esi
	testb	%r10b, %r10b
	jne	.L76
	cmpb	$31, %sil
	jbe	.L70
.L76:
	movzbl	%r10b, %r10d
	movl	%ebx, (%rdx)
	movzbl	%sil, %edx
	movl	%r10d, %eax
	sall	$6, %edx
	sall	$12, %eax
	orl	%edx, %eax
	orl	%ecx, %eax
	jmp	.L59
	.cfi_endproc
.LFE30:
	.size	utf8_prevCharSafeBody_67, .-utf8_prevCharSafeBody_67
	.p2align 4
	.globl	utf8_back1SafeBody_67
	.type	utf8_back1SafeBody_67, @function
utf8_back1SafeBody_67:
.LFB31:
	.cfi_startproc
	endbr64
	movslq	%edx, %rdx
	movq	%rdx, %rax
	movzbl	(%rdi,%rdx), %edx
	cmpb	$-64, %dl
	jge	.L127
	cmpl	%esi, %eax
	jle	.L127
	leal	-1(%rax), %r9d
	movslq	%r9d, %rcx
	movzbl	(%rdi,%rcx), %r8d
	leal	62(%r8), %ecx
	cmpb	$50, %cl
	ja	.L128
	cmpb	$-33, %r8b
	jbe	.L134
	cmpb	$-17, %r8b
	ja	.L129
	sarl	$5, %edx
	movl	$1, %esi
	movl	%edx, %ecx
	movq	%r8, %rdx
	sall	%cl, %esi
	andl	$15, %edx
	leaq	.LC1(%rip), %rcx
	movsbl	(%rcx,%rdx), %edx
	andl	%esi, %edx
.L130:
	testl	%edx, %edx
	cmovne	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	movl	%r9d, %eax
.L127:
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	cmpb	$-64, %r8b
	jge	.L127
	cmpl	%r9d, %esi
	jge	.L127
	leal	-2(%rax), %r10d
	movslq	%r10d, %rdx
	movzbl	(%rdi,%rdx), %r9d
	leal	32(%r9), %edx
	cmpb	$20, %dl
	ja	.L131
	movzbl	%r8b, %edx
	cmpb	$-17, %r9b
	jbe	.L159
	movl	%r9d, %ecx
	movl	$1, %esi
	sarl	$4, %edx
	andl	$7, %ecx
	movslq	%edx, %rdx
	sall	%cl, %esi
	leaq	.LC0(%rip), %rcx
	movsbl	(%rcx,%rdx), %edx
	andl	%esi, %edx
.L133:
	testl	%edx, %edx
	cmovne	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	movl	%r8d, %ecx
	movl	$1, %esi
	sarl	$4, %edx
	andl	$7, %ecx
	movslq	%edx, %rdx
	sall	%cl, %esi
	leaq	.LC0(%rip), %rcx
	movsbl	(%rcx,%rdx), %edx
	andl	%esi, %edx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L131:
	cmpb	$-64, %r9b
	jge	.L127
	cmpl	%r10d, %esi
	jge	.L127
	leal	-3(%rax), %edx
	movslq	%edx, %rcx
	movzbl	(%rdi,%rcx), %ecx
	leal	16(%rcx), %esi
	cmpb	$4, %sil
	ja	.L127
	shrq	$4, %r9
	leaq	.LC0(%rip), %rsi
	andl	$7, %ecx
	andl	$15, %r9d
	movsbl	(%rsi,%r9), %esi
	sarl	%cl, %esi
	andl	$1, %esi
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	sarl	$5, %edx
	movl	$1, %esi
	andl	$15, %r9d
	movl	%edx, %ecx
	sall	%cl, %esi
	leaq	.LC1(%rip), %rcx
	movsbl	(%rcx,%r9), %ecx
	movl	%esi, %edx
	andl	%ecx, %edx
	jmp	.L133
	.cfi_endproc
.LFE31:
	.size	utf8_back1SafeBody_67, .-utf8_back1SafeBody_67
	.section	.rodata
	.align 16
	.type	_ZL15utf8_errorValue, @object
	.size	_ZL15utf8_errorValue, 24
_ZL15utf8_errorValue:
	.long	21
	.long	159
	.long	65535
	.long	1114111
	.zero	8
	.globl	utf8_countTrailBytes_67
	.align 32
	.type	utf8_countTrailBytes_67, @object
	.size	utf8_countTrailBytes_67, 256
utf8_countTrailBytes_67:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\003\003\003\003\003"
	.zero	10
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
