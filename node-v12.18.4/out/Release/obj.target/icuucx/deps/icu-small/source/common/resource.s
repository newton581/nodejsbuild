	.file	"resource.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ResourceValueD2Ev
	.type	_ZN6icu_6713ResourceValueD2Ev, @function
_ZN6icu_6713ResourceValueD2Ev:
.LFB2285:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713ResourceValueE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2285:
	.size	_ZN6icu_6713ResourceValueD2Ev, .-_ZN6icu_6713ResourceValueD2Ev
	.globl	_ZN6icu_6713ResourceValueD1Ev
	.set	_ZN6icu_6713ResourceValueD1Ev,_ZN6icu_6713ResourceValueD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713ResourceValueD0Ev
	.type	_ZN6icu_6713ResourceValueD0Ev, @function
_ZN6icu_6713ResourceValueD0Ev:
.LFB2287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713ResourceValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2287:
	.size	_ZN6icu_6713ResourceValueD0Ev, .-_ZN6icu_6713ResourceValueD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ResourceSinkD2Ev
	.type	_ZN6icu_6712ResourceSinkD2Ev, @function
_ZN6icu_6712ResourceSinkD2Ev:
.LFB2289:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6712ResourceSinkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2289:
	.size	_ZN6icu_6712ResourceSinkD2Ev, .-_ZN6icu_6712ResourceSinkD2Ev
	.globl	_ZN6icu_6712ResourceSinkD1Ev
	.set	_ZN6icu_6712ResourceSinkD1Ev,_ZN6icu_6712ResourceSinkD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6712ResourceSinkD0Ev
	.type	_ZN6icu_6712ResourceSinkD0Ev, @function
_ZN6icu_6712ResourceSinkD0Ev:
.LFB2291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6712ResourceSinkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2291:
	.size	_ZN6icu_6712ResourceSinkD0Ev, .-_ZN6icu_6712ResourceSinkD0Ev
	.weak	_ZTSN6icu_6713ResourceValueE
	.section	.rodata._ZTSN6icu_6713ResourceValueE,"aG",@progbits,_ZTSN6icu_6713ResourceValueE,comdat
	.align 16
	.type	_ZTSN6icu_6713ResourceValueE, @object
	.size	_ZTSN6icu_6713ResourceValueE, 25
_ZTSN6icu_6713ResourceValueE:
	.string	"N6icu_6713ResourceValueE"
	.weak	_ZTIN6icu_6713ResourceValueE
	.section	.data.rel.ro._ZTIN6icu_6713ResourceValueE,"awG",@progbits,_ZTIN6icu_6713ResourceValueE,comdat
	.align 8
	.type	_ZTIN6icu_6713ResourceValueE, @object
	.size	_ZTIN6icu_6713ResourceValueE, 24
_ZTIN6icu_6713ResourceValueE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713ResourceValueE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6712ResourceSinkE
	.section	.rodata._ZTSN6icu_6712ResourceSinkE,"aG",@progbits,_ZTSN6icu_6712ResourceSinkE,comdat
	.align 16
	.type	_ZTSN6icu_6712ResourceSinkE, @object
	.size	_ZTSN6icu_6712ResourceSinkE, 24
_ZTSN6icu_6712ResourceSinkE:
	.string	"N6icu_6712ResourceSinkE"
	.weak	_ZTIN6icu_6712ResourceSinkE
	.section	.data.rel.ro._ZTIN6icu_6712ResourceSinkE,"awG",@progbits,_ZTIN6icu_6712ResourceSinkE,comdat
	.align 8
	.type	_ZTIN6icu_6712ResourceSinkE, @object
	.size	_ZTIN6icu_6712ResourceSinkE, 24
_ZTIN6icu_6712ResourceSinkE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6712ResourceSinkE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6713ResourceValueE
	.section	.data.rel.ro._ZTVN6icu_6713ResourceValueE,"awG",@progbits,_ZTVN6icu_6713ResourceValueE,comdat
	.align 8
	.type	_ZTVN6icu_6713ResourceValueE, @object
	.size	_ZTVN6icu_6713ResourceValueE, 144
_ZTVN6icu_6713ResourceValueE:
	.quad	0
	.quad	_ZTIN6icu_6713ResourceValueE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6712ResourceSinkE
	.section	.data.rel.ro._ZTVN6icu_6712ResourceSinkE,"awG",@progbits,_ZTVN6icu_6712ResourceSinkE,comdat
	.align 8
	.type	_ZTVN6icu_6712ResourceSinkE, @object
	.size	_ZTVN6icu_6712ResourceSinkE, 48
_ZTVN6icu_6712ResourceSinkE:
	.quad	0
	.quad	_ZTIN6icu_6712ResourceSinkE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
