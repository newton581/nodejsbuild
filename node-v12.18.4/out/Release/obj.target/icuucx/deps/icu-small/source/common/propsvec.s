	.file	"propsvec.cpp"
	.text
	.p2align 4
	.type	_ZL8_findRowP13UPropsVectorsi, @function
_ZL8_findRowP13UPropsVectorsi:
.LFB2742:
	.cfi_startproc
	movl	20(%rdi), %ecx
	movq	(%rdi), %r11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	16(%rdi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	8(%rdi), %ebx
	movl	%ebx, %eax
	imull	%ecx, %eax
	cltq
	leaq	(%r11,%rax,4), %r8
	cmpl	%esi, (%r8)
	jg	.L2
	cmpl	%esi, 4(%r8)
	jg	.L1
	movslq	%ebx, %rax
	salq	$2, %rax
	addq	%rax, %r8
	cmpl	%esi, 4(%r8)
	jg	.L28
	addq	%rax, %r8
	movl	4(%r8), %r9d
	cmpl	%esi, %r9d
	jg	.L29
	movl	%esi, %r10d
	subl	%r9d, %r10d
	cmpl	$9, %r10d
	jg	.L6
	addl	$2, %ecx
	cmpl	$1, %ebx
	jne	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	addq	%rax, %r8
	addl	$1, %ecx
	cmpl	%esi, 4(%r8)
	jle	.L7
.L8:
	movl	%ecx, 20(%rdi)
.L1:
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	cmpl	%esi, 4(%r11)
	jg	.L30
.L6:
	xorl	%r9d, %r9d
	leal	-1(%rdx), %r10d
	cmpl	$1, %ebx
	je	.L15
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L32:
	leal	(%rdx,%r9), %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	sarl	%eax
	movslq	%eax, %rcx
	movl	%eax, %r12d
	leaq	(%r11,%rcx,4), %r8
	cmpl	(%r8), %esi
	jge	.L31
	movl	%eax, %edx
	leal	-1(%rax), %r10d
.L15:
	cmpl	%r9d, %r10d
	jg	.L32
.L11:
	movl	%r9d, 20(%rdi)
	imull	%ebx, %r9d
	movslq	%r9d, %rbx
	leaq	(%r11,%rbx,4), %r8
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	leal	(%r9,%rdx), %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	movl	%ebx, %ecx
	sarl	%eax
	imull	%eax, %ecx
	movl	%eax, %r12d
	movslq	%ecx, %rcx
	leaq	(%r11,%rcx,4), %r8
	cmpl	%esi, (%r8)
	jle	.L33
	movl	%eax, %edx
	leal	-1(%rax), %r10d
.L12:
	cmpl	%r10d, %r9d
	jl	.L34
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L31:
	cmpl	4(%r8), %esi
	jl	.L13
	movl	%eax, %r9d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L29:
	addl	$2, %ecx
	movl	%ecx, 20(%rdi)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r11, %r8
	popq	%rbx
	popq	%r12
	movl	$0, 20(%rdi)
	movq	%r8, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	addl	$1, %ecx
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	movl	%ecx, 20(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	cmpl	%esi, 4(%r8)
	jg	.L13
	movl	%eax, %r9d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L9:
	addq	%rax, %r8
	addl	$1, %ecx
	cmpl	%esi, 4(%r8)
	jle	.L9
	jmp	.L8
.L13:
	movl	%r12d, 20(%rdi)
	jmp	.L1
	.cfi_endproc
.LFE2742:
	.size	_ZL8_findRowP13UPropsVectorsi, .-_ZL8_findRowP13UPropsVectorsi
	.p2align 4
	.type	_ZL17upvec_compareRowsPKvS0_S0_, @function
_ZL17upvec_compareRowsPKvS0_S0_:
.LFB2746:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %r8d
	movl	$2, %eax
	movl	%r8d, %edi
	.p2align 4,,10
	.p2align 3
.L40:
	movslq	%eax, %rcx
	movl	(%rdx,%rcx,4), %r9d
	cmpl	%r9d, (%rsi,%rcx,4)
	jne	.L44
	addl	$1, %eax
	subl	$1, %edi
	cmpl	%eax, %r8d
	je	.L45
	testl	%edi, %edi
	jg	.L40
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L40
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	sbbl	%eax, %eax
	orl	$1, %eax
	ret
	.cfi_endproc
.LFE2746:
	.size	_ZL17upvec_compareRowsPKvS0_S0_, .-_ZL17upvec_compareRowsPKvS0_S0_
	.p2align 4
	.globl	upvec_open_67
	.type	upvec_open_67, @function
upvec_open_67:
.LFB2740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L46
	movq	%rsi, %r14
	testl	%edi, %edi
	jle	.L54
	leal	2(%rdi), %ebx
	movl	$32, %edi
	call	uprv_malloc_67@PLT
	movl	%ebx, %edi
	sall	$14, %edi
	movq	%rax, %r12
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L52
	testq	%rax, %rax
	je	.L52
	movq	%rax, (%r12)
	pxor	%xmm0, %xmm0
	leal	(%rbx,%rbx,2), %edx
	xorl	%esi, %esi
	movabsq	$12884905984, %rax
	movl	%ebx, 8(%r12)
	sall	$2, %edx
	movq	%r13, %rdi
	movups	%xmm0, 16(%r12)
	movslq	%edx, %rdx
	movslq	%ebx, %rbx
	movq	%rax, 12(%r12)
	call	memset@PLT
	movabsq	$4785074604081152, %rax
	movabsq	$4785078900162560, %rcx
	movq	%rax, 0(%r13)
	leaq	0(%r13,%rbx,4), %rax
	movq	%rcx, (%rax)
	movabsq	$4785083195129857, %rcx
	movq	%rcx, (%rax,%rbx,4)
.L46:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movl	$1, (%rsi)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L52:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movl	$7, (%r14)
	jmp	.L46
	.cfi_endproc
.LFE2740:
	.size	upvec_open_67, .-upvec_open_67
	.p2align 4
	.globl	upvec_close_67
	.type	upvec_close_67, @function
upvec_close_67:
.LFB2741:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L55
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	ret
	.cfi_endproc
.LFE2741:
	.size	upvec_close_67, .-upvec_close_67
	.p2align 4
	.globl	upvec_setValue_67
	.type	upvec_setValue_67, @function
upvec_setValue_67:
.LFB2743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r15
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L60
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L63
	movl	%esi, %r12d
	testl	%esi, %esi
	js	.L63
	cmpl	%edx, %esi
	setg	%r10b
	cmpl	$1114113, %edx
	setg	%al
	orl	%eax, %r10d
	movl	%ecx, %eax
	shrl	$31, %eax
	orb	%al, %r10b
	je	.L101
.L63:
	movl	$1, (%r15)
.L60:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movl	8(%rdi), %r11d
	leal	-2(%r11), %eax
	cmpl	%ecx, %eax
	jle	.L63
	cmpb	$0, 24(%rdi)
	je	.L65
	movl	$30, (%r15)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L65:
	andl	%r9d, %r8d
	leal	1(%rdx), %eax
	movl	%ecx, -84(%rbp)
	movl	%r11d, -72(%rbp)
	movl	%r9d, -68(%rbp)
	movl	%r8d, -64(%rbp)
	movb	%r10b, -88(%rbp)
	movl	%edx, -80(%rbp)
	movl	%eax, -60(%rbp)
	call	_ZL8_findRowP13UPropsVectorsi
	movl	-80(%rbp), %edx
	movq	%rax, -56(%rbp)
	movl	%edx, %esi
	call	_ZL8_findRowP13UPropsVectorsi
	movl	-84(%rbp), %ecx
	movl	-64(%rbp), %r8d
	movq	%rax, %r14
	movq	-56(%rbp), %rax
	movl	-68(%rbp), %r9d
	leal	2(%rcx), %r13d
	movl	-72(%rbp), %r11d
	movslq	%r13d, %r13
	cmpl	%r12d, (%rax)
	leaq	0(,%r13,4), %rdi
	movq	%rdi, -80(%rbp)
	je	.L66
	movl	(%rax,%r13,4), %edi
	movl	-60(%rbp), %esi
	movzbl	-88(%rbp), %r10d
	andl	%r9d, %edi
	cmpl	%r8d, %edi
	movl	%edi, %eax
	setne	-64(%rbp)
	cmpl	%esi, 4(%r14)
	je	.L67
	movl	(%r14,%r13,4), %ecx
	andl	%r9d, %ecx
	cmpl	%r8d, %ecx
	je	.L67
.L99:
	movl	$1, -68(%rbp)
	movl	$1, %r10d
.L68:
	movl	16(%rbx), %edi
	movzbl	-64(%rbp), %eax
	movl	%eax, -88(%rbp)
	addl	%edi, %eax
	addl	-68(%rbp), %eax
	movl	%eax, -84(%rbp)
	movl	%eax, %esi
	movl	12(%rbx), %eax
	movl	%edi, -72(%rbp)
	cmpl	%eax, %esi
	jg	.L70
	movq	(%rbx), %rcx
	movslq	%r11d, %r13
.L71:
	movl	-72(%rbp), %eax
	leaq	0(,%r13,4), %r15
	leaq	(%r14,%r15), %rsi
	imull	%r11d, %eax
	movq	%rsi, -96(%rbp)
	cltq
	leaq	(%rcx,%rax,4), %rax
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	$2, %rdx
	testl	%edx, %edx
	jle	.L74
	movl	-68(%rbp), %edi
	movslq	%eax, %rdx
	movl	-88(%rbp), %eax
	movl	%r9d, -108(%rbp)
	movl	%r8d, -104(%rbp)
	leal	1(%rax,%rdi), %eax
	movb	%r10b, -72(%rbp)
	imull	%r11d, %eax
	movl	%r11d, -68(%rbp)
	cltq
	leaq	(%r14,%rax,4), %rdi
	call	memmove@PLT
	movl	-108(%rbp), %r9d
	movl	-104(%rbp), %r8d
	movzbl	-72(%rbp), %r10d
	movl	-68(%rbp), %r11d
.L74:
	movl	-84(%rbp), %eax
	cmpb	$0, -64(%rbp)
	movl	%eax, 16(%rbx)
	jne	.L102
.L75:
	testb	%r10b, %r10b
	jne	.L103
.L69:
	movq	%r14, %rax
	subq	(%rbx), %rax
	movl	%r9d, %r15d
	sarq	$2, %rax
	notl	%r15d
	cqto
	idivq	%r13
	movl	%eax, 20(%rbx)
	movq	-80(%rbp), %rbx
	movq	-56(%rbp), %rax
	addq	%rbx, %r14
	addq	%rbx, %rax
	movl	(%rax), %edx
	andl	%r15d, %edx
	orl	%r8d, %edx
	movl	%edx, (%rax)
	cmpq	%r14, %rax
	je	.L60
	salq	$2, %r13
	cmpl	$1, %r11d
	jne	.L78
	.p2align 4,,10
	.p2align 3
.L77:
	addq	%r13, %rax
	movl	(%rax), %edx
	andl	%r15d, %edx
	orl	%r8d, %edx
	movl	%edx, (%rax)
	cmpq	%rax, %r14
	jne	.L77
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L78:
	addq	%r13, %rax
	movl	(%rax), %edx
	andl	%r15d, %edx
	orl	%r8d, %edx
	movl	%edx, (%rax)
	cmpq	%rax, %r14
	jne	.L78
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L66:
	movl	-60(%rbp), %eax
	cmpl	4(%r14), %eax
	je	.L100
	movl	(%r14,%r13,4), %eax
	andl	%r9d, %eax
	cmpl	%eax, %r8d
	je	.L100
	movb	$0, -64(%rbp)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L67:
	movslq	%r11d, %r13
	cmpl	%r8d, %eax
	je	.L69
	movl	$0, -68(%rbp)
	movb	$1, -64(%rbp)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L70:
	cmpl	$65535, %eax
	jle	.L81
	movl	$1114114, -96(%rbp)
	cmpl	$1114113, %eax
	jg	.L104
.L72:
	movl	-96(%rbp), %edi
	movl	%r9d, -116(%rbp)
	movl	%r8d, -112(%rbp)
	imull	%r11d, %edi
	movb	%r10b, -108(%rbp)
	movl	%r11d, -104(%rbp)
	sall	$2, %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movl	-104(%rbp), %r11d
	movzbl	-108(%rbp), %r10d
	testq	%rax, %rax
	movl	-112(%rbp), %r8d
	movl	-116(%rbp), %r9d
	je	.L105
	movslq	-72(%rbp), %rdx
	movslq	%r11d, %r13
	movq	(%rbx), %r15
	movq	%rax, %rdi
	movl	%r9d, -120(%rbp)
	imulq	%r13, %rdx
	movq	%r15, %rsi
	movl	%r8d, -116(%rbp)
	subq	%r15, %r14
	movb	%r10b, -112(%rbp)
	movl	%r11d, -108(%rbp)
	salq	$2, %rdx
	call	memcpy@PLT
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	-56(%rbp), %rax
	movq	%rcx, -104(%rbp)
	addq	%rcx, %r14
	subq	%r15, %rax
	addq	%rcx, %rax
	movq	%rax, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-104(%rbp), %rcx
	movl	-96(%rbp), %eax
	movl	-120(%rbp), %r9d
	movl	-116(%rbp), %r8d
	movzbl	-112(%rbp), %r10d
	movl	-108(%rbp), %r11d
	movq	%rcx, (%rbx)
	movl	%eax, 12(%rbx)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L100:
	movslq	%r11d, %r13
	jmp	.L69
.L102:
	movq	-56(%rbp), %rax
	movl	%r9d, -84(%rbp)
	movl	%r8d, -72(%rbp)
	subq	%rax, %r14
	leaq	(%rax,%r15), %rcx
	movq	%rax, %rsi
	movb	%r10b, -68(%rbp)
	sarq	$2, %r14
	movq	%rcx, %rdi
	movl	%r11d, -64(%rbp)
	leal	(%r11,%r14), %edx
	movq	%rax, %r14
	movslq	%edx, %rdx
	salq	$2, %rdx
	call	memmove@PLT
	movl	-84(%rbp), %r9d
	movl	-72(%rbp), %r8d
	movzbl	-68(%rbp), %r10d
	movl	-64(%rbp), %r11d
	movl	%r12d, (%rax)
	movq	%rax, -56(%rbp)
	movl	%r12d, 4(%r14)
	movq	-96(%rbp), %r14
	jmp	.L75
.L103:
	leaq	(%r14,%r15), %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	%r9d, -72(%rbp)
	movq	%rcx, %rdi
	movl	%r8d, -68(%rbp)
	movl	%r11d, -64(%rbp)
	call	memcpy@PLT
	movl	-72(%rbp), %r9d
	movl	-68(%rbp), %r8d
	movq	%rax, %rcx
	movl	-60(%rbp), %eax
	movl	-64(%rbp), %r11d
	movl	%eax, (%rcx)
	movl	%eax, 4(%r14)
	jmp	.L69
.L81:
	movl	$65536, -96(%rbp)
	jmp	.L72
.L104:
	movl	$5, (%r15)
	jmp	.L60
.L105:
	movl	$7, (%r15)
	jmp	.L60
	.cfi_endproc
.LFE2743:
	.size	upvec_setValue_67, .-upvec_setValue_67
	.p2align 4
	.globl	upvec_getValue_67
	.type	upvec_getValue_67, @function
upvec_getValue_67:
.LFB2744:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpb	$0, 24(%rdi)
	jne	.L112
	cmpl	$1114113, %esi
	ja	.L113
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	subq	$8, %rsp
	testl	%edx, %edx
	js	.L109
	movl	8(%rdi), %ecx
	leal	-2(%rcx), %edx
	cmpl	%ebx, %edx
	jle	.L106
	call	_ZL8_findRowP13UPropsVectorsi
	addl	$2, %ebx
	movslq	%ebx, %rbx
	movl	(%rax,%rbx,4), %eax
.L106:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2744:
	.size	upvec_getValue_67, .-upvec_getValue_67
	.p2align 4
	.globl	upvec_getRow_67
	.type	upvec_getRow_67, @function
upvec_getRow_67:
.LFB2745:
	.cfi_startproc
	endbr64
	cmpb	$0, 24(%rdi)
	jne	.L121
	testl	%esi, %esi
	js	.L121
	cmpl	%esi, 16(%rdi)
	jle	.L121
	imull	8(%rdi), %esi
	movq	(%rdi), %rax
	movslq	%esi, %rsi
	leaq	(%rax,%rsi,4), %rax
	testq	%rdx, %rdx
	je	.L118
	movl	(%rax), %esi
	movl	%esi, (%rdx)
.L118:
	testq	%rcx, %rcx
	je	.L119
	movl	4(%rax), %edx
	subl	$1, %edx
	movl	%edx, (%rcx)
.L119:
	addq	$8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2745:
	.size	upvec_getRow_67, .-upvec_getRow_67
	.p2align 4
	.globl	upvec_compact_67
	.type	upvec_compact_67, @function
upvec_compact_67:
.LFB2747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r10d
	movq	%rsi, -72(%rbp)
	movq	%rdx, -80(%rbp)
	testl	%r10d, %r10d
	jg	.L128
	movq	%rcx, %rbx
	testq	%rsi, %rsi
	je	.L157
	cmpb	$0, 24(%rdi)
	movq	%rdi, %r12
	jne	.L128
	subq	$8, %rsp
	movl	16(%rdi), %eax
	movl	8(%rdi), %r15d
	movb	$1, 24(%rdi)
	pushq	%rcx
	movq	%rdi, %r8
	movq	(%rdi), %rdi
	xorl	%r9d, %r9d
	leal	0(,%r15,4), %edx
	leaq	_ZL17upvec_compareRowsPKvS0_S0_(%rip), %rcx
	movl	%eax, %esi
	movl	%eax, -52(%rbp)
	call	uprv_sortArray_67@PLT
	movl	(%rbx), %r9d
	popq	%rdi
	popq	%r8
	testl	%r9d, %r9d
	jg	.L128
	movl	$2, %eax
	leal	-2(%r15), %r13d
	movl	-52(%rbp), %esi
	movq	(%r12), %r14
	movl	%eax, %ecx
	movslq	%r13d, %rax
	leaq	0(,%rax,4), %rdx
	subl	%r15d, %ecx
	movq	%rdx, %rdi
	movl	%ecx, -56(%rbp)
	movq	%rdx, -104(%rbp)
	negq	%rdi
	testl	%esi, %esi
	jle	.L133
	leal	0(,%r13,4), %eax
	xorl	%r10d, %r10d
	movq	%r12, -112(%rbp)
	movl	%ecx, %r12d
	cltq
	movl	%r10d, %r15d
	movq	%rax, -96(%rbp)
	leaq	8(%rdx), %rax
	movq	%rax, -64(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%rax, %r14
.L138:
	movl	(%r14), %r11d
	testl	%r12d, %r12d
	js	.L134
	movq	-96(%rbp), %rdx
	movq	%r14, %rsi
	leaq	8(%r14), %rdi
	subq	-104(%rbp), %rsi
	movl	%r11d, -88(%rbp)
	call	memcmp@PLT
	movl	-88(%rbp), %r11d
	testl	%eax, %eax
	je	.L135
.L134:
	addl	%r13d, %r12d
.L135:
	cmpl	$1114111, %r11d
	jle	.L139
	subq	$8, %rsp
	movq	-72(%rbp), %rax
	movl	%r12d, %ecx
	movl	%r11d, %edx
	pushq	%rbx
	movq	-80(%rbp), %rdi
	movl	%r13d, %r9d
	leaq	8(%r14), %r8
	movl	%r11d, %esi
	call	*%rax
	movl	(%rbx), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L128
.L139:
	movq	-64(%rbp), %rax
	addl	$1, %r15d
	addq	%r14, %rax
	cmpl	%r15d, -52(%rbp)
	jne	.L158
	subq	$8, %rsp
	movl	%r12d, %ecx
	movq	-72(%rbp), %rax
	movl	$2097152, %edx
	pushq	%rbx
	addl	%r13d, %ecx
	movq	-112(%rbp), %r12
	movl	%r13d, %r9d
	movq	-80(%rbp), %rdi
	leaq	8(%r14), %r8
	movl	$2097152, %esi
	call	*%rax
	movl	(%rbx), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L128
	movq	(%r12), %rcx
	xorl	%eax, %eax
	movl	%r13d, -116(%rbp)
	movq	%r12, -88(%rbp)
	movl	-56(%rbp), %r13d
	leaq	8(%rcx), %r15
	movq	%rbx, -128(%rbp)
	movl	%eax, %ebx
	movq	%r15, %r12
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L154:
	movq	-88(%rbp), %rax
	movq	(%rax), %rcx
.L146:
	movl	-4(%r12), %eax
	movl	-8(%r12), %r14d
	movl	%eax, -56(%rbp)
	testl	%r13d, %r13d
	js	.L140
	movslq	%r13d, %r8
	movq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rcx, -112(%rbp)
	leaq	0(,%r8,4), %r15
	leaq	(%rcx,%r15), %rsi
	call	memcmp@PLT
	movq	-112(%rbp), %rcx
	testl	%eax, %eax
	je	.L141
.L140:
	addl	-116(%rbp), %r13d
	movq	-104(%rbp), %rdx
	movq	%r12, %rsi
	movslq	%r13d, %r8
	leaq	0(,%r8,4), %r15
	leaq	(%rcx,%r15), %rdi
	call	memmove@PLT
.L141:
	cmpl	$1114111, %r14d
	jg	.L145
	movq	-88(%rbp), %rcx
	movl	-56(%rbp), %edx
	subq	$8, %rsp
	movl	%r14d, %esi
	movl	-116(%rbp), %r9d
	movq	-72(%rbp), %r10
	pushq	-128(%rbp)
	movq	(%rcx), %r8
	subl	$1, %edx
	movl	%r13d, %ecx
	movq	-80(%rbp), %rdi
	addq	%r15, %r8
	call	*%r10
	movq	-128(%rbp), %rax
	popq	%r9
	popq	%r10
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L128
.L145:
	addl	$1, %ebx
	addq	-64(%rbp), %r12
	cmpl	%ebx, -52(%rbp)
	jne	.L154
	movl	%r13d, -56(%rbp)
	movq	-88(%rbp), %r12
	movl	-116(%rbp), %r13d
.L143:
	movl	-56(%rbp), %eax
	cltd
	idivl	%r13d
	addl	$1, %eax
	movl	%eax, 16(%r12)
.L128:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movl	$1, (%rcx)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L133:
	.cfi_restore_state
	subq	$8, %rsp
	leaq	(%r14,%rdi), %r8
	movl	$2097152, %esi
	movl	%r13d, %r9d
	pushq	%rbx
	movq	-80(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$2097152, %edx
	movq	-72(%rbp), %rax
	call	*%rax
	movl	(%rbx), %r8d
	popq	%rsi
	popq	%rdi
	testl	%r8d, %r8d
	jg	.L128
	jmp	.L143
	.cfi_endproc
.LFE2747:
	.size	upvec_compact_67, .-upvec_compact_67
	.p2align 4
	.globl	upvec_getArray_67
	.type	upvec_getArray_67, @function
upvec_getArray_67:
.LFB2748:
	.cfi_startproc
	endbr64
	cmpb	$0, 24(%rdi)
	je	.L163
	testq	%rsi, %rsi
	je	.L161
	movl	16(%rdi), %eax
	movl	%eax, (%rsi)
.L161:
	testq	%rdx, %rdx
	je	.L162
	movl	8(%rdi), %eax
	subl	$2, %eax
	movl	%eax, (%rdx)
.L162:
	movq	(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2748:
	.size	upvec_getArray_67, .-upvec_getArray_67
	.p2align 4
	.globl	upvec_cloneArray_67
	.type	upvec_cloneArray_67, @function
upvec_cloneArray_67:
.LFB2749:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jg	.L183
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 24(%rdi)
	je	.L186
	movl	8(%rdi), %eax
	movq	%rsi, %r14
	movq	%rdx, %r13
	leal	-2(%rax), %r12d
	imull	16(%rdi), %r12d
	sall	$2, %r12d
	movslq	%r12d, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L187
	movq	(%rbx), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	testq	%r14, %r14
	je	.L174
	movl	16(%rbx), %eax
	movl	%eax, (%r14)
.L174:
	testq	%r13, %r13
	je	.L170
	movl	8(%rbx), %eax
	subl	$2, %eax
	movl	%eax, 0(%r13)
.L170:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$1, (%rcx)
	jmp	.L170
.L187:
	movl	$7, (%r15)
	jmp	.L170
	.cfi_endproc
.LFE2749:
	.size	upvec_cloneArray_67, .-upvec_cloneArray_67
	.p2align 4
	.globl	upvec_compactToUTrie2WithRowIndexes_67
	.type	upvec_compactToUTrie2WithRowIndexes_67, @function
upvec_compactToUTrie2WithRowIndexes_67:
.LFB2750:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r11d
	testl	%r11d, %r11d
	jg	.L191
	cmpb	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L222
.L191:
	xorl	%r12d, %r12d
.L190:
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	utrie2_freeze_67@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jle	.L188
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	utrie2_close_67@PLT
.L188:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	subq	$8, %rsp
	movl	16(%rdi), %r15d
	movl	8(%rdi), %r12d
	movb	$1, 24(%rdi)
	pushq	%rsi
	movq	%rdi, %r8
	movq	(%rdi), %rdi
	xorl	%r9d, %r9d
	leal	0(,%r12,4), %edx
	leaq	_ZL17upvec_compareRowsPKvS0_S0_(%rip), %rcx
	movl	%r15d, %esi
	movl	%r15d, -52(%rbp)
	call	uprv_sortArray_67@PLT
	movl	0(%r13), %r10d
	popq	%r8
	popq	%r9
	testl	%r10d, %r10d
	jg	.L191
	movl	$2, %eax
	leal	-2(%r12), %ecx
	movq	(%rbx), %r14
	subl	%r12d, %eax
	movl	%ecx, -56(%rbp)
	movl	%eax, -116(%rbp)
	movslq	%ecx, %rax
	salq	$2, %rax
	movq	%rax, -72(%rbp)
	testl	%r15d, %r15d
	jle	.L223
	movl	%ecx, %eax
	movl	-116(%rbp), %ecx
	addq	$8, %r14
	xorl	%r12d, %r12d
	sall	$2, %eax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, -104(%rbp)
	cltq
	movq	%rbx, -112(%rbp)
	movl	%ecx, %r12d
	movl	%r9d, %ebx
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	movq	%r13, -80(%rbp)
	movq	%r14, %r13
	movl	%r8d, %r14d
	addq	$8, %rax
	movl	$0, -96(%rbp)
	movq	%rax, -64(%rbp)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L225:
	cmpl	$1114112, %r15d
	cmove	%r12d, %ebx
.L197:
	movq	-80(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L220
.L196:
	addl	$1, %r14d
	addq	-64(%rbp), %r13
	cmpl	%r14d, -52(%rbp)
	je	.L224
.L200:
	movl	-8(%r13), %r15d
	testl	%r12d, %r12d
	js	.L194
	movq	$-8, %rsi
	movq	-88(%rbp), %rdx
	subq	-72(%rbp), %rsi
	movq	%r13, %rdi
	addq	%r13, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L195
.L194:
	addl	-56(%rbp), %r12d
.L195:
	cmpl	$1114111, %r15d
	jle	.L196
	cmpl	$1114113, %r15d
	je	.L210
	cmpl	$2097152, %r15d
	jne	.L225
	cmpl	$65535, %r12d
	jle	.L199
	movq	-80(%rbp), %r13
	movq	-104(%rbp), %r12
	movl	$8, 0(%r13)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L210:
	movl	%r12d, -96(%rbp)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L224:
	movl	%r12d, %ecx
	addl	-56(%rbp), %ecx
	movl	%ebx, %r9d
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %rbx
	movq	-80(%rbp), %r13
	cmpl	$65535, %ecx
	jle	.L201
	movl	$8, 0(%r13)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L199:
	movq	-80(%rbp), %rdx
	movl	-96(%rbp), %esi
	movl	%ebx, %edi
	call	utrie2_open_67@PLT
	movq	%rax, -104(%rbp)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L201:
	movl	-96(%rbp), %esi
	movq	%r13, %rdx
	movl	%r9d, %edi
	call	utrie2_open_67@PLT
	movq	%rax, %r12
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L190
	movl	-56(%rbp), %eax
	movq	(%rbx), %rcx
	movq	%r12, -104(%rbp)
	movq	%rbx, -88(%rbp)
	movl	-116(%rbp), %ebx
	sall	$2, %eax
	leaq	8(%rcx), %r15
	movq	%r13, -112(%rbp)
	cltq
	movq	%r15, %r13
	movq	%rcx, %r15
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movl	%eax, %r12d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L218:
	movq	-88(%rbp), %rax
	movq	(%rax), %r15
.L207:
	movl	-4(%r13), %eax
	movl	-8(%r13), %r14d
	movl	%eax, -80(%rbp)
	testl	%ebx, %ebx
	js	.L202
	movslq	%ebx, %rax
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	leaq	(%r15,%rax,4), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L203
.L202:
	addl	-56(%rbp), %ebx
	movq	-72(%rbp), %rdx
	movq	%r13, %rsi
	movslq	%ebx, %rax
	leaq	(%r15,%rax,4), %rdi
	call	memmove@PLT
.L203:
	cmpl	$1114111, %r14d
	jg	.L206
	movq	-112(%rbp), %r15
	movl	-80(%rbp), %edx
	movl	%ebx, %ecx
	movl	$1, %r8d
	movq	-104(%rbp), %rdi
	movl	%r14d, %esi
	subl	$1, %edx
	movq	%r15, %r9
	call	utrie2_setRange32_67@PLT
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L226
.L206:
	addl	$1, %r12d
	addq	-64(%rbp), %r13
	cmpl	%r12d, -52(%rbp)
	jne	.L218
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	movl	%ebx, -116(%rbp)
	movq	-88(%rbp), %rbx
.L193:
	movl	-116(%rbp), %eax
	cltd
	idivl	-56(%rbp)
	addl	$1, %eax
	movl	%eax, 16(%rbx)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L226:
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	jmp	.L190
.L223:
	xorl	%edi, %edi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	utrie2_open_67@PLT
	movl	0(%r13), %edi
	movq	%rax, %r12
	testl	%edi, %edi
	jg	.L190
	jmp	.L193
.L220:
	movq	-104(%rbp), %r12
	movq	-80(%rbp), %r13
	jmp	.L190
	.cfi_endproc
.LFE2750:
	.size	upvec_compactToUTrie2WithRowIndexes_67, .-upvec_compactToUTrie2WithRowIndexes_67
	.p2align 4
	.globl	upvec_compactToUTrie2Handler_67
	.type	upvec_compactToUTrie2Handler_67, @function
upvec_compactToUTrie2Handler_67:
.LFB2751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rbp), %r9
	cmpl	$1114111, %esi
	jle	.L234
	cmpl	$1114113, %esi
	je	.L229
	cmpl	$2097152, %esi
	je	.L230
	cmpl	$1114112, %esi
	je	.L235
.L227:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	(%rdi), %rdi
	addq	$8, %rsp
	movl	$1, %r8d
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	utrie2_setRange32_67@PLT
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movl	%ecx, 8(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movl	%ecx, 16(%rdi)
	cmpl	$65535, %ecx
	jle	.L232
	movl	$8, (%r9)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movl	%ecx, 12(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movl	12(%rdi), %esi
	movl	8(%rdi), %edi
	movq	%r9, %rdx
	call	utrie2_open_67@PLT
	movq	%rax, (%rbx)
	jmp	.L227
	.cfi_endproc
.LFE2751:
	.size	upvec_compactToUTrie2Handler_67, .-upvec_compactToUTrie2Handler_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
