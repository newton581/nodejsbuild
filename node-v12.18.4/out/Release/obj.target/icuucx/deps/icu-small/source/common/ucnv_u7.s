	.file	"ucnv_u7.cpp"
	.text
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	_UTF7ToUnicodeWithOffsets, @function
_UTF7ToUnicodeWithOffsets:
.LFB2115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	8(%rdi), %r14
	movq	24(%rdi), %rax
	movq	40(%rdi), %rbx
	movq	16(%rdi), %rdx
	movl	72(%r14), %ecx
	movzbl	64(%r14), %r13d
	movq	%rax, -48(%rbp)
	movq	48(%rdi), %rsi
	movq	32(%rdi), %rax
	movq	%rbx, -64(%rbp)
	movl	%ecx, %r11d
	movl	%ecx, %edi
	shrl	$16, %r11d
	testb	%r13b, %r13b
	setne	%r9b
	negl	%r9d
	andl	$16777216, %ecx
	je	.L53
.L3:
	movq	-64(%rbp), %r8
	movq	-48(%rbp), %rcx
	subq	%rax, %r8
	subq	%rdx, %rcx
	sarq	%r8
	cmpl	%r8d, %ecx
	cmovge	%r8d, %ecx
	testl	%ecx, %ecx
	jle	.L54
	subl	$1, %ecx
	leaq	2(%rax,%rcx,2), %r8
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L5:
	cmpb	$43, %cl
	je	.L7
	addq	$2, %rax
	movw	%cx, -2(%rax)
	testq	%rsi, %rsi
	je	.L8
	movl	%r9d, (%rsi)
	addl	$1, %r9d
	addq	$4, %rsi
.L8:
	cmpq	%r8, %rax
	je	.L135
.L9:
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	leal	-32(%rcx), %r10d
	cmpb	$93, %r10b
	ja	.L84
	cmpb	$92, %cl
	jne	.L5
.L84:
	leal	-9(%rcx), %r10d
	cmpb	$1, %r10b
	jbe	.L5
	cmpb	$13, %cl
	je	.L5
	movq	-88(%rbp), %rbx
	movb	%cl, 65(%r14)
	movq	%rax, %r8
	movl	$1, %r9d
	movl	$1, %r13d
	movl	$12, (%rbx)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%r8d, %r8d
.L2:
	leaq	65(%r14), %rbx
	movq	%r15, -96(%rbp)
	leaq	.L33(%rip), %r10
	movl	%r9d, %r15d
	movq	%rbx, -72(%rbp)
	movq	%r14, %rbx
	movl	%r8d, %r14d
.L38:
	cmpq	-64(%rbp), %rax
	jnb	.L123
	movl	%r15d, -56(%rbp)
	leal	1(%r11), %r8d
.L22:
	cmpq	-48(%rbp), %rdx
	jnb	.L139
	leal	1(%r13), %ecx
	movq	-72(%rbp), %r15
	movzbl	(%rdx), %r9d
	leaq	1(%rdx), %r12
	movb	%cl, -73(%rbp)
	movzbl	%r13b, %ecx
	movb	%r9b, (%r15,%rcx)
	leal	1(%r14), %ecx
	movl	%ecx, -80(%rbp)
	cmpb	$125, %r9b
	ja	.L75
	movzbl	%r9b, %ecx
	leaq	_ZL10fromBase64(%rip), %r15
	movsbw	(%r15,%rcx), %cx
	cmpb	$-3, %cl
	je	.L131
	cmpb	$-1, %cl
	je	.L131
	testb	%cl, %cl
	js	.L140
	cmpb	$8, %r8b
	ja	.L76
	movzbl	%r8b, %r8d
	movl	-56(%rbp), %r15d
	movslq	(%r10,%r8,4), %r8
	addq	%r10, %r8
	notrack jmp	*%r8
	.section	.rodata
	.align 4
	.align 4
.L33:
	.long	.L37-.L33
	.long	.L37-.L33
	.long	.L34-.L33
	.long	.L36-.L33
	.long	.L34-.L33
	.long	.L34-.L33
	.long	.L35-.L33
	.long	.L34-.L33
	.long	.L32-.L33
	.text
.L35:
	movsbl	%cl, %r11d
	sall	$2, %edi
	movq	$0, -56(%rbp)
	leaq	2(%rax), %r8
	sarl	$4, %r11d
	orl	%r11d, %edi
	movw	%di, (%rax)
	testq	%rsi, %rsi
	je	.L41
	leaq	4(%rsi), %rax
	movl	%r15d, (%rsi)
	movl	%r14d, %r15d
	movq	%rax, -56(%rbp)
.L41:
	movl	%ecx, %edi
	movb	%r9b, 65(%rbx)
	andl	$15, %edi
	cmpq	%r12, -48(%rbp)
	jbe	.L141
	cmpq	%r8, -64(%rbp)
	jbe	.L142
	movzbl	(%r12), %eax
	addq	$2, %rdx
	addl	$2, %r14d
	movb	%al, 66(%rbx)
	cmpb	$125, %al
	ja	.L68
	leaq	_ZL10fromBase64(%rip), %rsi
	movsbw	(%rsi,%rax), %cx
	cmpb	$-3, %cl
	je	.L68
	cmpb	$-1, %cl
	je	.L69
	testb	%cl, %cl
	js	.L143
	movl	$7, %r11d
	movl	$2, %r13d
.L15:
	sall	$6, %edi
	movq	-56(%rbp), %rsi
	movq	%r8, %rax
	orl	%ecx, %edi
	jmp	.L38
.L36:
	movsbl	%cl, %r11d
	sall	$4, %edi
	movq	$0, -56(%rbp)
	leaq	2(%rax), %r8
	sarl	$2, %r11d
	orl	%r11d, %edi
	movw	%di, (%rax)
	testq	%rsi, %rsi
	je	.L39
	leaq	4(%rsi), %rax
	movl	%r15d, (%rsi)
	movl	%r14d, %r15d
	movq	%rax, -56(%rbp)
.L39:
	movl	%ecx, %edi
	movb	%r9b, 65(%rbx)
	andl	$3, %edi
	cmpq	%r12, -48(%rbp)
	jbe	.L144
	cmpq	%r8, -64(%rbp)
	jbe	.L12
	movzbl	(%r12), %eax
	addq	$2, %rdx
	addl	$2, %r14d
	movb	%al, 66(%rbx)
	cmpb	$125, %al
	ja	.L56
	leaq	_ZL10fromBase64(%rip), %rsi
	movsbw	(%rsi,%rax), %cx
	cmpb	$-3, %cl
	je	.L56
	cmpb	$-1, %cl
	je	.L57
	testb	%cl, %cl
	js	.L145
	movl	$4, %r11d
	movl	$2, %r13d
	jmp	.L15
.L32:
	sall	$6, %edi
	leaq	2(%rax), %r8
	orl	%ecx, %edi
	movw	%di, (%rax)
	testq	%rsi, %rsi
	je	.L43
	leaq	4(%rsi), %rax
	movl	%r15d, (%rsi)
	movq	%rax, -56(%rbp)
	cmpq	%r12, -48(%rbp)
	jbe	.L146
	cmpq	%r8, -64(%rbp)
	jbe	.L65
	movzbl	(%r12), %eax
	leaq	2(%rdx), %r9
	leal	2(%r14), %r11d
	movb	%al, 65(%rbx)
	cmpb	$125, %al
	ja	.L129
	leaq	_ZL10fromBase64(%rip), %rdx
	movsbw	(%rdx,%rax), %di
	cmpb	$-3, %dil
	je	.L129
	cmpb	$-1, %dil
	je	.L130
	testb	%dil, %dil
	js	.L147
	movl	-80(%rbp), %r15d
.L20:
	cmpq	%r9, -48(%rbp)
	jbe	.L148
	movb	$1, -73(%rbp)
	movl	$1, %ecx
	movl	$2, %r13d
.L52:
	movzbl	(%r9), %eax
	movq	-72(%rbp), %rsi
	leaq	1(%r9), %rdx
	leal	1(%r11), %r14d
	movb	%al, (%rsi,%rcx)
	cmpb	$125, %al
	ja	.L72
	leaq	_ZL10fromBase64(%rip), %rsi
	movsbw	(%rsi,%rax), %cx
	cmpb	$-3, %cl
	je	.L72
	cmpb	$-1, %cl
	je	.L73
	testb	%cl, %cl
	js	.L149
	movl	$2, %r11d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L43:
	cmpq	%r12, -48(%rbp)
	jbe	.L137
	cmpq	%r8, -64(%rbp)
	jbe	.L136
	movzbl	(%r12), %eax
	leaq	2(%rdx), %r9
	leal	2(%r14), %r11d
	movb	%al, 65(%rbx)
	cmpb	$125, %al
	ja	.L62
	leaq	_ZL10fromBase64(%rip), %rdx
	movsbw	(%rdx,%rax), %di
	cmpb	$-3, %dil
	je	.L62
	cmpb	$-1, %dil
	je	.L63
	testb	%dil, %dil
	js	.L150
	movq	$0, -56(%rbp)
	jmp	.L20
.L54:
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L135:
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
.L4:
	movzbl	%r11b, %ecx
	movzwl	%di, %r11d
	sall	$16, %ecx
	orl	%r11d, %ecx
	orl	$16777216, %ecx
	cmpq	-48(%rbp), %rdx
	jnb	.L10
	cmpq	%r8, -64(%rbp)
	ja	.L10
.L17:
	movq	-88(%rbp), %rax
	movl	$15, (%rax)
.L46:
	movl	%ecx, 72(%r14)
	movb	%r9b, 64(%r14)
	movq	%rdx, 16(%r15)
	movq	%r8, 32(%r15)
	movq	%rsi, 48(%r15)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L146:
	.cfi_restore_state
	movq	%rax, %rsi
.L137:
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L10:
	movq	-88(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L85
	cmpb	$0, 2(%r15)
	je	.L85
	cmpq	-48(%rbp), %rdx
	jne	.L85
	xorl	%r9d, %r9d
	testw	%di, %di
	je	.L46
	.p2align 4,,10
	.p2align 3
.L85:
	movl	%r13d, %r9d
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L7:
	addl	$1, %r9d
	movl	$-1, %r11d
	xorl	%edi, %edi
	xorl	%r13d, %r13d
	movl	%r9d, %r8d
	jmp	.L2
.L37:
	movsbw	%cl, %di
	cmpq	%r12, -48(%rbp)
	jbe	.L151
	movq	%rsi, -56(%rbp)
	movzbl	-73(%rbp), %ecx
	addl	$2, %r13d
	movq	%r12, %r9
	movl	-80(%rbp), %r11d
	movq	%rax, %r8
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L131:
	movq	-96(%rbp), %r15
	movl	%r14d, %r8d
	movq	%rbx, %r14
.L26:
	movl	%r8d, %r9d
	cmpb	$-1, %r11b
	jne	.L14
	movq	-88(%rbp), %rbx
	movzwl	%di, %edi
	movb	$43, 65(%r14)
	movq	%rax, %r8
	orl	$33488896, %edi
	movl	$1, %r9d
	movl	$12, (%rbx)
	movl	%edi, %ecx
	jmp	.L46
.L73:
	movl	%r13d, %eax
	movq	%rbx, %r14
	movq	-96(%rbp), %r15
	movq	%r9, %rbx
	movq	-56(%rbp), %rsi
	movzbl	-73(%rbp), %r13d
	movq	%rdx, %r12
	movb	%al, -73(%rbp)
	movl	%r11d, %r9d
	movq	%r8, %rax
	movq	%rbx, %rdx
	movl	$1, %r11d
.L14:
	testw	%di, %di
	jne	.L152
	cmpb	$-3, %cl
	jne	.L3
	movzbl	%r11b, %ecx
	movzbl	-73(%rbp), %r9d
	movq	%r12, %rdx
	movq	%rax, %r8
	sall	$16, %ecx
	orl	$16777216, %ecx
.L50:
	movq	-88(%rbp), %rax
	movl	$12, (%rax)
	jmp	.L46
.L123:
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	cmpq	-48(%rbp), %rdx
	jnb	.L23
	movzbl	%r11b, %ecx
	movzwl	%di, %edi
	movl	%r13d, %r9d
	movq	%rax, %r8
	sall	$16, %ecx
	orl	%edi, %ecx
	jmp	.L17
.L139:
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
.L23:
	movzbl	%r11b, %ecx
	movzwl	%di, %r11d
	movq	%rax, %r8
	sall	$16, %ecx
	orl	%r11d, %ecx
	jmp	.L10
.L34:
	movq	%rsi, -56(%rbp)
	movl	-80(%rbp), %r14d
	addl	$1, %r11d
	movq	%r12, %rdx
	movzbl	-73(%rbp), %r13d
	movq	%rax, %r8
	jmp	.L15
.L152:
	movb	%r13b, -73(%rbp)
	movzbl	%r11b, %r11d
	movq	%rax, %r8
	movq	%rdx, %r12
	sall	$16, %r11d
.L51:
	movzwl	%di, %edi
	movq	-88(%rbp), %rax
	movzbl	-73(%rbp), %r9d
	movq	%r12, %rdx
	orl	%r11d, %edi
	orl	$16777216, %edi
	movl	$12, (%rax)
	movl	%edi, %ecx
	jmp	.L46
.L75:
	movl	%r14d, %r8d
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	movl	$-3, %ecx
	jmp	.L26
.L140:
	movl	-56(%rbp), %r9d
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	cmpb	$-1, %r11b
	jne	.L80
	movl	$43, %ecx
	leaq	2(%rax), %rdx
	movw	%cx, (%rax)
	testq	%rsi, %rsi
	je	.L81
	subl	$1, %r9d
	movq	%rdx, %rax
	addq	$4, %rsi
	movq	%r12, %rdx
	movl	%r9d, -4(%rsi)
	movl	-80(%rbp), %r9d
	jmp	.L3
.L56:
	movb	$1, -73(%rbp)
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	movl	$16973824, %ecx
	movq	-56(%rbp), %rsi
	movl	$196608, %r11d
	movl	$2, %r13d
.L13:
	movl	%r13d, %r9d
	testw	%di, %di
	jne	.L51
	jmp	.L50
.L68:
	movb	$1, -73(%rbp)
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	movl	$17170432, %ecx
	movq	-56(%rbp), %rsi
	movl	$393216, %r11d
	movl	$2, %r13d
	jmp	.L13
.L72:
	movq	-96(%rbp), %r15
	movq	-56(%rbp), %rsi
	movq	%rbx, %r14
	movq	%r9, %r12
	movl	$16842752, %ecx
	movl	$65536, %r11d
	jmp	.L13
.L148:
	movzwl	%di, %ecx
	movq	-96(%rbp), %r15
	movq	-56(%rbp), %rsi
	movq	%rbx, %r14
	orl	$65536, %ecx
	movq	%r9, %rdx
	movl	$1, %r13d
	jmp	.L10
.L129:
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	movq	%r9, %rbx
.L18:
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdx
	movl	$1, %r9d
	movl	$16777216, %ecx
	jmp	.L50
.L80:
	movl	-80(%rbp), %r9d
	movq	%r12, %rdx
.L16:
	testw	%di, %di
	je	.L3
	movzbl	%r11b, %ecx
	movzwl	%di, %edi
	movq	-88(%rbp), %rbx
	movzbl	-73(%rbp), %r9d
	sall	$16, %ecx
	movq	%rax, %r8
	orl	%ecx, %edi
	movl	$12, (%rbx)
	orl	$16777216, %edi
	movl	%edi, %ecx
	jmp	.L46
.L141:
	movzwl	%di, %ecx
	movq	-96(%rbp), %r15
	movq	-56(%rbp), %rsi
	movq	%rbx, %r14
	orl	$393216, %ecx
	movq	%r12, %rdx
	movl	$1, %r13d
	jmp	.L10
.L144:
	movzwl	%di, %ecx
	movq	-96(%rbp), %r15
	movq	-56(%rbp), %rsi
	movq	%rbx, %r14
	orl	$196608, %ecx
	movq	%r12, %rdx
	movl	$1, %r13d
	jmp	.L10
.L12:
	movzwl	%di, %edi
	movq	-96(%rbp), %r15
	movq	-56(%rbp), %rsi
	movq	%rbx, %r14
	orl	$196608, %edi
	movq	%r12, %rdx
	movl	$1, %r9d
	movl	%edi, %ecx
	jmp	.L17
.L65:
	movq	%rax, %rsi
.L136:
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	movq	%r12, %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	jmp	.L17
.L142:
	movzwl	%di, %edi
	movq	-96(%rbp), %r15
	movq	-56(%rbp), %rsi
	movq	%rbx, %r14
	orl	$393216, %edi
	movq	%r12, %rdx
	movl	$1, %r9d
	movl	%edi, %ecx
	jmp	.L17
.L57:
	movq	%r8, %rax
	movq	%rdx, %r8
	movb	$2, -73(%rbp)
	movq	%r12, %rdx
	movq	-96(%rbp), %r15
	movl	-80(%rbp), %r9d
	movq	%rbx, %r14
	movq	%r8, %r12
	movq	-56(%rbp), %rsi
	movl	$3, %r11d
	movl	$1, %r13d
	jmp	.L14
.L145:
	movl	%r14d, %r9d
	movb	$2, -73(%rbp)
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	movq	-56(%rbp), %rsi
	movq	%r8, %rax
	movl	$3, %r11d
	jmp	.L16
.L149:
	movl	%r14d, %r9d
	movb	%r13b, -73(%rbp)
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	movq	-56(%rbp), %rsi
	movq	%r8, %rax
	movl	$1, %r11d
	jmp	.L16
.L143:
	movl	%r14d, %r9d
	movb	$2, -73(%rbp)
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	movq	-56(%rbp), %rsi
	movq	%r8, %rax
	movl	$6, %r11d
	jmp	.L16
.L69:
	movq	%r8, %rax
	movq	%rdx, %r8
	movb	$2, -73(%rbp)
	movq	%r12, %rdx
	movq	-96(%rbp), %r15
	movl	-80(%rbp), %r9d
	movq	%rbx, %r14
	movq	%r8, %r12
	movq	-56(%rbp), %rsi
	movl	$6, %r11d
	movl	$1, %r13d
	jmp	.L14
.L130:
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
.L19:
	movl	-80(%rbp), %r9d
	movq	-56(%rbp), %rsi
	movq	%r8, %rax
	movq	%r12, %rdx
	xorl	%r11d, %r11d
	xorl	%edi, %edi
	jmp	.L3
.L147:
	movq	%rbx, %r14
	movq	%r9, %rbx
	movq	-96(%rbp), %r15
	movl	%r11d, %r9d
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r8, %rax
	xorl	%r11d, %r11d
	xorl	%edi, %edi
	jmp	.L3
.L62:
	movq	%rbx, %r14
	movq	$0, -56(%rbp)
	movq	-96(%rbp), %r15
	movq	%r9, %rbx
	jmp	.L18
.L81:
	movq	%rdx, %rax
	movl	-80(%rbp), %r9d
	movq	%r12, %rdx
	jmp	.L3
.L150:
	movq	%rbx, %r14
	movq	%r9, %rbx
	movq	-96(%rbp), %r15
	movl	%r11d, %r9d
	movq	%rbx, %rdx
	movq	%r8, %rax
	xorl	%r11d, %r11d
	xorl	%edi, %edi
	jmp	.L3
.L63:
	movq	$0, -56(%rbp)
	movq	-96(%rbp), %r15
	movq	%rbx, %r14
	jmp	.L19
.L151:
	movzwl	%di, %ecx
	movq	-96(%rbp), %r15
	movzbl	-73(%rbp), %r13d
	movq	%rbx, %r14
	orl	$65536, %ecx
	movq	%rax, %r8
	movq	%r12, %rdx
	jmp	.L10
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_UTF7ToUnicodeWithOffsets.cold, @function
_UTF7ToUnicodeWithOffsets.cold:
.LFSB2115:
.L76:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	-80(%rbp), %r14d
	movzbl	-73(%rbp), %r13d
	movq	%r12, %rdx
	jmp	.L22
	.cfi_endproc
.LFE2115:
	.text
	.size	_UTF7ToUnicodeWithOffsets, .-_UTF7ToUnicodeWithOffsets
	.section	.text.unlikely
	.size	_UTF7ToUnicodeWithOffsets.cold, .-_UTF7ToUnicodeWithOffsets.cold
.LCOLDE0:
	.text
.LHOTE0:
	.p2align 4
	.type	_UTF7FromUnicodeWithOffsets, @function
_UTF7FromUnicodeWithOffsets:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZL21encodeDirectlyMaximum(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	8(%rdi), %r15
	movq	24(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	40(%rdi), %rbx
	movl	80(%r15), %esi
	movq	48(%rdi), %rcx
	movq	%rax, -48(%rbp)
	movq	32(%rdi), %rax
	leaq	_ZL24encodeDirectlyRestricted(%rip), %rdi
	cmpl	$268435456, %esi
	movb	%sil, -57(%rbp)
	cmovb	%r8, %rdi
	movl	%esi, %r8d
	shrl	$16, %r8d
	andl	$16777216, %esi
	jne	.L201
	xorl	%r10d, %r10d
.L156:
	cmpq	-48(%rbp), %rdx
	jnb	.L164
	movzbl	-57(%rbp), %r14d
	leaq	_ZL8toBase64(%rip), %r9
	cmpq	%rbx, %rax
	jb	.L165
.L257:
	movl	%r10d, %r9d
	movq	%rcx, %rdi
	movq	%rax, %r11
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L260:
	testb	%r8b, %r8b
	je	.L258
.L176:
	cmpq	%rdx, -48(%rbp)
	jbe	.L255
	cmpq	%rax, %rbx
	jbe	.L205
.L165:
	movq	%rdx, %r12
	movzwl	(%rdx), %esi
	leaq	2(%rdx), %rdx
	cmpw	$127, %si
	ja	.L170
	movzwl	%si, %r11d
	cmpb	$0, (%rdi,%r11)
	jne	.L259
.L170:
	cmpb	$1, %r8b
	je	.L174
	cmpb	$2, %r8b
	jne	.L260
	movzwl	%si, %r8d
	movzbl	%r14b, %r14d
	leaq	1(%rax), %r11
	andl	$63, %esi
	movl	%r8d, %r12d
	sarl	$6, %r8d
	movzbl	(%r9,%rsi), %esi
	sarl	$12, %r12d
	andl	$63, %r8d
	orl	%r12d, %r14d
	movzbl	(%r9,%r8), %r8d
	movslq	%r14d, %r12
	movzbl	(%r9,%r12), %r12d
	movb	%r12b, (%rax)
	cmpq	%r11, %rbx
	jbe	.L185
	leaq	2(%rax), %r11
	movb	%r8b, 1(%rax)
	cmpq	%r11, %rbx
	ja	.L261
	testq	%rcx, %rcx
	je	.L212
	movl	%r10d, (%rcx)
	leal	1(%r10), %r9d
	leaq	8(%rcx), %rdi
	movl	%r10d, 4(%rcx)
.L187:
	movq	-56(%rbp), %rax
	movb	%sil, 104(%r15)
	movb	$1, 91(%r15)
	movl	$15, (%rax)
	cmpq	%rdx, -48(%rbp)
	jbe	.L262
.L218:
	movb	$0, -57(%rbp)
	xorl	%r8d, %r8d
.L166:
	movq	-56(%rbp), %rax
	movq	%rdi, %rcx
	xorl	%esi, %esi
	movl	$15, (%rax)
	xorl	%eax, %eax
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L174:
	movl	%esi, %r8d
	movzbl	%r14b, %r14d
	leaq	1(%rax), %r11
	sarl	$14, %r8d
	orl	%r8d, %r14d
	movslq	%r14d, %r8
	movzbl	(%r9,%r8), %r8d
	movb	%r8b, (%rax)
	movl	%esi, %r8d
	sarl	$8, %r8d
	andl	$63, %r8d
	movzbl	(%r9,%r8), %r12d
	movl	%esi, %r8d
	sarl	$2, %r8d
	andl	$63, %r8d
	movzbl	(%r9,%r8), %r8d
	cmpq	%r11, %rbx
	jbe	.L180
	leaq	2(%rax), %r11
	movb	%r12b, 1(%rax)
	cmpq	%r11, %rbx
	ja	.L263
	testq	%rcx, %rcx
	je	.L183
	movl	%r10d, (%rcx)
	addq	$8, %rcx
	movl	%r10d, -4(%rcx)
	addl	$1, %r10d
.L183:
	movq	-56(%rbp), %rax
	movb	%r8b, 104(%r15)
	movb	$1, 91(%r15)
	movl	$15, (%rax)
	movq	%r11, %rax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L258:
	movl	%esi, %r8d
	leaq	1(%rax), %r11
	sarl	$10, %r8d
	movslq	%r8d, %r8
	movzbl	(%r9,%r8), %r8d
	movb	%r8b, (%rax)
	movl	%esi, %r8d
	sarl	$4, %r8d
	andl	$63, %r8d
	movzbl	(%r9,%r8), %r8d
	cmpq	%r11, %rbx
	jbe	.L177
	movb	%r8b, 1(%rax)
	addq	$2, %rax
	testq	%rcx, %rcx
	je	.L178
	movl	%r10d, (%rcx)
	addq	$8, %rcx
	movl	%r10d, -4(%rcx)
	addl	$1, %r10d
.L178:
	leal	0(,%rsi,4), %r14d
	movl	$1, %r8d
	andl	$60, %r14d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L177:
	testq	%rcx, %rcx
	je	.L179
	movl	%r10d, (%rcx)
	addl	$1, %r10d
	addq	$4, %rcx
.L179:
	movq	-56(%rbp), %rax
	movb	%r8b, 104(%r15)
	movb	$1, 91(%r15)
	movl	$15, (%rax)
	movq	%r11, %rax
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L185:
	testq	%rcx, %rcx
	je	.L213
	movl	%r10d, (%rcx)
	leal	1(%r10), %r9d
	leaq	4(%rcx), %rdi
.L189:
	movq	-56(%rbp), %rax
	movb	%r8b, 104(%r15)
	movb	%sil, 105(%r15)
	movb	$2, 91(%r15)
	movl	$15, (%rax)
	cmpq	%rdx, -48(%rbp)
	ja	.L218
.L262:
	movzbl	2(%r13), %r8d
	testb	%r8b, %r8b
	jne	.L191
	movb	$0, -57(%rbp)
	movq	%rdi, %rcx
	xorl	%esi, %esi
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L180:
	testq	%rcx, %rcx
	je	.L184
	movl	%r10d, (%rcx)
	addl	$1, %r10d
	addq	$4, %rcx
.L184:
	movq	-56(%rbp), %rax
	movb	%r12b, 104(%r15)
	movb	%r8b, 105(%r15)
	movb	$2, 91(%r15)
	movl	$15, (%rax)
	movq	%r11, %rax
.L182:
	sall	$4, %esi
	movl	$2, %r8d
	movl	%esi, %r14d
	andl	$48, %r14d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L201:
	xorl	%r9d, %r9d
.L173:
	movq	-48(%rbp), %rsi
	movq	%rbx, %r10
	subq	%rax, %r10
	subq	%rdx, %rsi
	sarq	%rsi
	cmpl	%r10d, %esi
	cmovge	%r10d, %esi
	testl	%esi, %esi
	jle	.L202
	subl	$1, %esi
	leaq	1(%rax,%rsi), %r11
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L264:
	movzwl	%si, %r10d
	cmpb	$0, (%rdi,%r10)
	je	.L159
	movb	%sil, -1(%rax)
	testq	%rcx, %rcx
	je	.L160
	movl	%r9d, (%rcx)
	addl	$1, %r9d
	addq	$4, %rcx
.L160:
	cmpq	%r11, %rax
	je	.L157
.L167:
	movq	%rdx, %r14
	movzwl	(%rdx), %esi
	addq	$2, %rdx
	movq	%rax, %r12
	addq	$1, %rax
	cmpw	$127, %si
	jbe	.L264
.L158:
	movb	$43, (%r12)
	testq	%rcx, %rcx
	je	.L203
	movl	%r9d, (%rcx)
	movl	%r9d, %r10d
	addq	$4, %rcx
	movq	%r14, %rdx
	xorl	%r8d, %r8d
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L159:
	cmpw	$43, %si
	jne	.L158
	movb	$43, (%r12)
	cmpq	%rax, %rbx
	jbe	.L161
	movb	$45, 1(%r12)
	leaq	2(%r12), %rax
	testq	%rcx, %rcx
	je	.L173
	movl	%r9d, (%rcx)
	addq	$8, %rcx
	movl	%r9d, -4(%rcx)
	addl	$1, %r9d
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L263:
	movb	%r8b, 2(%rax)
	addq	$3, %rax
	testq	%rcx, %rcx
	je	.L182
	movl	%r10d, (%rcx)
	addq	$12, %rcx
	movl	%r10d, -8(%rcx)
	movl	%r10d, -4(%rcx)
	addl	$1, %r10d
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L261:
	movb	%sil, 2(%rax)
	leaq	3(%rax), %r8
	testq	%rcx, %rcx
	je	.L211
	movl	%r10d, (%rcx)
	movq	%r8, %rax
	addq	$12, %rcx
	xorl	%r8d, %r8d
	movl	%r10d, -8(%rcx)
	xorl	%r14d, %r14d
	movl	%r10d, -4(%rcx)
	addl	$1, %r10d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L213:
	movl	%r10d, %r9d
	xorl	%edi, %edi
	jmp	.L189
.L161:
	testq	%rcx, %rcx
	je	.L163
	movl	%r9d, (%rcx)
	addl	$1, %r9d
	addq	$4, %rcx
.L163:
	movq	-56(%rbp), %rdi
	movb	$45, 104(%r15)
	movq	%rax, %r11
	movb	$1, 91(%r15)
	movl	$15, (%rdi)
	.p2align 4,,10
	.p2align 3
.L157:
	cmpq	-48(%rbp), %rdx
	jnb	.L204
	cmpq	%rbx, %r11
	jb	.L204
	movq	-56(%rbp), %rax
	movl	$16777216, %esi
	movl	$15, (%rax)
.L169:
	movl	80(%r15), %eax
	movsbl	%r8b, %r8d
	sall	$16, %r8d
	andl	$-268435456, %eax
	orl	%esi, %eax
	movzbl	-57(%rbp), %esi
	orl	%esi, %eax
	orl	%eax, %r8d
	movl	%r8d, 80(%r15)
.L196:
	movq	%rdx, 16(%r13)
	movq	%r11, 32(%r13)
	movq	%rcx, 48(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movl	$16777216, %esi
	movl	$1, %eax
.L168:
	cmpb	$0, 2(%r13)
	je	.L169
	cmpq	-48(%rbp), %rdx
	jb	.L169
	testb	%al, %al
	je	.L199
.L190:
	movl	80(%r15), %eax
	andl	$-268435456, %eax
	orl	$16777216, %eax
	movl	%eax, 80(%r15)
	jmp	.L196
.L214:
	movq	%rcx, %rdi
.L191:
	cmpq	%r11, %rbx
	jbe	.L215
	movb	$45, (%r11)
	leaq	1(%r11), %rax
	testq	%rdi, %rdi
	je	.L216
	subl	$1, %r9d
	movq	%rax, %r11
.L198:
	movl	%r9d, (%rdi)
	leaq	4(%rdi), %rcx
	jmp	.L190
.L211:
	movq	%r8, %rax
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
	jmp	.L176
.L212:
	movl	%r10d, %r9d
	xorl	%edi, %edi
	jmp	.L187
.L259:
	movb	%r14b, -57(%rbp)
	testb	%r8b, %r8b
	je	.L171
	movzbl	%r14b, %edx
	leaq	_ZL8toBase64(%rip), %r9
	addq	$1, %rax
	movzbl	(%r9,%rdx), %edx
	movb	%dl, -1(%rax)
	testq	%rcx, %rcx
	je	.L171
	leal	-1(%r10), %edx
	addq	$4, %rcx
	movl	%edx, -4(%rcx)
.L171:
	leaq	_ZL10fromBase64(%rip), %rdx
	cmpb	$-1, (%rdx,%rsi)
	je	.L207
	cmpq	%rbx, %rax
	jb	.L265
	movq	-56(%rbp), %rdi
	movb	$45, 104(%r15)
	movq	%rax, %r11
	movl	%r10d, %r9d
	movb	$1, 91(%r15)
	movq	%r12, %rdx
	movl	$16777216, %esi
	movl	$1, %eax
	movl	$15, (%rdi)
	jmp	.L168
.L217:
	movl	%r10d, %r9d
	movq	%rax, %r11
.L199:
	testb	%r8b, %r8b
	je	.L214
	movzbl	-57(%rbp), %eax
	leaq	_ZL8toBase64(%rip), %rsi
	movzbl	(%rsi,%rax), %esi
	cmpq	%r11, %rbx
	jbe	.L192
	movb	%sil, (%r11)
	leaq	1(%r11), %rax
	testq	%rcx, %rcx
	je	.L193
	subl	$1, %r9d
	leaq	4(%rcx), %rdi
	movl	%r9d, (%rcx)
	cmpq	%rax, %rbx
	jbe	.L266
	movb	$45, 1(%r11)
	addq	$2, %r11
	jmp	.L198
.L192:
	movsbq	91(%r15), %rax
	leal	1(%rax), %edi
	movb	%dil, 91(%r15)
	movb	%sil, 104(%r15,%rax)
	movq	-56(%rbp), %rax
	movl	$15, (%rax)
.L195:
	movsbq	91(%r15), %rax
	leal	1(%rax), %esi
	movb	%sil, 91(%r15)
	movb	$45, 104(%r15,%rax)
	movq	-56(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L190
.L215:
	movq	%rdi, %rcx
	jmp	.L195
.L202:
	movq	%rax, %r11
	jmp	.L157
.L265:
	movb	$45, (%rax)
	leaq	1(%rax), %rdx
	testq	%rcx, %rcx
	je	.L208
	leal	-1(%r10), %eax
	movl	%r10d, %r9d
	addq	$4, %rcx
	movl	%eax, -4(%rcx)
	movq	%rdx, %rax
	movq	%r12, %rdx
	jmp	.L173
.L207:
	movl	%r10d, %r9d
	movq	%r12, %rdx
	jmp	.L173
.L216:
	xorl	%ecx, %ecx
	movq	%rax, %r11
	jmp	.L190
.L203:
	movl	%r9d, %r10d
	movq	%r14, %rdx
	xorl	%r8d, %r8d
	jmp	.L156
.L208:
	movq	%rdx, %rax
	movl	%r10d, %r9d
	movq	%r12, %rdx
	jmp	.L173
.L193:
	cmpq	%rax, %rbx
	jbe	.L267
	movb	$45, 1(%r11)
	addq	$2, %r11
	jmp	.L190
.L266:
	movq	%rdi, %rcx
	movq	%rax, %r11
	jmp	.L195
.L267:
	movq	%rax, %r11
	jmp	.L195
.L255:
	movb	%r14b, -57(%rbp)
.L164:
	cmpb	$0, 2(%r13)
	jne	.L217
	movq	%rax, %r11
	xorl	%esi, %esi
	jmp	.L169
.L205:
	movb	%r14b, -57(%rbp)
	jmp	.L257
	.cfi_endproc
.LFE2116:
	.size	_UTF7FromUnicodeWithOffsets, .-_UTF7FromUnicodeWithOffsets
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"UTF-7,version=1"
.LC2:
	.string	"UTF-7"
	.text
	.p2align 4
	.type	_UTF7GetName, @function
_UTF7GetName:
.LFB2117:
	.cfi_startproc
	endbr64
	movl	80(%rdi), %eax
	leaq	.LC2(%rip), %rdx
	shrl	$28, %eax
	cmpl	$1, %eax
	leaq	.LC1(%rip), %rax
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE2117:
	.size	_UTF7GetName, .-_UTF7GetName
	.section	.text.unlikely
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4
	.type	_IMAPToUnicodeWithOffsets, @function
_IMAPToUnicodeWithOffsets:
.LFB2118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	8(%rdi), %r15
	movq	40(%rdi), %rbx
	movq	24(%rdi), %rsi
	movq	16(%rdi), %rax
	movl	72(%r15), %edx
	movzbl	64(%r15), %r13d
	movq	%rbx, -56(%rbp)
	movq	%rsi, -48(%rbp)
	movq	48(%rdi), %r9
	movl	%edx, %ebx
	movl	%edx, %r10d
	movq	32(%rdi), %rsi
	shrl	$16, %ebx
	testb	%r13b, %r13b
	setne	%r12b
	negl	%r12d
	andl	$16777216, %edx
	je	.L307
.L273:
	movq	-56(%rbp), %rcx
	movq	-48(%rbp), %rdx
	subq	%rsi, %rcx
	subq	%rax, %rdx
	sarq	%rcx
	cmpl	%ecx, %edx
	cmovge	%ecx, %edx
	testl	%edx, %edx
	jle	.L308
	subl	$1, %edx
	leaq	1(%rax,%rdx), %rcx
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L275:
	cmpb	$38, %dl
	je	.L276
	addq	$2, %rsi
	movw	%dx, -2(%rsi)
	testq	%r9, %r9
	je	.L277
	movl	%r12d, (%r9)
	addl	$1, %r12d
	addq	$4, %r9
.L277:
	cmpq	%rcx, %rax
	je	.L308
.L278:
	movzbl	(%rax), %edx
	addq	$1, %rax
	leal	-32(%rdx), %r8d
	cmpb	$94, %r8b
	jbe	.L275
	movb	%dl, 65(%r15)
	movq	-72(%rbp), %rdx
	movl	$1, %r13d
	movl	$12, (%rdx)
.L274:
	movzbl	%bl, %ebx
	sall	$16, %ebx
	movl	%ebx, %edx
	cmpq	-48(%rbp), %rax
	jnb	.L309
	cmpq	-56(%rbp), %rsi
	jb	.L309
	movq	-72(%rbp), %rbx
	movzwl	%r10w, %r10d
	movl	$15, (%rbx)
	movl	$16777216, %ebx
.L279:
	orl	%edx, %ebx
	movb	%r13b, 64(%r15)
	orl	%ebx, %r10d
	movl	%r10d, 72(%r15)
	movq	%rax, 16(%rdi)
	movq	%rsi, 32(%rdi)
	movq	%r9, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	movzbl	%r13b, %ecx
	xorl	%r11d, %r11d
.L272:
	cmpq	%rax, -48(%rbp)
	jbe	.L281
	cmpq	%rsi, -56(%rbp)
	jbe	.L283
	movq	%rdi, -80(%rbp)
	leaq	65(%r15), %rdx
	leaq	.L292(%rip), %r14
	movq	%r15, -64(%rbp)
	movq	%rdx, %r15
	.p2align 4,,10
	.p2align 3
.L286:
	movzbl	(%rax), %edx
	addq	$1, %rax
	movl	%r11d, %edi
	leal	1(%rcx), %r13d
	addl	$1, %r11d
	movb	%dl, (%r15,%rcx)
	cmpb	$126, %dl
	ja	.L349
	cmpb	$44, %dl
	je	.L310
	cmpb	$47, %dl
	je	.L346
	movzbl	%dl, %ecx
	leaq	_ZL10fromBase64(%rip), %r8
	movsbw	(%r8,%rcx), %r8w
	testb	%r8b, %r8b
	js	.L350
.L288:
	leal	1(%rbx), %ecx
	cmpb	$8, %cl
	ja	.L311
	movzbl	%cl, %ecx
	movslq	(%r14,%rcx,4), %rcx
	addq	%r14, %rcx
	notrack jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L292:
	.long	.L296-.L292
	.long	.L296-.L292
	.long	.L293-.L292
	.long	.L295-.L292
	.long	.L293-.L292
	.long	.L293-.L292
	.long	.L294-.L292
	.long	.L293-.L292
	.long	.L291-.L292
	.text
	.p2align 4,,10
	.p2align 3
.L293:
	sall	$6, %r10d
	addl	$1, %ebx
	movzbl	%r13b, %ecx
	orl	%r8d, %r10d
.L284:
	cmpq	%rax, -48(%rbp)
	je	.L351
.L285:
	cmpq	-56(%rbp), %rsi
	jb	.L286
	movq	-64(%rbp), %r15
	movq	-80(%rbp), %rdi
	movl	%ecx, %r13d
.L283:
	movq	-72(%rbp), %rdx
	movzbl	%bl, %ebx
	movzwl	%r10w, %r10d
	sall	$16, %ebx
	movl	$15, (%rdx)
	movl	%ebx, %edx
	xorl	%ebx, %ebx
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L296:
	movsbw	%r8b, %r10w
	movzbl	%r13b, %ecx
	movl	$1, %ebx
	cmpq	%rax, -48(%rbp)
	jne	.L285
	.p2align 4,,10
	.p2align 3
.L351:
	movq	-64(%rbp), %r15
	movq	-80(%rbp), %rdi
	movl	%ecx, %r13d
	movq	-48(%rbp), %rax
.L281:
	movq	-72(%rbp), %r14
	movzbl	%bl, %edx
	sall	$16, %edx
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jg	.L317
	testb	%cl, %cl
	jne	.L317
	xorl	%r13d, %r13d
	cmpb	$0, 2(%rdi)
	je	.L317
	cmpq	-48(%rbp), %rax
	jnb	.L352
.L317:
	xorl	%ebx, %ebx
	movzwl	%r10w, %r10d
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L291:
	movzwl	%r10w, %r10d
	movl	%r10d, %edx
	sall	$6, %edx
	orl	%edx, %r8d
	leal	-32(%r8), %edx
	cmpw	$94, %dx
	jbe	.L353
	movw	%r8w, (%rsi)
	leaq	2(%rsi), %rdx
	testq	%r9, %r9
	je	.L312
	movl	%r12d, (%r9)
	movq	%rdx, %rsi
	movl	%r11d, %r12d
	addq	$4, %r9
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L294:
	movzwl	%r10w, %r10d
	movsbl	%r8b, %ecx
	leal	0(,%r10,4), %ebx
	sarl	$4, %ecx
	orl	%ebx, %ecx
	leal	-32(%rcx), %ebx
	cmpw	$94, %bx
	jbe	.L354
	movw	%cx, (%rsi)
	leaq	2(%rsi), %rbx
	testq	%r9, %r9
	je	.L300
	movl	%r12d, (%r9)
	movl	%edi, %r12d
	addq	$4, %r9
.L300:
	movq	-64(%rbp), %rsi
	movl	%r8d, %r10d
	movl	$1, %ecx
	andl	$15, %r10d
	movb	%dl, 65(%rsi)
	movq	%rbx, %rsi
	movl	$6, %ebx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L295:
	movzwl	%r10w, %r10d
	movsbl	%r8b, %ecx
	movl	%r10d, %ebx
	sarl	$2, %ecx
	sall	$4, %ebx
	orl	%ebx, %ecx
	leal	-32(%rcx), %ebx
	cmpw	$94, %bx
	jbe	.L355
	movw	%cx, (%rsi)
	leaq	2(%rsi), %rbx
	testq	%r9, %r9
	je	.L298
	movl	%r12d, (%r9)
	movl	%edi, %r12d
	addq	$4, %r9
.L298:
	movq	-64(%rbp), %rsi
	movl	%r8d, %r10d
	movl	$1, %ecx
	andl	$3, %r10d
	movb	%dl, 65(%rsi)
	movq	%rbx, %rsi
	movl	$3, %ebx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$63, %r8d
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%rdx, %rsi
	xorl	%ebx, %ebx
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L308:
	xorl	%r13d, %r13d
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L309:
	movl	$16777216, %ebx
	movzwl	%r10w, %r10d
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L276:
	addl	$1, %r12d
	xorl	%r13d, %r13d
	movl	$-1, %ebx
	xorl	%r10d, %r10d
	movl	%r12d, %r11d
	xorl	%ecx, %ecx
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L349:
	movq	-64(%rbp), %r15
	movq	-80(%rbp), %rdi
.L305:
	movq	-72(%rbp), %rdx
	movzbl	%bl, %ebx
	movzwl	%r10w, %r10d
	sall	$16, %ebx
	movl	$12, (%rdx)
	movl	%ebx, %edx
	movl	$16777216, %ebx
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L346:
	movq	-64(%rbp), %r15
	movq	-80(%rbp), %rdi
.L289:
	cmpb	$-1, %bl
	jne	.L305
	movb	$38, 65(%r15)
	movl	$2, %r13d
	movb	%dl, 66(%r15)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L350:
	movq	-64(%rbp), %r15
	movq	-80(%rbp), %rdi
	cmpb	$-2, %r8b
	jne	.L289
	cmpb	$-1, %bl
	je	.L356
	testw	%r10w, %r10w
	jne	.L305
	cmpb	$6, %bl
	ja	.L305
	movl	$73, %edx
	movl	%r11d, %r12d
	btq	%rbx, %rdx
	jc	.L273
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L352:
	cmpb	$-1, %bl
	je	.L357
.L306:
	movq	-72(%rbp), %rbx
	movzwl	%r10w, %r10d
	movl	$11, (%rbx)
	movl	$16777216, %ebx
	jmp	.L279
.L357:
	movb	$38, 65(%r15)
	movl	$1, %r13d
	jmp	.L306
.L356:
	movl	$38, %r13d
	leaq	2(%rsi), %rdx
	movw	%r13w, (%rsi)
	testq	%r9, %r9
	je	.L313
	subl	$1, %r12d
	addq	$4, %r9
	movq	%rdx, %rsi
	movl	%r12d, -4(%r9)
	movl	%r11d, %r12d
	jmp	.L273
.L354:
	movq	-72(%rbp), %rbx
	movq	-64(%rbp), %r15
	movl	$327680, %edx
	movq	-80(%rbp), %rdi
	movl	$12, (%rbx)
	movl	$16777216, %ebx
	jmp	.L279
.L353:
	movq	-72(%rbp), %rbx
	movq	-64(%rbp), %r15
	movl	$458752, %edx
	movq	-80(%rbp), %rdi
	movl	$12, (%rbx)
	movl	$16777216, %ebx
	jmp	.L279
.L355:
	movq	-72(%rbp), %rbx
	movq	-64(%rbp), %r15
	movl	$131072, %edx
	movq	-80(%rbp), %rdi
	movl	$12, (%rbx)
	movl	$16777216, %ebx
	jmp	.L279
.L313:
	movl	%r11d, %r12d
	movq	%rdx, %rsi
	jmp	.L273
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_IMAPToUnicodeWithOffsets.cold, @function
_IMAPToUnicodeWithOffsets.cold:
.LFSB2118:
.L311:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movzbl	%r13b, %ecx
	jmp	.L284
	.cfi_endproc
.LFE2118:
	.text
	.size	_IMAPToUnicodeWithOffsets, .-_IMAPToUnicodeWithOffsets
	.section	.text.unlikely
	.size	_IMAPToUnicodeWithOffsets.cold, .-_IMAPToUnicodeWithOffsets.cold
.LCOLDE3:
	.text
.LHOTE3:
	.p2align 4
	.type	_IMAPFromUnicodeWithOffsets, @function
_IMAPFromUnicodeWithOffsets:
.LFB2119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	movq	8(%rdi), %r9
	movq	24(%rdi), %rax
	movq	16(%rdi), %r12
	movq	40(%rdi), %r14
	movl	80(%r9), %ecx
	movq	48(%rdi), %rdx
	movq	%rax, -48(%rbp)
	movq	32(%rdi), %rax
	movl	%ecx, %r13d
	movl	%ecx, %r15d
	shrl	$16, %r13d
	andl	$16777216, %ecx
	jne	.L437
	xorl	%r11d, %r11d
.L360:
	movq	-48(%rbp), %r8
	cmpq	%rax, %r14
	ja	.L371
	cmpq	%r12, -48(%rbp)
	jbe	.L372
	movq	%rax, %r8
	movq	%rdx, -64(%rbp)
	movq	-56(%rbp), %rax
	movl	%r11d, %esi
	movq	%r12, %r10
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L539:
	testb	%r13b, %r13b
	je	.L537
	movq	%r10, %r12
.L371:
	cmpq	%r8, %r12
	jnb	.L372
	movzwl	(%r12), %ecx
	leaq	2(%r12), %r10
	leal	-32(%rcx), %esi
	cmpw	$94, %si
	jbe	.L538
	cmpb	$1, %r13b
	je	.L449
	cmpb	$2, %r13b
	jne	.L539
.L375:
	movzwl	%cx, %esi
	movl	$44, %ebx
	movl	%esi, %r8d
	sarl	$12, %r8d
	orl	%r8d, %r15d
	cmpb	$62, %r15b
	ja	.L408
	movzbl	%r15b, %r15d
	leaq	_ZL8toBase64(%rip), %r8
	movzbl	(%r8,%r15), %ebx
.L408:
	sarl	$6, %esi
	movb	%bl, (%rax)
	leaq	1(%rax), %r8
	movl	%esi, %ebx
	andl	$63, %ebx
	cmpq	%r8, %r14
	jbe	.L409
	movl	$44, %esi
	cmpb	$63, %bl
	je	.L410
	andl	$63, %ebx
	leaq	_ZL8toBase64(%rip), %rsi
	movzbl	(%rsi,%rbx), %esi
.L410:
	leaq	2(%rax), %r8
	movb	%sil, 1(%rax)
	andl	$63, %ecx
	cmpq	%r8, %r14
	jbe	.L411
	movl	$44, %esi
	cmpb	$63, %cl
	je	.L412
	andl	$63, %ecx
	leaq	_ZL8toBase64(%rip), %rsi
	movzbl	(%rsi,%rcx), %esi
.L412:
	movb	%sil, 2(%rax)
	leaq	3(%rax), %r12
	testq	%rdx, %rdx
	je	.L413
	movl	%r11d, (%rdx)
	leal	1(%r11), %r15d
	leaq	12(%rdx), %r13
	movl	%r11d, 4(%rdx)
	movl	%r11d, 8(%rdx)
	cmpq	%r10, -48(%rbp)
	jbe	.L540
	cmpq	%r12, %r14
	jbe	.L443
	movzwl	(%r10), %ecx
	leaq	2(%r10), %rbx
	leal	-32(%rcx), %edx
	cmpw	$94, %dx
	jbe	.L541
.L377:
	movl	%ecx, %eax
	movl	$44, %edx
	sarl	$10, %eax
	cmpl	$63, %eax
	je	.L388
	cltq
	leaq	_ZL8toBase64(%rip), %rdx
	movzbl	(%rdx,%rax), %edx
.L388:
	movl	%ecx, %eax
	leaq	1(%r12), %r8
	movb	%dl, (%r12)
	sarl	$4, %eax
	andl	$63, %eax
	cmpq	%r8, %r14
	jbe	.L389
	movl	$44, %edx
	cmpb	$63, %al
	je	.L390
	andl	$63, %eax
	leaq	_ZL8toBase64(%rip), %rdx
	movzbl	(%rdx,%rax), %edx
.L390:
	movb	%dl, 1(%r12)
	leaq	2(%r12), %r10
	testq	%r13, %r13
	je	.L453
	movl	%r15d, 0(%r13)
	leal	1(%r15), %r11d
	leaq	8(%r13), %rdx
	movl	%r15d, 4(%r13)
.L391:
	sall	$2, %ecx
	movl	%ecx, %r15d
	andl	$60, %r15d
	cmpq	%rbx, -48(%rbp)
	jbe	.L542
	cmpq	%r10, %r14
	jbe	.L446
	movzwl	(%rbx), %ecx
	leaq	2(%rbx), %r12
	leal	-32(%rcx), %eax
	cmpw	$94, %ax
	ja	.L380
	movq	%r10, %rax
	movq	%rbx, %r12
	movl	$1, %r13d
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L409:
	testq	%rdx, %rdx
	je	.L474
	leaq	4(%rdx), %rax
	movl	%r11d, (%rdx)
	leal	1(%r11), %esi
	movq	%rax, -64(%rbp)
.L417:
	movl	$44, %eax
	cmpb	$63, %bl
	je	.L418
	andl	$63, %ebx
	leaq	_ZL8toBase64(%rip), %rax
	movzbl	(%rax,%rbx), %eax
.L418:
	andl	$63, %ecx
	movb	%al, 104(%r9)
	movl	$44, %eax
	cmpb	$63, %cl
	je	.L419
	andl	$63, %ecx
	leaq	_ZL8toBase64(%rip), %rax
	movzbl	(%rax,%rcx), %eax
.L419:
	movb	%al, 105(%r9)
	movq	-56(%rbp), %rax
	movb	$2, 91(%r9)
	movl	$15, (%rax)
	cmpq	%r10, -48(%rbp)
	jbe	.L477
.L535:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
.L373:
	movq	-64(%rbp), %rdx
	movl	$15, (%rax)
	movq	%r10, %r12
	xorl	%eax, %eax
	xorl	%r10d, %r10d
.L369:
	cmpb	$0, 2(%rdi)
	je	.L420
	cmpq	-48(%rbp), %r12
	jb	.L420
	testb	%al, %al
	je	.L543
.L422:
	movl	80(%r9), %eax
	andl	$-268435456, %eax
	orl	$16777216, %eax
	movl	%eax, 80(%r9)
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L437:
	xorl	%esi, %esi
.L387:
	movq	-48(%rbp), %rcx
	movq	%r14, %r8
	subq	%rax, %r8
	subq	%r12, %rcx
	sarq	%rcx
	cmpl	%r8d, %ecx
	cmovge	%r8d, %ecx
	testl	%ecx, %ecx
	jle	.L438
	subl	$1, %ecx
	leaq	1(%rax,%rcx), %r8
	.p2align 4,,10
	.p2align 3
.L368:
	movq	%r12, %rbx
	movzwl	(%r12), %ecx
	addq	$2, %r12
	movq	%rax, %r11
	addq	$1, %rax
	leal	-32(%rcx), %r10d
	cmpw	$94, %r10w
	ja	.L362
	cmpw	$38, %cx
	je	.L525
	movb	%cl, -1(%rax)
	testq	%rdx, %rdx
	je	.L363
	movl	%esi, (%rdx)
	addl	$1, %esi
	addq	$4, %rdx
.L363:
	cmpq	%r8, %rax
	jne	.L368
.L361:
	cmpq	-48(%rbp), %r12
	jnb	.L440
	cmpq	%r14, %r8
	jb	.L440
	movq	-56(%rbp), %rax
	movsbl	%r13b, %r13d
	movl	$16777216, %r10d
	movl	%r13d, %ecx
	movl	$15, (%rax)
	sall	$16, %ecx
.L370:
	movl	80(%r9), %eax
	orl	%r10d, %ecx
	movzbl	%r15b, %r15d
	andl	$-268435456, %eax
	orl	%eax, %ecx
	orl	%ecx, %r15d
	movl	%r15d, 80(%r9)
.L430:
	movq	%r12, 16(%rdi)
	movq	%r8, 32(%rdi)
	movq	%rdx, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L471
	leaq	8(%rdx), %rax
	movl	%r11d, (%rdx)
	leal	1(%r11), %esi
	movq	%rax, -64(%rbp)
	movl	%r11d, 4(%rdx)
.L415:
	movl	$44, %eax
	cmpb	$63, %cl
	je	.L416
	andl	$63, %ecx
	leaq	_ZL8toBase64(%rip), %rax
	movzbl	(%rax,%rcx), %eax
.L416:
	movb	%al, 104(%r9)
	movq	-56(%rbp), %rax
	movb	$1, 91(%r9)
	movl	$15, (%rax)
	cmpq	%r10, -48(%rbp)
	ja	.L535
.L477:
	movq	%r10, %r12
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L420:
	movsbl	%r13b, %r13d
	movl	%r13d, %ecx
	sall	$16, %ecx
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L362:
	cmpw	$38, %cx
	jne	.L364
.L525:
	movb	$38, (%r11)
	cmpq	%rax, %r14
	jbe	.L365
	movb	$45, 1(%r11)
	leaq	2(%r11), %rax
	testq	%rdx, %rdx
	je	.L387
	movl	%esi, (%rdx)
	addq	$8, %rdx
	movl	%esi, -4(%rdx)
	addl	$1, %esi
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L440:
	movl	$16777216, %r10d
	movl	$1, %eax
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%r10, %r12
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L380:
	movzwl	%cx, %ebx
	movl	$44, %eax
	movl	%ebx, %ecx
	sarl	$14, %ecx
	orl	%ecx, %r15d
	cmpb	$62, %r15b
	ja	.L395
	movzbl	%r15b, %r15d
	leaq	_ZL8toBase64(%rip), %rax
	movzbl	(%rax,%r15), %eax
.L395:
	movb	%al, (%r10)
	movl	%ebx, %eax
	leaq	1(%r10), %r8
	sarl	$8, %eax
	andl	$63, %eax
	cmpq	%r8, %r14
	jbe	.L396
	movl	$44, %ecx
	cmpb	$63, %al
	je	.L397
	andl	$63, %eax
	leaq	_ZL8toBase64(%rip), %rcx
	movzbl	(%rcx,%rax), %ecx
.L397:
	movl	%ebx, %eax
	leaq	2(%r10), %r8
	movb	%cl, 1(%r10)
	shrb	$2, %al
	cmpq	%r8, %r14
	jbe	.L398
	movl	$44, %ecx
	cmpb	$63, %al
	je	.L399
	andl	$63, %eax
	leaq	_ZL8toBase64(%rip), %rcx
	movzbl	(%rcx,%rax), %ecx
.L399:
	movb	%cl, 2(%r10)
	leaq	3(%r10), %rax
	testq	%rdx, %rdx
	je	.L400
	movl	%r11d, (%rdx)
	addq	$12, %rdx
	movl	%r11d, -8(%rdx)
	movl	%r11d, -4(%rdx)
	addl	$1, %r11d
.L400:
	sall	$4, %ebx
	movl	%ebx, %r15d
	andl	$48, %r15d
	cmpq	%r12, -48(%rbp)
	jbe	.L544
	cmpq	%rax, %r14
	jbe	.L441
	movzwl	(%r12), %ecx
	leaq	2(%r12), %r10
	leal	-32(%rcx), %esi
	cmpw	$94, %si
	ja	.L375
	movl	$2, %r13d
.L436:
	leaq	_ZL8toBase64(%rip), %rsi
	movzbl	%r15b, %ecx
	movzbl	(%rsi,%rcx), %ecx
	movl	%r11d, %esi
.L383:
	movb	%cl, (%rax)
	leaq	1(%rax), %r8
	testq	%rdx, %rdx
	je	.L384
	leaq	4(%rdx), %rcx
	leal	-1(%r11), %r10d
	movl	%r10d, (%rdx)
	movq	%rcx, %rdx
	cmpq	%r8, %r14
	ja	.L545
.L386:
	movq	-56(%rbp), %rax
	movb	$45, 104(%r9)
	movl	$16777216, %r10d
	movb	$1, 91(%r9)
	movl	$15, (%rax)
	movl	$1, %eax
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L396:
	testq	%rdx, %rdx
	je	.L463
	leaq	4(%rdx), %rcx
	movl	%r11d, (%rdx)
	leal	1(%r11), %esi
	movq	%rcx, -64(%rbp)
.L404:
	movl	$44, %edx
	cmpb	$63, %al
	je	.L405
	andl	$63, %eax
	leaq	_ZL8toBase64(%rip), %rdx
	movzbl	(%rdx,%rax), %edx
.L405:
	movl	%ebx, %eax
	movb	%dl, 104(%r9)
	movl	$44, %edx
	shrb	$2, %al
	cmpb	$63, %al
	je	.L406
	andl	$63, %eax
	leaq	_ZL8toBase64(%rip), %rdx
	movzbl	(%rdx,%rax), %edx
.L406:
	movq	-56(%rbp), %rax
	movb	%dl, 105(%r9)
	movb	$2, 91(%r9)
	movl	$15, (%rax)
.L403:
	sall	$4, %ebx
	movq	-56(%rbp), %rax
	movl	$131072, %ecx
	movq	%r12, %r10
	movl	%ebx, %r15d
	movl	$2, %r13d
	andl	$48, %r15d
	cmpq	%r12, -48(%rbp)
	ja	.L373
.L378:
	cmpb	$0, 2(%rdi)
	je	.L546
.L433:
	testb	%r13b, %r13b
	je	.L423
	movl	$44, %edx
	cmpq	%r14, %r8
	jnb	.L424
	cmpb	$62, %r15b
	ja	.L425
	movzbl	%r15b, %r15d
	leaq	_ZL8toBase64(%rip), %rax
	movzbl	(%rax,%r15), %edx
.L425:
	movq	-64(%rbp), %rbx
	movb	%dl, (%r8)
	leaq	1(%r8), %rax
	testq	%rbx, %rbx
	je	.L426
	subl	$1, %esi
	leaq	4(%rbx), %rdx
	movl	%esi, (%rbx)
	cmpq	%rax, %r14
	jbe	.L547
	movb	$45, 1(%r8)
	addq	$2, %r8
	movq	%rdx, -64(%rbp)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L398:
	testq	%rdx, %rdx
	je	.L461
	leaq	8(%rdx), %rcx
	movl	%r11d, (%rdx)
	leal	1(%r11), %esi
	movq	%rcx, -64(%rbp)
	movl	%r11d, 4(%rdx)
.L401:
	movl	$44, %edx
	cmpb	$63, %al
	je	.L402
	andl	$63, %eax
	leaq	_ZL8toBase64(%rip), %rdx
	movzbl	(%rdx,%rax), %edx
.L402:
	movq	-56(%rbp), %rax
	movb	%dl, 104(%r9)
	movb	$1, 91(%r9)
	movl	$15, (%rax)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L474:
	movq	$0, -64(%rbp)
	movl	%r11d, %esi
	jmp	.L417
.L413:
	cmpq	%r10, -48(%rbp)
	jbe	.L444
	cmpq	%r12, %r14
	jbe	.L445
	movzwl	(%r10), %ecx
	leaq	2(%r10), %rbx
	leal	-32(%rcx), %esi
	cmpw	$94, %si
	jbe	.L379
	movl	%r11d, %r15d
	xorl	%r13d, %r13d
	jmp	.L377
.L461:
	movq	$0, -64(%rbp)
	movl	%r11d, %esi
	jmp	.L401
.L471:
	movq	$0, -64(%rbp)
	movl	%r11d, %esi
	jmp	.L415
.L546:
	movq	-64(%rbp), %rdx
	xorl	%r10d, %r10d
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L389:
	testq	%r13, %r13
	je	.L454
	leaq	4(%r13), %rdx
	movl	%r15d, 0(%r13)
	leal	1(%r15), %esi
	movq	%rdx, -64(%rbp)
.L392:
	movl	$44, %edx
	cmpb	$63, %al
	je	.L393
	andl	$63, %eax
	leaq	_ZL8toBase64(%rip), %rdx
	movzbl	(%rdx,%rax), %edx
.L393:
	sall	$2, %ecx
	movq	-56(%rbp), %rax
	movb	%dl, 104(%r9)
	movq	%rbx, %r12
	movl	%ecx, %r15d
	movb	$1, 91(%r9)
	movl	$65536, %ecx
	movq	%rbx, %r10
	movl	$15, (%rax)
	andl	$60, %r15d
	movl	$1, %r13d
	cmpq	%rbx, -48(%rbp)
	jbe	.L378
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L454:
	movq	$0, -64(%rbp)
	movl	%r15d, %esi
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L463:
	movq	$0, -64(%rbp)
	movl	%r11d, %esi
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L453:
	movl	%r15d, %r11d
	xorl	%edx, %edx
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L537:
	movq	%r10, %rbx
	movl	%r11d, %r15d
	movq	%rdx, %r13
	movq	%rax, %r12
	jmp	.L377
.L372:
	cmpb	$0, 2(%rdi)
	je	.L548
	movq	%rdx, -64(%rbp)
	movl	%r11d, %esi
	movq	%rax, %r8
	jmp	.L433
.L423:
	cmpq	%r14, %r8
	jnb	.L428
	cmpq	$0, -64(%rbp)
	movb	$45, (%r8)
	leaq	1(%r8), %rax
	je	.L480
	subl	$1, %esi
	movq	%rax, %r8
.L432:
	movq	-64(%rbp), %rax
	movl	%esi, (%rax)
	leaq	4(%rax), %rdx
	jmp	.L422
.L424:
	cmpb	$62, %r15b
	ja	.L429
	movzbl	%r15b, %r15d
	leaq	_ZL8toBase64(%rip), %rax
	movzbl	(%rax,%r15), %edx
.L429:
	movsbq	91(%r9), %rax
	leal	1(%rax), %ecx
	movb	%cl, 91(%r9)
	movb	%dl, 104(%r9,%rax)
	movq	-56(%rbp), %rax
	movl	$15, (%rax)
.L428:
	movsbq	91(%r9), %rax
	leal	1(%rax), %edx
	movb	%dl, 91(%r9)
	movq	-64(%rbp), %rdx
	movb	$45, 104(%r9,%rax)
	movq	-56(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L422
.L538:
	testb	%r13b, %r13b
	jne	.L549
	movb	$45, (%rax)
	leaq	1(%rax), %rcx
	testq	%rdx, %rdx
	jne	.L435
	movl	%r11d, %esi
	movq	%rcx, %rax
	jmp	.L387
.L438:
	movq	%rax, %r8
	jmp	.L361
.L548:
	movsbl	%r13b, %ecx
	movq	%rax, %r8
	xorl	%r10d, %r10d
	sall	$16, %ecx
	jmp	.L370
.L364:
	movb	$38, (%r11)
	testq	%rdx, %rdx
	je	.L439
	movl	%esi, (%rdx)
	movl	%esi, %r11d
	addq	$4, %rdx
	movq	%rbx, %r12
	xorl	%r13d, %r13d
	jmp	.L360
.L549:
	cmpb	$62, %r15b
	jbe	.L436
	movl	%r11d, %esi
	movl	$44, %ecx
	jmp	.L383
.L384:
	cmpq	%r8, %r14
	jbe	.L386
	movb	$45, 1(%rax)
	addq	$2, %rax
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L541:
	movb	$45, 3(%rax)
	movl	%r15d, %r11d
	leaq	4(%rax), %rcx
	movq	%r13, %rdx
	movq	%r10, %r12
	xorl	%r15d, %r15d
.L435:
	movq	%rcx, %rax
	leal	-1(%r11), %r10d
	movl	%r11d, %esi
	movq	%rdx, %rcx
	xorl	%r13d, %r13d
.L434:
	movl	%r10d, (%rcx)
	leaq	4(%rcx), %rdx
	jmp	.L387
.L545:
	movb	$45, 1(%rax)
	addq	$2, %rax
	jmp	.L434
.L365:
	testq	%rdx, %rdx
	je	.L367
	movl	%esi, (%rdx)
	addl	$1, %esi
	addq	$4, %rdx
.L367:
	movq	-56(%rbp), %rbx
	movb	$45, 104(%r9)
	movq	%rax, %r8
	movb	$1, 91(%r9)
	movl	$15, (%rbx)
	jmp	.L361
.L480:
	xorl	%edx, %edx
	movq	%rax, %r8
	jmp	.L422
.L426:
	cmpq	%rax, %r14
	jbe	.L550
	movb	$45, 1(%r8)
	xorl	%edx, %edx
	addq	$2, %r8
	jmp	.L422
.L547:
	movq	%rdx, -64(%rbp)
	movq	%rax, %r8
	jmp	.L428
.L443:
	movq	%r13, -64(%rbp)
	movl	%r15d, %esi
	movq	-56(%rbp), %rax
	movq	%r12, %r8
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L373
.L540:
	movq	%r13, -64(%rbp)
	movl	%r15d, %esi
.L536:
	movq	%r12, %r8
	xorl	%ecx, %ecx
	movq	%r10, %r12
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L378
.L439:
	movl	%esi, %r11d
	movq	%rbx, %r12
	xorl	%r13d, %r13d
	jmp	.L360
.L444:
	movq	$0, -64(%rbp)
	movl	%r11d, %esi
	jmp	.L536
.L445:
	movq	-56(%rbp), %rax
	movl	%r11d, %esi
	movq	%r12, %r8
	xorl	%r13d, %r13d
	movq	$0, -64(%rbp)
	xorl	%r15d, %r15d
	jmp	.L373
.L550:
	movq	%rax, %r8
	jmp	.L428
.L543:
	movq	%rdx, -64(%rbp)
	jmp	.L433
.L379:
	movb	$45, 3(%rax)
	movl	%r11d, %esi
	addq	$4, %rax
	movq	%r10, %r12
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L387
.L446:
	movq	%r10, %r8
	movq	%rdx, -64(%rbp)
	movq	-56(%rbp), %rax
	movl	%r11d, %esi
	movq	%rbx, %r10
	movl	$1, %r13d
	jmp	.L373
.L542:
	movq	%rdx, -64(%rbp)
	movl	%r11d, %esi
	movq	%r10, %r8
	movq	%rbx, %r12
	movl	$65536, %ecx
	movl	$1, %r13d
	jmp	.L378
.L441:
	movq	%rax, %r8
	movq	%rdx, -64(%rbp)
	movq	-56(%rbp), %rax
	movl	%r11d, %esi
	movq	%r12, %r10
	movl	$2, %r13d
	jmp	.L373
.L544:
	movq	%rdx, -64(%rbp)
	movl	%r11d, %esi
	movq	%rax, %r8
	movl	$131072, %ecx
	movl	$2, %r13d
	jmp	.L378
	.cfi_endproc
.LFE2119:
	.size	_IMAPFromUnicodeWithOffsets, .-_IMAPFromUnicodeWithOffsets
	.p2align 4
	.type	_UTF7Open, @function
_UTF7Open:
.LFB2114:
	.cfi_startproc
	endbr64
	movl	56(%rdi), %eax
	testb	$14, %al
	je	.L554
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	sall	$28, %eax
	movl	$16777216, 72(%rdi)
	orl	$16777216, %eax
	movb	$0, 64(%rdi)
	movl	%eax, 80(%rdi)
	ret
	.cfi_endproc
.LFE2114:
	.size	_UTF7Open, .-_UTF7Open
	.p2align 4
	.type	_UTF7Reset, @function
_UTF7Reset:
.LFB2113:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	jle	.L556
.L558:
	movl	80(%rdi), %eax
	andl	$-268435456, %eax
	orl	$16777216, %eax
	movl	%eax, 80(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L556:
	movl	$16777216, 72(%rdi)
	movb	$0, 64(%rdi)
	jne	.L558
	ret
	.cfi_endproc
.LFE2113:
	.size	_UTF7Reset, .-_UTF7Reset
	.globl	_IMAPData_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_IMAPData_67, @object
	.size	_IMAPData_67, 296
_IMAPData_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL15_IMAPStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL9_IMAPImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL15_IMAPStaticData, @object
	.size	_ZL15_IMAPStaticData, 100
_ZL15_IMAPStaticData:
	.long	100
	.string	"IMAP-mailbox-name"
	.zero	42
	.long	0
	.byte	0
	.byte	32
	.byte	1
	.byte	4
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL9_IMAPImpl, @object
	.size	_ZL9_IMAPImpl, 144
_ZL9_IMAPImpl:
	.long	32
	.zero	4
	.quad	0
	.quad	0
	.quad	_UTF7Open
	.quad	0
	.quad	_UTF7Reset
	.quad	_IMAPToUnicodeWithOffsets
	.quad	_IMAPToUnicodeWithOffsets
	.quad	_IMAPFromUnicodeWithOffsets
	.quad	_IMAPFromUnicodeWithOffsets
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_UTF7Data_67
	.section	.data.rel.ro.local
	.align 32
	.type	_UTF7Data_67, @object
	.size	_UTF7Data_67, 296
_UTF7Data_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL15_UTF7StaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL9_UTF7Impl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL15_UTF7StaticData, @object
	.size	_ZL15_UTF7StaticData, 100
_ZL15_UTF7StaticData:
	.long	100
	.string	"UTF-7"
	.zero	54
	.long	0
	.byte	0
	.byte	27
	.byte	1
	.byte	4
	.string	"?"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL9_UTF7Impl, @object
	.size	_ZL9_UTF7Impl, 144
_ZL9_UTF7Impl:
	.long	27
	.zero	4
	.quad	0
	.quad	0
	.quad	_UTF7Open
	.quad	0
	.quad	_UTF7Reset
	.quad	_UTF7ToUnicodeWithOffsets
	.quad	_UTF7ToUnicodeWithOffsets
	.quad	_UTF7FromUnicodeWithOffsets
	.quad	_UTF7FromUnicodeWithOffsets
	.quad	0
	.quad	0
	.quad	_UTF7GetName
	.quad	0
	.quad	0
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL10fromBase64, @object
	.size	_ZL10fromBase64, 128
_ZL10fromBase64:
	.string	"\375\375\375\375\375\375\375\375\375\377\377\375\375\377\375\375\375\375\375\375\375\375\375\375\375\375\375\375\375\375\375\375\377\377\377\377\377\377\377\377\377\377\377>\377\376\377?456789:;<=\377\377\377\377\377\377\377"
	.ascii	"\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022"
	.ascii	"\023\024\025\026\027\030\031\377\375\377\377\377\377\032\033"
	.ascii	"\034\035\036\037 !\"#$%&'()*+,-./0123\377\377\377\375\375"
	.align 32
	.type	_ZL8toBase64, @object
	.size	_ZL8toBase64, 64
_ZL8toBase64:
	.ascii	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567"
	.ascii	"89+/"
	.align 32
	.type	_ZL24encodeDirectlyRestricted, @object
	.size	_ZL24encodeDirectlyRestricted, 128
_ZL24encodeDirectlyRestricted:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001"
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001\001"
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.zero	4
	.align 32
	.type	_ZL21encodeDirectlyMaximum, @object
	.size	_ZL21encodeDirectlyMaximum, 128
_ZL21encodeDirectlyMaximum:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001"
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\001\001\001\001\001\001\001\001\001\001"
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.string	"\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001"
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
