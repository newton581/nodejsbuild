	.file	"uinvchar.cpp"
	.text
	.p2align 4
	.globl	u_charsToUChars_67
	.type	u_charsToUChars_67, @function
u_charsToUChars_67:
.LFB2282:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jle	.L1
	movslq	%edx, %rax
	leaq	(%rsi,%rax,2), %rcx
	cmpq	%rcx, %rdi
	setnb	%cl
	addq	%rdi, %rax
	cmpq	%rax, %rsi
	setnb	%al
	orb	%al, %cl
	leal	-1(%rdx), %eax
	je	.L3
	cmpl	$14, %eax
	jbe	.L3
	movl	%edx, %ecx
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	shrl	$4, %ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rdi,%rax), %xmm0
	movdqa	%xmm0, %xmm2
	punpckhbw	%xmm1, %xmm0
	punpcklbw	%xmm1, %xmm2
	movups	%xmm0, 16(%rsi,%rax,2)
	movups	%xmm2, (%rsi,%rax,2)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L4
	movl	%edx, %r8d
	movl	%edx, %ecx
	andl	$-16, %r8d
	movl	%r8d, %eax
	subl	%r8d, %ecx
	addq	%rax, %rdi
	leaq	(%rsi,%rax,2), %rax
	cmpl	%r8d, %edx
	je	.L1
	movzbl	(%rdi), %edx
	movw	%dx, (%rax)
	cmpl	$1, %ecx
	je	.L1
	movzbl	1(%rdi), %edx
	movw	%dx, 2(%rax)
	cmpl	$2, %ecx
	je	.L1
	movzbl	2(%rdi), %edx
	movw	%dx, 4(%rax)
	cmpl	$3, %ecx
	je	.L1
	movzbl	3(%rdi), %edx
	movw	%dx, 6(%rax)
	cmpl	$4, %ecx
	je	.L1
	movzbl	4(%rdi), %edx
	movw	%dx, 8(%rax)
	cmpl	$5, %ecx
	je	.L1
	movzbl	5(%rdi), %edx
	movw	%dx, 10(%rax)
	cmpl	$6, %ecx
	je	.L1
	movzbl	6(%rdi), %edx
	movw	%dx, 12(%rax)
	cmpl	$7, %ecx
	je	.L1
	movzbl	7(%rdi), %edx
	movw	%dx, 14(%rax)
	cmpl	$8, %ecx
	je	.L1
	movzbl	8(%rdi), %edx
	movw	%dx, 16(%rax)
	cmpl	$9, %ecx
	je	.L1
	movzbl	9(%rdi), %edx
	movw	%dx, 18(%rax)
	cmpl	$10, %ecx
	je	.L1
	movzbl	10(%rdi), %edx
	movw	%dx, 20(%rax)
	cmpl	$11, %ecx
	je	.L1
	movzbl	11(%rdi), %edx
	movw	%dx, 22(%rax)
	cmpl	$12, %ecx
	je	.L1
	movzbl	12(%rdi), %edx
	movw	%dx, 24(%rax)
	cmpl	$13, %ecx
	je	.L1
	movzbl	13(%rdi), %edx
	movw	%dx, 26(%rax)
	cmpl	$14, %ecx
	je	.L1
	movzbl	14(%rdi), %edx
	movw	%dx, 28(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%eax, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L7:
	movzbl	(%rdi,%rax), %ecx
	movw	%cx, (%rsi,%rax,2)
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L7
.L1:
	ret
	.cfi_endproc
.LFE2282:
	.size	u_charsToUChars_67, .-u_charsToUChars_67
	.p2align 4
	.globl	u_UCharsToChars_67
	.type	u_UCharsToChars_67, @function
u_UCharsToChars_67:
.LFB2283:
	.cfi_startproc
	endbr64
	leal	-1(%rdx), %r8d
	xorl	%eax, %eax
	leaq	_ZL14invariantChars(%rip), %r9
	xorl	%r10d, %r10d
	testl	%edx, %edx
	jg	.L62
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rdx, %rax
.L62:
	movzwl	(%rdi,%rax,2), %ecx
	xorl	%edx, %edx
	cmpw	$127, %cx
	ja	.L60
	movq	%rcx, %rdx
	salq	$48, %rdx
	shrq	$53, %rdx
	movl	(%r9,%rdx,4), %edx
	shrl	%cl, %edx
	andl	$1, %edx
	movl	%ecx, %edx
	cmove	%r10d, %edx
.L60:
	movb	%dl, (%rsi,%rax)
	leaq	1(%rax), %rdx
	cmpq	%r8, %rax
	jne	.L66
.L58:
	ret
	.cfi_endproc
.LFE2283:
	.size	u_UCharsToChars_67, .-u_UCharsToChars_67
	.p2align 4
	.globl	uprv_isInvariantString_67
	.type	uprv_isInvariantString_67, @function
uprv_isInvariantString_67:
.LFB2284:
	.cfi_startproc
	endbr64
	addq	$1, %rdi
	leaq	_ZL14invariantChars(%rip), %rdx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L69:
	je	.L74
	movzbl	-1(%rdi), %ecx
	subl	$1, %esi
	testb	%cl, %cl
	je	.L72
.L71:
	testb	%cl, %cl
	js	.L75
	movq	%rcx, %rax
	shrq	$5, %rax
	andl	$7, %eax
	movl	(%rdx,%rax,4), %eax
	shrl	%cl, %eax
	testb	$1, %al
	je	.L75
.L72:
	addq	$1, %rdi
.L68:
	testl	%esi, %esi
	jns	.L69
	movzbl	-1(%rdi), %ecx
	testb	%cl, %cl
	jne	.L71
.L74:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2284:
	.size	uprv_isInvariantString_67, .-uprv_isInvariantString_67
	.p2align 4
	.globl	uprv_isInvariantUString_67
	.type	uprv_isInvariantUString_67, @function
uprv_isInvariantUString_67:
.LFB2285:
	.cfi_startproc
	endbr64
	addq	$2, %rdi
	leaq	_ZL14invariantChars(%rip), %rcx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L80:
	je	.L85
	movzwl	-2(%rdi), %eax
	subl	$1, %esi
.L82:
	cmpw	$127, %ax
	ja	.L86
	movq	%rax, %rdx
	addq	$2, %rdi
	salq	$48, %rdx
	shrq	$53, %rdx
	movl	(%rcx,%rdx,4), %edx
	btl	%eax, %edx
	jnc	.L86
.L83:
	testl	%esi, %esi
	jns	.L80
	movzwl	-2(%rdi), %eax
	testw	%ax, %ax
	jne	.L82
.L85:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2285:
	.size	uprv_isInvariantUString_67, .-uprv_isInvariantUString_67
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"uprv_ebcdicFromAscii() string[%d] contains a variant character in position %d\n"
	.text
	.p2align 4
	.globl	uprv_ebcdicFromAscii_67
	.type	uprv_ebcdicFromAscii_67, @function
uprv_ebcdicFromAscii_67:
.LFB2286:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L95
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%r8, %rbx
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L88
	testq	%rdi, %rdi
	movq	%rcx, %r11
	sete	%cl
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %cl
	jne	.L90
	testl	%edx, %edx
	js	.L90
	jle	.L91
	testq	%r11, %r11
	je	.L90
.L91:
	testl	%edx, %edx
	je	.L97
	leal	-1(%rdx), %r13d
	xorl	%eax, %eax
	leaq	_ZL14invariantChars(%rip), %r8
	leaq	_ZL15ebcdicFromAscii(%rip), %r12
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L92:
	movzbl	%cl, %r9d
	movl	%r9d, %r10d
	sarl	$5, %r10d
	movslq	%r10d, %r10
	movl	(%r8,%r10,4), %r10d
	shrl	%cl, %r10d
	andl	$1, %r10d
	je	.L93
	movzbl	(%r12,%r9), %ecx
	movb	%cl, (%r11,%rax)
	leaq	1(%rax), %rcx
	cmpq	%r13, %rax
	je	.L112
	movq	%rcx, %rax
.L94:
	movzbl	(%rsi,%rax), %ecx
	movl	%edx, %r14d
	subl	%eax, %r14d
	testb	%cl, %cl
	jns	.L92
.L93:
	movl	%edx, %ecx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rsi
	subl	%r14d, %ecx
	call	udata_printError_67@PLT
	movl	$10, (%rbx)
	xorl	%eax, %eax
.L88:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	%edx, %eax
	jmp	.L88
.L97:
	xorl	%eax, %eax
	jmp	.L88
	.cfi_endproc
.LFE2286:
	.size	uprv_ebcdicFromAscii_67, .-uprv_ebcdicFromAscii_67
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"uprv_copyFromAscii() string[%d] contains a variant character in position %d\n"
	.text
	.p2align 4
	.globl	uprv_copyAscii_67
	.type	uprv_copyAscii_67, @function
uprv_copyAscii_67:
.LFB2287:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L120
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%r8, %rbx
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L113
	movl	%edx, %r12d
	testq	%rsi, %rsi
	movl	%r12d, %eax
	sete	%dl
	shrl	$31, %eax
	orb	%al, %dl
	jne	.L115
	testq	%rdi, %rdi
	je	.L115
	testl	%r12d, %r12d
	movq	%rcx, %r9
	setg	%r11b
	testq	%rcx, %rcx
	jne	.L116
	testb	%r11b, %r11b
	jne	.L115
.L116:
	testl	%r12d, %r12d
	je	.L122
	movl	%r12d, %r8d
	movq	%rsi, %rdx
	leaq	_ZL14invariantChars(%rip), %r10
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%rcx, %rax
	shrq	$5, %rax
	andl	$7, %eax
	movl	(%r10,%rax,4), %eax
	shrl	%cl, %eax
	testb	$1, %al
	je	.L118
	subl	$1, %r8d
	je	.L144
.L119:
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	testb	%cl, %cl
	jns	.L117
.L118:
	movl	%r12d, %ecx
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	movl	%r12d, %edx
	subl	%r8d, %ecx
	call	udata_printError_67@PLT
	movl	$10, (%rbx)
	xorl	%eax, %eax
.L113:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	cmpq	%r9, %rsi
	je	.L123
	testb	%r11b, %r11b
	je	.L123
	movslq	%r12d, %rdx
	movq	%r9, %rdi
	call	memcpy@PLT
	movl	%r12d, %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L123:
	movl	%r12d, %eax
	jmp	.L113
.L122:
	xorl	%eax, %eax
	jmp	.L113
	.cfi_endproc
.LFE2287:
	.size	uprv_copyAscii_67, .-uprv_copyAscii_67
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"uprv_asciiFromEbcdic() string[%d] contains a variant character in position %d\n"
	.text
	.p2align 4
	.globl	uprv_asciiFromEbcdic_67
	.type	uprv_asciiFromEbcdic_67, @function
uprv_asciiFromEbcdic_67:
.LFB2288:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L152
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%r8, %rbx
	subq	$8, %rsp
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L145
	testq	%rdi, %rdi
	movq	%rcx, %r9
	sete	%cl
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %cl
	jne	.L147
	testl	%edx, %edx
	js	.L147
	jle	.L148
	testq	%r9, %r9
	je	.L147
.L148:
	testl	%edx, %edx
	je	.L154
	leal	-1(%rdx), %r11d
	xorl	%eax, %eax
	leaq	_ZL15asciiFromEbcdic(%rip), %r12
	leaq	_ZL14invariantChars(%rip), %r13
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L172:
	movq	%rcx, %rax
.L151:
	movzbl	(%rsi,%rax), %ecx
	movl	%edx, %r10d
	subl	%eax, %r10d
	testb	%cl, %cl
	je	.L149
	movzbl	(%r12,%rcx), %ecx
	testb	%cl, %cl
	jle	.L150
	movq	%rcx, %r8
	shrq	$5, %r8
	andl	$7, %r8d
	movl	0(%r13,%r8,4), %r8d
	shrl	%cl, %r8d
	andl	$1, %r8d
	je	.L150
.L149:
	movb	%cl, (%r9,%rax)
	leaq	1(%rax), %rcx
	cmpq	%rax, %r11
	jne	.L172
	movl	%edx, %eax
.L145:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	%edx, %ecx
	xorl	%eax, %eax
	leaq	.LC2(%rip), %rsi
	subl	%r10d, %ecx
	call	udata_printError_67@PLT
	movl	$10, (%rbx)
	xorl	%eax, %eax
	jmp	.L145
.L154:
	xorl	%eax, %eax
	jmp	.L145
	.cfi_endproc
.LFE2288:
	.size	uprv_asciiFromEbcdic_67, .-uprv_asciiFromEbcdic_67
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"uprv_copyEbcdic() string[%] contains a variant character in position %d\n"
	.text
	.p2align 4
	.globl	uprv_copyEbcdic_67
	.type	uprv_copyEbcdic_67, @function
uprv_copyEbcdic_67:
.LFB2289:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L181
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%r8, %rbx
	subq	$8, %rsp
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L173
	movl	%edx, %r12d
	testq	%rsi, %rsi
	movl	%r12d, %eax
	sete	%dl
	shrl	$31, %eax
	orb	%al, %dl
	jne	.L175
	testq	%rdi, %rdi
	je	.L175
	testl	%r12d, %r12d
	movq	%rcx, %r10
	setg	%r13b
	testq	%rcx, %rcx
	jne	.L176
	testb	%r13b, %r13b
	jne	.L175
.L176:
	testl	%r12d, %r12d
	je	.L183
	movl	%r12d, %r9d
	movq	%rsi, %rdx
	leaq	_ZL15asciiFromEbcdic(%rip), %r8
	leaq	_ZL14invariantChars(%rip), %r11
	.p2align 4,,10
	.p2align 3
.L180:
	movzbl	(%rdx), %eax
	addq	$1, %rdx
	testb	%al, %al
	je	.L177
	movzbl	(%r8,%rax), %ecx
	testb	%cl, %cl
	jle	.L178
	movq	%rcx, %rax
	shrq	$5, %rax
	andl	$7, %eax
	movl	(%r11,%rax,4), %eax
	shrl	%cl, %eax
	testb	$1, %al
	je	.L178
.L177:
	subl	$1, %r9d
	jne	.L180
	cmpq	%r10, %rsi
	je	.L184
	testb	%r13b, %r13b
	je	.L184
	movslq	%r12d, %rdx
	movq	%r10, %rdi
	call	memcpy@PLT
	movl	%r12d, %eax
.L173:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	%r12d, %ecx
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	movl	%r12d, %edx
	subl	%r9d, %ecx
	call	udata_printError_67@PLT
	movl	$10, (%rbx)
	xorl	%eax, %eax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L184:
	movl	%r12d, %eax
	jmp	.L173
.L183:
	xorl	%eax, %eax
	jmp	.L173
	.cfi_endproc
.LFE2289:
	.size	uprv_copyEbcdic_67, .-uprv_copyEbcdic_67
	.p2align 4
	.globl	uprv_isEbcdicAtSign_67
	.type	uprv_isEbcdicAtSign_67, @function
uprv_isEbcdicAtSign_67:
.LFB2290:
	.cfi_startproc
	endbr64
	testb	%dil, %dil
	je	.L209
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	%dil, %esi
	leaq	_ZZ22uprv_isEbcdicAtSign_67E13ebcdicAtSigns(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	strchr@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testq	%rax, %rax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore 6
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE2290:
	.size	uprv_isEbcdicAtSign_67, .-uprv_isEbcdicAtSign_67
	.p2align 4
	.globl	uprv_compareInvAscii_67
	.type	uprv_compareInvAscii_67, @function
uprv_compareInvAscii_67:
.LFB2291:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L231
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$-1, %edx
	jl	.L223
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L223
	movl	%r8d, %r14d
	cmpl	$-1, %r8d
	jl	.L223
	movq	%rsi, %rbx
	cmpl	$-1, %edx
	jne	.L216
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %r13d
.L216:
	cmpl	$-1, %r14d
	je	.L234
.L217:
	cmpl	%r14d, %r13d
	movl	%r14d, %eax
	cmovle	%r13d, %eax
	testl	%eax, %eax
	jle	.L218
	leal	-1(%rax), %esi
	xorl	%edi, %edi
	movl	$-1, %r8d
	leaq	_ZL14invariantChars(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L221:
	movzbl	(%rbx,%rdi), %ecx
	movl	$-1, %eax
	testb	%cl, %cl
	js	.L219
	movsbl	%cl, %eax
	movl	%eax, %r9d
	sarl	$5, %r9d
	movslq	%r9d, %r9
	movl	(%rdx,%r9,4), %r9d
	shrl	%cl, %r9d
	andl	$1, %r9d
	cmove	%r8d, %eax
.L219:
	movzwl	(%r12,%rdi,2), %ecx
	cmpl	$127, %ecx
	jg	.L220
	movl	%ecx, %r10d
	sarl	$5, %r10d
	movslq	%r10d, %r10
	movl	(%rdx,%r10,4), %r10d
	btl	%ecx, %r10d
	jc	.L235
.L220:
	popq	%rbx
	addl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	xorl	%eax, %eax
.L214:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	subl	%ecx, %eax
	jne	.L214
	leaq	1(%rdi), %rax
	cmpq	%rdi, %rsi
	je	.L218
	movq	%rax, %rdi
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L218:
	movl	%r13d, %eax
	subl	%r14d, %eax
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%r12, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %r14d
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2291:
	.size	uprv_compareInvAscii_67, .-uprv_compareInvAscii_67
	.p2align 4
	.globl	uprv_compareInvEbcdic_67
	.type	uprv_compareInvEbcdic_67, @function
uprv_compareInvEbcdic_67:
.LFB2292:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L255
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$-1, %edx
	jl	.L245
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L245
	movl	%r8d, %r14d
	cmpl	$-1, %r8d
	jl	.L245
	movq	%rsi, %rbx
	cmpl	$-1, %edx
	jne	.L238
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %r13d
.L238:
	cmpl	$-1, %r14d
	je	.L258
.L239:
	cmpl	%r14d, %r13d
	movl	%r14d, %eax
	cmovle	%r13d, %eax
	testl	%eax, %eax
	jle	.L240
	leal	-1(%rax), %edx
	xorl	%edi, %edi
	leaq	_ZL15asciiFromEbcdic(%rip), %r8
	movl	$-1, %r9d
	leaq	_ZL14invariantChars(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L243:
	movzbl	(%rbx,%rdi), %ecx
	xorl	%eax, %eax
	testb	%cl, %cl
	je	.L241
	movzbl	(%r8,%rcx), %ecx
	movl	$-1, %eax
	testb	%cl, %cl
	jg	.L259
.L241:
	movzwl	(%r12,%rdi,2), %ecx
	cmpl	$127, %ecx
	jg	.L242
	movl	%ecx, %r11d
	sarl	$5, %r11d
	movslq	%r11d, %r11
	movl	(%rsi,%r11,4), %r11d
	btl	%ecx, %r11d
	jc	.L260
.L242:
	popq	%rbx
	addl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	xorl	%eax, %eax
.L236:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	subl	%ecx, %eax
	jne	.L236
	leaq	1(%rdi), %rax
	cmpq	%rdi, %rdx
	je	.L240
	movq	%rax, %rdi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L259:
	movzbl	%cl, %eax
	movl	%eax, %r10d
	sarl	$5, %r10d
	movslq	%r10d, %r10
	movl	(%rsi,%r10,4), %r10d
	shrl	%cl, %r10d
	andl	$1, %r10d
	cmove	%r9d, %eax
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L240:
	movl	%r13d, %eax
	subl	%r14d, %eax
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%r12, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %r14d
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2292:
	.size	uprv_compareInvEbcdic_67, .-uprv_compareInvEbcdic_67
	.p2align 4
	.globl	uprv_compareInvEbcdicAsAscii_67
	.type	uprv_compareInvEbcdicAsAscii_67, @function
uprv_compareInvEbcdicAsAscii_67:
.LFB2293:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %r8d
	movzbl	(%rsi), %edx
	movl	$1, %eax
	cmpl	%r8d, %edx
	je	.L262
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L284:
	movzbl	(%rdi,%rax), %r8d
	movzbl	(%rsi,%rax), %edx
	addq	$1, %rax
	cmpl	%edx, %r8d
	jne	.L268
.L262:
	testl	%r8d, %r8d
	jne	.L284
.L261:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	testl	%r8d, %r8d
	je	.L263
	movslq	%r8d, %rax
	leaq	_ZL15asciiFromEbcdic(%rip), %rcx
	movzbl	(%rcx,%rax), %ecx
	testb	%cl, %cl
	jg	.L285
.L264:
	negl	%r8d
.L263:
	testl	%edx, %edx
	je	.L261
	movslq	%edx, %rax
	leaq	_ZL15asciiFromEbcdic(%rip), %rcx
	movzbl	(%rcx,%rax), %ecx
	testb	%cl, %cl
	jg	.L286
.L266:
	addl	%edx, %r8d
	movl	%r8d, %eax
	ret
.L285:
	movzbl	%cl, %esi
	leaq	_ZL14invariantChars(%rip), %rdi
	movl	%esi, %eax
	sarl	$5, %eax
	cltq
	movl	(%rdi,%rax,4), %eax
	shrl	%cl, %eax
	testb	$1, %al
	je	.L264
	movl	%esi, %r8d
	jmp	.L263
.L286:
	movzbl	%cl, %esi
	leaq	_ZL14invariantChars(%rip), %rdi
	movl	%esi, %eax
	sarl	$5, %eax
	cltq
	movl	(%rdi,%rax,4), %eax
	shrl	%cl, %eax
	testb	$1, %al
	je	.L266
	subl	%esi, %r8d
	jmp	.L261
	.cfi_endproc
.LFE2293:
	.size	uprv_compareInvEbcdicAsAscii_67, .-uprv_compareInvEbcdicAsAscii_67
	.p2align 4
	.globl	uprv_ebcdicToAscii_67
	.type	uprv_ebcdicToAscii_67, @function
uprv_ebcdicToAscii_67:
.LFB2294:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edi
	leaq	_ZL15asciiFromEbcdic(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	ret
	.cfi_endproc
.LFE2294:
	.size	uprv_ebcdicToAscii_67, .-uprv_ebcdicToAscii_67
	.p2align 4
	.globl	uprv_ebcdicToLowercaseAscii_67
	.type	uprv_ebcdicToLowercaseAscii_67, @function
uprv_ebcdicToLowercaseAscii_67:
.LFB2295:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edi
	leaq	_ZL24lowercaseAsciiFromEbcdic(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	ret
	.cfi_endproc
.LFE2295:
	.size	uprv_ebcdicToLowercaseAscii_67, .-uprv_ebcdicToLowercaseAscii_67
	.p2align 4
	.globl	uprv_aestrncpy_67
	.type	uprv_aestrncpy_67, @function
uprv_aestrncpy_67:
.LFB2296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	cmpl	$-1, %edx
	jne	.L290
	movq	%rsi, %rdi
	call	strlen@PLT
	leal	1(%rax), %edx
.L290:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L296
	testl	%edx, %edx
	jle	.L292
	movq	%r12, %rdi
	leaq	_ZL15asciiFromEbcdic(%rip), %rcx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L304:
	testl	%edx, %edx
	je	.L292
.L293:
	movzbl	(%rcx,%rax), %eax
	addq	$1, %rbx
	addq	$1, %rdi
	subl	$1, %edx
	movb	%al, -1(%rdi)
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L304
.L291:
	testl	%edx, %edx
	jle	.L292
	subl	$1, %edx
	xorl	%esi, %esi
	movslq	%edx, %rdx
	addq	$1, %rdx
	call	memset@PLT
.L292:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L296:
	.cfi_restore_state
	movq	%r12, %rdi
	jmp	.L291
	.cfi_endproc
.LFE2296:
	.size	uprv_aestrncpy_67, .-uprv_aestrncpy_67
	.p2align 4
	.globl	uprv_eastrncpy_67
	.type	uprv_eastrncpy_67, @function
uprv_eastrncpy_67:
.LFB2297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	cmpl	$-1, %edx
	jne	.L307
	movq	%rsi, %rdi
	call	strlen@PLT
	leal	1(%rax), %edx
.L307:
	movzbl	(%rbx), %eax
	movq	%r12, %rdi
	testb	%al, %al
	je	.L308
	leaq	_ZL15ebcdicFromAscii(%rip), %rsi
	movl	$111, %ecx
	testl	%edx, %edx
	jg	.L309
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L326:
	testl	%edx, %edx
	je	.L319
.L309:
	movzbl	(%rsi,%rax), %eax
	addq	$1, %rbx
	testb	%al, %al
	cmove	%ecx, %eax
	addq	$1, %rdi
	subl	$1, %edx
	movb	%al, -1(%rdi)
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L326
.L308:
	testl	%edx, %edx
	jle	.L319
	subl	$1, %edx
	xorl	%esi, %esi
	movslq	%edx, %rdx
	addq	$1, %rdx
	call	memset@PLT
.L319:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2297:
	.size	uprv_eastrncpy_67, .-uprv_eastrncpy_67
	.section	.rodata
	.align 8
	.type	_ZZ22uprv_isEbcdicAtSign_67E13ebcdicAtSigns, @object
	.size	_ZZ22uprv_isEbcdicAtSign_67E13ebcdicAtSigns, 11
_ZZ22uprv_isEbcdicAtSign_67E13ebcdicAtSigns:
	.string	"|Df\200\254\256\257\265\354\357"
	.align 16
	.type	_ZL14invariantChars, @object
	.size	_ZL14invariantChars, 16
_ZL14invariantChars:
	.long	-1025
	.long	-27
	.long	-2013265922
	.long	-2013265922
	.align 32
	.type	_ZL24lowercaseAsciiFromEbcdic, @object
	.size	_ZL24lowercaseAsciiFromEbcdic, 256
_ZL24lowercaseAsciiFromEbcdic:
	.string	""
	.string	"\001\002\003"
	.string	"\t"
	.string	"\177"
	.string	""
	.string	""
	.string	"\013\f\r\016\017\020\021\022\023"
	.string	"\n\b"
	.string	"\030\031"
	.string	""
	.string	"\034\035\036\037"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\n\027\033"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\005\006\007"
	.string	""
	.string	"\026"
	.string	""
	.string	""
	.string	""
	.string	"\004"
	.string	""
	.string	""
	.string	""
	.string	"\024\025"
	.string	"\032 "
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	".<(+|&"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"!$*);^-/"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	",%_>?"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"`:#@'=\""
	.string	"abcdefghi"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"jklmnopqr"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"~stuvwxyz"
	.string	""
	.string	""
	.string	"["
	.string	""
	.string	"^"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"[]"
	.string	"]"
	.string	""
	.string	"{abcdefghi"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"}jklmnopqr"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"|"
	.string	"stuvwxyz"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"0123456789"
	.zero	5
	.align 32
	.type	_ZL15ebcdicFromAscii, @object
	.size	_ZL15ebcdicFromAscii, 256
_ZL15ebcdicFromAscii:
	.string	""
	.string	"\001\002\0037-./\026\005"
	.string	"\013\f\r\016\017\020\021\022\023<=2&\030\031?'\034\035\036\037@"
	.string	"\177"
	.string	""
	.string	"lP}M]\\Nk`Ka\360\361\362\363\364\365\366\367\370\371z^L~no"
	.string	"\301\302\303\304\305\306\307\310\311\321\322\323\324\325\326\327\330\331\342\343\344\345\346\347\350\351"
	.string	""
	.string	""
	.string	""
	.string	"m"
	.string	"\201\202\203\204\205\206\207\210\211\221\222\223\224\225\226\227\230\231\242\243\244\245\246\247\250\251"
	.string	""
	.string	""
	.string	""
	.string	"\007"
	.zero	127
	.align 32
	.type	_ZL15asciiFromEbcdic, @object
	.size	_ZL15asciiFromEbcdic, 256
_ZL15asciiFromEbcdic:
	.string	""
	.string	"\001\002\003"
	.string	"\t"
	.string	"\177"
	.string	""
	.string	""
	.string	"\013\f\r\016\017\020\021\022\023"
	.string	"\n\b"
	.string	"\030\031"
	.string	""
	.string	"\034\035\036\037"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\n\027\033"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\005\006\007"
	.string	""
	.string	"\026"
	.string	""
	.string	""
	.string	""
	.string	"\004"
	.string	""
	.string	""
	.string	""
	.string	"\024\025"
	.string	"\032 "
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	".<(+|&"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"!$*);^-/"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	",%_>?"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"`:#@'=\""
	.string	"abcdefghi"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"jklmnopqr"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"~stuvwxyz"
	.string	""
	.string	""
	.string	"["
	.string	""
	.string	"^"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"[]"
	.string	"]"
	.string	""
	.string	"{ABCDEFGHI"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"}JKLMNOPQR"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\\"
	.string	"STUVWXYZ"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"0123456789"
	.zero	5
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
