	.file	"dictionarydata.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723UCharsDictionaryMatcher7getTypeEv
	.type	_ZNK6icu_6723UCharsDictionaryMatcher7getTypeEv, @function
_ZNK6icu_6723UCharsDictionaryMatcher7getTypeEv:
.LFB2403:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2403:
	.size	_ZNK6icu_6723UCharsDictionaryMatcher7getTypeEv, .-_ZNK6icu_6723UCharsDictionaryMatcher7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722BytesDictionaryMatcher7getTypeEv
	.type	_ZNK6icu_6722BytesDictionaryMatcher7getTypeEv, @function
_ZNK6icu_6722BytesDictionaryMatcher7getTypeEv:
.LFB2410:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2410:
	.size	_ZNK6icu_6722BytesDictionaryMatcher7getTypeEv, .-_ZNK6icu_6722BytesDictionaryMatcher7getTypeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UCharsDictionaryMatcherD2Ev
	.type	_ZN6icu_6723UCharsDictionaryMatcherD2Ev, @function
_ZN6icu_6723UCharsDictionaryMatcherD2Ev:
.LFB2400:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6723UCharsDictionaryMatcherE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	jmp	udata_close_67@PLT
	.cfi_endproc
.LFE2400:
	.size	_ZN6icu_6723UCharsDictionaryMatcherD2Ev, .-_ZN6icu_6723UCharsDictionaryMatcherD2Ev
	.globl	_ZN6icu_6723UCharsDictionaryMatcherD1Ev
	.set	_ZN6icu_6723UCharsDictionaryMatcherD1Ev,_ZN6icu_6723UCharsDictionaryMatcherD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UCharsDictionaryMatcherD0Ev
	.type	_ZN6icu_6723UCharsDictionaryMatcherD0Ev, @function
_ZN6icu_6723UCharsDictionaryMatcherD0Ev:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6723UCharsDictionaryMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	call	udata_close_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2402:
	.size	_ZN6icu_6723UCharsDictionaryMatcherD0Ev, .-_ZN6icu_6723UCharsDictionaryMatcherD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722BytesDictionaryMatcherD2Ev
	.type	_ZN6icu_6722BytesDictionaryMatcherD2Ev, @function
_ZN6icu_6722BytesDictionaryMatcherD2Ev:
.LFB2406:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6722BytesDictionaryMatcherE(%rip), %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	jmp	udata_close_67@PLT
	.cfi_endproc
.LFE2406:
	.size	_ZN6icu_6722BytesDictionaryMatcherD2Ev, .-_ZN6icu_6722BytesDictionaryMatcherD2Ev
	.globl	_ZN6icu_6722BytesDictionaryMatcherD1Ev
	.set	_ZN6icu_6722BytesDictionaryMatcherD1Ev,_ZN6icu_6722BytesDictionaryMatcherD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6722BytesDictionaryMatcherD0Ev
	.type	_ZN6icu_6722BytesDictionaryMatcherD0Ev, @function
_ZN6icu_6722BytesDictionaryMatcherD0Ev:
.LFB2408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6722BytesDictionaryMatcherE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	call	udata_close_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2408:
	.size	_ZN6icu_6722BytesDictionaryMatcherD0Ev, .-_ZN6icu_6722BytesDictionaryMatcherD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722BytesDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_
	.type	_ZNK6icu_6722BytesDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_, @function
_ZNK6icu_6722BytesDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_:
.LFB2411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdi, -112(%rbp)
	movl	%edx, -104(%rbp)
	movq	%rax, -144(%rbp)
	movq	24(%rbp), %rax
	movl	%ecx, -116(%rbp)
	movq	%r8, -128(%rbp)
	movq	%r9, -136(%rbp)
	movq	%rax, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rsi, %rdi
	movq	$0, -96(%rbp)
	movl	$-1, -72(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	call	utext_getNativeIndex_67@PLT
	movq	%r12, %rdi
	movl	%eax, -100(%rbp)
	call	utext_next32_67@PLT
	testl	%eax, %eax
	js	.L29
	movl	%eax, %edx
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r15
	xorl	%r13d, %r13d
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	$16777216, %ecx
	je	.L56
.L13:
	movl	$-1, -72(%rbp)
.L15:
	movq	-88(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_679BytesTrie8nextImplEPKhi@PLT
	movl	%eax, %r14d
.L18:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	utext_getNativeIndex_67@PLT
	movl	%eax, %ecx
	subl	-100(%rbp), %ecx
	cmpl	$1, %r14d
	jle	.L20
	cmpl	%r13d, -116(%rbp)
	jle	.L21
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movslq	%r13d, %rax
	movl	%ecx, -120(%rbp)
	leaq	(%rdi,%rax,4), %r8
	movq	-80(%rbp), %rdi
	movq	%r8, -152(%rbp)
	movzbl	(%rdi), %esi
	addq	$1, %rdi
	sarl	%esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	movq	-152(%rbp), %r8
	movl	-120(%rbp), %ecx
	movl	%eax, (%r8)
.L22:
	movq	-128(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L23
	movslq	%r13d, %rax
	movl	%ecx, (%rsi,%rax,4)
.L23:
	movq	-136(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L24
	movslq	%r13d, %rax
	movl	%ebx, (%rsi,%rax,4)
.L24:
	addl	$1, %r13d
.L21:
	cmpl	$2, %r14d
	je	.L11
.L25:
	cmpl	-104(%rbp), %ecx
	jge	.L11
	movq	%r12, %rdi
	call	utext_next32_67@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L11
.L26:
	movq	-112(%rbp), %rax
	movl	16(%rax), %eax
	movl	%eax, %ecx
	andl	$2130706432, %ecx
	testl	%ebx, %ebx
	je	.L57
	cmpl	$16777216, %ecx
	je	.L58
.L19:
	movl	%edx, %esi
	movq	%r15, %rdi
	call	_ZN6icu_679BytesTrie4nextEi@PLT
	movl	%eax, %r14d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	testl	%r14d, %r14d
	jne	.L25
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L27
	movl	%ebx, (%rax)
.L27:
	movq	%r15, %rdi
	call	_ZN6icu_679BytesTrieD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	cmpl	$8205, %edx
	je	.L17
	cmpl	$8204, %edx
	je	.L60
	andl	$2097151, %eax
	subl	%eax, %edx
	cmpl	$253, %edx
	jbe	.L13
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$-1, -72(%rbp)
	movl	$255, %edx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L58:
	cmpl	$8205, %edx
	je	.L30
	cmpl	$8204, %edx
	je	.L31
	andl	$2097151, %eax
	subl	%eax, %edx
	cmpl	$253, %edx
	jbe	.L19
	movl	$-1, %edx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$255, %edx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$254, %edx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$-1, -72(%rbp)
	movl	$254, %edx
	jmp	.L15
.L29:
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	leaq	-96(%rbp), %r15
	jmp	.L11
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2411:
	.size	_ZNK6icu_6722BytesDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_, .-_ZNK6icu_6722BytesDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6723UCharsDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_
	.type	_ZNK6icu_6723UCharsDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_, @function
_ZNK6icu_6723UCharsDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_:
.LFB2404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movl	%edx, -104(%rbp)
	movl	%ecx, -108(%rbp)
	movq	%rax, -136(%rbp)
	movq	24(%rbp), %rax
	movq	%r8, -120(%rbp)
	movq	%r9, -128(%rbp)
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movl	$-1, -72(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rsi, %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%r12, %rdi
	movl	%eax, -100(%rbp)
	call	utext_next32_67@PLT
	testl	%eax, %eax
	js	.L79
	movl	%eax, %edx
	xorl	%ebx, %ebx
	leaq	-96(%rbp), %r15
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L76:
	testl	%ebx, %ebx
	jne	.L63
	movq	-88(%rbp), %rsi
	movq	%r15, %rdi
	movl	$-1, -72(%rbp)
	call	_ZN6icu_6710UCharsTrie8nextImplEPKDsi@PLT
	movl	%eax, %r14d
.L64:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	utext_getNativeIndex_67@PLT
	subl	-100(%rbp), %eax
	cmpl	$1, %r14d
	jle	.L65
	cmpl	%r13d, -108(%rbp)
	jle	.L66
	movq	-136(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L67
	movq	-80(%rbp), %r8
	movslq	%r13d, %rcx
	leaq	(%rsi,%rcx,4), %rdi
	movzwl	(%r8), %ecx
	testw	%cx, %cx
	js	.L105
	movzwl	%cx, %r9d
	cmpw	$16447, %cx
	ja	.L71
	sarl	$6, %r9d
	leal	-1(%r9), %esi
.L69:
	movl	%esi, (%rdi)
.L67:
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L73
	movslq	%r13d, %rcx
	movl	%eax, (%rdx,%rcx,4)
.L73:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L74
	movslq	%r13d, %rcx
	movl	%ebx, (%rdi,%rcx,4)
.L74:
	addl	$1, %r13d
.L66:
	cmpl	$2, %r14d
	je	.L62
.L75:
	cmpl	-104(%rbp), %eax
	jge	.L62
	movq	%r12, %rdi
	call	utext_next32_67@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	jns	.L76
.L62:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L77
	movl	%ebx, (%rax)
.L77:
	movq	%r15, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$104, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	%edx, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6710UCharsTrie4nextEi@PLT
	movl	%eax, %r14d
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L65:
	testl	%r14d, %r14d
	jne	.L75
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L105:
	movl	%ecx, %r9d
	movl	%ecx, %esi
	andw	$32767, %r9w
	andl	$32767, %esi
	andb	$64, %ch
	je	.L69
	movzwl	2(%r8), %r10d
	cmpw	$32767, %r9w
	je	.L70
	leal	-16384(%rsi), %ecx
	sall	$16, %ecx
	orl	%r10d, %ecx
	movl	%ecx, %esi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L71:
	movzwl	2(%r8), %esi
	cmpl	$32703, %r9d
	jg	.L72
	andl	$32704, %ecx
	subl	$16448, %ecx
	sall	$10, %ecx
	orl	%ecx, %esi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L72:
	movzwl	4(%r8), %ecx
	sall	$16, %esi
	orl	%ecx, %esi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movzwl	4(%r8), %ecx
	movl	%r10d, %esi
	sall	$16, %esi
	orl	%ecx, %esi
	jmp	.L69
.L79:
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	leaq	-96(%rbp), %r15
	jmp	.L62
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2404:
	.size	_ZNK6icu_6723UCharsDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_, .-_ZNK6icu_6723UCharsDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717DictionaryMatcherD2Ev
	.type	_ZN6icu_6717DictionaryMatcherD2Ev, @function
_ZN6icu_6717DictionaryMatcherD2Ev:
.LFB2396:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2396:
	.size	_ZN6icu_6717DictionaryMatcherD2Ev, .-_ZN6icu_6717DictionaryMatcherD2Ev
	.globl	_ZN6icu_6717DictionaryMatcherD1Ev
	.set	_ZN6icu_6717DictionaryMatcherD1Ev,_ZN6icu_6717DictionaryMatcherD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717DictionaryMatcherD0Ev
	.type	_ZN6icu_6717DictionaryMatcherD0Ev, @function
_ZN6icu_6717DictionaryMatcherD0Ev:
.LFB2398:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2398:
	.size	_ZN6icu_6717DictionaryMatcherD0Ev, .-_ZN6icu_6717DictionaryMatcherD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6722BytesDictionaryMatcher9transformEi
	.type	_ZNK6icu_6722BytesDictionaryMatcher9transformEi, @function
_ZNK6icu_6722BytesDictionaryMatcher9transformEi:
.LFB2409:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %edx
	movl	%esi, %eax
	movl	%edx, %ecx
	andl	$2130706432, %ecx
	cmpl	$16777216, %ecx
	je	.L114
.L110:
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	cmpl	$8205, %esi
	je	.L111
	cmpl	$8204, %esi
	je	.L112
	andl	$2097151, %edx
	subl	%edx, %eax
	cmpl	$253, %eax
	jbe	.L110
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$255, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$254, %eax
	ret
	.cfi_endproc
.LFE2409:
	.size	_ZNK6icu_6722BytesDictionaryMatcher9transformEi, .-_ZNK6icu_6722BytesDictionaryMatcher9transformEi
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"udict_swap(): data format %02x.%02x.%02x.%02x (format version %02x) is not recognized as dictionary data\n"
	.align 8
.LC1:
	.string	"udict_swap(): too few bytes (%d after header) for dictionary data\n"
	.align 8
.LC2:
	.string	"udict_swap(): too few bytes (%d after header) for all of dictionary data\n"
	.align 8
.LC3:
	.string	"udict_swap(): unknown trie type!\n"
	.text
	.p2align 4
	.globl	udict_swap_67
	.type	udict_swap_67, @function
udict_swap_67:
.LFB2412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	udata_swapDataHeader_67@PLT
	xorl	%r8d, %r8d
	movl	%eax, -100(%rbp)
	testq	%r15, %r15
	je	.L115
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L115
	cmpw	$26948, 12(%r14)
	movzbl	16(%r14), %eax
	jne	.L117
	cmpw	$29795, 14(%r14)
	jne	.L117
	cmpb	$1, %al
	jne	.L117
	movslq	-100(%rbp), %rax
	movq	%rax, -128(%rbp)
	addq	%rax, %r14
	testl	%r12d, %r12d
	js	.L119
	subl	%eax, %r12d
	cmpl	$31, %r12d
	jg	.L119
	movl	%r12d, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	movl	%r8d, -100(%rbp)
	call	udata_printError_67@PLT
	movl	$8, (%r15)
	movl	-100(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L115:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	subq	$8, %rsp
	movzbl	12(%r14), %edx
	movzbl	13(%r14), %ecx
	movq	%r13, %rdi
	pushq	%rax
	movzbl	14(%r14), %r8d
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rsi
	movzbl	15(%r14), %r9d
	call	udata_printError_67@PLT
	popq	%rax
	movl	$16, (%r15)
	xorl	%r8d, %r8d
	popq	%rdx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	-96(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L120:
	movl	(%r14,%rdx), %esi
	movq	%r13, %rdi
	movq	%rdx, -112(%rbp)
	call	udata_readInt32_67@PLT
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rdi
	movl	%eax, (%rdi,%rdx)
	addq	$4, %rdx
	cmpq	$32, %rdx
	jne	.L120
	movl	-84(%rbp), %r9d
	testl	%r12d, %r12d
	js	.L125
	cmpl	%r9d, %r12d
	jl	.L135
	addq	-128(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L123
	movslq	%r9d, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%r9d, -112(%rbp)
	call	memcpy@PLT
	movl	-112(%rbp), %r9d
.L123:
	movl	%r9d, -112(%rbp)
	movq	%r15, %r8
	movq	%rbx, %rcx
	movl	$32, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*56(%r13)
	movl	-80(%rbp), %eax
	movl	-112(%rbp), %r9d
	andl	$7, %eax
	cmpl	$1, %eax
	je	.L136
	testl	%eax, %eax
	je	.L125
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$16, (%r15)
	xorl	%r8d, %r8d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L136:
	movl	-92(%rbp), %eax
	leaq	32(%rbx), %rcx
	leaq	32(%r14), %rsi
	movq	%r15, %r8
	movq	%r13, %rdi
	leal	-32(%rax), %edx
	call	*48(%r13)
	movl	-112(%rbp), %r9d
	.p2align 4,,10
	.p2align 3
.L125:
	movl	-100(%rbp), %r8d
	addl	%r9d, %r8d
	jmp	.L115
.L135:
	movl	%r12d, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$8, (%r15)
	xorl	%r8d, %r8d
	jmp	.L115
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2412:
	.size	udict_swap_67, .-udict_swap_67
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6717DictionaryMatcherE
	.section	.rodata._ZTSN6icu_6717DictionaryMatcherE,"aG",@progbits,_ZTSN6icu_6717DictionaryMatcherE,comdat
	.align 16
	.type	_ZTSN6icu_6717DictionaryMatcherE, @object
	.size	_ZTSN6icu_6717DictionaryMatcherE, 29
_ZTSN6icu_6717DictionaryMatcherE:
	.string	"N6icu_6717DictionaryMatcherE"
	.weak	_ZTIN6icu_6717DictionaryMatcherE
	.section	.data.rel.ro._ZTIN6icu_6717DictionaryMatcherE,"awG",@progbits,_ZTIN6icu_6717DictionaryMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_6717DictionaryMatcherE, @object
	.size	_ZTIN6icu_6717DictionaryMatcherE, 24
_ZTIN6icu_6717DictionaryMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717DictionaryMatcherE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6723UCharsDictionaryMatcherE
	.section	.rodata._ZTSN6icu_6723UCharsDictionaryMatcherE,"aG",@progbits,_ZTSN6icu_6723UCharsDictionaryMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_6723UCharsDictionaryMatcherE, @object
	.size	_ZTSN6icu_6723UCharsDictionaryMatcherE, 35
_ZTSN6icu_6723UCharsDictionaryMatcherE:
	.string	"N6icu_6723UCharsDictionaryMatcherE"
	.weak	_ZTIN6icu_6723UCharsDictionaryMatcherE
	.section	.data.rel.ro._ZTIN6icu_6723UCharsDictionaryMatcherE,"awG",@progbits,_ZTIN6icu_6723UCharsDictionaryMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_6723UCharsDictionaryMatcherE, @object
	.size	_ZTIN6icu_6723UCharsDictionaryMatcherE, 24
_ZTIN6icu_6723UCharsDictionaryMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6723UCharsDictionaryMatcherE
	.quad	_ZTIN6icu_6717DictionaryMatcherE
	.weak	_ZTSN6icu_6722BytesDictionaryMatcherE
	.section	.rodata._ZTSN6icu_6722BytesDictionaryMatcherE,"aG",@progbits,_ZTSN6icu_6722BytesDictionaryMatcherE,comdat
	.align 32
	.type	_ZTSN6icu_6722BytesDictionaryMatcherE, @object
	.size	_ZTSN6icu_6722BytesDictionaryMatcherE, 34
_ZTSN6icu_6722BytesDictionaryMatcherE:
	.string	"N6icu_6722BytesDictionaryMatcherE"
	.weak	_ZTIN6icu_6722BytesDictionaryMatcherE
	.section	.data.rel.ro._ZTIN6icu_6722BytesDictionaryMatcherE,"awG",@progbits,_ZTIN6icu_6722BytesDictionaryMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_6722BytesDictionaryMatcherE, @object
	.size	_ZTIN6icu_6722BytesDictionaryMatcherE, 24
_ZTIN6icu_6722BytesDictionaryMatcherE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6722BytesDictionaryMatcherE
	.quad	_ZTIN6icu_6717DictionaryMatcherE
	.weak	_ZTVN6icu_6717DictionaryMatcherE
	.section	.data.rel.ro._ZTVN6icu_6717DictionaryMatcherE,"awG",@progbits,_ZTVN6icu_6717DictionaryMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_6717DictionaryMatcherE, @object
	.size	_ZTVN6icu_6717DictionaryMatcherE, 48
_ZTVN6icu_6717DictionaryMatcherE:
	.quad	0
	.quad	_ZTIN6icu_6717DictionaryMatcherE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6723UCharsDictionaryMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_6723UCharsDictionaryMatcherE,"awG",@progbits,_ZTVN6icu_6723UCharsDictionaryMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_6723UCharsDictionaryMatcherE, @object
	.size	_ZTVN6icu_6723UCharsDictionaryMatcherE, 48
_ZTVN6icu_6723UCharsDictionaryMatcherE:
	.quad	0
	.quad	_ZTIN6icu_6723UCharsDictionaryMatcherE
	.quad	_ZN6icu_6723UCharsDictionaryMatcherD1Ev
	.quad	_ZN6icu_6723UCharsDictionaryMatcherD0Ev
	.quad	_ZNK6icu_6723UCharsDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_
	.quad	_ZNK6icu_6723UCharsDictionaryMatcher7getTypeEv
	.weak	_ZTVN6icu_6722BytesDictionaryMatcherE
	.section	.data.rel.ro.local._ZTVN6icu_6722BytesDictionaryMatcherE,"awG",@progbits,_ZTVN6icu_6722BytesDictionaryMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_6722BytesDictionaryMatcherE, @object
	.size	_ZTVN6icu_6722BytesDictionaryMatcherE, 48
_ZTVN6icu_6722BytesDictionaryMatcherE:
	.quad	0
	.quad	_ZTIN6icu_6722BytesDictionaryMatcherE
	.quad	_ZN6icu_6722BytesDictionaryMatcherD1Ev
	.quad	_ZN6icu_6722BytesDictionaryMatcherD0Ev
	.quad	_ZNK6icu_6722BytesDictionaryMatcher7matchesEP5UTextiiPiS3_S3_S3_
	.quad	_ZNK6icu_6722BytesDictionaryMatcher7getTypeEv
	.globl	_ZN6icu_6714DictionaryData21TRANSFORM_OFFSET_MASKE
	.section	.rodata
	.align 4
	.type	_ZN6icu_6714DictionaryData21TRANSFORM_OFFSET_MASKE, @object
	.size	_ZN6icu_6714DictionaryData21TRANSFORM_OFFSET_MASKE, 4
_ZN6icu_6714DictionaryData21TRANSFORM_OFFSET_MASKE:
	.long	2097151
	.globl	_ZN6icu_6714DictionaryData19TRANSFORM_TYPE_MASKE
	.align 4
	.type	_ZN6icu_6714DictionaryData19TRANSFORM_TYPE_MASKE, @object
	.size	_ZN6icu_6714DictionaryData19TRANSFORM_TYPE_MASKE, 4
_ZN6icu_6714DictionaryData19TRANSFORM_TYPE_MASKE:
	.long	2130706432
	.globl	_ZN6icu_6714DictionaryData21TRANSFORM_TYPE_OFFSETE
	.align 4
	.type	_ZN6icu_6714DictionaryData21TRANSFORM_TYPE_OFFSETE, @object
	.size	_ZN6icu_6714DictionaryData21TRANSFORM_TYPE_OFFSETE, 4
_ZN6icu_6714DictionaryData21TRANSFORM_TYPE_OFFSETE:
	.long	16777216
	.globl	_ZN6icu_6714DictionaryData14TRANSFORM_NONEE
	.align 4
	.type	_ZN6icu_6714DictionaryData14TRANSFORM_NONEE, @object
	.size	_ZN6icu_6714DictionaryData14TRANSFORM_NONEE, 4
_ZN6icu_6714DictionaryData14TRANSFORM_NONEE:
	.zero	4
	.globl	_ZN6icu_6714DictionaryData15TRIE_HAS_VALUESE
	.align 4
	.type	_ZN6icu_6714DictionaryData15TRIE_HAS_VALUESE, @object
	.size	_ZN6icu_6714DictionaryData15TRIE_HAS_VALUESE, 4
_ZN6icu_6714DictionaryData15TRIE_HAS_VALUESE:
	.long	8
	.globl	_ZN6icu_6714DictionaryData14TRIE_TYPE_MASKE
	.align 4
	.type	_ZN6icu_6714DictionaryData14TRIE_TYPE_MASKE, @object
	.size	_ZN6icu_6714DictionaryData14TRIE_TYPE_MASKE, 4
_ZN6icu_6714DictionaryData14TRIE_TYPE_MASKE:
	.long	7
	.globl	_ZN6icu_6714DictionaryData16TRIE_TYPE_UCHARSE
	.align 4
	.type	_ZN6icu_6714DictionaryData16TRIE_TYPE_UCHARSE, @object
	.size	_ZN6icu_6714DictionaryData16TRIE_TYPE_UCHARSE, 4
_ZN6icu_6714DictionaryData16TRIE_TYPE_UCHARSE:
	.long	1
	.globl	_ZN6icu_6714DictionaryData15TRIE_TYPE_BYTESE
	.align 4
	.type	_ZN6icu_6714DictionaryData15TRIE_TYPE_BYTESE, @object
	.size	_ZN6icu_6714DictionaryData15TRIE_TYPE_BYTESE, 4
_ZN6icu_6714DictionaryData15TRIE_TYPE_BYTESE:
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
