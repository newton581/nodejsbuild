	.file	"uset_props.cpp"
	.text
	.p2align 4
	.globl	uset_applyPattern_67
	.type	uset_applyPattern_67, @function
uset_applyPattern_67:
.LFB2334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L5
	movl	(%r8), %eax
	movq	%r8, %rbx
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jg	.L1
	movq	%rdi, %r12
	testq	%rdi, %rdi
	je	.L9
	leaq	-128(%rbp), %r14
	movl	%ecx, -148(%rbp)
	leaq	-144(%rbp), %r15
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movl	-148(%rbp), %ecx
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringERNS_13ParsePositionEjPKNS_11SymbolTableER10UErrorCode@PLT
	movq	%r15, %rdi
	movl	-136(%rbp), %r13d
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L10
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, (%r8)
	jmp	.L1
.L10:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2334:
	.size	uset_applyPattern_67, .-uset_applyPattern_67
	.p2align 4
	.globl	uset_applyIntPropertyValue_67
	.type	uset_applyIntPropertyValue_67, @function
uset_applyIntPropertyValue_67:
.LFB2335:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode@PLT
	.cfi_endproc
.LFE2335:
	.size	uset_applyIntPropertyValue_67, .-uset_applyIntPropertyValue_67
	.p2align 4
	.globl	uset_applyPropertyAlias_67
	.type	uset_applyPropertyAlias_67, @function
uset_applyPropertyAlias_67:
.LFB2336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-128(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-192(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movl	%r8d, -196(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movl	-196(%rbp), %r8d
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	%r8d, %edx
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2336:
	.size	uset_applyPropertyAlias_67, .-uset_applyPropertyAlias_67
	.p2align 4
	.globl	uset_resemblesPattern_67
	.type	uset_resemblesPattern_67, @function
uset_resemblesPattern_67:
.LFB2337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	movl	%esi, %edx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movzwl	-104(%rbp), %edx
	leal	1(%r12), %ecx
	testw	%dx, %dx
	js	.L17
	movswl	%dx, %eax
	sarl	$5, %eax
.L18:
	cmpl	%eax, %ecx
	jge	.L19
	cmpl	%r12d, %eax
	jbe	.L19
	andl	$2, %edx
	leaq	-102(%rbp), %rax
	cmove	-88(%rbp), %rax
	movslq	%r12d, %rdx
	movl	$1, %r14d
	cmpw	$91, (%rax,%rdx,2)
	je	.L21
.L19:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet16resemblesPatternERKNS_13UnicodeStringEi@PLT
	testb	%al, %al
	setne	%r14b
.L21:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$88, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	-100(%rbp), %eax
	jmp	.L18
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2337:
	.size	uset_resemblesPattern_67, .-uset_resemblesPattern_67
	.p2align 4
	.globl	uset_closeOver_67
	.type	uset_closeOver_67, @function
uset_closeOver_67:
.LFB2339:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet9closeOverEi@PLT
	.cfi_endproc
.LFE2339:
	.size	uset_closeOver_67, .-uset_closeOver_67
	.p2align 4
	.globl	uset_openPattern_67
	.type	uset_openPattern_67, @function
uset_openPattern_67:
.LFB2332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	leaq	-120(%rbp), %rdx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	movq	%rdi, -120(%rbp)
	movq	%r13, %rdi
	sete	%sil
	movzbl	%sil, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L29
	movq	%rax, %rdi
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L34
.L30:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L30
.L29:
	movl	$7, (%rbx)
	jmp	.L30
.L35:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2332:
	.size	uset_openPattern_67, .-uset_openPattern_67
	.p2align 4
	.globl	uset_openPatternOptions_67
	.type	uset_openPatternOptions_67, @function
uset_openPatternOptions_67:
.LFB2333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-120(%rbp), %rdx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	movl	%r9d, %ecx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	movq	%rdi, -120(%rbp)
	movq	%r13, %rdi
	sete	%sil
	movzbl	%sil, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L37
	movq	%rax, %rdi
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringEjPKNS_11SymbolTableER10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L42
.L38:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L38
.L37:
	movl	$7, (%rbx)
	jmp	.L38
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2333:
	.size	uset_openPatternOptions_67, .-uset_openPatternOptions_67
	.p2align 4
	.globl	uset_toPattern_67
	.type	uset_toPattern_67, @function
uset_toPattern_67:
.LFB2338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	movsbl	%cl, %edx
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%r12, %rsi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	call	_ZNK6icu_6710UnicodeSet9toPatternERNS_13UnicodeStringEa@PLT
	movl	%r13d, %edx
	leaq	-120(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rbx, -120(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r13d
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$96, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L47:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2338:
	.size	uset_toPattern_67, .-uset_toPattern_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
