	.file	"ustr_titlecase_brkiter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724WholeStringBreakIterator17getDynamicClassIDEv
	.type	_ZNK6icu_6724WholeStringBreakIterator17getDynamicClassIDEv, @function
_ZNK6icu_6724WholeStringBreakIterator17getDynamicClassIDEv:
.LFB3101:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6724WholeStringBreakIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3101:
	.size	_ZNK6icu_6724WholeStringBreakIterator17getDynamicClassIDEv, .-_ZNK6icu_6724WholeStringBreakIterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724WholeStringBreakIteratoreqERKNS_13BreakIteratorE
	.type	_ZNK6icu_6724WholeStringBreakIteratoreqERKNS_13BreakIteratorE, @function
_ZNK6icu_6724WholeStringBreakIteratoreqERKNS_13BreakIteratorE:
.LFB3106:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3106:
	.size	_ZNK6icu_6724WholeStringBreakIteratoreqERKNS_13BreakIteratorE, .-_ZNK6icu_6724WholeStringBreakIteratoreqERKNS_13BreakIteratorE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724WholeStringBreakIterator5cloneEv
	.type	_ZNK6icu_6724WholeStringBreakIterator5cloneEv, @function
_ZNK6icu_6724WholeStringBreakIterator5cloneEv:
.LFB3107:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3107:
	.size	_ZNK6icu_6724WholeStringBreakIterator5cloneEv, .-_ZNK6icu_6724WholeStringBreakIterator5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6724WholeStringBreakIterator8getUTextEP5UTextR10UErrorCode
	.type	_ZNK6icu_6724WholeStringBreakIterator8getUTextEP5UTextR10UErrorCode, @function
_ZNK6icu_6724WholeStringBreakIterator8getUTextEP5UTextR10UErrorCode:
.LFB3109:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L6
	movl	$16, (%rdx)
.L6:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3109:
	.size	_ZNK6icu_6724WholeStringBreakIterator8getUTextEP5UTextR10UErrorCode, .-_ZNK6icu_6724WholeStringBreakIterator8getUTextEP5UTextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIterator7setTextERKNS_13UnicodeStringE
	.type	_ZN6icu_6724WholeStringBreakIterator7setTextERKNS_13UnicodeStringE, @function
_ZN6icu_6724WholeStringBreakIterator7setTextERKNS_13UnicodeStringE:
.LFB3110:
	.cfi_startproc
	endbr64
	movswl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L8
	sarl	$5, %eax
	movl	%eax, 324(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	12(%rsi), %eax
	movl	%eax, 324(%rdi)
	ret
	.cfi_endproc
.LFE3110:
	.size	_ZN6icu_6724WholeStringBreakIterator7setTextERKNS_13UnicodeStringE, .-_ZN6icu_6724WholeStringBreakIterator7setTextERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIterator5firstEv
	.type	_ZN6icu_6724WholeStringBreakIterator5firstEv, @function
_ZN6icu_6724WholeStringBreakIterator5firstEv:
.LFB3113:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3113:
	.size	_ZN6icu_6724WholeStringBreakIterator5firstEv, .-_ZN6icu_6724WholeStringBreakIterator5firstEv
	.globl	_ZNK6icu_6724WholeStringBreakIterator7currentEv
	.set	_ZNK6icu_6724WholeStringBreakIterator7currentEv,_ZN6icu_6724WholeStringBreakIterator5firstEv
	.globl	_ZN6icu_6724WholeStringBreakIterator8previousEv
	.set	_ZN6icu_6724WholeStringBreakIterator8previousEv,_ZN6icu_6724WholeStringBreakIterator5firstEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIterator4lastEv
	.type	_ZN6icu_6724WholeStringBreakIterator4lastEv, @function
_ZN6icu_6724WholeStringBreakIterator4lastEv:
.LFB3114:
	.cfi_startproc
	endbr64
	movl	324(%rdi), %eax
	ret
	.cfi_endproc
.LFE3114:
	.size	_ZN6icu_6724WholeStringBreakIterator4lastEv, .-_ZN6icu_6724WholeStringBreakIterator4lastEv
	.globl	_ZN6icu_6724WholeStringBreakIterator4nextEv
	.set	_ZN6icu_6724WholeStringBreakIterator4nextEv,_ZN6icu_6724WholeStringBreakIterator4lastEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIterator9followingEi
	.type	_ZN6icu_6724WholeStringBreakIterator9followingEi, @function
_ZN6icu_6724WholeStringBreakIterator9followingEi:
.LFB3118:
	.cfi_startproc
	endbr64
	movl	324(%rdi), %eax
	ret
	.cfi_endproc
.LFE3118:
	.size	_ZN6icu_6724WholeStringBreakIterator9followingEi, .-_ZN6icu_6724WholeStringBreakIterator9followingEi
	.globl	_ZN6icu_6724WholeStringBreakIterator4nextEi
	.set	_ZN6icu_6724WholeStringBreakIterator4nextEi,_ZN6icu_6724WholeStringBreakIterator9followingEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIterator9precedingEi
	.type	_ZN6icu_6724WholeStringBreakIterator9precedingEi, @function
_ZN6icu_6724WholeStringBreakIterator9precedingEi:
.LFB3119:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3119:
	.size	_ZN6icu_6724WholeStringBreakIterator9precedingEi, .-_ZN6icu_6724WholeStringBreakIterator9precedingEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIterator10isBoundaryEi
	.type	_ZN6icu_6724WholeStringBreakIterator10isBoundaryEi, @function
_ZN6icu_6724WholeStringBreakIterator10isBoundaryEi:
.LFB3120:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3120:
	.size	_ZN6icu_6724WholeStringBreakIterator10isBoundaryEi, .-_ZN6icu_6724WholeStringBreakIterator10isBoundaryEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIterator17createBufferCloneEPvRiR10UErrorCode
	.type	_ZN6icu_6724WholeStringBreakIterator17createBufferCloneEPvRiR10UErrorCode, @function
_ZN6icu_6724WholeStringBreakIterator17createBufferCloneEPvRiR10UErrorCode:
.LFB3122:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L16
	movl	$16, (%rcx)
.L16:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3122:
	.size	_ZN6icu_6724WholeStringBreakIterator17createBufferCloneEPvRiR10UErrorCode, .-_ZN6icu_6724WholeStringBreakIterator17createBufferCloneEPvRiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIterator16refreshInputTextEP5UTextR10UErrorCode
	.type	_ZN6icu_6724WholeStringBreakIterator16refreshInputTextEP5UTextR10UErrorCode, @function
_ZN6icu_6724WholeStringBreakIterator16refreshInputTextEP5UTextR10UErrorCode:
.LFB3123:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	movq	%rdi, %rax
	testl	%ecx, %ecx
	jle	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$16, (%rdx)
	ret
	.cfi_endproc
.LFE3123:
	.size	_ZN6icu_6724WholeStringBreakIterator16refreshInputTextEP5UTextR10UErrorCode, .-_ZN6icu_6724WholeStringBreakIterator16refreshInputTextEP5UTextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIteratorD2Ev
	.type	_ZN6icu_6724WholeStringBreakIteratorD2Ev, @function
_ZN6icu_6724WholeStringBreakIteratorD2Ev:
.LFB3103:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6724WholeStringBreakIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6713BreakIteratorD2Ev@PLT
	.cfi_endproc
.LFE3103:
	.size	_ZN6icu_6724WholeStringBreakIteratorD2Ev, .-_ZN6icu_6724WholeStringBreakIteratorD2Ev
	.globl	_ZN6icu_6724WholeStringBreakIteratorD1Ev
	.set	_ZN6icu_6724WholeStringBreakIteratorD1Ev,_ZN6icu_6724WholeStringBreakIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIteratorD0Ev
	.type	_ZN6icu_6724WholeStringBreakIteratorD0Ev, @function
_ZN6icu_6724WholeStringBreakIteratorD0Ev:
.LFB3105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6724WholeStringBreakIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6713BreakIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3105:
	.size	_ZN6icu_6724WholeStringBreakIteratorD0Ev, .-_ZN6icu_6724WholeStringBreakIteratorD0Ev
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.globl	_ZNK6icu_6724WholeStringBreakIterator7getTextEv
	.type	_ZNK6icu_6724WholeStringBreakIterator7getTextEv, @function
_ZNK6icu_6724WholeStringBreakIterator7getTextEv:
.LFB3108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE3108:
	.size	_ZNK6icu_6724WholeStringBreakIterator7getTextEv, .-_ZNK6icu_6724WholeStringBreakIterator7getTextEv
	.align 2
	.globl	_ZN6icu_6724WholeStringBreakIterator9adoptTextEPNS_17CharacterIteratorE
	.type	_ZN6icu_6724WholeStringBreakIterator9adoptTextEPNS_17CharacterIteratorE, @function
_ZN6icu_6724WholeStringBreakIterator9adoptTextEPNS_17CharacterIteratorE:
.LFB3112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	abort@PLT
	.cfi_endproc
.LFE3112:
	.size	_ZN6icu_6724WholeStringBreakIterator9adoptTextEPNS_17CharacterIteratorE, .-_ZN6icu_6724WholeStringBreakIterator9adoptTextEPNS_17CharacterIteratorE
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIterator7setTextEP5UTextR10UErrorCode
	.type	_ZN6icu_6724WholeStringBreakIterator7setTextEP5UTextR10UErrorCode, @function
_ZN6icu_6724WholeStringBreakIterator7setTextEP5UTextR10UErrorCode:
.LFB3111:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L34
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	utext_nativeLength_67@PLT
	cmpq	$2147483647, %rax
	jle	.L35
	movl	$8, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	%eax, 324(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3111:
	.size	_ZN6icu_6724WholeStringBreakIterator7setTextEP5UTextR10UErrorCode, .-_ZN6icu_6724WholeStringBreakIterator7setTextEP5UTextR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6724WholeStringBreakIterator16getStaticClassIDEv
	.type	_ZN6icu_6724WholeStringBreakIterator16getStaticClassIDEv, @function
_ZN6icu_6724WholeStringBreakIterator16getStaticClassIDEv:
.LFB3100:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6724WholeStringBreakIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3100:
	.size	_ZN6icu_6724WholeStringBreakIterator16getStaticClassIDEv, .-_ZN6icu_6724WholeStringBreakIterator16getStaticClassIDEv
	.p2align 4
	.globl	ustrcase_getTitleBreakIterator_67
	.type	ustrcase_getTitleBreakIterator_67, @function
ustrcase_getTitleBreakIterator_67:
.LFB3124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$256, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L37
	movq	%r8, %rbx
	andl	$224, %edx
	je	.L39
	testq	%rcx, %rcx
	jne	.L64
.L39:
	movq	%rcx, %r13
	testq	%rcx, %rcx
	je	.L65
.L37:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L66
	addq	$256, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	cmpl	$32, %edx
	je	.L40
	cmpl	$64, %edx
	je	.L41
	testl	%edx, %edx
	je	.L67
	movl	$1, (%r9)
.L45:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L49
	movq	(%r12), %rax
	leaq	_ZN6icu_6724WholeStringBreakIteratorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L50
	leaq	16+_ZTVN6icu_6724WholeStringBreakIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6713BreakIteratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L49:
	movq	%r13, (%rbx)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$1, (%r9)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$328, %edi
	movq	%r9, -280(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-280(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L46
	movq	%rax, %rdi
	call	_ZN6icu_6713BreakIteratorC2Ev@PLT
	leaq	16+_ZTVN6icu_6724WholeStringBreakIteratorE(%rip), %rax
	movl	$0, 324(%r13)
	movq	%rax, 0(%r13)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r9, -280(%rbp)
	leaq	-272(%rbp), %r14
	testq	%rdi, %rdi
	je	.L47
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	-280(%rbp), %r9
.L48:
	movq	%r14, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r9, -280(%rbp)
	leaq	-272(%rbp), %r14
	testq	%rdi, %rdi
	je	.L43
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movq	-280(%rbp), %r9
.L44:
	movq	%r14, %rdi
	movq	%r9, %rsi
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L47:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-280(%rbp), %r9
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	-280(%rbp), %r9
	jmp	.L44
.L66:
	call	__stack_chk_fail@PLT
.L46:
	movl	$7, (%r9)
	jmp	.L45
	.cfi_endproc
.LFE3124:
	.size	ustrcase_getTitleBreakIterator_67, .-ustrcase_getTitleBreakIterator_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_677CaseMap7toTitleEPKcjPNS_13BreakIteratorEPKDsiPDsiPNS_5EditsER10UErrorCode
	.type	_ZN6icu_677CaseMap7toTitleEPKcjPNS_13BreakIteratorEPKDsiPDsiPNS_5EditsER10UErrorCode, @function
_ZN6icu_677CaseMap7toTitleEPKcjPNS_13BreakIteratorEPKDsiPDsiPNS_5EditsER10UErrorCode:
.LFB3125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movl	%esi, -316(%rbp)
	movq	%rcx, -312(%rbp)
	movq	32(%rbp), %r14
	movq	%r9, -328(%rbp)
	movq	%rax, -336(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L70
	movl	%esi, %eax
	movq	%rdi, %r10
	movq	%rdx, %r12
	movl	%r8d, %ebx
	andl	$224, %eax
	je	.L71
	testq	%rdx, %rdx
	jne	.L75
.L71:
	xorl	%r13d, %r13d
	leaq	-288(%rbp), %r15
	testq	%r12, %r12
	je	.L100
.L72:
	movq	-312(%rbp), %rax
	movl	%ebx, %esi
	movl	%ebx, %ecx
	movq	%r15, %rdi
	leaq	-296(%rbp), %rdx
	shrl	$31, %esi
	movq	%r10, -344(%rbp)
	movq	%rax, -296(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	(%r12), %rax
	leaq	_ZN6icu_6724WholeStringBreakIterator7setTextERKNS_13UnicodeStringE(%rip), %rdx
	movq	-344(%rbp), %r10
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L79
	movswl	-280(%rbp), %eax
	testw	%ax, %ax
	js	.L80
	sarl	$5, %eax
.L81:
	movl	%eax, 324(%r12)
.L82:
	movq	%r10, %rdi
	call	ustrcase_getCaseLocale_67@PLT
	pushq	%r14
	movl	16(%rbp), %r8d
	movq	%r12, %rdx
	pushq	-336(%rbp)
	movq	-312(%rbp), %r9
	movl	%eax, %edi
	movq	-328(%rbp), %rcx
	pushq	ustrcase_internalToTitle_67@GOTPCREL(%rip)
	movl	-316(%rbp), %esi
	pushq	%rbx
	call	ustrcase_map_67@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r13, %r13
	je	.L68
	movq	0(%r13), %rax
	leaq	_ZN6icu_6724WholeStringBreakIteratorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L84
	leaq	16+_ZTVN6icu_6724WholeStringBreakIteratorE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 0(%r13)
	call	_ZN6icu_6713BreakIteratorD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L68:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movl	-276(%rbp), %eax
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L100:
	cmpl	$32, %eax
	je	.L73
	cmpl	$64, %eax
	je	.L74
	testl	%eax, %eax
	je	.L102
.L75:
	movl	$1, (%r14)
.L70:
	xorl	%r12d, %r12d
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r10, -344(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	*%rax
	movq	-344(%rbp), %r10
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$328, %edi
	movq	%r10, -344(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L77
	movq	%rax, %rdi
	movq	%r12, %r13
	call	_ZN6icu_6713BreakIteratorC2Ev@PLT
	leaq	16+_ZTVN6icu_6724WholeStringBreakIteratorE(%rip), %rax
	movl	$0, 324(%r12)
	movq	-344(%rbp), %r10
	movq	%rax, (%r12)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%r10, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r10, -344(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-344(%rbp), %r10
.L76:
	testq	%r12, %r12
	je	.L70
	movq	%r12, %r13
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%r10, %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r10, -344(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-344(%rbp), %r10
	jmp	.L76
.L101:
	call	__stack_chk_fail@PLT
.L77:
	movl	$7, (%r14)
	jmp	.L70
	.cfi_endproc
.LFE3125:
	.size	_ZN6icu_677CaseMap7toTitleEPKcjPNS_13BreakIteratorEPKDsiPDsiPNS_5EditsER10UErrorCode, .-_ZN6icu_677CaseMap7toTitleEPKcjPNS_13BreakIteratorEPKDsiPDsiPNS_5EditsER10UErrorCode
	.p2align 4
	.globl	u_strToTitle_67
	.type	u_strToTitle_67, @function
u_strToTitle_67:
.LFB3126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -320(%rbp)
	movq	16(%rbp), %r15
	movl	%esi, -324(%rbp)
	movq	%rdx, -312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L104
	movl	%ecx, %ebx
	movq	%r8, %r12
	leaq	-288(%rbp), %r13
	xorl	%r14d, %r14d
	testq	%r8, %r8
	je	.L121
.L105:
	movq	-312(%rbp), %rax
	movl	%ebx, %esi
	movl	%ebx, %ecx
	movq	%r13, %rdi
	leaq	-296(%rbp), %rdx
	shrl	$31, %esi
	movq	%r9, -336(%rbp)
	movq	%rax, -296(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	(%r12), %rax
	leaq	_ZN6icu_6724WholeStringBreakIterator7setTextERKNS_13UnicodeStringE(%rip), %rdx
	movq	-336(%rbp), %r9
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L106
	movswl	-280(%rbp), %eax
	testw	%ax, %ax
	js	.L107
	sarl	$5, %eax
.L108:
	movl	%eax, 324(%r12)
.L109:
	movq	%r9, %rdi
	call	ustrcase_getCaseLocale_67@PLT
	subq	$8, %rsp
	movq	%r12, %rdx
	xorl	%esi, %esi
	pushq	%r15
	movq	-312(%rbp), %r9
	movl	%eax, %edi
	pushq	ustrcase_internalToTitle_67@GOTPCREL(%rip)
	movl	-324(%rbp), %r8d
	movq	-320(%rbp), %rcx
	pushq	%rbx
	call	ustrcase_mapWithOverlap_67@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r14, %r14
	je	.L103
	movq	(%r14), %rax
	leaq	_ZN6icu_6724WholeStringBreakIteratorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L111
	leaq	16+_ZTVN6icu_6724WholeStringBreakIteratorE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, (%r14)
	call	_ZN6icu_6713BreakIteratorD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L103:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	movl	-276(%rbp), %eax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%r12d, %r12d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r13, %rdi
	movq	%r9, -336(%rbp)
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN6icu_676LocaleD1Ev@PLT
	testq	%r12, %r12
	je	.L104
	movq	-336(%rbp), %r9
	movq	%r12, %r14
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r9, -336(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	movq	-336(%rbp), %r9
	jmp	.L109
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3126:
	.size	u_strToTitle_67, .-u_strToTitle_67
	.p2align 4
	.globl	ucasemap_toTitle_67
	.type	ucasemap_toTitle_67, @function
ucasemap_toTitle_67:
.LFB3127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L123
	cmpq	$0, (%rdi)
	movl	%r8d, %r13d
	movq	%rdi, %rbx
	movq	%rsi, %r11
	movl	%edx, %r10d
	movq	%rcx, %r14
	leaq	-136(%rbp), %r8
	movq	%r9, %r12
	je	.L140
.L125:
	movl	%r13d, %esi
	leaq	-128(%rbp), %r15
	movl	%r13d, %ecx
	movq	%r8, %rdx
	shrl	$31, %esi
	movq	%r15, %rdi
	movl	%r10d, -160(%rbp)
	movq	%r11, -152(%rbp)
	movq	%r14, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	(%rbx), %rdx
	leaq	_ZN6icu_6724WholeStringBreakIterator7setTextERKNS_13UnicodeStringE(%rip), %rcx
	movq	-152(%rbp), %r11
	movl	-160(%rbp), %r10d
	movq	(%rdx), %rax
	movq	56(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L129
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L130
	sarl	$5, %eax
.L131:
	movl	%eax, 324(%rdx)
.L132:
	movl	44(%rbx), %esi
	movl	40(%rbx), %edi
	pushq	%r12
	movq	%r14, %r9
	pushq	$0
	movl	%r10d, %r8d
	movq	%r11, %rcx
	pushq	ustrcase_internalToTitle_67@GOTPCREL(%rip)
	pushq	%r13
	call	ustrcase_map_67@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movl	%eax, -152(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-152(%rbp), %eax
.L123:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L141
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movl	%edx, -164(%rbp)
	movl	44(%rdi), %edx
	xorl	%ecx, %ecx
	movq	%rsi, -160(%rbp)
	leaq	8(%rdi), %rsi
	xorl	%edi, %edi
	movq	%r8, -152(%rbp)
	movq	$0, -136(%rbp)
	call	ustrcase_getTitleBreakIterator_67
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %r11
	testq	%rax, %rax
	movl	-164(%rbp), %r10d
	je	.L142
	movq	-136(%rbp), %rax
	movq	%rax, (%rbx)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L129:
	movl	%r10d, -160(%rbp)
	movq	%rdx, %rdi
	movq	%r15, %rsi
	movq	%r11, -152(%rbp)
	call	*%rax
	movq	(%rbx), %rdx
	movl	-160(%rbp), %r10d
	movq	-152(%rbp), %r11
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L130:
	movl	-116(%rbp), %eax
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L142:
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L127
	movq	(%r12), %rax
	leaq	_ZN6icu_6724WholeStringBreakIteratorD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L128
	leaq	16+_ZTVN6icu_6724WholeStringBreakIteratorE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_6713BreakIteratorD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L127:
	xorl	%eax, %eax
	jmp	.L123
.L128:
	movq	%r12, %rdi
	call	*%rax
	xorl	%eax, %eax
	jmp	.L123
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3127:
	.size	ucasemap_toTitle_67, .-ucasemap_toTitle_67
	.weak	_ZTSN6icu_6724WholeStringBreakIteratorE
	.section	.rodata._ZTSN6icu_6724WholeStringBreakIteratorE,"aG",@progbits,_ZTSN6icu_6724WholeStringBreakIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6724WholeStringBreakIteratorE, @object
	.size	_ZTSN6icu_6724WholeStringBreakIteratorE, 36
_ZTSN6icu_6724WholeStringBreakIteratorE:
	.string	"N6icu_6724WholeStringBreakIteratorE"
	.weak	_ZTIN6icu_6724WholeStringBreakIteratorE
	.section	.data.rel.ro._ZTIN6icu_6724WholeStringBreakIteratorE,"awG",@progbits,_ZTIN6icu_6724WholeStringBreakIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6724WholeStringBreakIteratorE, @object
	.size	_ZTIN6icu_6724WholeStringBreakIteratorE, 24
_ZTIN6icu_6724WholeStringBreakIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6724WholeStringBreakIteratorE
	.quad	_ZTIN6icu_6713BreakIteratorE
	.weak	_ZTVN6icu_6724WholeStringBreakIteratorE
	.section	.data.rel.ro._ZTVN6icu_6724WholeStringBreakIteratorE,"awG",@progbits,_ZTVN6icu_6724WholeStringBreakIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6724WholeStringBreakIteratorE, @object
	.size	_ZTVN6icu_6724WholeStringBreakIteratorE, 200
_ZTVN6icu_6724WholeStringBreakIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6724WholeStringBreakIteratorE
	.quad	_ZN6icu_6724WholeStringBreakIteratorD1Ev
	.quad	_ZN6icu_6724WholeStringBreakIteratorD0Ev
	.quad	_ZNK6icu_6724WholeStringBreakIterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6724WholeStringBreakIteratoreqERKNS_13BreakIteratorE
	.quad	_ZNK6icu_6724WholeStringBreakIterator5cloneEv
	.quad	_ZNK6icu_6724WholeStringBreakIterator7getTextEv
	.quad	_ZNK6icu_6724WholeStringBreakIterator8getUTextEP5UTextR10UErrorCode
	.quad	_ZN6icu_6724WholeStringBreakIterator7setTextERKNS_13UnicodeStringE
	.quad	_ZN6icu_6724WholeStringBreakIterator7setTextEP5UTextR10UErrorCode
	.quad	_ZN6icu_6724WholeStringBreakIterator9adoptTextEPNS_17CharacterIteratorE
	.quad	_ZN6icu_6724WholeStringBreakIterator5firstEv
	.quad	_ZN6icu_6724WholeStringBreakIterator4lastEv
	.quad	_ZN6icu_6724WholeStringBreakIterator8previousEv
	.quad	_ZN6icu_6724WholeStringBreakIterator4nextEv
	.quad	_ZNK6icu_6724WholeStringBreakIterator7currentEv
	.quad	_ZN6icu_6724WholeStringBreakIterator9followingEi
	.quad	_ZN6icu_6724WholeStringBreakIterator9precedingEi
	.quad	_ZN6icu_6724WholeStringBreakIterator10isBoundaryEi
	.quad	_ZN6icu_6724WholeStringBreakIterator4nextEi
	.quad	_ZNK6icu_6713BreakIterator13getRuleStatusEv
	.quad	_ZN6icu_6713BreakIterator16getRuleStatusVecEPiiR10UErrorCode
	.quad	_ZN6icu_6724WholeStringBreakIterator17createBufferCloneEPvRiR10UErrorCode
	.quad	_ZN6icu_6724WholeStringBreakIterator16refreshInputTextEP5UTextR10UErrorCode
	.local	_ZZN6icu_6724WholeStringBreakIterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6724WholeStringBreakIterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
