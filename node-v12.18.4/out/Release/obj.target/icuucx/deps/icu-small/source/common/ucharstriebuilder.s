	.file	"ucharstriebuilder.cpp"
	.text
	.section	.text._ZNK6icu_6717UCharsTrieBuilder23matchNodesCanHaveValuesEv,"axG",@progbits,_ZNK6icu_6717UCharsTrieBuilder23matchNodesCanHaveValuesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6717UCharsTrieBuilder23matchNodesCanHaveValuesEv
	.type	_ZNK6icu_6717UCharsTrieBuilder23matchNodesCanHaveValuesEv, @function
_ZNK6icu_6717UCharsTrieBuilder23matchNodesCanHaveValuesEv:
.LFB1380:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE1380:
	.size	_ZNK6icu_6717UCharsTrieBuilder23matchNodesCanHaveValuesEv, .-_ZNK6icu_6717UCharsTrieBuilder23matchNodesCanHaveValuesEv
	.section	.text._ZNK6icu_6717UCharsTrieBuilder31getMaxBranchLinearSubNodeLengthEv,"axG",@progbits,_ZNK6icu_6717UCharsTrieBuilder31getMaxBranchLinearSubNodeLengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6717UCharsTrieBuilder31getMaxBranchLinearSubNodeLengthEv
	.type	_ZNK6icu_6717UCharsTrieBuilder31getMaxBranchLinearSubNodeLengthEv, @function
_ZNK6icu_6717UCharsTrieBuilder31getMaxBranchLinearSubNodeLengthEv:
.LFB1381:
	.cfi_startproc
	endbr64
	movl	$5, %eax
	ret
	.cfi_endproc
.LFE1381:
	.size	_ZNK6icu_6717UCharsTrieBuilder31getMaxBranchLinearSubNodeLengthEv, .-_ZNK6icu_6717UCharsTrieBuilder31getMaxBranchLinearSubNodeLengthEv
	.section	.text._ZNK6icu_6717UCharsTrieBuilder17getMinLinearMatchEv,"axG",@progbits,_ZNK6icu_6717UCharsTrieBuilder17getMinLinearMatchEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6717UCharsTrieBuilder17getMinLinearMatchEv
	.type	_ZNK6icu_6717UCharsTrieBuilder17getMinLinearMatchEv, @function
_ZNK6icu_6717UCharsTrieBuilder17getMinLinearMatchEv:
.LFB1382:
	.cfi_startproc
	endbr64
	movl	$48, %eax
	ret
	.cfi_endproc
.LFE1382:
	.size	_ZNK6icu_6717UCharsTrieBuilder17getMinLinearMatchEv, .-_ZNK6icu_6717UCharsTrieBuilder17getMinLinearMatchEv
	.section	.text._ZNK6icu_6717UCharsTrieBuilder23getMaxLinearMatchLengthEv,"axG",@progbits,_ZNK6icu_6717UCharsTrieBuilder23getMaxLinearMatchLengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6717UCharsTrieBuilder23getMaxLinearMatchLengthEv
	.type	_ZNK6icu_6717UCharsTrieBuilder23getMaxLinearMatchLengthEv, @function
_ZNK6icu_6717UCharsTrieBuilder23getMaxLinearMatchLengthEv:
.LFB1383:
	.cfi_startproc
	endbr64
	movl	$16, %eax
	ret
	.cfi_endproc
.LFE1383:
	.size	_ZNK6icu_6717UCharsTrieBuilder23getMaxLinearMatchLengthEv, .-_ZNK6icu_6717UCharsTrieBuilder23getMaxLinearMatchLengthEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717UCharsTrieBuilder22getElementStringLengthEi
	.type	_ZNK6icu_6717UCharsTrieBuilder22getElementStringLengthEi, @function
_ZNK6icu_6717UCharsTrieBuilder22getElementStringLengthEi:
.LFB2403:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	movzwl	24(%rdi), %edx
	movslq	%esi, %rsi
	movslq	(%rax,%rsi,8), %rax
	testw	%dx, %dx
	js	.L7
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L8:
	movl	$65535, %r8d
	cmpl	%eax, %ecx
	jbe	.L6
	andl	$2, %edx
	jne	.L13
	movq	40(%rdi), %rdi
.L11:
	movzwl	(%rdi,%rax,2), %r8d
.L6:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$26, %rdi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L7:
	movl	28(%rdi), %ecx
	jmp	.L8
	.cfi_endproc
.LFE2403:
	.size	_ZNK6icu_6717UCharsTrieBuilder22getElementStringLengthEi, .-_ZNK6icu_6717UCharsTrieBuilder22getElementStringLengthEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717UCharsTrieBuilder14getElementUnitEii
	.type	_ZNK6icu_6717UCharsTrieBuilder14getElementUnitEii, @function
_ZNK6icu_6717UCharsTrieBuilder14getElementUnitEii:
.LFB2404:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,8), %eax
	leal	1(%rax,%rdx), %eax
	movzwl	24(%rdi), %edx
	testw	%dx, %dx
	js	.L15
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L16:
	movl	$-1, %r8d
	cmpl	%eax, %ecx
	jbe	.L14
	andl	$2, %edx
	jne	.L21
	movq	40(%rdi), %rdi
.L19:
	cltq
	movzwl	(%rdi,%rax,2), %r8d
.L14:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	addq	$26, %rdi
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L15:
	movl	28(%rdi), %ecx
	jmp	.L16
	.cfi_endproc
.LFE2404:
	.size	_ZNK6icu_6717UCharsTrieBuilder14getElementUnitEii, .-_ZNK6icu_6717UCharsTrieBuilder14getElementUnitEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717UCharsTrieBuilder15getElementValueEi
	.type	_ZNK6icu_6717UCharsTrieBuilder15getElementValueEi, @function
_ZNK6icu_6717UCharsTrieBuilder15getElementValueEi:
.LFB2405:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	movslq	%esi, %rsi
	movl	4(%rax,%rsi,8), %eax
	ret
	.cfi_endproc
.LFE2405:
	.size	_ZNK6icu_6717UCharsTrieBuilder15getElementValueEi, .-_ZNK6icu_6717UCharsTrieBuilder15getElementValueEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717UCharsTrieBuilder21getLimitOfLinearMatchEiii
	.type	_ZNK6icu_6717UCharsTrieBuilder21getLimitOfLinearMatchEiii, @function
_ZNK6icu_6717UCharsTrieBuilder21getLimitOfLinearMatchEiii:
.LFB2406:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	leaq	(%rax,%rdx,8), %r9
	movzwl	24(%rdi), %edx
	movl	(%rax,%rsi,8), %r10d
	testw	%dx, %dx
	js	.L24
	movswl	%dx, %esi
	sarl	$5, %esi
.L25:
	movl	$65535, %r8d
	cmpl	%r10d, %esi
	jbe	.L26
	leaq	26(%rdi), %r8
	testb	$2, %dl
	je	.L49
.L28:
	movslq	%r10d, %rax
	movzwl	(%r8,%rax,2), %r8d
.L26:
	leal	1(%rcx), %eax
	cmpl	%eax, %r8d
	jle	.L46
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movslq	(%r9), %rcx
	leal	1(%rcx), %r9d
	jne	.L50
	movslq	%r10d, %r12
	movslq	%r9d, %rdx
	movslq	%eax, %r11
	addl	$1, %r10d
	addq	%r11, %rdx
	subq	%rcx, %r12
	addq	%rdx, %rdx
	addq	%r12, %r12
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L32:
	movq	40(%rdi), %rcx
	leal	(%r9,%rax), %ebx
	leaq	(%rcx,%rdx), %r11
	movzwl	(%r11,%r12), %r11d
	cmpl	%ebx, %esi
	jbe	.L51
.L34:
	movzwl	(%rcx,%rdx), %ecx
.L35:
	cmpw	%cx, %r11w
	jne	.L23
.L33:
	addl	$1, %eax
	addq	$2, %rdx
	cmpl	%eax, %r8d
	je	.L23
.L36:
	leal	(%r10,%rax), %ecx
	cmpl	%ecx, %esi
	ja	.L32
	leal	(%r9,%rax), %ecx
	cmpl	%ecx, %esi
	jbe	.L33
	movq	40(%rdi), %rcx
	movl	$-1, %r11d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L50:
	movslq	%eax, %rdx
	movslq	%r10d, %rax
	leaq	(%rdi,%rcx,2), %r13
	addl	$1, %r10d
	leaq	(%rdi,%rax,2), %r12
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L52:
	leal	(%r9,%rdx), %ebx
	movzwl	28(%r12,%rdx,2), %edi
	movl	$-1, %ecx
	cmpl	%ebx, %esi
	jbe	.L38
.L40:
	movzwl	28(%r13,%rdx,2), %ecx
.L38:
	cmpw	%di, %cx
	jne	.L23
	addq	$1, %rdx
	leal	1(%r11), %eax
	cmpl	%edx, %r8d
	jle	.L23
.L31:
	leal	(%r10,%rdx), %ecx
	movl	%edx, %r11d
	movl	%edx, %eax
	cmpl	%ecx, %esi
	ja	.L52
	leal	(%r9,%rdx), %ecx
	movl	$-1, %edi
	cmpl	%ecx, %esi
	ja	.L40
	addq	$1, %rdx
	leal	1(%r11), %eax
	cmpl	%edx, %r8d
	jg	.L31
.L23:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movq	40(%rdi), %r8
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$-1, %ecx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	28(%rdi), %esi
	jmp	.L25
.L46:
	ret
	.cfi_endproc
.LFE2406:
	.size	_ZNK6icu_6717UCharsTrieBuilder21getLimitOfLinearMatchEiii, .-_ZNK6icu_6717UCharsTrieBuilder21getLimitOfLinearMatchEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717UCharsTrieBuilder17countElementUnitsEiii
	.type	_ZNK6icu_6717UCharsTrieBuilder17countElementUnitsEiii, @function
_ZNK6icu_6717UCharsTrieBuilder17countElementUnitsEiii:
.LFB2407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movswl	24(%rbx), %eax
	movq	80(%rdi), %rdi
	movl	%eax, %r14d
	sarl	$5, %eax
	movl	%eax, -44(%rbp)
	movslq	%esi, %rax
	movl	%r14d, %r12d
	movl	(%rdi,%rax,8), %eax
	andl	$2, %r12d
	leal	1(%rax), %r9d
	leaq	26(%rbx), %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L65:
	movl	-44(%rbp), %r10d
	addl	$1, %esi
	addl	%ecx, %r9d
	testw	%r14w, %r14w
	jns	.L54
	movl	28(%rbx), %r10d
.L54:
	movl	$-1, %r11d
	cmpl	%r9d, %r10d
	jbe	.L55
	testw	%r12w, %r12w
	je	.L56
	movq	-56(%rbp), %rax
.L57:
	movslq	%r9d, %r9
	movzwl	(%rax,%r9,2), %r11d
.L55:
	cmpl	%edx, %esi
	jge	.L58
	testw	%r12w, %r12w
	jne	.L59
	movslq	%esi, %r8
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L77:
	addq	$1, %r8
	cmpl	%r8d, %edx
	jle	.L58
.L62:
	movl	(%rdi,%r8,8), %r9d
	movl	%r8d, %esi
	movl	$-1, %r15d
	addl	$1, %r9d
	leal	(%r9,%rcx), %eax
	cmpl	%eax, %r10d
	jbe	.L60
	movq	40(%rbx), %r15
	cltq
	movzwl	(%r15,%rax,2), %r15d
.L60:
	cmpw	%r15w, %r11w
	je	.L77
.L61:
	addl	$1, %r13d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L59:
	movslq	%esi, %rax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L78:
	addq	$1, %rax
	cmpl	%eax, %edx
	jle	.L58
.L64:
	movl	(%rdi,%rax,8), %r9d
	movl	%eax, %esi
	movl	$-1, %r15d
	addl	$1, %r9d
	leal	(%r9,%rcx), %r8d
	cmpl	%r8d, %r10d
	jbe	.L63
	movslq	%r8d, %r8
	movzwl	26(%rbx,%r8,2), %r15d
.L63:
	cmpw	%r15w, %r11w
	je	.L78
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L56:
	movq	40(%rbx), %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L58:
	leal	1(%r13), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2407:
	.size	_ZNK6icu_6717UCharsTrieBuilder17countElementUnitsEiii, .-_ZNK6icu_6717UCharsTrieBuilder17countElementUnitsEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717UCharsTrieBuilder23skipElementsBySomeUnitsEiii
	.type	_ZNK6icu_6717UCharsTrieBuilder23skipElementsBySomeUnitsEiii, @function
_ZNK6icu_6717UCharsTrieBuilder23skipElementsBySomeUnitsEiii:
.LFB2408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	movq	%rax, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	26(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	movswl	24(%rdi), %r14d
	movq	80(%rdi), %rcx
	movl	%r14d, %r13d
	movl	(%rcx,%rax,8), %eax
	sarl	$5, %r14d
	movl	%r13d, %r12d
	andl	$2, %r12d
	leal	1(%rax), %r8d
	.p2align 4,,10
	.p2align 3
.L92:
	leal	1(%r9), %eax
	addl	%edx, %r8d
	movl	%r14d, %r10d
	testw	%r13w, %r13w
	jns	.L80
	movl	28(%rdi), %r10d
.L80:
	movl	$-1, %r11d
	cmpl	%r8d, %r10d
	jbe	.L81
	movq	%r15, %rsi
	testw	%r12w, %r12w
	jne	.L83
	movq	40(%rdi), %rsi
.L83:
	movslq	%r8d, %r8
	movzwl	(%rsi,%r8,2), %r11d
.L81:
	cltq
	testw	%r12w, %r12w
	jne	.L91
	movl	%ebx, -44(%rbp)
	.p2align 4,,10
	.p2align 3
.L88:
	movl	(%rcx,%rax,8), %r8d
	movl	%eax, %r9d
	addq	$1, %rax
	addl	$1, %r8d
	leal	(%r8,%rdx), %esi
	cmpl	%esi, %r10d
	jbe	.L85
	movq	40(%rdi), %rbx
	movslq	%esi, %rsi
	cmpw	(%rbx,%rsi,2), %r11w
	je	.L88
	movl	-44(%rbp), %ebx
.L87:
	subl	$1, %ebx
	testl	%ebx, %ebx
	jg	.L92
.L98:
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	cmpw	$-1, %r11w
	jne	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	movl	(%rcx,%rax,8), %r8d
	movl	%eax, %r9d
	addq	$1, %rax
	addl	$1, %r8d
	leal	(%r8,%rdx), %esi
	cmpl	%esi, %r10d
	jbe	.L89
	movslq	%esi, %rsi
	cmpw	26(%rdi,%rsi,2), %r11w
	je	.L91
	subl	$1, %ebx
	testl	%ebx, %ebx
	jg	.L92
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L85:
	cmpw	$-1, %r11w
	je	.L88
	movl	-44(%rbp), %ebx
	jmp	.L87
	.cfi_endproc
.LFE2408:
	.size	_ZNK6icu_6717UCharsTrieBuilder23skipElementsBySomeUnitsEiii, .-_ZNK6icu_6717UCharsTrieBuilder23skipElementsBySomeUnitsEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717UCharsTrieBuilder26indexOfElementWithNextUnitEiiDs
	.type	_ZNK6icu_6717UCharsTrieBuilder26indexOfElementWithNextUnitEiiDs, @function
_ZNK6icu_6717UCharsTrieBuilder26indexOfElementWithNextUnitEiiDs:
.LFB2409:
	.cfi_startproc
	endbr64
	movswl	24(%rdi), %r10d
	movq	80(%rdi), %r9
	movl	%r10d, %r8d
	sarl	$5, %r10d
	movl	%r8d, %eax
	andl	$2, %eax
	testw	%r8w, %r8w
	js	.L116
	testw	%ax, %ax
	movslq	%esi, %rax
	jne	.L115
	.p2align 4,,10
	.p2align 3
.L112:
	movl	(%r9,%rax,8), %esi
	movl	%eax, %r8d
	addq	$1, %rax
	leal	1(%rdx,%rsi), %esi
	cmpl	%esi, %r10d
	jbe	.L110
	movq	40(%rdi), %r11
	movslq	%esi, %rsi
	cmpw	(%r11,%rsi,2), %cx
	je	.L112
.L99:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	cmpw	$-1, %cx
	jne	.L99
	.p2align 4,,10
	.p2align 3
.L115:
	movl	(%r9,%rax,8), %esi
	movl	%eax, %r8d
	addq	$1, %rax
	leal	1(%rdx,%rsi), %esi
	cmpl	%esi, %r10d
	jbe	.L113
	movslq	%esi, %rsi
	cmpw	26(%rdi,%rsi,2), %cx
	je	.L115
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	cmpw	$-1, %cx
	je	.L112
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L116:
	testw	%ax, %ax
	jne	.L101
	movl	28(%rdi), %r11d
	movslq	%esi, %rax
	.p2align 4,,10
	.p2align 3
.L105:
	movl	(%r9,%rax,8), %esi
	movl	%eax, %r8d
	addq	$1, %rax
	leal	1(%rdx,%rsi), %esi
	cmpl	%esi, %r11d
	jbe	.L102
	movq	40(%rdi), %r10
	movslq	%esi, %rsi
	cmpw	(%r10,%rsi,2), %cx
	je	.L105
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L102:
	cmpw	$-1, %cx
	je	.L105
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L101:
	movl	28(%rdi), %r10d
	movslq	%esi, %rax
	.p2align 4,,10
	.p2align 3
.L108:
	movl	(%r9,%rax,8), %esi
	movl	%eax, %r8d
	addq	$1, %rax
	leal	1(%rdx,%rsi), %esi
	cmpl	%r10d, %esi
	jnb	.L106
	movslq	%esi, %rsi
	cmpw	26(%rdi,%rsi,2), %cx
	je	.L108
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L106:
	cmpw	$-1, %cx
	je	.L108
	jmp	.L99
	.cfi_endproc
.LFE2409:
	.size	_ZNK6icu_6717UCharsTrieBuilder26indexOfElementWithNextUnitEiiDs, .-_ZNK6icu_6717UCharsTrieBuilder26indexOfElementWithNextUnitEiiDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilderD2Ev
	.type	_ZN6icu_6717UCharsTrieBuilderD2Ev, @function
_ZN6icu_6717UCharsTrieBuilderD2Ev:
.LFB2395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717UCharsTrieBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	80(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZN6icu_677UMemorydaEPv@PLT
.L118:
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717StringTrieBuilderD2Ev@PLT
	.cfi_endproc
.LFE2395:
	.size	_ZN6icu_6717UCharsTrieBuilderD2Ev, .-_ZN6icu_6717UCharsTrieBuilderD2Ev
	.globl	_ZN6icu_6717UCharsTrieBuilderD1Ev
	.set	_ZN6icu_6717UCharsTrieBuilderD1Ev,_ZN6icu_6717UCharsTrieBuilderD2Ev
	.section	.text._ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD2Ev,"axG",@progbits,_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD2Ev
	.type	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD2Ev, @function
_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD2Ev:
.LFB2953:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2953:
	.size	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD2Ev, .-_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD2Ev
	.weak	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD1Ev
	.set	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD1Ev,_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD2Ev
	.section	.text._ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD0Ev,"axG",@progbits,_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD0Ev
	.type	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD0Ev, @function
_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD0Ev:
.LFB2955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2955:
	.size	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD0Ev, .-_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE
	.type	_ZNK6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE, @function
_ZNK6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE:
.LFB2417:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpq	%rsi, %rdi
	je	.L133
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZNK6icu_6717StringTrieBuilder15LinearMatchNodeeqERKNS0_4NodeE@PLT
	testb	%al, %al
	jne	.L136
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movl	24(%rbx), %edx
	movq	40(%r12), %rsi
	movq	40(%rbx), %rdi
	call	u_memcmp_67@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2417:
	.size	_ZNK6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE, .-_ZNK6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilderD0Ev
	.type	_ZN6icu_6717UCharsTrieBuilderD0Ev, @function
_ZN6icu_6717UCharsTrieBuilderD0Ev:
.LFB2397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717UCharsTrieBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	80(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L138
	call	_ZN6icu_677UMemorydaEPv@PLT
.L138:
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6717StringTrieBuilderD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2397:
	.size	_ZN6icu_6717UCharsTrieBuilderD0Ev, .-_ZN6icu_6717UCharsTrieBuilderD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717UCharsTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE
	.type	_ZNK6icu_6717UCharsTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE, @function
_ZNK6icu_6717UCharsTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE:
.LFB2419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movslq	%edx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	leaq	16(%rdi), %r8
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	movl	(%rax,%rsi,8), %edx
	movzwl	24(%rdi), %eax
	testw	%ax, %ax
	js	.L144
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L145:
	movl	$65535, %r9d
	cmpl	%edx, %ecx
	jbe	.L146
	testb	$2, %al
	jne	.L162
	movq	40(%rdi), %rdi
.L148:
	movslq	%edx, %rax
	movzwl	(%rdi,%rax,2), %r9d
.L146:
	leaq	-128(%rbp), %r14
	addl	$1, %edx
	movl	%r9d, %ecx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movzwl	-120(%rbp), %edx
	testb	$17, %dl
	jne	.L154
	leaq	-118(%rbp), %rax
	andl	$2, %edx
	cmove	-104(%rbp), %rax
.L149:
	leaq	(%rax,%r12,2), %rax
	movl	$48, %edi
	movq	%rax, -136(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L150
	leal	0(%r13,%r13,8), %eax
	leal	0(%r13,%rax,4), %edx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L151
	movl	8(%rbx), %eax
.L151:
	movq	-136(%rbp), %rdi
	movb	$0, 16(%r12)
	movq	%rbx, %xmm0
	leal	298634171(%rax,%rdx), %r15d
	leaq	16+_ZTVN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE(%rip), %rax
	movl	%r15d, 8(%r12)
	movl	%r13d, %esi
	movq	%rdi, %xmm1
	movl	%r13d, 24(%r12)
	movl	$0, 12(%r12)
	punpcklqdq	%xmm1, %xmm0
	movl	$0, 20(%r12)
	movq	%rax, (%r12)
	movups	%xmm0, 32(%r12)
	call	ustr_hashUCharsN_67@PLT
	movl	%eax, %r8d
	leal	(%r15,%r15,8), %eax
	leal	(%r15,%rax,4), %eax
	addl	%r8d, %eax
	movl	%eax, 8(%r12)
.L150:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	addq	$26, %rdi
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L144:
	movl	28(%rdi), %ecx
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%eax, %eax
	jmp	.L149
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2419:
	.size	_ZNK6icu_6717UCharsTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE, .-_ZNK6icu_6717UCharsTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder5writeEi
	.type	_ZN6icu_6717UCharsTrieBuilder5writeEi, @function
_ZN6icu_6717UCharsTrieBuilder5writeEi:
.LFB2421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	96(%rdi), %r15
	movl	108(%rdi), %eax
	testq	%r15, %r15
	je	.L169
	movl	104(%rdi), %ebx
	leal	1(%rax), %r14d
	movq	%rdi, %r12
	movl	%esi, %r13d
	cmpl	%ebx, %r14d
	jg	.L167
.L166:
	subl	%r14d, %ebx
	movl	%r14d, 108(%r12)
	movslq	%ebx, %rbx
	movw	%r13w, (%r15,%rbx,2)
.L164:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movl	%ebx, %edi
	addl	%ebx, %ebx
	cmpl	%ebx, %r14d
	jge	.L167
	sall	$2, %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L172
	movl	108(%r12), %edx
	movl	104(%r12), %eax
	movq	96(%r12), %rcx
	subl	%edx, %eax
	cltq
	leaq	(%rcx,%rax,2), %rsi
	movl	%ebx, %eax
	subl	%edx, %eax
	cltq
	leaq	(%r15,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r15, 96(%r12)
	movl	%ebx, 104(%r12)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L169:
	movl	%eax, %r14d
	jmp	.L164
.L172:
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movl	108(%r12), %r14d
	movq	$0, 96(%r12)
	movl	$0, 104(%r12)
	jmp	.L164
	.cfi_endproc
.LFE2421:
	.size	_ZN6icu_6717UCharsTrieBuilder5writeEi, .-_ZN6icu_6717UCharsTrieBuilder5writeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder17writeElementUnitsEiii
	.type	_ZN6icu_6717UCharsTrieBuilder17writeElementUnitsEiii, @function
_ZN6icu_6717UCharsTrieBuilder17writeElementUnitsEiii:
.LFB2423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	leaq	16(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movl	%edx, -132(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	movl	(%rax,%rsi,8), %edx
	movzwl	24(%rdi), %eax
	testw	%ax, %ax
	js	.L174
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L175:
	movl	$65535, %r10d
	cmpl	%edx, %ecx
	jbe	.L176
	testb	$2, %al
	jne	.L193
	movq	40(%rbx), %rax
.L178:
	movslq	%edx, %rcx
	movzwl	(%rax,%rcx,2), %r10d
.L176:
	leaq	-128(%rbp), %r14
	addl	$1, %edx
	movl	%r10d, %ecx
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movzwl	-120(%rbp), %eax
	testb	$17, %al
	jne	.L186
	leaq	-118(%rbp), %r15
	testb	$2, %al
	cmove	-104(%rbp), %r15
	movq	%r15, -144(%rbp)
.L179:
	movl	108(%rbx), %r12d
	movq	96(%rbx), %rcx
	leal	0(%r13,%r12), %r15d
	testq	%rcx, %rcx
	je	.L180
	movl	104(%rbx), %r12d
	cmpl	%r12d, %r15d
	jle	.L181
	.p2align 4,,10
	.p2align 3
.L182:
	movl	%r12d, %edi
	addl	%r12d, %r12d
	cmpl	%r12d, %r15d
	jge	.L182
	sall	$2, %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L194
	movl	108(%rbx), %edx
	movl	104(%rbx), %eax
	movq	%rcx, -152(%rbp)
	movq	96(%rbx), %rsi
	subl	%edx, %eax
	cltq
	leaq	(%rsi,%rax,2), %rsi
	movl	%r12d, %eax
	subl	%edx, %eax
	cltq
	leaq	(%rcx,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	-152(%rbp), %rcx
	movl	%r12d, 104(%rbx)
	movq	%rcx, 96(%rbx)
.L181:
	movslq	-132(%rbp), %rax
	movq	-144(%rbp), %rsi
	subl	%r15d, %r12d
	movl	%r13d, %edx
	movl	%r15d, 108(%rbx)
	movslq	%r12d, %r12
	leaq	(%rcx,%r12,2), %rdi
	leaq	(%rsi,%rax,2), %rsi
	call	u_memcpy_67@PLT
	movl	108(%rbx), %r12d
.L180:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L195
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	leaq	26(%rbx), %rax
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L174:
	movl	28(%rdi), %ecx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L186:
	movq	$0, -144(%rbp)
	jmp	.L179
.L195:
	call	__stack_chk_fail@PLT
.L194:
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	movl	$0, 104(%rbx)
	movl	108(%rbx), %r12d
	movq	$0, 96(%rbx)
	jmp	.L180
	.cfi_endproc
.LFE2423:
	.size	_ZN6icu_6717UCharsTrieBuilder17writeElementUnitsEiii, .-_ZN6icu_6717UCharsTrieBuilder17writeElementUnitsEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder17writeValueAndTypeEaii
	.type	_ZN6icu_6717UCharsTrieBuilder17writeValueAndTypeEaii, @function
_ZN6icu_6717UCharsTrieBuilder17writeValueAndTypeEaii:
.LFB2425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%sil, %sil
	jne	.L197
	movq	(%rdi), %rax
	leaq	_ZN6icu_6717UCharsTrieBuilder5writeEi(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L198
	movl	108(%rdi), %eax
	movq	96(%rdi), %r15
	leal	1(%rax), %r13d
	testq	%r15, %r15
	je	.L220
	movl	104(%rdi), %ebx
	cmpl	%ebx, %r13d
	jle	.L200
	.p2align 4,,10
	.p2align 3
.L201:
	movl	%ebx, %edi
	addl	%ebx, %ebx
	cmpl	%ebx, %r13d
	jge	.L201
	sall	$2, %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L219
	movl	108(%r12), %edx
	movl	104(%r12), %eax
	movq	96(%r12), %rcx
	subl	%edx, %eax
	cltq
	leaq	(%rcx,%rax,2), %rsi
	movl	%ebx, %eax
	subl	%edx, %eax
	cltq
	leaq	(%r15,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r15, 96(%r12)
	movl	%ebx, 104(%r12)
.L200:
	subl	%r13d, %ebx
	movl	%r13d, 108(%r12)
	movslq	%ebx, %rbx
	movw	%r14w, (%r15,%rbx,2)
.L196:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	cmpl	$16646143, %edx
	jbe	.L204
	movl	%edx, %eax
	movw	%dx, -58(%rbp)
	movl	$32704, %ecx
	movl	$3, %r15d
	shrl	$16, %eax
	movw	%ax, -60(%rbp)
.L205:
	orl	%ecx, %r14d
	movl	108(%r12), %r13d
	movw	%r14w, -62(%rbp)
	movq	96(%r12), %r14
	leal	(%r15,%r13), %ecx
	testq	%r14, %r14
	je	.L196
	movl	104(%r12), %ebx
	cmpl	%ebx, %ecx
	jg	.L209
.L208:
	subl	%ecx, %ebx
	movl	%ecx, 108(%r12)
	leaq	-62(%rbp), %rsi
	movl	%r15d, %edx
	movslq	%ebx, %rbx
	leaq	(%r14,%rbx,2), %rdi
	call	u_memcpy_67@PLT
	movl	108(%r12), %r13d
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L204:
	cmpl	$255, %edx
	jg	.L206
	leal	1(%rdx), %ecx
	movl	$1, %r15d
	sall	$6, %ecx
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L209:
	movl	%ebx, %edi
	addl	%ebx, %ebx
	cmpl	%ebx, %ecx
	jge	.L209
	sall	$2, %edi
	movl	%ecx, -68(%rbp)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movl	-68(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L219
	movl	108(%r12), %edx
	movl	104(%r12), %eax
	movl	%ecx, -68(%rbp)
	movq	96(%r12), %rsi
	subl	%edx, %eax
	cltq
	leaq	(%rsi,%rax,2), %rsi
	movl	%ebx, %eax
	subl	%edx, %eax
	cltq
	leaq	(%r14,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r14, 96(%r12)
	movl	-68(%rbp), %ecx
	movl	%ebx, 104(%r12)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L206:
	movl	%edx, %ecx
	movw	%dx, -60(%rbp)
	movl	$2, %r15d
	sarl	$10, %ecx
	andw	$32704, %cx
	addw	$16448, %cx
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L198:
	movl	%ecx, %esi
	call	*%rax
.L220:
	movl	%eax, %r13d
	jmp	.L196
.L221:
	call	__stack_chk_fail@PLT
.L219:
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movl	108(%r12), %r13d
	movq	$0, 96(%r12)
	movl	$0, 104(%r12)
	jmp	.L196
	.cfi_endproc
.LFE2425:
	.size	_ZN6icu_6717UCharsTrieBuilder17writeValueAndTypeEaii, .-_ZN6icu_6717UCharsTrieBuilder17writeValueAndTypeEaii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder18writeValueAndFinalEia
	.type	_ZN6icu_6717UCharsTrieBuilder18writeValueAndFinalEia, @function
_ZN6icu_6717UCharsTrieBuilder18writeValueAndFinalEia:
.LFB2424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movsbl	%dl, %r13d
	pushq	%r12
	sall	$15, %r13d
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$16383, %esi
	ja	.L223
	movq	(%rdi), %rax
	leaq	_ZN6icu_6717UCharsTrieBuilder5writeEi(%rip), %rdx
	orl	%esi, %r13d
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L224
	movl	108(%rdi), %eax
	movq	96(%rdi), %r15
	leal	1(%rax), %r14d
	testq	%r15, %r15
	je	.L245
	movl	104(%rdi), %ebx
	cmpl	%ebx, %r14d
	jle	.L226
	.p2align 4,,10
	.p2align 3
.L227:
	movl	%ebx, %edi
	addl	%ebx, %ebx
	cmpl	%ebx, %r14d
	jge	.L227
	sall	$2, %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L244
	movl	108(%r12), %edx
	movl	104(%r12), %eax
	movq	96(%r12), %rcx
	subl	%edx, %eax
	cltq
	leaq	(%rcx,%rax,2), %rsi
	movl	%ebx, %eax
	subl	%edx, %eax
	cltq
	leaq	(%r15,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r15, 96(%r12)
	movl	%ebx, 104(%r12)
.L226:
	subl	%r14d, %ebx
	movl	%r14d, 108(%r12)
	movslq	%ebx, %rbx
	movw	%r13w, (%r15,%rbx,2)
.L222:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L246
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	cmpl	$1073676287, %esi
	ja	.L247
	movl	%esi, %edx
	movl	%esi, %eax
	movl	$2, %r15d
	sarl	$16, %edx
	addw	$16384, %dx
.L231:
	movl	108(%r12), %r14d
	movq	96(%r12), %rbx
	orl	%edx, %r13d
	movw	%ax, -60(%rbp)
	movw	%r13w, -62(%rbp)
	leal	(%r15,%r14), %ecx
	testq	%rbx, %rbx
	je	.L222
	movl	104(%r12), %r13d
	cmpl	%r13d, %ecx
	jg	.L234
.L233:
	subl	%ecx, %r13d
	movl	%ecx, 108(%r12)
	leaq	-62(%rbp), %rsi
	movl	%r15d, %edx
	movslq	%r13d, %r13
	leaq	(%rbx,%r13,2), %rdi
	call	u_memcpy_67@PLT
	movl	108(%r12), %r14d
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L234:
	movl	%r13d, %edi
	addl	%r13d, %r13d
	cmpl	%r13d, %ecx
	jge	.L234
	sall	$2, %edi
	movl	%ecx, -68(%rbp)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movl	-68(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L244
	movl	108(%r12), %edx
	movl	104(%r12), %eax
	movl	%ecx, -68(%rbp)
	movq	96(%r12), %rsi
	subl	%edx, %eax
	cltq
	leaq	(%rsi,%rax,2), %rsi
	movl	%r13d, %eax
	subl	%edx, %eax
	cltq
	leaq	(%rbx,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%rbx, 96(%r12)
	movl	-68(%rbp), %ecx
	movl	%r13d, 104(%r12)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L247:
	movl	%esi, %eax
	movw	%si, -58(%rbp)
	movl	$32767, %edx
	movl	$3, %r15d
	shrl	$16, %eax
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L224:
	movl	%r13d, %esi
	call	*%rax
.L245:
	movl	%eax, %r14d
	jmp	.L222
.L246:
	call	__stack_chk_fail@PLT
.L244:
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movl	108(%r12), %r14d
	movq	$0, 96(%r12)
	movl	$0, 104(%r12)
	jmp	.L222
	.cfi_endproc
.LFE2424:
	.size	_ZN6icu_6717UCharsTrieBuilder18writeValueAndFinalEia, .-_ZN6icu_6717UCharsTrieBuilder18writeValueAndFinalEia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder12writeDeltaToEi
	.type	_ZN6icu_6717UCharsTrieBuilder12writeDeltaToEi, @function
_ZN6icu_6717UCharsTrieBuilder12writeDeltaToEi:
.LFB2426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	108(%rdi), %eax
	movl	%eax, %r13d
	subl	%esi, %r13d
	cmpl	$64511, %r13d
	jg	.L249
	movq	(%rdi), %rdx
	leaq	_ZN6icu_6717UCharsTrieBuilder5writeEi(%rip), %rcx
	movq	120(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L250
	movq	96(%rdi), %r15
	leal	1(%rax), %r14d
	testq	%r15, %r15
	je	.L264
	movl	104(%rdi), %ebx
	cmpl	%ebx, %r14d
	jle	.L252
	.p2align 4,,10
	.p2align 3
.L253:
	movl	%ebx, %edi
	addl	%ebx, %ebx
	cmpl	%ebx, %r14d
	jge	.L253
	sall	$2, %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L268
	movl	108(%r12), %edx
	movl	104(%r12), %eax
	movq	96(%r12), %rcx
	subl	%edx, %eax
	cltq
	leaq	(%rcx,%rax,2), %rsi
	movl	%ebx, %eax
	subl	%edx, %eax
	cltq
	leaq	(%r15,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r15, 96(%r12)
	movl	%ebx, 104(%r12)
.L252:
	subl	%r14d, %ebx
	movl	%r14d, 108(%r12)
	movslq	%ebx, %rbx
	movw	%r13w, (%r15,%rbx,2)
.L248:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L269
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	movl	%r13d, %ecx
	sarl	$16, %ecx
	cmpl	$67043327, %r13d
	jle	.L270
	movw	%cx, -60(%rbp)
	movl	$3, %r15d
	movl	$-1, %ecx
	movl	$2, %edx
.L257:
	movw	%cx, -62(%rbp)
	leal	(%rax,%r15), %r14d
	movw	%r13w, -62(%rbp,%rdx,2)
	movq	96(%r12), %r13
	testq	%r13, %r13
	je	.L264
	movl	104(%r12), %ebx
	cmpl	%ebx, %r14d
	jg	.L260
.L259:
	movl	%r14d, 108(%r12)
	subl	%r14d, %ebx
	leaq	-62(%rbp), %rsi
	movl	%r15d, %edx
	movslq	%ebx, %rbx
	leaq	0(%r13,%rbx,2), %rdi
	call	u_memcpy_67@PLT
	movl	108(%r12), %r14d
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L260:
	movl	%ebx, %edi
	addl	%ebx, %ebx
	cmpl	%ebx, %r14d
	jge	.L260
	sall	$2, %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L268
	movl	108(%r12), %edx
	movl	104(%r12), %eax
	movq	96(%r12), %rcx
	subl	%edx, %eax
	cltq
	leaq	(%rcx,%rax,2), %rsi
	movl	%ebx, %eax
	subl	%edx, %eax
	cltq
	leaq	0(%r13,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r13, 96(%r12)
	movl	%ebx, 104(%r12)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L270:
	subw	$1024, %cx
	movl	$2, %r15d
	movl	$1, %edx
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L250:
	movl	%r13d, %esi
	call	*%rdx
	movl	%eax, %r14d
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L264:
	movl	%eax, %r14d
	jmp	.L248
.L269:
	call	__stack_chk_fail@PLT
.L268:
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movl	108(%r12), %r14d
	movq	$0, 96(%r12)
	movl	$0, 104(%r12)
	jmp	.L248
	.cfi_endproc
.LFE2426:
	.size	_ZN6icu_6717UCharsTrieBuilder12writeDeltaToEi, .-_ZN6icu_6717UCharsTrieBuilder12writeDeltaToEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieElement5setToERKNS_13UnicodeStringEiRS1_R10UErrorCode
	.type	_ZN6icu_6717UCharsTrieElement5setToERKNS_13UnicodeStringEiRS1_R10UErrorCode, @function
_ZN6icu_6717UCharsTrieElement5setToERKNS_13UnicodeStringEiRS1_R10UErrorCode:
.LFB2389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L271
	movswl	8(%rsi), %eax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movl	%edx, %r14d
	movq	%rcx, %r13
	testw	%ax, %ax
	js	.L273
	sarl	$5, %eax
.L274:
	movswl	8(%r13), %edx
	testw	%dx, %dx
	js	.L275
	sarl	$5, %edx
.L276:
	movl	%edx, (%rbx)
	movl	$1, %ecx
	leaq	-42(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%ax, -42(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	8(%r12), %ecx
	movl	%r14d, 4(%rbx)
	testw	%cx, %cx
	js	.L277
	sarl	$5, %ecx
.L278:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L271:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L281
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movl	12(%rsi), %eax
	cmpl	$65535, %eax
	jle	.L274
	movl	$8, (%r8)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L277:
	movl	12(%r12), %ecx
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L275:
	movl	12(%r13), %edx
	jmp	.L276
.L281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2389:
	.size	_ZN6icu_6717UCharsTrieElement5setToERKNS_13UnicodeStringEiRS1_R10UErrorCode, .-_ZN6icu_6717UCharsTrieElement5setToERKNS_13UnicodeStringEiRS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717UCharsTrieElement15compareStringToERKS0_RKNS_13UnicodeStringE
	.type	_ZNK6icu_6717UCharsTrieElement15compareStringToERKS0_RKNS_13UnicodeStringE, @function
_ZNK6icu_6717UCharsTrieElement15compareStringToERKS0_RKNS_13UnicodeStringE:
.LFB2390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$152, %rsp
	movl	(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	js	.L283
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L284:
	movl	$65535, %r8d
	cmpl	%edx, %ecx
	jbe	.L285
	testb	$2, %al
	jne	.L307
	movq	24(%r12), %rax
.L287:
	movslq	%edx, %rcx
	movzwl	(%rax,%rcx,2), %r8d
.L285:
	leaq	-176(%rbp), %r13
	addl	$1, %edx
	movl	%r8d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movzwl	8(%r12), %eax
	movl	(%r14), %edx
	testw	%ax, %ax
	js	.L288
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L289:
	movl	$65535, %r8d
	cmpl	%edx, %ecx
	jbe	.L290
	testb	$2, %al
	jne	.L308
	movq	24(%r12), %rax
.L292:
	movslq	%edx, %rcx
	movzwl	(%rax,%rcx,2), %r8d
.L290:
	leaq	-112(%rbp), %r14
	movq	%r12, %rsi
	addl	$1, %edx
	movl	%r8d, %ecx
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movzwl	-104(%rbp), %esi
	testw	%si, %si
	js	.L293
	movswl	%si, %ecx
	sarl	$5, %ecx
.L294:
	movzwl	-168(%rbp), %eax
	testw	%ax, %ax
	js	.L295
	movswl	%ax, %edx
	sarl	$5, %edx
.L296:
	testb	$1, %sil
	je	.L297
	notl	%eax
	andl	$1, %eax
	movl	%eax, %r12d
.L298:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movsbl	%r12b, %eax
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L309
	addq	$152, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L299
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L299:
	andl	$2, %esi
	leaq	-102(%rbp), %rcx
	cmove	-88(%rbp), %rcx
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movl	%eax, %r12d
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L308:
	leaq	10(%r12), %rax
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L307:
	leaq	10(%r12), %rax
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L288:
	movl	12(%r12), %ecx
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L283:
	movl	12(%r12), %ecx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L295:
	movl	-164(%rbp), %edx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L293:
	movl	-100(%rbp), %ecx
	jmp	.L294
.L309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2390:
	.size	_ZNK6icu_6717UCharsTrieElement15compareStringToERKS0_RKNS_13UnicodeStringE, .-_ZNK6icu_6717UCharsTrieElement15compareStringToERKS0_RKNS_13UnicodeStringE
	.p2align 4
	.type	compareElementStrings, @function
compareElementStrings:
.LFB2399:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%r8, %rdx
	jmp	_ZNK6icu_6717UCharsTrieElement15compareStringToERKS0_RKNS_13UnicodeStringE
	.cfi_endproc
.LFE2399:
	.size	compareElementStrings, .-compareElementStrings
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilderC2ER10UErrorCode
	.type	_ZN6icu_6717UCharsTrieBuilderC2ER10UErrorCode, @function
_ZN6icu_6717UCharsTrieBuilderC2ER10UErrorCode:
.LFB2392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6717StringTrieBuilderC2Ev@PLT
	leaq	16+_ZTVN6icu_6717UCharsTrieBuilderE(%rip), %rax
	movq	$0, 80(%rbx)
	movq	%rax, (%rbx)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 16(%rbx)
	movl	$2, %eax
	movw	%ax, 24(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 104(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2392:
	.size	_ZN6icu_6717UCharsTrieBuilderC2ER10UErrorCode, .-_ZN6icu_6717UCharsTrieBuilderC2ER10UErrorCode
	.globl	_ZN6icu_6717UCharsTrieBuilderC1ER10UErrorCode
	.set	_ZN6icu_6717UCharsTrieBuilderC1ER10UErrorCode,_ZN6icu_6717UCharsTrieBuilderC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode
	.type	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode, @function
_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode:
.LFB2398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L316
	movq	%rcx, %rbx
	movl	108(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L338
	movslq	92(%r12), %rax
	movq	%rsi, %r14
	movl	%edx, %r13d
	cmpl	88(%r12), %eax
	je	.L317
	leal	1(%rax), %edx
	movq	80(%r12), %rcx
	movl	%edx, 92(%r12)
.L318:
	movswl	8(%r14), %edx
	testw	%dx, %dx
	js	.L324
	sarl	$5, %edx
.L325:
	movswl	24(%r12), %esi
	testw	%si, %si
	js	.L326
	sarl	$5, %esi
.L327:
	leaq	(%rcx,%rax,8), %r8
	leaq	16(%r12), %r15
	movl	$1, %ecx
	movl	%esi, (%r8)
	movq	%r15, %rdi
	leaq	-58(%rbp), %rsi
	movw	%dx, -58(%rbp)
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-72(%rbp), %r8
	movswl	8(%r14), %ecx
	movl	%r13d, 4(%r8)
	testw	%cx, %cx
	js	.L328
	sarl	$5, %ecx
.L329:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L316
	testb	$1, 24(%r12)
	je	.L316
.L330:
	movl	$7, (%rbx)
	.p2align 4,,10
	.p2align 3
.L316:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L339
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	movl	$30, (%rbx)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L324:
	movl	12(%r14), %edx
	cmpl	$65535, %edx
	jle	.L325
	movl	$8, (%rbx)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L317:
	testl	%eax, %eax
	je	.L332
	leal	0(,%rax,4), %r15d
	movabsq	$1152921504606846975, %rax
	movslq	%r15d, %rdi
	cmpq	%rax, %rdi
	jbe	.L340
	movq	$-1, %rdi
.L320:
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L330
	movslq	92(%r12), %rax
	movq	80(%r12), %rdi
	testl	%eax, %eax
	jle	.L322
	movq	%rdi, %rsi
	leaq	0(,%rax,8), %rdx
	movq	%rcx, %rdi
	call	memcpy@PLT
	movq	80(%r12), %rdi
	movq	%rax, %rcx
.L322:
	testq	%rdi, %rdi
	je	.L323
	movq	%rcx, -72(%rbp)
	call	_ZN6icu_677UMemorydaEPv@PLT
	movq	-72(%rbp), %rcx
.L323:
	movslq	92(%r12), %rax
	movl	(%rbx), %edx
	movq	%rcx, 80(%r12)
	movl	%r15d, 88(%r12)
	leal	1(%rax), %esi
	movl	%esi, 92(%r12)
	testl	%edx, %edx
	jg	.L316
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L332:
	movl	$8192, %edi
	movl	$1024, %r15d
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L328:
	movl	12(%r14), %ecx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L326:
	movl	28(%r12), %esi
	jmp	.L327
.L339:
	call	__stack_chk_fail@PLT
.L340:
	salq	$3, %rdi
	jmp	.L320
	.cfi_endproc
.LFE2398:
	.size	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode, .-_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode
	.type	_ZN6icu_6717UCharsTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode, @function
_ZN6icu_6717UCharsTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode:
.LFB2400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L342
	cmpq	$0, 96(%rdi)
	movl	108(%rdi), %eax
	movq	%rdi, %rbx
	movl	%esi, %r13d
	movq	%rdx, %r15
	je	.L343
	testl	%eax, %eax
	jg	.L375
.L343:
	testl	%eax, %eax
	jne	.L346
	movl	92(%rbx), %esi
	testl	%esi, %esi
	je	.L394
	testb	$1, 24(%rbx)
	jne	.L393
	subq	$8, %rsp
	movq	80(%rbx), %rdi
	leaq	16(%rbx), %r14
	xorl	%r9d, %r9d
	pushq	%r15
	leaq	compareElementStrings(%rip), %rcx
	movl	$8, %edx
	movq	%r14, %r8
	call	uprv_sortArray_67@PLT
	movl	(%r15), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jle	.L395
	.p2align 4,,10
	.p2align 3
.L342:
	xorl	%eax, %eax
.L341:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L396
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L346:
	movswl	24(%rbx), %r12d
	movl	$0, 108(%rbx)
	testw	%r12w, %r12w
	js	.L370
	sarl	$5, %r12d
.L371:
	cmpl	$1024, %r12d
	movl	$1024, %eax
	cmovl	%eax, %r12d
	cmpl	104(%rbx), %r12d
	jle	.L372
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	leal	(%r12,%r12), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 96(%rbx)
	testq	%rax, %rax
	je	.L397
	movl	%r12d, 104(%rbx)
.L372:
	movl	92(%rbx), %edx
	movq	%r15, %rcx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode@PLT
	cmpq	$0, 96(%rbx)
	je	.L393
	movl	(%r15), %eax
.L369:
	testl	%eax, %eax
	jg	.L342
.L375:
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L398
	movq	96(%rbx), %rcx
	movl	104(%rbx), %edx
	subl	108(%rbx), %edx
	movl	$-1, 24(%rax)
	movslq	%edx, %rdx
	movq	%rcx, (%rax)
	leaq	(%rcx,%rdx,2), %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 96(%rbx)
	movl	$0, 104(%rbx)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L370:
	movl	28(%rbx), %r12d
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L394:
	movl	$8, (%r15)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$7, (%r15)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L395:
	movq	80(%rbx), %rax
	movl	(%rax), %edx
	movzwl	24(%rbx), %eax
	testw	%ax, %ax
	js	.L350
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L351:
	movl	$65535, %r8d
	cmpl	%edx, %ecx
	jbe	.L352
	testb	$2, %al
	je	.L353
	leaq	26(%rbx), %rax
.L354:
	movslq	%edx, %rcx
	movzwl	(%rax,%rcx,2), %r8d
.L352:
	leaq	-192(%rbp), %rdi
	addl	$1, %edx
	movl	%r8d, %ecx
	movq	%r14, %rsi
	movq	%rdi, -216(%rbp)
	leaq	-128(%rbp), %r12
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	cmpl	$1, 92(%rbx)
	movq	$8, -208(%rbp)
	movl	$1, -196(%rbp)
	jg	.L355
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L400:
	movzbl	-120(%rbp), %eax
	andl	$1, %eax
.L362:
	testb	%al, %al
	jne	.L399
.L367:
	movq	-216(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	$1, -196(%rbp)
	movl	-196(%rbp), %eax
	addq	$8, -208(%rbp)
	cmpl	%eax, 92(%rbx)
	jle	.L368
.L355:
	movq	80(%rbx), %rax
	movq	-208(%rbp), %rcx
	movl	(%rax,%rcx), %edx
	movzwl	24(%rbx), %eax
	testw	%ax, %ax
	js	.L356
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L357:
	movl	$65535, %r9d
	cmpl	%edx, %ecx
	jbe	.L358
	leaq	26(%rbx), %rcx
	testb	$2, %al
	jne	.L360
	movq	40(%rbx), %rcx
.L360:
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %r9d
.L358:
	addl	$1, %edx
	movl	%r9d, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-184(%rbp), %edx
	testb	$1, %dl
	jne	.L400
	testw	%dx, %dx
	js	.L363
	sarl	$5, %edx
.L364:
	movzwl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L365
	movswl	%cx, %eax
	sarl	$5, %eax
.L366:
	cmpl	%edx, %eax
	jne	.L367
	andl	$1, %ecx
	jne	.L367
	movq	-216(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L356:
	movl	28(%rbx), %ecx
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L365:
	movl	-116(%rbp), %eax
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L363:
	movl	-180(%rbp), %edx
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L399:
	movl	$1, (%r15)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r15), %eax
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L350:
	movl	28(%rbx), %ecx
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L353:
	movq	40(%rbx), %rax
	jmp	.L354
.L396:
	call	__stack_chk_fail@PLT
.L398:
	movl	$7, (%r15)
	jmp	.L341
.L397:
	movl	$7, (%r15)
	movl	$0, 104(%rbx)
	jmp	.L342
	.cfi_endproc
.LFE2400:
	.size	_ZN6icu_6717UCharsTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode, .-_ZN6icu_6717UCharsTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder18buildUnicodeStringE22UStringTrieBuildOptionRNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6717UCharsTrieBuilder18buildUnicodeStringE22UStringTrieBuildOptionRNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6717UCharsTrieBuilder18buildUnicodeStringE22UStringTrieBuildOptionRNS_13UnicodeStringER10UErrorCode:
.LFB2401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L434
	movq	96(%rdi), %rdx
	movq	%rcx, %r12
	movq	%rdi, %rbx
	movl	108(%rdi), %ecx
	movl	%esi, %r15d
	testq	%rdx, %rdx
	je	.L403
	testl	%ecx, %ecx
	jg	.L404
.L403:
	testl	%ecx, %ecx
	jne	.L405
	movl	92(%rbx), %esi
	testl	%esi, %esi
	je	.L454
	testb	$1, 24(%rbx)
	jne	.L453
	subq	$8, %rsp
	movq	80(%rbx), %rdi
	leaq	16(%rbx), %rax
	xorl	%r9d, %r9d
	pushq	%r12
	movq	%rax, %r8
	movl	$8, %edx
	leaq	compareElementStrings(%rip), %rcx
	movq	%rax, -232(%rbp)
	call	uprv_sortArray_67@PLT
	movl	(%r12), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jle	.L455
	.p2align 4,,10
	.p2align 3
.L434:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L456
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore_state
	movq	-240(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L405:
	movswl	24(%rbx), %r13d
	movl	$0, 108(%rbx)
	testw	%r13w, %r13w
	js	.L429
	sarl	$5, %r13d
.L430:
	cmpl	$1024, %r13d
	movl	$1024, %eax
	cmovl	%eax, %r13d
	cmpl	104(%rbx), %r13d
	jle	.L431
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	leal	(%r13,%r13), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 96(%rbx)
	testq	%rax, %rax
	je	.L457
	movl	%r13d, 104(%rbx)
.L431:
	movl	92(%rbx), %edx
	movq	%r12, %rcx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode@PLT
	cmpq	$0, 96(%rbx)
	je	.L453
	movl	(%r12), %eax
.L428:
	testl	%eax, %eax
	jg	.L434
	movl	108(%rbx), %ecx
	movq	96(%rbx), %rdx
.L404:
	movl	104(%rbx), %eax
	xorl	%esi, %esi
	movq	%r14, %rdi
	subl	%ecx, %eax
	cltq
	leaq	(%rdx,%rax,2), %rax
	leaq	-200(%rbp), %rdx
	movq	%rax, -200(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L429:
	movl	28(%rbx), %r13d
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L454:
	movl	$8, (%r12)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L453:
	movl	$7, (%r12)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L455:
	movq	80(%rbx), %rax
	movl	(%rax), %edx
	movzwl	24(%rbx), %eax
	testw	%ax, %ax
	js	.L409
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L410:
	movl	$65535, %r8d
	cmpl	%edx, %ecx
	jbe	.L411
	testb	$2, %al
	je	.L412
	leaq	26(%rbx), %rax
.L413:
	movslq	%edx, %rcx
	movzwl	(%rax,%rcx,2), %r8d
.L411:
	movq	-232(%rbp), %rsi
	leaq	-192(%rbp), %rdi
	addl	$1, %edx
	movl	%r8d, %ecx
	movq	%rdi, -240(%rbp)
	leaq	-128(%rbp), %r13
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	cmpl	$1, 92(%rbx)
	movq	$8, -224(%rbp)
	movl	$1, -212(%rbp)
	jg	.L414
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L459:
	movzbl	-120(%rbp), %eax
	andl	$1, %eax
.L421:
	testb	%al, %al
	jne	.L458
.L426:
	movq	-240(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	$1, -212(%rbp)
	movl	-212(%rbp), %eax
	addq	$8, -224(%rbp)
	cmpl	%eax, 92(%rbx)
	jle	.L427
.L414:
	movq	80(%rbx), %rax
	movq	-224(%rbp), %rsi
	movl	(%rax,%rsi), %edx
	movzwl	24(%rbx), %eax
	testw	%ax, %ax
	js	.L415
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L416:
	movl	$65535, %r9d
	cmpl	%edx, %ecx
	jbe	.L417
	leaq	26(%rbx), %rcx
	testb	$2, %al
	jne	.L419
	movq	40(%rbx), %rcx
.L419:
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %r9d
.L417:
	movq	-232(%rbp), %rsi
	addl	$1, %edx
	movl	%r9d, %ecx
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-184(%rbp), %edx
	testb	$1, %dl
	jne	.L459
	testw	%dx, %dx
	js	.L422
	sarl	$5, %edx
.L423:
	movzwl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L424
	movswl	%cx, %eax
	sarl	$5, %eax
.L425:
	cmpl	%edx, %eax
	jne	.L426
	andl	$1, %ecx
	jne	.L426
	movq	-240(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L415:
	movl	28(%rbx), %ecx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L424:
	movl	-116(%rbp), %eax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L422:
	movl	-180(%rbp), %edx
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L458:
	movl	$1, (%r12)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-240(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%r12), %eax
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L409:
	movl	28(%rbx), %ecx
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L412:
	movq	40(%rbx), %rax
	jmp	.L413
.L456:
	call	__stack_chk_fail@PLT
.L457:
	movl	$7, (%r12)
	movl	$0, 104(%rbx)
	jmp	.L434
	.cfi_endproc
.LFE2401:
	.size	_ZN6icu_6717UCharsTrieBuilder18buildUnicodeStringE22UStringTrieBuildOptionRNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6717UCharsTrieBuilder18buildUnicodeStringE22UStringTrieBuildOptionRNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder11buildUCharsE22UStringTrieBuildOptionR10UErrorCode
	.type	_ZN6icu_6717UCharsTrieBuilder11buildUCharsE22UStringTrieBuildOptionR10UErrorCode, @function
_ZN6icu_6717UCharsTrieBuilder11buildUCharsE22UStringTrieBuildOptionR10UErrorCode:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L460
	cmpq	$0, 96(%rdi)
	movl	108(%rdi), %eax
	movq	%rdi, %rbx
	movl	%esi, %r13d
	movq	%rdx, %r15
	je	.L463
	testl	%eax, %eax
	jg	.L460
.L463:
	testl	%eax, %eax
	jne	.L465
	movl	92(%rbx), %esi
	testl	%esi, %esi
	je	.L508
	testb	$1, 24(%rbx)
	jne	.L507
	subq	$8, %rsp
	movq	80(%rbx), %rdi
	leaq	16(%rbx), %r14
	xorl	%r9d, %r9d
	pushq	%r15
	leaq	compareElementStrings(%rip), %rcx
	movl	$8, %edx
	movq	%r14, %r8
	call	uprv_sortArray_67@PLT
	movl	(%r15), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jle	.L509
	.p2align 4,,10
	.p2align 3
.L460:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L510
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L465:
	movswl	24(%rbx), %r12d
	movl	$0, 108(%rbx)
	testw	%r12w, %r12w
	js	.L488
	sarl	$5, %r12d
.L489:
	cmpl	$1024, %r12d
	movl	$1024, %eax
	cmovl	%eax, %r12d
	cmpl	104(%rbx), %r12d
	jle	.L490
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	leal	(%r12,%r12), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 96(%rbx)
	testq	%rax, %rax
	je	.L511
	movl	%r12d, 104(%rbx)
.L490:
	movl	92(%rbx), %edx
	movq	%r15, %rcx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode@PLT
	cmpq	$0, 96(%rbx)
	jne	.L460
.L507:
	movl	$7, (%r15)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L488:
	movl	28(%rbx), %r12d
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$8, (%r15)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L509:
	movq	80(%rbx), %rax
	movl	(%rax), %edx
	movzwl	24(%rbx), %eax
	testw	%ax, %ax
	js	.L469
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L470:
	movl	$65535, %r8d
	cmpl	%edx, %ecx
	jbe	.L471
	testb	$2, %al
	je	.L472
	leaq	26(%rbx), %rax
.L473:
	movslq	%edx, %rcx
	movzwl	(%rax,%rcx,2), %r8d
.L471:
	leaq	-192(%rbp), %rdi
	addl	$1, %edx
	movl	%r8d, %ecx
	movq	%r14, %rsi
	movq	%rdi, -216(%rbp)
	leaq	-128(%rbp), %r12
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	cmpl	$1, 92(%rbx)
	movq	$8, -208(%rbp)
	movl	$1, -196(%rbp)
	jg	.L474
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L513:
	movzbl	-120(%rbp), %eax
	andl	$1, %eax
.L481:
	testb	%al, %al
	jne	.L512
.L486:
	movq	-216(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addl	$1, -196(%rbp)
	movl	-196(%rbp), %eax
	addq	$8, -208(%rbp)
	cmpl	%eax, 92(%rbx)
	jle	.L487
.L474:
	movq	80(%rbx), %rax
	movq	-208(%rbp), %rcx
	movl	(%rax,%rcx), %edx
	movzwl	24(%rbx), %eax
	testw	%ax, %ax
	js	.L475
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L476:
	movl	$65535, %r9d
	cmpl	%edx, %ecx
	jbe	.L477
	leaq	26(%rbx), %rcx
	testb	$2, %al
	jne	.L479
	movq	40(%rbx), %rcx
.L479:
	movslq	%edx, %rax
	movzwl	(%rcx,%rax,2), %r9d
.L477:
	addl	$1, %edx
	movl	%r9d, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movswl	-184(%rbp), %edx
	testb	$1, %dl
	jne	.L513
	testw	%dx, %dx
	js	.L482
	sarl	$5, %edx
.L483:
	movzwl	-120(%rbp), %ecx
	testw	%cx, %cx
	js	.L484
	movswl	%cx, %eax
	sarl	$5, %eax
.L485:
	cmpl	%edx, %eax
	jne	.L486
	andl	$1, %ecx
	jne	.L486
	movq	-216(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L475:
	movl	28(%rbx), %ecx
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L484:
	movl	-116(%rbp), %eax
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L482:
	movl	-180(%rbp), %edx
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L512:
	movl	$1, (%r15)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-216(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L469:
	movl	28(%rbx), %ecx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L472:
	movq	40(%rbx), %rax
	jmp	.L473
.L510:
	call	__stack_chk_fail@PLT
.L511:
	movl	$7, (%r15)
	movl	$0, 104(%rbx)
	jmp	.L460
	.cfi_endproc
.LFE2402:
	.size	_ZN6icu_6717UCharsTrieBuilder11buildUCharsE22UStringTrieBuildOptionR10UErrorCode, .-_ZN6icu_6717UCharsTrieBuilder11buildUCharsE22UStringTrieBuildOptionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeC2EPKDsiPNS_17StringTrieBuilder4NodeE
	.type	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeC2EPKDsiPNS_17StringTrieBuilder4NodeE, @function
_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeC2EPKDsiPNS_17StringTrieBuilder4NodeE:
.LFB2415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	(%rdx,%rdx,8), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	%edx, %esi
	leal	(%rdx,%rax,4), %edx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L515
	movl	8(%rcx), %eax
.L515:
	leal	298634171(%rax,%rdx), %r12d
	movb	$0, 16(%rbx)
	leaq	16+_ZTVN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE(%rip), %rax
	movq	%rcx, %xmm0
	movl	%r12d, 8(%rbx)
	movq	%rdi, %xmm1
	movl	$0, 12(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movl	$0, 20(%rbx)
	movl	%esi, 24(%rbx)
	movq	%rax, (%rbx)
	movups	%xmm0, 32(%rbx)
	call	ustr_hashUCharsN_67@PLT
	movl	%eax, %r8d
	leal	(%r12,%r12,8), %eax
	leal	(%r12,%rax,4), %eax
	addl	%r8d, %eax
	movl	%eax, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2415:
	.size	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeC2EPKDsiPNS_17StringTrieBuilder4NodeE, .-_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeC2EPKDsiPNS_17StringTrieBuilder4NodeE
	.globl	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeC1EPKDsiPNS_17StringTrieBuilder4NodeE
	.set	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeC1EPKDsiPNS_17StringTrieBuilder4NodeE,_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeC2EPKDsiPNS_17StringTrieBuilder4NodeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder14ensureCapacityEi
	.type	_ZN6icu_6717UCharsTrieBuilder14ensureCapacityEi, @function
_ZN6icu_6717UCharsTrieBuilder14ensureCapacityEi:
.LFB2420:
	.cfi_startproc
	endbr64
	cmpq	$0, 96(%rdi)
	je	.L524
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	104(%rdi), %ebx
	cmpl	%esi, %ebx
	jge	.L520
	.p2align 4,,10
	.p2align 3
.L522:
	movl	%ebx, %edi
	addl	%ebx, %ebx
	cmpl	%ebx, %esi
	jge	.L522
	sall	$2, %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L531
	movl	108(%r12), %edx
	movl	104(%r12), %eax
	movq	96(%r12), %rcx
	subl	%edx, %eax
	cltq
	leaq	(%rcx,%rax,2), %rsi
	movl	%ebx, %eax
	subl	%edx, %eax
	cltq
	leaq	0(%r13,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r13, 96(%r12)
	movl	$1, %eax
	movl	%ebx, 104(%r12)
.L520:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
.L531:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	xorl	%eax, %eax
	movq	$0, 96(%r12)
	movl	$0, 104(%r12)
	jmp	.L520
	.cfi_endproc
.LFE2420:
	.size	_ZN6icu_6717UCharsTrieBuilder14ensureCapacityEi, .-_ZN6icu_6717UCharsTrieBuilder14ensureCapacityEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder5writeEPKDsi
	.type	_ZN6icu_6717UCharsTrieBuilder5writeEPKDsi, @function
_ZN6icu_6717UCharsTrieBuilder5writeEPKDsi:
.LFB2422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	108(%rdi), %eax
	movq	96(%rdi), %r14
	movq	%rsi, -56(%rbp)
	leal	(%rax,%rdx), %r13d
	testq	%r14, %r14
	je	.L532
	movl	104(%rdi), %r12d
	movq	%rdi, %rbx
	movl	%edx, %r15d
	cmpl	%r12d, %r13d
	jle	.L534
	.p2align 4,,10
	.p2align 3
.L535:
	movl	%r12d, %edi
	addl	%r12d, %r12d
	cmpl	%r12d, %r13d
	jge	.L535
	sall	$2, %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L542
	movl	108(%rbx), %edx
	movl	104(%rbx), %eax
	movq	96(%rbx), %rsi
	subl	%edx, %eax
	cltq
	leaq	(%rsi,%rax,2), %rsi
	movl	%r12d, %eax
	subl	%edx, %eax
	cltq
	leaq	(%r14,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	%r14, 96(%rbx)
	movl	%r12d, 104(%rbx)
.L534:
	movl	%r13d, 108(%rbx)
	subl	%r13d, %r12d
	movq	-56(%rbp), %rsi
	movl	%r15d, %edx
	movslq	%r12d, %r12
	leaq	(%r14,%r12,2), %rdi
	call	u_memcpy_67@PLT
	movl	108(%rbx), %eax
.L532:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L542:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	$0, 96(%rbx)
	movl	108(%rbx), %eax
	movl	$0, 104(%rbx)
	jmp	.L532
	.cfi_endproc
.LFE2422:
	.size	_ZN6icu_6717UCharsTrieBuilder5writeEPKDsi, .-_ZN6icu_6717UCharsTrieBuilder5writeEPKDsi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNode5writeERNS_17StringTrieBuilderE
	.type	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNode5writeERNS_17StringTrieBuilderE, @function
_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNode5writeERNS_17StringTrieBuilderE:
.LFB2418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	32(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*40(%rax)
	movl	24(%rbx), %r14d
	movl	108(%r12), %ecx
	movq	96(%r12), %r15
	addl	%r14d, %ecx
	testq	%r15, %r15
	je	.L544
	movq	40(%rbx), %rax
	movl	104(%r12), %r13d
	movq	%rax, -72(%rbp)
	cmpl	%r13d, %ecx
	jle	.L545
	.p2align 4,,10
	.p2align 3
.L546:
	movl	%r13d, %edi
	addl	%r13d, %r13d
	cmpl	%r13d, %ecx
	jge	.L546
	sall	$2, %edi
	movl	%ecx, -76(%rbp)
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movl	-76(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L570
	movl	108(%r12), %edx
	movl	104(%r12), %eax
	movl	%ecx, -76(%rbp)
	movq	96(%r12), %rsi
	subl	%edx, %eax
	cltq
	leaq	(%rsi,%rax,2), %rsi
	movl	%r13d, %eax
	subl	%edx, %eax
	cltq
	leaq	(%r15,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r15, 96(%r12)
	movl	-76(%rbp), %ecx
	movl	%r13d, 104(%r12)
.L545:
	subl	%ecx, %r13d
	movl	%ecx, 108(%r12)
	movq	-72(%rbp), %rsi
	movl	%r14d, %edx
	movslq	%r13d, %r13
	leaq	(%r15,%r13,2), %rdi
	call	u_memcpy_67@PLT
.L544:
	movq	(%r12), %rax
	leaq	_ZNK6icu_6717UCharsTrieBuilder17getMinLinearMatchEv(%rip), %rcx
	movq	96(%rax), %rdx
	movq	144(%rax), %r13
	movl	$48, %eax
	cmpq	%rcx, %rdx
	jne	.L571
.L548:
	addl	24(%rbx), %eax
	movl	20(%rbx), %edx
	leal	-1(%rax), %r15d
	leaq	_ZN6icu_6717UCharsTrieBuilder17writeValueAndTypeEaii(%rip), %rax
	movsbl	16(%rbx), %esi
	cmpq	%rax, %r13
	jne	.L549
	testb	%sil, %sil
	je	.L572
	cmpl	$16646143, %edx
	jbe	.L557
	movl	%edx, %eax
	movw	%dx, -58(%rbp)
	movl	$3, %edx
	shrl	$16, %eax
	movw	%ax, -60(%rbp)
	movl	$32704, %eax
.L558:
	orl	%r15d, %eax
	leaq	-62(%rbp), %rsi
	movq	%r12, %rdi
	movw	%ax, -62(%rbp)
	call	_ZN6icu_6717UCharsTrieBuilder5writeEPKDsi
	movl	%eax, %r14d
.L556:
	movl	%r14d, 12(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L573
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	movq	(%r12), %rax
	leaq	_ZN6icu_6717UCharsTrieBuilder5writeEi(%rip), %rdx
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L551
	movl	108(%r12), %eax
	movq	96(%r12), %rcx
	leal	1(%rax), %r14d
	testq	%rcx, %rcx
	je	.L569
	movl	104(%r12), %r13d
	cmpl	%r13d, %r14d
	jle	.L553
	.p2align 4,,10
	.p2align 3
.L554:
	movl	%r13d, %edi
	addl	%r13d, %r13d
	cmpl	%r13d, %r14d
	jge	.L554
	sall	$2, %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L574
	movl	108(%r12), %edx
	movl	104(%r12), %eax
	movq	%rcx, -72(%rbp)
	movq	96(%r12), %rsi
	subl	%edx, %eax
	cltq
	leaq	(%rsi,%rax,2), %rsi
	movl	%r13d, %eax
	subl	%edx, %eax
	cltq
	leaq	(%rcx,%rax,2), %rdi
	call	u_memcpy_67@PLT
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rcx
	movl	%r13d, 104(%r12)
	movq	%rcx, 96(%r12)
.L553:
	subl	%r14d, %r13d
	movl	%r14d, 108(%r12)
	movslq	%r13d, %r13
	movw	%r15w, (%rcx,%r13,2)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L549:
	movl	%r15d, %ecx
	movq	%r12, %rdi
	call	*%r13
.L569:
	movl	%eax, %r14d
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L557:
	cmpl	$255, %edx
	jg	.L559
	leal	1(%rdx), %eax
	movl	$1, %edx
	sall	$6, %eax
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L571:
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L559:
	movl	%edx, %eax
	movw	%dx, -60(%rbp)
	movl	$2, %edx
	sarl	$10, %eax
	andw	$32704, %ax
	addw	$16448, %ax
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L551:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r14d
	jmp	.L556
.L573:
	call	__stack_chk_fail@PLT
.L570:
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movq	$0, 96(%r12)
	movl	$0, 104(%r12)
	jmp	.L544
.L574:
	movq	96(%r12), %rdi
	call	uprv_free_67@PLT
	movl	108(%r12), %r14d
	movq	$0, 96(%r12)
	movl	$0, 104(%r12)
	jmp	.L556
	.cfi_endproc
.LFE2418:
	.size	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNode5writeERNS_17StringTrieBuilderE, .-_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNode5writeERNS_17StringTrieBuilderE
	.weak	_ZTSN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE
	.section	.rodata._ZTSN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE,"aG",@progbits,_ZTSN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE,comdat
	.align 32
	.type	_ZTSN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE, @object
	.size	_ZTSN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE, 49
_ZTSN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE:
	.string	"N6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE"
	.weak	_ZTIN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE
	.section	.data.rel.ro._ZTIN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE,"awG",@progbits,_ZTIN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE,comdat
	.align 8
	.type	_ZTIN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE, @object
	.size	_ZTIN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE, 24
_ZTIN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE
	.quad	_ZTIN6icu_6717StringTrieBuilder15LinearMatchNodeE
	.weak	_ZTSN6icu_6717UCharsTrieBuilderE
	.section	.rodata._ZTSN6icu_6717UCharsTrieBuilderE,"aG",@progbits,_ZTSN6icu_6717UCharsTrieBuilderE,comdat
	.align 16
	.type	_ZTSN6icu_6717UCharsTrieBuilderE, @object
	.size	_ZTSN6icu_6717UCharsTrieBuilderE, 29
_ZTSN6icu_6717UCharsTrieBuilderE:
	.string	"N6icu_6717UCharsTrieBuilderE"
	.weak	_ZTIN6icu_6717UCharsTrieBuilderE
	.section	.data.rel.ro._ZTIN6icu_6717UCharsTrieBuilderE,"awG",@progbits,_ZTIN6icu_6717UCharsTrieBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_6717UCharsTrieBuilderE, @object
	.size	_ZTIN6icu_6717UCharsTrieBuilderE, 24
_ZTIN6icu_6717UCharsTrieBuilderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717UCharsTrieBuilderE
	.quad	_ZTIN6icu_6717StringTrieBuilderE
	.weak	_ZTVN6icu_6717UCharsTrieBuilderE
	.section	.data.rel.ro._ZTVN6icu_6717UCharsTrieBuilderE,"awG",@progbits,_ZTVN6icu_6717UCharsTrieBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_6717UCharsTrieBuilderE, @object
	.size	_ZTVN6icu_6717UCharsTrieBuilderE, 176
_ZTVN6icu_6717UCharsTrieBuilderE:
	.quad	0
	.quad	_ZTIN6icu_6717UCharsTrieBuilderE
	.quad	_ZN6icu_6717UCharsTrieBuilderD1Ev
	.quad	_ZN6icu_6717UCharsTrieBuilderD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717UCharsTrieBuilder22getElementStringLengthEi
	.quad	_ZNK6icu_6717UCharsTrieBuilder14getElementUnitEii
	.quad	_ZNK6icu_6717UCharsTrieBuilder15getElementValueEi
	.quad	_ZNK6icu_6717UCharsTrieBuilder21getLimitOfLinearMatchEiii
	.quad	_ZNK6icu_6717UCharsTrieBuilder17countElementUnitsEiii
	.quad	_ZNK6icu_6717UCharsTrieBuilder23skipElementsBySomeUnitsEiii
	.quad	_ZNK6icu_6717UCharsTrieBuilder26indexOfElementWithNextUnitEiiDs
	.quad	_ZNK6icu_6717UCharsTrieBuilder23matchNodesCanHaveValuesEv
	.quad	_ZNK6icu_6717UCharsTrieBuilder31getMaxBranchLinearSubNodeLengthEv
	.quad	_ZNK6icu_6717UCharsTrieBuilder17getMinLinearMatchEv
	.quad	_ZNK6icu_6717UCharsTrieBuilder23getMaxLinearMatchLengthEv
	.quad	_ZNK6icu_6717UCharsTrieBuilder21createLinearMatchNodeEiiiPNS_17StringTrieBuilder4NodeE
	.quad	_ZN6icu_6717UCharsTrieBuilder5writeEi
	.quad	_ZN6icu_6717UCharsTrieBuilder17writeElementUnitsEiii
	.quad	_ZN6icu_6717UCharsTrieBuilder18writeValueAndFinalEia
	.quad	_ZN6icu_6717UCharsTrieBuilder17writeValueAndTypeEaii
	.quad	_ZN6icu_6717UCharsTrieBuilder12writeDeltaToEi
	.weak	_ZTVN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE
	.section	.data.rel.ro._ZTVN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE,"awG",@progbits,_ZTVN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE,comdat
	.align 8
	.type	_ZTVN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE, @object
	.size	_ZTVN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE, 64
_ZTVN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE:
	.quad	0
	.quad	_ZTIN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeE
	.quad	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD1Ev
	.quad	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717UCharsTrieBuilder18UCTLinearMatchNodeeqERKNS_17StringTrieBuilder4NodeE
	.quad	_ZN6icu_6717StringTrieBuilder15LinearMatchNode19markRightEdgesFirstEi
	.quad	_ZN6icu_6717UCharsTrieBuilder18UCTLinearMatchNode5writeERNS_17StringTrieBuilderE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
