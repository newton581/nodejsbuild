	.file	"unifilt.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeFilter9toMatcherEv
	.type	_ZNK6icu_6713UnicodeFilter9toMatcherEv, @function
_ZNK6icu_6713UnicodeFilter9toMatcherEv:
.LFB22:
	.cfi_startproc
	endbr64
	leaq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE22:
	.size	_ZNK6icu_6713UnicodeFilter9toMatcherEv, .-_ZNK6icu_6713UnicodeFilter9toMatcherEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeFilter7setDataEPKNS_23TransliterationRuleDataE
	.type	_ZN6icu_6713UnicodeFilter7setDataEPKNS_23TransliterationRuleDataE, @function
_ZN6icu_6713UnicodeFilter7setDataEPKNS_23TransliterationRuleDataE:
.LFB23:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23:
	.size	_ZN6icu_6713UnicodeFilter7setDataEPKNS_23TransliterationRuleDataE, .-_ZN6icu_6713UnicodeFilter7setDataEPKNS_23TransliterationRuleDataE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia
	.type	_ZN6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia, @function
_ZN6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia:
.LFB24:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movl	(%rdx), %esi
	cmpl	%ecx, %esi
	jge	.L5
	movq	(%rdi), %rax
	movq	%r13, %rdi
	movq	56(%rax), %rcx
	movq	0(%r13), %rax
	movq	%rcx, -64(%rbp)
	call	*80(%rax)
	movq	-64(%rbp), %rcx
	movq	%r14, %rdi
	movl	%eax, -56(%rbp)
	movl	%eax, %esi
	call	*%rcx
	movl	-56(%rbp), %edx
	testb	%al, %al
	jne	.L6
	movl	(%rbx), %esi
.L5:
	cmpl	%esi, %r12d
	jge	.L12
	movq	(%r14), %rax
	movq	%r13, %rdi
	movq	56(%rax), %rdx
	movq	0(%r13), %rax
	movq	%rdx, -56(%rbp)
	call	*80(%rax)
	movq	-56(%rbp), %rdx
	movq	%r14, %rdi
	movl	%eax, %esi
	call	*%rdx
	testb	%al, %al
	jne	.L19
.L12:
	testb	%r15b, %r15b
	jne	.L10
	xorl	%r12d, %r12d
.L4:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	cmpl	%r12d, (%rbx)
	sete	%r12b
	movzbl	%r12b, %r12d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	cmpl	$65535, %edx
	movl	$2, %r12d
	seta	%al
	addl	$1, %eax
	addl	%eax, (%rbx)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L19:
	movl	(%rbx), %eax
	movl	$2, %r12d
	leal	-1(%rax), %esi
	movl	%esi, (%rbx)
	testl	%esi, %esi
	js	.L4
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*80(%rax)
	cmpl	$65535, %eax
	seta	%al
	movzbl	%al, %eax
	subl	%eax, (%rbx)
	jmp	.L4
	.cfi_endproc
.LFE24:
	.size	_ZN6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia, .-_ZN6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia
	.p2align 4
	.globl	_ZThn8_N6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia
	.type	_ZThn8_N6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia, @function
_ZThn8_N6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia:
.LFB27:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movl	(%rdx), %esi
	cmpl	%esi, %ecx
	jle	.L21
	movq	-8(%rdi), %rax
	movq	%rdi, -72(%rbp)
	movq	%r13, %rdi
	movq	56(%rax), %rcx
	movq	0(%r13), %rax
	movq	%rcx, -64(%rbp)
	call	*80(%rax)
	movq	-64(%rbp), %rcx
	movq	%r15, %rdi
	movl	%eax, -56(%rbp)
	movl	%eax, %esi
	call	*%rcx
	movl	-56(%rbp), %edx
	testb	%al, %al
	jne	.L22
	movl	(%rbx), %esi
	movq	-72(%rbp), %r9
.L21:
	cmpl	%esi, %r12d
	jge	.L28
	movq	-8(%r9), %rax
	movq	%r13, %rdi
	movq	56(%rax), %rdx
	movq	0(%r13), %rax
	movq	%rdx, -56(%rbp)
	call	*80(%rax)
	movq	-56(%rbp), %rdx
	movq	%r15, %rdi
	movl	%eax, %esi
	call	*%rdx
	testb	%al, %al
	jne	.L35
.L28:
	testb	%r14b, %r14b
	jne	.L26
	xorl	%r12d, %r12d
.L20:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	cmpl	%r12d, (%rbx)
	sete	%r12b
	movzbl	%r12b, %r12d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%eax, %eax
	cmpl	$65535, %edx
	movl	$2, %r12d
	seta	%al
	addl	$1, %eax
	addl	%eax, (%rbx)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L35:
	movl	(%rbx), %eax
	movl	$2, %r12d
	leal	-1(%rax), %esi
	movl	%esi, (%rbx)
	testl	%esi, %esi
	js	.L20
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*80(%rax)
	cmpl	$65535, %eax
	seta	%al
	movzbl	%al, %eax
	subl	%eax, (%rbx)
	jmp	.L20
	.cfi_endproc
.LFE27:
	.size	_ZThn8_N6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia, .-_ZThn8_N6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeFilter16getStaticClassIDEv
	.type	_ZN6icu_6713UnicodeFilter16getStaticClassIDEv, @function
_ZN6icu_6713UnicodeFilter16getStaticClassIDEv:
.LFB13:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713UnicodeFilter16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE13:
	.size	_ZN6icu_6713UnicodeFilter16getStaticClassIDEv, .-_ZN6icu_6713UnicodeFilter16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714UnicodeMatcherD2Ev
	.type	_ZN6icu_6714UnicodeMatcherD2Ev, @function
_ZN6icu_6714UnicodeMatcherD2Ev:
.LFB15:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE15:
	.size	_ZN6icu_6714UnicodeMatcherD2Ev, .-_ZN6icu_6714UnicodeMatcherD2Ev
	.globl	_ZN6icu_6714UnicodeMatcherD1Ev
	.set	_ZN6icu_6714UnicodeMatcherD1Ev,_ZN6icu_6714UnicodeMatcherD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714UnicodeMatcherD0Ev
	.type	_ZN6icu_6714UnicodeMatcherD0Ev, @function
_ZN6icu_6714UnicodeMatcherD0Ev:
.LFB17:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE17:
	.size	_ZN6icu_6714UnicodeMatcherD0Ev, .-_ZN6icu_6714UnicodeMatcherD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeFilterD2Ev
	.type	_ZN6icu_6713UnicodeFilterD2Ev, @function
_ZN6icu_6713UnicodeFilterD2Ev:
.LFB19:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeFilterE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	.cfi_endproc
.LFE19:
	.size	_ZN6icu_6713UnicodeFilterD2Ev, .-_ZN6icu_6713UnicodeFilterD2Ev
	.globl	_ZN6icu_6713UnicodeFilterD1Ev
	.set	_ZN6icu_6713UnicodeFilterD1Ev,_ZN6icu_6713UnicodeFilterD2Ev
	.p2align 4
	.globl	_ZThn8_N6icu_6713UnicodeFilterD1Ev
	.type	_ZThn8_N6icu_6713UnicodeFilterD1Ev, @function
_ZThn8_N6icu_6713UnicodeFilterD1Ev:
.LFB25:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeFilterE(%rip), %rax
	subq	$8, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	.cfi_endproc
.LFE25:
	.size	_ZThn8_N6icu_6713UnicodeFilterD1Ev, .-_ZThn8_N6icu_6713UnicodeFilterD1Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeFilterD0Ev
	.type	_ZN6icu_6713UnicodeFilterD0Ev, @function
_ZN6icu_6713UnicodeFilterD0Ev:
.LFB21:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeFilterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE21:
	.size	_ZN6icu_6713UnicodeFilterD0Ev, .-_ZN6icu_6713UnicodeFilterD0Ev
	.p2align 4
	.globl	_ZThn8_N6icu_6713UnicodeFilterD0Ev
	.type	_ZThn8_N6icu_6713UnicodeFilterD0Ev, @function
_ZThn8_N6icu_6713UnicodeFilterD0Ev:
.LFB26:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeFilterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	movq	%r12, %rdi
	call	_ZN6icu_6714UnicodeFunctorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE26:
	.size	_ZThn8_N6icu_6713UnicodeFilterD0Ev, .-_ZThn8_N6icu_6713UnicodeFilterD0Ev
	.weak	_ZTSN6icu_6714UnicodeMatcherE
	.section	.rodata._ZTSN6icu_6714UnicodeMatcherE,"aG",@progbits,_ZTSN6icu_6714UnicodeMatcherE,comdat
	.align 16
	.type	_ZTSN6icu_6714UnicodeMatcherE, @object
	.size	_ZTSN6icu_6714UnicodeMatcherE, 26
_ZTSN6icu_6714UnicodeMatcherE:
	.string	"N6icu_6714UnicodeMatcherE"
	.weak	_ZTIN6icu_6714UnicodeMatcherE
	.section	.data.rel.ro._ZTIN6icu_6714UnicodeMatcherE,"awG",@progbits,_ZTIN6icu_6714UnicodeMatcherE,comdat
	.align 8
	.type	_ZTIN6icu_6714UnicodeMatcherE, @object
	.size	_ZTIN6icu_6714UnicodeMatcherE, 16
_ZTIN6icu_6714UnicodeMatcherE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_6714UnicodeMatcherE
	.weak	_ZTSN6icu_6713UnicodeFilterE
	.section	.rodata._ZTSN6icu_6713UnicodeFilterE,"aG",@progbits,_ZTSN6icu_6713UnicodeFilterE,comdat
	.align 16
	.type	_ZTSN6icu_6713UnicodeFilterE, @object
	.size	_ZTSN6icu_6713UnicodeFilterE, 25
_ZTSN6icu_6713UnicodeFilterE:
	.string	"N6icu_6713UnicodeFilterE"
	.weak	_ZTIN6icu_6713UnicodeFilterE
	.section	.data.rel.ro._ZTIN6icu_6713UnicodeFilterE,"awG",@progbits,_ZTIN6icu_6713UnicodeFilterE,comdat
	.align 8
	.type	_ZTIN6icu_6713UnicodeFilterE, @object
	.size	_ZTIN6icu_6713UnicodeFilterE, 56
_ZTIN6icu_6713UnicodeFilterE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6713UnicodeFilterE
	.long	0
	.long	2
	.quad	_ZTIN6icu_6714UnicodeFunctorE
	.quad	2
	.quad	_ZTIN6icu_6714UnicodeMatcherE
	.quad	2050
	.weak	_ZTVN6icu_6714UnicodeMatcherE
	.section	.data.rel.ro._ZTVN6icu_6714UnicodeMatcherE,"awG",@progbits,_ZTVN6icu_6714UnicodeMatcherE,comdat
	.align 8
	.type	_ZTVN6icu_6714UnicodeMatcherE, @object
	.size	_ZTVN6icu_6714UnicodeMatcherE, 64
_ZTVN6icu_6714UnicodeMatcherE:
	.quad	0
	.quad	_ZTIN6icu_6714UnicodeMatcherE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6713UnicodeFilterE
	.section	.data.rel.ro._ZTVN6icu_6713UnicodeFilterE,"awG",@progbits,_ZTVN6icu_6713UnicodeFilterE,comdat
	.align 8
	.type	_ZTVN6icu_6713UnicodeFilterE, @object
	.size	_ZTVN6icu_6713UnicodeFilterE, 152
_ZTVN6icu_6713UnicodeFilterE:
	.quad	0
	.quad	_ZTIN6icu_6713UnicodeFilterE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6713UnicodeFilter9toMatcherEv
	.quad	_ZNK6icu_6714UnicodeFunctor10toReplacerEv
	.quad	_ZN6icu_6713UnicodeFilter7setDataEPKNS_23TransliterationRuleDataE
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia
	.quad	-8
	.quad	_ZTIN6icu_6713UnicodeFilterE
	.quad	0
	.quad	0
	.quad	_ZThn8_N6icu_6713UnicodeFilter7matchesERKNS_11ReplaceableERiia
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.local	_ZZN6icu_6713UnicodeFilter16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6713UnicodeFilter16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
