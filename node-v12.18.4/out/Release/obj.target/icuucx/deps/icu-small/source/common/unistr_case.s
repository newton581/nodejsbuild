	.file	"unistr_case.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij
	.type	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij, @function
_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij:
.LFB3127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r10d
	movq	%rcx, %rdx
	movl	%r9d, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzwl	8(%rdi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	%r9d, %r12d
	andl	$1, %r12d
	jne	.L15
	testw	%r9w, %r9w
	js	.L3
	movswl	%r9w, %eax
	sarl	$5, %eax
.L4:
	testl	%esi, %esi
	js	.L16
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	movslq	%esi, %r11
	addq	%r11, %r11
.L5:
	xorl	%r13d, %r13d
	testl	%r10d, %r10d
	js	.L6
	subl	%esi, %eax
	cmpl	%r10d, %eax
	cmovle	%eax, %r10d
	movl	%r10d, %r13d
.L6:
	xorl	%eax, %eax
	testq	%rdx, %rdx
	cmove	%eax, %ecx
	cmove	%eax, %r8d
	andl	$2, %r9d
	jne	.L25
	movq	24(%rdi), %rdi
.L9:
	addq	%r11, %rdi
	testl	%r8d, %r8d
	je	.L10
	movslq	%r8d, %rax
	leaq	(%rdx,%rax,2), %rdx
.L10:
	cmpq	%rdi, %rdx
	je	.L11
	movl	16(%rbp), %r8d
	leaq	-28(%rbp), %r9
	movl	%r13d, %esi
	movl	$0, -28(%rbp)
	orl	$65536, %r8d
	call	u_strcmpFold_67@PLT
	movl	%eax, %edx
	sarl	$24, %edx
	orl	$1, %edx
	testl	%eax, %eax
	cmovne	%edx, %r12d
.L1:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	addq	$10, %rdi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L11:
	testl	%ecx, %ecx
	js	.L27
.L13:
	cmpl	%r13d, %ecx
	je	.L1
	subl	%ecx, %r13d
	movl	%r13d, %r12d
	sarl	$24, %r12d
	orl	$1, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%r11d, %r11d
	xorl	%esi, %esi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L3:
	movl	12(%rdi), %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L27:
	movslq	%r8d, %r8
	leaq	(%rdx,%r8,2), %rdi
	call	u_strlen_67@PLT
	movl	%eax, %ecx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$-1, %r12d
	jmp	.L1
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3127:
	.size	_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij, .-_ZNK6icu_6713UnicodeString13doCaseCompareEiiPKDsiij
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE
	.type	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE, @function
_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE:
.LFB3128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$840, %rsp
	.cfi_offset 3, -56
	movl	%esi, -836(%rbp)
	movswl	8(%rdi), %ebx
	movl	%edx, -840(%rbp)
	movq	%r8, -848(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ebx, %eax
	sarl	$5, %ebx
	je	.L29
	testb	$17, %al
	jne	.L29
	movq	%rcx, %r13
	testw	%ax, %ax
	jns	.L30
	movl	12(%rdi), %ebx
.L30:
	testb	$25, %al
	je	.L31
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	$2, %edi
	movl	$0, -820(%rbp)
	movq	%rcx, -768(%rbp)
	movw	%di, -760(%rbp)
.L32:
	cmpl	$26, %ebx
	setle	%dl
	xorl	%r15d, %r15d
.L36:
	andl	$2, %eax
	testb	%dl, %dl
	je	.L37
	leaq	10(%r12), %r11
	testw	%ax, %ax
	jne	.L39
	movq	24(%r12), %r11
.L39:
	leaq	-464(%rbp), %r14
	movl	%ebx, %edx
	movq	%r11, %rsi
	movq	%r11, -856(%rbp)
	movq	%r14, %rdi
	call	u_memcpy_67@PLT
	testb	%r15b, %r15b
	je	.L40
	testb	$2, 8(%r12)
	movq	-856(%rbp), %r11
	movl	$27, %r8d
	jne	.L41
	movl	16(%r12), %r8d
.L41:
	leaq	-768(%rbp), %r15
	testq	%r13, %r13
	je	.L44
	leaq	-816(%rbp), %rdx
	movl	%ebx, %ecx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%r8d, -864(%rbp)
	movq	%r11, -856(%rbp)
	movq	%r14, -816(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	0(%r13), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*56(%rax)
	movl	-864(%rbp), %r8d
	movq	-856(%rbp), %r11
.L44:
	subq	$8, %rsp
	movl	-840(%rbp), %esi
	movq	%r14, %r9
	movq	%r11, %rcx
	leaq	-820(%rbp), %rax
	movl	-836(%rbp), %edi
	movq	%r13, %rdx
	pushq	%rax
	movq	%rax, -864(%rbp)
	movq	-848(%rbp), %rax
	pushq	$0
	pushq	%rbx
	call	*%rax
	movl	%eax, %esi
	movl	-820(%rbp), %eax
	addq	$32, %rsp
	testl	%eax, %eax
	jle	.L86
	movq	%r14, -856(%rbp)
	cmpl	$15, %eax
	jne	.L62
.L47:
	xorl	%ecx, %ecx
	movl	$1, %r9d
	movl	%esi, %edx
	movq	%r12, %rdi
	leaq	-816(%rbp), %r8
	movq	$0, -816(%rbp)
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia@PLT
	testb	%al, %al
	je	.L43
	movl	$0, -820(%rbp)
	testb	$2, 8(%r12)
	jne	.L60
	movl	16(%r12), %r8d
	movq	24(%r12), %rcx
.L64:
	subq	$8, %rsp
	pushq	-864(%rbp)
	movq	%r13, %rdx
	movl	-836(%rbp), %edi
	movq	-856(%rbp), %r9
	pushq	$0
	movl	-840(%rbp), %esi
	movq	-848(%rbp), %rax
	pushq	%rbx
	call	*%rax
	movq	-816(%rbp), %rdi
	movl	%eax, %ebx
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L61
	call	uprv_free_67@PLT
.L61:
	movl	-820(%rbp), %eax
	testl	%eax, %eax
	jg	.L62
	movzwl	8(%r12), %eax
	cmpl	$1023, %ebx
	jg	.L63
	andl	$31, %eax
	sall	$5, %ebx
	orl	%eax, %ebx
	movw	%bx, 8(%r12)
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L29:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	testw	%ax, %ax
	je	.L48
	leaq	10(%r12), %rax
	movq	%rax, -856(%rbp)
.L49:
	movdqa	.LC0(%rip), %xmm0
	leaq	-676(%rbp), %rax
	movl	$0, -680(%rbp)
	leaq	-768(%rbp), %r15
	movq	%rax, -704(%rbp)
	movups	%xmm0, -696(%rbp)
	testq	%r13, %r13
	je	.L50
	movq	-856(%rbp), %rax
	movl	%ebx, %ecx
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	-816(%rbp), %rdx
	movq	%rax, -816(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	0(%r13), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*56(%rax)
.L50:
	subq	$8, %rsp
	movl	-840(%rbp), %esi
	leaq	-820(%rbp), %rax
	leaq	-464(%rbp), %r14
	pushq	%rax
	movq	-856(%rbp), %r9
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rax, -864(%rbp)
	leaq	-704(%rbp), %rax
	movl	-836(%rbp), %edi
	orl	$16384, %esi
	pushq	%rax
	movl	$200, %r8d
	pushq	%rbx
	movq	%rax, -872(%rbp)
	movq	-848(%rbp), %rax
	call	*%rax
	movl	-820(%rbp), %eax
	addq	$32, %rsp
	testl	%eax, %eax
	jle	.L88
	cmpl	$15, %eax
	jne	.L58
	movl	-688(%rbp), %esi
	movq	-872(%rbp), %rdi
	addl	%ebx, %esi
	movl	%esi, -876(%rbp)
	call	_ZN6icu_675EditsD1Ev@PLT
	movl	-876(%rbp), %esi
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L31:
	testb	$4, %al
	jne	.L89
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	$0, -820(%rbp)
	movq	%rcx, -768(%rbp)
	movl	$2, %ecx
	movw	%cx, -760(%rbp)
.L35:
	cmpl	$54, %ebx
	movl	$1, %r15d
	setle	%dl
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L48:
	movq	24(%r12), %rax
	movq	%rax, -856(%rbp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$27, %edx
	movl	$27, %esi
	movq	%r12, %rdi
	leaq	-768(%rbp), %r15
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia@PLT
	leaq	10(%r12), %r11
	movl	$27, %r8d
	testb	%al, %al
	jne	.L41
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8refCountEv@PLT
	movl	$2, %esi
	movl	$0, -820(%rbp)
	cmpl	$1, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%si, -760(%rbp)
	movq	%rax, -768(%rbp)
	movzwl	8(%r12), %eax
	jne	.L32
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L88:
	movl	-688(%rbp), %esi
	addl	%ebx, %esi
	cmpl	%ebx, %esi
	jle	.L54
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%esi, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia@PLT
	testb	%al, %al
	je	.L55
.L54:
	movl	-692(%rbp), %edx
	movq	-704(%rbp), %rsi
	movl	$1, %ecx
	leaq	-816(%rbp), %rbx
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN6icu_675Edits8IteratorC1EPKtiaa@PLT
	movq	-864(%rbp), %r13
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L90:
	movl	-788(%rbp), %r9d
	movl	-780(%rbp), %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	-792(%rbp), %edx
	movl	-776(%rbp), %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
.L53:
	movsbl	-796(%rbp), %esi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_675Edits8Iterator4nextEaR10UErrorCode@PLT
	testb	%al, %al
	jne	.L90
	movl	-820(%rbp), %edx
	testl	%edx, %edx
	jg	.L58
.L55:
	movq	-872(%rbp), %rdi
	call	_ZN6icu_675EditsD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	10(%r12), %rcx
	movl	$27, %r8d
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L86:
	movzwl	8(%r12), %eax
	cmpl	$1023, %esi
	jg	.L46
	andl	$31, %eax
	sall	$5, %esi
	orl	%esi, %eax
	movw	%ax, 8(%r12)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L63:
	orl	$-32, %eax
	movl	%ebx, 12(%r12)
	movw	%ax, 8(%r12)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L46:
	orl	$-32, %eax
	movl	%esi, 12(%r12)
	movw	%ax, 8(%r12)
	jmp	.L43
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3128:
	.size	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE, .-_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString8foldCaseEj
	.type	_ZN6icu_6713UnicodeString8foldCaseEj, @function
_ZN6icu_6713UnicodeString8foldCaseEj:
.LFB3129:
	.cfi_startproc
	endbr64
	movq	ustrcase_internalFold_67@GOTPCREL(%rip), %r8
	movl	%esi, %edx
	xorl	%ecx, %ecx
	movl	$1, %esi
	jmp	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE
	.cfi_endproc
.LFE3129:
	.size	_ZN6icu_6713UnicodeString8foldCaseEj, .-_ZN6icu_6713UnicodeString8foldCaseEj
	.p2align 4
	.globl	uhash_hashCaselessUnicodeString_67
	.type	uhash_hashCaselessUnicodeString_67, @function
uhash_hashCaselessUnicodeString_67:
.LFB3130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L92
	leaq	-96(%rbp), %r13
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	ustrcase_internalFold_67@GOTPCREL(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE
	movq	%rax, %rdi
	call	_ZNK6icu_6713UnicodeString10doHashCodeEv@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L92:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$80, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L99:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3130:
	.size	uhash_hashCaselessUnicodeString_67, .-uhash_hashCaselessUnicodeString_67
	.p2align 4
	.globl	uhash_compareCaselessUnicodeString_67
	.type	uhash_compareCaselessUnicodeString_67, @function
uhash_compareCaselessUnicodeString_67:
.LFB3131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L120
	testq	%rdi, %rdi
	je	.L121
	testq	%rsi, %rsi
	je	.L121
	movzwl	8(%rsi), %r9d
	testw	%r9w, %r9w
	js	.L102
	movzwl	8(%rdi), %eax
	movswl	%r9w, %edx
	sarl	$5, %edx
	testw	%ax, %ax
	js	.L104
.L133:
	movswl	%ax, %r8d
	sarl	$5, %r8d
.L105:
	testb	$1, %r9b
	je	.L106
	andl	$1, %eax
.L100:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L130
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	testl	%edx, %edx
	movl	$0, %r10d
	movl	$0, %r11d
	cmovle	%edx, %r10d
	js	.L108
	movl	%edx, %r11d
	subl	%r10d, %r11d
	cmpl	%edx, %r11d
	cmovg	%edx, %r11d
.L108:
	andl	$2, %r9d
	leaq	10(%rsi), %rdx
	jne	.L110
	movq	24(%rsi), %rdx
.L110:
	testb	$1, %al
	je	.L131
	.p2align 4,,10
	.p2align 3
.L121:
	xorl	%eax, %eax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L131:
	testl	%r8d, %r8d
	movl	$0, %ecx
	movl	$0, %esi
	cmovle	%r8d, %ecx
	js	.L111
	movl	%r8d, %esi
	subl	%ecx, %esi
	cmpl	%r8d, %esi
	cmovg	%r8d, %esi
.L111:
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	cmove	%r8d, %r11d
	cmove	%r8d, %r10d
	testb	$2, %al
	je	.L113
	addq	$10, %rdi
.L114:
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx,2), %rdi
	testl	%r10d, %r10d
	je	.L115
	movslq	%r10d, %r10
	leaq	(%rdx,%r10,2), %rdx
.L115:
	cmpq	%rdx, %rdi
	je	.L132
	leaq	-12(%rbp), %r9
	movl	$65536, %r8d
	movl	%r11d, %ecx
	movl	$0, -12(%rbp)
	call	u_strcmpFold_67@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$1, %eax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L102:
	movzwl	8(%rdi), %eax
	movl	12(%rsi), %edx
	testw	%ax, %ax
	jns	.L133
.L104:
	movl	12(%rdi), %r8d
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L113:
	movq	24(%rdi), %rdi
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L132:
	cmpl	%r11d, %esi
	sete	%al
	jmp	.L100
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3131:
	.size	uhash_compareCaselessUnicodeString_67, .-uhash_compareCaselessUnicodeString_67
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	100
	.long	0
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
