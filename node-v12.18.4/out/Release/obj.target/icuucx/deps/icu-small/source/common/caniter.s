	.file	"caniter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717CanonicalIterator17getDynamicClassIDEv
	.type	_ZNK6icu_6717CanonicalIterator17getDynamicClassIDEv, @function
_ZNK6icu_6717CanonicalIterator17getDynamicClassIDEv:
.LFB3170:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6717CanonicalIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3170:
	.size	_ZNK6icu_6717CanonicalIterator17getDynamicClassIDEv, .-_ZNK6icu_6717CanonicalIterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIterator16getStaticClassIDEv
	.type	_ZN6icu_6717CanonicalIterator16getStaticClassIDEv, @function
_ZN6icu_6717CanonicalIterator16getStaticClassIDEv:
.LFB3169:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6717CanonicalIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE3169:
	.size	_ZN6icu_6717CanonicalIterator16getStaticClassIDEv, .-_ZN6icu_6717CanonicalIterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIterator11cleanPiecesEv
	.type	_ZN6icu_6717CanonicalIterator11cleanPiecesEv, @function
_ZN6icu_6717CanonicalIterator11cleanPiecesEv:
.LFB3178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	80(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	movl	88(%r14), %edx
	testl	%edx, %edx
	jle	.L6
	xorl	%r12d, %r12d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$1, %r12
	cmpl	%r12d, %edx
	jle	.L6
.L10:
	movq	(%rdi,%r12,8), %rax
	leaq	0(,%r12,8), %r13
	testq	%rax, %rax
	je	.L7
	movq	-8(%rax), %rbx
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	movq	80(%r14), %rax
	cmpq	%rbx, (%rax,%r13)
	jne	.L9
.L8:
	leaq	-8(%rbx), %rdi
	addq	$1, %r12
	call	_ZN6icu_677UMemorydaEPv@PLT
	movl	88(%r14), %edx
	movq	80(%r14), %rdi
	cmpl	%r12d, %edx
	jg	.L10
.L6:
	call	uprv_free_67@PLT
	movq	$0, 80(%r14)
	movl	$0, 88(%r14)
.L5:
	movq	96(%r14), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	uprv_free_67@PLT
	movq	$0, 96(%r14)
.L11:
	movq	104(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4
	call	uprv_free_67@PLT
	movq	$0, 104(%r14)
	movl	$0, 112(%r14)
.L4:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3178:
	.size	_ZN6icu_6717CanonicalIterator11cleanPiecesEv, .-_ZN6icu_6717CanonicalIterator11cleanPiecesEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIteratorD2Ev
	.type	_ZN6icu_6717CanonicalIteratorD2Ev, @function
_ZN6icu_6717CanonicalIteratorD2Ev:
.LFB3175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CanonicalIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CanonicalIterator11cleanPiecesEv
	leaq	120(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3175:
	.size	_ZN6icu_6717CanonicalIteratorD2Ev, .-_ZN6icu_6717CanonicalIteratorD2Ev
	.globl	_ZN6icu_6717CanonicalIteratorD1Ev
	.set	_ZN6icu_6717CanonicalIteratorD1Ev,_ZN6icu_6717CanonicalIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIteratorD0Ev
	.type	_ZN6icu_6717CanonicalIteratorD0Ev, @function
_ZN6icu_6717CanonicalIteratorD0Ev:
.LFB3177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717CanonicalIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6717CanonicalIterator11cleanPiecesEv
	leaq	120(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3177:
	.size	_ZN6icu_6717CanonicalIteratorD0Ev, .-_ZN6icu_6717CanonicalIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIterator9getSourceEv
	.type	_ZN6icu_6717CanonicalIterator9getSourceEv, @function
_ZN6icu_6717CanonicalIterator9getSourceEv:
.LFB3179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3179:
	.size	_ZN6icu_6717CanonicalIterator9getSourceEv, .-_ZN6icu_6717CanonicalIterator9getSourceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIterator5resetEv
	.type	_ZN6icu_6717CanonicalIterator5resetEv, @function
_ZN6icu_6717CanonicalIterator5resetEv:
.LFB3180:
	.cfi_startproc
	endbr64
	movl	112(%rdi), %eax
	movb	$0, 72(%rdi)
	testl	%eax, %eax
	jle	.L34
	movq	104(%rdi), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$0, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 112(%rdi)
	jg	.L36
.L34:
	ret
	.cfi_endproc
.LFE3180:
	.size	_ZN6icu_6717CanonicalIterator5resetEv, .-_ZN6icu_6717CanonicalIterator5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIterator4nextEv
	.type	_ZN6icu_6717CanonicalIterator4nextEv, @function
_ZN6icu_6717CanonicalIterator4nextEv:
.LFB3181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	120(%rsi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 72(%rsi)
	jne	.L57
	movzwl	128(%rsi), %edx
	movq	%rsi, %r13
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	xorl	%ebx, %ebx
	movw	%ax, 128(%rsi)
	movl	88(%rsi), %eax
	testl	%eax, %eax
	jg	.L42
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L58:
	sarl	$5, %ecx
.L56:
	xorl	%edx, %edx
	movq	%r14, %rdi
	addq	$1, %rbx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	cmpl	%ebx, 88(%r13)
	jle	.L46
.L42:
	movq	104(%r13), %rdx
	movq	80(%r13), %rax
	movslq	(%rdx,%rbx,4), %rsi
	salq	$6, %rsi
	addq	(%rax,%rbx,8), %rsi
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	jns	.L58
	movl	12(%rsi), %ecx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L46:
	movl	112(%r13), %eax
	subl	$1, %eax
	js	.L43
	movq	104(%r13), %rcx
	movq	96(%r13), %rsi
	cltq
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$0, (%rcx,%rax,4)
	subq	$1, %rax
	testl	%eax, %eax
	js	.L43
.L49:
	movl	(%rcx,%rax,4), %edi
	leal	1(%rdi), %edx
	movl	%edx, (%rcx,%rax,4)
	cmpl	(%rsi,%rax,4), %edx
	jge	.L59
.L48:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movb	$1, 72(%r13)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3181:
	.size	_ZN6icu_6717CanonicalIterator4nextEv, .-_ZN6icu_6717CanonicalIterator4nextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIterator7permuteERNS_13UnicodeStringEaPNS_9HashtableER10UErrorCode
	.type	_ZN6icu_6717CanonicalIterator7permuteERNS_13UnicodeStringEaPNS_9HashtableER10UErrorCode, @function
_ZN6icu_6717CanonicalIterator7permuteERNS_13UnicodeStringEaPNS_9HashtableER10UErrorCode:
.LFB3183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -264(%rbp)
	movl	(%rcx), %r8d
	movq	%rdx, -248(%rbp)
	movb	%sil, -253(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L60
	movswl	8(%rdi), %eax
	movq	%rcx, %r12
	testw	%ax, %ax
	js	.L63
	sarl	$5, %eax
	cmpl	$2, %eax
	jle	.L123
.L65:
	movq	$0, -224(%rbp)
.L68:
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %r8
	leaq	-216(%rbp), %r14
	movq	%r14, %rdi
	call	uhash_init_67@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L124
.L90:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	uhash_close_67@PLT
.L60:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L125
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	12(%rdi), %eax
	cmpl	$2, %eax
	jg	.L65
.L123:
	movq	-264(%rbp), %rdi
	xorl	%esi, %esi
	movl	$2147483647, %edx
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	cmpl	$1, %eax
	jle	.L66
	movq	$0, -224(%rbp)
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L60
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L124:
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r14, %rdi
	movq	%r14, -224(%rbp)
	call	uhash_setKeyDeleter_67@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L90
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	-224(%rbp), %rdi
	call	uhash_setValueDeleter_67@PLT
	leaq	-128(%rbp), %rax
	movl	$0, -252(%rbp)
	movq	%rax, -272(%rbp)
	movq	-264(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	js	.L72
	.p2align 4,,10
	.p2align 3
.L127:
	sarl	$5, %eax
.L73:
	movl	-252(%rbp), %r14d
	cmpl	%eax, %r14d
	jge	.L90
	movq	-264(%rbp), %r15
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	-272(%rbp), %rdi
	movq	%r15, %rsi
	movl	$-1, -228(%rbp)
	movl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	cmpb	$0, -253(%rbp)
	je	.L77
	testl	%r14d, %r14d
	je	.L77
	movl	%ebx, %edi
	call	u_getCombiningClass_67@PLT
	testb	%al, %al
	je	.L88
.L77:
	movq	-224(%rbp), %rdi
	call	uhash_removeAll_67@PLT
	xorl	%edx, %edx
	cmpl	$65535, %ebx
	movl	-252(%rbp), %esi
	seta	%dl
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-272(%rbp), %rdi
	movsbl	-253(%rbp), %r14d
	addl	$1, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	leaq	-224(%rbp), %rdx
	movq	%r12, %rcx
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6717CanonicalIterator7permuteERNS_13UnicodeStringEaPNS_9HashtableER10UErrorCode
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L82
	leaq	-228(%rbp), %r14
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L126:
	sarl	$5, %ecx
.L86:
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L87
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	-248(%rbp), %rax
	movq	(%rax), %rdi
	call	uhash_put_67@PLT
.L119:
	movq	-224(%rbp), %rdi
	movq	%r14, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	je	.L88
	movl	$64, %edi
	movq	8(%rax), %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L84
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movswl	8(%r13), %ecx
	testw	%cx, %cx
	jns	.L126
	movl	12(%r13), %ecx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L88:
	movq	-272(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	xorl	%eax, %eax
	cmpl	$65535, %ebx
	movl	-252(%rbp), %ebx
	seta	%al
	leal	1(%rbx,%rax), %eax
	movl	%eax, -252(%rbp)
	movq	-264(%rbp), %rax
	movswl	8(%rax), %eax
	testw	%ax, %ax
	jns	.L127
.L72:
	movq	-264(%rbp), %rax
	movl	12(%rax), %eax
	jmp	.L73
.L84:
	movl	$7, (%r12)
.L82:
	movq	-272(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L90
.L66:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L69
	movq	-264(%rbp), %r15
	movq	%rax, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L70
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L70:
	movq	-248(%rbp), %rax
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	(%rax), %rdi
	call	uhash_put_67@PLT
	jmp	.L60
.L87:
	movq	-248(%rbp), %rax
	movq	%r12, %rcx
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	(%rax), %rdi
	call	uhash_put_67@PLT
	jmp	.L119
.L69:
	movl	$7, (%r12)
	jmp	.L60
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3183:
	.size	_ZN6icu_6717CanonicalIterator7permuteERNS_13UnicodeStringEaPNS_9HashtableER10UErrorCode, .-_ZN6icu_6717CanonicalIterator7permuteERNS_13UnicodeStringEaPNS_9HashtableER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode.part.0, @function
_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode.part.0:
.LFB4118:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$680, %rsp
	movq	%rdi, -672(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -640(%rbp)
	movq	%rdx, %rsi
	movq	%rdx, -656(%rbp)
	movl	%ecx, %edx
	movl	%ecx, -648(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -712(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L129
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L129:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L130
	movq	-712(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L130:
	movq	-640(%rbp), %rax
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	uhash_put_67@PLT
	leaq	-256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	leaq	-608(%rbp), %rax
	movl	-648(%rbp), %r11d
	movl	$0, -644(%rbp)
	movq	%rax, -664(%rbp)
	leaq	-536(%rbp), %rax
	movq	%rax, -696(%rbp)
	leaq	-544(%rbp), %rax
	movq	%rax, -688(%rbp)
	testl	%r11d, %r11d
	jle	.L158
	.p2align 4,,10
	.p2align 3
.L131:
	movslq	-644(%rbp), %rax
	movq	-656(%rbp), %rdx
	movzwl	(%rdx,%rax,2), %edx
	leaq	(%rax,%rax), %rcx
	movl	%edx, -676(%rbp)
	movl	%edx, %eax
	andl	$63488, %edx
	cmpl	$55296, %edx
	je	.L195
.L134:
	movq	-672(%rbp), %rax
	movq	-704(%rbp), %rdx
	movl	-676(%rbp), %esi
	movq	192(%rax), %rdi
	call	_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEiRNS_10UnicodeSetE@PLT
	testb	%al, %al
	jne	.L196
.L137:
	cmpl	$65535, -676(%rbp)
	ja	.L157
	addl	$1, -644(%rbp)
	movl	-644(%rbp), %eax
	cmpl	%eax, -648(%rbp)
	jg	.L131
.L158:
	movl	(%rbx), %r10d
	movq	-640(%rbp), %r15
	movl	$0, %r8d
	testl	%r10d, %r10d
	cmovg	%r8, %r15
.L133:
	movq	-704(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSetD1Ev@PLT
	movq	-712(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	addl	$2, -644(%rbp)
	movl	-644(%rbp), %eax
	cmpl	%eax, -648(%rbp)
	jg	.L131
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L195:
	testb	$4, %ah
	jne	.L135
	movl	-644(%rbp), %eax
	addl	$1, %eax
	cmpl	%eax, -648(%rbp)
	je	.L134
	movq	-656(%rbp), %rax
	movzwl	2(%rax,%rcx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L134
	movl	-676(%rbp), %edx
	sall	$10, %edx
	leal	-56613888(%rax,%rdx), %eax
	movl	%eax, -676(%rbp)
	jmp	.L134
.L196:
	movq	-704(%rbp), %rsi
	movq	-664(%rbp), %rdi
	call	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-664(%rbp), %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	je	.L138
.L200:
	movl	(%rbx), %r8d
	movl	-600(%rbp), %r12d
	movq	$0, -544(%rbp)
	testl	%r8d, %r8d
	jle	.L198
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	xorl	%edi, %edi
.L139:
	call	uhash_setValueDeleter_67@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L194
	subq	$8, %rsp
	movq	-656(%rbp), %rcx
	movl	-644(%rbp), %r9d
	movl	%r12d, %edx
	movl	-648(%rbp), %r8d
	movq	-688(%rbp), %rsi
	pushq	%rbx
	movq	-672(%rbp), %rdi
	call	_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L194
	movl	-644(%rbp), %edx
	movq	-656(%rbp), %rsi
	leaq	-384(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	leaq	-320(%rbp), %r12
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	leaq	-612(%rbp), %rax
	movq	-544(%rbp), %rdi
	movl	$-1, -612(%rbp)
	movq	%rax, %rsi
	movq	%rax, -632(%rbp)
	call	uhash_nextElement_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L155
	.p2align 4,,10
	.p2align 3
.L148:
	movq	8(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L150
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movswl	-312(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	%ecx, %eax
	sarl	$5, %ecx
	testw	%ax, %ax
	cmovs	-308(%rbp), %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L199
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
.L193:
	movq	-640(%rbp), %rax
	movq	(%rax), %rdi
	call	uhash_put_67@PLT
	movq	-544(%rbp), %rdi
	movq	-632(%rbp), %rsi
	call	uhash_nextElement_67@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r15, %r15
	jne	.L148
.L155:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L194:
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	uhash_close_67@PLT
	movq	-664(%rbp), %rdi
	call	_ZN6icu_6718UnicodeSetIterator4nextEv@PLT
	testb	%al, %al
	jne	.L200
.L138:
	movq	-664(%rbp), %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L198:
	movq	-696(%rbp), %rdi
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rsi
	call	uhash_init_67@PLT
	movl	(%rbx), %edi
	testl	%edi, %edi
	jle	.L140
	movq	-544(%rbp), %rdi
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L140:
	movq	-696(%rbp), %rdi
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rdi, -544(%rbp)
	movq	%rsi, -632(%rbp)
	call	uhash_setKeyDeleter_67@PLT
	movq	-544(%rbp), %rdi
	movq	-632(%rbp), %rsi
	jmp	.L139
.L135:
	movl	-644(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L134
	movq	-656(%rbp), %rax
	movzwl	-2(%rax,%rcx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L134
	movl	-676(%rbp), %edx
	sall	$10, %eax
	leal	-56613888(%rdx,%rax), %eax
	movl	%eax, -676(%rbp)
	jmp	.L134
.L199:
	movq	%rbx, %rcx
	movq	%r15, %rdx
	xorl	%esi, %esi
	jmp	.L193
.L150:
	movl	$7, (%rbx)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	uhash_close_67@PLT
.L161:
	movq	-664(%rbp), %rdi
	call	_ZN6icu_6718UnicodeSetIteratorD1Ev@PLT
	jmp	.L133
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4118:
	.size	_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode.part.0, .-_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode
	.type	_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode, @function
_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode:
.LFB3185:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L202
	jmp	_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L202:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3185:
	.size	_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode, .-_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIterator14getEquivalentsERKNS_13UnicodeStringERiR10UErrorCode
	.type	_ZN6icu_6717CanonicalIterator14getEquivalentsERKNS_13UnicodeStringERiR10UErrorCode, @function
_ZN6icu_6717CanonicalIterator14getEquivalentsERKNS_13UnicodeStringERiR10UErrorCode:
.LFB3184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1096, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -1080(%rbp)
	movq	%rdx, -1136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	movq	$0, -1056(%rbp)
	testl	%eax, %eax
	jle	.L287
.L204:
	movq	$0, -960(%rbp)
.L236:
	xorl	%r12d, %r12d
.L206:
	movq	-960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L232
	call	uhash_close_67@PLT
.L232:
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L203
	call	uhash_close_67@PLT
.L203:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L288
	addq	$1096, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L287:
	.cfi_restore_state
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %r13
	movq	%rcx, %r14
	movq	%rcx, %r8
	movq	%rdi, %r15
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rbx
	leaq	-1048(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%rdi, -1088(%rbp)
	movq	%rbx, %rsi
	call	uhash_init_67@PLT
	movl	(%r14), %eax
	movq	-1088(%rbp), %rdi
	testl	%eax, %eax
	jg	.L204
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %r12
	movq	%rdi, -1056(%rbp)
	movq	%r12, %rsi
	call	uhash_setKeyDeleter_67@PLT
	movl	(%r14), %eax
	movq	$0, -960(%rbp)
	testl	%eax, %eax
	jg	.L236
	leaq	-952(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %r8
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rdi, -1088(%rbp)
	call	uhash_init_67@PLT
	movl	(%r14), %eax
	movq	-1088(%rbp), %rdi
	testl	%eax, %eax
	jg	.L236
	movq	%r12, %rsi
	movq	%rdi, -960(%rbp)
	call	uhash_setKeyDeleter_67@PLT
	movl	(%r14), %eax
	movq	$0, -864(%rbp)
	testl	%eax, %eax
	jg	.L236
	leaq	-856(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %r8
	movq	%r13, %rdx
	leaq	-864(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rdi, -1088(%rbp)
	movq	%rax, -1096(%rbp)
	call	uhash_init_67@PLT
	movl	(%r14), %r11d
	movq	-1088(%rbp), %rdi
	testl	%r11d, %r11d
	jle	.L207
.L286:
	xorl	%r12d, %r12d
.L208:
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L206
	call	uhash_close_67@PLT
	jmp	.L206
.L207:
	movq	%r12, %rsi
	movq	%rdi, -864(%rbp)
	call	uhash_setKeyDeleter_67@PLT
	movl	(%r14), %r10d
	testl	%r10d, %r10d
	jg	.L286
	movq	-1056(%rbp), %rdi
	movq	%r12, %rsi
	call	uhash_setValueDeleter_67@PLT
	movq	-960(%rbp), %rdi
	movq	%r12, %rsi
	call	uhash_setValueDeleter_67@PLT
	movq	-864(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-576(%rbp), %r12
	call	uhash_setValueDeleter_67@PLT
	leaq	-1064(%rbp), %rax
	movq	%r14, %rcx
	movq	-1080(%rbp), %rdi
	movl	$256, %edx
	movq	%rax, %rsi
	movq	%r12, -1064(%rbp)
	movq	%rax, -1088(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %ecx
	movl	(%r14), %r9d
	testl	%r9d, %r9d
	jg	.L210
	movq	-1096(%rbp), %rsi
	movq	%r14, %r8
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode.part.0
.L210:
	leaq	-1068(%rbp), %rax
	movq	-864(%rbp), %rdi
	movl	$-1, -1068(%rbp)
	movq	%rax, %rsi
	movq	%rax, -1120(%rbp)
	call	uhash_nextElement_67@PLT
	movq	%rax, %rbx
	leaq	-768(%rbp), %rax
	movq	%rax, -1112(%rbp)
	leaq	-960(%rbp), %rax
	movq	%rax, -1128(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -1096(%rbp)
	testq	%rbx, %rbx
	je	.L214
	.p2align 4,,10
	.p2align 3
.L211:
	movq	8(%rbx), %rsi
	movq	-1112(%rbp), %rbx
	leaq	-640(%rbp), %r12
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-960(%rbp), %rdi
	call	uhash_removeAll_67@PLT
	movq	%rbx, %rdi
	movq	%r14, %rcx
	movl	$1, %esi
	movq	-1128(%rbp), %rdx
	leaq	-704(%rbp), %rbx
	call	_ZN6icu_6717CanonicalIterator7permuteERNS_13UnicodeStringEaPNS_9HashtableER10UErrorCode
	movq	-1088(%rbp), %rsi
	movq	-960(%rbp), %rdi
	movl	$-1, -1064(%rbp)
	call	uhash_nextElement_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L213
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L289:
	movq	-1080(%rbp), %rax
	movzwl	8(%rax), %eax
	movw	%ax, -1104(%rbp)
	movzbl	-1104(%rbp), %eax
	andl	$1, %eax
.L216:
	testb	%al, %al
	je	.L221
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L222
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L222:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L223
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -1104(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-1104(%rbp), %r8
.L223:
	movq	-1056(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	uhash_put_67@PLT
.L221:
	movq	-1088(%rbp), %rsi
	movq	-960(%rbp), %rdi
	call	uhash_nextElement_67@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%r13, %r13
	je	.L224
.L213:
	movq	8(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	$2, %edi
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	-1096(%rbp), %rax
	movw	%di, -632(%rbp)
	movq	%rbx, %rsi
	movq	184(%r15), %rdi
	movq	%rax, -640(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	movswl	-632(%rbp), %edx
	testb	$1, %dl
	jne	.L289
	testw	%dx, %dx
	js	.L217
	movq	-1080(%rbp), %rax
	sarl	$5, %edx
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	js	.L219
.L290:
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L220:
	testb	$1, %al
	jne	.L221
	cmpl	%edx, %ecx
	jne	.L221
	movq	-1080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-1080(%rbp), %rax
	movl	-628(%rbp), %edx
	movzwl	8(%rax), %eax
	testw	%ax, %ax
	jns	.L290
.L219:
	movq	-1080(%rbp), %rcx
	movl	12(%rcx), %ecx
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L224:
	movq	-864(%rbp), %rdi
	movq	-1120(%rbp), %rsi
	call	uhash_nextElement_67@PLT
	movq	-1112(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	testq	%rbx, %rbx
	jne	.L211
.L214:
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jg	.L286
	movq	-1056(%rbp), %rdi
	call	uhash_count_67@PLT
	testl	%eax, %eax
	je	.L225
	movslq	%eax, %rbx
	movabsq	$144115188075855871, %rax
	cmpq	%rax, %rbx
	ja	.L226
	movq	%rbx, %rdi
	salq	$6, %rdi
	addq	$8, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L228
	movq	%rbx, (%rax)
	addq	$8, %r12
	leaq	-1(%rbx), %rax
.L234:
	movq	%r12, %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$2, %esi
	subq	$1, %rax
	movq	%rcx, (%rdx)
	addq	$64, %rdx
	movw	%si, -56(%rdx)
	cmpq	$-1, %rax
	jne	.L230
.L229:
	movq	-1136(%rbp), %rax
	movq	-1120(%rbp), %rsi
	movl	$-1, -1068(%rbp)
	movq	-1056(%rbp), %rdi
	movl	$0, (%rax)
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	je	.L208
	movq	-1136(%rbp), %rbx
	movq	-1120(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L231:
	movslq	(%rbx), %rdi
	movq	8(%rax), %rsi
	leal	1(%rdi), %edx
	movq	%rdi, %rax
	movl	%edx, (%rbx)
	salq	$6, %rax
	leaq	(%r12,%rax), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-1056(%rbp), %rdi
	movq	%r13, %rsi
	call	uhash_nextElement_67@PLT
	testq	%rax, %rax
	jne	.L231
	jmp	.L208
.L226:
	movq	$-1, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L228
	movq	%rbx, (%rax)
	movq	%rbx, %rax
	addq	$8, %r12
	subq	$1, %rax
	jns	.L234
	jmp	.L229
.L225:
	movl	$1, (%r14)
	xorl	%r12d, %r12d
	jmp	.L208
.L228:
	movl	$7, (%r14)
	jmp	.L286
.L288:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3184:
	.size	_ZN6icu_6717CanonicalIterator14getEquivalentsERKNS_13UnicodeStringERiR10UErrorCode, .-_ZN6icu_6717CanonicalIterator14getEquivalentsERKNS_13UnicodeStringERiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIterator9setSourceERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6717CanonicalIterator9setSourceERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6717CanonicalIterator9setSourceERKNS_13UnicodeStringER10UErrorCode:
.LFB3182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	8(%r12), %r14
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r14, %rdx
	subq	$56, %rsp
	movq	184(%rdi), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	0(%r13), %r8d
	testl	%r8d, %r8d
	jle	.L346
.L291:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movb	$0, 72(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_6717CanonicalIterator11cleanPiecesEv
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L293
	sarl	$5, %eax
	testl	%eax, %eax
	je	.L347
.L295:
	movswl	16(%r12), %eax
	testw	%ax, %ax
	js	.L301
	sarl	$5, %eax
	movslq	%eax, %rbx
.L302:
	movq	%rbx, %rdi
	salq	$6, %rdi
	addq	$8, %rdi
.L303:
	call	_ZN6icu_677UMemorynaEm@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L345
	leaq	8(%rax), %rdx
	movq	%rbx, (%rax)
	movq	%rbx, %rax
	movq	%rdx, -56(%rbp)
	subq	$1, %rax
	js	.L304
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L305:
	movl	$2, %esi
	subq	$1, %rax
	movq	%rcx, (%rdx)
	addq	$64, %rdx
	movw	%si, -56(%rdx)
	cmpq	$-1, %rax
	jne	.L305
.L304:
	xorl	%esi, %esi
	movq	%r14, %rdi
	xorl	%ebx, %ebx
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	$0, -72(%rbp)
	cmpl	$65535, %eax
	movl	$1, -84(%rbp)
	seta	%bl
	xorl	%r8d, %r8d
	addl	$1, %ebx
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L348:
	movq	-72(%rbp), %r11
	sarl	$5, %eax
	salq	$6, %r11
	addq	-56(%rbp), %r11
	cmpl	%eax, %ebx
	jge	.L308
.L349:
	movl	%ebx, %esi
	movq	%r14, %rdi
	movl	%r8d, -64(%rbp)
	movq	%r11, -80(%rbp)
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movq	192(%r12), %rdi
	movl	%eax, %esi
	movl	%eax, %r15d
	call	_ZNK6icu_6715Normalizer2Impl21isCanonSegmentStarterEi@PLT
	movl	-64(%rbp), %r8d
	testb	%al, %al
	je	.L309
	movq	-80(%rbp), %r11
	movl	%ebx, %r9d
	subl	%r8d, %r9d
	movswl	8(%r11), %edx
	testw	%dx, %dx
	js	.L310
	sarl	$5, %edx
.L311:
	xorl	%esi, %esi
	movq	%r14, %rcx
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movslq	-84(%rbp), %rsi
	movl	%ebx, %r8d
	movq	%rsi, %rax
	movq	%rsi, -72(%rbp)
	addl	$1, %eax
	movl	%eax, -84(%rbp)
.L309:
	xorl	%eax, %eax
	cmpl	$65535, %r15d
	seta	%al
	leal	1(%rbx,%rax), %ebx
.L313:
	movswl	16(%r12), %eax
	testw	%ax, %ax
	jns	.L348
	movq	-72(%rbp), %r11
	movl	20(%r12), %eax
	salq	$6, %r11
	addq	-56(%rbp), %r11
	cmpl	%eax, %ebx
	jl	.L349
.L308:
	movzwl	8(%r11), %eax
	subl	%r8d, %ebx
	movl	%ebx, %r9d
	testw	%ax, %ax
	js	.L314
	movswl	%ax, %edx
	sarl	$5, %edx
.L315:
	movq	%r14, %rcx
	movq	%r11, %rdi
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movslq	-84(%rbp), %r14
	leaq	0(,%r14,8), %rdi
	movq	%r14, %rbx
	call	uprv_malloc_67@PLT
	movl	%r14d, 88(%r12)
	salq	$2, %r14
	movq	%rax, 80(%r12)
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	movq	%r14, %rdi
	movq	%rax, 96(%r12)
	call	uprv_malloc_67@PLT
	movq	80(%r12), %rcx
	movl	%ebx, 112(%r12)
	movq	%rax, 104(%r12)
	movq	%rax, %rdi
	testq	%rcx, %rcx
	je	.L316
	movq	96(%r12), %r8
	testq	%rax, %rax
	je	.L316
	testq	%r8, %r8
	je	.L316
	movq	-72(%rbp), %rax
	xorl	%esi, %esi
	movq	%rcx, -80(%rbp)
	movq	%r8, -64(%rbp)
	leaq	4(,%rax,4), %rdx
	call	memset@PLT
	movl	88(%r12), %eax
	testl	%eax, %eax
	jle	.L320
	movq	-56(%rbp), %r14
	movq	-80(%rbp), %rcx
	xorl	%ebx, %ebx
	movq	-64(%rbp), %r8
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L350:
	movq	96(%r12), %r8
	movq	80(%r12), %rcx
.L321:
	leaq	(%rcx,%rbx,8), %r9
	leaq	(%r8,%rbx,4), %rdx
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	addq	$1, %rbx
	addq	$64, %r14
	call	_ZN6icu_6717CanonicalIterator14getEquivalentsERKNS_13UnicodeStringERiR10UErrorCode
	movq	-64(%rbp), %r9
	movq	%rax, (%r9)
	cmpl	%ebx, 88(%r12)
	jg	.L350
.L320:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rbx
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L323
	.p2align 4,,10
	.p2align 3
.L322:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, -56(%rbp)
	jne	.L322
.L323:
	movq	-96(%rbp), %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydaEPv@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movabsq	$144115188075855871, %rax
	movslq	20(%r12), %rbx
	cmpq	%rax, %rbx
	jbe	.L302
	movq	$-1, %rdi
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L310:
	movl	12(%r11), %edx
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L293:
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jne	.L295
.L347:
	movl	$8, %edi
	call	uprv_malloc_67@PLT
	movl	$4, %edi
	movq	%rax, 80(%r12)
	call	uprv_malloc_67@PLT
	movl	$4, %edi
	movl	$1, 88(%r12)
	movq	%rax, 96(%r12)
	call	uprv_malloc_67@PLT
	cmpq	$0, 80(%r12)
	movl	$1, 112(%r12)
	movq	%rax, 104(%r12)
	je	.L345
	cmpq	$0, 96(%r12)
	je	.L345
	testq	%rax, %rax
	je	.L345
	movl	$0, (%rax)
	movl	$72, %edi
	call	_ZN6icu_677UMemorynaEm@PLT
	testq	%rax, %rax
	je	.L300
	movq	80(%r12), %rdx
	movl	$2, %edi
	addq	$8, %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movq	$1, -8(%rax)
	movq	%rsi, (%rax)
	movw	%di, 8(%rax)
	movq	%rax, (%rdx)
	movq	96(%r12), %rax
	movl	$1, (%rax)
	jmp	.L291
.L300:
	movq	80(%r12), %rax
	movq	$0, (%rax)
	movq	96(%r12), %rax
	movl	$1, (%rax)
.L345:
	movl	$7, 0(%r13)
.L299:
	addq	$56, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717CanonicalIterator11cleanPiecesEv
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movq	-96(%rbp), %rax
	movl	$7, 0(%r13)
	movq	(%rax), %rax
	movq	%rax, %rbx
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L324
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, -56(%rbp)
	jne	.L319
.L324:
	movq	-96(%rbp), %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L314:
	movl	12(%r11), %edx
	jmp	.L315
	.cfi_endproc
.LFE3182:
	.size	_ZN6icu_6717CanonicalIterator9setSourceERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6717CanonicalIterator9setSourceERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIteratorC2ERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6717CanonicalIteratorC2ERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6717CanonicalIteratorC2ERKNS_13UnicodeStringER10UErrorCode:
.LFB3172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	16+_ZTVN6icu_6717CanonicalIteratorE(%rip), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movl	$2, %ecx
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movl	$2, %esi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movw	%cx, 16(%rdi)
	movw	%si, 128(%rdi)
	movq	$0, 80(%rdi)
	movl	$0, 88(%rdi)
	movl	$0, 112(%rdi)
	movq	%rax, 120(%rdi)
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 96(%rdi)
	movq	%rdx, %rdi
	call	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode@PLT
	movq	%r13, %rdi
	movq	%rax, 184(%r12)
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	movl	0(%r13), %edi
	movq	%rax, 192(%r12)
	testl	%edi, %edi
	jle	.L359
.L351:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6715Normalizer2Impl19ensureCanonIterDataER10UErrorCode@PLT
	testb	%al, %al
	je	.L351
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717CanonicalIterator9setSourceERKNS_13UnicodeStringER10UErrorCode
	.cfi_endproc
.LFE3172:
	.size	_ZN6icu_6717CanonicalIteratorC2ERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6717CanonicalIteratorC2ERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6717CanonicalIteratorC1ERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6717CanonicalIteratorC1ERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6717CanonicalIteratorC2ERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode.part.0, @function
_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode.part.0:
.LFB4119:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-256(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$280, %rsp
	movq	%rdi, -288(%rbp)
	movq	%r13, %rdi
	movq	16(%rbp), %r14
	movq	%rsi, -304(%rbp)
	movl	%edx, %esi
	movl	%r9d, -292(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movswl	-248(%rbp), %eax
	testw	%ax, %ax
	js	.L361
	sarl	$5, %eax
	movl	%eax, -296(%rbp)
.L362:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r8d
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%rax, -192(%rbp)
	movq	-288(%rbp), %rax
	leaq	-192(%rbp), %r15
	movw	%r8w, -184(%rbp)
	movq	%r15, %rdx
	movq	184(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	(%r14), %r9d
	testl	%r9d, %r9d
	jg	.L428
	movswl	-184(%rbp), %eax
	testb	$1, %al
	jne	.L429
	testb	$17, %al
	jne	.L392
	leaq	-182(%rbp), %rdx
	testb	$2, %al
	cmove	-168(%rbp), %rdx
	movq	%rdx, -272(%rbp)
.L366:
	testw	%ax, %ax
	js	.L367
	sarl	$5, %eax
	movl	%eax, -264(%rbp)
.L368:
	movq	-272(%rbp), %rax
	movzwl	(%rax), %ecx
	movl	%ecx, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L394
	cmpl	$1, -264(%rbp)
	jne	.L430
.L394:
	movl	$1, %r8d
.L369:
	movl	-292(%rbp), %eax
	cmpl	%eax, %ebx
	jle	.L428
	movq	%r14, -320(%rbp)
	movl	%ecx, %r14d
	movq	%r15, -312(%rbp)
	movl	%r8d, %r15d
	movq	%r13, -280(%rbp)
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L434:
	cmpl	%r15d, -264(%rbp)
	je	.L431
	movq	-272(%rbp), %rcx
	movslq	%r15d, %rax
	leal	1(%r15), %edx
	leaq	(%rax,%rax), %rsi
	movzwl	(%rcx,%rax,2), %r14d
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L397
	cmpl	%edx, -264(%rbp)
	jne	.L432
.L397:
	movl	%edx, %r15d
.L377:
	cmpl	%r13d, %ebx
	jle	.L425
.L435:
	movl	%r13d, %eax
.L371:
	movslq	%eax, %rdx
	leal	1(%rax), %r13d
	movzwl	(%r12,%rdx,2), %esi
	leaq	(%rdx,%rdx), %r11
	movl	%esi, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L372
	cmpl	%r13d, %ebx
	jne	.L433
.L372:
	cmpl	%r14d, %esi
	je	.L434
	movq	-280(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	cmpl	%r13d, %ebx
	jg	.L435
.L425:
	movq	-280(%rbp), %r13
	movq	-312(%rbp), %r15
.L428:
	xorl	%r12d, %r12d
.L364:
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L436
	addq	$280, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movzwl	2(%r12,%r11), %edx
	movl	%edx, %r11d
	andl	$-1024, %r11d
	cmpl	$56320, %r11d
	jne	.L372
	sall	$10, %esi
	leal	2(%rax), %r13d
	leal	-56613888(%rdx,%rsi), %esi
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L432:
	movzwl	2(%rcx,%rsi), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L397
	movl	%r14d, %ecx
	addl	$2, %r15d
	sall	$10, %ecx
	leal	-56613888(%rax,%rcx), %r14d
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L361:
	movl	-244(%rbp), %eax
	movl	%eax, -296(%rbp)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L429:
	movl	$7, (%r14)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L430:
	movq	-272(%rbp), %rax
	movl	$1, %r8d
	movzwl	2(%rax), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L369
	sall	$10, %ecx
	movl	$2, %r8d
	leal	-56613888(%rax,%rcx), %ecx
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L367:
	movl	-180(%rbp), %eax
	movl	%eax, -264(%rbp)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L431:
	movslq	%r13d, %rax
	movq	-280(%rbp), %r13
	movl	%ebx, %ecx
	xorl	%edx, %edx
	leaq	(%r12,%rax,2), %rsi
	subl	%eax, %ecx
	movq	-320(%rbp), %r14
	movq	-312(%rbp), %r15
	movq	%r13, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-264(%rbp), %rsi
	movswl	-248(%rbp), %eax
	movl	%eax, %edx
	sarl	$5, %eax
	testw	%dx, %dx
	cmovs	-244(%rbp), %eax
	cmpl	%eax, -296(%rbp)
	je	.L437
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r10
	movl	$2, %edx
	movq	%rax, -128(%rbp)
	movq	-288(%rbp), %rax
	movq	%r14, %rcx
	movq	%r13, %rsi
	movw	%dx, -120(%rbp)
	movq	%r10, %rdx
	movq	184(%rax), %rdi
	movq	%r10, -264(%rbp)
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	(%r14), %ecx
	movq	-264(%rbp), %r10
	testl	%ecx, %ecx
	jle	.L382
.L427:
	xorl	%r12d, %r12d
.L383:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L392:
	movq	$0, -272(%rbp)
	jmp	.L366
.L382:
	movslq	-292(%rbp), %rax
	subl	%eax, %ebx
	movl	%ebx, %r9d
	leaq	(%r12,%rax,2), %rbx
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L384
	movswl	%ax, %edx
	sarl	$5, %edx
.L385:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r10, %rdi
	movq	%rbx, %rcx
	movq	%r10, -264(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-264(%rbp), %r10
	testb	%al, %al
	je	.L386
	jmp	.L427
.L386:
	movzwl	-248(%rbp), %eax
	testw	%ax, %ax
	js	.L387
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L388:
	testb	$17, %al
	jne	.L398
	leaq	-246(%rbp), %rdx
	testb	$2, %al
	cmove	-232(%rbp), %rdx
.L389:
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L427
	movslq	-296(%rbp), %rax
	movq	-304(%rbp), %rsi
	movq	%r14, %r8
	movq	%r10, -264(%rbp)
	movq	-288(%rbp), %rdi
	subl	%eax, %ecx
	leaq	(%rdx,%rax,2), %rdx
	call	_ZN6icu_6717CanonicalIterator15getEquivalents2EPNS_9HashtableEPKDsiR10UErrorCode.part.0
	movq	-264(%rbp), %r10
	movq	%rax, %r12
	jmp	.L383
.L387:
	movl	-244(%rbp), %ecx
	jmp	.L388
.L437:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L380
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	movq	%rax, (%r12)
	movw	%di, 8(%r12)
.L380:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movl	$64, %edi
	movq	%rax, -128(%rbp)
	movw	%si, -120(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	leaq	-128(%rbp), %r10
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L381
	movq	%r10, %rsi
	movq	%rax, %rdi
	movq	%r10, -264(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movq	-264(%rbp), %r10
.L381:
	movq	%r12, %rdx
	movq	-304(%rbp), %r12
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%r10, -264(%rbp)
	movq	(%r12), %rdi
	call	uhash_put_67@PLT
	movq	-264(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L364
.L398:
	xorl	%edx, %edx
	jmp	.L389
.L384:
	movl	-116(%rbp), %edx
	jmp	.L385
.L436:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4119:
	.size	_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode.part.0, .-_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode
	.type	_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode, @function
_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode:
.LFB3186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L439
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3186:
	.size	_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode, .-_ZN6icu_6717CanonicalIterator7extractEPNS_9HashtableEiPKDsiiR10UErrorCode
	.weak	_ZTSN6icu_6717CanonicalIteratorE
	.section	.rodata._ZTSN6icu_6717CanonicalIteratorE,"aG",@progbits,_ZTSN6icu_6717CanonicalIteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6717CanonicalIteratorE, @object
	.size	_ZTSN6icu_6717CanonicalIteratorE, 29
_ZTSN6icu_6717CanonicalIteratorE:
	.string	"N6icu_6717CanonicalIteratorE"
	.weak	_ZTIN6icu_6717CanonicalIteratorE
	.section	.data.rel.ro._ZTIN6icu_6717CanonicalIteratorE,"awG",@progbits,_ZTIN6icu_6717CanonicalIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6717CanonicalIteratorE, @object
	.size	_ZTIN6icu_6717CanonicalIteratorE, 24
_ZTIN6icu_6717CanonicalIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717CanonicalIteratorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6717CanonicalIteratorE
	.section	.data.rel.ro.local._ZTVN6icu_6717CanonicalIteratorE,"awG",@progbits,_ZTVN6icu_6717CanonicalIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6717CanonicalIteratorE, @object
	.size	_ZTVN6icu_6717CanonicalIteratorE, 40
_ZTVN6icu_6717CanonicalIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6717CanonicalIteratorE
	.quad	_ZN6icu_6717CanonicalIteratorD1Ev
	.quad	_ZN6icu_6717CanonicalIteratorD0Ev
	.quad	_ZNK6icu_6717CanonicalIterator17getDynamicClassIDEv
	.local	_ZZN6icu_6717CanonicalIterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6717CanonicalIterator16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
