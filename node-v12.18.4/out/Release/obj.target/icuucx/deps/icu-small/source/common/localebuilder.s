	.file	"localebuilder.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilderD2Ev
	.type	_ZN6icu_6713LocaleBuilderD2Ev, @function
_ZN6icu_6713LocaleBuilderD2Ev:
.LFB2394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713LocaleBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	32(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2
	cmpb	$0, 12(%r13)
	jne	.L13
.L3:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L2:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	call	*8(%rax)
.L4:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L3
	.cfi_endproc
.LFE2394:
	.size	_ZN6icu_6713LocaleBuilderD2Ev, .-_ZN6icu_6713LocaleBuilderD2Ev
	.globl	_ZN6icu_6713LocaleBuilderD1Ev
	.set	_ZN6icu_6713LocaleBuilderD1Ev,_ZN6icu_6713LocaleBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilderD0Ev
	.type	_ZN6icu_6713LocaleBuilderD0Ev, @function
_ZN6icu_6713LocaleBuilderD0Ev:
.LFB2396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713LocaleBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	32(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L15
	cmpb	$0, 12(%r13)
	jne	.L25
.L16:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L15:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L17
	movq	(%rdi), %rax
	call	*8(%rax)
.L17:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L16
	.cfi_endproc
.LFE2396:
	.size	_ZN6icu_6713LocaleBuilderD0Ev, .-_ZN6icu_6713LocaleBuilderD0Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2657:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2657:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2660:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L39
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L27
	cmpb	$0, 12(%rbx)
	jne	.L40
.L31:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L27:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L31
	.cfi_endproc
.LFE2660:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2663:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L43
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2663:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2666:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L46
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2666:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L52
.L48:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L53
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2668:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2669:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2669:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2670:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2670:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2671:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2671:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2672:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2672:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2673:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2673:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2674:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L69
	testl	%edx, %edx
	jle	.L69
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L72
.L61:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L61
	.cfi_endproc
.LFE2674:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L76
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L76
	testl	%r12d, %r12d
	jg	.L83
	cmpb	$0, 12(%rbx)
	jne	.L84
.L78:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L78
.L84:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2675:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L86
	movq	(%rdi), %r8
.L87:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L90
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L90
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2676:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2677:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L97
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2677:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2678:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2678:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2679:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2679:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2680:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2680:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2682:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2682:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2684:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2684:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilderC2Ev
	.type	_ZN6icu_6713LocaleBuilderC2Ev, @function
_ZN6icu_6713LocaleBuilderC2Ev:
.LFB2391:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713LocaleBuilderE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 13(%rdi)
	movl	$0, 22(%rdi)
	movl	$0, 26(%rdi)
	movb	$0, 12(%rdi)
	movb	$0, 21(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE2391:
	.size	_ZN6icu_6713LocaleBuilderC2Ev, .-_ZN6icu_6713LocaleBuilderC2Ev
	.globl	_ZN6icu_6713LocaleBuilderC1Ev
	.set	_ZN6icu_6713LocaleBuilderC1Ev,_ZN6icu_6713LocaleBuilderC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder11setLanguageENS_11StringPieceE
	.type	_ZN6icu_6713LocaleBuilder11setLanguageENS_11StringPieceE, @function
_ZN6icu_6713LocaleBuilder11setLanguageENS_11StringPieceE:
.LFB2400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jg	.L105
	movq	%rdx, %rbx
	testl	%edx, %edx
	jne	.L106
	movb	$0, 12(%rdi)
.L105:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	%rsi, %r13
	movl	%edx, %esi
	movq	%r13, %rdi
	call	ultag_isLanguageSubtag_67@PLT
	testb	%al, %al
	jne	.L109
	movl	$1, 8(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movslq	%ebx, %rbx
	leaq	12(%r12), %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movb	$0, 12(%r12,%rbx)
	jmp	.L105
	.cfi_endproc
.LFE2400:
	.size	_ZN6icu_6713LocaleBuilder11setLanguageENS_11StringPieceE, .-_ZN6icu_6713LocaleBuilder11setLanguageENS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder9setScriptENS_11StringPieceE
	.type	_ZN6icu_6713LocaleBuilder9setScriptENS_11StringPieceE, @function
_ZN6icu_6713LocaleBuilder9setScriptENS_11StringPieceE:
.LFB2401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jg	.L111
	movq	%rdx, %rbx
	testl	%edx, %edx
	jne	.L112
	movb	$0, 21(%rdi)
.L111:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	%rsi, %r13
	movl	%edx, %esi
	movq	%r13, %rdi
	call	ultag_isScriptSubtag_67@PLT
	testb	%al, %al
	jne	.L115
	movl	$1, 8(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movslq	%ebx, %rbx
	leaq	21(%r12), %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movb	$0, 21(%r12,%rbx)
	jmp	.L111
	.cfi_endproc
.LFE2401:
	.size	_ZN6icu_6713LocaleBuilder9setScriptENS_11StringPieceE, .-_ZN6icu_6713LocaleBuilder9setScriptENS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder9setRegionENS_11StringPieceE
	.type	_ZN6icu_6713LocaleBuilder9setRegionENS_11StringPieceE, @function
_ZN6icu_6713LocaleBuilder9setRegionENS_11StringPieceE:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jg	.L117
	movq	%rdx, %rbx
	testl	%edx, %edx
	jne	.L118
	movb	$0, 26(%rdi)
.L117:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	%rsi, %r13
	movl	%edx, %esi
	movq	%r13, %rdi
	call	ultag_isRegionSubtag_67@PLT
	testb	%al, %al
	jne	.L121
	movl	$1, 8(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movslq	%ebx, %rbx
	leaq	26(%r12), %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movb	$0, 26(%r12,%rbx)
	jmp	.L117
	.cfi_endproc
.LFE2402:
	.size	_ZN6icu_6713LocaleBuilder9setRegionENS_11StringPieceE, .-_ZN6icu_6713LocaleBuilder9setRegionENS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder10setVariantENS_11StringPieceE
	.type	_ZN6icu_6713LocaleBuilder10setVariantENS_11StringPieceE, @function
_ZN6icu_6713LocaleBuilder10setVariantENS_11StringPieceE:
.LFB2404:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	8(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L142
	movq	%rdx, %rbx
	testl	%edx, %edx
	jne	.L125
	movq	32(%rdi), %r13
	testq	%r13, %r13
	je	.L126
	cmpb	$0, 12(%r13)
	jne	.L151
.L127:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L126:
	movq	$0, 32(%r12)
.L142:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movl	$64, %edi
	movq	%rsi, %r13
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L128
	leaq	13(%rax), %rax
	movl	%ebx, %edx
	leaq	8(%r12), %rcx
	movq	%r13, %rsi
	movq	%rax, (%r14)
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	$0, 56(%r14)
	movl	$40, 8(%r14)
	movw	%ax, 12(%r14)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	8(%r12), %edx
	testl	%edx, %edx
	jg	.L142
	movl	56(%r14), %esi
	movq	(%r14), %r13
	testl	%esi, %esi
	jle	.L131
	leal	-1(%rsi), %eax
	leaq	1(%r13,%rax), %rbx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L153:
	movb	$45, 0(%r13)
	addq	$1, %r13
	cmpq	%rbx, %r13
	je	.L152
.L134:
	movsbl	0(%r13), %edi
	cmpb	$95, %dil
	je	.L153
	call	uprv_asciitolower_67@PLT
	addq	$1, %r13
	movb	%al, -1(%r13)
	cmpq	%rbx, %r13
	jne	.L134
.L152:
	movl	56(%r14), %esi
	movq	(%r14), %r13
.L131:
	movq	%r13, %rdi
	call	ultag_isVariantSubtags_67@PLT
	testb	%al, %al
	je	.L154
	movq	32(%r12), %r13
	testq	%r13, %r13
	je	.L137
	cmpb	$0, 12(%r13)
	jne	.L155
.L138:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L137:
	popq	%rbx
	movq	%r14, 32(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L154:
	cmpb	$0, 12(%r14)
	jne	.L156
.L136:
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	$1, 8(%r12)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L155:
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L156:
	movq	(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L136
.L128:
	cmpl	$0, 8(%r12)
	jg	.L142
	movl	$7, 8(%r12)
	jmp	.L142
	.cfi_endproc
.LFE2404:
	.size	_ZN6icu_6713LocaleBuilder10setVariantENS_11StringPieceE, .-_ZN6icu_6713LocaleBuilder10setVariantENS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder9setLocaleERKNS_6LocaleE
	.type	_ZN6icu_6713LocaleBuilder9setLocaleERKNS_6LocaleE, @function
_ZN6icu_6713LocaleBuilder9setLocaleERKNS_6LocaleE:
.LFB2397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, 8(%rdi)
	movb	$0, 12(%rdi)
	movb	$0, 21(%rdi)
	movb	$0, 26(%rdi)
	testq	%r14, %r14
	je	.L158
	cmpb	$0, 12(%r14)
	jne	.L179
.L159:
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L158:
	movq	$0, 32(%r12)
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L160
	movq	(%rdi), %rax
	call	*8(%rax)
.L160:
	movq	$0, 40(%r12)
	leaq	-80(%rbp), %r14
	leaq	8(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	8(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L161
	movslq	-72(%rbp), %rbx
	testl	%ebx, %ebx
	jne	.L162
	movb	$0, 12(%r12)
.L161:
	leaq	20(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	8(%r12), %edx
	testl	%edx, %edx
	jg	.L164
	movslq	-72(%rbp), %rbx
	testl	%ebx, %ebx
	jne	.L165
	movb	$0, 21(%r12)
.L164:
	leaq	26(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	8(%r12), %eax
	testl	%eax, %eax
	jg	.L167
	movslq	-72(%rbp), %rbx
	testl	%ebx, %ebx
	jne	.L168
	movb	$0, 26(%r12)
.L167:
	movslq	32(%r13), %rsi
	movq	%r14, %rdi
	addq	208(%r13), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6713LocaleBuilder10setVariantENS_11StringPieceE
	movq	%r13, %rdi
	call	_ZNK6icu_676Locale5cloneEv@PLT
	movq	%rax, 40(%r12)
	testq	%rax, %rax
	je	.L180
.L170:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L181
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	-80(%rbp), %r15
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	ultag_isRegionSubtag_67@PLT
	testb	%al, %al
	jne	.L182
	movl	$1, 8(%r12)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L165:
	movq	-80(%rbp), %r15
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	ultag_isScriptSubtag_67@PLT
	testb	%al, %al
	jne	.L183
	movl	$1, 8(%r12)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L162:
	movq	-80(%rbp), %r15
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	ultag_isLanguageSubtag_67@PLT
	testb	%al, %al
	jne	.L184
	movl	$1, 8(%r12)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L179:
	movq	(%r14), %rdi
	call	uprv_free_67@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$7, 8(%r12)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	12(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movb	$0, 12(%r12,%rbx)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L183:
	leaq	21(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movb	$0, 21(%r12,%rbx)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	26(%r12), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movb	$0, 26(%r12,%rbx)
	jmp	.L167
.L181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2397:
	.size	_ZN6icu_6713LocaleBuilder9setLocaleERKNS_6LocaleE, .-_ZN6icu_6713LocaleBuilder9setLocaleERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder23setUnicodeLocaleKeywordENS_11StringPieceES1_
	.type	_ZN6icu_6713LocaleBuilder23setUnicodeLocaleKeywordENS_11StringPieceES1_, @function
_ZN6icu_6713LocaleBuilder23setUnicodeLocaleKeywordENS_11StringPieceES1_:
.LFB2412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L197
.L193:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movq	%rsi, %r14
	movl	%edx, %esi
	movq	%rdx, %r15
	movq	%rcx, %r13
	movq	%r14, %rdi
	movq	%r8, %rbx
	call	ultag_isUnicodeLocaleKey_67@PLT
	testb	%al, %al
	je	.L189
	testl	%ebx, %ebx
	jne	.L198
.L188:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L199
.L190:
	leaq	8(%r12), %r9
	movq	%r13, %rcx
	movq	%rbx, %r8
	movq	%r14, %rsi
	movq	%r15, %rdx
	call	_ZN6icu_676Locale22setUnicodeKeywordValueENS_11StringPieceES1_R10UErrorCode@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L198:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	ultag_isUnicodeLocaleType_67@PLT
	testb	%al, %al
	jne	.L188
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$1, 8(%r12)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L199:
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L191
	movq	%rax, -56(%rbp)
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	-56(%rbp), %rdi
	movq	%rdi, 40(%r12)
	jmp	.L190
.L191:
	movq	$0, 40(%r12)
	movl	$7, 8(%r12)
	jmp	.L193
	.cfi_endproc
.LFE2412:
	.size	_ZN6icu_6713LocaleBuilder23setUnicodeLocaleKeywordENS_11StringPieceES1_, .-_ZN6icu_6713LocaleBuilder23setUnicodeLocaleKeywordENS_11StringPieceES1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder5clearEv
	.type	_ZN6icu_6713LocaleBuilder5clearEv, @function
_ZN6icu_6713LocaleBuilder5clearEv:
.LFB2415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	32(%rdi), %r13
	movq	%rdi, %r12
	movl	$0, 8(%rdi)
	movb	$0, 12(%rdi)
	movb	$0, 21(%rdi)
	movb	$0, 26(%rdi)
	testq	%r13, %r13
	je	.L201
	cmpb	$0, 12(%r13)
	jne	.L211
.L202:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L201:
	movq	$0, 32(%r12)
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L203
	movq	(%rdi), %rax
	call	*8(%rax)
.L203:
	movq	$0, 40(%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L202
	.cfi_endproc
.LFE2415:
	.size	_ZN6icu_6713LocaleBuilder5clearEv, .-_ZN6icu_6713LocaleBuilder5clearEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder15clearExtensionsEv
	.type	_ZN6icu_6713LocaleBuilder15clearExtensionsEv, @function
_ZN6icu_6713LocaleBuilder15clearExtensionsEv:
.LFB2416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L213
	movq	(%rdi), %rax
	call	*8(%rax)
.L213:
	movq	$0, 40(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2416:
	.size	_ZN6icu_6713LocaleBuilder15clearExtensionsEv, .-_ZN6icu_6713LocaleBuilder15clearExtensionsEv
	.p2align 4
	.globl	_ZN6icu_6715makeBogusLocaleEv
	.type	_ZN6icu_6715makeBogusLocaleEv, @function
_ZN6icu_6715makeBogusLocaleEv:
.LFB2417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676Locale10setToBogusEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2417:
	.size	_ZN6icu_6715makeBogusLocaleEv, .-_ZN6icu_6715makeBogusLocaleEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713LocaleBuilder11copyErrorToER10UErrorCode
	.type	_ZNK6icu_6713LocaleBuilder11copyErrorToER10UErrorCode, @function
_ZNK6icu_6713LocaleBuilder11copyErrorToER10UErrorCode:
.LFB2420:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jg	.L220
	movl	8(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, (%rsi)
	setg	%al
.L220:
	ret
	.cfi_endproc
.LFE2420:
	.size	_ZNK6icu_6713LocaleBuilder11copyErrorToER10UErrorCode, .-_ZNK6icu_6713LocaleBuilder11copyErrorToER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder14setLanguageTagENS_11StringPieceE
	.type	_ZN6icu_6713LocaleBuilder14setLanguageTagENS_11StringPieceE, @function
_ZN6icu_6713LocaleBuilder14setLanguageTagENS_11StringPieceE:
.LFB2398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-256(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$240, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale14forLanguageTagENS_11StringPieceER10UErrorCode@PLT
	movl	8(%r12), %eax
	testl	%eax, %eax
	jg	.L224
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713LocaleBuilder9setLocaleERKNS_6LocaleE
.L224:
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	addq	$240, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L227:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2398:
	.size	_ZN6icu_6713LocaleBuilder14setLanguageTagENS_11StringPieceE, .-_ZN6icu_6713LocaleBuilder14setLanguageTagENS_11StringPieceE
	.p2align 4
	.type	_ZN6icu_67L15_copyExtensionsERKNS_6LocaleEPNS_17StringEnumerationERS0_bR10UErrorCode.part.0, @function
_ZN6icu_67L15_copyExtensionsERKNS_6LocaleEPNS_17StringEnumerationERS0_bR10UErrorCode.part.0:
.LFB3005:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$168, %rsp
	movq	%rdi, -176(%rbp)
	movq	%rdx, -200(%rbp)
	movb	%cl, -185(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -208(%rbp)
	testq	%rsi, %rsi
	je	.L296
.L229:
	leaq	-128(%rbp), %rax
	movq	%rax, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L252:
	movq	(%r14), %rax
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	*40(%rax)
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L230
	movq	-184(%rbp), %rsi
	leaq	-160(%rbp), %r13
	leaq	-115(%rbp), %rax
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	movw	%dx, -116(%rbp)
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	leaq	-144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-144(%rbp), %rsi
	movq	%r13, %rcx
	movq	%rbx, %r8
	movq	-136(%rbp), %rdx
	movq	-176(%rbp), %rdi
	call	_ZNK6icu_676Locale15getKeywordValueENS_11StringPieceERNS_8ByteSinkER10UErrorCode@PLT
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L250
	movq	_ZN6icu_6713kAttributeKeyE(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	movq	-128(%rbp), %r15
	testl	%eax, %eax
	je	.L297
.L234:
	cmpb	$0, -185(%rbp)
	je	.L238
	movl	-72(%rbp), %eax
	cmpb	$0, 1(%r12)
	movl	%eax, -168(%rbp)
	je	.L298
	movq	_ZN6icu_6713kAttributeKeyE(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L248
	movl	-168(%rbp), %esi
	movq	%r15, %rdi
	call	ultag_isUnicodeLocaleAttributes_67@PLT
	testb	%al, %al
	je	.L245
.L291:
	movq	-128(%rbp), %r15
.L238:
	movq	-200(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L250
	movq	%r13, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	cmpb	$0, -116(%rbp)
	je	.L252
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L297:
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	jle	.L234
	subl	$1, %eax
	leaq	1(%r15,%rax), %rax
	movq	%rax, -168(%rbp)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L300:
	movb	$45, (%r15)
	addq	$1, %r15
	cmpq	-168(%rbp), %r15
	je	.L299
.L237:
	movsbl	(%r15), %edi
	cmpb	$95, %dil
	je	.L300
	call	uprv_asciitolower_67@PLT
	addq	$1, %r15
	movb	%al, -1(%r15)
	cmpq	-168(%rbp), %r15
	jne	.L237
.L299:
	movq	-128(%rbp), %r15
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%r12, %rdi
	call	uloc_toUnicodeLocaleKey_67@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	uloc_toUnicodeLocaleType_67@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %r15
	testq	%r8, %r8
	je	.L245
	testq	%rax, %rax
	je	.L245
	movl	$-1, %esi
	movq	%r8, %rdi
	call	ultag_isUnicodeLocaleKey_67@PLT
	testb	%al, %al
	jne	.L301
	.p2align 4,,10
	.p2align 3
.L245:
	movl	$1, (%rbx)
.L250:
	movq	%r13, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	cmpb	$0, -116(%rbp)
	jne	.L302
.L230:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L228
	movq	(%rdi), %rax
	call	*8(%rax)
.L228:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movsbl	(%r12), %edi
	call	uprv_asciitolower_67@PLT
	movsbl	%al, %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L246
	movsbl	(%r12), %edi
	call	uprv_asciitolower_67@PLT
	cmpb	$47, %al
	jle	.L245
	movsbl	(%r12), %edi
	call	uprv_asciitolower_67@PLT
	cmpb	$57, %al
	jg	.L245
	.p2align 4,,10
	.p2align 3
.L246:
	movsbl	(%r12), %edi
	call	uprv_asciitolower_67@PLT
	cmpb	$117, %al
	je	.L241
	cmpb	$120, %al
	je	.L242
	cmpb	$116, %al
	je	.L304
	movl	-168(%rbp), %esi
	movq	%r15, %rdi
	call	ultag_isExtensionSubtags_67@PLT
	testb	%al, %al
	setne	%al
	testb	%al, %al
	je	.L245
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L302:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L304:
	movl	-168(%rbp), %esi
	movq	%r15, %rdi
	call	ultag_isTransformedExtensionSubtags_67@PLT
	testb	%al, %al
	setne	%al
	testb	%al, %al
	je	.L245
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L242:
	movl	-168(%rbp), %esi
	movq	%r15, %rdi
	call	ultag_isPrivateuseValueSubtags_67@PLT
	testb	%al, %al
	setne	%al
	testb	%al, %al
	je	.L245
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L241:
	movl	-168(%rbp), %esi
	movq	%r15, %rdi
	call	ultag_isUnicodeExtensionSubtags_67@PLT
	testb	%al, %al
	setne	%al
	testb	%al, %al
	je	.L245
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L301:
	movl	$-1, %esi
	movq	%r15, %rdi
	call	ultag_isUnicodeLocaleType_67@PLT
	testb	%al, %al
	je	.L245
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%r8, %rsi
	call	_ZNK6icu_676Locale14createKeywordsER10UErrorCode@PLT
	movl	(%rbx), %esi
	movq	%rax, -208(%rbp)
	movq	%rax, %r14
	testl	%esi, %esi
	jg	.L230
	testq	%rax, %rax
	jne	.L229
	jmp	.L228
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3005:
	.size	_ZN6icu_67L15_copyExtensionsERKNS_6LocaleEPNS_17StringEnumerationERS0_bR10UErrorCode.part.0, .-_ZN6icu_67L15_copyExtensionsERKNS_6LocaleEPNS_17StringEnumerationERS0_bR10UErrorCode.part.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"und-u-"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder12setExtensionEcNS_11StringPieceE
	.type	_ZN6icu_6713LocaleBuilder12setExtensionEcNS_11StringPieceE, @function
_ZN6icu_6713LocaleBuilder12setExtensionEcNS_11StringPieceE:
.LFB2411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 3, -56
	movb	%sil, -436(%rbp)
	movl	8(%rdi), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jle	.L351
.L307:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L352
	addq	$408, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movsbl	%sil, %edi
	movq	%rdx, %r15
	movq	%rcx, %r13
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L308
	movzbl	-436(%rbp), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L353
.L308:
	xorl	%ecx, %ecx
	leaq	8(%r12), %r14
	movq	%r15, %rsi
	movl	%r13d, %edx
	movw	%cx, -404(%rbp)
	movq	%r14, %rcx
	leaq	-403(%rbp), %rax
	leaq	-416(%rbp), %rdi
	movq	%rax, -416(%rbp)
	movl	$0, -360(%rbp)
	movl	$40, -408(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	8(%r12), %esi
	testl	%esi, %esi
	jle	.L354
.L310:
	cmpb	$0, -404(%rbp)
	je	.L307
	movq	-416(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L354:
	movl	-360(%rbp), %r15d
	movq	-416(%rbp), %rbx
	testl	%r15d, %r15d
	jle	.L311
	leal	-1(%r15), %eax
	leaq	1(%rbx,%rax), %r15
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L356:
	movb	$45, (%rbx)
	addq	$1, %rbx
	cmpq	%r15, %rbx
	je	.L355
.L314:
	movsbl	(%rbx), %edi
	cmpb	$95, %dil
	je	.L356
	call	uprv_asciitolower_67@PLT
	addq	$1, %rbx
	movb	%al, -1(%rbx)
	cmpq	%r15, %rbx
	jne	.L314
.L355:
	movl	-360(%rbp), %r15d
.L311:
	testl	%r15d, %r15d
	jne	.L315
.L323:
	cmpq	$0, 40(%r12)
	je	.L357
.L317:
	movsbl	-436(%rbp), %edi
	call	uprv_asciitolower_67@PLT
	cmpb	$117, %al
	je	.L325
	movq	-416(%rbp), %rsi
	movq	40(%r12), %r13
	leaq	-432(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	%r14, %r9
	movl	$1, %edx
	movq	%r13, %rdi
	movq	-432(%rbp), %rcx
	movq	-424(%rbp), %r8
	leaq	-436(%rbp), %rsi
	call	_ZN6icu_676Locale15setKeywordValueENS_11StringPieceES1_R10UErrorCode@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L353:
	movl	$1, 8(%r12)
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L325:
	movq	40(%r12), %rbx
	movq	_ZN6icu_6713kAttributeKeyE(%rip), %rsi
	movq	%r14, %rcx
	leaq	.LC0(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_676Locale21createUnicodeKeywordsER10UErrorCode@PLT
	movq	%rax, %r15
	movl	8(%r12), %eax
	testl	%eax, %eax
	jg	.L326
	testq	%r15, %r15
	jne	.L328
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L358:
	leaq	-432(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	%r14, %r9
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	-432(%rbp), %rsi
	movq	-424(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_676Locale22setUnicodeKeywordValueENS_11StringPieceES1_R10UErrorCode@PLT
.L328:
	movq	(%r15), %rax
	xorl	%esi, %esi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	*40(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L358
.L330:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	movl	8(%r12), %eax
.L327:
	testl	%r13d, %r13d
	je	.L310
	testl	%eax, %eax
	jg	.L310
	leaq	-432(%rbp), %r13
	leaq	.LC1(%rip), %rsi
	movq	40(%r12), %r15
	movq	%r13, %rdi
	leaq	-352(%rbp), %rbx
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-424(%rbp), %edx
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movq	-432(%rbp), %rsi
	leaq	-339(%rbp), %rax
	movl	$0, -296(%rbp)
	movq	%rax, -352(%rbp)
	xorl	%eax, %eax
	movl	$40, -344(%rbp)
	movw	%ax, -340(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-360(%rbp), %edx
	movq	%r14, %rcx
	movq	%rbx, %rdi
	movq	-416(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-352(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-288(%rbp), %r13
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-424(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	-432(%rbp), %rsi
	call	_ZN6icu_676Locale14forLanguageTagENS_11StringPieceER10UErrorCode@PLT
	movl	8(%r12), %edx
	testl	%edx, %edx
	jg	.L331
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_67L15_copyExtensionsERKNS_6LocaleEPNS_17StringEnumerationERS0_bR10UErrorCode.part.0
.L331:
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	cmpb	$0, -340(%rbp)
	je	.L310
	movq	-352(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L326:
	testq	%r15, %r15
	jne	.L330
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L315:
	movsbl	-436(%rbp), %edi
	movq	-416(%rbp), %rbx
	call	uprv_asciitolower_67@PLT
	cmpb	$117, %al
	je	.L318
	cmpb	$120, %al
	je	.L319
	cmpb	$116, %al
	je	.L359
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	ultag_isExtensionSubtags_67@PLT
	testb	%al, %al
	setne	%al
.L322:
	testb	%al, %al
	jne	.L323
	movl	$1, 8(%r12)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L357:
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L324
	movq	%rax, %rdi
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%rbx, 40(%r12)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L359:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	ultag_isTransformedExtensionSubtags_67@PLT
	testb	%al, %al
	setne	%al
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L319:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	ultag_isPrivateuseValueSubtags_67@PLT
	testb	%al, %al
	setne	%al
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L318:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	ultag_isUnicodeExtensionSubtags_67@PLT
	testb	%al, %al
	setne	%al
	jmp	.L322
.L352:
	call	__stack_chk_fail@PLT
.L324:
	movq	$0, 40(%r12)
	movl	$7, 8(%r12)
	jmp	.L310
	.cfi_endproc
.LFE2411:
	.size	_ZN6icu_6713LocaleBuilder12setExtensionEcNS_11StringPieceE, .-_ZN6icu_6713LocaleBuilder12setExtensionEcNS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder18copyExtensionsFromERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713LocaleBuilder18copyExtensionsFromERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713LocaleBuilder18copyExtensionsFromERKNS_6LocaleER10UErrorCode:
.LFB2418:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L378
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r14, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_676Locale14createKeywordsER10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	%rax, %r12
	testl	%edx, %edx
	jg	.L363
	testq	%rax, %rax
	je	.L360
	movq	(%rax), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*32(%rax)
	testl	%eax, %eax
	je	.L366
	movq	40(%r13), %r15
	testq	%r15, %r15
	je	.L379
.L367:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L366
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_67L15_copyExtensionsERKNS_6LocaleEPNS_17StringEnumerationERS0_bR10UErrorCode.part.0
.L366:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L366
.L360:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L368
	movq	%rax, %rdi
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%r15, 40(%r13)
	jmp	.L367
.L368:
	movq	$0, 40(%r13)
	movl	$7, 8(%r13)
	jmp	.L366
	.cfi_endproc
.LFE2418:
	.size	_ZN6icu_6713LocaleBuilder18copyExtensionsFromERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713LocaleBuilder18copyExtensionsFromERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder5buildER10UErrorCode
	.type	_ZN6icu_6713LocaleBuilder5buildER10UErrorCode, @function
_ZN6icu_6713LocaleBuilder5buildER10UErrorCode:
.LFB2419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L398
	movl	8(%rsi), %eax
	movq	%rsi, %r13
	movq	%rdx, %rbx
	testl	%eax, %eax
	jle	.L383
	movl	%eax, (%rdx)
.L398:
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676Locale10setToBogusEv@PLT
.L380:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	addq	$344, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	leaq	-368(%rbp), %r14
	leaq	12(%rsi), %rsi
	movq	%r14, %rdi
	leaq	-352(%rbp), %r15
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	xorl	%esi, %esi
	movl	-360(%rbp), %edx
	movq	%rbx, %rcx
	movw	%si, -340(%rbp)
	movq	%r15, %rdi
	movq	-368(%rbp), %rsi
	leaq	-339(%rbp), %rax
	movq	%rax, -352(%rbp)
	movl	$0, -296(%rbp)
	movl	$40, -344(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	cmpb	$0, 21(%r13)
	jne	.L400
.L384:
	cmpb	$0, 26(%r13)
	jne	.L401
.L385:
	cmpq	$0, 32(%r13)
	je	.L386
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movl	$45, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	movq	32(%r13), %rax
	movq	(%rax), %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-360(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-368(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L386:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L387
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676Locale10setToBogusEv@PLT
.L388:
	cmpb	$0, -340(%rbp)
	je	.L380
	movq	-352(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L387:
	movq	-352(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	-288(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleC1EPKcS2_S2_S2_@PLT
	movq	40(%r13), %rdi
	testq	%rdi, %rdi
	je	.L389
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L392
	movq	%rbx, %r8
	movl	$1, %ecx
	movq	%r14, %rdx
	xorl	%esi, %esi
	call	_ZN6icu_67L15_copyExtensionsERKNS_6LocaleEPNS_17StringEnumerationERS0_bR10UErrorCode.part.0
.L389:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L392
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1EOS0_@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movl	$45, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	26(%r13), %rsi
	movq	%r14, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-376(%rbp), %r8
	movl	-360(%rbp), %edx
	movq	%rbx, %rcx
	movq	-368(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movl	$45, %esi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	leaq	21(%r13), %rsi
	movq	%r14, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-376(%rbp), %r8
	movl	-360(%rbp), %edx
	movq	%rbx, %rcx
	movq	-368(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%r12, %rdi
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_676Locale10setToBogusEv@PLT
.L391:
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L388
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2419:
	.size	_ZN6icu_6713LocaleBuilder5buildER10UErrorCode, .-_ZN6icu_6713LocaleBuilder5buildER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder25addUnicodeLocaleAttributeENS_11StringPieceE
	.type	_ZN6icu_6713LocaleBuilder25addUnicodeLocaleAttributeENS_11StringPieceE, @function
_ZN6icu_6713LocaleBuilder25addUnicodeLocaleAttributeENS_11StringPieceE:
.LFB2413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	movq	%r13, %rcx
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-256(%rbp), %rdi
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-243(%rbp), %rax
	movl	$0, -200(%rbp)
	movq	%rax, -256(%rbp)
	movl	$40, -248(%rbp)
	movw	%r9w, -244(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	8(%r12), %r10d
	testl	%r10d, %r10d
	jle	.L450
.L403:
	cmpb	$0, -244(%rbp)
	jne	.L451
.L431:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L452
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	movl	-200(%rbp), %esi
	movq	-256(%rbp), %r14
	testl	%esi, %esi
	jle	.L404
	leal	-1(%rsi), %eax
	leaq	1(%r14,%rax), %rbx
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L454:
	movb	$45, (%r14)
	addq	$1, %r14
	cmpq	%r14, %rbx
	je	.L453
.L407:
	movsbl	(%r14), %edi
	cmpb	$95, %dil
	je	.L454
	call	uprv_asciitolower_67@PLT
	addq	$1, %r14
	movb	%al, -1(%r14)
	cmpq	%r14, %rbx
	jne	.L407
.L453:
	movq	-256(%rbp), %r14
	movl	-200(%rbp), %esi
.L404:
	movq	%r14, %rdi
	call	ultag_isUnicodeLocaleAttribute_67@PLT
	testb	%al, %al
	je	.L455
	cmpq	$0, 40(%r12)
	je	.L456
	leaq	-179(%rbp), %rax
	xorl	%edi, %edi
	leaq	-192(%rbp), %rsi
	movl	$0, -136(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-288(%rbp), %rax
	leaq	-272(%rbp), %r15
	movw	%di, -180(%rbp)
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rax, -344(%rbp)
	movl	$40, -184(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	movq	_ZN6icu_6713kAttributeKeyE(%rip), %rsi
	movq	40(%r12), %r14
	movq	%r15, %rdi
	movl	$0, -292(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-272(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	-264(%rbp), %rdx
	leaq	-292(%rbp), %r8
	call	_ZNK6icu_676Locale15getKeywordValueENS_11StringPieceERNS_8ByteSinkER10UErrorCode@PLT
	movl	-292(%rbp), %r8d
	testl	%r8d, %r8d
	jg	.L457
	movslq	-136(%rbp), %rax
	movq	-192(%rbp), %rbx
	testl	%eax, %eax
	jle	.L414
	subl	$1, %eax
	leaq	1(%rbx,%rax), %r14
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L459:
	movb	$45, (%rbx)
.L416:
	addq	$1, %rbx
	cmpq	%rbx, %r14
	je	.L458
.L417:
	movsbl	(%rbx), %edi
	cmpb	$95, %dil
	je	.L459
	call	uprv_asciitolower_67@PLT
	movb	%al, (%rbx)
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L451:
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$224, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L410
	movq	%rax, %rdi
	call	_ZN6icu_676LocaleC1Ev@PLT
	movq	%r14, 40(%r12)
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-256(%rbp), %rdx
	movq	_ZN6icu_6713kAttributeKeyE(%rip), %rsi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L455:
	movl	$1, 8(%r12)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L458:
	movslq	-136(%rbp), %rax
	movq	-192(%rbp), %rbx
.L414:
	leaq	(%rbx,%rax), %rcx
	xorl	%edx, %edx
	leaq	-115(%rbp), %rax
	movl	$0, -72(%rbp)
	movq	%rcx, -328(%rbp)
	leaq	-128(%rbp), %r14
	movq	%rax, -128(%rbp)
	movl	$40, -120(%rbp)
	movw	%dx, -116(%rbp)
	movb	$0, -305(%rbp)
	cmpq	%rbx, %rcx
	ja	.L418
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L463:
	movq	%r14, -320(%rbp)
	movl	-72(%rbp), %eax
.L421:
	testl	%eax, %eax
	jne	.L461
.L424:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-264(%rbp), %edx
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-272(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	1(%rbx,%rax), %rbx
	cmpq	%rbx, -328(%rbp)
	jbe	.L462
.L418:
	cmpb	$0, -305(%rbp)
	jne	.L463
	movq	-256(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rsi, -336(%rbp)
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L422
	movq	%r14, -320(%rbp)
	movl	-72(%rbp), %eax
	jle	.L421
	testl	%eax, %eax
	movq	-336(%rbp), %rsi
	jne	.L464
.L423:
	movq	%r15, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-264(%rbp), %edx
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-272(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-72(%rbp), %eax
	movb	$1, -305(%rbp)
	testl	%eax, %eax
	je	.L424
.L461:
	movq	%r13, %rdx
	movl	$95, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L457:
	movq	-256(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	leaq	-115(%rbp), %rax
	xorl	%ecx, %ecx
	leaq	-128(%rbp), %rdi
	movq	%rax, -128(%rbp)
	movl	-264(%rbp), %edx
	movl	$0, -72(%rbp)
	movq	-272(%rbp), %rsi
	movl	$40, -120(%rbp)
	movw	%cx, -116(%rbp)
	movq	%r13, %rcx
.L449:
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L427:
	movq	40(%r12), %rdi
	movq	-128(%rbp), %rdx
	movq	%r13, %rcx
	movq	_ZN6icu_6713kAttributeKeyE(%rip), %rsi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
.L422:
	cmpb	$0, -116(%rbp)
	jne	.L465
.L413:
	movq	-344(%rbp), %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	cmpb	$0, -180(%rbp)
	je	.L403
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L464:
	movl	$95, %esi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-256(%rbp), %rsi
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L462:
	cmpb	$0, -305(%rbp)
	jne	.L427
	movl	-72(%rbp), %eax
	testl	%eax, %eax
	je	.L419
	movq	%r13, %rdx
	movl	$95, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L419:
	movq	-256(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-264(%rbp), %edx
	movq	-272(%rbp), %rsi
	movq	%r13, %rcx
	movq	-320(%rbp), %rdi
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L465:
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L413
.L460:
	movq	%r14, -320(%rbp)
	jmp	.L419
.L452:
	call	__stack_chk_fail@PLT
.L410:
	movq	$0, 40(%r12)
	movl	$7, 8(%r12)
	jmp	.L403
	.cfi_endproc
.LFE2413:
	.size	_ZN6icu_6713LocaleBuilder25addUnicodeLocaleAttributeENS_11StringPieceE, .-_ZN6icu_6713LocaleBuilder25addUnicodeLocaleAttributeENS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713LocaleBuilder28removeUnicodeLocaleAttributeENS_11StringPieceE
	.type	_ZN6icu_6713LocaleBuilder28removeUnicodeLocaleAttributeENS_11StringPieceE, @function
_ZN6icu_6713LocaleBuilder28removeUnicodeLocaleAttributeENS_11StringPieceE:
.LFB2414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	movq	%r13, %rcx
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-256(%rbp), %rdi
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-243(%rbp), %rax
	movl	$0, -200(%rbp)
	movq	%rax, -256(%rbp)
	movl	$40, -248(%rbp)
	movw	%r8w, -244(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	8(%r12), %r9d
	testl	%r9d, %r9d
	jle	.L509
.L468:
	cmpb	$0, -244(%rbp)
	jne	.L510
.L488:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L511
	addq	$296, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movl	-200(%rbp), %esi
	movq	-256(%rbp), %r14
	testl	%esi, %esi
	jle	.L469
	leal	-1(%rsi), %eax
	leaq	1(%r14,%rax), %rbx
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L513:
	movb	$45, (%r14)
	addq	$1, %r14
	cmpq	%rbx, %r14
	je	.L512
.L472:
	movsbl	(%r14), %edi
	cmpb	$95, %dil
	je	.L513
	call	uprv_asciitolower_67@PLT
	addq	$1, %r14
	movb	%al, -1(%r14)
	cmpq	%rbx, %r14
	jne	.L472
.L512:
	movq	-256(%rbp), %r14
	movl	-200(%rbp), %esi
.L469:
	movq	%r14, %rdi
	call	ultag_isUnicodeLocaleAttribute_67@PLT
	testb	%al, %al
	je	.L514
	cmpq	$0, 40(%r12)
	je	.L468
	leaq	-288(%rbp), %r14
	xorl	%ecx, %ecx
	leaq	-179(%rbp), %rax
	movl	$0, -292(%rbp)
	leaq	-192(%rbp), %rsi
	movq	%r14, %rdi
	movw	%cx, -180(%rbp)
	movq	%rax, -192(%rbp)
	movl	$0, -136(%rbp)
	movl	$40, -184(%rbp)
	call	_ZN6icu_6718CharStringByteSinkC1EPNS_10CharStringE@PLT
	leaq	-272(%rbp), %rax
	movq	40(%r12), %r15
	movq	_ZN6icu_6713kAttributeKeyE(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movq	-272(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	-264(%rbp), %rdx
	leaq	-292(%rbp), %r8
	call	_ZNK6icu_676Locale15getKeywordValueENS_11StringPieceERNS_8ByteSinkER10UErrorCode@PLT
	movl	-292(%rbp), %esi
	testl	%esi, %esi
	jg	.L475
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	je	.L475
	movq	-192(%rbp), %r15
	jle	.L476
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L480:
	movsbl	(%r15,%rbx), %edi
	cmpb	$95, %dil
	je	.L477
	cmpb	$45, %dil
	je	.L477
	call	uprv_asciitolower_67@PLT
	movb	%al, (%r15,%rbx)
	movl	-136(%rbp), %eax
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L480
.L479:
	movq	-192(%rbp), %r15
.L476:
	movslq	%eax, %rbx
	xorl	%edx, %edx
	leaq	-115(%rbp), %rax
	movl	$0, -72(%rbp)
	leaq	(%r15,%rbx), %rcx
	movq	%rax, -128(%rbp)
	movq	%rcx, -312(%rbp)
	movl	$40, -120(%rbp)
	movw	%dx, -116(%rbp)
	movb	$0, -321(%rbp)
	cmpq	%r15, %rcx
	ja	.L481
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L517:
	movl	-72(%rbp), %eax
	leaq	-128(%rbp), %rbx
	testl	%eax, %eax
	jne	.L515
.L484:
	movq	-320(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-264(%rbp), %edx
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-272(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
.L483:
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	1(%r15,%rax), %r15
	cmpq	%r15, -312(%rbp)
	jbe	.L516
.L481:
	movq	-256(%rbp), %rsi
	movq	%r15, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L517
	movb	$1, -321(%rbp)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-256(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L477:
	movb	$0, (%r15,%rbx)
	movl	-136(%rbp), %eax
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L480
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L516:
	cmpb	$0, -321(%rbp)
	je	.L485
	movq	40(%r12), %rdi
	movq	-128(%rbp), %rdx
	movq	%r13, %rcx
	movq	_ZN6icu_6713kAttributeKeyE(%rip), %rsi
	call	_ZN6icu_676Locale15setKeywordValueEPKcS2_R10UErrorCode@PLT
.L485:
	cmpb	$0, -116(%rbp)
	je	.L475
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L475:
	movq	%r14, %rdi
	call	_ZN6icu_6718CharStringByteSinkD1Ev@PLT
	cmpb	$0, -180(%rbp)
	je	.L468
	movq	-192(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L514:
	movl	$1, 8(%r12)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L515:
	movq	%r13, %rdx
	movl	$95, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	jmp	.L484
.L511:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2414:
	.size	_ZN6icu_6713LocaleBuilder28removeUnicodeLocaleAttributeENS_11StringPieceE, .-_ZN6icu_6713LocaleBuilder28removeUnicodeLocaleAttributeENS_11StringPieceE
	.weak	_ZTSN6icu_6713LocaleBuilderE
	.section	.rodata._ZTSN6icu_6713LocaleBuilderE,"aG",@progbits,_ZTSN6icu_6713LocaleBuilderE,comdat
	.align 16
	.type	_ZTSN6icu_6713LocaleBuilderE, @object
	.size	_ZTSN6icu_6713LocaleBuilderE, 25
_ZTSN6icu_6713LocaleBuilderE:
	.string	"N6icu_6713LocaleBuilderE"
	.weak	_ZTIN6icu_6713LocaleBuilderE
	.section	.data.rel.ro._ZTIN6icu_6713LocaleBuilderE,"awG",@progbits,_ZTIN6icu_6713LocaleBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_6713LocaleBuilderE, @object
	.size	_ZTIN6icu_6713LocaleBuilderE, 24
_ZTIN6icu_6713LocaleBuilderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713LocaleBuilderE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6713LocaleBuilderE
	.section	.data.rel.ro._ZTVN6icu_6713LocaleBuilderE,"awG",@progbits,_ZTVN6icu_6713LocaleBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_6713LocaleBuilderE, @object
	.size	_ZTVN6icu_6713LocaleBuilderE, 40
_ZTVN6icu_6713LocaleBuilderE:
	.quad	0
	.quad	_ZTIN6icu_6713LocaleBuilderE
	.quad	_ZN6icu_6713LocaleBuilderD1Ev
	.quad	_ZN6icu_6713LocaleBuilderD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.globl	_ZN6icu_6713kAttributeKeyE
	.section	.rodata.str1.1
.LC2:
	.string	"attribute"
	.section	.data.rel.local,"aw"
	.align 8
	.type	_ZN6icu_6713kAttributeKeyE, @object
	.size	_ZN6icu_6713kAttributeKeyE, 8
_ZN6icu_6713kAttributeKeyE:
	.quad	.LC2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
