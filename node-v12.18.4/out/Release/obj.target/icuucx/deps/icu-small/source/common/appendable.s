	.file	"appendable.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Appendable21reserveAppendCapacityEi
	.type	_ZN6icu_6710Appendable21reserveAppendCapacityEi, @function
_ZN6icu_6710Appendable21reserveAppendCapacityEi:
.LFB19:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE19:
	.size	_ZN6icu_6710Appendable21reserveAppendCapacityEi, .-_ZN6icu_6710Appendable21reserveAppendCapacityEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Appendable15getAppendBufferEiiPDsiPi
	.type	_ZN6icu_6710Appendable15getAppendBufferEiiPDsiPi, @function
_ZN6icu_6710Appendable15getAppendBufferEiiPDsiPi:
.LFB20:
	.cfi_startproc
	endbr64
	movq	%rcx, %rax
	testl	%esi, %esi
	jle	.L6
	cmpl	%r8d, %esi
	jg	.L6
	movl	%r8d, (%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movl	%r8d, (%r9)
	ret
	.cfi_endproc
.LFE20:
	.size	_ZN6icu_6710Appendable15getAppendBufferEiiPDsiPi, .-_ZN6icu_6710Appendable15getAppendBufferEiiPDsiPi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Appendable15appendCodePointEi
	.type	_ZN6icu_6710Appendable15appendCodePointEi, @function
_ZN6icu_6710Appendable15appendCodePointEi:
.LFB17:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movl	%esi, %ebx
	movq	24(%rax), %rax
	cmpl	$65535, %esi
	jg	.L11
	popq	%rbx
	movzwl	%si, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	sarl	$10, %esi
	subw	$10304, %si
	movzwl	%si, %esi
	call	*%rax
	testb	%al, %al
	je	.L10
	andl	$1023, %ebx
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	%ebx, %esi
	orl	$56320, %esi
	call	*24(%rax)
	testb	%al, %al
	setne	%al
.L10:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17:
	.size	_ZN6icu_6710Appendable15appendCodePointEi, .-_ZN6icu_6710Appendable15appendCodePointEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710Appendable12appendStringEPKDsi
	.type	_ZN6icu_6710Appendable12appendStringEPKDsi, @function
_ZN6icu_6710Appendable12appendStringEPKDsi:
.LFB18:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	testl	%edx, %edx
	js	.L20
	je	.L19
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx,2), %r13
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L36:
	cmpq	%rbx, %r13
	jbe	.L19
.L23:
	movq	(%r12), %rax
	addq	$2, %rbx
	movq	%r12, %rdi
	movzwl	-2(%rbx), %esi
	call	*24(%rax)
	testb	%al, %al
	jne	.L36
.L22:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	testb	%al, %al
	je	.L22
.L20:
	movzwl	(%rbx), %esi
	addq	$2, %rbx
	testw	%si, %si
	jne	.L37
.L19:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18:
	.size	_ZN6icu_6710Appendable12appendStringEPKDsi, .-_ZN6icu_6710Appendable12appendStringEPKDsi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710AppendableD2Ev
	.type	_ZN6icu_6710AppendableD2Ev, @function
_ZN6icu_6710AppendableD2Ev:
.LFB14:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6710AppendableE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE14:
	.size	_ZN6icu_6710AppendableD2Ev, .-_ZN6icu_6710AppendableD2Ev
	.globl	_ZN6icu_6710AppendableD1Ev
	.set	_ZN6icu_6710AppendableD1Ev,_ZN6icu_6710AppendableD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710AppendableD0Ev
	.type	_ZN6icu_6710AppendableD0Ev, @function
_ZN6icu_6710AppendableD0Ev:
.LFB16:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710AppendableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE16:
	.size	_ZN6icu_6710AppendableD0Ev, .-_ZN6icu_6710AppendableD0Ev
	.weak	_ZTSN6icu_6710AppendableE
	.section	.rodata._ZTSN6icu_6710AppendableE,"aG",@progbits,_ZTSN6icu_6710AppendableE,comdat
	.align 16
	.type	_ZTSN6icu_6710AppendableE, @object
	.size	_ZTSN6icu_6710AppendableE, 22
_ZTSN6icu_6710AppendableE:
	.string	"N6icu_6710AppendableE"
	.weak	_ZTIN6icu_6710AppendableE
	.section	.data.rel.ro._ZTIN6icu_6710AppendableE,"awG",@progbits,_ZTIN6icu_6710AppendableE,comdat
	.align 8
	.type	_ZTIN6icu_6710AppendableE, @object
	.size	_ZTIN6icu_6710AppendableE, 24
_ZTIN6icu_6710AppendableE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6710AppendableE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6710AppendableE
	.section	.data.rel.ro._ZTVN6icu_6710AppendableE,"awG",@progbits,_ZTVN6icu_6710AppendableE,comdat
	.align 8
	.type	_ZTVN6icu_6710AppendableE, @object
	.size	_ZTVN6icu_6710AppendableE, 80
_ZTVN6icu_6710AppendableE:
	.quad	0
	.quad	_ZTIN6icu_6710AppendableE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	_ZN6icu_6710Appendable15appendCodePointEi
	.quad	_ZN6icu_6710Appendable12appendStringEPKDsi
	.quad	_ZN6icu_6710Appendable21reserveAppendCapacityEi
	.quad	_ZN6icu_6710Appendable15getAppendBufferEiiPDsiPi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
