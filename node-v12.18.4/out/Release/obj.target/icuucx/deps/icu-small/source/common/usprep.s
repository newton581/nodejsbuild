	.file	"usprep.cpp"
	.text
	.p2align 4
	.type	isSPrepAcceptable, @function
isSPrepAcceptable:
.LFB3080:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpw	$19, (%rcx)
	jbe	.L1
	cmpw	$0, 4(%rcx)
	jne	.L1
	cmpw	$20563, 8(%rcx)
	jne	.L1
	cmpw	$20562, 10(%rcx)
	jne	.L1
	cmpb	$3, 12(%rcx)
	jne	.L1
	cmpw	$517, 14(%rcx)
	je	.L10
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	16(%rcx), %eax
	movl	%eax, _ZL11dataVersion(%rip)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3080:
	.size	isSPrepAcceptable, .-isSPrepAcceptable
	.p2align 4
	.type	getSPrepFoldingOffset, @function
getSPrepFoldingOffset:
.LFB3081:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE3081:
	.size	getSPrepFoldingOffset, .-getSPrepFoldingOffset
	.p2align 4
	.type	compareEntries, @function
compareEntries:
.LFB3083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r8
	movq	(%rsi), %r9
	movq	8(%rdi), %r12
	movq	8(%rsi), %r13
	movq	%r8, %rdi
	movq	%r9, %rsi
	call	uhash_compareChars_67@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	uhash_compareChars_67@PLT
	addq	$8, %rsp
	andl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3083:
	.size	compareEntries, .-compareEntries
	.p2align 4
	.type	hashEntry, @function
hashEntry:
.LFB3082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r8
	movq	8(%rdi), %r12
	movq	%r8, %rdi
	call	uhash_hashChars_67@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	uhash_hashChars_67@PLT
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
	addl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3082:
	.size	hashEntry, .-hashEntry
	.p2align 4
	.type	usprep_cleanup, @function
usprep_cleanup:
.LFB3086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, _ZL21SHARED_DATA_HASHTABLE(%rip)
	je	.L18
	leaq	_ZL11usprepMutex(%rip), %rdi
	movl	$-1, -44(%rbp)
	leaq	-44(%rbp), %rbx
	call	umtx_lock_67@PLT
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	testq	%rdi, %rdi
	jne	.L19
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	_ZL11usprepMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	uhash_count_67@PLT
	testl	%eax, %eax
	je	.L38
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$0, _ZL19gSharedDataInitOnce(%rip)
	mfence
	cmpq	$0, _ZL21SHARED_DATA_HASHTABLE(%rip)
	sete	%al
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L39
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	8(%rax), %r13
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	movq	16(%rax), %r12
	call	uhash_removeElement_67@PLT
	movq	112(%r13), %rdi
	call	udata_close_67@PLT
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	uprv_free_67@PLT
	movq	$0, (%r12)
.L23:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	uprv_free_67@PLT
	movq	$0, 8(%r12)
.L24:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
.L19:
	movq	%rbx, %rsi
	call	uhash_nextElement_67@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L40
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L38:
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	call	uhash_close_67@PLT
	movq	$0, _ZL21SHARED_DATA_HASHTABLE(%rip)
	jmp	.L18
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3086:
	.size	usprep_cleanup, .-usprep_cleanup
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"spp"
	.text
	.p2align 4
	.type	_ZL17usprep_getProfilePKcS0_P10UErrorCode, @function
_ZL17usprep_getProfilePKcS0_P10UErrorCode:
.LFB3090:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -136(%rbp)
	movl	(%rdx), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jle	.L97
.L42:
	xorl	%r15d, %r15d
.L41:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$136, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movl	_ZL19gSharedDataInitOnce(%rip), %eax
	movq	%rsi, %r12
	movq	%rdx, %r14
	cmpl	$2, %eax
	jne	.L99
.L43:
	movl	4+_ZL19gSharedDataInitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L45
	movl	%eax, (%r14)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L99:
	leaq	_ZL19gSharedDataInitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L43
	movq	%r14, %rcx
	xorl	%edx, %edx
	leaq	compareEntries(%rip), %rsi
	leaq	hashEntry(%rip), %rdi
	call	uhash_open_67@PLT
	movl	(%r14), %r9d
	movq	%rax, _ZL21SHARED_DATA_HASHTABLE(%rip)
	testl	%r9d, %r9d
	jle	.L44
	movq	$0, _ZL21SHARED_DATA_HASHTABLE(%rip)
.L44:
	movl	$1, %edi
	leaq	usprep_cleanup(%rip), %rsi
	call	ucln_common_registerCleanup_67@PLT
	movl	(%r14), %eax
	leaq	_ZL19gSharedDataInitOnce(%rip), %rdi
	movl	%eax, 4+_ZL19gSharedDataInitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L45:
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jg	.L42
	movq	%r12, %xmm0
	leaq	_ZL11usprepMutex(%rip), %rdi
	leaq	-128(%rbp), %r13
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	umtx_lock_67@PLT
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L46
	addl	$1, 120(%rax)
	leaq	_ZL11usprepMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	_ZL11usprepMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movl	$128, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L100
	leaq	8(%rbx), %rdi
	movq	%rbx, %rcx
	movq	%r15, %rax
	movq	$0, (%rbx)
	andq	$-8, %rdi
	movq	$0, 120(%rbx)
	subq	%rdi, %rcx
	subl	$-128, %ecx
	shrl	$3, %ecx
	rep stosq
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movl	(%r14), %edi
	pxor	%xmm0, %xmm0
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	testl	%edi, %edi
	jle	.L101
.L49:
	movq	%rbx, %rdi
	call	uprv_free_67@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L101:
	movq	-136(%rbp), %rdi
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	leaq	.LC0(%rip), %rsi
	leaq	isSPrepAcceptable(%rip), %rcx
	call	udata_openChoice_67@PLT
	movl	(%r14), %esi
	testl	%esi, %esi
	jg	.L49
	movq	%rax, %rdi
	movq	%rax, -160(%rbp)
	call	udata_getMemory_67@PLT
	leaq	-112(%rbp), %r10
	movq	%r14, %rcx
	movl	(%rax), %edx
	movq	%r10, %rdi
	leaq	64(%rax), %rsi
	movq	%r10, -144(%rbp)
	movq	%rax, -152(%rbp)
	call	utrie_unserialize_67@PLT
	movl	(%r14), %ecx
	leaq	getSPrepFoldingOffset(%rip), %rax
	movq	-144(%rbp), %r10
	movq	%rax, -96(%rbp)
	movq	-152(%rbp), %r9
	testl	%ecx, %ecx
	movq	-160(%rbp), %r8
	jg	.L96
	leaq	_ZL11usprepMutex(%rip), %rdi
	movq	%r10, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r8, -144(%rbp)
	call	umtx_lock_67@PLT
	movq	112(%rbx), %rdi
	movq	-144(%rbp), %r8
	movq	-152(%rbp), %r9
	movq	-160(%rbp), %r10
	testq	%rdi, %rdi
	je	.L102
	movq	%r8, -144(%rbp)
	call	udata_getMemory_67@PLT
	movq	-144(%rbp), %r8
	movq	%rax, %r9
.L53:
	leaq	_ZL11usprepMutex(%rip), %rdi
	movq	%r8, -144(%rbp)
	movq	%r9, -152(%rbp)
	call	umtx_unlock_67@PLT
	movslq	(%rbx), %rax
	movq	-152(%rbp), %r9
	leaq	-60(%rbp), %rdi
	leaq	64(%r9,%rax), %rax
	movq	%rax, 104(%rbx)
	call	u_getUnicodeVersion_67@PLT
	movl	(%r14), %edx
	movq	-144(%rbp), %r8
	testl	%edx, %edx
	jg	.L96
	movzbl	-60(%rbp), %eax
	movzbl	-59(%rbp), %edx
	movzbl	1+_ZL11dataVersion(%rip), %ecx
	sall	$16, %edx
	sall	$24, %eax
	addl	%edx, %eax
	movzbl	-58(%rbp), %edx
	sall	$16, %ecx
	sall	$8, %edx
	addl	%edx, %eax
	movzbl	-57(%rbp), %edx
	addl	%eax, %edx
	movzbl	_ZL11dataVersion(%rip), %eax
	sall	$24, %eax
	addl	%ecx, %eax
	movzbl	2+_ZL11dataVersion(%rip), %ecx
	sall	$8, %ecx
	addl	%ecx, %eax
	movzbl	3+_ZL11dataVersion(%rip), %ecx
	addl	%ecx, %eax
	movl	8(%rbx), %ecx
	cmpl	%ecx, %eax
	cmovg	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L55
	testb	$1, 28(%rbx)
	jne	.L103
.L55:
	movb	$1, 124(%rbx)
	testq	%r8, %r8
	je	.L66
	movq	%r8, %rdi
	call	udata_close_67@PLT
	cmpb	$0, 124(%rbx)
	je	.L49
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L49
.L66:
	movl	28(%rbx), %eax
	movl	$16, %edi
	movl	%eax, %edx
	andl	$1, %edx
	testb	$2, %al
	movb	%dl, 125(%rbx)
	setne	126(%rbx)
	call	uprv_malloc_67@PLT
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L69
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	movups	%xmm0, (%rax)
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	strlen@PLT
	addl	$1, %eax
	testl	%eax, %eax
	jle	.L69
	movslq	%eax, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L57
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	cmpq	$0, -136(%rbp)
	je	.L58
	movq	-136(%rbp), %rdi
	call	strlen@PLT
	addl	$1, %eax
	testl	%eax, %eax
	jle	.L57
	movslq	%eax, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	movq	%rax, -160(%rbp)
	movq	%rax, -168(%rbp)
	je	.L57
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	leaq	_ZL11usprepMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L104
.L59:
	addl	$1, 120(%r15)
	movq	112(%rbx), %rdi
	call	udata_close_67@PLT
.L61:
	leaq	_ZL11usprepMutex(%rip), %rdi
	call	umtx_unlock_67@PLT
	movq	-160(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-152(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	%rbx, %rdi
	call	uprv_free_67@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L69:
	movq	$0, -152(%rbp)
.L57:
	movl	$7, (%r14)
	movq	112(%rbx), %rdi
	call	udata_close_67@PLT
	xorl	%edi, %edi
	call	uprv_free_67@PLT
	movq	-152(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-144(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L49
.L100:
	movl	$7, (%r14)
	jmp	.L49
.L103:
	movl	$3, (%r14)
.L96:
	movq	%r8, %rdi
	call	udata_close_67@PLT
	jmp	.L49
.L58:
	leaq	_ZL11usprepMutex(%rip), %rdi
	call	umtx_lock_67@PLT
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	movq	$0, -160(%rbp)
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L59
	movq	-152(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rax, (%rcx)
	movq	%rax, %rdi
	call	strcpy@PLT
.L64:
	movl	$1, 120(%rbx)
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%rbx, %r15
	movq	-144(%rbp), %rsi
	movq	_ZL21SHARED_DATA_HASHTABLE(%rip), %rdi
	xorl	%ebx, %ebx
	call	uhash_put_67@PLT
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	jmp	.L61
.L104:
	movq	-144(%rbp), %r15
	movq	-152(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r8, -160(%rbp)
	movq	%rcx, (%r15)
	movq	%rcx, %rdi
	call	strcpy@PLT
	movq	-160(%rbp), %r8
	movq	-136(%rbp), %rsi
	movq	%r8, 8(%r15)
	movq	%r8, %rdi
	call	strcpy@PLT
	jmp	.L64
.L102:
	movdqa	(%r10), %xmm5
	movdqa	16(%r10), %xmm6
	movq	%r8, 112(%rbx)
	xorl	%r8d, %r8d
	movdqu	(%r9), %xmm1
	movq	32(%r10), %rdx
	movups	%xmm1, (%rbx)
	movdqu	16(%r9), %xmm2
	movups	%xmm2, 16(%rbx)
	movdqu	32(%r9), %xmm3
	movups	%xmm3, 32(%rbx)
	movdqu	48(%r9), %xmm4
	movq	%rdx, 96(%rbx)
	movups	%xmm4, 48(%rbx)
	movups	%xmm5, 64(%rbx)
	movups	%xmm6, 80(%rbx)
	jmp	.L53
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3090:
	.size	_ZL17usprep_getProfilePKcS0_P10UErrorCode, .-_ZL17usprep_getProfilePKcS0_P10UErrorCode
	.p2align 4
	.type	_ZL10usprep_mapPK18UStringPrepProfilePKDsiPDsiiP11UParseErrorP10UErrorCode, @function
_ZL10usprep_mapPK18UStringPrepProfilePKDsiPDsiiP11UParseErrorP10UErrorCode:
.LFB3096:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$1, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movl	%r9d, -72(%rbp)
	movq	%rax, -88(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -80(%rbp)
	testl	%edx, %edx
	jle	.L144
	movl	%edx, %r12d
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	movq	%rdi, %r15
	movl	%edx, %r14d
	movl	%r12d, %edx
	movq	%rsi, %r12
	movl	%r8d, %esi
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L190:
	leal	-65520(%r8), %eax
	cmpl	$65520, %r8d
	jne	.L122
	movl	-72(%rbp), %edi
	testl	%edi, %edi
	je	.L188
.L122:
	cmpl	$1, %eax
	je	.L118
	cmpl	$3, %eax
	je	.L124
.L118:
	leal	1(%r14), %eax
	cmpl	$65535, %r13d
	jle	.L114
	cmpl	%esi, %eax
	jge	.L143
	movl	%r13d, %eax
	andw	$1023, %r13w
	movslq	%r14d, %rdi
	sarl	$10, %eax
	orw	$-9216, %r13w
	subw	$10304, %ax
	movw	%r13w, 2(%rcx,%rdi,2)
	movw	%ax, (%rcx,%rdi,2)
.L143:
	addl	$2, %r14d
.L124:
	cmpl	%ebx, %edx
	jle	.L185
.L107:
	movslq	%ebx, %rax
	leal	1(%rbx), %edi
	movzwl	(%r12,%rax,2), %r13d
	leaq	(%rax,%rax), %r8
	movl	%r13d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L108
	cmpl	%edi, %edx
	jne	.L189
.L108:
	leal	-55296(%r13), %eax
	movq	64(%r15), %r8
	cmpl	$1024, %eax
	sbbl	%r9d, %r9d
	andl	$320, %r9d
.L111:
	movl	%r13d, %eax
	sarl	$5, %eax
	addl	%r9d, %eax
	cltq
	movzwl	(%r8,%rax,2), %r9d
	movl	%r13d, %eax
	andl	$31, %eax
	leal	(%rax,%r9,4), %eax
	cltq
	movzwl	(%r8,%rax,2), %eax
	testw	%ax, %ax
	je	.L112
	movzwl	%ax, %r8d
	movl	%edi, %ebx
.L113:
	cmpw	$-17, %ax
	ja	.L190
	sarl	$2, %r8d
	testb	$2, %al
	je	.L123
	cmpl	$16319, %r8d
	je	.L124
	movl	16(%r15), %eax
	cmpl	%r8d, 12(%r15)
	jg	.L133
	cmpl	%eax, %r8d
	jl	.L147
	movl	20(%r15), %edi
.L135:
	cmpl	%edi, %r8d
	jge	.L137
	movl	$2, %r11d
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L112:
	leal	1(%r14), %eax
	movl	%edi, %ebx
.L114:
	cmpl	%r14d, %esi
	jle	.L142
	movslq	%r14d, %r14
	movw	%r13w, (%rcx,%r14,2)
.L142:
	movl	%eax, %r14d
	cmpl	%ebx, %edx
	jg	.L107
.L185:
	movl	%r14d, %edx
	movl	%esi, %r15d
	movq	%rcx, %r14
.L106:
	movq	-80(%rbp), %rcx
	addq	$56, %rsp
	movl	%r15d, %esi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movzwl	2(%r12,%r8), %eax
	movl	%eax, %r8d
	andl	$-1024, %r8d
	cmpl	$56320, %r8d
	je	.L191
	movq	64(%r15), %r8
	movl	$320, %r9d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L191:
	sall	$10, %r13d
	addl	$2, %ebx
	leal	-56613888(%rax,%r13), %r13d
	cmpl	$65535, %r13d
	jbe	.L192
	cmpl	$1114111, %r13d
	ja	.L115
	movl	%r13d, %eax
	movq	64(%r15), %rdi
	movl	%esi, -68(%rbp)
	sarl	$10, %eax
	movq	%rcx, -64(%rbp)
	subw	$10304, %ax
	movl	%edx, -56(%rbp)
	movq	%rax, %r8
	andl	$31, %eax
	salq	$48, %r8
	shrq	$53, %r8
	movzwl	(%rdi,%r8,2), %r8d
	leal	(%rax,%r8,4), %eax
	cltq
	movzwl	(%rdi,%rax,2), %edi
	call	*80(%r15)
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rcx
	testl	%eax, %eax
	movl	-68(%rbp), %esi
	movl	%eax, %r8d
	jle	.L115
	movl	%r13d, %eax
	movq	64(%r15), %rdi
	sarl	$5, %eax
	andl	$31, %eax
	addl	%r8d, %eax
	cltq
	movzwl	(%rdi,%rax,2), %r8d
	movl	%r13d, %eax
	andl	$31, %eax
	leal	(%rax,%r8,4), %eax
	cltq
	movzwl	(%rdi,%rax,2), %eax
.L117:
	testw	%ax, %ax
	je	.L118
	movzwl	%ax, %r8d
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L115:
	movzwl	96(%r15), %eax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L123:
	cwtl
	sarl	$2, %eax
	subl	%eax, %r13d
	cmpl	$16319, %r8d
	jne	.L118
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L133:
	movl	20(%r15), %edi
	cmpl	%eax, %r8d
	jge	.L135
	cmpl	%edi, %r8d
	jge	.L137
.L138:
	movq	104(%r15), %rdi
	movslq	%r8d, %rax
	addl	$1, %r8d
	movzwl	(%rdi,%rax,2), %r11d
	testl	%r11d, %r11d
	je	.L124
.L134:
	cmpl	%r14d, %esi
	jle	.L139
	leal	(%r14,%r11), %r9d
	movslq	%r8d, %rax
	movq	104(%r15), %r13
	movslq	%r14d, %rdi
	cmpl	%esi, %r9d
	cmovg	%esi, %r9d
	addq	%rax, %rax
	.p2align 4,,10
	.p2align 3
.L140:
	movzwl	0(%r13,%rax), %r8d
	addq	$2, %rax
	movw	%r8w, (%rcx,%rdi,2)
	addq	$1, %rdi
	cmpl	%edi, %r9d
	jg	.L140
.L139:
	leal	-1(%r11), %eax
	testl	%r11d, %r11d
	movl	$0, %edi
	cmovle	%edi, %eax
	leal	1(%r14,%rax), %r14d
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$1, %r11d
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L188:
	xorl	%eax, %eax
	movq	-88(%rbp), %r15
	cmpl	$65535, %r13d
	movq	%r12, %r10
	seta	%al
	movl	%edx, %r12d
	addl	$1, %eax
	testq	%r15, %r15
	je	.L128
	subl	%eax, %ebx
	movl	$15, %eax
	leaq	8(%r15), %rdi
	movq	%r10, -56(%rbp)
	cmpl	$15, %ebx
	movl	%ebx, 4(%r15)
	movl	%ebx, %r13d
	cmovge	%ebx, %eax
	movl	$0, (%r15)
	subl	$15, %eax
	subl	%eax, %r13d
	cltq
	leaq	(%r10,%rax,2), %rsi
	movl	%r13d, %edx
	movslq	%r13d, %r13
	call	u_memcpy_67@PLT
	leal	15(%rbx), %eax
	xorl	%ecx, %ecx
	movq	-56(%rbp), %r10
	cmpl	%eax, %r12d
	movw	%cx, 8(%r15,%r13,2)
	cmovle	%r12d, %eax
	movl	%eax, %r13d
	subl	%ebx, %r13d
	cmpl	%r12d, %ebx
	jl	.L193
.L129:
	movq	-88(%rbp), %rax
	movslq	%r13d, %r13
	xorl	%edx, %edx
	movw	%dx, 40(%rax,%r13,2)
.L128:
	movq	-80(%rbp), %rax
	movl	$66561, (%rax)
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	cmpl	%r8d, 24(%r15)
	jle	.L138
	movl	$3, %r11d
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L193:
	movq	-88(%rbp), %rax
	movslq	%ebx, %rbx
	movl	%r13d, %edx
	leaq	(%r10,%rbx,2), %rsi
	leaq	40(%rax), %rdi
	call	u_memcpy_67@PLT
	jmp	.L129
.L144:
	xorl	%edx, %edx
	jmp	.L106
.L192:
	movl	%ebx, %edi
	jmp	.L108
	.cfi_endproc
.LFE3096:
	.size	_ZL10usprep_mapPK18UStringPrepProfilePKDsiPDsiiP11UParseErrorP10UErrorCode, .-_ZL10usprep_mapPK18UStringPrepProfilePKDsiPDsiiP11UParseErrorP10UErrorCode
	.p2align 4
	.globl	usprep_open_67
	.type	usprep_open_67, @function
usprep_open_67:
.LFB3091:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L194
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L194
	jmp	_ZL17usprep_getProfilePKcS0_P10UErrorCode
	.p2align 4,,10
	.p2align 3
.L194:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3091:
	.size	usprep_open_67, .-usprep_open_67
	.p2align 4
	.globl	usprep_openByType_67
	.type	usprep_openByType_67, @function
usprep_openByType_67:
.LFB3092:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	testq	%rsi, %rsi
	je	.L199
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L199
	cmpl	$13, %edi
	ja	.L205
	movslq	%edi, %rdi
	leaq	_ZL13PROFILE_NAMES(%rip), %rax
	movq	(%rax,%rdi,8), %rsi
	xorl	%edi, %edi
	jmp	_ZL17usprep_getProfilePKcS0_P10UErrorCode
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$1, (%rsi)
.L199:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3092:
	.size	usprep_openByType_67, .-usprep_openByType_67
	.p2align 4
	.globl	usprep_close_67
	.type	usprep_close_67, @function
usprep_close_67:
.LFB3093:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L206
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	_ZL11usprepMutex(%rip), %rdi
	subq	$8, %rsp
	call	umtx_lock_67@PLT
	movl	120(%rbx), %eax
	testl	%eax, %eax
	jle	.L208
	subl	$1, %eax
	movl	%eax, 120(%rbx)
.L208:
	addq	$8, %rsp
	leaq	_ZL11usprepMutex(%rip), %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	umtx_unlock_67@PLT
	.p2align 4,,10
	.p2align 3
.L206:
	ret
	.cfi_endproc
.LFE3093:
	.size	usprep_close_67, .-usprep_close_67
	.p2align 4
	.globl	uprv_syntaxError_67
	.type	uprv_syntaxError_67, @function
uprv_syntaxError_67:
.LFB3094:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L219
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$15, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	movl	%ebx, %r13d
	subq	$8, %rsp
	cmpl	$15, %ebx
	movl	%ebx, 4(%rcx)
	cmovge	%ebx, %eax
	movl	$0, (%rcx)
	subl	$15, %eax
	subl	%eax, %r13d
	cltq
	movl	%r13d, %edx
	leaq	(%rdi,%rax,2), %rsi
	leaq	8(%rcx), %rdi
	movslq	%r13d, %r13
	call	u_memcpy_67@PLT
	leal	15(%rbx), %eax
	xorl	%edx, %edx
	cmpl	%eax, %r15d
	movw	%dx, 8(%r12,%r13,2)
	cmovle	%r15d, %eax
	movl	%eax, %r13d
	subl	%ebx, %r13d
	cmpl	%r15d, %ebx
	jl	.L222
.L214:
	movslq	%r13d, %r13
	xorl	%eax, %eax
	movw	%ax, 40(%r12,%r13,2)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	leaq	(%r14,%rbx,2), %rsi
	leaq	40(%r12), %rdi
	movl	%r13d, %edx
	call	u_memcpy_67@PLT
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE3094:
	.size	uprv_syntaxError_67, .-uprv_syntaxError_67
	.p2align 4
	.globl	usprep_prepare_67
	.type	usprep_prepare_67, @function
usprep_prepare_67:
.LFB3097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %r12
	movq	%rcx, -232(%rbp)
	movl	%r8d, -236(%rbp)
	movq	%rax, -248(%rbp)
	movl	(%r12), %r11d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r11d, %r11d
	jg	.L223
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L225
	movq	%rsi, %r15
	movl	%edx, %r13d
	testq	%rsi, %rsi
	je	.L331
	cmpl	$-1, %edx
	jl	.L225
.L227:
	cmpq	$0, -232(%rbp)
	je	.L332
	movl	-236(%rbp), %r8d
	testl	%r8d, %r8d
	js	.L225
.L229:
	testl	%r13d, %r13d
	js	.L333
.L230:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	movl	%r13d, %esi
	movl	%r9d, -240(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-192(%rbp), %rax
	movw	%di, -184(%rbp)
	movq	%rax, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movl	-240(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L236
	pushq	%r12
	movl	$27, %r14d
	movl	%r13d, %edx
	movq	%r15, %rsi
	pushq	-248(%rbp)
	testb	$2, -184(%rbp)
	movl	%r14d, %r8d
	movq	%rbx, %rdi
	cmove	-176(%rbp), %r8d
	movl	%r9d, -260(%rbp)
	call	_ZL10usprep_mapPK18UStringPrepProfilePKDsiPDsiiP11UParseErrorP10UErrorCode
	movl	(%r12), %esi
	movq	-256(%rbp), %rdi
	popq	%rdx
	popq	%rcx
	movl	%eax, -240(%rbp)
	testl	%esi, %esi
	movl	$0, %esi
	cmovle	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movl	(%r12), %ecx
	movl	-240(%rbp), %eax
	movl	-260(%rbp), %r9d
	cmpl	$15, %ecx
	je	.L334
.L235:
	xorl	%r14d, %r14d
	testl	%ecx, %ecx
	jg	.L232
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r10d
	cmpb	$0, 125(%rbx)
	movq	%rax, -128(%rbp)
	movw	%r10w, -120(%rbp)
	je	.L239
	movq	%r12, %rdi
	call	_ZN6icu_6711Normalizer215getNFKCInstanceER10UErrorCode@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	uniset_getUnicode32Instance_67@PLT
	movl	(%r12), %r9d
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rdx
	movq	%r13, -216(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%rax, -208(%rbp)
	testl	%r9d, %r9d
	jg	.L335
	leaq	-128(%rbp), %rax
	leaq	-224(%rbp), %r13
	movq	-256(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%rax, -272(%rbp)
	call	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$7, (%r12)
	xorl	%r14d, %r14d
.L232:
	movq	-256(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L223:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L336
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L227
	.p2align 4,,10
	.p2align 3
.L225:
	movl	$1, (%r12)
	xorl	%r14d, %r14d
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%r15, %rdi
	movl	%r9d, -256(%rbp)
	call	u_strlen_67@PLT
	movl	-256(%rbp), %r9d
	movl	%eax, %r13d
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L332:
	movl	-236(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L229
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L239:
	leaq	-128(%rbp), %rax
	movq	-256(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_@PLT
.L242:
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jg	.L329
	movswl	-120(%rbp), %r15d
	testb	$17, %r15b
	jne	.L278
	leaq	-118(%rbp), %rcx
	testb	$2, %r15b
	cmove	-104(%rbp), %rcx
.L244:
	testw	%r15w, %r15w
	js	.L245
	sarl	$5, %r15d
.L246:
	testl	%r15d, %r15d
	jle	.L247
	xorl	%r14d, %r14d
	movq	%rbx, %r13
	movq	%r12, -288(%rbp)
	movl	%r15d, %ebx
	movl	$-1, -280(%rbp)
	movl	$23, %edx
	movl	%r14d, %r15d
	movq	%rcx, %r12
	movl	$-1, -276(%rbp)
	movb	$0, -261(%rbp)
	movb	$0, -262(%rbp)
	movl	$23, -240(%rbp)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L339:
	cmpw	$-14, %ax
	je	.L328
.L257:
	cmpb	$0, 126(%r13)
	jne	.L337
.L263:
	cmpl	%r15d, %ebx
	jle	.L249
.L248:
	movslq	%r15d, %rax
	leal	1(%r15), %r9d
	movzwl	(%r12,%rax,2), %r14d
	leaq	(%rax,%rax), %rdi
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L250
	cmpl	%ebx, %r9d
	jne	.L338
.L250:
	leal	-55296(%r14), %eax
	movl	%r9d, %r15d
	movq	64(%r13), %rdi
	cmpl	$1024, %eax
	sbbl	%r9d, %r9d
	andl	$320, %r9d
.L253:
	movl	%r14d, %eax
	sarl	$5, %eax
.L327:
	addl	%r9d, %eax
	cltq
	movzwl	(%rdi,%rax,2), %r9d
	movl	%r14d, %eax
	andl	$31, %eax
	leal	(%rax,%r9,4), %eax
	cltq
	movzwl	(%rdi,%rax,2), %eax
.L254:
	testw	%ax, %ax
	je	.L257
	cmpw	$-17, %ax
	ja	.L339
	testb	$1, %al
	je	.L257
.L328:
	movl	%r14d, %r13d
	xorl	%eax, %eax
	movq	%r12, %rcx
	movq	-288(%rbp), %r12
	cmpl	$65535, %r13d
	movq	-248(%rbp), %r13
	movl	%r15d, %r14d
	movl	%ebx, %r15d
	seta	%al
	movl	$66560, (%r12)
	addl	$1, %eax
	testq	%r13, %r13
	je	.L329
	subl	%eax, %r14d
	movl	$15, %eax
	movl	$0, 0(%r13)
	leaq	8(%r13), %rdi
	cmpl	$15, %r14d
	movl	%r14d, 4(%r13)
	movl	%r14d, %ebx
	cmovge	%r14d, %eax
	movq	%rcx, -232(%rbp)
	subl	$15, %eax
	subl	%eax, %ebx
	cltq
	leaq	(%rcx,%rax,2), %rsi
	movl	%ebx, %edx
	movslq	%ebx, %rbx
	call	u_memcpy_67@PLT
	leal	15(%r14), %eax
	xorl	%edi, %edi
	movq	-232(%rbp), %rcx
	cmpl	%eax, %r15d
	movw	%di, 8(%r13,%rbx,2)
	cmovle	%r15d, %eax
	movl	%eax, %ebx
	subl	%r14d, %ebx
	cmpl	%r15d, %r14d
	jl	.L340
.L262:
	movslq	%ebx, %rbx
.L330:
	movq	-248(%rbp), %rax
	xorl	%esi, %esi
	movw	%si, 40(%rax,%rbx,2)
.L329:
	xorl	%r14d, %r14d
.L241:
	movq	-272(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	-224(%rbp), %rdi
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
	leaq	-128(%rbp), %rax
	movq	%rax, -272(%rbp)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L337:
	movl	%r14d, %edi
	call	ubidi_getClass_67@PLT
	movl	%eax, %edx
	movl	-240(%rbp), %eax
	cmpl	$23, %eax
	cmove	%edx, %eax
	movl	%eax, -240(%rbp)
	testl	%edx, %edx
	jne	.L265
	leal	-1(%r15), %eax
	movb	$1, -262(%rbp)
	movl	%eax, -280(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L338:
	movzwl	2(%r12,%rdi), %eax
	movl	%eax, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	je	.L341
	movl	%r9d, %r15d
	movq	64(%r13), %rdi
	movl	$320, %r9d
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L341:
	sall	$10, %r14d
	addl	$2, %r15d
	leal	-56613888(%rax,%r14), %r14d
	cmpl	$65535, %r14d
	jbe	.L342
	cmpl	$1114111, %r14d
	ja	.L255
	movl	%r14d, %eax
	movq	64(%r13), %rdi
	movl	%edx, -260(%rbp)
	sarl	$10, %eax
	subw	$10304, %ax
	movq	%rax, %r9
	andl	$31, %eax
	salq	$48, %r9
	shrq	$53, %r9
	movzwl	(%rdi,%r9,2), %r9d
	leal	(%rax,%r9,4), %eax
	cltq
	movzwl	(%rdi,%rax,2), %edi
	call	*80(%r13)
	movl	-260(%rbp), %edx
	testl	%eax, %eax
	movl	%eax, %r9d
	jle	.L255
	movl	%r14d, %eax
	movq	64(%r13), %rdi
	sarl	$5, %eax
	andl	$31, %eax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L255:
	movzwl	96(%r13), %eax
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L265:
	cmpl	$1, %edx
	je	.L282
	cmpl	$13, %edx
	jne	.L263
.L282:
	leal	-1(%r15), %eax
	movb	$1, -261(%rbp)
	movl	%eax, -276(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L334:
	movq	-256(%rbp), %rdi
	movl	%eax, %esi
	movl	%r9d, -240(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L236
	testb	$2, -184(%rbp)
	movl	%r14d, %r8d
	movl	%r13d, %edx
	movq	%r15, %rsi
	movl	$0, (%r12)
	cmove	-176(%rbp), %r8d
	movq	%rbx, %rdi
	pushq	%r12
	movl	-240(%rbp), %r9d
	pushq	-248(%rbp)
	call	_ZL10usprep_mapPK18UStringPrepProfilePKDsiPDsiiP11UParseErrorP10UErrorCode
	movl	(%r12), %r14d
	movq	-256(%rbp), %rdi
	movl	%eax, %esi
	movl	$0, %eax
	popq	%r11
	popq	%r13
	testl	%r14d, %r14d
	cmovg	%eax, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movl	(%r12), %ecx
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L249:
	cmpb	$1, 126(%r13)
	movq	%r12, %rcx
	movl	%ebx, %r15d
	movq	-288(%rbp), %r12
	jne	.L247
	movzbl	-261(%rbp), %ebx
	testb	%bl, -262(%rbp)
	jne	.L343
	cmpb	$1, -261(%rbp)
	je	.L344
.L247:
	movq	-232(%rbp), %rax
	movl	-236(%rbp), %edx
	movq	%r12, %rcx
	leaq	-224(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	%rax, -224(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r14d
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L245:
	movl	-116(%rbp), %r15d
	jmp	.L246
.L278:
	xorl	%ecx, %ecx
	jmp	.L244
.L340:
	movq	-248(%rbp), %rax
	movslq	%r14d, %r14
	movl	%ebx, %edx
	leaq	(%rcx,%r14,2), %rsi
	leaq	40(%rax), %rdi
	call	u_memcpy_67@PLT
	jmp	.L262
.L343:
	movq	-248(%rbp), %r14
	movl	$66562, (%r12)
	testq	%r14, %r14
	je	.L329
	movslq	-276(%rbp), %rax
	movslq	-280(%rbp), %rbx
	leaq	8(%r14), %rdi
	movl	$0, (%r14)
	movq	%rcx, -232(%rbp)
	cmpl	%ebx, %eax
	cmovge	%rax, %rbx
	movl	$15, %eax
	cmpl	$15, %ebx
	movl	%ebx, 4(%r14)
	movl	%ebx, %r13d
	cmovge	%ebx, %eax
	subl	$15, %eax
	subl	%eax, %r13d
	cltq
	leaq	(%rcx,%rax,2), %rsi
	movl	%r13d, %edx
	movslq	%r13d, %r13
	call	u_memcpy_67@PLT
	leal	15(%rbx), %eax
	xorl	%ecx, %ecx
	cmpl	%eax, %r15d
	movw	%cx, 8(%r14,%r13,2)
	movq	-232(%rbp), %rcx
	cmovle	%r15d, %eax
	movl	%eax, %r13d
	subl	%ebx, %r13d
	cmpl	%r15d, %ebx
	jl	.L345
.L268:
	movq	-248(%rbp), %rax
	movslq	%r13d, %r13
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	movw	%dx, 40(%rax,%r13,2)
	jmp	.L241
.L344:
	movl	-240(%rbp), %eax
	cmpl	$1, %eax
	je	.L283
	cmpl	$13, %eax
	je	.L283
.L269:
	movq	-248(%rbp), %r13
	movl	$66562, (%r12)
	testq	%r13, %r13
	je	.L329
	movl	-276(%rbp), %r14d
	movl	$15, %eax
	leaq	8(%r13), %rdi
	movl	$0, 0(%r13)
	movq	%rcx, -232(%rbp)
	cmpl	$15, %r14d
	movl	%r14d, 4(%r13)
	movl	%r14d, %ebx
	cmovge	%r14d, %eax
	subl	$15, %eax
	subl	%eax, %ebx
	cltq
	leaq	(%rcx,%rax,2), %rsi
	movl	%ebx, %edx
	movslq	%ebx, %rbx
	call	u_memcpy_67@PLT
	xorl	%eax, %eax
	movq	-232(%rbp), %rcx
	movw	%ax, 8(%r13,%rbx,2)
	leal	15(%r14), %eax
	cmpl	%eax, %r15d
	cmovle	%r15d, %eax
	subl	%r14d, %eax
	cmpl	%r15d, %r14d
	movslq	%eax, %rbx
	jge	.L330
	movslq	-276(%rbp), %rsi
	movq	-248(%rbp), %rax
	movl	%ebx, %edx
	addq	%rsi, %rsi
	leaq	40(%rax), %rdi
	addq	%rcx, %rsi
	call	u_memcpy_67@PLT
	jmp	.L330
.L283:
	cmpl	$1, %edx
	je	.L247
	cmpl	$13, %edx
	jne	.L269
	jmp	.L247
.L345:
	movq	-248(%rbp), %rax
	addq	%rbx, %rbx
	movl	%r13d, %edx
	leaq	(%rcx,%rbx), %rsi
	leaq	40(%rax), %rdi
	call	u_memcpy_67@PLT
	jmp	.L268
.L336:
	call	__stack_chk_fail@PLT
.L342:
	movl	%r15d, %r9d
	jmp	.L250
	.cfi_endproc
.LFE3097:
	.size	usprep_prepare_67, .-usprep_prepare_67
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"usprep_swap(): data format %02x.%02x.%02x.%02x (format version %02x) is not recognized as StringPrep .spp data\n"
	.align 8
.LC2:
	.string	"usprep_swap(): too few bytes (%d after header) for StringPrep .spp data\n"
	.align 8
.LC3:
	.string	"usprep_swap(): too few bytes (%d after header) for all of StringPrep .spp data\n"
	.text
	.p2align 4
	.globl	usprep_swap_67
	.type	usprep_swap_67, @function
usprep_swap_67:
.LFB3098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	udata_swapDataHeader_67@PLT
	xorl	%r8d, %r8d
	movl	%eax, -148(%rbp)
	testq	%rbx, %rbx
	je	.L346
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L346
	cmpw	$20563, 12(%r15)
	movzbl	16(%r15), %eax
	jne	.L348
	cmpw	$20562, 14(%r15)
	jne	.L348
	cmpb	$3, %al
	jne	.L348
	movslq	-148(%rbp), %rax
	movq	%rax, -160(%rbp)
	addq	%rax, %r15
	testl	%r12d, %r12d
	js	.L350
	subl	%eax, %r12d
	cmpl	$63, %r12d
	jg	.L350
	movl	%r12d, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	%r8d, -136(%rbp)
	call	udata_printError_67@PLT
	movl	$8, (%rbx)
	movl	-136(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L346:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L360
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	subq	$8, %rsp
	movzbl	12(%r15), %edx
	movzbl	13(%r15), %ecx
	movq	%r14, %rdi
	pushq	%rax
	movzbl	14(%r15), %r8d
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	movzbl	15(%r15), %r9d
	call	udata_printError_67@PLT
	popq	%rax
	movl	$16, (%rbx)
	xorl	%r8d, %r8d
	popq	%rdx
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L350:
	leaq	-128(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L351:
	movl	(%r15,%rdx), %esi
	movq	%r14, %rdi
	movq	%rdx, -136(%rbp)
	call	udata_readInt32_67@PLT
	movq	-136(%rbp), %rdx
	movq	-144(%rbp), %rcx
	movl	%eax, (%rcx,%rdx)
	addq	$4, %rdx
	cmpq	$64, %rdx
	jne	.L351
	movl	-128(%rbp), %r11d
	movl	-124(%rbp), %r10d
	leal	64(%r11), %r9d
	leal	(%r9,%r10), %eax
	movl	%eax, -136(%rbp)
	testl	%r12d, %r12d
	js	.L352
	cmpl	%eax, %r12d
	jl	.L361
	addq	-160(%rbp), %r13
	cmpq	%r13, %r15
	je	.L354
	movslq	-136(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%r10d, -152(%rbp)
	movl	%r9d, -160(%rbp)
	movl	%r11d, -144(%rbp)
	call	memcpy@PLT
	movl	-152(%rbp), %r10d
	movl	-160(%rbp), %r9d
	movl	-144(%rbp), %r11d
.L354:
	movl	%r10d, -152(%rbp)
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	movl	%r9d, -160(%rbp)
	movl	$64, %edx
	movq	%r14, %rdi
	movl	%r11d, -144(%rbp)
	call	*56(%r14)
	leaq	64(%r13), %rcx
	leaq	64(%r15), %rsi
	movq	%rbx, %r8
	movl	-144(%rbp), %r11d
	movq	%r14, %rdi
	movl	%r11d, %edx
	call	utrie_swap_67@PLT
	movslq	-160(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r14, %rdi
	movl	-152(%rbp), %r10d
	leaq	0(%r13,%rsi), %rcx
	addq	%r15, %rsi
	movl	%r10d, %edx
	call	*48(%r14)
.L352:
	movl	-148(%rbp), %r8d
	addl	-136(%rbp), %r8d
	jmp	.L346
.L361:
	movl	%r12d, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$8, (%rbx)
	xorl	%r8d, %r8d
	jmp	.L346
.L360:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3098:
	.size	usprep_swap_67, .-usprep_swap_67
	.section	.rodata.str1.1
.LC4:
	.string	"rfc3491"
.LC5:
	.string	"rfc3530cs"
.LC6:
	.string	"rfc3530csci"
.LC7:
	.string	"rfc3530mixp"
.LC8:
	.string	"rfc3722"
.LC9:
	.string	"rfc3920node"
.LC10:
	.string	"rfc3920res"
.LC11:
	.string	"rfc4011"
.LC12:
	.string	"rfc4013"
.LC13:
	.string	"rfc4505"
.LC14:
	.string	"rfc4518"
.LC15:
	.string	"rfc4518ci"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL13PROFILE_NAMES, @object
	.size	_ZL13PROFILE_NAMES, 112
_ZL13PROFILE_NAMES:
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC4
	.quad	.LC7
	.quad	.LC4
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.local	_ZL11dataVersion
	.comm	_ZL11dataVersion,4,1
	.local	_ZL11usprepMutex
	.comm	_ZL11usprepMutex,56,32
	.local	_ZL19gSharedDataInitOnce
	.comm	_ZL19gSharedDataInitOnce,8,8
	.local	_ZL21SHARED_DATA_HASHTABLE
	.comm	_ZL21SHARED_DATA_HASHTABLE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
