	.file	"utrie.cpp"
	.text
	.p2align 4
	.globl	utrie_defaultGetFoldingOffset_67
	.type	utrie_defaultGetFoldingOffset_67, @function
utrie_defaultGetFoldingOffset_67:
.LFB2071:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE2071:
	.size	utrie_defaultGetFoldingOffset_67, .-utrie_defaultGetFoldingOffset_67
	.p2align 4
	.type	_ZL13enumSameValuePKvj, @function
_ZL13enumSameValuePKvj:
.LFB2074:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE2074:
	.size	_ZL13enumSameValuePKvj, .-_ZL13enumSameValuePKvj
	.p2align 4
	.type	_ZL13utrie_compactP8UNewTrieaP10UErrorCode, @function
_ZL13utrie_compactP8UNewTrieaP10UErrorCode:
.LFB2068:
	.cfi_startproc
	testq	%rdx, %rdx
	je	.L82
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L82
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdi, %rdi
	je	.L86
	cmpb	$0, 139419(%rdi)
	je	.L87
.L4:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L87:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	%esi, %r12d
	movl	%esi, %r14d
	movl	$139396, %edx
	movl	$255, %esi
	leaq	139420(%rdi), %rdi
	call	memset@PLT
	movl	139404(%rbx), %esi
	testl	%esi, %esi
	jle	.L10
	leal	-1(%rsi), %eax
	movq	%rbx, %rdx
	leaq	4(%rbx,%rax,4), %rdi
	.p2align 4,,10
	.p2align 3
.L11:
	movl	(%rdx), %ecx
	movl	(%rdx), %eax
	addq	$4, %rdx
	sarl	$31, %ecx
	xorl	%ecx, %eax
	subl	%ecx, %eax
	sarl	$5, %eax
	cltq
	movl	$0, 139420(%rbx,%rax,4)
	cmpq	%rdi, %rdx
	jne	.L11
.L10:
	cmpb	$1, 139418(%rbx)
	movl	139412(%rbx), %r13d
	movl	$0, 139420(%rbx)
	sbbl	%r15d, %r15d
	xorb	%r15b, %r15b
	addl	$288, %r15d
	cmpl	$32, %r13d
	jle	.L88
	cmpb	$1, %r12b
	movb	%r14b, -57(%rbp)
	movl	$32, %r9d
	movl	$32, %r8d
	sbbl	%r11d, %r11d
	andl	$28, %r11d
	addl	$4, %r11d
	movslq	%r11d, %r12
	salq	$2, %r12
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	%r8d, %r15d
	jle	.L89
.L18:
	cmpl	%r8d, %r9d
	jl	.L90
.L30:
	movl	%r8d, 139420(%rdi)
	leal	32(%r9), %r8d
	movl	139412(%rbx), %r13d
	movl	%r8d, %r9d
.L17:
	cmpl	%r13d, %r8d
	jge	.L91
.L14:
	movl	%r8d, %eax
	sarl	$5, %eax
	cltq
	leaq	(%rbx,%rax,4), %rdi
	movl	139420(%rdi), %eax
	testl	%eax, %eax
	jns	.L16
	addl	$32, %r8d
.L93:
	cmpl	%r13d, %r8d
	jl	.L14
.L91:
	movl	139404(%rbx), %esi
.L12:
	testl	%esi, %esi
	jle	.L35
	leal	-1(%rsi), %eax
	movq	%rbx, %rdx
	leaq	4(%rbx,%rax,4), %rsi
	.p2align 4,,10
	.p2align 3
.L36:
	movl	(%rdx), %ecx
	movl	(%rdx), %eax
	addq	$4, %rdx
	sarl	$31, %ecx
	xorl	%ecx, %eax
	subl	%ecx, %eax
	sarl	$5, %eax
	cltq
	movl	139420(%rbx,%rax,4), %eax
	movl	%eax, -4(%rdx)
	cmpq	%rdx, %rsi
	jne	.L36
.L35:
	movl	%r9d, 139412(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	cmpb	$0, -57(%rbp)
	movq	139392(%rbx), %r10
	leal	-32(%r9), %r14d
	je	.L92
	movslq	%r8d, %rax
	leaq	(%r10,%rax,4), %rdx
	testl	%r14d, %r14d
	js	.L43
.L44:
	movq	%r10, %rcx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%r10, -56(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L23:
	movl	(%rdx,%rax), %r10d
	cmpl	%r10d, (%rcx,%rax)
	jne	.L21
	addq	$4, %rax
	cmpq	$128, %rax
	jne	.L23
	movl	%esi, 139420(%rdi)
	addl	$32, %r8d
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L21:
	addl	%r11d, %esi
	movq	-56(%rbp), %r10
	addq	%r12, %rcx
	cmpl	%r14d, %esi
	jle	.L41
	cmpb	$0, -57(%rbp)
	je	.L18
.L43:
	movslq	%r9d, %rax
	movl	$28, %esi
	movq	%rax, -56(%rbp)
	leaq	-112(%r10,%rax,4), %r13
	.p2align 4,,10
	.p2align 3
.L27:
	movl	%r9d, %r14d
	xorl	%eax, %eax
	subl	%esi, %r14d
	.p2align 4,,10
	.p2align 3
.L26:
	movl	(%rdx,%rax,4), %ecx
	cmpl	%ecx, 0(%r13,%rax,4)
	jne	.L24
	addq	$1, %rax
	movl	%esi, %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jg	.L26
	movl	$31, %eax
	leal	(%r8,%rsi), %r13d
	movl	$32, %r8d
	movl	%r14d, 139420(%rdi)
	subl	%esi, %eax
	subl	%esi, %r8d
	movslq	%r13d, %rcx
	movl	%eax, %esi
	movq	-56(%rbp), %rax
	leaq	0(,%rcx,4), %rdx
	leaq	16(,%rcx,4), %r14
	salq	$2, %rax
	leaq	16(%rax), %rdi
	cmpq	%rdx, %rdi
	setle	-58(%rbp)
	cmpq	%rax, %r14
	movzbl	-58(%rbp), %r14d
	setle	%dil
	orb	%dil, %r14b
	je	.L39
	cmpl	$3, %esi
	jbe	.L39
	addq	%r10, %rdx
	movl	%r8d, %ecx
	addq	%r10, %rax
	movdqu	(%rdx), %xmm0
	shrl	$2, %ecx
	movups	%xmm0, (%rax)
	cmpl	$1, %ecx
	je	.L28
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rax)
	cmpl	$2, %ecx
	je	.L28
	movdqu	32(%rdx), %xmm2
	movups	%xmm2, 32(%rax)
	cmpl	$3, %ecx
	je	.L28
	movdqu	48(%rdx), %xmm3
	movups	%xmm3, 48(%rax)
	cmpl	$4, %ecx
	je	.L28
	movdqu	64(%rdx), %xmm4
	movups	%xmm4, 64(%rax)
	cmpl	$5, %ecx
	je	.L28
	movdqu	80(%rdx), %xmm5
	movups	%xmm5, 80(%rax)
	cmpl	$6, %ecx
	je	.L28
	movdqu	96(%rdx), %xmm6
	movups	%xmm6, 96(%rax)
.L28:
	leal	1(%r13,%rsi), %r8d
	leal	1(%r9,%rsi), %r9d
	movl	139412(%rbx), %r13d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$16, %r13
	subl	$4, %esi
	jne	.L27
	cmpl	%r8d, %r9d
	jge	.L30
.L90:
	movl	%r9d, 139420(%rdi)
	movslq	%r8d, %rdi
	movslq	%r9d, %rcx
	movq	139392(%rbx), %rsi
	leaq	0(,%rdi,4), %rdx
	leaq	0(,%rcx,4), %rax
	leaq	16(%rdx), %r10
	leaq	16(,%rcx,4), %r13
	cmpq	%r10, %rax
	jge	.L47
	cmpq	%r13, %rdx
	jl	.L31
.L47:
	addq	%rsi, %rdx
	addq	%rsi, %rax
	movdqu	(%rdx), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%rdx), %xmm2
	movups	%xmm2, 32(%rax)
	movdqu	48(%rdx), %xmm3
	movups	%xmm3, 48(%rax)
	movdqu	64(%rdx), %xmm4
	movups	%xmm4, 64(%rax)
	movdqu	80(%rdx), %xmm5
	movups	%xmm5, 80(%rax)
	movdqu	96(%rdx), %xmm6
	movups	%xmm6, 96(%rax)
	movdqu	112(%rdx), %xmm7
	movups	%xmm7, 112(%rax)
.L33:
	movl	139412(%rbx), %r13d
	addl	$32, %r8d
	addl	$32, %r9d
	jmp	.L17
.L92:
	testl	%r14d, %r14d
	js	.L18
	movslq	%r8d, %rax
	leaq	(%r10,%rax,4), %rdx
	jmp	.L44
.L86:
	movl	$1, (%rdx)
	jmp	.L4
.L31:
	leaq	(%rsi,%rdx), %rax
	subq	%rdi, %rcx
	leaq	128(%rsi,%rdx), %rsi
	.p2align 4,,10
	.p2align 3
.L34:
	movl	(%rax), %edx
	movl	%edx, (%rax,%rcx,4)
	addq	$4, %rax
	cmpq	%rsi, %rax
	jne	.L34
	jmp	.L33
.L88:
	movl	$32, %r9d
	jmp	.L12
.L39:
	leal	-1(%r8), %edx
	leaq	(%r10,%rcx,4), %rax
	addq	%rcx, %rdx
	leaq	4(%r10,%rdx,4), %rdi
	movq	-56(%rbp), %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
.L29:
	movl	(%rax), %edx
	movl	%edx, (%rax,%rcx,4)
	addq	$4, %rax
	cmpq	%rax, %rdi
	jne	.L29
	jmp	.L28
	.cfi_endproc
.LFE2068:
	.size	_ZL13utrie_compactP8UNewTrieaP10UErrorCode, .-_ZL13utrie_compactP8UNewTrieaP10UErrorCode
	.p2align 4
	.type	utrie_open_67.part.0, @function
utrie_open_67.part.0:
.LFB2538:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movl	%ecx, -52(%rbp)
	movl	%r8d, -56(%rbp)
	testq	%rdi, %rdi
	je	.L110
.L95:
	xorl	%esi, %esi
	movl	$278816, %edx
	movq	%r12, %rdi
	call	memset@PLT
	testq	%r13, %r13
	sete	139416(%r12)
	testq	%r15, %r15
	je	.L97
	movq	%r15, 139392(%r12)
	movb	$0, 139417(%r12)
.L98:
	movl	$32, %edx
	testb	%r14b, %r14b
	jne	.L111
.L100:
	movslq	%edx, %rax
	movl	%edx, 139412(%r12)
	shrl	$2, %edx
	movd	-52(%rbp), %xmm1
	leaq	-16(%r15,%rax,4), %rax
	salq	$4, %rdx
	movq	%rax, %rcx
	pshufd	$0, %xmm1, %xmm0
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L101:
	movups	%xmm0, (%rax)
	subq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L101
	movl	-56(%rbp), %eax
	movl	%ebx, 139408(%r12)
	movl	$34816, 139404(%r12)
	movl	%eax, 139400(%r12)
	movb	%r14b, 139418(%r12)
	movb	$0, 139419(%r12)
.L94:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movdqa	.LC0(%rip), %xmm0
	movl	$288, %edx
	movups	%xmm0, (%r12)
	movdqa	.LC1(%rip), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$278816, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L95
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L97:
	leal	0(,%rbx,4), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 139392(%r12)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L112
	movb	$1, 139417(%r12)
	jmp	.L98
.L112:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	jmp	.L94
	.cfi_endproc
.LFE2538:
	.size	utrie_open_67.part.0, .-utrie_open_67.part.0
	.p2align 4
	.type	_ZL21defaultGetFoldedValueP8UNewTrieii, @function
_ZL21defaultGetFoldedValueP8UNewTrieii:
.LFB2069:
	.cfi_startproc
	endbr64
	movq	139392(%rdi), %r10
	xorl	%eax, %eax
	cmpb	$0, 139419(%rdi)
	leal	1024(%rsi), %r9d
	movl	(%r10), %r11d
	jne	.L131
.L127:
	cmpl	$1114111, %esi
	ja	.L132
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
.L115:
	movl	%esi, %eax
	movl	%esi, %ecx
	sarl	$5, %eax
	andl	$31, %ecx
	cltq
	movl	(%rdi,%rax,4), %r8d
	movl	%r8d, %ebx
	sarl	$31, %ebx
	movl	%ebx, %eax
	xorl	%r8d, %eax
	subl	%ebx, %eax
	addl	%ecx, %eax
	cltq
	movl	(%r10,%rax,4), %eax
	testl	%r8d, %r8d
	je	.L117
	cmpl	%eax, %r11d
	jne	.L118
	addl	$1, %esi
	cmpl	%r9d, %esi
	jge	.L133
.L119:
	cmpl	$1114111, %esi
	jbe	.L115
.L117:
	addl	$32, %esi
	cmpl	%r9d, %esi
	jl	.L119
.L133:
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore 3
	.cfi_restore 6
	addl	$32, %esi
	cmpl	%r9d, %esi
	jl	.L127
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2069:
	.size	_ZL21defaultGetFoldedValueP8UNewTrieii, .-_ZL21defaultGetFoldedValueP8UNewTrieii
	.p2align 4
	.globl	utrie_open_67
	.type	utrie_open_67, @function
utrie_open_67:
.LFB2054:
	.cfi_startproc
	endbr64
	cmpl	$31, %edx
	jle	.L134
	testb	%r9b, %r9b
	je	.L139
	cmpl	$1023, %edx
	jle	.L134
.L139:
	movsbl	%r9b, %r9d
	jmp	utrie_open_67.part.0
	.p2align 4,,10
	.p2align 3
.L134:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2054:
	.size	utrie_open_67, .-utrie_open_67
	.p2align 4
	.globl	utrie_clone_67
	.type	utrie_clone_67, @function
utrie_clone_67:
.LFB2055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rsi, %rsi
	je	.L169
	movq	139392(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L169
	movzbl	139419(%rsi), %r14d
	testb	%r14b, %r14b
	jne	.L169
	movl	139408(%rsi), %r15d
	movq	%rdi, %r13
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L147
	movl	%ecx, %edx
	cmpl	%r15d, %ecx
	jl	.L147
.L148:
	movsbl	139418(%rbx), %r9d
	cmpl	$31, %edx
	jle	.L150
	testb	%r9b, %r9b
	je	.L152
	cmpl	$1023, %edx
	jle	.L150
.L152:
	movl	139400(%rbx), %r8d
	movl	(%rax), %ecx
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	utrie_open_67.part.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L150
	movl	$139392, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movslq	139412(%rbx), %rdx
	movq	139392(%r13), %rdi
	movq	139392(%rbx), %rsi
	salq	$2, %rdx
	call	memcpy@PLT
	movl	139412(%rbx), %eax
	movb	%r14b, 139417(%r13)
	movl	%eax, 139412(%r13)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
.L169:
	xorl	%r13d, %r13d
.L143:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	leal	0(,%r15,4), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L169
	movq	139392(%rbx), %rax
	movl	%r15d, %edx
	movl	$1, %r14d
	jmp	.L148
	.cfi_endproc
.LFE2055:
	.size	utrie_clone_67, .-utrie_clone_67
	.p2align 4
	.globl	utrie_close_67
	.type	utrie_close_67, @function
utrie_close_67:
.LFB2056:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L176
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 139417(%rdi)
	jne	.L179
	cmpb	$0, 139416(%r12)
	jne	.L180
.L170:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	139392(%rdi), %rdi
	call	uprv_free_67@PLT
	cmpb	$0, 139416(%r12)
	movq	$0, 139392(%r12)
	je	.L170
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2056:
	.size	utrie_close_67, .-utrie_close_67
	.p2align 4
	.globl	utrie_getData_67
	.type	utrie_getData_67, @function
utrie_getData_67:
.LFB2057:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L183
	testq	%rsi, %rsi
	je	.L183
	movl	139412(%rdi), %eax
	movl	%eax, (%rsi)
	movq	139392(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2057:
	.size	utrie_getData_67, .-utrie_getData_67
	.p2align 4
	.globl	utrie_set32_67
	.type	utrie_set32_67, @function
utrie_set32_67:
.LFB2060:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L185
	cmpb	$0, 139419(%rdi)
	jne	.L185
	cmpl	$1114111, %esi
	jbe	.L191
.L185:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	movl	%esi, %eax
	sarl	$5, %eax
	cltq
	leaq	(%rdi,%rax,4), %rax
	movslq	(%rax), %rcx
	testl	%ecx, %ecx
	jg	.L186
	movl	139412(%rdi), %r9d
	leal	32(%r9), %r8d
	cmpl	%r8d, 139408(%rdi)
	jl	.L185
	movl	%r8d, 139412(%rdi)
	testl	%r9d, %r9d
	js	.L185
	movl	%r9d, (%rax)
	movq	139392(%rdi), %rax
	salq	$2, %rcx
	movslq	%r9d, %r8
	leaq	(%rax,%r8,4), %r8
	subq	%rcx, %rax
	movl	%r9d, %ecx
	movdqu	(%rax), %xmm0
	movups	%xmm0, (%r8)
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%r8)
	movdqu	32(%rax), %xmm2
	movups	%xmm2, 32(%r8)
	movdqu	48(%rax), %xmm3
	movups	%xmm3, 48(%r8)
	movdqu	64(%rax), %xmm4
	movups	%xmm4, 64(%r8)
	movdqu	80(%rax), %xmm5
	movups	%xmm5, 80(%r8)
	movdqu	96(%rax), %xmm6
	movups	%xmm6, 96(%r8)
	movdqu	112(%rax), %xmm7
	movups	%xmm7, 112(%r8)
.L186:
	movl	%esi, %eax
	andl	$31, %eax
	addl	%ecx, %eax
	movq	139392(%rdi), %rcx
	cltq
	movl	%edx, (%rcx,%rax,4)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2060:
	.size	utrie_set32_67, .-utrie_set32_67
	.p2align 4
	.globl	utrie_get32_67
	.type	utrie_get32_67, @function
utrie_get32_67:
.LFB2061:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L193
	cmpb	$0, 139419(%rdi)
	jne	.L193
	cmpl	$1114111, %esi
	ja	.L193
	movl	%esi, %eax
	sarl	$5, %eax
	cltq
	movl	(%rdi,%rax,4), %eax
	testq	%rdx, %rdx
	je	.L196
	testl	%eax, %eax
	sete	(%rdx)
.L196:
	cltd
	andl	$31, %esi
	xorl	%edx, %eax
	subl	%edx, %eax
	addl	%eax, %esi
	movq	139392(%rdi), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %eax
.L192:
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L192
	movb	$1, (%rdx)
	ret
	.cfi_endproc
.LFE2061:
	.size	utrie_get32_67, .-utrie_get32_67
	.p2align 4
	.globl	utrie_setRange32_67
	.type	utrie_setRange32_67, @function
utrie_setRange32_67:
.LFB2063:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L262
	cmpb	$0, 139419(%rdi)
	jne	.L262
	cmpl	$1114111, %esi
	seta	%r9b
	cmpl	$1114112, %edx
	seta	%al
	orb	%al, %r9b
	jne	.L262
	cmpl	%edx, %esi
	jg	.L262
	je	.L268
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	andl	$31, %r12d
	.cfi_offset 3, -56
	movq	139392(%rdi), %r9
	movl	(%r9), %eax
	andl	$31, %r13d
	jne	.L269
.L214:
	xorl	%ebx, %ebx
	andl	$-32, %edx
	cmpl	%ecx, %eax
	setne	%bl
	negl	%ebx
	cmpl	%edx, %esi
	jge	.L229
	movd	%ecx, %xmm1
	pshufd	$0, %xmm1, %xmm0
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	(%r9,%r11,4), %r11
	testb	%r10b, %r10b
	je	.L231
	movups	%xmm0, (%r11)
	movups	%xmm0, 16(%r11)
	movups	%xmm0, 32(%r11)
	movups	%xmm0, 48(%r11)
	movups	%xmm0, 64(%r11)
	movups	%xmm0, 80(%r11)
	movups	%xmm0, 96(%r11)
	movups	%xmm0, 112(%r11)
.L232:
	addl	$32, %esi
	cmpl	%esi, %edx
	jle	.L229
.L237:
	movl	%esi, %r11d
	sarl	$5, %r11d
	movslq	%r11d, %r11
	leaq	(%rdi,%r11,4), %r13
	movslq	0(%r13), %r11
	testl	%r11d, %r11d
	jg	.L270
	movl	%r11d, %r14d
	negl	%r14d
	movslq	%r14d, %r14
	cmpl	%ecx, (%r9,%r14,4)
	je	.L232
	testl	%r11d, %r11d
	je	.L250
	testb	%r10b, %r10b
	je	.L232
.L250:
	cmpl	$-1, %ebx
	je	.L236
	movl	%ebx, %r11d
	addl	$32, %esi
	negl	%r11d
	movl	%r11d, 0(%r13)
	cmpl	%esi, %edx
	jg	.L237
	.p2align 4,,10
	.p2align 3
.L229:
	testl	%r12d, %r12d
	jne	.L271
.L222:
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	128(%r11), %r13
	.p2align 4,,10
	.p2align 3
.L234:
	cmpl	(%r11), %eax
	jne	.L233
	movl	%ecx, (%r11)
.L233:
	addq	$4, %r11
	cmpq	%r13, %r11
	jne	.L234
	jmp	.L232
.L269:
	movl	%esi, %r11d
	sarl	$5, %r11d
	movslq	%r11d, %r11
	leaq	(%rdi,%r11,4), %r12
	movslq	(%r12), %r11
	testl	%r11d, %r11d
	jg	.L211
	movslq	139412(%rdi), %rbx
	leal	32(%rbx), %r14d
	cmpl	%r14d, 139408(%rdi)
	jl	.L208
	movl	%r14d, 139412(%rdi)
	testl	%ebx, %ebx
	js	.L208
	movl	%ebx, (%r12)
	salq	$2, %r11
	salq	$2, %rbx
	leaq	(%r9,%rbx), %r12
	subq	%r11, %r9
	movdqu	(%r9), %xmm4
	movups	%xmm4, (%r12)
	movdqu	16(%r9), %xmm5
	movups	%xmm5, 16(%r12)
	movdqu	32(%r9), %xmm6
	movups	%xmm6, 32(%r12)
	movdqu	48(%r9), %xmm7
	movups	%xmm7, 48(%r12)
	movdqu	64(%r9), %xmm4
	movups	%xmm4, 64(%r12)
	movdqu	80(%r9), %xmm5
	movups	%xmm5, 80(%r12)
	movdqu	96(%r9), %xmm6
	movups	%xmm6, 96(%r12)
	movdqu	112(%r9), %xmm7
	movups	%xmm7, 112(%r12)
	movq	139392(%rdi), %r9
.L212:
	addl	$32, %esi
	movslq	%r13d, %r13
	leaq	(%r9,%rbx), %r15
	movl	%edx, %r12d
	salq	$2, %r13
	andl	$-32, %esi
	andl	$31, %r12d
	leaq	(%r15,%r13), %r11
	cmpl	%esi, %edx
	jl	.L272
	leaq	128(%r15), %r14
	testb	%r8b, %r8b
	je	.L267
	cmpq	%r11, %r14
	jbe	.L214
	addq	$127, %r15
	subq	%r11, %r15
	movq	%r15, -56(%rbp)
	shrq	$2, %r15
	addq	$1, %r15
	cmpq	$11, -56(%rbp)
	jbe	.L215
	addq	%r13, %rbx
	movq	%r15, %r13
	movd	%ecx, %xmm5
	shrq	$2, %r13
	addq	%r9, %rbx
	pshufd	$0, %xmm5, %xmm0
	salq	$4, %r13
	addq	%rbx, %r13
	.p2align 4,,10
	.p2align 3
.L217:
	movups	%xmm0, (%rbx)
	addq	$16, %rbx
	cmpq	%r13, %rbx
	jne	.L217
	movq	%r15, %rbx
	andq	$-4, %rbx
	leaq	(%r11,%rbx,4), %r11
	cmpq	%r15, %rbx
	je	.L214
.L215:
	leaq	4(%r11), %rbx
	movl	%ecx, (%r11)
	cmpq	%rbx, %r14
	jbe	.L214
	leaq	8(%r11), %rbx
	movl	%ecx, 4(%r11)
	cmpq	%rbx, %r14
	jbe	.L214
	movl	%ecx, 8(%r11)
	jmp	.L214
.L271:
	sarl	$5, %esi
	movslq	%esi, %rdx
	leaq	(%rdi,%rdx,4), %r10
	movslq	(%r10), %rsi
	testl	%esi, %esi
	jg	.L238
	movslq	139412(%rdi), %rdx
	leal	32(%rdx), %r11d
	cmpl	%r11d, 139408(%rdi)
	jl	.L208
	movl	%r11d, 139412(%rdi)
	testl	%edx, %edx
	js	.L208
	movl	%edx, (%r10)
	salq	$2, %rsi
	salq	$2, %rdx
	leaq	(%r9,%rdx), %r10
	subq	%rsi, %r9
	movdqu	(%r9), %xmm2
	movups	%xmm2, (%r10)
	movdqu	16(%r9), %xmm3
	movups	%xmm3, 16(%r10)
	movdqu	32(%r9), %xmm4
	movups	%xmm4, 32(%r10)
	movdqu	48(%r9), %xmm5
	movups	%xmm5, 48(%r10)
	movdqu	64(%r9), %xmm6
	movups	%xmm6, 64(%r10)
	movdqu	80(%r9), %xmm7
	movups	%xmm7, 80(%r10)
	movdqu	96(%r9), %xmm2
	movups	%xmm2, 96(%r10)
	movdqu	112(%r9), %xmm3
	movups	%xmm3, 112(%r10)
	movq	139392(%rdi), %r9
.L239:
	movslq	%r12d, %rsi
	addq	%rdx, %r9
	salq	$2, %rsi
	leaq	(%r9,%rsi), %rdx
	testb	%r8b, %r8b
	je	.L273
	cmpq	%rdx, %r9
	jnb	.L222
	subq	$1, %rsi
	movq	%r9, %rax
	movq	%rsi, %rdi
	shrq	$2, %rdi
	addq	$1, %rdi
	cmpq	$11, %rsi
	jbe	.L240
	movq	%rdi, %rsi
	movd	%ecx, %xmm4
	shrq	$2, %rsi
	pshufd	$0, %xmm4, %xmm0
	salq	$4, %rsi
	addq	%r9, %rsi
	.p2align 4,,10
	.p2align 3
.L242:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L242
	movq	%rdi, %rax
	andq	$-4, %rax
	leaq	(%r9,%rax,4), %r9
	cmpq	%rdi, %rax
	je	.L222
.L240:
	leaq	4(%r9), %rax
	movl	%ecx, (%r9)
	cmpq	%rax, %rdx
	jbe	.L222
	leaq	8(%r9), %rax
	movl	%ecx, 4(%r9)
	cmpq	%rax, %rdx
	jbe	.L222
	movl	%ecx, 8(%r9)
	jmp	.L222
.L268:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpl	(%r11), %eax
	jne	.L219
	movl	%ecx, (%r11)
.L219:
	addq	$4, %r11
.L267:
	cmpq	%r11, %r14
	ja	.L220
	jmp	.L214
.L211:
	leaq	0(,%r11,4), %rbx
	jmp	.L212
.L238:
	movslq	%esi, %rdx
	salq	$2, %rdx
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L208:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L236:
	.cfi_restore_state
	movl	139412(%rdi), %ebx
	leal	32(%rbx), %r14d
	cmpl	%r14d, 139408(%rdi)
	jl	.L208
	movl	%r14d, 139412(%rdi)
	testl	%ebx, %ebx
	js	.L208
	movslq	%ebx, %r14
	salq	$2, %r11
	movl	%ebx, 0(%r13)
	salq	$2, %r14
	leaq	(%r9,%r14), %r15
	subq	%r11, %r9
	movdqu	(%r9), %xmm2
	movups	%xmm2, (%r15)
	movdqu	16(%r9), %xmm3
	movups	%xmm3, 16(%r15)
	movdqu	32(%r9), %xmm4
	movups	%xmm4, 32(%r15)
	movdqu	48(%r9), %xmm5
	movups	%xmm5, 48(%r15)
	movdqu	64(%r9), %xmm6
	movups	%xmm6, 64(%r15)
	movdqu	80(%r9), %xmm7
	movups	%xmm7, 80(%r15)
	movdqu	96(%r9), %xmm2
	movups	%xmm2, 96(%r15)
	movdqu	112(%r9), %xmm3
	movl	%ebx, %r9d
	negl	%r9d
	movups	%xmm3, 112(%r15)
	movl	%r9d, 0(%r13)
	movq	139392(%rdi), %r9
	addq	%r9, %r14
	movups	%xmm0, (%r14)
	movups	%xmm0, 16(%r14)
	movups	%xmm0, 32(%r14)
	movups	%xmm0, 48(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 96(%r14)
	movups	%xmm0, 112(%r14)
	jmp	.L232
.L273:
	cmpq	%rdx, %r9
	jnb	.L222
	.p2align 4,,10
	.p2align 3
.L245:
	cmpl	(%r9), %eax
	jne	.L244
	movl	%ecx, (%r9)
.L244:
	addq	$4, %r9
	cmpq	%r9, %rdx
	ja	.L245
	jmp	.L222
.L272:
	movslq	%r12d, %r12
	leaq	(%r15,%r12,4), %rdx
	testb	%r8b, %r8b
	je	.L266
	cmpq	%r11, %rdx
	jbe	.L222
	leaq	-1(%rdx), %rax
	subq	%r11, %rax
	movq	%rax, %rsi
	shrq	$2, %rsi
	addq	$1, %rsi
	cmpq	$11, %rax
	jbe	.L223
	movq	%rsi, %rax
	addq	%r13, %rbx
	movd	%ecx, %xmm6
	shrq	$2, %rax
	addq	%rbx, %r9
	pshufd	$0, %xmm6, %xmm0
	salq	$4, %rax
	addq	%r9, %rax
.L225:
	movups	%xmm0, (%r9)
	addq	$16, %r9
	cmpq	%r9, %rax
	jne	.L225
	movq	%rsi, %rax
	andq	$-4, %rax
	leaq	(%r11,%rax,4), %r11
	cmpq	%rax, %rsi
	je	.L222
.L223:
	leaq	4(%r11), %rax
	movl	%ecx, (%r11)
	cmpq	%rax, %rdx
	jbe	.L222
	leaq	8(%r11), %rax
	movl	%ecx, 4(%r11)
	cmpq	%rax, %rdx
	jbe	.L222
	movl	%ecx, 8(%r11)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L228:
	cmpl	(%r11), %eax
	jne	.L227
	movl	%ecx, (%r11)
.L227:
	addq	$4, %r11
.L266:
	cmpq	%r11, %rdx
	ja	.L228
	jmp	.L222
	.cfi_endproc
.LFE2063:
	.size	utrie_setRange32_67, .-utrie_setRange32_67
	.p2align 4
	.globl	utrie_serialize_67
	.type	utrie_serialize_67, @function
utrie_serialize_67:
.LFB2070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L407
	movq	%r9, %r12
	movl	(%r9), %r9d
	testl	%r9d, %r9d
	jg	.L407
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L278
	movl	%edx, %r13d
	testl	%edx, %edx
	js	.L278
	movq	%rsi, %r14
	movq	%rcx, %r10
	movl	%r8d, %r15d
	jle	.L279
	testq	%rsi, %rsi
	jne	.L279
.L278:
	movl	$1, (%r12)
.L407:
	xorl	%r12d, %r12d
.L274:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L408
	addq	$200, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	testq	%r10, %r10
	leaq	_ZL21defaultGetFoldedValueP8UNewTrieii(%rip), %rax
	cmove	%rax, %r10
	cmpb	$0, 139419(%rbx)
	je	.L281
.L300:
	movl	139404(%rbx), %edx
	movl	139412(%rbx), %ecx
	leal	16(%rdx,%rdx), %eax
	testb	%r15b, %r15b
	je	.L409
	leal	(%rcx,%rdx), %esi
	cmpl	$262143, %esi
	jg	.L301
.L302:
	leal	(%rax,%rcx,2), %r12d
	cmpl	%r12d, %r13d
	jl	.L274
	movabsq	$160330574181, %rax
	leaq	16(%r14), %rsi
	movq	%rax, (%r14)
	cmpb	$0, 139418(%rbx)
	jne	.L410
.L405:
	movl	%edx, 8(%r14)
	movq	%rbx, %rdi
	movl	%ecx, 12(%r14)
	testl	%edx, %edx
	jle	.L307
	leal	-1(%rdx), %eax
	movl	%edx, %r8d
	movslq	%eax, %r9
	cmpl	$6, %r9d
	jle	.L330
	movl	%edx, %edi
	movd	%edx, %xmm6
	xorl	%eax, %eax
	shrl	$3, %edi
	pshufd	$0, %xmm6, %xmm3
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L309:
	movdqu	(%rbx,%rax,2), %xmm0
	movdqu	16(%rbx,%rax,2), %xmm1
	paddd	%xmm3, %xmm0
	paddd	%xmm3, %xmm1
	psrld	$2, %xmm1
	psrld	$2, %xmm0
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L309
	movl	%edx, %eax
	andl	$-8, %eax
	movl	%eax, %r10d
	subl	%eax, %edx
	leaq	(%rbx,%r10,4), %rdi
	leaq	(%rsi,%r10,2), %r10
	cmpl	%r8d, %eax
	je	.L310
	leal	-1(%rdx), %eax
.L308:
	movl	(%rdi), %r11d
	addl	%r8d, %r11d
	shrl	$2, %r11d
	movw	%r11w, (%r10)
	testl	%eax, %eax
	je	.L310
	movl	4(%rdi), %eax
	addl	%r8d, %eax
	shrl	$2, %eax
	movw	%ax, 2(%r10)
	cmpl	$2, %edx
	je	.L310
	movl	8(%rdi), %eax
	addl	%r8d, %eax
	shrl	$2, %eax
	movw	%ax, 4(%r10)
	cmpl	$3, %edx
	je	.L310
	movl	12(%rdi), %eax
	addl	%r8d, %eax
	shrl	$2, %eax
	movw	%ax, 6(%r10)
	cmpl	$4, %edx
	je	.L310
	movl	16(%rdi), %eax
	addl	%r8d, %eax
	shrl	$2, %eax
	movw	%ax, 8(%r10)
	cmpl	$5, %edx
	je	.L310
	movl	20(%rdi), %eax
	addl	%r8d, %eax
	shrl	$2, %eax
	movw	%ax, 10(%r10)
	cmpl	$6, %edx
	je	.L310
	addl	24(%rdi), %r8d
	shrl	$2, %r8d
	movw	%r8w, 12(%r10)
.L310:
	leaq	2(%rsi,%r9,2), %rsi
.L307:
	movq	139392(%rbx), %rdx
	testl	%ecx, %ecx
	jle	.L274
	movl	%ecx, %r8d
	cmpl	$7, %ecx
	jle	.L311
	movl	%ecx, %edi
	xorl	%eax, %eax
	shrl	$3, %edi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L313:
	movdqu	(%rdx,%rax,2), %xmm0
	movdqu	16(%rdx,%rax,2), %xmm4
	movdqa	%xmm0, %xmm1
	punpcklwd	%xmm4, %xmm0
	punpckhwd	%xmm4, %xmm1
	movdqa	%xmm0, %xmm2
	punpckhwd	%xmm1, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpcklwd	%xmm2, %xmm0
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L313
	movl	%ecx, %eax
	andl	$-8, %eax
	movl	%eax, %edi
	subl	%eax, %ecx
	leaq	(%rdx,%rdi,4), %rdx
	leaq	(%rsi,%rdi,2), %rsi
	cmpl	%r8d, %eax
	je	.L274
.L311:
	movl	(%rdx), %eax
	movw	%ax, (%rsi)
	cmpl	$1, %ecx
	je	.L274
	movl	4(%rdx), %eax
	movw	%ax, 2(%rsi)
	cmpl	$2, %ecx
	je	.L274
	movl	8(%rdx), %eax
	movw	%ax, 4(%rsi)
	cmpl	$3, %ecx
	je	.L274
	movl	12(%rdx), %eax
	movw	%ax, 6(%rsi)
	cmpl	$4, %ecx
	je	.L274
	movl	16(%rdx), %eax
	movw	%ax, 8(%rsi)
	cmpl	$5, %ecx
	je	.L274
	movl	20(%rdx), %eax
	movw	%ax, 10(%rsi)
	cmpl	$6, %ecx
	je	.L274
	movl	24(%rdx), %eax
	movw	%ax, 12(%rsi)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r10, -200(%rbp)
	call	_ZL13utrie_compactP8UNewTrieaP10UErrorCode
	movdqu	6912(%rbx), %xmm6
	movdqu	6928(%rbx), %xmm7
	movq	139392(%rbx), %rcx
	movl	139400(%rbx), %edx
	movaps	%xmm6, -192(%rbp)
	movdqu	6944(%rbx), %xmm6
	movq	-200(%rbp), %r10
	movaps	%xmm7, -176(%rbp)
	movdqu	6960(%rbx), %xmm7
	cmpl	(%rcx), %edx
	movaps	%xmm6, -160(%rbp)
	movdqu	6976(%rbx), %xmm6
	movaps	%xmm7, -144(%rbp)
	movdqu	6992(%rbx), %xmm7
	movaps	%xmm6, -128(%rbp)
	movdqu	7008(%rbx), %xmm6
	movaps	%xmm7, -112(%rbp)
	movdqu	7024(%rbx), %xmm7
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	je	.L328
	movl	139412(%rbx), %eax
	leal	32(%rax), %esi
	cmpl	%esi, 139408(%rbx)
	jl	.L287
	movl	%esi, 139412(%rbx)
	testl	%eax, %eax
	js	.L287
	movd	%edx, %xmm3
	movslq	%eax, %rdx
	negl	%eax
	pshufd	$0, %xmm3, %xmm0
	leaq	(%rcx,%rdx,4), %rdx
	movups	%xmm0, (%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 64(%rdx)
	movups	%xmm0, 80(%rdx)
	movups	%xmm0, 96(%rdx)
	movups	%xmm0, 112(%rdx)
	movd	%eax, %xmm0
.L284:
	pshufd	$0, %xmm0, %xmm0
	movl	$2048, %r11d
	leaq	8192(%rbx), %rax
	movl	%r15d, -224(%rbp)
	movq	%r12, -232(%rbp)
	movl	$65536, %r8d
	movq	%rbx, %r15
	movl	%r11d, %r12d
	movq	%rax, -208(%rbp)
	movq	%r14, -216(%rbp)
	movl	%r13d, -220(%rbp)
	movq	%r10, -200(%rbp)
	movups	%xmm0, 6912(%rbx)
	movups	%xmm0, 6928(%rbx)
	movups	%xmm0, 6944(%rbx)
	movups	%xmm0, 6960(%rbx)
	movups	%xmm0, 6976(%rbx)
	movups	%xmm0, 6992(%rbx)
	movups	%xmm0, 7008(%rbx)
	movups	%xmm0, 7024(%rbx)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L290:
	addl	$32, %r8d
.L297:
	cmpl	$1114111, %r8d
	jg	.L289
.L288:
	movl	%r8d, %eax
	sarl	$5, %eax
	cltq
	movl	(%r15,%rax,4), %edx
	testl	%edx, %edx
	je	.L290
	movl	%r8d, %ebx
	andl	$-1024, %ebx
	movl	%ebx, %r14d
	sarl	$5, %r14d
	cmpl	$2048, %r12d
	jle	.L332
	movslq	%r14d, %rsi
	movq	-208(%rbp), %rax
	movl	$2048, %r13d
	subq	$2048, %rsi
	.p2align 4,,10
	.p2align 3
.L326:
	leaq	128(%rax), %rdx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L411:
	addq	$4, %rax
	cmpq	%rdx, %rax
	je	.L291
.L293:
	movl	(%rax,%rsi,4), %ecx
	cmpl	%ecx, (%rax)
	je	.L411
	addl	$32, %r13d
	movq	%rdx, %rax
	subq	$32, %rsi
	cmpl	%r12d, %r13d
	jl	.L326
.L332:
	movl	%r12d, %r13d
	.p2align 4,,10
	.p2align 3
.L291:
	movq	-200(%rbp), %rax
	leal	32(%r13), %edx
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	*%rax
	cmpb	$0, 139419(%r15)
	jne	.L294
	movl	%ebx, %esi
	movq	139392(%r15), %rdi
	sarl	$10, %esi
	leal	-10304(%rsi), %edx
	movl	%esi, %ecx
	shrq	$3, %rdx
	andl	$31, %ecx
	andl	$8188, %edx
	addq	%r15, %rdx
	movslq	(%rdx), %r9
	movl	%r9d, %r10d
	sarl	$31, %r10d
	movl	%r10d, %esi
	xorl	%r9d, %esi
	subl	%r10d, %esi
	addl	%ecx, %esi
	movslq	%esi, %rsi
	cmpl	(%rdi,%rsi,4), %eax
	je	.L295
	testl	%r9d, %r9d
	jg	.L296
	movl	139412(%r15), %esi
	leal	32(%rsi), %r10d
	cmpl	%r10d, 139408(%r15)
	jl	.L403
	movl	%r10d, 139412(%r15)
	testl	%esi, %esi
	js	.L403
	movl	%esi, (%rdx)
	salq	$2, %r9
	movslq	%esi, %rdx
	leaq	(%rdi,%rdx,4), %rdx
	subq	%r9, %rdi
	movl	%esi, %r9d
	movdqu	(%rdi), %xmm7
	movups	%xmm7, (%rdx)
	movdqu	16(%rdi), %xmm4
	movups	%xmm4, 16(%rdx)
	movdqu	32(%rdi), %xmm5
	movups	%xmm5, 32(%rdx)
	movdqu	48(%rdi), %xmm3
	movups	%xmm3, 48(%rdx)
	movdqu	64(%rdi), %xmm7
	movups	%xmm7, 64(%rdx)
	movdqu	80(%rdi), %xmm4
	movups	%xmm4, 80(%rdx)
	movdqu	96(%rdi), %xmm5
	movups	%xmm5, 96(%rdx)
	movdqu	112(%rdi), %xmm6
	movups	%xmm6, 112(%rdx)
	movq	139392(%r15), %rdi
.L296:
	addl	%ecx, %r9d
	movslq	%r9d, %r9
	movl	%eax, (%rdi,%r9,4)
	cmpl	%r12d, %r13d
	je	.L412
.L295:
	leal	1024(%rbx), %r8d
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L409:
	cmpl	$262143, %ecx
	jg	.L303
.L304:
	leal	(%rax,%rcx,4), %r12d
	cmpl	%r12d, %r13d
	jl	.L274
	movabsq	$1259842201957, %rax
	leaq	16(%r14), %rdi
	movq	%rax, (%r14)
	cmpb	$0, 139418(%rbx)
	je	.L406
	movl	$805, 4(%r14)
.L406:
	movl	%edx, 8(%r14)
	movq	%rbx, %rsi
	movl	%ecx, 12(%r14)
	testl	%edx, %edx
	jle	.L315
	leal	-1(%rdx), %eax
	movl	%edx, %r10d
	movslq	%eax, %r9
	cmpl	$6, %r9d
	jle	.L331
	movl	%edx, %esi
	xorl	%eax, %eax
	shrl	$3, %esi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L317:
	movdqu	(%rbx,%rax,2), %xmm0
	movdqu	16(%rbx,%rax,2), %xmm1
	psrld	$2, %xmm0
	psrld	$2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklwd	%xmm1, %xmm0
	punpckhwd	%xmm1, %xmm2
	movdqa	%xmm0, %xmm1
	punpckhwd	%xmm2, %xmm1
	punpcklwd	%xmm2, %xmm0
	punpcklwd	%xmm1, %xmm0
	movups	%xmm0, (%rdi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L317
	movl	%edx, %eax
	andl	$-8, %eax
	movl	%eax, %r8d
	subl	%eax, %edx
	leaq	(%rbx,%r8,4), %rsi
	leaq	(%rdi,%r8,2), %r8
	cmpl	%r10d, %eax
	je	.L318
	leal	-1(%rdx), %eax
.L316:
	movl	(%rsi), %r10d
	shrl	$2, %r10d
	movw	%r10w, (%r8)
	testl	%eax, %eax
	je	.L318
	movl	4(%rsi), %eax
	shrl	$2, %eax
	movw	%ax, 2(%r8)
	cmpl	$2, %edx
	je	.L318
	movl	8(%rsi), %eax
	shrl	$2, %eax
	movw	%ax, 4(%r8)
	cmpl	$3, %edx
	je	.L318
	movl	12(%rsi), %eax
	shrl	$2, %eax
	movw	%ax, 6(%r8)
	cmpl	$4, %edx
	je	.L318
	movl	16(%rsi), %eax
	shrl	$2, %eax
	movw	%ax, 8(%r8)
	cmpl	$5, %edx
	je	.L318
	movl	20(%rsi), %eax
	shrl	$2, %eax
	movw	%ax, 10(%r8)
	cmpl	$6, %edx
	je	.L318
	movl	24(%rsi), %eax
	shrl	$2, %eax
	movw	%ax, 12(%r8)
.L318:
	leaq	2(%rdi,%r9,2), %rdi
.L315:
	movslq	%ecx, %rdx
	movq	139392(%rbx), %rsi
	salq	$2, %rdx
	call	memcpy@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L301:
	movl	$8, (%r12)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$8, (%r12)
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$549, 4(%r14)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L328:
	pxor	%xmm0, %xmm0
	jmp	.L284
.L330:
	movq	%rsi, %r10
	jmp	.L308
.L331:
	movq	%rdi, %r8
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L294:
	testl	%eax, %eax
	je	.L295
.L403:
	movq	%r15, %rbx
	movq	-216(%rbp), %r14
	movl	-220(%rbp), %r13d
	movl	-224(%rbp), %r15d
	movq	-232(%rbp), %r12
.L287:
	movl	$7, (%r12)
.L299:
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZL13utrie_compactP8UNewTrieaP10UErrorCode
	movb	$1, 139419(%rbx)
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L300
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L289:
	movl	%r12d, %r11d
	movq	%r15, %rbx
	movq	-216(%rbp), %r14
	movl	-220(%rbp), %r13d
	movl	-224(%rbp), %r15d
	movq	-232(%rbp), %r12
	cmpl	$34815, %r11d
	jle	.L298
	movl	$8, (%r12)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L412:
	movslq	%r12d, %rax
	movslq	%r14d, %r14
	movl	$128, %edx
	addl	$32, %r12d
	leaq	(%r15,%r14,4), %rsi
	leaq	(%r15,%rax,4), %rdi
	call	memmove@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L298:
	leaq	8192(%rbx), %rcx
	leal	-8192(,%r11,4), %edx
	movl	%r11d, -208(%rbp)
	movq	%rcx, %rsi
	movslq	%edx, %rdx
	leaq	8320(%rbx), %rdi
	movq	%rcx, -200(%rbp)
	call	memmove@PLT
	movdqa	-192(%rbp), %xmm7
	movq	-200(%rbp), %rcx
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	movups	%xmm7, 8192(%rbx)
	movdqa	-176(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm6
	movl	-208(%rbp), %r11d
	movups	%xmm4, 32(%rcx)
	movdqa	-96(%rbp), %xmm4
	movups	%xmm7, 16(%rcx)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm5, 48(%rcx)
	movdqa	-80(%rbp), %xmm5
	leal	32(%r11), %eax
	movups	%xmm6, 64(%rcx)
	movups	%xmm7, 80(%rcx)
	movups	%xmm4, 96(%rcx)
	movups	%xmm5, 112(%rcx)
	movl	%eax, 139404(%rbx)
	jmp	.L299
.L408:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2070:
	.size	utrie_serialize_67, .-utrie_serialize_67
	.p2align 4
	.globl	utrie_unserialize_67
	.type	utrie_unserialize_67, @function
utrie_unserialize_67:
.LFB2072:
	.cfi_startproc
	endbr64
	testq	%rcx, %rcx
	je	.L420
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L420
	cmpl	$15, %edx
	jle	.L416
	cmpl	$1416784229, (%rsi)
	jne	.L416
	movl	4(%rsi), %eax
	movl	%eax, %r8d
	andl	$15, %r8d
	cmpl	$5, %r8d
	jne	.L416
	movl	%eax, %r8d
	shrl	$4, %r8d
	andl	$15, %r8d
	cmpl	$2, %r8d
	jne	.L416
	movl	%eax, %r8d
	subl	$16, %edx
	shrl	$9, %r8d
	andl	$1, %r8d
	movb	%r8b, 36(%rdi)
	movl	8(%rsi), %r8d
	movl	12(%rsi), %r9d
	leal	(%r8,%r8), %r10d
	movl	%r8d, 24(%rdi)
	movl	%r9d, 28(%rdi)
	cmpl	%edx, %r10d
	jg	.L416
	addq	$16, %rsi
	movslq	%r8d, %r11
	subl	%r10d, %edx
	movq	%rsi, (%rdi)
	leaq	(%rsi,%r11,2), %rsi
	testb	$1, %ah
	jne	.L421
	leal	(%r9,%r9), %eax
	cmpl	%edx, %eax
	jg	.L416
	movzwl	(%rsi), %eax
	leaq	utrie_defaultGetFoldingOffset_67(%rip), %rcx
	movq	$0, 8(%rdi)
	movq	%rcx, 16(%rdi)
	movl	%eax, 32(%rdi)
	leal	8(%r8,%r9), %eax
	addl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	leal	0(,%r9,4), %eax
	cmpl	%edx, %eax
	jg	.L416
	movl	(%rsi), %eax
	leaq	utrie_defaultGetFoldingOffset_67(%rip), %rcx
	movq	%rsi, 8(%rdi)
	movq	%rcx, 16(%rdi)
	movl	%eax, 32(%rdi)
	leal	8(%r8,%r9,2), %eax
	addl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	movl	$3, (%rcx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2072:
	.size	utrie_unserialize_67, .-utrie_unserialize_67
	.p2align 4
	.globl	utrie_unserializeDummy_67
	.type	utrie_unserializeDummy_67, @function
utrie_unserializeDummy_67:
.LFB2073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	movq	16(%rbp), %rax
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rax, %rax
	je	.L439
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L439
	movl	$2080, 24(%rdi)
	movq	%rdi, %r10
	movl	%ecx, %r11d
	cmpl	%ecx, %r8d
	je	.L447
	movl	$288, 28(%rdi)
	movl	$4736, %r12d
	movl	$5312, %ebx
.L425:
	testb	%r9b, %r9b
	je	.L426
	cmpl	%edx, %r12d
	jg	.L448
	movb	$1, 36(%r10)
	movdqa	.LC2(%rip), %xmm0
	movq	%rsi, %rdx
	leaq	4160(%rsi), %rax
	movl	%r11d, 32(%r10)
	movq	%rsi, (%r10)
	.p2align 4,,10
	.p2align 3
.L430:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rax
	jne	.L430
	cmpl	%r11d, %r8d
	je	.L433
	movdqa	.LC3(%rip), %xmm0
	movups	%xmm0, 3456(%rsi)
	movups	%xmm0, 3472(%rsi)
	movups	%xmm0, 3488(%rsi)
	movups	%xmm0, 3504(%rsi)
.L433:
	movd	%r11d, %xmm0
	movq	$0, 8(%r10)
	leaq	4672(%rsi), %rdx
	punpcklwd	%xmm0, %xmm0
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L432:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L432
	cmpl	%r11d, %r8d
	je	.L434
	movd	%r8d, %xmm0
	punpcklwd	%xmm0, %xmm0
	pshufd	$0, %xmm0, %xmm0
	movups	%xmm0, 4672(%rsi)
	movups	%xmm0, 4688(%rsi)
	movups	%xmm0, 4704(%rsi)
	movups	%xmm0, 4720(%rsi)
.L434:
	leaq	utrie_defaultGetFoldingOffset_67(%rip), %rax
	movq	%rax, 16(%r10)
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	cmpl	%edx, %ebx
	jle	.L449
.L427:
	movl	$15, (%rax)
	movl	%ebx, %r12d
.L422:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L447:
	.cfi_restore_state
	movl	$256, 28(%rdi)
	movl	$4672, %r12d
	movl	$5184, %ebx
	jmp	.L425
.L449:
	leaq	8(%rsi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movb	$1, 36(%r10)
	andq	$-8, %rdi
	movl	%r11d, 32(%r10)
	subq	%rdi, %rcx
	movq	%rsi, (%r10)
	addl	$4160, %ecx
	movq	$0, (%rsi)
	movq	$0, 4152(%rsi)
	shrl	$3, %ecx
	rep stosq
	cmpl	%r11d, %r8d
	je	.L437
	movdqa	.LC4(%rip), %xmm0
	movups	%xmm0, 3456(%rsi)
	movups	%xmm0, 3472(%rsi)
	movups	%xmm0, 3488(%rsi)
	movups	%xmm0, 3504(%rsi)
.L437:
	movslq	24(%r10), %rcx
	movd	%r11d, %xmm1
	pshufd	$0, %xmm1, %xmm0
	addq	%rcx, %rcx
	leaq	(%rsi,%rcx), %rax
	movq	%rax, 8(%r10)
	leaq	1024(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L435:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L435
	movl	%ebx, %r12d
	cmpl	%r11d, %r8d
	je	.L434
	movd	%r8d, %xmm2
	leaq	1024(%rsi,%rcx), %rax
	pshufd	$0, %xmm2, %xmm0
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	jmp	.L434
.L448:
	movl	%r12d, %ebx
	jmp	.L427
.L439:
	movl	$-1, %r12d
	jmp	.L422
	.cfi_endproc
.LFE2073:
	.size	utrie_unserializeDummy_67, .-utrie_unserializeDummy_67
	.p2align 4
	.globl	utrie_enum_67
	.type	utrie_enum_67, @function
utrie_enum_67:
.LFB2075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movq	%rdx, -80(%rbp)
	testq	%rdi, %rdi
	je	.L450
	movq	(%rdi), %rax
	movq	%rax, -96(%rbp)
	testq	%rdx, %rdx
	je	.L450
	testq	%rax, %rax
	je	.L450
	testq	%rsi, %rsi
	leaq	_ZL13enumSameValuePKvj(%rip), %rax
	movq	8(%rdi), %rbx
	movq	%rcx, %r14
	cmovne	%rsi, %rax
	movl	32(%rdi), %esi
	movq	%rcx, %rdi
	movq	%rbx, -112(%rbp)
	movq	%rax, -56(%rbp)
	call	*%rax
	movl	$0, -104(%rbp)
	movl	%eax, -100(%rbp)
	testq	%rbx, %rbx
	je	.L558
.L455:
	movl	$0, -72(%rbp)
	movl	-100(%rbp), %ebx
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	movl	-104(%rbp), %r13d
	.p2align 4,,10
	.p2align 3
.L477:
	cmpl	$55296, %r10d
	je	.L508
	cmpl	$56320, %r10d
	je	.L509
	movslq	-72(%rbp), %rax
	addq	%rax, %rax
.L456:
	movq	-96(%rbp), %rdx
	movzwl	(%rdx,%rax), %r15d
	leal	0(,%r15,4), %eax
	movl	%eax, -68(%rbp)
	cmpl	%r13d, %eax
	je	.L559
	cmpl	%eax, -104(%rbp)
	jne	.L560
	cmpl	%ebx, -100(%rbp)
	je	.L511
	cmpl	%r12d, %r10d
	jle	.L512
	movl	%r10d, -64(%rbp)
	movq	-80(%rbp), %rax
	movl	%ebx, %ecx
	movl	%r10d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	je	.L450
	movl	-64(%rbp), %r10d
	movl	%r10d, %r12d
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$2048, -72(%rbp)
	movl	$4096, %eax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L559:
	addl	$32, %r10d
.L458:
	addl	$1, -72(%rbp)
	cmpl	$65535, %r10d
	jle	.L477
	movq	-96(%rbp), %rax
	movl	$55296, -116(%rbp)
	addq	$64, %rax
	movq	%rax, -144(%rbp)
.L476:
	movl	-116(%rbp), %eax
	movq	-96(%rbp), %rdx
	sarl	$5, %eax
	cltq
	movzwl	(%rdx,%rax,2), %r15d
	sall	$2, %r15d
	cmpl	%r15d, -104(%rbp)
	je	.L561
	movl	-116(%rbp), %eax
	movq	-112(%rbp), %rdx
	andl	$31, %eax
	addl	%r15d, %eax
	cltq
	testq	%rdx, %rdx
	je	.L482
	movl	(%rdx,%rax,4), %edi
.L483:
	movq	-128(%rbp), %rax
	movl	%r10d, -64(%rbp)
	call	*16(%rax)
	movl	-64(%rbp), %r10d
	testl	%eax, %eax
	jg	.L484
	cmpl	%ebx, -100(%rbp)
	je	.L485
	cmpl	%r12d, %r10d
	jle	.L517
	movq	-80(%rbp), %rax
	movl	%ebx, %ecx
	movl	%r10d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	je	.L450
	movl	-64(%rbp), %r10d
	movl	-104(%rbp), %r13d
	movl	%r10d, %r12d
.L485:
	movl	-100(%rbp), %ebx
	addl	$1024, %r10d
.L486:
	addl	$1, -116(%rbp)
.L481:
	cmpl	$56319, -116(%rbp)
	jle	.L476
	movq	-80(%rbp), %rax
	addq	$104, %rsp
	movl	%ebx, %ecx
	movl	%r12d, %esi
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	movl	%r10d, %edx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	cmpq	$0, -112(%rbp)
	movl	%r10d, %r13d
	je	.L562
	movslq	-68(%rbp), %rax
	movq	-112(%rbp), %rdx
	xorl	%r15d, %r15d
	movl	%r12d, -64(%rbp)
	movq	%r15, %r13
	movl	%r10d, %r12d
	movl	%ebx, %r15d
	leaq	(%rdx,%rax,4), %rax
	movq	%rax, %rbx
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L470:
	cmpl	-64(%rbp), %r12d
	jle	.L474
	movl	%eax, -88(%rbp)
	movl	-64(%rbp), %esi
	movl	%r15d, %ecx
	movl	%r12d, %edx
	movq	-80(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	movl	-88(%rbp), %r8d
	testb	%al, %al
	je	.L450
.L474:
	leal	1(%r12), %eax
	movl	%r12d, -64(%rbp)
	leal	1(%r13), %ecx
	movl	%eax, %r10d
	testq	%r13, %r13
	je	.L473
	movl	$-1, -68(%rbp)
	cmpl	$32, %ecx
	je	.L555
.L473:
	addq	$1, %r13
	movl	%eax, %r12d
	movl	%r8d, %r15d
.L475:
	movl	(%rbx,%r13,4), %esi
	movq	-56(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, %r8d
	cmpl	%r15d, %eax
	jne	.L470
	leal	1(%r12), %eax
	leal	1(%r13), %ecx
	movl	%eax, %r10d
	cmpl	$32, %ecx
	jne	.L473
.L555:
	movl	-64(%rbp), %r12d
.L463:
	movl	-68(%rbp), %r13d
	movl	%r8d, %ebx
	jmp	.L458
.L561:
	cmpl	%ebx, -100(%rbp)
	je	.L515
	cmpl	%r12d, %r10d
	jle	.L516
	movl	%r10d, -64(%rbp)
	movq	-80(%rbp), %rax
	movl	%ebx, %ecx
	movl	%r10d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L563
.L450:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movl	$1760, -72(%rbp)
	movl	$3520, %eax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L512:
	movl	%r10d, %r12d
.L469:
	movl	-68(%rbp), %r13d
	movl	-100(%rbp), %ebx
	addl	$32, %r10d
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L511:
	movl	%r13d, -68(%rbp)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L562:
	movq	-96(%rbp), %rdx
	cltq
	xorl	%r15d, %r15d
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, -64(%rbp)
	movq	%r15, %rax
	movl	%ebx, %r15d
	movq	%rax, %rbx
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L461:
	cmpl	%r13d, %r12d
	jge	.L465
	movl	%eax, -88(%rbp)
	movl	%r15d, %ecx
	movq	-80(%rbp), %rax
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*%rax
	movl	-88(%rbp), %r8d
	testb	%al, %al
	je	.L450
.L465:
	leal	1(%r13), %eax
	leal	1(%rbx), %ecx
	movl	%r13d, %r12d
	movl	%eax, %r10d
	testq	%rbx, %rbx
	je	.L464
	movl	$-1, -68(%rbp)
.L467:
	cmpl	$32, %ecx
	je	.L463
.L464:
	addq	$1, %rbx
	movl	%eax, %r13d
	movl	%r8d, %r15d
.L468:
	movq	-64(%rbp), %rax
	movq	%r14, %rdi
	movzwl	(%rax,%rbx,2), %esi
	movq	-56(%rbp), %rax
	call	*%rax
	movl	%eax, %r8d
	cmpl	%eax, %r15d
	jne	.L461
	leal	1(%r13), %eax
	leal	1(%rbx), %ecx
	movl	%eax, %r10d
	jmp	.L467
.L558:
	movq	-128(%rbp), %rax
	movl	24(%rax), %eax
	movl	%eax, -104(%rbp)
	jmp	.L455
.L484:
	movq	-96(%rbp), %rdx
	cltq
	addq	%rax, %rax
	addq	%rax, %rdx
	addq	-144(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, -136(%rbp)
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L487:
	cmpl	%eax, -104(%rbp)
	jne	.L564
	cmpl	-100(%rbp), %ebx
	je	.L519
	cmpl	%r12d, %r10d
	jle	.L520
	movl	%r10d, -64(%rbp)
	movq	-80(%rbp), %rax
	movl	%ebx, %ecx
	movl	%r10d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	je	.L450
	movl	-64(%rbp), %r10d
	movl	%r10d, %r12d
.L499:
	movl	-68(%rbp), %r13d
	movl	-100(%rbp), %ebx
	addl	$32, %r10d
.L488:
	addq	$2, -88(%rbp)
	movq	-88(%rbp), %rax
	cmpq	-136(%rbp), %rax
	je	.L486
.L506:
	movq	-88(%rbp), %rax
	movzwl	(%rax), %r15d
	leal	0(,%r15,4), %eax
	movl	%eax, -68(%rbp)
	cmpl	%eax, %r13d
	jne	.L487
	addl	$32, %r10d
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L564:
	cmpq	$0, -112(%rbp)
	movl	%r10d, %r13d
	je	.L565
	movslq	-68(%rbp), %rax
	movq	-112(%rbp), %rdx
	xorl	%r15d, %r15d
	movl	%r12d, -64(%rbp)
	movq	%r15, %r13
	movl	%r10d, %r12d
	movl	%ebx, %r15d
	leaq	(%rdx,%rax,4), %rax
	movq	%rax, %rbx
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L500:
	cmpl	-64(%rbp), %r12d
	jle	.L504
	movl	%eax, -72(%rbp)
	movl	-64(%rbp), %esi
	movl	%r15d, %ecx
	movl	%r12d, %edx
	movq	-80(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	movl	-72(%rbp), %r8d
	testb	%al, %al
	je	.L450
.L504:
	leal	1(%r12), %eax
	movl	%r12d, -64(%rbp)
	leal	1(%r13), %ecx
	movl	%eax, %r10d
	testq	%r13, %r13
	je	.L503
	movl	$-1, -68(%rbp)
	cmpl	$32, %ecx
	je	.L556
.L503:
	addq	$1, %r13
	movl	%eax, %r12d
	movl	%r8d, %r15d
.L505:
	movl	(%rbx,%r13,4), %esi
	movq	-56(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, %r8d
	cmpl	%r15d, %eax
	jne	.L500
	leal	1(%r12), %eax
	leal	1(%r13), %ecx
	movl	%eax, %r10d
	cmpl	$32, %ecx
	jne	.L503
.L556:
	movl	-64(%rbp), %r12d
.L493:
	movl	-68(%rbp), %r13d
	movl	%r8d, %ebx
	jmp	.L488
.L519:
	movl	%r13d, -68(%rbp)
	jmp	.L499
.L520:
	movl	%r10d, %r12d
	jmp	.L499
.L482:
	movq	-96(%rbp), %rdx
	movzwl	(%rdx,%rax,2), %edi
	jmp	.L483
.L565:
	movq	-96(%rbp), %rdx
	cltq
	xorl	%r15d, %r15d
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, -64(%rbp)
	movq	%r15, %rax
	movl	%ebx, %r15d
	movq	%rax, %rbx
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L491:
	cmpl	%r13d, %r12d
	jge	.L495
	movl	%eax, -72(%rbp)
	movl	%r15d, %ecx
	movq	-80(%rbp), %rax
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	*%rax
	movl	-72(%rbp), %r8d
	testb	%al, %al
	je	.L450
.L495:
	leal	1(%r13), %eax
	leal	1(%rbx), %ecx
	movl	%r13d, %r12d
	movl	%eax, %r10d
	testq	%rbx, %rbx
	je	.L494
	movl	$-1, -68(%rbp)
.L497:
	cmpl	$32, %ecx
	je	.L493
.L494:
	addq	$1, %rbx
	movl	%eax, %r13d
	movl	%r8d, %r15d
.L498:
	movq	-64(%rbp), %rax
	movq	%r14, %rdi
	movzwl	(%rax,%rbx,2), %esi
	movq	-56(%rbp), %rax
	call	*%rax
	movl	%eax, %r8d
	cmpl	%r15d, %eax
	jne	.L491
	leal	1(%r13), %eax
	leal	1(%rbx), %ecx
	movl	%eax, %r10d
	jmp	.L497
.L517:
	movl	-104(%rbp), %r13d
	movl	%r10d, %r12d
	jmp	.L485
.L563:
	movl	-64(%rbp), %r10d
	movl	%r10d, %r12d
.L480:
	addl	$32, -116(%rbp)
	movl	-100(%rbp), %ebx
	addl	$32768, %r10d
	movl	%r15d, %r13d
	jmp	.L481
.L516:
	movl	%r10d, %r12d
	jmp	.L480
.L515:
	movl	%r13d, %r15d
	jmp	.L480
	.cfi_endproc
.LFE2075:
	.size	utrie_enum_67, .-utrie_enum_67
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	32
	.long	64
	.long	96
	.long	128
	.align 16
.LC1:
	.long	160
	.long	192
	.long	224
	.long	256
	.align 16
.LC2:
	.value	520
	.value	520
	.value	520
	.value	520
	.value	520
	.value	520
	.value	520
	.value	520
	.align 16
.LC3:
	.value	584
	.value	584
	.value	584
	.value	584
	.value	584
	.value	584
	.value	584
	.value	584
	.align 16
.LC4:
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.value	64
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
