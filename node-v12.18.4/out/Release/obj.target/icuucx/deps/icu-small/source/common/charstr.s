	.file	"charstr.cpp"
	.text
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2562:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2562:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2565:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	cmpb	$0, 12(%rbx)
	jne	.L16
.L7:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L7
	.cfi_endproc
.LFE2565:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2568:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2568:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2571:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L22
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2571:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L28
.L24:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L29
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2573:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2574:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2574:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2575:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2575:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2576:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2576:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2577:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2577:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2578:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2578:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2579:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L45
	testl	%edx, %edx
	jle	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L48
.L37:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L37
	.cfi_endproc
.LFE2579:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L52
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L52
	testl	%r12d, %r12d
	jg	.L59
	cmpb	$0, 12(%rbx)
	jne	.L60
.L54:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L54
.L60:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2580:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L62
	movq	(%rdi), %r8
.L63:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L66
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L66
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2581:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2582:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2582:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2583:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2583:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2584:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2584:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2585:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2585:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2587:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2587:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2589:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2589:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharStringC2EOS0_
	.type	_ZN6icu_6710CharStringC2EOS0_, @function
_ZN6icu_6710CharStringC2EOS0_:
.LFB2310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	movl	56(%rbx), %eax
	movl	$0, 56(%rbx)
	popq	%rbx
	movl	%eax, 56(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2310:
	.size	_ZN6icu_6710CharStringC2EOS0_, .-_ZN6icu_6710CharStringC2EOS0_
	.globl	_ZN6icu_6710CharStringC1EOS0_
	.set	_ZN6icu_6710CharStringC1EOS0_,_ZN6icu_6710CharStringC2EOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharStringaSEOS0_
	.type	_ZN6icu_6710CharStringaSEOS0_, @function
_ZN6icu_6710CharStringaSEOS0_:
.LFB2312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L86
.L82:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L87
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
.L84:
	movl	56(%rbx), %eax
	movl	%eax, 56(%r12)
	movq	%r12, %rax
	movl	$0, 56(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	jmp	.L84
	.cfi_endproc
.LFE2312:
	.size	_ZN6icu_6710CharStringaSEOS0_, .-_ZN6icu_6710CharStringaSEOS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710CharString9cloneDataER10UErrorCode
	.type	_ZNK6icu_6710CharString9cloneDataER10UErrorCode, @function
_ZNK6icu_6710CharString9cloneDataER10UErrorCode:
.LFB2313:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	jg	.L93
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	56(%rdi), %eax
	movq	%rdi, %rbx
	leal	1(%rax), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L96
	movl	56(%rbx), %eax
	movq	(%rbx), %rsi
	movq	%r8, %rdi
	leal	1(%rax), %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	movq	%rax, %r8
.L88:
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movq	%r8, %rax
	ret
.L96:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$7, (%r12)
	jmp	.L88
	.cfi_endproc
.LFE2313:
	.size	_ZNK6icu_6710CharString9cloneDataER10UErrorCode, .-_ZNK6icu_6710CharString9cloneDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharString8copyFromERKS0_R10UErrorCode
	.type	_ZN6icu_6710CharString8copyFromERKS0_R10UErrorCode, @function
_ZN6icu_6710CharString8copyFromERKS0_R10UErrorCode:
.LFB2314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L98
	movq	%rsi, %r13
	cmpq	%rsi, %rdi
	jne	.L118
.L98:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movl	56(%rsi), %eax
	movq	%rdx, %rbx
	movl	8(%rdi), %edx
	leal	1(%rax), %r14d
	cmpl	%edx, %r14d
	jg	.L119
	movq	(%rdi), %r8
.L104:
	movl	%eax, 56(%r12)
	movq	0(%r13), %rsi
	movslq	%r14d, %rdx
	movq	%r8, %rdi
	call	memcpy@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L119:
	movl	56(%rdi), %eax
	leal	(%r14,%rdx), %r15d
	leal	1(%rax), %edx
	cmpl	%r15d, %r14d
	jl	.L120
.L100:
	movl	%edx, -56(%rbp)
	testl	%r14d, %r14d
	jle	.L105
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L105
	movl	-56(%rbp), %edx
	testl	%edx, %edx
	jle	.L106
	cmpl	%r14d, 8(%r12)
	movl	%r14d, %eax
	movq	(%r12), %rsi
	movq	%r8, %rdi
	cmovle	8(%r12), %eax
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	movq	%rax, %r8
.L106:
	cmpb	$0, 12(%r12)
	jne	.L121
.L107:
	movl	%r14d, 8(%r12)
	movb	$1, 12(%r12)
	movl	56(%r13), %eax
	movq	%r8, (%r12)
	leal	1(%rax), %r14d
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L120:
	testl	%r15d, %r15d
	jle	.L100
	movslq	%r15d, %rdi
	movl	%edx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movl	-56(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L122
	testl	%edx, %edx
	jle	.L102
	cmpl	%edx, %r15d
	movq	(%r12), %rsi
	movq	%rax, %rdi
	cmovle	%r15d, %edx
	cmpl	%edx, 8(%r12)
	cmovle	8(%r12), %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	movq	%rax, %r8
.L102:
	cmpb	$0, 12(%r12)
	jne	.L123
.L103:
	movl	%r15d, 8(%r12)
	movb	$1, 12(%r12)
	movl	56(%r13), %eax
	movq	%r8, (%r12)
	leal	1(%rax), %r14d
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$7, (%rbx)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L121:
	movq	(%r12), %rdi
	movq	%r8, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-56(%rbp), %r8
	jmp	.L107
.L123:
	movq	(%r12), %rdi
	movq	%r8, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-56(%rbp), %r8
	jmp	.L103
.L122:
	movl	56(%r12), %eax
	leal	1(%rax), %edx
	jmp	.L100
	.cfi_endproc
.LFE2314:
	.size	_ZN6icu_6710CharString8copyFromERKS0_R10UErrorCode, .-_ZN6icu_6710CharString8copyFromERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710CharString11lastIndexOfEc
	.type	_ZNK6icu_6710CharString11lastIndexOfEc, @function
_ZNK6icu_6710CharString11lastIndexOfEc:
.LFB2315:
	.cfi_startproc
	endbr64
	movslq	56(%rdi), %rdx
	movq	%rdx, %rax
	subq	$1, %rdx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	movq	(%rdi), %rcx
	subl	$1, %eax
	movzbl	(%rcx,%rdx), %ecx
	subq	$1, %rdx
	cmpb	%sil, %cl
	je	.L129
.L126:
	testl	%eax, %eax
	jg	.L130
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	ret
	.cfi_endproc
.LFE2315:
	.size	_ZNK6icu_6710CharString11lastIndexOfEc, .-_ZNK6icu_6710CharString11lastIndexOfEc
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6710CharString8containsENS_11StringPieceE
	.type	_ZNK6icu_6710CharString8containsENS_11StringPieceE, @function
_ZNK6icu_6710CharString8containsENS_11StringPieceE:
.LFB2316:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L139
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	56(%rdi), %r14d
	movq	(%rdi), %r15
	subl	%edx, %r14d
	js	.L131
	movq	%rsi, %r13
	movslq	%edx, %r12
	xorl	%ebx, %ebx
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L143:
	addq	$1, %rbx
	cmpl	%ebx, %r14d
	jl	.L142
.L133:
	leaq	(%r15,%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L143
	movl	$1, %eax
.L131:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2316:
	.size	_ZNK6icu_6710CharString8containsENS_11StringPieceE, .-_ZNK6icu_6710CharString8containsENS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharString8truncateEi
	.type	_ZN6icu_6710CharString8truncateEi, @function
_ZN6icu_6710CharString8truncateEi:
.LFB2317:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	movl	$0, %edx
	movq	%rdi, %rax
	cmovs	%edx, %esi
	cmpl	%esi, 56(%rdi)
	jle	.L145
	movq	(%rdi), %rdx
	movl	%esi, 56(%rdi)
	movslq	%esi, %rsi
	movb	$0, (%rdx,%rsi)
.L145:
	ret
	.cfi_endproc
.LFE2317:
	.size	_ZN6icu_6710CharString8truncateEi, .-_ZN6icu_6710CharString8truncateEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharString6appendEcR10UErrorCode
	.type	_ZN6icu_6710CharString6appendEcR10UErrorCode, @function
_ZN6icu_6710CharString6appendEcR10UErrorCode:
.LFB2318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L147
	movslq	56(%rdi), %rax
	movq	%rdx, %rbx
	movl	8(%rdi), %edx
	movl	%esi, %r13d
	leal	2(%rax), %ecx
	leal	1(%rax), %r14d
	cmpl	%edx, %ecx
	jg	.L161
	movq	(%rdi), %r8
.L153:
	movl	%r14d, 56(%r12)
	movb	%r13b, (%r8,%rax)
	movslq	56(%r12), %rax
	movq	(%r12), %rdx
	movb	$0, (%rdx,%rax)
.L147:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	leal	(%rcx,%rdx), %r15d
	cmpl	%r15d, %ecx
	jge	.L149
	testl	%r15d, %r15d
	jle	.L149
	movslq	%r15d, %rdi
	movl	%ecx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movl	-56(%rbp), %ecx
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L162
	testl	%r14d, %r14d
	jle	.L151
	cmpl	%r14d, 8(%r12)
	cmovle	8(%r12), %r14d
	movq	%rax, %rdi
	movq	(%r12), %rsi
	cmpl	%r14d, %r15d
	cmovle	%r15d, %r14d
	movslq	%r14d, %rdx
	call	memcpy@PLT
	movq	%rax, %r8
.L151:
	cmpb	$0, 12(%r12)
	jne	.L163
.L152:
	movslq	56(%r12), %rax
	movq	%r8, (%r12)
	movl	%r15d, 8(%r12)
	movb	$1, 12(%r12)
	leal	1(%rax), %r14d
	jmp	.L153
.L162:
	movl	56(%r12), %eax
	leal	1(%rax), %r14d
	.p2align 4,,10
	.p2align 3
.L149:
	testl	%ecx, %ecx
	jle	.L154
	movslq	%ecx, %rdi
	movl	%ecx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L154
	testl	%r14d, %r14d
	movl	-56(%rbp), %ecx
	jle	.L155
	cmpl	%ecx, 8(%r12)
	movl	%ecx, %eax
	cmovle	8(%r12), %eax
	movq	%r8, %rdi
	movq	(%r12), %rsi
	cmpl	%r14d, %eax
	cmovle	%eax, %r14d
	movslq	%r14d, %rdx
	call	memcpy@PLT
	movl	-56(%rbp), %ecx
	movq	%rax, %r8
.L155:
	cmpb	$0, 12(%r12)
	jne	.L164
.L156:
	movslq	56(%r12), %rax
	movq	%r8, (%r12)
	movl	%ecx, 8(%r12)
	movb	$1, 12(%r12)
	leal	1(%rax), %r14d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L154:
	movl	$7, (%rbx)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L164:
	movq	(%r12), %rdi
	movq	%r8, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-64(%rbp), %r8
	movl	-56(%rbp), %ecx
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L163:
	movq	(%r12), %rdi
	movq	%r8, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-56(%rbp), %r8
	jmp	.L152
	.cfi_endproc
.LFE2318:
	.size	_ZN6icu_6710CharString6appendEcR10UErrorCode, .-_ZN6icu_6710CharString6appendEcR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharString15getAppendBufferEiiRiR10UErrorCode
	.type	_ZN6icu_6710CharString15getAppendBufferEiiRiR10UErrorCode, @function
_ZN6icu_6710CharString15getAppendBufferEiiRiR10UErrorCode:
.LFB2320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L183
	movq	%rdi, %rbx
	movl	8(%rdi), %edi
	movl	56(%rbx), %ecx
	movl	%edi, %eax
	subl	%ecx, %eax
	subl	$1, %eax
	cmpl	%esi, %eax
	jl	.L168
	movl	%eax, 0(%r13)
	movslq	56(%rbx), %rax
	addq	(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L186:
	.cfi_restore_state
	movl	56(%rbx), %eax
	leal	1(%rax), %r15d
	.p2align 4,,10
	.p2align 3
.L171:
	testl	%r9d, %r9d
	jg	.L184
.L176:
	movl	$7, (%r8)
.L183:
	movl	$0, 0(%r13)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	leal	1(%rcx,%rsi), %r9d
	cmpl	%edi, %r9d
	jg	.L185
	movq	(%rbx), %r14
.L175:
	movl	%eax, 0(%r13)
	movslq	56(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	addq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	leal	1(%rcx,%rdx), %r12d
	addl	%r9d, %edi
	leal	1(%rcx), %r15d
	testl	%r12d, %r12d
	cmove	%edi, %r12d
	cmpl	%r12d, %r9d
	jge	.L171
	testl	%r12d, %r12d
	jle	.L171
	movslq	%r12d, %rdi
	movq	%r8, -64(%rbp)
	movl	%r9d, -52(%rbp)
	call	uprv_malloc_67@PLT
	movl	-52(%rbp), %r9d
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L186
	testl	%r15d, %r15d
	jle	.L173
	cmpl	%r15d, 8(%rbx)
	movl	%r15d, %ecx
	cmovle	8(%rbx), %ecx
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%ecx, %r12d
	movl	%ecx, %edx
	cmovle	%r12d, %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
.L173:
	cmpb	$0, 12(%rbx)
	jne	.L187
.L174:
	movl	%r12d, 8(%rbx)
	subl	56(%rbx), %r12d
	movq	%r14, (%rbx)
	leal	-1(%r12), %eax
	movb	$1, 12(%rbx)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L184:
	movslq	%r9d, %rdi
	movq	%r8, -64(%rbp)
	movl	%r9d, -52(%rbp)
	call	uprv_malloc_67@PLT
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L176
	testl	%r15d, %r15d
	movl	-52(%rbp), %r9d
	jle	.L177
	cmpl	%r9d, 8(%rbx)
	movl	%r9d, %eax
	cmovle	8(%rbx), %eax
	movq	%r14, %rdi
	movq	(%rbx), %rsi
	cmpl	%r15d, %eax
	cmovg	%r15d, %eax
	movslq	%eax, %rdx
	call	memcpy@PLT
	movl	-52(%rbp), %r9d
.L177:
	cmpb	$0, 12(%rbx)
	jne	.L188
.L178:
	movl	%r9d, 8(%rbx)
	subl	56(%rbx), %r9d
	movq	%r14, (%rbx)
	leal	-1(%r9), %eax
	movb	$1, 12(%rbx)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L188:
	movq	(%rbx), %rdi
	movl	%r9d, -52(%rbp)
	call	uprv_free_67@PLT
	movl	-52(%rbp), %r9d
	jmp	.L178
.L187:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L174
	.cfi_endproc
.LFE2320:
	.size	_ZN6icu_6710CharString15getAppendBufferEiiRiR10UErrorCode, .-_ZN6icu_6710CharString15getAppendBufferEiiRiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharString20appendInvariantCharsEPKDsiR10UErrorCode
	.type	_ZN6icu_6710CharString20appendInvariantCharsEPKDsiR10UErrorCode, @function
_ZN6icu_6710CharString20appendInvariantCharsEPKDsiR10UErrorCode:
.LFB2322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %edi
	testl	%edi, %edi
	jle	.L207
.L191:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	%rsi, %r14
	movl	%edx, %esi
	movl	%edx, %r13d
	movq	%rcx, %rbx
	movq	%r14, %rdi
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	jne	.L192
	movl	$26, (%rbx)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L192:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L191
	movslq	56(%r12), %rsi
	movl	8(%r12), %eax
	leal	1(%rsi,%r13), %r15d
	cmpl	%eax, %r15d
	jle	.L194
	leal	(%r15,%rax), %r9d
	leal	1(%rsi), %edx
	cmpl	%r9d, %r15d
	jl	.L208
.L195:
	movl	%edx, -56(%rbp)
	testl	%r15d, %r15d
	jle	.L200
	movslq	%r15d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L200
	movl	-56(%rbp), %edx
	testl	%edx, %edx
	jle	.L201
	cmpl	%r15d, 8(%r12)
	movl	%r15d, %eax
	movq	(%r12), %rsi
	movq	%rcx, %rdi
	cmovle	8(%r12), %eax
	cmpl	%edx, %eax
	cmovg	%edx, %eax
	movslq	%eax, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
.L201:
	cmpb	$0, 12(%r12)
	jne	.L209
.L202:
	movq	%rcx, (%r12)
	movslq	56(%r12), %rsi
	movl	%r15d, 8(%r12)
	movb	$1, 12(%r12)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L194:
	movq	(%r12), %rcx
.L199:
	movl	%r13d, %edx
	addq	%rcx, %rsi
	movq	%r14, %rdi
	call	u_UCharsToChars_67@PLT
	movl	56(%r12), %edx
	movq	(%r12), %rax
	addl	%r13d, %edx
	movl	%edx, 56(%r12)
	movslq	%edx, %rdx
	movb	$0, (%rax,%rdx)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L208:
	testl	%r9d, %r9d
	jle	.L195
	movslq	%r9d, %rdi
	movl	%edx, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	uprv_malloc_67@PLT
	movl	-56(%rbp), %r9d
	movl	-64(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L210
	testl	%edx, %edx
	jle	.L197
	cmpl	%edx, 8(%r12)
	movq	(%r12), %rsi
	movq	%rax, %rdi
	movl	%r9d, -56(%rbp)
	cmovle	8(%r12), %edx
	cmpl	%edx, %r9d
	cmovle	%r9d, %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	movl	-56(%rbp), %r9d
	movq	%rax, %rcx
.L197:
	cmpb	$0, 12(%r12)
	jne	.L211
.L198:
	movq	%rcx, (%r12)
	movslq	56(%r12), %rsi
	movl	%r9d, 8(%r12)
	movb	$1, 12(%r12)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L200:
	movl	$7, (%rbx)
	jmp	.L191
.L209:
	movq	(%r12), %rdi
	movq	%rcx, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rcx
	jmp	.L202
.L211:
	movq	(%r12), %rdi
	movq	%rcx, -64(%rbp)
	movl	%r9d, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %r9d
	jmp	.L198
.L210:
	movl	56(%r12), %eax
	leal	1(%rax), %edx
	jmp	.L195
	.cfi_endproc
.LFE2322:
	.size	_ZN6icu_6710CharString20appendInvariantCharsEPKDsiR10UErrorCode, .-_ZN6icu_6710CharString20appendInvariantCharsEPKDsiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharString14ensureCapacityEiiR10UErrorCode
	.type	_ZN6icu_6710CharString14ensureCapacityEiiR10UErrorCode, @function
_ZN6icu_6710CharString14ensureCapacityEiiR10UErrorCode:
.LFB2323:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L228
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	8(%rdi), %edx
	cmpl	%esi, %edx
	jge	.L212
	addl	%esi, %edx
	testl	%r13d, %r13d
	movl	56(%rdi), %eax
	movq	%rcx, %r14
	cmove	%edx, %r13d
	leal	1(%rax), %r15d
	cmpl	%esi, %r13d
	jle	.L215
	testl	%r13d, %r13d
	jle	.L215
	movslq	%r13d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L231
	testl	%r15d, %r15d
	jle	.L217
	cmpl	%r15d, 8(%rbx)
	cmovle	8(%rbx), %r15d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r15d, %r13d
	cmovle	%r13d, %r15d
	movslq	%r15d, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
.L217:
	cmpb	$0, 12(%rbx)
	jne	.L232
.L218:
	movq	%rcx, (%rbx)
	movl	$1, %eax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
.L212:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L231:
	.cfi_restore_state
	movl	56(%rbx), %eax
	leal	1(%rax), %r15d
	.p2align 4,,10
	.p2align 3
.L215:
	testl	%r12d, %r12d
	jle	.L219
	movslq	%r12d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L219
	testl	%r15d, %r15d
	jle	.L220
	cmpl	%r12d, 8(%rbx)
	movl	%r12d, %eax
	cmovle	8(%rbx), %eax
	movq	%r13, %rdi
	movq	(%rbx), %rsi
	cmpl	%r15d, %eax
	cmovle	%eax, %r15d
	movslq	%r15d, %rdx
	call	memcpy@PLT
.L220:
	cmpb	$0, 12(%rbx)
	jne	.L233
.L221:
	movq	%r13, (%rbx)
	movl	$1, %eax
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, (%r14)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L232:
	movq	(%rbx), %rdi
	movq	%rcx, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-56(%rbp), %rcx
	jmp	.L218
	.cfi_endproc
.LFE2323:
	.size	_ZN6icu_6710CharString14ensureCapacityEiiR10UErrorCode, .-_ZN6icu_6710CharString14ensureCapacityEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode:
.LFB2321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L235
	movswl	%ax, %ebx
	sarl	$5, %ebx
	testb	$17, %al
	jne	.L243
.L250:
	leaq	10(%rsi), %r12
	testb	$2, %al
	je	.L248
.L237:
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L249
.L240:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_restore_state
	movl	(%r14), %eax
	movq	24(%rsi), %r12
	testl	%eax, %eax
	jg	.L240
.L249:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	jne	.L241
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	movl	$26, (%r14)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movl	12(%rsi), %ebx
	testb	$17, %al
	je	.L250
.L243:
	xorl	%r12d, %r12d
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L241:
	movl	56(%r13), %esi
	xorl	%edx, %edx
	movq	%r14, %rcx
	movq	%r13, %rdi
	addl	%ebx, %esi
	addl	$1, %esi
	call	_ZN6icu_6710CharString14ensureCapacityEiiR10UErrorCode
	testb	%al, %al
	je	.L240
	movslq	56(%r13), %rsi
	movl	%ebx, %edx
	addq	0(%r13), %rsi
	movq	%r12, %rdi
	call	u_UCharsToChars_67@PLT
	addl	56(%r13), %ebx
	movq	0(%r13), %rax
	movl	%ebx, 56(%r13)
	movslq	%ebx, %rbx
	movb	$0, (%rax,%rbx)
	jmp	.L240
	.cfi_endproc
.LFE2321:
	.size	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharString6appendEPKciR10UErrorCode
	.type	_ZN6icu_6710CharString6appendEPKciR10UErrorCode, @function
_ZN6icu_6710CharString6appendEPKciR10UErrorCode:
.LFB2319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$96, %rsp
	movl	(%rcx), %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L278
	movl	%edx, %r13d
	movq	%rcx, %r14
	cmpl	$-1, %edx
	jl	.L254
	movq	%rsi, %r15
	testq	%rsi, %rsi
	jne	.L255
	testl	%edx, %edx
	je	.L255
.L254:
	movl	$1, (%r14)
	movq	%r12, %rax
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L255:
	cmpl	$-1, %r13d
	jne	.L256
	movq	%r15, %rdi
	call	strlen@PLT
	movl	%eax, %r13d
.L256:
	testl	%r13d, %r13d
	jle	.L278
	movslq	56(%r12), %rdx
	movq	(%r12), %rcx
	movq	%rdx, %rax
	addq	%rcx, %rdx
	cmpq	%rdx, %r15
	je	.L279
	jnb	.L260
	cmpq	%rcx, %r15
	jnb	.L280
.L260:
	xorl	%edx, %edx
	leal	1(%rax,%r13), %esi
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString14ensureCapacityEiiR10UErrorCode
	testb	%al, %al
	je	.L278
	movslq	56(%r12), %rdi
	movslq	%r13d, %rdx
	addq	(%r12), %rdi
	movq	%r15, %rsi
	call	memcpy@PLT
	addl	56(%r12), %r13d
	movq	(%r12), %rax
	movl	%r13d, 56(%r12)
	movslq	%r13d, %r13
	movb	$0, (%rax,%r13)
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%r12, %rax
.L251:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L281
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movl	8(%r12), %edx
	subl	%eax, %edx
	cmpl	%edx, %r13d
	jl	.L260
	leaq	-99(%rbp), %rax
	leaq	-112(%rbp), %rdi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%rax, -112(%rbp)
	movq	%r15, %rsi
	xorl	%eax, %eax
	movl	$0, -56(%rbp)
	movl	$40, -104(%rbp)
	movw	%ax, -100(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode
	movl	-56(%rbp), %edx
	movq	-112(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode
	cmpb	$0, -100(%rbp)
	je	.L251
	movq	-112(%rbp), %rdi
	movq	%rax, -120(%rbp)
	call	uprv_free_67@PLT
	movq	-120(%rbp), %rax
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L279:
	movl	8(%r12), %edx
	subl	%eax, %edx
	cmpl	%edx, %r13d
	jl	.L259
	movl	$5, (%r14)
	movq	%r12, %rax
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L259:
	addl	%r13d, %eax
	movl	%eax, 56(%r12)
	cltq
	movb	$0, (%rcx,%rax)
	movq	%r12, %rax
	jmp	.L251
.L281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2319:
	.size	_ZN6icu_6710CharString6appendEPKciR10UErrorCode, .-_ZN6icu_6710CharString6appendEPKciR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharString14appendPathPartENS_11StringPieceER10UErrorCode
	.type	_ZN6icu_6710CharString14appendPathPartENS_11StringPieceER10UErrorCode, @function
_ZN6icu_6710CharString14appendPathPartENS_11StringPieceER10UErrorCode:
.LFB2324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L285
	movq	%rdx, %rbx
	testl	%edx, %edx
	je	.L285
	movq	%rsi, %r14
	movl	56(%r12), %esi
	movq	%rcx, %r13
	movl	%edx, %r15d
	testl	%esi, %esi
	jle	.L305
	movq	(%r12), %rdx
	leal	-1(%rsi), %eax
	cltq
	cmpb	$47, (%rdx,%rax)
	jne	.L320
.L305:
	cmpl	$-1, %ebx
	jl	.L289
	testq	%r14, %r14
	je	.L289
	cmpl	$-1, %ebx
	jne	.L291
	movq	%r14, %rdi
	call	strlen@PLT
	movl	%eax, %r15d
.L291:
	testl	%r15d, %r15d
	jle	.L285
	movslq	56(%r12), %rdi
	movq	(%r12), %rcx
	movl	8(%r12), %edx
	movq	%rdi, %rax
	addq	%rcx, %rdi
	cmpq	%r14, %rdi
	je	.L321
	cmpq	%r14, %rcx
	ja	.L294
	cmpq	%r14, %rdi
	jbe	.L294
	movl	%edx, %ecx
	subl	%eax, %ecx
	cmpl	%ecx, %r15d
	jge	.L322
.L294:
	leal	1(%r15,%rax), %ecx
	cmpl	%edx, %ecx
	jle	.L297
	leal	(%rcx,%rdx), %r8d
	leal	1(%rax), %edx
	cmpl	%r8d, %ecx
	jge	.L298
	testl	%r8d, %r8d
	jle	.L298
	movslq	%r8d, %rdi
	movl	%ecx, -140(%rbp)
	movl	%edx, -136(%rbp)
	movl	%r8d, -132(%rbp)
	call	uprv_malloc_67@PLT
	movl	-132(%rbp), %r8d
	movl	-136(%rbp), %edx
	testq	%rax, %rax
	movl	-140(%rbp), %ecx
	movq	%rax, %rbx
	je	.L323
	testl	%edx, %edx
	jle	.L300
	cmpl	%edx, 8(%r12)
	movl	%edx, %eax
	cmovle	8(%r12), %eax
	movq	%rbx, %rdi
	movq	(%r12), %rsi
	movl	%r8d, -132(%rbp)
	cmpl	%eax, %r8d
	cmovle	%r8d, %eax
	movslq	%eax, %rdx
	call	memcpy@PLT
	movl	-132(%rbp), %r8d
.L300:
	cmpb	$0, 12(%r12)
	jne	.L324
.L301:
	movslq	56(%r12), %rdi
	movq	%rbx, (%r12)
	movl	%r8d, 8(%r12)
	movb	$1, 12(%r12)
	addq	%rbx, %rdi
	.p2align 4,,10
	.p2align 3
.L297:
	movslq	%r15d, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	addl	56(%r12), %r15d
	movq	(%r12), %rax
	movl	%r15d, 56(%r12)
	movslq	%r15d, %r15
	movb	$0, (%rax,%r15)
	.p2align 4,,10
	.p2align 3
.L285:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L325
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	addl	$2, %esi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString14ensureCapacityEiiR10UErrorCode
	testb	%al, %al
	je	.L287
	movslq	56(%r12), %rax
	leal	1(%rax), %edx
	movl	%edx, 56(%r12)
	movq	(%r12), %rdx
	movb	$47, (%rdx,%rax)
	movslq	56(%r12), %rax
	movq	(%r12), %rdx
	movb	$0, (%rdx,%rax)
.L287:
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L285
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L289:
	movl	$1, 0(%r13)
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L321:
	subl	%eax, %edx
	cmpl	%edx, %r15d
	jl	.L293
	movl	$5, 0(%r13)
	jmp	.L285
.L293:
	addl	%r15d, %eax
	movl	%eax, 56(%r12)
	cltq
	movb	$0, (%rcx,%rax)
	jmp	.L285
.L323:
	movl	56(%r12), %eax
	leal	1(%rax), %edx
.L298:
	movl	%edx, -132(%rbp)
	testl	%ecx, %ecx
	jle	.L302
	movslq	%ecx, %rdi
	movl	%ecx, -136(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L302
	movl	-132(%rbp), %edx
	movl	-136(%rbp), %ecx
	testl	%edx, %edx
	jle	.L303
	cmpl	%ecx, 8(%r12)
	movl	%ecx, %eax
	cmovle	8(%r12), %eax
	movq	%rbx, %rdi
	movq	(%r12), %rsi
	movl	%ecx, -132(%rbp)
	cmpl	%edx, %eax
	cmovg	%edx, %eax
	movslq	%eax, %rdx
	call	memcpy@PLT
	movl	-132(%rbp), %ecx
.L303:
	cmpb	$0, 12(%r12)
	jne	.L326
.L304:
	movslq	56(%r12), %rdi
	movq	%rbx, (%r12)
	movl	%ecx, 8(%r12)
	movb	$1, 12(%r12)
	addq	%rbx, %rdi
	jmp	.L297
.L322:
	leaq	-115(%rbp), %rax
	leaq	-128(%rbp), %rdi
	movq	%r13, %rcx
	movl	%r15d, %edx
	movq	%rax, -128(%rbp)
	movq	%r14, %rsi
	xorl	%eax, %eax
	movl	$0, -72(%rbp)
	movl	$40, -120(%rbp)
	movw	%ax, -116(%rbp)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode
	movl	-72(%rbp), %edx
	movq	-128(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode
	cmpb	$0, -116(%rbp)
	je	.L285
	movq	-128(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L285
.L302:
	movl	$7, 0(%r13)
	jmp	.L285
.L326:
	movq	(%r12), %rdi
	movl	%ecx, -132(%rbp)
	call	uprv_free_67@PLT
	movl	-132(%rbp), %ecx
	jmp	.L304
.L324:
	movq	(%r12), %rdi
	movl	%r8d, -132(%rbp)
	call	uprv_free_67@PLT
	movl	-132(%rbp), %r8d
	jmp	.L301
.L325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2324:
	.size	_ZN6icu_6710CharString14appendPathPartENS_11StringPieceER10UErrorCode, .-_ZN6icu_6710CharString14appendPathPartENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710CharString27ensureEndsWithFileSeparatorER10UErrorCode
	.type	_ZN6icu_6710CharString27ensureEndsWithFileSeparatorER10UErrorCode, @function
_ZN6icu_6710CharString27ensureEndsWithFileSeparatorER10UErrorCode:
.LFB2325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L340
.L338:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	movslq	56(%rdi), %rax
	testl	%eax, %eax
	jle	.L338
	movq	(%rdi), %rbx
	leal	-1(%rax), %edx
	movslq	%edx, %rdx
	cmpb	$47, (%rbx,%rdx)
	je	.L338
	movl	8(%rdi), %edx
	leal	2(%rax), %r15d
	leal	1(%rax), %r13d
	cmpl	%edx, %r15d
	jle	.L329
	leal	(%r15,%rdx), %r14d
	movq	%rsi, -56(%rbp)
	cmpl	%r14d, %r15d
	jge	.L330
	movslq	%r14d, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L341
	cmpl	%r13d, 8(%r12)
	cmovle	8(%r12), %r13d
	movq	%rax, %rdi
	movq	(%r12), %r15
	cmpl	%r13d, %r14d
	cmovle	%r14d, %r13d
	movq	%r15, %rsi
	movslq	%r13d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%r12)
	jne	.L342
.L334:
	movslq	56(%r12), %rax
	movq	%rbx, (%r12)
	movl	%r14d, 8(%r12)
	movb	$1, 12(%r12)
	leal	1(%rax), %r13d
	.p2align 4,,10
	.p2align 3
.L329:
	movl	%r13d, 56(%r12)
	movb	$47, (%rbx,%rax)
	movslq	56(%r12), %rax
	movq	(%r12), %rdx
	movb	$0, (%rdx,%rax)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L330:
	movslq	%r15d, %rdi
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L337
.L333:
	movl	$7, (%rsi)
	jmp	.L338
.L341:
	movl	56(%r12), %eax
	movslq	%r15d, %rdi
	leal	1(%rax), %r13d
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L333
	testl	%r13d, %r13d
	jle	.L335
.L337:
	cmpl	%r15d, 8(%r12)
	movl	%r15d, %eax
	movq	(%r12), %rsi
	movq	%rbx, %rdi
	cmovle	8(%r12), %eax
	cmpl	%r13d, %eax
	cmovg	%r13d, %eax
	movslq	%eax, %rdx
	call	memcpy@PLT
.L335:
	cmpb	$0, 12(%r12)
	jne	.L343
.L336:
	movslq	56(%r12), %rax
	movq	%rbx, (%r12)
	movl	%r15d, 8(%r12)
	movb	$1, 12(%r12)
	leal	1(%rax), %r13d
	jmp	.L329
.L343:
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L336
.L342:
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	jmp	.L334
	.cfi_endproc
.LFE2325:
	.size	_ZN6icu_6710CharString27ensureEndsWithFileSeparatorER10UErrorCode, .-_ZN6icu_6710CharString27ensureEndsWithFileSeparatorER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
