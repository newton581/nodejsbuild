	.file	"ucmndata.cpp"
	.text
	.p2align 4
	.type	offsetTOCEntryCount, @function
offsetTOCEntryCount:
.LFB2079:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1
	movl	(%rdx), %eax
.L1:
	ret
	.cfi_endproc
.LFE2079:
	.size	offsetTOCEntryCount, .-offsetTOCEntryCount
	.p2align 4
	.type	offsetTOCLookupFn, @function
offsetTOCLookupFn:
.LFB2080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L7
	movl	(%rax), %ebx
	leaq	4(%rax), %r14
	movl	%ebx, -48(%rbp)
	testl	%ebx, %ebx
	je	.L27
	movl	4(%rax), %r8d
	movzbl	(%rsi), %ecx
	addq	%rax, %r8
	movl	%ecx, %r9d
	testl	%ecx, %ecx
	movzbl	(%r8), %edx
	sete	%r10b
	subl	%edx, %r9d
	jne	.L24
	testb	%r10b, %r10b
	jne	.L24
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L10:
	movzbl	(%rsi,%rdx), %edi
	movzbl	(%r8,%rdx), %r9d
	movl	%edx, %r12d
	addq	$1, %rdx
	movl	%edi, %ebx
	subl	%r9d, %ebx
	movl	%ebx, %r9d
	jne	.L9
	testl	%edi, %edi
	jne	.L10
.L9:
	testl	%r9d, %r9d
	je	.L25
	movl	-48(%rbp), %ebx
	leal	-1(%rbx), %r9d
	movslq	%r9d, %rdx
	leaq	(%r14,%rdx,8), %r11
	movl	(%r11), %r8d
	addq	%rax, %r8
	movzbl	(%r8), %edx
	subl	%edx, %ecx
	jne	.L26
	testb	%r10b, %r10b
	jne	.L26
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	(%rsi,%rdx), %edi
	movzbl	(%r8,%rdx), %ecx
	movl	%edx, %r10d
	addq	$1, %rdx
	movl	%edi, %ebx
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	jne	.L13
	testl	%edi, %edi
	jne	.L14
.L13:
	movl	$1, %r15d
	testl	%ecx, %ecx
	je	.L44
.L20:
	cmpl	%r15d, %r9d
	jle	.L27
	.p2align 4,,10
	.p2align 3
.L46:
	leal	(%r9,%r15), %edx
	movl	%r12d, %r11d
	sarl	%edx
	cmpl	%r12d, %r10d
	movl	%edx, %r13d
	movslq	%edx, %rdx
	cmovle	%r10d, %r11d
	leaq	(%r14,%rdx,8), %rbx
	movq	%rbx, %rdi
	movslq	%r11d, %rcx
	movq	%rbx, -72(%rbp)
	movl	(%rdi), %edx
	leaq	(%rsi,%rcx), %rbx
	addq	%rcx, %rdx
	movzbl	(%rbx), %ecx
	addq	%rax, %rdx
	movzbl	(%rdx), %edi
	movl	%ecx, %r8d
	subl	%edi, %r8d
	movl	%r8d, %edi
	testl	%ecx, %ecx
	je	.L28
	testl	%r8d, %r8d
	jne	.L28
	movq	%rdx, -56(%rbp)
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L18:
	leal	(%r11,%rcx), %edi
	movzbl	(%rbx,%rcx), %r8d
	movl	%edi, -44(%rbp)
	movq	-56(%rbp), %rdi
	movl	%r8d, %edx
	movzbl	(%rdi,%rcx), %edi
	addq	$1, %rcx
	subl	%edi, %edx
	movl	%edx, %edi
	jne	.L17
	testl	%r8d, %r8d
	jne	.L18
.L17:
	testl	%edi, %edi
	jns	.L45
.L29:
	movl	%r13d, %r9d
	movl	-44(%rbp), %r10d
	cmpl	%r15d, %r9d
	jg	.L46
.L27:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movl	$1, %r15d
.L12:
	cmpl	%r15d, -48(%rbp)
	jle	.L32
	movl	12(%r14), %edx
	movq	%r14, %r11
	subl	4(%r14), %edx
.L22:
	movq	-64(%rbp), %rbx
	movl	%edx, (%rbx)
	movl	4(%r11), %edx
	addq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	%r11d, -44(%rbp)
	testl	%edi, %edi
	js	.L29
	.p2align 4,,10
	.p2align 3
.L45:
	leal	1(%r13), %r15d
	je	.L30
	movl	-44(%rbp), %r12d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L7:
	movq	8(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	testl	%r9d, %r9d
	js	.L27
.L43:
	movl	$-1, %edx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%r12d, %r12d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%r10d, %r10d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r14, %r11
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-72(%rbp), %r14
	jmp	.L12
	.cfi_endproc
.LFE2080:
	.size	offsetTOCLookupFn, .-offsetTOCLookupFn
	.p2align 4
	.type	pointerTOCEntryCount, @function
pointerTOCEntryCount:
.LFB2081:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L47
	movl	(%rdx), %eax
.L47:
	ret
	.cfi_endproc
.LFE2081:
	.size	pointerTOCEntryCount, .-pointerTOCEntryCount
	.p2align 4
	.type	pointerTOCLookupFn, @function
pointerTOCLookupFn:
.LFB2082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	movq	16(%rdi), %r8
	testq	%r8, %r8
	je	.L52
	movl	(%r8), %r11d
	testl	%r11d, %r11d
	je	.L74
	movq	8(%r8), %r10
	movzbl	(%rsi), %ecx
	movzbl	(%r10), %eax
	testl	%ecx, %ecx
	movl	%ecx, %edx
	sete	%r9b
	subl	%eax, %edx
	jne	.L68
	testb	%r9b, %r9b
	jne	.L68
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L55:
	movzbl	(%rsi,%rax), %edi
	movzbl	(%r10,%rax), %edx
	movl	%eax, %r12d
	addq	$1, %rax
	movl	%edi, %ebx
	subl	%edx, %ebx
	movl	%ebx, %edx
	jne	.L54
	testl	%edi, %edi
	jne	.L55
.L54:
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L57
	leal	-1(%r11), %edx
	leaq	8(%r8), %rbx
	movslq	%edx, %rax
	movq	%rbx, -72(%rbp)
	movq	%rax, %rdi
	salq	$4, %rdi
	movq	8(%r8,%rdi), %r11
	movzbl	(%r11), %edi
	subl	%edi, %ecx
	jne	.L69
	testb	%r9b, %r9b
	jne	.L69
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L59:
	movzbl	(%rsi,%rdi), %r10d
	movzbl	(%r11,%rdi), %ecx
	movl	%edi, %r9d
	addq	$1, %rdi
	movl	%r10d, %ebx
	subl	%ecx, %ebx
	movl	%ebx, %ecx
	jne	.L58
	testl	%r10d, %r10d
	jne	.L59
.L58:
	movl	$1, %r15d
	testl	%ecx, %ecx
	je	.L88
.L65:
	cmpl	%r15d, %edx
	jle	.L74
	.p2align 4,,10
	.p2align 3
.L90:
	leal	(%rdx,%r15), %eax
	movl	%r12d, %r11d
	movq	-72(%rbp), %r14
	sarl	%eax
	cmpl	%r12d, %r9d
	cmovle	%r9d, %r11d
	movslq	%eax, %rcx
	salq	$4, %rcx
	movslq	%r11d, %rdi
	leaq	(%rsi,%rdi), %rbx
	addq	(%r14,%rcx), %rdi
	movzbl	(%rbx), %ecx
	movzbl	(%rdi), %r10d
	movl	%ecx, %r13d
	subl	%r10d, %r13d
	testl	%ecx, %ecx
	je	.L71
	testl	%r13d, %r13d
	jne	.L71
	movq	%rdi, -56(%rbp)
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L63:
	movq	-56(%rbp), %rdi
	movzbl	(%rbx,%rcx), %r10d
	leal	(%r11,%rcx), %r14d
	movzbl	(%rdi,%rcx), %r13d
	movl	%r10d, %edi
	addq	$1, %rcx
	subl	%r13d, %edi
	movl	%edi, %r13d
	jne	.L62
	testl	%r10d, %r10d
	jne	.L63
.L62:
	testl	%r13d, %r13d
	jns	.L89
.L72:
	movl	%eax, %edx
	movl	%r14d, %r9d
	cmpl	%r15d, %edx
	jg	.L90
.L74:
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	testl	%edx, %edx
	js	.L74
.L57:
	movq	-64(%rbp), %rbx
	salq	$4, %rax
	movq	16(%r8,%rax), %rdi
	movl	$-1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	UDataMemory_normalizeDataPointer_67@PLT
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	movl	%r11d, %r14d
	testl	%r13d, %r13d
	js	.L72
	.p2align 4,,10
	.p2align 3
.L89:
	je	.L73
	leal	1(%rax), %r15d
	movl	%r14d, %r12d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L52:
	movq	8(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L69:
	xorl	%r9d, %r9d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L73:
	cltq
	jmp	.L57
	.cfi_endproc
.LFE2082:
	.size	pointerTOCLookupFn, .-pointerTOCLookupFn
	.p2align 4
	.globl	udata_getHeaderSize_67
	.type	udata_getHeaderSize_67, @function
udata_getHeaderSize_67:
.LFB2074:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L93
	cmpb	$0, 8(%rdi)
	movzwl	(%rdi), %eax
	jne	.L94
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	rolw	$8, %ax
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2074:
	.size	udata_getHeaderSize_67, .-udata_getHeaderSize_67
	.p2align 4
	.globl	udata_getInfoSize_67
	.type	udata_getInfoSize_67, @function
udata_getInfoSize_67:
.LFB2075:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L97
	cmpb	$0, 4(%rdi)
	movzwl	(%rdi), %eax
	jne	.L98
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	rolw	$8, %ax
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2075:
	.size	udata_getInfoSize_67, .-udata_getInfoSize_67
	.p2align 4
	.globl	udata_checkCommonData_67
	.type	udata_checkCommonData_67, @function
udata_checkCommonData_67:
.LFB2083:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L99
	testq	%rdi, %rdi
	je	.L101
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L101
	cmpw	$10202, 2(%rax)
	je	.L111
.L101:
	movl	$3, (%rsi)
	jmp	udata_close_67@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	cmpw	$0, 8(%rax)
	jne	.L101
	movzwl	12(%rax), %edx
	cmpw	$27971, %dx
	je	.L112
	cmpw	$28500, %dx
	jne	.L101
	cmpw	$20547, 14(%rax)
	jne	.L101
	cmpb	$1, 16(%rax)
	jne	.L101
	leaq	_ZL9ToCPFuncs(%rip), %rcx
	movzwl	(%rax), %edx
	movq	%rcx, (%rdi)
	cmpb	$0, 8(%rax)
	je	.L105
.L110:
	rolw	$8, %dx
.L105:
	movzwl	%dx, %edx
	addq	%rdx, %rax
	movq	%rax, 16(%rdi)
.L99:
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	cmpw	$17518, 14(%rax)
	jne	.L101
	cmpb	$1, 16(%rax)
	jne	.L101
	leaq	_ZL9CmnDFuncs(%rip), %rcx
	movzwl	(%rax), %edx
	movq	%rcx, (%rdi)
	cmpb	$0, 8(%rax)
	je	.L105
	jmp	.L110
	.cfi_endproc
.LFE2083:
	.size	udata_checkCommonData_67, .-udata_checkCommonData_67
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZL9ToCPFuncs, @object
	.size	_ZL9ToCPFuncs, 16
_ZL9ToCPFuncs:
	.quad	pointerTOCLookupFn
	.quad	pointerTOCEntryCount
	.align 16
	.type	_ZL9CmnDFuncs, @object
	.size	_ZL9CmnDFuncs, 16
_ZL9CmnDFuncs:
	.quad	offsetTOCLookupFn
	.quad	offsetTOCEntryCount
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
