	.file	"locmap.cpp"
	.text
	.p2align 4
	.type	_ZL9getHostIDPK13ILcidPosixMapPKcP10UErrorCode.isra.0, @function
_ZL9getHostIDPK13ILcidPosixMapPKcP10UErrorCode.isra.0:
.LFB2602:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r12d
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	movq	%rcx, -64(%rbp)
	call	strlen@PLT
	movzbl	(%rbx), %r11d
	testl	%r12d, %r12d
	je	.L11
	testb	%r11b, %r11b
	movq	%r14, %r9
	movl	%eax, %r15d
	sete	%r13b
	xorl	%r14d, %r14d
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L8:
	movq	8(%r9), %rsi
	cmpb	%r11b, (%rsi)
	jne	.L12
	testb	%r13b, %r13b
	jne	.L12
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L4:
	movzbl	(%rbx,%rax), %edx
	cmpb	%dl, (%rsi,%rax)
	movl	%eax, %edi
	setne	%cl
	testb	%dl, %dl
	sete	%dl
	addq	$1, %rax
	orb	%dl, %cl
	je	.L4
.L3:
	cmpl	%r10d, %edi
	jle	.L5
	movslq	%edi, %rax
	cmpb	$0, (%rsi,%rax)
	jne	.L5
	cmpl	%edi, %r15d
	je	.L22
	movl	%r8d, %r14d
	movslq	%edi, %r10
.L5:
	addl	$1, %r8d
	addq	$16, %r9
	cmpl	%r12d, %r8d
	jne	.L8
	movzbl	(%rbx,%r10), %r11d
.L2:
	cmpb	$95, %r11b
	je	.L13
	cmpb	$64, %r11b
	je	.L13
.L9:
	movq	-64(%rbp), %rax
	movl	$1, (%rax)
	movq	-56(%rbp), %rax
	movl	(%rax), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	xorl	%edi, %edi
	jmp	.L3
.L13:
	movslq	%r14d, %rax
	salq	$4, %rax
	addq	-56(%rbp), %rax
	movq	8(%rax), %rdx
	cmpb	$0, (%rdx,%r10)
	jne	.L9
	movq	-64(%rbp), %rbx
	movl	(%rax), %eax
	movl	$-128, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	movl	(%r9), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	jmp	.L2
	.cfi_endproc
.LFE2602:
	.size	_ZL9getHostIDPK13ILcidPosixMapPKcP10UErrorCode.isra.0, .-_ZL9getHostIDPK13ILcidPosixMapPKcP10UErrorCode.isra.0
	.p2align 4
	.globl	uprv_convertToPosix_67
	.type	uprv_convertToPosix_67, @function
uprv_convertToPosix_67:
.LFB2076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	xorl	%eax, %eax
	andl	$1023, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$54, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leaq	24+_ZL11gPosixIDmap(%rip), %rdx
	subq	$24, %rsp
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L24:
	addl	$1, %eax
	cmpl	$141, %eax
	je	.L29
	movq	(%rdx), %rcx
	addq	$16, %rdx
	movl	(%rcx), %esi
.L30:
	cmpl	%esi, %r8d
	jne	.L24
	leaq	_ZL11gPosixIDmap(%rip), %rdx
	salq	$4, %rax
	addq	%rdx, %rax
	movl	(%rax), %edx
	movq	8(%rax), %rcx
	testl	%edx, %edx
	je	.L25
	subl	$1, %edx
	movq	%rcx, %rax
	salq	$4, %rdx
	leaq	16(%rcx,%rdx), %rdx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L26:
	addq	$16, %rax
	cmpq	%rax, %rdx
	je	.L25
.L28:
	cmpl	(%rax), %edi
	jne	.L26
	movq	8(%rax), %r15
.L27:
	testq	%r15, %r15
	je	.L29
	movq	%r15, %rdi
	call	strlen@PLT
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	cmpl	%ebx, %eax
	movq	%rax, %r14
	movl	%eax, -52(%rbp)
	cmovle	%eax, %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	cmpl	%ebx, %r14d
	movl	-52(%rbp), %r8d
	jge	.L33
	movslq	%r14d, %r14
	movb	$0, (%r12,%r14)
	cmpl	$-124, 0(%r13)
	jne	.L23
	movl	$0, 0(%r13)
.L23:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	cmpl	%r14d, %ebx
	movl	$15, %eax
	movl	$-124, %edx
	cmove	%edx, %eax
	movl	%eax, 0(%r13)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L25:
	movq	8(%rcx), %r15
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$1, 0(%r13)
	movl	$-1, %r8d
	jmp	.L23
	.cfi_endproc
.LFE2076:
	.size	uprv_convertToPosix_67, .-uprv_convertToPosix_67
	.p2align 4
	.globl	uprv_convertToLCIDPlatform_67
	.type	uprv_convertToLCIDPlatform_67, @function
uprv_convertToLCIDPlatform_67:
.LFB2077:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2077:
	.size	uprv_convertToLCIDPlatform_67, .-uprv_convertToLCIDPlatform_67
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"lb"
	.text
	.p2align 4
	.globl	uprv_convertToLCID_67
	.type	uprv_convertToLCID_67, @function
uprv_convertToLCID_67:
.LFB2078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L53
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L53
	movq	%rdi, %rbx
	call	strlen@PLT
	xorl	%r9d, %r9d
	cmpq	$1, %rax
	jbe	.L44
	movq	%r12, %rdi
	movl	%r9d, -76(%rbp)
	call	strlen@PLT
	movl	$0, %r9d
	cmpq	$1, %rax
	jbe	.L44
	leaq	.LC0(%rip), %rsi
	movl	$141, %r13d
	xorl	%r15d, %r15d
	movl	$70, %r14d
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L64:
	je	.L63
	movl	%r14d, %r15d
	cmpl	%r13d, %r15d
	jnb	.L47
.L65:
	leal	(%r15,%r13), %eax
	shrl	%eax
	cmpl	%eax, %r14d
	je	.L47
	movl	%eax, %edx
	leaq	_ZL11gPosixIDmap(%rip), %rcx
	movl	%eax, %r14d
	salq	$4, %rdx
	movq	8(%rcx,%rdx), %rdx
	movq	8(%rdx), %rsi
.L46:
	movq	%rbx, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L64
	movl	%r14d, %r13d
	cmpl	%r13d, %r15d
	jb	.L65
.L47:
	leaq	_ZL11gPosixIDmap(%rip), %r15
	movl	$-1, %ebx
	leaq	-60(%rbp), %r13
	leaq	2256(%r15), %r14
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	$-128, %eax
	cmove	%r9d, %ebx
	addq	$16, %r15
	cmpq	%r15, %r14
	je	.L66
.L50:
	movq	8(%r15), %rsi
	movl	(%r15), %edi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	$0, -60(%rbp)
	call	_ZL9getHostIDPK13ILcidPosixMapPKcP10UErrorCode.isra.0
	movl	%eax, %r9d
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jne	.L67
.L44:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	addq	$40, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	xorl	%r9d, %r9d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	_ZL11gPosixIDmap(%rip), %rax
	salq	$4, %r14
	movq	-72(%rbp), %rcx
	movq	%r12, %rdx
	leaq	(%r14,%rax), %r13
	movq	8(%r13), %rsi
	movl	0(%r13), %edi
	call	_ZL9getHostIDPK13ILcidPosixMapPKcP10UErrorCode.isra.0
	movl	%eax, %r9d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-72(%rbp), %rax
	cmpl	$-1, %ebx
	je	.L51
	movl	$-128, (%rax)
	movl	%ebx, %r9d
	jmp	.L44
.L51:
	movl	$1, (%rax)
	xorl	%r9d, %r9d
	jmp	.L44
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2078:
	.size	uprv_convertToLCID_67, .-uprv_convertToLCID_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL11gPosixIDmap, @object
	.size	_ZL11gPosixIDmap, 2256
_ZL11gPosixIDmap:
	.long	2
	.zero	4
	.quad	_ZL9locmap_af
	.long	2
	.zero	4
	.quad	_ZL9locmap_am
	.long	18
	.zero	4
	.quad	_ZL9locmap_ar
	.long	2
	.zero	4
	.quad	_ZL10locmap_arn
	.long	2
	.zero	4
	.quad	_ZL9locmap_as
	.long	6
	.zero	4
	.quad	_ZL9locmap_az
	.long	2
	.zero	4
	.quad	_ZL9locmap_ba
	.long	2
	.zero	4
	.quad	_ZL9locmap_be
	.long	2
	.zero	4
	.quad	_ZL9locmap_bg
	.long	2
	.zero	4
	.quad	_ZL10locmap_bin
	.long	3
	.zero	4
	.quad	_ZL9locmap_bn
	.long	4
	.zero	4
	.quad	_ZL9locmap_bo
	.long	2
	.zero	4
	.quad	_ZL9locmap_br
	.long	3
	.zero	4
	.quad	_ZL9locmap_ca
	.long	4
	.zero	4
	.quad	_ZL10locmap_chr
	.long	3
	.zero	4
	.quad	_ZL10locmap_ckb
	.long	2
	.zero	4
	.quad	_ZL9locmap_co
	.long	2
	.zero	4
	.quad	_ZL9locmap_cs
	.long	2
	.zero	4
	.quad	_ZL9locmap_cy
	.long	2
	.zero	4
	.quad	_ZL9locmap_da
	.long	8
	.zero	4
	.quad	_ZL9locmap_de
	.long	2
	.zero	4
	.quad	_ZL9locmap_dv
	.long	2
	.zero	4
	.quad	_ZL9locmap_el
	.long	26
	.zero	4
	.quad	_ZL9locmap_en
	.long	1
	.zero	4
	.quad	_ZL18locmap_en_US_POSIX
	.long	25
	.zero	4
	.quad	_ZL9locmap_es
	.long	2
	.zero	4
	.quad	_ZL9locmap_et
	.long	2
	.zero	4
	.quad	_ZL9locmap_eu
	.long	3
	.zero	4
	.quad	_ZL9locmap_fa
	.long	2
	.zero	4
	.quad	_ZL12locmap_fa_AF
	.long	4
	.zero	4
	.quad	_ZL9locmap_ff
	.long	2
	.zero	4
	.quad	_ZL9locmap_fi
	.long	2
	.zero	4
	.quad	_ZL10locmap_fil
	.long	2
	.zero	4
	.quad	_ZL9locmap_fo
	.long	18
	.zero	4
	.quad	_ZL9locmap_fr
	.long	2
	.zero	4
	.quad	_ZL10locmap_fuv
	.long	2
	.zero	4
	.quad	_ZL9locmap_fy
	.long	3
	.zero	4
	.quad	_ZL9locmap_ga
	.long	2
	.zero	4
	.quad	_ZL9locmap_gd
	.long	2
	.zero	4
	.quad	_ZL9locmap_gl
	.long	2
	.zero	4
	.quad	_ZL9locmap_gn
	.long	2
	.zero	4
	.quad	_ZL10locmap_gsw
	.long	2
	.zero	4
	.quad	_ZL9locmap_gu
	.long	3
	.zero	4
	.quad	_ZL9locmap_ha
	.long	2
	.zero	4
	.quad	_ZL10locmap_haw
	.long	2
	.zero	4
	.quad	_ZL9locmap_he
	.long	2
	.zero	4
	.quad	_ZL9locmap_hi
	.long	20
	.zero	4
	.quad	_ZL9locmap_hr
	.long	4
	.zero	4
	.quad	_ZL10locmap_hsb
	.long	2
	.zero	4
	.quad	_ZL9locmap_hu
	.long	2
	.zero	4
	.quad	_ZL9locmap_hy
	.long	2
	.zero	4
	.quad	_ZL10locmap_ibb
	.long	2
	.zero	4
	.quad	_ZL9locmap_id
	.long	2
	.zero	4
	.quad	_ZL9locmap_ig
	.long	2
	.zero	4
	.quad	_ZL9locmap_ii
	.long	2
	.zero	4
	.quad	_ZL9locmap_is
	.long	3
	.zero	4
	.quad	_ZL9locmap_it
	.long	5
	.zero	4
	.quad	_ZL9locmap_iu
	.long	2
	.zero	4
	.quad	_ZL9locmap_iw
	.long	2
	.zero	4
	.quad	_ZL9locmap_ja
	.long	2
	.zero	4
	.quad	_ZL9locmap_ka
	.long	2
	.zero	4
	.quad	_ZL9locmap_kk
	.long	2
	.zero	4
	.quad	_ZL9locmap_kl
	.long	2
	.zero	4
	.quad	_ZL9locmap_km
	.long	2
	.zero	4
	.quad	_ZL9locmap_kn
	.long	3
	.zero	4
	.quad	_ZL9locmap_ko
	.long	2
	.zero	4
	.quad	_ZL10locmap_kok
	.long	2
	.zero	4
	.quad	_ZL9locmap_kr
	.long	3
	.zero	4
	.quad	_ZL9locmap_ks
	.long	2
	.zero	4
	.quad	_ZL9locmap_ky
	.long	2
	.zero	4
	.quad	_ZL9locmap_lb
	.long	3
	.zero	4
	.quad	_ZL9locmap_la
	.long	2
	.zero	4
	.quad	_ZL9locmap_lo
	.long	2
	.zero	4
	.quad	_ZL9locmap_lt
	.long	2
	.zero	4
	.quad	_ZL9locmap_lv
	.long	2
	.zero	4
	.quad	_ZL9locmap_mi
	.long	2
	.zero	4
	.quad	_ZL9locmap_mk
	.long	2
	.zero	4
	.quad	_ZL9locmap_ml
	.long	7
	.zero	4
	.quad	_ZL9locmap_mn
	.long	2
	.zero	4
	.quad	_ZL10locmap_mni
	.long	2
	.zero	4
	.quad	_ZL10locmap_moh
	.long	2
	.zero	4
	.quad	_ZL9locmap_mr
	.long	3
	.zero	4
	.quad	_ZL9locmap_ms
	.long	2
	.zero	4
	.quad	_ZL9locmap_mt
	.long	2
	.zero	4
	.quad	_ZL9locmap_my
	.long	3
	.zero	4
	.quad	_ZL9locmap_ne
	.long	3
	.zero	4
	.quad	_ZL9locmap_nl
	.long	7
	.zero	4
	.quad	_ZL9locmap_no
	.long	2
	.zero	4
	.quad	_ZL10locmap_nso
	.long	2
	.zero	4
	.quad	_ZL9locmap_oc
	.long	3
	.zero	4
	.quad	_ZL9locmap_om
	.long	2
	.zero	4
	.quad	_ZL12locmap_or_IN
	.long	4
	.zero	4
	.quad	_ZL9locmap_pa
	.long	3
	.zero	4
	.quad	_ZL10locmap_pap
	.long	2
	.zero	4
	.quad	_ZL9locmap_pl
	.long	2
	.zero	4
	.quad	_ZL9locmap_ps
	.long	3
	.zero	4
	.quad	_ZL9locmap_pt
	.long	7
	.zero	4
	.quad	_ZL9locmap_qu
	.long	3
	.zero	4
	.quad	_ZL10locmap_quc
	.long	3
	.zero	4
	.quad	_ZL10locmap_qut
	.long	2
	.zero	4
	.quad	_ZL9locmap_rm
	.long	3
	.zero	4
	.quad	_ZL9locmap_ro
	.long	1
	.zero	4
	.quad	_ZL11locmap_root
	.long	3
	.zero	4
	.quad	_ZL9locmap_ru
	.long	2
	.zero	4
	.quad	_ZL9locmap_rw
	.long	2
	.zero	4
	.quad	_ZL9locmap_sa
	.long	2
	.zero	4
	.quad	_ZL10locmap_sah
	.long	6
	.zero	4
	.quad	_ZL9locmap_sd
	.long	14
	.zero	4
	.quad	_ZL9locmap_se
	.long	2
	.zero	4
	.quad	_ZL9locmap_si
	.long	2
	.zero	4
	.quad	_ZL9locmap_sk
	.long	2
	.zero	4
	.quad	_ZL9locmap_sl
	.long	2
	.zero	4
	.quad	_ZL9locmap_so
	.long	2
	.zero	4
	.quad	_ZL9locmap_sq
	.long	2
	.zero	4
	.quad	_ZL9locmap_st
	.long	3
	.zero	4
	.quad	_ZL9locmap_sv
	.long	2
	.zero	4
	.quad	_ZL9locmap_sw
	.long	2
	.zero	4
	.quad	_ZL10locmap_syr
	.long	3
	.zero	4
	.quad	_ZL9locmap_ta
	.long	2
	.zero	4
	.quad	_ZL9locmap_te
	.long	3
	.zero	4
	.quad	_ZL9locmap_tg
	.long	2
	.zero	4
	.quad	_ZL9locmap_th
	.long	3
	.zero	4
	.quad	_ZL9locmap_ti
	.long	2
	.zero	4
	.quad	_ZL9locmap_tk
	.long	3
	.zero	4
	.quad	_ZL9locmap_tn
	.long	2
	.zero	4
	.quad	_ZL9locmap_tr
	.long	2
	.zero	4
	.quad	_ZL9locmap_ts
	.long	2
	.zero	4
	.quad	_ZL9locmap_tt
	.long	6
	.zero	4
	.quad	_ZL10locmap_tzm
	.long	3
	.zero	4
	.quad	_ZL9locmap_ug
	.long	2
	.zero	4
	.quad	_ZL9locmap_uk
	.long	3
	.zero	4
	.quad	_ZL9locmap_ur
	.long	6
	.zero	4
	.quad	_ZL9locmap_uz
	.long	3
	.zero	4
	.quad	_ZL9locmap_ve
	.long	2
	.zero	4
	.quad	_ZL9locmap_vi
	.long	2
	.zero	4
	.quad	_ZL9locmap_wo
	.long	2
	.zero	4
	.quad	_ZL9locmap_xh
	.long	2
	.zero	4
	.quad	_ZL9locmap_yi
	.long	2
	.zero	4
	.quad	_ZL9locmap_yo
	.long	22
	.zero	4
	.quad	_ZL9locmap_zh
	.long	2
	.zero	4
	.quad	_ZL9locmap_zu
	.section	.rodata.str1.1
.LC1:
	.string	"zu"
.LC2:
	.string	"zu_ZA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_zu, @object
	.size	_ZL9locmap_zu, 32
_ZL9locmap_zu:
	.long	53
	.zero	4
	.quad	.LC1
	.long	1077
	.zero	4
	.quad	.LC2
	.section	.rodata.str1.1
.LC3:
	.string	"zh_Hans"
.LC4:
	.string	"zh"
.LC5:
	.string	"zh_CN"
.LC6:
	.string	"zh_Hans_CN"
.LC7:
	.string	"zh_Hant_HK"
.LC8:
	.string	"zh_HK"
.LC9:
	.string	"zh_Hant_MO"
.LC10:
	.string	"zh_MO"
.LC11:
	.string	"zh_Hans_SG"
.LC12:
	.string	"zh_SG"
.LC13:
	.string	"zh_Hant_TW"
.LC14:
	.string	"zh_Hant"
.LC15:
	.string	"zh_TW"
.LC16:
	.string	"zh@collation=stroke"
.LC17:
	.string	"zh_Hant@collation=stroke"
.LC18:
	.string	"zh_Hant_TW@collation=stroke"
.LC19:
	.string	"zh_TW@collation=stroke"
.LC20:
	.string	"zh_Hans@collation=stroke"
.LC21:
	.string	"zh_Hans_CN@collation=stroke"
.LC22:
	.string	"zh_CN@collation=stroke"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_zh, @object
	.size	_ZL9locmap_zh, 352
_ZL9locmap_zh:
	.long	4
	.zero	4
	.quad	.LC3
	.long	30724
	.zero	4
	.quad	.LC4
	.long	2052
	.zero	4
	.quad	.LC5
	.long	2052
	.zero	4
	.quad	.LC6
	.long	3076
	.zero	4
	.quad	.LC7
	.long	3076
	.zero	4
	.quad	.LC8
	.long	5124
	.zero	4
	.quad	.LC9
	.long	5124
	.zero	4
	.quad	.LC10
	.long	4100
	.zero	4
	.quad	.LC11
	.long	4100
	.zero	4
	.quad	.LC12
	.long	1028
	.zero	4
	.quad	.LC13
	.long	31748
	.zero	4
	.quad	.LC14
	.long	1028
	.zero	4
	.quad	.LC15
	.long	197636
	.zero	4
	.quad	.LC13
	.long	197636
	.zero	4
	.quad	.LC15
	.long	131076
	.zero	4
	.quad	.LC16
	.long	132100
	.zero	4
	.quad	.LC17
	.long	132100
	.zero	4
	.quad	.LC18
	.long	132100
	.zero	4
	.quad	.LC19
	.long	133124
	.zero	4
	.quad	.LC20
	.long	133124
	.zero	4
	.quad	.LC21
	.long	133124
	.zero	4
	.quad	.LC22
	.section	.rodata.str1.1
.LC23:
	.string	"yo"
.LC24:
	.string	"yo_NG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_yo, @object
	.size	_ZL9locmap_yo, 32
_ZL9locmap_yo:
	.long	106
	.zero	4
	.quad	.LC23
	.long	1130
	.zero	4
	.quad	.LC24
	.section	.rodata.str1.1
.LC25:
	.string	"yi"
.LC26:
	.string	"yi_001"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_yi, @object
	.size	_ZL9locmap_yi, 32
_ZL9locmap_yi:
	.long	61
	.zero	4
	.quad	.LC25
	.long	1085
	.zero	4
	.quad	.LC26
	.section	.rodata.str1.1
.LC27:
	.string	"xh"
.LC28:
	.string	"xh_ZA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_xh, @object
	.size	_ZL9locmap_xh, 32
_ZL9locmap_xh:
	.long	52
	.zero	4
	.quad	.LC27
	.long	1076
	.zero	4
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"wo"
.LC30:
	.string	"wo_SN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_wo, @object
	.size	_ZL9locmap_wo, 32
_ZL9locmap_wo:
	.long	136
	.zero	4
	.quad	.LC29
	.long	1160
	.zero	4
	.quad	.LC30
	.section	.rodata.str1.1
.LC31:
	.string	"vi"
.LC32:
	.string	"vi_VN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_vi, @object
	.size	_ZL9locmap_vi, 32
_ZL9locmap_vi:
	.long	42
	.zero	4
	.quad	.LC31
	.long	1066
	.zero	4
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"ve"
.LC34:
	.string	"ve_ZA"
.LC35:
	.string	"ven_ZA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ve, @object
	.size	_ZL9locmap_ve, 48
_ZL9locmap_ve:
	.long	51
	.zero	4
	.quad	.LC33
	.long	1075
	.zero	4
	.quad	.LC34
	.long	1075
	.zero	4
	.quad	.LC35
	.section	.rodata.str1.1
.LC36:
	.string	"uz"
.LC37:
	.string	"uz_Cyrl_UZ"
.LC38:
	.string	"uz_Cyrl"
.LC39:
	.string	"uz_UZ"
.LC40:
	.string	"uz_Latn_UZ"
.LC41:
	.string	"uz_Latn"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_uz, @object
	.size	_ZL9locmap_uz, 96
_ZL9locmap_uz:
	.long	67
	.zero	4
	.quad	.LC36
	.long	2115
	.zero	4
	.quad	.LC37
	.long	30787
	.zero	4
	.quad	.LC38
	.long	2115
	.zero	4
	.quad	.LC39
	.long	1091
	.zero	4
	.quad	.LC40
	.long	31811
	.zero	4
	.quad	.LC41
	.section	.rodata.str1.1
.LC42:
	.string	"ur"
.LC43:
	.string	"ur_IN"
.LC44:
	.string	"ur_PK"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ur, @object
	.size	_ZL9locmap_ur, 48
_ZL9locmap_ur:
	.long	32
	.zero	4
	.quad	.LC42
	.long	2080
	.zero	4
	.quad	.LC43
	.long	1056
	.zero	4
	.quad	.LC44
	.section	.rodata.str1.1
.LC45:
	.string	"uk"
.LC46:
	.string	"uk_UA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_uk, @object
	.size	_ZL9locmap_uk, 32
_ZL9locmap_uk:
	.long	34
	.zero	4
	.quad	.LC45
	.long	1058
	.zero	4
	.quad	.LC46
	.section	.rodata.str1.1
.LC47:
	.string	"ug"
.LC48:
	.string	"ug_CN"
.LC49:
	.string	"ug_Arab_CN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ug, @object
	.size	_ZL9locmap_ug, 48
_ZL9locmap_ug:
	.long	128
	.zero	4
	.quad	.LC47
	.long	1152
	.zero	4
	.quad	.LC48
	.long	1152
	.zero	4
	.quad	.LC49
	.section	.rodata.str1.1
.LC50:
	.string	"tzm"
.LC51:
	.string	"tzm_Latn"
.LC52:
	.string	"tzm_Latn_DZ"
.LC53:
	.string	"tzm_Tfng_MA"
.LC54:
	.string	"tzm_Arab_MA"
.LC55:
	.string	"tmz"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_tzm, @object
	.size	_ZL10locmap_tzm, 96
_ZL10locmap_tzm:
	.long	95
	.zero	4
	.quad	.LC50
	.long	31839
	.zero	4
	.quad	.LC51
	.long	2143
	.zero	4
	.quad	.LC52
	.long	4191
	.zero	4
	.quad	.LC53
	.long	1119
	.zero	4
	.quad	.LC54
	.long	1119
	.zero	4
	.quad	.LC55
	.section	.rodata.str1.1
.LC56:
	.string	"tt"
.LC57:
	.string	"tt_RU"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_tt, @object
	.size	_ZL9locmap_tt, 32
_ZL9locmap_tt:
	.long	68
	.zero	4
	.quad	.LC56
	.long	1092
	.zero	4
	.quad	.LC57
	.section	.rodata.str1.1
.LC58:
	.string	"ts"
.LC59:
	.string	"ts_ZA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ts, @object
	.size	_ZL9locmap_ts, 32
_ZL9locmap_ts:
	.long	49
	.zero	4
	.quad	.LC58
	.long	1073
	.zero	4
	.quad	.LC59
	.section	.rodata.str1.1
.LC60:
	.string	"tr"
.LC61:
	.string	"tr_TR"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_tr, @object
	.size	_ZL9locmap_tr, 32
_ZL9locmap_tr:
	.long	31
	.zero	4
	.quad	.LC60
	.long	1055
	.zero	4
	.quad	.LC61
	.section	.rodata.str1.1
.LC62:
	.string	"tn"
.LC63:
	.string	"tn_BW"
.LC64:
	.string	"tn_ZA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_tn, @object
	.size	_ZL9locmap_tn, 48
_ZL9locmap_tn:
	.long	50
	.zero	4
	.quad	.LC62
	.long	2098
	.zero	4
	.quad	.LC63
	.long	1074
	.zero	4
	.quad	.LC64
	.section	.rodata.str1.1
.LC65:
	.string	"tk"
.LC66:
	.string	"tk_TM"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_tk, @object
	.size	_ZL9locmap_tk, 32
_ZL9locmap_tk:
	.long	66
	.zero	4
	.quad	.LC65
	.long	1090
	.zero	4
	.quad	.LC66
	.section	.rodata.str1.1
.LC67:
	.string	"ti"
.LC68:
	.string	"ti_ER"
.LC69:
	.string	"ti_ET"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ti, @object
	.size	_ZL9locmap_ti, 48
_ZL9locmap_ti:
	.long	115
	.zero	4
	.quad	.LC67
	.long	2163
	.zero	4
	.quad	.LC68
	.long	1139
	.zero	4
	.quad	.LC69
	.section	.rodata.str1.1
.LC70:
	.string	"th"
.LC71:
	.string	"th_TH"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_th, @object
	.size	_ZL9locmap_th, 32
_ZL9locmap_th:
	.long	30
	.zero	4
	.quad	.LC70
	.long	1054
	.zero	4
	.quad	.LC71
	.section	.rodata.str1.1
.LC72:
	.string	"tg"
.LC73:
	.string	"tg_Cyrl"
.LC74:
	.string	"tg_Cyrl_TJ"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_tg, @object
	.size	_ZL9locmap_tg, 48
_ZL9locmap_tg:
	.long	40
	.zero	4
	.quad	.LC72
	.long	31784
	.zero	4
	.quad	.LC73
	.long	1064
	.zero	4
	.quad	.LC74
	.section	.rodata.str1.1
.LC75:
	.string	"te"
.LC76:
	.string	"te_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_te, @object
	.size	_ZL9locmap_te, 32
_ZL9locmap_te:
	.long	74
	.zero	4
	.quad	.LC75
	.long	1098
	.zero	4
	.quad	.LC76
	.section	.rodata.str1.1
.LC77:
	.string	"ta"
.LC78:
	.string	"ta_IN"
.LC79:
	.string	"ta_LK"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ta, @object
	.size	_ZL9locmap_ta, 48
_ZL9locmap_ta:
	.long	73
	.zero	4
	.quad	.LC77
	.long	1097
	.zero	4
	.quad	.LC78
	.long	2121
	.zero	4
	.quad	.LC79
	.section	.rodata.str1.1
.LC80:
	.string	"syr"
.LC81:
	.string	"syr_SY"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_syr, @object
	.size	_ZL10locmap_syr, 32
_ZL10locmap_syr:
	.long	90
	.zero	4
	.quad	.LC80
	.long	1114
	.zero	4
	.quad	.LC81
	.section	.rodata.str1.1
.LC82:
	.string	"sw"
.LC83:
	.string	"sw_KE"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_sw, @object
	.size	_ZL9locmap_sw, 32
_ZL9locmap_sw:
	.long	65
	.zero	4
	.quad	.LC82
	.long	1089
	.zero	4
	.quad	.LC83
	.section	.rodata.str1.1
.LC84:
	.string	"sv"
.LC85:
	.string	"sv_FI"
.LC86:
	.string	"sv_SE"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_sv, @object
	.size	_ZL9locmap_sv, 48
_ZL9locmap_sv:
	.long	29
	.zero	4
	.quad	.LC84
	.long	2077
	.zero	4
	.quad	.LC85
	.long	1053
	.zero	4
	.quad	.LC86
	.section	.rodata.str1.1
.LC87:
	.string	"st"
.LC88:
	.string	"st_ZA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_st, @object
	.size	_ZL9locmap_st, 32
_ZL9locmap_st:
	.long	48
	.zero	4
	.quad	.LC87
	.long	1072
	.zero	4
	.quad	.LC88
	.section	.rodata.str1.1
.LC89:
	.string	"sq"
.LC90:
	.string	"sq_AL"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_sq, @object
	.size	_ZL9locmap_sq, 32
_ZL9locmap_sq:
	.long	28
	.zero	4
	.quad	.LC89
	.long	1052
	.zero	4
	.quad	.LC90
	.section	.rodata.str1.1
.LC91:
	.string	"so"
.LC92:
	.string	"so_SO"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_so, @object
	.size	_ZL9locmap_so, 32
_ZL9locmap_so:
	.long	119
	.zero	4
	.quad	.LC91
	.long	1143
	.zero	4
	.quad	.LC92
	.section	.rodata.str1.1
.LC93:
	.string	"sl"
.LC94:
	.string	"sl_SI"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_sl, @object
	.size	_ZL9locmap_sl, 32
_ZL9locmap_sl:
	.long	36
	.zero	4
	.quad	.LC93
	.long	1060
	.zero	4
	.quad	.LC94
	.section	.rodata.str1.1
.LC95:
	.string	"sk"
.LC96:
	.string	"sk_SK"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_sk, @object
	.size	_ZL9locmap_sk, 32
_ZL9locmap_sk:
	.long	27
	.zero	4
	.quad	.LC95
	.long	1051
	.zero	4
	.quad	.LC96
	.section	.rodata.str1.1
.LC97:
	.string	"si"
.LC98:
	.string	"si_LK"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_si, @object
	.size	_ZL9locmap_si, 32
_ZL9locmap_si:
	.long	91
	.zero	4
	.quad	.LC97
	.long	1115
	.zero	4
	.quad	.LC98
	.section	.rodata.str1.1
.LC99:
	.string	"se"
.LC100:
	.string	"se_FI"
.LC101:
	.string	"se_NO"
.LC102:
	.string	"se_SE"
.LC103:
	.string	"sma"
.LC104:
	.string	"sma_NO"
.LC105:
	.string	"sma_SE"
.LC106:
	.string	"smj"
.LC107:
	.string	"smn"
.LC108:
	.string	"sms"
.LC109:
	.string	"smj_NO"
.LC110:
	.string	"smj_SE"
.LC111:
	.string	"smn_FI"
.LC112:
	.string	"sms_FI"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_se, @object
	.size	_ZL9locmap_se, 224
_ZL9locmap_se:
	.long	59
	.zero	4
	.quad	.LC99
	.long	3131
	.zero	4
	.quad	.LC100
	.long	1083
	.zero	4
	.quad	.LC101
	.long	2107
	.zero	4
	.quad	.LC102
	.long	30779
	.zero	4
	.quad	.LC103
	.long	6203
	.zero	4
	.quad	.LC104
	.long	7227
	.zero	4
	.quad	.LC105
	.long	31803
	.zero	4
	.quad	.LC106
	.long	28731
	.zero	4
	.quad	.LC107
	.long	29755
	.zero	4
	.quad	.LC108
	.long	4155
	.zero	4
	.quad	.LC109
	.long	5179
	.zero	4
	.quad	.LC110
	.long	9275
	.zero	4
	.quad	.LC111
	.long	8251
	.zero	4
	.quad	.LC112
	.section	.rodata.str1.1
.LC113:
	.string	"sd"
.LC114:
	.string	"sd_Deva_IN"
.LC115:
	.string	"sd_IN"
.LC116:
	.string	"sd_Arab_PK"
.LC117:
	.string	"sd_PK"
.LC118:
	.string	"sd_Arab"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_sd, @object
	.size	_ZL9locmap_sd, 96
_ZL9locmap_sd:
	.long	89
	.zero	4
	.quad	.LC113
	.long	1113
	.zero	4
	.quad	.LC114
	.long	1113
	.zero	4
	.quad	.LC115
	.long	2137
	.zero	4
	.quad	.LC116
	.long	2137
	.zero	4
	.quad	.LC117
	.long	31833
	.zero	4
	.quad	.LC118
	.section	.rodata.str1.1
.LC119:
	.string	"sah"
.LC120:
	.string	"sah_RU"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_sah, @object
	.size	_ZL10locmap_sah, 32
_ZL10locmap_sah:
	.long	133
	.zero	4
	.quad	.LC119
	.long	1157
	.zero	4
	.quad	.LC120
	.section	.rodata.str1.1
.LC121:
	.string	"sa"
.LC122:
	.string	"sa_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_sa, @object
	.size	_ZL9locmap_sa, 32
_ZL9locmap_sa:
	.long	79
	.zero	4
	.quad	.LC121
	.long	1103
	.zero	4
	.quad	.LC122
	.section	.rodata.str1.1
.LC123:
	.string	"rw"
.LC124:
	.string	"rw_RW"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_rw, @object
	.size	_ZL9locmap_rw, 32
_ZL9locmap_rw:
	.long	135
	.zero	4
	.quad	.LC123
	.long	1159
	.zero	4
	.quad	.LC124
	.section	.rodata.str1.1
.LC125:
	.string	"ru"
.LC126:
	.string	"ru_RU"
.LC127:
	.string	"ru_MD"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ru, @object
	.size	_ZL9locmap_ru, 48
_ZL9locmap_ru:
	.long	25
	.zero	4
	.quad	.LC125
	.long	1049
	.zero	4
	.quad	.LC126
	.long	2073
	.zero	4
	.quad	.LC127
	.section	.rodata.str1.1
.LC128:
	.string	"root"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZL11locmap_root, @object
	.size	_ZL11locmap_root, 16
_ZL11locmap_root:
	.long	0
	.zero	4
	.quad	.LC128
	.section	.rodata.str1.1
.LC129:
	.string	"ro"
.LC130:
	.string	"ro_RO"
.LC131:
	.string	"ro_MD"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ro, @object
	.size	_ZL9locmap_ro, 48
_ZL9locmap_ro:
	.long	24
	.zero	4
	.quad	.LC129
	.long	1048
	.zero	4
	.quad	.LC130
	.long	2072
	.zero	4
	.quad	.LC131
	.section	.rodata.str1.1
.LC132:
	.string	"rm"
.LC133:
	.string	"rm_CH"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_rm, @object
	.size	_ZL9locmap_rm, 32
_ZL9locmap_rm:
	.long	23
	.zero	4
	.quad	.LC132
	.long	1047
	.zero	4
	.quad	.LC133
	.section	.rodata.str1.1
.LC134:
	.string	"qut"
.LC135:
	.string	"qut_GT"
.LC136:
	.string	"quc_Latn_GT"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_qut, @object
	.size	_ZL10locmap_qut, 48
_ZL10locmap_qut:
	.long	134
	.zero	4
	.quad	.LC134
	.long	1158
	.zero	4
	.quad	.LC135
	.long	1158
	.zero	4
	.quad	.LC136
	.section	.rodata.str1.1
.LC137:
	.string	"quc"
.LC138:
	.string	"quc_CO"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_quc, @object
	.size	_ZL10locmap_quc, 48
_ZL10locmap_quc:
	.long	147
	.zero	4
	.quad	.LC137
	.long	1171
	.zero	4
	.quad	.LC138
	.long	1158
	.zero	4
	.quad	.LC136
	.section	.rodata.str1.1
.LC139:
	.string	"qu"
.LC140:
	.string	"qu_BO"
.LC141:
	.string	"qu_EC"
.LC142:
	.string	"qu_PE"
.LC143:
	.string	"quz_BO"
.LC144:
	.string	"quz_EC"
.LC145:
	.string	"quz_PE"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_qu, @object
	.size	_ZL9locmap_qu, 112
_ZL9locmap_qu:
	.long	107
	.zero	4
	.quad	.LC139
	.long	1131
	.zero	4
	.quad	.LC140
	.long	2155
	.zero	4
	.quad	.LC141
	.long	3179
	.zero	4
	.quad	.LC142
	.long	1131
	.zero	4
	.quad	.LC143
	.long	2155
	.zero	4
	.quad	.LC144
	.long	3179
	.zero	4
	.quad	.LC145
	.section	.rodata.str1.1
.LC146:
	.string	"pt"
.LC147:
	.string	"pt_BR"
.LC148:
	.string	"pt_PT"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_pt, @object
	.size	_ZL9locmap_pt, 48
_ZL9locmap_pt:
	.long	22
	.zero	4
	.quad	.LC146
	.long	1046
	.zero	4
	.quad	.LC147
	.long	2070
	.zero	4
	.quad	.LC148
	.section	.rodata.str1.1
.LC149:
	.string	"ps"
.LC150:
	.string	"ps_AF"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ps, @object
	.size	_ZL9locmap_ps, 32
_ZL9locmap_ps:
	.long	99
	.zero	4
	.quad	.LC149
	.long	1123
	.zero	4
	.quad	.LC150
	.section	.rodata.str1.1
.LC151:
	.string	"pl"
.LC152:
	.string	"pl_PL"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_pl, @object
	.size	_ZL9locmap_pl, 32
_ZL9locmap_pl:
	.long	21
	.zero	4
	.quad	.LC151
	.long	1045
	.zero	4
	.quad	.LC152
	.section	.rodata.str1.1
.LC153:
	.string	"pap"
.LC154:
	.string	"pap_029"
.LC155:
	.string	"pap_AN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_pap, @object
	.size	_ZL10locmap_pap, 48
_ZL10locmap_pap:
	.long	121
	.zero	4
	.quad	.LC153
	.long	1145
	.zero	4
	.quad	.LC154
	.long	1145
	.zero	4
	.quad	.LC155
	.section	.rodata.str1.1
.LC156:
	.string	"pa"
.LC157:
	.string	"pa_IN"
.LC158:
	.string	"pa_Arab_PK"
.LC159:
	.string	"pa_PK"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_pa, @object
	.size	_ZL9locmap_pa, 64
_ZL9locmap_pa:
	.long	70
	.zero	4
	.quad	.LC156
	.long	1094
	.zero	4
	.quad	.LC157
	.long	2118
	.zero	4
	.quad	.LC158
	.long	2118
	.zero	4
	.quad	.LC159
	.section	.rodata.str1.1
.LC160:
	.string	"or"
.LC161:
	.string	"or_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL12locmap_or_IN, @object
	.size	_ZL12locmap_or_IN, 32
_ZL12locmap_or_IN:
	.long	72
	.zero	4
	.quad	.LC160
	.long	1096
	.zero	4
	.quad	.LC161
	.section	.rodata.str1.1
.LC162:
	.string	"om"
.LC163:
	.string	"om_ET"
.LC164:
	.string	"gaz_ET"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_om, @object
	.size	_ZL9locmap_om, 48
_ZL9locmap_om:
	.long	114
	.zero	4
	.quad	.LC162
	.long	1138
	.zero	4
	.quad	.LC163
	.long	1138
	.zero	4
	.quad	.LC164
	.section	.rodata.str1.1
.LC165:
	.string	"oc"
.LC166:
	.string	"oc_FR"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_oc, @object
	.size	_ZL9locmap_oc, 32
_ZL9locmap_oc:
	.long	130
	.zero	4
	.quad	.LC165
	.long	1154
	.zero	4
	.quad	.LC166
	.section	.rodata.str1.1
.LC167:
	.string	"nso"
.LC168:
	.string	"nso_ZA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_nso, @object
	.size	_ZL10locmap_nso, 32
_ZL10locmap_nso:
	.long	108
	.zero	4
	.quad	.LC167
	.long	1132
	.zero	4
	.quad	.LC168
	.section	.rodata.str1.1
.LC169:
	.string	"no"
.LC170:
	.string	"nb"
.LC171:
	.string	"nb_NO"
.LC172:
	.string	"no_NO"
.LC173:
	.string	"nn_NO"
.LC174:
	.string	"nn"
.LC175:
	.string	"no_NO_NY"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_no, @object
	.size	_ZL9locmap_no, 112
_ZL9locmap_no:
	.long	20
	.zero	4
	.quad	.LC169
	.long	31764
	.zero	4
	.quad	.LC170
	.long	1044
	.zero	4
	.quad	.LC171
	.long	1044
	.zero	4
	.quad	.LC172
	.long	2068
	.zero	4
	.quad	.LC173
	.long	30740
	.zero	4
	.quad	.LC174
	.long	2068
	.zero	4
	.quad	.LC175
	.section	.rodata.str1.1
.LC176:
	.string	"nl"
.LC177:
	.string	"nl_BE"
.LC178:
	.string	"nl_NL"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_nl, @object
	.size	_ZL9locmap_nl, 48
_ZL9locmap_nl:
	.long	19
	.zero	4
	.quad	.LC176
	.long	2067
	.zero	4
	.quad	.LC177
	.long	1043
	.zero	4
	.quad	.LC178
	.section	.rodata.str1.1
.LC179:
	.string	"ne"
.LC180:
	.string	"ne_IN"
.LC181:
	.string	"ne_NP"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ne, @object
	.size	_ZL9locmap_ne, 48
_ZL9locmap_ne:
	.long	97
	.zero	4
	.quad	.LC179
	.long	2145
	.zero	4
	.quad	.LC180
	.long	1121
	.zero	4
	.quad	.LC181
	.section	.rodata.str1.1
.LC182:
	.string	"my"
.LC183:
	.string	"my_MM"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_my, @object
	.size	_ZL9locmap_my, 32
_ZL9locmap_my:
	.long	85
	.zero	4
	.quad	.LC182
	.long	1109
	.zero	4
	.quad	.LC183
	.section	.rodata.str1.1
.LC184:
	.string	"mt"
.LC185:
	.string	"mt_MT"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_mt, @object
	.size	_ZL9locmap_mt, 32
_ZL9locmap_mt:
	.long	58
	.zero	4
	.quad	.LC184
	.long	1082
	.zero	4
	.quad	.LC185
	.section	.rodata.str1.1
.LC186:
	.string	"ms"
.LC187:
	.string	"ms_BN"
.LC188:
	.string	"ms_MY"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ms, @object
	.size	_ZL9locmap_ms, 48
_ZL9locmap_ms:
	.long	62
	.zero	4
	.quad	.LC186
	.long	2110
	.zero	4
	.quad	.LC187
	.long	1086
	.zero	4
	.quad	.LC188
	.section	.rodata.str1.1
.LC189:
	.string	"mr"
.LC190:
	.string	"mr_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_mr, @object
	.size	_ZL9locmap_mr, 32
_ZL9locmap_mr:
	.long	78
	.zero	4
	.quad	.LC189
	.long	1102
	.zero	4
	.quad	.LC190
	.section	.rodata.str1.1
.LC191:
	.string	"moh"
.LC192:
	.string	"moh_CA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_moh, @object
	.size	_ZL10locmap_moh, 32
_ZL10locmap_moh:
	.long	124
	.zero	4
	.quad	.LC191
	.long	1148
	.zero	4
	.quad	.LC192
	.section	.rodata.str1.1
.LC193:
	.string	"mni"
.LC194:
	.string	"mni_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_mni, @object
	.size	_ZL10locmap_mni, 32
_ZL10locmap_mni:
	.long	88
	.zero	4
	.quad	.LC193
	.long	1112
	.zero	4
	.quad	.LC194
	.section	.rodata.str1.1
.LC195:
	.string	"mn"
.LC196:
	.string	"mn_MN"
.LC197:
	.string	"mn_Mong"
.LC198:
	.string	"mn_Mong_CN"
.LC199:
	.string	"mn_CN"
.LC200:
	.string	"mn_Cyrl"
.LC201:
	.string	"mn_Mong_MN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_mn, @object
	.size	_ZL9locmap_mn, 112
_ZL9locmap_mn:
	.long	80
	.zero	4
	.quad	.LC195
	.long	1104
	.zero	4
	.quad	.LC196
	.long	31824
	.zero	4
	.quad	.LC197
	.long	2128
	.zero	4
	.quad	.LC198
	.long	2128
	.zero	4
	.quad	.LC199
	.long	30800
	.zero	4
	.quad	.LC200
	.long	3152
	.zero	4
	.quad	.LC201
	.section	.rodata.str1.1
.LC202:
	.string	"ml"
.LC203:
	.string	"ml_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ml, @object
	.size	_ZL9locmap_ml, 32
_ZL9locmap_ml:
	.long	76
	.zero	4
	.quad	.LC202
	.long	1100
	.zero	4
	.quad	.LC203
	.section	.rodata.str1.1
.LC204:
	.string	"mk"
.LC205:
	.string	"mk_MK"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_mk, @object
	.size	_ZL9locmap_mk, 32
_ZL9locmap_mk:
	.long	47
	.zero	4
	.quad	.LC204
	.long	1071
	.zero	4
	.quad	.LC205
	.section	.rodata.str1.1
.LC206:
	.string	"mi"
.LC207:
	.string	"mi_NZ"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_mi, @object
	.size	_ZL9locmap_mi, 32
_ZL9locmap_mi:
	.long	129
	.zero	4
	.quad	.LC206
	.long	1153
	.zero	4
	.quad	.LC207
	.section	.rodata.str1.1
.LC208:
	.string	"lv"
.LC209:
	.string	"lv_LV"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_lv, @object
	.size	_ZL9locmap_lv, 32
_ZL9locmap_lv:
	.long	38
	.zero	4
	.quad	.LC208
	.long	1062
	.zero	4
	.quad	.LC209
	.section	.rodata.str1.1
.LC210:
	.string	"lt"
.LC211:
	.string	"lt_LT"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_lt, @object
	.size	_ZL9locmap_lt, 32
_ZL9locmap_lt:
	.long	39
	.zero	4
	.quad	.LC210
	.long	1063
	.zero	4
	.quad	.LC211
	.section	.rodata.str1.1
.LC212:
	.string	"lo"
.LC213:
	.string	"lo_LA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_lo, @object
	.size	_ZL9locmap_lo, 32
_ZL9locmap_lo:
	.long	84
	.zero	4
	.quad	.LC212
	.long	1108
	.zero	4
	.quad	.LC213
	.section	.rodata.str1.1
.LC214:
	.string	"lb_LU"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_lb, @object
	.size	_ZL9locmap_lb, 32
_ZL9locmap_lb:
	.long	110
	.zero	4
	.quad	.LC0
	.long	1134
	.zero	4
	.quad	.LC214
	.section	.rodata.str1.1
.LC215:
	.string	"la"
.LC216:
	.string	"la_001"
.LC217:
	.string	"la_IT"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_la, @object
	.size	_ZL9locmap_la, 48
_ZL9locmap_la:
	.long	118
	.zero	4
	.quad	.LC215
	.long	1142
	.zero	4
	.quad	.LC216
	.long	1142
	.zero	4
	.quad	.LC217
	.section	.rodata.str1.1
.LC218:
	.string	"ky"
.LC219:
	.string	"ky_KG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ky, @object
	.size	_ZL9locmap_ky, 32
_ZL9locmap_ky:
	.long	64
	.zero	4
	.quad	.LC218
	.long	1088
	.zero	4
	.quad	.LC219
	.section	.rodata.str1.1
.LC220:
	.string	"ks"
.LC221:
	.string	"ks_Arab_IN"
.LC222:
	.string	"ks_Deva_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ks, @object
	.size	_ZL9locmap_ks, 48
_ZL9locmap_ks:
	.long	96
	.zero	4
	.quad	.LC220
	.long	1120
	.zero	4
	.quad	.LC221
	.long	2144
	.zero	4
	.quad	.LC222
	.section	.rodata.str1.1
.LC223:
	.string	"kr"
.LC224:
	.string	"kr_NG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_kr, @object
	.size	_ZL9locmap_kr, 32
_ZL9locmap_kr:
	.long	113
	.zero	4
	.quad	.LC223
	.long	1137
	.zero	4
	.quad	.LC224
	.section	.rodata.str1.1
.LC225:
	.string	"kok"
.LC226:
	.string	"kok_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_kok, @object
	.size	_ZL10locmap_kok, 32
_ZL10locmap_kok:
	.long	87
	.zero	4
	.quad	.LC225
	.long	1111
	.zero	4
	.quad	.LC226
	.section	.rodata.str1.1
.LC227:
	.string	"ko"
.LC228:
	.string	"ko_KP"
.LC229:
	.string	"ko_KR"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ko, @object
	.size	_ZL9locmap_ko, 48
_ZL9locmap_ko:
	.long	18
	.zero	4
	.quad	.LC227
	.long	2066
	.zero	4
	.quad	.LC228
	.long	1042
	.zero	4
	.quad	.LC229
	.section	.rodata.str1.1
.LC230:
	.string	"kn"
.LC231:
	.string	"kn_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_kn, @object
	.size	_ZL9locmap_kn, 32
_ZL9locmap_kn:
	.long	75
	.zero	4
	.quad	.LC230
	.long	1099
	.zero	4
	.quad	.LC231
	.section	.rodata.str1.1
.LC232:
	.string	"km"
.LC233:
	.string	"km_KH"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_km, @object
	.size	_ZL9locmap_km, 32
_ZL9locmap_km:
	.long	83
	.zero	4
	.quad	.LC232
	.long	1107
	.zero	4
	.quad	.LC233
	.section	.rodata.str1.1
.LC234:
	.string	"kl"
.LC235:
	.string	"kl_GL"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_kl, @object
	.size	_ZL9locmap_kl, 32
_ZL9locmap_kl:
	.long	111
	.zero	4
	.quad	.LC234
	.long	1135
	.zero	4
	.quad	.LC235
	.section	.rodata.str1.1
.LC236:
	.string	"kk"
.LC237:
	.string	"kk_KZ"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_kk, @object
	.size	_ZL9locmap_kk, 32
_ZL9locmap_kk:
	.long	63
	.zero	4
	.quad	.LC236
	.long	1087
	.zero	4
	.quad	.LC237
	.section	.rodata.str1.1
.LC238:
	.string	"ka"
.LC239:
	.string	"ka_GE"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ka, @object
	.size	_ZL9locmap_ka, 32
_ZL9locmap_ka:
	.long	55
	.zero	4
	.quad	.LC238
	.long	1079
	.zero	4
	.quad	.LC239
	.section	.rodata.str1.1
.LC240:
	.string	"ja"
.LC241:
	.string	"ja_JP"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ja, @object
	.size	_ZL9locmap_ja, 32
_ZL9locmap_ja:
	.long	17
	.zero	4
	.quad	.LC240
	.long	1041
	.zero	4
	.quad	.LC241
	.section	.rodata.str1.1
.LC242:
	.string	"iw"
.LC243:
	.string	"iw_IL"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_iw, @object
	.size	_ZL9locmap_iw, 32
_ZL9locmap_iw:
	.long	13
	.zero	4
	.quad	.LC242
	.long	1037
	.zero	4
	.quad	.LC243
	.section	.rodata.str1.1
.LC244:
	.string	"iu"
.LC245:
	.string	"iu_Cans_CA"
.LC246:
	.string	"iu_Cans"
.LC247:
	.string	"iu_Latn_CA"
.LC248:
	.string	"iu_Latn"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_iu, @object
	.size	_ZL9locmap_iu, 80
_ZL9locmap_iu:
	.long	93
	.zero	4
	.quad	.LC244
	.long	1117
	.zero	4
	.quad	.LC245
	.long	30813
	.zero	4
	.quad	.LC246
	.long	2141
	.zero	4
	.quad	.LC247
	.long	31837
	.zero	4
	.quad	.LC248
	.section	.rodata.str1.1
.LC249:
	.string	"it"
.LC250:
	.string	"it_CH"
.LC251:
	.string	"it_IT"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_it, @object
	.size	_ZL9locmap_it, 48
_ZL9locmap_it:
	.long	16
	.zero	4
	.quad	.LC249
	.long	2064
	.zero	4
	.quad	.LC250
	.long	1040
	.zero	4
	.quad	.LC251
	.section	.rodata.str1.1
.LC252:
	.string	"is"
.LC253:
	.string	"is_IS"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_is, @object
	.size	_ZL9locmap_is, 32
_ZL9locmap_is:
	.long	15
	.zero	4
	.quad	.LC252
	.long	1039
	.zero	4
	.quad	.LC253
	.section	.rodata.str1.1
.LC254:
	.string	"ii"
.LC255:
	.string	"ii_CN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ii, @object
	.size	_ZL9locmap_ii, 32
_ZL9locmap_ii:
	.long	120
	.zero	4
	.quad	.LC254
	.long	1144
	.zero	4
	.quad	.LC255
	.section	.rodata.str1.1
.LC256:
	.string	"ig"
.LC257:
	.string	"ig_NG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ig, @object
	.size	_ZL9locmap_ig, 32
_ZL9locmap_ig:
	.long	112
	.zero	4
	.quad	.LC256
	.long	1136
	.zero	4
	.quad	.LC257
	.section	.rodata.str1.1
.LC258:
	.string	"id"
.LC259:
	.string	"id_ID"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_id, @object
	.size	_ZL9locmap_id, 32
_ZL9locmap_id:
	.long	33
	.zero	4
	.quad	.LC258
	.long	1057
	.zero	4
	.quad	.LC259
	.section	.rodata.str1.1
.LC260:
	.string	"ibb"
.LC261:
	.string	"ibb_NG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_ibb, @object
	.size	_ZL10locmap_ibb, 32
_ZL10locmap_ibb:
	.long	105
	.zero	4
	.quad	.LC260
	.long	1129
	.zero	4
	.quad	.LC261
	.section	.rodata.str1.1
.LC262:
	.string	"hy"
.LC263:
	.string	"hy_AM"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_hy, @object
	.size	_ZL9locmap_hy, 32
_ZL9locmap_hy:
	.long	43
	.zero	4
	.quad	.LC262
	.long	1067
	.zero	4
	.quad	.LC263
	.section	.rodata.str1.1
.LC264:
	.string	"hu"
.LC265:
	.string	"hu_HU"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_hu, @object
	.size	_ZL9locmap_hu, 32
_ZL9locmap_hu:
	.long	14
	.zero	4
	.quad	.LC264
	.long	1038
	.zero	4
	.quad	.LC265
	.section	.rodata.str1.1
.LC266:
	.string	"hsb"
.LC267:
	.string	"hsb_DE"
.LC268:
	.string	"dsb_DE"
.LC269:
	.string	"dsb"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_hsb, @object
	.size	_ZL10locmap_hsb, 64
_ZL10locmap_hsb:
	.long	46
	.zero	4
	.quad	.LC266
	.long	1070
	.zero	4
	.quad	.LC267
	.long	2094
	.zero	4
	.quad	.LC268
	.long	31790
	.zero	4
	.quad	.LC269
	.section	.rodata.str1.1
.LC270:
	.string	"hr"
.LC271:
	.string	"bs_Latn_BA"
.LC272:
	.string	"bs_Latn"
.LC273:
	.string	"bs_BA"
.LC274:
	.string	"bs"
.LC275:
	.string	"bs_Cyrl_BA"
.LC276:
	.string	"bs_Cyrl"
.LC277:
	.string	"hr_BA"
.LC278:
	.string	"hr_HR"
.LC279:
	.string	"sr_Latn_ME"
.LC280:
	.string	"sr_Latn_RS"
.LC281:
	.string	"sr_Latn_BA"
.LC282:
	.string	"sr_Latn_CS"
.LC283:
	.string	"sr_Latn"
.LC284:
	.string	"sr_Cyrl_BA"
.LC285:
	.string	"sr_Cyrl_CS"
.LC286:
	.string	"sr_Cyrl_ME"
.LC287:
	.string	"sr_Cyrl_RS"
.LC288:
	.string	"sr_Cyrl"
.LC289:
	.string	"sr"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_hr, @object
	.size	_ZL9locmap_hr, 320
_ZL9locmap_hr:
	.long	26
	.zero	4
	.quad	.LC270
	.long	5146
	.zero	4
	.quad	.LC271
	.long	26650
	.zero	4
	.quad	.LC272
	.long	5146
	.zero	4
	.quad	.LC273
	.long	30746
	.zero	4
	.quad	.LC274
	.long	8218
	.zero	4
	.quad	.LC275
	.long	25626
	.zero	4
	.quad	.LC276
	.long	4122
	.zero	4
	.quad	.LC277
	.long	1050
	.zero	4
	.quad	.LC278
	.long	11290
	.zero	4
	.quad	.LC279
	.long	9242
	.zero	4
	.quad	.LC280
	.long	6170
	.zero	4
	.quad	.LC281
	.long	2074
	.zero	4
	.quad	.LC282
	.long	28698
	.zero	4
	.quad	.LC283
	.long	7194
	.zero	4
	.quad	.LC284
	.long	3098
	.zero	4
	.quad	.LC285
	.long	12314
	.zero	4
	.quad	.LC286
	.long	10266
	.zero	4
	.quad	.LC287
	.long	27674
	.zero	4
	.quad	.LC288
	.long	31770
	.zero	4
	.quad	.LC289
	.section	.rodata.str1.1
.LC290:
	.string	"hi"
.LC291:
	.string	"hi_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_hi, @object
	.size	_ZL9locmap_hi, 32
_ZL9locmap_hi:
	.long	57
	.zero	4
	.quad	.LC290
	.long	1081
	.zero	4
	.quad	.LC291
	.section	.rodata.str1.1
.LC292:
	.string	"he"
.LC293:
	.string	"he_IL"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_he, @object
	.size	_ZL9locmap_he, 32
_ZL9locmap_he:
	.long	13
	.zero	4
	.quad	.LC292
	.long	1037
	.zero	4
	.quad	.LC293
	.section	.rodata.str1.1
.LC294:
	.string	"haw"
.LC295:
	.string	"haw_US"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_haw, @object
	.size	_ZL10locmap_haw, 32
_ZL10locmap_haw:
	.long	117
	.zero	4
	.quad	.LC294
	.long	1141
	.zero	4
	.quad	.LC295
	.section	.rodata.str1.1
.LC296:
	.string	"ha"
.LC297:
	.string	"ha_Latn"
.LC298:
	.string	"ha_Latn_NG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ha, @object
	.size	_ZL9locmap_ha, 48
_ZL9locmap_ha:
	.long	104
	.zero	4
	.quad	.LC296
	.long	31848
	.zero	4
	.quad	.LC297
	.long	1128
	.zero	4
	.quad	.LC298
	.section	.rodata.str1.1
.LC299:
	.string	"gsw"
.LC300:
	.string	"gsw_FR"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_gsw, @object
	.size	_ZL10locmap_gsw, 32
_ZL10locmap_gsw:
	.long	132
	.zero	4
	.quad	.LC299
	.long	1156
	.zero	4
	.quad	.LC300
	.section	.rodata.str1.1
.LC301:
	.string	"gn"
.LC302:
	.string	"gn_PY"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_gn, @object
	.size	_ZL9locmap_gn, 32
_ZL9locmap_gn:
	.long	116
	.zero	4
	.quad	.LC301
	.long	1140
	.zero	4
	.quad	.LC302
	.section	.rodata.str1.1
.LC303:
	.string	"gu"
.LC304:
	.string	"gu_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_gu, @object
	.size	_ZL9locmap_gu, 32
_ZL9locmap_gu:
	.long	71
	.zero	4
	.quad	.LC303
	.long	1095
	.zero	4
	.quad	.LC304
	.section	.rodata.str1.1
.LC305:
	.string	"gl"
.LC306:
	.string	"gl_ES"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_gl, @object
	.size	_ZL9locmap_gl, 32
_ZL9locmap_gl:
	.long	86
	.zero	4
	.quad	.LC305
	.long	1110
	.zero	4
	.quad	.LC306
	.section	.rodata.str1.1
.LC307:
	.string	"gd"
.LC308:
	.string	"gd_GB"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_gd, @object
	.size	_ZL9locmap_gd, 32
_ZL9locmap_gd:
	.long	145
	.zero	4
	.quad	.LC307
	.long	1169
	.zero	4
	.quad	.LC308
	.section	.rodata.str1.1
.LC309:
	.string	"ga"
.LC310:
	.string	"ga_IE"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ga, @object
	.size	_ZL9locmap_ga, 48
_ZL9locmap_ga:
	.long	60
	.zero	4
	.quad	.LC309
	.long	2108
	.zero	4
	.quad	.LC310
	.long	1084
	.zero	4
	.quad	.LC308
	.section	.rodata.str1.1
.LC311:
	.string	"fy"
.LC312:
	.string	"fy_NL"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_fy, @object
	.size	_ZL9locmap_fy, 32
_ZL9locmap_fy:
	.long	98
	.zero	4
	.quad	.LC311
	.long	1122
	.zero	4
	.quad	.LC312
	.section	.rodata.str1.1
.LC313:
	.string	"fuv"
.LC314:
	.string	"fuv_NG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_fuv, @object
	.size	_ZL10locmap_fuv, 32
_ZL10locmap_fuv:
	.long	103
	.zero	4
	.quad	.LC313
	.long	1127
	.zero	4
	.quad	.LC314
	.section	.rodata.str1.1
.LC315:
	.string	"fr"
.LC316:
	.string	"fr_BE"
.LC317:
	.string	"fr_CA"
.LC318:
	.string	"fr_CD"
.LC319:
	.string	"fr_CG"
.LC320:
	.string	"fr_CH"
.LC321:
	.string	"fr_CI"
.LC322:
	.string	"fr_CM"
.LC323:
	.string	"fr_FR"
.LC324:
	.string	"fr_HT"
.LC325:
	.string	"fr_LU"
.LC326:
	.string	"fr_MA"
.LC327:
	.string	"fr_MC"
.LC328:
	.string	"fr_ML"
.LC329:
	.string	"fr_RE"
.LC330:
	.string	"fr_SN"
.LC331:
	.string	"fr_015"
.LC332:
	.string	"fr_029"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_fr, @object
	.size	_ZL9locmap_fr, 288
_ZL9locmap_fr:
	.long	12
	.zero	4
	.quad	.LC315
	.long	2060
	.zero	4
	.quad	.LC316
	.long	3084
	.zero	4
	.quad	.LC317
	.long	9228
	.zero	4
	.quad	.LC318
	.long	9228
	.zero	4
	.quad	.LC319
	.long	4108
	.zero	4
	.quad	.LC320
	.long	12300
	.zero	4
	.quad	.LC321
	.long	11276
	.zero	4
	.quad	.LC322
	.long	1036
	.zero	4
	.quad	.LC323
	.long	15372
	.zero	4
	.quad	.LC324
	.long	5132
	.zero	4
	.quad	.LC325
	.long	14348
	.zero	4
	.quad	.LC326
	.long	6156
	.zero	4
	.quad	.LC327
	.long	13324
	.zero	4
	.quad	.LC328
	.long	8204
	.zero	4
	.quad	.LC329
	.long	10252
	.zero	4
	.quad	.LC330
	.long	58380
	.zero	4
	.quad	.LC331
	.long	7180
	.zero	4
	.quad	.LC332
	.section	.rodata.str1.1
.LC333:
	.string	"fo"
.LC334:
	.string	"fo_FO"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_fo, @object
	.size	_ZL9locmap_fo, 32
_ZL9locmap_fo:
	.long	56
	.zero	4
	.quad	.LC333
	.long	1080
	.zero	4
	.quad	.LC334
	.section	.rodata.str1.1
.LC335:
	.string	"fil"
.LC336:
	.string	"fil_PH"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_fil, @object
	.size	_ZL10locmap_fil, 32
_ZL10locmap_fil:
	.long	100
	.zero	4
	.quad	.LC335
	.long	1124
	.zero	4
	.quad	.LC336
	.section	.rodata.str1.1
.LC337:
	.string	"fi"
.LC338:
	.string	"fi_FI"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_fi, @object
	.size	_ZL9locmap_fi, 32
_ZL9locmap_fi:
	.long	11
	.zero	4
	.quad	.LC337
	.long	1035
	.zero	4
	.quad	.LC338
	.section	.rodata.str1.1
.LC339:
	.string	"ff"
.LC340:
	.string	"ff_Latn"
.LC341:
	.string	"ff_Latn_SN"
.LC342:
	.string	"ff_NG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ff, @object
	.size	_ZL9locmap_ff, 64
_ZL9locmap_ff:
	.long	103
	.zero	4
	.quad	.LC339
	.long	31847
	.zero	4
	.quad	.LC340
	.long	2151
	.zero	4
	.quad	.LC341
	.long	1127
	.zero	4
	.quad	.LC342
	.section	.rodata.str1.1
.LC343:
	.string	"fa_AF"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL12locmap_fa_AF, @object
	.size	_ZL12locmap_fa_AF, 32
_ZL12locmap_fa_AF:
	.long	140
	.zero	4
	.quad	.LC343
	.long	1164
	.zero	4
	.quad	.LC343
	.section	.rodata.str1.1
.LC344:
	.string	"fa"
.LC345:
	.string	"fa_IR"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_fa, @object
	.size	_ZL9locmap_fa, 48
_ZL9locmap_fa:
	.long	41
	.zero	4
	.quad	.LC344
	.long	1065
	.zero	4
	.quad	.LC345
	.long	1164
	.zero	4
	.quad	.LC343
	.section	.rodata.str1.1
.LC346:
	.string	"eu"
.LC347:
	.string	"eu_ES"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_eu, @object
	.size	_ZL9locmap_eu, 32
_ZL9locmap_eu:
	.long	45
	.zero	4
	.quad	.LC346
	.long	1069
	.zero	4
	.quad	.LC347
	.section	.rodata.str1.1
.LC348:
	.string	"et"
.LC349:
	.string	"et_EE"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_et, @object
	.size	_ZL9locmap_et, 32
_ZL9locmap_et:
	.long	37
	.zero	4
	.quad	.LC348
	.long	1061
	.zero	4
	.quad	.LC349
	.section	.rodata.str1.1
.LC350:
	.string	"es"
.LC351:
	.string	"es_AR"
.LC352:
	.string	"es_BO"
.LC353:
	.string	"es_CL"
.LC354:
	.string	"es_CO"
.LC355:
	.string	"es_CR"
.LC356:
	.string	"es_CU"
.LC357:
	.string	"es_DO"
.LC358:
	.string	"es_EC"
.LC359:
	.string	"es_ES"
.LC360:
	.string	"es_GT"
.LC361:
	.string	"es_HN"
.LC362:
	.string	"es_MX"
.LC363:
	.string	"es_NI"
.LC364:
	.string	"es_PA"
.LC365:
	.string	"es_PE"
.LC366:
	.string	"es_PR"
.LC367:
	.string	"es_PY"
.LC368:
	.string	"es_SV"
.LC369:
	.string	"es_US"
.LC370:
	.string	"es_UY"
.LC371:
	.string	"es_VE"
.LC372:
	.string	"es_419"
.LC373:
	.string	"es_ES@collation=traditional"
.LC374:
	.string	"es@collation=traditional"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_es, @object
	.size	_ZL9locmap_es, 400
_ZL9locmap_es:
	.long	10
	.zero	4
	.quad	.LC350
	.long	11274
	.zero	4
	.quad	.LC351
	.long	16394
	.zero	4
	.quad	.LC352
	.long	13322
	.zero	4
	.quad	.LC353
	.long	9226
	.zero	4
	.quad	.LC354
	.long	5130
	.zero	4
	.quad	.LC355
	.long	23562
	.zero	4
	.quad	.LC356
	.long	7178
	.zero	4
	.quad	.LC357
	.long	12298
	.zero	4
	.quad	.LC358
	.long	3082
	.zero	4
	.quad	.LC359
	.long	4106
	.zero	4
	.quad	.LC360
	.long	18442
	.zero	4
	.quad	.LC361
	.long	2058
	.zero	4
	.quad	.LC362
	.long	19466
	.zero	4
	.quad	.LC363
	.long	6154
	.zero	4
	.quad	.LC364
	.long	10250
	.zero	4
	.quad	.LC365
	.long	20490
	.zero	4
	.quad	.LC366
	.long	15370
	.zero	4
	.quad	.LC367
	.long	17418
	.zero	4
	.quad	.LC368
	.long	21514
	.zero	4
	.quad	.LC369
	.long	14346
	.zero	4
	.quad	.LC370
	.long	8202
	.zero	4
	.quad	.LC371
	.long	22538
	.zero	4
	.quad	.LC372
	.long	1034
	.zero	4
	.quad	.LC373
	.long	1034
	.zero	4
	.quad	.LC374
	.section	.rodata.str1.1
.LC375:
	.string	"en_US_POSIX"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZL18locmap_en_US_POSIX, @object
	.size	_ZL18locmap_en_US_POSIX, 16
_ZL18locmap_en_US_POSIX:
	.long	127
	.zero	4
	.quad	.LC375
	.section	.rodata.str1.1
.LC376:
	.string	"en"
.LC377:
	.string	"en_AU"
.LC378:
	.string	"en_BZ"
.LC379:
	.string	"en_CA"
.LC380:
	.string	"en_GB"
.LC381:
	.string	"en_HK"
.LC382:
	.string	"en_ID"
.LC383:
	.string	"en_IE"
.LC384:
	.string	"en_IN"
.LC385:
	.string	"en_JM"
.LC386:
	.string	"en_MY"
.LC387:
	.string	"en_NZ"
.LC388:
	.string	"en_PH"
.LC389:
	.string	"en_SG"
.LC390:
	.string	"en_TT"
.LC391:
	.string	"en_US"
.LC392:
	.string	"en_029"
.LC393:
	.string	"en_ZA"
.LC394:
	.string	"en_ZW"
.LC395:
	.string	"en_VI"
.LC396:
	.string	"en_AS"
.LC397:
	.string	"en_GU"
.LC398:
	.string	"en_MH"
.LC399:
	.string	"en_MP"
.LC400:
	.string	"en_UM"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_en, @object
	.size	_ZL9locmap_en, 416
_ZL9locmap_en:
	.long	9
	.zero	4
	.quad	.LC376
	.long	3081
	.zero	4
	.quad	.LC377
	.long	10249
	.zero	4
	.quad	.LC378
	.long	4105
	.zero	4
	.quad	.LC379
	.long	2057
	.zero	4
	.quad	.LC380
	.long	15369
	.zero	4
	.quad	.LC381
	.long	14345
	.zero	4
	.quad	.LC382
	.long	6153
	.zero	4
	.quad	.LC383
	.long	16393
	.zero	4
	.quad	.LC384
	.long	8201
	.zero	4
	.quad	.LC385
	.long	17417
	.zero	4
	.quad	.LC386
	.long	5129
	.zero	4
	.quad	.LC387
	.long	13321
	.zero	4
	.quad	.LC388
	.long	18441
	.zero	4
	.quad	.LC389
	.long	11273
	.zero	4
	.quad	.LC390
	.long	1033
	.zero	4
	.quad	.LC391
	.long	127
	.zero	4
	.quad	.LC375
	.long	9225
	.zero	4
	.quad	.LC392
	.long	7177
	.zero	4
	.quad	.LC393
	.long	12297
	.zero	4
	.quad	.LC394
	.long	9225
	.zero	4
	.quad	.LC395
	.long	1033
	.zero	4
	.quad	.LC396
	.long	1033
	.zero	4
	.quad	.LC397
	.long	1033
	.zero	4
	.quad	.LC398
	.long	1033
	.zero	4
	.quad	.LC399
	.long	1033
	.zero	4
	.quad	.LC400
	.section	.rodata.str1.1
.LC401:
	.string	"el"
.LC402:
	.string	"el_GR"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_el, @object
	.size	_ZL9locmap_el, 32
_ZL9locmap_el:
	.long	8
	.zero	4
	.quad	.LC401
	.long	1032
	.zero	4
	.quad	.LC402
	.section	.rodata.str1.1
.LC403:
	.string	"dv"
.LC404:
	.string	"dv_MV"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_dv, @object
	.size	_ZL9locmap_dv, 32
_ZL9locmap_dv:
	.long	101
	.zero	4
	.quad	.LC403
	.long	1125
	.zero	4
	.quad	.LC404
	.section	.rodata.str1.1
.LC405:
	.string	"de"
.LC406:
	.string	"de_AT"
.LC407:
	.string	"de_CH"
.LC408:
	.string	"de_DE"
.LC409:
	.string	"de_LI"
.LC410:
	.string	"de_LU"
.LC411:
	.string	"de_DE@collation=phonebook"
.LC412:
	.string	"de@collation=phonebook"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_de, @object
	.size	_ZL9locmap_de, 128
_ZL9locmap_de:
	.long	7
	.zero	4
	.quad	.LC405
	.long	3079
	.zero	4
	.quad	.LC406
	.long	2055
	.zero	4
	.quad	.LC407
	.long	1031
	.zero	4
	.quad	.LC408
	.long	5127
	.zero	4
	.quad	.LC409
	.long	4103
	.zero	4
	.quad	.LC410
	.long	66567
	.zero	4
	.quad	.LC411
	.long	66567
	.zero	4
	.quad	.LC412
	.section	.rodata.str1.1
.LC413:
	.string	"da"
.LC414:
	.string	"da_DK"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_da, @object
	.size	_ZL9locmap_da, 32
_ZL9locmap_da:
	.long	6
	.zero	4
	.quad	.LC413
	.long	1030
	.zero	4
	.quad	.LC414
	.section	.rodata.str1.1
.LC415:
	.string	"cy"
.LC416:
	.string	"cy_GB"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_cy, @object
	.size	_ZL9locmap_cy, 32
_ZL9locmap_cy:
	.long	82
	.zero	4
	.quad	.LC415
	.long	1106
	.zero	4
	.quad	.LC416
	.section	.rodata.str1.1
.LC417:
	.string	"cs"
.LC418:
	.string	"cs_CZ"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_cs, @object
	.size	_ZL9locmap_cs, 32
_ZL9locmap_cs:
	.long	5
	.zero	4
	.quad	.LC417
	.long	1029
	.zero	4
	.quad	.LC418
	.section	.rodata.str1.1
.LC419:
	.string	"ckb"
.LC420:
	.string	"ckb_Arab"
.LC421:
	.string	"ckb_Arab_IQ"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_ckb, @object
	.size	_ZL10locmap_ckb, 48
_ZL10locmap_ckb:
	.long	146
	.zero	4
	.quad	.LC419
	.long	31890
	.zero	4
	.quad	.LC420
	.long	1170
	.zero	4
	.quad	.LC421
	.section	.rodata.str1.1
.LC422:
	.string	"chr"
.LC423:
	.string	"chr_Cher"
.LC424:
	.string	"chr_Cher_US"
.LC425:
	.string	"chr_US"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_chr, @object
	.size	_ZL10locmap_chr, 64
_ZL10locmap_chr:
	.long	92
	.zero	4
	.quad	.LC422
	.long	31836
	.zero	4
	.quad	.LC423
	.long	1116
	.zero	4
	.quad	.LC424
	.long	1116
	.zero	4
	.quad	.LC425
	.section	.rodata.str1.1
.LC426:
	.string	"co"
.LC427:
	.string	"co_FR"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_co, @object
	.size	_ZL9locmap_co, 32
_ZL9locmap_co:
	.long	131
	.zero	4
	.quad	.LC426
	.long	1155
	.zero	4
	.quad	.LC427
	.section	.rodata.str1.1
.LC428:
	.string	"ca"
.LC429:
	.string	"ca_ES"
.LC430:
	.string	"ca_ES_VALENCIA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ca, @object
	.size	_ZL9locmap_ca, 48
_ZL9locmap_ca:
	.long	3
	.zero	4
	.quad	.LC428
	.long	1027
	.zero	4
	.quad	.LC429
	.long	2051
	.zero	4
	.quad	.LC430
	.section	.rodata.str1.1
.LC431:
	.string	"br"
.LC432:
	.string	"br_FR"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_br, @object
	.size	_ZL9locmap_br, 32
_ZL9locmap_br:
	.long	126
	.zero	4
	.quad	.LC431
	.long	1150
	.zero	4
	.quad	.LC432
	.section	.rodata.str1.1
.LC433:
	.string	"bo"
.LC434:
	.string	"bo_BT"
.LC435:
	.string	"bo_CN"
.LC436:
	.string	"dz_BT"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_bo, @object
	.size	_ZL9locmap_bo, 64
_ZL9locmap_bo:
	.long	81
	.zero	4
	.quad	.LC433
	.long	2129
	.zero	4
	.quad	.LC434
	.long	1105
	.zero	4
	.quad	.LC435
	.long	3153
	.zero	4
	.quad	.LC436
	.section	.rodata.str1.1
.LC437:
	.string	"bn"
.LC438:
	.string	"bn_BD"
.LC439:
	.string	"bn_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_bn, @object
	.size	_ZL9locmap_bn, 48
_ZL9locmap_bn:
	.long	69
	.zero	4
	.quad	.LC437
	.long	2117
	.zero	4
	.quad	.LC438
	.long	1093
	.zero	4
	.quad	.LC439
	.section	.rodata.str1.1
.LC440:
	.string	"bin"
.LC441:
	.string	"bin_NG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_bin, @object
	.size	_ZL10locmap_bin, 32
_ZL10locmap_bin:
	.long	102
	.zero	4
	.quad	.LC440
	.long	1126
	.zero	4
	.quad	.LC441
	.section	.rodata.str1.1
.LC442:
	.string	"bg"
.LC443:
	.string	"bg_BG"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_bg, @object
	.size	_ZL9locmap_bg, 32
_ZL9locmap_bg:
	.long	2
	.zero	4
	.quad	.LC442
	.long	1026
	.zero	4
	.quad	.LC443
	.section	.rodata.str1.1
.LC444:
	.string	"be"
.LC445:
	.string	"be_BY"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_be, @object
	.size	_ZL9locmap_be, 32
_ZL9locmap_be:
	.long	35
	.zero	4
	.quad	.LC444
	.long	1059
	.zero	4
	.quad	.LC445
	.section	.rodata.str1.1
.LC446:
	.string	"ba"
.LC447:
	.string	"ba_RU"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ba, @object
	.size	_ZL9locmap_ba, 32
_ZL9locmap_ba:
	.long	109
	.zero	4
	.quad	.LC446
	.long	1133
	.zero	4
	.quad	.LC447
	.section	.rodata.str1.1
.LC448:
	.string	"az"
.LC449:
	.string	"az_Cyrl_AZ"
.LC450:
	.string	"az_Cyrl"
.LC451:
	.string	"az_Latn_AZ"
.LC452:
	.string	"az_Latn"
.LC453:
	.string	"az_AZ"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_az, @object
	.size	_ZL9locmap_az, 96
_ZL9locmap_az:
	.long	44
	.zero	4
	.quad	.LC448
	.long	2092
	.zero	4
	.quad	.LC449
	.long	29740
	.zero	4
	.quad	.LC450
	.long	1068
	.zero	4
	.quad	.LC451
	.long	30764
	.zero	4
	.quad	.LC452
	.long	1068
	.zero	4
	.quad	.LC453
	.section	.rodata.str1.1
.LC454:
	.string	"arn"
.LC455:
	.string	"arn_CL"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10locmap_arn, @object
	.size	_ZL10locmap_arn, 32
_ZL10locmap_arn:
	.long	122
	.zero	4
	.quad	.LC454
	.long	1146
	.zero	4
	.quad	.LC455
	.section	.rodata.str1.1
.LC456:
	.string	"am"
.LC457:
	.string	"am_ET"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_am, @object
	.size	_ZL9locmap_am, 32
_ZL9locmap_am:
	.long	94
	.zero	4
	.quad	.LC456
	.long	1118
	.zero	4
	.quad	.LC457
	.section	.rodata.str1.1
.LC458:
	.string	"as"
.LC459:
	.string	"as_IN"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_as, @object
	.size	_ZL9locmap_as, 32
_ZL9locmap_as:
	.long	77
	.zero	4
	.quad	.LC458
	.long	1101
	.zero	4
	.quad	.LC459
	.section	.rodata.str1.1
.LC460:
	.string	"ar"
.LC461:
	.string	"ar_AE"
.LC462:
	.string	"ar_BH"
.LC463:
	.string	"ar_DZ"
.LC464:
	.string	"ar_EG"
.LC465:
	.string	"ar_IQ"
.LC466:
	.string	"ar_JO"
.LC467:
	.string	"ar_KW"
.LC468:
	.string	"ar_LB"
.LC469:
	.string	"ar_LY"
.LC470:
	.string	"ar_MA"
.LC471:
	.string	"ar_MO"
.LC472:
	.string	"ar_OM"
.LC473:
	.string	"ar_QA"
.LC474:
	.string	"ar_SA"
.LC475:
	.string	"ar_SY"
.LC476:
	.string	"ar_TN"
.LC477:
	.string	"ar_YE"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_ar, @object
	.size	_ZL9locmap_ar, 288
_ZL9locmap_ar:
	.long	1
	.zero	4
	.quad	.LC460
	.long	14337
	.zero	4
	.quad	.LC461
	.long	15361
	.zero	4
	.quad	.LC462
	.long	5121
	.zero	4
	.quad	.LC463
	.long	3073
	.zero	4
	.quad	.LC464
	.long	2049
	.zero	4
	.quad	.LC465
	.long	11265
	.zero	4
	.quad	.LC466
	.long	13313
	.zero	4
	.quad	.LC467
	.long	12289
	.zero	4
	.quad	.LC468
	.long	4097
	.zero	4
	.quad	.LC469
	.long	6145
	.zero	4
	.quad	.LC470
	.long	6145
	.zero	4
	.quad	.LC471
	.long	8193
	.zero	4
	.quad	.LC472
	.long	16385
	.zero	4
	.quad	.LC473
	.long	1025
	.zero	4
	.quad	.LC474
	.long	10241
	.zero	4
	.quad	.LC475
	.long	7169
	.zero	4
	.quad	.LC476
	.long	9217
	.zero	4
	.quad	.LC477
	.section	.rodata.str1.1
.LC478:
	.string	"af"
.LC479:
	.string	"af_ZA"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL9locmap_af, @object
	.size	_ZL9locmap_af, 32
_ZL9locmap_af:
	.long	54
	.zero	4
	.quad	.LC478
	.long	1078
	.zero	4
	.quad	.LC479
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
