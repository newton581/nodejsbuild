	.file	"uvector.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UVector17getDynamicClassIDEv
	.type	_ZNK6icu_677UVector17getDynamicClassIDEv, @function
_ZNK6icu_677UVector17getDynamicClassIDEv:
.LFB2068:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_677UVector16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2068:
	.size	_ZNK6icu_677UVector17getDynamicClassIDEv, .-_ZNK6icu_677UVector17getDynamicClassIDEv
	.p2align 4
	.type	_ZN6icu_67L14sortComparatorEPKvS1_S1_, @function
_ZN6icu_67L14sortComparatorEPKvS1_S1_:
.LFB2119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	(%rsi), %rdi
	movq	(%rdx), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*(%r8)
	popq	%rbp
	.cfi_def_cfa 7, 8
	movsbl	%al, %eax
	ret
	.cfi_endproc
.LFE2119:
	.size	_ZN6icu_67L14sortComparatorEPKvS1_S1_, .-_ZN6icu_67L14sortComparatorEPKvS1_S1_
	.p2align 4
	.type	_ZN6icu_67L15sortiComparatorEPKvS1_S1_, @function
_ZN6icu_67L15sortiComparatorEPKvS1_S1_:
.LFB2120:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	movl	$-1, %eax
	cmpl	%ecx, (%rsi)
	jl	.L5
	setne	%al
	movzbl	%al, %eax
.L5:
	ret
	.cfi_endproc
.LFE2120:
	.size	_ZN6icu_67L15sortiComparatorEPKvS1_S1_, .-_ZN6icu_67L15sortiComparatorEPKvS1_S1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVectorD0Ev
	.type	_ZN6icu_677UVectorD0Ev, @function
_ZN6icu_677UVectorD0Ev:
.LFB2088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_677UVectorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	cmpq	$0, 24(%r12)
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	je	.L9
	movl	8(%r12), %eax
	testl	%eax, %eax
	jle	.L9
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%rdi,%rbx,8), %r8
	testq	%r8, %r8
	je	.L10
	movq	%r8, %rdi
	addq	$1, %rbx
	call	*24(%r12)
	movl	8(%r12), %eax
	movq	16(%r12), %rdi
	cmpl	%ebx, %eax
	jg	.L13
.L9:
	movl	$0, 8(%r12)
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	movq	$0, 16(%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L13
	jmp	.L9
	.cfi_endproc
.LFE2088:
	.size	_ZN6icu_677UVectorD0Ev, .-_ZN6icu_677UVectorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVectorD2Ev
	.type	_ZN6icu_677UVectorD2Ev, @function
_ZN6icu_677UVectorD2Ev:
.LFB2086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_677UVectorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	cmpq	$0, 24(%r12)
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	je	.L16
	movl	8(%r12), %eax
	testl	%eax, %eax
	jle	.L16
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%rdi,%rbx,8), %r8
	testq	%r8, %r8
	je	.L17
	movq	%r8, %rdi
	addq	$1, %rbx
	call	*24(%r12)
	movl	8(%r12), %eax
	movq	16(%r12), %rdi
	cmpl	%ebx, %eax
	jg	.L20
.L16:
	movl	$0, 8(%r12)
	call	uprv_free_67@PLT
	popq	%rbx
	movq	%r12, %rdi
	movq	$0, 16(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L20
	jmp	.L16
	.cfi_endproc
.LFE2086:
	.size	_ZN6icu_677UVectorD2Ev, .-_ZN6icu_677UVectorD2Ev
	.globl	_ZN6icu_677UVectorD1Ev
	.set	_ZN6icu_677UVectorD1Ev,_ZN6icu_677UVectorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector16getStaticClassIDEv
	.type	_ZN6icu_677UVector16getStaticClassIDEv, @function
_ZN6icu_677UVector16getStaticClassIDEv:
.LFB2067:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_677UVector16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2067:
	.size	_ZN6icu_677UVector16getStaticClassIDEv, .-_ZN6icu_677UVector16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVectorC2ER10UErrorCode
	.type	_ZN6icu_677UVectorC2ER10UErrorCode, @function
_ZN6icu_677UVectorC2ER10UErrorCode:
.LFB2073:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_677UVectorE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	(%rsi), %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, 24(%rdi)
	testl	%eax, %eax
	jg	.L27
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$64, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L30
	movl	$8, 12(%rbx)
.L23:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
.L30:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$7, (%r12)
	jmp	.L23
	.cfi_endproc
.LFE2073:
	.size	_ZN6icu_677UVectorC2ER10UErrorCode, .-_ZN6icu_677UVectorC2ER10UErrorCode
	.globl	_ZN6icu_677UVectorC1ER10UErrorCode
	.set	_ZN6icu_677UVectorC1ER10UErrorCode,_ZN6icu_677UVectorC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVectorC2EiR10UErrorCode
	.type	_ZN6icu_677UVectorC2EiR10UErrorCode, @function
_ZN6icu_677UVectorC2EiR10UErrorCode:
.LFB2076:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_677UVectorE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	(%rdx), %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, 24(%rdi)
	testl	%eax, %eax
	jg	.L37
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$268435454, %eax
	ja	.L35
	movslq	%esi, %rdi
	salq	$3, %rdi
.L33:
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L40
	movl	%r12d, 12(%rbx)
.L31:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	$64, %edi
	movl	$8, %r12d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
.L40:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, 0(%r13)
	jmp	.L31
	.cfi_endproc
.LFE2076:
	.size	_ZN6icu_677UVectorC2EiR10UErrorCode, .-_ZN6icu_677UVectorC2EiR10UErrorCode
	.globl	_ZN6icu_677UVectorC1EiR10UErrorCode
	.set	_ZN6icu_677UVectorC1EiR10UErrorCode,_ZN6icu_677UVectorC2EiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_ER10UErrorCode
	.type	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_ER10UErrorCode, @function
_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_ER10UErrorCode:
.LFB2079:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_677UVectorE(%rip), %rax
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	(%rcx), %eax
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 16(%rdi)
	movups	%xmm0, 24(%rdi)
	testl	%eax, %eax
	jg	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$64, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L48
	movl	$8, 12(%rbx)
.L41:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
.L48:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$7, (%r12)
	jmp	.L41
	.cfi_endproc
.LFE2079:
	.size	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_ER10UErrorCode, .-_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_ER10UErrorCode
	.globl	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode
	.set	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode,_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode
	.type	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode, @function
_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode:
.LFB2082:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_677UVectorE(%rip), %rax
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movl	(%r8), %eax
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 16(%rdi)
	movups	%xmm0, 24(%rdi)
	testl	%eax, %eax
	jg	.L55
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rcx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$268435454, %eax
	ja	.L53
	movslq	%ecx, %rdi
	salq	$3, %rdi
.L51:
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L58
	movl	%r12d, 12(%rbx)
.L49:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movl	$64, %edi
	movl	$8, %r12d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
.L58:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, 0(%r13)
	jmp	.L49
	.cfi_endproc
.LFE2082:
	.size	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode, .-_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode
	.globl	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode
	.set	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_EiR10UErrorCode,_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector5_initEiR10UErrorCode
	.type	_ZN6icu_677UVector5_initEiR10UErrorCode, @function
_ZN6icu_677UVector5_initEiR10UErrorCode:
.LFB2084:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L65
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-1(%rsi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	cmpl	$268435454, %eax
	ja	.L63
	movslq	%esi, %rdi
	salq	$3, %rdi
.L61:
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%r12)
	testq	%rax, %rax
	je	.L68
	movl	%ebx, 12(%r12)
.L59:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$64, %edi
	movl	$8, %ebx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
.L68:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$7, 0(%r13)
	jmp	.L59
	.cfi_endproc
.LFE2084:
	.size	_ZN6icu_677UVector5_initEiR10UErrorCode, .-_ZN6icu_677UVector5_initEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVectoreqERKS0_
	.type	_ZN6icu_677UVectoreqERKS0_, @function
_ZN6icu_677UVectoreqERKS0_:
.LFB2090:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edx
	cmpl	8(%rsi), %edx
	je	.L83
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L73
	testl	%edx, %edx
	jle	.L73
	movq	%rsi, %r13
	xorl	%ebx, %ebx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L84:
	addq	$1, %rbx
	cmpl	%ebx, 8(%r12)
	jle	.L73
	movq	32(%r12), %rax
.L74:
	movq	16(%r13), %rdx
	movq	(%rdx,%rbx,8), %rsi
	movq	16(%r12), %rdx
	movq	(%rdx,%rbx,8), %rdi
	call	*%rax
	testb	%al, %al
	jne	.L84
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2090:
	.size	_ZN6icu_677UVectoreqERKS0_, .-_ZN6icu_677UVectoreqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector10addElementEPvR10UErrorCode
	.type	_ZN6icu_677UVector10addElementEPvR10UErrorCode, @function
_ZN6icu_677UVector10addElementEPvR10UErrorCode:
.LFB2091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movslq	8(%rdi), %rcx
	movl	%ecx, %esi
	addl	$1, %esi
	js	.L90
	movl	12(%rdi), %r12d
	movq	%rdi, %rbx
	cmpl	%r12d, %esi
	jle	.L93
	cmpl	$1073741823, %r12d
	jg	.L90
	addl	%r12d, %r12d
	cmpl	%r12d, %esi
	cmovge	%esi, %r12d
	cmpl	$268435455, %r12d
	jle	.L94
.L90:
	movl	$1, (%rdx)
.L85:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	16(%rdi), %rax
.L89:
	movl	%esi, 8(%rbx)
	movq	%r13, (%rax,%rcx,8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movslq	%r12d, %rsi
	movq	%rdx, -40(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-40(%rbp), %rdx
	testq	%rax, %rax
	je	.L95
	movslq	8(%rbx), %rcx
	movq	%rax, 16(%rbx)
	movl	%r12d, 12(%rbx)
	leal	1(%rcx), %esi
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$7, (%rdx)
	jmp	.L85
	.cfi_endproc
.LFE2091:
	.size	_ZN6icu_677UVector10addElementEPvR10UErrorCode, .-_ZN6icu_677UVector10addElementEPvR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector10addElementEiR10UErrorCode
	.type	_ZN6icu_677UVector10addElementEiR10UErrorCode, @function
_ZN6icu_677UVector10addElementEiR10UErrorCode:
.LFB2092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movslq	8(%rdi), %rcx
	movl	%ecx, %esi
	addl	$1, %esi
	js	.L101
	movl	12(%rdi), %r12d
	movq	%rdi, %rbx
	cmpl	%r12d, %esi
	jle	.L104
	cmpl	$1073741823, %r12d
	jg	.L101
	addl	%r12d, %r12d
	cmpl	%r12d, %esi
	cmovge	%esi, %r12d
	cmpl	$268435455, %r12d
	jle	.L105
.L101:
	movl	$1, (%rdx)
.L96:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	16(%rdi), %rax
.L100:
	leaq	(%rax,%rcx,8), %rax
	movq	$0, (%rax)
	movl	%r13d, (%rax)
	movl	%esi, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movslq	%r12d, %rsi
	movq	%rdx, -40(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-40(%rbp), %rdx
	testq	%rax, %rax
	je	.L106
	movslq	8(%rbx), %rcx
	movq	%rax, 16(%rbx)
	movl	%r12d, 12(%rbx)
	leal	1(%rcx), %esi
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$7, (%rdx)
	jmp	.L96
	.cfi_endproc
.LFE2092:
	.size	_ZN6icu_677UVector10addElementEiR10UErrorCode, .-_ZN6icu_677UVector10addElementEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector12setElementAtEPvi
	.type	_ZN6icu_677UVector12setElementAtEPvi, @function
_ZN6icu_677UVector12setElementAtEPvi:
.LFB2093:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	js	.L117
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	cmpl	%edx, 8(%rdi)
	jle	.L107
	movq	16(%rdi), %rax
	movslq	%edx, %rdx
	leaq	0(,%rdx,8), %rbx
	addq	%rbx, %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L109
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L109
	movq	%rsi, -24(%rbp)
	call	*%rdx
	movq	16(%r12), %rax
	movq	-24(%rbp), %rsi
	addq	%rbx, %rax
.L109:
	movq	%rsi, (%rax)
.L107:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2093:
	.size	_ZN6icu_677UVector12setElementAtEPvi, .-_ZN6icu_677UVector12setElementAtEPvi
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector12setElementAtEii
	.type	_ZN6icu_677UVector12setElementAtEii, @function
_ZN6icu_677UVector12setElementAtEii:
.LFB2094:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	js	.L130
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	cmpl	%edx, 8(%rdi)
	jle	.L120
	movq	16(%rdi), %rax
	movslq	%edx, %rdx
	leaq	0(,%rdx,8), %rbx
	addq	%rbx, %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L122
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L122
	movl	%esi, -20(%rbp)
	call	*%rdx
	movq	16(%r12), %rax
	movl	-20(%rbp), %esi
	addq	%rbx, %rax
.L122:
	movq	$0, (%rax)
	movl	%esi, (%rax)
.L120:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2094:
	.size	_ZN6icu_677UVector12setElementAtEii, .-_ZN6icu_677UVector12setElementAtEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode
	.type	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode, @function
_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode:
.LFB2095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	subq	$24, %rsp
	testl	%ebx, %ebx
	js	.L133
	movslq	8(%rdi), %rax
	movq	%rdi, %r12
	cmpl	%ebx, %eax
	jge	.L143
.L133:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movl	12(%rdi), %edx
	leal	1(%rax), %r14d
	movq	%rsi, %r13
	cmpl	%edx, %r14d
	jle	.L144
	cmpl	$1073741823, %edx
	jg	.L138
	addl	%edx, %edx
	cmpl	%edx, %r14d
	cmovl	%edx, %r14d
	cmpl	$268435455, %r14d
	jg	.L138
	movq	16(%rdi), %rdi
	movslq	%r14d, %rsi
	movq	%rcx, -56(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L145
	movq	%rax, 16(%r12)
	movslq	8(%r12), %rax
	movl	%r14d, 12(%r12)
	leal	1(%rax), %r14d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L144:
	movq	16(%rdi), %r15
.L136:
	cmpl	%eax, %ebx
	jge	.L141
	movl	%eax, %edx
	salq	$3, %rax
	subl	%ebx, %edx
	subl	$1, %edx
	movq	%rdx, %rdi
	leaq	8(,%rdx,8), %rdx
	negq	%rdi
	salq	$3, %rdi
	leaq	-8(%rax,%rdi), %rsi
	addq	%rax, %rdi
	addq	%r15, %rsi
	addq	%r15, %rdi
	call	memmove@PLT
.L141:
	movq	%r13, (%r15,%rbx,8)
	movl	%r14d, 8(%r12)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L138:
	movl	$1, (%rcx)
	jmp	.L133
.L145:
	movl	$7, (%rcx)
	jmp	.L133
	.cfi_endproc
.LFE2095:
	.size	_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode, .-_ZN6icu_677UVector15insertElementAtEPviR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector15insertElementAtEiiR10UErrorCode
	.type	_ZN6icu_677UVector15insertElementAtEiiR10UErrorCode, @function
_ZN6icu_677UVector15insertElementAtEiiR10UErrorCode:
.LFB2096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	subq	$24, %rsp
	testl	%ebx, %ebx
	js	.L146
	movslq	8(%rdi), %rax
	movq	%rdi, %r12
	cmpl	%ebx, %eax
	jge	.L156
.L146:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movl	12(%rdi), %edx
	leal	1(%rax), %r14d
	movl	%esi, %r13d
	cmpl	%edx, %r14d
	jle	.L157
	cmpl	$1073741823, %edx
	jg	.L151
	addl	%edx, %edx
	cmpl	%edx, %r14d
	cmovl	%edx, %r14d
	cmpl	$268435455, %r14d
	jg	.L151
	movq	16(%rdi), %rdi
	movslq	%r14d, %rsi
	movq	%rcx, -56(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L158
	movq	%rax, 16(%r12)
	movslq	8(%r12), %rax
	movl	%r14d, 12(%r12)
	leal	1(%rax), %r14d
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L157:
	movq	16(%rdi), %r15
.L149:
	cmpl	%eax, %ebx
	jge	.L154
	movl	%eax, %edx
	salq	$3, %rax
	subl	%ebx, %edx
	subl	$1, %edx
	movq	%rdx, %rdi
	leaq	8(,%rdx,8), %rdx
	negq	%rdi
	salq	$3, %rdi
	leaq	-8(%rax,%rdi), %rsi
	addq	%rax, %rdi
	addq	%r15, %rsi
	addq	%r15, %rdi
	call	memmove@PLT
.L154:
	leaq	(%r15,%rbx,8), %rax
	movq	$0, (%rax)
	movl	%r13d, (%rax)
	movl	%r14d, 8(%r12)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$1, (%rcx)
	jmp	.L146
.L158:
	movl	$7, (%rcx)
	jmp	.L146
	.cfi_endproc
.LFE2096:
	.size	_ZN6icu_677UVector15insertElementAtEiiR10UErrorCode, .-_ZN6icu_677UVector15insertElementAtEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UVector9elementAtEi
	.type	_ZNK6icu_677UVector9elementAtEi, @function
_ZNK6icu_677UVector9elementAtEi:
.LFB2097:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L159
	cmpl	%esi, 8(%rdi)
	jle	.L159
	movq	16(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
.L159:
	ret
	.cfi_endproc
.LFE2097:
	.size	_ZNK6icu_677UVector9elementAtEi, .-_ZNK6icu_677UVector9elementAtEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UVector10elementAtiEi
	.type	_ZNK6icu_677UVector10elementAtiEi, @function
_ZNK6icu_677UVector10elementAtiEi:
.LFB2098:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L163
	cmpl	%esi, 8(%rdi)
	jle	.L163
	movq	16(%rdi), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,8), %eax
.L163:
	ret
	.cfi_endproc
.LFE2098:
	.size	_ZNK6icu_677UVector10elementAtiEi, .-_ZNK6icu_677UVector10elementAtiEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UVector11containsAllERKS0_
	.type	_ZNK6icu_677UVector11containsAllERKS0_, @function
_ZNK6icu_677UVector11containsAllERKS0_:
.LFB2099:
	.cfi_startproc
	endbr64
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jle	.L178
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	.p2align 4,,10
	.p2align 3
.L174:
	movq	16(%r15), %rax
	movl	8(%r12), %esi
	leaq	(%rax,%r14,8), %rax
	movq	(%rax), %rbx
	movl	(%rax), %edx
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.L169
	testl	%esi, %esi
	jle	.L170
	movl	%edx, %ecx
	xorl	%r13d, %r13d
	movq	%rcx, -56(%rbp)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L181:
	addq	$1, %r13
	cmpl	%r13d, 8(%r12)
	jle	.L170
	movq	32(%r12), %rax
.L172:
	movabsq	$-4294967296, %rcx
	movq	16(%r12), %rdx
	andq	%rcx, %rbx
	orq	-56(%rbp), %rbx
	movq	(%rdx,%r13,8), %rsi
	movq	%rbx, %rdi
	call	*%rax
	testb	%al, %al
	je	.L181
.L171:
	addq	$1, %r14
	cmpl	%r14d, 8(%r15)
	jg	.L174
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L170
	movq	16(%r12), %rax
	subl	$1, %esi
	leaq	8(%rax,%rsi,8), %rsi
	.p2align 4,,10
	.p2align 3
.L173:
	cmpl	(%rax), %edx
	je	.L171
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L173
.L170:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L178:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2099:
	.size	_ZNK6icu_677UVector11containsAllERKS0_, .-_ZNK6icu_677UVector11containsAllERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UVector12containsNoneERKS0_
	.type	_ZNK6icu_677UVector12containsNoneERKS0_, @function
_ZNK6icu_677UVector12containsNoneERKS0_:
.LFB2100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %edi
	testl	%edi, %edi
	jle	.L183
	movq	%rsi, %r15
	movl	8(%r12), %esi
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L190:
	movq	16(%r15), %rax
	leaq	(%rax,%r14,8), %rax
	movq	(%rax), %rbx
	movl	(%rax), %edx
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.L184
	testl	%esi, %esi
	jle	.L185
	movl	%edx, %edi
	xorl	%r13d, %r13d
	movq	%rdi, -56(%rbp)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L195:
	movl	8(%r12), %esi
	addq	$1, %r13
	cmpl	%r13d, %esi
	jle	.L194
	movq	32(%r12), %rax
.L188:
	movabsq	$-4294967296, %rcx
	movq	16(%r12), %rdx
	andq	%rcx, %rbx
	orq	-56(%rbp), %rbx
	movq	(%rdx,%r13,8), %rsi
	movq	%rbx, %rdi
	call	*%rax
	testb	%al, %al
	je	.L195
.L186:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movl	8(%r15), %edi
.L185:
	addq	$1, %r14
	cmpl	%r14d, %edi
	jg	.L190
.L183:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L185
	movq	16(%r12), %rax
	leal	-1(%rsi), %r9d
	leaq	8(%rax,%r9,8), %r9
	.p2align 4,,10
	.p2align 3
.L189:
	cmpl	(%rax), %edx
	je	.L186
	addq	$8, %rax
	cmpq	%rax, %r9
	jne	.L189
	addq	$1, %r14
	cmpl	%r14d, %edi
	jg	.L190
	jmp	.L183
	.cfi_endproc
.LFE2100:
	.size	_ZNK6icu_677UVector12containsNoneERKS0_, .-_ZNK6icu_677UVector12containsNoneERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector9removeAllERKS0_
	.type	_ZN6icu_677UVector9removeAllERKS0_, @function
_ZN6icu_677UVector9removeAllERKS0_:
.LFB2101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %esi
	testl	%esi, %esi
	jle	.L212
	movb	$0, -73(%rbp)
	movq	%rdi, %rbx
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L210:
	movq	16(%r12), %rax
	movl	8(%rbx), %ecx
	leaq	(%rax,%r13,8), %rax
	movq	(%rax), %rdx
	movl	(%rax), %r15d
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L198
	testl	%ecx, %ecx
	jle	.L209
	xorl	%ecx, %ecx
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L220:
	movq	-72(%rbp), %rcx
	movq	-56(%rbp), %rdx
	addq	$1, %rcx
	cmpl	%ecx, 8(%rbx)
	jle	.L219
	movq	32(%rbx), %rax
.L202:
	leaq	0(,%rcx,8), %r9
	movq	%rcx, -72(%rbp)
	movl	%ecx, %r14d
	movabsq	$-4294967296, %rdi
	andq	%rdi, %rdx
	movq	%r9, %rsi
	addq	16(%rbx), %rsi
	movq	%r9, -64(%rbp)
	orq	%r15, %rdx
	movq	(%rsi), %rsi
	movq	%rdx, -56(%rbp)
	movq	%rdx, %rdi
	call	*%rax
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L220
	movl	8(%rbx), %ecx
.L203:
	cmpl	%ecx, %r14d
	jge	.L218
	movq	16(%rbx), %rdi
	leal	-1(%rcx), %r8d
	movq	(%rdi,%r9), %r15
	cmpl	%r8d, %r14d
	jge	.L208
	movslq	%r14d, %rax
	subl	$2, %ecx
	movl	%r8d, -56(%rbp)
	salq	$3, %rax
	subl	%r14d, %ecx
	leaq	8(%rdi,%rax), %rsi
	leaq	8(,%rcx,8), %rdx
	addq	%rax, %rdi
	call	memmove@PLT
	movl	-56(%rbp), %r8d
.L208:
	movl	%r8d, 8(%rbx)
	testq	%r15, %r15
	je	.L218
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L218
	movq	%r15, %rdi
	call	*%rax
.L218:
	movb	$1, -73(%rbp)
	movl	8(%r12), %esi
.L209:
	addq	$1, %r13
	cmpl	%r13d, %esi
	jg	.L210
.L196:
	movzbl	-73(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movl	8(%r12), %esi
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L198:
	testl	%ecx, %ecx
	jle	.L209
	movq	16(%rbx), %r11
	leal	-1(%rcx), %edi
	xorl	%eax, %eax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%rdx, %rax
.L204:
	movl	%eax, %r14d
	leaq	0(,%rax,8), %r9
	cmpl	(%r11,%rax,8), %r15d
	je	.L203
	leaq	1(%rax), %rdx
	cmpq	%rdi, %rax
	jne	.L221
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L212:
	movb	$0, -73(%rbp)
	jmp	.L196
	.cfi_endproc
.LFE2101:
	.size	_ZN6icu_677UVector9removeAllERKS0_, .-_ZN6icu_677UVector9removeAllERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector9retainAllERKS0_
	.type	_ZN6icu_677UVector9retainAllERKS0_, @function
_ZN6icu_677UVector9retainAllERKS0_:
.LFB2102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	%rdi, -64(%rbp)
	movl	%eax, %r14d
	movl	%eax, -56(%rbp)
	subl	$1, %r14d
	js	.L236
	movb	$0, -65(%rbp)
	movslq	%r14d, %r9
	movq	%rsi, %r13
	leaq	0(,%r9,8), %r15
	.p2align 4,,10
	.p2align 3
.L234:
	movq	-64(%rbp), %rdi
	movl	8(%r13), %edx
	movq	16(%rdi), %rax
	addq	%r15, %rax
	movq	(%rax), %rbx
	movl	(%rax), %r12d
	movq	32(%r13), %rax
	testq	%rax, %rax
	je	.L224
	testl	%edx, %edx
	jle	.L225
	xorl	%edx, %edx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L243:
	movq	-56(%rbp), %rdx
	addq	$1, %rdx
	cmpl	%edx, 8(%r13)
	jle	.L225
	movq	32(%r13), %rax
.L227:
	movabsq	$-4294967296, %rcx
	movq	16(%r13), %rsi
	movq	%rdx, -56(%rbp)
	andq	%rcx, %rbx
	orq	%r12, %rbx
	movq	(%rsi,%rdx,8), %rsi
	movq	%rbx, %rdi
	call	*%rax
	testb	%al, %al
	je	.L243
.L233:
	subl	$1, %r14d
	subq	$8, %r15
	cmpl	$-1, %r14d
	jne	.L234
.L222:
	movzbl	-65(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	testl	%edx, %edx
	jle	.L225
	movq	16(%r13), %rax
	subl	$1, %edx
	leaq	8(%rax,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L228:
	cmpl	(%rax), %r12d
	je	.L233
	addq	$8, %rax
	cmpq	%rdx, %rax
	jne	.L228
.L225:
	movq	-64(%rbp), %rax
	movl	8(%rax), %eax
	cmpl	%r14d, %eax
	jle	.L242
	movq	-64(%rbp), %rcx
	leal	-1(%rax), %ebx
	movq	16(%rcx), %rdx
	leaq	(%rdx,%r15), %rdi
	movq	(%rdi), %r12
	cmpl	%ebx, %r14d
	jge	.L232
	subl	$2, %eax
	leaq	8(%rdx,%r15), %rsi
	subl	%r14d, %eax
	leaq	8(,%rax,8), %r10
	movq	%r10, %rdx
	call	memmove@PLT
.L232:
	movq	-64(%rbp), %rax
	movl	%ebx, 8(%rax)
	testq	%r12, %r12
	je	.L242
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L242
	movq	%r12, %rdi
	call	*%rax
.L242:
	subl	$1, %r14d
	movb	$1, -65(%rbp)
	subq	$8, %r15
	cmpl	$-1, %r14d
	jne	.L234
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L236:
	movb	$0, -65(%rbp)
	jmp	.L222
	.cfi_endproc
.LFE2102:
	.size	_ZN6icu_677UVector9retainAllERKS0_, .-_ZN6icu_677UVector9retainAllERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector15removeElementAtEi
	.type	_ZN6icu_677UVector15removeElementAtEi, @function
_ZN6icu_677UVector15removeElementAtEi:
.LFB2103:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L256
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	jge	.L244
	movq	16(%rdi), %rcx
	movslq	%esi, %rdx
	leal	-1(%rax), %r12d
	salq	$3, %rdx
	leaq	(%rcx,%rdx), %rdi
	movq	(%rdi), %r13
	cmpl	%r12d, %esi
	jge	.L248
	subl	%esi, %eax
	leaq	8(%rcx,%rdx), %rsi
	subl	$2, %eax
	leaq	8(,%rax,8), %r8
	movq	%r8, %rdx
	call	memmove@PLT
.L248:
	movl	%r12d, 8(%rbx)
	testq	%r13, %r13
	je	.L244
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L244
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE2103:
	.size	_ZN6icu_677UVector15removeElementAtEi, .-_ZN6icu_677UVector15removeElementAtEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector13removeElementEPv
	.type	_ZN6icu_677UVector13removeElementEPv, @function
_ZN6icu_677UVector13removeElementEPv:
.LFB2104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rax
	movl	8(%rdi), %edx
	testq	%rax, %rax
	je	.L260
	testl	%edx, %edx
	jle	.L261
	xorl	%r12d, %r12d
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L277:
	addq	$1, %r12
	cmpl	%r12d, 8(%rbx)
	jle	.L261
	movq	32(%rbx), %rax
.L263:
	movq	16(%rbx), %rdx
	movl	%r12d, %r14d
	leaq	0(,%r12,8), %r15
	movq	%r13, %rdi
	movq	(%rdx,%r12,8), %rsi
	call	*%rax
	testb	%al, %al
	je	.L277
	movl	8(%rbx), %edx
.L264:
	cmpl	%r14d, %edx
	jle	.L276
	movq	16(%rbx), %rdi
	leal	-1(%rdx), %r12d
	movq	(%rdi,%r15), %r13
	cmpl	%r14d, %r12d
	jle	.L269
	movslq	%r14d, %rax
	subl	$2, %edx
	salq	$3, %rax
	subl	%r14d, %edx
	leaq	8(%rdi,%rax), %rsi
	leaq	8(,%rdx,8), %rdx
	addq	%rax, %rdi
	call	memmove@PLT
.L269:
	movl	%r12d, 8(%rbx)
	testq	%r13, %r13
	je	.L276
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L276
	movq	%r13, %rdi
	call	*%rax
.L276:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	testl	%edx, %edx
	jle	.L261
	movq	16(%rdi), %rdi
	leal	-1(%rdx), %esi
	xorl	%eax, %eax
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%rcx, %rax
.L265:
	movl	%eax, %r14d
	leaq	0(,%rax,8), %r15
	cmpq	(%rdi,%rax,8), %r13
	je	.L264
	leaq	1(%rax), %rcx
	cmpq	%rax, %rsi
	jne	.L278
.L261:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2104:
	.size	_ZN6icu_677UVector13removeElementEPv, .-_ZN6icu_677UVector13removeElementEPv
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector17removeAllElementsEv
	.type	_ZN6icu_677UVector17removeAllElementsEv, @function
_ZN6icu_677UVector17removeAllElementsEv:
.LFB2105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	$0, 24(%rdi)
	je	.L281
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.L281
	xorl	%ebx, %ebx
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L288:
	call	*24(%r12)
	movl	8(%r12), %eax
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L281
.L287:
	movq	16(%r12), %rdx
.L284:
	movq	(%rdx,%rbx,8), %rdi
	testq	%rdi, %rdi
	jne	.L288
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L284
.L281:
	movl	$0, 8(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2105:
	.size	_ZN6icu_677UVector17removeAllElementsEv, .-_ZN6icu_677UVector17removeAllElementsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UVector6equalsERKS0_
	.type	_ZNK6icu_677UVector6equalsERKS0_, @function
_ZNK6icu_677UVector6equalsERKS0_:
.LFB2106:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	cmpl	8(%rsi), %edx
	jne	.L301
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.L304
	testl	%edx, %edx
	jle	.L299
	xorl	%ebx, %ebx
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L305:
	addq	$1, %rbx
	cmpl	%ebx, 8(%r12)
	jle	.L299
	movq	32(%r12), %rax
.L295:
	movq	16(%r13), %rdx
	leaq	(%rdx,%rbx,8), %rdi
	movq	16(%r12), %rdx
	movq	(%rdx,%rbx,8), %rsi
	call	*%rax
	testb	%al, %al
	jne	.L305
.L289:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	testl	%edx, %edx
	jle	.L299
	movq	16(%rdi), %rsi
	movq	16(%r13), %rcx
	leal	-1(%rdx), %edi
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L306:
	leaq	1(%rax), %rdx
	cmpq	%rax, %rdi
	je	.L299
	movq	%rdx, %rax
.L292:
	movq	(%rcx,%rax,8), %rbx
	cmpq	%rbx, (%rsi,%rax,8)
	je	.L306
	xorl	%eax, %eax
	jmp	.L289
	.cfi_endproc
.LFE2106:
	.size	_ZNK6icu_677UVector6equalsERKS0_, .-_ZNK6icu_677UVector6equalsERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UVector7indexOfEPvi
	.type	_ZNK6icu_677UVector7indexOfEPvi, @function
_ZNK6icu_677UVector7indexOfEPvi:
.LFB2107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	32(%rdi), %rax
	movq	%rdi, %rbx
	movl	8(%rdi), %ecx
	testq	%rax, %rax
	je	.L308
	cmpl	%ecx, %edx
	jge	.L309
	movslq	%edx, %r14
	salq	$3, %r14
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L314:
	addl	$1, %r12d
	addq	$8, %r14
	cmpl	%r12d, 8(%rbx)
	jle	.L309
	movq	32(%rbx), %rax
.L310:
	movq	16(%rbx), %rdx
	movq	%r13, %rdi
	movq	(%rdx,%r14), %rsi
	call	*%rax
	testb	%al, %al
	je	.L314
.L307:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	cmpl	%ecx, %edx
	jge	.L309
	movq	16(%rdi), %rax
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L312:
	movl	%edx, %r12d
	cmpq	(%rax,%rdx,8), %r13
	je	.L307
	addq	$1, %rdx
	cmpl	%edx, %ecx
	jg	.L312
.L309:
	movl	$-1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2107:
	.size	_ZNK6icu_677UVector7indexOfEPvi, .-_ZNK6icu_677UVector7indexOfEPvi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UVector7indexOfEii
	.type	_ZNK6icu_677UVector7indexOfEii, @function
_ZNK6icu_677UVector7indexOfEii:
.LFB2108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	32(%rdi), %rax
	movq	%rdi, %rbx
	movl	8(%rdi), %ecx
	testq	%rax, %rax
	je	.L316
	cmpl	%ecx, %edx
	jge	.L317
	movslq	%edx, %r13
	movl	%esi, %r14d
	movl	%edx, %r12d
	salq	$3, %r13
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L322:
	addl	$1, %r12d
	addq	$8, %r13
	cmpl	%r12d, 8(%rbx)
	jle	.L317
	movq	32(%rbx), %rax
.L318:
	movq	16(%rbx), %rdx
	movq	%r14, %rdi
	movq	(%rdx,%r13), %rsi
	call	*%rax
	testb	%al, %al
	je	.L322
.L315:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	cmpl	%ecx, %edx
	jge	.L317
	movq	16(%rdi), %rdi
	movslq	%edx, %rax
	.p2align 4,,10
	.p2align 3
.L320:
	movl	%eax, %r12d
	cmpl	(%rdi,%rax,8), %esi
	je	.L315
	addq	$1, %rax
	cmpl	%eax, %ecx
	jg	.L320
.L317:
	movl	$-1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2108:
	.size	_ZNK6icu_677UVector7indexOfEii, .-_ZNK6icu_677UVector7indexOfEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UVector7indexOfE8UElementia
	.type	_ZNK6icu_677UVector7indexOfE8UElementia, @function
_ZNK6icu_677UVector7indexOfE8UElementia:
.LFB2109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	32(%rdi), %rax
	movq	%rdi, %rbx
	movl	8(%rdi), %esi
	testq	%rax, %rax
	je	.L324
	cmpl	%esi, %edx
	jge	.L325
	movslq	%edx, %r13
	movl	%edx, %r12d
	salq	$3, %r13
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L332:
	addl	$1, %r12d
	addq	$8, %r13
	cmpl	%r12d, 8(%rbx)
	jle	.L325
	movq	32(%rbx), %rax
.L326:
	movq	16(%rbx), %rdx
	movq	%r14, %rdi
	movq	(%rdx,%r13), %rsi
	call	*%rax
	testb	%al, %al
	je	.L332
.L323:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movl	$-1, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	cmpl	%esi, %edx
	jge	.L325
	andl	$1, %ecx
	movq	16(%rdi), %rdi
	movl	%r14d, %r8d
	movslq	%edx, %rax
	jne	.L330
	movl	%eax, %r12d
	cmpl	(%rdi,%rax,8), %r8d
	je	.L323
	.p2align 4,,10
	.p2align 3
.L333:
	addq	$1, %rax
	cmpl	%eax, %esi
	jle	.L325
	movl	%eax, %r12d
	cmpl	(%rdi,%rax,8), %r8d
	jne	.L333
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L334:
	addq	$1, %rax
	cmpl	%eax, %esi
	jle	.L325
.L330:
	movl	%eax, %r12d
	cmpq	(%rdi,%rax,8), %r14
	jne	.L334
	jmp	.L323
	.cfi_endproc
.LFE2109:
	.size	_ZNK6icu_677UVector7indexOfE8UElementia, .-_ZNK6icu_677UVector7indexOfE8UElementia
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector14ensureCapacityEiR10UErrorCode
	.type	_ZN6icu_677UVector14ensureCapacityEiR10UErrorCode, @function
_ZN6icu_677UVector14ensureCapacityEiR10UErrorCode:
.LFB2110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testl	%esi, %esi
	js	.L338
	movl	12(%rdi), %ebx
	movq	%rdi, %r12
	movl	$1, %r13d
	cmpl	%esi, %ebx
	jge	.L335
	cmpl	$1073741823, %ebx
	jg	.L338
	addl	%ebx, %ebx
	cmpl	%ebx, %esi
	cmovge	%esi, %ebx
	cmpl	$268435455, %ebx
	jle	.L342
.L338:
	movl	$1, (%rdx)
	xorl	%r13d, %r13d
.L335:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	movq	16(%rdi), %rdi
	movslq	%ebx, %rsi
	movq	%rdx, -40(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-40(%rbp), %rdx
	testq	%rax, %rax
	je	.L343
	movq	%rax, 16(%r12)
	movl	%ebx, 12(%r12)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L343:
	movl	$7, (%rdx)
	xorl	%r13d, %r13d
	jmp	.L335
	.cfi_endproc
.LFE2110:
	.size	_ZN6icu_677UVector14ensureCapacityEiR10UErrorCode, .-_ZN6icu_677UVector14ensureCapacityEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector7setSizeEiR10UErrorCode
	.type	_ZN6icu_677UVector7setSizeEiR10UErrorCode, @function
_ZN6icu_677UVector7setSizeEiR10UErrorCode:
.LFB2111:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L368
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movl	8(%rdi), %eax
	cmpl	%esi, %eax
	jl	.L371
	leal	-1(%rax), %r13d
	cmpl	%r13d, %esi
	jg	.L354
	movslq	%r13d, %r15
	leal	-1(%rsi), %esi
	movl	%esi, -56(%rbp)
	salq	$3, %r15
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L372:
	movl	8(%r14), %eax
.L363:
	cmpl	%r13d, %eax
	jle	.L358
	movq	16(%r14), %rdx
	leal	-1(%rax), %ecx
	leaq	(%rdx,%r15), %rdi
	movq	(%rdi), %r12
	cmpl	%r13d, %ecx
	jle	.L362
	subl	$2, %eax
	leaq	8(%rdx,%r15), %rsi
	movl	%ecx, -60(%rbp)
	subl	%r13d, %eax
	leaq	8(,%rax,8), %r9
	movq	%r9, %rdx
	call	memmove@PLT
	movl	-60(%rbp), %ecx
.L362:
	movl	%ecx, 8(%r14)
	testq	%r12, %r12
	je	.L358
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L358
	movq	%r12, %rdi
	call	*%rax
.L358:
	subl	$1, %r13d
	subq	$8, %r15
	cmpl	-56(%rbp), %r13d
	jne	.L372
.L354:
	movl	%ebx, 8(%r14)
.L344:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	12(%rdi), %ecx
	cmpl	%ecx, %esi
	jle	.L347
	cmpl	$1073741823, %ecx
	jg	.L350
	addl	%ecx, %ecx
	cmpl	%ecx, %esi
	cmovge	%esi, %ecx
	movl	%ecx, %r12d
	cmpl	$268435455, %ecx
	jle	.L373
.L350:
	movl	$1, (%rdx)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L347:
	xorl	%ecx, %ecx
.L353:
	leal	-1(%rbx), %esi
	movslq	%eax, %r8
	movq	16(%r14), %rdi
	movl	%esi, %r10d
	subl	%eax, %r10d
	leaq	(%rdi,%r8,8), %rdx
	movq	%r10, %rax
	addq	%r8, %rax
	leaq	8(%rdi,%rax,8), %rax
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%rcx, (%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %rax
	jne	.L355
	jmp	.L354
.L373:
	movslq	%ecx, %rsi
	movq	16(%rdi), %rdi
	movq	%rdx, -56(%rbp)
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	je	.L374
	movq	%rax, 16(%r14)
	movl	8(%r14), %eax
	xorl	%ecx, %ecx
	movl	%r12d, 12(%r14)
	cmpl	%eax, %ebx
	jg	.L353
	jmp	.L354
.L374:
	movl	$7, (%rdx)
	jmp	.L344
	.cfi_endproc
.LFE2111:
	.size	_ZN6icu_677UVector7setSizeEiR10UErrorCode, .-_ZN6icu_677UVector7setSizeEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector6assignERKS0_PFvP8UElementS4_ER10UErrorCode
	.type	_ZN6icu_677UVector6assignERKS0_PFvP8UElementS4_ER10UErrorCode, @function
_ZN6icu_677UVector6assignERKS0_PFvP8UElementS4_ER10UErrorCode:
.LFB2089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rsi), %esi
	testl	%esi, %esi
	js	.L379
	movl	12(%rdi), %r13d
	movq	%rdi, %rbx
	movq	%rdx, %r14
	cmpl	%r13d, %esi
	jle	.L378
	cmpl	$1073741823, %r13d
	jg	.L379
	addl	%r13d, %r13d
	cmpl	%r13d, %esi
	cmovge	%esi, %r13d
	cmpl	$268435455, %r13d
	jle	.L392
.L379:
	movl	$1, (%r15)
.L375:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movslq	%r13d, %rsi
	movq	16(%rdi), %rdi
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L393
	movq	%rax, 16(%rbx)
	movl	8(%r12), %esi
	movl	%r13d, 12(%rbx)
.L378:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN6icu_677UVector7setSizeEiR10UErrorCode
	movl	(%r15), %edx
	testl	%edx, %edx
	jg	.L375
	movl	8(%r12), %eax
	testl	%eax, %eax
	jle	.L375
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L384:
	movq	16(%rbx), %rdi
	leaq	0(,%r15,8), %r13
	addq	%r13, %rdi
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L383
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L383
	movq	%r8, %rdi
	call	*%rax
	movq	16(%rbx), %rdi
	addq	%r13, %rdi
.L383:
	movq	16(%r12), %rsi
	addq	$1, %r15
	addq	%r13, %rsi
	call	*%r14
	cmpl	%r15d, 8(%r12)
	jg	.L384
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movl	$7, (%r15)
	jmp	.L375
	.cfi_endproc
.LFE2089:
	.size	_ZN6icu_677UVector6assignERKS0_PFvP8UElementS4_ER10UErrorCode, .-_ZN6icu_677UVector6assignERKS0_PFvP8UElementS4_ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_677UVector7toArrayEPPv
	.type	_ZNK6icu_677UVector7toArrayEPPv, @function
_ZNK6icu_677UVector7toArrayEPPv:
.LFB2112:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edx
	movq	%rsi, %rax
	testl	%edx, %edx
	jle	.L395
	subl	$1, %edx
	leaq	8(,%rdx,8), %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L396:
	movq	16(%rdi), %rcx
	movq	(%rcx,%rdx), %rcx
	movq	%rcx, (%rax,%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	jne	.L396
.L395:
	ret
	.cfi_endproc
.LFE2112:
	.size	_ZNK6icu_677UVector7toArrayEPPv, .-_ZNK6icu_677UVector7toArrayEPPv
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector10setDeleterEPFvPvE
	.type	_ZN6icu_677UVector10setDeleterEPFvPvE, @function
_ZN6icu_677UVector10setDeleterEPFvPvE:
.LFB2113:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	%rsi, 24(%rdi)
	ret
	.cfi_endproc
.LFE2113:
	.size	_ZN6icu_677UVector10setDeleterEPFvPvE, .-_ZN6icu_677UVector10setDeleterEPFvPvE
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector11setComparerEPFa8UElementS1_E
	.type	_ZN6icu_677UVector11setComparerEPFa8UElementS1_E, @function
_ZN6icu_677UVector11setComparerEPFa8UElementS1_E:
.LFB2114:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE2114:
	.size	_ZN6icu_677UVector11setComparerEPFa8UElementS1_E, .-_ZN6icu_677UVector11setComparerEPFa8UElementS1_E
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector15orphanElementAtEi
	.type	_ZN6icu_677UVector15orphanElementAtEi, @function
_ZN6icu_677UVector15orphanElementAtEi:
.LFB2115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testl	%esi, %esi
	js	.L400
	movl	8(%rdi), %eax
	movq	%rdi, %rbx
	cmpl	%esi, %eax
	jle	.L400
	movq	16(%rdi), %rcx
	movslq	%esi, %rdx
	leal	-1(%rax), %r13d
	salq	$3, %rdx
	leaq	(%rcx,%rdx), %rdi
	movq	(%rdi), %r12
	cmpl	%r13d, %esi
	jge	.L402
	subl	$2, %eax
	subl	%esi, %eax
	leaq	8(%rcx,%rdx), %rsi
	leaq	8(,%rax,8), %r8
	movq	%r8, %rdx
	call	memmove@PLT
.L402:
	movl	%r13d, 8(%rbx)
.L400:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2115:
	.size	_ZN6icu_677UVector15orphanElementAtEi, .-_ZN6icu_677UVector15orphanElementAtEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector12sortedInsertE8UElementPFaS1_S1_ER10UErrorCode
	.type	_ZN6icu_677UVector12sortedInsertE8UElementPFaS1_S1_ER10UErrorCode, @function
_ZN6icu_677UVector12sortedInsertE8UElementPFaS1_S1_ER10UErrorCode:
.LFB2118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movslq	8(%rdi), %r15
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	testl	%r15d, %r15d
	je	.L419
	.p2align 4,,10
	.p2align 3
.L408:
	leal	(%r14,%r15), %eax
	movq	%r12, %rsi
	movl	%eax, %ebx
	shrl	$31, %ebx
	addl	%eax, %ebx
	movq	16(%r13), %rax
	sarl	%ebx
	movslq	%ebx, %rdx
	movq	(%rax,%rdx,8), %rdi
	movq	-56(%rbp), %rax
	call	*%rax
	testb	%al, %al
	jle	.L421
	cmpl	%ebx, %r14d
	je	.L409
	movl	%ebx, %r15d
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L421:
	leal	1(%rbx), %r14d
	cmpl	%r15d, %r14d
	jne	.L408
.L409:
	movslq	8(%r13), %r15
	movl	%r15d, %ebx
	addl	$1, %ebx
	js	.L415
.L407:
	movl	12(%r13), %eax
	cmpl	%eax, %ebx
	jg	.L413
	movq	16(%r13), %r8
.L414:
	cmpl	%r15d, %r14d
	jge	.L418
	movl	%r15d, %eax
	salq	$3, %r15
	movq	%r8, -56(%rbp)
	subl	%r14d, %eax
	leal	-1(%rax), %edx
	movq	%rdx, %rax
	leaq	8(,%rdx,8), %rdx
	negq	%rax
	salq	$3, %rax
	leaq	-8(%r15,%rax), %rsi
	addq	%rax, %r15
	addq	%r8, %rsi
	leaq	(%r8,%r15), %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L418:
	movslq	%r14d, %rcx
	movq	%r12, (%r8,%rcx,8)
	movl	%ebx, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	cmpl	$1073741823, %eax
	jg	.L415
	addl	%eax, %eax
	cmpl	%eax, %ebx
	cmovl	%eax, %ebx
	cmpl	$268435455, %ebx
	jle	.L422
.L415:
	movq	-64(%rbp), %rax
	movl	$1, (%rax)
.L406:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movq	16(%r13), %rdi
	movslq	%ebx, %rsi
	salq	$3, %rsi
	call	uprv_realloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L423
	movslq	8(%r13), %r15
	movl	%ebx, 12(%r13)
	movq	%rax, 16(%r13)
	leal	1(%r15), %ebx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L419:
	movl	$1, %ebx
	jmp	.L407
.L423:
	movq	-64(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L406
	.cfi_endproc
.LFE2118:
	.size	_ZN6icu_677UVector12sortedInsertE8UElementPFaS1_S1_ER10UErrorCode, .-_ZN6icu_677UVector12sortedInsertE8UElementPFaS1_S1_ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector12sortedInsertEPvPFa8UElementS2_ER10UErrorCode
	.type	_ZN6icu_677UVector12sortedInsertEPvPFa8UElementS2_ER10UErrorCode, @function
_ZN6icu_677UVector12sortedInsertEPvPFa8UElementS2_ER10UErrorCode:
.LFB2116:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_677UVector12sortedInsertE8UElementPFaS1_S1_ER10UErrorCode
	.cfi_endproc
.LFE2116:
	.size	_ZN6icu_677UVector12sortedInsertEPvPFa8UElementS2_ER10UErrorCode, .-_ZN6icu_677UVector12sortedInsertEPvPFa8UElementS2_ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector12sortedInsertEiPFa8UElementS1_ER10UErrorCode
	.type	_ZN6icu_677UVector12sortedInsertEiPFa8UElementS1_ER10UErrorCode, @function
_ZN6icu_677UVector12sortedInsertEiPFa8UElementS1_ER10UErrorCode:
.LFB2117:
	.cfi_startproc
	endbr64
	movl	%esi, %esi
	jmp	_ZN6icu_677UVector12sortedInsertE8UElementPFaS1_S1_ER10UErrorCode
	.cfi_endproc
.LFE2117:
	.size	_ZN6icu_677UVector12sortedInsertEiPFa8UElementS1_ER10UErrorCode, .-_ZN6icu_677UVector12sortedInsertEiPFa8UElementS1_ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector5sortiER10UErrorCode
	.type	_ZN6icu_677UVector5sortiER10UErrorCode, @function
_ZN6icu_677UVector5sortiER10UErrorCode:
.LFB2121:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jle	.L432
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	8(%rdi), %r10d
	movl	$8, %edx
	xorl	%r9d, %r9d
	movq	16(%rdi), %rdi
	xorl	%r8d, %r8d
	leaq	_ZN6icu_67L15sortiComparatorEPKvS1_S1_(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%rsi
	movl	%r10d, %esi
	call	uprv_sortArray_67@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2121:
	.size	_ZN6icu_677UVector5sortiER10UErrorCode, .-_ZN6icu_677UVector5sortiER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector4sortEPFa8UElementS1_ER10UErrorCode
	.type	_ZN6icu_677UVector4sortEPFa8UElementS1_ER10UErrorCode, @function
_ZN6icu_677UVector4sortEPFa8UElementS1_ER10UErrorCode:
.LFB2122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	(%rdx), %ecx
	movq	%rsi, -8(%rbp)
	testl	%ecx, %ecx
	jle	.L436
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	subq	$8, %rsp
	movl	8(%rdi), %esi
	movq	16(%rdi), %rdi
	leaq	-8(%rbp), %r8
	pushq	%rdx
	xorl	%r9d, %r9d
	movl	$8, %edx
	leaq	_ZN6icu_67L14sortComparatorEPKvS1_S1_(%rip), %rcx
	call	uprv_sortArray_67@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2122:
	.size	_ZN6icu_677UVector4sortEPFa8UElementS1_ER10UErrorCode, .-_ZN6icu_677UVector4sortEPFa8UElementS1_ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_677UVector19sortWithUComparatorEPFiPKvS2_S2_ES2_R10UErrorCode
	.type	_ZN6icu_677UVector19sortWithUComparatorEPFiPKvS2_S2_ES2_R10UErrorCode, @function
_ZN6icu_677UVector19sortWithUComparatorEPFiPKvS2_S2_ES2_R10UErrorCode:
.LFB2123:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L443
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	8(%rdi), %r10d
	movq	%rdx, %r8
	movl	$1, %r9d
	movq	16(%rdi), %rdi
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%rcx
	movq	%rsi, %rcx
	movl	%r10d, %esi
	call	uprv_sortArray_67@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2123:
	.size	_ZN6icu_677UVector19sortWithUComparatorEPFiPKvS2_S2_ES2_R10UErrorCode, .-_ZN6icu_677UVector19sortWithUComparatorEPFiPKvS2_S2_ES2_R10UErrorCode
	.weak	_ZTSN6icu_677UVectorE
	.section	.rodata._ZTSN6icu_677UVectorE,"aG",@progbits,_ZTSN6icu_677UVectorE,comdat
	.align 16
	.type	_ZTSN6icu_677UVectorE, @object
	.size	_ZTSN6icu_677UVectorE, 18
_ZTSN6icu_677UVectorE:
	.string	"N6icu_677UVectorE"
	.weak	_ZTIN6icu_677UVectorE
	.section	.data.rel.ro._ZTIN6icu_677UVectorE,"awG",@progbits,_ZTIN6icu_677UVectorE,comdat
	.align 8
	.type	_ZTIN6icu_677UVectorE, @object
	.size	_ZTIN6icu_677UVectorE, 24
_ZTIN6icu_677UVectorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_677UVectorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_677UVectorE
	.section	.data.rel.ro.local._ZTVN6icu_677UVectorE,"awG",@progbits,_ZTVN6icu_677UVectorE,comdat
	.align 8
	.type	_ZTVN6icu_677UVectorE, @object
	.size	_ZTVN6icu_677UVectorE, 40
_ZTVN6icu_677UVectorE:
	.quad	0
	.quad	_ZTIN6icu_677UVectorE
	.quad	_ZN6icu_677UVectorD1Ev
	.quad	_ZN6icu_677UVectorD0Ev
	.quad	_ZNK6icu_677UVector17getDynamicClassIDEv
	.local	_ZZN6icu_677UVector16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_677UVector16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
