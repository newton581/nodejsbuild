	.file	"messagepattern.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePatternD2Ev
	.type	_ZN6icu_6714MessagePatternD2Ev, @function
_ZN6icu_6714MessagePatternD2Ev:
.LFB2349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714MessagePatternE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	80(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2
	cmpb	$0, 12(%r13)
	jne	.L14
.L3:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L2:
	movq	104(%r12), %r13
	testq	%r13, %r13
	je	.L4
	cmpb	$0, 12(%r13)
	jne	.L15
.L5:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L4:
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L14:
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L3
	.cfi_endproc
.LFE2349:
	.size	_ZN6icu_6714MessagePatternD2Ev, .-_ZN6icu_6714MessagePatternD2Ev
	.globl	_ZN6icu_6714MessagePatternD1Ev
	.set	_ZN6icu_6714MessagePatternD1Ev,_ZN6icu_6714MessagePatternD2Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0, @function
_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0:
.LFB2926:
	.cfi_startproc
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jg	.L26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	80(%rdi), %r8
	movslq	96(%rdi), %rbx
	cmpl	8(%r8), %ebx
	jl	.L18
	leal	(%rbx,%rbx), %r9d
	movl	%ecx, -60(%rbp)
	movq	%r8, -56(%rbp)
	testl	%r9d, %r9d
	jle	.L19
	movslq	%r9d, %rdi
	movl	%r9d, -72(%rbp)
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L19
	movq	-56(%rbp), %r8
	testl	%ebx, %ebx
	movl	-72(%rbp), %r9d
	movl	-60(%rbp), %ecx
	movq	(%r8), %rsi
	jg	.L29
.L20:
	cmpb	$0, 12(%r8)
	jne	.L30
.L21:
	movb	$1, 12(%r8)
	movslq	96(%r12), %rbx
	movq	%r10, (%r8)
	movl	%r9d, 8(%r8)
	movq	80(%r12), %r8
.L18:
	leal	1(%rbx), %eax
	salq	$4, %rbx
	addq	(%r8), %rbx
	movl	%eax, 96(%r12)
	movl	$1, %eax
	movl	%r15d, (%rbx)
	movl	%r14d, 4(%rbx)
	movw	%ax, 8(%rbx)
	movw	%cx, 10(%rbx)
	movl	$0, 12(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	cmpl	%r9d, 8(%r8)
	movl	%r9d, %eax
	cmovle	8(%r8), %eax
	movl	%ebx, %edx
	movq	%r10, %rdi
	movl	%ecx, -72(%rbp)
	cmpl	%ebx, %eax
	movl	%r9d, -60(%rbp)
	cmovle	%eax, %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	call	memcpy@PLT
	movq	-56(%rbp), %r8
	movl	-72(%rbp), %ecx
	movl	-60(%rbp), %r9d
	movq	%rax, %r10
	movq	(%r8), %rsi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rsi, %rdi
	movl	%ecx, -64(%rbp)
	movq	%r10, -72(%rbp)
	movl	%r9d, -60(%rbp)
	movq	%r8, -56(%rbp)
	call	uprv_free_67@PLT
	movl	-64(%rbp), %ecx
	movq	-72(%rbp), %r10
	movl	-60(%rbp), %r9d
	movq	-56(%rbp), %r8
	jmp	.L21
	.cfi_endproc
.LFE2926:
	.size	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0, .-_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePatternD0Ev
	.type	_ZN6icu_6714MessagePatternD0Ev, @function
_ZN6icu_6714MessagePatternD0Ev:
.LFB2351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714MessagePatternE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	80(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L32
	cmpb	$0, 12(%r13)
	jne	.L43
.L33:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L32:
	movq	104(%r12), %r13
	testq	%r13, %r13
	je	.L34
	cmpb	$0, 12(%r13)
	jne	.L44
.L35:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L34:
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L43:
	movq	0(%r13), %rdi
	call	uprv_free_67@PLT
	jmp	.L33
	.cfi_endproc
.LFE2351:
	.size	_ZN6icu_6714MessagePatternD0Ev, .-_ZN6icu_6714MessagePatternD0Ev
	.align 2
	.p2align 4
	.type	_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode.part.0, @function
_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode.part.0:
.LFB2918:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	104(%rdi), %r12
	movl	120(%rdi), %r14d
	movl	%edx, -60(%rbp)
	movsd	%xmm0, -56(%rbp)
	testq	%r12, %r12
	je	.L66
	cmpl	8(%r12), %r14d
	jl	.L49
	leal	(%r14,%r14), %r8d
	testl	%r8d, %r8d
	jle	.L65
	movslq	%r8d, %rdi
	movl	%r8d, -64(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L65
	testl	%r14d, %r14d
	movq	(%r12), %rsi
	movl	-64(%rbp), %r8d
	jg	.L67
	cmpb	$0, 12(%r12)
	jne	.L68
.L52:
	movq	%r9, (%r12)
	movl	%r8d, 8(%r12)
	movb	$1, 12(%r12)
.L49:
	cmpl	$32767, %r14d
	jg	.L69
	movq	104(%rbx), %rax
	movq	(%rax), %rdx
.L48:
	movslq	120(%rbx), %rax
	movsd	-56(%rbp), %xmm1
	leal	1(%rax), %esi
	movl	%esi, 120(%rbx)
	movsd	%xmm1, (%rdx,%rax,8)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L45
	movq	80(%rbx), %r8
	movslq	96(%rbx), %r12
	cmpl	8(%r8), %r12d
	jl	.L55
	leal	(%r12,%r12), %r9d
	movq	%r8, -56(%rbp)
	testl	%r9d, %r9d
	jle	.L65
	movslq	%r9d, %rdi
	movl	%r9d, -64(%rbp)
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L65
	movq	-56(%rbp), %r8
	testl	%r12d, %r12d
	movl	-64(%rbp), %r9d
	movq	(%r8), %rsi
	jle	.L56
	cmpl	%r9d, 8(%r8)
	movl	%r9d, %eax
	cmovle	8(%r8), %eax
	movl	%r12d, %edx
	movq	%r10, %rdi
	cmpl	%r12d, %eax
	cmovle	%eax, %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	call	memcpy@PLT
	movq	-56(%rbp), %r8
	movl	-64(%rbp), %r9d
	movq	%rax, %r10
	movq	(%r8), %rsi
.L56:
	cmpb	$0, 12(%r8)
	jne	.L70
.L57:
	movb	$1, 12(%r8)
	movslq	96(%rbx), %r12
	movq	%r10, (%r8)
	movl	%r9d, 8(%r8)
	movq	80(%rbx), %r8
.L55:
	leal	1(%r12), %eax
	salq	$4, %r12
	addq	(%r8), %r12
	movl	%eax, 96(%rbx)
	movzwl	-60(%rbp), %eax
	movl	$13, (%r12)
	movl	%r15d, 4(%r12)
	movw	%ax, 8(%r12)
	movw	%r14w, 10(%r12)
	movl	$0, 12(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L47:
	.cfi_restore_state
	movq	$0, 104(%rbx)
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$7, 0(%r13)
.L45:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L47
	leaq	16(%rax), %rdx
	movl	$8, 8(%rax)
	movq	%rdx, (%rax)
	movb	$0, 12(%rax)
	movq	%rax, 104(%rbx)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	%r8d, 8(%r12)
	movl	%r8d, %eax
	cmovle	8(%r12), %eax
	movq	%r9, %rdi
	cmpl	%r14d, %eax
	cmovg	%r14d, %eax
	movslq	%eax, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%r12)
	movq	(%r12), %rsi
	movl	-64(%rbp), %r8d
	movq	%rax, %r9
	je	.L52
.L68:
	movq	%rsi, %rdi
	movq	%r9, -72(%rbp)
	movl	%r8d, -64(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %r9
	movl	-64(%rbp), %r8d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$8, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	%rsi, %rdi
	movq	%r10, -72(%rbp)
	movl	%r9d, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %r10
	movl	-64(%rbp), %r9d
	movq	-56(%rbp), %r8
	jmp	.L57
	.cfi_endproc
.LFE2918:
	.size	_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode.part.0, .-_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePatternC2ER10UErrorCode
	.type	_ZN6icu_6714MessagePatternC2ER10UErrorCode, @function
_ZN6icu_6714MessagePatternC2ER10UErrorCode:
.LFB2313:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714MessagePatternE(%rip), %rax
	movl	(%rsi), %ecx
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 16(%rdi)
	movl	$2, %eax
	movl	$0, 8(%rdi)
	movw	%ax, 24(%rdi)
	movl	$0, 96(%rdi)
	movl	$0, 120(%rdi)
	movw	%dx, 124(%rdi)
	movb	$0, 126(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 104(%rdi)
	testl	%ecx, %ecx
	jle	.L78
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$528, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L73
	leaq	16(%rax), %rdx
	movl	$32, 8(%rax)
	movq	%rdx, (%rax)
	movb	$0, 12(%rax)
	movq	%rax, 80(%rbx)
	movq	%rdx, 88(%rbx)
.L71:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	movq	$0, 80(%rbx)
	movl	$7, (%r12)
	jmp	.L71
	.cfi_endproc
.LFE2313:
	.size	_ZN6icu_6714MessagePatternC2ER10UErrorCode, .-_ZN6icu_6714MessagePatternC2ER10UErrorCode
	.globl	_ZN6icu_6714MessagePatternC1ER10UErrorCode
	.set	_ZN6icu_6714MessagePatternC1ER10UErrorCode,_ZN6icu_6714MessagePatternC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePatternC2E29UMessagePatternApostropheModeR10UErrorCode
	.type	_ZN6icu_6714MessagePatternC2E29UMessagePatternApostropheModeR10UErrorCode, @function
_ZN6icu_6714MessagePatternC2E29UMessagePatternApostropheModeR10UErrorCode:
.LFB2316:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714MessagePatternE(%rip), %rax
	movl	%esi, 8(%rdi)
	movl	(%rdx), %esi
	pxor	%xmm0, %xmm0
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%ecx, %ecx
	movq	%rax, 16(%rdi)
	movl	$2, %eax
	movw	%ax, 24(%rdi)
	movl	$0, 96(%rdi)
	movl	$0, 120(%rdi)
	movw	%cx, 124(%rdi)
	movb	$0, 126(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 104(%rdi)
	testl	%esi, %esi
	jle	.L86
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$528, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L81
	leaq	16(%rax), %rdx
	movl	$32, 8(%rax)
	movq	%rdx, (%rax)
	movb	$0, 12(%rax)
	movq	%rax, 80(%rbx)
	movq	%rdx, 88(%rbx)
.L79:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	movq	$0, 80(%rbx)
	movl	$7, (%r12)
	jmp	.L79
	.cfi_endproc
.LFE2316:
	.size	_ZN6icu_6714MessagePatternC2E29UMessagePatternApostropheModeR10UErrorCode, .-_ZN6icu_6714MessagePatternC2E29UMessagePatternApostropheModeR10UErrorCode
	.globl	_ZN6icu_6714MessagePatternC1E29UMessagePatternApostropheModeR10UErrorCode
	.set	_ZN6icu_6714MessagePatternC1E29UMessagePatternApostropheModeR10UErrorCode,_ZN6icu_6714MessagePatternC2E29UMessagePatternApostropheModeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern4initER10UErrorCode
	.type	_ZN6icu_6714MessagePattern4initER10UErrorCode, @function
_ZN6icu_6714MessagePattern4initER10UErrorCode:
.LFB2321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L92
.L87:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	$528, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L89
	leaq	16(%rax), %rdx
	movb	$0, 12(%rax)
	movl	$1, %r13d
	movq	%rdx, (%rax)
	movl	$32, 8(%rax)
	movq	%rax, 80(%r12)
	movl	%r13d, %eax
	movq	%rdx, 88(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	movq	$0, 80(%r12)
	movl	$7, (%rbx)
	jmp	.L87
	.cfi_endproc
.LFE2321:
	.size	_ZN6icu_6714MessagePattern4initER10UErrorCode, .-_ZN6icu_6714MessagePattern4initER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePatternC2ERKS0_
	.type	_ZN6icu_6714MessagePatternC2ERKS0_, @function
_ZN6icu_6714MessagePatternC2ERKS0_:
.LFB2332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714MessagePatternE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	$16, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$16, %rdi
	subq	$24, %rsp
	movq	%rax, -16(%rdi)
	movl	-8(%rsi), %eax
	movl	%eax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movzbl	126(%r12), %eax
	pxor	%xmm0, %xmm0
	movzwl	124(%r12), %edx
	movl	$0, 96(%rbx)
	movl	$528, %edi
	movl	$0, 120(%rbx)
	movw	%dx, 124(%rbx)
	movb	%al, 126(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 104(%rbx)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L94
	movq	%rax, %r13
	leaq	16(%rax), %rax
	movb	$0, 12(%r13)
	movl	96(%r12), %r14d
	movq	%rax, 0(%r13)
	movl	$32, 8(%r13)
	movq	%r13, 80(%rbx)
	movq	%rax, 88(%rbx)
	testl	%r14d, %r14d
	jg	.L119
.L95:
	movl	120(%r12), %r14d
	testl	%r14d, %r14d
	jle	.L93
	movq	104(%rbx), %r13
	testq	%r13, %r13
	je	.L120
.L118:
	movslq	%r14d, %r15
	movq	104(%r12), %rdx
	salq	$3, %r15
	cmpl	%r14d, 8(%r13)
	jge	.L121
	movq	%r15, %rdi
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L108
	cmpb	$0, 12(%r13)
	movq	-56(%rbp), %rdx
	jne	.L122
.L106:
	movq	%r8, 0(%r13)
	movl	%r14d, 8(%r13)
	movb	$1, 12(%r13)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L119:
	movslq	%r14d, %rdx
	movq	80(%r12), %rcx
	salq	$4, %rdx
	cmpl	$32, %r14d
	jle	.L123
	movq	%rdx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L108
	cmpb	$0, 12(%r13)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jne	.L124
.L99:
	movq	%r15, 0(%r13)
	movl	%r14d, 8(%r13)
	movb	$1, 12(%r13)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L102
	movb	$0, 12(%r13)
	movl	120(%r12), %r14d
	leaq	16(%rax), %rax
	movq	%rax, 0(%r13)
	movl	$8, 8(%r13)
	movq	%r13, 104(%rbx)
	movq	%rax, 112(%rbx)
	testl	%r14d, %r14d
	jg	.L118
.L103:
	movq	0(%r13), %rax
	movl	%r14d, 120(%rbx)
	movq	%rax, 112(%rbx)
.L93:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	0(%r13), %r15
.L97:
	movq	(%rcx), %rsi
	movq	%r15, %rdi
	call	memcpy@PLT
	movq	80(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 88(%rbx)
	movl	96(%r12), %eax
	movl	%eax, 96(%rbx)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L121:
	movq	0(%r13), %r8
.L105:
	movq	(%rdx), %rsi
	movq	%r8, %rdi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	104(%rbx), %r13
	movl	120(%r12), %r14d
	jmp	.L103
.L102:
	movq	$0, 104(%rbx)
.L108:
	movzwl	24(%rbx), %edx
	movl	$0, 96(%rbx)
	movl	$0, 120(%rbx)
	movl	%edx, %eax
	movb	$0, 126(%rbx)
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 24(%rbx)
	xorl	%eax, %eax
	movw	%ax, 124(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	0(%r13), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %r8
	jmp	.L106
.L124:
	movq	0(%r13), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	jmp	.L99
.L94:
	movq	$0, 80(%rbx)
	jmp	.L108
	.cfi_endproc
.LFE2332:
	.size	_ZN6icu_6714MessagePatternC2ERKS0_, .-_ZN6icu_6714MessagePatternC2ERKS0_
	.globl	_ZN6icu_6714MessagePatternC1ERKS0_
	.set	_ZN6icu_6714MessagePatternC1ERKS0_,_ZN6icu_6714MessagePatternC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePatternaSERKS0_
	.type	_ZN6icu_6714MessagePatternaSERKS0_, @function
_ZN6icu_6714MessagePatternaSERKS0_:
.LFB2334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpq	%rsi, %rdi
	je	.L148
	movl	8(%rsi), %eax
	movq	%rsi, %rbx
	leaq	16(%rdi), %rdi
	leaq	16(%rsi), %rsi
	movl	%eax, -8(%rdi)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movzbl	124(%rbx), %eax
	movq	80(%r12), %r13
	movb	%al, 124(%r12)
	movzbl	125(%rbx), %eax
	movb	%al, 125(%r12)
	movzbl	126(%rbx), %eax
	movq	$0, 88(%r12)
	movb	%al, 126(%r12)
	movl	$0, 96(%r12)
	movq	$0, 112(%r12)
	movl	$0, 120(%r12)
	testq	%r13, %r13
	je	.L155
.L128:
	movl	96(%rbx), %r14d
	testl	%r14d, %r14d
	jg	.L156
.L130:
	movl	120(%rbx), %r14d
	testl	%r14d, %r14d
	jle	.L148
	movq	104(%r12), %r13
	testq	%r13, %r13
	je	.L157
.L154:
	movslq	%r14d, %r15
	movq	104(%rbx), %rdx
	salq	$3, %r15
	cmpl	%r14d, 8(%r13)
	jge	.L158
	movq	%r15, %rdi
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L143
	cmpb	$0, 12(%r13)
	movq	-56(%rbp), %rdx
	jne	.L159
.L141:
	movq	%r8, 0(%r13)
	movl	%r14d, 8(%r13)
	movb	$1, 12(%r13)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L157:
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L137
	movb	$0, 12(%r13)
	movl	120(%rbx), %r14d
	leaq	16(%rax), %rax
	movq	%rax, 0(%r13)
	movl	$8, 8(%r13)
	movq	%r13, 104(%r12)
	movq	%rax, 112(%r12)
	testl	%r14d, %r14d
	jg	.L154
.L138:
	movq	0(%r13), %rax
	movl	%r14d, 120(%r12)
	movq	%rax, 112(%r12)
.L148:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movslq	%r14d, %rdx
	movq	80(%rbx), %rcx
	salq	$4, %rdx
	cmpl	8(%r13), %r14d
	jle	.L160
	movq	%rdx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L143
	cmpb	$0, 12(%r13)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jne	.L161
.L134:
	movq	%r15, 0(%r13)
	movl	%r14d, 8(%r13)
	movb	$1, 12(%r13)
	jmp	.L132
.L137:
	movq	$0, 104(%r12)
.L143:
	movzwl	24(%r12), %edx
	movb	$0, 126(%r12)
	movl	$0, 96(%r12)
	movl	$0, 120(%r12)
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 24(%r12)
	xorl	%eax, %eax
	movw	%ax, 124(%r12)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L160:
	movq	0(%r13), %r15
.L132:
	movq	(%rcx), %rsi
	movq	%r15, %rdi
	call	memcpy@PLT
	movq	80(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, 88(%r12)
	movl	96(%rbx), %eax
	movl	%eax, 96(%r12)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L155:
	movl	$528, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L129
	leaq	16(%rax), %rax
	movl	$32, 8(%r13)
	movq	%rax, 0(%r13)
	movb	$0, 12(%r13)
	movq	%r13, 80(%r12)
	movq	%rax, 88(%r12)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L158:
	movq	0(%r13), %r8
.L140:
	movq	(%rdx), %rsi
	movq	%r8, %rdi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	104(%r12), %r13
	movl	120(%rbx), %r14d
	jmp	.L138
.L159:
	movq	0(%r13), %rdi
	movq	%rax, -64(%rbp)
	call	uprv_free_67@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rdx
	jmp	.L141
.L161:
	movq	0(%r13), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	jmp	.L134
.L129:
	movq	$0, 80(%r12)
	jmp	.L143
	.cfi_endproc
.LFE2334:
	.size	_ZN6icu_6714MessagePatternaSERKS0_, .-_ZN6icu_6714MessagePatternaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern11copyStorageERKS0_R10UErrorCode
	.type	_ZN6icu_6714MessagePattern11copyStorageERKS0_R10UErrorCode, @function
_ZN6icu_6714MessagePattern11copyStorageERKS0_R10UErrorCode:
.LFB2335:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L191
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	80(%rdi), %r13
	movq	$0, 88(%rdi)
	movl	$0, 96(%rdi)
	movq	$0, 112(%rdi)
	movl	$0, 120(%rdi)
	testq	%r13, %r13
	je	.L197
	movl	96(%rsi), %r15d
	testl	%r15d, %r15d
	jg	.L194
.L167:
	movl	120(%r14), %r15d
	movl	$1, %eax
	testl	%r15d, %r15d
	jle	.L162
	movq	104(%rbx), %r13
	testq	%r13, %r13
	je	.L198
.L173:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L196
	testl	%r15d, %r15d
	jg	.L199
.L175:
	testl	%eax, %eax
	jg	.L196
	movq	104(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 112(%rbx)
	movl	120(%r14), %eax
	movl	%eax, 120(%rbx)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	movl	$528, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L165
	movb	$0, 12(%r13)
	movl	96(%r14), %r15d
	leaq	16(%rax), %rax
	movq	%rax, 0(%r13)
	movl	$32, 8(%r13)
	movq	%r13, 80(%rbx)
	movq	%rax, 88(%rbx)
	testl	%r15d, %r15d
	jle	.L167
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L194
.L196:
	xorl	%eax, %eax
.L162:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L174
	leaq	16(%rax), %rax
	movb	$0, 12(%r13)
	movl	120(%r14), %r15d
	movq	%rax, 0(%r13)
	movl	$8, 8(%r13)
	movq	%r13, 104(%rbx)
	movq	%rax, 112(%rbx)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L194:
	movslq	%r15d, %rdx
	movq	80(%r14), %rcx
	salq	$4, %rdx
	cmpl	%r15d, 8(%r13)
	jge	.L200
	movq	%rdx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L195
	cmpb	$0, 12(%r13)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jne	.L201
.L171:
	movq	%r8, 0(%r13)
	movl	%r15d, 8(%r13)
	movb	$1, 12(%r13)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L199:
	movslq	%r15d, %rdx
	movq	104(%r14), %rcx
	salq	$3, %rdx
	cmpl	%r15d, 8(%r13)
	jge	.L202
	movq	%rdx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L195
	cmpb	$0, 12(%r13)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rcx
	jne	.L203
.L179:
	movq	%r8, 0(%r13)
	movl	%r15d, 8(%r13)
	movb	$1, 12(%r13)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L200:
	movq	0(%r13), %r8
.L169:
	movq	(%rcx), %rsi
	movq	%r8, %rdi
	call	memcpy@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L196
	movq	80(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 88(%rbx)
	movl	96(%r14), %eax
	movl	%eax, 96(%rbx)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L202:
	movq	0(%r13), %r8
.L177:
	movq	(%rcx), %rsi
	movq	%r8, %rdi
	call	memcpy@PLT
	movl	(%r12), %eax
	jmp	.L175
.L165:
	movq	$0, 80(%rbx)
.L195:
	movl	$7, (%r12)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L203:
	movq	0(%r13), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%rax, -72(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	jmp	.L179
.L201:
	movq	0(%r13), %rdi
	movq	%rdx, -72(%rbp)
	movq	%rax, -56(%rbp)
	call	uprv_free_67@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	jmp	.L171
.L174:
	movq	$0, 104(%rbx)
	xorl	%eax, %eax
	movl	$7, (%r12)
	jmp	.L162
	.cfi_endproc
.LFE2335:
	.size	_ZN6icu_6714MessagePattern11copyStorageERKS0_R10UErrorCode, .-_ZN6icu_6714MessagePattern11copyStorageERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern5clearEv
	.type	_ZN6icu_6714MessagePattern5clearEv, @function
_ZN6icu_6714MessagePattern5clearEv:
.LFB2356:
	.cfi_startproc
	endbr64
	movzwl	24(%rdi), %edx
	movl	$0, 96(%rdi)
	movl	$0, 120(%rdi)
	movl	%edx, %eax
	movb	$0, 126(%rdi)
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movw	%ax, 24(%rdi)
	xorl	%eax, %eax
	movw	%ax, 124(%rdi)
	ret
	.cfi_endproc
.LFE2356:
	.size	_ZN6icu_6714MessagePattern5clearEv, .-_ZN6icu_6714MessagePattern5clearEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714MessagePatterneqERKS0_
	.type	_ZNK6icu_6714MessagePatterneqERKS0_, @function
_ZNK6icu_6714MessagePatterneqERKS0_:
.LFB2357:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L219
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movl	8(%rsi), %eax
	cmpl	%eax, 8(%rdi)
	je	.L209
.L216:
	xorl	%eax, %eax
.L207:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movswl	24(%rsi), %ecx
	movswl	24(%rdi), %edx
	movl	%ecx, %eax
	andl	$1, %eax
	testb	$1, %dl
	jne	.L211
	testw	%dx, %dx
	js	.L212
	sarl	$5, %edx
.L213:
	testw	%cx, %cx
	js	.L214
	sarl	$5, %ecx
.L215:
	testb	%al, %al
	jne	.L216
	cmpl	%edx, %ecx
	jne	.L216
	leaq	16(%rbx), %rsi
	leaq	16(%r12), %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	setne	%al
	.p2align 4,,10
	.p2align 3
.L211:
	testb	%al, %al
	je	.L216
	movl	96(%r12), %esi
	cmpl	96(%rbx), %esi
	jne	.L216
	testl	%esi, %esi
	je	.L207
	movq	80(%r12), %rcx
	movq	80(%rbx), %rdx
	jle	.L207
	movq	(%rdx), %rdx
	subl	$1, %esi
	movq	(%rcx), %rcx
	salq	$4, %rsi
	leaq	16(%rdx,%rsi), %rsi
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L237:
	movl	4(%rdx), %ebx
	cmpl	%ebx, 4(%rcx)
	jne	.L216
	movl	8(%rdx), %ebx
	cmpl	%ebx, 8(%rcx)
	jne	.L216
	movl	12(%rdx), %ebx
	cmpl	%ebx, 12(%rcx)
	jne	.L216
	.p2align 4,,10
	.p2align 3
.L217:
	addq	$16, %rdx
	addq	$16, %rcx
	cmpq	%rsi, %rdx
	je	.L207
.L218:
	cmpq	%rcx, %rdx
	je	.L217
	movl	(%rdx), %edi
	cmpl	%edi, (%rcx)
	jne	.L216
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	28(%rdi), %edx
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L214:
	movl	28(%rbx), %ecx
	jmp	.L215
	.cfi_endproc
.LFE2357:
	.size	_ZNK6icu_6714MessagePatterneqERKS0_, .-_ZNK6icu_6714MessagePatterneqERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714MessagePattern8hashCodeEv
	.type	_ZNK6icu_6714MessagePattern8hashCodeEv, @function
_ZNK6icu_6714MessagePattern8hashCodeEv:
.LFB2358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	8(%rdi), %eax
	movq	%rdi, %rbx
	addq	$16, %rdi
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %r12d
	call	_ZNK6icu_6713UnicodeString10doHashCodeEv@PLT
	movl	96(%rbx), %edx
	addl	%r12d, %eax
	leal	(%rax,%rax,8), %ecx
	leal	(%rax,%rcx,4), %eax
	addl	%edx, %eax
	testl	%edx, %edx
	jle	.L238
	movq	88(%rbx), %rcx
	leal	-1(%rdx), %r8d
	salq	$4, %r8
	leaq	16(%rcx), %rsi
	addq	%rsi, %r8
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L243:
	addq	$16, %rsi
.L240:
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %edi
	movl	(%rcx), %edx
	leal	(%rdx,%rdx,8), %eax
	leal	(%rdx,%rax,4), %edx
	addl	4(%rcx), %edx
	leal	(%rdx,%rdx,8), %eax
	leal	(%rdx,%rax,4), %edx
	movzwl	8(%rcx), %eax
	addl	%eax, %edx
	leal	(%rdx,%rdx,8), %eax
	leal	(%rdx,%rax,4), %eax
	movswl	10(%rcx), %edx
	movq	%rsi, %rcx
	addl	%edx, %eax
	addl	%edi, %eax
	cmpq	%rsi, %r8
	jne	.L243
.L238:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2358:
	.size	_ZNK6icu_6714MessagePattern8hashCodeEv, .-_ZNK6icu_6714MessagePattern8hashCodeEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714MessagePattern23autoQuoteApostropheDeepEv
	.type	_ZNK6icu_6714MessagePattern23autoQuoteApostropheDeepEv, @function
_ZNK6icu_6714MessagePattern23autoQuoteApostropheDeepEv:
.LFB2360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	$16, %rsi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 126(%r12)
	jne	.L245
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
.L244:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L252
	addq	$120, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	96(%r12), %eax
	testl	%eax, %eax
	jle	.L247
	movslq	%eax, %r13
	subl	$1, %eax
	leaq	-130(%rbp), %rcx
	leaq	-1(%r13), %rbx
	subq	%rax, %r13
	salq	$4, %r13
	salq	$4, %rbx
	subq	$32, %r13
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L249:
	subq	$16, %rbx
	cmpq	%rbx, %r13
	je	.L247
.L248:
	movq	88(%r12), %rax
	addq	%rbx, %rax
	cmpl	$3, (%rax)
	jne	.L249
	movl	4(%rax), %esi
	movzwl	10(%rax), %eax
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rdi
	subq	$16, %rbx
	movq	%rcx, -152(%rbp)
	movw	%ax, -130(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movq	-152(%rbp), %rcx
	cmpq	%rbx, %r13
	jne	.L248
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6713UnicodeStringC1EOS0_@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L244
.L252:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2360:
	.size	_ZNK6icu_6714MessagePattern23autoQuoteApostropheDeepEv, .-_ZNK6icu_6714MessagePattern23autoQuoteApostropheDeepEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714MessagePattern15getNumericValueERKNS0_4PartE
	.type	_ZNK6icu_6714MessagePattern15getNumericValueERKNS0_4PartE, @function
_ZNK6icu_6714MessagePattern15getNumericValueERKNS0_4PartE:
.LFB2361:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	cmpl	$12, %eax
	je	.L257
	movsd	.LC0(%rip), %xmm0
	cmpl	$13, %eax
	je	.L258
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	movswq	10(%rsi), %rdx
	movq	112(%rdi), %rax
	movsd	(%rax,%rdx,8), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	movswl	10(%rsi), %eax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	ret
	.cfi_endproc
.LFE2361:
	.size	_ZNK6icu_6714MessagePattern15getNumericValueERKNS0_4PartE, .-_ZNK6icu_6714MessagePattern15getNumericValueERKNS0_4PartE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714MessagePattern15getPluralOffsetEi
	.type	_ZNK6icu_6714MessagePattern15getPluralOffsetEi, @function
_ZNK6icu_6714MessagePattern15getPluralOffsetEi:
.LFB2362:
	.cfi_startproc
	endbr64
	movslq	%esi, %rsi
	pxor	%xmm0, %xmm0
	salq	$4, %rsi
	addq	88(%rdi), %rsi
	movl	(%rsi), %edx
	leal	-12(%rdx), %eax
	cmpl	$1, %eax
	ja	.L259
	movswq	10(%rsi), %rax
	cmpl	$12, %edx
	je	.L263
	movq	112(%rdi), %rdx
	movsd	(%rdx,%rax,8), %xmm0
.L259:
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	ret
	.cfi_endproc
.LFE2362:
	.size	_ZNK6icu_6714MessagePattern15getPluralOffsetEi, .-_ZNK6icu_6714MessagePattern15getPluralOffsetEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6714MessagePattern4ParteqERKS1_
	.type	_ZNK6icu_6714MessagePattern4ParteqERKS1_, @function
_ZNK6icu_6714MessagePattern4ParteqERKS1_:
.LFB2363:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L267
	movl	(%rsi), %edx
	xorl	%eax, %eax
	cmpl	%edx, (%rdi)
	je	.L271
.L264:
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	movl	4(%rsi), %ecx
	cmpl	%ecx, 4(%rdi)
	jne	.L264
	movl	8(%rsi), %ecx
	cmpl	%ecx, 8(%rdi)
	jne	.L264
	movl	12(%rsi), %eax
	cmpl	%eax, 12(%rdi)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2363:
	.size	_ZNK6icu_6714MessagePattern4ParteqERKS1_, .-_ZNK6icu_6714MessagePattern4ParteqERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern8preParseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern8preParseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern8preParseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode:
.LFB2364:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L279
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L274
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	$0, (%rdx)
	movw	%cx, 8(%rdx)
	movw	%di, 40(%rdx)
.L274:
	leaq	16(%rbx), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%eax, %eax
	movl	$0, 96(%rbx)
	movl	$0, 120(%rbx)
	movw	%ax, 124(%rbx)
	movb	$0, 126(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2364:
	.size	_ZN6icu_6714MessagePattern8preParseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern8preParseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern9postParseEv
	.type	_ZN6icu_6714MessagePattern9postParseEv, @function
_ZN6icu_6714MessagePattern9postParseEv:
.LFB2365:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	testq	%rax, %rax
	je	.L283
	movq	(%rax), %rax
	movq	%rax, 88(%rdi)
.L283:
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.L282
	movq	(%rax), %rax
	movq	%rax, 112(%rdi)
.L282:
	ret
	.cfi_endproc
.LFE2365:
	.size	_ZN6icu_6714MessagePattern9postParseEv, .-_ZN6icu_6714MessagePattern9postParseEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern14parseArgNumberERKNS_13UnicodeStringEii
	.type	_ZN6icu_6714MessagePattern14parseArgNumberERKNS_13UnicodeStringEii, @function
_ZN6icu_6714MessagePattern14parseArgNumberERKNS_13UnicodeStringEii:
.LFB2371:
	.cfi_startproc
	endbr64
	cmpl	%edx, %esi
	jge	.L306
	movzwl	8(%rdi), %ecx
	leal	1(%rsi), %r8d
	testw	%cx, %cx
	js	.L293
	movswl	%cx, %r9d
	sarl	$5, %r9d
.L294:
	movl	$-1, %eax
	cmpl	%esi, %r9d
	jbe	.L291
	andl	$2, %ecx
	jne	.L312
	movq	24(%rdi), %rdi
.L296:
	movslq	%esi, %rax
	leaq	(%rax,%rax), %rcx
	movzwl	(%rdi,%rax,2), %eax
	cmpw	$48, %ax
	je	.L313
	leal	-49(%rax), %r10d
	cmpw	$8, %r10w
	ja	.L310
	subl	$48, %eax
	cmpl	%r8d, %edx
	jle	.L291
	xorl	%r11d, %r11d
.L298:
	addl	$2, %esi
	cmpl	%r8d, %r9d
	jbe	.L310
	leaq	2(%rdi,%rcx), %rdi
	movl	$1, %r10d
	movzwl	(%rdi), %ecx
	leal	-48(%rcx), %r8d
	cmpw	$9, %r8w
	ja	.L310
	.p2align 4,,10
	.p2align 3
.L315:
	cmpl	$214748364, %eax
	leal	(%rax,%rax,4), %eax
	cmovge	%r10d, %r11d
	leal	-48(%rcx,%rax,2), %eax
	cmpl	%esi, %edx
	jle	.L314
	leal	1(%rsi), %ecx
	addq	$2, %rdi
	cmpl	%esi, %r9d
	jbe	.L310
	movl	%ecx, %esi
	movzwl	(%rdi), %ecx
	leal	-48(%rcx), %r8d
	cmpw	$9, %r8w
	jbe	.L315
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$-1, %eax
.L291:
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	addq	$10, %rdi
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L314:
	testb	%r11b, %r11b
	movl	$-2, %edx
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	movl	12(%rdi), %r9d
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L313:
	xorl	%eax, %eax
	cmpl	%r8d, %edx
	je	.L291
	jle	.L306
	movl	$1, %r11d
	jmp	.L298
.L306:
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE2371:
	.size	_ZN6icu_6714MessagePattern14parseArgNumberERKNS_13UnicodeStringEii, .-_ZN6icu_6714MessagePattern14parseArgNumberERKNS_13UnicodeStringEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern20validateArgumentNameERKNS_13UnicodeStringE
	.type	_ZN6icu_6714MessagePattern20validateArgumentNameERKNS_13UnicodeStringE, @function
_ZN6icu_6714MessagePattern20validateArgumentNameERKNS_13UnicodeStringE:
.LFB2359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L317
	movswl	%ax, %esi
	sarl	$5, %esi
	testb	$17, %al
	jne	.L324
.L327:
	leaq	10(%r12), %rdi
	testb	$2, %al
	je	.L326
.L319:
	call	_ZN6icu_6712PatternProps12isIdentifierEPKDsi@PLT
	testb	%al, %al
	je	.L321
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L322
	sarl	$5, %edx
.L323:
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714MessagePattern14parseArgNumberERKNS_13UnicodeStringEii
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	24(%r12), %rdi
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L317:
	movl	12(%rdi), %esi
	testb	$17, %al
	je	.L327
.L324:
	xorl	%edi, %edi
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L322:
	movl	12(%r12), %edx
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L321:
	addq	$8, %rsp
	movl	$-2, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2359:
	.size	_ZN6icu_6714MessagePattern20validateArgumentNameERKNS_13UnicodeStringE, .-_ZN6icu_6714MessagePattern20validateArgumentNameERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern14skipWhiteSpaceEi
	.type	_ZN6icu_6714MessagePattern14skipWhiteSpaceEi, @function
_ZN6icu_6714MessagePattern14skipWhiteSpaceEi:
.LFB2373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzwl	24(%rdi), %eax
	testb	$17, %al
	jne	.L333
	leaq	26(%rdi), %rbx
	testb	$2, %al
	je	.L335
.L329:
	testw	%ax, %ax
	js	.L331
.L336:
	movswl	%ax, %esi
	sarl	$5, %esi
.L332:
	leaq	(%rbx,%rdx,2), %rdi
	subl	%edx, %esi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	addq	$8, %rsp
	subq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarq	%rax
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	40(%rdi), %rbx
	testw	%ax, %ax
	jns	.L336
.L331:
	movl	28(%rdi), %esi
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L333:
	xorl	%ebx, %ebx
	jmp	.L329
	.cfi_endproc
.LFE2373:
	.size	_ZN6icu_6714MessagePattern14skipWhiteSpaceEi, .-_ZN6icu_6714MessagePattern14skipWhiteSpaceEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern14skipIdentifierEi
	.type	_ZN6icu_6714MessagePattern14skipIdentifierEi, @function
_ZN6icu_6714MessagePattern14skipIdentifierEi:
.LFB2374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzwl	24(%rdi), %eax
	testb	$17, %al
	jne	.L342
	leaq	26(%rdi), %rbx
	testb	$2, %al
	je	.L344
.L338:
	testw	%ax, %ax
	js	.L340
.L345:
	movswl	%ax, %esi
	sarl	$5, %esi
.L341:
	leaq	(%rbx,%rdx,2), %rdi
	subl	%edx, %esi
	call	_ZN6icu_6712PatternProps14skipIdentifierEPKDsi@PLT
	addq	$8, %rsp
	subq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarq	%rax
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	movq	40(%rdi), %rbx
	testw	%ax, %ax
	jns	.L345
.L340:
	movl	28(%rdi), %esi
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L342:
	xorl	%ebx, %ebx
	jmp	.L338
	.cfi_endproc
.LFE2374:
	.size	_ZN6icu_6714MessagePattern14skipIdentifierEi, .-_ZN6icu_6714MessagePattern14skipIdentifierEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern10skipDoubleEi
	.type	_ZN6icu_6714MessagePattern10skipDoubleEi, @function
_ZN6icu_6714MessagePattern10skipDoubleEi:
.LFB2375:
	.cfi_startproc
	endbr64
	movzwl	24(%rdi), %edx
	movl	%esi, %eax
	testw	%dx, %dx
	js	.L347
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L348:
	cmpl	%ecx, %eax
	jge	.L346
	andl	$2, %edx
	movslq	%eax, %rsi
	jne	.L361
	addq	%rsi, %rsi
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L351:
	movq	40(%rdi), %rdx
	movzwl	(%rdx,%rsi), %edx
	cmpw	$43, %dx
	je	.L356
	cmpw	$47, %dx
	ja	.L356
	subl	$45, %edx
	cmpw	$1, %dx
	ja	.L393
.L352:
	addl	$1, %eax
	addq	$2, %rsi
	cmpl	%eax, %ecx
	je	.L394
.L357:
	cmpl	%ecx, %eax
	jb	.L351
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	movzwl	26(%rdi,%rsi,2), %edx
	cmpw	$47, %dx
	ja	.L358
	cmpw	$43, %dx
	je	.L358
	subl	$45, %edx
	cmpw	$1, %dx
	ja	.L395
.L359:
	addq	$1, %rsi
	leal	1(%r8), %eax
	cmpl	%esi, %ecx
	jle	.L346
.L361:
	movl	%esi, %r8d
	movl	%esi, %eax
	cmpl	%esi, %ecx
	ja	.L396
.L346:
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	cmpw	$57, %dx
	jbe	.L359
	cmpw	$101, %dx
	je	.L359
	cmpw	$69, %dx
	je	.L359
	cmpw	$8734, %dx
	je	.L359
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	cmpw	$101, %dx
	je	.L352
	cmpw	$57, %dx
	jbe	.L352
	cmpw	$8734, %dx
	je	.L352
	cmpw	$69, %dx
	je	.L352
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	movl	28(%rdi), %ecx
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L394:
	ret
.L393:
	ret
.L395:
	ret
	.cfi_endproc
.LFE2375:
	.size	_ZN6icu_6714MessagePattern10skipDoubleEi, .-_ZN6icu_6714MessagePattern10skipDoubleEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern13isArgTypeCharEi
	.type	_ZN6icu_6714MessagePattern13isArgTypeCharEi, @function
_ZN6icu_6714MessagePattern13isArgTypeCharEi:
.LFB2376:
	.cfi_startproc
	endbr64
	andl	$-33, %edi
	subl	$65, %edi
	cmpl	$25, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE2376:
	.size	_ZN6icu_6714MessagePattern13isArgTypeCharEi, .-_ZN6icu_6714MessagePattern13isArgTypeCharEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern8isChoiceEi
	.type	_ZN6icu_6714MessagePattern8isChoiceEi, @function
_ZN6icu_6714MessagePattern8isChoiceEi:
.LFB2377:
	.cfi_startproc
	endbr64
	movzwl	24(%rdi), %edx
	testw	%dx, %dx
	js	.L399
	movswl	%dx, %eax
	sarl	$5, %eax
.L400:
	xorl	%r8d, %r8d
	cmpl	%esi, %eax
	jbe	.L398
	andl	$2, %edx
	jne	.L415
	movq	40(%rdi), %rdi
.L403:
	movslq	%esi, %rdx
	xorl	%r8d, %r8d
	leaq	(%rdx,%rdx), %rcx
	movzwl	(%rdi,%rdx,2), %edx
	andl	$-33, %edx
	cmpw	$67, %dx
	jne	.L398
	leal	1(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L398
	movzwl	2(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$72, %dx
	jne	.L398
	leal	2(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L398
	movzwl	4(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$79, %dx
	jne	.L398
	leal	3(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L398
	movzwl	6(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$73, %dx
	jne	.L398
	leal	4(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L398
	movzwl	8(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$67, %dx
	jne	.L398
	addl	$5, %esi
	cmpl	%esi, %eax
	jbe	.L398
	movzwl	10(%rdi,%rcx), %eax
	andl	$-33, %eax
	cmpw	$69, %ax
	sete	%r8b
.L398:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	addq	$26, %rdi
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L399:
	movl	28(%rdi), %eax
	jmp	.L400
	.cfi_endproc
.LFE2377:
	.size	_ZN6icu_6714MessagePattern8isChoiceEi, .-_ZN6icu_6714MessagePattern8isChoiceEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern8isPluralEi
	.type	_ZN6icu_6714MessagePattern8isPluralEi, @function
_ZN6icu_6714MessagePattern8isPluralEi:
.LFB2378:
	.cfi_startproc
	endbr64
	movzwl	24(%rdi), %edx
	testw	%dx, %dx
	js	.L417
	movswl	%dx, %eax
	sarl	$5, %eax
.L418:
	xorl	%r8d, %r8d
	cmpl	%esi, %eax
	jbe	.L416
	andl	$2, %edx
	jne	.L433
	movq	40(%rdi), %rdi
.L421:
	movslq	%esi, %rdx
	xorl	%r8d, %r8d
	leaq	(%rdx,%rdx), %rcx
	movzwl	(%rdi,%rdx,2), %edx
	andl	$-33, %edx
	cmpw	$80, %dx
	jne	.L416
	leal	1(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L416
	movzwl	2(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$76, %dx
	jne	.L416
	leal	2(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L416
	movzwl	4(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$85, %dx
	jne	.L416
	leal	3(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L416
	movzwl	6(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$82, %dx
	jne	.L416
	leal	4(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L416
	movzwl	8(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$65, %dx
	jne	.L416
	addl	$5, %esi
	cmpl	%esi, %eax
	jbe	.L416
	movzwl	10(%rdi,%rcx), %eax
	andl	$-33, %eax
	cmpw	$76, %ax
	sete	%r8b
.L416:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	addq	$26, %rdi
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L417:
	movl	28(%rdi), %eax
	jmp	.L418
	.cfi_endproc
.LFE2378:
	.size	_ZN6icu_6714MessagePattern8isPluralEi, .-_ZN6icu_6714MessagePattern8isPluralEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern8isSelectEi
	.type	_ZN6icu_6714MessagePattern8isSelectEi, @function
_ZN6icu_6714MessagePattern8isSelectEi:
.LFB2379:
	.cfi_startproc
	endbr64
	movzwl	24(%rdi), %edx
	testw	%dx, %dx
	js	.L435
	movswl	%dx, %eax
	sarl	$5, %eax
.L436:
	xorl	%r8d, %r8d
	cmpl	%esi, %eax
	jbe	.L434
	andl	$2, %edx
	jne	.L451
	movq	40(%rdi), %rdi
.L439:
	movslq	%esi, %rdx
	xorl	%r8d, %r8d
	leaq	(%rdx,%rdx), %rcx
	movzwl	(%rdi,%rdx,2), %edx
	andl	$-33, %edx
	cmpw	$83, %dx
	jne	.L434
	leal	1(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L434
	movzwl	2(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$69, %dx
	jne	.L434
	leal	2(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L434
	movzwl	4(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$76, %dx
	jne	.L434
	leal	3(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L434
	movzwl	6(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$69, %dx
	jne	.L434
	leal	4(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L434
	movzwl	8(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$67, %dx
	jne	.L434
	addl	$5, %esi
	cmpl	%esi, %eax
	jbe	.L434
	movzwl	10(%rdi,%rcx), %eax
	andl	$-33, %eax
	cmpw	$84, %ax
	sete	%r8b
.L434:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	addq	$26, %rdi
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L435:
	movl	28(%rdi), %eax
	jmp	.L436
	.cfi_endproc
.LFE2379:
	.size	_ZN6icu_6714MessagePattern8isSelectEi, .-_ZN6icu_6714MessagePattern8isSelectEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern9isOrdinalEi
	.type	_ZN6icu_6714MessagePattern9isOrdinalEi, @function
_ZN6icu_6714MessagePattern9isOrdinalEi:
.LFB2380:
	.cfi_startproc
	endbr64
	movzwl	24(%rdi), %edx
	testw	%dx, %dx
	js	.L453
	movswl	%dx, %eax
	sarl	$5, %eax
.L454:
	xorl	%r8d, %r8d
	cmpl	%eax, %esi
	jnb	.L452
	andl	$2, %edx
	jne	.L471
	movq	40(%rdi), %rdi
.L457:
	movslq	%esi, %rdx
	xorl	%r8d, %r8d
	leaq	(%rdx,%rdx), %rcx
	movzwl	(%rdi,%rdx,2), %edx
	andl	$-33, %edx
	cmpw	$79, %dx
	jne	.L452
	leal	1(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L452
	movzwl	2(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$82, %dx
	jne	.L452
	leal	2(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L452
	movzwl	4(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$68, %dx
	jne	.L452
	leal	3(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L452
	movzwl	6(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$73, %dx
	jne	.L452
	leal	4(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L452
	movzwl	8(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$78, %dx
	jne	.L452
	leal	5(%rsi), %edx
	cmpl	%edx, %eax
	jbe	.L452
	movzwl	10(%rdi,%rcx), %edx
	andl	$-33, %edx
	cmpw	$65, %dx
	jne	.L452
	addl	$6, %esi
	cmpl	%esi, %eax
	jbe	.L452
	movzwl	12(%rdi,%rcx), %eax
	andl	$-33, %eax
	cmpw	$76, %ax
	sete	%r8b
.L452:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	addq	$26, %rdi
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L453:
	movl	28(%rdi), %eax
	jmp	.L454
	.cfi_endproc
.LFE2380:
	.size	_ZN6icu_6714MessagePattern9isOrdinalEi, .-_ZN6icu_6714MessagePattern9isOrdinalEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern22inMessageFormatPatternEi
	.type	_ZN6icu_6714MessagePattern22inMessageFormatPatternEi, @function
_ZN6icu_6714MessagePattern22inMessageFormatPatternEi:
.LFB2381:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	testl	%esi, %esi
	jle	.L475
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	movq	80(%rdi), %rax
	movq	(%rax), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE2381:
	.size	_ZN6icu_6714MessagePattern22inMessageFormatPatternEi, .-_ZN6icu_6714MessagePattern22inMessageFormatPatternEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern23inTopLevelChoiceMessageEi22UMessagePatternArgType
	.type	_ZN6icu_6714MessagePattern23inTopLevelChoiceMessageEi22UMessagePatternArgType, @function
_ZN6icu_6714MessagePattern23inTopLevelChoiceMessageEi22UMessagePatternArgType:
.LFB2382:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	jne	.L478
	cmpl	$2, %edx
	jne	.L478
	movq	80(%rdi), %rax
	movq	(%rax), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2382:
	.size	_ZN6icu_6714MessagePattern23inTopLevelChoiceMessageEi22UMessagePatternArgType, .-_ZN6icu_6714MessagePattern23inTopLevelChoiceMessageEi22UMessagePatternArgType
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	.type	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode, @function
_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode:
.LFB2383:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L495
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	80(%rdi), %rcx
	movslq	96(%rdi), %rbx
	cmpl	8(%rcx), %ebx
	jl	.L487
	leal	(%rbx,%rbx), %r10d
	movl	%r8d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	testl	%r10d, %r10d
	jle	.L488
	movslq	%r10d, %rdi
	movq	%r9, -80(%rbp)
	salq	$4, %rdi
	movl	%r10d, -72(%rbp)
	call	uprv_malloc_67@PLT
	movq	-80(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r11
	je	.L488
	movq	-56(%rbp), %rcx
	testl	%ebx, %ebx
	movl	-72(%rbp), %r10d
	movl	-60(%rbp), %r8d
	movq	(%rcx), %rsi
	jg	.L498
.L489:
	cmpb	$0, 12(%rcx)
	jne	.L499
.L490:
	movb	$1, 12(%rcx)
	movslq	96(%r12), %rbx
	movq	%r11, (%rcx)
	movl	%r10d, 8(%rcx)
	movq	80(%r12), %rcx
.L487:
	leal	1(%rbx), %eax
	salq	$4, %rbx
	addq	(%rcx), %rbx
	movl	%eax, 96(%r12)
	movl	%r14d, (%rbx)
	movl	%r13d, 4(%rbx)
	movw	%r15w, 8(%rbx)
	movw	%r8w, 10(%rbx)
	movl	$0, 12(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, (%r9)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	cmpl	%r10d, 8(%rcx)
	movl	%r10d, %eax
	cmovle	8(%rcx), %eax
	movl	%ebx, %edx
	movq	%r11, %rdi
	movl	%r8d, -72(%rbp)
	cmpl	%ebx, %eax
	movl	%r10d, -60(%rbp)
	cmovle	%eax, %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	call	memcpy@PLT
	movq	-56(%rbp), %rcx
	movl	-72(%rbp), %r8d
	movl	-60(%rbp), %r10d
	movq	%rax, %r11
	movq	(%rcx), %rsi
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L499:
	movq	%rsi, %rdi
	movl	%r8d, -80(%rbp)
	movq	%r11, -72(%rbp)
	movl	%r10d, -60(%rbp)
	movq	%rcx, -56(%rbp)
	call	uprv_free_67@PLT
	movl	-80(%rbp), %r8d
	movq	-72(%rbp), %r11
	movl	-60(%rbp), %r10d
	movq	-56(%rbp), %rcx
	jmp	.L490
	.cfi_endproc
.LFE2383:
	.size	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode, .-_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern12addLimitPartEi23UMessagePatternPartTypeiiiR10UErrorCode
	.type	_ZN6icu_6714MessagePattern12addLimitPartEi23UMessagePatternPartTypeiiiR10UErrorCode, @function
_ZN6icu_6714MessagePattern12addLimitPartEi23UMessagePatternPartTypeiiiR10UErrorCode:
.LFB2384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	salq	$4, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %r13
	movslq	96(%rdi), %rdx
	movq	16(%rbp), %rdi
	movq	0(%r13), %rax
	movl	%edx, 12(%rax,%rsi)
	movl	(%rdi), %esi
	testl	%esi, %esi
	jg	.L500
	movl	%ecx, %r15d
	movl	%r9d, %r12d
	cmpl	8(%r13), %edx
	jl	.L502
	leal	(%rdx,%rdx), %ecx
	movl	%r8d, -64(%rbp)
	movl	%edx, -52(%rbp)
	testl	%ecx, %ecx
	jle	.L503
	movslq	%ecx, %rdi
	movl	%ecx, -56(%rbp)
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L503
	movl	-52(%rbp), %edx
	movq	0(%r13), %rsi
	movl	-56(%rbp), %ecx
	movl	-64(%rbp), %r8d
	testl	%edx, %edx
	jg	.L510
.L504:
	cmpb	$0, 12(%r13)
	jne	.L511
.L505:
	movb	$1, 12(%r13)
	movq	80(%rbx), %rax
	movl	%ecx, 8(%r13)
	movslq	96(%rbx), %rdx
	movq	%r10, 0(%r13)
	movq	(%rax), %rax
.L502:
	leal	1(%rdx), %ecx
	salq	$4, %rdx
	addq	%rax, %rdx
	movl	%ecx, 96(%rbx)
	movl	%r14d, (%rdx)
	movl	%r15d, 4(%rdx)
	movw	%r8w, 8(%rdx)
	movw	%r12w, 10(%rdx)
	movl	$0, 12(%rdx)
.L500:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	movq	16(%rbp), %rax
	movl	$7, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	cmpl	%ecx, 8(%r13)
	movl	%ecx, %eax
	cmovle	8(%r13), %eax
	movq	%r10, %rdi
	movl	%ecx, -52(%rbp)
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	call	memcpy@PLT
	movq	0(%r13), %rsi
	movl	-64(%rbp), %r8d
	movl	-52(%rbp), %ecx
	movq	%rax, %r10
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L511:
	movq	%rsi, %rdi
	movl	%r8d, -56(%rbp)
	movq	%r10, -64(%rbp)
	movl	%ecx, -52(%rbp)
	call	uprv_free_67@PLT
	movl	-56(%rbp), %r8d
	movq	-64(%rbp), %r10
	movl	-52(%rbp), %ecx
	jmp	.L505
	.cfi_endproc
.LFE2384:
	.size	_ZN6icu_6714MessagePattern12addLimitPartEi23UMessagePatternPartTypeiiiR10UErrorCode, .-_ZN6icu_6714MessagePattern12addLimitPartEi23UMessagePatternPartTypeiiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode
	.type	_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode, @function
_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode:
.LFB2385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -52(%rbp)
	movl	(%rcx), %edx
	testl	%edx, %edx
	jg	.L512
	movq	104(%rdi), %r15
	movl	120(%rdi), %r13d
	movq	%rdi, %rbx
	movl	%esi, %r14d
	movq	%rcx, %r12
	testq	%r15, %r15
	je	.L535
	cmpl	8(%r15), %r13d
	jl	.L518
	leal	(%r13,%r13), %r8d
	movsd	%xmm0, -64(%rbp)
	testl	%r8d, %r8d
	jle	.L534
	movslq	%r8d, %rdi
	movl	%r8d, -72(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L534
	testl	%r13d, %r13d
	movq	(%r15), %rsi
	movl	-72(%rbp), %r8d
	movsd	-64(%rbp), %xmm0
	jle	.L520
	cmpl	%r8d, 8(%r15)
	movl	%r8d, %eax
	cmovle	8(%r15), %eax
	movq	%r9, %rdi
	movl	%r8d, -64(%rbp)
	cmpl	%r13d, %eax
	movsd	%xmm0, -72(%rbp)
	cmovg	%r13d, %eax
	movslq	%eax, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movq	(%r15), %rsi
	movsd	-72(%rbp), %xmm0
	movl	-64(%rbp), %r8d
	movq	%rax, %r9
.L520:
	cmpb	$0, 12(%r15)
	jne	.L536
.L521:
	movq	%r9, (%r15)
	movl	%r8d, 8(%r15)
	movb	$1, 12(%r15)
.L518:
	cmpl	$32767, %r13d
	jg	.L537
	movq	104(%rbx), %rax
	movq	(%rax), %rdx
.L517:
	movslq	120(%rbx), %rax
	leal	1(%rax), %esi
	movl	%esi, 120(%rbx)
	movsd	%xmm0, (%rdx,%rax,8)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L512
	movq	80(%rbx), %r8
	movslq	96(%rbx), %r15
	cmpl	8(%r8), %r15d
	jl	.L524
	leal	(%r15,%r15), %r9d
	movq	%r8, -64(%rbp)
	testl	%r9d, %r9d
	jle	.L534
	movslq	%r9d, %rdi
	movl	%r9d, -72(%rbp)
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L534
	movq	-64(%rbp), %r8
	testl	%r15d, %r15d
	movl	-72(%rbp), %r9d
	movq	(%r8), %rsi
	jg	.L538
.L525:
	cmpb	$0, 12(%r8)
	jne	.L539
.L526:
	movb	$1, 12(%r8)
	movslq	96(%rbx), %r15
	movq	%r10, (%r8)
	movl	%r9d, 8(%r8)
	movq	80(%rbx), %r8
.L524:
	leal	1(%r15), %eax
	salq	$4, %r15
	addq	(%r8), %r15
	movl	%eax, 96(%rbx)
	movzwl	-52(%rbp), %eax
	movl	$13, (%r15)
	movl	%r14d, 4(%r15)
	movw	%ax, 8(%r15)
	movw	%r13w, 10(%r15)
	movl	$0, 12(%r15)
.L512:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L516:
	.cfi_restore_state
	movq	$0, 104(%rbx)
.L534:
	movl	$7, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movl	$80, %edi
	movsd	%xmm0, -64(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L516
	leaq	16(%rax), %rdx
	movb	$0, 12(%rax)
	movsd	-64(%rbp), %xmm0
	movq	%rdx, (%rax)
	movl	$8, 8(%rax)
	movq	%rax, 104(%rbx)
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L537:
	movl	$8, (%r12)
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L536:
	movq	%rsi, %rdi
	movq	%r9, -72(%rbp)
	movl	%r8d, -64(%rbp)
	movsd	%xmm0, -80(%rbp)
	call	uprv_free_67@PLT
	movsd	-80(%rbp), %xmm0
	movq	-72(%rbp), %r9
	movl	-64(%rbp), %r8d
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L538:
	cmpl	%r9d, 8(%r8)
	movl	%r9d, %eax
	cmovle	8(%r8), %eax
	movl	%r15d, %edx
	movq	%r10, %rdi
	cmpl	%r15d, %eax
	cmovle	%eax, %edx
	movslq	%edx, %rdx
	salq	$4, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movl	-72(%rbp), %r9d
	movq	%rax, %r10
	movq	(%r8), %rsi
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%rsi, %rdi
	movq	%r10, -80(%rbp)
	movl	%r9d, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	uprv_free_67@PLT
	movq	-80(%rbp), %r10
	movl	-72(%rbp), %r9d
	movq	-64(%rbp), %r8
	jmp	.L526
	.cfi_endproc
.LFE2385:
	.size	_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode, .-_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	.type	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori, @function
_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori:
.LFB2386:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L561
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	%edx, 4(%rsi)
	cmpl	$15, %edx
	jg	.L564
	movslq	%edx, %r14
	xorl	%esi, %esi
.L542:
	leaq	8(%rbx), %rax
	leaq	16(%r13), %r15
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movq	%rax, %rcx
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movw	%dx, 8(%rbx,%r14,2)
	movzwl	24(%r13), %edx
	testw	%dx, %dx
	js	.L547
	movswl	%dx, %eax
	sarl	$5, %eax
.L548:
	movl	%eax, %r14d
	subl	%r12d, %r14d
	cmpl	$15, %r14d
	jle	.L549
	leal	14(%r12), %ecx
	movl	$15, %r14d
	cmpl	%ecx, %eax
	jbe	.L549
	andl	$2, %edx
	jne	.L565
	movq	40(%r13), %r13
.L551:
	movslq	%ecx, %rcx
	xorl	%r14d, %r14d
	movzwl	0(%r13,%rcx,2), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	setne	%r14b
	addl	$14, %r14d
.L549:
	leaq	40(%rbx), %r13
	xorl	%r8d, %r8d
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	xorl	%eax, %eax
	movslq	%r14d, %r14
	movw	%ax, 40(%rbx,%r14,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movzwl	24(%rdi), %eax
	leal	-15(%rdx), %esi
	testw	%ax, %ax
	js	.L543
	movswl	%ax, %edx
	sarl	$5, %edx
.L544:
	movl	$15, %r14d
	cmpl	%esi, %edx
	jbe	.L542
	testb	$2, %al
	jne	.L566
	movq	40(%r13), %rax
.L546:
	movslq	%esi, %rdx
	movl	$15, %r14d
	movzwl	(%rax,%rdx,2), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L542
	leal	-14(%r12), %esi
	movl	$14, %r14d
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L547:
	movl	28(%r13), %eax
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L566:
	leaq	26(%r13), %rax
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L565:
	addq	$26, %r13
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L561:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	28(%rdi), %edx
	jmp	.L544
	.cfi_endproc
.LFE2386:
	.size	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori, .-_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	.align 2
	.p2align 4
	.type	_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode.part.0:
.LFB2910:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	movl	%r12d, %eax
	.cfi_offset 3, -56
	leaq	16(%rdi), %rbx
	subq	$24, %rsp
	movzwl	24(%rdi), %esi
	movq	%rcx, -56(%rbp)
	testw	%si, %si
	js	.L569
	.p2align 4,,10
	.p2align 3
.L585:
	movswl	%si, %ecx
	sarl	$5, %ecx
	cmpl	%ecx, %eax
	jge	.L571
.L586:
	leal	1(%rax), %edx
	cmpl	%eax, %ecx
	jbe	.L572
	leaq	26(%r15), %rdi
	testb	$2, %sil
	jne	.L574
	movq	40(%r15), %rdi
.L574:
	movslq	%eax, %r9
	movzwl	(%rdi,%r9,2), %edi
	cmpw	$39, %di
	je	.L584
	cmpw	$123, %di
	jne	.L579
	addl	$1, %r13d
.L572:
	movl	%edx, %eax
.L587:
	testw	%si, %si
	jns	.L585
.L569:
	movl	28(%r15), %ecx
	cmpl	%ecx, %eax
	jl	.L586
.L571:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movq	-56(%rbp), %rax
	movl	$65801, (%rax)
	xorl	%eax, %eax
.L567:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	cmpw	$125, %di
	jne	.L572
	testl	%r13d, %r13d
	je	.L580
	subl	$1, %r13d
	movl	%edx, %eax
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L584:
	xorl	%r9d, %r9d
	testl	%edx, %edx
	js	.L576
	cmpl	%edx, %ecx
	cmovle	%ecx, %edx
	movl	%edx, %r9d
	subl	%edx, %ecx
.L576:
	movl	%r9d, %edx
	movl	$39, %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	js	.L588
	leal	1(%rax), %edx
	movzwl	24(%r15), %esi
	movl	%edx, %eax
	jmp	.L587
.L580:
	movl	%eax, %ecx
	subl	%r12d, %ecx
	cmpl	$65535, %ecx
	jle	.L581
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movq	-56(%rbp), %rax
	movl	$8, (%rax)
	xorl	%eax, %eax
	jmp	.L567
.L588:
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movq	-56(%rbp), %rax
	movl	$65799, (%rax)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L581:
	.cfi_restore_state
	movq	-56(%rbp), %r9
	xorl	%r8d, %r8d
	movl	%r12d, %edx
	movl	$10, %esi
	movq	%r15, %rdi
	movl	%eax, -60(%rbp)
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	movl	-60(%rbp), %eax
	jmp	.L567
	.cfi_endproc
.LFE2910:
	.size	_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode:
.LFB2368:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L591
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	jmp	_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode.part.0
	.cfi_endproc
.LFE2368:
	.size	_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode.part.0:
.LFB2919:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	1(%rsi), %eax
	movzwl	24(%rdi), %esi
	testw	%si, %si
	js	.L593
	movswl	%si, %edi
	sarl	$5, %edi
.L594:
	cmpl	%r12d, %edi
	jbe	.L595
	andl	$2, %esi
	leaq	26(%r13), %r9
	je	.L631
.L597:
	movslq	%r12d, %rsi
	movzwl	(%r9,%rsi,2), %r8d
	leaq	(%rsi,%rsi), %r10
	cmpw	$45, %r8w
	je	.L632
	cmpw	$43, %r8w
	jne	.L614
	cmpl	%edx, %eax
	je	.L599
	leal	2(%r12), %esi
	cmpl	%eax, %edi
	jbe	.L595
	movzwl	2(%r9,%r10), %r8d
	movl	%esi, %eax
	xorl	%ebx, %ebx
	cmpw	$8734, %r8w
	je	.L601
.L636:
	leal	-48(%r8), %ecx
	cmpw	$9, %cx
	ja	.L595
	subl	$48, %r8d
	cmpl	$1, %ebx
	sbbl	%r10d, %r10d
	addl	$32768, %r10d
	cmpl	%eax, %edx
	je	.L602
	cltq
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L633:
	movzwl	(%r9,%rax,2), %ecx
	leal	-48(%rcx), %esi
	cmpw	$9, %si
	ja	.L595
	leal	(%r8,%r8,4), %esi
	leal	-48(%rcx,%rsi,2), %r8d
	cmpl	%r10d, %r8d
	jg	.L595
	addq	$1, %rax
	cmpl	%eax, %edx
	je	.L602
.L607:
	cmpl	%eax, %edi
	ja	.L633
.L595:
	movl	%edx, %ebx
	subl	%r12d, %ebx
	cmpl	$127, %ebx
	jle	.L634
.L599:
	movl	%r12d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movl	$65799, (%r14)
.L592:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L635
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	movq	40(%r13), %r9
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L614:
	xorl	%ebx, %ebx
.L600:
	cmpw	$8734, %r8w
	jne	.L636
.L601:
	testb	%cl, %cl
	je	.L599
	cmpl	%eax, %edx
	jne	.L599
	movl	%edx, -216(%rbp)
	call	uprv_getInfinity_67@PLT
	testl	%ebx, %ebx
	movl	-216(%rbp), %edx
	je	.L604
	xorpd	.LC2(%rip), %xmm0
.L604:
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L592
	subl	%r12d, %edx
	movq	%r14, %rcx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode.part.0
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L593:
	movl	28(%rdi), %edi
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L632:
	cmpl	%edx, %eax
	je	.L599
	leal	2(%r12), %esi
	cmpl	%eax, %edi
	jbe	.L595
	movzwl	2(%r9,%r10), %r8d
	movl	%esi, %eax
	movl	$1, %ebx
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L634:
	leaq	-192(%rbp), %r10
	leaq	16(%r13), %rdi
	xorl	%r9d, %r9d
	movl	%ebx, %edx
	movq	%r10, %rcx
	movl	$128, %r8d
	movl	%r12d, %esi
	movq	%r10, -216(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE@PLT
	movq	-216(%rbp), %r10
	movq	%r10, %rcx
.L610:
	movl	(%rcx), %edx
	addq	$4, %rcx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L610
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rcx), %rdx
	cmove	%rdx, %rcx
	movl	%eax, %edi
	addb	%al, %dil
	sbbq	$3, %rcx
	subq	%r10, %rcx
	cmpl	%ecx, %ebx
	jg	.L599
	movq	%r10, %rdi
	leaq	-200(%rbp), %rsi
	movq	%r10, -216(%rbp)
	call	strtod@PLT
	movq	-216(%rbp), %r10
	movslq	%ebx, %rax
	addq	%rax, %r10
	cmpq	%r10, -200(%rbp)
	jne	.L599
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L592
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6714MessagePattern16addArgDoublePartEdiiR10UErrorCode.part.0
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L602:
	movl	%r8d, %eax
	movq	%r14, %r9
	movl	$12, %esi
	movq	%r13, %rdi
	negl	%eax
	testl	%ebx, %ebx
	cmovne	%eax, %r8d
	subl	%r12d, %edx
	movl	%edx, %ecx
	movl	%r12d, %edx
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	jmp	.L592
.L635:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2919:
	.size	_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode:
.LFB2372:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L637
	movsbl	%cl, %ecx
	jmp	_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L637:
	ret
	.cfi_endproc
.LFE2372:
	.size	_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode.part.0:
.LFB2922:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-3(%rsi), %eax
	andl	$-3, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	%esi, -84(%rbp)
	movswl	24(%r12), %esi
	movl	%eax, -72(%rbp)
	leal	1(%rcx), %eax
	movl	%eax, -68(%rbp)
	movl	%edx, %eax
	movl	%edx, -92(%rbp)
	movl	%ecx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movb	$0, -53(%rbp)
	movb	$1, -60(%rbp)
	testb	$17, %sil
	jne	.L691
.L723:
	leaq	26(%r12), %r13
	testb	$2, %sil
	jne	.L641
	movq	40(%r12), %r13
.L641:
	testw	%si, %si
	js	.L643
.L724:
	sarl	$5, %esi
.L644:
	movslq	%eax, %rbx
	subl	%eax, %esi
	leaq	0(%r13,%rbx,2), %rdi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	subq	%r13, %rax
	movq	%rax, %rbx
	movzwl	24(%r12), %eax
	sarq	%rbx
	movl	%ebx, -52(%rbp)
	testw	%ax, %ax
	js	.L645
	movswl	%ax, %esi
	sarl	$5, %esi
.L646:
	cmpl	%ebx, %esi
	je	.L647
	movslq	%ebx, %rdx
	leaq	(%rdx,%rdx), %rdi
	jbe	.L648
	leaq	26(%r12), %rcx
	testb	$2, %al
	jne	.L650
	movq	40(%r12), %rcx
.L650:
	movzwl	(%rcx,%rdx,2), %edx
	cmpw	$125, %dx
	je	.L721
	movl	-72(%rbp), %r9d
	xorl	%r15d, %r15d
	testl	%r9d, %r9d
	jne	.L688
	cmpw	$61, %dx
	je	.L655
	movl	$1, %r15d
.L688:
	testb	$17, %al
	jne	.L692
.L725:
	leaq	26(%r12), %r13
	testb	$2, %al
	jne	.L656
	movq	40(%r12), %r13
.L656:
	addq	%r13, %rdi
	subl	%ebx, %esi
	call	_ZN6icu_6712PatternProps14skipIdentifierEPKDsi@PLT
	subq	%r13, %rax
	movq	%rax, %r13
	sarq	%r13
	movl	%r13d, %r11d
	movl	%r13d, %r10d
	subl	%ebx, %r11d
	je	.L658
	cmpl	$6, %r11d
	jne	.L663
	testb	%r15b, %r15b
	jne	.L722
.L663:
	cmpl	$65535, %r11d
	jg	.L668
	leaq	16(%r12), %r15
.L690:
	xorl	%r8d, %r8d
	movl	%r11d, %ecx
	movq	%r14, %r9
	movl	%ebx, %edx
	movl	$11, %esi
	movq	%r12, %rdi
	movl	%r10d, -64(%rbp)
	movl	%r11d, -60(%rbp)
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	movl	-60(%rbp), %r11d
	xorl	%r8d, %r8d
	movl	%ebx, %esi
	leaq	_ZN6icu_67L6kOtherE(%rip), %rcx
	movl	$5, %r9d
	movq	%r15, %rdi
	movl	%r11d, %edx
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movzbl	-53(%rbp), %ecx
	movslq	-64(%rbp), %r10
	testb	%al, %al
	movl	$1, %eax
	cmove	%eax, %ecx
	movb	%cl, -53(%rbp)
.L662:
	movl	(%r14), %r15d
	testl	%r15d, %r15d
	jg	.L661
	movswl	24(%r12), %esi
	testb	$17, %sil
	jne	.L694
	leaq	26(%r12), %rbx
	testb	$2, %sil
	jne	.L676
	movq	40(%r12), %rbx
.L676:
	testw	%si, %si
	js	.L678
	sarl	$5, %esi
.L679:
	subl	%r10d, %esi
	leaq	(%rbx,%r10,2), %rdi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	subq	%rbx, %rax
	movq	%rax, %rsi
	movzwl	24(%r12), %eax
	sarq	%rsi
	testw	%ax, %ax
	js	.L680
	movswl	%ax, %edx
	sarl	$5, %edx
.L681:
	cmpl	%esi, %edx
	je	.L682
	jbe	.L682
	testb	$2, %al
	je	.L683
	leaq	26(%r12), %rax
.L684:
	movslq	%esi, %rdx
	cmpw	$123, (%rax,%rdx,2)
	jne	.L682
	movl	(%r14), %r13d
	testl	%r13d, %r13d
	jg	.L661
	cmpl	$32767, -68(%rbp)
	jle	.L686
	movl	$8, (%r14)
.L661:
	movl	$0, -52(%rbp)
.L639:
	movl	-52(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	subq	$8, %rsp
	movq	-80(%rbp), %r9
	movl	-84(%rbp), %r8d
	movq	%r12, %rdi
	pushq	%r14
	movl	-68(%rbp), %ecx
	movl	$1, %edx
	call	_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode.part.0
	movl	(%r14), %ebx
	popq	%r10
	popq	%r11
	testl	%ebx, %ebx
	jg	.L661
.L675:
	movswl	24(%r12), %esi
	movb	$0, -60(%rbp)
	testb	$17, %sil
	je	.L723
.L691:
	xorl	%r13d, %r13d
	testw	%si, %si
	jns	.L724
	.p2align 4,,10
	.p2align 3
.L643:
	movl	28(%r12), %esi
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L645:
	movl	28(%r12), %esi
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L680:
	movl	28(%r12), %edx
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L678:
	movl	28(%r12), %esi
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L683:
	movq	40(%r12), %rax
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L682:
	movl	-52(%rbp), %edx
.L719:
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movl	$65799, (%r14)
	movl	$0, -52(%rbp)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L648:
	movl	-72(%rbp), %r8d
	testl	%r8d, %r8d
	sete	%r15b
	testb	$17, %al
	je	.L725
.L692:
	xorl	%r13d, %r13d
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L655:
	leal	1(%rbx), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN6icu_6714MessagePattern10skipDoubleEi
	movl	%eax, %ecx
	subl	%ebx, %ecx
	cmpl	$1, %ecx
	je	.L658
	cmpl	$65535, %ecx
	jg	.L668
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movl	%ebx, %edx
	movl	$11, %esi
	movl	%eax, -60(%rbp)
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L661
	movl	-60(%rbp), %r10d
	movq	-80(%rbp), %r8
	movq	%r14, %r9
	xorl	%ecx, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%r10d, %edx
	call	_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode.part.0
	movslq	-60(%rbp), %r10
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L694:
	xorl	%ebx, %ebx
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L722:
	movswl	24(%r12), %eax
	testw	%ax, %ax
	js	.L664
	sarl	$5, %eax
.L665:
	leaq	16(%r12), %r15
	cmpl	%r13d, %eax
	jle	.L690
	xorl	%r8d, %r8d
	movl	$7, %r9d
	movl	$7, %edx
	movl	%ebx, %esi
	leaq	_ZN6icu_67L12kOffsetColonE(%rip), %rcx
	movq	%r15, %rdi
	movl	%r10d, -96(%rbp)
	movl	%r11d, -64(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	testb	%al, %al
	je	.L667
	movl	-64(%rbp), %r11d
	movl	-96(%rbp), %r10d
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L647:
	movl	-88(%rbp), %ecx
	movl	%esi, %eax
	testl	%ecx, %ecx
	jle	.L689
	.p2align 4,,10
	.p2align 3
.L658:
	movl	-92(%rbp), %edx
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L664:
	movl	28(%r12), %eax
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L667:
	cmpb	$0, -60(%rbp)
	je	.L658
	movzwl	24(%r12), %eax
	addl	$1, %r13d
	testb	$17, %al
	jne	.L693
	leaq	26(%r12), %rbx
	testb	$2, %al
	jne	.L670
	movq	40(%r12), %rbx
.L670:
	testw	%ax, %ax
	js	.L672
	movswl	%ax, %esi
	sarl	$5, %esi
.L673:
	subl	%r13d, %esi
	movslq	%r13d, %r13
	leaq	(%rbx,%r13,2), %rdi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	movq	%r12, %rdi
	subq	%rbx, %rax
	movq	%rax, %r10
	sarq	%r10
	movl	%r10d, %esi
	call	_ZN6icu_6714MessagePattern10skipDoubleEi
	cmpl	%r10d, %eax
	je	.L658
	movl	%eax, %edx
	subl	%r10d, %edx
	cmpl	$65535, %edx
	jg	.L726
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L661
	movq	-80(%rbp), %r8
	xorl	%ecx, %ecx
	movl	%eax, %edx
	movq	%r14, %r9
	movl	%r10d, %esi
	movl	%eax, -52(%rbp)
	call	_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode.part.0
	movl	(%r14), %eax
	testl	%eax, %eax
	movl	-52(%rbp), %eax
	jle	.L675
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L668:
	movl	-52(%rbp), %edx
.L720:
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movl	$8, (%r14)
	movl	$0, -52(%rbp)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L721:
	movl	-88(%rbp), %edx
	movl	%esi, %eax
	testl	%edx, %edx
	jg	.L653
	.p2align 4,,10
	.p2align 3
.L689:
	cmpl	%eax, -52(%rbp)
	movq	80(%r12), %rax
	sete	%dl
	movq	(%rax), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	sete	%al
	cmpb	%al, %dl
	je	.L658
.L653:
	cmpb	$0, -53(%rbp)
	jne	.L639
	movq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movl	$65807, (%r14)
	movl	$0, -52(%rbp)
	jmp	.L639
.L672:
	movl	28(%r12), %esi
	jmp	.L673
.L693:
	xorl	%ebx, %ebx
	jmp	.L670
.L726:
	movl	%r10d, %edx
	jmp	.L720
	.cfi_endproc
.LFE2922:
	.size	_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode:
.LFB2370:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L728
	jmp	_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L728:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2370:
	.size	_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode:
.LFB2354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L731
	movq	%rdx, %rbx
	movq	%rcx, %r13
	testq	%rdx, %rdx
	je	.L732
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	$0, (%rdx)
	movw	%cx, 8(%rdx)
	movw	%di, 40(%rdx)
.L732:
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%eax, %eax
	movb	$0, 126(%r12)
	movw	%ax, 124(%r12)
	movl	0(%r13), %edx
	movl	$0, 96(%r12)
	movl	$0, 120(%r12)
	testl	%edx, %edx
	jg	.L731
	movq	%r13, %r9
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode.part.0
.L731:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L734
	movq	(%rax), %rax
	movq	%rax, 88(%r12)
.L734:
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L735
	movq	(%rax), %rax
	movq	%rax, 112(%r12)
.L735:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2354:
	.size	_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern16parsePluralStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern16parseSelectStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern16parseSelectStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern16parseSelectStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode:
.LFB2355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L748
	movq	%rdx, %rbx
	movq	%rcx, %r13
	testq	%rdx, %rdx
	je	.L749
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	$0, (%rdx)
	movw	%cx, 8(%rdx)
	movw	%di, 40(%rdx)
.L749:
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%eax, %eax
	movb	$0, 126(%r12)
	movw	%ax, 124(%r12)
	movl	0(%r13), %edx
	movl	$0, 96(%r12)
	movl	$0, 120(%r12)
	testl	%edx, %edx
	jg	.L748
	movq	%r13, %r9
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode.part.0
.L748:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L751
	movq	(%rax), %rax
	movq	%rax, 88(%r12)
.L751:
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L752
	movq	(%rax), %rax
	movq	%rax, 112(%r12)
.L752:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2355:
	.size	_ZN6icu_6714MessagePattern16parseSelectStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern16parseSelectStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern8parseArgEiiiP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern8parseArgEiiiP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern8parseArgEiiiP11UParseErrorR10UErrorCode:
.LFB2367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movl	%ecx, -52(%rbp)
	testl	%r10d, %r10d
	jg	.L764
	movq	80(%rdi), %rcx
	movslq	96(%rdi), %r15
	movq	%rdi, %r14
	movl	%esi, %r11d
	movl	%edx, %r12d
	movq	%r8, %r13
	movq	%r9, %rbx
	cmpl	8(%rcx), %r15d
	jl	.L765
	leal	(%r15,%r15), %r8d
	movl	%esi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	testl	%r8d, %r8d
	jle	.L766
	movslq	%r8d, %rdi
	movl	%r8d, -80(%rbp)
	salq	$4, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L766
	movq	-64(%rbp), %rcx
	testl	%r15d, %r15d
	movl	-80(%rbp), %r8d
	movl	-72(%rbp), %r11d
	movq	(%rcx), %rsi
	jg	.L885
.L767:
	cmpb	$0, 12(%rcx)
	jne	.L886
.L768:
	movb	$1, 12(%rcx)
	movslq	96(%r14), %rax
	movq	%r9, (%rcx)
	movl	(%rbx), %edx
	movl	%r8d, 8(%rcx)
	movq	80(%r14), %rcx
	leal	1(%rax), %esi
	salq	$4, %rax
	movl	%esi, 96(%r14)
	addq	(%rcx), %rax
	xorl	%ecx, %ecx
	movl	$5, (%rax)
	movl	%r11d, 4(%rax)
	movw	%r12w, 8(%rax)
	movw	%cx, 10(%rax)
	movl	$0, 12(%rax)
	testl	%edx, %edx
	jg	.L764
.L769:
	movzwl	24(%r14), %eax
	addl	%r11d, %r12d
	testb	$17, %al
	jne	.L838
	leaq	26(%r14), %rcx
	testb	$2, %al
	je	.L887
.L770:
	testw	%ax, %ax
	js	.L772
.L893:
	movswl	%ax, %esi
	sarl	$5, %esi
.L773:
	subl	%r12d, %esi
	movslq	%r12d, %r12
	movq	%rcx, -64(%rbp)
	leaq	(%rcx,%r12,2), %rdi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	movq	-64(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, %r12
	movzwl	24(%r14), %eax
	sarq	%r12
	testw	%ax, %ax
	js	.L774
	movswl	%ax, %esi
	sarl	$5, %esi
.L775:
	cmpl	%r12d, %esi
	je	.L792
	testb	$17, %al
	jne	.L839
	leaq	26(%r14), %rdx
	testb	$2, %al
	jne	.L778
	movq	40(%r14), %rdx
.L778:
	movslq	%r12d, %rax
	subl	%r12d, %esi
	movq	%rdx, -64(%rbp)
	leaq	(%rdx,%rax,2), %rdi
	call	_ZN6icu_6712PatternProps14skipIdentifierEPKDsi@PLT
	movq	-64(%rbp), %rdx
	leaq	16(%r14), %rdi
	movl	%r12d, %esi
	subq	%rdx, %rax
	movq	%rax, %r11
	sarq	%r11
	movl	%r11d, %edx
	movq	%r11, -64(%rbp)
	call	_ZN6icu_6714MessagePattern14parseArgNumberERKNS_13UnicodeStringEii
	movq	-64(%rbp), %r11
	testl	%eax, %eax
	js	.L780
	movl	%r11d, %ecx
	subl	%r12d, %ecx
	cmpl	$65535, %ecx
	jg	.L884
	cmpl	$32767, %eax
	jg	.L884
	movb	$1, 125(%r14)
	movq	%rbx, %r9
	movl	%eax, %r8d
	movl	%r12d, %edx
	movl	$7, %esi
	movq	%r14, %rdi
	movq	%r11, -64(%rbp)
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	movq	-64(%rbp), %r11
.L783:
	movzwl	24(%r14), %eax
	testb	$17, %al
	jne	.L840
	leaq	26(%r14), %rdx
	testb	$2, %al
	jne	.L786
	movq	40(%r14), %rdx
.L786:
	testw	%ax, %ax
	js	.L788
	movswl	%ax, %esi
	sarl	$5, %esi
.L789:
	subl	%r11d, %esi
	movslq	%r11d, %r11
	movq	%rdx, -64(%rbp)
	leaq	(%rdx,%r11,2), %rdi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	movq	-64(%rbp), %rdx
	subq	%rdx, %rax
	movzwl	24(%r14), %edx
	sarq	%rax
	movl	%eax, %r11d
	testw	%dx, %dx
	js	.L790
	movswl	%dx, %esi
	sarl	$5, %esi
.L791:
	cmpl	%eax, %esi
	je	.L792
	jbe	.L827
	movl	%edx, %r8d
	leaq	26(%r14), %r9
	andw	$2, %r8w
	jne	.L795
	movq	40(%r14), %r9
.L795:
	movslq	%eax, %rcx
	leaq	(%rcx,%rcx), %rdi
	movzwl	(%r9,%rcx,2), %ecx
	cmpw	$125, %cx
	je	.L888
	cmpw	$44, %cx
	jne	.L827
	addl	$1, %eax
	andl	$17, %edx
	jne	.L841
	leaq	26(%r14), %rcx
	testw	%r8w, %r8w
	jne	.L799
	movq	40(%r14), %rcx
.L799:
	subl	%eax, %esi
	leaq	2(%rcx,%rdi), %rdi
	movq	%rcx, -64(%rbp)
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	movswl	24(%r14), %esi
	movq	-64(%rbp), %rcx
	movl	%esi, %r9d
	subq	%rcx, %rax
	sarl	$5, %esi
	movq	%rax, %rdx
	movl	%r9d, %eax
	sarq	%rdx
	andl	$2, %eax
	testw	%r9w, %r9w
	js	.L889
	testw	%ax, %ax
	jne	.L807
	movslq	%edx, %rcx
	movl	%edx, %edi
	addq	%rcx, %rcx
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L890:
	movq	40(%r14), %rax
	movzwl	(%rax,%rcx), %eax
	addq	$2, %rcx
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$25, %ax
	ja	.L803
	addl	$1, %edi
.L808:
	cmpl	%edi, %esi
	jle	.L803
	ja	.L890
.L803:
	movl	%edi, %ecx
	xorl	%r8d, %r8d
	subl	%edx, %ecx
	andl	$17, %r9d
	jne	.L810
	movq	40(%r14), %r8
.L810:
	subl	%edi, %esi
	movslq	%edi, %rdi
	movq	%rdx, -80(%rbp)
	leaq	(%r8,%rdi,2), %rdi
	movl	%ecx, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	movq	-64(%rbp), %r8
	movzwl	24(%r14), %esi
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	subq	%r8, %rax
	sarq	%rax
	testw	%si, %si
	movq	%rax, %r10
	movl	%eax, %r11d
	js	.L811
	movswl	%si, %eax
	sarl	$5, %eax
.L812:
	cmpl	%r10d, %eax
	je	.L792
	testl	%ecx, %ecx
	je	.L827
	cmpl	%r10d, %eax
	jbe	.L827
	andl	$2, %esi
	leaq	26(%r14), %rax
	jne	.L815
	movq	40(%r14), %rax
.L815:
	movslq	%r10d, %rsi
	movzwl	(%rax,%rsi,2), %eax
	movw	%ax, -64(%rbp)
	cmpw	$44, %ax
	je	.L816
	cmpw	$125, %ax
	jne	.L827
.L816:
	cmpl	$65535, %ecx
	jg	.L884
	movq	80(%r14), %rax
	salq	$4, %r15
	movq	(%rax), %r9
	addq	%r15, %r9
	cmpl	$6, %ecx
	je	.L891
	cmpl	$13, %ecx
	je	.L892
.L823:
	movl	$1, %r12d
	movw	%r12w, 10(%r9)
.L825:
	xorl	%r8d, %r8d
	movq	%rbx, %r9
	movl	$9, %esi
	movq	%r14, %rdi
	movq	%r10, -72(%rbp)
	movl	%r11d, -52(%rbp)
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	movq	-72(%rbp), %r10
	cmpw	$125, -64(%rbp)
	movl	-52(%rbp), %r11d
	leal	1(%r10), %eax
	je	.L882
	movl	(%rbx), %esi
	testl	%esi, %esi
	jle	.L833
	movq	80(%r14), %rax
	movl	$1, %r12d
	xorl	%r11d, %r11d
	addq	(%rax), %r15
	movl	$1, %eax
	movq	%r15, %r9
	.p2align 4,,10
	.p2align 3
.L797:
	movl	96(%r14), %edx
	movq	%rbx, %r8
	movl	%r12d, %ecx
	movl	$6, %esi
	movq	%r14, %rdi
	movl	%eax, -52(%rbp)
	movl	%edx, 12(%r9)
	movl	%r11d, %edx
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0
	movl	-52(%rbp), %eax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L766:
	movl	$7, (%rbx)
.L764:
	xorl	%eax, %eax
.L763:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	leal	1(%r15), %eax
	movl	%eax, 96(%rdi)
	movslq	%r15d, %rax
	salq	$4, %rax
	addq	(%rcx), %rax
	movw	%dx, 8(%rax)
	xorl	%edx, %edx
	movl	$5, (%rax)
	movl	%esi, 4(%rax)
	movw	%dx, 10(%rax)
	movl	$0, 12(%rax)
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L887:
	movq	40(%r14), %rcx
	testw	%ax, %ax
	jns	.L893
.L772:
	movl	28(%r14), %esi
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L780:
	cmpl	$-1, %eax
	jne	.L827
	movl	%r11d, %ecx
	subl	%r12d, %ecx
	cmpl	$65535, %ecx
	jg	.L884
	movb	$1, 124(%r14)
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movl	%r12d, %edx
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%r11, -64(%rbp)
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	movq	-64(%rbp), %r11
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L885:
	cmpl	%r8d, 8(%rcx)
	movl	%r8d, %eax
	cmovle	8(%rcx), %eax
	movq	%r9, %rdi
	movl	%r11d, -80(%rbp)
	cmpl	%r15d, %eax
	movl	%r8d, -72(%rbp)
	cmovg	%r15d, %eax
	movslq	%eax, %rdx
	salq	$4, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	movl	-80(%rbp), %r11d
	movl	-72(%rbp), %r8d
	movq	%rax, %r9
	movq	(%rcx), %rsi
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L886:
	movq	%rsi, %rdi
	movl	%r11d, -56(%rbp)
	movq	%r9, -80(%rbp)
	movl	%r8d, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	uprv_free_67@PLT
	movl	-56(%rbp), %r11d
	movq	-80(%rbp), %r9
	movl	-72(%rbp), %r8d
	movq	-64(%rbp), %rcx
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L774:
	movl	28(%r14), %esi
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L827:
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movl	$65799, (%rbx)
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movl	$65801, (%rbx)
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L884:
	.cfi_restore_state
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movl	$8, (%rbx)
	xorl	%eax, %eax
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L790:
	movl	28(%r14), %esi
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L788:
	movl	28(%r14), %esi
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L838:
	xorl	%ecx, %ecx
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L839:
	xorl	%edx, %edx
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L841:
	xorl	%ecx, %ecx
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L888:
	movq	80(%r14), %rdx
	salq	$4, %r15
	addl	$1, %eax
	xorl	%r12d, %r12d
	addq	(%rdx), %r15
	movq	%r15, %r9
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L840:
	xorl	%edx, %edx
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L889:
	movl	28(%r14), %esi
	testw	%ax, %ax
	jne	.L802
	movslq	%edx, %rcx
	movl	%edx, %edi
	addq	%rcx, %rcx
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L894:
	movq	40(%r14), %rax
	movzwl	(%rax,%rcx), %eax
	addq	$2, %rcx
	andl	$-33, %eax
	subl	$65, %eax
	cmpw	$25, %ax
	ja	.L803
	addl	$1, %edi
.L804:
	cmpl	%esi, %edi
	jge	.L803
	jb	.L894
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L811:
	movl	28(%r14), %eax
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L807:
	movslq	%edx, %rax
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L895:
	movzwl	26(%r14,%rax,2), %ecx
	addq	$1, %rax
	andl	$-33, %ecx
	subl	$65, %ecx
	cmpw	$25, %cx
	ja	.L805
.L809:
	movl	%eax, %edi
	cmpl	%eax, %esi
	jle	.L805
	cmpl	%esi, %eax
	jb	.L895
.L805:
	movl	%edi, %ecx
	leaq	26(%r14), %r8
	subl	%edx, %ecx
	andb	$17, %r9b
	je	.L810
	xorl	%r8d, %r8d
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L802:
	movslq	%edx, %rax
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L896:
	movzwl	26(%r14,%rax,2), %ecx
	addq	$1, %rax
	andl	$-33, %ecx
	subl	$65, %ecx
	cmpw	$25, %cx
	ja	.L805
.L806:
	movl	%eax, %edi
	cmpl	%eax, %esi
	jle	.L805
	cmpl	%esi, %eax
	jb	.L896
	jmp	.L805
.L891:
	movl	%edx, %esi
	movq	%r14, %rdi
	movq	%r10, -72(%rbp)
	movl	%ecx, -88(%rbp)
	movl	%r11d, -56(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN6icu_6714MessagePattern8isChoiceEi
	movq	-72(%rbp), %r10
	testb	%al, %al
	jne	.L819
	movq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movl	%edx, %esi
	call	_ZN6icu_6714MessagePattern8isPluralEi
	movq	-72(%rbp), %r10
	testb	%al, %al
	jne	.L820
	movq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r10, -96(%rbp)
	movl	%edx, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN6icu_6714MessagePattern8isSelectEi
	movq	-96(%rbp), %r10
	testb	%al, %al
	jne	.L821
	movq	-72(%rbp), %rdx
	movl	-56(%rbp), %r11d
	movw	$1, 10(%r9)
	movl	-88(%rbp), %ecx
	jmp	.L825
.L892:
	movl	%edx, %esi
	movq	%r14, %rdi
	movq	%r10, -88(%rbp)
	movl	%ecx, -56(%rbp)
	movl	%r11d, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN6icu_6714MessagePattern8isSelectEi
	movq	-72(%rbp), %rdx
	movl	-80(%rbp), %r11d
	testb	%al, %al
	movl	-56(%rbp), %ecx
	movq	-88(%rbp), %r10
	je	.L823
	leal	6(%rdx), %esi
	movq	%r14, %rdi
	movq	%r10, -72(%rbp)
	movl	%ecx, -88(%rbp)
	movl	%r11d, -56(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN6icu_6714MessagePattern9isOrdinalEi
	movq	-72(%rbp), %r10
	testb	%al, %al
	jne	.L826
	movl	$1, %eax
	movq	-80(%rbp), %rdx
	movl	-56(%rbp), %r11d
	movw	%ax, 10(%r9)
	movl	-88(%rbp), %ecx
	jmp	.L825
.L819:
	movl	$2, %eax
	cmpw	$125, -64(%rbp)
	movw	%ax, 10(%r9)
	je	.L827
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L897
	movl	$1, %eax
	movl	$2, %r12d
	xorl	%r11d, %r11d
	jmp	.L797
.L820:
	movl	$3, %r8d
	cmpw	$125, -64(%rbp)
	movw	%r8w, 10(%r9)
	je	.L827
	leal	1(%r10), %edx
	movl	$3, %r12d
	movl	$3, %esi
.L835:
	movl	(%rbx), %edi
	testl	%edi, %edi
	jg	.L842
	movl	-52(%rbp), %ecx
	movq	%rbx, %r9
	movq	%r13, %r8
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern24parsePluralOrSelectStyleE22UMessagePatternArgTypeiiP11UParseErrorR10UErrorCode.part.0
	movq	80(%r14), %rdx
	movl	%eax, %r11d
	leal	1(%rax), %eax
	addq	(%rdx), %r15
	movq	%r15, %r9
	jmp	.L797
.L842:
	movl	$1, %eax
	xorl	%r11d, %r11d
	jmp	.L797
.L897:
	movl	-52(%rbp), %edx
	leal	1(%r10), %esi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	movl	$2, %r12d
	call	_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode.part.0
	movq	80(%r14), %rdx
	movl	%eax, %r11d
	leal	1(%rax), %eax
	addq	(%rdx), %r15
	movq	%r15, %r9
	jmp	.L797
.L833:
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movl	%eax, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern16parseSimpleStyleEiP11UParseErrorR10UErrorCode.part.0
	movl	%eax, %r11d
	leal	1(%rax), %eax
.L882:
	movq	80(%r14), %rdx
	movl	$1, %r12d
	addq	(%rdx), %r15
	movq	%r15, %r9
	jmp	.L797
.L826:
	movl	$5, %eax
	cmpw	$125, -64(%rbp)
	movw	%ax, 10(%r9)
	je	.L827
	leal	1(%r10), %edx
	movl	$5, %r12d
	movl	$5, %esi
	jmp	.L835
.L821:
	cmpw	$125, -64(%rbp)
	movw	$4, 10(%r9)
	je	.L827
	leal	1(%r10), %edx
	movl	$4, %r12d
	movl	$4, %esi
	jmp	.L835
	.cfi_endproc
.LFE2367:
	.size	_ZN6icu_6714MessagePattern8parseArgEiiiP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern8parseArgEiiiP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode.part.0:
.LFB2921:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	addl	%r13d, %r15d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movl	96(%rdi), %edi
	movl	%r8d, -56(%rbp)
	movq	%r9, -64(%rbp)
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	%esi, %edx
	movl	%edi, -68(%rbp)
	movq	%rbx, %r9
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L976
	movl	-56(%rbp), %eax
	movzwl	24(%r14), %edx
	subl	$3, %eax
	movl	%eax, -52(%rbp)
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L981:
	movswl	%dx, %eax
	sarl	$5, %eax
	cmpl	%r15d, %eax
	jle	.L904
.L982:
	leal	1(%r15), %r13d
	jbe	.L905
	leaq	26(%r14), %rsi
	testb	$2, %dl
	jne	.L907
	movq	40(%r14), %rsi
.L907:
	movslq	%r15d, %rcx
	leaq	(%rcx,%rcx), %rdi
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	$39, %cx
	je	.L977
	testl	$-3, -52(%rbp)
	jne	.L930
	cmpw	$35, %cx
	je	.L978
.L930:
	cmpw	$123, %cx
	je	.L979
	testl	%r12d, %r12d
	jle	.L941
	cmpw	$125, %cx
	je	.L932
.L941:
	cmpw	$124, %cx
	jne	.L905
	cmpl	$2, -56(%rbp)
	je	.L980
	.p2align 4,,10
	.p2align 3
.L905:
	movl	%r13d, %r15d
.L899:
	testw	%dx, %dx
	jns	.L981
	movl	28(%r14), %eax
	cmpl	%r15d, %eax
	jg	.L982
.L904:
	testl	%r12d, %r12d
	jle	.L983
	cmpl	$1, %r12d
	jne	.L938
	cmpl	$2, -56(%rbp)
	jne	.L938
	movq	80(%r14), %rax
	movq	(%rax), %rdx
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L938
.L937:
	movslq	-68(%rbp), %rax
	movl	96(%r14), %ecx
	movq	%rbx, %r9
	movl	%r12d, %r8d
	movl	$1, %esi
	movq	%r14, %rdi
	salq	$4, %rax
	movl	%ecx, 12(%rdx,%rax)
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L977:
	cmpl	%r13d, %eax
	je	.L915
	jbe	.L911
	movzwl	2(%rsi,%rdi), %eax
	cmpw	$39, %ax
	je	.L984
	cmpl	$1, 8(%r14)
	je	.L913
	leal	-123(%rax), %edx
	testw	$-3, %dx
	je	.L913
	cmpl	$2, -56(%rbp)
	jne	.L940
	cmpw	$124, %ax
	jne	.L940
.L913:
	movl	%r15d, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movl	$2, %esi
	movq	%r14, %rdi
	leaq	16(%r14), %r15
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0
	movl	%r13d, %r11d
	movswl	24(%r14), %ecx
	xorl	%edx, %edx
	addl	$1, %r11d
	js	.L916
	testw	%cx, %cx
	js	.L917
.L985:
	movswl	%cx, %edx
	sarl	$5, %edx
.L918:
	cmpl	%edx, %r11d
	cmovle	%r11d, %edx
.L916:
	testw	%cx, %cx
	js	.L919
	sarl	$5, %ecx
.L920:
	subl	%edx, %ecx
	movl	$39, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L921
	leal	1(%rax), %r13d
	movzwl	24(%r14), %eax
	testw	%ax, %ax
	js	.L922
	movswl	%ax, %ecx
	sarl	$5, %ecx
	cmpl	%r13d, %ecx
	jbe	.L924
.L986:
	leaq	26(%r14), %rcx
	testb	$2, %al
	jne	.L926
	movq	40(%r14), %rcx
.L926:
	movslq	%r13d, %rax
	cmpw	$39, (%rcx,%rax,2)
	jne	.L924
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movq	%rbx, %r8
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0
	movl	%r13d, %r11d
	movswl	24(%r14), %ecx
	xorl	%edx, %edx
	addl	$1, %r11d
	js	.L916
	testw	%cx, %cx
	jns	.L985
.L917:
	movl	28(%r14), %edx
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L940:
	testl	$-3, -52(%rbp)
	jne	.L915
	cmpw	$35, %ax
	je	.L913
	.p2align 4,,10
	.p2align 3
.L915:
	movq	%rbx, %r9
	movl	$39, %r8d
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	movb	$1, 126(%r14)
	movl	(%rbx), %eax
.L910:
	testl	%eax, %eax
	jg	.L976
.L987:
	movzwl	24(%r14), %edx
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L919:
	movl	28(%r14), %ecx
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L922:
	movl	28(%r14), %ecx
	cmpl	%r13d, %ecx
	ja	.L986
.L924:
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L987
	.p2align 4,,10
	.p2align 3
.L976:
	xorl	%r15d, %r15d
.L898:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L979:
	.cfi_restore_state
	movq	-64(%rbp), %r8
	movq	%rbx, %r9
	movl	%r12d, %ecx
	movl	$1, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern8parseArgEiiiP11UParseErrorR10UErrorCode
	movl	%eax, %r13d
	movl	(%rbx), %eax
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L978:
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	movl	$4, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0
	movl	(%rbx), %eax
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L911:
	cmpl	$1, 8(%r14)
	jne	.L915
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L984:
	movl	%r13d, %edx
	addl	$2, %r15d
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	$2, %esi
	movq	%r14, %rdi
	movl	%r15d, %r13d
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0
	movl	(%rbx), %eax
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L932:
	movq	80(%r14), %rdx
	movslq	-68(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%rbx, %r9
	movl	%r12d, %r8d
	movl	$1, %esi
	movq	%r14, %rdi
	salq	$4, %rax
	addq	(%rdx), %rax
	movl	96(%r14), %edx
	cmpl	$2, -56(%rbp)
	movl	%edx, 12(%rax)
	setne	%cl
	movl	%r15d, %edx
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	cmpl	$2, -56(%rbp)
	cmovne	%r13d, %r15d
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L938:
	movq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movl	$65801, (%rbx)
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L921:
	movzwl	24(%r14), %eax
	testw	%ax, %ax
	js	.L928
	movswl	%ax, %r11d
	sarl	$5, %r11d
	movl	%r11d, %r13d
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L980:
	movq	80(%r14), %rdx
	movslq	-68(%rbp), %rax
	movq	%rbx, %r9
	movl	%r12d, %r8d
	movl	$1, %ecx
	movl	$1, %esi
	movq	%r14, %rdi
	salq	$4, %rax
	addq	(%rdx), %rax
	movl	96(%r14), %edx
	movl	%edx, 12(%rax)
	movl	%r15d, %edx
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode
	jmp	.L898
.L983:
	movq	80(%r14), %rax
	movq	(%rax), %rdx
	jmp	.L937
.L928:
	movl	28(%r14), %r13d
	jmp	.L915
	.cfi_endproc
.LFE2921:
	.size	_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode:
.LFB2366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L988
	cmpl	$32767, %ecx
	jle	.L990
	movl	$8, (%rax)
.L988:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L990:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode.part.0
	.cfi_endproc
.LFE2366:
	.size	_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode:
.LFB2352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rcx), %r9d
	testl	%r9d, %r9d
	jg	.L995
	movq	%rdx, %rbx
	movq	%rcx, %r13
	testq	%rdx, %rdx
	je	.L996
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movq	$0, (%rdx)
	movw	%di, 8(%rdx)
	movw	%r8w, 40(%rdx)
.L996:
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%ecx, %ecx
	movb	$0, 126(%r12)
	movw	%cx, 124(%r12)
	movl	0(%r13), %esi
	movl	$0, 96(%r12)
	movl	$0, 120(%r12)
	testl	%esi, %esi
	jg	.L995
	subq	$8, %rsp
	xorl	%edx, %edx
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	pushq	%r13
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode.part.0
	popq	%rax
	popq	%rdx
.L995:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L998
	movq	(%rax), %rax
	movq	%rax, 88(%r12)
.L998:
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L999
	movq	(%rax), %rax
	movq	%rax, 112(%r12)
.L999:
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2352:
	.size	_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern5parseERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode.part.0, @function
_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode.part.0:
.LFB2920:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movzwl	24(%rdi), %eax
	movl	%esi, -56(%rbp)
	movl	%edx, -60(%rbp)
	testb	$17, %al
	jne	.L1044
	leaq	26(%rdi), %r13
	testb	$2, %al
	je	.L1053
.L1011:
	testw	%ax, %ax
	js	.L1013
.L1057:
	movswl	%ax, %esi
	sarl	$5, %esi
.L1014:
	movslq	-56(%rbp), %rax
	subl	%eax, %esi
	leaq	0(%r13,%rax,2), %rdi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	movzwl	24(%r15), %edx
	subq	%r13, %rax
	sarq	%rax
	movl	%eax, %r11d
	testw	%dx, %dx
	js	.L1015
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L1016:
	cmpl	%eax, %ecx
	je	.L1017
	jbe	.L1018
	andl	$2, %edx
	leaq	26(%r15), %rdx
	je	.L1054
	cltq
	cmpw	$125, (%rdx,%rax,2)
	je	.L1017
.L1018:
	movl	-60(%rbp), %eax
	leal	1(%rax), %r13d
	.p2align 4,,10
	.p2align 3
.L1021:
	movl	%r11d, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern10skipDoubleEi
	movl	%eax, %edx
	subl	%r11d, %edx
	je	.L1032
	cmpl	$65535, %edx
	jg	.L1055
	movl	(%rbx), %r14d
	testl	%r14d, %r14d
	jg	.L1025
	movq	%rbx, %r9
	movq	%r12, %r8
	movl	$1, %ecx
	movl	%eax, %edx
	movl	%r11d, %esi
	movl	%eax, -52(%rbp)
	call	_ZN6icu_6714MessagePattern11parseDoubleEiiaP11UParseErrorR10UErrorCode.part.0
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L1025
	movswl	24(%r15), %esi
	movslq	-52(%rbp), %rax
	testb	$17, %sil
	jne	.L1045
	leaq	26(%r15), %r14
	testb	$2, %sil
	jne	.L1026
	movq	40(%r15), %r14
.L1026:
	testw	%si, %si
	js	.L1028
	sarl	$5, %esi
.L1029:
	subl	%eax, %esi
	leaq	(%r14,%rax,2), %rdi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	movzwl	24(%r15), %edx
	subq	%r14, %rax
	movq	%rax, %rsi
	sarq	%rsi
	movq	%rsi, %r14
	testw	%dx, %dx
	js	.L1030
	movswl	%dx, %ecx
	sarl	$5, %ecx
.L1031:
	cmpl	%r14d, %ecx
	je	.L1032
	jbe	.L1032
	andl	$2, %edx
	leaq	26(%r15), %rdx
	jne	.L1034
	movq	40(%r15), %rdx
.L1034:
	movslq	%r14d, %rcx
	movzwl	(%rdx,%rcx,2), %edx
	cmpw	$35, %dx
	setne	%sil
	cmpw	$60, %dx
	setne	%cl
	testb	%cl, %sil
	je	.L1047
	cmpw	$8804, %dx
	jne	.L1032
.L1047:
	movl	$11, %esi
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern7addPartE23UMessagePatternPartTypeiiiR10UErrorCode.constprop.0
	movl	(%rbx), %r9d
	leal	1(%r14), %esi
	testl	%r9d, %r9d
	jg	.L1025
	cmpl	$32767, %r13d
	jle	.L1036
	movl	$8, (%rbx)
.L1025:
	xorl	%eax, %eax
.L1010:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1036:
	.cfi_restore_state
	subq	$8, %rsp
	movl	$2, %r8d
	movq	%r15, %rdi
	movq	%r12, %r9
	pushq	%rbx
	movl	%r13d, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode.part.0
	movl	(%rbx), %r8d
	popq	%rsi
	popq	%rdi
	testl	%r8d, %r8d
	jg	.L1025
	movzwl	24(%r15), %edx
	testw	%dx, %dx
	js	.L1037
	movswl	%dx, %esi
	sarl	$5, %esi
.L1038:
	cmpl	%eax, %esi
	je	.L1010
	jbe	.L1039
	leaq	26(%r15), %rcx
	testb	$2, %dl
	jne	.L1041
	movq	40(%r15), %rcx
.L1041:
	movslq	%eax, %rdi
	cmpw	$125, (%rcx,%rdi,2)
	je	.L1056
.L1039:
	addl	$1, %eax
	testb	$17, %dl
	jne	.L1046
	andl	$2, %edx
	leaq	26(%r15), %r14
	jne	.L1042
	movq	40(%r15), %r14
.L1042:
	subl	%eax, %esi
	cltq
	leaq	(%r14,%rax,2), %rdi
	call	_ZN6icu_6712PatternProps14skipWhiteSpaceEPKDsi@PLT
	subq	%r14, %rax
	movq	%rax, %r11
	sarq	%r11
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1030:
	movl	28(%r15), %ecx
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1028:
	movl	28(%r15), %esi
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1037:
	movl	28(%r15), %esi
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1045:
	xorl	%r14d, %r14d
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1046:
	xorl	%r14d, %r14d
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	40(%rdi), %r13
	testw	%ax, %ax
	jns	.L1057
.L1013:
	movl	28(%r15), %esi
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	40(%r15), %rdx
	cltq
	cmpw	$125, (%rdx,%rax,2)
	jne	.L1018
.L1017:
	xorl	%edx, %edx
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1015:
	movl	28(%r15), %ecx
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1056:
	movl	-60(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L1010
	movq	80(%r15), %rdx
	movq	(%rdx), %rdx
	movl	(%rdx), %edx
	testl	%edx, %edx
	je	.L1010
	.p2align 4,,10
	.p2align 3
.L1032:
	movl	-56(%rbp), %edx
.L1052:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movl	$65799, (%rbx)
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1044:
	.cfi_restore_state
	xorl	%r13d, %r13d
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	%r12, %rsi
	movl	%r11d, %edx
	call	_ZN6icu_6714MessagePattern13setParseErrorEP11UParseErrori
	movl	$8, (%rbx)
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2920:
	.size	_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode.part.0, .-_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode:
.LFB2369:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L1060
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1060:
	jmp	_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode.part.0
	.cfi_endproc
.LFE2369:
	.size	_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePattern16parseChoiceStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePattern16parseChoiceStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePattern16parseChoiceStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode:
.LFB2353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L1063
	movq	%rdx, %r13
	movq	%rcx, %rbx
	testq	%rdx, %rdx
	je	.L1064
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	$0, (%rdx)
	movw	%cx, 8(%rdx)
	movw	%di, 40(%rdx)
.L1064:
	leaq	16(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%eax, %eax
	movb	$0, 126(%r12)
	movw	%ax, 124(%r12)
	movl	(%rbx), %edx
	movl	$0, 96(%r12)
	movl	$0, 120(%r12)
	testl	%edx, %edx
	jg	.L1063
	movq	%rbx, %r8
	movq	%r13, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6714MessagePattern16parseChoiceStyleEiiP11UParseErrorR10UErrorCode.part.0
.L1063:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L1066
	movq	(%rax), %rax
	movq	%rax, 88(%r12)
.L1066:
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L1067
	movq	(%rax), %rax
	movq	%rax, 112(%r12)
.L1067:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2353:
	.size	_ZN6icu_6714MessagePattern16parseChoiceStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePattern16parseChoiceStyleERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714MessagePatternC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.type	_ZN6icu_6714MessagePatternC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, @function
_ZN6icu_6714MessagePatternC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode:
.LFB2319:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714MessagePatternE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$2, %r11d
	movl	$0, 8(%rdi)
	movq	%rax, (%rdi)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, 16(%rdi)
	xorl	%eax, %eax
	movw	%ax, 124(%rdi)
	movl	(%rcx), %eax
	movw	%r11w, 24(%rdi)
	movl	$0, 96(%rdi)
	movl	$0, 120(%rdi)
	movb	$0, 126(%rdi)
	movups	%xmm0, 80(%rdi)
	movups	%xmm0, 104(%rdi)
	testl	%eax, %eax
	jle	.L1101
	ret
	.p2align 4,,10
	.p2align 3
.L1101:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$528, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L1081
	movb	$0, 12(%rax)
	movl	(%r12), %r10d
	leaq	16(%rax), %rdx
	movq	%rdx, (%rax)
	movl	$32, 8(%rax)
	movq	%rax, 80(%rbx)
	movq	%rdx, 88(%rbx)
	testl	%r10d, %r10d
	jg	.L1082
	testq	%r13, %r13
	je	.L1083
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	$0, 0(%r13)
	movw	%r8w, 8(%r13)
	movw	%r9w, 40(%r13)
.L1083:
	leaq	16(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	(%r12), %edi
	xorl	%esi, %esi
	movb	$0, 126(%rbx)
	movl	$0, 96(%rbx)
	movl	$0, 120(%rbx)
	movw	%si, 124(%rbx)
	testl	%edi, %edi
	jg	.L1102
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %r9
	pushq	%r12
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6714MessagePattern12parseMessageEiii22UMessagePatternArgTypeP11UParseErrorR10UErrorCode.part.0
	movq	80(%rbx), %rax
	popq	%rdx
	popq	%rcx
.L1085:
	testq	%rax, %rax
	jne	.L1082
.L1086:
	movq	104(%rbx), %rax
	testq	%rax, %rax
	je	.L1078
	movq	(%rax), %rax
	movq	%rax, 112(%rbx)
.L1078:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1082:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	%rax, 88(%rbx)
	jmp	.L1086
.L1081:
	movq	$0, 80(%rbx)
	movl	$7, (%r12)
	jmp	.L1078
.L1102:
	movq	80(%rbx), %rax
	jmp	.L1085
	.cfi_endproc
.LFE2319:
	.size	_ZN6icu_6714MessagePatternC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode, .-_ZN6icu_6714MessagePatternC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.globl	_ZN6icu_6714MessagePatternC1ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.set	_ZN6icu_6714MessagePatternC1ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode,_ZN6icu_6714MessagePatternC2ERKNS_13UnicodeStringEP11UParseErrorR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MessageImpl24appendReducedApostrophesERKNS_13UnicodeStringEiiRS1_
	.type	_ZN6icu_6711MessageImpl24appendReducedApostrophesERKNS_13UnicodeStringEiiRS1_, @function
_ZN6icu_6711MessageImpl24appendReducedApostrophesERKNS_13UnicodeStringEiiRS1_:
.LFB2387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$-1, %ebx
	subq	$40, %rsp
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L1104:
	movswl	8(%r15), %ecx
	xorl	%edx, %edx
	testl	%r14d, %r14d
	js	.L1105
	testw	%cx, %cx
	js	.L1106
.L1120:
	movswl	%cx, %edx
	sarl	$5, %edx
.L1107:
	cmpl	%edx, %r14d
	cmovle	%r14d, %edx
.L1105:
	testw	%cx, %cx
	js	.L1108
	sarl	$5, %ecx
.L1109:
	subl	%edx, %ecx
	movl	$39, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	js	.L1116
	cmpl	%eax, -68(%rbp)
	jle	.L1116
	cmpl	%eax, %ebx
	je	.L1119
	movl	%eax, %ecx
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	subl	%r14d, %ecx
	leal	1(%r12), %r14d
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	%r14d, %ebx
	movswl	8(%r15), %ecx
	xorl	%edx, %edx
	testl	%r14d, %r14d
	js	.L1105
	testw	%cx, %cx
	jns	.L1120
.L1106:
	movl	12(%r15), %edx
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1108:
	movl	12(%r15), %ecx
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1119:
	movl	$39, %eax
	leaq	-58(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movw	%ax, -58(%rbp)
	addl	$1, %r14d
	movl	$-1, %ebx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1116:
	movl	-68(%rbp), %ecx
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	subl	%r14d, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1121
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1121:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2387:
	.size	_ZN6icu_6711MessageImpl24appendReducedApostrophesERKNS_13UnicodeStringEiiRS1_, .-_ZN6icu_6711MessageImpl24appendReducedApostrophesERKNS_13UnicodeStringEiiRS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711MessageImpl33appendSubMessageWithoutSkipSyntaxERKNS_14MessagePatternEiRNS_13UnicodeStringE
	.type	_ZN6icu_6711MessageImpl33appendSubMessageWithoutSkipSyntaxERKNS_14MessagePatternEiRNS_13UnicodeStringE, @function
_ZN6icu_6711MessageImpl33appendSubMessageWithoutSkipSyntaxERKNS_14MessagePatternEiRNS_13UnicodeStringE:
.LFB2388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	leaq	16(%rdi), %r9
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leal	1(%rsi), %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	salq	$4, %rbx
	subq	$40, %rsp
	movq	88(%rdi), %rcx
	movzwl	8(%rcx,%rbx), %r14d
	addl	4(%rcx,%rbx), %r14d
	addq	$16, %rbx
	leaq	(%rcx,%rbx), %r13
	movl	0(%r13), %eax
	movl	4(%r13), %r15d
	cmpl	$1, %eax
	jne	.L1124
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1125:
	cmpl	$5, %eax
	je	.L1133
.L1126:
	addl	$1, %r12d
	movslq	%r12d, %rbx
	salq	$4, %rbx
	leaq	(%rcx,%rbx), %r13
	movl	0(%r13), %eax
	movl	4(%r13), %r15d
	cmpl	$1, %eax
	je	.L1123
.L1124:
	cmpl	$2, %eax
	jne	.L1125
	movl	%r15d, %ecx
	movl	%r14d, %edx
	movq	%r9, %rsi
	movq	%r8, %rdi
	subl	%r14d, %ecx
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-72(%rbp), %r10
	movzwl	8(%r13), %r14d
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	88(%r10), %rcx
	addl	4(%r13), %r14d
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1133:
	movl	%r15d, %ecx
	movl	%r14d, %edx
	movq	%r9, %rsi
	movq	%r8, %rdi
	subl	%r14d, %ecx
	movq	%r10, -64(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r8
	movl	%r15d, %esi
	movq	-80(%rbp), %r9
	movq	88(%r10), %rcx
	movq	%r10, -72(%rbp)
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movl	12(%rcx,%rbx), %eax
	movq	%r9, -56(%rbp)
	cmpl	%eax, %r12d
	cmovl	%eax, %r12d
	movslq	%r12d, %rax
	salq	$4, %rax
	movzwl	8(%rcx,%rax), %r14d
	addl	4(%rcx,%rax), %r14d
	movq	%r8, %rcx
	movl	%r14d, %edx
	call	_ZN6icu_6711MessageImpl24appendReducedApostrophesERKNS_13UnicodeStringEiiRS1_
	movq	-72(%rbp), %r10
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	88(%r10), %rcx
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1123:
	addq	$40, %rsp
	movl	%r15d, %ecx
	movl	%r14d, %edx
	movq	%r9, %rsi
	popq	%rbx
	subl	%r14d, %ecx
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	.cfi_endproc
.LFE2388:
	.size	_ZN6icu_6711MessageImpl33appendSubMessageWithoutSkipSyntaxERKNS_14MessagePatternEiRNS_13UnicodeStringE, .-_ZN6icu_6711MessageImpl33appendSubMessageWithoutSkipSyntaxERKNS_14MessagePatternEiRNS_13UnicodeStringE
	.weak	_ZTSN6icu_6714MessagePatternE
	.section	.rodata._ZTSN6icu_6714MessagePatternE,"aG",@progbits,_ZTSN6icu_6714MessagePatternE,comdat
	.align 16
	.type	_ZTSN6icu_6714MessagePatternE, @object
	.size	_ZTSN6icu_6714MessagePatternE, 26
_ZTSN6icu_6714MessagePatternE:
	.string	"N6icu_6714MessagePatternE"
	.weak	_ZTIN6icu_6714MessagePatternE
	.section	.data.rel.ro._ZTIN6icu_6714MessagePatternE,"awG",@progbits,_ZTIN6icu_6714MessagePatternE,comdat
	.align 8
	.type	_ZTIN6icu_6714MessagePatternE, @object
	.size	_ZTIN6icu_6714MessagePatternE, 24
_ZTIN6icu_6714MessagePatternE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714MessagePatternE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6714MessagePatternE
	.section	.data.rel.ro._ZTVN6icu_6714MessagePatternE,"awG",@progbits,_ZTVN6icu_6714MessagePatternE,comdat
	.align 8
	.type	_ZTVN6icu_6714MessagePatternE, @object
	.size	_ZTVN6icu_6714MessagePatternE, 40
_ZTVN6icu_6714MessagePatternE:
	.quad	0
	.quad	_ZTIN6icu_6714MessagePatternE
	.quad	_ZN6icu_6714MessagePatternD1Ev
	.quad	_ZN6icu_6714MessagePatternD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.section	.rodata
	.align 8
	.type	_ZN6icu_67L6kOtherE, @object
	.size	_ZN6icu_67L6kOtherE, 10
_ZN6icu_67L6kOtherE:
	.value	111
	.value	116
	.value	104
	.value	101
	.value	114
	.align 8
	.type	_ZN6icu_67L12kOffsetColonE, @object
	.size	_ZN6icu_67L12kOffsetColonE, 14
_ZN6icu_67L12kOffsetColonE:
	.value	111
	.value	102
	.value	102
	.value	115
	.value	101
	.value	116
	.value	58
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1409286144
	.long	-1046646988
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
