	.file	"ucnvisci.cpp"
	.text
	.p2align 4
	.type	_ISCIIgetName, @function
_ISCIIgetName:
.LFB2117:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	leaq	26(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %rax
	ret
	.cfi_endproc
.LFE2117:
	.size	_ISCIIgetName, .-_ISCIIgetName
	.p2align 4
	.type	_ISCIIReset, @function
_ISCIIReset:
.LFB2118:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movzwl	4(%rax), %ecx
	movl	20(%rax), %edx
	cmpl	$1, %esi
	jle	.L7
.L9:
	movl	$0, 84(%rdi)
	movl	$1, %r8d
	xorl	%edi, %edi
	movw	%di, 2(%rax)
	movl	%edx, 12(%rax)
	movw	%cx, 6(%rax)
	movw	%r8w, 24(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$-2, %esi
	movq	$65535, 72(%rdi)
	movw	%cx, 8(%rax)
	movl	%edx, 16(%rax)
	movw	%si, (%rax)
	movl	$0, 44(%rax)
	jne	.L9
	ret
	.cfi_endproc
.LFE2118:
	.size	_ISCIIReset, .-_ISCIIReset
	.p2align 4
	.type	UConverter_fromUnicode_ISCII_OFFSETS_LOGIC, @function
UConverter_fromUnicode_ISCII_OFFSETS_LOGIC:
.LFB2119:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r11
	movq	32(%rdi), %rdx
	movq	40(%rdi), %rcx
	movq	16(%rdi), %rax
	testq	%r11, %r11
	movq	24(%rdi), %r9
	sete	%r10b
	cmpq	%rcx, %rdx
	seta	%r8b
	orb	%r8b, %r10b
	jne	.L99
	cmpq	%r9, %rax
	ja	.L99
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	84(%r11), %r8d
	testl	%r8d, %r8d
	jne	.L14
	cmpq	%r9, %rax
	je	.L15
	movq	16(%r11), %r10
	movq	48(%rdi), %r13
	movq	%rcx, -48(%rbp)
	movq	%r9, %r14
	leaq	_ZL17lookupInitialData(%rip), %r15
	movzwl	6(%r10), %r8d
	sarl	$7, %r8d
	cmpl	$10, 80(%r11)
	movw	%r8w, -50(%rbp)
	je	.L133
.L17:
	movzwl	(%rax), %r8d
	addq	$2, %rax
	movl	%r8d, %ebx
	cmpl	$160, %r8d
	jg	.L22
	movq	8(%rdi), %r11
	movl	%r8d, 80(%r11)
	cmpq	-48(%rbp), %rdx
	jnb	.L23
	movq	16(%rdi), %r9
	leaq	1(%rdx), %r8
	movb	%bl, (%rdx)
	testq	%r13, %r13
	je	.L24
	movq	%rax, %rdx
	addq	$4, %r13
	subq	%r9, %rdx
	sarq	%rdx
	subl	$1, %edx
	movl	%edx, -4(%r13)
.L24:
	movq	%r8, %rdx
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jg	.L15
.L26:
	cmpq	%rax, %r14
	jbe	.L15
	movq	8(%rdi), %r11
	cmpl	$10, 80(%r11)
	jne	.L17
.L133:
	movzwl	-50(%rbp), %r8d
	movl	$0, 80(%r11)
	leaq	(%r8,%r8,2), %r8
	movl	8(%r15,%r8,4), %r8d
	movl	%r8d, %ebx
	cmpq	%rdx, -48(%rbp)
	jbe	.L18
	movq	16(%rdi), %r9
	leaq	1(%rdx), %r12
	movb	$-17, (%rdx)
	testq	%r13, %r13
	je	.L19
	movq	%rax, %rcx
	subq	%r9, %rcx
	movq	%rcx, %r9
	sarq	%r9
	subl	$1, %r9d
	movl	%r9d, 0(%r13)
	cmpq	%r12, -48(%rbp)
	jbe	.L91
	movb	%r8b, 1(%rdx)
	addq	$8, %r13
	addq	$2, %rdx
	movl	%r9d, -4(%r13)
.L89:
	movl	(%rsi), %r9d
	testl	%r9d, %r9d
	jle	.L17
.L15:
	movq	%rax, 16(%rdi)
	movq	%rdx, 32(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movzwl	2(%r10), %r9d
	cmpw	$8204, %r8w
	je	.L27
	cmpw	$8205, %r8w
	jne	.L134
	cmpw	$1, %r9w
	sbbl	%r12d, %r12d
	andl	$-16, %r12d
	subl	$23, %r12d
	cmpw	$1, %r9w
	sbbl	%ebx, %ebx
	xorl	%ecx, %ecx
	andl	$-16, %ebx
	movw	%cx, 2(%r10)
	addl	$233, %ebx
	cmpw	$256, 6(%r10)
	je	.L93
	movq	%rax, %r8
	subq	16(%rdi), %r8
	sarq	%r8
	subl	$1, %r8d
	cmpq	-48(%rbp), %rdx
	jb	.L135
.L69:
	movq	8(%rdi), %r8
	movsbl	91(%r8), %ecx
	leal	1(%rcx), %r9d
	testb	$-1, %bh
	je	.L75
	movslq	%ecx, %rcx
	movzbl	%bh, %ebx
	movb	%r9b, 91(%r8)
	movb	%bl, 104(%r8,%rcx)
	movq	8(%rdi), %r8
	movsbl	91(%r8), %ecx
	leal	1(%rcx), %r9d
.L75:
	movslq	%ecx, %rcx
	movb	%r9b, 91(%r8)
	movb	%r12b, 104(%r8,%rcx)
	movl	$15, (%rsi)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L23:
	movsbl	91(%r11), %ecx
	leal	1(%rcx), %r9d
	testw	$-256, %r8w
	je	.L25
	movslq	%ecx, %rcx
	movb	%r9b, 91(%r11)
	movb	$0, 104(%r11,%rcx)
	movq	8(%rdi), %r11
	movsbl	91(%r11), %ecx
	leal	1(%rcx), %r9d
.L25:
	movslq	%ecx, %rcx
	movb	%r9b, 91(%r11)
	movb	%r8b, 104(%r11,%rcx)
	movl	$15, (%rsi)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$3455, %r11d
	subl	%r8d, %r11d
	cmpw	$1151, %r11w
	ja	.L136
	leal	-2404(%r8), %r11d
	cmpl	$1, %r11d
	jbe	.L137
	movl	%r8d, %r11d
	leal	-2177(%r8), %ebx
	subl	$2304, %r11d
	cmovns	%r11d, %ebx
	sarl	$7, %ebx
	movl	%ebx, %r11d
	movw	%bx, -50(%rbp)
	sall	$7, %r11d
	cmpw	%r11w, 6(%r10)
	je	.L138
.L38:
	movzwl	%bx, %ebx
	movw	%r11w, 6(%r10)
	leaq	(%rbx,%rbx,2), %rbx
	movb	$0, 24(%r10)
	movl	4(%r15,%rbx,4), %r12d
	movb	$1, -56(%rbp)
	movl	%r12d, 12(%r10)
.L39:
	cmpw	$256, %r11w
	je	.L139
.L40:
	movzwl	%r11w, %r11d
	subl	%r11d, %r8d
.L37:
	movzbl	%r8b, %r11d
	leaq	_ZL16fromUnicodeTable(%rip), %rbx
	leaq	_ZL13validityTable(%rip), %rcx
	movzwl	(%rbx,%r11,2), %ebx
	movzbl	(%rcx,%r11), %r11d
	testl	%r12d, %r11d
	jne	.L41
	cmpw	$768, 6(%r10)
	jne	.L100
	cmpl	$2353, %r8d
	je	.L42
.L100:
	movl	$65535, %ebx
.L41:
	cmpb	$0, -56(%rbp)
	jne	.L44
	movzwl	6(%r10), %r11d
.L45:
	cmpl	$2417, %r8d
	jne	.L35
	cmpw	$256, %r11w
	jne	.L35
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rax, %r8
	testw	%r9w, %r9w
	je	.L26
	subq	16(%rdi), %r8
	sarq	%r8
	subl	$1, %r8d
.L31:
	movl	$232, %r9d
	movw	%r9w, 2(%r10)
	cmpq	%rdx, -48(%rbp)
	jbe	.L67
	movq	%rdx, %r9
	movq	%r13, %rbx
	addq	$1, %rdx
	movl	$-24, %r12d
.L68:
	movb	%r12b, (%r9)
	testq	%r13, %r13
	je	.L71
	movl	%r8d, (%rbx)
	addq	$4, %r13
.L71:
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jle	.L26
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L143:
	testl	$1024, %r8d
	jne	.L77
	movq	8(%rdi), %r11
	.p2align 4,,10
	.p2align 3
.L14:
	cmpq	%r9, %rax
	jnb	.L78
	movzwl	(%rax), %ecx
	movl	%ecx, %r9d
	andl	$-1024, %r9d
	cmpl	$56320, %r9d
	je	.L140
	movl	$12, (%rsi)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%rax, %r8
	subq	16(%rdi), %r8
	movl	%ebx, %r12d
	sarq	%r8
	subl	$1, %r8d
	cmpl	$232, %ebx
	je	.L31
	cmpq	%rdx, -48(%rbp)
	jbe	.L69
	leaq	1(%rdx), %r11
	cmpl	$255, %ebx
	jbe	.L141
	movb	%bh, (%rdx)
	testq	%r13, %r13
	je	.L72
	movl	%r8d, 0(%r13)
	cmpq	%r11, -48(%rbp)
	jbe	.L130
	movb	%bl, 1(%rdx)
	addq	$8, %r13
	addq	$2, %rdx
	movl	%r8d, -4(%r13)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L18:
	movsbq	91(%r11), %rcx
	leal	1(%rcx), %r9d
	movb	%r9b, 91(%r11)
	movb	$-17, 104(%r11,%rcx)
	movq	8(%rdi), %r9
	movsbq	91(%r9), %rcx
	leal	1(%rcx), %r10d
	movb	%r10b, 91(%r9)
	movb	%r8b, 104(%r9,%rcx)
	movl	$15, (%rsi)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$0, (%rsi)
.L80:
	movl	%r8d, 84(%r11)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L136:
	movzwl	6(%r10), %r11d
	movl	$65535, %ebx
.L35:
	xorl	%r12d, %r12d
	movw	%r12w, 2(%r10)
.L50:
	cmpw	$256, %r11w
	je	.L142
.L52:
	cmpl	$65535, %ebx
	jne	.L93
	movl	%r8d, %ecx
	movq	%r14, %r9
	andl	$-2048, %ecx
	cmpl	$55296, %ecx
	je	.L143
	movl	$10, (%rsi)
	movq	8(%rdi), %r11
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L67:
	movq	8(%rdi), %r8
	movl	$-24, %r12d
	movsbl	91(%r8), %ecx
	leal	1(%rcx), %r9d
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L91:
	movq	8(%rdi), %rcx
	movsbq	91(%rcx), %rdx
	leal	1(%rdx), %r8d
	movb	%r8b, 91(%rcx)
	movb	%bl, 104(%rcx,%rdx)
	movq	%r12, %rdx
	movl	$15, (%rsi)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L140:
	sall	$10, %r8d
	movl	$10, (%rsi)
	addq	$2, %rax
	leal	-56613888(%rcx,%r8), %r8d
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%rdx, %r9
	movq	%r13, %rbx
	addq	$1, %rdx
	jmp	.L68
.L72:
	cmpq	%r11, -48(%rbp)
	jbe	.L130
	movb	%bl, 1(%rdx)
	addq	$2, %rdx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L142:
	cmpw	$2673, %r9w
	jne	.L52
	leal	-2304(%r8), %r9d
	cmpl	$79, %r9d
	ja	.L52
	movslq	%r9d, %r9
	leaq	_ZL6pnjMap(%rip), %rcx
	testb	$1, (%rcx,%r9)
	je	.L52
	movl	%ebx, %r8d
	xorl	%r11d, %r11d
	sall	$16, %r8d
	movw	%r11w, 2(%r10)
	orl	%r8d, %ebx
	movl	%ebx, %r12d
	orl	$59392, %r12d
	movl	%r12d, %r8d
	shrl	$8, %r8d
	movb	%r8b, -56(%rbp)
	cmpq	-48(%rbp), %rdx
	jnb	.L56
	movq	%rax, %r9
	subq	16(%rdi), %r9
	sarq	%r9
	cmpl	$65535, %r12d
	ja	.L57
	movb	%r12b, -51(%rbp)
	subl	$1, %r9d
	leaq	1(%rdx), %rbx
	movb	%r8b, (%rdx)
	testq	%r13, %r13
	je	.L59
.L58:
	movl	%r9d, 0(%r13)
	cmpq	-48(%rbp), %rbx
	jnb	.L131
	movzbl	-51(%rbp), %ebx
	addq	$8, %r13
	addq	$2, %rdx
	movb	%bl, -1(%rdx)
	movl	%r9d, -4(%r13)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L19:
	cmpq	%r12, -48(%rbp)
	jbe	.L91
	movb	%r8b, 1(%rdx)
	addq	$2, %rdx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L44:
	movzwl	-50(%rbp), %r11d
	leaq	(%r11,%r11,2), %r11
	movl	8(%r15,%r11,4), %ecx
	movl	%ecx, -56(%rbp)
	cmpq	-48(%rbp), %rdx
	jnb	.L46
	movq	16(%rdi), %r11
	leaq	1(%rdx), %r12
	movb	$-17, (%rdx)
	testq	%r13, %r13
	je	.L47
	movq	%rax, %rcx
	subq	%r11, %rcx
	movq	%rcx, %r11
	sarq	%r11
	subl	$1, %r11d
	movl	%r11d, 0(%r13)
	cmpq	%r12, -48(%rbp)
	ja	.L48
	addq	$4, %r13
.L88:
	movq	8(%rdi), %r11
	movsbq	91(%r11), %rdx
	leal	1(%rdx), %ecx
	movb	%cl, 91(%r11)
	movzbl	-56(%rbp), %ecx
	movb	%cl, 104(%r11,%rdx)
	movq	%r12, %rdx
	movl	$15, (%rsi)
.L49:
	movzwl	6(%r10), %r11d
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L137:
	movb	$0, -56(%rbp)
	movl	12(%r10), %r12d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r11, %rbx
.L131:
	movq	8(%rdi), %rcx
	movsbq	91(%rcx), %rdx
	leal	1(%rdx), %r8d
	movb	%r8b, 91(%rcx)
	movb	%r12b, 104(%rcx,%rdx)
	movq	%rbx, %rdx
	movl	$15, (%rsi)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L46:
	movq	8(%rdi), %r12
	movsbq	91(%r12), %r11
	leal	1(%r11), %ecx
	movb	%cl, 91(%r12)
	movb	$-17, 104(%r12,%r11)
	movq	8(%rdi), %r12
	movsbq	91(%r12), %r11
	leal	1(%r11), %ecx
	movb	%cl, 91(%r12)
	movzbl	-56(%rbp), %ecx
	movb	%cl, 104(%r12,%r11)
	movl	$15, (%rsi)
	jmp	.L49
.L139:
	cmpl	$2672, %r8d
	je	.L97
	cmpl	$2673, %r8d
	jne	.L40
	movl	$2673, %ebx
	movw	%bx, 2(%r10)
	jmp	.L40
.L138:
	movzbl	24(%r10), %ecx
	movb	%cl, -56(%rbp)
	testb	%cl, %cl
	jne	.L38
	movl	12(%r10), %r12d
	jmp	.L39
.L48:
	movzbl	-56(%rbp), %ecx
	addq	$8, %r13
	addq	$2, %rdx
	movb	%cl, -1(%rdx)
	movl	%r11d, -4(%r13)
.L86:
	movl	(%rsi), %ecx
	movzwl	6(%r10), %r11d
	testl	%ecx, %ecx
	jle	.L45
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L56:
	movq	8(%rdi), %r8
	andl	$16711680, %ebx
	movsbl	91(%r8), %ecx
	leal	1(%rcx), %r9d
	je	.L66
	movb	%r9b, 91(%r8)
	movl	%r12d, %r9d
	movslq	%ecx, %rcx
	shrl	$16, %r9d
	movb	%r9b, 104(%r8,%rcx)
	movq	8(%rdi), %r8
	movsbl	91(%r8), %ecx
	leal	1(%rcx), %r9d
.L66:
	movzbl	-56(%rbp), %ebx
	movslq	%ecx, %rcx
	movb	%r9b, 91(%r8)
	movb	%bl, 104(%r8,%rcx)
	movq	8(%rdi), %r8
	movsbq	91(%r8), %rcx
	leal	1(%rcx), %r9d
	movb	%r9b, 91(%r8)
	movb	%r12b, 104(%r8,%rcx)
	movl	$15, (%rsi)
	jmp	.L15
.L47:
	cmpq	%r12, -48(%rbp)
	jbe	.L88
	movzbl	-56(%rbp), %ecx
	addq	$2, %rdx
	movb	%cl, -1(%rdx)
	jmp	.L86
.L97:
	movl	$2562, %r8d
	jmp	.L40
.L42:
	cmpb	$0, -56(%rbp)
	jne	.L44
	xorl	%ecx, %ecx
	movl	$2353, %r8d
	movw	%cx, 2(%r10)
	jmp	.L52
.L57:
	movl	%r12d, %r11d
	leaq	1(%rdx), %rbx
	shrl	$16, %r11d
	movq	%rbx, -64(%rbp)
	movb	%r11b, (%rdx)
	testq	%r13, %r13
	je	.L144
	subl	$2, %r9d
	movb	%r12b, -51(%rbp)
	leaq	4(%r13), %r11
	leaq	1(%rdx), %rcx
	movl	%r9d, 0(%r13)
	cmpq	%rcx, -48(%rbp)
	ja	.L63
.L62:
	movq	8(%rdi), %rcx
	movzbl	-56(%rbp), %ebx
	movsbq	91(%rcx), %rdx
	leal	1(%rdx), %r8d
	movb	%r8b, 91(%rcx)
	movb	%bl, 104(%rcx,%rdx)
	movq	8(%rdi), %rcx
	movzbl	-51(%rbp), %ebx
	movsbq	91(%rcx), %rdx
	leal	1(%rdx), %r8d
	movb	%r8b, 91(%rcx)
	movb	%bl, 104(%rcx,%rdx)
	movq	-64(%rbp), %rdx
	movl	$15, (%rsi)
	jmp	.L15
.L77:
	movl	$12, (%rsi)
	movq	8(%rdi), %r11
	jmp	.L80
.L144:
	cmpq	%rbx, -48(%rbp)
	ja	.L61
	movb	%r12b, -51(%rbp)
	jmp	.L62
.L61:
	movb	%r8b, 1(%rdx)
	leaq	2(%rdx), %rbx
	movq	-64(%rbp), %rdx
.L59:
	cmpq	%rbx, -48(%rbp)
	jbe	.L131
	movb	%r12b, 1(%rdx)
	xorl	%r13d, %r13d
	addq	$2, %rdx
	jmp	.L71
.L63:
	movb	%r8b, 1(%rdx)
	leaq	2(%rdx), %rbx
	movq	%r11, %r13
	movq	-64(%rbp), %rdx
	jmp	.L58
.L141:
	movq	%rdx, %r9
	movq	%r13, %rbx
	movq	%r11, %rdx
	jmp	.L68
	.cfi_endproc
.LFE2119:
	.size	UConverter_fromUnicode_ISCII_OFFSETS_LOGIC, .-UConverter_fromUnicode_ISCII_OFFSETS_LOGIC
	.p2align 4
	.type	UConverter_toUnicode_ISCII_OFFSETS_LOGIC, @function
UConverter_toUnicode_ISCII_OFFSETS_LOGIC:
.LFB2120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r10
	testq	%r10, %r10
	je	.L443
	movq	24(%rdi), %rax
	movl	(%r15), %r11d
	movq	16(%rdi), %rcx
	movq	32(%rdi), %rsi
	movq	%rax, -48(%rbp)
	movq	16(%r10), %rdx
	testl	%r11d, %r11d
	jg	.L258
	cmpq	%rax, %rcx
	jnb	.L148
	movq	40(%rdi), %rax
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rsi
	jnb	.L150
	leaq	.L185(%rip), %r9
	leaq	.L179(%rip), %r8
.L149:
	movzwl	(%rdx), %ebx
	addq	$1, %rcx
	movzbl	-1(%rcx), %eax
	cmpw	$239, %bx
	je	.L444
	cmpw	$240, %bx
	je	.L445
	movzbl	%al, %r11d
	cmpw	$217, %bx
	je	.L446
	cmpb	$-16, %al
	ja	.L174
	cmpb	$-40, %al
	jbe	.L442
	leal	39(%rax), %r12d
	cmpb	$23, %r12b
	ja	.L177
	movzbl	%r12b, %r12d
	movslq	(%r9,%r12,4), %r12
	addq	%r9, %r12
	notrack jmp	*%r12
	.section	.rodata
	.align 4
	.align 4
.L185:
	.long	.L178-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L187-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L182-.L185
	.long	.L181-.L185
	.long	.L186-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L177-.L185
	.long	.L178-.L185
	.long	.L178-.L185
	.text
	.p2align 4,,10
	.p2align 3
.L442:
	cmpb	$10, %al
	je	.L176
	cmpb	$13, %al
	jne	.L177
.L176:
	leaq	_ZL14toUnicodeTable(%rip), %rbx
	movb	$1, 25(%rdx)
	movzwl	(%rbx,%r11,2), %ebx
	movzbl	%al, %r11d
	movw	%r11w, (%rdx)
.L203:
	movl	72(%r10), %r11d
	cmpl	$65535, %r11d
	je	.L234
.L439:
	movzwl	8(%rdx), %r12d
.L199:
	movl	44(%rdx), %r13d
	movl	%r13d, -68(%rbp)
	cmpw	$256, %r12w
	je	.L447
.L235:
	movq	40(%rdi), %r14
	movl	-68(%rbp), %r13d
	movq	%r14, -64(%rbp)
	testl	%r13d, %r13d
	jne	.L237
	movq	%rsi, %r13
.L246:
	leal	-8204(%r11), %esi
	cmpl	$1, %esi
	leal	-2404(%r11), %esi
	seta	-68(%rbp)
	cmpl	$1, %esi
	movzbl	-68(%rbp), %r14d
	seta	%sil
	testb	%sil, %r14b
	je	.L254
	cmpl	$160, %r11d
	jg	.L247
.L254:
	cmpq	-64(%rbp), %r13
	jnb	.L255
	movw	%r11w, 0(%r13)
	movq	48(%rdi), %r11
	leaq	2(%r13), %rsi
	testq	%r11, %r11
	je	.L256
	movq	%rcx, %r12
	subq	16(%rdi), %r12
	leaq	4(%r11), %r13
	subl	$2, %r12d
	movq	%r13, 48(%rdi)
	movl	%r12d, (%r11)
.L256:
	movl	$65535, 72(%r10)
.L234:
	cmpl	$65535, %ebx
	je	.L200
.L197:
	cmpq	%rcx, -48(%rbp)
	movl	%ebx, 72(%r10)
	seta	%al
	cmpb	$1, 25(%rdx)
	jne	.L435
	movzwl	4(%rdx), %r11d
	movb	$0, 25(%rdx)
	movw	%r11w, 8(%rdx)
	movl	20(%rdx), %r11d
	movl	%r11d, 16(%rdx)
.L435:
	movl	(%r15), %r11d
	testl	%r11d, %r11d
	setle	%bl
	andl	%ebx, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L444:
	movl	$75, %ebx
	subl	%eax, %ebx
	cmpb	$9, %bl
	jbe	.L448
	cmpb	$64, %al
	je	.L449
	leal	-33(%rax), %ebx
	cmpb	$30, %bl
	jbe	.L153
	movl	$-2, %r13d
	movl	$12, (%r15)
	movw	%r13w, (%rdx)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L448:
	andl	$15, %eax
	leaq	_ZL11lookupTable(%rip), %r14
	movzwl	(%r14,%rax,4), %ebx
	movzwl	2(%r14,%rax,4), %eax
	movw	%bx, -64(%rbp)
	sall	$7, %ebx
	movw	%bx, 8(%rdx)
	movl	%eax, 16(%rdx)
.L153:
	testl	%r11d, %r11d
	movl	$-2, %r12d
	setle	%bl
	movw	%r12w, (%rdx)
	cmpq	%rcx, -48(%rbp)
	seta	%al
	andl	%ebx, %eax
.L156:
	testb	%al, %al
	je	.L148
	cmpq	-56(%rbp), %rsi
	jb	.L149
.L150:
	movl	$15, (%r15)
.L258:
	movq	%rsi, 32(%rdi)
	movq	%rcx, 16(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	movl	$-18, %r11d
	subl	%eax, %r11d
	cmpb	$77, %r11b
	ja	.L158
	cmpb	$-65, %al
	je	.L313
	cmpb	$-72, %al
	jne	.L200
.L313:
	cmpb	$-65, %al
	movl	$192, %r11d
	movl	$128, %ebx
	movl	$2386, %r12d
	cmove	%r11d, %ebx
	movl	$2416, %r11d
	cmovne	%r12d, %r11d
	testl	%ebx, 16(%rdx)
	jne	.L450
.L200:
	movl	$10, (%r15)
.L155:
	movq	8(%rdi), %r8
	movb	%al, 65(%r8)
	movq	8(%rdi), %rax
	movb	$1, 64(%rax)
	movl	(%r15), %r11d
.L148:
	testl	%r11d, %r11d
	jg	.L258
	cmpb	$0, 2(%rdi)
	je	.L258
	cmpq	-48(%rbp), %rcx
	jne	.L258
	movzwl	(%rdx), %eax
	movq	8(%rdi), %r8
	leal	-239(%rax), %r9d
	cmpw	$1, %r9w
	jbe	.L315
	cmpw	$217, %ax
	je	.L315
	movb	$0, 64(%r8)
.L261:
	movl	72(%r10), %eax
	cmpl	$65535, %eax
	je	.L258
	leal	-2404(%rax), %r8d
	cmpl	$1, %r8d
	leal	-8204(%rax), %r8d
	seta	%r9b
	cmpl	$1, %r8d
	seta	%r8b
	testb	%r8b, %r9b
	je	.L262
	cmpl	$160, %eax
	jg	.L451
.L262:
	cmpq	%rsi, 40(%rdi)
	jbe	.L263
	movw	%ax, (%rsi)
	movq	48(%rdi), %rax
	addq	$2, %rsi
	testq	%rax, %rax
	je	.L264
	movq	%rcx, %rdx
	subq	16(%rdi), %rdx
	leaq	4(%rax), %r8
	subl	$1, %edx
	movq	%r8, 48(%rdi)
	movl	%edx, (%rax)
.L264:
	movl	$65535, 72(%r10)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L450:
	movl	$-2, %ebx
	movq	40(%rdi), %rax
	movw	%bx, (%rdx)
	movl	44(%rdx), %ebx
	testl	%ebx, %ebx
	je	.L162
	cmpq	%rax, %rsi
	jnb	.L163
	movw	%bx, (%rsi)
	movq	48(%rdi), %rbx
	addq	$2, %rsi
	testq	%rbx, %rbx
	je	.L164
	movq	%rcx, %r12
	subq	16(%rdi), %r12
	leaq	4(%rbx), %r13
	subl	$1, %r12d
	movq	%r13, 48(%rdi)
	movl	%r12d, (%rbx)
.L164:
	movl	$0, 44(%rdx)
.L162:
	movzwl	8(%rdx), %ebx
	addl	%ebx, %r11d
	cmpq	%rax, %rsi
	jnb	.L165
	cmpq	%rcx, -48(%rbp)
	movq	48(%rdi), %rbx
	movw	%r11w, (%rsi)
	movl	(%r15), %r11d
	seta	%r12b
	testl	%r11d, %r11d
	setle	%al
	addq	$2, %rsi
	andl	%r12d, %eax
	testq	%rbx, %rbx
	je	.L156
	movq	%rcx, %r12
	subq	16(%rdi), %r12
	leaq	4(%rbx), %r13
	subl	$2, %r12d
	movq	%r13, 48(%rdi)
	movl	%r12d, (%rbx)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L446:
	movl	$32, %r12d
	cmpb	$-24, %al
	movl	$8205, %ebx
	movl	44(%rdx), %r13d
	cmove	%r12d, %ebx
	movq	40(%rdi), %r12
	movw	%bx, -64(%rbp)
	testl	%r13d, %r13d
	je	.L270
	cmpq	%r12, %rsi
	jnb	.L170
	movw	%r13w, (%rsi)
	movq	48(%rdi), %r13
	addq	$2, %rsi
	testq	%r13, %r13
	je	.L171
	movq	%rcx, %r14
	subq	16(%rdi), %r14
	leal	-1(%r14), %ebx
	leaq	4(%r13), %r14
	movq	%r14, 48(%rdi)
	movl	%ebx, 0(%r13)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L170:
	movq	8(%rdi), %r14
	movsbq	93(%r14), %r12
	leal	1(%r12), %ebx
	movb	%bl, 93(%r14)
	movw	%r13w, 144(%r14,%r12,2)
	movq	40(%rdi), %r12
	movl	$15, (%r15)
.L171:
	movl	$0, 44(%rdx)
.L270:
	cmpq	%r12, %rsi
	jnb	.L172
	movzwl	-64(%rbp), %ebx
	addq	$2, %rsi
	movw	%bx, -2(%rsi)
	movq	48(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L173
	movq	%rcx, %r12
	subq	16(%rdi), %r12
	leaq	4(%rbx), %r13
	subl	$2, %r12d
	movq	%r13, 48(%rdi)
	movl	%r12d, (%rbx)
.L173:
	movl	$-2, %r14d
	movw	%r14w, (%rdx)
	cmpb	$-16, %al
	ja	.L174
	cmpb	$-40, %al
	jbe	.L442
	leal	39(%rax), %ebx
	cmpb	$23, %bl
	ja	.L177
	movzbl	%bl, %ebx
	movslq	(%r8,%rbx,4), %rbx
	addq	%r8, %rbx
	notrack jmp	*%rbx
	.section	.rodata
	.align 4
	.align 4
.L179:
	.long	.L178-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L183-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L182-.L179
	.long	.L348-.L179
	.long	.L180-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L177-.L179
	.long	.L178-.L179
	.long	.L178-.L179
	.text
.L348:
	movzwl	(%rdx), %ebx
.L181:
	cmpw	$232, %bx
	je	.L452
	movzwl	8(%rdx), %r12d
	cmpw	$256, %r12w
	jne	.L314
	cmpw	$192, %bx
	je	.L206
.L314:
	movzbl	%bl, %r11d
	cmpb	$-90, %bl
	je	.L288
	cmpw	$234, %r11w
	je	.L289
	cmpw	$223, %r11w
	je	.L290
	cmpw	$161, %r11w
	je	.L291
	cmpw	$179, %r11w
	je	.L292
	cmpw	$180, %r11w
	je	.L293
	cmpw	$181, %r11w
	je	.L294
	cmpw	$186, %r11w
	je	.L295
	cmpw	$191, %r11w
	je	.L296
	cmpw	$192, %r11w
	je	.L297
	cmpw	$201, %r11w
	je	.L298
	cmpw	$170, %r11w
	je	.L299
	cmpw	$167, %r11w
	je	.L300
	cmpw	$219, %r11w
	je	.L301
	cmpw	$220, %r11w
	je	.L453
	movl	16(%rdx), %r13d
.L223:
	andl	$216, %r13d
	movl	$233, %r11d
	je	.L265
	movl	$233, %r11d
	movl	$2364, %ebx
	movw	%r11w, (%rdx)
	movl	72(%r10), %r11d
	cmpl	$65535, %r11d
	jne	.L199
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L447:
	testl	%r13d, %r13d
	je	.L236
	movq	40(%rdi), %r14
	leal	-2560(%r13), %r12d
	movq	%r14, -64(%rbp)
	cmpl	$79, %r12d
	ja	.L237
	movslq	%r12d, %r12
	leaq	_ZL6pnjMap(%rip), %r14
	testb	$1, (%r14,%r12)
	jne	.L454
	.p2align 4,,10
	.p2align 3
.L237:
	cmpq	-64(%rbp), %rsi
	jb	.L455
	movq	8(%rdi), %r12
	movzwl	-68(%rbp), %r14d
	movsbq	93(%r12), %r11
	leal	1(%r11), %r13d
	movb	%r13b, 93(%r12)
	movq	%rsi, %r13
	movw	%r14w, 144(%r12,%r11,2)
	movl	$15, (%r15)
.L243:
	movzwl	8(%rdx), %r12d
	movl	$0, 44(%rdx)
	movl	72(%r10), %r11d
	cmpw	$256, %r12w
	je	.L272
	movq	40(%rdi), %rsi
	movq	%rsi, -64(%rbp)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L255:
	movq	8(%rdi), %r12
	movsbq	93(%r12), %rsi
	leal	1(%rsi), %r14d
	movb	%r14b, 93(%r12)
	movw	%r11w, 144(%r12,%rsi,2)
	movq	%r13, %rsi
	movl	$15, (%r15)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L172:
	movq	8(%rdi), %r13
	movzwl	-64(%rbp), %ebx
	movsbq	93(%r13), %r12
	leal	1(%r12), %r14d
	movb	%r14b, 93(%r13)
	movw	%bx, 144(%r13,%r12,2)
	movl	$15, (%r15)
	jmp	.L173
.L186:
	cmpw	$234, %bx
	je	.L456
.L180:
	movl	$234, %r12d
	testb	$-128, 16(%rdx)
	movl	72(%r10), %r11d
	movw	%r12w, (%rdx)
	jne	.L198
	cmpl	$65535, %r11d
	je	.L457
	.p2align 4,,10
	.p2align 3
.L306:
	movl	$65535, %ebx
	jmp	.L439
.L178:
	movl	72(%r10), %r11d
	cmpq	%rcx, -48(%rbp)
	movw	%ax, (%rdx)
	seta	%al
	cmpl	$65535, %r11d
	je	.L435
	movl	44(%rdx), %r13d
	movq	40(%rdi), %r12
	movq	%rsi, %rbx
	testl	%r13d, %r13d
	je	.L190
	cmpq	%rsi, %r12
	jbe	.L191
	movw	%r13w, (%rsi)
	leaq	2(%rsi), %rbx
	movq	48(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L192
	movq	%rcx, %r11
	subq	16(%rdi), %r11
	leaq	4(%rsi), %r13
	subl	$1, %r11d
	movq	%r13, 48(%rdi)
	movl	%r11d, (%rsi)
	jmp	.L192
.L182:
	cmpw	$232, (%rdx)
	je	.L458
	movl	$232, %r11d
	cmpb	$0, 16(%rdx)
	movw	%r11w, (%rdx)
	movl	72(%r10), %r11d
	jne	.L202
	cmpl	$65535, %r11d
	jne	.L306
	movl	$-24, %eax
	jmp	.L200
.L177:
	leaq	_ZL14toUnicodeTable(%rip), %rbx
	movzwl	(%rbx,%r11,2), %ebx
	movzbl	%al, %r11d
	movl	%ebx, %r12d
	cmpb	$-96, %al
	ja	.L273
.L230:
	movw	%r11w, (%rdx)
	jmp	.L203
.L187:
	movl	16(%rdx), %r12d
	cmpb	$-92, %bl
	je	.L459
.L204:
	andl	$135, %r12d
	movl	72(%r10), %r11d
	jne	.L460
	movl	$224, %r13d
	movw	%r13w, (%rdx)
	cmpl	$65535, %r11d
	jne	.L306
	movl	$-32, %eax
	jmp	.L200
.L183:
	movl	16(%rdx), %r12d
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L455:
	movzwl	-68(%rbp), %r14d
	leaq	2(%rsi), %r13
	movw	%r14w, (%rsi)
	movq	48(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L243
	movq	%rcx, %r11
	subq	16(%rdi), %r11
	leaq	4(%rsi), %r12
	subl	$1, %r11d
	movq	%r12, 48(%rdi)
	movl	%r11d, (%rsi)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L165:
	movq	8(%rdi), %rdx
	movsbq	93(%rdx), %rax
	leal	1(%rax), %r8d
	movb	%r8b, 93(%rdx)
	movw	%r11w, 144(%rdx,%rax,2)
	movl	$15, (%r15)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%rsi, %r13
.L272:
	cmpl	$2306, %ebx
	je	.L461
	cmpl	$2381, %ebx
	je	.L249
.L440:
	movq	40(%rdi), %rsi
	movl	$256, %r12d
	movq	%rsi, -64(%rbp)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L452:
	movl	72(%r10), %r11d
	movl	$-2, %ebx
	movw	%bx, (%rdx)
	movl	$8205, %ebx
	cmpl	$65535, %r11d
	jne	.L439
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L443:
	movl	$1, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movzwl	4(%rdx), %eax
	movw	%ax, 8(%rdx)
	movl	20(%rdx), %eax
	movl	%eax, 16(%rdx)
	jmp	.L153
.L191:
	movq	8(%rdi), %rbx
	movsbq	93(%rbx), %r11
	leal	1(%r11), %r12d
	movb	%r12b, 93(%rbx)
	movq	40(%rdi), %r12
	movw	%r13w, 144(%rbx,%r11,2)
	movq	%rsi, %rbx
	movl	$15, (%r15)
.L192:
	movl	$0, 44(%rdx)
	movl	72(%r10), %r11d
.L190:
	leal	-2404(%r11), %esi
	cmpl	$1, %esi
	leal	-8204(%r11), %esi
	seta	%r13b
	cmpl	$1, %esi
	seta	%sil
	testb	%sil, %r13b
	je	.L193
	cmpl	$160, %r11d
	jg	.L462
.L193:
	cmpq	%r12, %rbx
	jnb	.L194
	movw	%r11w, (%rbx)
	movl	(%r15), %r11d
	leaq	2(%rbx), %rsi
	movq	48(%rdi), %rbx
	testl	%r11d, %r11d
	setle	%r12b
	andl	%r12d, %eax
	testq	%rbx, %rbx
	je	.L434
	movq	%rcx, %r12
	subq	16(%rdi), %r12
	leaq	4(%rbx), %r13
	subl	$2, %r12d
	movq	%r13, 48(%rdi)
	movl	%r12d, (%rbx)
.L434:
	movl	$65535, 72(%r10)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L461:
	movq	40(%rdi), %r12
	leal	-2304(%r11), %esi
	movq	%r12, -64(%rbp)
	cmpl	$79, %esi
	ja	.L308
	movslq	%esi, %rsi
	leaq	_ZL6pnjMap(%rip), %r14
	testb	$-2, (%r14,%rsi)
	je	.L309
	addl	$256, %r11d
	movl	%r11d, 72(%r10)
	cmpq	%r12, %r13
	jnb	.L248
	movq	48(%rdi), %rax
	movw	%r11w, 0(%r13)
	leaq	2(%r13), %rsi
	movl	$2416, %ebx
	testq	%rax, %rax
	je	.L197
	movq	%rcx, %r11
	subq	16(%rdi), %r11
	leaq	4(%rax), %r12
	subl	$2, %r11d
	movq	%r12, 48(%rdi)
	movl	%r11d, (%rax)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L249:
	leal	-2304(%r11), %esi
	cmpl	$79, %esi
	ja	.L440
	movslq	%esi, %rsi
	leaq	_ZL6pnjMap(%rip), %r14
	testb	$1, (%r14,%rsi)
	je	.L463
	addl	$256, %r11d
	movq	%r13, %rsi
	movl	$2381, %ebx
	movl	%r11d, 44(%rdx)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L163:
	movq	8(%rdi), %r12
	movsbq	93(%r12), %rax
	leal	1(%rax), %r13d
	movb	%r13b, 93(%r12)
	movw	%bx, 144(%r12,%rax,2)
	movq	40(%rdi), %rax
	movl	$15, (%r15)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L174:
	leaq	_ZL14toUnicodeTable(%rip), %rbx
	movzwl	(%rbx,%r11,2), %ebx
	movzbl	%al, %r11d
	movl	%ebx, %r12d
.L273:
	andl	$127, %r12d
	leaq	_ZL13validityTable(%rip), %r14
	movzbl	(%r14,%r12), %r12d
	testl	%r12d, 16(%rdx)
	jne	.L230
	cmpw	$768, 8(%rdx)
	jne	.L265
	cmpl	$2353, %ebx
	je	.L231
.L265:
	movw	%r11w, (%rdx)
	movl	72(%r10), %r11d
	cmpl	$65535, %r11d
	jne	.L306
	jmp	.L200
.L309:
	movl	$256, %r12d
.L247:
	addl	%r12d, %r11d
	movl	%r11d, 72(%r10)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L456:
	movl	$-2, %r13d
	movl	$2405, %ebx
	movw	%r13w, (%rdx)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L458:
	movl	72(%r10), %r11d
	movl	$-2, %ebx
	movw	%bx, (%rdx)
	movl	$8204, %ebx
	cmpl	$65535, %r11d
	jne	.L439
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L194:
	movq	8(%rdi), %rdx
	movsbq	93(%rdx), %rax
	leal	1(%rax), %esi
	movb	%sil, 93(%rdx)
	movq	%rbx, %rsi
	movw	%r11w, 144(%rdx,%rax,2)
	movl	$15, (%r15)
	movl	$65535, 72(%r10)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L202:
	movl	$2381, %ebx
	cmpl	$65535, %r11d
	jne	.L439
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$2404, %ebx
	cmpl	$65535, %r11d
	jne	.L439
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L460:
	movl	$224, %r12d
	movl	$2374, %ebx
	movw	%r12w, (%rdx)
	cmpl	$65535, %r11d
	jne	.L439
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L308:
	movl	$256, %r12d
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L454:
	cmpl	$2381, %r11d
	jne	.L237
	leal	256(%rbx), %r11d
	cmpl	%r13d, %r11d
	jne	.L237
	movq	%rcx, %rbx
	subq	16(%rdi), %rbx
	subl	$3, %ebx
	cmpq	-64(%rbp), %rsi
	jnb	.L238
	movq	48(%rdi), %rax
	movl	$2673, %r14d
	leaq	2(%rsi), %r11
	movw	%r14w, (%rsi)
	testq	%rax, %rax
	je	.L239
	leaq	4(%rax), %rsi
	movq	%rsi, 48(%rdi)
	movl	%ebx, (%rax)
	movl	44(%rdx), %r13d
.L239:
	cmpq	%r11, 40(%rdi)
	jbe	.L240
	movw	%r13w, (%r11)
	leaq	2(%r11), %rsi
	movl	(%r15), %r11d
	movq	48(%rdi), %r12
	testl	%r11d, %r11d
	setle	%r13b
	cmpq	%rcx, -48(%rbp)
	seta	%al
	andl	%r13d, %eax
	testq	%r12, %r12
	je	.L241
	leaq	4(%r12), %r13
	movq	%r13, 48(%rdi)
	movl	%ebx, (%r12)
.L241:
	movl	$0, 44(%rdx)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L206:
	movl	44(%rdx), %eax
	movq	40(%rdi), %rbx
	testl	%eax, %eax
	je	.L211
	cmpq	%rsi, %rbx
	jbe	.L212
	movw	%ax, (%rsi)
	movq	48(%rdi), %rax
	addq	$2, %rsi
	testq	%rax, %rax
	je	.L213
	movq	%rcx, %r11
	subq	16(%rdi), %r11
	leaq	4(%rax), %r12
	subl	$1, %r11d
	movq	%r12, 48(%rdi)
	movl	%r11d, (%rax)
.L213:
	movl	$0, 44(%rdx)
.L211:
	cmpq	%rcx, -48(%rbp)
	seta	%al
	cmpq	%rbx, %rsi
	jnb	.L214
	movq	48(%rdi), %r12
	movl	$2652, %r11d
	leaq	2(%rsi), %r14
	movw	%r11w, (%rsi)
	testq	%r12, %r12
	je	.L215
	movq	%rcx, %r11
	subq	16(%rdi), %r11
	leaq	4(%r12), %r13
	subl	$2, %r11d
	movq	%r13, 48(%rdi)
	movl	%r11d, (%r12)
	movl	(%r15), %r11d
	testl	%r11d, %r11d
	jg	.L304
	cmpq	%rbx, %r14
	jnb	.L218
	movl	$2637, %r13d
	leaq	4(%rsi), %r14
	movw	%r13w, 2(%rsi)
	movq	%rcx, %r13
	subq	16(%rdi), %r13
	subl	$2, %r13d
	movl	%r13d, -64(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, 48(%rdi)
	movl	-64(%rbp), %r13d
	movl	%r13d, 4(%r12)
	cmpq	%rbx, %r14
	jb	.L464
.L220:
	movq	8(%rdi), %rsi
	movl	$-2, %r9d
	movsbq	93(%rsi), %rax
	leal	1(%rax), %r8d
	movb	%r8b, 93(%rsi)
	movl	$2617, %r8d
	movw	%r8w, 144(%rsi,%rax,2)
	movq	%r14, %rsi
	movl	$15, (%r15)
	movl	$65535, 72(%r10)
	movw	%r9w, (%rdx)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$-2, %r14d
	movb	%al, 65(%r8)
	movb	$1, 64(%r8)
	movw	%r14w, (%rdx)
	jmp	.L261
.L457:
	movl	$-22, %eax
	jmp	.L200
.L299:
	movl	$2400, %ebx
	movl	$190, %r11d
	.p2align 4,,10
	.p2align 3
.L208:
	movl	16(%rdx), %r13d
	testl	%r11d, %r13d
	je	.L223
	movl	$-2, %r13d
	movw	%r13w, (%rdx)
	movl	$65535, 72(%r10)
	cmpw	$256, %r12w
	jne	.L197
	movl	44(%rdx), %r12d
	testl	%r12d, %r12d
	je	.L225
	cmpq	%rsi, 40(%rdi)
	jbe	.L226
	movq	48(%rdi), %rax
	movw	%r12w, (%rsi)
	addq	$2, %rsi
	testq	%rax, %rax
	je	.L227
	movq	%rcx, %r11
	subq	16(%rdi), %r11
	leaq	4(%rax), %r12
	subl	$1, %r11d
	movq	%r12, 48(%rdi)
	movl	%r11d, (%rax)
.L227:
	movl	$0, 44(%rdx)
.L225:
	movzwl	8(%rdx), %eax
	addl	%ebx, %eax
	cmpq	%rsi, 40(%rdi)
	jbe	.L228
	movw	%ax, (%rsi)
	leaq	2(%rsi), %rbx
	movq	48(%rdi), %rsi
	cmpq	%rcx, -48(%rbp)
	seta	%al
	testq	%rsi, %rsi
	je	.L436
	movq	%rcx, %r11
	subq	16(%rdi), %r11
	leaq	4(%rsi), %r12
	subl	$2, %r11d
	movq	%r12, 48(%rdi)
	movl	%r11d, (%rsi)
.L436:
	movl	(%r15), %r11d
	testl	%r11d, %r11d
	setle	%sil
	andl	%esi, %eax
	movq	%rbx, %rsi
	jmp	.L156
.L463:
	movq	40(%rdi), %rsi
	movl	$256, %r12d
	movq	%rsi, -64(%rbp)
	jmp	.L247
.L263:
	movq	8(%rdi), %r8
	movsbq	93(%r8), %rdx
	leal	1(%rdx), %r9d
	movb	%r9b, 93(%r8)
	movw	%ax, 144(%r8,%rdx,2)
	movl	$15, (%r15)
	jmp	.L264
.L214:
	movq	8(%rdi), %rbx
	movl	$2652, %r13d
	movsbq	93(%rbx), %r11
	leal	1(%r11), %r12d
	movb	%r12b, 93(%rbx)
	movw	%r13w, 144(%rbx,%r11,2)
	movl	$15, (%r15)
.L217:
	movq	8(%rdi), %rbx
	movl	$2617, %r13d
	movsbq	93(%rbx), %r11
	leal	1(%r11), %r12d
	movb	%r12b, 93(%rbx)
	movl	$2637, %r12d
	movw	%r12w, 144(%rbx,%r11,2)
	movq	8(%rdi), %rbx
	movsbq	93(%rbx), %r11
	leal	1(%r11), %r12d
	movb	%r12b, 93(%rbx)
	movw	%r13w, 144(%rbx,%r11,2)
	movl	(%r15), %r11d
	testl	%r11d, %r11d
	setle	%bl
	andl	%ebx, %eax
.L222:
	movl	$-2, %r14d
	movl	$65535, 72(%r10)
	movw	%r14w, (%rdx)
	jmp	.L156
.L158:
	movl	$-2, %r8d
	movw	%r8w, (%rdx)
	movl	$12, (%r15)
	jmp	.L155
.L462:
	movzwl	8(%rdx), %esi
	addl	%esi, %r11d
	movl	%r11d, 72(%r10)
	jmp	.L193
.L248:
	movq	8(%rdi), %rsi
	movsbq	93(%rsi), %rax
	leal	1(%rax), %ebx
	movb	%bl, 93(%rsi)
	movl	$2416, %ebx
	movw	%r11w, 144(%rsi,%rax,2)
	movq	%r13, %rsi
	movl	$15, (%r15)
	jmp	.L197
.L459:
	testb	$-128, %r12b
	je	.L204
	movl	$-2, %r14d
	movl	$2308, %ebx
	movw	%r14w, (%rdx)
	jmp	.L197
.L231:
	movw	%r11w, (%rdx)
	movl	72(%r10), %r11d
	movl	$2353, %ebx
	cmpl	$65535, %r11d
	je	.L197
	movl	44(%rdx), %ebx
	movl	$768, %r12d
	movl	%ebx, -68(%rbp)
	movl	$2353, %ebx
	jmp	.L235
.L212:
	movq	8(%rdi), %rbx
	movsbq	93(%rbx), %r11
	leal	1(%r11), %r12d
	movb	%r12b, 93(%rbx)
	movw	%ax, 144(%rbx,%r11,2)
	movq	40(%rdi), %rbx
	movl	$15, (%r15)
	jmp	.L213
.L218:
	movq	8(%rdi), %r11
	movl	$2617, %r12d
	movsbq	93(%r11), %rsi
	leal	1(%rsi), %ebx
	movb	%bl, 93(%r11)
	movl	$2637, %ebx
	movw	%bx, 144(%r11,%rsi,2)
	movq	8(%rdi), %r11
	movl	$15, (%r15)
	movsbq	93(%r11), %rsi
	leal	1(%rsi), %ebx
	movb	%bl, 93(%r11)
	movw	%r12w, 144(%r11,%rsi,2)
	movl	(%r15), %r11d
	testl	%r11d, %r11d
	setle	%sil
	andl	%esi, %eax
	movq	%r14, %rsi
	jmp	.L222
.L288:
	movl	$2316, %ebx
	movl	$158, %r11d
	jmp	.L208
.L289:
	movl	$2365, %ebx
	movl	$128, %r11d
	jmp	.L208
.L290:
	movl	$2372, %ebx
	movl	$172, %r11d
	jmp	.L208
.L451:
	movzwl	8(%rdx), %edx
	addl	%edx, %eax
	movl	%eax, 72(%r10)
	jmp	.L262
.L291:
	movl	$2384, %ebx
	movl	$160, %r11d
	jmp	.L208
.L215:
	movl	(%r15), %r11d
	testl	%r11d, %r11d
	jg	.L304
	cmpq	%rbx, %r14
	jnb	.L218
	movl	$2637, %r12d
	leaq	4(%rsi), %r14
	movw	%r12w, 2(%rsi)
	cmpq	%rbx, %r14
	jnb	.L220
	movw	$2617, 4(%rsi)
	addq	$6, %rsi
	jmp	.L222
.L292:
	movl	$2392, %ebx
	movl	$128, %r11d
	jmp	.L208
.L293:
	movl	$2393, %ebx
	movl	$192, %r11d
	jmp	.L208
.L294:
	movl	$2394, %ebx
	movl	$192, %r11d
	jmp	.L208
.L295:
	movl	$2395, %ebx
	movl	$192, %r11d
	jmp	.L208
.L296:
	movl	$2396, %ebx
	movl	$200, %r11d
	jmp	.L208
.L297:
	movl	$2397, %ebx
	movl	$152, %r11d
	jmp	.L208
.L298:
	movl	$2398, %ebx
	movl	$192, %r11d
	jmp	.L208
.L300:
	movl	$2401, %ebx
	movl	$158, %r11d
	jmp	.L208
.L301:
	movl	$2402, %ebx
	movl	$136, %r11d
	jmp	.L208
.L453:
	movl	$2403, %ebx
	movl	$136, %r11d
	jmp	.L208
.L304:
	movq	%r14, %rsi
	jmp	.L217
.L228:
	movq	8(%rdi), %r8
	movsbq	93(%r8), %rdx
	leal	1(%rdx), %r9d
	movb	%r9b, 93(%r8)
	movw	%ax, 144(%r8,%rdx,2)
	movl	$15, (%r15)
	jmp	.L258
.L464:
	movl	$2617, %ebx
	leaq	12(%r12), %r13
	addq	$6, %rsi
	movw	%bx, -2(%rsi)
	movq	%rcx, %rbx
	subq	16(%rdi), %rbx
	subl	$2, %ebx
	movq	%r13, 48(%rdi)
	movl	%ebx, 8(%r12)
	jmp	.L222
.L226:
	movq	8(%rdi), %r11
	movsbq	93(%r11), %rax
	leal	1(%rax), %r13d
	movb	%r13b, 93(%r11)
	movw	%r12w, 144(%r11,%rax,2)
	movl	$15, (%r15)
	jmp	.L227
.L238:
	movq	8(%rdi), %r11
	movsbq	93(%r11), %rax
	leal	1(%rax), %r12d
	movb	%r12b, 93(%r11)
	movl	$2673, %r12d
	movl	44(%rdx), %r13d
	movw	%r12w, 144(%r11,%rax,2)
	movq	%rsi, %r11
	movl	$15, (%r15)
	jmp	.L239
.L240:
	movq	8(%rdi), %rsi
	movsbq	93(%rsi), %rax
	leal	1(%rax), %r8d
	movb	%r8b, 93(%rsi)
	movw	%r13w, 144(%rsi,%rax,2)
	movq	%r11, %rsi
	movl	$15, (%r15)
	movl	$0, 44(%rdx)
	movl	$65535, 72(%r10)
	jmp	.L258
	.cfi_endproc
.LFE2120:
	.size	UConverter_toUnicode_ISCII_OFFSETS_LOGIC, .-UConverter_toUnicode_ISCII_OFFSETS_LOGIC
	.p2align 4
	.type	_ISCIIGetUnicodeSet, @function
_ISCIIGetUnicodeSet:
.LFB2122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$160, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	xorl	%esi, %esi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	128+_ZL13validityTable(%rip), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%r15), %rdi
	call	*16(%r15)
	leaq	4+_ZL17lookupInitialData(%rip), %rax
	movq	%rax, -72(%rbp)
	leaq	_ZL13validityTable(%rip), %rax
	movq	%rax, -64(%rbp)
	movl	%eax, -52(%rbp)
	movl	-64(%rbp), %eax
	movl	$0, -64(%rbp)
	negl	%eax
	movl	%eax, -56(%rbp)
.L469:
	movq	-72(%rbp), %rax
	leaq	_ZL13validityTable(%rip), %r14
	movzbl	(%rax), %ebx
	movl	-56(%rbp), %eax
	leal	2304(%rax), %r13d
	.p2align 4,,10
	.p2align 3
.L468:
	testb	%bl, (%r14)
	jne	.L466
	movl	%r14d, %edx
	subl	-52(%rbp), %edx
	cmpl	$49, %edx
	jne	.L467
	cmpl	$6, -64(%rbp)
	jne	.L467
.L466:
	movq	(%r15), %rdi
	leal	0(%r13,%r14), %esi
	call	*8(%r15)
.L467:
	addq	$1, %r14
	cmpq	%r14, %r12
	jne	.L468
	addl	$1, -64(%rbp)
	movl	-64(%rbp), %eax
	addq	$12, -72(%rbp)
	subl	$-128, -56(%rbp)
	cmpl	$9, %eax
	jne	.L469
	movq	(%r15), %rdi
	movl	$2404, %esi
	call	*8(%r15)
	movq	(%r15), %rdi
	movl	$2405, %esi
	call	*8(%r15)
	movq	(%r15), %rdi
	movl	$8204, %esi
	call	*8(%r15)
	movq	8(%r15), %rax
	movq	(%r15), %rdi
	addq	$40, %rsp
	popq	%rbx
	movl	$8205, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE2122:
	.size	_ISCIIGetUnicodeSet, .-_ISCIIGetUnicodeSet
	.p2align 4
	.type	_ISCIIClose, @function
_ISCIIClose:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L479
	cmpb	$0, 62(%rbx)
	je	.L486
.L481:
	movq	$0, 16(%rbx)
.L479:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	.cfi_restore_state
	call	uprv_free_67@PLT
	jmp	.L481
	.cfi_endproc
.LFE2116:
	.size	_ISCIIClose, .-_ISCIIClose
	.p2align 4
	.type	_ISCII_SafeClone, @function
_ISCII_SafeClone:
.LFB2121:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L487
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L491
	movq	16(%rdi), %rdx
	leaq	288(%rsi), %rax
	movdqu	(%rdx), %xmm0
	movups	%xmm0, 288(%rsi)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 304(%rsi)
	movdqu	32(%rdx), %xmm2
	movb	$1, 62(%rsi)
	movq	%rax, 16(%rsi)
	movq	%rsi, %rax
	movups	%xmm2, 320(%rsi)
.L487:
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	movl	$336, (%rdx)
	ret
	.cfi_endproc
.LFE2121:
	.size	_ISCII_SafeClone, .-_ISCII_SafeClone
	.p2align 4
	.type	_ISCIIOpen, @function
_ISCIIOpen:
.LFB2115:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rsi)
	je	.L500
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$48, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L494
	movl	$65535, 72(%r12)
	movl	$65534, (%rax)
	movb	$0, 25(%rax)
	movl	12(%rbx), %eax
	andl	$15, %eax
	cmpl	$8, %eax
	ja	.L495
	movl	%eax, %edx
	addl	$48, %eax
	movb	$1, 24(%rdi)
	leaq	(%rdx,%rdx,2), %rcx
	leaq	_ZL17lookupInitialData(%rip), %rdx
	movl	$1869181810, 34(%rdi)
	leaq	(%rdx,%rcx,4), %rcx
	movb	%al, 40(%rdi)
	movzwl	(%rcx), %edx
	movb	$0, 41(%rdi)
	movl	$0, 44(%rdi)
	sall	$7, %edx
	movw	%dx, 4(%rdi)
	movw	%dx, 8(%rdi)
	movw	%dx, 6(%rdi)
	movl	4(%rcx), %edx
	movabsq	$7311079738355962697, %rcx
	movq	%rcx, 26(%rdi)
	movl	%edx, 20(%rdi)
	movl	%edx, 16(%rdi)
	movl	%edx, 12(%rdi)
	movl	$15726, %edx
	movw	%dx, 38(%rdi)
.L492:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	call	uprv_free_67@PLT
	movq	$0, 16(%r12)
	movl	$1, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L494:
	.cfi_restore_state
	movl	$7, 0(%r13)
	jmp	.L492
	.cfi_endproc
.LFE2115:
	.size	_ISCIIOpen, .-_ISCIIOpen
	.globl	_ISCIIData_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ISCIIData_67, @object
	.size	_ISCIIData_67, 296
_ISCIIData_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL16_ISCIIStaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL10_ISCIIImpl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL16_ISCIIStaticData, @object
	.size	_ZL16_ISCIIStaticData, 100
_ZL16_ISCIIStaticData:
	.long	100
	.string	"ISCII"
	.zero	54
	.long	0
	.byte	0
	.byte	25
	.byte	1
	.byte	4
	.string	"\032"
	.zero	2
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL10_ISCIIImpl, @object
	.size	_ZL10_ISCIIImpl, 144
_ZL10_ISCIIImpl:
	.long	25
	.zero	4
	.quad	0
	.quad	0
	.quad	_ISCIIOpen
	.quad	_ISCIIClose
	.quad	_ISCIIReset
	.quad	UConverter_toUnicode_ISCII_OFFSETS_LOGIC
	.quad	UConverter_toUnicode_ISCII_OFFSETS_LOGIC
	.quad	UConverter_fromUnicode_ISCII_OFFSETS_LOGIC
	.quad	UConverter_fromUnicode_ISCII_OFFSETS_LOGIC
	.quad	0
	.quad	0
	.quad	_ISCIIgetName
	.quad	0
	.quad	_ISCII_SafeClone
	.quad	_ISCIIGetUnicodeSet
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL11lookupTable, @object
	.size	_ZL11lookupTable, 48
_ZL11lookupTable:
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	128
	.value	1
	.value	8
	.value	5
	.value	1
	.value	6
	.value	4
	.value	1
	.value	8
	.value	4
	.value	16
	.value	7
	.value	4
	.value	8
	.value	2
	.value	3
	.value	32
	.value	2
	.value	64
	.align 32
	.type	_ZL14toUnicodeTable, @object
	.size	_ZL14toUnicodeTable, 512
_ZL14toUnicodeTable:
	.value	0
	.value	1
	.value	2
	.value	3
	.value	4
	.value	5
	.value	6
	.value	7
	.value	8
	.value	9
	.value	10
	.value	11
	.value	12
	.value	13
	.value	14
	.value	15
	.value	16
	.value	17
	.value	18
	.value	19
	.value	20
	.value	21
	.value	22
	.value	23
	.value	24
	.value	25
	.value	26
	.value	27
	.value	28
	.value	29
	.value	30
	.value	31
	.value	32
	.value	33
	.value	34
	.value	35
	.value	36
	.value	37
	.value	38
	.value	39
	.value	40
	.value	41
	.value	42
	.value	43
	.value	44
	.value	45
	.value	46
	.value	47
	.value	48
	.value	49
	.value	50
	.value	51
	.value	52
	.value	53
	.value	54
	.value	55
	.value	56
	.value	57
	.value	58
	.value	59
	.value	60
	.value	61
	.value	62
	.value	63
	.value	64
	.value	65
	.value	66
	.value	67
	.value	68
	.value	69
	.value	70
	.value	71
	.value	72
	.value	73
	.value	74
	.value	75
	.value	76
	.value	77
	.value	78
	.value	79
	.value	80
	.value	81
	.value	82
	.value	83
	.value	84
	.value	85
	.value	86
	.value	87
	.value	88
	.value	89
	.value	90
	.value	91
	.value	92
	.value	93
	.value	94
	.value	95
	.value	96
	.value	97
	.value	98
	.value	99
	.value	100
	.value	101
	.value	102
	.value	103
	.value	104
	.value	105
	.value	106
	.value	107
	.value	108
	.value	109
	.value	110
	.value	111
	.value	112
	.value	113
	.value	114
	.value	115
	.value	116
	.value	117
	.value	118
	.value	119
	.value	120
	.value	121
	.value	122
	.value	123
	.value	124
	.value	125
	.value	126
	.value	127
	.value	128
	.value	129
	.value	130
	.value	131
	.value	132
	.value	133
	.value	134
	.value	135
	.value	136
	.value	137
	.value	138
	.value	139
	.value	140
	.value	141
	.value	142
	.value	143
	.value	144
	.value	145
	.value	146
	.value	147
	.value	148
	.value	149
	.value	150
	.value	151
	.value	152
	.value	153
	.value	154
	.value	155
	.value	156
	.value	157
	.value	158
	.value	159
	.value	160
	.value	2305
	.value	2306
	.value	2307
	.value	2309
	.value	2310
	.value	2311
	.value	2312
	.value	2313
	.value	2314
	.value	2315
	.value	2318
	.value	2319
	.value	2320
	.value	2317
	.value	2322
	.value	2323
	.value	2324
	.value	2321
	.value	2325
	.value	2326
	.value	2327
	.value	2328
	.value	2329
	.value	2330
	.value	2331
	.value	2332
	.value	2333
	.value	2334
	.value	2335
	.value	2336
	.value	2337
	.value	2338
	.value	2339
	.value	2340
	.value	2341
	.value	2342
	.value	2343
	.value	2344
	.value	2345
	.value	2346
	.value	2347
	.value	2348
	.value	2349
	.value	2350
	.value	2351
	.value	2399
	.value	2352
	.value	2353
	.value	2354
	.value	2355
	.value	2356
	.value	2357
	.value	2358
	.value	2359
	.value	2360
	.value	2361
	.value	8205
	.value	2366
	.value	2367
	.value	2368
	.value	2369
	.value	2370
	.value	2371
	.value	2374
	.value	2375
	.value	2376
	.value	2373
	.value	2378
	.value	2379
	.value	2380
	.value	2377
	.value	2381
	.value	2364
	.value	2404
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	2406
	.value	2407
	.value	2408
	.value	2409
	.value	2410
	.value	2411
	.value	2412
	.value	2413
	.value	2414
	.value	2415
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.align 32
	.type	_ZL16fromUnicodeTable, @object
	.size	_ZL16fromUnicodeTable, 256
_ZL16fromUnicodeTable:
	.value	160
	.value	161
	.value	162
	.value	163
	.value	-23328
	.value	164
	.value	165
	.value	166
	.value	167
	.value	168
	.value	169
	.value	170
	.value	-22807
	.value	174
	.value	171
	.value	172
	.value	173
	.value	178
	.value	175
	.value	176
	.value	177
	.value	179
	.value	180
	.value	181
	.value	182
	.value	183
	.value	184
	.value	185
	.value	186
	.value	187
	.value	188
	.value	189
	.value	190
	.value	191
	.value	192
	.value	193
	.value	194
	.value	195
	.value	196
	.value	197
	.value	198
	.value	199
	.value	200
	.value	201
	.value	202
	.value	203
	.value	204
	.value	205
	.value	207
	.value	208
	.value	209
	.value	210
	.value	211
	.value	212
	.value	213
	.value	214
	.value	215
	.value	216
	.value	-1
	.value	-1
	.value	233
	.value	-5399
	.value	218
	.value	219
	.value	220
	.value	221
	.value	222
	.value	223
	.value	-8215
	.value	227
	.value	224
	.value	225
	.value	226
	.value	231
	.value	228
	.value	229
	.value	230
	.value	232
	.value	236
	.value	237
	.value	-24087
	.value	-1
	.value	-3912
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-19479
	.value	-19223
	.value	-18967
	.value	-17687
	.value	-16407
	.value	-16151
	.value	-13847
	.value	206
	.value	-21783
	.value	-22551
	.value	-9239
	.value	-8983
	.value	234
	.value	-5398
	.value	241
	.value	242
	.value	243
	.value	244
	.value	245
	.value	246
	.value	247
	.value	248
	.value	249
	.value	250
	.value	-3905
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.value	-1
	.align 32
	.type	_ZL13validityTable, @object
	.size	_ZL13validityTable, 128
_ZL13validityTable:
	.string	""
	.string	"\370\377\377\200\377\377\377\377\377\377\276\236\240\207\377\377\240\207\377\377\377\376\376\376\377\377\376\377\376\377\377\376\376\376\377\377\376\376\376\377\201\377\376\376\376\377\377\377\203\377\367\203\367\376\277\377\377"
	.string	""
	.string	"\330\200\377\377\377\377\377\276\254\240\207\377\377\240\207\377\377\377"
	.string	""
	.string	"\240\200\200\200\200\004\024\032\200\300\300\300\310\230\300\230\276\236\210\210\200\200\377\377\377\377\377\377\377\377\377\377\300"
	.string	""
	.zero	13
	.align 32
	.type	_ZL6pnjMap, @object
	.size	_ZL6pnjMap, 80
_ZL6pnjMap:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\002"
	.string	"\002"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"
	.string	"\003\003\003\003\003\003\003"
	.string	""
	.string	""
	.string	""
	.string	"\003\003"
	.string	"\003\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\002"
	.string	"\002\002"
	.zero	12
	.align 32
	.type	_ZL17lookupInitialData, @object
	.size	_ZL17lookupInitialData, 108
_ZL17lookupInitialData:
	.long	0
	.long	128
	.long	66
	.long	1
	.long	8
	.long	67
	.long	2
	.long	64
	.long	75
	.long	3
	.long	32
	.long	74
	.long	4
	.long	16
	.long	71
	.long	5
	.long	1
	.long	68
	.long	6
	.long	4
	.long	69
	.long	7
	.long	4
	.long	72
	.long	8
	.long	2
	.long	73
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
