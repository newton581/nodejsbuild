	.file	"filteredbrk.cpp"
	.text
	.section	.text._ZNK6icu_6735SimpleFilteredSentenceBreakIterator17getDynamicClassIDEv,"axG",@progbits,_ZNK6icu_6735SimpleFilteredSentenceBreakIterator17getDynamicClassIDEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator17getDynamicClassIDEv
	.type	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator17getDynamicClassIDEv, @function
_ZNK6icu_6735SimpleFilteredSentenceBreakIterator17getDynamicClassIDEv:
.LFB2596:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2596:
	.size	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator17getDynamicClassIDEv, .-_ZNK6icu_6735SimpleFilteredSentenceBreakIterator17getDynamicClassIDEv
	.section	.text._ZNK6icu_6735SimpleFilteredSentenceBreakIteratoreqERKNS_13BreakIteratorE,"axG",@progbits,_ZNK6icu_6735SimpleFilteredSentenceBreakIteratoreqERKNS_13BreakIteratorE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6735SimpleFilteredSentenceBreakIteratoreqERKNS_13BreakIteratorE
	.type	_ZNK6icu_6735SimpleFilteredSentenceBreakIteratoreqERKNS_13BreakIteratorE, @function
_ZNK6icu_6735SimpleFilteredSentenceBreakIteratoreqERKNS_13BreakIteratorE:
.LFB2597:
	.cfi_startproc
	endbr64
	cmpq	%rdi, %rsi
	sete	%al
	ret
	.cfi_endproc
.LFE2597:
	.size	_ZNK6icu_6735SimpleFilteredSentenceBreakIteratoreqERKNS_13BreakIteratorE, .-_ZNK6icu_6735SimpleFilteredSentenceBreakIteratoreqERKNS_13BreakIteratorE
	.section	.text._ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextEP5UTextR10UErrorCode,"axG",@progbits,_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextEP5UTextR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextEP5UTextR10UErrorCode
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextEP5UTextR10UErrorCode, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextEP5UTextR10UErrorCode:
.LFB2598:
	.cfi_startproc
	endbr64
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*64(%rax)
	.cfi_endproc
.LFE2598:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextEP5UTextR10UErrorCode, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextEP5UTextR10UErrorCode
	.section	.text._ZN6icu_6735SimpleFilteredSentenceBreakIterator16refreshInputTextEP5UTextR10UErrorCode,"axG",@progbits,_ZN6icu_6735SimpleFilteredSentenceBreakIterator16refreshInputTextEP5UTextR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16refreshInputTextEP5UTextR10UErrorCode
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16refreshInputTextEP5UTextR10UErrorCode, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator16refreshInputTextEP5UTextR10UErrorCode:
.LFB2599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	call	*176(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2599:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16refreshInputTextEP5UTextR10UErrorCode, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator16refreshInputTextEP5UTextR10UErrorCode
	.section	.text._ZN6icu_6735SimpleFilteredSentenceBreakIterator9adoptTextEPNS_17CharacterIteratorE,"axG",@progbits,_ZN6icu_6735SimpleFilteredSentenceBreakIterator9adoptTextEPNS_17CharacterIteratorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9adoptTextEPNS_17CharacterIteratorE
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9adoptTextEPNS_17CharacterIteratorE, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator9adoptTextEPNS_17CharacterIteratorE:
.LFB2600:
	.cfi_startproc
	endbr64
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE2600:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9adoptTextEPNS_17CharacterIteratorE, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator9adoptTextEPNS_17CharacterIteratorE
	.section	.text._ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextERKNS_13UnicodeStringE,"axG",@progbits,_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextERKNS_13UnicodeStringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextERKNS_13UnicodeStringE
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextERKNS_13UnicodeStringE, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextERKNS_13UnicodeStringE:
.LFB2601:
	.cfi_startproc
	endbr64
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*56(%rax)
	.cfi_endproc
.LFE2601:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextERKNS_13UnicodeStringE, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextERKNS_13UnicodeStringE
	.section	.text._ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode,"axG",@progbits,_ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode
	.type	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode, @function
_ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode:
.LFB2602:
	.cfi_startproc
	endbr64
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*48(%rax)
	.cfi_endproc
.LFE2602:
	.size	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode, .-_ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode
	.section	.text._ZNK6icu_6735SimpleFilteredSentenceBreakIterator7getTextEv,"axG",@progbits,_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7getTextEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7getTextEv
	.type	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7getTextEv, @function
_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7getTextEv:
.LFB2603:
	.cfi_startproc
	endbr64
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE2603:
	.size	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7getTextEv, .-_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7getTextEv
	.section	.text._ZNK6icu_6735SimpleFilteredSentenceBreakIterator7currentEv,"axG",@progbits,_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7currentEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7currentEv
	.type	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7currentEv, @function
_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7currentEv:
.LFB2604:
	.cfi_startproc
	endbr64
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*112(%rax)
	.cfi_endproc
.LFE2604:
	.size	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7currentEv, .-_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7currentEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator5firstEv
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator5firstEv, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator5firstEv:
.LFB2622:
	.cfi_startproc
	endbr64
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*80(%rax)
	.cfi_endproc
.LFE2622:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator5firstEv, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator5firstEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4lastEv
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4lastEv, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator4lastEv:
.LFB2628:
	.cfi_startproc
	endbr64
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*88(%rax)
	.cfi_endproc
.LFE2628:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4lastEv, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator4lastEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UStringSetD2Ev
	.type	_ZN6icu_6710UStringSetD2Ev, @function
_ZN6icu_6710UStringSetD2Ev:
.LFB2580:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6710UStringSetE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UVectorD2Ev@PLT
	.cfi_endproc
.LFE2580:
	.size	_ZN6icu_6710UStringSetD2Ev, .-_ZN6icu_6710UStringSetD2Ev
	.globl	_ZN6icu_6710UStringSetD1Ev
	.set	_ZN6icu_6710UStringSetD1Ev,_ZN6icu_6710UStringSetD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UStringSetD0Ev
	.type	_ZN6icu_6710UStringSetD0Ev, @function
_ZN6icu_6710UStringSetD0Ev:
.LFB2582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710UStringSetE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UVectorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2582:
	.size	_ZN6icu_6710UStringSetD0Ev, .-_ZN6icu_6710UStringSetD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder20unsuppressBreakAfterERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder20unsuppressBreakAfterERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6734SimpleFilteredBreakIteratorBuilder20unsuppressBreakAfterERKNS_13UnicodeStringER10UErrorCode:
.LFB2640:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	jle	.L19
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$8, %rdi
	jmp	_ZN6icu_677UVector13removeElementEPv@PLT
	.cfi_endproc
.LFE2640:
	.size	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder20unsuppressBreakAfterERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6734SimpleFilteredBreakIteratorBuilder20unsuppressBreakAfterERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD2Ev
	.type	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD2Ev, @function
_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD2Ev:
.LFB2630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710UStringSetE(%rip), %rax
	leaq	16+_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE(%rip), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_677UVectorD2Ev@PLT
	leaq	16+_ZTVN6icu_6728FilteredBreakIteratorBuilderE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2630:
	.size	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD2Ev, .-_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD2Ev
	.globl	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD1Ev
	.set	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD1Ev,_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD0Ev
	.type	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD0Ev, @function
_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD0Ev:
.LFB2632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6710UStringSetE(%rip), %rax
	leaq	16+_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE(%rip), %rdx
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movups	%xmm0, -8(%rdi)
	call	_ZN6icu_677UVectorD2Ev@PLT
	leaq	16+_ZTVN6icu_6728FilteredBreakIteratorBuilderE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2632:
	.size	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD0Ev, .-_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD0Ev
	.p2align 4
	.type	_ZN6icu_67L20compareUnicodeStringE8UElementS0_, @function
_ZN6icu_67L20compareUnicodeStringE8UElementS0_:
.LFB2570:
	.cfi_startproc
	endbr64
	movzwl	8(%rsi), %r10d
	testw	%r10w, %r10w
	js	.L25
	movswl	%r10w, %ecx
	sarl	$5, %ecx
.L26:
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L27
	movswl	%ax, %edx
	sarl	$5, %edx
.L28:
	testb	$1, %r10b
	je	.L29
	notl	%eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	testl	%ecx, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	cmovle	%ecx, %r8d
	js	.L31
	movl	%ecx, %r9d
	subl	%r8d, %r9d
	cmpl	%ecx, %r9d
	cmovg	%ecx, %r9d
.L31:
	andl	$2, %r10d
	leaq	10(%rsi), %rcx
	jne	.L33
	movq	24(%rsi), %rcx
.L33:
	xorl	%esi, %esi
	jmp	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	movl	12(%rdi), %edx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L25:
	movl	12(%rsi), %ecx
	jmp	.L26
	.cfi_endproc
.LFE2570:
	.size	_ZN6icu_67L20compareUnicodeStringE8UElementS0_, .-_ZN6icu_67L20compareUnicodeStringE8UElementS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6731SimpleFilteredSentenceBreakDataD2Ev
	.type	_ZN6icu_6731SimpleFilteredSentenceBreakDataD2Ev, @function
_ZN6icu_6731SimpleFilteredSentenceBreakDataD2Ev:
.LFB2591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L37
	movq	%r12, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L37:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L36
	movq	%r12, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2591:
	.size	_ZN6icu_6731SimpleFilteredSentenceBreakDataD2Ev, .-_ZN6icu_6731SimpleFilteredSentenceBreakDataD2Ev
	.globl	_ZN6icu_6731SimpleFilteredSentenceBreakDataD1Ev
	.set	_ZN6icu_6731SimpleFilteredSentenceBreakDataD1Ev,_ZN6icu_6731SimpleFilteredSentenceBreakDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6731SimpleFilteredSentenceBreakDataD0Ev
	.type	_ZN6icu_6731SimpleFilteredSentenceBreakDataD0Ev, @function
_ZN6icu_6731SimpleFilteredSentenceBreakDataD0Ev:
.LFB2593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L44
	movq	%r13, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L44:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L45
	movq	%r13, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L45:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2593:
	.size	_ZN6icu_6731SimpleFilteredSentenceBreakDataD0Ev, .-_ZN6icu_6731SimpleFilteredSentenceBreakDataD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder18suppressBreakAfterERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder18suppressBreakAfterERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6734SimpleFilteredBreakIteratorBuilder18suppressBreakAfterERKNS_13UnicodeStringER10UErrorCode:
.LFB2639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L62
.L53:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	$64, %edi
	movq	%rsi, %r13
	movq	%rdx, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L55
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L56
.L61:
	movq	(%r14), %rax
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	call	*8(%rax)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L56:
	addq	$8, %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	jns	.L61
	movq	%rbx, %rcx
	leaq	_ZN6icu_67L20compareUnicodeStringE8UElementS0_(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_677UVector12sortedInsertEPvPFa8UElementS2_ER10UErrorCode@PLT
	movl	(%rbx), %eax
	movl	$1, %r15d
	testl	%eax, %eax
	jg	.L61
	jmp	.L53
.L55:
	movl	$7, (%rbx)
	jmp	.L53
	.cfi_endproc
.LFE2639:
	.size	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder18suppressBreakAfterERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6734SimpleFilteredBreakIteratorBuilder18suppressBreakAfterERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD2Ev
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD2Ev, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD2Ev:
.LFB2614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	328(%rdi), %r13
	movq	%rax, (%rdi)
	movl	24(%r13), %eax
	subl	$1, %eax
	movl	%eax, 24(%r13)
	testl	%eax, %eax
	jle	.L84
.L64:
	movq	344(%r12), %rdi
	movq	$0, 328(%r12)
	testq	%rdi, %rdi
	je	.L68
	call	utext_close_67@PLT
.L68:
	movq	336(%r12), %r13
	testq	%r13, %r13
	je	.L69
	movq	0(%r13), %rax
	leaq	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L70
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L69:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713BreakIteratorD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	0(%r13), %rax
	leaq	_ZN6icu_6731SimpleFilteredSentenceBreakDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L65
	movq	16(%r13), %r14
	leaq	16+_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L66
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L66:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L67
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L67:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L70:
	call	*%rax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L64
	.cfi_endproc
.LFE2614:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD2Ev, .-_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD2Ev
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD1Ev
	.set	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD1Ev,_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD0Ev
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD0Ev, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD0Ev:
.LFB2616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	328(%rdi), %r13
	movq	%rax, (%rdi)
	movl	24(%r13), %eax
	subl	$1, %eax
	movl	%eax, 24(%r13)
	testl	%eax, %eax
	jle	.L106
.L86:
	movq	344(%r12), %rdi
	movq	$0, 328(%r12)
	testq	%rdi, %rdi
	je	.L90
	call	utext_close_67@PLT
.L90:
	movq	336(%r12), %r13
	testq	%r13, %r13
	je	.L91
	movq	0(%r13), %rax
	leaq	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L92
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD1Ev
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L91:
	movq	%r12, %rdi
	call	_ZN6icu_6713BreakIteratorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movq	0(%r13), %rax
	leaq	_ZN6icu_6731SimpleFilteredSentenceBreakDataD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L87
	movq	16(%r13), %r14
	leaq	16+_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L88
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L88:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L89
	movq	%r14, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L89:
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L92:
	call	*%rax
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L86
	.cfi_endproc
.LFE2616:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD0Ev, .-_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2ERKS0_
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2ERKS0_, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2ERKS0_:
.LFB2608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	leaq	16+_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE(%rip), %rax
	movq	336(%r12), %r13
	leaq	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv(%rip), %rdx
	movq	%rax, (%rbx)
	movq	328(%r12), %rax
	addl	$1, 24(%rax)
	movq	%rax, 328(%rbx)
	movq	0(%r13), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L108
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L109
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC1ERKS0_
.L109:
	movq	%r12, 336(%rbx)
	movq	$0, 344(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L109
	.cfi_endproc
.LFE2608:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2ERKS0_, .-_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2ERKS0_
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC1ERKS0_
	.set	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC1ERKS0_,_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2ERKS0_
	.section	.text._ZN6icu_6735SimpleFilteredSentenceBreakIterator17createBufferCloneEPvRiR10UErrorCode,"axG",@progbits,_ZN6icu_6735SimpleFilteredSentenceBreakIterator17createBufferCloneEPvRiR10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6735SimpleFilteredSentenceBreakIterator17createBufferCloneEPvRiR10UErrorCode
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator17createBufferCloneEPvRiR10UErrorCode, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator17createBufferCloneEPvRiR10UErrorCode:
.LFB2594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movl	$-126, (%rcx)
	movq	32(%rax), %rbx
	leaq	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L115
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L114
	movq	%r13, %rsi
	movq	%rax, %rdi
	leaq	16+_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE(%rip), %r14
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	movq	328(%r13), %rax
	movq	%r14, (%r12)
	movq	336(%r13), %r13
	addl	$1, 24(%rax)
	movq	%rax, 328(%r12)
	movq	0(%r13), %rax
	movq	32(%rax), %r15
	cmpq	%rbx, %r15
	jne	.L117
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L118
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	movq	328(%r13), %rax
	movq	%r14, (%rbx)
	movq	336(%r13), %r13
	addl	$1, 24(%rax)
	movq	%rax, 328(%rbx)
	movq	0(%r13), %rax
	movq	32(%rax), %rax
	movq	%rax, -56(%rbp)
	cmpq	%r15, %rax
	jne	.L119
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L120
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	movq	328(%r13), %rax
	movq	336(%r13), %r8
	movq	%r14, (%r15)
	addl	$1, 24(%rax)
	movq	%rax, 328(%r15)
	movq	(%r8), %rax
	movq	32(%rax), %rdx
	movq	%rdx, -64(%rbp)
	cmpq	-56(%rbp), %rdx
	jne	.L121
	movl	$352, %edi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L122
	movq	-56(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	movq	-56(%rbp), %r8
	movq	%r14, 0(%r13)
	movq	328(%r8), %rax
	movq	336(%r8), %rsi
	addl	$1, 24(%rax)
	movq	%rax, 328(%r13)
	movq	(%rsi), %rax
	movq	32(%rax), %rax
	cmpq	-64(%rbp), %rax
	jne	.L123
	movl	$352, %edi
	movq	%rsi, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L124
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC1ERKS0_
.L124:
	movq	%r14, 336(%r13)
	movq	$0, 344(%r13)
.L122:
	movq	%r13, 336(%r15)
	movq	$0, 344(%r15)
.L120:
	movq	%r15, 336(%rbx)
	movq	$0, 344(%rbx)
.L118:
	movq	%rbx, 336(%r12)
	movq	$0, 344(%r12)
.L114:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	%r8, %rdi
	call	*%rdx
	movq	%rax, %r13
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r13, %rdi
	call	*%r15
	movq	%rax, %rbx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %r15
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%rsi, %rdi
	call	*%rax
	movq	%rax, %r14
	jmp	.L124
	.cfi_endproc
.LFE2594:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator17createBufferCloneEPvRiR10UErrorCode, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator17createBufferCloneEPvRiR10UErrorCode
	.section	.text._ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv,"axG",@progbits,_ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv
	.type	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv, @function
_ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv:
.LFB2595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$352, %edi
	subq	$24, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L141
	movq	%rax, %rdi
	movq	%rbx, %rsi
	leaq	16+_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE(%rip), %r13
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	movq	328(%rbx), %rax
	movq	336(%rbx), %r14
	movq	%r13, (%r12)
	addl	$1, 24(%rax)
	movq	%rax, 328(%r12)
	movq	(%r14), %rax
	movq	32(%rax), %r15
	leaq	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv(%rip), %rax
	cmpq	%rax, %r15
	jne	.L143
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L144
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	movq	328(%r14), %rax
	movq	%r13, (%rbx)
	movq	336(%r14), %r14
	addl	$1, 24(%rax)
	movq	%rax, 328(%rbx)
	movq	(%r14), %rax
	movq	32(%rax), %rax
	movq	%rax, -56(%rbp)
	cmpq	%r15, %rax
	jne	.L145
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L146
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	movq	328(%r14), %rax
	movq	336(%r14), %r8
	movq	%r13, (%r15)
	addl	$1, 24(%rax)
	movq	%rax, 328(%r15)
	movq	(%r8), %rax
	movq	32(%rax), %rdx
	movq	%rdx, -64(%rbp)
	cmpq	-56(%rbp), %rdx
	jne	.L147
	movl	$352, %edi
	movq	%r8, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L148
	movq	-56(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN6icu_6713BreakIteratorC2ERKS0_@PLT
	movq	-56(%rbp), %r8
	movq	%r13, (%r14)
	movq	328(%r8), %rax
	movq	336(%r8), %rsi
	addl	$1, 24(%rax)
	movq	%rax, 328(%r14)
	movq	(%rsi), %rax
	movq	32(%rax), %rax
	cmpq	-64(%rbp), %rax
	jne	.L149
	movl	$352, %edi
	movq	%rsi, -56(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L150
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC1ERKS0_
.L150:
	movq	%r13, 336(%r14)
	movq	$0, 344(%r14)
.L148:
	movq	%r14, 336(%r15)
	movq	$0, 344(%r15)
.L146:
	movq	%r15, 336(%rbx)
	movq	$0, 344(%rbx)
.L144:
	movq	%rbx, 336(%r12)
	movq	$0, 344(%r12)
.L141:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	*%rax
	movq	%rax, %r13
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%r14, %rdi
	call	*%r15
	movq	%rax, %rbx
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r14, %rdi
	call	*%rax
	movq	%rax, %r15
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L147:
	movq	%r8, %rdi
	call	*%rdx
	movq	%rax, %r14
	jmp	.L148
	.cfi_endproc
.LFE2595:
	.size	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv, .-_ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode:
.LFB2617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode(%rip), %rcx
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	336(%rdi), %rdi
	movq	344(%rbx), %rsi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	movq	$0, 344(%rbx)
	cmpq	%rcx, %rax
	jne	.L168
	movq	336(%rdi), %rdi
	movq	(%rdi), %rcx
	movq	48(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L169
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L168
	movq	336(%rdi), %rdi
	movq	(%rdi), %rcx
	movq	48(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L169
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%rax, %r12
.L172:
	movq	344(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L173
	call	utext_close_67@PLT
.L173:
	movq	%r12, 344(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	call	*%rcx
	movq	%rax, %r12
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L168:
	call	*%rax
	movq	%rax, %r12
	jmp	.L172
	.cfi_endproc
.LFE2617:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi:
.LFB2618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	344(%rdi), %rdi
	call	utext_setNativeIndex_67@PLT
	movq	328(%r13), %rax
	movq	344(%r13), %rdi
	movq	16(%rax), %rax
	movq	8(%rax), %rdx
	movl	$-1, 24(%rax)
	movq	%rdx, 16(%rax)
	call	utext_previous32_67@PLT
	cmpl	$32, %eax
	je	.L179
	movq	344(%r13), %rdi
	call	utext_next32_67@PLT
.L179:
	movl	$-1, %ebx
	movq	$-1, %r12
	.p2align 4,,10
	.p2align 3
.L184:
	movq	344(%r13), %rdi
	call	utext_previous32_67@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L181
	movq	328(%r13), %rax
	movq	16(%rax), %rdi
	call	_ZN6icu_6710UCharsTrie16nextForCodePointEi@PLT
	testb	$1, %al
	je	.L182
	cmpl	$1, %eax
	jle	.L184
	movq	344(%r13), %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rax, %r12
	movq	328(%r13), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rdx
	movzwl	(%rdx), %eax
	testw	%ax, %ax
	js	.L221
	movzwl	%ax, %ecx
	cmpw	$16447, %ax
	ja	.L189
	sarl	$6, %ecx
	leal	-1(%rcx), %ebx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L221:
	movl	%eax, %ecx
	movl	%eax, %ebx
	andw	$32767, %cx
	andl	$32767, %ebx
	testb	$64, %ah
	je	.L184
	movzwl	2(%rdx), %eax
	cmpw	$32767, %cx
	je	.L188
	subl	$16384, %ebx
	sall	$16, %ebx
	orl	%eax, %ebx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L189:
	movzwl	2(%rdx), %ebx
	cmpl	$32703, %ecx
	jg	.L190
	andl	$32704, %eax
	subl	$16448, %eax
	sall	$10, %eax
	orl	%eax, %ebx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L182:
	testl	%eax, %eax
	jne	.L181
.L185:
	xorl	%eax, %eax
	testq	%r12, %r12
	js	.L178
	cmpl	$2, %ebx
	je	.L198
	cmpl	$1, %ebx
	jne	.L178
	movq	328(%r13), %rdx
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L178
	movq	8(%rdx), %rax
	movl	$-1, 24(%rdx)
	movq	%r12, %rsi
	movq	344(%r13), %rdi
	movq	%rax, 16(%rdx)
	call	utext_setNativeIndex_67@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L223:
	movq	328(%r13), %rax
	movq	8(%rax), %rdi
	call	_ZN6icu_6710UCharsTrie16nextForCodePointEi@PLT
	testb	$1, %al
	je	.L222
.L199:
	movq	344(%r13), %rdi
	call	utext_next32_67@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	jne	.L223
.L198:
	movl	$1, %eax
.L178:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	328(%r13), %rax
	movq	16(%rax), %rax
	movq	16(%rax), %rdx
	movzwl	(%rdx), %eax
	testw	%ax, %ax
	js	.L224
	movzwl	%ax, %ecx
	cmpw	$16447, %ax
	ja	.L194
	sarl	$6, %ecx
	leal	-1(%rcx), %ebx
.L192:
	movq	344(%r13), %rdi
	call	utext_getNativeIndex_67@PLT
	movq	%rax, %r12
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L190:
	movzwl	4(%rdx), %eax
	sall	$16, %ebx
	orl	%eax, %ebx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L188:
	movzwl	4(%rdx), %ebx
	sall	$16, %eax
	orl	%eax, %ebx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L224:
	movl	%eax, %ecx
	movl	%eax, %ebx
	andw	$32767, %cx
	andl	$32767, %ebx
	testb	$64, %ah
	je	.L192
	movzwl	2(%rdx), %eax
	cmpw	$32767, %cx
	je	.L193
	subl	$16384, %ebx
	sall	$16, %ebx
	orl	%eax, %ebx
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L222:
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movzwl	2(%rdx), %ebx
	cmpl	$32703, %ecx
	jg	.L195
	andl	$32704, %eax
	subl	$16448, %eax
	sall	$10, %eax
	orl	%eax, %ebx
	jmp	.L192
.L195:
	movzwl	4(%rdx), %eax
	sall	$16, %ebx
	orl	%eax, %ebx
	jmp	.L192
.L193:
	movzwl	4(%rdx), %ebx
	sall	$16, %eax
	orl	%eax, %ebx
	jmp	.L192
	.cfi_endproc
.LFE2618:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10isBoundaryEi
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10isBoundaryEi, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator10isBoundaryEi:
.LFB2625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	336(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*136(%rax)
	testb	%al, %al
	je	.L225
	movq	328(%r12), %rdx
	movl	$1, %eax
	cmpq	$0, 16(%rdx)
	je	.L225
	leaq	-28(%rbp), %rsi
	movq	%r12, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi
	cmpl	$1, %eax
	setne	%al
.L225:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L233
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L233:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2625:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10isBoundaryEi, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator10isBoundaryEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEi
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEi, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEi:
.LFB2626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	336(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*144(%rax)
	cmpl	$-1, %eax
	je	.L235
	movl	%eax, %r12d
	movq	328(%rbx), %rax
	cmpq	$0, 16(%rax)
	je	.L234
	leaq	-44(%rbp), %rsi
	movq	%rbx, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L235
	movq	344(%rbx), %rdi
	call	utext_nativeLength_67@PLT
	movq	%rax, %r13
.L237:
	movslq	%r12d, %rax
	cmpq	%rax, %r13
	je	.L234
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi
	cmpl	$1, %eax
	je	.L244
.L234:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L245
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	movq	336(%rbx), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movl	%eax, %r12d
	cmpl	$-1, %eax
	jne	.L237
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$-1, %r12d
	jmp	.L234
.L245:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2626:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEi, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9followingEi
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9followingEi, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator9followingEi:
.LFB2627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	336(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*120(%rax)
	cmpl	$-1, %eax
	je	.L247
	movl	%eax, %r12d
	movq	328(%rbx), %rax
	cmpq	$0, 16(%rax)
	je	.L246
	leaq	-44(%rbp), %rsi
	movq	%rbx, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L247
	movq	344(%rbx), %rdi
	call	utext_nativeLength_67@PLT
	movq	%rax, %r13
.L249:
	movslq	%r12d, %rax
	cmpq	%rax, %r13
	je	.L246
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi
	cmpl	$1, %eax
	je	.L256
.L246:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	336(%rbx), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movl	%eax, %r12d
	cmpl	$-1, %eax
	jne	.L249
	.p2align 4,,10
	.p2align 3
.L247:
	movl	$-1, %r12d
	jmp	.L246
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2627:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9followingEi, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator9followingEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEv
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEv, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEv:
.LFB2621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	336(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*104(%rax)
	cmpl	$-1, %eax
	je	.L259
	movl	%eax, %r12d
	movq	328(%rbx), %rax
	cmpq	$0, 16(%rax)
	je	.L258
	leaq	-44(%rbp), %rsi
	movq	%rbx, %rdi
	movl	$0, -44(%rbp)
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode
	movl	-44(%rbp), %eax
	testl	%eax, %eax
	jg	.L259
	movq	344(%rbx), %rdi
	call	utext_nativeLength_67@PLT
	movq	%rax, %r13
.L261:
	movslq	%r12d, %rax
	cmpq	%rax, %r13
	je	.L258
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi
	cmpl	$1, %eax
	je	.L268
.L258:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L269
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	336(%rbx), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movl	%eax, %r12d
	cmpl	$-1, %eax
	jne	.L261
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$-1, %r12d
	jmp	.L258
.L269:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2621:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEv, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator8previousEv
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator8previousEv, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator8previousEv:
.LFB2624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	336(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*96(%rax)
	movl	%eax, %r12d
	addl	$1, %eax
	cmpl	$1, %eax
	jbe	.L270
	movq	328(%rbx), %rax
	cmpq	$0, 16(%rax)
	je	.L270
	leaq	-28(%rbp), %rsi
	movq	%rbx, %rdi
	movl	$0, -28(%rbp)
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L274
.L272:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi
	cmpl	$1, %eax
	je	.L277
.L270:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movq	336(%rbx), %rdi
	movq	(%rdi), %rax
	call	*96(%rax)
	movl	%eax, %r12d
	leal	1(%rax), %eax
	cmpl	$1, %eax
	ja	.L272
	jmp	.L270
.L274:
	movl	$-1, %r12d
	jmp	.L270
.L278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2624:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator8previousEv, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator8previousEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalNextEi
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalNextEi, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalNextEi:
.LFB2619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L280
	movq	328(%rdi), %rax
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movl	%esi, %r13d
	cmpq	$0, 16(%rax)
	je	.L279
	movq	336(%rdi), %rdi
	leaq	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode(%rip), %rdx
	movq	344(%rbx), %rsi
	movl	$0, -44(%rbp)
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	movq	$0, 344(%rbx)
	cmpq	%rdx, %rax
	jne	.L282
	movq	336(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	48(%rdx), %rcx
	cmpq	%rax, %rcx
	jne	.L283
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L282
	movq	336(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	48(%rdx), %rcx
	cmpq	%rax, %rcx
	jne	.L283
	movq	336(%rdi), %rdi
	leaq	-44(%rbp), %rdx
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%rax, %r13
.L286:
	movq	344(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L287
	call	utext_close_67@PLT
.L287:
	movl	-44(%rbp), %eax
	movq	%r13, 344(%rbx)
	testl	%eax, %eax
	jg	.L280
	movq	%r13, %rdi
	movl	%r12d, %r13d
	call	utext_nativeLength_67@PLT
	movq	%rax, %r14
.L288:
	movslq	%r13d, %rax
	cmpq	%rax, %r14
	je	.L279
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi
	cmpl	$1, %eax
	je	.L299
.L279:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L300
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movq	336(%rbx), %rdi
	movq	(%rdi), %rax
	call	*104(%rax)
	movl	%eax, %r13d
	cmpl	$-1, %eax
	jne	.L288
	.p2align 4,,10
	.p2align 3
.L280:
	movl	$-1, %r13d
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L282:
	leaq	-44(%rbp), %rdx
	call	*%rax
	movq	%rax, %r13
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	-44(%rbp), %rdx
	call	*%rcx
	movq	%rax, %r13
	jmp	.L286
.L300:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2619:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalNextEi, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalNextEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalPrevEi
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalPrevEi, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalPrevEi:
.LFB2620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	1(%rsi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%esi, %eax
	cmpl	$1, %edx
	jbe	.L301
	movq	328(%rdi), %rdx
	movq	%rdi, %r12
	cmpq	$0, 16(%rdx)
	je	.L301
	movq	336(%rdi), %rdi
	leaq	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode(%rip), %rdx
	movl	%esi, %ebx
	movl	$0, -44(%rbp)
	movq	344(%r12), %rsi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	movq	$0, 344(%r12)
	cmpq	%rdx, %rax
	jne	.L303
	movq	336(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	48(%rdx), %rcx
	cmpq	%rax, %rcx
	jne	.L304
	movq	336(%rdi), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L303
	movq	336(%rdi), %rdi
	movq	(%rdi), %rdx
	movq	48(%rdx), %rcx
	cmpq	%rax, %rcx
	jne	.L304
	movq	336(%rdi), %rdi
	leaq	-44(%rbp), %rdx
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%rax, %r13
.L307:
	movq	344(%r12), %rdi
	testq	%rdi, %rdi
	je	.L308
	call	utext_close_67@PLT
.L308:
	movl	-44(%rbp), %eax
	movq	%r13, 344(%r12)
	testl	%eax, %eax
	jg	.L313
.L309:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi
	cmpl	$1, %eax
	je	.L320
	movl	%ebx, %eax
.L301:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L321
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movq	336(%r12), %rdi
	movq	(%rdi), %rax
	call	*96(%rax)
	movl	%eax, %ebx
	leal	1(%rax), %eax
	cmpl	$1, %eax
	ja	.L309
	movl	%ebx, %eax
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L303:
	leaq	-44(%rbp), %rdx
	call	*%rax
	movq	%rax, %r13
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	-44(%rbp), %rdx
	call	*%rcx
	movq	%rax, %r13
	jmp	.L307
.L313:
	movl	$-1, %eax
	jmp	.L301
.L321:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2620:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalPrevEi, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalPrevEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9precedingEi
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9precedingEi, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIterator9precedingEi:
.LFB2623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	336(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*128(%rax)
	movl	%eax, %r12d
	addl	$1, %eax
	cmpl	$1, %eax
	jbe	.L322
	movq	328(%rbx), %rax
	cmpq	$0, 16(%rax)
	je	.L322
	leaq	-60(%rbp), %rsi
	movq	%rbx, %rdi
	movl	$0, -60(%rbp)
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10resetStateER10UErrorCode
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jg	.L332
	leaq	_ZN6icu_6735SimpleFilteredSentenceBreakIterator8previousEv(%rip), %r13
.L324:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16breakExceptionAtEi
	cmpl	$1, %eax
	je	.L334
.L322:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L335
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	336(%rbx), %r12
	movq	(%r12), %rax
	movq	96(%rax), %rax
	cmpq	%r13, %rax
	jne	.L325
	movq	336(%r12), %r14
	movq	(%r14), %rax
	movq	96(%rax), %rax
	cmpq	%r13, %rax
	jne	.L326
	movq	336(%r14), %r15
	movq	(%r15), %rax
	movq	96(%rax), %rax
	cmpq	%r13, %rax
	jne	.L327
	movq	336(%r15), %rdi
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalPrevEi
	movl	%eax, %esi
.L328:
	movq	%r14, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalPrevEi
	movl	%eax, %esi
.L329:
	movq	%r12, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIterator12internalPrevEi
	movl	%eax, %r12d
.L330:
	leal	1(%r12), %eax
	cmpl	$1, %eax
	ja	.L324
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L325:
	movq	%r12, %rdi
	call	*%rax
	movl	%eax, %r12d
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L326:
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, %esi
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r15, %rdi
	call	*%rax
	movl	%eax, %esi
	jmp	.L328
.L332:
	movl	$-1, %r12d
	jmp	.L322
.L335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2623:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9precedingEi, .-_ZN6icu_6735SimpleFilteredSentenceBreakIterator9precedingEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ER10UErrorCode
	.type	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ER10UErrorCode, @function
_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ER10UErrorCode:
.LFB2634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE(%rip), %rax
	movq	%rsi, %r8
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -8(%rdi)
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	call	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_6710UStringSetE(%rip), %rax
	movq	%rax, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2634:
	.size	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ER10UErrorCode, .-_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ER10UErrorCode
	.globl	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC1ER10UErrorCode
	.set	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC1ER10UErrorCode,_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ER10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"icudt67l-brkitr"
.LC1:
	.string	"exceptions"
.LC2:
	.string	"SentenceBreak"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ERKNS_6LocaleER10UErrorCode:
.LFB2637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	8(%rdi), %rbx
	subq	$136, %rsp
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE(%rip), %rax
	movq	%rax, (%rdi)
	movq	%rbx, %rdi
	call	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movl	(%r14), %r11d
	leaq	16+_ZTVN6icu_6710UStringSetE(%rip), %rax
	movq	%rax, 8(%r12)
	testl	%r11d, %r11d
	jle	.L394
.L338:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L394:
	.cfi_restore_state
	movl	$0, -144(%rbp)
	movq	%r13, %rdi
	leaq	-144(%rbp), %r13
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%r13, %rdx
	leaq	.LC0(%rip), %rdi
	movq	%rax, %rsi
	call	ures_open_67@PLT
	movq	%rax, %r12
	movl	-144(%rbp), %eax
	cmpl	$-127, %eax
	je	.L340
	testl	%eax, %eax
	jg	.L340
	movq	%r13, %rcx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	ures_getByKeyWithFallback_67@PLT
	movq	%rax, -152(%rbp)
	movl	-144(%rbp), %eax
	cmpl	$-127, %eax
	je	.L341
	testl	%eax, %eax
	jg	.L341
	movq	-152(%rbp), %rdi
	movq	%r13, %rcx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	call	ures_getByKeyWithFallback_67@PLT
	movq	%rax, -160(%rbp)
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	jg	.L344
	cmpl	$-127, %eax
	je	.L344
	movl	(%r14), %eax
	xorl	%r15d, %r15d
	movl	%eax, -144(%rbp)
	.p2align 4,,10
	.p2align 3
.L347:
	movq	-160(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	call	ures_getNextResource_67@PLT
	movq	%rax, %r15
	movl	-144(%rbp), %eax
	testq	%r15, %r15
	je	.L348
	testl	%eax, %eax
	jle	.L396
.L349:
	cmpl	$8, %eax
	je	.L359
	movl	(%r14), %edx
	testl	%edx, %edx
	jg	.L359
	movl	%eax, (%r14)
.L359:
	movq	%r15, %rdi
	call	ures_close_67@PLT
.L360:
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L346
	movq	%rax, %rdi
.L393:
	call	ures_close_67@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L340:
	movl	%eax, (%r14)
.L343:
	testq	%r12, %r12
	je	.L338
	movq	%r12, %rdi
	call	ures_close_67@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L341:
	movl	%eax, (%r14)
.L346:
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L343
	movq	%rax, %rdi
	call	ures_close_67@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L396:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r9d
	movq	%r14, %rdx
	movq	%r15, %rdi
	leaq	-140(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movw	%r9w, -120(%rbp)
	movl	$0, -140(%rbp)
	call	ures_getString_67@PLT
	movl	(%r14), %r10d
	testl	%r10d, %r10d
	jle	.L397
	leaq	-128(%rbp), %r10
	movq	%r10, %rdi
	movq	%r10, -168(%rbp)
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	-168(%rbp), %r10
.L351:
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jle	.L398
.L353:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	jle	.L347
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L344:
	movq	-160(%rbp), %rdi
	movl	%eax, (%r14)
	testq	%rdi, %rdi
	jne	.L393
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L397:
	leaq	-128(%rbp), %r10
	movl	-140(%rbp), %ecx
	leaq	-136(%rbp), %rdx
	movl	$1, %esi
	movq	%r10, %rdi
	movq	%rax, -136(%rbp)
	movq	%r10, -168(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	-168(%rbp), %r10
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L398:
	movl	$64, %edi
	movq	%r10, -168(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	-168(%rbp), %r10
	testq	%rax, %rax
	je	.L354
	movq	%r10, %rsi
	movq	%rax, %rdi
	movq	%r10, -176(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	movl	(%r14), %edi
	movq	-168(%rbp), %r11
	movq	-176(%rbp), %r10
	testl	%edi, %edi
	jle	.L355
.L356:
	movq	(%r11), %rax
	movq	%r10, -168(%rbp)
	movq	%r11, %rdi
	call	*8(%rax)
	movq	-168(%rbp), %r10
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L348:
	testl	%eax, %eax
	jle	.L360
	cmpl	$8, %eax
	je	.L360
	movl	(%r14), %ecx
	testl	%ecx, %ecx
	jg	.L360
	movl	%eax, (%r14)
	jmp	.L360
.L355:
	xorl	%edx, %edx
	movq	%r11, %rsi
	movq	%rbx, %rdi
	movq	%r10, -176(%rbp)
	movq	%r11, -168(%rbp)
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	movq	-168(%rbp), %r11
	movq	-176(%rbp), %r10
	testl	%eax, %eax
	jns	.L356
	movq	%r11, %rsi
	movq	%r14, %rcx
	leaq	_ZN6icu_67L20compareUnicodeStringE8UElementS0_(%rip), %rdx
	movq	%rbx, %rdi
	movq	%r10, -168(%rbp)
	movq	%r11, -176(%rbp)
	call	_ZN6icu_677UVector12sortedInsertEPvPFa8UElementS2_ER10UErrorCode@PLT
	movl	(%r14), %esi
	movq	-168(%rbp), %r10
	testl	%esi, %esi
	jle	.L353
	movq	-176(%rbp), %r11
	movq	(%r11), %rax
	movq	%r11, %rdi
	call	*8(%rax)
	movq	-168(%rbp), %r10
	jmp	.L353
.L395:
	call	__stack_chk_fail@PLT
.L354:
	movl	$7, (%r14)
	jmp	.L353
	.cfi_endproc
.LFE2637:
	.size	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC1ERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC1ERKNS_6LocaleER10UErrorCode,_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC2ERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FilteredBreakIteratorBuilderC2Ev
	.type	_ZN6icu_6728FilteredBreakIteratorBuilderC2Ev, @function
_ZN6icu_6728FilteredBreakIteratorBuilderC2Ev:
.LFB2646:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6728FilteredBreakIteratorBuilderE(%rip), %rax
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE2646:
	.size	_ZN6icu_6728FilteredBreakIteratorBuilderC2Ev, .-_ZN6icu_6728FilteredBreakIteratorBuilderC2Ev
	.globl	_ZN6icu_6728FilteredBreakIteratorBuilderC1Ev
	.set	_ZN6icu_6728FilteredBreakIteratorBuilderC1Ev,_ZN6icu_6728FilteredBreakIteratorBuilderC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FilteredBreakIteratorBuilderD2Ev
	.type	_ZN6icu_6728FilteredBreakIteratorBuilderD2Ev, @function
_ZN6icu_6728FilteredBreakIteratorBuilderD2Ev:
.LFB2649:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6728FilteredBreakIteratorBuilderE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2649:
	.size	_ZN6icu_6728FilteredBreakIteratorBuilderD2Ev, .-_ZN6icu_6728FilteredBreakIteratorBuilderD2Ev
	.globl	_ZN6icu_6728FilteredBreakIteratorBuilderD1Ev
	.set	_ZN6icu_6728FilteredBreakIteratorBuilderD1Ev,_ZN6icu_6728FilteredBreakIteratorBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FilteredBreakIteratorBuilderD0Ev
	.type	_ZN6icu_6728FilteredBreakIteratorBuilderD0Ev, @function
_ZN6icu_6728FilteredBreakIteratorBuilderD0Ev:
.LFB2651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6728FilteredBreakIteratorBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2651:
	.size	_ZN6icu_6728FilteredBreakIteratorBuilderD0Ev, .-_ZN6icu_6728FilteredBreakIteratorBuilderD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceERKNS_6LocaleER10UErrorCode:
.LFB2652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L404
	movq	%rdi, %r13
	movl	$48, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L405
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderC1ERKNS_6LocaleER10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jle	.L403
	movq	(%r12), %rdx
	leaq	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD0Ev(%rip), %rax
	cmpq	%rax, 8(%rdx)
	jne	.L414
	leaq	16+_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE(%rip), %rcx
	leaq	16+_ZTVN6icu_6710UStringSetE(%rip), %rax
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	8(%r12), %rdi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	call	_ZN6icu_677UVectorD2Ev@PLT
	leaq	16+_ZTVN6icu_6728FilteredBreakIteratorBuilderE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L403:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L405:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L404
	movl	$7, (%rbx)
.L404:
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD0Ev
	jmp	.L403
	.cfi_endproc
.LFE2652:
	.size	_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceER10UErrorCode
	.type	_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceER10UErrorCode, @function
_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceER10UErrorCode:
.LFB2653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L416
	movq	%rdi, %rbx
	movl	$48, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L417
	leaq	16+_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE(%rip), %r14
	leaq	8(%rax), %r13
	movq	%rbx, %r8
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	%r14, (%rax)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movl	(%rbx), %edx
	leaq	16+_ZTVN6icu_6710UStringSetE(%rip), %rax
	movq	%rax, 8(%r12)
	testl	%edx, %edx
	jle	.L415
	movq	(%r12), %rax
	leaq	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L426
	movq	%r14, (%r12)
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorD2Ev@PLT
	leaq	16+_ZTVN6icu_6728FilteredBreakIteratorBuilderE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L415:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L417:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L416
	movl	$7, (%rbx)
.L416:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L415
	.cfi_endproc
.LFE2653:
	.size	_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceER10UErrorCode, .-_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728FilteredBreakIteratorBuilder19createEmptyInstanceER10UErrorCode
	.type	_ZN6icu_6728FilteredBreakIteratorBuilder19createEmptyInstanceER10UErrorCode, @function
_ZN6icu_6728FilteredBreakIteratorBuilder19createEmptyInstanceER10UErrorCode:
.LFB2654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L428
	movq	%rdi, %rbx
	movl	$48, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L429
	leaq	16+_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE(%rip), %r14
	leaq	8(%rax), %r13
	movq	%rbx, %r8
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rdx
	movq	%r14, (%rax)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	movl	(%rbx), %edx
	leaq	16+_ZTVN6icu_6710UStringSetE(%rip), %rax
	movq	%rax, 8(%r12)
	testl	%edx, %edx
	jle	.L427
	movq	(%r12), %rax
	leaq	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L438
	movq	%r14, (%r12)
	movq	%r13, %rdi
	call	_ZN6icu_677UVectorD2Ev@PLT
	leaq	16+_ZTVN6icu_6728FilteredBreakIteratorBuilderE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	_ZN6icu_677UMemorydlEPv@PLT
.L427:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L429:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L428
	movl	$7, (%rbx)
.L428:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*%rax
	jmp	.L427
	.cfi_endproc
.LFE2654:
	.size	_ZN6icu_6728FilteredBreakIteratorBuilder19createEmptyInstanceER10UErrorCode, .-_ZN6icu_6728FilteredBreakIteratorBuilder19createEmptyInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2EPNS_13BreakIteratorEPNS_10UCharsTrieES4_R10UErrorCode
	.type	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2EPNS_13BreakIteratorEPNS_10UCharsTrieES4_R10UErrorCode, @function
_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2EPNS_13BreakIteratorEPNS_10UCharsTrieES4_R10UErrorCode:
.LFB2611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-512(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$488, %rsp
	movq	%rdx, -520(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -528(%rbp)
	movq	%r8, %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6713BreakIterator9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713BreakIterator9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6icu_6713BreakIteratorC2ERKNS_6LocaleES3_@PLT
	movq	%r14, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE(%rip), %rax
	movl	$32, %edi
	movq	%rax, (%rbx)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L440
	movq	-520(%rbp), %rdx
	movq	-528(%rbp), %rcx
	leaq	16+_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE(%rip), %rsi
	movl	$1, 24(%rax)
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	%rcx, 16(%rax)
.L440:
	movq	%rax, %xmm0
	movq	%r12, %xmm1
	movq	$0, 344(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 328(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L446
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L446:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2611:
	.size	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2EPNS_13BreakIteratorEPNS_10UCharsTrieES4_R10UErrorCode, .-_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2EPNS_13BreakIteratorEPNS_10UCharsTrieES4_R10UErrorCode
	.globl	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC1EPNS_13BreakIteratorEPNS_10UCharsTrieES4_R10UErrorCode
	.set	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC1EPNS_13BreakIteratorEPNS_10UCharsTrieES4_R10UErrorCode,_ZN6icu_6735SimpleFilteredSentenceBreakIteratorC2EPNS_13BreakIteratorEPNS_10UCharsTrieES4_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder5buildEPNS_13BreakIteratorER10UErrorCode
	.type	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder5buildEPNS_13BreakIteratorER10UErrorCode, @function
_ZN6icu_6734SimpleFilteredBreakIteratorBuilder5buildEPNS_13BreakIteratorER10UErrorCode:
.LFB2642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$112, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$568, %rsp
	movq	%rsi, -568(%rbp)
	movq	%rdx, -536(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -584(%rbp)
	testq	%rax, %rax
	je	.L448
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6717UCharsTrieBuilderC1ER10UErrorCode@PLT
.L449:
	movl	$112, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -576(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L450
	movq	-536(%rbp), %rbx
	xorl	%r14d, %r14d
	movq	%rbx, %rsi
	call	_ZN6icu_6717UCharsTrieBuilderC1ER10UErrorCode@PLT
	movl	(%rbx), %r15d
	testl	%r15d, %r15d
	jg	.L451
	movslq	16(%r12), %r13
	movl	%r13d, -524(%rbp)
	testq	%r13, %r13
	jne	.L549
	movl	$72, %edi
	movl	$1, %ebx
.L454:
	call	_ZN6icu_677UMemorynaEm@PLT
	testq	%rax, %rax
	je	.L458
	movq	%rbx, (%rax)
	addq	$8, %rax
	leaq	-1(%rbx), %rcx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movq	%rax, -544(%rbp)
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$2, %r14d
	subq	$1, %rcx
	movq	%rsi, (%rax)
	addq	$64, %rax
	movw	%r14w, -56(%rax)
	cmpq	$-1, %rcx
	jne	.L459
.L457:
	movl	-524(%rbp), %ebx
	testl	%ebx, %ebx
	jle	.L460
.L554:
	salq	$2, %r13
	movq	%r13, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -520(%rbp)
	testq	%rax, %rax
	je	.L461
	movq	%rax, %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	xorl	%edi, %edi
	call	uprv_free_67@PLT
.L461:
	movl	16(%r12), %eax
	testl	%eax, %eax
	jle	.L463
.L505:
	movq	-544(%rbp), %rbx
	movq	-520(%rbp), %r14
	leaq	8(%r12), %r13
	xorl	%r15d, %r15d
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L551:
	movq	%rbx, %rdi
	addq	$64, %rbx
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movl	$0, (%r14,%r15,4)
	addq	$1, %r15
	cmpl	%r15d, 16(%r12)
	jle	.L550
.L465:
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L551
	movq	%rax, %r14
	movq	-536(%rbp), %rax
	movl	$7, (%rax)
.L467:
	movq	-520(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-544(%rbp), %rax
	testq	%rax, %rax
	je	.L451
	movq	-8(%rax), %rdx
	movq	%rax, %r12
	movq	%rdx, %rbx
	movq	%rdx, -520(%rbp)
	salq	$6, %rbx
	addq	%rax, %rbx
	cmpq	%rbx, %rax
	je	.L497
	.p2align 4,,10
	.p2align 3
.L496:
	movq	-64(%rbx), %rax
	subq	$64, %rbx
	movq	%rbx, %rdi
	call	*(%rax)
	cmpq	%rbx, %r12
	jne	.L496
.L497:
	movq	-544(%rbp), %rdi
	subq	$8, %rdi
	call	_ZN6icu_677UMemorydaEPv@PLT
.L451:
	movq	-576(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
.L453:
	movq	-584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L498
	movq	(%rdi), %rax
	call	*8(%rax)
.L498:
	movq	-568(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L447
	movq	(%rcx), %rax
	leaq	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD0Ev(%rip), %rdx
	movq	%rcx, %rbx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L500
	movq	%rcx, %rdi
	call	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD1Ev
	movq	%rbx, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L447:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L552
	addq	$568, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	.cfi_restore_state
	movabsq	$144115188075855871, %rax
	cmpq	%rax, %r13
	jbe	.L553
	movq	$-1, %rdi
	call	_ZN6icu_677UMemorynaEm@PLT
	testq	%rax, %rax
	je	.L458
	movl	-524(%rbp), %ebx
	movq	%r13, (%rax)
	addq	$8, %rax
	movq	%rax, -544(%rbp)
	testl	%ebx, %ebx
	jg	.L554
.L460:
	movq	$0, -520(%rbp)
	movl	16(%r12), %edx
	testl	%edx, %edx
	jg	.L505
.L462:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L468
.L450:
	movq	-536(%rbp), %rax
	xorl	%r14d, %r14d
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L453
	movl	$7, (%rax)
	xorl	%r14d, %r14d
	jmp	.L453
.L448:
	movq	-536(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L449
	movl	$7, (%rax)
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L550:
	movl	-524(%rbp), %r11d
	testl	%r11d, %r11d
	jle	.L462
.L463:
	movq	-544(%rbp), %r12
	xorl	%r15d, %r15d
	leaq	-288(%rbp), %rax
	xorl	%r14d, %r14d
	movl	%r15d, -596(%rbp)
	movl	-524(%rbp), %r15d
	movq	%rax, -592(%rbp)
	movq	%r12, -608(%rbp)
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L557:
	sarl	$5, %ecx
.L470:
	xorl	%edx, %edx
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	testl	%eax, %eax
	js	.L471
	leal	1(%rax), %r10d
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L472
	sarl	$5, %eax
.L473:
	cmpl	%eax, %r10d
	jne	.L555
.L471:
	addq	$1, %r14
	addq	$64, %r12
	cmpl	%r14d, %r15d
	jle	.L556
.L485:
	movswl	8(%r12), %ecx
	testw	%cx, %cx
	jns	.L557
	movl	12(%r12), %ecx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L555:
	movq	-544(%rbp), %rax
	movq	%r14, %r11
	xorl	%ebx, %ebx
	movq	%r12, %rdi
	movl	$-1, -528(%rbp)
	movl	%r10d, %r14d
	leaq	8(%rax), %r13
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L559:
	movzbl	8(%rdi), %eax
	notl	%eax
	andl	$1, %eax
.L476:
	testb	%al, %al
	jne	.L474
	movq	-520(%rbp), %rdx
	movl	(%rdx,%rbx,4), %eax
	testl	%eax, %eax
	jne	.L480
	movl	$3, (%rdx,%rbx,4)
	.p2align 4,,10
	.p2align 3
.L474:
	addq	$1, %rbx
	addq	$64, %r13
	cmpl	%ebx, %r15d
	jle	.L558
.L481:
	movl	%ebx, %r12d
	cmpl	%ebx, %r11d
	je	.L474
	movzwl	0(%r13), %eax
	testb	$1, %al
	jne	.L559
	testw	%ax, %ax
	js	.L477
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L478:
	testl	%r9d, %r9d
	movl	$0, %r8d
	leaq	2(%r13), %rcx
	cmovle	%r9d, %r8d
	subl	%r8d, %r9d
	cmpl	%r14d, %r9d
	cmovg	%r14d, %r9d
	testb	$2, %al
	jne	.L479
	movq	16(%r13), %rcx
.L479:
	movl	%r14d, %edx
	xorl	%esi, %esi
	movq	%r11, -560(%rbp)
	movq	%rdi, -552(%rbp)
	call	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii@PLT
	movq	-560(%rbp), %r11
	movq	-552(%rbp), %rdi
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L480:
	testb	$1, %al
	cmove	-528(%rbp), %r12d
	addq	$1, %rbx
	addq	$64, %r13
	movl	%r12d, -528(%rbp)
	cmpl	%ebx, %r15d
	jg	.L481
	.p2align 4,,10
	.p2align 3
.L558:
	movl	%r14d, %r10d
	movq	%rdi, %rsi
	movq	%rdi, %r12
	movq	-592(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r10d, %ecx
	movq	%r11, %r14
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	cmpl	$-1, -528(%rbp)
	je	.L560
.L482:
	movq	-592(%rbp), %rdi
	addq	$1, %r14
	addq	$64, %r12
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	cmpl	%r14d, %r15d
	jg	.L485
.L556:
	movq	-608(%rbp), %r13
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	movl	-596(%rbp), %r15d
	movl	%ebx, -552(%rbp)
	movq	%r12, %rbx
	movq	-536(%rbp), %r12
	movq	%r13, %r14
	movq	-584(%rbp), %r13
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L562:
	movswl	8(%r14), %edx
	testw	%dx, %dx
	js	.L487
	sarl	$5, %edx
.L488:
	movq	%r14, %rdi
	xorl	%esi, %esi
	addl	$1, %r15d
	addq	$1, %rbx
	call	_ZN6icu_6713UnicodeString9doReverseEii@PLT
	movq	%r14, %rsi
	movq	%r12, %rcx
	movl	$2, %edx
	movq	%r13, %rdi
	addq	$64, %r14
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	cmpl	%ebx, -524(%rbp)
	jle	.L561
.L490:
	movq	-520(%rbp), %rax
	movl	(%rax,%rbx,4), %r9d
	testl	%r9d, %r9d
	je	.L562
	movq	%r14, %rsi
	movq	%r12, %rcx
	movl	$2, %edx
	addq	$1, %rbx
	movq	-576(%rbp), %rdi
	addq	$64, %r14
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	addl	$1, -552(%rbp)
	cmpl	%ebx, -524(%rbp)
	jg	.L490
.L561:
	movl	-552(%rbp), %ebx
	xorl	%r12d, %r12d
	testl	%r15d, %r15d
	jne	.L563
.L491:
	testl	%ebx, %ebx
	je	.L510
	movq	-536(%rbp), %rbx
	movq	-576(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	_ZN6icu_6717UCharsTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode@PLT
	movl	(%rbx), %edi
	movq	%rax, %r13
	testl	%edi, %edi
	jg	.L564
.L468:
	movl	$352, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L511
	leaq	-512(%rbp), %r15
	movq	-536(%rbp), %rcx
	movq	-568(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	leaq	-288(%rbp), %rbx
	call	_ZNK6icu_6713BreakIterator9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	-536(%rbp), %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	-568(%rbp), %rsi
	call	_ZNK6icu_6713BreakIterator9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713BreakIteratorC2ERKNS_6LocaleES3_@PLT
	movq	%rbx, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	leaq	16+_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE(%rip), %rax
	movl	$32, %edi
	movq	%rax, (%r14)
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L494
	leaq	16+_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE(%rip), %rdx
	movq	%r13, 8(%rax)
	movq	%rdx, (%rax)
	movq	%r12, 16(%rax)
	movl	$1, 24(%rax)
.L494:
	movq	$0, 344(%r14)
	movq	%rax, %xmm0
	movhps	-568(%rbp), %xmm0
	movq	$0, -568(%rbp)
	movups	%xmm0, 328(%r14)
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L477:
	movl	4(%r13), %r9d
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L472:
	movl	12(%r12), %eax
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L487:
	movl	12(%r14), %edx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L560:
	movq	-520(%rbp), %rax
	movl	(%rax,%r14,4), %r10d
	testl	%r10d, %r10d
	jne	.L482
	movzwl	-280(%rbp), %eax
	testw	%ax, %ax
	js	.L483
	movswl	%ax, %edx
	sarl	$5, %edx
.L484:
	movq	-592(%rbp), %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeString9doReverseEii@PLT
	movq	-536(%rbp), %rcx
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	-584(%rbp), %rdi
	call	_ZN6icu_6717UCharsTrieBuilder3addERKNS_13UnicodeStringEiR10UErrorCode@PLT
	movq	-520(%rbp), %rax
	addl	$1, -596(%rbp)
	movl	$3, (%rax,%r14,4)
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L553:
	movq	%r13, %rdi
	movq	%r13, %rbx
	salq	$6, %rdi
	addq	$8, %rdi
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L563:
	movq	-536(%rbp), %r14
	movq	-584(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r14, %rdx
	call	_ZN6icu_6717UCharsTrieBuilder5buildE22UStringTrieBuildOptionR10UErrorCode@PLT
	movl	(%r14), %r8d
	movq	%rax, %r12
	testl	%r8d, %r8d
	jle	.L491
	.p2align 4,,10
	.p2align 3
.L492:
	testq	%r12, %r12
	je	.L512
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L510:
	xorl	%r13d, %r13d
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L564:
	testq	%rax, %rax
	je	.L492
	movq	%rax, %rdi
	call	_ZN6icu_6710UCharsTrieD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L500:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L447
.L483:
	movl	-276(%rbp), %edx
	jmp	.L484
.L552:
	call	__stack_chk_fail@PLT
.L458:
	movq	$0, -544(%rbp)
	jmp	.L457
.L512:
	xorl	%r14d, %r14d
	jmp	.L467
.L511:
	movq	$0, -568(%rbp)
	jmp	.L467
	.cfi_endproc
.LFE2642:
	.size	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder5buildEPNS_13BreakIteratorER10UErrorCode, .-_ZN6icu_6734SimpleFilteredBreakIteratorBuilder5buildEPNS_13BreakIteratorER10UErrorCode
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6728FilteredBreakIteratorBuilderE
	.section	.rodata._ZTSN6icu_6728FilteredBreakIteratorBuilderE,"aG",@progbits,_ZTSN6icu_6728FilteredBreakIteratorBuilderE,comdat
	.align 32
	.type	_ZTSN6icu_6728FilteredBreakIteratorBuilderE, @object
	.size	_ZTSN6icu_6728FilteredBreakIteratorBuilderE, 40
_ZTSN6icu_6728FilteredBreakIteratorBuilderE:
	.string	"N6icu_6728FilteredBreakIteratorBuilderE"
	.weak	_ZTIN6icu_6728FilteredBreakIteratorBuilderE
	.section	.data.rel.ro._ZTIN6icu_6728FilteredBreakIteratorBuilderE,"awG",@progbits,_ZTIN6icu_6728FilteredBreakIteratorBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_6728FilteredBreakIteratorBuilderE, @object
	.size	_ZTIN6icu_6728FilteredBreakIteratorBuilderE, 24
_ZTIN6icu_6728FilteredBreakIteratorBuilderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6728FilteredBreakIteratorBuilderE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6710UStringSetE
	.section	.rodata._ZTSN6icu_6710UStringSetE,"aG",@progbits,_ZTSN6icu_6710UStringSetE,comdat
	.align 16
	.type	_ZTSN6icu_6710UStringSetE, @object
	.size	_ZTSN6icu_6710UStringSetE, 22
_ZTSN6icu_6710UStringSetE:
	.string	"N6icu_6710UStringSetE"
	.weak	_ZTIN6icu_6710UStringSetE
	.section	.data.rel.ro._ZTIN6icu_6710UStringSetE,"awG",@progbits,_ZTIN6icu_6710UStringSetE,comdat
	.align 8
	.type	_ZTIN6icu_6710UStringSetE, @object
	.size	_ZTIN6icu_6710UStringSetE, 24
_ZTIN6icu_6710UStringSetE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6710UStringSetE
	.quad	_ZTIN6icu_677UVectorE
	.weak	_ZTSN6icu_6731SimpleFilteredSentenceBreakDataE
	.section	.rodata._ZTSN6icu_6731SimpleFilteredSentenceBreakDataE,"aG",@progbits,_ZTSN6icu_6731SimpleFilteredSentenceBreakDataE,comdat
	.align 32
	.type	_ZTSN6icu_6731SimpleFilteredSentenceBreakDataE, @object
	.size	_ZTSN6icu_6731SimpleFilteredSentenceBreakDataE, 43
_ZTSN6icu_6731SimpleFilteredSentenceBreakDataE:
	.string	"N6icu_6731SimpleFilteredSentenceBreakDataE"
	.weak	_ZTIN6icu_6731SimpleFilteredSentenceBreakDataE
	.section	.data.rel.ro._ZTIN6icu_6731SimpleFilteredSentenceBreakDataE,"awG",@progbits,_ZTIN6icu_6731SimpleFilteredSentenceBreakDataE,comdat
	.align 8
	.type	_ZTIN6icu_6731SimpleFilteredSentenceBreakDataE, @object
	.size	_ZTIN6icu_6731SimpleFilteredSentenceBreakDataE, 24
_ZTIN6icu_6731SimpleFilteredSentenceBreakDataE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6731SimpleFilteredSentenceBreakDataE
	.quad	_ZTIN6icu_677UMemoryE
	.weak	_ZTSN6icu_6735SimpleFilteredSentenceBreakIteratorE
	.section	.rodata._ZTSN6icu_6735SimpleFilteredSentenceBreakIteratorE,"aG",@progbits,_ZTSN6icu_6735SimpleFilteredSentenceBreakIteratorE,comdat
	.align 32
	.type	_ZTSN6icu_6735SimpleFilteredSentenceBreakIteratorE, @object
	.size	_ZTSN6icu_6735SimpleFilteredSentenceBreakIteratorE, 47
_ZTSN6icu_6735SimpleFilteredSentenceBreakIteratorE:
	.string	"N6icu_6735SimpleFilteredSentenceBreakIteratorE"
	.weak	_ZTIN6icu_6735SimpleFilteredSentenceBreakIteratorE
	.section	.data.rel.ro._ZTIN6icu_6735SimpleFilteredSentenceBreakIteratorE,"awG",@progbits,_ZTIN6icu_6735SimpleFilteredSentenceBreakIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6735SimpleFilteredSentenceBreakIteratorE, @object
	.size	_ZTIN6icu_6735SimpleFilteredSentenceBreakIteratorE, 24
_ZTIN6icu_6735SimpleFilteredSentenceBreakIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6735SimpleFilteredSentenceBreakIteratorE
	.quad	_ZTIN6icu_6713BreakIteratorE
	.weak	_ZTSN6icu_6734SimpleFilteredBreakIteratorBuilderE
	.section	.rodata._ZTSN6icu_6734SimpleFilteredBreakIteratorBuilderE,"aG",@progbits,_ZTSN6icu_6734SimpleFilteredBreakIteratorBuilderE,comdat
	.align 32
	.type	_ZTSN6icu_6734SimpleFilteredBreakIteratorBuilderE, @object
	.size	_ZTSN6icu_6734SimpleFilteredBreakIteratorBuilderE, 46
_ZTSN6icu_6734SimpleFilteredBreakIteratorBuilderE:
	.string	"N6icu_6734SimpleFilteredBreakIteratorBuilderE"
	.weak	_ZTIN6icu_6734SimpleFilteredBreakIteratorBuilderE
	.section	.data.rel.ro._ZTIN6icu_6734SimpleFilteredBreakIteratorBuilderE,"awG",@progbits,_ZTIN6icu_6734SimpleFilteredBreakIteratorBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_6734SimpleFilteredBreakIteratorBuilderE, @object
	.size	_ZTIN6icu_6734SimpleFilteredBreakIteratorBuilderE, 24
_ZTIN6icu_6734SimpleFilteredBreakIteratorBuilderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6734SimpleFilteredBreakIteratorBuilderE
	.quad	_ZTIN6icu_6728FilteredBreakIteratorBuilderE
	.weak	_ZTVN6icu_6710UStringSetE
	.section	.data.rel.ro._ZTVN6icu_6710UStringSetE,"awG",@progbits,_ZTVN6icu_6710UStringSetE,comdat
	.align 8
	.type	_ZTVN6icu_6710UStringSetE, @object
	.size	_ZTVN6icu_6710UStringSetE, 40
_ZTVN6icu_6710UStringSetE:
	.quad	0
	.quad	_ZTIN6icu_6710UStringSetE
	.quad	_ZN6icu_6710UStringSetD1Ev
	.quad	_ZN6icu_6710UStringSetD0Ev
	.quad	_ZNK6icu_677UVector17getDynamicClassIDEv
	.weak	_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE
	.section	.data.rel.ro.local._ZTVN6icu_6731SimpleFilteredSentenceBreakDataE,"awG",@progbits,_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE,comdat
	.align 8
	.type	_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE, @object
	.size	_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE, 32
_ZTVN6icu_6731SimpleFilteredSentenceBreakDataE:
	.quad	0
	.quad	_ZTIN6icu_6731SimpleFilteredSentenceBreakDataE
	.quad	_ZN6icu_6731SimpleFilteredSentenceBreakDataD1Ev
	.quad	_ZN6icu_6731SimpleFilteredSentenceBreakDataD0Ev
	.weak	_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE
	.section	.data.rel.ro._ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE,"awG",@progbits,_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE, @object
	.size	_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE, 200
_ZTVN6icu_6735SimpleFilteredSentenceBreakIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6735SimpleFilteredSentenceBreakIteratorE
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD1Ev
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIteratorD0Ev
	.quad	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator17getDynamicClassIDEv
	.quad	_ZNK6icu_6735SimpleFilteredSentenceBreakIteratoreqERKNS_13BreakIteratorE
	.quad	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator5cloneEv
	.quad	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7getTextEv
	.quad	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator8getUTextEP5UTextR10UErrorCode
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextERKNS_13UnicodeStringE
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator7setTextEP5UTextR10UErrorCode
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9adoptTextEPNS_17CharacterIteratorE
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator5firstEv
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4lastEv
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator8previousEv
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEv
	.quad	_ZNK6icu_6735SimpleFilteredSentenceBreakIterator7currentEv
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9followingEi
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator9precedingEi
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator10isBoundaryEi
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator4nextEi
	.quad	_ZNK6icu_6713BreakIterator13getRuleStatusEv
	.quad	_ZN6icu_6713BreakIterator16getRuleStatusVecEPiiR10UErrorCode
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator17createBufferCloneEPvRiR10UErrorCode
	.quad	_ZN6icu_6735SimpleFilteredSentenceBreakIterator16refreshInputTextEP5UTextR10UErrorCode
	.weak	_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE
	.section	.data.rel.ro._ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE,"awG",@progbits,_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE, @object
	.size	_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE, 64
_ZTVN6icu_6734SimpleFilteredBreakIteratorBuilderE:
	.quad	0
	.quad	_ZTIN6icu_6734SimpleFilteredBreakIteratorBuilderE
	.quad	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD1Ev
	.quad	_ZN6icu_6734SimpleFilteredBreakIteratorBuilderD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder18suppressBreakAfterERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder20unsuppressBreakAfterERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZN6icu_6734SimpleFilteredBreakIteratorBuilder5buildEPNS_13BreakIteratorER10UErrorCode
	.weak	_ZTVN6icu_6728FilteredBreakIteratorBuilderE
	.section	.data.rel.ro._ZTVN6icu_6728FilteredBreakIteratorBuilderE,"awG",@progbits,_ZTVN6icu_6728FilteredBreakIteratorBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_6728FilteredBreakIteratorBuilderE, @object
	.size	_ZTVN6icu_6728FilteredBreakIteratorBuilderE, 64
_ZTVN6icu_6728FilteredBreakIteratorBuilderE:
	.quad	0
	.quad	_ZTIN6icu_6728FilteredBreakIteratorBuilderE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
