	.file	"rbbidata.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode.part.0, @function
_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode.part.0:
.LFB4206:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	28(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L2
	movl	24(%rsi), %eax
	addq	%rsi, %rax
	movq	%rax, 16(%rdi)
.L2:
	movl	32(%rbx), %esi
	movl	36(%rbx), %edx
	movq	%r13, %r8
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	addq	%rbx, %rsi
	call	utrie2_openFromSerialized_67@PLT
	movq	%rax, 48(%r12)
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L1
	movq	(%r12), %rax
	leaq	-48(%rbp), %rdx
	leaq	72(%r12), %rdi
	movl	$-1, %ecx
	movl	$1, %esi
	movl	40(%rax), %eax
	addq	%rbx, %rax
	movq	%rax, 24(%r12)
	movq	%rax, -48(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	(%r12), %rax
	movl	48(%rax), %eax
	addq	%rbx, %rax
	movq	%rax, 32(%r12)
	movl	52(%rbx), %eax
	shrl	$2, %eax
	movl	%eax, 40(%r12)
	movl	$1, 56(%r12)
	mfence
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L7
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4206:
	.size	_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode.part.0, .-_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderER10UErrorCode
	.type	_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderER10UErrorCode, @function
_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderER10UErrorCode:
.LFB3144:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movq	$0, 32(%rdi)
	pxor	%xmm0, %xmm0
	movq	%rax, 72(%rdi)
	movw	%cx, 80(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 64(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$0, 56(%rdi)
	mfence
	movb	$1, 136(%rdi)
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jg	.L8
	cmpl	$45472, (%rsi)
	movq	%rsi, (%rdi)
	je	.L13
.L10:
	movl	$3, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	cmpb	$5, 4(%rsi)
	jne	.L10
	movl	20(%rsi), %eax
	movb	$0, 136(%rdi)
	testl	%eax, %eax
	je	.L12
	movl	16(%rsi), %eax
	addq	%rsi, %rax
	movq	%rax, 8(%rdi)
.L12:
	jmp	_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode.part.0
	.cfi_endproc
.LFE3144:
	.size	_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderER10UErrorCode, .-_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderER10UErrorCode
	.globl	_ZN6icu_6715RBBIDataWrapperC1EPKNS_14RBBIDataHeaderER10UErrorCode
	.set	_ZN6icu_6715RBBIDataWrapperC1EPKNS_14RBBIDataHeaderER10UErrorCode,_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderENS0_10EDontAdoptER10UErrorCode
	.type	_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderENS0_10EDontAdoptER10UErrorCode, @function
_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderENS0_10EDontAdoptER10UErrorCode:
.LFB3147:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	$0, 32(%rdi)
	pxor	%xmm0, %xmm0
	movq	%rax, 72(%rdi)
	movw	%dx, 80(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 64(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$0, 56(%rdi)
	mfence
	movb	$1, 136(%rdi)
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L20
	cmpl	$45472, (%rsi)
	movq	%rsi, (%rdi)
	je	.L22
.L16:
	movl	$3, (%rcx)
.L20:
	movb	$1, 136(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	cmpb	$5, 4(%rsi)
	jne	.L16
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	20(%rsi), %eax
	movb	$0, 136(%rdi)
	testl	%eax, %eax
	je	.L18
	movl	16(%rsi), %eax
	addq	%rsi, %rax
	movq	%rax, 8(%rdi)
.L18:
	movq	%rcx, %rdx
	movq	%rdi, -8(%rbp)
	call	_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode.part.0
	movq	-8(%rbp), %rdi
	movb	$1, 136(%rdi)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3147:
	.size	_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderENS0_10EDontAdoptER10UErrorCode, .-_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderENS0_10EDontAdoptER10UErrorCode
	.globl	_ZN6icu_6715RBBIDataWrapperC1EPKNS_14RBBIDataHeaderENS0_10EDontAdoptER10UErrorCode
	.set	_ZN6icu_6715RBBIDataWrapperC1EPKNS_14RBBIDataHeaderENS0_10EDontAdoptER10UErrorCode,_ZN6icu_6715RBBIDataWrapperC2EPKNS_14RBBIDataHeaderENS0_10EDontAdoptER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapper23isDataVersionAcceptableEPKh
	.type	_ZN6icu_6715RBBIDataWrapper23isDataVersionAcceptableEPKh, @function
_ZN6icu_6715RBBIDataWrapper23isDataVersionAcceptableEPKh:
.LFB3152:
	.cfi_startproc
	endbr64
	cmpb	$5, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE3152:
	.size	_ZN6icu_6715RBBIDataWrapper23isDataVersionAcceptableEPKh, .-_ZN6icu_6715RBBIDataWrapper23isDataVersionAcceptableEPKh
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapper5init0Ev
	.type	_ZN6icu_6715RBBIDataWrapper5init0Ev, @function
_ZN6icu_6715RBBIDataWrapper5init0Ev:
.LFB3153:
	.cfi_startproc
	endbr64
	movq	$0, 32(%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rdi)
	movq	$0, 64(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$0, 56(%rdi)
	mfence
	movb	$1, 136(%rdi)
	ret
	.cfi_endproc
.LFE3153:
	.size	_ZN6icu_6715RBBIDataWrapper5init0Ev, .-_ZN6icu_6715RBBIDataWrapper5init0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode
	.type	_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode, @function
_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode:
.LFB3154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L25
	cmpl	$45472, (%rsi)
	movq	%rsi, (%rdi)
	movq	%rdi, %r13
	movq	%rsi, %rbx
	movq	%rdx, %r12
	je	.L35
.L28:
	movl	$3, (%r12)
.L25:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	cmpb	$5, 4(%rsi)
	jne	.L28
	movb	$0, 136(%rdi)
	movl	20(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L30
	movl	16(%rsi), %eax
	addq	%rsi, %rax
	movq	%rax, 8(%rdi)
.L30:
	movl	28(%rbx), %edx
	testl	%edx, %edx
	je	.L31
	movl	24(%rbx), %eax
	addq	%rbx, %rax
	movq	%rax, 16(%r13)
.L31:
	movl	32(%rbx), %esi
	movl	36(%rbx), %edx
	movq	%r12, %r8
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	addq	%rbx, %rsi
	call	utrie2_openFromSerialized_67@PLT
	movq	%rax, 48(%r13)
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L25
	movq	0(%r13), %rax
	leaq	-48(%rbp), %rdx
	leaq	72(%r13), %rdi
	movl	$-1, %ecx
	movl	$1, %esi
	movl	40(%rax), %eax
	addq	%rbx, %rax
	movq	%rax, 24(%r13)
	movq	%rax, -48(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	0(%r13), %rax
	movl	48(%rax), %eax
	addq	%rbx, %rax
	movq	%rax, 32(%r13)
	movl	52(%rbx), %eax
	shrl	$2, %eax
	movl	%eax, 40(%r13)
	movl	$1, 56(%r13)
	mfence
	jmp	.L25
.L36:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3154:
	.size	_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode, .-_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapperC2EP11UDataMemoryR10UErrorCode
	.type	_ZN6icu_6715RBBIDataWrapperC2EP11UDataMemoryR10UErrorCode, @function
_ZN6icu_6715RBBIDataWrapperC2EP11UDataMemoryR10UErrorCode:
.LFB3150:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	$0, 32(%rdi)
	pxor	%xmm0, %xmm0
	movq	%rax, 72(%rdi)
	movl	$2, %eax
	movw	%ax, 80(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 64(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$0, 56(%rdi)
	mfence
	movb	$1, 136(%rdi)
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L42
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %rsi
	movzwl	(%rsi), %eax
	cmpw	$19, %ax
	jbe	.L39
	cmpw	$0, 8(%rsi)
	jne	.L39
	cmpw	$29250, 12(%rsi)
	je	.L45
.L39:
	movl	$3, (%rdx)
.L37:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpw	$8299, 14(%rsi)
	jne	.L39
	cmpb	$5, 16(%rsi)
	jne	.L39
	addq	%rax, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN6icu_6715RBBIDataWrapper4initEPKNS_14RBBIDataHeaderER10UErrorCode
	movq	-24(%rbp), %rdi
	movq	%rbx, 64(%rdi)
	jmp	.L37
	.cfi_endproc
.LFE3150:
	.size	_ZN6icu_6715RBBIDataWrapperC2EP11UDataMemoryR10UErrorCode, .-_ZN6icu_6715RBBIDataWrapperC2EP11UDataMemoryR10UErrorCode
	.globl	_ZN6icu_6715RBBIDataWrapperC1EP11UDataMemoryR10UErrorCode
	.set	_ZN6icu_6715RBBIDataWrapperC1EP11UDataMemoryR10UErrorCode,_ZN6icu_6715RBBIDataWrapperC2EP11UDataMemoryR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapperD2Ev
	.type	_ZN6icu_6715RBBIDataWrapperD2Ev, @function
_ZN6icu_6715RBBIDataWrapperD2Ev:
.LFB3156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rdi
	call	utrie2_close_67@PLT
	movq	64(%rbx), %rdi
	movq	$0, 48(%rbx)
	testq	%rdi, %rdi
	je	.L47
	call	udata_close_67@PLT
.L48:
	addq	$8, %rsp
	leaq	72(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	cmpb	$0, 136(%rbx)
	jne	.L48
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	addq	$8, %rsp
	leaq	72(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE3156:
	.size	_ZN6icu_6715RBBIDataWrapperD2Ev, .-_ZN6icu_6715RBBIDataWrapperD2Ev
	.globl	_ZN6icu_6715RBBIDataWrapperD1Ev
	.set	_ZN6icu_6715RBBIDataWrapperD1Ev,_ZN6icu_6715RBBIDataWrapperD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715RBBIDataWrappereqERKS0_
	.type	_ZNK6icu_6715RBBIDataWrappereqERKS0_, @function
_ZNK6icu_6715RBBIDataWrappereqERKS0_:
.LFB3158:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	cmpq	%rsi, %rdi
	je	.L52
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	cmpl	8(%rsi), %edx
	je	.L58
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	memcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3158:
	.size	_ZNK6icu_6715RBBIDataWrappereqERKS0_, .-_ZNK6icu_6715RBBIDataWrappereqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapper8hashCodeEv
	.type	_ZN6icu_6715RBBIDataWrapper8hashCodeEv, @function
_ZN6icu_6715RBBIDataWrapper8hashCodeEv:
.LFB3159:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	20(%rax), %eax
	ret
	.cfi_endproc
.LFE3159:
	.size	_ZN6icu_6715RBBIDataWrapper8hashCodeEv, .-_ZN6icu_6715RBBIDataWrapper8hashCodeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapper15removeReferenceEv
	.type	_ZN6icu_6715RBBIDataWrapper15removeReferenceEv, @function
_ZN6icu_6715RBBIDataWrapper15removeReferenceEv:
.LFB3160:
	.cfi_startproc
	endbr64
	lock subl	$1, 56(%rdi)
	je	.L67
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	48(%rdi), %rdi
	call	utrie2_close_67@PLT
	movq	64(%r12), %rdi
	movq	$0, 48(%r12)
	testq	%rdi, %rdi
	je	.L62
	call	udata_close_67@PLT
.L63:
	leaq	72(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	cmpb	$0, 136(%r12)
	jne	.L63
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L63
	.cfi_endproc
.LFE3160:
	.size	_ZN6icu_6715RBBIDataWrapper15removeReferenceEv, .-_ZN6icu_6715RBBIDataWrapper15removeReferenceEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapper12addReferenceEv
	.type	_ZN6icu_6715RBBIDataWrapper12addReferenceEv, @function
_ZN6icu_6715RBBIDataWrapper12addReferenceEv:
.LFB3161:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	lock addl	$1, 56(%rdi)
	ret
	.cfi_endproc
.LFE3161:
	.size	_ZN6icu_6715RBBIDataWrapper12addReferenceEv, .-_ZN6icu_6715RBBIDataWrapper12addReferenceEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715RBBIDataWrapper19getRuleSourceStringEv
	.type	_ZNK6icu_6715RBBIDataWrapper19getRuleSourceStringEv, @function
_ZNK6icu_6715RBBIDataWrapper19getRuleSourceStringEv:
.LFB3162:
	.cfi_startproc
	endbr64
	leaq	72(%rdi), %rax
	ret
	.cfi_endproc
.LFE3162:
	.size	_ZNK6icu_6715RBBIDataWrapper19getRuleSourceStringEv, .-_ZNK6icu_6715RBBIDataWrapper19getRuleSourceStringEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBIDataWrapper9printDataEv
	.type	_ZN6icu_6715RBBIDataWrapper9printDataEv, @function
_ZN6icu_6715RBBIDataWrapper9printDataEv:
.LFB3163:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3163:
	.size	_ZN6icu_6715RBBIDataWrapper9printDataEv, .-_ZN6icu_6715RBBIDataWrapper9printDataEv
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"ubrk_swap(): data format %02x.%02x.%02x.%02x (format version %02x) is not recognized\n"
	.align 8
.LC1:
	.string	"ubrk_swap(): RBBI Data header is invalid.\n"
	.align 8
.LC2:
	.string	"ubrk_swap(): too few bytes (%d after ICU Data header) for break data.\n"
	.text
	.p2align 4
	.globl	ubrk_swap_67
	.type	ubrk_swap_67, @function
ubrk_swap_67:
.LFB3164:
	.cfi_startproc
	endbr64
	testq	%r8, %r8
	je	.L83
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movl	(%r8), %r8d
	testl	%r8d, %r8d
	jg	.L71
	testq	%rdi, %rdi
	movq	%rdi, %r13
	sete	%dil
	testq	%rsi, %rsi
	sete	%al
	orb	%al, %dil
	jne	.L73
	cmpl	$-1, %edx
	jl	.L73
	testl	%edx, %edx
	jle	.L74
	testq	%rcx, %rcx
	je	.L73
.L74:
	cmpw	$29250, 12(%rsi)
	movzbl	16(%rsi), %eax
	jne	.L75
	cmpw	$8299, 14(%rsi)
	jne	.L75
	cmpb	$5, %al
	je	.L100
.L75:
	subq	$8, %rsp
	movzbl	12(%rsi), %edx
	movzbl	13(%rsi), %ecx
	movq	%r13, %rdi
	pushq	%rax
	movzbl	15(%rsi), %r9d
	xorl	%eax, %eax
	movzbl	14(%rsi), %r8d
	leaq	.LC0(%rip), %rsi
	call	udata_printError_67@PLT
	movl	$16, (%rbx)
	xorl	%r9d, %r9d
	popq	%rax
	popq	%rdx
.L71:
	leaq	-40(%rbp), %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movl	$1, (%rbx)
	xorl	%r9d, %r9d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%r9d, %r9d
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rbx, %r8
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	movl	%edx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	udata_swapDataHeader_67@PLT
	movq	-56(%rbp), %rsi
	movslq	%eax, %r15
	leaq	(%rsi,%r15), %r12
	movl	(%r12), %edi
	call	*16(%r13)
	movl	-64(%rbp), %edx
	movq	-72(%rbp), %rcx
	cmpl	$45472, %eax
	jne	.L76
	cmpb	$5, 4(%r12)
	movq	%rcx, -64(%rbp)
	movl	%edx, -56(%rbp)
	jne	.L76
	movl	8(%r12), %edi
	call	*16(%r13)
	cmpl	$79, %eax
	jbe	.L76
	movl	8(%r12), %edi
	call	*16(%r13)
	movl	-56(%rbp), %edx
	movq	-64(%rbp), %rcx
	leal	(%r15,%rax), %r9d
	cmpl	$-1, %edx
	je	.L71
	cmpl	%r9d, %edx
	jl	.L101
	leaq	(%rcx,%r15), %r14
	cmpq	%r14, %r12
	je	.L80
	movslq	%eax, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%r9d, -56(%rbp)
	call	memset@PLT
	movl	-56(%rbp), %r9d
.L80:
	movl	%r9d, -56(%rbp)
	movl	16(%r12), %edi
	call	*16(%r13)
	movl	20(%r12), %edi
	movslq	%eax, %r15
	call	*16(%r13)
	movl	-56(%rbp), %r9d
	testl	%eax, %eax
	movl	%eax, -64(%rbp)
	jle	.L81
	leaq	(%r14,%r15), %rcx
	leaq	(%r12,%r15), %rsi
	movq	%rbx, %r8
	movl	$16, %edx
	movq	%r13, %rdi
	addq	$16, %r15
	call	*56(%r13)
	movl	-64(%rbp), %r10d
	leaq	(%r14,%r15), %rcx
	leaq	(%r12,%r15), %rsi
	movq	%rbx, %r8
	movq	%r13, %rdi
	leal	-16(%r10), %edx
	call	*48(%r13)
	movl	-56(%rbp), %r9d
.L81:
	movl	%r9d, -56(%rbp)
	movl	24(%r12), %edi
	call	*16(%r13)
	movl	28(%r12), %edi
	movslq	%eax, %r15
	call	*16(%r13)
	movl	-56(%rbp), %r9d
	testl	%eax, %eax
	movl	%eax, -64(%rbp)
	jle	.L82
	leaq	(%r14,%r15), %rcx
	leaq	(%r12,%r15), %rsi
	movq	%rbx, %r8
	movl	$16, %edx
	movq	%r13, %rdi
	addq	$16, %r15
	call	*56(%r13)
	movl	-64(%rbp), %r10d
	leaq	(%r14,%r15), %rcx
	leaq	(%r12,%r15), %rsi
	movq	%rbx, %r8
	movq	%r13, %rdi
	leal	-16(%r10), %edx
	call	*48(%r13)
	movl	-56(%rbp), %r9d
.L82:
	movl	%r9d, -72(%rbp)
	movl	32(%r12), %edi
	call	*16(%r13)
	movl	36(%r12), %edi
	movl	%eax, %r15d
	call	*16(%r13)
	movl	32(%r12), %edi
	addq	%r14, %r15
	movl	%eax, -56(%rbp)
	call	*16(%r13)
	movl	-56(%rbp), %edx
	movq	%rbx, %r8
	movq	%r15, %rcx
	movl	%eax, %esi
	movq	%r13, %rdi
	addq	%r12, %rsi
	call	utrie2_swap_67@PLT
	movq	48(%r13), %r15
	movl	40(%r12), %edi
	call	*16(%r13)
	movl	44(%r12), %edi
	movl	%eax, %ecx
	addq	%r14, %rcx
	movq	%rcx, -64(%rbp)
	call	*16(%r13)
	movl	40(%r12), %edi
	movl	%eax, -56(%rbp)
	call	*16(%r13)
	movl	-56(%rbp), %edx
	movq	%rbx, %r8
	movq	-64(%rbp), %rcx
	movl	%eax, %esi
	movq	%r13, %rdi
	addq	%r12, %rsi
	call	*%r15
	movq	56(%r13), %r15
	movl	48(%r12), %edi
	call	*16(%r13)
	movl	52(%r12), %edi
	movl	%eax, %ecx
	addq	%r14, %rcx
	movq	%rcx, -64(%rbp)
	call	*16(%r13)
	movl	48(%r12), %edi
	movl	%eax, -56(%rbp)
	call	*16(%r13)
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %edx
	movq	%rbx, %r8
	movl	%eax, %esi
	movq	%r13, %rdi
	addq	%r12, %rsi
	call	*%r15
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	$80, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*56(%r13)
	leaq	4(%r14), %rsi
	movq	%rbx, %r8
	movl	$4, %edx
	movq	%rsi, %rcx
	movq	%r13, %rdi
	call	*56(%r13)
	movl	-72(%rbp), %r9d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$16, (%rbx)
	xorl	%r9d, %r9d
	jmp	.L71
.L101:
	movl	%eax, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	udata_printError_67@PLT
	movl	$8, (%rbx)
	xorl	%r9d, %r9d
	jmp	.L71
	.cfi_endproc
.LFE3164:
	.size	ubrk_swap_67, .-ubrk_swap_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
