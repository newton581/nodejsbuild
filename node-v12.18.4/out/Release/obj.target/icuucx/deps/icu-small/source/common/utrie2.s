	.file	"utrie2.cpp"
	.text
	.p2align 4
	.type	_ZL13enumSameValuePKvj, @function
_ZL13enumSameValuePKvj:
.LFB2751:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE2751:
	.size	_ZL13enumSameValuePKvj, .-_ZL13enumSameValuePKvj
	.p2align 4
	.type	_ZL14enumEitherTriePK6UTrie2iiPFjPKvjEPFaS3_iijES3_, @function
_ZL14enumEitherTriePK6UTrie2iiPFjPKvjEPFaS3_iijES3_:
.LFB2752:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -136(%rbp)
	movl	%edx, -112(%rbp)
	movq	%r8, -88(%rbp)
	testq	%r8, %r8
	je	.L3
	testq	%rcx, %rcx
	leaq	_ZL13enumSameValuePKvj(%rip), %rax
	movl	%esi, %r12d
	movq	%r9, %r14
	cmovne	%rcx, %rax
	movq	%rax, -56(%rbp)
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.L84
	movq	144128(%rax), %rcx
	movl	144160(%rax), %edi
	movq	$0, -96(%rbp)
	movl	144164(%rax), %eax
	movq	%rcx, -128(%rbp)
	movl	%edi, -140(%rbp)
	movl	%eax, -104(%rbp)
.L8:
	movq	-136(%rbp), %rax
	movq	%r14, %rdi
	movl	%r12d, %r15d
	movl	44(%rax), %ebx
	movl	36(%rax), %esi
	movq	-56(%rbp), %rax
	call	*%rax
	movl	%eax, -100(%rbp)
	movl	-112(%rbp), %eax
	cmpl	%eax, %ebx
	cmovg	%eax, %ebx
	movl	%ebx, -144(%rbp)
	cmpl	%ebx, %r12d
	jge	.L42
	xorl	%r13d, %r13d
	movl	$-1, %r8d
	movl	$-1, %eax
	movl	%r13d, %r10d
	movl	%r8d, %ebx
	movl	%r12d, %r13d
.L10:
	movl	-112(%rbp), %ecx
	leal	2048(%r13), %r12d
	movl	%r13d, %edx
	cmpl	%r12d, %ecx
	cmovg	%r12d, %ecx
	cmpl	$65535, %r13d
	jg	.L11
	andl	$-2048, %edx
	cmpl	$55296, %edx
	je	.L12
	movl	%r13d, %edx
	sarl	$5, %edx
	movl	%edx, -72(%rbp)
.L13:
	movl	-140(%rbp), %ebx
	cmpl	%ebx, -72(%rbp)
	jne	.L19
.L39:
	cmpl	%r10d, -100(%rbp)
	je	.L44
	cmpl	%r13d, %r15d
	jl	.L85
.L83:
	movl	-104(%rbp), %eax
	movl	-100(%rbp), %r10d
	movl	%r13d, %r15d
	movl	%r12d, %r13d
.L18:
	cmpl	-144(%rbp), %r13d
	jl	.L10
	movl	%r13d, %r12d
	movl	%r10d, %r13d
.L9:
	cmpl	%r12d, -112(%rbp)
	jl	.L34
	jle	.L53
	cmpq	$0, -96(%rbp)
	movq	-136(%rbp), %rax
	je	.L35
	movq	-128(%rbp), %rdi
	movslq	48(%rax), %rax
	testq	%rdi, %rdi
	je	.L36
	movl	(%rdi,%rax,4), %esi
.L37:
	movq	-56(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, %ebx
	cmpl	%r13d, %eax
	je	.L34
	cmpl	%r12d, %r15d
	jge	.L54
	movq	-88(%rbp), %rax
	leal	-1(%r12), %edx
	movl	%r13d, %ecx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	je	.L3
.L54:
	movl	%r12d, %r15d
	movl	%ebx, %r13d
.L34:
	movl	-112(%rbp), %edx
	movq	-88(%rbp), %rax
	addq	$104, %rsp
	movl	%r13d, %ecx
	popq	%rbx
	movl	%r15d, %esi
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	subl	$1, %edx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	sarl	$11, %edx
	testq	%rdi, %rdi
	je	.L16
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edi
	movl	%edi, -72(%rbp)
.L17:
	cmpl	%ebx, -72(%rbp)
	jne	.L13
	movl	%r13d, %edx
	subl	%r15d, %edx
	cmpl	$2047, %edx
	jle	.L13
.L44:
	movl	%r12d, %r13d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L85:
	movq	-88(%rbp), %rax
	leal	-1(%r13), %edx
	movl	%r10d, %ecx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L83
.L3:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	%r13d, %edx
	movl	%r13d, %edi
	movl	%ecx, %esi
	movl	$64, -108(%rbp)
	sarl	$5, %edx
	sarl	$11, %edi
	sarl	$11, %esi
	andl	$63, %edx
	cmpl	%esi, %edi
	je	.L86
.L21:
	movslq	%edx, %rcx
	movslq	-72(%rbp), %rdx
	movl	%r13d, %r12d
	movl	%r10d, %r13d
	movq	%rcx, -80(%rbp)
	movq	-96(%rbp), %rcx
	leaq	(%rcx,%rdx,2), %rdx
	movq	%rdx, -120(%rbp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L89:
	cmpl	%r13d, -100(%rbp)
	je	.L27
	cmpl	%r12d, %r15d
	jge	.L48
	movq	-88(%rbp), %rax
	leal	-1(%r12), %edx
	movl	%r13d, %ecx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	je	.L3
.L48:
	movl	%r12d, %r15d
.L27:
	movl	-68(%rbp), %eax
	movl	-100(%rbp), %r13d
	addl	$32, %r12d
.L25:
	addq	$1, -80(%rbp)
	movq	-80(%rbp), %rcx
	cmpl	%ecx, -108(%rbp)
	jle	.L87
.L33:
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rcx
	addl	%ecx, %edx
	cmpq	$0, -96(%rbp)
	je	.L22
	movq	-120(%rbp), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	leal	0(,%rdx,4), %edi
	movl	%edi, -68(%rbp)
.L23:
	cmpl	%eax, -68(%rbp)
	je	.L88
.L24:
	movl	-104(%rbp), %edi
	cmpl	%edi, -68(%rbp)
	je	.L89
	cmpq	$0, -128(%rbp)
	movslq	-68(%rbp), %rax
	je	.L90
	movq	-128(%rbp), %rcx
	movl	%r15d, -60(%rbp)
	leaq	(%rcx,%rax,4), %rbx
	leal	32(%r12), %eax
	movl	%eax, -64(%rbp)
	movl	%r12d, %eax
	movq	%rbx, %r12
	movl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L32:
	movl	(%r12), %esi
	movq	-56(%rbp), %rax
	movl	%r13d, %r15d
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, %r13d
	cmpl	%r15d, %eax
	je	.L31
	movl	-60(%rbp), %esi
	cmpl	%esi, %ebx
	jle	.L51
	movq	-88(%rbp), %rax
	leal	-1(%rbx), %edx
	movl	%r15d, %ecx
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	je	.L3
.L51:
	movl	%ebx, -60(%rbp)
.L31:
	addl	$1, %ebx
	addq	$4, %r12
	cmpl	-64(%rbp), %ebx
	jne	.L32
	addq	$1, -80(%rbp)
	movl	-60(%rbp), %r15d
	movl	%ebx, %r12d
	movl	-68(%rbp), %eax
	movq	-80(%rbp), %rcx
	cmpl	%ecx, -108(%rbp)
	jg	.L33
.L87:
	movl	%r13d, %r10d
	movl	%r12d, %r13d
.L82:
	movl	-72(%rbp), %ebx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L88:
	movl	%r12d, %edx
	subl	%r15d, %edx
	cmpl	$31, %edx
	jle	.L24
	addl	$32, %r12d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L22:
	movq	-136(%rbp), %rcx
	movslq	%edx, %rdx
	movq	72(%rcx), %rcx
	movl	2176(%rcx,%rdx,4), %ecx
	movl	%ecx, -68(%rbp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L90:
	movq	-96(%rbp), %rdx
	movl	%r13d, %r8d
	leaq	(%rdx,%rax,2), %rbx
	leal	32(%r12), %eax
	movl	%eax, -60(%rbp)
	movq	%rbx, %r13
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L92:
	movl	%eax, -64(%rbp)
	leal	-1(%r12), %edx
	movq	-88(%rbp), %rax
	movl	%ebx, %ecx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	je	.L3
	movl	-64(%rbp), %r8d
	movl	%r12d, %r15d
.L29:
	addl	$1, %r12d
	addq	$2, %r13
	cmpl	%r12d, -60(%rbp)
	je	.L91
.L30:
	movl	%r8d, %ebx
	movzwl	0(%r13), %esi
	movq	-56(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, %r8d
	cmpl	%eax, %ebx
	je	.L29
	cmpl	%r12d, %r15d
	jl	.L92
	movl	%r12d, %r15d
	addq	$2, %r13
	addl	$1, %r12d
	cmpl	%r12d, -60(%rbp)
	jne	.L30
.L91:
	movl	-68(%rbp), %eax
	movl	%r8d, %r13d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L12:
	testl	$1024, %r13d
	jne	.L93
	cmpl	$2048, -140(%rbp)
	movl	$2048, %ebx
	je	.L39
	movl	%r13d, %edx
	movl	$32, -108(%rbp)
	sarl	$5, %edx
	movl	$2048, -72(%rbp)
	andl	$63, %edx
	jmp	.L38
.L16:
	movq	-136(%rbp), %rdi
	movslq	%edx, %rdx
	movq	72(%rdi), %rsi
	movl	(%rsi,%rdx,4), %edi
	movl	%edi, -72(%rbp)
	jmp	.L17
.L84:
	movq	%rdi, %rax
	movq	(%rdi), %rdx
	movq	16(%rdi), %rcx
	movzwl	34(%rax), %eax
	movzwl	32(%rdi), %edi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -128(%rbp)
	movl	%edi, -140(%rbp)
	movl	%eax, -104(%rbp)
	jmp	.L8
.L86:
	sarl	$5, %ecx
	andl	$63, %ecx
	movl	%ecx, -108(%rbp)
.L38:
	cmpl	-108(%rbp), %edx
	jl	.L21
	jmp	.L82
.L93:
	cmpl	$1728, -140(%rbp)
	movl	$1728, %ebx
	je	.L39
	movl	%r13d, %edx
	movl	$64, -108(%rbp)
	sarl	$5, %edx
	movl	$1728, -72(%rbp)
	andl	$63, %edx
	jmp	.L21
.L42:
	xorl	%r13d, %r13d
	jmp	.L9
.L53:
	movl	%r12d, -112(%rbp)
	jmp	.L34
.L36:
	movq	-96(%rbp), %rcx
	movzwl	(%rcx,%rax,2), %esi
	jmp	.L37
.L35:
	movq	72(%rax), %rax
	movslq	144152(%rax), %rdx
	movq	144128(%rax), %rax
	movl	-16(%rax,%rdx,4), %esi
	jmp	.L37
	.cfi_endproc
.LFE2752:
	.size	_ZL14enumEitherTriePK6UTrie2iiPFjPKvjEPFaS3_iijES3_, .-_ZL14enumEitherTriePK6UTrie2iiPFjPKvjEPFaS3_iijES3_
	.p2align 4
	.globl	utrie2_get32_67
	.type	utrie2_get32_67, @function
utrie2_get32_67:
.LFB2741:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	je	.L95
	movq	(%rdi), %rdx
	cmpl	$55295, %esi
	ja	.L96
	movl	%esi, %eax
	sarl	$5, %eax
.L116:
	cltq
	andl	$31, %esi
	movzwl	(%rdx,%rax,2), %eax
	leal	(%rsi,%rax,4), %eax
	cltq
	addq	%rax, %rax
.L97:
	movzwl	(%rdx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	cmpl	$65535, %esi
	jbe	.L119
	cmpl	$1114111, %esi
	jbe	.L100
	movl	24(%rdi), %eax
	subl	$-128, %eax
	cltq
	addq	%rax, %rax
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L95:
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L103
	cmpl	$55295, %esi
	jbe	.L120
	cmpl	$65535, %esi
	ja	.L106
	cmpl	$56320, %esi
	movl	$320, %eax
	movl	$0, %ecx
	movq	(%rdi), %rdi
	cmovl	%eax, %ecx
	movl	%esi, %eax
	sarl	$5, %eax
.L118:
	addl	%ecx, %eax
	andl	$31, %esi
	cltq
	movzwl	(%rdi,%rax,2), %eax
	leal	(%rsi,%rax,4), %eax
	cltq
	salq	$2, %rax
.L105:
	movl	(%rdx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	cmpl	$1114111, %esi
	ja	.L121
	movq	72(%rdi), %rax
	movq	144128(%rax), %rcx
	cmpl	144168(%rax), %esi
	jl	.L110
	movslq	144152(%rax), %rax
	movl	-16(%rcx,%rax,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	cmpl	$56320, %esi
	movl	$320, %eax
	movl	$0, %ecx
	cmovl	%eax, %ecx
	movl	%esi, %eax
	sarl	$5, %eax
	addl	%ecx, %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L120:
	movl	%esi, %eax
	movq	(%rdi), %rcx
	andl	$31, %esi
	sarl	$5, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	leal	(%rsi,%rax,4), %eax
	cltq
	salq	$2, %rax
	movl	(%rdx,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	movl	40(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	cmpl	%esi, 44(%rdi)
	jg	.L101
	movslq	48(%rdi), %rax
	addq	%rax, %rax
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L110:
	movl	%esi, %edi
	movl	%esi, %edx
	andl	$-1024, %edi
	sarl	$5, %edx
	cmpl	$55296, %edi
	je	.L122
	movl	%esi, %edi
	andl	$63, %edx
	sarl	$11, %edi
	movslq	%edi, %rdi
	addl	(%rax,%rdi,4), %edx
.L112:
	movslq	%edx, %rdx
	andl	$31, %esi
	addl	2176(%rax,%rdx,4), %esi
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$512, %eax
	cmpl	$1114111, %esi
	ja	.L105
	cmpl	%esi, 44(%rdi)
	jg	.L108
	movslq	48(%rdi), %rax
	salq	$2, %rax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L101:
	movl	%esi, %eax
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdx,%rax,2), %ecx
	movl	%esi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	addl	%ecx, %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L122:
	addl	$320, %edx
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L108:
	movl	%esi, %eax
	movq	(%rdi), %rdi
	sarl	$11, %eax
	addl	$2080, %eax
	cltq
	movzwl	(%rdi,%rax,2), %ecx
	movl	%esi, %eax
	sarl	$5, %eax
	andl	$63, %eax
	jmp	.L118
	.cfi_endproc
.LFE2741:
	.size	utrie2_get32_67, .-utrie2_get32_67
	.p2align 4
	.globl	utrie2_get32FromLeadSurrogateCodeUnit_67
	.type	utrie2_get32FromLeadSurrogateCodeUnit_67, @function
utrie2_get32FromLeadSurrogateCodeUnit_67:
.LFB2742:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L124
	movl	40(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	movl	%esi, %eax
	andl	$31, %esi
	sarl	$5, %eax
	cmpq	$0, 8(%rdi)
	je	.L126
	movq	(%rdi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	leal	(%rsi,%rax,4), %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L127
	movq	(%rdi), %rcx
	cltq
	movzwl	(%rcx,%rax,2), %eax
	leal	(%rsi,%rax,4), %eax
	cltq
	movl	(%rdx,%rax,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	movq	72(%rdi), %rdx
	andl	$63, %eax
	addl	108(%rdx), %eax
	cltq
	addl	2176(%rdx,%rax,4), %esi
	movq	144128(%rdx), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %eax
	ret
	.cfi_endproc
.LFE2742:
	.size	utrie2_get32FromLeadSurrogateCodeUnit_67, .-utrie2_get32FromLeadSurrogateCodeUnit_67
	.p2align 4
	.globl	utrie2_internalU8NextIndex_67
	.type	utrie2_internalU8NextIndex_67, @function
utrie2_internalU8NextIndex_67:
.LFB2744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	%rdx, %rcx
	movl	%esi, %r9d
	movl	$-1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movq	%rcx, %rdx
	leaq	-28(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$7, %rcx
	movl	$7, %eax
	movl	%r9d, %ecx
	cmovg	%rax, %rdx
	movl	$0, -28(%rbp)
	call	utf8_nextCharSafeBody_67@PLT
	movl	-28(%rbp), %ecx
	cmpl	$55295, %eax
	ja	.L129
	movl	%eax, %edx
	movq	(%rbx), %rsi
	andl	$31, %eax
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	leal	0(,%rdx,8), %eax
.L130:
	orl	%ecx, %eax
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L140
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	cmpl	$65535, %eax
	ja	.L131
	cmpl	$56320, %eax
	movl	$320, %edx
	movl	$0, %esi
	movq	(%rbx), %rdi
	cmovl	%edx, %esi
	movl	%eax, %edx
	sarl	$5, %edx
.L139:
	addl	%esi, %edx
	andl	$31, %eax
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %edx
	leal	0(,%rdx,8), %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L131:
	cmpl	$1114111, %eax
	ja	.L141
	cmpl	44(%rbx), %eax
	jl	.L134
	movl	48(%rbx), %edx
	leal	0(,%rdx,8), %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L141:
	cmpq	$0, 16(%rbx)
	movl	$1024, %eax
	jne	.L130
	movl	24(%rbx), %eax
	leal	1024(,%rax,8), %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L134:
	movl	%eax, %edx
	movq	(%rbx), %rdi
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %esi
	movl	%eax, %edx
	sarl	$5, %edx
	andl	$63, %edx
	jmp	.L139
.L140:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2744:
	.size	utrie2_internalU8NextIndex_67, .-utrie2_internalU8NextIndex_67
	.p2align 4
	.globl	utrie2_internalU8PrevIndex_67
	.type	utrie2_internalU8PrevIndex_67, @function
utrie2_internalU8PrevIndex_67:
.LFB2745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rcx, %rbx
	subq	%rdx, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$7, %rbx
	jle	.L144
	leaq	-7(%rcx), %rdi
	movl	$7, %ebx
.L144:
	leaq	-28(%rbp), %rdx
	movl	$-1, %r8d
	movl	%r9d, %ecx
	xorl	%esi, %esi
	movl	%ebx, -28(%rbp)
	call	utf8_prevCharSafeBody_67@PLT
	subl	-28(%rbp), %ebx
	cmpl	$55295, %eax
	ja	.L145
	movl	%eax, %edx
	movq	(%r12), %rcx
	andl	$31, %eax
	sarl	$5, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	leal	(%rax,%rdx,4), %eax
	sall	$3, %eax
.L146:
	orl	%ebx, %eax
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L157
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	cmpl	$65535, %eax
	ja	.L147
	cmpl	$56320, %eax
	movl	$320, %edx
	movl	$0, %ecx
	movq	(%r12), %rsi
	cmovl	%edx, %ecx
	movl	%eax, %edx
	sarl	$5, %edx
.L156:
	addl	%ecx, %edx
	andl	$31, %eax
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %eax
	sall	$3, %eax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L147:
	cmpl	$1114111, %eax
	ja	.L158
	cmpl	44(%r12), %eax
	jl	.L150
	movl	48(%r12), %eax
	sall	$3, %eax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L158:
	cmpq	$0, 16(%r12)
	movl	$1024, %eax
	jne	.L146
	movl	24(%r12), %eax
	leal	1024(,%rax,8), %eax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L150:
	movl	%eax, %edx
	movq	(%r12), %rsi
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %ecx
	movl	%eax, %edx
	sarl	$5, %edx
	andl	$63, %edx
	jmp	.L156
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2745:
	.size	utrie2_internalU8PrevIndex_67, .-utrie2_internalU8PrevIndex_67
	.p2align 4
	.globl	utrie2_openFromSerialized_67
	.type	utrie2_openFromSerialized_67, @function
utrie2_openFromSerialized_67:
.LFB2746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L159
	testl	%edx, %edx
	jle	.L161
	movq	%rsi, %rbx
	testb	$3, %sil
	jne	.L161
	movl	%edi, %r13d
	cmpl	$1, %edi
	ja	.L161
	cmpl	$15, %edx
	jle	.L165
	cmpl	$1416784178, (%rsi)
	jne	.L165
	movzwl	4(%rsi), %eax
	andl	$15, %eax
	cmpl	%edi, %eax
	jne	.L165
	movl	10(%rbx), %edi
	pxor	%xmm0, %xmm0
	movzwl	8(%rbx), %r9d
	movaps	%xmm0, -112(%rbp)
	movzwl	6(%rsi), %esi
	movzwl	12(%rbx), %r12d
	movl	%edi, -112(%rbp)
	movzwl	14(%rbx), %edi
	leal	0(,%r9,4), %eax
	movaps	%xmm0, -128(%rbp)
	movq	%rsi, %r15
	leal	8(%rsi), %r14d
	sall	$11, %edi
	movl	%esi, -120(%rbp)
	movl	%edi, -100(%rbp)
	leal	-4(%rax), %edi
	movl	%eax, -116(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%r13d, %r13d
	jne	.L166
	addl	%edi, %esi
	addl	%r14d, %eax
	movl	%esi, -96(%rbp)
	leal	(%rax,%rax), %r14d
.L170:
	movq	%rcx, -152(%rbp)
	cmpl	%edx, %r14d
	jg	.L165
	movl	$80, %edi
	movq	%r8, -160(%rbp)
	call	uprv_malloc_67@PLT
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rcx
	testq	%rax, %rax
	je	.L177
	movdqu	-88(%rbp), %xmm4
	movdqu	-120(%rbp), %xmm2
	movzwl	%r12w, %r8d
	movdqu	-136(%rbp), %xmm1
	movdqu	-104(%rbp), %xmm3
	movq	-72(%rbp), %rsi
	movups	%xmm4, 56(%rax)
	movq	%rbx, 56(%rax)
	addq	$16, %rbx
	movq	%rsi, 72(%rax)
	leaq	(%rbx,%r15,2), %rdx
	movl	%r14d, 64(%rax)
	movb	$0, 68(%rax)
	movq	%rbx, (%rax)
	movups	%xmm1, 8(%rax)
	movups	%xmm2, 24(%rax)
	movups	%xmm3, 40(%rax)
	cmpl	$1, %r13d
	je	.L168
	movq	%rdx, 8(%rax)
	movzwl	(%rbx,%r8,2), %esi
	movzwl	256(%rdx), %edx
	movq	$0, 16(%rax)
	movl	%esi, 36(%rax)
	movl	%edx, 40(%rax)
.L169:
	testq	%rcx, %rcx
	je	.L159
	movl	%r14d, (%rcx)
	.p2align 4,,10
	.p2align 3
.L159:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L178
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movl	$1, (%r8)
	xorl	%eax, %eax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$3, (%r8)
	xorl	%eax, %eax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L166:
	sall	$4, %r9d
	movl	%edi, -96(%rbp)
	leal	(%r9,%r14,2), %r14d
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L168:
	movl	(%rdx,%r8,4), %esi
	movq	%rdx, 16(%rax)
	movl	512(%rdx), %edx
	movq	$0, 8(%rax)
	movl	%esi, 36(%rax)
	movl	%edx, 40(%rax)
	jmp	.L169
.L178:
	call	__stack_chk_fail@PLT
.L177:
	movl	$7, (%r8)
	jmp	.L159
	.cfi_endproc
.LFE2746:
	.size	utrie2_openFromSerialized_67, .-utrie2_openFromSerialized_67
	.p2align 4
	.globl	utrie2_openDummy_67
	.type	utrie2_openDummy_67, @function
utrie2_openDummy_67:
.LFB2747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r11d
	movl	%edx, -52(%rbp)
	testl	%r11d, %r11d
	jg	.L179
	movl	%edi, %r13d
	movq	%rcx, %r14
	cmpl	$1, %edi
	ja	.L197
	sbbq	%r8, %r8
	movl	%esi, %r15d
	andq	$-392, %r8
	addq	$5024, %r8
	cmpl	$1, %edi
	movl	$80, %edi
	sbbl	%ebx, %ebx
	movq	%r8, -64(%rbp)
	movq	%r8, -72(%rbp)
	andl	$-392, %ebx
	call	uprv_malloc_67@PLT
	addl	$5024, %ebx
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L198
	pxor	%xmm0, %xmm0
	movq	%r8, %rdi
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	call	uprv_malloc_67@PLT
	movq	%rax, 56(%r12)
	testq	%rax, %rax
	je	.L199
	cmpl	$1, %r13d
	movl	%ebx, 64(%r12)
	movabsq	$841813592128, %rbx
	movl	$2112, %r9d
	sbbl	%edi, %edi
	movq	%rbx, 24(%r12)
	movl	-52(%rbp), %ebx
	andw	$2112, %di
	movb	$1, 68(%r12)
	subw	$-128, %di
	cmpl	$1, %r13d
	movl	%r15d, 36(%r12)
	sbbl	%ecx, %ecx
	movl	%ebx, 40(%r12)
	andw	$528, %cx
	cmpl	$1, %r13d
	movl	$1416784178, (%rax)
	sbbl	%edx, %edx
	movd	%ecx, %xmm0
	movl	$0, 44(%r12)
	leaq	4176(%rax), %rcx
	andl	$2112, %edx
	movw	%r13w, 4(%rax)
	punpcklwd	%xmm0, %xmm0
	addl	$192, %edx
	cmpl	$1, %r13d
	movw	%r9w, 6(%rax)
	pshufd	$0, %xmm0, %xmm0
	sbbl	%esi, %esi
	movl	%edx, 48(%r12)
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
	andw	$2112, %si
	leaq	16(%rax), %rdx
	movw	%r8w, 32(%r12)
	movw	%si, 34(%r12)
	movl	$49, 8(%rax)
	movw	%si, 12(%rax)
	movw	%r10w, 14(%rax)
	movq	%rdx, (%r12)
	.p2align 4,,10
	.p2align 3
.L186:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L186
	movd	%esi, %xmm0
	movw	%di, 4176(%rax)
	leaq	4240(%rax), %rdx
	punpcklwd	%xmm0, %xmm0
	movw	%di, 4178(%rax)
	pshufd	$0, %xmm0, %xmm0
	movw	%si, 4228(%rax)
	movw	%si, 4230(%rax)
	movw	%si, 4232(%rax)
	movw	%si, 4234(%rax)
	movw	%si, 4236(%rax)
	movw	%si, 4238(%rax)
	movups	%xmm0, 4180(%rax)
	movups	%xmm0, 4196(%rax)
	movups	%xmm0, 4212(%rax)
	cmpl	$1, %r13d
	je	.L200
	movd	%r15d, %xmm0
	movq	%rdx, 8(%r12)
	punpcklwd	%xmm0, %xmm0
	movw	%r15w, 4624(%rax)
	pshufd	$0, %xmm0, %xmm0
	movw	%r15w, 4626(%rax)
	movups	%xmm0, 4240(%rax)
	movups	%xmm0, 4256(%rax)
	movups	%xmm0, 4272(%rax)
	movups	%xmm0, 4288(%rax)
	movups	%xmm0, 4304(%rax)
	movups	%xmm0, 4320(%rax)
	movups	%xmm0, 4336(%rax)
	movups	%xmm0, 4352(%rax)
	movups	%xmm0, 4368(%rax)
	movups	%xmm0, 4384(%rax)
	movups	%xmm0, 4400(%rax)
	movups	%xmm0, 4416(%rax)
	movups	%xmm0, 4432(%rax)
	movups	%xmm0, 4448(%rax)
	movups	%xmm0, 4464(%rax)
	movups	%xmm0, 4480(%rax)
	movd	-52(%rbp), %xmm0
	punpcklwd	%xmm0, %xmm0
	movw	%r15w, 4628(%rax)
	movq	$0, 16(%r12)
	pshufd	$0, %xmm0, %xmm0
	movw	%r15w, 4630(%rax)
	movups	%xmm0, 4496(%rax)
	movups	%xmm0, 4512(%rax)
	movups	%xmm0, 4528(%rax)
	movups	%xmm0, 4544(%rax)
	movups	%xmm0, 4560(%rax)
	movups	%xmm0, 4576(%rax)
	movups	%xmm0, 4592(%rax)
	movups	%xmm0, 4608(%rax)
.L179:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L200:
	.cfi_restore_state
	movq	$0, 8(%r12)
	movd	%r15d, %xmm2
	leaq	4752(%rax), %rcx
	movq	%rdx, 16(%r12)
	pshufd	$0, %xmm2, %xmm0
	.p2align 4,,10
	.p2align 3
.L190:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L190
	movd	-52(%rbp), %xmm3
	movups	%xmm0, 5008(%rax)
	pshufd	$0, %xmm3, %xmm1
	movups	%xmm1, 4752(%rax)
	movups	%xmm1, 4768(%rax)
	movups	%xmm1, 4784(%rax)
	movups	%xmm1, 4800(%rax)
	movups	%xmm1, 4816(%rax)
	movups	%xmm1, 4832(%rax)
	movups	%xmm1, 4848(%rax)
	movups	%xmm1, 4864(%rax)
	movups	%xmm1, 4880(%rax)
	movups	%xmm1, 4896(%rax)
	movups	%xmm1, 4912(%rax)
	movups	%xmm1, 4928(%rax)
	movups	%xmm1, 4944(%rax)
	movups	%xmm1, 4960(%rax)
	movups	%xmm1, 4976(%rax)
	movups	%xmm1, 4992(%rax)
	jmp	.L179
.L197:
	movl	$1, (%rcx)
	jmp	.L179
.L198:
	movl	$7, (%r14)
	jmp	.L179
.L199:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	uprv_free_67@PLT
	movl	$7, (%r14)
	jmp	.L179
	.cfi_endproc
.LFE2747:
	.size	utrie2_openDummy_67, .-utrie2_openDummy_67
	.p2align 4
	.globl	utrie2_close_67
	.type	utrie2_close_67, @function
utrie2_close_67:
.LFB2748:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L201
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpb	$0, 68(%rdi)
	jne	.L211
	movq	72(%r12), %rax
	testq	%rax, %rax
	je	.L204
.L212:
	movq	144128(%rax), %rdi
	call	uprv_free_67@PLT
	movq	72(%r12), %rdi
	call	uprv_free_67@PLT
.L204:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movq	56(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	72(%r12), %rax
	testq	%rax, %rax
	jne	.L212
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2748:
	.size	utrie2_close_67, .-utrie2_close_67
	.p2align 4
	.globl	utrie2_isFrozen_67
	.type	utrie2_isFrozen_67, @function
utrie2_isFrozen_67:
.LFB2749:
	.cfi_startproc
	endbr64
	cmpq	$0, 72(%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE2749:
	.size	utrie2_isFrozen_67, .-utrie2_isFrozen_67
	.p2align 4
	.globl	utrie2_serialize_67
	.type	utrie2_serialize_67, @function
utrie2_serialize_67:
.LFB2750:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L237
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L216
	movq	%rsi, %rdi
	movq	56(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L216
	cmpq	$0, 72(%rbx)
	jne	.L216
	testl	%edx, %edx
	js	.L216
	je	.L217
	testq	%rdi, %rdi
	je	.L216
	testb	$3, %dil
	jne	.L216
.L217:
	movl	64(%rbx), %eax
	cmpl	%edx, %eax
	jle	.L240
	movl	$15, (%rcx)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$1, (%rcx)
	xorl	%eax, %eax
.L214:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movslq	%eax, %rdx
	call	memcpy@PLT
	movl	64(%rbx), %eax
	jmp	.L214
	.cfi_endproc
.LFE2750:
	.size	utrie2_serialize_67, .-utrie2_serialize_67
	.p2align 4
	.globl	utrie2_enum_67
	.type	utrie2_enum_67, @function
utrie2_enum_67:
.LFB2753:
	.cfi_startproc
	endbr64
	movq	%rdx, %r8
	movq	%rcx, %r9
	movl	$1114112, %edx
	movq	%rsi, %rcx
	xorl	%esi, %esi
	jmp	_ZL14enumEitherTriePK6UTrie2iiPFjPKvjEPFaS3_iijES3_
	.cfi_endproc
.LFE2753:
	.size	utrie2_enum_67, .-utrie2_enum_67
	.p2align 4
	.globl	utrie2_enumForLeadSurrogate_67
	.type	utrie2_enumForLeadSurrogate_67, @function
utrie2_enumForLeadSurrogate_67:
.LFB2754:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L244
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	subl	$55232, %esi
	movq	%r8, %r9
	movq	%rcx, %r8
	movq	%rdx, %rcx
	sall	$10, %esi
	leal	1024(%rsi), %r10d
	movl	%r10d, %edx
	jmp	_ZL14enumEitherTriePK6UTrie2iiPFjPKvjEPFaS3_iijES3_
	.cfi_endproc
.LFE2754:
	.size	utrie2_enumForLeadSurrogate_67, .-utrie2_enumForLeadSurrogate_67
	.align 2
	.p2align 4
	.globl	_ZN6icu_6728BackwardUTrie2StringIterator10previous16Ev
	.type	_ZN6icu_6728BackwardUTrie2StringIterator10previous16Ev, @function
_ZN6icu_6728BackwardUTrie2StringIterator10previous16Ev:
.LFB2755:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	32(%rdi), %rcx
	movq	(%rdi), %rsi
	movq	%rdx, 16(%rdi)
	cmpq	%rcx, %rdx
	jbe	.L259
	movzwl	-2(%rdx), %eax
	leaq	-2(%rdx), %r11
	movq	(%rsi), %r8
	movq	%r11, 8(%rdi)
	movl	%eax, %r10d
	movl	%eax, 24(%rdi)
	movl	%eax, %r9d
	andl	$-1024, %r10d
	cmpl	$56320, %r10d
	je	.L260
	cmpl	$55296, %r10d
	movl	$320, %ecx
	movl	$0, %edx
	cmovne	%edx, %ecx
.L249:
	sarl	$5, %eax
	addl	%ecx, %eax
	cltq
	movzwl	(%r8,%rax,2), %edx
	movl	%r9d, %eax
	andl	$31, %eax
	leal	(%rax,%rdx,4), %eax
	cltq
	movzwl	(%r8,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	cmpq	%r11, %rcx
	je	.L254
	movzwl	-4(%rdx), %r10d
	xorl	%ecx, %ecx
	movl	%r10d, %r11d
	andl	$-1024, %r11d
	cmpl	$55296, %r11d
	jne	.L249
	sall	$10, %r10d
	subq	$4, %rdx
	leal	-56613888(%rax,%r10), %eax
	movq	%rdx, 8(%rdi)
	movl	%eax, 24(%rdi)
	cmpl	44(%rsi), %eax
	jge	.L251
	movl	%eax, %edx
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %ecx
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	andl	$63, %edx
	addl	%ecx, %edx
	movslq	%edx, %rdx
	movzwl	(%r8,%rdx,2), %edx
	leal	(%rax,%rdx,4), %eax
	cltq
	addq	%rax, %rax
.L253:
	movzwl	(%r8,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$-1, 24(%rdi)
	movzwl	40(%rsi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	xorl	%ecx, %ecx
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L251:
	movslq	48(%rsi), %rax
	addq	%rax, %rax
	jmp	.L253
	.cfi_endproc
.LFE2755:
	.size	_ZN6icu_6728BackwardUTrie2StringIterator10previous16Ev, .-_ZN6icu_6728BackwardUTrie2StringIterator10previous16Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6727ForwardUTrie2StringIterator6next16Ev
	.type	_ZN6icu_6727ForwardUTrie2StringIterator6next16Ev, @function
_ZN6icu_6727ForwardUTrie2StringIterator6next16Ev:
.LFB2756:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	32(%rdi), %r8
	movq	(%rdi), %r9
	movq	%rdx, 8(%rdi)
	cmpq	%r8, %rdx
	je	.L270
	movzwl	(%rdx), %eax
	leaq	2(%rdx), %r10
	movq	(%r9), %rsi
	movq	%r10, 16(%rdi)
	movl	%eax, %r11d
	movl	%eax, 24(%rdi)
	movl	%eax, %ecx
	andl	$-1024, %r11d
	cmpl	$55296, %r11d
	je	.L264
	sarl	$5, %eax
.L269:
	cltq
	andl	$31, %ecx
	movzwl	(%rsi,%rax,2), %eax
	leal	(%rcx,%rax,4), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	cmpq	%r10, %r8
	je	.L265
	movzwl	2(%rdx), %r8d
	movl	%r8d, %r10d
	andl	$-1024, %r10d
	cmpl	$56320, %r10d
	jne	.L265
	sall	$10, %eax
	addq	$4, %rdx
	leal	-56613888(%r8,%rax), %eax
	movq	%rdx, 16(%rdi)
	movl	%eax, 24(%rdi)
	cmpl	44(%r9), %eax
	jge	.L271
	movl	%eax, %edx
	sarl	$11, %edx
	addl	$2080, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %ecx
	movl	%eax, %edx
	andl	$31, %eax
	sarl	$5, %edx
	andl	$63, %edx
	addl	%ecx, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	leal	(%rax,%rdx,4), %eax
	cltq
	addq	%rax, %rax
.L268:
	movzwl	(%rsi,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	sarl	$5, %eax
	addl	$320, %eax
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L270:
	movl	$-1, 24(%rdi)
	movzwl	40(%r9), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	movslq	48(%r9), %rax
	addq	%rax, %rax
	jmp	.L268
	.cfi_endproc
.LFE2756:
	.size	_ZN6icu_6727ForwardUTrie2StringIterator6next16Ev, .-_ZN6icu_6727ForwardUTrie2StringIterator6next16Ev
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
