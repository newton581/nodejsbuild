	.file	"unistr.cpp"
	.text
	.p2align 4
	.type	UnicodeString_charAt, @function
UnicodeString_charAt:
.LFB2960:
	.cfi_startproc
	endbr64
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L2
	movswl	%ax, %edx
	sarl	$5, %edx
.L3:
	movl	$-1, %r8d
	cmpl	%edi, %edx
	jbe	.L1
	testb	$2, %al
	jne	.L9
	movq	24(%rsi), %rsi
.L6:
	movslq	%edi, %rdi
	movzwl	(%rsi,%rdi,2), %r8d
.L1:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$10, %rsi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L2:
	movl	12(%rsi), %edx
	jmp	.L3
	.cfi_endproc
.LFE2960:
	.size	UnicodeString_charAt, .-UnicodeString_charAt
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString17getDynamicClassIDEv
	.type	_ZNK6icu_6713UnicodeString17getDynamicClassIDEv, @function
_ZNK6icu_6713UnicodeString17getDynamicClassIDEv:
.LFB2966:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713UnicodeString16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2966:
	.size	_ZNK6icu_6713UnicodeString17getDynamicClassIDEv, .-_ZNK6icu_6713UnicodeString17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Replaceable5cloneEv
	.type	_ZNK6icu_6711Replaceable5cloneEv, @function
_ZNK6icu_6711Replaceable5cloneEv:
.LFB3014:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3014:
	.size	_ZNK6icu_6711Replaceable5cloneEv, .-_ZNK6icu_6711Replaceable5cloneEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString9getLengthEv
	.type	_ZNK6icu_6713UnicodeString9getLengthEv, @function
_ZNK6icu_6713UnicodeString9getLengthEv:
.LFB3035:
	.cfi_startproc
	endbr64
	movswl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L13
	sarl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	12(%rdi), %eax
	ret
	.cfi_endproc
.LFE3035:
	.size	_ZNK6icu_6713UnicodeString9getLengthEv, .-_ZNK6icu_6713UnicodeString9getLengthEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString9getCharAtEi
	.type	_ZNK6icu_6713UnicodeString9getCharAtEi, @function
_ZNK6icu_6713UnicodeString9getCharAtEi:
.LFB3036:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L16
	movswl	%ax, %edx
	sarl	$5, %edx
.L17:
	movl	$-1, %r8d
	cmpl	%esi, %edx
	jbe	.L15
	testb	$2, %al
	jne	.L22
	movq	24(%rdi), %rdi
.L20:
	movslq	%esi, %rsi
	movzwl	(%rdi,%rsi,2), %r8d
.L15:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	addq	$10, %rdi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L16:
	movl	12(%rdi), %edx
	jmp	.L17
	.cfi_endproc
.LFE3036:
	.size	_ZNK6icu_6713UnicodeString9getCharAtEi, .-_ZNK6icu_6713UnicodeString9getCharAtEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Replaceable11hasMetaDataEv
	.type	_ZNK6icu_6711Replaceable11hasMetaDataEv, @function
_ZNK6icu_6711Replaceable11hasMetaDataEv:
.LFB3075:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3075:
	.size	_ZNK6icu_6711Replaceable11hasMetaDataEv, .-_ZNK6icu_6711Replaceable11hasMetaDataEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString11hasMetaDataEv
	.type	_ZNK6icu_6713UnicodeString11hasMetaDataEv, @function
_ZNK6icu_6713UnicodeString11hasMetaDataEv:
.LFB3076:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3076:
	.size	_ZNK6icu_6713UnicodeString11hasMetaDataEv, .-_ZNK6icu_6713UnicodeString11hasMetaDataEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UnicodeStringAppendableD2Ev
	.type	_ZN6icu_6723UnicodeStringAppendableD2Ev, @function
_ZN6icu_6723UnicodeStringAppendableD2Ev:
.LFB3085:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6710AppendableD2Ev@PLT
	.cfi_endproc
.LFE3085:
	.size	_ZN6icu_6723UnicodeStringAppendableD2Ev, .-_ZN6icu_6723UnicodeStringAppendableD2Ev
	.globl	_ZN6icu_6723UnicodeStringAppendableD1Ev
	.set	_ZN6icu_6723UnicodeStringAppendableD1Ev,_ZN6icu_6723UnicodeStringAppendableD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UnicodeStringAppendableD0Ev
	.type	_ZN6icu_6723UnicodeStringAppendableD0Ev, @function
_ZN6icu_6723UnicodeStringAppendableD0Ev:
.LFB3087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6723UnicodeStringAppendableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6710AppendableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3087:
	.size	_ZN6icu_6723UnicodeStringAppendableD0Ev, .-_ZN6icu_6723UnicodeStringAppendableD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString11getChar32AtEi
	.type	_ZNK6icu_6713UnicodeString11getChar32AtEi, @function
_ZNK6icu_6713UnicodeString11getChar32AtEi:
.LFB3037:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L29
	movswl	%dx, %eax
	sarl	$5, %eax
.L30:
	movl	$65535, %r8d
	cmpl	%eax, %esi
	jnb	.L28
	andl	$2, %edx
	jne	.L36
	movq	24(%rdi), %rdi
.L33:
	movslq	%esi, %rdx
	movzwl	(%rdi,%rdx,2), %r8d
	leaq	(%rdx,%rdx), %r9
	movl	%r8d, %ecx
	movl	%r8d, %edx
	andl	$63488, %ecx
	cmpl	$55296, %ecx
	jne	.L28
	andb	$4, %dh
	jne	.L34
	addl	$1, %esi
	cmpl	%esi, %eax
	je	.L28
	movzwl	2(%rdi,%r9), %edx
	movl	%edx, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L37
.L28:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	addq	$10, %rdi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L29:
	movl	12(%rdi), %eax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L34:
	testl	%esi, %esi
	jle	.L28
	movzwl	-2(%rdi,%r9), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L28
	sall	$10, %eax
	leal	-56613888(%r8,%rax), %r8d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L37:
	sall	$10, %r8d
	leal	-56613888(%rdx,%r8), %r8d
	jmp	.L28
	.cfi_endproc
.LFE3037:
	.size	_ZNK6icu_6713UnicodeString11getChar32AtEi, .-_ZNK6icu_6713UnicodeString11getChar32AtEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringD2Ev
	.type	_ZN6icu_6713UnicodeStringD2Ev, @function
_ZN6icu_6713UnicodeStringD2Ev:
.LFB3019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	testb	$4, 8(%rdi)
	jne	.L43
.L40:
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	jne	.L40
	movq	24(%rdi), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L40
	.cfi_endproc
.LFE3019:
	.size	_ZN6icu_6713UnicodeStringD2Ev, .-_ZN6icu_6713UnicodeStringD2Ev
	.globl	_ZN6icu_6713UnicodeStringD1Ev
	.set	_ZN6icu_6713UnicodeStringD1Ev,_ZN6icu_6713UnicodeStringD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringD0Ev
	.type	_ZN6icu_6713UnicodeStringD0Ev, @function
_ZN6icu_6713UnicodeStringD0Ev:
.LFB3021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	testb	$4, 8(%rdi)
	jne	.L49
.L46:
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	jne	.L46
	movq	24(%rdi), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L46
	.cfi_endproc
.LFE3021:
	.size	_ZN6icu_6713UnicodeStringD0Ev, .-_ZN6icu_6713UnicodeStringD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString5cloneEv
	.type	_ZNK6icu_6713UnicodeString5cloneEv, @function
_ZNK6icu_6713UnicodeString5cloneEv:
.LFB3015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L50
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %esi
	movq	%rax, (%r12)
	movw	%si, 8(%r12)
	cmpq	%rbx, %r12
	je	.L50
	movzwl	8(%rbx), %edx
	testb	$1, %dl
	jne	.L59
	movswl	%dx, %eax
	sarl	$5, %eax
	je	.L50
	movw	%dx, 8(%r12)
	movswl	8(%rbx), %ecx
	movl	%ecx, %edx
	andl	$31, %edx
	cmpw	$4, %dx
	je	.L55
	jg	.L56
	testw	%dx, %dx
	je	.L57
	cmpw	$2, %dx
	jne	.L59
	addl	%eax, %eax
	leaq	10(%r12), %rcx
	leaq	10(%rbx), %rsi
	cltq
	cmpq	$8, %rax
	jnb	.L60
	testb	$4, %al
	jne	.L94
	testq	%rax, %rax
	je	.L50
	movzbl	10(%rbx), %edx
	movb	%dl, 10(%r12)
	testb	$2, %al
	je	.L50
	movzwl	-2(%rsi,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
.L50:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movq	%r12, %rax
	movl	$0, 16(%r12)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	cmpw	$8, %dx
	jne	.L59
.L57:
	testw	%cx, %cx
	js	.L64
	movl	%ecx, %r13d
	sarl	$5, %r13d
	cmpl	$895, %ecx
	jg	.L66
.L65:
	movl	$2, %ecx
	movl	$2, %eax
	movw	%cx, 8(%r12)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L55:
	movq	24(%rbx), %rax
	lock addl	$1, -4(%rax)
	movq	24(%rbx), %rax
	cmpw	$0, 8(%r12)
	movq	%rax, 24(%r12)
	movl	16(%rbx), %eax
	movl	%eax, 16(%r12)
	jns	.L50
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L64:
	movl	12(%rbx), %r13d
	cmpl	$27, %r13d
	jle	.L65
	cmpl	$2147483637, %r13d
	jg	.L59
	.p2align 4,,10
	.p2align 3
.L66:
	leal	1(%r13), %eax
	cltq
	leaq	19(%rax,%rax), %r14
	andq	$-16, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L59
	subq	$4, %r14
	movl	$4, %edx
	movl	$1, (%rax)
	addq	$4, %rax
	shrq	%r14
	movw	%dx, 8(%r12)
	movl	%r14d, 16(%r12)
	movq	%rax, 24(%r12)
	xorl	%eax, %eax
.L68:
	leaq	10(%rbx), %rsi
	testb	$2, 8(%rbx)
	jne	.L71
	movq	24(%rbx), %rsi
.L71:
	leaq	10(%r12), %rdi
	testw	%ax, %ax
	jne	.L73
	movq	24(%r12), %rdi
.L73:
	movl	%r13d, %edx
	call	u_memcpy_67@PLT
	cmpl	$1023, %r13d
	jg	.L74
	movzwl	8(%r12), %eax
	sall	$5, %r13d
	andl	$31, %eax
	orl	%eax, %r13d
	movw	%r13w, 8(%r12)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L60:
	movq	10(%rbx), %rdx
	leaq	18(%r12), %rdi
	andq	$-8, %rdi
	movq	%rdx, 10(%r12)
	movq	-8(%rsi,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%rax, %rcx
	shrq	$3, %rcx
	rep movsq
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L74:
	orw	$-32, 8(%r12)
	movl	%r13d, 12(%r12)
	jmp	.L50
.L94:
	movl	10(%rbx), %edx
	movl	%edx, 10(%r12)
	movl	-4(%rsi,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L50
	.cfi_endproc
.LFE3015:
	.size	_ZNK6icu_6713UnicodeString5cloneEv, .-_ZNK6icu_6713UnicodeString5cloneEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UnicodeStringAppendable21reserveAppendCapacityEi
	.type	_ZN6icu_6723UnicodeStringAppendable21reserveAppendCapacityEi, @function
_ZN6icu_6723UnicodeStringAppendable21reserveAppendCapacityEi:
.LFB3091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	8(%r13), %r12d
	testw	%r12w, %r12w
	js	.L96
	movswl	%r12w, %ebx
	sarl	$5, %ebx
	addl	%esi, %ebx
	cmpl	$-1, %ebx
	je	.L149
.L98:
	xorl	%eax, %eax
	testb	$17, %r12b
	jne	.L95
	movl	%r12d, %r14d
	andl	$4, %r14d
	testb	$8, %r12b
	je	.L100
	movl	%r12d, %r8d
	andl	$2, %r8d
.L101:
	testw	%r12w, %r12w
	js	.L105
.L156:
	movswl	%r12w, %r15d
	sarl	$5, %r15d
	testw	%r8w, %r8w
	je	.L107
.L157:
	cmpl	$27, %ebx
	jle	.L150
	testl	%r15d, %r15d
	jle	.L110
	movslq	%r15d, %rdx
	leaq	10(%r13), %rsi
	leaq	-112(%rbp), %rdi
	movl	$54, %ecx
	addq	%rdx, %rdx
	movl	%r8d, -120(%rbp)
	call	__memmove_chk@PLT
	movl	-120(%rbp), %r8d
.L110:
	cmpl	$2147483637, %ebx
	jle	.L151
	movq	$0, 24(%r13)
	xorl	%ecx, %ecx
	movl	$0, 16(%r13)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	-112(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L126:
	addl	$1, %ebx
	movq	%rcx, -128(%rbp)
	movslq	%ebx, %rbx
	movl	%r8d, -120(%rbp)
	leaq	19(%rbx,%rbx), %rbx
	andq	$-16, %rbx
	movq	%rbx, %rdi
	call	uprv_malloc_67@PLT
	movl	-120(%rbp), %r8d
	movq	-128(%rbp), %rcx
	testq	%rax, %rax
	je	.L113
	subq	$4, %rbx
	leaq	4(%rax), %rdi
	movl	$4, %edx
	movl	$1, (%rax)
	shrq	%rbx
	movq	%rdi, 24(%r13)
	movl	$4, %eax
	cmpl	%ebx, %r15d
	movl	%ebx, 16(%r13)
	movw	%dx, 8(%r13)
	cmovl	%r15d, %ebx
	testq	%rcx, %rcx
	je	.L115
.L114:
	testl	%ebx, %ebx
	jg	.L152
.L118:
	sall	$5, %ebx
	orl	%eax, %ebx
	movw	%bx, 8(%r13)
	testw	%r14w, %r14w
	jne	.L153
.L122:
	movl	$1, %eax
.L95:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L154
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	testw	%r14w, %r14w
	jne	.L155
.L102:
	movl	%r12d, %r8d
	movl	$27, %edx
	andw	$2, %r8w
	jne	.L104
	movl	16(%r13), %edx
.L104:
	movl	$1, %eax
	cmpl	%edx, %ebx
	jle	.L95
	movl	%r12d, %r14d
	andl	$4, %r14d
	testw	%r12w, %r12w
	jns	.L156
.L105:
	movl	12(%r13), %r15d
	testw	%r8w, %r8w
	jne	.L157
.L107:
	movq	24(%r13), %rcx
	cmpl	$27, %ebx
	jle	.L158
	cmpl	$2147483637, %ebx
	jle	.L126
	movl	$0, 16(%r13)
.L116:
	movq	%rcx, 24(%r13)
.L117:
	movw	%r12w, 8(%r13)
	testw	%r14w, %r14w
	jne	.L159
.L124:
	movl	$1, %eax
	movq	$0, 24(%r13)
	movw	%ax, 8(%r13)
	xorl	%eax, %eax
	movl	$0, 16(%r13)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L96:
	movl	12(%r13), %ebx
	addl	%esi, %ebx
	cmpl	$-1, %ebx
	jne	.L98
.L149:
	movl	$27, %ebx
	testb	$2, %r12b
	jne	.L98
	movl	16(%r13), %ebx
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L150:
	cmpl	$27, %r15d
	movl	$27, %eax
	cmovle	%r15d, %eax
	movl	%eax, %ebx
.L109:
	movl	$2, %eax
	xorl	%ecx, %ecx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L152:
	movslq	%ebx, %rdx
	movq	%rcx, %rsi
	movq	%rcx, -120(%rbp)
	addq	%rdx, %rdx
	call	memmove@PLT
	movzwl	8(%r13), %eax
	movq	-120(%rbp), %rcx
.L115:
	cmpl	$1023, %ebx
	jle	.L160
	orl	$-32, %eax
	movl	%ebx, 12(%r13)
	movw	%ax, 8(%r13)
	testw	%r14w, %r14w
	je	.L122
.L153:
	lock subl	$1, -4(%rcx)
	jne	.L122
	leaq	-4(%rcx), %rdi
	call	uprv_free_67@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L155:
	movq	24(%r13), %rax
	movl	-4(%rax), %eax
	movzwl	8(%r13), %r12d
	cmpl	$1, %eax
	jle	.L102
	movl	%r12d, %r8d
	movl	%r12d, %r14d
	andl	$2, %r8d
	andl	$4, %r14d
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L158:
	movl	$2, %esi
	cmpl	$27, %r15d
	movl	$27, %ebx
	movw	%si, 8(%r13)
	cmovle	%r15d, %ebx
	testq	%rcx, %rcx
	je	.L109
	leaq	10(%r13), %rdi
	movl	$2, %eax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L159:
	lock subl	$1, -4(%rcx)
	jne	.L124
	movq	24(%r13), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L124
.L154:
	call	__stack_chk_fail@PLT
.L113:
	movw	$1, 8(%r13)
	movq	$0, 24(%r13)
	movl	$0, 16(%r13)
	testw	%r8w, %r8w
	je	.L116
	xorl	%ecx, %ecx
	jmp	.L117
.L160:
	andl	$31, %eax
	jmp	.L118
	.cfi_endproc
.LFE3091:
	.size	_ZN6icu_6723UnicodeStringAppendable21reserveAppendCapacityEi, .-_ZN6icu_6723UnicodeStringAppendable21reserveAppendCapacityEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ReplaceableD2Ev
	.type	_ZN6icu_6711ReplaceableD2Ev, @function
_ZN6icu_6711ReplaceableD2Ev:
.LFB2962:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2962:
	.size	_ZN6icu_6711ReplaceableD2Ev, .-_ZN6icu_6711ReplaceableD2Ev
	.globl	_ZN6icu_6711ReplaceableD1Ev
	.set	_ZN6icu_6711ReplaceableD1Ev,_ZN6icu_6711ReplaceableD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711ReplaceableD0Ev
	.type	_ZN6icu_6711ReplaceableD0Ev, @function
_ZN6icu_6711ReplaceableD0Ev:
.LFB2964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2964:
	.size	_ZN6icu_6711ReplaceableD0Ev, .-_ZN6icu_6711ReplaceableD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString16getStaticClassIDEv
	.type	_ZN6icu_6713UnicodeString16getStaticClassIDEv, @function
_ZN6icu_6713UnicodeString16getStaticClassIDEv:
.LFB2965:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6713UnicodeString16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2965:
	.size	_ZN6icu_6713UnicodeString16getStaticClassIDEv, .-_ZN6icu_6713UnicodeString16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString6addRefEv
	.type	_ZN6icu_6713UnicodeString6addRefEv, @function
_ZN6icu_6713UnicodeString6addRefEv:
.LFB2968:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	lock addl	$1, -4(%rax)
	ret
	.cfi_endproc
.LFE2968:
	.size	_ZN6icu_6713UnicodeString6addRefEv, .-_ZN6icu_6713UnicodeString6addRefEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString9removeRefEv
	.type	_ZN6icu_6713UnicodeString9removeRefEv, @function
_ZN6icu_6713UnicodeString9removeRefEv:
.LFB2969:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdx
	movl	$-1, %eax
	lock xaddl	%eax, -4(%rdx)
	subl	$1, %eax
	ret
	.cfi_endproc
.LFE2969:
	.size	_ZN6icu_6713UnicodeString9removeRefEv, .-_ZN6icu_6713UnicodeString9removeRefEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString8refCountEv
	.type	_ZNK6icu_6713UnicodeString8refCountEv, @function
_ZNK6icu_6713UnicodeString8refCountEv:
.LFB2970:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	-4(%rax), %eax
	ret
	.cfi_endproc
.LFE2970:
	.size	_ZNK6icu_6713UnicodeString8refCountEv, .-_ZNK6icu_6713UnicodeString8refCountEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString12releaseArrayEv
	.type	_ZN6icu_6713UnicodeString12releaseArrayEv, @function
_ZN6icu_6713UnicodeString12releaseArrayEv:
.LFB2971:
	.cfi_startproc
	endbr64
	testb	$4, 8(%rdi)
	jne	.L172
.L168:
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	jne	.L168
	movq	24(%rdi), %rdi
	subq	$4, %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2971:
	.size	_ZN6icu_6713UnicodeString12releaseArrayEv, .-_ZN6icu_6713UnicodeString12releaseArrayEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2Eiii
	.type	_ZN6icu_6713UnicodeStringC2Eiii, @function
_ZN6icu_6713UnicodeStringC2Eiii:
.LFB2973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movw	%r10w, 8(%rdi)
	testl	%ecx, %ecx
	jle	.L196
	movl	%edx, %r13d
	cmpl	$1114111, %edx
	ja	.L196
	movl	%ecx, %r12d
	cmpl	$65535, %edx
	jg	.L179
	cmpl	%esi, %ecx
	cmovge	%ecx, %esi
	cmpl	$27, %esi
	jle	.L216
	cmpl	$2147483637, %esi
	jg	.L178
	addl	$1, %esi
	movslq	%esi, %rsi
	leaq	19(%rsi,%rsi), %r14
	andq	$-16, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L178
	subq	$4, %r14
	leaq	4(%rax), %rcx
	movl	$4, %edi
	movl	$1, (%rax)
	shrq	%r14
	movq	%rcx, 24(%rbx)
	movl	%r14d, 16(%rbx)
	movw	%di, 8(%rbx)
.L181:
	testl	%r12d, %r12d
	movl	$1, %esi
	cmovg	%r12d, %esi
	cmpl	$7, %r12d
	jle	.L194
	movl	%esi, %edx
	movd	%r13d, %xmm0
	movq	%rcx, %rax
	shrl	$3, %edx
	punpcklwd	%xmm0, %xmm0
	salq	$4, %rdx
	pshufd	$0, %xmm0, %xmm0
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L183:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L183
	movl	%esi, %eax
	andl	$-8, %eax
	andl	$7, %esi
	je	.L184
.L182:
	movslq	%eax, %rdx
	movw	%r13w, (%rcx,%rdx,2)
	leal	1(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L184
	movslq	%edx, %rdx
	movw	%r13w, (%rcx,%rdx,2)
	leal	2(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L184
	movslq	%edx, %rdx
	movw	%r13w, (%rcx,%rdx,2)
	leal	3(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L184
	movslq	%edx, %rdx
	movw	%r13w, (%rcx,%rdx,2)
	leal	4(%rax), %edx
	cmpl	%r12d, %edx
	jge	.L184
	movslq	%edx, %rdx
	movw	%r13w, (%rcx,%rdx,2)
	leal	5(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L184
	movslq	%edx, %rdx
	addl	$6, %eax
	movw	%r13w, (%rcx,%rdx,2)
	cmpl	%eax, %r12d
	jle	.L184
	cltq
	movw	%r13w, (%rcx,%rax,2)
.L184:
	movzwl	8(%rbx), %eax
	cmpl	$1023, %r12d
	jg	.L193
	andl	$31, %eax
	sall	$5, %r12d
	orl	%r12d, %eax
	movw	%ax, 8(%rbx)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L196:
	cmpl	$27, %esi
	jle	.L212
	cmpl	$2147483637, %esi
	jle	.L217
.L178:
	movl	$1, %r9d
	movq	$0, 24(%rbx)
	movw	%r9w, 8(%rbx)
	movl	$0, 16(%rbx)
.L173:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	cmpl	$1073741823, %ecx
	jg	.L218
	addl	%r12d, %r12d
	cmpl	%r12d, %esi
	cmovl	%r12d, %esi
	cmpl	$27, %esi
	jle	.L219
	cmpl	$2147483637, %esi
	jg	.L178
	addl	$1, %esi
	movslq	%esi, %rsi
	leaq	19(%rsi,%rsi), %r14
	andq	$-16, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L178
	subq	$4, %r14
	leaq	4(%rax), %rcx
	movl	$1, (%rax)
	movl	$4, %eax
	shrq	%r14
	movq	%rcx, 24(%rbx)
	movl	%r14d, 16(%rbx)
	movw	%ax, 8(%rbx)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L212:
	movl	$2, %esi
	movw	%si, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	cmpl	$27, %esi
	jle	.L212
	cmpl	$2147483637, %esi
	jg	.L178
	leal	1(%rsi), %eax
	cltq
	leaq	19(%rax,%rax), %r12
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$2, %r8d
	leaq	10(%rdi), %rcx
	movw	%r8w, 8(%rdi)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L193:
	orl	$-32, %eax
	movl	%r12d, 12(%rbx)
	movw	%ax, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	addl	$1, %esi
	movslq	%esi, %rsi
	leaq	19(%rsi,%rsi), %r12
.L215:
	andq	$-16, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L178
	subq	$4, %r12
	movl	$1, (%rax)
	movl	$4, %ecx
	addq	$4, %rax
	shrq	%r12
	movq	%rax, 24(%rbx)
	movl	%r12d, 16(%rbx)
	movw	%cx, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movl	$2, %edx
	leaq	10(%rdi), %rcx
	movw	%dx, 8(%rdi)
.L189:
	leal	-1(%r12), %eax
	movl	%r13d, %esi
	andw	$1023, %r13w
	movl	%eax, %edi
	sarl	$10, %esi
	orw	$-9216, %r13w
	shrl	%edi
	subw	$10304, %si
	addl	$1, %edi
	cmpl	$5, %eax
	jbe	.L195
	movd	%esi, %xmm0
	movl	%edi, %edx
	movq	%rcx, %rax
	pinsrw	$1, %r13d, %xmm0
	shrl	$2, %edx
	punpckldq	%xmm0, %xmm0
	salq	$4, %rdx
	punpcklqdq	%xmm0, %xmm0
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L191:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L191
	movl	%edi, %edx
	andl	$-4, %edx
	leal	(%rdx,%rdx), %eax
	cmpl	%edi, %edx
	je	.L192
.L190:
	movslq	%eax, %rdx
	movw	%si, (%rcx,%rdx,2)
	movw	%r13w, 2(%rcx,%rdx,2)
	leal	2(%rax), %edx
	cmpl	%edx, %r12d
	jle	.L192
	movslq	%edx, %rdx
	addl	$4, %eax
	movw	%si, (%rcx,%rdx,2)
	movw	%r13w, 2(%rcx,%rdx,2)
	cmpl	%eax, %r12d
	jle	.L192
	cltq
	movw	%si, (%rcx,%rax,2)
	movw	%r13w, 2(%rcx,%rax,2)
.L192:
	movzwl	8(%rbx), %eax
	cmpl	$1023, %r12d
	jg	.L193
	andl	$31, %eax
	sall	$5, %r12d
	orl	%eax, %r12d
	movw	%r12w, 8(%rbx)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L194:
	xorl	%eax, %eax
	jmp	.L182
.L195:
	xorl	%eax, %eax
	jmp	.L190
	.cfi_endproc
.LFE2973:
	.size	_ZN6icu_6713UnicodeStringC2Eiii, .-_ZN6icu_6713UnicodeStringC2Eiii
	.globl	_ZN6icu_6713UnicodeStringC1Eiii
	.set	_ZN6icu_6713UnicodeStringC1Eiii,_ZN6icu_6713UnicodeStringC2Eiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EDs
	.type	_ZN6icu_6713UnicodeStringC2EDs, @function
_ZN6icu_6713UnicodeStringC2EDs:
.LFB2976:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%si, 10(%rdi)
	movq	%rax, (%rdi)
	movl	$34, %eax
	movw	%ax, 8(%rdi)
	ret
	.cfi_endproc
.LFE2976:
	.size	_ZN6icu_6713UnicodeStringC2EDs, .-_ZN6icu_6713UnicodeStringC2EDs
	.globl	_ZN6icu_6713UnicodeStringC1EDs
	.set	_ZN6icu_6713UnicodeStringC1EDs,_ZN6icu_6713UnicodeStringC2EDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2Ei
	.type	_ZN6icu_6713UnicodeStringC2Ei, @function
_ZN6icu_6713UnicodeStringC2Ei:
.LFB2979:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	cmpl	$65535, %esi
	ja	.L222
	movw	%si, 10(%rdi)
	movl	$34, %eax
.L223:
	movw	%ax, 8(%rdi)
.L221:
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	cmpl	$1114111, %esi
	ja	.L221
	movl	%esi, %eax
	andw	$1023, %si
	sarl	$10, %eax
	orw	$-9216, %si
	subw	$10304, %ax
	movw	%si, 12(%rdi)
	movw	%ax, 10(%rdi)
	movl	$66, %eax
	jmp	.L223
	.cfi_endproc
.LFE2979:
	.size	_ZN6icu_6713UnicodeStringC2Ei, .-_ZN6icu_6713UnicodeStringC2Ei
	.globl	_ZN6icu_6713UnicodeStringC1Ei
	.set	_ZN6icu_6713UnicodeStringC1Ei,_ZN6icu_6713UnicodeStringC2Ei
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EaNS_14ConstChar16PtrEi
	.type	_ZN6icu_6713UnicodeStringC2EaNS_14ConstChar16PtrEi, @function
_ZN6icu_6713UnicodeStringC2EaNS_14ConstChar16PtrEi:
.LFB2988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$8, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdx), %r12
	movq	%rax, (%rdi)
	movw	%r8w, 8(%rdi)
	testq	%r12, %r12
	je	.L247
	cmpl	$-1, %ecx
	jl	.L228
	jne	.L239
	testb	%sil, %sil
	jne	.L239
.L228:
	movl	$1, %eax
	movq	$0, 24(%rbx)
	movw	%ax, 8(%rbx)
	movl	$0, 16(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	testl	%ecx, %ecx
	js	.L230
	testb	%sil, %sil
	je	.L230
	movslq	%ecx, %rax
	cmpw	$0, (%r12,%rax,2)
	jne	.L228
	movl	$8, %eax
.L231:
	movl	%ecx, %edx
	addl	$1, %ecx
.L233:
	cmpl	$1023, %edx
	jg	.L234
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 8(%rbx)
.L235:
	movq	%r12, 24(%rbx)
	movl	%ecx, 16(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movl	$2, %edx
	movw	%dx, 8(%rdi)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movl	$8, %eax
	cmpl	$-1, %ecx
	je	.L248
.L232:
	movl	%ecx, %edx
	testb	%sil, %sil
	je	.L233
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L234:
	orl	$-32, %eax
	movl	%edx, 12(%rbx)
	movw	%ax, 8(%rbx)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%r12, %rdi
	movl	%esi, -20(%rbp)
	call	u_strlen_67@PLT
	movl	-20(%rbp), %esi
	movl	%eax, %ecx
	movzwl	8(%rbx), %eax
	jmp	.L232
	.cfi_endproc
.LFE2988:
	.size	_ZN6icu_6713UnicodeStringC2EaNS_14ConstChar16PtrEi, .-_ZN6icu_6713UnicodeStringC2EaNS_14ConstChar16PtrEi
	.globl	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi
	.set	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi,_ZN6icu_6713UnicodeStringC2EaNS_14ConstChar16PtrEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EPDsii
	.type	_ZN6icu_6713UnicodeStringC2EPDsii, @function
_ZN6icu_6713UnicodeStringC2EPDsii:
.LFB2991:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%r8d, %r8d
	movq	%rax, (%rdi)
	movw	%r8w, 8(%rdi)
	testq	%rsi, %rsi
	je	.L262
	cmpl	$-1, %edx
	setl	%r8b
	cmpl	%ecx, %edx
	setg	%al
	orb	%al, %r8b
	jne	.L260
	testl	%ecx, %ecx
	js	.L260
	cmpl	$-1, %edx
	je	.L263
.L254:
	cmpl	$1023, %edx
	jle	.L264
	movl	$-32, %eax
	movl	%edx, 12(%rdi)
	movw	%ax, 8(%rdi)
	movq	%rsi, 24(%rdi)
	movl	%ecx, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$1, %edx
	movq	$0, 24(%rdi)
	movw	%dx, 8(%rdi)
	movl	$0, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$2, %ecx
	movw	%cx, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	sall	$5, %edx
	movq	%rsi, 24(%rdi)
	movw	%dx, 8(%rdi)
	movl	%ecx, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	movslq	%ecx, %rax
	movq	%rsi, %rdx
	leaq	(%rsi,%rax,2), %rax
	cmpq	%rax, %rsi
	jne	.L256
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L265:
	addq	$2, %rdx
	cmpq	%rdx, %rax
	je	.L255
.L256:
	cmpw	$0, (%rdx)
	jne	.L265
.L255:
	subq	%rsi, %rdx
	sarq	%rdx
	jmp	.L254
	.cfi_endproc
.LFE2991:
	.size	_ZN6icu_6713UnicodeStringC2EPDsii, .-_ZN6icu_6713UnicodeStringC2EPDsii
	.globl	_ZN6icu_6713UnicodeStringC1EPDsii
	.set	_ZN6icu_6713UnicodeStringC1EPDsii,_ZN6icu_6713UnicodeStringC2EPDsii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EPKciNS0_10EInvariantE
	.type	_ZN6icu_6713UnicodeStringC2EPKciNS0_10EInvariantE, @function
_ZN6icu_6713UnicodeStringC2EPKciNS0_10EInvariantE:
.LFB2994:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movq	%rax, (%rdi)
	movw	%cx, 8(%rdi)
	testq	%rsi, %rsi
	je	.L281
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testl	%edx, %edx
	js	.L284
.L268:
	cmpl	$27, %r12d
	jle	.L270
	cmpl	$2147483637, %r12d
	jle	.L285
.L271:
	movl	$1, %eax
	movq	$0, 24(%rbx)
	movw	%ax, 8(%rbx)
	movl	$0, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	jne	.L268
	leaq	10(%rbx), %rsi
	movl	$-1, %edx
	movq	%r13, %rdi
	call	u_charsToUChars_67@PLT
.L273:
	movzwl	8(%rbx), %eax
	sall	$5, %r12d
	andl	$31, %eax
	orl	%eax, %r12d
	movw	%r12w, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leal	1(%r12), %eax
	cltq
	leaq	19(%rax,%rax), %rax
	andq	$-16, %rax
	movq	%rax, %rdi
	movq	%rax, %r14
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L271
	leaq	4(%rax), %rsi
	movl	$1, (%rax)
	leaq	-4(%r14), %rax
	movl	$4, %edx
	shrq	%rax
	movw	%dx, 8(%rbx)
	movq	%r13, %rdi
	movl	%r12d, %edx
	movq	%rsi, 24(%rbx)
	movl	%eax, 16(%rbx)
	call	u_charsToUChars_67@PLT
	cmpl	$1023, %r12d
	jle	.L273
	orw	$-32, 8(%rbx)
	movl	%r12d, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L270:
	.cfi_restore_state
	leaq	10(%rbx), %rsi
	movl	%r12d, %edx
	movq	%r13, %rdi
	call	u_charsToUChars_67@PLT
	jmp	.L273
	.cfi_endproc
.LFE2994:
	.size	_ZN6icu_6713UnicodeStringC2EPKciNS0_10EInvariantE, .-_ZN6icu_6713UnicodeStringC2EPKciNS0_10EInvariantE
	.globl	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE
	.set	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE,_ZN6icu_6713UnicodeStringC2EPKciNS0_10EInvariantE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EPKc
	.type	_ZN6icu_6713UnicodeStringC2EPKc, @function
_ZN6icu_6713UnicodeStringC2EPKc:
.LFB2997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	$2, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r13w, 8(%rdi)
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L286
	movq	%rdi, %rbx
	leaq	-128(%rbp), %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movzwl	8(%rbx), %r9d
	movq	-128(%rbp), %r13
	movl	-120(%rbp), %r8d
	testb	$1, %r9b
	je	.L385
	movl	$2, %r12d
	cmpl	$27, %r8d
	movl	$27, %eax
	movl	$2, %r9d
	movw	%r12w, 8(%rbx)
	leal	1(%r8), %r12d
	cmovle	%eax, %r12d
.L297:
	movl	%r9d, %r11d
	andw	$2, %r11w
	jne	.L299
	cmpl	%r12d, 16(%rbx)
	jge	.L300
.L344:
	movl	%r9d, %r14d
	andl	$4, %r14d
.L296:
	testw	%r9w, %r9w
	js	.L301
	movswl	%r9w, %r10d
	sarl	$5, %r10d
.L302:
	testw	%r11w, %r11w
	je	.L303
	cmpl	$27, %r12d
	jne	.L304
	cmpl	$27, %r10d
	cmovle	%r10d, %r12d
.L305:
	movl	$2, %eax
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L314:
	sall	$5, %r12d
	orl	%r12d, %eax
	movw	%ax, 8(%rbx)
.L316:
	testw	%r14w, %r14w
	jne	.L386
.L317:
	movl	%eax, %edx
	andl	$31, %edx
	orl	$16, %edx
	movw	%dx, 8(%rbx)
	testb	$2, %al
	je	.L322
.L343:
	movl	$0, -132(%rbp)
	leaq	10(%rbx), %rdi
.L383:
	movl	$27, %esi
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L303:
	movq	24(%rbx), %r15
	cmpl	$27, %r12d
	je	.L387
	cmpl	$2147483637, %r12d
	jle	.L340
	movl	$0, 16(%rbx)
.L312:
	movq	%r15, 24(%rbx)
.L313:
	movw	%r9w, 8(%rbx)
	testw	%r14w, %r14w
	jne	.L388
.L320:
	movl	$1, %r9d
	xorl	%edi, %edi
	movq	$0, 24(%rbx)
	movw	%r9w, 8(%rbx)
	movl	$0, 16(%rbx)
	movl	$0, -132(%rbp)
	.p2align 4,,10
	.p2align 3
.L293:
	movl	16(%rbx), %esi
.L294:
	leaq	-132(%rbp), %rax
	movq	%r13, %rcx
	leaq	-136(%rbp), %rdx
	movl	$65533, %r9d
	pushq	%rax
	pushq	$0
	call	u_strFromUTF8WithSub_67@PLT
	movzwl	8(%rbx), %ecx
	popq	%rsi
	movl	-136(%rbp), %eax
	popq	%rdi
	testb	$16, %cl
	je	.L323
	cmpl	$-1, %eax
	jl	.L323
	testb	$2, %cl
	jne	.L324
	movslq	16(%rbx), %rdx
	cmpl	$-1, %eax
	je	.L389
.L326:
	cmpl	%edx, %eax
	cmovg	%edx, %eax
.L332:
	cmpl	$1023, %eax
	jg	.L333
.L328:
	andl	$31, %ecx
	sall	$5, %eax
	orl	%eax, %ecx
.L334:
	andl	$-17, %ecx
	movw	%cx, 8(%rbx)
.L323:
	movl	-132(%rbp), %edx
	testl	%edx, %edx
	jle	.L286
	andl	$4, %ecx
	je	.L337
	movq	24(%rbx), %rax
	lock subl	$1, -4(%rax)
	jne	.L337
	movq	24(%rbx), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L337:
	movl	$1, %eax
	movq	$0, 24(%rbx)
	movw	%ax, 8(%rbx)
	movl	$0, 16(%rbx)
.L286:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L390
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	leal	1(%r8), %r12d
	cmpl	$27, %r8d
	movl	$27, %eax
	cmovle	%eax, %r12d
	testb	$17, %r9b
	je	.L391
	movl	$0, -132(%rbp)
	xorl	%edi, %edi
	andl	$2, %r9d
	je	.L293
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L300:
	movl	%r9d, %eax
	andl	$31, %eax
	orl	$16, %eax
	movw	%ax, 8(%rbx)
.L322:
	movl	$0, -132(%rbp)
	movq	24(%rbx), %rdi
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L391:
	movl	%r9d, %r14d
	andl	$4, %r14d
	testb	$8, %r9b
	je	.L295
	movl	%r9d, %r11d
	andl	$2, %r11d
	jmp	.L296
.L295:
	testw	%r14w, %r14w
	je	.L297
	movq	24(%rbx), %rax
	movl	-4(%rax), %eax
	movzwl	8(%rbx), %r9d
	cmpl	$1, %eax
	jle	.L297
	movl	%r9d, %r11d
	movl	%r9d, %r14d
	andl	$2, %r11d
	andl	$4, %r14d
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L301:
	movl	12(%rbx), %r10d
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L333:
	movl	%eax, 12(%rbx)
	orl	$-32, %ecx
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L324:
	movl	$27, %edx
	cmpl	$-1, %eax
	jne	.L326
	leaq	10(%rbx), %rsi
	movl	$54, %edx
.L327:
	addq	%rsi, %rdx
	cmpq	%rdx, %rsi
	jnb	.L351
	movq	%rsi, %rax
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L329:
	addq	$2, %rax
	cmpq	%rax, %rdx
	jbe	.L384
.L331:
	cmpw	$0, (%rax)
	jne	.L329
.L384:
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	%rdx
	movl	%edx, %eax
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L304:
	testl	%r10d, %r10d
	jg	.L392
.L306:
	cmpl	$2147483637, %r12d
	jle	.L393
	movq	$0, 24(%rbx)
	movl	$0, 16(%rbx)
.L382:
	xorl	%r15d, %r15d
	jmp	.L313
.L393:
	leaq	-112(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L340:
	addl	$1, %r12d
	movl	%r8d, -160(%rbp)
	movslq	%r12d, %r12
	movl	%r11d, -156(%rbp)
	leaq	19(%r12,%r12), %r12
	movl	%r9d, -152(%rbp)
	andq	$-16, %r12
	movl	%r10d, -148(%rbp)
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movl	-152(%rbp), %r9d
	movl	-156(%rbp), %r11d
	testq	%rax, %rax
	movl	-160(%rbp), %r8d
	je	.L309
	movl	$4, %r10d
	subq	$4, %r12
	leaq	4(%rax), %rdi
	movl	$1, (%rax)
	movw	%r10w, 8(%rbx)
	movl	-148(%rbp), %r10d
	shrq	%r12
	movl	$4, %eax
	movl	%r12d, 16(%rbx)
	cmpl	%r12d, %r10d
	movq	%rdi, 24(%rbx)
	cmovl	%r10d, %r12d
	testq	%r15, %r15
	je	.L311
.L310:
	testl	%r12d, %r12d
	jle	.L314
	movslq	%r12d, %rdx
	movq	%r15, %rsi
	movl	%r8d, -148(%rbp)
	addq	%rdx, %rdx
	call	memmove@PLT
	movzwl	8(%rbx), %eax
	movl	-148(%rbp), %r8d
.L311:
	cmpl	$1023, %r12d
	jle	.L394
	orl	$-32, %eax
	movl	%r12d, 12(%rbx)
	movw	%ax, 8(%rbx)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L299:
	cmpl	$27, %r12d
	jne	.L344
	movl	%r9d, %eax
	andl	$31, %eax
	orl	$16, %eax
	movw	%ax, 8(%rbx)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L392:
	movslq	%r10d, %rdx
	leaq	10(%rbx), %rsi
	leaq	-112(%rbp), %rdi
	movl	$54, %ecx
	addq	%rdx, %rdx
	movl	%r8d, -160(%rbp)
	movl	%r11d, -156(%rbp)
	movl	%r9d, -152(%rbp)
	movl	%r10d, -148(%rbp)
	call	__memmove_chk@PLT
	movl	-148(%rbp), %r10d
	movl	-152(%rbp), %r9d
	movl	-156(%rbp), %r11d
	movl	-160(%rbp), %r8d
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L386:
	lock subl	$1, -4(%r15)
	je	.L318
	movzwl	8(%rbx), %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L387:
	movl	$2, %r11d
	cmpl	$27, %r10d
	movw	%r11w, 8(%rbx)
	cmovle	%r10d, %r12d
	testq	%r15, %r15
	je	.L305
	leaq	10(%rbx), %rdi
	movl	$2, %eax
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L389:
	movq	24(%rbx), %rsi
	addq	%rdx, %rdx
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L388:
	lock subl	$1, -4(%r15)
	jne	.L320
	movq	24(%rbx), %rax
	movl	%r8d, -148(%rbp)
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	movl	-148(%rbp), %r8d
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L318:
	leaq	-4(%r15), %rdi
	movl	%r8d, -148(%rbp)
	call	uprv_free_67@PLT
	movzwl	8(%rbx), %eax
	movl	-148(%rbp), %r8d
	jmp	.L317
.L351:
	xorl	%eax, %eax
	jmp	.L328
.L390:
	call	__stack_chk_fail@PLT
.L309:
	movw	$1, 8(%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 16(%rbx)
	testw	%r11w, %r11w
	jne	.L382
	jmp	.L312
.L394:
	andl	$31, %eax
	jmp	.L314
	.cfi_endproc
.LFE2997:
	.size	_ZN6icu_6713UnicodeStringC2EPKc, .-_ZN6icu_6713UnicodeStringC2EPKc
	.globl	_ZN6icu_6713UnicodeStringC1EPKc
	.set	_ZN6icu_6713UnicodeStringC1EPKc,_ZN6icu_6713UnicodeStringC2EPKc
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2ERKS0_
	.type	_ZN6icu_6713UnicodeStringC2ERKS0_, @function
_ZN6icu_6713UnicodeStringC2ERKS0_:
.LFB3003:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %r8d
	movq	%rax, (%rdi)
	movw	%r8w, 8(%rdi)
	cmpq	%rsi, %rdi
	je	.L439
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzwl	8(%rsi), %eax
	testb	$1, %al
	jne	.L404
	movswl	%ax, %edx
	sarl	$5, %edx
	movl	%edx, %r12d
	je	.L395
	movl	%eax, %edx
	movw	%ax, 8(%rdi)
	andl	$31, %edx
	cmpw	$4, %dx
	je	.L400
	jg	.L401
	testw	%dx, %dx
	je	.L402
	cmpw	$2, %dx
	jne	.L404
	leal	(%r12,%r12), %edx
	leaq	10(%rdi), %rcx
	movslq	%edx, %rdx
	leaq	10(%rsi), %rax
	cmpq	$8, %rdx
	jnb	.L405
	testb	$4, %dl
	jne	.L442
	testq	%rdx, %rdx
	je	.L395
	movzbl	10(%rsi), %esi
	movb	%sil, 10(%rdi)
	testb	$2, %dl
	je	.L395
	movzwl	-2(%rax,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
.L395:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	movl	$1, %eax
	movq	$0, 24(%rbx)
	movw	%ax, 8(%rbx)
	movl	$0, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	cmpw	$8, %dx
	jne	.L404
.L402:
	testw	%ax, %ax
	js	.L443
	cmpl	$27, %r12d
	jg	.L419
.L418:
	movl	$2, %ecx
	movl	$2, %edx
	movw	%cx, 8(%rbx)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	24(%rsi), %rax
	lock addl	$1, -4(%rax)
	movq	24(%rsi), %rax
	cmpw	$0, 8(%rdi)
	movq	%rax, 24(%rdi)
	movl	16(%rsi), %eax
	movl	%eax, 16(%rdi)
	jns	.L395
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L443:
	movl	12(%rsi), %r12d
	cmpl	$27, %r12d
	jle	.L418
	cmpl	$2147483637, %r12d
	jg	.L404
	.p2align 4,,10
	.p2align 3
.L419:
	leal	1(%r12), %eax
	movq	%rsi, -40(%rbp)
	cltq
	leaq	19(%rax,%rax), %r13
	andq	$-16, %r13
	movq	%r13, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L404
	subq	$4, %r13
	movq	-40(%rbp), %rsi
	movl	$1, (%rax)
	addq	$4, %rax
	shrq	%r13
	movl	$4, %edx
	movq	%rax, 24(%rbx)
	movl	%r13d, 16(%rbx)
	movzwl	8(%rsi), %eax
	movw	%dx, 8(%rbx)
	xorl	%edx, %edx
.L411:
	testb	$2, %al
	je	.L413
	addq	$10, %rsi
.L414:
	leaq	10(%rbx), %rdi
	testw	%dx, %dx
	jne	.L416
	movq	24(%rbx), %rdi
.L416:
	movl	%r12d, %edx
	call	u_memcpy_67@PLT
	cmpl	$1023, %r12d
	jg	.L417
	movzwl	8(%rbx), %eax
	movl	%r12d, %edx
	sall	$5, %edx
	andl	$31, %eax
	orl	%eax, %edx
	movw	%dx, 8(%rbx)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L405:
	movq	10(%rsi), %rsi
	leaq	18(%rdi), %rdi
	movq	%rsi, -8(%rdi)
	movq	-8(%rax,%rdx), %rsi
	andq	$-8, %rdi
	movq	%rsi, -8(%rcx,%rdx)
	subq	%rdi, %rcx
	subq	%rcx, %rax
	addq	%rdx, %rcx
	movq	%rax, %rsi
	shrq	$3, %rcx
	rep movsq
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L413:
	movq	24(%rsi), %rsi
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L417:
	orw	$-32, 8(%rbx)
	movl	%r12d, 12(%rbx)
	jmp	.L395
.L442:
	movl	10(%rsi), %esi
	movl	%esi, 10(%rdi)
	movl	-4(%rax,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L395
	.cfi_endproc
.LFE3003:
	.size	_ZN6icu_6713UnicodeStringC2ERKS0_, .-_ZN6icu_6713UnicodeStringC2ERKS0_
	.globl	_ZN6icu_6713UnicodeStringC1ERKS0_
	.set	_ZN6icu_6713UnicodeStringC1ERKS0_,_ZN6icu_6713UnicodeStringC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EOS0_
	.type	_ZN6icu_6713UnicodeStringC2EOS0_, @function
_ZN6icu_6713UnicodeStringC2EOS0_:
.LFB3006:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, (%rdi)
	movswl	8(%rsi), %eax
	movw	%ax, 8(%rdi)
	testb	$2, %al
	je	.L445
	cmpq	%rsi, %rdi
	je	.L444
	sarl	$5, %eax
	leaq	10(%rdi), %rcx
	leaq	10(%rsi), %rdx
	addl	%eax, %eax
	cltq
	cmpq	$8, %rax
	jnb	.L447
	testb	$4, %al
	jne	.L458
	testq	%rax, %rax
	je	.L444
	movzbl	10(%rsi), %esi
	movb	%sil, 10(%rdi)
	testb	$2, %al
	jne	.L459
.L444:
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	movq	24(%rsi), %rdx
	movq	%rdx, 24(%rdi)
	movl	16(%rsi), %edx
	movl	%edx, 16(%rdi)
	testw	%ax, %ax
	js	.L460
.L451:
	movl	$1, %eax
	movq	$0, 24(%rsi)
	movw	%ax, 8(%rsi)
	movl	$0, 16(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L447:
	movq	10(%rsi), %rsi
	addq	$18, %rdi
	movq	%rsi, -8(%rdi)
	movq	-8(%rdx,%rax), %rsi
	andq	$-8, %rdi
	movq	%rsi, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rdx
	addq	%rax, %rcx
	movq	%rdx, %rsi
	shrq	$3, %rcx
	rep movsq
	ret
.L459:
	movzwl	-2(%rdx,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	movl	10(%rsi), %esi
	movl	%esi, 10(%rdi)
	movl	-4(%rdx,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	ret
	.cfi_endproc
.LFE3006:
	.size	_ZN6icu_6713UnicodeStringC2EOS0_, .-_ZN6icu_6713UnicodeStringC2EOS0_
	.globl	_ZN6icu_6713UnicodeStringC1EOS0_
	.set	_ZN6icu_6713UnicodeStringC1EOS0_,_ZN6icu_6713UnicodeStringC2EOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString8allocateEi
	.type	_ZN6icu_6713UnicodeString8allocateEi, @function
_ZN6icu_6713UnicodeString8allocateEi:
.LFB3017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$27, %esi
	jle	.L469
	cmpl	$2147483637, %esi
	jle	.L470
.L464:
	movl	$1, %eax
	movq	$0, 24(%rbx)
	movw	%ax, 8(%rbx)
	xorl	%eax, %eax
	movl	$0, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	addl	$1, %esi
	movslq	%esi, %rsi
	leaq	19(%rsi,%rsi), %r12
	andq	$-16, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L464
	subq	$4, %r12
	movl	$1, (%rax)
	movl	$4, %edx
	addq	$4, %rax
	shrq	%r12
	movq	%rax, 24(%rbx)
	movl	$1, %eax
	movl	%r12d, 16(%rbx)
	movw	%dx, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movl	$2, %ecx
	movl	$1, %eax
	movw	%cx, 8(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3017:
	.size	_ZN6icu_6713UnicodeString8allocateEi, .-_ZN6icu_6713UnicodeString8allocateEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE
	.type	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE, @function
_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE:
.LFB3022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	$2, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%bx, 8(%rdi)
	movq	%rax, (%rdi)
	cmpl	$27, %edx
	jle	.L472
	cmpl	$2147483636, %edx
	jle	.L507
.L473:
	movl	$1, %r10d
	movl	$0, -28(%rbp)
	xorl	%edi, %edi
	movw	%r10w, 8(%r12)
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
.L474:
	movl	16(%r12), %esi
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L472:
	movl	$0, -28(%rbp)
	movl	$18, %r9d
	leaq	10(%rdi), %rdi
	movl	$27, %esi
	movw	%r9w, -2(%rdi)
.L475:
	leaq	-28(%rbp), %rax
	leaq	-32(%rbp), %rdx
	movl	$65533, %r9d
	pushq	%rax
	pushq	$0
	call	u_strFromUTF8WithSub_67@PLT
	movzwl	8(%r12), %ecx
	popq	%rsi
	movl	-32(%rbp), %eax
	popq	%rdi
	testb	$16, %cl
	je	.L476
	cmpl	$-1, %eax
	jge	.L508
.L476:
	movl	-28(%rbp), %edx
	testl	%edx, %edx
	jg	.L509
.L471:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L510
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	testb	$2, %cl
	jne	.L477
	movslq	16(%r12), %rdx
	cmpl	$-1, %eax
	je	.L511
.L479:
	cmpl	%edx, %eax
	cmovg	%edx, %eax
.L485:
	cmpl	$1023, %eax
	jg	.L486
.L481:
	andl	$31, %ecx
	sall	$5, %eax
	orl	%eax, %ecx
.L487:
	movl	-28(%rbp), %edx
	andl	$-17, %ecx
	movw	%cx, 8(%r12)
	testl	%edx, %edx
	jle	.L471
.L509:
	andl	$4, %ecx
	jne	.L512
.L490:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movl	$0, 16(%r12)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L507:
	leal	2(%rdx), %eax
	movq	%rsi, -48(%rbp)
	cltq
	movq	%rdx, -40(%rbp)
	leaq	19(%rax,%rax), %rax
	andq	$-16, %rax
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	uprv_malloc_67@PLT
	movq	-40(%rbp), %r8
	movq	-48(%rbp), %rcx
	testq	%rax, %rax
	je	.L473
	leaq	4(%rax), %rdi
	movl	$1, (%rax)
	leaq	-4(%rbx), %rax
	movl	$20, %r11d
	shrq	%rax
	movq	%rdi, 24(%r12)
	movl	%eax, 16(%r12)
	movw	%r11w, 8(%r12)
	movl	$0, -28(%rbp)
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L477:
	movl	$27, %edx
	cmpl	$-1, %eax
	jne	.L479
	leaq	10(%r12), %rsi
	movl	$54, %edx
.L480:
	addq	%rsi, %rdx
	cmpq	%rdx, %rsi
	jnb	.L494
	movq	%rsi, %rax
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L482:
	addq	$2, %rax
	cmpq	%rax, %rdx
	jbe	.L506
.L484:
	cmpw	$0, (%rax)
	jne	.L482
.L506:
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	%rdx
	movl	%edx, %eax
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L486:
	movl	%eax, 12(%r12)
	orl	$-32, %ecx
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L512:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	jne	.L490
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L511:
	movq	24(%r12), %rsi
	addq	%rdx, %rdx
	jmp	.L480
.L494:
	xorl	%eax, %eax
	jmp	.L481
.L510:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3022:
	.size	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE, .-_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString9fromUTF32EPKii
	.type	_ZN6icu_6713UnicodeString9fromUTF32EPKii, @function
_ZN6icu_6713UnicodeString9fromUTF32EPKii:
.LFB3023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r10d
	movl	$27, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r10w, 8(%rdi)
	movq	%rax, (%rdi)
	cmpl	$27, %edx
	jle	.L514
	movl	%edx, %eax
	sarl	$4, %eax
	leal	4(%rdx,%rax), %r9d
.L514:
	leaq	-116(%rbp), %rax
	movl	$2, %ebx
	leaq	-120(%rbp), %r15
	movq	%rax, -136(%rbp)
.L562:
	cmpl	$-1, %r9d
	jl	.L607
	je	.L609
.L517:
	testb	$17, %bl
	jne	.L607
	movl	%ebx, %r11d
	andl	$4, %r11d
	testb	$8, %bl
	je	.L519
	movl	%ebx, %r8d
	andl	$2, %r8d
.L520:
	testw	%bx, %bx
	js	.L525
	movswl	%bx, %r10d
	sarl	$5, %r10d
.L526:
	testw	%r8w, %r8w
	je	.L527
	cmpl	$27, %r9d
	jle	.L610
	testl	%r10d, %r10d
	jle	.L530
	movslq	%r10d, %rdx
	leaq	10(%r12), %rsi
	leaq	-112(%rbp), %rdi
	movl	$54, %ecx
	addq	%rdx, %rdx
	movl	%r11d, -156(%rbp)
	movl	%r8d, -152(%rbp)
	movl	%r9d, -148(%rbp)
	movl	%r10d, -144(%rbp)
	call	__memmove_chk@PLT
	movl	-144(%rbp), %r10d
	movl	-148(%rbp), %r9d
	movl	-152(%rbp), %r8d
	movl	-156(%rbp), %r11d
.L530:
	cmpl	$2147483637, %r9d
	jle	.L611
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
.L605:
	xorl	%ecx, %ecx
.L537:
	movw	%bx, 8(%r12)
	testw	%r11w, %r11w
	jne	.L612
.L544:
	movl	$1, %esi
	movl	$0, -116(%rbp)
	xorl	%edi, %edi
	movw	%si, 8(%r12)
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L607:
	andl	$2, %ebx
	movl	$0, -116(%rbp)
	xorl	%edi, %edi
	testw	%bx, %bx
	jne	.L578
.L546:
	movl	16(%r12), %esi
.L548:
	pushq	-136(%rbp)
	movq	%r13, %rcx
	movq	%r15, %rdx
	movl	%r14d, %r8d
	pushq	$0
	movl	$65533, %r9d
	call	u_strFromUTF32WithSub_67@PLT
	movzwl	8(%r12), %ebx
	popq	%rdx
	movl	-120(%rbp), %r9d
	popq	%rcx
	testb	$16, %bl
	je	.L549
	cmpl	$-1, %r9d
	jge	.L613
.L549:
	movl	-116(%rbp), %eax
	cmpl	$15, %eax
	je	.L614
.L561:
	testl	%eax, %eax
	jg	.L615
.L513:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L616
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L613:
	.cfi_restore_state
	testb	$2, %bl
	jne	.L550
	movslq	16(%r12), %rax
	cmpl	$-1, %r9d
	je	.L617
.L552:
	cmpl	%eax, %r9d
	cmovle	%r9d, %eax
.L558:
	cmpl	$1023, %eax
	jle	.L554
	movl	%eax, 12(%r12)
	orl	$-32, %ebx
.L560:
	movl	-116(%rbp), %eax
	andl	$-17, %ebx
	movw	%bx, 8(%r12)
	cmpl	$15, %eax
	jne	.L561
.L614:
	addl	$1, %r9d
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L519:
	testw	%r11w, %r11w
	jne	.L618
.L521:
	movl	%ebx, %r8d
	andw	$2, %r8w
	jne	.L523
	cmpl	%r9d, 16(%r12)
	jl	.L572
	andl	$31, %ebx
	orl	$16, %ebx
	movw	%bx, 8(%r12)
.L547:
	movl	$0, -116(%rbp)
	movq	24(%r12), %rdi
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L615:
	andl	$4, %ebx
	jne	.L619
.L565:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movl	$0, 16(%r12)
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L609:
	movl	$27, %r9d
	testb	$2, %bl
	jne	.L517
	movl	16(%r12), %r9d
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L527:
	movq	24(%r12), %rcx
	cmpl	$27, %r9d
	jle	.L620
	cmpl	$2147483637, %r9d
	jle	.L568
	movl	$0, 16(%r12)
.L536:
	movq	%rcx, 24(%r12)
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L525:
	movl	12(%r12), %r10d
	jmp	.L526
.L579:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L554:
	andl	$31, %ebx
	sall	$5, %eax
	orl	%eax, %ebx
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L610:
	cmpl	$27, %r10d
	movl	$27, %ebx
	cmovle	%r10d, %ebx
.L529:
	movl	$2, %eax
	xorl	%ecx, %ecx
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L611:
	leaq	-112(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L568:
	addl	$1, %r9d
	movq	%rcx, -168(%rbp)
	movslq	%r9d, %r9
	movl	%r11d, -156(%rbp)
	leaq	19(%r9,%r9), %rdx
	movl	%r8d, -152(%rbp)
	andq	$-16, %rdx
	movl	%r10d, -148(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -144(%rbp)
	call	uprv_malloc_67@PLT
	movl	-152(%rbp), %r8d
	movl	-156(%rbp), %r11d
	testq	%rax, %rax
	movq	-168(%rbp), %rcx
	je	.L533
	leaq	4(%rax), %rdi
	movq	-144(%rbp), %rdx
	movl	-148(%rbp), %r10d
	movl	$4, %r8d
	movl	$1, (%rax)
	movl	$4, %eax
	leaq	-4(%rdx), %rbx
	movq	%rdi, 24(%r12)
	shrq	%rbx
	movw	%r8w, 8(%r12)
	cmpl	%ebx, %r10d
	movl	%ebx, 16(%r12)
	cmovl	%r10d, %ebx
	movl	$4, %r10d
	testq	%rcx, %rcx
	je	.L535
.L534:
	testl	%ebx, %ebx
	jg	.L621
.L538:
	sall	$5, %ebx
	movl	%ebx, %r10d
	orl	%eax, %r10d
	movw	%r10w, 8(%r12)
	testw	%r11w, %r11w
	jne	.L622
.L541:
	movl	%r10d, %eax
	andl	$31, %eax
	orl	$16, %eax
	andl	$2, %r10d
	movw	%ax, 8(%r12)
	je	.L547
.L571:
	movl	$0, -116(%rbp)
	leaq	10(%r12), %rdi
	movl	$27, %esi
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L523:
	cmpl	$27, %r9d
	jle	.L623
.L572:
	movl	%ebx, %r11d
	andl	$4, %r11d
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L617:
	movq	24(%r12), %rcx
	addq	%rax, %rax
.L553:
	leaq	(%rcx,%rax), %rdx
	cmpq	%rdx, %rcx
	jnb	.L579
	movq	%rcx, %rax
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L555:
	addq	$2, %rax
	cmpq	%rax, %rdx
	jbe	.L608
.L557:
	cmpw	$0, (%rax)
	jne	.L555
.L608:
	subq	%rcx, %rax
	sarq	%rax
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L621:
	movslq	%ebx, %rdx
	movq	%rcx, %rsi
	movl	%r11d, -148(%rbp)
	addq	%rdx, %rdx
	movq	%rcx, -144(%rbp)
	call	memmove@PLT
	movzwl	8(%r12), %r10d
	movl	-148(%rbp), %r11d
	movq	-144(%rbp), %rcx
.L535:
	cmpl	$1023, %ebx
	jle	.L624
	orl	$-32, %r10d
	movl	%ebx, 12(%r12)
	movw	%r10w, 8(%r12)
	testw	%r11w, %r11w
	je	.L541
.L622:
	lock subl	$1, -4(%rcx)
	jne	.L606
	leaq	-4(%rcx), %rdi
	call	uprv_free_67@PLT
.L606:
	movzwl	8(%r12), %r10d
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L619:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	jne	.L565
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L550:
	movl	$27, %eax
	cmpl	$-1, %r9d
	jne	.L552
	leaq	10(%r12), %rcx
	movl	$54, %eax
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L618:
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	movzwl	8(%r12), %ebx
	cmpl	$1, %eax
	jle	.L521
	movl	%ebx, %r8d
	movl	%ebx, %r11d
	andl	$2, %r8d
	andl	$4, %r11d
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L620:
	movl	$2, %r9d
	cmpl	$27, %r10d
	movl	$27, %ebx
	movw	%r9w, 8(%r12)
	cmovle	%r10d, %ebx
	testq	%rcx, %rcx
	je	.L529
	leaq	10(%r12), %rdi
	movl	$2, %eax
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L612:
	lock subl	$1, -4(%rcx)
	jne	.L544
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L578:
	movl	$27, %esi
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L623:
	andl	$31, %ebx
	orl	$16, %ebx
	movw	%bx, 8(%r12)
	jmp	.L571
.L616:
	call	__stack_chk_fail@PLT
.L624:
	movl	%r10d, %eax
	andl	$31, %eax
	jmp	.L538
.L533:
	movw	$1, 8(%r12)
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
	testw	%r8w, %r8w
	jne	.L605
	jmp	.L536
	.cfi_endproc
.LFE3023:
	.size	_ZN6icu_6713UnicodeString9fromUTF32EPKii, .-_ZN6icu_6713UnicodeString9fromUTF32EPKii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringaSERKS0_
	.type	_ZN6icu_6713UnicodeStringaSERKS0_, @function
_ZN6icu_6713UnicodeStringaSERKS0_:
.LFB3024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	cmpq	%rsi, %rdi
	je	.L662
	movzwl	8(%rdi), %eax
	movzwl	8(%rsi), %edx
	andl	$4, %eax
	testb	$1, %dl
	jne	.L677
	testw	%ax, %ax
	jne	.L678
	movswl	%dx, %eax
	sarl	$5, %eax
	movl	%eax, %r13d
	je	.L679
.L634:
	movl	%edx, %eax
	movw	%dx, 8(%r12)
	andl	$31, %eax
	cmpw	$4, %ax
	je	.L635
	jg	.L636
	testw	%ax, %ax
	je	.L637
	cmpw	$2, %ax
	jne	.L639
	leal	(%r13,%r13), %eax
	leaq	10(%r12), %rcx
	cltq
	leaq	10(%rsi), %rdx
	cmpq	$8, %rax
	jnb	.L640
	testb	$4, %al
	jne	.L680
	testq	%rax, %rax
	je	.L662
	movzbl	10(%rsi), %esi
	movb	%sil, 10(%r12)
	testb	$2, %al
	je	.L662
	movzwl	-2(%rdx,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
.L662:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	.cfi_restore_state
	testw	%ax, %ax
	je	.L639
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	jne	.L639
	movq	24(%rdi), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
.L639:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movq	%r12, %rax
	movl	$0, 16(%r12)
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	cmpw	$8, %ax
	jne	.L639
.L637:
	testw	%dx, %dx
	js	.L681
	cmpl	$27, %r13d
	jg	.L655
.L654:
	movl	$2, %ecx
	movl	$2, %eax
	movw	%cx, 8(%r12)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L679:
	movl	$2, %esi
	movq	%r12, %rax
	movw	%si, 8(%r12)
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L678:
	.cfi_restore_state
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	je	.L633
.L676:
	movzwl	8(%rsi), %edx
	movswl	%dx, %eax
	sarl	$5, %eax
	movl	%eax, %r13d
	jne	.L634
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L635:
	movq	24(%rsi), %rax
	lock addl	$1, -4(%rax)
	movq	24(%rsi), %rax
	cmpw	$0, 8(%r12)
	movq	%rax, 24(%r12)
	movl	16(%rsi), %eax
	movl	%eax, 16(%r12)
	jns	.L662
	movl	12(%rsi), %eax
	movl	%eax, 12(%r12)
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L681:
	movl	12(%rsi), %r13d
	cmpl	$27, %r13d
	jle	.L654
	cmpl	$2147483637, %r13d
	jg	.L639
	.p2align 4,,10
	.p2align 3
.L655:
	leal	1(%r13), %eax
	movq	%rsi, -40(%rbp)
	cltq
	leaq	19(%rax,%rax), %r14
	andq	$-16, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L639
	subq	$4, %r14
	movl	$4, %edx
	addq	$4, %rax
	movq	-40(%rbp), %rsi
	shrq	%r14
	movw	%dx, 8(%r12)
	movl	%r14d, 16(%r12)
	movl	$1, -4(%rax)
	movq	%rax, 24(%r12)
	xorl	%eax, %eax
.L647:
	testb	$2, 8(%rsi)
	je	.L649
	addq	$10, %rsi
.L650:
	leaq	10(%r12), %rdi
	testw	%ax, %ax
	jne	.L652
	movq	24(%r12), %rdi
.L652:
	movl	%r13d, %edx
	call	u_memcpy_67@PLT
	cmpl	$1023, %r13d
	jg	.L653
	movzwl	8(%r12), %edx
	movl	%r13d, %eax
	sall	$5, %eax
	andl	$31, %edx
	orl	%edx, %eax
	movw	%ax, 8(%r12)
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L640:
	movq	10(%rsi), %rsi
	leaq	18(%r12), %rdi
	andq	$-8, %rdi
	movq	%rsi, 10(%r12)
	movq	-8(%rdx,%rax), %rsi
	movq	%rsi, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rdx
	addq	%rax, %rcx
	movq	%rdx, %rsi
	shrq	$3, %rcx
	rep movsq
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L633:
	movq	24(%rdi), %rax
	movq	%rsi, -40(%rbp)
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	movq	-40(%rbp), %rsi
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L649:
	movq	24(%rsi), %rsi
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L653:
	orw	$-32, 8(%r12)
	movl	%r13d, 12(%r12)
	jmp	.L662
.L680:
	movl	10(%rsi), %esi
	movl	%esi, 10(%r12)
	movl	-4(%rdx,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L662
	.cfi_endproc
.LFE3024:
	.size	_ZN6icu_6713UnicodeStringaSERKS0_, .-_ZN6icu_6713UnicodeStringaSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_
	.type	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_, @function
_ZN6icu_6713UnicodeString12fastCopyFromERKS0_:
.LFB3025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	cmpq	%rsi, %rdi
	je	.L718
	movzwl	8(%rdi), %eax
	movzwl	8(%rsi), %edx
	andl	$4, %eax
	testb	$1, %dl
	jne	.L730
	testw	%ax, %ax
	jne	.L731
	movswl	%dx, %eax
	sarl	$5, %eax
	movl	%eax, %r14d
	je	.L732
.L691:
	movl	%edx, %r13d
	movw	%dx, 8(%r12)
	andl	$31, %r13d
	cmpw	$4, %r13w
	je	.L692
	jg	.L693
	testw	%r13w, %r13w
	je	.L694
	cmpw	$2, %r13w
	jne	.L696
	leal	(%rax,%rax), %eax
	leaq	10(%r12), %rcx
	cltq
	leaq	10(%rsi), %rdx
	cmpq	$8, %rax
	jnb	.L698
	testb	$4, %al
	jne	.L733
	testq	%rax, %rax
	je	.L718
	movzbl	10(%rsi), %esi
	movb	%sil, 10(%r12)
	testb	$2, %al
	je	.L718
	movzwl	-2(%rdx,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
.L718:
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L734:
	.cfi_restore_state
	movl	12(%rsi), %r14d
	cmpl	$27, %r14d
	jle	.L712
	cmpl	$2147483637, %r14d
	jle	.L713
	.p2align 4,,10
	.p2align 3
.L696:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movl	$0, 16(%r12)
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L693:
	cmpw	$8, %r13w
	jne	.L696
	movq	24(%rsi), %rax
	movq	%rax, 24(%r12)
	movl	16(%rsi), %eax
	movl	%eax, 16(%r12)
	testw	%dx, %dx
	jns	.L718
.L729:
	movl	12(%rsi), %eax
	movl	%eax, 12(%r12)
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L730:
	testw	%ax, %ax
	je	.L696
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	jne	.L696
	movq	24(%rdi), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L732:
	movl	$2, %esi
	movw	%si, 8(%r12)
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L731:
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	je	.L690
.L728:
	movzwl	8(%rsi), %edx
	movswl	%dx, %eax
	sarl	$5, %eax
	movl	%eax, %r14d
	jne	.L691
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L694:
	testw	%dx, %dx
	js	.L734
	cmpl	$27, %eax
	jg	.L713
.L712:
	movl	$2, %ecx
	movl	$2, %r13d
	movw	%cx, 8(%r12)
.L705:
	testb	$2, 8(%rsi)
	je	.L707
	addq	$10, %rsi
.L708:
	leaq	10(%r12), %rdi
	testw	%r13w, %r13w
	jne	.L710
	movq	24(%r12), %rdi
.L710:
	movl	%r14d, %edx
	call	u_memcpy_67@PLT
	cmpl	$1023, %r14d
	jg	.L711
	movzwl	8(%r12), %edx
	movl	%r14d, %eax
	sall	$5, %eax
	andl	$31, %edx
	orl	%edx, %eax
	movw	%ax, 8(%r12)
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L692:
	movq	24(%rsi), %rax
	lock addl	$1, -4(%rax)
	movq	24(%rsi), %rax
	cmpw	$0, 8(%r12)
	movq	%rax, 24(%r12)
	movl	16(%rsi), %eax
	movl	%eax, 16(%r12)
	jns	.L718
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L698:
	movq	10(%rsi), %rsi
	leaq	18(%r12), %rdi
	andq	$-8, %rdi
	movq	%rsi, 10(%r12)
	movq	-8(%rdx,%rax), %rsi
	movq	%rsi, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rdx
	addq	%rax, %rcx
	movq	%rdx, %rsi
	shrq	$3, %rcx
	rep movsq
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L713:
	leal	1(%r14), %eax
	movq	%rsi, -40(%rbp)
	cltq
	leaq	19(%rax,%rax), %r15
	andq	$-16, %r15
	movq	%r15, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L696
	subq	$4, %r15
	movl	$1, (%rax)
	movl	$4, %edx
	addq	$4, %rax
	shrq	%r15
	movq	%rax, 24(%r12)
	movq	-40(%rbp), %rsi
	movl	%r15d, 16(%r12)
	movw	%dx, 8(%r12)
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L690:
	movq	24(%rdi), %rax
	movq	%rsi, -40(%rbp)
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	movq	-40(%rbp), %rsi
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L711:
	orw	$-32, 8(%r12)
	movl	%r14d, 12(%r12)
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L707:
	movq	24(%rsi), %rsi
	jmp	.L708
.L733:
	movl	10(%rsi), %esi
	movl	%esi, 10(%r12)
	movl	-4(%rdx,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L718
	.cfi_endproc
.LFE3025:
	.size	_ZN6icu_6713UnicodeString12fastCopyFromERKS0_, .-_ZN6icu_6713UnicodeString12fastCopyFromERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString8copyFromERKS0_a
	.type	_ZN6icu_6713UnicodeString8copyFromERKS0_a, @function
_ZN6icu_6713UnicodeString8copyFromERKS0_a:
.LFB3026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	cmpq	%rsi, %rdi
	je	.L772
	movzwl	8(%rdi), %eax
	movzwl	8(%rsi), %ecx
	andl	$4, %eax
	testb	$1, %cl
	jne	.L785
	testw	%ax, %ax
	jne	.L786
.L742:
	movswl	%cx, %eax
	sarl	$5, %eax
	movl	%eax, %r13d
	je	.L787
.L744:
	movl	%ecx, %eax
	movw	%cx, 8(%r12)
	andl	$31, %eax
	cmpw	$4, %ax
	je	.L745
	jg	.L746
	testw	%ax, %ax
	je	.L747
	cmpw	$2, %ax
	jne	.L749
	leal	(%r13,%r13), %eax
	leaq	10(%r12), %rcx
	cltq
	leaq	10(%rsi), %rdx
	cmpq	$8, %rax
	jnb	.L751
	testb	$4, %al
	jne	.L788
	testq	%rax, %rax
	je	.L772
	movzbl	10(%rsi), %esi
	movb	%sil, 10(%r12)
	testb	$2, %al
	je	.L772
	movzwl	-2(%rdx,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
.L772:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	.cfi_restore_state
	movl	12(%rsi), %r13d
	cmpl	$27, %r13d
	jle	.L765
	cmpl	$2147483637, %r13d
	jle	.L766
.L749:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movq	%r12, %rax
	movl	$0, 16(%r12)
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L746:
	.cfi_restore_state
	cmpw	$8, %ax
	jne	.L749
	testb	%dl, %dl
	je	.L747
	movq	24(%rsi), %rax
	movq	%rax, 24(%r12)
	movl	16(%rsi), %eax
	movl	%eax, 16(%r12)
	testw	%cx, %cx
	jns	.L772
.L784:
	movl	12(%rsi), %eax
	movl	%eax, 12(%r12)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L785:
	testw	%ax, %ax
	je	.L749
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	jne	.L749
	movq	24(%rdi), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L787:
	movl	$2, %esi
	movq	%r12, %rax
	movw	%si, 8(%r12)
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	je	.L743
	movzwl	8(%rsi), %ecx
	movswl	%cx, %eax
	sarl	$5, %eax
	movl	%eax, %r13d
	jne	.L744
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L747:
	testw	%cx, %cx
	js	.L789
	cmpl	$27, %r13d
	jg	.L766
.L765:
	movl	$2, %ecx
	movl	$2, %eax
	movw	%cx, 8(%r12)
.L758:
	testb	$2, 8(%rsi)
	je	.L760
	addq	$10, %rsi
.L761:
	leaq	10(%r12), %rdi
	testw	%ax, %ax
	jne	.L763
	movq	24(%r12), %rdi
.L763:
	movl	%r13d, %edx
	call	u_memcpy_67@PLT
	cmpl	$1023, %r13d
	jg	.L764
	movzwl	8(%r12), %edx
	movl	%r13d, %eax
	sall	$5, %eax
	andl	$31, %edx
	orl	%edx, %eax
	movw	%ax, 8(%r12)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L745:
	movq	24(%rsi), %rax
	lock addl	$1, -4(%rax)
	movq	24(%rsi), %rax
	cmpw	$0, 8(%r12)
	movq	%rax, 24(%r12)
	movl	16(%rsi), %eax
	movl	%eax, 16(%r12)
	jns	.L772
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L751:
	movq	10(%rsi), %rsi
	leaq	18(%r12), %rdi
	andq	$-8, %rdi
	movq	%rsi, 10(%r12)
	movq	-8(%rdx,%rax), %rsi
	movq	%rsi, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rdx
	addq	%rax, %rcx
	movq	%rdx, %rsi
	shrq	$3, %rcx
	rep movsq
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L766:
	leal	1(%r13), %eax
	movq	%rsi, -40(%rbp)
	cltq
	leaq	19(%rax,%rax), %r14
	andq	$-16, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L749
	subq	$4, %r14
	movl	$1, (%rax)
	movl	$4, %edx
	addq	$4, %rax
	shrq	%r14
	movq	%rax, 24(%r12)
	movq	-40(%rbp), %rsi
	xorl	%eax, %eax
	movl	%r14d, 16(%r12)
	movw	%dx, 8(%r12)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L743:
	movq	24(%rdi), %rax
	movl	%edx, -44(%rbp)
	movq	%rsi, -40(%rbp)
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	movq	-40(%rbp), %rsi
	movl	-44(%rbp), %edx
	movzwl	8(%rsi), %ecx
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L760:
	movq	24(%rsi), %rsi
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L764:
	orw	$-32, 8(%r12)
	movl	%r13d, 12(%r12)
	jmp	.L772
.L788:
	movl	10(%rsi), %esi
	movl	%esi, 10(%r12)
	movl	-4(%rdx,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L772
	.cfi_endproc
.LFE3026:
	.size	_ZN6icu_6713UnicodeString8copyFromERKS0_a, .-_ZN6icu_6713UnicodeString8copyFromERKS0_a
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringaSEOS0_
	.type	_ZN6icu_6713UnicodeStringaSEOS0_, @function
_ZN6icu_6713UnicodeStringaSEOS0_:
.LFB3027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$24, %rsp
	testb	$4, 8(%rdi)
	jne	.L808
.L792:
	movswl	8(%rbx), %edx
	movw	%dx, 8(%rax)
	testb	$2, %dl
	je	.L794
	cmpq	%rbx, %rax
	je	.L795
	sarl	$5, %edx
	leaq	10(%rax), %rcx
	leaq	10(%rbx), %rsi
	addl	%edx, %edx
	movslq	%edx, %rdx
	cmpq	$8, %rdx
	jnb	.L796
	testb	$4, %dl
	jne	.L809
	testq	%rdx, %rdx
	je	.L795
	movzbl	10(%rbx), %edi
	movb	%dil, 10(%rax)
	testb	$2, %dl
	jne	.L810
.L795:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L794:
	.cfi_restore_state
	movq	24(%rbx), %rcx
	movq	%rcx, 24(%rax)
	movl	16(%rbx), %ecx
	movl	%ecx, 16(%rax)
	testw	%dx, %dx
	js	.L811
.L800:
	movl	$1, %edx
	movq	$0, 24(%rbx)
	movw	%dx, 8(%rbx)
	movl	$0, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	.cfi_restore_state
	movl	12(%rbx), %edx
	movl	%edx, 12(%rax)
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L808:
	movq	24(%rdi), %rdx
	lock subl	$1, -4(%rdx)
	jne	.L792
	movq	24(%rdi), %rsi
	movq	%rdi, -24(%rbp)
	leaq	-4(%rsi), %rdi
	call	uprv_free_67@PLT
	movq	-24(%rbp), %rax
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L796:
	movq	10(%rbx), %rdi
	movq	%rdi, 10(%rax)
	movq	-8(%rsi,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	18(%rax), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%rdx, %rcx
	shrq	$3, %rcx
	rep movsq
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L810:
	.cfi_restore_state
	movzwl	-2(%rsi,%rdx), %esi
	movw	%si, -2(%rcx,%rdx)
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L809:
	movl	10(%rbx), %edi
	movl	%edi, 10(%rax)
	movl	-4(%rsi,%rdx), %esi
	movl	%esi, -4(%rcx,%rdx)
	jmp	.L795
	.cfi_endproc
.LFE3027:
	.size	_ZN6icu_6713UnicodeStringaSEOS0_, .-_ZN6icu_6713UnicodeStringaSEOS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString14copyFieldsFromERS0_a
	.type	_ZN6icu_6713UnicodeString14copyFieldsFromERS0_a, @function
_ZN6icu_6713UnicodeString14copyFieldsFromERS0_a:
.LFB3028:
	.cfi_startproc
	endbr64
	movswl	8(%rsi), %eax
	movw	%ax, 8(%rdi)
	testb	$2, %al
	je	.L813
	cmpq	%rdi, %rsi
	je	.L812
	sarl	$5, %eax
	leaq	10(%rdi), %rcx
	leaq	10(%rsi), %rdx
	addl	%eax, %eax
	cltq
	cmpq	$8, %rax
	jnb	.L816
	testb	$4, %al
	jne	.L828
	testq	%rax, %rax
	je	.L812
	movzbl	10(%rsi), %esi
	movb	%sil, 10(%rdi)
	testb	$2, %al
	jne	.L829
.L812:
	ret
	.p2align 4,,10
	.p2align 3
.L813:
	movq	24(%rsi), %rcx
	movq	%rcx, 24(%rdi)
	movl	16(%rsi), %ecx
	movl	%ecx, 16(%rdi)
	testw	%ax, %ax
	js	.L830
	testb	%dl, %dl
	je	.L812
.L831:
	movl	$1, %eax
	movq	$0, 24(%rsi)
	movw	%ax, 8(%rsi)
	movl	$0, 16(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L830:
	movl	12(%rsi), %eax
	movl	%eax, 12(%rdi)
	testb	%dl, %dl
	je	.L812
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L816:
	movq	10(%rsi), %rsi
	addq	$18, %rdi
	movq	%rsi, -8(%rdi)
	movq	-8(%rdx,%rax), %rsi
	andq	$-8, %rdi
	movq	%rsi, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rdx
	addq	%rax, %rcx
	movq	%rdx, %rsi
	shrq	$3, %rcx
	rep movsq
	ret
.L829:
	movzwl	-2(%rdx,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L828:
	movl	10(%rsi), %esi
	movl	%esi, 10(%rdi)
	movl	-4(%rdx,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	ret
	.cfi_endproc
.LFE3028:
	.size	_ZN6icu_6713UnicodeString14copyFieldsFromERS0_a, .-_ZN6icu_6713UnicodeString14copyFieldsFromERS0_a
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString4swapERS0_
	.type	_ZN6icu_6713UnicodeString4swapERS0_, @function
_ZN6icu_6713UnicodeString4swapERS0_:
.LFB3029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movswl	8(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	%r13d, %r15d
	movq	%rax, -128(%rbp)
	movl	%r13d, %r14d
	movw	%r13w, -120(%rbp)
	andw	$2, %r15w
	je	.L833
	movl	%r13d, %edx
	leaq	10(%rdi), %rsi
	leaq	-118(%rbp), %rdi
	movl	$54, %ecx
	sarl	$5, %edx
	addl	%edx, %edx
	movslq	%edx, %rdx
	call	__memcpy_chk@PLT
.L834:
	movswl	8(%rbx), %eax
	movw	%ax, 8(%r12)
	testb	$2, %al
	je	.L835
	cmpq	%rbx, %r12
	je	.L837
	sarl	$5, %eax
	leaq	10(%r12), %rcx
	leaq	10(%rbx), %rsi
	addl	%eax, %eax
	cltq
	cmpq	$8, %rax
	jnb	.L838
	testb	$4, %al
	jne	.L863
	testq	%rax, %rax
	je	.L837
	movzbl	10(%rbx), %edx
	movb	%dl, 10(%r12)
	testb	$2, %al
	jne	.L864
	.p2align 4,,10
	.p2align 3
.L837:
	movw	%r14w, 8(%rbx)
	testw	%r15w, %r15w
	je	.L843
.L868:
	sarl	$5, %r13d
	leaq	10(%rbx), %rax
	leaq	-118(%rbp), %rsi
	leal	(%r13,%r13), %ecx
	movslq	%ecx, %rcx
	cmpq	$8, %rcx
	jnb	.L844
	testb	$4, %cl
	jne	.L865
	testq	%rcx, %rcx
	jne	.L866
	.p2align 4,,10
	.p2align 3
.L848:
	movl	$2, %eax
	leaq	-128(%rbp), %rdi
	movw	%ax, -120(%rbp)
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%rax, -128(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L867
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	movq	%rdx, 24(%r12)
	movl	16(%rbx), %edx
	movl	%edx, 16(%r12)
	testw	%ax, %ax
	jns	.L837
	movl	12(%rbx), %eax
	movl	%eax, 12(%r12)
	movw	%r14w, 8(%rbx)
	testw	%r15w, %r15w
	jne	.L868
.L843:
	movq	-104(%rbp), %rax
	movq	%rax, 24(%rbx)
	movl	-112(%rbp), %eax
	movl	%eax, 16(%rbx)
	testw	%r14w, %r14w
	jns	.L848
	movl	-116(%rbp), %eax
	movl	%eax, 12(%rbx)
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L833:
	movq	24(%rdi), %rax
	movq	%rax, -104(%rbp)
	movl	16(%rdi), %eax
	movl	%eax, -112(%rbp)
	testw	%r13w, %r13w
	jns	.L834
	movl	12(%rdi), %eax
	movl	%eax, -116(%rbp)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L844:
	movq	-118(%rbp), %rdx
	leaq	18(%rbx), %rdi
	andq	$-8, %rdi
	movq	%rdx, 10(%rbx)
	movq	-126(%rbp,%rcx), %rdx
	movq	%rdx, -8(%rax,%rcx)
	subq	%rdi, %rax
	addq	%rax, %rcx
	subq	%rax, %rsi
	shrq	$3, %rcx
	rep movsq
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L838:
	movq	10(%rbx), %rdx
	leaq	18(%r12), %rdi
	andq	$-8, %rdi
	movq	%rdx, 10(%r12)
	movq	-8(%rsi,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%rax, %rcx
	shrq	$3, %rcx
	rep movsq
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L866:
	movzbl	(%rsi), %edx
	movb	%dl, 10(%rbx)
	testb	$2, %cl
	je	.L848
	movzwl	-2(%rsi,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	jmp	.L848
.L864:
	movzwl	-2(%rsi,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L865:
	movl	(%rsi), %edx
	movl	%edx, 10(%rbx)
	movl	-4(%rsi,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L863:
	movl	10(%rbx), %edx
	movl	%edx, 10(%r12)
	movl	-4(%rsi,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L837
.L867:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3029:
	.size	_ZN6icu_6713UnicodeString4swapERS0_, .-_ZN6icu_6713UnicodeString4swapERS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString10unescapeAtERi
	.type	_ZNK6icu_6713UnicodeString10unescapeAtERi, @function
_ZNK6icu_6713UnicodeString10unescapeAtERi:
.LFB3031:
	.cfi_startproc
	endbr64
	movswl	8(%rdi), %edx
	movq	%rdi, %rcx
	testw	%dx, %dx
	js	.L870
	sarl	$5, %edx
	leaq	UnicodeString_charAt(%rip), %rdi
	jmp	u_unescapeAt_67@PLT
	.p2align 4,,10
	.p2align 3
.L870:
	movl	12(%rdi), %edx
	leaq	UnicodeString_charAt(%rip), %rdi
	jmp	u_unescapeAt_67@PLT
	.cfi_endproc
.LFE3031:
	.size	_ZNK6icu_6713UnicodeString10unescapeAtERi, .-_ZNK6icu_6713UnicodeString10unescapeAtERi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i
	.type	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i, @function
_ZNK6icu_6713UnicodeString8doEqualsERKS0_i:
.LFB3032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addl	%edx, %edx
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	$2, 8(%rsi)
	je	.L873
	addq	$10, %rsi
	testb	$2, 8(%rdi)
	je	.L875
.L878:
	addq	$10, %rdi
	call	memcmp@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L873:
	.cfi_restore_state
	movq	24(%rsi), %rsi
	testb	$2, 8(%rdi)
	jne	.L878
.L875:
	movq	24(%rdi), %rdi
	call	memcmp@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE3032:
	.size	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i, .-_ZNK6icu_6713UnicodeString8doEqualsERKS0_i
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii
	.type	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii, @function
_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii:
.LFB3033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movzwl	8(%rdi), %edi
	movl	%edi, %r12d
	andl	$1, %r12d
	jne	.L892
	testw	%di, %di
	js	.L881
	movswl	%di, %eax
	sarl	$5, %eax
.L882:
	xorl	%r11d, %r11d
	testl	%esi, %esi
	js	.L883
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	movslq	%esi, %r11
.L883:
	xorl	%r14d, %r14d
	testl	%edx, %edx
	js	.L884
	subl	%r11d, %eax
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movl	%edx, %r14d
.L884:
	testq	%rcx, %rcx
	je	.L904
	andl	$2, %edi
	jne	.L905
	movq	24(%r10), %r10
.L887:
	movslq	%r8d, %rdi
	leaq	(%r10,%r11,2), %rbx
	addq	%rdi, %rdi
	leaq	(%rcx,%rdi), %r13
	testl	%r9d, %r9d
	js	.L906
.L888:
	cmpl	%r14d, %r9d
	je	.L889
	movl	$-1, %r12d
	movl	$1, %eax
	cmovle	%r9d, %r14d
	cmovle	%eax, %r12d
.L889:
	testl	%r14d, %r14d
	jle	.L879
	cmpq	%r13, %rbx
	je	.L879
	leal	-1(%r14), %esi
	xorl	%eax, %eax
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L890:
	leaq	1(%rax), %rcx
	cmpq	%rax, %rsi
	je	.L879
	movq	%rcx, %rax
.L891:
	movzwl	(%rbx,%rax,2), %edx
	movzwl	0(%r13,%rax,2), %ecx
	subl	%ecx, %edx
	je	.L890
	sarl	$15, %edx
	movl	%edx, %r12d
	orl	$1, %r12d
.L879:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L905:
	.cfi_restore_state
	addq	$10, %r10
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L881:
	movl	12(%r10), %eax
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L906:
	addq	%r13, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %r9d
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L904:
	testl	%r14d, %r14d
	popq	%rbx
	setne	%r12b
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L892:
	.cfi_restore_state
	movl	$-1, %r12d
	jmp	.L879
	.cfi_endproc
.LFE3033:
	.size	_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii, .-_ZNK6icu_6713UnicodeString9doCompareEiiPKDsii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString23doCompareCodePointOrderEiiPKDsii
	.type	_ZNK6icu_6713UnicodeString23doCompareCodePointOrderEiiPKDsii, @function
_ZNK6icu_6713UnicodeString23doCompareCodePointOrderEiiPKDsii:
.LFB3034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movzwl	8(%rdi), %r10d
	movl	%r10d, %r12d
	andl	$1, %r12d
	jne	.L917
	movl	%edx, %r11d
	movq	%rcx, %rdx
	movl	%r9d, %ecx
	testw	%r10w, %r10w
	js	.L909
	movswl	%r10w, %eax
	sarl	$5, %eax
.L910:
	testl	%esi, %esi
	js	.L918
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	movslq	%esi, %r9
	addq	%r9, %r9
.L911:
	xorl	%r13d, %r13d
	testl	%r11d, %r11d
	js	.L912
	subl	%esi, %eax
	cmpl	%r11d, %eax
	cmovle	%eax, %r11d
	movl	%r11d, %r13d
.L912:
	testq	%rdx, %rdx
	je	.L923
	movslq	%r8d, %r8
	leaq	(%rdx,%r8,2), %rdx
.L916:
	andl	$2, %r10d
	jne	.L924
	movq	24(%rdi), %rdi
.L915:
	addq	%r9, %rdi
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movl	%r13d, %esi
	call	uprv_strCompare_67@PLT
	movl	%eax, %edx
	sarl	$15, %edx
	orl	$1, %edx
	testl	%eax, %eax
	cmovne	%edx, %r12d
.L907:
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore_state
	addq	$10, %rdi
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L918:
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L909:
	movl	12(%rdi), %eax
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L923:
	xorl	%ecx, %ecx
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L917:
	movl	$-1, %r12d
	jmp	.L907
	.cfi_endproc
.LFE3034:
	.size	_ZNK6icu_6713UnicodeString23doCompareCodePointOrderEiiPKDsii, .-_ZNK6icu_6713UnicodeString23doCompareCodePointOrderEiiPKDsii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString8char32AtEi
	.type	_ZNK6icu_6713UnicodeString8char32AtEi, @function
_ZNK6icu_6713UnicodeString8char32AtEi:
.LFB3038:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L926
	movswl	%dx, %eax
	sarl	$5, %eax
.L927:
	movl	$65535, %r8d
	cmpl	%eax, %esi
	jnb	.L925
	andl	$2, %edx
	jne	.L933
	movq	24(%rdi), %rdi
.L930:
	movslq	%esi, %rdx
	movzwl	(%rdi,%rdx,2), %r8d
	leaq	(%rdx,%rdx), %r9
	movl	%r8d, %ecx
	movl	%r8d, %edx
	andl	$63488, %ecx
	cmpl	$55296, %ecx
	jne	.L925
	andb	$4, %dh
	jne	.L931
	addl	$1, %esi
	cmpl	%eax, %esi
	je	.L925
	movzwl	2(%rdi,%r9), %edx
	movl	%edx, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L934
.L925:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L933:
	addq	$10, %rdi
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L926:
	movl	12(%rdi), %eax
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L931:
	testl	%esi, %esi
	jle	.L925
	movzwl	-2(%rdi,%r9), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L925
	sall	$10, %eax
	leal	-56613888(%r8,%rax), %r8d
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L934:
	sall	$10, %r8d
	leal	-56613888(%rdx,%r8), %r8d
	jmp	.L925
	.cfi_endproc
.LFE3038:
	.size	_ZNK6icu_6713UnicodeString8char32AtEi, .-_ZNK6icu_6713UnicodeString8char32AtEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString14getChar32StartEi
	.type	_ZNK6icu_6713UnicodeString14getChar32StartEi, @function
_ZNK6icu_6713UnicodeString14getChar32StartEi:
.LFB3039:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L936
	movswl	%dx, %eax
	sarl	$5, %eax
.L937:
	xorl	%r8d, %r8d
	cmpl	%esi, %eax
	jbe	.L935
	andl	$2, %edx
	jne	.L950
	movq	24(%rdi), %rdi
.L940:
	movslq	%esi, %rax
	leaq	(%rax,%rax), %rdx
	movzwl	(%rdi,%rax,2), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L942
	testl	%esi, %esi
	jg	.L951
.L942:
	movl	%esi, %r8d
.L935:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L950:
	addq	$10, %rdi
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L936:
	movl	12(%rdi), %eax
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L951:
	movzwl	-2(%rdi,%rdx), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	sete	%al
	movzbl	%al, %eax
	subl	%eax, %esi
	movl	%esi, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3039:
	.size	_ZNK6icu_6713UnicodeString14getChar32StartEi, .-_ZNK6icu_6713UnicodeString14getChar32StartEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString14getChar32LimitEi
	.type	_ZNK6icu_6713UnicodeString14getChar32LimitEi, @function
_ZNK6icu_6713UnicodeString14getChar32LimitEi:
.LFB3040:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L953
	movswl	%dx, %eax
	sarl	$5, %eax
.L954:
	cmpl	%eax, %esi
	jnb	.L952
	andl	$2, %edx
	jne	.L964
	movq	24(%rdi), %rdi
	testl	%esi, %esi
	jle	.L959
.L965:
	cmpl	%eax, %esi
	jl	.L963
	shrl	$31, %eax
	movl	%eax, %edx
	movl	%esi, %eax
	testb	%dl, %dl
	jne	.L963
.L952:
	ret
	.p2align 4,,10
	.p2align 3
.L964:
	addq	$10, %rdi
	testl	%esi, %esi
	jg	.L965
.L959:
	movl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L953:
	movl	12(%rdi), %eax
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L963:
	movslq	%esi, %rcx
	movl	%esi, %eax
	movzwl	-2(%rdi,%rcx,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L952
	movzwl	(%rdi,%rcx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	sete	%dl
	movzbl	%dl, %edx
	addl	%edx, %eax
	ret
	.cfi_endproc
.LFE3040:
	.size	_ZNK6icu_6713UnicodeString14getChar32LimitEi, .-_ZNK6icu_6713UnicodeString14getChar32LimitEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString11countChar32Eii
	.type	_ZNK6icu_6713UnicodeString11countChar32Eii, @function
_ZNK6icu_6713UnicodeString11countChar32Eii:
.LFB3041:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %ecx
	testw	%cx, %cx
	js	.L967
	movswl	%cx, %eax
	sarl	$5, %eax
	testl	%esi, %esi
	js	.L973
.L975:
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	movslq	%esi, %r8
	addq	%r8, %r8
.L969:
	xorl	%r9d, %r9d
	testl	%edx, %edx
	js	.L970
	subl	%esi, %eax
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movl	%edx, %r9d
.L970:
	andl	$2, %ecx
	je	.L971
	addq	$10, %rdi
	movl	%r9d, %esi
	addq	%r8, %rdi
	jmp	u_countChar32_67@PLT
	.p2align 4,,10
	.p2align 3
.L971:
	movq	24(%rdi), %rdi
	movl	%r9d, %esi
	addq	%r8, %rdi
	jmp	u_countChar32_67@PLT
	.p2align 4,,10
	.p2align 3
.L967:
	movl	12(%rdi), %eax
	testl	%esi, %esi
	jns	.L975
.L973:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	jmp	.L969
	.cfi_endproc
.LFE3041:
	.size	_ZNK6icu_6713UnicodeString11countChar32Eii, .-_ZNK6icu_6713UnicodeString11countChar32Eii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString17hasMoreChar32ThanEiii
	.type	_ZNK6icu_6713UnicodeString17hasMoreChar32ThanEiii, @function
_ZNK6icu_6713UnicodeString17hasMoreChar32ThanEiii:
.LFB3042:
	.cfi_startproc
	endbr64
	movl	%ecx, %r8d
	movzwl	8(%rdi), %ecx
	testw	%cx, %cx
	js	.L977
	movswl	%cx, %eax
	sarl	$5, %eax
	testl	%esi, %esi
	js	.L983
.L985:
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	movslq	%esi, %r9
	addq	%r9, %r9
.L979:
	xorl	%r10d, %r10d
	testl	%edx, %edx
	js	.L980
	subl	%esi, %eax
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movl	%edx, %r10d
.L980:
	andl	$2, %ecx
	je	.L981
	addq	$10, %rdi
	movl	%r8d, %edx
	movl	%r10d, %esi
	addq	%r9, %rdi
	jmp	u_strHasMoreChar32Than_67@PLT
	.p2align 4,,10
	.p2align 3
.L981:
	movq	24(%rdi), %rdi
	movl	%r8d, %edx
	movl	%r10d, %esi
	addq	%r9, %rdi
	jmp	u_strHasMoreChar32Than_67@PLT
	.p2align 4,,10
	.p2align 3
.L977:
	movl	12(%rdi), %eax
	testl	%esi, %esi
	jns	.L985
.L983:
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	jmp	.L979
	.cfi_endproc
.LFE3042:
	.size	_ZNK6icu_6713UnicodeString17hasMoreChar32ThanEiii, .-_ZNK6icu_6713UnicodeString17hasMoreChar32ThanEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString11moveIndex32Eii
	.type	_ZNK6icu_6713UnicodeString11moveIndex32Eii, @function
_ZNK6icu_6713UnicodeString11moveIndex32Eii:
.LFB3043:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %ecx
	testw	%cx, %cx
	js	.L987
	movswl	%cx, %r8d
	sarl	$5, %r8d
.L988:
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L989
	cmpl	%r8d, %esi
	movl	%esi, %eax
	cmovg	%r8d, %eax
.L989:
	andl	$2, %ecx
	je	.L990
	addq	$10, %rdi
	testl	%edx, %edx
	jle	.L992
	.p2align 4,,10
	.p2align 3
.L997:
	cmpl	%eax, %r8d
	jle	.L993
	movslq	%eax, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
.L994:
	andl	$64512, %ecx
	leal	1(%rax), %esi
	cmpl	$55296, %ecx
	jne	.L1004
	cmpl	%r8d, %esi
	jne	.L1023
.L1004:
	movl	%esi, %eax
.L996:
	subl	$1, %edx
	jne	.L997
.L1007:
	movl	%eax, %r8d
.L986:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1023:
	movslq	%esi, %rcx
	addl	$2, %eax
	movzwl	(%rdi,%rcx,2), %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	cmovne	%esi, %eax
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L993:
	testl	%r8d, %r8d
	jns	.L1007
	movslq	%eax, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	testw	%cx, %cx
	jne	.L994
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L990:
	movq	24(%rdi), %rdi
	testl	%edx, %edx
	jg	.L997
.L992:
	movl	%edx, %ecx
	negl	%ecx
	testl	%edx, %edx
	jne	.L1022
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1008:
	movl	%r8d, %eax
.L998:
	subl	$1, %ecx
	testl	%ecx, %ecx
	jle	.L1007
.L1022:
	testl	%eax, %eax
	jle	.L1007
	leal	-1(%rax), %r8d
	movslq	%r8d, %rdx
	leaq	(%rdx,%rdx), %rsi
	movzwl	(%rdi,%rdx,2), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L1008
	testl	%r8d, %r8d
	je	.L986
	movzwl	-2(%rdi,%rsi), %edx
	subl	$2, %eax
	andl	$-1024, %edx
	cmpl	$55296, %edx
	cmovne	%r8d, %eax
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L987:
	movl	12(%rdi), %r8d
	jmp	.L988
	.cfi_endproc
.LFE3043:
	.size	_ZNK6icu_6713UnicodeString11moveIndex32Eii, .-_ZNK6icu_6713UnicodeString11moveIndex32Eii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi
	.type	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi, @function
_ZNK6icu_6713UnicodeString9doExtractEiiPDsi:
.LFB3044:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %r9d
	testw	%r9w, %r9w
	js	.L1025
	movswl	%r9w, %eax
	sarl	$5, %eax
.L1026:
	testl	%esi, %esi
	js	.L1032
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	movslq	%esi, %r10
	addq	%r10, %r10
.L1027:
	testl	%edx, %edx
	js	.L1033
	subl	%esi, %eax
	cmpl	%edx, %eax
	cmovg	%edx, %eax
	testl	%eax, %eax
	setg	%dl
.L1028:
	andl	$2, %r9d
	leaq	10(%rdi), %rsi
	jne	.L1030
	movq	24(%rdi), %rsi
.L1030:
	movslq	%r8d, %r8
	addq	%r10, %rsi
	leaq	(%rcx,%r8,2), %rdi
	cmpq	%rdi, %rsi
	je	.L1024
	testb	%dl, %dl
	jne	.L1040
.L1024:
	ret
	.p2align 4,,10
	.p2align 3
.L1040:
	cltq
	leaq	(%rax,%rax), %rdx
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L1033:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1032:
	xorl	%r10d, %r10d
	xorl	%esi, %esi
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1025:
	movl	12(%rdi), %eax
	jmp	.L1026
	.cfi_endproc
.LFE3044:
	.size	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi, .-_ZNK6icu_6713UnicodeString9doExtractEiiPDsi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode
	.type	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode, @function
_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode:
.LFB3045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movzwl	8(%rdi), %eax
	testw	%ax, %ax
	js	.L1042
	movl	(%rcx), %edx
	movswl	%ax, %r12d
	sarl	$5, %r12d
	testl	%edx, %edx
	jle	.L1060
.L1041:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1042:
	.cfi_restore_state
	movl	(%rcx), %edx
	movl	12(%rdi), %r12d
	testl	%edx, %edx
	jg	.L1041
.L1060:
	testl	%r13d, %r13d
	js	.L1045
	testb	$1, %al
	je	.L1061
.L1045:
	movl	$1, (%rcx)
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1061:
	.cfi_restore_state
	movq	(%rbx), %r8
	testl	%r13d, %r13d
	je	.L1046
	testq	%r8, %r8
	je	.L1045
.L1046:
	leaq	10(%rdi), %rsi
	testb	$2, %al
	jne	.L1048
	movq	24(%rdi), %rsi
.L1048:
	testl	%r12d, %r12d
	jle	.L1049
	cmpl	%r13d, %r12d
	jg	.L1049
	cmpq	%r8, %rsi
	je	.L1049
	movq	%r8, %rdi
	movl	%r12d, %edx
	movq	%rcx, -40(%rbp)
	call	u_memcpy_67@PLT
	movq	(%rbx), %r8
	movq	-40(%rbp), %rcx
.L1049:
	addq	$24, %rsp
	movl	%r12d, %edx
	movl	%r13d, %esi
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
	.cfi_endproc
.LFE3045:
	.size	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode, .-_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE
	.type	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE, @function
_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE:
.LFB3046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	js	.L1062
	movq	%rcx, %r14
	movl	%r8d, %r12d
	jle	.L1077
	testq	%rcx, %rcx
	je	.L1062
.L1077:
	movzwl	8(%rdi), %ecx
	testw	%cx, %cx
	js	.L1065
	movswl	%cx, %r13d
	sarl	$5, %r13d
.L1066:
	xorl	%eax, %eax
	testl	%esi, %esi
	js	.L1067
	cmpl	%esi, %r13d
	movl	%esi, %eax
	cmovle	%r13d, %eax
.L1067:
	testl	%edx, %edx
	js	.L1076
	subl	%eax, %r13d
	cmpl	%edx, %r13d
	cmovg	%edx, %r13d
	cmpl	%r13d, %r12d
	jge	.L1068
.L1069:
	leaq	-44(%rbp), %rcx
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movl	$0, -44(%rbp)
	call	u_terminateChars_67@PLT
.L1062:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1082
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1076:
	.cfi_restore_state
	xorl	%r13d, %r13d
.L1068:
	andl	$2, %ecx
	jne	.L1083
	movq	24(%rdi), %rdi
.L1071:
	cltq
	movl	%r13d, %edx
	movq	%r14, %rsi
	leaq	(%rdi,%rax,2), %rdi
	call	u_UCharsToChars_67@PLT
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1083:
	addq	$10, %rdi
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1065:
	movl	12(%rdi), %r13d
	jmp	.L1066
.L1082:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3046:
	.size	_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE, .-_ZNK6icu_6713UnicodeString7extractEiiPciNS0_10EInvariantE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString13tempSubStringEii
	.type	_ZNK6icu_6713UnicodeString13tempSubStringEii, @function
_ZNK6icu_6713UnicodeString13tempSubStringEii:
.LFB3047:
	.cfi_startproc
	endbr64
	movzwl	8(%rsi), %r8d
	movq	%rdi, %rax
	testw	%r8w, %r8w
	js	.L1085
	movswl	%r8w, %edi
	sarl	$5, %edi
.L1086:
	testl	%edx, %edx
	js	.L1098
	cmpl	%edx, %edi
	cmovle	%edi, %edx
	movslq	%edx, %r9
	addq	%r9, %r9
.L1087:
	xorl	%r10d, %r10d
	testl	%ecx, %ecx
	js	.L1088
	subl	%edx, %edi
	cmpl	%ecx, %edi
	cmovle	%edi, %ecx
	movl	%ecx, %r10d
.L1088:
	testb	$17, %r8b
	jne	.L1089
	andl	$2, %r8d
	leaq	10(%rsi), %rdx
	jne	.L1091
	movq	24(%rsi), %rdx
	testq	%rdx, %rdx
	jne	.L1091
.L1089:
	leaq	10(%rsi,%r9), %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	movq	%rsi, (%rax)
.L1092:
	movl	$1, %esi
	movq	$0, 24(%rax)
	movw	%si, 8(%rax)
	movl	$0, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L1091:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rsi
	addq	%r9, %rdx
	movq	%rsi, (%rax)
	testl	%r10d, %r10d
	js	.L1092
	cmpl	$1023, %r10d
	jle	.L1103
	movl	$-24, %ecx
	movl	%r10d, 12(%rax)
	movw	%cx, 8(%rax)
.L1096:
	movq	%rdx, 24(%rax)
	movl	%r10d, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L1098:
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1085:
	movl	12(%rsi), %edi
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1103:
	movl	%r10d, %ecx
	sall	$5, %ecx
	orl	$8, %ecx
	movw	%cx, 8(%rax)
	jmp	.L1096
	.cfi_endproc
.LFE3047:
	.size	_ZNK6icu_6713UnicodeString13tempSubStringEii, .-_ZNK6icu_6713UnicodeString13tempSubStringEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString6toUTF8EiiPci
	.type	_ZNK6icu_6713UnicodeString6toUTF8EiiPci, @function
_ZNK6icu_6713UnicodeString6toUTF8EiiPci:
.LFB3048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movl	%r8d, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movzwl	8(%rdi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testw	%r9w, %r9w
	js	.L1105
	movswl	%r9w, %eax
	sarl	$5, %eax
.L1106:
	testl	%esi, %esi
	js	.L1112
	cmpl	%esi, %eax
	cmovle	%eax, %esi
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx), %rbx
.L1107:
	xorl	%r8d, %r8d
	testl	%edx, %edx
	js	.L1108
	subl	%esi, %eax
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movl	%edx, %r8d
.L1108:
	movl	$0, -28(%rbp)
	testb	$17, %r9b
	jne	.L1114
	andl	$2, %r9d
	leaq	10(%rdi), %rcx
	je	.L1116
.L1109:
	leaq	-28(%rbp), %rax
	addq	%rbx, %rcx
	leaq	-32(%rbp), %rdx
	movl	%r11d, %esi
	pushq	%rax
	movl	$65533, %r9d
	movq	%r10, %rdi
	pushq	$0
	call	u_strToUTF8WithSub_67@PLT
	movl	-32(%rbp), %eax
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1117
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1116:
	.cfi_restore_state
	movq	24(%rdi), %rcx
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1112:
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1105:
	movl	12(%rdi), %eax
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1114:
	xorl	%ecx, %ecx
	jmp	.L1109
.L1117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3048:
	.size	_ZNK6icu_6713UnicodeString6toUTF8EiiPci, .-_ZNK6icu_6713UnicodeString6toUTF8EiiPci
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString7extractEiiPcj
	.type	_ZNK6icu_6713UnicodeString7extractEiiPcj, @function
_ZNK6icu_6713UnicodeString7extractEiiPcj:
.LFB3049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L1134
	testq	%rcx, %rcx
	jne	.L1134
.L1118:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1139
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1134:
	.cfi_restore_state
	movzwl	8(%rdi), %r9d
	testl	%r8d, %r8d
	movl	$2147483647, %r10d
	cmovns	%r8d, %r10d
	testw	%r9w, %r9w
	js	.L1122
	movswl	%r9w, %eax
	sarl	$5, %eax
	testl	%esi, %esi
	js	.L1131
.L1140:
	cmpl	%eax, %esi
	cmovg	%eax, %esi
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx), %rbx
.L1124:
	xorl	%r8d, %r8d
	testl	%edx, %edx
	js	.L1125
	subl	%esi, %eax
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movl	%edx, %r8d
.L1125:
	movl	$0, -28(%rbp)
	testb	$17, %r9b
	jne	.L1133
	andl	$2, %r9d
	leaq	10(%rdi), %rcx
	jne	.L1126
	movq	24(%rdi), %rcx
.L1126:
	leaq	-28(%rbp), %rax
	addq	%rbx, %rcx
	leaq	-32(%rbp), %rdx
	movl	%r10d, %esi
	pushq	%rax
	movl	$65533, %r9d
	movq	%r11, %rdi
	pushq	$0
	call	u_strToUTF8WithSub_67@PLT
	popq	%rdx
	movl	-32(%rbp), %eax
	popq	%rcx
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1122:
	movl	12(%rdi), %eax
	testl	%esi, %esi
	jns	.L1140
.L1131:
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1133:
	xorl	%ecx, %ecx
	jmp	.L1126
.L1139:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3049:
	.size	_ZNK6icu_6713UnicodeString7extractEiiPcj, .-_ZNK6icu_6713UnicodeString7extractEiiPcj
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE
	.type	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE, @function
_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE:
.LFB3051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movswl	8(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testw	%si, %si
	js	.L1142
	sarl	$5, %esi
	jne	.L1165
.L1141:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1166
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1165:
	.cfi_restore_state
	movq	(%r12), %rax
	leal	(%rsi,%rsi,2), %edx
	movl	%esi, %r13d
	movl	$1024, -1100(%rbp)
	movq	24(%rax), %rax
.L1145:
	leaq	-1088(%rbp), %rcx
	leaq	-1100(%rbp), %r9
	movl	$1024, %r8d
	movq	%r12, %rdi
	call	*%rax
	movl	$0, -1096(%rbp)
	movl	$0, -1092(%rbp)
	movq	%rax, %r14
	movzwl	8(%rbx), %eax
	testb	$17, %al
	jne	.L1159
	leaq	10(%rbx), %rcx
	testb	$2, %al
	jne	.L1146
	movq	24(%rbx), %rcx
.L1146:
	leaq	-1092(%rbp), %rax
	movl	-1100(%rbp), %esi
	movq	%r14, %rdi
	movl	%r13d, %r8d
	pushq	%rax
	leaq	-1096(%rbp), %r15
	movl	$65533, %r9d
	pushq	$0
	movq	%r15, %rdx
	movq	%rax, -1112(%rbp)
	call	u_strToUTF8WithSub_67@PLT
	movl	-1092(%rbp), %eax
	popq	%rsi
	popq	%rdi
	cmpl	$15, %eax
	je	.L1167
	testl	%eax, %eax
	jg	.L1141
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	-1096(%rbp), %edx
	movq	%r14, %rsi
	call	*16(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1142:
	movl	12(%rdi), %r13d
	testl	%r13d, %r13d
	je	.L1141
	movq	(%r12), %rax
	movl	$1024, %esi
	cmpl	$1024, %r13d
	movl	$1024, -1100(%rbp)
	cmovl	%r13d, %esi
	leal	0(%r13,%r13,2), %edx
	movq	24(%rax), %rax
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1167:
	movslq	-1096(%rbp), %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1141
	movl	$0, -1092(%rbp)
	movzwl	8(%rbx), %eax
	testb	$17, %al
	jne	.L1160
	leaq	10(%rbx), %rcx
	testb	$2, %al
	je	.L1168
.L1151:
	pushq	-1112(%rbp)
	movq	%r15, %rdx
	movl	%r13d, %r8d
	movq	%r14, %rdi
	movl	-1096(%rbp), %esi
	pushq	$0
	movl	$65533, %r9d
	call	u_strToUTF8WithSub_67@PLT
	movl	-1092(%rbp), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L1156
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	-1096(%rbp), %edx
	movq	%r14, %rsi
	call	*16(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*32(%rax)
.L1156:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1159:
	xorl	%ecx, %ecx
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1168:
	movq	24(%rbx), %rcx
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1160:
	xorl	%ecx, %ecx
	jmp	.L1151
.L1166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3051:
	.size	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE, .-_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString7toUTF32EPiiR10UErrorCode
	.type	_ZNK6icu_6713UnicodeString7toUTF32EPiiR10UErrorCode, @function
_ZNK6icu_6713UnicodeString7toUTF32EPiiR10UErrorCode:
.LFB3052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -8(%rbp)
	xorl	%ecx, %ecx
	movl	(%rax), %ecx
	movl	$0, -12(%rbp)
	testl	%ecx, %ecx
	jle	.L1180
.L1169:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1181
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1180:
	.cfi_restore_state
	movq	%rsi, %r10
	movl	%edx, %esi
	movzwl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L1171
	movswl	%dx, %r8d
	sarl	$5, %r8d
.L1172:
	testb	$17, %dl
	jne	.L1177
	andl	$2, %edx
	leaq	10(%rdi), %rcx
	jne	.L1173
	movq	24(%rdi), %rcx
.L1173:
	pushq	%rax
	leaq	-12(%rbp), %rdx
	movl	$65533, %r9d
	movq	%r10, %rdi
	pushq	$0
	call	u_strToUTF32WithSub_67@PLT
	popq	%rax
	movl	-12(%rbp), %r8d
	popq	%rdx
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1171:
	movl	12(%rdi), %r8d
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1177:
	xorl	%ecx, %ecx
	jmp	.L1173
.L1181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3052:
	.size	_ZNK6icu_6713UnicodeString7toUTF32EPiiR10UErrorCode, .-_ZNK6icu_6713UnicodeString7toUTF32EPiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii
	.type	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii, @function
_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii:
.LFB3053:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %r11d
	testb	$1, %r11b
	jne	.L1199
	testq	%rsi, %rsi
	movl	%edx, %eax
	sete	%r10b
	shrl	$31, %eax
	orb	%al, %r10b
	jne	.L1199
	testl	%ecx, %ecx
	je	.L1199
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx,2), %rdx
	js	.L1201
.L1186:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testw	%r11w, %r11w
	js	.L1187
	movswl	%r11w, %esi
	sarl	$5, %esi
.L1188:
	testl	%r8d, %r8d
	js	.L1193
	cmpl	%r8d, %esi
	cmovle	%esi, %r8d
	movslq	%r8d, %rax
	addq	%rax, %rax
.L1189:
	xorl	%r10d, %r10d
	testl	%r9d, %r9d
	js	.L1190
	subl	%r8d, %esi
	cmpl	%r9d, %esi
	cmovle	%esi, %r9d
	movl	%r9d, %r10d
.L1190:
	andl	$2, %r11d
	leaq	10(%rdi), %rbx
	je	.L1202
.L1192:
	leaq	(%rbx,%rax), %rdi
	movl	%r10d, %esi
	call	u_strFindFirst_67@PLT
	testq	%rax, %rax
	je	.L1185
	subq	%rbx, %rax
	sarq	%rax
.L1182:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1202:
	.cfi_restore_state
	movq	24(%rdi), %rbx
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1201:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	cmpw	$0, (%rdx)
	jne	.L1186
.L1199:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1193:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1187:
	movl	12(%rdi), %esi
	jmp	.L1188
.L1185:
	movl	$-1, %eax
	jmp	.L1182
	.cfi_endproc
.LFE3053:
	.size	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii, .-_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString9doIndexOfEDsii
	.type	_ZNK6icu_6713UnicodeString9doIndexOfEDsii, @function
_ZNK6icu_6713UnicodeString9doIndexOfEDsii:
.LFB3054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzwl	8(%rdi), %r9d
	testw	%r9w, %r9w
	js	.L1204
	movswl	%r9w, %eax
	sarl	$5, %eax
	testl	%edx, %edx
	js	.L1211
.L1215:
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movslq	%edx, %r8
	addq	%r8, %r8
.L1206:
	xorl	%r10d, %r10d
	testl	%ecx, %ecx
	js	.L1207
	subl	%edx, %eax
	cmpl	%ecx, %eax
	cmovle	%eax, %ecx
	movl	%ecx, %r10d
.L1207:
	andl	$2, %r9d
	leaq	10(%rdi), %rbx
	jne	.L1209
	movq	24(%rdi), %rbx
.L1209:
	movzwl	%si, %esi
	leaq	(%rbx,%r8), %rdi
	movl	%r10d, %edx
	call	u_memchr_67@PLT
	testq	%rax, %rax
	je	.L1213
	subq	%rbx, %rax
	sarq	%rax
.L1203:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1204:
	.cfi_restore_state
	movl	12(%rdi), %eax
	testl	%edx, %edx
	jns	.L1215
.L1211:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L1206
.L1213:
	movl	$-1, %eax
	jmp	.L1203
	.cfi_endproc
.LFE3054:
	.size	_ZNK6icu_6713UnicodeString9doIndexOfEDsii, .-_ZNK6icu_6713UnicodeString9doIndexOfEDsii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString9doIndexOfEiii
	.type	_ZNK6icu_6713UnicodeString9doIndexOfEiii, @function
_ZNK6icu_6713UnicodeString9doIndexOfEiii:
.LFB3055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzwl	8(%rdi), %r9d
	testw	%r9w, %r9w
	js	.L1217
	movswl	%r9w, %eax
	sarl	$5, %eax
	testl	%edx, %edx
	js	.L1224
.L1228:
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movslq	%edx, %r8
	addq	%r8, %r8
.L1219:
	xorl	%r10d, %r10d
	testl	%ecx, %ecx
	js	.L1220
	subl	%edx, %eax
	cmpl	%ecx, %eax
	cmovle	%eax, %ecx
	movl	%ecx, %r10d
.L1220:
	andl	$2, %r9d
	leaq	10(%rdi), %rbx
	jne	.L1222
	movq	24(%rdi), %rbx
.L1222:
	leaq	(%rbx,%r8), %rdi
	movl	%r10d, %edx
	call	u_memchr32_67@PLT
	testq	%rax, %rax
	je	.L1226
	subq	%rbx, %rax
	sarq	%rax
.L1216:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1217:
	.cfi_restore_state
	movl	12(%rdi), %eax
	testl	%edx, %edx
	jns	.L1228
.L1224:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L1219
.L1226:
	movl	$-1, %eax
	jmp	.L1216
	.cfi_endproc
.LFE3055:
	.size	_ZNK6icu_6713UnicodeString9doIndexOfEiii, .-_ZNK6icu_6713UnicodeString9doIndexOfEiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString11lastIndexOfEPKDsiiii
	.type	_ZNK6icu_6713UnicodeString11lastIndexOfEPKDsiiii, @function
_ZNK6icu_6713UnicodeString11lastIndexOfEPKDsiiii:
.LFB3056:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %r11d
	testb	$1, %r11b
	jne	.L1246
	testq	%rsi, %rsi
	movl	%edx, %eax
	sete	%r10b
	shrl	$31, %eax
	orb	%al, %r10b
	jne	.L1246
	testl	%ecx, %ecx
	je	.L1246
	movslq	%edx, %rdx
	leaq	(%rsi,%rdx,2), %rdx
	js	.L1248
.L1233:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testw	%r11w, %r11w
	js	.L1234
	movswl	%r11w, %esi
	sarl	$5, %esi
.L1235:
	testl	%r8d, %r8d
	js	.L1240
	cmpl	%r8d, %esi
	cmovle	%esi, %r8d
	movslq	%r8d, %rax
	addq	%rax, %rax
.L1236:
	xorl	%r10d, %r10d
	testl	%r9d, %r9d
	js	.L1237
	subl	%r8d, %esi
	cmpl	%r9d, %esi
	cmovle	%esi, %r9d
	movl	%r9d, %r10d
.L1237:
	andl	$2, %r11d
	leaq	10(%rdi), %rbx
	je	.L1249
.L1239:
	leaq	(%rbx,%rax), %rdi
	movl	%r10d, %esi
	call	u_strFindLast_67@PLT
	testq	%rax, %rax
	je	.L1232
	subq	%rbx, %rax
	sarq	%rax
.L1229:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1249:
	.cfi_restore_state
	movq	24(%rdi), %rbx
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1248:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	cmpw	$0, (%rdx)
	jne	.L1233
.L1246:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1240:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1234:
	movl	12(%rdi), %esi
	jmp	.L1235
.L1232:
	movl	$-1, %eax
	jmp	.L1229
	.cfi_endproc
.LFE3056:
	.size	_ZNK6icu_6713UnicodeString11lastIndexOfEPKDsiiii, .-_ZNK6icu_6713UnicodeString11lastIndexOfEPKDsiiii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString13doLastIndexOfEDsii
	.type	_ZNK6icu_6713UnicodeString13doLastIndexOfEDsii, @function
_ZNK6icu_6713UnicodeString13doLastIndexOfEDsii:
.LFB3057:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %r9d
	testb	$1, %r9b
	jne	.L1268
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testw	%r9w, %r9w
	js	.L1253
	movswl	%r9w, %eax
	sarl	$5, %eax
.L1254:
	testl	%edx, %edx
	js	.L1260
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movslq	%edx, %r8
	addq	%r8, %r8
.L1255:
	xorl	%r10d, %r10d
	testl	%ecx, %ecx
	js	.L1256
	subl	%edx, %eax
	cmpl	%ecx, %eax
	cmovle	%eax, %ecx
	movl	%ecx, %r10d
.L1256:
	andl	$2, %r9d
	leaq	10(%rdi), %rbx
	je	.L1269
.L1258:
	movzwl	%si, %esi
	leaq	(%rbx,%r8), %rdi
	movl	%r10d, %edx
	call	u_memrchr_67@PLT
	testq	%rax, %rax
	je	.L1259
	subq	%rbx, %rax
	sarq	%rax
.L1250:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1269:
	.cfi_restore_state
	movq	24(%rdi), %rbx
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1260:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1253:
	movl	12(%rdi), %eax
	jmp	.L1254
.L1259:
	movl	$-1, %eax
	jmp	.L1250
.L1268:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	orl	$-1, %eax
	ret
	.cfi_endproc
.LFE3057:
	.size	_ZNK6icu_6713UnicodeString13doLastIndexOfEDsii, .-_ZNK6icu_6713UnicodeString13doLastIndexOfEDsii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString13doLastIndexOfEiii
	.type	_ZNK6icu_6713UnicodeString13doLastIndexOfEiii, @function
_ZNK6icu_6713UnicodeString13doLastIndexOfEiii:
.LFB3058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movzwl	8(%rdi), %r9d
	testw	%r9w, %r9w
	js	.L1271
	movswl	%r9w, %eax
	sarl	$5, %eax
	testl	%edx, %edx
	js	.L1278
.L1282:
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movslq	%edx, %r8
	addq	%r8, %r8
.L1273:
	xorl	%r10d, %r10d
	testl	%ecx, %ecx
	js	.L1274
	subl	%edx, %eax
	cmpl	%ecx, %eax
	cmovle	%eax, %ecx
	movl	%ecx, %r10d
.L1274:
	andl	$2, %r9d
	leaq	10(%rdi), %rbx
	jne	.L1276
	movq	24(%rdi), %rbx
.L1276:
	leaq	(%rbx,%r8), %rdi
	movl	%r10d, %edx
	call	u_memrchr32_67@PLT
	testq	%rax, %rax
	je	.L1280
	subq	%rbx, %rax
	sarq	%rax
.L1270:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1271:
	.cfi_restore_state
	movl	12(%rdi), %eax
	testl	%edx, %edx
	jns	.L1282
.L1278:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L1273
.L1280:
	movl	$-1, %eax
	jmp	.L1270
	.cfi_endproc
.LFE3058:
	.size	_ZNK6icu_6713UnicodeString13doLastIndexOfEiii, .-_ZNK6icu_6713UnicodeString13doLastIndexOfEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString10setToBogusEv
	.type	_ZN6icu_6713UnicodeString10setToBogusEv, @function
_ZN6icu_6713UnicodeString10setToBogusEv:
.LFB3060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testb	$4, 8(%rdi)
	jne	.L1288
.L1285:
	movl	$1, %eax
	movq	$0, 24(%rbx)
	movw	%ax, 8(%rbx)
	movl	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1288:
	.cfi_restore_state
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	jne	.L1285
	movq	24(%rdi), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L1285
	.cfi_endproc
.LFE3060:
	.size	_ZN6icu_6713UnicodeString10setToBogusEv, .-_ZN6icu_6713UnicodeString10setToBogusEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString7unBogusEv
	.type	_ZN6icu_6713UnicodeString7unBogusEv, @function
_ZN6icu_6713UnicodeString7unBogusEv:
.LFB3061:
	.cfi_startproc
	endbr64
	testb	$1, 8(%rdi)
	je	.L1289
	movl	$2, %eax
	movw	%ax, 8(%rdi)
.L1289:
	ret
	.cfi_endproc
.LFE3061:
	.size	_ZN6icu_6713UnicodeString7unBogusEv, .-_ZN6icu_6713UnicodeString7unBogusEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString19getTerminatedBufferEv
	.type	_ZN6icu_6713UnicodeString19getTerminatedBufferEv, @function
_ZN6icu_6713UnicodeString19getTerminatedBufferEv:
.LFB3062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzwl	8(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$17, %r13b
	jne	.L1384
	movl	%r13d, %ecx
	movq	%rdi, %rbx
	leaq	10(%rdi), %rax
	andw	$2, %cx
	jne	.L1298
	movq	24(%rdi), %rax
.L1298:
	testw	%r13w, %r13w
	js	.L1299
	movswl	%r13w, %r12d
	sarl	$5, %r12d
.L1300:
	movl	$27, %edx
	testw	%cx, %cx
	jne	.L1301
	movl	16(%rbx), %edx
.L1301:
	cmpl	%r12d, %edx
	jle	.L1302
	testb	$8, %r13b
	je	.L1303
	movslq	%r12d, %rdx
	cmpw	$0, (%rax,%rdx,2)
	je	.L1294
	leal	1(%r12), %r15d
	cmpl	$-2, %r12d
	je	.L1342
.L1381:
	movl	%r13d, %r14d
	andl	$2, %r14d
.L1312:
	testw	%r13w, %r13w
	js	.L1317
	movswl	%r13w, %r8d
	sarl	$5, %r8d
.L1318:
	testw	%r14w, %r14w
	je	.L1319
	cmpl	$27, %r15d
	jg	.L1320
	cmpl	$27, %r8d
	movl	$27, %r14d
	cmovle	%r8d, %r14d
.L1321:
	movl	$2, %eax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1330:
	sall	$5, %r14d
	orl	%r14d, %eax
	movw	%ax, 8(%rbx)
.L1332:
	andl	$4, %r13d
	jne	.L1385
.L1333:
	testb	$2, %al
	je	.L1316
.L1344:
	leaq	10(%rbx), %rax
.L1338:
	movslq	%r12d, %r12
	xorl	%edx, %edx
	movw	%dx, (%rax,%r12,2)
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	24(%rbx), %rcx
	cmpl	$27, %r15d
	jle	.L1386
	cmpl	$2147483637, %r15d
	jle	.L1339
	movl	$0, 16(%rbx)
.L1328:
	movq	%rcx, 24(%rbx)
.L1329:
	movw	%r13w, 8(%rbx)
	andl	$4, %r13d
	jne	.L1387
.L1336:
	movl	$1, %ecx
	movq	$0, 24(%rbx)
	movw	%cx, 8(%rbx)
	movl	$0, 16(%rbx)
	.p2align 4,,10
	.p2align 3
.L1384:
	xorl	%eax, %eax
.L1294:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L1388
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1299:
	.cfi_restore_state
	movl	12(%rbx), %r12d
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1302:
	cmpl	$2147483647, %r12d
	je	.L1384
	leal	1(%r12), %r15d
	cmpl	$-2, %r12d
	je	.L1342
.L1343:
	testb	$8, %r13b
	jne	.L1381
	testb	$4, %r13b
	jne	.L1389
.L1313:
	movl	%r13d, %r14d
	andw	$2, %r14w
	jne	.L1315
	cmpl	%r15d, 16(%rbx)
	jl	.L1312
.L1316:
	movq	24(%rbx), %rax
	movslq	%r12d, %r12
	xorl	%edx, %edx
	movw	%dx, (%rax,%r12,2)
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1303:
	andl	$4, %r13d
	je	.L1338
	movq	24(%rbx), %rdx
	movl	-4(%rdx), %edx
	cmpl	$1, %edx
	je	.L1338
	movzwl	8(%rbx), %r13d
	leal	1(%r12), %r15d
	movl	%r13d, %ecx
	andl	$17, %ecx
	cmpl	$-2, %r12d
	jne	.L1310
	movl	$27, %r15d
	testb	$2, %r13b
	je	.L1345
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1317:
	movl	12(%rbx), %r8d
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1342:
	movl	$27, %r15d
	testw	%cx, %cx
	jne	.L1343
.L1345:
	movl	16(%rbx), %r15d
.L1310:
	testw	%cx, %cx
	jne	.L1384
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1320:
	testl	%r8d, %r8d
	jg	.L1390
.L1322:
	cmpl	$2147483637, %r15d
	jle	.L1391
	movq	$0, 24(%rbx)
	xorl	%ecx, %ecx
	movl	$0, 16(%rbx)
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1385:
	lock subl	$1, -4(%rcx)
	je	.L1334
.L1383:
	movzwl	8(%rbx), %eax
	jmp	.L1333
.L1391:
	leaq	-112(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1339:
	addl	$1, %r15d
	movq	%rcx, -128(%rbp)
	movslq	%r15d, %r15
	movl	%r8d, -120(%rbp)
	leaq	19(%r15,%r15), %r15
	andq	$-16, %r15
	movq	%r15, %rdi
	call	uprv_malloc_67@PLT
	movq	-128(%rbp), %rcx
	testq	%rax, %rax
	je	.L1325
	movl	-120(%rbp), %r8d
	leaq	-4(%r15), %r14
	leaq	4(%rax), %rdi
	movl	$4, %esi
	shrq	%r14
	movl	$1, (%rax)
	movl	$4, %eax
	cmpl	%r14d, %r8d
	movl	%r14d, 16(%rbx)
	movq	%rdi, 24(%rbx)
	cmovl	%r8d, %r14d
	movw	%si, 8(%rbx)
	testq	%rcx, %rcx
	je	.L1327
.L1326:
	testl	%r14d, %r14d
	jle	.L1330
	movslq	%r14d, %rdx
	movq	%rcx, %rsi
	movq	%rcx, -120(%rbp)
	addq	%rdx, %rdx
	call	memmove@PLT
	movzwl	8(%rbx), %eax
	movq	-120(%rbp), %rcx
.L1327:
	cmpl	$1023, %r14d
	jle	.L1392
	orl	$-32, %eax
	movl	%r14d, 12(%rbx)
	movw	%ax, 8(%rbx)
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	24(%rbx), %rax
	movl	-4(%rax), %eax
	movzwl	8(%rbx), %r13d
	cmpl	$1, %eax
	jg	.L1381
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1390:
	movslq	%r8d, %rdx
	leaq	10(%rbx), %rsi
	leaq	-112(%rbp), %rdi
	movl	$54, %ecx
	addq	%rdx, %rdx
	movl	%r8d, -120(%rbp)
	call	__memmove_chk@PLT
	movl	-120(%rbp), %r8d
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1334:
	leaq	-4(%rcx), %rdi
	call	uprv_free_67@PLT
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1315:
	cmpl	$27, %r15d
	jle	.L1344
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1386:
	movl	$2, %edi
	cmpl	$27, %r8d
	movl	$27, %r14d
	movw	%di, 8(%rbx)
	cmovle	%r8d, %r14d
	testq	%rcx, %rcx
	je	.L1321
	leaq	10(%rbx), %rdi
	movl	$2, %eax
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1387:
	lock subl	$1, -4(%rcx)
	jne	.L1336
	movq	24(%rbx), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L1336
.L1388:
	call	__stack_chk_fail@PLT
.L1392:
	andl	$31, %eax
	jmp	.L1330
.L1325:
	movw	$1, 8(%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 16(%rbx)
	testw	%r14w, %r14w
	je	.L1328
	xorl	%ecx, %ecx
	jmp	.L1329
	.cfi_endproc
.LFE3062:
	.size	_ZN6icu_6713UnicodeString19getTerminatedBufferEv, .-_ZN6icu_6713UnicodeString19getTerminatedBufferEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi
	.type	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi, @function
_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi:
.LFB3063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movzwl	8(%rdi), %eax
	testb	$16, %al
	jne	.L1394
	movq	(%rdx), %rbx
	andl	$4, %eax
	testq	%rbx, %rbx
	je	.L1421
	cmpl	$-1, %ecx
	jl	.L1399
	jne	.L1413
	testb	%sil, %sil
	jne	.L1413
.L1399:
	testw	%ax, %ax
	jne	.L1422
.L1403:
	movq	$0, 24(%r12)
	movl	$1, %ecx
	movw	%cx, 8(%r12)
	movl	$0, 16(%r12)
.L1394:
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1413:
	.cfi_restore_state
	testl	%ecx, %ecx
	js	.L1401
	testb	%sil, %sil
	je	.L1401
	movslq	%ecx, %rdx
	cmpw	$0, (%rbx,%rdx,2)
	jne	.L1399
.L1401:
	testw	%ax, %ax
	jne	.L1423
.L1406:
	cmpl	$-1, %ecx
	je	.L1424
.L1408:
	cmpb	$1, %sil
	movl	%ecx, %eax
	sbbl	$-1, %eax
	cmpl	$1023, %ecx
	jle	.L1425
	movl	$-24, %edx
	movl	%ecx, 12(%r12)
	movw	%dx, 8(%r12)
.L1411:
	movq	%rbx, 24(%r12)
	movl	%eax, 16(%r12)
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1425:
	.cfi_restore_state
	sall	$5, %ecx
	orl	$8, %ecx
	movw	%cx, 8(%r12)
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1423:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	jne	.L1406
	movq	24(%r12), %rax
	movl	%ecx, -24(%rbp)
	movl	%esi, -20(%rbp)
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	movl	-24(%rbp), %ecx
	movl	-20(%rbp), %esi
	jmp	.L1406
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	jne	.L1403
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L1403
	.p2align 4,,10
	.p2align 3
.L1421:
	testw	%ax, %ax
	jne	.L1426
.L1397:
	movl	$2, %esi
	movw	%si, 8(%r12)
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1424:
	movq	%rbx, %rdi
	movl	%esi, -20(%rbp)
	call	u_strlen_67@PLT
	movl	-20(%rbp), %esi
	movl	%eax, %ecx
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	24(%rdi), %rax
	lock subl	$1, -4(%rax)
	jne	.L1397
	movq	24(%rdi), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L1397
	.cfi_endproc
.LFE3063:
	.size	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi, .-_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString5setToEPDsii
	.type	_ZN6icu_6713UnicodeString5setToEPDsii, @function
_ZN6icu_6713UnicodeString5setToEPDsii:
.LFB3064:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movzwl	8(%rdi), %edi
	testb	$16, %dil
	jne	.L1450
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$4, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	testq	%rsi, %rsi
	je	.L1453
	cmpl	$-1, %edx
	setl	%r9b
	cmpl	%ecx, %edx
	setg	%r8b
	orb	%r8b, %r9b
	jne	.L1447
	testl	%ecx, %ecx
	js	.L1447
	cmpl	$-1, %edx
	je	.L1454
	testw	%di, %di
	jne	.L1455
.L1442:
	cmpl	$1023, %edx
	jle	.L1456
	movl	$-32, %edi
	movl	%edx, 12(%rax)
	movw	%di, 8(%rax)
.L1445:
	movq	%rsi, 24(%rax)
	movl	%ecx, 16(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1447:
	.cfi_restore_state
	testw	%di, %di
	jne	.L1457
.L1436:
	movl	$1, %r8d
	movq	$0, 24(%rax)
	movw	%r8w, 8(%rax)
	movl	$0, 16(%rax)
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1450:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L1456:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	sall	$5, %edx
	movw	%dx, 8(%rax)
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	24(%rax), %rdx
	lock subl	$1, -4(%rdx)
	jne	.L1436
	movq	24(%rax), %rsi
	movq	%rax, -8(%rbp)
	leaq	-4(%rsi), %rdi
	call	uprv_free_67@PLT
	movq	-8(%rbp), %rax
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1454:
	movslq	%ecx, %rdx
	leaq	(%rsi,%rdx,2), %r8
	movq	%rsi, %rdx
	cmpq	%r8, %rsi
	jne	.L1440
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1458:
	addq	$2, %rdx
	cmpq	%rdx, %r8
	je	.L1439
.L1440:
	cmpw	$0, (%rdx)
	jne	.L1458
.L1439:
	subq	%rsi, %rdx
	sarq	%rdx
	testw	%di, %di
	je	.L1442
.L1455:
	movq	24(%rax), %rdi
	lock subl	$1, -4(%rdi)
	jne	.L1442
	movq	%rsi, -16(%rbp)
	movq	24(%rax), %rsi
	movl	%ecx, -24(%rbp)
	leaq	-4(%rsi), %rdi
	movl	%edx, -20(%rbp)
	movq	%rax, -8(%rbp)
	call	uprv_free_67@PLT
	movl	-24(%rbp), %ecx
	movl	-20(%rbp), %edx
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rax
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1453:
	testw	%di, %di
	jne	.L1459
.L1431:
	movl	$2, %r9d
	movw	%r9w, 8(%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1459:
	.cfi_restore_state
	movq	24(%rax), %rdx
	lock subl	$1, -4(%rdx)
	jne	.L1431
	movq	24(%rax), %rsi
	movq	%rax, -8(%rbp)
	leaq	-4(%rsi), %rdi
	call	uprv_free_67@PLT
	movq	-8(%rbp), %rax
	jmp	.L1431
	.cfi_endproc
.LFE3064:
	.size	_ZN6icu_6713UnicodeString5setToEPDsii, .-_ZN6icu_6713UnicodeString5setToEPDsii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString9setToUTF8ENS_11StringPieceE
	.type	_ZN6icu_6713UnicodeString9setToUTF8ENS_11StringPieceE, @function
_ZN6icu_6713UnicodeString9setToUTF8ENS_11StringPieceE:
.LFB3065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movzwl	8(%rdi), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, %r9b
	jne	.L1461
	leal	1(%rdx), %ebx
	cmpl	$27, %edx
	movl	$27, %eax
	movl	%edx, %r8d
	cmovle	%eax, %ebx
	testb	$17, %r9b
	je	.L1556
	xorl	%edi, %edi
	andl	$2, %r9d
	movl	$0, -116(%rbp)
	jne	.L1554
.L1466:
	movl	16(%r12), %esi
.L1467:
	leaq	-116(%rbp), %rax
	movq	%r13, %rcx
	leaq	-120(%rbp), %rdx
	movl	$65533, %r9d
	pushq	%rax
	pushq	$0
	call	u_strFromUTF8WithSub_67@PLT
	movzwl	8(%r12), %ecx
	popq	%rsi
	movl	-120(%rbp), %eax
	popq	%rdi
	testb	$16, %cl
	je	.L1496
	cmpl	$-1, %eax
	jge	.L1557
.L1496:
	movl	-116(%rbp), %edx
	testl	%edx, %edx
	jg	.L1558
.L1508:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1559
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1461:
	.cfi_restore_state
	movl	$2, %ebx
	cmpl	$27, %edx
	movl	$27, %eax
	movl	%edx, %r8d
	movw	%bx, 8(%rdi)
	leal	1(%rdx), %ebx
	movl	$2, %r9d
	cmovle	%eax, %ebx
.L1470:
	movl	%r9d, %r11d
	andw	$2, %r11w
	jne	.L1472
	cmpl	%ebx, 16(%r12)
	jl	.L1517
	movl	%r9d, %eax
	andl	$31, %eax
	orl	$16, %eax
	movw	%ax, 8(%r12)
.L1495:
	movl	$0, -116(%rbp)
	movq	24(%r12), %rdi
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1556:
	movl	%r9d, %r14d
	andl	$4, %r14d
	testb	$8, %r9b
	je	.L1468
	movl	%r9d, %r11d
	andl	$2, %r11d
.L1469:
	testw	%r9w, %r9w
	js	.L1474
.L1568:
	movswl	%r9w, %r10d
	sarl	$5, %r10d
.L1475:
	testw	%r11w, %r11w
	je	.L1476
	cmpl	$27, %ebx
	je	.L1560
	testl	%r10d, %r10d
	jle	.L1479
	movslq	%r10d, %rdx
	leaq	10(%r12), %rsi
	leaq	-112(%rbp), %rdi
	movl	$54, %ecx
	addq	%rdx, %rdx
	movl	%r11d, -144(%rbp)
	movl	%r8d, -140(%rbp)
	movl	%r9d, -136(%rbp)
	movl	%r10d, -132(%rbp)
	call	__memmove_chk@PLT
	movl	-132(%rbp), %r10d
	movl	-136(%rbp), %r9d
	movl	-140(%rbp), %r8d
	movl	-144(%rbp), %r11d
.L1479:
	cmpl	$2147483637, %ebx
	jle	.L1561
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
.L1553:
	xorl	%r15d, %r15d
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1558:
	andl	$4, %ecx
	jne	.L1562
.L1510:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movl	$0, 16(%r12)
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1476:
	movq	24(%r12), %r15
	cmpl	$27, %ebx
	jne	.L1480
	movl	$2, %r11d
	cmpl	$27, %r10d
	movw	%r11w, 8(%r12)
	cmovle	%r10d, %ebx
	testq	%r15, %r15
	je	.L1478
	leaq	10(%r12), %rdi
	movl	$2, %eax
.L1483:
	testl	%ebx, %ebx
	jg	.L1563
.L1487:
	sall	$5, %ebx
	orl	%ebx, %eax
	movw	%ax, 8(%r12)
.L1489:
	testw	%r14w, %r14w
	jne	.L1564
.L1490:
	movl	%eax, %edx
	andl	$31, %edx
	orl	$16, %edx
	movw	%dx, 8(%r12)
	testb	$2, %al
	je	.L1495
.L1516:
	movl	$0, -116(%rbp)
	leaq	10(%r12), %rdi
.L1554:
	movl	$27, %esi
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1557:
	testb	$2, %cl
	jne	.L1497
	movslq	16(%r12), %rdx
	cmpl	$-1, %eax
	je	.L1565
.L1499:
	cmpl	%edx, %eax
	cmovg	%edx, %eax
.L1505:
	cmpl	$1023, %eax
	jg	.L1506
.L1501:
	andl	$31, %ecx
	sall	$5, %eax
	orl	%eax, %ecx
.L1507:
	andl	$-17, %ecx
	movw	%cx, 8(%r12)
	jmp	.L1496
.L1468:
	testw	%r14w, %r14w
	je	.L1470
	movq	24(%rdi), %rax
	movl	-4(%rax), %eax
	movzwl	8(%rdi), %r9d
	cmpl	$1, %eax
	jle	.L1470
	movl	%r9d, %r11d
	movl	%r9d, %r14d
	andl	$2, %r11d
	andl	$4, %r14d
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1480:
	cmpl	$2147483637, %ebx
	jle	.L1513
	movl	$0, 16(%r12)
.L1485:
	movq	%r15, 24(%r12)
.L1486:
	movw	%r9w, 8(%r12)
	testw	%r14w, %r14w
	jne	.L1566
.L1493:
	movl	$1, %r9d
	movl	$0, -116(%rbp)
	xorl	%edi, %edi
	movw	%r9w, 8(%r12)
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1497:
	movl	$27, %edx
	cmpl	$-1, %eax
	jne	.L1499
	leaq	10(%r12), %rsi
	movl	$54, %edx
.L1500:
	addq	%rsi, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1524
	movq	%rsi, %rax
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1502:
	addq	$2, %rax
	cmpq	%rax, %rdx
	jbe	.L1555
.L1504:
	cmpw	$0, (%rax)
	jne	.L1502
.L1555:
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	%rdx
	movl	%edx, %eax
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1560:
	cmpl	$27, %r10d
	cmovle	%r10d, %ebx
.L1478:
	movl	$2, %eax
	xorl	%r15d, %r15d
	jmp	.L1487
	.p2align 4,,10
	.p2align 3
.L1472:
	cmpl	$27, %ebx
	je	.L1567
.L1517:
	movl	%r9d, %r14d
	andl	$4, %r14d
	testw	%r9w, %r9w
	jns	.L1568
.L1474:
	movl	12(%r12), %r10d
	jmp	.L1475
	.p2align 4,,10
	.p2align 3
.L1506:
	movl	%eax, 12(%r12)
	orl	$-32, %ecx
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	jne	.L1510
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1564:
	lock subl	$1, -4(%r15)
	je	.L1491
	movzwl	8(%r12), %eax
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1561:
	leaq	-112(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L1513:
	addl	$1, %ebx
	movl	%r11d, -144(%rbp)
	movslq	%ebx, %rbx
	movl	%r8d, -140(%rbp)
	leaq	19(%rbx,%rbx), %rbx
	movl	%r9d, -136(%rbp)
	andq	$-16, %rbx
	movl	%r10d, -132(%rbp)
	movq	%rbx, %rdi
	call	uprv_malloc_67@PLT
	movl	-136(%rbp), %r9d
	movl	-140(%rbp), %r8d
	testq	%rax, %rax
	movl	-144(%rbp), %r11d
	je	.L1482
	movl	$4, %r10d
	subq	$4, %rbx
	leaq	4(%rax), %rdi
	movl	$1, (%rax)
	movw	%r10w, 8(%r12)
	movl	-132(%rbp), %r10d
	shrq	%rbx
	movl	$4, %eax
	movl	%ebx, 16(%r12)
	cmpl	%ebx, %r10d
	movq	%rdi, 24(%r12)
	cmovl	%r10d, %ebx
	testq	%r15, %r15
	jne	.L1483
	.p2align 4,,10
	.p2align 3
.L1484:
	cmpl	$1023, %ebx
	jle	.L1569
	orl	$-32, %eax
	movl	%ebx, 12(%r12)
	movw	%ax, 8(%r12)
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1563:
	movslq	%ebx, %rdx
	movq	%r15, %rsi
	movl	%r8d, -132(%rbp)
	addq	%rdx, %rdx
	call	memmove@PLT
	movzwl	8(%r12), %eax
	movl	-132(%rbp), %r8d
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	24(%r12), %rsi
	addq	%rdx, %rdx
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1567:
	movl	%r9d, %eax
	andl	$31, %eax
	orl	$16, %eax
	movw	%ax, 8(%r12)
	jmp	.L1516
	.p2align 4,,10
	.p2align 3
.L1566:
	lock subl	$1, -4(%r15)
	jne	.L1493
	movq	24(%r12), %rax
	movl	%r8d, -132(%rbp)
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	movl	-132(%rbp), %r8d
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1491:
	leaq	-4(%r15), %rdi
	movl	%r8d, -132(%rbp)
	call	uprv_free_67@PLT
	movzwl	8(%r12), %eax
	movl	-132(%rbp), %r8d
	jmp	.L1490
.L1524:
	xorl	%eax, %eax
	jmp	.L1501
.L1559:
	call	__stack_chk_fail@PLT
.L1569:
	andl	$31, %eax
	jmp	.L1487
.L1482:
	movw	$1, 8(%r12)
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
	testw	%r11w, %r11w
	jne	.L1553
	jmp	.L1485
	.cfi_endproc
.LFE3065:
	.size	_ZN6icu_6713UnicodeString9setToUTF8ENS_11StringPieceE, .-_ZN6icu_6713UnicodeString9setToUTF8ENS_11StringPieceE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString9setCharAtEiDs
	.type	_ZN6icu_6713UnicodeString9setCharAtEiDs, @function
_ZN6icu_6713UnicodeString9setCharAtEiDs:
.LFB3066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movzwl	8(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testw	%bx, %bx
	js	.L1571
	movswl	%bx, %r8d
	sarl	$5, %r8d
.L1572:
	movl	%ebx, %r11d
	movl	$27, %r13d
	andw	$2, %r11w
	jne	.L1573
	movl	16(%r12), %r13d
.L1573:
	testb	$17, %bl
	jne	.L1575
	movl	%ebx, %r10d
	andl	$4, %r10d
	testb	$8, %bl
	jne	.L1576
	testw	%r10w, %r10w
	jne	.L1631
.L1577:
	movl	$27, %eax
	testw	%r11w, %r11w
	jne	.L1579
	movl	16(%r12), %eax
.L1579:
	cmpl	%eax, %r13d
	jg	.L1629
.L1580:
	testl	%r8d, %r8d
	jle	.L1575
	xorl	%esi, %esi
	testl	%r9d, %r9d
	js	.L1603
	cmpl	%r8d, %r9d
	jge	.L1604
	movslq	%r9d, %rsi
	addq	%rsi, %rsi
.L1603:
	leaq	10(%r12), %rax
	testb	$2, 8(%r12)
	jne	.L1606
	movq	24(%r12), %rax
.L1606:
	movw	%r15w, (%rax,%rsi)
.L1575:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1632
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1631:
	.cfi_restore_state
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	movzwl	8(%r12), %ebx
	movl	%ebx, %r11d
	andl	$2, %r11d
	cmpl	$1, %eax
	jle	.L1577
.L1629:
	movl	%ebx, %r10d
	andl	$4, %r10d
.L1576:
	testw	%bx, %bx
	js	.L1581
	movswl	%bx, %r14d
	sarl	$5, %r14d
	testw	%r11w, %r11w
	je	.L1583
.L1635:
	cmpl	$27, %r13d
	jle	.L1633
	testl	%r14d, %r14d
	jle	.L1586
	movslq	%r14d, %rdx
	leaq	10(%r12), %rsi
	leaq	-112(%rbp), %rdi
	movl	%r10d, -128(%rbp)
	addq	%rdx, %rdx
	movl	$54, %ecx
	movl	%r9d, -132(%rbp)
	movl	%r11d, -124(%rbp)
	movl	%r8d, -120(%rbp)
	call	__memmove_chk@PLT
	movl	-120(%rbp), %r8d
	movl	-124(%rbp), %r11d
	movl	-128(%rbp), %r10d
	movl	-132(%rbp), %r9d
.L1586:
	cmpl	$2147483637, %r13d
	jle	.L1634
	movq	$0, 24(%r12)
	xorl	%ecx, %ecx
	movl	$0, 16(%r12)
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1581:
	movl	12(%r12), %r14d
	testw	%r11w, %r11w
	jne	.L1635
.L1583:
	movq	24(%r12), %rcx
	cmpl	$27, %r13d
	jle	.L1636
	cmpl	$2147483637, %r13d
	jle	.L1607
	movl	$0, 16(%r12)
.L1592:
	movq	%rcx, 24(%r12)
.L1593:
	movw	%bx, 8(%r12)
	testw	%r10w, %r10w
	jne	.L1637
.L1600:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movl	$0, 16(%r12)
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1571:
	movl	12(%rdi), %r8d
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1633:
	cmpl	$27, %r14d
	movl	$27, %ebx
	cmovle	%r14d, %ebx
.L1585:
	movl	$2, %eax
	xorl	%ecx, %ecx
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1634:
	leaq	-112(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1607:
	addl	$1, %r13d
	movq	%rcx, -144(%rbp)
	movslq	%r13d, %r13
	movl	%r10d, -132(%rbp)
	leaq	19(%r13,%r13), %r13
	movl	%r11d, -128(%rbp)
	andq	$-16, %r13
	movl	%r9d, -124(%rbp)
	movq	%r13, %rdi
	movl	%r8d, -120(%rbp)
	call	uprv_malloc_67@PLT
	movl	-128(%rbp), %r11d
	movl	-132(%rbp), %r10d
	testq	%rax, %rax
	movq	-144(%rbp), %rcx
	je	.L1589
	leaq	-4(%r13), %rbx
	leaq	4(%rax), %rdi
	movl	-120(%rbp), %r8d
	movl	-124(%rbp), %r9d
	shrq	%rbx
	movl	$4, %edx
	movl	$1, (%rax)
	movl	$4, %eax
	cmpl	%ebx, %r14d
	movl	%ebx, 16(%r12)
	cmovl	%r14d, %ebx
	testq	%rcx, %rcx
	movq	%rdi, 24(%r12)
	movw	%dx, 8(%r12)
	je	.L1591
.L1590:
	testl	%ebx, %ebx
	jg	.L1638
.L1594:
	sall	$5, %ebx
	orl	%ebx, %eax
	movw	%ax, 8(%r12)
.L1596:
	testw	%r10w, %r10w
	je	.L1580
	lock subl	$1, -4(%rcx)
	jne	.L1580
	leaq	-4(%rcx), %rdi
	movl	%r9d, -124(%rbp)
	movl	%r8d, -120(%rbp)
	call	uprv_free_67@PLT
	movl	-120(%rbp), %r8d
	movl	-124(%rbp), %r9d
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1638:
	movslq	%ebx, %rdx
	movq	%rcx, %rsi
	movl	%r9d, -132(%rbp)
	addq	%rdx, %rdx
	movl	%r10d, -128(%rbp)
	movl	%r8d, -124(%rbp)
	movq	%rcx, -120(%rbp)
	call	memmove@PLT
	movzwl	8(%r12), %eax
	movl	-132(%rbp), %r9d
	movl	-128(%rbp), %r10d
	movl	-124(%rbp), %r8d
	movq	-120(%rbp), %rcx
.L1591:
	cmpl	$1023, %ebx
	jle	.L1639
	orl	$-32, %eax
	movl	%ebx, 12(%r12)
	movw	%ax, 8(%r12)
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1604:
	leal	-1(%r8), %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1636:
	movl	$2, %esi
	cmpl	$27, %r14d
	movl	$27, %ebx
	movw	%si, 8(%r12)
	cmovle	%r14d, %ebx
	testq	%rcx, %rcx
	je	.L1585
	leaq	10(%r12), %rdi
	movl	$2, %eax
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1637:
	lock subl	$1, -4(%rcx)
	jne	.L1600
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L1600
.L1632:
	call	__stack_chk_fail@PLT
.L1589:
	movw	$1, 8(%r12)
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
	testw	%r11w, %r11w
	je	.L1592
	xorl	%ecx, %ecx
	jmp	.L1593
.L1639:
	andl	$31, %eax
	jmp	.L1594
	.cfi_endproc
.LFE3066:
	.size	_ZN6icu_6713UnicodeString9setCharAtEiDs, .-_ZN6icu_6713UnicodeString9setCharAtEiDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString9doReverseEii
	.type	_ZN6icu_6713UnicodeString9doReverseEii, @function
_ZN6icu_6713UnicodeString9doReverseEii:
.LFB3077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	jle	.L1669
	movzwl	8(%rdi), %ebx
	movl	%esi, %r14d
	movl	%edx, %r13d
	movl	$27, %r11d
	movl	%ebx, %r10d
	andw	$2, %r10w
	jne	.L1642
	movl	16(%rdi), %r11d
.L1642:
	testb	$17, %bl
	jne	.L1669
	movl	%ebx, %r8d
	andl	$4, %r8d
	testb	$8, %bl
	jne	.L1643
	testw	%r8w, %r8w
	jne	.L1711
.L1644:
	movl	$27, %eax
	testw	%r10w, %r10w
	jne	.L1646
	movl	16(%r12), %eax
.L1646:
	cmpl	%eax, %r11d
	jle	.L1647
.L1708:
	movl	%ebx, %r8d
	andl	$4, %r8d
.L1643:
	testw	%bx, %bx
	js	.L1648
	movswl	%bx, %r15d
	sarl	$5, %r15d
	movl	%r15d, %r9d
	testw	%r10w, %r10w
	je	.L1650
.L1714:
	cmpl	$27, %r11d
	jg	.L1651
	cmpl	$27, %r9d
	movl	$27, %r15d
	cmovle	%r9d, %r15d
.L1652:
	movl	$2, %ebx
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1661:
	sall	$5, %r15d
	orl	%r15d, %ebx
	movw	%bx, 8(%r12)
.L1663:
	testw	%r8w, %r8w
	jne	.L1712
.L1647:
	testw	%bx, %bx
	js	.L1670
	movswl	%bx, %r8d
	sarl	$5, %r8d
.L1671:
	xorl	%eax, %eax
	testl	%r14d, %r14d
	js	.L1672
	cmpl	%r14d, %r8d
	movl	%r14d, %eax
	cmovle	%r8d, %eax
	subl	%eax, %r8d
.L1672:
	cmpl	%r8d, %r13d
	cmovle	%r13d, %r8d
	cmpl	$1, %r8d
	jle	.L1669
	andl	$2, %ebx
	leaq	10(%r12), %rbx
	jne	.L1674
	movq	24(%r12), %rbx
.L1674:
	cltq
	movslq	%r8d, %r8
	xorl	%r9d, %r9d
	leaq	(%rax,%rax), %r10
	addq	%r8, %r8
	addq	%r10, %rbx
	leaq	-2(%r8), %r11
	leaq	(%rbx,%r11), %r13
	movq	%rbx, %rax
	movq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L1675:
	movzwl	(%rax), %edi
	movzwl	(%rdx), %ecx
	addq	$2, %rax
	movl	%edi, %esi
	movw	%cx, -2(%rax)
	andl	$64512, %esi
	cmpl	$55296, %esi
	sete	%sil
	andl	$64512, %ecx
	cmpl	$55296, %ecx
	sete	%cl
	subq	$2, %rdx
	orl	%esi, %ecx
	movw	%di, 2(%rdx)
	orl	%ecx, %r9d
	cmpq	%rdx, %rax
	jb	.L1675
	subq	$3, %r8
	leaq	-1(%rbx), %rdx
	subq	$2, %r13
	shrq	$2, %r8
	cmpq	%rdx, %r13
	movl	$2, %edx
	leaq	2(%r8,%r8), %rax
	cmovb	%rdx, %rax
	movzwl	(%rbx,%rax), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	sete	%al
	orb	%r9b, %al
	je	.L1669
	leaq	10(%r12), %rax
	testb	$2, 8(%r12)
	jne	.L1677
	movq	24(%r12), %rax
.L1677:
	addq	%r10, %rax
	addq	%rax, %r11
	cmpq	%r11, %rax
	jb	.L1678
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1679:
	addq	$2, %rax
	cmpq	%rax, %r11
	jbe	.L1669
.L1678:
	movzwl	(%rax), %edx
	movl	%edx, %ecx
	andl	$64512, %ecx
	cmpl	$56320, %ecx
	jne	.L1679
	movzwl	2(%rax), %ecx
	movl	%ecx, %esi
	andl	$64512, %esi
	cmpl	$55296, %esi
	jne	.L1679
	movw	%cx, (%rax)
	addq	$4, %rax
	movw	%dx, -2(%rax)
	cmpq	%rax, %r11
	ja	.L1678
	.p2align 4,,10
	.p2align 3
.L1669:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1713
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1648:
	.cfi_restore_state
	movl	12(%r12), %r9d
	testw	%r10w, %r10w
	jne	.L1714
.L1650:
	movq	24(%r12), %rcx
	cmpl	$27, %r11d
	jle	.L1715
	cmpl	$2147483637, %r11d
	jle	.L1682
	movl	$0, 16(%r12)
.L1659:
	movq	%rcx, 24(%r12)
.L1660:
	movw	%bx, 8(%r12)
	testw	%r8w, %r8w
	jne	.L1716
.L1667:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movl	$0, 16(%r12)
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1670:
	movl	12(%r12), %r8d
	jmp	.L1671
	.p2align 4,,10
	.p2align 3
.L1651:
	testl	%r9d, %r9d
	jg	.L1717
.L1653:
	cmpl	$2147483637, %r11d
	jle	.L1718
	movq	$0, 24(%r12)
	xorl	%ecx, %ecx
	movl	$0, 16(%r12)
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1712:
	lock subl	$1, -4(%rcx)
	je	.L1665
.L1710:
	movzwl	8(%r12), %ebx
	jmp	.L1647
.L1718:
	leaq	-112(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1682:
	addl	$1, %r11d
	movl	%r8d, -136(%rbp)
	movslq	%r11d, %r11
	movl	%r10d, -132(%rbp)
	leaq	19(%r11,%r11), %r15
	movq	%rcx, -128(%rbp)
	andq	$-16, %r15
	movl	%r9d, -120(%rbp)
	movq	%r15, %rdi
	call	uprv_malloc_67@PLT
	movq	-128(%rbp), %rcx
	movl	-132(%rbp), %r10d
	testq	%rax, %rax
	movl	-136(%rbp), %r8d
	je	.L1656
	movl	-120(%rbp), %r9d
	subq	$4, %r15
	leaq	4(%rax), %rdi
	movl	$4, %edx
	shrq	%r15
	movq	%rdi, 24(%r12)
	movl	$4, %ebx
	cmpl	%r15d, %r9d
	movl	%r15d, 16(%r12)
	movl	$1, (%rax)
	cmovl	%r9d, %r15d
	movw	%dx, 8(%r12)
	testq	%rcx, %rcx
	je	.L1658
.L1657:
	testl	%r15d, %r15d
	jle	.L1661
	movslq	%r15d, %rdx
	movq	%rcx, %rsi
	movl	%r8d, -128(%rbp)
	addq	%rdx, %rdx
	movq	%rcx, -120(%rbp)
	call	memmove@PLT
	movzwl	8(%r12), %ebx
	movl	-128(%rbp), %r8d
	movq	-120(%rbp), %rcx
.L1658:
	cmpl	$1023, %r15d
	jle	.L1719
	orl	$-32, %ebx
	movl	%r15d, 12(%r12)
	movw	%bx, 8(%r12)
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	movzwl	8(%r12), %ebx
	movl	%ebx, %r10d
	andl	$2, %r10d
	cmpl	$1, %eax
	jg	.L1708
	jmp	.L1644
	.p2align 4,,10
	.p2align 3
.L1717:
	movslq	%r9d, %rdx
	leaq	10(%r12), %rsi
	leaq	-112(%rbp), %rdi
	movl	%r10d, -128(%rbp)
	addq	%rdx, %rdx
	movl	$54, %ecx
	movl	%r9d, -120(%rbp)
	movl	%r8d, -136(%rbp)
	movl	%r11d, -132(%rbp)
	call	__memmove_chk@PLT
	movl	-120(%rbp), %r9d
	movl	-128(%rbp), %r10d
	movl	-132(%rbp), %r11d
	movl	-136(%rbp), %r8d
	jmp	.L1653
.L1665:
	leaq	-4(%rcx), %rdi
	call	uprv_free_67@PLT
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L1715:
	movl	$2, %esi
	cmpl	$27, %r9d
	movl	$27, %r15d
	movw	%si, 8(%r12)
	cmovle	%r9d, %r15d
	testq	%rcx, %rcx
	je	.L1652
	leaq	10(%r12), %rdi
	movl	$2, %ebx
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1716:
	lock subl	$1, -4(%rcx)
	jne	.L1667
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L1667
.L1713:
	call	__stack_chk_fail@PLT
.L1719:
	andl	$31, %ebx
	jmp	.L1661
.L1656:
	movw	$1, 8(%r12)
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
	testw	%r10w, %r10w
	je	.L1659
	xorl	%ecx, %ecx
	jmp	.L1660
	.cfi_endproc
.LFE3077:
	.size	_ZN6icu_6713UnicodeString9doReverseEii, .-_ZN6icu_6713UnicodeString9doReverseEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString10padLeadingEiDs
	.type	_ZN6icu_6713UnicodeString10padLeadingEiDs, @function
_ZN6icu_6713UnicodeString10padLeadingEiDs:
.LFB3078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movzwl	8(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testw	%r13w, %r13w
	js	.L1721
	movswl	%r13w, %r15d
	sarl	$5, %r15d
.L1722:
	xorl	%eax, %eax
	cmpl	%r15d, %r12d
	jg	.L1806
.L1720:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L1807
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1806:
	.cfi_restore_state
	movl	%r12d, %r8d
	cmpl	$-1, %r12d
	je	.L1808
.L1724:
	xorl	%eax, %eax
	testb	$17, %r13b
	jne	.L1720
	movl	%r13d, %r10d
	andl	$4, %r10d
	testb	$8, %r13b
	je	.L1725
	movl	%r13d, %r11d
	andl	$2, %r11d
.L1726:
	testw	%r13w, %r13w
	js	.L1731
	movswl	%r13w, %r9d
	sarl	$5, %r9d
.L1732:
	testw	%r11w, %r11w
	je	.L1733
	cmpl	$27, %r8d
	jg	.L1734
	cmpl	$27, %r9d
	movl	$27, %r13d
	cmovle	%r9d, %r13d
.L1735:
	movl	$2, %eax
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1744:
	sall	$5, %r13d
	orl	%r13d, %eax
	movw	%ax, 8(%rbx)
.L1746:
	testw	%r10w, %r10w
	jne	.L1809
.L1747:
	testb	$2, %al
	je	.L1730
.L1762:
	leaq	10(%rbx), %r13
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1721:
	movl	12(%rdi), %r15d
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1725:
	testw	%r10w, %r10w
	jne	.L1810
.L1727:
	movl	%r13d, %r11d
	andw	$2, %r11w
	jne	.L1729
	cmpl	%r8d, 16(%rbx)
	jl	.L1761
.L1730:
	movq	24(%rbx), %r13
.L1752:
	movl	%r12d, %r8d
	subl	%r15d, %r8d
	testl	%r15d, %r15d
	jle	.L1753
	movslq	%r8d, %rax
	movslq	%r15d, %rdx
	movq	%r13, %rsi
	movl	%r8d, -120(%rbp)
	addq	%rdx, %rdx
	leaq	0(%r13,%rax,2), %rdi
	call	memmove@PLT
	movl	-120(%rbp), %r8d
.L1753:
	leal	-1(%r8), %edx
	cmpl	$6, %edx
	jbe	.L1754
	movslq	%edx, %rax
	movl	%r8d, %ecx
	movd	%r14d, %xmm0
	leaq	-14(%r13,%rax,2), %rax
	shrl	$3, %ecx
	punpcklwd	%xmm0, %xmm0
	salq	$4, %rcx
	movq	%rax, %rdi
	pshufd	$0, %xmm0, %xmm0
	subq	%rcx, %rdi
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L1755:
	movups	%xmm0, (%rax)
	subq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L1755
	movl	%r8d, %eax
	andl	$-8, %eax
	subl	%eax, %edx
	cmpl	%r8d, %eax
	je	.L1756
.L1754:
	movslq	%edx, %rax
	movw	%r14w, 0(%r13,%rax,2)
	leal	-1(%rdx), %eax
	testl	%edx, %edx
	je	.L1756
	cltq
	movw	%r14w, 0(%r13,%rax,2)
	leal	-2(%rdx), %eax
	cmpl	$1, %edx
	je	.L1756
	cltq
	movw	%r14w, 0(%r13,%rax,2)
	leal	-3(%rdx), %eax
	cmpl	$2, %edx
	je	.L1756
	cltq
	movw	%r14w, 0(%r13,%rax,2)
	leal	-4(%rdx), %eax
	cmpl	$3, %edx
	je	.L1756
	cltq
	movw	%r14w, 0(%r13,%rax,2)
	leal	-5(%rdx), %eax
	cmpl	$4, %edx
	je	.L1756
	cltq
	movw	%r14w, 0(%r13,%rax,2)
	leal	-6(%rdx), %eax
	cmpl	$5, %edx
	je	.L1756
	cltq
	movw	%r14w, 0(%r13,%rax,2)
.L1756:
	movzwl	8(%rbx), %eax
	cmpl	$1023, %r12d
	jg	.L1757
	andl	$31, %eax
	sall	$5, %r12d
	orl	%r12d, %eax
	movw	%ax, 8(%rbx)
	movl	$1, %eax
	jmp	.L1720
	.p2align 4,,10
	.p2align 3
.L1729:
	cmpl	$27, %r8d
	jle	.L1762
.L1761:
	movl	%r13d, %r10d
	andl	$4, %r10d
	jmp	.L1726
	.p2align 4,,10
	.p2align 3
.L1808:
	movl	$27, %r8d
	testb	$2, %r13b
	jne	.L1724
	movl	16(%rbx), %r8d
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	24(%rbx), %rcx
	cmpl	$27, %r8d
	jle	.L1811
	cmpl	$2147483637, %r8d
	jle	.L1758
	movl	$0, 16(%rbx)
.L1742:
	movq	%rcx, 24(%rbx)
.L1743:
	movw	%r13w, 8(%rbx)
	testw	%r10w, %r10w
	jne	.L1812
.L1750:
	movl	$1, %eax
	movq	$0, 24(%rbx)
	movw	%ax, 8(%rbx)
	xorl	%eax, %eax
	movl	$0, 16(%rbx)
	jmp	.L1720
	.p2align 4,,10
	.p2align 3
.L1757:
	orl	$-32, %eax
	movl	%r12d, 12(%rbx)
	movw	%ax, 8(%rbx)
	movl	$1, %eax
	jmp	.L1720
	.p2align 4,,10
	.p2align 3
.L1731:
	movl	12(%rbx), %r9d
	jmp	.L1732
	.p2align 4,,10
	.p2align 3
.L1734:
	testl	%r9d, %r9d
	jg	.L1813
.L1736:
	cmpl	$2147483637, %r8d
	jle	.L1814
	movq	$0, 24(%rbx)
	xorl	%ecx, %ecx
	movl	$0, 16(%rbx)
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1809:
	lock subl	$1, -4(%rcx)
	je	.L1748
.L1805:
	movzwl	8(%rbx), %eax
	jmp	.L1747
.L1814:
	leaq	-112(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1758:
	addl	$1, %r8d
	movq	%rcx, -144(%rbp)
	movslq	%r8d, %r8
	movl	%r11d, -132(%rbp)
	leaq	19(%r8,%r8), %rdx
	movl	%r10d, -128(%rbp)
	andq	$-16, %rdx
	movl	%r9d, -124(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -120(%rbp)
	call	uprv_malloc_67@PLT
	movl	-128(%rbp), %r10d
	movl	-132(%rbp), %r11d
	testq	%rax, %rax
	movq	-144(%rbp), %rcx
	je	.L1739
	movq	-120(%rbp), %rdx
	movl	-124(%rbp), %r9d
	leaq	4(%rax), %rdi
	movl	$1, (%rax)
	movq	%rdi, 24(%rbx)
	movl	$4, %eax
	leaq	-4(%rdx), %r13
	movl	$4, %edx
	shrq	%r13
	movw	%dx, 8(%rbx)
	cmpl	%r13d, %r9d
	movl	%r13d, 16(%rbx)
	cmovl	%r9d, %r13d
	testq	%rcx, %rcx
	je	.L1741
.L1740:
	testl	%r13d, %r13d
	jle	.L1744
	movslq	%r13d, %rdx
	movq	%rcx, %rsi
	movl	%r10d, -124(%rbp)
	addq	%rdx, %rdx
	movq	%rcx, -120(%rbp)
	call	memmove@PLT
	movzwl	8(%rbx), %eax
	movl	-124(%rbp), %r10d
	movq	-120(%rbp), %rcx
.L1741:
	cmpl	$1023, %r13d
	jle	.L1815
	orl	$-32, %eax
	movl	%r13d, 12(%rbx)
	movw	%ax, 8(%rbx)
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1810:
	movq	24(%rbx), %rax
	movl	-4(%rax), %eax
	movzwl	8(%rbx), %r13d
	cmpl	$1, %eax
	jle	.L1727
	movl	%r13d, %r11d
	movl	%r13d, %r10d
	andl	$2, %r11d
	andl	$4, %r10d
	jmp	.L1726
	.p2align 4,,10
	.p2align 3
.L1813:
	movslq	%r9d, %rdx
	leaq	10(%rbx), %rsi
	leaq	-112(%rbp), %rdi
	movl	$54, %ecx
	addq	%rdx, %rdx
	movl	%r11d, -132(%rbp)
	movl	%r8d, -128(%rbp)
	movl	%r10d, -124(%rbp)
	movl	%r9d, -120(%rbp)
	call	__memmove_chk@PLT
	movl	-120(%rbp), %r9d
	movl	-124(%rbp), %r10d
	movl	-128(%rbp), %r8d
	movl	-132(%rbp), %r11d
	jmp	.L1736
.L1748:
	leaq	-4(%rcx), %rdi
	call	uprv_free_67@PLT
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1811:
	movl	$2, %esi
	cmpl	$27, %r9d
	movl	$27, %r13d
	movw	%si, 8(%rbx)
	cmovle	%r9d, %r13d
	testq	%rcx, %rcx
	je	.L1735
	leaq	10(%rbx), %rdi
	movl	$2, %eax
	jmp	.L1740
	.p2align 4,,10
	.p2align 3
.L1812:
	lock subl	$1, -4(%rcx)
	jne	.L1750
	movq	24(%rbx), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L1750
.L1807:
	call	__stack_chk_fail@PLT
.L1815:
	andl	$31, %eax
	jmp	.L1744
.L1739:
	movw	$1, 8(%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 16(%rbx)
	testw	%r11w, %r11w
	je	.L1742
	xorl	%ecx, %ecx
	jmp	.L1743
	.cfi_endproc
.LFE3078:
	.size	_ZN6icu_6713UnicodeString10padLeadingEiDs, .-_ZN6icu_6713UnicodeString10padLeadingEiDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString11padTrailingEiDs
	.type	_ZN6icu_6713UnicodeString11padTrailingEiDs, @function
_ZN6icu_6713UnicodeString11padTrailingEiDs:
.LFB3079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movzwl	8(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testw	%bx, %bx
	js	.L1817
	movswl	%bx, %r14d
	sarl	$5, %r14d
.L1818:
	xorl	%eax, %eax
	cmpl	%r14d, %r13d
	jg	.L1884
.L1816:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1885
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1884:
	.cfi_restore_state
	movl	%r13d, %r8d
	cmpl	$-1, %r13d
	je	.L1886
.L1820:
	xorl	%eax, %eax
	testb	$17, %bl
	jne	.L1816
	movl	%ebx, %r10d
	andl	$4, %r10d
	testb	$8, %bl
	je	.L1821
	movl	%ebx, %r11d
	andl	$2, %r11d
.L1822:
	testw	%bx, %bx
	js	.L1827
	movswl	%bx, %ecx
	sarl	$5, %ecx
	movl	%ecx, %r9d
.L1828:
	testw	%r11w, %r11w
	je	.L1829
	cmpl	$27, %r8d
	jg	.L1830
	cmpl	$27, %r9d
	movl	$27, %ecx
	cmovle	%r9d, %ecx
.L1831:
	movl	$2, %ebx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L1840:
	sall	$5, %ecx
	orl	%ecx, %ebx
	movw	%bx, 8(%r12)
.L1842:
	testw	%r10w, %r10w
	jne	.L1887
.L1843:
	testb	$2, %bl
	je	.L1826
.L1858:
	leaq	10(%r12), %rcx
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1817:
	movl	12(%rdi), %r14d
	jmp	.L1818
	.p2align 4,,10
	.p2align 3
.L1821:
	testw	%r10w, %r10w
	jne	.L1888
.L1823:
	movl	%ebx, %r11d
	andw	$2, %r11w
	jne	.L1825
	cmpl	%r8d, 16(%r12)
	jl	.L1857
.L1826:
	movq	24(%r12), %rcx
.L1848:
	leal	-1(%r13), %edx
	cmpl	%edx, %r14d
	jg	.L1849
	movl	%edx, %eax
	movl	%r13d, %edi
	subl	%r14d, %eax
	subl	%r14d, %edi
	cmpl	$6, %eax
	jbe	.L1850
	movslq	%edx, %rax
	movl	%edi, %esi
	movd	%r15d, %xmm0
	leaq	-14(%rcx,%rax,2), %rax
	shrl	$3, %esi
	punpcklwd	%xmm0, %xmm0
	salq	$4, %rsi
	movq	%rax, %rbx
	pshufd	$0, %xmm0, %xmm0
	subq	%rsi, %rbx
	movq	%rbx, %rsi
	.p2align 4,,10
	.p2align 3
.L1851:
	movups	%xmm0, (%rax)
	subq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L1851
	movl	%edi, %eax
	andl	$-8, %eax
	subl	%eax, %edx
	cmpl	%edi, %eax
	je	.L1852
.L1850:
	movslq	%edx, %rax
	movw	%r15w, (%rcx,%rax,2)
	leal	-1(%rdx), %eax
	cmpl	%eax, %r14d
	jg	.L1852
	cltq
	movw	%r15w, (%rcx,%rax,2)
	leal	-2(%rdx), %eax
	cmpl	%eax, %r14d
	jg	.L1852
	cltq
	movw	%r15w, (%rcx,%rax,2)
	leal	-3(%rdx), %eax
	cmpl	%r14d, %eax
	jl	.L1852
	cltq
	movw	%r15w, (%rcx,%rax,2)
	leal	-4(%rdx), %eax
	cmpl	%eax, %r14d
	jg	.L1852
	cltq
	movw	%r15w, (%rcx,%rax,2)
	leal	-5(%rdx), %eax
	cmpl	%eax, %r14d
	jg	.L1852
	cltq
	subl	$6, %edx
	movw	%r15w, (%rcx,%rax,2)
	cmpl	%edx, %r14d
	jg	.L1852
	movslq	%edx, %rdx
	movw	%r15w, (%rcx,%rdx,2)
.L1852:
	movzwl	8(%r12), %ebx
.L1849:
	cmpl	$1023, %r13d
	jg	.L1853
	andl	$31, %ebx
	sall	$5, %r13d
	movl	$1, %eax
	orl	%r13d, %ebx
	movw	%bx, 8(%r12)
	jmp	.L1816
	.p2align 4,,10
	.p2align 3
.L1825:
	cmpl	$27, %r8d
	jle	.L1858
.L1857:
	movl	%ebx, %r10d
	andl	$4, %r10d
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1886:
	movl	$27, %r8d
	testb	$2, %bl
	jne	.L1820
	movl	16(%r12), %r8d
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1829:
	movq	24(%r12), %rsi
	cmpl	$27, %r8d
	jle	.L1889
	cmpl	$2147483637, %r8d
	jle	.L1854
	movl	$0, 16(%r12)
.L1838:
	movq	%rsi, 24(%r12)
.L1839:
	movw	%bx, 8(%r12)
	testw	%r10w, %r10w
	jne	.L1890
.L1846:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	xorl	%eax, %eax
	movl	$0, 16(%r12)
	jmp	.L1816
	.p2align 4,,10
	.p2align 3
.L1853:
	orl	$-32, %ebx
	movl	%r13d, 12(%r12)
	movl	$1, %eax
	movw	%bx, 8(%r12)
	jmp	.L1816
	.p2align 4,,10
	.p2align 3
.L1827:
	movl	12(%r12), %r9d
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L1830:
	testl	%r9d, %r9d
	jg	.L1891
.L1832:
	cmpl	$2147483637, %r8d
	jle	.L1892
	movq	$0, 24(%r12)
	xorl	%esi, %esi
	movl	$0, 16(%r12)
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1887:
	lock subl	$1, -4(%rsi)
	je	.L1844
.L1883:
	movzwl	8(%r12), %ebx
	jmp	.L1843
.L1892:
	leaq	-112(%rbp), %rsi
	.p2align 4,,10
	.p2align 3
.L1854:
	addl	$1, %r8d
	movl	%r10d, -140(%rbp)
	movslq	%r8d, %r8
	movq	%rsi, -136(%rbp)
	leaq	19(%r8,%r8), %rcx
	movl	%r11d, -128(%rbp)
	andq	$-16, %rcx
	movl	%r9d, -124(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -120(%rbp)
	call	uprv_malloc_67@PLT
	movl	-128(%rbp), %r11d
	movq	-136(%rbp), %rsi
	testq	%rax, %rax
	movl	-140(%rbp), %r10d
	je	.L1835
	movq	-120(%rbp), %rcx
	movl	-124(%rbp), %r9d
	leaq	4(%rax), %rdi
	movl	$4, %edx
	movl	$1, (%rax)
	movl	$4, %ebx
	subq	$4, %rcx
	movq	%rdi, 24(%r12)
	shrq	%rcx
	movw	%dx, 8(%r12)
	cmpl	%ecx, %r9d
	movl	%ecx, 16(%r12)
	cmovl	%r9d, %ecx
	testq	%rsi, %rsi
	je	.L1837
.L1836:
	testl	%ecx, %ecx
	jle	.L1840
	movslq	%ecx, %rdx
	movl	%r10d, -128(%rbp)
	addq	%rdx, %rdx
	movl	%ecx, -124(%rbp)
	movq	%rsi, -120(%rbp)
	call	memmove@PLT
	movzwl	8(%r12), %ebx
	movl	-128(%rbp), %r10d
	movl	-124(%rbp), %ecx
	movq	-120(%rbp), %rsi
.L1837:
	cmpl	$1023, %ecx
	jle	.L1893
	orl	$-32, %ebx
	movl	%ecx, 12(%r12)
	movw	%bx, 8(%r12)
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1888:
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	movzwl	8(%r12), %ebx
	cmpl	$1, %eax
	jle	.L1823
	movl	%ebx, %r11d
	movl	%ebx, %r10d
	andl	$2, %r11d
	andl	$4, %r10d
	jmp	.L1822
	.p2align 4,,10
	.p2align 3
.L1891:
	movslq	%r9d, %rdx
	leaq	10(%r12), %rsi
	leaq	-112(%rbp), %rdi
	movl	%r11d, -128(%rbp)
	addq	%rdx, %rdx
	movl	$54, %ecx
	movl	%r10d, -136(%rbp)
	movl	%r8d, -124(%rbp)
	movl	%r9d, -120(%rbp)
	call	__memmove_chk@PLT
	movl	-120(%rbp), %r9d
	movl	-124(%rbp), %r8d
	movl	-128(%rbp), %r11d
	movl	-136(%rbp), %r10d
	jmp	.L1832
.L1844:
	leaq	-4(%rsi), %rdi
	call	uprv_free_67@PLT
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1889:
	movl	$2, %ecx
	cmpl	$27, %r9d
	movw	%cx, 8(%r12)
	movl	$27, %ecx
	cmovle	%r9d, %ecx
	testq	%rsi, %rsi
	je	.L1831
	leaq	10(%r12), %rdi
	movl	$2, %ebx
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1890:
	lock subl	$1, -4(%rsi)
	jne	.L1846
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L1846
.L1885:
	call	__stack_chk_fail@PLT
.L1893:
	andl	$31, %ebx
	jmp	.L1840
.L1835:
	movw	$1, 8(%r12)
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
	testw	%r11w, %r11w
	je	.L1838
	xorl	%esi, %esi
	jmp	.L1839
	.cfi_endproc
.LFE3079:
	.size	_ZN6icu_6713UnicodeString11padTrailingEiDs, .-_ZN6icu_6713UnicodeString11padTrailingEiDs
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString10doHashCodeEv
	.type	_ZNK6icu_6713UnicodeString10doHashCodeEv, @function
_ZNK6icu_6713UnicodeString10doHashCodeEv:
.LFB3080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	8(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testw	%ax, %ax
	js	.L1895
	movswl	%ax, %esi
	sarl	$5, %esi
.L1896:
	testb	$2, %al
	je	.L1897
	addq	$10, %rdi
.L1898:
	call	ustr_hashUCharsN_67@PLT
	movl	$1, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1897:
	.cfi_restore_state
	movq	24(%rdi), %rdi
	jmp	.L1898
	.p2align 4,,10
	.p2align 3
.L1895:
	movl	12(%rdi), %esi
	jmp	.L1896
	.cfi_endproc
.LFE3080:
	.size	_ZNK6icu_6713UnicodeString10doHashCodeEv, .-_ZNK6icu_6713UnicodeString10doHashCodeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString9getBufferEi
	.type	_ZN6icu_6713UnicodeString9getBufferEi, @function
_ZN6icu_6713UnicodeString9getBufferEi:
.LFB3081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	jl	.L1901
	movzwl	8(%rdi), %r8d
	movq	%rdi, %rbx
	movl	%esi, %r12d
	je	.L1962
.L1903:
	xorl	%eax, %eax
	testb	$17, %r8b
	jne	.L1901
	movl	%r8d, %r13d
	andl	$4, %r13d
	testb	$8, %r8b
	je	.L1904
	movl	%r8d, %r9d
	andl	$2, %r9d
.L1905:
	testw	%r8w, %r8w
	js	.L1910
.L1971:
	movswl	%r8w, %r14d
	sarl	$5, %r14d
.L1911:
	testw	%r9w, %r9w
	je	.L1912
	cmpl	$27, %r12d
	jle	.L1963
	testl	%r14d, %r14d
	jle	.L1915
	movslq	%r14d, %rdx
	leaq	10(%rbx), %rsi
	leaq	-112(%rbp), %rdi
	movl	$54, %ecx
	addq	%rdx, %rdx
	movl	%r8d, -120(%rbp)
	movl	%r9d, -116(%rbp)
	call	__memmove_chk@PLT
	movl	-116(%rbp), %r9d
	movl	-120(%rbp), %r8d
.L1915:
	cmpl	$2147483637, %r12d
	jle	.L1964
	movq	$0, 24(%rbx)
	movl	$0, 16(%rbx)
.L1960:
	xorl	%r15d, %r15d
	jmp	.L1922
	.p2align 4,,10
	.p2align 3
.L1912:
	movq	24(%rbx), %r15
	cmpl	$27, %r12d
	jg	.L1916
	movl	$2, %ecx
	cmpl	$27, %r14d
	movl	$27, %r12d
	movw	%cx, 8(%rbx)
	cmovle	%r14d, %r12d
	testq	%r15, %r15
	je	.L1914
	leaq	10(%rbx), %rdi
	movl	$2, %eax
.L1919:
	testl	%r12d, %r12d
	jg	.L1965
.L1923:
	sall	$5, %r12d
	orl	%r12d, %eax
	movw	%ax, 8(%rbx)
.L1925:
	testw	%r13w, %r13w
	jne	.L1966
.L1926:
	movl	%eax, %edx
	andl	$31, %edx
	orl	$16, %edx
	movw	%dx, 8(%rbx)
	testb	$2, %al
	je	.L1931
.L1935:
	leaq	10(%rbx), %rax
.L1901:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1967
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1904:
	.cfi_restore_state
	testw	%r13w, %r13w
	jne	.L1968
.L1906:
	movl	%r8d, %r9d
	andw	$2, %r9w
	jne	.L1908
	cmpl	%r12d, 16(%rbx)
	jl	.L1936
	movl	%r8d, %eax
	andl	$31, %eax
	orl	$16, %eax
	movw	%ax, 8(%rbx)
.L1931:
	movq	24(%rbx), %rax
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1962:
	movl	$27, %r12d
	testb	$2, %r8b
	jne	.L1903
	movl	16(%rdi), %r12d
	jmp	.L1903
	.p2align 4,,10
	.p2align 3
.L1916:
	cmpl	$2147483637, %r12d
	jle	.L1932
	movl	$0, 16(%rbx)
.L1921:
	movq	%r15, 24(%rbx)
.L1922:
	movw	%r8w, 8(%rbx)
	testw	%r13w, %r13w
	jne	.L1969
.L1929:
	movl	$1, %eax
	movq	$0, 24(%rbx)
	movw	%ax, 8(%rbx)
	xorl	%eax, %eax
	movl	$0, 16(%rbx)
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1963:
	cmpl	$27, %r14d
	movl	$27, %eax
	cmovle	%r14d, %eax
	movl	%eax, %r12d
.L1914:
	movl	$2, %eax
	xorl	%r15d, %r15d
	jmp	.L1923
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	24(%rbx), %rax
	movl	-4(%rax), %eax
	movzwl	8(%rbx), %r8d
	cmpl	$1, %eax
	jle	.L1906
	movl	%r8d, %r9d
	movl	%r8d, %r13d
	andl	$2, %r9d
	andl	$4, %r13d
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L1908:
	cmpl	$27, %r12d
	jle	.L1970
.L1936:
	movl	%r8d, %r13d
	andl	$4, %r13d
	testw	%r8w, %r8w
	jns	.L1971
.L1910:
	movl	12(%rbx), %r14d
	jmp	.L1911
	.p2align 4,,10
	.p2align 3
.L1966:
	lock subl	$1, -4(%r15)
	jne	.L1961
	leaq	-4(%r15), %rdi
	call	uprv_free_67@PLT
.L1961:
	movzwl	8(%rbx), %eax
	jmp	.L1926
	.p2align 4,,10
	.p2align 3
.L1964:
	leaq	-112(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L1932:
	addl	$1, %r12d
	movl	%r8d, -120(%rbp)
	movslq	%r12d, %r12
	movl	%r9d, -116(%rbp)
	leaq	19(%r12,%r12), %r12
	andq	$-16, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movl	-116(%rbp), %r9d
	movl	-120(%rbp), %r8d
	testq	%rax, %rax
	je	.L1918
	subq	$4, %r12
	leaq	4(%rax), %rdi
	movl	$4, %edx
	movl	$1, (%rax)
	shrq	%r12
	movq	%rdi, 24(%rbx)
	movl	$4, %eax
	cmpl	%r12d, %r14d
	movl	%r12d, 16(%rbx)
	movw	%dx, 8(%rbx)
	cmovl	%r14d, %r12d
	testq	%r15, %r15
	jne	.L1919
	.p2align 4,,10
	.p2align 3
.L1920:
	cmpl	$1023, %r12d
	jle	.L1972
	orl	$-32, %eax
	movl	%r12d, 12(%rbx)
	movw	%ax, 8(%rbx)
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L1965:
	movslq	%r12d, %rdx
	movq	%r15, %rsi
	addq	%rdx, %rdx
	call	memmove@PLT
	movzwl	8(%rbx), %eax
	jmp	.L1920
	.p2align 4,,10
	.p2align 3
.L1970:
	movl	%r8d, %eax
	andl	$31, %eax
	orl	$16, %eax
	movw	%ax, 8(%rbx)
	jmp	.L1935
	.p2align 4,,10
	.p2align 3
.L1969:
	lock subl	$1, -4(%r15)
	jne	.L1929
	movq	24(%rbx), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L1929
.L1967:
	call	__stack_chk_fail@PLT
.L1972:
	andl	$31, %eax
	jmp	.L1923
.L1918:
	movw	$1, 8(%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 16(%rbx)
	testw	%r9w, %r9w
	jne	.L1960
	jmp	.L1921
	.cfi_endproc
.LFE3081:
	.size	_ZN6icu_6713UnicodeString9getBufferEi, .-_ZN6icu_6713UnicodeString9getBufferEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString13releaseBufferEi
	.type	_ZN6icu_6713UnicodeString13releaseBufferEi, @function
_ZN6icu_6713UnicodeString13releaseBufferEi:
.LFB3082:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %edx
	testb	$16, %dl
	je	.L1973
	cmpl	$-1, %esi
	jge	.L1996
.L1973:
	ret
	.p2align 4,,10
	.p2align 3
.L1996:
	testb	$2, %dl
	jne	.L1975
	movl	16(%rdi), %eax
	cmpl	$-1, %esi
	je	.L1997
.L1977:
	cmpl	%eax, %esi
	cmovg	%eax, %esi
.L1983:
	cmpl	$1023, %esi
	jg	.L1984
.L1979:
	andl	$31, %edx
	sall	$5, %esi
	orl	%esi, %edx
	andl	$-17, %edx
	movw	%dx, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1975:
	movl	$27, %eax
	cmpl	$-1, %esi
	jne	.L1977
	leaq	10(%rdi), %rsi
	movl	$54, %ecx
.L1978:
	addq	%rsi, %rcx
	cmpq	%rsi, %rcx
	jbe	.L1987
	movq	%rsi, %rax
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L1980:
	addq	$2, %rax
	cmpq	%rax, %rcx
	jbe	.L1995
.L1982:
	cmpw	$0, (%rax)
	jne	.L1980
.L1995:
	subq	%rsi, %rax
	sarq	%rax
	movl	%eax, %esi
	jmp	.L1983
	.p2align 4,,10
	.p2align 3
.L1984:
	orl	$-32, %edx
	movl	%esi, 12(%rdi)
	andl	$-17, %edx
	movw	%dx, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1997:
	movslq	%eax, %rcx
	movq	24(%rdi), %rsi
	addq	%rcx, %rcx
	jmp	.L1978
.L1987:
	xorl	%esi, %esi
	jmp	.L1979
	.cfi_endproc
.LFE3082:
	.size	_ZN6icu_6713UnicodeString13releaseBufferEi, .-_ZN6icu_6713UnicodeString13releaseBufferEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia
	.type	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia, @function
_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia:
.LFB3083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%r8, -120(%rbp)
	movzwl	8(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %esi
	je	.L2071
.L1999:
	xorl	%eax, %eax
	testb	$17, %r14b
	jne	.L1998
	movl	%r14d, %r10d
	andl	$4, %r10d
	testb	%r9b, %r9b
	je	.L2001
.L2067:
	movl	%r14d, %r11d
	andl	$2, %r11d
.L2002:
	testl	%r12d, %r12d
	js	.L2041
	cmpl	$27, %r13d
	jg	.L2007
	cmpl	$27, %r12d
	movl	$27, %eax
	cmovg	%eax, %r12d
.L2007:
	testw	%r14w, %r14w
	js	.L2009
	movswl	%r14w, %r8d
	sarl	$5, %r8d
.L2010:
	testw	%r11w, %r11w
	je	.L2011
	testb	%r15b, %r15b
	je	.L2043
	cmpl	$27, %r12d
	jle	.L2043
	leaq	-112(%rbp), %r9
	testl	%r8d, %r8d
	jle	.L2013
	movslq	%r8d, %rdx
	movq	%r9, %rdi
	leaq	10(%rbx), %rsi
	movl	$54, %ecx
	addq	%rdx, %rdx
	movl	%r11d, -140(%rbp)
	movl	%r10d, -136(%rbp)
	movl	%r8d, -128(%rbp)
	call	__memmove_chk@PLT
	movl	-128(%rbp), %r8d
	movl	-136(%rbp), %r10d
	movl	-140(%rbp), %r11d
	movq	%rax, %r9
	jmp	.L2013
	.p2align 4,,10
	.p2align 3
.L2075:
	leal	1(%r12), %eax
	movl	%r11d, -148(%rbp)
	cltq
	movl	%r10d, -144(%rbp)
	leaq	19(%rax,%rax), %rdx
	movl	%r8d, -140(%rbp)
	andq	$-16, %rdx
	movq	%r9, -136(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -128(%rbp)
	call	uprv_malloc_67@PLT
	movq	-136(%rbp), %r9
	movl	-140(%rbp), %r8d
	testq	%rax, %rax
	movl	-144(%rbp), %r10d
	movl	-148(%rbp), %r11d
	je	.L2016
	movq	-128(%rbp), %rdx
	movl	$1, (%rax)
	movl	$4, %edi
	addq	$4, %rax
	movq	%rax, 24(%rbx)
	subq	$4, %rdx
	movw	%di, 8(%rbx)
	shrq	%rdx
	movl	%edx, 16(%rbx)
	testb	%r15b, %r15b
	jne	.L2017
.L2068:
	movl	$4, %eax
.L2015:
	movw	%ax, 8(%rbx)
.L2028:
	testw	%r10w, %r10w
	jne	.L2072
.L2031:
	movl	$1, %eax
.L1998:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2073
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2001:
	.cfi_restore_state
	testb	$8, %r14b
	jne	.L2067
	testw	%r10w, %r10w
	jne	.L2074
.L2004:
	movl	%r14d, %r11d
	movl	$27, %edx
	andw	$2, %r11w
	jne	.L2006
	movl	16(%rbx), %edx
.L2006:
	movl	$1, %eax
	cmpl	%edx, %r13d
	jle	.L1998
	movl	%r14d, %r10d
	andl	$4, %r10d
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2071:
	movl	$27, %r13d
	testb	$2, %r14b
	jne	.L1999
	movl	16(%rdi), %r13d
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L2011:
	movq	24(%rbx), %r9
.L2012:
	cmpl	$27, %r12d
	jle	.L2070
.L2013:
	cmpl	$2147483637, %r12d
	jle	.L2075
.L2016:
	movl	$1, %esi
	movq	$0, 24(%rbx)
	movw	%si, 8(%rbx)
	movl	$0, 16(%rbx)
	cmpl	%r12d, %r13d
	jge	.L2018
	cmpl	$27, %r13d
	jle	.L2070
	cmpl	$2147483637, %r13d
	jle	.L2076
.L2020:
	movq	$0, 24(%rbx)
	movl	$0, 16(%rbx)
.L2018:
	testw	%r11w, %r11w
	je	.L2021
.L2022:
	movw	%r14w, 8(%rbx)
	testw	%r10w, %r10w
	jne	.L2077
.L2033:
	movl	$1, %eax
	movq	$0, 24(%rbx)
	movw	%ax, 8(%rbx)
	xorl	%eax, %eax
	movl	$0, 16(%rbx)
	jmp	.L1998
	.p2align 4,,10
	.p2align 3
.L2070:
	movl	$2, %ecx
	movl	$2, %eax
	movw	%cx, 8(%rbx)
	testb	%r15b, %r15b
	je	.L2015
	cmpl	$27, %r8d
	movl	$27, %r12d
	cmovle	%r8d, %r12d
	testq	%r9, %r9
	je	.L2078
	leaq	10(%rbx), %rdi
	movl	$2, %eax
.L2025:
	testl	%r12d, %r12d
	jg	.L2079
.L2026:
	movl	%r12d, %r8d
	sall	$5, %r8d
	orl	%r8d, %eax
	movw	%ax, 8(%rbx)
	testw	%r10w, %r10w
	je	.L2031
.L2072:
	lock subl	$1, -4(%r9)
	jne	.L2031
	cmpq	$0, -120(%rbp)
	leaq	-4(%r9), %rdi
	je	.L2080
	movq	-120(%rbp), %rax
	movq	%rdi, (%rax)
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2009:
	movl	12(%rbx), %r8d
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2041:
	movl	%r13d, %r12d
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L2043:
	xorl	%r9d, %r9d
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2076:
	addl	$1, %r13d
	movl	%r11d, -144(%rbp)
	movslq	%r13d, %r13
	movl	%r10d, -140(%rbp)
	leaq	19(%r13,%r13), %r12
	movq	%r9, -128(%rbp)
	andq	$-16, %r12
	movl	%r8d, -136(%rbp)
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	-128(%rbp), %r9
	movl	-140(%rbp), %r10d
	testq	%rax, %rax
	movl	-144(%rbp), %r11d
	je	.L2020
	subq	$4, %r12
	movl	$1, (%rax)
	addq	$4, %rax
	movl	$4, %edx
	shrq	%r12
	testb	%r15b, %r15b
	movq	%rax, 24(%rbx)
	movl	-136(%rbp), %r8d
	movl	%r12d, 16(%rbx)
	movw	%dx, 8(%rbx)
	je	.L2068
	.p2align 4,,10
	.p2align 3
.L2017:
	cmpl	%r8d, 16(%rbx)
	cmovle	16(%rbx), %r8d
	movl	$4, %eax
	movl	%r8d, %r12d
	testq	%r9, %r9
	je	.L2024
	movq	24(%rbx), %rdi
	movl	$4, %eax
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2079:
	movslq	%r12d, %rdx
	movq	%r9, %rsi
	movl	%r10d, -136(%rbp)
	addq	%rdx, %rdx
	movq	%r9, -128(%rbp)
	call	memmove@PLT
	movzwl	8(%rbx), %eax
	movl	-136(%rbp), %r10d
	movq	-128(%rbp), %r9
.L2024:
	cmpl	$1023, %r12d
	jle	.L2081
	orl	$-32, %eax
	movl	%r12d, 12(%rbx)
	movw	%ax, 8(%rbx)
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2074:
	movq	24(%rbx), %rax
	movl	-4(%rax), %eax
	movzwl	8(%rbx), %r14d
	cmpl	$1, %eax
	jle	.L2004
	movl	%r14d, %r11d
	movl	%r14d, %r10d
	andl	$2, %r11d
	andl	$4, %r10d
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2021:
	movq	%r9, 24(%rbx)
	jmp	.L2022
.L2080:
	call	uprv_free_67@PLT
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2077:
	movq	24(%rbx), %rax
	lock subl	$1, -4(%rax)
	jne	.L2033
	movq	24(%rbx), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2081:
	andl	$31, %eax
	jmp	.L2026
.L2073:
	call	__stack_chk_fail@PLT
.L2078:
	movl	$2, %eax
	jmp	.L2026
	.cfi_endproc
.LFE3083:
	.size	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia, .-_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia
	.align 2
	.p2align 4
	.type	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0, @function
_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0:
.LFB3786:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	(%rsi,%rdx,2), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	js	.L2159
.L2083:
	movswl	8(%r12), %ebx
	testw	%bx, %bx
	js	.L2085
	sarl	$5, %ebx
.L2086:
	leaq	-132(%rbp), %rdx
	movl	%r13d, %esi
	movl	%ebx, %edi
	call	uprv_add32_overflow_67@PLT
	testb	%al, %al
	jne	.L2160
	movzwl	8(%r12), %eax
	movl	%eax, %edx
	andw	$2, %dx
	je	.L2091
	leaq	10(%r12), %rcx
	testb	$25, %al
	jne	.L2156
.L2092:
	testb	$4, %al
	jne	.L2161
.L2095:
	movslq	%r13d, %rax
	leaq	(%r14,%rax,2), %rax
	cmpq	%rax, %rcx
	jnb	.L2099
	movslq	%ebx, %rax
	leaq	(%rcx,%rax,2), %rax
	cmpq	%rax, %r14
	jnb	.L2099
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	movl	$2, %edx
	testl	%r13d, %r13d
	movq	%rbx, -128(%rbp)
	sete	%r8b
	movw	%dx, -120(%rbp)
	testq	%r14, %r14
	je	.L2100
	testb	%r8b, %r8b
	jne	.L2100
	leaq	-128(%rbp), %r15
	xorl	%edx, %edx
	movl	%r13d, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movb	%r8b, -152(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movzwl	-120(%rbp), %edx
	movzbl	-152(%rbp), %r8d
	testb	$1, %dl
	jne	.L2162
	testb	$2, %dl
	jne	.L2105
	movq	-104(%rbp), %rsi
	movq	%r12, %rax
	testb	$17, 8(%r12)
	jne	.L2104
.L2118:
	testq	%rsi, %rsi
	je	.L2122
	testb	%r8b, %r8b
	jne	.L2122
.L2119:
	xorl	%edx, %edx
	movl	%r13d, %ecx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movzwl	-120(%rbp), %edx
.L2104:
	andl	$4, %edx
	movq	%rbx, -128(%rbp)
	jne	.L2163
.L2107:
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%rax, -128(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	-152(%rbp), %rax
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2091:
	movq	24(%r12), %rcx
	testb	$25, %al
	je	.L2092
.L2094:
	movl	16(%r12), %ecx
	movl	-132(%rbp), %esi
	xorl	%edx, %edx
	cmpl	%ecx, %esi
	jle	.L2164
.L2109:
	movl	$2147483637, %ecx
	movl	%esi, %eax
	sarl	$2, %eax
	movl	%ecx, %edi
	subl	$-128, %eax
	subl	%esi, %edi
	cmpl	%edi, %eax
	leal	(%rax,%rsi), %edx
	movq	%r12, %rdi
	cmovg	%ecx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia
	testb	%al, %al
	je	.L2158
.L2157:
	movzwl	8(%r12), %eax
	movl	%eax, %edx
	andl	$2, %edx
.L2110:
	testw	%dx, %dx
	jne	.L2165
	movq	24(%r12), %rdx
.L2115:
	movslq	%ebx, %rbx
	leaq	(%rdx,%rbx,2), %rdi
	cmpq	%rdi, %r14
	je	.L2116
	testl	%r13d, %r13d
	jle	.L2116
	movslq	%r13d, %rdx
	movq	%r14, %rsi
	addq	%rdx, %rdx
	call	memmove@PLT
	movzwl	8(%r12), %eax
.L2116:
	movl	-132(%rbp), %edx
	cmpl	$1023, %edx
	jg	.L2117
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 8(%r12)
.L2158:
	movq	%r12, %rax
.L2082:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2166
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2161:
	.cfi_restore_state
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	cmpl	$1, %eax
	je	.L2095
.L2099:
	movzwl	8(%r12), %eax
	movl	%eax, %edx
	andl	$2, %edx
	testw	%dx, %dx
	je	.L2094
.L2156:
	movl	-132(%rbp), %esi
	movl	$27, %ecx
	cmpl	%ecx, %esi
	jg	.L2109
.L2164:
	testb	$25, %al
	jne	.L2109
	testb	$4, %al
	je	.L2110
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	movl	-132(%rbp), %esi
	cmpl	$1, %eax
	jne	.L2109
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2085:
	movl	12(%r12), %ebx
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2159:
	movq	%r14, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L2083
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2165:
	leaq	10(%r12), %rdx
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2117:
	orl	$-32, %eax
	movl	%edx, 12(%r12)
	movw	%ax, 8(%r12)
	movq	%r12, %rax
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2160:
	testb	$4, 8(%r12)
	jne	.L2167
.L2089:
	movl	$1, %ecx
	movq	%r12, %rax
	movq	$0, 24(%r12)
	movw	%cx, 8(%r12)
	movl	$0, 16(%r12)
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2167:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	jne	.L2089
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2100:
	testb	$17, 8(%r12)
	jne	.L2124
	movl	$2, %edx
	leaq	-118(%rbp), %rsi
	leaq	-128(%rbp), %r15
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	-104(%rbp), %rdx
	lock subl	$1, -4(%rdx)
	jne	.L2107
	movq	%rax, -152(%rbp)
	movq	-104(%rbp), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	movq	-152(%rbp), %rax
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L2162:
	testb	$4, 8(%r12)
	jne	.L2168
.L2102:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movq	%r12, %rax
	movl	$0, 16(%r12)
	jmp	.L2104
	.p2align 4,,10
	.p2align 3
.L2122:
	movq	%r12, %rax
	jmp	.L2104
.L2124:
	movq	%r12, %rax
	leaq	-128(%rbp), %r15
	jmp	.L2107
.L2168:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	je	.L2103
.L2155:
	movzwl	-120(%rbp), %edx
	jmp	.L2102
.L2105:
	movq	%r12, %rax
	leaq	-118(%rbp), %rsi
	testb	$17, 8(%r12)
	je	.L2119
	jmp	.L2104
.L2103:
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2155
.L2166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3786:
	.size	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0, .-_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString8doAppendEPKDsii
	.type	_ZN6icu_6713UnicodeString8doAppendEPKDsii, @function
_ZN6icu_6713UnicodeString8doAppendEPKDsii:
.LFB3072:
	.cfi_startproc
	endbr64
	testb	$17, 8(%rdi)
	jne	.L2170
	testl	%ecx, %ecx
	je	.L2170
	testq	%rsi, %rsi
	je	.L2170
	jmp	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	.p2align 4,,10
	.p2align 3
.L2170:
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE3072:
	.size	_ZN6icu_6713UnicodeString8doAppendEPKDsii, .-_ZN6icu_6713UnicodeString8doAppendEPKDsii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString8doAppendERKS0_ii
	.type	_ZN6icu_6713UnicodeString8doAppendERKS0_ii, @function
_ZN6icu_6713UnicodeString8doAppendERKS0_ii:
.LFB3071:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	testl	%ecx, %ecx
	je	.L2172
	movzwl	8(%rsi), %r8d
	testw	%r8w, %r8w
	js	.L2173
	movswl	%r8w, %edi
	sarl	$5, %edi
.L2174:
	xorl	%r10d, %r10d
	testl	%edx, %edx
	js	.L2175
	cmpl	%edi, %edx
	cmovg	%edi, %edx
	movl	%edx, %r10d
.L2175:
	xorl	%r9d, %r9d
	testl	%ecx, %ecx
	js	.L2176
	subl	%r10d, %edi
	cmpl	%ecx, %edi
	cmovle	%edi, %ecx
	movl	%ecx, %r9d
.L2176:
	andl	$2, %r8d
	je	.L2177
	addq	$10, %rsi
.L2178:
	testb	$17, 8(%rax)
	jne	.L2172
	testl	%r9d, %r9d
	je	.L2172
	testq	%rsi, %rsi
	je	.L2172
	movl	%r9d, %ecx
	movl	%r10d, %edx
	movq	%rax, %rdi
	jmp	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	.p2align 4,,10
	.p2align 3
.L2172:
	ret
	.p2align 4,,10
	.p2align 3
.L2173:
	movl	12(%rsi), %edi
	jmp	.L2174
	.p2align 4,,10
	.p2align 3
.L2177:
	movq	24(%rsi), %rsi
	jmp	.L2178
	.cfi_endproc
.LFE3071:
	.size	_ZN6icu_6713UnicodeString8doAppendERKS0_ii, .-_ZN6icu_6713UnicodeString8doAppendERKS0_ii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString6appendEi
	.type	_ZN6icu_6713UnicodeString6appendEi, @function
_ZN6icu_6713UnicodeString6appendEi:
.LFB3068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$65535, %esi
	ja	.L2185
	movw	%si, -12(%rbp)
	movl	$1, %ecx
.L2186:
	testb	$17, 8(%rdi)
	jne	.L2189
	leaq	-12(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	jmp	.L2184
	.p2align 4,,10
	.p2align 3
.L2185:
	cmpl	$1114111, %esi
	jbe	.L2187
.L2189:
	movq	%rdi, %rax
.L2184:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2192
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2187:
	.cfi_restore_state
	movl	%esi, %eax
	andw	$1023, %si
	movl	$2, %ecx
	sarl	$10, %eax
	orw	$-9216, %si
	subw	$10304, %ax
	movw	%si, -10(%rbp)
	movw	%ax, -12(%rbp)
	jmp	.L2186
.L2192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3068:
	.size	_ZN6icu_6713UnicodeString6appendEi, .-_ZN6icu_6713UnicodeString6appendEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UnicodeStringAppendable15appendCodePointEi
	.type	_ZN6icu_6723UnicodeStringAppendable15appendCodePointEi, @function
_ZN6icu_6723UnicodeStringAppendable15appendCodePointEi:
.LFB3089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$65535, %esi
	ja	.L2194
	movw	%si, -12(%rbp)
	movl	$1, %ecx
.L2195:
	movq	8(%rdi), %rdi
	movzwl	8(%rdi), %eax
	andw	$17, %ax
	jne	.L2197
	leaq	-12(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movzwl	8(%rax), %eax
	andl	$17, %eax
.L2197:
	testw	%ax, %ax
	sete	%al
.L2193:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2201
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2194:
	.cfi_restore_state
	xorl	%eax, %eax
	cmpl	$1114111, %esi
	ja	.L2193
	movl	%esi, %eax
	andw	$1023, %si
	movl	$2, %ecx
	sarl	$10, %eax
	orw	$-9216, %si
	subw	$10304, %ax
	movw	%si, -10(%rbp)
	movw	%ax, -12(%rbp)
	jmp	.L2195
.L2201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3089:
	.size	_ZN6icu_6723UnicodeStringAppendable15appendCodePointEi, .-_ZN6icu_6723UnicodeStringAppendable15appendCodePointEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EPKDsi
	.type	_ZN6icu_6713UnicodeStringC2EPKDsi, @function
_ZN6icu_6713UnicodeStringC2EPKDsi:
.LFB2985:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	%edx, %ecx
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	testl	%edx, %edx
	je	.L2202
	testq	%rsi, %rsi
	je	.L2202
	xorl	%edx, %edx
	jmp	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	.p2align 4,,10
	.p2align 3
.L2202:
	ret
	.cfi_endproc
.LFE2985:
	.size	_ZN6icu_6713UnicodeStringC2EPKDsi, .-_ZN6icu_6713UnicodeStringC2EPKDsi
	.globl	_ZN6icu_6713UnicodeStringC1EPKDsi
	.set	_ZN6icu_6713UnicodeStringC1EPKDsi,_ZN6icu_6713UnicodeStringC2EPKDsi
	.p2align 4
	.globl	_ZN6icu_67plERKNS_13UnicodeStringES2_
	.type	_ZN6icu_67plERKNS_13UnicodeStringES2_, @function
_ZN6icu_67plERKNS_13UnicodeStringES2_:
.LFB2967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movzwl	8(%rsi), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testw	%si, %si
	js	.L2205
	movswl	%si, %eax
	sarl	$5, %eax
	movl	%eax, %edx
.L2206:
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L2207
	sarl	$5, %eax
.L2208:
	addl	%edx, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %r14
	xorl	%r10d, %r10d
	leal	1(%rax), %edx
	movq	%r14, -128(%rbp)
	movw	%r10w, -120(%rbp)
	cmpl	$27, %edx
	jle	.L2284
	cmpl	$2147483637, %edx
	jle	.L2285
.L2211:
	movq	$0, -104(%rbp)
	movl	$1, %edi
	movl	$0, -112(%rbp)
	movw	%di, -120(%rbp)
	movl	$1, %edi
.L2210:
	testw	%si, %si
	js	.L2213
	movswl	%si, %ecx
	sarl	$5, %ecx
	je	.L2256
	xorl	%edx, %edx
.L2215:
	movl	%ecx, %eax
	subl	%edx, %eax
	cmpl	%ecx, %eax
	cmovle	%eax, %ecx
.L2216:
	andl	$2, %esi
	leaq	10(%r13), %rsi
	jne	.L2218
	movq	24(%r13), %rsi
.L2218:
	testw	%di, %di
	jne	.L2256
	leaq	-128(%rbp), %r15
	testl	%ecx, %ecx
	je	.L2257
	testq	%rsi, %rsi
	je	.L2257
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movq	%rax, %r13
.L2214:
	movzwl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L2219
.L2289:
	movswl	%ax, %ecx
	sarl	$5, %ecx
	je	.L2220
	xorl	%r8d, %r8d
.L2221:
	movl	%ecx, %edx
	subl	%r8d, %edx
	cmpl	%ecx, %edx
	cmovle	%edx, %ecx
.L2222:
	leaq	10(%rbx), %rsi
	testb	$2, %al
	jne	.L2224
	movq	24(%rbx), %rsi
.L2224:
	testb	$17, 8(%r13)
	jne	.L2220
	testl	%ecx, %ecx
	je	.L2220
	testq	%rsi, %rsi
	je	.L2220
	movq	%r13, %rdi
	movl	%r8d, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movq	%rax, %r13
.L2220:
	movl	$2, %esi
	movq	%r14, (%r12)
	movw	%si, 8(%r12)
	cmpq	%r13, %r12
	je	.L2226
	movzwl	8(%r13), %eax
	testb	$1, %al
	jne	.L2233
	movswl	%ax, %edx
	sarl	$5, %edx
	je	.L2226
	movw	%ax, 8(%r12)
	movswl	8(%r13), %ecx
	movl	%ecx, %eax
	andl	$31, %eax
	cmpw	$4, %ax
	je	.L2229
	jg	.L2230
	testw	%ax, %ax
	je	.L2231
	cmpw	$2, %ax
	jne	.L2233
	addl	%edx, %edx
	leaq	10(%r12), %rcx
	leaq	10(%r13), %rsi
	movslq	%edx, %rdx
	cmpq	$8, %rdx
	jnb	.L2234
	testb	$4, %dl
	jne	.L2286
	testq	%rdx, %rdx
	je	.L2226
	movzbl	(%rsi), %eax
	movb	%al, 10(%r12)
	testb	$2, %dl
	je	.L2226
	movzwl	-2(%rsi,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	.p2align 4,,10
	.p2align 3
.L2226:
	movq	%r14, -128(%rbp)
	testb	$4, -120(%rbp)
	jne	.L2287
.L2250:
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2288
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2256:
	.cfi_restore_state
	movzwl	8(%rbx), %eax
	leaq	-128(%rbp), %r15
	movq	%r15, %r13
	testw	%ax, %ax
	jns	.L2289
.L2219:
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L2220
	movl	$0, %r8d
	cmovle	%ecx, %r8d
	jns	.L2221
	xorl	%ecx, %ecx
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2207:
	movl	12(%rbx), %eax
	jmp	.L2208
	.p2align 4,,10
	.p2align 3
.L2205:
	movl	12(%r13), %edx
	jmp	.L2206
	.p2align 4,,10
	.p2align 3
.L2213:
	movl	12(%r13), %ecx
	testl	%ecx, %ecx
	je	.L2256
	movl	$0, %edx
	cmovle	%ecx, %edx
	jns	.L2215
	xorl	%ecx, %ecx
	jmp	.L2216
	.p2align 4,,10
	.p2align 3
.L2233:
	movl	$1, %eax
	movq	%r14, -128(%rbp)
	movw	%ax, 8(%r12)
	movq	$0, 24(%r12)
	movl	$0, 16(%r12)
	testb	$4, -120(%rbp)
	je	.L2250
.L2287:
	movq	-104(%rbp), %rax
	lock subl	$1, -4(%rax)
	jne	.L2250
	movq	-104(%rbp), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2250
	.p2align 4,,10
	.p2align 3
.L2230:
	cmpw	$8, %ax
	jne	.L2233
.L2231:
	testw	%cx, %cx
	js	.L2238
	movl	%ecx, %ebx
	sarl	$5, %ebx
	cmpl	$895, %ecx
	jg	.L2240
.L2239:
	movl	$2, %ecx
	movl	$2, %eax
	movw	%cx, 8(%r12)
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2285:
	addl	$2, %eax
	cltq
	leaq	19(%rax,%rax), %r15
	andq	$-16, %r15
	movq	%r15, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L2290
	subq	$4, %r15
	movl	$1, (%rax)
	addq	$4, %rax
	xorl	%edi, %edi
	shrq	%r15
	movl	$4, %r8d
	movq	%rax, -104(%rbp)
	movzwl	8(%r13), %esi
	movl	%r15d, -112(%rbp)
	movw	%r8w, -120(%rbp)
	jmp	.L2210
	.p2align 4,,10
	.p2align 3
.L2284:
	movl	$2, %r9d
	xorl	%edi, %edi
	movw	%r9w, -120(%rbp)
	jmp	.L2210
	.p2align 4,,10
	.p2align 3
.L2257:
	movq	%r15, %r13
	jmp	.L2214
	.p2align 4,,10
	.p2align 3
.L2229:
	movq	24(%r13), %rax
	lock addl	$1, -4(%rax)
	movq	24(%r13), %rax
	cmpw	$0, 8(%r12)
	movq	%rax, 24(%r12)
	movl	16(%r13), %eax
	movl	%eax, 16(%r12)
	jns	.L2226
	movl	12(%r13), %eax
	movl	%eax, 12(%r12)
	jmp	.L2226
	.p2align 4,,10
	.p2align 3
.L2238:
	movl	12(%r13), %ebx
	cmpl	$27, %ebx
	jle	.L2239
	cmpl	$2147483637, %ebx
	jg	.L2233
	.p2align 4,,10
	.p2align 3
.L2240:
	leal	1(%rbx), %eax
	cltq
	leaq	19(%rax,%rax), %rdx
	andq	$-16, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -136(%rbp)
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L2233
	movq	-136(%rbp), %rdx
	movl	$1, (%rax)
	addq	$4, %rax
	movq	%rax, 24(%r12)
	xorl	%eax, %eax
	subq	$4, %rdx
	shrq	%rdx
	movl	%edx, 16(%r12)
	movl	$4, %edx
	movw	%dx, 8(%r12)
.L2242:
	leaq	10(%r13), %rsi
	testb	$2, 8(%r13)
	jne	.L2245
	movq	24(%r13), %rsi
.L2245:
	leaq	10(%r12), %rdi
	testw	%ax, %ax
	jne	.L2247
	movq	24(%r12), %rdi
.L2247:
	movl	%ebx, %edx
	call	u_memcpy_67@PLT
	cmpl	$1023, %ebx
	jg	.L2248
	movzwl	8(%r12), %eax
	sall	$5, %ebx
	andl	$31, %eax
	orl	%eax, %ebx
	movw	%bx, 8(%r12)
	jmp	.L2226
	.p2align 4,,10
	.p2align 3
.L2234:
	movq	10(%r13), %rax
	leaq	18(%r12), %rdi
	andq	$-8, %rdi
	movq	%rax, 10(%r12)
	movq	-8(%rsi,%rdx), %rax
	movq	%rax, -8(%rcx,%rdx)
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addq	%rdx, %rcx
	shrq	$3, %rcx
	rep movsq
	jmp	.L2226
	.p2align 4,,10
	.p2align 3
.L2248:
	orw	$-32, 8(%r12)
	movl	%ebx, 12(%r12)
	jmp	.L2226
.L2286:
	movl	(%rsi), %eax
	movl	%eax, 10(%r12)
	movl	-4(%rsi,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L2226
.L2288:
	call	__stack_chk_fail@PLT
.L2290:
	movzwl	8(%r13), %esi
	jmp	.L2211
	.cfi_endproc
.LFE2967:
	.size	_ZN6icu_67plERKNS_13UnicodeStringES2_, .-_ZN6icu_67plERKNS_13UnicodeStringES2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString8unescapeEv
	.type	_ZNK6icu_6713UnicodeString8unescapeEv, @function
_ZNK6icu_6713UnicodeString8unescapeEv:
.LFB3030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movswl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L2292
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdi
	movl	%eax, %edx
	movq	%rdi, 0(%r13)
	xorl	%edi, %edi
	sarl	$5, %edx
	movw	%di, 8(%r13)
	cmpl	$895, %eax
	jg	.L2293
.L2295:
	movl	$2, %esi
	movl	$2, %edi
	movw	%si, 8(%r13)
.L2294:
	movswl	8(%r14), %ebx
	testb	$17, %bl
	jne	.L2316
	leaq	10(%r14), %r12
	testb	$2, %bl
	jne	.L2298
	movq	24(%r14), %r12
.L2298:
	testw	%bx, %bx
	js	.L2300
	sarl	$5, %ebx
.L2301:
	movl	$0, -48(%rbp)
	xorl	%eax, %eax
	xorl	%r8d, %r8d
.L2314:
	xorl	%edx, %edx
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2302:
	leal	1(%rax), %esi
	addq	$1, %rax
	movl	$1, %edx
	cmpw	$92, -2(%r12,%rax,2)
	je	.L2328
.L2305:
	movl	%eax, %ecx
	cmpl	%eax, %ebx
	jne	.L2302
	testb	%dl, %dl
	je	.L2303
	movl	%ebx, -48(%rbp)
.L2303:
	andl	$17, %edi
	jne	.L2291
	movl	%ebx, %ecx
	subl	%r8d, %ecx
	je	.L2291
	testq	%r12, %r12
	je	.L2291
	movl	%r8d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
.L2291:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2329
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2328:
	.cfi_restore_state
	andl	$17, %edi
	movl	%esi, -48(%rbp)
	jne	.L2306
	subl	%r8d, %ecx
	je	.L2306
	movl	%r8d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
.L2306:
	movzwl	8(%r14), %eax
	testw	%ax, %ax
	js	.L2307
	movswl	%ax, %edx
	sarl	$5, %edx
.L2308:
	leaq	-48(%rbp), %rsi
	movq	%r14, %rcx
	leaq	UnicodeString_charAt(%rip), %rdi
	call	u_unescapeAt_67@PLT
	testl	%eax, %eax
	js	.L2330
	movzwl	8(%r13), %edi
	movl	%edi, %edx
	andl	$17, %edx
	cmpl	$65535, %eax
	jg	.L2311
	movw	%ax, -44(%rbp)
	movl	$1, %ecx
.L2312:
	testw	%dx, %dx
	jne	.L2313
	movq	%r13, %rdi
	leaq	-44(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movzwl	8(%r13), %edi
.L2313:
	movl	-48(%rbp), %r8d
	movslq	%r8d, %rax
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2292:
	movl	12(%rsi), %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%ecx, %ecx
	movq	%rax, (%rdi)
	movw	%cx, 8(%rdi)
	cmpl	$27, %edx
	jle	.L2295
	cmpl	$2147483637, %edx
	jle	.L2293
.L2296:
	movl	$1, %eax
	movq	$0, 24(%r13)
	movw	%ax, 8(%r13)
	movl	$0, 16(%r13)
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2311:
	cmpl	$1114111, %eax
	jg	.L2313
	movl	%eax, %ecx
	andw	$1023, %ax
	sarl	$10, %ecx
	orw	$-9216, %ax
	subw	$10304, %cx
	movw	%ax, -42(%rbp)
	movw	%cx, -44(%rbp)
	movl	$2, %ecx
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2300:
	movl	12(%r14), %ebx
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2293:
	leal	1(%rdx), %eax
	cltq
	leaq	19(%rax,%rax), %rbx
	andq	$-16, %rbx
	movq	%rbx, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L2296
	subq	$4, %rbx
	movl	$1, (%rax)
	movl	$4, %edx
	addq	$4, %rax
	shrq	%rbx
	movq	%rax, 24(%r13)
	movl	$4, %edi
	movl	%ebx, 16(%r13)
	movw	%dx, 8(%r13)
	jmp	.L2294
	.p2align 4,,10
	.p2align 3
.L2307:
	movl	12(%r14), %edx
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2316:
	xorl	%r12d, %r12d
	jmp	.L2298
.L2330:
	movzwl	8(%r13), %eax
	movl	%eax, %edx
	andl	$31, %edx
	testb	$1, %al
	movl	$2, %eax
	cmove	%edx, %eax
	movw	%ax, 8(%r13)
	jmp	.L2291
.L2329:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3030:
	.size	_ZNK6icu_6713UnicodeString8unescapeEv, .-_ZNK6icu_6713UnicodeString8unescapeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	.type	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii, @function
_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii:
.LFB3070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rdi), %eax
	movl	%eax, %esi
	andw	$17, %si
	jne	.L2424
	movq	%rcx, %r14
	movl	%r9d, %r13d
	testw	%ax, %ax
	js	.L2334
	movswl	%ax, %ebx
	sarl	$5, %ebx
.L2335:
	testl	%r13d, %r13d
	sete	%cl
	testb	$8, %al
	je	.L2336
	testb	%cl, %cl
	jne	.L2425
.L2336:
	cmpl	%r15d, %ebx
	je	.L2426
.L2345:
	testq	%r14, %r14
	je	.L2388
	movslq	%r8d, %r8
	leaq	(%r14,%r8,2), %r14
	testl	%r13d, %r13d
	js	.L2427
.L2347:
	testw	%ax, %ax
	js	.L2348
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L2349:
	xorl	%r10d, %r10d
	testl	%r15d, %r15d
	js	.L2350
	cmpl	%r15d, %ecx
	movl	%r15d, %r10d
	cmovle	%ecx, %r10d
.L2350:
	testl	%edx, %edx
	js	.L2390
	subl	%r10d, %ecx
	movl	%ebx, %r15d
	cmpl	%edx, %ecx
	cmovle	%ecx, %edx
	movl	%edx, -152(%rbp)
	subl	%edx, %r15d
.L2351:
	movl	$2147483647, %edx
	subl	%r15d, %edx
	cmpl	%r13d, %edx
	jl	.L2428
	movl	%eax, %edx
	addl	%r13d, %r15d
	andw	$2, %dx
	jne	.L2429
	movq	24(%r12), %r11
	testb	$25, %al
	jne	.L2359
.L2357:
	testb	$4, %al
	jne	.L2430
.L2360:
	movslq	%r13d, %rax
	leaq	(%r14,%rax,2), %rax
	cmpq	%rax, %r11
	jnb	.L2362
	movslq	%ebx, %rax
	leaq	(%r11,%rax,2), %rax
	cmpq	%rax, %r14
	jb	.L2431
.L2362:
	movzwl	8(%r12), %edx
	andl	$2, %edx
.L2358:
	cmpl	$27, %r15d
	jle	.L2359
	testw	%dx, %dx
	je	.L2359
	leaq	-128(%rbp), %rcx
	movq	%r11, %rsi
	movl	%ebx, %edx
	movl	%r10d, -164(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -160(%rbp)
	call	u_memcpy_67@PLT
	movq	-160(%rbp), %rcx
	movl	-164(%rbp), %r10d
	movq	%rcx, %r11
	.p2align 4,,10
	.p2align 3
.L2359:
	movl	$2147483637, %ecx
	movl	%r15d, %eax
	leaq	-136(%rbp), %r8
	movq	%r12, %rdi
	sarl	$2, %eax
	movl	%ecx, %esi
	movl	%r10d, -164(%rbp)
	subl	$-128, %eax
	subl	%r15d, %esi
	movq	%r11, -160(%rbp)
	cmpl	%esi, %eax
	leal	(%r15,%rax), %edx
	movl	%r15d, %esi
	movq	$0, -136(%rbp)
	cmovg	%ecx, %edx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia
	testb	%al, %al
	je	.L2424
	testb	$2, 8(%r12)
	movq	-160(%rbp), %r11
	leaq	10(%r12), %rcx
	movslq	-164(%rbp), %r10
	jne	.L2376
	movq	24(%r12), %rcx
.L2376:
	cmpq	%rcx, %r11
	je	.L2377
	testl	%r10d, %r10d
	jle	.L2378
	movslq	%r10d, %rdx
	movq	%r11, %rsi
	movq	%rcx, %rdi
	movl	%r10d, -164(%rbp)
	addq	%rdx, %rdx
	movq	%r11, -160(%rbp)
	call	memmove@PLT
	movslq	-164(%rbp), %r10
	movq	-160(%rbp), %r11
	movq	%rax, %rcx
.L2378:
	movl	-152(%rbp), %eax
	addl	%r10d, %eax
	subl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.L2380
	cltq
	movslq	%ebx, %rbx
	movl	%r10d, -160(%rbp)
	leaq	(%r11,%rax,2), %rsi
	leal	0(%r13,%r10), %eax
	movq	%rcx, -152(%rbp)
	cltq
	leaq	(%rbx,%rbx), %rdx
	leaq	(%rcx,%rax,2), %rdi
	call	memmove@PLT
	movq	-152(%rbp), %rcx
	movslq	-160(%rbp), %r10
.L2380:
	testl	%r13d, %r13d
	jle	.L2382
	movslq	%r13d, %rdx
	leaq	(%rcx,%r10,2), %rdi
	movq	%r14, %rsi
	addq	%rdx, %rdx
	call	memmove@PLT
.L2382:
	movzwl	8(%r12), %eax
	cmpl	$1023, %r15d
	jg	.L2383
	andl	$31, %eax
	sall	$5, %r15d
	orl	%eax, %r15d
	movw	%r15w, 8(%r12)
.L2384:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2424
	call	uprv_free_67@PLT
	movq	%r12, %rax
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2428:
	testb	$4, %al
	jne	.L2432
.L2354:
	movq	$0, 24(%r12)
	movl	$1, %ecx
	movw	%cx, 8(%r12)
	movl	$0, 16(%r12)
	.p2align 4,,10
	.p2align 3
.L2424:
	movq	%r12, %rax
.L2331:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2433
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2334:
	.cfi_restore_state
	movl	12(%rdi), %ebx
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2429:
	leaq	10(%r12), %r11
	testb	$25, %al
	je	.L2357
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2390:
	movl	$0, -152(%rbp)
	movl	%ebx, %r15d
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2348:
	movl	12(%r12), %ecx
	jmp	.L2349
	.p2align 4,,10
	.p2align 3
.L2425:
	testl	%r15d, %r15d
	je	.L2434
	js	.L2435
	cmpl	%ebx, %r15d
	movl	%ebx, %ecx
	cmovg	%ebx, %r15d
	subl	%r15d, %ecx
	cmpl	%edx, %ecx
	jle	.L2436
.L2342:
	cmpl	%r15d, %ebx
	jne	.L2345
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2388:
	xorl	%r13d, %r13d
	jmp	.L2347
	.p2align 4,,10
	.p2align 3
.L2383:
	orl	$-32, %eax
	movl	%r15d, 12(%r12)
	movw	%ax, 8(%r12)
	jmp	.L2384
	.p2align 4,,10
	.p2align 3
.L2427:
	movq	%r14, %rdi
	movl	%edx, -152(%rbp)
	call	u_strlen_67@PLT
	movl	-152(%rbp), %edx
	movl	%eax, %r13d
	movzwl	8(%r12), %eax
	jmp	.L2347
	.p2align 4,,10
	.p2align 3
.L2377:
	movl	-152(%rbp), %eax
	cmpl	%eax, %r13d
	je	.L2380
	addl	%r10d, %eax
	subl	%eax, %ebx
	testl	%ebx, %ebx
	jle	.L2380
	cltq
	movslq	%ebx, %rbx
	movl	%r10d, -160(%rbp)
	leaq	(%rcx,%rax,2), %rsi
	leal	0(%r13,%r10), %eax
	movq	%rcx, -152(%rbp)
	cltq
	leaq	(%rbx,%rbx), %rdx
	leaq	(%rcx,%rax,2), %rdi
	call	memmove@PLT
	movslq	-160(%rbp), %r10
	movq	-152(%rbp), %rcx
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2426:
	testq	%r14, %r14
	je	.L2424
	testb	%cl, %cl
	jne	.L2424
	movl	%r13d, %ecx
	movl	%r8d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2430:
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	cmpl	$1, %eax
	jne	.L2362
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2434:
	xorl	%ecx, %ecx
	testl	%edx, %edx
	js	.L2338
	cmpl	%edx, %ebx
	cmovle	%ebx, %edx
	movslq	%edx, %rcx
	movq	%rcx, %r15
	addq	%rcx, %rcx
	subl	%r15d, %ebx
.L2338:
	addq	%rcx, 24(%r12)
	subl	%r15d, 16(%r12)
	cmpl	$1023, %ebx
	jg	.L2339
	andl	$31, %eax
	sall	$5, %ebx
	orl	%ebx, %eax
	movw	%ax, 8(%r12)
	movq	%r12, %rax
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2339:
	orl	$-32, %eax
	movl	%ebx, 12(%r12)
	movw	%ax, 8(%r12)
	movq	%r12, %rax
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2436:
	cmpl	$1023, %r15d
	jle	.L2437
	orl	$-32, %eax
	movl	%r15d, 12(%r12)
	movw	%ax, 8(%r12)
.L2344:
	movl	%r15d, 16(%r12)
	movq	%r12, %rax
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2435:
	xorl	%r15d, %r15d
	cmpl	%edx, %ebx
	jg	.L2342
.L2341:
	andl	$31, %eax
	orl	%eax, %esi
	movw	%si, 8(%r12)
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2432:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	jne	.L2354
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2431:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	movl	$2, %edx
	movq	%rbx, -128(%rbp)
	movw	%dx, -120(%rbp)
	testl	%r13d, %r13d
	je	.L2391
	testq	%r14, %r14
	je	.L2391
	leaq	-128(%rbp), %r15
	xorl	%edx, %edx
	movl	%r13d, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r10d, -160(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movzwl	-120(%rbp), %edx
	movl	-160(%rbp), %r10d
	testb	$1, %dl
	jne	.L2438
	leaq	-118(%rbp), %rcx
	andl	$2, %edx
	cmove	-104(%rbp), %rcx
.L2363:
	movl	-152(%rbp), %edx
	movl	%r13d, %r9d
	xorl	%r8d, %r8d
	movl	%r10d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	movzwl	-120(%rbp), %edx
.L2370:
	andl	$4, %edx
	movq	%rbx, -128(%rbp)
	jne	.L2439
.L2372:
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%rax, -128(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	-152(%rbp), %rax
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2391:
	leaq	-118(%rbp), %rcx
	leaq	-128(%rbp), %r15
	jmp	.L2363
.L2439:
	movq	-104(%rbp), %rdx
	lock subl	$1, -4(%rdx)
	jne	.L2372
	movq	%rax, -152(%rbp)
	movq	-104(%rbp), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	movq	-152(%rbp), %rax
	jmp	.L2372
.L2438:
	testb	$4, 8(%r12)
	jne	.L2440
.L2368:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movq	%r12, %rax
	movl	$0, 16(%r12)
	jmp	.L2370
.L2437:
	movl	%r15d, %esi
	sall	$5, %esi
	jmp	.L2341
.L2440:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	je	.L2369
.L2423:
	movzwl	-120(%rbp), %edx
	jmp	.L2368
.L2433:
	call	__stack_chk_fail@PLT
.L2369:
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2423
	.cfi_endproc
.LFE3070:
	.size	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii, .-_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString7replaceEiii
	.type	_ZN6icu_6713UnicodeString7replaceEiii, @function
_ZN6icu_6713UnicodeString7replaceEiii:
.LFB3067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$65535, %ecx
	ja	.L2442
	movw	%cx, -12(%rbp)
	movl	$1, %r9d
.L2443:
	xorl	%r8d, %r8d
	leaq	-12(%rbp), %rcx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2447
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2442:
	.cfi_restore_state
	xorl	%r9d, %r9d
	cmpl	$1114111, %ecx
	ja	.L2443
	movl	%ecx, %eax
	andw	$1023, %cx
	movl	$2, %r9d
	sarl	$10, %eax
	orw	$-9216, %cx
	subw	$10304, %ax
	movw	%cx, -10(%rbp)
	movw	%ax, -12(%rbp)
	jmp	.L2443
.L2447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3067:
	.size	_ZN6icu_6713UnicodeString7replaceEiii, .-_ZN6icu_6713UnicodeString7replaceEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii
	.type	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii, @function
_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii:
.LFB3069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %r11d
	movl	%r9d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movzwl	8(%rcx), %ebx
	testw	%bx, %bx
	js	.L2449
	movswl	%bx, %eax
	sarl	$5, %eax
.L2450:
	xorl	%r8d, %r8d
	testl	%r11d, %r11d
	js	.L2451
	cmpl	%r11d, %eax
	cmovle	%eax, %r11d
	movl	%r11d, %r8d
.L2451:
	xorl	%r9d, %r9d
	testl	%r10d, %r10d
	js	.L2452
	subl	%r8d, %eax
	cmpl	%r10d, %eax
	cmovle	%eax, %r10d
	movl	%r10d, %r9d
.L2452:
	andl	$2, %ebx
	je	.L2453
	popq	%rbx
	addq	$10, %rcx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	.p2align 4,,10
	.p2align 3
.L2453:
	.cfi_restore_state
	movq	24(%rcx), %rcx
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	.p2align 4,,10
	.p2align 3
.L2449:
	.cfi_restore_state
	movl	12(%rcx), %eax
	jmp	.L2450
	.cfi_endproc
.LFE3069:
	.size	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii, .-_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString4copyEiii
	.type	_ZN6icu_6713UnicodeString4copyEiii, @function
_ZN6icu_6713UnicodeString4copyEiii:
.LFB3074:
	.cfi_startproc
	endbr64
	cmpl	%esi, %edx
	jg	.L2492
	ret
	.p2align 4,,10
	.p2align 3
.L2492:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	subl	%esi, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movslq	%r9d, %rdi
	pushq	%r12
	addq	%rdi, %rdi
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movl	%r9d, -52(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2458
	xorl	%eax, %eax
	testl	%r12d, %r12d
	movzwl	8(%r13), %ecx
	movl	-52(%rbp), %r9d
	js	.L2462
	testw	%cx, %cx
	js	.L2463
	movswl	%cx, %eax
	sarl	$5, %eax
.L2464:
	cmpl	%eax, %r12d
	cmovle	%r12d, %eax
.L2462:
	xorl	%edx, %edx
	testl	%ebx, %ebx
	js	.L2465
	testw	%cx, %cx
	js	.L2466
	movswl	%cx, %edx
	sarl	$5, %edx
.L2467:
	cmpl	%edx, %ebx
	cmovle	%ebx, %edx
.L2465:
	movl	%edx, %edi
	subl	%eax, %edi
	testw	%cx, %cx
	js	.L2468
	movswl	%cx, %edx
	sarl	$5, %edx
	testl	%eax, %eax
	js	.L2477
.L2494:
	cmpl	%edx, %eax
	cmovg	%edx, %eax
	movslq	%eax, %rsi
	leaq	(%rsi,%rsi), %r8
	testl	%edi, %edi
	js	.L2478
.L2495:
	subl	%eax, %edx
	cmpl	%edi, %edx
	cmovg	%edi, %edx
	testl	%edx, %edx
	setg	%al
.L2471:
	andl	$2, %ecx
	leaq	10(%r13), %rsi
	jne	.L2473
	movq	24(%r13), %rsi
.L2473:
	addq	%r8, %rsi
	cmpq	%rsi, %r15
	je	.L2474
	testb	%al, %al
	jne	.L2493
.L2474:
	movq	%r13, %rdi
	movq	%r15, %rcx
	movl	%r14d, %esi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	addq	$24, %rsp
	movq	%r15, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
.L2458:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2468:
	.cfi_restore_state
	movl	12(%r13), %edx
	testl	%eax, %eax
	jns	.L2494
.L2477:
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	testl	%edi, %edi
	jns	.L2495
.L2478:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L2493:
	movslq	%edx, %rdx
	movq	%r15, %rdi
	movl	%r9d, -52(%rbp)
	addq	%rdx, %rdx
	call	memmove@PLT
	movl	-52(%rbp), %r9d
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2463:
	movl	12(%r13), %eax
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2466:
	movl	12(%r13), %edx
	jmp	.L2467
	.cfi_endproc
.LFE3074:
	.size	_ZN6icu_6713UnicodeString4copyEiii, .-_ZN6icu_6713UnicodeString4copyEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString20handleReplaceBetweenEiiRKS0_
	.type	_ZN6icu_6713UnicodeString20handleReplaceBetweenEiiRKS0_, @function
_ZN6icu_6713UnicodeString20handleReplaceBetweenEiiRKS0_:
.LFB3073:
	.cfi_startproc
	endbr64
	movzwl	8(%rcx), %r10d
	subl	%esi, %edx
	testw	%r10w, %r10w
	js	.L2497
	movswl	%r10w, %r9d
	xorl	%r8d, %r8d
	sarl	$5, %r9d
.L2498:
	movl	%r9d, %eax
	subl	%r8d, %eax
	cmpl	%r9d, %eax
	cmovle	%eax, %r9d
.L2499:
	andl	$2, %r10d
	je	.L2500
	addq	$10, %rcx
	jmp	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	.p2align 4,,10
	.p2align 3
.L2500:
	movq	24(%rcx), %rcx
	jmp	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	.p2align 4,,10
	.p2align 3
.L2497:
	movl	12(%rcx), %r9d
	movl	$0, %r8d
	testl	%r9d, %r9d
	cmovle	%r9d, %r8d
	jns	.L2498
	xorl	%r9d, %r9d
	jmp	.L2499
	.cfi_endproc
.LFE3073:
	.size	_ZN6icu_6713UnicodeString20handleReplaceBetweenEiiRKS0_, .-_ZN6icu_6713UnicodeString20handleReplaceBetweenEiiRKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2ERKS0_i
	.type	_ZN6icu_6713UnicodeStringC2ERKS0_i, @function
_ZN6icu_6713UnicodeStringC2ERKS0_i:
.LFB3009:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movzwl	8(%rsi), %ecx
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L2504
	testw	%cx, %cx
	js	.L2505
	movswl	%cx, %eax
	sarl	$5, %eax
.L2506:
	cmpl	%eax, %edx
	cmovle	%edx, %eax
.L2504:
	testw	%cx, %cx
	js	.L2507
	movswl	%cx, %edx
	sarl	$5, %edx
	movl	%edx, %r10d
	subl	%eax, %r10d
.L2508:
	xorl	%r8d, %r8d
	testl	%eax, %eax
	js	.L2509
	cmpl	%edx, %eax
	cmovg	%edx, %eax
	movl	%eax, %r8d
.L2509:
	xorl	%r9d, %r9d
	testl	%r10d, %r10d
	js	.L2510
	subl	%r8d, %edx
	movl	%r10d, %r9d
	cmpl	%r10d, %edx
	cmovle	%edx, %r9d
.L2510:
	andl	$2, %ecx
	leaq	10(%rsi), %rcx
	jne	.L2512
	movq	24(%rsi), %rcx
.L2512:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	.p2align 4,,10
	.p2align 3
.L2505:
	movl	12(%rsi), %eax
	jmp	.L2506
	.p2align 4,,10
	.p2align 3
.L2507:
	movl	12(%rsi), %edx
	movl	%edx, %r10d
	subl	%eax, %r10d
	jmp	.L2508
	.cfi_endproc
.LFE3009:
	.size	_ZN6icu_6713UnicodeStringC2ERKS0_i, .-_ZN6icu_6713UnicodeStringC2ERKS0_i
	.globl	_ZN6icu_6713UnicodeStringC1ERKS0_i
	.set	_ZN6icu_6713UnicodeStringC1ERKS0_i,_ZN6icu_6713UnicodeStringC2ERKS0_i
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2ERKS0_ii
	.type	_ZN6icu_6713UnicodeStringC2ERKS0_ii, @function
_ZN6icu_6713UnicodeStringC2ERKS0_ii:
.LFB3012:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movzwl	8(%rsi), %r10d
	movq	%rax, (%rdi)
	movl	$2, %eax
	movw	%ax, 8(%rdi)
	testw	%r10w, %r10w
	js	.L2517
	movswl	%r10w, %eax
	sarl	$5, %eax
.L2518:
	xorl	%r8d, %r8d
	testl	%edx, %edx
	js	.L2519
	cmpl	%eax, %edx
	cmovg	%eax, %edx
	movl	%edx, %r8d
.L2519:
	xorl	%r9d, %r9d
	testl	%ecx, %ecx
	js	.L2520
	subl	%r8d, %eax
	cmpl	%ecx, %eax
	cmovle	%eax, %ecx
	movl	%ecx, %r9d
.L2520:
	andl	$2, %r10d
	leaq	10(%rsi), %rcx
	jne	.L2522
	movq	24(%rsi), %rcx
.L2522:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	.p2align 4,,10
	.p2align 3
.L2517:
	movl	12(%rsi), %eax
	jmp	.L2518
	.cfi_endproc
.LFE3012:
	.size	_ZN6icu_6713UnicodeStringC2ERKS0_ii, .-_ZN6icu_6713UnicodeStringC2ERKS0_ii
	.globl	_ZN6icu_6713UnicodeStringC1ERKS0_ii
	.set	_ZN6icu_6713UnicodeStringC1ERKS0_ii,_ZN6icu_6713UnicodeStringC2ERKS0_ii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713UnicodeString14extractBetweenEiiRS0_
	.type	_ZNK6icu_6713UnicodeString14extractBetweenEiiRS0_, @function
_ZNK6icu_6713UnicodeString14extractBetweenEiiRS0_:
.LFB3050:
	.cfi_startproc
	endbr64
	movq	%rcx, %r10
	movzwl	8(%rdi), %r11d
	xorl	%ecx, %ecx
	testl	%esi, %esi
	js	.L2526
	testw	%r11w, %r11w
	js	.L2527
	movswl	%r11w, %ecx
	sarl	$5, %ecx
.L2528:
	cmpl	%esi, %ecx
	cmovg	%esi, %ecx
.L2526:
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L2529
	testw	%r11w, %r11w
	js	.L2530
	movswl	%r11w, %eax
	sarl	$5, %eax
.L2531:
	cmpl	%edx, %eax
	cmovg	%edx, %eax
.L2529:
	movswl	8(%r10), %edx
	subl	%ecx, %eax
	testw	%dx, %dx
	js	.L2532
	sarl	$5, %edx
.L2533:
	testw	%r11w, %r11w
	js	.L2534
	movswl	%r11w, %esi
	sarl	$5, %esi
.L2535:
	xorl	%r8d, %r8d
	testl	%ecx, %ecx
	js	.L2536
	cmpl	%esi, %ecx
	cmovg	%esi, %ecx
	movl	%ecx, %r8d
.L2536:
	xorl	%r9d, %r9d
	testl	%eax, %eax
	js	.L2537
	subl	%r8d, %esi
	cmpl	%eax, %esi
	cmovle	%esi, %eax
	movl	%eax, %r9d
.L2537:
	andl	$2, %r11d
	leaq	10(%rdi), %rcx
	jne	.L2539
	movq	24(%rdi), %rcx
.L2539:
	xorl	%esi, %esi
	movq	%r10, %rdi
	jmp	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	.p2align 4,,10
	.p2align 3
.L2527:
	movl	12(%rdi), %ecx
	jmp	.L2528
	.p2align 4,,10
	.p2align 3
.L2530:
	movl	12(%rdi), %eax
	jmp	.L2531
	.p2align 4,,10
	.p2align 3
.L2534:
	movl	12(%rdi), %esi
	jmp	.L2535
	.p2align 4,,10
	.p2align 3
.L2532:
	movl	12(%r10), %edx
	jmp	.L2533
	.cfi_endproc
.LFE3050:
	.size	_ZNK6icu_6713UnicodeString14extractBetweenEiiRS0_, .-_ZNK6icu_6713UnicodeString14extractBetweenEiiRS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii
	.type	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii, @function
_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii:
.LFB3059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movswl	8(%rdi), %eax
	testb	$1, %al
	jne	.L2582
	movl	%esi, %r10d
	movzwl	8(%rcx), %esi
	movq	%rcx, %rbx
	testb	$1, %sil
	jne	.L2582
	movq	16(%rbp), %rdi
	movswl	8(%rdi), %ecx
	testb	$1, %cl
	jne	.L2582
	testw	%ax, %ax
	js	.L2547
	sarl	$5, %eax
.L2548:
	xorl	%r11d, %r11d
	testl	%r10d, %r10d
	js	.L2549
	cmpl	%r10d, %eax
	cmovle	%eax, %r10d
	movl	%r10d, %r11d
.L2549:
	xorl	%r14d, %r14d
	testl	%edx, %edx
	js	.L2550
	subl	%r11d, %eax
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	movl	%edx, %r14d
.L2550:
	testw	%si, %si
	js	.L2551
	movswl	%si, %eax
	sarl	$5, %eax
.L2552:
	movl	$0, -64(%rbp)
	testl	%r8d, %r8d
	js	.L2553
	cmpl	%r8d, %eax
	cmovle	%eax, %r8d
	movl	%r8d, -64(%rbp)
.L2553:
	xorl	%r13d, %r13d
	testl	%r9d, %r9d
	js	.L2554
	subl	-64(%rbp), %eax
	cmpl	%r9d, %eax
	cmovle	%eax, %r9d
	movl	%r9d, %r13d
.L2554:
	testw	%cx, %cx
	js	.L2555
	sarl	$5, %ecx
.L2556:
	movl	24(%rbp), %edx
	movl	$0, -68(%rbp)
	testl	%edx, %edx
	js	.L2557
	cmpl	24(%rbp), %ecx
	movl	%ecx, %eax
	cmovg	24(%rbp), %eax
	movl	%eax, -68(%rbp)
.L2557:
	movl	32(%rbp), %eax
	xorl	%r12d, %r12d
	testl	%eax, %eax
	js	.L2558
	subl	-68(%rbp), %ecx
	cmpl	32(%rbp), %ecx
	cmovg	32(%rbp), %ecx
	movl	%ecx, %r12d
.L2558:
	testl	%r13d, %r13d
	je	.L2582
	cmpl	%r13d, %r14d
	jl	.L2582
	testl	%r14d, %r14d
	jle	.L2582
	movq	%r15, %rax
	movq	%rbx, -56(%rbp)
	movl	%r13d, %r15d
	movl	%r11d, %ebx
	movq	%rax, %r13
	jmp	.L2559
	.p2align 4,,10
	.p2align 3
.L2593:
	movswl	%si, %ecx
	sarl	$5, %ecx
.L2561:
	movl	-64(%rbp), %eax
	xorl	%edx, %edx
	testl	%eax, %eax
	js	.L2562
	cmpl	%ecx, %eax
	movl	%eax, %edx
	cmovg	%ecx, %edx
.L2562:
	testl	%r15d, %r15d
	js	.L2591
	subl	%edx, %ecx
	cmpl	%r15d, %ecx
	cmovg	%r15d, %ecx
	testl	%ecx, %ecx
	jle	.L2591
	movq	-56(%rbp), %rax
	andl	$2, %esi
	leaq	10(%rax), %rsi
	jne	.L2564
	movq	24(%rax), %rsi
.L2564:
	movl	%r14d, %r9d
	movl	%ebx, %r8d
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L2591
	movq	16(%rbp), %rax
	movzwl	8(%rax), %edx
	testw	%dx, %dx
	js	.L2565
	movswl	%dx, %eax
	sarl	$5, %eax
.L2566:
	movl	-68(%rbp), %edi
	xorl	%r8d, %r8d
	testl	%edi, %edi
	js	.L2567
	cmpl	%eax, %edi
	movl	%edi, %r8d
	cmovg	%eax, %r8d
.L2567:
	xorl	%r9d, %r9d
	testl	%r12d, %r12d
	js	.L2568
	subl	%r8d, %eax
	cmpl	%r12d, %eax
	cmovg	%r12d, %eax
	movl	%eax, %r9d
.L2568:
	movq	16(%rbp), %rax
	andl	$2, %edx
	leaq	10(%rax), %rcx
	jne	.L2570
	movq	24(%rax), %rcx
.L2570:
	movl	%r15d, %edx
	movq	%r13, %rdi
	movl	%esi, -60(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii
	movl	-60(%rbp), %esi
	leal	(%r15,%rsi), %eax
	subl	%ebx, %eax
	leal	(%r12,%rsi), %ebx
	subl	%eax, %r14d
	cmpl	%r15d, %r14d
	jl	.L2591
	testl	%r14d, %r14d
	jle	.L2591
	movq	-56(%rbp), %rax
	movzwl	8(%rax), %esi
	testb	$1, %sil
	jne	.L2591
.L2559:
	testw	%si, %si
	jns	.L2593
	movq	-56(%rbp), %rax
	movl	12(%rax), %ecx
	jmp	.L2561
	.p2align 4,,10
	.p2align 3
.L2591:
	movq	%r13, %r15
.L2582:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2547:
	.cfi_restore_state
	movl	12(%r15), %eax
	jmp	.L2548
	.p2align 4,,10
	.p2align 3
.L2565:
	movl	12(%rax), %eax
	jmp	.L2566
	.p2align 4,,10
	.p2align 3
.L2555:
	movq	16(%rbp), %rax
	movl	12(%rax), %ecx
	jmp	.L2556
	.p2align 4,,10
	.p2align 3
.L2551:
	movl	12(%rbx), %eax
	jmp	.L2552
	.cfi_endproc
.LFE3059:
	.size	_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii, .-_ZN6icu_6713UnicodeString14findAndReplaceEiiRKS0_iiS2_ii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UnicodeStringAppendable15getAppendBufferEiiPDsiPi
	.type	_ZN6icu_6723UnicodeStringAppendable15getAppendBufferEiiPDsiPi, @function
_ZN6icu_6723UnicodeStringAppendable15getAppendBufferEiiPDsiPi:
.LFB3092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	jle	.L2605
	movl	%r8d, %r13d
	cmpl	%r8d, %esi
	jg	.L2605
	movq	%rdi, %r15
	movq	8(%rdi), %rdi
	movq	%rcx, %rbx
	movswl	8(%rdi), %r12d
	testw	%r12w, %r12w
	js	.L2598
	sarl	$5, %r12d
.L2599:
	movl	$2147483637, %eax
	movl	%edx, %ecx
	subl	%r12d, %eax
	cmpl	%edx, %esi
	cmovge	%esi, %ecx
	cmpl	%ecx, %eax
	jge	.L2610
.L2600:
	movl	%r13d, (%r14)
	movq	%rbx, %rax
.L2594:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2598:
	.cfi_restore_state
	movl	12(%rdi), %r12d
	jmp	.L2599
	.p2align 4,,10
	.p2align 3
.L2605:
	movl	$0, (%r14)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2610:
	.cfi_restore_state
	addl	%r12d, %edx
	addl	%r12d, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia
	testb	%al, %al
	je	.L2600
	movq	8(%r15), %rax
	movl	$27, %edx
	testb	$2, 8(%rax)
	jne	.L2602
	movl	16(%rax), %edx
.L2602:
	subl	%r12d, %edx
	movl	%edx, (%r14)
	testb	$2, 8(%rax)
	jne	.L2611
	movq	24(%rax), %rax
.L2604:
	movslq	%r12d, %r12
	leaq	(%rax,%r12,2), %rax
	jmp	.L2594
	.p2align 4,,10
	.p2align 3
.L2611:
	addq	$10, %rax
	jmp	.L2604
	.cfi_endproc
.LFE3092:
	.size	_ZN6icu_6723UnicodeStringAppendable15getAppendBufferEiiPDsiPi, .-_ZN6icu_6723UnicodeStringAppendable15getAppendBufferEiiPDsiPi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EPKci
	.type	_ZN6icu_6713UnicodeStringC2EPKci, @function
_ZN6icu_6713UnicodeStringC2EPKci:
.LFB3000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%r8w, 8(%rdi)
	sete	%dl
	cmpl	$-1, %r12d
	movq	%rax, (%rdi)
	setl	%al
	orb	%al, %dl
	jne	.L2612
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L2612
	movq	%rdi, %rbx
	cmpl	$-1, %r12d
	jne	.L2614
	movq	%rsi, %rdi
	call	strlen@PLT
	movl	%eax, %r12d
.L2614:
	cmpl	$27, %r12d
	leal	1(%r12), %esi
	movl	$27, %eax
	movq	%rbx, %rdi
	cmovle	%eax, %esi
	movl	$1, %ecx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$-1, %edx
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia
	testb	%al, %al
	je	.L2652
	movzwl	8(%rbx), %edx
	movl	%edx, %eax
	andl	$31, %eax
	orl	$16, %eax
	andl	$2, %edx
	movw	%ax, 8(%rbx)
	jne	.L2653
	movl	$0, -44(%rbp)
	movq	24(%rbx), %rdi
.L2617:
	movl	16(%rbx), %esi
.L2618:
	leaq	-44(%rbp), %rax
	movq	%r13, %rcx
	leaq	-48(%rbp), %rdx
	movl	%r12d, %r8d
	pushq	%rax
	movl	$65533, %r9d
	pushq	$0
	call	u_strFromUTF8WithSub_67@PLT
	movzwl	8(%rbx), %ecx
	popq	%rsi
	movl	-48(%rbp), %eax
	popq	%rdi
	testb	$16, %cl
	je	.L2620
	cmpl	$-1, %eax
	jge	.L2654
.L2620:
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jle	.L2612
	andl	$4, %ecx
	jne	.L2655
.L2634:
	movl	$1, %eax
	movq	$0, 24(%rbx)
	movw	%ax, 8(%rbx)
	movl	$0, 16(%rbx)
.L2612:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2656
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2653:
	.cfi_restore_state
	movl	$0, -44(%rbp)
	leaq	10(%rbx), %rdi
.L2650:
	movl	$27, %esi
	jmp	.L2618
	.p2align 4,,10
	.p2align 3
.L2652:
	movl	$0, -44(%rbp)
	xorl	%edi, %edi
	testb	$2, 8(%rbx)
	je	.L2617
	jmp	.L2650
	.p2align 4,,10
	.p2align 3
.L2654:
	testb	$2, %cl
	jne	.L2621
	movslq	16(%rbx), %rdx
	cmpl	$-1, %eax
	je	.L2657
.L2623:
	cmpl	%edx, %eax
	cmovg	%edx, %eax
.L2629:
	cmpl	$1023, %eax
	jg	.L2630
.L2625:
	andl	$31, %ecx
	sall	$5, %eax
	orl	%eax, %ecx
.L2631:
	andl	$-17, %ecx
	movw	%cx, 8(%rbx)
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2630:
	movl	%eax, 12(%rbx)
	orl	$-32, %ecx
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2655:
	movq	24(%rbx), %rax
	lock subl	$1, -4(%rax)
	jne	.L2634
	movq	24(%rbx), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2634
	.p2align 4,,10
	.p2align 3
.L2621:
	movl	$27, %edx
	cmpl	$-1, %eax
	jne	.L2623
	leaq	10(%rbx), %rsi
	movl	$54, %edx
.L2624:
	addq	%rsi, %rdx
	cmpq	%rdx, %rsi
	jnb	.L2640
	movq	%rsi, %rax
	jmp	.L2628
	.p2align 4,,10
	.p2align 3
.L2626:
	addq	$2, %rax
	cmpq	%rax, %rdx
	jbe	.L2651
.L2628:
	cmpw	$0, (%rax)
	jne	.L2626
.L2651:
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	%rdx
	movl	%edx, %eax
	jmp	.L2629
	.p2align 4,,10
	.p2align 3
.L2657:
	movq	24(%rbx), %rsi
	addq	%rdx, %rdx
	jmp	.L2624
.L2640:
	xorl	%eax, %eax
	jmp	.L2625
.L2656:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3000:
	.size	_ZN6icu_6713UnicodeStringC2EPKci, .-_ZN6icu_6713UnicodeStringC2EPKci
	.globl	_ZN6icu_6713UnicodeStringC1EPKci
	.set	_ZN6icu_6713UnicodeStringC1EPKci,_ZN6icu_6713UnicodeStringC2EPKci
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UnicodeStringAppendable14appendCodeUnitEDs
	.type	_ZN6icu_6723UnicodeStringAppendable14appendCodeUnitEDs, @function
_ZN6icu_6723UnicodeStringAppendable14appendCodeUnitEDs:
.LFB3088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movw	%si, -132(%rbp)
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzwl	8(%r12), %eax
	movl	%eax, %r13d
	andw	$17, %r13w
	jne	.L2658
	testw	%ax, %ax
	js	.L2660
	movswl	%ax, %ebx
	sarl	$5, %ebx
.L2661:
	leaq	-116(%rbp), %rdx
	movl	$1, %esi
	movl	%ebx, %edi
	call	uprv_add32_overflow_67@PLT
	testb	%al, %al
	jne	.L2714
	movzwl	8(%r12), %eax
	movl	%eax, %ecx
	andw	$2, %cx
	je	.L2667
	leaq	10(%r12), %rdx
	testb	$25, %al
	jne	.L2715
	testb	$4, %al
	jne	.L2716
.L2671:
	leaq	-130(%rbp), %rax
	leaq	-132(%rbp), %rsi
	cmpq	%rax, %rdx
	jb	.L2673
.L2713:
	movzwl	8(%r12), %eax
	movl	%eax, %edx
	andl	$2, %edx
	testw	%dx, %dx
	jne	.L2717
.L2670:
	movl	16(%r12), %edx
.L2669:
	movl	-116(%rbp), %esi
	cmpl	%edx, %esi
	jle	.L2718
.L2684:
	movl	$2147483637, %ecx
	movl	%esi, %eax
	sarl	$2, %eax
	movl	%ecx, %edi
	subl	$-128, %eax
	subl	%esi, %edi
	cmpl	%edi, %eax
	leal	(%rax,%rsi), %edx
	movq	%r12, %rdi
	cmovg	%ecx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia
	testb	%al, %al
	jne	.L2688
	testb	$17, 8(%r12)
	sete	%r8b
.L2658:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2719
	addq	$120, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2660:
	.cfi_restore_state
	movl	12(%r12), %ebx
	jmp	.L2661
	.p2align 4,,10
	.p2align 3
.L2667:
	movq	24(%r12), %rdx
	testb	$25, %al
	jne	.L2670
	testb	$4, %al
	je	.L2671
	jmp	.L2716
	.p2align 4,,10
	.p2align 3
.L2718:
	testb	$25, %al
	jne	.L2684
	testb	$4, %al
	je	.L2685
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	movl	-116(%rbp), %esi
	cmpl	$1, %eax
	jne	.L2684
	.p2align 4,,10
	.p2align 3
.L2688:
	movzwl	8(%r12), %eax
	movl	%eax, %r13d
	andl	$2, %r13d
.L2685:
	leaq	10(%r12), %rdx
	testw	%r13w, %r13w
	jne	.L2690
	movq	24(%r12), %rdx
.L2690:
	movslq	%ebx, %rbx
	leaq	-132(%rbp), %rcx
	leaq	(%rdx,%rbx,2), %rdx
	cmpq	%rcx, %rdx
	je	.L2691
	movzwl	-132(%rbp), %eax
	movw	%ax, (%rdx)
	movzwl	8(%r12), %eax
.L2691:
	movl	-116(%rbp), %edx
	cmpl	$1023, %edx
	jg	.L2692
	andl	$31, %eax
	sall	$5, %edx
	orl	%eax, %edx
	movw	%dx, 8(%r12)
	andl	$17, %edx
	sete	%r8b
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L2692:
	movl	%eax, %ecx
	movl	%edx, 12(%r12)
	orl	$-32, %ecx
	testb	$17, %al
	movw	%cx, 8(%r12)
	sete	%r8b
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L2673:
	movslq	%ebx, %rax
	leaq	(%rdx,%rax,2), %rax
	cmpq	%rax, %rsi
	jnb	.L2713
	movl	$2, %edx
	leaq	-112(%rbp), %r13
	movl	$1, %ecx
	movw	%dx, -104(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbx, -112(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movzwl	-104(%rbp), %eax
	testb	$1, %al
	jne	.L2720
	movzwl	8(%r12), %edx
	andl	$17, %edx
	testb	$2, %al
	jne	.L2680
	movq	-88(%rbp), %rsi
	testw	%dx, %dx
	jne	.L2679
	testq	%rsi, %rsi
	jne	.L2693
	.p2align 4,,10
	.p2align 3
.L2679:
	movq	%rbx, -112(%rbp)
	testb	$4, %al
	jne	.L2721
.L2682:
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	testb	$17, 8(%r12)
	sete	%r8b
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L2715:
	movl	%ecx, %r13d
	movl	$27, %edx
	jmp	.L2669
	.p2align 4,,10
	.p2align 3
.L2716:
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	cmpl	$1, %eax
	jne	.L2713
	jmp	.L2671
	.p2align 4,,10
	.p2align 3
.L2714:
	testb	$4, 8(%r12)
	jne	.L2722
.L2664:
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	$0, 24(%r12)
	movw	%cx, 8(%r12)
	movl	$0, 16(%r12)
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L2722:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	jne	.L2664
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2664
	.p2align 4,,10
	.p2align 3
.L2717:
	movl	%edx, %r13d
	movl	$27, %edx
	jmp	.L2669
	.p2align 4,,10
	.p2align 3
.L2680:
	leaq	-102(%rbp), %rsi
	testw	%dx, %dx
	jne	.L2679
.L2693:
	movq	%r12, %rdi
	movl	$1, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movq	%rax, %r12
	movzwl	-104(%rbp), %eax
	jmp	.L2679
	.p2align 4,,10
	.p2align 3
.L2721:
	movq	-88(%rbp), %rax
	lock subl	$1, -4(%rax)
	jne	.L2682
	movq	-88(%rbp), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2682
.L2720:
	testb	$4, 8(%r12)
	jne	.L2723
.L2677:
	movq	$0, 24(%r12)
	movl	$1, %eax
	movw	%ax, 8(%r12)
	movzwl	-104(%rbp), %eax
	movl	$0, 16(%r12)
	jmp	.L2679
.L2723:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	jne	.L2677
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2677
.L2719:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3088:
	.size	_ZN6icu_6723UnicodeStringAppendable14appendCodeUnitEDs, .-_ZN6icu_6723UnicodeStringAppendable14appendCodeUnitEDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeStringC2EPKDs
	.type	_ZN6icu_6713UnicodeStringC2EPKDs, @function
_ZN6icu_6713UnicodeStringC2EPKDs:
.LFB2982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, (%rdi)
	movw	%r8w, 8(%rdi)
	testq	%rsi, %rsi
	je	.L2724
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	u_strlen_67@PLT
	movl	%eax, %r15d
	testl	%eax, %eax
	je	.L2724
	movswl	8(%r12), %r14d
	testw	%r14w, %r14w
	js	.L2728
	sarl	$5, %r14d
.L2729:
	leaq	-132(%rbp), %rdx
	movl	%r15d, %esi
	movl	%r14d, %edi
	call	uprv_add32_overflow_67@PLT
	testb	%al, %al
	jne	.L2800
	movzwl	8(%r12), %eax
	movl	%eax, %edx
	andw	$2, %dx
	je	.L2734
	leaq	10(%r12), %rcx
	testb	$25, %al
	je	.L2735
.L2798:
	movl	$27, %ecx
.L2736:
	movl	-132(%rbp), %esi
	cmpl	%ecx, %esi
	jle	.L2801
.L2751:
	movl	$2147483637, %ecx
	movl	%esi, %eax
	sarl	$2, %eax
	movl	%ecx, %edi
	subl	$-128, %eax
	subl	%esi, %edi
	cmpl	%edi, %eax
	leal	(%rsi,%rax), %edx
	movq	%r12, %rdi
	cmovg	%ecx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia
	testb	%al, %al
	je	.L2724
.L2799:
	movzwl	8(%r12), %eax
	movl	%eax, %edx
	andl	$2, %edx
.L2752:
	testw	%dx, %dx
	je	.L2756
	leaq	10(%r12), %rdx
.L2757:
	movslq	%r14d, %r14
	leaq	(%rdx,%r14,2), %rdi
	cmpq	%rdi, %r13
	je	.L2758
	testl	%r15d, %r15d
	jg	.L2802
.L2758:
	movl	-132(%rbp), %edx
	cmpl	$1023, %edx
	jg	.L2759
	andl	$31, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movw	%ax, 8(%r12)
	.p2align 4,,10
	.p2align 3
.L2724:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2803
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2728:
	.cfi_restore_state
	movl	12(%r12), %r14d
	jmp	.L2729
	.p2align 4,,10
	.p2align 3
.L2734:
	movq	24(%r12), %rcx
	testb	$25, %al
	jne	.L2737
.L2735:
	testb	$4, %al
	jne	.L2804
.L2738:
	movslq	%r15d, %rax
	leaq	0(%r13,%rax,2), %rax
	cmpq	%rax, %rcx
	jnb	.L2742
	movslq	%r14d, %rax
	leaq	(%rcx,%rax,2), %rax
	cmpq	%rax, %r13
	jb	.L2805
.L2742:
	movzwl	8(%r12), %eax
	movl	%eax, %edx
	andl	$2, %edx
	testw	%dx, %dx
	jne	.L2798
.L2737:
	movl	16(%r12), %ecx
	xorl	%edx, %edx
	jmp	.L2736
	.p2align 4,,10
	.p2align 3
.L2801:
	testb	$25, %al
	jne	.L2751
	testb	$4, %al
	je	.L2752
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	movl	-132(%rbp), %esi
	cmpl	$1, %eax
	jne	.L2751
	jmp	.L2799
	.p2align 4,,10
	.p2align 3
.L2759:
	orl	$-32, %eax
	movl	%edx, 12(%r12)
	movw	%ax, 8(%r12)
	jmp	.L2724
	.p2align 4,,10
	.p2align 3
.L2756:
	movq	24(%r12), %rdx
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L2800:
	testb	$4, 8(%r12)
	jne	.L2806
.L2732:
	movq	$0, 24(%r12)
	movl	$1, %esi
	movw	%si, 8(%r12)
	movl	$0, 16(%r12)
	jmp	.L2724
	.p2align 4,,10
	.p2align 3
.L2802:
	movslq	%r15d, %rdx
	movq	%r13, %rsi
	addq	%rdx, %rdx
	call	memmove@PLT
	movzwl	8(%r12), %eax
	jmp	.L2758
	.p2align 4,,10
	.p2align 3
.L2806:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	jne	.L2732
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2732
	.p2align 4,,10
	.p2align 3
.L2805:
	movl	$2, %ecx
	leaq	-128(%rbp), %r14
	xorl	%edx, %edx
	movq	%r13, %rsi
	movw	%cx, -120(%rbp)
	movq	%r14, %rdi
	movl	%r15d, %ecx
	movq	%rbx, -128(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movzwl	-120(%rbp), %eax
	testb	$1, %al
	jne	.L2807
	movzwl	8(%r12), %edx
	andl	$17, %edx
	testb	$2, %al
	jne	.L2747
	testw	%dx, %dx
	jne	.L2746
	movq	-104(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L2760
	.p2align 4,,10
	.p2align 3
.L2746:
	movq	%rbx, -128(%rbp)
	testb	$4, %al
	jne	.L2808
.L2749:
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	jmp	.L2724
	.p2align 4,,10
	.p2align 3
.L2804:
	movq	24(%r12), %rax
	movl	-4(%rax), %eax
	cmpl	$1, %eax
	jne	.L2742
	jmp	.L2738
.L2747:
	leaq	-118(%rbp), %rsi
	testw	%dx, %dx
	jne	.L2746
.L2760:
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movzwl	-120(%rbp), %eax
	jmp	.L2746
.L2808:
	movq	-104(%rbp), %rax
	lock subl	$1, -4(%rax)
	jne	.L2749
	movq	-104(%rbp), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2749
.L2807:
	testb	$4, 8(%r12)
	jne	.L2809
.L2744:
	movq	$0, 24(%r12)
	movl	$1, %edx
	movw	%dx, 8(%r12)
	movl	$0, 16(%r12)
	jmp	.L2746
.L2809:
	movq	24(%r12), %rax
	lock subl	$1, -4(%rax)
	je	.L2745
.L2797:
	movzwl	-120(%rbp), %eax
	jmp	.L2744
.L2803:
	call	__stack_chk_fail@PLT
.L2745:
	movq	24(%r12), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2797
	.cfi_endproc
.LFE2982:
	.size	_ZN6icu_6713UnicodeStringC2EPKDs, .-_ZN6icu_6713UnicodeStringC2EPKDs
	.globl	_ZN6icu_6713UnicodeStringC1EPKDs
	.set	_ZN6icu_6713UnicodeStringC1EPKDs,_ZN6icu_6713UnicodeStringC2EPKDs
	.align 2
	.p2align 4
	.globl	_ZN6icu_6723UnicodeStringAppendable12appendStringEPKDsi
	.type	_ZN6icu_6723UnicodeStringAppendable12appendStringEPKDsi, @function
_ZN6icu_6723UnicodeStringAppendable12appendStringEPKDsi:
.LFB3090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	8(%r13), %edx
	movl	%edx, %r15d
	andw	$17, %r15w
	jne	.L2810
	testl	%r12d, %r12d
	je	.L2850
	movq	%rsi, %r14
	testq	%rsi, %rsi
	je	.L2850
	testl	%r12d, %r12d
	js	.L2879
.L2812:
	testw	%dx, %dx
	js	.L2815
	movswl	%dx, %ebx
	sarl	$5, %ebx
.L2816:
	leaq	-132(%rbp), %rdx
	movl	%r12d, %esi
	movl	%ebx, %edi
	call	uprv_add32_overflow_67@PLT
	testb	%al, %al
	jne	.L2880
	movzwl	8(%r13), %eax
	movl	%eax, %ecx
	andw	$2, %cx
	jne	.L2881
	movq	24(%r13), %rdx
	testb	$25, %al
	jne	.L2824
.L2822:
	testb	$4, %al
	jne	.L2882
.L2825:
	movslq	%r12d, %rax
	leaq	(%r14,%rax,2), %rax
	cmpq	%rax, %rdx
	jnb	.L2877
	movslq	%ebx, %rax
	leaq	(%rdx,%rax,2), %rax
	cmpq	%rax, %r14
	jb	.L2828
.L2877:
	movzwl	8(%r13), %eax
	movl	%eax, %edx
	andl	$2, %edx
	testw	%dx, %dx
	jne	.L2883
.L2824:
	movl	16(%r13), %edx
.L2823:
	movl	-132(%rbp), %esi
	cmpl	%edx, %esi
	jle	.L2884
.L2838:
	movl	$2147483637, %ecx
	movl	%esi, %eax
	sarl	$2, %eax
	movl	%ecx, %edi
	subl	$-128, %eax
	subl	%esi, %edi
	cmpl	%edi, %eax
	leal	(%rax,%rsi), %edx
	movq	%r13, %rdi
	cmovg	%ecx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString18cloneArrayIfNeededEiiaPPia
	testb	%al, %al
	je	.L2878
.L2842:
	movzwl	8(%r13), %eax
	movl	%eax, %r15d
	andl	$2, %r15d
.L2839:
	leaq	10(%r13), %rdx
	testw	%r15w, %r15w
	jne	.L2844
	movq	24(%r13), %rdx
.L2844:
	movslq	%ebx, %rbx
	leaq	(%rdx,%rbx,2), %rdi
	testl	%r12d, %r12d
	jle	.L2845
	cmpq	%rdi, %r14
	je	.L2845
	movslq	%r12d, %rdx
	movq	%r14, %rsi
	addq	%rdx, %rdx
	call	memmove@PLT
	movzwl	8(%r13), %eax
.L2845:
	movl	-132(%rbp), %edx
	cmpl	$1023, %edx
	jg	.L2846
	andl	$31, %eax
	sall	$5, %edx
	orl	%eax, %edx
	movw	%dx, 8(%r13)
	andl	$17, %edx
	sete	%al
	.p2align 4,,10
	.p2align 3
.L2810:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2885
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2850:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L2810
	.p2align 4,,10
	.p2align 3
.L2881:
	leaq	10(%r13), %rdx
	testb	$25, %al
	je	.L2822
	movl	%ecx, %r15d
	movl	$27, %edx
	jmp	.L2823
	.p2align 4,,10
	.p2align 3
.L2828:
	movl	$2, %edx
	leaq	-128(%rbp), %r15
	movl	%r12d, %ecx
	movq	%r14, %rsi
	movw	%dx, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rbx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rbx, -128(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movzwl	-120(%rbp), %eax
	testb	$1, %al
	jne	.L2886
	movzwl	8(%r13), %edx
	andl	$17, %edx
	testb	$2, %al
	jne	.L2834
	testw	%dx, %dx
	jne	.L2833
	movq	-104(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L2847
	.p2align 4,,10
	.p2align 3
.L2833:
	movq	%rbx, -128(%rbp)
	testb	$4, %al
	jne	.L2887
.L2836:
	leaq	16+_ZTVN6icu_6711ReplaceableE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L2878:
	testb	$17, 8(%r13)
	sete	%al
	jmp	.L2810
	.p2align 4,,10
	.p2align 3
.L2815:
	movl	12(%r13), %ebx
	jmp	.L2816
	.p2align 4,,10
	.p2align 3
.L2879:
	movq	%rsi, %rdi
	call	u_strlen_67@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L2878
	movzwl	8(%r13), %edx
	jmp	.L2812
	.p2align 4,,10
	.p2align 3
.L2884:
	testb	$25, %al
	jne	.L2838
	testb	$4, %al
	je	.L2839
	movq	24(%r13), %rax
	movl	-4(%rax), %eax
	movl	-132(%rbp), %esi
	cmpl	$1, %eax
	jne	.L2838
	jmp	.L2842
	.p2align 4,,10
	.p2align 3
.L2846:
	movl	%eax, %ecx
	movl	%edx, 12(%r13)
	orl	$-32, %ecx
	testb	$17, %al
	movw	%cx, 8(%r13)
	sete	%al
	jmp	.L2810
	.p2align 4,,10
	.p2align 3
.L2880:
	testb	$4, 8(%r13)
	jne	.L2888
.L2819:
	movl	$1, %ecx
	movq	$0, 24(%r13)
	xorl	%eax, %eax
	movw	%cx, 8(%r13)
	movl	$0, 16(%r13)
	jmp	.L2810
	.p2align 4,,10
	.p2align 3
.L2882:
	movq	24(%r13), %rax
	movl	-4(%rax), %eax
	cmpl	$1, %eax
	jne	.L2877
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2888:
	movq	24(%r13), %rax
	lock subl	$1, -4(%rax)
	jne	.L2819
	movq	24(%r13), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2819
	.p2align 4,,10
	.p2align 3
.L2883:
	movl	%edx, %r15d
	movl	$27, %edx
	jmp	.L2823
	.p2align 4,,10
	.p2align 3
.L2834:
	leaq	-118(%rbp), %rsi
	testw	%dx, %dx
	jne	.L2833
.L2847:
	movq	%r13, %rdi
	movl	%r12d, %ecx
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii.part.0
	movq	%rax, %r13
	movzwl	-120(%rbp), %eax
	jmp	.L2833
.L2887:
	movq	-104(%rbp), %rax
	lock subl	$1, -4(%rax)
	jne	.L2836
	movq	-104(%rbp), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2836
.L2886:
	testb	$4, 8(%r13)
	jne	.L2889
.L2831:
	movl	$1, %eax
	movq	$0, 24(%r13)
	movw	%ax, 8(%r13)
	movzwl	-120(%rbp), %eax
	movl	$0, 16(%r13)
	jmp	.L2833
.L2889:
	movq	24(%r13), %rax
	lock subl	$1, -4(%rax)
	jne	.L2831
	movq	24(%r13), %rax
	leaq	-4(%rax), %rdi
	call	uprv_free_67@PLT
	jmp	.L2831
.L2885:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3090:
	.size	_ZN6icu_6723UnicodeStringAppendable12appendStringEPKDsi, .-_ZN6icu_6723UnicodeStringAppendable12appendStringEPKDsi
	.p2align 4
	.globl	uhash_hashUnicodeString_67
	.type	uhash_hashUnicodeString_67, @function
uhash_hashUnicodeString_67:
.LFB3093:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2898
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	8(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testw	%ax, %ax
	js	.L2892
	movswl	%ax, %esi
	sarl	$5, %esi
	testb	$2, %al
	je	.L2894
.L2903:
	addq	$10, %rdi
.L2895:
	call	ustr_hashUCharsN_67@PLT
	movl	$1, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2892:
	.cfi_restore_state
	movl	12(%rdi), %esi
	testb	$2, %al
	jne	.L2903
.L2894:
	movq	24(%rdi), %rdi
	jmp	.L2895
	.p2align 4,,10
	.p2align 3
.L2898:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3093:
	.size	uhash_hashUnicodeString_67, .-uhash_hashUnicodeString_67
	.p2align 4
	.globl	uhash_compareUnicodeString_67
	.type	uhash_compareUnicodeString_67, @function
uhash_compareUnicodeString_67:
.LFB3094:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L2915
	testq	%rdi, %rdi
	je	.L2916
	testq	%rsi, %rsi
	je	.L2916
	movzwl	8(%rdi), %edx
	movzwl	8(%rsi), %ecx
	movl	%edx, %r10d
	movl	%ecx, %eax
	andl	$1, %eax
	andl	$1, %r10d
	je	.L2928
	ret
	.p2align 4,,10
	.p2align 3
.L2928:
	testw	%dx, %dx
	js	.L2906
	movswl	%dx, %r8d
	sarl	$5, %r8d
	testw	%cx, %cx
	js	.L2908
.L2930:
	movswl	%cx, %r9d
	sarl	$5, %r9d
.L2909:
	cmpl	%r8d, %r9d
	jne	.L2925
	testb	$1, %al
	je	.L2929
.L2925:
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2916:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2906:
	movl	12(%rdi), %r8d
	testw	%cx, %cx
	jns	.L2930
.L2908:
	movl	12(%rsi), %r9d
	jmp	.L2909
	.p2align 4,,10
	.p2align 3
.L2915:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2929:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addl	%r8d, %r8d
	andl	$2, %ecx
	movslq	%r8d, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	je	.L2911
	addq	$10, %rsi
.L2912:
	andl	$2, %edx
	je	.L2913
	addq	$10, %rdi
.L2914:
	movq	%r8, %rdx
	call	memcmp@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%r10b
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2911:
	.cfi_restore_state
	movq	24(%rsi), %rsi
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L2913:
	movq	24(%rdi), %rdi
	jmp	.L2914
	.cfi_endproc
.LFE3094:
	.size	uhash_compareUnicodeString_67, .-uhash_compareUnicodeString_67
	.weak	_ZTSN6icu_6723UnicodeStringAppendableE
	.section	.rodata._ZTSN6icu_6723UnicodeStringAppendableE,"aG",@progbits,_ZTSN6icu_6723UnicodeStringAppendableE,comdat
	.align 32
	.type	_ZTSN6icu_6723UnicodeStringAppendableE, @object
	.size	_ZTSN6icu_6723UnicodeStringAppendableE, 35
_ZTSN6icu_6723UnicodeStringAppendableE:
	.string	"N6icu_6723UnicodeStringAppendableE"
	.weak	_ZTIN6icu_6723UnicodeStringAppendableE
	.section	.data.rel.ro._ZTIN6icu_6723UnicodeStringAppendableE,"awG",@progbits,_ZTIN6icu_6723UnicodeStringAppendableE,comdat
	.align 8
	.type	_ZTIN6icu_6723UnicodeStringAppendableE, @object
	.size	_ZTIN6icu_6723UnicodeStringAppendableE, 24
_ZTIN6icu_6723UnicodeStringAppendableE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6723UnicodeStringAppendableE
	.quad	_ZTIN6icu_6710AppendableE
	.weak	_ZTSN6icu_6711ReplaceableE
	.section	.rodata._ZTSN6icu_6711ReplaceableE,"aG",@progbits,_ZTSN6icu_6711ReplaceableE,comdat
	.align 16
	.type	_ZTSN6icu_6711ReplaceableE, @object
	.size	_ZTSN6icu_6711ReplaceableE, 23
_ZTSN6icu_6711ReplaceableE:
	.string	"N6icu_6711ReplaceableE"
	.weak	_ZTIN6icu_6711ReplaceableE
	.section	.data.rel.ro._ZTIN6icu_6711ReplaceableE,"awG",@progbits,_ZTIN6icu_6711ReplaceableE,comdat
	.align 8
	.type	_ZTIN6icu_6711ReplaceableE, @object
	.size	_ZTIN6icu_6711ReplaceableE, 24
_ZTIN6icu_6711ReplaceableE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6711ReplaceableE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6713UnicodeStringE
	.section	.rodata._ZTSN6icu_6713UnicodeStringE,"aG",@progbits,_ZTSN6icu_6713UnicodeStringE,comdat
	.align 16
	.type	_ZTSN6icu_6713UnicodeStringE, @object
	.size	_ZTSN6icu_6713UnicodeStringE, 25
_ZTSN6icu_6713UnicodeStringE:
	.string	"N6icu_6713UnicodeStringE"
	.weak	_ZTIN6icu_6713UnicodeStringE
	.section	.data.rel.ro._ZTIN6icu_6713UnicodeStringE,"awG",@progbits,_ZTIN6icu_6713UnicodeStringE,comdat
	.align 8
	.type	_ZTIN6icu_6713UnicodeStringE, @object
	.size	_ZTIN6icu_6713UnicodeStringE, 24
_ZTIN6icu_6713UnicodeStringE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713UnicodeStringE
	.quad	_ZTIN6icu_6711ReplaceableE
	.weak	_ZTVN6icu_6711ReplaceableE
	.section	.data.rel.ro._ZTVN6icu_6711ReplaceableE,"awG",@progbits,_ZTVN6icu_6711ReplaceableE,comdat
	.align 8
	.type	_ZTVN6icu_6711ReplaceableE, @object
	.size	_ZTVN6icu_6711ReplaceableE, 104
_ZTVN6icu_6711ReplaceableE:
	.quad	0
	.quad	_ZTIN6icu_6711ReplaceableE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6711Replaceable11hasMetaDataEv
	.quad	_ZNK6icu_6711Replaceable5cloneEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6713UnicodeStringE
	.section	.data.rel.ro.local._ZTVN6icu_6713UnicodeStringE,"awG",@progbits,_ZTVN6icu_6713UnicodeStringE,comdat
	.align 8
	.type	_ZTVN6icu_6713UnicodeStringE, @object
	.size	_ZTVN6icu_6713UnicodeStringE, 104
_ZTVN6icu_6713UnicodeStringE:
	.quad	0
	.quad	_ZTIN6icu_6713UnicodeStringE
	.quad	_ZN6icu_6713UnicodeStringD1Ev
	.quad	_ZN6icu_6713UnicodeStringD0Ev
	.quad	_ZNK6icu_6713UnicodeString17getDynamicClassIDEv
	.quad	_ZNK6icu_6713UnicodeString14extractBetweenEiiRS0_
	.quad	_ZN6icu_6713UnicodeString20handleReplaceBetweenEiiRKS0_
	.quad	_ZN6icu_6713UnicodeString4copyEiii
	.quad	_ZNK6icu_6713UnicodeString11hasMetaDataEv
	.quad	_ZNK6icu_6713UnicodeString5cloneEv
	.quad	_ZNK6icu_6713UnicodeString9getLengthEv
	.quad	_ZNK6icu_6713UnicodeString9getCharAtEi
	.quad	_ZNK6icu_6713UnicodeString11getChar32AtEi
	.weak	_ZTVN6icu_6723UnicodeStringAppendableE
	.section	.data.rel.ro._ZTVN6icu_6723UnicodeStringAppendableE,"awG",@progbits,_ZTVN6icu_6723UnicodeStringAppendableE,comdat
	.align 8
	.type	_ZTVN6icu_6723UnicodeStringAppendableE, @object
	.size	_ZTVN6icu_6723UnicodeStringAppendableE, 80
_ZTVN6icu_6723UnicodeStringAppendableE:
	.quad	0
	.quad	_ZTIN6icu_6723UnicodeStringAppendableE
	.quad	_ZN6icu_6723UnicodeStringAppendableD1Ev
	.quad	_ZN6icu_6723UnicodeStringAppendableD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZN6icu_6723UnicodeStringAppendable14appendCodeUnitEDs
	.quad	_ZN6icu_6723UnicodeStringAppendable15appendCodePointEi
	.quad	_ZN6icu_6723UnicodeStringAppendable12appendStringEPKDsi
	.quad	_ZN6icu_6723UnicodeStringAppendable21reserveAppendCapacityEi
	.quad	_ZN6icu_6723UnicodeStringAppendable15getAppendBufferEiiPDsiPi
	.local	_ZZN6icu_6713UnicodeString16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6713UnicodeString16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
