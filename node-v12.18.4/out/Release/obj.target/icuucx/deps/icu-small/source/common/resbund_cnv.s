	.file	"resbund_cnv.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundle18constructForLocaleERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714ResourceBundle18constructForLocaleERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714ResourceBundle18constructForLocaleERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode:
.LFB2266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movswl	8(%rsi), %eax
	shrl	$5, %eax
	jne	.L2
	movq	40(%rdx), %rsi
	xorl	%edi, %edi
	movq	%rcx, %rdx
	call	ures_open_67@PLT
	movq	%rax, 8(%rbx)
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L10
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	xorl	%eax, %eax
	leaq	-114(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movw	%ax, -114(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	-104(%rbp), %eax
	movq	40(%r13), %rsi
	testb	$17, %al
	jne	.L6
	leaq	-102(%rbp), %rdi
	testb	$2, %al
	cmove	-88(%rbp), %rdi
.L4:
	movq	%r12, %rdx
	call	ures_openU_67@PLT
	movq	%r14, %rdi
	movq	%rax, 8(%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%edi, %edi
	jmp	.L4
.L10:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2266:
	.size	_ZN6icu_6714ResourceBundle18constructForLocaleERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714ResourceBundle18constructForLocaleERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode:
.LFB2261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	$0, 16(%rdi)
	movq	%rax, (%rdi)
	movswl	8(%rsi), %eax
	shrl	$5, %eax
	jne	.L12
	movq	40(%rdx), %rsi
	xorl	%edi, %edi
	movq	%rcx, %rdx
	call	ures_open_67@PLT
	movq	%rax, 8(%rbx)
.L11:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	xorl	%eax, %eax
	leaq	-114(%rbp), %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movw	%ax, -114(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	-104(%rbp), %eax
	movq	40(%r13), %rsi
	testb	$17, %al
	jne	.L16
	leaq	-102(%rbp), %rdi
	testb	$2, %al
	cmove	-88(%rbp), %rdi
.L14:
	movq	%r12, %rdx
	call	ures_openU_67@PLT
	movq	%r14, %rdi
	movq	%rax, 8(%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%edi, %edi
	jmp	.L14
.L19:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2261:
	.size	_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.globl	_ZN6icu_6714ResourceBundleC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.set	_ZN6icu_6714ResourceBundleC1ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode,_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringER10UErrorCode:
.LFB2264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6714ResourceBundleE(%rip), %rax
	movq	$0, 16(%rdi)
	movq	%rax, (%rdi)
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	movq	%rax, %r14
	movswl	8(%r12), %eax
	shrl	$5, %eax
	jne	.L21
	movq	40(%r14), %rsi
	movq	%r13, %rdx
	xorl	%edi, %edi
	call	ures_open_67@PLT
	movq	%rax, 8(%rbx)
.L20:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	leaq	-128(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	xorl	%eax, %eax
	leaq	-130(%rbp), %rsi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rdi
	movw	%ax, -130(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzwl	-120(%rbp), %eax
	movq	40(%r14), %rsi
	testb	$17, %al
	jne	.L25
	leaq	-118(%rbp), %rdi
	testb	$2, %al
	cmove	-104(%rbp), %rdi
.L23:
	movq	%r13, %rdx
	call	ures_openU_67@PLT
	movq	%r15, %rdi
	movq	%rax, 8(%rbx)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%edi, %edi
	jmp	.L23
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2264:
	.size	_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6714ResourceBundleC1ERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6714ResourceBundleC1ERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6714ResourceBundleC2ERKNS_13UnicodeStringER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
