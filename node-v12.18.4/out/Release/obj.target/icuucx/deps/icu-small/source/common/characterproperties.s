	.file	"characterproperties.cpp"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_18_set_addEP4USeti, @function
_ZN12_GLOBAL__N_18_set_addEP4USeti:
.LFB3151:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet3addEi@PLT
	.cfi_endproc
.LFE3151:
	.size	_ZN12_GLOBAL__N_18_set_addEP4USeti, .-_ZN12_GLOBAL__N_18_set_addEP4USeti
	.p2align 4
	.type	_ZN12_GLOBAL__N_127characterproperties_cleanupEv, @function
_ZN12_GLOBAL__N_127characterproperties_cleanupEv:
.LFB3154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	8+_ZN12_GLOBAL__N_111gInclusionsE(%rip), %rbx
	leaq	640(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L7:
	movq	-8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movq	$0, -8(%rbx)
.L4:
	addq	$16, %rbx
	movl	$0, -16(%rbx)
	mfence
	cmpq	%r12, %rbx
	jne	.L7
	leaq	_ZN12_GLOBAL__N_14setsE(%rip), %rbx
	leaq	520(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	addq	$8, %rbx
	movq	$0, -8(%rbx)
	cmpq	%r12, %rbx
	jne	.L11
.L9:
	leaq	_ZN12_GLOBAL__N_14mapsE(%rip), %rbx
	leaq	200(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	ucptrie_close_67@PLT
	movq	$0, -8(%rbx)
	cmpq	%r12, %rbx
	jne	.L12
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.L11
	jmp	.L9
	.cfi_endproc
.LFE3154:
	.size	_ZN12_GLOBAL__N_127characterproperties_cleanupEv, .-_ZN12_GLOBAL__N_127characterproperties_cleanupEv
	.p2align 4
	.type	_ZN12_GLOBAL__N_113initInclusionE15UPropertySourceR10UErrorCode, @function
_ZN12_GLOBAL__N_113initInclusionE15UPropertySourceR10UErrorCode:
.LFB3155:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jne	.L17
	movl	$5, (%rsi)
.L16:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	%edi, %ebx
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L19
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	pxor	%xmm0, %xmm0
	leaq	_ZN12_GLOBAL__N_113_set_addRangeEP4USetii(%rip), %rax
	leaq	_ZN12_GLOBAL__N_18_set_addEP4USeti(%rip), %rcx
	movq	%rax, %xmm1
	movaps	%xmm0, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	_ZN12_GLOBAL__N_114_set_addStringEP4USetPKDsi(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -72(%rbp)
	movq	%r13, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	cmpl	$14, %ebx
	ja	.L20
	leaq	.L22(%rip), %rcx
	movl	%ebx, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L22:
	.long	.L20-.L22
	.long	.L32-.L22
	.long	.L31-.L22
	.long	.L20-.L22
	.long	.L30-.L22
	.long	.L29-.L22
	.long	.L28-.L22
	.long	.L27-.L22
	.long	.L26-.L22
	.long	.L25-.L22
	.long	.L24-.L22
	.long	.L23-.L22
	.long	.L21-.L22
	.long	.L21-.L22
	.long	.L21-.L22
	.text
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdx
	movl	%ebx, %edi
	call	uprops_addPropertyStarts_67@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L38
	testb	$1, 32(%r13)
	je	.L37
	movl	$7, (%r12)
.L38:
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	uchar_addPropertyStarts_67@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	upropsvec_addPropertyStarts_67@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	ucase_addPropertyStarts_67@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	ubidi_addPropertyStarts_67@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	-96(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	uchar_addPropertyStarts_67@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	upropsvec_addPropertyStarts_67@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r12, %rdi
	leaq	-96(%rbp), %r14
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	movl	(%r12), %r8d
	testl	%r8d, %r8d
	jle	.L46
.L34:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	ucase_addPropertyStarts_67@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r12, %rdi
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L38
.L44:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	_ZNK6icu_6715Normalizer2Impl17addPropertyStartsEPK9USetAdderR10UErrorCode@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r12, %rdi
	call	_ZN6icu_6718Normalizer2Factory14getNFKC_CFImplER10UErrorCode@PLT
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L38
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r12, %rdi
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L38
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	_ZNK6icu_6715Normalizer2Impl26addCanonIterPropertyStartsEPK9USetAdderR10UErrorCode@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r12, %rdi
	call	_ZN6icu_6718Normalizer2Factory11getNFKCImplER10UErrorCode@PLT
	movl	(%r12), %esi
	testl	%esi, %esi
	jg	.L38
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$5, (%r12)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r13, %rdi
	movslq	%ebx, %rbx
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	salq	$4, %rbx
	movl	$14, %edi
	leaq	_ZN12_GLOBAL__N_111gInclusionsE(%rip), %rax
	movq	%r13, (%rax,%rbx)
	leaq	_ZN12_GLOBAL__N_127characterproperties_cleanupEv(%rip), %rsi
	call	ucln_common_registerCleanup_67@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZNK6icu_6715Normalizer2Impl17addPropertyStartsEPK9USetAdderR10UErrorCode@PLT
	jmp	.L34
.L45:
	call	__stack_chk_fail@PLT
.L19:
	movl	$7, (%r12)
	jmp	.L16
	.cfi_endproc
.LFE3155:
	.size	_ZN12_GLOBAL__N_113initInclusionE15UPropertySourceR10UErrorCode, .-_ZN12_GLOBAL__N_113initInclusionE15UPropertySourceR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode
	.type	_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode, @function
_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode:
.LFB3160:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	testl	%edx, %edx
	jg	.L97
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-4096(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	cmpl	$24, %eax
	jbe	.L98
	call	uprops_getSource_67@PLT
	movl	%eax, %r12d
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L96
	cmpl	$14, %r12d
	ja	.L99
	movl	%r12d, %eax
	leaq	_ZN12_GLOBAL__N_111gInclusionsE(%rip), %r13
	salq	$4, %rax
	leaq	8(%r13,%rax), %r14
	movl	(%r14), %eax
	cmpl	$2, %eax
	jne	.L100
.L69:
	movslq	%r12d, %r12
	movq	%r12, %rax
	salq	$4, %rax
	movl	12(%r13,%rax), %eax
	testl	%eax, %eax
	jle	.L70
	movl	%eax, (%rbx)
.L70:
	salq	$4, %r12
	movq	0(%r13,%r12), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movl	$1, (%rbx)
.L96:
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	leal	-4081(%rdi), %r12d
	leaq	_ZN12_GLOBAL__N_111gInclusionsE(%rip), %r13
	movslq	%r12d, %rax
	movq	%rax, -64(%rbp)
	salq	$4, %rax
	leaq	8(%r13,%rax), %rdi
	movq	%rdi, -88(%rbp)
	movl	(%rdi), %eax
	cmpl	$2, %eax
	jne	.L101
.L51:
	movq	-64(%rbp), %rax
	salq	$4, %rax
	movl	12(%r13,%rax), %eax
	testl	%eax, %eax
	jle	.L66
	movl	%eax, (%rbx)
.L66:
	movq	-64(%rbp), %r12
	salq	$4, %r12
	movq	0(%r13,%r12), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L69
	movl	%r12d, %edi
	movq	%rbx, %rsi
	movslq	%r12d, %r12
	call	_ZN12_GLOBAL__N_113initInclusionE15UPropertySourceR10UErrorCode
	movl	(%rbx), %edx
	movq	%r12, %rax
	movq	%r14, %rdi
	salq	$4, %rax
	movl	%edx, 12(%r13,%rax)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L101:
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L51
	movl	%r15d, %edi
	call	uprops_getSource_67@PLT
	movl	(%rbx), %edx
	movl	%eax, %r14d
	testl	%edx, %edx
	jg	.L57
	cmpl	$14, %eax
	ja	.L102
	movl	%eax, %eax
	salq	$4, %rax
	leaq	8(%r13,%rax), %r12
	movl	(%r12), %eax
	cmpl	$2, %eax
	je	.L54
	movq	%r12, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L54
	movl	%r14d, %edi
	movq	%rbx, %rsi
	movslq	%r14d, %r14
	call	_ZN12_GLOBAL__N_113initInclusionE15UPropertySourceR10UErrorCode
	movl	(%rbx), %edx
	movq	%r14, %rax
	movq	%r12, %rdi
	salq	$4, %rax
	movl	%edx, 12(%r13,%rax)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movl	(%rbx), %edx
.L55:
	testl	%edx, %edx
	jg	.L57
	salq	$4, %r14
	movl	$200, %edi
	movq	0(%r13,%r14), %r14
	movq	%r14, -96(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L58
	xorl	%edx, %edx
	movq	%rax, %rdi
	xorl	%esi, %esi
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	movq	%r14, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	$0, -68(%rbp)
	xorl	%edx, %edx
	movl	%eax, -72(%rbp)
	testl	%eax, %eax
	jle	.L65
	.p2align 4,,10
	.p2align 3
.L59:
	movq	-96(%rbp), %r12
	movl	-68(%rbp), %esi
	movl	%edx, -52(%rbp)
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	-68(%rbp), %esi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	-52(%rbp), %edx
	cmpl	%eax, %r14d
	movl	%eax, %r12d
	jl	.L62
	leal	1(%r14), %eax
	movl	%eax, -52(%rbp)
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%r15d, %esi
	movl	%r12d, %edi
	movl	%edx, %r14d
	call	u_getIntPropertyValue_67@PLT
	movl	%eax, %edx
	cmpl	%eax, %r14d
	je	.L63
	movq	-80(%rbp), %rdi
	movl	%r12d, %esi
	movl	%eax, -56(%rbp)
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	-56(%rbp), %edx
.L63:
	addl	$1, %r12d
	cmpl	-52(%rbp), %r12d
	jne	.L64
.L62:
	addl	$1, -68(%rbp)
	movl	-68(%rbp), %eax
	cmpl	%eax, -72(%rbp)
	jne	.L59
.L65:
	movq	-80(%rbp), %rax
	testb	$1, 32(%rax)
	je	.L103
	movl	$7, (%rbx)
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movl	(%rbx), %edx
.L57:
	movq	-64(%rbp), %rax
	movq	-88(%rbp), %rdi
	salq	$4, %rax
	movl	%edx, 12(%r13,%rax)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
.L54:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movslq	%r14d, %r14
	movq	%r14, %rax
	salq	$4, %rax
	movl	12(%r13,%rax), %edx
	testl	%edx, %edx
	jle	.L104
	movl	%edx, (%rbx)
	jmp	.L57
.L102:
	movl	$1, (%rbx)
	movl	$1, %edx
	jmp	.L57
.L103:
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN6icu_6710UnicodeSet7compactEv@PLT
	movq	-64(%rbp), %rax
	movl	$14, %edi
	leaq	_ZN12_GLOBAL__N_127characterproperties_cleanupEv(%rip), %rsi
	salq	$4, %rax
	movq	%r14, 0(%r13,%rax)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %edx
	jmp	.L57
.L104:
	movl	(%rbx), %edx
	jmp	.L55
.L58:
	movl	$7, (%rbx)
	movl	$7, %edx
	jmp	.L57
	.cfi_endproc
.LFE3160:
	.size	_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode, .-_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode
	.p2align 4
	.globl	u_getIntPropertyMap_67
	.type	u_getIntPropertyMap_67, @function
u_getIntPropertyMap_67:
.LFB3164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jg	.L105
	leal	-4096(%rdi), %ebx
	movl	%edi, %r13d
	movq	%rsi, %r12
	cmpl	$24, %ebx
	ja	.L133
	leaq	_ZN12_GLOBAL__N_17cpMutexE(%rip), %rdi
	call	umtx_lock_67@PLT
	movslq	%ebx, %rax
	leaq	_ZN12_GLOBAL__N_14mapsE(%rip), %rdx
	movq	(%rdx,%rax,8), %r14
	movq	%rax, -88(%rbp)
	testq	%r14, %r14
	je	.L134
.L108:
	leaq	_ZN12_GLOBAL__N_17cpMutexE(%rip), %rdi
	call	umtx_unlock_67@PLT
.L105:
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movl	$1, (%rsi)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L134:
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L109
	cmpl	$4106, %r13d
	movl	$103, %edx
	movl	$0, %eax
	cmove	%edx, %eax
	movq	%r12, %rdx
	movl	%eax, %esi
	movl	%eax, %edi
	movl	%eax, -60(%rbp)
	call	umutablecptrie_open_67@PLT
	movq	%r12, %rsi
	movl	%r13d, %edi
	movq	%rax, -72(%rbp)
	call	_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode
	movl	(%r12), %edx
	movq	%rax, -96(%rbp)
	testl	%edx, %edx
	jle	.L135
.L111:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L109
	movq	%rax, %rdi
	call	umutablecptrie_close_67@PLT
.L109:
	movq	-88(%rbp), %rcx
	leaq	_ZN12_GLOBAL__N_14mapsE(%rip), %rax
	movq	%r14, (%rax,%rcx,8)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%rax, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	-60(%rbp), %r15d
	movl	%eax, -64(%rbp)
	testl	%eax, %eax
	jle	.L121
	movl	$0, -56(%rbp)
	movq	%r12, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-96(%rbp), %r12
	movl	-56(%rbp), %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	-56(%rbp), %esi
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	cmpl	%eax, %ebx
	jl	.L113
	addl	$1, %ebx
	movl	%ebx, %r12d
	movl	%eax, %ebx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L136:
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdi
	movl	%r15d, %ecx
	movl	%r14d, %esi
	leal	-1(%rbx), %edx
	movl	%eax, -52(%rbp)
	movl	%ebx, %r14d
	call	umutablecptrie_setRange_67@PLT
	movl	-52(%rbp), %eax
	movl	%eax, %r15d
.L114:
	addl	$1, %ebx
	cmpl	%ebx, %r12d
	je	.L113
.L115:
	movl	%r13d, %esi
	movl	%ebx, %edi
	call	u_getIntPropertyValue_67@PLT
	cmpl	%r15d, %eax
	je	.L114
	cmpl	%r15d, -60(%rbp)
	jne	.L136
	movl	%ebx, %r14d
	addl	$1, %ebx
	movl	%eax, %r15d
	cmpl	%ebx, %r12d
	jne	.L115
.L113:
	addl	$1, -56(%rbp)
	movl	-56(%rbp), %eax
	cmpl	%eax, -64(%rbp)
	jne	.L116
	movq	-80(%rbp), %r12
.L112:
	testl	%r15d, %r15d
	jne	.L137
.L117:
	cmpl	$4096, %r13d
	movl	%r13d, %edi
	sete	%r14b
	cmpl	$4101, %r13d
	sete	%sil
	orl	%esi, %r14d
	call	u_getIntPropertyMaxValue_67@PLT
	xorl	$1, %r14d
	movl	$2, %edx
	movzbl	%r14b, %r14d
	cmpl	$255, %eax
	jle	.L118
	xorl	%edx, %edx
	cmpl	$65535, %eax
	setg	%dl
.L118:
	movq	-72(%rbp), %rdi
	movl	%r14d, %esi
	movq	%r12, %rcx
	call	umutablecptrie_buildImmutable_67@PLT
	movq	%rax, %r14
	jmp	.L111
.L137:
	movq	-72(%rbp), %rdi
	movq	%r12, %r8
	movl	%r15d, %ecx
	movl	$1114111, %edx
	movl	%r14d, %esi
	call	umutablecptrie_setRange_67@PLT
	jmp	.L117
.L121:
	xorl	%r14d, %r14d
	jmp	.L112
	.cfi_endproc
.LFE3164:
	.size	u_getIntPropertyMap_67, .-u_getIntPropertyMap_67
	.p2align 4
	.type	_ZN12_GLOBAL__N_114_set_addStringEP4USetPKDsi, @function
_ZN12_GLOBAL__N_114_set_addStringEP4USetPKDsi:
.LFB3153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	leaq	-104(%rbp), %rdx
	movq	%r12, %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -104(%rbp)
	movl	%ecx, %esi
	shrl	$31, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L141:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3153:
	.size	_ZN12_GLOBAL__N_114_set_addStringEP4USetPKDsi, .-_ZN12_GLOBAL__N_114_set_addStringEP4USetPKDsi
	.p2align 4
	.type	_ZN12_GLOBAL__N_113_set_addRangeEP4USetii, @function
_ZN12_GLOBAL__N_113_set_addRangeEP4USetii:
.LFB3152:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6710UnicodeSet3addEii@PLT
	.cfi_endproc
.LFE3152:
	.size	_ZN12_GLOBAL__N_113_set_addRangeEP4USetii, .-_ZN12_GLOBAL__N_113_set_addRangeEP4USetii
	.p2align 4
	.globl	u_getBinaryPropertySet_67
	.type	u_getBinaryPropertySet_67, @function
u_getBinaryPropertySet_67:
.LFB3163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L143
	movl	%edi, %ebx
	movq	%rsi, %r12
	cmpl	$64, %edi
	ja	.L165
	leaq	_ZN12_GLOBAL__N_17cpMutexE(%rip), %rdi
	movslq	%ebx, %r14
	call	umtx_lock_67@PLT
	leaq	_ZN12_GLOBAL__N_14setsE(%rip), %rax
	movq	(%rax,%r14,8), %r15
	movl	(%r12), %eax
	testq	%r15, %r15
	je	.L146
.L147:
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%rax, %r15
.L158:
	leaq	_ZN12_GLOBAL__N_17cpMutexE(%rip), %rdi
	call	umtx_unlock_67@PLT
.L143:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movl	$1, (%rsi)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L146:
	testl	%eax, %eax
	jg	.L148
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L149
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r12, %rsi
	movl	%ebx, %edi
	call	_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode
	movl	(%r12), %edx
	movq	%rax, -72(%rbp)
	testl	%edx, %edx
	jle	.L166
	movq	-64(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
	movl	(%r12), %eax
.L148:
	leaq	_ZN12_GLOBAL__N_14setsE(%rip), %rcx
	movq	%r15, (%rcx,%r14,8)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%rax, %rdi
	movl	$-1, %r15d
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	$0, -56(%rbp)
	movl	%eax, -76(%rbp)
	testl	%eax, %eax
	jle	.L152
	.p2align 4,,10
	.p2align 3
.L151:
	movq	-72(%rbp), %r13
	movl	-56(%rbp), %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	-56(%rbp), %esi
	movq	%r13, %rdi
	movl	%eax, -52(%rbp)
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	-52(%rbp), %edx
	movl	%eax, %r13d
	cmpl	%eax, %edx
	jl	.L153
	leal	1(%rdx), %eax
	movl	%eax, -52(%rbp)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L167:
	testl	%r15d, %r15d
	cmovs	%r13d, %r15d
.L155:
	addl	$1, %r13d
	cmpl	-52(%rbp), %r13d
	je	.L153
.L156:
	movl	%ebx, %esi
	movl	%r13d, %edi
	call	u_hasBinaryProperty_67@PLT
	testb	%al, %al
	jne	.L167
	testl	%r15d, %r15d
	js	.L155
	movq	-64(%rbp), %rdi
	leal	-1(%r13), %edx
	movl	%r15d, %esi
	addl	$1, %r13d
	movl	$-1, %r15d
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	cmpl	-52(%rbp), %r13d
	jne	.L156
	.p2align 4,,10
	.p2align 3
.L153:
	addl	$1, -56(%rbp)
	movl	-56(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L151
	testl	%r15d, %r15d
	js	.L152
	movq	-64(%rbp), %rdi
	movl	$1114111, %edx
	movl	%r15d, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
.L152:
	movq	-64(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	movl	(%r12), %eax
	jmp	.L148
.L149:
	leaq	_ZN12_GLOBAL__N_14setsE(%rip), %rax
	movl	$7, (%r12)
	movq	$0, (%rax,%r14,8)
	jmp	.L158
	.cfi_endproc
.LFE3163:
	.size	u_getBinaryPropertySet_67, .-u_getBinaryPropertySet_67
	.local	_ZN12_GLOBAL__N_17cpMutexE
	.comm	_ZN12_GLOBAL__N_17cpMutexE,56,32
	.local	_ZN12_GLOBAL__N_14mapsE
	.comm	_ZN12_GLOBAL__N_14mapsE,200,32
	.local	_ZN12_GLOBAL__N_14setsE
	.comm	_ZN12_GLOBAL__N_14setsE,520,32
	.local	_ZN12_GLOBAL__N_111gInclusionsE
	.comm	_ZN12_GLOBAL__N_111gInclusionsE,640,32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
