	.file	"uvectr32.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679UVector3217getDynamicClassIDEv
	.type	_ZNK6icu_679UVector3217getDynamicClassIDEv, @function
_ZNK6icu_679UVector3217getDynamicClassIDEv:
.LFB2090:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_679UVector3216getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2090:
	.size	_ZNK6icu_679UVector3217getDynamicClassIDEv, .-_ZNK6icu_679UVector3217getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector32D2Ev
	.type	_ZN6icu_679UVector32D2Ev, @function
_ZN6icu_679UVector32D2Ev:
.LFB2102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679UVector32E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	movq	$0, 24(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2102:
	.size	_ZN6icu_679UVector32D2Ev, .-_ZN6icu_679UVector32D2Ev
	.globl	_ZN6icu_679UVector32D1Ev
	.set	_ZN6icu_679UVector32D1Ev,_ZN6icu_679UVector32D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector32D0Ev
	.type	_ZN6icu_679UVector32D0Ev, @function
_ZN6icu_679UVector32D0Ev:
.LFB2104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679UVector32E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	movq	$0, 24(%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2104:
	.size	_ZN6icu_679UVector32D0Ev, .-_ZN6icu_679UVector32D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector3216getStaticClassIDEv
	.type	_ZN6icu_679UVector3216getStaticClassIDEv, @function
_ZN6icu_679UVector3216getStaticClassIDEv:
.LFB2089:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_679UVector3216getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2089:
	.size	_ZN6icu_679UVector3216getStaticClassIDEv, .-_ZN6icu_679UVector3216getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector32C2ER10UErrorCode
	.type	_ZN6icu_679UVector32C2ER10UErrorCode, @function
_ZN6icu_679UVector32C2ER10UErrorCode:
.LFB2095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679UVector32E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	$32, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.L12
	movl	$8, 12(%rbx)
.L8:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L8
	.cfi_endproc
.LFE2095:
	.size	_ZN6icu_679UVector32C2ER10UErrorCode, .-_ZN6icu_679UVector32C2ER10UErrorCode
	.globl	_ZN6icu_679UVector32C1ER10UErrorCode
	.set	_ZN6icu_679UVector32C1ER10UErrorCode,_ZN6icu_679UVector32C2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector32C2EiR10UErrorCode
	.type	_ZN6icu_679UVector32C2EiR10UErrorCode, @function
_ZN6icu_679UVector32C2EiR10UErrorCode:
.LFB2098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_679UVector32E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movl	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	testl	%esi, %esi
	jle	.L18
	movslq	%esi, %rdi
	movl	%esi, %r12d
	salq	$2, %rdi
	cmpl	$536870911, %esi
	jle	.L14
	movl	$8, %edi
	xorl	%esi, %esi
	call	uprv_min_67@PLT
	movslq	%eax, %rdi
	movq	%rdi, %r12
	salq	$2, %rdi
.L14:
	call	uprv_malloc_67@PLT
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.L20
	movl	%r12d, 12(%rbx)
.L13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movl	$32, %edi
	movl	$8, %r12d
	jmp	.L14
.L20:
	movl	$7, 0(%r13)
	jmp	.L13
	.cfi_endproc
.LFE2098:
	.size	_ZN6icu_679UVector32C2EiR10UErrorCode, .-_ZN6icu_679UVector32C2EiR10UErrorCode
	.globl	_ZN6icu_679UVector32C1EiR10UErrorCode
	.set	_ZN6icu_679UVector32C1EiR10UErrorCode,_ZN6icu_679UVector32C2EiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector325_initEiR10UErrorCode
	.type	_ZN6icu_679UVector325_initEiR10UErrorCode, @function
_ZN6icu_679UVector325_initEiR10UErrorCode:
.LFB2100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movl	16(%rdi), %esi
	testl	%ebx, %ebx
	jle	.L31
	testl	%esi, %esi
	jle	.L25
.L23:
	cmpl	%esi, %ebx
	cmovg	%esi, %ebx
.L25:
	cmpl	$536870911, %ebx
	jg	.L26
	movslq	%ebx, %rdi
	salq	$2, %rdi
.L24:
	call	uprv_malloc_67@PLT
	movq	%rax, 24(%r12)
	testq	%rax, %rax
	je	.L32
	movl	%ebx, 12(%r12)
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	$8, %edi
	call	uprv_min_67@PLT
	movslq	%eax, %rdi
	movq	%rdi, %rbx
	salq	$2, %rdi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$8, %ebx
	movl	$32, %edi
	testl	%esi, %esi
	jg	.L23
	jmp	.L24
.L32:
	movl	$7, 0(%r13)
	jmp	.L21
	.cfi_endproc
.LFE2100:
	.size	_ZN6icu_679UVector325_initEiR10UErrorCode, .-_ZN6icu_679UVector325_initEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector32eqERKS0_
	.type	_ZN6icu_679UVector32eqERKS0_, @function
_ZN6icu_679UVector32eqERKS0_:
.LFB2106:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	xorl	%r8d, %r8d
	cmpl	8(%rsi), %eax
	jne	.L33
	testl	%eax, %eax
	jle	.L37
	movq	24(%rsi), %rcx
	movq	24(%rdi), %rdi
	leal	-1(%rax), %esi
	xorl	%eax, %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	1(%rax), %rdx
	cmpq	%rsi, %rax
	je	.L37
	movq	%rdx, %rax
.L35:
	movl	(%rcx,%rax,4), %edx
	cmpl	%edx, (%rdi,%rax,4)
	je	.L40
	xorl	%r8d, %r8d
.L33:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2106:
	.size	_ZN6icu_679UVector32eqERKS0_, .-_ZN6icu_679UVector32eqERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector3212setElementAtEii
	.type	_ZN6icu_679UVector3212setElementAtEii, @function
_ZN6icu_679UVector3212setElementAtEii:
.LFB2107:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	js	.L41
	cmpl	%edx, 8(%rdi)
	jle	.L41
	movq	24(%rdi), %rax
	movslq	%edx, %rdx
	movl	%esi, (%rax,%rdx,4)
.L41:
	ret
	.cfi_endproc
.LFE2107:
	.size	_ZN6icu_679UVector3212setElementAtEii, .-_ZN6icu_679UVector3212setElementAtEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector3215insertElementAtEiiR10UErrorCode
	.type	_ZN6icu_679UVector3215insertElementAtEiiR10UErrorCode, @function
_ZN6icu_679UVector3215insertElementAtEiiR10UErrorCode:
.LFB2108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	subq	$24, %rsp
	testl	%ebx, %ebx
	js	.L43
	movslq	8(%rdi), %rax
	movq	%rdi, %r12
	cmpl	%ebx, %eax
	jl	.L43
	movl	%esi, %r13d
	movl	12(%rdi), %esi
	leal	1(%rax), %edx
	cmpl	%esi, %edx
	jle	.L59
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L43
	movl	16(%rdi), %r15d
	testl	%r15d, %r15d
	jle	.L47
	cmpl	%r15d, %edx
	jg	.L60
	cmpl	$1073741823, %esi
	jg	.L49
	addl	%esi, %esi
	cmpl	%esi, %edx
	cmovl	%esi, %edx
	cmpl	%r15d, %edx
	cmovle	%edx, %r15d
.L53:
	cmpl	$536870911, %r15d
	jg	.L49
	movq	24(%r12), %rdi
	movslq	%r15d, %rsi
	movq	%rcx, -56(%rbp)
	salq	$2, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L61
	movq	%rax, 24(%r12)
	movslq	8(%r12), %rax
	movl	%r15d, 12(%r12)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L59:
	movq	24(%rdi), %r14
.L46:
	cmpl	%eax, %ebx
	jge	.L52
	movl	%eax, %edx
	salq	$2, %rax
	subl	%ebx, %edx
	subl	$1, %edx
	movq	%rdx, %rdi
	leaq	4(,%rdx,4), %rdx
	negq	%rdi
	salq	$2, %rdi
	leaq	-4(%rax,%rdi), %rsi
	addq	%rax, %rdi
	addq	%r14, %rsi
	addq	%r14, %rdi
	call	memmove@PLT
.L52:
	movl	%r13d, (%r14,%rbx,4)
	addl	$1, 8(%r12)
.L43:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	cmpl	$1073741823, %esi
	jg	.L49
	addl	%esi, %esi
	cmpl	%esi, %edx
	cmovge	%edx, %esi
	movl	%esi, %r15d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$1, (%rcx)
	jmp	.L43
.L60:
	movl	$15, (%rcx)
	jmp	.L43
.L61:
	movl	$7, (%rcx)
	jmp	.L43
	.cfi_endproc
.LFE2108:
	.size	_ZN6icu_679UVector3215insertElementAtEiiR10UErrorCode, .-_ZN6icu_679UVector3215insertElementAtEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679UVector3211containsAllERKS0_
	.type	_ZNK6icu_679UVector3211containsAllERKS0_, @function
_ZNK6icu_679UVector3211containsAllERKS0_:
.LFB2109:
	.cfi_startproc
	endbr64
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jle	.L71
	movl	8(%rdi), %r9d
	movq	24(%rsi), %rsi
	subl	$1, %eax
	leal	-1(%r9), %r8d
	leaq	4(%rsi,%rax,4), %r10
	salq	$2, %r8
	.p2align 4,,10
	.p2align 3
.L66:
	movl	(%rsi), %edx
	testl	%r9d, %r9d
	jle	.L68
	movq	24(%rdi), %rax
	leaq	4(%rax,%r8), %rcx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$4, %rax
	cmpq	%rcx, %rax
	je	.L68
.L65:
	cmpl	(%rax), %edx
	jne	.L72
	addq	$4, %rsi
	cmpq	%r10, %rsi
	jne	.L66
.L71:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2109:
	.size	_ZNK6icu_679UVector3211containsAllERKS0_, .-_ZNK6icu_679UVector3211containsAllERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679UVector3212containsNoneERKS0_
	.type	_ZNK6icu_679UVector3212containsNoneERKS0_, @function
_ZNK6icu_679UVector3212containsNoneERKS0_:
.LFB2110:
	.cfi_startproc
	endbr64
	movl	8(%rsi), %eax
	testl	%eax, %eax
	jle	.L81
	movl	8(%rdi), %r9d
	movq	24(%rsi), %rsi
	subl	$1, %eax
	leal	-1(%r9), %r8d
	leaq	4(%rsi,%rax,4), %r10
	salq	$2, %r8
	.p2align 4,,10
	.p2align 3
.L77:
	movl	(%rsi), %edx
	testl	%r9d, %r9d
	jle	.L75
	movq	24(%rdi), %rax
	leaq	4(%rax,%r8), %rcx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$4, %rax
	cmpq	%rcx, %rax
	je	.L75
.L76:
	cmpl	(%rax), %edx
	jne	.L82
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$4, %rsi
	cmpq	%r10, %rsi
	jne	.L77
.L81:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2110:
	.size	_ZNK6icu_679UVector3212containsNoneERKS0_, .-_ZNK6icu_679UVector3212containsNoneERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector329removeAllERKS0_
	.type	_ZN6icu_679UVector329removeAllERKS0_, @function
_ZN6icu_679UVector329removeAllERKS0_:
.LFB2111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	8(%rsi), %r13d
	testl	%r13d, %r13d
	jle	.L91
	movq	24(%rsi), %rbx
	movl	8(%rdi), %ecx
	movq	%rsi, %r12
	xorl	%r11d, %r11d
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L90:
	movl	(%rbx,%r11,4), %r9d
	testl	%ecx, %ecx
	jle	.L85
	leal	-1(%rcx), %r10d
	movq	24(%rdi), %r8
	xorl	%eax, %eax
	movq	%r10, %rsi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	1(%rax), %rdx
	cmpq	%rax, %r10
	je	.L85
	movq	%rdx, %rax
.L87:
	cmpl	(%r8,%rax,4), %r9d
	jne	.L95
	movl	%esi, %ecx
	cmpl	%eax, %esi
	jle	.L88
	cltq
	.p2align 4,,10
	.p2align 3
.L89:
	movl	4(%r8,%rax,4), %edx
	movl	%edx, (%r8,%rax,4)
	movl	8(%rdi), %esi
	addq	$1, %rax
	leal	-1(%rsi), %ecx
	cmpl	%eax, %ecx
	jg	.L89
.L88:
	movl	%ecx, 8(%rdi)
	movl	8(%r12), %r13d
	movl	$1, %r14d
.L85:
	addq	$1, %r11
	cmpl	%r11d, %r13d
	jg	.L90
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L91:
	.cfi_restore_state
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2111:
	.size	_ZN6icu_679UVector329removeAllERKS0_, .-_ZN6icu_679UVector329removeAllERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector329retainAllERKS0_
	.type	_ZN6icu_679UVector329retainAllERKS0_, @function
_ZN6icu_679UVector329retainAllERKS0_:
.LFB2112:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %r8d
	subl	$1, %r8d
	js	.L104
	movq	24(%rdi), %r9
	movslq	%r8d, %r8
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L103:
	movl	8(%rsi), %ecx
	movl	(%r9,%r8,4), %edx
	testl	%ecx, %ecx
	jle	.L98
	movq	24(%rsi), %rax
	subl	$1, %ecx
	leaq	4(%rax,%rcx,4), %rcx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L108:
	addq	$4, %rax
	cmpq	%rax, %rcx
	je	.L98
.L100:
	cmpl	(%rax), %edx
	jne	.L108
	subq	$1, %r8
	testl	%r8d, %r8d
	jns	.L103
.L96:
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	movl	8(%rdi), %eax
	subl	$1, %eax
	cmpl	%r8d, %eax
	jle	.L101
	movq	%r8, %rdx
.L102:
	movl	4(%r9,%rdx,4), %eax
	movl	%eax, (%r9,%rdx,4)
	movl	8(%rdi), %eax
	addq	$1, %rdx
	subl	$1, %eax
	cmpl	%edx, %eax
	jg	.L102
.L101:
	subq	$1, %r8
	movl	%eax, 8(%rdi)
	movl	$1, %r10d
	testl	%r8d, %r8d
	jns	.L103
	jmp	.L96
.L104:
	xorl	%r10d, %r10d
	movl	%r10d, %eax
	ret
	.cfi_endproc
.LFE2112:
	.size	_ZN6icu_679UVector329retainAllERKS0_, .-_ZN6icu_679UVector329retainAllERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector3215removeElementAtEi
	.type	_ZN6icu_679UVector3215removeElementAtEi, @function
_ZN6icu_679UVector3215removeElementAtEi:
.LFB2113:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L109
	movl	8(%rdi), %eax
	subl	$1, %eax
	cmpl	%eax, %esi
	jge	.L111
	movq	24(%rdi), %rdx
	movslq	%esi, %rsi
	.p2align 4,,10
	.p2align 3
.L112:
	movl	4(%rdx,%rsi,4), %eax
	movl	%eax, (%rdx,%rsi,4)
	movl	8(%rdi), %eax
	addq	$1, %rsi
	subl	$1, %eax
	cmpl	%esi, %eax
	jg	.L112
.L111:
	movl	%eax, 8(%rdi)
.L109:
	ret
	.cfi_endproc
.LFE2113:
	.size	_ZN6icu_679UVector3215removeElementAtEi, .-_ZN6icu_679UVector3215removeElementAtEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector3217removeAllElementsEv
	.type	_ZN6icu_679UVector3217removeAllElementsEv, @function
_ZN6icu_679UVector3217removeAllElementsEv:
.LFB2114:
	.cfi_startproc
	endbr64
	movl	$0, 8(%rdi)
	ret
	.cfi_endproc
.LFE2114:
	.size	_ZN6icu_679UVector3217removeAllElementsEv, .-_ZN6icu_679UVector3217removeAllElementsEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679UVector326equalsERKS0_
	.type	_ZNK6icu_679UVector326equalsERKS0_, @function
_ZNK6icu_679UVector326equalsERKS0_:
.LFB2648:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	xorl	%r8d, %r8d
	cmpl	8(%rsi), %eax
	jne	.L115
	testl	%eax, %eax
	jle	.L119
	movq	24(%rsi), %rcx
	movq	24(%rdi), %rdi
	leal	-1(%rax), %esi
	xorl	%eax, %eax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L122:
	leaq	1(%rax), %rdx
	cmpq	%rsi, %rax
	je	.L119
	movq	%rdx, %rax
.L117:
	movl	(%rcx,%rax,4), %edx
	cmpl	%edx, (%rdi,%rax,4)
	je	.L122
	xorl	%r8d, %r8d
.L115:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2648:
	.size	_ZNK6icu_679UVector326equalsERKS0_, .-_ZNK6icu_679UVector326equalsERKS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679UVector327indexOfEii
	.type	_ZNK6icu_679UVector327indexOfEii, @function
_ZNK6icu_679UVector327indexOfEii:
.LFB2116:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	cmpl	%eax, %edx
	jge	.L126
	movq	24(%rdi), %rcx
	movslq	%edx, %rdx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L128:
	addq	$1, %rdx
	cmpl	%edx, %eax
	jle	.L126
.L125:
	movl	%edx, %r8d
	cmpl	%esi, (%rcx,%rdx,4)
	jne	.L128
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$-1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE2116:
	.size	_ZNK6icu_679UVector327indexOfEii, .-_ZNK6icu_679UVector327indexOfEii
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode
	.type	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode, @function
_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode:
.LFB2117:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L144
	testl	%esi, %esi
	js	.L147
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movl	12(%rdi), %ebx
	cmpl	%ebx, %esi
	jle	.L129
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jle	.L132
	cmpl	%eax, %esi
	jg	.L148
	cmpl	$1073741823, %ebx
	jg	.L134
	addl	%ebx, %ebx
	cmpl	%ebx, %esi
	cmovl	%ebx, %esi
	cmpl	%eax, %esi
	cmovle	%esi, %eax
	movl	%eax, %ebx
.L136:
	cmpl	$536870911, %ebx
	jg	.L134
	movq	24(%r13), %rdi
	movslq	%ebx, %rsi
	movq	%rdx, -24(%rbp)
	salq	$2, %rsi
	call	uprv_realloc_67@PLT
	movq	-24(%rbp), %rdx
	testq	%rax, %rax
	je	.L149
	movl	%ebx, 12(%r13)
	movq	%rax, 24(%r13)
	movl	$1, %eax
.L129:
	addq	$16, %rsp
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 13
	movl	$1, (%rdx)
	xorl	%eax, %eax
.L144:
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 13, -24
	cmpl	$1073741823, %ebx
	jg	.L134
	addl	%ebx, %ebx
	cmpl	%ebx, %esi
	cmovge	%esi, %ebx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$1, (%rdx)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movl	$15, (%rdx)
	xorl	%eax, %eax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$7, (%rdx)
	jmp	.L129
	.cfi_endproc
.LFE2117:
	.size	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode, .-_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector3214setMaxCapacityEi
	.type	_ZN6icu_679UVector3214setMaxCapacityEi, @function
_ZN6icu_679UVector3214setMaxCapacityEi:
.LFB2118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%esi, %esi
	js	.L159
	cmpl	$536870911, %esi
	jg	.L150
	movl	%esi, 16(%rdi)
	cmpl	%esi, 12(%rdi)
	jle	.L150
	testl	%esi, %esi
	je	.L150
	movslq	%esi, %rsi
	movq	24(%rdi), %rdi
	salq	$2, %rsi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L150
	movq	%rax, 24(%rbx)
	movl	16(%rbx), %eax
	movl	%eax, 12(%rbx)
	cmpl	8(%rbx), %eax
	jge	.L150
	movl	%eax, 8(%rbx)
.L150:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movl	$0, 16(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2118:
	.size	_ZN6icu_679UVector3214setMaxCapacityEi, .-_ZN6icu_679UVector3214setMaxCapacityEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector327setSizeEi
	.type	_ZN6icu_679UVector327setSizeEi, @function
_ZN6icu_679UVector327setSizeEi:
.LFB2119:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L180
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movslq	8(%rdi), %rdx
	cmpl	%esi, %edx
	jge	.L168
	movl	12(%rdi), %eax
	cmpl	%eax, %esi
	jle	.L164
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jle	.L183
	cmpl	%edx, %esi
	jg	.L160
	cmpl	$1073741823, %eax
	jg	.L160
	leal	(%rax,%rax), %r13d
	cmpl	%r13d, %ebx
	cmovge	%ebx, %r13d
	cmpl	%edx, %r13d
	cmovg	%edx, %r13d
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L164:
	movq	24(%rdi), %rax
.L172:
	leal	-1(%rbx), %ecx
	leaq	(%rax,%rdx,4), %rdi
	subl	%edx, %ecx
	cmpl	%edx, %ebx
	leaq	4(,%rcx,4), %r8
	movl	$4, %ecx
	cmovle	%rcx, %r8
	xorl	%esi, %esi
	movq	%r8, %rdx
	call	memset@PLT
.L168:
	movl	%ebx, 8(%r12)
.L160:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpl	$1073741823, %eax
	jg	.L160
	addl	%eax, %eax
	cmpl	%eax, %ebx
	cmovge	%ebx, %eax
	movl	%eax, %r13d
.L169:
	cmpl	$536870911, %r13d
	jg	.L160
	movslq	%r13d, %rsi
	movq	24(%r12), %rdi
	salq	$2, %rsi
	call	uprv_realloc_67@PLT
	testq	%rax, %rax
	je	.L160
	movslq	8(%r12), %rdx
	movq	%rax, 24(%r12)
	movl	%r13d, 12(%r12)
	cmpl	%edx, %ebx
	jg	.L172
	jmp	.L168
	.cfi_endproc
.LFE2119:
	.size	_ZN6icu_679UVector327setSizeEi, .-_ZN6icu_679UVector327setSizeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector326assignERKS0_R10UErrorCode
	.type	_ZN6icu_679UVector326assignERKS0_R10UErrorCode, @function
_ZN6icu_679UVector326assignERKS0_R10UErrorCode:
.LFB2105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	8(%rsi), %esi
	testl	%esi, %esi
	js	.L185
	movl	12(%rdi), %eax
	movq	%rdi, %r12
	cmpl	%eax, %esi
	jle	.L186
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L184
	movl	16(%r12), %r14d
	testl	%r14d, %r14d
	jle	.L189
	cmpl	%r14d, %esi
	jg	.L205
	cmpl	$1073741823, %eax
	jg	.L191
	addl	%eax, %eax
	cmpl	%eax, %esi
	cmovl	%eax, %esi
	cmpl	%r14d, %esi
	cmovle	%esi, %r14d
.L196:
	cmpl	$536870911, %r14d
	jg	.L191
	movq	24(%r12), %rdi
	movslq	%r14d, %rsi
	movq	%rdx, -40(%rbp)
	salq	$2, %rsi
	call	uprv_realloc_67@PLT
	movq	-40(%rbp), %rdx
	testq	%rax, %rax
	je	.L206
	movq	%rax, 24(%r12)
	movl	8(%rbx), %esi
	movl	%r14d, 12(%r12)
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%r12, %rdi
	call	_ZN6icu_679UVector327setSizeEi
	movl	8(%rbx), %esi
	testl	%esi, %esi
	jle	.L184
	movq	24(%rbx), %rsi
	movq	24(%r12), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L195:
	movl	(%rsi,%rax,4), %edx
	movl	%edx, (%rcx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 8(%rbx)
	jg	.L195
.L184:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L184
.L191:
	movl	$1, (%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	cmpl	$1073741823, %eax
	jg	.L191
	addl	%eax, %eax
	cmpl	%eax, %esi
	cmovge	%esi, %eax
	movl	%eax, %r14d
	jmp	.L196
.L206:
	movl	$7, (%rdx)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$15, (%rdx)
	jmp	.L184
	.cfi_endproc
.LFE2105:
	.size	_ZN6icu_679UVector326assignERKS0_R10UErrorCode, .-_ZN6icu_679UVector326assignERKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679UVector3212sortedInsertEiR10UErrorCode
	.type	_ZN6icu_679UVector3212sortedInsertEiR10UErrorCode, @function
_ZN6icu_679UVector3212sortedInsertEiR10UErrorCode:
.LFB2120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movslq	8(%rdi), %r8
	movslq	%r8d, %rbx
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L231:
	leal	(%rsi,%rbx), %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	movq	24(%r12), %rcx
	sarl	%eax
	movslq	%eax, %rdi
	cmpl	%r13d, (%rcx,%rdi,4)
	jle	.L230
	movslq	%eax, %rbx
.L209:
	cmpl	%ebx, %esi
	jne	.L231
	movl	%r8d, %eax
	addl	$1, %eax
	js	.L211
	movl	12(%r12), %ecx
	cmpl	%ecx, %eax
	jle	.L232
	movl	(%rdx), %esi
	testl	%esi, %esi
	jg	.L207
	movl	16(%r12), %r15d
	testl	%r15d, %r15d
	jle	.L215
	cmpl	%r15d, %eax
	jg	.L233
	cmpl	$1073741823, %ecx
	jg	.L217
	addl	%ecx, %ecx
	cmpl	%ecx, %eax
	cmovl	%ecx, %eax
	cmpl	%r15d, %eax
	cmovle	%eax, %r15d
.L221:
	cmpl	$536870911, %r15d
	jg	.L217
	movq	24(%r12), %rdi
	movslq	%r15d, %rsi
	movq	%rdx, -56(%rbp)
	salq	$2, %rsi
	call	uprv_realloc_67@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L234
	movq	%rax, 24(%r12)
	movslq	8(%r12), %r8
	movl	%r15d, 12(%r12)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L230:
	leal	1(%rax), %esi
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L232:
	movq	24(%r12), %r14
.L213:
	cmpl	%r8d, %ebx
	jge	.L220
	movl	%r8d, %eax
	salq	$2, %r8
	subl	%ebx, %eax
	leal	-1(%rax), %edx
	movq	%rdx, %rax
	leaq	4(,%rdx,4), %rdx
	negq	%rax
	salq	$2, %rax
	leaq	-4(%r8,%rax), %rsi
	addq	%rax, %r8
	addq	%r14, %rsi
	leaq	(%r14,%r8), %rdi
	call	memmove@PLT
.L220:
	movl	%r13d, (%r14,%rbx,4)
	addl	$1, 8(%r12)
.L207:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movl	(%rdx), %edi
	testl	%edi, %edi
	jg	.L207
.L217:
	movl	$1, (%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L215:
	.cfi_restore_state
	cmpl	$1073741823, %ecx
	jg	.L217
	addl	%ecx, %ecx
	cmpl	%ecx, %eax
	cmovge	%eax, %ecx
	movl	%ecx, %r15d
	jmp	.L221
.L234:
	movl	$7, (%rdx)
	jmp	.L207
.L233:
	movl	$15, (%rdx)
	jmp	.L207
	.cfi_endproc
.LFE2120:
	.size	_ZN6icu_679UVector3212sortedInsertEiR10UErrorCode, .-_ZN6icu_679UVector3212sortedInsertEiR10UErrorCode
	.weak	_ZTSN6icu_679UVector32E
	.section	.rodata._ZTSN6icu_679UVector32E,"aG",@progbits,_ZTSN6icu_679UVector32E,comdat
	.align 16
	.type	_ZTSN6icu_679UVector32E, @object
	.size	_ZTSN6icu_679UVector32E, 20
_ZTSN6icu_679UVector32E:
	.string	"N6icu_679UVector32E"
	.weak	_ZTIN6icu_679UVector32E
	.section	.data.rel.ro._ZTIN6icu_679UVector32E,"awG",@progbits,_ZTIN6icu_679UVector32E,comdat
	.align 8
	.type	_ZTIN6icu_679UVector32E, @object
	.size	_ZTIN6icu_679UVector32E, 24
_ZTIN6icu_679UVector32E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_679UVector32E
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_679UVector32E
	.section	.data.rel.ro.local._ZTVN6icu_679UVector32E,"awG",@progbits,_ZTVN6icu_679UVector32E,comdat
	.align 8
	.type	_ZTVN6icu_679UVector32E, @object
	.size	_ZTVN6icu_679UVector32E, 40
_ZTVN6icu_679UVector32E:
	.quad	0
	.quad	_ZTIN6icu_679UVector32E
	.quad	_ZN6icu_679UVector32D1Ev
	.quad	_ZN6icu_679UVector32D0Ev
	.quad	_ZNK6icu_679UVector3217getDynamicClassIDEv
	.local	_ZZN6icu_679UVector3216getStaticClassIDEvE7classID
	.comm	_ZZN6icu_679UVector3216getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
