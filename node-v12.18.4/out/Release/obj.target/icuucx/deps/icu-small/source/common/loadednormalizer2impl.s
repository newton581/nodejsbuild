	.file	"loadednormalizer2impl.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721LoadedNormalizer2Impl12isAcceptableEPvPKcS3_PK9UDataInfo
	.type	_ZN6icu_6721LoadedNormalizer2Impl12isAcceptableEPvPKcS3_PK9UDataInfo, @function
_ZN6icu_6721LoadedNormalizer2Impl12isAcceptableEPvPKcS3_PK9UDataInfo:
.LFB3252:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpw	$19, (%rcx)
	jbe	.L1
	cmpw	$0, 4(%rcx)
	jne	.L1
	cmpw	$29262, 8(%rcx)
	jne	.L1
	cmpw	$12909, 10(%rcx)
	jne	.L1
	cmpb	$4, 12(%rcx)
	sete	%al
.L1:
	ret
	.cfi_endproc
.LFE3252:
	.size	_ZN6icu_6721LoadedNormalizer2Impl12isAcceptableEPvPKcS3_PK9UDataInfo, .-_ZN6icu_6721LoadedNormalizer2Impl12isAcceptableEPvPKcS3_PK9UDataInfo
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721LoadedNormalizer2ImplD2Ev
	.type	_ZN6icu_6721LoadedNormalizer2ImplD2Ev, @function
_ZN6icu_6721LoadedNormalizer2ImplD2Ev:
.LFB3249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721LoadedNormalizer2ImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	80(%rdi), %rdi
	call	udata_close_67@PLT
	movq	88(%r12), %rdi
	call	ucptrie_close_67@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6715Normalizer2ImplD2Ev@PLT
	.cfi_endproc
.LFE3249:
	.size	_ZN6icu_6721LoadedNormalizer2ImplD2Ev, .-_ZN6icu_6721LoadedNormalizer2ImplD2Ev
	.globl	_ZN6icu_6721LoadedNormalizer2ImplD1Ev
	.set	_ZN6icu_6721LoadedNormalizer2ImplD1Ev,_ZN6icu_6721LoadedNormalizer2ImplD2Ev
	.p2align 4
	.type	uprv_loaded_normalizer2_cleanup, @function
uprv_loaded_normalizer2_cleanup:
.LFB3257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %r12
	testq	%r12, %r12
	je	.L11
	movq	%r12, %rdi
	call	_ZN6icu_6713Norm2AllModesD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L11:
	movq	$0, _ZN6icu_67L13nfkcSingletonE(%rip)
	movl	$0, _ZN6icu_67L12nfkcInitOnceE(%rip)
	mfence
	movq	_ZN6icu_67L16nfkc_cfSingletonE(%rip), %r12
	testq	%r12, %r12
	je	.L12
	movq	%r12, %rdi
	call	_ZN6icu_6713Norm2AllModesD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L12:
	movq	$0, _ZN6icu_67L16nfkc_cfSingletonE(%rip)
	movl	$0, _ZN6icu_67L15nfkc_cfInitOnceE(%rip)
	mfence
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	call	uhash_close_67@PLT
	movl	$1, %eax
	movq	$0, _ZN6icu_67L5cacheE(%rip)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3257:
	.size	uprv_loaded_normalizer2_cleanup, .-uprv_loaded_normalizer2_cleanup
	.p2align 4
	.type	deleteNorm2AllModes, @function
deleteNorm2AllModes:
.LFB3256:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L20
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN6icu_6713Norm2AllModesD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	ret
	.cfi_endproc
.LFE3256:
	.size	deleteNorm2AllModes, .-deleteNorm2AllModes
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"nrm"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode.part.0, @function
_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode.part.0:
.LFB4269:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r12, %r8
	movq	%rcx, %rbx
	leaq	_ZN6icu_6721LoadedNormalizer2Impl12isAcceptableEPvPKcS3_PK9UDataInfo(%rip), %rcx
	call	udata_openChoice_67@PLT
	movl	(%rbx), %edx
	movq	%rax, 80(%r12)
	testl	%edx, %edx
	jle	.L31
.L25:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	movq	%rax, %rdi
	call	udata_getMemory_67@PLT
	movslq	(%rax), %rdx
	movq	%rax, %r13
	cmpl	$75, %edx
	jg	.L28
	movl	$3, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	4(%rax), %r14d
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	xorl	%edi, %edi
	movl	%r14d, %ecx
	subl	%edx, %ecx
	addq	%rax, %rdx
	call	ucptrie_openFromBinary_67@PLT
	movq	%rax, 88(%r12)
	movq	%rax, %rdx
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L25
	movslq	8(%r13), %rax
	movslq	%r14d, %rcx
	popq	%rbx
	movq	%r13, %rsi
	addq	%r13, %rcx
	movq	%r12, %rdi
	popq	%r12
	leaq	0(%r13,%rax), %r8
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6715Normalizer2Impl4initEPKiPK7UCPTriePKtPKh@PLT
	.cfi_endproc
.LFE4269:
	.size	_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode.part.0, .-_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721LoadedNormalizer2ImplD0Ev
	.type	_ZN6icu_6721LoadedNormalizer2ImplD0Ev, @function
_ZN6icu_6721LoadedNormalizer2ImplD0Ev:
.LFB3251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6721LoadedNormalizer2ImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	80(%rdi), %rdi
	call	udata_close_67@PLT
	movq	88(%r12), %rdi
	call	ucptrie_close_67@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6715Normalizer2ImplD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3251:
	.size	_ZN6icu_6721LoadedNormalizer2ImplD0Ev, .-_ZN6icu_6721LoadedNormalizer2ImplD0Ev
	.section	.rodata.str1.1
.LC1:
	.string	"nfkc"
.LC2:
	.string	"nfkc_cf"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4
	.type	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode, @function
_ZN6icu_67L14initSingletonsEPKcR10UErrorCode:
.LFB3255:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movl	$5, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	.LC1(%rip), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rdi, %rsi
	movq	%r14, %rdi
	subq	$8, %rsp
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	jne	.L35
	movl	(%r12), %esi
	xorl	%r13d, %r13d
	testl	%esi, %esi
	jg	.L36
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L37
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rax)
	leaq	16+_ZTVN6icu_6721LoadedNormalizer2ImplE(%rip), %rax
	movq	$0, 64(%r13)
	movups	%xmm0, 72(%r13)
	movl	(%r12), %ecx
	movq	%rax, 0(%r13)
	movq	$0, 88(%r13)
	testl	%ecx, %ecx
	jle	.L47
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode@PLT
	movq	%rax, %r13
.L36:
	movq	%r13, _ZN6icu_67L13nfkcSingletonE(%rip)
.L39:
	addq	$8, %rsp
	leaq	uprv_loaded_normalizer2_cleanup(%rip), %rsi
	movl	$12, %edi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ucln_common_registerCleanup_67@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	leaq	.LC2(%rip), %r14
	movq	%rax, %rsi
	movl	$8, %ecx
	movq	%r14, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L40
	movl	(%r12), %edx
	xorl	%r13d, %r13d
	testl	%edx, %edx
	jg	.L41
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L42
	movq	$0, 32(%rax)
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6721LoadedNormalizer2ImplE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	$0, 64(%r13)
	movups	%xmm0, 72(%r13)
	movl	(%r12), %eax
	movq	$0, 88(%r13)
	testl	%eax, %eax
	jle	.L48
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode@PLT
	movq	%rax, %r13
.L41:
	movq	%r13, _ZN6icu_67L16nfkc_cfSingletonE(%rip)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r13, %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode.part.0
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode@PLT
	movq	%rax, %r13
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r13, %rdi
	movq	%r12, %rcx
	movq	%r14, %rdx
	xorl	%esi, %esi
	call	_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode.part.0
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode@PLT
	movq	%rax, %r13
	jmp	.L41
.L37:
	movl	$7, (%r12)
	jmp	.L36
.L42:
	movl	$7, (%r12)
	jmp	.L41
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode.cold, @function
_ZN6icu_67L14initSingletonsEPKcR10UErrorCode.cold:
.LFSB3255:
.L40:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	abort@PLT
	.cfi_endproc
.LFE3255:
	.text
	.size	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode, .-_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	.section	.text.unlikely
	.size	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode.cold, .-_ZN6icu_67L14initSingletonsEPKcR10UErrorCode.cold
.LCOLDE3:
	.text
.LHOTE3:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode
	.type	_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode, @function
_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode:
.LFB3253:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L58
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r12, %r8
	movq	%rcx, %rbx
	leaq	_ZN6icu_6721LoadedNormalizer2Impl12isAcceptableEPvPKcS3_PK9UDataInfo(%rip), %rcx
	call	udata_openChoice_67@PLT
	movl	(%rbx), %edx
	movq	%rax, 80(%r12)
	testl	%edx, %edx
	jle	.L59
.L49:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%rax, %rdi
	call	udata_getMemory_67@PLT
	movslq	(%rax), %rdx
	movq	%rax, %r13
	cmpl	$75, %edx
	jg	.L53
	movl	$3, (%rbx)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L53:
	movl	4(%rax), %r14d
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	xorl	%edi, %edi
	movl	%r14d, %ecx
	subl	%edx, %ecx
	addq	%rax, %rdx
	call	ucptrie_openFromBinary_67@PLT
	movq	%rax, 88(%r12)
	movq	%rax, %rdx
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L49
	movslq	8(%r13), %rax
	movslq	%r14d, %rcx
	popq	%rbx
	.cfi_restore 3
	movq	%r13, %rsi
	addq	%r13, %rcx
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	leaq	0(%r13,%rax), %r8
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6715Normalizer2Impl4initEPKiPK7UCPTriePKtPKh@PLT
	.cfi_endproc
.LFE3253:
	.size	_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode, .-_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713Norm2AllModes14createInstanceEPKcS2_R10UErrorCode
	.type	_ZN6icu_6713Norm2AllModes14createInstanceEPKcS2_R10UErrorCode, @function
_ZN6icu_6713Norm2AllModes14createInstanceEPKcS2_R10UErrorCode:
.LFB3254:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L66
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$96, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L62
	movq	$0, 32(%rax)
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6721LoadedNormalizer2ImplE(%rip), %rax
	movq	%rax, (%r12)
	movq	$0, 64(%r12)
	movups	%xmm0, 72(%r12)
	movl	0(%r13), %eax
	movq	$0, 88(%r12)
	testl	%eax, %eax
	jle	.L69
.L63:
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode.part.0
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
.L62:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, 0(%r13)
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3254:
	.size	_ZN6icu_6713Norm2AllModes14createInstanceEPKcS2_R10UErrorCode, .-_ZN6icu_6713Norm2AllModes14createInstanceEPKcS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713Norm2AllModes15getNFKCInstanceER10UErrorCode
	.type	_ZN6icu_6713Norm2AllModes15getNFKCInstanceER10UErrorCode, @function
_ZN6icu_6713Norm2AllModes15getNFKCInstanceER10UErrorCode:
.LFB3258:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L81
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L86
.L72:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L73
	movl	%eax, (%rbx)
.L73:
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L72
	movq	%rbx, %rsi
	leaq	.LC1(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L73
	.cfi_endproc
.LFE3258:
	.size	_ZN6icu_6713Norm2AllModes15getNFKCInstanceER10UErrorCode, .-_ZN6icu_6713Norm2AllModes15getNFKCInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713Norm2AllModes18getNFKC_CFInstanceER10UErrorCode
	.type	_ZN6icu_6713Norm2AllModes18getNFKC_CFInstanceER10UErrorCode, @function
_ZN6icu_6713Norm2AllModes18getNFKC_CFInstanceER10UErrorCode:
.LFB3259:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L98
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L103
.L89:
	movl	4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L90
	movl	%eax, (%rbx)
.L90:
	movq	_ZN6icu_67L16nfkc_cfSingletonE(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L89
	movq	%rbx, %rsi
	leaq	.LC2(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L90
	.cfi_endproc
.LFE3259:
	.size	_ZN6icu_6713Norm2AllModes18getNFKC_CFInstanceER10UErrorCode, .-_ZN6icu_6713Norm2AllModes18getNFKC_CFInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Normalizer215getNFKCInstanceER10UErrorCode
	.type	_ZN6icu_6711Normalizer215getNFKCInstanceER10UErrorCode, @function
_ZN6icu_6711Normalizer215getNFKCInstanceER10UErrorCode:
.LFB3260:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L122
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L123
.L107:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L108
	movl	%eax, (%rbx)
.L108:
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L109
	addq	$8, %rsp
	addq	$8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L107
	movq	%rbx, %rsi
	leaq	.LC1(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L109:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3260:
	.size	_ZN6icu_6711Normalizer215getNFKCInstanceER10UErrorCode, .-_ZN6icu_6711Normalizer215getNFKCInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Normalizer215getNFKDInstanceER10UErrorCode
	.type	_ZN6icu_6711Normalizer215getNFKDInstanceER10UErrorCode, @function
_ZN6icu_6711Normalizer215getNFKDInstanceER10UErrorCode:
.LFB3261:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L142
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L143
.L127:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L128
	movl	%eax, (%rbx)
.L128:
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L129
	addq	$8, %rsp
	addq	$32, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L127
	movq	%rbx, %rsi
	leaq	.LC1(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3261:
	.size	_ZN6icu_6711Normalizer215getNFKDInstanceER10UErrorCode, .-_ZN6icu_6711Normalizer215getNFKDInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Normalizer223getNFKCCasefoldInstanceER10UErrorCode
	.type	_ZN6icu_6711Normalizer223getNFKCCasefoldInstanceER10UErrorCode, @function
_ZN6icu_6711Normalizer223getNFKCCasefoldInstanceER10UErrorCode:
.LFB3262:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L162
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L163
.L147:
	movl	4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L148
	movl	%eax, (%rbx)
.L148:
	movq	_ZN6icu_67L16nfkc_cfSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L149
	addq	$8, %rsp
	addq	$8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L147
	movq	%rbx, %rsi
	leaq	.LC2(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L149:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3262:
	.size	_ZN6icu_6711Normalizer223getNFKCCasefoldInstanceER10UErrorCode, .-_ZN6icu_6711Normalizer223getNFKCCasefoldInstanceER10UErrorCode
	.section	.rodata.str1.1
.LC4:
	.string	"nfc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Normalizer211getInstanceEPKcS2_19UNormalization2ModeR10UErrorCode
	.type	_ZN6icu_6711Normalizer211getInstanceEPKcS2_19UNormalization2ModeR10UErrorCode, @function
_ZN6icu_6711Normalizer211getInstanceEPKcS2_19UNormalization2ModeR10UErrorCode:
.LFB3263:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L224
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L166
	cmpb	$0, (%rsi)
	je	.L166
	movq	%rdi, %r15
	movl	%edx, %r14d
	testq	%rdi, %rdi
	je	.L169
.L178:
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L230
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	xorl	%edi, %edi
	movq	%rax, %r12
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	je	.L183
.L192:
	movl	(%rbx), %eax
.L182:
	testl	%eax, %eax
	jg	.L165
	cmpl	$2, %r14d
	je	.L195
	ja	.L196
	leaq	32(%r12), %rdx
	leaq	8(%r12), %rax
	testl	%r14d, %r14d
	cmovne	%rdx, %rax
.L164:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movl	$4, %ecx
	leaq	.LC4(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L231
	leaq	.LC1(%rip), %r12
	movl	$5, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L232
	leaq	.LC2(%rip), %r12
	movl	$8, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L178
	movl	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L179
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L179
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movl	(%rbx), %eax
.L180:
	movq	_ZN6icu_67L16nfkc_cfSingletonE(%rip), %r12
	jmp	.L173
.L238:
	movl	$7, (%rbx)
.L189:
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L228:
	testq	%r12, %r12
	je	.L165
	movq	%r12, %rdi
	call	_ZN6icu_6713Norm2AllModesD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L165:
	xorl	%eax, %eax
.L233:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	cmpl	$3, %r14d
	leaq	64(%r12), %rax
	movl	$0, %edx
	cmovne	%rdx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	48(%r12), %rax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L231:
	movq	%rbx, %rdi
	call	_ZN6icu_6713Norm2AllModes14getNFCInstanceER10UErrorCode@PLT
	movq	%rax, %r12
	movl	(%rbx), %eax
.L173:
	testq	%r12, %r12
	jne	.L182
	testl	%eax, %eax
	jle	.L178
	xorl	%eax, %eax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L230:
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L183:
	leaq	uprv_loaded_normalizer2_cleanup(%rip), %rsi
	movl	$12, %edi
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L165
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L185
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rax)
	leaq	16+_ZTVN6icu_6721LoadedNormalizer2ImplE(%rip), %rax
	movq	$0, 64(%r12)
	movups	%xmm0, 72(%r12)
	movl	(%rbx), %ecx
	movq	%rax, (%r12)
	movq	$0, 88(%r12)
	testl	%ecx, %ecx
	jg	.L186
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode.part.0
.L186:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	%rax, %r12
	testl	%edx, %edx
	jg	.L228
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L234
.L188:
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L235
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	je	.L194
	movq	%r12, %rdi
	call	_ZN6icu_6713Norm2AllModesD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L194:
	movq	%r15, %r12
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L232:
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L175
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L175
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movl	(%rbx), %eax
.L176:
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %r12
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L175:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L236
	movl	%eax, (%rbx)
	jmp	.L176
.L179:
	movl	4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L237
	movl	%eax, (%rbx)
	jmp	.L180
.L236:
	movl	(%rbx), %eax
	jmp	.L176
.L237:
	movl	(%rbx), %eax
	jmp	.L180
.L234:
	movq	uhash_hashChars_67@GOTPCREL(%rip), %rdi
	movq	uhash_compareChars_67@GOTPCREL(%rip), %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	call	uhash_open_67@PLT
	movq	%rax, _ZN6icu_67L5cacheE(%rip)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L189
	movq	uprv_free_67@GOTPCREL(%rip), %rsi
	call	uhash_setKeyDeleter_67@PLT
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	leaq	deleteNorm2AllModes(%rip), %rsi
	call	uhash_setValueDeleter_67@PLT
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	jmp	.L188
.L235:
	movq	%r13, %rdi
	call	strlen@PLT
	leal	1(%rax), %edx
	movslq	%edx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	je	.L238
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	uhash_put_67@PLT
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	jne	.L192
	xorl	%eax, %eax
	jmp	.L233
.L185:
	movl	$7, (%rbx)
	xorl	%eax, %eax
	jmp	.L233
	.cfi_endproc
.LFE3263:
	.size	_ZN6icu_6711Normalizer211getInstanceEPKcS2_19UNormalization2ModeR10UErrorCode, .-_ZN6icu_6711Normalizer211getInstanceEPKcS2_19UNormalization2ModeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode
	.type	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode, @function
_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode:
.LFB3266:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L278
	cmpl	$6, %edi
	ja	.L275
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.L244(%rip), %rdx
	movl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L244:
	.long	.L242-.L244
	.long	.L242-.L244
	.long	.L248-.L244
	.long	.L247-.L244
	.long	.L246-.L244
	.long	.L245-.L244
	.long	.L243-.L244
	.text
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	xorl	%eax, %eax
	ret
.L243:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%rsi, %rdi
	jmp	_ZN6icu_6718Normalizer2Factory14getFCDInstanceER10UErrorCode@PLT
.L248:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%rsi, %rdi
	jmp	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode@PLT
.L246:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%rsi, %rdi
	jmp	_ZN6icu_6711Normalizer214getNFCInstanceER10UErrorCode@PLT
.L247:
	.cfi_restore_state
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L279
.L249:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L250
	movl	%eax, (%rsi)
.L250:
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L251
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$32, %rax
	ret
.L245:
	.cfi_restore_state
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L280
.L252:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L253
	movl	%eax, (%rsi)
.L253:
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L251
	leave
	.cfi_def_cfa 7, 8
	addq	$8, %rax
	ret
.L275:
	.cfi_restore 6
	movq	%rsi, %rdi
	jmp	_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode@PLT
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movq	%rsi, -8(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movq	-8(%rbp), %rsi
	testb	%al, %al
	je	.L252
	leaq	.LC1(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movq	-8(%rbp), %rsi
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	(%rsi), %eax
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movq	%rsi, -8(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movq	-8(%rbp), %rsi
	testb	%al, %al
	je	.L249
	leaq	.LC1(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movq	-8(%rbp), %rsi
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	(%rsi), %eax
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L250
.L242:
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	movq	%rsi, %rdi
	jmp	_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode@PLT
.L251:
	.cfi_restore_state
	leave
	.cfi_def_cfa 7, 8
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3266:
	.size	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode, .-_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718Normalizer2Factory11getNFKCImplER10UErrorCode
	.type	_ZN6icu_6718Normalizer2Factory11getNFKCImplER10UErrorCode, @function
_ZN6icu_6718Normalizer2Factory11getNFKCImplER10UErrorCode:
.LFB3267:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L299
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L300
.L284:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L285
	movl	%eax, (%rbx)
.L285:
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L286
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L284
	movq	%rbx, %rsi
	leaq	.LC1(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L286:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3267:
	.size	_ZN6icu_6718Normalizer2Factory11getNFKCImplER10UErrorCode, .-_ZN6icu_6718Normalizer2Factory11getNFKCImplER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718Normalizer2Factory14getNFKC_CFImplER10UErrorCode
	.type	_ZN6icu_6718Normalizer2Factory14getNFKC_CFImplER10UErrorCode, @function
_ZN6icu_6718Normalizer2Factory14getNFKC_CFImplER10UErrorCode:
.LFB3268:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L319
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L320
.L304:
	movl	4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L305
	movl	%eax, (%rbx)
.L305:
	movq	_ZN6icu_67L16nfkc_cfSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L306
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L304
	movq	%rbx, %rsi
	leaq	.LC2(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L306:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3268:
	.size	_ZN6icu_6718Normalizer2Factory14getNFKC_CFImplER10UErrorCode, .-_ZN6icu_6718Normalizer2Factory14getNFKC_CFImplER10UErrorCode
	.p2align 4
	.globl	unorm2_getNFKCInstance_67
	.type	unorm2_getNFKCInstance_67, @function
unorm2_getNFKCInstance_67:
.LFB3269:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L339
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L340
.L324:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L325
	movl	%eax, (%rbx)
.L325:
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L326
	addq	$8, %rsp
	addq	$8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L324
	movq	%rbx, %rsi
	leaq	.LC1(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L326:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3269:
	.size	unorm2_getNFKCInstance_67, .-unorm2_getNFKCInstance_67
	.p2align 4
	.globl	unorm2_getNFKDInstance_67
	.type	unorm2_getNFKDInstance_67, @function
unorm2_getNFKDInstance_67:
.LFB3270:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L359
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L360
.L344:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L345
	movl	%eax, (%rbx)
.L345:
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L346
	addq	$8, %rsp
	addq	$32, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L344
	movq	%rbx, %rsi
	leaq	.LC1(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L346:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3270:
	.size	unorm2_getNFKDInstance_67, .-unorm2_getNFKDInstance_67
	.p2align 4
	.globl	unorm2_getNFKCCasefoldInstance_67
	.type	unorm2_getNFKCCasefoldInstance_67, @function
unorm2_getNFKCCasefoldInstance_67:
.LFB3271:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L379
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L380
.L364:
	movl	4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L365
	movl	%eax, (%rbx)
.L365:
	movq	_ZN6icu_67L16nfkc_cfSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L366
	addq	$8, %rsp
	addq	$8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L364
	movq	%rbx, %rsi
	leaq	.LC2(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L366:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3271:
	.size	unorm2_getNFKCCasefoldInstance_67, .-unorm2_getNFKCCasefoldInstance_67
	.p2align 4
	.globl	unorm2_getInstance_67
	.type	unorm2_getInstance_67, @function
unorm2_getInstance_67:
.LFB3272:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L441
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L383
	cmpb	$0, (%rsi)
	je	.L383
	movq	%rdi, %r15
	movl	%edx, %r14d
	testq	%rdi, %rdi
	je	.L386
.L395:
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L447
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	xorl	%edi, %edi
	movq	%rax, %r12
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	je	.L400
.L409:
	movl	(%rbx), %eax
.L399:
	testl	%eax, %eax
	jg	.L382
	cmpl	$2, %r14d
	je	.L412
	ja	.L413
	leaq	32(%r12), %rdx
	leaq	8(%r12), %rax
	testl	%r14d, %r14d
	cmovne	%rdx, %rax
.L381:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	movl	$4, %ecx
	leaq	.LC4(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L448
	leaq	.LC1(%rip), %r12
	movl	$5, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L449
	leaq	.LC2(%rip), %r12
	movl	$8, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L395
	movl	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L396
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L396
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movl	(%rbx), %eax
.L397:
	movq	_ZN6icu_67L16nfkc_cfSingletonE(%rip), %r12
	jmp	.L390
.L455:
	movl	$7, (%rbx)
.L406:
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L445:
	testq	%r12, %r12
	je	.L382
	movq	%r12, %rdi
	call	_ZN6icu_6713Norm2AllModesD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L382:
	xorl	%eax, %eax
.L450:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	cmpl	$3, %r14d
	leaq	64(%r12), %rax
	movl	$0, %edx
	cmovne	%rdx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	movl	$1, (%rbx)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	48(%r12), %rax
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%rbx, %rdi
	call	_ZN6icu_6713Norm2AllModes14getNFCInstanceER10UErrorCode@PLT
	movq	%rax, %r12
	movl	(%rbx), %eax
.L390:
	testq	%r12, %r12
	jne	.L399
	testl	%eax, %eax
	jle	.L395
	xorl	%eax, %eax
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L447:
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
.L400:
	leaq	uprv_loaded_normalizer2_cleanup(%rip), %rsi
	movl	$12, %edi
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L382
	movl	$96, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L402
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rax)
	leaq	16+_ZTVN6icu_6721LoadedNormalizer2ImplE(%rip), %rax
	movq	$0, 64(%r12)
	movups	%xmm0, 72(%r12)
	movl	(%rbx), %ecx
	movq	%rax, (%r12)
	movq	$0, 88(%r12)
	testl	%ecx, %ecx
	jg	.L403
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6721LoadedNormalizer2Impl4loadEPKcS2_R10UErrorCode.part.0
.L403:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode@PLT
	movl	(%rbx), %edx
	movq	%rax, %r12
	testl	%edx, %edx
	jg	.L445
	xorl	%edi, %edi
	call	umtx_lock_67@PLT
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L451
.L405:
	movq	%r13, %rsi
	call	uhash_get_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L452
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	je	.L411
	movq	%r12, %rdi
	call	_ZN6icu_6713Norm2AllModesD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L411:
	movq	%r15, %r12
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L449:
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L392
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L392
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movl	(%rbx), %eax
.L393:
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %r12
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L392:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L453
	movl	%eax, (%rbx)
	jmp	.L393
.L396:
	movl	4+_ZN6icu_67L15nfkc_cfInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L454
	movl	%eax, (%rbx)
	jmp	.L397
.L453:
	movl	(%rbx), %eax
	jmp	.L393
.L454:
	movl	(%rbx), %eax
	jmp	.L397
.L451:
	movq	uhash_hashChars_67@GOTPCREL(%rip), %rdi
	movq	uhash_compareChars_67@GOTPCREL(%rip), %rsi
	movq	%rbx, %rcx
	xorl	%edx, %edx
	call	uhash_open_67@PLT
	movq	%rax, _ZN6icu_67L5cacheE(%rip)
	movq	%rax, %rdi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L406
	movq	uprv_free_67@GOTPCREL(%rip), %rsi
	call	uhash_setKeyDeleter_67@PLT
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	leaq	deleteNorm2AllModes(%rip), %rsi
	call	uhash_setValueDeleter_67@PLT
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	jmp	.L405
.L452:
	movq	%r13, %rdi
	call	strlen@PLT
	leal	1(%rax), %edx
	movslq	%edx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	uprv_malloc_67@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	je	.L455
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	_ZN6icu_67L5cacheE(%rip), %rdi
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	uhash_put_67@PLT
	xorl	%edi, %edi
	call	umtx_unlock_67@PLT
	testq	%r12, %r12
	jne	.L409
	xorl	%eax, %eax
	jmp	.L450
.L402:
	movl	$7, (%rbx)
	xorl	%eax, %eax
	jmp	.L450
	.cfi_endproc
.LFE3272:
	.size	unorm2_getInstance_67, .-unorm2_getInstance_67
	.p2align 4
	.globl	unorm_getQuickCheck_67
	.type	unorm_getQuickCheck_67, @function
unorm_getQuickCheck_67:
.LFB3273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-2(%rsi), %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	cmpl	$3, %edx
	jbe	.L490
.L456:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L491
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movl	$0, -28(%rbp)
	movl	%edi, %r12d
	cmpl	$4, %esi
	je	.L458
	cmpl	$5, %esi
	je	.L459
	cmpl	$3, %esi
	je	.L460
	leaq	-28(%rbp), %rdi
	call	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode@PLT
	movq	%rax, %rdi
	movl	-28(%rbp), %eax
.L461:
	testl	%eax, %eax
	jg	.L468
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*168(%rax)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L462:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rdi
	testl	%eax, %eax
	jle	.L463
	movl	%eax, -28(%rbp)
	testq	%rdi, %rdi
	jne	.L464
	.p2align 4,,10
	.p2align 3
.L468:
	movl	$2, %eax
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L458:
	leaq	-28(%rbp), %rdi
	call	_ZN6icu_6711Normalizer214getNFCInstanceER10UErrorCode@PLT
	movq	%rax, %rdi
	movl	-28(%rbp), %eax
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L460:
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L462
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L462
	leaq	-28(%rbp), %rsi
	leaq	.LC1(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rdi
.L463:
	movl	-28(%rbp), %eax
	testq	%rdi, %rdi
	je	.L461
.L464:
	addq	$32, %rdi
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L459:
	movl	_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L465
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L465
	leaq	-28(%rbp), %rsi
	leaq	.LC1(%rip), %rdi
	call	_ZN6icu_67L14initSingletonsEPKcR10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZN6icu_67L12nfkcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12nfkcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rdi
.L466:
	movl	-28(%rbp), %eax
	testq	%rdi, %rdi
	je	.L461
.L467:
	addq	$8, %rdi
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L465:
	movl	4+_ZN6icu_67L12nfkcInitOnceE(%rip), %eax
	movq	_ZN6icu_67L13nfkcSingletonE(%rip), %rdi
	testl	%eax, %eax
	jle	.L466
	movl	%eax, -28(%rbp)
	testq	%rdi, %rdi
	jne	.L467
	jmp	.L468
.L491:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3273:
	.size	unorm_getQuickCheck_67, .-unorm_getQuickCheck_67
	.weak	_ZTSN6icu_6721LoadedNormalizer2ImplE
	.section	.rodata._ZTSN6icu_6721LoadedNormalizer2ImplE,"aG",@progbits,_ZTSN6icu_6721LoadedNormalizer2ImplE,comdat
	.align 32
	.type	_ZTSN6icu_6721LoadedNormalizer2ImplE, @object
	.size	_ZTSN6icu_6721LoadedNormalizer2ImplE, 33
_ZTSN6icu_6721LoadedNormalizer2ImplE:
	.string	"N6icu_6721LoadedNormalizer2ImplE"
	.weak	_ZTIN6icu_6721LoadedNormalizer2ImplE
	.section	.data.rel.ro._ZTIN6icu_6721LoadedNormalizer2ImplE,"awG",@progbits,_ZTIN6icu_6721LoadedNormalizer2ImplE,comdat
	.align 8
	.type	_ZTIN6icu_6721LoadedNormalizer2ImplE, @object
	.size	_ZTIN6icu_6721LoadedNormalizer2ImplE, 24
_ZTIN6icu_6721LoadedNormalizer2ImplE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6721LoadedNormalizer2ImplE
	.quad	_ZTIN6icu_6715Normalizer2ImplE
	.weak	_ZTVN6icu_6721LoadedNormalizer2ImplE
	.section	.data.rel.ro._ZTVN6icu_6721LoadedNormalizer2ImplE,"awG",@progbits,_ZTVN6icu_6721LoadedNormalizer2ImplE,comdat
	.align 8
	.type	_ZTVN6icu_6721LoadedNormalizer2ImplE, @object
	.size	_ZTVN6icu_6721LoadedNormalizer2ImplE, 40
_ZTVN6icu_6721LoadedNormalizer2ImplE:
	.quad	0
	.quad	_ZTIN6icu_6721LoadedNormalizer2ImplE
	.quad	_ZN6icu_6721LoadedNormalizer2ImplD1Ev
	.quad	_ZN6icu_6721LoadedNormalizer2ImplD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.local	_ZN6icu_67L5cacheE
	.comm	_ZN6icu_67L5cacheE,8,8
	.local	_ZN6icu_67L15nfkc_cfInitOnceE
	.comm	_ZN6icu_67L15nfkc_cfInitOnceE,8,8
	.local	_ZN6icu_67L16nfkc_cfSingletonE
	.comm	_ZN6icu_67L16nfkc_cfSingletonE,8,8
	.local	_ZN6icu_67L12nfkcInitOnceE
	.comm	_ZN6icu_67L12nfkcInitOnceE,8,8
	.local	_ZN6icu_67L13nfkcSingletonE
	.comm	_ZN6icu_67L13nfkcSingletonE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
