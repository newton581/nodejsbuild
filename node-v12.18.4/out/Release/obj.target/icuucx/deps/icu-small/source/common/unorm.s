	.file	"unorm.cpp"
	.text
	.p2align 4
	.type	_ZL8_iterateP13UCharIteratoraPDsiPKN6icu_6711Normalizer2EaPaP10UErrorCode.part.0, @function
_ZL8_iterateP13UCharIteratoraPDsiPKN6icu_6711Normalizer2EaPaP10UErrorCode.part.0:
.LFB3983:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -224(%rbp)
	movl	%r9d, -216(%rbp)
	movq	%rax, -240(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%sil, %sil
	je	.L2
	call	*48(%rdi)
	testb	%al, %al
	jne	.L3
.L5:
	movq	-232(%rbp), %rcx
	movq	-224(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r15d, %esi
	call	u_terminateUChars_67@PLT
	movl	%eax, %r12d
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$200, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	%rbx, %rdi
	movq	%rax, -192(%rbp)
	leaq	-192(%rbp), %r13
	movw	%dx, -184(%rbp)
	call	uiter_next32_67@PLT
	movl	%eax, %esi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L52:
	movq	(%r12), %rax
	movl	%esi, -212(%rbp)
	movq	%r12, %rdi
	call	*120(%rax)
	movl	-212(%rbp), %esi
	testb	%al, %al
	jne	.L51
.L48:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
	movq	%rbx, %rdi
	call	uiter_next32_67@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L52
	.p2align 4,,10
	.p2align 3
.L8:
	movq	-224(%rbp), %rsi
	leaq	-128(%rbp), %rbx
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movswl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L14
	sarl	$5, %eax
.L15:
	testl	%eax, %eax
	jle	.L16
	cmpb	$0, -216(%rbp)
	je	.L16
	movq	-232(%rbp), %r14
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	(%r12), %rax
	movq	%r14, %rcx
	call	*24(%rax)
	leaq	-200(%rbp), %rsi
	movq	%r14, %rcx
	movl	%r15d, %edx
	movq	%rax, %rdi
	movq	-224(%rbp), %rax
	movq	%rax, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	cmpq	$0, -240(%rbp)
	movswl	-120(%rbp), %r12d
	je	.L18
	movq	-232(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L53
.L18:
	testw	%r12w, %r12w
	js	.L27
	sarl	$5, %r12d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L2:
	call	*56(%rdi)
	testb	%al, %al
	je	.L5
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-192(%rbp), %r13
	movq	%rax, -192(%rbp)
	movl	$2, %eax
	movw	%ax, -184(%rbp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	%eax, %ecx
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	movq	(%r12), %rax
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*120(%rax)
	testb	%al, %al
	jne	.L8
.L13:
	movq	%rbx, %rdi
	call	uiter_previous32_67@PLT
	movl	%eax, %r14d
	testl	%eax, %eax
	jns	.L54
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L16:
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %rcx
	movl	%r15d, %edx
	movq	%r13, %rdi
	leaq	-200(%rbp), %rsi
	movq	%rax, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
.L19:
	movq	%rbx, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movl	-180(%rbp), %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L27:
	movl	-116(%rbp), %r12d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L51:
	cmpl	$65536, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	setl	%sil
	movzbl	%sil, %esi
	subl	$2, %esi
	call	*40(%rbx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L53:
	movswl	-184(%rbp), %eax
	movl	%eax, %edx
	andl	$1, %edx
	testb	$1, %r12b
	je	.L20
	movl	%edx, %eax
	xorl	$1, %eax
.L21:
	movq	-240(%rbp), %rcx
	movb	%al, (%rcx)
	jmp	.L18
.L20:
	testw	%r12w, %r12w
	js	.L22
	movswl	%r12w, %r8d
	sarl	$5, %r8d
.L23:
	testw	%ax, %ax
	js	.L24
	sarl	$5, %eax
.L25:
	cmpl	%r8d, %eax
	jne	.L30
	testb	%dl, %dl
	je	.L55
.L30:
	movl	$1, %eax
	jmp	.L21
.L24:
	movl	-180(%rbp), %eax
	jmp	.L25
.L22:
	movl	-116(%rbp), %r8d
	jmp	.L23
.L55:
	movl	%r8d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movswl	-120(%rbp), %r12d
	testb	%al, %al
	sete	%al
	jmp	.L21
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3983:
	.size	_ZL8_iterateP13UCharIteratoraPDsiPKN6icu_6711Normalizer2EaPaP10UErrorCode.part.0, .-_ZL8_iterateP13UCharIteratoraPDsiPKN6icu_6711Normalizer2EaPaP10UErrorCode.part.0
	.p2align 4
	.globl	unorm_quickCheck_67
	.type	unorm_quickCheck_67, @function
unorm_quickCheck_67:
.LFB3050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	movq	%rcx, %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	%edx, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	subq	$8, %rsp
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rcx
	movl	%r14d, %edx
	popq	%r12
	movq	%r13, %rsi
	movq	%rax, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	unorm2_quickCheck_67@PLT
	.cfi_endproc
.LFE3050:
	.size	unorm_quickCheck_67, .-unorm_quickCheck_67
	.p2align 4
	.globl	unorm_quickCheckWithOptions_67
	.type	unorm_quickCheckWithOptions_67, @function
unorm_quickCheckWithOptions_67:
.LFB3051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	movq	%r8, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	%edx, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	andl	$32, %ebx
	movq	%rax, %r15
	je	.L59
	movq	%r12, %rdi
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rbx
	call	uniset_getUnicode32Instance_67@PLT
	movq	%r15, -72(%rbp)
	leaq	-80(%rbp), %r15
	movq	%r12, %rcx
	movq	%r15, %rdi
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rbx, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	unorm2_quickCheck_67@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
.L58:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	unorm2_quickCheck_67@PLT
	movl	%eax, %r12d
	jmp	.L58
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3051:
	.size	unorm_quickCheckWithOptions_67, .-unorm_quickCheckWithOptions_67
	.p2align 4
	.globl	unorm_isNormalized_67
	.type	unorm_isNormalized_67, @function
unorm_isNormalized_67:
.LFB3052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	movq	%rcx, %rsi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	%edx, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	subq	$8, %rsp
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	addq	$8, %rsp
	movq	%r12, %rcx
	movl	%r14d, %edx
	popq	%r12
	movq	%r13, %rsi
	movq	%rax, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	unorm2_isNormalized_67@PLT
	.cfi_endproc
.LFE3052:
	.size	unorm_isNormalized_67, .-unorm_isNormalized_67
	.p2align 4
	.globl	unorm_isNormalizedWithOptions_67
	.type	unorm_isNormalizedWithOptions_67, @function
unorm_isNormalizedWithOptions_67:
.LFB3053:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	movq	%r8, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	%edx, %edi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	andl	$32, %ebx
	movq	%rax, %r15
	je	.L67
	movq	%r12, %rdi
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rbx
	call	uniset_getUnicode32Instance_67@PLT
	movq	%r15, -72(%rbp)
	leaq	-80(%rbp), %r15
	movq	%r12, %rcx
	movq	%r15, %rdi
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rbx, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	unorm2_isNormalized_67@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
.L66:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	unorm2_isNormalized_67@PLT
	movl	%eax, %r12d
	jmp	.L66
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3053:
	.size	unorm_isNormalizedWithOptions_67, .-unorm_isNormalizedWithOptions_67
	.p2align 4
	.globl	unorm_normalize_67
	.type	unorm_normalize_67, @function
unorm_normalize_67:
.LFB3054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	%edx, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movl	%r9d, -92(%rbp)
	movq	16(%rbp), %r9
	movq	%r9, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r9, -88(%rbp)
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	andl	$32, %ebx
	movq	-88(%rbp), %r9
	movl	-92(%rbp), %r8d
	movq	%rax, %r12
	je	.L73
	movq	%r9, %rdi
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rbx
	call	uniset_getUnicode32Instance_67@PLT
	movq	%rbx, -80(%rbp)
	movq	-88(%rbp), %r9
	movq	%r15, %rcx
	leaq	-80(%rbp), %rbx
	movl	-92(%rbp), %r8d
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r12, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	unorm2_normalize_67@PLT
	movq	%rbx, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
.L72:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	unorm2_normalize_67@PLT
	movl	%eax, %r12d
	jmp	.L72
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3054:
	.size	unorm_normalize_67, .-unorm_normalize_67
	.p2align 4
	.globl	unorm_previous_67
	.type	unorm_previous_67, @function
unorm_previous_67:
.LFB3057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	%ecx, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	24(%rbp), %r15
	movq	%rax, -88(%rbp)
	movq	%r15, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movsbl	%r9b, %eax
	movl	%eax, -92(%rbp)
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movq	%rax, %r8
	movl	%ebx, %eax
	andl	$32, %eax
	je	.L79
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	call	uniset_getUnicode32Instance_67@PLT
	movl	(%r15), %r9d
	movq	-104(%rbp), %r8
	movq	%rax, %rcx
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L100
.L78:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L101
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L78
	testl	%r12d, %r12d
	js	.L87
	testq	%r13, %r13
	sete	%sil
	testl	%r12d, %r12d
	setg	%cl
	testb	%cl, %sil
	jne	.L87
	testq	%r14, %r14
	je	.L87
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L90
	movb	$0, (%rax)
.L90:
	pushq	%r15
	movl	-92(%rbp), %r9d
	movl	%r12d, %ecx
	movq	%r13, %rdx
	pushq	-88(%rbp)
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZL8_iterateP13UCharIteratoraPDsiPKN6icu_6711Normalizer2EaPaP10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rax
	movq	%r8, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rcx, -64(%rbp)
	testl	%r12d, %r12d
	js	.L81
	testq	%r13, %r13
	sete	%cl
	testl	%r12d, %r12d
	setg	%al
	testb	%al, %cl
	jne	.L81
	testq	%r14, %r14
	je	.L81
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L85
	movb	$0, (%rax)
.L85:
	pushq	%r15
	movl	-92(%rbp), %r9d
	leaq	-80(%rbp), %r8
	movq	%r14, %rdi
	pushq	-88(%rbp)
	movl	%r12d, %ecx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%r8, -88(%rbp)
	call	_ZL8_iterateP13UCharIteratoraPDsiPKN6icu_6711Normalizer2EaPaP10UErrorCode.part.0
	popq	%rdi
	popq	%r8
	movq	-88(%rbp), %r8
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$1, (%r15)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$1, (%r15)
	xorl	%eax, %eax
	leaq	-80(%rbp), %r8
.L84:
	movq	%r8, %rdi
	movl	%eax, -88(%rbp)
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
	movl	-88(%rbp), %eax
	jmp	.L78
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3057:
	.size	unorm_previous_67, .-unorm_previous_67
	.p2align 4
	.globl	unorm_next_67
	.type	unorm_next_67, @function
unorm_next_67:
.LFB3058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	%ecx, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	24(%rbp), %r15
	movq	%rax, -88(%rbp)
	movq	%r15, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movsbl	%r9b, %eax
	movl	%eax, -92(%rbp)
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movq	%rax, %r8
	movl	%ebx, %eax
	andl	$32, %eax
	je	.L103
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	call	uniset_getUnicode32Instance_67@PLT
	movl	(%r15), %r9d
	movq	-104(%rbp), %r8
	movq	%rax, %rcx
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L124
.L102:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L125
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L102
	testl	%r12d, %r12d
	js	.L111
	testq	%r13, %r13
	sete	%sil
	testl	%r12d, %r12d
	setg	%cl
	testb	%cl, %sil
	jne	.L111
	testq	%r14, %r14
	je	.L111
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L114
	movb	$0, (%rax)
.L114:
	pushq	%r15
	movl	-92(%rbp), %r9d
	movl	%r12d, %ecx
	movq	%r13, %rdx
	pushq	-88(%rbp)
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZL8_iterateP13UCharIteratoraPDsiPKN6icu_6711Normalizer2EaPaP10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rax
	movq	%r8, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rcx, -64(%rbp)
	testl	%r12d, %r12d
	js	.L105
	testq	%r13, %r13
	sete	%cl
	testl	%r12d, %r12d
	setg	%al
	testb	%al, %cl
	jne	.L105
	testq	%r14, %r14
	je	.L105
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L109
	movb	$0, (%rax)
.L109:
	pushq	%r15
	movl	-92(%rbp), %r9d
	leaq	-80(%rbp), %r8
	movq	%r14, %rdi
	pushq	-88(%rbp)
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r8, -88(%rbp)
	call	_ZL8_iterateP13UCharIteratoraPDsiPKN6icu_6711Normalizer2EaPaP10UErrorCode.part.0
	popq	%rdi
	popq	%r8
	movq	-88(%rbp), %r8
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$1, (%r15)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$1, (%r15)
	xorl	%eax, %eax
	leaq	-80(%rbp), %r8
.L108:
	movq	%r8, %rdi
	movl	%eax, -88(%rbp)
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
	movl	-88(%rbp), %eax
	jmp	.L102
.L125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3058:
	.size	unorm_next_67, .-unorm_next_67
	.p2align 4
	.globl	unorm_concatenate_67
	.type	unorm_concatenate_67, @function
unorm_concatenate_67:
.LFB3060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$232, %rsp
	movl	%esi, -248(%rbp)
	movq	32(%rbp), %r15
	movl	%ecx, -252(%rbp)
	movl	16(%rbp), %edi
	movq	%r15, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movq	%rax, %r9
	movl	24(%rbp), %eax
	andl	$32, %eax
	je	.L127
	movq	%r15, %rdi
	movq	%r9, -264(%rbp)
	call	uniset_getUnicode32Instance_67@PLT
	movl	(%r15), %r8d
	movq	-264(%rbp), %r9
	movq	%rax, %rdx
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L176
.L126:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L177
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movl	(%r15), %esi
	testl	%esi, %esi
	jg	.L126
	testl	%r12d, %r12d
	js	.L148
	testq	%rbx, %rbx
	jne	.L156
	testl	%r12d, %r12d
	jg	.L148
.L156:
	testq	%r14, %r14
	je	.L148
	cmpl	$-1, -248(%rbp)
	jl	.L148
	testq	%r13, %r13
	je	.L148
	cmpl	$-1, -252(%rbp)
	jl	.L148
	testq	%rbx, %rbx
	je	.L146
	cmpq	%rbx, %r13
	jb	.L147
	movslq	%r12d, %rdx
	leaq	(%rbx,%rdx,2), %rdx
	cmpq	%rdx, %r13
	jb	.L148
.L147:
	movl	-252(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L149
	cmpq	%rbx, %r13
	ja	.L149
	movslq	%ecx, %rdx
	leaq	0(%r13,%rdx,2), %rdx
	cmpq	%rdx, %rbx
	jb	.L148
.L149:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	movq	%rax, -192(%rbp)
	movw	%cx, -184(%rbp)
	cmpq	%rbx, %r14
	jne	.L151
	leaq	-192(%rbp), %rax
	movl	-248(%rbp), %edx
	movl	%r12d, %ecx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%r9, -272(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movq	-272(%rbp), %r9
.L152:
	movq	(%r9), %rax
	movl	-252(%rbp), %ecx
	xorl	%esi, %esi
	leaq	-232(%rbp), %rdx
	movq	%r9, -248(%rbp)
	cmpl	$-1, %ecx
	movq	48(%rax), %r14
	movq	%r13, -232(%rbp)
	leaq	-128(%rbp), %r13
	sete	%sil
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-248(%rbp), %r9
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	-264(%rbp), %rsi
	movq	%r9, %rdi
	call	*%r14
	leaq	-224(%rbp), %rsi
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	%rax, %rdi
	movq	%rbx, -224(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -248(%rbp)
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-264(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-248(%rbp), %eax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$1, (%r15)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L176:
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rax
	movq	%r9, -216(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rdx, -208(%rbp)
	testl	%r12d, %r12d
	js	.L136
	testq	%rbx, %rbx
	jne	.L155
	testl	%r12d, %r12d
	jle	.L155
.L136:
	movl	$1, (%r15)
	xorl	%eax, %eax
	leaq	-224(%rbp), %r13
.L133:
	movq	%r13, %rdi
	movl	%eax, -248(%rbp)
	call	_ZN6icu_6719FilteredNormalizer2D1Ev@PLT
	movl	-248(%rbp), %eax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L155:
	testq	%r14, %r14
	je	.L136
	cmpl	$-1, -248(%rbp)
	jl	.L136
	testq	%r13, %r13
	je	.L136
	cmpl	$-1, -252(%rbp)
	jl	.L136
	testq	%rbx, %rbx
	je	.L134
	cmpq	%rbx, %r13
	jb	.L135
	movslq	%r12d, %rax
	leaq	(%rbx,%rax,2), %rax
	cmpq	%rax, %r13
	jb	.L136
.L135:
	movslq	-252(%rbp), %rax
	testl	%eax, %eax
	jle	.L137
	cmpq	%rbx, %r13
	ja	.L137
	leaq	0(%r13,%rax,2), %rax
	cmpq	%rax, %rbx
	jb	.L136
.L137:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edi
	movq	%rax, -192(%rbp)
	movw	%di, -184(%rbp)
	cmpq	%rbx, %r14
	jne	.L139
	leaq	-192(%rbp), %rax
	movl	-248(%rbp), %edx
	movl	%r12d, %ecx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
.L140:
	movl	-252(%rbp), %ecx
	leaq	-128(%rbp), %r14
	xorl	%esi, %esi
	leaq	-240(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r13, -240(%rbp)
	leaq	-224(%rbp), %r13
	cmpl	$-1, %ecx
	sete	%sil
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	-264(%rbp), %rsi
	call	_ZNK6icu_6719FilteredNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode@PLT
	leaq	-232(%rbp), %rsi
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	%rax, %rdi
	movq	%rbx, -232(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -248(%rbp)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-264(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-248(%rbp), %eax
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L146:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -192(%rbp)
	movl	$2, %eax
	movw	%ax, -184(%rbp)
.L151:
	leaq	-192(%rbp), %rax
	movl	%r12d, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%r9, -272(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movl	-248(%rbp), %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	-264(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-272(%rbp), %r9
	jmp	.L152
.L134:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	%rax, -192(%rbp)
	movw	%dx, -184(%rbp)
.L139:
	leaq	-192(%rbp), %rax
	movl	%r12d, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEPDsii@PLT
	movl	-248(%rbp), %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	-264(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L140
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3060:
	.size	unorm_concatenate_67, .-unorm_concatenate_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
