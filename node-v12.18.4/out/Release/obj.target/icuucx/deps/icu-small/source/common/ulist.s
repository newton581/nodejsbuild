	.file	"ulist.cpp"
	.text
	.p2align 4
	.globl	ulist_createEmptyList_67
	.type	ulist_createEmptyList_67, @function
ulist_createEmptyList_67:
.LFB2073:
	.cfi_startproc
	endbr64
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L6
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L10
	movq	$0, 16(%rax)
	pxor	%xmm0, %xmm0
	movl	$0, 24(%rax)
	movups	%xmm0, (%rax)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore 3
	.cfi_restore 6
	ret
.L10:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	$7, (%rbx)
	jmp	.L1
	.cfi_endproc
.LFE2073:
	.size	ulist_createEmptyList_67, .-ulist_createEmptyList_67
	.p2align 4
	.globl	ulist_addItemEndList_67
	.type	ulist_addItemEndList_67, @function
ulist_addItemEndList_67:
.LFB2076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L12
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L12
	testq	%rsi, %rsi
	je	.L12
	movl	$32, %edi
	movq	%rcx, %r12
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L21
	movl	24(%rbx), %edx
	movq	%r14, (%rax)
	movb	%r13b, 24(%rax)
	testl	%edx, %edx
	je	.L22
	movq	16(%rbx), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, 16(%rax)
	movq	%rax, 8(%rcx)
	movq	%rax, 16(%rbx)
.L18:
	addl	$1, %edx
	movl	%edx, 24(%rbx)
.L11:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	testb	%r13b, %r13b
	je	.L11
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	jmp	.L18
.L21:
	testb	%r13b, %r13b
	jne	.L23
.L16:
	movl	$7, (%r12)
	jmp	.L11
.L23:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L16
	.cfi_endproc
.LFE2076:
	.size	ulist_addItemEndList_67, .-ulist_addItemEndList_67
	.p2align 4
	.globl	ulist_addItemBeginList_67
	.type	ulist_addItemBeginList_67, @function
ulist_addItemBeginList_67:
.LFB2077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L25
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L25
	testq	%rsi, %rsi
	je	.L25
	movl	$32, %edi
	movq	%rcx, %r12
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L34
	movl	24(%rbx), %edx
	movq	%r14, (%rax)
	movb	%r13b, 24(%rax)
	testl	%edx, %edx
	je	.L35
	movq	8(%rbx), %rcx
	movq	$0, 16(%rax)
	movq	%rcx, 8(%rax)
	movq	%rax, 16(%rcx)
	movq	%rax, 8(%rbx)
.L31:
	addl	$1, %edx
	movl	%edx, 24(%rbx)
.L24:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	testb	%r13b, %r13b
	je	.L24
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	jmp	.L31
.L34:
	testb	%r13b, %r13b
	jne	.L36
.L29:
	movl	$7, (%r12)
	jmp	.L24
.L36:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L29
	.cfi_endproc
.LFE2077:
	.size	ulist_addItemBeginList_67, .-ulist_addItemBeginList_67
	.p2align 4
	.globl	ulist_containsString_67
	.type	ulist_containsString_67, @function
ulist_containsString_67:
.LFB2078:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L47
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L37
	movq	%rsi, %r14
	movl	%edx, %r13d
	movslq	%edx, %r15
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L39:
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L50
.L40:
	movq	(%rbx), %r12
	movq	%r12, %rdi
	call	strlen@PLT
	cmpl	%eax, %r13d
	jne	.L39
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L39
	movl	$1, %eax
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE2078:
	.size	ulist_containsString_67, .-ulist_containsString_67
	.p2align 4
	.globl	ulist_removeString_67
	.type	ulist_removeString_67, @function
ulist_removeString_67:
.LFB2079:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L66
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.L60
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L53:
	testq	%rbx, %rbx
	je	.L69
.L60:
	movq	(%rbx), %r12
	movq	%r13, %rdi
	movq	%rbx, %r15
	movq	%r12, %rsi
	call	strcmp@PLT
	movq	8(%rbx), %rbx
	testl	%eax, %eax
	jne	.L53
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.L70
	movq	%rbx, 8(%rax)
	movq	8(%r15), %rbx
.L55:
	testq	%rbx, %rbx
	je	.L71
	movq	%rax, 16(%rbx)
.L57:
	cmpq	%r15, (%r14)
	je	.L72
.L58:
	subl	$1, 24(%r14)
	cmpb	$0, 24(%r15)
	jne	.L73
.L59:
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	movl	$1, %eax
.L51:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rbx, (%r14)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%rax, 16(%r14)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%rbx, 8(%r14)
	jmp	.L55
	.cfi_endproc
.LFE2079:
	.size	ulist_removeString_67, .-ulist_removeString_67
	.p2align 4
	.globl	ulist_getNext_67
	.type	ulist_getNext_67, @function
ulist_getNext_67:
.LFB2080:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L76
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L74
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%eax, %eax
.L74:
	ret
	.cfi_endproc
.LFE2080:
	.size	ulist_getNext_67, .-ulist_getNext_67
	.p2align 4
	.globl	ulist_getListSize_67
	.type	ulist_getListSize_67, @function
ulist_getListSize_67:
.LFB2081:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L82
	movl	24(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2081:
	.size	ulist_getListSize_67, .-ulist_getListSize_67
	.p2align 4
	.globl	ulist_resetList_67
	.type	ulist_resetList_67, @function
ulist_resetList_67:
.LFB2082:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L83
	movq	8(%rdi), %rax
	movq	%rax, (%rdi)
.L83:
	ret
	.cfi_endproc
.LFE2082:
	.size	ulist_resetList_67, .-ulist_resetList_67
	.p2align 4
	.globl	ulist_deleteList_67
	.type	ulist_deleteList_67, @function
ulist_deleteList_67:
.LFB2083:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L88
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %r12
	testq	%r12, %r12
	jne	.L93
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	testq	%rbx, %rbx
	je	.L90
.L92:
	movq	%rbx, %r12
.L93:
	cmpb	$0, 24(%r12)
	movq	8(%r12), %rbx
	je	.L91
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	testq	%rbx, %rbx
	jne	.L92
.L90:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	ret
	.cfi_endproc
.LFE2083:
	.size	ulist_deleteList_67, .-ulist_deleteList_67
	.p2align 4
	.globl	ulist_close_keyword_values_iterator_67
	.type	ulist_close_keyword_values_iterator_67, @function
ulist_close_keyword_values_iterator_67:
.LFB2084:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L106
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r14
	testq	%r14, %r14
	je	.L108
	movq	8(%r14), %r12
	testq	%r12, %r12
	jne	.L112
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	testq	%rbx, %rbx
	je	.L109
.L111:
	movq	%rbx, %r12
.L112:
	cmpb	$0, 24(%r12)
	movq	8(%r12), %rbx
	je	.L110
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	testq	%rbx, %rbx
	jne	.L111
.L109:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
.L108:
	popq	%rbx
	.cfi_restore 3
	movq	%r13, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	ret
	.cfi_endproc
.LFE2084:
	.size	ulist_close_keyword_values_iterator_67, .-ulist_close_keyword_values_iterator_67
	.p2align 4
	.globl	ulist_count_keyword_values_67
	.type	ulist_count_keyword_values_67, @function
ulist_count_keyword_values_67:
.LFB2085:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L131
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L131
	movl	24(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE2085:
	.size	ulist_count_keyword_values_67, .-ulist_count_keyword_values_67
	.p2align 4
	.globl	ulist_next_keyword_value_67
	.type	ulist_next_keyword_value_67, @function
ulist_next_keyword_value_67:
.LFB2086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movl	(%rdx), %eax
	testl	%eax, %eax
	jg	.L132
	movq	8(%rdi), %r12
	testq	%r12, %r12
	je	.L132
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L135
	movq	8(%rax), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.L132
	movq	%rsi, %rbx
	testq	%rsi, %rsi
	je	.L132
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, (%rbx)
.L132:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L132
	.cfi_endproc
.LFE2086:
	.size	ulist_next_keyword_value_67, .-ulist_next_keyword_value_67
	.p2align 4
	.globl	ulist_reset_keyword_values_iterator_67
	.type	ulist_reset_keyword_values_iterator_67, @function
ulist_reset_keyword_values_iterator_67:
.LFB2087:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L146
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L146
	movq	8(%rax), %rdx
	movq	%rdx, (%rax)
.L146:
	ret
	.cfi_endproc
.LFE2087:
	.size	ulist_reset_keyword_values_iterator_67, .-ulist_reset_keyword_values_iterator_67
	.p2align 4
	.globl	ulist_getListFromEnum_67
	.type	ulist_getListFromEnum_67, @function
ulist_getListFromEnum_67:
.LFB2088:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE2088:
	.size	ulist_getListFromEnum_67, .-ulist_getListFromEnum_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
