	.file	"normalizer2.cpp"
	.text
	.section	.text._ZNK6icu_6719Normalizer2WithImpl13getQuickCheckEi,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl13getQuickCheckEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl13getQuickCheckEi
	.type	_ZNK6icu_6719Normalizer2WithImpl13getQuickCheckEi, @function
_ZNK6icu_6719Normalizer2WithImpl13getQuickCheckEi:
.LFB3168:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3168:
	.size	_ZNK6icu_6719Normalizer2WithImpl13getQuickCheckEi, .-_ZNK6icu_6719Normalizer2WithImpl13getQuickCheckEi
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Normalizer219getRawDecompositionEiRNS_13UnicodeStringE
	.type	_ZNK6icu_6711Normalizer219getRawDecompositionEiRNS_13UnicodeStringE, @function
_ZNK6icu_6711Normalizer219getRawDecompositionEiRNS_13UnicodeStringE:
.LFB3210:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3210:
	.size	_ZNK6icu_6711Normalizer219getRawDecompositionEiRNS_13UnicodeStringE, .-_ZNK6icu_6711Normalizer219getRawDecompositionEiRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Normalizer211composePairEii
	.type	_ZNK6icu_6711Normalizer211composePairEii, @function
_ZNK6icu_6711Normalizer211composePairEii:
.LFB3211:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3211:
	.size	_ZNK6icu_6711Normalizer211composePairEii, .-_ZNK6icu_6711Normalizer211composePairEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Normalizer217getCombiningClassEi
	.type	_ZNK6icu_6711Normalizer217getCombiningClassEi, @function
_ZNK6icu_6711Normalizer217getCombiningClassEi:
.LFB3212:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3212:
	.size	_ZNK6icu_6711Normalizer217getCombiningClassEi, .-_ZNK6icu_6711Normalizer217getCombiningClassEi
	.section	.text._ZNK6icu_6715NoopNormalizer216getDecompositionEiRNS_13UnicodeStringE,"axG",@progbits,_ZNK6icu_6715NoopNormalizer216getDecompositionEiRNS_13UnicodeStringE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer216getDecompositionEiRNS_13UnicodeStringE
	.type	_ZNK6icu_6715NoopNormalizer216getDecompositionEiRNS_13UnicodeStringE, @function
_ZNK6icu_6715NoopNormalizer216getDecompositionEiRNS_13UnicodeStringE:
.LFB3218:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3218:
	.size	_ZNK6icu_6715NoopNormalizer216getDecompositionEiRNS_13UnicodeStringE, .-_ZNK6icu_6715NoopNormalizer216getDecompositionEiRNS_13UnicodeStringE
	.section	.text._ZNK6icu_6715NoopNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode,"axG",@progbits,_ZNK6icu_6715NoopNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6715NoopNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6715NoopNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode:
.LFB3219:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	testl	%eax, %eax
	setle	%al
	ret
	.cfi_endproc
.LFE3219:
	.size	_ZNK6icu_6715NoopNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6715NoopNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.section	.text._ZNK6icu_6715NoopNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode,"axG",@progbits,_ZNK6icu_6715NoopNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.type	_ZNK6icu_6715NoopNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode, @function
_ZNK6icu_6715NoopNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode:
.LFB3220:
	.cfi_startproc
	endbr64
	movl	(%rcx), %eax
	testl	%eax, %eax
	setle	%al
	ret
	.cfi_endproc
.LFE3220:
	.size	_ZNK6icu_6715NoopNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode, .-_ZNK6icu_6715NoopNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.section	.text._ZNK6icu_6715NoopNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode,"axG",@progbits,_ZNK6icu_6715NoopNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6715NoopNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6715NoopNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode:
.LFB3221:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3221:
	.size	_ZNK6icu_6715NoopNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6715NoopNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode
	.section	.text._ZNK6icu_6715NoopNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode,"axG",@progbits,_ZNK6icu_6715NoopNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6715NoopNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6715NoopNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode:
.LFB3222:
	.cfi_startproc
	endbr64
	movswl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L11
	sarl	$5, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	12(%rsi), %eax
	ret
	.cfi_endproc
.LFE3222:
	.size	_ZNK6icu_6715NoopNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6715NoopNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.section	.text._ZNK6icu_6715NoopNormalizer217hasBoundaryBeforeEi,"axG",@progbits,_ZNK6icu_6715NoopNormalizer217hasBoundaryBeforeEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer217hasBoundaryBeforeEi
	.type	_ZNK6icu_6715NoopNormalizer217hasBoundaryBeforeEi, @function
_ZNK6icu_6715NoopNormalizer217hasBoundaryBeforeEi:
.LFB3223:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3223:
	.size	_ZNK6icu_6715NoopNormalizer217hasBoundaryBeforeEi, .-_ZNK6icu_6715NoopNormalizer217hasBoundaryBeforeEi
	.section	.text._ZNK6icu_6715NoopNormalizer216hasBoundaryAfterEi,"axG",@progbits,_ZNK6icu_6715NoopNormalizer216hasBoundaryAfterEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer216hasBoundaryAfterEi
	.type	_ZNK6icu_6715NoopNormalizer216hasBoundaryAfterEi, @function
_ZNK6icu_6715NoopNormalizer216hasBoundaryAfterEi:
.LFB3224:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3224:
	.size	_ZNK6icu_6715NoopNormalizer216hasBoundaryAfterEi, .-_ZNK6icu_6715NoopNormalizer216hasBoundaryAfterEi
	.section	.text._ZNK6icu_6715NoopNormalizer27isInertEi,"axG",@progbits,_ZNK6icu_6715NoopNormalizer27isInertEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer27isInertEi
	.type	_ZNK6icu_6715NoopNormalizer27isInertEi, @function
_ZNK6icu_6715NoopNormalizer27isInertEi:
.LFB3225:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3225:
	.size	_ZNK6icu_6715NoopNormalizer27isInertEi, .-_ZNK6icu_6715NoopNormalizer27isInertEi
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NoopNormalizer2D2Ev
	.type	_ZN6icu_6715NoopNormalizer2D2Ev, @function
_ZN6icu_6715NoopNormalizer2D2Ev:
.LFB3227:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3227:
	.size	_ZN6icu_6715NoopNormalizer2D2Ev, .-_ZN6icu_6715NoopNormalizer2D2Ev
	.globl	_ZN6icu_6715NoopNormalizer2D1Ev
	.set	_ZN6icu_6715NoopNormalizer2D1Ev,_ZN6icu_6715NoopNormalizer2D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720DecomposeNormalizer2D2Ev
	.type	_ZN6icu_6720DecomposeNormalizer2D2Ev, @function
_ZN6icu_6720DecomposeNormalizer2D2Ev:
.LFB3235:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3235:
	.size	_ZN6icu_6720DecomposeNormalizer2D2Ev, .-_ZN6icu_6720DecomposeNormalizer2D2Ev
	.globl	_ZN6icu_6720DecomposeNormalizer2D1Ev
	.set	_ZN6icu_6720DecomposeNormalizer2D1Ev,_ZN6icu_6720DecomposeNormalizer2D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718ComposeNormalizer2D2Ev
	.type	_ZN6icu_6718ComposeNormalizer2D2Ev, @function
_ZN6icu_6718ComposeNormalizer2D2Ev:
.LFB3239:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3239:
	.size	_ZN6icu_6718ComposeNormalizer2D2Ev, .-_ZN6icu_6718ComposeNormalizer2D2Ev
	.globl	_ZN6icu_6718ComposeNormalizer2D1Ev
	.set	_ZN6icu_6718ComposeNormalizer2D1Ev,_ZN6icu_6718ComposeNormalizer2D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714FCDNormalizer2D2Ev
	.type	_ZN6icu_6714FCDNormalizer2D2Ev, @function
_ZN6icu_6714FCDNormalizer2D2Ev:
.LFB3243:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3243:
	.size	_ZN6icu_6714FCDNormalizer2D2Ev, .-_ZN6icu_6714FCDNormalizer2D2Ev
	.globl	_ZN6icu_6714FCDNormalizer2D1Ev
	.set	_ZN6icu_6714FCDNormalizer2D1Ev,_ZN6icu_6714FCDNormalizer2D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715NoopNormalizer2D0Ev
	.type	_ZN6icu_6715NoopNormalizer2D0Ev, @function
_ZN6icu_6715NoopNormalizer2D0Ev:
.LFB3229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3229:
	.size	_ZN6icu_6715NoopNormalizer2D0Ev, .-_ZN6icu_6715NoopNormalizer2D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720DecomposeNormalizer2D0Ev
	.type	_ZN6icu_6720DecomposeNormalizer2D0Ev, @function
_ZN6icu_6720DecomposeNormalizer2D0Ev:
.LFB3237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3237:
	.size	_ZN6icu_6720DecomposeNormalizer2D0Ev, .-_ZN6icu_6720DecomposeNormalizer2D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718ComposeNormalizer2D0Ev
	.type	_ZN6icu_6718ComposeNormalizer2D0Ev, @function
_ZN6icu_6718ComposeNormalizer2D0Ev:
.LFB3241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3241:
	.size	_ZN6icu_6718ComposeNormalizer2D0Ev, .-_ZN6icu_6718ComposeNormalizer2D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6714FCDNormalizer2D0Ev
	.type	_ZN6icu_6714FCDNormalizer2D0Ev, @function
_ZN6icu_6714FCDNormalizer2D0Ev:
.LFB3245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3245:
	.size	_ZN6icu_6714FCDNormalizer2D0Ev, .-_ZN6icu_6714FCDNormalizer2D0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Normalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.type	_ZNK6icu_6711Normalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode, @function
_ZNK6icu_6711Normalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode:
.LFB3213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L28
	movq	(%rdi), %rax
	leaq	-112(%rbp), %r15
	movq	%rcx, %r12
	movq	%rdi, %r13
	movq	%r15, %rdi
	movq	88(%rax), %r14
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	*%r14
	movq	%r15, %rdi
	testb	%al, %al
	setne	%r14b
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L28:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$80, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3213:
	.size	_ZNK6icu_6711Normalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode, .-_ZNK6icu_6711Normalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6711Normalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZNK6icu_6711Normalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZNK6icu_6711Normalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB3209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$160, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	16(%rbp), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%r12), %edx
	testl	%edx, %edx
	jg	.L35
	testq	%r9, %r9
	je	.L37
	movl	$16, (%r12)
.L35:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$160, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	leaq	-176(%rbp), %r14
	movq	%rdi, %r13
	movq	%rcx, %rdx
	movq	%r8, -184(%rbp)
	movq	%r14, %rdi
	leaq	-112(%rbp), %r15
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -112(%rbp)
	movl	$2, %eax
	movw	%ax, -104(%rbp)
	movq	0(%r13), %rax
	call	*24(%rax)
	movq	-184(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L35
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3209:
	.size	_ZNK6icu_6711Normalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZNK6icu_6711Normalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.section	.text._ZNK6icu_6714FCDNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode,"axG",@progbits,_ZNK6icu_6714FCDNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714FCDNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6714FCDNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6714FCDNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode:
.LFB3196:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode@PLT
	.cfi_endproc
.LFE3196:
	.size	_ZNK6icu_6714FCDNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6714FCDNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode
	.section	.text._ZNK6icu_6714FCDNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode,"axG",@progbits,_ZNK6icu_6714FCDNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714FCDNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode
	.type	_ZNK6icu_6714FCDNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode, @function
_ZNK6icu_6714FCDNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode:
.LFB3198:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	jmp	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode@PLT
	.cfi_endproc
.LFE3198:
	.size	_ZNK6icu_6714FCDNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode, .-_ZNK6icu_6714FCDNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode
	.section	.text._ZNK6icu_6718ComposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6718ComposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6718ComposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode:
.LFB3182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movsbl	16(%rdi), %ecx
	movq	8(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r8
	movl	$1, %r8d
	call	_ZNK6icu_6715Normalizer2Impl7composeEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3182:
	.size	_ZNK6icu_6718ComposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6718ComposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode
	.section	.text._ZNK6icu_6720DecomposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode,"axG",@progbits,_ZNK6icu_6720DecomposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6720DecomposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6720DecomposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6720DecomposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode:
.LFB3172:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode@PLT
	.cfi_endproc
.LFE3172:
	.size	_ZNK6icu_6720DecomposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6720DecomposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode
	.section	.text._ZNK6icu_6720DecomposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode,"axG",@progbits,_ZNK6icu_6720DecomposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6720DecomposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode
	.type	_ZNK6icu_6720DecomposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode, @function
_ZNK6icu_6720DecomposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode:
.LFB3174:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	jmp	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode@PLT
	.cfi_endproc
.LFE3174:
	.size	_ZNK6icu_6720DecomposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode, .-_ZNK6icu_6720DecomposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode
	.section	.text._ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.type	_ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode, @function
_ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode:
.LFB3157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L71
	movzwl	8(%rsi), %eax
	movq	%rsi, %rbx
	movq	%rcx, %r13
	testb	$17, %al
	jne	.L51
	movq	%rdi, %r14
	leaq	10(%rsi), %r15
	testb	$2, %al
	je	.L72
.L53:
	cmpq	%r12, %rbx
	je	.L51
	testq	%r15, %r15
	je	.L51
	movzwl	8(%r12), %edx
	pxor	%xmm0, %xmm0
	movq	%r12, -120(%rbp)
	movq	$0, -96(%rbp)
	movl	%edx, %eax
	movl	$0, -88(%rbp)
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	movb	$0, -84(%rbp)
	cmovne	%edx, %eax
	movaps	%xmm0, -112(%rbp)
	movw	%ax, 8(%r12)
	movq	8(%r14), %rax
	movswl	8(%rbx), %esi
	movq	%rax, -128(%rbp)
	testw	%si, %si
	js	.L57
	sarl	$5, %esi
.L58:
	leaq	-128(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rcx, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L59
	movq	(%r14), %rax
	movq	-136(%rbp), %rcx
	movq	144(%rax), %r9
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L60
	sarl	$5, %eax
.L61:
	cltq
	movq	%r13, %r8
	movq	%r15, %rsi
	movq	%r14, %rdi
	leaq	(%r15,%rax,2), %rdx
	call	*%r9
.L59:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L50
	movq	-96(%rbp), %rsi
	movq	-120(%rbp), %rdi
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
.L50:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	24(%rsi), %r15
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L57:
	movl	12(%rbx), %esi
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$1, 0(%r13)
.L71:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L60:
	movl	12(%rbx), %eax
	jmp	.L61
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3157:
	.size	_ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode, .-_ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.section	.text._ZNK6icu_6718ComposeNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6718ComposeNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6718ComposeNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode:
.LFB3185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L74
	movzwl	8(%rsi), %eax
	movq	%rsi, %r14
	movq	%rdx, %rbx
	testb	$17, %al
	jne	.L76
	movq	%rdi, %r15
	leaq	10(%rsi), %r10
	testb	$2, %al
	jne	.L78
	movq	24(%rsi), %r10
	testq	%r10, %r10
	jne	.L78
.L76:
	movl	$1, (%rbx)
	xorl	%r12d, %r12d
.L74:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L92
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %ecx
	leaq	-128(%rbp), %r13
	movq	%rbx, %rdx
	leaq	-192(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	8(%r15), %rax
	movl	$5, %esi
	movq	%r9, %rdi
	movq	%r10, -208(%rbp)
	movw	%cx, -120(%rbp)
	movq	%rax, -192(%rbp)
	movq	%r13, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movl	$0, -152(%rbp)
	movb	$0, -148(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L79
	movswl	8(%r14), %eax
	movq	8(%r15), %rdi
	movsbl	16(%r15), %ecx
	movq	-200(%rbp), %r9
	testw	%ax, %ax
	movq	-208(%rbp), %r10
	js	.L80
	sarl	$5, %eax
.L81:
	subq	$8, %rsp
	cltq
	xorl	%r8d, %r8d
	movq	%r10, %rsi
	pushq	%rbx
	leaq	(%r10,%rax,2), %rdx
	call	_ZNK6icu_6715Normalizer2Impl7composeEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode@PLT
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
.L79:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L82
	movq	-160(%rbp), %rsi
	movq	-184(%rbp), %rdi
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
.L82:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L80:
	movl	12(%r14), %eax
	jmp	.L81
.L92:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3185:
	.size	_ZNK6icu_6718ComposeNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6718ComposeNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.section	.text._ZNK6icu_6715NoopNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode,"axG",@progbits,_ZNK6icu_6715NoopNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.type	_ZNK6icu_6715NoopNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode, @function
_ZNK6icu_6715NoopNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode:
.LFB3214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L97
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	cmpq	%rsi, %rdx
	je	.L95
	movq	%rdx, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	$1, (%rcx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3214:
	.size	_ZNK6icu_6715NoopNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode, .-_ZNK6icu_6715NoopNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.section	.text._ZNK6icu_6718ComposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode
	.type	_ZNK6icu_6718ComposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode, @function
_ZNK6icu_6718ComposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode:
.LFB3188:
	.cfi_startproc
	endbr64
	movsbl	16(%rdi), %ecx
	movq	8(%rdi), %rdi
	xorl	%r8d, %r8d
	jmp	_ZNK6icu_6715Normalizer2Impl17composeQuickCheckEPKDsS2_aP25UNormalizationCheckResult@PLT
	.cfi_endproc
.LFE3188:
	.size	_ZNK6icu_6718ComposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode, .-_ZNK6icu_6718ComposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode
	.section	.text._ZNK6icu_6718ComposeNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6718ComposeNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6718ComposeNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode:
.LFB3187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$2, %eax
	testl	%ecx, %ecx
	jg	.L99
	movswl	8(%rsi), %eax
	testb	$17, %al
	jne	.L101
	leaq	10(%rsi), %r9
	testb	$2, %al
	jne	.L103
	movq	24(%rsi), %r9
	testq	%r9, %r9
	je	.L101
.L103:
	movq	8(%rdi), %r10
	movsbl	16(%rdi), %ecx
	movl	$1, -12(%rbp)
	testw	%ax, %ax
	js	.L104
	sarl	$5, %eax
.L105:
	cltq
	leaq	-12(%rbp), %r8
	movq	%r9, %rsi
	movq	%r10, %rdi
	leaq	(%r9,%rax,2), %rdx
	call	_ZNK6icu_6715Normalizer2Impl17composeQuickCheckEPKDsS2_aP25UNormalizationCheckResult@PLT
	movl	-12(%rbp), %eax
.L99:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L109
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movl	12(%rsi), %eax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$1, (%rdx)
	movl	$2, %eax
	jmp	.L99
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3187:
	.size	_ZNK6icu_6718ComposeNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6718ComposeNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode
	.section	.text._ZNK6icu_6714FCDNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode,"axG",@progbits,_ZNK6icu_6714FCDNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714FCDNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6714FCDNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6714FCDNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode:
.LFB3197:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movsbl	%cl, %ecx
	jmp	_ZNK6icu_6715Normalizer2Impl16makeFCDAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode@PLT
	.cfi_endproc
.LFE3197:
	.size	_ZNK6icu_6714FCDNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6714FCDNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.section	.text._ZNK6icu_6718ComposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6718ComposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6718ComposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode:
.LFB3184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %r10
	movsbl	%cl, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	16(%rbp)
	pushq	%r9
	movq	%r8, %r9
	movsbl	16(%rdi), %r8d
	movq	%r10, %rdi
	call	_ZNK6icu_6715Normalizer2Impl16composeAndAppendEPKDsS2_aaRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3184:
	.size	_ZNK6icu_6718ComposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6718ComposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.section	.text._ZNK6icu_6720DecomposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode,"axG",@progbits,_ZNK6icu_6720DecomposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6720DecomposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6720DecomposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6720DecomposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode:
.LFB3173:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movsbl	%cl, %ecx
	jmp	_ZNK6icu_6715Normalizer2Impl18decomposeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode@PLT
	.cfi_endproc
.LFE3173:
	.size	_ZNK6icu_6720DecomposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6720DecomposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.section	.text._ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.type	_ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode, @function
_ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode:
.LFB3159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L145
.L121:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movswl	8(%rsi), %r14d
	movq	%rcx, %rbx
	testb	$1, %r14b
	je	.L116
.L133:
	movl	$1, (%rbx)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L116:
	movzwl	8(%rdx), %eax
	movq	%rdx, %r13
	testb	$17, %al
	jne	.L133
	movq	%rdi, %r15
	leaq	10(%r13), %r10
	testb	$2, %al
	jne	.L118
	movq	24(%r13), %r10
.L118:
	cmpq	%r13, %r12
	je	.L133
	testq	%r10, %r10
	je	.L133
	testw	%r14w, %r14w
	js	.L122
	sarl	$5, %r14d
	movl	%r14d, -196(%rbp)
.L123:
	movq	8(%r15), %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	$2, %esi
	pxor	%xmm0, %xmm0
	movq	%rcx, -128(%rbp)
	movw	%si, -120(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%r12, -184(%rbp)
	movq	$0, -160(%rbp)
	movl	$0, -152(%rbp)
	movb	$0, -148(%rbp)
	movaps	%xmm0, -176(%rbp)
	testw	%ax, %ax
	js	.L124
	movswl	%ax, %esi
	sarl	$5, %esi
.L125:
	leaq	-192(%rbp), %r9
	addl	-196(%rbp), %esi
	movq	%rbx, %rdx
	movq	%r10, -216(%rbp)
	movq	%r9, %rdi
	movq	%r9, -208(%rbp)
	leaq	-128(%rbp), %r14
	call	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L147
.L126:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L129
	movq	-160(%rbp), %rsi
	movq	-184(%rbp), %rdi
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
.L129:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L130
	movswl	-120(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L131
	sarl	$5, %r9d
.L132:
	movl	-196(%rbp), %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$2147483647, %edx
	subl	%r9d, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
.L130:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L147:
	movswl	8(%r13), %edx
	movq	(%r15), %rax
	movq	-208(%rbp), %r9
	movq	-216(%rbp), %r10
	testw	%dx, %dx
	movq	152(%rax), %rax
	js	.L127
	sarl	$5, %edx
.L128:
	subq	$8, %rsp
	leaq	-128(%rbp), %r14
	movslq	%edx, %rdx
	xorl	%ecx, %ecx
	pushq	%rbx
	leaq	(%r10,%rdx,2), %rdx
	movq	%r14, %r8
	movq	%r10, %rsi
	movq	%r15, %rdi
	call	*%rax
	popq	%rdx
	popq	%rcx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L131:
	movl	-116(%rbp), %r9d
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L124:
	movl	12(%r13), %esi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L122:
	movl	12(%r12), %ecx
	movl	%ecx, -196(%rbp)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L127:
	movl	12(%r13), %edx
	jmp	.L128
.L146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3159:
	.size	_ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode, .-_ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.section	.text._ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE
	.type	_ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE, @function
_ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE:
.LFB3162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-124(%rbp), %rcx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	movq	%r13, %rdx
	subq	$104, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6715Normalizer2Impl19getRawDecompositionEiPDsRi@PLT
	testq	%rax, %rax
	je	.L154
	movl	-124(%rbp), %r14d
	cmpq	%r13, %rax
	je	.L156
	leaq	-120(%rbp), %rdx
	movl	%r14d, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movl	$1, %eax
.L148:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L157
	addq	$104, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L151
	sarl	$5, %edx
.L152:
	movl	%r14d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	$1, %eax
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L151:
	movl	12(%r12), %edx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%eax, %eax
	jmp	.L148
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3162:
	.size	_ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE, .-_ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE
	.section	.text._ZNK6icu_6719Normalizer2WithImpl11composePairEii,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl11composePairEii,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl11composePairEii
	.type	_ZNK6icu_6719Normalizer2WithImpl11composePairEii, @function
_ZNK6icu_6719Normalizer2WithImpl11composePairEii:
.LFB3163:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZNK6icu_6715Normalizer2Impl11composePairEii@PLT
	.cfi_endproc
.LFE3163:
	.size	_ZNK6icu_6719Normalizer2WithImpl11composePairEii, .-_ZNK6icu_6719Normalizer2WithImpl11composePairEii
	.section	.text._ZNK6icu_6718ComposeNormalizer217hasBoundaryBeforeEi,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer217hasBoundaryBeforeEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer217hasBoundaryBeforeEi
	.type	_ZNK6icu_6718ComposeNormalizer217hasBoundaryBeforeEi, @function
_ZNK6icu_6718ComposeNormalizer217hasBoundaryBeforeEi:
.LFB3190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	movzwl	10(%rbx), %eax
	cmpl	%eax, %esi
	jge	.L160
.L167:
	movl	$1, %eax
.L159:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	movl	%esi, %eax
	movl	$1, %edx
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L162
	movq	32(%rbx), %rdi
	movq	8(%rdi), %r12
	cmpl	$65535, %esi
	jg	.L163
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
.L164:
	movzwl	(%r12,%rsi), %edx
.L162:
	cmpw	%dx, 22(%rbx)
	ja	.L167
	xorl	%eax, %eax
	cmpw	%dx, 26(%rbx)
	ja	.L159
	cmpw	%dx, 30(%rbx)
	seta	%al
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L163:
	cmpl	$1114111, %esi
	jg	.L165
	cmpl	24(%rdi), %esi
	jl	.L166
	movl	20(%rdi), %esi
	subl	$2, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L165:
	movl	20(%rdi), %esi
	subl	$1, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L166:
	call	ucptrie_internalSmallIndex_67@PLT
	movslq	%eax, %rsi
	addq	%rsi, %rsi
	jmp	.L164
	.cfi_endproc
.LFE3190:
	.size	_ZNK6icu_6718ComposeNormalizer217hasBoundaryBeforeEi, .-_ZNK6icu_6718ComposeNormalizer217hasBoundaryBeforeEi
	.section	.text._ZNK6icu_6720DecomposeNormalizer217hasBoundaryBeforeEi,"axG",@progbits,_ZNK6icu_6720DecomposeNormalizer217hasBoundaryBeforeEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6720DecomposeNormalizer217hasBoundaryBeforeEi
	.type	_ZNK6icu_6720DecomposeNormalizer217hasBoundaryBeforeEi, @function
_ZNK6icu_6720DecomposeNormalizer217hasBoundaryBeforeEi:
.LFB3176:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZNK6icu_6715Normalizer2Impl23hasDecompBoundaryBeforeEi@PLT
	.cfi_endproc
.LFE3176:
	.size	_ZNK6icu_6720DecomposeNormalizer217hasBoundaryBeforeEi, .-_ZNK6icu_6720DecomposeNormalizer217hasBoundaryBeforeEi
	.section	.text._ZNK6icu_6714FCDNormalizer217hasBoundaryBeforeEi,"axG",@progbits,_ZNK6icu_6714FCDNormalizer217hasBoundaryBeforeEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714FCDNormalizer217hasBoundaryBeforeEi
	.type	_ZNK6icu_6714FCDNormalizer217hasBoundaryBeforeEi, @function
_ZNK6icu_6714FCDNormalizer217hasBoundaryBeforeEi:
.LFB3199:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZNK6icu_6715Normalizer2Impl23hasDecompBoundaryBeforeEi@PLT
	.cfi_endproc
.LFE3199:
	.size	_ZNK6icu_6714FCDNormalizer217hasBoundaryBeforeEi, .-_ZNK6icu_6714FCDNormalizer217hasBoundaryBeforeEi
	.section	.text._ZNK6icu_6720DecomposeNormalizer216hasBoundaryAfterEi,"axG",@progbits,_ZNK6icu_6720DecomposeNormalizer216hasBoundaryAfterEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6720DecomposeNormalizer216hasBoundaryAfterEi
	.type	_ZNK6icu_6720DecomposeNormalizer216hasBoundaryAfterEi, @function
_ZNK6icu_6720DecomposeNormalizer216hasBoundaryAfterEi:
.LFB3177:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZNK6icu_6715Normalizer2Impl22hasDecompBoundaryAfterEi@PLT
	.cfi_endproc
.LFE3177:
	.size	_ZNK6icu_6720DecomposeNormalizer216hasBoundaryAfterEi, .-_ZNK6icu_6720DecomposeNormalizer216hasBoundaryAfterEi
	.section	.text._ZNK6icu_6714FCDNormalizer216hasBoundaryAfterEi,"axG",@progbits,_ZNK6icu_6714FCDNormalizer216hasBoundaryAfterEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714FCDNormalizer216hasBoundaryAfterEi
	.type	_ZNK6icu_6714FCDNormalizer216hasBoundaryAfterEi, @function
_ZNK6icu_6714FCDNormalizer216hasBoundaryAfterEi:
.LFB3200:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	jmp	_ZNK6icu_6715Normalizer2Impl22hasDecompBoundaryAfterEi@PLT
	.cfi_endproc
.LFE3200:
	.size	_ZNK6icu_6714FCDNormalizer216hasBoundaryAfterEi, .-_ZNK6icu_6714FCDNormalizer216hasBoundaryAfterEi
	.section	.text._ZNK6icu_6718ComposeNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.type	_ZNK6icu_6718ComposeNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode, @function
_ZNK6icu_6718ComposeNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode:
.LFB3186:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L184
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsbl	16(%rdi), %r10d
	movslq	%edx, %rdx
	xorl	%r9d, %r9d
	movq	8(%rdi), %rdi
	leaq	(%rsi,%rdx), %r8
	movl	%r10d, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rcx
	movq	%rsi, %rcx
	xorl	%esi, %esi
	pushq	$0
	call	_ZNK6icu_6715Normalizer2Impl11composeUTF8EjaPKhS2_PNS_8ByteSinkEPNS_5EditsER10UErrorCode@PLT
	popq	%rdx
	popq	%rcx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3186:
	.size	_ZNK6icu_6718ComposeNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode, .-_ZNK6icu_6718ComposeNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.section	.text._ZNK6icu_6718ComposeNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZNK6icu_6718ComposeNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZNK6icu_6718ComposeNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB3183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$32, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	16(%rbp), %r15
	movl	(%r15), %r10d
	testl	%r10d, %r10d
	jg	.L185
	movq	%rdi, %r14
	movq	%rdx, %r13
	movq	%r8, %r12
	testq	%r9, %r9
	je	.L187
	testl	$8192, %esi
	je	.L192
.L187:
	movsbl	16(%r14), %edx
	movq	8(%r14), %rdi
	pushq	%r15
	movslq	%ecx, %rcx
	pushq	%r9
	leaq	0(%r13,%rcx), %r8
	movq	%r12, %r9
	movq	%r13, %rcx
	call	_ZNK6icu_6715Normalizer2Impl11composeUTF8EjaPKhS2_PNS_8ByteSinkEPNS_5EditsER10UErrorCode@PLT
	popq	%rax
	movq	(%r12), %rax
	movq	%r12, %rdi
	popq	%rdx
	movq	32(%rax), %rax
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	leaq	-32(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	%r9, %rdi
	movq	%rcx, -56(%rbp)
	movl	%esi, -44(%rbp)
	movq	%r9, -40(%rbp)
	call	_ZN6icu_675Edits5resetEv@PLT
	movq	-56(%rbp), %rcx
	movl	-44(%rbp), %esi
	movq	-40(%rbp), %r9
	jmp	.L187
	.cfi_endproc
.LFE3183:
	.size	_ZNK6icu_6718ComposeNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZNK6icu_6718ComposeNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.section	.text._ZNK6icu_6715NoopNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode,"axG",@progbits,_ZNK6icu_6715NoopNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZNK6icu_6715NoopNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZNK6icu_6715NoopNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB3215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L202
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movl	%esi, %ebx
	movq	%rdx, %r14
	movq	%rcx, %r13
	movq	%r8, %r12
	testq	%r9, %r9
	je	.L195
	testb	$32, %bh
	je	.L203
.L196:
	movl	%r13d, %esi
	movq	%r9, %rdi
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
.L195:
	movq	(%r12), %rax
	andb	$64, %bh
	jne	.L197
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	movq	(%r12), %rax
.L197:
	movq	32(%rax), %rax
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	%r9, %rdi
	movq	%r9, -40(%rbp)
	call	_ZN6icu_675Edits5resetEv@PLT
	movq	-40(%rbp), %r9
	jmp	.L196
	.cfi_endproc
.LFE3215:
	.size	_ZNK6icu_6715NoopNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZNK6icu_6715NoopNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.section	.text._ZNK6icu_6719Normalizer2WithImpl10quickCheckERKNS_13UnicodeStringER10UErrorCode,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl10quickCheckERKNS_13UnicodeStringER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl10quickCheckERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6719Normalizer2WithImpl10quickCheckERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6719Normalizer2WithImpl10quickCheckERKNS_13UnicodeStringER10UErrorCode:
.LFB3166:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L213
	movswl	8(%rsi), %eax
	testb	$17, %al
	jne	.L206
	leaq	10(%rsi), %r8
	testb	$2, %al
	jne	.L208
	movq	24(%rsi), %r8
	testq	%r8, %r8
	je	.L206
.L208:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testw	%ax, %ax
	js	.L209
	sarl	$5, %eax
.L210:
	cltq
	movq	%rdx, %rcx
	movq	%r8, %rsi
	leaq	(%r8,%rax,2), %rbx
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*160(%rax)
	cmpq	%rax, %rbx
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	12(%rsi), %eax
	jmp	.L210
	.cfi_endproc
.LFE3166:
	.size	_ZNK6icu_6719Normalizer2WithImpl10quickCheckERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6719Normalizer2WithImpl10quickCheckERKNS_13UnicodeStringER10UErrorCode
	.section	.text._ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode:
.LFB3167:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L224
	movswl	8(%rsi), %eax
	movq	%rdx, %rcx
	testb	$17, %al
	jne	.L225
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	10(%rsi), %rbx
	subq	$8, %rsp
	testb	$2, %al
	jne	.L219
	movq	24(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L228
.L219:
	movq	(%rdi), %rdx
	movq	160(%rdx), %r8
	testw	%ax, %ax
	js	.L220
	sarl	$5, %eax
.L221:
	cltq
	movq	%rbx, %rsi
	leaq	(%rbx,%rax,2), %rdx
	call	*%r8
	addq	$8, %rsp
	subq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sarq	%rax
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movl	$1, (%rdx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, (%rdx)
	xorl	%eax, %eax
.L224:
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	12(%rsi), %eax
	jmp	.L221
	.cfi_endproc
.LFE3167:
	.size	_ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.section	.text._ZNK6icu_6715NoopNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode,"axG",@progbits,_ZNK6icu_6715NoopNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.type	_ZNK6icu_6715NoopNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode, @function
_ZNK6icu_6715NoopNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode:
.LFB3217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L235
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movq	%rdx, %rsi
	cmpq	%rdx, %r12
	je	.L231
	movswl	8(%rdx), %ecx
	testw	%cx, %cx
	js	.L232
	sarl	$5, %ecx
.L233:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movl	12(%rdx), %ecx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$1, (%rcx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3217:
	.size	_ZNK6icu_6715NoopNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode, .-_ZNK6icu_6715NoopNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.text
	.p2align 4
	.type	_ZL24normalizeSecondAndAppendPK12UNormalizer2PDsiiPKDsiaP10UErrorCode.part.0, @function
_ZL24normalizeSecondAndAppendPK12UNormalizer2PDsiiPKDsiaP10UErrorCode.part.0:
.LFB4151:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$248, %rsp
	movl	16(%rbp), %eax
	movq	%rdi, -272(%rbp)
	movq	%r13, %rdi
	movq	%r8, -288(%rbp)
	movq	24(%rbp), %r15
	movl	%r9d, -264(%rbp)
	movl	%eax, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movswl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L237
	sarl	$5, %eax
	movl	%eax, -276(%rbp)
.L238:
	movl	-264(%rbp), %r8d
	leaq	-256(%rbp), %r14
	testl	%r8d, %r8d
	je	.L239
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L240
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6719Normalizer2WithImplE(%rip), %rdx
	leaq	_ZTIN6icu_6711Normalizer2E(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L240
	movq	8(%rax), %rax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	$2, %edi
	movq	%r15, %rdx
	movq	%rcx, -128(%rbp)
	movl	-276(%rbp), %ecx
	movq	%rax, -256(%rbp)
	movl	-264(%rbp), %eax
	movw	%di, -120(%rbp)
	movq	%r14, %rdi
	leal	1(%rcx,%rax), %esi
	movq	%r10, -272(%rbp)
	movq	%r13, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movl	$0, -216(%rbp)
	movb	$0, -212(%rbp)
	call	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode@PLT
	leaq	-128(%rbp), %r11
	testb	%al, %al
	je	.L241
	movq	-272(%rbp), %r10
	movl	-264(%rbp), %edi
	xorl	%edx, %edx
	movsbl	-280(%rbp), %ecx
	movq	(%r10), %rax
	movq	152(%rax), %rax
	testl	%edi, %edi
	js	.L242
	movq	-288(%rbp), %rdx
	movslq	%edi, %r9
	leaq	(%rdx,%r9,2), %rdx
.L242:
	leaq	-128(%rbp), %r11
	subq	$8, %rsp
	movq	%r14, %r9
	movq	%r10, %rdi
	pushq	%r15
	movq	-288(%rbp), %rsi
	movq	%r11, %r8
	movq	%r11, -264(%rbp)
	call	*%rax
	movq	-264(%rbp), %r11
	popq	%rcx
	popq	%rsi
.L241:
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L243
	movq	-224(%rbp), %rsi
	movq	-248(%rbp), %rdi
	movq	%r11, -264(%rbp)
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	-264(%rbp), %r11
.L243:
	movl	(%r15), %edx
	testl	%edx, %edx
	jle	.L274
.L244:
	testq	%rbx, %rbx
	je	.L252
	movswl	-120(%rbp), %eax
	movslq	-276(%rbp), %r9
	testw	%ax, %ax
	js	.L249
	sarl	$5, %eax
.L250:
	movq	%r9, %rdx
	cltq
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	subq	%rax, %rdx
	movq	%r11, %rdi
	movq	%r9, -288(%rbp)
	leaq	(%rbx,%rdx,2), %rax
	movl	$2147483647, %edx
	movq	%r11, -264(%rbp)
	movq	%rax, %rcx
	movq	%rax, -272(%rbp)
	call	_ZNK6icu_6713UnicodeString9doExtractEiiPDsi@PLT
	movq	-272(%rbp), %rax
	cmpl	-276(%rbp), %r12d
	movq	-264(%rbp), %r11
	jle	.L252
	movq	-288(%rbp), %r9
	xorl	%eax, %eax
	movw	%ax, (%rbx,%r9,2)
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L240:
	movl	-264(%rbp), %ecx
	leaq	-128(%rbp), %r11
	movq	-288(%rbp), %rax
	leaq	-256(%rbp), %r14
	movq	%r14, %rdx
	movq	%r11, %rdi
	movq	%r11, -264(%rbp)
	movl	%ecx, %esi
	movq	%rax, -256(%rbp)
	shrl	$31, %esi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-272(%rbp), %rdi
	movq	-264(%rbp), %r11
	movq	%r15, %rcx
	cmpb	$0, -280(%rbp)
	movq	(%rdi), %rax
	movq	%r11, %rdx
	jne	.L275
	movq	%r11, -264(%rbp)
	movq	%r13, %rsi
	call	*48(%rax)
	movq	-264(%rbp), %r11
.L252:
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L239:
	movl	%r12d, %edx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rbx, -256(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r12d
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movl	-180(%rbp), %eax
	movl	%eax, -276(%rbp)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%r13, %rsi
	call	*40(%rax)
	movq	-264(%rbp), %r11
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L274:
	movswl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L245
	sarl	$5, %eax
.L246:
	cmpl	%eax, %r12d
	jge	.L252
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L249:
	movl	-116(%rbp), %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L245:
	movl	-180(%rbp), %eax
	jmp	.L246
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4151:
	.size	_ZL24normalizeSecondAndAppendPK12UNormalizer2PDsiiPKDsiaP10UErrorCode.part.0, .-_ZL24normalizeSecondAndAppendPK12UNormalizer2PDsiiPKDsiaP10UErrorCode.part.0
	.section	.text._ZNK6icu_6715NoopNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode,"axG",@progbits,_ZNK6icu_6715NoopNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715NoopNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.type	_ZNK6icu_6715NoopNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode, @function
_ZNK6icu_6715NoopNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode:
.LFB3216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L283
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movq	%rdx, %rsi
	cmpq	%rdx, %r12
	je	.L279
	movswl	8(%rdx), %ecx
	testw	%cx, %cx
	js	.L280
	sarl	$5, %ecx
.L281:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movl	12(%rdx), %ecx
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L279:
	movl	$1, (%rcx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3216:
	.size	_ZNK6icu_6715NoopNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode, .-_ZNK6icu_6715NoopNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.section	.text._ZNK6icu_6714FCDNormalizer27isInertEi,"axG",@progbits,_ZNK6icu_6714FCDNormalizer27isInertEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6714FCDNormalizer27isInertEi
	.type	_ZNK6icu_6714FCDNormalizer27isInertEi, @function
_ZNK6icu_6714FCDNormalizer27isInertEi:
.LFB3201:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	$1, %eax
	movzwl	8(%rdi), %edx
	cmpl	%edx, %esi
	jl	.L293
	cmpl	$65535, %esi
	jg	.L286
	movl	%esi, %edx
	movq	56(%rdi), %rcx
	sarl	$8, %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	testb	%dl, %dl
	je	.L293
	movl	%esi, %ecx
	shrb	$5, %cl
	btl	%ecx, %edx
	jnc	.L293
.L286:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpw	$1, %ax
	setbe	%al
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE3201:
	.size	_ZNK6icu_6714FCDNormalizer27isInertEi, .-_ZNK6icu_6714FCDNormalizer27isInertEi
	.section	.text._ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE
	.type	_ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE, @function
_ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE:
.LFB3161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r13
	leaq	-60(%rbp), %rcx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	movq	%r13, %rdx
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi@PLT
	testq	%rax, %rax
	je	.L301
	movl	-60(%rbp), %r14d
	cmpq	%r13, %rax
	je	.L303
	leaq	-56(%rbp), %rdx
	movl	%r14d, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movl	$1, %eax
.L295:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L304
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	8(%r12), %edx
	testw	%dx, %dx
	js	.L298
	sarl	$5, %edx
.L299:
	movl	%r14d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	movl	$1, %eax
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L298:
	movl	12(%r12), %edx
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L301:
	xorl	%eax, %eax
	jmp	.L295
.L304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3161:
	.size	_ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE, .-_ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE
	.section	.text._ZNK6icu_6719Normalizer2WithImpl12isNormalizedERKNS_13UnicodeStringER10UErrorCode,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl12isNormalizedERKNS_13UnicodeStringER10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl12isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6719Normalizer2WithImpl12isNormalizedERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6719Normalizer2WithImpl12isNormalizedERKNS_13UnicodeStringER10UErrorCode:
.LFB3165:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L314
	movswl	8(%rsi), %eax
	testb	$17, %al
	jne	.L307
	leaq	10(%rsi), %r8
	testb	$2, %al
	jne	.L309
	movq	24(%rsi), %r8
	testq	%r8, %r8
	je	.L307
.L309:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testw	%ax, %ax
	js	.L310
	sarl	$5, %eax
.L311:
	cltq
	movq	%rdx, %rcx
	movq	%r8, %rsi
	leaq	(%r8,%rax,2), %rbx
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	call	*160(%rax)
	cmpq	%rax, %rbx
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movl	12(%rsi), %eax
	jmp	.L311
	.cfi_endproc
.LFE3165:
	.size	_ZNK6icu_6719Normalizer2WithImpl12isNormalizedERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6719Normalizer2WithImpl12isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.text
	.p2align 4
	.type	uprv_normalizer2_cleanup, @function
uprv_normalizer2_cleanup:
.LFB3264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN6icu_67L13noopSingletonE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L317
	movq	(%rdi), %rax
	call	*8(%rax)
.L317:
	movq	$0, _ZN6icu_67L13noopSingletonE(%rip)
	movl	$0, _ZN6icu_67L12noopInitOnceE(%rip)
	mfence
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %r12
	testq	%r12, %r12
	je	.L318
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L319
	movq	(%rdi), %rax
	call	*8(%rax)
.L319:
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rbx
	leaq	64(%r12), %rdi
	movq	%rbx, 64(%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%rbx, 48(%r12)
	leaq	48(%r12), %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%rbx, 32(%r12)
	leaq	32(%r12), %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%rbx, 8(%r12)
	leaq	8(%r12), %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L318:
	movq	$0, _ZN6icu_67L12nfcSingletonE(%rip)
	movl	$1, %eax
	movl	$0, _ZN6icu_67L11nfcInitOnceE(%rip)
	mfence
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3264:
	.size	uprv_normalizer2_cleanup, .-uprv_normalizer2_cleanup
	.align 2
	.p2align 4
	.type	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0, @function
_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0:
.LFB4157:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	$80, %edi
	subq	$16, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L331
	movq	%rax, %r12
	leaq	16+_ZTVN6icu_6715Normalizer2ImplE(%rip), %rax
	leaq	_ZL23norm2_nfc_data_smallFCD(%rip), %r8
	movq	%rax, (%r12)
	leaq	_ZL24norm2_nfc_data_extraData(%rip), %rcx
	movq	%r12, %rdi
	leaq	_ZL19norm2_nfc_data_trie(%rip), %rdx
	movq	$0, 32(%r12)
	leaq	_ZL22norm2_nfc_data_indexes(%rip), %rsi
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	call	_ZN6icu_6715Normalizer2Impl4initEPKiPK7UCPTriePKtPKh@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L336
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L334
	leaq	16+_ZTVN6icu_6718ComposeNormalizer2E(%rip), %rdx
	leaq	16+_ZTVN6icu_6720DecomposeNormalizer2E(%rip), %rcx
	movq	%r12, (%rax)
	leaq	16+_ZTVN6icu_6714FCDNormalizer2E(%rip), %rsi
	movq	%r12, 16(%rax)
	movq	%rdx, 8(%rax)
	movb	$0, 24(%rax)
	movq	%r12, 40(%rax)
	movq	%rcx, 32(%rax)
	movq	%r12, 56(%rax)
	movq	%rsi, 48(%rax)
	movq	%r12, 72(%rax)
	movq	%rdx, 64(%rax)
	movb	$1, 80(%rax)
.L330:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L331:
	.cfi_restore_state
	movl	$7, (%rbx)
	xorl	%eax, %eax
	jmp	.L330
.L334:
	movq	(%r12), %rdx
	movq	%rax, -24(%rbp)
	movq	%r12, %rdi
	movl	$7, (%rbx)
	call	*8(%rdx)
	movq	-24(%rbp), %rax
	jmp	.L330
	.cfi_endproc
.LFE4157:
	.size	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0, .-_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
	.section	.text._ZNK6icu_6720DecomposeNormalizer27isInertEi,"axG",@progbits,_ZNK6icu_6720DecomposeNormalizer27isInertEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6720DecomposeNormalizer27isInertEi
	.type	_ZNK6icu_6720DecomposeNormalizer27isInertEi, @function
_ZNK6icu_6720DecomposeNormalizer27isInertEi:
.LFB3178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$-1024, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	cmpl	$55296, %eax
	je	.L347
	movq	32(%rbx), %rdi
	movq	8(%rdi), %r12
	cmpl	$65535, %esi
	ja	.L341
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
.L342:
	movzwl	(%r12,%rsi), %edx
	cmpw	%dx, 14(%rbx)
	ja	.L345
	cmpw	$-512, %dx
	jne	.L348
.L345:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L341:
	.cfi_restore_state
	cmpl	$1114111, %esi
	ja	.L343
	cmpl	24(%rdi), %esi
	jl	.L344
	movl	20(%rdi), %esi
	subl	$2, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L347:
	cmpw	$1, 14(%rbx)
	ja	.L345
	cmpw	$1, 30(%rbx)
	popq	%rbx
	setbe	%al
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	movl	20(%rdi), %esi
	subl	$1, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L348:
	cmpw	%dx, 30(%rbx)
	popq	%rbx
	setbe	%al
	cmpw	$-1024, %dx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	setbe	%dl
	andl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	call	ucptrie_internalSmallIndex_67@PLT
	movslq	%eax, %rsi
	addq	%rsi, %rsi
	jmp	.L342
	.cfi_endproc
.LFE3178:
	.size	_ZNK6icu_6720DecomposeNormalizer27isInertEi, .-_ZNK6icu_6720DecomposeNormalizer27isInertEi
	.section	.text._ZNK6icu_6718ComposeNormalizer216hasBoundaryAfterEi,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer216hasBoundaryAfterEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer216hasBoundaryAfterEi
	.type	_ZNK6icu_6718ComposeNormalizer216hasBoundaryAfterEi, @function
_ZNK6icu_6718ComposeNormalizer216hasBoundaryAfterEi:
.LFB3191:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L363
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movzbl	16(%rdi), %r13d
	movq	32(%rbx), %rdi
	movq	8(%rdi), %r12
	cmpl	$65535, %esi
	ja	.L352
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
.L353:
	movzwl	(%r12,%rsi), %edx
	xorl	%eax, %eax
	testb	$1, %dl
	je	.L349
	cmpw	$1, %dx
	je	.L356
	testb	%r13b, %r13b
	je	.L356
	cmpw	%dx, 26(%rbx)
	ja	.L357
	andl	$6, %edx
	cmpw	$2, %dx
	setbe	%al
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L352:
	cmpl	$1114111, %esi
	ja	.L354
	cmpl	24(%rdi), %esi
	jl	.L355
	movl	20(%rdi), %esi
	subl	$2, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L356:
	movl	$1, %eax
.L349:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movl	20(%rdi), %esi
	subl	$1, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	48(%rbx), %rax
	andl	$65534, %edx
	cmpw	$511, (%rdx,%rax)
	setbe	%al
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L355:
	call	ucptrie_internalSmallIndex_67@PLT
	movslq	%eax, %rsi
	addq	%rsi, %rsi
	jmp	.L353
	.cfi_endproc
.LFE3191:
	.size	_ZNK6icu_6718ComposeNormalizer216hasBoundaryAfterEi, .-_ZNK6icu_6718ComposeNormalizer216hasBoundaryAfterEi
	.section	.text._ZNK6icu_6718ComposeNormalizer27isInertEi,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer27isInertEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer27isInertEi
	.type	_ZNK6icu_6718ComposeNormalizer27isInertEi, @function
_ZNK6icu_6718ComposeNormalizer27isInertEi:
.LFB3192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$-1024, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	cmpl	$55296, %eax
	je	.L365
	movzbl	16(%rdi), %r13d
	movq	32(%rbx), %rdi
	movq	8(%rdi), %r12
	cmpl	$65535, %esi
	ja	.L366
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
.L367:
	movzwl	(%r12,%rsi), %edx
	xorl	%eax, %eax
	cmpw	%dx, 18(%rbx)
	jbe	.L364
	testb	$1, %dl
	jne	.L376
.L364:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	cmpl	$1114111, %esi
	ja	.L368
	cmpl	24(%rdi), %esi
	jl	.L369
	movl	20(%rdi), %esi
	subl	$2, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L365:
	cmpw	$1, 18(%rbx)
	seta	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movl	20(%rdi), %esi
	subl	$1, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L376:
	testb	%r13b, %r13b
	je	.L373
	cmpw	$1, %dx
	je	.L373
	movq	48(%rbx), %rax
	andl	$65534, %edx
	cmpw	$511, (%rdx,%rax)
	setbe	%al
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L369:
	call	ucptrie_internalSmallIndex_67@PLT
	movslq	%eax, %rsi
	addq	%rsi, %rsi
	jmp	.L367
.L373:
	movl	$1, %eax
	jmp	.L364
	.cfi_endproc
.LFE3192:
	.size	_ZNK6icu_6718ComposeNormalizer27isInertEi, .-_ZNK6icu_6718ComposeNormalizer27isInertEi
	.section	.text._ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi
	.type	_ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi, @function
_ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi:
.LFB3164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$-1024, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	cmpl	$55296, %eax
	je	.L384
	movq	32(%rbx), %rdi
	movq	8(%rdi), %r12
	cmpl	$65535, %esi
	ja	.L379
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
.L380:
	movzwl	(%r12,%rsi), %eax
	movl	%eax, %edx
	cmpw	$-1025, %ax
	jbe	.L378
.L390:
	sarl	%eax
	movl	%eax, %r8d
.L377:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	.cfi_restore_state
	cmpl	$1114111, %esi
	ja	.L381
	cmpl	24(%rdi), %esi
	jl	.L382
	movl	20(%rdi), %esi
	subl	$2, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	movzwl	(%r12,%rsi), %eax
	movl	%eax, %edx
	cmpw	$-1025, %ax
	ja	.L390
	.p2align 4,,10
	.p2align 3
.L378:
	xorl	%r8d, %r8d
	cmpw	%dx, 18(%rbx)
	ja	.L377
	cmpw	%dx, 26(%rbx)
	jbe	.L377
	sarl	%eax
	movslq	%eax, %rdx
	movq	48(%rbx), %rax
	leaq	(%rax,%rdx,2), %rax
	testb	$-128, (%rax)
	je	.L377
	movzbl	-2(%rax), %r8d
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	movl	$1, %edx
	movl	$1, %eax
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L381:
	movl	20(%rdi), %esi
	subl	$1, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L382:
	call	ucptrie_internalSmallIndex_67@PLT
	movslq	%eax, %rsi
	addq	%rsi, %rsi
	jmp	.L380
	.cfi_endproc
.LFE3164:
	.size	_ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi, .-_ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi
	.section	.text._ZNK6icu_6720DecomposeNormalizer213getQuickCheckEi,"axG",@progbits,_ZNK6icu_6720DecomposeNormalizer213getQuickCheckEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6720DecomposeNormalizer213getQuickCheckEi
	.type	_ZNK6icu_6720DecomposeNormalizer213getQuickCheckEi, @function
_ZNK6icu_6720DecomposeNormalizer213getQuickCheckEi:
.LFB3175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movl	$1, %eax
	andl	$-1024, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	cmpl	$55296, %edx
	je	.L392
	movq	32(%rbx), %rdi
	movq	8(%rdi), %r12
	cmpl	$65535, %esi
	ja	.L393
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
.L394:
	movzwl	(%r12,%rsi), %eax
.L392:
	movl	$1, %r8d
	cmpw	%ax, 14(%rbx)
	ja	.L391
	xorl	%r8d, %r8d
	cmpw	%ax, 30(%rbx)
	setbe	%r8b
.L391:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	cmpl	$1114111, %esi
	ja	.L395
	cmpl	24(%rdi), %esi
	jl	.L396
	movl	20(%rdi), %esi
	subl	$2, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L395:
	movl	20(%rdi), %esi
	subl	$1, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L396:
	call	ucptrie_internalSmallIndex_67@PLT
	movslq	%eax, %rsi
	addq	%rsi, %rsi
	jmp	.L394
	.cfi_endproc
.LFE3175:
	.size	_ZNK6icu_6720DecomposeNormalizer213getQuickCheckEi, .-_ZNK6icu_6720DecomposeNormalizer213getQuickCheckEi
	.section	.text._ZNK6icu_6718ComposeNormalizer213getQuickCheckEi,"axG",@progbits,_ZNK6icu_6718ComposeNormalizer213getQuickCheckEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6718ComposeNormalizer213getQuickCheckEi
	.type	_ZNK6icu_6718ComposeNormalizer213getQuickCheckEi, @function
_ZNK6icu_6718ComposeNormalizer213getQuickCheckEi:
.LFB3189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$-1024, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	cmpl	$55296, %eax
	je	.L415
	movq	32(%rbx), %rdi
	movq	8(%rdi), %r12
	cmpl	$65535, %esi
	ja	.L407
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%eax, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
.L408:
	movzwl	(%r12,%rsi), %eax
	cmpw	%ax, 18(%rbx)
	ja	.L411
	cmpw	$-511, %ax
	jbe	.L405
.L411:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	cmpl	$1114111, %esi
	ja	.L409
	cmpl	24(%rdi), %esi
	jl	.L410
	movl	20(%rdi), %esi
	subl	$2, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L415:
	cmpw	$1, 18(%rbx)
	movl	$1, %eax
	ja	.L411
.L405:
	cmpw	%ax, 30(%rbx)
	popq	%rbx
	setbe	%al
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	addl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	movl	20(%rdi), %esi
	subl	$1, %esi
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L410:
	call	ucptrie_internalSmallIndex_67@PLT
	movslq	%eax, %rsi
	addq	%rsi, %rsi
	jmp	.L408
	.cfi_endproc
.LFE3189:
	.size	_ZNK6icu_6718ComposeNormalizer213getQuickCheckEi, .-_ZNK6icu_6718ComposeNormalizer213getQuickCheckEi
	.section	.text._ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode,"axG",@progbits,_ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.type	_ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode, @function
_ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode:
.LFB3158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L447
.L423:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L448
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	movswl	8(%rsi), %r14d
	movq	%rcx, %rbx
	testb	$1, %r14b
	je	.L418
.L435:
	movl	$1, (%rbx)
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L418:
	movzwl	8(%rdx), %eax
	movq	%rdx, %r13
	testb	$17, %al
	jne	.L435
	movq	%rdi, %r15
	leaq	10(%r13), %r10
	testb	$2, %al
	jne	.L420
	movq	24(%r13), %r10
.L420:
	cmpq	%r13, %r12
	je	.L435
	testq	%r10, %r10
	je	.L435
	testw	%r14w, %r14w
	js	.L424
	sarl	$5, %r14d
	movl	%r14d, -196(%rbp)
.L425:
	movq	8(%r15), %rdx
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	$2, %esi
	pxor	%xmm0, %xmm0
	movq	%rcx, -128(%rbp)
	movw	%si, -120(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%r12, -184(%rbp)
	movq	$0, -160(%rbp)
	movl	$0, -152(%rbp)
	movb	$0, -148(%rbp)
	movaps	%xmm0, -176(%rbp)
	testw	%ax, %ax
	js	.L426
	movswl	%ax, %esi
	sarl	$5, %esi
.L427:
	leaq	-192(%rbp), %r9
	addl	-196(%rbp), %esi
	movq	%rbx, %rdx
	movq	%r10, -216(%rbp)
	movq	%r9, %rdi
	movq	%r9, -208(%rbp)
	leaq	-128(%rbp), %r14
	call	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode@PLT
	testb	%al, %al
	jne	.L449
.L428:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L431
	movq	-160(%rbp), %rsi
	movq	-184(%rbp), %rdi
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
.L431:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L432
	movswl	-120(%rbp), %r9d
	testw	%r9w, %r9w
	js	.L433
	sarl	$5, %r9d
.L434:
	movl	-196(%rbp), %esi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$2147483647, %edx
	subl	%r9d, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
.L432:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L449:
	movswl	8(%r13), %edx
	movq	(%r15), %rax
	movq	-208(%rbp), %r9
	movq	-216(%rbp), %r10
	testw	%dx, %dx
	movq	152(%rax), %rax
	js	.L429
	sarl	$5, %edx
.L430:
	subq	$8, %rsp
	leaq	-128(%rbp), %r14
	movslq	%edx, %rdx
	movl	$1, %ecx
	pushq	%rbx
	leaq	(%r10,%rdx,2), %rdx
	movq	%r14, %r8
	movq	%r10, %rsi
	movq	%r15, %rdi
	call	*%rax
	popq	%rdx
	popq	%rcx
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L433:
	movl	-116(%rbp), %r9d
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L426:
	movl	12(%r13), %esi
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L424:
	movl	12(%r12), %ecx
	movl	%ecx, -196(%rbp)
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L429:
	movl	12(%r13), %edx
	jmp	.L430
.L448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3158:
	.size	_ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode, .-_ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Normalizer2D2Ev
	.type	_ZN6icu_6711Normalizer2D2Ev, @function
_ZN6icu_6711Normalizer2D2Ev:
.LFB3206:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3206:
	.size	_ZN6icu_6711Normalizer2D2Ev, .-_ZN6icu_6711Normalizer2D2Ev
	.globl	_ZN6icu_6711Normalizer2D1Ev
	.set	_ZN6icu_6711Normalizer2D1Ev,_ZN6icu_6711Normalizer2D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Normalizer2D0Ev
	.type	_ZN6icu_6711Normalizer2D0Ev, @function
_ZN6icu_6711Normalizer2D0Ev:
.LFB3208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3208:
	.size	_ZN6icu_6711Normalizer2D0Ev, .-_ZN6icu_6711Normalizer2D0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719Normalizer2WithImplD2Ev
	.type	_ZN6icu_6719Normalizer2WithImplD2Ev, @function
_ZN6icu_6719Normalizer2WithImplD2Ev:
.LFB3231:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3231:
	.size	_ZN6icu_6719Normalizer2WithImplD2Ev, .-_ZN6icu_6719Normalizer2WithImplD2Ev
	.globl	_ZN6icu_6719Normalizer2WithImplD1Ev
	.set	_ZN6icu_6719Normalizer2WithImplD1Ev,_ZN6icu_6719Normalizer2WithImplD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719Normalizer2WithImplD0Ev
	.type	_ZN6icu_6719Normalizer2WithImplD0Ev, @function
_ZN6icu_6719Normalizer2WithImplD0Ev:
.LFB3233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3233:
	.size	_ZN6icu_6719Normalizer2WithImplD0Ev, .-_ZN6icu_6719Normalizer2WithImplD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode
	.type	_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode, @function
_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode:
.LFB3250:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	testl	%eax, %eax
	jg	.L469
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L12noopInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L474
.L458:
	movl	4+_ZN6icu_67L12noopInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L461
	movl	%eax, (%rbx)
.L461:
	movq	_ZN6icu_67L13noopSingletonE(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L12noopInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L458
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L475
.L459:
	leaq	_ZN6icu_67L12noopInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L12noopInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L475:
	movl	$8, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L460
	leaq	16+_ZTVN6icu_6715NoopNormalizer2E(%rip), %rdx
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L13noopSingletonE(%rip)
	movq	%rdx, (%rax)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	jmp	.L459
.L460:
	movq	$0, _ZN6icu_67L13noopSingletonE(%rip)
	movl	$7, %eax
	movl	$7, (%rbx)
	jmp	.L459
	.cfi_endproc
.LFE3250:
	.size	_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode, .-_ZN6icu_6718Normalizer2Factory15getNoopInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718Normalizer2Factory7getImplEPKNS_11Normalizer2E
	.type	_ZN6icu_6718Normalizer2Factory7getImplEPKNS_11Normalizer2E, @function
_ZN6icu_6718Normalizer2Factory7getImplEPKNS_11Normalizer2E:
.LFB3251:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE3251:
	.size	_ZN6icu_6718Normalizer2Factory7getImplEPKNS_11Normalizer2E, .-_ZN6icu_6718Normalizer2Factory7getImplEPKNS_11Normalizer2E
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713Norm2AllModesD2Ev
	.type	_ZN6icu_6713Norm2AllModesD2Ev, @function
_ZN6icu_6713Norm2AllModesD2Ev:
.LFB3253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L478
	movq	(%rdi), %rax
	call	*8(%rax)
.L478:
	leaq	16+_ZTVN6icu_6711Normalizer2E(%rip), %r12
	leaq	64(%rbx), %rdi
	movq	%r12, 64(%rbx)
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, 48(%rbx)
	leaq	48(%rbx), %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, 32(%rbx)
	leaq	32(%rbx), %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, 8(%rbx)
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3253:
	.size	_ZN6icu_6713Norm2AllModesD2Ev, .-_ZN6icu_6713Norm2AllModesD2Ev
	.globl	_ZN6icu_6713Norm2AllModesD1Ev
	.set	_ZN6icu_6713Norm2AllModesD1Ev,_ZN6icu_6713Norm2AllModesD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode
	.type	_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode, @function
_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode:
.LFB3255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L492
	movl	$88, %edi
	movq	%rsi, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L487
	leaq	16+_ZTVN6icu_6720DecomposeNormalizer2E(%rip), %rcx
	leaq	16+_ZTVN6icu_6718ComposeNormalizer2E(%rip), %rdx
	movq	%r12, (%rax)
	movq	%rcx, 32(%rax)
	leaq	16+_ZTVN6icu_6714FCDNormalizer2E(%rip), %rcx
	movq	%r12, 16(%rax)
	movq	%rdx, 8(%rax)
	movb	$0, 24(%rax)
	movq	%r12, 40(%rax)
	movq	%r12, 56(%rax)
	movq	%rcx, 48(%rax)
	movq	%r12, 72(%rax)
	movq	%rdx, 64(%rax)
	movb	$1, 80(%rax)
.L483:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L491
	movq	(%rdi), %rax
	call	*8(%rax)
.L491:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L487:
	.cfi_restore_state
	movl	$7, (%rbx)
	testq	%r12, %r12
	je	.L491
	movq	(%r12), %rdx
	movq	%rax, -24(%rbp)
	movq	%r12, %rdi
	call	*8(%rdx)
	movq	-24(%rbp), %rax
	jmp	.L483
	.cfi_endproc
.LFE3255:
	.size	_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode, .-_ZN6icu_6713Norm2AllModes14createInstanceEPNS_15Normalizer2ImplER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode
	.type	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode, @function
_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode:
.LFB3256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	(%rdi), %edx
	testl	%edx, %edx
	jg	.L493
	movq	%rdi, %rbx
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L495
	leaq	16+_ZTVN6icu_6715Normalizer2ImplE(%rip), %rax
	leaq	_ZL23norm2_nfc_data_smallFCD(%rip), %r8
	movq	$0, 32(%r12)
	movq	%r12, %rdi
	movq	%rax, (%r12)
	leaq	_ZL24norm2_nfc_data_extraData(%rip), %rcx
	leaq	_ZL19norm2_nfc_data_trie(%rip), %rdx
	movq	$0, 64(%r12)
	leaq	_ZL22norm2_nfc_data_indexes(%rip), %rsi
	movq	$0, 72(%r12)
	call	_ZN6icu_6715Normalizer2Impl4initEPKiPK7UCPTriePKtPKh@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L500
	movl	$88, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L497
	movq	%r12, (%rax)
	leaq	16+_ZTVN6icu_6720DecomposeNormalizer2E(%rip), %rdx
	leaq	16+_ZTVN6icu_6714FCDNormalizer2E(%rip), %rcx
	movq	%r12, 16(%rax)
	leaq	16+_ZTVN6icu_6718ComposeNormalizer2E(%rip), %rax
	movq	%rax, 8(%r13)
	movb	$0, 24(%r13)
	movq	%r12, 40(%r13)
	movq	%rdx, 32(%r13)
	movq	%r12, 56(%r13)
	movq	%rcx, 48(%r13)
	movq	%r12, 72(%r13)
	movq	%rax, 64(%r13)
	movb	$1, 80(%r13)
.L493:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L497:
	.cfi_restore_state
	movl	$7, (%rbx)
.L500:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L495:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L493
	.cfi_endproc
.LFE3256:
	.size	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode, .-_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713Norm2AllModes14getNFCInstanceER10UErrorCode
	.type	_ZN6icu_6713Norm2AllModes14getNFCInstanceER10UErrorCode, @function
_ZN6icu_6713Norm2AllModes14getNFCInstanceER10UErrorCode:
.LFB3258:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L514
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L519
.L503:
	movl	4+_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L505
	movl	%eax, (%rbx)
.L505:
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L519:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L503
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L504
	movq	%rbx, %rdi
	call	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
.L504:
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L12nfcSingletonE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L11nfcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L505
	.cfi_endproc
.LFE3258:
	.size	_ZN6icu_6713Norm2AllModes14getNFCInstanceER10UErrorCode, .-_ZN6icu_6713Norm2AllModes14getNFCInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Normalizer214getNFCInstanceER10UErrorCode
	.type	_ZN6icu_6711Normalizer214getNFCInstanceER10UErrorCode, @function
_ZN6icu_6711Normalizer214getNFCInstanceER10UErrorCode:
.LFB3259:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L540
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L541
.L523:
	movl	4+_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L525
	movl	%eax, (%rbx)
.L525:
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L526
	addq	$8, %rsp
	addq	$8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L541:
	.cfi_restore_state
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L523
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L524
	movq	%rbx, %rdi
	call	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
.L524:
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L12nfcSingletonE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L11nfcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L526:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3259:
	.size	_ZN6icu_6711Normalizer214getNFCInstanceER10UErrorCode, .-_ZN6icu_6711Normalizer214getNFCInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode
	.type	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode, @function
_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode:
.LFB3260:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L562
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L563
.L545:
	movl	4+_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L547
	movl	%eax, (%rbx)
.L547:
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L548
	addq	$8, %rsp
	addq	$32, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	.cfi_restore_state
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L545
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L546
	movq	%rbx, %rdi
	call	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
.L546:
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L12nfcSingletonE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L11nfcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L548:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3260:
	.size	_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode, .-_ZN6icu_6711Normalizer214getNFDInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718Normalizer2Factory14getFCDInstanceER10UErrorCode
	.type	_ZN6icu_6718Normalizer2Factory14getFCDInstanceER10UErrorCode, @function
_ZN6icu_6718Normalizer2Factory14getFCDInstanceER10UErrorCode:
.LFB3261:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L584
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L585
.L567:
	movl	4+_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L569
	movl	%eax, (%rbx)
.L569:
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L570
	addq	$8, %rsp
	addq	$48, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L567
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L568
	movq	%rbx, %rdi
	call	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
.L568:
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L12nfcSingletonE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L11nfcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L570:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3261:
	.size	_ZN6icu_6718Normalizer2Factory14getFCDInstanceER10UErrorCode, .-_ZN6icu_6718Normalizer2Factory14getFCDInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718Normalizer2Factory14getFCCInstanceER10UErrorCode
	.type	_ZN6icu_6718Normalizer2Factory14getFCCInstanceER10UErrorCode, @function
_ZN6icu_6718Normalizer2Factory14getFCCInstanceER10UErrorCode:
.LFB3262:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L606
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L607
.L589:
	movl	4+_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L591
	movl	%eax, (%rbx)
.L591:
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L592
	addq	$8, %rsp
	addq	$64, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L589
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L590
	movq	%rbx, %rdi
	call	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
.L590:
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L12nfcSingletonE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L11nfcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L592:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3262:
	.size	_ZN6icu_6718Normalizer2Factory14getFCCInstanceER10UErrorCode, .-_ZN6icu_6718Normalizer2Factory14getFCCInstanceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode
	.type	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode, @function
_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode:
.LFB3263:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L628
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L629
.L611:
	movl	4+_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L613
	movl	%eax, (%rbx)
.L613:
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L614
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L629:
	.cfi_restore_state
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L611
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L612
	movq	%rbx, %rdi
	call	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
.L612:
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L12nfcSingletonE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L11nfcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L614:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3263:
	.size	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode, .-_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode
	.p2align 4
	.globl	unorm2_getNFCInstance_67
	.type	unorm2_getNFCInstance_67, @function
unorm2_getNFCInstance_67:
.LFB3265:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L650
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L651
.L633:
	movl	4+_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L635
	movl	%eax, (%rbx)
.L635:
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L636
	addq	$8, %rsp
	addq	$8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L633
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L634
	movq	%rbx, %rdi
	call	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
.L634:
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L12nfcSingletonE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L11nfcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L636:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3265:
	.size	unorm2_getNFCInstance_67, .-unorm2_getNFCInstance_67
	.p2align 4
	.globl	unorm2_getNFDInstance_67
	.type	unorm2_getNFDInstance_67, @function
unorm2_getNFDInstance_67:
.LFB3266:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L672
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L673
.L655:
	movl	4+_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L657
	movl	%eax, (%rbx)
.L657:
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rax
	testq	%rax, %rax
	je	.L658
	addq	$8, %rsp
	addq	$32, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	.cfi_restore_state
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L655
	movl	(%rbx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L656
	movq	%rbx, %rdi
	call	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
.L656:
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L12nfcSingletonE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L11nfcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L658:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3266:
	.size	unorm2_getNFDInstance_67, .-unorm2_getNFDInstance_67
	.p2align 4
	.globl	unorm2_close_67
	.type	unorm2_close_67, @function
unorm2_close_67:
.LFB3267:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L674
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L674:
	ret
	.cfi_endproc
.LFE3267:
	.size	unorm2_close_67, .-unorm2_close_67
	.p2align 4
	.globl	unorm2_normalize_67
	.type	unorm2_normalize_67, @function
unorm2_normalize_67:
.LFB3268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r9), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L676
	movq	%rdi, %r10
	movq	%rsi, %r12
	movslq	%edx, %r13
	movq	%rcx, %rbx
	movl	%r8d, %r14d
	testq	%rsi, %rsi
	je	.L715
	cmpl	$-1, %r13d
	jl	.L679
	testq	%rbx, %rbx
	je	.L716
.L681:
	testl	%r14d, %r14d
	js	.L679
.L682:
	cmpq	%rbx, %r12
	jne	.L683
	testq	%r12, %r12
	je	.L683
.L679:
	movl	$1, (%r9)
	xorl	%eax, %eax
.L676:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L717
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	testl	%r13d, %r13d
	jne	.L679
	testq	%rbx, %rbx
	jne	.L681
.L716:
	testl	%r14d, %r14d
	je	.L682
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L683:
	leaq	-192(%rbp), %r15
	xorl	%edx, %edx
	movl	%r14d, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r9, -264(%rbp)
	movq	%r10, -272(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	testl	%r13d, %r13d
	movq	-264(%rbp), %r9
	leaq	-256(%rbp), %r11
	je	.L684
	movq	-272(%rbp), %r10
	movq	%r11, -288(%rbp)
	testq	%r10, %r10
	je	.L685
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	leaq	_ZTIN6icu_6719Normalizer2WithImplE(%rip), %rdx
	movq	%r9, -280(%rbp)
	leaq	_ZTIN6icu_6711Normalizer2E(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	-272(%rbp), %r10
	movq	-280(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, -264(%rbp)
	je	.L685
	movq	-288(%rbp), %r11
	movq	8(%rax), %rax
	movq	%r9, %rdx
	movl	%r13d, %esi
	movq	%r15, -248(%rbp)
	movq	%r11, %rdi
	movq	%r11, -272(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movl	$0, -216(%rbp)
	movb	$0, -212(%rbp)
	call	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode@PLT
	movq	-272(%rbp), %r11
	movq	-280(%rbp), %r9
	testb	%al, %al
	je	.L686
	movq	-264(%rbp), %rax
	xorl	%edx, %edx
	movq	(%rax), %rax
	movq	144(%rax), %rax
	testl	%r13d, %r13d
	js	.L687
	leaq	(%r12,%r13,2), %rdx
.L687:
	movq	%r9, -280(%rbp)
	movq	%r9, %r8
	movq	%r11, %rcx
	movq	%r12, %rsi
	movq	%r11, -272(%rbp)
	movq	-264(%rbp), %rdi
	call	*%rax
	movq	-280(%rbp), %r9
	movq	-272(%rbp), %r11
.L686:
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L684
	movq	-224(%rbp), %rsi
	movq	-248(%rbp), %rdi
	movq	%r11, -272(%rbp)
	movq	%r9, -264(%rbp)
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	-264(%rbp), %r9
	movq	-272(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%r9, %rcx
	movl	%r14d, %edx
	movq	%r11, %rsi
	movq	%r15, %rdi
	movq	%rbx, -256(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, -264(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-264(%rbp), %eax
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L685:
	leaq	-256(%rbp), %r11
	movq	%r12, -256(%rbp)
	movl	%r13d, %esi
	movl	%r13d, %ecx
	leaq	-128(%rbp), %r12
	shrl	$31, %esi
	movq	%r11, %rdx
	movq	%r9, -280(%rbp)
	movq	%r12, %rdi
	movq	%r10, -264(%rbp)
	movq	%r11, -272(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	-264(%rbp), %r10
	movq	-280(%rbp), %r9
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	(%r10), %rax
	movq	%r9, -264(%rbp)
	movq	%r9, %rcx
	movq	%r10, %rdi
	call	*24(%rax)
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-272(%rbp), %r11
	movq	-264(%rbp), %r9
	jmp	.L684
.L717:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3268:
	.size	unorm2_normalize_67, .-unorm2_normalize_67
	.p2align 4
	.globl	unorm2_normalizeSecondAndAppend_67
	.type	unorm2_normalizeSecondAndAppend_67, @function
unorm2_normalizeSecondAndAppend_67:
.LFB3270:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %r10
	movl	(%r10), %r11d
	testl	%r11d, %r11d
	jg	.L718
	testq	%r8, %r8
	je	.L727
	cmpl	$-1, %r9d
	jl	.L721
.L722:
	testq	%rsi, %rsi
	je	.L728
	testl	%ecx, %ecx
	js	.L721
	cmpl	$-1, %edx
	jl	.L721
	cmpq	%r8, %rsi
	je	.L721
.L724:
	pushq	%r10
	pushq	$1
	call	_ZL24normalizeSecondAndAppendPK12UNormalizer2PDsiiPKDsiaP10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
.L718:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L727:
	.cfi_restore_state
	testl	%r9d, %r9d
	je	.L722
.L721:
	movl	$1, (%r10)
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L728:
	.cfi_restore_state
	movl	%edx, %eax
	orl	%ecx, %eax
	je	.L724
	jmp	.L721
	.cfi_endproc
.LFE3270:
	.size	unorm2_normalizeSecondAndAppend_67, .-unorm2_normalizeSecondAndAppend_67
	.p2align 4
	.globl	unorm2_append_67
	.type	unorm2_append_67, @function
unorm2_append_67:
.LFB3271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %r10
	movl	(%r10), %r11d
	testl	%r11d, %r11d
	jg	.L729
	testq	%r8, %r8
	je	.L738
	cmpl	$-1, %r9d
	jl	.L732
.L733:
	testq	%rsi, %rsi
	je	.L739
	testl	%ecx, %ecx
	js	.L732
	cmpl	$-1, %edx
	jl	.L732
	cmpq	%r8, %rsi
	je	.L732
.L735:
	pushq	%r10
	pushq	$0
	call	_ZL24normalizeSecondAndAppendPK12UNormalizer2PDsiiPKDsiaP10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
.L729:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L738:
	.cfi_restore_state
	testl	%r9d, %r9d
	je	.L733
.L732:
	movl	$1, (%r10)
	xorl	%eax, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	.cfi_restore_state
	movl	%edx, %eax
	orl	%ecx, %eax
	je	.L735
	jmp	.L732
	.cfi_endproc
.LFE3271:
	.size	unorm2_append_67, .-unorm2_append_67
	.p2align 4
	.globl	unorm2_getDecomposition_67
	.type	unorm2_getDecomposition_67, @function
unorm2_getDecomposition_67:
.LFB3272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L740
	movl	%ecx, %eax
	movq	%rdi, %r15
	movl	%esi, %r9d
	movq	%rdx, %rbx
	movl	%ecx, %r14d
	movq	%r8, %r12
	shrl	$31, %eax
	testq	%rdx, %rdx
	je	.L751
	testb	%al, %al
	je	.L744
.L754:
	movl	$1, (%r12)
	xorl	%r13d, %r13d
.L740:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L752
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore_state
	leaq	-128(%rbp), %r8
	xorl	%edx, %edx
	movl	%r14d, %ecx
	movq	%rbx, %rsi
	movq	%r8, %rdi
	movl	%r9d, -156(%rbp)
	movl	$-1, %r13d
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	-152(%rbp), %r8
	movq	(%r15), %rax
	movq	%r15, %rdi
	movl	-156(%rbp), %r9d
	movq	%r8, %rdx
	movl	%r9d, %esi
	call	*56(%rax)
	movq	-152(%rbp), %r8
	testb	%al, %al
	jne	.L753
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L751:
	testl	%ecx, %ecx
	setne	%al
	testb	%al, %al
	jne	.L754
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L753:
	leaq	-136(%rbp), %rsi
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r8, %rdi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r13d
	movq	-152(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L740
.L752:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3272:
	.size	unorm2_getDecomposition_67, .-unorm2_getDecomposition_67
	.p2align 4
	.globl	unorm2_getRawDecomposition_67
	.type	unorm2_getRawDecomposition_67, @function
unorm2_getRawDecomposition_67:
.LFB3273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L755
	movl	%ecx, %eax
	movq	%rdi, %r15
	movl	%esi, %r9d
	movq	%rdx, %rbx
	movl	%ecx, %r14d
	movq	%r8, %r12
	shrl	$31, %eax
	testq	%rdx, %rdx
	je	.L766
	testb	%al, %al
	je	.L759
.L769:
	movl	$1, (%r12)
	xorl	%r13d, %r13d
.L755:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L767
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L759:
	.cfi_restore_state
	leaq	-128(%rbp), %r8
	xorl	%edx, %edx
	movl	%r14d, %ecx
	movq	%rbx, %rsi
	movq	%r8, %rdi
	movl	%r9d, -156(%rbp)
	movl	$-1, %r13d
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPDsii@PLT
	movq	-152(%rbp), %r8
	movq	(%r15), %rax
	movq	%r15, %rdi
	movl	-156(%rbp), %r9d
	movq	%r8, %rdx
	movl	%r9d, %esi
	call	*64(%rax)
	movq	-152(%rbp), %r8
	testb	%al, %al
	jne	.L768
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L766:
	testl	%ecx, %ecx
	setne	%al
	testb	%al, %al
	jne	.L769
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L768:
	leaq	-136(%rbp), %rsi
	movq	%r12, %rcx
	movl	%r14d, %edx
	movq	%r8, %rdi
	movq	%rbx, -136(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movl	%eax, %r13d
	movq	-152(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L755
.L767:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3273:
	.size	unorm2_getRawDecomposition_67, .-unorm2_getRawDecomposition_67
	.p2align 4
	.globl	unorm2_composePair_67
	.type	unorm2_composePair_67, @function
unorm2_composePair_67:
.LFB3274:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*72(%rax)
	.cfi_endproc
.LFE3274:
	.size	unorm2_composePair_67, .-unorm2_composePair_67
	.p2align 4
	.globl	unorm2_getCombiningClass_67
	.type	unorm2_getCombiningClass_67, @function
unorm2_getCombiningClass_67:
.LFB3275:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*80(%rax)
	.cfi_endproc
.LFE3275:
	.size	unorm2_getCombiningClass_67, .-unorm2_getCombiningClass_67
	.p2align 4
	.globl	unorm2_isNormalized_67
	.type	unorm2_isNormalized_67, @function
unorm2_isNormalized_67:
.LFB3276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$120, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L772
	testq	%rsi, %rsi
	movq	%rcx, %r12
	sete	%cl
	testl	%edx, %edx
	setne	%al
	testb	%al, %cl
	jne	.L778
	cmpl	$-1, %edx
	jl	.L778
	movq	%rsi, -120(%rbp)
	leaq	-120(%rbp), %r8
	sete	%sil
	leaq	-112(%rbp), %r14
	movl	%edx, %ecx
	movq	%rdi, %r13
	movzbl	%sil, %esi
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*88(%rax)
	movq	%r14, %rdi
	movb	%al, -129(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movzbl	-129(%rbp), %eax
.L772:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L780
	addq	$120, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L778:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L772
.L780:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3276:
	.size	unorm2_isNormalized_67, .-unorm2_isNormalized_67
	.p2align 4
	.globl	unorm2_quickCheck_67
	.type	unorm2_quickCheck_67, @function
unorm2_quickCheck_67:
.LFB3277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$120, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L781
	testq	%rsi, %rsi
	movq	%rcx, %r12
	sete	%cl
	testl	%edx, %edx
	setne	%al
	testb	%al, %cl
	jne	.L787
	cmpl	$-1, %edx
	jl	.L787
	movq	%rsi, -120(%rbp)
	leaq	-120(%rbp), %r8
	sete	%sil
	leaq	-112(%rbp), %r14
	movl	%edx, %ecx
	movq	%rdi, %r13
	movzbl	%sil, %esi
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*104(%rax)
	movq	%r14, %rdi
	movl	%eax, -132(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-132(%rbp), %eax
.L781:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L789
	addq	$120, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L781
.L789:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3277:
	.size	unorm2_quickCheck_67, .-unorm2_quickCheck_67
	.p2align 4
	.globl	unorm2_spanQuickCheckYes_67
	.type	unorm2_spanQuickCheckYes_67, @function
unorm2_spanQuickCheckYes_67:
.LFB3278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$120, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L790
	testq	%rsi, %rsi
	movq	%rcx, %r12
	sete	%cl
	testl	%edx, %edx
	setne	%al
	testb	%al, %cl
	jne	.L796
	cmpl	$-1, %edx
	jl	.L796
	movq	%rsi, -120(%rbp)
	leaq	-120(%rbp), %r8
	sete	%sil
	leaq	-112(%rbp), %r14
	movl	%edx, %ecx
	movq	%rdi, %r13
	movzbl	%sil, %esi
	movq	%r8, %rdx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*112(%rax)
	movq	%r14, %rdi
	movl	%eax, -132(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-132(%rbp), %eax
.L790:
	movq	-40(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L798
	addq	$120, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	.cfi_restore_state
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L790
.L798:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3278:
	.size	unorm2_spanQuickCheckYes_67, .-unorm2_spanQuickCheckYes_67
	.p2align 4
	.globl	unorm2_hasBoundaryBefore_67
	.type	unorm2_hasBoundaryBefore_67, @function
unorm2_hasBoundaryBefore_67:
.LFB3279:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*120(%rax)
	.cfi_endproc
.LFE3279:
	.size	unorm2_hasBoundaryBefore_67, .-unorm2_hasBoundaryBefore_67
	.p2align 4
	.globl	unorm2_hasBoundaryAfter_67
	.type	unorm2_hasBoundaryAfter_67, @function
unorm2_hasBoundaryAfter_67:
.LFB3280:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*128(%rax)
	.cfi_endproc
.LFE3280:
	.size	unorm2_hasBoundaryAfter_67, .-unorm2_hasBoundaryAfter_67
	.p2align 4
	.globl	unorm2_isInert_67
	.type	unorm2_isInert_67, @function
unorm2_isInert_67:
.LFB3281:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*136(%rax)
	.cfi_endproc
.LFE3281:
	.size	unorm2_isInert_67, .-unorm2_isInert_67
	.p2align 4
	.globl	u_getCombiningClass_67
	.type	u_getCombiningClass_67, @function
u_getCombiningClass_67:
.LFB3282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L822
.L803:
	movl	4+_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rdi
	testl	%eax, %eax
	jle	.L805
	movl	%eax, -28(%rbp)
	testq	%rdi, %rdi
	jne	.L806
	.p2align 4,,10
	.p2align 3
.L809:
	xorl	%eax, %eax
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L822:
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L803
	movl	-28(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L804
	leaq	-28(%rbp), %rdi
	call	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
.L804:
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L12nfcSingletonE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	-28(%rbp), %eax
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L11nfcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rdi
.L805:
	movl	-28(%rbp), %eax
	testq	%rdi, %rdi
	je	.L808
.L806:
	addq	$32, %rdi
.L808:
	testl	%eax, %eax
	jg	.L809
	movq	(%rdi), %rax
	movl	%r12d, %esi
	call	*80(%rax)
.L802:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L823
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L823:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3282:
	.size	u_getCombiningClass_67, .-u_getCombiningClass_67
	.p2align 4
	.globl	unorm_getFCD16_67
	.type	unorm_getFCD16_67, @function
unorm_getFCD16_67:
.LFB3283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edi, %r12d
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L851
.L825:
	movl	4+_ZN6icu_67L11nfcInitOnceE(%rip), %eax
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rdi
	testl	%eax, %eax
	jle	.L827
	movl	%eax, -28(%rbp)
	testq	%rdi, %rdi
	jne	.L828
	.p2align 4,,10
	.p2align 3
.L831:
	xorl	%eax, %eax
.L824:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L852
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L851:
	.cfi_restore_state
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L825
	movl	-28(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L826
	leaq	-28(%rbp), %rdi
	call	_ZN6icu_6713Norm2AllModes17createNFCInstanceER10UErrorCode.part.0
.L826:
	leaq	uprv_normalizer2_cleanup(%rip), %rsi
	movl	$13, %edi
	movq	%rax, _ZN6icu_67L12nfcSingletonE(%rip)
	call	ucln_common_registerCleanup_67@PLT
	movl	-28(%rbp), %eax
	leaq	_ZN6icu_67L11nfcInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN6icu_67L11nfcInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	_ZN6icu_67L12nfcSingletonE(%rip), %rdi
.L827:
	movl	-28(%rbp), %eax
	testq	%rdi, %rdi
	je	.L830
.L828:
	movq	(%rdi), %rdi
.L830:
	testl	%eax, %eax
	jg	.L831
	movzwl	8(%rdi), %eax
	cmpl	%eax, %r12d
	jl	.L831
	cmpl	$65535, %r12d
	jg	.L832
	movl	%r12d, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L831
	movl	%r12d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L831
.L832:
	movl	%r12d, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi@PLT
	jmp	.L824
.L852:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3283:
	.size	unorm_getFCD16_67, .-unorm_getFCD16_67
	.weak	_ZTSN6icu_6711Normalizer2E
	.section	.rodata._ZTSN6icu_6711Normalizer2E,"aG",@progbits,_ZTSN6icu_6711Normalizer2E,comdat
	.align 16
	.type	_ZTSN6icu_6711Normalizer2E, @object
	.size	_ZTSN6icu_6711Normalizer2E, 23
_ZTSN6icu_6711Normalizer2E:
	.string	"N6icu_6711Normalizer2E"
	.weak	_ZTIN6icu_6711Normalizer2E
	.section	.data.rel.ro._ZTIN6icu_6711Normalizer2E,"awG",@progbits,_ZTIN6icu_6711Normalizer2E,comdat
	.align 8
	.type	_ZTIN6icu_6711Normalizer2E, @object
	.size	_ZTIN6icu_6711Normalizer2E, 24
_ZTIN6icu_6711Normalizer2E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6711Normalizer2E
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6719Normalizer2WithImplE
	.section	.rodata._ZTSN6icu_6719Normalizer2WithImplE,"aG",@progbits,_ZTSN6icu_6719Normalizer2WithImplE,comdat
	.align 16
	.type	_ZTSN6icu_6719Normalizer2WithImplE, @object
	.size	_ZTSN6icu_6719Normalizer2WithImplE, 31
_ZTSN6icu_6719Normalizer2WithImplE:
	.string	"N6icu_6719Normalizer2WithImplE"
	.weak	_ZTIN6icu_6719Normalizer2WithImplE
	.section	.data.rel.ro._ZTIN6icu_6719Normalizer2WithImplE,"awG",@progbits,_ZTIN6icu_6719Normalizer2WithImplE,comdat
	.align 8
	.type	_ZTIN6icu_6719Normalizer2WithImplE, @object
	.size	_ZTIN6icu_6719Normalizer2WithImplE, 24
_ZTIN6icu_6719Normalizer2WithImplE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719Normalizer2WithImplE
	.quad	_ZTIN6icu_6711Normalizer2E
	.weak	_ZTSN6icu_6720DecomposeNormalizer2E
	.section	.rodata._ZTSN6icu_6720DecomposeNormalizer2E,"aG",@progbits,_ZTSN6icu_6720DecomposeNormalizer2E,comdat
	.align 32
	.type	_ZTSN6icu_6720DecomposeNormalizer2E, @object
	.size	_ZTSN6icu_6720DecomposeNormalizer2E, 32
_ZTSN6icu_6720DecomposeNormalizer2E:
	.string	"N6icu_6720DecomposeNormalizer2E"
	.weak	_ZTIN6icu_6720DecomposeNormalizer2E
	.section	.data.rel.ro._ZTIN6icu_6720DecomposeNormalizer2E,"awG",@progbits,_ZTIN6icu_6720DecomposeNormalizer2E,comdat
	.align 8
	.type	_ZTIN6icu_6720DecomposeNormalizer2E, @object
	.size	_ZTIN6icu_6720DecomposeNormalizer2E, 24
_ZTIN6icu_6720DecomposeNormalizer2E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6720DecomposeNormalizer2E
	.quad	_ZTIN6icu_6719Normalizer2WithImplE
	.weak	_ZTSN6icu_6718ComposeNormalizer2E
	.section	.rodata._ZTSN6icu_6718ComposeNormalizer2E,"aG",@progbits,_ZTSN6icu_6718ComposeNormalizer2E,comdat
	.align 16
	.type	_ZTSN6icu_6718ComposeNormalizer2E, @object
	.size	_ZTSN6icu_6718ComposeNormalizer2E, 30
_ZTSN6icu_6718ComposeNormalizer2E:
	.string	"N6icu_6718ComposeNormalizer2E"
	.weak	_ZTIN6icu_6718ComposeNormalizer2E
	.section	.data.rel.ro._ZTIN6icu_6718ComposeNormalizer2E,"awG",@progbits,_ZTIN6icu_6718ComposeNormalizer2E,comdat
	.align 8
	.type	_ZTIN6icu_6718ComposeNormalizer2E, @object
	.size	_ZTIN6icu_6718ComposeNormalizer2E, 24
_ZTIN6icu_6718ComposeNormalizer2E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718ComposeNormalizer2E
	.quad	_ZTIN6icu_6719Normalizer2WithImplE
	.weak	_ZTSN6icu_6714FCDNormalizer2E
	.section	.rodata._ZTSN6icu_6714FCDNormalizer2E,"aG",@progbits,_ZTSN6icu_6714FCDNormalizer2E,comdat
	.align 16
	.type	_ZTSN6icu_6714FCDNormalizer2E, @object
	.size	_ZTSN6icu_6714FCDNormalizer2E, 26
_ZTSN6icu_6714FCDNormalizer2E:
	.string	"N6icu_6714FCDNormalizer2E"
	.weak	_ZTIN6icu_6714FCDNormalizer2E
	.section	.data.rel.ro._ZTIN6icu_6714FCDNormalizer2E,"awG",@progbits,_ZTIN6icu_6714FCDNormalizer2E,comdat
	.align 8
	.type	_ZTIN6icu_6714FCDNormalizer2E, @object
	.size	_ZTIN6icu_6714FCDNormalizer2E, 24
_ZTIN6icu_6714FCDNormalizer2E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6714FCDNormalizer2E
	.quad	_ZTIN6icu_6719Normalizer2WithImplE
	.weak	_ZTSN6icu_6715NoopNormalizer2E
	.section	.rodata._ZTSN6icu_6715NoopNormalizer2E,"aG",@progbits,_ZTSN6icu_6715NoopNormalizer2E,comdat
	.align 16
	.type	_ZTSN6icu_6715NoopNormalizer2E, @object
	.size	_ZTSN6icu_6715NoopNormalizer2E, 27
_ZTSN6icu_6715NoopNormalizer2E:
	.string	"N6icu_6715NoopNormalizer2E"
	.weak	_ZTIN6icu_6715NoopNormalizer2E
	.section	.data.rel.ro._ZTIN6icu_6715NoopNormalizer2E,"awG",@progbits,_ZTIN6icu_6715NoopNormalizer2E,comdat
	.align 8
	.type	_ZTIN6icu_6715NoopNormalizer2E, @object
	.size	_ZTIN6icu_6715NoopNormalizer2E, 24
_ZTIN6icu_6715NoopNormalizer2E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715NoopNormalizer2E
	.quad	_ZTIN6icu_6711Normalizer2E
	.weak	_ZTVN6icu_6711Normalizer2E
	.section	.data.rel.ro._ZTVN6icu_6711Normalizer2E,"awG",@progbits,_ZTVN6icu_6711Normalizer2E,comdat
	.align 8
	.type	_ZTVN6icu_6711Normalizer2E, @object
	.size	_ZTVN6icu_6711Normalizer2E, 160
_ZTVN6icu_6711Normalizer2E:
	.quad	0
	.quad	_ZTIN6icu_6711Normalizer2E
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6711Normalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6711Normalizer219getRawDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6711Normalizer211composePairEii
	.quad	_ZNK6icu_6711Normalizer217getCombiningClassEi
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6711Normalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6715NoopNormalizer2E
	.section	.data.rel.ro._ZTVN6icu_6715NoopNormalizer2E,"awG",@progbits,_ZTVN6icu_6715NoopNormalizer2E,comdat
	.align 8
	.type	_ZTVN6icu_6715NoopNormalizer2E, @object
	.size	_ZTVN6icu_6715NoopNormalizer2E, 160
_ZTVN6icu_6715NoopNormalizer2E:
	.quad	0
	.quad	_ZTIN6icu_6715NoopNormalizer2E
	.quad	_ZN6icu_6715NoopNormalizer2D1Ev
	.quad	_ZN6icu_6715NoopNormalizer2D0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6715NoopNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.quad	_ZNK6icu_6715NoopNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.quad	_ZNK6icu_6715NoopNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6715NoopNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6715NoopNormalizer216getDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6711Normalizer219getRawDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6711Normalizer211composePairEii
	.quad	_ZNK6icu_6711Normalizer217getCombiningClassEi
	.quad	_ZNK6icu_6715NoopNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6715NoopNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.quad	_ZNK6icu_6715NoopNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6715NoopNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6715NoopNormalizer217hasBoundaryBeforeEi
	.quad	_ZNK6icu_6715NoopNormalizer216hasBoundaryAfterEi
	.quad	_ZNK6icu_6715NoopNormalizer27isInertEi
	.weak	_ZTVN6icu_6719Normalizer2WithImplE
	.section	.data.rel.ro._ZTVN6icu_6719Normalizer2WithImplE,"awG",@progbits,_ZTVN6icu_6719Normalizer2WithImplE,comdat
	.align 8
	.type	_ZTVN6icu_6719Normalizer2WithImplE, @object
	.size	_ZTVN6icu_6719Normalizer2WithImplE, 192
_ZTVN6icu_6719Normalizer2WithImplE:
	.quad	0
	.quad	_ZTIN6icu_6719Normalizer2WithImplE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.quad	_ZNK6icu_6711Normalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6719Normalizer2WithImpl11composePairEii
	.quad	_ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi
	.quad	_ZNK6icu_6719Normalizer2WithImpl12isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6711Normalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl10quickCheckERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6719Normalizer2WithImpl13getQuickCheckEi
	.weak	_ZTVN6icu_6720DecomposeNormalizer2E
	.section	.data.rel.ro._ZTVN6icu_6720DecomposeNormalizer2E,"awG",@progbits,_ZTVN6icu_6720DecomposeNormalizer2E,comdat
	.align 8
	.type	_ZTVN6icu_6720DecomposeNormalizer2E, @object
	.size	_ZTVN6icu_6720DecomposeNormalizer2E, 192
_ZTVN6icu_6720DecomposeNormalizer2E:
	.quad	0
	.quad	_ZTIN6icu_6720DecomposeNormalizer2E
	.quad	_ZN6icu_6720DecomposeNormalizer2D1Ev
	.quad	_ZN6icu_6720DecomposeNormalizer2D0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.quad	_ZNK6icu_6711Normalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6719Normalizer2WithImpl11composePairEii
	.quad	_ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi
	.quad	_ZNK6icu_6719Normalizer2WithImpl12isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6711Normalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl10quickCheckERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6720DecomposeNormalizer217hasBoundaryBeforeEi
	.quad	_ZNK6icu_6720DecomposeNormalizer216hasBoundaryAfterEi
	.quad	_ZNK6icu_6720DecomposeNormalizer27isInertEi
	.quad	_ZNK6icu_6720DecomposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode
	.quad	_ZNK6icu_6720DecomposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.quad	_ZNK6icu_6720DecomposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode
	.quad	_ZNK6icu_6720DecomposeNormalizer213getQuickCheckEi
	.weak	_ZTVN6icu_6718ComposeNormalizer2E
	.section	.data.rel.ro._ZTVN6icu_6718ComposeNormalizer2E,"awG",@progbits,_ZTVN6icu_6718ComposeNormalizer2E,comdat
	.align 8
	.type	_ZTVN6icu_6718ComposeNormalizer2E, @object
	.size	_ZTVN6icu_6718ComposeNormalizer2E, 192
_ZTVN6icu_6718ComposeNormalizer2E:
	.quad	0
	.quad	_ZTIN6icu_6718ComposeNormalizer2E
	.quad	_ZN6icu_6718ComposeNormalizer2D1Ev
	.quad	_ZN6icu_6718ComposeNormalizer2D0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.quad	_ZNK6icu_6718ComposeNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6719Normalizer2WithImpl11composePairEii
	.quad	_ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi
	.quad	_ZNK6icu_6718ComposeNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6718ComposeNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.quad	_ZNK6icu_6718ComposeNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6718ComposeNormalizer217hasBoundaryBeforeEi
	.quad	_ZNK6icu_6718ComposeNormalizer216hasBoundaryAfterEi
	.quad	_ZNK6icu_6718ComposeNormalizer27isInertEi
	.quad	_ZNK6icu_6718ComposeNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode
	.quad	_ZNK6icu_6718ComposeNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.quad	_ZNK6icu_6718ComposeNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode
	.quad	_ZNK6icu_6718ComposeNormalizer213getQuickCheckEi
	.weak	_ZTVN6icu_6714FCDNormalizer2E
	.section	.data.rel.ro._ZTVN6icu_6714FCDNormalizer2E,"awG",@progbits,_ZTVN6icu_6714FCDNormalizer2E,comdat
	.align 8
	.type	_ZTVN6icu_6714FCDNormalizer2E, @object
	.size	_ZTVN6icu_6714FCDNormalizer2E, 192
_ZTVN6icu_6714FCDNormalizer2E:
	.quad	0
	.quad	_ZTIN6icu_6714FCDNormalizer2E
	.quad	_ZN6icu_6714FCDNormalizer2D1Ev
	.quad	_ZN6icu_6714FCDNormalizer2D0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6719Normalizer2WithImpl9normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.quad	_ZNK6icu_6711Normalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl24normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl6appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl16getDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6719Normalizer2WithImpl19getRawDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6719Normalizer2WithImpl11composePairEii
	.quad	_ZNK6icu_6719Normalizer2WithImpl17getCombiningClassEi
	.quad	_ZNK6icu_6719Normalizer2WithImpl12isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6711Normalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl10quickCheckERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl17spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6714FCDNormalizer217hasBoundaryBeforeEi
	.quad	_ZNK6icu_6714FCDNormalizer216hasBoundaryAfterEi
	.quad	_ZNK6icu_6714FCDNormalizer27isInertEi
	.quad	_ZNK6icu_6714FCDNormalizer29normalizeEPKDsS2_RNS_16ReorderingBufferER10UErrorCode
	.quad	_ZNK6icu_6714FCDNormalizer218normalizeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.quad	_ZNK6icu_6714FCDNormalizer217spanQuickCheckYesEPKDsS2_R10UErrorCode
	.quad	_ZNK6icu_6719Normalizer2WithImpl13getQuickCheckEi
	.local	_ZN6icu_67L11nfcInitOnceE
	.comm	_ZN6icu_67L11nfcInitOnceE,8,8
	.local	_ZN6icu_67L12nfcSingletonE
	.comm	_ZN6icu_67L12nfcSingletonE,8,8
	.local	_ZN6icu_67L12noopInitOnceE
	.comm	_ZN6icu_67L12noopInitOnceE,8,8
	.local	_ZN6icu_67L13noopSingletonE
	.comm	_ZN6icu_67L13noopSingletonE,8,8
	.section	.rodata
	.align 32
	.type	_ZL23norm2_nfc_data_smallFCD, @object
	.size	_ZL23norm2_nfc_data_smallFCD, 256
_ZL23norm2_nfc_data_smallFCD:
	.string	"\300\357\003\177\337p\317\207\307\346fFdFf[\022"
	.string	""
	.string	"\004"
	.string	""
	.string	""
	.string	"C \002i\256\302\300\377\377\300r\277"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"@"
	.string	"\200\210"
	.string	""
	.string	"\376"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\230"
	.string	"\303f\340\200"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\007"
	.string	""
	.string	"\002"
	.align 32
	.type	_ZL24norm2_nfc_data_extraData, @object
	.size	_ZL24norm2_nfc_data_extraData, 15464
_ZL24norm2_nfc_data_extraData:
	.value	-1
	.value	-1
	.value	-31120
	.value	17628
	.value	-31120
	.value	17600
	.value	-31120
	.value	17630
	.value	1536
	.value	384
	.value	1538
	.value	386
	.value	1540
	.value	389
	.value	1542
	.value	390
	.value	1544
	.value	512
	.value	1548
	.value	517
	.value	1550
	.value	1101
	.value	1552
	.value	393
	.value	1554
	.value	15684
	.value	1556
	.value	395
	.value	1560
	.value	922
	.value	1566
	.value	1024
	.value	1570
	.value	1028
	.value	1606
	.value	15681
	.value	1610
	.value	15360
	.value	-31152
	.value	520
	.value	1550
	.value	15364
	.value	1606
	.value	15368
	.value	-31134
	.value	15372
	.value	1538
	.value	524
	.value	1540
	.value	528
	.value	1550
	.value	532
	.value	1560
	.value	536
	.value	-31154
	.value	399
	.value	1550
	.value	15380
	.value	1560
	.value	540
	.value	1606
	.value	15384
	.value	1614
	.value	15392
	.value	1626
	.value	15396
	.value	-31134
	.value	15388
	.value	1536
	.value	400
	.value	1538
	.value	402
	.value	1540
	.value	405
	.value	1542
	.value	15736
	.value	1544
	.value	549
	.value	1548
	.value	552
	.value	1550
	.value	556
	.value	1552
	.value	406
	.value	1554
	.value	15732
	.value	1560
	.value	564
	.value	1566
	.value	1032
	.value	1570
	.value	1036
	.value	1606
	.value	15729
	.value	1614
	.value	1105
	.value	1616
	.value	560
	.value	1626
	.value	15408
	.value	-31136
	.value	15412
	.value	-31218
	.value	15420
	.value	1538
	.value	1000
	.value	1540
	.value	568
	.value	1544
	.value	15424
	.value	1548
	.value	572
	.value	1550
	.value	576
	.value	1560
	.value	972
	.value	-31154
	.value	580
	.value	1540
	.value	584
	.value	1550
	.value	15428
	.value	1552
	.value	15436
	.value	1560
	.value	1084
	.value	1606
	.value	15432
	.value	1614
	.value	15440
	.value	-31140
	.value	15444
	.value	1536
	.value	408
	.value	1538
	.value	410
	.value	1540
	.value	412
	.value	1542
	.value	592
	.value	1544
	.value	596
	.value	1548
	.value	600
	.value	1550
	.value	608
	.value	1552
	.value	415
	.value	1554
	.value	15760
	.value	1560
	.value	926
	.value	1566
	.value	1040
	.value	1570
	.value	1044
	.value	1606
	.value	15764
	.value	1616
	.value	604
	.value	-31136
	.value	15448
	.value	-31228
	.value	616
	.value	1538
	.value	15456
	.value	1560
	.value	976
	.value	1606
	.value	15460
	.value	1614
	.value	620
	.value	-31134
	.value	15464
	.value	1538
	.value	626
	.value	1560
	.value	634
	.value	1606
	.value	15469
	.value	1614
	.value	630
	.value	1626
	.value	15480
	.value	-31134
	.value	15476
	.value	1538
	.value	15484
	.value	1550
	.value	15488
	.value	-31162
	.value	15492
	.value	1536
	.value	1008
	.value	1538
	.value	646
	.value	1542
	.value	418
	.value	1550
	.value	15496
	.value	1560
	.value	654
	.value	1606
	.value	15500
	.value	1614
	.value	650
	.value	1626
	.value	15508
	.value	-31134
	.value	15504
	.value	1536
	.value	420
	.value	1538
	.value	422
	.value	1540
	.value	425
	.value	1542
	.value	427
	.value	1544
	.value	665
	.value	1548
	.value	668
	.value	1550
	.value	1117
	.value	1552
	.value	429
	.value	1554
	.value	15772
	.value	1558
	.value	672
	.value	1560
	.value	930
	.value	1566
	.value	1048
	.value	1570
	.value	1052
	.value	1590
	.value	833
	.value	1606
	.value	15769
	.value	-31152
	.value	981
	.value	1538
	.value	15528
	.value	-31218
	.value	15532
	.value	1538
	.value	680
	.value	1550
	.value	15536
	.value	1560
	.value	688
	.value	1566
	.value	1056
	.value	1570
	.value	1060
	.value	1606
	.value	15541
	.value	1614
	.value	684
	.value	-31134
	.value	15548
	.value	1538
	.value	693
	.value	1540
	.value	696
	.value	1550
	.value	15552
	.value	1560
	.value	705
	.value	1606
	.value	15557
	.value	1612
	.value	1072
	.value	-31154
	.value	700
	.value	1550
	.value	15572
	.value	1560
	.value	712
	.value	1606
	.value	15576
	.value	1612
	.value	1076
	.value	1614
	.value	708
	.value	1626
	.value	15584
	.value	-31134
	.value	15580
	.value	1536
	.value	434
	.value	1538
	.value	436
	.value	1540
	.value	438
	.value	1542
	.value	721
	.value	1544
	.value	725
	.value	1548
	.value	728
	.value	1552
	.value	441
	.value	1554
	.value	15820
	.value	1556
	.value	732
	.value	1558
	.value	736
	.value	1560
	.value	934
	.value	1566
	.value	1064
	.value	1570
	.value	1068
	.value	1590
	.value	863
	.value	1606
	.value	15816
	.value	1608
	.value	15588
	.value	1616
	.value	740
	.value	1626
	.value	15596
	.value	-31136
	.value	15592
	.value	1542
	.value	15608
	.value	-31162
	.value	15612
	.value	1536
	.value	15616
	.value	1538
	.value	15620
	.value	1540
	.value	744
	.value	1550
	.value	15628
	.value	1552
	.value	15624
	.value	-31162
	.value	15632
	.value	1550
	.value	15636
	.value	-31216
	.value	15640
	.value	1536
	.value	15844
	.value	1538
	.value	442
	.value	1540
	.value	748
	.value	1542
	.value	15856
	.value	1544
	.value	1124
	.value	1550
	.value	15644
	.value	1552
	.value	752
	.value	1554
	.value	15852
	.value	-31162
	.value	15848
	.value	1538
	.value	754
	.value	1540
	.value	15648
	.value	1550
	.value	758
	.value	1560
	.value	762
	.value	1606
	.value	15652
	.value	-31134
	.value	15656
	.value	1536
	.value	448
	.value	1538
	.value	450
	.value	1540
	.value	453
	.value	1542
	.value	454
	.value	1544
	.value	514
	.value	1548
	.value	519
	.value	1550
	.value	1103
	.value	1552
	.value	457
	.value	1554
	.value	15686
	.value	1556
	.value	459
	.value	1560
	.value	924
	.value	1566
	.value	1026
	.value	1570
	.value	1030
	.value	1606
	.value	15683
	.value	1610
	.value	15362
	.value	-31152
	.value	522
	.value	1550
	.value	15366
	.value	1606
	.value	15370
	.value	-31134
	.value	15374
	.value	1538
	.value	526
	.value	1540
	.value	530
	.value	1550
	.value	534
	.value	1560
	.value	538
	.value	-31154
	.value	463
	.value	1550
	.value	15382
	.value	1560
	.value	542
	.value	1606
	.value	15386
	.value	1614
	.value	15394
	.value	1626
	.value	15398
	.value	-31134
	.value	15390
	.value	1536
	.value	464
	.value	1538
	.value	466
	.value	1540
	.value	469
	.value	1542
	.value	15738
	.value	1544
	.value	551
	.value	1548
	.value	554
	.value	1550
	.value	558
	.value	1552
	.value	470
	.value	1554
	.value	15734
	.value	1560
	.value	566
	.value	1566
	.value	1034
	.value	1570
	.value	1038
	.value	1606
	.value	15731
	.value	1614
	.value	1107
	.value	1616
	.value	562
	.value	1626
	.value	15410
	.value	-31136
	.value	15414
	.value	-31218
	.value	15422
	.value	1538
	.value	1002
	.value	1540
	.value	570
	.value	1544
	.value	15426
	.value	1548
	.value	574
	.value	1550
	.value	578
	.value	1560
	.value	974
	.value	-31154
	.value	582
	.value	1540
	.value	586
	.value	1550
	.value	15430
	.value	1552
	.value	15438
	.value	1560
	.value	1086
	.value	1606
	.value	15434
	.value	1614
	.value	15442
	.value	1628
	.value	15446
	.value	-31134
	.value	15660
	.value	1536
	.value	472
	.value	1538
	.value	474
	.value	1540
	.value	476
	.value	1542
	.value	594
	.value	1544
	.value	598
	.value	1548
	.value	602
	.value	1552
	.value	479
	.value	1554
	.value	15762
	.value	1560
	.value	928
	.value	1566
	.value	1042
	.value	1570
	.value	1046
	.value	1606
	.value	15766
	.value	1616
	.value	606
	.value	-31136
	.value	15450
	.value	1540
	.value	618
	.value	-31208
	.value	992
	.value	1538
	.value	15458
	.value	1560
	.value	978
	.value	1606
	.value	15462
	.value	1614
	.value	622
	.value	-31134
	.value	15466
	.value	1538
	.value	628
	.value	1560
	.value	636
	.value	1606
	.value	15471
	.value	1614
	.value	632
	.value	1626
	.value	15482
	.value	-31134
	.value	15478
	.value	1538
	.value	15486
	.value	1550
	.value	15490
	.value	-31162
	.value	15494
	.value	1536
	.value	1010
	.value	1538
	.value	648
	.value	1542
	.value	482
	.value	1550
	.value	15498
	.value	1560
	.value	656
	.value	1606
	.value	15502
	.value	1614
	.value	652
	.value	1626
	.value	15510
	.value	-31134
	.value	15506
	.value	1536
	.value	484
	.value	1538
	.value	486
	.value	1540
	.value	489
	.value	1542
	.value	491
	.value	1544
	.value	667
	.value	1548
	.value	670
	.value	1550
	.value	1119
	.value	1552
	.value	493
	.value	1554
	.value	15774
	.value	1558
	.value	674
	.value	1560
	.value	932
	.value	1566
	.value	1050
	.value	1570
	.value	1054
	.value	1590
	.value	835
	.value	1606
	.value	15771
	.value	-31152
	.value	983
	.value	1538
	.value	15530
	.value	-31218
	.value	15534
	.value	1538
	.value	682
	.value	1550
	.value	15538
	.value	1560
	.value	690
	.value	1566
	.value	1058
	.value	1570
	.value	1062
	.value	1606
	.value	15543
	.value	1614
	.value	686
	.value	-31134
	.value	15550
	.value	1538
	.value	695
	.value	1540
	.value	698
	.value	1550
	.value	15554
	.value	1560
	.value	707
	.value	1606
	.value	15559
	.value	1612
	.value	1074
	.value	-31154
	.value	702
	.value	1550
	.value	15574
	.value	1552
	.value	15662
	.value	1560
	.value	714
	.value	1606
	.value	15578
	.value	1612
	.value	1078
	.value	1614
	.value	710
	.value	1626
	.value	15586
	.value	-31134
	.value	15582
	.value	1536
	.value	498
	.value	1538
	.value	500
	.value	1540
	.value	502
	.value	1542
	.value	723
	.value	1544
	.value	727
	.value	1548
	.value	730
	.value	1552
	.value	505
	.value	1554
	.value	15822
	.value	1556
	.value	734
	.value	1558
	.value	738
	.value	1560
	.value	936
	.value	1566
	.value	1066
	.value	1570
	.value	1070
	.value	1590
	.value	865
	.value	1606
	.value	15818
	.value	1608
	.value	15590
	.value	1616
	.value	742
	.value	1626
	.value	15598
	.value	-31136
	.value	15594
	.value	1542
	.value	15610
	.value	-31162
	.value	15614
	.value	1536
	.value	15618
	.value	1538
	.value	15622
	.value	1540
	.value	746
	.value	1550
	.value	15630
	.value	1552
	.value	15626
	.value	1556
	.value	15664
	.value	-31162
	.value	15634
	.value	1550
	.value	15638
	.value	-31216
	.value	15642
	.value	1536
	.value	15846
	.value	1538
	.value	506
	.value	1540
	.value	750
	.value	1542
	.value	15858
	.value	1544
	.value	1126
	.value	1550
	.value	15646
	.value	1552
	.value	510
	.value	1554
	.value	15854
	.value	1556
	.value	15666
	.value	-31162
	.value	15850
	.value	1538
	.value	756
	.value	1540
	.value	15650
	.value	1550
	.value	760
	.value	1560
	.value	764
	.value	1606
	.value	15654
	.value	-31134
	.value	15658
	.value	1536
	.value	16346
	.value	1538
	.value	1802
	.value	-31100
	.value	16258
	.value	1538
	.value	1016
	.value	-31224
	.value	964
	.value	-31230
	.value	1020
	.value	1538
	.value	1018
	.value	-31224
	.value	966
	.value	-31230
	.value	1022
	.value	-31218
	.value	15670
	.value	-31208
	.value	988
	.value	-31208
	.value	990
	.value	1536
	.value	16244
	.value	1538
	.value	1804
	.value	1544
	.value	16242
	.value	1548
	.value	16240
	.value	1574
	.value	15889
	.value	1576
	.value	15891
	.value	-31094
	.value	16248
	.value	1536
	.value	16272
	.value	1538
	.value	1808
	.value	1574
	.value	15921
	.value	-31192
	.value	15923
	.value	1536
	.value	16276
	.value	1538
	.value	1810
	.value	1574
	.value	15953
	.value	1576
	.value	15955
	.value	-31094
	.value	16280
	.value	1536
	.value	16308
	.value	1538
	.value	1812
	.value	1544
	.value	16306
	.value	1548
	.value	16304
	.value	1552
	.value	1876
	.value	1574
	.value	15985
	.value	-31192
	.value	15987
	.value	1536
	.value	16368
	.value	1538
	.value	1816
	.value	1574
	.value	16017
	.value	-31192
	.value	16019
	.value	-31192
	.value	16344
	.value	1536
	.value	16340
	.value	1538
	.value	1820
	.value	1544
	.value	16338
	.value	1548
	.value	16336
	.value	1552
	.value	1878
	.value	-31192
	.value	16051
	.value	1536
	.value	16372
	.value	1538
	.value	1822
	.value	1574
	.value	16081
	.value	1576
	.value	16083
	.value	-31094
	.value	16376
	.value	1536
	.value	16097
	.value	1538
	.value	1881
	.value	1544
	.value	16226
	.value	1548
	.value	16224
	.value	1574
	.value	15873
	.value	1576
	.value	15875
	.value	1668
	.value	16237
	.value	-31094
	.value	16230
	.value	1536
	.value	16100
	.value	1538
	.value	1882
	.value	1574
	.value	15905
	.value	-31192
	.value	15907
	.value	1536
	.value	16105
	.value	1538
	.value	1885
	.value	1574
	.value	15937
	.value	1576
	.value	15939
	.value	1668
	.value	16269
	.value	-31094
	.value	16262
	.value	1536
	.value	16108
	.value	1538
	.value	1886
	.value	1544
	.value	16290
	.value	1548
	.value	16288
	.value	1552
	.value	1941
	.value	1574
	.value	15969
	.value	1576
	.value	15971
	.value	-31100
	.value	16300
	.value	1536
	.value	16112
	.value	1538
	.value	1944
	.value	1574
	.value	16001
	.value	-31192
	.value	16003
	.value	1574
	.value	16328
	.value	-31192
	.value	16330
	.value	1536
	.value	16116
	.value	1538
	.value	1946
	.value	1544
	.value	16322
	.value	1548
	.value	16320
	.value	1552
	.value	1943
	.value	1574
	.value	16033
	.value	1576
	.value	16035
	.value	-31100
	.value	16332
	.value	1536
	.value	16121
	.value	1538
	.value	1949
	.value	1574
	.value	16065
	.value	1576
	.value	16067
	.value	1668
	.value	16365
	.value	-31094
	.value	16358
	.value	1538
	.value	1958
	.value	-31216
	.value	1960
	.value	-31216
	.value	2062
	.value	1548
	.value	2464
	.value	-31216
	.value	2468
	.value	-31230
	.value	2054
	.value	1536
	.value	2048
	.value	1548
	.value	2476
	.value	-31216
	.value	2050
	.value	1548
	.value	2434
	.value	-31216
	.value	2488
	.value	-31216
	.value	2492
	.value	1536
	.value	2074
	.value	1544
	.value	2500
	.value	1548
	.value	2098
	.value	-31216
	.value	2504
	.value	-31230
	.value	2072
	.value	-31216
	.value	2508
	.value	1544
	.value	2524
	.value	1548
	.value	2076
	.value	1552
	.value	2528
	.value	-31210
	.value	2532
	.value	-31216
	.value	2536
	.value	-31216
	.value	2544
	.value	-31216
	.value	2520
	.value	1548
	.value	2466
	.value	-31216
	.value	2470
	.value	-31230
	.value	2214
	.value	1536
	.value	2208
	.value	1548
	.value	2478
	.value	-31216
	.value	2210
	.value	1548
	.value	2436
	.value	-31216
	.value	2490
	.value	-31216
	.value	2494
	.value	1536
	.value	2234
	.value	1544
	.value	2502
	.value	1548
	.value	2162
	.value	-31216
	.value	2506
	.value	-31230
	.value	2232
	.value	-31216
	.value	2510
	.value	1544
	.value	2526
	.value	1548
	.value	2236
	.value	1552
	.value	2530
	.value	-31210
	.value	2534
	.value	-31216
	.value	2538
	.value	-31216
	.value	2546
	.value	-31216
	.value	2522
	.value	-31216
	.value	2222
	.value	-31202
	.value	2284
	.value	-31202
	.value	2286
	.value	-31216
	.value	2484
	.value	-31216
	.value	2486
	.value	-31216
	.value	2516
	.value	-31216
	.value	2518
	.value	3238
	.value	3140
	.value	3240
	.value	3142
	.value	-29526
	.value	3146
	.value	-29528
	.value	3144
	.value	-29528
	.value	3148
	.value	-29528
	.value	3460
	.value	-29528
	.value	3494
	.value	-29528
	.value	3456
	.value	-28040
	.value	4690
	.value	-28040
	.value	4706
	.value	-28040
	.value	4712
	.value	4988
	.value	5014
	.value	-27730
	.value	5016
	.value	5756
	.value	5782
	.value	5804
	.value	5776
	.value	-26962
	.value	5784
	.value	-26706
	.value	5928
	.value	6012
	.value	6036
	.value	-26706
	.value	6040
	.value	-26756
	.value	6038
	.value	-26452
	.value	6288
	.value	-26198
	.value	6528
	.value	6532
	.value	6549
	.value	6570
	.value	6542
	.value	-26196
	.value	6544
	.value	6780
	.value	6804
	.value	-25938
	.value	6808
	.value	-25988
	.value	6806
	.value	7060
	.value	7092
	.value	7070
	.value	7097
	.value	-25666
	.value	7100
	.value	-24484
	.value	8268
	.value	-18838
	.value	13836
	.value	-18838
	.value	13840
	.value	-18838
	.value	13844
	.value	-18838
	.value	13848
	.value	-18838
	.value	13852
	.value	-18838
	.value	13860
	.value	-18838
	.value	13942
	.value	-18838
	.value	13946
	.value	-18838
	.value	13952
	.value	-18838
	.value	13954
	.value	-18838
	.value	13958
	.value	1536
	.value	16282
	.value	1538
	.value	16284
	.value	-31100
	.value	16286
	.value	1536
	.value	16314
	.value	1538
	.value	16316
	.value	-31100
	.value	16318
	.value	-31120
	.value	17204
	.value	-31120
	.value	17206
	.value	-31120
	.value	17244
	.value	-31120
	.value	17306
	.value	-31120
	.value	17310
	.value	-31120
	.value	17308
	.value	-31120
	.value	17416
	.value	-31120
	.value	17426
	.value	-31120
	.value	17432
	.value	-31120
	.value	17480
	.value	-31120
	.value	17484
	.value	-31120
	.value	17538
	.value	-31120
	.value	17544
	.value	-31120
	.value	17550
	.value	-31120
	.value	17554
	.value	-31120
	.value	17626
	.value	-31120
	.value	17604
	.value	-31120
	.value	17632
	.value	-31120
	.value	17634
	.value	-31120
	.value	17640
	.value	-31120
	.value	17642
	.value	-31120
	.value	17648
	.value	-31120
	.value	17650
	.value	-31120
	.value	17664
	.value	-31120
	.value	17666
	.value	-31120
	.value	17856
	.value	-31120
	.value	17858
	.value	-31120
	.value	17672
	.value	-31120
	.value	17674
	.value	-31120
	.value	17680
	.value	-31120
	.value	17682
	.value	-31120
	.value	17860
	.value	-31120
	.value	17862
	.value	-31120
	.value	17752
	.value	-31120
	.value	17754
	.value	-31120
	.value	17756
	.value	-31120
	.value	17758
	.value	-31120
	.value	17876
	.value	-31120
	.value	17878
	.value	-31120
	.value	17880
	.value	-31120
	.value	17882
	.value	-7886
	.value	24872
	.value	-7886
	.value	24728
	.value	-7886
	.value	24732
	.value	-7886
	.value	24736
	.value	-7886
	.value	24740
	.value	-7886
	.value	24744
	.value	-7886
	.value	24748
	.value	-7886
	.value	24752
	.value	-7886
	.value	24756
	.value	-7886
	.value	24760
	.value	-7886
	.value	24764
	.value	-7886
	.value	24768
	.value	-7886
	.value	24772
	.value	-7886
	.value	24778
	.value	-7886
	.value	24782
	.value	-7886
	.value	24786
	.value	24882
	.value	24800
	.value	-7884
	.value	24802
	.value	24882
	.value	24806
	.value	-7884
	.value	24808
	.value	24882
	.value	24812
	.value	-7884
	.value	24814
	.value	24882
	.value	24818
	.value	-7884
	.value	24820
	.value	24882
	.value	24824
	.value	-7884
	.value	24826
	.value	-7886
	.value	24892
	.value	-7886
	.value	25064
	.value	-7886
	.value	24920
	.value	-7886
	.value	24924
	.value	-7886
	.value	24928
	.value	-7886
	.value	24932
	.value	-7886
	.value	24936
	.value	-7886
	.value	24940
	.value	-7886
	.value	24944
	.value	-7886
	.value	24948
	.value	-7886
	.value	24952
	.value	-7886
	.value	24956
	.value	-7886
	.value	24960
	.value	-7886
	.value	24964
	.value	-7886
	.value	24970
	.value	-7886
	.value	24974
	.value	-7886
	.value	24978
	.value	24882
	.value	24992
	.value	-7884
	.value	24994
	.value	24882
	.value	24998
	.value	-7884
	.value	25000
	.value	24882
	.value	25004
	.value	-7884
	.value	25006
	.value	24882
	.value	25010
	.value	-7884
	.value	25012
	.value	24882
	.value	25016
	.value	-7884
	.value	25018
	.value	-7886
	.value	25070
	.value	-7886
	.value	25072
	.value	-7886
	.value	25074
	.value	-7886
	.value	25076
	.value	-7886
	.value	25084
	.value	-19319
	.value	11906
	.value	8500
	.value	-19319
	.value	11906
	.value	8504
	.value	-19319
	.value	11906
	.value	8534
	.value	-19319
	.value	18882
	.value	8796
	.value	-19319
	.value	18882
	.value	8798
	.value	13449
	.value	-12414
	.value	9878
	.value	-19319
	.value	-10814
	.value	9880
	.value	13451
	.value	11266
	.value	10616
	.value	13451
	.value	11906
	.value	10614
	.value	-19317
	.value	12098
	.value	10620
	.value	-19317
	.value	27586
	.value	11124
	.value	-19317
	.value	27586
	.value	11126
	.value	-19315
	.value	19458
	.value	12912
	.value	2
	.value	-6654
	.value	65
	.value	770
	.value	1536
	.value	15692
	.value	1538
	.value	15688
	.value	1542
	.value	15700
	.value	-31214
	.value	15696
	.value	-6654
	.value	65
	.value	776
	.value	-31224
	.value	956
	.value	-6654
	.value	65
	.value	778
	.value	-31230
	.value	1012
	.value	-13822
	.value	67
	.value	807
	.value	-31230
	.value	15376
	.value	-6654
	.value	69
	.value	770
	.value	1536
	.value	15744
	.value	1538
	.value	15740
	.value	1542
	.value	15752
	.value	-31214
	.value	15748
	.value	-6654
	.value	73
	.value	776
	.value	-31230
	.value	15452
	.value	-6654
	.value	79
	.value	770
	.value	1536
	.value	15780
	.value	1538
	.value	15776
	.value	1542
	.value	15788
	.value	-31214
	.value	15784
	.value	-6654
	.value	79
	.value	771
	.value	1538
	.value	15512
	.value	1544
	.value	1112
	.value	-31216
	.value	15516
	.value	-6654
	.value	79
	.value	776
	.value	-31224
	.value	1108
	.value	-6654
	.value	85
	.value	776
	.value	1536
	.value	950
	.value	1538
	.value	942
	.value	1544
	.value	938
	.value	-31208
	.value	946
	.value	-6654
	.value	97
	.value	770
	.value	1536
	.value	15694
	.value	1538
	.value	15690
	.value	1542
	.value	15702
	.value	-31214
	.value	15698
	.value	-6654
	.value	97
	.value	776
	.value	-31224
	.value	958
	.value	-6654
	.value	97
	.value	778
	.value	-31230
	.value	1014
	.value	-13822
	.value	99
	.value	807
	.value	-31230
	.value	15378
	.value	-6654
	.value	101
	.value	770
	.value	1536
	.value	15746
	.value	1538
	.value	15742
	.value	1542
	.value	15754
	.value	-31214
	.value	15750
	.value	-6654
	.value	105
	.value	776
	.value	-31230
	.value	15454
	.value	-6654
	.value	111
	.value	770
	.value	1536
	.value	15782
	.value	1538
	.value	15778
	.value	1542
	.value	15790
	.value	-31214
	.value	15786
	.value	-6654
	.value	111
	.value	771
	.value	1538
	.value	15514
	.value	1544
	.value	1114
	.value	-31216
	.value	15518
	.value	-6654
	.value	111
	.value	776
	.value	-31224
	.value	1110
	.value	-6654
	.value	117
	.value	776
	.value	1536
	.value	952
	.value	1538
	.value	944
	.value	1544
	.value	940
	.value	-31208
	.value	948
	.value	-6654
	.value	65
	.value	774
	.value	1536
	.value	15712
	.value	1538
	.value	15708
	.value	1542
	.value	15720
	.value	-31214
	.value	15716
	.value	-6654
	.value	97
	.value	774
	.value	1536
	.value	15714
	.value	1538
	.value	15710
	.value	1542
	.value	15722
	.value	-31214
	.value	15718
	.value	-6654
	.value	69
	.value	772
	.value	1536
	.value	15400
	.value	-31230
	.value	15404
	.value	-6654
	.value	101
	.value	772
	.value	1536
	.value	15402
	.value	-31230
	.value	15406
	.value	-6654
	.value	79
	.value	772
	.value	1536
	.value	15520
	.value	-31230
	.value	15524
	.value	-6654
	.value	111
	.value	772
	.value	1536
	.value	15522
	.value	-31230
	.value	15526
	.value	-6654
	.value	83
	.value	769
	.value	-31218
	.value	15560
	.value	-6654
	.value	115
	.value	769
	.value	-31218
	.value	15562
	.value	-6654
	.value	83
	.value	780
	.value	-31218
	.value	15564
	.value	-6654
	.value	115
	.value	780
	.value	-31218
	.value	15566
	.value	-6654
	.value	85
	.value	771
	.value	-31230
	.value	15600
	.value	-6654
	.value	117
	.value	771
	.value	-31230
	.value	15602
	.value	-6654
	.value	85
	.value	772
	.value	-31216
	.value	15604
	.value	-6654
	.value	117
	.value	772
	.value	-31216
	.value	15606
	.value	-10238
	.value	79
	.value	795
	.value	1536
	.value	15800
	.value	1538
	.value	15796
	.value	1542
	.value	15808
	.value	1554
	.value	15804
	.value	-31162
	.value	15812
	.value	-10238
	.value	111
	.value	795
	.value	1536
	.value	15802
	.value	1538
	.value	15798
	.value	1542
	.value	15810
	.value	1554
	.value	15806
	.value	-31162
	.value	15814
	.value	-10238
	.value	85
	.value	795
	.value	1536
	.value	15828
	.value	1538
	.value	15824
	.value	1542
	.value	15836
	.value	1554
	.value	15832
	.value	-31162
	.value	15840
	.value	-10238
	.value	117
	.value	795
	.value	1536
	.value	15830
	.value	1538
	.value	15826
	.value	1542
	.value	15838
	.value	1554
	.value	15834
	.value	-31162
	.value	15842
	.value	-13822
	.value	79
	.value	808
	.value	-31224
	.value	984
	.value	-13822
	.value	111
	.value	808
	.value	-31224
	.value	986
	.value	-6654
	.value	65
	.value	775
	.value	-31224
	.value	960
	.value	-6654
	.value	97
	.value	775
	.value	-31224
	.value	962
	.value	-13822
	.value	69
	.value	807
	.value	-31220
	.value	15416
	.value	-13822
	.value	101
	.value	807
	.value	-31220
	.value	15418
	.value	-6654
	.value	79
	.value	775
	.value	-31224
	.value	1120
	.value	-6654
	.value	111
	.value	775
	.value	-31224
	.value	1122
	.value	-6654
	.value	945
	.value	769
	.value	-31094
	.value	16232
	.value	-6654
	.value	951
	.value	769
	.value	-31094
	.value	16264
	.value	-6654
	.value	953
	.value	776
	.value	1536
	.value	16292
	.value	1538
	.value	1824
	.value	-31100
	.value	16302
	.value	-6654
	.value	965
	.value	776
	.value	1536
	.value	16324
	.value	1538
	.value	1888
	.value	-31100
	.value	16334
	.value	-6654
	.value	969
	.value	769
	.value	-31094
	.value	16360
	.value	2
	.value	3270
	.value	3266
	.value	-26198
	.value	6550
	.value	2
	.value	3545
	.value	3535
	.value	-25708
	.value	7098
	.value	-9214
	.value	76
	.value	803
	.value	-31224
	.value	15472
	.value	-9214
	.value	108
	.value	803
	.value	-31224
	.value	15474
	.value	-9214
	.value	82
	.value	803
	.value	-31224
	.value	15544
	.value	-9214
	.value	114
	.value	803
	.value	-31224
	.value	15546
	.value	-9214
	.value	83
	.value	803
	.value	-31218
	.value	15568
	.value	-9214
	.value	115
	.value	803
	.value	-31218
	.value	15570
	.value	-9214
	.value	65
	.value	803
	.value	1540
	.value	15704
	.value	-31220
	.value	15724
	.value	-9214
	.value	97
	.value	803
	.value	1540
	.value	15706
	.value	-31220
	.value	15726
	.value	-9214
	.value	69
	.value	803
	.value	-31228
	.value	15756
	.value	-9214
	.value	101
	.value	803
	.value	-31228
	.value	15758
	.value	-9214
	.value	79
	.value	803
	.value	-31228
	.value	15792
	.value	-9214
	.value	111
	.value	803
	.value	-31228
	.value	15794
	.value	-6654
	.value	945
	.value	787
	.value	1536
	.value	15877
	.value	1538
	.value	15881
	.value	1668
	.value	15885
	.value	-31094
	.value	16128
	.value	-6654
	.value	945
	.value	788
	.value	1536
	.value	15879
	.value	1538
	.value	15883
	.value	1668
	.value	15887
	.value	-31094
	.value	16130
	.value	7936
	.value	-6589
	.value	945
	.value	787
	.value	768
	.value	-31094
	.value	16132
	.value	7937
	.value	-6589
	.value	945
	.value	788
	.value	768
	.value	-31094
	.value	16134
	.value	7936
	.value	-6589
	.value	945
	.value	787
	.value	769
	.value	-31094
	.value	16136
	.value	7937
	.value	-6589
	.value	945
	.value	788
	.value	769
	.value	-31094
	.value	16138
	.value	7936
	.value	-6589
	.value	945
	.value	787
	.value	834
	.value	-31094
	.value	16140
	.value	7937
	.value	-6589
	.value	945
	.value	788
	.value	834
	.value	-31094
	.value	16142
	.value	-6654
	.value	913
	.value	787
	.value	1536
	.value	15893
	.value	1538
	.value	15897
	.value	1668
	.value	15901
	.value	-31094
	.value	16144
	.value	-6654
	.value	913
	.value	788
	.value	1536
	.value	15895
	.value	1538
	.value	15899
	.value	1668
	.value	15903
	.value	-31094
	.value	16146
	.value	7944
	.value	-6589
	.value	913
	.value	787
	.value	768
	.value	-31094
	.value	16148
	.value	7945
	.value	-6589
	.value	913
	.value	788
	.value	768
	.value	-31094
	.value	16150
	.value	7944
	.value	-6589
	.value	913
	.value	787
	.value	769
	.value	-31094
	.value	16152
	.value	7945
	.value	-6589
	.value	913
	.value	788
	.value	769
	.value	-31094
	.value	16154
	.value	7944
	.value	-6589
	.value	913
	.value	787
	.value	834
	.value	-31094
	.value	16156
	.value	7945
	.value	-6589
	.value	913
	.value	788
	.value	834
	.value	-31094
	.value	16158
	.value	-6654
	.value	949
	.value	787
	.value	1536
	.value	15908
	.value	-31230
	.value	15912
	.value	-6654
	.value	949
	.value	788
	.value	1536
	.value	15910
	.value	-31230
	.value	15914
	.value	-6654
	.value	917
	.value	787
	.value	1536
	.value	15924
	.value	-31230
	.value	15928
	.value	-6654
	.value	917
	.value	788
	.value	1536
	.value	15926
	.value	-31230
	.value	15930
	.value	-6654
	.value	951
	.value	787
	.value	1536
	.value	15941
	.value	1538
	.value	15945
	.value	1668
	.value	15949
	.value	-31094
	.value	16160
	.value	-6654
	.value	951
	.value	788
	.value	1536
	.value	15943
	.value	1538
	.value	15947
	.value	1668
	.value	15951
	.value	-31094
	.value	16162
	.value	7968
	.value	-6589
	.value	951
	.value	787
	.value	768
	.value	-31094
	.value	16164
	.value	7969
	.value	-6589
	.value	951
	.value	788
	.value	768
	.value	-31094
	.value	16166
	.value	7968
	.value	-6589
	.value	951
	.value	787
	.value	769
	.value	-31094
	.value	16168
	.value	7969
	.value	-6589
	.value	951
	.value	788
	.value	769
	.value	-31094
	.value	16170
	.value	7968
	.value	-6589
	.value	951
	.value	787
	.value	834
	.value	-31094
	.value	16172
	.value	7969
	.value	-6589
	.value	951
	.value	788
	.value	834
	.value	-31094
	.value	16174
	.value	-6654
	.value	919
	.value	787
	.value	1536
	.value	15957
	.value	1538
	.value	15961
	.value	1668
	.value	15965
	.value	-31094
	.value	16176
	.value	-6654
	.value	919
	.value	788
	.value	1536
	.value	15959
	.value	1538
	.value	15963
	.value	1668
	.value	15967
	.value	-31094
	.value	16178
	.value	7976
	.value	-6589
	.value	919
	.value	787
	.value	768
	.value	-31094
	.value	16180
	.value	7977
	.value	-6589
	.value	919
	.value	788
	.value	768
	.value	-31094
	.value	16182
	.value	7976
	.value	-6589
	.value	919
	.value	787
	.value	769
	.value	-31094
	.value	16184
	.value	7977
	.value	-6589
	.value	919
	.value	788
	.value	769
	.value	-31094
	.value	16186
	.value	7976
	.value	-6589
	.value	919
	.value	787
	.value	834
	.value	-31094
	.value	16188
	.value	7977
	.value	-6589
	.value	919
	.value	788
	.value	834
	.value	-31094
	.value	16190
	.value	-6654
	.value	953
	.value	787
	.value	1536
	.value	15972
	.value	1538
	.value	15976
	.value	-31100
	.value	15980
	.value	-6654
	.value	953
	.value	788
	.value	1536
	.value	15974
	.value	1538
	.value	15978
	.value	-31100
	.value	15982
	.value	-6654
	.value	921
	.value	787
	.value	1536
	.value	15988
	.value	1538
	.value	15992
	.value	-31100
	.value	15996
	.value	-6654
	.value	921
	.value	788
	.value	1536
	.value	15990
	.value	1538
	.value	15994
	.value	-31100
	.value	15998
	.value	-6654
	.value	959
	.value	787
	.value	1536
	.value	16004
	.value	-31230
	.value	16008
	.value	-6654
	.value	959
	.value	788
	.value	1536
	.value	16006
	.value	-31230
	.value	16010
	.value	-6654
	.value	927
	.value	787
	.value	1536
	.value	16020
	.value	-31230
	.value	16024
	.value	-6654
	.value	927
	.value	788
	.value	1536
	.value	16022
	.value	-31230
	.value	16026
	.value	-6654
	.value	965
	.value	787
	.value	1536
	.value	16036
	.value	1538
	.value	16040
	.value	-31100
	.value	16044
	.value	-6654
	.value	965
	.value	788
	.value	1536
	.value	16038
	.value	1538
	.value	16042
	.value	-31100
	.value	16046
	.value	-6654
	.value	933
	.value	788
	.value	1536
	.value	16054
	.value	1538
	.value	16058
	.value	-31100
	.value	16062
	.value	-6654
	.value	969
	.value	787
	.value	1536
	.value	16069
	.value	1538
	.value	16073
	.value	1668
	.value	16077
	.value	-31094
	.value	16192
	.value	-6654
	.value	969
	.value	788
	.value	1536
	.value	16071
	.value	1538
	.value	16075
	.value	1668
	.value	16079
	.value	-31094
	.value	16194
	.value	8032
	.value	-6589
	.value	969
	.value	787
	.value	768
	.value	-31094
	.value	16196
	.value	8033
	.value	-6589
	.value	969
	.value	788
	.value	768
	.value	-31094
	.value	16198
	.value	8032
	.value	-6589
	.value	969
	.value	787
	.value	769
	.value	-31094
	.value	16200
	.value	8033
	.value	-6589
	.value	969
	.value	788
	.value	769
	.value	-31094
	.value	16202
	.value	8032
	.value	-6589
	.value	969
	.value	787
	.value	834
	.value	-31094
	.value	16204
	.value	8033
	.value	-6589
	.value	969
	.value	788
	.value	834
	.value	-31094
	.value	16206
	.value	-6654
	.value	937
	.value	787
	.value	1536
	.value	16085
	.value	1538
	.value	16089
	.value	1668
	.value	16093
	.value	-31094
	.value	16208
	.value	-6654
	.value	937
	.value	788
	.value	1536
	.value	16087
	.value	1538
	.value	16091
	.value	1668
	.value	16095
	.value	-31094
	.value	16210
	.value	8040
	.value	-6589
	.value	937
	.value	787
	.value	768
	.value	-31094
	.value	16212
	.value	8041
	.value	-6589
	.value	937
	.value	788
	.value	768
	.value	-31094
	.value	16214
	.value	8040
	.value	-6589
	.value	937
	.value	787
	.value	769
	.value	-31094
	.value	16216
	.value	8041
	.value	-6589
	.value	937
	.value	788
	.value	769
	.value	-31094
	.value	16218
	.value	8040
	.value	-6589
	.value	937
	.value	787
	.value	834
	.value	-31094
	.value	16220
	.value	8041
	.value	-6589
	.value	937
	.value	788
	.value	834
	.value	-31094
	.value	16222
	.value	-6654
	.value	945
	.value	768
	.value	-31094
	.value	16228
	.value	-6654
	.value	951
	.value	768
	.value	-31094
	.value	16260
	.value	-6654
	.value	969
	.value	768
	.value	-31094
	.value	16356
	.value	-6654
	.value	945
	.value	834
	.value	-31094
	.value	16238
	.value	-6654
	.value	951
	.value	834
	.value	-31094
	.value	16270
	.value	-6654
	.value	969
	.value	834
	.value	-31094
	.value	16366
	.value	3
	.value	-6654
	.value	65
	.value	768
	.value	-6654
	.value	65
	.value	769
	.value	-6654
	.value	65
	.value	771
	.value	-6654
	.value	69
	.value	768
	.value	-6654
	.value	69
	.value	769
	.value	-6654
	.value	69
	.value	776
	.value	-6654
	.value	73
	.value	768
	.value	-6654
	.value	73
	.value	769
	.value	-6654
	.value	73
	.value	770
	.value	-6654
	.value	78
	.value	771
	.value	-6654
	.value	79
	.value	768
	.value	-6654
	.value	79
	.value	769
	.value	-6654
	.value	85
	.value	768
	.value	-6654
	.value	85
	.value	769
	.value	-6654
	.value	85
	.value	770
	.value	-6654
	.value	89
	.value	769
	.value	-6654
	.value	97
	.value	768
	.value	-6654
	.value	97
	.value	769
	.value	-6654
	.value	97
	.value	771
	.value	-6654
	.value	101
	.value	768
	.value	-6654
	.value	101
	.value	769
	.value	-6654
	.value	101
	.value	776
	.value	-6654
	.value	105
	.value	768
	.value	-6654
	.value	105
	.value	769
	.value	-6654
	.value	105
	.value	770
	.value	-6654
	.value	110
	.value	771
	.value	-6654
	.value	111
	.value	768
	.value	-6654
	.value	111
	.value	769
	.value	-6654
	.value	117
	.value	768
	.value	-6654
	.value	117
	.value	769
	.value	-6654
	.value	117
	.value	770
	.value	-6654
	.value	121
	.value	769
	.value	-6654
	.value	121
	.value	776
	.value	-6654
	.value	65
	.value	772
	.value	-6654
	.value	97
	.value	772
	.value	-13822
	.value	65
	.value	808
	.value	-13822
	.value	97
	.value	808
	.value	-6654
	.value	67
	.value	769
	.value	-6654
	.value	99
	.value	769
	.value	-6654
	.value	67
	.value	770
	.value	-6654
	.value	99
	.value	770
	.value	-6654
	.value	67
	.value	775
	.value	-6654
	.value	99
	.value	775
	.value	-6654
	.value	67
	.value	780
	.value	-6654
	.value	99
	.value	780
	.value	-6654
	.value	68
	.value	780
	.value	-6654
	.value	100
	.value	780
	.value	-6654
	.value	69
	.value	774
	.value	-6654
	.value	101
	.value	774
	.value	-6654
	.value	69
	.value	775
	.value	-6654
	.value	101
	.value	775
	.value	-13822
	.value	69
	.value	808
	.value	-13822
	.value	101
	.value	808
	.value	-6654
	.value	69
	.value	780
	.value	-6654
	.value	101
	.value	780
	.value	-6654
	.value	71
	.value	770
	.value	-6654
	.value	103
	.value	770
	.value	-6654
	.value	71
	.value	774
	.value	-6654
	.value	103
	.value	774
	.value	-6654
	.value	71
	.value	775
	.value	-6654
	.value	103
	.value	775
	.value	-13822
	.value	71
	.value	807
	.value	-13822
	.value	103
	.value	807
	.value	-6654
	.value	72
	.value	770
	.value	-6654
	.value	104
	.value	770
	.value	-6654
	.value	73
	.value	771
	.value	-6654
	.value	105
	.value	771
	.value	-6654
	.value	73
	.value	772
	.value	-6654
	.value	105
	.value	772
	.value	-6654
	.value	73
	.value	774
	.value	-6654
	.value	105
	.value	774
	.value	-13822
	.value	73
	.value	808
	.value	-13822
	.value	105
	.value	808
	.value	-6654
	.value	73
	.value	775
	.value	-6654
	.value	74
	.value	770
	.value	-6654
	.value	106
	.value	770
	.value	-13822
	.value	75
	.value	807
	.value	-13822
	.value	107
	.value	807
	.value	-6654
	.value	76
	.value	769
	.value	-6654
	.value	108
	.value	769
	.value	-13822
	.value	76
	.value	807
	.value	-13822
	.value	108
	.value	807
	.value	-6654
	.value	76
	.value	780
	.value	-6654
	.value	108
	.value	780
	.value	-6654
	.value	78
	.value	769
	.value	-6654
	.value	110
	.value	769
	.value	-13822
	.value	78
	.value	807
	.value	-13822
	.value	110
	.value	807
	.value	-6654
	.value	78
	.value	780
	.value	-6654
	.value	110
	.value	780
	.value	-6654
	.value	79
	.value	774
	.value	-6654
	.value	111
	.value	774
	.value	-6654
	.value	79
	.value	779
	.value	-6654
	.value	111
	.value	779
	.value	-6654
	.value	82
	.value	769
	.value	-6654
	.value	114
	.value	769
	.value	-13822
	.value	82
	.value	807
	.value	-13822
	.value	114
	.value	807
	.value	-6654
	.value	82
	.value	780
	.value	-6654
	.value	114
	.value	780
	.value	-6654
	.value	83
	.value	770
	.value	-6654
	.value	115
	.value	770
	.value	-13822
	.value	83
	.value	807
	.value	-13822
	.value	115
	.value	807
	.value	-13822
	.value	84
	.value	807
	.value	-13822
	.value	116
	.value	807
	.value	-6654
	.value	84
	.value	780
	.value	-6654
	.value	116
	.value	780
	.value	-6654
	.value	85
	.value	774
	.value	-6654
	.value	117
	.value	774
	.value	-6654
	.value	85
	.value	778
	.value	-6654
	.value	117
	.value	778
	.value	-6654
	.value	85
	.value	779
	.value	-6654
	.value	117
	.value	779
	.value	-13822
	.value	85
	.value	808
	.value	-13822
	.value	117
	.value	808
	.value	-6654
	.value	87
	.value	770
	.value	-6654
	.value	119
	.value	770
	.value	-6654
	.value	89
	.value	770
	.value	-6654
	.value	121
	.value	770
	.value	-6654
	.value	89
	.value	776
	.value	-6654
	.value	90
	.value	769
	.value	-6654
	.value	122
	.value	769
	.value	-6654
	.value	90
	.value	775
	.value	-6654
	.value	122
	.value	775
	.value	-6654
	.value	90
	.value	780
	.value	-6654
	.value	122
	.value	780
	.value	-6654
	.value	65
	.value	780
	.value	-6654
	.value	97
	.value	780
	.value	-6654
	.value	73
	.value	780
	.value	-6654
	.value	105
	.value	780
	.value	-6654
	.value	79
	.value	780
	.value	-6654
	.value	111
	.value	780
	.value	-6654
	.value	85
	.value	780
	.value	-6654
	.value	117
	.value	780
	.value	220
	.value	-6589
	.value	85
	.value	776
	.value	772
	.value	252
	.value	-6589
	.value	117
	.value	776
	.value	772
	.value	220
	.value	-6589
	.value	85
	.value	776
	.value	769
	.value	252
	.value	-6589
	.value	117
	.value	776
	.value	769
	.value	220
	.value	-6589
	.value	85
	.value	776
	.value	780
	.value	252
	.value	-6589
	.value	117
	.value	776
	.value	780
	.value	220
	.value	-6589
	.value	85
	.value	776
	.value	768
	.value	252
	.value	-6589
	.value	117
	.value	776
	.value	768
	.value	196
	.value	-6589
	.value	65
	.value	776
	.value	772
	.value	228
	.value	-6589
	.value	97
	.value	776
	.value	772
	.value	550
	.value	-6589
	.value	65
	.value	775
	.value	772
	.value	551
	.value	-6589
	.value	97
	.value	775
	.value	772
	.value	-6654
	.value	198
	.value	772
	.value	-6654
	.value	230
	.value	772
	.value	-6654
	.value	71
	.value	780
	.value	-6654
	.value	103
	.value	780
	.value	-6654
	.value	75
	.value	780
	.value	-6654
	.value	107
	.value	780
	.value	490
	.value	-6589
	.value	79
	.value	808
	.value	772
	.value	491
	.value	-6589
	.value	111
	.value	808
	.value	772
	.value	-6654
	.value	439
	.value	780
	.value	-6654
	.value	658
	.value	780
	.value	-6654
	.value	106
	.value	780
	.value	-6654
	.value	71
	.value	769
	.value	-6654
	.value	103
	.value	769
	.value	-6654
	.value	78
	.value	768
	.value	-6654
	.value	110
	.value	768
	.value	197
	.value	-6589
	.value	65
	.value	778
	.value	769
	.value	229
	.value	-6589
	.value	97
	.value	778
	.value	769
	.value	-6654
	.value	198
	.value	769
	.value	-6654
	.value	230
	.value	769
	.value	-6654
	.value	216
	.value	769
	.value	-6654
	.value	248
	.value	769
	.value	-6654
	.value	65
	.value	783
	.value	-6654
	.value	97
	.value	783
	.value	-6654
	.value	65
	.value	785
	.value	-6654
	.value	97
	.value	785
	.value	-6654
	.value	69
	.value	783
	.value	-6654
	.value	101
	.value	783
	.value	-6654
	.value	69
	.value	785
	.value	-6654
	.value	101
	.value	785
	.value	-6654
	.value	73
	.value	783
	.value	-6654
	.value	105
	.value	783
	.value	-6654
	.value	73
	.value	785
	.value	-6654
	.value	105
	.value	785
	.value	-6654
	.value	79
	.value	783
	.value	-6654
	.value	111
	.value	783
	.value	-6654
	.value	79
	.value	785
	.value	-6654
	.value	111
	.value	785
	.value	-6654
	.value	82
	.value	783
	.value	-6654
	.value	114
	.value	783
	.value	-6654
	.value	82
	.value	785
	.value	-6654
	.value	114
	.value	785
	.value	-6654
	.value	85
	.value	783
	.value	-6654
	.value	117
	.value	783
	.value	-6654
	.value	85
	.value	785
	.value	-6654
	.value	117
	.value	785
	.value	-9214
	.value	83
	.value	806
	.value	-9214
	.value	115
	.value	806
	.value	-9214
	.value	84
	.value	806
	.value	-9214
	.value	116
	.value	806
	.value	-6654
	.value	72
	.value	780
	.value	-6654
	.value	104
	.value	780
	.value	214
	.value	-6589
	.value	79
	.value	776
	.value	772
	.value	246
	.value	-6589
	.value	111
	.value	776
	.value	772
	.value	213
	.value	-6589
	.value	79
	.value	771
	.value	772
	.value	245
	.value	-6589
	.value	111
	.value	771
	.value	772
	.value	558
	.value	-6589
	.value	79
	.value	775
	.value	772
	.value	559
	.value	-6589
	.value	111
	.value	775
	.value	772
	.value	-6654
	.value	89
	.value	772
	.value	-6654
	.value	121
	.value	772
	.value	-6654
	.value	168
	.value	769
	.value	-6654
	.value	913
	.value	769
	.value	-6654
	.value	917
	.value	769
	.value	-6654
	.value	919
	.value	769
	.value	-6654
	.value	921
	.value	769
	.value	-6654
	.value	927
	.value	769
	.value	-6654
	.value	933
	.value	769
	.value	-6654
	.value	937
	.value	769
	.value	970
	.value	-6589
	.value	953
	.value	776
	.value	769
	.value	-6654
	.value	921
	.value	776
	.value	-6654
	.value	933
	.value	776
	.value	-6654
	.value	949
	.value	769
	.value	-6654
	.value	953
	.value	769
	.value	971
	.value	-6589
	.value	965
	.value	776
	.value	769
	.value	-6654
	.value	959
	.value	769
	.value	-6654
	.value	965
	.value	769
	.value	-6654
	.value	978
	.value	769
	.value	-6654
	.value	978
	.value	776
	.value	-6654
	.value	1045
	.value	768
	.value	-6654
	.value	1045
	.value	776
	.value	-6654
	.value	1043
	.value	769
	.value	-6654
	.value	1030
	.value	776
	.value	-6654
	.value	1050
	.value	769
	.value	-6654
	.value	1048
	.value	768
	.value	-6654
	.value	1059
	.value	774
	.value	-6654
	.value	1048
	.value	774
	.value	-6654
	.value	1080
	.value	774
	.value	-6654
	.value	1077
	.value	768
	.value	-6654
	.value	1077
	.value	776
	.value	-6654
	.value	1075
	.value	769
	.value	-6654
	.value	1110
	.value	776
	.value	-6654
	.value	1082
	.value	769
	.value	-6654
	.value	1080
	.value	768
	.value	-6654
	.value	1091
	.value	774
	.value	-6654
	.value	1140
	.value	783
	.value	-6654
	.value	1141
	.value	783
	.value	-6654
	.value	1046
	.value	774
	.value	-6654
	.value	1078
	.value	774
	.value	-6654
	.value	1040
	.value	774
	.value	-6654
	.value	1072
	.value	774
	.value	-6654
	.value	1040
	.value	776
	.value	-6654
	.value	1072
	.value	776
	.value	-6654
	.value	1045
	.value	774
	.value	-6654
	.value	1077
	.value	774
	.value	-6654
	.value	1240
	.value	776
	.value	-6654
	.value	1241
	.value	776
	.value	-6654
	.value	1046
	.value	776
	.value	-6654
	.value	1078
	.value	776
	.value	-6654
	.value	1047
	.value	776
	.value	-6654
	.value	1079
	.value	776
	.value	-6654
	.value	1048
	.value	772
	.value	-6654
	.value	1080
	.value	772
	.value	-6654
	.value	1048
	.value	776
	.value	-6654
	.value	1080
	.value	776
	.value	-6654
	.value	1054
	.value	776
	.value	-6654
	.value	1086
	.value	776
	.value	-6654
	.value	1256
	.value	776
	.value	-6654
	.value	1257
	.value	776
	.value	-6654
	.value	1069
	.value	776
	.value	-6654
	.value	1101
	.value	776
	.value	-6654
	.value	1059
	.value	772
	.value	-6654
	.value	1091
	.value	772
	.value	-6654
	.value	1059
	.value	776
	.value	-6654
	.value	1091
	.value	776
	.value	-6654
	.value	1059
	.value	779
	.value	-6654
	.value	1091
	.value	779
	.value	-6654
	.value	1063
	.value	776
	.value	-6654
	.value	1095
	.value	776
	.value	-6654
	.value	1067
	.value	776
	.value	-6654
	.value	1099
	.value	776
	.value	-6654
	.value	1575
	.value	1619
	.value	-6654
	.value	1575
	.value	1620
	.value	-6654
	.value	1608
	.value	1620
	.value	-9214
	.value	1575
	.value	1621
	.value	-6654
	.value	1610
	.value	1620
	.value	-6654
	.value	1749
	.value	1620
	.value	-6654
	.value	1729
	.value	1620
	.value	-6654
	.value	1746
	.value	1620
	.value	1794
	.value	2344
	.value	2364
	.value	1794
	.value	2352
	.value	2364
	.value	1794
	.value	2355
	.value	2364
	.value	2
	.value	2503
	.value	2494
	.value	2
	.value	2503
	.value	2519
	.value	2
	.value	2887
	.value	2902
	.value	2
	.value	2887
	.value	2878
	.value	2
	.value	2887
	.value	2903
	.value	2
	.value	2962
	.value	3031
	.value	2
	.value	3014
	.value	3006
	.value	2
	.value	3015
	.value	3006
	.value	2
	.value	3014
	.value	3031
	.value	23298
	.value	3142
	.value	3158
	.value	2
	.value	3263
	.value	3285
	.value	2
	.value	3270
	.value	3285
	.value	2
	.value	3270
	.value	3286
	.value	3274
	.value	67
	.value	3270
	.value	3266
	.value	3285
	.value	2
	.value	3398
	.value	3390
	.value	2
	.value	3399
	.value	3390
	.value	2
	.value	3398
	.value	3415
	.value	2306
	.value	3545
	.value	3530
	.value	3548
	.value	2371
	.value	3545
	.value	3535
	.value	3530
	.value	2
	.value	3545
	.value	3551
	.value	2
	.value	4133
	.value	4142
	.value	2
	.value	6917
	.value	6965
	.value	2
	.value	6919
	.value	6965
	.value	2
	.value	6921
	.value	6965
	.value	2
	.value	6923
	.value	6965
	.value	2
	.value	6925
	.value	6965
	.value	2
	.value	6929
	.value	6965
	.value	2
	.value	6970
	.value	6965
	.value	2
	.value	6972
	.value	6965
	.value	2
	.value	6974
	.value	6965
	.value	2
	.value	6975
	.value	6965
	.value	2
	.value	6978
	.value	6965
	.value	-9214
	.value	65
	.value	805
	.value	-9214
	.value	97
	.value	805
	.value	-6654
	.value	66
	.value	775
	.value	-6654
	.value	98
	.value	775
	.value	-9214
	.value	66
	.value	803
	.value	-9214
	.value	98
	.value	803
	.value	-9214
	.value	66
	.value	817
	.value	-9214
	.value	98
	.value	817
	.value	199
	.value	-6589
	.value	67
	.value	807
	.value	769
	.value	231
	.value	-6589
	.value	99
	.value	807
	.value	769
	.value	-6654
	.value	68
	.value	775
	.value	-6654
	.value	100
	.value	775
	.value	-9214
	.value	68
	.value	803
	.value	-9214
	.value	100
	.value	803
	.value	-9214
	.value	68
	.value	817
	.value	-9214
	.value	100
	.value	817
	.value	-13822
	.value	68
	.value	807
	.value	-13822
	.value	100
	.value	807
	.value	-9214
	.value	68
	.value	813
	.value	-9214
	.value	100
	.value	813
	.value	274
	.value	-6589
	.value	69
	.value	772
	.value	768
	.value	275
	.value	-6589
	.value	101
	.value	772
	.value	768
	.value	274
	.value	-6589
	.value	69
	.value	772
	.value	769
	.value	275
	.value	-6589
	.value	101
	.value	772
	.value	769
	.value	-9214
	.value	69
	.value	813
	.value	-9214
	.value	101
	.value	813
	.value	-9214
	.value	69
	.value	816
	.value	-9214
	.value	101
	.value	816
	.value	552
	.value	-6589
	.value	69
	.value	807
	.value	774
	.value	553
	.value	-6589
	.value	101
	.value	807
	.value	774
	.value	-6654
	.value	70
	.value	775
	.value	-6654
	.value	102
	.value	775
	.value	-6654
	.value	71
	.value	772
	.value	-6654
	.value	103
	.value	772
	.value	-6654
	.value	72
	.value	775
	.value	-6654
	.value	104
	.value	775
	.value	-9214
	.value	72
	.value	803
	.value	-9214
	.value	104
	.value	803
	.value	-6654
	.value	72
	.value	776
	.value	-6654
	.value	104
	.value	776
	.value	-13822
	.value	72
	.value	807
	.value	-13822
	.value	104
	.value	807
	.value	-9214
	.value	72
	.value	814
	.value	-9214
	.value	104
	.value	814
	.value	-9214
	.value	73
	.value	816
	.value	-9214
	.value	105
	.value	816
	.value	207
	.value	-6589
	.value	73
	.value	776
	.value	769
	.value	239
	.value	-6589
	.value	105
	.value	776
	.value	769
	.value	-6654
	.value	75
	.value	769
	.value	-6654
	.value	107
	.value	769
	.value	-9214
	.value	75
	.value	803
	.value	-9214
	.value	107
	.value	803
	.value	-9214
	.value	75
	.value	817
	.value	-9214
	.value	107
	.value	817
	.value	7734
	.value	-6589
	.value	76
	.value	803
	.value	772
	.value	7735
	.value	-6589
	.value	108
	.value	803
	.value	772
	.value	-9214
	.value	76
	.value	817
	.value	-9214
	.value	108
	.value	817
	.value	-9214
	.value	76
	.value	813
	.value	-9214
	.value	108
	.value	813
	.value	-6654
	.value	77
	.value	769
	.value	-6654
	.value	109
	.value	769
	.value	-6654
	.value	77
	.value	775
	.value	-6654
	.value	109
	.value	775
	.value	-9214
	.value	77
	.value	803
	.value	-9214
	.value	109
	.value	803
	.value	-6654
	.value	78
	.value	775
	.value	-6654
	.value	110
	.value	775
	.value	-9214
	.value	78
	.value	803
	.value	-9214
	.value	110
	.value	803
	.value	-9214
	.value	78
	.value	817
	.value	-9214
	.value	110
	.value	817
	.value	-9214
	.value	78
	.value	813
	.value	-9214
	.value	110
	.value	813
	.value	213
	.value	-6589
	.value	79
	.value	771
	.value	769
	.value	245
	.value	-6589
	.value	111
	.value	771
	.value	769
	.value	213
	.value	-6589
	.value	79
	.value	771
	.value	776
	.value	245
	.value	-6589
	.value	111
	.value	771
	.value	776
	.value	332
	.value	-6589
	.value	79
	.value	772
	.value	768
	.value	333
	.value	-6589
	.value	111
	.value	772
	.value	768
	.value	332
	.value	-6589
	.value	79
	.value	772
	.value	769
	.value	333
	.value	-6589
	.value	111
	.value	772
	.value	769
	.value	-6654
	.value	80
	.value	769
	.value	-6654
	.value	112
	.value	769
	.value	-6654
	.value	80
	.value	775
	.value	-6654
	.value	112
	.value	775
	.value	-6654
	.value	82
	.value	775
	.value	-6654
	.value	114
	.value	775
	.value	7770
	.value	-6589
	.value	82
	.value	803
	.value	772
	.value	7771
	.value	-6589
	.value	114
	.value	803
	.value	772
	.value	-9214
	.value	82
	.value	817
	.value	-9214
	.value	114
	.value	817
	.value	-6654
	.value	83
	.value	775
	.value	-6654
	.value	115
	.value	775
	.value	346
	.value	-6589
	.value	83
	.value	769
	.value	775
	.value	347
	.value	-6589
	.value	115
	.value	769
	.value	775
	.value	352
	.value	-6589
	.value	83
	.value	780
	.value	775
	.value	353
	.value	-6589
	.value	115
	.value	780
	.value	775
	.value	7778
	.value	-6589
	.value	83
	.value	803
	.value	775
	.value	7779
	.value	-6589
	.value	115
	.value	803
	.value	775
	.value	-6654
	.value	84
	.value	775
	.value	-6654
	.value	116
	.value	775
	.value	-9214
	.value	84
	.value	803
	.value	-9214
	.value	116
	.value	803
	.value	-9214
	.value	84
	.value	817
	.value	-9214
	.value	116
	.value	817
	.value	-9214
	.value	84
	.value	813
	.value	-9214
	.value	116
	.value	813
	.value	-9214
	.value	85
	.value	804
	.value	-9214
	.value	117
	.value	804
	.value	-9214
	.value	85
	.value	816
	.value	-9214
	.value	117
	.value	816
	.value	-9214
	.value	85
	.value	813
	.value	-9214
	.value	117
	.value	813
	.value	360
	.value	-6589
	.value	85
	.value	771
	.value	769
	.value	361
	.value	-6589
	.value	117
	.value	771
	.value	769
	.value	362
	.value	-6589
	.value	85
	.value	772
	.value	776
	.value	363
	.value	-6589
	.value	117
	.value	772
	.value	776
	.value	-6654
	.value	86
	.value	771
	.value	-6654
	.value	118
	.value	771
	.value	-9214
	.value	86
	.value	803
	.value	-9214
	.value	118
	.value	803
	.value	-6654
	.value	87
	.value	768
	.value	-6654
	.value	119
	.value	768
	.value	-6654
	.value	87
	.value	769
	.value	-6654
	.value	119
	.value	769
	.value	-6654
	.value	87
	.value	776
	.value	-6654
	.value	119
	.value	776
	.value	-6654
	.value	87
	.value	775
	.value	-6654
	.value	119
	.value	775
	.value	-9214
	.value	87
	.value	803
	.value	-9214
	.value	119
	.value	803
	.value	-6654
	.value	88
	.value	775
	.value	-6654
	.value	120
	.value	775
	.value	-6654
	.value	88
	.value	776
	.value	-6654
	.value	120
	.value	776
	.value	-6654
	.value	89
	.value	775
	.value	-6654
	.value	121
	.value	775
	.value	-6654
	.value	90
	.value	770
	.value	-6654
	.value	122
	.value	770
	.value	-9214
	.value	90
	.value	803
	.value	-9214
	.value	122
	.value	803
	.value	-9214
	.value	90
	.value	817
	.value	-9214
	.value	122
	.value	817
	.value	-9214
	.value	104
	.value	817
	.value	-6654
	.value	116
	.value	776
	.value	-6654
	.value	119
	.value	778
	.value	-6654
	.value	121
	.value	778
	.value	-6654
	.value	383
	.value	775
	.value	-6654
	.value	65
	.value	777
	.value	-6654
	.value	97
	.value	777
	.value	194
	.value	-6589
	.value	65
	.value	770
	.value	769
	.value	226
	.value	-6589
	.value	97
	.value	770
	.value	769
	.value	194
	.value	-6589
	.value	65
	.value	770
	.value	768
	.value	226
	.value	-6589
	.value	97
	.value	770
	.value	768
	.value	194
	.value	-6589
	.value	65
	.value	770
	.value	777
	.value	226
	.value	-6589
	.value	97
	.value	770
	.value	777
	.value	194
	.value	-6589
	.value	65
	.value	770
	.value	771
	.value	226
	.value	-6589
	.value	97
	.value	770
	.value	771
	.value	7840
	.value	-6589
	.value	65
	.value	803
	.value	770
	.value	7841
	.value	-6589
	.value	97
	.value	803
	.value	770
	.value	258
	.value	-6589
	.value	65
	.value	774
	.value	769
	.value	259
	.value	-6589
	.value	97
	.value	774
	.value	769
	.value	258
	.value	-6589
	.value	65
	.value	774
	.value	768
	.value	259
	.value	-6589
	.value	97
	.value	774
	.value	768
	.value	258
	.value	-6589
	.value	65
	.value	774
	.value	777
	.value	259
	.value	-6589
	.value	97
	.value	774
	.value	777
	.value	258
	.value	-6589
	.value	65
	.value	774
	.value	771
	.value	259
	.value	-6589
	.value	97
	.value	774
	.value	771
	.value	7840
	.value	-6589
	.value	65
	.value	803
	.value	774
	.value	7841
	.value	-6589
	.value	97
	.value	803
	.value	774
	.value	-6654
	.value	69
	.value	777
	.value	-6654
	.value	101
	.value	777
	.value	-6654
	.value	69
	.value	771
	.value	-6654
	.value	101
	.value	771
	.value	202
	.value	-6589
	.value	69
	.value	770
	.value	769
	.value	234
	.value	-6589
	.value	101
	.value	770
	.value	769
	.value	202
	.value	-6589
	.value	69
	.value	770
	.value	768
	.value	234
	.value	-6589
	.value	101
	.value	770
	.value	768
	.value	202
	.value	-6589
	.value	69
	.value	770
	.value	777
	.value	234
	.value	-6589
	.value	101
	.value	770
	.value	777
	.value	202
	.value	-6589
	.value	69
	.value	770
	.value	771
	.value	234
	.value	-6589
	.value	101
	.value	770
	.value	771
	.value	7864
	.value	-6589
	.value	69
	.value	803
	.value	770
	.value	7865
	.value	-6589
	.value	101
	.value	803
	.value	770
	.value	-6654
	.value	73
	.value	777
	.value	-6654
	.value	105
	.value	777
	.value	-9214
	.value	73
	.value	803
	.value	-9214
	.value	105
	.value	803
	.value	-6654
	.value	79
	.value	777
	.value	-6654
	.value	111
	.value	777
	.value	212
	.value	-6589
	.value	79
	.value	770
	.value	769
	.value	244
	.value	-6589
	.value	111
	.value	770
	.value	769
	.value	212
	.value	-6589
	.value	79
	.value	770
	.value	768
	.value	244
	.value	-6589
	.value	111
	.value	770
	.value	768
	.value	212
	.value	-6589
	.value	79
	.value	770
	.value	777
	.value	244
	.value	-6589
	.value	111
	.value	770
	.value	777
	.value	212
	.value	-6589
	.value	79
	.value	770
	.value	771
	.value	244
	.value	-6589
	.value	111
	.value	770
	.value	771
	.value	7884
	.value	-6589
	.value	79
	.value	803
	.value	770
	.value	7885
	.value	-6589
	.value	111
	.value	803
	.value	770
	.value	416
	.value	-6589
	.value	79
	.value	795
	.value	769
	.value	417
	.value	-6589
	.value	111
	.value	795
	.value	769
	.value	416
	.value	-6589
	.value	79
	.value	795
	.value	768
	.value	417
	.value	-6589
	.value	111
	.value	795
	.value	768
	.value	416
	.value	-6589
	.value	79
	.value	795
	.value	777
	.value	417
	.value	-6589
	.value	111
	.value	795
	.value	777
	.value	416
	.value	-6589
	.value	79
	.value	795
	.value	771
	.value	417
	.value	-6589
	.value	111
	.value	795
	.value	771
	.value	416
	.value	-9149
	.value	79
	.value	795
	.value	803
	.value	417
	.value	-9149
	.value	111
	.value	795
	.value	803
	.value	-9214
	.value	85
	.value	803
	.value	-9214
	.value	117
	.value	803
	.value	-6654
	.value	85
	.value	777
	.value	-6654
	.value	117
	.value	777
	.value	431
	.value	-6589
	.value	85
	.value	795
	.value	769
	.value	432
	.value	-6589
	.value	117
	.value	795
	.value	769
	.value	431
	.value	-6589
	.value	85
	.value	795
	.value	768
	.value	432
	.value	-6589
	.value	117
	.value	795
	.value	768
	.value	431
	.value	-6589
	.value	85
	.value	795
	.value	777
	.value	432
	.value	-6589
	.value	117
	.value	795
	.value	777
	.value	431
	.value	-6589
	.value	85
	.value	795
	.value	771
	.value	432
	.value	-6589
	.value	117
	.value	795
	.value	771
	.value	431
	.value	-9149
	.value	85
	.value	795
	.value	803
	.value	432
	.value	-9149
	.value	117
	.value	795
	.value	803
	.value	-6654
	.value	89
	.value	768
	.value	-6654
	.value	121
	.value	768
	.value	-9214
	.value	89
	.value	803
	.value	-9214
	.value	121
	.value	803
	.value	-6654
	.value	89
	.value	777
	.value	-6654
	.value	121
	.value	777
	.value	-6654
	.value	89
	.value	771
	.value	-6654
	.value	121
	.value	771
	.value	7952
	.value	-6589
	.value	949
	.value	787
	.value	768
	.value	7953
	.value	-6589
	.value	949
	.value	788
	.value	768
	.value	7952
	.value	-6589
	.value	949
	.value	787
	.value	769
	.value	7953
	.value	-6589
	.value	949
	.value	788
	.value	769
	.value	7960
	.value	-6589
	.value	917
	.value	787
	.value	768
	.value	7961
	.value	-6589
	.value	917
	.value	788
	.value	768
	.value	7960
	.value	-6589
	.value	917
	.value	787
	.value	769
	.value	7961
	.value	-6589
	.value	917
	.value	788
	.value	769
	.value	7984
	.value	-6589
	.value	953
	.value	787
	.value	768
	.value	7985
	.value	-6589
	.value	953
	.value	788
	.value	768
	.value	7984
	.value	-6589
	.value	953
	.value	787
	.value	769
	.value	7985
	.value	-6589
	.value	953
	.value	788
	.value	769
	.value	7984
	.value	-6589
	.value	953
	.value	787
	.value	834
	.value	7985
	.value	-6589
	.value	953
	.value	788
	.value	834
	.value	7992
	.value	-6589
	.value	921
	.value	787
	.value	768
	.value	7993
	.value	-6589
	.value	921
	.value	788
	.value	768
	.value	7992
	.value	-6589
	.value	921
	.value	787
	.value	769
	.value	7993
	.value	-6589
	.value	921
	.value	788
	.value	769
	.value	7992
	.value	-6589
	.value	921
	.value	787
	.value	834
	.value	7993
	.value	-6589
	.value	921
	.value	788
	.value	834
	.value	8000
	.value	-6589
	.value	959
	.value	787
	.value	768
	.value	8001
	.value	-6589
	.value	959
	.value	788
	.value	768
	.value	8000
	.value	-6589
	.value	959
	.value	787
	.value	769
	.value	8001
	.value	-6589
	.value	959
	.value	788
	.value	769
	.value	8008
	.value	-6589
	.value	927
	.value	787
	.value	768
	.value	8009
	.value	-6589
	.value	927
	.value	788
	.value	768
	.value	8008
	.value	-6589
	.value	927
	.value	787
	.value	769
	.value	8009
	.value	-6589
	.value	927
	.value	788
	.value	769
	.value	8016
	.value	-6589
	.value	965
	.value	787
	.value	768
	.value	8017
	.value	-6589
	.value	965
	.value	788
	.value	768
	.value	8016
	.value	-6589
	.value	965
	.value	787
	.value	769
	.value	8017
	.value	-6589
	.value	965
	.value	788
	.value	769
	.value	8016
	.value	-6589
	.value	965
	.value	787
	.value	834
	.value	8017
	.value	-6589
	.value	965
	.value	788
	.value	834
	.value	8025
	.value	-6589
	.value	933
	.value	788
	.value	768
	.value	8025
	.value	-6589
	.value	933
	.value	788
	.value	769
	.value	8025
	.value	-6589
	.value	933
	.value	788
	.value	834
	.value	-6654
	.value	949
	.value	768
	.value	-6654
	.value	953
	.value	768
	.value	-6654
	.value	959
	.value	768
	.value	-6654
	.value	965
	.value	768
	.value	7936
	.value	-4029
	.value	945
	.value	787
	.value	837
	.value	7937
	.value	-4029
	.value	945
	.value	788
	.value	837
	.value	7938
	.value	837
	.value	2
	.value	-4028
	.value	945
	.value	787
	.value	768
	.value	837
	.value	7939
	.value	837
	.value	2
	.value	-4028
	.value	945
	.value	788
	.value	768
	.value	837
	.value	7940
	.value	837
	.value	2
	.value	-4028
	.value	945
	.value	787
	.value	769
	.value	837
	.value	7941
	.value	837
	.value	2
	.value	-4028
	.value	945
	.value	788
	.value	769
	.value	837
	.value	7942
	.value	837
	.value	2
	.value	-4028
	.value	945
	.value	787
	.value	834
	.value	837
	.value	7943
	.value	837
	.value	2
	.value	-4028
	.value	945
	.value	788
	.value	834
	.value	837
	.value	7944
	.value	-4029
	.value	913
	.value	787
	.value	837
	.value	7945
	.value	-4029
	.value	913
	.value	788
	.value	837
	.value	7946
	.value	837
	.value	2
	.value	-4028
	.value	913
	.value	787
	.value	768
	.value	837
	.value	7947
	.value	837
	.value	2
	.value	-4028
	.value	913
	.value	788
	.value	768
	.value	837
	.value	7948
	.value	837
	.value	2
	.value	-4028
	.value	913
	.value	787
	.value	769
	.value	837
	.value	7949
	.value	837
	.value	2
	.value	-4028
	.value	913
	.value	788
	.value	769
	.value	837
	.value	7950
	.value	837
	.value	2
	.value	-4028
	.value	913
	.value	787
	.value	834
	.value	837
	.value	7951
	.value	837
	.value	2
	.value	-4028
	.value	913
	.value	788
	.value	834
	.value	837
	.value	7968
	.value	-4029
	.value	951
	.value	787
	.value	837
	.value	7969
	.value	-4029
	.value	951
	.value	788
	.value	837
	.value	7970
	.value	837
	.value	2
	.value	-4028
	.value	951
	.value	787
	.value	768
	.value	837
	.value	7971
	.value	837
	.value	2
	.value	-4028
	.value	951
	.value	788
	.value	768
	.value	837
	.value	7972
	.value	837
	.value	2
	.value	-4028
	.value	951
	.value	787
	.value	769
	.value	837
	.value	7973
	.value	837
	.value	2
	.value	-4028
	.value	951
	.value	788
	.value	769
	.value	837
	.value	7974
	.value	837
	.value	2
	.value	-4028
	.value	951
	.value	787
	.value	834
	.value	837
	.value	7975
	.value	837
	.value	2
	.value	-4028
	.value	951
	.value	788
	.value	834
	.value	837
	.value	7976
	.value	-4029
	.value	919
	.value	787
	.value	837
	.value	7977
	.value	-4029
	.value	919
	.value	788
	.value	837
	.value	7978
	.value	837
	.value	2
	.value	-4028
	.value	919
	.value	787
	.value	768
	.value	837
	.value	7979
	.value	837
	.value	2
	.value	-4028
	.value	919
	.value	788
	.value	768
	.value	837
	.value	7980
	.value	837
	.value	2
	.value	-4028
	.value	919
	.value	787
	.value	769
	.value	837
	.value	7981
	.value	837
	.value	2
	.value	-4028
	.value	919
	.value	788
	.value	769
	.value	837
	.value	7982
	.value	837
	.value	2
	.value	-4028
	.value	919
	.value	787
	.value	834
	.value	837
	.value	7983
	.value	837
	.value	2
	.value	-4028
	.value	919
	.value	788
	.value	834
	.value	837
	.value	8032
	.value	-4029
	.value	969
	.value	787
	.value	837
	.value	8033
	.value	-4029
	.value	969
	.value	788
	.value	837
	.value	8034
	.value	837
	.value	2
	.value	-4028
	.value	969
	.value	787
	.value	768
	.value	837
	.value	8035
	.value	837
	.value	2
	.value	-4028
	.value	969
	.value	788
	.value	768
	.value	837
	.value	8036
	.value	837
	.value	2
	.value	-4028
	.value	969
	.value	787
	.value	769
	.value	837
	.value	8037
	.value	837
	.value	2
	.value	-4028
	.value	969
	.value	788
	.value	769
	.value	837
	.value	8038
	.value	837
	.value	2
	.value	-4028
	.value	969
	.value	787
	.value	834
	.value	837
	.value	8039
	.value	837
	.value	2
	.value	-4028
	.value	969
	.value	788
	.value	834
	.value	837
	.value	8040
	.value	-4029
	.value	937
	.value	787
	.value	837
	.value	8041
	.value	-4029
	.value	937
	.value	788
	.value	837
	.value	8042
	.value	837
	.value	2
	.value	-4028
	.value	937
	.value	787
	.value	768
	.value	837
	.value	8043
	.value	837
	.value	2
	.value	-4028
	.value	937
	.value	788
	.value	768
	.value	837
	.value	8044
	.value	837
	.value	2
	.value	-4028
	.value	937
	.value	787
	.value	769
	.value	837
	.value	8045
	.value	837
	.value	2
	.value	-4028
	.value	937
	.value	788
	.value	769
	.value	837
	.value	8046
	.value	837
	.value	2
	.value	-4028
	.value	937
	.value	787
	.value	834
	.value	837
	.value	8047
	.value	837
	.value	2
	.value	-4028
	.value	937
	.value	788
	.value	834
	.value	837
	.value	-6654
	.value	945
	.value	774
	.value	-6654
	.value	945
	.value	772
	.value	8048
	.value	-4029
	.value	945
	.value	768
	.value	837
	.value	-4094
	.value	945
	.value	837
	.value	940
	.value	-4029
	.value	945
	.value	769
	.value	837
	.value	8118
	.value	-4029
	.value	945
	.value	834
	.value	837
	.value	-6654
	.value	913
	.value	774
	.value	-6654
	.value	913
	.value	772
	.value	-6654
	.value	913
	.value	768
	.value	-4094
	.value	913
	.value	837
	.value	-6654
	.value	168
	.value	834
	.value	8052
	.value	-4029
	.value	951
	.value	768
	.value	837
	.value	-4094
	.value	951
	.value	837
	.value	942
	.value	-4029
	.value	951
	.value	769
	.value	837
	.value	8134
	.value	-4029
	.value	951
	.value	834
	.value	837
	.value	-6654
	.value	917
	.value	768
	.value	-6654
	.value	919
	.value	768
	.value	-4094
	.value	919
	.value	837
	.value	-6654
	.value	8127
	.value	768
	.value	-6654
	.value	8127
	.value	769
	.value	-6654
	.value	8127
	.value	834
	.value	-6654
	.value	953
	.value	774
	.value	-6654
	.value	953
	.value	772
	.value	970
	.value	-6589
	.value	953
	.value	776
	.value	768
	.value	-6654
	.value	953
	.value	834
	.value	970
	.value	-6589
	.value	953
	.value	776
	.value	834
	.value	-6654
	.value	921
	.value	774
	.value	-6654
	.value	921
	.value	772
	.value	-6654
	.value	921
	.value	768
	.value	-6654
	.value	8190
	.value	768
	.value	-6654
	.value	8190
	.value	769
	.value	-6654
	.value	8190
	.value	834
	.value	-6654
	.value	965
	.value	774
	.value	-6654
	.value	965
	.value	772
	.value	971
	.value	-6589
	.value	965
	.value	776
	.value	768
	.value	-6654
	.value	961
	.value	787
	.value	-6654
	.value	961
	.value	788
	.value	-6654
	.value	965
	.value	834
	.value	971
	.value	-6589
	.value	965
	.value	776
	.value	834
	.value	-6654
	.value	933
	.value	774
	.value	-6654
	.value	933
	.value	772
	.value	-6654
	.value	933
	.value	768
	.value	-6654
	.value	929
	.value	788
	.value	-6654
	.value	168
	.value	768
	.value	8060
	.value	-4029
	.value	969
	.value	768
	.value	837
	.value	-4094
	.value	969
	.value	837
	.value	974
	.value	-4029
	.value	969
	.value	769
	.value	837
	.value	8182
	.value	-4029
	.value	969
	.value	834
	.value	837
	.value	-6654
	.value	927
	.value	768
	.value	-6654
	.value	937
	.value	768
	.value	-4094
	.value	937
	.value	837
	.value	258
	.value	8592
	.value	824
	.value	258
	.value	8594
	.value	824
	.value	258
	.value	8596
	.value	824
	.value	258
	.value	8656
	.value	824
	.value	258
	.value	8660
	.value	824
	.value	258
	.value	8658
	.value	824
	.value	258
	.value	8707
	.value	824
	.value	258
	.value	8712
	.value	824
	.value	258
	.value	8715
	.value	824
	.value	258
	.value	8739
	.value	824
	.value	258
	.value	8741
	.value	824
	.value	258
	.value	8764
	.value	824
	.value	258
	.value	8771
	.value	824
	.value	258
	.value	8773
	.value	824
	.value	258
	.value	8776
	.value	824
	.value	258
	.value	61
	.value	824
	.value	258
	.value	8801
	.value	824
	.value	258
	.value	8781
	.value	824
	.value	258
	.value	60
	.value	824
	.value	258
	.value	62
	.value	824
	.value	258
	.value	8804
	.value	824
	.value	258
	.value	8805
	.value	824
	.value	258
	.value	8818
	.value	824
	.value	258
	.value	8819
	.value	824
	.value	258
	.value	8822
	.value	824
	.value	258
	.value	8823
	.value	824
	.value	258
	.value	8826
	.value	824
	.value	258
	.value	8827
	.value	824
	.value	258
	.value	8834
	.value	824
	.value	258
	.value	8835
	.value	824
	.value	258
	.value	8838
	.value	824
	.value	258
	.value	8839
	.value	824
	.value	258
	.value	8866
	.value	824
	.value	258
	.value	8872
	.value	824
	.value	258
	.value	8873
	.value	824
	.value	258
	.value	8875
	.value	824
	.value	258
	.value	8828
	.value	824
	.value	258
	.value	8829
	.value	824
	.value	258
	.value	8849
	.value	824
	.value	258
	.value	8850
	.value	824
	.value	258
	.value	8882
	.value	824
	.value	258
	.value	8883
	.value	824
	.value	258
	.value	8884
	.value	824
	.value	258
	.value	8885
	.value	824
	.value	2050
	.value	12363
	.value	12441
	.value	2050
	.value	12365
	.value	12441
	.value	2050
	.value	12367
	.value	12441
	.value	2050
	.value	12369
	.value	12441
	.value	2050
	.value	12371
	.value	12441
	.value	2050
	.value	12373
	.value	12441
	.value	2050
	.value	12375
	.value	12441
	.value	2050
	.value	12377
	.value	12441
	.value	2050
	.value	12379
	.value	12441
	.value	2050
	.value	12381
	.value	12441
	.value	2050
	.value	12383
	.value	12441
	.value	2050
	.value	12385
	.value	12441
	.value	2050
	.value	12388
	.value	12441
	.value	2050
	.value	12390
	.value	12441
	.value	2050
	.value	12392
	.value	12441
	.value	2050
	.value	12399
	.value	12441
	.value	2050
	.value	12399
	.value	12442
	.value	2050
	.value	12402
	.value	12441
	.value	2050
	.value	12402
	.value	12442
	.value	2050
	.value	12405
	.value	12441
	.value	2050
	.value	12405
	.value	12442
	.value	2050
	.value	12408
	.value	12441
	.value	2050
	.value	12408
	.value	12442
	.value	2050
	.value	12411
	.value	12441
	.value	2050
	.value	12411
	.value	12442
	.value	2050
	.value	12358
	.value	12441
	.value	2050
	.value	12445
	.value	12441
	.value	2050
	.value	12459
	.value	12441
	.value	2050
	.value	12461
	.value	12441
	.value	2050
	.value	12463
	.value	12441
	.value	2050
	.value	12465
	.value	12441
	.value	2050
	.value	12467
	.value	12441
	.value	2050
	.value	12469
	.value	12441
	.value	2050
	.value	12471
	.value	12441
	.value	2050
	.value	12473
	.value	12441
	.value	2050
	.value	12475
	.value	12441
	.value	2050
	.value	12477
	.value	12441
	.value	2050
	.value	12479
	.value	12441
	.value	2050
	.value	12481
	.value	12441
	.value	2050
	.value	12484
	.value	12441
	.value	2050
	.value	12486
	.value	12441
	.value	2050
	.value	12488
	.value	12441
	.value	2050
	.value	12495
	.value	12441
	.value	2050
	.value	12495
	.value	12442
	.value	2050
	.value	12498
	.value	12441
	.value	2050
	.value	12498
	.value	12442
	.value	2050
	.value	12501
	.value	12441
	.value	2050
	.value	12501
	.value	12442
	.value	2050
	.value	12504
	.value	12441
	.value	2050
	.value	12504
	.value	12442
	.value	2050
	.value	12507
	.value	12441
	.value	2050
	.value	12507
	.value	12442
	.value	2050
	.value	12454
	.value	12441
	.value	2050
	.value	12527
	.value	12441
	.value	2050
	.value	12528
	.value	12441
	.value	2050
	.value	12529
	.value	12441
	.value	2050
	.value	12530
	.value	12441
	.value	2050
	.value	12541
	.value	12441
	.value	1796
	.value	-10236
	.value	-9063
	.value	-10236
	.value	-9030
	.value	1796
	.value	-10236
	.value	-9061
	.value	-10236
	.value	-9030
	.value	1796
	.value	-10236
	.value	-9051
	.value	-10236
	.value	-9030
	.value	4
	.value	-10236
	.value	-8911
	.value	-10236
	.value	-8921
	.value	4
	.value	-10236
	.value	-8910
	.value	-10236
	.value	-8921
	.value	4
	.value	-10236
	.value	-8377
	.value	-10236
	.value	-8386
	.value	4
	.value	-10236
	.value	-8377
	.value	-10236
	.value	-8361
	.value	4
	.value	-10235
	.value	-9031
	.value	-10235
	.value	-9030
	.value	4
	.value	-10235
	.value	-9031
	.value	-10235
	.value	-9040
	.value	4
	.value	-10235
	.value	-9031
	.value	-10235
	.value	-9027
	.value	4
	.value	-10235
	.value	-8776
	.value	-10235
	.value	-8785
	.value	4
	.value	-10235
	.value	-8775
	.value	-10235
	.value	-8785
	.value	4
	.value	-10234
	.value	-8907
	.value	-10234
	.value	-8912
	.value	1
	.value	697
	.value	1
	.value	59
	.value	1
	.value	183
	.value	1794
	.value	2325
	.value	2364
	.value	1794
	.value	2326
	.value	2364
	.value	1794
	.value	2327
	.value	2364
	.value	1794
	.value	2332
	.value	2364
	.value	1794
	.value	2337
	.value	2364
	.value	1794
	.value	2338
	.value	2364
	.value	1794
	.value	2347
	.value	2364
	.value	1794
	.value	2351
	.value	2364
	.value	1794
	.value	2465
	.value	2492
	.value	1794
	.value	2466
	.value	2492
	.value	1794
	.value	2479
	.value	2492
	.value	1794
	.value	2610
	.value	2620
	.value	1794
	.value	2616
	.value	2620
	.value	1794
	.value	2582
	.value	2620
	.value	1794
	.value	2583
	.value	2620
	.value	1794
	.value	2588
	.value	2620
	.value	1794
	.value	2603
	.value	2620
	.value	1794
	.value	2849
	.value	2876
	.value	1794
	.value	2850
	.value	2876
	.value	2
	.value	3906
	.value	4023
	.value	2
	.value	3916
	.value	4023
	.value	2
	.value	3921
	.value	4023
	.value	2
	.value	3926
	.value	4023
	.value	2
	.value	3931
	.value	4023
	.value	2
	.value	3904
	.value	4021
	.value	-32254
	.value	4018
	.value	3968
	.value	-32254
	.value	4019
	.value	3968
	.value	2
	.value	3986
	.value	4023
	.value	2
	.value	3996
	.value	4023
	.value	2
	.value	4001
	.value	4023
	.value	2
	.value	4006
	.value	4023
	.value	2
	.value	4011
	.value	4023
	.value	2
	.value	3984
	.value	4021
	.value	1
	.value	953
	.value	1
	.value	96
	.value	1
	.value	180
	.value	1
	.value	937
	.value	1
	.value	75
	.value	1
	.value	12296
	.value	1
	.value	12297
	.value	258
	.value	10973
	.value	824
	.value	1
	.value	-29624
	.value	1
	.value	26356
	.value	1
	.value	-28982
	.value	1
	.value	-29496
	.value	1
	.value	28369
	.value	1
	.value	20018
	.value	1
	.value	21477
	.value	1
	.value	-24676
	.value	1
	.value	22865
	.value	1
	.value	-28207
	.value	1
	.value	21895
	.value	1
	.value	22856
	.value	1
	.value	25078
	.value	1
	.value	30313
	.value	1
	.value	32645
	.value	1
	.value	-31169
	.value	1
	.value	-30790
	.value	1
	.value	-30472
	.value	1
	.value	-28529
	.value	1
	.value	27138
	.value	1
	.value	27931
	.value	1
	.value	28889
	.value	1
	.value	29662
	.value	1
	.value	-31683
	.value	1
	.value	-28310
	.value	1
	.value	-26127
	.value	1
	.value	20098
	.value	1
	.value	21365
	.value	1
	.value	27396
	.value	1
	.value	29211
	.value	1
	.value	-31187
	.value	1
	.value	-25058
	.value	1
	.value	23888
	.value	1
	.value	28651
	.value	1
	.value	-31283
	.value	1
	.value	-30364
	.value	1
	.value	25289
	.value	1
	.value	-32296
	.value	1
	.value	-30689
	.value	1
	.value	24266
	.value	1
	.value	26391
	.value	1
	.value	28010
	.value	1
	.value	29436
	.value	1
	.value	-28466
	.value	1
	.value	20358
	.value	1
	.value	20919
	.value	1
	.value	21214
	.value	1
	.value	25796
	.value	1
	.value	27347
	.value	1
	.value	29200
	.value	1
	.value	30439
	.value	1
	.value	-32767
	.value	1
	.value	-31226
	.value	1
	.value	-31140
	.value	1
	.value	-29201
	.value	1
	.value	-26830
	.value	1
	.value	-25745
	.value	1
	.value	-25094
	.value	1
	.value	30860
	.value	1
	.value	31103
	.value	1
	.value	32160
	.value	1
	.value	-31799
	.value	1
	.value	-27900
	.value	1
	.value	-24961
	.value	1
	.value	-29994
	.value	1
	.value	22751
	.value	1
	.value	24324
	.value	1
	.value	31840
	.value	1
	.value	-32642
	.value	1
	.value	29282
	.value	1
	.value	30922
	.value	1
	.value	-29502
	.value	1
	.value	-26889
	.value	1
	.value	22744
	.value	1
	.value	23650
	.value	1
	.value	27155
	.value	1
	.value	28122
	.value	1
	.value	28431
	.value	1
	.value	32047
	.value	1
	.value	32311
	.value	1
	.value	-27061
	.value	1
	.value	21202
	.value	1
	.value	-32629
	.value	1
	.value	20956
	.value	1
	.value	20940
	.value	1
	.value	31260
	.value	1
	.value	32190
	.value	1
	.value	-31759
	.value	1
	.value	-27019
	.value	1
	.value	-29824
	.value	1
	.value	25295
	.value	1
	.value	-29954
	.value	1
	.value	20025
	.value	1
	.value	23527
	.value	1
	.value	24594
	.value	1
	.value	29575
	.value	1
	.value	30064
	.value	1
	.value	21271
	.value	1
	.value	30971
	.value	1
	.value	20415
	.value	1
	.value	24489
	.value	1
	.value	19981
	.value	1
	.value	27852
	.value	1
	.value	25976
	.value	1
	.value	32034
	.value	1
	.value	21443
	.value	1
	.value	22622
	.value	1
	.value	30465
	.value	1
	.value	-31671
	.value	1
	.value	-30038
	.value	1
	.value	27578
	.value	1
	.value	-28752
	.value	1
	.value	27784
	.value	1
	.value	25342
	.value	1
	.value	-32027
	.value	1
	.value	25504
	.value	1
	.value	30053
	.value	1
	.value	20142
	.value	1
	.value	20841
	.value	1
	.value	20937
	.value	1
	.value	26753
	.value	1
	.value	31975
	.value	1
	.value	-32145
	.value	1
	.value	-29998
	.value	1
	.value	-28209
	.value	1
	.value	21237
	.value	1
	.value	21570
	.value	1
	.value	22899
	.value	1
	.value	24300
	.value	1
	.value	26053
	.value	1
	.value	28670
	.value	1
	.value	31018
	.value	1
	.value	-27219
	.value	1
	.value	-26006
	.value	1
	.value	-24937
	.value	1
	.value	-24882
	.value	1
	.value	21147
	.value	1
	.value	26310
	.value	1
	.value	27511
	.value	1
	.value	-28830
	.value	1
	.value	24180
	.value	1
	.value	24976
	.value	1
	.value	25088
	.value	1
	.value	25754
	.value	1
	.value	28451
	.value	1
	.value	29001
	.value	1
	.value	29833
	.value	1
	.value	31178
	.value	1
	.value	32244
	.value	1
	.value	-32657
	.value	1
	.value	-28890
	.value	1
	.value	-31506
	.value	1
	.value	-28637
	.value	1
	.value	-27830
	.value	1
	.value	21015
	.value	1
	.value	21155
	.value	1
	.value	21693
	.value	1
	.value	28872
	.value	1
	.value	-30526
	.value	1
	.value	24265
	.value	1
	.value	24565
	.value	1
	.value	25467
	.value	1
	.value	27566
	.value	1
	.value	31806
	.value	1
	.value	29557
	.value	1
	.value	20196
	.value	1
	.value	22265
	.value	1
	.value	23994
	.value	1
	.value	24604
	.value	1
	.value	29618
	.value	1
	.value	29801
	.value	1
	.value	32666
	.value	1
	.value	-32698
	.value	1
	.value	-28108
	.value	1
	.value	-26890
	.value	1
	.value	-26808
	.value	1
	.value	-26600
	.value	1
	.value	20363
	.value	1
	.value	31150
	.value	1
	.value	-28236
	.value	1
	.value	-26952
	.value	1
	.value	24801
	.value	1
	.value	20102
	.value	1
	.value	20698
	.value	1
	.value	23534
	.value	1
	.value	23615
	.value	1
	.value	26009
	.value	1
	.value	29134
	.value	1
	.value	30274
	.value	1
	.value	-31492
	.value	1
	.value	-28548
	.value	1
	.value	-24691
	.value	1
	.value	26248
	.value	1
	.value	-27090
	.value	1
	.value	21129
	.value	1
	.value	26491
	.value	1
	.value	26611
	.value	1
	.value	27969
	.value	1
	.value	28316
	.value	1
	.value	29705
	.value	1
	.value	30041
	.value	1
	.value	30827
	.value	1
	.value	32016
	.value	1
	.value	-26530
	.value	1
	.value	20845
	.value	1
	.value	25134
	.value	1
	.value	-27016
	.value	1
	.value	20523
	.value	1
	.value	23833
	.value	1
	.value	28138
	.value	1
	.value	-28886
	.value	1
	.value	24459
	.value	1
	.value	24900
	.value	1
	.value	26647
	.value	1
	.value	-27002
	.value	1
	.value	21033
	.value	1
	.value	21519
	.value	1
	.value	23653
	.value	1
	.value	26131
	.value	1
	.value	26446
	.value	1
	.value	26792
	.value	1
	.value	27877
	.value	1
	.value	29702
	.value	1
	.value	30178
	.value	1
	.value	32633
	.value	1
	.value	-30513
	.value	1
	.value	-30495
	.value	1
	.value	-28212
	.value	1
	.value	-26910
	.value	1
	.value	21311
	.value	1
	.value	28346
	.value	1
	.value	21533
	.value	1
	.value	29136
	.value	1
	.value	29848
	.value	1
	.value	-31238
	.value	1
	.value	-26973
	.value	1
	.value	-25513
	.value	1
	.value	-24929
	.value	1
	.value	26519
	.value	1
	.value	28107
	.value	1
	.value	-32280
	.value	1
	.value	31435
	.value	1
	.value	31520
	.value	1
	.value	31890
	.value	1
	.value	29376
	.value	1
	.value	28825
	.value	1
	.value	-29864
	.value	1
	.value	20160
	.value	1
	.value	-31946
	.value	1
	.value	21050
	.value	1
	.value	20999
	.value	1
	.value	24230
	.value	1
	.value	25299
	.value	1
	.value	31958
	.value	1
	.value	23429
	.value	1
	.value	27934
	.value	1
	.value	26292
	.value	1
	.value	-28869
	.value	1
	.value	-30644
	.value	1
	.value	-27059
	.value	1
	.value	-30325
	.value	1
	.value	24275
	.value	1
	.value	20800
	.value	1
	.value	21952
	.value	1
	.value	22618
	.value	1
	.value	26228
	.value	1
	.value	20958
	.value	1
	.value	29482
	.value	1
	.value	30410
	.value	1
	.value	31036
	.value	1
	.value	31070
	.value	1
	.value	31077
	.value	1
	.value	31119
	.value	1
	.value	-26794
	.value	1
	.value	31934
	.value	1
	.value	32701
	.value	1
	.value	-31214
	.value	1
	.value	-29960
	.value	1
	.value	-28616
	.value	1
	.value	-28419
	.value	1
	.value	-26385
	.value	1
	.value	-26372
	.value	1
	.value	-26328
	.value	1
	.value	-25164
	.value	1
	.value	-28450
	.value	1
	.value	-26953
	.value	1
	.value	20398
	.value	1
	.value	20711
	.value	1
	.value	20813
	.value	1
	.value	21193
	.value	1
	.value	21220
	.value	1
	.value	21329
	.value	1
	.value	21917
	.value	1
	.value	22022
	.value	1
	.value	22120
	.value	1
	.value	22592
	.value	1
	.value	22696
	.value	1
	.value	23652
	.value	1
	.value	23662
	.value	1
	.value	24724
	.value	1
	.value	24936
	.value	1
	.value	24974
	.value	1
	.value	25074
	.value	1
	.value	25935
	.value	1
	.value	26082
	.value	1
	.value	26257
	.value	1
	.value	26757
	.value	1
	.value	28023
	.value	1
	.value	28186
	.value	1
	.value	28450
	.value	1
	.value	29038
	.value	1
	.value	29227
	.value	1
	.value	29730
	.value	1
	.value	30865
	.value	1
	.value	31038
	.value	1
	.value	31049
	.value	1
	.value	31048
	.value	1
	.value	31056
	.value	1
	.value	31062
	.value	1
	.value	31069
	.value	1
	.value	31117
	.value	1
	.value	31118
	.value	1
	.value	31296
	.value	1
	.value	31361
	.value	1
	.value	31680
	.value	1
	.value	32265
	.value	1
	.value	32321
	.value	1
	.value	32626
	.value	1
	.value	-32763
	.value	1
	.value	-32275
	.value	1
	.value	-32135
	.value	1
	.value	-31657
	.value	1
	.value	-30448
	.value	1
	.value	-30314
	.value	1
	.value	-29951
	.value	1
	.value	-29895
	.value	1
	.value	-29485
	.value	1
	.value	-29432
	.value	1
	.value	-28746
	.value	1
	.value	-26909
	.value	1
	.value	-26625
	.value	1
	.value	-26565
	.value	1
	.value	24693
	.value	2
	.value	-10160
	.value	-8466
	.value	1
	.value	-32232
	.value	1
	.value	20006
	.value	1
	.value	20917
	.value	1
	.value	20840
	.value	1
	.value	20352
	.value	1
	.value	20805
	.value	1
	.value	20864
	.value	1
	.value	21191
	.value	1
	.value	21242
	.value	1
	.value	21845
	.value	1
	.value	21913
	.value	1
	.value	21986
	.value	1
	.value	22707
	.value	1
	.value	22852
	.value	1
	.value	22868
	.value	1
	.value	23138
	.value	1
	.value	23336
	.value	1
	.value	24274
	.value	1
	.value	24281
	.value	1
	.value	24425
	.value	1
	.value	24493
	.value	1
	.value	24792
	.value	1
	.value	24910
	.value	1
	.value	24840
	.value	1
	.value	24928
	.value	1
	.value	25140
	.value	1
	.value	25540
	.value	1
	.value	25628
	.value	1
	.value	25682
	.value	1
	.value	25942
	.value	1
	.value	26395
	.value	1
	.value	26454
	.value	1
	.value	27513
	.value	1
	.value	28379
	.value	1
	.value	28363
	.value	1
	.value	28702
	.value	1
	.value	30631
	.value	1
	.value	29237
	.value	1
	.value	29359
	.value	1
	.value	29809
	.value	1
	.value	29958
	.value	1
	.value	30011
	.value	1
	.value	30237
	.value	1
	.value	30239
	.value	1
	.value	30427
	.value	1
	.value	30452
	.value	1
	.value	30538
	.value	1
	.value	30528
	.value	1
	.value	30924
	.value	1
	.value	31409
	.value	1
	.value	31867
	.value	1
	.value	32091
	.value	1
	.value	32574
	.value	1
	.value	-31918
	.value	1
	.value	-31761
	.value	1
	.value	-30855
	.value	1
	.value	-30399
	.value	1
	.value	-30330
	.value	1
	.value	-30017
	.value	1
	.value	-30005
	.value	1
	.value	-29971
	.value	1
	.value	-29814
	.value	1
	.value	-28872
	.value	1
	.value	-28558
	.value	1
	.value	-28263
	.value	1
	.value	-28042
	.value	1
	.value	-27012
	.value	1
	.value	-26661
	.value	1
	.value	-26613
	.value	1
	.value	-25838
	.value	2
	.value	-10166
	.value	-9142
	.value	2
	.value	-10166
	.value	-9148
	.value	2
	.value	-10164
	.value	-8235
	.value	1
	.value	15261
	.value	1
	.value	16408
	.value	1
	.value	16441
	.value	2
	.value	-10156
	.value	-8631
	.value	2
	.value	-10153
	.value	-9008
	.value	2
	.value	-10145
	.value	-8493
	.value	1
	.value	-24765
	.value	1
	.value	-24690
	.value	3586
	.value	1497
	.value	1460
	.value	4354
	.value	1522
	.value	1463
	.value	6146
	.value	1513
	.value	1473
	.value	6402
	.value	1513
	.value	1474
	.value	-1207
	.value	6211
	.value	1513
	.value	1468
	.value	1473
	.value	-1207
	.value	6467
	.value	1513
	.value	1468
	.value	1474
	.value	4354
	.value	1488
	.value	1463
	.value	4610
	.value	1488
	.value	1464
	.value	5378
	.value	1488
	.value	1468
	.value	5378
	.value	1489
	.value	1468
	.value	5378
	.value	1490
	.value	1468
	.value	5378
	.value	1491
	.value	1468
	.value	5378
	.value	1492
	.value	1468
	.value	5378
	.value	1493
	.value	1468
	.value	5378
	.value	1494
	.value	1468
	.value	5378
	.value	1496
	.value	1468
	.value	5378
	.value	1497
	.value	1468
	.value	5378
	.value	1498
	.value	1468
	.value	5378
	.value	1499
	.value	1468
	.value	5378
	.value	1500
	.value	1468
	.value	5378
	.value	1502
	.value	1468
	.value	5378
	.value	1504
	.value	1468
	.value	5378
	.value	1505
	.value	1468
	.value	5378
	.value	1507
	.value	1468
	.value	5378
	.value	1508
	.value	1468
	.value	5378
	.value	1510
	.value	1468
	.value	5378
	.value	1511
	.value	1468
	.value	5378
	.value	1512
	.value	1468
	.value	5378
	.value	1513
	.value	1468
	.value	5378
	.value	1514
	.value	1468
	.value	4866
	.value	1493
	.value	1465
	.value	5890
	.value	1489
	.value	1471
	.value	5890
	.value	1499
	.value	1471
	.value	5890
	.value	1508
	.value	1471
	.value	-10236
	.value	-10188
	.value	-8873
	.value	-10188
	.value	-8859
	.value	-10236
	.value	-10188
	.value	-8872
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8865
	.value	-10188
	.value	-8850
	.value	4
	.value	-10170
	.value	-10188
	.value	-8872
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8850
	.value	-10188
	.value	-8865
	.value	-10188
	.value	-8849
	.value	4
	.value	-10170
	.value	-10188
	.value	-8872
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8849
	.value	-10188
	.value	-8865
	.value	-10188
	.value	-8848
	.value	4
	.value	-10170
	.value	-10188
	.value	-8872
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8848
	.value	-10188
	.value	-8865
	.value	-10188
	.value	-8847
	.value	4
	.value	-10170
	.value	-10188
	.value	-8872
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8847
	.value	-10188
	.value	-8865
	.value	-10188
	.value	-8846
	.value	4
	.value	-10170
	.value	-10188
	.value	-8872
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8846
	.value	-10236
	.value	-10188
	.value	-8775
	.value	-10188
	.value	-8859
	.value	-10236
	.value	-10188
	.value	-8774
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8773
	.value	-10188
	.value	-8850
	.value	4
	.value	-10170
	.value	-10188
	.value	-8775
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8850
	.value	-10188
	.value	-8772
	.value	-10188
	.value	-8850
	.value	4
	.value	-10170
	.value	-10188
	.value	-8774
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8850
	.value	-10188
	.value	-8773
	.value	-10188
	.value	-8849
	.value	4
	.value	-10170
	.value	-10188
	.value	-8775
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8849
	.value	-10188
	.value	-8772
	.value	-10188
	.value	-8849
	.value	4
	.value	-10170
	.value	-10188
	.value	-8774
	.value	-10188
	.value	-8859
	.value	-10188
	.value	-8849
	.value	1
	.value	20029
	.value	1
	.value	20024
	.value	1
	.value	20033
	.value	2
	.value	-10176
	.value	-8926
	.value	1
	.value	20320
	.value	1
	.value	20411
	.value	1
	.value	20482
	.value	1
	.value	20602
	.value	1
	.value	20633
	.value	1
	.value	20687
	.value	1
	.value	13470
	.value	2
	.value	-10175
	.value	-8646
	.value	1
	.value	20820
	.value	1
	.value	20836
	.value	1
	.value	20855
	.value	2
	.value	-10175
	.value	-8932
	.value	1
	.value	13497
	.value	1
	.value	20839
	.value	1
	.value	20877
	.value	2
	.value	-10175
	.value	-8885
	.value	1
	.value	20887
	.value	1
	.value	20900
	.value	1
	.value	20172
	.value	1
	.value	20908
	.value	2
	.value	-10140
	.value	-8737
	.value	1
	.value	20981
	.value	1
	.value	20995
	.value	1
	.value	13535
	.value	1
	.value	21051
	.value	1
	.value	21062
	.value	1
	.value	21106
	.value	1
	.value	21111
	.value	1
	.value	13589
	.value	1
	.value	21253
	.value	1
	.value	21254
	.value	1
	.value	21321
	.value	1
	.value	21338
	.value	1
	.value	21363
	.value	1
	.value	21373
	.value	1
	.value	21375
	.value	2
	.value	-10174
	.value	-8660
	.value	1
	.value	28784
	.value	1
	.value	21450
	.value	1
	.value	21471
	.value	2
	.value	-10174
	.value	-8349
	.value	1
	.value	21483
	.value	1
	.value	21489
	.value	1
	.value	21510
	.value	1
	.value	21662
	.value	1
	.value	21560
	.value	1
	.value	21576
	.value	1
	.value	21608
	.value	1
	.value	21666
	.value	1
	.value	21750
	.value	1
	.value	21776
	.value	1
	.value	21843
	.value	1
	.value	21859
	.value	1
	.value	21892
	.value	1
	.value	21931
	.value	1
	.value	21939
	.value	1
	.value	21954
	.value	1
	.value	22294
	.value	1
	.value	22295
	.value	1
	.value	22097
	.value	1
	.value	22132
	.value	1
	.value	22766
	.value	1
	.value	22478
	.value	1
	.value	22516
	.value	1
	.value	22541
	.value	1
	.value	22411
	.value	1
	.value	22578
	.value	1
	.value	22577
	.value	1
	.value	22700
	.value	2
	.value	-10171
	.value	-8988
	.value	1
	.value	22770
	.value	1
	.value	22775
	.value	1
	.value	22790
	.value	1
	.value	22810
	.value	1
	.value	22818
	.value	1
	.value	22882
	.value	2
	.value	-10171
	.value	-8536
	.value	2
	.value	-10171
	.value	-8470
	.value	1
	.value	23020
	.value	1
	.value	23067
	.value	1
	.value	23079
	.value	1
	.value	23000
	.value	1
	.value	23142
	.value	1
	.value	14062
	.value	1
	.value	14076
	.value	1
	.value	23304
	.value	1
	.value	23358
	.value	2
	.value	-10170
	.value	-8760
	.value	1
	.value	23491
	.value	1
	.value	23512
	.value	1
	.value	23539
	.value	2
	.value	-10170
	.value	-8424
	.value	1
	.value	23551
	.value	1
	.value	23558
	.value	1
	.value	24403
	.value	1
	.value	23586
	.value	1
	.value	14209
	.value	1
	.value	23648
	.value	1
	.value	23744
	.value	1
	.value	23693
	.value	2
	.value	-10169
	.value	-8732
	.value	1
	.value	23875
	.value	2
	.value	-10169
	.value	-8730
	.value	1
	.value	23918
	.value	1
	.value	23915
	.value	1
	.value	23932
	.value	1
	.value	24033
	.value	1
	.value	24034
	.value	1
	.value	14383
	.value	1
	.value	24061
	.value	1
	.value	24104
	.value	1
	.value	24125
	.value	1
	.value	24169
	.value	1
	.value	14434
	.value	2
	.value	-10168
	.value	-8829
	.value	1
	.value	14460
	.value	1
	.value	24240
	.value	1
	.value	24243
	.value	1
	.value	24246
	.value	2
	.value	-10136
	.value	-8302
	.value	1
	.value	24318
	.value	2
	.value	-10168
	.value	-8399
	.value	1
	.value	-32255
	.value	1
	.value	24354
	.value	1
	.value	14535
	.value	2
	.value	-10164
	.value	-8520
	.value	2
	.value	-10152
	.value	-8742
	.value	1
	.value	24418
	.value	1
	.value	24427
	.value	1
	.value	14563
	.value	1
	.value	24474
	.value	1
	.value	24525
	.value	1
	.value	24535
	.value	1
	.value	24569
	.value	1
	.value	24705
	.value	1
	.value	14650
	.value	1
	.value	14620
	.value	2
	.value	-10167
	.value	-8492
	.value	1
	.value	24775
	.value	1
	.value	24904
	.value	1
	.value	24908
	.value	1
	.value	24954
	.value	1
	.value	25010
	.value	1
	.value	24996
	.value	1
	.value	25007
	.value	1
	.value	25054
	.value	1
	.value	25104
	.value	1
	.value	25115
	.value	1
	.value	25181
	.value	1
	.value	25265
	.value	1
	.value	25300
	.value	1
	.value	25424
	.value	2
	.value	-10166
	.value	-8436
	.value	1
	.value	25405
	.value	1
	.value	25340
	.value	1
	.value	25448
	.value	1
	.value	25475
	.value	1
	.value	25572
	.value	2
	.value	-10166
	.value	-8207
	.value	1
	.value	25634
	.value	1
	.value	25541
	.value	1
	.value	25513
	.value	1
	.value	14894
	.value	1
	.value	25705
	.value	1
	.value	25726
	.value	1
	.value	25757
	.value	1
	.value	25719
	.value	1
	.value	14956
	.value	1
	.value	25964
	.value	2
	.value	-10164
	.value	-9206
	.value	1
	.value	26083
	.value	1
	.value	26360
	.value	1
	.value	26185
	.value	1
	.value	15129
	.value	1
	.value	15112
	.value	1
	.value	15076
	.value	1
	.value	20882
	.value	1
	.value	20885
	.value	1
	.value	26368
	.value	1
	.value	26268
	.value	1
	.value	-32595
	.value	1
	.value	17369
	.value	1
	.value	26401
	.value	1
	.value	26462
	.value	1
	.value	26451
	.value	2
	.value	-10164
	.value	-8253
	.value	1
	.value	15177
	.value	1
	.value	26618
	.value	1
	.value	26501
	.value	1
	.value	26706
	.value	2
	.value	-10163
	.value	-9107
	.value	1
	.value	26766
	.value	1
	.value	26655
	.value	1
	.value	26900
	.value	1
	.value	26946
	.value	1
	.value	27043
	.value	1
	.value	27114
	.value	1
	.value	27304
	.value	2
	.value	-10163
	.value	-8541
	.value	1
	.value	27355
	.value	1
	.value	15384
	.value	1
	.value	27425
	.value	2
	.value	-10162
	.value	-9049
	.value	1
	.value	27476
	.value	1
	.value	15438
	.value	1
	.value	27506
	.value	1
	.value	27551
	.value	1
	.value	27579
	.value	2
	.value	-10162
	.value	-8563
	.value	2
	.value	-10169
	.value	-8949
	.value	2
	.value	-10162
	.value	-8454
	.value	1
	.value	27726
	.value	2
	.value	-10161
	.value	-9028
	.value	1
	.value	27839
	.value	1
	.value	27853
	.value	1
	.value	27751
	.value	1
	.value	27926
	.value	1
	.value	27966
	.value	1
	.value	28009
	.value	1
	.value	28024
	.value	1
	.value	28037
	.value	2
	.value	-10161
	.value	-8930
	.value	1
	.value	27956
	.value	1
	.value	28207
	.value	1
	.value	28270
	.value	1
	.value	15667
	.value	1
	.value	28359
	.value	2
	.value	-10161
	.value	-8495
	.value	1
	.value	28153
	.value	1
	.value	28526
	.value	2
	.value	-10161
	.value	-8354
	.value	2
	.value	-10161
	.value	-8306
	.value	1
	.value	28614
	.value	1
	.value	28729
	.value	1
	.value	28699
	.value	1
	.value	15766
	.value	1
	.value	28746
	.value	1
	.value	28797
	.value	1
	.value	28791
	.value	1
	.value	28845
	.value	2
	.value	-10175
	.value	-8923
	.value	1
	.value	28997
	.value	2
	.value	-10160
	.value	-8605
	.value	1
	.value	29084
	.value	2
	.value	-10160
	.value	-8277
	.value	1
	.value	29224
	.value	1
	.value	29264
	.value	2
	.value	-10159
	.value	-8696
	.value	1
	.value	29312
	.value	1
	.value	29333
	.value	2
	.value	-10159
	.value	-8395
	.value	2
	.value	-10158
	.value	-9196
	.value	1
	.value	29562
	.value	1
	.value	29579
	.value	1
	.value	16044
	.value	1
	.value	29605
	.value	1
	.value	16056
	.value	1
	.value	29767
	.value	1
	.value	29788
	.value	1
	.value	29829
	.value	1
	.value	29898
	.value	1
	.value	16155
	.value	1
	.value	29988
	.value	2
	.value	-10157
	.value	-9162
	.value	1
	.value	30014
	.value	2
	.value	-10157
	.value	-9070
	.value	2
	.value	-10168
	.value	-8801
	.value	1
	.value	30224
	.value	2
	.value	-10157
	.value	-8287
	.value	2
	.value	-10157
	.value	-8264
	.value	2
	.value	-10156
	.value	-9148
	.value	1
	.value	16380
	.value	1
	.value	16392
	.value	2
	.value	-10156
	.value	-8973
	.value	2
	.value	-10156
	.value	-8974
	.value	2
	.value	-10156
	.value	-8935
	.value	2
	.value	-10156
	.value	-8909
	.value	1
	.value	30494
	.value	1
	.value	30495
	.value	1
	.value	30603
	.value	1
	.value	16454
	.value	1
	.value	16534
	.value	2
	.value	-10155
	.value	-9187
	.value	1
	.value	30798
	.value	1
	.value	16611
	.value	2
	.value	-10155
	.value	-8666
	.value	2
	.value	-10155
	.value	-8550
	.value	2
	.value	-10155
	.value	-8507
	.value	1
	.value	31211
	.value	1
	.value	16687
	.value	1
	.value	31306
	.value	1
	.value	31311
	.value	2
	.value	-10154
	.value	-8836
	.value	2
	.value	-10154
	.value	-8537
	.value	1
	.value	31470
	.value	1
	.value	16898
	.value	2
	.value	-10154
	.value	-8277
	.value	1
	.value	31686
	.value	1
	.value	31689
	.value	1
	.value	16935
	.value	2
	.value	-10153
	.value	-9088
	.value	1
	.value	31954
	.value	1
	.value	17056
	.value	1
	.value	31976
	.value	1
	.value	31971
	.value	1
	.value	32000
	.value	2
	.value	-10153
	.value	-8314
	.value	1
	.value	32099
	.value	1
	.value	17153
	.value	1
	.value	32199
	.value	1
	.value	32258
	.value	1
	.value	32325
	.value	1
	.value	17204
	.value	2
	.value	-10152
	.value	-8664
	.value	2
	.value	-10152
	.value	-8633
	.value	1
	.value	17241
	.value	2
	.value	-10152
	.value	-8487
	.value	1
	.value	32634
	.value	2
	.value	-10152
	.value	-8386
	.value	1
	.value	32661
	.value	1
	.value	32762
	.value	2
	.value	-10151
	.value	-8998
	.value	2
	.value	-10151
	.value	-8925
	.value	1
	.value	-32672
	.value	2
	.value	-10151
	.value	-8792
	.value	1
	.value	-32656
	.value	2
	.value	-10164
	.value	-8353
	.value	1
	.value	17365
	.value	1
	.value	-32590
	.value	1
	.value	-32509
	.value	1
	.value	17419
	.value	1
	.value	-32450
	.value	1
	.value	23221
	.value	2
	.value	-10151
	.value	-8281
	.value	2
	.value	-10151
	.value	-8267
	.value	2
	.value	-10164
	.value	-8301
	.value	2
	.value	-10164
	.value	-8292
	.value	1
	.value	-32252
	.value	1
	.value	-28770
	.value	1
	.value	17515
	.value	1
	.value	-32111
	.value	1
	.value	-32117
	.value	1
	.value	-32099
	.value	1
	.value	21171
	.value	1
	.value	-32079
	.value	1
	.value	-32077
	.value	1
	.value	-32067
	.value	1
	.value	-32026
	.value	2
	.value	-10150
	.value	-8388
	.value	1
	.value	-31971
	.value	1
	.value	-31901
	.value	1
	.value	-31827
	.value	1
	.value	-31965
	.value	1
	.value	-31811
	.value	1
	.value	-31769
	.value	1
	.value	-31917
	.value	1
	.value	-31798
	.value	1
	.value	-31796
	.value	1
	.value	-31780
	.value	2
	.value	-10149
	.value	-9162
	.value	2
	.value	-10149
	.value	-8853
	.value	2
	.value	-10149
	.value	-9003
	.value	1
	.value	17707
	.value	1
	.value	-31503
	.value	1
	.value	-31501
	.value	1
	.value	-31466
	.value	2
	.value	-10148
	.value	-8246
	.value	1
	.value	-31388
	.value	2
	.value	-10149
	.value	-8404
	.value	1
	.value	17757
	.value	1
	.value	17761
	.value	2
	.value	-10149
	.value	-8271
	.value	2
	.value	-10148
	.value	-9006
	.value	1
	.value	17771
	.value	1
	.value	-31152
	.value	1
	.value	-31129
	.value	1
	.value	-31127
	.value	1
	.value	-31063
	.value	1
	.value	-31096
	.value	1
	.value	-30962
	.value	1
	.value	-31006
	.value	1
	.value	-30936
	.value	1
	.value	-30869
	.value	1
	.value	-30842
	.value	1
	.value	17879
	.value	1
	.value	-30751
	.value	1
	.value	-30719
	.value	1
	.value	17913
	.value	1
	.value	-30624
	.value	1
	.value	-30621
	.value	2
	.value	-10147
	.value	-8601
	.value	1
	.value	-30505
	.value	1
	.value	-30498
	.value	1
	.value	17973
	.value	1
	.value	-30470
	.value	1
	.value	13499
	.value	2
	.value	-10146
	.value	-9042
	.value	2
	.value	-10146
	.value	-8858
	.value	1
	.value	18110
	.value	1
	.value	18119
	.value	1
	.value	-30048
	.value	1
	.value	-29611
	.value	2
	.value	-10145
	.value	-9048
	.value	1
	.value	-29525
	.value	1
	.value	-29503
	.value	1
	.value	-29413
	.value	1
	.value	-29321
	.value	2
	.value	-10145
	.value	-8401
	.value	2
	.value	-10174
	.value	-9212
	.value	1
	.value	-29237
	.value	1
	.value	-29252
	.value	1
	.value	-29200
	.value	2
	.value	-10174
	.value	-8994
	.value	1
	.value	-28972
	.value	2
	.value	-10143
	.value	-8750
	.value	2
	.value	-10143
	.value	-8723
	.value	1
	.value	-28524
	.value	1
	.value	-28431
	.value	1
	.value	-28399
	.value	2
	.value	-10143
	.value	-8402
	.value	1
	.value	-28389
	.value	1
	.value	-28104
	.value	1
	.value	-27945
	.value	1
	.value	-27944
	.value	1
	.value	-28036
	.value	1
	.value	-27655
	.value	1
	.value	-27627
	.value	2
	.value	-10142
	.value	-8198
	.value	1
	.value	-27253
	.value	1
	.value	18837
	.value	1
	.value	-27209
	.value	2
	.value	-10141
	.value	-8841
	.value	1
	.value	18918
	.value	1
	.value	-26941
	.value	1
	.value	23986
	.value	1
	.value	-26845
	.value	2
	.value	-10140
	.value	-8891
	.value	2
	.value	-10140
	.value	-8678
	.value	1
	.value	19054
	.value	1
	.value	19062
	.value	1
	.value	-26656
	.value	2
	.value	-10139
	.value	-9206
	.value	1
	.value	19122
	.value	2
	.value	-10139
	.value	-9066
	.value	1
	.value	-26583
	.value	2
	.value	-10139
	.value	-8778
	.value	1
	.value	-26398
	.value	1
	.value	19251
	.value	1
	.value	-26327
	.value	1
	.value	-26201
	.value	1
	.value	-26174
	.value	1
	.value	-26114
	.value	1
	.value	19406
	.value	2
	.value	-10138
	.value	-8400
	.value	1
	.value	-25536
	.value	1
	.value	-25347
	.value	1
	.value	19662
	.value	1
	.value	19693
	.value	1
	.value	-25241
	.value	2
	.value	-10136
	.value	-9010
	.value	1
	.value	19704
	.value	2
	.value	-10136
	.value	-8955
	.value	2
	.value	-10136
	.value	-8690
	.value	2
	.value	-10136
	.value	-8559
	.value	1
	.value	-24901
	.value	1
	.value	19798
	.value	1
	.value	-24839
	.value	1
	.value	-24834
	.value	1
	.value	-24827
	.value	1
	.value	-24817
	.value	1
	.value	-24810
	.value	1
	.value	-24773
	.value	2
	.value	-10135
	.value	-8704
	.value	940
	.value	-6590
	.value	945
	.value	769
	.value	941
	.value	-6590
	.value	949
	.value	769
	.value	942
	.value	-6590
	.value	951
	.value	769
	.value	943
	.value	-6590
	.value	953
	.value	769
	.value	972
	.value	-6590
	.value	959
	.value	769
	.value	973
	.value	-6590
	.value	965
	.value	769
	.value	974
	.value	-6590
	.value	969
	.value	769
	.value	902
	.value	-6590
	.value	913
	.value	769
	.value	904
	.value	-6590
	.value	917
	.value	769
	.value	905
	.value	-6590
	.value	919
	.value	769
	.value	912
	.value	1
	.value	-6589
	.value	953
	.value	776
	.value	769
	.value	906
	.value	-6590
	.value	921
	.value	769
	.value	944
	.value	1
	.value	-6589
	.value	965
	.value	776
	.value	769
	.value	910
	.value	-6590
	.value	933
	.value	769
	.value	901
	.value	-6590
	.value	168
	.value	769
	.value	908
	.value	-6590
	.value	927
	.value	769
	.value	911
	.value	-6590
	.value	937
	.value	769
	.value	197
	.value	-6590
	.value	65
	.value	778
	.value	-6426
	.value	-6527
	.value	768
	.value	-6426
	.value	-6527
	.value	769
	.value	-6426
	.value	-6527
	.value	787
	.value	-6426
	.value	-6526
	.value	776
	.value	769
	.value	-32512
	.value	-32126
	.value	3953
	.value	3954
	.value	-32512
	.value	-31614
	.value	3953
	.value	3956
	.value	-32512
	.value	-32126
	.value	3953
	.value	3968
	.zero	2
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL19norm2_nfc_data_trie, @object
	.size	_ZL19norm2_nfc_data_trie, 48
_ZL19norm2_nfc_data_trie:
	.quad	_ZL24norm2_nfc_data_trieIndex
	.quad	_ZL23norm2_nfc_data_trieData
	.long	1746
	.long	7892
	.long	195584
	.value	48
	.byte	0
	.byte	0
	.long	0
	.value	0
	.value	196
	.long	550
	.long	1
	.section	.rodata
	.align 32
	.type	_ZL23norm2_nfc_data_trieData, @object
	.size	_ZL23norm2_nfc_data_trieData, 15784
_ZL23norm2_nfc_data_trieData:
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	4
	.value	8
	.value	12
	.value	1
	.value	1
	.value	16
	.value	80
	.value	92
	.value	112
	.value	136
	.value	204
	.value	208
	.value	236
	.value	264
	.value	324
	.value	328
	.value	348
	.value	372
	.value	384
	.value	420
	.value	484
	.value	1
	.value	492
	.value	524
	.value	552
	.value	580
	.value	656
	.value	664
	.value	688
	.value	696
	.value	732
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	756
	.value	820
	.value	832
	.value	852
	.value	876
	.value	944
	.value	948
	.value	976
	.value	1008
	.value	1064
	.value	1072
	.value	1092
	.value	1116
	.value	1128
	.value	1164
	.value	1228
	.value	1
	.value	1236
	.value	1268
	.value	1296
	.value	1328
	.value	1404
	.value	1412
	.value	1440
	.value	1448
	.value	1488
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1512
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	4746
	.value	4752
	.value	2788
	.value	4758
	.value	2810
	.value	2820
	.value	1524
	.value	2830
	.value	4764
	.value	4770
	.value	2840
	.value	4776
	.value	4782
	.value	4788
	.value	4794
	.value	2862
	.value	1
	.value	4800
	.value	4806
	.value	4812
	.value	2872
	.value	2894
	.value	2912
	.value	1
	.value	1532
	.value	4818
	.value	4824
	.value	4830
	.value	2922
	.value	4836
	.value	1
	.value	1
	.value	4842
	.value	4848
	.value	2944
	.value	4854
	.value	2966
	.value	2976
	.value	1536
	.value	2986
	.value	4860
	.value	4866
	.value	2996
	.value	4872
	.value	4878
	.value	4884
	.value	4890
	.value	3018
	.value	1
	.value	4896
	.value	4902
	.value	4908
	.value	3028
	.value	3050
	.value	3068
	.value	1
	.value	1544
	.value	4914
	.value	4920
	.value	4926
	.value	3078
	.value	4932
	.value	1
	.value	4938
	.value	4944
	.value	4950
	.value	3100
	.value	3122
	.value	4957
	.value	4963
	.value	4968
	.value	4974
	.value	4980
	.value	4986
	.value	4992
	.value	4998
	.value	5004
	.value	5010
	.value	5016
	.value	5022
	.value	1
	.value	1
	.value	3144
	.value	3158
	.value	5028
	.value	5034
	.value	5040
	.value	5046
	.value	5053
	.value	5059
	.value	5064
	.value	5070
	.value	5076
	.value	5082
	.value	5088
	.value	5094
	.value	5100
	.value	5106
	.value	5113
	.value	5119
	.value	5124
	.value	5130
	.value	1
	.value	1
	.value	5136
	.value	5142
	.value	5148
	.value	5154
	.value	5160
	.value	5166
	.value	5173
	.value	5179
	.value	5184
	.value	1
	.value	1
	.value	1
	.value	5191
	.value	5197
	.value	5203
	.value	5209
	.value	1
	.value	5214
	.value	5220
	.value	5227
	.value	5233
	.value	5238
	.value	5244
	.value	1
	.value	1
	.value	1
	.value	5250
	.value	5256
	.value	5263
	.value	5269
	.value	5274
	.value	5280
	.value	1
	.value	1
	.value	1
	.value	3172
	.value	3186
	.value	5286
	.value	5292
	.value	5298
	.value	5304
	.value	1
	.value	1
	.value	5310
	.value	5316
	.value	5323
	.value	5329
	.value	5334
	.value	5340
	.value	3200
	.value	3210
	.value	5346
	.value	5352
	.value	5359
	.value	5365
	.value	3220
	.value	3230
	.value	5371
	.value	5377
	.value	5382
	.value	5388
	.value	1
	.value	1
	.value	3240
	.value	3250
	.value	3260
	.value	3270
	.value	5394
	.value	5400
	.value	5406
	.value	5412
	.value	5418
	.value	5424
	.value	5431
	.value	5437
	.value	5442
	.value	5448
	.value	5454
	.value	5460
	.value	5466
	.value	5472
	.value	5478
	.value	5484
	.value	5490
	.value	5496
	.value	5502
	.value	1548
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	3280
	.value	3306
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	3332
	.value	3358
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1552
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	5508
	.value	5514
	.value	5520
	.value	5526
	.value	5532
	.value	5538
	.value	5544
	.value	5550
	.value	5558
	.value	5568
	.value	5578
	.value	5588
	.value	5598
	.value	5608
	.value	5618
	.value	5628
	.value	1
	.value	5638
	.value	5648
	.value	5658
	.value	5668
	.value	5677
	.value	5683
	.value	1
	.value	1
	.value	5688
	.value	5694
	.value	5700
	.value	5706
	.value	3384
	.value	3394
	.value	5715
	.value	5725
	.value	5733
	.value	5739
	.value	5745
	.value	1
	.value	1
	.value	1
	.value	5750
	.value	5756
	.value	1
	.value	1
	.value	5762
	.value	5768
	.value	5776
	.value	5786
	.value	5795
	.value	5801
	.value	5807
	.value	5813
	.value	5818
	.value	5824
	.value	5830
	.value	5836
	.value	5842
	.value	5848
	.value	5854
	.value	5860
	.value	5866
	.value	5872
	.value	5878
	.value	5884
	.value	5890
	.value	5896
	.value	5902
	.value	5908
	.value	5914
	.value	5920
	.value	5926
	.value	5932
	.value	5938
	.value	5944
	.value	5950
	.value	5956
	.value	5962
	.value	5968
	.value	5974
	.value	5980
	.value	1
	.value	1
	.value	5986
	.value	5992
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	3404
	.value	3414
	.value	3424
	.value	3434
	.value	6000
	.value	6010
	.value	6020
	.value	6030
	.value	3444
	.value	3454
	.value	6040
	.value	6050
	.value	6058
	.value	6064
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1556
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-564
	.value	-564
	.value	-564
	.value	-564
	.value	-564
	.value	-52
	.value	-564
	.value	-564
	.value	-564
	.value	-564
	.value	-564
	.value	-564
	.value	-564
	.value	-52
	.value	-52
	.value	-564
	.value	-52
	.value	-564
	.value	-52
	.value	-564
	.value	-564
	.value	-48
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-48
	.value	-592
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-108
	.value	-108
	.value	-584
	.value	-584
	.value	-584
	.value	-584
	.value	-620
	.value	-620
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-584
	.value	-584
	.value	-72
	.value	-584
	.value	-584
	.value	-72
	.value	-72
	.value	-510
	.value	-510
	.value	-510
	.value	-510
	.value	-1022
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	15414
	.value	15420
	.value	-564
	.value	15426
	.value	15432
	.value	-544
	.value	-52
	.value	-72
	.value	-72
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-72
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-52
	.value	-48
	.value	-72
	.value	-72
	.value	-52
	.value	-46
	.value	-44
	.value	-44
	.value	-46
	.value	-44
	.value	-44
	.value	-46
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10721
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10725
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	6071
	.value	6077
	.value	10729
	.value	6083
	.value	6089
	.value	6095
	.value	1
	.value	6101
	.value	1
	.value	6107
	.value	6113
	.value	6121
	.value	1560
	.value	1
	.value	1
	.value	1
	.value	1588
	.value	1
	.value	1604
	.value	1
	.value	1624
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1652
	.value	1
	.value	1668
	.value	1
	.value	1
	.value	1
	.value	1672
	.value	1
	.value	1
	.value	1
	.value	1696
	.value	6129
	.value	6135
	.value	3464
	.value	6141
	.value	3474
	.value	6147
	.value	6155
	.value	1716
	.value	1
	.value	1
	.value	1
	.value	1748
	.value	1
	.value	1764
	.value	1
	.value	1788
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1820
	.value	1
	.value	1836
	.value	1
	.value	1
	.value	1
	.value	1844
	.value	1
	.value	1
	.value	1
	.value	1876
	.value	3484
	.value	3502
	.value	6163
	.value	6169
	.value	3520
	.value	1
	.value	1
	.value	1
	.value	1900
	.value	6175
	.value	6181
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	6187
	.value	6193
	.value	1
	.value	6199
	.value	1
	.value	1
	.value	1908
	.value	6205
	.value	1
	.value	1
	.value	1
	.value	1
	.value	6211
	.value	6217
	.value	6223
	.value	1
	.value	1912
	.value	1
	.value	1
	.value	1920
	.value	1
	.value	1924
	.value	1936
	.value	1944
	.value	1948
	.value	6229
	.value	1964
	.value	1
	.value	1
	.value	1
	.value	1968
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1972
	.value	1
	.value	1
	.value	1
	.value	1988
	.value	1
	.value	1
	.value	1
	.value	1992
	.value	1
	.value	1996
	.value	1
	.value	1
	.value	2000
	.value	1
	.value	1
	.value	2008
	.value	1
	.value	2012
	.value	2024
	.value	2032
	.value	2036
	.value	6235
	.value	2052
	.value	1
	.value	1
	.value	1
	.value	2056
	.value	1
	.value	1
	.value	1
	.value	2060
	.value	1
	.value	1
	.value	1
	.value	2076
	.value	1
	.value	1
	.value	1
	.value	2080
	.value	1
	.value	2084
	.value	1
	.value	1
	.value	6241
	.value	6247
	.value	1
	.value	6253
	.value	1
	.value	1
	.value	2088
	.value	6259
	.value	1
	.value	1
	.value	1
	.value	1
	.value	6265
	.value	6271
	.value	6277
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2092
	.value	2096
	.value	6283
	.value	6289
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	6295
	.value	6301
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	6307
	.value	6313
	.value	6319
	.value	6325
	.value	1
	.value	1
	.value	6331
	.value	6337
	.value	2100
	.value	2104
	.value	6343
	.value	6349
	.value	6355
	.value	6361
	.value	6367
	.value	6373
	.value	1
	.value	1
	.value	6379
	.value	6385
	.value	6391
	.value	6397
	.value	6403
	.value	6409
	.value	2108
	.value	2112
	.value	6415
	.value	6421
	.value	6427
	.value	6433
	.value	6439
	.value	6445
	.value	6451
	.value	6457
	.value	6463
	.value	6469
	.value	6475
	.value	6481
	.value	1
	.value	1
	.value	6487
	.value	6493
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-68
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-68
	.value	-56
	.value	-52
	.value	-492
	.value	-490
	.value	-488
	.value	-486
	.value	-484
	.value	-482
	.value	-480
	.value	-478
	.value	-476
	.value	-474
	.value	-474
	.value	-472
	.value	-470
	.value	-468
	.value	1
	.value	-466
	.value	1
	.value	-464
	.value	-462
	.value	1
	.value	-52
	.value	-72
	.value	1
	.value	-476
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-452
	.value	-450
	.value	-448
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	6498
	.value	6504
	.value	6511
	.value	6517
	.value	6523
	.value	2116
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2128
	.value	1
	.value	2132
	.value	-458
	.value	-456
	.value	-454
	.value	-452
	.value	-450
	.value	-448
	.value	-446
	.value	-444
	.value	-564
	.value	-564
	.value	-584
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-442
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	6529
	.value	2136
	.value	6535
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2140
	.value	6541
	.value	1
	.value	2144
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	1
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-440
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	-72
	.value	-72
	.value	-52
	.value	-72
	.value	-72
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-72
	.value	-52
	.value	-72
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	-72
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-72
	.value	-72
	.value	-458
	.value	-456
	.value	-454
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2148
	.value	6547
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2152
	.value	6553
	.value	1
	.value	2156
	.value	6559
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1010
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	10732
	.value	10738
	.value	10744
	.value	10750
	.value	10756
	.value	10762
	.value	10768
	.value	10774
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2160
	.value	1
	.value	1
	.value	1
	.value	6565
	.value	6571
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10780
	.value	10786
	.value	1
	.value	10792
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10798
	.value	1
	.value	1
	.value	10804
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10810
	.value	10816
	.value	10822
	.value	1
	.value	1
	.value	10828
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2168
	.value	6577
	.value	1
	.value	1
	.value	6583
	.value	6589
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10834
	.value	10840
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2180
	.value	1
	.value	6595
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2184
	.value	2192
	.value	1
	.value	1
	.value	6601
	.value	6607
	.value	6613
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2196
	.value	1
	.value	6619
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-344
	.value	-842
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	1
	.value	1
	.value	2200
	.value	6625
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	2204
	.value	6631
	.value	6637
	.value	1
	.value	3530
	.value	6645
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	-494
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2216
	.value	2224
	.value	1
	.value	1
	.value	6653
	.value	6659
	.value	6665
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1006
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2228
	.value	6671
	.value	1
	.value	3540
	.value	6679
	.value	6687
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-306
	.value	-306
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-298
	.value	-298
	.value	-298
	.value	-298
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-276
	.value	-276
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-268
	.value	-268
	.value	-268
	.value	-268
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	1
	.value	-72
	.value	1
	.value	-80
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10847
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10853
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10859
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10865
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10871
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10877
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-254
	.value	-252
	.value	15440
	.value	-248
	.value	15448
	.value	10882
	.value	1
	.value	10888
	.value	1
	.value	-252
	.value	-252
	.value	-252
	.value	-252
	.value	1
	.value	1
	.value	-252
	.value	15456
	.value	-52
	.value	-52
	.value	-494
	.value	1
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10895
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10901
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10907
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10913
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10919
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10925
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2240
	.value	6693
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	1
	.value	-494
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	2
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	-512
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-56
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-68
	.value	-52
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	1
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2244
	.value	6699
	.value	2248
	.value	6705
	.value	2252
	.value	6711
	.value	2256
	.value	6717
	.value	2260
	.value	6723
	.value	1
	.value	1
	.value	2264
	.value	6729
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2268
	.value	6735
	.value	2272
	.value	6741
	.value	2276
	.value	2280
	.value	6747
	.value	6753
	.value	2284
	.value	6759
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	-510
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-52
	.value	-52
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-52
	.value	1
	.value	-510
	.value	-510
	.value	-510
	.value	-510
	.value	-510
	.value	-510
	.value	-510
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-52
	.value	-44
	.value	-84
	.value	-72
	.value	-108
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-48
	.value	-56
	.value	-56
	.value	-72
	.value	1
	.value	-52
	.value	-46
	.value	-72
	.value	-52
	.value	-72
	.value	6764
	.value	6770
	.value	6776
	.value	6782
	.value	6789
	.value	6795
	.value	6801
	.value	6807
	.value	6815
	.value	6825
	.value	6832
	.value	6838
	.value	6844
	.value	6850
	.value	6856
	.value	6862
	.value	6869
	.value	6875
	.value	6880
	.value	6886
	.value	6894
	.value	6904
	.value	6914
	.value	6924
	.value	6932
	.value	6938
	.value	6944
	.value	6950
	.value	6959
	.value	6969
	.value	6977
	.value	6983
	.value	6988
	.value	6994
	.value	7000
	.value	7006
	.value	7012
	.value	7018
	.value	7024
	.value	7030
	.value	7037
	.value	7043
	.value	7048
	.value	7054
	.value	7060
	.value	7066
	.value	7074
	.value	7084
	.value	7092
	.value	7098
	.value	7104
	.value	7110
	.value	7116
	.value	7122
	.value	3550
	.value	3560
	.value	7130
	.value	7140
	.value	7148
	.value	7154
	.value	7160
	.value	7166
	.value	7172
	.value	7178
	.value	7184
	.value	7190
	.value	7197
	.value	7203
	.value	7208
	.value	7214
	.value	7220
	.value	7226
	.value	7232
	.value	7238
	.value	7244
	.value	7250
	.value	7258
	.value	7268
	.value	7278
	.value	7288
	.value	7298
	.value	7308
	.value	7318
	.value	7328
	.value	7337
	.value	7343
	.value	7349
	.value	7355
	.value	7360
	.value	7366
	.value	3570
	.value	3580
	.value	7374
	.value	7384
	.value	7392
	.value	7398
	.value	7404
	.value	7410
	.value	3590
	.value	3600
	.value	7418
	.value	7428
	.value	7438
	.value	7448
	.value	7458
	.value	7468
	.value	7476
	.value	7482
	.value	7488
	.value	7494
	.value	7500
	.value	7506
	.value	7512
	.value	7518
	.value	7524
	.value	7530
	.value	7536
	.value	7542
	.value	7548
	.value	7554
	.value	7562
	.value	7572
	.value	7582
	.value	7592
	.value	7600
	.value	7606
	.value	7613
	.value	7619
	.value	7624
	.value	7630
	.value	7636
	.value	7642
	.value	7648
	.value	7654
	.value	7660
	.value	7666
	.value	7673
	.value	7679
	.value	7685
	.value	7691
	.value	7697
	.value	7703
	.value	7708
	.value	7714
	.value	7720
	.value	7726
	.value	7733
	.value	7739
	.value	7745
	.value	7751
	.value	7756
	.value	7762
	.value	7768
	.value	7774
	.value	1
	.value	7781
	.value	1
	.value	1
	.value	1
	.value	1
	.value	3610
	.value	3624
	.value	7786
	.value	7792
	.value	7800
	.value	7810
	.value	7820
	.value	7830
	.value	7840
	.value	7850
	.value	7860
	.value	7870
	.value	7880
	.value	7890
	.value	7900
	.value	7910
	.value	7920
	.value	7930
	.value	7940
	.value	7950
	.value	7960
	.value	7970
	.value	7980
	.value	7990
	.value	3638
	.value	3648
	.value	7998
	.value	8004
	.value	8010
	.value	8016
	.value	8024
	.value	8034
	.value	8044
	.value	8054
	.value	8064
	.value	8074
	.value	8084
	.value	8094
	.value	8104
	.value	8114
	.value	8122
	.value	8128
	.value	8134
	.value	8140
	.value	3658
	.value	3668
	.value	8146
	.value	8152
	.value	8160
	.value	8170
	.value	8180
	.value	8190
	.value	8200
	.value	8210
	.value	8220
	.value	8230
	.value	8240
	.value	8250
	.value	8260
	.value	8270
	.value	8280
	.value	8290
	.value	8300
	.value	8310
	.value	8320
	.value	8330
	.value	8340
	.value	8350
	.value	8358
	.value	8364
	.value	8370
	.value	8376
	.value	8384
	.value	8394
	.value	8404
	.value	8414
	.value	8424
	.value	8434
	.value	8444
	.value	8454
	.value	8464
	.value	8474
	.value	8482
	.value	8488
	.value	8495
	.value	8501
	.value	8506
	.value	8512
	.value	8518
	.value	8524
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	3678
	.value	3700
	.value	3724
	.value	3738
	.value	3752
	.value	3766
	.value	3780
	.value	3794
	.value	3806
	.value	3828
	.value	3852
	.value	3866
	.value	3880
	.value	3894
	.value	3908
	.value	3922
	.value	3934
	.value	3948
	.value	8533
	.value	8543
	.value	8553
	.value	8563
	.value	1
	.value	1
	.value	3962
	.value	3976
	.value	8573
	.value	8583
	.value	8593
	.value	8603
	.value	1
	.value	1
	.value	3990
	.value	4012
	.value	4036
	.value	4050
	.value	4064
	.value	4078
	.value	4092
	.value	4106
	.value	4118
	.value	4140
	.value	4164
	.value	4178
	.value	4192
	.value	4206
	.value	4220
	.value	4234
	.value	4246
	.value	4264
	.value	8613
	.value	8623
	.value	8633
	.value	8643
	.value	8653
	.value	8663
	.value	4282
	.value	4300
	.value	8673
	.value	8683
	.value	8693
	.value	8703
	.value	8713
	.value	8723
	.value	4318
	.value	4332
	.value	8733
	.value	8743
	.value	8753
	.value	8763
	.value	1
	.value	1
	.value	4346
	.value	4360
	.value	8773
	.value	8783
	.value	8793
	.value	8803
	.value	1
	.value	1
	.value	4374
	.value	4392
	.value	8813
	.value	8823
	.value	8833
	.value	8843
	.value	8853
	.value	8863
	.value	1
	.value	4410
	.value	1
	.value	8873
	.value	1
	.value	8883
	.value	1
	.value	8893
	.value	4428
	.value	4450
	.value	4474
	.value	4488
	.value	4502
	.value	4516
	.value	4530
	.value	4544
	.value	4556
	.value	4578
	.value	4602
	.value	4616
	.value	4630
	.value	4644
	.value	4658
	.value	4672
	.value	4684
	.value	15262
	.value	8901
	.value	15270
	.value	4694
	.value	15278
	.value	8907
	.value	15286
	.value	8913
	.value	15294
	.value	8919
	.value	15302
	.value	4704
	.value	15310
	.value	1
	.value	1
	.value	8926
	.value	8936
	.value	8951
	.value	8967
	.value	8983
	.value	8999
	.value	9015
	.value	9031
	.value	9042
	.value	9052
	.value	9067
	.value	9083
	.value	9099
	.value	9115
	.value	9131
	.value	9147
	.value	9158
	.value	9168
	.value	9183
	.value	9199
	.value	9215
	.value	9231
	.value	9247
	.value	9263
	.value	9274
	.value	9284
	.value	9299
	.value	9315
	.value	9331
	.value	9347
	.value	9363
	.value	9379
	.value	9390
	.value	9400
	.value	9415
	.value	9431
	.value	9447
	.value	9463
	.value	9479
	.value	9495
	.value	9506
	.value	9516
	.value	9531
	.value	9547
	.value	9563
	.value	9579
	.value	9595
	.value	9611
	.value	9621
	.value	9627
	.value	9635
	.value	9642
	.value	9651
	.value	1
	.value	4714
	.value	9661
	.value	9669
	.value	9675
	.value	9681
	.value	15318
	.value	9686
	.value	1
	.value	10930
	.value	2288
	.value	1
	.value	9693
	.value	9701
	.value	9708
	.value	9717
	.value	1
	.value	4724
	.value	9727
	.value	9735
	.value	15326
	.value	9741
	.value	15334
	.value	9746
	.value	9753
	.value	9759
	.value	9765
	.value	9771
	.value	9777
	.value	9785
	.value	15344
	.value	1
	.value	1
	.value	9793
	.value	9801
	.value	9809
	.value	9815
	.value	9821
	.value	15354
	.value	1
	.value	9827
	.value	9833
	.value	9839
	.value	9845
	.value	9851
	.value	9859
	.value	15364
	.value	9867
	.value	9873
	.value	9879
	.value	9887
	.value	9895
	.value	9901
	.value	9907
	.value	15374
	.value	9913
	.value	9919
	.value	15382
	.value	10935
	.value	1
	.value	1
	.value	9927
	.value	9934
	.value	9943
	.value	1
	.value	4734
	.value	9953
	.value	9961
	.value	15390
	.value	9967
	.value	15398
	.value	9972
	.value	10939
	.value	2300
	.value	1
	.value	-1527
	.value	-1527
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-510
	.value	-510
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-510
	.value	-510
	.value	-510
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	-510
	.value	-510
	.value	-52
	.value	-72
	.value	-52
	.value	-510
	.value	-510
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10942
	.value	1
	.value	1
	.value	1
	.value	10946
	.value	15406
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2312
	.value	1
	.value	2316
	.value	1
	.value	2320
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	9979
	.value	9985
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	9991
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	9997
	.value	10003
	.value	10009
	.value	2324
	.value	1
	.value	2328
	.value	1
	.value	2332
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2336
	.value	10015
	.value	1
	.value	1
	.value	1
	.value	2340
	.value	10021
	.value	1
	.value	2344
	.value	10027
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2348
	.value	10033
	.value	2352
	.value	10039
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2356
	.value	1
	.value	1
	.value	1
	.value	10045
	.value	1
	.value	2360
	.value	10051
	.value	2364
	.value	1
	.value	10057
	.value	2368
	.value	10063
	.value	1
	.value	1
	.value	1
	.value	2372
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10069
	.value	2376
	.value	10075
	.value	1
	.value	2380
	.value	2384
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10081
	.value	10087
	.value	10093
	.value	10099
	.value	10105
	.value	2388
	.value	2392
	.value	10111
	.value	10117
	.value	2396
	.value	2400
	.value	10123
	.value	10129
	.value	2404
	.value	2408
	.value	2412
	.value	2416
	.value	1
	.value	1
	.value	10135
	.value	10141
	.value	2420
	.value	2424
	.value	10147
	.value	10153
	.value	2428
	.value	2432
	.value	10159
	.value	10165
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2436
	.value	2440
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2444
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2448
	.value	2452
	.value	1
	.value	2456
	.value	10171
	.value	10177
	.value	10183
	.value	10189
	.value	1
	.value	1
	.value	2460
	.value	2464
	.value	2468
	.value	2472
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10195
	.value	10201
	.value	10207
	.value	10213
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10219
	.value	10225
	.value	10231
	.value	10237
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10951
	.value	10955
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10959
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-76
	.value	-56
	.value	-48
	.value	-68
	.value	-64
	.value	-64
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2476
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2480
	.value	10243
	.value	2484
	.value	10249
	.value	2488
	.value	10255
	.value	2492
	.value	10261
	.value	2496
	.value	10267
	.value	2500
	.value	10273
	.value	2504
	.value	10279
	.value	2508
	.value	10285
	.value	2512
	.value	10291
	.value	2516
	.value	10297
	.value	2520
	.value	10303
	.value	2524
	.value	10309
	.value	1
	.value	2528
	.value	10315
	.value	2532
	.value	10321
	.value	2536
	.value	10327
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2540
	.value	10333
	.value	10339
	.value	2548
	.value	10345
	.value	10351
	.value	2556
	.value	10357
	.value	10363
	.value	2564
	.value	10369
	.value	10375
	.value	2572
	.value	10381
	.value	10387
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10393
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1008
	.value	-1008
	.value	1
	.value	1
	.value	2580
	.value	10399
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2584
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2588
	.value	10405
	.value	2592
	.value	10411
	.value	2596
	.value	10417
	.value	2600
	.value	10423
	.value	2604
	.value	10429
	.value	2608
	.value	10435
	.value	2612
	.value	10441
	.value	2616
	.value	10447
	.value	2620
	.value	10453
	.value	2624
	.value	10459
	.value	2628
	.value	10465
	.value	2632
	.value	10471
	.value	1
	.value	2636
	.value	10477
	.value	2640
	.value	10483
	.value	2644
	.value	10489
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2648
	.value	10495
	.value	10501
	.value	2656
	.value	10507
	.value	10513
	.value	2664
	.value	10519
	.value	10525
	.value	2672
	.value	10531
	.value	10537
	.value	2680
	.value	10543
	.value	10549
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2688
	.value	2692
	.value	2696
	.value	2700
	.value	1
	.value	10555
	.value	1
	.value	1
	.value	10561
	.value	10567
	.value	10573
	.value	10579
	.value	1
	.value	1
	.value	2704
	.value	10585
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	-72
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	1
	.value	-52
	.value	-52
	.value	-72
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	1
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2786
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	2786
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	2786
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	2786
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	4745
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	15462
	.value	1
	.value	15462
	.value	15462
	.value	15462
	.value	15462
	.value	15462
	.value	15462
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	15462
	.value	15462
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	15462
	.value	1
	.value	1
	.value	1
	.value	1
	.value	15462
	.value	1
	.value	1
	.value	1
	.value	15462
	.value	1
	.value	15462
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	15255
	.value	1
	.value	10965
	.value	10969
	.value	10973
	.value	10977
	.value	10981
	.value	10985
	.value	10989
	.value	10993
	.value	10993
	.value	10997
	.value	11001
	.value	11005
	.value	11009
	.value	11013
	.value	11017
	.value	11021
	.value	11025
	.value	11029
	.value	11033
	.value	11037
	.value	11041
	.value	11045
	.value	11049
	.value	11053
	.value	11057
	.value	11061
	.value	11065
	.value	11069
	.value	11073
	.value	11077
	.value	11081
	.value	11085
	.value	11089
	.value	11093
	.value	11097
	.value	11101
	.value	11105
	.value	11109
	.value	11113
	.value	11117
	.value	11121
	.value	11125
	.value	11129
	.value	11133
	.value	11137
	.value	11141
	.value	11145
	.value	11149
	.value	11153
	.value	11157
	.value	11161
	.value	11165
	.value	11169
	.value	11173
	.value	11177
	.value	11181
	.value	11185
	.value	11189
	.value	11193
	.value	11197
	.value	11201
	.value	11205
	.value	11209
	.value	11213
	.value	11217
	.value	11221
	.value	11225
	.value	11229
	.value	11233
	.value	11237
	.value	11241
	.value	11245
	.value	11249
	.value	11253
	.value	11257
	.value	11261
	.value	11265
	.value	11269
	.value	11273
	.value	11277
	.value	11281
	.value	11285
	.value	11289
	.value	11293
	.value	11297
	.value	11301
	.value	11305
	.value	11309
	.value	11313
	.value	11317
	.value	11321
	.value	11325
	.value	11041
	.value	11329
	.value	11333
	.value	11337
	.value	11341
	.value	11345
	.value	11349
	.value	11353
	.value	11357
	.value	11361
	.value	11365
	.value	11369
	.value	11373
	.value	11377
	.value	11381
	.value	11385
	.value	11389
	.value	11393
	.value	11397
	.value	11401
	.value	11405
	.value	11409
	.value	11413
	.value	11417
	.value	11421
	.value	11425
	.value	11429
	.value	11433
	.value	11437
	.value	11441
	.value	11445
	.value	11449
	.value	11453
	.value	11457
	.value	11461
	.value	11465
	.value	11469
	.value	11473
	.value	11477
	.value	11481
	.value	11485
	.value	11489
	.value	11493
	.value	11497
	.value	11501
	.value	11505
	.value	11509
	.value	11513
	.value	11517
	.value	11521
	.value	11525
	.value	11529
	.value	11533
	.value	11537
	.value	11541
	.value	11545
	.value	11549
	.value	11553
	.value	11557
	.value	11561
	.value	11565
	.value	11569
	.value	11573
	.value	11577
	.value	11581
	.value	11585
	.value	11589
	.value	11593
	.value	11597
	.value	11401
	.value	11601
	.value	11605
	.value	11609
	.value	11613
	.value	11617
	.value	11621
	.value	11625
	.value	11629
	.value	11337
	.value	11633
	.value	11637
	.value	11641
	.value	11645
	.value	11649
	.value	11653
	.value	11657
	.value	11661
	.value	11665
	.value	11669
	.value	11673
	.value	11677
	.value	11681
	.value	11685
	.value	11689
	.value	11693
	.value	11697
	.value	11701
	.value	11705
	.value	11709
	.value	11041
	.value	11713
	.value	11717
	.value	11721
	.value	11725
	.value	11729
	.value	11733
	.value	11737
	.value	11741
	.value	11745
	.value	11749
	.value	11753
	.value	11757
	.value	11761
	.value	11765
	.value	11769
	.value	11773
	.value	11777
	.value	11781
	.value	11785
	.value	11789
	.value	11793
	.value	11797
	.value	11801
	.value	11805
	.value	11809
	.value	11813
	.value	11817
	.value	11345
	.value	11821
	.value	11825
	.value	11829
	.value	11833
	.value	11837
	.value	11841
	.value	11845
	.value	11849
	.value	11853
	.value	11857
	.value	11861
	.value	11865
	.value	11869
	.value	11873
	.value	11877
	.value	11881
	.value	11885
	.value	11889
	.value	11893
	.value	11897
	.value	11901
	.value	11905
	.value	11909
	.value	11913
	.value	11917
	.value	11921
	.value	11925
	.value	11929
	.value	11933
	.value	11937
	.value	11941
	.value	11945
	.value	11949
	.value	11953
	.value	11957
	.value	11961
	.value	11965
	.value	11969
	.value	11973
	.value	11977
	.value	11981
	.value	11985
	.value	11989
	.value	11993
	.value	11997
	.value	12001
	.value	12005
	.value	12009
	.value	12013
	.value	12017
	.value	1
	.value	1
	.value	12021
	.value	1
	.value	12025
	.value	1
	.value	1
	.value	12029
	.value	12033
	.value	12037
	.value	12041
	.value	12045
	.value	12049
	.value	12053
	.value	12057
	.value	12061
	.value	12065
	.value	1
	.value	12069
	.value	1
	.value	12073
	.value	1
	.value	1
	.value	12077
	.value	12081
	.value	1
	.value	1
	.value	1
	.value	12085
	.value	12089
	.value	12093
	.value	12097
	.value	12101
	.value	12105
	.value	12109
	.value	12113
	.value	12117
	.value	12121
	.value	12125
	.value	12129
	.value	12133
	.value	12137
	.value	12141
	.value	12145
	.value	12149
	.value	12153
	.value	12157
	.value	12161
	.value	12165
	.value	12169
	.value	12173
	.value	12177
	.value	12181
	.value	12185
	.value	12189
	.value	12193
	.value	12197
	.value	12201
	.value	12205
	.value	12209
	.value	12213
	.value	12217
	.value	12221
	.value	12225
	.value	12229
	.value	12233
	.value	12237
	.value	12241
	.value	12245
	.value	12249
	.value	12253
	.value	12257
	.value	12261
	.value	11557
	.value	12265
	.value	12269
	.value	12273
	.value	12277
	.value	12281
	.value	12285
	.value	12285
	.value	12289
	.value	12293
	.value	12297
	.value	12301
	.value	12305
	.value	12309
	.value	12313
	.value	12317
	.value	12077
	.value	12321
	.value	12325
	.value	12329
	.value	12333
	.value	12337
	.value	12343
	.value	1
	.value	1
	.value	12347
	.value	12351
	.value	12355
	.value	12359
	.value	12363
	.value	12367
	.value	12371
	.value	12375
	.value	12133
	.value	12379
	.value	12383
	.value	12387
	.value	12021
	.value	12391
	.value	12395
	.value	12399
	.value	12403
	.value	12407
	.value	12411
	.value	12415
	.value	12419
	.value	12423
	.value	12427
	.value	12431
	.value	12435
	.value	12169
	.value	12439
	.value	12173
	.value	12443
	.value	12447
	.value	12451
	.value	12455
	.value	12459
	.value	12025
	.value	11125
	.value	12463
	.value	12467
	.value	12471
	.value	11405
	.value	11753
	.value	12475
	.value	12479
	.value	12201
	.value	12483
	.value	12205
	.value	12487
	.value	12491
	.value	12495
	.value	12033
	.value	12499
	.value	12503
	.value	12507
	.value	12511
	.value	12515
	.value	12037
	.value	12519
	.value	12523
	.value	12527
	.value	12531
	.value	12535
	.value	12539
	.value	12261
	.value	12543
	.value	12547
	.value	11557
	.value	12551
	.value	12277
	.value	12555
	.value	12559
	.value	12563
	.value	12567
	.value	12571
	.value	12297
	.value	12575
	.value	12073
	.value	12579
	.value	12301
	.value	11329
	.value	12583
	.value	12305
	.value	12587
	.value	12313
	.value	12591
	.value	12595
	.value	12599
	.value	12603
	.value	12607
	.value	12321
	.value	12057
	.value	12611
	.value	12325
	.value	12615
	.value	12329
	.value	12619
	.value	10993
	.value	12623
	.value	12629
	.value	12635
	.value	12641
	.value	12645
	.value	12649
	.value	12653
	.value	12659
	.value	12665
	.value	12671
	.value	12675
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	12678
	.value	-460
	.value	12684
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	12690
	.value	12696
	.value	12704
	.value	12714
	.value	12722
	.value	12728
	.value	12734
	.value	12740
	.value	12746
	.value	12752
	.value	12758
	.value	12764
	.value	12770
	.value	1
	.value	12776
	.value	12782
	.value	12788
	.value	12794
	.value	12800
	.value	1
	.value	12806
	.value	1
	.value	12812
	.value	12818
	.value	1
	.value	12824
	.value	12830
	.value	1
	.value	12836
	.value	12842
	.value	12848
	.value	12854
	.value	12860
	.value	12866
	.value	12872
	.value	12878
	.value	12884
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	1
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-510
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	-72
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-52
	.value	-72
	.value	-72
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2708
	.value	10591
	.value	2714
	.value	10601
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2720
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10611
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	-1010
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	10621
	.value	10631
	.value	1
	.value	2726
	.value	2732
	.value	-494
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	-498
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	-498
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2738
	.value	1
	.value	1
	.value	1
	.value	10641
	.value	10651
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	1
	.value	1
	.value	1
	.value	-498
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2750
	.value	-1024
	.value	10661
	.value	10671
	.value	-1024
	.value	10681
	.value	1
	.value	1
	.value	-494
	.value	-498
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2768
	.value	2774
	.value	10691
	.value	10701
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	-498
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	-498
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-1024
	.value	1
	.value	1
	.value	1
	.value	1
	.value	2780
	.value	1
	.value	1
	.value	10711
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-494
	.value	-494
	.value	1
	.value	-510
	.value	-510
	.value	-510
	.value	-510
	.value	-510
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-500
	.value	-500
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-510
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	12890
	.value	12900
	.value	12920
	.value	12944
	.value	12968
	.value	12992
	.value	13016
	.value	-80
	.value	-80
	.value	-510
	.value	-510
	.value	-510
	.value	1
	.value	1
	.value	1
	.value	-60
	.value	-80
	.value	-80
	.value	-80
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-72
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	13030
	.value	13040
	.value	13060
	.value	13084
	.value	13108
	.value	13132
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	-52
	.value	-52
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	-72
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-52
	.value	-498
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	13147
	.value	13151
	.value	13155
	.value	13159
	.value	13165
	.value	12109
	.value	13169
	.value	13173
	.value	13177
	.value	13181
	.value	12113
	.value	13185
	.value	13189
	.value	13193
	.value	12117
	.value	13199
	.value	13203
	.value	13207
	.value	13211
	.value	13217
	.value	13221
	.value	13225
	.value	13229
	.value	13235
	.value	13239
	.value	13243
	.value	13247
	.value	12351
	.value	13251
	.value	13257
	.value	13261
	.value	13265
	.value	13269
	.value	13273
	.value	13277
	.value	13281
	.value	13285
	.value	12371
	.value	12121
	.value	12125
	.value	12375
	.value	13289
	.value	13293
	.value	11353
	.value	13297
	.value	12129
	.value	13301
	.value	13305
	.value	13309
	.value	13313
	.value	13313
	.value	13313
	.value	13317
	.value	13323
	.value	13327
	.value	13331
	.value	13335
	.value	13341
	.value	13345
	.value	13349
	.value	13353
	.value	13357
	.value	13361
	.value	13365
	.value	13369
	.value	13373
	.value	13377
	.value	13381
	.value	13385
	.value	13389
	.value	13389
	.value	12383
	.value	13393
	.value	13397
	.value	13401
	.value	13405
	.value	12137
	.value	13409
	.value	13413
	.value	13417
	.value	11965
	.value	13421
	.value	13425
	.value	13429
	.value	13433
	.value	13437
	.value	13441
	.value	13445
	.value	13449
	.value	13453
	.value	13459
	.value	13463
	.value	13467
	.value	13471
	.value	13475
	.value	13479
	.value	13483
	.value	13489
	.value	13495
	.value	13499
	.value	13503
	.value	13507
	.value	13511
	.value	13515
	.value	13519
	.value	13523
	.value	13527
	.value	13527
	.value	13531
	.value	13537
	.value	13541
	.value	11337
	.value	13545
	.value	13549
	.value	13555
	.value	13559
	.value	13563
	.value	13567
	.value	13571
	.value	13575
	.value	12157
	.value	13579
	.value	13583
	.value	13587
	.value	13593
	.value	13597
	.value	13603
	.value	13607
	.value	13611
	.value	13615
	.value	13619
	.value	13623
	.value	13627
	.value	13631
	.value	13635
	.value	13639
	.value	13643
	.value	13647
	.value	13653
	.value	13657
	.value	13661
	.value	13665
	.value	11121
	.value	13669
	.value	13675
	.value	13679
	.value	13679
	.value	13685
	.value	13689
	.value	13689
	.value	13693
	.value	13697
	.value	13703
	.value	13709
	.value	13713
	.value	13717
	.value	13721
	.value	13725
	.value	13729
	.value	13733
	.value	13737
	.value	13741
	.value	13745
	.value	12161
	.value	13749
	.value	13755
	.value	13759
	.value	13763
	.value	12431
	.value	13763
	.value	13767
	.value	12169
	.value	13771
	.value	13775
	.value	13779
	.value	13783
	.value	12173
	.value	11013
	.value	13787
	.value	13791
	.value	13795
	.value	13799
	.value	13803
	.value	13807
	.value	13811
	.value	13817
	.value	13821
	.value	13825
	.value	13829
	.value	13833
	.value	13837
	.value	13843
	.value	13847
	.value	13851
	.value	13855
	.value	13859
	.value	13863
	.value	13867
	.value	13871
	.value	13875
	.value	12177
	.value	13879
	.value	13883
	.value	13889
	.value	13893
	.value	13897
	.value	13901
	.value	12185
	.value	13905
	.value	13909
	.value	13913
	.value	13917
	.value	13921
	.value	13925
	.value	13929
	.value	13933
	.value	11125
	.value	12463
	.value	13937
	.value	13941
	.value	13945
	.value	13949
	.value	13955
	.value	13959
	.value	13963
	.value	13967
	.value	12189
	.value	13971
	.value	13977
	.value	13981
	.value	13985
	.value	12641
	.value	13989
	.value	13993
	.value	13997
	.value	14001
	.value	14005
	.value	14011
	.value	14015
	.value	14019
	.value	14023
	.value	14029
	.value	14033
	.value	14037
	.value	14041
	.value	11405
	.value	14045
	.value	14049
	.value	14055
	.value	14061
	.value	14067
	.value	14071
	.value	14077
	.value	14081
	.value	14085
	.value	14089
	.value	14093
	.value	12193
	.value	11753
	.value	14097
	.value	14101
	.value	14105
	.value	14109
	.value	14115
	.value	14119
	.value	14123
	.value	14127
	.value	12479
	.value	14131
	.value	14135
	.value	14141
	.value	14145
	.value	14149
	.value	14155
	.value	14161
	.value	14165
	.value	12483
	.value	14169
	.value	14173
	.value	14177
	.value	14181
	.value	14185
	.value	14189
	.value	14193
	.value	14199
	.value	14203
	.value	14209
	.value	14213
	.value	14219
	.value	12491
	.value	14223
	.value	14227
	.value	14233
	.value	14237
	.value	14241
	.value	14247
	.value	14253
	.value	14257
	.value	14261
	.value	14265
	.value	14269
	.value	14269
	.value	14273
	.value	14277
	.value	12499
	.value	14281
	.value	14285
	.value	14289
	.value	14293
	.value	14297
	.value	14303
	.value	14307
	.value	11349
	.value	14313
	.value	14319
	.value	14323
	.value	14329
	.value	14335
	.value	14341
	.value	14345
	.value	12523
	.value	14349
	.value	14355
	.value	14361
	.value	14367
	.value	14373
	.value	14377
	.value	14377
	.value	12527
	.value	12649
	.value	14381
	.value	14385
	.value	14389
	.value	14393
	.value	14399
	.value	11197
	.value	12535
	.value	14403
	.value	14407
	.value	12237
	.value	14413
	.value	14419
	.value	12053
	.value	14425
	.value	14429
	.value	12253
	.value	14433
	.value	14437
	.value	14441
	.value	14447
	.value	14447
	.value	14453
	.value	14457
	.value	14461
	.value	14467
	.value	14471
	.value	14475
	.value	14479
	.value	14485
	.value	14489
	.value	14493
	.value	14497
	.value	14501
	.value	14505
	.value	14511
	.value	14515
	.value	14519
	.value	14523
	.value	14527
	.value	14531
	.value	14535
	.value	14541
	.value	14547
	.value	14551
	.value	14557
	.value	14561
	.value	14567
	.value	14571
	.value	12277
	.value	14575
	.value	14581
	.value	14587
	.value	14591
	.value	14597
	.value	14601
	.value	14607
	.value	14611
	.value	14615
	.value	14619
	.value	14623
	.value	14627
	.value	14631
	.value	14637
	.value	14643
	.value	14649
	.value	13685
	.value	14655
	.value	14659
	.value	14663
	.value	14667
	.value	14671
	.value	14675
	.value	14679
	.value	14683
	.value	14687
	.value	14691
	.value	14695
	.value	14699
	.value	11421
	.value	14705
	.value	14709
	.value	14713
	.value	14717
	.value	14721
	.value	14725
	.value	12289
	.value	14729
	.value	14733
	.value	14737
	.value	14741
	.value	14745
	.value	14751
	.value	14757
	.value	14763
	.value	14767
	.value	14771
	.value	14775
	.value	14779
	.value	14785
	.value	14789
	.value	14795
	.value	14799
	.value	14803
	.value	14809
	.value	14815
	.value	14819
	.value	11177
	.value	14823
	.value	14827
	.value	14831
	.value	14835
	.value	14839
	.value	14843
	.value	12563
	.value	14847
	.value	14851
	.value	14855
	.value	14859
	.value	14863
	.value	14867
	.value	14871
	.value	14875
	.value	14879
	.value	14883
	.value	14889
	.value	14893
	.value	14897
	.value	14901
	.value	14905
	.value	14909
	.value	14915
	.value	14921
	.value	14925
	.value	14929
	.value	12583
	.value	12587
	.value	14933
	.value	14937
	.value	14943
	.value	14947
	.value	14951
	.value	14955
	.value	14959
	.value	14965
	.value	14971
	.value	14975
	.value	14979
	.value	14983
	.value	14989
	.value	12591
	.value	14993
	.value	14999
	.value	15005
	.value	15009
	.value	15013
	.value	15017
	.value	15023
	.value	15027
	.value	15031
	.value	15035
	.value	15039
	.value	15043
	.value	15047
	.value	15051
	.value	15057
	.value	15061
	.value	15065
	.value	15069
	.value	15075
	.value	15079
	.value	15083
	.value	15087
	.value	15091
	.value	15097
	.value	15103
	.value	15107
	.value	15111
	.value	15115
	.value	15121
	.value	15125
	.value	12615
	.value	12615
	.value	15131
	.value	15135
	.value	15141
	.value	15145
	.value	15149
	.value	15153
	.value	15157
	.value	15161
	.value	15165
	.value	15169
	.value	12619
	.value	15175
	.value	15179
	.value	15183
	.value	15187
	.value	15191
	.value	15195
	.value	15201
	.value	15205
	.value	15211
	.value	15217
	.value	15223
	.value	15227
	.value	15231
	.value	15235
	.value	15239
	.value	15243
	.value	15247
	.value	15251
	.value	15255
	.value	1
	.value	1
	.align 32
	.type	_ZL24norm2_nfc_data_trieIndex, @object
	.size	_ZL24norm2_nfc_data_trieIndex, 3492
_ZL24norm2_nfc_data_trieIndex:
	.value	0
	.value	64
	.value	123
	.value	187
	.value	251
	.value	314
	.value	378
	.value	434
	.value	498
	.value	550
	.value	596
	.value	550
	.value	660
	.value	724
	.value	787
	.value	851
	.value	915
	.value	978
	.value	1039
	.value	1102
	.value	550
	.value	550
	.value	1160
	.value	1224
	.value	1272
	.value	1328
	.value	550
	.value	1392
	.value	1439
	.value	1502
	.value	550
	.value	1523
	.value	1585
	.value	1631
	.value	550
	.value	1676
	.value	1740
	.value	1801
	.value	1833
	.value	1896
	.value	1959
	.value	2020
	.value	2051
	.value	2112
	.value	1833
	.value	2169
	.value	2215
	.value	2278
	.value	550
	.value	2336
	.value	2359
	.value	2423
	.value	2446
	.value	2509
	.value	550
	.value	2563
	.value	2595
	.value	2654
	.value	2666
	.value	2725
	.value	2765
	.value	2826
	.value	2890
	.value	2948
	.value	2975
	.value	550
	.value	3034
	.value	550
	.value	3098
	.value	3129
	.value	3183
	.value	3244
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	3279
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	3323
	.value	550
	.value	550
	.value	3376
	.value	550
	.value	550
	.value	3406
	.value	550
	.value	3448
	.value	550
	.value	550
	.value	550
	.value	3508
	.value	3540
	.value	3604
	.value	3667
	.value	3726
	.value	3790
	.value	3842
	.value	3886
	.value	2056
	.value	550
	.value	550
	.value	3938
	.value	550
	.value	550
	.value	550
	.value	4002
	.value	4066
	.value	4130
	.value	4194
	.value	4258
	.value	4322
	.value	4386
	.value	4450
	.value	4514
	.value	4578
	.value	550
	.value	550
	.value	4626
	.value	4675
	.value	550
	.value	4723
	.value	4774
	.value	4835
	.value	4898
	.value	4962
	.value	5016
	.value	5062
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	5105
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	3261
	.value	550
	.value	5134
	.value	550
	.value	5198
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	5262
	.value	5320
	.value	5382
	.value	5446
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	5509
	.value	5571
	.value	5603
	.value	550
	.value	550
	.value	550
	.value	550
	.value	5661
	.value	550
	.value	550
	.value	5701
	.value	5751
	.value	5797
	.value	2060
	.value	5816
	.value	550
	.value	550
	.value	5832
	.value	5896
	.value	550
	.value	550
	.value	550
	.value	5152
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	5980
	.value	5960
	.value	5968
	.value	5976
	.value	5984
	.value	5964
	.value	5972
	.value	6036
	.value	550
	.value	6100
	.value	6159
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	6223
	.value	6287
	.value	6351
	.value	6415
	.value	6479
	.value	6543
	.value	6607
	.value	6671
	.value	6706
	.value	6770
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	6802
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	1621
	.value	1636
	.value	1660
	.value	1691
	.value	1712
	.value	1712
	.value	1712
	.value	1716
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	3034
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	1359
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	1036
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	6853
	.value	550
	.value	550
	.value	6869
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	3526
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	6885
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	5590
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	6895
	.value	1359
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	2027
	.value	550
	.value	550
	.value	2490
	.value	550
	.value	6911
	.value	6924
	.value	6936
	.value	550
	.value	550
	.value	550
	.value	550
	.value	1044
	.value	550
	.value	6947
	.value	6963
	.value	550
	.value	550
	.value	550
	.value	2016
	.value	550
	.value	550
	.value	550
	.value	550
	.value	6979
	.value	550
	.value	550
	.value	550
	.value	6990
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	6997
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7008
	.value	7023
	.value	2294
	.value	7037
	.value	1042
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7051
	.value	1944
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7067
	.value	7082
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	2262
	.value	7090
	.value	7106
	.value	550
	.value	550
	.value	550
	.value	2490
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7116
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	2022
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7113
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7132
	.value	2016
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	2125
	.value	550
	.value	550
	.value	550
	.value	2029
	.value	2026
	.value	550
	.value	550
	.value	550
	.value	550
	.value	2024
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	2490
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	3028
	.value	550
	.value	550
	.value	550
	.value	550
	.value	2026
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7148
	.value	550
	.value	550
	.value	550
	.value	3835
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7164
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7166
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7181
	.value	7197
	.value	7211
	.value	7224
	.value	550
	.value	7236
	.value	7250
	.value	7266
	.value	550
	.value	550
	.value	550
	.value	550
	.value	3306
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7282
	.value	7290
	.value	7304
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	3835
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	1276
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7320
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7332
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	7348
	.value	7364
	.value	7380
	.value	7396
	.value	7412
	.value	7428
	.value	7444
	.value	7460
	.value	7476
	.value	7492
	.value	7508
	.value	7524
	.value	7540
	.value	7556
	.value	7572
	.value	7588
	.value	7604
	.value	7620
	.value	7636
	.value	7652
	.value	7668
	.value	7684
	.value	7700
	.value	7716
	.value	7732
	.value	7748
	.value	7764
	.value	7780
	.value	7796
	.value	7812
	.value	7828
	.value	7844
	.value	7860
	.value	7876
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	550
	.value	1032
	.value	1064
	.value	196
	.value	196
	.value	196
	.value	1096
	.value	1111
	.value	1133
	.value	1161
	.value	1190
	.value	1218
	.value	1247
	.value	1276
	.value	1307
	.value	1336
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	1362
	.value	196
	.value	1382
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	1414
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	1425
	.value	1454
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	1486
	.value	1506
	.value	196
	.value	196
	.value	1525
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	196
	.value	1557
	.value	1589
	.align 32
	.type	_ZL22norm2_nfc_data_indexes, @object
	.size	_ZL22norm2_nfc_data_indexes, 80
_ZL22norm2_nfc_data_indexes:
	.long	80
	.long	19372
	.long	34836
	.long	35092
	.long	35092
	.long	35092
	.long	35092
	.long	35092
	.long	192
	.long	768
	.long	2786
	.long	10720
	.long	15462
	.long	64512
	.long	4744
	.long	15260
	.long	15412
	.long	15462
	.long	768
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
