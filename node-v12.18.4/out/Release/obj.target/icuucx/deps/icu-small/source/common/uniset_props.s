	.file	"uniset_props.cpp"
	.text
	.p2align 4
	.type	uset_cleanup, @function
uset_cleanup:
.LFB3344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZL14uni32Singleton(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L2
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L2:
	movq	$0, _ZL14uni32Singleton(%rip)
	movl	$1, %eax
	movl	$0, _ZL13uni32InitOnce(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3344:
	.size	uset_cleanup, .-uset_cleanup
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_1L13versionFilterEiPv, @function
_ZN6icu_6712_GLOBAL__N_1L13versionFilterEiPv:
.LFB3376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-28(%rbp), %r13
	movq	%rsi, %r12
	movq	%r13, %rsi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	u_charAge_67@PLT
	xorl	%eax, %eax
	cmpb	$0, -28(%rbp)
	je	.L8
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	setle	%al
.L8:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L15
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3376:
	.size	_ZN6icu_6712_GLOBAL__N_1L13versionFilterEiPv, .-_ZN6icu_6712_GLOBAL__N_1L13versionFilterEiPv
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_1L18numericValueFilterEiPv, @function
_ZN6icu_6712_GLOBAL__N_1L18numericValueFilterEiPv:
.LFB3374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	u_getNumericValue_67@PLT
	ucomisd	(%rbx), %xmm0
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3374:
	.size	_ZN6icu_6712_GLOBAL__N_1L18numericValueFilterEiPv, .-_ZN6icu_6712_GLOBAL__N_1L18numericValueFilterEiPv
	.align 2
	.p2align 4
	.type	_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode.part.0, @function
_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode.part.0:
.LFB4532:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%rdi, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -80(%rbp)
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movq	%rbx, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	%eax, -56(%rbp)
	testl	%eax, %eax
	jle	.L26
	movl	$0, -52(%rbp)
	movl	$-1, %r15d
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-72(%rbp), %rbx
	movl	-52(%rbp), %esi
	movq	%rbx, %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	-52(%rbp), %esi
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	cmpl	%eax, %r14d
	jg	.L20
	leal	1(%rax), %ebx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L33:
	testl	%r15d, %r15d
	cmovs	%r14d, %r15d
.L22:
	addl	$1, %r14d
	cmpl	%ebx, %r14d
	je	.L20
.L23:
	movq	%r13, %rsi
	movl	%r14d, %edi
	call	*%r12
	testb	%al, %al
	jne	.L33
	testl	%r15d, %r15d
	js	.L22
	movq	-64(%rbp), %rdi
	leal	-1(%r14), %edx
	movl	%r15d, %esi
	addl	$1, %r14d
	movl	$-1, %r15d
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	cmpl	%ebx, %r14d
	jne	.L23
	.p2align 4,,10
	.p2align 3
.L20:
	addl	$1, -52(%rbp)
	movl	-52(%rbp), %eax
	cmpl	%eax, -56(%rbp)
	jne	.L25
	testl	%r15d, %r15d
	jns	.L34
.L26:
	movq	-64(%rbp), %rax
	testb	$1, 32(%rax)
	je	.L18
	movq	-80(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L35
.L18:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
	movq	-64(%rbp), %rdi
	movl	$1114111, %edx
	movl	%r15d, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L26
.L35:
	movl	$7, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4532:
	.size	_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode.part.0, .-_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode.part.0
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3832:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3832:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3835:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L49
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L37
	cmpb	$0, 12(%rbx)
	jne	.L50
.L41:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L41
	.cfi_endproc
.LFE3835:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3838:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L53
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3838:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3841:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L56
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3841:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L62
.L58:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L63
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3843:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3844:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3844:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3845:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3845:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3846:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3846:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3847:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3847:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3848:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3848:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3849:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L79
	testl	%edx, %edx
	jle	.L79
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L82
.L71:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L71
	.cfi_endproc
.LFE3849:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L86
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L86
	testl	%r12d, %r12d
	jg	.L93
	cmpb	$0, 12(%rbx)
	jne	.L94
.L88:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L88
.L94:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L86:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3850:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L96
	movq	(%rdi), %r8
.L97:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L100
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L100
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L100:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3851:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3852:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L107
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3852:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3853:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3853:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3854:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3854:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3855:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3855:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3857:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3857:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3859:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3859:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet16resemblesPatternERKNS_13UnicodeStringEi
	.type	_ZN6icu_6710UnicodeSet16resemblesPatternERKNS_13UnicodeStringEi, @function
_ZN6icu_6710UnicodeSet16resemblesPatternERKNS_13UnicodeStringEi:
.LFB3364:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %edx
	leal	1(%rsi), %ecx
	testw	%dx, %dx
	js	.L114
	movswl	%dx, %eax
	sarl	$5, %eax
.L115:
	cmpl	%eax, %ecx
	jge	.L116
	xorl	%r8d, %r8d
	cmpl	%esi, %eax
	jbe	.L113
	andw	$2, %dx
	leaq	10(%rdi), %r11
	je	.L138
.L119:
	movslq	%esi, %r9
	movl	$1, %r8d
	cmpw	$91, (%r11,%r9,2)
	leaq	(%r9,%r9), %r10
	je	.L113
	addl	$4, %esi
	xorl	%r8d, %r8d
	cmpl	%esi, %eax
	jg	.L125
.L113:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	leal	4(%rsi), %r9d
	xorl	%r8d, %r8d
	cmpl	%r9d, %eax
	jle	.L113
	cmpl	%esi, %eax
	jbe	.L113
	movslq	%esi, %rsi
	andl	$2, %edx
	leaq	(%rsi,%rsi), %r10
.L125:
	testw	%dx, %dx
	je	.L121
	movzwl	10(%rdi,%r10), %edx
	addq	$10, %rdi
	xorl	%r8d, %r8d
	cmpw	$91, %dx
	je	.L139
.L123:
	cmpw	$92, %dx
	jne	.L113
	cmpl	%ecx, %eax
	jbe	.L113
	movslq	%ecx, %rcx
	movl	$1, %r8d
	movzwl	(%rdi,%rcx,2), %eax
	andl	$-33, %eax
	cmpw	$80, %ax
	je	.L113
	cmpw	$78, (%rdi,%rcx,2)
	sete	%r8b
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L114:
	movl	12(%rdi), %eax
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L121:
	movq	24(%rdi), %rdi
	xorl	%r8d, %r8d
	movzwl	(%rdi,%r10), %edx
	cmpw	$91, %dx
	jne	.L123
.L139:
	cmpl	%eax, %ecx
	jnb	.L113
	movslq	%ecx, %rcx
	cmpw	$58, (%rdi,%rcx,2)
	sete	%r8b
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	movq	24(%rdi), %r11
	jmp	.L119
	.cfi_endproc
.LFE3364:
	.size	_ZN6icu_6710UnicodeSet16resemblesPatternERKNS_13UnicodeStringEi, .-_ZN6icu_6710UnicodeSet16resemblesPatternERKNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode
	.type	_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode, @function
_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode:
.LFB3379:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L142
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	jmp	_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode.part.0
	.cfi_endproc
.LFE3379:
	.size	_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode, .-_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode
	.type	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode, @function
_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode:
.LFB3381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jg	.L145
	cmpq	$0, 40(%rdi)
	je	.L209
.L145:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L145
	movl	%esi, %r13d
	movl	%edx, %ebx
	movq	%rcx, %r15
	cmpl	$8192, %esi
	je	.L210
	cmpl	$28672, %esi
	je	.L211
	cmpl	$64, %esi
	ja	.L164
	cmpl	$1, %edx
	jbe	.L212
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$8192, %edi
	movq	%rcx, %rsi
	call	_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode@PLT
	movl	(%r15), %edi
	movq	%rax, -64(%rbp)
	testl	%edi, %edi
	jg	.L145
	movq	%r12, %rdi
	movl	$-1, %r14d
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movq	-64(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	$0, -56(%rbp)
	movl	%eax, -72(%rbp)
	testl	%eax, %eax
	jle	.L170
	.p2align 4,,10
	.p2align 3
.L148:
	movl	-56(%rbp), %esi
	movq	-64(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	-56(%rbp), %esi
	movq	-64(%rbp), %rdi
	movl	%eax, %r13d
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	cmpl	%eax, %r13d
	jg	.L150
	addl	$1, %eax
	movl	%eax, -52(%rbp)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L213:
	testl	%r14d, %r14d
	cmovs	%r13d, %r14d
.L152:
	addl	$1, %r13d
	cmpl	-52(%rbp), %r13d
	je	.L150
.L153:
	movl	%r13d, %edi
	call	u_charType_67@PLT
	btl	%eax, %ebx
	jc	.L213
	testl	%r14d, %r14d
	js	.L152
	movl	%r14d, %esi
	leal	-1(%r13), %edx
	movq	%r12, %rdi
	movl	$-1, %r14d
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L150:
	addl	$1, -56(%rbp)
	movl	-56(%rbp), %eax
	cmpl	%eax, -72(%rbp)
	jne	.L148
.L208:
	testl	%r14d, %r14d
	js	.L170
	movl	$1114111, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
.L170:
	testb	$1, 32(%r12)
	je	.L145
	movl	(%r15), %eax
	testl	%eax, %eax
	jg	.L145
	movl	$7, (%r15)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L164:
	leal	-4096(%rsi), %eax
	cmpl	$24, %eax
	jbe	.L214
	movl	$1, (%rcx)
	jmp	.L145
.L211:
	movq	%rcx, %rsi
	movl	$28672, %edi
	call	_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode@PLT
	movl	(%r15), %esi
	movq	%rax, -64(%rbp)
	testl	%esi, %esi
	jg	.L145
	movq	%r12, %rdi
	movl	$-1, %r14d
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movq	-64(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	$0, -56(%rbp)
	movl	%eax, -72(%rbp)
	testl	%eax, %eax
	jle	.L170
	.p2align 4,,10
	.p2align 3
.L156:
	movl	-56(%rbp), %esi
	movq	-64(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	-56(%rbp), %esi
	movq	-64(%rbp), %rdi
	movl	%eax, %r13d
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	cmpl	%eax, %r13d
	jg	.L158
	addl	$1, %eax
	movl	%eax, -52(%rbp)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L215:
	testl	%r14d, %r14d
	cmovs	%r13d, %r14d
.L160:
	addl	$1, %r13d
	cmpl	-52(%rbp), %r13d
	je	.L158
.L161:
	movl	%ebx, %esi
	movl	%r13d, %edi
	call	uscript_hasScript_67@PLT
	testb	%al, %al
	jne	.L215
	testl	%r14d, %r14d
	js	.L160
	movl	%r14d, %esi
	leal	-1(%r13), %edx
	movq	%r12, %rdi
	movl	$-1, %r14d
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L158:
	addl	$1, -56(%rbp)
	movl	-56(%rbp), %eax
	cmpl	%eax, -72(%rbp)
	jne	.L156
	jmp	.L208
.L212:
	movq	%rcx, %rsi
	movl	%r13d, %edi
	call	u_getBinaryPropertySet_67@PLT
	movl	(%r15), %ecx
	testl	%ecx, %ecx
	jg	.L145
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet8copyFromERKS0_a@PLT
	testl	%ebx, %ebx
	jne	.L145
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	jmp	.L145
.L214:
	movq	%rcx, %rsi
	movl	%r13d, %edi
	call	_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode@PLT
	movl	(%r15), %edx
	movq	%rax, -72(%rbp)
	testl	%edx, %edx
	jg	.L145
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movq	-72(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movl	$0, -64(%rbp)
	movl	$-1, %r8d
	movl	%eax, -76(%rbp)
	testl	%eax, %eax
	jle	.L170
	.p2align 4,,10
	.p2align 3
.L169:
	movl	-64(%rbp), %esi
	movq	-72(%rbp), %rdi
	movl	%r8d, -52(%rbp)
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movl	-64(%rbp), %esi
	movq	-72(%rbp), %rdi
	movl	%eax, %r14d
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	-52(%rbp), %r8d
	cmpl	%eax, %r14d
	jg	.L171
	addl	$1, %eax
	movl	%eax, -56(%rbp)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L216:
	testl	%r8d, %r8d
	cmovs	%r14d, %r8d
.L173:
	addl	$1, %r14d
	cmpl	%r14d, -56(%rbp)
	je	.L171
.L174:
	movl	%r13d, %esi
	movl	%r14d, %edi
	movl	%r8d, -52(%rbp)
	call	u_getIntPropertyValue_67@PLT
	movl	-52(%rbp), %r8d
	cmpl	%ebx, %eax
	je	.L216
	testl	%r8d, %r8d
	js	.L173
	movl	%r8d, %esi
	leal	-1(%r14), %edx
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$-1, %r8d
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L171:
	addl	$1, -64(%rbp)
	movl	-64(%rbp), %eax
	cmpl	%eax, -76(%rbp)
	jne	.L169
	testl	%r8d, %r8d
	js	.L170
	movl	$1114111, %edx
	movl	%r8d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	jmp	.L170
	.cfi_endproc
.LFE3381:
	.size	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode, .-_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode.part.0, @function
_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode.part.0:
.LFB4533:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	8(%rsi), %eax
	testw	%ax, %ax
	js	.L218
	movswl	%ax, %esi
	sarl	$5, %esi
	testb	$17, %al
	jne	.L282
.L335:
	leaq	10(%r12), %rdi
	testb	$2, %al
	je	.L331
.L220:
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	je	.L222
	movzwl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L223
	movswl	%ax, %esi
	sarl	$5, %esi
	testb	$17, %al
	jne	.L283
.L333:
	leaq	10(%rbx), %rdi
	testb	$2, %al
	jne	.L225
	movq	24(%rbx), %rdi
.L225:
	call	uprv_isInvariantUString_67@PLT
	testb	%al, %al
	je	.L222
	leaq	-323(%rbp), %rax
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	movq	%rax, -336(%rbp)
	leaq	-336(%rbp), %rdi
	leaq	-259(%rbp), %rax
	movq	%r12, %rsi
	leaq	-272(%rbp), %r15
	movw	%r8w, -324(%rbp)
	movw	%r9w, -260(%rbp)
	movl	$0, -280(%rbp)
	movl	$40, -328(%rbp)
	movq	%rax, -272(%rbp)
	movl	$0, -216(%rbp)
	movl	$40, -264(%rbp)
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710CharString20appendInvariantCharsERKNS_13UnicodeStringER10UErrorCode@PLT
	movl	0(%r13), %r10d
	testl	%r10d, %r10d
	jg	.L228
	movswl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L230
	sarl	$5, %eax
.L231:
	movq	-336(%rbp), %rdi
	testl	%eax, %eax
	jle	.L232
	call	u_getPropertyEnum_67@PLT
	movl	%eax, %r12d
	cmpl	$-1, %eax
	je	.L268
	cmpl	$4101, %eax
	je	.L234
	leal	-4096(%rax), %eax
	cmpl	$24, %eax
	jbe	.L235
	cmpl	$64, %r12d
	jbe	.L235
	cmpl	$8192, %r12d
	je	.L234
	cmpl	$16389, %r12d
	je	.L245
	jg	.L246
	cmpl	$12288, %r12d
	je	.L247
	cmpl	$16384, %r12d
	jne	.L268
	movq	-272(%rbp), %rax
	movzbl	(%rax), %edx
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L263
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L266:
	cmpl	$126, %eax
	jg	.L268
	movslq	%eax, %rsi
	addl	$1, %eax
	movb	%dl, -192(%rbp,%rsi)
.L267:
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	testb	%dl, %dl
	je	.L265
.L263:
	cmpb	$32, %dl
	jne	.L266
	testl	%eax, %eax
	je	.L267
	movslq	%eax, %rsi
	cmpb	$32, -193(%rbp,%rsi)
	jne	.L266
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L222:
	movl	$1, 0(%r13)
.L217:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L332
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movq	24(%r12), %rdi
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L223:
	movl	12(%rbx), %esi
	testb	$17, %al
	je	.L333
.L283:
	xorl	%edi, %edi
	jmp	.L225
.L265:
	testl	%eax, %eax
	je	.L269
	movslq	%eax, %rdx
	leaq	-192(%rbp), %rsi
	cmpb	$32, -193(%rbp,%rdx)
	je	.L270
	leaq	(%rsi,%rdx), %rax
.L264:
	movb	$0, (%rax)
	leaq	-196(%rbp), %r12
	movq	%r12, %rdi
	call	u_versionFromString_67@PLT
	movq	%r13, %rsi
	movl	$16384, %edi
	call	_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode@PLT
	movl	0(%r13), %edx
	testl	%edx, %edx
	jg	.L228
	movq	%r13, %r8
	movq	%rax, %rcx
	movq	%r12, %rdx
	movq	%r14, %rdi
	leaq	_ZN6icu_6712_GLOBAL__N_1L13versionFilterEiPv(%rip), %rsi
	call	_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L228:
	cmpb	$0, -260(%rbp)
	jne	.L334
.L280:
	cmpb	$0, -324(%rbp)
	je	.L217
	movq	-336(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L218:
	movl	12(%rsi), %esi
	testb	$17, %al
	je	.L335
.L282:
	xorl	%edi, %edi
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L334:
	movq	-272(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L232:
	movq	%rdi, %rsi
	movl	$8192, %edi
	call	u_getPropertyValueEnum_67@PLT
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L336
	movq	%r13, %rcx
	movl	$8192, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode
.L243:
	testb	$1, 32(%r14)
	je	.L228
	movl	0(%r13), %eax
	testl	%eax, %eax
	jg	.L228
	movl	$7, 0(%r13)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L230:
	movl	12(%rbx), %eax
	jmp	.L231
.L339:
	leal	-4112(%r12), %eax
	cmpl	$1, %eax
	jbe	.L238
	.p2align 4,,10
	.p2align 3
.L268:
	movl	$1, 0(%r13)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L235:
	movq	-272(%rbp), %rsi
	movl	%r12d, %edi
	call	u_getPropertyValueEnum_67@PLT
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L337
.L237:
	movq	%r13, %rcx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L246:
	cmpl	$28672, %r12d
	jne	.L268
	movq	-272(%rbp), %rsi
	movl	$4106, %edi
	call	u_getPropertyValueEnum_67@PLT
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L268
	movq	%r13, %rcx
	movl	$28672, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L234:
	movq	-272(%rbp), %rsi
	movl	$8192, %edi
	movl	$8192, %r12d
	call	u_getPropertyValueEnum_67@PLT
	movl	%eax, %edx
	cmpl	$-1, %eax
	jne	.L237
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L336:
	movq	-336(%rbp), %rsi
	movl	$4106, %edi
	call	u_getPropertyValueEnum_67@PLT
	movl	%eax, %edx
	cmpl	$-1, %eax
	je	.L338
	movq	%r13, %rcx
	movl	$4106, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L247:
	movq	-272(%rbp), %rdi
	leaq	-352(%rbp), %rsi
	call	strtod@PLT
	movq	-352(%rbp), %rax
	movsd	%xmm0, -344(%rbp)
	cmpb	$0, (%rax)
	jne	.L268
	movq	%r13, %rsi
	movl	$12288, %edi
	call	_ZN6icu_6719CharacterProperties24getInclusionsForPropertyE9UPropertyR10UErrorCode@PLT
	movl	0(%r13), %esi
	movq	%rax, %rcx
	testl	%esi, %esi
	jg	.L228
	leaq	-344(%rbp), %rdx
	movq	%r13, %r8
	leaq	_ZN6icu_6712_GLOBAL__N_1L18numericValueFilterEiPv(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet11applyFilterEPFaiPvES1_PKS0_R10UErrorCode.part.0
	jmp	.L228
.L245:
	movq	-272(%rbp), %rax
	movzbl	(%rax), %edx
	leaq	1(%rax), %rcx
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L253
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L256:
	cmpl	$126, %eax
	jg	.L268
	movslq	%eax, %rsi
	addl	$1, %eax
	movb	%dl, -192(%rbp,%rsi)
.L257:
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	testb	%dl, %dl
	je	.L255
.L253:
	cmpb	$32, %dl
	jne	.L256
	testl	%eax, %eax
	je	.L257
	movslq	%eax, %rsi
	cmpb	$32, -193(%rbp,%rsi)
	jne	.L256
	jmp	.L257
.L337:
	cmpl	$4098, %r12d
	jne	.L339
.L238:
	movq	-272(%rbp), %rdi
	leaq	-344(%rbp), %rsi
	call	strtod@PLT
	movq	-344(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L268
	comisd	.LC0(%rip), %xmm0
	jb	.L268
	movsd	.LC1(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L268
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L268
	je	.L237
	jmp	.L268
.L255:
	testl	%eax, %eax
	je	.L259
	movslq	%eax, %rdx
	leaq	-192(%rbp), %rsi
	cmpb	$32, -193(%rbp,%rdx)
	je	.L260
	leaq	(%rsi,%rdx), %rax
.L254:
	movb	$0, (%rax)
	movq	%r13, %rdx
	movl	$2, %edi
	call	u_charFromName_67@PLT
	movl	0(%r13), %ecx
	movl	%eax, %r12d
	testl	%ecx, %ecx
	jg	.L268
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L228
.L338:
	movq	-336(%rbp), %rdi
	call	u_getPropertyEnum_67@PLT
	movl	%eax, %esi
	cmpl	$64, %eax
	ja	.L340
	movq	%r13, %rcx
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode
	jmp	.L243
.L259:
	leaq	-192(%rbp), %rsi
	movq	%rsi, %rax
	jmp	.L254
.L269:
	leaq	-192(%rbp), %rsi
	movq	%rsi, %rax
	jmp	.L264
.L340:
	movq	-336(%rbp), %rsi
	leaq	_ZL3ANY(%rip), %rdi
	call	uprv_compareASCIIPropertyNames_67@PLT
	testl	%eax, %eax
	je	.L341
	movq	-336(%rbp), %rsi
	leaq	_ZL5ASCII(%rip), %rdi
	call	uprv_compareASCIIPropertyNames_67@PLT
	testl	%eax, %eax
	je	.L342
	movq	-336(%rbp), %rsi
	leaq	_ZL8ASSIGNED(%rip), %rdi
	call	uprv_compareASCIIPropertyNames_67@PLT
	testl	%eax, %eax
	jne	.L268
	movq	%r14, %rdi
	movq	%r13, %rcx
	movl	$1, %edx
	movl	$8192, %esi
	call	_ZN6icu_6710UnicodeSet21applyIntPropertyValueE9UPropertyiR10UErrorCode
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	jmp	.L243
.L270:
	subl	$1, %eax
	cltq
	addq	%rsi, %rax
	jmp	.L264
.L260:
	subl	$1, %eax
	cltq
	addq	%rsi, %rax
	jmp	.L254
.L332:
	call	__stack_chk_fail@PLT
.L341:
	movl	$1114111, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3setEii@PLT
	jmp	.L228
.L342:
	movl	$127, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3setEii@PLT
	jmp	.L228
	.cfi_endproc
.LFE4533:
	.size	_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode.part.0, .-_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode
	.type	_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode, @function
_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode:
.LFB3382:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movl	(%rcx), %edi
	testl	%edi, %edi
	jg	.L346
	cmpq	$0, 40(%rax)
	je	.L349
.L346:
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	cmpq	$0, 88(%rax)
	jne	.L346
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rax, -8(%rbp)
	call	_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode.part.0
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3382:
	.size	_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode, .-_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERKNS_13UnicodeStringEi
	.type	_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERKNS_13UnicodeStringEi, @function
_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERKNS_13UnicodeStringEi:
.LFB3383:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %edx
	leal	4(%rsi), %ecx
	testw	%dx, %dx
	js	.L351
	movswl	%dx, %eax
	xorl	%r8d, %r8d
	sarl	$5, %eax
	cmpl	%eax, %ecx
	jge	.L350
.L353:
	xorl	%r8d, %r8d
	cmpl	%esi, %eax
	jbe	.L350
	andl	$2, %edx
	jne	.L369
	movq	24(%rdi), %rdi
.L356:
	movslq	%esi, %rdx
	leaq	(%rdx,%rdx), %rcx
	movzwl	(%rdi,%rdx,2), %edx
	cmpw	$91, %dx
	je	.L370
	xorl	%r8d, %r8d
	cmpw	$92, %dx
	je	.L371
.L350:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	addq	$10, %rdi
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L351:
	movl	12(%rdi), %eax
	xorl	%r8d, %r8d
	cmpl	%eax, %ecx
	jl	.L353
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	addl	$1, %esi
	cmpl	%esi, %eax
	jbe	.L358
	movzwl	2(%rdi,%rcx), %edx
	movl	$1, %r8d
	andl	$-33, %edx
	cmpw	$80, %dx
	je	.L350
.L358:
	xorl	%r8d, %r8d
	cmpl	%esi, %eax
	jbe	.L350
	cmpw	$78, 2(%rdi,%rcx)
	sete	%r8b
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L370:
	addl	$1, %esi
	xorl	%r8d, %r8d
	cmpl	%esi, %eax
	jbe	.L350
	cmpw	$58, 2(%rdi,%rcx)
	sete	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3383:
	.size	_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERKNS_13UnicodeStringEi, .-_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERKNS_13UnicodeStringEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERNS_21RuleCharacterIteratorEi
	.type	_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERNS_21RuleCharacterIteratorEi, @function
_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERNS_21RuleCharacterIteratorEi:
.LFB3384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	leaq	-85(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movq	%r14, %rsi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -84(%rbp)
	call	_ZNK6icu_6721RuleCharacterIterator6getPosERNS0_3PosE@PLT
	movl	%ebx, %esi
	leaq	-84(%rbp), %rcx
	movq	%r15, %rdx
	andl	$-3, %esi
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode@PLT
	movq	-104(%rbp), %rcx
	movl	%eax, %r13d
	subl	$91, %eax
	cmpl	$1, %eax
	jbe	.L373
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN6icu_6721RuleCharacterIterator6setPosERKNS0_3PosE@PLT
.L372:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L382
	addq	$72, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_restore_state
	andl	$-7, %ebx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode@PLT
	cmpl	$91, %r13d
	je	.L383
	movl	%eax, %edx
	andl	$-33, %edx
	cmpl	$80, %edx
	sete	%r13b
	cmpl	$78, %eax
	sete	%al
	orl	%eax, %r13d
.L376:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6721RuleCharacterIterator6setPosERKNS0_3PosE@PLT
	testb	%r13b, %r13b
	je	.L372
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	setle	%r13b
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L383:
	cmpl	$58, %eax
	sete	%r13b
	jmp	.L376
.L382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3384:
	.size	_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERNS_21RuleCharacterIteratorEi, .-_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERNS_21RuleCharacterIteratorEi
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"na"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet20applyPropertyPatternERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode
	.type	_ZN6icu_6710UnicodeSet20applyPropertyPatternERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode, @function
_ZN6icu_6710UnicodeSet20applyPropertyPatternERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode:
.LFB3385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L385
	movq	%rsi, %r13
	movl	8(%rdx), %eax
	movq	%rcx, %rbx
	movq	%rdx, %r14
	movzwl	8(%r13), %ecx
	leal	4(%rax), %esi
	testw	%cx, %cx
	js	.L386
	movswl	%cx, %edx
	sarl	$5, %edx
	cmpl	%edx, %esi
	jge	.L388
.L387:
	cmpl	%eax, %edx
	jbe	.L388
	andl	$2, %ecx
	leaq	10(%r13), %rcx
	jne	.L391
	movq	24(%r13), %rcx
.L391:
	movslq	%eax, %rsi
	leaq	(%rsi,%rsi), %r8
	movzwl	(%rcx,%rsi,2), %esi
	cmpw	$91, %si
	je	.L444
	cmpw	$92, %si
	jne	.L388
	leal	1(%rax), %r12d
	cmpl	%r12d, %edx
	jbe	.L388
	movzwl	2(%rcx,%r8), %r9d
	leaq	2(%r8), %rsi
	andl	$-33, %r9d
	cmpw	$80, %r9w
	jne	.L445
.L399:
	movzwl	(%rcx,%rsi), %r12d
	addl	$2, %eax
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	-260(%rbp), %rsi
	movl	%eax, -260(%rbp)
	call	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia@PLT
	movswl	8(%r13), %ecx
	movl	%ecx, %edx
	sarl	$5, %ecx
	testw	%dx, %dx
	jns	.L400
	movl	12(%r13), %ecx
.L400:
	cmpl	%ecx, %eax
	je	.L388
	leal	1(%rax), %esi
	movl	%esi, -260(%rbp)
	cmpl	%eax, %ecx
	jbe	.L388
	andl	$2, %edx
	leaq	10(%r13), %rdx
	jne	.L402
	movq	24(%r13), %rdx
.L402:
	cltq
	cmpw	$123, (%rdx,%rax,2)
	jne	.L388
	xorl	%edx, %edx
	testl	%esi, %esi
	js	.L403
	cmpl	%ecx, %esi
	cmovg	%ecx, %esi
	movl	%esi, %edx
.L403:
	cmpw	$80, %r12w
	movl	$125, %esi
	movq	%r13, %rdi
	sete	-280(%rbp)
	cmpw	$78, %r12w
	sete	-288(%rbp)
	subl	%edx, %ecx
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	$1, -300(%rbp)
	movl	%eax, %r12d
.L406:
	testl	%r12d, %r12d
	js	.L388
	movl	-260(%rbp), %ecx
	movzwl	8(%r13), %eax
	xorl	%edx, %edx
	testl	%ecx, %ecx
	js	.L407
	testw	%ax, %ax
	js	.L408
	movswl	%ax, %edx
	sarl	$5, %edx
.L409:
	cmpl	%edx, %ecx
	cmovle	%ecx, %edx
.L407:
	testw	%ax, %ax
	js	.L410
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L411:
	subl	%edx, %ecx
	movl	$61, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString9doIndexOfEDsii@PLT
	movl	$2, %ecx
	movl	$2, %esi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rdx
	movq	%rdx, -256(%rbp)
	movw	%cx, -248(%rbp)
	movq	%rdx, -192(%rbp)
	movw	%si, -184(%rbp)
	testl	%eax, %eax
	js	.L412
	cmpl	%eax, %r12d
	jle	.L412
	movq	0(%r13), %rdx
	cmpb	$0, -288(%rbp)
	movl	-260(%rbp), %esi
	movq	24(%rdx), %r9
	je	.L446
	leaq	-256(%rbp), %rax
	movl	%r12d, %edx
	movq	%r13, %rdi
	movq	%rax, -296(%rbp)
	movq	%rax, %rcx
	call	*%r9
.L422:
	leaq	-192(%rbp), %r8
	movq	-296(%rbp), %rsi
	leaq	-128(%rbp), %r13
	movq	%r8, %rdi
	movq	%r8, -288(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKciNS0_10EInvariantE@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN6icu_6713UnicodeStringaSEOS0_@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-288(%rbp), %r8
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L412:
	leaq	-256(%rbp), %rax
	movl	-260(%rbp), %esi
	movl	%r12d, %edx
	movq	%r13, %rdi
	movq	%rax, -296(%rbp)
	movq	%rax, %rcx
	movq	0(%r13), %rax
	call	*24(%rax)
	cmpb	$0, -288(%rbp)
	jne	.L422
	leaq	-192(%rbp), %r8
.L414:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L416
	cmpq	$0, 40(%r15)
	jne	.L420
	cmpq	$0, 88(%r15)
	jne	.L420
	movq	-296(%rbp), %rsi
	movq	%r8, %rdx
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	%r8, -288(%rbp)
	call	_ZN6icu_6710UnicodeSet18applyPropertyAliasERKNS_13UnicodeStringES3_R10UErrorCode.part.0
	movl	(%rbx), %eax
	movq	-288(%rbp), %r8
	testl	%eax, %eax
	jle	.L420
	.p2align 4,,10
	.p2align 3
.L416:
	movq	%r8, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-296(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L385:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L447
	addq	$264, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	movl	12(%r13), %edx
	cmpl	%edx, %esi
	jl	.L387
	.p2align 4,,10
	.p2align 3
.L388:
	movl	$1, (%rbx)
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L444:
	leal	1(%rax), %esi
	cmpl	%esi, %edx
	jbe	.L388
	cmpw	$58, 2(%rcx,%r8)
	jne	.L388
	addl	$2, %eax
	xorl	%edx, %edx
	leaq	-260(%rbp), %rsi
	movq	%r13, %rdi
	movl	%eax, -260(%rbp)
	call	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia@PLT
	movzwl	8(%r13), %edx
	movl	%eax, -260(%rbp)
	testw	%dx, %dx
	js	.L393
	movswl	%dx, %r9d
	sarl	$5, %r9d
.L394:
	movb	$0, -280(%rbp)
	cmpl	%r9d, %eax
	jge	.L395
	cmpl	%eax, %r9d
	jbe	.L395
	andl	$2, %edx
	leaq	10(%r13), %rcx
	jne	.L397
	movq	24(%r13), %rcx
.L397:
	movslq	%eax, %rdx
	movb	$0, -280(%rbp)
	cmpw	$94, (%rcx,%rdx,2)
	jne	.L395
	addl	$1, %eax
	movb	$1, -280(%rbp)
	movl	%eax, -260(%rbp)
	.p2align 4,,10
	.p2align 3
.L395:
	xorl	%r8d, %r8d
	testl	%eax, %eax
	js	.L405
	cmpl	%eax, %r9d
	cmovle	%r9d, %eax
	movl	%eax, %r8d
.L405:
	subl	%r8d, %r9d
	movl	$2, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	_ZL11POSIX_CLOSE(%rip), %rsi
	call	_ZNK6icu_6713UnicodeString7indexOfEPKDsiiii@PLT
	movb	$0, -288(%rbp)
	movl	$2, -300(%rbp)
	movl	%eax, %r12d
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L420:
	cmpb	$0, -280(%rbp)
	jne	.L448
.L419:
	movl	-300(%rbp), %eax
	addl	%r12d, %eax
	movl	%eax, 8(%r14)
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L393:
	movl	12(%r13), %r9d
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	-256(%rbp), %rdi
	movl	%eax, %edx
	movl	%eax, -288(%rbp)
	movq	%rdi, %rcx
	movq	%rdi, -296(%rbp)
	movq	%r13, %rdi
	call	*%r9
	movl	-288(%rbp), %eax
	movq	0(%r13), %r9
	movl	%r12d, %edx
	leaq	-192(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, -288(%rbp)
	movq	%r8, %rcx
	leal	1(%rax), %esi
	call	*24(%r9)
	movq	-288(%rbp), %r8
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L408:
	movl	12(%r13), %edx
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%r15, %rdi
	movq	%r8, -280(%rbp)
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	movq	-280(%rbp), %r8
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L445:
	cmpl	%r12d, %edx
	jbe	.L388
	cmpw	$78, 2(%rcx,%r8)
	jne	.L388
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L410:
	movl	12(%r13), %ecx
	jmp	.L411
.L447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3385:
	.size	_ZN6icu_6710UnicodeSet20applyPropertyPatternERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode, .-_ZN6icu_6710UnicodeSet20applyPropertyPatternERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet20applyPropertyPatternERNS_21RuleCharacterIteratorERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6710UnicodeSet20applyPropertyPatternERNS_21RuleCharacterIteratorERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6710UnicodeSet20applyPropertyPatternERNS_21RuleCharacterIteratorERNS_13UnicodeStringER10UErrorCode:
.LFB3386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rcx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L456
.L449:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	movq	%rsi, %r12
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdi, %r13
	movq	%rdx, %r14
	leaq	-128(%rbp), %r15
	movq	%rax, -128(%rbp)
	movl	$-1, %edx
	movq	%r12, %rdi
	movl	$2, %eax
	movq	%r15, %rsi
	movq	%rcx, %rbx
	movw	%ax, -120(%rbp)
	call	_ZNK6icu_6721RuleCharacterIterator9lookaheadERNS_13UnicodeStringEi@PLT
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-144(%rbp), %r8
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%r8, %rdx
	movq	%rax, -144(%rbp)
	movabsq	$-4294967296, %rax
	movq	%r8, -152(%rbp)
	movq	%rax, -136(%rbp)
	call	_ZN6icu_6710UnicodeSet20applyPropertyPatternERKNS_13UnicodeStringERNS_13ParsePositionER10UErrorCode
	movl	(%rbx), %edx
	movq	-152(%rbp), %r8
	testl	%edx, %edx
	jg	.L451
	movl	-136(%rbp), %esi
	testl	%esi, %esi
	jne	.L452
	movl	$65538, (%rbx)
.L451:
	movq	%r8, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%r12, %rdi
	movq	%r8, -152(%rbp)
	call	_ZN6icu_6721RuleCharacterIterator9jumpaheadEi@PLT
	movl	-136(%rbp), %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-152(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L449
.L457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3386:
	.size	_ZN6icu_6710UnicodeSet20applyPropertyPatternERNS_21RuleCharacterIteratorERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6710UnicodeSet20applyPropertyPatternERNS_21RuleCharacterIteratorERNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet12applyPatternERNS_21RuleCharacterIteratorEPKNS_11SymbolTableERNS_13UnicodeStringEjMS0_FRS0_iEiR10UErrorCode
	.type	_ZN6icu_6710UnicodeSet12applyPatternERNS_21RuleCharacterIteratorEPKNS_11SymbolTableERNS_13UnicodeStringEjMS0_FRS0_iEiR10UErrorCode, @function
_ZN6icu_6710UnicodeSet12applyPatternERNS_21RuleCharacterIteratorEPKNS_11SymbolTableERNS_13UnicodeStringEjMS0_FRS0_iEiR10UErrorCode:
.LFB3373:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rbp), %rbx
	movq	%rdi, -232(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rcx, -272(%rbp)
	movl	(%rbx), %r10d
	movl	%r8d, -248(%rbp)
	movl	%r9d, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r10d, %r10d
	jg	.L458
	cmpl	$100, %r9d
	jg	.L600
	movl	%r8d, %eax
	movl	$2, %edx
	movl	$2, %ecx
	movq	%rsi, %r12
	andl	$1, %eax
	movw	%dx, -184(%rbp)
	leaq	-192(%rbp), %r14
	cmpl	$1, %eax
	movq	16(%rbp), %rax
	movw	%cx, -120(%rbp)
	sbbl	%r13d, %r13d
	movq	%rax, -304(%rbp)
	movq	24(%rbp), %rax
	andl	$-4, %r13d
	addl	$7, %r13d
	movq	%rax, -312(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -192(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	leaq	-128(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -280(%rbp)
	movq	%rax, -288(%rbp)
	leaq	-208(%rbp), %rax
	movb	$0, -313(%rbp)
	movw	%si, -242(%rbp)
	movl	$0, -260(%rbp)
	movb	$0, -240(%rbp)
	movb	$0, -243(%rbp)
	movb	$0, -244(%rbp)
	movq	%rax, -296(%rbp)
.L471:
	movq	%r12, %rdi
	call	_ZNK6icu_6721RuleCharacterIterator5atEndEv@PLT
	testb	%al, %al
	jne	.L463
	movl	%r13d, %esi
	movq	%r12, %rdi
	movb	$0, -211(%rbp)
	xorl	%r15d, %r15d
	call	_ZN6icu_6710UnicodeSet24resemblesPropertyPatternERNS_21RuleCharacterIteratorEi
	movl	$2, %r8d
	testb	%al, %al
	je	.L601
.L464:
	cmpb	$1, -243(%rbp)
	je	.L602
.L473:
	movzwl	-242(%rbp), %eax
	cmpw	$45, %ax
	je	.L548
	cmpw	$38, %ax
	je	.L548
.L475:
	testq	%r15, %r15
	je	.L603
.L477:
	cmpb	$2, %r8b
	je	.L479
	cmpb	$3, %r8b
	je	.L480
	movq	-304(%rbp), %rax
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	subq	$8, %rsp
	movl	-248(%rbp), %r8d
	movq	-256(%rbp), %rdx
	movq	%rax, 16(%rbp)
	movq	-312(%rbp), %rax
	pushq	%rbx
	movq	%rax, 24(%rbp)
	movl	-264(%rbp), %eax
	pushq	24(%rbp)
	pushq	16(%rbp)
	leal	1(%rax), %r9d
	call	_ZN6icu_6710UnicodeSet12applyPatternERNS_21RuleCharacterIteratorEPKNS_11SymbolTableERNS_13UnicodeStringEjMS0_FRS0_iEiR10UErrorCode
	addq	$32, %rsp
.L481:
	cmpb	$0, -240(%rbp)
	jne	.L482
	movq	-232(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6710UnicodeSetaSERKS0_@PLT
	movb	$1, -244(%rbp)
.L483:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6721RuleCharacterIterator11skipIgnoredEi@PLT
	testb	$2, -248(%rbp)
	je	.L524
	movq	-304(%rbp), %rcx
	movq	-312(%rbp), %rdi
	addq	-232(%rbp), %rdi
	testb	$1, %cl
	je	.L525
	movq	(%rdi), %rax
	movq	-1(%rax,%rcx), %rax
	movq	%rax, -304(%rbp)
.L525:
	movq	-304(%rbp), %rax
	movl	$2, %esi
	call	*%rax
.L526:
	cmpb	$0, -313(%rbp)
	jne	.L604
.L528:
	cmpb	$0, -244(%rbp)
	je	.L529
	movzwl	-184(%rbp), %eax
	testw	%ax, %ax
	js	.L530
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L531:
	movq	-272(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
.L532:
	movq	-232(%rbp), %rax
	testb	$1, 32(%rax)
	je	.L523
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L523
	movl	$7, (%rbx)
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L601:
	movq	-296(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-211(%rbp), %r15
	call	_ZNK6icu_6721RuleCharacterIterator6getPosERNS0_3PosE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode@PLT
	movl	%eax, %r8d
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L523
	cmpl	$91, %r8d
	je	.L605
.L466:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L606
.L472:
	cmpb	$0, -240(%rbp)
	je	.L463
	cmpb	$0, -211(%rbp)
	jne	.L470
	cmpl	$93, %r8d
	je	.L489
	jle	.L607
	cmpl	$94, %r8d
	jne	.L608
.L463:
	movl	$65538, (%rbx)
.L523:
	movq	-280(%rbp), %rax
	testq	%rax, %rax
	je	.L536
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L536:
	movq	-288(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
.L458:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L609
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	cmpl	$38, %r8d
	je	.L491
	cmpl	$45, %r8d
	jne	.L610
	cmpw	$0, -242(%rbp)
	jne	.L463
	cmpb	$0, -243(%rbp)
	jne	.L542
	movq	-232(%rbp), %rdi
	movl	$45, %edx
	movl	$45, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L523
	cmpl	$93, %eax
	jne	.L463
	cmpb	$0, -211(%rbp)
	jne	.L463
	movl	$2, %ecx
	xorl	%edx, %edx
	leaq	_ZL18HYPHEN_RIGHT_BRACE(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	leaq	_ZL18HYPHEN_RIGHT_BRACE(%rip), %rax
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L608:
	cmpl	$123, %r8d
	jne	.L470
	cmpw	$0, -242(%rbp)
	jne	.L463
	cmpb	$1, -243(%rbp)
	je	.L611
.L502:
	movzwl	-120(%rbp), %eax
	testb	$1, %al
	je	.L503
	movq	-288(%rbp), %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	%r14, -240(%rbp)
	movq	-288(%rbp), %r14
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L612:
	movl	%r13d, %esi
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode@PLT
	movl	%eax, %esi
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L592
	cmpl	$125, %esi
	jne	.L509
	cmpb	$0, -211(%rbp)
	je	.L545
.L509:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString6appendEi@PLT
.L504:
	movq	%r12, %rdi
	call	_ZNK6icu_6721RuleCharacterIterator5atEndEv@PLT
	testb	%al, %al
	je	.L612
	movq	-240(%rbp), %r14
	movl	$1, %edx
.L508:
	movswl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L510
	sarl	$5, %eax
.L511:
	testl	%eax, %eax
	jle	.L463
	testb	%dl, %dl
	jne	.L463
	movq	-288(%rbp), %r15
	movq	-232(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6710UnicodeSet3addERKNS_13UnicodeStringE@PLT
	movl	$123, %r11d
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	-210(%rbp), %r9
	movl	$1, %ecx
	movw	%r11w, -210(%rbp)
	movq	%r9, %rsi
	movq	%r9, -240(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringERKS1_a@PLT
	movl	$125, %r15d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	-240(%rbp), %r9
	movl	$1, %ecx
	movw	%r15w, -210(%rbp)
	movq	%r9, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzbl	-244(%rbp), %eax
	movb	$0, -243(%rbp)
	movb	%al, -240(%rbp)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L610:
	cmpl	$36, %r8d
	jne	.L470
	movq	-296(%rbp), %rsi
	movq	%r12, %rdi
	movl	%r8d, -320(%rbp)
	call	_ZNK6icu_6721RuleCharacterIterator6getPosERNS0_3PosE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode@PLT
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L523
	cmpl	$93, %eax
	movl	-320(%rbp), %r8d
	je	.L613
.L513:
	cmpq	$0, -256(%rbp)
	jne	.L463
	movq	-296(%rbp), %rsi
	movq	%r12, %rdi
	movl	%r8d, -240(%rbp)
	call	_ZN6icu_6721RuleCharacterIterator6setPosERKNS0_3PosE@PLT
	movl	-240(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L470:
	movzbl	-243(%rbp), %eax
	cmpb	$1, %al
	je	.L517
	cmpb	$2, %al
	je	.L518
	testb	%al, %al
	je	.L547
	movl	-260(%rbp), %r8d
.L519:
	movl	%r8d, -260(%rbp)
.L522:
	movb	$1, -240(%rbp)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L482:
	movzwl	-242(%rbp), %eax
	cmpw	$38, %ax
	je	.L484
	cmpw	$45, %ax
	je	.L485
	testw	%ax, %ax
	je	.L486
.L596:
	xorl	%r8d, %r8d
	movb	$2, -243(%rbp)
	movw	%r8w, -242(%rbp)
.L487:
	movzbl	-240(%rbp), %eax
	movb	%al, -244(%rbp)
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L600:
	movl	$1, (%rbx)
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L606:
	movq	(%rdi), %rax
	movl	%r8d, -320(%rbp)
	movl	%r8d, %esi
	call	*24(%rax)
	movl	-320(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L472
	xorl	%ecx, %ecx
	leaq	_ZTIN6icu_6710UnicodeSetE(%rip), %rdx
	leaq	_ZTIN6icu_6714UnicodeFunctorE(%rip), %rsi
	call	__dynamic_cast@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L463
	cmpb	$1, -243(%rbp)
	movl	$3, %r8d
	jne	.L473
.L602:
	cmpw	$0, -242(%rbp)
	jne	.L463
	movl	-260(%rbp), %edx
	movq	-232(%rbp), %rdi
	movb	%r8b, -243(%rbp)
	movl	%edx, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	-260(%rbp), %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia@PLT
	movzbl	-243(%rbp), %r8d
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L605:
	cmpb	$0, -211(%rbp)
	jne	.L466
	cmpb	$1, -240(%rbp)
	je	.L614
	movl	$1, %ecx
	xorl	%edx, %edx
	movl	$91, %eax
	movq	%r14, %rdi
	leaq	-210(%rbp), %r9
	movw	%ax, -210(%rbp)
	movq	%r9, %rsi
	movq	%r9, -240(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6721RuleCharacterIterator6getPosERNS0_3PosE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode@PLT
	movl	%eax, %r8d
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L523
	cmpl	$94, %r8d
	movq	-240(%rbp), %r9
	je	.L615
.L468:
	cmpl	$45, %r8d
	jne	.L469
	movb	$1, -211(%rbp)
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L518:
	cmpw	$0, -242(%rbp)
	jne	.L463
.L547:
	movb	$1, -243(%rbp)
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L548:
	movzwl	-242(%rbp), %eax
	leaq	-210(%rbp), %r9
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%r14, %rdi
	movb	%r8b, -243(%rbp)
	movw	%ax, -210(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzbl	-243(%rbp), %r8d
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L485:
	movq	-232(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6710UnicodeSet9removeAllERKS0_@PLT
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L486:
	movq	-232(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	movb	$2, -243(%rbp)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L517:
	cmpw	$45, -242(%rbp)
	jne	.L520
	cmpl	-260(%rbp), %r8d
	jle	.L463
	movl	-260(%rbp), %r15d
	movq	-232(%rbp), %rdi
	movl	%r8d, %edx
	movl	%r8d, -240(%rbp)
	movl	%r15d, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia@PLT
	movl	$45, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	leaq	-210(%rbp), %r9
	movw	%si, -210(%rbp)
	movl	$1, %ecx
	movq	%r9, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-240(%rbp), %r8d
	movq	%r14, %rdi
	xorl	%edx, %edx
	movl	%r8d, %esi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia@PLT
	xorl	%edi, %edi
	movb	$0, -243(%rbp)
	movl	%r15d, %r8d
	movw	%di, -242(%rbp)
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L484:
	movq	-232(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6710UnicodeSet9retainAllERKS0_@PLT
	xorl	%edi, %edi
	movb	$2, -243(%rbp)
	movw	%di, -242(%rbp)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L480:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet10_toPatternERNS_13UnicodeStringEa@PLT
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L479:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6721RuleCharacterIterator11skipIgnoredEi@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet20applyPropertyPatternERNS_21RuleCharacterIteratorERNS_13UnicodeStringER10UErrorCode
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L481
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L603:
	movq	-280(%rbp), %rax
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L477
	movl	$200, %edi
	movb	%r8b, -243(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L478
	movq	%rax, %rdi
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movq	%r15, -280(%rbp)
	movzbl	-243(%rbp), %r8d
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L491:
	cmpb	$2, -243(%rbp)
	jne	.L463
	cmpw	$0, -242(%rbp)
	jne	.L463
	movzbl	-244(%rbp), %eax
	movl	$38, %edx
	movb	$2, -243(%rbp)
	movw	%dx, -242(%rbp)
	movb	%al, -240(%rbp)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L524:
	testb	$4, -248(%rbp)
	je	.L526
	movq	-304(%rbp), %rcx
	movq	-312(%rbp), %rdi
	addq	-232(%rbp), %rdi
	testb	$1, %cl
	je	.L527
	movq	(%rdi), %rax
	movq	-1(%rax,%rcx), %rax
	movq	%rax, -304(%rbp)
.L527:
	movq	-304(%rbp), %rax
	movl	$4, %esi
	call	*%rax
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L520:
	movl	-260(%rbp), %r15d
	movq	-232(%rbp), %rdi
	movl	%r8d, -240(%rbp)
	movl	%r15d, %edx
	movl	%r15d, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia@PLT
	movl	-240(%rbp), %r8d
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L489:
	cmpb	$1, -243(%rbp)
	je	.L616
.L496:
	cmpw	$45, -242(%rbp)
	je	.L617
	cmpw	$38, -242(%rbp)
	leaq	-210(%rbp), %rsi
	je	.L463
.L498:
	movl	$93, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movw	%cx, -210(%rbp)
	movl	$1, %ecx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L529:
	movq	-272(%rbp), %rsi
	movq	-232(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZNK6icu_6710UnicodeSet16_generatePatternERNS_13UnicodeStringEa@PLT
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L604:
	movq	-232(%rbp), %rdi
	call	_ZN6icu_6710UnicodeSet10complementEv@PLT
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L530:
	movl	-180(%rbp), %ecx
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L614:
	movq	-296(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN6icu_6721RuleCharacterIterator6setPosERKNS0_3PosE@PLT
	movl	$1, %r8d
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L542:
	movzbl	-244(%rbp), %eax
	movl	$45, %ecx
	movw	%cx, -242(%rbp)
	movb	%al, -240(%rbp)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L503:
	testw	%ax, %ax
	js	.L505
	movswl	%ax, %edx
	sarl	$5, %edx
.L506:
	testl	%edx, %edx
	je	.L591
	andl	$31, %eax
	movq	%r14, -240(%rbp)
	movq	-288(%rbp), %r14
	movw	%ax, -120(%rbp)
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L592:
	movq	-240(%rbp), %r14
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L615:
	cmpb	$0, -211(%rbp)
	je	.L618
.L469:
	movq	-296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6721RuleCharacterIterator6setPosERKNS0_3PosE@PLT
	movb	$1, -240(%rbp)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L613:
	cmpb	$0, -211(%rbp)
	jne	.L513
	cmpw	$0, -242(%rbp)
	jne	.L463
	cmpb	$1, -243(%rbp)
	je	.L619
.L516:
	movq	-232(%rbp), %rdi
	movl	$65535, %esi
	leaq	-210(%rbp), %r15
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movl	$36, %r8d
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movw	%r8w, -210(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	$93, %r9d
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	$1, %ecx
	movq	%r14, %rdi
	movw	%r9w, -210(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movzbl	-240(%rbp), %eax
	movb	%al, -244(%rbp)
	jmp	.L483
.L591:
	movq	%r14, -240(%rbp)
	movq	-288(%rbp), %r14
	jmp	.L504
.L510:
	movl	-116(%rbp), %eax
	jmp	.L511
.L616:
	movl	-260(%rbp), %r15d
	movq	-232(%rbp), %rdi
	movl	%r15d, %edx
	movl	%r15d, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia@PLT
	jmp	.L496
.L505:
	movl	-116(%rbp), %edx
	jmp	.L506
.L611:
	movl	-260(%rbp), %edx
	movq	-232(%rbp), %rdi
	movl	%edx, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	-260(%rbp), %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia@PLT
	jmp	.L502
.L545:
	movq	-240(%rbp), %r14
	xorl	%edx, %edx
	jmp	.L508
.L618:
	movl	$94, %r10d
	movq	%r9, %rsi
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movw	%r10w, -210(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK6icu_6721RuleCharacterIterator6getPosERNS0_3PosE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6721RuleCharacterIterator4nextEiRaR10UErrorCode@PLT
	movl	(%rbx), %r11d
	movl	%eax, %r8d
	testl	%r11d, %r11d
	jg	.L523
	movb	$1, -313(%rbp)
	jmp	.L468
.L617:
	movq	-232(%rbp), %rdi
	movl	$45, %edx
	movl	$45, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$45, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movw	%si, -210(%rbp)
	movl	$1, %ecx
	leaq	-210(%rbp), %rsi
	movq	%rsi, -240(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-240(%rbp), %rsi
	jmp	.L498
.L619:
	movl	-260(%rbp), %r15d
	movq	-232(%rbp), %rdi
	movl	%r15d, %edx
	movl	%r15d, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	xorl	%edx, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet12_appendToPatERNS_13UnicodeStringEia@PLT
	jmp	.L516
.L609:
	call	__stack_chk_fail@PLT
.L478:
	movl	$7, (%rbx)
	jmp	.L536
	.cfi_endproc
.LFE3373:
	.size	_ZN6icu_6710UnicodeSet12applyPatternERNS_21RuleCharacterIteratorEPKNS_11SymbolTableERNS_13UnicodeStringEjMS0_FRS0_iEiR10UErrorCode, .-_ZN6icu_6710UnicodeSet12applyPatternERNS_21RuleCharacterIteratorEPKNS_11SymbolTableERNS_13UnicodeStringEjMS0_FRS0_iEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet23applyPatternIgnoreSpaceERKNS_13UnicodeStringERNS_13ParsePositionEPKNS_11SymbolTableER10UErrorCode
	.type	_ZN6icu_6710UnicodeSet23applyPatternIgnoreSpaceERKNS_13UnicodeStringERNS_13ParsePositionEPKNS_11SymbolTableER10UErrorCode, @function
_ZN6icu_6710UnicodeSet23applyPatternIgnoreSpaceERKNS_13UnicodeStringERNS_13ParsePositionEPKNS_11SymbolTableER10UErrorCode:
.LFB3363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L620
	cmpq	$0, 40(%rdi)
	movq	%rdi, %r12
	movq	%r8, %rbx
	je	.L634
.L622:
	movl	$30, (%rbx)
.L620:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L635
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L622
	movq	%rcx, %r13
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-128(%rbp), %r14
	movq	%rdx, %rcx
	leaq	-176(%rbp), %r15
	movq	%rax, -128(%rbp)
	movq	%r13, %rdx
	movl	$2, %eax
	movq	%r15, %rdi
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6721RuleCharacterIteratorC1ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE@PLT
	subq	$8, %rsp
	movq	%r13, %rdx
	xorl	%r9d, %r9d
	pushq	%rbx
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	%r15, %rsi
	pushq	$0
	movq	%r12, %rdi
	pushq	$0
	call	_ZN6icu_6710UnicodeSet12applyPatternERNS_21RuleCharacterIteratorEPKNS_11SymbolTableERNS_13UnicodeStringEjMS0_FRS0_iEiR10UErrorCode
	movl	(%rbx), %edx
	addq	$32, %rsp
	testl	%edx, %edx
	jg	.L623
	movq	-152(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L625
	movl	$65538, (%rbx)
.L623:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L620
.L625:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L626
	movswl	%ax, %edx
	sarl	$5, %edx
.L627:
	testb	$17, %al
	jne	.L628
	leaq	-118(%rbp), %rsi
	testb	$2, %al
	cmove	-104(%rbp), %rsi
.L628:
	movq	%r12, %rdi
	call	_ZN6icu_6710UnicodeSet10setPatternEPKDsi@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L620
.L626:
	movl	-116(%rbp), %edx
	jmp	.L627
.L635:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3363:
	.size	_ZN6icu_6710UnicodeSet23applyPatternIgnoreSpaceERKNS_13UnicodeStringERNS_13ParsePositionEPKNS_11SymbolTableER10UErrorCode, .-_ZN6icu_6710UnicodeSet23applyPatternIgnoreSpaceERKNS_13UnicodeStringERNS_13ParsePositionEPKNS_11SymbolTableER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringER10UErrorCode:
.LFB3360:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movb	$0, 32(%rdi)
	leaq	-280(%rax), %rcx
	movq	%rax, %xmm1
	leaq	96(%rdi), %rax
	movl	$0, 56(%rdi)
	movq	%rcx, %xmm0
	movq	%rax, 16(%rdi)
	movl	(%rdx), %ecx
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%rdi)
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, -192(%rbp)
	movabsq	$-4294967296, %rax
	movups	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 64(%rdi)
	movl	$0, 72(%rdi)
	movq	%rax, -184(%rbp)
	movups	%xmm0, 40(%rdi)
	movups	%xmm0, 80(%rdi)
	testl	%ecx, %ecx
	jg	.L645
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rdx, %rbx
	movq	%rdi, %r12
	xorl	%edx, %edx
	leaq	-176(%rbp), %r15
	movq	%rax, -128(%rbp)
	movq	%r14, %rcx
	movl	$2, %eax
	movq	%r15, %rdi
	movw	%ax, -120(%rbp)
	movq	%rsi, %r13
	call	_ZN6icu_6721RuleCharacterIteratorC1ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE@PLT
	subq	$8, %rsp
	leaq	-128(%rbp), %r10
	xorl	%edx, %edx
	pushq	%rbx
	xorl	%r9d, %r9d
	movq	%r10, %rcx
	movl	$1, %r8d
	pushq	$0
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	$0
	movq	%r10, -200(%rbp)
	call	_ZN6icu_6710UnicodeSet12applyPatternERNS_21RuleCharacterIteratorEPKNS_11SymbolTableERNS_13UnicodeStringEjMS0_FRS0_iEiR10UErrorCode
	movl	(%rbx), %edx
	addq	$32, %rsp
	movq	-200(%rbp), %r10
	testl	%edx, %edx
	jg	.L638
	movq	-152(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L639
	movl	$65538, (%rbx)
.L638:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
.L643:
	testl	%eax, %eax
	jg	.L645
	movl	-184(%rbp), %eax
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, -176(%rbp)
	call	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia@PLT
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L646
	sarl	$5, %eax
.L647:
	cmpl	%eax, -176(%rbp)
	je	.L645
	movl	$1, (%rbx)
.L645:
	movq	%r14, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L653
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	movl	12(%r13), %eax
	jmp	.L647
.L639:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L640
	movswl	%ax, %edx
	sarl	$5, %edx
.L641:
	testb	$17, %al
	jne	.L642
	leaq	-118(%rbp), %rsi
	testb	$2, %al
	cmove	-104(%rbp), %rsi
.L642:
	movq	%r12, %rdi
	movq	%r10, -200(%rbp)
	call	_ZN6icu_6710UnicodeSet10setPatternEPKDsi@PLT
	movq	-200(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	jmp	.L643
.L640:
	movl	-116(%rbp), %edx
	jmp	.L641
.L653:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3360:
	.size	_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6710UnicodeSetC1ERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6710UnicodeSetC2ERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode:
.LFB3362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713ParsePositionE(%rip), %rax
	movq	%rax, -192(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rax, -184(%rbp)
	testl	%ecx, %ecx
	jg	.L666
	cmpq	$0, 40(%rdi)
	movq	%rdx, %rbx
	je	.L674
.L656:
	movl	$30, (%rbx)
	leaq	-192(%rbp), %r14
.L666:
	movq	%r14, %rdi
	call	_ZN6icu_6713ParsePositionD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L675
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	jne	.L656
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	xorl	%edx, %edx
	movq	%r14, %rcx
	movq	%rsi, %r12
	leaq	-176(%rbp), %r15
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movq	%r15, %rdi
	movw	%ax, -120(%rbp)
	call	_ZN6icu_6721RuleCharacterIteratorC1ERKNS_13UnicodeStringEPKNS_11SymbolTableERNS_13ParsePositionE@PLT
	subq	$8, %rsp
	leaq	-128(%rbp), %r10
	xorl	%edx, %edx
	pushq	%rbx
	xorl	%r9d, %r9d
	movq	%r10, %rcx
	movl	$1, %r8d
	pushq	$0
	movq	%r15, %rsi
	movq	%r13, %rdi
	pushq	$0
	movq	%r10, -200(%rbp)
	call	_ZN6icu_6710UnicodeSet12applyPatternERNS_21RuleCharacterIteratorEPKNS_11SymbolTableERNS_13UnicodeStringEjMS0_FRS0_iEiR10UErrorCode
	movl	(%rbx), %edx
	addq	$32, %rsp
	movq	-200(%rbp), %r10
	testl	%edx, %edx
	jg	.L657
	movq	-152(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L660
	movl	$65538, (%rbx)
.L657:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
.L664:
	testl	%eax, %eax
	jg	.L666
	movl	-184(%rbp), %eax
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%eax, -176(%rbp)
	call	_ZN6icu_6711ICU_Utility14skipWhitespaceERKNS_13UnicodeStringERia@PLT
	movswl	8(%r12), %eax
	testw	%ax, %ax
	js	.L667
	sarl	$5, %eax
.L668:
	cmpl	%eax, -176(%rbp)
	je	.L666
	movl	$1, (%rbx)
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L667:
	movl	12(%r12), %eax
	jmp	.L668
.L660:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L661
	movswl	%ax, %edx
	sarl	$5, %edx
.L662:
	testb	$17, %al
	jne	.L663
	leaq	-118(%rbp), %rsi
	testb	$2, %al
	cmove	-104(%rbp), %rsi
.L663:
	movq	%r13, %rdi
	movq	%r10, -200(%rbp)
	call	_ZN6icu_6710UnicodeSet10setPatternEPKDsi@PLT
	movq	-200(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	(%rbx), %eax
	jmp	.L664
.L661:
	movl	-116(%rbp), %edx
	jmp	.L662
.L675:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3362:
	.size	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode
	.section	.rodata.str2.2,"aMS",@progbits,2
	.align 2
.LC3:
	.string	"["
	.string	":"
	.string	"a"
	.string	"g"
	.string	"e"
	.string	"="
	.string	"3"
	.string	"."
	.string	"2"
	.string	":"
	.string	"]"
	.string	""
	.string	""
	.text
	.p2align 4
	.globl	uniset_getUnicode32Instance_67
	.type	uniset_getUnicode32Instance_67, @function
uniset_getUnicode32Instance_67:
.LFB3346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L695
.L678:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	_ZL14uni32Singleton(%rip), %rax
	jne	.L696
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	movl	_ZL13uni32InitOnce(%rip), %eax
	movq	%rdi, %rbx
	cmpl	$2, %eax
	jne	.L697
.L679:
	movl	4+_ZL13uni32InitOnce(%rip), %eax
	testl	%eax, %eax
	jle	.L678
	movl	%eax, (%rbx)
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L697:
	leaq	_ZL13uni32InitOnce(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L679
	leaq	-112(%rbp), %r13
	leaq	-120(%rbp), %rdx
	movl	$-1, %ecx
	movl	$1, %esi
	leaq	.LC3(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EaNS_14ConstChar16PtrEi@PLT
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L680
	leaq	296+_ZTVN6icu_6710UnicodeSetE(%rip), %rax
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movb	$0, 32(%r12)
	leaq	-280(%rax), %rsi
	movq	%rax, %xmm1
	leaq	96(%r12), %rax
	movl	$0, 56(%r12)
	movq	%rsi, %xmm0
	movq	%rax, 16(%r12)
	movq	%r13, %rsi
	movabsq	$4294967321, %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 24(%r12)
	movq	$0, 64(%r12)
	movl	$0, 72(%r12)
	movups	%xmm0, (%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 40(%r12)
	movups	%xmm0, 80(%r12)
	call	_ZN6icu_6710UnicodeSet12applyPatternERKNS_13UnicodeStringER10UErrorCode
.L680:
	movq	%r13, %rdi
	movq	%r12, _ZL14uni32Singleton(%rip)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	_ZL14uni32Singleton(%rip), %rdi
	testq	%rdi, %rdi
	je	.L698
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
.L682:
	leaq	uset_cleanup(%rip), %rsi
	movl	$15, %edi
	call	ucln_common_registerCleanup_67@PLT
	movl	(%rbx), %eax
	leaq	_ZL13uni32InitOnce(%rip), %rdi
	movl	%eax, 4+_ZL13uni32InitOnce(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L698:
	movl	$7, (%rbx)
	jmp	.L682
.L696:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3346:
	.size	uniset_getUnicode32Instance_67, .-uniset_getUnicode32Instance_67
	.local	_ZL13uni32InitOnce
	.comm	_ZL13uni32InitOnce,8,8
	.local	_ZL14uni32Singleton
	.comm	_ZL14uni32Singleton,8,8
	.section	.rodata
	.align 8
	.type	_ZL8ASSIGNED, @object
	.size	_ZL8ASSIGNED, 9
_ZL8ASSIGNED:
	.string	"Assigned"
	.type	_ZL5ASCII, @object
	.size	_ZL5ASCII, 6
_ZL5ASCII:
	.string	"ASCII"
	.type	_ZL3ANY, @object
	.size	_ZL3ANY, 4
_ZL3ANY:
	.string	"ANY"
	.align 2
	.type	_ZL18HYPHEN_RIGHT_BRACE, @object
	.size	_ZL18HYPHEN_RIGHT_BRACE, 6
_ZL18HYPHEN_RIGHT_BRACE:
	.value	45
	.value	93
	.value	0
	.align 2
	.type	_ZL11POSIX_CLOSE, @object
	.size	_ZL11POSIX_CLOSE, 6
_ZL11POSIX_CLOSE:
	.value	58
	.value	93
	.value	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	1081073664
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
