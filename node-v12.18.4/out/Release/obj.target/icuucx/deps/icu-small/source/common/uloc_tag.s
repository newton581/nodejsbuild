	.file	"uloc_tag.cpp"
	.text
	.p2align 4
	.type	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a, @function
_ZL19_addExtensionToListPP18ExtensionListEntryS0_a:
.LFB2405:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%r15, %r15
	je	.L42
	movq	(%rsi), %rbx
	movl	%edx, %r12d
	xorl	%r13d, %r13d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	(%r15), %r14
	movq	%rax, -56(%rbp)
	movq	%r14, %rdi
	call	strlen@PLT
	movq	-56(%rbp), %rcx
	cmpl	$1, %ecx
	jne	.L9
	cmpl	$1, %eax
	jne	.L5
	movsbl	(%rbx), %eax
	movsbl	(%r14), %ecx
	cmpb	%cl, %al
	je	.L19
	cmpb	$120, %al
	je	.L6
	cmpb	$120, %cl
	je	.L7
	subl	%ecx, %eax
.L8:
	testl	%eax, %eax
	js	.L7
.L12:
	je	.L19
.L6:
	movq	16(%r15), %rax
	movq	%r15, %r13
	testq	%rax, %rax
	je	.L15
.L46:
	movq	%rax, %r15
.L16:
	testb	%r12b, %r12b
	jne	.L43
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jns	.L12
.L7:
	testq	%r13, %r13
	je	.L44
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r13)
.L14:
	movq	%r15, 16(%rax)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	cmpl	$1, %eax
	je	.L45
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	movl	%eax, -56(%rbp)
	je	.L19
	leaq	_ZL20LOCALE_ATTRIBUTE_KEY(%rip), %rsi
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L6
	leaq	_ZL20LOCALE_ATTRIBUTE_KEY(%rip), %rsi
	movq	%rbx, %rdi
	call	strcmp@PLT
	movl	-56(%rbp), %ecx
	testl	%eax, %eax
	je	.L7
	testl	%ecx, %ecx
	js	.L7
	movq	16(%r15), %rax
	movq	%r15, %r13
	testq	%rax, %rax
	jne	.L46
.L15:
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r15)
	movq	$0, 16(%rax)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	cmpl	$1, %ecx
	jne	.L9
	movsbl	(%rbx), %eax
	subl	$117, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L45:
	movsbl	(%r14), %eax
	movl	$117, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L42:
	movq	$0, 16(%rsi)
	movl	$1, %eax
	movq	%rsi, (%rdi)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rdx, %rax
	jmp	.L14
	.cfi_endproc
.LFE2405:
	.size	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a, .-_ZL19_addExtensionToListPP18ExtensionListEntryS0_a
	.p2align 4
	.type	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0, @function
_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0:
.LFB3226:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L57
	movq	(%rsi), %r13
	xorl	%r12d, %r12d
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rax, %rbx
.L54:
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	js	.L58
	je	.L55
	movq	16(%rbx), %rax
	movq	%rbx, %r12
	testq	%rax, %rax
	jne	.L59
	movq	%r14, 16(%rbx)
	movl	$1, %eax
	movq	$0, 16(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	$0, 16(%rsi)
	movl	$1, %eax
	movq	%rsi, (%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L60
	movq	%r14, 16(%r12)
.L52:
	movq	%rbx, 16(%r14)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L60:
	.cfi_restore_state
	movq	%r14, (%r15)
	jmp	.L52
	.cfi_endproc
.LFE3226:
	.size	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0, .-_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0
	.p2align 4
	.type	_ZL24_isPrivateuseValueSubtagPKci, @function
_ZL24_isPrivateuseValueSubtagPKci:
.LFB2389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testl	%esi, %esi
	jns	.L62
	call	strlen@PLT
	movl	%eax, %esi
.L62:
	subl	$1, %esi
	cmpl	$7, %esi
	jbe	.L69
.L63:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	leaq	1(%rbx,%rsi), %r12
	.p2align 4,,10
	.p2align 3
.L66:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L64
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L63
.L64:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L66
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2389:
	.size	_ZL24_isPrivateuseValueSubtagPKci, .-_ZL24_isPrivateuseValueSubtagPKci
	.p2align 4
	.type	_ZL12_isSepListOfPFaPKciES0_i.constprop.4, @function
_ZL12_isSepListOfPFaPKciES0_i.constprop.4:
.LFB3221:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	jns	.L71
	call	strlen@PLT
	movl	%eax, %esi
.L71:
	movslq	%esi, %r13
	testq	%r13, %r13
	jle	.L72
	movq	%r14, %r12
	xorl	%ebx, %ebx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L73:
	testq	%rbx, %rbx
	cmove	%r12, %rbx
	addq	$1, %r12
	movq	%r12, %rax
	subq	%r14, %rax
	cmpq	%r13, %rax
	jge	.L93
.L78:
	cmpb	$45, (%r12)
	jne	.L73
	testq	%rbx, %rbx
	je	.L72
	movq	%r12, %rdx
	subq	%rbx, %rdx
	movl	%edx, %eax
	testl	%edx, %edx
	jns	.L74
	movq	%rbx, %rdi
	call	strlen@PLT
.L74:
	subl	$1, %eax
	cmpl	$7, %eax
	jbe	.L94
.L72:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	leaq	1(%rbx,%rax), %r15
	.p2align 4,,10
	.p2align 3
.L77:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L75
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L72
.L75:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	jne	.L77
	addq	$1, %r12
	xorl	%ebx, %ebx
	movq	%r12, %rax
	subq	%r14, %rax
	cmpq	%r13, %rax
	jl	.L78
	.p2align 4,,10
	.p2align 3
.L93:
	testq	%rbx, %rbx
	je	.L72
	subq	%rbx, %r12
	movl	%r12d, %eax
	testl	%r12d, %r12d
	js	.L95
	subl	$1, %eax
	cmpl	$7, %eax
	ja	.L72
.L96:
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L82:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L80
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L72
.L80:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L82
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	strlen@PLT
	subl	$1, %eax
	cmpl	$7, %eax
	ja	.L72
	jmp	.L96
	.cfi_endproc
.LFE3221:
	.size	_ZL12_isSepListOfPFaPKciES0_i.constprop.4, .-_ZL12_isSepListOfPFaPKciES0_i.constprop.4
	.p2align 4
	.type	_ZL16_isVariantSubtagPKci, @function
_ZL16_isVariantSubtagPKci:
.LFB2382:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testl	%esi, %esi
	js	.L110
.L98:
	leal	-5(%rsi), %eax
	cmpl	$3, %eax
	ja	.L100
	leal	-1(%rsi), %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L104:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L101
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L99
.L101:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L104
.L103:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	cmpl	$4, %esi
	jne	.L99
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L111
.L99:
	xorl	%eax, %eax
.L112:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	call	strlen@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L98
	xorl	%eax, %eax
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	1(%rbx), %r12
	addq	$4, %rbx
	.p2align 4,,10
	.p2align 3
.L106:
	movsbl	(%r12), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L105
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L99
.L105:
	addq	$1, %r12
	cmpq	%r12, %rbx
	jne	.L106
	jmp	.L103
	.cfi_endproc
.LFE2382:
	.size	_ZL16_isVariantSubtagPKci, .-_ZL16_isVariantSubtagPKci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2715:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2715:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2718:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L126
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L114
	cmpb	$0, 12(%rbx)
	jne	.L127
.L118:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L114:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L118
	.cfi_endproc
.LFE2718:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2721:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L130
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2721:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2724:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L133
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2724:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L139
.L135:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L140
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2726:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2727:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2727:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2728:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2728:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2729:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2729:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2730:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2730:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2731:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2731:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2732:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L156
	testl	%edx, %edx
	jle	.L156
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L159
.L148:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L148
	.cfi_endproc
.LFE2732:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L163
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L163
	testl	%r12d, %r12d
	jg	.L170
	cmpb	$0, 12(%rbx)
	jne	.L171
.L165:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L165
.L171:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L163:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2733:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L173
	movq	(%rdi), %r8
.L174:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L177
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L177
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L177:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2734:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2735:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L184
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2735:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2736:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2736:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2737:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2737:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2738:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2738:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2740:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2740:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2742:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2742:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.p2align 4
	.globl	ultag_isLanguageSubtag_67
	.type	ultag_isLanguageSubtag_67, @function
ultag_isLanguageSubtag_67:
.LFB2378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testl	%esi, %esi
	jns	.L191
	call	strlen@PLT
	movl	%eax, %esi
.L191:
	leal	-2(%rsi), %eax
	cmpl	$6, %eax
	ja	.L192
	leal	-1(%rsi), %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L194:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L192
	addq	$1, %rbx
	cmpq	%r12, %rbx
	jne	.L194
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2378:
	.size	ultag_isLanguageSubtag_67, .-ultag_isLanguageSubtag_67
	.p2align 4
	.globl	ultag_isScriptSubtag_67
	.type	ultag_isScriptSubtag_67, @function
ultag_isScriptSubtag_67:
.LFB2380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testl	%esi, %esi
	jns	.L201
	call	strlen@PLT
	movl	%eax, %esi
.L201:
	cmpl	$4, %esi
	je	.L210
.L202:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	leaq	4(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L204:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L202
	addq	$1, %rbx
	cmpq	%r12, %rbx
	jne	.L204
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2380:
	.size	ultag_isScriptSubtag_67, .-ultag_isScriptSubtag_67
	.p2align 4
	.globl	ultag_isRegionSubtag_67
	.type	ultag_isRegionSubtag_67, @function
ultag_isRegionSubtag_67:
.LFB2381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%esi, %esi
	jns	.L212
	call	strlen@PLT
	movl	%eax, %esi
.L212:
	cmpl	$2, %esi
	je	.L222
	cmpl	$3, %esi
	je	.L223
.L214:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L214
	movzbl	1(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L214
	movzbl	2(%rbx), %edx
	movl	$1, %eax
	subl	$48, %edx
	cmpb	$9, %dl
	ja	.L214
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L214
	movsbl	1(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2381:
	.size	ultag_isRegionSubtag_67, .-ultag_isRegionSubtag_67
	.p2align 4
	.globl	ultag_isVariantSubtags_67
	.type	ultag_isVariantSubtags_67, @function
ultag_isVariantSubtags_67:
.LFB2384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	jns	.L225
	call	strlen@PLT
	movl	%eax, %esi
.L225:
	movslq	%esi, %rbx
	movq	%r13, %r14
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	jg	.L226
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L228:
	testq	%r12, %r12
	cmove	%r14, %r12
	addq	$1, %r14
	movq	%r14, %rax
	subq	%r13, %rax
	cmpq	%rbx, %rax
	jge	.L250
.L226:
	cmpb	$45, (%r14)
	jne	.L228
	testq	%r12, %r12
	je	.L227
	movq	%r14, %rax
	subq	%r12, %rax
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L251
.L230:
	leal	-5(%rdx), %eax
	cmpl	$3, %eax
	ja	.L252
	leal	-1(%rdx), %eax
	leaq	1(%r12,%rax), %r15
	.p2align 4,,10
	.p2align 3
.L236:
	movsbl	(%r12), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L234
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L227
.L234:
	addq	$1, %r12
	cmpq	%r12, %r15
	jne	.L236
	xorl	%r12d, %r12d
.L254:
	addq	$1, %r14
	movq	%r14, %rax
	subq	%r13, %rax
	cmpq	%rbx, %rax
	jl	.L226
	.p2align 4,,10
	.p2align 3
.L250:
	testq	%r12, %r12
	je	.L227
	addq	$8, %rsp
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	subq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZL16_isVariantSubtagPKci
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	cmpl	$4, %edx
	jne	.L227
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L253
.L227:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, %edx
	testl	%eax, %eax
	js	.L227
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L253:
	leaq	1(%r12), %r15
	addq	$4, %r12
	.p2align 4,,10
	.p2align 3
.L238:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L237
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L227
.L237:
	addq	$1, %r15
	cmpq	%r15, %r12
	jne	.L238
	xorl	%r12d, %r12d
	jmp	.L254
	.cfi_endproc
.LFE2384:
	.size	ultag_isVariantSubtags_67, .-ultag_isVariantSubtags_67
	.p2align 4
	.globl	ultag_isExtensionSubtags_67
	.type	ultag_isExtensionSubtags_67, @function
ultag_isExtensionSubtags_67:
.LFB2388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	jns	.L256
	call	strlen@PLT
	movl	%eax, %esi
.L256:
	movslq	%esi, %r14
	testq	%r14, %r14
	jle	.L257
	movq	%r13, %r12
	xorl	%ebx, %ebx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L258:
	testq	%rbx, %rbx
	cmove	%r12, %rbx
	addq	$1, %r12
	movq	%r12, %rax
	subq	%r13, %rax
	cmpq	%r14, %rax
	jge	.L278
.L263:
	cmpb	$45, (%r12)
	jne	.L258
	testq	%rbx, %rbx
	je	.L257
	movq	%r12, %rdx
	subq	%rbx, %rdx
	movl	%edx, %eax
	testl	%edx, %edx
	jns	.L259
	movq	%rbx, %rdi
	call	strlen@PLT
.L259:
	leal	-2(%rax), %edx
	cmpl	$6, %edx
	jbe	.L279
.L257:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	subl	$1, %eax
	leaq	1(%rbx,%rax), %r15
	.p2align 4,,10
	.p2align 3
.L262:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L260
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L257
.L260:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	jne	.L262
	addq	$1, %r12
	xorl	%ebx, %ebx
	movq	%r12, %rax
	subq	%r13, %rax
	cmpq	%r14, %rax
	jl	.L263
	.p2align 4,,10
	.p2align 3
.L278:
	testq	%rbx, %rbx
	je	.L257
	subq	%rbx, %r12
	movl	%r12d, %eax
	testl	%r12d, %r12d
	js	.L280
	leal	-2(%rax), %edx
	cmpl	$6, %edx
	ja	.L257
.L281:
	subl	$1, %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L267:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L265
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L257
.L265:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L267
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	strlen@PLT
	leal	-2(%rax), %edx
	cmpl	$6, %edx
	ja	.L257
	jmp	.L281
	.cfi_endproc
.LFE2388:
	.size	ultag_isExtensionSubtags_67, .-ultag_isExtensionSubtags_67
	.p2align 4
	.globl	ultag_isPrivateuseValueSubtags_67
	.type	ultag_isPrivateuseValueSubtags_67, @function
ultag_isPrivateuseValueSubtags_67:
.LFB2390:
	.cfi_startproc
	endbr64
	jmp	_ZL12_isSepListOfPFaPKciES0_i.constprop.4
	.cfi_endproc
.LFE2390:
	.size	ultag_isPrivateuseValueSubtags_67, .-ultag_isPrivateuseValueSubtags_67
	.p2align 4
	.globl	ultag_isUnicodeLocaleAttribute_67
	.type	ultag_isUnicodeLocaleAttribute_67, @function
ultag_isUnicodeLocaleAttribute_67:
.LFB2391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testl	%esi, %esi
	jns	.L284
	call	strlen@PLT
	movl	%eax, %esi
.L284:
	leal	-3(%rsi), %eax
	cmpl	$5, %eax
	jbe	.L291
.L285:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	leal	-1(%rsi), %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L288:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L286
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L285
.L286:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L288
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2391:
	.size	ultag_isUnicodeLocaleAttribute_67, .-ultag_isUnicodeLocaleAttribute_67
	.p2align 4
	.globl	ultag_isUnicodeLocaleAttributes_67
	.type	ultag_isUnicodeLocaleAttributes_67, @function
ultag_isUnicodeLocaleAttributes_67:
.LFB2392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	jns	.L293
	call	strlen@PLT
	movl	%eax, %esi
.L293:
	movslq	%esi, %r14
	testq	%r14, %r14
	jle	.L294
	movq	%r13, %r12
	xorl	%ebx, %ebx
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L295:
	testq	%rbx, %rbx
	cmove	%r12, %rbx
	addq	$1, %r12
	movq	%r12, %rax
	subq	%r13, %rax
	cmpq	%r14, %rax
	jge	.L315
.L300:
	cmpb	$45, (%r12)
	jne	.L295
	testq	%rbx, %rbx
	je	.L294
	movq	%r12, %rdx
	subq	%rbx, %rdx
	movl	%edx, %eax
	testl	%edx, %edx
	jns	.L296
	movq	%rbx, %rdi
	call	strlen@PLT
.L296:
	leal	-3(%rax), %edx
	cmpl	$5, %edx
	jbe	.L316
.L294:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	subl	$1, %eax
	leaq	1(%rbx,%rax), %r15
	.p2align 4,,10
	.p2align 3
.L299:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L297
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L294
.L297:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	jne	.L299
	addq	$1, %r12
	xorl	%ebx, %ebx
	movq	%r12, %rax
	subq	%r13, %rax
	cmpq	%r14, %rax
	jl	.L300
	.p2align 4,,10
	.p2align 3
.L315:
	testq	%rbx, %rbx
	je	.L294
	subq	%rbx, %r12
	movl	%r12d, %eax
	testl	%r12d, %r12d
	js	.L317
	leal	-3(%rax), %edx
	cmpl	$5, %edx
	ja	.L294
.L318:
	subl	$1, %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L304:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L302
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L294
.L302:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L304
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	strlen@PLT
	leal	-3(%rax), %edx
	cmpl	$5, %edx
	ja	.L294
	jmp	.L318
	.cfi_endproc
.LFE2392:
	.size	ultag_isUnicodeLocaleAttributes_67, .-ultag_isUnicodeLocaleAttributes_67
	.p2align 4
	.globl	ultag_isUnicodeLocaleKey_67
	.type	ultag_isUnicodeLocaleKey_67, @function
ultag_isUnicodeLocaleKey_67:
.LFB2393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%esi, %esi
	jns	.L320
	call	strlen@PLT
	movl	%eax, %esi
.L320:
	cmpl	$2, %esi
	je	.L321
.L324:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L323
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L324
.L323:
	movsbl	1(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2393:
	.size	ultag_isUnicodeLocaleKey_67, .-ultag_isUnicodeLocaleKey_67
	.p2align 4
	.globl	_isUnicodeLocaleTypeSubtag_67
	.type	_isUnicodeLocaleTypeSubtag_67, @function
_isUnicodeLocaleTypeSubtag_67:
.LFB3217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testl	%esi, %esi
	jns	.L327
	call	strlen@PLT
	movl	%eax, %esi
.L327:
	leal	-3(%rsi), %eax
	cmpl	$5, %eax
	jbe	.L334
.L328:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	leal	-1(%rsi), %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L331:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L329
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L328
.L329:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L331
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3217:
	.size	_isUnicodeLocaleTypeSubtag_67, .-_isUnicodeLocaleTypeSubtag_67
	.p2align 4
	.globl	ultag_isUnicodeLocaleType_67
	.type	ultag_isUnicodeLocaleType_67, @function
ultag_isUnicodeLocaleType_67:
.LFB2395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%esi, %esi
	jns	.L336
	call	strlen@PLT
	movl	%eax, %esi
.L336:
	movslq	%esi, %r14
	testq	%r14, %r14
	jle	.L337
	movq	%r13, %r12
	xorl	%ebx, %ebx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L338:
	testq	%rbx, %rbx
	cmove	%r12, %rbx
	addq	$1, %r12
	movq	%r12, %rax
	subq	%r13, %rax
	cmpq	%r14, %rax
	jge	.L358
.L343:
	cmpb	$45, (%r12)
	jne	.L338
	testq	%rbx, %rbx
	je	.L337
	movq	%r12, %rdx
	subq	%rbx, %rdx
	movl	%edx, %eax
	testl	%edx, %edx
	jns	.L339
	movq	%rbx, %rdi
	call	strlen@PLT
.L339:
	leal	-3(%rax), %edx
	cmpl	$5, %edx
	jbe	.L359
.L337:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	subl	$1, %eax
	leaq	1(%rbx,%rax), %r15
	.p2align 4,,10
	.p2align 3
.L342:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L340
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L337
.L340:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	jne	.L342
	addq	$1, %r12
	xorl	%ebx, %ebx
	movq	%r12, %rax
	subq	%r13, %rax
	cmpq	%r14, %rax
	jl	.L343
	.p2align 4,,10
	.p2align 3
.L358:
	testq	%rbx, %rbx
	je	.L337
	subq	%rbx, %r12
	movl	%r12d, %eax
	testl	%r12d, %r12d
	js	.L360
	leal	-3(%rax), %edx
	cmpl	$5, %edx
	ja	.L337
.L361:
	subl	$1, %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L347:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L345
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L337
.L345:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L347
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	strlen@PLT
	leal	-3(%rax), %edx
	cmpl	$5, %edx
	ja	.L337
	jmp	.L361
	.cfi_endproc
.LFE2395:
	.size	ultag_isUnicodeLocaleType_67, .-ultag_isUnicodeLocaleType_67
	.p2align 4
	.globl	ultag_isTransformedExtensionSubtags_67
	.type	ultag_isTransformedExtensionSubtags_67, @function
ultag_isTransformedExtensionSubtags_67:
.LFB2401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	testl	%esi, %esi
	jns	.L363
	call	strlen@PLT
	movl	%eax, %ebx
.L363:
	testl	%ebx, %ebx
	jle	.L584
	movl	$0, -60(%rbp)
	movq	%r15, %r12
	xorl	%r13d, %r13d
	movl	$1, %r14d
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L404:
	addl	$1, %r13d
	addq	$1, %r12
	subl	$1, %ebx
	je	.L596
.L417:
	cmpb	$45, (%r12)
	jne	.L404
	leaq	.L369(%rip), %rcx
	movl	%r14d, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L369:
	.long	.L374-.L369
	.long	.L373-.L369
	.long	.L372-.L369
	.long	.L371-.L369
	.long	.L370-.L369
	.long	.L370-.L369
	.long	.L584-.L369
	.long	.L368-.L369
	.text
	.p2align 4,,10
	.p2align 3
.L403:
	movl	$2, %r13d
	.p2align 4,,10
	.p2align 3
.L435:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZL16_isVariantSubtagPKci
	testb	%al, %al
	jne	.L421
.L569:
	cmpl	$2, %r13d
	jne	.L584
.L447:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
.L584:
	xorl	%eax, %eax
.L362:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L373:
	.cfi_restore_state
	leal	-2(%r13), %eax
	cmpl	$6, %eax
	ja	.L376
	leal	-1(%r13), %eax
	movq	%r15, %r14
	leaq	1(%r15,%rax), %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L378:
	movsbl	(%r14), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L376
	addq	$1, %r14
	cmpq	%r14, -56(%rbp)
	jne	.L378
	movl	%ebx, %r13d
	leaq	1(%r12), %r15
	subl	$1, %r13d
	je	.L411
	cmpb	$45, 1(%r12)
	jne	.L564
	movl	%r13d, %ebx
	movl	-60(%rbp), %r13d
	movq	%r15, %r12
.L370:
	leal	-5(%r13), %eax
	xorl	%r14d, %r14d
	cmpl	$3, %eax
	ja	.L442
	.p2align 4,,10
	.p2align 3
.L388:
	movsbl	(%r15,%r14), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L387
	movzbl	(%r15,%r14), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L584
.L387:
	addq	$1, %r14
	cmpl	%r14d, %r13d
	jg	.L388
.L456:
	movl	$4, -60(%rbp)
	movl	$5, %r14d
.L382:
	leaq	1(%r12), %r15
	xorl	%r13d, %r13d
	movq	%r15, %r12
	subl	$1, %ebx
	jne	.L417
.L596:
	leaq	.L439(%rip), %rcx
	movl	%r14d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L439:
	.long	.L427-.L439
	.long	.L440-.L439
	.long	.L414-.L439
	.long	.L420-.L439
	.long	.L435-.L439
	.long	.L435-.L439
	.long	.L584-.L439
	.long	.L430-.L439
	.text
.L371:
	cmpl	$2, %r13d
	je	.L413
	cmpl	$3, %r13d
	jne	.L370
.L572:
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L454
	movzbl	1(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L454
	movzbl	2(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L454
	movl	%ebx, %eax
	leaq	1(%r12), %r15
	subl	$1, %eax
	je	.L400
	cmpb	$45, 1(%r12)
	je	.L459
	movl	%ebx, %eax
	leaq	2(%r12), %rdx
	subl	$2, %eax
	je	.L402
	cmpb	$45, 2(%r12)
	je	.L467
	leaq	3(%r12), %rax
	subl	$3, %ebx
	je	.L403
	cmpb	$45, 3(%r12)
	movq	%rax, %r12
	je	.L468
	movl	$3, -60(%rbp)
	movl	$2, %r13d
	movl	$4, %r14d
	jmp	.L404
.L368:
	cmpl	$2, %r13d
	je	.L593
	leal	-3(%r13), %eax
	cmpl	$5, %eax
	ja	.L584
	xorl	%edx, %edx
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L398:
	addq	$1, %rdx
	cmpl	%edx, %r13d
	jle	.L382
.L399:
	movsbl	(%r15,%rdx), %edi
	movq	%rdx, -56(%rbp)
	call	uprv_isASCIILetter_67@PLT
	movq	-56(%rbp), %rdx
	testb	%al, %al
	jne	.L398
	movzbl	(%r15,%rdx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L398
	jmp	.L584
.L374:
	leal	-3(%r13), %eax
	cmpl	$5, %eax
	ja	.L584
	leal	-1(%r13), %eax
	movq	%r15, %r14
	leaq	1(%r15,%rax), %r15
	.p2align 4,,10
	.p2align 3
.L395:
	movsbl	(%r14), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L394
	movzbl	(%r14), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L584
.L394:
	addq	$1, %r14
	cmpq	%r14, %r15
	jne	.L395
	movl	$6, -60(%rbp)
	movl	$7, %r14d
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L467:
	movq	%rdx, %r12
	movl	$1, %r13d
	movl	%eax, %ebx
.L442:
	cmpl	$4, %r13d
	je	.L597
.L376:
	cmpl	$2, %r13d
	jne	.L584
.L593:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L584
	movzbl	1(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L584
	leaq	1(%r12), %r15
	cmpl	$1, %ebx
	je	.L584
	cmpb	$45, 1(%r12)
	je	.L584
	cmpl	$2, %ebx
	je	.L584
	cmpb	$45, 2(%r12)
	je	.L584
	leaq	3(%r12), %rax
	subl	$3, %ebx
	je	.L408
	cmpb	$45, 3(%r12)
	je	.L584
	movl	$-1, -60(%rbp)
	movq	%rax, %r12
	xorl	%r14d, %r14d
	jmp	.L404
.L372:
	cmpl	$4, %r13d
	jne	.L371
	leaq	4(%r15), %rax
	movq	%r15, %r14
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L383:
	movsbl	(%r14), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L370
	addq	$1, %r14
	cmpq	%r14, -56(%rbp)
	jne	.L383
	movl	$2, -60(%rbp)
	movl	$3, %r14d
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L464:
	movq	%rdx, %r12
	movl	%eax, %ebx
	.p2align 4,,10
	.p2align 3
.L413:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L468
	movsbl	1(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L468
	movl	%ebx, %eax
	leaq	1(%r12), %r15
	subl	$1, %eax
	je	.L400
	cmpb	$45, 1(%r12)
	je	.L459
	movl	%ebx, %eax
	leaq	2(%r12), %rdx
	subl	$2, %eax
	je	.L402
	cmpb	$45, 2(%r12)
	je	.L467
	leaq	3(%r12), %rax
	subl	$3, %ebx
	je	.L403
	cmpb	$45, 3(%r12)
	movq	%rax, %r12
	je	.L468
	movl	$3, -60(%rbp)
	movl	$2, %r13d
	movl	$4, %r14d
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L597:
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L584
	leaq	1(%r15), %r14
	addq	$4, %r15
	.p2align 4,,10
	.p2align 3
.L392:
	movsbl	(%r14), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L391
	movzbl	(%r14), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L584
.L391:
	addq	$1, %r14
	cmpq	%r14, %r15
	jne	.L392
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L564:
	movl	%ebx, %eax
	leaq	2(%r12), %rdx
	subl	$2, %eax
	je	.L462
	cmpb	$45, 2(%r12)
	je	.L467
	movl	%ebx, %eax
	leaq	3(%r12), %rdx
	subl	$3, %eax
	je	.L412
	cmpb	$45, 3(%r12)
	je	.L464
	leaq	4(%r12), %rax
	subl	$4, %ebx
	je	.L529
	cmpb	$45, 4(%r12)
	movl	$3, %r13d
	movq	%rax, %r12
	je	.L572
	movl	$1, -60(%rbp)
	movl	$2, %r14d
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L459:
	movq	%r15, %r12
	xorl	%r13d, %r13d
	movl	%eax, %ebx
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L414:
	cmpl	$4, %r13d
	je	.L598
.L420:
	cmpl	$2, %r13d
	je	.L412
	cmpl	$3, %r13d
	jne	.L411
.L529:
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L426
	movzbl	1(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L426
	movzbl	2(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L421
.L426:
	movl	$3, %esi
	movq	%r15, %rdi
	call	_ZL16_isVariantSubtagPKci
	testb	%al, %al
	je	.L584
.L421:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	cmpl	$2, %r13d
	je	.L599
	leal	-3(%r13), %eax
	cmpl	$5, %eax
	ja	.L584
	leal	-1(%r13), %eax
	leaq	1(%r15,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L433:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L432
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L584
.L432:
	addq	$1, %r15
	cmpq	%r15, %r12
	jne	.L433
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L440:
	leal	-2(%r13), %eax
	xorl	%ebx, %ebx
	cmpl	$6, %eax
	ja	.L569
	.p2align 4,,10
	.p2align 3
.L419:
	movsbl	(%r15,%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L569
	addq	$1, %rbx
	cmpl	%ebx, %r13d
	jg	.L419
	jmp	.L421
.L408:
	movl	$2, %r13d
	.p2align 4,,10
	.p2align 3
.L427:
	leal	-3(%r13), %eax
	cmpl	$5, %eax
	ja	.L584
	leal	-1(%r13), %eax
	leaq	1(%r15,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L429:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L428
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L584
.L428:
	addq	$1, %r15
	cmpq	%r15, %r12
	jne	.L429
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L468:
	movl	%ebx, %eax
	movl	$2, %r13d
	movl	%eax, %ebx
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L462:
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L411:
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZL16_isVariantSubtagPKci
	testb	%al, %al
	je	.L584
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L412:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L423
	movsbl	1(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L421
.L423:
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZL16_isVariantSubtagPKci
	testb	%al, %al
	je	.L447
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r15, %rbx
	leaq	4(%r15), %r12
	.p2align 4,,10
	.p2align 3
.L422:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L411
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L422
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L454:
	movl	%ebx, %eax
	movl	%eax, %ebx
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L400:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZL16_isVariantSubtagPKci
	testb	%al, %al
	je	.L584
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L599:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L362
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L402:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZL16_isVariantSubtagPKci
	testb	%al, %al
	jne	.L421
	jmp	.L584
	.cfi_endproc
.LFE2401:
	.size	ultag_isTransformedExtensionSubtags_67, .-ultag_isTransformedExtensionSubtags_67
	.p2align 4
	.globl	ultag_isUnicodeExtensionSubtags_67
	.type	ultag_isUnicodeExtensionSubtags_67, @function
ultag_isUnicodeExtensionSubtags_67:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testl	%esi, %esi
	jns	.L601
	call	strlen@PLT
	movl	%eax, %r14d
.L601:
	testl	%r14d, %r14d
	jle	.L602
	movq	%rbx, %r13
	xorl	%edx, %edx
	xorl	%r12d, %r12d
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L680:
	addl	$1, %r12d
.L604:
	addq	$1, %r13
	subl	$1, %r14d
	je	.L679
.L630:
	cmpb	$45, 0(%r13)
	jne	.L680
	cmpl	$1, %edx
	je	.L605
	cmpl	$2, %edx
	je	.L606
	testl	%edx, %edx
	je	.L607
.L602:
	xorl	%eax, %eax
.L600:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	cmpl	$2, %r12d
	je	.L681
	leal	-3(%r12), %eax
	cmpl	$5, %eax
	ja	.L602
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L615:
	movsbl	(%rbx,%r15), %edi
	movl	%edx, -52(%rbp)
	call	uprv_isASCIILetter_67@PLT
	movl	-52(%rbp), %edx
	testb	%al, %al
	jne	.L613
	movzbl	(%rbx,%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L602
.L613:
	addq	$1, %r15
	cmpl	%r15d, %r12d
	jg	.L615
	leaq	1(%r13), %rbx
	xorl	%r12d, %r12d
	movq	%rbx, %r13
.L683:
	subl	$1, %r14d
	jne	.L630
.L679:
	cmpl	$2, %edx
	je	.L646
	jg	.L602
	testl	%edx, %edx
	je	.L648
	cmpl	$1, %edx
	jne	.L602
	cmpl	$2, %r12d
	je	.L678
.L636:
	leal	-3(%r12), %eax
	cmpl	$5, %eax
	ja	.L602
	leal	-1(%r12), %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L639:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L638
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L602
.L638:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L639
.L634:
	movl	$1, %eax
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L606:
	cmpl	$2, %r12d
	je	.L682
	leal	-3(%r12), %eax
	cmpl	$5, %eax
	ja	.L602
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L625:
	movsbl	(%rbx,%r15), %edi
	movl	%edx, -52(%rbp)
	call	uprv_isASCIILetter_67@PLT
	movl	-52(%rbp), %edx
	testb	%al, %al
	jne	.L624
	movzbl	(%rbx,%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L602
.L624:
	addq	$1, %r15
	cmpl	%r15d, %r12d
	jg	.L625
	leaq	1(%r13), %rbx
	xorl	%r12d, %r12d
	movq	%rbx, %r13
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L605:
	leal	(%r14,%r13), %r15d
	cmpl	$2, %r12d
	je	.L650
	leal	-3(%r12), %eax
	cmpl	$5, %eax
	ja	.L602
	leal	-1(%r12), %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L620:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L619
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L602
.L619:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L620
	leaq	1(%r13), %rbx
	movl	$2, %edx
	xorl	%r12d, %r12d
	movq	%rbx, %r13
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L681:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L610
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L602
.L610:
	movsbl	1(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L602
	leaq	1(%r13), %rbx
	cmpl	$1, %r14d
	je	.L602
	cmpb	$45, 1(%r13)
	je	.L602
	leaq	2(%r13), %rax
	subl	$2, %r14d
	je	.L656
	cmpb	$45, 2(%r13)
	je	.L602
	movq	%rax, %r13
	movl	$1, %edx
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L650:
	movsbl	(%rbx), %edi
	movl	%edx, -52(%rbp)
	call	uprv_isASCIILetter_67@PLT
	movl	-52(%rbp), %edx
	testb	%al, %al
	jne	.L616
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L602
.L616:
	movsbl	1(%rbx), %edi
	movl	%edx, -52(%rbp)
	call	uprv_isASCIILetter_67@PLT
	movl	-52(%rbp), %edx
	testb	%al, %al
	je	.L602
	addq	$1, %r13
	movl	%r15d, %r14d
	subl	%r13d, %r14d
	testl	%r14d, %r14d
	jle	.L655
	cmpb	$45, 0(%r13)
	je	.L602
	movl	$1, %r12d
	movq	%r13, %rbx
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L682:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L622
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L602
.L622:
	movsbl	1(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L602
	leaq	1(%r13), %rbx
	subl	$1, %r14d
	je	.L673
	cmpb	$45, 1(%r13)
	je	.L602
	movq	%rbx, %r13
	movl	$1, %r12d
	movl	$1, %edx
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L646:
	cmpl	$2, %r12d
	je	.L678
	leal	-3(%r12), %eax
	cmpl	$5, %eax
	ja	.L602
	leal	-1(%r12), %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L643:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L642
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L602
.L642:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L643
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L648:
	cmpl	$2, %r12d
	je	.L678
	leal	-3(%r12), %eax
	cmpl	$5, %eax
	ja	.L602
	leal	-1(%r12), %eax
	leaq	1(%rbx,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L635:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L633
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L602
.L633:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L635
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L678:
	movsbl	(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L632
	movzbl	(%rbx), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L602
.L632:
	movsbl	1(%rbx), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L602
	jmp	.L634
.L655:
	movq	%r13, %rbx
.L673:
	xorl	%r12d, %r12d
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L656:
	movl	$1, %r12d
	jmp	.L636
	.cfi_endproc
.LFE2402:
	.size	ultag_isUnicodeExtensionSubtags_67, .-ultag_isUnicodeExtensionSubtags_67
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"-"
.LC1:
	.string	"-u"
.LC2:
	.string	"true"
.LC3:
	.string	"yes"
	.text
	.p2align 4
	.globl	ulocimp_toLanguageTag_67
	.type	ulocimp_toLanguageTag_67, @function
ulocimp_toLanguageTag_67:
.LFB2430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-883(%rbp), %rbx
	subq	$1000, %rsp
	movq	%rsi, -960(%rbp)
	movl	%edx, -952(%rbp)
	movq	%rcx, -936(%rbp)
	movb	%dl, -945(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	movq	%rbx, -896(%rbp)
	movl	$0, -840(%rbp)
	movl	$40, -888(%rbp)
	movw	%ax, -884(%rbp)
	movl	$0, -924(%rbp)
	call	strlen@PLT
	movl	%eax, -920(%rbp)
	testl	%eax, %eax
	jle	.L997
	movl	%eax, %esi
	leaq	-924(%rbp), %r12
	leaq	-920(%rbp), %rbx
	leaq	-896(%rbp), %r14
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L686:
	movl	-920(%rbp), %edx
	movq	%r15, %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	uloc_canonicalize_67@PLT
	movl	%eax, %esi
	movl	-924(%rbp), %eax
	cmpl	$15, %eax
	jne	.L688
	movl	%esi, -920(%rbp)
	movl	$0, -924(%rbp)
.L689:
	movq	%r12, %r8
	movq	%rbx, %rcx
	movl	%esi, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString15getAppendBufferEiiRiR10UErrorCode@PLT
	movq	%rax, %r15
	movl	-924(%rbp), %eax
	testl	%eax, %eax
	jle	.L686
.L692:
	movq	-936(%rbp), %rcx
	movl	%eax, (%rcx)
.L687:
	cmpb	$0, -884(%rbp)
	jne	.L1247
.L684:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1248
	addq	$1000, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	testl	%eax, %eax
	jle	.L690
.L964:
	movq	-936(%rbp), %rax
	cmpb	$0, -884(%rbp)
	movl	$1, (%rax)
	je	.L684
.L1247:
	movq	-896(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L997:
	movq	%rbx, %rdi
.L685:
	call	locale_getKeywordsStart_67@PLT
	movq	-896(%rbp), %rbx
	cmpq	%rbx, %rax
	je	.L1249
.L693:
	movq	-936(%rbp), %rax
	movl	$0, -904(%rbp)
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L1250
.L732:
	movl	$0, -904(%rbp)
	testl	%eax, %eax
	jg	.L755
	leaq	-224(%rbp), %r12
	leaq	-904(%rbp), %rcx
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	uloc_getCountry_67@PLT
	movl	%eax, %r13d
	movl	-904(%rbp), %eax
	testl	%eax, %eax
	jg	.L733
	cmpl	$-124, %eax
	je	.L733
	testl	%r13d, %r13d
	jg	.L1251
.L735:
	movq	-896(%rbp), %rbx
.L1227:
	movq	-936(%rbp), %rax
.L1226:
	movl	(%rax), %eax
.L755:
	movl	$0, -904(%rbp)
	movb	$0, -946(%rbp)
	testl	%eax, %eax
	jg	.L792
	leaq	-224(%rbp), %r13
	leaq	-904(%rbp), %rcx
	movq	%rbx, %rdi
	movl	$157, %edx
	movq	%r13, %rsi
	call	uloc_getVariant_67@PLT
	movq	-896(%rbp), %rbx
	movl	%eax, -944(%rbp)
	movl	%eax, %ecx
	movl	-904(%rbp), %eax
	cmpl	$-124, %eax
	je	.L756
	testl	%eax, %eax
	jg	.L756
	testl	%ecx, %ecx
	jg	.L1252
	.p2align 4,,10
	.p2align 3
.L792:
	movq	-936(%rbp), %r14
	leaq	-744(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%rax, -760(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, -664(%rbp)
	movq	%r14, %rsi
	leaq	-552(%rbp), %rax
	movl	$0, -128(%rbp)
	movl	$0, -768(%rbp)
	movl	$8, -752(%rbp)
	movb	$0, -748(%rbp)
	movl	$0, -672(%rbp)
	movl	$8, -656(%rbp)
	movb	$0, -652(%rbp)
	movl	$0, -576(%rbp)
	movq	%rax, -568(%rbp)
	movl	$8, -560(%rbp)
	movb	$0, -556(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	call	uloc_openKeywords_67@PLT
	movl	(%r14), %r12d
	movq	%rax, -968(%rbp)
	testl	%r12d, %r12d
	jle	.L793
	cmpb	$0, -946(%rbp)
	je	.L794
.L795:
	leaq	-456(%rbp), %rax
	movq	$0, -904(%rbp)
	leaq	-916(%rbp), %r15
	movq	%rax, -472(%rbp)
	leaq	-819(%rbp), %rax
	movq	%rax, -984(%rbp)
	leaq	-912(%rbp), %rax
	movl	$0, -480(%rbp)
	movl	$8, -464(%rbp)
	movb	$0, -460(%rbp)
	movl	$0, -916(%rbp)
	movq	$0, -976(%rbp)
	movq	$0, -992(%rbp)
	movq	$0, -1000(%rbp)
	movq	%rax, -944(%rbp)
.L797:
	movq	-984(%rbp), %rax
	movq	-936(%rbp), %rdx
	xorl	%r11d, %r11d
	xorl	%esi, %esi
	movq	-968(%rbp), %rdi
	movw	%r11w, -820(%rbp)
	movq	%rax, -832(%rbp)
	movl	$0, -776(%rbp)
	movl	$40, -824(%rbp)
	call	uenum_next_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L798
	movl	$100, %r9d
	leaq	-832(%rbp), %r13
	movl	$100, -912(%rbp)
	movl	%r9d, %esi
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L1253:
	movl	-912(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	uloc_getKeywordValue_67@PLT
	movl	%eax, %esi
	movl	-916(%rbp), %eax
	cmpl	$15, %eax
	jne	.L800
	movl	%esi, -912(%rbp)
	movl	$0, -916(%rbp)
.L801:
	movq	-944(%rbp), %rcx
	movq	%r15, %r8
	movl	%esi, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString15getAppendBufferEiiRiR10UErrorCode@PLT
	movq	%rax, %r14
	movl	-916(%rbp), %eax
	testl	%eax, %eax
	jle	.L1253
.L799:
	cmpl	$7, %eax
	je	.L979
	cmpb	$0, -945(%rbp)
	je	.L804
.L877:
	movq	-936(%rbp), %rax
	movl	$1, (%rax)
.L798:
	cmpb	$0, -820(%rbp)
	jne	.L1254
.L882:
	cmpb	$0, -946(%rbp)
	jne	.L1255
.L884:
	movq	-936(%rbp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jle	.L897
.L900:
	movl	-480(%rbp), %eax
	movq	-472(%rbp), %rdi
	testl	%eax, %eax
	jle	.L898
	xorl	%ebx, %ebx
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L908:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-480(%rbp), %eax
	movq	-472(%rbp), %rdi
.L907:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L898
.L899:
	movq	(%rdi,%rbx,8), %r12
	testq	%r12, %r12
	je	.L907
	cmpb	$0, 12(%r12)
	je	.L908
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L793:
	cmpb	$0, -946(%rbp)
	jne	.L795
	cmpq	$0, -968(%rbp)
	jne	.L795
	.p2align 4,,10
	.p2align 3
.L796:
	movl	-576(%rbp), %eax
	movq	-568(%rbp), %rdi
	testl	%eax, %eax
	jle	.L911
	xorl	%ebx, %ebx
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L913:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-576(%rbp), %eax
	movq	-568(%rbp), %rdi
.L912:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L911
.L914:
	movq	(%rdi,%rbx,8), %r12
	testq	%r12, %r12
	je	.L912
	cmpb	$0, 12(%r12)
	je	.L913
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L911:
	cmpb	$0, -556(%rbp)
	jne	.L1256
.L915:
	movl	-672(%rbp), %eax
	movq	-664(%rbp), %r8
	testl	%eax, %eax
	jle	.L916
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L919:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L917
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-672(%rbp), %eax
	addq	$1, %rbx
	movq	-664(%rbp), %r8
	cmpl	%ebx, %eax
	jg	.L919
.L916:
	cmpb	$0, -652(%rbp)
	jne	.L1257
.L920:
	movl	-768(%rbp), %eax
	movq	-760(%rbp), %r8
	testl	%eax, %eax
	jle	.L921
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L924:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L922
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-768(%rbp), %eax
	addq	$1, %rbx
	movq	-760(%rbp), %r8
	cmpl	%ebx, %eax
	jg	.L924
.L921:
	cmpb	$0, -748(%rbp)
	jne	.L1258
.L926:
	movq	-936(%rbp), %rax
	movq	-896(%rbp), %rdi
	movl	$0, -904(%rbp)
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L687
	leaq	-384(%rbp), %r13
	movl	$157, %edx
	leaq	-904(%rbp), %rcx
	movq	%r13, %rsi
	call	uloc_getVariant_67@PLT
	movl	-904(%rbp), %edx
	testl	%edx, %edx
	jg	.L945
	cmpl	$-124, %edx
	je	.L945
	testl	%eax, %eax
	jle	.L1259
	movb	$1, -945(%rbp)
	xorl	%r15d, %r15d
.L1235:
	xorl	%r12d, %r12d
.L946:
	movzbl	0(%r13), %ebx
	cmpb	$45, %bl
	sete	%dl
	cmpb	$95, %bl
	sete	%al
	orb	%al, %dl
	jne	.L1026
	testb	%bl, %bl
	jne	.L1260
.L1200:
	testq	%r12, %r12
	jne	.L952
.L953:
	movq	-936(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L687
.L972:
	movq	-960(%rbp), %rdi
	leaq	-224(%rbp), %rsi
	movl	%r15d, %edx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L917:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L919
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L922:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L924
	cmpb	$0, -748(%rbp)
	je	.L926
.L1258:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L898:
	cmpb	$0, -460(%rbp)
	jne	.L1261
.L909:
	movq	-968(%rbp), %rax
	testq	%rax, %rax
	je	.L796
	movq	%rax, %rdi
	call	uenum_close_67@PLT
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L800:
	testl	%eax, %eax
	jg	.L799
	movl	%esi, -1008(%rbp)
	movl	%esi, %edx
	movq	%r15, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	cmpl	$-124, -916(%rbp)
	movl	-1008(%rbp), %r9d
	jne	.L806
	movl	$0, -916(%rbp)
.L806:
	leaq	_ZL20LOCALE_ATTRIBUTE_KEY(%rip), %rsi
	movq	%r12, %rdi
	movl	%r9d, -1008(%rbp)
	call	strcmp@PLT
	movl	-1008(%rbp), %r9d
	testl	%eax, %eax
	jne	.L807
	testl	%r9d, %r9d
	jg	.L1262
.L808:
	movl	-656(%rbp), %r12d
	cmpl	-672(%rbp), %r12d
	jne	.L871
	cmpl	$8, %r12d
	je	.L872
	leal	(%r12,%r12), %r13d
	testl	%r13d, %r13d
	jg	.L1263
.L979:
	movq	-936(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L690:
	movl	%esi, %edx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-924(%rbp), %eax
	cmpl	$-124, %eax
	je	.L1264
	movq	-896(%rbp), %rdi
	testl	%eax, %eax
	jg	.L692
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L1250:
	leaq	-904(%rbp), %r14
	leaq	-224(%rbp), %r12
	movl	$12, %edx
	movq	%rbx, %rdi
	movq	%r14, %rcx
	movq	%r12, %rsi
	call	uloc_getLanguage_67@PLT
	movl	%eax, %r13d
	movl	-904(%rbp), %eax
	cmpl	$-124, %eax
	je	.L712
	testl	%eax, %eax
	jle	.L1265
.L712:
	cmpb	$0, -952(%rbp)
	jne	.L1266
.L715:
	movq	-960(%rbp), %rdi
	movl	$3, %edx
	leaq	_ZL8LANG_UND(%rip), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
.L708:
	movq	-936(%rbp), %rax
	movq	-896(%rbp), %rbx
	movl	$0, -904(%rbp)
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L732
	movl	$6, %edx
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	uloc_getScript_67@PLT
	movl	-904(%rbp), %edx
	cmpl	$-124, %edx
	je	.L728
	testl	%edx, %edx
	jg	.L728
	testl	%eax, %eax
	jg	.L1267
.L725:
	movq	-896(%rbp), %rbx
	movq	-936(%rbp), %rax
.L1225:
	movl	(%rax), %eax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L1249:
	leaq	-924(%rbp), %rbx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	uloc_openKeywords_67@PLT
	movl	-924(%rbp), %r15d
	movq	%rax, %r14
	testl	%r15d, %r15d
	jle	.L1268
.L696:
	testq	%r14, %r14
	je	.L1224
	movq	%r14, %rdi
	call	uenum_close_67@PLT
.L1224:
	movq	-896(%rbp), %rbx
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L728:
	cmpb	$0, -952(%rbp)
	movq	-896(%rbp), %rbx
	movq	-936(%rbp), %rax
	je	.L1225
.L1231:
	movl	$1, (%rax)
	movb	$0, -946(%rbp)
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L804:
	movl	$0, -916(%rbp)
.L805:
	cmpb	$0, -820(%rbp)
	je	.L797
	movq	-832(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L897:
	movq	-904(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L900
	xorl	%r13d, %r13d
	movq	-960(%rbp), %r12
	movb	%r13b, -944(%rbp)
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L902:
	movq	(%r12), %rax
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	movq	(%rbx), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	(%r12), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdx
	movl	$5, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L904
	movl	$4, %ecx
	movq	%rdx, %rsi
	leaq	.LC3(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L904
	movq	(%r12), %rax
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	movq	8(%rbx), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	(%r12), %rax
	call	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L904:
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L900
.L906:
	cmpb	$0, -944(%rbp)
	movq	(%rbx), %r14
	jne	.L901
	movq	%r14, %rdi
	call	strlen@PLT
	cmpq	$1, %rax
	jbe	.L901
	movq	(%r12), %rax
	movl	$2, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	movb	$1, -944(%rbp)
	movq	(%rbx), %r14
.L901:
	leaq	_ZL20LOCALE_ATTRIBUTE_KEY(%rip), %rsi
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L902
	movq	-976(%rbp), %rax
	leaq	.LC0(%rip), %r14
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L904
	.p2align 4,,10
	.p2align 3
.L903:
	movq	(%r12), %rax
	movl	$1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*16(%rax)
	movq	(%r15), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	(%r12), %rax
	call	*16(%rax)
	movq	8(%r15), %r15
	testq	%r15, %r15
	jne	.L903
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L1256:
	call	uprv_free_67@PLT
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L920
.L892:
	cmpb	$0, -460(%rbp)
	je	.L794
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L794:
	movq	-968(%rbp), %rax
	testq	%rax, %rax
	je	.L929
	movq	%rax, %rdi
	call	uenum_close_67@PLT
.L929:
	movl	-576(%rbp), %eax
	movq	-568(%rbp), %rdi
	testl	%eax, %eax
	jle	.L930
	xorl	%ebx, %ebx
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L932:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-576(%rbp), %eax
	movq	-568(%rbp), %rdi
.L931:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L930
.L933:
	movq	(%rdi,%rbx,8), %r12
	testq	%r12, %r12
	je	.L931
	cmpb	$0, 12(%r12)
	je	.L932
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L930:
	cmpb	$0, -556(%rbp)
	jne	.L1269
.L934:
	movl	-672(%rbp), %eax
	movq	-664(%rbp), %r8
	testl	%eax, %eax
	jle	.L935
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L938:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L936
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-672(%rbp), %eax
	addq	$1, %rbx
	movq	-664(%rbp), %r8
	cmpl	%ebx, %eax
	jg	.L938
.L935:
	cmpb	$0, -652(%rbp)
	jne	.L1270
.L939:
	movl	-768(%rbp), %eax
	movq	-760(%rbp), %r8
	testl	%eax, %eax
	jle	.L921
	xorl	%ebx, %ebx
.L943:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L941
.L1271:
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-768(%rbp), %eax
	addq	$1, %rbx
	movq	-760(%rbp), %r8
	cmpl	%ebx, %eax
	jle	.L921
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	jne	.L1271
.L941:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L943
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L936:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L938
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L1255:
	movl	-656(%rbp), %ebx
	cmpl	-672(%rbp), %ebx
	jne	.L886
	cmpl	$8, %ebx
	je	.L887
	leal	(%rbx,%rbx), %r12d
	testl	%r12d, %r12d
	jg	.L1272
.L978:
	movq	-936(%rbp), %rax
	movq	-472(%rbp), %rdi
	movl	$7, (%rax)
	movl	-480(%rbp), %eax
	testl	%eax, %eax
	jle	.L892
	xorl	%ebx, %ebx
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L894:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-480(%rbp), %eax
	movq	-472(%rbp), %rdi
.L893:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L892
.L895:
	movq	(%rdi,%rbx,8), %r12
	testq	%r12, %r12
	je	.L893
	cmpb	$0, 12(%r12)
	je	.L894
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L894
.L887:
	movl	$256, %edi
	movl	$32, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L978
.L987:
	cmpl	%ebx, -656(%rbp)
	cmovle	-656(%rbp), %ebx
	movq	%r13, %rdi
	movq	-664(%rbp), %r14
	cmpl	%r12d, %ebx
	movl	%ebx, %edx
	cmovg	%r12d, %edx
	movq	%r14, %rsi
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
.L889:
	cmpb	$0, -652(%rbp)
	jne	.L1273
.L890:
	movq	%r13, -664(%rbp)
	movl	%r12d, -656(%rbp)
	movb	$1, -652(%rbp)
.L886:
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L891
	movq	$0, 16(%rax)
	movslq	-672(%rbp), %rax
	leaq	_ZL9POSIX_KEY(%rip), %rcx
	leaq	-904(%rbp), %rdi
	movq	%rcx, %xmm0
	leal	1(%rax), %edx
	movl	%edx, -672(%rbp)
	movq	-664(%rbp), %rdx
	movq	%rsi, (%rdx,%rax,8)
	leaq	_ZL11POSIX_VALUE(%rip), %rax
	movl	$1, %edx
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rsi)
	call	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L1254:
	movq	-832(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L733:
	cmpb	$0, -952(%rbp)
	movq	-896(%rbp), %rbx
	movq	-936(%rbp), %rax
	jne	.L1231
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L756:
	cmpb	$0, -952(%rbp)
	movb	$0, -946(%rbp)
	je	.L792
	movq	-936(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L945:
	cmpb	$0, -952(%rbp)
	je	.L687
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1261:
	call	uprv_free_67@PLT
	jmp	.L909
.L807:
	movq	%r12, %rdi
	movl	%r9d, -1008(%rbp)
	call	strlen@PLT
	movl	-1008(%rbp), %r9d
	cmpl	$1, %eax
	jg	.L1274
	movzbl	(%r12), %edx
	cmpb	$120, %dl
	je	.L1275
	cmpl	$1, %eax
	je	.L1276
.L850:
	cmpb	$0, -945(%rbp)
	jne	.L877
	jmp	.L805
.L1265:
	testl	%r13d, %r13d
	je	.L715
	movl	%r13d, %edx
	js	.L1277
.L709:
	leal	-2(%rdx), %eax
	cmpl	$6, %eax
	ja	.L712
	leal	-1(%rdx), %eax
	movq	%r12, %r15
	leaq	-223(%rbp,%rax), %rbx
.L714:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L712
	addq	$1, %r15
	cmpq	%r15, %rbx
	jne	.L714
	movq	%r12, %rcx
.L716:
	movl	(%rcx), %edx
	addq	$4, %rcx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L716
	movl	%eax, %edx
	movq	%rcx, %rbx
	leaq	_ZL15DEPRECATEDLANGS(%rip), %r15
	movl	%r13d, -944(%rbp)
	shrl	$16, %edx
	testl	$32896, %eax
	movq	%r12, %r13
	cmove	%edx, %eax
	leaq	2(%rcx), %rdx
	movq	%rdx, %rcx
	cmovne	%rbx, %rcx
	movl	%eax, %edx
	addb	%al, %dl
	movl	$2, %eax
	sbbq	$3, %rcx
	xorl	%ebx, %ebx
	subq	%r12, %rcx
	movq	%rcx, %r12
	movq	%r14, %rcx
	movq	%r15, %r14
	movq	%rcx, %r15
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L1279:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1278
	addl	$2, %ebx
	addq	$8, %r14
	cmpl	$156, %ebx
	je	.L1206
	movq	%r14, %rdi
	call	strlen@PLT
.L720:
	cmpq	%r12, %rax
	jbe	.L1279
.L1206:
	movq	%r13, %r12
	movl	-944(%rbp), %r13d
	movq	%r15, %r14
.L718:
	movq	-960(%rbp), %rdi
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L708
.L1252:
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	%r14, -968(%rbp)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L1280:
	cmpb	$0, -945(%rbp)
	jne	.L776
.L764:
	testb	%r12b, %r12b
	je	.L1207
	xorl	%ebx, %ebx
.L780:
	addq	$1, %r13
.L758:
	movzbl	0(%r13), %r12d
	cmpb	$45, %r12b
	je	.L759
	testb	%r12b, %r12b
	je	.L762
	cmpb	$95, %r12b
	jne	.L760
	testb	%r12b, %r12b
	jne	.L759
	.p2align 4,,10
	.p2align 3
.L762:
	testq	%rbx, %rbx
	je	.L1280
	movsbl	(%rbx), %edi
	movq	%rbx, %r15
	testb	%dil, %dil
	je	.L769
	.p2align 4,,10
	.p2align 3
.L766:
	call	uprv_asciitolower_67@PLT
	addq	$1, %r15
	movb	%al, -1(%r15)
	movsbl	(%r15), %edi
	testb	%dil, %dil
	jne	.L766
.L769:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	_ZL16_isVariantSubtagPKci
	testb	%al, %al
	je	.L1281
	leaq	_ZL11POSIX_VALUE(%rip), %rsi
	movq	%rbx, %rdi
	call	strcmp@PLT
	cmpl	$5, -944(%rbp)
	jne	.L1025
	testl	%eax, %eax
	je	.L1006
.L1025:
	movl	$16, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1282
	movq	%rbx, (%rax)
	movq	-968(%rbp), %rax
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L772
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	%rax, %r14
.L772:
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L773
	movq	8(%r14), %rax
	testq	%rax, %rax
	jne	.L1284
	movq	%r15, 8(%r14)
	movq	$0, 8(%r15)
	jmp	.L764
.L1259:
	movq	-936(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L687
	xorl	%r15d, %r15d
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1251:
	cmpl	$2, %r13d
	je	.L1285
	cmpl	$3, %r13d
	je	.L1286
.L740:
	cmpb	$0, -952(%rbp)
	movq	-896(%rbp), %rbx
	je	.L1227
	movq	-936(%rbp), %rax
	jmp	.L1231
.L1267:
	cmpl	$4, %eax
	jne	.L728
	movq	%r12, %r13
	leaq	-220(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L730:
	movsbl	0(%r13), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L728
	addq	$1, %r13
	cmpq	%r13, %rbx
	jne	.L730
	movq	-960(%rbp), %rbx
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	$4, %edx
	movq	%r12, %rsi
	call	*16(%rax)
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L759:
	movb	$0, 0(%r13)
	movl	$1, %r12d
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L1026:
	testb	%bl, %bl
	je	.L1200
	movb	$0, 0(%r13)
	testq	%r12, %r12
	jne	.L1287
.L970:
	addq	$1, %r13
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L1287:
	movl	$1, %ebx
.L952:
	movsbl	(%r12), %edi
	movq	%r12, %r14
	testb	%dil, %dil
	je	.L958
	.p2align 4,,10
	.p2align 3
.L955:
	call	uprv_asciitolower_67@PLT
	addq	$1, %r14
	movb	%al, -1(%r14)
	movsbl	(%r14), %edi
	testb	%dil, %dil
	jne	.L955
.L958:
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, %r14
	leal	-1(%rax), %eax
	cmpl	$7, %eax
	jbe	.L1288
.L957:
	cmpb	$0, -952(%rbp)
	jne	.L964
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L760:
	testq	%rbx, %rbx
	cmove	%r13, %rbx
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L1281:
	cmpb	$0, -945(%rbp)
	jne	.L776
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	%rax, %r15
	leal	-1(%rax), %eax
	cmpl	$7, %eax
	ja	.L764
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L779:
	movsbl	(%rbx,%r14), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L777
	movzbl	(%rbx,%r14), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L764
.L777:
	addq	$1, %r14
	cmpl	%r14d, %r15d
	jg	.L779
.L1207:
	movq	-936(%rbp), %rax
	movq	-968(%rbp), %r14
	movl	(%rax), %r13d
	testl	%r13d, %r13d
	jg	.L1240
	testq	%r14, %r14
	je	.L1230
	movq	8(%r14), %r15
	testq	%r15, %r15
	je	.L784
	movq	%r14, -944(%rbp)
	movq	%r14, %rbx
.L785:
	movq	%r15, %r12
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1289:
	movq	%r13, (%rbx)
	movq	%r14, (%r12)
	movq	8(%r12), %r12
	testq	%r12, %r12
	je	.L787
.L1229:
	movq	(%rbx), %r14
.L789:
	movq	(%r12), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jg	.L1289
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.L789
.L787:
	movq	8(%r15), %rax
	movq	%r15, %rbx
	testq	%rax, %rax
	je	.L1209
	movq	%rax, %r15
	jmp	.L785
.L773:
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	cmpb	$0, -945(%rbp)
	je	.L764
	movq	-936(%rbp), %rax
	movq	-968(%rbp), %r14
	movl	$1, (%rax)
	.p2align 4,,10
	.p2align 3
.L791:
	movq	%r14, %rdi
	movq	8(%r14), %r14
	call	uprv_free_67@PLT
.L1240:
	testq	%r14, %r14
	jne	.L791
.L1230:
	movq	-896(%rbp), %rbx
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L1260:
	testq	%r12, %r12
	jne	.L970
	movq	%r13, %r12
	addq	$1, %r13
	jmp	.L946
.L1288:
	movl	%r14d, -944(%rbp)
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L961:
	movsbl	(%r12,%r14), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L959
	movzbl	(%r12,%r14), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L957
.L959:
	addq	$1, %r14
	cmpl	%r14d, -944(%rbp)
	jg	.L961
	cmpb	$0, -945(%rbp)
	jne	.L1290
	cmpl	$156, %r15d
	jle	.L1291
	movq	%r12, %rdi
	movl	%r15d, %r14d
	call	strlen@PLT
	movl	%eax, %ecx
.L969:
	movb	$0, -945(%rbp)
	leal	(%rcx,%r14), %r15d
.L963:
	addq	$1, %r13
	testb	%bl, %bl
	jne	.L1235
	jmp	.L953
.L872:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L979
	movl	$32, %r13d
.L988:
	cmpl	%r12d, -656(%rbp)
	cmovle	-656(%rbp), %r12d
	movq	%r14, %rdi
	movq	-664(%rbp), %r8
	cmpl	%r13d, %r12d
	movl	%r12d, %edx
	cmovg	%r13d, %edx
	movq	%r8, %rsi
	movq	%r8, -1008(%rbp)
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movq	-1008(%rbp), %r8
.L874:
	cmpb	$0, -652(%rbp)
	jne	.L1292
.L875:
	movq	%r14, -664(%rbp)
	movl	%r13d, -656(%rbp)
	movb	$1, -652(%rbp)
.L871:
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L876
	movq	$0, 16(%rax)
	movslq	-672(%rbp), %rax
	leaq	-904(%rbp), %rdi
	movq	-1000(%rbp), %xmm0
	leal	1(%rax), %edx
	movl	%edx, -672(%rbp)
	movq	-664(%rbp), %rdx
	movhps	-992(%rbp), %xmm0
	movq	%rsi, (%rdx,%rax,8)
	movl	$1, %edx
	movups	%xmm0, (%rsi)
	call	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a
	testb	%al, %al
	jne	.L805
	cmpb	$0, -945(%rbp)
	je	.L805
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L1291:
	leal	1(%r15), %r14d
	movslq	%r15d, %r15
	movb	$45, -224(%rbp,%r15)
.L966:
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, %ecx
	cmpl	$156, %r14d
	jg	.L969
	movl	$157, %esi
	movl	%eax, %edi
	movl	%eax, -944(%rbp)
	subl	%r14d, %esi
	call	uprv_min_67@PLT
	movslq	%r14d, %rdx
	movq	%r12, %rsi
	leaq	-224(%rbp,%rdx), %rdi
	movslq	%eax, %rdx
	call	memcpy@PLT
	movl	-944(%rbp), %ecx
	jmp	.L969
.L1290:
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZL16_isVariantSubtagPKci
	testb	%al, %al
	jne	.L963
	cmpl	$156, %r15d
	jg	.L1202
	movslq	%r15d, %rdx
	leal	1(%r15), %eax
	movb	$45, -224(%rbp,%rdx)
	je	.L967
	cltq
	leal	2(%r15), %edx
	movb	$120, -224(%rbp,%rax)
	cmpl	$155, %r15d
	je	.L967
	movslq	%edx, %rax
	leal	3(%r15), %r14d
	movb	$45, -224(%rbp,%rax)
	cmpl	$154, %r15d
	je	.L967
	movl	$157, %esi
	movl	$8, %edi
	subl	%r14d, %esi
	movslq	%r14d, %r14
	call	uprv_min_67@PLT
	leaq	-224(%rbp,%r14), %rdi
	leaq	_ZL22PRIVUSE_VARIANT_PREFIX(%rip), %rsi
	movslq	%eax, %rdx
	call	memcpy@PLT
	leal	11(%r15), %edx
	cmpl	$156, %edx
	jg	.L968
	movslq	%edx, %rdx
	leal	12(%r15), %r14d
	movb	$45, -224(%rbp,%rdx)
	jmp	.L966
.L1264:
	movl	$0, -924(%rbp)
	movq	-896(%rbp), %rdi
	jmp	.L685
.L1266:
	movq	-936(%rbp), %rax
	movq	-896(%rbp), %rbx
	movl	$1, (%rax)
	movl	$1, %eax
	jmp	.L755
.L1202:
	movq	%r12, %rdi
	leal	8(%r15), %r14d
	call	strlen@PLT
	movl	%eax, %ecx
	jmp	.L969
.L1006:
	movb	$1, -946(%rbp)
	jmp	.L764
.L1283:
	movq	$0, 8(%r15)
	movq	%r15, -968(%rbp)
	jmp	.L764
.L1270:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L939
.L1269:
	call	uprv_free_67@PLT
	jmp	.L934
.L1268:
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	uenum_count_67@PLT
	cmpl	$1, %eax
	jne	.L696
	leaq	-904(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movl	$0, -904(%rbp)
	call	uenum_next_67@PLT
	cmpl	$1, -904(%rbp)
	movq	%rax, %rsi
	jne	.L696
	cmpb	$120, (%rax)
	jne	.L696
	leaq	-222(%rbp), %r15
	movq	%rbx, %r8
	movl	$98, %ecx
	movq	%r13, %rdi
	movq	%r15, %rdx
	leaq	-224(%rbp), %r12
	movw	$11640, -224(%rbp)
	call	uloc_getKeywordValue_67@PLT
	cmpl	$0, -924(%rbp)
	movl	%eax, -904(%rbp)
	movl	%eax, %esi
	jle	.L1293
.L698:
	movq	-936(%rbp), %rax
	movl	$1, (%rax)
.L700:
	testq	%r14, %r14
	je	.L687
	movq	%r14, %rdi
	call	uenum_close_67@PLT
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L1274:
	movq	%r12, %rdi
	call	uloc_toUnicodeLocaleKey_67@PLT
	movq	%rax, -1000(%rbp)
	testq	%rax, %rax
	je	.L850
	movq	-832(%rbp), %rsi
	movq	%r12, %rdi
	call	uloc_toUnicodeLocaleType_67@PLT
	movq	%rax, -992(%rbp)
	testq	%rax, %rax
	je	.L850
	cmpq	-832(%rbp), %rax
	jne	.L808
	movl	-464(%rbp), %r12d
	cmpl	-480(%rbp), %r12d
	jne	.L838
	cmpl	$8, %r12d
	je	.L839
	leal	(%r12,%r12), %r14d
	testl	%r14d, %r14d
	jle	.L979
	movslq	%r14d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L979
	testl	%r12d, %r12d
	jg	.L991
.L841:
	cmpb	$0, -460(%rbp)
	jne	.L1294
.L842:
	movq	%r13, -472(%rbp)
	movl	%r14d, -464(%rbp)
	movb	$1, -460(%rbp)
.L838:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L868
	leaq	13(%rax), %rax
	xorl	%r8d, %r8d
	movl	$0, 56(%r13)
	movq	-992(%rbp), %rdi
	movq	%rax, 0(%r13)
	movslq	-480(%rbp), %rax
	movw	%r8w, 12(%r13)
	leal	1(%rax), %edx
	movl	$40, 8(%r13)
	movl	%edx, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	%r13, (%rdx,%rax,8)
	call	strlen@PLT
	leaq	-908(%rbp), %rcx
	movq	%r15, %r8
	movq	%r13, %rdi
	movl	%eax, %edx
	movl	%eax, %esi
	movq	%rax, %r12
	call	_ZN6icu_6710CharString15getAppendBufferEiiRiR10UErrorCode@PLT
	movq	%rax, %r14
	movl	-916(%rbp), %eax
	testl	%eax, %eax
	jg	.L869
	movq	-992(%rbp), %rsi
	movq	%r14, %rdi
	call	strcpy@PLT
	movq	%r14, %rdi
	call	T_CString_toLowerCase_67@PLT
	movq	%r15, %rcx
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movl	-916(%rbp), %eax
	testl	%eax, %eax
	jg	.L869
	movq	0(%r13), %rax
	movq	%rax, -992(%rbp)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L1262:
	xorl	%r13d, %r13d
	movq	%rbx, -1000(%rbp)
	xorl	%r12d, %r12d
	movl	%r9d, %ebx
	movq	%r15, -1008(%rbp)
	movq	-832(%rbp), %rcx
	movq	%r13, %r15
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L1295:
	movb	%al, -224(%rbp,%r12)
	leal	1(%r15), %eax
	leal	1(%r12), %edx
	cmpl	%eax, %ebx
	jle	.L811
.L812:
	addq	$1, %r15
	movslq	%edx, %r12
.L809:
	movzbl	(%rcx,%r15), %eax
	cmpb	$45, %al
	jne	.L1295
	testl	%r12d, %r12d
	jne	.L1296
	leal	1(%r15), %eax
	cmpl	%eax, %ebx
	jle	.L1222
.L814:
	movl	-752(%rbp), %r13d
	cmpl	-768(%rbp), %r13d
	jne	.L815
	cmpl	$8, %r13d
	je	.L816
	leal	(%r13,%r13), %ecx
	testl	%ecx, %ecx
	jg	.L1297
.L1214:
	movq	-1000(%rbp), %rbx
	movq	-1008(%rbp), %r15
.L980:
	movq	-936(%rbp), %rax
	movq	$0, -992(%rbp)
	movl	$7, (%rax)
	leaq	_ZL20LOCALE_ATTRIBUTE_KEY(%rip), %rax
	movq	%rax, -1000(%rbp)
	jmp	.L808
.L816:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1214
	movl	$32, %ecx
.L990:
	cmpl	%r13d, -752(%rbp)
	movq	-760(%rbp), %r9
	movq	%r14, %rdi
	movl	%ecx, -1016(%rbp)
	cmovle	-752(%rbp), %r13d
	movq	%r9, %rsi
	movq	%r9, -992(%rbp)
	cmpl	%r13d, %ecx
	movslq	%r13d, %rdx
	cmovle	%rcx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movl	-1016(%rbp), %ecx
	movq	-992(%rbp), %r9
.L818:
	cmpb	$0, -748(%rbp)
	jne	.L1298
.L819:
	movq	%r14, -760(%rbp)
	movl	%ecx, -752(%rbp)
	movb	$1, -748(%rbp)
.L815:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -992(%rbp)
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L820
	pxor	%xmm1, %xmm1
	movups	%xmm1, (%rax)
	movslq	-768(%rbp), %rax
	movl	-560(%rbp), %r13d
	leal	1(%rax), %edx
	movl	%edx, -768(%rbp)
	movq	-760(%rbp), %rdx
	movq	%rcx, (%rdx,%rax,8)
	cmpl	-576(%rbp), %r13d
	jne	.L822
	cmpl	$8, %r13d
	je	.L823
	leal	(%r13,%r13), %r14d
	testl	%r14d, %r14d
	jle	.L1214
	movslq	%r14d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1214
	movq	-568(%rbp), %r10
	testl	%r13d, %r13d
	jg	.L989
.L825:
	cmpb	$0, -556(%rbp)
	jne	.L1299
.L826:
	movq	%rcx, -568(%rbp)
	movl	%r14d, -560(%rbp)
	movb	$1, -556(%rbp)
.L822:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L827
	leaq	13(%rax), %rax
	xorl	%r9d, %r9d
	movl	%r12d, %edx
	movq	%r13, %rdi
	movq	-936(%rbp), %r14
	movq	%rax, 0(%r13)
	leaq	-224(%rbp), %rsi
	movl	$0, 56(%r13)
	movl	$40, 8(%r13)
	movq	%r14, %rcx
	movw	%r9w, 12(%r13)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movslq	-576(%rbp), %rax
	movl	(%r14), %r10d
	leal	1(%rax), %edx
	movl	%edx, -576(%rbp)
	movq	-568(%rbp), %rdx
	movq	%r13, (%rdx,%rax,8)
	testl	%r10d, %r10d
	jg	.L1222
	movq	0(%r13), %r12
	movq	-992(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%r12, (%rax)
	movq	-976(%rbp), %rax
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L829
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1302:
	movq	%rax, %r14
.L829:
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	js	.L1301
	je	.L833
	movq	8(%r14), %rax
	movq	%r14, %r13
	testq	%rax, %rax
	jne	.L1302
	movq	-992(%rbp), %rax
	movq	%rax, 8(%r14)
	movq	$0, 8(%rax)
	.p2align 4,,10
	.p2align 3
.L830:
	leal	1(%r15), %eax
	cmpl	%eax, %ebx
	jle	.L1222
	movq	-832(%rbp), %rcx
	xorl	%edx, %edx
	jmp	.L812
.L1296:
	movl	%r12d, %edx
.L811:
	movslq	%edx, %rax
	movl	%edx, %r12d
	movb	$0, -224(%rbp,%rax)
	jmp	.L814
.L833:
	cmpb	$0, -945(%rbp)
	je	.L830
	movq	-936(%rbp), %rax
	movq	-1000(%rbp), %rbx
	movq	$0, -992(%rbp)
	movq	-1008(%rbp), %r15
	movl	$1, (%rax)
	leaq	_ZL20LOCALE_ATTRIBUTE_KEY(%rip), %rax
	movq	%rax, -1000(%rbp)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L1300:
	movq	-992(%rbp), %rax
	movq	$0, 8(%rax)
	movq	%rax, -976(%rbp)
	jmp	.L830
.L1301:
	movq	-992(%rbp), %rax
	testq	%r13, %r13
	je	.L1012
	movq	%rax, 8(%r13)
.L832:
	movq	%r14, 8(%rax)
	jmp	.L830
.L823:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1214
	movl	$32, %r14d
.L989:
	cmpl	%r13d, -560(%rbp)
	cmovle	-560(%rbp), %r13d
	movq	%rcx, %rdi
	movq	-568(%rbp), %r10
	cmpl	%r13d, %r14d
	movl	%r13d, %edx
	cmovle	%r14d, %edx
	movq	%r10, %rsi
	movq	%r10, -1016(%rbp)
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movq	-1016(%rbp), %r10
	movq	%rax, %rcx
	jmp	.L825
.L1297:
	movslq	%ecx, %rdi
	movl	%ecx, -992(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1214
	testl	%r13d, %r13d
	movq	-760(%rbp), %r9
	movslq	-992(%rbp), %rcx
	jle	.L818
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1298:
	movq	%r9, %rdi
	movl	%ecx, -992(%rbp)
	call	uprv_free_67@PLT
	movl	-992(%rbp), %ecx
	jmp	.L819
.L1299:
	movq	%r10, %rdi
	movq	%rcx, -1016(%rbp)
	call	uprv_free_67@PLT
	movq	-1016(%rbp), %rcx
	jmp	.L826
.L1222:
	leaq	_ZL20LOCALE_ATTRIBUTE_KEY(%rip), %rax
	movq	-1000(%rbp), %rbx
	movq	$0, -992(%rbp)
	movq	%rax, -1000(%rbp)
	movq	-1008(%rbp), %r15
	jmp	.L808
.L1012:
	movq	%rax, -976(%rbp)
	jmp	.L832
.L1272:
	movslq	%r12d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L978
	movq	-664(%rbp), %r14
	testl	%ebx, %ebx
	jle	.L889
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1263:
	movslq	%r13d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L979
	movq	-664(%rbp), %r8
	testl	%r12d, %r12d
	jle	.L874
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	-832(%rbp), %rdi
	movl	%r9d, %esi
	movl	%r9d, -1008(%rbp)
	call	_ZL12_isSepListOfPFaPKciES0_i.constprop.4
	movl	-1008(%rbp), %r9d
	testb	%al, %al
	je	.L850
.L849:
	movq	-832(%rbp), %rax
	movl	-464(%rbp), %r14d
	movq	%rax, -992(%rbp)
	cmpl	-480(%rbp), %r14d
	jne	.L863
	cmpl	$8, %r14d
	je	.L864
	leal	(%r14,%r14), %r8d
	testl	%r8d, %r8d
	jle	.L979
	movslq	%r8d, %rdi
	movl	%r9d, -1008(%rbp)
	salq	$3, %rdi
	movl	%r8d, -1000(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L979
	testl	%r14d, %r14d
	movq	-472(%rbp), %r10
	movslq	-1000(%rbp), %r8
	movl	-1008(%rbp), %r9d
	jg	.L992
.L866:
	cmpb	$0, -460(%rbp)
	jne	.L1303
.L867:
	movq	%r13, -472(%rbp)
	movl	%r8d, -464(%rbp)
	movb	$1, -460(%rbp)
.L863:
	movl	$64, %edi
	movl	%r9d, -1000(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L868
	leaq	13(%rax), %rax
	xorl	%edi, %edi
	movl	-1000(%rbp), %r9d
	movq	%r15, %rcx
	movw	%di, 12(%r14)
	movq	-992(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, (%r14)
	movl	%r9d, %edx
	movl	$0, 56(%r14)
	movl	$40, 8(%r14)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movslq	-480(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	%r14, (%rdx,%rax,8)
	movl	-916(%rbp), %eax
	testl	%eax, %eax
	jg	.L869
	movq	(%r14), %rax
	movq	%r12, -1000(%rbp)
	movq	%rax, -992(%rbp)
	jmp	.L808
.L1285:
	movsbl	-224(%rbp), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L740
	movsbl	-223(%rbp), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L740
.L739:
	movq	-960(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpw	$21826, -224(%rbp)
	je	.L1304
.L741:
	cmpw	$17476, -224(%rbp)
	je	.L1305
.L744:
	cmpw	$22598, -224(%rbp)
	je	.L1306
.L746:
	cmpw	$20564, -224(%rbp)
	je	.L1307
.L748:
	cmpw	$17497, -224(%rbp)
	je	.L1308
.L750:
	cmpw	$21082, -224(%rbp)
	je	.L1309
.L754:
	movq	-960(%rbp), %rdi
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L735
.L776:
	movq	-936(%rbp), %rax
	movq	-968(%rbp), %r14
	movl	$1, (%rax)
	jmp	.L1240
.L1273:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L890
.L1286:
	movzbl	-224(%rbp), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L740
	movzbl	-223(%rbp), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L740
	movzbl	-222(%rbp), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L739
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L875
.L1305:
	cmpb	$0, -222(%rbp)
	movl	$2, %eax
	jne	.L744
.L743:
	addl	$1, %eax
	movl	$4, %edx
	movq	%r12, %rdi
	cltq
	leaq	(%rax,%rax,2), %rsi
	leaq	_ZL17DEPRECATEDREGIONS(%rip), %rax
	addq	%rax, %rsi
	call	__stpcpy_chk@PLT
	movl	%eax, %r13d
	subl	%r12d, %r13d
	jmp	.L754
.L1304:
	xorl	%eax, %eax
	cmpb	$0, -222(%rbp)
	je	.L743
	jmp	.L741
.L1307:
	cmpb	$0, -222(%rbp)
	movl	$6, %eax
	je	.L743
	jmp	.L748
.L1306:
	cmpb	$0, -222(%rbp)
	movl	$4, %eax
	je	.L743
	jmp	.L746
.L1277:
	movq	%r12, %rdx
.L710:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L710
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	subl	%r12d, %edx
	jmp	.L709
.L1209:
	movq	-944(%rbp), %r14
.L784:
	movq	-960(%rbp), %r13
	movq	%r14, %r12
	leaq	.LC0(%rip), %rbx
.L790:
	movq	0(%r13), %rax
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*16(%rax)
	movq	(%r12), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	0(%r13), %rax
	call	*16(%rax)
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.L790
	jmp	.L791
.L1308:
	cmpb	$0, -222(%rbp)
	movl	$8, %eax
	je	.L743
	jmp	.L750
.L1309:
	cmpb	$0, -222(%rbp)
	jne	.L754
	movl	$10, %eax
	jmp	.L743
.L1276:
	movsbl	%dl, %edi
	movl	%r9d, -1008(%rbp)
	call	uprv_isASCIILetter_67@PLT
	movl	-1008(%rbp), %r9d
	testb	%al, %al
	movzbl	(%r12), %eax
	jne	.L852
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	ja	.L850
.L852:
	movsbl	%al, %edi
	movl	%r9d, -1008(%rbp)
	call	uprv_asciitolower_67@PLT
	cmpb	$120, %al
	je	.L850
	movq	-832(%rbp), %rdi
	movl	-1008(%rbp), %r9d
	movq	%rdi, %r14
	movl	%r9d, %eax
	testl	%r9d, %r9d
	jns	.L853
	call	strlen@PLT
	movl	-1008(%rbp), %r9d
.L853:
	cltq
	testq	%rax, %rax
	jle	.L850
	movq	%r14, %rcx
	xorl	%r13d, %r13d
	movq	%r12, -1024(%rbp)
	movq	%rbx, -1008(%rbp)
	movq	%r13, %r12
	movq	%r14, %rbx
	movq	%rcx, %r13
	movl	%r9d, -1028(%rbp)
	movq	%rax, %r14
	movq	%r15, -1016(%rbp)
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L854:
	testq	%r12, %r12
	cmove	%rbx, %r12
.L857:
	addq	$1, %rbx
	movq	%rbx, %rax
	subq	%r13, %rax
	cmpq	%rax, %r14
	jle	.L1310
.L859:
	cmpb	$45, (%rbx)
	jne	.L854
	testq	%r12, %r12
	je	.L1218
	movq	%rbx, %rdx
	subq	%r12, %rdx
	movl	%edx, %eax
	testl	%edx, %edx
	jns	.L855
	movq	%r12, %rdi
	call	strlen@PLT
.L855:
	leal	-2(%rax), %edx
	cmpl	$6, %edx
	jbe	.L1311
.L1218:
	movq	-1008(%rbp), %rbx
	movq	-1016(%rbp), %r15
	jmp	.L850
.L1311:
	subl	$1, %eax
	movq	%r12, %r15
	leaq	1(%r12,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L858:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L856
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1218
.L856:
	addq	$1, %r15
	cmpq	%r15, %r12
	jne	.L858
	xorl	%r12d, %r12d
	jmp	.L857
.L1310:
	movq	%r12, %r13
	movq	%rbx, %r14
	movl	-1028(%rbp), %r9d
	movq	-1008(%rbp), %rbx
	movq	-1024(%rbp), %r12
	movq	-1016(%rbp), %r15
	testq	%r13, %r13
	je	.L850
	movq	%r14, %rax
	subq	%r13, %rax
	movl	%eax, %edx
	testl	%eax, %eax
	jns	.L860
	movq	%r13, %rdi
	movl	%r9d, -1008(%rbp)
	call	strlen@PLT
	movl	-1008(%rbp), %r9d
	movl	%eax, %edx
.L860:
	leal	-2(%rdx), %eax
	cmpl	$6, %eax
	ja	.L850
	leal	-1(%rdx), %eax
	movq	%r15, -1008(%rbp)
	movq	%r13, %r14
	movq	%rbx, %r15
	leaq	1(%r13,%rax), %r13
	movl	%r9d, %ebx
.L862:
	movsbl	(%r14), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L861
	movzbl	(%r14), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1219
.L861:
	addq	$1, %r14
	cmpq	%r14, %r13
	jne	.L862
	movl	%ebx, %r9d
	movq	%r15, %rbx
	movq	-1008(%rbp), %r15
	jmp	.L849
.L1278:
	leal	1(%rbx), %eax
	leaq	_ZL15DEPRECATEDLANGS(%rip), %rcx
	movq	%r13, %rdi
	movq	%r13, %r12
	cltq
	movl	$12, %edx
	movq	%r15, %r14
	leaq	(%rcx,%rax,4), %rsi
	call	__stpcpy_chk@PLT
	movl	%eax, %r13d
	subl	%r12d, %r13d
	jmp	.L718
.L864:
	movl	$256, %edi
	movl	%r9d, -1000(%rbp)
	call	uprv_malloc_67@PLT
	movl	-1000(%rbp), %r9d
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L979
	movl	$32, %r8d
.L992:
	cmpl	%r14d, -464(%rbp)
	cmovle	-464(%rbp), %r14d
	movq	%r13, %rdi
	movl	%r9d, -1016(%rbp)
	movq	-472(%rbp), %r10
	movl	%r8d, -1008(%rbp)
	cmpl	%r8d, %r14d
	movslq	%r14d, %rdx
	cmovg	%r8, %rdx
	movq	%r10, %rsi
	movq	%r10, -1000(%rbp)
	salq	$3, %rdx
	call	memcpy@PLT
	movl	-1016(%rbp), %r9d
	movl	-1008(%rbp), %r8d
	movq	-1000(%rbp), %r10
	jmp	.L866
.L1303:
	movq	%r10, %rdi
	movl	%r8d, -1008(%rbp)
	movl	%r9d, -1000(%rbp)
	call	uprv_free_67@PLT
	movl	-1008(%rbp), %r8d
	movl	-1000(%rbp), %r9d
	jmp	.L867
.L1248:
	call	__stack_chk_fail@PLT
.L839:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L979
	movl	$32, %r14d
.L991:
	cmpl	%r12d, -464(%rbp)
	cmovle	-464(%rbp), %r12d
	movq	%r13, %rdi
	movq	-472(%rbp), %rsi
	cmpl	%r14d, %r12d
	movl	%r12d, %edx
	cmovg	%r14d, %edx
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	jmp	.L841
.L827:
	movslq	-576(%rbp), %rax
	movq	-1000(%rbp), %rbx
	movq	-1008(%rbp), %r15
	leal	1(%rax), %edx
	movl	%edx, -576(%rbp)
	movq	-568(%rbp), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L980
.L820:
	movslq	-768(%rbp), %rax
	movq	-1000(%rbp), %rbx
	movq	-1008(%rbp), %r15
	leal	1(%rax), %edx
	movl	%edx, -768(%rbp)
	movq	-760(%rbp), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L980
.L1294:
	movq	-472(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L842
.L876:
	movslq	-672(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -672(%rbp)
	movq	-664(%rbp), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L979
.L1282:
	movq	-936(%rbp), %rax
	movq	-968(%rbp), %r14
	movl	$7, (%rax)
	jmp	.L1240
.L967:
	movl	$165, %edx
.L968:
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r12, %rdi
	movl	%edx, %r14d
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leal	-1(%rax), %ecx
	jmp	.L969
.L891:
	movslq	-672(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -672(%rbp)
	movq	-664(%rbp), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L978
.L1293:
	movq	%r15, %rdi
	call	_ZL12_isSepListOfPFaPKciES0_i.constprop.4
	testb	%al, %al
	jne	.L1312
	cmpb	$0, -952(%rbp)
	je	.L696
	jmp	.L698
.L1219:
	movq	%r15, %rbx
	movq	-1008(%rbp), %r15
	jmp	.L850
.L869:
	movq	-936(%rbp), %rcx
	movl	%eax, (%rcx)
	jmp	.L798
.L868:
	movslq	-480(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L979
.L1312:
	movl	-904(%rbp), %eax
	movq	-960(%rbp), %rdi
	movq	%r12, %rsi
	leal	2(%rax), %edx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L700
	.cfi_endproc
.LFE2430:
	.size	ulocimp_toLanguageTag_67, .-ulocimp_toLanguageTag_67
	.section	.rodata.str1.1
.LC4:
	.string	"_"
.LC5:
	.string	"@"
.LC6:
	.string	"="
.LC7:
	.string	";"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB8:
	.text
.LHOTB8:
	.p2align 4
	.globl	ulocimp_forLanguageTag_67
	.type	ulocimp_forLanguageTag_67, @function
ulocimp_forLanguageTag_67:
.LFB2432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$600, %rsp
	movq	%rdi, -536(%rbp)
	movq	%rdx, -592(%rbp)
	movq	%rcx, -584(%rbp)
	movq	%r8, -576(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L1314
	movl	$0, (%rcx)
.L1314:
	movq	-576(%rbp), %rax
	movl	(%rax), %r12d
	testl	%r12d, %r12d
	jg	.L1313
	testl	%ebx, %ebx
	js	.L1924
.L1317:
	leal	1(%rbx), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -520(%rbp)
	testq	%rax, %rax
	je	.L1919
	movq	-536(%rbp), %rsi
	movslq	%ebx, %r13
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	movb	$0, (%r14,%r13)
	movl	$88, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, -560(%rbp)
	testq	%rax, %rax
	je	.L1925
	movq	-520(%rbp), %r12
	movq	%rax, %rcx
	leaq	_ZL5EMPTY(%rip), %rax
	movq	%rax, 8(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 24(%rcx)
	movq	$0, 32(%rcx)
	movq	%rax, 40(%rcx)
	movq	%rax, 48(%rcx)
	movq	$0, 56(%rcx)
	movq	$0, 64(%rcx)
	movq	%rax, 80(%rcx)
	movq	%rax, 72(%rcx)
	movq	%r12, (%rcx)
	cmpl	$1, %ebx
	jle	.L1406
	xorl	%r13d, %r13d
	movl	$10, %edx
	leaq	_ZL13GRANDFATHERED(%rip), %r14
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1927:
	movq	(%r14,%r13,8), %rdi
	call	strlen@PLT
	movq	%rax, %rdx
.L1330:
	movl	%r13d, %ecx
	movl	%edx, %r15d
	cmpl	%edx, %ebx
	jl	.L1321
	jle	.L1322
	movslq	%edx, %rax
	cmpb	$45, (%r12,%rax)
	jne	.L1321
.L1322:
	movq	(%r14,%r13,8), %rdi
	movq	%r12, %rsi
	movl	%ecx, -528(%rbp)
	call	uprv_strnicmp_67@PLT
	movl	-528(%rbp), %ecx
	testl	%eax, %eax
	je	.L1926
.L1321:
	addq	$2, %r13
	cmpq	$52, %r13
	jne	.L1927
	movq	$0, -600(%rbp)
.L1329:
	movl	%ebx, -528(%rbp)
	movq	-520(%rbp), %r15
	xorl	%r13d, %r13d
	movl	$6, %r14d
	leaq	_ZL9REDUNDANT(%rip), %r12
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	(%r12,%r13,8), %rdi
	call	strlen@PLT
	movq	%rax, %r14
.L1336:
	movq	(%r12,%r13,8), %rdi
	movl	%r14d, %edx
	movq	%r15, %rsi
	movl	%r13d, %ebx
	call	uprv_strnicmp_67@PLT
	testl	%eax, %eax
	jne	.L1332
	leaq	(%r15,%r14), %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L1666
	cmpb	$45, %al
	je	.L1666
.L1332:
	addq	$2, %r13
	cmpq	$52, %r13
	jne	.L1928
.L1331:
	movq	-520(%rbp), %r15
	movq	$0, -552(%rbp)
	movl	$129, %r13d
	movq	$0, -568(%rbp)
	movq	$0, -536(%rbp)
	movq	%r15, %r12
	movb	$0, -608(%rbp)
	movl	$0, -612(%rbp)
.L1337:
	movzbl	(%r12), %eax
	movl	%r13d, %edx
	andl	$1, %edx
	testb	%al, %al
	je	.L1339
	movq	%r12, %rcx
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1930:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L1929
.L1341:
	movq	%rcx, %rbx
	addq	$1, %rcx
	cmpb	$45, %al
	jne	.L1930
	movq	%rcx, -544(%rbp)
.L1340:
	movq	%rbx, %r14
	subq	%r12, %r14
	movl	%r14d, -528(%rbp)
	testw	%dx, %dx
	jne	.L1931
.L1342:
	testb	$2, %r13b
	jne	.L1932
.L1349:
	testb	$4, %r13b
	jne	.L1933
.L1353:
	testb	$8, %r13b
	jne	.L1934
.L1362:
	testb	$16, %r13b
	jne	.L1935
.L1367:
	testb	$32, %r13b
	jne	.L1936
.L1378:
	testb	$64, %r13b
	jne	.L1937
.L1386:
	andl	$128, %r13d
	je	.L1338
	movsbl	(%r12), %edi
	call	uprv_asciitolower_67@PLT
	cmpb	$120, %al
	jne	.L1338
	cmpl	$1, -528(%rbp)
	jne	.L1338
	cmpq	$0, -536(%rbp)
	je	.L1391
	cmpq	$0, -568(%rbp)
	je	.L1912
	cmpq	$0, -552(%rbp)
	je	.L1912
	movq	-552(%rbp), %rax
	movq	-568(%rbp), %rdi
	movb	$0, (%rax)
	call	T_CString_toLowerCase_67@PLT
	movq	-536(%rbp), %rsi
	movq	%rax, 8(%rsi)
	movq	-560(%rbp), %rax
	leaq	64(%rax), %rdi
	call	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0
	testb	%al, %al
	je	.L1912
	movq	-552(%rbp), %rax
	movq	%rax, -520(%rbp)
.L1391:
	movq	-544(%rbp), %rax
	testq	%rax, %rax
	je	.L1385
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L1598:
	movzbl	(%r14), %r13d
	movl	$8, %edx
	leaq	_ZL22PRIVUSE_VARIANT_PREFIX(%rip), %rsi
	movq	%r14, %rdi
	call	strncmp@PLT
	testb	%r13b, %r13b
	je	.L1394
	movq	%r14, %r12
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1939:
	movzbl	(%r12), %r13d
	testb	%r13b, %r13b
	je	.L1938
.L1396:
	movq	%r12, %rbx
	addq	$1, %r12
	cmpb	$45, %r13b
	jne	.L1939
	movq	%rbx, %rsi
	subq	%r14, %rsi
	movl	%esi, %r15d
	testl	%eax, %eax
	je	.L1613
.L1397:
	testl	%esi, %esi
	jns	.L1911
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rsi
	movl	%eax, %r15d
.L1911:
	subl	$1, %esi
	cmpl	$7, %esi
	jbe	.L1940
.L1400:
	movq	-520(%rbp), %rax
	subq	-544(%rbp), %rax
	testq	%rax, %rax
	jg	.L1941
.L1385:
	movq	-584(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L1406
	movq	-560(%rbp), %rcx
	movq	-520(%rbp), %rax
	subq	(%rcx), %rax
	addl	-600(%rbp), %eax
	movl	%eax, (%rsi)
.L1406:
	movq	-576(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L1410
	movq	-560(%rbp), %rbx
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1416
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	%rbx, %rdi
	movq	8(%rbx), %rbx
	call	uprv_free_67@PLT
	testq	%rbx, %rbx
	jne	.L1413
.L1416:
	movq	-560(%rbp), %rax
	movq	64(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1586
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	uprv_free_67@PLT
	testq	%rbx, %rbx
	jne	.L1414
.L1586:
	movq	-560(%rbp), %rdi
	call	uprv_free_67@PLT
.L1313:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1942
	addq	$600, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1924:
	.cfi_restore_state
	movq	-536(%rbp), %rdi
	call	strlen@PLT
	movl	%eax, %ebx
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	%r12, %rbx
	xorl	%r12d, %r12d
	movq	%rbx, %rsi
	subq	%r14, %rsi
	movl	%esi, %r15d
	testl	%eax, %eax
	jne	.L1397
.L1613:
	movb	$0, (%rbx)
	movl	$16, %r13d
	movb	$1, -608(%rbp)
	movq	$0, -536(%rbp)
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1940:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1403:
	movsbl	(%r14,%r13), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L1401
	movzbl	(%r14,%r13), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1400
.L1401:
	addq	$1, %r13
	cmpl	%r13d, %r15d
	jg	.L1403
	movq	%rbx, -520(%rbp)
	testq	%r12, %r12
	je	.L1400
	movq	%r12, %r14
	jmp	.L1598
.L1369:
	movq	-576(%rbp), %rax
	movq	-560(%rbp), %rbx
	movl	$7, (%rax)
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1327
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	%rbx, %rdi
	movq	8(%rbx), %rbx
	call	uprv_free_67@PLT
	testq	%rbx, %rbx
	jne	.L1326
.L1327:
	movq	-560(%rbp), %rax
	movq	64(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1408
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	uprv_free_67@PLT
	testq	%rbx, %rbx
	jne	.L1407
.L1408:
	movq	-560(%rbp), %rdi
	call	uprv_free_67@PLT
	movq	-576(%rbp), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	jg	.L1313
	movq	$0, -560(%rbp)
.L1410:
	movq	-560(%rbp), %rax
	movq	16(%rax), %r12
	testq	%r12, %r12
	je	.L1943
.L1412:
	leaq	_ZL8LANG_UND(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	movb	$1, -520(%rbp)
	testl	%eax, %eax
	je	.L1417
	movq	%r12, %rdi
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L1417
	movq	-592(%rbp), %rdi
	movl	%eax, %edx
	movq	%r12, %rsi
	movq	(%rdi), %rcx
	call	*16(%rcx)
	movb	$0, -520(%rbp)
.L1417:
	movq	-560(%rbp), %rax
	movq	40(%rax), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	%rax, %r13
	testl	%eax, %eax
	jle	.L1418
	movq	-592(%rbp), %r14
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	leaq	-496(%rbp), %r12
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	movsbl	(%rbx), %edi
	call	uprv_toupper_67@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	%al, -496(%rbp)
	movq	(%r14), %rax
	call	*16(%rax)
	movq	(%r14), %rax
	leal	-1(%r13), %edx
	leaq	1(%rbx), %rsi
	movq	%r14, %rdi
	call	*16(%rax)
	movb	$0, -520(%rbp)
.L1418:
	movq	-560(%rbp), %rax
	movq	48(%rax), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movb	$1, -528(%rbp)
	testl	%eax, %eax
	jle	.L1419
	movq	-592(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	leaq	-496(%rbp), %r12
	movq	(%rdi), %rax
	call	*16(%rax)
	movzbl	(%rbx), %eax
	movq	-592(%rbp), %r13
	testb	%al, %al
	je	.L1421
	.p2align 4,,10
	.p2align 3
.L1420:
	movsbl	%al, %edi
	addq	$1, %rbx
	call	uprv_toupper_67@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%al, -496(%rbp)
	movq	0(%r13), %rax
	call	*16(%rax)
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L1420
.L1421:
	movb	$0, -520(%rbp)
	movb	$0, -528(%rbp)
.L1419:
	movq	-560(%rbp), %rax
	movq	56(%rax), %r14
	testq	%r14, %r14
	je	.L1422
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.L1635
	movq	%r14, -536(%rbp)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L1424:
	movq	%r12, %rbx
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	%r15, (%r14)
	movq	%r13, (%rbx)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1427
.L1913:
	movq	(%r14), %r13
.L1429:
	movq	(%rbx), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jg	.L1944
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1429
.L1427:
	movq	8(%r12), %rax
	movq	%r12, %r14
	testq	%rax, %rax
	je	.L1636
	movq	%rax, %r12
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	-560(%rbp), %rax
	cmpq	$0, 64(%rax)
	je	.L1945
	cmpb	$0, -520(%rbp)
	je	.L1440
	movq	-592(%rbp), %rdi
	movl	$3, %edx
	leaq	_ZL8LANG_UND(%rip), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
.L1440:
	leaq	-456(%rbp), %rax
	movq	$0, -512(%rbp)
	movq	%rax, -472(%rbp)
	leaq	-360(%rbp), %rax
	movq	%rax, -376(%rbp)
	movq	-576(%rbp), %rax
	movl	$0, -480(%rbp)
	movl	(%rax), %r9d
	movl	$8, -464(%rbp)
	movb	$0, -460(%rbp)
	movl	$0, -384(%rbp)
	movl	$8, -368(%rbp)
	movb	$0, -364(%rbp)
	testl	%r9d, %r9d
	jg	.L1441
	movq	-560(%rbp), %rax
	movq	64(%rax), %r14
	cmpq	$0, 56(%rax)
	setne	-600(%rbp)
	testq	%r14, %r14
	je	.L1443
	movq	%r14, %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	16(%rdx), %rdx
	addl	$1, %eax
	testq	%rdx, %rdx
	jne	.L1444
	movl	%eax, -584(%rbp)
	leaq	-512(%rbp), %rax
	movq	(%r14), %rbx
	movl	$0, -552(%rbp)
	movq	%rax, -608(%rbp)
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	8(%r14), %r14
	movl	-552(%rbp), %eax
	cmpb	$117, (%rbx)
	movl	%eax, -568(%rbp)
	movq	%r14, %r15
	je	.L1946
.L1452:
	movl	-464(%rbp), %r12d
	cmpl	-480(%rbp), %r12d
	jne	.L1553
	cmpl	$8, %r12d
	je	.L1554
	leal	(%r12,%r12), %r14d
	testl	%r14d, %r14d
	jg	.L1947
.L1588:
	movq	-576(%rbp), %rax
	movl	$7, (%rax)
.L1517:
	movl	-384(%rbp), %eax
	movq	-376(%rbp), %rdi
	testl	%eax, %eax
	jle	.L1574
	xorl	%ebx, %ebx
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-384(%rbp), %eax
	movq	-376(%rbp), %rdi
.L1575:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jle	.L1574
.L1577:
	movq	(%rdi,%rbx,8), %r12
	testq	%r12, %r12
	je	.L1575
	cmpb	$0, 12(%r12)
	je	.L1576
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1554:
	movl	$256, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1588
	movl	$32, %r14d
.L1612:
	cmpl	%r12d, -464(%rbp)
	cmovle	-464(%rbp), %r12d
	movq	%r13, %rdi
	movq	-472(%rbp), %r8
	cmpl	%r14d, %r12d
	movl	%r12d, %edx
	cmovg	%r14d, %edx
	movq	%r8, %rsi
	movq	%r8, -520(%rbp)
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, -460(%rbp)
	movq	-520(%rbp), %r8
	jne	.L1948
.L1557:
	movq	%r13, -472(%rbp)
	movl	%r14d, -464(%rbp)
	movb	$1, -460(%rbp)
.L1553:
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1558
	movq	$0, 16(%rax)
	movslq	-480(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r15, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	-608(%rbp), %rdi
	leal	1(%rax), %edx
	movl	%edx, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	%rsi, (%rdx,%rax,8)
	movups	%xmm0, (%rsi)
	call	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0
	testb	%al, %al
	je	.L1570
.L1552:
	addl	$1, -552(%rbp)
	movl	-552(%rbp), %eax
	cmpl	-584(%rbp), %eax
	je	.L1560
	movq	-560(%rbp), %rax
	movq	64(%rax), %r15
	testq	%r15, %r15
	je	.L1561
	movl	-568(%rbp), %ecx
	movq	%r15, %rbx
	xorl	%eax, %eax
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1881:
	cmpl	%edx, %ecx
	je	.L1446
.L1562:
	movq	16(%rbx), %rbx
	movl	%eax, %edx
	addl	$1, %eax
	testq	%rbx, %rbx
	jne	.L1881
.L1447:
	movl	-568(%rbp), %ecx
	xorl	%eax, %eax
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1451:
	cmpl	%edx, %ecx
	je	.L1949
.L1449:
	movq	16(%r15), %r15
	movl	%eax, %edx
	addl	$1, %eax
	testq	%r15, %r15
	jne	.L1451
	movl	-552(%rbp), %eax
	cmpb	$117, (%rbx)
	movl	%eax, -568(%rbp)
	jne	.L1452
.L1946:
	leaq	-264(%rbp), %rax
	movq	$0, -504(%rbp)
	movl	$0, -288(%rbp)
	movq	%rax, -280(%rbp)
	movl	$8, -272(%rbp)
	movb	$0, -268(%rbp)
	movzbl	(%r15), %r9d
	movq	$0, -544(%rbp)
	movl	$0, -528(%rbp)
	movl	$0, -520(%rbp)
	testb	%r9b, %r9b
	je	.L1454
	cmpb	$45, %r9b
	je	.L1638
	.p2align 4,,10
	.p2align 3
.L1960:
	movl	$1, %eax
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1950:
	testb	%dl, %dl
	je	.L1901
.L1457:
	movzbl	(%r15,%rax), %edx
	movl	%eax, %ebx
	leaq	(%r15,%rax), %r13
	movq	%rax, %rcx
	addq	$1, %rax
	cmpb	$45, %dl
	jne	.L1950
.L1901:
	movq	%rcx, -536(%rbp)
	cmpl	$2, %ebx
	je	.L1951
.L1455:
	movl	-272(%rbp), %edx
	cmpl	%edx, -528(%rbp)
	jne	.L1460
	cmpl	$8, %edx
	je	.L1461
	leal	(%rdx,%rdx), %r12d
	testl	%r12d, %r12d
	jg	.L1952
	movq	-280(%rbp), %rdi
.L1464:
	movq	-576(%rbp), %rax
	movl	$7, (%rax)
.L1470:
	testl	%edx, %edx
	jle	.L1503
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	(%rdi,%rbx,8), %r8
	testq	%r8, %r8
	je	.L1504
	movq	%r8, %rdi
	addq	$1, %rbx
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-288(%rbp), %edx
	movq	-280(%rbp), %rdi
	cmpl	%ebx, %edx
	jg	.L1506
.L1503:
	cmpb	$0, -268(%rbp)
	jne	.L1953
.L1507:
	movq	-576(%rbp), %rax
	movb	$0, -600(%rbp)
	movl	(%rax), %eax
.L1550:
	testl	%eax, %eax
	jle	.L1552
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	%r15, %r14
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	(%rbx), %rbx
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1461:
	movl	$256, %edi
	movl	%edx, -528(%rbp)
	call	uprv_malloc_67@PLT
	movl	-528(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1915
	movl	$32, %r12d
.L1611:
	cmpl	%edx, -272(%rbp)
	cmovle	-272(%rbp), %edx
	movq	%r14, %rdi
	movq	-280(%rbp), %r9
	cmpl	%r12d, %edx
	cmovg	%r12d, %edx
	movq	%r9, %rsi
	movq	%r9, -528(%rbp)
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, -268(%rbp)
	movq	-528(%rbp), %r9
	jne	.L1954
.L1466:
	movq	%r14, -280(%rbp)
	movl	%r12d, -272(%rbp)
	movb	$1, -268(%rbp)
.L1460:
	movl	$16, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1467
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movslq	-288(%rbp), %rax
	movq	-280(%rbp), %rdi
	leal	1(%rax), %esi
	movl	%esi, -288(%rbp)
	movq	%r14, (%rdi,%rax,8)
	movl	$100, %eax
	subl	-520(%rbp), %eax
	movl	%esi, -528(%rbp)
	cmpl	%eax, %ebx
	jge	.L1955
	movslq	-520(%rbp), %rax
	movq	-536(%rbp), %rdx
	movq	%r15, %rsi
	leaq	-192(%rbp,%rax), %r12
	movq	%r12, %rdi
	call	memcpy@PLT
	movl	-520(%rbp), %ecx
	leal	(%rbx,%rcx), %eax
	cltq
	movb	$0, -192(%rbp,%rax)
	leal	1(%rbx,%rcx), %eax
	xorl	%ebx, %ebx
	movl	%eax, -520(%rbp)
	movq	-544(%rbp), %rax
	movq	%r12, (%r14)
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1471
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L1958:
	movq	%rax, %r15
.L1471:
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	js	.L1957
	je	.L1472
	movq	8(%r15), %rax
	movq	%r15, %rbx
	testq	%rax, %rax
	jne	.L1958
	movq	%r14, 8(%r15)
	movq	$0, 8(%r14)
.L1472:
	cmpb	$0, 0(%r13)
	jne	.L1476
.L1641:
	xorl	%r15d, %r15d
.L1477:
	movl	-464(%rbp), %ebx
	cmpl	-480(%rbp), %ebx
	jne	.L1479
	cmpl	$8, %ebx
	je	.L1480
	leal	(%rbx,%rbx), %r13d
	testl	%r13d, %r13d
	jg	.L1959
.L1592:
	movq	-576(%rbp), %rax
	movl	-288(%rbp), %edx
	movq	-280(%rbp), %rdi
	movl	$7, (%rax)
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1476:
	movzbl	1(%r13), %r9d
	leaq	1(%r13), %r15
	testb	%r9b, %r9b
	je	.L1641
	cmpb	$45, %r9b
	jne	.L1960
.L1638:
	movq	$0, -536(%rbp)
	movq	%r15, %r13
	xorl	%ebx, %ebx
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1504:
	addq	$1, %rbx
	cmpl	%ebx, %edx
	jg	.L1506
	cmpb	$0, -268(%rbp)
	je	.L1507
.L1953:
	call	uprv_free_67@PLT
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	$0, 8(%r14)
	movq	%r14, -544(%rbp)
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1957:
	testq	%rbx, %rbx
	je	.L1640
	movq	%r14, 8(%rbx)
.L1474:
	movq	%r15, 8(%r14)
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1951:
	movsbl	%r9b, %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L1458
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1914
.L1458:
	movsbl	1(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L1459
.L1914:
	movl	-288(%rbp), %eax
	movl	%eax, -528(%rbp)
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1952:
	movslq	%r12d, %rdi
	movl	%edx, -528(%rbp)
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movl	-528(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1915
	movq	-280(%rbp), %r9
	testl	%edx, %edx
	jg	.L1611
	cmpb	$0, -268(%rbp)
	je	.L1466
.L1954:
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	%r14, -544(%rbp)
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1915:
	movl	-288(%rbp), %edx
	movq	-280(%rbp), %rdi
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	-536(%rbp), %r14
	xorl	%eax, %eax
	movq	%r14, %rcx
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	8(%rcx), %rcx
	movl	%eax, %r13d
	addl	$1, %eax
	testq	%rcx, %rcx
	jne	.L1425
.L1423:
	movq	-592(%rbp), %rbx
	cmpb	$0, -528(%rbp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	16(%rax), %rcx
	je	.L1430
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	*%rcx
	movq	(%rbx), %rax
	movb	$0, -520(%rbp)
	movq	16(%rax), %rcx
	movq	-560(%rbp), %rax
	movq	56(%rax), %r14
.L1430:
	movq	-592(%rbp), %r15
	addl	$1, %r13d
	xorl	%ebx, %ebx
	movq	%r14, %rax
	leaq	-496(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L1438:
	testq	%rax, %rax
	je	.L1900
	testl	%ebx, %ebx
	je	.L1432
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	8(%rax), %rax
	addl	$1, %edx
	testq	%rax, %rax
	je	.L1961
	cmpl	%ebx, %edx
	jne	.L1433
.L1432:
	movq	(%rax), %r14
	movq	%r15, %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	*%rcx
	movsbl	(%r14), %edi
	testb	%dil, %dil
	je	.L1437
	.p2align 4,,10
	.p2align 3
.L1434:
	call	uprv_toupper_67@PLT
	addq	$1, %r14
	movq	%r15, %rdi
	movq	%r12, %rsi
	movb	%al, -496(%rbp)
	movq	(%r15), %rax
	movl	$1, %edx
	call	*16(%rax)
	movsbl	(%r14), %edi
	testb	%dil, %dil
	jne	.L1434
.L1437:
	addl	$1, %ebx
	cmpl	%r13d, %ebx
	je	.L1422
	movq	(%r15), %rax
	movq	16(%rax), %rcx
	movq	-560(%rbp), %rax
	movq	56(%rax), %rax
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1947:
	movslq	%r14d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1588
	movq	-472(%rbp), %r8
	testl	%r12d, %r12d
	jg	.L1612
	cmpb	$0, -460(%rbp)
	je	.L1557
.L1948:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	$0, -544(%rbp)
	movq	%rcx, %rbx
	jmp	.L1340
.L1480:
	movl	$256, %edi
	movl	$32, %r13d
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1592
.L1610:
	cmpl	%ebx, -464(%rbp)
	cmovle	-464(%rbp), %ebx
	movq	%r12, %rdi
	movq	-472(%rbp), %r14
	cmpl	%r13d, %ebx
	movl	%ebx, %edx
	cmovg	%r13d, %edx
	movq	%r14, %rsi
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
.L1482:
	cmpb	$0, -460(%rbp)
	jne	.L1962
.L1483:
	movq	%r12, -472(%rbp)
	movl	%r13d, -464(%rbp)
	movb	$1, -460(%rbp)
.L1479:
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, -528(%rbp)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1484
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movl	-368(%rbp), %ebx
	movups	%xmm0, (%rax)
	movslq	-480(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	%rsi, (%rdx,%rax,8)
	cmpl	-384(%rbp), %ebx
	jne	.L1486
	cmpl	$8, %ebx
	je	.L1487
	leal	(%rbx,%rbx), %r14d
	testl	%r14d, %r14d
	jle	.L1592
	movslq	%r14d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1592
	movq	-376(%rbp), %r12
	testl	%ebx, %ebx
	jg	.L1609
.L1489:
	cmpb	$0, -364(%rbp)
	jne	.L1963
.L1490:
	movq	%r13, -376(%rbp)
	movl	%r14d, -368(%rbp)
	movb	$1, -364(%rbp)
.L1486:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1491
	leaq	13(%rax), %rax
	xorl	%r8d, %r8d
	movl	$0, 56(%rbx)
	movq	-576(%rbp), %r12
	movq	%rax, (%rbx)
	movslq	-384(%rbp), %rax
	leaq	-496(%rbp), %r13
	movw	%r8w, 12(%rbx)
	leal	1(%rax), %edx
	movl	$40, 8(%rbx)
	movl	%edx, -384(%rbp)
	movq	-376(%rbp), %rdx
	movq	%r15, -520(%rbp)
	movq	%rbx, (%rdx,%rax,8)
	movq	-544(%rbp), %rax
	movq	8(%rax), %r14
	movq	%r14, %r15
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	8(%r15), %r14
	cmpq	-544(%rbp), %r15
	je	.L1493
	movq	%r12, %rdx
	movl	$45, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
.L1493:
	movq	%r15, %rax
	movq	%r14, %r15
.L1492:
	movq	(%rax), %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6711StringPieceC1EPKc@PLT
	movl	-488(%rbp), %edx
	movq	%r12, %rcx
	movq	%rbx, %rdi
	movq	-496(%rbp), %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	testq	%r15, %r15
	jne	.L1494
	movq	-576(%rbp), %rax
	movq	-520(%rbp), %r15
	movl	(%rax), %edi
	testl	%edi, %edi
	jle	.L1964
	movl	-288(%rbp), %edx
	movq	-280(%rbp), %rdi
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1574:
	cmpb	$0, -364(%rbp)
	jne	.L1965
.L1578:
	movl	-480(%rbp), %eax
	movq	-472(%rbp), %r8
	testl	%eax, %eax
	jle	.L1579
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	(%r8,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L1580
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-480(%rbp), %eax
	addq	$1, %rbx
	movq	-472(%rbp), %r8
	cmpl	%ebx, %eax
	jg	.L1582
.L1579:
	cmpb	$0, -460(%rbp)
	je	.L1441
.L1966:
	movq	%r8, %rdi
	call	uprv_free_67@PLT
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	-560(%rbp), %rbx
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1587
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	%rbx, %rdi
	movq	8(%rbx), %rbx
	call	uprv_free_67@PLT
	testq	%rbx, %rbx
	jne	.L1584
.L1587:
	movq	-560(%rbp), %rax
	movq	64(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1586
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	uprv_free_67@PLT
	testq	%rbx, %rbx
	jne	.L1585
	movq	-560(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1955:
	movq	-576(%rbp), %rax
	movl	%esi, %edx
	movl	$1, (%rax)
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1580:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L1582
	cmpb	$0, -460(%rbp)
	je	.L1441
	jmp	.L1966
.L1376:
	movq	%r13, %rdi
	call	uprv_free_67@PLT
.L1338:
	cmpq	$0, -536(%rbp)
	je	.L1385
	cmpq	$0, -568(%rbp)
	je	.L1912
	cmpq	$0, -552(%rbp)
	je	.L1912
	movq	-552(%rbp), %rax
	movq	-568(%rbp), %rdi
	movb	$0, (%rax)
	call	T_CString_toLowerCase_67@PLT
	movq	-536(%rbp), %rsi
	movq	%rax, 8(%rsi)
	movq	-560(%rbp), %rax
	leaq	64(%rax), %rdi
	call	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0
	testb	%al, %al
	je	.L1912
	movq	-552(%rbp), %rax
	movq	%rax, -520(%rbp)
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1912:
	movq	-536(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L1385
.L1945:
	movq	72(%rax), %rax
	cmpb	$0, (%rax)
	jne	.L1440
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1931:
	testl	%r14d, %r14d
	js	.L1343
	leal	-2(%r14), %eax
	movl	%r14d, %ecx
.L1344:
	cmpl	$6, %eax
	ja	.L1342
	xorl	%r15d, %r15d
	movw	%r13w, -624(%rbp)
	movq	%r15, %r13
	movl	%ecx, %r15d
	.p2align 4,,10
	.p2align 3
.L1346:
	movsbl	(%r12,%r13), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L1889
	addq	$1, %r13
	cmpl	%r13d, %r15d
	jg	.L1346
	movb	$0, (%rbx)
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	T_CString_toLowerCase_67@PLT
	movq	-560(%rbp), %rcx
	cmpl	$3, %r14d
	movq	%rbx, -520(%rbp)
	setle	%r13b
	movq	-544(%rbp), %r12
	movq	%rax, 8(%rcx)
	leal	188(%r13,%r13), %r13d
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1932:
	movl	%r14d, %eax
	testl	%r14d, %r14d
	js	.L1967
	cmpl	$3, %eax
	jne	.L1349
.L1981:
	leaq	3(%r12), %rax
	movw	%r13w, -624(%rbp)
	movq	%r12, %r13
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L1352:
	movsbl	0(%r13), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L1890
	addq	$1, %r13
	cmpq	%r13, %r15
	jne	.L1352
	movl	-612(%rbp), %r14d
	movb	$0, (%rbx)
	movq	%r12, %rdi
	leal	1(%r14), %r13d
	call	T_CString_toLowerCase_67@PLT
	cmpl	$3, %r13d
	movl	%r13d, -612(%rbp)
	movq	-560(%rbp), %rcx
	setl	%r13b
	movq	%rax, %r8
	movslq	%r14d, %rax
	movq	%rbx, -520(%rbp)
	movzbl	%r13b, %r13d
	movq	%r8, 16(%rcx,%rax,8)
	movq	-544(%rbp), %r12
	leal	188(%r13,%r13), %r13d
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1933:
	testl	%r14d, %r14d
	js	.L1968
.L1616:
	movl	-528(%rbp), %eax
.L1356:
	cmpl	$4, %eax
	jne	.L1353
	movq	%r12, %r15
	leaq	4(%r12), %r14
	.p2align 4,,10
	.p2align 3
.L1358:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L1353
	addq	$1, %r15
	cmpq	%r15, %r14
	jne	.L1358
	movb	$0, (%rbx)
	movsbl	(%r12), %edi
	leaq	1(%r12), %r13
	call	uprv_toupper_67@PLT
	movb	%al, (%r12)
	movzbl	1(%r12), %eax
	testb	%al, %al
	je	.L1599
.L1359:
	movsbl	%al, %edi
	addq	$1, %r13
	call	uprv_asciitolower_67@PLT
	movb	%al, -1(%r13)
	movzbl	0(%r13), %eax
	testb	%al, %al
	jne	.L1359
.L1599:
	movq	-560(%rbp), %rax
	movq	%rbx, -520(%rbp)
	movl	$184, %r13d
	movq	%r12, 40(%rax)
	movq	-544(%rbp), %r12
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1934:
	movl	-528(%rbp), %r11d
	testl	%r11d, %r11d
	js	.L1969
.L1620:
	movl	-528(%rbp), %eax
.L1364:
	cmpl	$2, %eax
	je	.L1970
	cmpl	$3, %eax
	jne	.L1362
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1362
	movzbl	1(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1362
	movzbl	2(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1362
.L1366:
	movb	$0, (%rbx)
	movq	%r12, %rdi
	movl	$176, %r13d
	call	T_CString_toUpperCase_67@PLT
	movq	-560(%rbp), %rcx
	movq	%rbx, -520(%rbp)
	movq	-544(%rbp), %r12
	movq	%rax, 48(%rcx)
.L1360:
	testq	%r12, %r12
	jne	.L1337
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1935:
	movl	-528(%rbp), %esi
	movq	%r12, %rdi
	call	_ZL16_isVariantSubtagPKci
	testb	%al, %al
	jne	.L1373
	cmpb	$0, -608(%rbp)
	je	.L1367
	movl	-528(%rbp), %ecx
	movl	%ecx, %eax
	testl	%ecx, %ecx
	js	.L1971
.L1371:
	subl	$1, %eax
	cmpl	$7, %eax
	ja	.L1367
	movq	%r12, %r15
	leaq	1(%r12,%rax), %r14
.L1374:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L1372
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1367
.L1372:
	addq	$1, %r15
	cmpq	%r15, %r14
	jne	.L1374
.L1373:
	movl	$16, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1369
	movb	$0, (%rbx)
	movq	%r12, %rdi
	call	T_CString_toUpperCase_67@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %r14
	movq	-560(%rbp), %rax
	movq	56(%rax), %r15
	testq	%r15, %r15
	jne	.L1375
	jmp	.L1972
	.p2align 4,,10
	.p2align 3
.L1973:
	movq	%rax, %r15
.L1375:
	movq	(%r15), %rsi
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L1376
	movq	8(%r15), %rax
	testq	%rax, %rax
	jne	.L1973
	movq	%r13, 8(%r15)
	movq	-544(%rbp), %r12
	movq	$0, 8(%r13)
	movl	$176, %r13d
	movq	%rbx, -520(%rbp)
	jmp	.L1360
.L1394:
	movq	%r14, %r15
	testl	%eax, %eax
	jne	.L1400
	movq	%r15, %rbx
	xorl	%r12d, %r12d
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1936:
	movl	-528(%rbp), %ecx
	movl	%ecx, %eax
	testl	%ecx, %ecx
	js	.L1974
.L1379:
	cmpl	$1, %eax
	jne	.L1378
	movsbl	(%r12), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	movzbl	(%r12), %eax
	jne	.L1381
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	ja	.L1378
.L1381:
	movsbl	%al, %edi
	call	uprv_asciitolower_67@PLT
	cmpb	$120, %al
	je	.L1378
	cmpq	$0, -536(%rbp)
	je	.L1628
	cmpq	$0, -568(%rbp)
	je	.L1912
	cmpq	$0, -552(%rbp)
	je	.L1912
	movq	-552(%rbp), %rax
	movq	-568(%rbp), %rdi
	movb	$0, (%rax)
	call	T_CString_toLowerCase_67@PLT
	movq	-536(%rbp), %rsi
	movq	%rax, 8(%rsi)
	movq	-560(%rbp), %rax
	leaq	64(%rax), %rdi
	call	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0
	testb	%al, %al
	je	.L1912
.L1382:
	movl	$24, %edi
	call	uprv_malloc_67@PLT
	movq	%rax, -536(%rbp)
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1369
	movb	$0, (%rbx)
	movq	%r12, %rdi
	movl	$64, %r13d
	call	T_CString_toLowerCase_67@PLT
	movq	$0, 8(%r14)
	movq	-544(%rbp), %r12
	movq	%rax, (%r14)
	movq	-552(%rbp), %rax
	movq	$0, -568(%rbp)
	movq	%rax, -520(%rbp)
	movq	$0, -552(%rbp)
	jmp	.L1360
.L1343:
	movq	%r12, %rdi
	call	strlen@PLT
	movl	%eax, %ecx
	subl	$2, %eax
	jmp	.L1344
.L1964:
	movq	-528(%rbp), %rsi
	leaq	_ZL20LOCALE_ATTRIBUTE_KEY(%rip), %rax
	leaq	-504(%rbp), %rdi
	movq	%rax, (%rsi)
	movq	(%rbx), %rax
	movq	%rax, 8(%rsi)
	call	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0
	testb	%al, %al
	je	.L1975
.L1478:
	movl	-288(%rbp), %eax
	movq	-280(%rbp), %r9
	testl	%eax, %eax
	jle	.L1499
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1496:
	movq	(%r9,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L1498
	call	_ZN6icu_677UMemorydlEPv@PLT
	movl	-288(%rbp), %eax
	addq	$1, %rbx
	movq	-280(%rbp), %r9
	cmpl	%ebx, %eax
	jg	.L1496
.L1499:
	movzbl	-268(%rbp), %eax
	testb	%al, %al
	je	.L1501
	movq	%r9, %rdi
	call	uprv_free_67@PLT
.L1501:
	testq	%r15, %r15
	je	.L1643
	xorl	%r9d, %r9d
	leaq	-195(%rbp), %rax
	movzbl	(%r15), %r14d
	xorl	%esi, %esi
	movl	$0, -528(%rbp)
	xorl	%r10d, %r10d
	xorl	%r13d, %r13d
	movq	%rax, -520(%rbp)
	movb	%r9b, -612(%rbp)
	.p2align 4,,10
	.p2align 3
.L1502:
	testb	%r14b, %r14b
	jne	.L1976
.L1644:
	movq	$0, -536(%rbp)
	xorl	%r12d, %r12d
	movl	$1, %r14d
.L1509:
	movslq	%r13d, %rbx
	movq	-520(%rbp), %rdi
	movl	$3, %ecx
	movq	%r10, -544(%rbp)
	movq	%rbx, %rdx
	call	__strncpy_chk@PLT
	movq	-520(%rbp), %rdi
	movb	$0, -195(%rbp,%rbx)
	call	uloc_toLegacyKey_67@PLT
	movq	-544(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1570
	cmpq	-520(%rbp), %rbx
	je	.L1977
.L1518:
	testq	%r10, %r10
	je	.L1647
	cmpl	$127, -528(%rbp)
	jg	.L1570
	movslq	-528(%rbp), %r13
	leaq	-192(%rbp), %r9
	movq	%r10, %rsi
	movl	$128, %ecx
	movq	%r9, %rdi
	movq	%r13, %rdx
	call	__strncpy_chk@PLT
	movq	%rbx, %rdi
	movb	$0, -192(%rbp,%r13)
	movq	%rax, %rsi
	movq	%rax, -544(%rbp)
	call	uloc_toLegacyType_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1570
	movq	-544(%rbp), %r9
	cmpq	%r9, %rax
	je	.L1978
.L1527:
	cmpb	$0, -600(%rbp)
	jne	.L1539
	cmpb	$118, (%rbx)
	jne	.L1539
	cmpb	$97, 1(%rbx)
	jne	.L1539
	cmpb	$0, 2(%rbx)
	jne	.L1539
	leaq	_ZL11POSIX_VALUE(%rip), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L1539
	movb	$1, -612(%rbp)
	.p2align 4,,10
	.p2align 3
.L1541:
	movq	-536(%rbp), %rsi
	movl	$0, %r13d
	testq	%rsi, %rsi
	cmovne	%r12d, %r13d
	testb	%r14b, %r14b
	jne	.L1905
	movzbl	(%r15), %r14d
	xorl	%r10d, %r10d
	movl	$0, -528(%rbp)
	testb	%r14b, %r14b
	je	.L1644
.L1976:
	cmpb	$45, %r14b
	je	.L1645
	movl	%r15d, %ecx
	leaq	1(%r15), %rax
	negl	%ecx
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1979:
	testb	%dl, %dl
	je	.L1511
.L1512:
	movzbl	(%rax), %edx
	leal	(%rcx,%rax), %r12d
	movq	%rax, %rbx
	addq	$1, %rax
	cmpb	$45, %dl
	jne	.L1979
.L1511:
	cmpl	$2, %r12d
	je	.L1980
.L1510:
	testq	%r10, %r10
	je	.L1646
.L1985:
	movl	-528(%rbp), %eax
	movl	%edx, %r14d
	leal	1(%rax,%r12), %eax
	movl	%eax, -528(%rbp)
.L1516:
	movq	%rbx, %r15
	testb	%r14b, %r14b
	je	.L1644
	movzbl	1(%rbx), %r14d
	leaq	1(%rbx), %r15
	jmp	.L1502
.L1967:
	movq	%r12, %rdi
	call	strlen@PLT
	cmpl	$3, %eax
	jne	.L1349
	jmp	.L1981
.L1941:
	movq	-520(%rbp), %rax
	movq	-544(%rbp), %rdi
	movb	$0, (%rax)
	call	T_CString_toLowerCase_67@PLT
	movq	-560(%rbp), %rcx
	movq	%rax, 72(%rcx)
	jmp	.L1385
.L1968:
	movq	%r12, %rdi
	call	strlen@PLT
	jmp	.L1356
.L1969:
	movq	%r12, %rdi
	call	strlen@PLT
	jmp	.L1364
.L1974:
	movq	%r12, %rdi
	call	strlen@PLT
	jmp	.L1379
.L1487:
	movl	$256, %edi
	movl	$32, %r14d
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1592
.L1609:
	cmpl	%ebx, -368(%rbp)
	cmovle	-368(%rbp), %ebx
	movq	%r13, %rdi
	movq	-376(%rbp), %r12
	cmpl	%ebx, %r14d
	movl	%ebx, %edx
	cmovle	%r14d, %edx
	movq	%r12, %rsi
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1539:
	movl	-464(%rbp), %edx
	cmpl	-480(%rbp), %edx
	jne	.L1542
	cmpl	$8, %edx
	je	.L1543
	leal	(%rdx,%rdx), %r9d
	testl	%r9d, %r9d
	jle	.L1588
	movslq	%r9d, %rdi
	movl	%edx, -544(%rbp)
	salq	$3, %rdi
	movl	%r9d, -528(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1588
	movl	-544(%rbp), %edx
	movq	-472(%rbp), %r10
	movl	-528(%rbp), %r9d
	testl	%edx, %edx
	jg	.L1603
	cmpb	$0, -460(%rbp)
	jne	.L1982
.L1545:
	movq	%rcx, -472(%rbp)
	movl	%r9d, -464(%rbp)
	movb	$1, -460(%rbp)
.L1542:
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1558
	movq	$0, 16(%rax)
	movslq	-480(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm2
	punpcklqdq	%xmm2, %xmm0
	leaq	-504(%rbp), %rdi
	leal	1(%rax), %edx
	movl	%edx, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	%rsi, (%rdx,%rax,8)
	movups	%xmm0, (%rsi)
	call	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1498:
	addq	$1, %rbx
	cmpl	%ebx, %eax
	jg	.L1496
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1647:
	leaq	_ZL15LOCALE_TYPE_YES(%rip), %r13
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1977:
	movq	-520(%rbp), %rdi
	movq	%r10, -544(%rbp)
	call	T_CString_toLowerCase_67@PLT
	movl	-368(%rbp), %edx
	cmpl	-384(%rbp), %edx
	movq	-544(%rbp), %r10
	jne	.L1519
	cmpl	$8, %edx
	je	.L1520
	leal	(%rdx,%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L1588
	movslq	%ecx, %rdi
	movq	%r10, -632(%rbp)
	salq	$3, %rdi
	movl	%edx, -624(%rbp)
	movl	%ecx, -544(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1588
	movl	-624(%rbp), %edx
	movq	-376(%rbp), %r9
	movl	-544(%rbp), %ecx
	movq	-632(%rbp), %r10
	testl	%edx, %edx
	jg	.L1605
.L1522:
	cmpb	$0, -364(%rbp)
	jne	.L1983
.L1523:
	movq	%rbx, -376(%rbp)
	movl	%ecx, -368(%rbp)
	movb	$1, -364(%rbp)
.L1519:
	movl	$64, %edi
	movq	%r10, -544(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1524
	xorl	%esi, %esi
	leaq	13(%rax), %rax
	movl	$0, 56(%rbx)
	movl	%r13d, %edx
	movw	%si, 12(%rbx)
	movq	-576(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movq	-520(%rbp), %rsi
	movl	$40, 8(%rbx)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movslq	-384(%rbp), %rax
	movq	-544(%rbp), %r10
	leal	1(%rax), %edx
	movl	%edx, -384(%rbp)
	movq	-376(%rbp), %rdx
	movq	%rbx, (%rdx,%rax,8)
	movq	-576(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1917
	movq	(%rbx), %rbx
	jmp	.L1518
.L1543:
	movl	$256, %edi
	movl	%edx, -528(%rbp)
	call	uprv_malloc_67@PLT
	movl	-528(%rbp), %edx
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L1588
	movl	$32, %r9d
.L1603:
	cmpl	%edx, -464(%rbp)
	cmovle	-464(%rbp), %edx
	movq	%rcx, %rdi
	movl	%r9d, -544(%rbp)
	movq	-472(%rbp), %r10
	cmpl	%edx, %r9d
	cmovle	%r9d, %edx
	movq	%r10, %rsi
	movq	%r10, -528(%rbp)
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	cmpb	$0, -460(%rbp)
	movl	-544(%rbp), %r9d
	movq	-528(%rbp), %r10
	movq	%rax, %rcx
	je	.L1545
.L1982:
	movq	%r10, %rdi
	movl	%r9d, -544(%rbp)
	movq	%rcx, -528(%rbp)
	call	uprv_free_67@PLT
	movl	-544(%rbp), %r9d
	movq	-528(%rbp), %rcx
	jmp	.L1545
.L1570:
	movq	-576(%rbp), %rax
	movl	$1, (%rax)
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1459:
	cmpq	$0, -544(%rbp)
	je	.L1478
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1978:
	movq	%r9, %rdi
	call	T_CString_toLowerCase_67@PLT
	movl	-368(%rbp), %r13d
	cmpl	-384(%rbp), %r13d
	movq	-544(%rbp), %r9
	jne	.L1531
	cmpl	$8, %r13d
	je	.L1532
	leal	(%r13,%r13), %r10d
	testl	%r10d, %r10d
	jle	.L1588
	movslq	%r10d, %rdi
	movq	%r9, -624(%rbp)
	salq	$3, %rdi
	movl	%r10d, -544(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1588
	testl	%r13d, %r13d
	movq	-376(%rbp), %r11
	movslq	-544(%rbp), %r10
	movq	-624(%rbp), %r9
	jg	.L1604
.L1534:
	cmpb	$0, -364(%rbp)
	jne	.L1984
.L1535:
	movq	%rcx, -376(%rbp)
	movl	%r10d, -368(%rbp)
	movb	$1, -364(%rbp)
.L1531:
	movl	$64, %edi
	movq	%r9, -544(%rbp)
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1524
	leaq	13(%rax), %rax
	xorl	%ecx, %ecx
	movq	-544(%rbp), %r9
	movq	%r13, %rdi
	movw	%cx, 12(%r13)
	movl	-528(%rbp), %edx
	movq	%rax, 0(%r13)
	movq	-576(%rbp), %rcx
	movq	%r9, %rsi
	movl	$0, 56(%r13)
	movl	$40, 8(%r13)
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movslq	-384(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -384(%rbp)
	movq	-376(%rbp), %rdx
	movq	%r13, (%rdx,%rax,8)
	movq	-576(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1917
	movq	0(%r13), %r13
	jmp	.L1527
.L1926:
	leal	1(%rcx), %eax
	cltq
	movq	(%r14,%rax,8), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -528(%rbp)
	call	strlen@PLT
	movq	-528(%rbp), %rsi
	leal	(%rbx,%rax), %r13d
	movq	%rax, %r14
	subl	%r15d, %r13d
	cmpl	%r13d, %ebx
	jl	.L1323
	movq	-560(%rbp), %rax
	movl	%ebx, %r13d
	movq	(%rax), %rdi
.L1324:
	movl	%r15d, %eax
	subl	%r14d, %eax
	cltq
	movq	%rax, -600(%rbp)
	call	strcpy@PLT
	cmpl	%r13d, %r15d
	je	.L1328
	movq	-560(%rbp), %rax
	movslq	%r15d, %rsi
	movslq	%r14d, %rdi
	addq	-536(%rbp), %rsi
	addq	(%rax), %rdi
	call	strcpy@PLT
.L1328:
	testl	%r15d, %r15d
	jne	.L1331
	movl	%r13d, %ebx
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1980:
	movsbl	%r14b, %edi
	movq	%rsi, -544(%rbp)
	movq	%r10, -536(%rbp)
	call	uprv_isASCIILetter_67@PLT
	movq	-536(%rbp), %r10
	movq	-544(%rbp), %rsi
	testb	%al, %al
	jne	.L1513
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1916
.L1513:
	movsbl	1(%r15), %edi
	movq	%rsi, -544(%rbp)
	movq	%r10, -536(%rbp)
	call	uprv_isASCIILetter_67@PLT
	movq	-536(%rbp), %r10
	movq	-544(%rbp), %rsi
	testb	%al, %al
	jne	.L1514
.L1916:
	movzbl	(%rbx), %edx
	testq	%r10, %r10
	jne	.L1985
.L1646:
	movl	%r12d, -528(%rbp)
	movl	%edx, %r14d
	movq	%r15, %r10
	jmp	.L1516
.L1514:
	movzbl	(%rbx), %r14d
	testq	%rsi, %rsi
	je	.L1986
	movq	%r15, -536(%rbp)
	testb	%r14b, %r14b
	je	.L1987
	leaq	1(%rbx), %r15
	xorl	%r14d, %r14d
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1937:
	movl	-528(%rbp), %esi
	movl	%esi, %eax
	testl	%esi, %esi
	jns	.L1387
	movq	%r12, %rdi
	call	strlen@PLT
.L1387:
	leal	-2(%rax), %edx
	cmpl	$6, %edx
	ja	.L1386
	subl	$1, %eax
	movq	%r12, %r15
	leaq	1(%r12,%rax), %r14
	.p2align 4,,10
	.p2align 3
.L1390:
	movsbl	(%r15), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L1388
	movzbl	(%r15), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1386
.L1388:
	addq	$1, %r15
	cmpq	%r15, %r14
	jne	.L1390
	cmpq	$0, -568(%rbp)
	movq	%rbx, -552(%rbp)
	je	.L1988
	movq	-544(%rbp), %r12
	movl	$224, %r13d
	jmp	.L1360
.L1943:
	cmpq	$0, 24(%rax)
	jne	.L1412
	cmpq	$0, 32(%rax)
	jne	.L1412
	movq	8(%rax), %r12
	jmp	.L1412
.L1905:
	movzbl	-612(%rbp), %r9d
.L1454:
	movq	-504(%rbp), %rbx
	movq	-608(%rbp), %r12
	movl	%r9d, %r13d
	testq	%rbx, %rbx
	je	.L1551
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	%rbx, %rsi
	movq	16(%rbx), %rbx
	movq	%r12, %rdi
	call	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0
	testq	%rbx, %rbx
	jne	.L1549
	movl	%r13d, %r9d
.L1551:
	movq	-576(%rbp), %rax
	movl	(%rax), %eax
.L1918:
	movb	%r9b, -600(%rbp)
	jmp	.L1550
.L1986:
	movl	$2, %r13d
	movq	%r15, %rsi
	jmp	.L1516
.L1666:
	movl	%ebx, %eax
	movq	%rdx, %r15
	movl	-528(%rbp), %ebx
	addl	$1, %eax
	cltq
	movq	(%r12,%rax,8), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rax, %r12
	movq	%rax, %rdx
	movq	-560(%rbp), %rax
	movq	(%rax), %rdi
	call	strncpy@PLT
	movq	-520(%rbp), %rax
	cmpb	$45, (%r15)
	leaq	(%rax,%r12), %rdi
	je	.L1989
	movb	$0, (%rdi)
.L1335:
	subq	%r12, %r14
	movq	%r14, -600(%rbp)
	jmp	.L1331
.L1965:
	call	uprv_free_67@PLT
	jmp	.L1578
.L1560:
	movq	-576(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L1517
	movq	-560(%rbp), %rax
	movq	72(%rax), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	testl	%eax, %eax
	jg	.L1990
.L1563:
	testb	$1, -600(%rbp)
	je	.L1601
.L1600:
	movq	-592(%rbp), %rdi
	movl	$6, %edx
	leaq	_ZL6_POSIX(%rip), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-576(%rbp), %rax
	movl	(%rax), %eax
.L1572:
	testl	%eax, %eax
	jg	.L1517
.L1601:
	movq	-512(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1517
	movq	-592(%rbp), %r13
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %r12
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L1991:
	movq	0(%r13), %rax
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	*16(%rax)
.L1573:
	movq	(%rbx), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	0(%r13), %rax
	call	*16(%rax)
	movq	0(%r13), %rax
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*16(%rax)
	movq	8(%rbx), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	0(%r13), %rax
	call	*16(%rax)
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1991
	jmp	.L1517
.L1645:
	movl	$45, %edx
	movq	%r15, %rbx
	xorl	%r12d, %r12d
	jmp	.L1510
.L1323:
	movq	-520(%rbp), %rdi
	movq	%rsi, -528(%rbp)
	call	uprv_free_67@PLT
	leal	1(%r13), %edi
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	-528(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, -520(%rbp)
	je	.L1369
	movq	-560(%rbp), %rcx
	movq	%rax, %rdi
	movq	%rax, (%rcx)
	jmp	.L1324
.L1970:
	movsbl	(%r12), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	je	.L1362
	movsbl	1(%r12), %edi
	call	uprv_isASCIILetter_67@PLT
	testb	%al, %al
	jne	.L1366
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1959:
	movslq	%r13d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1592
	movq	-472(%rbp), %r14
	testl	%ebx, %ebx
	jle	.L1482
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1889:
	movzwl	-624(%rbp), %r13d
	jmp	.L1342
.L1635:
	xorl	%r13d, %r13d
	jmp	.L1423
.L1962:
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L1483
.L1643:
	xorl	%r9d, %r9d
	jmp	.L1454
.L1339:
	testw	%dx, %dx
	jne	.L1992
	testb	$2, %r13b
	jne	.L1347
.L1348:
	movl	$0, -528(%rbp)
	movq	%r12, %rbx
	movq	$0, -544(%rbp)
	testb	$8, %r13b
	je	.L1362
	jmp	.L1620
.L1971:
	movq	%r12, %rdi
	call	strlen@PLT
	jmp	.L1371
.L1992:
	testb	$2, %r13b
	jne	.L1993
.L1347:
	testb	$4, %r13b
	je	.L1348
.L1664:
	movq	$0, -544(%rbp)
	movq	%r12, %rbx
	movl	$0, -528(%rbp)
	jmp	.L1353
.L1988:
	movq	%r12, -568(%rbp)
	movl	$224, %r13d
	movq	-544(%rbp), %r12
	jmp	.L1360
.L1520:
	movl	$256, %edi
	movq	%r10, -624(%rbp)
	movl	%edx, -544(%rbp)
	call	uprv_malloc_67@PLT
	movl	-544(%rbp), %edx
	movq	-624(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1588
	movl	$32, %ecx
.L1605:
	cmpl	%edx, -368(%rbp)
	movq	%rbx, %rdi
	cmovle	-368(%rbp), %edx
	movq	%r10, -632(%rbp)
	movq	-376(%rbp), %r9
	movl	%ecx, -624(%rbp)
	cmpl	%edx, %ecx
	cmovle	%ecx, %edx
	movq	%r9, %rsi
	movq	%r9, -544(%rbp)
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	movq	-632(%rbp), %r10
	movl	-624(%rbp), %ecx
	movq	-544(%rbp), %r9
	jmp	.L1522
.L1890:
	movzwl	-624(%rbp), %r13d
	jmp	.L1349
.L1975:
	movq	-576(%rbp), %rax
	movl	-288(%rbp), %edx
	movq	-280(%rbp), %rdi
	movl	$1, (%rax)
	jmp	.L1470
.L1963:
	movq	%r12, %rdi
	call	uprv_free_67@PLT
	jmp	.L1490
.L1990:
	movl	-464(%rbp), %r12d
	cmpl	-480(%rbp), %r12d
	jne	.L1564
	cmpl	$8, %r12d
	je	.L1565
	leal	(%r12,%r12), %r14d
	testl	%r14d, %r14d
	jle	.L1588
	movslq	%r14d, %rdi
	salq	$3, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1588
	movq	-472(%rbp), %r15
	testl	%r12d, %r12d
	jg	.L1602
.L1567:
	cmpb	$0, -460(%rbp)
	jne	.L1994
.L1568:
	movq	%r13, -472(%rbp)
	movl	%r14d, -464(%rbp)
	movb	$1, -460(%rbp)
.L1564:
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1558
	movq	$0, 16(%rax)
	movslq	-480(%rbp), %rax
	leaq	-512(%rbp), %rdi
	leal	1(%rax), %edx
	movl	%edx, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	%rsi, (%rdx,%rax,8)
	leaq	_ZL14PRIVATEUSE_KEY(%rip), %rax
	movq	%rax, (%rsi)
	movq	%rbx, 8(%rsi)
	call	_ZL19_addExtensionToListPP18ExtensionListEntryS0_a.constprop.0
	testb	%al, %al
	je	.L1570
	movq	-576(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L1517
	testb	$1, -600(%rbp)
	jne	.L1600
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	72(%rax), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L1563
	jmp	.L1564
.L1972:
	movq	$0, 8(%r13)
	movq	-544(%rbp), %r12
	movq	%r13, 56(%rax)
	movl	$176, %r13d
	movq	%rbx, -520(%rbp)
	jmp	.L1360
.L1532:
	movl	$256, %edi
	movq	%r9, -544(%rbp)
	call	uprv_malloc_67@PLT
	movq	-544(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L1588
	movl	$32, %r10d
.L1604:
	cmpl	%r13d, -368(%rbp)
	cmovle	-368(%rbp), %r13d
	movq	%rcx, %rdi
	movq	%r9, -632(%rbp)
	movq	-376(%rbp), %r11
	movl	%r10d, -624(%rbp)
	cmpl	%r10d, %r13d
	movslq	%r13d, %rdx
	cmovg	%r10, %rdx
	movq	%r11, %rsi
	movq	%r11, -544(%rbp)
	salq	$3, %rdx
	call	memcpy@PLT
	movq	-632(%rbp), %r9
	movl	-624(%rbp), %r10d
	movq	-544(%rbp), %r11
	movq	%rax, %rcx
	jmp	.L1534
.L1983:
	movq	%r9, %rdi
	movq	%r10, -624(%rbp)
	movl	%ecx, -544(%rbp)
	call	uprv_free_67@PLT
	movq	-624(%rbp), %r10
	movl	-544(%rbp), %ecx
	jmp	.L1523
.L1989:
	movslq	%ebx, %rdx
	movq	%r15, %rsi
	addq	$1, %rdx
	subq	%r14, %rdx
	call	memmove@PLT
	jmp	.L1335
.L1984:
	movq	%r11, %rdi
	movq	%r9, -632(%rbp)
	movl	%r10d, -624(%rbp)
	movq	%rcx, -544(%rbp)
	call	uprv_free_67@PLT
	movq	-632(%rbp), %r9
	movl	-624(%rbp), %r10d
	movq	-544(%rbp), %rcx
	jmp	.L1535
.L1628:
	movq	-520(%rbp), %rax
	movq	%rax, -552(%rbp)
	jmp	.L1382
.L1565:
	movl	$256, %edi
	movl	$32, %r14d
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1588
.L1602:
	cmpl	%r12d, -464(%rbp)
	cmovle	-464(%rbp), %r12d
	movq	%r13, %rdi
	movq	-472(%rbp), %r15
	cmpl	%r12d, %r14d
	movl	%r12d, %edx
	cmovle	%r14d, %edx
	movq	%r15, %rsi
	movslq	%edx, %rdx
	salq	$3, %rdx
	call	memcpy@PLT
	jmp	.L1567
.L1961:
	jmp	.L1431
.L1917:
	movzbl	-612(%rbp), %r9d
	jmp	.L1918
.L1994:
	movq	%r15, %rdi
	call	uprv_free_67@PLT
	jmp	.L1568
.L1993:
	testb	$4, %r13b
	je	.L1664
	movq	$0, -544(%rbp)
	movq	%r12, %rbx
	movl	$0, -528(%rbp)
	jmp	.L1616
.L1467:
	movslq	-288(%rbp), %rax
	movq	-280(%rbp), %rdi
	leal	1(%rax), %edx
	movl	%edx, -288(%rbp)
	movq	$0, (%rdi,%rax,8)
	jmp	.L1464
.L1558:
	movslq	-480(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L1588
.L1900:
	jmp	.L1431
.L1484:
	movslq	-480(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L1592
.L1942:
	call	__stack_chk_fail@PLT
.L1925:
	movq	-520(%rbp), %rdi
	call	uprv_free_67@PLT
.L1919:
	movq	-576(%rbp), %rax
	movl	$7, (%rax)
	jmp	.L1313
.L1491:
	movslq	-384(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -384(%rbp)
	movq	-376(%rbp), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L1592
.L1524:
	movslq	-384(%rbp), %rax
	leal	1(%rax), %edx
	movl	%edx, -384(%rbp)
	movq	-376(%rbp), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L1588
.L1987:
	movq	%rbx, %r15
	jmp	.L1509
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	ulocimp_forLanguageTag_67.cold, @function
ulocimp_forLanguageTag_67.cold:
.LFSB2432:
.L1561:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movzbl	0, %eax
	ud2
.L1431:
	movq	-592(%rbp), %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	*%rcx
	movzbl	0, %eax
	ud2
	.cfi_endproc
.LFE2432:
	.text
	.size	ulocimp_forLanguageTag_67, .-ulocimp_forLanguageTag_67
	.section	.text.unlikely
	.size	ulocimp_forLanguageTag_67.cold, .-ulocimp_forLanguageTag_67.cold
.LCOLDE8:
	.text
.LHOTE8:
	.p2align 4
	.globl	uloc_toLanguageTag_67
	.type	uloc_toLanguageTag_67, @function
uloc_toLanguageTag_67:
.LFB2429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L2003
.L1995:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2004
	addq	$72, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2003:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	movq	%rdi, -104(%rbp)
	movq	%r8, %r12
	movl	%ecx, %ebx
	movq	%r15, %rdi
	movl	%edx, -108(%rbp)
	movq	%rsi, %r13
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	movq	-104(%rbp), %r10
	movsbl	%bl, %edx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r10, %rdi
	call	ulocimp_toLanguageTag_67
	movl	(%r12), %eax
	movl	-72(%rbp), %r14d
	testl	%eax, %eax
	jg	.L1997
	cmpb	$0, -68(%rbp)
	movl	-108(%rbp), %r9d
	je	.L1998
	movl	$15, (%r12)
.L1997:
	movq	%r15, %rdi
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	%r12, %rcx
	movl	%r14d, %edx
	movl	%r9d, %esi
	movq	%r13, %rdi
	call	u_terminateChars_67@PLT
	jmp	.L1997
.L2004:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2429:
	.size	uloc_toLanguageTag_67, .-uloc_toLanguageTag_67
	.p2align 4
	.globl	uloc_forLanguageTag_67
	.type	uloc_forLanguageTag_67, @function
uloc_forLanguageTag_67:
.LFB2431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L2013
.L2005:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2014
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2013:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	movq	%rdi, %rbx
	movq	%r8, %r12
	movq	%rcx, -104(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	movl	%edx, %r13d
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	movq	-104(%rbp), %rcx
	movq	%r12, %r8
	movq	%r14, %rdx
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	ulocimp_forLanguageTag_67
	movl	(%r12), %eax
	movl	-72(%rbp), %r15d
	testl	%eax, %eax
	jg	.L2007
	cmpb	$0, -68(%rbp)
	movq	-112(%rbp), %r9
	je	.L2008
	movl	$15, (%r12)
.L2007:
	movq	%r14, %rdi
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	jmp	.L2005
	.p2align 4,,10
	.p2align 3
.L2008:
	movq	%r12, %rcx
	movl	%r15d, %edx
	movl	%r13d, %esi
	movq	%r9, %rdi
	call	u_terminateChars_67@PLT
	jmp	.L2007
.L2014:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2431:
	.size	uloc_forLanguageTag_67, .-uloc_forLanguageTag_67
	.section	.rodata
	.align 32
	.type	_ZL17DEPRECATEDREGIONS, @object
	.size	_ZL17DEPRECATEDREGIONS, 36
_ZL17DEPRECATEDREGIONS:
	.string	"BU"
	.string	"MM"
	.string	"DD"
	.string	"DE"
	.string	"FX"
	.string	"FR"
	.string	"TP"
	.string	"TL"
	.string	"YD"
	.string	"YE"
	.string	"ZR"
	.string	"CD"
	.align 32
	.type	_ZL15DEPRECATEDLANGS, @object
	.size	_ZL15DEPRECATEDLANGS, 624
_ZL15DEPRECATEDLANGS:
	.string	"in"
	.zero	1
	.string	"id"
	.zero	1
	.string	"iw"
	.zero	1
	.string	"he"
	.zero	1
	.string	"ji"
	.zero	1
	.string	"yi"
	.zero	1
	.string	"jw"
	.zero	1
	.string	"jv"
	.zero	1
	.string	"mo"
	.zero	1
	.string	"ro"
	.zero	1
	.string	"aam"
	.string	"aas"
	.string	"adp"
	.string	"dz"
	.zero	1
	.string	"aue"
	.string	"ktz"
	.string	"ayx"
	.string	"nun"
	.string	"bgm"
	.string	"bcg"
	.string	"bjd"
	.string	"drl"
	.string	"ccq"
	.string	"rki"
	.string	"cjr"
	.string	"mom"
	.string	"cka"
	.string	"cmr"
	.string	"cmk"
	.string	"xch"
	.string	"coy"
	.string	"pij"
	.string	"cqu"
	.string	"quh"
	.string	"drh"
	.string	"khk"
	.string	"drw"
	.string	"prs"
	.string	"gav"
	.string	"dev"
	.string	"gfx"
	.string	"vaj"
	.string	"ggn"
	.string	"gvr"
	.string	"gti"
	.string	"nyc"
	.string	"guv"
	.string	"duz"
	.string	"hrr"
	.string	"jal"
	.string	"ibi"
	.string	"opa"
	.string	"ilw"
	.string	"gal"
	.string	"jeg"
	.string	"oyb"
	.string	"kgc"
	.string	"tdf"
	.string	"kgh"
	.string	"kml"
	.string	"koj"
	.string	"kwv"
	.string	"krm"
	.string	"bmf"
	.string	"ktr"
	.string	"dtp"
	.string	"kvs"
	.string	"gdj"
	.string	"kwq"
	.string	"yam"
	.string	"kxe"
	.string	"tvd"
	.string	"kzj"
	.string	"dtp"
	.string	"kzt"
	.string	"dtp"
	.string	"lii"
	.string	"raq"
	.string	"lmm"
	.string	"rmx"
	.string	"meg"
	.string	"cir"
	.string	"mst"
	.string	"mry"
	.string	"mwj"
	.string	"vaj"
	.string	"myt"
	.string	"mry"
	.string	"nad"
	.string	"xny"
	.string	"ncp"
	.string	"kdz"
	.string	"nnx"
	.string	"ngv"
	.string	"nts"
	.string	"pij"
	.string	"oun"
	.string	"vaj"
	.string	"pcr"
	.string	"adx"
	.string	"pmc"
	.string	"huw"
	.string	"pmu"
	.string	"phr"
	.string	"ppa"
	.string	"bfy"
	.string	"ppr"
	.string	"lcq"
	.string	"pry"
	.string	"prt"
	.string	"puz"
	.string	"pub"
	.string	"sca"
	.string	"hle"
	.string	"skk"
	.string	"oyb"
	.string	"tdu"
	.string	"dtp"
	.string	"thc"
	.string	"tpo"
	.string	"thx"
	.string	"oyb"
	.string	"tie"
	.string	"ras"
	.string	"tkk"
	.string	"twm"
	.string	"tlw"
	.string	"weo"
	.string	"tmp"
	.string	"tyj"
	.string	"tne"
	.string	"kak"
	.string	"tnf"
	.string	"prs"
	.string	"tsf"
	.string	"taj"
	.string	"uok"
	.string	"ema"
	.string	"xba"
	.string	"cax"
	.string	"xia"
	.string	"acn"
	.string	"xkh"
	.string	"waw"
	.string	"xsj"
	.string	"suj"
	.string	"ybd"
	.string	"rki"
	.string	"yma"
	.string	"lrr"
	.string	"ymt"
	.string	"mtm"
	.string	"yos"
	.string	"zom"
	.string	"yuu"
	.string	"yug"
	.section	.rodata.str1.1
.LC9:
	.string	"sgn-br"
.LC10:
	.string	"bzs"
.LC11:
	.string	"sgn-co"
.LC12:
	.string	"csn"
.LC13:
	.string	"sgn-de"
.LC14:
	.string	"gsg"
.LC15:
	.string	"sgn-dk"
.LC16:
	.string	"dsl"
.LC17:
	.string	"sgn-es"
.LC18:
	.string	"ssp"
.LC19:
	.string	"sgn-fr"
.LC20:
	.string	"fsl"
.LC21:
	.string	"sgn-gb"
.LC22:
	.string	"bfi"
.LC23:
	.string	"sgn-gr"
.LC24:
	.string	"gss"
.LC25:
	.string	"sgn-ie"
.LC26:
	.string	"isg"
.LC27:
	.string	"sgn-it"
.LC28:
	.string	"ise"
.LC29:
	.string	"sgn-jp"
.LC30:
	.string	"jsl"
.LC31:
	.string	"sgn-mx"
.LC32:
	.string	"mfs"
.LC33:
	.string	"sgn-ni"
.LC34:
	.string	"ncs"
.LC35:
	.string	"sgn-nl"
.LC36:
	.string	"dse"
.LC37:
	.string	"sgn-no"
.LC38:
	.string	"nsl"
.LC39:
	.string	"sgn-pt"
.LC40:
	.string	"psr"
.LC41:
	.string	"sgn-se"
.LC42:
	.string	"swl"
.LC43:
	.string	"sgn-us"
.LC44:
	.string	"ase"
.LC45:
	.string	"sgn-za"
.LC46:
	.string	"sfs"
.LC47:
	.string	"zh-cmn"
.LC48:
	.string	"cmn"
.LC49:
	.string	"zh-cmn-hans"
.LC50:
	.string	"cmn-hans"
.LC51:
	.string	"zh-cmn-hant"
.LC52:
	.string	"cmn-hant"
.LC53:
	.string	"zh-gan"
.LC54:
	.string	"gan"
.LC55:
	.string	"zh-wuu"
.LC56:
	.string	"wuu"
.LC57:
	.string	"zh-yue"
.LC58:
	.string	"yue"
.LC59:
	.string	"ja-latn-hepburn-heploc"
.LC60:
	.string	"ja-latn-alalc97"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL9REDUNDANT, @object
	.size	_ZL9REDUNDANT, 416
_ZL9REDUNDANT:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.section	.rodata.str1.1
.LC61:
	.string	"art-lojban"
.LC62:
	.string	"jbo"
.LC63:
	.string	"en-gb-oed"
.LC64:
	.string	"en-gb-oxendict"
.LC65:
	.string	"i-ami"
.LC66:
	.string	"ami"
.LC67:
	.string	"i-bnn"
.LC68:
	.string	"bnn"
.LC69:
	.string	"i-hak"
.LC70:
	.string	"hak"
.LC71:
	.string	"i-klingon"
.LC72:
	.string	"tlh"
.LC73:
	.string	"i-lux"
.LC74:
	.string	"lb"
.LC75:
	.string	"i-navajo"
.LC76:
	.string	"nv"
.LC77:
	.string	"i-pwn"
.LC78:
	.string	"pwn"
.LC79:
	.string	"i-tao"
.LC80:
	.string	"tao"
.LC81:
	.string	"i-tay"
.LC82:
	.string	"tay"
.LC83:
	.string	"i-tsu"
.LC84:
	.string	"tsu"
.LC85:
	.string	"no-bok"
.LC86:
	.string	"nb"
.LC87:
	.string	"no-nyn"
.LC88:
	.string	"nn"
.LC89:
	.string	"sgn-be-fr"
.LC90:
	.string	"sfb"
.LC91:
	.string	"sgn-be-nl"
.LC92:
	.string	"vgt"
.LC93:
	.string	"sgn-ch-de"
.LC94:
	.string	"sgg"
.LC95:
	.string	"zh-guoyu"
.LC96:
	.string	"zh-hakka"
.LC97:
	.string	"zh-min-nan"
.LC98:
	.string	"nan"
.LC99:
	.string	"zh-xiang"
.LC100:
	.string	"hsn"
.LC101:
	.string	"cel-gaulish"
.LC102:
	.string	"xtg-x-cel-gaulish"
.LC103:
	.string	"i-default"
.LC104:
	.string	"en-x-i-default"
.LC105:
	.string	"i-enochian"
.LC106:
	.string	"und-x-i-enochian"
.LC107:
	.string	"i-mingo"
.LC108:
	.string	"see-x-i-mingo"
.LC109:
	.string	"zh-min"
.LC110:
	.string	"nan-x-zh-min"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL13GRANDFATHERED, @object
	.size	_ZL13GRANDFATHERED, 416
_ZL13GRANDFATHERED:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC48
	.quad	.LC96
	.quad	.LC70
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.section	.rodata
	.type	_ZL15LOCALE_TYPE_YES, @object
	.size	_ZL15LOCALE_TYPE_YES, 4
_ZL15LOCALE_TYPE_YES:
	.string	"yes"
	.align 8
	.type	_ZL22PRIVUSE_VARIANT_PREFIX, @object
	.size	_ZL22PRIVUSE_VARIANT_PREFIX, 9
_ZL22PRIVUSE_VARIANT_PREFIX:
	.string	"lvariant"
	.align 8
	.type	_ZL20LOCALE_ATTRIBUTE_KEY, @object
	.size	_ZL20LOCALE_ATTRIBUTE_KEY, 10
_ZL20LOCALE_ATTRIBUTE_KEY:
	.string	"attribute"
	.type	_ZL11POSIX_VALUE, @object
	.size	_ZL11POSIX_VALUE, 6
_ZL11POSIX_VALUE:
	.string	"posix"
	.type	_ZL9POSIX_KEY, @object
	.size	_ZL9POSIX_KEY, 3
_ZL9POSIX_KEY:
	.string	"va"
	.type	_ZL6_POSIX, @object
	.size	_ZL6_POSIX, 7
_ZL6_POSIX:
	.string	"_POSIX"
	.type	_ZL14PRIVATEUSE_KEY, @object
	.size	_ZL14PRIVATEUSE_KEY, 2
_ZL14PRIVATEUSE_KEY:
	.string	"x"
	.type	_ZL8LANG_UND, @object
	.size	_ZL8LANG_UND, 4
_ZL8LANG_UND:
	.string	"und"
	.type	_ZL5EMPTY, @object
	.size	_ZL5EMPTY, 1
_ZL5EMPTY:
	.zero	1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
