	.file	"normalizer2impl.cpp"
	.text
	.p2align 4
	.type	segmentStarterMapper, @function
segmentStarterMapper:
.LFB3224:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$-2147483648, %eax
	ret
	.cfi_endproc
.LFE3224:
	.size	segmentStarterMapper, .-segmentStarterMapper
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_, @function
_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_:
.LFB3199:
	.cfi_startproc
	subq	%rdi, %rsi
	movzbl	(%rdi), %edx
	cmpq	$3, %rsi
	je	.L4
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jg	.L5
	cmpq	$1, %rsi
	jne	.L15
	movzbl	%dl, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	cmpq	$2, %rsi
	jne	.L16
	movl	%edx, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sall	$6, %eax
	andl	$1984, %eax
	movl	%eax, %edx
	movzbl	1(%rdi), %eax
	andl	$63, %eax
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	cmpq	$4, %rsi
	jne	.L17
	movzbl	1(%rdi), %eax
	sall	$18, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	andl	$1835008, %edx
	sall	$12, %eax
	andl	$258048, %eax
	orl	%edx, %eax
	movzbl	3(%rdi), %edx
	andl	$63, %edx
	orl	%edx, %eax
	movzbl	2(%rdi), %edx
	sall	$6, %edx
	andl	$4032, %edx
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore 6
	movzbl	2(%rdi), %eax
	movl	%eax, %ecx
	movl	%edx, %eax
	movzbl	1(%rdi), %edx
	andl	$63, %ecx
	sall	$12, %eax
	sall	$6, %edx
	orl	%ecx, %eax
	andw	$4032, %dx
	orl	%edx, %eax
	movzwl	%ax, %eax
	ret
.L17:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	jmp	.L8
.L16:
	jmp	.L8
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_.cold, @function
_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_.cold:
.LFSB3199:
.L8:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	call	abort@PLT
	.cfi_endproc
.LFE3199:
	.text
	.size	_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_, .-_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_
	.section	.text.unlikely
	.size	_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_.cold, .-_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBufferC2ERKNS_15Normalizer2ImplERNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6716ReorderingBufferC2ERKNS_15Normalizer2ImplERNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6716ReorderingBufferC2ERKNS_15Normalizer2ImplERNS_13UnicodeStringER10UErrorCode:
.LFB3204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	movl	$8, %esi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	movups	%xmm0, (%rbx)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	8(%rbx), %rcx
	movl	$27, %edx
	movq	%rax, %xmm0
	movq	%rax, 32(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	testb	$2, 8(%rcx)
	jne	.L19
	movl	16(%rcx), %edx
.L19:
	movl	%edx, 40(%rbx)
	movb	$0, 44(%rbx)
	testq	%rax, %rax
	je	.L24
.L18:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L18
	popq	%rbx
	movl	$7, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3204:
	.size	_ZN6icu_6716ReorderingBufferC2ERKNS_15Normalizer2ImplERNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6716ReorderingBufferC2ERKNS_15Normalizer2ImplERNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6716ReorderingBufferC1ERKNS_15Normalizer2ImplERNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6716ReorderingBufferC1ERKNS_15Normalizer2ImplERNS_13UnicodeStringER10UErrorCode,_ZN6icu_6716ReorderingBufferC2ERKNS_15Normalizer2ImplERNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode
	.type	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode, @function
_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode:
.LFB3206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	movswl	8(%rdi), %r12d
	testw	%r12w, %r12w
	js	.L26
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	sarl	$5, %r12d
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L64
.L28:
	movslq	%r12d, %rdx
	movq	8(%rbx), %rsi
	movl	$27, %ecx
	leaq	(%rax,%rdx,2), %rdx
	movq	%rdx, 32(%rbx)
	testb	$2, 8(%rsi)
	jne	.L30
	movl	16(%rsi), %ecx
.L30:
	subl	%r12d, %ecx
	movq	%rax, 24(%rbx)
	movl	%ecx, 40(%rbx)
	cmpq	%rdx, %rax
	je	.L65
	movq	%rdx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	jnb	.L63
	movzwl	-2(%rdx), %esi
	leaq	-2(%rdx), %rcx
	movq	%rcx, 48(%rbx)
	movl	%esi, %edi
	andl	$-1024, %edi
	cmpl	$56320, %edi
	jne	.L34
	cmpq	%rcx, %rax
	jb	.L66
.L34:
	movq	(%rbx), %rax
	movzwl	10(%rax), %ecx
	cmpl	%esi, %ecx
	jle	.L35
.L63:
	movb	$0, 44(%rbx)
.L33:
	movq	%rdx, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movb	$0, 44(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	12(%rdi), %r12d
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	jne	.L28
.L64:
	movl	$7, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	movl	%esi, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L63
	movq	32(%rax), %rdi
	movq	8(%rdi), %r12
	cmpl	$65535, %esi
	jle	.L67
	cmpl	$1114111, %esi
	jg	.L39
	cmpl	24(%rdi), %esi
	jl	.L40
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	.p2align 4,,10
	.p2align 3
.L38:
	movzwl	(%r12,%rax), %eax
	cmpw	$-1025, %ax
	jbe	.L68
	sarl	%eax
	movb	%al, 44(%rbx)
	cmpb	$1, %al
	jbe	.L42
.L49:
	movq	48(%rbx), %rdx
	movq	%rdx, 56(%rbx)
	cmpq	24(%rbx), %rdx
	jbe	.L33
	movzwl	-2(%rdx), %esi
	leaq	-2(%rdx), %rax
	movq	%rax, 48(%rbx)
	movl	%esi, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L69
.L43:
	movq	(%rbx), %rax
	movzwl	10(%rax), %ecx
	cmpl	%esi, %ecx
	jg	.L33
	movl	%esi, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L33
	movq	32(%rax), %rdi
	movq	8(%rdi), %r12
	cmpl	$65535, %esi
	jg	.L44
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
.L45:
	movzwl	(%r12,%rax), %eax
	cmpw	$-1025, %ax
	jbe	.L42
	sarl	%eax
	cmpb	$1, %al
	ja	.L49
	.p2align 4,,10
	.p2align 3
.L42:
	movq	56(%rbx), %rdx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L66:
	movzwl	-4(%rdx), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L34
	leaq	-4(%rdx), %rcx
	sall	$10, %eax
	movq	%rcx, 48(%rbx)
	leal	-56613888(%rsi,%rax), %esi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L67:
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L69:
	cmpq	16(%rbx), %rax
	jbe	.L43
	movzwl	-4(%rdx), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L43
	leaq	-4(%rdx), %rcx
	sall	$10, %eax
	movq	%rcx, 48(%rbx)
	leal	-56613888(%rsi,%rax), %esi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	cmpl	$1114111, %esi
	jg	.L46
	cmpl	24(%rdi), %esi
	jl	.L47
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L47:
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L45
.L39:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L38
.L68:
	movb	$0, 44(%rbx)
	movq	56(%rbx), %rdx
	jmp	.L33
.L40:
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L38
	.cfi_endproc
.LFE3206:
	.size	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode, .-_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716ReorderingBuffer6equalsEPKDsS2_
	.type	_ZNK6icu_6716ReorderingBuffer6equalsEPKDsS2_, @function
_ZNK6icu_6716ReorderingBuffer6equalsEPKDsS2_:
.LFB3207:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	movq	16(%rdi), %r9
	movq	32(%rdi), %rdx
	subq	%rsi, %rax
	subq	%r9, %rdx
	sarq	%rax
	sarq	%rdx
	cmpl	%eax, %edx
	je	.L78
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	u_memcmp_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3207:
	.size	_ZNK6icu_6716ReorderingBuffer6equalsEPKDsS2_, .-_ZNK6icu_6716ReorderingBuffer6equalsEPKDsS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6716ReorderingBuffer6equalsEPKhS2_
	.type	_ZNK6icu_6716ReorderingBuffer6equalsEPKhS2_, @function
_ZNK6icu_6716ReorderingBuffer6equalsEPKhS2_:
.LFB3208:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r11
	movq	32(%rdi), %rax
	subq	%rsi, %rdx
	xorl	%r8d, %r8d
	subq	%r11, %rax
	sarq	%rax
	cmpl	%edx, %eax
	jg	.L94
	movslq	%edx, %rcx
	movl	%edx, %edi
	imulq	$1431655766, %rcx, %rcx
	sarl	$31, %edi
	shrq	$32, %rcx
	subl	%edi, %ecx
	cmpl	%eax, %ecx
	jg	.L94
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%eax, %ebx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L91:
	movl	%r9d, %ecx
.L83:
	cmpl	%eax, %r8d
	jne	.L89
.L86:
	cmpl	%ebx, %edi
	jge	.L97
	cmpl	%ecx, %r12d
	jle	.L89
	movslq	%edi, %rax
	leal	1(%rdi), %r9d
	movzwl	(%r11,%rax,2), %r8d
	leaq	(%rax,%rax), %r10
	movl	%r8d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L98
	movl	%r9d, %edi
.L82:
	movslq	%ecx, %rax
	leal	1(%rcx), %r9d
	movzbl	(%rsi,%rax), %eax
	testb	%al, %al
	jns	.L91
	movslq	%r9d, %r9
	movzbl	(%rsi,%r9), %r10d
	cmpl	$223, %eax
	jg	.L84
	sall	$6, %eax
	andl	$63, %r10d
	addl	$2, %ecx
	andl	$1984, %eax
	orl	%r10d, %eax
	cmpl	%eax, %r8d
	je	.L86
.L89:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movzwl	2(%r11,%r10), %eax
	sall	$10, %r8d
	addl	$2, %edi
	leal	-56613888(%rax,%r8), %r8d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L84:
	movzbl	1(%rsi,%r9), %r13d
	cmpl	$239, %eax
	jg	.L85
	sall	$12, %eax
	andl	$63, %r13d
	sall	$6, %r10d
	addl	$3, %ecx
	orl	%r13d, %eax
	andw	$4032, %r10w
	orl	%eax, %r10d
	movzwl	%r10w, %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L85:
	movzbl	2(%rsi,%r9), %r9d
	sall	$12, %r10d
	sall	$18, %eax
	addl	$4, %ecx
	andl	$258048, %r10d
	andl	$1835008, %eax
	sall	$6, %r13d
	andl	$63, %r9d
	andl	$4032, %r13d
	orl	%r9d, %r10d
	orl	%r10d, %eax
	orl	%r13d, %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L97:
	cmpl	%ecx, %edx
	popq	%rbx
	popq	%r12
	setle	%r8b
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE3208:
	.size	_ZNK6icu_6716ReorderingBuffer6equalsEPKhS2_, .-_ZNK6icu_6716ReorderingBuffer6equalsEPKhS2_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer12appendZeroCCEiR10UErrorCode
	.type	_ZN6icu_6716ReorderingBuffer12appendZeroCCEiR10UErrorCode, @function
_ZN6icu_6716ReorderingBuffer12appendZeroCCEiR10UErrorCode:
.LFB3211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$65535, %esi
	movl	40(%rdi), %eax
	movq	%rdx, -56(%rbp)
	seta	%r13b
	movq	32(%rdi), %rdx
	addl	$1, %r13d
	cmpl	%r13d, %eax
	jl	.L112
	subl	%r13d, %eax
	movl	%eax, 40(%rbx)
	cmpl	$1, %r13d
	je	.L113
.L106:
	movl	%r12d, %eax
	andw	$1023, %r12w
	sarl	$10, %eax
	orw	$-9216, %r12w
	subw	$10304, %ax
	movw	%r12w, 2(%rdx)
	movw	%ax, (%rdx)
	leaq	4(%rdx), %rax
	movq	%rax, 32(%rbx)
.L107:
	movb	$0, 44(%rbx)
	movq	%rax, 24(%rbx)
	movl	$1, %eax
.L99:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	24(%rdi), %r15
	movq	8(%rdi), %rdi
	subq	%rax, %rdx
	subq	%rax, %r15
	sarq	%rdx
	sarq	%r15
	movl	%edx, %esi
	movq	%rdx, %r14
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	8(%rbx), %rdi
	leal	0(%r13,%r14), %eax
	movl	$54, %esi
	testb	$2, 8(%rdi)
	jne	.L102
	movl	16(%rdi), %ecx
	leal	(%rcx,%rcx), %esi
.L102:
	cmpl	$256, %eax
	movl	$256, %edx
	cmovl	%edx, %eax
	cmpl	%eax, %esi
	cmovl	%eax, %esi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L114
	movslq	%r14d, %rdx
	movslq	%r15d, %r15
	movq	8(%rbx), %rsi
	leaq	(%rax,%rdx,2), %rdx
	leaq	(%rax,%r15,2), %rax
	movq	%rax, %xmm0
	movq	%rdx, %xmm1
	movl	$27, %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	testb	$2, 8(%rsi)
	jne	.L105
	movl	16(%rsi), %eax
.L105:
	subl	%r14d, %eax
	subl	%r13d, %eax
	movl	%eax, 40(%rbx)
	cmpl	$1, %r13d
	jne	.L106
.L113:
	leaq	2(%rdx), %rax
	movq	%rax, 32(%rbx)
	movw	%r12w, (%rdx)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L114:
	movq	-56(%rbp), %rax
	movl	$7, (%rax)
	xorl	%eax, %eax
	jmp	.L99
	.cfi_endproc
.LFE3211:
	.size	_ZN6icu_6716ReorderingBuffer12appendZeroCCEiR10UErrorCode, .-_ZN6icu_6716ReorderingBuffer12appendZeroCCEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	.type	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode, @function
_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode:
.LFB3212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	cmpq	%rdx, %rsi
	je	.L121
	movq	%rdx, %r12
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	32(%rdi), %rdi
	subq	%rsi, %r12
	sarq	%r12
	cmpl	%r12d, 40(%rbx)
	jge	.L117
	movq	16(%rbx), %rax
	movq	24(%rbx), %r15
	subq	%rax, %rdi
	subq	%rax, %r15
	sarq	%rdi
	sarq	%r15
	movq	%rdi, %r13
	movq	8(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	8(%rbx), %rdi
	leal	0(%r13,%r12), %eax
	movl	$54, %esi
	testb	$2, 8(%rdi)
	jne	.L118
	movl	16(%rdi), %edx
	leal	(%rdx,%rdx), %esi
.L118:
	cmpl	$256, %eax
	movl	$256, %ecx
	cmovl	%ecx, %eax
	cmpl	%eax, %esi
	cmovl	%eax, %esi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L125
	movslq	%r13d, %rcx
	movslq	%r15d, %r15
	leaq	(%rax,%rcx,2), %rdi
	leaq	(%rax,%r15,2), %rax
	movq	8(%rbx), %rcx
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	movl	$27, %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	testb	$2, 8(%rcx)
	jne	.L120
	movl	16(%rcx), %eax
.L120:
	subl	%r13d, %eax
	movl	%eax, 40(%rbx)
.L117:
	movl	%r12d, %edx
	movq	%r14, %rsi
	call	u_memcpy_67@PLT
	movq	32(%rbx), %rax
	movslq	%r12d, %rdx
	subl	%r12d, 40(%rbx)
	movb	$0, 44(%rbx)
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, %xmm0
	movl	$1, %eax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
.L115:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	movl	$7, (%rax)
	xorl	%eax, %eax
	jmp	.L115
	.cfi_endproc
.LFE3212:
	.size	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode, .-_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer6removeEv
	.type	_ZN6icu_6716ReorderingBuffer6removeEv, @function
_ZN6icu_6716ReorderingBuffer6removeEv:
.LFB3213:
	.cfi_startproc
	endbr64
	movdqu	8(%rdi), %xmm0
	movq	8(%rdi), %rdx
	movl	$27, %eax
	punpckhqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%rdi)
	testb	$2, 8(%rdx)
	jne	.L127
	movl	16(%rdx), %eax
.L127:
	movl	%eax, 40(%rdi)
	movb	$0, 44(%rdi)
	ret
	.cfi_endproc
.LFE3213:
	.size	_ZN6icu_6716ReorderingBuffer6removeEv, .-_ZN6icu_6716ReorderingBuffer6removeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer12removeSuffixEi
	.type	_ZN6icu_6716ReorderingBuffer12removeSuffixEi, @function
_ZN6icu_6716ReorderingBuffer12removeSuffixEi:
.LFB3214:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	movq	16(%rdi), %rax
	movslq	%esi, %rcx
	movq	%r8, %rdx
	subq	%rax, %rdx
	sarq	%rdx
	cmpq	%rdx, %rcx
	jge	.L130
	addq	%rcx, %rcx
	movq	%r8, %rax
	addl	%esi, 40(%rdi)
	subq	%rcx, %rax
	movb	$0, 44(%rdi)
	movq	%rax, 32(%rdi)
	movq	%rax, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	movq	8(%rdi), %rcx
	movq	%rax, 32(%rdi)
	movl	$27, %edx
	testb	$2, 8(%rcx)
	jne	.L132
	movl	16(%rcx), %edx
.L132:
	movl	%edx, 40(%rdi)
	movb	$0, 44(%rdi)
	movq	%rax, 24(%rdi)
	ret
	.cfi_endproc
.LFE3214:
	.size	_ZN6icu_6716ReorderingBuffer12removeSuffixEi, .-_ZN6icu_6716ReorderingBuffer12removeSuffixEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	.type	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode, @function
_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode:
.LFB3215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	32(%rdi), %r12
	movq	24(%rdi), %r13
	movq	8(%rdi), %rdi
	subq	%rax, %r12
	sarq	%r12
	subq	%rax, %r13
	movl	%r12d, %esi
	sarq	%r13
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	8(%rbx), %rdi
	leal	(%r15,%r12), %esi
	movl	$54, %eax
	testb	$2, 8(%rdi)
	jne	.L135
	movl	16(%rdi), %eax
	addl	%eax, %eax
.L135:
	cmpl	$256, %esi
	movl	$256, %edx
	cmovl	%edx, %esi
	cmpl	%esi, %eax
	cmovge	%eax, %esi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L143
	movslq	%r12d, %rdx
	movslq	%r13d, %r13
	leaq	(%rax,%r13,2), %rcx
	leaq	(%rax,%rdx,2), %rax
	movq	8(%rbx), %rdx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movl	$27, %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	testb	$2, 8(%rdx)
	jne	.L138
	movl	16(%rdx), %eax
.L138:
	subl	%r12d, %eax
	movl	%eax, 40(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movl	$7, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3215:
	.size	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode, .-_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer12skipPreviousEv
	.type	_ZN6icu_6716ReorderingBuffer12skipPreviousEv, @function
_ZN6icu_6716ReorderingBuffer12skipPreviousEv:
.LFB3216:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movzwl	-2(%rax), %edx
	leaq	-2(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	andl	$-1024, %edx
	movups	%xmm0, 48(%rdi)
	cmpl	$56320, %edx
	je	.L146
.L144:
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	cmpq	16(%rdi), %rcx
	jbe	.L144
	movzwl	-4(%rax), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L144
	subq	$4, %rax
	movq	%rax, 48(%rdi)
	ret
	.cfi_endproc
.LFE3216:
	.size	_ZN6icu_6716ReorderingBuffer12skipPreviousEv, .-_ZN6icu_6716ReorderingBuffer12skipPreviousEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer10previousCCEv
	.type	_ZN6icu_6716ReorderingBuffer10previousCCEv, @function
_ZN6icu_6716ReorderingBuffer10previousCCEv:
.LFB3217:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	movq	%rax, 56(%rdi)
	cmpq	24(%rdi), %rax
	jbe	.L160
	movzwl	-2(%rax), %esi
	leaq	-2(%rax), %rdx
	movq	%rdx, 48(%rdi)
	movl	%esi, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L163
.L150:
	movq	(%rdi), %rax
	movzwl	10(%rax), %edx
	cmpl	%edx, %esi
	jge	.L164
.L160:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	movl	%esi, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L160
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	32(%rax), %rdi
	movq	8(%rdi), %rbx
	cmpl	$65535, %esi
	jg	.L152
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
.L153:
	movzwl	(%rbx,%rax), %eax
	cmpw	$-1025, %ax
	jbe	.L151
	addq	$8, %rsp
	sarl	%eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore 3
	.cfi_restore 6
	cmpq	16(%rdi), %rdx
	jbe	.L150
	movzwl	-4(%rax), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L150
	subq	$4, %rax
	sall	$10, %edx
	movq	%rax, 48(%rdi)
	leal	-56613888(%rsi,%rdx), %esi
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	cmpl	$1114111, %esi
	jg	.L154
	cmpl	24(%rdi), %esi
	jl	.L155
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L151:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L155:
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L153
	.cfi_endproc
.LFE3217:
	.size	_ZN6icu_6716ReorderingBuffer10previousCCEv, .-_ZN6icu_6716ReorderingBuffer10previousCCEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer6insertEih
	.type	_ZN6icu_6716ReorderingBuffer6insertEih, @function
_ZN6icu_6716ReorderingBuffer6insertEih:
.LFB3218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rcx
	movzwl	-2(%rcx), %edx
	leaq	-2(%rcx), %rax
	movq	%rax, 48(%rdi)
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L187
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%rax, 56(%rbx)
	cmpq	%rax, 24(%rbx)
	jnb	.L167
	movzwl	-2(%rax), %esi
	leaq	-2(%rax), %rdx
	movq	%rdx, 48(%rbx)
	movl	%esi, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L188
.L168:
	movq	(%rbx), %rdx
	movzwl	10(%rdx), %ecx
	cmpl	%esi, %ecx
	jle	.L189
.L167:
	cmpl	$65536, %r12d
	movq	32(%rbx), %rdx
	sbbq	%rcx, %rcx
	andq	$-2, %rcx
	leaq	4(%rdx,%rcx), %r8
	movq	%rdx, %rcx
	movq	%r8, 32(%rbx)
	movq	%r8, %rsi
	.p2align 4,,10
	.p2align 3
.L177:
	movzwl	-2(%rcx), %edi
	subq	$2, %rcx
	subq	$2, %rsi
	movw	%di, (%rsi)
	cmpq	%rax, %rcx
	jne	.L177
	leaq	-2(%rdx), %rax
	subq	%rcx, %rax
	movl	%r12d, %ecx
	notq	%rax
	andq	$-2, %rax
	addq	%rax, %rdx
	addq	%r8, %rax
	cmpl	$65535, %r12d
	jle	.L179
	movl	%r12d, %ecx
	andw	$1023, %r12w
	orw	$-9216, %r12w
	sarl	$10, %ecx
	movw	%r12w, 2(%rdx)
	subw	$10304, %cx
.L179:
	movw	%cx, (%rdx)
	cmpb	$1, %r13b
	ja	.L165
	movq	%rax, 24(%rbx)
.L165:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movl	%esi, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L167
	movq	32(%rdx), %rdi
	movq	8(%rdi), %r15
	cmpl	$65535, %esi
	jg	.L169
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
.L170:
	movzwl	(%r15,%rax), %eax
	cmpw	$-1025, %ax
	jbe	.L174
.L173:
	sarl	%eax
	cmpb	%al, %r14b
	jnb	.L174
	movq	48(%rbx), %rax
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L188:
	cmpq	16(%rbx), %rdx
	jbe	.L168
	movzwl	-4(%rax), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L168
	leaq	-4(%rax), %rcx
	sall	$10, %edx
	movq	%rcx, 48(%rbx)
	leal	-56613888(%rsi,%rdx), %esi
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L169:
	cmpl	$1114111, %esi
	jg	.L171
	cmpl	24(%rdi), %esi
	jl	.L172
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	movzwl	(%r15,%rax), %eax
	cmpw	$-1025, %ax
	ja	.L173
	.p2align 4,,10
	.p2align 3
.L174:
	movq	56(%rbx), %rax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L171:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L187:
	cmpq	16(%rdi), %rax
	jbe	.L175
	movzwl	-4(%rcx), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L175
	leaq	-4(%rcx), %rax
	movq	%rax, 48(%rdi)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L172:
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L170
	.cfi_endproc
.LFE3218:
	.size	_ZN6icu_6716ReorderingBuffer6insertEih, .-_ZN6icu_6716ReorderingBuffer6insertEih
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer19appendSupplementaryEihR10UErrorCode
	.type	_ZN6icu_6716ReorderingBuffer19appendSupplementaryEihR10UErrorCode, @function
_ZN6icu_6716ReorderingBuffer19appendSupplementaryEihR10UErrorCode:
.LFB3209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$1, 40(%rdi)
	jle	.L191
.L195:
	cmpb	%r13b, 44(%rbx)
	jbe	.L192
	testb	%r13b, %r13b
	jne	.L202
.L192:
	movl	%r12d, %edx
	movq	32(%rbx), %rax
	andw	$1023, %r12w
	sarl	$10, %edx
	orw	$-9216, %r12w
	subw	$10304, %dx
	movw	%r12w, 2(%rax)
	addq	$4, %rax
	movw	%dx, -4(%rax)
	movq	%rax, 32(%rbx)
	movb	%r13b, 44(%rbx)
	cmpb	$1, %r13b
	jbe	.L203
.L196:
	subl	$2, 40(%rbx)
	movl	$1, %eax
.L190:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	%rax, 24(%rbx)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%rcx, %rdx
	movl	$2, %esi
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	jne	.L195
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L202:
	movzbl	%r13b, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	jmp	.L196
	.cfi_endproc
.LFE3209:
	.size	_ZN6icu_6716ReorderingBuffer19appendSupplementaryEihR10UErrorCode, .-_ZN6icu_6716ReorderingBuffer19appendSupplementaryEihR10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode.part.0, @function
_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode.part.0:
.LFB4278:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movslq	%edx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movb	%cl, -50(%rbp)
	subl	%r13d, 40(%rdi)
	movb	%r9b, -49(%rbp)
	cmpb	%r8b, 44(%rdi)
	jbe	.L265
	testb	%r8b, %r8b
	jne	.L205
.L265:
	movq	32(%rbx), %rcx
	addq	%r13, %r13
	cmpb	$1, %r9b
	jbe	.L294
	cmpb	$1, %r8b
	ja	.L208
	leaq	2(%rcx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L294:
	leaq	(%rcx,%r13), %rax
	movq	%rax, 24(%rbx)
.L208:
	leaq	-2(%r13), %r8
	leaq	15(%r12), %rax
	movq	%r8, %rdx
	subq	%rcx, %rax
	leaq	(%r12,%r13), %r10
	shrq	%rdx
	cmpq	$30, %rax
	jbe	.L256
	cmpq	$12, %r8
	jbe	.L256
	leaq	1(%rdx), %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	$3, %rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L210:
	movdqu	(%r12,%rax), %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L210
	movq	%rdi, %rdx
	andq	$-8, %rdx
	leaq	(%rdx,%rdx), %rax
	addq	%rax, %r12
	addq	%rcx, %rax
	cmpq	%rdi, %rdx
	je	.L212
	movzwl	(%r12), %edx
	movw	%dx, (%rax)
	leaq	2(%r12), %rdx
	cmpq	%rdx, %r10
	je	.L212
	movzwl	2(%r12), %edx
	movw	%dx, 2(%rax)
	leaq	4(%r12), %rdx
	cmpq	%rdx, %r10
	je	.L212
	movzwl	4(%r12), %edx
	movw	%dx, 4(%rax)
	leaq	6(%r12), %rdx
	cmpq	%rdx, %r10
	je	.L212
	movzwl	6(%r12), %edx
	movw	%dx, 6(%rax)
	leaq	8(%r12), %rdx
	cmpq	%rdx, %r10
	je	.L212
	movzwl	8(%r12), %edx
	movw	%dx, 8(%rax)
	leaq	10(%r12), %rdx
	cmpq	%rdx, %r10
	je	.L212
	movzwl	10(%r12), %edx
	movw	%dx, 10(%rax)
	leaq	12(%r12), %rdx
	cmpq	%rdx, %r10
	je	.L212
	movzwl	12(%r12), %edx
	movw	%dx, 12(%rax)
.L212:
	leaq	2(%rcx,%r8), %rax
	movb	%r9b, 44(%rbx)
	movq	%rax, 32(%rbx)
.L273:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movzwl	(%rsi), %esi
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L257
	cmpl	$1, %r13d
	jne	.L295
.L257:
	movl	$1, %r15d
.L214:
	movzbl	%r8b, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	cmpl	%r15d, %r13d
	jle	.L273
	movzbl	-49(%rbp), %eax
	movl	%eax, -56(%rbp)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L217:
	cmpl	%r11d, %r13d
	jle	.L296
	cmpb	$0, -50(%rbp)
	movq	(%rbx), %rdx
	jne	.L297
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L298
.L228:
	movl	40(%rbx), %eax
	movl	%r11d, %r15d
	movl	$1, %esi
	movl	$1, %r9d
.L233:
	cmpw	%si, 18(%rdx)
	ja	.L263
	cmpw	%si, 26(%rdx)
	jbe	.L263
	movq	48(%rdx), %rdx
	sarl	%r9d
	movslq	%r9d, %r9
	leaq	(%rdx,%r9,2), %rdx
	testb	$-128, (%rdx)
	je	.L263
	movzbl	-2(%rdx), %r9d
	movl	%r9d, %r8d
	.p2align 4,,10
	.p2align 3
.L220:
	cmpl	$65535, %r14d
	jle	.L299
	cmpl	$1, %eax
	jle	.L240
.L243:
	cmpb	44(%rbx), %r8b
	jnb	.L241
	testb	%r8b, %r8b
	jne	.L300
.L241:
	movl	%r14d, %edx
	movq	32(%rbx), %rax
	andw	$1023, %r14w
	sarl	$10, %edx
	orw	$-9216, %r14w
	subw	$10304, %dx
	movw	%r14w, 2(%rax)
	addq	$4, %rax
	movw	%dx, -4(%rax)
	movq	%rax, 32(%rbx)
	movb	%r8b, 44(%rbx)
	cmpb	$1, %r8b
	ja	.L244
	movq	%rax, 24(%rbx)
.L244:
	subl	$2, 40(%rbx)
.L236:
	cmpl	%r15d, %r13d
	jle	.L273
.L216:
	movslq	%r15d, %rax
	leal	1(%r15), %r11d
	movzwl	(%r12,%rax,2), %r14d
	leaq	(%rax,%rax), %rdx
	movl	%r14d, %eax
	movl	%r14d, %r10d
	andl	$64512, %eax
	cmpl	$55296, %eax
	jne	.L217
	cmpl	%r11d, %r13d
	je	.L218
	movzwl	2(%r12,%rdx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L217
	sall	$10, %r14d
	addl	$2, %r15d
	leal	-56613888(%rax,%r14), %r14d
	cmpl	%r15d, %r13d
	jg	.L219
	movl	-56(%rbp), %r9d
	movl	40(%rbx), %eax
	movzbl	-49(%rbp), %r8d
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L296:
	movl	-56(%rbp), %r9d
	movzbl	-49(%rbp), %r8d
	movl	%r11d, %r15d
.L247:
	movl	40(%rbx), %eax
	testl	%eax, %eax
	je	.L301
.L235:
	cmpb	%r8b, 44(%rbx)
	jbe	.L266
	testb	%r8b, %r8b
	jne	.L237
.L266:
	movq	32(%rbx), %rdx
	movl	40(%rbx), %eax
	leaq	2(%rdx), %rsi
	movq	%rsi, 32(%rbx)
	movw	%r10w, (%rdx)
	movb	%r8b, 44(%rbx)
	cmpb	$1, %r8b
	jbe	.L250
.L239:
	subl	$1, %eax
	movl	%eax, 40(%rbx)
	jmp	.L236
.L219:
	cmpb	$0, -50(%rbp)
	movq	(%rbx), %rdx
	je	.L221
	movq	32(%rdx), %rdi
	movq	8(%rdi), %rdx
	cmpl	$65535, %r14d
	jbe	.L302
	cmpl	$1114111, %r14d
	ja	.L225
	cmpl	24(%rdi), %r14d
	jl	.L226
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
.L227:
	movzwl	(%rdx,%rax), %r9d
	movl	40(%rbx), %eax
	cmpw	$-1025, %r9w
	ja	.L293
.L263:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L297:
	movq	32(%rdx), %rdi
	movq	8(%rdi), %rdx
.L251:
	movl	%r14d, %eax
	movq	(%rdi), %rcx
	movl	%r11d, %r15d
	sarl	$6, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	movl	%r14d, %ecx
	andl	$63, %ecx
	addl	%ecx, %eax
	cltq
	movzwl	(%rdx,%rax,2), %r9d
	movl	40(%rbx), %eax
	cmpw	$-1025, %r9w
	ja	.L293
	testl	%eax, %eax
	je	.L303
	movq	32(%rbx), %rdx
	leaq	2(%rdx), %rsi
	movq	%rsi, 32(%rbx)
	movw	%r14w, (%rdx)
	movb	$0, 44(%rbx)
.L250:
	movq	%rsi, 24(%rbx)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L301:
	movq	16(%rbp), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r9d, -76(%rbp)
	movb	%r8b, -72(%rbp)
	movl	%r10d, -64(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-64(%rbp), %r10d
	movzbl	-72(%rbp), %r8d
	testb	%al, %al
	movl	-76(%rbp), %r9d
	jne	.L235
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L240:
	movq	16(%rbp), %rdx
	movl	$2, %esi
	movq	%rbx, %rdi
	movl	%r9d, -72(%rbp)
	movb	%r8b, -64(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movzbl	-64(%rbp), %r8d
	movl	-72(%rbp), %r9d
	testb	%al, %al
	jne	.L243
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L298:
	movq	32(%rdx), %rdi
	movl	%r11d, %r15d
	movq	8(%rdi), %r8
.L254:
	movl	%r14d, %eax
	movq	(%rdi), %rsi
	sarl	$6, %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	movl	%r14d, %esi
	andl	$63, %esi
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
.L230:
	movzwl	(%r8,%rax), %r9d
	movl	40(%rbx), %eax
	movl	%r9d, %esi
	cmpw	$-1025, %r9w
	jbe	.L233
.L293:
	sarl	%r9d
	movl	%r9d, %r8d
	movzbl	%r9b, %r9d
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L218:
	movl	-56(%rbp), %r9d
	movzbl	-49(%rbp), %r8d
	movl	%r13d, %r15d
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L300:
	movl	%r9d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L256:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L209:
	movzwl	(%r12,%rax,2), %esi
	movw	%si, (%rcx,%rax,2)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rdx, %rsi
	jne	.L209
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L237:
	movl	%r9d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	movl	40(%rbx), %eax
	jmp	.L239
.L295:
	movzwl	2(%r12), %eax
	movl	$1, %r15d
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L214
	sall	$10, %esi
	movl	$2, %r15d
	leal	-56613888(%rax,%rsi), %esi
	jmp	.L214
.L303:
	movq	16(%rbp), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r11d, -64(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-64(%rbp), %r11d
	movl	%r11d, %r15d
	testb	%al, %al
	je	.L236
	movq	32(%rbx), %rdx
	movl	40(%rbx), %eax
	leaq	2(%rdx), %rsi
	movq	%rsi, 32(%rbx)
	movw	%r14w, (%rdx)
	movb	$0, 44(%rbx)
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L225:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L227
.L226:
	movl	%r14d, %esi
	movq	%rdx, -64(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	-64(%rbp), %rdx
	cltq
	addq	%rax, %rax
	jmp	.L227
.L299:
	movl	%r14d, %r10d
	movzwl	%r14w, %r14d
	jmp	.L247
.L302:
	movl	%r15d, %r11d
	jmp	.L251
.L221:
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L260
	movq	32(%rdx), %rdi
	movq	8(%rdi), %r8
	cmpl	$65535, %r14d
	jbe	.L254
	cmpl	$1114111, %r14d
	ja	.L231
	cmpl	24(%rdi), %r14d
	jl	.L232
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L231:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L230
.L232:
	movl	%r14d, %esi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	cltq
	addq	%rax, %rax
	jmp	.L230
.L260:
	movl	%r15d, %r11d
	jmp	.L228
	.cfi_endproc
.LFE4278:
	.size	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode.part.0, .-_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode
	.type	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode, @function
_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode:
.LFB3210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	testl	%edx, %edx
	je	.L308
	movq	%rsi, %r13
	movl	%edx, %r12d
	movl	%ecx, %r14d
	movl	%r8d, %r15d
	cmpl	%edx, 40(%rdi)
	jl	.L306
.L307:
	movq	%rbx, 16(%rbp)
	addq	$24, %rsp
	movsbl	%r14b, %ecx
	movzbl	%r15b, %r8d
	popq	%rbx
	movl	%r12d, %edx
	movq	%r13, %rsi
	popq	%r12
	movzbl	%r9b, %r9d
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movq	%rbx, %rdx
	movl	%r12d, %esi
	movl	%r9d, -60(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movq	-56(%rbp), %rdi
	movl	-60(%rbp), %r9d
	testb	%al, %al
	jne	.L307
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3210:
	.size	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode, .-_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	" 000000000000\02000"
	.section	.rodata
.LC2:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.type	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0, @function
_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0:
.LFB4280:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rdx, -80(%rbp)
	movb	%cl, -65(%rbp)
	movq	%rax, -88(%rbp)
	movb	%r8b, -66(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdx
	jbe	.L410
	leaq	-62(%rbp), %rax
	movq	%rdi, %r13
	movq	%rsi, %r8
	movq	%r9, %r14
	movq	%rax, -120(%rbp)
.L312:
	movzbl	(%r8), %eax
	movq	32(%r13), %r9
	leaq	1(%r8), %r15
	movl	%eax, %edx
	testb	%al, %al
	js	.L412
.L313:
	movq	8(%r9), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %r12d
	cmpw	26(%r13), %r12w
	jb	.L319
	movzwl	%r12w, %ebx
	cmpw	30(%r13), %r12w
	jnb	.L413
	cmpb	$0, -65(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rdx, -96(%rbp)
	jne	.L367
	movq	%r8, %rdi
	movq	%r15, %rsi
	movq	%r8, -112(%rbp)
	sarl	$3, %ebx
	call	_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_
	movq	-112(%rbp), %r8
	movq	-96(%rbp), %rdx
	addl	%eax, %ebx
	movzwl	28(%r13), %eax
	movq	-104(%rbp), %r9
	subl	%eax, %ebx
	cmpl	$65535, %ebx
	ja	.L335
	movl	%ebx, %eax
	movq	(%r9), %rcx
	movzwl	14(%r13), %r12d
	sarl	$6, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	movl	%ebx, %ecx
	andl	$63, %ecx
	addl	%ecx, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	cmpw	%r12w, %ax
	jb	.L414
.L337:
	cmpw	%r12w, %ax
	je	.L350
	movzwl	16(%r13), %ecx
	movzwl	%ax, %edx
	movl	%eax, %r12d
	orl	$1, %ecx
	cmpw	%ax, %cx
	je	.L350
.L351:
	movq	48(%r13), %rax
	sarl	%edx
	xorl	%r8d, %r8d
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rcx
	movzwl	(%rcx), %ebx
	movl	%ebx, %eax
	andl	$31, %eax
	testb	$-128, %bl
	jne	.L415
	testw	%ax, %ax
	je	.L348
.L425:
	movzwl	%ax, %r10d
	cmpl	40(%r14), %r10d
	jg	.L358
.L359:
	subq	$8, %rsp
	pushq	-88(%rbp)
	movzbl	%bh, %eax
	leaq	2(%rcx), %rsi
	movl	%r10d, %edx
	movl	$1, %ecx
	movl	%eax, %r9d
	movq	%r14, %rdi
	call	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L411
	.p2align 4,,10
	.p2align 3
.L348:
	cmpb	$0, -65(%rbp)
	je	.L328
.L420:
	testb	$1, %r12b
	je	.L328
	cmpb	$0, -66(%rbp)
	je	.L310
	cmpw	$1, %r12w
	jne	.L416
.L310:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L417
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	cmpb	$0, -65(%rbp)
	je	.L342
	cmpw	22(%r13), %r12w
	jb	.L367
.L342:
	movzwl	14(%r13), %eax
	cmpw	%ax, %r12w
	jnb	.L418
.L343:
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_
	movl	%eax, %ebx
.L345:
	movl	40(%r14), %edx
	cmpl	$65535, %ebx
	jle	.L361
	cmpl	$1, %edx
	jle	.L419
.L349:
	movl	%ebx, %eax
	movq	32(%r14), %rcx
	andw	$1023, %bx
	subl	$2, %edx
	sarl	$10, %eax
	orw	$-9216, %bx
	subw	$10304, %ax
	cmpb	$0, -65(%rbp)
	movw	%bx, 2(%rcx)
	movw	%ax, (%rcx)
	leaq	4(%rcx), %rax
	movq	%rax, %xmm0
	movb	$0, 44(%r14)
	punpcklqdq	%xmm0, %xmm0
	movl	%edx, 40(%r14)
	movups	%xmm0, 24(%r14)
	jne	.L420
	.p2align 4,,10
	.p2align 3
.L328:
	cmpq	%r15, -80(%rbp)
	jbe	.L310
	movq	%r15, %r8
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L412:
	movq	-80(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L314
	cmpl	$223, %eax
	jle	.L315
	cmpl	$239, %eax
	jg	.L316
	movzbl	1(%r8), %ecx
	movl	%eax, %esi
	andl	$15, %edx
	leaq	.LC1(%rip), %rax
	movsbl	(%rax,%rdx), %edx
	andl	$15, %esi
	movl	%ecx, %eax
	shrb	$5, %al
	btl	%eax, %edx
	jnc	.L314
	leaq	2(%r8), %r15
	cmpq	%r15, %rdi
	je	.L314
	movzbl	2(%r8), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L314
	sall	$6, %esi
	andl	$63, %ecx
	movzbl	%al, %eax
	addl	%esi, %ecx
	movslq	%ecx, %rdx
	movq	(%r9), %rcx
	movzwl	(%rcx,%rdx,2), %edx
	addl	%edx, %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L418:
	movzwl	%r12w, %edx
	cmpw	%ax, %r12w
	je	.L352
	movzwl	16(%r13), %eax
	orl	$1, %eax
	cmpw	%ax, %r12w
	jne	.L351
.L352:
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_
	movl	%eax, %ebx
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L315:
	cmpl	$193, %eax
	jg	.L421
	.p2align 4,,10
	.p2align 3
.L314:
	movl	20(%r9), %eax
	subl	$1, %eax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%r8, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_
	movl	%eax, %r8d
	cmpw	$-1025, %r12w
	ja	.L422
	movl	40(%r14), %eax
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	cmpl	$65535, %r8d
	jg	.L322
.L426:
	testl	%eax, %eax
	je	.L323
.L326:
	cmpb	44(%r14), %r12b
	jnb	.L324
	testb	%r12b, %r12b
	jne	.L423
.L324:
	movq	32(%r14), %rax
	leaq	2(%rax), %rdx
	movq	%rdx, 32(%r14)
	movw	%r8w, (%rax)
	movb	%r12b, 44(%r14)
	cmpb	$1, %r12b
	jbe	.L424
.L327:
	subl	$1, 40(%r14)
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L335:
	cmpl	$1114111, %ebx
	ja	.L338
	cmpl	24(%r9), %ebx
	jl	.L339
	movl	20(%r9), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
.L340:
	movzwl	(%rdx,%rax), %r12d
	movzwl	14(%r13), %edx
	cmpw	%dx, %r12w
	jnb	.L409
	movl	40(%r14), %edx
	cmpl	$1, %edx
	jg	.L349
.L419:
	movq	-88(%rbp), %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L411
	movl	40(%r14), %edx
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L415:
	movzbl	-1(%rcx), %r8d
	testw	%ax, %ax
	jne	.L425
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L422:
	sarl	%ebx
	movl	40(%r14), %eax
	movl	%ebx, %r12d
	movzbl	%bl, %ebx
	cmpl	$65535, %r8d
	jle	.L426
.L322:
	cmpl	$1, %eax
	jle	.L329
.L333:
	cmpb	44(%r14), %r12b
	jnb	.L330
	testb	%r12b, %r12b
	jne	.L427
.L330:
	movl	%r8d, %eax
	movq	32(%r14), %rdx
	andw	$1023, %r8w
	sarl	$10, %eax
	orw	$-9216, %r8w
	subw	$10304, %ax
	movw	%r8w, 2(%rdx)
	addq	$4, %rdx
	movw	%ax, -4(%rdx)
	movq	%rdx, 32(%r14)
	movb	%r12b, 44(%r14)
	cmpb	$1, %r12b
	jbe	.L428
.L334:
	subl	$2, 40(%r14)
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L338:
	movl	20(%r9), %eax
	subl	$1, %eax
	cltq
	movzwl	(%rdx,%rax,2), %r12d
	movzwl	14(%r13), %edx
	cmpw	%r12w, %dx
	jbe	.L409
	testl	%ebx, %ebx
	js	.L343
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L350:
	testl	%ebx, %ebx
	js	.L352
.L353:
	subl	$44032, %ebx
	movslq	%ebx, %rdx
	movl	%ebx, %eax
	imulq	$-1840700269, %rdx, %rdx
	sarl	$31, %eax
	shrq	$32, %rdx
	addl	%ebx, %edx
	sarl	$4, %edx
	movl	%edx, %esi
	subl	%eax, %edx
	movslq	%edx, %rcx
	subl	%eax, %esi
	movl	%edx, %eax
	imulq	$818089009, %rcx, %rcx
	sarl	$31, %eax
	imull	$28, %esi, %esi
	sarq	$34, %rcx
	subl	%eax, %ecx
	leal	4352(%rcx), %eax
	movw	%ax, -62(%rbp)
	leal	(%rcx,%rcx,4), %eax
	leal	(%rcx,%rax,4), %eax
	subl	%eax, %edx
	addw	$4449, %dx
	subl	%esi, %ebx
	movw	%dx, -60(%rbp)
	movl	%ebx, %esi
	movl	$4, %edx
	je	.L356
	addw	$4519, %si
	movl	$6, %edx
	movw	%si, -58(%rbp)
.L356:
	movq	-120(%rbp), %rsi
	movq	-88(%rbp), %rcx
	movq	%r14, %rdi
	addq	%rsi, %rdx
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	testb	%al, %al
	jne	.L348
	.p2align 4,,10
	.p2align 3
.L411:
	xorl	%r15d, %r15d
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L421:
	movzbl	1(%r8), %eax
	leal	-128(%rax), %ecx
	cmpb	$63, %cl
	ja	.L314
	movq	(%r9), %rax
	andl	$31, %edx
	movzbl	%cl, %ecx
	movzwl	(%rax,%rdx,2), %eax
	addl	%ecx, %eax
.L317:
	addq	$1, %r15
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L316:
	subl	$240, %eax
	cmpl	$4, %eax
	jg	.L314
	movzbl	1(%r8), %esi
	leaq	.LC2(%rip), %rdi
	movq	%rsi, %rdx
	shrq	$4, %rdx
	andl	$15, %edx
	movsbl	(%rdi,%rdx), %edx
	btl	%eax, %edx
	jnc	.L314
	movq	-80(%rbp), %rdi
	sall	$6, %eax
	andl	$63, %esi
	leaq	2(%r8), %r15
	orl	%esi, %eax
	cmpq	%r15, %rdi
	je	.L314
	movzbl	2(%r8), %esi
	leal	-128(%rsi), %edx
	cmpb	$63, %dl
	ja	.L314
	leaq	3(%r8), %r15
	cmpq	%r15, %rdi
	je	.L314
	movzbl	3(%r8), %edi
	leal	-128(%rdi), %ecx
	cmpb	$63, %cl
	ja	.L314
	movzwl	28(%r9), %esi
	cmpl	%esi, %eax
	jl	.L318
	movl	20(%r9), %eax
	subl	$2, %eax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L416:
	cmpw	%r12w, 26(%r13)
	ja	.L360
	andl	$6, %r12d
	cmpw	$2, %r12w
	jbe	.L310
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L414:
	movl	40(%r14), %edx
	movl	%eax, %r12d
.L361:
	testl	%edx, %edx
	je	.L429
.L347:
	movq	32(%r14), %rcx
	subl	$1, %edx
	leaq	2(%rcx), %rax
	movq	%rax, 32(%r14)
	movw	%bx, (%rcx)
	movb	$0, 44(%r14)
	movq	%rax, 24(%r14)
	movl	%edx, 40(%r14)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L429:
	movq	-88(%rbp), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L411
	movl	40(%r14), %edx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L409:
	movl	%r12d, %eax
	movl	%edx, %r12d
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L360:
	movq	48(%r13), %rax
	andl	$65534, %r12d
	cmpw	$511, (%r12,%rax)
	ja	.L328
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%rdx, 24(%r14)
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rdx, 24(%r14)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L358:
	movq	-88(%rbp), %rdx
	movl	%r10d, %esi
	movq	%r14, %rdi
	movl	%r8d, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movl	%r10d, -96(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-96(%rbp), %r10d
	movq	-104(%rbp), %rcx
	testb	%al, %al
	movl	-112(%rbp), %r8d
	jne	.L359
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L323:
	movq	-88(%rbp), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%r8d, -96(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-96(%rbp), %r8d
	testb	%al, %al
	jne	.L326
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L339:
	movl	%ebx, %esi
	movq	%r9, %rdi
	movq	%r8, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r8
	cltq
	addq	%rax, %rax
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L329:
	movq	-88(%rbp), %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	movl	%r8d, -96(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-96(%rbp), %r8d
	testb	%al, %al
	jne	.L333
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%r8, %r14
.L410:
	movq	%r14, %r15
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L427:
	movl	%ebx, %edx
	movl	%r8d, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L423:
	movzwl	%r8w, %esi
	movl	%ebx, %edx
	movq	%r14, %rdi
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	jmp	.L327
.L318:
	movq	%r9, %rdi
	movzbl	%cl, %ecx
	movzbl	%dl, %edx
	movl	%eax, %esi
	movq	%r8, -96(%rbp)
	call	ucptrie_internalSmallU8Index_67@PLT
	movq	32(%r13), %r9
	movq	-96(%rbp), %r8
	jmp	.L317
.L417:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4280:
	.size	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0, .-_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0
	.align 2
	.p2align 4
	.type	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0, @function
_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0:
.LFB4279:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movb	%r8b, -73(%rbp)
	movq	%rax, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rsi, %rdx
	jbe	.L431
	movl	%ecx, %r13d
	movq	%rdi, %rbx
	movq	%r9, %r15
	movq	%rsi, %rcx
	movq	%rdx, %r12
.L432:
	movzwl	(%rcx), %eax
	testb	%r13b, %r13b
	je	.L433
	cmpw	%ax, 10(%rbx)
	ja	.L528
.L433:
	movl	%eax, %edx
	movq	32(%rbx), %rdi
	leaq	2(%rcx), %r14
	movzwl	%ax, %r8d
	andl	$63488, %edx
	cmpl	$55296, %edx
	je	.L434
	movl	%r8d, %edx
	movq	(%rdi), %rsi
	andl	$63, %eax
	sarl	$6, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	addl	%edx, %eax
.L435:
	movq	8(%rdi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %r10d
	testb	%r13b, %r13b
	je	.L439
.L535:
	cmpw	22(%rbx), %r10w
	jb	.L528
	cmpw	26(%rbx), %r10w
	jnb	.L530
	movzwl	%r10w, %r11d
	movl	%r10d, %esi
.L442:
	cmpw	%si, 14(%rbx)
	ja	.L531
.L462:
	je	.L466
	movzwl	16(%rbx), %eax
	orl	$1, %eax
	cmpw	%ax, %si
	je	.L466
	andl	$65534, %esi
	addq	48(%rbx), %rsi
	xorl	%r8d, %r8d
	movzwl	(%rsi), %r9d
	movq	%rsi, %rcx
	movl	%r9d, %eax
	andl	$31, %eax
	testb	$-128, %r9b
	je	.L471
	movzbl	-1(%rsi), %r8d
.L471:
	testw	%ax, %ax
	je	.L451
	movzwl	%ax, %esi
	cmpl	40(%r15), %esi
	jg	.L473
.L474:
	movl	%r9d, %eax
	subq	$8, %rsp
	pushq	-72(%rbp)
	movl	%esi, %edx
	movzbl	%ah, %eax
	movq	%r15, %rdi
	movl	%r11d, -88(%rbp)
	movl	%eax, %r9d
	leaq	2(%rcx), %rax
	movl	$1, %ecx
	movl	%r10d, -80(%rbp)
	movq	%rax, %rsi
	call	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode.part.0
	popq	%rdx
	movl	-80(%rbp), %r10d
	movl	-88(%rbp), %r11d
	popq	%rcx
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L452:
	movq	-72(%rbp), %rdx
	movl	$2, %esi
	movq	%r15, %rdi
	movl	%r11d, -104(%rbp)
	movl	%r9d, -100(%rbp)
	movb	%cl, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-80(%rbp), %r8d
	movl	-88(%rbp), %r10d
	testb	%al, %al
	movzbl	-96(%rbp), %ecx
	movl	-100(%rbp), %r9d
	movl	-104(%rbp), %r11d
	jne	.L456
.L455:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L431:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L532
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	cmpw	30(%rbx), %r10w
	jnb	.L533
	.p2align 4,,10
	.p2align 3
.L528:
	movq	%rcx, %r12
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L434:
	testb	$4, %ah
	jne	.L436
	cmpq	%r14, %r12
	je	.L436
	movzwl	2(%rcx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L534
.L436:
	movl	20(%rdi), %eax
	movq	8(%rdi), %rdx
	subl	$1, %eax
	cltq
	movzwl	(%rdx,%rax,2), %r10d
	testb	%r13b, %r13b
	jne	.L535
.L439:
	movzwl	%r10w, %r11d
	cmpw	26(%rbx), %r10w
	jb	.L480
	cmpw	30(%rbx), %r10w
	jb	.L443
	cmpw	$-1025, %r10w
	ja	.L536
.L481:
	movl	40(%r15), %eax
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	cmpl	$65535, %r8d
	jg	.L445
.L541:
	testl	%eax, %eax
	je	.L446
.L449:
	cmpb	44(%r15), %cl
	jnb	.L447
	testb	%cl, %cl
	jne	.L537
.L447:
	movq	32(%r15), %rax
	leaq	2(%rax), %rdx
	movq	%rdx, 32(%r15)
	movw	%r8w, (%rax)
	movb	%cl, 44(%r15)
	cmpb	$1, %cl
	ja	.L450
	movq	%rdx, 24(%r15)
.L450:
	subl	$1, 40(%r15)
	testb	%r13b, %r13b
	jne	.L538
	.p2align 4,,10
	.p2align 3
.L475:
	cmpq	%r14, %r12
	jbe	.L484
	movq	%r14, %rcx
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L480:
	movl	%r10d, %esi
	cmpw	%si, 14(%rbx)
	jbe	.L462
.L531:
	movl	40(%r15), %eax
	cmpl	$65535, %r8d
	jg	.L463
	testl	%eax, %eax
	je	.L539
.L464:
	movq	32(%r15), %rcx
	subl	$1, %eax
	leaq	2(%rcx), %rdx
	movq	%rdx, 32(%r15)
	movw	%r8w, (%rcx)
	movb	$0, 44(%r15)
	movq	%rdx, 24(%r15)
	movl	%eax, 40(%r15)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L466:
	subl	$44032, %r8d
	movslq	%r8d, %rax
	movl	%r8d, %edx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	addl	%r8d, %eax
	sarl	$4, %eax
	movl	%eax, %ecx
	subl	%edx, %eax
	subl	%edx, %ecx
	movslq	%eax, %rdx
	movl	%eax, %esi
	imulq	$818089009, %rdx, %rdx
	sarl	$31, %esi
	imull	$28, %ecx, %ecx
	sarq	$34, %rdx
	subl	%esi, %edx
	leal	4352(%rdx), %esi
	movw	%si, -62(%rbp)
	leal	(%rdx,%rdx,4), %esi
	leal	(%rdx,%rsi,4), %edx
	subl	%edx, %eax
	movl	$4, %edx
	addw	$4449, %ax
	subl	%ecx, %r8d
	movw	%ax, -60(%rbp)
	movl	%r8d, %ecx
	jne	.L540
.L468:
	leaq	-62(%rbp), %rsi
	movq	-72(%rbp), %rcx
	movq	%r15, %rdi
	movl	%r11d, -88(%rbp)
	addq	%rsi, %rdx
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movl	-80(%rbp), %r10d
	movl	-88(%rbp), %r11d
.L472:
	testb	%al, %al
	je	.L455
.L451:
	testb	%r13b, %r13b
	je	.L475
.L538:
	testb	$1, %r10b
	je	.L475
	cmpb	$0, -73(%rbp)
	je	.L484
	cmpw	$1, %r10w
	je	.L484
	cmpw	26(%rbx), %r10w
	jb	.L476
	andl	$6, %r10d
	cmpw	$2, %r10w
	ja	.L475
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%r14, %r12
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L536:
	movl	%r11d, %r9d
	movl	40(%r15), %eax
	sarl	%r9d
	movl	%r9d, %ecx
	movzbl	%r9b, %r9d
	cmpl	$65535, %r8d
	jle	.L541
.L445:
	cmpl	$1, %eax
	jle	.L452
.L456:
	cmpb	44(%r15), %cl
	jnb	.L453
	testb	%cl, %cl
	jne	.L542
.L453:
	movl	%r8d, %edx
	movq	32(%r15), %rax
	andw	$1023, %r8w
	sarl	$10, %edx
	orw	$-9216, %r8w
	subw	$10304, %dx
	movw	%r8w, 2(%rax)
	addq	$4, %rax
	movw	%dx, -4(%rax)
	movq	%rax, 32(%r15)
	movb	%cl, 44(%r15)
	cmpb	$1, %cl
	ja	.L457
	movq	%rax, 24(%r15)
.L457:
	subl	$2, 40(%r15)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L443:
	movl	%r11d, %eax
	sarl	$3, %eax
	addl	%eax, %r8d
	movzwl	28(%rbx), %eax
	subl	%eax, %r8d
	cmpl	$65535, %r8d
	ja	.L458
	movl	%r8d, %eax
	movq	(%rdi), %rcx
	sarl	$6, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	movl	%r8d, %ecx
	andl	$63, %ecx
	addl	%ecx, %eax
	cltq
	addq	%rax, %rax
.L459:
	movzwl	(%rdx,%rax), %esi
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L533:
	movzwl	%r10w, %r11d
	cmpw	$-1025, %r10w
	jbe	.L481
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L540:
	addw	$4519, %cx
	movl	$6, %edx
	movw	%cx, -58(%rbp)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L463:
	cmpl	$1, %eax
	jle	.L543
.L465:
	movq	32(%r15), %rdx
	movl	%r8d, %ecx
	andw	$1023, %r8w
	subl	$2, %eax
	sarl	$10, %ecx
	orw	$-9216, %r8w
	leaq	4(%rdx), %rdi
	subw	$10304, %cx
	movw	%r8w, 2(%rdx)
	movq	%rdi, %xmm0
	movw	%cx, (%rdx)
	punpcklqdq	%xmm0, %xmm0
	movb	$0, 44(%r15)
	movl	%eax, 40(%r15)
	movups	%xmm0, 24(%r15)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L534:
	sall	$10, %r8d
	leaq	4(%rcx), %r14
	leal	-56613888(%rax,%r8), %r8d
	cmpl	24(%rdi), %r8d
	jl	.L544
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L458:
	cmpl	$1114111, %r8d
	ja	.L460
	cmpl	24(%rdi), %r8d
	jl	.L461
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L446:
	movq	-72(%rbp), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movl	%r11d, -104(%rbp)
	movl	%r9d, -100(%rbp)
	movb	%cl, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-80(%rbp), %r8d
	movl	-88(%rbp), %r10d
	testb	%al, %al
	movzbl	-96(%rbp), %ecx
	movl	-100(%rbp), %r9d
	movl	-104(%rbp), %r11d
	jne	.L449
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L460:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L476:
	sarl	%r11d
	movq	48(%rbx), %rax
	movslq	%r11d, %r11
	cmpw	$511, (%rax,%r11,2)
	ja	.L475
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L544:
	movl	%r8d, %esi
	movq	%rcx, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%rbx), %rdi
	movq	-88(%rbp), %rcx
	movl	-80(%rbp), %r8d
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L473:
	movq	-72(%rbp), %rdx
	movq	%r15, %rdi
	movl	%r11d, -108(%rbp)
	movl	%r8d, -104(%rbp)
	movl	%r9d, -100(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%esi, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-80(%rbp), %esi
	movl	-88(%rbp), %r10d
	testb	%al, %al
	movq	-96(%rbp), %rcx
	movl	-100(%rbp), %r9d
	movl	-104(%rbp), %r8d
	movl	-108(%rbp), %r11d
	jne	.L474
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L539:
	movq	-72(%rbp), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movl	%r11d, -96(%rbp)
	movl	%r8d, -88(%rbp)
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L455
	movl	40(%r15), %eax
	movl	-96(%rbp), %r11d
	movl	-88(%rbp), %r8d
	movl	-80(%rbp), %r10d
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L543:
	movq	-72(%rbp), %rdx
	movl	$2, %esi
	movq	%r15, %rdi
	movl	%r11d, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L455
	movl	40(%r15), %eax
	movl	-96(%rbp), %r11d
	movl	-88(%rbp), %r10d
	movl	-80(%rbp), %r8d
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L542:
	movl	%r9d, %edx
	movl	%r8d, %esi
	movq	%r15, %rdi
	movl	%r11d, -88(%rbp)
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	movl	-88(%rbp), %r11d
	movl	-80(%rbp), %r10d
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L537:
	movzwl	%r8w, %esi
	movl	%r9d, %edx
	movq	%r15, %rdi
	movl	%r11d, -88(%rbp)
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	movl	-88(%rbp), %r11d
	movl	-80(%rbp), %r10d
	jmp	.L450
.L461:
	movl	%r8d, %esi
	movl	%r11d, -100(%rbp)
	movl	%r10d, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movl	-80(%rbp), %r8d
	movq	-88(%rbp), %rdx
	cltq
	movl	-96(%rbp), %r10d
	movl	-100(%rbp), %r11d
	addq	%rax, %rax
	jmp	.L459
.L532:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4279:
	.size	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0, .-_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715Normalizer2Impl4initEPKiPK7UCPTriePKtPKh
	.type	_ZN6icu_6715Normalizer2Impl4initEPKiPK7UCPTriePKtPKh, @function
_ZN6icu_6715Normalizer2Impl4initEPKiPK7UCPTriePKtPKh:
.LFB3223:
	.cfi_startproc
	endbr64
	movl	32(%rsi), %eax
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movw	%ax, 8(%rdi)
	movl	36(%rsi), %eax
	movw	%ax, 10(%rdi)
	movl	72(%rsi), %eax
	movw	%ax, 12(%rdi)
	movl	40(%rsi), %eax
	movw	%ax, 14(%rdi)
	movl	56(%rsi), %eax
	movw	%ax, 16(%rdi)
	movl	44(%rsi), %eax
	movw	%ax, 18(%rdi)
	movl	60(%rsi), %eax
	movw	%ax, 20(%rdi)
	movl	64(%rsi), %eax
	movw	%ax, 22(%rdi)
	movl	68(%rsi), %eax
	movw	%ax, 24(%rdi)
	movl	48(%rsi), %eax
	movw	%ax, 26(%rdi)
	movl	52(%rsi), %eax
	movq	%r8, 56(%rdi)
	movl	%eax, %esi
	movzwl	%ax, %edx
	movw	%ax, 30(%rdi)
	movl	$64512, %eax
	subl	%edx, %eax
	shrw	$3, %si
	movups	%xmm0, 32(%rdi)
	sarl	%eax
	subl	$65, %esi
	cltq
	movw	%si, 28(%rdi)
	leaq	(%rcx,%rax,2), %rax
	movq	%rax, 48(%rdi)
	ret
	.cfi_endproc
.LFE3223:
	.size	_ZN6icu_6715Normalizer2Impl4initEPKiPK7UCPTriePKtPKh, .-_ZN6icu_6715Normalizer2Impl4initEPKiPK7UCPTriePKtPKh
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl30copyLowPrefixFromNulTerminatedEPKDsiPNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl30copyLowPrefixFromNulTerminatedEPKDsiPNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl30copyLowPrefixFromNulTerminatedEPKDsiPNS_16ReorderingBufferER10UErrorCode:
.LFB3228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L567:
	testw	%di, %di
	je	.L555
.L547:
	movq	%rax, %r12
	movzwl	(%rax), %edi
	addq	$2, %rax
	cmpl	%edx, %edi
	jl	.L567
.L555:
	testq	%rbx, %rbx
	je	.L546
	cmpq	%r13, %r12
	je	.L546
	movq	%r12, %r14
	subq	%r13, %r14
	sarq	%r14
	cmpl	40(%rbx), %r14d
	jle	.L554
	movq	%r8, %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L546
.L554:
	movq	32(%rbx), %rdi
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	u_memcpy_67@PLT
	movq	32(%rbx), %rax
	movslq	%r14d, %rdx
	subl	%r14d, 40(%rbx)
	movb	$0, 44(%rbx)
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
.L546:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3228:
	.size	_ZNK6icu_6715Normalizer2Impl30copyLowPrefixFromNulTerminatedEPKDsiPNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl30copyLowPrefixFromNulTerminatedEPKDsiPNS_16ReorderingBufferER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode:
.LFB3232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r11
	movb	%r8b, -73(%rbp)
	movq	%rdx, -72(%rbp)
	movl	(%r11), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L588
	movq	%rsi, %r12
	cmpq	-72(%rbp), %rsi
	jnb	.L570
	movl	%ecx, %r13d
	movq	%rdi, %rbx
	movq	%r9, %r14
	movq	%rsi, %rcx
	movq	%r11, %r12
.L571:
	movzwl	(%rcx), %eax
	testb	%r13b, %r13b
	je	.L572
	cmpw	%ax, 10(%rbx)
	ja	.L670
.L572:
	movl	%eax, %edx
	movq	32(%rbx), %rdi
	leaq	2(%rcx), %r15
	movzwl	%ax, %r8d
	andl	$63488, %edx
	cmpl	$55296, %edx
	je	.L573
	movl	%r8d, %edx
	movq	(%rdi), %rsi
	andl	$63, %eax
	sarl	$6, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	addl	%edx, %eax
.L574:
	movq	8(%rdi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %r10d
	testb	%r13b, %r13b
	je	.L578
.L678:
	cmpw	22(%rbx), %r10w
	jnb	.L672
.L670:
	movq	%rcx, %r12
.L570:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L673
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore_state
	cmpw	26(%rbx), %r10w
	jb	.L579
	cmpw	30(%rbx), %r10w
	jb	.L670
	movzwl	%r10w, %r11d
.L616:
	cmpw	$-1025, %r10w
	ja	.L674
	movl	40(%r14), %eax
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	cmpl	$65535, %r8d
	jg	.L584
.L681:
	testl	%eax, %eax
	je	.L585
.L589:
	cmpb	44(%r14), %cl
	jnb	.L586
	testb	%cl, %cl
	jne	.L675
.L586:
	movq	32(%r14), %rax
	leaq	2(%rax), %rdx
	movq	%rdx, 32(%r14)
	movw	%r8w, (%rax)
	movb	%cl, 44(%r14)
	cmpb	$1, %cl
	ja	.L590
	movq	%rdx, 24(%r14)
.L590:
	subl	$1, 40(%r14)
	testb	%r13b, %r13b
	jne	.L676
	.p2align 4,,10
	.p2align 3
.L614:
	cmpq	%r15, -72(%rbp)
	jbe	.L623
	movq	%r15, %rcx
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L573:
	testb	$4, %ah
	jne	.L575
	cmpq	%r15, -72(%rbp)
	je	.L575
	movzwl	2(%rcx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L575
	sall	$10, %r8d
	leaq	4(%rcx), %r15
	leal	-56613888(%rax,%r8), %r8d
	cmpl	24(%rdi), %r8d
	jl	.L677
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L575:
	movl	20(%rdi), %eax
	movq	8(%rdi), %rdx
	subl	$1, %eax
	cltq
	movzwl	(%rdx,%rax,2), %r10d
	testb	%r13b, %r13b
	jne	.L678
.L578:
	movzwl	%r10w, %r11d
	cmpw	26(%rbx), %r10w
	jb	.L619
	cmpw	30(%rbx), %r10w
	jnb	.L616
	movl	%r11d, %eax
	sarl	$3, %eax
	addl	%eax, %r8d
	movzwl	28(%rbx), %eax
	subl	%eax, %r8d
	cmpl	$65535, %r8d
	ja	.L597
	movl	%r8d, %eax
	movq	(%rdi), %rcx
	sarl	$6, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	movl	%r8d, %ecx
	andl	$63, %ecx
	addl	%ecx, %eax
	cltq
	addq	%rax, %rax
.L598:
	movzwl	(%rdx,%rax), %esi
	cmpw	%si, 14(%rbx)
	jbe	.L601
.L680:
	movl	40(%r14), %eax
	cmpl	$65535, %r8d
	jg	.L602
	testl	%eax, %eax
	je	.L679
.L603:
	movq	32(%r14), %rcx
	subl	$1, %eax
	leaq	2(%rcx), %rdx
	movq	%rdx, 32(%r14)
	movw	%r8w, (%rcx)
	movb	$0, 44(%r14)
	movq	%rdx, 24(%r14)
	movl	%eax, 40(%r14)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L592:
	movq	%r12, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	movl	%r11d, -104(%rbp)
	movl	%r9d, -100(%rbp)
	movb	%cl, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-80(%rbp), %r8d
	movl	-88(%rbp), %r10d
	testb	%al, %al
	movzbl	-96(%rbp), %ecx
	movl	-100(%rbp), %r9d
	movl	-104(%rbp), %r11d
	jne	.L595
	.p2align 4,,10
	.p2align 3
.L588:
	xorl	%r12d, %r12d
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L579:
	movzwl	%r10w, %r11d
	movl	%r10d, %esi
.L581:
	cmpw	%si, 14(%rbx)
	ja	.L680
.L601:
	je	.L605
	movzwl	16(%rbx), %eax
	orl	$1, %eax
	cmpw	%ax, %si
	je	.L605
	andl	$65534, %esi
	addq	48(%rbx), %rsi
	xorl	%r8d, %r8d
	movzwl	(%rsi), %r9d
	movq	%rsi, %rcx
	movl	%r9d, %eax
	andl	$31, %eax
	testb	$-128, %r9b
	je	.L610
	movzbl	-1(%rsi), %r8d
.L610:
	testw	%ax, %ax
	je	.L591
	movzwl	%ax, %esi
	cmpl	40(%r14), %esi
	jg	.L612
.L613:
	subq	$8, %rsp
	movl	%r9d, %eax
	movl	%esi, %edx
	movq	%r14, %rdi
	pushq	%r12
	movzbl	%ah, %eax
	movl	%eax, %r9d
	leaq	2(%rcx), %rax
	movl	$1, %ecx
	movl	%r11d, -88(%rbp)
	movq	%rax, %rsi
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode.part.0
	movl	-80(%rbp), %r10d
	movl	-88(%rbp), %r11d
	popq	%rdx
	popq	%rcx
.L611:
	testb	%al, %al
	je	.L588
.L591:
	testb	%r13b, %r13b
	je	.L614
.L676:
	testb	$1, %r10b
	je	.L614
	cmpb	$0, -73(%rbp)
	je	.L623
	cmpw	$1, %r10w
	je	.L623
	cmpw	26(%rbx), %r10w
	jb	.L615
	andl	$6, %r10d
	cmpw	$2, %r10w
	ja	.L614
	.p2align 4,,10
	.p2align 3
.L623:
	movq	%r15, %r12
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L674:
	movl	%r11d, %r9d
	movl	40(%r14), %eax
	sarl	%r9d
	movl	%r9d, %ecx
	movzbl	%r9b, %r9d
	cmpl	$65535, %r8d
	jle	.L681
.L584:
	cmpl	$1, %eax
	jle	.L592
.L595:
	cmpb	44(%r14), %cl
	jnb	.L593
	testb	%cl, %cl
	jne	.L682
.L593:
	movl	%r8d, %edx
	movq	32(%r14), %rax
	andw	$1023, %r8w
	sarl	$10, %edx
	orw	$-9216, %r8w
	subw	$10304, %dx
	movw	%r8w, 2(%rax)
	addq	$4, %rax
	movw	%dx, -4(%rax)
	movq	%rax, 32(%r14)
	movb	%cl, 44(%r14)
	cmpb	$1, %cl
	ja	.L596
	movq	%rax, 24(%r14)
.L596:
	subl	$2, 40(%r14)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L605:
	subl	$44032, %r8d
	movslq	%r8d, %rax
	movl	%r8d, %edx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	addl	%r8d, %eax
	sarl	$4, %eax
	movl	%eax, %ecx
	subl	%edx, %eax
	subl	%edx, %ecx
	movslq	%eax, %rdx
	movl	%eax, %esi
	imulq	$818089009, %rdx, %rdx
	sarl	$31, %esi
	imull	$28, %ecx, %ecx
	sarq	$34, %rdx
	subl	%esi, %edx
	leal	4352(%rdx), %esi
	movw	%si, -62(%rbp)
	leal	(%rdx,%rdx,4), %esi
	leal	(%rdx,%rsi,4), %edx
	subl	%edx, %eax
	movl	$4, %edx
	addw	$4449, %ax
	subl	%ecx, %r8d
	movw	%ax, -60(%rbp)
	movl	%r8d, %ecx
	je	.L607
	addw	$4519, %cx
	movl	$6, %edx
	movw	%cx, -58(%rbp)
.L607:
	leaq	-62(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	movl	%r11d, -88(%rbp)
	addq	%rsi, %rdx
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movl	-80(%rbp), %r10d
	movl	-88(%rbp), %r11d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L619:
	movl	%r10d, %esi
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L602:
	cmpl	$1, %eax
	jle	.L683
.L604:
	movq	32(%r14), %rdx
	movl	%r8d, %ecx
	andw	$1023, %r8w
	subl	$2, %eax
	sarl	$10, %ecx
	orw	$-9216, %r8w
	leaq	4(%rdx), %rdi
	subw	$10304, %cx
	movw	%r8w, 2(%rdx)
	movq	%rdi, %xmm0
	movw	%cx, (%rdx)
	punpcklqdq	%xmm0, %xmm0
	movb	$0, 44(%r14)
	movl	%eax, 40(%r14)
	movups	%xmm0, 24(%r14)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L597:
	cmpl	$1114111, %r8d
	ja	.L599
	cmpl	24(%rdi), %r8d
	jl	.L600
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L599:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L615:
	sarl	%r11d
	movq	48(%rbx), %rax
	movslq	%r11d, %r11
	cmpw	$511, (%rax,%r11,2)
	ja	.L614
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L682:
	movl	%r9d, %edx
	movl	%r8d, %esi
	movq	%r14, %rdi
	movl	%r11d, -88(%rbp)
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	movl	-88(%rbp), %r11d
	movl	-80(%rbp), %r10d
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L675:
	movzwl	%r8w, %esi
	movl	%r9d, %edx
	movq	%r14, %rdi
	movl	%r11d, -88(%rbp)
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	movl	-88(%rbp), %r11d
	movl	-80(%rbp), %r10d
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L677:
	movl	%r8d, %esi
	movq	%rcx, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%rbx), %rdi
	movq	-88(%rbp), %rcx
	movl	-80(%rbp), %r8d
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L585:
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%r11d, -104(%rbp)
	movl	%r9d, -100(%rbp)
	movb	%cl, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-80(%rbp), %r8d
	movl	-88(%rbp), %r10d
	testb	%al, %al
	movzbl	-96(%rbp), %ecx
	movl	-100(%rbp), %r9d
	movl	-104(%rbp), %r11d
	jne	.L589
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L612:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movl	%r11d, -108(%rbp)
	movl	%r8d, -104(%rbp)
	movl	%r9d, -100(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%esi, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-80(%rbp), %esi
	movl	-88(%rbp), %r10d
	testb	%al, %al
	movq	-96(%rbp), %rcx
	movl	-100(%rbp), %r9d
	movl	-104(%rbp), %r8d
	movl	-108(%rbp), %r11d
	jne	.L613
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L679:
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%r11d, -96(%rbp)
	movl	%r8d, -88(%rbp)
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L588
	movl	40(%r14), %eax
	movl	-96(%rbp), %r11d
	movl	-88(%rbp), %r8d
	movl	-80(%rbp), %r10d
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L683:
	movq	%r12, %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	movl	%r11d, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L588
	movl	40(%r14), %eax
	movl	-96(%rbp), %r11d
	movl	-88(%rbp), %r10d
	movl	-80(%rbp), %r8d
	jmp	.L604
.L600:
	movl	%r8d, %esi
	movl	%r11d, -100(%rbp)
	movl	%r10d, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movl	-80(%rbp), %r8d
	movq	-88(%rbp), %rdx
	cltq
	movl	-96(%rbp), %r10d
	movl	-100(%rbp), %r11d
	addq	%rax, %rax
	jmp	.L598
.L673:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3232:
	.size	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl9decomposeEitRNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl9decomposeEitRNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl9decomposeEitRNS_16ReorderingBufferER10UErrorCode:
.LFB3233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpw	%dx, 26(%rdi)
	ja	.L738
	movzwl	%dx, %esi
	cmpw	30(%rdi), %dx
	jb	.L686
	cmpw	$-1025, %dx
	ja	.L740
	movl	40(%r13), %eax
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	cmpl	$65535, %r12d
	jg	.L688
.L746:
	testl	%eax, %eax
	je	.L689
.L692:
	cmpb	44(%r13), %bl
	jnb	.L690
	testb	%bl, %bl
	jne	.L741
.L690:
	movq	32(%r13), %rax
	leaq	2(%rax), %rdx
	movq	%rdx, 32(%r13)
	movw	%r12w, (%rax)
	movb	%bl, 44(%r13)
	cmpb	$1, %bl
	ja	.L694
	movq	%rdx, 24(%r13)
.L694:
	subl	$1, 40(%r13)
	movl	$1, %eax
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L738:
	movl	%edx, %esi
.L685:
	cmpw	%si, 14(%rbx)
	ja	.L742
	je	.L709
	movzwl	16(%rbx), %eax
	orl	$1, %eax
	cmpw	%ax, %si
	je	.L709
	andl	$65534, %esi
	addq	48(%rbx), %rsi
	xorl	%r14d, %r14d
	movzwl	(%rsi), %r12d
	movq	%rsi, %rbx
	movl	%r12d, %edx
	andl	$31, %edx
	testb	$-128, %r12b
	je	.L714
	movzbl	-1(%rsi), %r14d
.L714:
	movl	$1, %eax
	testw	%dx, %dx
	je	.L684
	movzwl	%dx, %r15d
	cmpl	40(%r13), %r15d
	jg	.L715
.L716:
	subq	$8, %rsp
	movl	%r12d, %eax
	movl	$1, %ecx
	movl	%r15d, %edx
	pushq	%r8
	movzbl	%ah, %eax
	leaq	2(%rbx), %rsi
	movl	%r14d, %r8d
	movl	%eax, %r9d
	movq	%r13, %rdi
	call	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L742:
	movl	40(%r13), %eax
	cmpl	$65535, %r12d
	jg	.L706
	testl	%eax, %eax
	je	.L743
.L707:
	movq	32(%r13), %rcx
	subl	$1, %eax
	leaq	2(%rcx), %rdx
	movq	%rdx, 32(%r13)
	movw	%r12w, (%rcx)
	movl	%eax, 40(%r13)
	movl	$1, %eax
	movb	$0, 44(%r13)
	movq	%rdx, 24(%r13)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L709:
	subl	$44032, %r12d
	movslq	%r12d, %rax
	movl	%r12d, %edx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edx
	shrq	$32, %rax
	addl	%r12d, %eax
	sarl	$4, %eax
	movl	%eax, %esi
	subl	%edx, %eax
	subl	%edx, %esi
	movslq	%eax, %rdx
	movl	%eax, %edi
	imulq	$818089009, %rdx, %rdx
	sarl	$31, %edi
	imull	$28, %esi, %esi
	sarq	$34, %rdx
	subl	%edi, %edx
	leal	4352(%rdx), %edi
	movw	%di, -62(%rbp)
	leal	(%rdx,%rdx,4), %edi
	leal	(%rdx,%rdi,4), %edx
	subl	%edx, %eax
	movl	$4, %edx
	addw	$4449, %ax
	subl	%esi, %r12d
	movw	%ax, -60(%rbp)
	movl	%r12d, %esi
	jne	.L744
.L711:
	leaq	-62(%rbp), %rsi
	movq	%r8, %rcx
	movq	%r13, %rdi
	addq	%rsi, %rdx
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
.L684:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L745
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L740:
	.cfi_restore_state
	sarl	%esi
	movl	40(%r13), %eax
	movl	%esi, %ebx
	movzbl	%sil, %r14d
	cmpl	$65535, %r12d
	jle	.L746
.L688:
	cmpl	$1, %eax
	jle	.L695
.L699:
	cmpb	44(%r13), %bl
	jnb	.L696
	testb	%bl, %bl
	jne	.L747
.L696:
	movl	%r12d, %edx
	movq	32(%r13), %rax
	andw	$1023, %r12w
	sarl	$10, %edx
	orw	$-9216, %r12w
	subw	$10304, %dx
	movw	%r12w, 2(%rax)
	addq	$4, %rax
	movw	%dx, -4(%rax)
	movq	%rax, 32(%r13)
	movb	%bl, 44(%r13)
	cmpb	$1, %bl
	ja	.L700
	movq	%rax, 24(%r13)
.L700:
	subl	$2, 40(%r13)
	movl	$1, %eax
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L686:
	movzwl	28(%rdi), %eax
	sarl	$3, %esi
	movq	32(%rdi), %rdi
	addl	%esi, %r12d
	subl	%eax, %r12d
	movq	8(%rdi), %r14
	cmpl	$65535, %r12d
	ja	.L701
	movl	%r12d, %eax
	movq	(%rdi), %rdx
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%r12d, %edx
	andl	$63, %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
.L702:
	movzwl	(%r14,%rax), %esi
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L744:
	addw	$4519, %si
	movl	$6, %edx
	movw	%si, -58(%rbp)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L706:
	cmpl	$1, %eax
	jle	.L748
.L708:
	movl	%r12d, %ecx
	movq	32(%r13), %rdx
	andw	$1023, %r12w
	subl	$2, %eax
	sarl	$10, %ecx
	orw	$-9216, %r12w
	subw	$10304, %cx
	movw	%r12w, 2(%rdx)
	movw	%cx, (%rdx)
	leaq	4(%rdx), %rcx
	movq	%rcx, %xmm0
	movl	%eax, 40(%r13)
	movl	$1, %eax
	punpcklqdq	%xmm0, %xmm0
	movb	$0, 44(%r13)
	movups	%xmm0, 24(%r13)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L701:
	cmpl	$1114111, %r12d
	ja	.L703
	cmpl	24(%rdi), %r12d
	jl	.L704
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L689:
	movq	%r8, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	jne	.L692
.L698:
	xorl	%eax, %eax
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L703:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%r8, %rdx
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	jne	.L699
	xorl	%eax, %eax
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L715:
	movq	%r8, %rdx
	movl	%r15d, %esi
	movq	%r13, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movq	-72(%rbp), %r8
	testb	%al, %al
	jne	.L716
	xorl	%eax, %eax
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L743:
	movq	%r8, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L698
	movl	40(%r13), %eax
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L748:
	movq	%r8, %rdx
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L698
	movl	40(%r13), %eax
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L747:
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L741:
	movzwl	%r12w, %esi
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6716ReorderingBuffer6insertEih
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L704:
	movl	%r12d, %esi
	movq	%r8, -72(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	-72(%rbp), %r8
	cltq
	addq	%rax, %rax
	jmp	.L702
.L745:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3233:
	.size	_ZNK6icu_6715Normalizer2Impl9decomposeEitRNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl9decomposeEitRNS_16ReorderingBufferER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode:
.LFB3231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -56(%rbp)
	movzwl	8(%rdi), %r15d
	testq	%rdx, %rdx
	je	.L809
.L780:
	movq	%r9, -64(%rbp)
	movb	$0, -81(%rbp)
.L755:
	cmpq	%r13, %r9
	je	.L779
	movzwl	(%r9), %ecx
	movq	%r9, %rbx
	movl	%ecx, %eax
	.p2align 4,,10
	.p2align 3
.L756:
	movl	%ecx, %esi
	cmpl	%ecx, %r15d
	jg	.L758
.L811:
	movq	32(%r14), %rdi
	movl	%ecx, %edx
	andl	$63, %eax
	sarl	$6, %edx
	movq	(%rdi), %rcx
	movslq	%edx, %rdx
	movq	8(%rdi), %r12
	movzwl	(%rcx,%rdx,2), %edx
	addl	%edx, %eax
	movzwl	14(%r14), %edx
	cltq
	movzwl	(%r12,%rax,2), %r8d
	cmpw	%dx, %r8w
	jb	.L758
	movl	%r8d, %eax
	andb	$-3, %ah
	cmpw	$-1024, %ax
	je	.L758
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L805
	leaq	2(%rbx), %r8
	cmpq	%r13, %r8
	je	.L757
	movzwl	2(%rbx), %ecx
	movl	%ecx, %r11d
	movl	%ecx, %eax
	andl	$64512, %r11d
	cmpl	$56320, %r11d
	je	.L810
	movq	%r8, %rbx
	movl	%ecx, %esi
	cmpl	%ecx, %r15d
	jle	.L811
	.p2align 4,,10
	.p2align 3
.L758:
	addq	$2, %rbx
.L761:
	cmpq	%r13, %rbx
	je	.L757
	movzwl	(%rbx), %ecx
	movl	%ecx, %eax
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L757:
	testq	%r10, %r10
	je	.L782
	movq	-56(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r9, %rsi
	movq	%r10, %rdi
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	testb	%al, %al
	jne	.L812
.L782:
	movq	%r13, -64(%rbp)
.L749:
	movq	-64(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L810:
	.cfi_restore_state
	sall	$10, %esi
	leal	-56613888(%rsi,%rcx), %esi
	cmpl	%esi, 24(%rdi)
	jg	.L763
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
.L764:
	movzwl	(%r12,%rax), %r8d
	cmpw	%dx, %r8w
	jb	.L765
	movl	%r8d, %eax
	andb	$-3, %ah
	cmpw	$-1024, %ax
	je	.L765
.L805:
	movl	%esi, %r12d
	cmpq	%rbx, %r9
	jne	.L813
.L766:
	cmpl	$65536, %r12d
	movzwl	%r8w, %edx
	sbbq	%rax, %rax
	andq	$-2, %rax
	leaq	4(%rbx,%rax), %r9
	testq	%r10, %r10
	je	.L770
	movq	-56(%rbp), %r8
	movq	%r10, %rcx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl9decomposeEitRNS_16ReorderingBufferER10UErrorCode
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r9
	testb	%al, %al
	jne	.L755
.L779:
	movq	%r9, -64(%rbp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L765:
	addq	$4, %rbx
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L770:
	cmpw	14(%r14), %r8w
	jb	.L772
	cmpw	30(%r14), %r8w
	jb	.L749
.L772:
	cmpw	$-1025, %r8w
	jbe	.L780
	sarl	%edx
	cmpb	%dl, -81(%rbp)
	jbe	.L784
	testb	%dl, %dl
	jne	.L749
.L784:
	movq	-64(%rbp), %rax
	cmpb	$2, %dl
	movb	%dl, -81(%rbp)
	cmovb	%r9, %rax
	movq	%rax, -64(%rbp)
	jmp	.L755
.L763:
	movq	%r10, -96(%rbp)
	movq	%r9, -80(%rbp)
	movl	%esi, -72(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movzwl	14(%r14), %edx
	movq	-96(%rbp), %r10
	cltq
	movq	-80(%rbp), %r9
	movl	-72(%rbp), %esi
	addq	%rax, %rax
	jmp	.L764
.L809:
	movq	%rsi, %rdx
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L814:
	cmpl	%eax, %r15d
	jle	.L783
.L751:
	movq	%rdx, %rcx
	movzwl	(%rdx), %eax
	addq	$2, %rdx
	testw	%ax, %ax
	jne	.L814
.L783:
	movq	%rcx, -64(%rbp)
	movq	%rcx, %rdx
	testq	%r10, %r10
	je	.L753
	cmpq	%rcx, %r9
	je	.L753
	movq	-56(%rbp), %rcx
	movq	%r10, %rdi
	movq	%r9, %rsi
	movq	%r10, -72(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movq	-72(%rbp), %r10
.L753:
	movq	-56(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L749
	movq	-64(%rbp), %rbx
	xorl	%esi, %esi
	movq	%r10, -72(%rbp)
	movq	%rbx, %rdi
	call	u_strchr_67@PLT
	movq	-72(%rbp), %r10
	movq	%rbx, %r9
	movq	%rax, %r13
	jmp	.L780
.L812:
	movq	%r13, %rbx
.L768:
	movq	%rbx, -64(%rbp)
	jmp	.L749
.L813:
	testq	%r10, %r10
	je	.L777
	movq	-56(%rbp), %rcx
	movq	%r10, %rdi
	movq	%rbx, %rdx
	movq	%r9, %rsi
	movl	%r8d, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movq	-72(%rbp), %r10
	movl	-80(%rbp), %r8d
	testb	%al, %al
	je	.L815
.L767:
	cmpq	%rbx, %r13
	jne	.L766
	jmp	.L768
.L777:
	movq	%rbx, -64(%rbp)
	movb	$0, -81(%rbp)
	jmp	.L767
.L815:
	movq	%rbx, %r13
	jmp	.L782
	.cfi_endproc
.LFE3231:
	.size	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode:
.LFB3230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movl	%r8d, %esi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rcx, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%r9, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jns	.L817
	testq	%rdx, %rdx
	movq	%r12, %rax
	setne	%dl
	subq	%r14, %rax
	sarq	%rax
	testb	%dl, %dl
	cmovne	%eax, %esi
.L817:
	movzwl	8(%rdi), %edx
	movq	%rdi, %xmm1
	movq	%r13, %xmm0
	movq	$0, -80(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movl	$0, -72(%rbp)
	movl	%edx, %eax
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	movb	$0, -68(%rbp)
	cmovne	%edx, %eax
	movaps	%xmm0, -96(%rbp)
	movw	%ax, 8(%rdi)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L831
	movq	-104(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movl	$27, %edx
	testb	$2, 8(%rcx)
	jne	.L821
	movl	16(%rcx), %edx
.L821:
	movl	%edx, -72(%rbp)
	leaq	-112(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -88(%rbp)
	movb	$0, -68(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L816
	movq	-80(%rbp), %rsi
	movq	-104(%rbp), %rdi
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
.L816:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L832
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L831:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L816
.L832:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3230:
	.size	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode:
.LFB3229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	(%rcx), %eax
	testl	%eax, %eax
	jg	.L844
	movswl	8(%rsi), %r8d
	movq	%rcx, %r9
	testb	$17, %r8b
	jne	.L836
	leaq	10(%rsi), %r10
	testb	$2, %r8b
	je	.L845
.L838:
	cmpq	%r12, %rsi
	je	.L836
	testq	%r10, %r10
	je	.L836
	testw	%r8w, %r8w
	js	.L841
	sarl	$5, %r8d
.L842:
	movslq	%r8d, %rax
	movq	%r12, %rcx
	movq	%r10, %rsi
	leaq	(%r10,%rax,2), %rdx
	call	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_RNS_13UnicodeStringEiR10UErrorCode
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_restore_state
	movq	24(%rsi), %r10
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L841:
	movl	12(%rsi), %r8d
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L836:
	movl	$1, (%r9)
.L844:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3229:
	.size	_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl9decomposeERKNS_13UnicodeStringERS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode:
.LFB3234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L847
	movsbl	%cl, %ecx
	movsbl	%r8b, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3234:
	.size	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi
	.type	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi, @function
_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi:
.LFB3235:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %eax
	cmpl	%esi, %eax
	jg	.L872
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$-1024, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$8, %rsp
	cmpl	$55296, %eax
	je	.L865
	movq	32(%rdi), %rdi
	movq	8(%rdi), %r15
	cmpl	$65535, %esi
	jg	.L853
	movl	%esi, %eax
	movq	(%rdi), %rdx
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%esi, %edx
	andl	$63, %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
.L854:
	movzwl	(%r15,%rax), %esi
	movl	%esi, %eax
.L852:
	xorl	%r8d, %r8d
	cmpw	%ax, 30(%r12)
	jbe	.L849
	cmpw	%ax, 26(%r12)
	ja	.L858
	movzwl	28(%r12), %eax
	sarl	$3, %esi
	movq	32(%r12), %rdi
	addl	%esi, %ebx
	subl	%eax, %ebx
	movq	8(%rdi), %r15
	cmpl	$65535, %ebx
	ja	.L859
	movl	%ebx, %eax
	movq	(%rdi), %rdx
	movl	$1, (%r14)
	sarl	$6, %eax
	movw	%bx, 0(%r13)
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%ebx, %edx
	andl	$63, %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
.L860:
	movzwl	(%r15,%rax), %eax
	movq	%r13, %r8
.L858:
	cmpw	%ax, 14(%r12)
	ja	.L849
	je	.L863
	movzwl	16(%r12), %edx
	orl	$1, %edx
	cmpw	%dx, %ax
	je	.L863
	andl	$65534, %eax
	addq	48(%r12), %rax
	movzwl	(%rax), %edx
	leaq	2(%rax), %r8
	andl	$31, %edx
	movl	%edx, (%r14)
.L849:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L865:
	.cfi_restore_state
	movl	$1, %esi
	movl	$1, %eax
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L853:
	cmpl	$1114111, %esi
	jg	.L855
	cmpl	24(%rdi), %esi
	jl	.L856
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L859:
	movl	%ebx, %eax
	sarl	$10, %eax
	subw	$10304, %ax
	movw	%ax, 0(%r13)
	movl	%ebx, %eax
	andw	$1023, %ax
	movl	$2, (%r14)
	orw	$-9216, %ax
	movw	%ax, 2(%r13)
	cmpl	$1114111, %ebx
	ja	.L861
	cmpl	24(%rdi), %ebx
	jl	.L862
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L855:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L863:
	subl	$44032, %ebx
	movslq	%ebx, %rax
	movl	%ebx, %esi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %esi
	shrq	$32, %rax
	addl	%ebx, %eax
	sarl	$4, %eax
	movl	%eax, %edx
	subl	%esi, %edx
	imull	$28, %edx, %ecx
	movslq	%edx, %rdx
	movq	%rdx, %rax
	imulq	$818089009, %rdx, %rdx
	movl	%eax, %esi
	sarl	$31, %esi
	sarq	$34, %rdx
	subl	%esi, %edx
	leal	4352(%rdx), %esi
	movw	%si, 0(%r13)
	leal	(%rdx,%rdx,4), %esi
	leal	(%rdx,%rsi,4), %edx
	subl	%edx, %eax
	addw	$4449, %ax
	subl	%ecx, %ebx
	movw	%ax, 2(%r13)
	movl	%ebx, %edx
	movl	$2, %eax
	je	.L864
	addw	$4519, %dx
	movl	$3, %eax
	movw	%dx, 4(%r13)
.L864:
	movl	%eax, (%r14)
	movq	%r13, %r8
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L872:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L861:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L862:
	movl	%ebx, %esi
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L856:
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L854
	.cfi_endproc
.LFE3235:
	.size	_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi, .-_ZNK6icu_6715Normalizer2Impl16getDecompositionEiPDsRi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl19getRawDecompositionEiPDsRi
	.type	_ZNK6icu_6715Normalizer2Impl19getRawDecompositionEiPDsRi, @function
_ZNK6icu_6715Normalizer2Impl19getRawDecompositionEiPDsRi:
.LFB3236:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %eax
	cmpl	%esi, %eax
	jg	.L895
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$-1024, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpl	$55296, %eax
	je	.L890
	movq	32(%rdi), %rdi
	movq	8(%rdi), %r14
	cmpl	$65535, %esi
	jg	.L877
	movl	%esi, %eax
	movq	(%rdi), %rdx
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%esi, %edx
	andl	$63, %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
.L878:
	movzwl	(%r14,%rax), %ecx
	movzwl	14(%rbx), %edx
	movl	%ecx, %eax
	cmpw	%ax, %dx
	ja	.L881
.L897:
	cmpw	%ax, 30(%rbx)
	jbe	.L881
	cmpw	%ax, %dx
	je	.L882
	movzwl	16(%rbx), %edx
	orl	$1, %edx
	cmpw	%ax, %dx
	je	.L882
	cmpw	%ax, 26(%rbx)
	jbe	.L896
	movq	48(%rbx), %rax
	sarl	%ecx
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,2), %rcx
	movzwl	(%rcx), %edx
	movl	%edx, %ebx
	andl	$31, %ebx
	testb	$64, %dl
	je	.L888
	shrl	$7, %edx
	movq	%rcx, %rax
	andl	$1, %edx
	addq	%rdx, %rdx
	subq	%rdx, %rax
	movzwl	-2(%rax), %edx
	subq	$2, %rax
	cmpw	$31, %dx
	ja	.L889
	movzwl	%dx, %ecx
	addq	%rdx, %rdx
	movl	%ecx, 0(%r13)
	subq	%rdx, %rax
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L890:
	movzwl	14(%rbx), %edx
	movl	$1, %eax
	movl	$1, %ecx
	cmpw	%ax, %dx
	jbe	.L897
.L881:
	xorl	%eax, %eax
.L873:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L895:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	cmpl	$1114111, %esi
	jg	.L879
	cmpl	24(%rdi), %esi
	jl	.L880
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L879:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L882:
	leal	-44032(%rsi), %ecx
	movslq	%ecx, %rax
	movl	%ecx, %edi
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %edi
	shrq	$32, %rax
	addl	%ecx, %eax
	sarl	$4, %eax
	movl	%eax, %edx
	subl	%edi, %edx
	imull	$28, %edx, %edx
	subl	%edx, %ecx
	je	.L898
	subl	%ecx, %esi
	leal	4519(%rcx), %eax
.L886:
	movw	%ax, 2(%r12)
	movq	%r12, %rax
	movw	%si, (%r12)
	movl	$2, 0(%r13)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L898:
	subl	%edi, %eax
	movslq	%eax, %rdx
	movl	%eax, %ecx
	imulq	$818089009, %rdx, %rdx
	sarl	$31, %ecx
	sarq	$34, %rdx
	subl	%ecx, %edx
	leal	(%rdx,%rdx,4), %ecx
	leal	4352(%rdx), %esi
	leal	(%rdx,%rcx,4), %edx
	subl	%edx, %eax
	addw	$4449, %ax
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L888:
	movl	%ebx, 0(%r13)
	leaq	2(%rcx), %rax
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L880:
	movl	%esi, -36(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movl	-36(%rbp), %esi
	cltq
	addq	%rax, %rax
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L896:
	movzwl	28(%rbx), %eax
	sarl	$3, %ecx
	addl	%ecx, %esi
	subl	%eax, %esi
	cmpl	$65535, %esi
	ja	.L887
	movl	$1, 0(%r13)
	movq	%r12, %rax
	movw	%si, (%r12)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L887:
	movl	%esi, %eax
	andw	$1023, %si
	sarl	$10, %eax
	orw	$-9216, %si
	subw	$10304, %ax
	movw	%ax, (%r12)
	movq	%r12, %rax
	movl	$2, 0(%r13)
	movw	%si, 2(%r12)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L889:
	movw	%dx, (%r12)
	leaq	6(%rcx), %rsi
	leal	-2(%rbx), %edx
	subl	$1, %ebx
	leaq	2(%r12), %rdi
	call	u_memcpy_67@PLT
	movl	%ebx, 0(%r13)
	movq	%r12, %rax
	jmp	.L873
	.cfi_endproc
.LFE3236:
	.size	_ZNK6icu_6715Normalizer2Impl19getRawDecompositionEiPDsRi, .-_ZNK6icu_6715Normalizer2Impl19getRawDecompositionEiPDsRi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl18decomposeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl18decomposeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl18decomposeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode:
.LFB3237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r8, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	24(%r9), %rbx
	movq	32(%r9), %r9
	movl	%ecx, -64(%rbp)
	movq	16(%rbp), %rax
	movq	%r8, -72(%rbp)
	subq	%rbx, %r9
	sarq	%r9
	movq	%rax, -56(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %r9
	movswl	8(%rdi), %edx
	testw	%dx, %dx
	js	.L900
	sarl	$5, %edx
.L901:
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	xorl	%esi, %esi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	cmpb	$0, -64(%rbp)
	jne	.L902
	cmpq	%r12, %r14
	je	.L924
	movq	32(%r13), %rdi
	movq	%r14, %rbx
	xorl	%r11d, %r11d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L939:
	sarl	%eax
.L911:
	testb	%al, %al
	je	.L937
	testb	%cl, %cl
	cmovne	%eax, %r8d
	xorl	%ecx, %ecx
	cmpq	%r12, %rdx
	je	.L938
	movq	%rdx, %rbx
	movl	%eax, %r11d
.L904:
	movzwl	(%rbx), %esi
	leaq	2(%rbx), %rdx
	movl	%esi, %r9d
	movl	%esi, %eax
	andl	$63488, %r9d
	cmpl	$55296, %r9d
	je	.L905
	movq	(%rdi), %r9
	sarl	$6, %esi
	andl	$63, %eax
	movslq	%esi, %rsi
	movzwl	(%r9,%rsi,2), %esi
	addl	%esi, %eax
.L906:
	movq	8(%rdi), %rsi
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpw	$-1025, %ax
	ja	.L939
	cmpw	18(%r13), %ax
	jb	.L937
	cmpw	26(%r13), %ax
	jnb	.L937
	movq	48(%r13), %rsi
	sarl	%eax
	cltq
	leaq	(%rsi,%rax,2), %rax
	testb	$-128, (%rax)
	je	.L937
	movzbl	-2(%rax), %eax
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L937:
	movzbl	%r11b, %r13d
	movzbl	%r8b, %r8d
.L903:
	testq	%r12, %r12
	je	.L940
.L918:
	movq	%rbx, %r11
	subq	%r14, %r11
	sarq	%r11
	testl	%r11d, %r11d
	je	.L921
.L919:
	cmpl	40(%r15), %r11d
	jg	.L920
.L923:
	subq	$8, %rsp
	pushq	-56(%rbp)
	xorl	%ecx, %ecx
	movl	%r11d, %edx
	movl	%r13d, %r9d
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6716ReorderingBuffer6appendEPKDsiahhR10UErrorCode.part.0
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	jne	.L921
.L899:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L905:
	.cfi_restore_state
	testb	$4, %ah
	jne	.L907
	cmpq	%rdx, %r12
	je	.L907
	movzwl	2(%rbx), %eax
	movl	%eax, %r9d
	andl	$-1024, %r9d
	cmpl	$56320, %r9d
	je	.L941
.L907:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L900:
	movl	12(%rdi), %edx
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L938:
	movzbl	%al, %r13d
	movzbl	%r8b, %r8d
	movq	%r12, %rbx
	testq	%r12, %r12
	jne	.L918
.L940:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	%r8d, -64(%rbp)
	call	u_strchr_67@PLT
	movq	%rbx, %r11
	movl	-64(%rbp), %r8d
	subq	%r14, %r11
	movq	%rax, %r12
	sarq	%r11
	testl	%r11d, %r11d
	jne	.L919
.L921:
	movq	-56(%rbp), %rcx
	leaq	-40(%rbp), %rsp
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	.p2align 4,,10
	.p2align 3
.L941:
	.cfi_restore_state
	sall	$10, %esi
	leaq	4(%rbx), %rdx
	leal	-56613888(%rax,%rsi), %esi
	cmpl	%esi, 24(%rdi)
	jg	.L942
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L902:
	movq	-56(%rbp), %r8
	leaq	-40(%rbp), %rsp
	movq	%r15, %rcx
	movq	%r12, %rdx
	popq	%rbx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6715Normalizer2Impl9decomposeEPKDsS2_PNS_16ReorderingBufferER10UErrorCode
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_restore_state
	movq	-56(%rbp), %rdx
	movl	%r11d, %esi
	movq	%r15, %rdi
	movl	%r8d, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movq	-64(%rbp), %r11
	movl	-72(%rbp), %r8d
	testb	%al, %al
	jne	.L923
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L942:
	movb	%cl, -81(%rbp)
	movb	%r11b, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movb	%r8b, -64(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%r13), %rdi
	movzbl	-81(%rbp), %ecx
	movzbl	-80(%rbp), %r11d
	movq	-72(%rbp), %rdx
	movzbl	-64(%rbp), %r8d
	jmp	.L906
.L924:
	movq	%r14, %rbx
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	jmp	.L903
	.cfi_endproc
.LFE3237:
	.size	_ZNK6icu_6715Normalizer2Impl18decomposeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl18decomposeAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl23hasDecompBoundaryBeforeEi
	.type	_ZNK6icu_6715Normalizer2Impl23hasDecompBoundaryBeforeEi, @function
_ZNK6icu_6715Normalizer2Impl23hasDecompBoundaryBeforeEi:
.LFB3238:
	.cfi_startproc
	endbr64
	movzwl	12(%rdi), %eax
	cmpl	%esi, %eax
	jle	.L944
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$65535, %esi
	jg	.L946
	movl	%esi, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L947
	movl	%esi, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L947
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L971
	movq	32(%rbx), %rdx
	movl	%esi, %eax
	andl	$63, %esi
	sarl	$6, %eax
	movq	8(%rdx), %r12
	movq	(%rdx), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
.L950:
	movzwl	(%r12,%rax), %eax
	movl	%eax, %edx
.L954:
	cmpw	%dx, 22(%rbx)
	ja	.L947
	cmpw	%dx, 26(%rbx)
	jbe	.L972
	movq	48(%rbx), %rdx
	sarl	%eax
	cltq
	leaq	(%rdx,%rax,2), %rax
	testb	$-128, (%rax)
	je	.L947
	testw	$-256, -2(%rax)
	sete	%al
.L952:
	testb	%al, %al
	je	.L943
	.p2align 4,,10
	.p2align 3
.L947:
	movl	$1, %eax
.L943:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L946:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	movq	8(%rdi), %r12
	cmpl	$1114111, %esi
	jg	.L973
	cmpl	24(%rdi), %esi
	jl	.L949
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L972:
	cmpw	$-1024, %dx
	setbe	%al
	cmpw	$-512, %dx
	sete	%dl
	orl	%edx, %eax
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L949:
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L973:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L971:
	movl	$1, %eax
	movl	$1, %edx
	jmp	.L954
	.cfi_endproc
.LFE3238:
	.size	_ZNK6icu_6715Normalizer2Impl23hasDecompBoundaryBeforeEi, .-_ZNK6icu_6715Normalizer2Impl23hasDecompBoundaryBeforeEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl29norm16HasDecompBoundaryBeforeEt
	.type	_ZNK6icu_6715Normalizer2Impl29norm16HasDecompBoundaryBeforeEt, @function
_ZNK6icu_6715Normalizer2Impl29norm16HasDecompBoundaryBeforeEt:
.LFB3239:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpw	%si, 22(%rdi)
	ja	.L974
	cmpw	%si, 26(%rdi)
	jbe	.L981
	andl	$65534, %esi
	addq	48(%rdi), %rsi
	testb	$-128, (%rsi)
	je	.L974
	testw	$-256, -2(%rsi)
	sete	%al
.L974:
	ret
	.p2align 4,,10
	.p2align 3
.L981:
	cmpw	$-1024, %si
	setbe	%al
	cmpw	$-512, %si
	sete	%dl
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE3239:
	.size	_ZNK6icu_6715Normalizer2Impl29norm16HasDecompBoundaryBeforeEt, .-_ZNK6icu_6715Normalizer2Impl29norm16HasDecompBoundaryBeforeEt
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl22hasDecompBoundaryAfterEi
	.type	_ZNK6icu_6715Normalizer2Impl22hasDecompBoundaryAfterEi, @function
_ZNK6icu_6715Normalizer2Impl22hasDecompBoundaryAfterEi:
.LFB3240:
	.cfi_startproc
	endbr64
	movzwl	8(%rdi), %eax
	cmpl	%esi, %eax
	jg	.L1011
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$65535, %esi
	jg	.L985
	movl	%esi, %eax
	movq	56(%rdi), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L986
	movl	%esi, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L986
	movl	%esi, %eax
	movl	$1, %ecx
	movl	$1, %edx
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L994
	movq	32(%rbx), %rdx
	movl	%esi, %eax
	andl	$63, %esi
	sarl	$6, %eax
	movq	8(%rdx), %r12
	movq	(%rdx), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
.L989:
	movzwl	(%r12,%rax), %ecx
	movl	%ecx, %edx
.L994:
	cmpw	%dx, 14(%rbx)
	jnb	.L986
	movzwl	16(%rbx), %eax
	orl	$1, %eax
	cmpw	%dx, %ax
	je	.L986
	cmpw	%dx, 26(%rbx)
	ja	.L990
	cmpw	30(%rbx), %dx
	jb	.L991
	cmpw	$-1024, %dx
	setbe	%al
	cmpw	$-512, %dx
	sete	%dl
	orl	%edx, %eax
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L985:
	movq	32(%rdi), %rdi
	movq	8(%rdi), %r12
	cmpl	$1114111, %esi
	jg	.L1012
	cmpl	24(%rdi), %esi
	jl	.L988
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L990:
	movq	48(%rbx), %rax
	sarl	%ecx
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,2), %rcx
	xorl	%eax, %eax
	movzwl	(%rcx), %edx
	cmpw	$511, %dx
	ja	.L982
	cmpw	$255, %dx
	ja	.L1013
	.p2align 4,,10
	.p2align 3
.L986:
	movl	$1, %eax
.L982:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1011:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1012:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L991:
	andl	$6, %edx
	cmpw	$2, %dx
	setbe	%al
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1013:
	andl	$128, %edx
	movl	$1, %eax
	je	.L982
	testw	$-256, -2(%rcx)
	sete	%al
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L988:
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L989
	.cfi_endproc
.LFE3240:
	.size	_ZNK6icu_6715Normalizer2Impl22hasDecompBoundaryAfterEi, .-_ZNK6icu_6715Normalizer2Impl22hasDecompBoundaryAfterEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl28norm16HasDecompBoundaryAfterEt
	.type	_ZNK6icu_6715Normalizer2Impl28norm16HasDecompBoundaryAfterEt, @function
_ZNK6icu_6715Normalizer2Impl28norm16HasDecompBoundaryAfterEt:
.LFB3241:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpw	%si, 14(%rdi)
	jnb	.L1014
	movzwl	16(%rdi), %edx
	orl	$1, %edx
	cmpw	%dx, %si
	je	.L1014
	cmpw	%si, 26(%rdi)
	ja	.L1016
	cmpw	30(%rdi), %si
	jb	.L1017
	cmpw	$-1024, %si
	setbe	%al
	cmpw	$-512, %si
	sete	%dl
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1016:
	andl	$65534, %esi
	addq	48(%rdi), %rsi
	xorl	%eax, %eax
	movzwl	(%rsi), %edx
	cmpw	$511, %dx
	ja	.L1014
	movl	$1, %eax
	cmpw	$255, %dx
	jbe	.L1014
	andl	$128, %edx
	je	.L1014
	testw	$-256, -2(%rsi)
	sete	%al
.L1014:
	ret
	.p2align 4,,10
	.p2align 3
.L1017:
	andl	$6, %esi
	cmpw	$2, %si
	setbe	%al
	ret
	.cfi_endproc
.LFE3241:
	.size	_ZNK6icu_6715Normalizer2Impl28norm16HasDecompBoundaryAfterEt, .-_ZNK6icu_6715Normalizer2Impl28norm16HasDecompBoundaryAfterEt
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715Normalizer2Impl7combineEPKti
	.type	_ZN6icu_6715Normalizer2Impl7combineEPKti, @function
_ZN6icu_6715Normalizer2Impl7combineEPKti:
.LFB3242:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %edx
	cmpl	$13311, %esi
	jg	.L1026
	leal	(%rsi,%rsi), %eax
	cmpw	%dx, %ax
	jbe	.L1027
	.p2align 4,,10
	.p2align 3
.L1028:
	andl	$1, %edx
	addl	$2, %edx
	movslq	%edx, %rdx
	leaq	(%rdi,%rdx,2), %rdi
	movzwl	(%rdi), %edx
	cmpw	%dx, %ax
	ja	.L1028
.L1027:
	movl	%edx, %ecx
	andw	$32766, %cx
	cmpw	%cx, %ax
	jne	.L1037
	andl	$1, %edx
	movzwl	2(%rdi), %eax
	je	.L1025
	movzwl	4(%rdi), %edx
	sall	$16, %eax
	orl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1026:
	movl	%esi, %eax
	sall	$6, %esi
	sarl	$9, %eax
	andl	$-2, %eax
	addw	$13312, %ax
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1042:
	andl	$1, %edx
	addl	$2, %edx
	movslq	%edx, %rdx
	leaq	(%rdi,%rdx,2), %rdi
.L1032:
	movzwl	(%rdi), %edx
.L1030:
	cmpw	%dx, %ax
	ja	.L1042
	movl	%edx, %ecx
	andw	$32766, %cx
	cmpw	%cx, %ax
	jne	.L1037
	movzwl	2(%rdi), %ecx
	cmpw	%cx, %si
	jbe	.L1033
	testw	%dx, %dx
	js	.L1037
	addq	$6, %rdi
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1037:
	movl	$-1, %eax
.L1025:
	ret
	.p2align 4,,10
	.p2align 3
.L1033:
	movzwl	%cx, %eax
	andl	$-64, %ecx
	cmpw	%cx, %si
	jne	.L1037
	movzwl	4(%rdi), %edx
	sall	$16, %eax
	andl	$4128768, %eax
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE3242:
	.size	_ZN6icu_6715Normalizer2Impl7combineEPKti, .-_ZN6icu_6715Normalizer2Impl7combineEPKti
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl13addCompositesEPKtRNS_10UnicodeSetE
	.type	_ZNK6icu_6715Normalizer2Impl13addCompositesEPKtRNS_10UnicodeSetE, @function
_ZNK6icu_6715Normalizer2Impl13addCompositesEPKtRNS_10UnicodeSetE:
.LFB3243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
.L1051:
	movzwl	2(%r13), %eax
	movzwl	0(%r13), %r14d
	movl	%eax, %edx
	testb	$1, %r14b
	jne	.L1044
	movl	%eax, %r15d
	addq	$4, %r13
	sarl	%r15d
	andl	$1, %edx
	jne	.L1059
.L1046:
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	testw	%r14w, %r14w
	jns	.L1051
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1044:
	.cfi_restore_state
	movzwl	4(%r13), %edx
	sall	$16, %eax
	addq	$6, %r13
	andl	$4128768, %eax
	orl	%edx, %eax
	movl	%eax, %r15d
	sarl	%r15d
	testb	$1, %al
	je	.L1046
	movq	32(%r12), %rdi
	movq	8(%rdi), %rdx
	cmpl	$65535, %r15d
	jle	.L1052
	cmpl	$1114111, %r15d
	jg	.L1049
	cmpl	24(%rdi), %r15d
	jl	.L1050
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	32(%r12), %rdi
	movq	8(%rdi), %rdx
.L1052:
	movq	(%rdi), %rcx
	sarl	$7, %eax
	cltq
	movzwl	(%rcx,%rax,2), %eax
	movl	%r15d, %ecx
	andl	$63, %ecx
	addl	%ecx, %eax
	cltq
	addq	%rax, %rax
.L1048:
	movzwl	(%rdx,%rax), %eax
	movq	%r12, %rdi
	andl	$65534, %eax
	addq	48(%r12), %rax
	movzwl	(%rax), %edx
	andl	$31, %edx
	leaq	2(%rax,%rdx,2), %rsi
	movq	%rbx, %rdx
	call	_ZNK6icu_6715Normalizer2Impl13addCompositesEPKtRNS_10UnicodeSetE
	jmp	.L1046
	.p2align 4,,10
	.p2align 3
.L1049:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1050:
	movl	%r15d, %esi
	movq	%rdx, -56(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	-56(%rbp), %rdx
	cltq
	addq	%rax, %rax
	jmp	.L1048
	.cfi_endproc
.LFE3243:
	.size	_ZNK6icu_6715Normalizer2Impl13addCompositesEPKtRNS_10UnicodeSetE, .-_ZNK6icu_6715Normalizer2Impl13addCompositesEPKtRNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl9recomposeERNS_16ReorderingBufferEia
	.type	_ZNK6icu_6715Normalizer2Impl9recomposeERNS_16ReorderingBufferEia, @function
_ZNK6icu_6715Normalizer2Impl9recomposeERNS_16ReorderingBufferEia:
.LFB3244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %rax
	movb	%cl, -56(%rbp)
	movq	%rsi, -80(%rbp)
	leaq	(%rax,%rdx,2), %rcx
	movq	32(%rsi), %rax
	cmpq	%rax, %rcx
	je	.L1060
	xorl	%r15d, %r15d
	movq	32(%rdi), %r12
	movq	%rdi, %r14
	movb	$0, -72(%rbp)
	movq	%rcx, %rdi
	movq	$0, -64(%rbp)
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movq	%rax, %r15
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1167:
	leal	-2(%r8), %eax
	cmpw	$-1027, %ax
	ja	.L1118
	cmpw	%r11w, %r8w
	jnb	.L1108
	movq	48(%r14), %rax
	sarl	%r10d
	movslq	%r10d, %r10
	leaq	(%rax,%r10,2), %rdx
.L1109:
	cmpl	$65535, %esi
	ja	.L1110
	leaq	-2(%rbx), %rax
	movb	$0, -72(%rbp)
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L1082:
	movl	%r9d, %ecx
	movq	%rbx, %rdi
.L1062:
	movzwl	(%rdi), %esi
	leaq	2(%rdi), %rbx
	movl	%esi, %r8d
	movl	%esi, %eax
	andl	$63488, %r8d
	cmpl	$55296, %r8d
	je	.L1063
	movl	%esi, %edi
	movq	(%r12), %r8
	andl	$63, %eax
	sarl	$6, %edi
	movslq	%edi, %rdi
	movzwl	(%r8,%rdi,2), %edi
	addl	%edi, %eax
.L1064:
	movq	8(%r12), %r13
	cltq
	movzwl	30(%r14), %r11d
	movzwl	0(%r13,%rax,2), %r10d
	movl	$0, %eax
	movl	%r10d, %r9d
	movq	%r10, %r8
	sarl	%r9d
	cmpw	$-1025, %r10w
	cmovbe	%eax, %r9d
	cmpw	$-512, %r10w
	setbe	%dil
	cmpw	%r11w, %r10w
	setnb	%al
	testb	%al, %dil
	je	.L1069
	testq	%rdx, %rdx
	jne	.L1166
.L1069:
	cmpq	%r15, %rbx
	je	.L1164
	testb	%r9b, %r9b
	je	.L1167
	cmpb	$0, -56(%rbp)
	movl	$0, %eax
	cmovne	%rax, %rdx
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1063:
	testb	$4, %ah
	jne	.L1065
	cmpq	%rbx, %r15
	je	.L1065
	movzwl	2(%rdi), %eax
	movl	%eax, %r8d
	andl	$-1024, %r8d
	cmpl	$56320, %r8d
	je	.L1168
.L1065:
	movl	20(%r12), %eax
	subl	$1, %eax
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1166:
	cmpb	%r9b, %cl
	jb	.L1119
	testb	%cl, %cl
	jne	.L1069
.L1119:
	cmpw	$-512, %r8w
	jne	.L1071
	cmpl	$4518, %esi
	jg	.L1072
	movq	-64(%rbp), %rax
	movzwl	(%rax), %eax
	movw	%ax, -88(%rbp)
	subw	$4352, %ax
	cmpw	$18, %ax
	jbe	.L1169
.L1072:
	cmpq	%r15, %rbx
	je	.L1164
	movl	%ecx, %r9d
	xorl	%edx, %edx
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1164:
	movq	%r15, %rdx
.L1081:
	movq	-80(%rbp), %rcx
	movq	%rdx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	32(%rcx), %rax
	movb	$0, 44(%rcx)
	movups	%xmm0, 24(%rcx)
	movq	%rax, -56(%rbp)
	subq	%rdx, %rax
	sarq	%rax
	addl	%eax, 40(%rcx)
.L1060:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1110:
	.cfi_restore_state
	leaq	-4(%rbx), %rax
	movb	$1, -72(%rbp)
	movq	%rax, -64(%rbp)
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	40(%r14), %rax
	subq	%r11, %r8
	leaq	(%rax,%r8,2), %rdx
	testq	%rdx, %rdx
	je	.L1082
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1118:
	xorl	%edx, %edx
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1071:
	movq	%rdx, %rdi
	movl	%esi, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movb	%cl, -97(%rbp)
	call	_ZN6icu_6715Normalizer2Impl7combineEPKti
	movq	-88(%rbp), %rdx
	movl	-96(%rbp), %esi
	testl	%eax, %eax
	movl	%eax, -104(%rbp)
	js	.L1069
	movl	%eax, %r8d
	movl	%eax, %edx
	movzbl	-97(%rbp), %ecx
	sarl	%r8d
	cmpl	$65536, %esi
	sbbq	%rax, %rax
	andl	$2, %eax
	cmpb	$0, -72(%rbp)
	leaq	-4(%rbx,%rax), %r11
	leal	-65536(%r8), %eax
	jne	.L1170
	cmpl	$1048575, %eax
	jbe	.L1171
	movq	-64(%rbp), %rax
	movw	%r8w, (%rax)
.L1086:
	cmpq	%r11, %rbx
	ja	.L1172
.L1097:
	cmpq	%r15, %rbx
	je	.L1164
	movl	%ecx, %r9d
	xorl	%edx, %edx
	testb	$1, -104(%rbp)
	je	.L1082
	cmpl	$65535, %r8d
	jle	.L1173
	cmpl	$1114111, %r8d
	jg	.L1105
	cmpl	24(%r12), %r8d
	jl	.L1106
	movl	20(%r12), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
.L1104:
	movzwl	0(%r13,%rax), %eax
	movl	%ecx, %r9d
	andl	$65534, %eax
	addq	48(%r14), %rax
	movzwl	(%rax), %edx
	andl	$31, %edx
	leaq	2(%rax,%rdx,2), %rdx
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1168:
	sall	$10, %esi
	leaq	4(%rdi), %rbx
	leal	-56613888(%rax,%rsi), %esi
	cmpl	%esi, 24(%r12)
	jg	.L1174
	movl	20(%r12), %eax
	subl	$2, %eax
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1170:
	cmpl	$1048575, %eax
	ja	.L1085
	movl	%edx, %eax
	movq	-64(%rbp), %rdx
	sarl	$11, %eax
	subw	$10304, %ax
	movw	%ax, (%rdx)
	movl	%r8d, %eax
	andw	$1023, %ax
	orw	$-9216, %ax
	movw	%ax, 2(%rdx)
	jmp	.L1086
.L1169:
	movzwl	%ax, %eax
	leaq	-2(%rbx), %r9
	leal	(%rax,%rax,4), %edx
	leal	(%rax,%rdx,4), %eax
	leal	-4449(%rsi,%rax), %edx
	leal	0(,%rdx,8), %eax
	subl	%edx, %eax
	leal	-21504(,%rax,4), %eax
	cmpq	%r15, %rbx
	je	.L1073
	movzwl	(%rbx), %edx
	movq	%rbx, %rsi
	subw	$4519, %dx
	cmpw	$27, %dx
	jbe	.L1175
.L1074:
	movq	-64(%rbp), %rdx
	movw	%ax, (%rdx)
	cmpq	%r15, %rsi
	jnb	.L1075
	leaq	14(%rbx), %rax
	leaq	-1(%r15), %rdx
	subq	%rsi, %rdx
	cmpq	%rax, %rsi
	leaq	16(%rsi), %rax
	setnb	%dil
	cmpq	%rax, %r9
	setnb	%al
	orb	%al, %dil
	je	.L1113
	cmpq	$13, %rdx
	jbe	.L1113
	movq	%rdx, %rdi
	xorl	%eax, %eax
	shrq	%rdi
	addq	$1, %rdi
	movq	%rdi, %r8
	shrq	$3, %r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L1078:
	movdqu	(%rsi,%rax), %xmm2
	movups	%xmm2, -2(%rbx,%rax)
	addq	$16, %rax
	cmpq	%r8, %rax
	jne	.L1078
	movq	%rdi, %r8
	andq	$-8, %r8
	leaq	(%r8,%r8), %r10
	leaq	(%r9,%r10), %rax
	addq	%r10, %rsi
	cmpq	%rdi, %r8
	je	.L1080
	movzwl	(%rsi), %edi
	movw	%di, (%rax)
	leaq	2(%rsi), %rdi
	cmpq	%rdi, %r15
	jbe	.L1080
	movzwl	2(%rsi), %edi
	movw	%di, 2(%rax)
	leaq	4(%rsi), %rdi
	cmpq	%rdi, %r15
	jbe	.L1080
	movzwl	4(%rsi), %edi
	movw	%di, 4(%rax)
	leaq	6(%rsi), %rdi
	cmpq	%rdi, %r15
	jbe	.L1080
	movzwl	6(%rsi), %edi
	movw	%di, 6(%rax)
	leaq	8(%rsi), %rdi
	cmpq	%rdi, %r15
	jbe	.L1080
	movzwl	8(%rsi), %edi
	movw	%di, 8(%rax)
	leaq	10(%rsi), %rdi
	cmpq	%rdi, %r15
	jbe	.L1080
	movzwl	10(%rsi), %edi
	movw	%di, 10(%rax)
	leaq	12(%rsi), %rdi
	cmpq	%rdi, %r15
	jbe	.L1080
	movzwl	12(%rsi), %esi
	movw	%si, 12(%rax)
.L1080:
	shrq	%rdx
	movq	%r9, %rbx
	leaq	2(%r9,%rdx,2), %r15
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	%r12, %rdi
	movb	%cl, -97(%rbp)
	movq	%rdx, -96(%rbp)
	movl	%esi, -88(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%r14), %r12
	movzbl	-97(%rbp), %ecx
	movq	-96(%rbp), %rdx
	movl	-88(%rbp), %esi
	jmp	.L1064
.L1172:
	cmpq	%r15, %rbx
	jnb	.L1115
	leaq	15(%rbx), %rax
	leaq	-1(%r15), %r9
	subq	%r11, %rax
	subq	%rbx, %r9
	cmpq	$30, %rax
	jbe	.L1116
	cmpq	$13, %r9
	jbe	.L1116
	movq	%r9, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	addq	$1, %rdx
	movq	%rdx, %rsi
	shrq	$3, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1100:
	movdqu	(%rbx,%rax), %xmm1
	movups	%xmm1, (%r11,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L1100
	movq	%rdx, %rsi
	andq	$-8, %rsi
	leaq	(%rsi,%rsi), %rax
	leaq	(%r11,%rax), %rdi
	addq	%rbx, %rax
	cmpq	%rdx, %rsi
	je	.L1102
	movzwl	(%rax), %edx
	movw	%dx, (%rdi)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L1102
	movzwl	2(%rax), %edx
	movw	%dx, 2(%rdi)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L1102
	movzwl	4(%rax), %edx
	movw	%dx, 4(%rdi)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L1102
	movzwl	6(%rax), %edx
	movw	%dx, 6(%rdi)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L1102
	movzwl	8(%rax), %edx
	movw	%dx, 8(%rdi)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L1102
	movzwl	10(%rax), %edx
	movw	%dx, 10(%rdi)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L1102
	movzwl	12(%rax), %eax
	movw	%ax, 12(%rdi)
.L1102:
	shrq	%r9
	movq	%r11, %rbx
	leaq	2(%r11,%r9,2), %r15
	jmp	.L1097
.L1173:
	movl	-104(%rbp), %eax
	movq	(%r12), %rdx
	andl	$63, %r8d
	sarl	$7, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%r8d, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1104
.L1175:
	leaq	2(%rbx), %rsi
	addl	%edx, %eax
	jmp	.L1074
.L1085:
	movq	-64(%rbp), %rax
	leaq	4(%rax), %rdx
	movw	%r8w, (%rax)
	leaq	2(%rax), %r10
	cmpq	%rdx, %r11
	jbe	.L1091
	movq	%r11, %rsi
	subq	%rax, %rsi
	leaq	-5(%rsi), %r9
	leaq	18(%rax), %rsi
	cmpq	%rsi, %rdx
	movq	%rax, %rsi
	setnb	%dil
	addq	$20, %rsi
	cmpq	%rsi, %r10
	setnb	%sil
	orb	%sil, %dil
	je	.L1144
	cmpq	$13, %r9
	jbe	.L1144
	shrq	%r9
	leaq	1(%r9), %rdi
	movq	%rdi, %r9
	shrq	$3, %r9
	movq	%r9, %rsi
	salq	$4, %rsi
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L1090:
	movdqu	4(%rax), %xmm3
	addq	$16, %rax
	movups	%xmm3, -14(%rax)
	cmpq	%rax, %rsi
	jne	.L1090
	movq	%rdi, %rax
	andq	$-8, %rax
	leaq	(%rax,%rax), %rsi
	leaq	(%r10,%rsi), %r9
	addq	%rsi, %rdx
	cmpq	%rdi, %rax
	je	.L1091
	movzwl	(%rdx), %eax
	movw	%ax, (%r9)
	leaq	2(%rdx), %rax
	cmpq	%rax, %r11
	jbe	.L1091
	movzwl	2(%rdx), %eax
	movw	%ax, 2(%r9)
	leaq	4(%rdx), %rax
	cmpq	%rax, %r11
	jbe	.L1091
	movzwl	4(%rdx), %eax
	movw	%ax, 4(%r9)
	leaq	6(%rdx), %rax
	cmpq	%rax, %r11
	jbe	.L1091
	movzwl	6(%rdx), %eax
	movw	%ax, 6(%r9)
	leaq	8(%rdx), %rax
	cmpq	%rax, %r11
	jbe	.L1091
	movzwl	8(%rdx), %eax
	movw	%ax, 8(%r9)
	leaq	10(%rdx), %rax
	cmpq	%rax, %r11
	jbe	.L1091
	movzwl	10(%rdx), %eax
	movw	%ax, 10(%r9)
	leaq	12(%rdx), %rax
	cmpq	%rax, %r11
	jbe	.L1091
	movzwl	12(%rdx), %eax
	movw	%ax, 12(%r9)
.L1091:
	movb	$0, -72(%rbp)
	subq	$2, %r11
	jmp	.L1086
.L1144:
	movzwl	(%rdx), %eax
	addq	$2, %rdx
	movw	%ax, -4(%rdx)
	cmpq	%rdx, %r11
	ja	.L1144
	jmp	.L1091
.L1171:
	movq	-64(%rbp), %rax
	leaq	2(%r11), %r9
	addq	$2, %rax
	cmpq	%rax, %r11
	jbe	.L1096
	movq	%r11, %rax
	subq	-64(%rbp), %rax
	movb	%cl, -96(%rbp)
	subq	$3, %rax
	movl	%r8d, -88(%rbp)
	shrq	%rax
	movq	%r9, -72(%rbp)
	movq	%rax, %rdi
	leaq	2(%rax,%rax), %rdx
	notq	%rdi
	addq	%rdi, %rdi
	leaq	(%r11,%rdi), %rsi
	addq	%r9, %rdi
	call	memmove@PLT
	movq	-72(%rbp), %r9
	movl	-88(%rbp), %r8d
	movzbl	-96(%rbp), %ecx
.L1096:
	movl	%r8d, %eax
	movq	-64(%rbp), %rdx
	movb	$1, -72(%rbp)
	movq	%r9, %r11
	andw	$1023, %ax
	orw	$-9216, %ax
	movw	%ax, 2(%rdx)
	movl	-104(%rbp), %eax
	sarl	$11, %eax
	subw	$10304, %ax
	movw	%ax, (%rdx)
	jmp	.L1086
.L1116:
	movq	%r11, %rdi
	movq	%rbx, %rsi
	.p2align 4,,10
	.p2align 3
.L1098:
	movsw
	cmpq	%rsi, %r15
	ja	.L1098
	jmp	.L1102
.L1113:
	movq	%r9, %rdi
.L1076:
	movsw
	cmpq	%rsi, %r15
	ja	.L1076
	jmp	.L1080
.L1105:
	movl	20(%r12), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1104
.L1073:
	movq	-64(%rbp), %rcx
	movw	%ax, (%rcx)
.L1075:
	movq	%r9, %rdx
	jmp	.L1081
.L1115:
	movq	%r11, %rdx
	jmp	.L1081
.L1106:
	movq	%r12, %rdi
	movl	%r8d, %esi
	movb	%cl, -88(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%r14), %r12
	movzbl	-88(%rbp), %ecx
	cltq
	addq	%rax, %rax
	jmp	.L1104
	.cfi_endproc
.LFE3244:
	.size	_ZNK6icu_6715Normalizer2Impl9recomposeERNS_16ReorderingBufferEia, .-_ZNK6icu_6715Normalizer2Impl9recomposeERNS_16ReorderingBufferEia
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl11composePairEii
	.type	_ZNK6icu_6715Normalizer2Impl11composePairEii, @function
_ZNK6icu_6715Normalizer2Impl11composePairEii:
.LFB3245:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L1194
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	32(%rdi), %rdi
	movq	8(%rdi), %r14
	cmpl	$65535, %esi
	ja	.L1179
	movl	%esi, %eax
	movq	(%rdi), %rdx
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%esi, %edx
	andl	$63, %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
.L1180:
	movzwl	(%r14,%rax), %eax
	movl	%eax, %edx
	cmpw	$1, %ax
	je	.L1183
	cmpw	%ax, 16(%r12)
	jbe	.L1184
	cmpw	$2, %ax
	je	.L1195
	movzwl	14(%r12), %ecx
	cmpw	%ax, %cx
	je	.L1196
	movq	48(%r12), %rsi
	sarl	%eax
	cltq
	leaq	(%rsi,%rax,2), %rdi
	cmpw	%dx, %cx
	jb	.L1197
	.p2align 4,,10
	.p2align 3
.L1187:
	cmpl	$1114111, %r13d
	ja	.L1183
	movl	%r13d, %esi
	call	_ZN6icu_6715Normalizer2Impl7combineEPKti
	addq	$16, %rsp
	popq	%rbx
	sarl	%eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	cmpl	$1114111, %esi
	ja	.L1181
	cmpl	24(%rdi), %esi
	jl	.L1182
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1184:
	movzwl	30(%r12), %ecx
	cmpw	$-1025, %ax
	ja	.L1183
	cmpw	%ax, %cx
	ja	.L1183
	subl	%ecx, %eax
	movq	40(%r12), %rdx
	sarl	%eax
	cltq
	leaq	(%rdx,%rax,2), %rdi
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1183:
	addq	$16, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1181:
	.cfi_restore_state
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1197:
	movzwl	(%rdi), %eax
	andl	$31, %eax
	leaq	2(%rdi,%rax,2), %rdi
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1195:
	subl	$4449, %r13d
	cmpl	$20, %r13d
	ja	.L1183
	leal	-4352(%rsi), %ebx
	addq	$16, %rsp
	leal	(%rbx,%rbx,4), %eax
	leal	(%rbx,%rax,4), %eax
	popq	%rbx
	popq	%r12
	addl	%r13d, %eax
	popq	%r13
	popq	%r14
	imull	$28, %eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	addl	$44032, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1196:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leal	-4519(%r13), %eax
	subl	$4520, %r13d
	cmpl	$26, %r13d
	ja	.L1183
	addq	$16, %rsp
	addl	%esi, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	.cfi_restore_state
	movl	%esi, -36(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movl	-36(%rbp), %esi
	cltq
	addq	%rax, %rax
	jmp	.L1180
	.cfi_endproc
.LFE3245:
	.size	_ZNK6icu_6715Normalizer2Impl11composePairEii, .-_ZNK6icu_6715Normalizer2Impl11composePairEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl17composeQuickCheckEPKDsS2_aP25UNormalizationCheckResult
	.type	_ZNK6icu_6715Normalizer2Impl17composeQuickCheckEPKDsS2_aP25UNormalizationCheckResult, @function
_ZNK6icu_6715Normalizer2Impl17composeQuickCheckEPKDsS2_aP25UNormalizationCheckResult:
.LFB3247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movb	%cl, -49(%rbp)
	movzwl	10(%rdi), %r15d
	testq	%rdx, %rdx
	je	.L1293
.L1199:
	movq	%rbx, %r9
	movl	%r15d, %ecx
.L1232:
	cmpq	%r13, %rbx
	je	.L1247
	movzwl	(%rbx), %eax
	movl	%eax, %edx
	.p2align 4,,10
	.p2align 3
.L1205:
	movl	%eax, %esi
	leaq	2(%rbx), %r12
	cmpl	%eax, %ecx
	jg	.L1206
.L1295:
	movq	32(%r14), %rdi
	movl	%eax, %r10d
	andl	$63, %edx
	sarl	$6, %r10d
	movq	(%rdi), %r11
	movslq	%r10d, %r10
	movq	8(%rdi), %r15
	movzwl	(%r11,%r10,2), %r10d
	addl	%r10d, %edx
	movzwl	18(%r14), %r10d
	movslq	%edx, %rdx
	movzwl	(%r15,%rdx,2), %edx
	cmpw	%r10w, %dx
	jb	.L1206
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L1207
	cmpq	%r12, %r13
	je	.L1198
	movzwl	2(%rbx), %eax
	movl	%eax, %r11d
	movl	%eax, %edx
	andl	$64512, %r11d
	cmpl	$56320, %r11d
	je	.L1294
	movq	%r12, %rbx
	movl	%eax, %esi
	leaq	2(%rbx), %r12
	cmpl	%eax, %ecx
	jle	.L1295
	.p2align 4,,10
	.p2align 3
.L1206:
	cmpq	%r12, %r13
	je	.L1198
	movzwl	(%r12), %eax
	movq	%r12, %rbx
	movl	%eax, %edx
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1294:
	sall	$10, %esi
	leaq	4(%rbx), %r12
	leal	-56613888(%rsi,%rax), %esi
	cmpl	%esi, 24(%rdi)
	jg	.L1209
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
.L1210:
	movzwl	(%r15,%rax), %edx
	cmpw	%r10w, %dx
	jb	.L1206
	.p2align 4,,10
	.p2align 3
.L1207:
	cmpq	%rbx, %r9
	je	.L1241
	cmpw	22(%r14), %dx
	jb	.L1241
	cmpw	26(%r14), %dx
	jb	.L1212
	cmpw	30(%r14), %dx
	jb	.L1213
.L1212:
	movzwl	-2(%rbx), %eax
	movq	32(%r14), %rdi
	leaq	-2(%rbx), %r15
	movl	%eax, %r10d
	movl	%eax, %esi
	andl	$63488, %r10d
	cmpl	$55296, %r10d
	je	.L1214
	movq	(%rdi), %r9
	sarl	$6, %eax
	andl	$63, %esi
	cltq
	movzwl	(%r9,%rax,2), %eax
	addl	%esi, %eax
.L1215:
	movq	8(%rdi), %rsi
	cltq
	movzwl	(%rsi,%rax,2), %eax
	movl	%eax, %r9d
	movl	%eax, %edi
	andw	$1, %r9w
	je	.L1242
	cmpw	$1, %ax
	je	.L1243
	cmpb	$0, -49(%rbp)
	je	.L1243
	cmpw	26(%r14), %ax
	jb	.L1219
	movl	%eax, %esi
	andl	$6, %esi
	cmpw	$3, %si
	movl	$1, %esi
	cmovnb	%r15, %rbx
	cmovb	%r9d, %edi
	cmovb	%esi, %eax
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1230:
	cmpw	18(%r14), %dx
	jb	.L1251
.L1213:
	movq	%rbx, %r12
	testq	%r8, %r8
	je	.L1198
	movl	$0, (%r8)
.L1198:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1209:
	.cfi_restore_state
	movq	%r8, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movzwl	18(%r14), %r10d
	movq	-80(%rbp), %r8
	cltq
	movl	-72(%rbp), %ecx
	movq	-64(%rbp), %r9
	addq	%rax, %rax
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1293:
	movl	%ecx, %r12d
	movq	%rsi, %rdx
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1296:
	cmpl	%eax, %r15d
	jle	.L1253
.L1200:
	movq	%rdx, %rcx
	movzwl	(%rdx), %eax
	addq	$2, %rdx
	testw	%ax, %ax
	jne	.L1296
.L1253:
	xorl	%esi, %esi
	movq	%rcx, %rdi
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	u_strchr_67@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	cmpq	%rcx, %rbx
	je	.L1199
	movzwl	-2(%rcx), %edx
	movq	%rcx, %rbx
	movl	%edx, %esi
	movl	%edx, %eax
	andl	$64512, %esi
	cmpl	$55296, %esi
	je	.L1199
	movq	32(%r14), %rsi
	sarl	$6, %edx
	andl	$63, %eax
	movslq	%edx, %rdx
	movq	(%rsi), %rdi
	movzwl	(%rdi,%rdx,2), %edx
	addl	%edx, %eax
	movq	8(%rsi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	testb	$1, %al
	je	.L1202
	cmpw	$1, %ax
	je	.L1235
	testb	%r12b, %r12b
	je	.L1235
	cmpw	%ax, 26(%r14)
	ja	.L1203
	andl	$6, %eax
	cmpw	$2, %ax
	jbe	.L1199
.L1202:
	leaq	-2(%rcx), %rbx
	jmp	.L1199
.L1241:
	movl	$1, %eax
	movl	$1, %edi
.L1211:
	cmpw	30(%r14), %dx
	jb	.L1213
	cmpw	$-1025, %dx
	jbe	.L1246
	movzwl	%dx, %esi
	sarl	%esi
	cmpb	$0, -49(%rbp)
	movl	%esi, %r9d
	je	.L1223
	testb	%sil, %sil
	je	.L1223
	cmpw	14(%r14), %di
	jbe	.L1223
	sarl	%eax
	movq	48(%r14), %rdi
	cltq
	cmpb	%sil, 1(%rdi,%rax,2)
	jbe	.L1223
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1297:
	movzwl	%dx, %eax
	sarl	%eax
	cmpb	%al, %r9b
	jbe	.L1250
	testb	%al, %al
	jne	.L1230
.L1250:
	movl	%eax, %r9d
.L1231:
	movq	%r15, %r12
.L1223:
	cmpw	$-511, %dx
	ja	.L1224
	testq	%r8, %r8
	je	.L1247
	movl	$2, (%r8)
.L1224:
	cmpq	%r13, %r12
	je	.L1198
	movzwl	(%r12), %edx
	movq	32(%r14), %rdi
	leaq	2(%r12), %r15
	movl	%edx, %esi
	movl	%edx, %eax
	andl	$63488, %esi
	cmpl	$55296, %esi
	je	.L1225
	movq	(%rdi), %rsi
	sarl	$6, %edx
	andl	$63, %eax
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	addl	%edx, %eax
.L1226:
	movq	8(%rdi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %edx
	cmpw	30(%r14), %dx
	jb	.L1230
	cmpw	$-1025, %dx
	ja	.L1297
	xorl	%r9d, %r9d
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1225:
	testb	$4, %dh
	jne	.L1227
	cmpq	%r13, %r15
	je	.L1227
	movzwl	2(%r12), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	je	.L1228
.L1227:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L1226
.L1246:
	xorl	%r9d, %r9d
	jmp	.L1223
.L1228:
	sall	$10, %edx
	leaq	4(%r12), %r15
	leal	-56613888(%rax,%rdx), %esi
	cmpl	%esi, 24(%rdi)
	jg	.L1229
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L1226
.L1247:
	movq	%rbx, %r12
	jmp	.L1198
.L1214:
	testb	$4, %ah
	je	.L1216
	cmpq	%r15, %r9
	je	.L1216
	movzwl	-4(%rbx), %esi
	movl	%esi, %r9d
	andl	$-1024, %r9d
	cmpl	$55296, %r9d
	je	.L1217
.L1216:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L1215
.L1235:
	movq	%rcx, %rbx
	jmp	.L1199
.L1251:
	movq	%r12, %r9
	movq	%r15, %rbx
	jmp	.L1232
.L1229:
	movq	%r8, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movb	%r9b, -64(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%r14), %rdi
	movq	-80(%rbp), %r8
	movl	-72(%rbp), %ecx
	movzbl	-64(%rbp), %r9d
	jmp	.L1226
.L1242:
	movq	%r15, %rbx
	jmp	.L1211
.L1243:
	movl	%r9d, %edi
	movl	$1, %eax
	jmp	.L1211
.L1203:
	movq	48(%r14), %rdx
	andl	$65534, %eax
	cmpw	$511, (%rax,%rdx)
	jbe	.L1199
	jmp	.L1202
.L1219:
	movl	%eax, %esi
	movq	48(%r14), %r10
	sarl	%esi
	movslq	%esi, %rsi
	cmpw	$512, (%r10,%rsi,2)
	movl	$1, %esi
	cmovnb	%r15, %rbx
	cmovb	%r9d, %edi
	cmovb	%esi, %eax
	jmp	.L1211
.L1217:
	sall	$10, %esi
	leaq	-4(%rbx), %r15
	leal	-56613888(%rax,%rsi), %esi
	cmpl	%esi, 24(%rdi)
	jg	.L1218
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L1215
.L1218:
	movq	%r8, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movl	%edx, -64(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%r14), %rdi
	movq	-80(%rbp), %r8
	movl	-72(%rbp), %ecx
	movl	-64(%rbp), %edx
	jmp	.L1215
	.cfi_endproc
.LFE3247:
	.size	_ZNK6icu_6715Normalizer2Impl17composeQuickCheckEPKDsS2_aP25UNormalizationCheckResult, .-_ZNK6icu_6715Normalizer2Impl17composeQuickCheckEPKDsS2_aP25UNormalizationCheckResult
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKDsS2_
	.type	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKDsS2_, @function
_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKDsS2_:
.LFB3250:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rsi
	je	.L1306
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzwl	(%rsi), %eax
	cmpw	10(%rdi), %ax
	jb	.L1298
	movl	%eax, %r8d
	movq	32(%rdi), %rdi
	movzwl	%ax, %ecx
	andl	$63488, %r8d
	cmpl	$55296, %r8d
	je	.L1300
	movq	(%rdi), %rdx
	sarl	$6, %ecx
	andl	$63, %eax
	movslq	%ecx, %rcx
	movzwl	(%rdx,%rcx,2), %edx
	addl	%edx, %eax
.L1301:
	movq	8(%rdi), %rdx
	cltq
	movl	$1, %r8d
	movzwl	(%rdx,%rax,2), %eax
	cmpw	22(%rbx), %ax
	jb	.L1298
	xorl	%r8d, %r8d
	cmpw	26(%rbx), %ax
	jb	.L1298
	cmpw	30(%rbx), %ax
	setb	%r8b
.L1298:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1300:
	.cfi_restore_state
	testb	$4, %ah
	jne	.L1302
	leaq	2(%rsi), %rax
	cmpq	%rax, %rdx
	je	.L1302
	movzwl	2(%rsi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L1321
.L1302:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1306:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1321:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	sall	$10, %ecx
	leal	-56613888(%rax,%rcx), %esi
	cmpl	%esi, 24(%rdi)
	jg	.L1322
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L1301
.L1322:
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%rbx), %rdi
	jmp	.L1301
	.cfi_endproc
.LFE3250:
	.size	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKDsS2_, .-_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKDsS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKhS2_
	.type	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKhS2_, @function
_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKhS2_:
.LFB3251:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rsi
	je	.L1331
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	(%rsi), %eax
	movq	32(%rdi), %rdi
	movl	%eax, %ecx
	testb	%al, %al
	js	.L1344
.L1325:
	movq	8(%rdi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %edx
	movl	$1, %eax
	cmpw	22(%rbx), %dx
	jb	.L1323
	xorl	%eax, %eax
	cmpw	26(%rbx), %dx
	jb	.L1323
	cmpw	30(%rbx), %dx
	setb	%al
.L1323:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1344:
	.cfi_restore_state
	leaq	1(%rsi), %r8
	cmpq	%r8, %rdx
	je	.L1326
	cmpl	$223, %eax
	jle	.L1327
	cmpl	$239, %eax
	jg	.L1328
	movzbl	1(%rsi), %r8d
	movl	%eax, %r9d
	andl	$15, %ecx
	leaq	.LC1(%rip), %rax
	movsbl	(%rax,%rcx), %ecx
	andl	$15, %r9d
	movl	%r8d, %eax
	shrb	$5, %al
	btl	%eax, %ecx
	jnc	.L1326
	leaq	2(%rsi), %rax
	cmpq	%rax, %rdx
	je	.L1326
	movzbl	2(%rsi), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L1326
	sall	$6, %r9d
	andl	$63, %r8d
	movq	(%rdi), %rcx
	movzbl	%al, %eax
	addl	%r9d, %r8d
	movslq	%r8d, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	addl	%edx, %eax
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1327:
	cmpl	$193, %eax
	jle	.L1326
	movzbl	1(%rsi), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L1326
	movq	(%rdi), %rax
	andl	$31, %ecx
	movzbl	%dl, %edx
	movzwl	(%rax,%rcx,2), %eax
	addl	%edx, %eax
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1328:
	subl	$240, %eax
	cmpl	$4, %eax
	jle	.L1345
	.p2align 4,,10
	.p2align 3
.L1326:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1331:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1345:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movzbl	1(%rsi), %r8d
	leaq	.LC2(%rip), %r9
	movq	%r8, %rcx
	shrq	$4, %rcx
	andl	$15, %ecx
	movsbl	(%r9,%rcx), %ecx
	btl	%eax, %ecx
	jnc	.L1326
	sall	$6, %eax
	andl	$63, %r8d
	leaq	2(%rsi), %rcx
	orl	%r8d, %eax
	cmpq	%rcx, %rdx
	je	.L1326
	movzbl	2(%rsi), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	ja	.L1326
	leaq	3(%rsi), %r8
	cmpq	%r8, %rdx
	je	.L1326
	movzbl	3(%rsi), %edx
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L1326
	movzwl	28(%rdi), %esi
	cmpl	%eax, %esi
	jg	.L1329
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L1325
.L1329:
	movzbl	%dl, %r8d
	movl	%eax, %esi
	movzbl	%cl, %edx
	movl	%r8d, %ecx
	call	ucptrie_internalSmallU8Index_67@PLT
	movq	32(%rbx), %rdi
	jmp	.L1325
	.cfi_endproc
.LFE3251:
	.size	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKhS2_, .-_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKhS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKDsS2_a
	.type	_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKDsS2_a, @function
_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKDsS2_a:
.LFB3252:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rsi
	je	.L1355
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzwl	-2(%rdx), %edi
	movq	32(%rbx), %r8
	movl	%edi, %r9d
	movl	%edi, %eax
	andl	$63488, %r9d
	cmpl	$55296, %r9d
	je	.L1348
	movq	(%r8), %rdx
	sarl	$6, %edi
	andl	$63, %eax
	movslq	%edi, %rdi
	movzwl	(%rdx,%rdi,2), %edx
	addl	%edx, %eax
.L1349:
	movq	8(%r8), %rdx
	cltq
	xorl	%r8d, %r8d
	movzwl	(%rdx,%rax,2), %eax
	testb	$1, %al
	je	.L1346
	testb	%cl, %cl
	je	.L1357
	cmpw	$1, %ax
	jne	.L1370
.L1357:
	movl	$1, %r8d
.L1346:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1348:
	.cfi_restore_state
	testb	$4, %ah
	je	.L1350
	leaq	-2(%rdx), %rax
	cmpq	%rax, %rsi
	je	.L1350
	movzwl	-4(%rdx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L1371
.L1350:
	movl	20(%r8), %eax
	subl	$1, %eax
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1370:
	cmpw	26(%rbx), %ax
	jb	.L1354
	andl	$6, %eax
	cmpw	$2, %ax
	setbe	%r8b
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1355:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1354:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	48(%rbx), %rdx
	andl	$65534, %eax
	cmpw	$511, (%rax,%rdx)
	setbe	%r8b
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1371:
	sall	$10, %eax
	leal	-56613888(%rdi,%rax), %esi
	cmpl	%esi, 24(%r8)
	jg	.L1372
	movl	20(%r8), %eax
	subl	$2, %eax
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1372:
	movq	%r8, %rdi
	movl	%ecx, -20(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%rbx), %r8
	movl	-20(%rbp), %ecx
	jmp	.L1349
	.cfi_endproc
.LFE3252:
	.size	_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKDsS2_a, .-_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKDsS2_a
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKhS2_a
	.type	_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKhS2_a, @function
_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKhS2_a:
.LFB3253:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rsi
	je	.L1378
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movzbl	-1(%rdx), %r8d
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	testb	%r8b, %r8b
	js	.L1386
.L1375:
	movq	8(%rdi), %rax
	movslq	%r8d, %r8
	movzwl	(%rax,%r8,2), %eax
	xorl	%r8d, %r8d
	testb	$1, %al
	je	.L1373
	testb	%r12b, %r12b
	je	.L1380
	cmpw	$1, %ax
	jne	.L1387
.L1380:
	movl	$1, %r8d
.L1373:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1386:
	.cfi_restore_state
	leaq	-1(%rdx), %rcx
	movq	%rsi, %rdx
	movl	%r8d, %esi
	call	ucptrie_internalU8PrevIndex_67@PLT
	movq	32(%rbx), %rdi
	sarl	$3, %eax
	movl	%eax, %r8d
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1387:
	cmpw	26(%rbx), %ax
	jb	.L1377
	andl	$6, %eax
	cmpw	$2, %ax
	setbe	%r8b
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1378:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1377:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	48(%rbx), %rdx
	andl	$65534, %eax
	cmpw	$511, (%rax,%rdx)
	setbe	%r8b
	jmp	.L1373
	.cfi_endproc
.LFE3253:
	.size	_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKhS2_a, .-_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKhS2_a
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl24findPreviousCompBoundaryEPKDsS2_a
	.type	_ZNK6icu_6715Normalizer2Impl24findPreviousCompBoundaryEPKDsS2_a, @function
_ZNK6icu_6715Normalizer2Impl24findPreviousCompBoundaryEPKDsS2_a:
.LFB3254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	%rsi, %rdx
	je	.L1403
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	movq	%rsi, %r12
.L1390:
	movzwl	-2(%rdx), %r14d
	leaq	-2(%rdx), %r13
	movl	%r14d, %esi
	movl	%r14d, %eax
	andl	$63488, %esi
	cmpl	$55296, %esi
	je	.L1391
	movl	%r14d, %esi
	movq	(%rdi), %r8
	andl	$63, %eax
	sarl	$6, %esi
	movslq	%esi, %rsi
	movzwl	(%r8,%rsi,2), %esi
	addl	%esi, %eax
.L1392:
	movq	8(%rdi), %rsi
	cltq
	movzwl	(%rsi,%rax,2), %eax
	testb	$1, %al
	je	.L1396
	testb	%cl, %cl
	je	.L1403
	cmpw	$1, %ax
	jne	.L1415
.L1403:
	movq	%rdx, %r13
.L1388:
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1415:
	.cfi_restore_state
	cmpw	26(%rbx), %ax
	jb	.L1397
	movl	%eax, %esi
	andl	$6, %esi
	cmpw	$2, %si
	jbe	.L1403
	.p2align 4,,10
	.p2align 3
.L1396:
	movzwl	10(%rbx), %edx
	cmpl	%edx, %r14d
	jl	.L1388
	cmpw	22(%rbx), %ax
	jb	.L1388
	cmpw	26(%rbx), %ax
	jb	.L1398
	cmpw	30(%rbx), %ax
	jb	.L1388
.L1398:
	cmpq	%r12, %r13
	je	.L1388
	movq	%r13, %rdx
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1391:
	testb	$4, %ah
	je	.L1393
	cmpq	%r13, %r12
	je	.L1393
	movzwl	-4(%rdx), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	je	.L1416
.L1393:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1397:
	movq	48(%rbx), %rsi
	movq	%rax, %r8
	andl	$65534, %r8d
	cmpw	$511, (%r8,%rsi)
	jbe	.L1403
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1416:
	sall	$10, %eax
	leaq	-4(%rdx), %r13
	leal	-56613888(%r14,%rax), %r14d
	cmpl	%r14d, 24(%rdi)
	jg	.L1417
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1417:
	movl	%r14d, %esi
	movb	%cl, -41(%rbp)
	movq	%rdx, -40(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%rbx), %rdi
	movzbl	-41(%rbp), %ecx
	movq	-40(%rbp), %rdx
	jmp	.L1392
	.cfi_endproc
.LFE3254:
	.size	_ZNK6icu_6715Normalizer2Impl24findPreviousCompBoundaryEPKDsS2_a, .-_ZNK6icu_6715Normalizer2Impl24findPreviousCompBoundaryEPKDsS2_a
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl20findNextCompBoundaryEPKDsS2_a
	.type	_ZNK6icu_6715Normalizer2Impl20findNextCompBoundaryEPKDsS2_a, @function
_ZNK6icu_6715Normalizer2Impl20findNextCompBoundaryEPKDsS2_a:
.LFB3255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	cmpq	%rdx, %rsi
	je	.L1418
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
.L1420:
	movzwl	(%r12), %r14d
	leaq	2(%r12), %r13
	movl	%r14d, %esi
	movl	%r14d, %eax
	andl	$63488, %esi
	cmpl	$55296, %esi
	je	.L1421
	movl	%r14d, %esi
	movq	(%rdi), %r8
	andl	$63, %eax
	sarl	$6, %esi
	movslq	%esi, %rsi
	movzwl	(%r8,%rsi,2), %esi
	addl	%esi, %eax
.L1422:
	movzwl	10(%rbx), %esi
	cmpl	%esi, %r14d
	jl	.L1418
	movq	8(%rdi), %rsi
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpw	22(%rbx), %ax
	jnb	.L1444
.L1418:
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1444:
	.cfi_restore_state
	movzwl	26(%rbx), %esi
	cmpw	%si, %ax
	jb	.L1426
	cmpw	30(%rbx), %ax
	jb	.L1418
.L1426:
	testb	$1, %al
	je	.L1427
	testb	%cl, %cl
	je	.L1432
	cmpw	$1, %ax
	jne	.L1445
.L1432:
	movq	%r13, %r12
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1421:
	testb	$4, %ah
	jne	.L1423
	cmpq	%r13, %rdx
	je	.L1423
	movzwl	2(%r12), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	je	.L1446
.L1423:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1445:
	cmpw	%si, %ax
	jb	.L1428
	andl	$6, %eax
	cmpw	$2, %ax
	jbe	.L1432
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	%r13, %r12
	cmpq	%rdx, %r13
	jne	.L1420
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1446:
	sall	$10, %r14d
	leaq	4(%r12), %r13
	leal	-56613888(%rax,%r14), %r14d
	cmpl	%r14d, 24(%rdi)
	jg	.L1447
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	48(%rbx), %rsi
	andl	$65534, %eax
	cmpw	$511, (%rax,%rsi)
	ja	.L1427
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1447:
	movl	%r14d, %esi
	movb	%cl, -41(%rbp)
	movq	%rdx, -40(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%rbx), %rdi
	movzbl	-41(%rbp), %ecx
	movq	-40(%rbp), %rdx
	jmp	.L1422
	.cfi_endproc
.LFE3255:
	.size	_ZNK6icu_6715Normalizer2Impl20findNextCompBoundaryEPKDsS2_a, .-_ZNK6icu_6715Normalizer2Impl20findNextCompBoundaryEPKDsS2_a
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKDsS2_
	.type	_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKDsS2_, @function
_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKDsS2_:
.LFB3256:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rsi
	je	.L1471
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	%rsi, %rdx
	sarq	%rdx
	subl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	movslq	%edx, %rax
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	(%rax,%rax), %rdi
	subq	$8, %rsp
	movzwl	(%rsi,%rax,2), %r12d
	movzwl	8(%rbx), %ecx
	movl	%r12d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L1450
	testl	%edx, %edx
	jg	.L1490
.L1450:
	cmpl	%ecx, %r12d
	jge	.L1468
.L1489:
	xorl	%eax, %eax
.L1448:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1468:
	.cfi_restore_state
	movl	%r12d, %eax
	movq	56(%rbx), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L1489
	movl	%r12d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L1489
	movl	%r12d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L1491
	movq	32(%rbx), %rdx
	movl	%r12d, %eax
	sarl	$6, %eax
	movq	8(%rdx), %r13
	movq	(%rdx), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%r12d, %edx
	andl	$63, %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
.L1458:
	movzwl	0(%r13,%rax), %eax
	cmpw	%ax, 26(%rbx)
	ja	.L1457
	cmpw	$-1025, %ax
	jbe	.L1460
	shrw	%ax
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1490:
	movzwl	-2(%rsi,%rdi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L1450
	sall	$10, %eax
	leal	-56613888(%r12,%rax), %r12d
	cmpl	%ecx, %r12d
	jl	.L1489
	cmpl	$65535, %r12d
	jle	.L1468
	movq	32(%rbx), %rdi
	movq	8(%rdi), %r13
	cmpl	$1114111, %r12d
	ja	.L1492
	cmpl	24(%rdi), %r12d
	jl	.L1459
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1460:
	cmpw	%ax, 30(%rbx)
	jbe	.L1489
	movl	%eax, %edx
	andl	$6, %edx
	cmpw	$2, %dx
	jbe	.L1493
	movzwl	%ax, %esi
	movq	32(%rbx), %rdi
	movl	%esi, %eax
	sarl	$3, %eax
	leal	(%rax,%r12), %esi
	movzwl	28(%rbx), %eax
	movq	8(%rdi), %r12
	subl	%eax, %esi
	cmpl	$65535, %esi
	ja	.L1462
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
.L1463:
	movzwl	(%r12,%rax), %eax
	.p2align 4,,10
	.p2align 3
.L1457:
	cmpw	%ax, 14(%rbx)
	jnb	.L1489
	movzwl	16(%rbx), %ecx
	movzwl	%ax, %edx
	orl	$1, %ecx
	cmpw	%cx, %ax
	je	.L1489
	movq	48(%rbx), %rax
	sarl	%edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rax
	movzwl	(%rax), %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	movzbl	%dh, %eax
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1471:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1491:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	cmpw	$1, 26(%rbx)
	movl	$1, %eax
	ja	.L1457
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1492:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1459:
	movl	%r12d, %esi
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1462:
	cmpl	$1114111, %esi
	ja	.L1464
	cmpl	24(%rdi), %esi
	jl	.L1465
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1463
.L1464:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1463
.L1465:
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L1463
.L1493:
	movl	%edx, %eax
	shrl	%eax
	andl	$3, %eax
	jmp	.L1448
	.cfi_endproc
.LFE3256:
	.size	_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKDsS2_, .-_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKDsS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl7composeEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl7composeEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl7composeEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode:
.LFB3246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%r9, -56(%rbp)
	movzwl	10(%rdi), %r10d
	movq	%rsi, %r9
	movl	%r8d, -60(%rbp)
	movb	%cl, -62(%rbp)
	movb	%r8b, -61(%rbp)
	testq	%rdx, %rdx
	je	.L1719
.L1506:
	cmpq	%r13, %r9
	je	.L1507
	movzwl	(%r9), %edx
	movl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L1508:
	movl	%edx, %r14d
	leaq	2(%r9), %r12
	cmpl	%edx, %r10d
	jg	.L1511
.L1721:
	movq	32(%rbx), %rdi
	sarl	$6, %edx
	andl	$63, %eax
	movslq	%edx, %rdx
	movq	(%rdi), %rcx
	movq	8(%rdi), %r15
	movzwl	(%rcx,%rdx,2), %edx
	movzwl	18(%rbx), %ecx
	addl	%edx, %eax
	cltq
	movzwl	(%r15,%rax,2), %r8d
	cmpw	%cx, %r8w
	jb	.L1511
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L1512
	cmpq	%r12, %r13
	je	.L1507
	movzwl	2(%r9), %edx
	movl	%edx, %esi
	movl	%edx, %eax
	andl	$64512, %esi
	cmpl	$56320, %esi
	je	.L1720
	movq	%r12, %r9
	movl	%edx, %r14d
	leaq	2(%r9), %r12
	cmpl	%edx, %r10d
	jle	.L1721
	.p2align 4,,10
	.p2align 3
.L1511:
	cmpq	%r12, %r13
	je	.L1507
	movzwl	(%r12), %edx
	movq	%r12, %r9
	movl	%edx, %eax
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1507:
	cmpq	%r13, %r11
	je	.L1509
	cmpb	$0, -60(%rbp)
	jne	.L1722
.L1509:
	movl	$1, %eax
.L1494:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1720:
	.cfi_restore_state
	sall	$10, %r14d
	leaq	4(%r9), %r12
	leal	-56613888(%r14,%rdx), %r14d
	cmpl	%r14d, 24(%rdi)
	jg	.L1514
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
.L1515:
	movzwl	(%r15,%rax), %r8d
	cmpw	%cx, %r8w
	jb	.L1511
	.p2align 4,,10
	.p2align 3
.L1512:
	movzwl	%r8w, %r15d
	cmpw	30(%rbx), %r8w
	jnb	.L1516
	cmpb	$0, -61(%rbp)
	je	.L1546
	cmpw	26(%rbx), %r8w
	jb	.L1518
	testb	$1, %r8b
	je	.L1519
	cmpw	$1, %r8w
	je	.L1524
	cmpb	$0, -62(%rbp)
	je	.L1524
	movl	%r8d, %eax
	andl	$6, %eax
	cmpw	$2, %ax
	ja	.L1519
.L1524:
	cmpq	%r9, %r11
	je	.L1523
	movq	16(%rbp), %rcx
	movq	-56(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r11, %rsi
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movl	-80(%rbp), %r10d
	testb	%al, %al
	je	.L1509
.L1523:
	movzwl	28(%rbx), %eax
	sarl	$3, %r15d
	addl	%r15d, %r14d
	subl	%eax, %r14d
	movq	-56(%rbp), %rax
	movl	40(%rax), %eax
	cmpl	$65535, %r14d
	jg	.L1723
	testl	%eax, %eax
	je	.L1724
.L1528:
	movq	-56(%rbp), %rsi
	subl	$1, %eax
	movq	%r12, %r11
	movq	%r12, %r9
	movq	32(%rsi), %rcx
	leaq	2(%rcx), %rdx
	movq	%rdx, 32(%rsi)
	movw	%r14w, (%rcx)
	movb	$0, 44(%rsi)
	movq	%rdx, 24(%rsi)
	movl	%eax, 40(%rsi)
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1722:
	movq	16(%rbp), %rcx
	movq	-56(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r11, %rsi
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1514:
	.cfi_restore_state
	movl	%r14d, %esi
	movq	%r11, -96(%rbp)
	movq	%r9, -88(%rbp)
	movl	%r10d, -80(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movzwl	18(%rbx), %ecx
	movq	-96(%rbp), %r11
	cltq
	movq	-88(%rbp), %r9
	movl	-80(%rbp), %r10d
	addq	%rax, %rax
	jmp	.L1515
	.p2align 4,,10
	.p2align 3
.L1719:
	cmpb	$0, -60(%rbp)
	movl	%ecx, %r12d
	cmovne	-56(%rbp), %r13
	movq	%rsi, %rax
	jmp	.L1497
	.p2align 4,,10
	.p2align 3
.L1725:
	testw	%cx, %cx
	je	.L1611
.L1497:
	movq	%rax, %r9
	movzwl	(%rax), %ecx
	addq	$2, %rax
	cmpl	%ecx, %r10d
	jg	.L1725
.L1611:
	cmpq	%r9, %r11
	je	.L1499
	testq	%r13, %r13
	je	.L1500
	movq	16(%rbp), %rcx
	movq	%r9, %rdx
	movq	%r11, %rsi
	movq	%r13, %rdi
	movl	%r10d, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movq	-80(%rbp), %r9
	movl	-88(%rbp), %r10d
.L1500:
	movq	16(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L1726
.L1546:
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1516:
	.cfi_restore_state
	cmpw	$-512, %r8w
	jne	.L1612
	cmpq	%r9, %r11
	jne	.L1541
.L1612:
	cmpw	$-512, %r8w
	jbe	.L1525
	sarl	%r15d
	cmpb	$0, -62(%rbp)
	movl	%r15d, %r14d
	je	.L1573
	movq	%r9, %rdx
	movq	%r11, %rsi
	movq	%rbx, %rdi
	movl	%r10d, -104(%rbp)
	movl	%r8d, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r11, -80(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKDsS2_
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %r9
	cmpb	%r15b, %al
	movl	-96(%rbp), %r8d
	movl	-104(%rbp), %r10d
	jbe	.L1573
	cmpb	$0, -61(%rbp)
	je	.L1546
.L1525:
	cmpq	%r9, %r11
	je	.L1606
.L1545:
	cmpw	22(%rbx), %r8w
	jnb	.L1591
.L1576:
	cmpb	$0, -61(%rbp)
	je	.L1608
	cmpq	%r9, %r11
	je	.L1608
.L1590:
	movq	16(%rbp), %rcx
	movq	-56(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r11, %rsi
	movl	%r10d, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	testb	%al, %al
	je	.L1509
	movq	-80(%rbp), %r9
	movl	-88(%rbp), %r10d
	movq	%r9, %r15
.L1575:
	movq	-56(%rbp), %rax
	movq	16(%rbp), %rsi
	movl	%r10d, -88(%rbp)
	movq	32(%rax), %r14
	movl	(%rsi), %r11d
	subq	16(%rax), %r14
	sarq	%r14
	movq	%r14, -80(%rbp)
	movsbl	-62(%rbp), %r14d
	testl	%r11d, %r11d
	jg	.L1509
	subq	$8, %rsp
	movq	%rax, %r9
	movl	%r14d, %r8d
	xorl	%ecx, %ecx
	pushq	%rsi
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0
	movq	16(%rbp), %rsi
	popq	%r8
	popq	%r9
	movl	(%rsi), %r10d
	testl	%r10d, %r10d
	jg	.L1509
	subq	$8, %rsp
	movq	-56(%rbp), %r9
	movl	$1, %ecx
	movq	%rbx, %rdi
	pushq	%rsi
	movl	%r14d, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdx
	call	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0
	popq	%rcx
	popq	%rsi
	movq	%rax, %r9
	movq	16(%rbp), %rax
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L1509
	movq	%r9, %r12
	movl	$4294967294, %eax
	movl	-88(%rbp), %r10d
	subq	%r15, %r12
	cmpq	%rax, %r12
	jg	.L1727
	movl	-80(%rbp), %edx
	movq	-56(%rbp), %rsi
	movl	%r14d, %ecx
	movq	%rbx, %rdi
	movq	%r9, -96(%rbp)
	movl	%r10d, -88(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl9recomposeERNS_16ReorderingBufferEia
	movq	-96(%rbp), %r9
	cmpb	$0, -61(%rbp)
	movl	-88(%rbp), %r10d
	movq	%r9, %r11
	jne	.L1506
	movq	-56(%rbp), %rax
	sarq	%r12
	movq	16(%rax), %rdi
	movq	32(%rax), %rax
	movq	%rax, %rdx
	movq	%rax, -80(%rbp)
	subq	%rdi, %rdx
	sarq	%rdx
	cmpl	%r12d, %edx
	jne	.L1546
	movq	%r15, %rsi
	call	u_memcmp_67@PLT
	testl	%eax, %eax
	jne	.L1546
	movq	-56(%rbp), %rax
	movl	-88(%rbp), %r10d
	movq	-96(%rbp), %r9
	movdqu	8(%rax), %xmm1
	movq	8(%rax), %rdx
	movdqa	%xmm1, %xmm0
	movaps	%xmm1, -80(%rbp)
	punpckhqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rax)
	testb	$2, 8(%rdx)
	movl	$27, %eax
	jne	.L1586
	movl	16(%rdx), %eax
.L1586:
	movq	-56(%rbp), %rsi
	movq	%r9, %r11
	movl	%eax, 40(%rsi)
	movb	$0, 44(%rsi)
	jmp	.L1506
.L1726:
	xorl	%esi, %esi
	movq	%r9, %rdi
	movl	%r10d, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	u_strchr_67@PLT
	movq	-80(%rbp), %r9
	movl	-88(%rbp), %r10d
	movq	%rax, %r13
	movzwl	-2(%r9), %edx
	movl	%edx, %ecx
	movl	%edx, %eax
	andl	$64512, %ecx
	cmpl	$55296, %ecx
	je	.L1594
	movq	32(%rbx), %rcx
	sarl	$6, %edx
	andl	$63, %eax
	movslq	%edx, %rdx
	movq	(%rcx), %rsi
	movzwl	(%rsi,%rdx,2), %edx
	addl	%edx, %eax
	movq	8(%rcx), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	testb	$1, %al
	je	.L1501
	cmpw	$1, %ax
	je	.L1594
	testb	%r12b, %r12b
	je	.L1594
	cmpw	%ax, 26(%rbx)
	ja	.L1502
	andl	$6, %eax
	movq	%r9, %r11
	cmpw	$2, %ax
	jbe	.L1506
.L1501:
	movq	-56(%rbp), %rsi
	movq	32(%rsi), %rdx
	movq	16(%rsi), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	cmpq	$2, %rcx
	jle	.L1503
	leaq	-2(%rdx), %rax
	addl	$1, 40(%rsi)
	movq	%rax, 32(%rsi)
.L1504:
	subq	$2, %r9
	movb	$0, 44(%rsi)
	movq	%rax, 24(%rsi)
	movq	%r9, %r11
	jmp	.L1506
.L1499:
	movq	16(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jg	.L1546
	movq	%r11, %rdi
	xorl	%esi, %esi
	movq	%r9, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movq	%r11, -80(%rbp)
	call	u_strchr_67@PLT
	movq	-80(%rbp), %r11
	movl	-88(%rbp), %r10d
	movq	-96(%rbp), %r9
	movq	%rax, %r13
	jmp	.L1506
.L1518:
	cmpw	%r8w, 20(%rbx)
	jbe	.L1531
	testb	$1, %r8b
	je	.L1532
	cmpb	$0, -62(%rbp)
	je	.L1537
	cmpw	$1, %r8w
	je	.L1537
	movl	%r15d, %eax
	movq	48(%rbx), %rdx
	sarl	%eax
	cltq
	cmpw	$511, (%rdx,%rax,2)
	jbe	.L1537
.L1532:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r11, -104(%rbp)
	movq	%r9, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKDsS2_
	movl	-88(%rbp), %r10d
	movq	-96(%rbp), %r9
	testb	%al, %al
	movq	-104(%rbp), %r11
	je	.L1718
.L1537:
	cmpq	%r9, %r11
	je	.L1536
	movq	16(%rbp), %rcx
	movq	-56(%rbp), %rdi
	movq	%r9, %rdx
	movq	%r11, %rsi
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movl	-80(%rbp), %r10d
	testb	%al, %al
	je	.L1509
.L1536:
	movl	%r15d, %eax
	movq	48(%rbx), %rdx
	movq	16(%rbp), %rcx
	movl	%r10d, -80(%rbp)
	sarl	%eax
	cltq
	leaq	(%rdx,%rax,2), %rax
	leaq	2(%rax), %rsi
	movzwl	(%rax), %eax
	andl	$31, %eax
	leaq	(%rsi,%rax,2), %rdx
	jmp	.L1717
.L1731:
	cmpw	$-512, 22(%rbx)
	movl	-80(%rbp), %r8d
	ja	.L1590
.L1591:
	cmpw	26(%rbx), %r8w
	jb	.L1577
	cmpw	30(%rbx), %r8w
	jb	.L1576
.L1577:
	movzwl	-2(%r9), %eax
	movq	32(%rbx), %rdi
	leaq	-2(%r9), %r14
	movl	%eax, %ecx
	movl	%eax, %edx
	andl	$63488, %ecx
	cmpl	$55296, %ecx
	je	.L1578
	movq	(%rdi), %rcx
	sarl	$6, %eax
	andl	$63, %edx
	cltq
	movzwl	(%rcx,%rax,2), %eax
	addl	%edx, %eax
.L1579:
	movq	8(%rdi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	testb	$1, %al
	je	.L1607
	cmpw	$1, %ax
	je	.L1576
	cmpb	$0, -62(%rbp)
	je	.L1576
	cmpw	26(%rbx), %ax
	jb	.L1583
	andl	$6, %eax
	cmpw	$3, %ax
	cmovnb	%r14, %r9
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1730:
	movq	(%rdi), %rcx
	sarl	$6, %edx
	andl	$63, %eax
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	addl	%edx, %eax
.L1568:
	movq	8(%rdi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %edx
	cmpw	$-511, %dx
	jbe	.L1572
	movzwl	%dx, %eax
	sarl	%eax
	cmpb	%al, %r14b
	ja	.L1728
	movl	%eax, %r14d
	movq	%r15, %r12
.L1573:
	cmpq	%r13, %r12
	je	.L1729
	movzwl	(%r12), %edx
	movq	32(%rbx), %rdi
	leaq	2(%r12), %r15
	movl	%edx, %ecx
	movl	%edx, %eax
	andl	$63488, %ecx
	cmpl	$55296, %ecx
	jne	.L1730
	testb	$4, %dh
	jne	.L1569
	cmpq	%r13, %r15
	je	.L1569
	movzwl	2(%r12), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	je	.L1570
.L1569:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L1568
.L1519:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r11, -104(%rbp)
	movq	%r9, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKDsS2_
	movl	-88(%rbp), %r10d
	movq	-96(%rbp), %r9
	testb	%al, %al
	movq	-104(%rbp), %r11
	jne	.L1524
.L1718:
	movl	-80(%rbp), %r8d
	jmp	.L1525
.L1531:
	cmpw	%r8w, 24(%rbx)
	ja	.L1525
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r11, -104(%rbp)
	movq	%r9, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKDsS2_
	movl	-80(%rbp), %r8d
	movl	-88(%rbp), %r10d
	testb	%al, %al
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r11
	je	.L1538
.L1540:
	cmpq	%r9, %r11
	je	.L1715
	movl	%r10d, -80(%rbp)
	movq	16(%rbp), %rcx
	movq	%r9, %rdx
	movq	%r11, %rsi
.L1717:
	movq	-56(%rbp), %rdi
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movl	-80(%rbp), %r10d
	testb	%al, %al
	je	.L1509
.L1715:
	movq	%r12, %r11
	movq	%r12, %r9
	jmp	.L1506
.L1541:
	movzwl	-2(%r9), %eax
	cmpl	$4518, %r14d
	jg	.L1544
	leal	-4352(%rax), %r15d
	cmpw	$18, %r15w
	ja	.L1545
	cmpb	$0, -61(%rbp)
	je	.L1546
	cmpq	%r13, %r12
	je	.L1547
	movzwl	(%r12), %edx
	leal	-4520(%rdx), %eax
	cmpl	$26, %eax
	jbe	.L1548
.L1547:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r11, -104(%rbp)
	movq	%r9, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKDsS2_
	movl	-88(%rbp), %r10d
	movq	-96(%rbp), %r9
	testb	%al, %al
	movq	-104(%rbp), %r11
	je	.L1731
	xorl	%edx, %edx
.L1549:
	movzwl	%r15w, %eax
	leal	(%rax,%rax,4), %ecx
	leal	(%rax,%rcx,4), %eax
	leal	-4449(%r14,%rax), %eax
	imull	$28, %eax, %eax
	leal	44032(%rdx,%rax), %r14d
	leaq	-2(%r9), %rdx
	cmpq	%rdx, %r11
	je	.L1554
	movq	16(%rbp), %rcx
	movq	-56(%rbp), %rdi
	movq	%r11, %rsi
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movl	-80(%rbp), %r10d
	testb	%al, %al
	je	.L1509
.L1554:
	movq	-56(%rbp), %rsi
	movl	40(%rsi), %eax
	movq	32(%rsi), %rdx
	testl	%eax, %eax
	je	.L1732
.L1553:
	movq	-56(%rbp), %rsi
	leaq	2(%rdx), %rcx
	subl	$1, %eax
	movq	%rcx, 32(%rsi)
	movw	%r14w, (%rdx)
	movb	$0, 44(%rsi)
	movq	%rcx, 24(%rsi)
	movl	%eax, 40(%rsi)
	jmp	.L1715
	.p2align 4,,10
	.p2align 3
.L1608:
	movq	%r9, %r15
	jmp	.L1575
.L1594:
	movq	%r9, %r11
	jmp	.L1506
.L1606:
	movq	%r11, %r15
	jmp	.L1575
.L1723:
	cmpl	$1, %eax
	jle	.L1733
.L1530:
	movq	-56(%rbp), %rsi
	movl	%r14d, %edx
	subl	$2, %eax
	movq	%r12, %r11
	sarl	$10, %edx
	movq	%r12, %r9
	movq	32(%rsi), %rcx
	subw	$10304, %dx
	movw	%dx, (%rcx)
	movl	%r14d, %edx
	addq	$4, %rcx
	andw	$1023, %dx
	orw	$-9216, %dx
	movw	%dx, -2(%rcx)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movb	$0, 44(%rsi)
	movl	%eax, 40(%rsi)
	movups	%xmm0, 24(%rsi)
	jmp	.L1506
.L1503:
	movq	8(%rsi), %rcx
	movq	%rax, 32(%rsi)
	movl	$27, %edx
	testb	$2, 8(%rcx)
	jne	.L1505
	movl	16(%rcx), %edx
.L1505:
	movq	-56(%rbp), %rsi
	movl	%edx, 40(%rsi)
	jmp	.L1504
.L1728:
	cmpb	$0, -61(%rbp)
	je	.L1546
.L1572:
	cmpw	22(%rbx), %dx
	jb	.L1574
	cmpw	26(%rbx), %dx
	jb	.L1525
	cmpw	30(%rbx), %dx
	jnb	.L1525
.L1574:
	cmpw	18(%rbx), %dx
	cmovnb	%r12, %r15
	movq	%r15, %r9
	jmp	.L1506
.L1607:
	movq	%r14, %r9
	jmp	.L1576
.L1544:
	leal	-44032(%rax), %edx
	cmpl	$11171, %edx
	ja	.L1545
	imull	$-1227133513, %edx, %edx
	addl	$306783376, %edx
	rorl	$2, %edx
	cmpl	$153391688, %edx
	ja	.L1545
	cmpb	$0, -61(%rbp)
	je	.L1546
	leaq	-2(%r9), %rdx
	leal	-4519(%rax,%r14), %r14d
	cmpq	%rdx, %r11
	je	.L1561
	movq	16(%rbp), %rcx
	movq	-56(%rbp), %rdi
	movq	%r11, %rsi
	movl	%r10d, -80(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movl	-80(%rbp), %r10d
	testb	%al, %al
	je	.L1509
.L1561:
	movq	-56(%rbp), %rax
	movl	40(%rax), %edx
	movq	32(%rax), %rax
	testl	%edx, %edx
	je	.L1734
.L1560:
	movq	-56(%rbp), %rsi
	leaq	2(%rax), %rcx
	subl	$1, %edx
	movq	%r12, %r11
	movq	%r12, %r9
	movq	%rcx, 32(%rsi)
	movw	%r14w, (%rax)
	movb	$0, 44(%rsi)
	movq	%rcx, 24(%rsi)
	movl	%edx, 40(%rsi)
	jmp	.L1506
.L1578:
	andb	$4, %dh
	je	.L1580
	cmpq	%r14, %r11
	je	.L1580
	movzwl	-4(%r9), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L1581
.L1580:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L1579
.L1570:
	sall	$10, %edx
	leaq	4(%r12), %r15
	leal	-56613888(%rax,%rdx), %esi
	cmpl	%esi, 24(%rdi)
	jg	.L1571
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L1568
.L1729:
	cmpb	$0, -60(%rbp)
	je	.L1509
	movq	16(%rbp), %rcx
	movq	-56(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r11, %rsi
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movl	$1, %eax
	jmp	.L1494
.L1502:
	movq	48(%rbx), %rdx
	andl	$65534, %eax
	movq	%r9, %r11
	cmpw	$511, (%rax,%rdx)
	jbe	.L1506
	jmp	.L1501
.L1724:
	movq	-56(%rbp), %r15
	movq	16(%rbp), %rdx
	movl	$1, %esi
	movl	%r10d, -80(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L1509
	movl	40(%r15), %eax
	movl	-80(%rbp), %r10d
	jmp	.L1528
.L1733:
	movq	-56(%rbp), %r15
	movq	16(%rbp), %rdx
	movl	$2, %esi
	movl	%r10d, -80(%rbp)
	movq	%r15, %rdi
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	testb	%al, %al
	je	.L1509
	movl	40(%r15), %eax
	movl	-80(%rbp), %r10d
	jmp	.L1530
.L1732:
	subq	16(%rsi), %rdx
	movq	8(%rsi), %rdi
	movl	%r10d, -80(%rbp)
	sarq	%rdx
	movl	%edx, %esi
	movq	%rdx, %r15
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	-56(%rbp), %rax
	movl	-80(%rbp), %r10d
	leal	1(%r15), %esi
	movq	8(%rax), %rdi
	movl	$27, %eax
	testb	$2, 8(%rdi)
	jne	.L1555
	movl	16(%rdi), %eax
.L1555:
	cmpl	$256, %esi
	movl	$256, %edx
	movl	%r10d, -80(%rbp)
	cmovl	%edx, %esi
	addl	%eax, %eax
	cmpl	%esi, %eax
	cmovge	%eax, %esi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	-56(%rbp), %rsi
	movl	-80(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, 16(%rsi)
	je	.L1563
	movslq	%r15d, %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	-56(%rbp), %rax
	movq	8(%rax), %rcx
	movl	$27, %eax
	testb	$2, 8(%rcx)
	jne	.L1557
	movl	16(%rcx), %eax
.L1557:
	subl	%r15d, %eax
	jmp	.L1553
.L1734:
	movq	-56(%rbp), %rsi
	movl	%r10d, -80(%rbp)
	subq	16(%rsi), %rax
	movq	8(%rsi), %rdi
	sarq	%rax
	movl	%eax, %esi
	movq	%rax, %r15
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	movq	-56(%rbp), %rax
	movl	-80(%rbp), %r10d
	leal	1(%r15), %esi
	movq	8(%rax), %rdi
	movl	$27, %eax
	testb	$2, 8(%rdi)
	jne	.L1562
	movl	16(%rdi), %eax
.L1562:
	cmpl	$256, %esi
	movl	$256, %edx
	movl	%r10d, -80(%rbp)
	cmovl	%edx, %esi
	addl	%eax, %eax
	cmpl	%esi, %eax
	cmovge	%eax, %esi
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, 16(%rsi)
	testq	%rax, %rax
	je	.L1563
	movslq	%r15d, %rdx
	movl	-80(%rbp), %r10d
	addq	%rdx, %rdx
	addq	%rdx, %rax
	movq	8(%rsi), %rdx
	testb	$2, 8(%rdx)
	jne	.L1603
	movl	16(%rdx), %edx
.L1564:
	subl	%r15d, %edx
	jmp	.L1560
.L1538:
	movsbl	-62(%rbp), %ecx
	movq	%r9, %rdx
	movq	%r11, %rsi
	movq	%rbx, %rdi
	movl	%r10d, -104(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r11, -80(%rbp)
	movl	%r8d, -96(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKDsS2_a
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %r9
	testb	%al, %al
	movl	-104(%rbp), %r10d
	jne	.L1540
	movl	-96(%rbp), %r8d
	jmp	.L1525
.L1548:
	subl	$4519, %edx
	addq	$2, %r12
	jmp	.L1549
.L1581:
	sall	$10, %edx
	leaq	-4(%r9), %r14
	leal	-56613888(%rax,%rdx), %esi
	cmpl	%esi, 24(%rdi)
	jg	.L1582
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L1579
.L1583:
	movq	48(%rbx), %rdx
	andl	$65534, %eax
	cmpw	$511, (%rax,%rdx)
	cmova	%r14, %r9
	jmp	.L1576
.L1727:
	movq	16(%rbp), %rax
	movl	$8, (%rax)
	movl	$1, %eax
	jmp	.L1494
.L1571:
	movq	%r11, -104(%rbp)
	movq	%r9, -96(%rbp)
	movl	%r10d, -88(%rbp)
	movl	%r8d, -80(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%rbx), %rdi
	movq	-104(%rbp), %r11
	movq	-96(%rbp), %r9
	movl	-88(%rbp), %r10d
	movl	-80(%rbp), %r8d
	jmp	.L1568
.L1582:
	movq	%r11, -96(%rbp)
	movq	%r9, -88(%rbp)
	movl	%r10d, -80(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%rbx), %rdi
	movq	-96(%rbp), %r11
	movq	-88(%rbp), %r9
	movl	-80(%rbp), %r10d
	jmp	.L1579
.L1563:
	movq	16(%rbp), %rax
	movl	$7, (%rax)
	movl	$1, %eax
	jmp	.L1494
.L1603:
	movl	$27, %edx
	jmp	.L1564
	.cfi_endproc
.LFE3246:
	.size	_ZNK6icu_6715Normalizer2Impl7composeEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl7composeEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl16composeAndAppendEPKDsS2_aaRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl16composeAndAppendEPKDsS2_aaRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl16composeAndAppendEPKDsS2_aaRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode:
.LFB3248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %r15
	movq	24(%rbp), %r14
	movl	%ecx, -132(%rbp)
	movl	%r8d, -136(%rbp)
	movq	%r9, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r15), %rax
	cmpq	%rax, 16(%r15)
	je	.L1736
	movsbl	%r8b, %r11d
	movl	%r11d, %ecx
	movl	%r11d, -152(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl20findNextCompBoundaryEPKDsS2_a
	movq	%rax, -160(%rbp)
	cmpq	%rax, %r12
	je	.L1736
	movl	-152(%rbp), %r11d
	movq	32(%r15), %rdx
	movq	%rbx, %rdi
	movq	16(%r15), %rsi
	movl	%r11d, %ecx
	movl	%r11d, -172(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl24findPreviousCompBoundaryEPKDsS2_a
	movq	32(%r15), %rcx
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rdi, -152(%rbp)
	subq	%rax, %rcx
	sarq	%rcx
	movl	%ecx, %edx
	movq	%rcx, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	32(%r15), %rdi
	movq	16(%r15), %rax
	movq	-168(%rbp), %rcx
	movl	-172(%rbp), %r11d
	movq	%rdi, %rdx
	subq	%rax, %rdx
	movslq	%ecx, %rsi
	sarq	%rdx
	cmpq	%rdx, %rsi
	jl	.L1753
	movq	8(%r15), %rcx
	movq	%rax, 32(%r15)
	movl	$27, %edx
	testb	$2, 8(%rcx)
	jne	.L1739
	movl	16(%rcx), %edx
.L1739:
	movl	%edx, 40(%r15)
.L1738:
	movb	$0, 44(%r15)
	movq	-152(%rbp), %rsi
	movq	%rax, 24(%r15)
	movq	-144(%rbp), %rdi
	movl	%r11d, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-160(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	-152(%rbp), %rdi
	subq	%r12, %rcx
	sarq	%rcx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	-120(%rbp), %eax
	movl	-168(%rbp), %r11d
	testb	$17, %al
	jne	.L1750
	leaq	-118(%rbp), %rsi
	testb	$2, %al
	cmove	-104(%rbp), %rsi
.L1740:
	testw	%ax, %ax
	js	.L1741
	sarl	$5, %eax
.L1742:
	subq	$8, %rsp
	cltq
	movl	$1, %r8d
	movl	%r11d, %ecx
	pushq	%r14
	leaq	(%rsi,%rax,2), %rdx
	movq	%rbx, %rdi
	movq	%r15, %r9
	call	_ZNK6icu_6715Normalizer2Impl7composeEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode
	movl	(%r14), %r8d
	popq	%rcx
	movq	-152(%rbp), %rdi
	popq	%rsi
	testl	%r8d, %r8d
	jg	.L1754
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-160(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L1736:
	cmpb	$0, -132(%rbp)
	jne	.L1755
	testq	%r13, %r13
	je	.L1756
.L1746:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
.L1735:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1757
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1755:
	.cfi_restore_state
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r15, %r9
	movl	$1, %r8d
	pushq	%r14
	movsbl	-136(%rbp), %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6715Normalizer2Impl7composeEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode
	popq	%rax
	popq	%rdx
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1753:
	addq	%rsi, %rsi
	movq	%rdi, %rax
	addl	%ecx, 40(%r15)
	subq	%rsi, %rax
	movq	%rax, 32(%r15)
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L1756:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	u_strchr_67@PLT
	movq	%rax, %r13
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1754:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1741:
	movl	-116(%rbp), %eax
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1750:
	xorl	%esi, %esi
	jmp	.L1740
.L1757:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3248:
	.size	_ZNK6icu_6715Normalizer2Impl16composeAndAppendEPKDsS2_aaRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl16composeAndAppendEPKDsS2_aaRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKhS2_
	.type	_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKhS2_, @function
_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKhS2_:
.LFB3257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	je	.L1791
	subq	%rsi, %rdx
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subl	$1, %edx
	movl	%edx, -28(%rbp)
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %ecx
	testb	%cl, %cl
	js	.L1792
	movzwl	8(%rbx), %eax
	cmpl	%eax, %ecx
	jl	.L1791
.L1778:
	movl	%ecx, %eax
	movq	56(%rbx), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L1791
	movl	%ecx, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L1791
	movl	%ecx, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L1793
	movq	32(%rbx), %rdx
	movl	%ecx, %eax
	sarl	$6, %eax
	movq	8(%rdx), %r12
	movq	(%rdx), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%ecx, %edx
	andl	$63, %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
.L1768:
	movzwl	(%r12,%rax), %eax
	cmpw	%ax, 26(%rbx)
	ja	.L1767
	cmpw	$-1025, %ax
	jbe	.L1770
	shrw	%ax
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1793:
	cmpw	$1, 26(%rbx)
	movl	$1, %eax
	ja	.L1767
	.p2align 4,,10
	.p2align 3
.L1791:
	xorl	%eax, %eax
.L1758:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1794
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1792:
	.cfi_restore_state
	leaq	-28(%rbp), %rdx
	movl	$-1, %r8d
	xorl	%esi, %esi
	call	utf8_prevCharSafeBody_67@PLT
	movl	%eax, %ecx
	movzwl	8(%rbx), %eax
	cmpl	%eax, %ecx
	jl	.L1791
	cmpl	$65535, %ecx
	jle	.L1778
	movq	32(%rbx), %rdi
	movq	8(%rdi), %r12
	cmpl	$1114111, %ecx
	ja	.L1795
	cmpl	24(%rdi), %ecx
	jl	.L1769
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1770:
	cmpw	%ax, 30(%rbx)
	jbe	.L1791
	movl	%eax, %edx
	andl	$6, %edx
	cmpw	$2, %dx
	jbe	.L1796
	sarl	$3, %eax
	movq	32(%rbx), %rdi
	leal	(%rax,%rcx), %esi
	movzwl	28(%rbx), %eax
	movq	8(%rdi), %r12
	subl	%eax, %esi
	cmpl	$65535, %esi
	ja	.L1772
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
.L1773:
	movzwl	(%r12,%rax), %eax
	.p2align 4,,10
	.p2align 3
.L1767:
	cmpw	%ax, 14(%rbx)
	jnb	.L1791
	movzwl	16(%rbx), %ecx
	movzwl	%ax, %edx
	orl	$1, %ecx
	cmpw	%cx, %ax
	je	.L1791
	movq	48(%rbx), %rax
	sarl	%edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rax
	movzwl	(%rax), %edx
	movzbl	%dh, %eax
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1795:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1769:
	movl	%ecx, %esi
	movl	%ecx, -36(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movl	-36(%rbp), %ecx
	cltq
	addq	%rax, %rax
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1772:
	cmpl	$1114111, %esi
	ja	.L1774
	cmpl	24(%rdi), %esi
	jl	.L1775
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1773
.L1774:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L1773
.L1775:
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L1773
.L1794:
	call	__stack_chk_fail@PLT
.L1796:
	movl	%edx, %eax
	shrl	%eax
	andl	$3, %eax
	jmp	.L1758
	.cfi_endproc
.LFE3257:
	.size	_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKhS2_, .-_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKhS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl11composeUTF8EjaPKhS2_PNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl11composeUTF8EjaPKhS2_PNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl11composeUTF8EjaPKhS2_PNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB3249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movl	$2, %ecx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	16(%rbp), %rax
	movb	%dl, -237(%rbp)
	movl	%esi, -236(%rbp)
	movq	%rax, -232(%rbp)
	movq	24(%rbp), %rax
	movq	%r9, -216(%rbp)
	movq	%rax, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -120(%rbp)
	movq	%rax, -128(%rbp)
	movzwl	10(%rdi), %eax
	movl	%eax, %edx
	movl	%eax, %r11d
	cmpl	$127, %eax
	jle	.L1799
	sarl	$6, %edx
	subl	$64, %edx
	cmpl	$2048, %eax
	movl	$-32, %eax
	cmovl	%edx, %eax
	movl	%eax, %r11d
.L1799:
	leaq	-128(%rbp), %rax
	movq	%r12, -200(%rbp)
	movl	%r11d, %r15d
	movq	%rax, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L1800:
	cmpq	%r13, %r12
	je	.L2002
.L1801:
	movzbl	(%r12), %eax
	leaq	1(%r12), %r10
	cmpb	%r15b, %al
	jb	.L1894
	movq	32(%rbx), %rdi
	movzbl	%al, %edx
	movq	%r10, %r14
	testb	%al, %al
	js	.L2003
.L1808:
	movq	8(%rdi), %rax
	movslq	%edx, %rdx
	movzwl	(%rax,%rdx,2), %r8d
	cmpw	18(%rbx), %r8w
	jnb	.L2004
.L1999:
	movq	%r14, %r12
	cmpq	%r13, %r12
	jne	.L1801
.L2002:
	cmpq	%r13, -200(%rbp)
	je	.L1889
	cmpq	$0, -216(%rbp)
	jne	.L2005
.L1889:
	movl	$1, %r15d
.L1890:
	movq	-224(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2006
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1894:
	.cfi_restore_state
	movq	%r10, %r12
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L2003:
	cmpq	%r10, %r13
	je	.L1896
	cmpb	$-33, %al
	jbe	.L1810
	cmpb	$-17, %al
	ja	.L1811
	movzbl	1(%r12), %edx
	movl	%eax, %ecx
	leaq	.LC1(%rip), %rsi
	andl	$15, %eax
	movsbl	(%rsi,%rax), %esi
	andl	$15, %ecx
	movl	%edx, %eax
	shrb	$5, %al
	btl	%eax, %esi
	jc	.L2007
	.p2align 4,,10
	.p2align 3
.L1901:
	movq	%r10, %r14
.L1809:
	movl	20(%rdi), %eax
	leal	-1(%rax), %edx
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1810:
	cmpb	$-63, %al
	jbe	.L1901
	movzbl	1(%r12), %esi
	leal	-128(%rsi), %ecx
	cmpb	$63, %cl
	ja	.L1901
	movq	(%rdi), %rdx
	andl	$31, %eax
	movzbl	%cl, %ecx
	movzwl	(%rdx,%rax,2), %edx
	addl	%ecx, %edx
.L1812:
	addq	$1, %r14
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1896:
	movq	%r13, %r14
	jmp	.L1809
	.p2align 4,,10
	.p2align 3
.L1811:
	subl	$240, %edx
	cmpl	$4, %edx
	jg	.L1901
	movzbl	1(%r12), %esi
	leaq	.LC2(%rip), %rcx
	movq	%rsi, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%rcx,%rax), %eax
	btl	%edx, %eax
	jnc	.L1901
	sall	$6, %edx
	andl	$63, %esi
	leaq	2(%r12), %r14
	orl	%edx, %esi
	cmpq	%r14, %r13
	je	.L1809
	movzbl	2(%r12), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L1809
	leaq	3(%r12), %r14
	cmpq	%r14, %r13
	je	.L1809
	movzbl	3(%r12), %ecx
	leal	-128(%rcx), %edx
	cmpb	$63, %dl
	ja	.L1809
	movzwl	28(%rdi), %ecx
	cmpl	%esi, %ecx
	jg	.L1813
	movl	20(%rdi), %eax
	leal	-2(%rax), %edx
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L2005:
	movq	-208(%rbp), %r9
	movl	-236(%rbp), %ecx
	movq	%r13, %rsi
	movl	$1, %r15d
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rdx
	movq	-200(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode@PLT
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L2007:
	leaq	2(%r12), %r14
	cmpq	%r14, %r13
	je	.L1809
	movzbl	2(%r12), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L1809
	sall	$6, %ecx
	andl	$63, %edx
	movzbl	%al, %eax
	addl	%ecx, %edx
	movq	(%rdi), %rcx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	addl	%eax, %edx
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L2004:
	movzwl	%r8w, %r11d
	cmpw	30(%rbx), %r8w
	jnb	.L1814
	cmpq	$0, -216(%rbp)
	je	.L1852
	cmpw	26(%rbx), %r8w
	jb	.L1816
	testb	$1, %r8b
	je	.L1817
	cmpb	$0, -237(%rbp)
	je	.L1822
	cmpw	$1, %r8w
	je	.L1822
	movl	%r8d, %eax
	andl	$6, %eax
	cmpw	$2, %ax
	ja	.L1817
.L1822:
	cmpq	%r12, -200(%rbp)
	je	.L1821
	movq	-208(%rbp), %r9
	movl	-236(%rbp), %ecx
	movq	%r12, %rsi
	movq	%r10, -256(%rbp)
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rdx
	movl	%r11d, -248(%rbp)
	movq	-200(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode@PLT
	movl	-248(%rbp), %r11d
	movq	-256(%rbp), %r10
	testb	%al, %al
	je	.L1889
.L1821:
	movzwl	28(%rbx), %edx
	movq	%r14, %rcx
	sarl	$3, %r11d
	subq	%r12, %rcx
	subl	%edx, %r11d
	cmpl	$1, %ecx
	je	.L2008
	movzbl	-1(%r14), %edx
	addl	%r11d, %edx
	leal	-128(%rdx), %eax
	cmpl	$63, %eax
	ja	.L1828
	movzbl	(%r12), %eax
	leaq	-1(%r14), %rsi
	movb	%al, -60(%rbp)
	cmpq	%r10, %rsi
	jbe	.L1829
	movzbl	1(%r12), %eax
	movb	%al, -59(%rbp)
	leaq	2(%r12), %rax
	cmpq	%rax, %rsi
	jbe	.L1829
	movzbl	2(%r12), %eax
	movb	%al, -58(%rbp)
	leaq	3(%r12), %rax
	cmpq	%rax, %rsi
	jbe	.L1829
	movzbl	3(%r12), %eax
	movb	%al, -57(%rbp)
.L1829:
	leaq	-2(%r14), %rax
	subq	%r12, %rax
	cmpq	%r10, %rsi
	movl	$0, %r12d
	cmovnb	%eax, %r12d
	addl	$1, %eax
	addl	$2, %r12d
	cmpq	%r10, %rsi
	movl	$1, %esi
	cmovb	%esi, %eax
	cltq
	movb	%dl, -60(%rbp,%rax)
.L1827:
	movq	-232(%rbp), %rax
	testq	%rax, %rax
	je	.L1835
	movl	%r12d, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	_ZN6icu_675Edits10addReplaceEii@PLT
.L1835:
	movq	-216(%rbp), %rdi
	movl	%r12d, %edx
	leaq	-60(%rbp), %rsi
	movq	%r14, %r12
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r14, -200(%rbp)
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1814:
	cmpw	$-512, %r8w
	je	.L2009
	jbe	.L1823
	sarl	%r11d
	cmpb	$0, -237(%rbp)
	movl	%r11d, -248(%rbp)
	movl	%r11d, %r10d
	je	.L1991
	movq	-200(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	%r8d, -264(%rbp)
	movb	%r11b, -256(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl18getPreviousTrailCCEPKhS2_
	movl	-248(%rbp), %r11d
	movzbl	-256(%rbp), %r10d
	movl	-264(%rbp), %r8d
	cmpb	%r11b, %al
	jbe	.L1991
	cmpq	$0, -216(%rbp)
	jne	.L1823
.L1852:
	xorl	%r15d, %r15d
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L1816:
	cmpw	%r8w, 20(%rbx)
	jbe	.L1837
	testb	$1, %r8b
	je	.L1838
	cmpw	$1, %r8w
	je	.L1843
	cmpb	$0, -237(%rbp)
	je	.L1843
	movl	%r11d, %eax
	movq	48(%rbx), %rdx
	sarl	%eax
	cltq
	cmpw	$511, (%rdx,%rax,2)
	ja	.L1838
.L1843:
	cmpq	%r12, -200(%rbp)
	je	.L1842
	movq	-208(%rbp), %r9
	movl	-236(%rbp), %ecx
	movq	%r12, %rsi
	movl	%r11d, -248(%rbp)
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rdx
	movq	-200(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode@PLT
	movl	-248(%rbp), %r11d
	testb	%al, %al
	je	.L1889
.L1842:
	movq	48(%rbx), %rdx
	sarl	%r11d
	subq	$8, %rsp
	movq	%r12, %rdi
	movslq	%r11d, %rax
	movq	-232(%rbp), %r9
	movq	-216(%rbp), %r8
	movq	%r14, %rsi
	leaq	(%rdx,%rax,2), %rax
	movzwl	(%rax), %ecx
	pushq	-208(%rbp)
	leaq	2(%rax), %rdx
	andl	$31, %ecx
	call	_ZN6icu_6712ByteSinkUtil12appendChangeEPKhS2_PKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode@PLT
	popq	%r12
	popq	%rdx
	testb	%al, %al
	je	.L1889
.L1997:
	movq	%r14, -200(%rbp)
	movq	%r14, %r12
	jmp	.L1800
.L2009:
	movq	%r12, %rax
	subq	-200(%rbp), %rax
	movzbl	1(%r12), %ecx
	cmpq	$2, %rax
	jle	.L1823
	movzbl	-3(%r12), %eax
	leal	31(%rax), %edx
	cmpb	$12, %dl
	ja	.L1823
	movzbl	-2(%r12), %esi
	leal	-128(%rsi), %edx
	cmpb	$63, %dl
	ja	.L1823
	movzbl	-1(%r12), %esi
	addl	$-128, %esi
	cmpb	$63, %sil
	ja	.L1823
	cmpb	$-20, %al
	jbe	.L1913
	cmpb	$31, %dl
	ja	.L1823
.L1913:
	movl	%eax, %r10d
	movzbl	%dl, %edx
	movzbl	%sil, %esi
	sall	$12, %r10d
	sall	$6, %edx
	andl	$61440, %r10d
	orl	%edx, %r10d
	orl	%esi, %r10d
	cmpb	$-123, %cl
	jne	.L1851
	subl	$4352, %r10d
	cmpl	$18, %r10d
	ja	.L1823
	cmpq	$0, -216(%rbp)
	je	.L1852
	movq	%r13, %rax
	subq	%r14, %rax
	cmpq	$2, %rax
	jle	.L1853
	cmpb	$-31, (%r14)
	jne	.L1853
	movzbl	1(%r14), %eax
	cmpb	$-122, %al
	je	.L2010
	cmpb	$-121, %al
	jne	.L1853
	movzbl	2(%r14), %eax
	cmpb	$-125, %al
	jge	.L1853
	leal	-103(%rax), %r11d
.L1855:
	addq	$3, %r14
.L1891:
	movzbl	2(%r12), %eax
	subq	$3, %r12
	movb	%al, -248(%rbp)
	cmpq	%r12, -200(%rbp)
	je	.L1857
	movq	-208(%rbp), %r9
	movl	-236(%rbp), %ecx
	movq	%r12, %rsi
	movl	%r11d, -264(%rbp)
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rdx
	movl	%r10d, -256(%rbp)
	movq	-200(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode@PLT
	movl	-256(%rbp), %r10d
	movl	-264(%rbp), %r11d
	testb	%al, %al
	je	.L1889
.L1857:
	leal	(%r10,%r10,4), %eax
	leal	(%r10,%rax,4), %edx
	movzbl	-248(%rbp), %eax
	leal	-161(%rdx,%rax), %eax
	imull	$28, %eax, %eax
	leal	44032(%r11,%rax), %esi
.L1998:
	movq	-232(%rbp), %rcx
	movq	-216(%rbp), %rdx
	movq	%r14, %rdi
	subq	%r12, %rdi
	call	_ZN6icu_6712ByteSinkUtil15appendCodePointEiiRNS_8ByteSinkEPNS_5EditsE@PLT
	movq	%r14, -200(%rbp)
	jmp	.L1999
.L1838:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%r11d, -248(%rbp)
	movl	%r8d, -256(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKhS2_
	movl	-248(%rbp), %r11d
	testb	%al, %al
	jne	.L1843
	movl	-256(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L1823:
	cmpq	%r12, -200(%rbp)
	je	.L1873
	cmpw	22(%rbx), %r8w
	jb	.L1873
	cmpw	26(%rbx), %r8w
	jb	.L1874
	cmpw	30(%rbx), %r8w
	jnb	.L1874
	.p2align 4,,10
	.p2align 3
.L1873:
	movq	-224(%rbp), %rax
	movl	$8, %esi
	movq	%rbx, -192(%rbp)
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN6icu_6713UnicodeString9getBufferEi@PLT
	movq	-184(%rbp), %rcx
	movl	$27, %edx
	movq	%rax, %xmm0
	movq	%rax, -160(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -176(%rbp)
	testb	$2, 8(%rcx)
	jne	.L1877
	movl	16(%rcx), %edx
.L1877:
	movq	-208(%rbp), %rsi
	movl	%edx, -152(%rbp)
	movb	$0, -148(%rbp)
	movl	(%rsi), %edx
	testq	%rax, %rax
	je	.L2011
	testl	%edx, %edx
	jg	.L1879
	subq	$8, %rsp
	pushq	-208(%rbp)
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movsbl	-237(%rbp), %r8d
	leaq	-192(%rbp), %r9
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r9, -256(%rbp)
	movl	%r8d, -248(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0
	movq	-208(%rbp), %rax
	popq	%r9
	movl	-248(%rbp), %r8d
	movq	-256(%rbp), %r9
	movl	(%rax), %r11d
	popq	%r10
	testl	%r11d, %r11d
	jg	.L2001
	subq	$8, %rsp
	pushq	-208(%rbp)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%r9, -256(%rbp)
	movl	%r8d, -248(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKhS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0
	popq	%rsi
	movq	-256(%rbp), %r9
	movq	%rax, %r14
	movq	-208(%rbp), %rax
	popq	%rdi
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	movl	-248(%rbp), %r8d
	jg	.L2001
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L2012
	xorl	%edx, %edx
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movl	%r8d, %ecx
	movq	%r9, -248(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl9recomposeERNS_16ReorderingBufferEia
	movq	-248(%rbp), %r9
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZNK6icu_6716ReorderingBuffer6equalsEPKhS2_
	testb	%al, %al
	jne	.L1885
	movq	-216(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1994
	movq	-200(%rbp), %rax
	cmpq	%r12, %rax
	je	.L1886
	movq	-208(%rbp), %r9
	movq	-232(%rbp), %r8
	movq	%rax, %rdi
	movl	-236(%rbp), %ecx
	call	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode@PLT
	testb	%al, %al
	je	.L2001
.L1886:
	movq	-176(%rbp), %rdx
	subq	$8, %rsp
	movq	-160(%rbp), %rcx
	pushq	-208(%rbp)
	movq	-232(%rbp), %r9
	movq	%r14, %rsi
	movq	%r12, %rdi
	subq	%rdx, %rcx
	movq	-216(%rbp), %r8
	sarq	%rcx
	call	_ZN6icu_6712ByteSinkUtil12appendChangeEPKhS2_PKDsiRNS_8ByteSinkEPNS_5EditsER10UErrorCode@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	je	.L2001
	movq	%r14, -200(%rbp)
.L1885:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L1999
	movq	-160(%rbp), %rsi
	movq	-184(%rbp), %rdi
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L1999
.L1874:
	movzbl	-1(%r12), %esi
	movq	32(%rbx), %rdi
	leaq	-1(%r12), %rcx
	testb	%sil, %sil
	js	.L2013
.L1875:
	movq	8(%rdi), %rax
	movslq	%esi, %rsi
	movzwl	(%rax,%rsi,2), %eax
	testb	$1, %al
	je	.L1910
	cmpb	$0, -237(%rbp)
	je	.L1873
	cmpw	$1, %ax
	je	.L1873
	cmpw	26(%rbx), %ax
	jb	.L1876
	andl	$6, %eax
	cmpw	$3, %ax
	cmovnb	%rcx, %r12
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L2011:
	testl	%edx, %edx
	jg	.L1889
	movl	$7, (%rsi)
	jmp	.L1889
.L1817:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r10, -256(%rbp)
	movl	%r11d, -248(%rbp)
	movl	%r8d, -264(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKhS2_
	movl	-248(%rbp), %r11d
	movq	-256(%rbp), %r10
	testb	%al, %al
	jne	.L1822
	movl	-264(%rbp), %r8d
	jmp	.L1823
.L2001:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L1889
.L1879:
	movq	-160(%rbp), %rsi
	movq	-184(%rbp), %rdi
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L1889
.L1837:
	cmpw	%r8w, 24(%rbx)
	ja	.L1823
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%r8d, -248(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKhS2_
	movl	-248(%rbp), %r8d
	testb	%al, %al
	je	.L1844
.L1847:
	cmpq	%r12, -200(%rbp)
	je	.L1846
	movq	-208(%rbp), %r9
	movl	-236(%rbp), %ecx
	movq	%r12, %rsi
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rdx
	movq	-200(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode@PLT
	testb	%al, %al
	je	.L1889
.L1846:
	cmpq	$0, -232(%rbp)
	je	.L1997
	movq	%r14, %rsi
	movq	-232(%rbp), %rdi
	xorl	%edx, %edx
	subq	%r12, %rsi
	call	_ZN6icu_675Edits10addReplaceEii@PLT
	jmp	.L1997
.L1910:
	movq	%rcx, %r12
	jmp	.L1873
.L1991:
	movq	%r12, %r9
	movq	%rbx, %r12
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1864:
	movq	8(%rdi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %edx
	cmpw	$-511, %dx
	jbe	.L1992
	movzwl	%dx, %eax
	sarl	%eax
	cmpb	%al, %r10b
	ja	.L2014
	movl	%eax, %r10d
	movq	%rbx, %r14
.L1871:
	cmpq	%r13, %r14
	je	.L2015
	movzbl	(%r14), %eax
	movq	32(%r12), %rdi
	leaq	1(%r14), %rbx
	movl	%eax, %edx
	testb	%al, %al
	jns	.L1864
	cmpq	%rbx, %r13
	je	.L1865
	cmpl	$223, %eax
	jle	.L1866
	cmpl	$239, %eax
	jg	.L1867
	movzbl	1(%r14), %ecx
	movl	%eax, %esi
	andl	$15, %edx
	leaq	.LC1(%rip), %rax
	movsbl	(%rax,%rdx), %edx
	andl	$15, %esi
	movl	%ecx, %eax
	shrb	$5, %al
	btl	%eax, %edx
	jnc	.L1865
	leaq	2(%r14), %rbx
	cmpq	%rbx, %r13
	je	.L1865
	movzbl	2(%r14), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L1865
	sall	$6, %esi
	andl	$63, %ecx
	movzbl	%al, %eax
	movl	%esi, %edx
	addl	%ecx, %edx
	movq	(%rdi), %rcx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	addl	%edx, %eax
.L1868:
	addq	$1, %rbx
	jmp	.L1864
.L2013:
	movq	-200(%rbp), %rdx
	movq	%rcx, -248(%rbp)
	call	ucptrie_internalU8PrevIndex_67@PLT
	movq	-248(%rbp), %rcx
	movq	32(%rbx), %rdi
	movl	%eax, %esi
	andl	$7, %eax
	subq	%rax, %rcx
	sarl	$3, %esi
	jmp	.L1875
.L1865:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L1864
.L1866:
	cmpl	$193, %eax
	jle	.L1865
	movzbl	1(%r14), %eax
	leal	-128(%rax), %ecx
	cmpb	$63, %cl
	ja	.L1865
	movq	(%rdi), %rax
	andl	$31, %edx
	movzbl	%cl, %ecx
	movzwl	(%rax,%rdx,2), %eax
	addl	%ecx, %eax
	jmp	.L1868
.L1867:
	subl	$240, %eax
	cmpl	$4, %eax
	jg	.L1865
	movzbl	1(%r14), %edx
	leaq	.LC2(%rip), %rsi
	movq	%rdx, %rcx
	shrq	$4, %rcx
	andl	$15, %ecx
	movsbl	(%rsi,%rcx), %ecx
	btl	%eax, %ecx
	jnc	.L1865
	andl	$63, %edx
	sall	$6, %eax
	leaq	2(%r14), %rbx
	movl	%edx, %esi
	orl	%eax, %esi
	cmpq	%rbx, %r13
	je	.L1865
	movzbl	2(%r14), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L1865
	leaq	3(%r14), %rbx
	cmpq	%rbx, %r13
	je	.L1865
	movzbl	3(%r14), %ecx
	leal	-128(%rcx), %edx
	cmpb	$63, %dl
	ja	.L1865
	movzwl	28(%rdi), %ecx
	cmpl	%esi, %ecx
	jg	.L1869
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L1868
	.p2align 4,,10
	.p2align 3
.L1851:
	leal	-44032(%r10), %eax
	cmpl	$11171, %eax
	ja	.L1823
	imull	$-1227133513, %eax, %eax
	addl	$306783376, %eax
	rorl	$2, %eax
	cmpl	$153391688, %eax
	ja	.L1823
	cmpq	$0, -216(%rbp)
	je	.L1852
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$2, %rax
	jle	.L1906
	cmpb	$-31, (%r12)
	jne	.L1906
	cmpb	$-122, %cl
	je	.L2016
	cmpb	$-121, %cl
	jne	.L1906
	movzbl	2(%r12), %r11d
	movl	%r11d, %eax
	subl	$103, %r11d
	cmpb	$-125, %al
	movl	$-1, %eax
	cmovge	%eax, %r11d
.L1858:
	subq	$3, %r12
	cmpq	%r12, -200(%rbp)
	je	.L1861
	movq	-208(%rbp), %r9
	movl	-236(%rbp), %ecx
	movq	%r12, %rsi
	movl	%r11d, -256(%rbp)
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rdx
	movl	%r10d, -248(%rbp)
	movq	-200(%rbp), %rdi
	call	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode@PLT
	movl	-248(%rbp), %r10d
	movl	-256(%rbp), %r11d
	testb	%al, %al
	je	.L1889
.L1861:
	leal	(%r10,%r11), %esi
	jmp	.L1998
.L2015:
	movq	-216(%rbp), %rax
	testq	%rax, %rax
	je	.L1889
	movq	-208(%rbp), %r9
	movl	-236(%rbp), %ecx
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	-232(%rbp), %r8
	movq	-200(%rbp), %rdi
	movl	$1, %r15d
	call	_ZN6icu_6712ByteSinkUtil15appendUnchangedEPKhS2_RNS_8ByteSinkEjPNS_5EditsER10UErrorCode@PLT
	jmp	.L1890
.L2014:
	movq	%r9, %rax
	cmpq	$0, -216(%rbp)
	movq	%rbx, %r9
	movq	%r12, %rbx
	movq	%rax, %r12
	je	.L1852
.L1870:
	cmpw	22(%rbx), %dx
	jb	.L1872
	cmpw	26(%rbx), %dx
	jb	.L1823
	cmpw	30(%rbx), %dx
	jnb	.L1823
.L1872:
	cmpw	18(%rbx), %dx
	cmovnb	%r14, %r9
	movq	%r9, %r12
	jmp	.L1800
.L1992:
	movq	%r9, %rax
	movq	%rbx, %r9
	movq	%r12, %rbx
	movq	%rax, %r12
	jmp	.L1870
.L2008:
	movzbl	(%r12), %eax
	movl	$1, %r12d
	addl	%r11d, %eax
	movb	%al, -60(%rbp)
	jmp	.L1827
.L1828:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rcx, -248(%rbp)
	movl	%r11d, -200(%rbp)
	call	_ZN6icu_6712_GLOBAL__N_122codePointFromValidUTF8EPKhS2_
	movl	-200(%rbp), %r11d
	movq	-248(%rbp), %rcx
	addl	%r11d, %eax
	cmpl	$127, %eax
	ja	.L1830
	movb	%al, -60(%rbp)
	movl	$1, %r12d
	jmp	.L1827
.L1813:
	movzbl	%dl, %ecx
	movzbl	%al, %edx
	movq	%r10, -248(%rbp)
	call	ucptrie_internalSmallU8Index_67@PLT
	movq	32(%rbx), %rdi
	movq	-248(%rbp), %r10
	movl	%eax, %edx
	jmp	.L1812
.L2012:
	movq	-208(%rbp), %rax
	movl	$1, %r15d
	movl	$8, (%rax)
.L1884:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L1890
	movq	-160(%rbp), %rsi
	movq	-184(%rbp), %rdi
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
	jmp	.L1890
.L1876:
	movq	48(%rbx), %rdx
	andl	$65534, %eax
	cmpw	$511, (%rax,%rdx)
	cmova	%rcx, %r12
	jmp	.L1873
.L1844:
	movsbl	-237(%rbp), %ecx
	movq	-200(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movl	%r8d, -248(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl20hasCompBoundaryAfterEPKhS2_a
	testb	%al, %al
	jne	.L1847
	movl	-248(%rbp), %r8d
	jmp	.L1823
.L2010:
	movzbl	2(%r14), %eax
	leal	88(%rax), %edx
	cmpb	$23, %dl
	ja	.L1853
	leal	-167(%rax), %r11d
	jmp	.L1855
.L1853:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%r8d, -248(%rbp)
	movl	%r10d, -256(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl21hasCompBoundaryBeforeEPKhS2_
	movl	-248(%rbp), %r8d
	testb	%al, %al
	je	.L1823
	movl	-256(%rbp), %r10d
	xorl	%r11d, %r11d
	jmp	.L1891
.L1830:
	movl	%eax, %edx
	shrl	$6, %edx
	cmpl	$2047, %eax
	ja	.L1831
	orl	$-64, %edx
	movl	$2, %r12d
	movl	$1, %esi
	movb	%dl, -60(%rbp)
.L1832:
	andl	$63, %eax
	movslq	%esi, %rsi
	orl	$-128, %eax
	movb	%al, -60(%rbp,%rsi)
	jmp	.L1827
.L2016:
	movzbl	2(%r12), %r11d
	movl	$-1, %eax
	leal	88(%r11), %edx
	subl	$167, %r11d
	cmpb	$24, %dl
	cmovnb	%eax, %r11d
	jmp	.L1858
.L1994:
	movl	%eax, %r15d
	jmp	.L1884
.L1906:
	orl	$-1, %r11d
	jmp	.L1858
.L1831:
	movl	%eax, %esi
	shrl	$12, %esi
	cmpl	$65535, %eax
	ja	.L1833
	movl	%esi, %edi
	movl	$1, %r12d
	orl	$-32, %edi
.L1834:
	andl	$63, %edx
	movb	%dil, -60(%rbp)
	movslq	%r12d, %rdi
	leal	1(%r12), %esi
	orl	$-128, %edx
	addl	$2, %r12d
	movb	%dl, -60(%rbp,%rdi)
	jmp	.L1832
.L1833:
	movl	%eax, %edi
	andl	$63, %esi
	movl	$2, %r12d
	shrl	$18, %edi
	orl	$-128, %esi
	movb	%sil, -59(%rbp)
	orl	$-16, %edi
	jmp	.L1834
.L2006:
	call	__stack_chk_fail@PLT
.L1869:
	movzbl	%dl, %ecx
	movzbl	%al, %edx
	movq	%r9, -264(%rbp)
	movl	%r8d, -256(%rbp)
	movb	%r10b, -248(%rbp)
	call	ucptrie_internalSmallU8Index_67@PLT
	movq	32(%r12), %rdi
	movzbl	-248(%rbp), %r10d
	movl	-256(%rbp), %r8d
	movq	-264(%rbp), %r9
	jmp	.L1868
	.cfi_endproc
.LFE3249:
	.size	_ZNK6icu_6715Normalizer2Impl11composeUTF8EjaPKhS2_PNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl11composeUTF8EjaPKhS2_PNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	.type	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi, @function
_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi:
.LFB3258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	andl	$-1024, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpl	$55296, %eax
	je	.L2040
	movq	32(%rdi), %rdi
	movq	8(%rdi), %r12
	cmpl	$65535, %esi
	ja	.L2021
	movl	%esi, %eax
	movq	(%rdi), %rdx
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%esi, %edx
	andl	$63, %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
.L2022:
	movzwl	(%r12,%rax), %eax
	cmpw	%ax, 26(%rbx)
	ja	.L2020
.L2041:
	cmpw	$-1025, %ax
	jbe	.L2025
	sarl	%eax
	movl	%eax, %edx
	movzbl	%al, %eax
	sall	$8, %edx
	orl	%edx, %eax
.L2017:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2021:
	.cfi_restore_state
	cmpl	$1114111, %esi
	ja	.L2023
	cmpl	24(%rdi), %esi
	jl	.L2024
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	movzwl	(%r12,%rax), %eax
	cmpw	%ax, 26(%rbx)
	jbe	.L2041
	.p2align 4,,10
	.p2align 3
.L2020:
	cmpw	%ax, 14(%rbx)
	jnb	.L2039
	movzwl	16(%rbx), %ecx
	movzwl	%ax, %edx
	orl	$1, %ecx
	cmpw	%cx, %ax
	je	.L2039
	movq	48(%rbx), %rax
	sarl	%edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rsi
	movzwl	(%rsi), %edx
	movzbl	%dh, %ecx
	andl	$128, %edx
	movl	%ecx, %eax
	je	.L2017
	movzwl	-2(%rsi), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	xorb	%al, %al
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	orl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2040:
	.cfi_restore_state
	cmpw	$1, 26(%rdi)
	movl	$1, %eax
	ja	.L2020
.L2039:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2023:
	.cfi_restore_state
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L2022
	.p2align 4,,10
	.p2align 3
.L2025:
	cmpw	%ax, 30(%rbx)
	jbe	.L2039
	movl	%eax, %edx
	andl	$6, %edx
	cmpw	$2, %dx
	jbe	.L2042
	sarl	$3, %eax
	movq	32(%rbx), %rdi
	addl	%eax, %esi
	movzwl	28(%rbx), %eax
	movq	8(%rdi), %r12
	subl	%eax, %esi
	cmpl	$65535, %esi
	ja	.L2029
	movl	%esi, %eax
	movq	(%rdi), %rdx
	andl	$63, %esi
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
.L2030:
	movzwl	(%r12,%rax), %eax
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2024:
	movl	%esi, -20(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movl	-20(%rbp), %esi
	cltq
	addq	%rax, %rax
	jmp	.L2022
	.p2align 4,,10
	.p2align 3
.L2029:
	cmpl	$1114111, %esi
	ja	.L2031
	cmpl	24(%rdi), %esi
	jl	.L2032
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L2031:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L2030
.L2032:
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L2030
.L2042:
	movl	%edx, %eax
	shrl	%eax
	andl	$3, %eax
	jmp	.L2017
	.cfi_endproc
.LFE3258:
	.size	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi, .-_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl17addPropertyStartsEPK9USetAdderR10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl17addPropertyStartsEPK9USetAdderR10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl17addPropertyStartsEPK9USetAdderR10UErrorCode:
.LFB3226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-60(%rbp), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2078:
	movq	(%r12), %rdi
	movl	%ebx, %esi
	call	*8(%r12)
	cmpl	%r13d, %ebx
	je	.L2045
	movl	-60(%rbp), %eax
	cmpw	26(%r14), %ax
	jb	.L2045
	cmpw	30(%r14), %ax
	jnb	.L2045
	andl	$6, %eax
	cmpl	$2, %eax
	ja	.L2077
	.p2align 4,,10
	.p2align 3
.L2045:
	leal	1(%r13), %ebx
.L2058:
	subq	$8, %rsp
	pushq	-72(%rbp)
	movq	32(%r14), %rdi
	movl	$1, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%ebx, %esi
	call	ucptrie_getRange_67@PLT
	movl	%eax, %r13d
	popq	%rax
	popq	%rdx
	testl	%r13d, %r13d
	jns	.L2078
	movl	$44032, %ebx
	.p2align 4,,10
	.p2align 3
.L2044:
	movl	%ebx, %esi
	movq	(%r12), %rdi
	call	*8(%r12)
	leal	1(%rbx), %esi
	addl	$28, %ebx
	movq	(%r12), %rdi
	call	*8(%r12)
	cmpl	$55204, %ebx
	jne	.L2044
	movq	(%r12), %rdi
	movl	$55204, %esi
	call	*8(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2079
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2077:
	.cfi_restore_state
	movzwl	8(%r14), %eax
	xorl	%ecx, %ecx
	cmpl	%eax, %ebx
	jl	.L2046
	cmpl	$65535, %ebx
	jg	.L2047
	movl	%ebx, %eax
	movq	56(%r14), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L2046
	movl	%ebx, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L2046
.L2047:
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	movl	%eax, %ecx
.L2046:
	leal	1(%rbx), %r15d
	cmpl	%r13d, %r15d
	jg	.L2045
	cmpl	$65535, %r15d
	jg	.L2054
	cmpl	$65535, %r13d
	movl	$65535, %ebx
	movl	%ecx, %r8d
	cmovle	%r13d, %ebx
	jmp	.L2050
	.p2align 4,,10
	.p2align 3
.L2064:
	movl	%ecx, %r8d
.L2050:
	movzwl	8(%r14), %eax
	xorl	%ecx, %ecx
	cmpl	%r15d, %eax
	jg	.L2056
	movl	%r15d, %eax
	movq	56(%r14), %rsi
	sarl	$8, %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	testb	%al, %al
	je	.L2056
	movl	%r15d, %esi
	shrb	$5, %sil
	btl	%esi, %eax
	jnc	.L2056
	movl	%r15d, %esi
	movq	%r14, %rdi
	movl	%r8d, -76(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	movl	-76(%rbp), %r8d
	movl	%eax, %ecx
.L2056:
	cmpw	%r8w, %cx
	je	.L2057
	movl	%ecx, -76(%rbp)
	movq	(%r12), %rdi
	movl	%r15d, %esi
	call	*8(%r12)
	movl	-76(%rbp), %ecx
.L2057:
	addl	$1, %r15d
	cmpl	%ebx, %r15d
	jle	.L2064
	cmpl	%r15d, %r13d
	jge	.L2054
	jmp	.L2045
	.p2align 4,,10
	.p2align 3
.L2066:
	movl	%ebx, %ecx
.L2054:
	movzwl	8(%r14), %eax
	xorl	%ebx, %ebx
	cmpl	%r15d, %eax
	jg	.L2052
	movl	%r15d, %esi
	movq	%r14, %rdi
	movl	%ecx, -76(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	movl	-76(%rbp), %ecx
	movl	%eax, %ebx
.L2052:
	cmpw	%cx, %bx
	je	.L2053
	movq	(%r12), %rdi
	movl	%r15d, %esi
	call	*8(%r12)
.L2053:
	addl	$1, %r15d
	cmpl	%r15d, %r13d
	jge	.L2066
	jmp	.L2045
.L2079:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3226:
	.size	_ZNK6icu_6715Normalizer2Impl17addPropertyStartsEPK9USetAdderR10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl17addPropertyStartsEPK9USetAdderR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl23findPreviousFCDBoundaryEPKDsS2_
	.type	_ZNK6icu_6715Normalizer2Impl23findPreviousFCDBoundaryEPKDsS2_, @function
_ZNK6icu_6715Normalizer2Impl23findPreviousFCDBoundaryEPKDsS2_:
.LFB3261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
.L2093:
	cmpq	%r13, %r12
	jbe	.L2080
	movzwl	-2(%r12), %r15d
	movq	32(%rbx), %rdi
	leaq	-2(%r12), %r14
	movl	%r15d, %edx
	movl	%r15d, %eax
	andl	$63488, %edx
	cmpl	$55296, %edx
	je	.L2082
	movl	%r15d, %edx
	movq	(%rdi), %rcx
	andl	$63, %eax
	sarl	$6, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %ecx
	addl	%ecx, %eax
.L2083:
	movq	8(%rdi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movzwl	8(%rbx), %edx
	cmpl	%r15d, %edx
	jg	.L2080
	cmpw	14(%rbx), %ax
	jbe	.L2080
	movzwl	16(%rbx), %edx
	orl	$1, %edx
	cmpw	%dx, %ax
	je	.L2080
	movzwl	26(%rbx), %ecx
	movzwl	%ax, %edx
	cmpw	%cx, %ax
	jb	.L2087
	cmpw	30(%rbx), %ax
	jb	.L2088
	cmpw	$-1024, %ax
	setbe	%sil
	cmpw	$-512, %ax
	sete	%dil
	orl	%edi, %esi
.L2089:
	testb	%sil, %sil
	jne	.L2080
	cmpw	22(%rbx), %ax
	jb	.L2098
	cmpw	%cx, %ax
	jnb	.L2110
	movq	48(%rbx), %rax
	sarl	%edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rdi
	movzwl	(%rdi), %esi
.L2094:
	andl	$128, %esi
	je	.L2098
	testw	$-256, -2(%rdi)
	sete	%al
.L2092:
	movq	%r14, %r12
	testb	%al, %al
	je	.L2093
.L2080:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2090:
	.cfi_restore_state
	cmpw	22(%rbx), %ax
	jnb	.L2094
	.p2align 4,,10
	.p2align 3
.L2098:
	movq	%r14, %r12
	jmp	.L2080
	.p2align 4,,10
	.p2align 3
.L2082:
	testb	$4, %ah
	je	.L2084
	cmpq	%r14, %r13
	je	.L2084
	movzwl	-4(%r12), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L2111
.L2084:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2087:
	movl	%edx, %esi
	movq	48(%rbx), %rdi
	sarl	%esi
	movslq	%esi, %rsi
	leaq	(%rdi,%rsi,2), %rdi
	movzwl	(%rdi), %esi
	cmpw	$511, %si
	ja	.L2090
	cmpw	$255, %si
	jbe	.L2080
	andl	$128, %esi
	je	.L2080
	testw	$-256, -2(%rdi)
	sete	%sil
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2110:
	cmpw	$-1024, %ax
	setbe	%dl
	cmpw	$-512, %ax
	sete	%al
	orl	%edx, %eax
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2088:
	movl	%eax, %esi
	andl	$6, %esi
	cmpw	$2, %si
	setbe	%sil
	jmp	.L2089
	.p2align 4,,10
	.p2align 3
.L2111:
	sall	$10, %eax
	leaq	-4(%r12), %r14
	leal	-56613888(%r15,%rax), %r15d
	cmpl	%r15d, 24(%rdi)
	jg	.L2112
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L2083
	.p2align 4,,10
	.p2align 3
.L2112:
	movl	%r15d, %esi
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%rbx), %rdi
	jmp	.L2083
	.cfi_endproc
.LFE3261:
	.size	_ZNK6icu_6715Normalizer2Impl23findPreviousFCDBoundaryEPKDsS2_, .-_ZNK6icu_6715Normalizer2Impl23findPreviousFCDBoundaryEPKDsS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl19findNextFCDBoundaryEPKDsS2_
	.type	_ZNK6icu_6715Normalizer2Impl19findNextFCDBoundaryEPKDsS2_, @function
_ZNK6icu_6715Normalizer2Impl19findNextFCDBoundaryEPKDsS2_:
.LFB3262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	%rdx, %rsi
	jnb	.L2131
	movq	%rdi, %rbx
	movq	%rdx, %r14
	movq	32(%rdi), %rdi
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2147:
	movl	%r15d, %edx
	movq	(%rdi), %rcx
	andl	$63, %eax
	sarl	$6, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %ecx
	addl	%ecx, %eax
.L2117:
	movq	8(%rdi), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movzwl	12(%rbx), %edx
	cmpl	%r15d, %edx
	jg	.L2131
	cmpw	22(%rbx), %ax
	jb	.L2131
	movzwl	26(%rbx), %esi
	movzwl	%ax, %ecx
	cmpw	%si, %ax
	jnb	.L2146
	movl	%ecx, %edx
	movq	48(%rbx), %r8
	sarl	%edx
	movslq	%edx, %rdx
	leaq	(%r8,%rdx,2), %rdx
	testb	$-128, (%rdx)
	je	.L2131
	testw	$-256, -2(%rdx)
	sete	%dl
.L2122:
	testb	%dl, %dl
	jne	.L2131
	cmpw	14(%rbx), %ax
	jbe	.L2113
	movzwl	16(%rbx), %edx
	orl	$1, %edx
	cmpw	%dx, %ax
	je	.L2113
	cmpw	%si, %ax
	jb	.L2123
	cmpw	30(%rbx), %ax
	jb	.L2124
	cmpw	$-1024, %ax
	setbe	%dl
	cmpw	$-512, %ax
	sete	%al
	orl	%edx, %eax
.L2125:
	testb	%al, %al
	jne	.L2113
.L2126:
	cmpq	%r14, %r13
	jnb	.L2113
	movq	%r13, %r12
.L2115:
	movzwl	(%r12), %r15d
	leaq	2(%r12), %r13
	movl	%r15d, %edx
	movl	%r15d, %eax
	andl	$63488, %edx
	cmpl	$55296, %edx
	jne	.L2147
	testb	$4, %ah
	jne	.L2118
	cmpq	%r13, %r14
	je	.L2118
	movzwl	2(%r12), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L2148
.L2118:
	movl	20(%rdi), %eax
	subl	$1, %eax
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2131:
	movq	%r12, %r13
.L2113:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2123:
	.cfi_restore_state
	movq	48(%rbx), %rax
	sarl	%ecx
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,2), %rdx
	movzwl	(%rdx), %eax
	cmpw	$511, %ax
	ja	.L2126
	cmpw	$255, %ax
	jbe	.L2113
	testb	$-128, %al
	je	.L2113
	testw	$-256, -2(%rdx)
	sete	%al
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2146:
	cmpw	$-1024, %ax
	setbe	%dl
	cmpw	$-512, %ax
	sete	%r8b
	orl	%r8d, %edx
	jmp	.L2122
	.p2align 4,,10
	.p2align 3
.L2124:
	andl	$6, %eax
	cmpw	$2, %ax
	setbe	%al
	jmp	.L2125
	.p2align 4,,10
	.p2align 3
.L2148:
	sall	$10, %r15d
	leaq	4(%r12), %r13
	leal	-56613888(%rax,%r15), %r15d
	cmpl	%r15d, 24(%rdi)
	jg	.L2149
	movl	20(%rdi), %eax
	subl	$2, %eax
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2149:
	movl	%r15d, %esi
	call	ucptrie_internalSmallIndex_67@PLT
	movq	32(%rbx), %rdi
	jmp	.L2117
	.cfi_endproc
.LFE3262:
	.size	_ZNK6icu_6715Normalizer2Impl19findNextFCDBoundaryEPKDsS2_, .-_ZNK6icu_6715Normalizer2Impl19findNextFCDBoundaryEPKDsS2_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode:
.LFB3259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%r8, -80(%rbp)
	movq	%rsi, -72(%rbp)
	testq	%rdx, %rdx
	je	.L2251
.L2157:
	cmpq	%r15, %r11
	je	.L2225
	movq	%r11, %rdx
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2252:
	notl	%ebx
	addq	$2, %rdx
	movl	%ebx, %r13d
.L2161:
	cmpq	%r15, %rdx
	je	.L2159
.L2158:
	movzwl	(%rdx), %ebx
	movl	%ebx, %eax
	cmpw	12(%r12), %bx
	jb	.L2252
	movq	56(%r12), %rcx
	sarl	$8, %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	testb	%al, %al
	je	.L2162
	movl	%ebx, %ecx
	shrb	$5, %cl
	btl	%ecx, %eax
	jnc	.L2162
	movl	%ebx, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L2253
.L2164:
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%r11, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r11
	cmpw	$255, %ax
	movl	%eax, %r8d
	jbe	.L2194
.L2165:
	cmpq	%rdx, %r11
	jne	.L2183
	movzwl	%r8w, %ecx
.L2171:
	cmpl	$65536, %ebx
	movzbl	%r13b, %r13d
	sbbq	%rax, %rax
	andq	$-2, %rax
	leaq	4(%rdx,%rax), %r11
	movl	%ecx, %eax
	sarl	$8, %eax
	cmpl	%r13d, %eax
	jge	.L2190
	testq	%r14, %r14
	je	.L2210
	movq	32(%r14), %rdi
	movq	16(%r14), %rax
	subq	-72(%rbp), %rdx
	movq	%rdi, %rcx
	sarq	%rdx
	subq	%rax, %rcx
	movslq	%edx, %rsi
	sarq	%rcx
	cmpq	%rcx, %rsi
	jge	.L2180
	addq	%rsi, %rsi
	movq	%rdi, %rax
	addl	%edx, 40(%r14)
	subq	%rsi, %rax
	movq	%rax, 32(%r14)
.L2181:
	movb	$0, 44(%r14)
	movq	%r11, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, 24(%r14)
	call	_ZNK6icu_6715Normalizer2Impl19findNextFCDBoundaryEPKDsS2_
	movq	-80(%rbp), %rbx
	movq	%rax, %r11
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L2215
	subq	$8, %rsp
	movq	-72(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	pushq	%rbx
	movq	%rax, %rdx
	movq	%r14, %r9
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl14decomposeShortEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode.part.0
	movl	(%rbx), %ecx
	popq	%rax
	movq	-56(%rbp), %r11
	popq	%rdx
	testl	%ecx, %ecx
	jg	.L2215
	movq	%r11, -72(%rbp)
	xorl	%r13d, %r13d
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2162:
	addq	$2, %rdx
	xorl	%r13d, %r13d
	cmpq	%r15, %rdx
	jne	.L2158
.L2159:
	testq	%r14, %r14
	je	.L2225
	movq	-80(%rbp), %rcx
	movq	%r11, %rsi
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movq	-56(%rbp), %rdx
	testb	%al, %al
	jne	.L2205
.L2225:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2194:
	.cfi_restore_state
	movzwl	%r8w, %r13d
	movl	$2, %eax
.L2166:
	addq	%rax, %rdx
	jmp	.L2161
	.p2align 4,,10
	.p2align 3
.L2253:
	leaq	2(%rdx), %rax
	cmpq	%rax, %r15
	je	.L2164
	movzwl	2(%rdx), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L2164
	sall	$10, %ebx
	movq	%r12, %rdi
	movq	%r11, -64(%rbp)
	leal	-56613888(%rax,%rbx), %ebx
	movq	%rdx, -56(%rbp)
	movl	%ebx, %esi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r11
	cmpw	$255, %ax
	movl	%eax, %r8d
	ja	.L2165
	cmpl	$65536, %ebx
	movzwl	%ax, %r13d
	sbbq	%rax, %rax
	andq	$-2, %rax
	addq	$4, %rax
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2261:
	movzwl	8(%r12), %eax
	notl	%r13d
	cmpl	%r13d, %eax
	jle	.L2254
	cmpl	$65535, %ebx
	ja	.L2255
	movq	%rdx, -72(%rbp)
	leaq	2(%rdx), %r11
	.p2align 4,,10
	.p2align 3
.L2190:
	movq	-72(%rbp), %rax
	andl	$254, %r8d
	cmove	%r11, %rax
	movq	%rax, -72(%rbp)
	testq	%r14, %r14
	je	.L2250
	movl	40(%r14), %eax
	cmpl	$65535, %ebx
	ja	.L2256
	testl	%eax, %eax
	jle	.L2257
	subl	$1, %eax
	movl	%eax, 40(%r14)
.L2187:
	movq	32(%r14), %rdx
	leaq	2(%rdx), %rax
	movq	%rax, 32(%r14)
	movw	%bx, (%rdx)
.L2179:
	movb	$0, 44(%r14)
	movq	%rax, 24(%r14)
.L2250:
	movl	%ecx, %r13d
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2180:
	movq	8(%r14), %rcx
	movq	%rax, 32(%r14)
	movl	$27, %edx
	testb	$2, 8(%rcx)
	jne	.L2182
	movl	16(%rcx), %edx
.L2182:
	movl	%edx, 40(%r14)
	jmp	.L2181
.L2257:
	movq	-80(%rbp), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r11, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %r11
	testb	%al, %al
	jne	.L2258
.L2215:
	movq	%r11, %r15
	jmp	.L2225
	.p2align 4,,10
	.p2align 3
.L2256:
	cmpl	$1, %eax
	jle	.L2259
	subl	$2, %eax
	movl	%eax, 40(%r14)
.L2184:
	movl	%ebx, %edx
	movq	32(%r14), %rax
	andw	$1023, %bx
	sarl	$10, %edx
	orw	$-9216, %bx
	subw	$10304, %dx
	movw	%bx, 2(%rax)
	addq	$4, %rax
	movw	%dx, -4(%rax)
	movq	%rax, 32(%r14)
	jmp	.L2179
.L2251:
	movzwl	12(%rdi), %ecx
	movq	%rsi, %rax
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2260:
	cmpw	%dx, %cx
	jbe	.L2216
.L2152:
	movq	%rax, %r15
	movzwl	(%rax), %edx
	addq	$2, %rax
	testw	%dx, %dx
	jne	.L2260
.L2216:
	testq	%r14, %r14
	je	.L2154
	cmpq	%r15, %r11
	je	.L2154
	movq	-80(%rbp), %rcx
	movq	%r11, %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%r11, -56(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movq	-56(%rbp), %r11
.L2154:
	movq	-80(%rbp), %rax
	movl	(%rax), %edi
	testl	%edi, %edi
	jg	.L2225
	cmpq	%r15, %r11
	jnb	.L2199
	movzwl	-2(%r15), %esi
	movzwl	-2(%r15), %eax
	cmpw	%ax, 8(%r12)
	ja	.L2202
	movq	56(%r12), %rdx
	movl	%esi, %eax
	movzbl	%ah, %eax
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L2202
	movl	%esi, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L2202
	movq	%r12, %rdi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	movzwl	%ax, %r13d
	cmpl	$1, %r13d
	jle	.L2203
	leaq	-2(%r15), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2156
.L2259:
	movq	-80(%rbp), %rdx
	movl	$2, %esi
	movq	%r14, %rdi
	movq	%r11, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN6icu_6716ReorderingBuffer6resizeEiR10UErrorCode
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %r11
	testb	%al, %al
	je	.L2215
	subl	$2, 40(%r14)
	jmp	.L2184
.L2199:
	movq	%r11, -72(%rbp)
	xorl	%r13d, %r13d
.L2156:
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	u_strchr_67@PLT
	movq	%r15, %r11
	movq	%rax, %r15
	jmp	.L2157
.L2202:
	movq	%r15, -72(%rbp)
	xorl	%r13d, %r13d
	jmp	.L2156
.L2210:
	movq	-72(%rbp), %r15
	jmp	.L2225
.L2203:
	movq	%r15, -72(%rbp)
	jmp	.L2156
.L2258:
	subl	$1, 40(%r14)
	jmp	.L2187
.L2183:
	testq	%r14, %r14
	je	.L2167
	movq	-80(%rbp), %rcx
	movq	%r11, %rsi
	movq	%r14, %rdi
	movl	%r8d, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %rdx
	testb	%al, %al
	movl	-72(%rbp), %r8d
	je	.L2205
.L2167:
	cmpq	%rdx, %r15
	je	.L2205
	movzwl	%r8w, %ecx
	testl	%r13d, %r13d
	js	.L2261
	movzwl	-2(%rdx), %eax
	leaq	-2(%rdx), %rdi
	movq	%rdi, -72(%rbp)
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$56320, %esi
	jne	.L2172
	cmpq	%rdi, %r11
	jb	.L2262
.L2172:
	movq	-72(%rbp), %rax
	cmpl	$1, %r13d
	cmovle	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L2171
.L2255:
	movq	%rdx, -72(%rbp)
	leaq	4(%rdx), %r11
	jmp	.L2190
.L2254:
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%ecx, -84(%rbp)
	movl	%r8d, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	movq	-56(%rbp), %rdx
	movl	-64(%rbp), %r8d
	movzwl	%ax, %r13d
	movl	-84(%rbp), %ecx
	leaq	-2(%rdx), %rax
	cmpl	$1, %r13d
	cmovle	%rdx, %rax
	movq	%rax, -72(%rbp)
	jmp	.L2171
.L2262:
	movzwl	-4(%rdx), %esi
	movl	%esi, %edi
	andl	$-1024, %edi
	cmpl	$55296, %edi
	jne	.L2172
	leaq	-4(%rdx), %rdi
	sall	$10, %esi
	movl	%ecx, -84(%rbp)
	movq	%rdi, -72(%rbp)
	leal	-56613888(%rax,%rsi), %esi
	movq	%r12, %rdi
	movl	%r8d, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	movl	-84(%rbp), %ecx
	movl	-64(%rbp), %r8d
	movq	-56(%rbp), %rdx
	movzwl	%ax, %r13d
	jmp	.L2172
.L2205:
	movq	%rdx, %r15
	jmp	.L2225
	.cfi_endproc
.LFE3259:
	.size	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl16makeFCDAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl16makeFCDAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl16makeFCDAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode:
.LFB3260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movl	%ecx, -132(%rbp)
	movq	16(%rbp), %r15
	movq	%r8, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r9), %rax
	cmpq	%rax, 16(%r9)
	je	.L2264
	call	_ZNK6icu_6715Normalizer2Impl19findNextFCDBoundaryEPKDsS2_
	movq	%rax, -160(%rbp)
	cmpq	%rax, %r12
	je	.L2264
	movq	32(%r14), %rdx
	movq	16(%r14), %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6715Normalizer2Impl23findPreviousFCDBoundaryEPKDsS2_
	movq	32(%r14), %rcx
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rdi, -152(%rbp)
	subq	%rax, %rcx
	sarq	%rcx
	movl	%ecx, %edx
	movq	%rcx, -168(%rbp)
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movq	32(%r14), %rdi
	movq	16(%r14), %rax
	movq	-168(%rbp), %rcx
	movq	%rdi, %rdx
	subq	%rax, %rdx
	movslq	%ecx, %rsi
	sarq	%rdx
	cmpq	%rdx, %rsi
	jl	.L2281
	movq	8(%r14), %rcx
	movq	%rax, 32(%r14)
	movl	$27, %edx
	testb	$2, 8(%rcx)
	jne	.L2267
	movl	16(%rcx), %edx
.L2267:
	movl	%edx, 40(%r14)
.L2266:
	movb	$0, 44(%r14)
	movq	-152(%rbp), %rsi
	movq	%rax, 24(%r14)
	movq	-144(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-160(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	-152(%rbp), %rdi
	subq	%r12, %rcx
	sarq	%rcx
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movswl	-120(%rbp), %eax
	testb	$17, %al
	jne	.L2278
	leaq	-118(%rbp), %rsi
	testb	$2, %al
	cmove	-104(%rbp), %rsi
.L2268:
	testw	%ax, %ax
	js	.L2269
	sarl	$5, %eax
.L2270:
	cltq
	movq	%rbx, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode
	movl	(%r15), %eax
	movq	-152(%rbp), %rdi
	testl	%eax, %eax
	jg	.L2282
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-160(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L2264:
	cmpb	$0, -132(%rbp)
	jne	.L2283
	testq	%r13, %r13
	je	.L2284
.L2274:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN6icu_6716ReorderingBuffer12appendZeroCCEPKDsS2_R10UErrorCode
.L2263:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2285
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2283:
	.cfi_restore_state
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK6icu_6715Normalizer2Impl7makeFCDEPKDsS2_PNS_16ReorderingBufferER10UErrorCode
	jmp	.L2263
	.p2align 4,,10
	.p2align 3
.L2281:
	addq	%rsi, %rsi
	movq	%rdi, %rax
	addl	%ecx, 40(%r14)
	subq	%rsi, %rax
	movq	%rax, 32(%r14)
	jmp	.L2266
	.p2align 4,,10
	.p2align 3
.L2284:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	u_strchr_67@PLT
	movq	%rax, %r13
	jmp	.L2274
	.p2align 4,,10
	.p2align 3
.L2282:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L2263
	.p2align 4,,10
	.p2align 3
.L2269:
	movl	-116(%rbp), %eax
	jmp	.L2270
	.p2align 4,,10
	.p2align 3
.L2278:
	xorl	%esi, %esi
	jmp	.L2268
.L2285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3260:
	.size	_ZNK6icu_6715Normalizer2Impl16makeFCDAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl16makeFCDAndAppendEPKDsS2_aRNS_13UnicodeStringERNS_16ReorderingBufferER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CanonIterDataC2ER10UErrorCode
	.type	_ZN6icu_6713CanonIterDataC2ER10UErrorCode, @function
_ZN6icu_6713CanonIterDataC2ER10UErrorCode:
.LFB3264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	xorl	%edi, %edi
	call	umutablecptrie_open_67@PLT
	leaq	16(%rbx), %rdi
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%rax, (%rbx)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	$0, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	.cfi_endproc
.LFE3264:
	.size	_ZN6icu_6713CanonIterDataC2ER10UErrorCode, .-_ZN6icu_6713CanonIterDataC2ER10UErrorCode
	.globl	_ZN6icu_6713CanonIterDataC1ER10UErrorCode
	.set	_ZN6icu_6713CanonIterDataC1ER10UErrorCode,_ZN6icu_6713CanonIterDataC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CanonIterData13addToStartSetEiiR10UErrorCode
	.type	_ZN6icu_6713CanonIterData13addToStartSetEiiR10UErrorCode, @function
_ZN6icu_6713CanonIterData13addToStartSetEiiR10UErrorCode:
.LFB3269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movl	%edx, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	call	umutablecptrie_get_67@PLT
	movl	%eax, %ebx
	testl	$4194303, %eax
	jne	.L2289
	testl	%r13d, %r13d
	jne	.L2304
.L2289:
	testl	$2097152, %ebx
	jne	.L2290
	movl	$200, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L2291
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN6icu_6710UnicodeSetC1Ev@PLT
	movl	24(%r12), %edx
	movl	%ebx, %r9d
	movq	%r14, %rcx
	andl	$-4194304, %ebx
	movq	(%r12), %rdi
	andl	$2097151, %r9d
	movl	%r15d, %esi
	orl	%ebx, %edx
	movl	%r9d, -60(%rbp)
	orl	$2097152, %edx
	call	umutablecptrie_set_67@PLT
	movq	-56(%rbp), %r8
	leaq	16(%r12), %rdi
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	_ZN6icu_677UVector10addElementEPvR10UErrorCode@PLT
	movl	-60(%rbp), %r9d
	movq	-56(%rbp), %r8
	testl	%r9d, %r9d
	je	.L2292
	movq	%r8, %rdi
	movl	%r9d, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	movq	-56(%rbp), %r8
	jmp	.L2292
	.p2align 4,,10
	.p2align 3
.L2290:
	andl	$2097151, %ebx
	leaq	16(%r12), %rdi
	movl	%ebx, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, %r8
.L2292:
	addq	$24, %rsp
	movl	%r13d, %esi
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet3addEi@PLT
	.p2align 4,,10
	.p2align 3
.L2304:
	.cfi_restore_state
	movq	(%r12), %rdi
	movl	%r13d, %edx
	addq	$24, %rsp
	movq	%r14, %rcx
	popq	%rbx
	movl	%r15d, %esi
	popq	%r12
	orl	%eax, %edx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	umutablecptrie_set_67@PLT
.L2291:
	.cfi_restore_state
	movl	$7, (%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3269:
	.size	_ZN6icu_6713CanonIterData13addToStartSetEiiR10UErrorCode, .-_ZN6icu_6713CanonIterData13addToStartSetEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl27makeCanonIterDataFromNorm16EiitRNS_13CanonIterDataER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl27makeCanonIterDataFromNorm16EiitRNS_13CanonIterDataER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl27makeCanonIterDataFromNorm16EiitRNS_13CanonIterDataER10UErrorCode:
.LFB3272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, -64(%rbp)
	cmpw	$1, %cx
	je	.L2305
	movq	%rdi, %rbx
	movl	%esi, %r15d
	movq	%r8, %r13
	movl	%ecx, %r12d
	cmpw	%cx, 14(%rdi)
	jbe	.L2307
.L2310:
	cmpl	%edx, %r15d
	jg	.L2305
	movzwl	%cx, %ecx
	leal	1(%rdx), %eax
	sarl	$3, %ecx
	movl	%eax, -52(%rbp)
	movl	%ecx, -56(%rbp)
	jmp	.L2328
	.p2align 4,,10
	.p2align 3
.L2350:
	movl	%r14d, %r9d
	orl	$-1073741824, %eax
	orl	$-2147483648, %r9d
	cmpw	$-1025, %r12w
	cmovbe	%eax, %r9d
.L2313:
	cmpl	%r14d, %r9d
	je	.L2326
	movq	-64(%rbp), %rcx
	movq	0(%r13), %rdi
	movl	%r9d, %edx
	movl	%r15d, %esi
	call	umutablecptrie_set_67@PLT
.L2326:
	addl	$1, %r15d
	cmpl	-52(%rbp), %r15d
	je	.L2305
.L2328:
	movq	0(%r13), %rdi
	movl	%r15d, %esi
	call	umutablecptrie_get_67@PLT
	movl	%eax, %r14d
	cmpw	30(%rbx), %r12w
	jnb	.L2350
	movzwl	14(%rbx), %eax
	cmpw	%r12w, %ax
	jbe	.L2314
	movl	%r14d, %r9d
	orl	$1073741824, %r9d
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2314:
	movl	%r12d, %r10d
	movl	%r15d, %edx
	cmpw	26(%rbx), %r12w
	jnb	.L2351
	cmpw	%ax, %r10w
	jbe	.L2320
.L2353:
	movq	%r10, %rax
	andl	$65534, %eax
	addq	48(%rbx), %rax
	movzwl	(%rax), %ecx
	movl	%ecx, %r11d
	andl	$31, %r11d
	testb	%cl, %cl
	jns	.L2321
	cmpl	%r15d, %edx
	je	.L2352
.L2321:
	testw	%r11w, %r11w
	je	.L2326
	movl	%r14d, %r9d
.L2329:
	movzwl	2(%rax), %edx
	movl	$1, %r8d
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L2322
	movzwl	4(%rax), %ecx
	sall	$10, %edx
	movl	$2, %r8d
	leal	-56613888(%rdx,%rcx), %edx
.L2322:
	movq	-64(%rbp), %rcx
	movl	%r15d, %esi
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movl	%r11d, -72(%rbp)
	movl	%r8d, -88(%rbp)
	movl	%r10d, -80(%rbp)
	movl	%r9d, -68(%rbp)
	call	_ZN6icu_6713CanonIterData13addToStartSetEiiR10UErrorCode
	movl	-80(%rbp), %r10d
	cmpw	%r10w, 18(%rbx)
	movl	-68(%rbp), %r9d
	movl	-88(%rbp), %r8d
	movl	-72(%rbp), %r11d
	movq	-96(%rbp), %rax
	ja	.L2313
	movzwl	%r11w, %edi
	cmpl	%edi, %r8d
	jge	.L2313
	addq	$2, %rax
	movl	%r14d, -80(%rbp)
	movl	%edi, %r14d
	movq	%rbx, -88(%rbp)
	movq	%rax, %rbx
	movw	%r12w, -96(%rbp)
	movl	%r8d, %r12d
	movl	%r15d, -72(%rbp)
	jmp	.L2323
	.p2align 4,,10
	.p2align 3
.L2333:
	movl	%edx, %r12d
.L2324:
	movq	0(%r13), %rdi
	movl	%r15d, %esi
	call	umutablecptrie_get_67@PLT
	testl	%eax, %eax
	js	.L2325
	orl	$-2147483648, %eax
	movq	-64(%rbp), %rcx
	movq	0(%r13), %rdi
	movl	%r15d, %esi
	movl	%eax, %edx
	call	umutablecptrie_set_67@PLT
.L2325:
	cmpl	%r14d, %r12d
	jge	.L2348
.L2323:
	movslq	%r12d, %rax
	leal	1(%r12), %edx
	movzwl	(%rbx,%rax,2), %r15d
	leaq	(%rax,%rax), %rcx
	movl	%r15d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L2333
	movl	%r15d, %esi
	movzwl	2(%rbx,%rcx), %eax
	addl	$2, %r12d
	sall	$10, %esi
	leal	-56613888(%rax,%rsi), %r15d
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2307:
	cmpw	%cx, 18(%rdi)
	jbe	.L2310
.L2305:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2351:
	.cfi_restore_state
	movl	-56(%rbp), %edi
	movzwl	28(%rbx), %ecx
	leal	(%r15,%rdi), %edx
	movq	32(%rbx), %rdi
	subl	%ecx, %edx
	movq	8(%rdi), %r9
	cmpl	$65535, %edx
	ja	.L2316
	movl	%edx, %ecx
	movq	(%rdi), %rsi
	sarl	$6, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	movl	%edx, %esi
	andl	$63, %esi
	addl	%esi, %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rcx
.L2317:
	movzwl	(%r9,%rcx), %r10d
	cmpw	%ax, %r10w
	ja	.L2353
.L2320:
	movq	-64(%rbp), %rcx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713CanonIterData13addToStartSetEiiR10UErrorCode
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2316:
	cmpl	$1114111, %edx
	ja	.L2318
	cmpl	24(%rdi), %edx
	jl	.L2319
	movl	20(%rdi), %ecx
	subl	$2, %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rcx
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L2352:
	cmpb	$0, -2(%rax)
	je	.L2321
	movl	%r14d, %r9d
	orl	$-2147483648, %r9d
	testw	%r11w, %r11w
	je	.L2313
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2318:
	movl	20(%rdi), %ecx
	subl	$1, %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %rcx
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L2348:
	movl	-68(%rbp), %r9d
	movl	-80(%rbp), %r14d
	movq	-88(%rbp), %rbx
	movl	-72(%rbp), %r15d
	movzwl	-96(%rbp), %r12d
	jmp	.L2313
.L2319:
	movl	%edx, %esi
	movq	%r9, -80(%rbp)
	movl	%edx, -68(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movl	-68(%rbp), %edx
	movq	-80(%rbp), %r9
	movslq	%eax, %rcx
	movzwl	14(%rbx), %eax
	addq	%rcx, %rcx
	jmp	.L2317
	.cfi_endproc
.LFE3272:
	.size	_ZNK6icu_6715Normalizer2Impl27makeCanonIterDataFromNorm16EiitRNS_13CanonIterDataER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl27makeCanonIterDataFromNorm16EiitRNS_13CanonIterDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl13getCanonValueEi
	.type	_ZNK6icu_6715Normalizer2Impl13getCanonValueEi, @function
_ZNK6icu_6715Normalizer2Impl13getCanonValueEi:
.LFB3274:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rax
	movq	8(%rax), %rdi
	jmp	ucptrie_get_67@PLT
	.cfi_endproc
.LFE3274:
	.size	_ZNK6icu_6715Normalizer2Impl13getCanonValueEi, .-_ZNK6icu_6715Normalizer2Impl13getCanonValueEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEi
	.type	_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEi, @function
_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEi:
.LFB3275:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	addq	$16, %rdi
	jmp	_ZNK6icu_677UVector9elementAtEi@PLT
	.cfi_endproc
.LFE3275:
	.size	_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEi, .-_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl21isCanonSegmentStarterEi
	.type	_ZNK6icu_6715Normalizer2Impl21isCanonSegmentStarterEi, @function
_ZNK6icu_6715Normalizer2Impl21isCanonSegmentStarterEi:
.LFB3276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	72(%rdi), %rax
	movq	8(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ucptrie_get_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	notl	%eax
	shrl	$31, %eax
	ret
	.cfi_endproc
.LFE3276:
	.size	_ZNK6icu_6715Normalizer2Impl21isCanonSegmentStarterEi, .-_ZNK6icu_6715Normalizer2Impl21isCanonSegmentStarterEi
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"unorm2_swap(): data format %02x.%02x.%02x.%02x (format version %02x) is not recognized as Normalizer2 data\n"
	.align 8
.LC4:
	.string	"unorm2_swap(): too few bytes (%d after header) for Normalizer2 data\n"
	.align 8
.LC5:
	.string	"unorm2_swap(): too few bytes (%d after header) for all of Normalizer2 data\n"
	.text
	.p2align 4
	.globl	unorm2_swap_67
	.type	unorm2_swap_67, @function
unorm2_swap_67:
.LFB3278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	udata_swapDataHeader_67@PLT
	movl	%eax, -108(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L2358
	movl	0(%r13), %ecx
	testl	%ecx, %ecx
	jg	.L2358
	cmpw	$29262, 12(%r14)
	movzbl	16(%r14), %eax
	jne	.L2360
	cmpw	$12909, 14(%r14)
	jne	.L2360
	leal	-1(%rax), %edx
	cmpb	$3, %dl
	ja	.L2360
	movslq	-108(%rbp), %rcx
	movl	$56, %edx
	movq	%rcx, -128(%rbp)
	addq	%rcx, %r14
	cmpb	$1, %al
	je	.L2362
	cmpb	$2, %al
	movl	$60, %edx
	movl	$76, %eax
	cmovne	%eax, %edx
.L2362:
	testl	%r12d, %r12d
	js	.L2363
	subl	-108(%rbp), %r12d
	cmpl	%edx, %r12d
	jl	.L2377
.L2363:
	leaq	-96(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L2364:
	movl	(%r14,%rdx), %esi
	movq	%r15, %rdi
	movq	%rdx, -104(%rbp)
	call	udata_readInt32_67@PLT
	movq	-104(%rbp), %rdx
	movq	-120(%rbp), %rdi
	movl	%eax, (%rdi,%rdx)
	addq	$4, %rdx
	cmpq	$32, %rdx
	jne	.L2364
	movl	-68(%rbp), %r9d
	testl	%r12d, %r12d
	js	.L2365
	cmpl	%r9d, %r12d
	jl	.L2378
	addq	-128(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L2367
	movslq	%r9d, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%r9d, -104(%rbp)
	call	memcpy@PLT
	movl	-104(%rbp), %r9d
.L2367:
	movl	-96(%rbp), %edx
	movl	%r9d, -120(%rbp)
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%edx, -104(%rbp)
	call	*56(%r15)
	movl	-92(%rbp), %r12d
	movslq	-104(%rbp), %rsi
	movq	%r13, %r8
	movq	%r15, %rdi
	movl	%r12d, %eax
	leaq	(%rbx,%rsi), %rcx
	subl	%esi, %eax
	addq	%r14, %rsi
	movl	%eax, %edx
	call	utrie_swapAnyVersion_67@PLT
	movl	-88(%rbp), %edx
	movslq	%r12d, %rsi
	movq	%r13, %r8
	leaq	(%rbx,%rsi), %rcx
	movq	%r15, %rdi
	addq	%r14, %rsi
	subl	%r12d, %edx
	call	*48(%r15)
	movl	-120(%rbp), %r9d
.L2365:
	movl	-108(%rbp), %eax
	addl	%r9d, %eax
	.p2align 4,,10
	.p2align 3
.L2358:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2379
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2360:
	.cfi_restore_state
	subq	$8, %rsp
	movzbl	12(%r14), %edx
	movzbl	13(%r14), %ecx
	movq	%r15, %rdi
	pushq	%rax
	movzbl	15(%r14), %r9d
	xorl	%eax, %eax
	leaq	.LC3(%rip), %rsi
	movzbl	14(%r14), %r8d
	call	udata_printError_67@PLT
	popq	%rax
	movl	$16, 0(%r13)
	xorl	%eax, %eax
	popq	%rdx
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2377:
	movl	%r12d, %edx
	leaq	.LC4(%rip), %rsi
.L2376:
	xorl	%eax, %eax
	movq	%r15, %rdi
	call	udata_printError_67@PLT
	movl	$8, 0(%r13)
	xorl	%eax, %eax
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2378:
	movl	%r12d, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L2376
.L2379:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3278:
	.size	unorm2_swap_67, .-unorm2_swap_67
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl12addLcccCharsERNS_10UnicodeSetE
	.type	_ZNK6icu_6715Normalizer2Impl12addLcccCharsERNS_10UnicodeSetE, @function
_ZNK6icu_6715Normalizer2Impl12addLcccCharsERNS_10UnicodeSetE:
.LFB3225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-60(%rbp), %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2403:
	cmpl	$65024, %eax
	je	.L2382
.L2402:
	movl	%ebx, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
.L2383:
	leal	1(%rbx), %r14d
.L2387:
	subq	$8, %rsp
	movq	32(%r15), %rdi
	movl	$1, %edx
	xorl	%r9d, %r9d
	pushq	%r12
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%r14d, %esi
	call	ucptrie_getRange_67@PLT
	movl	%eax, %ebx
	popq	%rax
	popq	%rdx
	testl	%ebx, %ebx
	js	.L2380
	movl	-60(%rbp), %eax
	cmpl	$64512, %eax
	ja	.L2403
.L2382:
	movzwl	22(%r15), %edx
	cmpl	%eax, %edx
	ja	.L2383
	movzwl	26(%r15), %edx
	cmpl	%eax, %edx
	jbe	.L2383
	movzwl	8(%r15), %eax
	cmpl	%eax, %r14d
	jl	.L2383
	cmpl	$65535, %r14d
	jg	.L2386
	movl	%r14d, %eax
	movq	56(%r15), %rdx
	sarl	$8, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	testb	%al, %al
	je	.L2383
	movl	%r14d, %edx
	shrb	$5, %dl
	btl	%edx, %eax
	jnc	.L2383
.L2386:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6715Normalizer2Impl20getFCD16FromNormDataEi
	cmpw	$255, %ax
	ja	.L2402
	jmp	.L2383
	.p2align 4,,10
	.p2align 3
.L2380:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2404
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2404:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3225:
	.size	_ZNK6icu_6715Normalizer2Impl12addLcccCharsERNS_10UnicodeSetE, .-_ZNK6icu_6715Normalizer2Impl12addLcccCharsERNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713CanonIterDataD2Ev
	.type	_ZN6icu_6713CanonIterDataD2Ev, @function
_ZN6icu_6713CanonIterDataD2Ev:
.LFB3267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	umutablecptrie_close_67@PLT
	movq	8(%rbx), %rdi
	call	ucptrie_close_67@PLT
	addq	$8, %rsp
	leaq	16(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UVectorD1Ev@PLT
	.cfi_endproc
.LFE3267:
	.size	_ZN6icu_6713CanonIterDataD2Ev, .-_ZN6icu_6713CanonIterDataD2Ev
	.globl	_ZN6icu_6713CanonIterDataD1Ev
	.set	_ZN6icu_6713CanonIterDataD1Ev,_ZN6icu_6713CanonIterDataD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715Normalizer2ImplD0Ev
	.type	_ZN6icu_6715Normalizer2ImplD0Ev, @function
_ZN6icu_6715Normalizer2ImplD0Ev:
.LFB3222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715Normalizer2ImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	72(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2408
	movq	0(%r13), %rdi
	call	umutablecptrie_close_67@PLT
	movq	8(%r13), %rdi
	call	ucptrie_close_67@PLT
	leaq	16(%r13), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L2408:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3222:
	.size	_ZN6icu_6715Normalizer2ImplD0Ev, .-_ZN6icu_6715Normalizer2ImplD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715Normalizer2ImplD2Ev
	.type	_ZN6icu_6715Normalizer2ImplD2Ev, @function
_ZN6icu_6715Normalizer2ImplD2Ev:
.LFB3220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715Normalizer2ImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	72(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2414
	movq	0(%r13), %rdi
	call	umutablecptrie_close_67@PLT
	movq	8(%r13), %rdi
	call	ucptrie_close_67@PLT
	leaq	16(%r13), %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L2414:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3220:
	.size	_ZN6icu_6715Normalizer2ImplD2Ev, .-_ZN6icu_6715Normalizer2ImplD2Ev
	.globl	_ZN6icu_6715Normalizer2ImplD1Ev
	.set	_ZN6icu_6715Normalizer2ImplD1Ev,_ZN6icu_6715Normalizer2ImplD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717InitCanonIterData6doInitEPNS_15Normalizer2ImplER10UErrorCode
	.type	_ZN6icu_6717InitCanonIterData6doInitEPNS_15Normalizer2ImplER10UErrorCode, @function
_ZN6icu_6717InitCanonIterData6doInitEPNS_15Normalizer2ImplER10UErrorCode:
.LFB3271:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$56, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L2420
	movq	%r13, %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, %r14
	call	umutablecptrie_open_67@PLT
	leaq	16(%r14), %r15
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%rax, (%r14)
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	movq	$0, 8(%r14)
	call	_ZN6icu_677UVectorC1EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	movl	0(%r13), %esi
	movq	%r14, 72(%r12)
	testl	%esi, %esi
	jle	.L2432
.L2421:
	movq	(%r14), %rdi
	call	umutablecptrie_close_67@PLT
	movq	8(%r14), %rdi
	call	ucptrie_close_67@PLT
	movq	%r15, %rdi
	call	_ZN6icu_677UVectorD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L2426:
	movq	$0, 72(%r12)
.L2419:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2433
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2432:
	.cfi_restore_state
	xorl	%r15d, %r15d
	leaq	-60(%rbp), %r14
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2434:
	movl	-60(%rbp), %ecx
	cmpl	$1, %ecx
	je	.L2423
	movq	72(%r12), %r8
	movzwl	%cx, %ecx
	movq	%r13, %r9
	movl	%eax, %edx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6715Normalizer2Impl27makeCanonIterDataFromNorm16EiitRNS_13CanonIterDataER10UErrorCode
.L2423:
	leal	1(%rbx), %r15d
.L2424:
	subq	$8, %rsp
	movq	32(%r12), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%r14
	movl	$1, %ecx
	movl	$1, %edx
	movl	%r15d, %esi
	call	ucptrie_getRange_67@PLT
	popq	%rdx
	popq	%rcx
	movl	%eax, %ebx
	testl	%eax, %eax
	jns	.L2434
	movq	72(%r12), %rbx
	movq	%r13, %rcx
	movl	$1, %edx
	movl	$1, %esi
	movq	(%rbx), %rdi
	call	umutablecptrie_buildImmutable_67@PLT
	movq	%rax, 8(%rbx)
	movq	72(%r12), %rax
	movq	(%rax), %rdi
	call	umutablecptrie_close_67@PLT
	movq	72(%r12), %r14
	movl	0(%r13), %eax
	movq	$0, (%r14)
	testl	%eax, %eax
	jle	.L2419
	leaq	16(%r14), %r15
	jmp	.L2421
.L2420:
	movl	$7, 0(%r13)
	jmp	.L2426
.L2433:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3271:
	.size	_ZN6icu_6717InitCanonIterData6doInitEPNS_15Normalizer2ImplER10UErrorCode, .-_ZN6icu_6717InitCanonIterData6doInitEPNS_15Normalizer2ImplER10UErrorCode
	.p2align 4
	.type	initCanonIterData, @function
initCanonIterData:
.LFB3270:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_6717InitCanonIterData6doInitEPNS_15Normalizer2ImplER10UErrorCode
	.cfi_endproc
.LFE3270:
	.size	initCanonIterData, .-initCanonIterData
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl26addCanonIterPropertyStartsEPK9USetAdderR10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl26addCanonIterPropertyStartsEPK9USetAdderR10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl26addCanonIterPropertyStartsEPK9USetAdderR10UErrorCode:
.LFB3227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.L2450
.L2436:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2451
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2450:
	.cfi_restore_state
	movl	64(%rdi), %eax
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %rbx
	leaq	64(%rdi), %r14
	cmpl	$2, %eax
	jne	.L2452
.L2438:
	movl	68(%r13), %eax
	testl	%eax, %eax
	jle	.L2439
	movl	%eax, (%rbx)
	jmp	.L2436
	.p2align 4,,10
	.p2align 3
.L2452:
	movq	%r14, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L2438
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN6icu_6717InitCanonIterData6doInitEPNS_15Normalizer2ImplER10UErrorCode
	movl	(%rbx), %eax
	movq	%r14, %rdi
	movl	%eax, 68(%r13)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L2439:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L2436
	leaq	-60(%rbp), %rax
	xorl	%r15d, %r15d
	leaq	segmentStarterMapper(%rip), %r14
	movq	%rax, -72(%rbp)
	jmp	.L2440
	.p2align 4,,10
	.p2align 3
.L2453:
	movl	%r15d, %esi
	movq	(%r12), %rdi
	leal	1(%rbx), %r15d
	call	*8(%r12)
.L2440:
	movq	72(%r13), %rax
	subq	$8, %rsp
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movl	%r15d, %esi
	movq	8(%rax), %rdi
	pushq	-72(%rbp)
	call	ucptrie_getRange_67@PLT
	movl	%eax, %ebx
	popq	%rax
	popq	%rdx
	testl	%ebx, %ebx
	jns	.L2453
	jmp	.L2436
.L2451:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3227:
	.size	_ZNK6icu_6715Normalizer2Impl26addCanonIterPropertyStartsEPK9USetAdderR10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl26addCanonIterPropertyStartsEPK9USetAdderR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl19ensureCanonIterDataER10UErrorCode
	.type	_ZNK6icu_6715Normalizer2Impl19ensureCanonIterDataER10UErrorCode, @function
_ZNK6icu_6715Normalizer2Impl19ensureCanonIterDataER10UErrorCode:
.LFB3273:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L2468
	testl	%eax, %eax
	setle	%al
	ret
	.p2align 4,,10
	.p2align 3
.L2468:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	64(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	64(%rdi), %eax
	cmpl	$2, %eax
	jne	.L2469
.L2456:
	movl	68(%r12), %eax
	testl	%eax, %eax
	jle	.L2470
	movl	%eax, (%rbx)
.L2455:
	testl	%eax, %eax
	setle	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2470:
	.cfi_restore_state
	movl	(%rbx), %eax
	testl	%eax, %eax
	setle	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2469:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L2456
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6717InitCanonIterData6doInitEPNS_15Normalizer2ImplER10UErrorCode
	movl	(%rbx), %eax
	movq	%r13, %rdi
	movl	%eax, 68(%r12)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movl	(%rbx), %eax
	jmp	.L2455
	.cfi_endproc
.LFE3273:
	.size	_ZNK6icu_6715Normalizer2Impl19ensureCanonIterDataER10UErrorCode, .-_ZNK6icu_6715Normalizer2Impl19ensureCanonIterDataER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEiRNS_10UnicodeSetE
	.type	_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEiRNS_10UnicodeSetE, @function
_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEiRNS_10UnicodeSetE:
.LFB3277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	72(%rdi), %rax
	movq	8(%rax), %rdi
	call	ucptrie_get_67@PLT
	movl	%eax, %ebx
	xorl	%eax, %eax
	testl	$2147483647, %ebx
	jne	.L2494
.L2471:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2494:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet5clearEv@PLT
	movl	%ebx, %esi
	andl	$2097151, %esi
	testl	$2097152, %ebx
	jne	.L2495
	testl	%esi, %esi
	jne	.L2496
.L2474:
	andl	$1073741824, %ebx
	movl	$1, %eax
	je	.L2471
	movq	32(%r13), %rdi
	movq	8(%rdi), %rbx
	cmpl	$65535, %r12d
	ja	.L2475
	movl	%r12d, %eax
	movq	(%rdi), %rdx
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%r12d, %edx
	andl	$63, %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
.L2476:
	movzwl	(%rbx,%rax), %eax
	cmpw	$2, %ax
	je	.L2497
	movzwl	%ax, %edx
	cmpw	14(%r13), %ax
	jb	.L2480
	movzwl	30(%r13), %ecx
	cmpw	%cx, %ax
	jb	.L2498
	leal	-2(%rax), %edx
	cmpw	$-1027, %dx
	ja	.L2487
.L2483:
	movq	40(%r13), %rdx
	subq	%rcx, %rax
	leaq	(%rdx,%rax,2), %rsi
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2495:
	movq	72(%r13), %rax
	leaq	16(%rax), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6710UnicodeSet6addAllERKS0_@PLT
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2496:
	movq	%r14, %rdi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2475:
	cmpl	$1114111, %r12d
	ja	.L2477
	cmpl	24(%rdi), %r12d
	jl	.L2478
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L2476
	.p2align 4,,10
	.p2align 3
.L2497:
	leal	-4352(%r12), %esi
	movq	%r14, %rdi
	imull	$588, %esi, %esi
	leal	44619(%rsi), %edx
	addl	$44032, %esi
	call	_ZN6icu_6710UnicodeSet3addEii@PLT
	movl	$1, %eax
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L2480:
	leal	-2(%rax), %ecx
	cmpw	$-1027, %cx
	ja	.L2487
	movzwl	30(%r13), %ecx
	cmpw	%cx, %ax
	jnb	.L2483
	movq	48(%r13), %rax
	sarl	%edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rsi
.L2482:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZNK6icu_6715Normalizer2Impl13addCompositesEPKtRNS_10UnicodeSetE
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2477:
	.cfi_restore_state
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L2476
	.p2align 4,,10
	.p2align 3
.L2498:
	movq	48(%r13), %rax
	sarl	%edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rdx
	movzwl	(%rdx), %eax
	andl	$31, %eax
	leaq	2(%rdx,%rax,2), %rsi
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2487:
	xorl	%esi, %esi
	jmp	.L2482
	.p2align 4,,10
	.p2align 3
.L2478:
	movl	%r12d, %esi
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L2476
	.cfi_endproc
.LFE3277:
	.size	_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEiRNS_10UnicodeSetE, .-_ZNK6icu_6715Normalizer2Impl16getCanonStartSetEiRNS_10UnicodeSetE
	.weak	_ZTSN6icu_6715Normalizer2ImplE
	.section	.rodata._ZTSN6icu_6715Normalizer2ImplE,"aG",@progbits,_ZTSN6icu_6715Normalizer2ImplE,comdat
	.align 16
	.type	_ZTSN6icu_6715Normalizer2ImplE, @object
	.size	_ZTSN6icu_6715Normalizer2ImplE, 27
_ZTSN6icu_6715Normalizer2ImplE:
	.string	"N6icu_6715Normalizer2ImplE"
	.weak	_ZTIN6icu_6715Normalizer2ImplE
	.section	.data.rel.ro._ZTIN6icu_6715Normalizer2ImplE,"awG",@progbits,_ZTIN6icu_6715Normalizer2ImplE,comdat
	.align 8
	.type	_ZTIN6icu_6715Normalizer2ImplE, @object
	.size	_ZTIN6icu_6715Normalizer2ImplE, 24
_ZTIN6icu_6715Normalizer2ImplE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6715Normalizer2ImplE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6715Normalizer2ImplE
	.section	.data.rel.ro._ZTVN6icu_6715Normalizer2ImplE,"awG",@progbits,_ZTVN6icu_6715Normalizer2ImplE,comdat
	.align 8
	.type	_ZTVN6icu_6715Normalizer2ImplE, @object
	.size	_ZTVN6icu_6715Normalizer2ImplE, 40
_ZTVN6icu_6715Normalizer2ImplE:
	.quad	0
	.quad	_ZTIN6icu_6715Normalizer2ImplE
	.quad	_ZN6icu_6715Normalizer2ImplD1Ev
	.quad	_ZN6icu_6715Normalizer2ImplD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
