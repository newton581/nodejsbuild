	.file	"bytestrieiterator.cpp"
	.text
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB2618:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2618:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB2621:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3
	cmpb	$0, 12(%rbx)
	jne	.L16
.L7:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L7
	.cfi_endproc
.LFE2621:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB2624:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L19
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2624:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB2627:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L22
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE2627:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB2629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L28
.L24:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L29
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2629:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB2630:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2630:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB2631:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2631:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB2632:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE2632:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2633:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2633:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB2634:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE2634:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB2635:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L45
	testl	%edx, %edx
	jle	.L45
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L48
.L37:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L37
	.cfi_endproc
.LFE2635:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB2636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L52
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L52
	testl	%r12d, %r12d
	jg	.L59
	cmpb	$0, 12(%rbx)
	jne	.L60
.L54:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L54
.L60:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2636:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB2637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L62
	movq	(%rdi), %r8
.L63:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L66
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L66
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2637:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB2638:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L73
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE2638:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB2639:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE2639:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB2640:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2640:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB2641:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2641:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB2643:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2643:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB2645:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2645:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie8IteratorC2EPKviR10UErrorCode
	.type	_ZN6icu_679BytesTrie8IteratorC2EPKviR10UErrorCode, @function
_ZN6icu_679BytesTrie8IteratorC2EPKviR10UErrorCode:
.LFB2368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	punpcklqdq	%xmm1, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rsi, 16(%rdi)
	movl	(%rcx), %esi
	movq	$-1, 24(%rdi)
	movq	$0, 32(%rdi)
	movl	%edx, 40(%rdi)
	movl	$0, 44(%rdi)
	movq	$0, 48(%rdi)
	movups	%xmm1, (%rdi)
	testl	%esi, %esi
	jle	.L90
.L79:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movl	$64, %edi
	movq	%rcx, %r12
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L82
	leaq	13(%rax), %rdx
	movl	$0, 56(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movl	$40, 8(%rax)
	movw	%dx, 12(%rax)
.L82:
	movq	%rax, 32(%rbx)
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L83
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movl	(%r12), %eax
	movq	%r13, 48(%rbx)
	testl	%eax, %eax
	jg	.L79
	cmpq	$0, 32(%rbx)
	jne	.L79
.L85:
	movl	$7, (%r12)
	jmp	.L79
.L83:
	cmpl	$0, (%r12)
	movq	$0, 48(%rbx)
	jg	.L79
	jmp	.L85
	.cfi_endproc
.LFE2368:
	.size	_ZN6icu_679BytesTrie8IteratorC2EPKviR10UErrorCode, .-_ZN6icu_679BytesTrie8IteratorC2EPKviR10UErrorCode
	.globl	_ZN6icu_679BytesTrie8IteratorC1EPKviR10UErrorCode
	.set	_ZN6icu_679BytesTrie8IteratorC1EPKviR10UErrorCode,_ZN6icu_679BytesTrie8IteratorC2EPKviR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie8IteratorC2ERKS0_iR10UErrorCode
	.type	_ZN6icu_679BytesTrie8IteratorC2ERKS0_iR10UErrorCode, @function
_ZN6icu_679BytesTrie8IteratorC2ERKS0_iR10UErrorCode:
.LFB2371:
	.cfi_startproc
	endbr64
	movq	16(%rsi), %rax
	movdqu	8(%rsi), %xmm0
	movl	%edx, 40(%rdi)
	movq	$0, 32(%rdi)
	movq	%rax, 16(%rdi)
	movl	24(%rsi), %eax
	movl	(%rcx), %esi
	movl	$0, 44(%rdi)
	movl	%eax, 24(%rdi)
	movl	%eax, 28(%rdi)
	movq	$0, 48(%rdi)
	movups	%xmm0, (%rdi)
	testl	%esi, %esi
	jle	.L107
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$64, %edi
	subq	$8, %rsp
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L94
	leaq	13(%rax), %rdx
	movl	$0, 56(%rax)
	movq	%rdx, (%rax)
	xorl	%edx, %edx
	movl	$40, 8(%rax)
	movw	%dx, 12(%rax)
.L94:
	movq	%rax, 32(%rbx)
	movl	$32, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L95
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN6icu_679UVector32C1ER10UErrorCode@PLT
	movl	(%r12), %eax
	movq	%r13, 48(%rbx)
	testl	%eax, %eax
	jg	.L91
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L99
	movl	24(%rbx), %r13d
	testl	%r13d, %r13d
	js	.L91
	movl	40(%rbx), %eax
	addl	$1, %r13d
	testl	%eax, %eax
	jle	.L98
	cmpl	%eax, %r13d
	cmovg	%eax, %r13d
.L98:
	movq	8(%rbx), %rsi
	movq	%r12, %rcx
	movl	%r13d, %edx
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movslq	%r13d, %rax
	subl	%r13d, 24(%rbx)
	addq	%rax, 8(%rbx)
.L91:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L95:
	.cfi_restore_state
	cmpl	$0, (%r12)
	movq	$0, 48(%rbx)
	jg	.L91
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$7, (%r12)
	jmp	.L91
	.cfi_endproc
.LFE2371:
	.size	_ZN6icu_679BytesTrie8IteratorC2ERKS0_iR10UErrorCode, .-_ZN6icu_679BytesTrie8IteratorC2ERKS0_iR10UErrorCode
	.globl	_ZN6icu_679BytesTrie8IteratorC1ERKS0_iR10UErrorCode
	.set	_ZN6icu_679BytesTrie8IteratorC1ERKS0_iR10UErrorCode,_ZN6icu_679BytesTrie8IteratorC2ERKS0_iR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie8IteratorD2Ev
	.type	_ZN6icu_679BytesTrie8IteratorD2Ev, @function
_ZN6icu_679BytesTrie8IteratorD2Ev:
.LFB2374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L109
	cmpb	$0, 12(%r12)
	jne	.L116
.L110:
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
.L109:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L108
	movq	(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	uprv_free_67@PLT
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L108:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2374:
	.size	_ZN6icu_679BytesTrie8IteratorD2Ev, .-_ZN6icu_679BytesTrie8IteratorD2Ev
	.globl	_ZN6icu_679BytesTrie8IteratorD1Ev
	.set	_ZN6icu_679BytesTrie8IteratorD1Ev,_ZN6icu_679BytesTrie8IteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie8Iterator5resetEv
	.type	_ZN6icu_679BytesTrie8Iterator5resetEv, @function
_ZN6icu_679BytesTrie8Iterator5resetEv:
.LFB2376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rax
	movl	28(%rdi), %ebx
	movq	%rax, 8(%rdi)
	movl	40(%rdi), %eax
	movl	%ebx, 24(%rdi)
	addl	$1, %ebx
	testl	%eax, %eax
	jle	.L118
	cmpl	%eax, %ebx
	cmovg	%eax, %ebx
.L118:
	movq	32(%r12), %rdi
	movl	%ebx, %esi
	call	_ZN6icu_6710CharString8truncateEi@PLT
	movslq	%ebx, %rax
	subl	%ebx, 24(%r12)
	xorl	%esi, %esi
	addq	%rax, 8(%r12)
	movq	48(%r12), %rdi
	call	_ZN6icu_679UVector327setSizeEi@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2376:
	.size	_ZN6icu_679BytesTrie8Iterator5resetEv, .-_ZN6icu_679BytesTrie8Iterator5resetEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679BytesTrie8Iterator7hasNextEv
	.type	_ZNK6icu_679BytesTrie8Iterator7hasNextEv, @function
_ZNK6icu_679BytesTrie8Iterator7hasNextEv:
.LFB2377:
	.cfi_startproc
	endbr64
	cmpq	$0, 8(%rdi)
	movl	$1, %eax
	je	.L123
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	movq	48(%rdi), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE2377:
	.size	_ZNK6icu_679BytesTrie8Iterator7hasNextEv, .-_ZNK6icu_679BytesTrie8Iterator7hasNextEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_679BytesTrie8Iterator9getStringEv
	.type	_ZNK6icu_679BytesTrie8Iterator9getStringEv, @function
_ZNK6icu_679BytesTrie8Iterator9getStringEv:
.LFB2379:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L126
	movq	(%rax), %r8
	movl	56(%rax), %edx
.L126:
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE2379:
	.size	_ZNK6icu_679BytesTrie8Iterator9getStringEv, .-_ZNK6icu_679BytesTrie8Iterator9getStringEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie8Iterator15truncateAndStopEv
	.type	_ZN6icu_679BytesTrie8Iterator15truncateAndStopEv, @function
_ZN6icu_679BytesTrie8Iterator15truncateAndStopEv:
.LFB2380:
	.cfi_startproc
	endbr64
	movq	$0, 8(%rdi)
	movl	$1, %eax
	movl	$-1, 44(%rdi)
	ret
	.cfi_endproc
.LFE2380:
	.size	_ZN6icu_679BytesTrie8Iterator15truncateAndStopEv, .-_ZN6icu_679BytesTrie8Iterator15truncateAndStopEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie8Iterator10branchNextEPKhiR10UErrorCode
	.type	_ZN6icu_679BytesTrie8Iterator10branchNextEPKhiR10UErrorCode, @function
_ZN6icu_679BytesTrie8Iterator10branchNextEPKhiR10UErrorCode:
.LFB2381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	cmpl	$5, %edx
	jg	.L140
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L164:
	movq	48(%rbx), %rcx
.L136:
	movq	32(%rbx), %rax
	movl	%r14d, %r12d
	sarl	%r12d
	movl	56(%rax), %r8d
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L137
	cmpl	12(%rcx), %esi
	jle	.L138
.L137:
	movq	%r13, %rdx
	movq	%rcx, %rdi
	movl	%r8d, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L139
	movq	-56(%rbp), %rcx
	movl	-64(%rbp), %r8d
	movslq	8(%rcx), %rax
.L138:
	subl	%r12d, %r14d
	movq	24(%rcx), %rdx
	sall	$16, %r14d
	orl	%r8d, %r14d
	movl	%r14d, (%rdx,%rax,4)
	addl	$1, 8(%rcx)
.L139:
	movq	%r15, %rdi
	call	_ZN6icu_679BytesTrie11jumpByDeltaEPKh@PLT
	movq	%rax, %r8
	cmpl	$5, %r12d
	jle	.L129
	movl	%r12d, %r14d
.L140:
	movzbl	1(%r8), %edx
	movq	48(%rbx), %r12
	leaq	1(%r8), %r15
	leaq	2(%r8), %r9
	movl	%edx, %eax
	cmpl	$191, %edx
	jle	.L130
	cmpl	$239, %edx
	jg	.L131
	leaq	3(%r8), %r9
.L130:
	movslq	8(%r12), %rax
	movq	(%rbx), %r8
	movl	%eax, %esi
	addl	$1, %esi
	js	.L133
	cmpl	12(%r12), %esi
	jle	.L155
.L133:
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L164
	movslq	8(%r12), %rax
	movq	48(%rbx), %rcx
.L134:
	movq	24(%r12), %rdx
	subq	%r8, %r9
	movl	%r9d, (%rdx,%rax,4)
	addl	$1, 8(%r12)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r12, %rcx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L131:
	cmpl	$253, %edx
	jg	.L132
	leaq	4(%r8), %r9
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L132:
	andl	$1, %eax
	addl	$3, %eax
	cltq
	addq	%rax, %r9
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L163:
	movl	%edx, %r12d
	.p2align 4,,10
	.p2align 3
.L129:
	movzbl	1(%r8), %edx
	movzbl	(%r8), %eax
	leaq	2(%r8), %r14
	movq	%r8, -88(%rbp)
	movq	%r14, %rdi
	movl	%edx, %ecx
	movb	%al, -56(%rbp)
	movl	%edx, %eax
	sarl	%ecx
	andl	$1, %eax
	movl	%edx, -72(%rbp)
	movl	%ecx, %esi
	movb	%al, -64(%rbp)
	movl	%ecx, -80(%rbp)
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	movl	-72(%rbp), %edx
	movl	%eax, %r15d
	cmpl	$161, %edx
	jle	.L141
	cmpl	$215, %edx
	movl	-80(%rbp), %ecx
	movq	-88(%rbp), %r8
	jg	.L142
	leaq	3(%r8), %r14
.L141:
	movq	48(%rbx), %rcx
	movq	(%rbx), %r8
	movslq	8(%rcx), %rax
	movl	%eax, %esi
	addl	$1, %esi
	js	.L144
	cmpl	12(%rcx), %esi
	jle	.L157
.L144:
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	testb	%al, %al
	jne	.L146
	movq	48(%rbx), %rdi
.L147:
	movslq	8(%rdi), %rax
	movq	32(%rbx), %r8
	movl	%eax, %esi
	movl	56(%r8), %ecx
	addl	$1, %esi
	js	.L148
	cmpl	12(%rdi), %esi
	jle	.L149
.L148:
	movq	%r13, %rdx
	movl	%ecx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN6icu_679UVector3214expandCapacityEiR10UErrorCode@PLT
	movq	-72(%rbp), %rdi
	movl	-80(%rbp), %ecx
	testb	%al, %al
	jne	.L150
	movq	32(%rbx), %r8
.L151:
	movsbl	-56(%rbp), %esi
	movq	%r13, %rdx
	movq	%r8, %rdi
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movslq	%r15d, %rax
	addq	%r14, %rax
	cmpb	$0, -64(%rbp)
	jne	.L165
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movslq	8(%rcx), %rax
	movq	48(%rbx), %rdi
.L145:
	movq	24(%rcx), %rdx
	movq	%r14, %rsi
	subq	%r8, %rsi
	movl	%esi, (%rdx,%rax,4)
	addl	$1, 8(%rcx)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%rcx, %rdi
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L142:
	cmpl	$251, %edx
	jg	.L143
	leaq	4(%r8), %r14
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L150:
	movslq	8(%rdi), %rax
	movq	32(%rbx), %r8
.L149:
	subl	$1, %r12d
	movq	24(%rdi), %rdx
	sall	$16, %r12d
	orl	%ecx, %r12d
	movl	%r12d, (%rdx,%rax,4)
	addl	$1, 8(%rdi)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L165:
	movl	%r15d, 44(%rbx)
	xorl	%eax, %eax
	movq	$0, 8(%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	andl	$1, %ecx
	leal	3(%rcx), %eax
	cltq
	addq	%rax, %r14
	jmp	.L141
	.cfi_endproc
.LFE2381:
	.size	_ZN6icu_679BytesTrie8Iterator10branchNextEPKhiR10UErrorCode, .-_ZN6icu_679BytesTrie8Iterator10branchNextEPKhiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_679BytesTrie8Iterator4nextER10UErrorCode
	.type	_ZN6icu_679BytesTrie8Iterator4nextER10UErrorCode, @function
_ZN6icu_679BytesTrie8Iterator4nextER10UErrorCode:
.LFB2378:
	.cfi_startproc
	endbr64
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L202
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L208
.L168:
	movl	24(%r12), %eax
	testl	%eax, %eax
	js	.L184
.L207:
	movq	$0, 8(%r12)
	movl	$-1, 44(%r12)
.L206:
	movl	$1, %eax
.L166:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	cmpl	$15, %ebx
	jg	.L209
.L186:
	testl	%ebx, %ebx
	jne	.L182
	movzbl	1(%rcx), %ebx
	leaq	2(%rcx), %r13
.L182:
	movq	%r14, %rcx
	leal	1(%rbx), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_679BytesTrie8Iterator10branchNextEPKhiR10UErrorCode
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L206
.L184:
	movzbl	(%rcx), %ebx
	leaq	1(%rcx), %r13
	movl	%ebx, %r15d
	cmpl	$31, %ebx
	jg	.L210
.L174:
	movl	40(%r12), %esi
	testl	%esi, %esi
	jle	.L180
	movq	32(%r12), %rdi
	movl	56(%rdi), %edx
	cmpl	%edx, %esi
	je	.L207
	cmpl	$15, %ebx
	jle	.L186
	subl	$15, %ebx
	leal	(%rdx,%rbx), %eax
	cmpl	%eax, %esi
	jl	.L211
.L185:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movslq	%ebx, %rbx
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	leaq	0(%r13,%rbx), %rcx
	movzbl	(%rcx), %ebx
	leaq	1(%rcx), %r13
	movl	%ebx, %r15d
	cmpl	$31, %ebx
	jle	.L174
.L210:
	movl	%ebx, %r14d
	movq	%r13, %rdi
	movq	%rcx, -56(%rbp)
	sarl	%r14d
	movl	%r14d, %esi
	call	_ZN6icu_679BytesTrie9readValueEPKhi@PLT
	andl	$1, %r15d
	movl	%eax, 44(%r12)
	jne	.L175
	movl	40(%r12), %eax
	movq	-56(%rbp), %rcx
	testl	%eax, %eax
	jle	.L176
	movq	32(%r12), %rdx
	cmpl	56(%rdx), %eax
	je	.L175
.L176:
	cmpl	$161, %ebx
	jle	.L177
	cmpl	$215, %ebx
	jg	.L178
	leaq	2(%rcx), %r13
.L177:
	movq	%r13, 8(%r12)
	movl	$1, %eax
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L209:
	movq	32(%r12), %rdi
	subl	$15, %ebx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	48(%rdi), %rdi
	movl	8(%rdi), %edx
	testl	%edx, %edx
	je	.L166
	jg	.L212
	movl	$0, -56(%rbp)
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
.L169:
	movq	(%r12), %r15
	leal	-2(%rdx), %esi
	cmpl	$1, %edx
	jg	.L213
.L170:
	call	_ZN6icu_679UVector327setSizeEi@PLT
	movq	32(%r12), %rdi
	movl	%r13d, %esi
	call	_ZN6icu_6710CharString8truncateEi@PLT
	cmpl	$1, %ebx
	jbe	.L171
	movl	-56(%rbp), %edx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_679BytesTrie8Iterator10branchNextEPKhiR10UErrorCode
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L168
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L175:
	movq	$0, 8(%r12)
	movl	$1, %eax
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L171:
	movsbl	(%r15), %esi
	movq	32(%r12), %rdi
	leaq	1(%r15), %rcx
	movq	%r14, %rdx
	movq	%rcx, -56(%rbp)
	call	_ZN6icu_6710CharString6appendEcR10UErrorCode@PLT
	movq	-56(%rbp), %rcx
	jmp	.L168
.L211:
	subl	%edx, %esi
	movq	%r14, %rcx
	movl	%esi, %edx
	movq	%r13, %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L212:
	movq	24(%rdi), %rcx
	leal	-1(%rdx), %eax
	cltq
	movl	(%rcx,%rax,4), %ebx
	movzwl	%bx, %r13d
	shrl	$16, %ebx
	movl	%ebx, -56(%rbp)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L213:
	movq	24(%rdi), %rdx
	movslq	%esi, %rax
	movslq	(%rdx,%rax,4), %rax
	addq	%rax, %r15
	jmp	.L170
.L178:
	cmpl	$251, %ebx
	jg	.L179
	leaq	3(%rcx), %r13
	jmp	.L177
.L179:
	movl	%r14d, %eax
	andl	$1, %eax
	addl	$3, %eax
	cltq
	addq	%rax, %r13
	jmp	.L177
	.cfi_endproc
.LFE2378:
	.size	_ZN6icu_679BytesTrie8Iterator4nextER10UErrorCode, .-_ZN6icu_679BytesTrie8Iterator4nextER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
