	.file	"unistr_props.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString4trimEv
	.type	_ZN6icu_6713UnicodeString4trimEv, @function
_ZN6icu_6713UnicodeString4trimEv:
.LFB1300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movswl	8(%rdi), %r14d
	testb	$1, %r14b
	jne	.L37
	leaq	10(%rdi), %r15
	testb	$2, %r14b
	jne	.L4
	movq	24(%rdi), %r15
.L4:
	testw	%r14w, %r14w
	js	.L5
	sarl	$5, %r14d
.L6:
	testl	%r14d, %r14d
	jle	.L37
	movl	%r14d, %r12d
	.p2align 4,,10
	.p2align 3
.L7:
	leal	-1(%r12), %ebx
	movslq	%ebx, %rax
	movzwl	(%r15,%rax,2), %edi
	leaq	(%rax,%rax), %rdx
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L53
	cmpl	$32, %edi
	jne	.L12
.L30:
	movl	%ebx, %r12d
	testl	%ebx, %ebx
	jne	.L7
.L8:
	movzwl	8(%r13), %eax
	movl	%r12d, %edx
	sall	$5, %edx
	andl	$31, %eax
	orl	%edx, %eax
	movw	%ax, 8(%r13)
	testl	%r12d, %r12d
	je	.L37
.L17:
	xorl	%ebx, %ebx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	cmpl	$32, %edi
	je	.L23
	call	u_isWhitespace_67@PLT
	testb	%al, %al
	je	.L54
.L23:
	cmpl	%r12d, %r14d
	jge	.L21
	movl	%r14d, %ebx
.L20:
	movslq	%ebx, %rax
	leal	1(%rbx), %r14d
	movzwl	(%r15,%rax,2), %edi
	leaq	(%rax,%rax), %rdx
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L22
	cmpl	%r12d, %r14d
	je	.L22
	movzwl	2(%r15,%rdx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L22
	sall	$10, %edi
	leal	2(%rbx), %r14d
	leal	-56613888(%rax,%rdi), %edi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$32, %edi
	jne	.L55
.L13:
	andw	$31, 8(%r13)
.L37:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	movl	12(%r13), %r14d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L56:
	sall	$10, %eax
	leal	-2(%r12), %ebx
	leal	-56613888(%rdi,%rax), %edi
	.p2align 4,,10
	.p2align 3
.L12:
	call	u_isWhitespace_67@PLT
	testb	%al, %al
	jne	.L30
.L28:
	cmpl	%r12d, %r14d
	jle	.L17
	movzwl	8(%r13), %eax
	cmpl	$1023, %r12d
	jle	.L8
	orl	$-32, %eax
	movl	%r12d, 12(%r13)
	movw	%ax, 8(%r13)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L53:
	testl	%ebx, %ebx
	je	.L10
	movzwl	-2(%r15,%rdx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L56
	cmpl	$32, %edi
	jne	.L24
.L51:
	movl	%ebx, %r12d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L55:
	call	u_isWhitespace_67@PLT
	testb	%al, %al
	jne	.L13
	cmpl	$1, %r14d
	jne	.L27
	movl	$1, %r12d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L24:
	call	u_isWhitespace_67@PLT
	testb	%al, %al
	jne	.L51
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L54:
	testl	%ebx, %ebx
	jle	.L37
	movl	%ebx, %r14d
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	jmp	.L37
.L27:
	movzwl	8(%r13), %eax
	movl	$1, %r12d
	andl	$31, %eax
	orl	$32, %eax
	movw	%ax, 8(%r13)
	jmp	.L17
	.cfi_endproc
.LFE1300:
	.size	_ZN6icu_6713UnicodeString4trimEv, .-_ZN6icu_6713UnicodeString4trimEv
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
