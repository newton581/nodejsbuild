	.file	"unistr_titlecase_brkiter.cpp"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorE
	.type	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorE, @function
_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorE:
.LFB3013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_676Locale10getDefaultEv@PLT
	xorl	%edx, %edx
	leaq	-52(%rbp), %r9
	leaq	-48(%rbp), %r8
	movq	%r13, %rcx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	movq	$0, -48(%rbp)
	movl	$0, -52(%rbp)
	call	ustrcase_getTitleBreakIterator_67@PLT
	testq	%rax, %rax
	je	.L11
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%rax, %rdi
	call	ustrcase_getCaseLocale_67@PLT
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	ustrcase_internalToTitle_67@GOTPCREL(%rip), %r8
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE@PLT
.L3:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	call	*8(%rax)
.L4:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L12
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L3
.L12:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3013:
	.size	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorE, .-_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleE
	.type	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleE, @function
_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleE:
.LFB3014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	leaq	-52(%rbp), %r9
	leaq	-48(%rbp), %r8
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movl	$0, -52(%rbp)
	call	ustrcase_getTitleBreakIterator_67@PLT
	testq	%rax, %rax
	je	.L22
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%rax, %rdi
	call	ustrcase_getCaseLocale_67@PLT
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	ustrcase_internalToTitle_67@GOTPCREL(%rip), %r8
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE@PLT
.L15:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	movq	(%rdi), %rax
	call	*8(%rax)
.L16:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L15
.L23:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3014:
	.size	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleE, .-_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj
	.type	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj, @function
_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj:
.LFB3015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	leaq	-52(%rbp), %r9
	leaq	-48(%rbp), %r8
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	movq	%rsi, %rcx
	leaq	.LC0(%rip), %rsi
	pushq	%r12
	movl	%r13d, %edx
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movl	$0, -52(%rbp)
	call	ustrcase_getTitleBreakIterator_67@PLT
	testq	%rax, %rax
	je	.L33
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZNK6icu_676Locale11getBaseNameEv@PLT
	movq	%rax, %rdi
	call	ustrcase_getCaseLocale_67@PLT
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	ustrcase_internalToTitle_67@GOTPCREL(%rip), %r8
	movl	%eax, %esi
	call	_ZN6icu_6713UnicodeString7caseMapEijPNS_13BreakIteratorEPFiijS2_PDsiPKDsiPNS_5EditsER10UErrorCodeE@PLT
.L26:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	call	*8(%rax)
.L27:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	jmp	.L26
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3015:
	.size	_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj, .-_ZN6icu_6713UnicodeString7toTitleEPNS_13BreakIteratorERKNS_6LocaleEj
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
