	.file	"ucnv_u8.cpp"
	.text
	.p2align 4
	.globl	ucnv_fromUnicode_UTF8_67
	.type	ucnv_fromUnicode_UTF8_67, @function
ucnv_fromUnicode_UTF8_67:
.LFB2117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r11
	movq	16(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %r10
	movq	32(%rdi), %rcx
	movl	84(%r11), %eax
	movq	40(%rdi), %r9
	movq	48(%r11), %rbx
	testl	%eax, %eax
	je	.L2
	movl	$1, %edx
	cmpq	%r9, %rcx
	jb	.L44
.L3:
	cmpq	%r10, %r8
	jnb	.L25
	testb	%dl, %dl
	je	.L25
	movl	(%rsi), %eax
	testl	%eax, %eax
	jg	.L25
	movl	$15, (%rsi)
	movq	%rcx, %r12
	jmp	.L9
.L48:
	addq	%r13, %rcx
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	%r9, %rcx
	setnb	%dl
	cmpq	%r8, %r10
	ja	.L42
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L45:
	movb	%al, (%rcx)
	addq	$1, %rcx
.L6:
	cmpq	%rcx, %r9
	setbe	%dl
	cmpq	%r8, %r10
	jbe	.L3
.L42:
	testb	%dl, %dl
	jne	.L3
	movzwl	(%r8), %eax
	addq	$2, %r8
	movl	%eax, %edx
	cmpl	$127, %eax
	jle	.L45
	cmpl	$2047, %eax
	jg	.L7
	sarl	$6, %eax
	andl	$63, %edx
	leaq	1(%rcx), %r12
	orl	$-64, %eax
	orl	$-128, %edx
	movb	%al, (%rcx)
	cmpq	%r12, %r9
	jbe	.L8
	movb	%dl, 1(%rcx)
	addq	$2, %rcx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	movb	%dl, 104(%r11)
	movb	$1, 91(%r11)
	movl	$15, (%rsi)
.L9:
	movq	%r12, 32(%rdi)
	movq	%r8, 16(%rdi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	andl	$-2048, %edx
	cmpl	$55296, %edx
	jne	.L10
	leaq	_CESU8Data_67(%rip), %rdx
	cmpq	%rdx, %rbx
	jne	.L4
.L10:
	movq	%r9, %r12
	leaq	-60(%rbp), %rdx
	subq	%rcx, %r12
	cmpq	$4, %r12
	movl	%eax, %r12d
	cmovge	%rcx, %rdx
	sarl	$12, %r12d
	cmpl	$65535, %eax
	jle	.L47
	movl	%eax, %r14d
	andl	$63, %r12d
	movl	$4, %r13d
	movl	$2, %r15d
	orl	$-128, %r12d
	sarl	$18, %r14d
	movb	%r12b, 1(%rdx)
	orl	$-16, %r14d
	movl	$3, %r12d
.L15:
	movb	%r14b, (%rdx)
	movl	%eax, %r14d
	andl	$63, %eax
	sarl	$6, %r14d
	orl	$-128, %eax
	andl	$63, %r14d
	orl	$-128, %r14d
	movb	%r14b, (%rdx,%r15)
	movb	%al, (%rdx,%r12)
	cmpq	%rdx, %rcx
	je	.L48
	leaq	-60(%rbp,%r12), %r12
	cmpq	%r12, %rdx
	ja	.L2
	addq	$1, %r12
	jmp	.L20
.L49:
	movzbl	(%rdx), %eax
	addq	$1, %rcx
	movb	%al, -1(%rcx)
.L19:
	addq	$1, %rdx
	cmpq	%r12, %rdx
	je	.L2
.L20:
	cmpq	%rcx, %r9
	ja	.L49
	movsbq	91(%r11), %rax
	leal	1(%rax), %r13d
	movb	%r13b, 91(%r11)
	movzbl	(%rdx), %r13d
	movb	%r13b, 104(%r11,%rax)
	movl	$15, (%rsi)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$0, 84(%r11)
.L4:
	cmpq	%r10, %r8
	jnb	.L11
	testb	$4, %ah
	jne	.L12
	movzwl	(%r8), %edx
	movl	%edx, %r12d
	andl	$-1024, %r12d
	cmpl	$56320, %r12d
	jne	.L12
	sall	$10, %eax
	addq	$2, %r8
	leal	-56613888(%rdx,%rax), %eax
	jmp	.L10
.L12:
	movl	%eax, 84(%r11)
	movq	%rcx, %r12
	movl	$12, (%rsi)
	jmp	.L9
.L11:
	movl	%eax, 84(%r11)
	movq	%rcx, %r12
	jmp	.L9
.L47:
	movl	%r12d, %r14d
	movl	$3, %r13d
	movl	$1, %r15d
	movl	$2, %r12d
	orl	$-32, %r14d
	jmp	.L15
.L25:
	movq	%rcx, %r12
	jmp	.L9
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2117:
	.size	ucnv_fromUnicode_UTF8_67, .-ucnv_fromUnicode_UTF8_67
	.p2align 4
	.globl	ucnv_fromUnicode_UTF8_OFFSETS_LOGIC_67
	.type	ucnv_fromUnicode_UTF8_OFFSETS_LOGIC_67, @function
ucnv_fromUnicode_UTF8_OFFSETS_LOGIC_67:
.LFB2118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r11
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rcx
	movq	24(%rdi), %r9
	movl	84(%r11), %r13d
	movq	32(%rdi), %rax
	movq	40(%rdi), %r8
	movq	48(%r11), %rbx
	testl	%r13d, %r13d
	je	.L51
	movl	$1, %r10d
	cmpq	%r8, %rax
	jb	.L96
.L52:
	cmpq	%r9, %rdx
	jnb	.L77
	testb	%r10b, %r10b
	je	.L77
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jg	.L77
	movl	$15, (%rsi)
	movq	%rax, %r10
	jmp	.L58
.L100:
	addq	-96(%rbp), %rax
	cmpl	$3, -72(%rbp)
	movl	%r10d, (%rcx)
	movl	%r10d, 4(%rcx)
	movl	%r10d, 8(%rcx)
	jne	.L66
	movl	%r10d, 12(%rcx)
.L66:
	movl	-68(%rbp), %r13d
	addq	-104(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L51:
	cmpq	%rax, %r8
	setbe	%r12b
	movl	%r12d, %r10d
	cmpq	%rdx, %r9
	jbe	.L52
	testb	%r12b, %r12b
	jne	.L52
	movl	%r13d, %r14d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L97:
	movl	%r14d, (%rcx)
	addq	$1, %rax
	addq	$4, %rcx
	movb	%r13b, -1(%rax)
.L55:
	cmpq	%rax, %r8
	setbe	%r10b
	addl	$1, %r14d
	cmpq	%rdx, %r9
	jbe	.L52
	testb	%r10b, %r10b
	jne	.L52
.L67:
	movzwl	(%rdx), %r13d
	addq	$2, %rdx
	movl	%r14d, %r10d
	movl	%r13d, %r12d
	cmpl	$127, %r13d
	jle	.L97
	cmpl	$2047, %r13d
	jg	.L56
	sarl	$6, %r13d
	andl	$63, %r12d
	leaq	1(%rax), %r10
	movl	%r14d, (%rcx)
	orl	$-64, %r13d
	orl	$-128, %r12d
	movb	%r13b, (%rax)
	cmpq	%r10, %r8
	jbe	.L57
	movl	%r14d, 4(%rcx)
	addq	$2, %rax
	addq	$8, %rcx
	movb	%r12b, -1(%rax)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L57:
	movb	%r12b, 104(%r11)
	addq	$4, %rcx
	movb	$1, 91(%r11)
	movl	$15, (%rsi)
.L58:
	movq	%r10, 32(%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rcx, 48(%rdi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	leal	1(%r14), %r15d
	movl	%r13d, %r14d
	andl	$-2048, %r14d
	movl	%r15d, -68(%rbp)
	cmpl	$55296, %r14d
	jne	.L59
	leaq	_CESU8Data_67(%rip), %r14
	cmpq	%r14, %rbx
	jne	.L53
.L59:
	movq	%r8, %r15
	leaq	-60(%rbp), %r14
	subq	%rax, %r15
	cmpq	$4, %r15
	movq	%r14, %r15
	movl	%r13d, %r14d
	cmovge	%rax, %r15
	sarl	$12, %r14d
	movl	%r14d, -80(%rbp)
	cmpl	$65535, %r13d
	jle	.L99
	movl	%r13d, %r14d
	movq	$16, -104(%rbp)
	sarl	$18, %r14d
	movq	$4, -96(%rbp)
	orl	$-16, %r14d
	movq	$2, -88(%rbp)
	movl	%r14d, %r12d
	movzbl	-80(%rbp), %r14d
	movl	$3, -72(%rbp)
	movq	$3, -80(%rbp)
	andl	$63, %r14d
	orl	$-128, %r14d
	movb	%r14b, 1(%r15)
.L64:
	movl	%r13d, %r14d
	movb	%r12b, (%r15)
	movq	-88(%rbp), %r12
	andl	$63, %r13d
	sarl	$6, %r14d
	orl	$-128, %r13d
	andl	$63, %r14d
	orl	$-128, %r14d
	movb	%r14b, (%r15,%r12)
	movq	-80(%rbp), %r14
	movb	%r13b, (%r15,%r14)
	cmpq	%r15, %rax
	je	.L100
	leaq	-60(%rbp,%r14), %r14
	cmpq	%r14, %r15
	ja	.L75
	addq	$1, %r14
	jmp	.L70
.L101:
	movl	%r10d, (%rcx)
	movzbl	(%r15), %r12d
	addq	$1, %rax
	addq	$4, %rcx
	movb	%r12b, -1(%rax)
.L69:
	addq	$1, %r15
	cmpq	%r14, %r15
	je	.L75
.L70:
	cmpq	%rax, %r8
	ja	.L101
	movsbq	91(%r11), %r12
	leal	1(%r12), %r13d
	movb	%r13b, 91(%r11)
	movzbl	(%r15), %r13d
	movb	%r13b, 104(%r11,%r12)
	movl	$15, (%rsi)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$0, 84(%r11)
	movl	$-1, %r10d
	movl	$0, -68(%rbp)
.L53:
	cmpq	%r9, %rdx
	jnb	.L60
	testl	$1024, %r13d
	jne	.L61
	movzwl	(%rdx), %r14d
	movl	%r14d, %r15d
	andl	$-1024, %r15d
	cmpl	$56320, %r15d
	jne	.L61
	sall	$10, %r13d
	addl	$1, -68(%rbp)
	addq	$2, %rdx
	leal	-56613888(%r14,%r13), %r13d
	jmp	.L59
.L61:
	movl	%r13d, 84(%r11)
	movq	%rax, %r10
	movl	$12, (%rsi)
	jmp	.L58
.L60:
	movl	%r13d, 84(%r11)
	movq	%rax, %r10
	jmp	.L58
.L99:
	orl	$-32, %r14d
	movq	$12, -104(%rbp)
	movq	$3, -96(%rbp)
	movl	%r14d, %r12d
	movq	$1, -88(%rbp)
	movq	$2, -80(%rbp)
	movl	$2, -72(%rbp)
	jmp	.L64
.L75:
	movl	-68(%rbp), %r13d
	jmp	.L51
.L77:
	movq	%rax, %r10
	jmp	.L58
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2118:
	.size	ucnv_fromUnicode_UTF8_OFFSETS_LOGIC_67, .-ucnv_fromUnicode_UTF8_OFFSETS_LOGIC_67
	.section	.rodata
.LC1:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	" 000000000000\02000"
	.text
	.p2align 4
	.type	ucnv_toUnicode_UTF8, @function
ucnv_toUnicode_UTF8:
.LFB2115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -64(%rbp)
	movq	8(%rdi), %r14
	movq	16(%rdi), %rax
	movq	32(%rdi), %rdx
	movq	24(%rdi), %r10
	movq	48(%r14), %rcx
	movq	40(%rdi), %rbx
	movq	%rcx, -56(%rbp)
	movzbl	64(%r14), %ecx
	testb	%cl, %cl
	jle	.L103
	movl	$1, %esi
	cmpq	%rbx, %rdx
	jb	.L149
.L104:
	cmpq	%r10, %rax
	jnb	.L125
	testb	%sil, %sil
	je	.L125
	movq	-64(%rbp), %rbx
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L125
	movl	$15, (%rbx)
.L125:
	movq	%rdx, 32(%rdi)
	movq	%rax, 16(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movl	76(%r14), %esi
	movb	$0, 64(%r14)
	movsbl	%cl, %r9d
	movl	%esi, -48(%rbp)
	movl	72(%r14), %esi
	movl	$0, 72(%r14)
.L105:
	movl	-48(%rbp), %r11d
	cmpl	%r9d, %r11d
	jle	.L128
	cmpq	%r10, %rax
	jnb	.L109
	cmpl	$2, %r11d
	movslq	%r9d, %rcx
	movq	%rbx, -96(%rbp)
	leaq	1(%rax), %r13
	setle	-41(%rbp)
	addq	%r14, %rcx
	xorl	%r8d, %r8d
	movq	%r14, -72(%rbp)
	movq	%rcx, %rbx
	movq	%rdx, -80(%rbp)
	movq	%rdi, -88(%rbp)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L152:
	sall	$6, %esi
	leaq	0(%r13,%r8), %r12
	leal	1(%r9), %ecx
	addl	%edi, %esi
.L119:
	addq	$1, %r8
	movl	%ecx, %r9d
	cmpq	%r12, %r10
	jbe	.L150
.L110:
	movzbl	(%rax,%r8), %edi
	leaq	(%rax,%r8), %r12
	movb	%dil, 65(%rbx,%r8)
	movl	%edi, %edx
	cmpl	$1, %r9d
	jg	.L131
	cmpb	$0, -41(%rbp)
	jne	.L131
	cmpl	$3, %r11d
	je	.L151
	movl	%esi, %ecx
	movl	$1, %r15d
	leaq	.LC1(%rip), %r14
	andl	$7, %ecx
	sall	%cl, %r15d
	movl	%r15d, %ecx
	movl	%edi, %r15d
	sarl	$4, %r15d
	movslq	%r15d, %r15
	andb	(%r14,%r15), %cl
.L116:
	testb	%cl, %cl
	jne	.L152
.L113:
	leaq	_CESU8Data_67(%rip), %r15
	cmpq	%r15, -56(%rbp)
	jne	.L117
	cmpl	$1, %r9d
	jne	.L117
	cmpl	$237, %esi
	jne	.L130
	cmpb	$-64, %dl
	jge	.L130
.L114:
	sall	$6, %esi
	leal	1(%r9), %ecx
	leaq	0(%r13,%r8), %r12
	addl	%edi, %esi
	movl	%ecx, %r9d
	cmpl	%ecx, %r11d
	jg	.L119
	movq	-72(%rbp), %r14
	movq	-96(%rbp), %rbx
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rdi
.L108:
	cmpl	%r9d, -48(%rbp)
	jne	.L118
	leaq	_CESU8Data_67(%rip), %rax
	cmpq	%rax, -56(%rbp)
	jne	.L121
	movl	-48(%rbp), %eax
	movl	%eax, %r9d
	cmpl	$3, %eax
	jg	.L118
.L121:
	movslq	-48(%rbp), %rax
	leaq	_ZL15offsetsFromUTF8(%rip), %rcx
	subl	(%rcx,%rax,4), %esi
	leaq	2(%rdx), %rcx
	cmpl	$65535, %esi
	ja	.L122
	movw	%si, (%rdx)
	movq	%r12, %rax
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L103:
	cmpq	%rdx, %rbx
	setbe	%sil
	cmpq	%rax, %r10
	ja	.L148
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L153:
	addq	$2, %rdx
	movw	%si, -2(%rdx)
	cmpq	%rdx, %rbx
	setbe	%sil
	cmpq	%rax, %r10
	jbe	.L104
.L148:
	testb	%sil, %sil
	jne	.L104
	movzbl	(%rax), %esi
	addq	$1, %rax
	testb	%sil, %sil
	jns	.L153
	leal	62(%rsi), %r11d
	movb	%sil, 65(%r14)
	movl	$1, %ecx
	movl	$1, %r9d
	movl	$0, -48(%rbp)
	cmpb	$50, %r11b
	ja	.L105
	xorl	%r11d, %r11d
	cmpb	$-33, %sil
	seta	%r11b
	cmpb	$-17, %sil
	seta	%r8b
	movzbl	%r8b, %r8d
	leal	2(%r11,%r8), %r11d
	movl	%r11d, -48(%rbp)
	jmp	.L105
.L130:
	movq	-72(%rbp), %r14
	movq	-80(%rbp), %rdx
	movl	$1, %r9d
	movq	-88(%rbp), %rdi
.L118:
	movq	-64(%rbp), %rax
	movb	%r9b, 64(%r14)
	movl	$12, (%rax)
	movq	%r12, %rax
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L131:
	cmpb	$-64, %dl
	jl	.L114
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L151:
	movl	%edi, %ecx
	movl	$1, %r14d
	movl	%esi, %r15d
	sarl	$5, %ecx
	andl	$15, %r15d
	sall	%cl, %r14d
	movl	%r14d, %ecx
	leaq	.LC0(%rip), %r14
	andb	(%r14,%r15), %cl
	jmp	.L116
.L150:
	movq	-72(%rbp), %r14
	movq	-96(%rbp), %rbx
	movq	%r12, %rax
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rdi
.L109:
	movl	%esi, 72(%r14)
	movl	-48(%rbp), %esi
	cmpq	%rbx, %rdx
	movb	%cl, 64(%r14)
	movl	%esi, 76(%r14)
	setnb	%sil
	jmp	.L104
.L128:
	movq	%rax, %r12
	jmp	.L108
.L117:
	movq	-72(%rbp), %r14
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rdi
	jmp	.L118
.L122:
	movl	%esi, %eax
	andw	$1023, %si
	shrl	$10, %eax
	subw	$10304, %ax
	movw	%ax, (%rdx)
	movl	%esi, %eax
	orw	$-9216, %ax
	cmpq	%rcx, %rbx
	jbe	.L123
	movw	%ax, 2(%rdx)
	addq	$4, %rdx
	movq	%r12, %rax
	jmp	.L103
.L123:
	movw	%ax, 144(%r14)
	movq	-64(%rbp), %rax
	movq	%rcx, %rdx
	movb	$1, 93(%r14)
	movl	$15, (%rax)
	movq	%r12, %rax
	jmp	.L125
	.cfi_endproc
.LFE2115:
	.size	ucnv_toUnicode_UTF8, .-ucnv_toUnicode_UTF8
	.p2align 4
	.type	ucnv_toUnicode_UTF8_OFFSETS_LOGIC, @function
ucnv_toUnicode_UTF8_OFFSETS_LOGIC:
.LFB2116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	8(%rdi), %rax
	movq	16(%rdi), %r12
	movq	32(%rdi), %rdx
	movq	48(%rdi), %r8
	movq	48(%rax), %rcx
	movq	24(%rdi), %rbx
	movq	%rax, -64(%rbp)
	movq	40(%rdi), %r15
	movq	%rcx, -48(%rbp)
	movzbl	64(%rax), %ecx
	testb	%cl, %cl
	jle	.L179
	movl	$1, %esi
	cmpq	%r15, %rdx
	jb	.L201
.L156:
	cmpq	%rbx, %r12
	jnb	.L177
	testb	%sil, %sil
	je	.L177
	movq	-72(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L177
	movl	$15, (%rax)
.L177:
	movq	%rdx, 32(%rdi)
	movq	%r12, 16(%rdi)
	movq	%r8, 48(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movl	76(%rax), %esi
	movl	72(%rax), %r9d
	movb	$0, 64(%rax)
	movsbl	%cl, %r11d
	movl	$0, 72(%rax)
	movq	%r12, %rax
	movl	%esi, -56(%rbp)
	xorl	%esi, %esi
.L157:
	movl	-56(%rbp), %r12d
	cmpl	%r11d, %r12d
	jle	.L182
	cmpq	%rbx, %rax
	jnb	.L183
	cmpl	$2, %r12d
	movslq	%r11d, %rcx
	movq	%r8, -88(%rbp)
	leaq	1(%rax), %r13
	setle	-49(%rbp)
	addq	-64(%rbp), %rcx
	xorl	%r10d, %r10d
	movq	%rdi, -96(%rbp)
	movq	%rcx, %r8
	movl	%r12d, %edi
	movq	%r15, -104(%rbp)
	movl	%esi, -108(%rbp)
	movq	%rdx, -80(%rbp)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L204:
	sall	$6, %r9d
	leaq	0(%r13,%r10), %r12
	leal	1(%r11), %ecx
	addl	%esi, %r9d
.L171:
	addq	$1, %r10
	movl	%ecx, %r11d
	cmpq	%rbx, %r12
	jnb	.L202
.L162:
	movzbl	(%rax,%r10), %esi
	leaq	(%rax,%r10), %r12
	movb	%sil, 65(%r8,%r10)
	movl	%esi, %edx
	cmpl	$1, %r11d
	jg	.L186
	cmpb	$0, -49(%rbp)
	jne	.L186
	cmpl	$3, %edi
	je	.L203
	movl	%r9d, %ecx
	movl	$1, %r15d
	leaq	.LC1(%rip), %r14
	andl	$7, %ecx
	sall	%cl, %r15d
	movl	%r15d, %ecx
	movl	%esi, %r15d
	sarl	$4, %r15d
	movslq	%r15d, %r15
	andb	(%r14,%r15), %cl
.L168:
	testb	%cl, %cl
	jne	.L204
.L165:
	leaq	_CESU8Data_67(%rip), %r15
	cmpq	%r15, -48(%rbp)
	jne	.L169
	cmpl	$1, %r11d
	jne	.L169
	cmpl	$237, %r9d
	jne	.L185
	cmpb	$-64, %dl
	jge	.L185
.L166:
	sall	$6, %r9d
	leal	1(%r11), %ecx
	leaq	0(%r13,%r10), %r12
	addl	%esi, %r9d
	movl	%ecx, %r11d
	cmpl	%ecx, %edi
	jg	.L171
	movq	-104(%rbp), %r15
	movl	-108(%rbp), %esi
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rdi
.L160:
	cmpl	%r11d, -56(%rbp)
	jne	.L170
	leaq	_CESU8Data_67(%rip), %rax
	cmpq	%rax, -48(%rbp)
	jne	.L173
	movl	-56(%rbp), %eax
	cmpl	$3, %eax
	jle	.L173
	movl	%eax, %r11d
.L170:
	movq	-64(%rbp), %rax
	movb	%r11b, 64(%rax)
	movq	-72(%rbp), %rax
	movl	$12, (%rax)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L179:
	xorl	%r11d, %r11d
.L155:
	cmpq	%rdx, %r15
	setbe	%al
	movl	%eax, %esi
	cmpq	%r12, %rbx
	jbe	.L156
	testb	%al, %al
	jne	.L156
	movq	%r12, %rax
	subl	%r12d, %r11d
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L205:
	addq	$2, %rdx
	addq	$4, %r8
	movw	%r9w, -2(%rdx)
	cmpq	%rdx, %r15
	movl	%esi, -4(%r8)
	setbe	%sil
	cmpq	%rax, %rbx
	jbe	.L181
	testb	%sil, %sil
	jne	.L181
.L159:
	leal	(%r11,%rax), %esi
	movzbl	(%rax), %r9d
	addq	$1, %rax
	testb	%r9b, %r9b
	jns	.L205
	movq	-64(%rbp), %rcx
	leal	62(%r9), %r12d
	movl	$0, -56(%rbp)
	movl	$1, %r11d
	movb	%r9b, 65(%rcx)
	movl	$1, %ecx
	cmpb	$50, %r12b
	ja	.L157
	xorl	%r12d, %r12d
	cmpb	$-33, %r9b
	seta	%r12b
	cmpb	$-17, %r9b
	seta	%r10b
	movzbl	%r10b, %r10d
	leal	2(%r12,%r10), %r14d
	movl	%r14d, -56(%rbp)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L186:
	cmpb	$-64, %dl
	jl	.L166
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L203:
	movl	%esi, %ecx
	movl	$1, %r14d
	movl	%r9d, %r15d
	sarl	$5, %ecx
	andl	$15, %r15d
	sall	%cl, %r14d
	movl	%r14d, %ecx
	leaq	.LC0(%rip), %r14
	andb	(%r14,%r15), %cl
	jmp	.L168
.L185:
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r8
	movl	$1, %r11d
	movq	-96(%rbp), %rdi
	jmp	.L170
.L202:
	movq	-104(%rbp), %r15
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rdi
.L161:
	movq	-64(%rbp), %rax
	movl	-56(%rbp), %esi
	cmpq	%rdx, %r15
	movl	%esi, 76(%rax)
	setbe	%sil
	movl	%r9d, 72(%rax)
	movb	%cl, 64(%rax)
	jmp	.L156
.L182:
	movq	%rax, %r12
	jmp	.L160
.L181:
	movq	%rax, %r12
	jmp	.L156
.L169:
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rdi
	jmp	.L170
.L173:
	movslq	-56(%rbp), %rax
	leaq	_ZL15offsetsFromUTF8(%rip), %rcx
	subl	(%rcx,%rax,4), %r9d
	leaq	2(%rdx), %rax
	leaq	4(%r8), %rcx
	cmpl	$65535, %r9d
	ja	.L174
	movw	%r9w, (%rdx)
	movq	%rax, %rdx
	movl	%esi, (%r8)
	movq	%rcx, %r8
.L175:
	addl	%esi, %r11d
	jmp	.L155
.L174:
	movl	%r9d, %r10d
	andw	$1023, %r9w
	shrl	$10, %r10d
	orw	$-9216, %r9w
	subw	$10304, %r10w
	movw	%r10w, (%rdx)
	movl	%esi, (%r8)
	cmpq	%rax, %r15
	jbe	.L176
	movw	%r9w, 2(%rdx)
	addq	$8, %r8
	addq	$4, %rdx
	movl	%esi, -4(%r8)
	jmp	.L175
.L176:
	movq	-64(%rbp), %rdx
	movq	%rcx, %r8
	movw	%r9w, 144(%rdx)
	movb	$1, 93(%rdx)
	movq	-72(%rbp), %rdx
	movl	$15, (%rdx)
	movq	%rax, %rdx
	jmp	.L175
.L183:
	movq	%rax, %r12
	jmp	.L161
	.cfi_endproc
.LFE2116:
	.size	ucnv_toUnicode_UTF8_OFFSETS_LOGIC, .-ucnv_toUnicode_UTF8_OFFSETS_LOGIC
	.p2align 4
	.type	ucnv_UTF8FromUTF8, @function
ucnv_UTF8FromUTF8:
.LFB2120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	movq	8(%rsi), %r10
	movq	24(%rsi), %r14
	movq	16(%rsi), %rbx
	movq	32(%rdi), %r8
	movzbl	64(%r10), %edx
	movq	%r14, %rcx
	subq	%rbx, %rcx
	testb	%dl, %dl
	jle	.L207
	movl	76(%r10), %esi
	movsbl	%dl, %r12d
	movl	72(%r10), %eax
	leal	(%r12,%rcx), %edi
	movl	%esi, %r9d
	movsbl	%sil, %esi
	cmpl	%edi, %esi
	jg	.L208
.L272:
	movq	-56(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, -72(%rbp)
	subq	%r8, %rcx
	cmpl	%esi, %ecx
	jl	.L393
	cmpl	%ecx, %edi
	cmovg	%ecx, %edi
	movl	%edi, %r11d
	subl	%esi, %r11d
	testl	%r11d, %r11d
	jle	.L208
	movslq	%r11d, %rcx
	movq	%rcx, -72(%rbp)
	movzbl	-1(%rbx,%rcx), %ecx
	testb	%cl, %cl
	jns	.L208
	leal	62(%rcx), %r13d
	cmpb	$50, %r13b
	jbe	.L394
	cmpb	$-64, %cl
	jge	.L208
	cmpl	$1, %r11d
	je	.L395
	movq	-72(%rbp), %r13
	movzbl	-2(%rbx,%r13), %r13d
	movb	%r13b, -80(%rbp)
	addl	$32, %r13d
	cmpb	$20, %r13b
	ja	.L214
	movzbl	%cl, %r13d
	cmpb	$-17, -80(%rbp)
	movl	%r13d, -72(%rbp)
	ja	.L215
	movl	%r13d, %ecx
	movl	$1, %r13d
	sarl	$5, %ecx
	sall	%cl, %r13d
	movq	-80(%rbp), %rcx
	movl	%r13d, -72(%rbp)
	leaq	.LC0(%rip), %r13
	andl	$15, %ecx
	movsbl	0(%r13,%rcx), %ecx
	andl	-72(%rbp), %ecx
.L216:
	leal	-2(%rsi,%r11), %esi
	testl	%ecx, %ecx
	cmovne	%esi, %edi
	.p2align 4,,10
	.p2align 3
.L208:
	testl	%eax, %eax
	jne	.L396
.L218:
	testl	%edi, %edi
	jle	.L221
	leaq	.LC0(%rip), %rcx
.L257:
	movzbl	(%rbx), %eax
	leaq	1(%rbx), %rsi
	testb	%al, %al
	jns	.L397
	cmpb	$-33, %al
	ja	.L398
	cmpb	$-63, %al
	jbe	.L225
	movzbl	1(%rbx), %edx
	cmpb	$-64, %dl
	jl	.L399
.L226:
	cmpb	$-33, %al
	seta	%r9b
	cmpb	$-17, %al
	seta	%dl
	leal	2(%r9,%rdx), %r9d
.L227:
	movl	$0, -72(%rbp)
	movq	$-1, %rcx
	movl	$1, %edx
	xorl	%r11d, %r11d
	movl	$1, %r12d
.L228:
	cmpb	%dl, %r9b
	jle	.L219
	movsbl	%r9b, %ebx
	movl	%ebx, -80(%rbp)
	cmpq	%rsi, %r14
	jbe	.L274
	cmpl	$2, %ebx
	leaq	.LC1(%rip), %r13
	setle	%r12b
	cmpb	$3, %r9b
	je	.L400
	movl	%edi, -84(%rbp)
.L230:
	movzbl	(%rsi), %edi
	movl	%edi, %ecx
	cmpb	$1, %dl
	jg	.L232
	testb	%r12b, %r12b
	jne	.L232
	movl	%eax, %ecx
	movl	$1, %ebx
	andl	$7, %ecx
	sall	%cl, %ebx
	movl	%ebx, %ecx
	movl	%edi, %ebx
	sarl	$4, %ebx
	movslq	%ebx, %rbx
	andb	0(%r13,%rbx), %cl
.L237:
	testb	%cl, %cl
	je	.L234
	sall	$6, %eax
	addl	$1, %edx
	addq	$1, %rsi
	addl	%edi, %eax
	cmpb	%dl, %r9b
	je	.L401
	cmpq	%rsi, %r14
	jne	.L230
.L238:
	movsbl	%dl, %ecx
	subl	-72(%rbp), %ecx
	movslq	%ecx, %rcx
	negq	%rcx
.L229:
	addq	%r14, %rcx
	cmpb	%dl, %r11b
	jge	.L244
	movzbl	(%rcx), %r9d
	movsbq	%r11b, %rdi
	leal	1(%r11), %esi
	movb	%r9b, 65(%r10,%rdi)
	cmpb	%sil, %dl
	jle	.L245
	movzbl	1(%rcx), %r9d
	movsbq	%sil, %rsi
	leal	2(%r11), %edi
	movb	%r9b, 65(%r10,%rsi)
	cmpb	%dil, %dl
	jle	.L245
	movzbl	2(%rcx), %r9d
	movsbq	%dil, %rdi
	leal	3(%r11), %esi
	movb	%r9b, 65(%r10,%rdi)
	cmpb	%sil, %dl
	jle	.L245
	movzbl	3(%rcx), %r9d
	movsbq	%sil, %rsi
	leal	4(%r11), %edi
	movb	%r9b, 65(%r10,%rsi)
	cmpb	%dil, %dl
	jle	.L245
	movzbl	4(%rcx), %r9d
	movsbq	%dil, %rdi
	leal	5(%r11), %esi
	movb	%r9b, 65(%r10,%rdi)
	cmpb	%sil, %dl
	jle	.L245
	movzbl	5(%rcx), %r9d
	movsbq	%sil, %rsi
	leal	6(%r11), %edi
	movb	%r9b, 65(%r10,%rsi)
	cmpb	%dil, %dl
	jle	.L245
	movzbl	6(%rcx), %esi
	movsbq	%dil, %rdi
	movb	%sil, 65(%r10,%rdi)
.L245:
	notl	%r11d
	leal	(%r11,%rdx), %esi
	movzbl	%sil, %esi
	leaq	1(%rcx,%rsi), %rcx
.L244:
	movl	%eax, 72(%r10)
	movl	-80(%rbp), %eax
	movb	%dl, 64(%r10)
	movl	%eax, 76(%r10)
	movq	-56(%rbp), %rax
	movq	%rcx, 16(%r15)
	movq	%r8, 32(%rax)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L397:
	movb	%al, (%r8)
	subl	$1, %edi
	addq	$1, %r8
	movq	%rsi, %rbx
.L223:
	testl	%edi, %edi
	jg	.L257
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L398:
	cmpb	$-17, %al
	ja	.L225
	movzbl	1(%rbx), %edx
	movq	%rax, %r9
	andl	$15, %r9d
	movsbl	(%rcx,%r9), %r11d
	movl	%edx, %r9d
	shrb	$5, %r9b
	btl	%r9d, %r11d
	jnc	.L226
	movzbl	2(%rbx), %r9d
	cmpb	$-64, %r9b
	jge	.L226
	movb	%al, (%r8)
	addq	$3, %rbx
	subl	$3, %edi
	addq	$3, %r8
	movb	%dl, -2(%r8)
	movb	%r9b, -1(%r8)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$0, 72(%r10)
	movl	%edx, %r11d
	movq	%rbx, %rsi
	xorl	%ecx, %ecx
	movb	$0, 64(%r10)
	movl	%r12d, -72(%rbp)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L232:
	cmpb	$-64, %cl
	setl	%cl
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L207:
	testl	%ecx, %ecx
	jns	.L402
.L221:
	movq	-64(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L258
	cmpq	%r14, %rbx
	jb	.L403
.L258:
	movq	-56(%rbp), %rax
	movq	%rbx, 16(%r15)
	movq	%r8, 32(%rax)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L225:
	leal	62(%rax), %edx
	cmpb	$50, %dl
	jbe	.L226
	xorl	%r9d, %r9d
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L400:
	movl	%edi, -84(%rbp)
	movq	%rsi, %r12
	movl	%edx, %ebx
	movb	%r9b, -85(%rbp)
.L231:
	movzbl	(%r12), %edi
	movl	%ebx, %edx
	movq	%r12, %rsi
	movl	%edi, %ecx
	cmpb	$1, %bl
	jg	.L404
	sarl	$5, %ecx
	movl	$1, %r9d
	sall	%cl, %r9d
	movl	%eax, %ecx
	movl	%r9d, %r13d
	andl	$15, %ecx
	leaq	.LC0(%rip), %r9
	testb	%r13b, (%r9,%rcx)
	je	.L234
	sall	$6, %eax
	leaq	1(%r12), %rcx
	leal	1(%rbx), %edx
	addl	%edi, %eax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L404:
	cmpb	$-64, %dil
	jge	.L234
	leaq	1(%r12), %rcx
	sall	$6, %eax
	leal	1(%rbx), %edx
	movq	%rcx, %rsi
	addl	%edi, %eax
	cmpb	$2, %dl
	jg	.L405
.L243:
	movq	%rcx, %r12
	addl	$1, %ebx
	cmpq	%r14, %rcx
	jb	.L231
	movq	%rcx, %r14
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L402:
	movl	%ecx, %edi
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L234:
	movsbl	%dl, %ecx
	subl	-72(%rbp), %ecx
	movslq	%ecx, %rcx
	negq	%rcx
.L242:
	addq	%rcx, %rsi
	cmpb	%dl, %r11b
	jge	.L247
	movzbl	(%rsi), %edi
	movsbq	%r11b, %rcx
	leal	1(%r11), %eax
	movb	%dil, 65(%r10,%rcx)
	cmpb	%dl, %al
	jge	.L248
	movzbl	1(%rsi), %edi
	movsbq	%al, %rax
	leal	2(%r11), %ecx
	movb	%dil, 65(%r10,%rax)
	cmpb	%cl, %dl
	jle	.L248
	movzbl	2(%rsi), %edi
	movsbq	%cl, %rcx
	leal	3(%r11), %eax
	movb	%dil, 65(%r10,%rcx)
	cmpb	%dl, %al
	jge	.L248
	movzbl	3(%rsi), %edi
	movsbq	%al, %rax
	leal	4(%r11), %ecx
	movb	%dil, 65(%r10,%rax)
	cmpb	%dl, %cl
	jge	.L248
	movzbl	4(%rsi), %edi
	movsbq	%cl, %rcx
	leal	5(%r11), %eax
	movb	%dil, 65(%r10,%rcx)
	cmpb	%dl, %al
	jge	.L248
	movzbl	5(%rsi), %edi
	movsbq	%al, %rax
	leal	6(%r11), %ecx
	movb	%dil, 65(%r10,%rax)
	cmpb	%dl, %cl
	jge	.L248
	movzbl	6(%rsi), %eax
	movsbq	%cl, %rcx
	movb	%al, 65(%r10,%rcx)
.L248:
	notl	%r11d
	leal	(%r11,%rdx), %eax
	movzbl	%al, %eax
	leaq	1(%rsi,%rax), %rsi
.L247:
	movq	-56(%rbp), %rax
	movb	%dl, 64(%r10)
	movq	%rsi, 16(%r15)
	movq	%r8, 32(%rax)
	movq	-64(%rbp), %rax
	movl	$12, (%rax)
.L206:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movl	$-127, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	movb	%al, (%r8)
	addq	$2, %rbx
	subl	$2, %edi
	addq	$2, %r8
	movb	%dl, -1(%r8)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L401:
	movl	-84(%rbp), %edi
.L235:
	movsbl	%dl, %r12d
	movl	%r12d, %ecx
	subl	-72(%rbp), %ecx
	movslq	%ecx, %rcx
	negq	%rcx
.L219:
	cmpb	%dl, %r9b
	jne	.L242
	testb	%r11b, %r11b
	je	.L249
	movzbl	65(%r10), %eax
	movb	%al, (%r8)
	cmpb	$1, %r11b
	je	.L250
	movzbl	66(%r10), %eax
	movb	%al, 1(%r8)
	cmpb	$2, %r11b
	je	.L250
	movzbl	67(%r10), %eax
	movb	%al, 2(%r8)
	cmpb	$3, %r11b
	je	.L250
	movzbl	68(%r10), %eax
	movb	%al, 3(%r8)
	cmpb	$4, %r11b
	je	.L250
	movzbl	69(%r10), %eax
	movb	%al, 4(%r8)
	cmpb	$5, %r11b
	je	.L250
	movzbl	70(%r10), %eax
	movb	%al, 5(%r8)
	cmpb	$6, %r11b
	je	.L250
	movzbl	71(%r10), %eax
	movb	%al, 6(%r8)
.L250:
	leal	-1(%r11), %eax
	movzbl	%al, %eax
	leaq	1(%r8,%rax), %r8
.L249:
	leaq	(%rsi,%rcx), %rbx
	cmpb	%r11b, %dl
	jle	.L251
	leaq	16(%rsi,%rcx), %rax
	leal	-1(%rdx), %r13d
	subl	%r11d, %r13d
	cmpq	%rax, %r8
	leaq	16(%r8), %rax
	setnb	%cl
	cmpq	%rax, %rbx
	setnb	%al
	orb	%al, %cl
	je	.L252
	cmpb	$14, %r13b
	jbe	.L252
	movl	%edx, %esi
	movdqu	(%rbx), %xmm0
	subl	%r11d, %esi
	movl	%esi, %eax
	movups	%xmm0, (%r8)
	shrb	$4, %al
	cmpb	$1, %al
	je	.L253
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 16(%r8)
	cmpb	$2, %al
	je	.L253
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 32(%r8)
	cmpb	$3, %al
	je	.L253
	movdqu	48(%rbx), %xmm3
	movups	%xmm3, 48(%r8)
	cmpb	$4, %al
	je	.L253
	movdqu	64(%rbx), %xmm4
	movups	%xmm4, 64(%r8)
	cmpb	$5, %al
	je	.L253
	movdqu	80(%rbx), %xmm5
	movups	%xmm5, 80(%r8)
	cmpb	$6, %al
	je	.L253
	movdqu	96(%rbx), %xmm6
	movups	%xmm6, 96(%r8)
.L253:
	movl	%esi, %r9d
	movq	%rsi, %rax
	andl	$240, %eax
	andl	$-16, %r9d
	leaq	(%rbx,%rax), %rcx
	addl	%r9d, %r11d
	addq	%r8, %rax
	cmpb	%sil, %r9b
	je	.L392
	movzbl	(%rcx), %esi
	movb	%sil, (%rax)
	leal	1(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	1(%rcx), %esi
	movb	%sil, 1(%rax)
	leal	2(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	2(%rcx), %esi
	movb	%sil, 2(%rax)
	leal	3(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	3(%rcx), %esi
	movb	%sil, 3(%rax)
	leal	4(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	4(%rcx), %esi
	movb	%sil, 4(%rax)
	leal	5(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	5(%rcx), %esi
	movb	%sil, 5(%rax)
	leal	6(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	6(%rcx), %esi
	movb	%sil, 6(%rax)
	leal	7(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	7(%rcx), %esi
	movb	%sil, 7(%rax)
	leal	8(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	8(%rcx), %esi
	movb	%sil, 8(%rax)
	leal	9(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	9(%rcx), %esi
	movb	%sil, 9(%rax)
	leal	10(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	10(%rcx), %esi
	movb	%sil, 10(%rax)
	leal	11(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	11(%rcx), %esi
	movb	%sil, 11(%rax)
	leal	12(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	12(%rcx), %esi
	movb	%sil, 12(%rax)
	leal	13(%r11), %esi
	cmpb	%sil, %dl
	jle	.L392
	movzbl	13(%rcx), %esi
	addl	$14, %r11d
	movb	%sil, 13(%rax)
	cmpb	%r11b, %dl
	jle	.L392
	movzbl	14(%rcx), %edx
	movb	%dl, 14(%rax)
	.p2align 4,,10
	.p2align 3
.L392:
	movzbl	%r13b, %r13d
.L255:
	addq	$1, %r13
	addq	%r13, %rbx
	addq	%r13, %r8
.L251:
	subl	%r12d, %edi
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L394:
	leal	-1(%rsi,%r11), %edi
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rsi, %r14
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L405:
	movl	-84(%rbp), %edi
	movzbl	-85(%rbp), %r9d
	jmp	.L235
.L252:
	movzbl	%r13b, %r13d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L256:
	movzbl	(%rbx,%rax), %edx
	movb	%dl, (%r8,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%r13, %rdx
	jne	.L256
	jmp	.L255
.L403:
	movq	-56(%rbp), %rax
	cmpq	%r8, 40(%rax)
	je	.L406
	movzbl	(%rbx), %ecx
	movl	$1, %edx
	movl	$1, %eax
	testb	%cl, %cl
	js	.L407
.L260:
	movq	%r14, %rsi
	subq	%rbx, %rsi
	cmpq	%rdx, %rsi
	jge	.L261
	addq	$1, %rbx
	movb	%cl, 65(%r10)
	movzbl	%cl, %edx
	movsbl	%al, %r13d
	cmpq	%rbx, %r14
	je	.L408
	cmpl	$2, %r13d
	setle	%r9b
	cmpb	$3, %al
	movl	$1, %eax
	je	.L277
	leaq	.LC1(%rip), %r12
	jmp	.L268
.L409:
	testb	%r9b, %r9b
	jne	.L264
	movl	%edx, %ecx
	movl	$1, %r11d
	andl	$7, %ecx
	sall	%cl, %r11d
	movl	%r11d, %ecx
	movl	%edi, %r11d
	sarl	$4, %r11d
	movslq	%r11d, %r11
	andb	(%r12,%r11), %cl
.L265:
	testb	%cl, %cl
	je	.L266
	sall	$6, %edx
	addq	$1, %rbx
	movb	%sil, 65(%r10,%rax)
	leal	1(%rax), %ecx
	addl	%edi, %edx
	cmpq	%rbx, %r14
	je	.L267
	movsbq	%cl, %rax
.L268:
	movzbl	(%rbx), %edi
	movl	%edi, %esi
	cmpb	$1, %al
	jle	.L409
.L264:
	cmpb	$-64, %sil
	setl	%cl
	jmp	.L265
.L214:
	cmpb	$-64, -80(%rbp)
	jge	.L208
	cmpl	$2, %r11d
	je	.L410
	movq	-72(%rbp), %rcx
	movzbl	-3(%rbx,%rcx), %ecx
	movb	%cl, -72(%rbp)
	addl	$16, %ecx
	cmpb	$4, %cl
	ja	.L208
	movq	-80(%rbp), %rcx
	leaq	.LC1(%rip), %r13
	shrq	$4, %rcx
	andl	$15, %ecx
	movsbl	0(%r13,%rcx), %ecx
	movl	%ecx, %r13d
	movzbl	-72(%rbp), %ecx
	andl	$7, %ecx
	sarl	%cl, %r13d
	leal	-3(%rsi,%r11), %ecx
	andl	$1, %r13d
	cmovne	%ecx, %edi
	jmp	.L208
.L395:
	leal	1(%rsi), %edi
	jmp	.L208
.L261:
	movq	-64(%rbp), %rax
	movl	$-127, (%rax)
	jmp	.L258
.L215:
	movzbl	-80(%rbp), %ecx
	movl	$1, %r13d
	andl	$7, %ecx
	sall	%cl, %r13d
	movl	-72(%rbp), %ecx
	movl	%r13d, -80(%rbp)
	leaq	.LC1(%rip), %r13
	sarl	$4, %ecx
	movslq	%ecx, %rcx
	movsbl	0(%r13,%rcx), %ecx
	andl	-80(%rbp), %ecx
	jmp	.L216
.L406:
	movq	-64(%rbp), %rax
	movl	$15, (%rax)
	jmp	.L258
.L407:
	leal	62(%rcx), %eax
	cmpb	$50, %al
	ja	.L276
	cmpb	$-33, %cl
	seta	%al
	cmpb	$-17, %cl
	seta	%dl
	leal	2(%rax,%rdx), %eax
	movzbl	%al, %edx
	jmp	.L260
.L408:
	movl	$1, %ecx
.L267:
	movl	%edx, 72(%r10)
	movb	%cl, 64(%r10)
	movl	%r13d, 76(%r10)
	jmp	.L258
.L277:
	movl	$1, %r12d
	leaq	.LC0(%rip), %r11
	jmp	.L263
.L271:
	sall	$6, %edx
	addq	$1, %rbx
	movb	%sil, 65(%r10,%rax)
	leal	1(%rax), %ecx
	addl	%edi, %edx
	cmpq	%rbx, %r14
	je	.L267
	movsbq	%cl, %rax
.L263:
	movzbl	(%rbx), %edi
	cmpb	$-64, %dil
	movl	%edi, %esi
	setl	%cl
	cmpb	$1, %al
	jg	.L270
	movl	%edi, %ecx
	movl	%r12d, %r9d
	sarl	$5, %ecx
	sall	%cl, %r9d
	movl	%r9d, %ecx
	movl	%edx, %r9d
	andl	$15, %r9d
	andb	(%r11,%r9), %cl
.L270:
	testb	%cl, %cl
	jne	.L271
.L266:
	movb	%al, 64(%r10)
	movq	-64(%rbp), %rax
	movl	$12, (%rax)
	jmp	.L258
.L276:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L260
.L410:
	leal	2(%rsi), %edi
	jmp	.L208
	.cfi_endproc
.LFE2120:
	.size	ucnv_UTF8FromUTF8, .-ucnv_UTF8FromUTF8
	.p2align 4
	.type	ucnv_getNextUChar_UTF8, @function
ucnv_getNextUChar_UTF8:
.LFB2119:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rcx
	movq	24(%rdi), %r10
	cmpq	%rcx, %r10
	jbe	.L458
	movzbl	(%rcx), %eax
	leaq	1(%rcx), %rdx
	testb	%al, %al
	jns	.L459
	leal	62(%rax), %r8d
	movq	8(%rdi), %r9
	cmpb	$50, %r8b
	ja	.L415
	xorl	%r8d, %r8d
	cmpb	$-33, %al
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	seta	%r8b
	xorl	%r11d, %r11d
	cmpb	$-17, %al
	seta	%r11b
	addl	%r11d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	leal	1(%r8), %ebx
	movzwl	%bx, %r11d
	addq	%rdx, %r11
	cmpq	%r11, %r10
	jb	.L460
	movzbl	%al, %r8d
	movl	%r8d, %r10d
	movzbl	1(%rcx), %r8d
	sall	$6, %r10d
	cmpw	$2, %bx
	je	.L461
	cmpw	$1, %bx
	je	.L462
	movl	%r8d, %r11d
	leaq	.LC1(%rip), %rbx
	andl	$7, %eax
	sarl	$4, %r11d
	movslq	%r11d, %r11
	movsbl	(%rbx,%r11), %r11d
	btl	%eax, %r11d
	jnc	.L428
	movzbl	2(%rcx), %edx
	cmpb	$-64, %dl
	jge	.L457
	movzbl	3(%rcx), %r11d
	cmpb	$-64, %r11b
	jl	.L432
	leaq	3(%rcx), %rdx
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%rdx, 16(%rdi)
	cmpq	%rcx, %rdx
	jbe	.L437
	subq	%rcx, %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L434:
	movzbl	(%rcx,%rax), %r8d
	movsbq	%al, %rdi
	addq	$1, %rax
	movb	%r8b, 65(%r9,%rdi)
	cmpq	%rax, %rdx
	jne	.L434
.L433:
	movb	%dl, 64(%r9)
	movl	$65535, %eax
	movl	$12, (%rsi)
.L411:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 14
	movq	%rdx, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	movl	$8, (%rsi)
	movl	$65535, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 14, -24
	andl	$15, %eax
	leaq	.LC0(%rip), %r11
	movsbl	(%r11,%rax), %r11d
	movl	%r8d, %eax
	sarl	$5, %eax
	btl	%eax, %r11d
	jnc	.L428
	movzbl	2(%rcx), %eax
	cmpb	$-64, %al
	jge	.L457
	addl	%r10d, %r8d
	addq	$3, %rcx
	sall	$6, %r8d
	movq	%rcx, 16(%rdi)
	leal	-925824(%r8,%rax), %eax
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	2(%rcx), %rdx
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 14
	movb	%al, 65(%r9)
	movl	$65535, %eax
	movb	$1, 64(%r9)
	movl	$12, (%rsi)
	movq	%rdx, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 14, -24
	movb	%al, 65(%r9)
	addl	$2, %r8d
	movl	$11, (%rsi)
	cmpq	24(%rdi), %rdx
	jnb	.L435
	movl	%eax, %ecx
	movl	$1, %r11d
	andl	$7, %ecx
	sall	%cl, %r11d
	cmpw	$2, %r8w
	setbe	%bl
	cmpw	$3, %r8w
	movl	$1, %r8d
	je	.L436
	leaq	.LC1(%rip), %r12
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L463:
	testb	%bl, %bl
	jne	.L420
	movq	%rcx, %r10
	shrq	$4, %r10
	andl	$15, %r10d
	movzbl	(%r12,%r10), %r14d
	andl	%r11d, %r14d
	movl	%r14d, %r10d
.L421:
	testb	%r10b, %r10b
	je	.L422
	movb	%cl, 65(%r9,%rax)
	addl	$1, %r8d
	addq	$1, %rdx
	cmpq	%rdx, 24(%rdi)
	jbe	.L418
.L424:
	movzbl	(%rdx), %ecx
	movsbq	%r8b, %rax
	cmpb	$1, %r8b
	jle	.L463
.L420:
	cmpb	$-64, %cl
	setl	%r10b
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L462:
	cmpb	$-64, %r8b
	jge	.L428
	addq	$2, %rcx
	movzbl	%r8b, %eax
	movq	%rcx, 16(%rdi)
	leal	-12416(%r10,%rax), %eax
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L436:
	movl	$1, %r12d
	leaq	.LC0(%rip), %rbx
	andl	$15, %eax
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L464:
	movb	%r11b, 65(%r9,%r10)
	addl	$1, %r8d
	addq	$1, %rdx
	cmpq	%rdx, 24(%rdi)
	jbe	.L418
.L419:
	movzbl	(%rdx), %r11d
	movsbq	%r8b, %r10
	cmpb	$-64, %r11b
	setl	%cl
	cmpb	$1, %r8b
	jg	.L426
	movl	%r11d, %ecx
	movl	%r12d, %r14d
	shrb	$5, %cl
	sall	%cl, %r14d
	movl	%r14d, %ecx
	andb	(%rbx,%rax), %cl
.L426:
	testb	%cl, %cl
	jne	.L464
.L422:
	movl	$12, (%rsi)
.L418:
	movb	%r8b, 64(%r9)
	movl	$65535, %eax
	movq	%rdx, 16(%rdi)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L432:
	leal	(%r8,%r10), %eax
	addq	$4, %rcx
	sall	$6, %eax
	movq	%rcx, 16(%rdi)
	movl	%eax, %r8d
	movzbl	%dl, %eax
	addl	%r8d, %eax
	sall	$6, %eax
	leal	-63447168(%rax,%r11), %eax
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L437:
	xorl	%edx, %edx
	jmp	.L433
.L435:
	movl	$1, %r8d
	jmp	.L418
	.cfi_endproc
.LFE2119:
	.size	ucnv_getNextUChar_UTF8, .-ucnv_getNextUChar_UTF8
	.globl	_CESU8Data_67
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_CESU8Data_67, @object
	.size	_CESU8Data_67, 296
_CESU8Data_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL16_CESU8StaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL10_CESU8Impl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL16_CESU8StaticData, @object
	.size	_ZL16_CESU8StaticData, 100
_ZL16_CESU8StaticData:
	.long	100
	.string	"CESU-8"
	.zero	53
	.long	9400
	.byte	-1
	.byte	31
	.byte	1
	.byte	3
	.string	"\357\277\275"
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro,"aw"
	.align 32
	.type	_ZL10_CESU8Impl, @object
	.size	_ZL10_CESU8Impl, 144
_ZL10_CESU8Impl:
	.long	31
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ucnv_toUnicode_UTF8
	.quad	ucnv_toUnicode_UTF8_OFFSETS_LOGIC
	.quad	ucnv_fromUnicode_UTF8_67
	.quad	ucnv_fromUnicode_UTF8_OFFSETS_LOGIC_67
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ucnv_getCompleteUnicodeSet_67
	.quad	0
	.quad	0
	.globl	_UTF8Data_67
	.section	.data.rel.ro.local
	.align 32
	.type	_UTF8Data_67, @object
	.size	_UTF8Data_67, 296
_UTF8Data_67:
	.long	296
	.long	-1
	.quad	0
	.quad	_ZL15_UTF8StaticData
	.byte	0
	.byte	0
	.zero	6
	.quad	_ZL9_UTF8Impl
	.long	0
	.zero	4
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	128
	.quad	0
	.quad	0
	.long	0
	.byte	0
	.byte	0
	.byte	0
	.zero	1
	.value	0
	.zero	2
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.section	.rodata
	.align 32
	.type	_ZL15_UTF8StaticData, @object
	.size	_ZL15_UTF8StaticData, 100
_ZL15_UTF8StaticData:
	.long	100
	.string	"UTF-8"
	.zero	54
	.long	1208
	.byte	0
	.byte	4
	.byte	1
	.byte	3
	.string	"\357\277\275"
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.string	""
	.zero	18
	.section	.data.rel.ro
	.align 32
	.type	_ZL9_UTF8Impl, @object
	.size	_ZL9_UTF8Impl, 144
_ZL9_UTF8Impl:
	.long	4
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ucnv_toUnicode_UTF8
	.quad	ucnv_toUnicode_UTF8_OFFSETS_LOGIC
	.quad	ucnv_fromUnicode_UTF8_67
	.quad	ucnv_fromUnicode_UTF8_OFFSETS_LOGIC_67
	.quad	ucnv_getNextUChar_UTF8
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	ucnv_getNonSurrogateUnicodeSet_67
	.quad	ucnv_UTF8FromUTF8
	.quad	ucnv_UTF8FromUTF8
	.section	.rodata
	.align 16
	.type	_ZL15offsetsFromUTF8, @object
	.size	_ZL15offsetsFromUTF8, 20
_ZL15offsetsFromUTF8:
	.long	0
	.long	0
	.long	12416
	.long	925824
	.long	63447168
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
