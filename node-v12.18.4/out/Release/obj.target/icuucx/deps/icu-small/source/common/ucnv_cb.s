	.file	"ucnv_cb.cpp"
	.text
	.p2align 4
	.globl	ucnv_cbFromUWriteBytes_67
	.type	ucnv_cbFromUWriteBytes_67, @function
ucnv_cbFromUWriteBytes_67:
.LFB2113:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jle	.L8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %r10
	leaq	32(%rdi), %r11
	leaq	48(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r8
	pushq	%rcx
	movq	40(%rdi), %r8
	movq	%r11, %rcx
	movq	%r10, %rdi
	call	ucnv_fromUWriteBytes_67@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2113:
	.size	ucnv_cbFromUWriteBytes_67, .-ucnv_cbFromUWriteBytes_67
	.p2align 4
	.globl	ucnv_cbFromUWriteUChars_67
	.type	ucnv_cbFromUWriteUChars_67, @function
ucnv_cbFromUWriteUChars_67:
.LFB2114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L27
.L9:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	32(%rdi), %r10
	movq	%rdi, %rbx
	movq	%rsi, %r14
	movq	%rdx, %r15
	leaq	32(%rdi), %rsi
	movq	40(%rdi), %rdx
	movq	8(%rdi), %rdi
	pushq	%r8
	pushq	$0
	movl	%ecx, %r13d
	movq	%r8, %r12
	movq	%r14, %rcx
	xorl	%r9d, %r9d
	movq	%r15, %r8
	movq	%r10, -88(%rbp)
	call	ucnv_fromUnicode_67@PLT
	movq	48(%rbx), %rcx
	popq	%rsi
	popq	%rdi
	testq	%rcx, %rcx
	je	.L12
	movq	32(%rbx), %rdi
	movq	-88(%rbp), %r10
	cmpq	%rdi, %r10
	je	.L12
	leaq	-1(%rdi), %rax
	movq	%rdi, %r8
	subq	%r10, %rax
	subq	%r10, %r8
	cmpq	$2, %rax
	jbe	.L21
	movq	%r8, %rsi
	movd	%r13d, %xmm1
	xorl	%eax, %eax
	shrq	$2, %rsi
	pshufd	$0, %xmm1, %xmm0
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rax, %rdx
	addq	$1, %rax
	salq	$4, %rdx
	movups	%xmm0, (%rcx,%rdx)
	cmpq	%rax, %rsi
	jne	.L14
	movq	%r8, %rax
	andq	$-4, %rax
	addq	%rax, %r10
	leaq	(%rcx,%rax,4), %rdx
	cmpq	%rax, %r8
	je	.L15
.L13:
	leaq	1(%r10), %rax
	movl	%r13d, (%rdx)
	cmpq	%rax, %rdi
	je	.L15
	addq	$2, %r10
	movl	%r13d, 4(%rdx)
	cmpq	%r10, %rdi
	je	.L15
	movl	%r13d, 8(%rdx)
.L15:
	leaq	(%rcx,%r8,4), %rax
	movq	%rax, 48(%rbx)
.L12:
	cmpl	$15, (%r12)
	jne	.L9
	movq	8(%rbx), %rax
	movl	$0, -68(%rbp)
	movsbq	91(%rax), %rdx
	leaq	136(%rax), %r13
	leaq	104(%rax,%rdx), %rdx
	movq	%rdx, -64(%rbp)
	cmpq	%r13, %rdx
	jnb	.L19
	movb	$0, 91(%rax)
	leaq	-68(%rbp), %rax
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	pushq	%rax
	movq	%r13, %rdx
	leaq	-64(%rbp), %rsi
	xorl	%r9d, %r9d
	pushq	$0
	movq	%r15, %r8
	call	ucnv_fromUnicode_67@PLT
	movq	8(%rbx), %rdx
	movq	-64(%rbp), %rax
	leaq	104(%rdx), %rcx
	movq	%rax, %rbx
	subq	%rcx, %rbx
	movb	%bl, 91(%rdx)
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %r13
	jbe	.L19
	cmpl	$15, -68(%rbp)
	jne	.L9
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$5, (%r12)
	jmp	.L9
.L21:
	movq	%rcx, %rdx
	jmp	.L13
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2114:
	.size	ucnv_cbFromUWriteUChars_67, .-ucnv_cbFromUWriteUChars_67
	.p2align 4
	.globl	ucnv_cbFromUWriteSub_67
	.type	ucnv_cbFromUWriteSub_67, @function
ucnv_cbFromUWriteSub_67:
.LFB2115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L29
	movq	8(%rdi), %r10
	movsbq	89(%r10), %r11
	movq	%r11, %rax
	testl	%r11d, %r11d
	je	.L29
	js	.L39
	movq	48(%r10), %rax
	movq	32(%rax), %rax
	movq	104(%rax), %rax
	testq	%rax, %rax
	je	.L32
	call	*%rax
.L29:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	movq	-8(%rbp), %r12
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	cmpb	$0, 94(%r10)
	movq	40(%rdi), %r8
	leaq	48(%rdi), %r9
	leaq	32(%rdi), %rcx
	je	.L33
	cmpw	$255, 140(%r10)
	jbe	.L41
.L33:
	movq	40(%r10), %r12
	pushq	%rdx
	movq	%r10, %rdi
	movl	%r11d, %edx
	pushq	%rsi
	movq	%r12, %rsi
	call	ucnv_fromUWriteBytes_67@PLT
	popq	%rax
	popq	%rdx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L39:
	movq	40(%r10), %rcx
	addq	%rax, %rax
	leaq	-32(%rbp), %r10
	movq	%rdx, %r8
	movq	%rcx, -32(%rbp)
	subq	%rax, %rcx
	movq	%rcx, %r9
	movl	%esi, %ecx
	movq	%r10, %rsi
	movq	%r9, %rdx
	call	ucnv_cbFromUWriteUChars_67
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L41:
	pushq	%rdx
	leaq	94(%r10), %r11
	movl	$1, %edx
	movq	%r10, %rdi
	pushq	%rsi
	movq	%r11, %rsi
	call	ucnv_fromUWriteBytes_67@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L29
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2115:
	.size	ucnv_cbFromUWriteSub_67, .-ucnv_cbFromUWriteSub_67
	.p2align 4
	.globl	ucnv_cbToUWriteUChars_67
	.type	ucnv_cbToUWriteUChars_67, @function
ucnv_cbToUWriteUChars_67:
.LFB2116:
	.cfi_startproc
	endbr64
	movl	(%r8), %r9d
	testl	%r9d, %r9d
	jle	.L48
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %r10
	leaq	32(%rdi), %r11
	leaq	48(%rdi), %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r8
	pushq	%rcx
	movq	40(%rdi), %r8
	movq	%r11, %rcx
	movq	%r10, %rdi
	call	ucnv_toUWriteUChars_67@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2116:
	.size	ucnv_cbToUWriteUChars_67, .-ucnv_cbToUWriteUChars_67
	.p2align 4
	.globl	ucnv_cbToUWriteSub_67
	.type	ucnv_cbToUWriteSub_67, @function
ucnv_cbToUWriteSub_67:
.LFB2117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %r10
	movl	(%rdx), %eax
	cmpb	$1, 90(%r10)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	je	.L53
.L50:
	testl	%eax, %eax
	jle	.L54
.L49:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	pushq	%rdx
	leaq	32(%rdi), %rcx
	leaq	48(%rdi), %r9
	movl	$1, %edx
	pushq	%rsi
	movq	40(%rdi), %r8
	leaq	_ZZ21ucnv_cbToUWriteSub_67E15kSubstituteChar(%rip), %rsi
	movq	%r10, %rdi
	call	ucnv_toUWriteUChars_67@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	cmpb	$0, 94(%r10)
	je	.L50
	testl	%eax, %eax
	jg	.L49
	pushq	%rdx
	leaq	32(%rdi), %rcx
	leaq	48(%rdi), %r9
	movl	$1, %edx
	pushq	%rsi
	movq	40(%rdi), %r8
	leaq	_ZZ21ucnv_cbToUWriteSub_67E16kSubstituteChar1(%rip), %rsi
	movq	%r10, %rdi
	call	ucnv_toUWriteUChars_67@PLT
	popq	%rcx
	popq	%rsi
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2117:
	.size	ucnv_cbToUWriteSub_67, .-ucnv_cbToUWriteSub_67
	.section	.rodata
	.align 2
	.type	_ZZ21ucnv_cbToUWriteSub_67E15kSubstituteChar, @object
	.size	_ZZ21ucnv_cbToUWriteSub_67E15kSubstituteChar, 2
_ZZ21ucnv_cbToUWriteSub_67E15kSubstituteChar:
	.value	-3
	.align 2
	.type	_ZZ21ucnv_cbToUWriteSub_67E16kSubstituteChar1, @object
	.size	_ZZ21ucnv_cbToUWriteSub_67E16kSubstituteChar1, 2
_ZZ21ucnv_cbToUWriteSub_67E16kSubstituteChar1:
	.value	26
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
