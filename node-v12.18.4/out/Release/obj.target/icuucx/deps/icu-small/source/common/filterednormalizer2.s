	.file	"filterednormalizer2.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719FilteredNormalizer2D2Ev
	.type	_ZN6icu_6719FilteredNormalizer2D2Ev, @function
_ZN6icu_6719FilteredNormalizer2D2Ev:
.LFB2383:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_6711Normalizer2D2Ev@PLT
	.cfi_endproc
.LFE2383:
	.size	_ZN6icu_6719FilteredNormalizer2D2Ev, .-_ZN6icu_6719FilteredNormalizer2D2Ev
	.globl	_ZN6icu_6719FilteredNormalizer2D1Ev
	.set	_ZN6icu_6719FilteredNormalizer2D1Ev,_ZN6icu_6719FilteredNormalizer2D2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6719FilteredNormalizer2D0Ev
	.type	_ZN6icu_6719FilteredNormalizer2D0Ev, @function
_ZN6icu_6719FilteredNormalizer2D0Ev:
.LFB2385:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_6711Normalizer2D2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2385:
	.size	_ZN6icu_6719FilteredNormalizer2D0Ev, .-_ZN6icu_6719FilteredNormalizer2D0Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode:
.LFB2398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -56(%rbp)
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jg	.L5
	movq	%rdi, %r14
	movq	%rsi, %r15
	movl	%edx, %r13d
	movl	$2, %r12d
	testl	%edx, %edx
	jg	.L7
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L20:
	movq	8(%r14), %rdi
	movq	-56(%rbp), %r12
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	movq	%r12, %rcx
	call	*96(%rax)
	testb	%al, %al
	je	.L9
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L9
	movslq	%ebx, %rax
	subl	%ebx, %r13d
	xorl	%r12d, %r12d
	addq	%rax, %r15
	testl	%r13d, %r13d
	jle	.L11
.L7:
	movq	16(%r14), %rdi
	movl	%r12d, %ecx
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition@PLT
	movl	%eax, %ebx
	testl	%r12d, %r12d
	jne	.L20
	movslq	%ebx, %rax
	subl	%ebx, %r13d
	movl	$2, %r12d
	addq	%rax, %r15
	testl	%r13d, %r13d
	jg	.L7
.L11:
	movl	$1, %eax
.L5:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2398:
	.size	_ZNK6icu_6719FilteredNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode:
.LFB2399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -144(%rbp)
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L43
.L22:
	movl	$2, -132(%rbp)
.L21:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	movl	-132(%rbp), %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movzwl	8(%rsi), %eax
	movq	%rsi, %r13
	testb	$1, %al
	je	.L34
	movl	$1, (%rdx)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$1, -132(%rbp)
	movq	%rdi, %rbx
	xorl	%r12d, %r12d
	movl	$2, %r14d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L46:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%r12d, %edx
	jle	.L21
	movq	16(%rbx), %rdi
	testl	%r12d, %r12d
	js	.L35
.L47:
	cmpl	%edx, %r12d
	movl	%edx, %r15d
	cmovle	%r12d, %r15d
	movslq	%r15d, %rsi
	subl	%r15d, %edx
	leaq	(%rsi,%rsi), %r9
	testb	$17, %al
	jne	.L36
.L48:
	leaq	10(%r13), %rsi
	testb	$2, %al
	jne	.L29
	movq	24(%r13), %rsi
.L29:
	addq	%r9, %rsi
	movl	%r14d, %ecx
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	addl	%eax, %r15d
	testl	%r14d, %r14d
	jne	.L45
	movl	$2, %r14d
.L31:
	movzwl	8(%r13), %eax
	movl	%r15d, %r12d
.L23:
	testw	%ax, %ax
	jns	.L46
	movl	12(%r13), %edx
	cmpl	%r12d, %edx
	jle	.L21
	movq	16(%rbx), %rdi
	testl	%r12d, %r12d
	jns	.L47
.L35:
	xorl	%r9d, %r9d
	xorl	%r15d, %r15d
	testb	$17, %al
	je	.L48
.L36:
	xorl	%esi, %esi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L45:
	movq	8(%rbx), %r10
	leaq	-128(%rbp), %r14
	movl	%r15d, %ecx
	movl	%r12d, %edx
	subl	%r12d, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	(%r10), %rax
	movq	%r10, -160(%rbp)
	movq	104(%rax), %rax
	movq	%rax, -152(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-160(%rbp), %r10
	movq	%r14, %rsi
	movq	-144(%rbp), %rdx
	movq	-152(%rbp), %rax
	movq	%r10, %rdi
	call	*%rax
	movq	%r14, %rdi
	movl	%eax, %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-144(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L38
	testl	%r12d, %r12d
	je	.L39
	cmpl	$2, %r12d
	movl	$2, %eax
	cmovne	-132(%rbp), %eax
	xorl	%r14d, %r14d
	movl	%eax, -132(%rbp)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L38:
	movl	%r12d, -132(%rbp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$0, -132(%rbp)
	jmp	.L21
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2399:
	.size	_ZNK6icu_6719FilteredNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode:
.LFB2400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -136(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L70
.L50:
	xorl	%r12d, %r12d
.L49:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L71
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movq	%rdx, %rax
	movzwl	8(%rsi), %edx
	movq	%rsi, %r14
	testb	$1, %dl
	je	.L63
	movl	$1, (%rax)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L63:
	xorl	%ebx, %ebx
	movl	$2, %r15d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L73:
	movswl	%dx, %r12d
	sarl	$5, %r12d
	cmpl	%ebx, %r12d
	jle	.L49
	movq	-136(%rbp), %rax
	movq	16(%rax), %rdi
	testl	%ebx, %ebx
	js	.L64
.L74:
	cmpl	%r12d, %ebx
	movl	%r12d, %r13d
	cmovle	%ebx, %r13d
	movslq	%r13d, %rsi
	subl	%r13d, %r12d
	leaq	(%rsi,%rsi), %rax
	testb	$17, %dl
	jne	.L65
.L75:
	andl	$2, %edx
	leaq	10(%r14), %rsi
	jne	.L57
	movq	24(%r14), %rsi
.L57:
	addq	%rax, %rsi
	movl	%r15d, %ecx
	movl	%r12d, %edx
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	addl	%eax, %r13d
	testl	%r15d, %r15d
	jne	.L72
	movl	$2, %r15d
.L59:
	movzwl	8(%r14), %edx
	movl	%r13d, %ebx
.L51:
	testw	%dx, %dx
	jns	.L73
	movl	12(%r14), %r12d
	cmpl	%ebx, %r12d
	jle	.L49
	movq	-136(%rbp), %rax
	movq	16(%rax), %rdi
	testl	%ebx, %ebx
	jns	.L74
.L64:
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	testb	$17, %dl
	je	.L75
.L65:
	xorl	%esi, %esi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-136(%rbp), %rax
	leaq	-128(%rbp), %r15
	movl	%r13d, %ecx
	movl	%ebx, %edx
	subl	%ebx, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	8(%rax), %r12
	movq	(%r12), %rax
	movq	112(%rax), %rax
	movq	%rax, -152(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	-144(%rbp), %rdx
	movq	-152(%rbp), %rax
	call	*%rax
	movq	%r15, %rdi
	leal	(%rbx,%rax), %r12d
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-144(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L49
	cmpl	%r13d, %r12d
	jl	.L49
	xorl	%r15d, %r15d
	jmp	.L59
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2400:
	.size	_ZNK6icu_6719FilteredNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode:
.LFB2397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -160(%rbp)
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L97
.L77:
	movb	$0, -133(%rbp)
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	movzbl	-133(%rbp), %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movzwl	8(%rsi), %eax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movl	%eax, %edi
	andl	$1, %edi
	movb	%dil, -133(%rbp)
	je	.L91
	movl	$1, (%rdx)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L91:
	xorl	%r15d, %r15d
	movl	$2, %r14d
	testw	%ax, %ax
	js	.L79
	.p2align 4,,10
	.p2align 3
.L100:
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%r15d, %edx
	jle	.L96
.L82:
	movq	16(%r12), %rdi
	testl	%r15d, %r15d
	js	.L92
	cmpl	%edx, %r15d
	movl	%edx, %r13d
	cmovle	%r15d, %r13d
	movslq	%r13d, %rsi
	subl	%r13d, %edx
	leaq	(%rsi,%rsi), %r9
	testb	$17, %al
	jne	.L93
.L101:
	leaq	10(%rbx), %rsi
	testb	$2, %al
	jne	.L84
	movq	24(%rbx), %rsi
.L84:
	addq	%r9, %rsi
	movl	%r14d, %ecx
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	leal	0(%r13,%rax), %r8d
	testl	%r14d, %r14d
	jne	.L99
	movl	$2, %r14d
.L86:
	movzwl	8(%rbx), %eax
	movl	%r8d, %r15d
	testw	%ax, %ax
	jns	.L100
.L79:
	movl	12(%rbx), %edx
	cmpl	%r15d, %edx
	jg	.L82
.L96:
	movb	$1, -133(%rbp)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	testb	$17, %al
	je	.L101
.L93:
	xorl	%esi, %esi
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L99:
	movq	8(%r12), %r10
	movl	%r8d, %ecx
	leaq	-128(%rbp), %r14
	movl	%r15d, %edx
	subl	%r15d, %ecx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%r8d, -132(%rbp)
	movq	(%r10), %rax
	movq	%r10, -152(%rbp)
	movq	88(%rax), %rax
	movq	%rax, -144(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %rdx
	movq	%r14, %rsi
	movq	-144(%rbp), %rax
	movq	%r10, %rdi
	call	*%rax
	movl	-132(%rbp), %r8d
	movq	%r14, %rdi
	testb	%al, %al
	je	.L88
	movq	-160(%rbp), %rax
	movl	%r8d, -132(%rbp)
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L102
.L88:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L102:
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	xorl	%r14d, %r14d
	movl	-132(%rbp), %r8d
	jmp	.L86
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2397:
	.size	_ZNK6icu_6719FilteredNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_17USetSpanConditionR10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_17USetSpanConditionR10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_17USetSpanConditionR10UErrorCode:
.LFB2387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	movl	$2, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$200, %rsp
	movq	%rdx, -208(%rbp)
	movq	%r8, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%cx, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -232(%rbp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L134:
	movl	$2, %r14d
	testl	%ecx, %ecx
	jne	.L133
.L113:
	movl	%r15d, %r13d
.L104:
	movzwl	8(%rbx), %eax
	testw	%ax, %ax
	js	.L105
	movswl	%ax, %edx
	sarl	$5, %edx
	cmpl	%edx, %r13d
	jge	.L108
.L107:
	movq	16(%r12), %rdi
	testl	%r13d, %r13d
	js	.L126
	cmpl	%edx, %r13d
	movl	%edx, %r15d
	cmovle	%r13d, %r15d
	movslq	%r15d, %rsi
	subl	%r15d, %edx
	leaq	(%rsi,%rsi), %rcx
	testb	$17, %al
	jne	.L127
.L136:
	leaq	10(%rbx), %rsi
	testb	$2, %al
	jne	.L110
	movq	24(%rbx), %rsi
.L110:
	addq	%rcx, %rsi
	movl	%r14d, %ecx
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	addl	%eax, %r15d
	movl	%r15d, %ecx
	subl	%r13d, %ecx
	testl	%r14d, %r14d
	je	.L134
	testl	%ecx, %ecx
	jne	.L114
.L121:
	xorl	%r14d, %r14d
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L105:
	movl	12(%rbx), %edx
	cmpl	%edx, %r13d
	jl	.L107
.L108:
	movq	-232(%rbp), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	movq	-208(%rbp), %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	testb	$17, %al
	je	.L136
.L127:
	xorl	%esi, %esi
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L133:
	movq	-208(%rbp), %rdi
	movl	%r13d, %edx
	movq	%rbx, %rsi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L114:
	movq	8(%r12), %r10
	leaq	-128(%rbp), %r14
	movl	%r13d, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	(%r10), %rax
	movq	%r10, -224(%rbp)
	movq	24(%rax), %rax
	movq	%rax, -216(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movq	-216(%rbp), %rax
	leaq	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode(%rip), %rdi
	movq	-224(%rbp), %r10
	cmpq	%rdi, %rax
	jne	.L115
	movq	-200(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L137
.L116:
	movq	-232(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	movq	%r13, %rsi
.L118:
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	js	.L119
	sarl	$5, %ecx
.L120:
	movq	-208(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-200(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jle	.L121
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L119:
	movl	12(%rsi), %ecx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L137:
	testb	$1, -120(%rbp)
	je	.L117
	movl	$1, (%rax)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L117:
	movzwl	-184(%rbp), %edx
	movq	%rax, %r8
	movq	%r14, %rsi
	movq	%r10, %rdi
	movl	$2, %ecx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movq	-232(%rbp), %rdx
	movw	%ax, -184(%rbp)
	call	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_17USetSpanConditionR10UErrorCode
	movq	%rax, %rsi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r14, %rsi
	movq	-200(%rbp), %rcx
	movq	-232(%rbp), %rdx
	movq	%r10, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L118
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2387:
	.size	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_17USetSpanConditionR10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_17USetSpanConditionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode:
.LFB2386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	(%rcx), %eax
	testl	%eax, %eax
	jle	.L148
.L139:
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString10setToBogusEv@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	%rcx, %r8
	testb	$1, 8(%rsi)
	je	.L140
	movl	$1, (%rcx)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L140:
	cmpq	%rdx, %rsi
	je	.L149
	movzwl	8(%r12), %edx
	movl	$2, %ecx
	movl	%edx, %eax
	andl	$31, %eax
	andl	$1, %edx
	movl	$2, %edx
	cmovne	%edx, %eax
	movq	%r12, %rdx
	movw	%ax, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_17USetSpanConditionR10UErrorCode
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movl	$1, (%r8)
	addq	$8, %rsp
	movq	%rsi, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2386:
	.size	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjPKciRNS_8ByteSinkEPNS_5EditsE17USetSpanConditionR10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjPKciRNS_8ByteSinkEPNS_5EditsE17USetSpanConditionR10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjPKciRNS_8ByteSinkEPNS_5EditsE17USetSpanConditionR10UErrorCode:
.LFB2389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -60(%rbp)
	movl	16(%rbp), %r13d
	movq	%r8, -56(%rbp)
	testl	%ecx, %ecx
	jle	.L150
	andl	$16384, %esi
	movq	%rdi, %r12
	movq	%rdx, %r15
	movl	%ecx, %ebx
	je	.L152
	testq	%r9, %r9
	je	.L191
	movl	%r13d, %r14d
	movq	%r9, -72(%rbp)
	movl	%ecx, %r13d
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L192:
	testl	%eax, %eax
	je	.L159
	movq	8(%r12), %rdi
	subq	$8, %rsp
	movq	-72(%rbp), %r9
	movl	%ebx, %ecx
	movq	-56(%rbp), %r8
	movl	-60(%rbp), %esi
	movq	%r15, %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	pushq	24(%rbp)
	call	*%rax
	movq	24(%rbp), %rax
	popq	%r9
	popq	%r10
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jg	.L150
.L159:
	movslq	%ebx, %rax
	subl	%ebx, %r13d
	xorl	%r14d, %r14d
	addq	%rax, %r15
	testl	%r13d, %r13d
	jle	.L150
.L153:
	movq	16(%r12), %rdi
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition@PLT
	movl	%eax, %ebx
	testl	%r14d, %r14d
	jne	.L192
	testl	%eax, %eax
	je	.L161
	movq	-72(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
.L161:
	movslq	%ebx, %rax
	subl	%ebx, %r13d
	movl	$2, %r14d
	addq	%rax, %r15
	testl	%r13d, %r13d
	jg	.L153
.L150:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	testq	%r9, %r9
	je	.L193
	movq	%r9, -72(%rbp)
	movl	%ecx, %r14d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$2, %r13d
	testl	%eax, %eax
	je	.L168
	movq	-72(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN6icu_675Edits12addUnchangedEi@PLT
	movq	-56(%rbp), %rdi
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
.L168:
	movslq	%ebx, %rax
	subl	%ebx, %r14d
	addq	%rax, %r15
	testl	%r14d, %r14d
	jle	.L150
.L162:
	movq	16(%r12), %rdi
	movl	%r13d, %ecx
	movl	%r14d, %edx
	movq	%r15, %rsi
	call	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition@PLT
	movl	%eax, %ebx
	testl	%r13d, %r13d
	je	.L194
	testl	%eax, %eax
	je	.L170
	movq	8(%r12), %rdi
	subq	$8, %rsp
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %r8
	movq	(%rdi), %rax
	movl	-60(%rbp), %esi
	movq	32(%rax), %rax
	pushq	24(%rbp)
	call	*%rax
	popq	%rax
	movq	24(%rbp), %rax
	popq	%rdx
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L150
.L170:
	xorl	%r13d, %r13d
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L191:
	movl	16(%rbp), %r12d
	movq	%rdi, %r13
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L195:
	testl	%eax, %eax
	je	.L156
	movq	8(%r13), %rdi
	subq	$8, %rsp
	movq	-56(%rbp), %r8
	movl	%r14d, %ecx
	movl	-60(%rbp), %esi
	xorl	%r9d, %r9d
	movq	%r15, %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	pushq	24(%rbp)
	call	*%rax
	popq	%r12
	popq	%rax
	movq	24(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L150
.L156:
	xorl	%r12d, %r12d
.L154:
	movslq	%r14d, %rax
	subl	%r14d, %ebx
	addq	%rax, %r15
	testl	%ebx, %ebx
	jle	.L150
.L157:
	movq	16(%r13), %rdi
	movl	%r12d, %ecx
	movl	%ebx, %edx
	movq	%r15, %rsi
	call	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition@PLT
	movl	%eax, %r14d
	testl	%r12d, %r12d
	jne	.L195
	movl	$2, %r12d
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L193:
	movl	%r13d, 16(%rbp)
	movl	16(%rbp), %ebx
	movl	%ecx, %r13d
	.p2align 4,,10
	.p2align 3
.L166:
	movq	16(%r12), %rdi
	movl	%ebx, %ecx
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition@PLT
	movl	%eax, %r14d
	testl	%ebx, %ebx
	je	.L163
	testl	%eax, %eax
	je	.L164
	movq	8(%r12), %rdi
	subq	$8, %rsp
	movq	-56(%rbp), %r8
	movl	%r14d, %ecx
	movl	-60(%rbp), %esi
	xorl	%r9d, %r9d
	movq	%r15, %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	pushq	24(%rbp)
	call	*%rax
	movq	24(%rbp), %rax
	popq	%rsi
	popq	%rdi
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jg	.L150
.L164:
	xorl	%ebx, %ebx
.L165:
	movslq	%r14d, %rax
	subl	%r14d, %r13d
	addq	%rax, %r15
	testl	%r13d, %r13d
	jg	.L166
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L163:
	movl	$2, %ebx
	testl	%eax, %eax
	je	.L165
	movq	-56(%rbp), %rdi
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L165
	.cfi_endproc
.LFE2389:
	.size	_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjPKciRNS_8ByteSinkEPNS_5EditsE17USetSpanConditionR10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjPKciRNS_8ByteSinkEPNS_5EditsE17USetSpanConditionR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode:
.LFB2388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rbp), %r13
	movl	0(%r13), %r10d
	testl	%r10d, %r10d
	jg	.L196
	movq	%rdi, %r12
	testq	%r9, %r9
	je	.L198
	testl	$8192, %esi
	je	.L203
.L198:
	pushq	%r13
	orl	$8192, %esi
	movq	%r12, %rdi
	pushq	$2
	call	_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjPKciRNS_8ByteSinkEPNS_5EditsE17USetSpanConditionR10UErrorCode
	popq	%rax
	popq	%rdx
.L196:
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	%r9, %rdi
	movq	%rcx, -56(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%r8, -40(%rbp)
	movl	%esi, -28(%rbp)
	movq	%r9, -24(%rbp)
	call	_ZN6icu_675Edits5resetEv@PLT
	movq	-56(%rbp), %rcx
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %r8
	movl	-28(%rbp), %esi
	movq	-24(%rbp), %r9
	jmp	.L198
	.cfi_endproc
.LFE2388:
	.size	_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode:
.LFB2392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L251
.L207:
	movq	%r12, %rax
.L204:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L252
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movzwl	8(%rsi), %eax
	movq	%r8, %r14
	testb	$1, %al
	je	.L206
.L240:
	movl	$1, (%r14)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L206:
	movl	%ecx, %r10d
	movzwl	8(%rdx), %ecx
	movq	%rdx, %r13
	testb	$1, %cl
	jne	.L240
	cmpq	%r13, %r12
	je	.L253
	movswl	%ax, %edx
	movq	%rdi, %r15
	shrl	$5, %edx
	jne	.L209
	testb	%r10b, %r10b
	je	.L210
	movq	(%rdi), %rdx
	movq	24(%rdx), %r8
	leaq	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode(%rip), %rdx
	cmpq	%rdx, %r8
	jne	.L211
	andl	$31, %eax
	movq	%r14, %r8
	movl	$2, %ecx
	movq	%r12, %rdx
	movw	%ax, 8(%r12)
	movq	%r13, %rsi
	call	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_17USetSpanConditionR10UErrorCode
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L209:
	movq	16(%rdi), %rdi
	testw	%cx, %cx
	js	.L212
	movswl	%cx, %edx
	sarl	$5, %edx
.L213:
	testl	%edx, %edx
	movl	$0, %ebx
	cmovle	%edx, %ebx
	subl	%ebx, %edx
	testb	$17, %cl
	jne	.L243
	andl	$2, %ecx
	leaq	10(%r13), %rax
	jne	.L214
	movq	24(%r13), %rax
.L214:
	movslq	%ebx, %rcx
	movl	%r10d, -200(%rbp)
	leaq	(%rax,%rcx,2), %rsi
	movl	$2, %ecx
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	movl	-200(%rbp), %r10d
	addl	%eax, %ebx
	jne	.L254
.L216:
	movswl	8(%r13), %eax
	testw	%ax, %ax
	js	.L232
	sarl	$5, %eax
.L233:
	cmpl	%ebx, %eax
	jle	.L207
	leaq	-128(%rbp), %r11
	movl	$2147483647, %ecx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movl	%r10d, -208(%rbp)
	movq	%r11, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movl	-208(%rbp), %r10d
	movq	-200(%rbp), %r11
	testb	%r10b, %r10b
	jne	.L255
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L236
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L237:
	movq	%r11, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r11, -200(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movq	-200(%rbp), %r11
.L235:
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L253:
	movl	$1, (%r14)
	movq	%r12, %rax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L212:
	movl	12(%r13), %edx
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L232:
	movl	12(%r13), %eax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	-192(%rbp), %rax
	xorl	%edx, %edx
	movl	%ebx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movl	%r10d, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZNK6icu_6713UnicodeString13tempSubStringEii@PLT
	movzwl	8(%r12), %eax
	movq	16(%r15), %rdi
	movl	-208(%rbp), %r10d
	testw	%ax, %ax
	js	.L217
	movswl	%ax, %edx
	sarl	$5, %edx
.L218:
	testb	$17, %al
	jne	.L244
	leaq	10(%r12), %rsi
	testb	$2, %al
	jne	.L219
	movq	24(%r12), %rsi
.L219:
	movl	$2, %ecx
	movl	%r10d, -208(%rbp)
	call	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition@PLT
	movl	-208(%rbp), %r10d
	testl	%eax, %eax
	movl	%eax, -212(%rbp)
	jne	.L221
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	testb	%r10b, %r10b
	je	.L222
	movq	40(%rax), %rax
	leaq	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L223
	movq	-200(%rbp), %rdx
	movq	%r14, %r8
	movl	$1, %ecx
	movq	%r12, %rsi
	call	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode
	movl	-208(%rbp), %r10d
.L224:
	movq	-200(%rbp), %rdi
	movl	%r10d, -208(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-208(%rbp), %r10d
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	*%r8
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L217:
	movl	12(%r12), %edx
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%r11, %rsi
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_17USetSpanConditionR10UErrorCode
	movq	-200(%rbp), %r11
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L243:
	xorl	%eax, %eax
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L222:
	movq	48(%rax), %rax
	leaq	_ZNK6icu_6719FilteredNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode(%rip), %rdx
	movl	%r10d, -208(%rbp)
	cmpq	%rdx, %rax
	jne	.L225
	movq	-200(%rbp), %rdx
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	call	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode
	movl	-208(%rbp), %r10d
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	-128(%rbp), %r11
	movl	%eax, %edx
	movl	$2147483647, %ecx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movl	%r10d, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN6icu_6713UnicodeStringC1ERKS0_ii@PLT
	movl	-216(%rbp), %r10d
	movq	8(%r15), %rdi
	movq	-208(%rbp), %r11
	testb	%r10b, %r10b
	movq	(%rdi), %rax
	je	.L226
	movq	40(%rax), %rax
	leaq	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L227
	movq	-200(%rbp), %rdx
	movq	%r11, %rsi
	movq	%r14, %r8
	movl	$1, %ecx
	call	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode
	movq	-208(%rbp), %r11
	movl	-216(%rbp), %r10d
.L228:
	movzwl	-120(%rbp), %eax
	testw	%ax, %ax
	js	.L230
	movswl	%ax, %r9d
	sarl	$5, %r9d
.L231:
	movl	-212(%rbp), %esi
	movq	%r11, %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movl	$2147483647, %edx
	movl	%r10d, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN6icu_6713UnicodeString9doReplaceEiiRKS0_ii@PLT
	movq	-208(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-216(%rbp), %r10d
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L236:
	movl	-116(%rbp), %ecx
	jmp	.L237
.L244:
	xorl	%esi, %esi
	jmp	.L219
.L226:
	movq	48(%rax), %rax
	leaq	_ZNK6icu_6719FilteredNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode(%rip), %rdx
	movl	%r10d, -216(%rbp)
	cmpq	%rdx, %rax
	jne	.L229
	movq	-200(%rbp), %rdx
	movq	%r11, %rsi
	movq	%r14, %r8
	xorl	%ecx, %ecx
	movq	%r11, -208(%rbp)
	call	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode
	movq	-208(%rbp), %r11
	movl	-216(%rbp), %r10d
	jmp	.L228
.L223:
	movl	%r10d, -208(%rbp)
.L225:
	movq	-200(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rsi
	call	*%rax
	movl	-208(%rbp), %r10d
	jmp	.L224
.L230:
	movl	-116(%rbp), %r9d
	jmp	.L231
.L227:
	movl	%r10d, -216(%rbp)
	movq	%r11, %rsi
	movq	%r14, %rcx
	movq	-200(%rbp), %rdx
	movq	%r11, -208(%rbp)
	call	*%rax
	movq	-208(%rbp), %r11
	movl	-216(%rbp), %r10d
	jmp	.L228
.L229:
	movq	%r11, -208(%rbp)
	movq	%r11, %rsi
	movq	-200(%rbp), %rdx
	movq	%r14, %rcx
	call	*%rax
	movl	-216(%rbp), %r10d
	movq	-208(%rbp), %r11
	jmp	.L228
.L252:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2392:
	.size	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode:
.LFB2390:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	movl	$1, %ecx
	jmp	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode
	.cfi_endproc
.LFE2390:
	.size	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.type	_ZNK6icu_6719FilteredNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode, @function
_ZNK6icu_6719FilteredNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode:
.LFB2391:
	.cfi_startproc
	endbr64
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	jmp	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_aR10UErrorCode
	.cfi_endproc
.LFE2391:
	.size	_ZNK6icu_6719FilteredNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode, .-_ZNK6icu_6719FilteredNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.p2align 4
	.globl	unorm2_openFiltered_67
	.type	unorm2_openFiltered_67, @function
unorm2_openFiltered_67:
.LFB2404:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L264
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L267
	movq	%rdi, %r13
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L261
	leaq	16+_ZTVN6icu_6719FilteredNormalizer2E(%rip), %rcx
	movq	%r13, 8(%rax)
	movq	%rcx, (%rax)
	movq	%rbx, 16(%rax)
.L258:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$1, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L261:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L258
	.cfi_endproc
.LFE2404:
	.size	unorm2_openFiltered_67, .-unorm2_openFiltered_67
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer216getDecompositionEiRNS_13UnicodeStringE
	.type	_ZNK6icu_6719FilteredNormalizer216getDecompositionEiRNS_13UnicodeStringE, @function
_ZNK6icu_6719FilteredNormalizer216getDecompositionEiRNS_13UnicodeStringE:
.LFB2393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L268
	movq	8(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*56(%rax)
	testb	%al, %al
	setne	%al
.L268:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2393:
	.size	_ZNK6icu_6719FilteredNormalizer216getDecompositionEiRNS_13UnicodeStringE, .-_ZNK6icu_6719FilteredNormalizer216getDecompositionEiRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer219getRawDecompositionEiRNS_13UnicodeStringE
	.type	_ZNK6icu_6719FilteredNormalizer219getRawDecompositionEiRNS_13UnicodeStringE, @function
_ZNK6icu_6719FilteredNormalizer219getRawDecompositionEiRNS_13UnicodeStringE:
.LFB2394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L274
	movq	8(%rbx), %rdi
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*64(%rax)
	testb	%al, %al
	setne	%al
.L274:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2394:
	.size	_ZNK6icu_6719FilteredNormalizer219getRawDecompositionEiRNS_13UnicodeStringE, .-_ZNK6icu_6719FilteredNormalizer219getRawDecompositionEiRNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer211composePairEii
	.type	_ZNK6icu_6719FilteredNormalizer211composePairEii, @function
_ZNK6icu_6719FilteredNormalizer211composePairEii:
.LFB2395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L282
	movq	16(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L282
	movq	8(%rbx), %rdi
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2395:
	.size	_ZNK6icu_6719FilteredNormalizer211composePairEii, .-_ZNK6icu_6719FilteredNormalizer211composePairEii
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer217getCombiningClassEi
	.type	_ZNK6icu_6719FilteredNormalizer217getCombiningClassEi, @function
_ZNK6icu_6719FilteredNormalizer217getCombiningClassEi:
.LFB2396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L289
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2396:
	.size	_ZNK6icu_6719FilteredNormalizer217getCombiningClassEi, .-_ZNK6icu_6719FilteredNormalizer217getCombiningClassEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer217hasBoundaryBeforeEi
	.type	_ZNK6icu_6719FilteredNormalizer217hasBoundaryBeforeEi, @function
_ZNK6icu_6719FilteredNormalizer217hasBoundaryBeforeEi:
.LFB2401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	je	.L291
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*120(%rax)
	testb	%al, %al
	setne	%al
.L291:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2401:
	.size	_ZNK6icu_6719FilteredNormalizer217hasBoundaryBeforeEi, .-_ZNK6icu_6719FilteredNormalizer217hasBoundaryBeforeEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer216hasBoundaryAfterEi
	.type	_ZNK6icu_6719FilteredNormalizer216hasBoundaryAfterEi, @function
_ZNK6icu_6719FilteredNormalizer216hasBoundaryAfterEi:
.LFB2402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	je	.L297
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*128(%rax)
	testb	%al, %al
	setne	%al
.L297:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2402:
	.size	_ZNK6icu_6719FilteredNormalizer216hasBoundaryAfterEi, .-_ZNK6icu_6719FilteredNormalizer216hasBoundaryAfterEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6719FilteredNormalizer27isInertEi
	.type	_ZNK6icu_6719FilteredNormalizer27isInertEi, @function
_ZNK6icu_6719FilteredNormalizer27isInertEi:
.LFB2403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testb	%r8b, %r8b
	je	.L303
	movq	8(%rbx), %rdi
	movl	%r12d, %esi
	movq	(%rdi), %rax
	call	*136(%rax)
	testb	%al, %al
	setne	%al
.L303:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2403:
	.size	_ZNK6icu_6719FilteredNormalizer27isInertEi, .-_ZNK6icu_6719FilteredNormalizer27isInertEi
	.weak	_ZTSN6icu_6719FilteredNormalizer2E
	.section	.rodata._ZTSN6icu_6719FilteredNormalizer2E,"aG",@progbits,_ZTSN6icu_6719FilteredNormalizer2E,comdat
	.align 16
	.type	_ZTSN6icu_6719FilteredNormalizer2E, @object
	.size	_ZTSN6icu_6719FilteredNormalizer2E, 31
_ZTSN6icu_6719FilteredNormalizer2E:
	.string	"N6icu_6719FilteredNormalizer2E"
	.weak	_ZTIN6icu_6719FilteredNormalizer2E
	.section	.data.rel.ro._ZTIN6icu_6719FilteredNormalizer2E,"awG",@progbits,_ZTIN6icu_6719FilteredNormalizer2E,comdat
	.align 8
	.type	_ZTIN6icu_6719FilteredNormalizer2E, @object
	.size	_ZTIN6icu_6719FilteredNormalizer2E, 24
_ZTIN6icu_6719FilteredNormalizer2E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6719FilteredNormalizer2E
	.quad	_ZTIN6icu_6711Normalizer2E
	.weak	_ZTVN6icu_6719FilteredNormalizer2E
	.section	.data.rel.ro._ZTVN6icu_6719FilteredNormalizer2E,"awG",@progbits,_ZTVN6icu_6719FilteredNormalizer2E,comdat
	.align 8
	.type	_ZTVN6icu_6719FilteredNormalizer2E, @object
	.size	_ZTVN6icu_6719FilteredNormalizer2E, 160
_ZTVN6icu_6719FilteredNormalizer2E:
	.quad	0
	.quad	_ZTIN6icu_6719FilteredNormalizer2E
	.quad	_ZN6icu_6719FilteredNormalizer2D1Ev
	.quad	_ZN6icu_6719FilteredNormalizer2D0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6719FilteredNormalizer29normalizeERKNS_13UnicodeStringERS1_R10UErrorCode
	.quad	_ZNK6icu_6719FilteredNormalizer213normalizeUTF8EjNS_11StringPieceERNS_8ByteSinkEPNS_5EditsER10UErrorCode
	.quad	_ZNK6icu_6719FilteredNormalizer224normalizeSecondAndAppendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6719FilteredNormalizer26appendERNS_13UnicodeStringERKS1_R10UErrorCode
	.quad	_ZNK6icu_6719FilteredNormalizer216getDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6719FilteredNormalizer219getRawDecompositionEiRNS_13UnicodeStringE
	.quad	_ZNK6icu_6719FilteredNormalizer211composePairEii
	.quad	_ZNK6icu_6719FilteredNormalizer217getCombiningClassEi
	.quad	_ZNK6icu_6719FilteredNormalizer212isNormalizedERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6719FilteredNormalizer216isNormalizedUTF8ENS_11StringPieceER10UErrorCode
	.quad	_ZNK6icu_6719FilteredNormalizer210quickCheckERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6719FilteredNormalizer217spanQuickCheckYesERKNS_13UnicodeStringER10UErrorCode
	.quad	_ZNK6icu_6719FilteredNormalizer217hasBoundaryBeforeEi
	.quad	_ZNK6icu_6719FilteredNormalizer216hasBoundaryAfterEi
	.quad	_ZNK6icu_6719FilteredNormalizer27isInertEi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
