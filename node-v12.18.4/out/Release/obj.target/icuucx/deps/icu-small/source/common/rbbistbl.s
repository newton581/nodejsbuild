	.file	"rbbistbl.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715RBBISymbolTable13lookupMatcherEi
	.type	_ZNK6icu_6715RBBISymbolTable13lookupMatcherEi, @function
_ZNK6icu_6715RBBISymbolTable13lookupMatcherEi:
.LFB3185:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$65535, %esi
	jne	.L1
	movq	96(%rdi), %rax
	movq	$0, 96(%rdi)
.L1:
	ret
	.cfi_endproc
.LFE3185:
	.size	_ZNK6icu_6715RBBISymbolTable13lookupMatcherEi, .-_ZNK6icu_6715RBBISymbolTable13lookupMatcherEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBISymbolTableD2Ev
	.type	_ZN6icu_6715RBBISymbolTableD2Ev, @function
_ZN6icu_6715RBBISymbolTableD2Ev:
.LFB3181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715RBBISymbolTableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	call	uhash_close_67@PLT
	leaq	32(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6711SymbolTableD2Ev@PLT
	.cfi_endproc
.LFE3181:
	.size	_ZN6icu_6715RBBISymbolTableD2Ev, .-_ZN6icu_6715RBBISymbolTableD2Ev
	.globl	_ZN6icu_6715RBBISymbolTableD1Ev
	.set	_ZN6icu_6715RBBISymbolTableD1Ev,_ZN6icu_6715RBBISymbolTableD2Ev
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715RBBISymbolTable6lookupERKNS_13UnicodeStringE
	.type	_ZNK6icu_6715RBBISymbolTable6lookupERKNS_13UnicodeStringE, @function
_ZNK6icu_6715RBBISymbolTable6lookupERKNS_13UnicodeStringE:
.LFB3184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L7
	movq	72(%rax), %rax
	movq	16(%rax), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L9
	movq	16(%rax), %rax
	movq	32(%rax), %rax
	movq	%rax, 96(%rbx)
	leaq	32(%rbx), %rax
.L7:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	addq	$48, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3184:
	.size	_ZNK6icu_6715RBBISymbolTable6lookupERKNS_13UnicodeStringE, .-_ZNK6icu_6715RBBISymbolTable6lookupERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715RBBISymbolTable10lookupNodeERKNS_13UnicodeStringE
	.type	_ZNK6icu_6715RBBISymbolTable10lookupNodeERKNS_13UnicodeStringE, @function
_ZNK6icu_6715RBBISymbolTable10lookupNodeERKNS_13UnicodeStringE:
.LFB3187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L14
	movq	72(%rax), %rax
.L14:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3187:
	.size	_ZNK6icu_6715RBBISymbolTable10lookupNodeERKNS_13UnicodeStringE, .-_ZNK6icu_6715RBBISymbolTable10lookupNodeERKNS_13UnicodeStringE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715RBBISymbolTable14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi
	.type	_ZNK6icu_6715RBBISymbolTable14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi, @function
_ZNK6icu_6715RBBISymbolTable14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi:
.LFB3186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	8(%rcx), %rsi
	movq	%rax, (%rdi)
	movl	$2, %eax
	movq	%rdi, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movw	%ax, 8(%rdi)
	cmpl	%esi, %r8d
	jle	.L20
	movq	%rdx, %r12
	movl	%r8d, %r14d
	movq	%rsi, %r13
	movl	%esi, %ebx
	leaq	(%rsi,%rsi), %r15
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L43:
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L24:
	movl	$65535, %edi
	cmpl	%ebx, %ecx
	jbe	.L25
	testb	$2, %al
	je	.L26
	leaq	10(%r12), %rax
.L27:
	movzwl	(%rax,%r15), %edi
.L25:
	cmpl	%ebx, %r13d
	je	.L28
.L32:
	call	u_isIDPart_67@PLT
	testb	%al, %al
	je	.L42
	addl	$1, %ebx
	addq	$2, %r15
	cmpl	%ebx, %r14d
	je	.L33
.L34:
	movzwl	8(%r12), %eax
	testw	%ax, %ax
	jns	.L43
	movl	12(%r12), %ecx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movq	24(%r12), %rax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	movl	%edi, -52(%rbp)
	call	u_isIDStart_67@PLT
	movl	-52(%rbp), %edi
	testb	%al, %al
	jne	.L32
.L20:
	movq	-64(%rbp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	cmpl	%ebx, %r13d
	je	.L20
	movl	%ebx, %r14d
.L33:
	movq	-72(%rbp), %rax
	movq	-64(%rbp), %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%r14d, 8(%rax)
	movq	(%r12), %rax
	call	*24(%rax)
	jmp	.L20
	.cfi_endproc
.LFE3186:
	.size	_ZNK6icu_6715RBBISymbolTable14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi, .-_ZNK6icu_6715RBBISymbolTable14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBISymbolTable8addEntryERKNS_13UnicodeStringEPNS_8RBBINodeER10UErrorCode
	.type	_ZN6icu_6715RBBISymbolTable8addEntryERKNS_13UnicodeStringEPNS_8RBBINodeER10UErrorCode, @function
_ZN6icu_6715RBBISymbolTable8addEntryERKNS_13UnicodeStringEPNS_8RBBINodeER10UErrorCode:
.LFB3188:
	.cfi_startproc
	endbr64
	movl	(%rcx), %r8d
	testl	%r8d, %r8d
	jle	.L52
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	call	uhash_get_67@PLT
	testq	%rax, %rax
	je	.L46
	movl	$66054, (%r12)
.L44:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movl	$80, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L47
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	$0, 72(%r15)
	leaq	8(%r15), %r8
	movq	%r13, %rsi
	movq	%rax, 8(%r15)
	movl	$2, %eax
	movq	%r8, %rdi
	movw	%ax, 16(%r15)
	movq	%r8, -56(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	%r14, 72(%r15)
	movq	-56(%rbp), %r8
	movq	%r12, %rcx
	movq	16(%rbx), %rdi
	addq	$24, %rsp
	movq	%r15, %rdx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r8, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uhash_put_67@PLT
.L47:
	.cfi_restore_state
	movl	$7, (%r12)
	jmp	.L44
	.cfi_endproc
.LFE3188:
	.size	_ZN6icu_6715RBBISymbolTable8addEntryERKNS_13UnicodeStringEPNS_8RBBINodeER10UErrorCode, .-_ZN6icu_6715RBBISymbolTable8addEntryERKNS_13UnicodeStringEPNS_8RBBINodeER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBISymbolTableD0Ev
	.type	_ZN6icu_6715RBBISymbolTableD0Ev, @function
_ZN6icu_6715RBBISymbolTableD0Ev:
.LFB3183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715RBBISymbolTableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	call	uhash_close_67@PLT
	leaq	32(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_6711SymbolTableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3183:
	.size	_ZN6icu_6715RBBISymbolTableD0Ev, .-_ZN6icu_6715RBBISymbolTableD0Ev
	.p2align 4
	.type	RBBISymbolTableEntry_deleter, @function
RBBISymbolTableEntry_deleter:
.LFB3173:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L55
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	72(%rdi), %r13
	movq	16(%r13), %r14
	testq	%r14, %r14
	je	.L57
	movq	%r14, %rdi
	call	_ZN6icu_678RBBINodeD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	72(%r12), %r13
.L57:
	movq	$0, 16(%r13)
	movq	%r13, %rdi
	call	_ZN6icu_678RBBINodeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	leaq	8(%r12), %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	ret
	.cfi_endproc
.LFE3173:
	.size	RBBISymbolTableEntry_deleter, .-RBBISymbolTableEntry_deleter
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3454:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3454:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3457:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L77
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L65
	cmpb	$0, 12(%rbx)
	jne	.L78
.L69:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L65:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L69
	.cfi_endproc
.LFE3457:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3460:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L81
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3460:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3463:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L84
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3463:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L90
.L86:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L91
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3465:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3466:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3466:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3467:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3467:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3468:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3468:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3469:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3469:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3470:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3470:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3471:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L107
	testl	%edx, %edx
	jle	.L107
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L110
.L99:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L99
	.cfi_endproc
.LFE3471:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L114
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L114
	testl	%r12d, %r12d
	jg	.L121
	cmpb	$0, 12(%rbx)
	jne	.L122
.L116:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L116
.L122:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3472:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L124
	movq	(%rdi), %r8
.L125:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L128
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L128
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L128:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3473:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3474:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L135
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3474:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3475:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3475:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3476:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3476:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3477:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3477:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3479:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3479:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3481:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3481:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715RBBISymbolTableC2EPNS_15RBBIRuleScannerERKNS_13UnicodeStringER10UErrorCode
	.type	_ZN6icu_6715RBBISymbolTableC2EPNS_15RBBIRuleScannerERKNS_13UnicodeStringER10UErrorCode, @function
_ZN6icu_6715RBBISymbolTableC2EPNS_15RBBIRuleScannerERKNS_13UnicodeStringER10UErrorCode:
.LFB3178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6715RBBISymbolTableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$32, %rdi
	movq	%rax, -32(%rdi)
	movq	%rdx, -24(%rdi)
	movq	%rsi, -8(%rdi)
	movl	$65535, %esi
	call	_ZN6icu_6713UnicodeStringC1EDs@PLT
	movq	$0, 16(%rbx)
	xorl	%edx, %edx
	movq	uhash_compareUnicodeString_67@GOTPCREL(%rip), %rsi
	movq	$0, 96(%rbx)
	movq	uhash_hashUnicodeString_67@GOTPCREL(%rip), %rdi
	movq	%r12, %rcx
	call	uhash_open_67@PLT
	movl	(%r12), %edx
	movq	%rax, 16(%rbx)
	testl	%edx, %edx
	jle	.L144
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	popq	%rbx
	leaq	RBBISymbolTableEntry_deleter(%rip), %rsi
	popq	%r12
	movq	%rax, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uhash_setValueDeleter_67@PLT
	.cfi_endproc
.LFE3178:
	.size	_ZN6icu_6715RBBISymbolTableC2EPNS_15RBBIRuleScannerERKNS_13UnicodeStringER10UErrorCode, .-_ZN6icu_6715RBBISymbolTableC2EPNS_15RBBIRuleScannerERKNS_13UnicodeStringER10UErrorCode
	.globl	_ZN6icu_6715RBBISymbolTableC1EPNS_15RBBIRuleScannerERKNS_13UnicodeStringER10UErrorCode
	.set	_ZN6icu_6715RBBISymbolTableC1EPNS_15RBBIRuleScannerERKNS_13UnicodeStringER10UErrorCode,_ZN6icu_6715RBBISymbolTableC2EPNS_15RBBIRuleScannerERKNS_13UnicodeStringER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720RBBISymbolTableEntryC2Ev
	.type	_ZN6icu_6720RBBISymbolTableEntryC2Ev, @function
_ZN6icu_6720RBBISymbolTableEntryC2Ev:
.LFB3190:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	$0, 72(%rdi)
	movq	%rax, 8(%rdi)
	movl	$2, %eax
	movw	%ax, 16(%rdi)
	ret
	.cfi_endproc
.LFE3190:
	.size	_ZN6icu_6720RBBISymbolTableEntryC2Ev, .-_ZN6icu_6720RBBISymbolTableEntryC2Ev
	.globl	_ZN6icu_6720RBBISymbolTableEntryC1Ev
	.set	_ZN6icu_6720RBBISymbolTableEntryC1Ev,_ZN6icu_6720RBBISymbolTableEntryC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720RBBISymbolTableEntryD2Ev
	.type	_ZN6icu_6720RBBISymbolTableEntryD2Ev, @function
_ZN6icu_6720RBBISymbolTableEntryD2Ev:
.LFB3193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %r12
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L147
	movq	%r13, %rdi
	call	_ZN6icu_678RBBINodeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	movq	72(%rbx), %r12
.L147:
	movq	$0, 16(%r12)
	movq	%r12, %rdi
	call	_ZN6icu_678RBBINodeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN6icu_677UMemorydlEPv@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE3193:
	.size	_ZN6icu_6720RBBISymbolTableEntryD2Ev, .-_ZN6icu_6720RBBISymbolTableEntryD2Ev
	.globl	_ZN6icu_6720RBBISymbolTableEntryD1Ev
	.set	_ZN6icu_6720RBBISymbolTableEntryD1Ev,_ZN6icu_6720RBBISymbolTableEntryD2Ev
	.weak	_ZTSN6icu_677UMemoryE
	.section	.rodata._ZTSN6icu_677UMemoryE,"aG",@progbits,_ZTSN6icu_677UMemoryE,comdat
	.align 16
	.type	_ZTSN6icu_677UMemoryE, @object
	.size	_ZTSN6icu_677UMemoryE, 18
_ZTSN6icu_677UMemoryE:
	.string	"N6icu_677UMemoryE"
	.weak	_ZTIN6icu_677UMemoryE
	.section	.data.rel.ro._ZTIN6icu_677UMemoryE,"awG",@progbits,_ZTIN6icu_677UMemoryE,comdat
	.align 8
	.type	_ZTIN6icu_677UMemoryE, @object
	.size	_ZTIN6icu_677UMemoryE, 16
_ZTIN6icu_677UMemoryE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN6icu_677UMemoryE
	.weak	_ZTSN6icu_6715RBBISymbolTableE
	.section	.rodata._ZTSN6icu_6715RBBISymbolTableE,"aG",@progbits,_ZTSN6icu_6715RBBISymbolTableE,comdat
	.align 16
	.type	_ZTSN6icu_6715RBBISymbolTableE, @object
	.size	_ZTSN6icu_6715RBBISymbolTableE, 27
_ZTSN6icu_6715RBBISymbolTableE:
	.string	"N6icu_6715RBBISymbolTableE"
	.weak	_ZTIN6icu_6715RBBISymbolTableE
	.section	.data.rel.ro._ZTIN6icu_6715RBBISymbolTableE,"awG",@progbits,_ZTIN6icu_6715RBBISymbolTableE,comdat
	.align 8
	.type	_ZTIN6icu_6715RBBISymbolTableE, @object
	.size	_ZTIN6icu_6715RBBISymbolTableE, 56
_ZTIN6icu_6715RBBISymbolTableE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN6icu_6715RBBISymbolTableE
	.long	0
	.long	2
	.quad	_ZTIN6icu_677UMemoryE
	.quad	2
	.quad	_ZTIN6icu_6711SymbolTableE
	.quad	2
	.weak	_ZTVN6icu_6715RBBISymbolTableE
	.section	.data.rel.ro.local._ZTVN6icu_6715RBBISymbolTableE,"awG",@progbits,_ZTVN6icu_6715RBBISymbolTableE,comdat
	.align 8
	.type	_ZTVN6icu_6715RBBISymbolTableE, @object
	.size	_ZTVN6icu_6715RBBISymbolTableE, 72
_ZTVN6icu_6715RBBISymbolTableE:
	.quad	0
	.quad	_ZTIN6icu_6715RBBISymbolTableE
	.quad	_ZN6icu_6715RBBISymbolTableD1Ev
	.quad	_ZN6icu_6715RBBISymbolTableD0Ev
	.quad	_ZNK6icu_6715RBBISymbolTable6lookupERKNS_13UnicodeStringE
	.quad	_ZNK6icu_6715RBBISymbolTable13lookupMatcherEi
	.quad	_ZNK6icu_6715RBBISymbolTable14parseReferenceERKNS_13UnicodeStringERNS_13ParsePositionEi
	.quad	_ZNK6icu_6715RBBISymbolTable10lookupNodeERKNS_13UnicodeStringE
	.quad	_ZN6icu_6715RBBISymbolTable8addEntryERKNS_13UnicodeStringEPNS_8RBBINodeER10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
