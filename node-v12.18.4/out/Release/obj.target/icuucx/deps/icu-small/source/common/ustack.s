	.file	"ustack.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676UStack17getDynamicClassIDEv
	.type	_ZNK6icu_676UStack17getDynamicClassIDEv, @function
_ZNK6icu_676UStack17getDynamicClassIDEv:
.LFB2068:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_676UStack16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2068:
	.size	_ZNK6icu_676UStack17getDynamicClassIDEv, .-_ZNK6icu_676UStack17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UStackD2Ev
	.type	_ZN6icu_676UStackD2Ev, @function
_ZN6icu_676UStackD2Ev:
.LFB2082:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_676UStackE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UVectorD2Ev@PLT
	.cfi_endproc
.LFE2082:
	.size	_ZN6icu_676UStackD2Ev, .-_ZN6icu_676UStackD2Ev
	.globl	_ZN6icu_676UStackD1Ev
	.set	_ZN6icu_676UStackD1Ev,_ZN6icu_676UStackD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UStackD0Ev
	.type	_ZN6icu_676UStackD0Ev, @function
_ZN6icu_676UStackD0Ev:
.LFB2084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_676UStackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UVectorD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2084:
	.size	_ZN6icu_676UStackD0Ev, .-_ZN6icu_676UStackD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UStack16getStaticClassIDEv
	.type	_ZN6icu_676UStack16getStaticClassIDEv, @function
_ZN6icu_676UStack16getStaticClassIDEv:
.LFB2067:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_676UStack16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2067:
	.size	_ZN6icu_676UStack16getStaticClassIDEv, .-_ZN6icu_676UStack16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UStackC2ER10UErrorCode
	.type	_ZN6icu_676UStackC2ER10UErrorCode, @function
_ZN6icu_676UStackC2ER10UErrorCode:
.LFB2070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677UVectorC2ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_676UStackE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2070:
	.size	_ZN6icu_676UStackC2ER10UErrorCode, .-_ZN6icu_676UStackC2ER10UErrorCode
	.globl	_ZN6icu_676UStackC1ER10UErrorCode
	.set	_ZN6icu_676UStackC1ER10UErrorCode,_ZN6icu_676UStackC2ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UStackC2EiR10UErrorCode
	.type	_ZN6icu_676UStackC2EiR10UErrorCode, @function
_ZN6icu_676UStackC2EiR10UErrorCode:
.LFB2073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677UVectorC2EiR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_676UStackE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2073:
	.size	_ZN6icu_676UStackC2EiR10UErrorCode, .-_ZN6icu_676UStackC2EiR10UErrorCode
	.globl	_ZN6icu_676UStackC1EiR10UErrorCode
	.set	_ZN6icu_676UStackC1EiR10UErrorCode,_ZN6icu_676UStackC2EiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_ER10UErrorCode
	.type	_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_ER10UErrorCode, @function
_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_ER10UErrorCode:
.LFB2076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_ER10UErrorCode@PLT
	leaq	16+_ZTVN6icu_676UStackE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2076:
	.size	_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_ER10UErrorCode, .-_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_ER10UErrorCode
	.globl	_ZN6icu_676UStackC1EPFvPvEPFa8UElementS4_ER10UErrorCode
	.set	_ZN6icu_676UStackC1EPFvPvEPFa8UElementS4_ER10UErrorCode,_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_ER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_EiR10UErrorCode
	.type	_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_EiR10UErrorCode, @function
_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_EiR10UErrorCode:
.LFB2079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN6icu_677UVectorC2EPFvPvEPFa8UElementS4_EiR10UErrorCode@PLT
	leaq	16+_ZTVN6icu_676UStackE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2079:
	.size	_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_EiR10UErrorCode, .-_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_EiR10UErrorCode
	.globl	_ZN6icu_676UStackC1EPFvPvEPFa8UElementS4_EiR10UErrorCode
	.set	_ZN6icu_676UStackC1EPFvPvEPFa8UElementS4_EiR10UErrorCode,_ZN6icu_676UStackC2EPFvPvEPFa8UElementS4_EiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UStack3popEv
	.type	_ZN6icu_676UStack3popEv, @function
_ZN6icu_676UStack3popEv:
.LFB2085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	8(%rdi), %r13d
	subl	$1, %r13d
	js	.L18
	movq	%rdi, %r12
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN6icu_677UVector15removeElementAtEi@PLT
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%r14d, %r14d
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2085:
	.size	_ZN6icu_676UStack3popEv, .-_ZN6icu_676UStack3popEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_676UStack4popiEv
	.type	_ZN6icu_676UStack4popiEv, @function
_ZN6icu_676UStack4popiEv:
.LFB2086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	8(%rdi), %r13d
	subl	$1, %r13d
	jns	.L25
	addq	$8, %rsp
	xorl	%r14d, %r14d
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	%r13d, %esi
	call	_ZNK6icu_677UVector10elementAtiEi@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN6icu_677UVector15removeElementAtEi@PLT
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2086:
	.size	_ZN6icu_676UStack4popiEv, .-_ZN6icu_676UStack4popiEv
	.align 2
	.p2align 4
	.globl	_ZNK6icu_676UStack6searchEPv
	.type	_ZNK6icu_676UStack6searchEPv, @function
_ZNK6icu_676UStack6searchEPv:
.LFB2087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZNK6icu_677UVector7indexOfEPvi@PLT
	testl	%eax, %eax
	js	.L26
	movl	8(%rbx), %edx
	subl	%eax, %edx
	movl	%edx, %eax
.L26:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2087:
	.size	_ZNK6icu_676UStack6searchEPv, .-_ZNK6icu_676UStack6searchEPv
	.weak	_ZTSN6icu_676UStackE
	.section	.rodata._ZTSN6icu_676UStackE,"aG",@progbits,_ZTSN6icu_676UStackE,comdat
	.align 16
	.type	_ZTSN6icu_676UStackE, @object
	.size	_ZTSN6icu_676UStackE, 17
_ZTSN6icu_676UStackE:
	.string	"N6icu_676UStackE"
	.weak	_ZTIN6icu_676UStackE
	.section	.data.rel.ro._ZTIN6icu_676UStackE,"awG",@progbits,_ZTIN6icu_676UStackE,comdat
	.align 8
	.type	_ZTIN6icu_676UStackE, @object
	.size	_ZTIN6icu_676UStackE, 24
_ZTIN6icu_676UStackE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_676UStackE
	.quad	_ZTIN6icu_677UVectorE
	.weak	_ZTVN6icu_676UStackE
	.section	.data.rel.ro.local._ZTVN6icu_676UStackE,"awG",@progbits,_ZTVN6icu_676UStackE,comdat
	.align 8
	.type	_ZTVN6icu_676UStackE, @object
	.size	_ZTVN6icu_676UStackE, 40
_ZTVN6icu_676UStackE:
	.quad	0
	.quad	_ZTIN6icu_676UStackE
	.quad	_ZN6icu_676UStackD1Ev
	.quad	_ZN6icu_676UStackD0Ev
	.quad	_ZNK6icu_676UStack17getDynamicClassIDEv
	.local	_ZZN6icu_676UStack16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_676UStack16getStaticClassIDEvE7classID,1,1
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
