	.file	"utrie_swap.cpp"
	.text
	.p2align 4
	.globl	utrie_swap_67
	.type	utrie_swap_67, @function
utrie_swap_67:
.LFB2760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r8, %r8
	je	.L1
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L1
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L3
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L3
	movq	%rcx, %r15
	testl	%edx, %edx
	js	.L4
	testq	%rcx, %rcx
	jne	.L4
.L3:
	movl	$1, (%r8)
	xorl	%r13d, %r13d
.L1:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	cmpl	$15, %edx
	jbe	.L9
	movq	%r8, -56(%rbp)
	movl	(%r12), %edi
	movl	%edx, -64(%rbp)
	call	*16(%rbx)
	movl	4(%r12), %edi
	movl	%eax, %r13d
	call	*16(%rbx)
	movl	8(%r12), %esi
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	udata_readInt32_67@PLT
	movl	12(%r12), %esi
	movq	%rbx, %rdi
	movl	%eax, -60(%rbp)
	call	udata_readInt32_67@PLT
	cmpl	$1416784229, %r13d
	movq	-56(%rbp), %r8
	movl	%eax, %r10d
	jne	.L6
	movl	%r14d, %edi
	andl	$15, %edi
	cmpl	$5, %edi
	jne	.L6
	movl	%r14d, %esi
	shrl	$4, %esi
	movl	%esi, %eax
	andl	$15, %eax
	cmpl	$2, %eax
	jne	.L6
	movl	-60(%rbp), %eax
	cmpl	$2047, %eax
	jle	.L6
	cmpl	$31, %r10d
	jle	.L6
	movl	%eax, %ecx
	movl	%r10d, %eax
	andl	$31, %ecx
	andl	$3, %eax
	orl	%eax, %ecx
	jne	.L6
	testl	$512, %r14d
	movl	-64(%rbp), %edx
	je	.L7
	cmpl	$287, %r10d
	jle	.L6
.L7:
	movl	%r14d, %r11d
	movl	-60(%rbp), %eax
	movl	%r10d, -56(%rbp)
	andl	$256, %r11d
	cmpl	$1, %r11d
	leal	(%rax,%rax), %r14d
	movl	%r11d, -64(%rbp)
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	imull	%r10d, %eax
	leal	16(%r14,%rax), %r13d
	testl	%edx, %edx
	js	.L1
	cmpl	%r13d, %edx
	jl	.L9
	movq	%r8, -72(%rbp)
	movq	%r15, %rcx
	movq	%r12, %rsi
	movl	$16, %edx
	movq	%rbx, %rdi
	call	*56(%rbx)
	movl	-64(%rbp), %r11d
	movq	-72(%rbp), %r8
	leaq	16(%r15), %rcx
	movl	-56(%rbp), %r10d
	leaq	16(%r12), %rsi
	testl	%r11d, %r11d
	je	.L10
	movl	%r10d, -64(%rbp)
	movl	%r14d, %edx
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	*48(%rbx)
	movslq	-60(%rbp), %rsi
	movl	-64(%rbp), %r10d
	movq	%rbx, %rdi
	movq	-56(%rbp), %r8
	addq	%rsi, %rsi
	leal	0(,%r10,4), %edx
	addq	$16, %rsi
	leaq	(%r15,%rsi), %rcx
	addq	%r12, %rsi
	call	*56(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$8, (%r8)
	xorl	%r13d, %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$3, (%r8)
	xorl	%r13d, %r13d
	jmp	.L1
.L10:
	movl	-60(%rbp), %edx
	movq	%rbx, %rdi
	addl	%r10d, %edx
	addl	%edx, %edx
	call	*48(%rbx)
	jmp	.L1
	.cfi_endproc
.LFE2760:
	.size	utrie_swap_67, .-utrie_swap_67
	.p2align 4
	.globl	utrie2_swap_67
	.type	utrie2_swap_67, @function
utrie2_swap_67:
.LFB2761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L25
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L27
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L27
	movq	%rcx, %r14
	testl	%edx, %edx
	js	.L28
	testq	%rcx, %rcx
	jne	.L28
.L27:
	movl	$1, (%r8)
	xorl	%r13d, %r13d
.L25:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	cmpl	$15, %edx
	jbe	.L35
	movq	%r8, -56(%rbp)
	movl	(%r12), %edi
	movl	%edx, -64(%rbp)
	call	*16(%rbx)
	movzwl	4(%r12), %edi
	movl	%eax, %r13d
	call	*8(%rbx)
	movzwl	6(%r12), %edi
	movl	%eax, %r15d
	call	*8(%rbx)
	movzwl	8(%r12), %edi
	movw	%ax, -58(%rbp)
	call	*8(%rbx)
	cmpl	$1416784178, %r13d
	movq	-56(%rbp), %r8
	jne	.L30
	testb	$14, %r15b
	movzwl	%ax, %eax
	setne	%sil
	cmpw	$2111, -58(%rbp)
	leal	0(,%rax,4), %r10d
	setbe	%cl
	orb	%cl, %sil
	jne	.L30
	cmpl	$191, %r10d
	movl	-64(%rbp), %edx
	jg	.L31
.L30:
	movl	$3, (%r8)
	xorl	%r13d, %r13d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$8, (%r8)
	xorl	%r13d, %r13d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%r15d, %r9d
	movzwl	-58(%rbp), %r15d
	andl	$15, %r9d
	leal	(%r15,%r15), %r11d
	leal	16(%r11), %r13d
	cmpw	$1, %r9w
	je	.L33
	leal	0(%r13,%rax,8), %r13d
.L34:
	movl	%r11d, -68(%rbp)
	movl	%r10d, -64(%rbp)
	movl	%r9d, -56(%rbp)
	testl	%edx, %edx
	js	.L25
	cmpl	%r13d, %edx
	jl	.L35
	movq	%r8, -80(%rbp)
	movq	%r14, %rcx
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%rbx)
	movq	-80(%rbp), %r8
	leaq	4(%r14), %rcx
	leaq	4(%r12), %rsi
	movl	$12, %edx
	movq	%rbx, %rdi
	call	*48(%rbx)
	movl	-56(%rbp), %r9d
	movq	-80(%rbp), %r8
	leaq	16(%r14), %rcx
	movl	-64(%rbp), %r10d
	movl	-68(%rbp), %r11d
	leaq	16(%r12), %rsi
	cmpw	$1, %r9w
	je	.L36
	leal	(%r10,%r15), %edx
	movq	%rbx, %rdi
	addl	%edx, %edx
	call	*48(%rbx)
	jmp	.L25
.L33:
	sall	$4, %eax
	addl	%eax, %r13d
	jmp	.L34
.L36:
	movl	%r10d, -64(%rbp)
	movl	%r11d, %edx
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	*48(%rbx)
	movzwl	-58(%rbp), %eax
	movl	-64(%rbp), %r10d
	movq	%rbx, %rdi
	movq	-56(%rbp), %r8
	leaq	16(%rax,%rax), %rsi
	leal	0(,%r10,4), %edx
	leaq	(%r14,%rsi), %rcx
	addq	%r12, %rsi
	call	*56(%rbx)
	jmp	.L25
	.cfi_endproc
.LFE2761:
	.size	utrie2_swap_67, .-utrie2_swap_67
	.p2align 4
	.globl	ucptrie_swap_67
	.type	ucptrie_swap_67, @function
ucptrie_swap_67:
.LFB2762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L42
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L44
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L44
	movq	%rcx, %r13
	testl	%edx, %edx
	js	.L45
	testq	%rcx, %rcx
	jne	.L45
.L44:
	movl	$1, (%r8)
	xorl	%r14d, %r14d
.L42:
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	cmpl	$15, %edx
	jbe	.L54
	movq	%r8, -56(%rbp)
	movl	(%r12), %edi
	movl	%edx, -64(%rbp)
	call	*16(%rbx)
	movzwl	4(%r12), %edi
	movl	%eax, %r14d
	call	*8(%rbx)
	movzwl	6(%r12), %edi
	movl	%eax, %r15d
	call	*8(%rbx)
	movzwl	8(%r12), %edi
	movw	%ax, -58(%rbp)
	call	*8(%rbx)
	movzwl	%r15w, %r9d
	movq	-56(%rbp), %r8
	movl	%r9d, %edi
	sarl	$6, %edi
	movl	%edi, %ecx
	andl	$3, %ecx
	cmpl	$1, %ecx
	sbbl	%ecx, %ecx
	andl	$960, %ecx
	addl	$64, %ecx
	cmpl	$1416784179, %r14d
	jne	.L48
	andl	$2, %edi
	jne	.L48
	movl	%r15d, %r10d
	andl	$7, %r10d
	andl	$56, %r15d
	jne	.L48
	cmpw	$2, %r10w
	ja	.L48
	sall	$4, %r9d
	movzwl	%ax, %eax
	movzwl	-58(%rbp), %r11d
	andl	$983040, %r9d
	orl	%eax, %r9d
	cmpl	$127, %r9d
	jle	.L48
	cmpl	%ecx, %r11d
	movl	-64(%rbp), %edx
	jl	.L48
	leal	(%r11,%r11), %eax
	movl	%eax, %r15d
	addl	$16, %eax
	cmpw	$1, %r10w
	je	.L51
	leal	(%rax,%r9,2), %r14d
	addl	%r9d, %eax
	cmpw	$2, %r10w
	cmove	%eax, %r14d
.L53:
	movl	%r11d, -68(%rbp)
	movl	%r9d, -64(%rbp)
	movl	%r10d, -56(%rbp)
	testl	%edx, %edx
	js	.L42
	cmpl	%r14d, %edx
	jl	.L54
	movq	%r8, -80(%rbp)
	movq	%r13, %rcx
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%rbx)
	movq	-80(%rbp), %r8
	leaq	4(%r13), %rcx
	leaq	4(%r12), %rsi
	movl	$12, %edx
	movq	%rbx, %rdi
	call	*48(%rbx)
	movl	-56(%rbp), %r10d
	movq	-80(%rbp), %r8
	leaq	16(%r13), %rcx
	movl	-64(%rbp), %r9d
	leaq	16(%r12), %rsi
	cmpw	$1, %r10w
	je	.L55
	cmpw	$2, %r10w
	je	.L56
	movl	-68(%rbp), %r11d
	movq	%rbx, %rdi
	leal	(%r9,%r11), %edx
	addl	%edx, %edx
	call	*48(%rbx)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$8, (%r8)
	xorl	%r14d, %r14d
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$3, (%r8)
	xorl	%r14d, %r14d
	jmp	.L42
.L51:
	leal	(%rax,%r9,4), %r14d
	jmp	.L53
.L56:
	movl	%r9d, -56(%rbp)
	movl	%r15d, %edx
	movq	%rbx, %rdi
	call	*48(%rbx)
	cmpq	%r13, %r12
	je	.L42
	movzwl	-58(%rbp), %edi
	movslq	-56(%rbp), %rdx
	addq	$1, %rdi
	salq	$4, %rdi
	leaq	(%r12,%rdi), %rsi
	addq	%r13, %rdi
	call	memmove@PLT
	jmp	.L42
.L55:
	movl	%r9d, -64(%rbp)
	movl	%r15d, %edx
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	*48(%rbx)
	movzwl	-58(%rbp), %r15d
	movl	-64(%rbp), %r9d
	movq	%rbx, %rdi
	movq	-56(%rbp), %r8
	leaq	16(%r15,%r15), %rax
	leal	0(,%r9,4), %edx
	leaq	0(%r13,%rax), %rcx
	leaq	(%r12,%rax), %rsi
	call	*56(%rbx)
	jmp	.L42
	.cfi_endproc
.LFE2762:
	.size	ucptrie_swap_67, .-ucptrie_swap_67
	.p2align 4
	.globl	utrie_swapAnyVersion_67
	.type	utrie_swapAnyVersion_67, @function
utrie_swapAnyVersion_67:
.LFB2764:
	.cfi_startproc
	endbr64
	movl	(%r8), %eax
	testl	%eax, %eax
	jg	.L63
	cmpl	$15, %edx
	jle	.L65
	testq	%rsi, %rsi
	je	.L65
	testb	$3, %sil
	jne	.L65
	movl	(%rsi), %eax
	cmpl	$1416784179, %eax
	je	.L66
	cmpl	$862548564, %eax
	je	.L66
	cmpl	$1416784178, %eax
	je	.L67
	cmpl	$845771348, %eax
	je	.L67
	cmpl	$1416784229, %eax
	je	.L70
	cmpl	$1701409364, %eax
	je	.L70
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$3, (%r8)
.L63:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	jmp	ucptrie_swap_67
	.p2align 4,,10
	.p2align 3
.L70:
	jmp	utrie_swap_67
	.p2align 4,,10
	.p2align 3
.L67:
	jmp	utrie2_swap_67
	.cfi_endproc
.LFE2764:
	.size	utrie_swapAnyVersion_67, .-utrie_swapAnyVersion_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
