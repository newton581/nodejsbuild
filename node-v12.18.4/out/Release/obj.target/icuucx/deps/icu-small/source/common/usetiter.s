	.file	"usetiter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6718UnicodeSetIterator17getDynamicClassIDEv
	.type	_ZNK6icu_6718UnicodeSetIterator17getDynamicClassIDEv, @function
_ZNK6icu_6718UnicodeSetIterator17getDynamicClassIDEv:
.LFB2331:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718UnicodeSetIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2331:
	.size	_ZNK6icu_6718UnicodeSetIterator17getDynamicClassIDEv, .-_ZNK6icu_6718UnicodeSetIterator17getDynamicClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIteratorD2Ev
	.type	_ZN6icu_6718UnicodeSetIteratorD2Ev, @function
_ZN6icu_6718UnicodeSetIteratorD2Ev:
.LFB2339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718UnicodeSetIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	movq	(%rdi), %rax
	call	*8(%rax)
.L4:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2339:
	.size	_ZN6icu_6718UnicodeSetIteratorD2Ev, .-_ZN6icu_6718UnicodeSetIteratorD2Ev
	.globl	_ZN6icu_6718UnicodeSetIteratorD1Ev
	.set	_ZN6icu_6718UnicodeSetIteratorD1Ev,_ZN6icu_6718UnicodeSetIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIteratorD0Ev
	.type	_ZN6icu_6718UnicodeSetIteratorD0Ev, @function
_ZN6icu_6718UnicodeSetIteratorD0Ev:
.LFB2341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718UnicodeSetIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L10
	movq	(%rdi), %rax
	call	*8(%rax)
.L10:
	movq	%r12, %rdi
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2341:
	.size	_ZN6icu_6718UnicodeSetIteratorD0Ev, .-_ZN6icu_6718UnicodeSetIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIterator16getStaticClassIDEv
	.type	_ZN6icu_6718UnicodeSetIterator16getStaticClassIDEv, @function
_ZN6icu_6718UnicodeSetIterator16getStaticClassIDEv:
.LFB2330:
	.cfi_startproc
	endbr64
	leaq	_ZZN6icu_6718UnicodeSetIterator16getStaticClassIDEvE7classID(%rip), %rax
	ret
	.cfi_endproc
.LFE2330:
	.size	_ZN6icu_6718UnicodeSetIterator16getStaticClassIDEv, .-_ZN6icu_6718UnicodeSetIterator16getStaticClassIDEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIterator9getStringEv
	.type	_ZN6icu_6718UnicodeSetIterator9getStringEv, @function
_ZN6icu_6718UnicodeSetIterator9getStringEv:
.LFB2347:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L30
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	8(%rdi), %r12d
	cmpl	$-1, %r12d
	je	.L16
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L31
.L18:
	movq	%rax, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movq	-24(%rbp), %rax
	movswl	8(%rax), %edx
	testw	%dx, %dx
	js	.L20
	sarl	$5, %edx
.L21:
	movl	%r12d, %ecx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	movq	56(%rbx), %rax
.L22:
	movq	%rax, 16(%rbx)
.L16:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movl	12(%rax), %edx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$64, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	testq	%rax, %rax
	je	.L19
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rcx
	movl	$2, %edx
	movl	8(%rbx), %r12d
	movq	%rcx, (%rax)
	movw	%dx, 8(%rax)
	movq	%rax, 56(%rbx)
	jmp	.L18
.L19:
	movq	$0, 56(%rbx)
	jmp	.L22
	.cfi_endproc
.LFE2347:
	.size	_ZN6icu_6718UnicodeSetIterator9getStringEv, .-_ZN6icu_6718UnicodeSetIterator9getStringEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIterator9loadRangeEi
	.type	_ZN6icu_6718UnicodeSetIterator9loadRangeEi, @function
_ZN6icu_6718UnicodeSetIterator9loadRangeEi:
.LFB2346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	24(%rbx), %rdi
	movl	%r12d, %esi
	movl	%eax, 44(%rbx)
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%eax, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2346:
	.size	_ZN6icu_6718UnicodeSetIterator9loadRangeEi, .-_ZN6icu_6718UnicodeSetIterator9loadRangeEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIteratorC2ERKNS_10UnicodeSetE
	.type	_ZN6icu_6718UnicodeSetIteratorC2ERKNS_10UnicodeSetE, @function
_ZN6icu_6718UnicodeSetIteratorC2ERKNS_10UnicodeSetE:
.LFB2333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6718UnicodeSetIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rsi, 24(%rbx)
	movq	%rax, (%rbx)
	movq	$0, 56(%rbx)
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movq	24(%rbx), %rdi
	subl	$1, %eax
	movl	%eax, 32(%rbx)
	call	_ZNK6icu_6710UnicodeSet11stringsSizeEv@PLT
	movl	$0, 36(%rbx)
	movl	%eax, 52(%rbx)
	movl	$4294967295, %eax
	movq	%rax, 40(%rbx)
	movl	32(%rbx), %eax
	testl	%eax, %eax
	js	.L35
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movl	%eax, 44(%rbx)
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%eax, 40(%rbx)
.L35:
	movl	$0, 48(%rbx)
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2333:
	.size	_ZN6icu_6718UnicodeSetIteratorC2ERKNS_10UnicodeSetE, .-_ZN6icu_6718UnicodeSetIteratorC2ERKNS_10UnicodeSetE
	.globl	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE
	.set	_ZN6icu_6718UnicodeSetIteratorC1ERKNS_10UnicodeSetE,_ZN6icu_6718UnicodeSetIteratorC2ERKNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIteratorC2Ev
	.type	_ZN6icu_6718UnicodeSetIteratorC2Ev, @function
_ZN6icu_6718UnicodeSetIteratorC2Ev:
.LFB2336:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN6icu_6718UnicodeSetIteratorE(%rip), %rax
	movq	$0, 48(%rdi)
	movups	%xmm0, 16(%rdi)
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, (%rdi)
	movq	$0, 56(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE2336:
	.size	_ZN6icu_6718UnicodeSetIteratorC2Ev, .-_ZN6icu_6718UnicodeSetIteratorC2Ev
	.globl	_ZN6icu_6718UnicodeSetIteratorC1Ev
	.set	_ZN6icu_6718UnicodeSetIteratorC1Ev,_ZN6icu_6718UnicodeSetIteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIterator5resetERKNS_10UnicodeSetE
	.type	_ZN6icu_6718UnicodeSetIterator5resetERKNS_10UnicodeSetE, @function
_ZN6icu_6718UnicodeSetIterator5resetERKNS_10UnicodeSetE:
.LFB2344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rsi, 24(%rbx)
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movq	24(%rbx), %rdi
	subl	$1, %eax
	movl	%eax, 32(%rbx)
	call	_ZNK6icu_6710UnicodeSet11stringsSizeEv@PLT
	movl	$0, 36(%rbx)
	movl	%eax, 52(%rbx)
	movl	$4294967295, %eax
	movq	%rax, 40(%rbx)
	movl	32(%rbx), %eax
	testl	%eax, %eax
	js	.L39
	movq	(%rbx), %rax
	leaq	_ZN6icu_6718UnicodeSetIterator9loadRangeEi(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L40
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movl	%eax, 44(%rbx)
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%eax, 40(%rbx)
.L39:
	movl	$0, 48(%rbx)
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L39
	.cfi_endproc
.LFE2344:
	.size	_ZN6icu_6718UnicodeSetIterator5resetERKNS_10UnicodeSetE, .-_ZN6icu_6718UnicodeSetIterator5resetERKNS_10UnicodeSetE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIterator5resetEv
	.type	_ZN6icu_6718UnicodeSetIterator5resetEv, @function
_ZN6icu_6718UnicodeSetIterator5resetEv:
.LFB2345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	_ZNK6icu_6710UnicodeSet13getRangeCountEv@PLT
	movq	24(%rbx), %rdi
	subl	$1, %eax
	movl	%eax, 32(%rbx)
	call	_ZNK6icu_6710UnicodeSet11stringsSizeEv@PLT
	movl	$0, 36(%rbx)
	movl	%eax, 52(%rbx)
	movl	$4294967295, %eax
	movq	%rax, 40(%rbx)
	movl	32(%rbx), %eax
	testl	%eax, %eax
	js	.L44
	movq	(%rbx), %rax
	leaq	_ZN6icu_6718UnicodeSetIterator9loadRangeEi(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L45
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movl	%eax, 44(%rbx)
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%eax, 40(%rbx)
.L44:
	movl	$0, 48(%rbx)
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	$4294967295, %eax
	movl	$0, 52(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rax, 40(%rbx)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L44
	.cfi_endproc
.LFE2345:
	.size	_ZN6icu_6718UnicodeSetIterator5resetEv, .-_ZN6icu_6718UnicodeSetIterator5resetEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIterator9nextRangeEv
	.type	_ZN6icu_6718UnicodeSetIterator9nextRangeEv, @function
_ZN6icu_6718UnicodeSetIterator9nextRangeEv:
.LFB2343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	44(%rdi), %edx
	movq	%rdi, %rbx
	movl	40(%rdi), %eax
	movq	$0, 16(%rdi)
	cmpl	%eax, %edx
	jle	.L56
	movl	36(%rdi), %esi
	cmpl	32(%rdi), %esi
	jl	.L57
	movl	48(%rdi), %esi
	xorl	%eax, %eax
	cmpl	52(%rdi), %esi
	jge	.L48
	movq	24(%rdi), %rax
	movl	$-1, 8(%rdi)
	movq	80(%rax), %rdi
	leal	1(%rsi), %eax
	movl	%eax, 48(%rbx)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, 16(%rbx)
	movl	$1, %eax
.L48:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	(%rdi), %rax
	leal	1(%rsi), %r12d
	leaq	_ZN6icu_6718UnicodeSetIterator9loadRangeEi(%rip), %rdx
	movq	24(%rax), %rax
	movl	%r12d, 36(%rdi)
	cmpq	%rdx, %rax
	jne	.L52
	movq	24(%rdi), %rdi
	movl	%r12d, %esi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	24(%rbx), %rdi
	movl	%r12d, %esi
	movl	%eax, 44(%rbx)
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%eax, 40(%rbx)
.L53:
	movl	44(%rbx), %edx
	movl	%eax, 12(%rbx)
	addl	$1, %eax
	movl	%eax, 44(%rbx)
	movl	$1, %eax
	movl	%edx, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	%eax, 12(%rdi)
	addl	$1, %eax
	movl	%eax, 44(%rdi)
	movl	$1, %eax
	movl	%edx, 8(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movl	%r12d, %esi
	call	*%rax
	movl	40(%rbx), %eax
	jmp	.L53
	.cfi_endproc
.LFE2343:
	.size	_ZN6icu_6718UnicodeSetIterator9nextRangeEv, .-_ZN6icu_6718UnicodeSetIterator9nextRangeEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6718UnicodeSetIterator4nextEv
	.type	_ZN6icu_6718UnicodeSetIterator4nextEv, @function
_ZN6icu_6718UnicodeSetIterator4nextEv:
.LFB2342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	44(%rdi), %eax
	cmpl	40(%rdi), %eax
	jle	.L66
	movl	36(%rdi), %esi
	cmpl	32(%rdi), %esi
	jl	.L67
	movl	48(%rdi), %esi
	xorl	%eax, %eax
	cmpl	52(%rdi), %esi
	jge	.L58
	movq	24(%rdi), %rax
	movl	$-1, 8(%rdi)
	movq	80(%rax), %rdi
	leal	1(%rsi), %eax
	movl	%eax, 48(%rbx)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	%rax, 16(%rbx)
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	(%rdi), %rax
	leal	1(%rsi), %r12d
	leaq	_ZN6icu_6718UnicodeSetIterator9loadRangeEi(%rip), %rdx
	movq	24(%rax), %rax
	movl	%r12d, 36(%rdi)
	cmpq	%rdx, %rax
	jne	.L62
	movq	24(%rdi), %rdi
	movl	%r12d, %esi
	call	_ZNK6icu_6710UnicodeSet13getRangeStartEi@PLT
	movq	24(%rbx), %rdi
	movl	%r12d, %esi
	movl	%eax, 44(%rbx)
	call	_ZNK6icu_6710UnicodeSet11getRangeEndEi@PLT
	movl	%eax, 40(%rbx)
.L63:
	movl	44(%rbx), %eax
.L66:
	leal	1(%rax), %edx
	movq	$0, 16(%rbx)
	movl	%edx, 44(%rbx)
	movl	%eax, 12(%rbx)
	movl	%eax, 8(%rbx)
	movl	$1, %eax
.L58:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	%r12d, %esi
	call	*%rax
	jmp	.L63
	.cfi_endproc
.LFE2342:
	.size	_ZN6icu_6718UnicodeSetIterator4nextEv, .-_ZN6icu_6718UnicodeSetIterator4nextEv
	.weak	_ZTSN6icu_6718UnicodeSetIteratorE
	.section	.rodata._ZTSN6icu_6718UnicodeSetIteratorE,"aG",@progbits,_ZTSN6icu_6718UnicodeSetIteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6718UnicodeSetIteratorE, @object
	.size	_ZTSN6icu_6718UnicodeSetIteratorE, 30
_ZTSN6icu_6718UnicodeSetIteratorE:
	.string	"N6icu_6718UnicodeSetIteratorE"
	.weak	_ZTIN6icu_6718UnicodeSetIteratorE
	.section	.data.rel.ro._ZTIN6icu_6718UnicodeSetIteratorE,"awG",@progbits,_ZTIN6icu_6718UnicodeSetIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6718UnicodeSetIteratorE, @object
	.size	_ZTIN6icu_6718UnicodeSetIteratorE, 24
_ZTIN6icu_6718UnicodeSetIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6718UnicodeSetIteratorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6718UnicodeSetIteratorE
	.section	.data.rel.ro.local._ZTVN6icu_6718UnicodeSetIteratorE,"awG",@progbits,_ZTVN6icu_6718UnicodeSetIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6718UnicodeSetIteratorE, @object
	.size	_ZTVN6icu_6718UnicodeSetIteratorE, 48
_ZTVN6icu_6718UnicodeSetIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6718UnicodeSetIteratorE
	.quad	_ZN6icu_6718UnicodeSetIteratorD1Ev
	.quad	_ZN6icu_6718UnicodeSetIteratorD0Ev
	.quad	_ZNK6icu_6718UnicodeSetIterator17getDynamicClassIDEv
	.quad	_ZN6icu_6718UnicodeSetIterator9loadRangeEi
	.local	_ZZN6icu_6718UnicodeSetIterator16getStaticClassIDEvE7classID
	.comm	_ZZN6icu_6718UnicodeSetIterator16getStaticClassIDEvE7classID,1,1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	-1
	.long	0
	.long	-1
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
