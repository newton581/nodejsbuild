	.file	"stringtriebuilder.cpp"
	.text
	.p2align 4
	.type	hashStringTrieNode, @function
hashStringTrieNode:
.LFB2123:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2123:
	.size	hashStringTrieNode, .-hashStringTrieNode
	.p2align 4
	.type	equalStringTrieNodes, @function
equalStringTrieNodes:
.LFB2124:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE2124:
	.size	equalStringTrieNodes, .-equalStringTrieNodes
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder4Node19markRightEdgesFirstEi
	.type	_ZN6icu_6717StringTrieBuilder4Node19markRightEdgesFirstEi, @function
_ZN6icu_6717StringTrieBuilder4Node19markRightEdgesFirstEi:
.LFB2144:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movl	%esi, %eax
	testl	%edx, %edx
	jne	.L5
	movl	%esi, 12(%rdi)
.L5:
	ret
	.cfi_endproc
.LFE2144:
	.size	_ZN6icu_6717StringTrieBuilder4Node19markRightEdgesFirstEi, .-_ZN6icu_6717StringTrieBuilder4Node19markRightEdgesFirstEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder14FinalValueNode5writeERS0_
	.type	_ZN6icu_6717StringTrieBuilder14FinalValueNode5writeERS0_, @function
_ZN6icu_6717StringTrieBuilder14FinalValueNode5writeERS0_:
.LFB2146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	(%rdi), %rax
	movl	16(%rbx), %esi
	call	*136(%rax)
	movl	%eax, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2146:
	.size	_ZN6icu_6717StringTrieBuilder14FinalValueNode5writeERS0_, .-_ZN6icu_6717StringTrieBuilder14FinalValueNode5writeERS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder21IntermediateValueNode5writeERS0_
	.type	_ZN6icu_6717StringTrieBuilder21IntermediateValueNode5writeERS0_, @function
_ZN6icu_6717StringTrieBuilder21IntermediateValueNode5writeERS0_:
.LFB2150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movl	20(%rbx), %esi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	*136(%rax)
	movl	%eax, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2150:
	.size	_ZN6icu_6717StringTrieBuilder21IntermediateValueNode5writeERS0_, .-_ZN6icu_6717StringTrieBuilder21IntermediateValueNode5writeERS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder14ListBranchNode5writeERS0_
	.type	_ZN6icu_6717StringTrieBuilder14ListBranchNode5writeERS0_, @function
_ZN6icu_6717StringTrieBuilder14ListBranchNode5writeERS0_:
.LFB2155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movslq	64(%rdi), %rax
	movq	%rax, %r12
	leaq	(%rdi,%rax,8), %rax
	movq	16(%rax), %r14
	testq	%r14, %r14
	je	.L78
	movl	12(%r14), %edx
.L12:
	movq	8(%rax), %rdi
	leal	-2(%r12), %r15d
	testq	%rdi, %rdi
	je	.L13
	movl	12(%rdi), %eax
	testl	%eax, %eax
	js	.L79
.L13:
	testl	%r15d, %r15d
	jle	.L15
	leal	-3(%r12), %r15d
	movslq	%r15d, %rax
	movq	24(%rbx,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L16
	movl	12(%rdi), %eax
	testl	%eax, %eax
	js	.L80
.L16:
	testl	%r15d, %r15d
	je	.L15
	leal	-4(%r12), %r15d
	movslq	%r15d, %rax
	movq	24(%rbx,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L18
	movl	12(%rdi), %eax
	testl	%eax, %eax
	js	.L81
.L18:
	testl	%r15d, %r15d
	je	.L15
	leal	-5(%r12), %r15d
	movslq	%r15d, %rax
	movq	24(%rbx,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L20
	movl	12(%rdi), %eax
	testl	%eax, %eax
	js	.L82
.L20:
	testl	%r15d, %r15d
	je	.L15
.L85:
	subl	$6, %r12d
	movslq	%r12d, %r12
	movq	24(%rbx,%r12,8), %rdi
	testq	%rdi, %rdi
	je	.L15
	movl	12(%rdi), %eax
	testl	%eax, %eax
	js	.L83
	.p2align 4,,10
	.p2align 3
.L15:
	movl	64(%rbx), %r12d
	leal	-1(%r12), %r15d
	testq	%r14, %r14
	je	.L84
	movq	(%r14), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movslq	%r15d, %r15
	call	*40(%rax)
.L25:
	movq	0(%r13), %rax
	movzwl	88(%rbx,%r15,2), %esi
	movq	%r13, %rdi
	call	*120(%rax)
	leal	-2(%r12), %edx
	movl	%eax, 12(%rbx)
	movslq	%edx, %r12
	testl	%edx, %edx
	jns	.L30
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L27:
	subl	12(%rdx), %eax
	xorl	%edx, %edx
	movl	%eax, %esi
	movq	0(%r13), %rax
.L77:
	movq	%r13, %rdi
	call	*136(%rax)
	movq	0(%r13), %rax
	movzwl	88(%rbx,%r12,2), %esi
	movq	%r13, %rdi
	subq	$1, %r12
	call	*120(%rax)
	movl	%eax, 12(%rbx)
	testl	%r12d, %r12d
	js	.L10
.L30:
	movq	24(%rbx,%r12,8), %rdx
	testq	%rdx, %rdx
	jne	.L27
	movl	68(%rbx,%r12,4), %esi
	movq	0(%r13), %rax
	movl	$1, %edx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L10:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	cmpl	%eax, 16(%rbx)
	jl	.L31
	cmpl	%eax, %edx
	jle	.L13
.L31:
	movq	(%rdi), %rax
	movl	%edx, -52(%rbp)
	movq	%r13, %rsi
	call	*40(%rax)
	movl	-52(%rbp), %edx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L80:
	cmpl	%eax, 16(%rbx)
	jl	.L32
	cmpl	%eax, %edx
	jle	.L16
.L32:
	movq	(%rdi), %rax
	movl	%edx, -52(%rbp)
	movq	%r13, %rsi
	call	*40(%rax)
	movl	-52(%rbp), %edx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L78:
	movl	16(%rdi), %edx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L84:
	movq	0(%r13), %rax
	movslq	%r15d, %r15
	movl	$1, %edx
	movq	%r13, %rdi
	movl	68(%rbx,%r15,4), %esi
	call	*136(%rax)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L81:
	cmpl	16(%rbx), %eax
	jg	.L33
	cmpl	%eax, %edx
	jle	.L18
.L33:
	movq	(%rdi), %rax
	movl	%edx, -52(%rbp)
	movq	%r13, %rsi
	call	*40(%rax)
	movl	-52(%rbp), %edx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L82:
	cmpl	16(%rbx), %eax
	jg	.L34
	cmpl	%eax, %edx
	jle	.L20
.L34:
	movq	(%rdi), %rax
	movl	%edx, -52(%rbp)
	movq	%r13, %rsi
	call	*40(%rax)
	movl	-52(%rbp), %edx
	testl	%r15d, %r15d
	jne	.L85
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L83:
	cmpl	%eax, 16(%rbx)
	jl	.L35
	cmpl	%eax, %edx
	jle	.L15
.L35:
	movq	(%rdi), %rax
	movq	%r13, %rsi
	call	*40(%rax)
	jmp	.L15
	.cfi_endproc
.LFE2155:
	.size	_ZN6icu_6717StringTrieBuilder14ListBranchNode5writeERS0_, .-_ZN6icu_6717StringTrieBuilder14ListBranchNode5writeERS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder15SplitBranchNode5writeERS0_
	.type	_ZN6icu_6717StringTrieBuilder15SplitBranchNode5writeERS0_, @function
_ZN6icu_6717StringTrieBuilder15SplitBranchNode5writeERS0_:
.LFB2158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	24(%rdi), %r8
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	movl	12(%r8), %eax
	testl	%eax, %eax
	js	.L94
.L87:
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*40(%rax)
	movq	24(%rbx), %rax
	movq	%r12, %rdi
	movl	12(%rax), %esi
	movq	(%r12), %rax
	call	*152(%rax)
	movq	(%r12), %rax
	movzwl	20(%rbx), %esi
	movq	%r12, %rdi
	call	*120(%rax)
	movl	%eax, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	cmpl	12(%rdi), %eax
	jl	.L89
	cmpl	%eax, 16(%rbx)
	jge	.L87
.L89:
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	%r12, %rsi
	call	*40(%rax)
	movq	32(%rbx), %rdi
	jmp	.L87
	.cfi_endproc
.LFE2158:
	.size	_ZN6icu_6717StringTrieBuilder15SplitBranchNode5writeERS0_, .-_ZN6icu_6717StringTrieBuilder15SplitBranchNode5writeERS0_
	.section	.text._ZN6icu_6717StringTrieBuilder14FinalValueNodeD2Ev,"axG",@progbits,_ZN6icu_6717StringTrieBuilder14FinalValueNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717StringTrieBuilder14FinalValueNodeD2Ev
	.type	_ZN6icu_6717StringTrieBuilder14FinalValueNodeD2Ev, @function
_ZN6icu_6717StringTrieBuilder14FinalValueNodeD2Ev:
.LFB2701:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2701:
	.size	_ZN6icu_6717StringTrieBuilder14FinalValueNodeD2Ev, .-_ZN6icu_6717StringTrieBuilder14FinalValueNodeD2Ev
	.weak	_ZN6icu_6717StringTrieBuilder14FinalValueNodeD1Ev
	.set	_ZN6icu_6717StringTrieBuilder14FinalValueNodeD1Ev,_ZN6icu_6717StringTrieBuilder14FinalValueNodeD2Ev
	.section	.text._ZN6icu_6717StringTrieBuilder14BranchHeadNodeD2Ev,"axG",@progbits,_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD2Ev
	.type	_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD2Ev, @function
_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD2Ev:
.LFB2685:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2685:
	.size	_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD2Ev, .-_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD2Ev
	.weak	_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD1Ev
	.set	_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD1Ev,_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD2Ev
	.section	.text._ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD2Ev,"axG",@progbits,_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD2Ev
	.type	_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD2Ev, @function
_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD2Ev:
.LFB2697:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2697:
	.size	_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD2Ev, .-_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD2Ev
	.weak	_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD1Ev
	.set	_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD1Ev,_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD2Ev
	.section	.text._ZN6icu_6717StringTrieBuilder15SplitBranchNodeD2Ev,"axG",@progbits,_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD2Ev
	.type	_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD2Ev, @function
_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD2Ev:
.LFB2689:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2689:
	.size	_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD2Ev, .-_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD2Ev
	.weak	_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD1Ev
	.set	_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD1Ev,_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD2Ev
	.section	.text._ZN6icu_6717StringTrieBuilder14ListBranchNodeD2Ev,"axG",@progbits,_ZN6icu_6717StringTrieBuilder14ListBranchNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717StringTrieBuilder14ListBranchNodeD2Ev
	.type	_ZN6icu_6717StringTrieBuilder14ListBranchNodeD2Ev, @function
_ZN6icu_6717StringTrieBuilder14ListBranchNodeD2Ev:
.LFB2693:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2693:
	.size	_ZN6icu_6717StringTrieBuilder14ListBranchNodeD2Ev, .-_ZN6icu_6717StringTrieBuilder14ListBranchNodeD2Ev
	.weak	_ZN6icu_6717StringTrieBuilder14ListBranchNodeD1Ev
	.set	_ZN6icu_6717StringTrieBuilder14ListBranchNodeD1Ev,_ZN6icu_6717StringTrieBuilder14ListBranchNodeD2Ev
	.section	.text._ZN6icu_6717StringTrieBuilder14FinalValueNodeD0Ev,"axG",@progbits,_ZN6icu_6717StringTrieBuilder14FinalValueNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717StringTrieBuilder14FinalValueNodeD0Ev
	.type	_ZN6icu_6717StringTrieBuilder14FinalValueNodeD0Ev, @function
_ZN6icu_6717StringTrieBuilder14FinalValueNodeD0Ev:
.LFB2703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2703:
	.size	_ZN6icu_6717StringTrieBuilder14FinalValueNodeD0Ev, .-_ZN6icu_6717StringTrieBuilder14FinalValueNodeD0Ev
	.section	.text._ZN6icu_6717StringTrieBuilder14BranchHeadNodeD0Ev,"axG",@progbits,_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD0Ev
	.type	_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD0Ev, @function
_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD0Ev:
.LFB2687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2687:
	.size	_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD0Ev, .-_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD0Ev
	.section	.text._ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD0Ev,"axG",@progbits,_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD0Ev
	.type	_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD0Ev, @function
_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD0Ev:
.LFB2699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2699:
	.size	_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD0Ev, .-_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD0Ev
	.section	.text._ZN6icu_6717StringTrieBuilder15SplitBranchNodeD0Ev,"axG",@progbits,_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD0Ev
	.type	_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD0Ev, @function
_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD0Ev:
.LFB2691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2691:
	.size	_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD0Ev, .-_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD0Ev
	.section	.text._ZN6icu_6717StringTrieBuilder14ListBranchNodeD0Ev,"axG",@progbits,_ZN6icu_6717StringTrieBuilder14ListBranchNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6717StringTrieBuilder14ListBranchNodeD0Ev
	.type	_ZN6icu_6717StringTrieBuilder14ListBranchNodeD0Ev, @function
_ZN6icu_6717StringTrieBuilder14ListBranchNodeD0Ev:
.LFB2695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2695:
	.size	_ZN6icu_6717StringTrieBuilder14ListBranchNodeD0Ev, .-_ZN6icu_6717StringTrieBuilder14ListBranchNodeD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringTrieBuilder4NodeeqERKS1_
	.type	_ZNK6icu_6717StringTrieBuilder4NodeeqERKS1_, @function
_ZNK6icu_6717StringTrieBuilder4NodeeqERKS1_:
.LFB2143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L110
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L112
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L110
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L110
.L112:
	movl	8(%rbx), %eax
	cmpl	%eax, 8(%r12)
	sete	%r13b
.L110:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2143:
	.size	_ZNK6icu_6717StringTrieBuilder4NodeeqERKS1_, .-_ZNK6icu_6717StringTrieBuilder4NodeeqERKS1_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder21IntermediateValueNode19markRightEdgesFirstEi
	.type	_ZN6icu_6717StringTrieBuilder21IntermediateValueNode19markRightEdgesFirstEi, @function
_ZN6icu_6717StringTrieBuilder21IntermediateValueNode19markRightEdgesFirstEi:
.LFB2149:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movl	%esi, %eax
	testl	%edx, %edx
	jne	.L120
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	movq	(%rdi), %rdx
	call	*32(%rdx)
	movl	%eax, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2149:
	.size	_ZN6icu_6717StringTrieBuilder21IntermediateValueNode19markRightEdgesFirstEi, .-_ZN6icu_6717StringTrieBuilder21IntermediateValueNode19markRightEdgesFirstEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder15LinearMatchNode19markRightEdgesFirstEi
	.type	_ZN6icu_6717StringTrieBuilder15LinearMatchNode19markRightEdgesFirstEi, @function
_ZN6icu_6717StringTrieBuilder15LinearMatchNode19markRightEdgesFirstEi:
.LFB2152:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movl	%esi, %eax
	testl	%edx, %edx
	jne	.L126
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	call	*32(%rdx)
	movl	%eax, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2152:
	.size	_ZN6icu_6717StringTrieBuilder15LinearMatchNode19markRightEdgesFirstEi, .-_ZN6icu_6717StringTrieBuilder15LinearMatchNode19markRightEdgesFirstEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder14ListBranchNode19markRightEdgesFirstEi
	.type	_ZN6icu_6717StringTrieBuilder14ListBranchNode19markRightEdgesFirstEi, @function
_ZN6icu_6717StringTrieBuilder14ListBranchNode19markRightEdgesFirstEi:
.LFB2154:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movl	%esi, %eax
	testl	%edx, %edx
	jne	.L138
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movslq	64(%rdi), %rbx
	movl	%esi, 16(%rdi)
.L132:
	movq	16(%r12,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L131
	subl	%edx, %eax
	movl	%eax, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
.L131:
	subq	$1, %rbx
	movl	$1, %edx
	testl	%ebx, %ebx
	jg	.L132
	movl	%eax, 12(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE2154:
	.size	_ZN6icu_6717StringTrieBuilder14ListBranchNode19markRightEdgesFirstEi, .-_ZN6icu_6717StringTrieBuilder14ListBranchNode19markRightEdgesFirstEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder15SplitBranchNode19markRightEdgesFirstEi
	.type	_ZN6icu_6717StringTrieBuilder15SplitBranchNode19markRightEdgesFirstEi, @function
_ZN6icu_6717StringTrieBuilder15SplitBranchNode19markRightEdgesFirstEi:
.LFB2157:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movl	%esi, %eax
	testl	%edx, %edx
	jne	.L144
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	%esi, 16(%rdi)
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	call	*32(%rdx)
	movq	24(%rbx), %rdi
	leal	-1(%rax), %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	%eax, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2157:
	.size	_ZN6icu_6717StringTrieBuilder15SplitBranchNode19markRightEdgesFirstEi, .-_ZN6icu_6717StringTrieBuilder15SplitBranchNode19markRightEdgesFirstEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder14BranchHeadNode19markRightEdgesFirstEi
	.type	_ZN6icu_6717StringTrieBuilder14BranchHeadNode19markRightEdgesFirstEi, @function
_ZN6icu_6717StringTrieBuilder14BranchHeadNode19markRightEdgesFirstEi:
.LFB2160:
	.cfi_startproc
	endbr64
	movl	12(%rdi), %edx
	movl	%esi, %eax
	testl	%edx, %edx
	jne	.L150
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rdx
	call	*32(%rdx)
	movl	%eax, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE2160:
	.size	_ZN6icu_6717StringTrieBuilder14BranchHeadNode19markRightEdgesFirstEi, .-_ZN6icu_6717StringTrieBuilder14BranchHeadNode19markRightEdgesFirstEi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder14BranchHeadNode5writeERS0_
	.type	_ZN6icu_6717StringTrieBuilder14BranchHeadNode5writeERS0_, @function
_ZN6icu_6717StringTrieBuilder14BranchHeadNode5writeERS0_:
.LFB2161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movl	24(%rbx), %r13d
	movq	%r12, %rdi
	call	*96(%rax)
	cmpl	%eax, %r13d
	movl	24(%rbx), %eax
	jg	.L154
	leal	-1(%rax), %ecx
	movq	(%r12), %rax
	movl	20(%rbx), %edx
	movq	%r12, %rdi
	movsbl	16(%rbx), %esi
	call	*144(%rax)
	movl	%eax, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	leal	-1(%rax), %esi
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*120(%rax)
	movq	(%r12), %rax
	movl	20(%rbx), %edx
	movq	%r12, %rdi
	movsbl	16(%rbx), %esi
	xorl	%ecx, %ecx
	call	*144(%rax)
	movl	%eax, 12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2161:
	.size	_ZN6icu_6717StringTrieBuilder14BranchHeadNode5writeERS0_, .-_ZN6icu_6717StringTrieBuilder14BranchHeadNode5writeERS0_
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringTrieBuilder14FinalValueNodeeqERKNS0_4NodeE
	.type	_ZNK6icu_6717StringTrieBuilder14FinalValueNodeeqERKNS0_4NodeE, @function
_ZNK6icu_6717StringTrieBuilder14FinalValueNodeeqERKNS0_4NodeE:
.LFB2145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L160
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L159
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L157
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L157
.L159:
	movl	8(%rbx), %eax
	xorl	%r13d, %r13d
	cmpl	%eax, 8(%r12)
	jne	.L157
	movl	16(%rbx), %eax
	cmpl	%eax, 16(%r12)
	sete	%r13b
.L157:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2145:
	.size	_ZNK6icu_6717StringTrieBuilder14FinalValueNodeeqERKNS0_4NodeE, .-_ZNK6icu_6717StringTrieBuilder14FinalValueNodeeqERKNS0_4NodeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringTrieBuilder9ValueNodeeqERKNS0_4NodeE
	.type	_ZNK6icu_6717StringTrieBuilder9ValueNodeeqERKNS0_4NodeE, @function
_ZNK6icu_6717StringTrieBuilder9ValueNodeeqERKNS0_4NodeE:
.LFB2147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L169
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L167
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L165
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L165
.L167:
	movl	8(%rbx), %eax
	xorl	%r13d, %r13d
	cmpl	%eax, 8(%r12)
	je	.L177
.L165:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movzbl	16(%r12), %eax
	cmpb	16(%rbx), %al
	jne	.L165
	movl	$1, %r13d
	testb	%al, %al
	je	.L165
	movl	20(%rbx), %eax
	cmpl	%eax, 20(%r12)
	sete	%r13b
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L169:
	addq	$8, %rsp
	movl	$1, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2147:
	.size	_ZNK6icu_6717StringTrieBuilder9ValueNodeeqERKNS0_4NodeE, .-_ZNK6icu_6717StringTrieBuilder9ValueNodeeqERKNS0_4NodeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringTrieBuilder15SplitBranchNodeeqERKNS0_4NodeE
	.type	_ZNK6icu_6717StringTrieBuilder15SplitBranchNodeeqERKNS0_4NodeE, @function
_ZNK6icu_6717StringTrieBuilder15SplitBranchNodeeqERKNS0_4NodeE:
.LFB2156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L182
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L180
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L178
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L178
.L180:
	movl	8(%rbx), %eax
	xorl	%r13d, %r13d
	cmpl	%eax, 8(%r12)
	je	.L189
.L178:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movzwl	20(%rbx), %eax
	cmpw	%ax, 20(%r12)
	jne	.L178
	movq	24(%rbx), %rax
	cmpq	%rax, 24(%r12)
	jne	.L178
	movq	32(%rbx), %rax
	cmpq	%rax, 32(%r12)
	sete	%r13b
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L182:
	addq	$8, %rsp
	movl	$1, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2156:
	.size	_ZNK6icu_6717StringTrieBuilder15SplitBranchNodeeqERKNS0_4NodeE, .-_ZNK6icu_6717StringTrieBuilder15SplitBranchNodeeqERKNS0_4NodeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringTrieBuilder14ListBranchNodeeqERKNS0_4NodeE
	.type	_ZNK6icu_6717StringTrieBuilder14ListBranchNodeeqERKNS0_4NodeE, @function
_ZNK6icu_6717StringTrieBuilder14ListBranchNodeeqERKNS0_4NodeE:
.LFB2153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L213
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L192
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L190
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L190
.L192:
	movl	8(%r12), %eax
	xorl	%r13d, %r13d
	cmpl	%eax, 8(%rbx)
	je	.L219
.L190:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movl	64(%rbx), %eax
	testl	%eax, %eax
	jle	.L213
	movzwl	88(%r12), %edx
	cmpw	%dx, 88(%rbx)
	jne	.L190
	movl	68(%rbx), %ecx
	cmpl	%ecx, 68(%r12)
	jne	.L190
	movq	24(%rbx), %rcx
	cmpq	%rcx, 24(%r12)
	jne	.L190
	cmpl	$1, %eax
	je	.L213
	movzwl	90(%rbx), %edx
	cmpw	%dx, 90(%r12)
	jne	.L190
	movl	72(%r12), %ecx
	cmpl	%ecx, 72(%rbx)
	jne	.L190
	movq	32(%r12), %rdx
	cmpq	%rdx, 32(%rbx)
	jne	.L190
	cmpl	$2, %eax
	je	.L213
	movzwl	92(%r12), %ecx
	cmpw	%cx, 92(%rbx)
	jne	.L216
	movl	76(%r12), %edx
	cmpl	%edx, 76(%rbx)
	jne	.L216
	movq	40(%r12), %rdx
	cmpq	%rdx, 40(%rbx)
	jne	.L216
	cmpl	$3, %eax
	je	.L213
	movzwl	94(%r12), %ecx
	cmpw	%cx, 94(%rbx)
	jne	.L216
	movl	80(%r12), %edx
	cmpl	%edx, 80(%rbx)
	jne	.L216
	movq	48(%r12), %rdx
	cmpq	%rdx, 48(%rbx)
	jne	.L216
	cmpl	$4, %eax
	je	.L213
	movzwl	96(%r12), %ecx
	cmpw	%cx, 96(%rbx)
	jne	.L216
	movl	84(%r12), %ecx
	cmpl	%ecx, 84(%rbx)
	jne	.L216
	movq	56(%r12), %rdx
	cmpq	%rdx, 56(%rbx)
	jne	.L216
	cmpl	$5, %eax
	sete	%r13b
	jmp	.L190
.L216:
	xorl	%r13d, %r13d
	jmp	.L190
	.cfi_endproc
.LFE2153:
	.size	_ZNK6icu_6717StringTrieBuilder14ListBranchNodeeqERKNS0_4NodeE, .-_ZNK6icu_6717StringTrieBuilder14ListBranchNodeeqERKNS0_4NodeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringTrieBuilder21IntermediateValueNodeeqERKNS0_4NodeE
	.type	_ZNK6icu_6717StringTrieBuilder21IntermediateValueNodeeqERKNS0_4NodeE, @function
_ZNK6icu_6717StringTrieBuilder21IntermediateValueNodeeqERKNS0_4NodeE:
.LFB2148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L224
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L222
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L220
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L220
.L222:
	movl	8(%rbx), %eax
	xorl	%r13d, %r13d
	cmpl	%eax, 8(%r12)
	je	.L233
.L220:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movzbl	16(%r12), %eax
	cmpb	16(%rbx), %al
	jne	.L220
	testb	%al, %al
	je	.L223
	movl	20(%rbx), %eax
	cmpl	%eax, 20(%r12)
	jne	.L220
.L223:
	movq	24(%rbx), %rax
	cmpq	%rax, 24(%r12)
	sete	%r13b
	addq	$8, %rsp
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2148:
	.size	_ZNK6icu_6717StringTrieBuilder21IntermediateValueNodeeqERKNS0_4NodeE, .-_ZNK6icu_6717StringTrieBuilder21IntermediateValueNodeeqERKNS0_4NodeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringTrieBuilder14BranchHeadNodeeqERKNS0_4NodeE
	.type	_ZNK6icu_6717StringTrieBuilder14BranchHeadNodeeqERKNS0_4NodeE, @function
_ZNK6icu_6717StringTrieBuilder14BranchHeadNodeeqERKNS0_4NodeE:
.LFB2159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L240
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L236
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L234
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L234
.L236:
	movl	8(%rbx), %eax
	xorl	%r13d, %r13d
	cmpl	%eax, 8(%r12)
	je	.L249
.L234:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	movzbl	16(%r12), %eax
	cmpb	16(%rbx), %al
	jne	.L234
	testb	%al, %al
	je	.L237
	movl	20(%rbx), %eax
	cmpl	%eax, 20(%r12)
	jne	.L234
.L237:
	movl	24(%rbx), %eax
	cmpl	%eax, 24(%r12)
	je	.L238
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	movq	32(%rbx), %rax
	cmpq	%rax, 32(%r12)
	sete	%r13b
	addq	$8, %rsp
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2159:
	.size	_ZNK6icu_6717StringTrieBuilder14BranchHeadNodeeqERKNS0_4NodeE, .-_ZNK6icu_6717StringTrieBuilder14BranchHeadNodeeqERKNS0_4NodeE
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6717StringTrieBuilder15LinearMatchNodeeqERKNS0_4NodeE
	.type	_ZNK6icu_6717StringTrieBuilder15LinearMatchNodeeqERKNS0_4NodeE, @function
_ZNK6icu_6717StringTrieBuilder15LinearMatchNodeeqERKNS0_4NodeE:
.LFB2151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%rsi, %rdi
	je	.L256
	movq	(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	-8(%rax), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
	movq	8(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L252
	xorl	%r13d, %r13d
	cmpb	$42, (%rdi)
	je	.L250
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L250
.L252:
	movl	8(%rbx), %eax
	xorl	%r13d, %r13d
	cmpl	%eax, 8(%r12)
	je	.L265
.L250:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movzbl	16(%r12), %eax
	cmpb	16(%rbx), %al
	jne	.L250
	testb	%al, %al
	je	.L253
	movl	20(%rbx), %eax
	cmpl	%eax, 20(%r12)
	jne	.L250
.L253:
	movl	24(%rbx), %eax
	cmpl	%eax, 24(%r12)
	je	.L254
	addq	$8, %rsp
	xorl	%r13d, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movq	32(%rbx), %rax
	cmpq	%rax, 32(%r12)
	sete	%r13b
	addq	$8, %rsp
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2151:
	.size	_ZNK6icu_6717StringTrieBuilder15LinearMatchNodeeqERKNS0_4NodeE, .-_ZNK6icu_6717StringTrieBuilder15LinearMatchNodeeqERKNS0_4NodeE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilderC2Ev
	.type	_ZN6icu_6717StringTrieBuilderC2Ev, @function
_ZN6icu_6717StringTrieBuilderC2Ev:
.LFB2126:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6717StringTrieBuilderE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE2126:
	.size	_ZN6icu_6717StringTrieBuilderC2Ev, .-_ZN6icu_6717StringTrieBuilderC2Ev
	.globl	_ZN6icu_6717StringTrieBuilderC1Ev
	.set	_ZN6icu_6717StringTrieBuilderC1Ev,_ZN6icu_6717StringTrieBuilderC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilderD2Ev
	.type	_ZN6icu_6717StringTrieBuilderD2Ev, @function
_ZN6icu_6717StringTrieBuilderD2Ev:
.LFB2129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringTrieBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	call	uhash_close_67@PLT
	movq	%r12, %rdi
	movq	$0, 8(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE2129:
	.size	_ZN6icu_6717StringTrieBuilderD2Ev, .-_ZN6icu_6717StringTrieBuilderD2Ev
	.globl	_ZN6icu_6717StringTrieBuilderD1Ev
	.set	_ZN6icu_6717StringTrieBuilderD1Ev,_ZN6icu_6717StringTrieBuilderD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilderD0Ev
	.type	_ZN6icu_6717StringTrieBuilderD0Ev, @function
_ZN6icu_6717StringTrieBuilderD0Ev:
.LFB2131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6717StringTrieBuilderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	call	uhash_close_67@PLT
	movq	%r12, %rdi
	movq	$0, 8(%r12)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE2131:
	.size	_ZN6icu_6717StringTrieBuilderD0Ev, .-_ZN6icu_6717StringTrieBuilderD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder20createCompactBuilderEiR10UErrorCode
	.type	_ZN6icu_6717StringTrieBuilder20createCompactBuilderEiR10UErrorCode, @function
_ZN6icu_6717StringTrieBuilder20createCompactBuilderEiR10UErrorCode:
.LFB2132:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jle	.L280
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movl	%esi, %ecx
	leaq	equalStringTrieNodes(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	hashStringTrieNode(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	xorl	%edx, %edx
	call	uhash_openSize_67@PLT
	movl	(%rbx), %edx
	movq	%rax, 8(%r12)
	testl	%edx, %edx
	jle	.L281
.L271:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L282
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%rax, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uhash_setKeyDeleter_67@PLT
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L271
	.cfi_endproc
.LFE2132:
	.size	_ZN6icu_6717StringTrieBuilder20createCompactBuilderEiR10UErrorCode, .-_ZN6icu_6717StringTrieBuilder20createCompactBuilderEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder20deleteCompactBuilderEv
	.type	_ZN6icu_6717StringTrieBuilder20deleteCompactBuilderEv, @function
_ZN6icu_6717StringTrieBuilder20deleteCompactBuilderEv:
.LFB2133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	uhash_close_67@PLT
	movq	$0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2133:
	.size	_ZN6icu_6717StringTrieBuilder20deleteCompactBuilderEv, .-_ZN6icu_6717StringTrieBuilder20deleteCompactBuilderEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder18writeBranchSubNodeEiiii
	.type	_ZN6icu_6717StringTrieBuilder18writeBranchSubNodeEiiii, @function
_ZN6icu_6717StringTrieBuilder18writeBranchSubNodeEiiii:
.LFB2136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movl	%edx, -252(%rbp)
	movl	%r8d, -212(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.L287:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*88(%rax)
	movl	-212(%rbp), %ecx
	cmpl	%ecx, %eax
	jge	.L286
	movl	%ecx, %r14d
	movq	(%rbx), %rax
	movl	%r15d, %esi
	movl	%r12d, %edx
	shrl	$31, %r14d
	movq	%rbx, %rdi
	addl	%ecx, %r14d
	sarl	%r14d
	movl	%r14d, %ecx
	call	*64(%rax)
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movl	%eax, %r9d
	movq	(%rbx), %rax
	movl	%r9d, %esi
	movl	%r9d, -224(%rbp)
	call	*32(%rax)
	movl	-224(%rbp), %r9d
	movl	%r15d, %esi
	movl	%r14d, %r8d
	movw	%ax, -160(%rbp,%r13,2)
	movl	%r12d, %ecx
	movq	%rbx, %rdi
	movl	%r9d, %edx
	call	_ZN6icu_6717StringTrieBuilder18writeBranchSubNodeEiiii
	movl	-224(%rbp), %r9d
	subl	%r14d, -212(%rbp)
	movl	%eax, -128(%rbp,%r13,4)
	addq	$1, %r13
	movl	%r9d, %r15d
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L286:
	movl	%r12d, %eax
	movl	%r15d, %r12d
	movq	%rbx, %r15
	movl	%r13d, -248(%rbp)
	movl	%eax, %ebx
	movl	-212(%rbp), %eax
	xorl	%r13d, %r13d
	leaq	-60(%rbp), %r14
	movq	%r14, -240(%rbp)
	movq	%r13, %r14
	movq	%r15, %r13
	subl	$1, %eax
	movl	%eax, -232(%rbp)
	leal	1(%rbx), %eax
	movl	%eax, -216(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L291:
	movq	-224(%rbp), %rax
	movl	%ebx, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	leal	1(%r12), %r15d
	movl	%r12d, (%rax,%r14,4)
	movq	0(%r13), %rax
	call	*32(%rax)
	movl	%ebx, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	movzwl	%ax, %ecx
	movq	0(%r13), %rax
	call	*72(%rax)
	movl	%eax, %r8d
	leal	-1(%rax), %eax
	cmpl	%r12d, %eax
	je	.L327
	movq	-240(%rbp), %rax
	movb	$0, (%rax,%r14)
	addq	$1, %r14
	cmpl	%r14d, -232(%rbp)
	jle	.L322
.L289:
	movl	%r8d, %r12d
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L327:
	movq	0(%r13), %rax
	movl	%r8d, -244(%rbp)
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	*24(%rax)
	cmpl	-216(%rbp), %eax
	movq	-240(%rbp), %rax
	movl	-244(%rbp), %r8d
	sete	(%rax,%r14)
	addq	$1, %r14
	cmpl	%r14d, -232(%rbp)
	jg	.L289
	movq	%r13, %r15
	movq	%rax, %r14
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%rax, %r14
	movq	%r13, %r15
.L290:
	cmpl	$1, -212(%rbp)
	movl	$1, %r11d
	cmovg	-232(%rbp), %r11d
	leal	-1(%r11), %r9d
	movslq	%r11d, %rax
	movslq	%r9d, %r12
	movl	%r8d, -192(%rbp,%rax,4)
	cmpb	$0, -60(%rbp,%r12)
	je	.L328
.L292:
	testl	%r9d, %r9d
	je	.L293
	leal	-2(%r11), %r9d
	movslq	%r9d, %rax
	cmpb	$0, -60(%rbp,%rax)
	movq	%rax, -232(%rbp)
	je	.L329
.L294:
	testl	%r9d, %r9d
	je	.L293
	leal	-3(%r11), %r12d
	movslq	%r12d, %r13
	cmpb	$0, -60(%rbp,%r13)
	je	.L330
	testl	%r12d, %r12d
	je	.L293
.L333:
	subl	$4, %r11d
	movslq	%r11d, %r11
	cmpb	$0, -60(%rbp,%r11)
	je	.L331
.L293:
	movl	-216(%rbp), %ecx
	movl	-252(%rbp), %edx
	movl	%r8d, %esi
	movq	%r15, %rdi
	movl	%r8d, -232(%rbp)
	call	_ZN6icu_6717StringTrieBuilder9writeNodeEiii
	movq	(%r15), %rax
	movl	%ebx, %edx
	movq	%r15, %rdi
	movl	-232(%rbp), %r8d
	movq	120(%rax), %r12
	movl	%r8d, %esi
	call	*32(%rax)
	movq	%r15, %rdi
	movzwl	%ax, %esi
	call	*%r12
	movl	-212(%rbp), %r8d
	leaq	-208(%rbp), %rdx
	movq	%rdx, -232(%rbp)
	subl	$2, %r8d
	movslq	%r8d, %r12
	js	.L304
.L303:
	movq	-224(%rbp), %rdx
	movzbl	(%r14,%r12), %r13d
	movq	(%r15), %rcx
	movl	(%rdx,%r12,4), %r8d
	testb	%r13b, %r13b
	je	.L300
	movl	%r8d, %esi
	movq	%r15, %rdi
	movl	%r8d, -212(%rbp)
	call	*40(%rcx)
	movsbl	%r13b, %edx
	movq	%r15, %rdi
	movl	%eax, %esi
	movq	(%r15), %rax
	call	*136(%rax)
.L326:
	movl	-212(%rbp), %r8d
	movq	(%r15), %rax
	movl	%ebx, %edx
	movq	%r15, %rdi
	subq	$1, %r12
	movl	%r8d, %esi
	movq	120(%rax), %r13
	call	*32(%rax)
	movq	%r15, %rdi
	movzwl	%ax, %esi
	call	*%r13
	testl	%r12d, %r12d
	jns	.L303
.L304:
	movl	-248(%rbp), %edx
	testl	%edx, %edx
	je	.L285
	movslq	-248(%rbp), %rbx
	leaq	-160(%rbp), %r12
	leaq	-128(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L305:
	movq	(%r15), %rax
	movl	-4(%r13,%rbx,4), %esi
	movq	%r15, %rdi
	call	*152(%rax)
	movq	(%r15), %rax
	movzwl	-2(%r12,%rbx,2), %esi
	subq	$1, %rbx
	movq	%r15, %rdi
	call	*120(%rax)
	testl	%ebx, %ebx
	jne	.L305
.L285:
	movq	-56(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L332
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	movq	-232(%rbp), %rdx
	movl	%r8d, -212(%rbp)
	movq	%r15, %rdi
	subl	(%rdx,%r12,4), %eax
	xorl	%edx, %edx
	movl	%eax, %esi
	call	*136(%rcx)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L328:
	movl	-192(%rbp,%r12,4), %esi
	movl	-216(%rbp), %ecx
	movl	%r8d, %edx
	movq	%r15, %rdi
	movl	%r11d, -244(%rbp)
	movl	%r9d, -240(%rbp)
	movl	%r8d, -232(%rbp)
	call	_ZN6icu_6717StringTrieBuilder9writeNodeEiii
	movl	-244(%rbp), %r11d
	movl	-240(%rbp), %r9d
	movl	%eax, -208(%rbp,%r12,4)
	movl	-232(%rbp), %r8d
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L329:
	movl	-192(%rbp,%r12,4), %edx
	movl	-192(%rbp,%rax,4), %esi
	movq	%r15, %rdi
	movq	%rax, %r13
	movl	-216(%rbp), %ecx
	movl	%r11d, -256(%rbp)
	movl	%r9d, -244(%rbp)
	movl	%r8d, -240(%rbp)
	call	_ZN6icu_6717StringTrieBuilder9writeNodeEiii
	movl	-256(%rbp), %r11d
	movl	-244(%rbp), %r9d
	movl	%eax, -208(%rbp,%r13,4)
	movl	-240(%rbp), %r8d
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L330:
	movq	-232(%rbp), %rax
	movl	-192(%rbp,%r13,4), %esi
	movq	%r15, %rdi
	movl	%r11d, -244(%rbp)
	movl	-216(%rbp), %ecx
	movl	%r8d, -240(%rbp)
	movl	-192(%rbp,%rax,4), %edx
	call	_ZN6icu_6717StringTrieBuilder9writeNodeEiii
	movl	-244(%rbp), %r11d
	movl	-240(%rbp), %r8d
	movl	%eax, -208(%rbp,%r13,4)
	testl	%r12d, %r12d
	jne	.L333
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L331:
	movl	-216(%rbp), %ecx
	movl	-192(%rbp,%r11,4), %esi
	movq	%r15, %rdi
	movl	%r8d, -240(%rbp)
	movl	-192(%rbp,%r13,4), %edx
	movq	%r11, -232(%rbp)
	call	_ZN6icu_6717StringTrieBuilder9writeNodeEiii
	movq	-232(%rbp), %r11
	movl	-240(%rbp), %r8d
	movl	%eax, -208(%rbp,%r11,4)
	jmp	.L293
.L332:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2136:
	.size	_ZN6icu_6717StringTrieBuilder18writeBranchSubNodeEiiii, .-_ZN6icu_6717StringTrieBuilder18writeBranchSubNodeEiiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder9writeNodeEiii
	.type	_ZN6icu_6717StringTrieBuilder9writeNodeEiii, @function
_ZN6icu_6717StringTrieBuilder9writeNodeEiii:
.LFB2135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	cmpl	%r12d, %eax
	je	.L345
.L335:
	movq	(%r15), %rax
	movl	%r12d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*32(%rax)
	leal	-1(%r13), %r8d
	movl	%r12d, %edx
	movq	%r15, %rdi
	movl	%eax, %ebx
	movq	(%r15), %rax
	movl	%r8d, -56(%rbp)
	movl	%r8d, %esi
	call	*32(%rax)
	movl	-56(%rbp), %r8d
	movl	%r12d, %ecx
	cmpw	%ax, %bx
	movq	(%r15), %rax
	je	.L346
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*56(%rax)
	movl	%r12d, %ecx
	movq	%r15, %rdi
	movl	%r13d, %edx
	movl	%eax, %r8d
	movl	%r14d, %esi
	movl	%eax, %ebx
	call	_ZN6icu_6717StringTrieBuilder18writeBranchSubNodeEiiii
	movq	(%r15), %rax
	leal	-1(%rbx), %r12d
	movq	%r15, %rdi
	call	*96(%rax)
	cmpl	%r12d, %eax
	jle	.L347
.L339:
	movq	(%r15), %rax
	movl	-64(%rbp), %edx
	movl	%r12d, %ecx
	movq	%r15, %rdi
	movl	-68(%rbp), %esi
	movq	144(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movq	(%r15), %rax
	movl	%r12d, %esi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	*120(%rax)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L345:
	movq	(%r15), %rax
	movl	%r14d, %esi
	leal	1(%r14), %ebx
	movq	%r15, %rdi
	movl	%ebx, %r14d
	call	*40(%rax)
	movl	$1, -68(%rbp)
	movl	%eax, -64(%rbp)
	cmpl	%r13d, %ebx
	jne	.L335
	movl	%eax, %esi
	movq	(%r15), %rax
	movq	%r15, %rdi
	movl	$1, %edx
	movq	136(%rax), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movl	%r8d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*48(%rax)
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movl	%eax, %ecx
	movl	%eax, -56(%rbp)
	call	_ZN6icu_6717StringTrieBuilder9writeNodeEiii
	movl	-56(%rbp), %ecx
	movq	(%r15), %rax
	movq	%r15, %rdi
	movl	%ecx, %r13d
	call	*104(%rax)
	subl	%r12d, %r13d
	movl	%eax, %ebx
	cmpl	%eax, %r13d
	jle	.L337
	movl	-56(%rbp), %ecx
	movl	%ecx, %eax
	subl	%ebx, %eax
	subl	%r13d, %eax
	movl	%eax, -60(%rbp)
	.p2align 4,,10
	.p2align 3
.L338:
	movl	-60(%rbp), %eax
	movl	%r14d, %esi
	movl	%ebx, %ecx
	movq	%r15, %rdi
	leal	(%rax,%r13), %edx
	movq	(%r15), %rax
	subl	%ebx, %r13d
	call	*128(%rax)
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	120(%rax), %rdx
	movq	%rdx, -56(%rbp)
	call	*96(%rax)
	movq	-56(%rbp), %rdx
	movq	%r15, %rdi
	leal	-1(%rbx,%rax), %esi
	call	*%rdx
	cmpl	%r13d, %ebx
	jl	.L338
.L337:
	movq	(%r15), %rax
	movl	%r12d, %edx
	movl	%r13d, %ecx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	*128(%rax)
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*96(%rax)
	leal	-1(%r13,%rax), %r12d
	jmp	.L339
	.cfi_endproc
.LFE2135:
	.size	_ZN6icu_6717StringTrieBuilder9writeNodeEiii, .-_ZN6icu_6717StringTrieBuilder9writeNodeEiii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder12registerNodeEPNS0_4NodeER10UErrorCode
	.type	_ZN6icu_6717StringTrieBuilder12registerNodeEPNS0_4NodeER10UErrorCode, @function
_ZN6icu_6717StringTrieBuilder12registerNodeEPNS0_4NodeER10UErrorCode:
.LFB2139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L358
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	je	.L359
	movq	%rdi, %r14
	movq	8(%rdi), %rdi
	call	uhash_find_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L352
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	16(%r13), %r13
.L348:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	xorl	%r13d, %r13d
	testq	%rsi, %rsi
	je	.L348
.L357:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movq	8(%r14), %rdi
	movq	%rbx, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	call	uhash_puti_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L357
	movq	%r12, %r13
	popq	%rbx
	popq	%r12
	movq	%r13, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	xorl	%r13d, %r13d
	movl	$7, (%rdx)
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2139:
	.size	_ZN6icu_6717StringTrieBuilder12registerNodeEPNS0_4NodeER10UErrorCode, .-_ZN6icu_6717StringTrieBuilder12registerNodeEPNS0_4NodeER10UErrorCode
	.align 2
	.p2align 4
	.type	_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode.part.0, @function
_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode.part.0:
.LFB2721:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rbx, %rdi
	subq	$216, %rsp
	movl	%edx, -244(%rbp)
	movq	%r9, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movl	%esi, -212(%rbp)
	call	*88(%rax)
	cmpl	%r14d, %eax
	jge	.L361
.L401:
	movl	%r14d, %r8d
	movq	(%rbx), %rax
	movl	-212(%rbp), %esi
	movl	%r13d, %edx
	shrl	$31, %r8d
	movq	%rbx, %rdi
	addl	%r14d, %r8d
	sarl	%r8d
	movl	%r8d, -216(%rbp)
	movl	%r8d, %ecx
	call	*64(%rax)
	movl	%r13d, %edx
	movq	%rbx, %rdi
	movl	%eax, %r15d
	movq	(%rbx), %rax
	movl	%r15d, %esi
	call	*32(%rax)
	movq	-224(%rbp), %rdi
	movw	%ax, -208(%rbp,%r12,2)
	xorl	%eax, %eax
	movl	(%rdi), %edx
	testl	%edx, %edx
	jg	.L362
	movl	-216(%rbp), %r8d
	movq	%rdi, %r9
	movl	%r13d, %ecx
	movl	%r15d, %edx
	movl	-212(%rbp), %esi
	movq	%rbx, %rdi
	call	_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode.part.0
.L362:
	movq	%rax, -176(%rbp,%r12,8)
	movl	%r14d, %eax
	addq	$1, %r12
	movq	%rbx, %rdi
	shrl	$31, %eax
	movl	%r15d, -212(%rbp)
	addl	%r14d, %eax
	sarl	%eax
	subl	%eax, %r14d
	movq	(%rbx), %rax
	call	*88(%rax)
	cmpl	%r14d, %eax
	jl	.L401
.L361:
	movq	-224(%rbp), %rax
	movl	%r12d, -248(%rbp)
	movl	%r14d, %r15d
	xorl	%r12d, %r12d
	movl	-212(%rbp), %r14d
	movl	(%rax), %eax
	testl	%eax, %eax
	jg	.L360
	movl	$104, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L365
	movq	$4473924, 8(%rax)
	leaq	16+_ZTVN6icu_6717StringTrieBuilder14ListBranchNodeE(%rip), %rax
	movq	%rax, (%r12)
	leal	-1(%r15), %eax
	movl	%r14d, %r15d
	movq	%r12, %r14
	movl	%eax, -240(%rbp)
	leal	1(%r13), %eax
	movl	$0, 64(%r12)
	movl	%eax, -236(%rbp)
	movl	$0, -212(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L367:
	movl	-236(%rbp), %ecx
	movl	%r15d, %edx
	movl	%r11d, %esi
	movq	%rbx, %rdi
	movq	-224(%rbp), %r8
	call	_ZN6icu_6717StringTrieBuilder8makeNodeEiiiR10UErrorCode
	movslq	64(%r14), %rcx
	movzwl	-230(%rbp), %esi
	movq	%rcx, %rdx
	movw	%si, 88(%r14,%rcx,2)
	addl	$1, %edx
	movq	%rax, 24(%r14,%rcx,8)
	movl	$0, 68(%r14,%rcx,4)
	movl	%edx, 64(%r14)
	movl	8(%r14), %edx
	leal	(%rdx,%rdx,8), %ecx
	leal	(%rdx,%rcx,4), %edx
	addl	%edx, %r12d
	leal	(%r12,%r12,8), %edx
	leal	(%r12,%rdx,4), %edx
	testq	%rax, %rax
	je	.L369
	addl	8(%rax), %edx
.L369:
	movl	%edx, 8(%r14)
.L368:
	addl	$1, -212(%rbp)
	movl	-212(%rbp), %eax
	cmpl	-240(%rbp), %eax
	jge	.L402
.L370:
	movq	(%rbx), %rax
	leal	1(%r15), %r8d
	movl	%r15d, %esi
	movl	%r13d, %edx
	movl	%r8d, -228(%rbp)
	movq	%rbx, %rdi
	call	*32(%rax)
	movl	-228(%rbp), %r8d
	movl	%r13d, %edx
	movq	%rbx, %rdi
	movw	%ax, -230(%rbp)
	movzwl	%ax, %r12d
	movq	(%rbx), %rax
	movl	%r15d, -216(%rbp)
	movl	%r12d, %ecx
	movl	%r8d, %esi
	call	*72(%rax)
	movl	-216(%rbp), %r11d
	movl	%eax, %r15d
	leal	-1(%rax), %eax
	cmpl	%eax, %r11d
	jne	.L367
	movq	(%rbx), %rax
	movl	%r11d, %esi
	movq	%rbx, %rdi
	call	*24(%rax)
	cmpl	-236(%rbp), %eax
	movl	-216(%rbp), %r11d
	jne	.L367
	movq	(%rbx), %rax
	movl	%r11d, %esi
	movq	%rbx, %rdi
	call	*40(%rax)
	movslq	64(%r14), %rcx
	movzwl	-230(%rbp), %esi
	movq	%rcx, %rdx
	movw	%si, 88(%r14,%rcx,2)
	movq	$0, 24(%r14,%rcx,8)
	addl	$1, %edx
	movl	%eax, 68(%r14,%rcx,4)
	movl	%edx, 64(%r14)
	movl	8(%r14), %edx
	leal	(%rdx,%rdx,8), %ecx
	leal	(%rdx,%rcx,4), %edx
	addl	%edx, %r12d
	leal	(%r12,%r12,8), %edx
	leal	(%r12,%rdx,4), %edx
	addl	%edx, %eax
	movl	%eax, 8(%r14)
	jmp	.L368
.L365:
	movq	-224(%rbp), %rax
	movl	$7, (%rax)
	.p2align 4,,10
	.p2align 3
.L360:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L403
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	movq	(%rbx), %rax
	movl	%r13d, %edx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%r14, %r12
	movl	%r15d, %r14d
	call	*32(%rax)
	movl	%eax, %r15d
	movl	-244(%rbp), %eax
	movzwl	%r15w, %r13d
	subl	$1, %eax
	cmpl	%eax, %r14d
	jne	.L372
	movq	(%rbx), %rax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movzwl	%r15w, %r13d
	call	*24(%rax)
	cmpl	-236(%rbp), %eax
	je	.L373
.L372:
	movl	-236(%rbp), %ecx
	movl	-244(%rbp), %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movq	-224(%rbp), %r8
	call	_ZN6icu_6717StringTrieBuilder8makeNodeEiiiR10UErrorCode
	movslq	64(%r12), %rcx
	movq	%rcx, %rdx
	movw	%r15w, 88(%r12,%rcx,2)
	addl	$1, %edx
	movq	%rax, 24(%r12,%rcx,8)
	movl	$0, 68(%r12,%rcx,4)
	movl	%edx, 64(%r12)
	movl	8(%r12), %edx
	leal	(%rdx,%rdx,8), %ecx
	leal	(%rdx,%rcx,4), %edx
	addl	%edx, %r13d
	leal	0(%r13,%r13,8), %edx
	leal	0(%r13,%rdx,4), %edx
	testq	%rax, %rax
	je	.L375
	addl	8(%rax), %edx
.L375:
	movl	%edx, 8(%r12)
.L376:
	movq	-224(%rbp), %r15
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	call	_ZN6icu_6717StringTrieBuilder12registerNodeEPNS0_4NodeER10UErrorCode
	movq	%rax, %r12
	movl	-248(%rbp), %eax
	testl	%eax, %eax
	je	.L360
	movslq	%eax, %r13
	leaq	16+_ZTVN6icu_6717StringTrieBuilder15SplitBranchNodeE(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L380:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L377
	movzwl	-210(%rbp,%r13,2), %edx
	movq	-184(%rbp,%r13,8), %rax
	leal	(%rdx,%rdx,8), %edi
	movl	%edx, %ecx
	leal	(%rdx,%rdi,4), %edx
	testq	%rax, %rax
	je	.L378
	addl	8(%rax), %edx
.L378:
	leal	(%rdx,%rdx,8), %edi
	leal	(%rdx,%rdi,4), %edi
	xorl	%edx, %edx
	testq	%r12, %r12
	je	.L379
	movl	8(%r12), %edx
.L379:
	leal	-195751071(%rdx,%rdi), %edx
	movq	%rax, %xmm0
	movq	%r12, %xmm1
	movl	$0, 12(%rsi)
	movl	%edx, 8(%rsi)
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, (%rsi)
	movw	%cx, 20(%rsi)
	movups	%xmm0, 24(%rsi)
.L377:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	subq	$1, %r13
	call	_ZN6icu_6717StringTrieBuilder12registerNodeEPNS0_4NodeER10UErrorCode
	movq	%rax, %r12
	testl	%r13d, %r13d
	jne	.L380
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L373:
	movq	(%rbx), %rax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*40(%rax)
	movslq	64(%r12), %rcx
	movq	%rcx, %rdx
	movw	%r15w, 88(%r12,%rcx,2)
	movq	$0, 24(%r12,%rcx,8)
	addl	$1, %edx
	movl	%eax, 68(%r12,%rcx,4)
	movl	%edx, 64(%r12)
	movl	8(%r12), %edx
	leal	(%rdx,%rdx,8), %ecx
	leal	(%rdx,%rcx,4), %edx
	addl	%edx, %r13d
	leal	0(%r13,%r13,8), %edx
	leal	0(%r13,%rdx,4), %edx
	addl	%edx, %eax
	movl	%eax, 8(%r12)
	jmp	.L376
.L403:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2721:
	.size	_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode.part.0, .-_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode
	.type	_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode, @function
_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode:
.LFB2138:
	.cfi_startproc
	endbr64
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L405
	jmp	_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode.part.0
	.p2align 4,,10
	.p2align 3
.L405:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2138:
	.size	_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode, .-_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder8makeNodeEiiiR10UErrorCode
	.type	_ZN6icu_6717StringTrieBuilder8makeNodeEiiiR10UErrorCode, @function
_ZN6icu_6717StringTrieBuilder8makeNodeEiiiR10UErrorCode:
.LFB2137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -84(%rbp)
	movl	(%r8), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jg	.L410
	movq	(%rdi), %rax
	movq	%rdi, %r15
	movl	%esi, %r13d
	movl	%edx, %r14d
	movq	%r8, %r12
	call	*24(%rax)
	movb	$0, -85(%rbp)
	movl	$0, -100(%rbp)
	cmpl	-84(%rbp), %eax
	je	.L446
.L409:
	movq	(%r15), %rax
	movl	-84(%rbp), %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	*32(%rax)
	leal	-1(%r14), %r8d
	movl	-84(%rbp), %edx
	movq	%r15, %rdi
	movl	%eax, %ebx
	movq	(%r15), %rax
	movl	%r8d, -96(%rbp)
	movl	%r8d, %esi
	call	*32(%rax)
	movl	-96(%rbp), %r8d
	cmpw	%ax, %bx
	movq	(%r15), %rax
	je	.L447
	movl	-84(%rbp), %ecx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	*56(%rax)
	movl	%eax, %ebx
	movl	(%r12), %eax
	testl	%eax, %eax
	jg	.L448
	movl	-84(%rbp), %ecx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %r9
	movl	%ebx, %r8d
	movq	%r15, %rdi
	call	_ZN6icu_6717StringTrieBuilder17makeBranchSubNodeEiiiiR10UErrorCode.part.0
	movl	$40, %edi
	movq	%rax, %r14
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L422
	leal	(%rbx,%rbx,8), %eax
	leal	(%rbx,%rax,4), %edx
	testq	%r14, %r14
	je	.L429
	movl	8(%r14), %eax
.L421:
	leal	597268342(%rdx,%rax), %eax
	movl	$0, 12(%r13)
	movl	%eax, 8(%r13)
	leaq	16+_ZTVN6icu_6717StringTrieBuilder14BranchHeadNodeE(%rip), %rax
	movb	$0, 16(%r13)
	movl	$0, 20(%r13)
	movq	%rax, 0(%r13)
	movl	%ebx, 24(%r13)
	movq	%r14, 32(%r13)
.L417:
	testq	%r13, %r13
	je	.L420
	cmpb	$0, -85(%rbp)
	je	.L420
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*80(%rax)
	testb	%al, %al
	je	.L423
	movl	8(%r13), %eax
	movl	-100(%rbp), %ecx
	movb	$1, 16(%r13)
	leal	(%rax,%rax,8), %edx
	movl	%ecx, 20(%r13)
	leal	(%rax,%rdx,4), %eax
	addl	%ecx, %eax
	movl	%eax, 8(%r13)
.L420:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6717StringTrieBuilder12registerNodeEPNS0_4NodeER10UErrorCode
	movq	%rax, %r14
.L406:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L449
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	movl	-84(%rbp), %ebx
	movl	%r8d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	%ebx, %ecx
	call	*48(%rax)
	movq	%r12, %r8
	movl	%r14d, %edx
	movl	%r13d, %esi
	movl	%eax, %ecx
	movq	%r15, %rdi
	movl	%eax, -104(%rbp)
	call	_ZN6icu_6717StringTrieBuilder8makeNodeEiiiR10UErrorCode
	movl	-104(%rbp), %ecx
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	movq	(%r15), %rax
	movl	%ecx, %r14d
	call	*104(%rax)
	subl	%ebx, %r14d
	movq	-96(%rbp), %r8
	cmpl	%eax, %r14d
	movl	%eax, %ebx
	jle	.L415
	movl	-104(%rbp), %ecx
	movl	%ecx, %eax
	subl	%ebx, %eax
	subl	%r14d, %eax
	movl	%eax, -96(%rbp)
	movq	%r15, %rax
	movl	%ebx, %r15d
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L416:
	movl	-96(%rbp), %eax
	movl	%r15d, %ecx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	leal	(%rax,%r14), %edx
	movq	(%rbx), %rax
	subl	%r15d, %r14d
	call	*112(%rax)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN6icu_6717StringTrieBuilder12registerNodeEPNS0_4NodeER10UErrorCode
	movq	%rax, %r8
	cmpl	%r14d, %r15d
	jl	.L416
	movq	%rbx, %r15
.L415:
	movq	(%r15), %rax
	movl	%r13d, %esi
	movl	-84(%rbp), %edx
	movl	%r14d, %ecx
	movq	%r15, %rdi
	call	*112(%rax)
	movq	%rax, %r13
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L410:
	xorl	%r14d, %r14d
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L446:
	movq	(%r15), %rax
	movl	%r13d, %esi
	leal	1(%r13), %ebx
	movq	%r15, %rdi
	movl	%ebx, %r13d
	call	*40(%rax)
	movb	$1, -85(%rbp)
	movl	%eax, -100(%rbp)
	cmpl	%r14d, %ebx
	jne	.L409
	movl	(%r12), %ecx
	testl	%ecx, %ecx
	jg	.L410
	leaq	-80(%rbp), %r13
	movq	8(%r15), %rdi
	movl	%eax, %ecx
	leaq	16+_ZTVN6icu_6717StringTrieBuilder14FinalValueNodeE(%rip), %rbx
	leal	41383797(%rax), %eax
	movq	%r13, %rsi
	movl	$0, -68(%rbp)
	movl	%eax, -84(%rbp)
	movl	%eax, -72(%rbp)
	movq	%rbx, -80(%rbp)
	movl	%ecx, -64(%rbp)
	call	uhash_find_67@PLT
	testq	%rax, %rax
	je	.L411
	movq	16(%rax), %r14
.L412:
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L423:
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN6icu_6717StringTrieBuilder12registerNodeEPNS0_4NodeER10UErrorCode
	movl	$32, %edi
	movq	%rax, %rbx
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L422
	movl	$-1232566318, %eax
	testq	%rbx, %rbx
	je	.L424
	movl	8(%rbx), %eax
	addl	$82767594, %eax
	leal	(%rax,%rax,8), %edx
	leal	(%rax,%rdx,4), %eax
.L424:
	leaq	16+_ZTVN6icu_6717StringTrieBuilder21IntermediateValueNodeE(%rip), %rcx
	movl	$0, 12(%r13)
	movq	%rcx, 0(%r13)
	movl	-100(%rbp), %ecx
	movq	%rbx, 24(%r13)
	addl	%ecx, %eax
	movb	$1, 16(%r13)
	movl	%ecx, 20(%r13)
	movl	%eax, 8(%r13)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L448:
	movl	$40, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L422
	leal	(%rbx,%rbx,8), %eax
	xorl	%r14d, %r14d
	leal	(%rbx,%rax,4), %edx
	xorl	%eax, %eax
	jmp	.L421
.L411:
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L413
	movl	-84(%rbp), %eax
	movq	%rbx, (%r14)
	movl	$1, %edx
	movq	%r12, %rcx
	movl	$0, 12(%r14)
	movq	8(%r15), %rdi
	movq	%r14, %rsi
	movl	%eax, 8(%r14)
	movl	-100(%rbp), %eax
	movl	%eax, 16(%r14)
	call	uhash_puti_67@PLT
	movl	(%r12), %edx
	testl	%edx, %edx
	jle	.L412
	movq	(%r14), %rax
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	*8(%rax)
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L429:
	xorl	%eax, %eax
	jmp	.L421
.L449:
	call	__stack_chk_fail@PLT
.L422:
	xorl	%r13d, %r13d
	jmp	.L420
.L413:
	movl	$7, (%r12)
	jmp	.L412
	.cfi_endproc
.LFE2137:
	.size	_ZN6icu_6717StringTrieBuilder8makeNodeEiiiR10UErrorCode, .-_ZN6icu_6717StringTrieBuilder8makeNodeEiiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode
	.type	_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode, @function
_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode:
.LFB2134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testl	%esi, %esi
	je	.L458
	movq	%rcx, %rbx
	movl	(%rcx), %ecx
	testl	%ecx, %ecx
	jle	.L459
.L453:
	movl	%r13d, %edx
	movq	%rbx, %r8
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN6icu_6717StringTrieBuilder8makeNodeEiiiR10UErrorCode
	movq	%rax, %r13
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L460
.L456:
	movq	8(%r12), %rdi
	call	uhash_close_67@PLT
	movq	$0, 8(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movl	$-1, %esi
	call	*32(%rax)
	movq	0(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*40(%rax)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L459:
	leal	(%rdx,%rdx), %ecx
	movq	%rbx, %r8
	xorl	%edx, %edx
	leaq	equalStringTrieNodes(%rip), %rsi
	leaq	hashStringTrieNode(%rip), %rdi
	call	uhash_openSize_67@PLT
	movl	(%rbx), %edx
	movq	%rax, 8(%r12)
	testl	%edx, %edx
	jg	.L453
	testq	%rax, %rax
	je	.L461
	movq	uprv_deleteUObject_67@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	call	uhash_setKeyDeleter_67@PLT
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L458:
	addq	$8, %rsp
	xorl	%ecx, %ecx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6717StringTrieBuilder9writeNodeEiii
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movl	$7, (%rbx)
	jmp	.L453
	.cfi_endproc
.LFE2134:
	.size	_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode, .-_ZN6icu_6717StringTrieBuilder5buildE22UStringTrieBuildOptioniR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder18registerFinalValueEiR10UErrorCode
	.type	_ZN6icu_6717StringTrieBuilder18registerFinalValueEiR10UErrorCode, @function
_ZN6icu_6717StringTrieBuilder18registerFinalValueEiR10UErrorCode:
.LFB2140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L462
	leal	41383797(%rsi), %eax
	movq	%rdi, %r15
	leaq	-80(%rbp), %r14
	movl	%esi, -64(%rbp)
	movq	8(%rdi), %rdi
	movl	%eax, -84(%rbp)
	movl	%esi, %ebx
	movq	%r14, %rsi
	movl	%eax, -72(%rbp)
	leaq	16+_ZTVN6icu_6717StringTrieBuilder14FinalValueNodeE(%rip), %rax
	movq	%rdx, %r12
	movl	$0, -68(%rbp)
	movq	%rax, -80(%rbp)
	call	uhash_find_67@PLT
	testq	%rax, %rax
	je	.L464
	movq	16(%rax), %r13
.L465:
	leaq	16+_ZTVN6icu_6717StringTrieBuilder4NodeE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN6icu_677UObjectD2Ev@PLT
.L462:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L471
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	movl	$24, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L466
	movl	-84(%rbp), %eax
	movl	%ebx, 16(%r13)
	movq	%r12, %rcx
	movl	$1, %edx
	movl	$0, 12(%r13)
	movq	8(%r15), %rdi
	movq	%r13, %rsi
	movl	%eax, 8(%r13)
	leaq	16+_ZTVN6icu_6717StringTrieBuilder14FinalValueNodeE(%rip), %rax
	movq	%rax, 0(%r13)
	call	uhash_puti_67@PLT
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L465
	movq	0(%r13), %rax
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	*8(%rax)
	jmp	.L465
.L471:
	call	__stack_chk_fail@PLT
.L466:
	movl	$7, (%r12)
	jmp	.L465
	.cfi_endproc
.LFE2140:
	.size	_ZN6icu_6717StringTrieBuilder18registerFinalValueEiR10UErrorCode, .-_ZN6icu_6717StringTrieBuilder18registerFinalValueEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder8hashNodeEPKv
	.type	_ZN6icu_6717StringTrieBuilder8hashNodeEPKv, @function
_ZN6icu_6717StringTrieBuilder8hashNodeEPKv:
.LFB2141:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE2141:
	.size	_ZN6icu_6717StringTrieBuilder8hashNodeEPKv, .-_ZN6icu_6717StringTrieBuilder8hashNodeEPKv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6717StringTrieBuilder10equalNodesEPKvS2_
	.type	_ZN6icu_6717StringTrieBuilder10equalNodesEPKvS2_, @function
_ZN6icu_6717StringTrieBuilder10equalNodesEPKvS2_:
.LFB2142:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE2142:
	.size	_ZN6icu_6717StringTrieBuilder10equalNodesEPKvS2_, .-_ZN6icu_6717StringTrieBuilder10equalNodesEPKvS2_
	.weak	_ZTSN6icu_6717StringTrieBuilder4NodeE
	.section	.rodata._ZTSN6icu_6717StringTrieBuilder4NodeE,"aG",@progbits,_ZTSN6icu_6717StringTrieBuilder4NodeE,comdat
	.align 32
	.type	_ZTSN6icu_6717StringTrieBuilder4NodeE, @object
	.size	_ZTSN6icu_6717StringTrieBuilder4NodeE, 34
_ZTSN6icu_6717StringTrieBuilder4NodeE:
	.string	"N6icu_6717StringTrieBuilder4NodeE"
	.weak	_ZTIN6icu_6717StringTrieBuilder4NodeE
	.section	.data.rel.ro._ZTIN6icu_6717StringTrieBuilder4NodeE,"awG",@progbits,_ZTIN6icu_6717StringTrieBuilder4NodeE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringTrieBuilder4NodeE, @object
	.size	_ZTIN6icu_6717StringTrieBuilder4NodeE, 24
_ZTIN6icu_6717StringTrieBuilder4NodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringTrieBuilder4NodeE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTSN6icu_6717StringTrieBuilder14FinalValueNodeE
	.section	.rodata._ZTSN6icu_6717StringTrieBuilder14FinalValueNodeE,"aG",@progbits,_ZTSN6icu_6717StringTrieBuilder14FinalValueNodeE,comdat
	.align 32
	.type	_ZTSN6icu_6717StringTrieBuilder14FinalValueNodeE, @object
	.size	_ZTSN6icu_6717StringTrieBuilder14FinalValueNodeE, 45
_ZTSN6icu_6717StringTrieBuilder14FinalValueNodeE:
	.string	"N6icu_6717StringTrieBuilder14FinalValueNodeE"
	.weak	_ZTIN6icu_6717StringTrieBuilder14FinalValueNodeE
	.section	.data.rel.ro._ZTIN6icu_6717StringTrieBuilder14FinalValueNodeE,"awG",@progbits,_ZTIN6icu_6717StringTrieBuilder14FinalValueNodeE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringTrieBuilder14FinalValueNodeE, @object
	.size	_ZTIN6icu_6717StringTrieBuilder14FinalValueNodeE, 24
_ZTIN6icu_6717StringTrieBuilder14FinalValueNodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringTrieBuilder14FinalValueNodeE
	.quad	_ZTIN6icu_6717StringTrieBuilder4NodeE
	.weak	_ZTSN6icu_6717StringTrieBuilder9ValueNodeE
	.section	.rodata._ZTSN6icu_6717StringTrieBuilder9ValueNodeE,"aG",@progbits,_ZTSN6icu_6717StringTrieBuilder9ValueNodeE,comdat
	.align 32
	.type	_ZTSN6icu_6717StringTrieBuilder9ValueNodeE, @object
	.size	_ZTSN6icu_6717StringTrieBuilder9ValueNodeE, 39
_ZTSN6icu_6717StringTrieBuilder9ValueNodeE:
	.string	"N6icu_6717StringTrieBuilder9ValueNodeE"
	.weak	_ZTIN6icu_6717StringTrieBuilder9ValueNodeE
	.section	.data.rel.ro._ZTIN6icu_6717StringTrieBuilder9ValueNodeE,"awG",@progbits,_ZTIN6icu_6717StringTrieBuilder9ValueNodeE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringTrieBuilder9ValueNodeE, @object
	.size	_ZTIN6icu_6717StringTrieBuilder9ValueNodeE, 24
_ZTIN6icu_6717StringTrieBuilder9ValueNodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringTrieBuilder9ValueNodeE
	.quad	_ZTIN6icu_6717StringTrieBuilder4NodeE
	.weak	_ZTSN6icu_6717StringTrieBuilder21IntermediateValueNodeE
	.section	.rodata._ZTSN6icu_6717StringTrieBuilder21IntermediateValueNodeE,"aG",@progbits,_ZTSN6icu_6717StringTrieBuilder21IntermediateValueNodeE,comdat
	.align 32
	.type	_ZTSN6icu_6717StringTrieBuilder21IntermediateValueNodeE, @object
	.size	_ZTSN6icu_6717StringTrieBuilder21IntermediateValueNodeE, 52
_ZTSN6icu_6717StringTrieBuilder21IntermediateValueNodeE:
	.string	"N6icu_6717StringTrieBuilder21IntermediateValueNodeE"
	.weak	_ZTIN6icu_6717StringTrieBuilder21IntermediateValueNodeE
	.section	.data.rel.ro._ZTIN6icu_6717StringTrieBuilder21IntermediateValueNodeE,"awG",@progbits,_ZTIN6icu_6717StringTrieBuilder21IntermediateValueNodeE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringTrieBuilder21IntermediateValueNodeE, @object
	.size	_ZTIN6icu_6717StringTrieBuilder21IntermediateValueNodeE, 24
_ZTIN6icu_6717StringTrieBuilder21IntermediateValueNodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringTrieBuilder21IntermediateValueNodeE
	.quad	_ZTIN6icu_6717StringTrieBuilder9ValueNodeE
	.weak	_ZTSN6icu_6717StringTrieBuilder15LinearMatchNodeE
	.section	.rodata._ZTSN6icu_6717StringTrieBuilder15LinearMatchNodeE,"aG",@progbits,_ZTSN6icu_6717StringTrieBuilder15LinearMatchNodeE,comdat
	.align 32
	.type	_ZTSN6icu_6717StringTrieBuilder15LinearMatchNodeE, @object
	.size	_ZTSN6icu_6717StringTrieBuilder15LinearMatchNodeE, 46
_ZTSN6icu_6717StringTrieBuilder15LinearMatchNodeE:
	.string	"N6icu_6717StringTrieBuilder15LinearMatchNodeE"
	.weak	_ZTIN6icu_6717StringTrieBuilder15LinearMatchNodeE
	.section	.data.rel.ro._ZTIN6icu_6717StringTrieBuilder15LinearMatchNodeE,"awG",@progbits,_ZTIN6icu_6717StringTrieBuilder15LinearMatchNodeE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringTrieBuilder15LinearMatchNodeE, @object
	.size	_ZTIN6icu_6717StringTrieBuilder15LinearMatchNodeE, 24
_ZTIN6icu_6717StringTrieBuilder15LinearMatchNodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringTrieBuilder15LinearMatchNodeE
	.quad	_ZTIN6icu_6717StringTrieBuilder9ValueNodeE
	.weak	_ZTSN6icu_6717StringTrieBuilder10BranchNodeE
	.section	.rodata._ZTSN6icu_6717StringTrieBuilder10BranchNodeE,"aG",@progbits,_ZTSN6icu_6717StringTrieBuilder10BranchNodeE,comdat
	.align 32
	.type	_ZTSN6icu_6717StringTrieBuilder10BranchNodeE, @object
	.size	_ZTSN6icu_6717StringTrieBuilder10BranchNodeE, 41
_ZTSN6icu_6717StringTrieBuilder10BranchNodeE:
	.string	"N6icu_6717StringTrieBuilder10BranchNodeE"
	.weak	_ZTIN6icu_6717StringTrieBuilder10BranchNodeE
	.section	.data.rel.ro._ZTIN6icu_6717StringTrieBuilder10BranchNodeE,"awG",@progbits,_ZTIN6icu_6717StringTrieBuilder10BranchNodeE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringTrieBuilder10BranchNodeE, @object
	.size	_ZTIN6icu_6717StringTrieBuilder10BranchNodeE, 24
_ZTIN6icu_6717StringTrieBuilder10BranchNodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringTrieBuilder10BranchNodeE
	.quad	_ZTIN6icu_6717StringTrieBuilder4NodeE
	.weak	_ZTSN6icu_6717StringTrieBuilder14ListBranchNodeE
	.section	.rodata._ZTSN6icu_6717StringTrieBuilder14ListBranchNodeE,"aG",@progbits,_ZTSN6icu_6717StringTrieBuilder14ListBranchNodeE,comdat
	.align 32
	.type	_ZTSN6icu_6717StringTrieBuilder14ListBranchNodeE, @object
	.size	_ZTSN6icu_6717StringTrieBuilder14ListBranchNodeE, 45
_ZTSN6icu_6717StringTrieBuilder14ListBranchNodeE:
	.string	"N6icu_6717StringTrieBuilder14ListBranchNodeE"
	.weak	_ZTIN6icu_6717StringTrieBuilder14ListBranchNodeE
	.section	.data.rel.ro._ZTIN6icu_6717StringTrieBuilder14ListBranchNodeE,"awG",@progbits,_ZTIN6icu_6717StringTrieBuilder14ListBranchNodeE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringTrieBuilder14ListBranchNodeE, @object
	.size	_ZTIN6icu_6717StringTrieBuilder14ListBranchNodeE, 24
_ZTIN6icu_6717StringTrieBuilder14ListBranchNodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringTrieBuilder14ListBranchNodeE
	.quad	_ZTIN6icu_6717StringTrieBuilder10BranchNodeE
	.weak	_ZTSN6icu_6717StringTrieBuilder15SplitBranchNodeE
	.section	.rodata._ZTSN6icu_6717StringTrieBuilder15SplitBranchNodeE,"aG",@progbits,_ZTSN6icu_6717StringTrieBuilder15SplitBranchNodeE,comdat
	.align 32
	.type	_ZTSN6icu_6717StringTrieBuilder15SplitBranchNodeE, @object
	.size	_ZTSN6icu_6717StringTrieBuilder15SplitBranchNodeE, 46
_ZTSN6icu_6717StringTrieBuilder15SplitBranchNodeE:
	.string	"N6icu_6717StringTrieBuilder15SplitBranchNodeE"
	.weak	_ZTIN6icu_6717StringTrieBuilder15SplitBranchNodeE
	.section	.data.rel.ro._ZTIN6icu_6717StringTrieBuilder15SplitBranchNodeE,"awG",@progbits,_ZTIN6icu_6717StringTrieBuilder15SplitBranchNodeE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringTrieBuilder15SplitBranchNodeE, @object
	.size	_ZTIN6icu_6717StringTrieBuilder15SplitBranchNodeE, 24
_ZTIN6icu_6717StringTrieBuilder15SplitBranchNodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringTrieBuilder15SplitBranchNodeE
	.quad	_ZTIN6icu_6717StringTrieBuilder10BranchNodeE
	.weak	_ZTSN6icu_6717StringTrieBuilder14BranchHeadNodeE
	.section	.rodata._ZTSN6icu_6717StringTrieBuilder14BranchHeadNodeE,"aG",@progbits,_ZTSN6icu_6717StringTrieBuilder14BranchHeadNodeE,comdat
	.align 32
	.type	_ZTSN6icu_6717StringTrieBuilder14BranchHeadNodeE, @object
	.size	_ZTSN6icu_6717StringTrieBuilder14BranchHeadNodeE, 45
_ZTSN6icu_6717StringTrieBuilder14BranchHeadNodeE:
	.string	"N6icu_6717StringTrieBuilder14BranchHeadNodeE"
	.weak	_ZTIN6icu_6717StringTrieBuilder14BranchHeadNodeE
	.section	.data.rel.ro._ZTIN6icu_6717StringTrieBuilder14BranchHeadNodeE,"awG",@progbits,_ZTIN6icu_6717StringTrieBuilder14BranchHeadNodeE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringTrieBuilder14BranchHeadNodeE, @object
	.size	_ZTIN6icu_6717StringTrieBuilder14BranchHeadNodeE, 24
_ZTIN6icu_6717StringTrieBuilder14BranchHeadNodeE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringTrieBuilder14BranchHeadNodeE
	.quad	_ZTIN6icu_6717StringTrieBuilder9ValueNodeE
	.weak	_ZTSN6icu_6717StringTrieBuilderE
	.section	.rodata._ZTSN6icu_6717StringTrieBuilderE,"aG",@progbits,_ZTSN6icu_6717StringTrieBuilderE,comdat
	.align 16
	.type	_ZTSN6icu_6717StringTrieBuilderE, @object
	.size	_ZTSN6icu_6717StringTrieBuilderE, 29
_ZTSN6icu_6717StringTrieBuilderE:
	.string	"N6icu_6717StringTrieBuilderE"
	.weak	_ZTIN6icu_6717StringTrieBuilderE
	.section	.data.rel.ro._ZTIN6icu_6717StringTrieBuilderE,"awG",@progbits,_ZTIN6icu_6717StringTrieBuilderE,comdat
	.align 8
	.type	_ZTIN6icu_6717StringTrieBuilderE, @object
	.size	_ZTIN6icu_6717StringTrieBuilderE, 24
_ZTIN6icu_6717StringTrieBuilderE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6717StringTrieBuilderE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6717StringTrieBuilderE
	.section	.data.rel.ro._ZTVN6icu_6717StringTrieBuilderE,"awG",@progbits,_ZTVN6icu_6717StringTrieBuilderE,comdat
	.align 8
	.type	_ZTVN6icu_6717StringTrieBuilderE, @object
	.size	_ZTVN6icu_6717StringTrieBuilderE, 176
_ZTVN6icu_6717StringTrieBuilderE:
	.quad	0
	.quad	_ZTIN6icu_6717StringTrieBuilderE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6717StringTrieBuilder4NodeE
	.section	.data.rel.ro._ZTVN6icu_6717StringTrieBuilder4NodeE,"awG",@progbits,_ZTVN6icu_6717StringTrieBuilder4NodeE,comdat
	.align 8
	.type	_ZTVN6icu_6717StringTrieBuilder4NodeE, @object
	.size	_ZTVN6icu_6717StringTrieBuilder4NodeE, 64
_ZTVN6icu_6717StringTrieBuilder4NodeE:
	.quad	0
	.quad	_ZTIN6icu_6717StringTrieBuilder4NodeE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringTrieBuilder4NodeeqERKS1_
	.quad	_ZN6icu_6717StringTrieBuilder4Node19markRightEdgesFirstEi
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6717StringTrieBuilder14FinalValueNodeE
	.section	.data.rel.ro._ZTVN6icu_6717StringTrieBuilder14FinalValueNodeE,"awG",@progbits,_ZTVN6icu_6717StringTrieBuilder14FinalValueNodeE,comdat
	.align 8
	.type	_ZTVN6icu_6717StringTrieBuilder14FinalValueNodeE, @object
	.size	_ZTVN6icu_6717StringTrieBuilder14FinalValueNodeE, 64
_ZTVN6icu_6717StringTrieBuilder14FinalValueNodeE:
	.quad	0
	.quad	_ZTIN6icu_6717StringTrieBuilder14FinalValueNodeE
	.quad	_ZN6icu_6717StringTrieBuilder14FinalValueNodeD1Ev
	.quad	_ZN6icu_6717StringTrieBuilder14FinalValueNodeD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringTrieBuilder14FinalValueNodeeqERKNS0_4NodeE
	.quad	_ZN6icu_6717StringTrieBuilder4Node19markRightEdgesFirstEi
	.quad	_ZN6icu_6717StringTrieBuilder14FinalValueNode5writeERS0_
	.weak	_ZTVN6icu_6717StringTrieBuilder9ValueNodeE
	.section	.data.rel.ro._ZTVN6icu_6717StringTrieBuilder9ValueNodeE,"awG",@progbits,_ZTVN6icu_6717StringTrieBuilder9ValueNodeE,comdat
	.align 8
	.type	_ZTVN6icu_6717StringTrieBuilder9ValueNodeE, @object
	.size	_ZTVN6icu_6717StringTrieBuilder9ValueNodeE, 64
_ZTVN6icu_6717StringTrieBuilder9ValueNodeE:
	.quad	0
	.quad	_ZTIN6icu_6717StringTrieBuilder9ValueNodeE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringTrieBuilder9ValueNodeeqERKNS0_4NodeE
	.quad	_ZN6icu_6717StringTrieBuilder4Node19markRightEdgesFirstEi
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6717StringTrieBuilder21IntermediateValueNodeE
	.section	.data.rel.ro._ZTVN6icu_6717StringTrieBuilder21IntermediateValueNodeE,"awG",@progbits,_ZTVN6icu_6717StringTrieBuilder21IntermediateValueNodeE,comdat
	.align 8
	.type	_ZTVN6icu_6717StringTrieBuilder21IntermediateValueNodeE, @object
	.size	_ZTVN6icu_6717StringTrieBuilder21IntermediateValueNodeE, 64
_ZTVN6icu_6717StringTrieBuilder21IntermediateValueNodeE:
	.quad	0
	.quad	_ZTIN6icu_6717StringTrieBuilder21IntermediateValueNodeE
	.quad	_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD1Ev
	.quad	_ZN6icu_6717StringTrieBuilder21IntermediateValueNodeD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringTrieBuilder21IntermediateValueNodeeqERKNS0_4NodeE
	.quad	_ZN6icu_6717StringTrieBuilder21IntermediateValueNode19markRightEdgesFirstEi
	.quad	_ZN6icu_6717StringTrieBuilder21IntermediateValueNode5writeERS0_
	.weak	_ZTVN6icu_6717StringTrieBuilder15LinearMatchNodeE
	.section	.data.rel.ro._ZTVN6icu_6717StringTrieBuilder15LinearMatchNodeE,"awG",@progbits,_ZTVN6icu_6717StringTrieBuilder15LinearMatchNodeE,comdat
	.align 8
	.type	_ZTVN6icu_6717StringTrieBuilder15LinearMatchNodeE, @object
	.size	_ZTVN6icu_6717StringTrieBuilder15LinearMatchNodeE, 64
_ZTVN6icu_6717StringTrieBuilder15LinearMatchNodeE:
	.quad	0
	.quad	_ZTIN6icu_6717StringTrieBuilder15LinearMatchNodeE
	.quad	0
	.quad	0
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringTrieBuilder15LinearMatchNodeeqERKNS0_4NodeE
	.quad	_ZN6icu_6717StringTrieBuilder15LinearMatchNode19markRightEdgesFirstEi
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6icu_6717StringTrieBuilder14ListBranchNodeE
	.section	.data.rel.ro._ZTVN6icu_6717StringTrieBuilder14ListBranchNodeE,"awG",@progbits,_ZTVN6icu_6717StringTrieBuilder14ListBranchNodeE,comdat
	.align 8
	.type	_ZTVN6icu_6717StringTrieBuilder14ListBranchNodeE, @object
	.size	_ZTVN6icu_6717StringTrieBuilder14ListBranchNodeE, 64
_ZTVN6icu_6717StringTrieBuilder14ListBranchNodeE:
	.quad	0
	.quad	_ZTIN6icu_6717StringTrieBuilder14ListBranchNodeE
	.quad	_ZN6icu_6717StringTrieBuilder14ListBranchNodeD1Ev
	.quad	_ZN6icu_6717StringTrieBuilder14ListBranchNodeD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringTrieBuilder14ListBranchNodeeqERKNS0_4NodeE
	.quad	_ZN6icu_6717StringTrieBuilder14ListBranchNode19markRightEdgesFirstEi
	.quad	_ZN6icu_6717StringTrieBuilder14ListBranchNode5writeERS0_
	.weak	_ZTVN6icu_6717StringTrieBuilder15SplitBranchNodeE
	.section	.data.rel.ro._ZTVN6icu_6717StringTrieBuilder15SplitBranchNodeE,"awG",@progbits,_ZTVN6icu_6717StringTrieBuilder15SplitBranchNodeE,comdat
	.align 8
	.type	_ZTVN6icu_6717StringTrieBuilder15SplitBranchNodeE, @object
	.size	_ZTVN6icu_6717StringTrieBuilder15SplitBranchNodeE, 64
_ZTVN6icu_6717StringTrieBuilder15SplitBranchNodeE:
	.quad	0
	.quad	_ZTIN6icu_6717StringTrieBuilder15SplitBranchNodeE
	.quad	_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD1Ev
	.quad	_ZN6icu_6717StringTrieBuilder15SplitBranchNodeD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringTrieBuilder15SplitBranchNodeeqERKNS0_4NodeE
	.quad	_ZN6icu_6717StringTrieBuilder15SplitBranchNode19markRightEdgesFirstEi
	.quad	_ZN6icu_6717StringTrieBuilder15SplitBranchNode5writeERS0_
	.weak	_ZTVN6icu_6717StringTrieBuilder14BranchHeadNodeE
	.section	.data.rel.ro._ZTVN6icu_6717StringTrieBuilder14BranchHeadNodeE,"awG",@progbits,_ZTVN6icu_6717StringTrieBuilder14BranchHeadNodeE,comdat
	.align 8
	.type	_ZTVN6icu_6717StringTrieBuilder14BranchHeadNodeE, @object
	.size	_ZTVN6icu_6717StringTrieBuilder14BranchHeadNodeE, 64
_ZTVN6icu_6717StringTrieBuilder14BranchHeadNodeE:
	.quad	0
	.quad	_ZTIN6icu_6717StringTrieBuilder14BranchHeadNodeE
	.quad	_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD1Ev
	.quad	_ZN6icu_6717StringTrieBuilder14BranchHeadNodeD0Ev
	.quad	_ZNK6icu_677UObject17getDynamicClassIDEv
	.quad	_ZNK6icu_6717StringTrieBuilder14BranchHeadNodeeqERKNS0_4NodeE
	.quad	_ZN6icu_6717StringTrieBuilder14BranchHeadNode19markRightEdgesFirstEi
	.quad	_ZN6icu_6717StringTrieBuilder14BranchHeadNode5writeERS0_
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
