	.file	"ubidiwrt.cpp"
	.text
	.p2align 4
	.type	_ZL14doWriteReversePKDsiPDsitP10UErrorCode, @function
_ZL14doWriteReversePKDsiPDsitP10UErrorCode:
.LFB2096:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r8d, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movl	%esi, -52(%rbp)
	andw	$11, %ax
	je	.L2
	cmpw	$1, %ax
	je	.L3
	movl	%esi, -56(%rbp)
	movl	%esi, %eax
	movl	%r8d, %esi
	andw	$8, %si
	movw	%si, -58(%rbp)
	jne	.L162
.L25:
	movl	%r8d, %eax
	movl	$448, %r13d
	andl	$1, %eax
	movw	%ax, -60(%rbp)
	movl	%r8d, %eax
	andl	$2, %eax
	movw	%ax, -62(%rbp)
	cmpl	%ecx, -56(%rbp)
	jle	.L56
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L166:
	movl	%r12d, %eax
	andl	$-4, %eax
	cmpl	$8204, %eax
	je	.L46
	leal	-8234(%r12), %eax
	cmpl	$4, %eax
	jbe	.L46
	leal	-8294(%r12), %eax
	cmpl	$3, %eax
	ja	.L45
.L46:
	testl	%r14d, %r14d
	jle	.L1
.L66:
	movl	%r14d, -52(%rbp)
.L56:
	movl	-52(%rbp), %eax
	leal	-1(%rax), %r14d
	movslq	%r14d, %rax
	movzwl	(%r15,%rax,2), %r12d
	leaq	(%rax,%rax), %rsi
	movl	%r12d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L38
	testl	%r14d, %r14d
	jg	.L164
.L38:
	cmpw	$0, -60(%rbp)
	jne	.L165
.L40:
	cmpw	$0, -58(%rbp)
	jne	.L166
.L45:
	cmpw	$0, -62(%rbp)
	movl	%r14d, %esi
	jne	.L167
.L47:
	movl	-52(%rbp), %eax
	cmpl	%eax, %esi
	jge	.L46
	movl	%eax, %r10d
	movslq	%esi, %rax
	leaq	16(%rbx), %rdi
	leaq	(%r15,%rax,2), %r8
	subl	%esi, %r10d
	cmpq	%rdi, %r8
	leaq	16(%r15,%rax,2), %rdi
	leal	-1(%r10), %r9d
	setnb	%r11b
	cmpq	%rdi, %rbx
	setnb	%dil
	orb	%dil, %r11b
	je	.L50
	cmpl	$6, %r9d
	jbe	.L50
	movl	%r10d, %edi
	xorl	%eax, %eax
	shrl	$3, %edi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L52:
	movdqu	(%r8,%rax), %xmm4
	movups	%xmm4, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdi, %rax
	jne	.L52
	movl	%r10d, %edi
	andl	$-8, %edi
	movl	%edi, %eax
	addl	%edi, %esi
	leaq	(%rbx,%rax,2), %rax
	cmpl	%r10d, %edi
	je	.L54
	movslq	%esi, %r8
	movl	-52(%rbp), %edx
	leal	1(%rsi), %edi
	movzwl	(%r15,%r8,2), %r8d
	movw	%r8w, (%rax)
	cmpl	%edx, %edi
	jge	.L54
	movslq	%edi, %rdi
	leaq	(%rdi,%rdi), %r8
	movzwl	(%r15,%rdi,2), %edi
	movw	%di, 2(%rax)
	leal	2(%rsi), %edi
	cmpl	%edi, %edx
	jle	.L54
	movzwl	2(%r15,%r8), %edi
	movw	%di, 4(%rax)
	leal	3(%rsi), %edi
	cmpl	%edi, %edx
	jle	.L54
	movzwl	4(%r15,%r8), %edi
	movw	%di, 6(%rax)
	leal	4(%rsi), %edi
	cmpl	%edx, %edi
	jge	.L54
	movzwl	6(%r15,%r8), %edi
	movw	%di, 8(%rax)
	leal	5(%rsi), %edi
	cmpl	%edx, %edi
	jge	.L54
	movzwl	8(%r15,%r8), %edi
	addl	$6, %esi
	movw	%di, 10(%rax)
	cmpl	%esi, %edx
	jle	.L54
	movzwl	10(%r15,%r8), %esi
	movw	%si, 12(%rax)
.L54:
	leaq	2(%rbx,%r9,2), %rbx
	testl	%r14d, %r14d
	jg	.L66
.L1:
	movl	-56(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	testl	%r14d, %r14d
	jle	.L40
.L41:
	movl	%r12d, %edi
	movq	%rbx, -72(%rbp)
	movl	%r14d, %ebx
	call	u_charType_67@PLT
	btq	%rax, %r13
	jnc	.L159
	.p2align 4,,10
	.p2align 3
.L169:
	subl	$1, %r14d
	movslq	%r14d, %rax
	movzwl	(%r15,%rax,2), %r12d
	leaq	(%rax,%rax), %rcx
	movl	%r12d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L168
.L43:
	testl	%r14d, %r14d
	je	.L159
.L44:
	movl	%r12d, %edi
	movl	%r14d, %ebx
	call	u_charType_67@PLT
	btq	%rax, %r13
	jc	.L169
	.p2align 4,,10
	.p2align 3
.L159:
	movq	-72(%rbp), %rbx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L168:
	testl	%r14d, %r14d
	je	.L159
	movzwl	-2(%r15,%rcx), %eax
	movl	%eax, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	jne	.L44
	sall	$10, %eax
	leal	-2(%rbx), %r14d
	leal	-56613888(%r12,%rax), %r12d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L164:
	movzwl	-2(%r15,%rsi), %eax
	movl	%eax, %esi
	andl	$-1024, %esi
	cmpl	$55296, %esi
	je	.L170
	cmpw	$0, -60(%rbp)
	je	.L40
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$1, %r12d
	movl	$2, %r11d
	cmpl	%esi, %ecx
	jl	.L13
	.p2align 4,,10
	.p2align 3
.L5:
	leal	-1(%rsi), %edx
	movslq	%edx, %rcx
	movzwl	(%r15,%rcx,2), %eax
	leaq	(%rcx,%rcx), %rdi
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L7
	testl	%edx, %edx
	jg	.L171
.L7:
	leaq	16(%r15,%rcx,2), %r10
	movl	%esi, %r9d
	leaq	(%r15,%rcx,2), %rdi
	movq	%rcx, %rax
	subl	%edx, %r9d
	cmpq	%r10, %rbx
	leaq	16(%rbx), %r10
	setnb	%r13b
	cmpq	%r10, %rdi
	leal	-1(%r9), %r8d
	setnb	%r10b
	orb	%r10b, %r13b
	je	.L8
	cmpl	$6, %r8d
	seta	%r13b
	cmpl	%edx, %esi
	setg	%r10b
	testb	%r10b, %r13b
	je	.L8
	cmpl	%edx, %esi
	cmovle	%r12d, %r9d
	xorl	%eax, %eax
	movl	%r9d, %ecx
	shrl	$3, %ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L9:
	movdqu	(%rdi,%rax), %xmm6
	movups	%xmm6, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L9
	movl	%r9d, %edi
	andl	$-8, %edi
	movl	%edi, %eax
	leaq	(%rbx,%rax,2), %rcx
	leal	(%rdi,%rdx), %eax
	cmpl	%edi, %r9d
	je	.L11
	movslq	%eax, %r9
	leal	1(%rax), %edi
	movzwl	(%r15,%r9,2), %r9d
	movw	%r9w, (%rcx)
	cmpl	%edi, %esi
	jle	.L11
	movslq	%edi, %rdi
	leaq	(%rdi,%rdi), %r9
	movzwl	(%r15,%rdi,2), %edi
	movw	%di, 2(%rcx)
	leal	2(%rax), %edi
	cmpl	%edi, %esi
	jle	.L11
	movzwl	2(%r15,%r9), %edi
	movw	%di, 4(%rcx)
	leal	3(%rax), %edi
	cmpl	%edi, %esi
	jle	.L11
	movzwl	4(%r15,%r9), %edi
	movw	%di, 6(%rcx)
	leal	4(%rax), %edi
	cmpl	%edi, %esi
	jle	.L11
	movzwl	6(%r15,%r9), %edi
	movw	%di, 8(%rcx)
	leal	5(%rax), %edi
	cmpl	%edi, %esi
	jle	.L11
	movzwl	8(%r15,%r9), %edi
	addl	$6, %eax
	movw	%di, 10(%rcx)
	cmpl	%eax, %esi
	jle	.L11
	movzwl	10(%r15,%r9), %eax
	movw	%ax, 12(%rcx)
.L11:
	cmpl	%edx, %esi
	leaq	2(%r8,%r8), %rax
	cmovle	%r11, %rax
	addq	%rax, %rbx
	testl	%edx, %edx
	jle	.L23
	movl	%edx, %esi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L23:
	movl	-52(%rbp), %eax
	movl	%eax, -56(%rbp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L171:
	movzwl	-2(%r15,%rdi), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L7
	leal	-2(%rsi), %edx
	movslq	%edx, %rcx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	negq	%rcx
	leaq	(%rbx,%rcx,2), %rdi
	.p2align 4,,10
	.p2align 3
.L12:
	movzwl	(%r15,%rax,2), %ecx
	movw	%cx, (%rdi,%rax,2)
	addq	$1, %rax
	cmpl	%eax, %esi
	jg	.L12
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	%esi, %ecx
	jl	.L13
	movl	%esi, %r12d
	movl	$448, %r13d
	.p2align 4,,10
	.p2align 3
.L24:
	movl	%r12d, %r14d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L59:
	movl	%eax, %r14d
.L14:
	testl	%r14d, %r14d
	jle	.L61
	call	u_charType_67@PLT
	btq	%rax, %r13
	jnc	.L61
.L17:
	leal	-1(%r14), %eax
	movslq	%eax, %rdx
	movzwl	(%r15,%rdx,2), %edi
	leaq	(%rdx,%rdx), %rsi
	movl	%edi, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L59
	testl	%eax, %eax
	jle	.L15
	movzwl	-2(%r15,%rsi), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$55296, %ecx
	je	.L172
	movl	%eax, %r14d
	call	u_charType_67@PLT
	btq	%rax, %r13
	jc	.L17
	.p2align 4,,10
	.p2align 3
.L61:
	movslq	%r14d, %rdx
	movq	%rdx, %rax
.L15:
	leaq	16(%r15,%rdx,2), %r9
	movl	%r12d, %r8d
	leaq	(%r15,%rdx,2), %rsi
	movq	%rdx, %rcx
	subl	%eax, %r8d
	cmpq	%r9, %rbx
	leaq	16(%rbx), %r9
	setnb	%r10b
	cmpq	%r9, %rsi
	leal	-1(%r8), %edi
	setnb	%r9b
	orb	%r9b, %r10b
	je	.L18
	cmpl	%eax, %r12d
	setg	%r10b
	cmpl	$6, %edi
	seta	%r9b
	testb	%r9b, %r10b
	je	.L18
	cmpl	%eax, %r12d
	movl	$1, %edx
	cmovle	%edx, %r8d
	xorl	%edx, %edx
	movl	%r8d, %ecx
	shrl	$3, %ecx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L19:
	movdqu	(%rsi,%rdx), %xmm7
	movups	%xmm7, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L19
	movl	%r8d, %esi
	andl	$-8, %esi
	movl	%esi, %edx
	leaq	(%rbx,%rdx,2), %rcx
	leal	(%rsi,%rax), %edx
	cmpl	%esi, %r8d
	je	.L21
	movslq	%edx, %r8
	leal	1(%rdx), %esi
	movzwl	(%r15,%r8,2), %r8d
	movw	%r8w, (%rcx)
	cmpl	%esi, %r12d
	jle	.L21
	movslq	%esi, %rsi
	leaq	(%rsi,%rsi), %r8
	movzwl	(%r15,%rsi,2), %esi
	movw	%si, 2(%rcx)
	leal	2(%rdx), %esi
	cmpl	%esi, %r12d
	jle	.L21
	movzwl	2(%r15,%r8), %esi
	movw	%si, 4(%rcx)
	leal	3(%rdx), %esi
	cmpl	%esi, %r12d
	jle	.L21
	movzwl	4(%r15,%r8), %esi
	movw	%si, 6(%rcx)
	leal	4(%rdx), %esi
	cmpl	%esi, %r12d
	jle	.L21
	movzwl	6(%r15,%r8), %esi
	movw	%si, 8(%rcx)
	leal	5(%rdx), %esi
	cmpl	%esi, %r12d
	jle	.L21
	movzwl	8(%r15,%r8), %esi
	addl	$6, %edx
	movw	%si, 10(%rcx)
	cmpl	%edx, %r12d
	jle	.L21
	movzwl	10(%r15,%r8), %edx
	movw	%dx, 12(%rcx)
.L21:
	cmpl	%eax, %r12d
	leaq	2(%rdi,%rdi), %rdx
	movl	$2, %ecx
	cmovle	%rcx, %rdx
	addq	%rdx, %rbx
	testl	%eax, %eax
	jle	.L23
	movl	%eax, %r12d
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L172:
	sall	$10, %edx
	subl	$2, %r14d
	leal	-56613888(%rdi,%rdx), %edi
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L18:
	negq	%rdx
	leaq	(%rbx,%rdx,2), %rsi
	.p2align 4,,10
	.p2align 3
.L22:
	movzwl	(%r15,%rcx,2), %edx
	movw	%dx, (%rsi,%rcx,2)
	addq	$1, %rcx
	cmpl	%ecx, %r12d
	jg	.L22
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L167:
	movl	%r12d, %edi
	call	u_charMirror_67@PLT
	cmpl	$65535, %eax
	jbe	.L173
	movl	%eax, %edi
	andw	$1023, %ax
	movl	$2, %esi
	orw	$-9216, %ax
	sarl	$10, %edi
	movw	%ax, 2(%rbx)
	subw	$10304, %di
	movl	$4, %eax
.L49:
	movw	%di, (%rbx)
	addl	%r14d, %esi
	addq	%rax, %rbx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L13:
	movl	-52(%rbp), %eax
	movl	$15, (%r9)
	movl	%eax, -56(%rbp)
	jmp	.L1
.L162:
	testl	%eax, %eax
	movl	$1, %edi
	cmovg	%eax, %edi
	cmpl	$7, %eax
	jle	.L63
	movl	%edi, %esi
	pxor	%xmm2, %xmm2
	movq	%r15, %rax
	movdqa	.LC2(%rip), %xmm3
	movdqa	.LC1(%rip), %xmm5
	shrl	$3, %esi
	movdqa	.LC4(%rip), %xmm4
	pxor	%xmm7, %xmm7
	salq	$4, %rsi
	movdqa	%xmm2, %xmm6
	movdqa	.LC0(%rip), %xmm11
	movdqa	.LC3(%rip), %xmm10
	movdqa	.LC5(%rip), %xmm9
	addq	%r15, %rsi
	psubd	%xmm3, %xmm5
	psubd	%xmm3, %xmm4
	movdqa	.LC6(%rip), %xmm8
	.p2align 4,,10
	.p2align 3
.L27:
	movdqu	(%rax), %xmm0
	movdqa	%xmm11, %xmm1
	movdqa	%xmm10, %xmm13
	addq	$16, %rax
	movdqa	%xmm0, %xmm12
	punpckhwd	%xmm7, %xmm0
	punpcklwd	%xmm7, %xmm12
	paddd	%xmm12, %xmm1
	paddd	%xmm12, %xmm13
	pand	%xmm9, %xmm12
	pcmpeqd	%xmm8, %xmm12
	psubd	%xmm3, %xmm1
	psubd	%xmm3, %xmm13
	pcmpgtd	%xmm5, %xmm1
	pcmpgtd	%xmm4, %xmm13
	pcmpeqd	%xmm6, %xmm12
	pand	%xmm13, %xmm1
	pand	%xmm12, %xmm1
	movdqa	%xmm10, %xmm12
	psubd	%xmm1, %xmm2
	movdqa	%xmm11, %xmm1
	paddd	%xmm0, %xmm12
	paddd	%xmm0, %xmm1
	pand	%xmm9, %xmm0
	psubd	%xmm3, %xmm12
	pcmpeqd	%xmm8, %xmm0
	pcmpgtd	%xmm4, %xmm12
	psubd	%xmm3, %xmm1
	pcmpgtd	%xmm5, %xmm1
	pcmpeqd	%xmm6, %xmm0
	pand	%xmm12, %xmm1
	pand	%xmm1, %xmm0
	psubd	%xmm0, %xmm2
	cmpq	%rsi, %rax
	jne	.L27
	movdqa	%xmm2, %xmm0
	movl	%edi, %r11d
	movl	-52(%rbp), %esi
	psrldq	$8, %xmm0
	andl	$-8, %r11d
	paddd	%xmm0, %xmm2
	movl	%r11d, %eax
	subl	%r11d, %esi
	movdqa	%xmm2, %xmm0
	leaq	(%r15,%rax,2), %rax
	psrldq	$4, %xmm0
	paddd	%xmm0, %xmm2
	movd	%xmm2, -56(%rbp)
	cmpl	%edi, %r11d
	je	.L28
.L26:
	movzwl	(%rax), %edi
	movl	%edi, %r11d
	andl	$-4, %r11d
	cmpl	$8204, %r11d
	je	.L29
	leal	-8234(%rdi), %r11d
	cmpl	$4, %r11d
	ja	.L174
.L29:
	leal	-1(%rsi), %edi
	testl	%edi, %edi
	jle	.L28
	movzwl	2(%rax), %edi
	movl	%edi, %r11d
	andl	$-4, %r11d
	cmpl	$8204, %r11d
	je	.L30
	leal	-8234(%rdi), %r11d
	cmpl	$4, %r11d
	ja	.L175
.L30:
	cmpl	$2, %esi
	je	.L28
	movzwl	4(%rax), %edi
	movl	%edi, %r11d
	andl	$-4, %r11d
	cmpl	$8204, %r11d
	je	.L31
	leal	-8234(%rdi), %r11d
	cmpl	$4, %r11d
	ja	.L176
.L31:
	cmpl	$3, %esi
	je	.L28
	movzwl	6(%rax), %edi
	movl	%edi, %r11d
	andl	$-4, %r11d
	cmpl	$8204, %r11d
	je	.L32
	leal	-8234(%rdi), %r11d
	cmpl	$4, %r11d
	ja	.L177
.L32:
	cmpl	$4, %esi
	je	.L28
	movzwl	8(%rax), %edi
	movl	%edi, %r11d
	andl	$-4, %r11d
	cmpl	$8204, %r11d
	je	.L33
	leal	-8234(%rdi), %r11d
	cmpl	$4, %r11d
	ja	.L178
.L33:
	cmpl	$5, %esi
	je	.L28
	movzwl	10(%rax), %edi
	movl	%edi, %r11d
	andl	$-4, %r11d
	cmpl	$8204, %r11d
	je	.L34
	leal	-8234(%rdi), %r11d
	cmpl	$4, %r11d
	ja	.L179
.L34:
	cmpl	$6, %esi
	je	.L28
	movzwl	12(%rax), %eax
	movl	%eax, %esi
	andl	$-4, %esi
	cmpl	$8204, %esi
	je	.L28
	leal	-8234(%rax), %esi
	cmpl	$4, %esi
	jbe	.L28
	subl	$8294, %eax
	cmpl	$4, %eax
	sbbl	$-1, -56(%rbp)
.L28:
	movl	-52(%rbp), %edx
	movl	$2, %esi
	leal	-1(%rdx), %eax
	testl	%edx, %edx
	leaq	2(%rax,%rax), %rax
	cmovle	%rsi, %rax
	movslq	%edx, %rsi
	addq	%rsi, %rsi
	subq	%rsi, %rax
	addq	%rax, %r15
	jmp	.L25
.L173:
	movl	%eax, %edi
	movl	$1, %esi
	movl	$2, %eax
	jmp	.L49
.L50:
	movq	%rax, %rsi
	movl	-52(%rbp), %ecx
	negq	%rsi
	leaq	(%rbx,%rsi,2), %rdi
	.p2align 4,,10
	.p2align 3
.L55:
	movzwl	(%r15,%rax,2), %edx
	movw	%dx, (%rdi,%rax,2)
	addq	$1, %rax
	cmpl	%eax, %ecx
	jg	.L55
	jmp	.L54
.L170:
	movl	-52(%rbp), %esi
	sall	$10, %eax
	leal	-56613888(%r12,%rax), %r12d
	leal	-2(%rsi), %r14d
	jmp	.L38
.L163:
	movl	$15, (%r9)
	jmp	.L1
.L174:
	subl	$8294, %edi
	cmpl	$4, %edi
	sbbl	$-1, -56(%rbp)
	jmp	.L29
.L175:
	subl	$8294, %edi
	cmpl	$4, %edi
	sbbl	$-1, -56(%rbp)
	jmp	.L30
.L176:
	subl	$8294, %edi
	cmpl	$4, %edi
	sbbl	$-1, -56(%rbp)
	jmp	.L31
.L177:
	subl	$8294, %edi
	cmpl	$4, %edi
	sbbl	$-1, -56(%rbp)
	jmp	.L32
.L178:
	subl	$8294, %edi
	cmpl	$4, %edi
	sbbl	$-1, -56(%rbp)
	jmp	.L33
.L179:
	subl	$8294, %edi
	cmpl	$4, %edi
	sbbl	$-1, -56(%rbp)
	jmp	.L34
.L63:
	movl	%eax, %esi
	movl	$0, -56(%rbp)
	movq	%r15, %rax
	jmp	.L26
	.cfi_endproc
.LFE2096:
	.size	_ZL14doWriteReversePKDsiPDsitP10UErrorCode, .-_ZL14doWriteReversePKDsiPDsitP10UErrorCode
	.p2align 4
	.type	_ZL14doWriteForwardPKDsiPDsitP10UErrorCode, @function
_ZL14doWriteForwardPKDsiPDsitP10UErrorCode:
.LFB2095:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	andl	$10, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	cmpw	$2, %r8w
	je	.L181
	cmpw	$8, %r8w
	je	.L231
	xorl	%r9d, %r9d
	testw	%r8w, %r8w
	je	.L353
	.p2align 4,,10
	.p2align 3
.L183:
	movzwl	(%r12), %edi
	movl	%edi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L234
	cmpl	$1, %ebx
	jne	.L354
.L234:
	movl	$2, %edx
	movl	$1, %eax
.L212:
	addq	%rdx, %r12
	movl	%edi, %edx
	subl	%eax, %ebx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L214
.L230:
	leal	-8234(%rdi), %edx
	cmpl	$4, %edx
	jbe	.L214
	leal	-8294(%rdi), %edx
	cmpl	$3, %edx
	jbe	.L214
	subl	%eax, %ecx
	js	.L355
	movl	%r9d, -56(%rbp)
	movl	%ecx, -52(%rbp)
	call	u_charMirror_67@PLT
	movslq	-56(%rbp), %rdx
	movl	-52(%rbp), %ecx
	movq	%rdx, %r9
	addq	%rdx, %rdx
	cmpl	$65535, %eax
	leaq	0(%r13,%rdx), %rdi
	leal	1(%r9), %esi
	ja	.L229
	movw	%ax, (%rdi)
	movl	%esi, %r9d
	.p2align 4,,10
	.p2align 3
.L214:
	testl	%ebx, %ebx
	jg	.L183
.L180:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movzwl	2(%r12), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L356
	addq	$2, %r12
	subl	$1, %ebx
	movl	$1, %eax
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L229:
	movl	%eax, %esi
	andw	$1023, %ax
	addl	$2, %r9d
	sarl	$10, %esi
	orw	$-9216, %ax
	subw	$10304, %si
	movw	%si, (%rdi)
	movw	%ax, 2(%r13,%rdx)
	testl	%ebx, %ebx
	jg	.L183
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L231:
	movl	%ecx, %esi
	.p2align 4,,10
	.p2align 3
.L182:
	movzwl	(%r12), %edx
	addq	$2, %r12
	movl	%edx, %ecx
	movl	%edx, %eax
	andl	$65532, %ecx
	cmpl	$8204, %ecx
	je	.L196
	leal	-8234(%rdx), %ecx
	cmpl	$4, %ecx
	jbe	.L196
	subl	$8294, %edx
	cmpl	$3, %edx
	jbe	.L196
	subl	$1, %esi
	js	.L357
	movw	%ax, 0(%r13)
	addq	$2, %r13
	.p2align 4,,10
	.p2align 3
.L196:
	subl	$1, %ebx
	testl	%ebx, %ebx
	jg	.L182
.L352:
	subl	%esi, %r14d
	movl	%r14d, %r9d
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L356:
	sall	$10, %edi
	movl	$4, %edx
	leal	-56613888(%rax,%rdi), %edi
	movl	$2, %eax
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L181:
	cmpl	%esi, %ecx
	jl	.L190
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L359:
	movw	%ax, (%rcx)
	movl	%esi, %r14d
.L193:
	cmpl	%ebx, %r15d
	jge	.L194
	movl	%r15d, %eax
.L195:
	movslq	%eax, %rdx
	leal	1(%rax), %r15d
	movzwl	(%r12,%rdx,2), %edi
	leaq	(%rdx,%rdx), %rcx
	movl	%edi, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L191
	cmpl	%r15d, %ebx
	jne	.L358
.L191:
	call	u_charMirror_67@PLT
	movslq	%r14d, %rdx
	leal	1(%r14), %esi
	addq	%rdx, %rdx
	leaq	0(%r13,%rdx), %rcx
	cmpl	$65535, %eax
	jbe	.L359
	movl	%eax, %esi
	andw	$1023, %ax
	addl	$2, %r14d
	sarl	$10, %esi
	orw	$-9216, %ax
	subw	$10304, %si
	movw	%si, (%rcx)
	movw	%ax, 2(%r13,%rdx)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L353:
	cmpl	%esi, %ecx
	jl	.L190
	leaq	15(%rdi), %rax
	subq	%rdx, %rax
	cmpq	$30, %rax
	jbe	.L233
	cmpl	$7, %esi
	jle	.L233
	testl	%esi, %esi
	movl	$1, %ecx
	cmovg	%esi, %ecx
	xorl	%eax, %eax
	movl	%ecx, %edx
	shrl	$3, %edx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L187:
	movdqu	(%r12,%rax), %xmm5
	movups	%xmm5, 0(%r13,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L187
	movl	%ecx, %edx
	andl	$-8, %edx
	movl	%edx, %eax
	addq	%rax, %rax
	addq	%rax, %r12
	addq	%rax, %r13
	movl	%ebx, %eax
	subl	%edx, %eax
	cmpl	%edx, %ecx
	je	.L194
	movzwl	(%r12), %edx
	movw	%dx, 0(%r13)
	leal	-1(%rax), %edx
	testl	%edx, %edx
	jle	.L194
	movzwl	2(%r12), %edx
	movw	%dx, 2(%r13)
	cmpl	$2, %eax
	je	.L194
	movzwl	4(%r12), %edx
	movw	%dx, 4(%r13)
	cmpl	$3, %eax
	je	.L194
	movzwl	6(%r12), %edx
	movw	%dx, 6(%r13)
	cmpl	$4, %eax
	je	.L194
	movzwl	8(%r12), %edx
	movw	%dx, 8(%r13)
	cmpl	$5, %eax
	je	.L194
	movzwl	10(%r12), %edx
	movw	%dx, 10(%r13)
	cmpl	$6, %eax
	je	.L194
	movzwl	12(%r12), %eax
	movw	%ax, 12(%r13)
	.p2align 4,,10
	.p2align 3
.L194:
	movl	%ebx, %r9d
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L190:
	movl	$15, (%r15)
	movl	%ebx, %r9d
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L358:
	movzwl	2(%r12,%rcx), %edx
	movl	%edx, %ecx
	andl	$-1024, %ecx
	cmpl	$56320, %ecx
	jne	.L191
	sall	$10, %edi
	leal	2(%rax), %r15d
	leal	-56613888(%rdx,%rdi), %edi
	jmp	.L191
.L357:
	leal	-1(%rbx), %ecx
	movl	$15, (%r15)
	testl	%ecx, %ecx
	jle	.L352
	subl	$2, %ebx
	movl	%ecx, %edi
	cmpl	$6, %ebx
	jbe	.L199
	movl	%ecx, %edx
	pxor	%xmm1, %xmm1
	movq	%r12, %rax
	movdqa	.LC2(%rip), %xmm2
	movdqa	.LC4(%rip), %xmm4
	shrl	$3, %edx
	movdqa	.LC1(%rip), %xmm3
	pxor	%xmm10, %xmm10
	salq	$4, %rdx
	movdqa	.LC0(%rip), %xmm7
	movdqa	.LC3(%rip), %xmm8
	movdqa	%xmm1, %xmm9
	movdqa	.LC5(%rip), %xmm6
	addq	%r12, %rdx
	psubd	%xmm2, %xmm4
	psubd	%xmm2, %xmm3
	movdqa	.LC6(%rip), %xmm5
	.p2align 4,,10
	.p2align 3
.L210:
	movdqu	(%rax), %xmm0
	movdqa	%xmm8, %xmm11
	movdqa	%xmm7, %xmm13
	addq	$16, %rax
	movdqa	%xmm0, %xmm12
	punpckhwd	%xmm10, %xmm0
	punpcklwd	%xmm10, %xmm12
	paddd	%xmm12, %xmm11
	paddd	%xmm12, %xmm13
	pand	%xmm6, %xmm12
	pcmpeqd	%xmm5, %xmm12
	psubd	%xmm2, %xmm11
	psubd	%xmm2, %xmm13
	pcmpgtd	%xmm4, %xmm11
	pcmpgtd	%xmm3, %xmm13
	pcmpeqd	%xmm9, %xmm12
	pand	%xmm13, %xmm11
	pand	%xmm12, %xmm11
	movdqa	%xmm7, %xmm12
	paddd	%xmm1, %xmm11
	movdqa	%xmm8, %xmm1
	paddd	%xmm0, %xmm12
	paddd	%xmm0, %xmm1
	pand	%xmm6, %xmm0
	psubd	%xmm2, %xmm12
	pcmpeqd	%xmm5, %xmm0
	pcmpgtd	%xmm3, %xmm12
	psubd	%xmm2, %xmm1
	pcmpgtd	%xmm4, %xmm1
	pcmpeqd	%xmm9, %xmm0
	pand	%xmm12, %xmm1
	pand	%xmm0, %xmm1
	paddd	%xmm11, %xmm1
	cmpq	%rdx, %rax
	jne	.L210
	movdqa	%xmm1, %xmm0
	psrldq	$8, %xmm0
	paddd	%xmm0, %xmm1
	movdqa	%xmm1, %xmm0
	psrldq	$4, %xmm0
	paddd	%xmm0, %xmm1
	movd	%xmm1, %eax
	addl	%eax, %esi
	movl	%ecx, %eax
	andl	$-8, %eax
	movl	%eax, %edx
	subl	%eax, %ecx
	leaq	(%r12,%rdx,2), %r12
	cmpl	%eax, %edi
	je	.L352
.L199:
	movzwl	(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L202
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L202
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %esi
.L202:
	leal	-1(%rcx), %eax
	testl	%eax, %eax
	jle	.L352
	movzwl	2(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L203
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L203
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %esi
.L203:
	cmpl	$2, %ecx
	je	.L352
	movzwl	4(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L204
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L204
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %esi
.L204:
	cmpl	$3, %ecx
	je	.L352
	movzwl	6(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L205
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L205
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %esi
.L205:
	cmpl	$4, %ecx
	je	.L352
	movzwl	8(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L206
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L206
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %esi
.L206:
	cmpl	$5, %ecx
	je	.L352
	movzwl	10(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L207
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L207
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %esi
.L207:
	cmpl	$6, %ecx
	je	.L352
	movzwl	12(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L352
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L352
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %esi
	jmp	.L352
.L355:
	movl	$15, (%r15)
	testl	%ebx, %ebx
	jle	.L216
	movl	%ebx, %esi
	cmpl	$7, %ebx
	jle	.L217
	movl	%ebx, %edx
	pxor	%xmm0, %xmm0
	movq	%r12, %rax
	movdqa	.LC2(%rip), %xmm2
	movdqa	.LC4(%rip), %xmm4
	shrl	$3, %edx
	movdqa	.LC1(%rip), %xmm3
	pxor	%xmm10, %xmm10
	salq	$4, %rdx
	movdqa	.LC0(%rip), %xmm7
	movdqa	.LC3(%rip), %xmm8
	movdqa	%xmm0, %xmm9
	movdqa	.LC5(%rip), %xmm6
	addq	%r12, %rdx
	psubd	%xmm2, %xmm4
	psubd	%xmm2, %xmm3
	movdqa	.LC6(%rip), %xmm5
	.p2align 4,,10
	.p2align 3
.L219:
	movdqu	(%rax), %xmm1
	movdqa	%xmm8, %xmm11
	movdqa	%xmm7, %xmm13
	addq	$16, %rax
	movdqa	%xmm1, %xmm12
	punpckhwd	%xmm10, %xmm1
	punpcklwd	%xmm10, %xmm12
	paddd	%xmm12, %xmm11
	paddd	%xmm12, %xmm13
	pand	%xmm6, %xmm12
	pcmpeqd	%xmm5, %xmm12
	psubd	%xmm2, %xmm11
	psubd	%xmm2, %xmm13
	pcmpgtd	%xmm4, %xmm11
	pcmpgtd	%xmm3, %xmm13
	pcmpeqd	%xmm9, %xmm12
	pand	%xmm13, %xmm11
	pand	%xmm12, %xmm11
	movdqa	%xmm8, %xmm12
	paddd	%xmm0, %xmm11
	movdqa	%xmm7, %xmm0
	paddd	%xmm1, %xmm12
	paddd	%xmm1, %xmm0
	pand	%xmm6, %xmm1
	psubd	%xmm2, %xmm12
	pcmpeqd	%xmm5, %xmm1
	pcmpgtd	%xmm4, %xmm12
	psubd	%xmm2, %xmm0
	pcmpgtd	%xmm3, %xmm0
	pcmpeqd	%xmm9, %xmm1
	pand	%xmm12, %xmm0
	pand	%xmm1, %xmm0
	paddd	%xmm11, %xmm0
	cmpq	%rdx, %rax
	jne	.L219
	movdqa	%xmm0, %xmm1
	psrldq	$8, %xmm1
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	addl	%eax, %ecx
	movl	%ebx, %eax
	andl	$-8, %eax
	movl	%eax, %edx
	subl	%eax, %ebx
	leaq	(%r12,%rdx,2), %r12
	cmpl	%esi, %eax
	je	.L216
.L217:
	movzwl	(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L221
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L221
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %ecx
.L221:
	cmpl	$1, %ebx
	je	.L216
	movzwl	2(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L222
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L222
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %ecx
.L222:
	cmpl	$2, %ebx
	je	.L216
	movzwl	4(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L223
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L223
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %ecx
.L223:
	cmpl	$3, %ebx
	je	.L216
	movzwl	6(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L224
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L224
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %ecx
.L224:
	cmpl	$4, %ebx
	je	.L216
	movzwl	8(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L225
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L225
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %ecx
.L225:
	cmpl	$5, %ebx
	je	.L216
	movzwl	10(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L226
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L226
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %ecx
.L226:
	cmpl	$6, %ebx
	je	.L216
	movzwl	12(%r12), %eax
	movl	%eax, %edx
	andl	$-4, %edx
	cmpl	$8204, %edx
	je	.L216
	leal	-8234(%rax), %edx
	cmpl	$4, %edx
	jbe	.L216
	subl	$8294, %eax
	cmpl	$4, %eax
	adcl	$-1, %ecx
.L216:
	subl	%ecx, %r14d
	movl	%r14d, %r9d
	jmp	.L180
.L233:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L186:
	movzwl	(%r12,%rax,2), %edx
	movw	%dx, 0(%r13,%rax,2)
	addq	$1, %rax
	movl	%ebx, %edx
	subl	%eax, %edx
	testl	%edx, %edx
	jg	.L186
	jmp	.L194
	.cfi_endproc
.LFE2095:
	.size	_ZL14doWriteForwardPKDsiPDsitP10UErrorCode, .-_ZL14doWriteForwardPKDsiPDsitP10UErrorCode
	.p2align 4
	.globl	ubidi_writeReverse_67
	.type	ubidi_writeReverse_67, @function
ubidi_writeReverse_67:
.LFB2097:
	.cfi_startproc
	endbr64
	testq	%r9, %r9
	je	.L383
	movl	(%r9), %eax
	testl	%eax, %eax
	jg	.L383
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$32, %rsp
	testq	%rdi, %rdi
	sete	%dl
	cmpl	$-1, %esi
	setl	%al
	orb	%al, %dl
	jne	.L362
	movl	%ecx, %r13d
	testl	%ecx, %ecx
	js	.L362
	jle	.L363
	testq	%r12, %r12
	je	.L362
.L381:
	cmpq	%r12, %rdi
	jnb	.L386
.L365:
	movslq	%esi, %rax
	leaq	(%rdi,%rax,2), %rax
	cmpq	%rax, %r12
	jb	.L362
.L364:
	cmpl	$-1, %esi
	je	.L387
.L366:
	xorl	%edx, %edx
	testl	%esi, %esi
	jle	.L367
	movq	%r12, %rdx
	movzwl	%r8w, %r8d
	movl	%r13d, %ecx
	movq	%r9, -24(%rbp)
	call	_ZL14doWriteReversePKDsiPDsitP10UErrorCode
	movq	-24(%rbp), %r9
	movl	%eax, %edx
.L367:
	addq	$32, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%r9, %rcx
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	u_terminateUChars_67@PLT
	.p2align 4,,10
	.p2align 3
.L383:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movslq	%r13d, %rax
	leaq	(%r12,%rax,2), %rax
	cmpq	%rax, %rdi
	jnb	.L388
.L362:
	movl	$1, (%r9)
	addq	$32, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L364
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L388:
	cmpq	%r12, %rdi
	jne	.L364
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%r9, -40(%rbp)
	movl	%r8d, -28(%rbp)
	movq	%rdi, -24(%rbp)
	call	u_strlen_67@PLT
	movq	-40(%rbp), %r9
	movl	-28(%rbp), %r8d
	movq	-24(%rbp), %rdi
	movl	%eax, %esi
	jmp	.L366
	.cfi_endproc
.LFE2097:
	.size	ubidi_writeReverse_67, .-ubidi_writeReverse_67
	.p2align 4
	.globl	ubidi_writeReordered_67
	.type	ubidi_writeReordered_67, @function
ubidi_writeReordered_67:
.LFB2098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -68(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L510
	movl	(%r8), %ebx
	movq	%r8, %r13
	testl	%ebx, %ebx
	jg	.L510
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L393
	movq	8(%rdi), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L393
	movl	20(%rdi), %eax
	movl	-68(%rbp), %edi
	testl	%edi, %edi
	js	.L393
	testl	%eax, %eax
	js	.L393
	movq	%rsi, %r12
	movl	%ecx, %r15d
	movl	%ecx, %edx
	testl	%edi, %edi
	jle	.L458
	testq	%rsi, %rsi
	jne	.L395
	.p2align 4,,10
	.p2align 3
.L393:
	movl	$1, 0(%r13)
.L510:
	xorl	%eax, %eax
.L389:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L516
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L396
.L395:
	movq	-80(%rbp), %rsi
	cmpq	%r12, %rsi
	jnb	.L517
.L397:
	movslq	16(%rbx), %rcx
	movq	-80(%rbp), %rsi
	leaq	(%rsi,%rcx,2), %rcx
	cmpq	%rcx, %r12
	jb	.L393
.L396:
	testl	%eax, %eax
	jne	.L398
	movl	-68(%rbp), %esi
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	u_terminateUChars_67@PLT
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	%edx, -88(%rbp)
	call	ubidi_countRuns_67@PLT
	movl	0(%r13), %r11d
	movl	%eax, -104(%rbp)
	testl	%r11d, %r11d
	jg	.L510
	movl	136(%rbx), %ecx
	andl	$-9, %r15d
	movl	-88(%rbp), %edx
	orl	$4, %r15d
	testb	$1, %cl
	cmovne	%r15d, %edx
	movl	%edx, %eax
	andl	$-5, %eax
	orl	$8, %eax
	andl	$2, %ecx
	cmovne	%eax, %edx
	movl	132(%rbx), %eax
	movl	%edx, %ecx
	subl	$3, %eax
	andl	$16, %ecx
	cmpl	$3, %eax
	ja	.L518
	movzwl	%dx, %eax
	movl	%eax, -72(%rbp)
	movl	%edx, %eax
	andl	$4, %eax
	testw	%cx, %cx
	jne	.L404
	testw	%ax, %ax
	je	.L402
	movq	112(%rbx), %rax
	movq	%rax, -112(%rbp)
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jle	.L456
	subl	$1, %eax
	xorl	%r11d, %r11d
	movq	%r13, -104(%rbp)
	movl	-68(%rbp), %r15d
	movq	%rax, -120(%rbp)
	leaq	-60(%rbp), %rax
	movq	%rbx, %r13
	movq	%r12, %r14
	movq	%rax, -88(%rbp)
	leaq	-64(%rbp), %rax
	movq	%r11, %rbx
	movq	%rax, -96(%rbp)
	movl	%edx, %eax
	andl	$65533, %eax
	movq	%r12, -136(%rbp)
	movl	%eax, -124(%rbp)
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L520:
	cmpb	$0, 128(%r13)
	je	.L412
	movq	-112(%rbp), %rax
	cmpb	$0, (%rax,%rcx)
	je	.L412
	orl	$1, %r12d
	movl	$8206, %eax
.L413:
	testl	%r15d, %r15d
	jle	.L415
	movw	%ax, (%r14)
	addq	$2, %r14
.L415:
	subl	$1, %r15d
.L414:
	movq	-104(%rbp), %r9
	movl	-124(%rbp), %r8d
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movl	-60(%rbp), %esi
	call	_ZL14doWriteForwardPKDsiPDsitP10UErrorCode
	movl	%eax, -60(%rbp)
	testq	%r14, %r14
	je	.L416
	movslq	%eax, %rdx
	leaq	(%r14,%rdx,2), %r14
.L416:
	subl	%eax, %r15d
	cmpb	$0, 128(%r13)
	je	.L426
	addl	-64(%rbp), %eax
	movq	-112(%rbp), %rsi
	cltq
	cmpb	$0, -1(%rsi,%rax)
	je	.L426
.L454:
	movl	$8206, %eax
.L427:
	testl	%r15d, %r15d
	jle	.L428
	movw	%ax, (%r14)
	addq	$2, %r14
.L428:
	subl	$1, %r15d
.L419:
	leaq	1(%rbx), %rax
	cmpq	%rbx, -120(%rbp)
	je	.L519
	movq	%rax, %rbx
.L429:
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movl	%ebx, %esi
	call	ubidi_getVisualRun_67@PLT
	movslq	-64(%rbp), %rcx
	movq	304(%r13), %r9
	movl	%eax, %r8d
	movq	-80(%rbp), %rax
	movq	%rcx, %rdx
	leaq	(%rax,%rcx,2), %rdi
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r9,%rax,4), %rax
	movl	8(%rax), %r12d
	movl	$0, %eax
	testl	%r12d, %r12d
	cmovs	%eax, %r12d
	testl	%r8d, %r8d
	je	.L520
	movl	%r12d, %ecx
	movl	-60(%rbp), %esi
	andl	$1, %ecx
	cmpb	$0, 128(%r13)
	je	.L421
	addl	%esi, %edx
	movq	-112(%rbp), %rax
	movslq	%edx, %rdx
	movzbl	-1(%rax,%rdx), %eax
	movl	$8194, %edx
	btq	%rax, %rdx
	jc	.L421
	movl	$8207, %eax
	orl	$4, %r12d
	subl	%ecx, %eax
.L422:
	testl	%r15d, %r15d
	jle	.L424
	movw	%ax, (%r14)
	addq	$2, %r14
.L424:
	subl	$1, %r15d
.L423:
	movq	-104(%rbp), %r9
	movl	-72(%rbp), %r8d
	movl	%r15d, %ecx
	movq	%r14, %rdx
	call	_ZL14doWriteReversePKDsiPDsitP10UErrorCode
	movl	%eax, -60(%rbp)
	testq	%r14, %r14
	je	.L425
	movslq	%eax, %rdx
	leaq	(%r14,%rdx,2), %r14
.L425:
	subl	%eax, %r15d
	cmpb	$0, 128(%r13)
	je	.L426
	movslq	-64(%rbp), %rax
	movq	-112(%rbp), %rdi
	movl	%r12d, %edx
	orl	$8, %edx
	movzbl	(%rdi,%rax), %ecx
	movl	$8194, %eax
	shrq	%cl, %rax
	testb	$1, %al
	cmove	%edx, %r12d
.L426:
	testb	$2, %r12b
	jne	.L454
	andl	$8, %r12d
	movl	$8207, %eax
	jne	.L427
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L517:
	movslq	-68(%rbp), %rcx
	leaq	(%r12,%rcx,2), %rcx
	cmpq	%rcx, %rsi
	jb	.L393
	cmpq	%r12, %rsi
	jne	.L396
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L518:
	andl	$-5, %edx
	movzwl	%dx, %eax
	movl	%eax, -72(%rbp)
	testw	%cx, %cx
	jne	.L521
.L402:
	movl	-104(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L456
	xorl	%r14d, %r14d
	movl	-68(%rbp), %eax
	leaq	-60(%rbp), %rsi
	leaq	-64(%rbp), %rcx
	movl	%r14d, %edi
	andl	$-3, %edx
	movq	%r12, -120(%rbp)
	movq	%r12, %r14
	movq	%rsi, -88(%rbp)
	movl	%eax, %r12d
	movl	%edi, %r15d
	movq	%rcx, -96(%rbp)
	movw	%dx, -112(%rbp)
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L523:
	movq	-80(%rbp), %rsi
	movzwl	-112(%rbp), %r8d
	movq	%r13, %r9
	movl	%r12d, %ecx
	movq	%r14, %rdx
	leaq	(%rsi,%rax,2), %rdi
	movl	-60(%rbp), %esi
	call	_ZL14doWriteForwardPKDsiPDsitP10UErrorCode
	movl	%eax, -60(%rbp)
.L408:
	testq	%r14, %r14
	je	.L409
	movslq	%eax, %rdx
	leaq	(%r14,%rdx,2), %r14
.L409:
	subl	%eax, %r12d
	addl	$1, %r15d
	cmpl	%r15d, -104(%rbp)
	je	.L522
.L410:
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	ubidi_getVisualRun_67@PLT
	testl	%eax, %eax
	movslq	-64(%rbp), %rax
	je	.L523
	movq	-80(%rbp), %rdi
	movl	-72(%rbp), %r8d
	movq	%r13, %r9
	movl	%r12d, %ecx
	movl	-60(%rbp), %esi
	movq	%r14, %rdx
	leaq	(%rdi,%rax,2), %rdi
	call	_ZL14doWriteReversePKDsiPDsitP10UErrorCode
	movl	%eax, -60(%rbp)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L421:
	movl	$8206, %eax
	testl	%ecx, %ecx
	jne	.L422
	testb	$4, %r12b
	je	.L423
	movl	$8207, %eax
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L412:
	testb	$1, %r12b
	jne	.L450
	movl	$8207, %eax
	testb	$4, %r12b
	jne	.L413
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L450:
	movl	$8206, %eax
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L404:
	movl	-104(%rbp), %r14d
	subl	$1, %r14d
	testw	%ax, %ax
	je	.L430
	movq	112(%rbx), %rax
	movq	%rax, %rcx
	testl	%r14d, %r14d
	js	.L456
	leaq	-60(%rbp), %rax
	movq	%rbx, -104(%rbp)
	movl	-68(%rbp), %r15d
	movq	%r12, %rbx
	movq	%rax, -88(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	%edx, %eax
	andl	$65533, %eax
	movq	%r12, -120(%rbp)
	movq	%rcx, %r12
	movl	%eax, -112(%rbp)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L525:
	movl	-60(%rbp), %esi
	addl	%esi, %eax
	cltq
	cmpb	$0, -1(%r12,%rax)
	je	.L438
	testl	%r15d, %r15d
	jle	.L439
	movl	$8206, %r8d
	addq	$2, %rbx
	movw	%r8w, -2(%rbx)
.L439:
	subl	$1, %r15d
.L438:
	movl	-112(%rbp), %r8d
	movq	%r13, %r9
	movl	%r15d, %ecx
	movq	%rbx, %rdx
	call	_ZL14doWriteReversePKDsiPDsitP10UErrorCode
	movl	%eax, -60(%rbp)
	testq	%rbx, %rbx
	je	.L440
	movslq	%eax, %rdx
	leaq	(%rbx,%rdx,2), %rbx
.L440:
	subl	%eax, %r15d
	movslq	-64(%rbp), %rax
	cmpb	$0, (%r12,%rax)
	je	.L441
	testl	%r15d, %r15d
	jle	.L446
	movl	$8206, %ecx
	addq	$2, %rbx
	movw	%cx, -2(%rbx)
.L446:
	subl	$1, %r15d
.L441:
	subl	$1, %r14d
	cmpl	$-1, %r14d
	je	.L524
.L436:
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	%r14d, %esi
	movq	-104(%rbp), %rdi
	call	ubidi_getVisualRun_67@PLT
	movslq	-64(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movl	%eax, %r8d
	leaq	(%rcx,%rdx,2), %rdi
	movq	%rdx, %rax
	testl	%r8d, %r8d
	je	.L525
	movzbl	(%r12,%rdx), %eax
	movl	$8194, %esi
	btq	%rax, %rsi
	jc	.L443
	testl	%r15d, %r15d
	jle	.L444
	movl	$8207, %edx
	addq	$2, %rbx
	movw	%dx, -2(%rbx)
.L444:
	subl	$1, %r15d
.L443:
	movl	-72(%rbp), %r8d
	movl	-60(%rbp), %esi
	movq	%r13, %r9
	movl	%r15d, %ecx
	movq	%rbx, %rdx
	call	_ZL14doWriteForwardPKDsiPDsitP10UErrorCode
	movl	%eax, -60(%rbp)
	testq	%rbx, %rbx
	je	.L445
	movslq	%eax, %rdx
	leaq	(%rbx,%rdx,2), %rbx
.L445:
	subl	%eax, %r15d
	addl	-64(%rbp), %eax
	movl	$8194, %edi
	cltq
	movzbl	-1(%r12,%rax), %eax
	btq	%rax, %rdi
	jc	.L441
	testl	%r15d, %r15d
	jle	.L446
	movl	$8207, %eax
	addq	$2, %rbx
	movw	%ax, -2(%rbx)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L521:
	movl	-104(%rbp), %r14d
	subl	$1, %r14d
.L430:
	testl	%r14d, %r14d
	js	.L456
	leaq	-60(%rbp), %rsi
	movl	-68(%rbp), %eax
	leaq	-64(%rbp), %rcx
	andl	$-3, %edx
	movq	%rsi, -88(%rbp)
	movl	%r14d, %esi
	movq	%r12, %r14
	movq	%r12, -112(%rbp)
	movl	%esi, %r15d
	movl	%eax, %r12d
	movq	%rcx, -96(%rbp)
	movw	%dx, -104(%rbp)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-80(%rbp), %rsi
	movzwl	-104(%rbp), %r8d
	movq	%r13, %r9
	movl	%r12d, %ecx
	movq	%r14, %rdx
	leaq	(%rsi,%rax,2), %rdi
	movl	-60(%rbp), %esi
	call	_ZL14doWriteReversePKDsiPDsitP10UErrorCode
	movl	%eax, -60(%rbp)
.L433:
	testq	%r14, %r14
	je	.L434
	movslq	%eax, %rdx
	leaq	(%r14,%rdx,2), %r14
.L434:
	subl	$1, %r15d
	subl	%eax, %r12d
	cmpl	$-1, %r15d
	je	.L526
.L435:
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdx
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	ubidi_getVisualRun_67@PLT
	testl	%eax, %eax
	movslq	-64(%rbp), %rax
	je	.L527
	movq	-80(%rbp), %rdi
	movl	-72(%rbp), %r8d
	movq	%r13, %r9
	movl	%r12d, %ecx
	movl	-60(%rbp), %esi
	movq	%r14, %rdx
	leaq	(%rdi,%rax,2), %rdi
	call	_ZL14doWriteForwardPKDsiPDsitP10UErrorCode
	movl	%eax, -60(%rbp)
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L526:
	movl	-68(%rbp), %edx
	movl	%r12d, %eax
	movq	-112(%rbp), %r12
	subl	%eax, %edx
.L406:
	movl	-68(%rbp), %esi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	u_terminateUChars_67@PLT
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L524:
	movl	-68(%rbp), %edx
	movq	-120(%rbp), %r12
	subl	%r15d, %edx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L519:
	movl	-68(%rbp), %edx
	movq	-136(%rbp), %r12
	movq	-104(%rbp), %r13
	subl	%r15d, %edx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L522:
	movl	-68(%rbp), %edx
	movl	%r12d, %eax
	movq	-120(%rbp), %r12
	subl	%eax, %edx
	jmp	.L406
.L456:
	xorl	%edx, %edx
	jmp	.L406
.L516:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2098:
	.size	ubidi_writeReordered_67, .-ubidi_writeReordered_67
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	-8294
	.long	-8294
	.long	-8294
	.long	-8294
	.align 16
.LC1:
	.long	3
	.long	3
	.long	3
	.long	3
	.align 16
.LC2:
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.long	-2147483648
	.align 16
.LC3:
	.long	-8234
	.long	-8234
	.long	-8234
	.long	-8234
	.align 16
.LC4:
	.long	4
	.long	4
	.long	4
	.long	4
	.align 16
.LC5:
	.long	-4
	.long	-4
	.long	-4
	.long	-4
	.align 16
.LC6:
	.long	8204
	.long	8204
	.long	8204
	.long	8204
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
