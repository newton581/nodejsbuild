	.file	"brkiter.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713BreakIterator13getRuleStatusEv
	.type	_ZNK6icu_6713BreakIterator13getRuleStatusEv, @function
_ZNK6icu_6713BreakIterator13getRuleStatusEv:
.LFB3201:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3201:
	.size	_ZNK6icu_6713BreakIterator13getRuleStatusEv, .-_ZNK6icu_6713BreakIterator13getRuleStatusEv
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator16getRuleStatusVecEPiiR10UErrorCode
	.type	_ZN6icu_6713BreakIterator16getRuleStatusVecEPiiR10UErrorCode, @function
_ZN6icu_6713BreakIterator16getRuleStatusVecEPiiR10UErrorCode:
.LFB3202:
	.cfi_startproc
	endbr64
	movl	(%rcx), %edi
	xorl	%eax, %eax
	testl	%edi, %edi
	jg	.L3
	testl	%edx, %edx
	jle	.L7
	movl	$0, (%rsi)
	movl	$1, %eax
.L3:
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$15, (%rcx)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3202:
	.size	_ZN6icu_6713BreakIterator16getRuleStatusVecEPiiR10UErrorCode, .-_ZN6icu_6713BreakIterator16getRuleStatusVecEPiiR10UErrorCode
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev:
.LFB3628:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3628:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5Ei,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei:
.LFB3631:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	cmpl	$40, %esi
	jg	.L21
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L9
	cmpb	$0, 12(%rbx)
	jne	.L22
.L13:
	movq	%r13, (%rbx)
	movl	%r12d, 8(%rbx)
	movb	$1, 12(%rbx)
.L9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L13
	.cfi_endproc
.LFE3631:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1Ei,_ZN6icu_6715MaybeStackArrayIcLi40EEC2Ei
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EED2Ev,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, @function
_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev:
.LFB3634:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L25
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3634:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev, .-_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EED1Ev,_ZN6icu_6715MaybeStackArrayIcLi40EED2Ev
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5EOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_:
.LFB3637:
	.cfi_startproc
	endbr64
	movzbl	12(%rsi), %eax
	movq	(%rsi), %r8
	movslq	8(%rsi), %rdx
	movb	%al, 12(%rdi)
	leaq	13(%rsi), %rax
	movq	%r8, (%rdi)
	movl	%edx, 8(%rdi)
	cmpq	%rax, %r8
	je	.L28
	movq	%rax, (%rsi)
	movl	$40, 8(%rsi)
	movb	$0, 12(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	13(%rdi), %r9
	movq	%r8, %rsi
	movq	%r9, (%rdi)
	movq	%r9, %rdi
	jmp	memcpy@PLT
	.cfi_endproc
.LFE3637:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1EOS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2EOS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_:
.LFB3639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpb	$0, 12(%rdi)
	movq	%rsi, %rbx
	jne	.L34
.L30:
	movl	8(%rbx), %eax
	movl	%eax, 8(%r12)
	movzbl	12(%rbx), %eax
	movb	%al, 12(%r12)
	movq	(%rbx), %rsi
	leaq	13(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L35
	movq	%rsi, (%r12)
	movq	%rax, (%rbx)
	movq	%r12, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	(%rdi), %rdi
	call	uprv_free_67@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	13(%r12), %rdi
	movslq	8(%rbx), %rdx
	movq	%rdi, (%r12)
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3639:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSEOS1_
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv:
.LFB3640:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3640:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE11getCapacityEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv:
.LFB3641:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3641:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE8getAliasEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv:
.LFB3642:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	addq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE3642:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv, .-_ZNK6icu_6715MaybeStackArrayIcLi40EE13getArrayLimitEv
	.section	.text._ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3643:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3643:
	.size	_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZNK6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEixEl,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEixEl,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEixEl:
.LFB3644:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	addq	%rsi, %rax
	ret
	.cfi_endproc
.LFE3644:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEixEl, .-_ZN6icu_6715MaybeStackArrayIcLi40EEixEl
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci:
.LFB3645:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L51
	testl	%edx, %edx
	jle	.L51
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpb	$0, 12(%rdi)
	jne	.L54
.L43:
	movq	%rsi, (%rbx)
	movl	%edx, 8(%rbx)
	movb	$0, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movl	%edx, -28(%rbp)
	movq	%rsi, -24(%rbp)
	call	uprv_free_67@PLT
	movl	-28(%rbp), %edx
	movq	-24(%rbp), %rsi
	jmp	.L43
	.cfi_endproc
.LFE3645:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12aliasInsteadEPci
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii:
.LFB3646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L58
	movq	%rdi, %rbx
	movslq	%esi, %rdi
	movl	%esi, %r13d
	movl	%edx, %r12d
	call	uprv_malloc_67@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L58
	testl	%r12d, %r12d
	jg	.L65
	cmpb	$0, 12(%rbx)
	jne	.L66
.L60:
	movq	%r14, (%rbx)
	movq	%r14, %rax
	movl	%r13d, 8(%rbx)
	movb	$1, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	cmpl	%r12d, 8(%rbx)
	cmovle	8(%rbx), %r12d
	movq	%rax, %rdi
	movq	(%rbx), %rsi
	cmpl	%r12d, %r13d
	cmovle	%r13d, %r12d
	movslq	%r12d, %rdx
	call	memcpy@PLT
	cmpb	$0, 12(%rbx)
	je	.L60
.L66:
	movq	(%rbx), %rdi
	call	uprv_free_67@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L58:
	xorl	%r14d, %r14d
	popq	%rbx
	popq	%r12
	movq	%r14, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3646:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii, .-_ZN6icu_6715MaybeStackArrayIcLi40EE6resizeEii
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi:
.LFB3647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	cmpb	$0, 12(%rdi)
	movq	%rdi, %rbx
	je	.L68
	movq	(%rdi), %r8
.L69:
	leaq	13(%rbx), %rax
	movl	%r12d, 0(%r13)
	movq	%rax, (%rbx)
	movq	%r8, %rax
	movl	$40, 8(%rbx)
	movb	$0, 12(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L72
	movl	8(%rdi), %eax
	cmpl	%eax, %esi
	cmovg	%eax, %r12d
	movslq	%r12d, %r14
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L72
	movq	(%rbx), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3647:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi, .-_ZN6icu_6715MaybeStackArrayIcLi40EE13orphanOrCloneEiRi
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv:
.LFB3648:
	.cfi_startproc
	endbr64
	cmpb	$0, 12(%rdi)
	jne	.L79
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	movq	(%rdi), %rdi
	jmp	uprv_free_67@PLT
	.cfi_endproc
.LFE3648:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE12releaseArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, @function
_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv:
.LFB3649:
	.cfi_startproc
	endbr64
	leaq	13(%rdi), %rax
	movl	$40, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 12(%rdi)
	ret
	.cfi_endproc
.LFE3649:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv, .-_ZN6icu_6715MaybeStackArrayIcLi40EE17resetToStackArrayEv
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_:
.LFB3650:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3650:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEeqERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_:
.LFB3651:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3651:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEneERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEC5ERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_:
.LFB3653:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3653:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_
	.set	_ZN6icu_6715MaybeStackArrayIcLi40EEC1ERKS1_,_ZN6icu_6715MaybeStackArrayIcLi40EEC2ERKS1_
	.section	.text._ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,"axG",@progbits,_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.type	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, @function
_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_:
.LFB3655:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3655:
	.size	_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_, .-_ZN6icu_6715MaybeStackArrayIcLi40EEaSERKS1_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"icudt67l-brkitr"
.LC1:
	.string	"boundaries"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	.type	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode, @function
_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode:
.LFB3176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$824, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %r10d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-800(%rbp), %rax
	movl	$0, -324(%rbp)
	movq	%rax, -840(%rbp)
	leaq	-787(%rbp), %rax
	movq	%rax, -800(%rbp)
	movl	$0, -744(%rbp)
	movl	$40, -792(%rbp)
	movw	%r9w, -788(%rbp)
	testl	%r10d, %r10d
	jg	.L85
	leaq	-736(%rbp), %r15
	movq	%rdi, %r13
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%r15, %rdi
	leaq	-528(%rbp), %r12
	call	ures_initStackObject_67@PLT
	movq	%r12, %rdi
	call	ures_initStackObject_67@PLT
	movq	40(%r13), %rsi
	movq	%rbx, %rdx
	leaq	.LC0(%rip), %rdi
	call	ures_openNoDefault_67@PLT
	movl	(%rbx), %r8d
	movq	%rax, %r13
	testl	%r8d, %r8d
	jle	.L113
	movq	%r15, %r8
.L110:
	leaq	-324(%rbp), %r14
	leaq	-320(%rbp), %r15
.L87:
	movq	%r8, %rdi
	call	ures_close_67@PLT
	movq	%r12, %rdi
	call	ures_close_67@PLT
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdx
	leaq	.LC0(%rip), %rdi
	call	udata_open_67@PLT
	movl	(%rbx), %ecx
	movq	%rax, %r14
	testl	%ecx, %ecx
	jg	.L114
	movl	$640, %edi
	call	_ZN6icu_677UMemorynwEm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L93
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN6icu_6722RuleBasedBreakIteratorC1EP11UDataMemoryR10UErrorCode@PLT
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L111
	leaq	165(%r12), %rax
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, %xmm0
	leaq	8(%r12), %rax
	movq	-800(%rbp), %r14
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -816(%rbp)
	call	ures_getLocaleByType_67@PLT
	leaq	-816(%rbp), %rdi
	movq	%r14, %rdx
	movq	%rax, %rsi
	call	_ZN6icu_6711LocaleBased12setLocaleIDsEPKcS2_@PLT
.L111:
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L92
	movq	(%r12), %rax
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	*8(%rax)
.L92:
	cmpb	$0, -788(%rbp)
	je	.L85
	movq	-800(%rbp), %rdi
	call	uprv_free_67@PLT
.L85:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$824, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	%rbx, %rcx
	movq	%r15, %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	call	ures_getByKeyWithFallback_67@PLT
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -848(%rbp)
	call	ures_getByKeyWithFallback_67@PLT
	leaq	-820(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	ures_getString_67@PLT
	movq	-848(%rbp), %r8
	movq	%rax, %r9
	movslq	-820(%rbp), %rax
	cmpq	$255, %rax
	jbe	.L88
	movl	(%rbx), %edi
	leaq	-324(%rbp), %r14
	movl	$0, -820(%rbp)
	leaq	-320(%rbp), %r15
	testl	%edi, %edi
	jg	.L87
	movl	$15, (%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	ures_close_67@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L88:
	movl	(%rbx), %esi
	testl	%esi, %esi
	jg	.L110
	testq	%r9, %r9
	je	.L110
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, -848(%rbp)
	movq	%r9, -856(%rbp)
	call	ures_getLocaleInternal_67@PLT
	movq	-840(%rbp), %rdi
	movq	%rbx, %rcx
	movl	$-1, %edx
	movq	%rax, %rsi
	call	_ZN6icu_6710CharString6appendEPKciR10UErrorCode@PLT
	movq	-856(%rbp), %r9
	movl	$46, %esi
	movq	%r9, %rdi
	movq	%r9, -840(%rbp)
	call	u_strchr_67@PLT
	movq	-848(%rbp), %r8
	testq	%rax, %rax
	je	.L101
	movq	-840(%rbp), %r9
	movq	%rax, %r10
	leaq	2(%rax), %rdi
	movl	$4, %edx
	leaq	-324(%rbp), %r14
	movq	%r8, -864(%rbp)
	leaq	-320(%rbp), %r15
	subq	%r9, %r10
	movq	%r14, %rsi
	movq	%r9, -856(%rbp)
	sarq	%r10
	movq	%r10, -848(%rbp)
	movl	%r10d, -840(%rbp)
	call	u_UCharsToChars_67@PLT
	movq	-848(%rbp), %r10
	movq	-856(%rbp), %r9
	movq	%r15, %rsi
	movl	%r10d, %edx
	movq	%r9, %rdi
	call	u_UCharsToChars_67@PLT
	movslq	-840(%rbp), %rcx
	movq	-864(%rbp), %r8
.L90:
	movb	$0, -320(%rbp,%rcx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L101:
	xorl	%ecx, %ecx
	leaq	-324(%rbp), %r14
	leaq	-320(%rbp), %r15
	jmp	.L90
.L115:
	call	__stack_chk_fail@PLT
.L93:
	movq	%r13, %rdi
	call	ures_close_67@PLT
	movq	%r14, %rdi
	call	udata_close_67@PLT
	cmpl	$0, (%rbx)
	jg	.L92
	movl	$7, (%rbx)
	jmp	.L92
	.cfi_endproc
.LFE3176:
	.size	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode, .-_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	.section	.rodata.str1.1
.LC2:
	.string	"word"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode:
.LFB3177:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L120
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	leaq	.LC2(%rip), %rsi
	subq	$8, %rsp
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L124
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3177:
	.size	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode
	.section	.rodata.str1.1
.LC3:
	.string	"lb"
.LC4:
	.string	"strict"
.LC5:
	.string	"normal"
.LC6:
	.string	"loose"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator18createLineInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713BreakIterator18createLineInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713BreakIterator18createLineInstanceERKNS_6LocaleER10UErrorCode:
.LFB3178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L125
	leaq	-80(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	%rsi, %rbx
	movl	$32, %ecx
	leaq	-116(%rbp), %r8
	movq	%r13, %rdx
	movb	$0, -108(%rbp)
	movq	%rdi, %r12
	leaq	.LC3(%rip), %rsi
	movl	$1701734764, -112(%rbp)
	leaq	-112(%rbp), %r14
	movl	$0, -116(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	-116(%rbp), %esi
	testl	%esi, %esi
	jg	.L127
	testl	%eax, %eax
	jg	.L150
.L127:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	movl	$0, %edx
	cmovg	%rdx, %rax
.L125:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L151
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movl	$7, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L128
	movl	$7, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L152
.L128:
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
.L129:
	movl	(%rdi), %edx
	addq	$4, %rdi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L129
	movl	%eax, %edx
	movq	%r13, %rsi
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rdi), %rdx
	cmove	%rdx, %rdi
	movq	%r14, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	movl	$95, %ecx
	sbbq	$3, %rdi
	movw	%cx, (%rdi)
	addq	$1, %rdi
	subq	%rdi, %rdx
	addq	$32, %rdx
	call	__strcpy_chk@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L152:
	movl	$6, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L127
	jmp	.L128
.L151:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3178:
	.size	_ZN6icu_6713BreakIterator18createLineInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713BreakIterator18createLineInstanceERKNS_6LocaleER10UErrorCode
	.section	.rodata.str1.1
.LC7:
	.string	"grapheme"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode:
.LFB3179:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L157
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	leaq	.LC7(%rip), %rsi
	subq	$8, %rsp
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L161
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3179:
	.size	_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode
	.section	.rodata.str1.1
.LC8:
	.string	"sentence"
.LC9:
	.string	"ss"
.LC10:
	.string	"standard"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode:
.LFB3180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rsi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L163
	movq	%rsi, %rdx
	movq	%rdi, %r13
	leaq	-100(%rbp), %r15
	movq	%rsi, %rbx
	leaq	-96(%rbp), %r14
	leaq	.LC8(%rip), %rsi
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	pxor	%xmm0, %xmm0
	movq	%r15, %r8
	movq	%r14, %rdx
	movl	$32, %ecx
	movq	%r13, %rdi
	movq	%rax, %r12
	movaps	%xmm0, -96(%rbp)
	leaq	.LC9(%rip), %rsi
	movl	$0, -100(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	-100(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L164
	testl	%eax, %eax
	jg	.L174
.L164:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L163
.L162:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L175
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L174:
	movl	$9, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L164
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	-100(%rbp), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jg	.L164
	movq	(%rax), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	call	*40(%rax)
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	0(%r13), %rax
	call	*8(%rax)
	jmp	.L164
.L175:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3180:
	.size	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode
	.section	.rodata.str1.1
.LC11:
	.string	"title"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator19createTitleInstanceERKNS_6LocaleER10UErrorCode
	.type	_ZN6icu_6713BreakIterator19createTitleInstanceERKNS_6LocaleER10UErrorCode, @function
_ZN6icu_6713BreakIterator19createTitleInstanceERKNS_6LocaleER10UErrorCode:
.LFB3181:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	testl	%ecx, %ecx
	jg	.L180
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	leaq	.LC11(%rip), %rsi
	subq	$8, %rsp
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movl	(%rbx), %edx
	testl	%edx, %edx
	jg	.L184
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3181:
	.size	_ZN6icu_6713BreakIterator19createTitleInstanceERKNS_6LocaleER10UErrorCode, .-_ZN6icu_6713BreakIterator19createTitleInstanceERKNS_6LocaleER10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator19getAvailableLocalesERi
	.type	_ZN6icu_6713BreakIterator19getAvailableLocalesERi, @function
_ZN6icu_6713BreakIterator19getAvailableLocalesERi:
.LFB3182:
	.cfi_startproc
	endbr64
	jmp	_ZN6icu_676Locale19getAvailableLocalesERi@PLT
	.cfi_endproc
.LFE3182:
	.size	_ZN6icu_6713BreakIterator19getAvailableLocalesERi, .-_ZN6icu_6713BreakIterator19getAvailableLocalesERi
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIteratorC2Ev
	.type	_ZN6icu_6713BreakIteratorC2Ev, @function
_ZN6icu_6713BreakIteratorC2Ev:
.LFB3184:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713BreakIteratorE(%rip), %rax
	movb	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 165(%rdi)
	ret
	.cfi_endproc
.LFE3184:
	.size	_ZN6icu_6713BreakIteratorC2Ev, .-_ZN6icu_6713BreakIteratorC2Ev
	.globl	_ZN6icu_6713BreakIteratorC1Ev
	.set	_ZN6icu_6713BreakIteratorC1Ev,_ZN6icu_6713BreakIteratorC2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIteratorC2ERKS0_
	.type	_ZN6icu_6713BreakIteratorC2ERKS0_, @function
_ZN6icu_6713BreakIteratorC2ERKS0_:
.LFB3190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713BreakIteratorE(%rip), %rax
	movl	$157, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	8(%rsi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	8(%rdi), %rdi
	movq	%rax, -8(%rdi)
	call	strncpy@PLT
	leaq	165(%r12), %rsi
	movl	$157, %edx
	leaq	165(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	strncpy@PLT
	.cfi_endproc
.LFE3190:
	.size	_ZN6icu_6713BreakIteratorC2ERKS0_, .-_ZN6icu_6713BreakIteratorC2ERKS0_
	.globl	_ZN6icu_6713BreakIteratorC1ERKS0_
	.set	_ZN6icu_6713BreakIteratorC1ERKS0_,_ZN6icu_6713BreakIteratorC2ERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIteratoraSERKS0_
	.type	_ZN6icu_6713BreakIteratoraSERKS0_, @function
_ZN6icu_6713BreakIteratoraSERKS0_:
.LFB3192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpq	%rsi, %rdi
	je	.L190
	movq	%rsi, %rbx
	leaq	8(%rdi), %rdi
	leaq	8(%rsi), %rsi
	movl	$157, %edx
	call	strncpy@PLT
	leaq	165(%rbx), %rsi
	movl	$157, %edx
	leaq	165(%r12), %rdi
	call	strncpy@PLT
.L190:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3192:
	.size	_ZN6icu_6713BreakIteratoraSERKS0_, .-_ZN6icu_6713BreakIteratoraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIteratorD2Ev
	.type	_ZN6icu_6713BreakIteratorD2Ev, @function
_ZN6icu_6713BreakIteratorD2Ev:
.LFB3194:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6713BreakIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_677UObjectD2Ev@PLT
	.cfi_endproc
.LFE3194:
	.size	_ZN6icu_6713BreakIteratorD2Ev, .-_ZN6icu_6713BreakIteratorD2Ev
	.globl	_ZN6icu_6713BreakIteratorD1Ev
	.set	_ZN6icu_6713BreakIteratorD1Ev,_ZN6icu_6713BreakIteratorD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIteratorD0Ev
	.type	_ZN6icu_6713BreakIteratorD0Ev, @function
_ZN6icu_6713BreakIteratorD0Ev:
.LFB3196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6713BreakIteratorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_677UObjectD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE3196:
	.size	_ZN6icu_6713BreakIteratorD0Ev, .-_ZN6icu_6713BreakIteratorD0Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator14createInstanceERKNS_6LocaleEiR10UErrorCode
	.type	_ZN6icu_6713BreakIterator14createInstanceERKNS_6LocaleEiR10UErrorCode, @function
_ZN6icu_6713BreakIterator14createInstanceERKNS_6LocaleEiR10UErrorCode:
.LFB3197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L195
	movq	%rdx, %rbx
	cmpl	$4, %esi
	ja	.L197
	leaq	.L199(%rip), %rdx
	movl	%esi, %esi
	movq	%rdi, %r13
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L199:
	.long	.L203-.L199
	.long	.L202-.L199
	.long	.L201-.L199
	.long	.L200-.L199
	.long	.L198-.L199
	.text
.L198:
	movq	%rbx, %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movq	%rax, %r12
.L204:
	movl	(%rbx), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovg	%rax, %r12
.L195:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L235
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L200:
	.cfi_restore_state
	movq	%rbx, %rdx
	leaq	.LC8(%rip), %rsi
	leaq	-132(%rbp), %r15
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	leaq	-96(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%r15, %r8
	movl	$32, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %r12
	leaq	.LC9(%rip), %rsi
	movaps	%xmm0, -96(%rbp)
	movl	$0, -132(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	-132(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L204
	testl	%eax, %eax
	jle	.L204
	movl	$9, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L204
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	-132(%rbp), %edx
	movq	%rax, %r13
	testl	%edx, %edx
	jg	.L204
	movq	(%rax), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	call	*40(%rax)
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	0(%r13), %rax
	call	*8(%rax)
	jmp	.L204
.L202:
	movq	%rbx, %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movq	%rax, %r12
	jmp	.L204
.L203:
	movq	%rbx, %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movq	%rax, %r12
	jmp	.L204
.L201:
	leaq	-96(%rbp), %r12
	pxor	%xmm0, %xmm0
	movb	$0, -124(%rbp)
	leaq	-132(%rbp), %r8
	movl	$32, %ecx
	movq	%r12, %rdx
	movaps	%xmm0, -96(%rbp)
	leaq	-128(%rbp), %r14
	leaq	.LC3(%rip), %rsi
	movl	$1701734764, -128(%rbp)
	movl	$0, -132(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	-132(%rbp), %edi
	testl	%edi, %edi
	jg	.L205
	testl	%eax, %eax
	jle	.L205
	movl	$7, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L206
	movl	$7, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L206
	movl	$6, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r12, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L205
.L206:
	leaq	-128(%rbp), %r14
	movq	%r14, %rdi
.L207:
	movl	(%rdi), %edx
	addq	$4, %rdi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L207
	movl	%eax, %edx
	movl	$95, %esi
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rdi), %rdx
	cmove	%rdx, %rdi
	movq	%r14, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdi
	movw	%si, (%rdi)
	addq	$1, %rdi
	movq	%r12, %rsi
	subq	%rdi, %rdx
	addq	$32, %rdx
	call	__strcpy_chk@PLT
.L205:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movq	%rax, %r12
	jmp	.L204
.L197:
	movl	$1, (%rdx)
	jmp	.L195
.L235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3197:
	.size	_ZN6icu_6713BreakIterator14createInstanceERKNS_6LocaleEiR10UErrorCode, .-_ZN6icu_6713BreakIterator14createInstanceERKNS_6LocaleEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIterator12makeInstanceERKNS_6LocaleEiR10UErrorCode
	.type	_ZN6icu_6713BreakIterator12makeInstanceERKNS_6LocaleEiR10UErrorCode, @function
_ZN6icu_6713BreakIterator12makeInstanceERKNS_6LocaleEiR10UErrorCode:
.LFB3198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	(%rdx), %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jg	.L237
	movq	%rdx, %rbx
	cmpl	$4, %esi
	ja	.L238
	leaq	.L240(%rip), %rdx
	movl	%esi, %esi
	movq	%rdi, %r12
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L240:
	.long	.L244-.L240
	.long	.L243-.L240
	.long	.L242-.L240
	.long	.L241-.L240
	.long	.L239-.L240
	.text
.L239:
	movq	%rbx, %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L245:
	movl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L236
	.p2align 4,,10
	.p2align 3
.L237:
	xorl	%r13d, %r13d
.L236:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L275
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L241:
	.cfi_restore_state
	movq	%rbx, %rdx
	leaq	.LC8(%rip), %rsi
	leaq	-132(%rbp), %r15
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	leaq	-96(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%r15, %r8
	movl	$32, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	leaq	.LC9(%rip), %rsi
	movaps	%xmm0, -96(%rbp)
	movl	$0, -132(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	-132(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L245
	testl	%eax, %eax
	jle	.L245
	movl	$9, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L245
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN6icu_6728FilteredBreakIteratorBuilder14createInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	-132(%rbp), %edx
	movq	%rax, %r12
	testl	%edx, %edx
	jg	.L245
	movq	(%rax), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	call	*40(%rax)
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	(%r12), %rax
	call	*8(%rax)
	jmp	.L245
.L243:
	movq	%rbx, %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movq	%rax, %r13
	jmp	.L245
.L244:
	movq	%rbx, %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movq	%rax, %r13
	jmp	.L245
.L242:
	leaq	-96(%rbp), %r13
	pxor	%xmm0, %xmm0
	movb	$0, -124(%rbp)
	leaq	-132(%rbp), %r8
	movl	$32, %ecx
	movq	%r13, %rdx
	movaps	%xmm0, -96(%rbp)
	leaq	-128(%rbp), %r14
	leaq	.LC3(%rip), %rsi
	movl	$1701734764, -128(%rbp)
	movl	$0, -132(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK6icu_676Locale15getKeywordValueEPKcPciR10UErrorCode@PLT
	movl	-132(%rbp), %edi
	testl	%edi, %edi
	jg	.L246
	testl	%eax, %eax
	jg	.L276
.L246:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN6icu_6713BreakIterator13buildInstanceERKNS_6LocaleEPKcR10UErrorCode
	movq	%rax, %r13
	jmp	.L245
.L238:
	movl	$1, (%rdx)
	xorl	%r13d, %r13d
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L276:
	movl	$7, %ecx
	leaq	.LC4(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L247
	movl	$7, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L247
	movl	$6, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r13, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L246
.L247:
	leaq	-128(%rbp), %r14
	movq	%r14, %rdi
.L248:
	movl	(%rdi), %edx
	addq	$4, %rdi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L248
	movl	%eax, %edx
	movl	$95, %esi
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rdi), %rdx
	cmove	%rdx, %rdi
	movq	%r14, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdi
	movw	%si, (%rdi)
	addq	$1, %rdi
	movq	%r13, %rsi
	subq	%rdi, %rdx
	addq	$32, %rdx
	call	__strcpy_chk@PLT
	jmp	.L246
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3198:
	.size	_ZN6icu_6713BreakIterator12makeInstanceERKNS_6LocaleEiR10UErrorCode, .-_ZN6icu_6713BreakIterator12makeInstanceERKNS_6LocaleEiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713BreakIterator9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_6713BreakIterator9getLocaleE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_6713BreakIterator9getLocaleE18ULocDataLocaleTypeR10UErrorCode:
.LFB3199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	165(%rsi), %rax
	addq	$8, %rsi
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	leaq	-48(%rbp), %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZNK6icu_6711LocaleBased9getLocaleE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L280:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3199:
	.size	_ZNK6icu_6713BreakIterator9getLocaleE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_6713BreakIterator9getLocaleE18ULocDataLocaleTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6713BreakIterator11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode
	.type	_ZNK6icu_6713BreakIterator11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode, @function
_ZNK6icu_6713BreakIterator11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode:
.LFB3200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	165(%rdi), %rax
	addq	$8, %rdi
	movq	%rdi, %xmm1
	movq	%rax, %xmm0
	leaq	-32(%rbp), %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -32(%rbp)
	call	_ZNK6icu_6711LocaleBased11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L284
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L284:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3200:
	.size	_ZNK6icu_6713BreakIterator11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode, .-_ZNK6icu_6713BreakIterator11getLocaleIDE18ULocDataLocaleTypeR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6713BreakIteratorC2ERKNS_6LocaleES3_
	.type	_ZN6icu_6713BreakIteratorC2ERKNS_6LocaleES3_, @function
_ZN6icu_6713BreakIteratorC2ERKNS_6LocaleES3_:
.LFB3204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713BreakIteratorE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	165(%rdi), %rax
	addq	$8, %rdi
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	leaq	-32(%rbp), %rdi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -32(%rbp)
	call	_ZN6icu_6711LocaleBased12setLocaleIDsERKNS_6LocaleES3_@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L288
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L288:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3204:
	.size	_ZN6icu_6713BreakIteratorC2ERKNS_6LocaleES3_, .-_ZN6icu_6713BreakIteratorC2ERKNS_6LocaleES3_
	.globl	_ZN6icu_6713BreakIteratorC1ERKNS_6LocaleES3_
	.set	_ZN6icu_6713BreakIteratorC1ERKNS_6LocaleES3_,_ZN6icu_6713BreakIteratorC2ERKNS_6LocaleES3_
	.weak	_ZTSN6icu_6713BreakIteratorE
	.section	.rodata._ZTSN6icu_6713BreakIteratorE,"aG",@progbits,_ZTSN6icu_6713BreakIteratorE,comdat
	.align 16
	.type	_ZTSN6icu_6713BreakIteratorE, @object
	.size	_ZTSN6icu_6713BreakIteratorE, 25
_ZTSN6icu_6713BreakIteratorE:
	.string	"N6icu_6713BreakIteratorE"
	.weak	_ZTIN6icu_6713BreakIteratorE
	.section	.data.rel.ro._ZTIN6icu_6713BreakIteratorE,"awG",@progbits,_ZTIN6icu_6713BreakIteratorE,comdat
	.align 8
	.type	_ZTIN6icu_6713BreakIteratorE, @object
	.size	_ZTIN6icu_6713BreakIteratorE, 24
_ZTIN6icu_6713BreakIteratorE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN6icu_6713BreakIteratorE
	.quad	_ZTIN6icu_677UObjectE
	.weak	_ZTVN6icu_6713BreakIteratorE
	.section	.data.rel.ro._ZTVN6icu_6713BreakIteratorE,"awG",@progbits,_ZTVN6icu_6713BreakIteratorE,comdat
	.align 8
	.type	_ZTVN6icu_6713BreakIteratorE, @object
	.size	_ZTVN6icu_6713BreakIteratorE, 200
_ZTVN6icu_6713BreakIteratorE:
	.quad	0
	.quad	_ZTIN6icu_6713BreakIteratorE
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK6icu_6713BreakIterator13getRuleStatusEv
	.quad	_ZN6icu_6713BreakIterator16getRuleStatusVecEPiiR10UErrorCode
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
