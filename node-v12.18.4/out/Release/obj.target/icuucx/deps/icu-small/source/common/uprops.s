	.file	"uprops.cpp"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_120ulayout_isAcceptableEPvPKcS2_PK9UDataInfo, @function
_ZN12_GLOBAL__N_120ulayout_isAcceptableEPvPKcS2_PK9UDataInfo:
.LFB3144:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpw	$19, (%rcx)
	jbe	.L1
	cmpw	$0, 4(%rcx)
	jne	.L1
	cmpw	$24908, 8(%rcx)
	je	.L8
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpw	$28537, 10(%rcx)
	jne	.L1
	cmpb	$1, 12(%rcx)
	sete	%al
	ret
	.cfi_endproc
.LFE3144:
	.size	_ZN12_GLOBAL__N_120ulayout_isAcceptableEPvPKcS2_PK9UDataInfo, .-_ZN12_GLOBAL__N_120ulayout_isAcceptableEPvPKcS2_PK9UDataInfo
	.p2align 4
	.type	_ZL19isRegionalIndicatorRK14BinaryPropertyi9UProperty, @function
_ZL19isRegionalIndicatorRK14BinaryPropertyi9UProperty:
.LFB3163:
	.cfi_startproc
	endbr64
	subl	$127462, %esi
	cmpl	$25, %esi
	setbe	%al
	ret
	.cfi_endproc
.LFE3163:
	.size	_ZL19isRegionalIndicatorRK14BinaryPropertyi9UProperty, .-_ZL19isRegionalIndicatorRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty, @function
_ZL20getMaxValueFromShiftRK11IntProperty9UProperty:
.LFB3167:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE3167:
	.size	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty, .-_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.p2align 4
	.type	_ZL21changesWhenCasefoldedRK14BinaryPropertyi9UProperty, @function
_ZL21changesWhenCasefoldedRK14BinaryPropertyi9UProperty:
.LFB3155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-252(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$224, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movw	%dx, -232(%rbp)
	movq	%rax, -240(%rbp)
	movl	$0, -252(%rbp)
	call	_ZN6icu_6711Normalizer214getNFCInstanceER10UErrorCode@PLT
	movl	-252(%rbp), %ecx
	testl	%ecx, %ecx
	jg	.L42
	movq	%rax, %rdi
	movq	(%rax), %rax
	leaq	-240(%rbp), %r13
	movl	%r12d, %esi
	movq	%r13, %rdx
	call	*56(%rax)
	testb	%al, %al
	jne	.L43
	testl	%r12d, %r12d
	js	.L22
.L19:
	movl	%r12d, %edi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	ucase_toFullFolding_67@PLT
	notl	%eax
	movl	%eax, %r12d
	shrl	$31, %r12d
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$224, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movzwl	-232(%rbp), %eax
	testw	%ax, %ax
	js	.L15
	movswl	%ax, %r14d
	sarl	$5, %r14d
.L16:
	cmpl	$1, %r14d
	je	.L45
	cmpl	$2, %r14d
	jle	.L46
.L20:
	testw	%ax, %ax
	js	.L23
	movswl	%ax, %ecx
	sarl	$5, %ecx
.L24:
	testb	$17, %al
	jne	.L32
	leaq	-230(%rbp), %rdx
	testb	$2, %al
	cmove	-216(%rbp), %rdx
.L25:
	leaq	-176(%rbp), %r14
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	u_strFoldCase_67@PLT
	movl	%eax, %ecx
	movl	-252(%rbp), %eax
	testl	%eax, %eax
	jg	.L13
	movzwl	-232(%rbp), %eax
	testw	%ax, %ax
	js	.L27
	movswl	%ax, %esi
	sarl	$5, %esi
.L28:
	testb	$17, %al
	jne	.L35
	leaq	-230(%rbp), %rdi
	testb	$2, %al
	cmove	-216(%rbp), %rdi
.L29:
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	call	u_strCompare_67@PLT
	testl	%eax, %eax
	setne	%r12b
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	-240(%rbp), %r13
.L22:
	xorl	%r12d, %r12d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L15:
	movl	-228(%rbp), %r14d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L23:
	movl	-228(%rbp), %ecx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	-230(%rbp), %rdx
	testb	$2, %al
	movq	%rdx, %rax
	cmove	-216(%rbp), %rax
	movzwl	(%rax), %r12d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%edx, %edx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8char32AtEi@PLT
	movl	%eax, %r12d
	cmpl	$2, %r14d
	je	.L47
.L37:
	movzwl	-232(%rbp), %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	movl	-228(%rbp), %esi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L47:
	cmpl	$65535, %eax
	jle	.L37
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%edi, %edi
	jmp	.L29
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3155:
	.size	_ZL21changesWhenCasefoldedRK14BinaryPropertyi9UProperty, .-_ZL21changesWhenCasefoldedRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL14isPOSIX_xdigitRK14BinaryPropertyi9UProperty, @function
_ZL14isPOSIX_xdigitRK14BinaryPropertyi9UProperty:
.LFB3162:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	u_isxdigit_67@PLT
	.cfi_endproc
.LFE3162:
	.size	_ZL14isPOSIX_xdigitRK14BinaryPropertyi9UProperty, .-_ZL14isPOSIX_xdigitRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL13isPOSIX_printRK14BinaryPropertyi9UProperty, @function
_ZL13isPOSIX_printRK14BinaryPropertyi9UProperty:
.LFB3161:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	u_isprintPOSIX_67@PLT
	.cfi_endproc
.LFE3161:
	.size	_ZL13isPOSIX_printRK14BinaryPropertyi9UProperty, .-_ZL13isPOSIX_printRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL13isPOSIX_graphRK14BinaryPropertyi9UProperty, @function
_ZL13isPOSIX_graphRK14BinaryPropertyi9UProperty:
.LFB3160:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	u_isgraphPOSIX_67@PLT
	.cfi_endproc
.LFE3160:
	.size	_ZL13isPOSIX_graphRK14BinaryPropertyi9UProperty, .-_ZL13isPOSIX_graphRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL13isPOSIX_blankRK14BinaryPropertyi9UProperty, @function
_ZL13isPOSIX_blankRK14BinaryPropertyi9UProperty:
.LFB3159:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	u_isblank_67@PLT
	.cfi_endproc
.LFE3159:
	.size	_ZL13isPOSIX_blankRK14BinaryPropertyi9UProperty, .-_ZL13isPOSIX_blankRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL13isPOSIX_alnumRK14BinaryPropertyi9UProperty, @function
_ZL13isPOSIX_alnumRK14BinaryPropertyi9UProperty:
.LFB3158:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	u_isalnumPOSIX_67@PLT
	.cfi_endproc
.LFE3158:
	.size	_ZL13isPOSIX_alnumRK14BinaryPropertyi9UProperty, .-_ZL13isPOSIX_alnumRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL21isCanonSegmentStarterRK14BinaryPropertyi9UProperty, @function
_ZL21isCanonSegmentStarterRK14BinaryPropertyi9UProperty:
.LFB3157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-44(%rbp), %r14
	movl	%esi, %r13d
	pushq	%r12
	movq	%r14, %rdi
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jle	.L54
.L56:
	xorl	%eax, %eax
.L53:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L62
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK6icu_6715Normalizer2Impl19ensureCanonIterDataER10UErrorCode@PLT
	testb	%al, %al
	je	.L56
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZNK6icu_6715Normalizer2Impl21isCanonSegmentStarterEi@PLT
	testb	%al, %al
	setne	%al
	jmp	.L53
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3157:
	.size	_ZL21isCanonSegmentStarterRK14BinaryPropertyi9UProperty, .-_ZL21isCanonSegmentStarterRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL11isNormInertRK14BinaryPropertyi9UProperty, @function
_ZL11isNormInertRK14BinaryPropertyi9UProperty:
.LFB3154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-35(%rdx), %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	leaq	-28(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	call	_ZN6icu_6718Normalizer2Factory11getInstanceE18UNormalizationModeR10UErrorCode@PLT
	movl	-28(%rbp), %edx
	testl	%edx, %edx
	jle	.L69
	xorl	%eax, %eax
.L63:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L70
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movq	%rax, %rdi
	movq	(%rax), %rax
	movl	%r12d, %esi
	call	*136(%rax)
	testb	%al, %al
	setne	%al
	jmp	.L63
.L70:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3154:
	.size	_ZL11isNormInertRK14BinaryPropertyi9UProperty, .-_ZL11isNormInertRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty, @function
_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty:
.LFB3149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edi
	movl	%edx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	ucase_hasBinaryProperty_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3149:
	.size	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty, .-_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL13isJoinControlRK14BinaryPropertyi9UProperty, @function
_ZL13isJoinControlRK14BinaryPropertyi9UProperty:
.LFB3152:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	ubidi_isJoinControl_67@PLT
	.cfi_endproc
.LFE3152:
	.size	_ZL13isJoinControlRK14BinaryPropertyi9UProperty, .-_ZL13isJoinControlRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL10isMirroredRK14BinaryPropertyi9UProperty, @function
_ZL10isMirroredRK14BinaryPropertyi9UProperty:
.LFB3151:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	ubidi_isMirrored_67@PLT
	.cfi_endproc
.LFE3151:
	.size	_ZL10isMirroredRK14BinaryPropertyi9UProperty, .-_ZL10isMirroredRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL13isBidiControlRK14BinaryPropertyi9UProperty, @function
_ZL13isBidiControlRK14BinaryPropertyi9UProperty:
.LFB3150:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	ubidi_isBidiControl_67@PLT
	.cfi_endproc
.LFE3150:
	.size	_ZL13isBidiControlRK14BinaryPropertyi9UProperty, .-_ZL13isBidiControlRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL15defaultContainsRK14BinaryPropertyi9UProperty, @function
_ZL15defaultContainsRK14BinaryPropertyi9UProperty:
.LFB3148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$8, %rsp
	movl	(%rbx), %esi
	call	u_getUnicodeProperties_67@PLT
	testl	%eax, 4(%rbx)
	setne	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3148:
	.size	_ZL15defaultContainsRK14BinaryPropertyi9UProperty, .-_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL21getHangulSyllableTypeRK11IntPropertyi9UProperty, @function
_ZL21getHangulSyllableTypeRK11IntPropertyi9UProperty:
.LFB3178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edi
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	u_getUnicodeProperties_67@PLT
	xorl	%r8d, %r8d
	sarl	$5, %eax
	andl	$31, %eax
	cmpl	$9, %eax
	jg	.L78
	cltq
	leaq	_ZL8gcbToHst(%rip), %rdx
	movl	(%rdx,%rax,4), %r8d
.L78:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3178:
	.size	_ZL21getHangulSyllableTypeRK11IntPropertyi9UProperty, .-_ZL21getHangulSyllableTypeRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL15defaultGetValueRK11IntPropertyi9UProperty, @function
_ZL15defaultGetValueRK11IntPropertyi9UProperty:
.LFB3165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$8, %rsp
	movl	(%rbx), %esi
	call	u_getUnicodeProperties_67@PLT
	movl	8(%rbx), %ecx
	andl	4(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	sarl	%cl, %eax
	ret
	.cfi_endproc
.LFE3165:
	.size	_ZL15defaultGetValueRK11IntPropertyi9UProperty, .-_ZL15defaultGetValueRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL18getGeneralCategoryRK11IntPropertyi9UProperty, @function
_ZL18getGeneralCategoryRK11IntPropertyi9UProperty:
.LFB3172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	u_charType_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movsbl	%al, %eax
	ret
	.cfi_endproc
.LFE3172:
	.size	_ZL18getGeneralCategoryRK11IntPropertyi9UProperty, .-_ZL18getGeneralCategoryRK11IntPropertyi9UProperty
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ulayout"
.LC1:
	.string	"icu"
	.text
	.p2align 4
	.type	_ZN12_GLOBAL__N_112ulayout_loadER10UErrorCode, @function
_ZN12_GLOBAL__N_112ulayout_loadER10UErrorCode:
.LFB3145:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	leaq	.LC0(%rip), %rdx
	xorl	%r8d, %r8d
	leaq	_ZN12_GLOBAL__N_120ulayout_isAcceptableEPvPKcS2_PK9UDataInfo(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	xorl	%edi, %edi
	call	udata_openChoice_67@PLT
	movl	(%rbx), %edx
	movq	%rax, _ZN12_GLOBAL__N_113gLayoutMemoryE(%rip)
	testl	%edx, %edx
	jle	.L94
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	%rax, %rdi
	call	udata_getMemory_67@PLT
	movl	(%rax), %edx
	movq	%rax, %r12
	cmpl	$11, %edx
	jle	.L95
	movl	4(%rax), %r14d
	sall	$2, %edx
	movl	%r14d, %ecx
	subl	%edx, %ecx
	cmpl	$15, %ecx
	jg	.L96
.L90:
	movl	8(%r12), %r13d
	movl	%r13d, %ecx
	subl	%r14d, %ecx
	cmpl	$15, %ecx
	jg	.L97
.L91:
	movl	12(%r12), %ecx
	subl	%r13d, %ecx
	cmpl	$15, %ecx
	jg	.L98
.L92:
	movl	36(%r12), %eax
	popq	%rbx
	movl	$17, %edi
	leaq	_ZN12_GLOBAL__N_114uprops_cleanupEv(%rip), %rsi
	popq	%r12
	popq	%r13
	movl	%eax, %edx
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	shrl	$24, %edx
	movl	%edx, _ZN12_GLOBAL__N_113gMaxInpcValueE(%rip)
	movl	%eax, %edx
	movzbl	%ah, %eax
	shrl	$16, %edx
	movl	%eax, _ZN12_GLOBAL__N_111gMaxVoValueE(%rip)
	andl	$255, %edx
	movl	%edx, _ZN12_GLOBAL__N_113gMaxInscValueE(%rip)
	jmp	ucln_common_registerCleanup_67@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movslq	%r13d, %rdx
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movl	$-1, %esi
	addq	%r12, %rdx
	movl	$-1, %edi
	call	ucptrie_openFromBinary_67@PLT
	movq	%rax, _ZN12_GLOBAL__N_17gVoTrieE(%rip)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L97:
	movslq	%r14d, %rdx
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movl	$-1, %esi
	addq	%r12, %rdx
	movl	$-1, %edi
	call	ucptrie_openFromBinary_67@PLT
	movq	%rax, _ZN12_GLOBAL__N_19gInscTrieE(%rip)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L96:
	movslq	%edx, %rdx
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	movl	$-1, %esi
	addq	%rax, %rdx
	movl	$-1, %edi
	call	ucptrie_openFromBinary_67@PLT
	movq	%rax, _ZN12_GLOBAL__N_19gInpcTrieE(%rip)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$3, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3145:
	.size	_ZN12_GLOBAL__N_112ulayout_loadER10UErrorCode, .-_ZN12_GLOBAL__N_112ulayout_loadER10UErrorCode
	.p2align 4
	.type	_ZN12_GLOBAL__N_114uprops_cleanupEv, @function
_ZN12_GLOBAL__N_114uprops_cleanupEv:
.LFB3143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	_ZN12_GLOBAL__N_113gLayoutMemoryE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	udata_close_67@PLT
	movq	_ZN12_GLOBAL__N_19gInpcTrieE(%rip), %rdi
	movq	$0, _ZN12_GLOBAL__N_113gLayoutMemoryE(%rip)
	call	ucptrie_close_67@PLT
	movq	_ZN12_GLOBAL__N_19gInscTrieE(%rip), %rdi
	movq	$0, _ZN12_GLOBAL__N_19gInpcTrieE(%rip)
	call	ucptrie_close_67@PLT
	movq	_ZN12_GLOBAL__N_17gVoTrieE(%rip), %rdi
	movq	$0, _ZN12_GLOBAL__N_19gInscTrieE(%rip)
	call	ucptrie_close_67@PLT
	movl	$1, %eax
	movq	$0, _ZN12_GLOBAL__N_17gVoTrieE(%rip)
	movl	$0, _ZN12_GLOBAL__N_113gMaxInpcValueE(%rip)
	movl	$0, _ZN12_GLOBAL__N_113gMaxInscValueE(%rip)
	movl	$0, _ZN12_GLOBAL__N_111gMaxVoValueE(%rip)
	movl	$0, _ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip)
	mfence
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3143:
	.size	_ZN12_GLOBAL__N_114uprops_cleanupEv, .-_ZN12_GLOBAL__N_114uprops_cleanupEv
	.p2align 4
	.type	_ZL24getBiDiPairedBracketTypeRK11IntPropertyi9UProperty, @function
_ZL24getBiDiPairedBracketTypeRK11IntPropertyi9UProperty:
.LFB3169:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	ubidi_getPairedBracketType_67@PLT
	.cfi_endproc
.LFE3169:
	.size	_ZL24getBiDiPairedBracketTypeRK11IntPropertyi9UProperty, .-_ZL24getBiDiPairedBracketTypeRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL22getTrailCombiningClassRK11IntPropertyi9UProperty, @function
_ZL22getTrailCombiningClassRK11IntPropertyi9UProperty:
.LFB3181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	unorm_getFCD16_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE3181:
	.size	_ZL22getTrailCombiningClassRK11IntPropertyi9UProperty, .-_ZL22getTrailCombiningClassRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL21getLeadCombiningClassRK11IntPropertyi9UProperty, @function
_ZL21getLeadCombiningClassRK11IntPropertyi9UProperty:
.LFB3180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	unorm_getFCD16_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%ah, %eax
	ret
	.cfi_endproc
.LFE3180:
	.size	_ZL21getLeadCombiningClassRK11IntPropertyi9UProperty, .-_ZL21getLeadCombiningClassRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL17getNormQuickCheckRK11IntPropertyi9UProperty, @function
_ZL17getNormQuickCheckRK11IntPropertyi9UProperty:
.LFB3179:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	leal	-4106(%rdx), %esi
	jmp	unorm_getQuickCheck_67@PLT
	.cfi_endproc
.LFE3179:
	.size	_ZL17getNormQuickCheckRK11IntPropertyi9UProperty, .-_ZL17getNormQuickCheckRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL17scriptGetMaxValueRK11IntProperty9UProperty, @function
_ZL17scriptGetMaxValueRK11IntProperty9UProperty:
.LFB3177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uprv_getMaxValues_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %edx
	movzbl	%al, %eax
	shrl	$12, %edx
	andl	$768, %edx
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE3177:
	.size	_ZL17scriptGetMaxValueRK11IntProperty9UProperty, .-_ZL17scriptGetMaxValueRK11IntProperty9UProperty
	.p2align 4
	.type	_ZL18defaultGetMaxValueRK11IntProperty9UProperty, @function
_ZL18defaultGetMaxValueRK11IntProperty9UProperty:
.LFB3166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %edi
	call	uprv_getMaxValues_67@PLT
	movl	8(%rbx), %ecx
	andl	4(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrl	%cl, %eax
	ret
	.cfi_endproc
.LFE3166:
	.size	_ZL18defaultGetMaxValueRK11IntProperty9UProperty, .-_ZL18defaultGetMaxValueRK11IntProperty9UProperty
	.p2align 4
	.type	_ZL9getScriptRK11IntPropertyi9UProperty, @function
_ZL9getScriptRK11IntPropertyi9UProperty:
.LFB3176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-12(%rbp), %rsi
	movl	$0, -12(%rbp)
	call	uscript_getScript_67@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L114
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3176:
	.size	_ZL9getScriptRK11IntPropertyi9UProperty, .-_ZL9getScriptRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL14getNumericTypeRK11IntPropertyi9UProperty, @function
_ZL14getNumericTypeRK11IntPropertyi9UProperty:
.LFB3175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	u_getMainProperties_67@PLT
	xorl	%r8d, %r8d
	shrl	$6, %eax
	je	.L115
	movl	$1, %r8d
	cmpl	$10, %eax
	jbe	.L115
	xorl	%r8d, %r8d
	cmpl	$20, %eax
	setg	%r8b
	addl	$2, %r8d
.L115:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3175:
	.size	_ZL14getNumericTypeRK11IntPropertyi9UProperty, .-_ZL14getNumericTypeRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL14getJoiningTypeRK11IntPropertyi9UProperty, @function
_ZL14getJoiningTypeRK11IntPropertyi9UProperty:
.LFB3174:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	ubidi_getJoiningType_67@PLT
	.cfi_endproc
.LFE3174:
	.size	_ZL14getJoiningTypeRK11IntPropertyi9UProperty, .-_ZL14getJoiningTypeRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL15getJoiningGroupRK11IntPropertyi9UProperty, @function
_ZL15getJoiningGroupRK11IntPropertyi9UProperty:
.LFB3173:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	ubidi_getJoiningGroup_67@PLT
	.cfi_endproc
.LFE3173:
	.size	_ZL15getJoiningGroupRK11IntPropertyi9UProperty, .-_ZL15getJoiningGroupRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL17getCombiningClassRK11IntPropertyi9UProperty, @function
_ZL17getCombiningClassRK11IntPropertyi9UProperty:
.LFB3171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	u_getCombiningClass_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE3171:
	.size	_ZL17getCombiningClassRK11IntPropertyi9UProperty, .-_ZL17getCombiningClassRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL15biDiGetMaxValueRK11IntProperty9UProperty, @function
_ZL15biDiGetMaxValueRK11IntProperty9UProperty:
.LFB3170:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	ubidi_getMaxValue_67@PLT
	.cfi_endproc
.LFE3170:
	.size	_ZL15biDiGetMaxValueRK11IntProperty9UProperty, .-_ZL15biDiGetMaxValueRK11IntProperty9UProperty
	.p2align 4
	.type	_ZL12getBiDiClassRK11IntPropertyi9UProperty, @function
_ZL12getBiDiClassRK11IntPropertyi9UProperty:
.LFB3168:
	.cfi_startproc
	endbr64
	movl	%esi, %edi
	jmp	u_charDirection_67@PLT
	.cfi_endproc
.LFE3168:
	.size	_ZL12getBiDiClassRK11IntPropertyi9UProperty, .-_ZL12getBiDiClassRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL5getVoRK11IntPropertyi9UProperty, @function
_ZL5getVoRK11IntPropertyi9UProperty:
.LFB3184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L143
.L129:
	movl	4+_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L130
.L128:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L144
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	leaq	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L129
	leaq	-28(%rbp), %rdi
	call	_ZN12_GLOBAL__N_112ulayout_loadER10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L130:
	movl	-28(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L128
	movq	_ZN12_GLOBAL__N_17gVoTrieE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L128
	movl	%r12d, %esi
	call	ucptrie_get_67@PLT
	jmp	.L128
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3184:
	.size	_ZL5getVoRK11IntPropertyi9UProperty, .-_ZL5getVoRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL7getInSCRK11IntPropertyi9UProperty, @function
_ZL7getInSCRK11IntPropertyi9UProperty:
.LFB3183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L160
.L146:
	movl	4+_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L147
.L145:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L161
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	leaq	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L146
	leaq	-28(%rbp), %rdi
	call	_ZN12_GLOBAL__N_112ulayout_loadER10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L147:
	movl	-28(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L145
	movq	_ZN12_GLOBAL__N_19gInscTrieE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L145
	movl	%r12d, %esi
	call	ucptrie_get_67@PLT
	jmp	.L145
.L161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3183:
	.size	_ZL7getInSCRK11IntPropertyi9UProperty, .-_ZL7getInSCRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL17layoutGetMaxValueRK11IntProperty9UProperty, @function
_ZL17layoutGetMaxValueRK11IntProperty9UProperty:
.LFB3185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L179
.L163:
	movl	4+_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %edx
	testl	%edx, %edx
	jle	.L164
.L169:
	xorl	%eax, %eax
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L163
	leaq	-28(%rbp), %rdi
	call	_ZN12_GLOBAL__N_112ulayout_loadER10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L164:
	movl	-28(%rbp), %eax
	testl	%eax, %eax
	jg	.L169
	cmpl	$4119, %ebx
	je	.L166
	movl	_ZN12_GLOBAL__N_111gMaxVoValueE(%rip), %eax
	cmpl	$4120, %ebx
	je	.L162
	cmpl	$4118, %ebx
	movl	$0, %eax
	cmove	_ZN12_GLOBAL__N_113gMaxInpcValueE(%rip), %eax
.L162:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L180
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movl	_ZN12_GLOBAL__N_113gMaxInscValueE(%rip), %eax
	jmp	.L162
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3185:
	.size	_ZL17layoutGetMaxValueRK11IntProperty9UProperty, .-_ZL17layoutGetMaxValueRK11IntProperty9UProperty
	.p2align 4
	.type	_ZL7getInPCRK11IntPropertyi9UProperty, @function
_ZL7getInPCRK11IntPropertyi9UProperty:
.LFB3182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -28(%rbp)
	movl	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %eax
	cmpl	$2, %eax
	jne	.L196
.L182:
	movl	4+_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.L183
.L181:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L197
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	leaq	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %rdi
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	testb	%al, %al
	je	.L182
	leaq	-28(%rbp), %rdi
	call	_ZN12_GLOBAL__N_112ulayout_loadER10UErrorCode
	movl	-28(%rbp), %eax
	leaq	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %rdi
	movl	%eax, 4+_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
.L183:
	movl	-28(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L181
	movq	_ZN12_GLOBAL__N_19gInpcTrieE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L181
	movl	%r12d, %esi
	call	ucptrie_get_67@PLT
	jmp	.L181
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3182:
	.size	_ZL7getInPCRK11IntPropertyi9UProperty, .-_ZL7getInPCRK11IntPropertyi9UProperty
	.p2align 4
	.type	_ZL27hasFullCompositionExclusionRK14BinaryPropertyi9UProperty, @function
_ZL27hasFullCompositionExclusionRK14BinaryPropertyi9UProperty:
.LFB3153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-44(%rbp), %rdi
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	call	_ZN6icu_6718Normalizer2Factory10getNFCImplER10UErrorCode@PLT
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	jle	.L199
.L206:
	xorl	%eax, %eax
.L198:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L211
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movl	%r12d, %edx
	movq	%rax, %rbx
	movl	$1, %eax
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L201
	movq	32(%rbx), %rdi
	movq	8(%rdi), %r13
	cmpl	$65535, %r12d
	ja	.L202
	movl	%r12d, %eax
	movq	(%rdi), %rdx
	movl	%r12d, %esi
	sarl	$6, %eax
	andl	$63, %esi
	cltq
	movzwl	(%rdx,%rax,2), %eax
	addl	%esi, %eax
	cltq
	addq	%rax, %rax
.L203:
	movzwl	0(%r13,%rax), %eax
.L201:
	cmpw	%ax, 18(%rbx)
	ja	.L206
	cmpw	%ax, 30(%rbx)
	seta	%al
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L202:
	cmpl	$1114111, %r12d
	ja	.L204
	cmpl	24(%rdi), %r12d
	jl	.L205
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L204:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L205:
	movl	%r12d, %esi
	call	ucptrie_internalSmallIndex_67@PLT
	cltq
	addq	%rax, %rax
	jmp	.L203
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3153:
	.size	_ZL27hasFullCompositionExclusionRK14BinaryPropertyi9UProperty, .-_ZL27hasFullCompositionExclusionRK14BinaryPropertyi9UProperty
	.p2align 4
	.type	_ZL26changesWhenNFKC_CasefoldedRK14BinaryPropertyi9UProperty, @function
_ZL26changesWhenNFKC_CasefoldedRK14BinaryPropertyi9UProperty:
.LFB3156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-260(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -260(%rbp)
	call	_ZN6icu_6718Normalizer2Factory14getNFKC_CFImplER10UErrorCode@PLT
	movl	-260(%rbp), %edi
	testl	%edi, %edi
	jle	.L247
.L212:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	leaq	-192(%rbp), %r14
	movl	%r13d, %esi
	movq	%rax, %r15
	movq	%r14, %rdi
	leaq	-256(%rbp), %r12
	leaq	-128(%rbp), %r13
	call	_ZN6icu_6713UnicodeStringC1Ei@PLT
	movl	$2, %esi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movw	%si, -120(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$5, %esi
	movq	%rax, -128(%rbp)
	movq	%r15, -256(%rbp)
	movq	%r13, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movl	$0, -216(%rbp)
	movb	$0, -212(%rbp)
	call	_ZN6icu_6716ReorderingBuffer4initEiR10UErrorCode@PLT
	testb	%al, %al
	je	.L214
	movswl	-184(%rbp), %eax
	testb	$17, %al
	jne	.L229
	leaq	-182(%rbp), %rsi
	testb	$2, %al
	cmove	-168(%rbp), %rsi
.L215:
	testw	%ax, %ax
	js	.L216
	sarl	$5, %eax
.L217:
	subq	$8, %rsp
	cltq
	xorl	%ecx, %ecx
	movq	%r12, %r9
	pushq	%rbx
	leaq	(%rsi,%rax,2), %rdx
	movl	$1, %r8d
	movq	%r15, %rdi
	call	_ZNK6icu_6715Normalizer2Impl7composeEPKDsS2_aaRNS_16ReorderingBufferER10UErrorCode@PLT
	popq	%rdx
	popq	%rcx
.L214:
	movq	-240(%rbp), %rax
	testq	%rax, %rax
	je	.L218
	movq	-224(%rbp), %rsi
	movq	-248(%rbp), %rdi
	subq	%rax, %rsi
	sarq	%rsi
	call	_ZN6icu_6713UnicodeString13releaseBufferEi@PLT
.L218:
	movl	-260(%rbp), %eax
	xorl	%r12d, %r12d
	testl	%eax, %eax
	jle	.L249
.L219:
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L249:
	movswl	-184(%rbp), %ecx
	movzwl	-120(%rbp), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	je	.L220
	movl	$1, %r12d
	subl	%esi, %r12d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L216:
	movl	-180(%rbp), %eax
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L220:
	testw	%ax, %ax
	js	.L222
	movswl	%ax, %edx
	sarl	$5, %edx
.L223:
	testw	%cx, %cx
	js	.L224
	sarl	$5, %ecx
.L225:
	testb	%sil, %sil
	jne	.L232
	cmpl	%edx, %ecx
	je	.L250
.L232:
	movl	$1, %r12d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L222:
	movl	-116(%rbp), %edx
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L229:
	xorl	%esi, %esi
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L224:
	movl	-180(%rbp), %ecx
	jmp	.L225
.L250:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	testb	%al, %al
	sete	%r12b
	jmp	.L219
.L248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3156:
	.size	_ZL26changesWhenNFKC_CasefoldedRK14BinaryPropertyi9UProperty, .-_ZL26changesWhenNFKC_CasefoldedRK14BinaryPropertyi9UProperty
	.p2align 4
	.globl	u_hasBinaryProperty_67
	.type	u_hasBinaryProperty_67, @function
u_hasBinaryProperty_67:
.LFB3164:
	.cfi_startproc
	endbr64
	movl	%edi, %r8d
	movl	%esi, %edx
	cmpl	$64, %esi
	ja	.L252
	movslq	%esi, %rdi
	leaq	_ZL8binProps(%rip), %rax
	movl	%r8d, %esi
	salq	$4, %rdi
	addq	%rax, %rdi
	movq	8(%rdi), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L252:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3164:
	.size	u_hasBinaryProperty_67, .-u_hasBinaryProperty_67
	.p2align 4
	.globl	u_getIntPropertyValue_67
	.type	u_getIntPropertyValue_67, @function
u_getIntPropertyValue_67:
.LFB3186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$4095, %esi
	jg	.L254
	xorl	%eax, %eax
	cmpl	$64, %esi
	ja	.L253
	movslq	%esi, %rax
	leaq	_ZL8binProps(%rip), %rcx
	movl	%edi, %esi
	salq	$4, %rax
	addq	%rcx, %rax
	movq	%rax, %rdi
	call	*8(%rax)
	movsbl	%al, %eax
.L253:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	cmpl	$4120, %esi
	jg	.L256
	leal	-4096(%rsi), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %esi
	movslq	%eax, %rdi
	movq	%rdi, %rax
	leaq	_ZL8intProps(%rip), %rdi
	salq	$5, %rax
	addq	%rax, %rdi
	movq	16(%rdi), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	xorl	%eax, %eax
	cmpl	$8192, %esi
	jne	.L253
	call	u_charType_67@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	ret
	.cfi_endproc
.LFE3186:
	.size	u_getIntPropertyValue_67, .-u_getIntPropertyValue_67
	.p2align 4
	.globl	u_getIntPropertyMinValue_67
	.type	u_getIntPropertyMinValue_67, @function
u_getIntPropertyMinValue_67:
.LFB3187:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3187:
	.size	u_getIntPropertyMinValue_67, .-u_getIntPropertyMinValue_67
	.p2align 4
	.globl	u_getIntPropertyMaxValue_67
	.type	u_getIntPropertyMaxValue_67, @function
u_getIntPropertyMaxValue_67:
.LFB3188:
	.cfi_startproc
	endbr64
	movl	%edi, %esi
	cmpl	$4095, %edi
	jg	.L262
	cmpl	$65, %edi
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	cmpl	$4120, %edi
	jg	.L265
	leal	-4096(%rdi), %edi
	leaq	_ZL8intProps(%rip), %rax
	movslq	%edi, %rdi
	salq	$5, %rdi
	addq	%rax, %rdi
	jmp	*24(%rdi)
	.p2align 4,,10
	.p2align 3
.L265:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE3188:
	.size	u_getIntPropertyMaxValue_67, .-u_getIntPropertyMaxValue_67
	.p2align 4
	.globl	uprops_getSource_67
	.type	uprops_getSource_67, @function
uprops_getSource_67:
.LFB3189:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edi, %edi
	js	.L266
	cmpl	$64, %edi
	jg	.L268
	movslq	%edi, %rdi
	leaq	_ZL8binProps(%rip), %rax
	salq	$4, %rdi
	addq	%rax, %rdi
	movl	$2, %eax
	movl	4(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L266
.L279:
	movl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	cmpl	$16397, %edi
	jle	.L280
	xorl	%eax, %eax
	cmpl	$28672, %edi
	sete	%al
	addl	%eax, %eax
.L266:
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	cmpl	$4095, %edi
	jle	.L266
	cmpl	$4120, %edi
	jle	.L281
	cmpl	$16383, %edi
	jg	.L270
	andl	$-4097, %edi
	xorl	%eax, %eax
	cmpl	$8192, %edi
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	subl	$4096, %edi
	leaq	_ZL8intProps(%rip), %rax
	movslq	%edi, %rdi
	salq	$5, %rdi
	addq	%rax, %rdi
	movl	$2, %eax
	movl	4(%rdi), %edx
	testl	%edx, %edx
	je	.L279
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L280:
	leal	-16384(%rdi), %edx
	je	.L266
	leaq	CSWTCH.150(%rip), %rax
	movl	(%rax,%rdx,4), %eax
	ret
	.cfi_endproc
.LFE3189:
	.size	uprops_getSource_67, .-uprops_getSource_67
	.p2align 4
	.globl	uprops_addPropertyStarts_67
	.type	uprops_addPropertyStarts_67, @function
uprops_addPropertyStarts_67:
.LFB3190:
	.cfi_startproc
	endbr64
	movl	(%rdx), %r8d
	testl	%r8d, %r8d
	jle	.L305
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %eax
	cmpl	$2, %eax
	je	.L284
	leaq	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN6icu_6720umtx_initImplPreInitERNS_9UInitOnceE@PLT
	movq	-40(%rbp), %rdx
	testb	%al, %al
	jne	.L306
.L284:
	movl	4+_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %eax
	testl	%eax, %eax
	jle	.L285
	movl	%eax, (%rdx)
.L282:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN12_GLOBAL__N_112ulayout_loadER10UErrorCode
	movq	-40(%rbp), %rdx
	leaq	_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip), %rdi
	movl	(%rdx), %eax
	movl	%eax, 4+_ZN12_GLOBAL__N_115gLayoutInitOnceE(%rip)
	call	_ZN6icu_6721umtx_initImplPostInitERNS_9UInitOnceE@PLT
	movq	-40(%rbp), %rdx
.L285:
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L282
	cmpl	$13, %r13d
	je	.L287
	cmpl	$14, %r13d
	je	.L288
	cmpl	$12, %r13d
	je	.L307
	movl	$1, (%rdx)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L287:
	movq	_ZN12_GLOBAL__N_19gInscTrieE(%rip), %r13
.L290:
	xorl	%r14d, %r14d
	testq	%r13, %r13
	jne	.L291
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L309:
	movl	%r14d, %esi
	movq	(%r12), %rdi
	leal	1(%rbx), %r14d
	call	*8(%r12)
.L291:
	subq	$8, %rsp
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%ecx, %ecx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	ucptrie_getRange_67@PLT
	movl	%eax, %ebx
	popq	%rax
	popq	%rdx
	testl	%ebx, %ebx
	jns	.L309
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	movq	_ZN12_GLOBAL__N_19gInpcTrieE(%rip), %r13
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L288:
	movq	_ZN12_GLOBAL__N_17gVoTrieE(%rip), %r13
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L308:
	movl	$2, (%rdx)
	jmp	.L282
	.cfi_endproc
.LFE3190:
	.size	uprops_addPropertyStarts_67, .-uprops_addPropertyStarts_67
	.p2align 4
	.globl	u_getFC_NFKC_Closure_67
	.type	u_getFC_NFKC_Closure_67, @function
u_getFC_NFKC_Closure_67:
.LFB3191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L353
	movl	(%rcx), %r8d
	movq	%rcx, %r12
	testl	%r8d, %r8d
	jg	.L353
	movl	%edx, %ebx
	testl	%edx, %edx
	js	.L314
	movl	%edi, %r14d
	movq	%rsi, %r13
	testq	%rsi, %rsi
	jne	.L315
	testl	%edx, %edx
	jle	.L315
.L314:
	movl	$1, (%r12)
.L353:
	xorl	%eax, %eax
.L310:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L354
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN6icu_6711Normalizer215getNFKCInstanceER10UErrorCode@PLT
	movl	(%r12), %edi
	testl	%edi, %edi
	jg	.L353
	movl	$2, %esi
	movq	%rax, -344(%rbp)
	xorl	%edx, %edx
	movl	%r14d, %edi
	movw	%si, -312(%rbp)
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-336(%rbp), %rsi
	movq	%rax, -320(%rbp)
	call	ucase_toFullFolding_67@PLT
	movq	-344(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %r15d
	js	.L355
	movq	%r10, -352(%rbp)
	cmpl	$31, %eax
	jg	.L356
	movq	-336(%rbp), %rax
	leaq	-328(%rbp), %rdx
	movl	%r15d, %ecx
	xorl	%esi, %esi
	movq	%rax, -328(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN6icu_6713UnicodeString5setToEaNS_14ConstChar16PtrEi@PLT
	movq	-352(%rbp), %r10
.L326:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	leaq	-256(%rbp), %r14
	movq	%r10, %rdi
	movq	%r12, %rcx
	movq	%rax, -256(%rbp)
	movl	$2, %eax
	movq	%r14, %rdx
	leaq	-192(%rbp), %r15
	movw	%ax, -248(%rbp)
	movq	(%r10), %rax
	movq	%r10, -352(%rbp)
	movq	-344(%rbp), %rsi
	call	*24(%rax)
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1ERKS0_@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8foldCaseEj@PLT
	movq	-352(%rbp), %r10
	leaq	-128(%rbp), %r8
	movq	%r12, %rcx
	movq	%rax, %rsi
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	%r8, -352(%rbp)
	movq	%rax, -128(%rbp)
	movq	(%r10), %rax
	movq	%r10, %rdi
	movw	%dx, -120(%rbp)
	movq	%r8, %rdx
	call	*24(%rax)
	movl	(%r12), %ecx
	movq	-352(%rbp), %r8
	testl	%ecx, %ecx
	jle	.L357
.L331:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%r8, -352(%rbp)
	call	u_terminateUChars_67@PLT
	movq	-352(%rbp), %r8
.L338:
	movq	%r8, %rdi
	movl	%eax, -352(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-352(%rbp), %eax
.L327:
	movq	-344(%rbp), %rdi
	movl	%eax, -352(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movl	-352(%rbp), %eax
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L356:
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-312(%rbp), %eax
	movq	-352(%rbp), %r10
	testw	%ax, %ax
	js	.L329
	movswl	%ax, %edx
	sarl	$5, %edx
.L330:
	movq	-344(%rbp), %rdi
	movl	%r15d, %ecx
	xorl	%esi, %esi
	movq	%r10, -352(%rbp)
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	movq	-352(%rbp), %r10
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L357:
	movswl	-120(%rbp), %ecx
	movzwl	-248(%rbp), %eax
	movl	%ecx, %esi
	andl	$1, %esi
	testb	$1, %al
	jne	.L332
	testw	%ax, %ax
	js	.L333
	movswl	%ax, %edx
	sarl	$5, %edx
.L334:
	testw	%cx, %cx
	js	.L335
	sarl	$5, %ecx
.L336:
	testb	%sil, %sil
	jne	.L337
	cmpl	%edx, %ecx
	je	.L358
.L337:
	leaq	-328(%rbp), %rsi
	movq	%r12, %rcx
	movl	%ebx, %edx
	movq	%r8, %rdi
	movq	%r13, -328(%rbp)
	movq	%r8, -352(%rbp)
	call	_ZNK6icu_6713UnicodeString7extractENS_9Char16PtrEiR10UErrorCode@PLT
	movq	-352(%rbp), %r8
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%r10, %rdi
	call	_ZN6icu_6718Normalizer2Factory7getImplEPKNS_11Normalizer2E@PLT
	movq	-344(%rbp), %r10
	movq	%rax, %rcx
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L359
	movq	32(%rcx), %rdi
	movq	8(%rdi), %r15
	cmpl	$65535, %r14d
	ja	.L320
	movl	%r14d, %eax
	movq	(%rdi), %rdx
	sarl	$6, %eax
	cltq
	movzwl	(%rdx,%rax,2), %eax
	movl	%r14d, %edx
	andl	$63, %edx
	addl	%edx, %eax
	cltq
	addq	%rax, %rax
.L321:
	movzwl	(%r15,%rax), %eax
	cmpw	%ax, 18(%rcx)
	ja	.L318
	cmpw	$-511, %ax
	jbe	.L319
.L318:
	movl	%ebx, %esi
	movq	%r12, %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	u_terminateUChars_67@PLT
	leaq	-320(%rbp), %rbx
	movq	%rbx, -344(%rbp)
	jmp	.L327
.L358:
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -352(%rbp)
	call	_ZNK6icu_6713UnicodeString8doEqualsERKS0_i@PLT
	movq	-352(%rbp), %r8
	testb	%al, %al
	setne	%sil
	.p2align 4,,10
	.p2align 3
.L332:
	testb	%sil, %sil
	jne	.L331
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L329:
	movl	-308(%rbp), %edx
	jmp	.L330
.L359:
	cmpw	$1, 18(%rcx)
	movl	$1, %eax
	ja	.L318
.L319:
	movq	%r10, -352(%rbp)
	cmpw	%ax, 30(%rcx)
	jbe	.L318
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movzwl	-312(%rbp), %eax
	movq	-352(%rbp), %r10
	testw	%ax, %ax
	js	.L324
	movswl	%ax, %edx
	sarl	$5, %edx
.L325:
	movq	-344(%rbp), %rdi
	movl	%r14d, %ecx
	xorl	%esi, %esi
	movq	%r10, -352(%rbp)
	call	_ZN6icu_6713UnicodeString7replaceEiii@PLT
	movq	-352(%rbp), %r10
	jmp	.L326
.L320:
	cmpl	$1114111, %r14d
	ja	.L322
	cmpl	24(%rdi), %r14d
	jl	.L323
	movl	20(%rdi), %eax
	subl	$2, %eax
	cltq
	addq	%rax, %rax
	jmp	.L321
.L333:
	movl	-244(%rbp), %edx
	jmp	.L334
.L335:
	movl	-116(%rbp), %ecx
	jmp	.L336
.L322:
	movl	20(%rdi), %eax
	subl	$1, %eax
	cltq
	addq	%rax, %rax
	jmp	.L321
.L324:
	movl	-308(%rbp), %edx
	jmp	.L325
.L323:
	movl	%r14d, %esi
	movq	%rcx, -352(%rbp)
	movq	%r10, -344(%rbp)
	call	ucptrie_internalSmallIndex_67@PLT
	movq	-344(%rbp), %r10
	movq	-352(%rbp), %rcx
	cltq
	addq	%rax, %rax
	jmp	.L321
.L354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3191:
	.size	u_getFC_NFKC_Closure_67, .-u_getFC_NFKC_Closure_67
	.section	.rodata
	.align 32
	.type	CSWTCH.150, @object
	.size	CSWTCH.150, 52
CSWTCH.150:
	.long	2
	.long	5
	.long	4
	.long	3
	.long	4
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	3
	.long	4
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	_ZL8intProps, @object
	.size	_ZL8intProps, 800
_ZL8intProps:
	.long	5
	.long	0
	.long	0
	.zero	4
	.quad	_ZL12getBiDiClassRK11IntPropertyi9UProperty
	.quad	_ZL15biDiGetMaxValueRK11IntProperty9UProperty
	.long	0
	.long	130816
	.long	8
	.zero	4
	.quad	_ZL15defaultGetValueRK11IntPropertyi9UProperty
	.quad	_ZL18defaultGetMaxValueRK11IntProperty9UProperty
	.long	8
	.long	0
	.long	255
	.zero	4
	.quad	_ZL17getCombiningClassRK11IntPropertyi9UProperty
	.quad	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.long	2
	.long	31
	.long	0
	.zero	4
	.quad	_ZL15defaultGetValueRK11IntPropertyi9UProperty
	.quad	_ZL18defaultGetMaxValueRK11IntProperty9UProperty
	.long	0
	.long	917504
	.long	17
	.zero	4
	.quad	_ZL15defaultGetValueRK11IntPropertyi9UProperty
	.quad	_ZL18defaultGetMaxValueRK11IntProperty9UProperty
	.long	1
	.long	0
	.long	29
	.zero	4
	.quad	_ZL18getGeneralCategoryRK11IntPropertyi9UProperty
	.quad	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.long	5
	.long	0
	.long	0
	.zero	4
	.quad	_ZL15getJoiningGroupRK11IntPropertyi9UProperty
	.quad	_ZL15biDiGetMaxValueRK11IntProperty9UProperty
	.long	5
	.long	0
	.long	0
	.zero	4
	.quad	_ZL14getJoiningTypeRK11IntPropertyi9UProperty
	.quad	_ZL15biDiGetMaxValueRK11IntProperty9UProperty
	.long	2
	.long	66060288
	.long	20
	.zero	4
	.quad	_ZL15defaultGetValueRK11IntPropertyi9UProperty
	.quad	_ZL18defaultGetMaxValueRK11IntProperty9UProperty
	.long	1
	.long	0
	.long	3
	.zero	4
	.quad	_ZL14getNumericTypeRK11IntPropertyi9UProperty
	.quad	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.long	2
	.long	0
	.long	0
	.zero	4
	.quad	_ZL9getScriptRK11IntPropertyi9UProperty
	.quad	_ZL17scriptGetMaxValueRK11IntProperty9UProperty
	.long	2
	.long	0
	.long	5
	.zero	4
	.quad	_ZL21getHangulSyllableTypeRK11IntPropertyi9UProperty
	.quad	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.long	8
	.long	0
	.long	1
	.zero	4
	.quad	_ZL17getNormQuickCheckRK11IntPropertyi9UProperty
	.quad	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.long	9
	.long	0
	.long	1
	.zero	4
	.quad	_ZL17getNormQuickCheckRK11IntPropertyi9UProperty
	.quad	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.long	8
	.long	0
	.long	2
	.zero	4
	.quad	_ZL17getNormQuickCheckRK11IntPropertyi9UProperty
	.quad	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.long	9
	.long	0
	.long	2
	.zero	4
	.quad	_ZL17getNormQuickCheckRK11IntPropertyi9UProperty
	.quad	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.long	8
	.long	0
	.long	255
	.zero	4
	.quad	_ZL21getLeadCombiningClassRK11IntPropertyi9UProperty
	.quad	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.long	8
	.long	0
	.long	255
	.zero	4
	.quad	_ZL22getTrailCombiningClassRK11IntPropertyi9UProperty
	.quad	_ZL20getMaxValueFromShiftRK11IntProperty9UProperty
	.long	2
	.long	992
	.long	5
	.zero	4
	.quad	_ZL15defaultGetValueRK11IntPropertyi9UProperty
	.quad	_ZL18defaultGetMaxValueRK11IntProperty9UProperty
	.long	2
	.long	1015808
	.long	15
	.zero	4
	.quad	_ZL15defaultGetValueRK11IntPropertyi9UProperty
	.quad	_ZL18defaultGetMaxValueRK11IntProperty9UProperty
	.long	2
	.long	31744
	.long	10
	.zero	4
	.quad	_ZL15defaultGetValueRK11IntPropertyi9UProperty
	.quad	_ZL18defaultGetMaxValueRK11IntProperty9UProperty
	.long	5
	.long	0
	.long	0
	.zero	4
	.quad	_ZL24getBiDiPairedBracketTypeRK11IntPropertyi9UProperty
	.quad	_ZL15biDiGetMaxValueRK11IntProperty9UProperty
	.long	12
	.long	0
	.long	0
	.zero	4
	.quad	_ZL7getInPCRK11IntPropertyi9UProperty
	.quad	_ZL17layoutGetMaxValueRK11IntProperty9UProperty
	.long	13
	.long	0
	.long	0
	.zero	4
	.quad	_ZL7getInSCRK11IntPropertyi9UProperty
	.quad	_ZL17layoutGetMaxValueRK11IntProperty9UProperty
	.long	14
	.long	0
	.long	0
	.zero	4
	.quad	_ZL5getVoRK11IntPropertyi9UProperty
	.quad	_ZL17layoutGetMaxValueRK11IntProperty9UProperty
	.section	.rodata
	.align 32
	.type	_ZL8gcbToHst, @object
	.size	_ZL8gcbToHst, 40
_ZL8gcbToHst:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	0
	.long	4
	.long	5
	.long	3
	.long	2
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL8binProps, @object
	.size	_ZL8binProps, 1040
_ZL8binProps:
	.long	1
	.long	256
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	128
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	5
	.long	0
	.quad	_ZL13isBidiControlRK14BinaryPropertyi9UProperty
	.long	5
	.long	0
	.quad	_ZL10isMirroredRK14BinaryPropertyi9UProperty
	.long	1
	.long	2
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	524288
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	1048576
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	1024
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	2048
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	8
	.long	0
	.quad	_ZL27hasFullCompositionExclusionRK14BinaryPropertyi9UProperty
	.long	1
	.long	67108864
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	8192
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	16384
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	64
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	4
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	33554432
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	16777216
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	512
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	32768
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	65536
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	5
	.long	0
	.quad	_ZL13isJoinControlRK14BinaryPropertyi9UProperty
	.long	1
	.long	2097152
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	4
	.long	0
	.quad	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	32
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	4096
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	8
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	131072
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	4
	.long	0
	.quad	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	16
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	262144
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	4
	.long	0
	.quad	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	1
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	8388608
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	4194304
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	4
	.long	0
	.quad	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	134217728
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	268435456
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	8
	.long	0
	.quad	_ZL11isNormInertRK14BinaryPropertyi9UProperty
	.long	9
	.long	0
	.quad	_ZL11isNormInertRK14BinaryPropertyi9UProperty
	.long	8
	.long	0
	.quad	_ZL11isNormInertRK14BinaryPropertyi9UProperty
	.long	9
	.long	0
	.quad	_ZL11isNormInertRK14BinaryPropertyi9UProperty
	.long	11
	.long	0
	.quad	_ZL21isCanonSegmentStarterRK14BinaryPropertyi9UProperty
	.long	1
	.long	536870912
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	1
	.long	1073741824
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	6
	.long	0
	.quad	_ZL13isPOSIX_alnumRK14BinaryPropertyi9UProperty
	.long	1
	.long	0
	.quad	_ZL13isPOSIX_blankRK14BinaryPropertyi9UProperty
	.long	1
	.long	0
	.quad	_ZL13isPOSIX_graphRK14BinaryPropertyi9UProperty
	.long	1
	.long	0
	.quad	_ZL13isPOSIX_printRK14BinaryPropertyi9UProperty
	.long	1
	.long	0
	.quad	_ZL14isPOSIX_xdigitRK14BinaryPropertyi9UProperty
	.long	4
	.long	0
	.quad	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.long	4
	.long	0
	.quad	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.long	4
	.long	0
	.quad	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.long	4
	.long	0
	.quad	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.long	4
	.long	0
	.quad	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.long	7
	.long	0
	.quad	_ZL21changesWhenCasefoldedRK14BinaryPropertyi9UProperty
	.long	4
	.long	0
	.quad	_ZL26caseBinaryPropertyContainsRK14BinaryPropertyi9UProperty
	.long	10
	.long	0
	.quad	_ZL26changesWhenNFKC_CasefoldedRK14BinaryPropertyi9UProperty
	.long	2
	.long	268435456
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	2
	.long	536870912
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	2
	.long	1073741824
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	2
	.long	-2147483648
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	2
	.long	134217728
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	2
	.long	0
	.quad	_ZL19isRegionalIndicatorRK14BinaryPropertyi9UProperty
	.long	1
	.long	-2147483648
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.long	2
	.long	67108864
	.quad	_ZL15defaultContainsRK14BinaryPropertyi9UProperty
	.local	_ZN12_GLOBAL__N_111gMaxVoValueE
	.comm	_ZN12_GLOBAL__N_111gMaxVoValueE,4,4
	.local	_ZN12_GLOBAL__N_113gMaxInscValueE
	.comm	_ZN12_GLOBAL__N_113gMaxInscValueE,4,4
	.local	_ZN12_GLOBAL__N_113gMaxInpcValueE
	.comm	_ZN12_GLOBAL__N_113gMaxInpcValueE,4,4
	.local	_ZN12_GLOBAL__N_17gVoTrieE
	.comm	_ZN12_GLOBAL__N_17gVoTrieE,8,8
	.local	_ZN12_GLOBAL__N_19gInscTrieE
	.comm	_ZN12_GLOBAL__N_19gInscTrieE,8,8
	.local	_ZN12_GLOBAL__N_19gInpcTrieE
	.comm	_ZN12_GLOBAL__N_19gInpcTrieE,8,8
	.local	_ZN12_GLOBAL__N_113gLayoutMemoryE
	.comm	_ZN12_GLOBAL__N_113gLayoutMemoryE,8,8
	.local	_ZN12_GLOBAL__N_115gLayoutInitOnceE
	.comm	_ZN12_GLOBAL__N_115gLayoutInitOnceE,8,8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
