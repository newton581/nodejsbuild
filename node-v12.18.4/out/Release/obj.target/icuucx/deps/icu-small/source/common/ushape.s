	.file	"ushape.cpp"
	.text
	.p2align 4
	.type	_ZL31_shapeToArabicDigitsWithContextPDsiDsaa, @function
_ZL31_shapeToArabicDigitsWithContextPDsiDsaa:
.LFB2075:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	-48(%rdx), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movw	%ax, -58(%rbp)
	testb	%cl, %cl
	jne	.L2
	testl	%esi, %esi
	jle	.L1
	movslq	%esi, %r14
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	movl	%r13d, %r15d
.L9:
	subq	$1, %r14
	testl	%r14d, %r14d
	jle	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	movq	-56(%rbp), %rax
	movzwl	-2(%rax,%r14,2), %ebx
	movl	%ebx, %edi
	movl	%ebx, %r12d
	call	ubidi_getClass_67@PLT
	cmpl	$2, %eax
	je	.L8
	jbe	.L11
	cmpl	$13, %eax
	movl	$1, %eax
	cmove	%eax, %r15d
	subq	$1, %r14
	testl	%r14d, %r14d
	jg	.L4
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L1
	leal	-1(%rsi), %eax
	movq	%rdi, %rbx
	leaq	2(%rdi,%rax,2), %rax
	movq	%rax, -56(%rbp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%r15d, %r15d
.L6:
	addq	$2, %rbx
	cmpq	%rbx, -56(%rbp)
	je	.L1
.L7:
	movzwl	(%rbx), %r14d
	movl	%r14d, %edi
	movl	%r14d, %r12d
	call	ubidi_getClass_67@PLT
	cmpl	$2, %eax
	je	.L5
	jbe	.L10
	cmpl	$13, %eax
	cmove	%r13d, %r15d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	testb	%r15b, %r15b
	je	.L9
	leal	-48(%rbx), %edx
	cmpl	$9, %edx
	ja	.L9
	movq	-56(%rbp), %rax
	addw	-58(%rbp), %r12w
	movw	%r12w, -2(%rax,%r14,2)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L5:
	testb	%r15b, %r15b
	je	.L6
	leal	-48(%r14), %edx
	cmpl	$9, %edx
	ja	.L6
	addw	-58(%rbp), %r12w
	movw	%r12w, (%rbx)
	jmp	.L6
	.cfi_endproc
.LFE2075:
	.size	_ZL31_shapeToArabicDigitsWithContextPDsiDsaa, .-_ZL31_shapeToArabicDigitsWithContextPDsiDsaa
	.p2align 4
	.type	_ZL13calculateSizePKDsiij.isra.0, @function
_ZL13calculateSizePKDsiij.isra.0:
.LFB2626:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	andl	$24, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	testb	$8, %dl
	je	.L21
	testl	$65539, %edx
	jne	.L21
.L74:
	testb	$4, %dl
	je	.L22
	testl	%esi, %esi
	jle	.L77
	leal	-1(%rsi), %r12d
	testl	%r12d, %r12d
	jle	.L76
	cmpl	%r12d, %esi
	movl	%r12d, %ebx
	leaq	2(%rdi), %r10
	movl	%esi, %eax
	cmovle	%esi, %ebx
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L29:
	movzwl	-2(%r10), %ecx
	leal	-1570(%rcx), %r11d
	cmpw	$1, %r11w
	jbe	.L80
	movl	%ecx, %r11d
	andl	$-3, %r11d
	cmpw	$1573, %r11w
	jne	.L25
.L80:
	cmpw	$1604, (%r10)
	je	.L27
.L25:
	addw	$400, %cx
	cmpw	$15, %cx
	ja	.L28
.L27:
	subl	$1, %eax
.L28:
	addl	$1, %r8d
	addq	$2, %r10
	cmpl	%r8d, %ebx
	jg	.L29
	cmpl	%r8d, %esi
	jle	.L23
.L24:
	movl	%esi, %r11d
	movl	$1, %ecx
	subl	%r8d, %r11d
	cmpl	%esi, %r8d
	cmovge	%ecx, %r11d
	subl	%r8d, %r12d
	cmpl	$6, %r12d
	jbe	.L30
	cmpl	%esi, %r8d
	jge	.L30
	movl	%r11d, %r10d
	pxor	%xmm1, %xmm1
	pxor	%xmm5, %xmm5
	movslq	%r8d, %rcx
	shrl	$3, %r10d
	leaq	(%rdi,%rcx,2), %rcx
	movdqa	.LC0(%rip), %xmm4
	movdqa	.LC1(%rip), %xmm3
	salq	$4, %r10
	addq	%rcx, %r10
	.p2align 4,,10
	.p2align 3
.L32:
	movdqu	(%rcx), %xmm0
	movdqa	%xmm5, %xmm2
	addq	$16, %rcx
	paddw	%xmm4, %xmm0
	psubusw	%xmm3, %xmm0
	pcmpeqw	%xmm5, %xmm0
	pcmpgtw	%xmm0, %xmm2
	movdqa	%xmm0, %xmm6
	punpcklwd	%xmm2, %xmm6
	punpckhwd	%xmm2, %xmm0
	paddd	%xmm6, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%r10, %rcx
	jne	.L32
	movdqa	%xmm1, %xmm0
	psrldq	$8, %xmm0
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %ecx
	addl	%ecx, %eax
	movl	%r11d, %ecx
	andl	$-8, %ecx
	addl	%ecx, %r8d
	cmpl	%ecx, %r11d
	je	.L23
.L30:
	movslq	%r8d, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$400, %cx
	cmpw	$16, %cx
	leal	1(%r8), %ecx
	sbbl	$0, %eax
	cmpl	%ecx, %esi
	jle	.L23
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$400, %cx
	cmpw	$16, %cx
	leal	2(%r8), %ecx
	sbbl	$0, %eax
	cmpl	%ecx, %esi
	jle	.L23
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$400, %cx
	cmpw	$16, %cx
	leal	3(%r8), %ecx
	sbbl	$0, %eax
	cmpl	%ecx, %esi
	jle	.L23
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$400, %cx
	cmpw	$16, %cx
	leal	4(%r8), %ecx
	sbbl	$0, %eax
	cmpl	%ecx, %esi
	jle	.L23
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$400, %cx
	cmpw	$16, %cx
	leal	5(%r8), %ecx
	sbbl	$0, %eax
	cmpl	%ecx, %esi
	jle	.L23
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$400, %cx
	cmpw	$16, %cx
	sbbl	$0, %eax
	addl	$6, %r8d
	cmpl	%r8d, %esi
	jle	.L23
	movslq	%r8d, %r8
	movzwl	(%rdi,%r8,2), %ecx
	addw	$400, %cx
	cmpw	$16, %cx
	sbbl	$0, %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%edx, %eax
	andl	$917528, %eax
	cmpl	$524296, %eax
	je	.L74
.L77:
	movl	%esi, %eax
.L23:
	cmpl	$16, %r9d
	je	.L93
.L20:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L77
	leal	-1(%rsi), %ebx
	testl	%ebx, %ebx
	jle	.L78
	cmpl	%ebx, %esi
	movl	%ebx, %r11d
	leaq	2(%rdi), %r10
	movl	%esi, %eax
	cmovle	%esi, %r11d
	xorl	%ecx, %ecx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L44:
	addw	$400, %r8w
	cmpw	$15, %r8w
	ja	.L46
.L45:
	subl	$1, %eax
.L46:
	addl	$1, %ecx
	addq	$2, %r10
	cmpl	%r11d, %ecx
	jge	.L94
.L47:
	movzwl	-2(%r10), %r8d
	cmpw	$1604, %r8w
	jne	.L44
	movzwl	(%r10), %r8d
	leal	-1570(%r8), %r12d
	cmpw	$1, %r12w
	jbe	.L45
	andl	$-3, %r8d
	cmpw	$1573, %r8w
	je	.L45
	addl	$1, %ecx
	addq	$2, %r10
	cmpl	%r11d, %ecx
	jl	.L47
	.p2align 4,,10
	.p2align 3
.L94:
	cmpl	%ecx, %esi
	jle	.L23
.L43:
	movl	%esi, %r11d
	movl	$1, %r8d
	subl	%ecx, %r11d
	cmpl	%ecx, %esi
	cmovle	%r8d, %r11d
	subl	%ecx, %ebx
	cmpl	$6, %ebx
	jbe	.L48
	cmpl	%ecx, %esi
	jle	.L48
	movl	%r11d, %r10d
	movslq	%ecx, %r8
	movdqa	.LC0(%rip), %xmm4
	movdqa	.LC1(%rip), %xmm3
	leaq	(%rdi,%r8,2), %r8
	pxor	%xmm2, %xmm2
	shrl	$3, %r10d
	pxor	%xmm5, %xmm5
	salq	$4, %r10
	movdqa	.LC2(%rip), %xmm7
	addq	%r8, %r10
	.p2align 4,,10
	.p2align 3
.L50:
	movdqu	(%r8), %xmm0
	movdqu	(%r8), %xmm1
	addq	$16, %r8
	pcmpeqw	%xmm7, %xmm0
	paddw	%xmm4, %xmm1
	psubusw	%xmm3, %xmm1
	pcmpeqw	%xmm5, %xmm1
	pcmpeqw	%xmm5, %xmm0
	pand	%xmm1, %xmm0
	movdqa	%xmm5, %xmm1
	pcmpgtw	%xmm0, %xmm1
	movdqa	%xmm0, %xmm6
	punpcklwd	%xmm1, %xmm6
	punpckhwd	%xmm1, %xmm0
	paddd	%xmm6, %xmm2
	paddd	%xmm0, %xmm2
	cmpq	%r10, %r8
	jne	.L50
	movdqa	%xmm2, %xmm0
	psrldq	$8, %xmm0
	paddd	%xmm2, %xmm0
	movdqa	%xmm0, %xmm2
	psrldq	$4, %xmm2
	paddd	%xmm2, %xmm0
	movd	%xmm0, %r8d
	addl	%r8d, %eax
	movl	%r11d, %r8d
	andl	$-8, %r8d
	addl	%r8d, %ecx
	cmpl	%r8d, %r11d
	je	.L23
.L48:
	movslq	%ecx, %r8
	movzwl	(%rdi,%r8,2), %r8d
	addw	$400, %r8w
	cmpw	$16, %r8w
	leal	1(%rcx), %r8d
	sbbl	$0, %eax
	cmpl	%r8d, %esi
	jle	.L23
	movslq	%r8d, %r8
	movzwl	(%rdi,%r8,2), %r8d
	addw	$400, %r8w
	cmpw	$16, %r8w
	leal	2(%rcx), %r8d
	sbbl	$0, %eax
	cmpl	%r8d, %esi
	jle	.L23
	movslq	%r8d, %r8
	movzwl	(%rdi,%r8,2), %r8d
	addw	$400, %r8w
	cmpw	$16, %r8w
	leal	3(%rcx), %r8d
	sbbl	$0, %eax
	cmpl	%r8d, %esi
	jle	.L23
	movslq	%r8d, %r8
	movzwl	(%rdi,%r8,2), %r8d
	addw	$400, %r8w
	cmpw	$16, %r8w
	leal	4(%rcx), %r8d
	sbbl	$0, %eax
	cmpl	%r8d, %esi
	jle	.L23
	movslq	%r8d, %r8
	movzwl	(%rdi,%r8,2), %r8d
	addw	$400, %r8w
	cmpw	$16, %r8w
	leal	5(%rcx), %r8d
	sbbl	$0, %eax
	cmpl	%r8d, %esi
	jle	.L23
	movslq	%r8d, %r8
	movzwl	(%rdi,%r8,2), %r8d
	addw	$400, %r8w
	cmpw	$16, %r8w
	sbbl	$0, %eax
	addl	$6, %ecx
	cmpl	%ecx, %esi
	jle	.L23
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$400, %cx
	cmpw	$16, %cx
	sbbl	$0, %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L93:
	andl	$65539, %edx
	jne	.L20
	testl	%esi, %esi
	jle	.L20
	leal	-1(%rsi), %edx
	cmpl	$6, %edx
	jbe	.L79
	movl	%esi, %ecx
	movq	%rdi, %rdx
	pxor	%xmm1, %xmm1
	movdqa	.LC3(%rip), %xmm7
	shrl	$3, %ecx
	movdqa	.LC4(%rip), %xmm6
	movdqa	.LC5(%rip), %xmm5
	pxor	%xmm3, %xmm3
	salq	$4, %rcx
	addq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L64:
	movdqu	(%rdx), %xmm0
	movdqa	%xmm3, %xmm2
	addq	$16, %rdx
	paddw	%xmm7, %xmm0
	psubusw	%xmm6, %xmm0
	pcmpeqw	%xmm3, %xmm0
	pand	%xmm5, %xmm0
	pcmpgtw	%xmm0, %xmm2
	movdqa	%xmm0, %xmm4
	punpcklwd	%xmm2, %xmm4
	punpckhwd	%xmm2, %xmm0
	paddd	%xmm4, %xmm1
	paddd	%xmm0, %xmm1
	cmpq	%rcx, %rdx
	jne	.L64
	movdqa	%xmm1, %xmm0
	psrldq	$8, %xmm0
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %edx
	addl	%edx, %eax
	movl	%esi, %edx
	andl	$-8, %edx
	testb	$7, %sil
	je	.L20
.L62:
	movslq	%edx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$267, %cx
	cmpw	$8, %cx
	leal	1(%rdx), %ecx
	adcl	$0, %eax
	cmpl	%ecx, %esi
	jle	.L20
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$267, %cx
	cmpw	$8, %cx
	leal	2(%rdx), %ecx
	adcl	$0, %eax
	cmpl	%ecx, %esi
	jle	.L20
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$267, %cx
	cmpw	$8, %cx
	leal	3(%rdx), %ecx
	adcl	$0, %eax
	cmpl	%ecx, %esi
	jle	.L20
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$267, %cx
	cmpw	$8, %cx
	leal	4(%rdx), %ecx
	adcl	$0, %eax
	cmpl	%ecx, %esi
	jle	.L20
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$267, %cx
	cmpw	$8, %cx
	leal	5(%rdx), %ecx
	adcl	$0, %eax
	cmpl	%ecx, %esi
	jle	.L20
	movslq	%ecx, %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	addw	$267, %cx
	cmpw	$8, %cx
	adcl	$0, %eax
	addl	$6, %edx
	cmpl	%edx, %esi
	jle	.L20
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
	addw	$267, %dx
	cmpw	$8, %dx
	adcl	$0, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L76:
	movl	%esi, %eax
	xorl	%r8d, %r8d
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L78:
	movl	%esi, %eax
	xorl	%ecx, %ecx
	jmp	.L43
.L79:
	xorl	%edx, %edx
	jmp	.L62
	.cfi_endproc
.LFE2626:
	.size	_ZL13calculateSizePKDsiij.isra.0, .-_ZL13calculateSizePKDsiij.isra.0
	.p2align 4
	.type	_ZL24expandCompositCharAtNearPDsiiP10UErrorCodeiii15uShapeVariables.isra.0, @function
_ZL24expandCompositCharAtNearPDsiiP10UErrorCodeiii15uShapeVariables.isra.0:
.LFB2628:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	testl	%esi, %esi
	jle	.L96
	leal	-1(%rsi), %eax
	xorl	%r10d, %r10d
	leaq	_ZL14convertLamAlef(%rip), %rbx
	leaq	_ZL13yehHamzaToYeh(%rip), %r13
	leaq	_ZL23tailFamilyIsolatedFinal(%rip), %rsi
	testl	%r8d, %r8d
	je	.L97
	.p2align 4,,10
	.p2align 3
.L115:
	movzwl	(%rdi,%r10,2), %r11d
	leal	335(%r11), %r14d
	cmpw	$13, %r14w
	ja	.L97
	subl	$65201, %r11d
	movslq	%r11d, %r11
	cmpb	$0, (%rsi,%r11)
	je	.L97
	testq	%r10, %r10
	je	.L101
	cmpw	$32, -2(%rdi,%r10,2)
	je	.L114
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$20, (%rdx)
.L102:
	leaq	1(%r10), %r11
	cmpq	%rax, %r10
	je	.L96
.L106:
	movq	%r11, %r10
	testl	%r8d, %r8d
	jne	.L115
.L97:
	testl	%ecx, %ecx
	je	.L100
	movzwl	(%rdi,%r10,2), %r11d
	leal	375(%r11), %r14d
	cmpw	$1, %r14w
	jbe	.L116
.L100:
	testl	%r9d, %r9d
	je	.L102
	movzwl	2(%rdi,%r10,2), %r11d
	leal	267(%r11), %r14d
	cmpw	$7, %r14w
	ja	.L102
	cmpw	$32, (%rdi,%r10,2)
	jne	.L101
	subl	$65269, %r11d
	movl	$1604, %r14d
	movslq	%r11d, %r11
	movw	%r14w, 2(%rdi,%r10,2)
	movzwl	(%rbx,%r11,2), %r11d
	movw	%r11w, (%rdi,%r10,2)
	leaq	1(%r10), %r11
	cmpq	%rax, %r10
	jne	.L106
.L96:
	popq	%rbx
	movl	%r12d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	testq	%r10, %r10
	je	.L101
	cmpw	$32, -2(%rdi,%r10,2)
	jne	.L101
	subl	$65161, %r11d
	movslq	%r11d, %r11
	movzwl	0(%r13,%r11,2), %r11d
	movw	%r11w, (%rdi,%r10,2)
	movl	$-384, %r11d
	movw	%r11w, -2(%rdi,%r10,2)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L114:
	movzwl	16(%rbp), %r14d
	movw	%r14w, -2(%rdi,%r10,2)
	jmp	.L102
	.cfi_endproc
.LFE2628:
	.size	_ZL24expandCompositCharAtNearPDsiiP10UErrorCodeiii15uShapeVariables.isra.0, .-_ZL24expandCompositCharAtNearPDsiiP10UErrorCodeiii15uShapeVariables.isra.0
	.p2align 4
	.type	_ZL25expandCompositCharAtBeginPDsiiP10UErrorCode.isra.0, @function
_ZL25expandCompositCharAtBeginPDsiiP10UErrorCode.isra.0:
.LFB2630:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	2(%rsi,%rsi), %r15d
	pushq	%r14
	movslq	%r15d, %r15
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L132
	xorl	%esi, %esi
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	cmpw	$32, (%r12)
	jne	.L128
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L121:
	movl	%eax, %edi
	addq	$1, %rax
	cmpw	$32, -2(%r12,%rax,2)
	je	.L121
.L120:
	leal	-2(%r14), %ecx
	movl	%r14d, %edx
	leaq	_ZL14convertLamAlef(%rip), %r8
	movslq	%ecx, %rcx
	subl	$1, %edx
	jns	.L127
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L133:
	cmpw	$7, %r10w
	ja	.L124
	subl	$65269, %eax
	movl	$1604, %r10d
	subl	$1, %edi
	subl	$2, %edx
	cltq
	movw	%r10w, (%r9)
	movzwl	(%r8,%rax,2), %eax
	movw	%ax, -2(%r13,%rsi)
.L125:
	movl	%ecx, %esi
	movl	%edx, %eax
	subq	$1, %rcx
	shrl	$31, %esi
	shrl	$31, %eax
	orb	%al, %sil
	jne	.L126
.L127:
	movzwl	2(%r12,%rcx,2), %eax
	movslq	%edx, %rsi
	leal	-1(%rdx), %r11d
	addq	%rsi, %rsi
	leal	267(%rax), %r10d
	leaq	0(%r13,%rsi), %r9
	testl	%edi, %edi
	jne	.L133
	cmpw	$7, %r10w
	ja	.L124
	movl	$20, (%rbx)
.L124:
	movw	%ax, (%r9)
	movl	%r11d, %edx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%r12, %rdi
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	u_memcpy_67@PLT
	movq	%r13, %rdi
	call	uprv_free_67@PLT
.L119:
	addq	$8, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	xorl	%edi, %edi
	jmp	.L120
.L132:
	movl	$7, (%rbx)
	xorl	%r14d, %r14d
	jmp	.L119
	.cfi_endproc
.LFE2630:
	.size	_ZL25expandCompositCharAtBeginPDsiiP10UErrorCode.isra.0, .-_ZL25expandCompositCharAtBeginPDsiiP10UErrorCode.isra.0
	.p2align 4
	.type	_ZL23expandCompositCharAtEndPDsiiP10UErrorCode.isra.0, @function
_ZL23expandCompositCharAtEndPDsiiP10UErrorCode.isra.0:
.LFB2631:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leal	2(%rsi,%rsi), %r12d
	pushq	%rbx
	movslq	%r12d, %r12
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	call	uprv_malloc_67@PLT
	testq	%rax, %rax
	je	.L155
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leal	-1(%r15), %r8d
	cmpw	$32, -4(%r14,%r12)
	movl	%r8d, -52(%rbp)
	jne	.L137
	leaq	-2(%r14,%r12), %rax
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L139:
	subq	$2, %rax
	addl	$1, %r12d
	cmpw	$32, -2(%rax)
	je	.L139
	movl	%r15d, %eax
	subl	%r12d, %eax
	subl	$1, %eax
	testl	%r8d, %r8d
	js	.L145
	testl	%eax, %eax
	js	.L145
.L150:
	subl	$1, %eax
	movl	%r8d, %ecx
	leaq	_ZL14convertLamAlef(%rip), %r11
	cltq
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L157:
	cmpw	$7, %di
	ja	.L142
	subl	$65269, %edx
	movl	$1604, %edi
	subl	$1, %r12d
	subl	$2, %ecx
	movslq	%edx, %rdx
	movw	%di, (%r10)
	movzwl	(%r11,%rdx,2), %edx
	movw	%dx, -2(%r13,%rsi)
.L143:
	movl	%eax, %esi
	movl	%ecx, %edx
	subq	$1, %rax
	shrl	$31, %esi
	shrl	$31, %edx
	movl	%esi, %edi
	orb	%dl, %dil
	jne	.L156
.L144:
	movzwl	2(%r14,%rax,2), %edx
	movslq	%ecx, %rsi
	leal	-1(%rcx), %r9d
	addq	%rsi, %rsi
	leal	267(%rdx), %edi
	leaq	0(%r13,%rsi), %r10
	testl	%r12d, %r12d
	jne	.L157
	cmpw	$7, %di
	ja	.L142
	movl	$20, (%rbx)
.L142:
	movw	%dx, (%r10)
	movl	%r9d, %ecx
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L156:
	testl	%r12d, %r12d
	jne	.L145
.L140:
	movq	%r14, %rdi
	movl	%r15d, %edx
	movq	%r13, %rsi
	call	u_memcpy_67@PLT
	movq	%r13, %rdi
	call	uprv_free_67@PLT
.L153:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movslq	%r12d, %rax
	movq	%r13, %rdi
	movl	%r15d, %edx
	movl	%r8d, -56(%rbp)
	leaq	0(%r13,%rax,2), %rsi
	call	u_memmove_67@PLT
	movq	%r13, %rdi
	call	u_strlen_67@PLT
	cmpl	%eax, %r15d
	jle	.L140
	movl	%r15d, %edx
	movl	-56(%rbp), %r8d
	subl	%r12d, %edx
	cmpl	%r8d, %edx
	jg	.L140
	movl	%r8d, %eax
	movl	%r15d, %esi
	subl	%edx, %eax
	subl	%edx, %esi
	cmpl	$6, %eax
	jbe	.L146
	movslq	%r8d, %rax
	movl	%esi, %ecx
	movdqa	.LC6(%rip), %xmm0
	leaq	-14(%r13,%rax,2), %rax
	shrl	$3, %ecx
	salq	$4, %rcx
	movq	%rax, %rbx
	subq	%rcx, %rbx
	movq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L148:
	movups	%xmm0, (%rax)
	subq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L148
	movl	%esi, %eax
	andl	$-8, %eax
	subl	%eax, %r8d
	movl	%r8d, -52(%rbp)
	cmpl	%esi, %eax
	je	.L140
.L146:
	movslq	-52(%rbp), %rax
	movl	$32, %r10d
	movw	%r10w, 0(%r13,%rax,2)
	movq	%rax, %rbx
	leal	-1(%rax), %eax
	cmpl	%eax, %edx
	jg	.L140
	cltq
	movl	$32, %r9d
	movw	%r9w, 0(%r13,%rax,2)
	leal	-2(%rbx), %eax
	cmpl	%eax, %edx
	jg	.L140
	cltq
	movl	$32, %r8d
	movw	%r8w, 0(%r13,%rax,2)
	leal	-3(%rbx), %eax
	cmpl	%eax, %edx
	jg	.L140
	cltq
	movl	$32, %edi
	movw	%di, 0(%r13,%rax,2)
	leal	-4(%rbx), %eax
	cmpl	%eax, %edx
	jg	.L140
	cltq
	movl	$32, %esi
	movw	%si, 0(%r13,%rax,2)
	leal	-5(%rbx), %eax
	cmpl	%eax, %edx
	jg	.L140
	cltq
	movl	$32, %ecx
	movw	%cx, 0(%r13,%rax,2)
	movl	%ebx, %eax
	subl	$6, %eax
	cmpl	%eax, %edx
	jg	.L140
	cltq
	movl	$32, %edx
	movw	%dx, 0(%r13,%rax,2)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L137:
	testl	%r15d, %r15d
	jle	.L140
	movl	%r8d, %eax
	xorl	%r12d, %r12d
	jmp	.L150
.L155:
	movl	$7, (%rbx)
	xorl	%r15d, %r15d
	jmp	.L153
	.cfi_endproc
.LFE2631:
	.size	_ZL23expandCompositCharAtEndPDsiiP10UErrorCode.isra.0, .-_ZL23expandCompositCharAtEndPDsiiP10UErrorCode.isra.0
	.p2align 4
	.type	_ZL12shapeUnicodePDsiijP10UErrorCodei15uShapeVariables.isra.0, @function
_ZL12shapeUnicodePDsiijP10UErrorCodei15uShapeVariables.isra.0:
.LFB2633:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	16(%rbp), %eax
	movl	%esi, -68(%rbp)
	movl	%edx, -100(%rbp)
	movl	%eax, -108(%rbp)
	movl	24(%rbp), %eax
	movq	%rcx, -128(%rbp)
	movl	%eax, -112(%rbp)
	movl	32(%rbp), %eax
	movl	%r8d, -80(%rbp)
	movl	%eax, -104(%rbp)
	movl	40(%rbp), %eax
	movw	%r9w, -130(%rbp)
	movl	%eax, -116(%rbp)
	movl	48(%rbp), %eax
	movl	%eax, -120(%rbp)
	andb	$-128, %dh
	je	.L159
	leal	-1(%rsi), %eax
	movl	%eax, -84(%rbp)
.L162:
	movslq	-84(%rbp), %rax
	movl	%eax, %r9d
	movzwl	(%r14,%rax,2), %eax
	leal	-1570(%rax), %edx
	movl	%eax, %ecx
	cmpw	$177, %dx
	jbe	.L404
	cmpw	$8205, %ax
	je	.L292
	leal	-8301(%rax), %edx
	movl	$4, %esi
	cmpw	$2, %dx
	jbe	.L166
	leal	1200(%rax), %edx
	cmpw	$274, %dx
	jbe	.L405
	leal	400(%rax), %edx
	xorl	%esi, %esi
	cmpw	$140, %dx
	jbe	.L406
.L166:
	movl	-84(%rbp), %eax
	cmpl	$-1, %eax
	je	.L295
.L426:
	movl	$0, -96(%rbp)
	movl	%eax, %r12d
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	movl	$0, -76(%rbp)
	movl	$-2, %eax
	xorl	%r15d, %r15d
	movl	$0, -88(%rbp)
	movl	$0, -92(%rbp)
	.p2align 4,,10
	.p2align 3
.L169:
	leal	-1(%r9), %edx
	testw	$-256, %si
	jne	.L171
.L411:
	leal	-1570(%rcx), %r10d
	movzwl	%cx, %edi
	cmpw	$177, %r10w
	jbe	.L407
	cmpw	$8205, %cx
	je	.L174
	leal	-8301(%rcx), %r10d
	cmpw	$2, %r10w
	jbe	.L171
	leal	1200(%rcx), %r10d
	cmpw	$274, %r10w
	jbe	.L408
	leal	400(%rcx), %r10d
	cmpw	$140, %r10w
	ja	.L174
	subl	$65136, %edi
	leaq	_ZL9presBLink(%rip), %rbx
	movslq	%edi, %rdi
	movzbl	(%rbx,%rdi), %edi
.L173:
	andl	$4, %edi
	jne	.L171
.L174:
	movl	%esi, %r10d
	andl	$4, %r10d
.L177:
	testw	%r10w, %r10w
	je	.L204
	movl	%r15d, %esi
	movl	%r12d, %r9d
	movl	%r13d, %r15d
.L204:
	cmpl	%edx, %eax
	je	.L205
	cmpl	$-1, %edx
	je	.L170
.L420:
	movslq	%edx, %rcx
	movzwl	(%r14,%rcx,2), %edi
	leal	-1570(%rdi), %r10d
	movl	%edi, %ecx
	cmpw	$177, %r10w
	jbe	.L409
	cmpw	$8205, %di
	je	.L310
	leal	-8301(%rdi), %r11d
	movl	$4, %r10d
	cmpw	$2, %r11w
	jbe	.L207
	leal	1200(%rdi), %r10d
	cmpw	$274, %r10w
	jbe	.L410
	leal	400(%rdi), %r11d
	xorl	%r10d, %r10d
	cmpw	$140, %r11w
	ja	.L207
	subl	$65136, %edi
	leaq	_ZL9presBLink(%rip), %rbx
	movslq	%edi, %rdi
	movzbl	(%rbx,%rdi), %r10d
	.p2align 4,,10
	.p2align 3
.L207:
	movl	%r15d, %r13d
	movl	%r9d, %r12d
	movl	%esi, %r15d
	movl	%edx, %r9d
	movl	%r10d, %esi
	leal	-1(%r9), %edx
	testw	$-256, %si
	je	.L411
.L171:
	movslq	%r9d, %rdi
	leaq	(%rdi,%rdi), %r11
	leaq	(%r14,%r11), %rbx
	movq	%rbx, -64(%rbp)
	movl	%esi, %ebx
	andl	$32, %ebx
	testl	%eax, %eax
	jns	.L178
	movl	%edx, %eax
	cmpl	$-1, %edx
	jne	.L180
	xorl	%edi, %edi
	movl	$3000, %eax
	xorl	%r8d, %r8d
	testw	%bx, %bx
	je	.L287
.L188:
	testb	$16, %r15b
	jne	.L190
.L402:
	movzwl	%si, %ebx
	movl	%esi, %r10d
	movl	%ebx, -72(%rbp)
	movl	%esi, %ebx
	andl	$4, %r10d
	andl	$3, %ebx
	movw	%bx, -52(%rbp)
	movl	%esi, %ebx
	andl	$3, %ebx
	movl	%ebx, -56(%rbp)
.L189:
	testl	%r9d, %r9d
	jle	.L191
	cmpw	$32, -2(%r14,%r11)
	je	.L412
.L192:
	movq	%r15, %rbx
	movslq	%edi, %rdi
	movslq	-56(%rbp), %r11
	andl	$3, %ebx
	cmpw	$1, -52(%rbp)
	leaq	(%rbx,%rdi,4), %rbx
	leaq	_ZL10shapeTable(%rip), %rdi
	leaq	(%rdi,%rbx,4), %rdi
	movzbl	(%rdi,%r11), %edi
	je	.L413
	leal	-1611(%rcx), %r11d
	cmpw	$7, %r11w
	ja	.L195
	testb	$2, %r15b
	je	.L196
	cmpl	$1, -80(%rbp)
	jne	.L196
	testb	$1, %r8b
	jne	.L414
.L196:
	cmpl	$2, -80(%rbp)
	jne	.L318
	movl	$1, %edi
	cmpw	$1617, %cx
	je	.L198
.L318:
	xorl	%edi, %edi
.L198:
	cmpl	$2, -80(%rbp)
	movzwl	%cx, %r11d
	jne	.L201
	cmpw	$1617, %cx
	jne	.L415
.L201:
	leal	-1611(%r11), %ecx
	leaq	_ZL13IrrelevantPos(%rip), %r11
	movq	-64(%rbp), %rbx
	movslq	%ecx, %rcx
	movzbl	(%r11,%rcx), %ecx
	leal	-400(%rcx,%rdi), %edi
	movw	%di, (%rbx)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L180:
	movslq	%eax, %rdi
	movzwl	(%r14,%rdi,2), %r8d
	leal	-1570(%r8), %r10d
	movl	%r8d, %edi
	cmpw	$177, %r10w
	jbe	.L416
	cmpw	$8205, %r8w
	je	.L297
	leal	-8301(%r8), %r10d
	cmpw	$2, %r10w
	jbe	.L186
	leal	1200(%r8), %r10d
	cmpw	$274, %r10w
	jbe	.L417
	addw	$400, %di
	cmpw	$140, %di
	ja	.L298
	leal	-65136(%r8), %edi
	leaq	_ZL9presBLink(%rip), %r8
	movslq	%edi, %rdi
	movzbl	(%r8,%rdi), %r8d
.L184:
	testb	$4, %r8b
	je	.L185
.L186:
	subl	$1, %eax
.L181:
	cmpl	$-1, %eax
	jne	.L180
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movl	$3000, %eax
.L182:
	testw	%bx, %bx
	jne	.L188
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L205:
	cmpl	$-1, %eax
	je	.L170
.L290:
	movslq	%eax, %rdx
	movl	%r15d, %r13d
	movl	%r9d, %r12d
	movl	%esi, %r15d
	movl	%eax, %r9d
	movzwl	(%r14,%rdx,2), %ecx
	movl	%r8d, %esi
	movl	$-2, %eax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L407:
	subl	$1570, %edi
	leaq	_ZL7araLink(%rip), %rbx
	movslq	%edi, %rdi
	movzwl	(%rbx,%rdi,2), %edi
	jmp	.L173
.L298:
	xorl	%r8d, %r8d
.L185:
	testl	%eax, %eax
	js	.L181
.L178:
	movl	%r8d, %edi
	andl	$3, %edi
	jmp	.L182
.L191:
	jne	.L192
	movl	%r8d, %ebx
	.p2align 4,,10
	.p2align 3
.L287:
	movzwl	%si, %r8d
	movzwl	(%r14), %r11d
	movl	%esi, %r10d
	movl	$0, %r9d
	movl	%r8d, -72(%rbp)
	movl	%esi, %r8d
	andl	$4, %r10d
	andl	$3, %r8d
	movw	%r8w, -52(%rbp)
	movl	%esi, %r8d
	andl	$3, %r8d
	movl	%r8d, -56(%rbp)
	leal	-1587(%r11), %r8d
	cmpw	$3, %r8w
	movl	%ebx, %r8d
	jbe	.L304
	cmpw	$1574, %r11w
	movl	$1, %ebx
	cmovne	-76(%rbp), %ebx
	movl	%ebx, -76(%rbp)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L413:
	movl	%ecx, %r11d
	xorw	$1536, %r11w
	cmpw	$255, %r11w
	ja	.L177
	leal	-1611(%rcx), %r11d
	andl	$1, %edi
	cmpw	$7, %r11w
	jbe	.L198
	movl	-72(%rbp), %ecx
	sarl	$8, %ecx
	testb	$8, %sil
	jne	.L418
.L202:
	testl	%ecx, %ecx
	je	.L177
	testw	%r10w, %r10w
	je	.L419
	movl	%r15d, %esi
	movl	%r12d, %r9d
	movl	%r13d, %r15d
.L203:
	cmpl	%edx, %eax
	je	.L290
	cmpl	$-1, %edx
	jne	.L420
	.p2align 4,,10
	.p2align 3
.L170:
	movl	-88(%rbp), %r12d
	movl	-92(%rbp), %esi
	orl	-76(%rbp), %r12d
	orl	-96(%rbp), %esi
	movl	-68(%rbp), %eax
	jne	.L421
.L209:
	testl	%r12d, %r12d
	jne	.L422
.L158:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	xorb	$6, %ch
	cmpw	$255, %cx
	ja	.L177
	movl	-72(%rbp), %ecx
	sarl	$8, %ecx
	testb	$8, %sil
	je	.L202
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L416:
	subl	$1570, %r8d
	leaq	_ZL7araLink(%rip), %rdi
	movslq	%r8d, %r8
	movzwl	(%rdi,%r8,2), %r8d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L409:
	subl	$1570, %edi
	leaq	_ZL7araLink(%rip), %rbx
	movslq	%edi, %rdi
	movzwl	(%rbx,%rdi,2), %r10d
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L412:
	leal	-1587(%rcx), %ebx
	cmpw	$3, %bx
	ja	.L423
.L304:
	movl	$1, -88(%rbp)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L190:
	leal	-1570(%rcx), %esi
	cmpw	$5, %si
	ja	.L299
	movzwl	%si, %esi
	leaq	CSWTCH.60(%rip), %r10
	movzwl	(%r10,%rsi,2), %r10d
	testw	%r10w, %r10w
	je	.L300
	movq	-64(%rbp), %rsi
	movl	$-1, %edx
	movzwl	%r10w, %ecx
	movw	%dx, (%rsi)
	movslq	%r12d, %rdx
	leaq	(%rdx,%rdx), %r11
	leal	-1(%r12), %edx
	leaq	(%r14,%r11), %rsi
	movq	%rsi, -64(%rbp)
	movw	%r10w, (%rsi)
	leal	-1570(%r10), %esi
	cmpw	$177, %si
	ja	.L301
	subl	$1570, %ecx
	leaq	_ZL7araLink(%rip), %rsi
	movl	%r13d, %r15d
	movl	%r12d, %r9d
	movslq	%ecx, %rcx
	movl	$1, -92(%rbp)
	movzwl	(%rsi,%rcx,2), %ecx
	movl	%ecx, %esi
	movl	%ecx, -72(%rbp)
	andl	$3, %ecx
	movw	%cx, -52(%rbp)
	movl	%esi, %ecx
	andl	$3, %ecx
	movl	%ecx, -56(%rbp)
	movl	%r10d, %ecx
	movl	%esi, %r10d
	andl	$4, %r10d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L408:
	subl	$64336, %edi
	leaq	_ZL9presALink(%rip), %rbx
	movslq	%edi, %rdi
	movzbl	(%rbx,%rdi), %edi
	jmp	.L173
.L417:
	leal	-64336(%r8), %edi
	leaq	_ZL9presALink(%rip), %r8
	movslq	%edi, %rdi
	movzbl	(%r8,%rdi), %r8d
	jmp	.L184
.L410:
	subl	$64336, %edi
	leaq	_ZL9presALink(%rip), %rbx
	movslq	%edi, %rdi
	movzbl	(%rbx,%rdi), %r10d
	jmp	.L207
.L159:
	movl	-68(%rbp), %ecx
	movl	%esi, %eax
	testl	%ecx, %ecx
	jle	.L424
	subl	$1, %eax
	movq	%rdi, %rdx
	leaq	_ZL13convertFEto06(%rip), %r8
	leaq	2(%rdi,%rax,2), %rsi
	movl	%eax, -84(%rbp)
	leaq	_ZL13convertFBto06(%rip), %rdi
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L425:
	subl	$64336, %eax
	cltq
	movzwl	(%rdi,%rax,2), %eax
	testw	%ax, %ax
	je	.L164
.L400:
	movw	%ax, (%rdx)
.L164:
	addq	$2, %rdx
	cmpq	%rdx, %rsi
	je	.L162
.L165:
	movzwl	(%rdx), %eax
	leal	1200(%rax), %ecx
	cmpw	$175, %cx
	jbe	.L425
	leal	400(%rax), %ecx
	cmpw	$140, %cx
	ja	.L164
	subl	$65136, %eax
	cltq
	movzwl	(%r8,%rax,2), %eax
	jmp	.L400
.L299:
	movl	%r13d, %r15d
.L401:
	xorl	%ebx, %ebx
	movl	$0, -56(%rbp)
	xorl	%r10d, %r10d
	xorl	%esi, %esi
	movw	%bx, -52(%rbp)
	movl	$0, -72(%rbp)
	movl	$1, -92(%rbp)
	jmp	.L189
.L418:
	movq	-64(%rbp), %rbx
	leal	-1200(%rcx,%rdi), %edi
	movw	%di, (%rbx)
	jmp	.L177
.L423:
	cmpw	$1574, %cx
	movl	$1, %ebx
	cmovne	-76(%rbp), %ebx
	movl	%ebx, -76(%rbp)
	jmp	.L192
.L415:
	movq	-64(%rbp), %rdi
	movl	$-2, %r11d
	movl	$1, -96(%rbp)
	movw	%r11w, (%rdi)
	jmp	.L177
.L404:
	subl	$1570, %eax
	leaq	_ZL7araLink(%rip), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %esi
	movl	-84(%rbp), %eax
	cmpl	$-1, %eax
	jne	.L426
.L295:
	addq	$104, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L422:
	.cfi_restore_state
	movl	-100(%rbp), %esi
	movl	%esi, %edx
	andl	$58720256, %esi
	andl	$7340032, %edx
	cmpl	$16777216, %esi
	je	.L427
	cmpl	$2097152, %edx
	jne	.L158
	xorl	%ecx, %ecx
.L275:
	movzwl	-130(%rbp), %eax
	movl	-68(%rbp), %esi
	movl	%r12d, %r8d
	movq	%r14, %rdi
	movq	-128(%rbp), %rdx
	xorl	%r9d, %r9d
	movw	%ax, 16(%rbp)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZL24expandCompositCharAtNearPDsiiP10UErrorCodeiii15uShapeVariables.isra.0
.L421:
	.cfi_restore_state
	movl	%eax, %esi
	movl	-100(%rbp), %eax
	leal	2(%rsi,%rsi), %ebx
	movl	%eax, %r13d
	movslq	%ebx, %rbx
	andl	$917504, %eax
	movl	%eax, -52(%rbp)
	andl	$65539, %r13d
	movq	%rbx, %rdi
	cmpl	$524288, %eax
	je	.L210
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L276
	testl	%r13d, %r13d
	je	.L212
	movl	-68(%rbp), %eax
.L213:
	cmpl	$1, %r13d
	je	.L428
	cmpl	%r13d, -108(%rbp)
	jne	.L231
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	memset@PLT
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	jns	.L429
.L232:
	movl	-68(%rbp), %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	u_memcpy_67@PLT
	movl	-68(%rbp), %eax
.L236:
	cmpl	%r13d, -112(%rbp)
	je	.L254
.L281:
	cmpl	$65536, %r13d
	jne	.L283
	testb	$1, -120(%rbp)
	je	.L254
.L283:
	movl	-52(%rbp), %edi
	cmpl	%edi, -116(%rbp)
	je	.L430
.L256:
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	uprv_free_67@PLT
	movl	-52(%rbp), %eax
	jmp	.L209
.L297:
	movl	$3, %r8d
	testl	%eax, %eax
	js	.L181
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$3, %r10d
	jmp	.L207
.L300:
	xorl	%r15d, %r15d
	movl	$0, -56(%rbp)
	xorl	%esi, %esi
	movw	%r15w, -52(%rbp)
	movl	%r13d, %r15d
	movl	$0, -72(%rbp)
	movl	$1, -92(%rbp)
	jmp	.L189
.L406:
	subl	$65136, %eax
	leaq	_ZL9presBLink(%rip), %rdx
	cltq
	movzbl	(%rdx,%rax), %esi
	jmp	.L166
.L419:
	movq	-64(%rbp), %rbx
	leal	-400(%rcx,%rdi), %edi
	movw	%di, (%rbx)
	jmp	.L203
.L231:
	cmpl	$65536, %r13d
	movl	-52(%rbp), %edi
	sete	%cl
	andb	-120(%rbp), %cl
	cmpl	%edi, -104(%rbp)
	je	.L234
	testb	%cl, %cl
	je	.L236
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	memset@PLT
	cmpl	$0, -68(%rbp)
	jns	.L288
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L414:
	leal	-1612(%rcx), %r11d
	xorl	%edi, %edi
	cmpw	$1, %r11w
	jbe	.L198
	movl	$1, %edi
	testb	$32, %r8b
	je	.L198
	movl	%r15d, %edi
	shrw	$4, %di
	xorl	$1, %edi
	andl	$1, %edi
	jmp	.L198
.L424:
	subl	$1, %eax
	movl	%eax, -84(%rbp)
	jmp	.L162
.L405:
	subl	$64336, %eax
	leaq	_ZL9presALink(%rip), %rdx
	cltq
	movzbl	(%rdx,%rax), %esi
	jmp	.L166
.L254:
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	memset@PLT
	movl	-68(%rbp), %r13d
	testl	%r13d, %r13d
	jle	.L257
	movl	-84(%rbp), %edx
	movl	-52(%rbp), %esi
	movq	%r14, %rax
	leaq	2(%r14,%rdx,2), %rdi
	xorl	%edx, %edx
	cmpl	%esi, -116(%rbp)
	movl	$0, %esi
	je	.L270
	jmp	.L267
.L265:
	movslq	%esi, %r8
	addl	$1, %esi
	movw	%cx, (%r15,%r8,2)
.L266:
	addq	$2, %rax
	cmpq	%rdi, %rax
	je	.L262
.L267:
	movzwl	(%rax), %ecx
	cmpw	$-1, %cx
	jne	.L265
	addl	$1, %edx
	jmp	.L266
.L268:
	movslq	%esi, %r8
	addl	$1, %esi
	movw	%cx, (%r15,%r8,2)
.L269:
	addq	$2, %rax
	cmpq	%rax, %rdi
	je	.L262
.L270:
	movzwl	(%rax), %ecx
	cmpw	$-3, %cx
	jbe	.L268
	addl	$1, %edx
	jmp	.L269
.L428:
	movl	-68(%rbp), %esi
	testl	%esi, %esi
	jle	.L227
	movl	-84(%rbp), %edx
	movq	%r14, %rax
	leaq	2(%r14,%rdx,2), %rdx
	.p2align 4,,10
	.p2align 3
.L230:
	cmpw	$-1, (%rax)
	jne	.L228
	movl	$32, %ecx
	movw	%cx, (%rax)
.L228:
	addq	$2, %rax
	cmpq	%rax, %rdx
	jne	.L230
	movl	-52(%rbp), %esi
	cmpl	%esi, -104(%rbp)
	je	.L280
	movl	-68(%rbp), %eax
	jmp	.L281
.L210:
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L276
	movq	%r15, %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movl	-68(%rbp), %edi
	testl	%edi, %edi
	jle	.L214
	movl	-84(%rbp), %edx
	movq	%r14, %rax
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	leaq	2(%r14,%rdx,2), %r8
	testl	%r13d, %r13d
	je	.L225
	jmp	.L219
.L216:
	movslq	%ecx, %rsi
	addl	$1, %ecx
	movw	%dx, (%r15,%rsi,2)
.L217:
	addq	$2, %rax
	cmpq	%r8, %rax
	je	.L218
.L219:
	movzwl	(%rax), %edx
	cmpw	$-2, %dx
	jne	.L216
	addl	$1, %edi
	jmp	.L217
.L223:
	movslq	%ecx, %rsi
	addl	$1, %ecx
	movw	%dx, (%r15,%rsi,2)
.L224:
	addq	$2, %rax
	cmpq	%rax, %r8
	je	.L218
.L225:
	movzwl	(%rax), %edx
	cmpw	$-3, %dx
	jbe	.L223
	addl	$1, %edi
	jmp	.L224
.L427:
	cmpl	$2097152, %edx
	movl	%r12d, %ecx
	sete	%r12b
	movzbl	%r12b, %r12d
	jmp	.L275
.L218:
	movslq	%edi, %rdi
	xorl	%esi, %esi
	leaq	2(%rdi,%rdi), %rdx
	negq	%rdi
	leaq	-2(%rbx,%rdi,2), %rdi
	addq	%r15, %rdi
	call	memset@PLT
.L284:
	movl	-68(%rbp), %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	u_memcpy_67@PLT
	movq	%r14, %rdi
	call	u_strlen_67@PLT
	jmp	.L213
.L262:
	leal	1(%rdx), %esi
	cmpl	$6, %edx
	jle	.L315
	movl	%esi, %ecx
	leaq	-16(%r15,%rbx), %rax
	movdqa	.LC6(%rip), %xmm0
	shrl	$3, %ecx
	movq	%rax, %rdi
	salq	$4, %rcx
	subq	%rcx, %rdi
	movq	%rdi, %rcx
.L272:
	movups	%xmm0, (%rax)
	subq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L272
	movl	-68(%rbp), %eax
	movl	%esi, %ecx
	andl	$-8, %ecx
	subl	%ecx, %edx
	subl	%ecx, %eax
	cmpl	%ecx, %esi
	je	.L278
.L271:
	movslq	%eax, %rcx
	movl	$32, %ebx
	movw	%bx, (%r15,%rcx,2)
	leal	-1(%rax), %ecx
	testl	%edx, %edx
	je	.L278
	movslq	%ecx, %rcx
	movl	$32, %r11d
	movw	%r11w, (%r15,%rcx,2)
	leal	-2(%rax), %ecx
	cmpl	$1, %edx
	je	.L278
	movslq	%ecx, %rcx
	movl	$32, %r10d
	movw	%r10w, (%r15,%rcx,2)
	leal	-3(%rax), %ecx
	cmpl	$2, %edx
	je	.L278
	movslq	%ecx, %rcx
	movl	$32, %r9d
	movw	%r9w, (%r15,%rcx,2)
	leal	-4(%rax), %ecx
	cmpl	$3, %edx
	je	.L278
	movslq	%ecx, %rcx
	movl	$32, %r8d
	subl	$5, %eax
	movw	%r8w, (%r15,%rcx,2)
	cmpl	$4, %edx
	je	.L278
	cltq
	movl	$32, %edi
	movw	%di, (%r15,%rax,2)
	leaq	(%rax,%rax), %rcx
	cmpl	$5, %edx
	je	.L278
	movl	$32, %esi
	movw	%si, -2(%r15,%rcx)
.L278:
	movl	-68(%rbp), %ebx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movl	%ebx, %edx
	call	u_memcpy_67@PLT
	movl	%ebx, %eax
	jmp	.L256
.L292:
	movl	$3, %esi
	jmp	.L166
.L430:
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	memset@PLT
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	jle	.L257
	movl	-84(%rbp), %edx
	movq	%r14, %rax
	xorl	%esi, %esi
	leaq	2(%r14,%rdx,2), %rdi
	xorl	%edx, %edx
	jmp	.L263
.L260:
	movslq	%esi, %r8
	addl	$1, %esi
	movw	%cx, (%r15,%r8,2)
.L261:
	addq	$2, %rax
	cmpq	%rax, %rdi
	je	.L262
.L263:
	movzwl	(%rax), %ecx
	cmpw	$-2, %cx
	jne	.L260
	addl	$1, %edx
	jmp	.L261
.L280:
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	memset@PLT
.L279:
	movslq	-68(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	jmp	.L243
.L240:
	movslq	%esi, %rdi
	subl	$1, %esi
	movw	%cx, (%r15,%rdi,2)
.L241:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L242
.L243:
	movzwl	(%r14,%rax,2), %ecx
	cmpw	$-2, %cx
	jne	.L240
	addl	$1, %edx
	jmp	.L241
.L242:
	testl	%edx, %edx
	je	.L232
	leal	-1(%rdx), %eax
	cmpl	$6, %eax
	jbe	.L314
	movl	%edx, %ecx
	movdqa	.LC6(%rip), %xmm0
	movq	%r15, %rax
	shrl	$3, %ecx
	salq	$4, %rcx
	addq	%r15, %rcx
.L252:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rcx, %rax
	jne	.L252
	movl	%edx, %eax
	andl	$-8, %eax
	testb	$7, %dl
	je	.L232
.L250:
	movslq	%eax, %rcx
	movl	$32, %r11d
	movw	%r11w, (%r15,%rcx,2)
	leal	1(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L232
	movslq	%ecx, %rcx
	movl	$32, %r10d
	movw	%r10w, (%r15,%rcx,2)
	leal	2(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L232
	movslq	%ecx, %rcx
	movl	$32, %r9d
	movw	%r9w, (%r15,%rcx,2)
	leal	3(%rax), %ecx
	cmpl	%edx, %ecx
	jge	.L232
	movslq	%ecx, %rcx
	movl	$32, %r8d
	movw	%r8w, (%r15,%rcx,2)
	leal	4(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L232
	movslq	%ecx, %rcx
	movl	$32, %edi
	movw	%di, (%r15,%rcx,2)
	leal	5(%rax), %ecx
	cmpl	%ecx, %edx
	jle	.L232
	movslq	%ecx, %rcx
	movl	$32, %esi
	addl	$6, %eax
	movw	%si, (%r15,%rcx,2)
	cmpl	%edx, %eax
	jge	.L232
	cltq
	movl	$32, %edx
	movw	%dx, (%r15,%rax,2)
	jmp	.L232
.L429:
	movl	-52(%rbp), %esi
	cmpl	%esi, -104(%rbp)
	je	.L239
.L288:
	movslq	-68(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	jmp	.L246
.L244:
	movslq	%esi, %rdi
	subl	$1, %esi
	movw	%cx, (%r15,%rdi,2)
.L245:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L242
.L246:
	movzwl	(%r14,%rax,2), %ecx
	cmpw	$-1, %cx
	jne	.L244
	addl	$1, %edx
	jmp	.L245
.L234:
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movb	%cl, -56(%rbp)
	call	memset@PLT
	cmpl	$0, -68(%rbp)
	movzbl	-56(%rbp), %ecx
	js	.L232
	testb	%cl, %cl
	je	.L279
.L239:
	movslq	-68(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	jmp	.L249
.L247:
	movslq	%esi, %rdi
	subl	$1, %esi
	movw	%cx, (%r15,%rdi,2)
.L248:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L242
.L249:
	movzwl	(%r14,%rax,2), %ecx
	cmpw	$-3, %cx
	jbe	.L247
	addl	$1, %edx
	jmp	.L248
.L214:
	xorl	%eax, %eax
	movw	%ax, (%r15)
	jmp	.L284
.L227:
	movl	-52(%rbp), %esi
	cmpl	%esi, -104(%rbp)
	je	.L282
	movl	-68(%rbp), %eax
	jmp	.L283
.L257:
	movl	$32, %edx
	movw	%dx, (%r15)
	jmp	.L278
.L314:
	xorl	%eax, %eax
	jmp	.L250
.L315:
	movl	-68(%rbp), %eax
	jmp	.L271
.L276:
	movq	-128(%rbp), %rax
	movl	$7, (%rax)
	xorl	%eax, %eax
	jmp	.L209
.L212:
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	memset@PLT
	cmpl	$0, -68(%rbp)
	jle	.L214
	movl	-84(%rbp), %edx
	movq	%r14, %rax
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	leaq	2(%r14,%rdx,2), %r8
	jmp	.L222
.L220:
	movslq	%ecx, %rsi
	addl	$1, %ecx
	movw	%dx, (%r15,%rsi,2)
.L221:
	addq	$2, %rax
	cmpq	%rax, %r8
	je	.L218
.L222:
	movzwl	(%rax), %edx
	cmpw	$-1, %dx
	jne	.L220
	addl	$1, %edi
	jmp	.L221
.L301:
	movl	%r10d, %ecx
	movl	%r13d, %r15d
	movl	%r12d, %r9d
	jmp	.L401
.L282:
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	memset@PLT
	jmp	.L232
	.cfi_endproc
.LFE2633:
	.size	_ZL12shapeUnicodePDsiijP10UErrorCodei15uShapeVariables.isra.0, .-_ZL12shapeUnicodePDsiijP10UErrorCodei15uShapeVariables.isra.0
	.p2align 4
	.globl	u_shapeArabic_67
	.type	u_shapeArabic_67, @function
u_shapeArabic_67:
.LFB2100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$696, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	je	.L589
	movq	%r9, %r12
	movl	(%r9), %r9d
	testl	%r9d, %r9d
	jg	.L431
	movq	%rdi, %r14
	testq	%rdi, %rdi
	je	.L435
	movl	%esi, %r13d
	cmpl	$-1, %esi
	jl	.L435
	testq	%rdx, %rdx
	movq	%rdx, %r11
	movl	%ecx, %r10d
	sete	%dl
	testl	%ecx, %ecx
	setne	%al
	testb	%al, %dl
	jne	.L435
	testl	%ecx, %ecx
	js	.L435
	movl	%r8d, %ecx
	movl	%r8d, %ebx
	andl	$917504, %ecx
	je	.L437
	testb	$16, %r8b
	jne	.L435
.L437:
	testb	$2, %bh
	jne	.L435
	movl	%ebx, %eax
	andl	$224, %eax
	movl	%eax, -664(%rbp)
	cmpl	$160, %eax
	je	.L435
	movl	%ebx, %r15d
	movl	%ebx, %eax
	andl	$16384, %r15d
	andl	$65539, %eax
	movl	%eax, -672(%rbp)
	jne	.L761
	testl	%r15d, %r15d
	je	.L440
	movl	%ebx, %eax
	andl	$24, %eax
	cmpl	$24, %eax
	jne	.L435
.L440:
	testl	$131072, %ebx
	je	.L629
	cmpl	$393216, %ecx
	jne	.L435
.L629:
	cmpl	$-1, %r13d
	je	.L762
.L443:
	testl	%r13d, %r13d
	jle	.L763
	testq	%r11, %r11
	je	.L445
	cmpq	%r11, %r14
	jbe	.L764
.L446:
	movslq	%r10d, %rax
	leaq	(%r11,%rax,2), %rax
	cmpq	%rax, %r14
	jb	.L435
.L445:
	movl	%ebx, %eax
	andl	$134217728, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andw	$8600, %ax
	subw	$397, %ax
	movw	%ax, -688(%rbp)
	movl	%ebx, %eax
	andl	$24, %eax
	movl	%eax, -680(%rbp)
	je	.L448
	testl	%r15d, %r15d
	je	.L592
	movl	%ebx, %eax
	andl	$16408, %eax
	movl	%eax, -712(%rbp)
	testb	$4, %bl
	jne	.L765
	leal	0(,%r13,4), %edi
	movl	%r10d, -728(%rbp)
	movslq	%edi, %rdi
	movq	%r11, -720(%rbp)
	movl	%ecx, -696(%rbp)
	call	uprv_malloc_67@PLT
	movl	-696(%rbp), %ecx
	movq	-720(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, -704(%rbp)
	movl	-728(%rbp), %r10d
	je	.L576
	movq	%rax, %r15
	leal	-1(%r13), %eax
	xorl	%edx, %edx
	movq	%r11, -720(%rbp)
	leaq	2(%r14,%rax,2), %rax
	movl	%ecx, -696(%rbp)
	xorl	%r13d, %r13d
	xorl	%edi, %edi
	movl	%r10d, -728(%rbp)
	movl	$1, %r9d
	movl	$-1, %esi
	movq	%rax, %r11
	movl	-712(%rbp), %r10d
	movl	%ebx, -712(%rbp)
	movl	%edx, %ebx
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L455:
	cmpw	$8205, %dx
	je	.L593
	leal	-8301(%rdx), %r8d
	movl	$4, %ecx
	cmpw	$2, %r8w
	jbe	.L456
	leal	1200(%rdx), %ecx
	cmpw	$274, %cx
	jbe	.L766
	leal	400(%rdx), %r8d
	xorl	%ecx, %ecx
	cmpw	$140, %r8w
	ja	.L456
	subl	$65136, %edx
	leaq	_ZL9presBLink(%rip), %rcx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %ecx
	.p2align 4,,10
	.p2align 3
.L456:
	cmpl	$16408, %r10d
	je	.L767
.L458:
	addl	$1, %esi
	addl	$1, %r13d
	movl	%eax, %ebx
	movl	%ecx, %edi
	movslq	%esi, %rdx
	movl	$1, %r9d
	movw	%ax, (%r15,%rdx,2)
.L462:
	addq	$2, %r14
	cmpq	%r14, %r11
	je	.L768
.L464:
	movzwl	(%r14), %edx
	leal	-1570(%rdx), %ecx
	movl	%edx, %eax
	cmpw	$177, %cx
	ja	.L455
	subl	$1570, %edx
	leaq	_ZL7araLink(%rip), %rcx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %ecx
	jmp	.L456
.L765:
	leal	0(,%r13,4), %edi
	movl	%r10d, -728(%rbp)
	leal	(%r13,%r13), %r15d
	movslq	%edi, %rdi
	movq	%r11, -720(%rbp)
	movl	%ecx, -704(%rbp)
	call	uprv_malloc_67@PLT
	movl	-704(%rbp), %ecx
	movq	-720(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, -696(%rbp)
	movl	-728(%rbp), %r10d
	jne	.L769
.L576:
	movl	$7, (%r12)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L431:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L770
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore_state
	testl	%r15d, %r15d
	jne	.L435
	movl	%eax, %edi
	movl	%ebx, %eax
	andl	$65538, %eax
	cmpl	$2, %eax
	setne	%dl
	cmpl	$65536, %edi
	setne	%al
	testb	%al, %dl
	je	.L440
	cmpl	$1, %edi
	je	.L440
	.p2align 4,,10
	.p2align 3
.L435:
	movl	$1, (%r12)
	xorl	%eax, %eax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L589:
	xorl	%eax, %eax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L767:
	orl	%ecx, %edi
	andw	$192, %di
	cmpw	$192, %di
	jne	.L458
	testl	%r9d, %r9d
	je	.L458
	leal	-2542(%rax), %ecx
	cmpw	%bx, %ax
	leal	-2542(%rbx), %edx
	movslq	%esi, %rax
	cmovbe	%ecx, %edx
	movw	%dx, (%r15,%rax,2)
	leal	-1570(%rdx), %eax
	movzwl	%dx, %edi
	cmpw	$177, %ax
	jbe	.L771
	cmpw	$8205, %dx
	je	.L596
	leal	-8301(%rdx), %eax
	cmpw	$2, %ax
	jbe	.L597
	leal	1200(%rdx), %eax
	cmpw	$274, %ax
	jbe	.L772
	addw	$400, %dx
	cmpw	$140, %dx
	ja	.L598
	leal	-65136(%rdi), %eax
	leaq	_ZL9presBLink(%rip), %rdi
	xorl	%r9d, %r9d
	cltq
	movzbl	(%rdi,%rax), %edi
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L448:
	cmpl	%r10d, %r13d
	jg	.L759
	movq	%r11, %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	movl	%r10d, -680(%rbp)
	movq	%r11, -672(%rbp)
	call	u_memcpy_67@PLT
	movl	-680(%rbp), %r10d
	movq	-672(%rbp), %r11
.L553:
	movl	-664(%rbp), %esi
	testl	%esi, %esi
	je	.L556
	movl	%ebx, %eax
	movl	$1632, %edx
	andl	$768, %eax
	je	.L557
	cmpl	$256, %eax
	movl	$1776, %edx
	movl	$0, %eax
	cmovne	%eax, %edx
.L557:
	movl	-664(%rbp), %eax
	cmpl	$96, %eax
	je	.L558
	ja	.L559
	cmpl	$32, %eax
	je	.L560
	cmpl	$64, %eax
	jne	.L556
	testl	%r13d, %r13d
	jle	.L556
	leal	-1(%r13), %ecx
	movl	$48, %r8d
	movzwl	%dx, %edi
	movq	%r11, %rax
	addq	%rcx, %rcx
	subl	%edx, %r8d
	leaq	2(%r11,%rcx), %rsi
.L567:
	movzwl	(%rax), %edx
	movl	%edx, %ecx
	subl	%edi, %edx
	cmpl	$9, %edx
	ja	.L566
	addl	%r8d, %ecx
	movw	%cx, (%rax)
.L566:
	addq	$2, %rax
	cmpq	%rax, %rsi
	jne	.L567
.L556:
	movq	%r12, %rcx
	movl	%r13d, %edx
	movl	%r10d, %esi
	movq	%r11, %rdi
	call	u_terminateUChars_67@PLT
	jmp	.L431
.L764:
	movslq	%r13d, %rax
	leaq	(%r14,%rax,2), %rax
	cmpq	%rax, %r11
	jb	.L435
	cmpq	%r11, %r14
	jne	.L445
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L763:
	movq	%r12, %rcx
	xorl	%edx, %edx
	movl	%r10d, %esi
	movq	%r11, %rdi
	call	u_terminateUChars_67@PLT
	jmp	.L431
.L608:
	xorl	%eax, %eax
.L481:
	cmpl	%eax, %r13d
	jle	.L611
	movslq	%r13d, %rsi
	cmpw	$32, -2(%r15,%rsi,2)
	leaq	(%rsi,%rsi), %rdx
	jne	.L611
	addq	%r15, %rdx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L486:
	subq	$2, %rdx
	addl	$1, %esi
	cmpw	$32, -2(%rdx)
	je	.L486
	movl	%r13d, %edx
	subl	%esi, %edx
.L484:
	subl	$1, %edx
	cmpl	%eax, %edx
	jle	.L482
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L487:
	movzwl	(%r15,%rax,2), %esi
	movzwl	(%r15,%rdx,2), %edi
	movw	%di, (%r15,%rax,2)
	addq	$1, %rax
	movw	%si, (%r15,%rdx,2)
	subq	$1, %rdx
	cmpl	%eax, %edx
	jg	.L487
.L482:
	movl	-680(%rbp), %eax
	cmpl	$16, %eax
	je	.L573
	cmpl	$24, %eax
	je	.L623
	cmpl	$8, %eax
	je	.L624
.L580:
	movl	%r10d, %esi
	xorl	%edi, %edi
	movl	%r10d, -680(%rbp)
	xorl	%r13d, %r13d
	movq	%r11, -672(%rbp)
	call	uprv_min_67@PLT
	movq	-672(%rbp), %r11
	movq	%r15, %rsi
	movl	%eax, %edx
	movq	%r11, %rdi
	call	u_memcpy_67@PLT
	cmpq	-712(%rbp), %r15
	movq	-672(%rbp), %r11
	movl	-680(%rbp), %r10d
	je	.L553
.L578:
	movq	%r15, %rdi
	movl	%r10d, -680(%rbp)
	movq	%r11, -672(%rbp)
	call	uprv_free_67@PLT
	movl	-680(%rbp), %r10d
	movq	-672(%rbp), %r11
.L551:
	cmpl	%r13d, %r10d
	jge	.L553
.L759:
	movl	$15, (%r12)
	movl	%r13d, %eax
	jmp	.L431
.L762:
	movq	%r14, %rdi
	movl	%r10d, -696(%rbp)
	movq	%r11, -688(%rbp)
	movl	%ecx, -680(%rbp)
	call	u_strlen_67@PLT
	movl	-696(%rbp), %r10d
	movq	-688(%rbp), %r11
	movl	-680(%rbp), %ecx
	movl	%eax, %r13d
	jmp	.L443
.L766:
	subl	$64336, %edx
	leaq	_ZL9presALink(%rip), %rcx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %ecx
	jmp	.L456
.L592:
	movq	$0, -696(%rbp)
.L449:
	cmpl	$524288, %ecx
	je	.L630
	movl	-672(%rbp), %esi
	movl	%r13d, %eax
	testl	%esi, %esi
	jne	.L474
.L630:
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	movl	%r10d, -720(%rbp)
	movq	%r11, -712(%rbp)
	movl	%ecx, -704(%rbp)
	call	_ZL13calculateSizePKDsiij.isra.0
	movl	-720(%rbp), %r10d
	movq	-712(%rbp), %r11
	movl	-704(%rbp), %ecx
.L474:
	cmpl	%r10d, %eax
	jg	.L773
	cmpl	%eax, %r13d
	cmovge	%r13d, %eax
	movl	%eax, -704(%rbp)
	cmpl	$300, %eax
	jle	.L606
	leal	(%rax,%rax), %edi
	movl	%r10d, -732(%rbp)
	movslq	%edi, %rdi
	movq	%r11, -728(%rbp)
	movl	%ecx, -720(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L742
	leaq	-656(%rbp), %rax
	movl	-720(%rbp), %ecx
	movq	-728(%rbp), %r11
	movq	%rax, -712(%rbp)
	movl	-732(%rbp), %r10d
.L477:
	movq	%r15, %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	movl	%r10d, -732(%rbp)
	movq	%r11, -728(%rbp)
	movl	%ecx, -720(%rbp)
	call	u_memcpy_67@PLT
	movq	-696(%rbp), %rdi
	movl	-720(%rbp), %ecx
	movq	-728(%rbp), %r11
	movl	-732(%rbp), %r10d
	testq	%rdi, %rdi
	je	.L478
	movl	%r10d, -728(%rbp)
	movq	%r11, -720(%rbp)
	movl	%ecx, -696(%rbp)
	call	uprv_free_67@PLT
	movl	-728(%rbp), %r10d
	movq	-720(%rbp), %r11
	movl	-696(%rbp), %ecx
.L478:
	cmpl	-704(%rbp), %r13d
	jl	.L774
.L479:
	movl	%ebx, %eax
	andl	$4, %eax
	movl	%eax, -696(%rbp)
	jne	.L480
	cmpw	$32, (%r15)
	jne	.L608
	testl	%r13d, %r13d
	je	.L482
	leal	-1(%r13), %esi
	movl	$1, %edx
	addq	$2, %rsi
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L485:
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	je	.L611
.L483:
	cmpw	$32, (%r15,%rdx,2)
	movslq	%edx, %rax
	je	.L485
	jmp	.L481
.L559:
	cmpl	$128, -664(%rbp)
	jne	.L556
	shrl	$2, %ebx
	movq	%r11, %rdi
	movl	$1, %r8d
	movl	%r13d, %esi
	movl	%ebx, %ecx
	movl	%r10d, -672(%rbp)
	xorl	$1, %ecx
	movq	%r11, -664(%rbp)
	andl	$1, %ecx
	call	_ZL31_shapeToArabicDigitsWithContextPDsiDsaa
	movl	-672(%rbp), %r10d
	movq	-664(%rbp), %r11
	jmp	.L556
.L768:
	movq	-704(%rbp), %rax
	movl	-696(%rbp), %ecx
	movq	-720(%rbp), %r11
	movl	-728(%rbp), %r10d
	movq	%rax, -696(%rbp)
	movl	-712(%rbp), %ebx
	movq	%rax, %r14
	jmp	.L449
.L773:
	movq	-696(%rbp), %rdi
	movl	$15, (%r12)
	testq	%rdi, %rdi
	je	.L431
.L758:
	movl	%eax, -664(%rbp)
	call	uprv_free_67@PLT
	movl	-664(%rbp), %eax
	jmp	.L431
.L480:
	movl	%ebx, %esi
	andl	$67108864, %esi
	cmpl	$1, %esi
	sbbl	%r9d, %r9d
	addl	$1, %r9d
	cmpl	$1, %esi
	sbbl	%edx, %edx
	andl	$131072, %edx
	addl	$262144, %edx
	cmpl	$1, %esi
	sbbl	%eax, %eax
	andl	$-131072, %eax
	addl	$393216, %eax
	cmpl	$1, %esi
	sbbl	%edi, %edi
	addl	$3, %edi
	cmpl	$1, %esi
	sbbl	%r14d, %r14d
	notl	%r14d
	addl	$3, %r14d
	cmpl	$16, -680(%rbp)
	je	.L775
	movl	-680(%rbp), %esi
	cmpl	$24, %esi
	je	.L570
	cmpl	$8, %esi
	jne	.L580
.L571:
	movl	%r10d, -680(%rbp)
	movq	%r11, -672(%rbp)
	testl	%ecx, %ecx
	je	.L488
	cmpl	$786432, %ecx
	je	.L489
	pushq	%rcx
	movl	$2, %r8d
	pushq	%r9
	movzwl	-688(%rbp), %r9d
	pushq	%rdx
	pushq	%rax
	pushq	%rdi
	pushq	%r14
.L757:
	movl	%r13d, %esi
	movq	%r12, %rcx
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZL12shapeUnicodePDsiijP10UErrorCodei15uShapeVariables.isra.0
	movq	-672(%rbp), %r11
	movl	-680(%rbp), %r10d
	addq	$48, %rsp
	movl	%eax, %r13d
.L490:
	movl	-696(%rbp), %edi
	testl	%edi, %edi
	jne	.L543
	cmpw	$32, (%r15)
	jne	.L617
	testl	%r13d, %r13d
	jle	.L618
	leal	-1(%r13), %ecx
	movl	$1, %edx
	addq	$2, %rcx
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L547:
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	je	.L620
.L546:
	cmpw	$32, (%r15,%rdx,2)
	movslq	%edx, %rax
	je	.L547
.L544:
	cmpl	%r13d, %eax
	jge	.L620
	movslq	%r13d, %rcx
	cmpw	$32, -2(%r15,%rcx,2)
	leaq	(%rcx,%rcx), %rdx
	jne	.L620
	addq	%r15, %rdx
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L549:
	subq	$2, %rdx
	addl	$1, %ecx
	cmpw	$32, -2(%rdx)
	je	.L549
	movl	%r13d, %edx
	subl	%ecx, %edx
.L545:
	subl	$1, %edx
	cmpl	%eax, %edx
	jle	.L543
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L550:
	movzwl	(%r15,%rax,2), %ecx
	movzwl	(%r15,%rdx,2), %esi
	movw	%si, (%r15,%rax,2)
	addq	$1, %rax
	movw	%cx, (%r15,%rdx,2)
	subq	$1, %rdx
	cmpl	%eax, %edx
	jg	.L550
.L543:
	movl	%r10d, %esi
	movl	%r13d, %edi
	movl	%r10d, -680(%rbp)
	movq	%r11, -672(%rbp)
	call	uprv_min_67@PLT
	movq	-672(%rbp), %r11
	movq	%r15, %rsi
	movl	%eax, %edx
	movq	%r11, %rdi
	call	u_memcpy_67@PLT
	cmpq	-712(%rbp), %r15
	movq	-672(%rbp), %r11
	movl	-680(%rbp), %r10d
	jne	.L578
	jmp	.L551
.L606:
	leaq	-656(%rbp), %rax
	movl	$300, -704(%rbp)
	movq	%rax, -712(%rbp)
	movq	%rax, %r15
	jmp	.L477
.L593:
	movl	$3, %ecx
	jmp	.L456
.L774:
	movl	-704(%rbp), %edx
	movslq	%r13d, %rax
	xorl	%esi, %esi
	movl	%r10d, -728(%rbp)
	leaq	(%r15,%rax,2), %rdi
	movq	%r11, -720(%rbp)
	subl	%r13d, %edx
	movl	%ecx, -696(%rbp)
	addl	%edx, %edx
	movslq	%edx, %rdx
	call	memset@PLT
	movl	-728(%rbp), %r10d
	movq	-720(%rbp), %r11
	movl	-696(%rbp), %ecx
	jmp	.L479
.L611:
	movl	%r13d, %edx
	jmp	.L484
.L623:
	movl	$3, %r14d
	xorl	%r9d, %r9d
	movl	$393216, %edx
	movl	$2, %edi
	movl	$262144, %eax
.L570:
	subq	$8, %rsp
	movl	%r10d, -680(%rbp)
	xorl	%r8d, %r8d
	pushq	%r9
	movzwl	-688(%rbp), %r9d
	pushq	%rdx
	movq	%r11, -672(%rbp)
	pushq	%rax
	pushq	%rdi
	pushq	%r14
	jmp	.L757
.L795:
	xorl	%eax, %eax
.L620:
	movl	%r13d, %edx
	jmp	.L545
.L624:
	movl	$3, %r14d
	xorl	%r9d, %r9d
	movl	$393216, %edx
	movl	$2, %edi
	movl	$262144, %eax
	jmp	.L571
.L560:
	subl	$48, %edx
	testl	%r13d, %r13d
	jle	.L556
	leal	-1(%r13), %ecx
	movq	%r11, %rax
	addq	%rcx, %rcx
	leaq	2(%r11,%rcx), %rdi
.L565:
	movzwl	(%rax), %ecx
	movl	%ecx, %esi
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L564
	addl	%edx, %esi
	movw	%si, (%rax)
.L564:
	addq	$2, %rax
	cmpq	%rdi, %rax
	jne	.L565
	jmp	.L556
.L558:
	shrl	$2, %ebx
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	movl	%r13d, %esi
	movl	%ebx, %ecx
	movl	%r10d, -672(%rbp)
	xorl	$1, %ecx
	movq	%r11, -664(%rbp)
	andl	$1, %ecx
	call	_ZL31_shapeToArabicDigitsWithContextPDsiDsaa
	movq	-664(%rbp), %r11
	movl	-672(%rbp), %r10d
	jmp	.L556
.L769:
	leal	-1(%r13), %r9d
	movl	%r10d, -728(%rbp)
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	movl	-712(%rbp), %r10d
	movl	%ecx, -704(%rbp)
	movslq	%r9d, %r9
	xorl	%edi, %edi
	movl	%ebx, -712(%rbp)
	movl	$1, %r8d
	movq	-696(%rbp), %rbx
	movq	%r11, -720(%rbp)
	jmp	.L453
.L465:
	cmpw	$8205, %dx
	je	.L599
	leal	-8301(%rdx), %r11d
	movl	$4, %ecx
	cmpw	$2, %r11w
	jbe	.L466
	leal	1200(%rdx), %ecx
	cmpw	$274, %cx
	jbe	.L776
	leal	400(%rdx), %r11d
	xorl	%ecx, %ecx
	cmpw	$140, %r11w
	ja	.L466
	subl	$65136, %edx
	leaq	_ZL9presBLink(%rip), %rcx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %ecx
.L466:
	cmpl	$16408, %r10d
	je	.L777
.L468:
	subl	$1, %r15d
	addl	$1, %r13d
	movl	%eax, %esi
	movl	%ecx, %edi
	movslq	%r15d, %rdx
	movl	$1, %r8d
	leaq	(%rbx,%rdx,2), %rdx
	movw	%ax, (%rdx)
.L472:
	subq	$1, %r9
	cmpl	$-1, %r9d
	je	.L778
.L453:
	movzwl	(%r14,%r9,2), %edx
	leal	-1570(%rdx), %ecx
	movl	%edx, %eax
	cmpw	$177, %cx
	ja	.L465
	subl	$1570, %edx
	leaq	_ZL7araLink(%rip), %rcx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %ecx
	jmp	.L466
.L777:
	orl	%ecx, %edi
	andw	$192, %di
	cmpw	$192, %di
	jne	.L468
	testl	%r8d, %r8d
	je	.L468
	leal	-2542(%rax), %ecx
	cmpw	%si, %ax
	jbe	.L470
	leal	-2542(%rsi), %ecx
.L470:
	movslq	%r15d, %rdx
	leal	-1570(%rcx), %eax
	movzwl	%cx, %edi
	addq	%rdx, %rdx
	addq	%rbx, %rdx
	movw	%cx, (%rdx)
	cmpw	$177, %ax
	jbe	.L779
	cmpw	$8205, %cx
	je	.L602
	leal	-8301(%rcx), %eax
	cmpw	$2, %ax
	jbe	.L603
	leal	1200(%rcx), %eax
	cmpw	$274, %ax
	jbe	.L780
	addw	$400, %cx
	cmpw	$140, %cx
	ja	.L604
	leal	-65136(%rdi), %eax
	leaq	_ZL9presBLink(%rip), %rdi
	xorl	%r8d, %r8d
	cltq
	movzbl	(%rdi,%rax), %edi
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L776:
	subl	$64336, %edx
	leaq	_ZL9presALink(%rip), %rcx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %ecx
	jmp	.L466
.L778:
	movl	-704(%rbp), %ecx
	movq	-720(%rbp), %r11
	movq	%rdx, -704(%rbp)
	movq	%rdx, %r14
	movl	-728(%rbp), %r10d
	movl	-712(%rbp), %ebx
	jmp	.L449
.L775:
	movl	%ebx, %eax
	andl	$58720256, %eax
	movl	%eax, -680(%rbp)
	movl	%ebx, %eax
	andl	$7340032, %eax
	testl	%r13d, %r13d
	je	.L580
.L583:
	leal	-1(%r13), %esi
	movl	%esi, -704(%rbp)
	testl	%esi, %esi
	jle	.L612
	cmpl	%esi, %r13d
	movl	%edi, -732(%rbp)
	movq	%r15, %rcx
	movl	-680(%rbp), %edi
	cmovle	%r13d, %esi
	movl	%r9d, -736(%rbp)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, -720(%rbp)
	movl	%eax, %r9d
	movl	%r14d, -728(%rbp)
	jmp	.L513
.L782:
	subl	$64336, %eax
	leaq	_ZL13convertFBto06(%rip), %r15
	cltq
	movzwl	(%r15,%rax,2), %eax
	testw	%ax, %ax
	je	.L512
.L753:
	movw	%ax, (%rcx)
.L504:
	addw	$267, %ax
	cmpw	$7, %ax
	movl	$1, %eax
	cmovbe	%eax, %r8d
.L512:
	addl	$1, %edx
	addq	$2, %rcx
	cmpl	%esi, %edx
	jge	.L781
.L513:
	movzwl	(%rcx), %eax
	leal	1200(%rax), %r14d
	cmpw	$175, %r14w
	jbe	.L782
	cmpl	$16777216, %edi
	je	.L783
.L505:
	cmpl	$2097152, %r9d
	je	.L784
.L508:
	leal	400(%rax), %r14d
	cmpw	$132, %r14w
	ja	.L504
	subl	$65136, %eax
	leaq	_ZL13convertFEto06(%rip), %r15
	cltq
	movzwl	(%r15,%rax,2), %eax
	jmp	.L753
.L783:
	cmpw	$1569, %ax
	je	.L631
	cmpw	$-384, %ax
	jne	.L505
.L631:
	cmpl	%edx, -704(%rbp)
	jle	.L505
	movzwl	2(%rcx), %r14d
	leal	273(%r14), %r15d
	cmpw	$1, %r15w
	jbe	.L632
	cmpw	$1609, %r14w
	jne	.L505
.L632:
	movw	$32, (%rcx)
	movw	$1574, 2(%rcx)
	jmp	.L512
.L784:
	cmpw	$8203, %ax
	je	.L633
	cmpw	$-397, %ax
	jne	.L508
.L633:
	movzwl	2(%rcx), %r14d
	leal	335(%r14), %r15d
	cmpw	$13, %r15w
	ja	.L508
	subl	$65201, %r14d
	leaq	_ZL23tailFamilyIsolatedFinal(%rip), %r15
	movslq	%r14d, %r14
	cmpb	$0, (%r15,%r14)
	je	.L508
	movw	$32, (%rcx)
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L781:
	movq	-720(%rbp), %r15
	movl	-728(%rbp), %r14d
	movl	-732(%rbp), %edi
	movl	-736(%rbp), %r9d
	cmpl	%edx, %r13d
	jle	.L522
.L501:
	movl	%r14d, -720(%rbp)
	movslq	%edx, %rdx
	movl	-680(%rbp), %r14d
	movl	$1, %esi
	movl	%edi, -680(%rbp)
	jmp	.L523
.L786:
	subl	$64336, %eax
	leaq	_ZL13convertFBto06(%rip), %rcx
	cltq
	movzwl	(%rcx,%rax,2), %eax
	testw	%ax, %ax
	je	.L521
.L754:
	movw	%ax, (%r15,%rdx,2)
.L517:
	addw	$267, %ax
	cmpw	$7, %ax
	cmovbe	%esi, %r8d
.L521:
	addq	$1, %rdx
	cmpl	%edx, %r13d
	jle	.L785
.L523:
	movzwl	(%r15,%rdx,2), %eax
	leal	1200(%rax), %ecx
	cmpw	$175, %cx
	jbe	.L786
	cmpl	$16777216, %r14d
	je	.L787
.L518:
	leal	400(%rax), %ecx
	cmpw	$132, %cx
	ja	.L517
	subl	$65136, %eax
	leaq	_ZL13convertFEto06(%rip), %rcx
	cltq
	movzwl	(%rcx,%rax,2), %eax
	jmp	.L754
.L787:
	cmpw	$-384, %ax
	je	.L634
	cmpw	$1569, %ax
	jne	.L518
.L634:
	cmpl	%edx, -704(%rbp)
	jle	.L518
	movzwl	2(%r15,%rdx,2), %ecx
	leal	273(%rcx), %edi
	cmpw	$1, %di
	jbe	.L635
	cmpw	$1609, %cx
	jne	.L518
.L635:
	movw	$32, (%r15,%rdx,2)
	movw	$1574, 2(%r15,%rdx,2)
	jmp	.L521
.L785:
	movl	-720(%rbp), %r14d
	movl	-680(%rbp), %edi
.L522:
	testl	%r8d, %r8d
	je	.L490
	cmpl	$65536, -672(%rbp)
	je	.L788
	cmpl	%edi, -672(%rbp)
	je	.L789
	movl	-672(%rbp), %eax
	cmpl	%r14d, %eax
	je	.L534
	subl	$1, %eax
	jne	.L614
	movzwl	-688(%rbp), %eax
	pushq	%r8
	movl	%r13d, %esi
	xorl	%r8d, %r8d
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	pushq	%rax
	movl	%r10d, -680(%rbp)
	movq	%r11, -672(%rbp)
	call	_ZL24expandCompositCharAtNearPDsiiP10UErrorCodeiii15uShapeVariables.isra.0
	popq	%r9
	movq	-672(%rbp), %r11
	popq	%r10
	movl	%eax, %r13d
	movl	-680(%rbp), %r10d
	jmp	.L490
.L612:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L501
.L573:
	movl	%ebx, %eax
	andl	$58720256, %eax
	movl	%eax, -680(%rbp)
	movl	%ebx, %eax
	andl	$7340032, %eax
	testl	%r13d, %r13d
	je	.L586
	movl	$3, %r14d
	xorl	%r9d, %r9d
	movl	$2, %edi
	jmp	.L583
.L771:
	leal	-1570(%rdi), %eax
	leaq	_ZL7araLink(%rip), %rdi
	xorl	%r9d, %r9d
	cltq
	movzwl	(%rdi,%rax,2), %edi
	jmp	.L462
.L599:
	movl	$3, %ecx
	jmp	.L466
.L489:
	pushq	%r11
	movl	%r13d, %esi
	movl	$1, %r8d
	movq	%r12, %rcx
	pushq	%r9
	movzwl	-688(%rbp), %r9d
	pushq	%rdx
	movl	%ebx, %edx
	pushq	%rax
	pushq	%rdi
	movq	%r15, %rdi
	pushq	%r14
	call	_ZL12shapeUnicodePDsiijP10UErrorCodei15uShapeVariables.isra.0
	addq	$48, %rsp
	movq	-672(%rbp), %r11
	movl	-680(%rbp), %r10d
	testl	%eax, %eax
	movl	%eax, %r13d
	jle	.L490
	leal	-1(%rax), %eax
	movq	%r15, %rcx
	leaq	_ZL14tashkeelMedial(%rip), %rsi
	addq	%rax, %rax
	leaq	2(%r15,%rax), %rdi
	jmp	.L500
.L792:
	cmpw	$-397, %ax
	je	.L491
	movl	%eax, %r8d
	movzwl	%ax, %edx
	andl	$-9, %r8d
	cmpw	$-395, %r8w
	jne	.L790
	cmpw	$-387, %ax
	je	.L495
	cmpw	$-395, %ax
	jne	.L791
.L497:
	addq	$2, %rcx
	cmpq	%rcx, %rdi
	je	.L490
.L500:
	movzwl	(%rcx), %eax
	leal	400(%rax), %edx
	cmpw	$15, %dx
	jbe	.L792
.L491:
	leal	782(%rax), %edx
	cmpw	$2, %dx
	jbe	.L495
	cmpw	$-387, %ax
	je	.L495
	addw	$930, %ax
	cmpw	$5, %ax
	ja	.L497
.L498:
	movw	$32, (%rcx)
	jmp	.L497
.L488:
	movl	$1, %r8d
	pushq	%rcx
	pushq	%r9
	movzwl	-688(%rbp), %r9d
	pushq	%rdx
	pushq	%rax
	pushq	%rdi
	pushq	%r14
	jmp	.L757
.L495:
	movw	$-387, (%rcx)
	jmp	.L497
.L770:
	call	__stack_chk_fail@PLT
.L790:
	subl	$65136, %edx
	movslq	%edx, %r8
	movzbl	(%rsi,%r8), %r8d
	cmpl	$1, %r8d
	jne	.L793
	movw	$1600, (%rcx)
	jmp	.L497
.L598:
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	jmp	.L462
.L772:
	leal	-64336(%rdi), %eax
	leaq	_ZL9presALink(%rip), %rdi
	xorl	%r9d, %r9d
	cltq
	movzbl	(%rdi,%rax), %edi
	jmp	.L462
.L597:
	movl	$4, %edi
	xorl	%r9d, %r9d
	jmp	.L462
.L788:
	movl	%r10d, -680(%rbp)
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movq	%r11, -672(%rbp)
	testl	%r9d, %r9d
	jne	.L525
	call	_ZL23expandCompositCharAtEndPDsiiP10UErrorCode.isra.0
	cmpl	$20, (%r12)
	movq	-672(%rbp), %r11
	movl	-680(%rbp), %r10d
	je	.L794
.L615:
	movl	%eax, %r13d
	jmp	.L490
.L779:
	leal	-1570(%rdi), %eax
	leaq	_ZL7araLink(%rip), %rdi
	xorl	%r8d, %r8d
	cltq
	movzwl	(%rdi,%rax,2), %edi
	jmp	.L472
.L793:
	cmpl	$2, %r8d
	je	.L495
.L568:
	movslq	%edx, %rdx
	cmpb	$1, (%rsi,%rdx)
	je	.L497
	cmpw	$-388, %ax
	je	.L497
	jmp	.L498
.L596:
	movl	$3, %edi
	xorl	%r9d, %r9d
	jmp	.L462
.L586:
	cmpw	$32, (%r15)
	jne	.L795
.L618:
	movl	%r13d, %edx
	xorl	%eax, %eax
	jmp	.L545
.L789:
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	%r10d, -688(%rbp)
	movq	%r11, -680(%rbp)
	call	_ZL23expandCompositCharAtEndPDsiiP10UErrorCode.isra.0
	cmpl	%r14d, -672(%rbp)
	movq	-680(%rbp), %r11
	movl	-688(%rbp), %r10d
	jne	.L533
.L534:
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	%r10d, -688(%rbp)
	movq	%r11, -680(%rbp)
	call	_ZL25expandCompositCharAtBeginPDsiiP10UErrorCode.isra.0
	movq	-680(%rbp), %r11
	movl	-688(%rbp), %r10d
.L533:
	cmpl	$0, -672(%rbp)
	jne	.L615
	movl	%r13d, %esi
	movl	%ebx, %edx
	movq	%r15, %rdi
	movl	%r10d, -688(%rbp)
	movq	%r11, -680(%rbp)
	call	_ZL13calculateSizePKDsiij.isra.0
	movl	%eax, %r13d
	leal	2(%rax,%rax), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -672(%rbp)
	call	uprv_malloc_67@PLT
	movq	-672(%rbp), %rdx
	movq	-680(%rbp), %r11
	testq	%rax, %rax
	movl	-688(%rbp), %r10d
	movq	%rax, %r14
	je	.L796
	xorl	%esi, %esi
	movq	%rax, %rdi
	movl	%r10d, -680(%rbp)
	movq	%r11, -672(%rbp)
	call	memset@PLT
	xorl	%eax, %eax
	testl	%r13d, %r13d
	movq	-672(%rbp), %r11
	movl	-680(%rbp), %r10d
	movl	$1, %edx
	leaq	_ZL14convertLamAlef(%rip), %r9
	jg	.L539
	jmp	.L541
.L797:
	subl	$65269, %ecx
	addl	$2, %eax
	movslq	%ecx, %rcx
	movzwl	(%r9,%rcx,2), %ecx
	movw	%cx, (%rdi)
	movw	$1604, 2(%r14,%rsi)
.L756:
	cmpl	%edx, %eax
	movl	%edx, %ecx
	cmovg	%eax, %ecx
	addq	$1, %rdx
	cmpl	%ecx, %r13d
	jle	.L541
.L539:
	movzwl	-2(%r15,%rdx,2), %ecx
	movslq	%eax, %rsi
	addq	%rsi, %rsi
	leal	267(%rcx), %r8d
	leaq	(%r14,%rsi), %rdi
	cmpw	$7, %r8w
	jbe	.L797
	movw	%cx, (%rdi)
	addl	$1, %eax
	jmp	.L756
.L541:
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r10d, -680(%rbp)
	movq	%r11, -672(%rbp)
	call	u_memcpy_67@PLT
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	movq	-672(%rbp), %r11
	movl	-680(%rbp), %r10d
	jmp	.L490
.L791:
	subl	$65136, %edx
	jmp	.L568
.L617:
	xorl	%eax, %eax
	jmp	.L544
.L604:
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	jmp	.L472
.L780:
	leal	-64336(%rdi), %eax
	leaq	_ZL9presALink(%rip), %rdi
	xorl	%r8d, %r8d
	cltq
	movzbl	(%rdi,%rax), %edi
	jmp	.L472
.L603:
	movl	$4, %edi
	xorl	%r8d, %r8d
	jmp	.L472
.L525:
	call	_ZL25expandCompositCharAtBeginPDsiiP10UErrorCode.isra.0
	cmpl	$20, (%r12)
	movq	-672(%rbp), %r11
	movl	-680(%rbp), %r10d
	jne	.L615
	movl	$0, (%r12)
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZL23expandCompositCharAtEndPDsiiP10UErrorCode.isra.0
	movl	-680(%rbp), %r10d
	movq	-672(%rbp), %r11
.L527:
	cmpl	$20, (%r12)
	jne	.L615
	movl	$0, (%r12)
	xorl	%edx, %edx
	leaq	_ZL14convertLamAlef(%rip), %rcx
	jmp	.L531
.L530:
	movl	$20, (%r12)
.L529:
	addq	$1, %rdx
	cmpl	%edx, %r13d
	jle	.L798
.L531:
	movzwl	2(%r15,%rdx,2), %eax
	leal	267(%rax), %esi
	cmpw	$7, %si
	ja	.L529
	cmpw	$32, (%r15,%rdx,2)
	jne	.L530
	subl	$65269, %eax
	movw	$1604, 2(%r15,%rdx,2)
	cltq
	movzwl	(%rcx,%rax,2), %eax
	movw	%ax, (%r15,%rdx,2)
	jmp	.L529
.L798:
	movl	%r13d, %eax
	jmp	.L615
.L602:
	movl	$3, %edi
	xorl	%r8d, %r8d
	jmp	.L472
.L794:
	movl	$0, (%r12)
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZL25expandCompositCharAtBeginPDsiiP10UErrorCode.isra.0
	movq	-672(%rbp), %r11
	movl	-680(%rbp), %r10d
	jmp	.L527
.L796:
	movl	$7, (%r12)
	jmp	.L580
.L614:
	movl	%r13d, %eax
	jmp	.L533
.L742:
	movq	-696(%rbp), %rdi
	movl	$7, (%r12)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	jne	.L758
	jmp	.L431
	.cfi_endproc
.LFE2100:
	.size	u_shapeArabic_67, .-u_shapeArabic_67
	.section	.rodata
	.align 8
	.type	CSWTCH.60, @object
	.size	CSWTCH.60, 12
CSWTCH.60:
	.value	1628
	.value	1629
	.value	0
	.value	1630
	.value	0
	.value	1631
	.align 32
	.type	_ZL10shapeTable, @object
	.size	_ZL10shapeTable, 64
_ZL10shapeTable:
	.string	""
	.zero	3
	.string	""
	.zero	3
	.string	""
	.string	"\001"
	.ascii	"\003"
	.string	""
	.string	"\001"
	.ascii	"\001"
	.string	""
	.string	""
	.ascii	"\002\002"
	.string	""
	.string	""
	.ascii	"\001\002"
	.string	""
	.ascii	"\001\001\002"
	.string	""
	.ascii	"\001\001\003"
	.string	""
	.zero	3
	.string	""
	.zero	3
	.string	""
	.string	"\001"
	.ascii	"\003"
	.string	""
	.string	"\001"
	.ascii	"\003"
	.string	""
	.string	""
	.ascii	"\001\002"
	.string	""
	.string	""
	.ascii	"\001\002"
	.string	""
	.ascii	"\001\001\002"
	.string	""
	.ascii	"\001\001\003"
	.align 32
	.type	_ZL13convertFEto06, @object
	.size	_ZL13convertFEto06, 282
_ZL13convertFEto06:
	.value	1611
	.value	1611
	.value	1612
	.value	1612
	.value	1613
	.value	1613
	.value	1614
	.value	1614
	.value	1615
	.value	1615
	.value	1616
	.value	1616
	.value	1617
	.value	1617
	.value	1618
	.value	1618
	.value	1569
	.value	1570
	.value	1570
	.value	1571
	.value	1571
	.value	1572
	.value	1572
	.value	1573
	.value	1573
	.value	1574
	.value	1574
	.value	1574
	.value	1574
	.value	1575
	.value	1575
	.value	1576
	.value	1576
	.value	1576
	.value	1576
	.value	1577
	.value	1577
	.value	1578
	.value	1578
	.value	1578
	.value	1578
	.value	1579
	.value	1579
	.value	1579
	.value	1579
	.value	1580
	.value	1580
	.value	1580
	.value	1580
	.value	1581
	.value	1581
	.value	1581
	.value	1581
	.value	1582
	.value	1582
	.value	1582
	.value	1582
	.value	1583
	.value	1583
	.value	1584
	.value	1584
	.value	1585
	.value	1585
	.value	1586
	.value	1586
	.value	1587
	.value	1587
	.value	1587
	.value	1587
	.value	1588
	.value	1588
	.value	1588
	.value	1588
	.value	1589
	.value	1589
	.value	1589
	.value	1589
	.value	1590
	.value	1590
	.value	1590
	.value	1590
	.value	1591
	.value	1591
	.value	1591
	.value	1591
	.value	1592
	.value	1592
	.value	1592
	.value	1592
	.value	1593
	.value	1593
	.value	1593
	.value	1593
	.value	1594
	.value	1594
	.value	1594
	.value	1594
	.value	1601
	.value	1601
	.value	1601
	.value	1601
	.value	1602
	.value	1602
	.value	1602
	.value	1602
	.value	1603
	.value	1603
	.value	1603
	.value	1603
	.value	1604
	.value	1604
	.value	1604
	.value	1604
	.value	1605
	.value	1605
	.value	1605
	.value	1605
	.value	1606
	.value	1606
	.value	1606
	.value	1606
	.value	1607
	.value	1607
	.value	1607
	.value	1607
	.value	1608
	.value	1608
	.value	1609
	.value	1609
	.value	1610
	.value	1610
	.value	1610
	.value	1610
	.value	1628
	.value	1628
	.value	1629
	.value	1629
	.value	1630
	.value	1630
	.value	1631
	.value	1631
	.align 32
	.type	_ZL13convertFBto06, @object
	.size	_ZL13convertFBto06, 352
_ZL13convertFBto06:
	.value	1649
	.value	1649
	.value	1659
	.value	1659
	.value	1659
	.value	1659
	.value	1662
	.value	1662
	.value	1662
	.value	1662
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1658
	.value	1658
	.value	1658
	.value	1658
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1657
	.value	1657
	.value	1657
	.value	1657
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1670
	.value	1670
	.value	1670
	.value	1670
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1677
	.value	1677
	.value	1676
	.value	1676
	.value	1678
	.value	1678
	.value	1672
	.value	1672
	.value	1688
	.value	1688
	.value	1681
	.value	1681
	.value	1705
	.value	1705
	.value	1705
	.value	1705
	.value	1711
	.value	1711
	.value	1711
	.value	1711
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1722
	.value	1722
	.value	1723
	.value	1723
	.value	1723
	.value	1723
	.value	1728
	.value	1728
	.value	1729
	.value	1729
	.value	1729
	.value	1729
	.value	1726
	.value	1726
	.value	1726
	.value	1726
	.value	1746
	.value	1746
	.value	1747
	.value	1747
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1735
	.value	1735
	.value	1734
	.value	1734
	.value	1736
	.value	1736
	.value	0
	.value	1739
	.value	1739
	.value	1733
	.value	1733
	.value	1737
	.value	1737
	.value	1744
	.value	1744
	.value	1744
	.value	1744
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1740
	.value	1740
	.value	1740
	.value	1740
	.align 32
	.type	_ZL9presBLink, @object
	.size	_ZL9presBLink, 144
_ZL9presBLink:
	.string	"\003\003\003"
	.string	"\003"
	.string	"\003\003\003\003\003\003\003\003\003\003"
	.string	""
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001\002\003"
	.string	"\001"
	.string	"\001\002\003"
	.string	"\001"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	"\001"
	.string	"\001"
	.string	"\001\002\003"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	""
	.string	""
	.align 32
	.type	_ZL9presALink, @object
	.size	_ZL9presALink, 275
_ZL9presALink:
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\002\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\002\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	"\001\002\003"
	.string	"\001\002\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001\002\003"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\004\004\004\004\004"
	.align 32
	.type	_ZL7araLink, @object
	.size	_ZL7araLink, 356
_ZL7araLink:
	.value	4385
	.value	4897
	.value	5377
	.value	5921
	.value	6403
	.value	7457
	.value	7939
	.value	8961
	.value	9475
	.value	10499
	.value	11523
	.value	12547
	.value	13571
	.value	14593
	.value	15105
	.value	15617
	.value	16129
	.value	16643
	.value	17667
	.value	18691
	.value	19715
	.value	20739
	.value	21763
	.value	22787
	.value	23811
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	3
	.value	24835
	.value	25859
	.value	26883
	.value	27923
	.value	28931
	.value	29955
	.value	30979
	.value	32001
	.value	32513
	.value	-32509
	.value	260
	.value	388
	.value	388
	.value	388
	.value	388
	.value	388
	.value	836
	.value	260
	.value	1796
	.value	2052
	.value	2052
	.value	260
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	-31487
	.value	-30975
	.value	-30463
	.value	-29951
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1540
	.value	9
	.value	33
	.value	33
	.value	0
	.value	33
	.value	1
	.value	1
	.value	3
	.value	5643
	.value	3595
	.value	523
	.value	3
	.value	3
	.value	1547
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	10763
	.value	3
	.value	14345
	.value	1
	.value	1
	.value	1
	.value	13321
	.value	12809
	.value	13833
	.value	1
	.value	1
	.value	15369
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	14857
	.value	1
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	15883
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	16907
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	3
	.value	19977
	.value	20491
	.value	3
	.value	3
	.value	23051
	.value	3
	.value	21513
	.value	22027
	.value	1
	.value	1
	.value	1
	.value	-28663
	.value	-30455
	.value	-30967
	.value	-29943
	.value	-28151
	.value	1
	.value	-29175
	.value	-21493
	.value	1
	.value	3
	.value	3
	.value	-27637
	.value	3
	.value	24073
	.value	24585
	.align 16
	.type	_ZL14convertLamAlef, @object
	.size	_ZL14convertLamAlef, 16
_ZL14convertLamAlef:
	.value	1570
	.value	1570
	.value	1571
	.value	1571
	.value	1573
	.value	1573
	.value	1575
	.value	1575
	.align 8
	.type	_ZL13IrrelevantPos, @object
	.size	_ZL13IrrelevantPos, 8
_ZL13IrrelevantPos:
	.string	""
	.ascii	"\002\004\006\b\n\f\016"
	.align 2
	.type	_ZL13yehHamzaToYeh, @object
	.size	_ZL13yehHamzaToYeh, 4
_ZL13yehHamzaToYeh:
	.value	-273
	.value	-272
	.align 16
	.type	_ZL14tashkeelMedial, @object
	.size	_ZL14tashkeelMedial, 16
_ZL14tashkeelMedial:
	.string	""
	.string	"\001"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.string	"\001"
	.ascii	"\001"
	.align 8
	.type	_ZL23tailFamilyIsolatedFinal, @object
	.size	_ZL23tailFamilyIsolatedFinal, 14
_ZL23tailFamilyIsolatedFinal:
	.string	"\001\001"
	.string	""
	.string	"\001\001"
	.string	""
	.string	"\001\001"
	.string	""
	.ascii	"\001\001"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.value	400
	.value	400
	.value	400
	.value	400
	.value	400
	.value	400
	.value	400
	.value	400
	.align 16
.LC1:
	.value	15
	.value	15
	.value	15
	.value	15
	.value	15
	.value	15
	.value	15
	.value	15
	.align 16
.LC2:
	.value	1604
	.value	1604
	.value	1604
	.value	1604
	.value	1604
	.value	1604
	.value	1604
	.value	1604
	.align 16
.LC3:
	.value	267
	.value	267
	.value	267
	.value	267
	.value	267
	.value	267
	.value	267
	.value	267
	.align 16
.LC4:
	.value	7
	.value	7
	.value	7
	.value	7
	.value	7
	.value	7
	.value	7
	.value	7
	.align 16
.LC5:
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.value	1
	.align 16
.LC6:
	.value	32
	.value	32
	.value	32
	.value	32
	.value	32
	.value	32
	.value	32
	.value	32
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
