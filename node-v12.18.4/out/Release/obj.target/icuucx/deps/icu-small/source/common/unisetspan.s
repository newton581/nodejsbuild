	.file	"unisetspan.cpp"
	.text
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720UnicodeSetStringSpanC2ERKS0_RKNS_7UVectorE
	.type	_ZN6icu_6720UnicodeSetStringSpanC2ERKS0_RKNS_7UVectorE, @function
_ZN6icu_6720UnicodeSetStringSpanC2ERKS0_RKNS_7UVectorE:
.LFB2350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	8(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r14, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$8, %rsp
	call	_ZN6icu_6710UnicodeSetC1ERKS0_@PLT
	movl	256(%r12), %edx
	movq	248(%r12), %rcx
	movq	$0, 208(%rbx)
	movq	208(%r12), %rdi
	movq	%r13, 216(%rbx)
	movq	%rcx, 248(%rbx)
	movl	248(%r12), %eax
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	$0, 240(%rbx)
	movl	%edx, 256(%rbx)
	movb	$1, 260(%rbx)
	cmpq	%rdi, %r14
	je	.L10
	call	_ZNK6icu_6710UnicodeSet5cloneEv@PLT
	movq	216(%rbx), %r13
	movq	%rax, 208(%rbx)
	movl	248(%rbx), %eax
.L3:
	movl	8(%r13), %r13d
	leal	(%rax,%r13,8), %eax
	movslq	%eax, %r14
	cmpl	$128, %eax
	jg	.L4
	leaq	264(%rbx), %rdi
	movq	%rdi, 224(%rbx)
.L5:
	movslq	%r13d, %rax
	sall	$2, %r13d
	movq	224(%r12), %rsi
	movq	%r14, %rdx
	leaq	(%rdi,%rax,4), %rax
	movslq	%r13d, %r13
	addq	%rax, %r13
	movq	%rax, 232(%rbx)
	movq	%r13, 240(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	%r15, 208(%rbx)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r14, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, 224(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L5
	movq	$0, 252(%rbx)
	popq	%rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2350:
	.size	_ZN6icu_6720UnicodeSetStringSpanC2ERKS0_RKNS_7UVectorE, .-_ZN6icu_6720UnicodeSetStringSpanC2ERKS0_RKNS_7UVectorE
	.globl	_ZN6icu_6720UnicodeSetStringSpanC1ERKS0_RKNS_7UVectorE
	.set	_ZN6icu_6720UnicodeSetStringSpanC1ERKS0_RKNS_7UVectorE,_ZN6icu_6720UnicodeSetStringSpanC2ERKS0_RKNS_7UVectorE
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720UnicodeSetStringSpanD2Ev
	.type	_ZN6icu_6720UnicodeSetStringSpanD2Ev, @function
_ZN6icu_6720UnicodeSetStringSpanD2Ev:
.LFB2353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	208(%rdi), %rdi
	leaq	8(%rbx), %r12
	testq	%rdi, %rdi
	je	.L12
	cmpq	%r12, %rdi
	je	.L12
	call	_ZN6icu_6710UnicodeSetD0Ev@PLT
.L12:
	movq	224(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L13
	addq	$264, %rbx
	cmpq	%rbx, %rdi
	je	.L13
	call	uprv_free_67@PLT
.L13:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSetD1Ev@PLT
	.cfi_endproc
.LFE2353:
	.size	_ZN6icu_6720UnicodeSetStringSpanD2Ev, .-_ZN6icu_6720UnicodeSetStringSpanD2Ev
	.globl	_ZN6icu_6720UnicodeSetStringSpanD1Ev
	.set	_ZN6icu_6720UnicodeSetStringSpanD1Ev,_ZN6icu_6720UnicodeSetStringSpanD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720UnicodeSetStringSpan15addToSpanNotSetEi
	.type	_ZN6icu_6720UnicodeSetStringSpan15addToSpanNotSetEi, @function
_ZN6icu_6720UnicodeSetStringSpan15addToSpanNotSetEi:
.LFB2355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	8(%rbx), %r13
	subq	$8, %rsp
	movq	208(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L22
	cmpq	%r13, %rdi
	je	.L22
.L23:
	addq	$8, %rsp
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_6710UnicodeSet3addEi@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L21
	movq	%r13, %rdi
	call	_ZNK6icu_6710UnicodeSet13cloneAsThawedEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L21
	movq	%rax, 208(%rbx)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2355:
	.size	_ZN6icu_6720UnicodeSetStringSpan15addToSpanNotSetEi, .-_ZN6icu_6720UnicodeSetStringSpan15addToSpanNotSetEi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720UnicodeSetStringSpan7spanNotEPKDsi
	.type	_ZNK6icu_6720UnicodeSetStringSpan7spanNotEPKDsi, @function
_ZNK6icu_6720UnicodeSetStringSpan7spanNotEPKDsi:
.LFB2367:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	216(%rdi), %rax
	movq	%rsi, -64(%rbp)
	movl	%edx, -68(%rbp)
	movl	8(%rax), %eax
	movl	%eax, -72(%rbp)
	leaq	8(%rdi), %rax
	movq	%rax, -80(%rbp)
.L41:
	movq	-64(%rbp), %rbx
	movslq	%r13d, %rax
	movq	208(%r14), %rdi
	xorl	%ecx, %ecx
	movl	%r12d, %edx
	leaq	(%rbx,%rax,2), %rsi
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	cmpl	%eax, %r12d
	je	.L40
	leal	0(%r13,%rax), %ebx
	subl	%eax, %r12d
	movl	%ebx, -52(%rbp)
	movslq	%ebx, %rax
	movq	-64(%rbp), %rbx
	leaq	(%rbx,%rax,2), %r13
	movzwl	0(%r13), %esi
	movl	%esi, %eax
	addw	$10240, %ax
	cmpw	$1023, %ax
	ja	.L37
	cmpl	$1, %r12d
	jle	.L37
	movzwl	2(%r13), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L78
.L37:
	movq	-80(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L79
.L34:
	movl	-52(%rbp), %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	sall	$10, %esi
	movq	-80(%rbp), %rdi
	leal	-56613888(%rax,%rsi), %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L34
	movl	$-2, -84(%rbp)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L43:
	movl	-52(%rbp), %r13d
	movl	-84(%rbp), %eax
	subl	%eax, %r13d
	addl	%eax, %r12d
	jne	.L41
.L40:
	movl	-68(%rbp), %eax
	movl	%eax, -52(%rbp)
	jmp	.L34
.L79:
	movl	$-1, -84(%rbp)
.L38:
	movl	-72(%rbp), %eax
	movl	-68(%rbp), %esi
	xorl	%r15d, %r15d
	subl	-52(%rbp), %esi
	movl	%esi, -56(%rbp)
	leal	-1(%rax), %ebx
	testl	%eax, %eax
	jle	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	movq	232(%r14), %rax
	cmpb	$-1, (%rax,%r15)
	je	.L49
	movq	216(%r14), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movswl	8(%rax), %edx
	testb	$17, %dl
	jne	.L54
	leaq	10(%rax), %rdi
	testb	$2, %dl
	jne	.L45
	movq	24(%rax), %rdi
.L45:
	testw	%dx, %dx
	js	.L47
	sarl	$5, %edx
.L48:
	cmpl	%edx, %r12d
	jl	.L49
	xorl	%eax, %eax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$1, %rax
	movl	%edx, %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L80
.L50:
	movzwl	(%rdi,%rax,2), %esi
	cmpw	%si, 0(%r13,%rax,2)
	je	.L81
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	1(%r15), %rax
	cmpq	%r15, %rbx
	je	.L43
	movq	%rax, %r15
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L47:
	movl	12(%rax), %edx
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L80:
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jle	.L52
	movzwl	-2(%r13), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L82
.L52:
	cmpl	-56(%rbp), %edx
	jge	.L34
	movslq	%edx, %rdx
	movzwl	-2(%r13,%rdx,2), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L34
	movzwl	0(%r13,%rdx,2), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L34
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%edi, %edi
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L82:
	movzwl	0(%r13), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L52
	jmp	.L49
	.cfi_endproc
.LFE2367:
	.size	_ZNK6icu_6720UnicodeSetStringSpan7spanNotEPKDsi, .-_ZNK6icu_6720UnicodeSetStringSpan7spanNotEPKDsi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720UnicodeSetStringSpan4spanEPKDsi17USetSpanCondition
	.type	_ZNK6icu_6720UnicodeSetStringSpan4spanEPKDsi17USetSpanCondition, @function
_ZNK6icu_6720UnicodeSetStringSpan4spanEPKDsi17USetSpanCondition:
.LFB2363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movl	%edx, -144(%rbp)
	movl	%ecx, -148(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	jne	.L84
	call	_ZNK6icu_6720UnicodeSetStringSpan7spanNotEPKDsi
	movl	%eax, -104(%rbp)
.L83:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L207
	movl	-104(%rbp), %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L84:
	.cfi_restore_state
	movq	%rdi, %rax
	movl	$1, %ecx
	movq	%rsi, %r14
	movl	%edx, %ebx
	addq	$8, %rax
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	movl	%eax, -104(%rbp)
	cmpl	%eax, %ebx
	je	.L83
	leaq	-76(%rbp), %rax
	cmpl	$1, -148(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -96(%rbp)
	movl	$0, -80(%rbp)
	je	.L208
.L86:
	movq	-128(%rbp), %rax
	movl	-104(%rbp), %ebx
	movl	-144(%rbp), %r13d
	movq	216(%rax), %rax
	movl	%ebx, -140(%rbp)
	subl	%ebx, %r13d
	movl	8(%rax), %eax
	movl	%r13d, %r15d
	movq	%r14, %r13
	movl	%eax, -172(%rbp)
.L90:
	cmpl	$1, -148(%rbp)
	movl	-172(%rbp), %eax
	je	.L209
	testl	%eax, %eax
	jle	.L92
	subl	$1, %eax
	movl	-140(%rbp), %r14d
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	movq	%rax, -136(%rbp)
	movl	-144(%rbp), %eax
	xorl	%r12d, %r12d
	subl	%r14d, %eax
	movl	%eax, -100(%rbp)
	.p2align 4,,10
	.p2align 3
.L122:
	movq	-128(%rbp), %rdi
	movl	%r8d, %esi
	movq	%r8, -112(%rbp)
	movq	232(%rdi), %rax
	movq	216(%rdi), %rdi
	movzbl	(%rax,%r8), %edx
	movl	%edx, -120(%rbp)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movq	-112(%rbp), %r8
	movl	-120(%rbp), %edx
	movswl	8(%rax), %edi
	testb	$17, %dil
	jne	.L147
	leaq	10(%rax), %r11
	testb	$2, %dil
	jne	.L109
	movq	24(%rax), %r11
.L109:
	testw	%di, %di
	js	.L111
	sarl	$5, %edi
.L112:
	cmpl	$254, %edx
	movl	-104(%rbp), %eax
	movl	%edi, %ecx
	cmovge	%edi, %edx
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	subl	%edx, %ecx
	cmpl	%ebx, %edx
	jl	.L148
	cmpl	%ecx, %r15d
	jl	.L148
	movslq	%edi, %rax
	addq	%rax, %rax
	movq	%rax, -120(%rbp)
	subq	$2, %rax
	movq	%rax, -112(%rbp)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L115:
	addl	$1, %ecx
	subl	$1, %edx
	cmpl	%ecx, %r15d
	jl	.L148
	cmpl	%ebx, %edx
	jl	.L148
.L120:
	cmpl	%ebx, %edx
	jg	.L153
	cmpl	%ecx, %r12d
	jge	.L115
.L153:
	movl	%r14d, %r10d
	subl	%edx, %r10d
	movslq	%r10d, %rax
	leaq	0(%r13,%rax,2), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L117:
	movzwl	(%r11,%rax,2), %esi
	cmpw	%si, (%r9,%rax,2)
	jne	.L115
	addq	$1, %rax
	movl	%edi, %esi
	subl	%eax, %esi
	testl	%esi, %esi
	jg	.L117
	testl	%r10d, %r10d
	jle	.L119
	movzwl	-2(%r9), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L210
.L119:
	movl	-100(%rbp), %eax
	addl	%edx, %eax
	cmpl	%eax, %edi
	jl	.L211
.L114:
	leaq	1(%r8), %rax
	cmpq	%r8, -136(%rbp)
	je	.L212
.L149:
	movq	%rax, %r8
	movl	%edx, %ebx
	movl	%ecx, %r12d
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L211:
	movq	-112(%rbp), %rax
	movzwl	(%r9,%rax), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L114
	movq	-120(%rbp), %rax
	movzwl	(%r9,%rax), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L115
	leaq	1(%r8), %rax
	cmpq	%r8, -136(%rbp)
	jne	.L149
.L212:
	orl	%ecx, %edx
	je	.L92
	subl	%ecx, %r15d
	jne	.L123
.L205:
	movl	-144(%rbp), %eax
	movq	-96(%rbp), %r14
	movl	%eax, -104(%rbp)
.L107:
	cmpq	-192(%rbp), %r14
	je	.L83
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L148:
	movl	%ebx, %edx
	movl	%r12d, %ecx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L210:
	movzwl	(%r9), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L119
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L111:
	movl	12(%rax), %edi
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L147:
	xorl	%r11d, %r11d
	jmp	.L109
.L197:
	movq	-168(%rbp), %r13
.L92:
	movl	-104(%rbp), %esi
	testl	%esi, %esi
	jne	.L155
	movl	-140(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L155
	movslq	-140(%rbp), %rax
	leaq	0(%r13,%rax,2), %rsi
	movl	-84(%rbp), %eax
	testl	%eax, %eax
	jne	.L127
	movq	-184(%rbp), %rdi
	movl	$1, %ecx
	movl	%r15d, %edx
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	movl	%eax, -104(%rbp)
	cmpl	%eax, %r15d
	je	.L156
	testl	%eax, %eax
	je	.L156
	addl	%eax, -140(%rbp)
	subl	%eax, %r15d
	jmp	.L90
.L209:
	testl	%eax, %eax
	jle	.L92
	subl	$1, %eax
	movq	%r13, -168(%rbp)
	xorl	%r12d, %r12d
	movq	-128(%rbp), %r13
	movq	%rax, -112(%rbp)
	movl	-144(%rbp), %eax
	subl	-140(%rbp), %eax
	movl	%eax, -152(%rbp)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	1(%r12), %rax
	cmpq	%r12, -112(%rbp)
	je	.L197
	movq	%rax, %r12
.L94:
	movq	232(%r13), %rax
	movzbl	(%rax,%r12), %ebx
	cmpl	$255, %ebx
	je	.L100
	movq	216(%r13), %rdi
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L145
	leaq	10(%rax), %r9
	testb	$2, %dl
	jne	.L95
	movq	24(%rax), %r9
.L95:
	testw	%dx, %dx
	js	.L97
	movswl	%dx, %edi
	sarl	$5, %edi
	cmpl	$254, %ebx
	je	.L213
.L99:
	movl	-104(%rbp), %esi
	movl	%ebx, %eax
	cmpl	%ebx, %esi
	cmovle	%esi, %eax
	movl	%edi, %esi
	subl	%eax, %esi
	cmpl	%esi, %r15d
	jl	.L100
	movslq	%edi, %rdx
	leal	(%rax,%rsi), %r11d
	movq	%r12, -136(%rbp)
	movq	-168(%rbp), %r12
	leaq	(%rdx,%rdx), %rbx
	movq	%rbx, -160(%rbp)
	subq	$2, %rbx
	movq	%rbx, -120(%rbp)
	movl	-140(%rbp), %ebx
	subl	%eax, %ebx
	addl	-152(%rbp), %eax
	addl	%esi, %eax
	subl	%esi, %ebx
	movl	%eax, -100(%rbp)
	.p2align 4,,10
	.p2align 3
.L108:
	movl	-80(%rbp), %edx
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r14
	addl	%esi, %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	cmpl	%eax, %edx
	cmovge	%ecx, %edx
	movslq	%edx, %rdx
	addq	%r14, %rdx
	cmpb	$0, (%rdx)
	jne	.L102
	leal	(%rbx,%rsi), %r10d
	movslq	%r10d, %rax
	leaq	(%r12,%rax,2), %r8
	xorl	%eax, %eax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L215:
	addq	$1, %rax
	movl	%edi, %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L214
.L103:
	movzwl	(%r9,%rax,2), %ecx
	cmpw	%cx, (%r8,%rax,2)
	je	.L215
.L102:
	cmpl	%r11d, %esi
	je	.L199
	addl	$1, %esi
	cmpl	%esi, %r15d
	jge	.L108
.L199:
	movq	-136(%rbp), %r12
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L214:
	testl	%r10d, %r10d
	jle	.L105
	movzwl	-2(%r8), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L216
.L105:
	movl	-100(%rbp), %eax
	subl	%esi, %eax
	cmpl	%eax, %edi
	jge	.L106
	movq	-120(%rbp), %rax
	movzwl	(%r8,%rax), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L217
.L106:
	cmpl	%esi, %r15d
	je	.L146
.L218:
	movb	$1, (%rdx)
	addl	$1, -84(%rbp)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L216:
	movzwl	(%r8), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L105
	jmp	.L102
.L217:
	movq	-160(%rbp), %rax
	movzwl	(%r8,%rax), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L102
	cmpl	%esi, %r15d
	jne	.L218
.L146:
	movl	-144(%rbp), %eax
	movl	%eax, -104(%rbp)
	jmp	.L107
.L97:
	movl	12(%rax), %edi
	cmpl	$254, %ebx
	jne	.L99
.L213:
	leal	-1(%rdi), %ebx
	movslq	%ebx, %rax
	leaq	(%rax,%rax), %rdx
	movzwl	(%r9,%rax,2), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L99
	testl	%ebx, %ebx
	jle	.L99
	movzwl	-2(%r9,%rdx), %eax
	leal	-2(%rdi), %edx
	andl	$-1024, %eax
	cmpl	$55296, %eax
	cmove	%edx, %ebx
	jmp	.L99
.L155:
	movl	-84(%rbp), %edx
	testl	%edx, %edx
	je	.L219
.L126:
	movl	-80(%rbp), %r8d
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	leal	1(%r8), %eax
	cltq
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	(%rdx,%rax), %rdi
	addq	$1, %rax
	cmpb	$0, -1(%rdx,%rax)
	jne	.L220
.L138:
	movl	%eax, %esi
	cmpl	%eax, %ecx
	jg	.L221
	subl	%r8d, %ecx
	cmpb	$0, (%rdx)
	jne	.L151
	leaq	1(%rdx), %rax
	movl	$1, %edi
	subl	%eax, %edi
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%rax, %rdx
	leal	(%rdi,%rax), %esi
	leaq	1(%rax), %rax
	cmpb	$0, (%rdx)
	je	.L141
	addl	%esi, %ecx
.L140:
	movb	$0, (%rdx)
	subl	$1, -84(%rbp)
	movl	%esi, -80(%rbp)
.L139:
	addl	%ecx, -140(%rbp)
	subl	%ecx, %r15d
	movl	$0, -104(%rbp)
	jmp	.L90
.L145:
	xorl	%r9d, %r9d
	jmp	.L95
.L123:
	addl	%ecx, -140(%rbp)
	movl	$0, -104(%rbp)
	jmp	.L90
.L127:
	movzwl	(%rsi), %r8d
	movl	%r8d, %eax
	addw	$10240, %ax
	cmpw	$1023, %ax
	ja	.L131
	cmpl	$1, %r15d
	jle	.L131
	movzwl	2(%rsi), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	je	.L222
	movq	-184(%rbp), %rdi
	movl	%r8d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L126
	movl	$1, %eax
.L134:
	movl	-88(%rbp), %edx
	addl	%eax, -140(%rbp)
	subl	%eax, %r15d
	addl	-80(%rbp), %eax
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	%edx, %eax
	cmovge	%ecx, %eax
	movslq	%eax, %rdx
	addq	-96(%rbp), %rdx
	cmpb	$0, (%rdx)
	je	.L136
	movb	$0, (%rdx)
	subl	$1, -84(%rbp)
.L136:
	movl	%eax, -80(%rbp)
	movl	$0, -104(%rbp)
	jmp	.L90
.L131:
	movq	-184(%rbp), %rdi
	movl	%r8d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L126
	movl	$1, %eax
.L133:
	cmpl	%eax, %r15d
	je	.L205
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L220:
	movb	$0, (%rdi)
	movl	%esi, %ecx
	subl	-80(%rbp), %ecx
	subl	$1, -84(%rbp)
	movl	%esi, -80(%rbp)
	jmp	.L139
.L208:
	movq	-128(%rbp), %rax
	movl	252(%rax), %ebx
	cmpl	$16, %ebx
	jg	.L87
	movl	$16, -88(%rbp)
	movq	-192(%rbp), %rdi
	movl	$16, %r12d
.L88:
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L86
.L156:
	movl	-140(%rbp), %ebx
	movq	-96(%rbp), %r14
	addl	%ebx, -104(%rbp)
	jmp	.L107
.L87:
	movslq	%ebx, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L223
	movq	%rax, -96(%rbp)
	movl	%ebx, -88(%rbp)
	jmp	.L88
.L151:
	xorl	%esi, %esi
	jmp	.L140
.L222:
	sall	$10, %r8d
	movq	-184(%rbp), %rdi
	leal	-56613888(%rax,%r8), %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	%eax, %r8d
	movl	$2, %eax
	testb	%r8b, %r8b
	jne	.L133
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L219:
	movl	-140(%rbp), %eax
	movq	-96(%rbp), %r14
	movl	%eax, -104(%rbp)
	jmp	.L107
.L223:
	movslq	-88(%rbp), %r12
	movq	-96(%rbp), %rdi
	jmp	.L88
.L207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2363:
	.size	_ZNK6icu_6720UnicodeSetStringSpan4spanEPKDsi17USetSpanCondition, .-_ZNK6icu_6720UnicodeSetStringSpan4spanEPKDsi17USetSpanCondition
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720UnicodeSetStringSpan15spanNotBackUTF8EPKhi
	.type	_ZNK6icu_6720UnicodeSetStringSpan15spanNotBackUTF8EPKhi, @function
_ZNK6icu_6720UnicodeSetStringSpan15spanNotBackUTF8EPKhi:
.LFB2370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	232(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	216(%rdi), %rax
	cmpb	$0, 260(%rdi)
	movl	8(%rax), %eax
	movl	%eax, -72(%rbp)
	je	.L225
	leal	(%rax,%rax,2), %eax
	cltq
	addq	%rax, %rbx
.L225:
	leaq	8(%r15), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L238:
	movq	208(%r15), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	call	_ZNK6icu_6710UnicodeSet12spanBackUTF8EPKci17USetSpanCondition@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L237
	movslq	%eax, %r14
	movzbl	-1(%r13,%r14), %esi
	testb	%sil, %sil
	js	.L228
	movq	-80(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L259
.L224:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movl	$-3, %r8d
	movl	%esi, %ecx
	leal	-1(%r12), %eax
	xorl	%esi, %esi
	leaq	-60(%rbp), %rdx
	movq	%r13, %rdi
	movl	%eax, -60(%rbp)
	call	utf8_prevCharSafeBody_67@PLT
	movl	-60(%rbp), %edx
	movq	-80(%rbp), %rdi
	movl	%eax, %esi
	movl	%edx, -68(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	-68(%rbp), %edx
	movl	%eax, %r8d
	movl	%r12d, %eax
	subl	%edx, %eax
	subl	%r12d, %edx
	testb	%r8b, %r8b
	cmove	%edx, %eax
	movl	%eax, -68(%rbp)
	testl	%eax, %eax
	jg	.L224
.L230:
	movl	-72(%rbp), %eax
	movq	240(%r15), %rsi
	testl	%eax, %eax
	jle	.L233
	leal	-1(%rax), %r11d
	movq	224(%r15), %r10
	xorl	%eax, %eax
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L234:
	addq	%rdi, %rsi
	leaq	1(%rax), %rdx
	cmpq	%rax, %r11
	je	.L233
.L240:
	movq	%rdx, %rax
.L236:
	movslq	(%r10,%rax,4), %rdi
	movq	%rdi, %rcx
	testl	%edi, %edi
	je	.L234
	cmpb	$-1, (%rbx,%rax)
	je	.L234
	cmpl	%edi, %r12d
	jl	.L234
	movq	%r14, %r9
	xorl	%edx, %edx
	subq	%rdi, %r9
	addq	%r13, %r9
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L261:
	addq	$1, %rdx
	movl	%ecx, %r8d
	subl	%edx, %r8d
	testl	%r8d, %r8d
	jle	.L224
.L235:
	movzbl	(%rsi,%rdx), %r8d
	cmpb	%r8b, (%r9,%rdx)
	je	.L261
	addq	%rdi, %rsi
	leaq	1(%rax), %rdx
	cmpq	%rax, %r11
	jne	.L240
	.p2align 4,,10
	.p2align 3
.L233:
	movl	-68(%rbp), %edx
	addl	%r12d, %edx
	jne	.L238
.L237:
	xorl	%r12d, %r12d
	jmp	.L224
.L259:
	movl	$-1, -68(%rbp)
	jmp	.L230
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2370:
	.size	_ZNK6icu_6720UnicodeSetStringSpan15spanNotBackUTF8EPKhi, .-_ZNK6icu_6720UnicodeSetStringSpan15spanNotBackUTF8EPKhi
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	" 000000000000\02000"
	.section	.rodata
.LC1:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\036\017\017\017"
	.string	""
	.string	""
	.string	""
	.string	""
	.text
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720UnicodeSetStringSpan12spanBackUTF8EPKhi17USetSpanCondition
	.type	_ZNK6icu_6720UnicodeSetStringSpan12spanBackUTF8EPKhi17USetSpanCondition, @function
_ZNK6icu_6720UnicodeSetStringSpan12spanBackUTF8EPKhi17USetSpanCondition:
.LFB2366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -128(%rbp)
	movl	%edx, -144(%rbp)
	movl	%ecx, -140(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	jne	.L263
	call	_ZNK6icu_6720UnicodeSetStringSpan15spanNotBackUTF8EPKhi
	movl	%eax, %r12d
.L262:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L380
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L263:
	.cfi_restore_state
	leaq	8(%rdi), %rax
	movl	$1, %ecx
	movl	%edx, %ebx
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	_ZNK6icu_6710UnicodeSet12spanBackUTF8EPKci17USetSpanCondition@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L262
	subl	%eax, %ebx
	leaq	-76(%rbp), %rax
	cmpl	$1, -140(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -184(%rbp)
	movl	%ebx, %r13d
	movq	%rax, -96(%rbp)
	movl	$0, -80(%rbp)
	je	.L381
.L265:
	movq	216(%r14), %rax
	movq	232(%r14), %rbx
	cmpb	$0, 260(%r14)
	movl	8(%rax), %eax
	movq	%rbx, -120(%rbp)
	movl	%eax, -148(%rbp)
	jne	.L382
.L269:
	movq	%r14, %rax
	movq	240(%r14), %r15
	movl	%r13d, %r14d
	movq	%rax, %r13
.L270:
	cmpl	$1, -140(%rbp)
	movl	-148(%rbp), %eax
	je	.L383
.L271:
	testl	%eax, %eax
	jle	.L272
	movq	224(%r13), %rdx
	subl	$1, %eax
	movslq	%r12d, %rsi
	movq	%r15, %r10
	movq	%r15, -160(%rbp)
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	movq	%r13, -168(%rbp)
	movq	%rax, %r15
	movq	%rdx, %r13
	movq	%rsi, -136(%rbp)
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	1(%r11), %rax
	cmpq	%r11, %r15
	je	.L384
.L331:
	movq	%rax, %r11
.L295:
	movslq	0(%r13,%r11,4), %r8
	testl	%r8d, %r8d
	je	.L287
	movq	-120(%rbp), %rax
	movl	%r8d, %esi
	movzbl	(%rax,%r11), %ecx
	cmpl	$254, %ecx
	cmovge	%r8d, %ecx
	cmpl	%ecx, %r14d
	cmovle	%r14d, %ecx
	subl	%ecx, %esi
	cmpl	%r9d, %ecx
	jl	.L330
	cmpl	%esi, %r12d
	jl	.L330
	movq	-136(%rbp), %rdx
	movslq	%esi, %rax
	subq	%rax, %rdx
	addq	-128(%rbp), %rdx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L385:
	cmpl	%ebx, %esi
	jg	.L334
.L290:
	addl	$1, %esi
	subl	$1, %ecx
	subq	$1, %rdx
	cmpl	%esi, %r12d
	jl	.L330
	cmpl	%r9d, %ecx
	jl	.L330
.L293:
	cmpb	$-64, (%rdx)
	jl	.L290
	cmpl	%ecx, %r9d
	jge	.L385
.L334:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L292:
	movzbl	(%r10,%rax), %edi
	cmpb	%dil, (%rdx,%rax)
	jne	.L290
	addq	$1, %rax
	movl	%r8d, %edi
	subl	%eax, %edi
	testl	%edi, %edi
	jg	.L292
	addq	%r8, %r10
	movl	%ecx, %r9d
	movl	%esi, %ebx
	.p2align 4,,10
	.p2align 3
.L386:
	leaq	1(%r11), %rax
	cmpq	%r11, %r15
	jne	.L331
.L384:
	orl	%ebx, %r9d
	movq	-160(%rbp), %r15
	movq	-168(%rbp), %r13
	je	.L272
	subl	%ebx, %r12d
	jne	.L379
.L296:
	movq	-96(%rbp), %r10
.L301:
	cmpq	-184(%rbp), %r10
	je	.L262
	movq	%r10, %rdi
	call	uprv_free_67@PLT
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L330:
	movl	%r9d, %ecx
	movl	%ebx, %esi
	addq	%r8, %r10
	movl	%ecx, %r9d
	movl	%esi, %ebx
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L272:
	movl	-84(%rbp), %eax
	testl	%r14d, %r14d
	jne	.L336
	cmpl	-144(%rbp), %r12d
	je	.L336
	testl	%eax, %eax
	je	.L387
	movq	-128(%rbp), %rbx
	movslq	%r12d, %rax
	movzbl	-1(%rbx,%rax), %esi
	testb	%sil, %sil
	js	.L304
	movq	-176(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L307
	movl	$1, %eax
.L306:
	cmpl	%eax, %r12d
	je	.L388
	movl	-88(%rbp), %edx
	subl	%eax, %r12d
	addl	-80(%rbp), %eax
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	%edx, %eax
	cmovge	%ecx, %eax
	movslq	%eax, %rdx
	addq	-96(%rbp), %rdx
	cmpb	$0, (%rdx)
	je	.L312
	movb	$0, (%rdx)
	subl	$1, -84(%rbp)
.L312:
	movl	%eax, -80(%rbp)
	movq	240(%r13), %r15
.L379:
	xorl	%r14d, %r14d
	cmpl	$1, -140(%rbp)
	movl	-148(%rbp), %eax
	jne	.L271
.L383:
	testl	%eax, %eax
	jle	.L272
	leal	-1(%rax), %ebx
	movslq	%r12d, %rax
	xorl	%r11d, %r11d
	movq	%rax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L286:
	movq	224(%r13), %rax
	movslq	(%rax,%r11,4), %r8
	testl	%r8d, %r8d
	je	.L273
	movq	-120(%rbp), %rax
	movzbl	(%rax,%r11), %r9d
	cmpl	$255, %r9d
	je	.L284
	cmpl	$254, %r9d
	je	.L389
.L275:
	cmpl	%r9d, %r14d
	movl	%r8d, %ecx
	cmovle	%r14d, %r9d
	subl	%r9d, %ecx
	cmpl	%ecx, %r12d
	jl	.L284
	movq	-136(%rbp), %rsi
	movslq	%ecx, %rax
	addl	%ecx, %r9d
	subq	%rax, %rsi
	addq	-128(%rbp), %rsi
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L280:
	cmpl	%r9d, %ecx
	je	.L284
.L391:
	addl	$1, %ecx
	subq	$1, %rsi
	cmpl	%ecx, %r12d
	jl	.L284
.L285:
	cmpb	$-64, (%rsi)
	jl	.L280
	movl	-80(%rbp), %eax
	movl	-88(%rbp), %edx
	movq	-96(%rbp), %r10
	addl	%ecx, %eax
	movl	%eax, %edi
	subl	%edx, %edi
	cmpl	%eax, %edx
	cmovle	%edi, %eax
	cltq
	leaq	(%r10,%rax), %rdx
	cmpb	$0, (%rdx)
	jne	.L280
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L282:
	movzbl	(%r15,%rax), %edi
	cmpb	%dil, (%rsi,%rax)
	jne	.L280
	addq	$1, %rax
	movl	%r8d, %edi
	subl	%eax, %edi
	testl	%edi, %edi
	jg	.L282
	cmpl	%ecx, %r12d
	je	.L390
	movb	$1, (%rdx)
	addl	$1, -84(%rbp)
	cmpl	%r9d, %ecx
	jne	.L391
	.p2align 4,,10
	.p2align 3
.L284:
	addq	%r8, %r15
.L273:
	leaq	1(%r11), %rax
	cmpq	%r11, %rbx
	je	.L272
	movq	%rax, %r11
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L389:
	movzbl	(%r15), %eax
	leal	62(%rax), %edx
	cmpb	$50, %dl
	ja	.L320
	cmpl	$1, %r8d
	je	.L320
	leal	32(%rax), %ecx
	movzbl	1(%r15), %edx
	cmpb	$15, %cl
	ja	.L277
	andl	$15, %eax
	leaq	.LC0(%rip), %rsi
	movsbl	(%rsi,%rax), %ecx
	movl	%edx, %eax
	movl	$1, %edx
	shrb	$5, %al
	btl	%eax, %ecx
	jnc	.L276
	movl	$2, %edx
	cmpl	$2, %r8d
	je	.L276
	xorl	%edx, %edx
	cmpb	$-64, 2(%r15)
	setl	%dl
	addl	$2, %edx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L320:
	movl	$1, %edx
.L276:
	movl	%r8d, %r9d
	subl	%edx, %r9d
	jmp	.L275
.L336:
	testl	%eax, %eax
	je	.L296
.L307:
	movl	-80(%rbp), %r8d
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	leal	1(%r8), %eax
	cltq
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	(%rdx,%rax), %rdi
	addq	$1, %rax
	cmpb	$0, -1(%rdx,%rax)
	jne	.L392
.L314:
	movl	%eax, %esi
	cmpl	%eax, %ecx
	jg	.L393
	subl	%r8d, %ecx
	cmpb	$0, (%rdx)
	jne	.L332
	movl	%edx, %edi
	leaq	1(%rdx), %rax
	negl	%edi
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%rax, %rdx
	leal	(%rdi,%rax), %esi
	leaq	1(%rax), %rax
	cmpb	$0, (%rdx)
	je	.L317
	addl	%esi, %ecx
.L316:
	movb	$0, (%rdx)
	subl	$1, -84(%rbp)
	movl	%esi, -80(%rbp)
.L315:
	movq	240(%r13), %r15
	subl	%ecx, %r12d
	xorl	%r14d, %r14d
	jmp	.L270
.L382:
	leal	(%rax,%rax,2), %eax
	cltq
	addq	%rax, %rbx
	movq	%rbx, -120(%rbp)
	jmp	.L269
.L387:
	movq	-128(%rbp), %rsi
	movl	$1, %ecx
	movl	%r12d, %edx
	movl	%r12d, %r14d
	movq	-176(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet12spanBackUTF8EPKci17USetSpanCondition@PLT
	subl	%eax, %r14d
	testl	%eax, %eax
	je	.L303
	testl	%r14d, %r14d
	je	.L303
	movq	240(%r13), %r15
	movl	%eax, %r12d
	jmp	.L270
.L277:
	cmpb	$-33, %al
	ja	.L278
	cmpb	$-64, %dl
	setl	%dl
	movzbl	%dl, %edx
	addl	$1, %edx
	jmp	.L276
.L392:
	movb	$0, (%rdi)
	movl	%esi, %ecx
	subl	-80(%rbp), %ecx
	subl	$1, -84(%rbp)
	movl	%esi, -80(%rbp)
	jmp	.L315
.L303:
	movq	-96(%rbp), %r10
	movl	%eax, %r12d
	jmp	.L301
.L381:
	movl	256(%r14), %ebx
	cmpl	$16, %ebx
	jg	.L266
	movl	$16, -88(%rbp)
	movq	%rax, %rdi
	movl	$16, %r15d
.L267:
	movq	%r15, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L265
.L390:
	xorl	%r12d, %r12d
	jmp	.L301
.L304:
	movq	%rbx, %rdi
	leaq	-100(%rbp), %rdx
	movl	$-3, %r8d
	movl	%esi, %ecx
	leal	-1(%r12), %eax
	xorl	%esi, %esi
	movl	%eax, -100(%rbp)
	call	utf8_prevCharSafeBody_67@PLT
	movq	-176(%rbp), %rdi
	movl	-100(%rbp), %ebx
	movl	%eax, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	%r12d, %edx
	movl	%eax, %r8d
	movl	%ebx, %eax
	subl	%ebx, %edx
	subl	%r12d, %eax
	testb	%r8b, %r8b
	cmovne	%edx, %eax
	testl	%eax, %eax
	jle	.L307
	jmp	.L306
.L278:
	shrq	$4, %rdx
	leaq	.LC1(%rip), %rsi
	andl	$7, %eax
	andl	$15, %edx
	movsbl	(%rsi,%rdx), %ecx
	movl	$1, %edx
	btl	%eax, %ecx
	jnc	.L276
	movl	$2, %edx
	cmpl	$2, %r8d
	je	.L276
	cmpb	$-64, 2(%r15)
	jge	.L276
	movl	$3, %edx
	cmpl	$3, %r8d
	je	.L276
	xorl	%edx, %edx
	cmpb	$-64, 3(%r15)
	setl	%dl
	addl	$3, %edx
	jmp	.L276
.L266:
	movslq	%ebx, %r15
	movq	%r15, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L394
	movq	%rax, -96(%rbp)
	movl	%ebx, -88(%rbp)
	jmp	.L267
.L388:
	movq	-96(%rbp), %r10
	xorl	%r12d, %r12d
	jmp	.L301
.L332:
	xorl	%esi, %esi
	jmp	.L316
.L394:
	movslq	-88(%rbp), %r15
	movq	-96(%rbp), %rdi
	jmp	.L267
.L380:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2366:
	.size	_ZNK6icu_6720UnicodeSetStringSpan12spanBackUTF8EPKhi17USetSpanCondition, .-_ZNK6icu_6720UnicodeSetStringSpan12spanBackUTF8EPKhi17USetSpanCondition
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720UnicodeSetStringSpan11spanNotBackEPKDsi
	.type	_ZNK6icu_6720UnicodeSetStringSpan11spanNotBackEPKDsi, @function
_ZNK6icu_6720UnicodeSetStringSpan11spanNotBackEPKDsi:
.LFB2368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	216(%rdi), %rax
	movl	%edx, -52(%rbp)
	movl	8(%rax), %eax
	movl	%eax, -56(%rbp)
	leaq	8(%rdi), %rax
	movq	%rax, -64(%rbp)
.L402:
	movq	208(%r14), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	call	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L401
	movslq	%eax, %rdx
	addq	%rdx, %rdx
	movzwl	-2(%r13,%rdx), %esi
	movl	%esi, %eax
	addw	$9216, %ax
	cmpw	$1023, %ax
	ja	.L398
	cmpl	$1, %r12d
	jle	.L398
	movzwl	-4(%r13,%rdx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L447
.L398:
	movq	-64(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L448
.L395:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L447:
	.cfi_restore_state
	sall	$10, %eax
	movq	-64(%rbp), %rdi
	leal	-56613888(%rsi,%rax), %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L395
	movl	$-2, -68(%rbp)
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L404:
	movl	-68(%rbp), %edx
	addl	%r12d, %edx
	jne	.L402
.L401:
	xorl	%r12d, %r12d
	jmp	.L395
.L448:
	movl	$-1, -68(%rbp)
.L399:
	movl	-56(%rbp), %eax
	xorl	%r15d, %r15d
	leal	-1(%rax), %ebx
	testl	%eax, %eax
	jle	.L404
	.p2align 4,,10
	.p2align 3
.L405:
	movq	232(%r14), %rax
	cmpb	$-1, (%rax,%r15)
	je	.L410
	movq	216(%r14), %rdi
	movl	%r15d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L415
	leaq	10(%rax), %rsi
	testb	$2, %dl
	jne	.L406
	movq	24(%rax), %rsi
.L406:
	testw	%dx, %dx
	js	.L408
	movswl	%dx, %eax
	sarl	$5, %eax
.L409:
	cmpl	%eax, %r12d
	jl	.L410
	movl	%r12d, %r8d
	subl	%eax, %r8d
	movslq	%r8d, %rdx
	leaq	0(%r13,%rdx,2), %rdi
	xorl	%edx, %edx
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L450:
	addq	$1, %rdx
	movl	%eax, %ecx
	subl	%edx, %ecx
	testl	%ecx, %ecx
	jle	.L449
.L411:
	movzwl	(%rsi,%rdx,2), %ecx
	cmpw	%cx, (%rdi,%rdx,2)
	je	.L450
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	1(%r15), %rax
	cmpq	%r15, %rbx
	je	.L404
	movq	%rax, %r15
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L408:
	movl	12(%rax), %eax
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L449:
	testl	%r8d, %r8d
	je	.L413
	movzwl	-2(%rdi), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L451
.L413:
	movl	-52(%rbp), %edx
	subl	%r8d, %edx
	cmpl	%edx, %eax
	jge	.L395
	cltq
	movzwl	-2(%rdi,%rax,2), %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L395
	movzwl	(%rdi,%rax,2), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L395
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L415:
	xorl	%esi, %esi
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L451:
	movzwl	(%rdi), %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L413
	jmp	.L410
	.cfi_endproc
.LFE2368:
	.size	_ZNK6icu_6720UnicodeSetStringSpan11spanNotBackEPKDsi, .-_ZNK6icu_6720UnicodeSetStringSpan11spanNotBackEPKDsi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720UnicodeSetStringSpan8spanBackEPKDsi17USetSpanCondition
	.type	_ZNK6icu_6720UnicodeSetStringSpan8spanBackEPKDsi17USetSpanCondition, @function
_ZNK6icu_6720UnicodeSetStringSpan8spanBackEPKDsi17USetSpanCondition:
.LFB2364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -128(%rbp)
	movq	%rsi, -152(%rbp)
	movl	%edx, -140(%rbp)
	movl	%ecx, -144(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	jne	.L453
	call	_ZNK6icu_6720UnicodeSetStringSpan11spanNotBackEPKDsi
	movl	%eax, %r13d
.L452:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L590
	addq	$136, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L453:
	.cfi_restore_state
	movq	%rdi, %rax
	movl	$1, %ecx
	movl	%edx, %ebx
	addq	$8, %rax
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L452
	subl	%eax, %ebx
	leaq	-76(%rbp), %rax
	cmpl	$1, -144(%rbp)
	movq	$0, -88(%rbp)
	movl	%ebx, -104(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -96(%rbp)
	movl	$0, -80(%rbp)
	je	.L591
.L455:
	movq	-128(%rbp), %rbx
	movq	216(%rbx), %rax
	movq	232(%rbx), %rdi
	cmpb	$0, 260(%rbx)
	movslq	8(%rax), %rax
	movq	%rdi, -120(%rbp)
	movl	%eax, -156(%rbp)
	jne	.L592
.L460:
	cmpl	$1, -144(%rbp)
	movl	-156(%rbp), %eax
	je	.L593
	testl	%eax, %eax
	jle	.L462
	subl	$1, %eax
	movq	-152(%rbp), %r15
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	movq	%rax, -136(%rbp)
	movl	-140(%rbp), %eax
	xorl	%r12d, %r12d
	subl	%r13d, %eax
	movl	%eax, -100(%rbp)
	.p2align 4,,10
	.p2align 3
.L493:
	movq	-120(%rbp), %rax
	movl	%r14d, %esi
	movzbl	(%rax,%r14), %edx
	movq	-128(%rbp), %rax
	movq	216(%rax), %rdi
	movl	%edx, -112(%rbp)
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movl	-112(%rbp), %edx
	movswl	8(%rax), %edi
	testb	$17, %dil
	jne	.L520
	leaq	10(%rax), %r10
	testb	$2, %dil
	jne	.L480
	movq	24(%rax), %r10
.L480:
	testw	%di, %di
	js	.L482
	sarl	$5, %edi
.L483:
	cmpl	$254, %edx
	movl	-104(%rbp), %eax
	movl	%edi, %ecx
	cmovge	%edi, %edx
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	subl	%edx, %ecx
	cmpl	%ecx, %r13d
	jl	.L521
	cmpl	%ebx, %edx
	jl	.L521
	movslq	%edi, %rax
	addq	%rax, %rax
	movq	%rax, -112(%rbp)
	leaq	-2(%rax), %r11
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L486:
	addl	$1, %ecx
	subl	$1, %edx
	cmpl	%ecx, %r13d
	jl	.L521
	cmpl	%ebx, %edx
	jl	.L521
.L491:
	cmpl	%edx, %ebx
	jl	.L529
	cmpl	%r12d, %ecx
	jle	.L486
.L529:
	movl	%r13d, %r9d
	subl	%ecx, %r9d
	movslq	%r9d, %rax
	leaq	(%r15,%rax,2), %r8
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L488:
	movzwl	(%r10,%rax,2), %esi
	cmpw	%si, (%r8,%rax,2)
	jne	.L486
	addq	$1, %rax
	movl	%edi, %esi
	subl	%eax, %esi
	testl	%esi, %esi
	jg	.L488
	testl	%r9d, %r9d
	je	.L490
	movzwl	-2(%r8), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L594
.L490:
	movl	-100(%rbp), %eax
	addl	%ecx, %eax
	cmpl	%eax, %edi
	jl	.L595
.L485:
	leaq	1(%r14), %rax
	cmpq	%r14, -136(%rbp)
	je	.L596
.L522:
	movq	%rax, %r14
	movl	%edx, %ebx
	movl	%ecx, %r12d
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L595:
	movzwl	(%r8,%r11), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L485
	movq	-112(%rbp), %rax
	movzwl	(%r8,%rax), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	je	.L486
	leaq	1(%r14), %rax
	cmpq	%r14, -136(%rbp)
	jne	.L522
.L596:
	orl	%ecx, %edx
	je	.L462
	subl	%ecx, %r13d
	jne	.L587
.L588:
	movq	-96(%rbp), %r14
.L478:
	cmpq	-176(%rbp), %r14
	je	.L452
	movq	%r14, %rdi
	call	uprv_free_67@PLT
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L521:
	movl	%ebx, %edx
	movl	%r12d, %ecx
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L594:
	movzwl	(%r8), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L490
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L482:
	movl	12(%rax), %edi
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L520:
	xorl	%r10d, %r10d
	jmp	.L480
.L462:
	movl	-104(%rbp), %edx
	movl	-84(%rbp), %eax
	testl	%edx, %edx
	jne	.L531
	cmpl	-140(%rbp), %r13d
	je	.L531
	testl	%eax, %eax
	jne	.L498
	movq	-152(%rbp), %rsi
	movq	-168(%rbp), %rdi
	movl	%r13d, %edx
	movl	$1, %ecx
	call	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition@PLT
	subl	%eax, %r13d
	movl	%r13d, -104(%rbp)
	testl	%eax, %eax
	je	.L532
	testl	%r13d, %r13d
	je	.L532
	movl	%eax, %r13d
	jmp	.L460
.L593:
	testl	%eax, %eax
	jle	.L462
	movl	-140(%rbp), %r12d
	subl	$1, %eax
	movq	-152(%rbp), %rbx
	xorl	%r15d, %r15d
	movq	%rax, -112(%rbp)
	subl	%r13d, %r12d
	movl	%r12d, -100(%rbp)
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L471:
	leaq	1(%r15), %rax
	cmpq	%r15, -112(%rbp)
	je	.L462
	movq	%rax, %r15
.L464:
	movq	-120(%rbp), %rax
	movzbl	(%rax,%r15), %r12d
	cmpl	$255, %r12d
	je	.L471
	movq	-128(%rbp), %rax
	movl	%r15d, %esi
	movq	216(%rax), %rdi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movzwl	8(%rax), %edx
	testb	$17, %dl
	jne	.L516
	leaq	10(%rax), %r9
	testb	$2, %dl
	jne	.L465
	movq	24(%rax), %r9
.L465:
	testw	%dx, %dx
	js	.L467
	movswl	%dx, %edi
	sarl	$5, %edi
	cmpl	$254, %r12d
	je	.L597
.L469:
	movl	-104(%rbp), %eax
	movl	%r12d, %r10d
	movl	%edi, %esi
	cmpl	%r12d, %eax
	cmovle	%eax, %r10d
	subl	%r10d, %esi
	cmpl	%esi, %r13d
	jl	.L471
	movslq	%edi, %rax
	addl	%esi, %r10d
	addq	%rax, %rax
	movq	%rax, -136(%rbp)
	leaq	-2(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L479:
	movl	-80(%rbp), %edx
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r14
	addl	%esi, %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	cmpl	%eax, %edx
	cmovge	%ecx, %edx
	movslq	%edx, %rdx
	addq	%r14, %rdx
	cmpb	$0, (%rdx)
	jne	.L473
	movl	%r13d, %r11d
	subl	%esi, %r11d
	movslq	%r11d, %rax
	leaq	(%rbx,%rax,2), %r8
	xorl	%eax, %eax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L599:
	addq	$1, %rax
	movl	%edi, %ecx
	subl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L598
.L474:
	movzwl	(%r9,%rax,2), %ecx
	cmpw	%cx, (%r8,%rax,2)
	je	.L599
.L473:
	cmpl	%r10d, %esi
	je	.L471
	addl	$1, %esi
	cmpl	%esi, %r13d
	jge	.L479
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L598:
	testl	%r11d, %r11d
	je	.L600
	movzwl	-2(%r8), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	je	.L601
.L476:
	movl	-100(%rbp), %eax
	addl	%esi, %eax
	cmpl	%eax, %edi
	jl	.L514
.L477:
	cmpl	%esi, %r13d
	je	.L528
	movb	$1, (%rdx)
	addl	$1, -84(%rbp)
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L600:
	movl	-100(%rbp), %eax
	addl	%esi, %eax
	cmpl	%eax, %edi
	jge	.L528
.L514:
	movzwl	(%r8,%r12), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L477
	movq	-136(%rbp), %rax
	movzwl	(%r8,%rax), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L477
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L601:
	movzwl	(%r8), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L476
	jmp	.L473
.L467:
	movl	12(%rax), %edi
	cmpl	$254, %r12d
	jne	.L469
.L597:
	movzwl	(%r9), %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L517
	cmpl	$1, %edi
	jne	.L602
.L517:
	movl	$1, %eax
.L470:
	movl	%edi, %r12d
	subl	%eax, %r12d
	jmp	.L469
.L531:
	testl	%eax, %eax
	je	.L588
.L497:
	movl	-80(%rbp), %r8d
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	leal	1(%r8), %eax
	cltq
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L604:
	leaq	(%rdx,%rax), %rdi
	addq	$1, %rax
	cmpb	$0, -1(%rdx,%rax)
	jne	.L603
.L507:
	movl	%eax, %esi
	cmpl	%eax, %ecx
	jg	.L604
	subl	%r8d, %ecx
	cmpb	$0, (%rdx)
	jne	.L526
	leaq	1(%rdx), %rax
	movl	$1, %edi
	subl	%eax, %edi
	.p2align 4,,10
	.p2align 3
.L510:
	movq	%rax, %rdx
	leal	(%rdi,%rax), %esi
	leaq	1(%rax), %rax
	cmpb	$0, (%rdx)
	je	.L510
	addl	%esi, %ecx
.L509:
	movb	$0, (%rdx)
	subl	$1, -84(%rbp)
	movl	%esi, -80(%rbp)
.L508:
	movl	$0, -104(%rbp)
	subl	%ecx, %r13d
	jmp	.L460
.L592:
	addq	%rax, %rdi
	movq	%rdi, -120(%rbp)
	jmp	.L460
.L498:
	movq	-152(%rbp), %rbx
	movslq	%r13d, %rdx
	addq	%rdx, %rdx
	movzwl	-2(%rbx,%rdx), %esi
	movl	%esi, %eax
	addw	$9216, %ax
	cmpw	$1023, %ax
	ja	.L500
	cmpl	$1, %r13d
	jle	.L500
	movzwl	-4(%rbx,%rdx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	je	.L605
	movq	-168(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L497
	movl	$1, %eax
.L503:
	movl	-88(%rbp), %edx
	subl	%eax, %r13d
	addl	-80(%rbp), %eax
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	%edx, %eax
	cmovge	%ecx, %eax
	movslq	%eax, %rdx
	addq	-96(%rbp), %rdx
	cmpb	$0, (%rdx)
	je	.L505
	movb	$0, (%rdx)
	subl	$1, -84(%rbp)
.L505:
	movl	%eax, -80(%rbp)
.L587:
	movl	$0, -104(%rbp)
	jmp	.L460
.L528:
	xorl	%r13d, %r13d
	jmp	.L478
.L602:
	movzwl	2(%r9), %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	sete	%al
	movzbl	%al, %eax
	addl	$1, %eax
	jmp	.L470
.L500:
	movq	-168(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L497
	movl	$1, %eax
.L502:
	cmpl	%eax, %r13d
	jne	.L503
	movq	-96(%rbp), %r14
	xorl	%r13d, %r13d
	jmp	.L478
.L516:
	xorl	%r9d, %r9d
	jmp	.L465
.L603:
	movb	$0, (%rdi)
	movl	%esi, %ecx
	subl	-80(%rbp), %ecx
	subl	$1, -84(%rbp)
	movl	%esi, -80(%rbp)
	jmp	.L508
.L591:
	movq	-128(%rbp), %rax
	movl	252(%rax), %ebx
	cmpl	$16, %ebx
	jg	.L456
	movl	$16, -88(%rbp)
	movq	-176(%rbp), %rdi
	movl	$16, %r12d
.L457:
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L455
.L532:
	movq	-96(%rbp), %r14
	movl	%eax, %r13d
	jmp	.L478
.L456:
	movslq	%ebx, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L606
	movq	%rax, -96(%rbp)
	movl	%ebx, -88(%rbp)
	jmp	.L457
.L526:
	xorl	%esi, %esi
	jmp	.L509
.L605:
	sall	$10, %eax
	movq	-168(%rbp), %rdi
	leal	-56613888(%rsi,%rax), %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	%eax, %r8d
	movl	$2, %eax
	testb	%r8b, %r8b
	jne	.L502
	jmp	.L497
.L590:
	call	__stack_chk_fail@PLT
.L606:
	movslq	-88(%rbp), %r12
	movq	-96(%rbp), %rdi
	jmp	.L457
	.cfi_endproc
.LFE2364:
	.size	_ZNK6icu_6720UnicodeSetStringSpan8spanBackEPKDsi17USetSpanCondition, .-_ZNK6icu_6720UnicodeSetStringSpan8spanBackEPKDsi17USetSpanCondition
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720UnicodeSetStringSpan11spanNotUTF8EPKhi
	.type	_ZNK6icu_6720UnicodeSetStringSpan11spanNotUTF8EPKhi, @function
_ZNK6icu_6720UnicodeSetStringSpan11spanNotUTF8EPKhi:
.LFB2369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	216(%rdi), %rax
	cmpb	$0, 260(%rdi)
	movq	%rsi, -56(%rbp)
	movl	%edx, -76(%rbp)
	movq	232(%rdi), %r14
	movl	8(%rax), %ebx
	je	.L608
	leal	(%rbx,%rbx), %eax
	cltq
	addq	%rax, %r14
.L608:
	leaq	8(%r15), %rax
	movl	-76(%rbp), %r13d
	xorl	%r12d, %r12d
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L623:
	movq	208(%r15), %rdi
	movslq	%r12d, %rsi
	xorl	%ecx, %ecx
	addq	-56(%rbp), %rsi
	movl	%r13d, %edx
	call	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition@PLT
	cmpl	%eax, %r13d
	je	.L622
	leal	(%r12,%rax), %r9d
	movl	%r13d, %r8d
	movslq	%r9d, %r12
	addq	-56(%rbp), %r12
	subl	%eax, %r8d
	movzbl	(%r12), %esi
	movl	%esi, %eax
	testb	%sil, %sil
	jns	.L652
	cmpl	$1, %r8d
	je	.L631
	cmpl	$223, %esi
	jle	.L614
	cmpl	$239, %esi
	jg	.L615
	movzbl	1(%r12), %edx
	andl	$15, %eax
	movl	%esi, %ecx
	leaq	.LC0(%rip), %rsi
	movsbl	(%rsi,%rax), %esi
	andl	$15, %ecx
	movl	%edx, %eax
	shrb	$5, %al
	btl	%eax, %esi
	jnc	.L631
	andl	$63, %edx
	movl	$2, %esi
	movl	$2, %r13d
.L616:
	movl	$65533, %r10d
	cmpl	%r13d, %r8d
	je	.L613
	sall	$6, %ecx
	movzbl	%dl, %eax
	orl	%ecx, %eax
	jmp	.L617
.L615:
	subl	$240, %esi
	cmpl	$4, %esi
	jg	.L631
	movzbl	1(%r12), %ecx
	leaq	.LC1(%rip), %rdx
	movq	%rcx, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%rdx,%rax), %eax
	btl	%esi, %eax
	jc	.L653
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$1, %r13d
	movl	$65533, %r10d
.L613:
	movq	-72(%rbp), %rdi
	movl	%r10d, %esi
	movl	%r9d, -60(%rbp)
	movl	%r8d, -64(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	-60(%rbp), %r9d
	testb	%al, %al
	je	.L654
.L607:
	addq	$40, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	.cfi_restore_state
	movq	-72(%rbp), %rdi
	movl	%r9d, -64(%rbp)
	movl	%r8d, -60(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	movl	-60(%rbp), %r8d
	movl	-64(%rbp), %r9d
	testb	%al, %al
	jne	.L607
	movl	$-1, %r13d
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L654:
	movl	-64(%rbp), %r8d
	negl	%r13d
.L612:
	movq	240(%r15), %rsi
	testl	%ebx, %ebx
	jle	.L618
	movq	224(%r15), %r10
	leal	-1(%rbx), %r11d
	xorl	%edx, %edx
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L619:
	addq	%rax, %rsi
	leaq	1(%rdx), %rax
	cmpq	%rdx, %r11
	je	.L618
.L633:
	movq	%rax, %rdx
.L621:
	movslq	(%r10,%rdx,4), %rax
	testl	%eax, %eax
	je	.L619
	cmpb	$-1, (%r14,%rdx)
	je	.L619
	cmpl	%eax, %r8d
	jl	.L619
	xorl	%ecx, %ecx
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L655:
	addq	$1, %rcx
	movl	%eax, %edi
	subl	%ecx, %edi
	testl	%edi, %edi
	jle	.L607
.L620:
	movzbl	(%rsi,%rcx), %edi
	cmpb	%dil, (%r12,%rcx)
	je	.L655
	addq	%rax, %rsi
	leaq	1(%rdx), %rax
	cmpq	%rdx, %r11
	jne	.L633
	.p2align 4,,10
	.p2align 3
.L618:
	subl	%r13d, %r9d
	movl	%r9d, %r12d
	addl	%r8d, %r13d
	jne	.L623
.L622:
	movl	-76(%rbp), %r9d
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L614:
	cmpl	$193, %esi
	jle	.L631
	andl	$31, %eax
	movl	$1, %esi
	movl	$1, %r13d
.L617:
	movzbl	(%r12,%rsi), %edx
	movl	$65533, %r10d
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L613
	sall	$6, %eax
	movzbl	%dl, %r10d
	addl	$1, %r13d
	orl	%eax, %r10d
	jmp	.L613
.L653:
	cmpl	$2, %r8d
	je	.L629
	movzbl	2(%r12), %eax
	leal	-128(%rax), %edx
	cmpb	$63, %dl
	ja	.L629
	sall	$6, %esi
	andl	$63, %ecx
	movl	$3, %r13d
	orl	%esi, %ecx
	movl	$3, %esi
	jmp	.L616
.L629:
	movl	$2, %r13d
	movl	$65533, %r10d
	jmp	.L613
	.cfi_endproc
.LFE2369:
	.size	_ZNK6icu_6720UnicodeSetStringSpan11spanNotUTF8EPKhi, .-_ZNK6icu_6720UnicodeSetStringSpan11spanNotUTF8EPKhi
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6720UnicodeSetStringSpan8spanUTF8EPKhi17USetSpanCondition
	.type	_ZNK6icu_6720UnicodeSetStringSpan8spanUTF8EPKhi17USetSpanCondition, @function
_ZNK6icu_6720UnicodeSetStringSpan8spanUTF8EPKhi17USetSpanCondition:
.LFB2365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -112(%rbp)
	movl	%edx, -128(%rbp)
	movl	%ecx, -124(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	testl	%ecx, %ecx
	jne	.L657
	call	_ZNK6icu_6720UnicodeSetStringSpan11spanNotUTF8EPKhi
	movl	%eax, -100(%rbp)
.L656:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L774
	movl	-100(%rbp), %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L657:
	.cfi_restore_state
	leaq	8(%rdi), %rax
	movl	$1, %ecx
	movl	%edx, %ebx
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition@PLT
	movl	%eax, -100(%rbp)
	cmpl	%eax, %ebx
	je	.L656
	leaq	-76(%rbp), %rax
	cmpl	$1, -124(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -96(%rbp)
	movl	$0, -80(%rbp)
	je	.L775
.L659:
	movq	216(%r14), %rax
	movq	232(%r14), %rbx
	movl	-128(%rbp), %r15d
	subl	-100(%rbp), %r15d
	movl	8(%rax), %eax
	cmpb	$0, 260(%r14)
	movq	%rbx, -144(%rbp)
	movl	%eax, -132(%rbp)
	jne	.L776
.L663:
	movl	-132(%rbp), %eax
	movq	240(%r14), %rbx
	subl	$1, %eax
	movq	%rax, -192(%rbp)
	salq	$2, %rax
	movq	%rax, -168(%rbp)
	movl	-100(%rbp), %eax
	movl	%eax, -104(%rbp)
.L664:
	cmpl	$1, -124(%rbp)
	je	.L777
	movl	-132(%rbp), %edi
	testl	%edi, %edi
	jle	.L666
	movq	%rbx, -152(%rbp)
	movq	%rbx, %r10
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	movq	224(%r14), %rax
	movq	-168(%rbp), %rsi
	movq	%r14, -160(%rbp)
	movslq	-104(%rbp), %rcx
	movq	-144(%rbp), %r13
	leaq	4(%rax), %r12
	movl	-100(%rbp), %r14d
	addq	%r12, %rsi
	movq	%rcx, -120(%rbp)
	movq	%rsi, %rbx
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%r12, %rax
	addq	$1, %r13
	cmpq	%r12, %rbx
	je	.L765
.L779:
	addq	$4, %r12
.L686:
	movslq	(%rax), %r8
	testl	%r8d, %r8d
	je	.L678
	movzbl	0(%r13), %ecx
	movl	%r8d, %esi
	cmpl	$254, %ecx
	cmovge	%r8d, %ecx
	cmpl	%ecx, %r14d
	cmovle	%r14d, %ecx
	subl	%ecx, %esi
	cmpl	%r9d, %ecx
	jl	.L716
	cmpl	%esi, %r15d
	jl	.L716
	movq	-120(%rbp), %rdx
	movslq	%ecx, %rax
	subq	%rax, %rdx
	addq	-112(%rbp), %rdx
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L778:
	cmpl	%r11d, %esi
	jg	.L726
.L681:
	addl	$1, %esi
	subl	$1, %ecx
	addq	$1, %rdx
	cmpl	%esi, %r15d
	jl	.L716
	cmpl	%r9d, %ecx
	jl	.L716
.L684:
	cmpb	$-64, (%rdx)
	jl	.L681
	cmpl	%r9d, %ecx
	jle	.L778
.L726:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L683:
	movzbl	(%r10,%rax), %edi
	cmpb	%dil, (%rdx,%rax)
	jne	.L681
	addq	$1, %rax
	movl	%r8d, %edi
	subl	%eax, %edi
	testl	%edi, %edi
	jg	.L683
	addq	%r8, %r10
	movl	%ecx, %r9d
	movl	%esi, %r11d
	.p2align 4,,10
	.p2align 3
.L781:
	movq	%r12, %rax
	addq	$1, %r13
	cmpq	%r12, %rbx
	jne	.L779
.L765:
	orl	%r11d, %r9d
	movq	-152(%rbp), %rbx
	movq	-160(%rbp), %r14
	je	.L666
	subl	%r11d, %r15d
	jne	.L780
.L773:
	movl	-128(%rbp), %eax
	movq	-96(%rbp), %r9
	movl	%eax, -100(%rbp)
.L692:
	cmpq	-184(%rbp), %r9
	je	.L656
	movq	%r9, %rdi
	call	uprv_free_67@PLT
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L716:
	movl	%r9d, %ecx
	movl	%r11d, %esi
	addq	%r8, %r10
	movl	%ecx, %r9d
	movl	%esi, %r11d
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L666:
	movl	-100(%rbp), %esi
	testl	%esi, %esi
	jne	.L728
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L728
	movl	-84(%rbp), %eax
	movslq	-104(%rbp), %rsi
	addq	-112(%rbp), %rsi
	testl	%eax, %eax
	jne	.L693
	movq	-176(%rbp), %rdi
	movl	$1, %ecx
	movl	%r15d, %edx
	call	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition@PLT
	movl	%eax, -100(%rbp)
	cmpl	%eax, %r15d
	je	.L729
	testl	%eax, %eax
	je	.L729
	addl	%eax, -104(%rbp)
	movq	240(%r14), %rbx
	subl	%eax, %r15d
	jmp	.L664
.L777:
	movl	-132(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L666
	movslq	-104(%rbp), %r10
	movq	%rbx, %rdi
	movq	-144(%rbp), %r8
	xorl	%r13d, %r13d
	movq	-192(%rbp), %rbx
	movq	%r10, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L677:
	movq	224(%r14), %rax
	movslq	(%rax,%r13,4), %r12
	testl	%r12d, %r12d
	je	.L667
	movzbl	(%r8,%r13), %edx
	cmpl	$255, %edx
	je	.L675
	cmpl	$254, %edx
	je	.L782
.L669:
	movl	-100(%rbp), %eax
	movl	%r12d, %ecx
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	subl	%edx, %ecx
	cmpl	%ecx, %r15d
	jl	.L675
	movq	-120(%rbp), %rsi
	movslq	%edx, %rax
	leal	(%rdx,%rcx), %r11d
	subq	%rax, %rsi
	addq	-112(%rbp), %rsi
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L671:
	cmpl	%r11d, %ecx
	je	.L675
.L784:
	addl	$1, %ecx
	addq	$1, %rsi
	cmpl	%ecx, %r15d
	jl	.L675
.L676:
	cmpb	$-64, (%rsi)
	jl	.L671
	movl	-80(%rbp), %eax
	movl	-88(%rbp), %edx
	addl	%ecx, %eax
	movl	%eax, %r9d
	subl	%edx, %r9d
	cmpl	%eax, %edx
	cmovle	%r9d, %eax
	movq	-96(%rbp), %r9
	cltq
	leaq	(%r9,%rax), %rdx
	cmpb	$0, (%rdx)
	jne	.L671
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L673:
	movzbl	(%rdi,%rax), %r10d
	cmpb	%r10b, (%rsi,%rax)
	jne	.L671
	addq	$1, %rax
	movl	%r12d, %r10d
	subl	%eax, %r10d
	testl	%r10d, %r10d
	jg	.L673
	cmpl	%ecx, %r15d
	je	.L783
	movb	$1, (%rdx)
	addl	$1, -84(%rbp)
	cmpl	%r11d, %ecx
	jne	.L784
	.p2align 4,,10
	.p2align 3
.L675:
	addq	%r12, %rdi
.L667:
	leaq	1(%r13), %rax
	cmpq	%r13, %rbx
	je	.L666
	movq	%rax, %r13
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L782:
	leal	-1(%r12), %edx
	movslq	%edx, %rax
	cmpb	$-64, (%rdi,%rax)
	jge	.L669
	xorl	%esi, %esi
	movq	%r8, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	utf8_back1SafeBody_67@PLT
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rdi
	movl	%eax, %edx
	jmp	.L669
.L728:
	movl	-84(%rbp), %edx
	testl	%edx, %edx
	je	.L785
.L691:
	movl	-80(%rbp), %r8d
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %rdx
	leal	1(%r8), %eax
	cltq
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L787:
	leaq	(%rdx,%rax), %rdi
	addq	$1, %rax
	cmpb	$0, -1(%rdx,%rax)
	jne	.L786
.L710:
	movl	%eax, %esi
	cmpl	%eax, %ecx
	jg	.L787
	subl	%r8d, %ecx
	cmpb	$0, (%rdx)
	jne	.L724
	leaq	1(%rdx), %rax
	movl	$1, %edi
	subl	%eax, %edi
	.p2align 4,,10
	.p2align 3
.L713:
	movq	%rax, %rdx
	leal	(%rdi,%rax), %esi
	leaq	1(%rax), %rax
	cmpb	$0, (%rdx)
	je	.L713
	addl	%esi, %ecx
.L712:
	movb	$0, (%rdx)
	subl	$1, -84(%rbp)
	movl	%esi, -80(%rbp)
.L711:
	addl	%ecx, -104(%rbp)
	movq	240(%r14), %rbx
	subl	%ecx, %r15d
	movl	$0, -100(%rbp)
	jmp	.L664
.L776:
	addl	%eax, %eax
	cltq
	addq	%rax, %rbx
	movq	%rbx, -144(%rbp)
	jmp	.L663
.L780:
	addl	%r11d, -104(%rbp)
	movl	$0, -100(%rbp)
	jmp	.L664
.L693:
	movzbl	(%rsi), %r8d
	movl	%r8d, %eax
	testb	%r8b, %r8b
	jns	.L788
	cmpl	$1, %r15d
	je	.L699
	cmpl	$223, %r8d
	jle	.L700
	cmpl	$239, %r8d
	jg	.L701
	movzbl	1(%rsi), %ecx
	andl	$15, %eax
	leaq	.LC0(%rip), %rbx
	movl	%r8d, %edx
	movsbl	(%rbx,%rax), %edi
	andl	$15, %edx
	movl	%ecx, %eax
	shrb	$5, %al
	btl	%eax, %edi
	jnc	.L722
	movl	%ecx, %eax
	movl	$2, %ebx
	movl	$2, %ecx
	andl	$63, %eax
.L703:
	cmpl	%ebx, %r15d
	je	.L699
	sall	$6, %edx
	movzbl	%al, %eax
	orl	%edx, %eax
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L786:
	movb	$0, (%rdi)
	movl	%esi, %ecx
	subl	-80(%rbp), %ecx
	subl	$1, -84(%rbp)
	movl	%esi, -80(%rbp)
	jmp	.L711
.L729:
	movl	-104(%rbp), %ebx
	movq	-96(%rbp), %r9
	addl	%ebx, -100(%rbp)
	jmp	.L692
.L775:
	movl	256(%r14), %ebx
	cmpl	$16, %ebx
	jg	.L660
	movl	$16, -88(%rbp)
	movq	%rax, %rdi
	movl	$16, %r12d
.L661:
	movq	%r12, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	jmp	.L659
.L783:
	movl	-128(%rbp), %eax
	movl	%eax, -100(%rbp)
	jmp	.L692
.L788:
	movq	-176(%rbp), %rdi
	movl	%r8d, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L691
	movl	$1, %ebx
.L698:
	cmpl	%ebx, %r15d
	je	.L773
.L706:
	movl	-88(%rbp), %eax
	addl	%ebx, -104(%rbp)
	subl	%ebx, %r15d
	addl	-80(%rbp), %ebx
	movl	%ebx, %edx
	subl	%eax, %edx
	cmpl	%eax, %ebx
	cmovge	%edx, %ebx
	movslq	%ebx, %rax
	addq	-96(%rbp), %rax
	cmpb	$0, (%rax)
	je	.L708
	movb	$0, (%rax)
	subl	$1, -84(%rbp)
.L708:
	movl	%ebx, -80(%rbp)
	movq	240(%r14), %rbx
	movl	$0, -100(%rbp)
	jmp	.L664
.L700:
	cmpl	$193, %r8d
	jle	.L722
	andl	$31, %eax
	movl	$1, %ecx
	movl	$1, %ebx
.L704:
	movzbl	(%rsi,%rcx), %edx
	movl	$65533, %esi
	addl	$-128, %edx
	cmpb	$63, %dl
	ja	.L705
	sall	$6, %eax
	movzbl	%dl, %edx
	addl	$1, %ebx
	orl	%edx, %eax
	movl	%eax, %esi
.L705:
	movq	-176(%rbp), %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L691
	jmp	.L698
.L724:
	xorl	%esi, %esi
	jmp	.L712
.L722:
	movl	$1, %ebx
.L702:
	movq	-176(%rbp), %rdi
	movl	$65533, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	je	.L691
	jmp	.L706
.L699:
	movq	-176(%rbp), %rdi
	movl	$65533, %esi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L773
	jmp	.L691
.L660:
	movslq	%ebx, %r12
	movq	%r12, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L789
	movq	%rax, -96(%rbp)
	movl	%ebx, -88(%rbp)
	jmp	.L661
.L701:
	subl	$240, %r8d
	cmpl	$4, %r8d
	jg	.L722
	movzbl	1(%rsi), %edx
	leaq	.LC1(%rip), %rbx
	movq	%rdx, %rax
	shrq	$4, %rax
	andl	$15, %eax
	movsbl	(%rbx,%rax), %eax
	btl	%r8d, %eax
	jnc	.L722
	cmpl	$2, %r15d
	je	.L699
	movzbl	2(%rsi), %eax
	addl	$-128, %eax
	cmpb	$63, %al
	ja	.L721
	sall	$6, %r8d
	andl	$63, %edx
	movl	$3, %ecx
	movl	$3, %ebx
	orl	%r8d, %edx
	jmp	.L703
.L785:
	movl	-104(%rbp), %eax
	movq	-96(%rbp), %r9
	movl	%eax, -100(%rbp)
	jmp	.L692
.L789:
	movslq	-88(%rbp), %r12
	movq	-96(%rbp), %rdi
	jmp	.L661
.L774:
	call	__stack_chk_fail@PLT
.L721:
	movl	$2, %ebx
	jmp	.L702
	.cfi_endproc
.LFE2365:
	.size	_ZNK6icu_6720UnicodeSetStringSpan8spanUTF8EPKhi17USetSpanCondition, .-_ZNK6icu_6720UnicodeSetStringSpan8spanUTF8EPKhi17USetSpanCondition
	.align 2
	.p2align 4
	.globl	_ZN6icu_6720UnicodeSetStringSpanC2ERKNS_10UnicodeSetERKNS_7UVectorEj
	.type	_ZN6icu_6720UnicodeSetStringSpanC2ERKNS_10UnicodeSetERKNS_7UVectorEj, @function
_ZN6icu_6720UnicodeSetStringSpanC2ERKNS_10UnicodeSetERKNS_7UVectorEj:
.LFB2347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$1114111, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r15, %rdi
	subq	$104, %rsp
	movl	%ecx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6710UnicodeSetC1Eii@PLT
	cmpl	$63, %r14d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	$0, 208(%rbx)
	movq	%r13, 216(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 232(%rbx)
	movq	$0, 240(%rbx)
	movq	$0, 248(%rbx)
	movl	$0, 256(%rbx)
	sete	260(%rbx)
	call	_ZN6icu_6710UnicodeSet9retainAllERKS0_@PLT
	andl	$1, %r14d
	movl	%r14d, -100(%rbp)
	je	.L791
	movq	%r15, 208(%rbx)
.L791:
	movq	216(%rbx), %rdi
	movl	8(%rdi), %eax
	movl	%eax, -84(%rbp)
	testl	%eax, %eax
	jle	.L793
	movl	-104(%rbp), %eax
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movl	%eax, %ecx
	andl	$8, %ecx
	movl	%ecx, -80(%rbp)
	movl	%eax, %ecx
	andl	$2, %eax
	andl	$4, %ecx
	movl	%eax, -88(%rbp)
	movl	%ecx, -76(%rbp)
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L934:
	sarl	$5, %r8d
.L797:
	movl	%r8d, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%r8d, -72(%rbp)
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	movl	-72(%rbp), %r8d
	movl	-80(%rbp), %r11d
	cmpl	%r8d, %eax
	movl	$1, %eax
	cmovl	%eax, %r13d
	setl	%dl
	testl	%r11d, %r11d
	je	.L799
	cmpl	%r8d, 252(%rbx)
	jge	.L799
	movl	%r8d, 252(%rbx)
.L799:
	movl	-76(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L801
	testb	%dl, %dl
	jne	.L802
	movl	-88(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L801
.L802:
	leaq	-60(%rbp), %rdx
	leaq	-64(%rbp), %r9
	movq	%r14, %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movl	$0, -64(%rbp)
	movl	$0, -60(%rbp)
	call	u_strToUTF8_67@PLT
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jle	.L861
	xorl	%eax, %eax
	cmpl	$15, %edx
	je	.L861
.L804:
	addl	%eax, 248(%rbx)
	cmpl	%eax, 256(%rbx)
	jge	.L801
	movl	%eax, 256(%rbx)
	.p2align 4,,10
	.p2align 3
.L801:
	addl	$1, %r12d
	cmpl	-84(%rbp), %r12d
	je	.L806
	movq	216(%rbx), %rdi
.L807:
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movswl	8(%rax), %r8d
	testb	$17, %r8b
	jne	.L854
	leaq	10(%rax), %r14
	testb	$2, %r8b
	jne	.L794
	movq	24(%rax), %r14
.L794:
	testw	%r8w, %r8w
	jns	.L934
	movl	12(%rax), %r8d
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L854:
	xorl	%r14d, %r14d
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L861:
	movl	-60(%rbp), %eax
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L806:
	testb	%r13b, %r13b
	je	.L793
	cmpb	$0, 260(%rbx)
	jne	.L935
.L811:
	movl	-84(%rbp), %ecx
	movl	-76(%rbp), %r8d
	movl	%ecx, %edi
	testl	%r8d, %r8d
	je	.L812
	movl	248(%rbx), %eax
	leal	(%rax,%rcx,4), %edi
	addl	%ecx, %edi
	cmpl	$128, %edi
	jle	.L936
.L813:
	movslq	%edi, %rdi
	call	uprv_malloc_67@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, 224(%rbx)
	testq	%rax, %rax
	jne	.L814
	.p2align 4,,10
	.p2align 3
.L793:
	movq	$0, 252(%rbx)
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L935:
	movq	%r15, %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	cmpb	$0, 260(%rbx)
	je	.L811
	movl	248(%rbx), %eax
	movl	-84(%rbp), %esi
	leal	(%rax,%rsi,8), %edi
.L812:
	cmpl	$128, %edi
	jg	.L813
.L936:
	leaq	264(%rbx), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, 224(%rbx)
.L814:
	cmpb	$0, 260(%rbx)
	je	.L815
	movslq	-84(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,4), %rdx
	leaq	(%rdx,%rax), %rcx
	movq	%rdx, 232(%rbx)
	leaq	(%rcx,%rax), %rsi
	movq	%rcx, -96(%rbp)
	leaq	(%rsi,%rax), %rcx
	movq	%rsi, -120(%rbp)
	addq	%rcx, %rax
	movq	%rcx, -128(%rbp)
	movq	%rax, 240(%rbx)
.L816:
	movl	-104(%rbp), %eax
	xorl	%r12d, %r12d
	movl	%eax, %ecx
	andl	$16, %eax
	movl	%eax, -108(%rbp)
	movl	-84(%rbp), %eax
	andl	$32, %ecx
	movl	%ecx, -104(%rbp)
	subl	$1, %eax
	movl	$0, -84(%rbp)
	movq	%rax, -72(%rbp)
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L942:
	movl	-80(%rbp), %esi
	testl	%esi, %esi
	je	.L824
	movl	-88(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L825
	movl	-104(%rbp), %edx
	testl	%edx, %edx
	je	.L826
	movq	232(%rbx), %rdx
	movl	$254, %ecx
	addq	%r12, %rdx
	cmpl	$254, %eax
	cmovge	%ecx, %eax
	movb	%al, (%rdx)
.L826:
	movl	-108(%rbp), %eax
	testl	%eax, %eax
	jne	.L937
	.p2align 4,,10
	.p2align 3
.L824:
	movl	-76(%rbp), %eax
	testl	%eax, %eax
	jne	.L938
.L830:
	movl	-100(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L838
	movl	-104(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L840
	movzwl	0(%r13), %esi
	movl	%esi, %eax
	andl	$-1024, %eax
	cmpl	$55296, %eax
	jne	.L841
	cmpl	$1, %r14d
	jne	.L939
.L841:
	movq	208(%rbx), %rdi
	cmpq	%rdi, %r15
	je	.L862
	testq	%rdi, %rdi
	je	.L862
.L842:
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
.L840:
	movl	-108(%rbp), %edi
	testl	%edi, %edi
	je	.L838
	leal	-1(%r14), %r8d
	movslq	%r8d, %rax
	movzwl	0(%r13,%rax,2), %r14d
	leaq	(%rax,%rax), %rdx
	movl	%r14d, %eax
	andl	$-1024, %eax
	cmpl	$56320, %eax
	jne	.L845
	testl	%r8d, %r8d
	jg	.L940
.L845:
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L863
	cmpq	%rdi, %r15
	je	.L863
.L846:
	movl	%r14d, %esi
	call	_ZN6icu_6710UnicodeSet3addEi@PLT
	.p2align 4,,10
	.p2align 3
.L838:
	leaq	1(%r12), %rax
	cmpq	-72(%rbp), %r12
	je	.L941
.L860:
	movq	%rax, %r12
.L852:
	movq	216(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZNK6icu_677UVector9elementAtEi@PLT
	movswl	8(%rax), %r8d
	testb	$17, %r8b
	jne	.L858
	leaq	10(%rax), %r13
	testb	$2, %r8b
	jne	.L818
	movq	24(%rax), %r13
.L818:
	testw	%r8w, %r8w
	js	.L820
	movl	%r8d, %r14d
	sarl	$5, %r14d
.L821:
	movl	$1, %ecx
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet4spanEPKDsi17USetSpanCondition@PLT
	cmpl	%r14d, %eax
	jl	.L942
	movl	-76(%rbp), %esi
	testl	%esi, %esi
	je	.L848
	movl	-88(%rbp), %ecx
	leaq	0(,%r12,4), %r11
	testl	%ecx, %ecx
	jne	.L943
	movq	224(%rbx), %rax
	movl	$0, (%rax,%r12,4)
.L848:
	cmpb	$0, 260(%rbx)
	je	.L851
	movq	-128(%rbp), %rax
	movb	$-1, (%rax,%r12)
	movq	-120(%rbp), %rax
	movb	$-1, (%rax,%r12)
	movq	-96(%rbp), %rax
	movb	$-1, (%rax,%r12)
	movq	232(%rbx), %rax
	movb	$-1, (%rax,%r12)
	leaq	1(%r12), %rax
	cmpq	-72(%rbp), %r12
	jne	.L860
.L941:
	cmpb	$0, 260(%rbx)
	jne	.L944
.L790:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L945
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_restore_state
	movl	12(%rax), %r14d
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L851:
	movq	232(%rbx), %rax
	movb	$-1, (%rax,%r12)
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L858:
	xorl	%r13d, %r13d
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L938:
	movslq	-84(%rbp), %r11
	movl	248(%rbx), %esi
	movl	%r14d, %r8d
	movq	%r13, %rcx
	leaq	-60(%rbp), %rdx
	leaq	-64(%rbp), %r9
	movl	$0, -64(%rbp)
	movq	%r11, %rax
	addq	240(%rbx), %r11
	movl	$0, -60(%rbp)
	subl	%eax, %esi
	movq	%r11, %rdi
	movq	%r11, -136(%rbp)
	call	u_strToUTF8_67@PLT
	movl	-64(%rbp), %eax
	movq	-136(%rbp), %r11
	testl	%eax, %eax
	jle	.L831
	movq	224(%rbx), %rax
	movl	$0, (%rax,%r12,4)
.L832:
	movq	-128(%rbp), %rax
	movb	$-1, (%rax,%r12)
	movq	-120(%rbp), %rax
	movb	$-1, (%rax,%r12)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L825:
	movq	-96(%rbp), %rax
	movb	$0, (%rax,%r12)
	movq	232(%rbx), %rax
	movb	$0, (%rax,%r12)
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L943:
	movslq	-84(%rbp), %rdi
	movl	248(%rbx), %esi
	movl	%r14d, %r8d
	movq	%r13, %rcx
	leaq	-60(%rbp), %rdx
	leaq	-64(%rbp), %r9
	movq	%r11, -136(%rbp)
	movq	%rdi, %rax
	addq	240(%rbx), %rdi
	movl	$0, -64(%rbp)
	subl	%eax, %esi
	movl	$0, -60(%rbp)
	call	u_strToUTF8_67@PLT
	movl	-64(%rbp), %edx
	xorl	%eax, %eax
	movq	-136(%rbp), %r11
	testl	%edx, %edx
	jg	.L850
	movl	-60(%rbp), %eax
	addl	%eax, -84(%rbp)
.L850:
	movq	224(%rbx), %rdx
	movl	%eax, (%rdx,%r11)
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L831:
	movl	-60(%rbp), %edx
	movq	224(%rbx), %rax
	addl	%edx, -84(%rbp)
	movl	%edx, (%rax,%r12,4)
	testl	%edx, %edx
	je	.L832
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	je	.L834
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L946
.L835:
	movl	-108(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L830
	movl	$1, %ecx
	movq	%r11, %rsi
	movq	%r15, %rdi
	movl	%edx, -136(%rbp)
	call	_ZNK6icu_6710UnicodeSet12spanBackUTF8EPKci17USetSpanCondition@PLT
	movl	-136(%rbp), %edx
	subl	%eax, %edx
	movl	$254, %eax
	cmpl	$254, %edx
	cmovge	%eax, %edx
	movq	-128(%rbp), %rax
	movb	%dl, (%rax,%r12)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L815:
	movl	-76(%rbp), %edi
	testl	%edi, %edi
	je	.L817
	movslq	-84(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	(%rsi,%rax,4), %rcx
	addq	%rcx, %rax
	movq	%rcx, -96(%rbp)
	movq	%rax, 240(%rbx)
.L817:
	movq	-96(%rbp), %rax
	movq	%rax, 232(%rbx)
	movq	%rax, -128(%rbp)
	movq	%rax, -120(%rbp)
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L863:
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L838
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet13cloneAsThawedEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L838
	movq	%rax, 208(%rbx)
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L862:
	movq	%r15, %rdi
	movl	%esi, -136(%rbp)
	call	_ZNK6icu_6710UnicodeSet8containsEi@PLT
	testb	%al, %al
	jne	.L840
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet13cloneAsThawedEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L840
	movq	%rax, 208(%rbx)
	movl	-136(%rbp), %esi
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L834:
	movq	-128(%rbp), %rax
	movb	$0, (%rax,%r12)
	movq	-120(%rbp), %rax
	movb	$0, (%rax,%r12)
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L937:
	movl	%r14d, %edx
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZNK6icu_6710UnicodeSet8spanBackEPKDsi17USetSpanCondition@PLT
	movl	%r14d, %esi
	movl	$254, %edx
	subl	%eax, %esi
	movl	%esi, %eax
	cmpl	$254, %esi
	movq	-96(%rbp), %rsi
	cmovge	%edx, %eax
	movb	%al, (%rsi,%r12)
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L944:
	movq	208(%rbx), %rdi
	call	_ZN6icu_6710UnicodeSet6freezeEv@PLT
	jmp	.L790
.L946:
	movl	$1, %ecx
	movq	%r11, %rsi
	movq	%r15, %rdi
	movl	%edx, -112(%rbp)
	movq	%r11, -136(%rbp)
	call	_ZNK6icu_6710UnicodeSet8spanUTF8EPKci17USetSpanCondition@PLT
	movl	$254, %ecx
	movl	-112(%rbp), %edx
	movq	-136(%rbp), %r11
	cmpl	$254, %eax
	cmovge	%ecx, %eax
	movq	-120(%rbp), %rcx
	movb	%al, (%rcx,%r12)
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L939:
	movzwl	2(%r13), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$56320, %edx
	jne	.L841
	sall	$10, %esi
	leal	-56613888(%rax,%rsi), %esi
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L940:
	movzwl	-2(%r13,%rdx), %eax
	movl	%eax, %edx
	andl	$-1024, %edx
	cmpl	$55296, %edx
	jne	.L845
	sall	$10, %eax
	leal	-56613888(%r14,%rax), %r14d
	jmp	.L845
.L945:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2347:
	.size	_ZN6icu_6720UnicodeSetStringSpanC2ERKNS_10UnicodeSetERKNS_7UVectorEj, .-_ZN6icu_6720UnicodeSetStringSpanC2ERKNS_10UnicodeSetERKNS_7UVectorEj
	.globl	_ZN6icu_6720UnicodeSetStringSpanC1ERKNS_10UnicodeSetERKNS_7UVectorEj
	.set	_ZN6icu_6720UnicodeSetStringSpanC1ERKNS_10UnicodeSetERKNS_7UVectorEj,_ZN6icu_6720UnicodeSetStringSpanC2ERKNS_10UnicodeSetERKNS_7UVectorEj
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
