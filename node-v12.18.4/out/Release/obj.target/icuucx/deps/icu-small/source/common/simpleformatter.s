	.file	"simpleformatter.cpp"
	.text
	.align 2
	.p2align 4
	.type	_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode.part.0, @function
_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode.part.0:
.LFB1794:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	24(%rbp), %eax
	movq	%rdx, -56(%rbp)
	movq	%r8, -72(%rbp)
	movb	%r9b, -73(%rbp)
	testl	%eax, %eax
	jle	.L5
	movl	24(%rbp), %eax
	movq	16(%rbp), %rdi
	movl	$255, %esi
	subl	$1, %eax
	leaq	4(,%rax,4), %rdx
	call	memset@PLT
.L5:
	cmpl	$1, %r12d
	jle	.L1
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L4:
	leal	1(%rax), %r13d
	cltq
	movzwl	(%rbx,%rax,2), %ecx
	leaq	(%rax,%rax), %rsi
	cmpl	$255, %ecx
	jg	.L6
	movq	-56(%rbp), %rax
	movq	(%rax,%rcx,8), %rsi
	testq	%rsi, %rsi
	je	.L10
	cmpq	%r15, %rsi
	je	.L26
	cmpl	%ecx, 24(%rbp)
	jle	.L18
	movq	16(%rbp), %rax
	leaq	(%rax,%rcx,4), %rdx
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L19
	sarl	$5, %eax
.L20:
	movl	%eax, (%rdx)
.L18:
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	js	.L21
	sarl	$5, %ecx
.L22:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	%r13d, %eax
.L12:
	cmpl	%eax, %r12d
	jg	.L4
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	leal	-256(%rcx), %r14d
	leaq	2(%rbx,%rsi), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r14d, %ecx
	movq	%rsi, -64(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-64(%rbp), %rsi
	leal	0(%r13,%r14), %eax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L26:
	cmpb	$0, -73(%rbp)
	jne	.L10
	cmpl	$2, %r13d
	je	.L27
	cmpl	%ecx, 24(%rbp)
	jle	.L13
	movq	16(%rbp), %rax
	leaq	(%rax,%rcx,4), %rdx
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L14
	sarl	$5, %eax
.L15:
	movl	%eax, (%rdx)
.L13:
	movq	-72(%rbp), %rax
	movswl	8(%rax), %ecx
	testw	%cx, %cx
	js	.L16
	sarl	$5, %ecx
	movq	%rax, %rsi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L21:
	movl	12(%rsi), %ecx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L19:
	movl	12(%r15), %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$2, %eax
	cmpl	%ecx, 24(%rbp)
	jle	.L12
	movq	16(%rbp), %rdi
	movl	$0, (%rdi,%rcx,4)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	movl	12(%rax), %ecx
	movq	%rax, %rsi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rbp), %rax
	movl	$1, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	12(%r15), %eax
	jmp	.L15
	.cfi_endproc
.LFE1794:
	.size	_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode.part.0, .-_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode.part.0
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715SimpleFormatteraSERKS0_
	.type	_ZN6icu_6715SimpleFormatteraSERKS0_, @function
_ZN6icu_6715SimpleFormatteraSERKS0_:
.LFB1318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpq	%rsi, %rdi
	je	.L29
	addq	$8, %rsi
	leaq	8(%rdi), %rdi
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
.L29:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1318:
	.size	_ZN6icu_6715SimpleFormatteraSERKS0_, .-_ZN6icu_6715SimpleFormatteraSERKS0_
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715SimpleFormatterD2Ev
	.type	_ZN6icu_6715SimpleFormatterD2Ev, @function
_ZN6icu_6715SimpleFormatterD2Ev:
.LFB1320:
	.cfi_startproc
	endbr64
	addq	$8, %rdi
	jmp	_ZN6icu_6713UnicodeStringD1Ev@PLT
	.cfi_endproc
.LFE1320:
	.size	_ZN6icu_6715SimpleFormatterD2Ev, .-_ZN6icu_6715SimpleFormatterD2Ev
	.globl	_ZN6icu_6715SimpleFormatterD1Ev
	.set	_ZN6icu_6715SimpleFormatterD1Ev,_ZN6icu_6715SimpleFormatterD2Ev
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode
	.type	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode, @function
_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode:
.LFB1322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%ecx, -104(%rbp)
	movl	(%r8), %ecx
	movq	%rdi, -72(%rbp)
	movl	%edx, -100(%rbp)
	movq	%r8, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jg	.L32
	movswl	8(%rsi), %ebx
	testb	$17, %bl
	jne	.L64
	leaq	10(%rsi), %r13
	testb	$2, %bl
	jne	.L34
	movq	24(%rsi), %r13
.L34:
	testw	%bx, %bx
	js	.L36
	sarl	$5, %ebx
.L37:
	movq	-72(%rbp), %r15
	xorl	%edx, %edx
	movw	%dx, -58(%rbp)
	leaq	8(%r15), %r14
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString7unBogusEv@PLT
	movswl	16(%r15), %edx
	testw	%dx, %dx
	js	.L38
	sarl	$5, %edx
.L39:
	leaq	-58(%rbp), %r15
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	%r15, %rcx
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9doReplaceEiiPKDsii@PLT
	testl	%ebx, %ebx
	jle	.L65
	movl	$-1, -88(%rbp)
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	movzwl	0(%r13), %r9d
.L41:
	leal	1(%r12), %r8d
	cmpw	$39, %r9w
	je	.L83
	testb	%r10b, %r10b
	jne	.L46
	cmpw	$123, %r9w
	jne	.L46
	testl	%eax, %eax
	je	.L48
	movq	-72(%rbp), %rdi
	leal	256(%rax), %r9d
	movzwl	%r9w, %r9d
	movzwl	16(%rdi), %edx
	testw	%dx, %dx
	js	.L49
	movswl	%dx, %esi
	sarl	$5, %esi
.L50:
	subl	%eax, %esi
	movl	%r9d, %edx
	movq	%r14, %rdi
	movl	%r8d, -76(%rbp)
	subl	$1, %esi
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
	movl	-76(%rbp), %r8d
.L48:
	leal	2(%r12), %edx
	cmpl	%ebx, %edx
	jge	.L51
	movslq	%r8d, %rax
	leaq	(%rax,%rax), %rcx
	movzwl	0(%r13,%rax,2), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L84
.L51:
	cmpl	%ebx, %r8d
	jge	.L54
	movslq	%r8d, %r8
	movzwl	0(%r13,%r8,2), %eax
	leal	-49(%rax), %ecx
	cmpw	$8, %cx
	jbe	.L85
	.p2align 4,,10
	.p2align 3
.L54:
	movq	-96(%rbp), %rax
	movl	$1, (%rax)
	xorl	%eax, %eax
.L32:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L86
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	cmpl	%ebx, %r8d
	jge	.L44
	movslq	%r8d, %rdx
	movzwl	0(%r13,%rdx,2), %edx
	cmpw	$39, %dx
	je	.L87
	testb	%r10b, %r10b
	jne	.L66
	leal	-123(%rdx), %ecx
	testw	$-3, %cx
	jne	.L46
	leal	2(%r12), %r8d
	movl	%edx, %r9d
	movl	$1, %r10d
	.p2align 4,,10
	.p2align 3
.L46:
	leal	1(%rax), %r11d
	testl	%eax, %eax
	je	.L88
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movb	%r10b, -84(%rbp)
	movl	%r11d, -80(%rbp)
	movl	%r8d, -76(%rbp)
	movw	%r9w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-80(%rbp), %r11d
	movl	-76(%rbp), %r8d
	movl	$0, %eax
	movzbl	-84(%rbp), %r10d
	cmpl	$65279, %r11d
	movl	%r8d, %r12d
	cmovne	%r11d, %eax
.L58:
	cmpl	%ebx, %r12d
	jge	.L42
	movslq	%r12d, %rdx
	movzwl	0(%r13,%rdx,2), %r9d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$-1, %eax
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movb	%r10b, -80(%rbp)
	movl	%r8d, -76(%rbp)
	movl	%r9d, -84(%rbp)
	movw	%ax, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-84(%rbp), %r9d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movw	%r9w, -58(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movl	-76(%rbp), %r8d
	movzbl	-80(%rbp), %r10d
	movl	$1, %eax
	movl	%r8d, %r12d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L44:
	testb	%r10b, %r10b
	je	.L46
.L42:
	movl	-88(%rbp), %ebx
	addl	$1, %ebx
	testl	%eax, %eax
	je	.L40
	movq	-72(%rbp), %rdi
	leal	256(%rax), %r8d
	movzwl	%r8w, %r8d
	movzwl	16(%rdi), %edx
	testw	%dx, %dx
	js	.L60
	movswl	%dx, %esi
	sarl	$5, %esi
.L61:
	subl	%eax, %esi
	movl	%r8d, %edx
	movq	%r14, %rdi
	subl	$1, %esi
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
.L40:
	cmpl	%ebx, -100(%rbp)
	jg	.L54
	cmpl	%ebx, -104(%rbp)
	jl	.L54
	movzwl	%bx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString9setCharAtEiDs@PLT
	movl	$1, %eax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-72(%rbp), %rax
	movl	20(%rax), %edx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L36:
	movl	12(%rsi), %ebx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L85:
	subl	$48, %eax
	movslq	%edx, %rdx
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	%edx, %ebx
	jle	.L54
	movzwl	0(%r13,%rdx,2), %ecx
	cmpw	$47, %cx
	jbe	.L54
	leal	1(%rdx), %r12d
	cmpw	$57, %cx
	jbe	.L56
	cmpw	$125, %cx
	jne	.L54
.L55:
	movl	-88(%rbp), %edi
	movl	$1, %ecx
	movq	%r15, %rsi
	movw	%ax, -58(%rbp)
	cmpl	%eax, %edi
	cmovl	%eax, %edi
	xorl	%edx, %edx
	movl	%edi, -88(%rbp)
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L56:
	leal	(%rax,%rax,4), %eax
	addq	$1, %rdx
	leal	-48(%rcx,%rax,2), %eax
	cmpl	$255, %eax
	jle	.L57
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L49:
	movl	20(%rdi), %esi
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%r13d, %r13d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L87:
	leal	2(%r12), %r8d
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L84:
	cmpw	$125, 2(%r13,%rcx)
	jne	.L51
	addl	$3, %r12d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L66:
	movl	%edx, %r9d
	movl	%r8d, %r12d
	xorl	%r10d, %r10d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L60:
	movl	20(%rdi), %esi
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L65:
	xorl	%ebx, %ebx
	jmp	.L40
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1322:
	.size	_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode, .-_ZN6icu_6715SimpleFormatter27applyPatternMinMaxArgumentsERKNS_13UnicodeStringEiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringERS1_R10UErrorCode
	.type	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringERS1_R10UErrorCode, @function
_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringERS1_R10UErrorCode:
.LFB1323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rcx), %eax
	movq	%rsi, -64(%rbp)
	testl	%eax, %eax
	jg	.L96
	movzwl	16(%rdi), %eax
	movq	%rcx, %rbx
	testw	%ax, %ax
	js	.L91
	movswl	%ax, %r15d
	sarl	$5, %r15d
.L92:
	testb	$17, %al
	jne	.L93
	leaq	18(%rdi), %rdx
	testb	$2, %al
	jne	.L95
	movq	32(%rdi), %rdx
.L95:
	testl	%r15d, %r15d
	jne	.L116
.L106:
	leaq	18(%rdi), %r13
	testb	$2, %al
	je	.L117
.L98:
	cmpl	$1, %r15d
	jle	.L96
	movl	$1, %eax
.L99:
	leal	1(%rax), %r14d
	cltq
	leaq	(%rax,%rax), %rcx
	movzwl	0(%r13,%rax,2), %eax
	cmpl	$255, %eax
	jg	.L100
	movq	-64(%rbp,%rax,8), %rsi
	testq	%rsi, %rsi
	je	.L101
	cmpq	%rsi, %r12
	je	.L101
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	js	.L102
	sarl	$5, %ecx
.L103:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	%r14d, %eax
.L104:
	cmpl	%eax, %r15d
	jg	.L99
	.p2align 4,,10
	.p2align 3
.L96:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movl	20(%rdi), %r15d
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L116:
	cmpw	$1, (%rdx)
	jbe	.L106
.L101:
	movl	$1, (%rbx)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L100:
	leal	-256(%rax), %r8d
	leaq	2(%r13,%rcx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%r8d, %ecx
	movq	%rsi, -72(%rbp)
	movl	%r8d, -76(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-72(%rbp), %rsi
	movl	-76(%rbp), %r8d
	leal	(%r14,%r8), %eax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L102:
	movl	12(%rsi), %ecx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L117:
	movq	32(%rdi), %r13
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L93:
	testl	%r15d, %r15d
	je	.L96
	xorl	%r13d, %r13d
	cmpw	$1, 0
	jbe	.L98
	jmp	.L101
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1323:
	.size	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringERS1_R10UErrorCode, .-_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringERS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode
	.type	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode, @function
_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode:
.LFB1324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r8), %eax
	movaps	%xmm0, -80(%rbp)
	testl	%eax, %eax
	jg	.L126
	movzwl	16(%rdi), %eax
	movq	%r8, %rbx
	testw	%ax, %ax
	js	.L121
	movswl	%ax, %r15d
	sarl	$5, %r15d
.L122:
	testb	$17, %al
	jne	.L123
	leaq	18(%rdi), %rdx
	testb	$2, %al
	jne	.L125
	movq	32(%rdi), %rdx
.L125:
	testl	%r15d, %r15d
	jne	.L146
.L136:
	leaq	18(%rdi), %r13
	testb	$2, %al
	je	.L147
.L128:
	cmpl	$1, %r15d
	jle	.L126
	movl	$1, %eax
.L129:
	leal	1(%rax), %r14d
	cltq
	leaq	(%rax,%rax), %rcx
	movzwl	0(%r13,%rax,2), %eax
	cmpl	$255, %eax
	jg	.L130
	movq	-80(%rbp,%rax,8), %rsi
	testq	%rsi, %rsi
	je	.L131
	cmpq	%rsi, %r12
	je	.L131
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	js	.L132
	sarl	$5, %ecx
.L133:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	%r14d, %eax
.L134:
	cmpl	%eax, %r15d
	jg	.L129
	.p2align 4,,10
	.p2align 3
.L126:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movl	20(%rdi), %r15d
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L146:
	cmpw	$2, (%rdx)
	jbe	.L136
.L131:
	movl	$1, (%rbx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	leal	-256(%rax), %r8d
	leaq	2(%r13,%rcx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%r8d, %ecx
	movq	%rsi, -88(%rbp)
	movl	%r8d, -92(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-88(%rbp), %rsi
	movl	-92(%rbp), %r8d
	leal	(%r14,%r8), %eax
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L132:
	movl	12(%rsi), %ecx
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L147:
	movq	32(%rdi), %r13
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L123:
	testl	%r15d, %r15d
	je	.L126
	xorl	%r13d, %r13d
	cmpw	$2, 0
	jbe	.L128
	jmp	.L131
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1324:
	.size	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode, .-_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_RS1_R10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_S3_RS1_R10UErrorCode
	.type	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_S3_RS1_R10UErrorCode, @function
_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_S3_RS1_R10UErrorCode:
.LFB1325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%r9), %eax
	movq	%rcx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testl	%eax, %eax
	jg	.L156
	movzwl	16(%rdi), %eax
	movq	%r9, %rbx
	testw	%ax, %ax
	js	.L151
	movswl	%ax, %r15d
	sarl	$5, %r15d
.L152:
	testb	$17, %al
	jne	.L153
	leaq	18(%rdi), %rdx
	testb	$2, %al
	jne	.L155
	movq	32(%rdi), %rdx
.L155:
	testl	%r15d, %r15d
	jne	.L176
.L166:
	leaq	18(%rdi), %r13
	testb	$2, %al
	je	.L177
.L158:
	cmpl	$1, %r15d
	jle	.L156
	movl	$1, %eax
.L159:
	leal	1(%rax), %r14d
	cltq
	leaq	(%rax,%rax), %rcx
	movzwl	0(%r13,%rax,2), %eax
	cmpl	$255, %eax
	jg	.L160
	movq	-80(%rbp,%rax,8), %rsi
	testq	%rsi, %rsi
	je	.L161
	cmpq	%rsi, %r12
	je	.L161
	movswl	8(%rsi), %ecx
	testw	%cx, %cx
	js	.L162
	sarl	$5, %ecx
.L163:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN6icu_6713UnicodeString8doAppendERKS0_ii@PLT
	movl	%r14d, %eax
.L164:
	cmpl	%eax, %r15d
	jg	.L159
	.p2align 4,,10
	.p2align 3
.L156:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movl	20(%rdi), %r15d
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L176:
	cmpw	$3, (%rdx)
	jbe	.L166
.L161:
	movl	$1, (%rbx)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L160:
	leal	-256(%rax), %r8d
	leaq	2(%r13,%rcx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movl	%r8d, %ecx
	movq	%rsi, -88(%rbp)
	movl	%r8d, -92(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-88(%rbp), %rsi
	movl	-92(%rbp), %r8d
	leal	(%r14,%r8), %eax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L162:
	movl	12(%rsi), %ecx
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L177:
	movq	32(%rdi), %r13
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L153:
	testl	%r15d, %r15d
	je	.L156
	xorl	%r13d, %r13d
	cmpw	$3, 0
	jbe	.L158
	jmp	.L161
.L178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1325:
	.size	_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_S3_RS1_R10UErrorCode, .-_ZNK6icu_6715SimpleFormatter6formatERKNS_13UnicodeStringES3_S3_RS1_R10UErrorCode
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode
	.type	_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode, @function
_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode:
.LFB1326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rbp), %rcx
	movl	(%rcx), %r10d
	testl	%r10d, %r10d
	jg	.L195
	testl	%edx, %edx
	js	.L181
	testq	%rsi, %rsi
	sete	%r11b
	testl	%edx, %edx
	setne	%r10b
	testb	%r10b, %r11b
	jne	.L181
	testl	%r9d, %r9d
	jns	.L200
.L181:
	movl	$1, (%rcx)
.L195:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L193
	testl	%r9d, %r9d
	jne	.L181
.L193:
	movzwl	16(%rdi), %r10d
	testw	%r10w, %r10w
	js	.L183
	movswl	%r10w, %r11d
	sarl	$5, %r11d
.L184:
	testb	$17, %r10b
	jne	.L185
	andl	$2, %r10d
	je	.L186
	testl	%r11d, %r11d
	je	.L188
	movzwl	18(%rdi), %r10d
	cmpl	%r10d, %edx
	jl	.L181
.L188:
	addq	$18, %rdi
.L191:
	subq	$8, %rsp
	movq	%rsi, %rdx
	movl	%r11d, %esi
	movq	%rax, -8(%rbp)
	pushq	%rcx
	movq	%rax, %rcx
	pushq	%r9
	movl	$1, %r9d
	pushq	%r8
	xorl	%r8d, %r8d
	call	_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode.part.0
	movq	-8(%rbp), %rax
	addq	$32, %rsp
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	32(%rdi), %rdi
	testl	%r11d, %r11d
	je	.L191
	movzwl	(%rdi), %r10d
	cmpl	%r10d, %edx
	jl	.L181
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L183:
	movl	20(%rdi), %r11d
	jmp	.L184
.L185:
	testl	%r11d, %r11d
	jne	.L198
	xorl	%edi, %edi
	jmp	.L191
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode.cold, @function
_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode.cold:
.LFSB1326:
.L198:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movzwl	0, %eax
	ud2
	.cfi_endproc
.LFE1326:
	.text
	.size	_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode, .-_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode
	.section	.text.unlikely
	.size	_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode.cold, .-_ZNK6icu_6715SimpleFormatter15formatAndAppendEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode.cold
.LCOLDE0:
	.text
.LHOTE0:
	.align 2
	.p2align 4
	.globl	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode
	.type	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode, @function
_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode:
.LFB1327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jg	.L202
	testl	%edx, %edx
	js	.L203
	setne	%cl
	testq	%rsi, %rsi
	movq	%rsi, %r13
	sete	%al
	testb	%al, %cl
	jne	.L203
	testl	%r9d, %r9d
	jns	.L243
.L203:
	movl	$1, (%rbx)
.L202:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	je	.L228
	testq	%r8, %r8
	je	.L203
.L228:
	movswl	16(%rdi), %r15d
	testb	$17, %r15b
	jne	.L245
	leaq	18(%rdi), %r14
	testb	$2, %r15b
	jne	.L205
	movq	32(%rdi), %r14
.L205:
	testw	%r15w, %r15w
	js	.L208
	sarl	$5, %r15d
.L209:
	testl	%r15d, %r15d
	je	.L210
	movzwl	(%r14), %eax
	cmpl	%eax, %edx
	jl	.L203
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movq	%rax, -128(%rbp)
	movl	$2, %eax
	movw	%ax, -120(%rbp)
	cmpw	$0, (%r14)
	je	.L242
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	-128(%rbp), %r10
	cmpl	$1, %r15d
	jg	.L213
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L216:
	cmpl	%r15d, %edx
	jge	.L214
.L213:
	leal	1(%rdx), %edi
	movslq	%edx, %rdx
	movzwl	(%r14,%rdx,2), %esi
	leal	-256(%rdi,%rsi), %edx
	cmpl	$255, %esi
	jg	.L216
	movl	%edi, %edx
	cmpq	%r12, 0(%r13,%rsi,8)
	jne	.L216
	cmpl	$2, %edi
	je	.L225
	movswl	-120(%rbp), %eax
	shrl	$5, %eax
	jne	.L216
	movswl	8(%r12), %eax
	shrl	$5, %eax
	je	.L216
	movl	%edi, -140(%rbp)
	movq	%r12, %rsi
	movq	%r10, %rdi
	movl	%r9d, -156(%rbp)
	movq	%r8, -152(%rbp)
	movl	%ecx, -144(%rbp)
	movq	%r10, -136(%rbp)
	call	_ZN6icu_6713UnicodeStringaSERKS0_@PLT
	movq	-136(%rbp), %r10
	movl	-140(%rbp), %edx
	movl	-144(%rbp), %ecx
	movq	-152(%rbp), %r8
	movl	-156(%rbp), %r9d
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	16+_ZTVN6icu_6713UnicodeStringE(%rip), %rax
	movl	$2, %edx
	movq	%rax, -128(%rbp)
	movw	%dx, -120(%rbp)
.L242:
	leaq	-128(%rbp), %r10
.L217:
	movzwl	8(%r12), %edx
	movl	$2, %eax
	movl	%edx, %ecx
	andl	$31, %ecx
	andl	$1, %edx
	cmove	%ecx, %eax
	movw	%ax, 8(%r12)
.L218:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L220
	subq	$8, %rsp
	movq	%r12, %rcx
	movq	%r13, %rdx
	movl	%r15d, %esi
	pushq	%rbx
	movq	%r14, %rdi
	pushq	%r9
	xorl	%r9d, %r9d
	pushq	%r8
	movq	%r10, %r8
	movq	%r10, -136(%rbp)
	call	_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode.part.0
	movq	-136(%rbp), %r10
	addq	$32, %rsp
.L220:
	movq	%r10, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L208:
	movl	20(%rdi), %r15d
	jmp	.L209
.L245:
	xorl	%r14d, %r14d
	jmp	.L205
.L214:
	cmpl	$-1, %ecx
	jne	.L218
	jmp	.L217
.L225:
	movl	%esi, %ecx
	jmp	.L216
.L244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1327:
	.size	_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode, .-_ZNK6icu_6715SimpleFormatter16formatAndReplaceEPKPKNS_13UnicodeStringEiRS1_PiiR10UErrorCode
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715SimpleFormatter22getTextWithNoArgumentsEPKDsiPii
	.type	_ZN6icu_6715SimpleFormatter22getTextWithNoArgumentsEPKDsiPii, @function
_ZN6icu_6715SimpleFormatter22getTextWithNoArgumentsEPKDsiPii:
.LFB1328:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	testl	%r8d, %r8d
	jle	.L247
	leal	-1(%r8), %eax
	movl	$255, %esi
	movq	%rcx, %rdi
	leaq	4(,%rax,4), %rdx
	call	memset@PLT
.L247:
	leal	-1(%r14), %esi
	testl	%r14d, %r14d
	jne	.L258
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$-1, %esi
	movq	%r15, %rdi
	call	_ZN6icu_6713UnicodeStringC1Eiii@PLT
.L246:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	subl	%eax, %esi
	call	_ZN6icu_6713UnicodeStringC1Eiii@PLT
	cmpl	$1, %r14d
	jle	.L246
	movl	$1, %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L251:
	movl	%r12d, %eax
	cmpl	%edx, %ebx
	jle	.L252
	movq	-72(%rbp), %rax
	leaq	(%rax,%rdx,4), %rdx
	movswl	8(%r15), %eax
	testw	%ax, %ax
	js	.L253
	sarl	$5, %eax
.L254:
	movl	%eax, (%rdx)
	movl	%r12d, %eax
.L252:
	cmpl	%r14d, %eax
	jge	.L246
.L250:
	leal	1(%rax), %r12d
	cltq
	movzwl	0(%r13,%rax,2), %edx
	leaq	(%rax,%rax), %rsi
	cmpl	$256, %edx
	jle	.L251
	leal	-256(%rdx), %ecx
	leaq	2(%r13,%rsi), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN6icu_6713UnicodeString8doAppendEPKDsii@PLT
	movq	-56(%rbp), %rsi
	movl	-60(%rbp), %ecx
	leal	(%r12,%rcx), %eax
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L253:
	movl	12(%r15), %eax
	jmp	.L254
	.cfi_endproc
.LFE1328:
	.size	_ZN6icu_6715SimpleFormatter22getTextWithNoArgumentsEPKDsiPii, .-_ZN6icu_6715SimpleFormatter22getTextWithNoArgumentsEPKDsiPii
	.align 2
	.p2align 4
	.globl	_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode
	.type	_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode, @function
_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode:
.LFB1329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	subq	$8, %rsp
	movq	32(%rbp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L260
	subq	$8, %rsp
	movsbl	%r9b, %r9d
	movq	%r12, %rcx
	pushq	%rax
	movl	24(%rbp), %eax
	pushq	%rax
	pushq	16(%rbp)
	call	_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode.part.0
	addq	$32, %rsp
.L260:
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1329:
	.size	_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode, .-_ZN6icu_6715SimpleFormatter6formatEPKDsiPKPKNS_13UnicodeStringERS3_S5_aPiiR10UErrorCode
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
