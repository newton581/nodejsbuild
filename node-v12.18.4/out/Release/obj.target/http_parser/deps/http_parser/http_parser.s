	.file	"http_parser.c"
	.text
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	parse_url_char.part.0, @function
parse_url_char.part.0:
.LFB31:
	.cfi_startproc
	leal	-20(%rdi), %edx
	movl	%edi, %eax
	cmpl	$11, %edx
	ja	.L15
	leaq	.L4(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L13-.L4
	.long	.L12-.L4
	.long	.L11-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1, %eax
	cmpb	$64, %sil
	je	.L1
.L9:
	cmpb	$47, %sil
	je	.L23
	cmpb	$63, %sil
	je	.L24
	cmpb	$64, %sil
	je	.L25
	movl	%esi, %edx
	movl	$25, %eax
	orl	$32, %edx
	subl	$97, %edx
	cmpb	$25, %dl
	jbe	.L1
	leal	-33(%rsi), %ecx
	cmpb	$62, %cl
	ja	.L42
	movabsq	$6052837899588583417, %rax
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$24, %eax
	addl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%esi, %eax
	leaq	normal_url_char(%rip), %rdx
	shrb	$3, %al
	andl	$31, %eax
	movzbl	(%rdx,%rax), %edx
	movl	%esi, %eax
	andl	$7, %eax
	btl	%eax, %edx
	jc	.L32
	testb	%sil, %sil
	js	.L32
	movl	$30, %eax
	cmpb	$35, %sil
	je	.L1
	cmpb	$63, %sil
	movl	$29, %edx
	movl	$1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%esi, %edx
	leaq	normal_url_char(%rip), %rcx
	shrb	$3, %dl
	andl	$31, %edx
	movzbl	(%rcx,%rdx), %ecx
	movl	%esi, %edx
	andl	$7, %edx
	btl	%edx, %ecx
	jc	.L1
	testb	%sil, %sil
	js	.L1
	cmpb	$35, %sil
	je	.L1
	cmpb	$63, %sil
	movl	$1, %edx
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	cmpb	$47, %sil
	je	.L23
	cmpb	$42, %sil
	je	.L23
	orl	$32, %esi
	subl	$97, %esi
	cmpb	$26, %sil
	sbbl	%eax, %eax
	andl	$20, %eax
	addl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	%esi, %edx
	orl	$32, %edx
	subl	$97, %edx
	cmpb	$25, %dl
	jbe	.L1
	cmpb	$58, %sil
	movl	$22, %edx
	movl	$1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%esi, %edx
	leaq	normal_url_char(%rip), %rcx
	shrb	$3, %dl
	andl	$31, %edx
	movzbl	(%rcx,%rdx), %ecx
	movl	%esi, %edx
	andl	$7, %edx
	btl	%edx, %ecx
	jc	.L1
	testb	%sil, %sil
	js	.L1
	movl	$30, %eax
	cmpb	$35, %sil
	je	.L1
	cmpb	$63, %sil
	movl	$28, %edx
	movl	$1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	cmpb	$47, %sil
	movl	$23, %edi
	movl	$1, %eax
	cmove	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	cmpb	$47, %sil
	movl	$24, %edi
	movl	$1, %eax
	cmove	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	%esi, %edx
	leaq	normal_url_char(%rip), %rcx
	shrb	$3, %dl
	andl	$31, %edx
	movzbl	(%rcx,%rdx), %ecx
	movl	%esi, %edx
	andl	$7, %edx
	btl	%edx, %ecx
	jc	.L35
	testb	%sil, %sil
	js	.L35
	cmpb	$35, %sil
	je	.L1
	cmpb	$63, %sil
	movl	$31, %edx
	movl	$1, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$31, %eax
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	cmpb	$126, %sil
	movl	$1, %edx
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$27, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$29, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$28, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$26, %eax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	parse_url_char.part.0.cold, @function
parse_url_char.part.0.cold:
.LFSB31:
.L15:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE31:
	.text
	.size	parse_url_char.part.0, .-parse_url_char.part.0
	.section	.text.unlikely
	.size	parse_url_char.part.0.cold, .-parse_url_char.part.0.cold
.LCOLDE0:
	.text
.LHOTE0:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"content-length"
.LC2:
	.string	"proxy-connection"
.LC1:
	.string	"connection"
.LC5:
	.string	"upgrade"
.LC4:
	.string	"transfer-encoding"
.LC8:
	.string	"close"
.LC7:
	.string	"keep-alive"
.LC6:
	.string	"chunked"
	.text
	.p2align 4
	.globl	http_parser_execute
	.type	http_parser_execute, @function
http_parser_execute:
.LFB14:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	31(%rdi), %r8d
	movq	%rsi, -72(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	testb	$127, %r8b
	jne	.L793
	movzwl	(%rdi), %edx
	movl	8(%rdi), %r14d
	movq	%rdi, %r13
	shrw	$2, %dx
	andl	$127, %edx
	cmpq	$0, -64(%rbp)
	jne	.L46
	cmpb	$63, %dl
	je	.L47
	ja	.L48
	xorl	%eax, %eax
	cmpb	$4, %dl
	je	.L43
	jbe	.L801
	cmpb	$18, %dl
	jne	.L48
.L43:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movzbl	%dl, %r11d
	cmpl	$45, %r11d
	je	.L51
	cmpl	$50, %r11d
	jne	.L802
	movq	-56(%rbp), %rax
	movq	$0, -104(%rbp)
	movl	$50, %edx
	movq	$0, -96(%rbp)
	movq	%rax, -80(%rbp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L802:
	cmpb	$16, %dl
	jne	.L803
	movq	-56(%rbp), %rax
	movq	$0, -96(%rbp)
	movl	$16, %r11d
	movq	$0, -80(%rbp)
	movq	%rax, -104(%rbp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L803:
	leal	107(%rdx), %eax
	xorl	%esi, %esi
	movq	$0, -80(%rbp)
	andl	$127, %eax
	movq	%rsi, -104(%rbp)
	cmpb	$11, %al
	movq	%rsi, %rax
	cmovb	-56(%rbp), %rax
	movq	%rax, -96(%rbp)
.L52:
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %r15
	addq	%rax, %r15
	cmpq	%rax, %r15
	je	.L804
	movq	$0, -112(%rbp)
	movq	%rax, %r9
.L459:
	movzbl	2(%r13), %eax
	movq	$0, -88(%rbp)
	movq	%r13, %r10
	leaq	.L60(%rip), %r12
	movq	%r9, %r13
	movl	%r8d, %r9d
	shrb	$7, %al
	movb	%al, -113(%rbp)
	movzbl	%al, %eax
	movl	%eax, -120(%rbp)
	movl	%r14d, %eax
	movq	%r15, %r14
	movl	%r11d, %r15d
	movl	%eax, %r11d
	.p2align 4,,10
	.p2align 3
.L439:
	movsbl	0(%r13), %ebx
	cmpl	$58, %r15d
	ja	.L53
	addl	$1, %r11d
	cmpl	%r11d, max_header_size(%rip)
	jb	.L805
.L53:
	leaq	.L365(%rip), %rcx
.L57:
	cmpl	$64, %r15d
	ja	.L58
	movl	%r15d, %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L60:
	.long	.L58-.L60
	.long	.L113-.L60
	.long	.L112-.L60
	.long	.L111-.L60
	.long	.L110-.L60
	.long	.L109-.L60
	.long	.L108-.L60
	.long	.L107-.L60
	.long	.L106-.L60
	.long	.L105-.L60
	.long	.L104-.L60
	.long	.L103-.L60
	.long	.L102-.L60
	.long	.L101-.L60
	.long	.L100-.L60
	.long	.L99-.L60
	.long	.L98-.L60
	.long	.L97-.L60
	.long	.L96-.L60
	.long	.L95-.L60
	.long	.L94-.L60
	.long	.L93-.L60
	.long	.L93-.L60
	.long	.L93-.L60
	.long	.L93-.L60
	.long	.L92-.L60
	.long	.L92-.L60
	.long	.L92-.L60
	.long	.L92-.L60
	.long	.L92-.L60
	.long	.L92-.L60
	.long	.L92-.L60
	.long	.L91-.L60
	.long	.L90-.L60
	.long	.L89-.L60
	.long	.L86-.L60
	.long	.L88-.L60
	.long	.L87-.L60
	.long	.L86-.L60
	.long	.L85-.L60
	.long	.L84-.L60
	.long	.L83-.L60
	.long	.L82-.L60
	.long	.L81-.L60
	.long	.L80-.L60
	.long	.L79-.L60
	.long	.L78-.L60
	.long	.L77-.L60
	.long	.L76-.L60
	.long	.L75-.L60
	.long	.L74-.L60
	.long	.L73-.L60
	.long	.L72-.L60
	.long	.L71-.L60
	.long	.L70-.L60
	.long	.L69-.L60
	.long	.L68-.L60
	.long	.L67-.L60
	.long	.L384-.L60
	.long	.L65-.L60
	.long	.L64-.L60
	.long	.L63-.L60
	.long	.L62-.L60
	.long	.L61-.L60
	.long	.L59-.L60
	.text
	.p2align 4,,10
	.p2align 3
.L47:
	movq	56(%rsi), %rax
	testq	%rax, %rax
	je	.L793
	call	*%rax
	testl	%eax, %eax
	je	.L793
	movzbl	31(%r13), %eax
	movl	%r14d, 8(%r13)
	andl	$-128, %eax
	orl	$7, %eax
	movb	%al, 31(%r13)
.L793:
	addq	$104, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	addl	$127, %edx
	andl	$127, %edx
	cmpb	$1, %dl
	jbe	.L43
.L48:
	movzbl	31(%r13), %eax
	andl	$-128, %eax
	orl	$11, %eax
	movb	%al, 31(%r13)
	addq	$104, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	addq	$1, %r13
	movl	$36, %r15d
	.p2align 4,,10
	.p2align 3
.L119:
	cmpq	%r13, %r14
	jne	.L439
	movl	%r11d, %eax
	movq	%r10, %r13
	movl	%r15d, %r11d
	movq	%r14, %r15
	movl	%eax, %r14d
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L51:
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %r15
	addq	%rax, %r15
	cmpq	%rax, %r15
	je	.L806
	movq	%rax, -112(%rbp)
	movq	%rax, %r9
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -80(%rbp)
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L805:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$12, %eax
	movb	%al, 31(%r10)
	movl	%r15d, %eax
	andl	$127, %eax
.L54:
	movzbl	31(%r13), %edx
	testb	$127, %dl
	jne	.L450
	andl	$-128, %edx
	orl	$33, %edx
	movb	%dl, 31(%r13)
.L450:
	andl	$127, %eax
	movl	%r14d, 8(%r13)
	leal	0(,%rax,4), %edx
	movzwl	0(%r13), %eax
	andw	$-509, %ax
	orl	%edx, %eax
	movw	%ax, 0(%r13)
	movq	%r9, %rax
	subq	-56(%rbp), %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	cmpb	$13, %bl
	je	.L219
	cmpb	$32, %bl
	je	.L220
	cmpb	$10, %bl
	je	.L807
	movl	%r15d, %edi
	movsbl	%bl, %esi
	movq	%r10, -136(%rbp)
	movl	%r11d, -128(%rbp)
	call	parse_url_char.part.0
	movl	-128(%rbp), %r11d
	movq	-136(%rbp), %r10
	cmpl	$1, %eax
	movl	%eax, %r15d
	je	.L231
	addq	$1, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L93:
	cmpb	$32, %bl
	ja	.L217
	movabsq	$4294976512, %rax
	btq	%rbx, %rax
	jc	.L808
.L217:
	movl	%r15d, %edi
	movsbl	%bl, %esi
	movq	%r10, -136(%rbp)
	movl	%r11d, -128(%rbp)
	call	parse_url_char.part.0
	movl	-128(%rbp), %r11d
	movq	-136(%rbp), %r10
	cmpl	$1, %eax
	movl	%eax, %r15d
	je	.L231
	addq	$1, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L101:
	leal	-48(%rbx), %eax
	cmpb	$9, %al
	jbe	.L139
	cmpb	$32, %bl
	jne	.L140
	addq	$1, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L100:
	leal	-48(%rbx), %eax
	cmpb	$9, %al
	jbe	.L141
	cmpb	$13, %bl
	je	.L142
	cmpb	$32, %bl
	je	.L143
	cmpb	$10, %bl
	je	.L809
.L146:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$15, %eax
	movb	%al, 31(%r10)
	movl	$14, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L74:
	movzbl	1(%r10), %edi
	movl	max_header_size(%rip), %eax
	shrb	%dil
	movl	%eax, -128(%rbp)
	movzbl	%dil, %edi
	cmpq	%r13, %r14
	je	.L478
	movzbl	-113(%rbp), %esi
	movq	%rax, -136(%rbp)
	movq	%r13, %r8
	movl	%r15d, %ecx
	xorl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L349:
	movsbl	(%r8), %ebx
	cmpb	$13, %bl
	je	.L810
	cmpb	$10, %bl
	je	.L811
	cmpb	$9, %bl
	je	.L316
	testb	%sil, %sil
	je	.L316
	cmpb	$31, %bl
	jbe	.L518
	cmpb	$127, %bl
	je	.L518
.L316:
	movl	%ebx, %edx
	orl	$32, %edx
	cmpl	$26, %edi
	ja	.L318
	leaq	.L320(%rip), %r15
	movl	%edi, %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L320:
	.long	.L334-.L320
	.long	.L318-.L320
	.long	.L318-.L320
	.long	.L318-.L320
	.long	.L318-.L320
	.long	.L318-.L320
	.long	.L318-.L320
	.long	.L318-.L320
	.long	.L318-.L320
	.long	.L789-.L320
	.long	.L333-.L320
	.long	.L332-.L320
	.long	.L331-.L320
	.long	.L789-.L320
	.long	.L318-.L320
	.long	.L329-.L320
	.long	.L328-.L320
	.long	.L327-.L320
	.long	.L326-.L320
	.long	.L325-.L320
	.long	.L324-.L320
	.long	.L323-.L320
	.long	.L322-.L320
	.long	.L321-.L320
	.long	.L319-.L320
	.long	.L319-.L320
	.long	.L319-.L320
	.text
	.p2align 4,,10
	.p2align 3
.L318:
	addq	$1, %r8
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L335:
	cmpq	%r8, %r14
	jne	.L349
.L781:
	movl	%ecx, %r15d
.L305:
	movzbl	1(%r10), %edx
	leal	(%rdi,%rdi), %eax
	movq	%r14, %r8
	andl	$1, %edx
	orl	%edx, %eax
	movb	%al, 1(%r10)
.L452:
	subq	$1, %r8
.L350:
	movq	%r8, %rax
	subq	%r13, %rax
	addl	%eax, %r11d
	cmpl	%r11d, -128(%rbp)
	jb	.L351
	leaq	1(%r8), %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L319:
	addq	$1, %r8
	cmpb	$44, %bl
	je	.L812
	cmpb	$32, %bl
	movl	$22, %eax
	cmovne	%eax, %edi
	jmp	.L335
.L334:
	movq	-136(%rbp), %rdx
	movq	%r14, %rax
	movl	%ecx, %r15d
	subq	%r8, %rax
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	leaq	(%r8,%rax), %rdx
	cmpq	%rdx, %r8
	jne	.L336
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L340:
	movzbl	1(%r8), %ebx
	cmpb	$13, %bl
	je	.L780
	cmpb	$10, %bl
	je	.L780
	movq	%rax, %r8
.L336:
	cmpb	$9, %bl
	je	.L338
	testb	%sil, %sil
	je	.L338
	cmpb	$31, %bl
	jbe	.L518
	cmpb	$127, %bl
	je	.L518
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	1(%r8), %rax
	cmpq	%rax, %rdx
	jne	.L340
	movl	%r15d, %ecx
	movq	%rdx, %r8
.L337:
	cmpq	%r8, %r14
	je	.L781
.L789:
	addq	$1, %r8
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L73:
	movzbl	1(%r10), %eax
	cmpb	$32, %bl
	je	.L519
	cmpb	$9, %bl
	je	.L519
	shrb	%al
	cmpb	$25, %al
	je	.L356
	ja	.L357
	cmpb	$23, %al
	je	.L358
	cmpb	$24, %al
	jne	.L80
	orw	$2, 4(%r10)
	.p2align 4,,10
	.p2align 3
.L80:
	cmpb	$13, %bl
	je	.L813
	cmpb	$10, %bl
	jne	.L814
.L67:
	movzwl	4(%r10), %eax
	testb	$16, %al
	je	.L373
	movq	-72(%rbp), %rax
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L59
	movl	%r11d, -136(%rbp)
	movzwl	(%r10), %eax
	movq	%r10, %rdi
	andw	$-509, %ax
	orb	$1, %ah
	movw	%ax, (%r10)
	movq	%r10, -128(%rbp)
	call	*%rdx
	movq	-128(%rbp), %r10
	movl	-136(%rbp), %r11d
	leaq	.L365(%rip), %rcx
	testl	%eax, %eax
	jne	.L815
.L374:
	movzwl	(%r10), %r15d
	movzbl	31(%r10), %r9d
	shrw	$2, %r15w
	andl	$127, %r15d
	testb	$127, %r9b
	je	.L57
.L800:
	movq	%r13, %rax
	subq	-56(%rbp), %rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L384:
	movzwl	4(%r10), %eax
	movl	$0, 8(%r10)
	movl	$1, %edx
	movl	%eax, %ecx
	andw	$1, %cx
	je	.L816
.L385:
	testb	%r9b, %r9b
	js	.L817
	testb	$64, %al
	je	.L388
	movzbl	(%r10), %edx
	movq	-72(%rbp), %rdi
	andl	$3, %edx
	cmpb	$1, %dl
	sbbl	%eax, %eax
	andl	$14, %eax
	addl	$4, %eax
	cmpb	$1, %dl
	movq	56(%rdi), %rdx
	sbbl	%r15d, %r15d
	andl	$14, %r15d
	addl	$4, %r15d
	testq	%rdx, %rdx
	je	.L818
	movzwl	(%r10), %ecx
	andl	$127, %eax
	movq	%r10, %rdi
	sall	$2, %eax
	andw	$-509, %cx
	orl	%ecx, %eax
	movw	%ax, (%r10)
	movq	%r10, -128(%rbp)
	call	*%rdx
	movq	-128(%rbp), %r10
	testl	%eax, %eax
	jne	.L819
.L395:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	addq	$1, %r13
	xorl	%r11d, %r11d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L71:
	movzbl	%bl, %ebx
	leaq	unhex(%rip), %rax
	movsbq	(%rax,%rbx), %rax
	cmpb	$-1, %al
	je	.L820
	movq	%rax, 16(%r10)
	addq	$1, %r13
	movl	$54, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L70:
	cmpb	$13, %bl
	je	.L821
	movzbl	%bl, %eax
	leaq	unhex(%rip), %rdx
	movsbq	(%rdx,%rax), %rax
	cmpb	$-1, %al
	je	.L822
	movq	16(%r10), %rdx
	movq	%rdx, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	movabsq	$1152921504606846974, %rcx
	cmpq	%rcx, %rdx
	ja	.L823
	movq	%rax, 16(%r10)
	addq	$1, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L81:
	cmpb	$10, %bl
	jne	.L240
	addq	$1, %r13
	movl	$44, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L65:
	movq	16(%r10), %rax
	movq	%r14, %rdx
	movq	-88(%rbp), %rdi
	subq	%r13, %rdx
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	testq	%rdi, %rdi
	cmove	%r13, %rdi
	subq	%rdx, %rax
	addq	%rdx, %r13
	movq	%rax, 16(%r10)
	testq	%rax, %rax
	movl	$60, %eax
	movq	%rdi, -88(%rbp)
	cmove	%eax, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L69:
	addq	$1, %r13
	movl	$56, %eax
	cmpb	$13, %bl
	cmove	%eax, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L68:
	cmpq	$0, 16(%r10)
	movl	$0, 8(%r10)
	jne	.L508
	orw	$16, 4(%r10)
	movl	$44, %eax
	movl	$44, %r15d
.L427:
	movq	-72(%rbp), %rsi
	movq	64(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L824
	movzwl	(%r10), %ecx
	andl	$63, %eax
	movq	%r10, %rdi
	sall	$2, %eax
	andw	$-509, %cx
	orl	%ecx, %eax
	movw	%ax, (%r10)
	movq	%r10, -128(%rbp)
	call	*%rdx
	movq	-128(%rbp), %r10
	testl	%eax, %eax
	jne	.L825
.L429:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	addq	$1, %r13
	xorl	%r11d, %r11d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L61:
	movl	%r11d, %eax
	movl	%r15d, %r11d
	movq	%r14, %r15
	movq	%r13, %r9
	movl	%eax, %r14d
	movq	-88(%rbp), %rax
	movq	%r10, %r13
	testq	%rax, %rax
	cmovne	%rax, %r9
	movq	%r9, -88(%rbp)
.L415:
	movl	%r11d, %edx
	andl	$127, %edx
	cmpq	$0, -112(%rbp)
	je	.L440
.L460:
	movq	-72(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L440
	andl	$127, %edx
	movq	%r13, %rdi
	leal	0(,%rdx,4), %ecx
	movzwl	0(%r13), %edx
	andw	$-509, %dx
	orl	%ecx, %edx
	movw	%dx, 0(%r13)
	movq	-112(%rbp), %rsi
	movq	%r15, %rdx
	subq	%rsi, %rdx
	call	*%rax
	testl	%eax, %eax
	jne	.L826
.L441:
	movzwl	0(%r13), %edx
	shrw	$2, %dx
	andl	$127, %edx
	testb	$127, 31(%r13)
	jne	.L794
.L440:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L442
	movq	-72(%rbp), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L442
	andl	$127, %edx
	movq	%rdi, %rsi
	leal	0(,%rdx,4), %ecx
	movzwl	0(%r13), %edx
	andw	$-509, %dx
	orl	%ecx, %edx
	movw	%dx, 0(%r13)
	movq	%r15, %rdx
	subq	%rdi, %rdx
	movq	%r13, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L827
.L443:
	movzwl	0(%r13), %edx
	shrw	$2, %dx
	andl	$127, %edx
	testb	$127, 31(%r13)
	jne	.L794
.L442:
	movq	-96(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L444
	movq	-72(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L444
	andl	$127, %edx
	movq	%r13, %rdi
	leal	0(,%rdx,4), %ecx
	movzwl	0(%r13), %edx
	andw	$-509, %dx
	orl	%ecx, %edx
	movw	%dx, 0(%r13)
	movq	%r15, %rdx
	subq	%rsi, %rdx
	call	*%rax
	testl	%eax, %eax
	jne	.L828
.L445:
	movzwl	0(%r13), %edx
	shrw	$2, %dx
	andl	$127, %edx
	testb	$127, 31(%r13)
	jne	.L794
.L444:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L446
	movq	-72(%rbp), %rax
	movq	48(%rax), %rcx
	testq	%rcx, %rcx
	je	.L446
	movzwl	0(%r13), %eax
	andl	$127, %edx
	movq	%rdi, %rsi
	sall	$2, %edx
	andw	$-509, %ax
	orl	%edx, %eax
	movq	%r15, %rdx
	subq	%rdi, %rdx
	movw	%ax, 0(%r13)
	movq	%r13, %rdi
	call	*%rcx
	testl	%eax, %eax
	jne	.L829
.L447:
	movzwl	0(%r13), %edx
	shrw	$2, %dx
	andl	$127, %edx
	testb	$127, 31(%r13)
	jne	.L794
.L446:
	movq	-104(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L448
	movq	-72(%rbp), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L448
	movzwl	0(%r13), %eax
	andl	$127, %edx
	movq	%r13, %rdi
	sall	$2, %edx
	andw	$-509, %ax
	orl	%edx, %eax
	movq	%r15, %rdx
	subq	%rsi, %rdx
	movw	%ax, 0(%r13)
	call	*%rcx
	testl	%eax, %eax
	jne	.L830
.L449:
	movzwl	0(%r13), %edx
	shrw	$2, %dx
	andl	$127, %edx
	testb	$127, 31(%r13)
	jne	.L794
.L448:
	movzwl	0(%r13), %eax
	andl	$127, %edx
	movl	%r14d, 8(%r13)
	sall	$2, %edx
	andw	$-509, %ax
	orl	%edx, %eax
	movw	%ax, 0(%r13)
	movq	-64(%rbp), %rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L63:
	movq	-72(%rbp), %rax
	movl	$0, 8(%r10)
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L831
	movzwl	(%r10), %eax
	movq	%r10, %rdi
	andw	$-509, %ax
	orb	$-44, %al
	movw	%ax, (%r10)
	movq	%r10, -128(%rbp)
	call	*%rdx
	movq	-128(%rbp), %r10
	testl	%eax, %eax
	jne	.L832
.L437:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	addq	$1, %r13
	xorl	%r11d, %r11d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L62:
	movq	16(%r10), %rax
	movq	%r14, %rdx
	movq	-88(%rbp), %rsi
	subq	%r13, %rdx
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	testq	%rsi, %rsi
	cmove	%r13, %rsi
	subq	%rdx, %rax
	leaq	-1(%r13,%rdx), %r8
	movq	%rsi, -88(%rbp)
	movq	%rax, 16(%r10)
	jne	.L769
	movq	-72(%rbp), %rax
	movq	48(%rax), %r13
	testq	%r13, %r13
	je	.L505
	movl	%r11d, -136(%rbp)
	movzwl	(%r10), %eax
	movq	%r8, %rdx
	movq	%r10, %rdi
	subq	%rsi, %rdx
	andw	$-509, %ax
	addq	$1, %rdx
	orb	$1, %ah
	movw	%ax, (%r10)
	movq	%r8, -128(%rbp)
	movq	%r10, -88(%rbp)
	call	*%r13
	movq	-88(%rbp), %r10
	movq	-128(%rbp), %r8
	leaq	.L365(%rip), %rcx
	testl	%eax, %eax
	movl	-136(%rbp), %r11d
	jne	.L833
.L413:
	movzwl	(%r10), %r15d
	movzbl	31(%r10), %r9d
	shrw	$2, %r15w
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L799
	movq	$0, -88(%rbp)
	movq	%r8, %r13
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L64:
	cmpq	$0, -88(%rbp)
	je	.L834
	movq	-72(%rbp), %rax
	movq	48(%rax), %rcx
	testq	%rcx, %rcx
	je	.L835
	movl	%r11d, -128(%rbp)
	movzwl	(%r10), %eax
	movq	%r13, %rdx
	movq	%r10, %rdi
	andw	$-509, %ax
	orb	$-12, %al
	movw	%ax, (%r10)
	movq	-88(%rbp), %rsi
	movq	%r10, -88(%rbp)
	subq	%rsi, %rdx
	call	*%rcx
	movq	-88(%rbp), %r10
	movl	-128(%rbp), %r11d
	testl	%eax, %eax
	jne	.L836
.L434:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	movq	$0, -88(%rbp)
	addq	$1, %r13
	jmp	.L119
.L505:
	movq	$0, -88(%rbp)
	movq	%r8, %r13
	.p2align 4,,10
	.p2align 3
.L59:
	movzbl	(%r10), %edx
	movq	-72(%rbp), %rdi
	andl	$3, %edx
	cmpb	$1, %dl
	sbbl	%eax, %eax
	andl	$14, %eax
	addl	$4, %eax
	cmpb	$1, %dl
	movq	56(%rdi), %rdx
	sbbl	%r15d, %r15d
	andl	$14, %r15d
	addl	$4, %r15d
	testq	%rdx, %rdx
	je	.L419
	movl	%r11d, -136(%rbp)
	movzwl	(%r10), %ecx
	movzbl	%al, %eax
	movq	%r10, %rdi
	sall	$2, %eax
	andw	$-509, %cx
	orl	%ecx, %eax
	movw	%ax, (%r10)
	movq	%r10, -128(%rbp)
	call	*%rdx
	movq	-128(%rbp), %r10
	movl	-136(%rbp), %r11d
	testl	%eax, %eax
	jne	.L837
.L418:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
.L419:
	testb	%r9b, %r9b
	js	.L420
	addq	$1, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L83:
	leal	-48(%rbx), %eax
	cmpb	$9, %al
	ja	.L838
	subl	$48, %ebx
	addq	$1, %r13
	movl	$42, %r15d
	movw	%bx, 26(%r10)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L82:
	cmpb	$13, %bl
	je	.L839
	cmpb	$10, %bl
	jne	.L239
	addq	$1, %r13
	movl	$44, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L85:
	leal	-48(%rbx), %eax
	cmpb	$9, %al
	ja	.L840
	subl	$48, %ebx
	addq	$1, %r13
	movl	$40, %r15d
	movw	%bx, 24(%r10)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L84:
	cmpb	$46, %bl
	jne	.L236
	addq	$1, %r13
	movl	$41, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L110:
	cmpb	$13, %bl
	je	.L513
	cmpb	$10, %bl
	je	.L513
	xorl	%esi, %esi
	movq	$-1, 16(%r10)
	movw	%si, 4(%r10)
	cmpb	$72, %bl
	jne	.L131
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L841
	movl	%r11d, -136(%rbp)
	movzwl	(%r10), %eax
	movq	%r10, %rdi
	andw	$-509, %ax
	orl	$20, %eax
	movw	%ax, (%r10)
	movq	%r10, -128(%rbp)
	call	*%rdx
	movq	-128(%rbp), %r10
	movl	-136(%rbp), %r11d
	testl	%eax, %eax
	jne	.L842
.L133:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	addq	$1, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L77:
	addq	$1, %r13
	movl	$48, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L76:
	cmpb	$32, %bl
	je	.L520
	cmpb	$9, %bl
	je	.L520
	movzbl	1(%r10), %eax
	shrb	%al
	addl	$118, %eax
	andl	$127, %eax
	cmpb	$16, %al
	ja	.L363
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L365:
	.long	.L369-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L363-.L365
	.long	.L368-.L365
	.long	.L367-.L365
	.long	.L366-.L365
	.long	.L364-.L365
	.text
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$1, %r13
	movl	$39, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L87:
	addq	$1, %r13
	movl	$38, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L105:
	leal	-48(%rbx), %eax
	cmpb	$9, %al
	ja	.L843
	subl	$48, %ebx
	addq	$1, %r13
	movl	$10, %r15d
	movw	%bx, 24(%r10)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L104:
	cmpb	$46, %bl
	jne	.L136
	addq	$1, %r13
	movl	$11, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L103:
	leal	-48(%rbx), %eax
	cmpb	$9, %al
	ja	.L844
	subl	$48, %ebx
	addq	$1, %r13
	movl	$12, %r15d
	movw	%bx, 26(%r10)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L89:
	addq	$1, %r13
	movl	$35, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L106:
	addq	$1, %r13
	movl	$9, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L97:
	addq	$1, %r13
	movl	$44, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L95:
	testb	%bl, %bl
	je	.L775
	movzbl	30(%r10), %edi
	movzbl	2(%r10), %ecx
	leaq	method_strings(%rip), %rsi
	movq	(%rsi,%rdi,8), %rsi
	movl	%ecx, %edx
	andl	$127, %ecx
	movq	%rdi, %rax
	andl	$127, %edx
	movzbl	(%rsi,%rcx), %ecx
	cmpb	$32, %bl
	je	.L845
	cmpb	%cl, %bl
	je	.L182
	leal	-65(%rbx), %ecx
	cmpb	$25, %cl
	jbe	.L515
	cmpb	$45, %bl
	jne	.L775
.L515:
	movzbl	%dl, %ecx
	sall	$16, %eax
	sall	$8, %ecx
	orl	%ecx, %eax
	orl	%eax, %ebx
	cmpl	$655695, %ebx
	je	.L186
	jg	.L187
	cmpl	$328008, %ebx
	je	.L188
	jle	.L846
	cmpl	$655661, %ebx
	je	.L196
	jle	.L847
	cmpl	$655685, %ebx
	jne	.L775
	movb	$23, 30(%r10)
	.p2align 4,,10
	.p2align 3
.L182:
	addl	$1, %edx
	addq	$1, %r13
	movl	%edx, %eax
	movzbl	2(%r10), %edx
	andl	$127, %eax
	andl	$-128, %edx
	orl	%eax, %edx
	movb	%dl, 2(%r10)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L94:
	cmpb	$32, %bl
	je	.L848
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	cmove	%r13, %rax
	cmpb	$5, 30(%r10)
	movq	%rax, -96(%rbp)
	movl	$24, %eax
	cmove	%eax, %r15d
	cmpb	$13, %bl
	je	.L231
	cmpb	$10, %bl
	je	.L231
	movl	%r15d, %edi
	movsbl	%bl, %esi
	movq	%r10, -136(%rbp)
	movl	%r11d, -128(%rbp)
	call	parse_url_char.part.0
	movl	-128(%rbp), %r11d
	movq	-136(%rbp), %r10
	cmpl	$1, %eax
	movl	%eax, %r15d
	je	.L231
	addq	$1, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L108:
	addq	$1, %r13
	movl	$7, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L102:
	cmpb	$32, %bl
	jne	.L138
	addq	$1, %r13
	movl	$13, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L109:
	addq	$1, %r13
	movl	$6, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L107:
	addq	$1, %r13
	movl	$8, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L79:
	movl	max_header_size(%rip), %eax
	movq	%r13, %rbx
	leaq	tokens(%rip), %r8
	movl	%eax, -128(%rbp)
	movq	%rax, -136(%rbp)
	cmpq	%r13, %r14
	je	.L849
	.p2align 4,,10
	.p2align 3
.L279:
	movzbl	(%rbx), %eax
	movzbl	(%r8,%rax), %esi
	movq	%rax, %rdi
	testb	%sil, %sil
	je	.L250
	movzbl	1(%r10), %ecx
	leaq	1(%rbx), %rax
	movl	%ecx, %edx
	shrb	%dl
	cmpb	$29, %cl
	ja	.L468
	leaq	.L253(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L253:
	.long	.L262-.L253
	.long	.L261-.L253
	.long	.L260-.L253
	.long	.L259-.L253
	.long	.L258-.L253
	.long	.L257-.L253
	.long	.L256-.L253
	.long	.L255-.L253
	.long	.L254-.L253
	.long	.L252-.L253
	.long	.L252-.L253
	.long	.L468-.L253
	.long	.L468-.L253
	.long	.L252-.L253
	.long	.L252-.L253
	.text
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%rax, %rbx
	cmpb	$32, %dil
	je	.L251
	andb	$1, 1(%r10)
	.p2align 4,,10
	.p2align 3
.L251:
	cmpq	%rax, %r14
	jne	.L279
.L116:
	leaq	-1(%rbx), %rax
	movq	%rax, %rdx
	subq	%r13, %rdx
	addl	%edx, %r11d
	cmpl	%r11d, -128(%rbp)
	jb	.L850
	movq	%rbx, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L78:
	cmpb	$32, %bl
	je	.L516
	cmpb	$9, %bl
	je	.L516
	cmpb	$13, %bl
	je	.L851
	cmpb	$10, %bl
	je	.L766
	movzbl	1(%r10), %eax
.L290:
	shrb	%al
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L91:
	cmpb	$72, %bl
	je	.L232
	cmpb	$73, %bl
	je	.L233
	cmpb	$32, %bl
	je	.L852
.L234:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$29, %eax
	movb	%al, 31(%r10)
	movl	$32, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L90:
	addq	$1, %r13
	movl	$34, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L112:
	cmpb	$13, %bl
	je	.L512
	cmpb	$10, %bl
	je	.L512
	xorl	%edi, %edi
	movq	$-1, 16(%r10)
	movw	%di, 4(%r10)
	cmpb	$72, %bl
	je	.L853
	andb	$-4, (%r10)
.L126:
	movl	%ebx, %eax
	xorl	%ecx, %ecx
	movq	$-1, 16(%r10)
	orl	$32, %eax
	movw	%cx, 4(%r10)
	subl	$97, %eax
	cmpb	$25, %al
	ja	.L160
	movzbl	2(%r10), %eax
	subl	$65, %ebx
	movb	$0, 30(%r10)
	andl	$-128, %eax
	orl	$1, %eax
	movb	%al, 2(%r10)
	cmpb	$20, %bl
	ja	.L160
	leaq	.L162(%rip), %rdx
	movzbl	%bl, %ebx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L162:
	.long	.L176-.L162
	.long	.L175-.L162
	.long	.L174-.L162
	.long	.L173-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L172-.L162
	.long	.L171-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L160-.L162
	.long	.L170-.L162
	.long	.L169-.L162
	.long	.L168-.L162
	.long	.L167-.L162
	.long	.L166-.L162
	.long	.L160-.L162
	.long	.L165-.L162
	.long	.L164-.L162
	.long	.L163-.L162
	.long	.L161-.L162
	.text
	.p2align 4,,10
	.p2align 3
.L111:
	cmpb	$84, %bl
	je	.L854
	cmpb	$69, %bl
	jne	.L855
	movl	(%r10), %eax
	movb	$2, 30(%r10)
	addq	$1, %r13
	movl	$19, %r15d
	andl	$-8323076, %eax
	orl	$131072, %eax
	movl	%eax, (%r10)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L113:
	cmpb	$13, %bl
	je	.L511
	cmpb	$10, %bl
	jne	.L117
.L511:
	addq	$1, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L72:
	cmpb	$10, %bl
	jne	.L352
	addq	$1, %r13
	movl	$51, %r15d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L99:
	cmpq	$0, -104(%rbp)
	je	.L456
.L147:
	andb	$-128, 2(%r10)
	cmpb	$13, %bl
	jne	.L856
.L55:
	movq	-72(%rbp), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L857
	movl	%r11d, -128(%rbp)
	movzwl	(%r10), %eax
	movq	%r13, %rdx
	movq	%r10, %rdi
	andw	$-509, %ax
	orl	$68, %eax
	movw	%ax, (%r10)
	movq	-104(%rbp), %rsi
	movq	%r10, -104(%rbp)
	subq	%rsi, %rdx
	call	*%rcx
	movq	-104(%rbp), %r10
	movl	-128(%rbp), %r11d
	testl	%eax, %eax
	jne	.L858
.L151:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	movq	$0, -104(%rbp)
	addq	$1, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L96:
	cmpb	$13, %bl
	je	.L514
	cmpb	$10, %bl
	jne	.L126
.L514:
	addq	$1, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L75:
	movzbl	1(%r10), %eax
	shrb	%al
.L114:
	movq	-80(%rbp), %rdi
	movl	%ebx, %edx
	testq	%rdi, %rdi
	cmove	%r13, %rdi
	addl	$119, %eax
	andb	$-128, 2(%r10)
	orl	$32, %edx
	andl	$127, %eax
	movq	%rdi, -80(%rbp)
	cmpb	$9, %al
	ja	.L292
	leaq	.L294(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L294:
	.long	.L298-.L294
	.long	.L297-.L294
	.long	.L292-.L294
	.long	.L293-.L294
	.long	.L296-.L294
	.long	.L295-.L294
	.long	.L293-.L294
	.long	.L292-.L294
	.long	.L292-.L294
	.long	.L293-.L294
	.text
.L98:
	cmpb	$13, %bl
	jne	.L149
	cmpq	$0, -104(%rbp)
	jne	.L55
	addq	$1, %r13
	movl	$17, %r15d
	jmp	.L119
.L260:
	movzbl	2(%r10), %edx
	movq	%rax, %rbx
	leal	1(%rdx), %ecx
	andl	$-128, %edx
	andl	$127, %ecx
	orl	%ecx, %edx
	cmpb	$110, %sil
	movzbl	1(%r10), %ecx
	movb	%dl, 2(%r10)
	sete	%dl
	leal	(%rdx,%rdx,2), %edx
	andl	$1, %ecx
	addl	%edx, %edx
	orl	%ecx, %edx
	movb	%dl, 1(%r10)
	jmp	.L251
.L256:
	movzbl	2(%r10), %edx
	leal	1(%rdx), %ecx
	andl	$-128, %edx
	andl	$127, %ecx
	orl	%ecx, %edx
	movb	%dl, 2(%r10)
	cmpb	$14, %cl
	ja	.L277
	andl	$127, %ecx
	leaq	.LC3(%rip), %rdi
	cmpb	%sil, (%rdi,%rcx)
	je	.L274
	.p2align 4,,10
	.p2align 3
.L277:
	andb	$1, 1(%r10)
	movq	%rax, %rbx
	jmp	.L251
.L257:
	movzbl	2(%r10), %edx
	leal	1(%rdx), %ecx
	andl	$-128, %edx
	andl	$127, %ecx
	orl	%ecx, %edx
	movb	%dl, 2(%r10)
	cmpb	$16, %cl
	ja	.L277
	andl	$127, %ecx
	leaq	.LC2(%rip), %rdi
	cmpb	%sil, (%rdi,%rcx)
	jne	.L277
	andl	$127, %edx
	movq	%rax, %rbx
	cmpb	$15, %dl
	jne	.L251
.L786:
	movzbl	1(%r10), %edx
	andl	$1, %edx
	orl	$18, %edx
	movb	%dl, 1(%r10)
	jmp	.L251
.L258:
	movzbl	2(%r10), %edx
	leal	1(%rdx), %ecx
	andl	$-128, %edx
	andl	$127, %ecx
	orl	%ecx, %edx
	movb	%dl, 2(%r10)
	cmpb	$10, %cl
	ja	.L277
	andl	$127, %ecx
	leaq	.LC1(%rip), %rdi
	cmpb	%sil, (%rdi,%rcx)
	jne	.L277
	andl	$127, %edx
	movq	%rax, %rbx
	cmpb	$9, %dl
	jne	.L251
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L259:
	movzbl	2(%r10), %edx
	leal	1(%rdx), %ecx
	andl	$-128, %edx
	andl	$127, %ecx
	orl	%ecx, %edx
	movb	%dl, 2(%r10)
	cmpb	$110, %sil
	je	.L266
	cmpb	$116, %sil
	jne	.L277
	movzbl	1(%r10), %edx
	movq	%rax, %rbx
	andl	$1, %edx
	orl	$12, %edx
	movb	%dl, 1(%r10)
	jmp	.L251
.L254:
	movzbl	2(%r10), %edx
	leal	1(%rdx), %ecx
	andl	$-128, %edx
	andl	$127, %ecx
	orl	%ecx, %edx
	movb	%dl, 2(%r10)
	cmpb	$7, %cl
	ja	.L277
	andl	$127, %ecx
	leaq	.LC5(%rip), %rdi
	cmpb	%sil, (%rdi,%rcx)
	jne	.L277
	andl	$127, %edx
	movq	%rax, %rbx
	cmpb	$6, %dl
	jne	.L251
	movzbl	1(%r10), %edx
	andl	$1, %edx
	orl	$28, %edx
	movb	%dl, 1(%r10)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L262:
	movq	-136(%rbp), %rdi
	movq	%r14, %rdx
	subq	%rbx, %rdx
	cmpq	%rdi, %rdx
	cmova	%rdi, %rdx
	leaq	(%rbx,%rdx), %rcx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L859:
	movzbl	(%rax), %edx
	cmpb	$0, (%r8,%rdx)
	je	.L251
	addq	$1, %rax
.L263:
	movq	%rax, %rbx
	cmpq	%rcx, %rax
	jb	.L859
	jmp	.L251
.L261:
	movzbl	2(%r10), %edx
	movq	%rax, %rbx
	leal	1(%rdx), %ecx
	andl	$-128, %edx
	andl	$127, %ecx
	orl	%ecx, %edx
	cmpb	$111, %sil
	movb	%dl, 2(%r10)
	sete	%dl
	leal	0(,%rdx,4), %ecx
	movzbl	1(%r10), %edx
	andl	$1, %edx
	orl	%ecx, %edx
	movb	%dl, 1(%r10)
	jmp	.L251
.L255:
	movzbl	2(%r10), %edx
	leal	1(%rdx), %ecx
	andl	$-128, %edx
	andl	$127, %ecx
	orl	%ecx, %edx
	movb	%dl, 2(%r10)
	cmpb	$17, %cl
	ja	.L277
	andl	$127, %ecx
	leaq	.LC4(%rip), %rdi
	cmpb	%sil, (%rdi,%rcx)
	jne	.L277
	andl	$127, %edx
	movq	%rax, %rbx
	cmpb	$16, %dl
	jne	.L251
	movzbl	1(%r10), %edx
	orw	$256, 4(%r10)
	andl	$1, %edx
	orl	$26, %edx
	movb	%dl, 1(%r10)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%rax, %rbx
	jmp	.L251
.L321:
	addq	$1, %r8
	movl	$17, %eax
	cmpb	$32, %bl
	cmovne	%eax, %edi
	jmp	.L335
.L322:
	addq	$1, %r8
	cmpb	$44, %bl
	jne	.L335
.L347:
	andb	$-128, 2(%r10)
	movl	$18, %edi
	jmp	.L335
.L323:
	movzbl	2(%r10), %eax
	addq	$1, %r8
	leal	1(%rax), %ebx
	andl	$-128, %eax
	andl	$127, %ebx
	orl	%ebx, %eax
	movb	%al, 2(%r10)
	cmpb	$7, %bl
	ja	.L495
	andl	$127, %ebx
	leaq	.LC5(%rip), %r15
	cmpb	%dl, (%r15,%rbx)
	je	.L860
	.p2align 4,,10
	.p2align 3
.L495:
	movl	$22, %edi
	jmp	.L335
.L324:
	movzbl	2(%r10), %eax
	addq	$1, %r8
	leal	1(%rax), %ebx
	andl	$-128, %eax
	andl	$127, %ebx
	orl	%ebx, %eax
	movb	%al, 2(%r10)
	cmpb	$5, %bl
	ja	.L495
	andl	$127, %ebx
	leaq	.LC8(%rip), %r15
	cmpb	%dl, (%r15,%rbx)
	jne	.L495
	andl	$127, %eax
	cmpb	$4, %al
	movl	$25, %eax
	cmove	%eax, %edi
	jmp	.L335
.L325:
	movzbl	2(%r10), %eax
	addq	$1, %r8
	leal	1(%rax), %ebx
	andl	$-128, %eax
	andl	$127, %ebx
	orl	%ebx, %eax
	movb	%al, 2(%r10)
	cmpb	$10, %bl
	ja	.L495
	andl	$127, %ebx
	leaq	.LC7(%rip), %r15
	cmpb	%dl, (%r15,%rbx)
	jne	.L495
	andl	$127, %eax
	cmpb	$9, %al
	movl	$24, %eax
	cmove	%eax, %edi
	jmp	.L335
.L326:
	addq	$1, %r8
	cmpb	$107, %dl
	je	.L486
	cmpb	$99, %dl
	je	.L487
	cmpb	$117, %dl
	je	.L488
	cmpb	$32, %dl
	je	.L335
	movzbl	%dl, %edx
	leaq	tokens(%rip), %rax
	cmpb	$1, (%rax,%rdx)
	sbbl	%edi, %edi
	notl	%edi
	andl	$22, %edi
	jmp	.L335
.L327:
	addq	$1, %r8
	cmpb	$44, %bl
	jne	.L335
	andb	$-128, 2(%r10)
	movl	$15, %edi
	jmp	.L335
.L329:
	addq	$1, %r8
	cmpb	$99, %dl
	je	.L482
	cmpb	$32, %dl
	je	.L335
	movzbl	%dl, %edx
	leaq	tokens(%rip), %rax
	cmpb	$1, (%rax,%rdx)
	sbbl	%edi, %edi
	notl	%edi
	andl	$17, %edi
	jmp	.L335
.L331:
	cmpb	$32, %bl
	je	.L789
	movzbl	31(%r10), %eax
	movl	%r11d, %r14d
	movq	%r10, %r13
	movq	%r8, %r9
	andl	$-128, %eax
	orl	$25, %eax
	movb	%al, 31(%r10)
	movzbl	1(%r10), %eax
	andl	$1, %eax
	orl	$24, %eax
	movb	%al, 1(%r10)
	movl	$50, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L332:
	cmpb	$32, %bl
	jne	.L341
	addq	$1, %r8
	movl	$12, %edi
	jmp	.L335
.L333:
	cmpb	$32, %bl
	je	.L789
.L341:
	leal	-48(%rbx), %eax
	cmpb	$9, %al
	ja	.L792
	movq	16(%r10), %rax
	movsbl	%bl, %edi
	subl	$48, %edi
	movslq	%edi, %rdi
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rdi,%rdx,2), %rdx
	movabsq	$1844674407370955160, %rdi
	cmpq	%rdi, %rax
	ja	.L792
	movq	%rdx, 16(%r10)
	addq	$1, %r8
	movl	$11, %edi
	jmp	.L335
.L328:
	movzbl	2(%r10), %eax
	addq	$1, %r8
	leal	1(%rax), %ebx
	andl	$-128, %eax
	andl	$127, %ebx
	orl	%ebx, %eax
	movb	%al, 2(%r10)
	cmpb	$7, %bl
	ja	.L485
	andl	$127, %ebx
	leaq	.LC6(%rip), %r15
	cmpb	%dl, (%r15,%rbx)
	je	.L861
.L485:
	movl	$17, %edi
	jmp	.L335
.L367:
	orw	$2, 4(%r10)
	.p2align 4,,10
	.p2align 3
.L363:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rsi
	movq	32(%rax), %r8
	testq	%rsi, %rsi
	cmove	%r13, %rsi
	testq	%r8, %r8
	je	.L767
	movl	%r11d, -128(%rbp)
	movzwl	(%r10), %eax
	movq	%r13, %rdx
	movq	%r10, %rdi
	subq	%rsi, %rdx
	andw	$-509, %ax
	orb	$-80, %al
	movw	%ax, (%r10)
	movq	%r10, -80(%rbp)
	call	*%r8
	movq	-80(%rbp), %r10
	movl	-128(%rbp), %r11d
	leaq	.L365(%rip), %rcx
	testl	%eax, %eax
	jne	.L862
.L372:
	movzwl	(%r10), %r15d
	movzbl	31(%r10), %r9d
	shrw	$2, %r15w
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L800
	movq	$0, -80(%rbp)
	jmp	.L57
.L519:
	movl	%eax, %edx
	andl	$-2, %edx
	cmpb	$22, %dl
	jne	.L290
	andl	$1, %eax
	orl	$24, %eax
	cmpq	$0, -80(%rbp)
	movb	%al, 1(%r10)
	je	.L355
	andb	$-128, 2(%r10)
.L293:
	addq	$1, %r13
	movl	$50, %r15d
	jmp	.L119
.L292:
	andb	$1, 1(%r10)
	addq	$1, %r13
	movl	$50, %r15d
	jmp	.L119
.L297:
	leal	-48(%rbx), %eax
	cmpb	$9, %al
	ja	.L863
	movzwl	4(%r10), %eax
	testb	$-128, %al
	jne	.L864
	orb	$-128, %al
	addq	$1, %r13
	movl	$50, %r15d
	movw	%ax, 4(%r10)
	movsbl	%bl, %eax
	subl	$48, %eax
	cltq
	movq	%rax, 16(%r10)
	movzbl	1(%r10), %eax
	andl	$1, %eax
	orl	$22, %eax
	movb	%al, 1(%r10)
	jmp	.L119
.L298:
	movzbl	1(%r10), %eax
	addq	$1, %r13
	andl	$1, %eax
	cmpb	$107, %dl
	je	.L865
	cmpb	$99, %dl
	je	.L866
	cmpb	$117, %dl
	je	.L867
	orl	$44, %eax
	movl	$50, %r15d
	movb	%al, 1(%r10)
	jmp	.L119
.L295:
	orw	$32, 4(%r10)
	addq	$1, %r13
	movl	$50, %r15d
	andb	$1, 1(%r10)
	jmp	.L119
.L296:
	movzbl	1(%r10), %eax
	addq	$1, %r13
	andl	$1, %eax
	cmpb	$99, %dl
	je	.L868
	orl	$34, %eax
	movl	$50, %r15d
	movb	%al, 1(%r10)
	jmp	.L119
.L160:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$16, %eax
	movb	%al, 31(%r10)
	movl	$18, %eax
	jmp	.L54
.L368:
	orw	$1, 4(%r10)
	jmp	.L363
.L369:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$25, %eax
	movb	%al, 31(%r10)
	movl	$48, %eax
	jmp	.L54
.L364:
	orw	$8, 4(%r10)
	jmp	.L363
.L366:
	orw	$4, 4(%r10)
	jmp	.L363
.L176:
	movb	$19, 30(%r10)
.L173:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L869
	movl	%r11d, -136(%rbp)
	movzwl	(%r10), %eax
	movq	%r10, %rdi
	andw	$-509, %ax
	orl	$76, %eax
	movw	%ax, (%r10)
	movq	%r10, -128(%rbp)
	call	*%rdx
	movq	-128(%rbp), %r10
	movl	-136(%rbp), %r11d
	testl	%eax, %eax
	jne	.L870
.L178:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	addq	$1, %r13
	jmp	.L119
.L172:
	movb	$1, 30(%r10)
	jmp	.L173
.L174:
	movb	$5, 30(%r10)
	jmp	.L173
.L175:
	movb	$16, 30(%r10)
	jmp	.L173
.L164:
	movb	$26, 30(%r10)
	jmp	.L173
.L165:
	movb	$20, 30(%r10)
	jmp	.L173
.L166:
	movb	$3, 30(%r10)
	jmp	.L173
.L167:
	movb	$6, 30(%r10)
	jmp	.L173
.L168:
	movb	$25, 30(%r10)
	jmp	.L173
.L169:
	movb	$10, 30(%r10)
	jmp	.L173
.L170:
	movb	$9, 30(%r10)
	jmp	.L173
.L171:
	movb	$2, 30(%r10)
	jmp	.L173
.L161:
	movb	$15, 30(%r10)
	jmp	.L173
.L163:
	movb	$7, 30(%r10)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L58:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$30, %eax
	movb	%al, 31(%r10)
	movl	%r15d, %eax
	andl	$127, %eax
	jmp	.L54
.L250:
	cmpq	%rbx, %r14
	je	.L116
	movq	%rbx, %rax
	subq	%r13, %rax
	addl	%eax, %r11d
	cmpl	%r11d, -128(%rbp)
	jb	.L871
	cmpb	$58, %dil
	jne	.L282
	cmpq	$0, -112(%rbp)
	leaq	1(%rbx), %r13
	movl	$46, %r15d
	je	.L119
	movq	-72(%rbp), %rax
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.L872
	movl	%r11d, -128(%rbp)
	movzwl	(%r10), %eax
	movq	%rbx, %rdx
	movq	%r10, %rdi
	andw	$-509, %ax
	orb	$-72, %al
	movw	%ax, (%r10)
	movq	-112(%rbp), %rsi
	movq	%r10, -112(%rbp)
	subq	%rsi, %rdx
	call	*%rcx
	movq	-112(%rbp), %r10
	movl	-128(%rbp), %r11d
	testl	%eax, %eax
	jne	.L873
.L285:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L286
	movq	$0, -112(%rbp)
	leaq	1(%rbx), %r13
	jmp	.L119
.L438:
	movq	%r13, %r9
.L796:
	movq	%r9, %rax
	subq	-56(%rbp), %rax
	addq	$1, %rax
	jmp	.L43
.L516:
	addq	$1, %r13
	jmp	.L119
.L513:
	addq	$1, %r13
	jmp	.L119
.L357:
	cmpb	$26, %al
	jne	.L80
	orw	$8, 4(%r10)
	jmp	.L80
.L373:
	movl	%eax, %edx
	andw	$384, %dx
	cmpw	$384, %dx
	je	.L874
.L375:
	andl	$40, %eax
	cmpw	$40, %ax
	je	.L875
	cmpb	$5, 30(%r10)
	sete	%al
.L790:
	sall	$7, %eax
	movl	%eax, %edx
	movzbl	31(%r10), %eax
	andl	$127, %eax
	orl	%edx, %eax
	movb	%al, 31(%r10)
	movq	-72(%rbp), %rax
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L380
	movl	%r11d, -136(%rbp)
	movq	%r10, %rdi
	movq	%r10, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %r10
	movl	-136(%rbp), %r11d
	cmpl	$1, %eax
	je	.L381
	cmpl	$2, %eax
	je	.L382
	testl	%eax, %eax
	je	.L380
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	movq	%r13, %r9
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$5, %eax
.L798:
	movb	%al, 31(%r13)
.L797:
	movzwl	0(%r13), %eax
	andw	$-509, %ax
	orb	$-24, %al
	movw	%ax, 0(%r13)
	movq	%r9, %rax
	subq	-56(%rbp), %rax
	jmp	.L43
.L139:
	movsbw	%bl, %ax
	addq	$1, %r13
	movl	$14, %r15d
	subl	$48, %eax
	movw	%ax, 28(%r10)
	jmp	.L119
.L508:
	movl	$59, %eax
	movl	$59, %r15d
	jmp	.L427
.L388:
	testw	%cx, %cx
	je	.L397
	addq	$1, %r13
	xorl	%r11d, %r11d
	movl	$53, %r15d
	jmp	.L119
.L846:
	cmpl	$196949, %ebx
	je	.L190
	jle	.L876
	cmpl	$262738, %ebx
	jne	.L775
	movb	$29, 30(%r10)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L847:
	cmpl	$328272, %ebx
	je	.L198
	cmpl	$590153, %ebx
	jne	.L775
	movb	$31, 30(%r10)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L187:
	cmpl	$983635, %ebx
	je	.L201
	jle	.L877
	cmpl	$1704261, %ebx
	je	.L208
	jle	.L878
	cmpl	$1704271, %ebx
	jne	.L775
	movb	$33, 30(%r10)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L876:
	cmpl	$196929, %ebx
	je	.L192
	cmpl	$196946, %ebx
	jne	.L775
	movb	$12, 30(%r10)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L878:
	cmpl	$983881, %ebx
	je	.L210
	cmpl	$1311298, %ebx
	jne	.L775
	movb	$17, 30(%r10)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L877:
	cmpl	$787536, %ebx
	je	.L203
	jle	.L879
	cmpl	$983618, %ebx
	jne	.L775
	movb	$18, 30(%r10)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L879:
	cmpl	$655937, %ebx
	je	.L205
	cmpl	$656193, %ebx
	jne	.L775
	movb	$30, 30(%r10)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L817:
	cmpb	$5, 30(%r10)
	je	.L387
	testb	$64, %al
	jne	.L387
	testl	%edx, %edx
	jne	.L388
.L387:
	movzbl	(%r10), %eax
	movq	-72(%rbp), %rdi
	movq	%r13, %r9
	movq	%r10, %r13
	andl	$3, %eax
	movq	56(%rdi), %rcx
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$14, %eax
	addl	$4, %eax
	movl	%eax, %edx
	andl	$127, %edx
	testq	%rcx, %rcx
	je	.L391
	movzbl	%dl, %eax
	movq	%r9, -64(%rbp)
	movq	%r10, %rdi
	leal	0(,%rax,4), %edx
	movzwl	(%r10), %eax
	andw	$-509, %ax
	orl	%edx, %eax
	movw	%ax, (%r10)
	call	*%rcx
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	jne	.L880
.L392:
	movzwl	0(%r13), %eax
	shrw	$2, %ax
	andl	$127, %eax
	testb	$127, 31(%r13)
	jne	.L796
.L391:
	movzwl	0(%r13), %edx
	movl	$0, 8(%r13)
	andl	$127, %eax
	sall	$2, %eax
	andw	$-509, %dx
.L795:
	orl	%edx, %eax
	movw	%ax, 0(%r13)
	movq	%r9, %rax
	subq	-56(%rbp), %rax
	addq	$1, %rax
	jmp	.L43
.L382:
	orb	$-128, 31(%r10)
.L381:
	orw	$64, 4(%r10)
.L380:
	movzbl	31(%r10), %r9d
	testb	$127, %r9b
	je	.L384
	movq	%r13, %r9
	movl	%r11d, 8(%r10)
	movq	%r10, %r13
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L856:
	cmpb	$10, %bl
	jne	.L881
	cmpb	$13, %bl
	je	.L55
	cmpb	$10, %bl
	je	.L154
.L461:
	addq	$1, %r13
	movl	$16, %r15d
	jmp	.L119
.L839:
	addq	$1, %r13
	movl	$43, %r15d
	jmp	.L119
.L854:
	movzbl	(%r10), %eax
	addq	$1, %r13
	movl	$6, %r15d
	andl	$-4, %eax
	orl	$1, %eax
	movb	%al, (%r10)
	jmp	.L119
.L821:
	addq	$1, %r13
	movl	$56, %r15d
	jmp	.L119
.L848:
	addq	$1, %r13
	jmp	.L119
.L220:
	cmpq	$0, -96(%rbp)
	je	.L882
	movq	-72(%rbp), %rax
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.L883
	movl	%r11d, -128(%rbp)
	movzwl	(%r10), %eax
	movq	%r13, %rdx
	movq	%r10, %rdi
	andw	$-509, %ax
	orb	$-128, %al
	movw	%ax, (%r10)
	movq	-96(%rbp), %rsi
	movq	%r10, -96(%rbp)
	subq	%rsi, %rdx
	call	*%rcx
	movq	-96(%rbp), %r10
	movl	-128(%rbp), %r11d
	testl	%eax, %eax
	jne	.L884
.L225:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	movq	$0, -96(%rbp)
	addq	$1, %r13
	jmp	.L119
.L816:
	movq	16(%r10), %rdi
	leaq	-1(%rdi), %rdx
	cmpq	$-3, %rdx
	setbe	%dl
	movzbl	%dl, %edx
	jmp	.L385
.L885:
	cmpb	$32, %cl
	je	.L182
.L775:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$16, %eax
	movb	%al, 31(%r10)
	movl	$19, %eax
	jmp	.L54
.L780:
	movl	%r15d, %ecx
	jmp	.L337
.L143:
	addq	$1, %r13
	movl	$15, %r15d
	jmp	.L119
.L845:
	testb	%cl, %cl
	jne	.L885
	movl	$20, %r15d
	jmp	.L182
.L834:
	addq	$1, %r13
	movl	$61, %r15d
	jmp	.L119
.L794:
	movq	%r15, %rax
	subq	-56(%rbp), %rax
	jmp	.L43
.L233:
	cmpb	$33, 30(%r10)
	jne	.L234
	addq	$1, %r13
	movl	$37, %r15d
	jmp	.L119
.L232:
	addq	$1, %r13
	movl	$33, %r15d
	jmp	.L119
.L852:
	addq	$1, %r13
	jmp	.L119
.L266:
	movzbl	1(%r10), %edx
	movq	%rax, %rbx
	andl	$1, %edx
	orl	$8, %edx
	movb	%dl, 1(%r10)
	jmp	.L251
.L397:
	testb	$1, %ah
	je	.L398
	testb	$3, (%r10)
	jne	.L521
	cmpb	$0, -113(%rbp)
	je	.L399
.L521:
	addq	$1, %r13
	xorl	%r11d, %r11d
	movl	$63, %r15d
	jmp	.L119
.L456:
	movq	%r13, -104(%rbp)
	jmp	.L147
.L482:
	movl	$16, %edi
	jmp	.L335
.L812:
	movzwl	4(%r10), %eax
	cmpl	$24, %edi
	je	.L886
	movl	%eax, %edx
	orl	$4, %eax
	orl	$8, %edx
	cmpl	$25, %edi
	cmovne	%edx, %eax
	movw	%ax, 4(%r10)
	jmp	.L347
.L486:
	movl	$19, %edi
	jmp	.L335
.L811:
	movq	%r8, %rax
	subq	%r13, %rax
	addl	%eax, %r11d
	cmpl	-128(%rbp), %r11d
	ja	.L887
	leal	(%rdi,%rdi), %eax
	movzbl	1(%r10), %edi
	andl	$1, %edi
	orl	%eax, %edi
	cmpq	$0, -80(%rbp)
	movb	%dil, 1(%r10)
	je	.L888
	movq	-72(%rbp), %rax
	movq	32(%rax), %r13
	testq	%r13, %r13
	je	.L889
	movl	%r11d, -136(%rbp)
	movzwl	(%r10), %eax
	movq	%r8, %rdx
	movq	%r10, %rdi
	andw	$-509, %ax
	orb	$-48, %al
	movw	%ax, (%r10)
	movq	-80(%rbp), %rsi
	movq	%r8, -128(%rbp)
	movq	%r10, -80(%rbp)
	subq	%rsi, %rdx
	call	*%r13
	movq	-80(%rbp), %r10
	movq	-128(%rbp), %r8
	leaq	.L365(%rip), %rcx
	testl	%eax, %eax
	movl	-136(%rbp), %r11d
	jne	.L890
.L314:
	movzwl	(%r10), %r15d
	movzbl	31(%r10), %r9d
	shrw	$2, %r15w
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L799
	movq	$0, -80(%rbp)
	movq	%r8, %r13
	jmp	.L57
.L810:
	movzbl	1(%r10), %edx
	movl	%edi, %ebx
	leal	(%rdi,%rdi), %eax
	movq	-80(%rbp), %rdi
	andl	$127, %ebx
	andl	$1, %edx
	orl	%eax, %edx
	movb	%dl, 1(%r10)
	testq	%rdi, %rdi
	je	.L480
	movq	-72(%rbp), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L480
	movl	%r11d, -136(%rbp)
	movzwl	(%r10), %edx
	movq	%rdi, %rsi
	andw	$-509, %dx
	orb	$-48, %dl
	movw	%dx, (%r10)
	movq	%r8, %rdx
	subq	%rdi, %rdx
	movq	%r8, -128(%rbp)
	movq	%r10, %rdi
	movq	%r10, -80(%rbp)
	call	*%rax
	movq	-80(%rbp), %r10
	movq	-128(%rbp), %r8
	testl	%eax, %eax
	movl	-136(%rbp), %r11d
	jne	.L891
.L308:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L309
	movl	max_header_size(%rip), %eax
	movzbl	1(%r10), %edx
	movl	%eax, -128(%rbp)
.L307:
	addl	%ebx, %ebx
	andl	$1, %edx
	movq	$0, -80(%rbp)
	orl	%ebx, %edx
	movb	%dl, 1(%r10)
	cmpq	%r8, %r14
	jne	.L350
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L822:
	cmpb	$59, %bl
	je	.L522
	cmpb	$32, %bl
	jne	.L424
.L522:
	addq	$1, %r13
	movl	$55, %r15d
	jmp	.L119
.L807:
	movl	$589824, 24(%r10)
	movl	$44, %edx
	movl	$44, %r15d
.L458:
	cmpq	$0, -96(%rbp)
	je	.L892
	movq	-72(%rbp), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L893
	movl	%r11d, -128(%rbp)
	movzwl	(%r10), %ecx
	sall	$2, %edx
	movq	%r10, %rdi
	andw	$-509, %cx
	orl	%ecx, %edx
	movw	%dx, (%r10)
	movq	-96(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r10, -96(%rbp)
	subq	%rsi, %rdx
	call	*%rax
	movq	-96(%rbp), %r10
	movl	-128(%rbp), %r11d
	testl	%eax, %eax
	jne	.L894
.L229:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	movq	$0, -96(%rbp)
	addq	$1, %r13
	jmp	.L119
.L231:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$17, %eax
	movb	%al, 31(%r10)
	movl	$1, %eax
	jmp	.L54
.L769:
	addq	%rdx, %r13
	jmp	.L119
.L809:
	cmpq	$0, -104(%rbp)
	je	.L895
	andb	$-128, 2(%r10)
.L154:
	movq	-72(%rbp), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L896
	movl	%r11d, -128(%rbp)
	movzwl	(%r10), %eax
	movq	%r13, %rdx
	movq	%r10, %rdi
	andw	$-509, %ax
	orb	$-80, %al
	movw	%ax, (%r10)
	movq	-104(%rbp), %rsi
	movq	%r10, -104(%rbp)
	subq	%rsi, %rdx
	call	*%rcx
	movq	-104(%rbp), %r10
	movl	-128(%rbp), %r11d
	testl	%eax, %eax
	jne	.L897
.L156:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	movq	$0, -104(%rbp)
	addq	$1, %r13
	jmp	.L119
.L875:
	movl	$1, %eax
	testb	$3, (%r10)
	je	.L790
	cmpw	$101, 28(%r10)
	sete	%al
	jmp	.L790
.L874:
	movl	-120(%rbp), %edx
	testl	%edx, %edx
	je	.L376
	testb	$1, %al
	je	.L375
.L376:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$26, %eax
	movb	%al, 31(%r10)
	movl	$57, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L851:
	addq	$1, %r13
	movl	$47, %r15d
	jmp	.L119
.L142:
	cmpq	$0, -104(%rbp)
	je	.L456
	andb	$-128, 2(%r10)
	jmp	.L55
.L487:
	movl	$20, %edi
	jmp	.L335
.L792:
	movzbl	31(%r10), %eax
	movl	%r11d, %r14d
	movq	%r10, %r13
	movq	%r8, %r9
	andl	$-128, %eax
	orl	$25, %eax
	movb	%al, 31(%r10)
	movzbl	1(%r10), %eax
	andl	$1, %eax
	orl	$22, %eax
	movb	%al, 1(%r10)
	movl	$50, %eax
	jmp	.L54
.L518:
	movzbl	31(%r10), %eax
	movl	%r11d, %r14d
	movq	%r10, %r13
	movq	%r8, %r9
	andl	$-128, %eax
	orl	$24, %eax
	movb	%al, 31(%r10)
	movl	$50, %eax
	jmp	.L54
.L274:
	andl	$127, %edx
	movq	%rax, %rbx
	cmpb	$13, %dl
	jne	.L251
	movzbl	1(%r10), %edx
	andl	$1, %edx
	orl	$20, %edx
	movb	%dl, 1(%r10)
	jmp	.L251
.L861:
	andl	$127, %eax
	cmpb	$6, %al
	movl	$23, %eax
	cmove	%eax, %edi
	jmp	.L335
.L398:
	movq	16(%r10), %rax
	testq	%rax, %rax
	jne	.L401
	movzbl	(%r10), %edx
	movq	-72(%rbp), %rdi
	andl	$3, %edx
	cmpb	$1, %dl
	sbbl	%eax, %eax
	andl	$14, %eax
	addl	$4, %eax
	cmpb	$1, %dl
	movq	56(%rdi), %rdx
	sbbl	%r15d, %r15d
	andl	$14, %r15d
	addl	$4, %r15d
	testq	%rdx, %rdx
	je	.L898
	movzwl	(%r10), %ecx
	andl	$127, %eax
	movq	%r10, %rdi
	sall	$2, %eax
	andw	$-509, %cx
	orl	%ecx, %eax
	movw	%ax, (%r10)
	movq	%r10, -128(%rbp)
	call	*%rdx
	movq	-128(%rbp), %r10
	testl	%eax, %eax
	jne	.L899
.L404:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	addq	$1, %r13
	xorl	%r11d, %r11d
	jmp	.L119
.L860:
	andl	$127, %eax
	cmpb	$6, %al
	movl	$26, %eax
	cmove	%eax, %edi
	jmp	.L335
.L219:
	movl	$589824, 24(%r10)
	movl	$43, %edx
	movl	$43, %r15d
	jmp	.L458
.L804:
	movq	$0, -88(%rbp)
	jmp	.L440
.L766:
	addq	$1, %r13
	movl	$48, %r15d
	jmp	.L119
.L488:
	movl	$21, %edi
	jmp	.L335
.L844:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$14, %eax
	movb	%al, 31(%r10)
	movl	$11, %eax
	jmp	.L54
.L236:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$14, %eax
	movb	%al, 31(%r10)
	movl	$40, %eax
	jmp	.L54
.L138:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$14, %eax
	movb	%al, 31(%r10)
	movl	$12, %eax
	jmp	.L54
.L892:
	addq	$1, %r13
	jmp	.L119
.L843:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$14, %eax
	movb	%al, 31(%r10)
	movl	$9, %eax
	jmp	.L54
.L136:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$14, %eax
	movb	%al, 31(%r10)
	movl	$10, %eax
	jmp	.L54
.L240:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$23, %eax
	movb	%al, 31(%r10)
	movl	$43, %eax
	jmp	.L54
.L840:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$14, %eax
	movb	%al, 31(%r10)
	movl	$39, %eax
	jmp	.L54
.L824:
	addq	$1, %r13
	xorl	%r11d, %r11d
	jmp	.L119
.L831:
	addq	$1, %r13
	xorl	%r11d, %r11d
	movl	$53, %r15d
	jmp	.L119
.L882:
	addq	$1, %r13
	movl	$32, %r15d
	jmp	.L119
.L356:
	orw	$4, 4(%r10)
	jmp	.L80
.L117:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$13, %eax
	movb	%al, 31(%r10)
	movl	$1, %eax
	jmp	.L54
.L352:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$23, %eax
	movb	%al, 31(%r10)
	movl	$52, %eax
	jmp	.L54
.L820:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$27, %eax
	movb	%al, 31(%r10)
	movl	$53, %eax
	jmp	.L54
.L838:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$14, %eax
	movb	%al, 31(%r10)
	movl	$41, %eax
	jmp	.L54
.L358:
	orw	$1, 4(%r10)
	jmp	.L80
.L825:
	movzbl	31(%r10), %eax
	movl	$0, 8(%r10)
	andl	$-128, %eax
	orl	$9, %eax
	movb	%al, 31(%r10)
	jmp	.L429
.L799:
	movq	%r8, %rax
	subq	-56(%rbp), %rax
	jmp	.L43
.L837:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$7, %eax
	movb	%al, 31(%r10)
	jmp	.L418
.L832:
	movzbl	31(%r10), %eax
	movl	$0, 8(%r10)
	andl	$-128, %eax
	orl	$10, %eax
	movb	%al, 31(%r10)
	jmp	.L437
.L480:
	movl	$52, %r15d
	jmp	.L307
.L886:
	orl	$2, %eax
	movw	%ax, 4(%r10)
	jmp	.L347
.L835:
	movq	$0, -88(%rbp)
	addq	$1, %r13
	movl	$61, %r15d
	jmp	.L119
.L833:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$6, %eax
	movb	%al, 31(%r10)
	jmp	.L413
.L871:
	movzbl	31(%r10), %eax
	movl	%r11d, %r14d
	movq	%r10, %r13
	movq	%rbx, %r9
	andl	$-128, %eax
	orl	$12, %eax
	movb	%al, 31(%r10)
	movl	$45, %eax
	jmp	.L54
.L865:
	orl	$38, %eax
	movl	$50, %r15d
	movb	%al, 1(%r10)
	jmp	.L119
.L855:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$29, %eax
	movb	%al, 31(%r10)
	movl	$3, %eax
	jmp	.L54
.L868:
	orl	$32, %eax
	movl	$50, %r15d
	movb	%al, 1(%r10)
	jmp	.L119
.L826:
	movzbl	31(%r13), %eax
	movl	%r14d, 8(%r13)
	andl	$-128, %eax
	orl	$3, %eax
	movb	%al, 31(%r13)
	jmp	.L441
.L767:
	movq	$0, -80(%rbp)
	jmp	.L80
.L862:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$4, %eax
	movb	%al, 31(%r10)
	jmp	.L372
.L401:
	cmpq	$-1, %rax
	je	.L406
	addq	$1, %r13
	xorl	%r11d, %r11d
	movl	$62, %r15d
	jmp	.L119
.L895:
	andb	$-128, 2(%r10)
	movq	%r13, -104(%rbp)
	jmp	.L154
.L827:
	movzbl	31(%r13), %eax
	movl	%r14d, 8(%r13)
	andl	$-128, %eax
	orl	$4, %eax
	movb	%al, 31(%r13)
	jmp	.L443
.L836:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$6, %eax
	movb	%al, 31(%r10)
	jmp	.L434
.L829:
	movzbl	31(%r13), %eax
	movl	%r14d, 8(%r13)
	andl	$-128, %eax
	orl	$6, %eax
	movb	%al, 31(%r13)
	jmp	.L447
.L849:
	movq	%r14, %rbx
	jmp	.L116
.L828:
	movzbl	31(%r13), %eax
	movl	%r14d, 8(%r13)
	andl	$-128, %eax
	orl	$2, %eax
	movb	%al, 31(%r13)
	jmp	.L445
.L806:
	movq	%r15, -112(%rbp)
	movl	$45, %edx
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	jmp	.L460
.L830:
	movzbl	31(%r13), %eax
	movl	%r14d, 8(%r13)
	andl	$-128, %eax
	orl	$8, %eax
	movb	%al, 31(%r13)
	jmp	.L449
.L818:
	addq	$1, %r13
	xorl	%r11d, %r11d
	jmp	.L119
.L355:
	andb	$-128, 2(%r10)
	movq	%r13, -80(%rbp)
	jmp	.L293
.L841:
	addq	$1, %r13
	movl	$5, %r15d
	jmp	.L119
.L842:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$1, %eax
	movb	%al, 31(%r10)
	jmp	.L133
.L866:
	orl	$40, %eax
	movl	$50, %r15d
	movb	%al, 1(%r10)
	jmp	.L119
.L869:
	addq	$1, %r13
	movl	$19, %r15d
	jmp	.L119
.L872:
	movq	$0, -112(%rbp)
	leaq	1(%rbx), %r13
	movl	$46, %r15d
	jmp	.L119
.L815:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$10, %eax
	movb	%al, 31(%r10)
	jmp	.L374
.L823:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$25, %eax
	movb	%al, 31(%r10)
	movl	$54, %eax
	jmp	.L54
.L351:
	movzbl	31(%r10), %eax
	movl	%r11d, %r14d
	movq	%r10, %r13
	movq	%r8, %r9
	andl	$-128, %eax
	orl	$12, %eax
	movb	%al, 31(%r10)
	movl	%r15d, %eax
	andl	$127, %eax
	jmp	.L54
.L819:
	movzbl	31(%r10), %eax
	movl	$0, 8(%r10)
	andl	$-128, %eax
	orl	$7, %eax
	movb	%al, 31(%r10)
	jmp	.L395
.L286:
	movq	%rbx, %rax
	subq	-56(%rbp), %rax
	addq	$1, %rax
	jmp	.L43
.L870:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$1, %eax
	movb	%al, 31(%r10)
	jmp	.L178
.L873:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$3, %eax
	movb	%al, 31(%r10)
	jmp	.L285
.L888:
	leaq	1(%r8), %r13
	movl	$51, %r15d
	jmp	.L119
.L850:
	movzbl	31(%r10), %edx
	movq	%rax, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	movl	$45, %eax
	andl	$-128, %edx
	orl	$12, %edx
	movb	%dl, 31(%r10)
	jmp	.L54
.L867:
	orl	$42, %eax
	movl	$50, %r15d
	movb	%al, 1(%r10)
	jmp	.L119
.L478:
	movq	%r14, %r13
	jmp	.L305
.L406:
	testb	$3, (%r10)
	je	.L503
	movzwl	28(%r10), %eax
	cmpw	$304, %ax
	sete	%cl
	cmpw	$204, %ax
	sete	%dl
	orb	%dl, %cl
	jne	.L504
	subl	$100, %eax
	cmpw	$99, %ax
	jbe	.L504
	addq	$1, %r13
	xorl	%r11d, %r11d
	movl	$63, %r15d
	jmp	.L119
.L893:
	movq	$0, -96(%rbp)
	addq	$1, %r13
	jmp	.L119
.L883:
	movq	$0, -96(%rbp)
	addq	$1, %r13
	movl	$32, %r15d
	jmp	.L119
.L205:
	movb	$21, 30(%r10)
	jmp	.L182
.L884:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$2, %eax
	movb	%al, 31(%r10)
	jmp	.L225
.L881:
	addq	$1, %r13
	movl	$16, %r15d
	jmp	.L119
.L196:
	movb	$24, 30(%r10)
	jmp	.L182
.L814:
	leaq	tokens(%rip), %r8
	movzbl	%bl, %ebx
	movzbl	(%r8,%rbx), %eax
	testb	%al, %al
	je	.L900
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	cmove	%r13, %rdi
	andb	$-128, 2(%r10)
	addq	$1, %r13
	movq	%rdi, -112(%rbp)
	cmpb	$116, %al
	je	.L244
	jg	.L245
	cmpb	$99, %al
	je	.L246
	cmpb	$112, %al
	jne	.L248
	movzbl	1(%r10), %eax
	movl	$45, %r15d
	andl	$1, %eax
	orl	$10, %eax
	movb	%al, 1(%r10)
	jmp	.L119
.L813:
	addq	$1, %r13
	movl	$57, %r15d
	jmp	.L119
.L808:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$17, %eax
	movb	%al, 31(%r10)
	movl	%r15d, %eax
	andl	$127, %eax
	jmp	.L54
.L887:
	movzbl	31(%r10), %eax
	movl	%r11d, %r14d
	movq	%r10, %r13
	movq	%r8, %r9
	andl	$-128, %eax
	orl	$12, %eax
	movb	%al, 31(%r10)
	movl	$52, %eax
	jmp	.L54
.L853:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L901
	movl	%r11d, -136(%rbp)
	movzwl	(%r10), %eax
	movq	%r10, %rdi
	andw	$-509, %ax
	orl	$12, %eax
	movw	%ax, (%r10)
	movq	%r10, -128(%rbp)
	call	*%rdx
	movq	-128(%rbp), %r10
	movl	-136(%rbp), %r11d
	testl	%eax, %eax
	jne	.L902
.L124:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	addq	$1, %r13
	jmp	.L119
.L512:
	addq	$1, %r13
	jmp	.L119
.L141:
	imulw	$10, 28(%r10), %ax
	leal	-48(%rax,%rbx), %eax
	movw	%ax, 28(%r10)
	cmpw	$999, %ax
	ja	.L146
	addq	$1, %r13
	jmp	.L119
.L863:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$25, %eax
	movb	%al, 31(%r10)
	movl	$50, %eax
	jmp	.L54
.L203:
	movb	$13, 30(%r10)
	jmp	.L182
.L190:
	movb	$4, 30(%r10)
	jmp	.L182
.L188:
	movb	$22, 30(%r10)
	jmp	.L182
.L186:
	movb	$11, 30(%r10)
	jmp	.L182
.L198:
	movb	$8, 30(%r10)
	jmp	.L182
.L208:
	movb	$14, 30(%r10)
	jmp	.L182
.L192:
	movb	$28, 30(%r10)
	jmp	.L182
.L201:
	movb	$27, 30(%r10)
	jmp	.L182
.L210:
	movb	$32, 30(%r10)
	jmp	.L182
.L894:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$2, %eax
	movb	%al, 31(%r10)
	jmp	.L229
.L858:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$8, %eax
	movb	%al, 31(%r10)
	jmp	.L151
.L857:
	movq	$0, -104(%rbp)
	addq	$1, %r13
	movl	$17, %r15d
	jmp	.L119
.L520:
	addq	$1, %r13
	movl	$46, %r15d
	jmp	.L119
.L891:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$4, %eax
	movb	%al, 31(%r10)
	jmp	.L308
.L245:
	cmpb	$117, %al
	jne	.L248
	movzbl	1(%r10), %eax
	movl	$45, %r15d
	andl	$1, %eax
	orl	$16, %eax
	movb	%al, 1(%r10)
	jmp	.L119
.L890:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$4, %eax
	movb	%al, 31(%r10)
	jmp	.L314
.L889:
	movq	$0, -80(%rbp)
	leaq	1(%r8), %r13
	movl	$51, %r15d
	jmp	.L119
.L420:
	movl	%r15d, %eax
	movq	%r13, %r9
	movl	%r11d, 8(%r10)
	movq	%r10, %r13
	andl	$127, %eax
	leal	0(,%rax,4), %edx
	movzwl	(%r10), %eax
	andw	$-509, %ax
	jmp	.L795
.L309:
	movq	%r8, %rax
	subq	-56(%rbp), %rax
	addq	$1, %rax
	jmp	.L43
.L897:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$8, %eax
	movb	%al, 31(%r10)
	jmp	.L156
.L896:
	movq	$0, -104(%rbp)
	addq	$1, %r13
	movl	$44, %r15d
	jmp	.L119
.L282:
	movzbl	31(%r10), %eax
	movl	%r11d, %r14d
	movq	%r10, %r13
	movq	%rbx, %r9
	andl	$-128, %eax
	orl	$24, %eax
	movb	%al, 31(%r10)
	movl	$45, %eax
	jmp	.L54
.L131:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$29, %eax
	movb	%al, 31(%r10)
	movl	$4, %eax
	jmp	.L54
.L140:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$15, %eax
	movb	%al, 31(%r10)
	movl	$13, %eax
	jmp	.L54
.L239:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$14, %eax
	movb	%al, 31(%r10)
	movl	$42, %eax
	jmp	.L54
.L246:
	movzbl	1(%r10), %eax
	movl	$45, %r15d
	andl	$1, %eax
	orl	$2, %eax
	movb	%al, 1(%r10)
	jmp	.L119
.L899:
	movzbl	31(%r10), %eax
	movl	$0, 8(%r10)
	andl	$-128, %eax
	orl	$7, %eax
	movb	%al, 31(%r10)
	jmp	.L404
.L898:
	addq	$1, %r13
	xorl	%r11d, %r11d
	jmp	.L119
.L244:
	movzbl	1(%r10), %eax
	movl	$45, %r15d
	andl	$1, %eax
	orl	$14, %eax
	movb	%al, 1(%r10)
	jmp	.L119
.L248:
	andb	$1, 1(%r10)
	movl	$45, %r15d
	jmp	.L119
.L864:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$26, %eax
	movb	%al, 31(%r10)
	movl	$50, %eax
	jmp	.L54
.L424:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$27, %eax
	movb	%al, 31(%r10)
	movl	$54, %eax
	jmp	.L54
.L504:
	movl	$4, %r15d
.L407:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rcx
	testq	%rcx, %rcx
	je	.L903
	movl	%r15d, %eax
	movq	%r10, %rdi
	andl	$127, %eax
	leal	0(,%rax,4), %edx
	movzwl	(%r10), %eax
	andw	$-509, %ax
	orl	%edx, %eax
	movw	%ax, (%r10)
	movq	%r10, -128(%rbp)
	call	*%rcx
	movq	-128(%rbp), %r10
	testl	%eax, %eax
	jne	.L904
.L409:
	movzwl	(%r10), %eax
	movzbl	31(%r10), %r9d
	shrw	$2, %ax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	$127, %r9b
	jne	.L438
	addq	$1, %r13
	xorl	%r11d, %r11d
	jmp	.L119
.L503:
	movl	$18, %r15d
	jmp	.L407
.L880:
	movzbl	31(%r13), %eax
	movl	$0, 8(%r13)
	andl	$-128, %eax
	orl	$7, %eax
	movb	%al, 31(%r13)
	jmp	.L392
.L399:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$28, %eax
	jmp	.L798
.L900:
	movzbl	31(%r10), %eax
	movq	%r13, %r9
	movl	%r11d, %r14d
	movq	%r10, %r13
	andl	$-128, %eax
	orl	$24, %eax
	movb	%al, 31(%r10)
	movl	$44, %eax
	jmp	.L54
.L902:
	movzbl	31(%r10), %eax
	movl	%r11d, 8(%r10)
	andl	$-128, %eax
	orl	$1, %eax
	movb	%al, 31(%r10)
	jmp	.L124
.L901:
	addq	$1, %r13
	movl	$3, %r15d
	jmp	.L119
.L149:
	cmpb	$10, %bl
	jne	.L461
	cmpq	$0, -104(%rbp)
	jne	.L154
	addq	$1, %r13
	movl	$44, %r15d
	jmp	.L119
.L904:
	movzbl	31(%r10), %eax
	movl	$0, 8(%r10)
	andl	$-128, %eax
	orl	$7, %eax
	movb	%al, 31(%r10)
	jmp	.L409
.L903:
	addq	$1, %r13
	xorl	%r11d, %r11d
	jmp	.L119
	.cfi_endproc
.LFE14:
	.size	http_parser_execute, .-http_parser_execute
	.p2align 4
	.globl	http_message_needs_eof
	.type	http_message_needs_eof, @function
http_message_needs_eof:
.LFB15:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	testb	$3, (%rdi)
	je	.L905
	movzwl	28(%rdi), %eax
	cmpw	$204, %ax
	sete	%cl
	cmpw	$304, %ax
	sete	%dl
	orb	%dl, %cl
	jne	.L908
	subl	$100, %eax
	cmpw	$99, %ax
	jbe	.L908
	movzwl	4(%rdi), %eax
	testb	$64, %al
	jne	.L905
	movl	%eax, %edx
	movl	$1, %r8d
	andw	$257, %dx
	cmpw	$256, %dx
	je	.L905
	xorl	%r8d, %r8d
	testb	$1, %al
	jne	.L905
	xorl	%r8d, %r8d
	cmpq	$-1, 16(%rdi)
	sete	%r8b
.L905:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	xorl	%r8d, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE15:
	.size	http_message_needs_eof, .-http_message_needs_eof
	.p2align 4
	.globl	http_should_keep_alive
	.type	http_should_keep_alive, @function
http_should_keep_alive:
.LFB16:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpw	$0, 24(%rdi)
	movzwl	4(%rdi), %edx
	je	.L915
	cmpw	$0, 26(%rdi)
	je	.L915
	testb	$4, %dl
	jne	.L929
.L917:
	movl	$1, %eax
	testb	$3, (%rdi)
	je	.L914
	movzwl	28(%rdi), %ecx
	cmpw	$204, %cx
	sete	%r8b
	cmpw	$304, %cx
	sete	%sil
	orb	%sil, %r8b
	jne	.L922
	subl	$100, %ecx
	cmpw	$99, %cx
	jbe	.L922
	testb	$64, %dl
	jne	.L914
	movl	%edx, %ecx
	xorl	%eax, %eax
	andw	$257, %cx
	cmpw	$256, %cx
	je	.L914
	andl	$1, %edx
	movl	$1, %eax
	jne	.L914
	xorl	%eax, %eax
	cmpq	$-1, 16(%rdi)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L915:
	testb	$2, %dl
	jne	.L917
.L914:
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L929:
	ret
	.cfi_endproc
.LFE16:
	.size	http_should_keep_alive, .-http_should_keep_alive
	.section	.rodata.str1.1
.LC9:
	.string	"<unknown>"
	.text
	.p2align 4
	.globl	http_method_str
	.type	http_method_str, @function
http_method_str:
.LFB17:
	.cfi_startproc
	endbr64
	leaq	.LC9(%rip), %rax
	cmpl	$33, %edi
	ja	.L930
	movl	%edi, %edi
	leaq	method_strings(%rip), %rax
	movq	(%rax,%rdi,8), %rax
.L930:
	ret
	.cfi_endproc
.LFE17:
	.size	http_method_str, .-http_method_str
	.section	.rodata.str1.1
.LC10:
	.string	"Continue"
.LC11:
	.string	"Processing"
.LC12:
	.string	"OK"
.LC13:
	.string	"Created"
.LC14:
	.string	"Accepted"
.LC15:
	.string	"Non-Authoritative Information"
.LC16:
	.string	"No Content"
.LC17:
	.string	"Reset Content"
.LC18:
	.string	"Partial Content"
.LC19:
	.string	"Multi-Status"
.LC20:
	.string	"Already Reported"
.LC21:
	.string	"IM Used"
.LC22:
	.string	"Multiple Choices"
.LC23:
	.string	"Moved Permanently"
.LC24:
	.string	"Found"
.LC25:
	.string	"See Other"
.LC26:
	.string	"Not Modified"
.LC27:
	.string	"Use Proxy"
.LC28:
	.string	"Temporary Redirect"
.LC29:
	.string	"Permanent Redirect"
.LC30:
	.string	"Bad Request"
.LC31:
	.string	"Unauthorized"
.LC32:
	.string	"Payment Required"
.LC33:
	.string	"Forbidden"
.LC34:
	.string	"Not Found"
.LC35:
	.string	"Method Not Allowed"
.LC36:
	.string	"Not Acceptable"
.LC37:
	.string	"Proxy Authentication Required"
.LC38:
	.string	"Request Timeout"
.LC39:
	.string	"Conflict"
.LC40:
	.string	"Gone"
.LC41:
	.string	"Length Required"
.LC42:
	.string	"Precondition Failed"
.LC43:
	.string	"Payload Too Large"
.LC44:
	.string	"URI Too Long"
.LC45:
	.string	"Unsupported Media Type"
.LC46:
	.string	"Range Not Satisfiable"
.LC47:
	.string	"Expectation Failed"
.LC48:
	.string	"Misdirected Request"
.LC49:
	.string	"Unprocessable Entity"
.LC50:
	.string	"Locked"
.LC51:
	.string	"Failed Dependency"
.LC52:
	.string	"Upgrade Required"
.LC53:
	.string	"Precondition Required"
.LC54:
	.string	"Too Many Requests"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"Request Header Fields Too Large"
	.section	.rodata.str1.1
.LC56:
	.string	"Unavailable For Legal Reasons"
.LC57:
	.string	"Internal Server Error"
.LC58:
	.string	"Not Implemented"
.LC59:
	.string	"Bad Gateway"
.LC60:
	.string	"Service Unavailable"
.LC61:
	.string	"Gateway Timeout"
.LC62:
	.string	"HTTP Version Not Supported"
.LC63:
	.string	"Variant Also Negotiates"
.LC64:
	.string	"Insufficient Storage"
.LC65:
	.string	"Loop Detected"
.LC66:
	.string	"Not Extended"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"Network Authentication Required"
	.section	.rodata.str1.1
.LC68:
	.string	"Switching Protocols"
	.text
	.p2align 4
	.globl	http_status_str
	.type	http_status_str, @function
http_status_str:
.LFB18:
	.cfi_startproc
	endbr64
	subl	$100, %edi
	cmpl	$411, %edi
	ja	.L934
	leaq	.L936(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L936:
	.long	.L995-.L936
	.long	.L993-.L936
	.long	.L992-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L991-.L936
	.long	.L990-.L936
	.long	.L989-.L936
	.long	.L988-.L936
	.long	.L987-.L936
	.long	.L986-.L936
	.long	.L985-.L936
	.long	.L984-.L936
	.long	.L983-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L982-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L981-.L936
	.long	.L980-.L936
	.long	.L979-.L936
	.long	.L978-.L936
	.long	.L977-.L936
	.long	.L976-.L936
	.long	.L934-.L936
	.long	.L975-.L936
	.long	.L974-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L973-.L936
	.long	.L972-.L936
	.long	.L971-.L936
	.long	.L970-.L936
	.long	.L969-.L936
	.long	.L968-.L936
	.long	.L967-.L936
	.long	.L966-.L936
	.long	.L965-.L936
	.long	.L964-.L936
	.long	.L963-.L936
	.long	.L962-.L936
	.long	.L961-.L936
	.long	.L960-.L936
	.long	.L959-.L936
	.long	.L958-.L936
	.long	.L957-.L936
	.long	.L956-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L955-.L936
	.long	.L954-.L936
	.long	.L953-.L936
	.long	.L952-.L936
	.long	.L934-.L936
	.long	.L951-.L936
	.long	.L934-.L936
	.long	.L950-.L936
	.long	.L949-.L936
	.long	.L934-.L936
	.long	.L948-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L947-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L934-.L936
	.long	.L946-.L936
	.long	.L945-.L936
	.long	.L944-.L936
	.long	.L943-.L936
	.long	.L942-.L936
	.long	.L941-.L936
	.long	.L940-.L936
	.long	.L939-.L936
	.long	.L938-.L936
	.long	.L934-.L936
	.long	.L937-.L936
	.long	.L935-.L936
	.text
	.p2align 4,,10
	.p2align 3
.L934:
	leaq	.LC9(%rip), %rax
	ret
.L935:
	leaq	.LC67(%rip), %rax
	ret
.L937:
	leaq	.LC66(%rip), %rax
	ret
.L938:
	leaq	.LC65(%rip), %rax
	ret
.L939:
	leaq	.LC64(%rip), %rax
	ret
.L940:
	leaq	.LC63(%rip), %rax
	ret
.L941:
	leaq	.LC62(%rip), %rax
	ret
.L942:
	leaq	.LC61(%rip), %rax
	ret
.L943:
	leaq	.LC60(%rip), %rax
	ret
.L944:
	leaq	.LC59(%rip), %rax
	ret
.L945:
	leaq	.LC58(%rip), %rax
	ret
.L946:
	leaq	.LC57(%rip), %rax
	ret
.L947:
	leaq	.LC56(%rip), %rax
	ret
.L948:
	leaq	.LC55(%rip), %rax
	ret
.L949:
	leaq	.LC54(%rip), %rax
	ret
.L950:
	leaq	.LC53(%rip), %rax
	ret
.L951:
	leaq	.LC52(%rip), %rax
	ret
.L952:
	leaq	.LC51(%rip), %rax
	ret
.L953:
	leaq	.LC50(%rip), %rax
	ret
.L954:
	leaq	.LC49(%rip), %rax
	ret
.L955:
	leaq	.LC48(%rip), %rax
	ret
.L956:
	leaq	.LC47(%rip), %rax
	ret
.L957:
	leaq	.LC46(%rip), %rax
	ret
.L958:
	leaq	.LC45(%rip), %rax
	ret
.L959:
	leaq	.LC44(%rip), %rax
	ret
.L960:
	leaq	.LC43(%rip), %rax
	ret
.L961:
	leaq	.LC42(%rip), %rax
	ret
.L962:
	leaq	.LC41(%rip), %rax
	ret
.L963:
	leaq	.LC40(%rip), %rax
	ret
.L964:
	leaq	.LC39(%rip), %rax
	ret
.L965:
	leaq	.LC38(%rip), %rax
	ret
.L966:
	leaq	.LC37(%rip), %rax
	ret
.L967:
	leaq	.LC36(%rip), %rax
	ret
.L968:
	leaq	.LC35(%rip), %rax
	ret
.L969:
	leaq	.LC34(%rip), %rax
	ret
.L970:
	leaq	.LC33(%rip), %rax
	ret
.L971:
	leaq	.LC32(%rip), %rax
	ret
.L972:
	leaq	.LC31(%rip), %rax
	ret
.L973:
	leaq	.LC30(%rip), %rax
	ret
.L974:
	leaq	.LC29(%rip), %rax
	ret
.L975:
	leaq	.LC28(%rip), %rax
	ret
.L976:
	leaq	.LC27(%rip), %rax
	ret
.L977:
	leaq	.LC26(%rip), %rax
	ret
.L978:
	leaq	.LC25(%rip), %rax
	ret
.L979:
	leaq	.LC24(%rip), %rax
	ret
.L980:
	leaq	.LC23(%rip), %rax
	ret
.L981:
	leaq	.LC22(%rip), %rax
	ret
.L982:
	leaq	.LC21(%rip), %rax
	ret
.L983:
	leaq	.LC20(%rip), %rax
	ret
.L984:
	leaq	.LC19(%rip), %rax
	ret
.L985:
	leaq	.LC18(%rip), %rax
	ret
.L986:
	leaq	.LC17(%rip), %rax
	ret
.L987:
	leaq	.LC16(%rip), %rax
	ret
.L988:
	leaq	.LC15(%rip), %rax
	ret
.L989:
	leaq	.LC14(%rip), %rax
	ret
.L990:
	leaq	.LC13(%rip), %rax
	ret
.L991:
	leaq	.LC12(%rip), %rax
	ret
.L992:
	leaq	.LC11(%rip), %rax
	ret
.L995:
	leaq	.LC10(%rip), %rax
	ret
.L993:
	leaq	.LC68(%rip), %rax
	ret
	.cfi_endproc
.LFE18:
	.size	http_status_str, .-http_status_str
	.p2align 4
	.globl	http_parser_init
	.type	http_parser_init, @function
http_parser_init:
.LFB19:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	pxor	%xmm0, %xmm0
	andl	$3, %eax
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movb	%al, (%rdi)
	movl	$18, %eax
	testl	%esi, %esi
	je	.L997
	cmpl	$1, %esi
	sete	%al
	leal	2(%rax,%rax), %eax
.L997:
	movzwl	(%rdi), %edx
	movzbl	%al, %eax
	andb	$-128, 31(%rdi)
	sall	$2, %eax
	andw	$-509, %dx
	orl	%edx, %eax
	movw	%ax, (%rdi)
	ret
	.cfi_endproc
.LFE19:
	.size	http_parser_init, .-http_parser_init
	.p2align 4
	.globl	http_parser_settings_init
	.type	http_parser_settings_init, @function
http_parser_settings_init:
.LFB20:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE20:
	.size	http_parser_settings_init, .-http_parser_settings_init
	.p2align 4
	.globl	http_errno_name
	.type	http_errno_name, @function
http_errno_name:
.LFB21:
	.cfi_startproc
	endbr64
	movl	%edi, %edi
	leaq	http_strerror_tab(%rip), %rax
	salq	$4, %rdi
	movq	(%rax,%rdi), %rax
	ret
	.cfi_endproc
.LFE21:
	.size	http_errno_name, .-http_errno_name
	.p2align 4
	.globl	http_errno_description
	.type	http_errno_description, @function
http_errno_description:
.LFB22:
	.cfi_startproc
	endbr64
	movl	%edi, %edi
	leaq	http_strerror_tab(%rip), %rax
	salq	$4, %rdi
	movq	8(%rax,%rdi), %rax
	ret
	.cfi_endproc
.LFE22:
	.size	http_errno_description, .-http_errno_description
	.p2align 4
	.globl	http_parser_url_init
	.type	http_parser_url_init, @function
http_parser_url_init:
.LFB25:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	ret
	.cfi_endproc
.LFE25:
	.size	http_parser_url_init, .-http_parser_url_init
	.p2align 4
	.globl	http_parser_parse_url
	.type	http_parser_parse_url, @function
http_parser_parse_url:
.LFB26:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L1228
	cmpl	$1, %edx
	movl	$0, (%rcx)
	movq	%rcx, %r8
	sbbl	%eax, %eax
	addq	%rdi, %rsi
	andl	$-4, %eax
	addl	$24, %eax
	cmpq	%rsi, %rdi
	jnb	.L1008
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movl	$7, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movabsq	$4294976512, %rbx
.L1078:
	movzbl	(%r10), %r9d
	cmpb	$32, %r9b
	ja	.L1059
	btq	%r9, %rbx
	jnc	.L1306
.L1006:
	movl	$1, %eax
.L1005:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1315:
	.cfi_restore_state
	addw	$1, 26(%r8)
	cmpq	%r10, %rsi
	jbe	.L1027
	movzbl	1(%r15), %r9d
	cmpb	$32, %r9b
	jbe	.L1251
	movl	$31, %eax
	.p2align 4,,10
	.p2align 3
.L1059:
	subl	$21, %eax
	cmpl	$10, %eax
	ja	.L1010
	leaq	.L1012(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1012:
	.long	.L1025-.L1012
	.long	.L1019-.L1012
	.long	.L1018-.L1012
	.long	.L1017-.L1012
	.long	.L1017-.L1012
	.long	.L1016-.L1012
	.long	.L1044-.L1012
	.long	.L1014-.L1012
	.long	.L1014-.L1012
	.long	.L1060-.L1012
	.long	.L1071-.L1012
	.text
	.p2align 4,,10
	.p2align 3
.L1306:
	subl	$21, %eax
	cmpl	$10, %eax
	jbe	.L1307
.L1023:
	orl	$32, %r9d
	subl	$97, %r9d
	cmpb	$25, %r9b
	ja	.L1006
	leaq	1(%r10), %rax
	testl	%r11d, %r11d
	je	.L1308
	movq	%r10, %rcx
	movl	$1, %r15d
	orw	$1, (%r8)
	subq	%rdi, %rcx
	movw	%r15w, 6(%r8)
	movw	%cx, 4(%r8)
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	1(%r10), %r9d
	cmpb	$32, %r9b
	ja	.L1161
	btq	%r9, %rbx
	jc	.L1006
	movq	%rax, %r10
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L1025:
	movl	%r9d, %eax
	orl	$32, %eax
	subl	$97, %eax
	cmpb	$25, %al
	jbe	.L1026
	cmpb	$58, %r9b
	jne	.L1006
	leaq	1(%r10), %rax
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	1(%r10), %r9d
	cmpb	$32, %r9b
	jbe	.L1006
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L1019:
	cmpb	$47, %r9b
	jne	.L1006
	leaq	1(%r10), %rax
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	1(%r10), %r9d
	cmpb	$32, %r9b
	jbe	.L1006
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L1018:
	cmpb	$47, %r9b
	jne	.L1006
	leaq	1(%r10), %rax
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	1(%r10), %r9d
	cmpb	$32, %r9b
	ja	.L1128
	btq	%r9, %rbx
	jc	.L1006
.L1128:
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	%r10, %rax
.L1028:
	movq	%rax, %r15
	cmpb	$47, %r9b
	je	.L1029
	cmpb	$63, %r9b
	je	.L1309
	cmpb	$64, %r9b
	je	.L1032
	movl	%r9d, %ecx
	orl	$32, %ecx
	subl	$97, %ecx
	cmpb	$25, %cl
	jbe	.L1035
	leal	-48(%r9), %ecx
	cmpb	$9, %cl
	jbe	.L1035
	cmpb	$45, %r9b
	je	.L1035
	cmpb	$95, %r9b
	je	.L1035
	cmpb	$46, %r9b
	je	.L1035
	cmpb	$33, %r9b
	je	.L1035
	cmpb	$126, %r9b
	je	.L1035
	cmpb	$42, %r9b
	je	.L1035
	leal	-39(%r9), %ecx
	cmpb	$2, %cl
	jbe	.L1035
	cmpb	$37, %r9b
	je	.L1035
	cmpb	$59, %r9b
	je	.L1035
	cmpb	$58, %r9b
	je	.L1035
	cmpb	$38, %r9b
	je	.L1035
	cmpb	$61, %r9b
	je	.L1035
	cmpb	$43, %r9b
	je	.L1035
	movl	%r9d, %ecx
	andl	$-9, %ecx
	cmpb	$36, %cl
	je	.L1035
	subl	$91, %r9d
	andl	$253, %r9d
	jne	.L1006
	.p2align 4,,10
	.p2align 3
.L1035:
	leaq	1(%rax), %rcx
	movq	%rcx, %r10
	cmpl	$1, %r11d
	je	.L1310
	movl	$2, %r9d
	movl	$25, %eax
	movl	$1, %r11d
.L1052:
	movl	%r11d, %ecx
	subq	%rdi, %r15
	movl	$1, %r12d
	leaq	(%r8,%rcx,4), %rcx
	movw	%r15w, 4(%rcx)
	movw	%r12w, 6(%rcx)
	orw	%r9w, (%r8)
	cmpq	%r10, %rsi
	ja	.L1078
.L1027:
	movzwl	(%r8), %eax
	movl	%eax, %ecx
	andl	$3, %ecx
	cmpw	$1, %cx
	je	.L1006
	testb	$2, %al
	je	.L1079
	movzwl	8(%r8), %ecx
	movzwl	10(%r8), %esi
	xorl	%r10d, %r10d
	movw	%r10w, 10(%r8)
	addl	%ecx, %esi
	movq	%rcx, %rax
	movslq	%esi, %rsi
	addq	%rdi, %rax
	addq	%rdi, %rsi
	testl	%r14d, %r14d
	jne	.L1080
	cmpq	%rax, %rsi
	jbe	.L1006
	movzbl	(%rax), %ecx
.L1085:
	cmpb	$91, %cl
	je	.L1311
	movl	%ecx, %r9d
	orl	$32, %r9d
	subl	$97, %r9d
	cmpb	$25, %r9b
	jbe	.L1091
	subl	$45, %ecx
	cmpb	$50, %cl
	ja	.L1006
	movabsq	$1125899906850811, %r9
	btq	%rcx, %r9
	jnc	.L1006
.L1091:
	movq	%rax, %rcx
	movl	$1, %r9d
	movabsq	$1125899906850811, %rbx
	subq	%rdi, %rcx
	subl	%eax, %r9d
	movw	%cx, 8(%r8)
.L1106:
	leal	(%r9,%rax), %ecx
	leaq	1(%rax), %r11
	movw	%cx, 10(%r8)
	cmpq	%rsi, %r11
	jnb	.L1099
	movzbl	1(%rax), %r10d
	movl	%r10d, %ecx
	orl	$32, %ecx
	subl	$97, %ecx
	cmpb	$25, %cl
	jbe	.L1093
	leal	-45(%r10), %ecx
	cmpb	$50, %cl
	ja	.L1006
	btq	%rcx, %rbx
	jnc	.L1312
.L1093:
	movq	%r11, %rax
	jmp	.L1106
.L1010:
	cmpb	$47, %r9b
	je	.L1022
	cmpb	$42, %r9b
	jne	.L1023
.L1022:
	leaq	1(%r10), %rax
	cmpl	$3, %r11d
	je	.L1313
	movq	%r10, %rcx
	movl	$1, %r15d
	orw	$8, (%r8)
	subq	%rdi, %rcx
	movw	%r15w, 18(%r8)
	movw	%cx, 16(%r8)
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	1(%r10), %r9d
	movl	$3, %r11d
	cmpb	$32, %r9b
	ja	.L1066
.L1273:
	btq	%r9, %rbx
	jc	.L1006
.L1066:
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L1044:
	movl	%r9d, %eax
	leaq	normal_url_char(%rip), %rcx
	shrb	$3, %al
	andl	$31, %eax
	movzbl	(%rcx,%rax), %r15d
	movl	%r9d, %ecx
	movl	%r13d, %eax
	andl	$7, %ecx
	sall	%cl, %eax
	testl	%eax, %r15d
	jne	.L1045
	testb	%r9b, %r9b
	js	.L1045
	cmpb	$35, %r9b
	je	.L1046
	cmpb	$63, %r9b
	jne	.L1006
	leaq	1(%r10), %rax
	cmpq	%rsi, %rax
	jnb	.L1027
	movzbl	1(%r10), %r9d
	cmpb	$32, %r9b
	ja	.L1136
	btq	%r9, %rbx
	jc	.L1006
.L1136:
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	%r10, %rax
.L1031:
	movl	%r9d, %ecx
	leaq	normal_url_char(%rip), %r10
	movl	%r13d, %r12d
	movq	%rax, %r15
	shrb	$3, %cl
	andl	$31, %ecx
	movzbl	(%r10,%rcx), %r10d
	movl	%r9d, %ecx
	andl	$7, %ecx
	sall	%cl, %r12d
	testl	%r12d, %r10d
	jne	.L1049
	testb	%r9b, %r9b
	js	.L1049
	cmpb	$35, %r9b
	je	.L1050
	cmpb	$63, %r9b
	jne	.L1006
.L1049:
	leaq	1(%rax), %rcx
	movq	%rcx, %r10
	cmpl	$4, %r11d
	je	.L1314
	movl	$16, %r9d
	movl	$29, %eax
	movl	$4, %r11d
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1316:
	testb	%r9b, %r9b
	js	.L1062
	cmpb	$35, %r9b
	je	.L1063
	cmpb	$63, %r9b
	jne	.L1006
.L1063:
	addq	$1, %r10
	cmpl	$5, %r11d
	je	.L1315
	movq	%r15, %rax
	movl	$1, %r9d
	orw	$32, (%r8)
	subq	%rdi, %rax
	movw	%r9w, 26(%r8)
	movw	%ax, 24(%r8)
	cmpq	%r10, %rsi
	jbe	.L1027
	movzbl	1(%r15), %r9d
	cmpb	$32, %r9b
	ja	.L1232
.L1251:
	btq	%r9, %rbx
	jc	.L1006
	movl	$5, %r11d
.L1071:
	movl	%r9d, %eax
	leaq	normal_url_char(%rip), %rcx
	movl	%r13d, %r12d
	movq	%r10, %r15
	shrb	$3, %al
	andl	$31, %eax
	movzbl	(%rcx,%rax), %eax
	movl	%r9d, %ecx
	andl	$7, %ecx
	sall	%cl, %r12d
	testl	%r12d, %eax
	je	.L1316
.L1062:
	addq	$1, %r10
	cmpl	$5, %r11d
	je	.L1317
	movl	$32, %r9d
	movl	$31, %eax
	movl	$5, %r11d
	jmp	.L1052
.L1046:
	leaq	1(%r10), %rax
	cmpq	%rsi, %rax
	jnb	.L1027
	movzbl	1(%r10), %r9d
	cmpb	$32, %r9b
	ja	.L1130
	btq	%r9, %rbx
	jc	.L1006
.L1130:
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L1060:
	movl	%r9d, %eax
	leaq	normal_url_char(%rip), %rcx
	shrb	$3, %al
	andl	$31, %eax
	movzbl	(%rcx,%rax), %r15d
	movl	%r9d, %ecx
	movl	%r13d, %eax
	andl	$7, %ecx
	sall	%cl, %eax
	testl	%eax, %r15d
	jne	.L1058
	testb	%r9b, %r9b
	js	.L1058
	cmpb	$35, %r9b
	je	.L1057
	cmpb	$63, %r9b
	jne	.L1006
.L1058:
	leaq	1(%r10), %rax
	cmpl	$5, %r11d
	je	.L1318
	movq	%r10, %rcx
	orw	$32, (%r8)
	subq	%rdi, %rcx
	movw	%cx, 24(%r8)
	movl	$1, %ecx
	movw	%cx, 26(%r8)
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	1(%r10), %r9d
	cmpb	$32, %r9b
	ja	.L1171
	btq	%r9, %rbx
	jc	.L1006
	movq	%rax, %r10
	movl	$5, %r11d
	jmp	.L1071
.L1032:
	leaq	1(%rax), %r10
	cmpl	$1, %r11d
	je	.L1319
	movq	%rax, %rcx
	movl	$1, %r12d
	orw	$2, (%r8)
	subq	%rdi, %rcx
	movw	%r12w, 10(%r8)
	movw	%cx, 8(%r8)
	cmpq	%r10, %rsi
	jbe	.L1163
	movzbl	1(%rax), %r9d
	cmpb	$32, %r9b
	ja	.L1165
	btq	%r9, %rbx
	jc	.L1006
.L1165:
	movl	$1, %r11d
.L1233:
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L1016:
	cmpb	$64, %r9b
	jne	.L1017
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1307:
	leaq	.L1126(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1126:
	.long	.L1025-.L1126
	.long	.L1019-.L1126
	.long	.L1018-.L1126
	.long	.L1017-.L1126
	.long	.L1017-.L1126
	.long	.L1016-.L1126
	.long	.L1044-.L1126
	.long	.L1014-.L1126
	.long	.L1014-.L1126
	.long	.L1060-.L1126
	.long	.L1071-.L1126
	.text
	.p2align 4,,10
	.p2align 3
.L1026:
	leaq	1(%r10), %rax
	testl	%r11d, %r11d
	je	.L1320
	movq	%r10, %r15
	movl	$1, %r9d
	movq	%rax, %r10
	xorl	%r11d, %r11d
	movl	$21, %eax
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1045:
	leaq	1(%r10), %rax
	cmpl	$3, %r11d
	je	.L1321
	movq	%r10, %r15
	movl	$8, %r9d
	movq	%rax, %r10
	movl	$3, %r11d
	movl	$27, %eax
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1310:
	addw	$1, 10(%r8)
	cmpq	%rcx, %rsi
	jbe	.L1027
	movzbl	1(%rax), %r9d
	cmpb	$32, %r9b
	ja	.L1069
	btq	%r9, %rbx
	jc	.L1006
.L1069:
	movq	%rcx, %rax
	movl	$1, %r11d
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1171:
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L1232:
	movl	$31, %eax
	movl	$5, %r11d
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1228:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1008:
	testl	%edx, %edx
	jne	.L1228
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1029:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	1(%rax), %r10
	cmpl	$3, %r11d
	je	.L1322
	movq	%rax, %rcx
	movl	$1, %r11d
	orw	$8, (%r8)
	subq	%rdi, %rcx
	movw	%r11w, 18(%r8)
	movw	%cx, 16(%r8)
	cmpq	%r10, %rsi
	jbe	.L1027
	movzbl	1(%rax), %r9d
	cmpb	$32, %r9b
	ja	.L1166
	btq	%r9, %rbx
	jc	.L1006
	movl	$3, %r11d
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1050:
	leaq	1(%rax), %r10
	cmpq	%r10, %rsi
	jbe	.L1027
	movzbl	1(%rax), %r9d
	cmpb	$32, %r9b
	ja	.L1060
.L1249:
	btq	%r9, %rbx
	jc	.L1006
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1057:
	addq	$1, %r10
	cmpq	%r10, %rsi
	jbe	.L1027
	movzbl	(%r10), %r9d
	cmpb	$32, %r9b
	jbe	.L1249
	movl	$30, %eax
	jmp	.L1059
.L1309:
	leaq	1(%rax), %r10
	cmpq	%r10, %rsi
	jbe	.L1027
	movzbl	1(%rax), %r9d
	cmpb	$32, %r9b
	ja	.L1014
	btq	%r9, %rbx
	jc	.L1006
	jmp	.L1014
.L1314:
	addw	$1, 22(%r8)
	cmpq	%rcx, %rsi
	jbe	.L1027
	movzbl	1(%rax), %r9d
	cmpb	$32, %r9b
	ja	.L1053
	btq	%r9, %rbx
	jc	.L1006
.L1053:
	movq	%rcx, %rax
	movl	$4, %r11d
	jmp	.L1031
.L1318:
	addw	$1, 26(%r8)
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	1(%r10), %r9d
	cmpb	$32, %r9b
	ja	.L1155
	btq	%r9, %rbx
	jc	.L1006
.L1155:
	movq	%rax, %r10
	jmp	.L1071
.L1320:
	addw	$1, 6(%r8)
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	(%rax), %r9d
	cmpb	$32, %r9b
	ja	.L1070
.L1271:
	btq	%r9, %rbx
	jc	.L1006
.L1070:
	movq	%rax, %r10
	jmp	.L1025
.L1317:
	addw	$1, 26(%r8)
	cmpq	%r10, %rsi
	jbe	.L1027
	movzbl	(%r10), %r9d
	cmpb	$32, %r9b
	ja	.L1071
	btq	%r9, %rbx
	jnc	.L1071
	jmp	.L1006
.L1311:
	leaq	1(%rax), %rcx
	cmpq	%rsi, %rcx
	jnb	.L1006
	movzbl	1(%rax), %eax
	xorl	%r10d, %r10d
	leal	-48(%rax), %r9d
	cmpb	$9, %r9b
	jbe	.L1089
	xorl	%ebx, %ebx
	movl	$5, %r11d
.L1090:
	movl	%eax, %r9d
	orl	$32, %r9d
	subl	$97, %r9d
	cmpb	$5, %r9b
	jbe	.L1095
	cmpb	$58, %al
	je	.L1095
	cmpb	$46, %al
	je	.L1095
	cmpb	$37, %al
	jne	.L1006
	testb	%bl, %bl
	je	.L1006
	addl	$1, %r10d
	leaq	1(%rcx), %rax
	movw	%r10w, 10(%r8)
	cmpq	%rax, %rsi
	jbe	.L1006
	movzbl	1(%rcx), %eax
	addq	$2, %rcx
.L1098:
	movl	%eax, %r9d
	orl	$32, %r9d
	subl	$97, %r9d
	cmpb	$25, %r9b
	jbe	.L1104
	leal	-48(%rax), %r9d
	cmpb	$9, %r9b
	jbe	.L1104
	cmpb	$37, %al
	je	.L1104
	cmpb	$46, %al
	je	.L1104
	cmpb	$45, %al
	sete	%r10b
	cmpb	$95, %al
	sete	%r9b
	orb	%r9b, %r10b
	jne	.L1104
	cmpb	$126, %al
	jne	.L1006
.L1104:
	addw	$1, 10(%r8)
	movq	%rcx, %r9
	cmpq	%rsi, %rcx
	jnb	.L1006
	movzbl	(%rcx), %eax
	addq	$1, %rcx
	cmpb	$93, %al
	jne	.L1098
	leaq	1(%r9), %r10
	cmpq	%rsi, %r10
	jnb	.L1099
	movzbl	1(%r9), %eax
.L1100:
	cmpb	$58, %al
	jne	.L1006
	leaq	1(%r10), %rcx
	cmpq	%rsi, %rcx
	jnb	.L1006
	movzbl	1(%r10), %eax
.L1094:
	movl	$11, %r9d
	jmp	.L1116
.L1111:
	movq	%rcx, %rax
	movl	$1, %r9d
	addq	$1, %rcx
	orw	$4, (%r8)
	subq	%rdi, %rax
	movw	%r9w, 14(%r8)
	movw	%ax, 12(%r8)
	cmpq	%rsi, %rcx
	jnb	.L1099
.L1112:
	movzbl	(%rcx), %eax
	movl	$12, %r9d
.L1116:
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1006
	cmpl	$12, %r9d
	jne	.L1111
	addq	$1, %rcx
	addw	$1, 14(%r8)
	cmpq	%rsi, %rcx
	jb	.L1112
.L1099:
	movzwl	(%r8), %eax
.L1079:
	testl	%edx, %edx
	je	.L1120
	cmpw	$6, %ax
	jne	.L1006
.L1121:
	movzwl	12(%r8), %eax
	movzwl	14(%r8), %ecx
	addq	%rax, %rcx
	addq	%rdi, %rcx
	addq	%rax, %rdi
	cmpq	%rdi, %rcx
	jbe	.L1178
	movsbl	(%rdi), %eax
	subl	$48, %eax
	cltq
	cmpq	$65535, %rax
	jbe	.L1123
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1124:
	leaq	(%rax,%rax,4), %rdx
	movsbl	(%rdi), %eax
	subl	$48, %eax
	cltq
	leaq	(%rax,%rdx,2), %rax
	cmpq	$65535, %rax
	ja	.L1006
.L1123:
	addq	$1, %rdi
	cmpq	%rdi, %rcx
	jne	.L1124
.L1122:
	movw	%ax, 2(%r8)
	xorl	%eax, %eax
	jmp	.L1005
.L1166:
	movl	$27, %eax
	movl	$3, %r11d
	jmp	.L1059
.L1321:
	addw	$1, 18(%r8)
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	(%rax), %r9d
	cmpb	$32, %r9b
	ja	.L1066
	jmp	.L1273
.L1080:
	cmpq	%rax, %rsi
	jbe	.L1006
	movzbl	(%rax), %ecx
	movl	$2, %r9d
	movabsq	$4611686018830024697, %r11
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	%rax, %rcx
	addq	$1, %rax
	orw	$64, (%r8)
	subq	%rdi, %rcx
	movw	%cx, 28(%r8)
	movl	$1, %ecx
	movw	%cx, 30(%r8)
	cmpq	%rsi, %rax
	je	.L1006
.L1114:
	movzbl	(%rax), %ecx
	movl	$3, %r9d
.L1115:
	cmpb	$64, %cl
	je	.L1323
	movl	%ecx, %r10d
	orl	$32, %r10d
	subl	$97, %r10d
	cmpb	$25, %r10b
	jbe	.L1086
	leal	-33(%rcx), %r10d
	cmpb	$62, %r10b
	ja	.L1324
	btq	%r10, %r11
	jnc	.L1006
.L1086:
	cmpl	$3, %r9d
	jne	.L1113
	addq	$1, %rax
	addw	$1, 30(%r8)
	cmpq	%rsi, %rax
	jne	.L1114
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	%rax, %r10
	xorl	%r11d, %r11d
	movl	$21, %eax
	jmp	.L1059
.L1120:
	testb	$4, %al
	jne	.L1121
	xorl	%eax, %eax
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1324:
	cmpb	$126, %cl
	jne	.L1006
	jmp	.L1086
.L1322:
	addw	$1, 18(%r8)
	cmpq	%r10, %rsi
	jbe	.L1027
	movzbl	1(%rax), %r9d
	cmpb	$32, %r9b
	ja	.L1044
	btq	%r9, %rbx
	jc	.L1006
	jmp	.L1044
.L1313:
	addw	$1, 18(%r8)
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	1(%r10), %r9d
	cmpb	$32, %r9b
	ja	.L1066
	jmp	.L1273
.L1308:
	addw	$1, 6(%r8)
	cmpq	%rax, %rsi
	jbe	.L1027
	movzbl	1(%r10), %r9d
	cmpb	$32, %r9b
	ja	.L1070
	jmp	.L1271
.L1319:
	addw	$1, 10(%r8)
	cmpq	%r10, %rsi
	jbe	.L1163
	movzbl	1(%rax), %r9d
	cmpb	$32, %r9b
	ja	.L1233
	btq	%r9, %rbx
	jc	.L1006
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1089:
	movq	%rcx, %rax
	movq	%rcx, %r9
	subq	%rdi, %rax
	movw	%ax, 8(%r8)
.L1125:
	addl	$1, %r10d
	addq	$1, %rcx
	movw	%r10w, 10(%r8)
	cmpq	%rsi, %rcx
	jnb	.L1006
	movzbl	1(%r9), %eax
	cmpb	$93, %al
	je	.L1325
	leal	-48(%rax), %r9d
	cmpb	$9, %r9b
	jbe	.L1179
	movl	$1, %ebx
	movl	$7, %r11d
	jmp	.L1090
.L1095:
	cmpl	$7, %r11d
	jne	.L1089
.L1179:
	movq	%rcx, %r9
	jmp	.L1125
.L1325:
	leaq	2(%r9), %r10
	cmpq	%rsi, %r10
	jnb	.L1099
	movzbl	2(%r9), %eax
	jmp	.L1100
.L1323:
	leaq	1(%rax), %r9
	cmpq	%rsi, %r9
	jnb	.L1006
	movzbl	1(%rax), %ecx
	movq	%r9, %rax
	jmp	.L1085
.L1312:
	cmpb	$58, %r10b
	jne	.L1006
	leaq	2(%rax), %rcx
	cmpq	%rsi, %rcx
	jnb	.L1006
	movzbl	2(%rax), %eax
	jmp	.L1094
.L1178:
	xorl	%eax, %eax
	jmp	.L1122
.L1163:
	movl	$1, %r14d
	jmp	.L1027
	.cfi_endproc
.LFE26:
	.size	http_parser_parse_url, .-http_parser_parse_url
	.p2align 4
	.globl	http_parser_pause
	.type	http_parser_pause, @function
http_parser_pause:
.LFB27:
	.cfi_startproc
	endbr64
	movzbl	31(%rdi), %eax
	testb	$95, %al
	jne	.L1326
	testl	%esi, %esi
	setne	%dl
	andl	$-128, %eax
	sall	$5, %edx
	orl	%edx, %eax
	movb	%al, 31(%rdi)
.L1326:
	ret
	.cfi_endproc
.LFE27:
	.size	http_parser_pause, .-http_parser_pause
	.p2align 4
	.globl	http_body_is_final
	.type	http_body_is_final, @function
http_body_is_final:
.LFB28:
	.cfi_startproc
	endbr64
	movzwl	(%rdi), %eax
	andw	$508, %ax
	cmpw	$256, %ax
	sete	%al
	movzbl	%al, %eax
	ret
	.cfi_endproc
.LFE28:
	.size	http_body_is_final, .-http_body_is_final
	.p2align 4
	.globl	http_parser_version
	.type	http_parser_version, @function
http_parser_version:
.LFB29:
	.cfi_startproc
	endbr64
	movl	$133379, %eax
	ret
	.cfi_endproc
.LFE29:
	.size	http_parser_version, .-http_parser_version
	.p2align 4
	.globl	http_parser_set_max_header_size
	.type	http_parser_set_max_header_size, @function
http_parser_set_max_header_size:
.LFB30:
	.cfi_startproc
	endbr64
	movl	%edi, max_header_size(%rip)
	ret
	.cfi_endproc
.LFE30:
	.size	http_parser_set_max_header_size, .-http_parser_set_max_header_size
	.section	.rodata.str1.1
.LC69:
	.string	"HPE_OK"
.LC70:
	.string	"success"
.LC71:
	.string	"HPE_CB_message_begin"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"the on_message_begin callback failed"
	.section	.rodata.str1.1
.LC73:
	.string	"HPE_CB_url"
.LC74:
	.string	"the on_url callback failed"
.LC75:
	.string	"HPE_CB_header_field"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"the on_header_field callback failed"
	.section	.rodata.str1.1
.LC77:
	.string	"HPE_CB_header_value"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"the on_header_value callback failed"
	.section	.rodata.str1.1
.LC79:
	.string	"HPE_CB_headers_complete"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"the on_headers_complete callback failed"
	.section	.rodata.str1.1
.LC81:
	.string	"HPE_CB_body"
.LC82:
	.string	"the on_body callback failed"
.LC83:
	.string	"HPE_CB_message_complete"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"the on_message_complete callback failed"
	.section	.rodata.str1.1
.LC85:
	.string	"HPE_CB_status"
.LC86:
	.string	"the on_status callback failed"
.LC87:
	.string	"HPE_CB_chunk_header"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"the on_chunk_header callback failed"
	.section	.rodata.str1.1
.LC89:
	.string	"HPE_CB_chunk_complete"
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"the on_chunk_complete callback failed"
	.section	.rodata.str1.1
.LC91:
	.string	"HPE_INVALID_EOF_STATE"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"stream ended at an unexpected time"
	.section	.rodata.str1.1
.LC93:
	.string	"HPE_HEADER_OVERFLOW"
	.section	.rodata.str1.8
	.align 8
.LC94:
	.string	"too many header bytes seen; overflow detected"
	.section	.rodata.str1.1
.LC95:
	.string	"HPE_CLOSED_CONNECTION"
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"data received after completed connection: close message"
	.section	.rodata.str1.1
.LC97:
	.string	"HPE_INVALID_VERSION"
.LC98:
	.string	"invalid HTTP version"
.LC99:
	.string	"HPE_INVALID_STATUS"
.LC100:
	.string	"invalid HTTP status code"
.LC101:
	.string	"HPE_INVALID_METHOD"
.LC102:
	.string	"invalid HTTP method"
.LC103:
	.string	"HPE_INVALID_URL"
.LC104:
	.string	"invalid URL"
.LC105:
	.string	"HPE_INVALID_HOST"
.LC106:
	.string	"invalid host"
.LC107:
	.string	"HPE_INVALID_PORT"
.LC108:
	.string	"invalid port"
.LC109:
	.string	"HPE_INVALID_PATH"
.LC110:
	.string	"invalid path"
.LC111:
	.string	"HPE_INVALID_QUERY_STRING"
.LC112:
	.string	"invalid query string"
.LC113:
	.string	"HPE_INVALID_FRAGMENT"
.LC114:
	.string	"invalid fragment"
.LC115:
	.string	"HPE_LF_EXPECTED"
.LC116:
	.string	"LF character expected"
.LC117:
	.string	"HPE_INVALID_HEADER_TOKEN"
.LC118:
	.string	"invalid character in header"
.LC119:
	.string	"HPE_INVALID_CONTENT_LENGTH"
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"invalid character in content-length header"
	.section	.rodata.str1.1
.LC121:
	.string	"HPE_UNEXPECTED_CONTENT_LENGTH"
	.section	.rodata.str1.8
	.align 8
.LC122:
	.string	"unexpected content-length header"
	.section	.rodata.str1.1
.LC123:
	.string	"HPE_INVALID_CHUNK_SIZE"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"invalid character in chunk size header"
	.section	.rodata.str1.1
.LC125:
	.string	"HPE_INVALID_TRANSFER_ENCODING"
	.section	.rodata.str1.8
	.align 8
.LC126:
	.string	"request has invalid transfer-encoding"
	.section	.rodata.str1.1
.LC127:
	.string	"HPE_INVALID_CONSTANT"
.LC128:
	.string	"invalid constant string"
.LC129:
	.string	"HPE_INVALID_INTERNAL_STATE"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"encountered unexpected internal state"
	.section	.rodata.str1.1
.LC131:
	.string	"HPE_STRICT"
.LC132:
	.string	"strict mode assertion failed"
.LC133:
	.string	"HPE_PAUSED"
.LC134:
	.string	"parser is paused"
.LC135:
	.string	"HPE_UNKNOWN"
.LC136:
	.string	"an unknown error occurred"
	.section	.data.rel.ro.local,"aw"
	.align 32
	.type	http_strerror_tab, @object
	.size	http_strerror_tab, 544
http_strerror_tab:
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.quad	.LC136
	.section	.rodata
	.align 32
	.type	normal_url_char, @object
	.size	normal_url_char, 32
normal_url_char:
	.string	""
	.string	"\022"
	.string	""
	.string	"\366\377\377\177\377\377\377\377\377\377\377\177"
	.zero	15
	.align 32
	.type	unhex, @object
	.size	unhex, 256
unhex:
	.string	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.string	"\001\002\003\004\005\006\007\b\t\377\377\377\377\377\377\377\n\013\f\r\016\017\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\n\013\f\r\016\017\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.zero	127
	.align 32
	.type	tokens, @object
	.size	tokens, 256
tokens:
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	" !"
	.string	"#$%&'"
	.string	""
	.string	"*+"
	.string	"-."
	.string	"0123456789"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"abcdefghijklmnopqrstuvwxyz"
	.string	""
	.string	""
	.string	"^_`abcdefghijklmnopqrstuvwxyz"
	.string	"|"
	.string	"~"
	.string	""
	.zero	127
	.section	.rodata.str1.1
.LC137:
	.string	"DELETE"
.LC138:
	.string	"GET"
.LC139:
	.string	"HEAD"
.LC140:
	.string	"POST"
.LC141:
	.string	"PUT"
.LC142:
	.string	"CONNECT"
.LC143:
	.string	"OPTIONS"
.LC144:
	.string	"TRACE"
.LC145:
	.string	"COPY"
.LC146:
	.string	"LOCK"
.LC147:
	.string	"MKCOL"
.LC148:
	.string	"MOVE"
.LC149:
	.string	"PROPFIND"
.LC150:
	.string	"PROPPATCH"
.LC151:
	.string	"SEARCH"
.LC152:
	.string	"UNLOCK"
.LC153:
	.string	"BIND"
.LC154:
	.string	"REBIND"
.LC155:
	.string	"UNBIND"
.LC156:
	.string	"ACL"
.LC157:
	.string	"REPORT"
.LC158:
	.string	"MKACTIVITY"
.LC159:
	.string	"CHECKOUT"
.LC160:
	.string	"MERGE"
.LC161:
	.string	"M-SEARCH"
.LC162:
	.string	"NOTIFY"
.LC163:
	.string	"SUBSCRIBE"
.LC164:
	.string	"UNSUBSCRIBE"
.LC165:
	.string	"PATCH"
.LC166:
	.string	"PURGE"
.LC167:
	.string	"MKCALENDAR"
.LC168:
	.string	"LINK"
.LC169:
	.string	"UNLINK"
.LC170:
	.string	"SOURCE"
	.section	.data.rel.ro.local
	.align 32
	.type	method_strings, @object
	.size	method_strings, 272
method_strings:
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.quad	.LC148
	.quad	.LC149
	.quad	.LC150
	.quad	.LC151
	.quad	.LC152
	.quad	.LC153
	.quad	.LC154
	.quad	.LC155
	.quad	.LC156
	.quad	.LC157
	.quad	.LC158
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.quad	.LC162
	.quad	.LC163
	.quad	.LC164
	.quad	.LC165
	.quad	.LC166
	.quad	.LC167
	.quad	.LC168
	.quad	.LC169
	.quad	.LC170
	.data
	.align 4
	.type	max_header_size, @object
	.size	max_header_size, 4
max_header_size:
	.long	8192
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
