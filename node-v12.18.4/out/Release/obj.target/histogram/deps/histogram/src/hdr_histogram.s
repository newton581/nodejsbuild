	.file	"hdr_histogram.c"
	.text
	.p2align 4
	.type	move_next, @function
move_next:
.LFB98:
	.cfi_startproc
	movl	8(%rdi), %eax
	movq	(%rdi), %rsi
	leal	1(%rax), %edx
	xorl	%eax, %eax
	movl	%edx, 8(%rdi)
	cmpl	80(%rsi), %edx
	jge	.L1
	movl	64(%rsi), %ecx
	movl	%edx, %eax
	testl	%ecx, %ecx
	je	.L3
	movl	80(%rsi), %r8d
	subl	%ecx, %eax
	js	.L13
	movl	%eax, %ecx
	subl	%r8d, %ecx
	cmpl	%r8d, %eax
	cmovge	%ecx, %eax
.L3:
	movq	96(%rsi), %rcx
	cltq
	movl	24(%rsi), %r8d
	movl	28(%rsi), %r9d
	movq	(%rcx,%rax,8), %rax
	movq	24(%rdi), %rcx
	addq	%rax, %rcx
	movq	%rax, %xmm0
	movl	%edx, %eax
	movq	%rcx, %xmm1
	movl	%r8d, %ecx
	sarl	%cl, %eax
	punpcklqdq	%xmm1, %xmm0
	movl	%eax, %ecx
	leal	-1(%r9), %eax
	movups	%xmm0, 16(%rdi)
	andl	%edx, %eax
	subl	$1, %ecx
	js	.L10
	addl	%r9d, %eax
.L6:
	movl	16(%rsi), %r9d
	cltq
	movl	$64, %edx
	addl	$1, %r8d
	addl	%r9d, %ecx
	salq	%cl, %rax
	movq	32(%rsi), %rcx
	movq	%rax, 32(%rdi)
	orq	%rax, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %edx
	subl	%r9d, %edx
	subl	%r8d, %edx
	leal	(%rdx,%r9), %ecx
	sarq	%cl, %rax
	movslq	%eax, %r8
	salq	%cl, %r8
	cmpl	40(%rsi), %eax
	jl	.L7
	leal	1(%rdx,%r9), %ecx
.L7:
	movl	$1, %eax
	movq	%r8, 48(%rdi)
	salq	%cl, %rax
	leaq	-1(%r8,%rax), %rdx
	sarq	%rax
	movq	%rdx, 40(%rdi)
	addq	%r8, %rax
	movq	%rax, 56(%rdi)
	movl	$1, %eax
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%ecx, %ecx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L13:
	addl	%r8d, %eax
	jmp	.L3
	.cfi_endproc
.LFE98:
	.size	move_next, .-move_next
	.p2align 4
	.type	_percentile_iter_next, @function
_percentile_iter_next:
.LFB106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rax
	movslq	12(%rdi), %rdx
	cmpq	%rdx, %rax
	jl	.L15
	xorl	%eax, %eax
	cmpb	$0, 80(%rdi)
	je	.L29
.L14:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	.LC0(%rip), %rax
	movb	$1, 80(%rdi)
	movq	%rax, 96(%rdi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	cmpl	$-1, 8(%rdi)
	movq	(%rdi), %rdx
	je	.L30
.L21:
	cmpq	$0, 16(%rbx)
	je	.L18
	pxor	%xmm0, %xmm0
	pxor	%xmm2, %xmm2
	movsd	88(%rbx), %xmm1
	cvtsi2sdq	%rax, %xmm0
	mulsd	.LC0(%rip), %xmm0
	cvtsi2sdq	88(%rdx), %xmm2
	divsd	%xmm2, %xmm0
	comisd	%xmm1, %xmm0
	jnb	.L31
.L18:
	movslq	12(%rbx), %rcx
	cmpq	%rax, %rcx
	jle	.L25
	movl	80(%rdx), %eax
	cmpl	%eax, 8(%rbx)
	jl	.L28
.L25:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	80(%rdx), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L14
.L28:
	movq	%rbx, %rdi
	call	move_next
	movq	24(%rbx), %rax
	movq	(%rbx), %rdx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L31:
	movq	32(%rbx), %rsi
	movq	32(%rdx), %rcx
	movl	$64, %eax
	movl	16(%rdx), %r8d
	orq	%rsi, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %eax
	movl	24(%rdx), %ecx
	subl	%r8d, %eax
	addl	$1, %ecx
	subl	%ecx, %eax
	leal	(%r8,%rax), %ecx
	sarq	%cl, %rsi
	movslq	%esi, %rdi
	salq	%cl, %rdi
	cmpl	40(%rdx), %esi
	jl	.L20
	leal	1(%r8,%rax), %ecx
.L20:
	movl	$1, %eax
	movq	72(%rbx), %xmm0
	movsd	%xmm1, 96(%rbx)
	movsd	.LC0(%rip), %xmm3
	salq	%cl, %rax
	leaq	-1(%rdi,%rax), %rax
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 64(%rbx)
	movsd	.LC0(%rip), %xmm0
	subsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm0
	call	log@PLT
	divsd	.LC1(%rip), %xmm0
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC2(%rip), %xmm0
	addq	$1, %rax
	cvtsi2sdq	%rax, %xmm1
	call	pow@PLT
	movslq	84(%rbx), %rdx
	movsd	.LC0(%rip), %xmm4
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm0, %xmm0
	imulq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	movl	$1, %eax
	divsd	%xmm0, %xmm4
	movsd	88(%rbx), %xmm0
	addsd	%xmm4, %xmm0
	movsd	%xmm0, 88(%rbx)
	jmp	.L14
	.cfi_endproc
.LFE106:
	.size	_percentile_iter_next, .-_percentile_iter_next
	.p2align 4
	.type	_iter_linear_next, @function
_iter_linear_next:
.LFB111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	$0, 88(%rdi)
	movq	24(%rdi), %rax
	movq	104(%rdi), %r14
	movq	(%rdi), %rsi
	movq	%rax, -56(%rbp)
	movq	%rax, %rbx
	movslq	12(%rdi), %rax
	cmpq	%rbx, %rax
	jg	.L33
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	cmpl	80(%rsi), %edx
	jge	.L32
	movl	24(%rsi), %ecx
	leal	1(%rdx), %r8d
	movl	%r8d, %edx
	sarl	%cl, %edx
	movl	28(%rsi), %ecx
	leal	-1(%rcx), %eax
	andl	%r8d, %eax
	subl	$1, %edx
	js	.L50
	addl	%ecx, %eax
.L44:
	addl	16(%rsi), %edx
	cltq
	movl	%edx, %ecx
	salq	%cl, %rax
	movq	%rax, %rdx
	xorl	%eax, %eax
	cmpq	%r14, %rdx
	jle	.L32
.L33:
	cmpq	%r14, 32(%rdi)
	jge	.L56
	movl	8(%rdi), %eax
	movl	80(%rsi), %r15d
	xorl	%ebx, %ebx
	leal	1(%rax), %edx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L58:
	movl	64(%rsi), %ecx
	movl	%edx, %eax
	testl	%ecx, %ecx
	je	.L36
	movl	80(%rsi), %r8d
	subl	%ecx, %eax
	js	.L57
	movl	%eax, %ecx
	subl	%r8d, %ecx
	cmpl	%r8d, %eax
	cmovge	%ecx, %eax
.L36:
	movq	96(%rsi), %rcx
	cltq
	movl	24(%rsi), %r10d
	movl	28(%rsi), %r8d
	movq	(%rcx,%rax,8), %r9
	addq	%r9, -56(%rbp)
	movl	%r10d, %ecx
	movl	%edx, %eax
	sarl	%cl, %eax
	movq	%r9, %xmm0
	movl	%eax, %ecx
	leal	-1(%r8), %eax
	andl	%edx, %eax
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, 16(%rdi)
	subl	$1, %ecx
	js	.L48
	addl	%r8d, %eax
.L39:
	movl	16(%rsi), %r11d
	cltq
	movl	$64, %r12d
	addl	$1, %r10d
	addl	%r11d, %ecx
	salq	%cl, %rax
	movq	32(%rsi), %rcx
	movq	%rax, %r13
	movq	%rax, 32(%rdi)
	orq	%rax, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %r12d
	subl	%r11d, %r12d
	subl	%r10d, %r12d
	leal	(%r11,%r12), %ecx
	sarq	%cl, %r13
	movslq	%r13d, %r8
	salq	%cl, %r8
	cmpl	40(%rsi), %r13d
	jl	.L40
	leal	1(%r11,%r12), %ecx
.L40:
	movl	$1, %r11d
	movq	%r8, 48(%rdi)
	addq	%r9, %rbx
	addl	$1, %edx
	salq	%cl, %r11
	movq	%rbx, 88(%rdi)
	movq	%r11, %rcx
	leaq	-1(%r8,%r11), %r11
	sarq	%rcx
	movq	%r11, 40(%rdi)
	addq	%rcx, %r8
	movq	%r8, 56(%rdi)
	cmpq	%r14, %rax
	jge	.L41
.L43:
	movl	%edx, 8(%rdi)
	cmpl	%r15d, %edx
	jl	.L58
	movl	$1, %eax
.L32:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	xorl	%ecx, %ecx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L57:
	addl	%r8d, %eax
	jmp	.L36
.L56:
	movl	24(%rsi), %eax
	leal	1(%rax), %r10d
.L41:
	movq	96(%rdi), %rax
	movq	72(%rdi), %rdx
	movl	$64, %ecx
	movq	%rax, 72(%rdi)
	addq	80(%rdi), %rax
	movq	%rdx, 64(%rdi)
	movq	32(%rsi), %rdx
	movq	%rax, 96(%rdi)
	orq	%rax, %rdx
	bsrq	%rdx, %rdx
	xorq	$63, %rdx
	subl	%edx, %ecx
	subl	%r10d, %ecx
	sarq	%cl, %rax
	cltq
	salq	%cl, %rax
	movq	%rax, 104(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L44
	.cfi_endproc
.LFE111:
	.size	_iter_linear_next, .-_iter_linear_next
	.p2align 4
	.type	_log_iter_next, @function
_log_iter_next:
.LFB113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	$0, 88(%rdi)
	movq	24(%rdi), %rax
	movq	104(%rdi), %r14
	movq	(%rdi), %rsi
	movq	%rax, -56(%rbp)
	movq	%rax, %rbx
	movslq	12(%rdi), %rax
	cmpq	%rbx, %rax
	jg	.L60
	movl	8(%rdi), %edx
	xorl	%eax, %eax
	cmpl	80(%rsi), %edx
	jge	.L59
	movl	24(%rsi), %ecx
	leal	1(%rdx), %r8d
	movl	%r8d, %edx
	sarl	%cl, %edx
	movl	28(%rsi), %ecx
	leal	-1(%rcx), %eax
	andl	%r8d, %eax
	subl	$1, %edx
	js	.L77
	addl	%ecx, %eax
.L71:
	addl	16(%rsi), %edx
	cltq
	movl	%edx, %ecx
	salq	%cl, %rax
	movq	%rax, %rdx
	xorl	%eax, %eax
	cmpq	%r14, %rdx
	jle	.L59
.L60:
	cmpq	%r14, 32(%rdi)
	jge	.L83
	movl	8(%rdi), %eax
	movl	80(%rsi), %r15d
	xorl	%ebx, %ebx
	leal	1(%rax), %edx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L85:
	movl	64(%rsi), %ecx
	movl	%edx, %eax
	testl	%ecx, %ecx
	je	.L63
	movl	80(%rsi), %r8d
	subl	%ecx, %eax
	js	.L84
	movl	%eax, %ecx
	subl	%r8d, %ecx
	cmpl	%r8d, %eax
	cmovge	%ecx, %eax
.L63:
	movq	96(%rsi), %rcx
	cltq
	movl	24(%rsi), %r10d
	movl	28(%rsi), %r8d
	movq	(%rcx,%rax,8), %r9
	addq	%r9, -56(%rbp)
	movl	%r10d, %ecx
	movl	%edx, %eax
	sarl	%cl, %eax
	movq	%r9, %xmm0
	movl	%eax, %ecx
	leal	-1(%r8), %eax
	andl	%edx, %eax
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, 16(%rdi)
	subl	$1, %ecx
	js	.L75
	addl	%r8d, %eax
.L66:
	movl	16(%rsi), %r11d
	cltq
	movl	$64, %r12d
	addl	$1, %r10d
	addl	%r11d, %ecx
	salq	%cl, %rax
	movq	32(%rsi), %rcx
	movq	%rax, %r13
	movq	%rax, 32(%rdi)
	orq	%rax, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %r12d
	subl	%r11d, %r12d
	subl	%r10d, %r12d
	leal	(%r11,%r12), %ecx
	sarq	%cl, %r13
	movslq	%r13d, %r8
	salq	%cl, %r8
	cmpl	40(%rsi), %r13d
	jl	.L67
	leal	1(%r11,%r12), %ecx
.L67:
	movl	$1, %r11d
	movq	%r8, 48(%rdi)
	addq	%r9, %rbx
	addl	$1, %edx
	salq	%cl, %r11
	movq	%rbx, 88(%rdi)
	movq	%r11, %rcx
	leaq	-1(%r8,%r11), %r11
	sarq	%rcx
	movq	%r11, 40(%rdi)
	addq	%rcx, %r8
	movq	%r8, 56(%rdi)
	cmpq	%r14, %rax
	jge	.L68
.L70:
	movl	%edx, 8(%rdi)
	cmpl	%r15d, %edx
	jl	.L85
	movl	$1, %eax
.L59:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	xorl	%ecx, %ecx
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L84:
	addl	%r8d, %eax
	jmp	.L63
.L83:
	movl	24(%rsi), %eax
	leal	1(%rax), %r10d
.L68:
	movq	72(%rdi), %xmm0
	cvttsd2siq	80(%rdi), %rax
	movl	$64, %ecx
	imulq	96(%rdi), %rax
	movhps	96(%rdi), %xmm0
	movups	%xmm0, 64(%rdi)
	movq	32(%rsi), %rdx
	movq	%rax, 96(%rdi)
	orq	%rax, %rdx
	bsrq	%rdx, %rdx
	xorq	$63, %rdx
	subl	%edx, %ecx
	subl	%r10d, %ecx
	sarq	%cl, %rax
	cltq
	salq	%cl, %rax
	movq	%rax, 104(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L71
	.cfi_endproc
.LFE113:
	.size	_log_iter_next, .-_log_iter_next
	.p2align 4
	.type	_all_values_iter_next, @function
_all_values_iter_next:
.LFB103:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	movq	(%rdi), %r8
	xorl	%r9d, %r9d
	addl	$1, %eax
	movl	%eax, 8(%rdi)
	cmpl	80(%r8), %eax
	jge	.L86
	movl	64(%r8), %ecx
	movl	%eax, %edx
	testl	%ecx, %ecx
	je	.L88
	movl	80(%r8), %esi
	subl	%ecx, %edx
	js	.L97
	movl	%edx, %ecx
	subl	%esi, %ecx
	cmpl	%esi, %edx
	cmovge	%ecx, %edx
.L88:
	movq	96(%r8), %rcx
	movslq	%edx, %rdx
	movl	%eax, %esi
	movl	28(%r8), %r9d
	movq	(%rcx,%rdx,8), %rdx
	movq	24(%rdi), %rcx
	movq	%rdx, %xmm0
	addq	%rdx, %rcx
	movl	24(%r8), %edx
	movq	%rcx, %xmm1
	movl	%edx, %ecx
	punpcklqdq	%xmm1, %xmm0
	sarl	%cl, %esi
	movups	%xmm0, 16(%rdi)
	movl	%esi, %ecx
	leal	-1(%r9), %esi
	andl	%esi, %eax
	subl	$1, %ecx
	js	.L95
	addl	%r9d, %eax
.L91:
	movl	16(%r8), %r9d
	cltq
	movl	$64, %esi
	addl	$1, %edx
	addl	%r9d, %ecx
	salq	%cl, %rax
	movq	32(%r8), %rcx
	movq	%rax, %r11
	movq	%rax, 32(%rdi)
	orq	%rax, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %esi
	subl	%r9d, %esi
	subl	%edx, %esi
	leal	(%r9,%rsi), %ecx
	sarq	%cl, %r11
	movslq	%r11d, %rdx
	salq	%cl, %rdx
	movq	%rdx, %r10
	cmpl	40(%r8), %r11d
	jl	.L92
	leal	1(%r9,%rsi), %ecx
.L92:
	movl	$1, %edx
	movq	%r10, 48(%rdi)
	movl	$1, %r9d
	salq	%cl, %rdx
	leaq	-1(%r10,%rdx), %rcx
	sarq	%rdx
	addq	%r10, %rdx
	movq	%rcx, 40(%rdi)
	movq	%rdx, 56(%rdi)
	movq	72(%rdi), %rdx
	movq	%rax, 72(%rdi)
	movq	%rdx, 64(%rdi)
.L86:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%ecx, %ecx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L97:
	addl	%esi, %edx
	jmp	.L88
	.cfi_endproc
.LFE103:
	.size	_all_values_iter_next, .-_all_values_iter_next
	.p2align 4
	.type	_recorded_iter_next, @function
_recorded_iter_next:
.LFB109:
	.cfi_startproc
	endbr64
	movslq	12(%rdi), %r9
	movq	24(%rdi), %r8
	movl	$64, %r11d
	movl	$1, %r10d
	cmpq	%r8, %r9
	jle	.L118
	movq	(%rdi), %rsi
	movl	8(%rdi), %eax
	movl	80(%rsi), %edx
	cmpl	%edx, %eax
	jl	.L122
.L118:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
.L119:
	addl	$1, %eax
	movl	%eax, 8(%rdi)
	cmpl	%eax, %edx
	jle	.L123
	movl	64(%rsi), %ecx
	movl	%eax, %edx
	testl	%ecx, %ecx
	je	.L103
	movl	80(%rsi), %ebx
	subl	%ecx, %edx
	js	.L124
	movl	%edx, %ecx
	subl	%ebx, %ecx
	cmpl	%ebx, %edx
	cmovge	%ecx, %edx
.L103:
	movq	96(%rsi), %rcx
	movslq	%edx, %rdx
	movl	24(%rsi), %ebx
	movl	%eax, %r12d
	movl	28(%rsi), %r13d
	movq	(%rcx,%rdx,8), %rdx
	movl	%ebx, %ecx
	sarl	%cl, %r12d
	addq	%rdx, %r8
	movq	%rdx, %xmm0
	movl	%r12d, %ecx
	movq	%r8, %xmm1
	leal	-1(%r13), %r12d
	punpcklqdq	%xmm1, %xmm0
	andl	%r12d, %eax
	movups	%xmm0, 16(%rdi)
	subl	$1, %ecx
	js	.L112
	addl	%r13d, %eax
.L106:
	movl	16(%rsi), %r12d
	cltq
	movl	%r11d, %r13d
	addl	%r12d, %ecx
	salq	%cl, %rax
	movq	32(%rsi), %rcx
	movq	%rax, 32(%rdi)
	orq	%rax, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %r13d
	leal	1(%rbx), %ecx
	subl	%r12d, %r13d
	movl	%r13d, %ebx
	subl	%ecx, %ebx
	leal	(%r12,%rbx), %ecx
	sarq	%cl, %rax
	movslq	%eax, %r13
	salq	%cl, %r13
	cmpl	40(%rsi), %eax
	jl	.L107
	leal	1(%r12,%rbx), %ecx
.L107:
	movq	%r10, %rax
	movq	%r13, 48(%rdi)
	salq	%cl, %rax
	leaq	-1(%r13,%rax), %rcx
	sarq	%rax
	addq	%r13, %rax
	movq	%rcx, 40(%rdi)
	movq	%rax, 56(%rdi)
.L102:
	testq	%rdx, %rdx
	jne	.L125
	cmpq	%r8, %r9
	jle	.L110
	movq	(%rdi), %rsi
	movl	8(%rdi), %eax
	movl	80(%rsi), %edx
	cmpl	%edx, %eax
	jl	.L119
.L110:
	xorl	%eax, %eax
.L98:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	xorl	%ecx, %ecx
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L124:
	addl	%ebx, %edx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L123:
	movq	16(%rdi), %rdx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L125:
	movdqu	64(%rdi), %xmm0
	movdqu	32(%rdi), %xmm2
	movq	%rdx, 80(%rdi)
	movl	$1, %eax
	shufpd	$1, %xmm2, %xmm0
	movups	%xmm0, 64(%rdi)
	jmp	.L98
	.cfi_endproc
.LFE109:
	.size	_recorded_iter_next, .-_recorded_iter_next
	.p2align 4
	.globl	counts_index_for
	.hidden	counts_index_for
	.type	counts_index_for, @function
counts_index_for:
.LFB64:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movl	16(%rdi), %ecx
	movl	$64, %edx
	movl	24(%rdi), %r8d
	orq	%rsi, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	leal	1(%r8), %eax
	subl	%ecx, %edx
	subl	%eax, %edx
	addl	%edx, %ecx
	addl	$1, %edx
	sarq	%cl, %rsi
	movl	%r8d, %ecx
	subl	28(%rdi), %esi
	sall	%cl, %edx
	leal	(%rsi,%rdx), %eax
	ret
	.cfi_endproc
.LFE64:
	.size	counts_index_for, .-counts_index_for
	.p2align 4
	.globl	hdr_value_at_index
	.hidden	hdr_value_at_index
	.type	hdr_value_at_index, @function
hdr_value_at_index:
.LFB65:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %ecx
	movl	%esi, %edx
	movl	%esi, %eax
	sarl	%cl, %edx
	movl	%edx, %ecx
	movl	28(%rdi), %edx
	leal	-1(%rdx), %esi
	andl	%eax, %esi
	subl	$1, %ecx
	js	.L129
	addl	%edx, %esi
	addl	16(%rdi), %ecx
	movslq	%esi, %rax
	salq	%cl, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	xorl	%ecx, %ecx
	movslq	%esi, %rax
	addl	16(%rdi), %ecx
	salq	%cl, %rax
	ret
	.cfi_endproc
.LFE65:
	.size	hdr_value_at_index, .-hdr_value_at_index
	.p2align 4
	.globl	hdr_size_of_equivalent_value_range
	.hidden	hdr_size_of_equivalent_value_range
	.type	hdr_size_of_equivalent_value_range, @function
hdr_size_of_equivalent_value_range:
.LFB66:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	movl	16(%rdi), %r8d
	movl	$64, %eax
	movl	24(%rdi), %ecx
	orq	%rsi, %rdx
	bsrq	%rdx, %rdx
	xorq	$63, %rdx
	subl	%edx, %eax
	leal	1(%rcx), %edx
	subl	%r8d, %eax
	subl	%edx, %eax
	leal	(%r8,%rax), %ecx
	sarq	%cl, %rsi
	cmpl	%esi, 40(%rdi)
	jg	.L131
	leal	1(%r8,%rax), %ecx
.L131:
	movl	$1, %eax
	salq	%cl, %rax
	ret
	.cfi_endproc
.LFE66:
	.size	hdr_size_of_equivalent_value_range, .-hdr_size_of_equivalent_value_range
	.p2align 4
	.globl	hdr_next_non_equivalent_value
	.hidden	hdr_next_non_equivalent_value
	.type	hdr_next_non_equivalent_value, @function
hdr_next_non_equivalent_value:
.LFB68:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movl	$64, %edx
	movl	16(%rdi), %r9d
	orq	%rsi, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movl	24(%rdi), %eax
	subl	%r9d, %edx
	addl	$1, %eax
	subl	%eax, %edx
	leal	(%r9,%rdx), %ecx
	sarq	%cl, %rsi
	movslq	%esi, %rax
	salq	%cl, %rax
	movq	%rax, %r8
	cmpl	%esi, 40(%rdi)
	jg	.L133
	leal	1(%r9,%rdx), %ecx
.L133:
	movl	$1, %eax
	salq	%cl, %rax
	addq	%r8, %rax
	ret
	.cfi_endproc
.LFE68:
	.size	hdr_next_non_equivalent_value, .-hdr_next_non_equivalent_value
	.p2align 4
	.globl	hdr_median_equivalent_value
	.hidden	hdr_median_equivalent_value
	.type	hdr_median_equivalent_value, @function
hdr_median_equivalent_value:
.LFB70:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movl	$64, %edx
	movl	16(%rdi), %r9d
	orq	%rsi, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movl	24(%rdi), %eax
	subl	%r9d, %edx
	addl	$1, %eax
	subl	%eax, %edx
	leal	(%r9,%rdx), %ecx
	sarq	%cl, %rsi
	movslq	%esi, %rax
	salq	%cl, %rax
	movq	%rax, %r8
	cmpl	%esi, 40(%rdi)
	jg	.L135
	leal	1(%r9,%rdx), %ecx
.L135:
	movl	$1, %eax
	salq	%cl, %rax
	sarq	%rax
	addq	%r8, %rax
	ret
	.cfi_endproc
.LFE70:
	.size	hdr_median_equivalent_value, .-hdr_median_equivalent_value
	.p2align 4
	.globl	hdr_reset_internal_counters
	.hidden	hdr_reset_internal_counters
	.type	hdr_reset_internal_counters, @function
hdr_reset_internal_counters:
.LFB72:
	.cfi_startproc
	endbr64
	movl	80(%rdi), %eax
	testl	%eax, %eax
	jle	.L157
	leal	-1(%rax), %r9d
	movq	96(%rdi), %r10
	xorl	%eax, %eax
	xorl	%esi, %esi
	movl	$-1, %r11d
	movl	$-1, %r8d
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L149:
	movl	%eax, %r11d
	movl	%eax, %r8d
.L139:
	leaq	1(%rax), %rdx
	cmpq	%rax, %r9
	je	.L158
.L150:
	movq	%rdx, %rax
.L141:
	movq	(%r10,%rax,8), %rdx
	movl	%eax, %ecx
	testq	%rdx, %rdx
	jle	.L139
	addq	%rdx, %rsi
	testl	%eax, %eax
	je	.L153
	cmpl	$-1, %r8d
	je	.L149
.L153:
	movl	%ecx, %r11d
	leaq	1(%rax), %rdx
	cmpq	%rax, %r9
	jne	.L150
.L158:
	cmpl	$-1, %r11d
	je	.L159
	movl	24(%rdi), %edx
	movl	%r11d, %eax
	movl	28(%rdi), %r9d
	movl	%edx, %ecx
	sarl	%cl, %eax
	movl	%eax, %ecx
	leal	-1(%r9), %eax
	andl	%eax, %r11d
	subl	$1, %ecx
	js	.L151
	addl	%r9d, %r11d
.L144:
	movl	16(%rdi), %r9d
	movslq	%r11d, %r11
	movl	$64, %eax
	addl	$1, %edx
	addl	%r9d, %ecx
	salq	%cl, %r11
	movq	32(%rdi), %rcx
	orq	%r11, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %eax
	subl	%r9d, %eax
	subl	%edx, %eax
	leal	(%rax,%r9), %ecx
	sarq	%cl, %r11
	movslq	%r11d, %rdx
	salq	%cl, %rdx
	cmpl	40(%rdi), %r11d
	jl	.L145
	leal	1(%rax,%r9), %ecx
.L145:
	movl	$1, %eax
	salq	%cl, %rax
	leaq	-1(%rdx,%rax), %rax
	movq	%rax, 56(%rdi)
.L143:
	cmpl	$-1, %r8d
	je	.L138
	movl	24(%rdi), %ecx
	movl	%r8d, %eax
	movl	28(%rdi), %edx
	sarl	%cl, %eax
	movl	%eax, %ecx
	leal	-1(%rdx), %eax
	andl	%eax, %r8d
	subl	$1, %ecx
	js	.L152
	addl	%edx, %r8d
.L148:
	movslq	%r8d, %r8
	addl	16(%rdi), %ecx
	movq	%rsi, 88(%rdi)
	salq	%cl, %r8
	movq	%r8, 48(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%ecx, %ecx
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L157:
	movq	$0, 56(%rdi)
	xorl	%esi, %esi
.L138:
	movabsq	$9223372036854775807, %rax
	movq	%rsi, 88(%rdi)
	movq	%rax, 48(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	xorl	%ecx, %ecx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L159:
	movq	$0, 56(%rdi)
	jmp	.L143
	.cfi_endproc
.LFE72:
	.size	hdr_reset_internal_counters, .-hdr_reset_internal_counters
	.p2align 4
	.globl	hdr_calculate_bucket_config
	.hidden	hdr_calculate_bucket_config
	.type	hdr_calculate_bucket_config, @function
hdr_calculate_bucket_config:
.LFB74:
	.cfi_startproc
	endbr64
	leal	-1(%rdx), %eax
	cmpl	$4, %eax
	ja	.L180
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	jle	.L164
	leaq	(%rdi,%rdi), %rax
	movq	%rsi, %r12
	cmpq	%rsi, %rax
	jg	.L164
	movq	%rdi, %xmm0
	movq	%rsi, %xmm6
	movslq	%edx, %rdx
	movq	%rcx, %rbx
	punpcklqdq	%xmm6, %xmm0
	movq	%rdx, 24(%rcx)
	movups	%xmm0, (%rcx)
	cmpq	$1, %rdx
	je	.L172
	cmpq	$2, %rdx
	je	.L173
	subq	$3, %rdx
	je	.L174
	cmpq	$1, %rdx
	movl	$10000, %eax
	movl	$1000, %edx
	cmove	%rdx, %rax
.L165:
	leaq	(%rax,%rax,4), %rax
	pxor	%xmm0, %xmm0
	salq	$2, %rax
	cvtsi2sdq	%rax, %xmm0
	call	log@PLT
	movq	.LC4(%rip), %xmm2
	divsd	.LC1(%rip), %xmm0
	movsd	.LC3(%rip), %xmm7
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm1
	andpd	%xmm2, %xmm3
	movapd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm7
	jbe	.L166
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm3, %xmm3
	movsd	.LC5(%rip), %xmm5
	andnpd	%xmm0, %xmm4
	cvtsi2sdq	%rax, %xmm3
	cmpnlesd	%xmm3, %xmm1
	andpd	%xmm5, %xmm1
	addsd	%xmm3, %xmm1
	orpd	%xmm4, %xmm1
.L166:
	cvttsd2sil	%xmm1, %eax
	movl	$1, %edx
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r13, %xmm0
	testl	%eax, %eax
	cmovle	%edx, %eax
	subl	$1, %eax
	movl	%eax, 32(%rbx)
	call	log@PLT
	movq	.LC4(%rip), %xmm2
	divsd	.LC1(%rip), %xmm0
	movsd	.LC3(%rip), %xmm5
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm5
	jbe	.L167
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC5(%rip), %xmm4
	andnpd	%xmm3, %xmm2
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm1, %xmm6
	cmpnlesd	%xmm0, %xmm6
	movapd	%xmm6, %xmm0
	andpd	%xmm4, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	orpd	%xmm2, %xmm0
.L167:
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	movsd	.LC2(%rip), %xmm0
	cltq
	movq	%rax, 16(%rbx)
	movl	32(%rbx), %eax
	addl	$1, %eax
	cvtsi2sdl	%eax, %xmm1
	call	pow@PLT
	movq	16(%rbx), %rcx
	cvttsd2sil	%xmm0, %eax
	movl	%eax, %r8d
	movl	%eax, 48(%rbx)
	cltq
	leaq	-1(%rax), %rdx
	sarl	%r8d
	salq	%cl, %rdx
	movl	%r8d, 36(%rbx)
	movq	%rdx, 40(%rbx)
	movslq	32(%rbx), %rdx
	addq	%rcx, %rdx
	cmpq	$61, %rdx
	jg	.L164
	salq	%cl, %rax
	cmpq	%rax, %r12
	jl	.L176
	movabsq	$4611686018427387903, %rsi
	cmpq	%rsi, %rax
	jg	.L177
	movl	$1, %edx
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L171:
	cmpq	%rsi, %rax
	jg	.L183
.L170:
	movl	%edx, %ecx
	addq	%rax, %rax
	addl	$1, %edx
	leal	2(%rcx), %edi
	cmpq	%rax, %r12
	jge	.L171
.L168:
	imull	%r8d, %edi
	movl	%edx, 52(%rbx)
	xorl	%eax, %eax
	movl	%edi, 56(%rbx)
.L184:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$22, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movl	%edi, %edx
	leal	3(%rcx), %edi
	xorl	%eax, %eax
	imull	%r8d, %edi
	movl	%edx, 52(%rbx)
	movl	%edi, 56(%rbx)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	$1, %eax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L173:
	movl	$10, %eax
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L174:
	movl	$100, %eax
	jmp	.L165
.L176:
	movl	$2, %edi
	movl	$1, %edx
	jmp	.L168
.L177:
	movl	$3, %edi
	movl	$2, %edx
	jmp	.L168
	.cfi_endproc
.LFE74:
	.size	hdr_calculate_bucket_config, .-hdr_calculate_bucket_config
	.p2align 4
	.globl	hdr_init_preallocated
	.hidden	hdr_init_preallocated
	.type	hdr_init_preallocated, @function
hdr_init_preallocated:
.LFB75:
	.cfi_startproc
	endbr64
	movdqu	(%rsi), %xmm1
	movdqa	.LC6(%rip), %xmm0
	movups	%xmm1, (%rdi)
	movq	16(%rsi), %rax
	movl	%eax, 16(%rdi)
	movq	24(%rsi), %rax
	movl	%eax, 20(%rdi)
	movl	32(%rsi), %eax
	movl	%eax, 24(%rdi)
	movl	36(%rsi), %eax
	movl	%eax, 28(%rdi)
	movq	40(%rsi), %rax
	movq	%rax, 32(%rdi)
	movl	48(%rsi), %eax
	movl	$0, 64(%rdi)
	movl	%eax, 40(%rdi)
	movq	.LC5(%rip), %rax
	movups	%xmm0, 48(%rdi)
	movq	%rax, 72(%rdi)
	movl	52(%rsi), %eax
	movl	%eax, 44(%rdi)
	movl	56(%rsi), %eax
	movq	$0, 88(%rdi)
	movl	%eax, 80(%rdi)
	ret
	.cfi_endproc
.LFE75:
	.size	hdr_init_preallocated, .-hdr_init_preallocated
	.p2align 4
	.globl	hdr_init
	.hidden	hdr_init
	.type	hdr_init, @function
hdr_init:
.LFB76:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	leaq	-112(%rbp), %rcx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	hdr_calculate_bucket_config
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L191
.L186:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$80, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movslq	-56(%rbp), %rdi
	movl	$8, %esi
	movq	%rdi, %r14
	call	calloc@PLT
	movl	$104, %esi
	movl	$1, %edi
	movq	%rax, %r13
	call	calloc@PLT
	testq	%r13, %r13
	je	.L189
	testq	%rax, %rax
	je	.L189
	movq	-96(%rbp), %rdx
	movdqa	-112(%rbp), %xmm1
	movq	%r13, 96(%rax)
	movdqa	.LC6(%rip), %xmm0
	movq	.LC5(%rip), %rcx
	movl	%r14d, 80(%rax)
	movl	%edx, 16(%rax)
	movq	-88(%rbp), %rdx
	movl	$0, 64(%rax)
	movl	%edx, 20(%rax)
	movq	-80(%rbp), %rdx
	movq	%rcx, 72(%rax)
	movq	%rdx, 24(%rax)
	movq	-72(%rbp), %rdx
	movq	$0, 88(%rax)
	movq	%rdx, 32(%rax)
	movq	-64(%rbp), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 40(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm0, 48(%rax)
	jmp	.L186
.L192:
	call	__stack_chk_fail@PLT
.L189:
	movl	$12, %r12d
	jmp	.L186
	.cfi_endproc
.LFE76:
	.size	hdr_init, .-hdr_init
	.p2align 4
	.globl	hdr_close
	.hidden	hdr_close
	.type	hdr_close, @function
hdr_close:
.LFB77:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	96(%rdi), %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.cfi_endproc
.LFE77:
	.size	hdr_close, .-hdr_close
	.p2align 4
	.globl	hdr_alloc
	.hidden	hdr_alloc
	.type	hdr_alloc, @function
hdr_alloc:
.LFB78:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-112(%rbp), %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movl	$1, %edi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	hdr_calculate_bucket_config
	movl	%eax, %r12d
	testl	%eax, %eax
	je	.L200
.L195:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L201
	addq	$80, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movslq	-56(%rbp), %rdi
	movl	$8, %esi
	movq	%rdi, %r14
	call	calloc@PLT
	movl	$104, %esi
	movl	$1, %edi
	movq	%rax, %r13
	call	calloc@PLT
	testq	%r13, %r13
	je	.L198
	testq	%rax, %rax
	je	.L198
	movq	-96(%rbp), %rdx
	movdqa	-112(%rbp), %xmm1
	movq	%r13, 96(%rax)
	movdqa	.LC6(%rip), %xmm0
	movq	.LC5(%rip), %rcx
	movl	%r14d, 80(%rax)
	movl	%edx, 16(%rax)
	movq	-88(%rbp), %rdx
	movl	$0, 64(%rax)
	movl	%edx, 20(%rax)
	movq	-80(%rbp), %rdx
	movq	%rcx, 72(%rax)
	movq	%rdx, 24(%rax)
	movq	-72(%rbp), %rdx
	movq	$0, 88(%rax)
	movq	%rdx, 32(%rax)
	movq	-64(%rbp), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 40(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm0, 48(%rax)
	jmp	.L195
.L201:
	call	__stack_chk_fail@PLT
.L198:
	movl	$12, %r12d
	jmp	.L195
	.cfi_endproc
.LFE78:
	.size	hdr_alloc, .-hdr_alloc
	.p2align 4
	.globl	hdr_reset
	.hidden	hdr_reset
	.type	hdr_reset, @function
hdr_reset:
.LFB79:
	.cfi_startproc
	endbr64
	movdqa	.LC6(%rip), %xmm0
	movslq	80(%rdi), %rdx
	movq	$0, 88(%rdi)
	xorl	%esi, %esi
	movups	%xmm0, 48(%rdi)
	movq	96(%rdi), %rdi
	salq	$3, %rdx
	jmp	memset@PLT
	.cfi_endproc
.LFE79:
	.size	hdr_reset, .-hdr_reset
	.p2align 4
	.globl	hdr_get_memory_size
	.hidden	hdr_get_memory_size
	.type	hdr_get_memory_size, @function
hdr_get_memory_size:
.LFB80:
	.cfi_startproc
	endbr64
	movslq	80(%rdi), %rax
	leaq	104(,%rax,8), %rax
	ret
	.cfi_endproc
.LFE80:
	.size	hdr_get_memory_size, .-hdr_get_memory_size
	.p2align 4
	.globl	hdr_record_value
	.hidden	hdr_record_value
	.type	hdr_record_value, @function
hdr_record_value:
.LFB81:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	testq	%rsi, %rsi
	js	.L204
	movq	32(%rdi), %rdx
	movl	16(%rdi), %ecx
	movl	$64, %eax
	movl	24(%rdi), %r9d
	orq	%rsi, %rdx
	bsrq	%rdx, %rdx
	xorq	$63, %rdx
	subl	%edx, %eax
	leal	1(%r9), %edx
	subl	%ecx, %eax
	subl	%edx, %eax
	movq	%rsi, %rdx
	addl	%eax, %ecx
	addl	$1, %eax
	sarq	%cl, %rdx
	movl	%r9d, %ecx
	subl	28(%rdi), %edx
	sall	%cl, %eax
	addl	%edx, %eax
	js	.L204
	movl	80(%rdi), %edx
	cmpl	%edx, %eax
	jge	.L204
	movl	64(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L206
	subl	%ecx, %eax
	js	.L218
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	%eax, %edx
	cmovle	%ecx, %eax
.L206:
	movq	96(%rdi), %rdx
	cltq
	addq	$1, (%rdx,%rax,8)
	movq	48(%rdi), %rax
	addq	$1, 88(%rdi)
	testq	%rsi, %rsi
	je	.L209
	cmpq	%rax, %rsi
	cmovl	%rsi, %rax
.L209:
	cmpq	%rsi, 56(%rdi)
	cmovge	56(%rdi), %rsi
	movq	%rax, 48(%rdi)
	movl	$1, %r8d
	movq	%rsi, 56(%rdi)
.L204:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	addl	%edx, %eax
	jmp	.L206
	.cfi_endproc
.LFE81:
	.size	hdr_record_value, .-hdr_record_value
	.p2align 4
	.globl	hdr_record_values
	.hidden	hdr_record_values
	.type	hdr_record_values, @function
hdr_record_values:
.LFB82:
	.cfi_startproc
	endbr64
	xorl	%r9d, %r9d
	testq	%rsi, %rsi
	js	.L219
	movq	32(%rdi), %r8
	movl	16(%rdi), %ecx
	movl	$64, %eax
	movl	24(%rdi), %r10d
	orq	%rsi, %r8
	bsrq	%r8, %r8
	xorq	$63, %r8
	subl	%r8d, %eax
	leal	1(%r10), %r8d
	subl	%ecx, %eax
	subl	%r8d, %eax
	movq	%rsi, %r8
	addl	%eax, %ecx
	addl	$1, %eax
	sarq	%cl, %r8
	movl	%r10d, %ecx
	subl	28(%rdi), %r8d
	sall	%cl, %eax
	addl	%r8d, %eax
	js	.L219
	movl	80(%rdi), %ecx
	cmpl	%eax, %ecx
	jle	.L219
	movl	64(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L221
	subl	%r8d, %eax
	js	.L233
	movl	%eax, %r8d
	subl	%ecx, %r8d
	cmpl	%eax, %ecx
	cmovle	%r8d, %eax
.L221:
	movq	96(%rdi), %rcx
	cltq
	addq	%rdx, (%rcx,%rax,8)
	movq	48(%rdi), %rax
	addq	%rdx, 88(%rdi)
	testq	%rsi, %rsi
	je	.L224
	cmpq	%rax, %rsi
	cmovl	%rsi, %rax
.L224:
	cmpq	%rsi, 56(%rdi)
	cmovge	56(%rdi), %rsi
	movq	%rax, 48(%rdi)
	movl	$1, %r9d
	movq	%rsi, 56(%rdi)
.L219:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	addl	%ecx, %eax
	jmp	.L221
	.cfi_endproc
.LFE82:
	.size	hdr_record_values, .-hdr_record_values
	.p2align 4
	.globl	hdr_record_corrected_value
	.hidden	hdr_record_corrected_value
	.type	hdr_record_corrected_value, @function
hdr_record_corrected_value:
.LFB83:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	hdr_record_values
	testb	%al, %al
	je	.L240
	testq	%r11, %r11
	jle	.L234
	cmpq	%r11, %rbx
	jle	.L234
	subq	%r11, %rbx
	movq	%rbx, %rsi
	cmpq	%rbx, %r11
	jg	.L234
	movq	32(%rdi), %rdx
	movl	16(%rdi), %r9d
	movl	$64, %r13d
	movl	24(%rdi), %r10d
	orq	%rbx, %rdx
	movl	28(%rdi), %ebx
	bsrq	%rdx, %rdx
	leal	1(%r10), %r12d
	xorq	$63, %rdx
	subl	%edx, %r13d
	movq	%rsi, %rdx
	subl	%r9d, %r13d
	subl	%r12d, %r13d
	leal	(%r9,%r13), %ecx
	addl	$1, %r13d
	sarq	%cl, %rdx
	movl	%r10d, %ecx
	movl	%edx, %r8d
	sall	%cl, %r13d
	subl	%ebx, %r8d
	movl	%r8d, %edx
	addl	%r13d, %edx
	js	.L240
	movl	80(%rdi), %r13d
	movl	$64, %r14d
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L251:
	movl	64(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L242
	subl	%ecx, %edx
	js	.L250
	movl	%edx, %ecx
	subl	%r13d, %ecx
	cmpl	%edx, %r13d
	cmovle	%ecx, %edx
.L242:
	movq	96(%rdi), %rcx
	movslq	%edx, %rdx
	addq	$1, (%rcx,%rdx,8)
	movq	%rsi, %rdx
	addq	$1, 88(%rdi)
	cmpq	%rsi, 48(%rdi)
	cmovle	48(%rdi), %rdx
	cmpq	%rsi, 56(%rdi)
	movq	%rdx, 48(%rdi)
	movq	%rsi, %rdx
	cmovge	56(%rdi), %rdx
	subq	%r11, %rsi
	movq	%rdx, 56(%rdi)
	cmpq	%rsi, %r11
	jg	.L234
	movq	32(%rdi), %rdx
	movl	%r14d, %r15d
	orq	%rsi, %rdx
	bsrq	%rdx, %rdx
	xorq	$63, %rdx
	subl	%edx, %r15d
	movq	%rsi, %rdx
	subl	%r9d, %r15d
	subl	%r12d, %r15d
	leal	(%r15,%r9), %ecx
	addl	$1, %r15d
	sarq	%cl, %rdx
	movl	%r10d, %ecx
	movl	%edx, %r8d
	sall	%cl, %r15d
	subl	%ebx, %r8d
	movl	%r8d, %edx
	addl	%r15d, %edx
	js	.L240
.L241:
	cmpl	%r13d, %edx
	jl	.L251
.L240:
	xorl	%eax, %eax
.L234:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	addl	%r13d, %edx
	jmp	.L242
	.cfi_endproc
.LFE83:
	.size	hdr_record_corrected_value, .-hdr_record_corrected_value
	.p2align 4
	.globl	hdr_record_corrected_values
	.hidden	hdr_record_corrected_values
	.type	hdr_record_corrected_values, @function
hdr_record_corrected_values:
.LFB84:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	hdr_record_values
	testb	%al, %al
	je	.L258
	testq	%r11, %r11
	jle	.L252
	cmpq	%r11, %rbx
	jle	.L252
	subq	%r11, %rbx
	movq	%rbx, %rsi
	cmpq	%rbx, %r11
	jg	.L252
	movq	32(%rdi), %rcx
	movl	16(%rdi), %r8d
	movl	$64, %r13d
	movq	%rsi, %r9
	movl	24(%rdi), %r10d
	orq	%rbx, %rcx
	movl	28(%rdi), %ebx
	bsrq	%rcx, %rcx
	leal	1(%r10), %r12d
	xorq	$63, %rcx
	subl	%ecx, %r13d
	subl	%r8d, %r13d
	subl	%r12d, %r13d
	leal	(%r8,%r13), %ecx
	addl	$1, %r13d
	sarq	%cl, %r9
	movl	%r10d, %ecx
	subl	%ebx, %r9d
	sall	%cl, %r13d
	movl	%r9d, %ecx
	addl	%r13d, %ecx
	js	.L258
	movl	80(%rdi), %r9d
	movl	$64, %r13d
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L269:
	movl	64(%rdi), %r14d
	testl	%r14d, %r14d
	je	.L260
	subl	%r14d, %ecx
	js	.L268
	movl	%ecx, %r14d
	subl	%r9d, %r14d
	cmpl	%ecx, %r9d
	cmovle	%r14d, %ecx
.L260:
	movq	96(%rdi), %r14
	movslq	%ecx, %rcx
	addq	%rdx, (%r14,%rcx,8)
	movq	%rsi, %rcx
	addq	%rdx, 88(%rdi)
	cmpq	%rsi, 48(%rdi)
	cmovle	48(%rdi), %rcx
	cmpq	%rsi, 56(%rdi)
	movq	%rcx, 48(%rdi)
	movq	%rsi, %rcx
	cmovge	56(%rdi), %rcx
	subq	%r11, %rsi
	movq	%rcx, 56(%rdi)
	cmpq	%rsi, %r11
	jg	.L252
	movq	32(%rdi), %rcx
	movl	%r13d, %r15d
	movq	%rsi, %r14
	orq	%rsi, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %r15d
	subl	%r8d, %r15d
	subl	%r12d, %r15d
	leal	(%r15,%r8), %ecx
	addl	$1, %r15d
	sarq	%cl, %r14
	movl	%r10d, %ecx
	subl	%ebx, %r14d
	sall	%cl, %r15d
	movl	%r14d, %ecx
	addl	%r15d, %ecx
	js	.L258
.L259:
	cmpl	%r9d, %ecx
	jl	.L269
.L258:
	xorl	%eax, %eax
.L252:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	addl	%r9d, %ecx
	jmp	.L260
	.cfi_endproc
.LFE84:
	.size	hdr_record_corrected_values, .-hdr_record_corrected_values
	.p2align 4
	.globl	hdr_add
	.hidden	hdr_add
	.type	hdr_add, @function
hdr_add:
.LFB85:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	$64, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-160(%rbp), %rbx
	movq	%rbx, %rdi
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	%rsi, -160(%rbp)
	movl	$-1, -152(%rbp)
	movl	%eax, -148(%rbp)
	leaq	_recorded_iter_next(%rip), %rax
	movq	$0, -80(%rbp)
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	*%rax
	testb	%al, %al
	je	.L286
	.p2align 4,,10
	.p2align 3
.L279:
	movq	-128(%rbp), %rsi
	movq	-144(%rbp), %rdi
	testq	%rsi, %rsi
	js	.L272
	movq	32(%r14), %rax
	movl	%r13d, %edx
	movl	16(%r14), %ecx
	movl	24(%r14), %r8d
	orq	%rsi, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movl	%edx, %eax
	leal	1(%r8), %edx
	subl	%ecx, %eax
	subl	%edx, %eax
	movq	%rsi, %rdx
	addl	%eax, %ecx
	addl	$1, %eax
	sarq	%cl, %rdx
	movl	%r8d, %ecx
	subl	28(%r14), %edx
	sall	%cl, %eax
	addl	%edx, %eax
	js	.L272
	movl	80(%r14), %edx
	cmpl	%edx, %eax
	jge	.L272
	movl	64(%r14), %ecx
	testl	%ecx, %ecx
	je	.L273
	subl	%ecx, %eax
	js	.L287
	movl	%eax, %ecx
	subl	%edx, %ecx
	cmpl	%eax, %edx
	cmovle	%ecx, %eax
.L273:
	movq	96(%r14), %rdx
	cltq
	addq	%rdi, (%rdx,%rax,8)
	movq	48(%r14), %rax
	addq	%rdi, 88(%r14)
	testq	%rsi, %rsi
	je	.L276
	cmpq	%rax, %rsi
	cmovl	%rsi, %rax
.L276:
	cmpq	%rsi, 56(%r14)
	cmovge	56(%r14), %rsi
	movq	%rax, 48(%r14)
	movq	%rsi, 56(%r14)
.L278:
	movq	-48(%rbp), %rax
	movq	%rbx, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L279
.L286:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L288
	subq	$-128, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	addq	%rdi, %r12
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L287:
	addl	%edx, %eax
	jmp	.L273
.L288:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE85:
	.size	hdr_add, .-hdr_add
	.p2align 4
	.globl	hdr_add_while_correcting_for_coordinated_omission
	.hidden	hdr_add_while_correcting_for_coordinated_omission
	.type	hdr_add_while_correcting_for_coordinated_omission, @function
hdr_add_while_correcting_for_coordinated_omission:
.LFB86:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	88(%rsi), %rax
	movq	%rsi, -176(%rbp)
	movl	$-1, -168(%rbp)
	movl	%eax, -164(%rbp)
	leaq	_recorded_iter_next(%rip), %rax
	movq	$0, -96(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	*%rax
	testb	%al, %al
	je	.L307
	.p2align 4,,10
	.p2align 3
.L298:
	movq	-144(%rbp), %r15
	movq	-160(%rbp), %r11
	movq	%rbx, %rdi
	movq	%r11, %rdx
	movq	%r15, %rsi
	call	hdr_record_values
	testb	%al, %al
	je	.L291
	testq	%r12, %r12
	jle	.L292
	cmpq	%r12, %r15
	jle	.L292
	movq	%r15, %rdx
	subq	%r12, %rdx
	cmpq	%rdx, %r12
	jg	.L292
	movq	32(%rbx), %rax
	movl	16(%rbx), %edi
	movl	$64, %r8d
	movq	%rdx, %rsi
	movl	24(%rbx), %r9d
	movl	28(%rbx), %r15d
	orq	%rdx, %rax
	bsrq	%rax, %rax
	leal	1(%r9), %r10d
	xorq	$63, %rax
	subl	%eax, %r8d
	subl	%edi, %r8d
	subl	%r10d, %r8d
	leal	(%r8,%rdi), %ecx
	sarq	%cl, %rsi
	movl	%r9d, %ecx
	movl	%esi, %eax
	leal	1(%r8), %esi
	subl	%r15d, %eax
	sall	%cl, %esi
	addl	%eax, %esi
	js	.L291
	movl	80(%rbx), %r8d
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L309:
	movl	64(%rbx), %eax
	testl	%eax, %eax
	je	.L294
	subl	%eax, %esi
	js	.L308
	movl	%esi, %eax
	subl	%r8d, %eax
	cmpl	%esi, %r8d
	cmovle	%eax, %esi
.L294:
	movq	96(%rbx), %rcx
	movslq	%esi, %rsi
	movq	%rdx, %rax
	addq	%r11, (%rcx,%rsi,8)
	addq	%r11, 88(%rbx)
	cmpq	%rdx, 48(%rbx)
	cmovle	48(%rbx), %rax
	cmpq	%rdx, 56(%rbx)
	movq	%rax, 48(%rbx)
	movq	%rdx, %rax
	cmovge	56(%rbx), %rax
	subq	%r12, %rdx
	movq	%rax, 56(%rbx)
	cmpq	%rdx, %r12
	jg	.L292
	movq	32(%rbx), %rax
	movl	$64, %ecx
	movq	%rdx, %rsi
	orq	%rdx, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %ecx
	movl	%ecx, %eax
	subl	%edi, %eax
	subl	%r10d, %eax
	leal	(%rax,%rdi), %ecx
	addl	$1, %eax
	sarq	%cl, %rsi
	movl	%r9d, %ecx
	subl	%r15d, %esi
	sall	%cl, %eax
	addl	%eax, %esi
	js	.L291
.L293:
	cmpl	%esi, %r8d
	jg	.L309
.L291:
	addq	%r11, %r13
.L292:
	movq	-64(%rbp), %rax
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L298
.L307:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L310
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	addl	%r8d, %esi
	jmp	.L294
.L310:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE86:
	.size	hdr_add_while_correcting_for_coordinated_omission, .-hdr_add_while_correcting_for_coordinated_omission
	.p2align 4
	.globl	hdr_max
	.hidden	hdr_max
	.type	hdr_max, @function
hdr_max:
.LFB87:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L311
	movq	32(%rdi), %rcx
	movl	16(%rdi), %r8d
	movl	$64, %edx
	movl	24(%rdi), %esi
	orq	%rax, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %edx
	leal	1(%rsi), %ecx
	subl	%r8d, %edx
	subl	%ecx, %edx
	leal	(%r8,%rdx), %ecx
	sarq	%cl, %rax
	movslq	%eax, %rsi
	salq	%cl, %rsi
	cmpl	40(%rdi), %eax
	jl	.L313
	leal	1(%r8,%rdx), %ecx
.L313:
	movl	$1, %eax
	salq	%cl, %rax
	leaq	-1(%rsi,%rax), %rax
.L311:
	ret
	.cfi_endproc
.LFE87:
	.size	hdr_max, .-hdr_max
	.p2align 4
	.globl	hdr_min
	.hidden	hdr_min
	.type	hdr_min, @function
hdr_min:
.LFB88:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	xorl	%edx, %edx
	testl	%eax, %eax
	je	.L318
	movl	80(%rdi), %ecx
	jg	.L325
	movl	%eax, %edx
	movl	%ecx, %esi
	negl	%edx
	negl	%esi
	subl	%eax, %esi
	cmpl	%ecx, %edx
	cmovge	%esi, %edx
.L320:
	movslq	%edx, %rdx
	salq	$3, %rdx
.L318:
	movq	96(%rdi), %rcx
	xorl	%eax, %eax
	cmpq	$0, (%rcx,%rdx)
	jg	.L317
	movabsq	$9223372036854775807, %rdx
	movq	48(%rdi), %rax
	cmpq	%rdx, %rax
	je	.L317
	movq	32(%rdi), %rdx
	movl	$64, %ecx
	orq	%rax, %rdx
	bsrq	%rdx, %rdx
	xorq	$63, %rdx
	subl	%edx, %ecx
	movl	24(%rdi), %edx
	addl	$1, %edx
	subl	%edx, %ecx
	sarq	%cl, %rax
	cltq
	salq	%cl, %rax
.L317:
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	subl	%eax, %ecx
	movl	%ecx, %edx
	jmp	.L320
	.cfi_endproc
.LFE88:
	.size	hdr_min, .-hdr_min
	.p2align 4
	.globl	hdr_value_at_percentile
	.hidden	hdr_value_at_percentile
	.type	hdr_value_at_percentile, @function
hdr_value_at_percentile:
.LFB89:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movsd	.LC0(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	comisd	%xmm0, %xmm1
	jbe	.L337
	divsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
.L327:
	movq	88(%r13), %rax
	pxor	%xmm0, %xmm0
	movl	$1, %edx
	movq	%r13, -160(%rbp)
	movl	$-1, -152(%rbp)
	leaq	-160(%rbp), %r14
	cvtsi2sdq	%rax, %xmm0
	movl	%eax, -148(%rbp)
	leaq	_all_values_iter_next(%rip), %rax
	movq	%rax, -48(%rbp)
	mulsd	%xmm1, %xmm0
	addsd	.LC7(%rip), %xmm0
	cvttsd2siq	%xmm0, %r12
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -96(%rbp)
	testq	%r12, %r12
	cmovle	%rdx, %r12
	xorl	%ebx, %ebx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L333:
	addq	-144(%rbp), %rbx
	cmpq	%rbx, %r12
	jle	.L339
	movq	-48(%rbp), %rax
.L329:
	movq	%r14, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L333
	xorl	%eax, %eax
.L326:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L340
	subq	$-128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movsd	.LC5(%rip), %xmm1
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L339:
	movq	-128(%rbp), %rdx
	movq	32(%r13), %rcx
	movl	$64, %eax
	movl	16(%r13), %edi
	movl	24(%r13), %esi
	orq	%rdx, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %eax
	leal	1(%rsi), %ecx
	subl	%edi, %eax
	subl	%ecx, %eax
	leal	(%rdi,%rax), %ecx
	sarq	%cl, %rdx
	movslq	%edx, %rsi
	salq	%cl, %rsi
	cmpl	40(%r13), %edx
	jl	.L331
	leal	1(%rdi,%rax), %ecx
.L331:
	movl	$1, %eax
	salq	%cl, %rax
	leaq	-1(%rsi,%rax), %rax
	jmp	.L326
.L340:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE89:
	.size	hdr_value_at_percentile, .-hdr_value_at_percentile
	.p2align 4
	.globl	hdr_mean
	.hidden	hdr_mean
	.type	hdr_mean, @function
hdr_mean:
.LFB90:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	$64, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-160(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	xorl	%ebx, %ebx
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	88(%rdi), %rax
	movq	%rdi, -160(%rbp)
	movl	$-1, -152(%rbp)
	movl	%eax, -148(%rbp)
	leaq	_all_values_iter_next(%rip), %rax
	movq	%rax, -48(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -96(%rbp)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L345:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L343
	movq	-128(%rbp), %rsi
	movq	32(%r14), %rax
	movl	%r13d, %edx
	movl	16(%r14), %r8d
	orq	%rsi, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movl	24(%r14), %eax
	subl	%r8d, %edx
	addl	$1, %eax
	subl	%eax, %edx
	leal	(%r8,%rdx), %ecx
	sarq	%cl, %rsi
	movslq	%esi, %rax
	salq	%cl, %rax
	movq	%rax, %r9
	cmpl	40(%r14), %esi
	jl	.L344
	leal	1(%r8,%rdx), %ecx
.L344:
	movl	$1, %eax
	salq	%cl, %rax
	sarq	%rax
	addq	%r9, %rax
	imulq	%rdi, %rax
	addq	%rax, %rbx
.L343:
	movq	-48(%rbp), %rax
.L342:
	movq	%r12, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L345
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rbx, %xmm0
	cvtsi2sdq	88(%r14), %xmm1
	divsd	%xmm1, %xmm0
	jne	.L351
	subq	$-128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L351:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE90:
	.size	hdr_mean, .-hdr_mean
	.p2align 4
	.globl	hdr_stddev
	.hidden	hdr_stddev
	.type	hdr_stddev, @function
hdr_stddev:
.LFB91:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	$64, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-160(%rbp), %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	hdr_mean
	movq	88(%r13), %rax
	movq	%r13, -160(%rbp)
	movsd	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movl	%eax, -148(%rbp)
	leaq	_all_values_iter_next(%rip), %rax
	movl	$-1, -152(%rbp)
	movq	%rax, -48(%rbp)
	movq	$0x000000000, -168(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -96(%rbp)
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L356:
	movq	-144(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L354
	movq	-128(%rbp), %rdi
	movq	32(%r13), %rax
	movl	%r12d, %esi
	movl	16(%r13), %r9d
	orq	%rdi, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %esi
	movl	24(%r13), %eax
	subl	%r9d, %esi
	addl	$1, %eax
	subl	%eax, %esi
	leal	(%r9,%rsi), %ecx
	sarq	%cl, %rdi
	movslq	%edi, %rax
	salq	%cl, %rax
	movq	%rax, %r8
	cmpl	40(%r13), %edi
	jl	.L355
	leal	1(%r9,%rsi), %ecx
.L355:
	movl	$1, %eax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	salq	%cl, %rax
	cvtsi2sdq	%rdx, %xmm1
	sarq	%rax
	addq	%r8, %rax
	cvtsi2sdq	%rax, %xmm0
	subsd	-176(%rbp), %xmm0
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	-168(%rbp), %xmm0
	movsd	%xmm0, -168(%rbp)
.L354:
	movq	-48(%rbp), %rax
.L353:
	movq	%rbx, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L356
	pxor	%xmm0, %xmm0
	movsd	-168(%rbp), %xmm4
	pxor	%xmm5, %xmm5
	cvtsi2sdq	88(%r13), %xmm0
	divsd	%xmm0, %xmm4
	ucomisd	%xmm4, %xmm5
	movapd	%xmm4, %xmm1
	movapd	%xmm4, %xmm0
	sqrtsd	%xmm1, %xmm1
	ja	.L363
.L352:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L364
	addq	$152, %rsp
	movapd	%xmm1, %xmm0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L363:
	.cfi_restore_state
	movsd	%xmm1, -168(%rbp)
	call	sqrt@PLT
	movsd	-168(%rbp), %xmm1
	jmp	.L352
.L364:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE91:
	.size	hdr_stddev, .-hdr_stddev
	.p2align 4
	.globl	hdr_values_are_equivalent
	.hidden	hdr_values_are_equivalent
	.type	hdr_values_are_equivalent, @function
hdr_values_are_equivalent:
.LFB92:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	%rsi, %r8
	movl	24(%rdi), %r9d
	movl	$64, %ecx
	movl	%ecx, %edi
	orq	%rax, %r8
	orq	%rdx, %rax
	addl	$1, %r9d
	bsrq	%rax, %rax
	bsrq	%r8, %r8
	xorq	$63, %rax
	xorq	$63, %r8
	subl	%eax, %ecx
	subl	%r8d, %edi
	subl	%r9d, %ecx
	subl	%r9d, %edi
	sarq	%cl, %rdx
	movslq	%edx, %rdx
	salq	%cl, %rdx
	movl	%edi, %ecx
	sarq	%cl, %rsi
	movslq	%esi, %rsi
	salq	%cl, %rsi
	cmpq	%rsi, %rdx
	sete	%al
	ret
	.cfi_endproc
.LFE92:
	.size	hdr_values_are_equivalent, .-hdr_values_are_equivalent
	.p2align 4
	.globl	hdr_lowest_equivalent_value
	.hidden	hdr_lowest_equivalent_value
	.type	hdr_lowest_equivalent_value, @function
hdr_lowest_equivalent_value:
.LFB93:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movl	$64, %ecx
	orq	%rsi, %rax
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %ecx
	movl	24(%rdi), %eax
	addl	$1, %eax
	subl	%eax, %ecx
	sarq	%cl, %rsi
	movslq	%esi, %rax
	salq	%cl, %rax
	ret
	.cfi_endproc
.LFE93:
	.size	hdr_lowest_equivalent_value, .-hdr_lowest_equivalent_value
	.p2align 4
	.globl	hdr_count_at_value
	.hidden	hdr_count_at_value
	.type	hdr_count_at_value, @function
hdr_count_at_value:
.LFB94:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	movl	16(%rdi), %ecx
	movl	$64, %eax
	movl	24(%rdi), %r8d
	orq	%rsi, %rdx
	bsrq	%rdx, %rdx
	xorq	$63, %rdx
	subl	%edx, %eax
	leal	1(%r8), %edx
	subl	%ecx, %eax
	subl	%edx, %eax
	movl	64(%rdi), %edx
	addl	%eax, %ecx
	addl	$1, %eax
	sarq	%cl, %rsi
	movl	%r8d, %ecx
	subl	28(%rdi), %esi
	sall	%cl, %eax
	addl	%esi, %eax
	testl	%edx, %edx
	je	.L368
	movl	80(%rdi), %ecx
	subl	%edx, %eax
	js	.L374
	movl	%eax, %edx
	subl	%ecx, %edx
	cmpl	%ecx, %eax
	cmovge	%edx, %eax
.L368:
	movq	96(%rdi), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	movq	96(%rdi), %rdx
	addl	%ecx, %eax
	cltq
	movq	(%rdx,%rax,8), %rax
	ret
	.cfi_endproc
.LFE94:
	.size	hdr_count_at_value, .-hdr_count_at_value
	.p2align 4
	.globl	hdr_count_at_index
	.hidden	hdr_count_at_index
	.type	hdr_count_at_index, @function
hdr_count_at_index:
.LFB95:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	testl	%eax, %eax
	je	.L376
	movl	80(%rdi), %edx
	subl	%eax, %esi
	js	.L382
	movl	%esi, %eax
	subl	%edx, %eax
	cmpl	%edx, %esi
	cmovge	%eax, %esi
.L376:
	movq	96(%rdi), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L382:
	movq	96(%rdi), %rax
	addl	%edx, %esi
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE95:
	.size	hdr_count_at_index, .-hdr_count_at_index
	.p2align 4
	.globl	hdr_iter_init
	.hidden	hdr_iter_init
	.type	hdr_iter_init, @function
hdr_iter_init:
.LFB104:
	.cfi_startproc
	endbr64
	movq	88(%rsi), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rdi)
	movl	$-1, 8(%rdi)
	movl	%eax, 12(%rdi)
	leaq	_all_values_iter_next(%rip), %rax
	movq	%rax, 112(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE104:
	.size	hdr_iter_init, .-hdr_iter_init
	.p2align 4
	.globl	hdr_iter_next
	.hidden	hdr_iter_next
	.type	hdr_iter_next, @function
hdr_iter_next:
.LFB105:
	.cfi_startproc
	endbr64
	jmp	*112(%rdi)
	.cfi_endproc
.LFE105:
	.size	hdr_iter_next, .-hdr_iter_next
	.p2align 4
	.globl	hdr_iter_percentile_init
	.hidden	hdr_iter_percentile_init
	.type	hdr_iter_percentile_init, @function
hdr_iter_percentile_init:
.LFB107:
	.cfi_startproc
	endbr64
	movq	88(%rsi), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rdi)
	movups	%xmm0, 16(%rdi)
	movl	%eax, 12(%rdi)
	leaq	_percentile_iter_next(%rip), %rax
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 64(%rdi)
	pxor	%xmm0, %xmm0
	movl	$-1, 8(%rdi)
	movb	$0, 80(%rdi)
	movl	%edx, 84(%rdi)
	movq	%rax, 112(%rdi)
	movups	%xmm0, 88(%rdi)
	ret
	.cfi_endproc
.LFE107:
	.size	hdr_iter_percentile_init, .-hdr_iter_percentile_init
	.p2align 4
	.globl	hdr_iter_recorded_init
	.hidden	hdr_iter_recorded_init
	.type	hdr_iter_recorded_init, @function
hdr_iter_recorded_init:
.LFB110:
	.cfi_startproc
	endbr64
	movq	88(%rsi), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rdi)
	movl	$-1, 8(%rdi)
	movl	%eax, 12(%rdi)
	leaq	_recorded_iter_next(%rip), %rax
	movq	$0, 80(%rdi)
	movq	%rax, 112(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE110:
	.size	hdr_iter_recorded_init, .-hdr_iter_recorded_init
	.p2align 4
	.globl	hdr_iter_linear_init
	.hidden	hdr_iter_linear_init
	.type	hdr_iter_linear_init, @function
hdr_iter_linear_init:
.LFB112:
	.cfi_startproc
	endbr64
	movq	88(%rsi), %rax
	pxor	%xmm0, %xmm0
	movl	$64, %ecx
	movq	%rsi, (%rdi)
	movups	%xmm0, 16(%rdi)
	movl	%eax, 12(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 64(%rdi)
	movq	32(%rsi), %rax
	movq	%rdx, 80(%rdi)
	orq	%rdx, %rax
	movq	%rdx, 96(%rdi)
	bsrq	%rax, %rax
	movl	$-1, 8(%rdi)
	xorq	$63, %rax
	movq	$0, 88(%rdi)
	subl	%eax, %ecx
	movl	24(%rsi), %eax
	addl	$1, %eax
	subl	%eax, %ecx
	leaq	_iter_linear_next(%rip), %rax
	sarq	%cl, %rdx
	movq	%rax, 112(%rdi)
	movslq	%edx, %rdx
	salq	%cl, %rdx
	movq	%rdx, 104(%rdi)
	ret
	.cfi_endproc
.LFE112:
	.size	hdr_iter_linear_init, .-hdr_iter_linear_init
	.p2align 4
	.globl	hdr_iter_log_init
	.hidden	hdr_iter_log_init
	.type	hdr_iter_log_init, @function
hdr_iter_log_init:
.LFB114:
	.cfi_startproc
	endbr64
	movq	88(%rsi), %rax
	movapd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	movq	%rdx, 96(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$64, %ecx
	movl	%eax, 12(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 64(%rdi)
	movq	32(%rsi), %rax
	movq	%rsi, (%rdi)
	orq	%rdx, %rax
	movl	$-1, 8(%rdi)
	bsrq	%rax, %rax
	movq	$0, 88(%rdi)
	xorq	$63, %rax
	movsd	%xmm1, 80(%rdi)
	subl	%eax, %ecx
	movl	24(%rsi), %eax
	addl	$1, %eax
	subl	%eax, %ecx
	leaq	_log_iter_next(%rip), %rax
	sarq	%cl, %rdx
	movq	%rax, 112(%rdi)
	movslq	%edx, %rdx
	salq	%cl, %rdx
	movq	%rdx, 104(%rdi)
	ret
	.cfi_endproc
.LFE114:
	.size	hdr_iter_log_init, .-hdr_iter_log_init
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC9:
	.string	"%12s %12s %12s %12s\n\n"
.LC10:
	.string	"%s,%s,%s,%s\n"
.LC11:
	.string	"%."
.LC12:
	.string	"%s%d%s"
.LC13:
	.string	"f,%f,%d,%.2f\n"
.LC14:
	.string	"%12."
.LC15:
	.string	"f %12f %12d %12.2f\n"
.LC16:
	.string	"TotalCount"
.LC17:
	.string	"Percentile"
.LC18:
	.string	"Value"
.LC19:
	.string	"1/(1-Percentile)"
	.text
	.p2align 4
	.globl	hdr_percentiles_print
	.hidden	hdr_percentiles_print
	.type	hdr_percentiles_print, @function
hdr_percentiles_print:
.LFB116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-96(%rbp), %rbx
	subq	$200, %rsp
	movsd	%xmm0, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	20(%rdi), %eax
	cmpl	$1, %ecx
	je	.L404
	leaq	.LC15(%rip), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	movl	$25, %ecx
	pushq	%rsi
	leaq	.LC14(%rip), %r9
	movl	$25, %esi
	leaq	.LC12(%rip), %r8
	pushq	%rax
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	popq	%rsi
	leaq	.LC9(%rip), %rdx
	popq	%rdi
.L391:
	movq	88(%r14), %rax
	subq	$8, %rsp
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movaps	%xmm0, -208(%rbp)
	leaq	.LC18(%rip), %rcx
	leaq	.LC16(%rip), %r9
	movl	$1, %esi
	movl	%eax, -212(%rbp)
	leaq	_percentile_iter_next(%rip), %rax
	leaq	.LC17(%rip), %r8
	movq	%rax, -112(%rbp)
	leaq	.LC19(%rip), %rax
	pushq	%rax
	xorl	%eax, %eax
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movl	%r13d, -140(%rbp)
	leaq	-224(%rbp), %r13
	movq	%r14, -224(%rbp)
	movl	$-1, -216(%rbp)
	movb	$0, -144(%rbp)
	movups	%xmm0, -136(%rbp)
	call	__fprintf_chk@PLT
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jns	.L392
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L395:
	movsd	-128(%rbp), %xmm1
	movsd	.LC5(%rip), %xmm2
	pxor	%xmm0, %xmm0
	divsd	.LC0(%rip), %xmm1
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movl	$3, %eax
	subsd	%xmm1, %xmm2
	movq	-200(%rbp), %rcx
	movsd	.LC5(%rip), %xmm4
	cvtsi2sdq	-184(%rbp), %xmm0
	divsd	-232(%rbp), %xmm0
	divsd	%xmm2, %xmm4
	movapd	%xmm4, %xmm2
	call	__fprintf_chk@PLT
	testl	%eax, %eax
	js	.L394
.L392:
	movq	%r13, %rdi
	call	*-112(%rbp)
	testb	%al, %al
	jne	.L395
	testl	%r15d, %r15d
	je	.L405
.L396:
.L393:
	endbr64
	xorl	%eax, %eax
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%r14, %rdi
	call	hdr_mean
	movq	%r14, %rdi
	movapd	%xmm0, %xmm3
	divsd	-232(%rbp), %xmm3
	movsd	%xmm3, -240(%rbp)
	call	hdr_stddev
	movq	56(%r14), %rdx
	movsd	-240(%rbp), %xmm3
	movapd	%xmm0, %xmm1
	divsd	-232(%rbp), %xmm1
	testq	%rdx, %rdx
	je	.L406
	movq	32(%r14), %rcx
	movl	16(%r14), %edi
	movl	$64, %eax
	pxor	%xmm2, %xmm2
	movl	24(%r14), %ebx
	movl	40(%r14), %r9d
	orq	%rdx, %rcx
	bsrq	%rcx, %rcx
	xorq	$63, %rcx
	subl	%ecx, %eax
	leal	1(%rbx), %ecx
	subl	%edi, %eax
	subl	%ecx, %eax
	leal	(%rdi,%rax), %ecx
	leal	1(%rdi,%rax), %eax
	sarq	%cl, %rdx
	movslq	%edx, %rsi
	salq	%cl, %rsi
	cmpl	%edx, %r9d
	cmovle	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	leaq	-1(%rsi,%rax), %rax
	cvtsi2sdq	%rax, %xmm2
.L398:
	movq	88(%r14), %rcx
	movl	44(%r14), %r8d
	movapd	%xmm3, %xmm0
	movq	%r12, %rdi
	leaq	CLASSIC_FOOTER(%rip), %rdx
	movl	$1, %esi
	movl	$3, %eax
	divsd	-232(%rbp), %xmm2
	call	__fprintf_chk@PLT
	testl	%eax, %eax
	jns	.L396
	.p2align 4,,10
	.p2align 3
.L394:
	movl	$5, %eax
.L389:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L407
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	leaq	.LC13(%rip), %rcx
	leaq	.LC11(%rip), %r9
	movl	$1, %edx
	movq	%rbx, %rdi
	pushq	%rcx
	leaq	.LC12(%rip), %r8
	movl	$25, %ecx
	movl	$25, %esi
	pushq	%rax
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	popq	%r8
	leaq	.LC10(%rip), %rdx
	popq	%r9
	jmp	.L391
.L406:
	movl	40(%r14), %r9d
	pxor	%xmm2, %xmm2
	jmp	.L398
.L407:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE116:
	.size	hdr_percentiles_print, .-hdr_percentiles_print
	.section	.rodata
	.align 32
	.type	CLASSIC_FOOTER, @object
	.size	CLASSIC_FOOTER, 131
CLASSIC_FOOTER:
	.string	"#[Mean    = %12.3f, StdDeviation   = %12.3f]\n#[Max     = %12.3f, Total count    = %12lu]\n#[Buckets = %12d, SubBuckets     = %12d]\n"
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1079574528
	.align 8
.LC1:
	.long	4277811695
	.long	1072049730
	.align 8
.LC2:
	.long	0
	.long	1073741824
	.align 8
.LC3:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	1072693248
	.section	.rodata.cst16
	.align 16
.LC6:
	.quad	9223372036854775807
	.quad	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	0
	.long	1071644672
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
