	.file	"sampler.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB5791:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE5791:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB5792:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5792:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv:
.LFB5510:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base5MutexC1Ev@PLT
	.cfi_endproc
.LFE5510:
	.size	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.section	.text._ZN2v87sampler11AtomicGuardC2EPSt6atomicIbEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler11AtomicGuardC2EPSt6atomicIbEb
	.type	_ZN2v87sampler11AtomicGuardC2EPSt6atomicIbEb, @function
_ZN2v87sampler11AtomicGuardC2EPSt6atomicIbEb:
.LFB4685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	leaq	-9(%rbp), %r9
	movb	$0, 8(%rdi)
.L13:
	xorl	%eax, %eax
	movb	$0, -9(%rbp)
	lock cmpxchgb	%r8b, (%rsi)
	sete	%cl
	je	.L12
	movb	%al, (%r9)
.L12:
	movb	%cl, 8(%rdi)
	testb	%dl, %dl
	jne	.L11
	testb	%cl, %cl
	je	.L13
.L11:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4685:
	.size	_ZN2v87sampler11AtomicGuardC2EPSt6atomicIbEb, .-_ZN2v87sampler11AtomicGuardC2EPSt6atomicIbEb
	.globl	_ZN2v87sampler11AtomicGuardC1EPSt6atomicIbEb
	.set	_ZN2v87sampler11AtomicGuardC1EPSt6atomicIbEb,_ZN2v87sampler11AtomicGuardC2EPSt6atomicIbEb
	.section	.text._ZN2v87sampler11AtomicGuardD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler11AtomicGuardD2Ev
	.type	_ZN2v87sampler11AtomicGuardD2Ev, @function
_ZN2v87sampler11AtomicGuardD2Ev:
.LFB4688:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	jne	.L22
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%rdi), %rax
	movb	$0, (%rax)
	mfence
	ret
	.cfi_endproc
.LFE4688:
	.size	_ZN2v87sampler11AtomicGuardD2Ev, .-_ZN2v87sampler11AtomicGuardD2Ev
	.globl	_ZN2v87sampler11AtomicGuardD1Ev
	.set	_ZN2v87sampler11AtomicGuardD1Ev,_ZN2v87sampler11AtomicGuardD2Ev
	.section	.text._ZNK2v87sampler11AtomicGuard10is_successEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v87sampler11AtomicGuard10is_successEv
	.type	_ZNK2v87sampler11AtomicGuard10is_successEv, @function
_ZNK2v87sampler11AtomicGuard10is_successEv:
.LFB4690:
	.cfi_startproc
	endbr64
	movzbl	8(%rdi), %eax
	ret
	.cfi_endproc
.LFE4690:
	.size	_ZNK2v87sampler11AtomicGuard10is_successEv, .-_ZNK2v87sampler11AtomicGuard10is_successEv
	.section	.text._ZN2v87sampler14SamplerManager8DoSampleERKNS_13RegisterStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler14SamplerManager8DoSampleERKNS_13RegisterStateE
	.type	_ZN2v87sampler14SamplerManager8DoSampleERKNS_13RegisterStateE, @function
_ZN2v87sampler14SamplerManager8DoSampleERKNS_13RegisterStateE:
.LFB4707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	56(%rdi), %rdx
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$0, -57(%rbp)
	lock cmpxchgb	%cl, (%rdx)
	je	.L40
	movb	%al, -57(%rbp)
.L24:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movq	%rdi, %r12
	call	pthread_self@PLT
	movq	8(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L27
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L57:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L27
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L27
.L29:
	cmpq	%rsi, %r8
	jne	.L57
	movq	24(%rcx), %rax
	movq	16(%rcx), %rbx
	xorl	%r15d, %r15d
	movq	%rax, -72(%rbp)
	cmpq	%rbx, %rax
	je	.L27
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%rbx), %r14
	movl	%r15d, %eax
	xchgb	33(%r14), %al
	testb	%al, %al
	je	.L32
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L32
	movq	%r13, %rdi
	call	_ZN2v87Isolate7IsInUseEv@PLT
	testb	%al, %al
	je	.L32
	call	_ZN2v86Locker8IsActiveEv@PLT
	testb	%al, %al
	jne	.L34
.L35:
	movq	(%r14), %rax
	movq	-80(%rbp), %rsi
	movq	%r14, %rdi
	call	*16(%rax)
.L32:
	addq	$8, %rbx
	cmpq	%rbx, -72(%rbp)
	jne	.L36
.L27:
	movb	$0, 56(%r12)
	mfence
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r13, %rdi
	call	_ZN2v86Locker8IsLockedEPNS_7IsolateE@PLT
	testb	%al, %al
	jne	.L35
	jmp	.L32
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4707:
	.size	_ZN2v87sampler14SamplerManager8DoSampleERKNS_13RegisterStateE, .-_ZN2v87sampler14SamplerManager8DoSampleERKNS_13RegisterStateE
	.section	.text._ZN2v87sampler13SignalHandler20HandleProfilerSignalEiP9siginfo_tPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler13SignalHandler20HandleProfilerSignalEiP9siginfo_tPv
	.type	_ZN2v87sampler13SignalHandler20HandleProfilerSignalEiP9siginfo_tPv, @function
_ZN2v87sampler13SignalHandler20HandleProfilerSignalEiP9siginfo_tPv:
.LFB4714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$27, %edi
	je	.L68
.L58:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movq	168(%rdx), %rax
	movq	$0, -24(%rbp)
	movq	%rax, -48(%rbp)
	movq	160(%rdx), %rax
	movq	%rax, -40(%rbp)
	movq	120(%rdx), %rax
	movq	%rax, -32(%rbp)
	movzbl	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %eax
	testb	%al, %al
	je	.L70
.L61:
	leaq	-48(%rbp), %rsi
	leaq	_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	call	_ZN2v87sampler14SamplerManager8DoSampleERKNS_13RegisterStateE
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L61
	xorl	%eax, %eax
	leaq	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	movl	$0, 57+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movw	%ax, 61+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	leaq	48+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rax
	movq	%rax, _ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movb	$0, 63+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$1, 8+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 16+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 24+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movl	$0x3f800000, 32+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 40+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 48+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movb	$0, 56+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L61
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4714:
	.size	_ZN2v87sampler13SignalHandler20HandleProfilerSignalEiP9siginfo_tPv, .-_ZN2v87sampler13SignalHandler20HandleProfilerSignalEiP9siginfo_tPv
	.section	.text._ZN2v87sampler14SamplerManager8instanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler14SamplerManager8instanceEv
	.type	_ZN2v87sampler14SamplerManager8instanceEv, @function
_ZN2v87sampler14SamplerManager8instanceEv:
.LFB4708:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %eax
	testb	%al, %al
	je	.L82
	leaq	_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L83
	leaq	_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	xorl	%eax, %eax
	leaq	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	movl	$0, 57+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movw	%ax, 61+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	leaq	48+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rax
	movq	%rax, _ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movb	$0, 63+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$1, 8+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 16+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 24+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movl	$0x3f800000, 32+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 40+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 48+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movb	$0, 56+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	call	__cxa_guard_release@PLT
	leaq	_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4708:
	.size	_ZN2v87sampler14SamplerManager8instanceEv, .-_ZN2v87sampler14SamplerManager8instanceEv
	.section	.text._ZN2v87sampler13SignalHandler17FillRegisterStateEPvPNS_13RegisterStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler13SignalHandler17FillRegisterStateEPvPNS_13RegisterStateE
	.type	_ZN2v87sampler13SignalHandler17FillRegisterStateEPvPNS_13RegisterStateE, @function
_ZN2v87sampler13SignalHandler17FillRegisterStateEPvPNS_13RegisterStateE:
.LFB4715:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rax
	movq	%rax, (%rsi)
	movq	160(%rdi), %rax
	movq	%rax, 8(%rsi)
	movq	120(%rdi), %rax
	movq	%rax, 16(%rsi)
	ret
	.cfi_endproc
.LFE4715:
	.size	_ZN2v87sampler13SignalHandler17FillRegisterStateEPvPNS_13RegisterStateE, .-_ZN2v87sampler13SignalHandler17FillRegisterStateEPvPNS_13RegisterStateE
	.section	.text._ZN2v87sampler7SamplerC2EPNS_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler7SamplerC2EPNS_7IsolateE
	.type	_ZN2v87sampler7SamplerC2EPNS_7IsolateE, @function
_ZN2v87sampler7SamplerC2EPNS_7IsolateE:
.LFB4717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v87sampler7SamplerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movq	%rsi, 24(%rdi)
	movb	$0, 8(%rdi)
	movq	$0, 12(%rdi)
	movw	%ax, 32(%rdi)
	movl	$8, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	call	pthread_self@PLT
	movq	%r12, 40(%rbx)
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4717:
	.size	_ZN2v87sampler7SamplerC2EPNS_7IsolateE, .-_ZN2v87sampler7SamplerC2EPNS_7IsolateE
	.globl	_ZN2v87sampler7SamplerC1EPNS_7IsolateE
	.set	_ZN2v87sampler7SamplerC1EPNS_7IsolateE,_ZN2v87sampler7SamplerC2EPNS_7IsolateE
	.section	.text._ZN2v87sampler7SamplerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler7SamplerD2Ev
	.type	_ZN2v87sampler7SamplerD2Ev, @function
_ZN2v87sampler7SamplerD2Ev:
.LFB4720:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v87sampler7SamplerE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L87
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	ret
	.cfi_endproc
.LFE4720:
	.size	_ZN2v87sampler7SamplerD2Ev, .-_ZN2v87sampler7SamplerD2Ev
	.globl	_ZN2v87sampler7SamplerD1Ev
	.set	_ZN2v87sampler7SamplerD1Ev,_ZN2v87sampler7SamplerD2Ev
	.section	.text._ZN2v87sampler7SamplerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler7SamplerD0Ev
	.type	_ZN2v87sampler7SamplerD0Ev, @function
_ZN2v87sampler7SamplerD0Ev:
.LFB4722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v87sampler7SamplerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L90
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L90:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4722:
	.size	_ZN2v87sampler7SamplerD0Ev, .-_ZN2v87sampler7SamplerD0Ev
	.section	.text._ZN2v87sampler7Sampler8DoSampleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler7Sampler8DoSampleEv
	.type	_ZN2v87sampler7Sampler8DoSampleEv, @function
_ZN2v87sampler7Sampler8DoSampleEv:
.LFB4725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v87sampler13SignalHandler6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L110
.L96:
	leaq	8+_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	_ZN2v87sampler13SignalHandler25signal_handler_installed_E(%rip), %ebx
	leaq	8+_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	testb	%bl, %bl
	je	.L95
	movb	$1, 33(%r12)
	movl	$27, %esi
	movq	40(%r12), %rax
	movq	(%rax), %rdi
	call	pthread_kill@PLT
.L95:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rax
	leaq	-80(%rbp), %r13
	movq	%rax, -72(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L96
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L96
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4725:
	.size	_ZN2v87sampler7Sampler8DoSampleEv, .-_ZN2v87sampler7Sampler8DoSampleEv
	.section	.rodata._ZNSt6vectorIPN2v87sampler7SamplerESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v87sampler7SamplerESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v87sampler7SamplerESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v87sampler7SamplerESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIPN2v87sampler7SamplerESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIPN2v87sampler7SamplerESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB5351:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L126
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L122
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L127
.L114:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L121:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L128
	testq	%r13, %r13
	jg	.L117
	testq	%r9, %r9
	jne	.L120
.L118:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L117
.L120:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L127:
	testq	%rsi, %rsi
	jne	.L115
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L118
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$8, %r14d
	jmp	.L114
.L126:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L115:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L114
	.cfi_endproc
.LFE5351:
	.size	_ZNSt6vectorIPN2v87sampler7SamplerESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIPN2v87sampler7SamplerESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRmS8_EEES0_INSB_14_Node_iteratorIS9_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRmS8_EEES0_INSB_14_Node_iteratorIS9_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRmS8_EEES0_INSB_14_Node_iteratorIS9_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRmS8_EEES0_INSB_14_Node_iteratorIS9_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRmS8_EEES0_INSB_14_Node_iteratorIS9_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB5484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$40, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	call	_Znwm@PLT
	movq	(%r14), %r14
	movdqu	(%rbx), %xmm1
	pxor	%xmm0, %xmm0
	movq	$0, (%rax)
	movq	%rax, %r12
	movq	(%rbx), %r8
	xorl	%edx, %edx
	movq	%r14, 8(%rax)
	movups	%xmm1, 16(%rax)
	movq	16(%rbx), %rax
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	movq	8(%r13), %rsi
	movq	%rax, 32(%r12)
	movq	%r14, %rax
	divq	%rsi
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L130
	movq	(%rax), %rbx
	movq	%rdx, %rdi
	movq	8(%rbx), %rcx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L167:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L130
	movq	8(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L130
.L132:
	cmpq	%rcx, %r14
	jne	.L167
	testq	%r8, %r8
	je	.L148
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L148:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	24(%r13), %rdx
	leaq	32(%r13), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rbx
	testb	%al, %al
	jne	.L134
	movq	0(%r13), %r8
.L135:
	leaq	(%r8,%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L144
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rax
	movq	%r12, (%rax)
.L145:
	addq	$1, 24(%r13)
	addq	$24, %rsp
	movq	%r12, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L168
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L169
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r13), %r10
	movq	%rax, %r8
.L137:
	movq	16(%r13), %rsi
	movq	$0, 16(%r13)
	testq	%rsi, %rsi
	je	.L139
	xorl	%edi, %edi
	leaq	16(%r13), %r9
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L141:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L142:
	testq	%rsi, %rsi
	je	.L139
.L140:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%rbx
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L141
	movq	16(%r13), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r13)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L149
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L139:
	movq	0(%r13), %rdi
	cmpq	%rdi, %r10
	je	.L143
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L143:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%rbx, 8(%r13)
	divq	%rbx
	movq	%r8, 0(%r13)
	leaq	0(,%rdx,8), %r15
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L144:
	movq	16(%r13), %rdx
	movq	%r12, 16(%r13)
	movq	%rdx, (%r12)
	testq	%rdx, %rdx
	je	.L146
	movq	8(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r13)
	movq	%r12, (%r8,%rdx,8)
	movq	0(%r13), %rax
	addq	%r15, %rax
.L146:
	leaq	16(%r13), %rdx
	movq	%rdx, (%rax)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%rdx, %rdi
	jmp	.L142
.L168:
	leaq	48(%r13), %r8
	movq	$0, 48(%r13)
	movq	%r8, %r10
	jmp	.L137
.L169:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE5484:
	.size	_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRmS8_EEES0_INSB_14_Node_iteratorIS9_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRmS8_EEES0_INSB_14_Node_iteratorIS9_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPPN2v87sampler7SamplerESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPPN2v87sampler7SamplerESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPPN2v87sampler7SamplerESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPPN2v87sampler7SamplerESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPPN2v87sampler7SamplerESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag:
.LFB5489:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	subq	%rdi, %r8
	movq	%r8, %rax
	sarq	$5, %r8
	sarq	$3, %rax
	testq	%r8, %r8
	jle	.L171
	salq	$5, %r8
	movq	(%rdx), %rcx
	addq	%rdi, %r8
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L172:
	cmpq	8(%rdi), %rcx
	je	.L189
	cmpq	16(%rdi), %rcx
	je	.L190
	cmpq	24(%rdi), %rcx
	je	.L191
	addq	$32, %rdi
	cmpq	%r8, %rdi
	je	.L192
.L177:
	cmpq	(%rdi), %rcx
	jne	.L172
.L188:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
.L171:
	cmpq	$2, %rax
	je	.L178
	cmpq	$3, %rax
	je	.L179
	cmpq	$1, %rax
	je	.L193
.L181:
	movq	%rsi, %rax
	ret
.L179:
	movq	(%rdx), %rax
	cmpq	%rax, (%rdi)
	je	.L188
	addq	$8, %rdi
	jmp	.L182
.L193:
	movq	(%rdx), %rax
.L183:
	cmpq	(%rdi), %rax
	jne	.L181
	jmp	.L188
.L178:
	movq	(%rdx), %rax
.L182:
	cmpq	%rax, (%rdi)
	je	.L188
	addq	$8, %rdi
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	leaq	16(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE5489:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPPN2v87sampler7SamplerESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPPN2v87sampler7SamplerESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag
	.section	.text._ZN2v87sampler14SamplerManager10AddSamplerEPNS0_7SamplerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler14SamplerManager10AddSamplerEPNS0_7SamplerE
	.type	_ZN2v87sampler14SamplerManager10AddSamplerEPNS0_7SamplerE, @function
_ZN2v87sampler14SamplerManager10AddSamplerEPNS0_7SamplerE:
.LFB4695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	56(%rdi), %rdx
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.L196:
	xorl	%eax, %eax
	movb	$0, -64(%rbp)
	lock cmpxchgb	%cl, (%rdx)
	jne	.L196
	movq	-88(%rbp), %rax
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	40(%rax), %rax
	movq	(%rax), %rsi
	movq	%rsi, %rax
	movq	%rsi, -72(%rbp)
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L197
	movq	(%rax), %rbx
	movq	8(%rbx), %rdi
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L219:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L197
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	jne	.L197
.L199:
	cmpq	%rdi, %rsi
	jne	.L219
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rdi
	leaq	-88(%rbp), %r13
	movq	%r13, %rdx
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPPN2v87sampler7SamplerESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	24(%rbx), %rsi
	cmpq	%rax, %rsi
	je	.L220
.L201:
	movb	$0, 56(%r12)
	mfence
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	call	_ZNSt6vectorIPN2v87sampler7SamplerESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	%r12, %rdi
	leaq	-72(%rbp), %rsi
	movq	%r13, %rdx
	call	_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRmS8_EEES0_INSB_14_Node_iteratorIS9_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZdlPv@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L220:
	cmpq	32(%rbx), %rsi
	je	.L202
	movq	-88(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 24(%rbx)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L202:
	leaq	16(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIPN2v87sampler7SamplerESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L201
.L221:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4695:
	.size	_ZN2v87sampler14SamplerManager10AddSamplerEPNS0_7SamplerE, .-_ZN2v87sampler14SamplerManager10AddSamplerEPNS0_7SamplerE
	.section	.text._ZN2v87sampler7Sampler5StartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler7Sampler5StartEv
	.type	_ZN2v87sampler7Sampler5StartEv, @function
_ZN2v87sampler7Sampler5StartEv:
.LFB4723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$1, 32(%rdi)
	movzbl	_ZN2v87sampler13SignalHandler6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L240
.L223:
	leaq	8+_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	_ZN2v87sampler13SignalHandler13client_count_E(%rip), %eax
	addl	$1, %eax
	movl	%eax, _ZN2v87sampler13SignalHandler13client_count_E(%rip)
	cmpl	$1, %eax
	je	.L241
.L225:
	leaq	8+_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movzbl	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %eax
	testb	%al, %al
	je	.L242
.L227:
	movq	%r12, %rsi
	leaq	_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	call	_ZN2v87sampler14SamplerManager10AddSamplerEPNS0_7SamplerE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L243
	addq	$160, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -176(%rbp)
	leaq	8+_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-176(%rbp), %r13
	movq	%rax, -168(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L223
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L227
	xorl	%eax, %eax
	leaq	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	movl	$0, 57+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movw	%ax, 61+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	leaq	48+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rax
	movq	%rax, _ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movb	$0, 63+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$1, 8+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 16+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 24+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movl	$0x3f800000, 32+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 40+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 48+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movb	$0, 56+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	_ZN2v87sampler13SignalHandler20HandleProfilerSignalEiP9siginfo_tPv(%rip), %rax
	leaq	-168(%rbp), %rdi
	leaq	-176(%rbp), %r13
	movq	%rax, -176(%rbp)
	call	sigemptyset@PLT
	leaq	_ZN2v87sampler13SignalHandler19old_signal_handler_E(%rip), %rdx
	movq	%r13, %rsi
	movl	$27, %edi
	movl	$268435460, -40(%rbp)
	call	sigaction@PLT
	testl	%eax, %eax
	sete	_ZN2v87sampler13SignalHandler25signal_handler_installed_E(%rip)
	jmp	.L225
.L243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4723:
	.size	_ZN2v87sampler7Sampler5StartEv, .-_ZN2v87sampler7Sampler5StartEv
	.section	.text._ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE,"axG",@progbits,_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE
	.type	_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE, @function
_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE:
.LFB5501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rsi
	movq	8(%r12), %rax
	movq	(%rbx), %r8
	divq	%rsi
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %rdi
	leaq	(%r8,%r10), %r9
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %r12
	jne	.L245
	movq	(%r12), %r13
	cmpq	%rcx, %rax
	je	.L260
	testq	%r13, %r13
	je	.L248
	movq	8(%r13), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L248
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r12), %r13
.L248:
	movq	%r13, (%rcx)
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L250
	call	_ZdlPv@PLT
.L250:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L251
	movq	8(%r13), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %rdi
	je	.L248
	movq	%rcx, (%r8,%rdx,8)
	addq	(%rbx), %r10
	movq	(%r10), %rax
	movq	%r10, %r9
.L247:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L261
.L249:
	movq	$0, (%r9)
	movq	(%r12), %r13
	jmp	.L248
.L251:
	movq	%rcx, %rax
	jmp	.L247
.L261:
	movq	%r13, 16(%rbx)
	jmp	.L249
	.cfi_endproc
.LFE5501:
	.size	_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE, .-_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE
	.section	.text.unlikely._ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE,"ax",@progbits
	.align 2
.LCOLDB2:
	.section	.text._ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE,"ax",@progbits
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE
	.type	_ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE, @function
_ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE:
.LFB4706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	56(%rdi), %rdx
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rsi, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.L264:
	xorl	%eax, %eax
	movb	$0, -41(%rbp)
	lock cmpxchgb	%cl, (%rdx)
	jne	.L264
	movq	-56(%rbp), %rax
	movq	8(%rbx), %rcx
	xorl	%edx, %edx
	movq	40(%rax), %rax
	movq	(%rax), %rdi
	movq	%rdi, %rax
	divq	%rcx
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L265
	movq	(%rax), %r12
	movq	8(%r12), %rsi
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L291:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L265
	movq	8(%r12), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	jne	.L290
.L267:
	cmpq	%rsi, %rdi
	jne	.L291
	movq	24(%r12), %r13
	movq	16(%r12), %rdi
	leaq	-56(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPPN2v87sampler7SamplerESt6vectorIS5_SaIS5_EEEENS0_5__ops16_Iter_equals_valIKS5_EEET_SF_SF_T0_St26random_access_iterator_tag
	movq	%rax, %rcx
	cmpq	%rax, %r13
	je	.L292
	leaq	8(%rcx), %rax
	cmpq	%rax, %r13
	je	.L269
	.p2align 4,,10
	.p2align 3
.L271:
	movq	(%rax), %rdx
	cmpq	-56(%rbp), %rdx
	je	.L270
	movq	%rdx, (%rcx)
	addq	$8, %rcx
.L270:
	addq	$8, %rax
	cmpq	%rax, %r13
	jne	.L271
	movq	24(%r12), %rsi
	cmpq	%rcx, %r13
	je	.L278
.L276:
	movq	%rsi, %rdx
	subq	%r13, %rdx
	cmpq	%rsi, %r13
	je	.L272
	movq	%r13, %rsi
	movq	%rcx, %rdi
	call	memmove@PLT
	movq	24(%r12), %rsi
	movq	%rax, %rcx
	movq	%rsi, %rdx
	subq	%r13, %rdx
.L272:
	addq	%rdx, %rcx
	cmpq	%rcx, %rsi
	je	.L268
	movq	%rcx, 24(%r12)
.L268:
	cmpq	%rcx, 16(%r12)
	je	.L293
.L273:
	movb	$0, 56(%rbx)
	mfence
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L294
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	movq	24(%r12), %rcx
	cmpq	%rcx, 16(%r12)
	jne	.L273
.L293:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNSt10_HashtableImSt4pairIKmSt6vectorIPN2v87sampler7SamplerESaIS6_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENSB_20_Node_const_iteratorIS9_Lb0ELb0EEE
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L269:
	movq	24(%r12), %rsi
	jmp	.L276
.L294:
	call	__stack_chk_fail@PLT
.L278:
	movq	%rsi, %rcx
	jmp	.L268
.L290:
	jmp	.L265
	.cfi_endproc
	.section	.text.unlikely._ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE
	.cfi_startproc
	.type	_ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE.cold, @function
_ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE.cold:
.LFSB4706:
.L265:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	24, %rax
	ud2
	.cfi_endproc
.LFE4706:
	.section	.text._ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE
	.size	_ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE, .-_ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE
	.section	.text.unlikely._ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE
	.size	_ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE.cold, .-_ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE.cold
.LCOLDE2:
	.section	.text._ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE
.LHOTE2:
	.section	.text._ZN2v87sampler7Sampler4StopEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v87sampler7Sampler4StopEv
	.type	_ZN2v87sampler7Sampler4StopEv, @function
_ZN2v87sampler7Sampler4StopEv:
.LFB4724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %eax
	testb	%al, %al
	je	.L313
.L297:
	movq	%rbx, %rsi
	leaq	_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	call	_ZN2v87sampler14SamplerManager13RemoveSamplerEPNS0_7SamplerE
	movzbl	_ZN2v87sampler13SignalHandler6mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L314
.L299:
	leaq	8+_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	subl	$1, _ZN2v87sampler13SignalHandler13client_count_E(%rip)
	jne	.L301
	cmpb	$0, _ZN2v87sampler13SignalHandler25signal_handler_installed_E(%rip)
	jne	.L315
.L301:
	leaq	8+_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movb	$0, 32(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v87sampler13SignalHandler6mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L299
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L313:
	leaq	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L297
	xorl	%eax, %eax
	leaq	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rdi
	movl	$0, 57+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movw	%ax, 61+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	leaq	48+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip), %rax
	movq	%rax, _ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movb	$0, 63+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$1, 8+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 16+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 24+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movl	$0x3f800000, 32+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 40+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movq	$0, 48+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	movb	$0, 56+_ZZN2v87sampler14SamplerManager8instanceEvE8instance(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L315:
	xorl	%edx, %edx
	leaq	_ZN2v87sampler13SignalHandler19old_signal_handler_E(%rip), %rsi
	movl	$27, %edi
	call	sigaction@PLT
	movb	$0, _ZN2v87sampler13SignalHandler25signal_handler_installed_E(%rip)
	jmp	.L301
.L316:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4724:
	.size	_ZN2v87sampler7Sampler4StopEv, .-_ZN2v87sampler7Sampler4StopEv
	.weak	_ZTVN2v87sampler7SamplerE
	.section	.data.rel.ro._ZTVN2v87sampler7SamplerE,"awG",@progbits,_ZTVN2v87sampler7SamplerE,comdat
	.align 8
	.type	_ZTVN2v87sampler7SamplerE, @object
	.size	_ZTVN2v87sampler7SamplerE, 40
_ZTVN2v87sampler7SamplerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.globl	_ZN2v87sampler13SignalHandler25signal_handler_installed_E
	.section	.bss._ZN2v87sampler13SignalHandler25signal_handler_installed_E,"aw",@nobits
	.type	_ZN2v87sampler13SignalHandler25signal_handler_installed_E, @object
	.size	_ZN2v87sampler13SignalHandler25signal_handler_installed_E, 1
_ZN2v87sampler13SignalHandler25signal_handler_installed_E:
	.zero	1
	.globl	_ZN2v87sampler13SignalHandler19old_signal_handler_E
	.section	.bss._ZN2v87sampler13SignalHandler19old_signal_handler_E,"aw",@nobits
	.align 32
	.type	_ZN2v87sampler13SignalHandler19old_signal_handler_E, @object
	.size	_ZN2v87sampler13SignalHandler19old_signal_handler_E, 152
_ZN2v87sampler13SignalHandler19old_signal_handler_E:
	.zero	152
	.globl	_ZN2v87sampler13SignalHandler13client_count_E
	.section	.bss._ZN2v87sampler13SignalHandler13client_count_E,"aw",@nobits
	.align 4
	.type	_ZN2v87sampler13SignalHandler13client_count_E, @object
	.size	_ZN2v87sampler13SignalHandler13client_count_E, 4
_ZN2v87sampler13SignalHandler13client_count_E:
	.zero	4
	.globl	_ZN2v87sampler13SignalHandler6mutex_E
	.section	.bss._ZN2v87sampler13SignalHandler6mutex_E,"aw",@nobits
	.align 32
	.type	_ZN2v87sampler13SignalHandler6mutex_E, @object
	.size	_ZN2v87sampler13SignalHandler6mutex_E, 48
_ZN2v87sampler13SignalHandler6mutex_E:
	.zero	48
	.section	.bss._ZGVZN2v87sampler14SamplerManager8instanceEvE8instance,"aw",@nobits
	.align 8
	.type	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance, @object
	.size	_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance, 8
_ZGVZN2v87sampler14SamplerManager8instanceEvE8instance:
	.zero	8
	.section	.bss._ZZN2v87sampler14SamplerManager8instanceEvE8instance,"aw",@nobits
	.align 32
	.type	_ZZN2v87sampler14SamplerManager8instanceEvE8instance, @object
	.size	_ZZN2v87sampler14SamplerManager8instanceEvE8instance, 64
_ZZN2v87sampler14SamplerManager8instanceEvE8instance:
	.zero	64
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
