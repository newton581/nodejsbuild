	.file	"class-debug-reader-generator.cc"
	.text
	.section	.text._ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev,"axG",@progbits,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.type	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev, @function
_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev:
.LFB7720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	56(%rbx), %rdi
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt6localeD1Ev@PLT
	.cfi_endproc
.LFE7720:
	.size	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev, .-_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	.set	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED2Ev
	.section	.text._ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev,"axG",@progbits,_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev
	.type	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev, @function
_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev:
.LFB7722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	56(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZNSt6localeD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7722:
	.size	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev, .-_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED0Ev
	.section	.rodata._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Type '"
	.section	.rodata._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"' requires a constexpr representation"
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB5:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB5:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB12808:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA12808
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$472, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -504(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
.LEHB0:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE0:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r12
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB1:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE1:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	-368(%rbp), %r14
	movq	.LC2(%rip), %xmm0
	movq	%r14, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movhps	.LC3(%rip), %xmm0
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	movl	$24, -360(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -488(%rbp)
	movq	%rax, -352(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rax, -496(%rbp)
.LEHB2:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE2:
	movl	$6, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
.LEHB3:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$37, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE3:
	movq	-384(%rbp), %rax
	leaq	-464(%rbp), %rbx
	movq	$0, -472(%rbp)
	leaq	-480(%rbp), %r12
	movq	%rbx, -480(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L13
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L35
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
.LEHB4:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE4:
.L15:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
.LEHB5:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE5:
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	.LC2(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC4(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-488(%rbp), %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r13, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$472, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
.LEHB6:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE6:
	jmp	.L15
.L36:
	call	__stack_chk_fail@PLT
.L25:
	endbr64
	movq	%rax, %r12
	jmp	.L19
.L28:
	endbr64
	movq	%rax, %rbx
	jmp	.L12
.L29:
	endbr64
	movq	%rax, %rbx
	jmp	.L10
.L30:
	endbr64
	movq	%rax, %r12
	jmp	.L17
.L27:
	endbr64
	movq	%rax, %rbx
	jmp	.L11
.L26:
	endbr64
	movq	%rax, %r12
	jmp	.L22
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA12808:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE12808-.LLSDACSB12808
.LLSDACSB12808:
	.uleb128 .LEHB0-.LFB12808
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L27-.LFB12808
	.uleb128 0
	.uleb128 .LEHB1-.LFB12808
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L29-.LFB12808
	.uleb128 0
	.uleb128 .LEHB2-.LFB12808
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L28-.LFB12808
	.uleb128 0
	.uleb128 .LEHB3-.LFB12808
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L25-.LFB12808
	.uleb128 0
	.uleb128 .LEHB4-.LFB12808
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L30-.LFB12808
	.uleb128 0
	.uleb128 .LEHB5-.LFB12808
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L26-.LFB12808
	.uleb128 0
	.uleb128 .LEHB6-.LFB12808
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L30-.LFB12808
	.uleb128 0
.LLSDACSE12808:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC12808
	.type	_ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB12808:
.L17:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	-504(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB7:
	call	_Unwind_Resume@PLT
.L12:
	movq	-496(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L11:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE7:
.L10:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L11
.L22:
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L19
	call	_ZdlPv@PLT
	jmp	.L19
	.cfi_endproc
.LFE12808:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC12808:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC12808-.LLSDACSBC12808
.LLSDACSBC12808:
	.uleb128 .LEHB7-.LCOLDB5
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0
	.uleb128 0
.LLSDACSEC12808:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE5:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE5:
	.section	.text._ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm:
.LFB11542:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11542
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L59
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L60
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB8:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L39:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L41
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L44:
	testq	%rsi, %rsi
	je	.L41
.L42:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L43
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L48
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L42
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L39
.L60:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE8:
.L49:
	endbr64
	movq	%rax, %rdi
.L46:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB9:
	call	__cxa_rethrow@PLT
.LEHE9:
.L50:
	endbr64
	movq	%rax, %r12
.L47:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB10:
	call	_Unwind_Resume@PLT
.LEHE10:
	.cfi_endproc
.LFE11542:
	.section	.gcc_except_table._ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA11542:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11542-.LLSDATTD11542
.LLSDATTD11542:
	.byte	0x1
	.uleb128 .LLSDACSE11542-.LLSDACSB11542
.LLSDACSB11542:
	.uleb128 .LEHB8-.LFB11542
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L49-.LFB11542
	.uleb128 0x1
	.uleb128 .LEHB9-.LFB11542
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L50-.LFB11542
	.uleb128 0
	.uleb128 .LEHB10-.LFB11542
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0
	.uleb128 0
.LLSDACSE11542:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11542:
	.section	.text._ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	.type	_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm, @function
_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm:
.LFB11098:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11098
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB11:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE11:
	testb	%al, %al
	je	.L62
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB12:
	call	_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE9_M_rehashEmRKm
.LEHE12:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L62:
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L63
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L64:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L65
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L65:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L64
.L76:
	call	__stack_chk_fail@PLT
.L69:
	endbr64
	movq	%rax, %rdi
.L66:
	call	__cxa_begin_catch@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB13:
	call	__cxa_rethrow@PLT
.LEHE13:
.L70:
	endbr64
	movq	%rax, %r12
.L67:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB14:
	call	_Unwind_Resume@PLT
.LEHE14:
	.cfi_endproc
.LFE11098:
	.section	.gcc_except_table._ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,"aG",@progbits,_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,comdat
	.align 4
.LLSDA11098:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11098-.LLSDATTD11098
.LLSDATTD11098:
	.byte	0x1
	.uleb128 .LLSDACSE11098-.LLSDACSB11098
.LLSDACSB11098:
	.uleb128 .LEHB11-.LFB11098
	.uleb128 .LEHE11-.LEHB11
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB12-.LFB11098
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L69-.LFB11098
	.uleb128 0x1
	.uleb128 .LEHB13-.LFB11098
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L70-.LFB11098
	.uleb128 0
	.uleb128 .LEHB14-.LFB11098
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
.LLSDACSE11098:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11098:
	.section	.text._ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,comdat
	.size	_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm, .-_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"Decompress(value, address_)"
.LC7:
	.string	"value"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1
.LC9:
	.string	"\nclass Tq"
.LC10:
	.string	" : public Tq"
.LC11:
	.string	" {\n"
.LC12:
	.string	" public:\n"
.LC13:
	.string	"  inline Tq"
.LC14:
	.string	"(uintptr_t address) : Tq"
.LC15:
	.string	"(address) {}\n"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.8
	.align 8
.LC16:
	.string	"  std::vector<std::unique_ptr<ObjectProperty>> GetProperties(d::MemoryAccessor accessor) const override;\n"
	.align 8
.LC17:
	.string	"  const char* GetName() const override;\n"
	.align 8
.LC18:
	.string	"  void Visit(TqObjectVisitor* visitor) const override;\n"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1
.LC19:
	.string	"\nconst char* Tq"
.LC20:
	.string	"::GetName() const {\n"
.LC21:
	.string	"  return \"v8::internal::"
.LC22:
	.string	"\";\n"
.LC23:
	.string	"}\n"
.LC24:
	.string	"\nvoid Tq"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.8
	.align 8
.LC25:
	.string	"::Visit(TqObjectVisitor* visitor) const {\n"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1
.LC26:
	.string	"  visitor->Visit"
.LC27:
	.string	"(this);\n"
.LC28:
	.string	"  virtual void Visit"
.LC29:
	.string	"(const Tq"
.LC30:
	.string	"* object) {\n"
.LC31:
	.string	"    Visit"
.LC32:
	.string	"(object);\n"
.LC33:
	.string	"  }\n"
.LC34:
	.string	"};\n"
.LC35:
	.string	"uintptr_t"
.LC36:
	.string	"i::Tagged_t"
.LC37:
	.string	"basic_string::append"
.LC38:
	.string	"v8::internal::"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.8
	.align 8
.LC39:
	.string	" /*Failing? Ensure constexpr type name is fully qualified and necessary #includes are in debug-helper-internal.h*/"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1
.LC40:
	.string	"Get"
.LC41:
	.string	"Value"
.LC42:
	.string	"Address"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.8
	.align 8
.LC43:
	.string	"i::PlatformSmiTagging::SmiToInt(indexed_field_count.value)"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1
.LC44:
	.string	"indexed_field_count.value"
.LC45:
	.string	"Unsupported index type: "
.LC46:
	.string	"  Value<"
.LC47:
	.string	"> indexed_field_count = Get"
.LC48:
	.string	"Value(accessor);\n"
.LC49:
	.string	", "
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.8
	.align 8
.LC50:
	.string	", GetArrayKind(indexed_field_count.validity)"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1
.LC51:
	.string	", size_t offset"
.LC52:
	.string	" + offset * sizeof(value)"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.8
	.align 8
.LC53:
	.string	"  result.push_back(v8::base::make_unique<ObjectProperty>(\""
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1
.LC54:
	.string	"\", \""
.LC55:
	.string	"\", "
.LC56:
	.string	"()"
.LC57:
	.string	"));\n"
.LC58:
	.string	"  uintptr_t "
.LC59:
	.string	"() const;\n"
.LC60:
	.string	"> "
.LC61:
	.string	"(d::MemoryAccessor accessor "
.LC62:
	.string	") const;\n"
.LC63:
	.string	"\nuintptr_t Tq"
.LC64:
	.string	"::"
.LC65:
	.string	"() const {\n"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.8
	.align 8
.LC66:
	.string	"  return address_ - i::kHeapObjectTag + "
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1
.LC67:
	.string	";\n"
.LC68:
	.string	"\nValue<"
.LC69:
	.string	"> Tq"
.LC70:
	.string	"(d::MemoryAccessor accessor"
.LC71:
	.string	") const {\n"
.LC72:
	.string	"  "
.LC73:
	.string	" value{};\n"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.8
	.align 8
.LC74:
	.string	"  d::MemoryAccessResult validity = accessor("
	.align 8
.LC75:
	.string	", reinterpret_cast<uint8_t*>(&value), sizeof(value));\n"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1
.LC76:
	.string	"  return {validity, "
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.8
	.align 8
.LC77:
	.string	"\nstd::vector<std::unique_ptr<ObjectProperty>> Tq"
	.align 8
.LC78:
	.string	"::GetProperties(d::MemoryAccessor accessor) const {\n"
	.align 8
.LC79:
	.string	"  std::vector<std::unique_ptr<ObjectProperty>> result = Tq"
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.str1.1
.LC80:
	.string	"::GetProperties(accessor);\n"
.LC81:
	.string	"  return result;\n"
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE,"ax",@progbits
.LCOLDB82:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE,"ax",@progbits
.LHOTB82:
	.p2align 4
	.type	_ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE, @function
_ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE:
.LFB7031:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7031
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1784, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -1680(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$1, 176(%rdi)
	jne	.L503
.L77:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L504
	addq	$1784, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	movq	%rdi, %r13
	movq	8(%r8), %rdi
	movq	%rdx, %r12
	xorl	%edx, %edx
	movq	%r13, %rax
	movq	%rcx, %r14
	movq	%r8, %rbx
	divq	%rdi
	movq	(%r8), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L79
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L505:
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r15
	jne	.L79
.L81:
	cmpq	%rsi, %r13
	je	.L77
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L505
.L79:
	movl	$16, %edi
.LEHB15:
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r13, 8(%rax)
	call	_ZNSt10_HashtableIPKN2v88internal6torque9ClassTypeES5_SaIS5_ENSt8__detail9_IdentityESt8equal_toIS5_ESt4hashIS5_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	movq	16(%r13), %r9
	testq	%r9, %r9
	je	.L82
	cmpl	$5, 8(%r9)
	jne	.L329
	movq	-1680(%rbp), %rsi
	movq	%r9, %rdi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r9, -1592(%rbp)
	call	_ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE
.LEHE15:
	movq	-1592(%rbp), %r9
.L82:
	movq	112(%r13), %rbx
	leaq	-1552(%rbp), %rax
	movq	120(%r13), %r15
	movq	%rax, -1760(%rbp)
	movq	%rax, -1568(%rbp)
	movq	%rbx, %rax
	addq	%r15, %rax
	je	.L84
	testq	%rbx, %rbx
	je	.L506
.L84:
	movq	%r15, -1576(%rbp)
	cmpq	$15, %r15
	ja	.L507
	cmpq	$1, %r15
	jne	.L87
	movzbl	(%rbx), %eax
	movb	%al, -1552(%rbp)
	movq	-1760(%rbp), %rax
.L88:
	movq	%r15, -1560(%rbp)
	movb	$0, (%rax,%r15)
	leaq	-1520(%rbp), %rax
	movq	%rax, -1768(%rbp)
	movq	%rax, -1536(%rbp)
	testq	%r9, %r9
	je	.L508
	movq	112(%r9), %rbx
	movq	120(%r9), %r15
	movq	%rbx, %rax
	addq	%r15, %rax
	je	.L91
	testq	%rbx, %rbx
	je	.L509
.L91:
	movq	%r15, -1576(%rbp)
	cmpq	$15, %r15
	ja	.L510
	cmpq	$1, %r15
	jne	.L94
	movzbl	(%rbx), %eax
	movb	%al, -1520(%rbp)
	movq	-1768(%rbp), %rax
.L95:
	movq	%r15, -1528(%rbp)
	movb	$0, (%rax,%r15)
.L90:
	movq	-1680(%rbp), %rbx
	movl	$9, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
.LEHB16:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1528(%rbp), %rdx
	movq	-1536(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$11, %edx
	leaq	.LC13(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$24, %edx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1528(%rbp), %rdx
	movq	-1536(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$13, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$105, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$40, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$55, %edx
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$15, %edx
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$20, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$24, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$3, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$8, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$42, %edx
	leaq	.LC25(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$16, %edx
	leaq	.LC26(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$8, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$20, %edx
	leaq	.LC28(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC29(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$12, %edx
	leaq	.LC30(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1528(%rbp), %rdx
	movq	-1536(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$10, %edx
	leaq	.LC32(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$4, %edx
	leaq	.LC33(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE16:
	leaq	-720(%rbp), %rax
	leaq	-848(%rbp), %r15
	movq	%rax, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rbx
	movq	%r15, -1816(%rbp)
	movq	%rax, -1752(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rbx, -720(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movw	%r8w, -496(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -848(%rbp)
	movq	$0, -504(%rbp)
	movups	%xmm0, -488(%rbp)
	movups	%xmm0, -472(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -848(%rbp,%rax)
	movq	-848(%rbp), %rax
	movq	$0, -840(%rbp)
	addq	-24(%rax), %r15
	movq	%r15, %rdi
.LEHB17:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE17:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rcx, -832(%rbp)
	movq	-24(%rcx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -832(%rbp,%rax)
	movq	-832(%rbp), %rax
	leaq	-832(%rbp), %rcx
	movq	%rcx, -1720(%rbp)
	addq	-24(%rax), %rcx
	movq	%rcx, %rdi
.LEHB18:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE18:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	leaq	-824(%rbp), %r15
	movq	.LC2(%rip), %xmm1
	movq	%r14, -848(%rbp)
	movq	-24(%r14), %rax
	movhps	.LC3(%rip), %xmm1
	movq	%rcx, -848(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -848(%rbp)
	addq	$80, %rcx
	movq	%rcx, -720(%rbp)
	leaq	-768(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1776(%rbp)
	movaps	%xmm1, -1808(%rbp)
	movaps	%xmm1, -832(%rbp)
	movaps	%xmm0, -816(%rbp)
	movaps	%xmm0, -800(%rbp)
	movaps	%xmm0, -784(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r15, %rsi
	movq	-1752(%rbp), %rdi
	movq	%rcx, -824(%rbp)
	leaq	-736(%rbp), %rcx
	movl	$24, -760(%rbp)
	movq	%rcx, -1784(%rbp)
	movq	%rcx, -752(%rbp)
	movq	$0, -744(%rbp)
	movb	$0, -736(%rbp)
.LEHB19:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE19:
	cmpb	$0, 72(%r13)
	jne	.L104
	movq	0(%r13), %rax
	movq	%r13, %rdi
.LEHB20:
	call	*96(%rax)
.LEHE20:
.L104:
	movq	88(%r13), %rax
	movq	80(%r13), %r15
	movq	%rax, -1728(%rbp)
	cmpq	%rax, %r15
	jne	.L102
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L123:
	movq	(%r14), %rax
	movq	%r14, %rdi
.LEHB21:
	call	*56(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L511
	leaq	-1280(%rbp), %rdi
	call	_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev@PLT
	movq	-1280(%rbp), %rax
	leaq	-1264(%rbp), %rbx
	movq	-1408(%rbp), %rdi
	movq	-1272(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L512
	movq	-1264(%rbp), %rcx
	cmpq	-1624(%rbp), %rdi
	je	.L513
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	-1392(%rbp), %rsi
	movq	%rax, -1408(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -1400(%rbp)
	testq	%rdi, %rdi
	je	.L167
	movq	%rdi, -1280(%rbp)
	movq	%rsi, -1264(%rbp)
.L165:
	movq	$0, -1272(%rbp)
	movb	$0, (%rdi)
	movq	-1280(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L168
	call	_ZdlPv@PLT
.L168:
	leaq	-1408(%rbp), %rsi
	leaq	-1376(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE21:
	movq	-1408(%rbp), %rax
	movq	-1400(%rbp), %r14
	leaq	-1072(%rbp), %rbx
	movq	%rbx, -1088(%rbp)
	movq	%rax, %rcx
	movq	%rax, -1664(%rbp)
	addq	%r14, %rcx
	je	.L169
	testq	%rax, %rax
	je	.L514
.L169:
	movq	%r14, -1576(%rbp)
	cmpq	$15, %r14
	ja	.L515
	cmpq	$1, %r14
	jne	.L172
	movq	-1664(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1072(%rbp)
	movq	%rbx, %rax
.L173:
	movq	%r14, -1080(%rbp)
	movb	$0, (%rax,%r14)
	movabsq	$4611686018427387903, %rax
	subq	-1080(%rbp), %rax
	cmpq	$113, %rax
	jbe	.L516
	leaq	-1088(%rbp), %rdi
	movl	$114, %edx
	leaq	.LC39(%rip), %rsi
.LEHB22:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE22:
	movq	-1088(%rbp), %rax
	movq	-1472(%rbp), %rdi
	movq	-1080(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L517
	movq	-1072(%rbp), %rcx
	cmpq	-1640(%rbp), %rdi
	je	.L518
	movq	%rdx, %xmm0
	movq	%rcx, %xmm4
	movq	-1456(%rbp), %rsi
	movq	%rax, -1472(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -1464(%rbp)
	testq	%rdi, %rdi
	je	.L184
	movq	%rdi, -1088(%rbp)
	movq	%rsi, -1072(%rbp)
.L182:
	movq	$0, -1080(%rbp)
	movb	$0, (%rdi)
	movq	-1088(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L185
	call	_ZdlPv@PLT
.L185:
	movq	-1632(%rbp), %rsi
	leaq	-1440(%rbp), %rdi
.LEHB23:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.L157:
	leaq	-912(%rbp), %r14
	leaq	48(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE23:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC40(%rip), %rcx
.LEHB24:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE24:
	movq	%r13, -448(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L519
	movq	%rcx, -448(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -432(%rbp)
.L187:
	movq	8(%rax), %rcx
	movq	%rcx, -440(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-440(%rbp), %rax
	cmpq	$4, %rax
	jbe	.L520
	movq	-1592(%rbp), %rdi
	movl	$5, %edx
	leaq	.LC41(%rip), %rsi
.LEHB25:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE25:
	leaq	-1232(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -1248(%rbp)
	movq	(%rax), %rcx
	movq	%rbx, -1688(%rbp)
	cmpq	%rdx, %rcx
	je	.L521
	movq	%rcx, -1248(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -1232(%rbp)
.L190:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -1240(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L191
	call	_ZdlPv@PLT
.L191:
	movq	-912(%rbp), %rdi
	leaq	-896(%rbp), %rax
	movq	%rax, -1632(%rbp)
	cmpq	%rax, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	leaq	48(%r15), %rsi
	movq	%r14, %rdi
.LEHB26:
	call	_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE26:
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	.LC40(%rip), %rcx
.LEHB27:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE27:
	movq	%r13, -448(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L522
	movq	%rcx, -448(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -432(%rbp)
.L194:
	movq	8(%rax), %rcx
	movq	%rcx, -440(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-440(%rbp), %rax
	cmpq	$6, %rax
	jbe	.L523
	movq	-1592(%rbp), %rdi
	movl	$7, %edx
	leaq	.LC42(%rip), %rsi
.LEHB28:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE28:
	leaq	-1200(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -1216(%rbp)
	movq	(%rax), %rcx
	movq	%rbx, -1696(%rbp)
	cmpq	%rdx, %rcx
	je	.L524
	movq	%rcx, -1216(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -1200(%rbp)
.L197:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -1208(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	-912(%rbp), %rdi
	cmpq	-1632(%rbp), %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	leaq	-1168(%rbp), %rax
	movq	$0, -1176(%rbp)
	movq	%rax, -1184(%rbp)
	movq	%rax, -1664(%rbp)
	leaq	-1136(%rbp), %rax
	movq	%rax, -1152(%rbp)
	movq	%rax, -1704(%rbp)
	leaq	-1104(%rbp), %rax
	movb	$0, -1168(%rbp)
	movq	$0, -1144(%rbp)
	movb	$0, -1136(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -1112(%rbp)
	movb	$0, -1104(%rbp)
	cmpb	$0, 32(%r15)
	movq	%rax, -1712(%rbp)
	je	.L200
	movq	40(%r15), %rax
	leaq	-1072(%rbp), %rbx
	movq	80(%rax), %rax
	movq	%rbx, -1088(%rbp)
	movq	$0, -1080(%rbp)
	movq	%rax, -1672(%rbp)
	movq	-1600(%rbp), %rax
	movb	$0, -1072(%rbp)
	movq	%rax, -1024(%rbp)
	movq	$0, -1016(%rbp)
	movb	$0, -1008(%rbp)
.LEHB29:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE29:
	movl	$27987, %ecx
	movq	%r13, -448(%rbp)
	movq	-1592(%rbp), %rdi
	movw	%cx, 0(%r13)
	movb	$105, 2(%r13)
	movq	$3, -440(%rbp)
	movb	$0, -429(%rbp)
.LEHB30:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE30:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L201
	movq	%rax, -1744(%rbp)
	call	_ZdlPv@PLT
	movq	-1744(%rbp), %rax
.L201:
	cmpq	%rax, -1672(%rbp)
	je	.L525
	movq	-1672(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, -1744(%rbp)
.LEHB31:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE31:
	movl	$25701, %edx
	movq	%r13, -448(%rbp)
	movq	-1592(%rbp), %rdi
	movl	$1734828372, 0(%r13)
	movw	%dx, 4(%r13)
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
.LEHB32:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE32:
	movq	-448(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%r13, %rdi
	je	.L208
	movq	%rax, -1792(%rbp)
	call	_ZdlPv@PLT
	movq	-1792(%rbp), %rsi
.L208:
	movq	-1672(%rbp), %rdi
	movq	-1744(%rbp), %rax
.LEHB33:
	call	*%rax
	testb	%al, %al
	jne	.L526
	movq	-1672(%rbp), %rdi
	movq	(%rdi), %rax
	call	*56(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L527
	leaq	-1056(%rbp), %rdi
	call	_ZNK2v88internal6torque4Type20GetGeneratedTypeNameB5cxx11Ev@PLT
	movq	-1056(%rbp), %rax
	leaq	-1040(%rbp), %r8
	movq	-1088(%rbp), %rdi
	movq	-1048(%rbp), %rdx
	cmpq	%r8, %rax
	je	.L528
	movq	-1040(%rbp), %rcx
	cmpq	%rbx, %rdi
	je	.L529
	movq	%rdx, %xmm0
	movq	%rcx, %xmm4
	movq	-1072(%rbp), %rsi
	movq	%rax, -1088(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -1080(%rbp)
	testq	%rdi, %rdi
	je	.L222
	movq	%rdi, -1056(%rbp)
	movq	%rsi, -1040(%rbp)
.L220:
	movq	$0, -1048(%rbp)
	movb	$0, (%rdi)
	movq	-1056(%rbp), %rdi
	cmpq	%r8, %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	movq	-1016(%rbp), %rdx
	movq	-1736(%rbp), %rdi
	movl	$25, %r8d
	xorl	%esi, %esi
	leaq	.LC44(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L207:
	movq	-1720(%rbp), %rdi
	movl	$8, %edx
	leaq	.LC46(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1080(%rbp), %rdx
	movq	-1088(%rbp), %rsi
	movq	-1720(%rbp), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$27, %edx
	leaq	.LC47(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -1672(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r15), %rcx
	movq	-1592(%rbp), %rdi
	leaq	48(%rcx), %rsi
	call	_ZN2v88internal6torque14CamelifyStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE33:
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	-1672(%rbp), %rdi
.LEHB34:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$17, %edx
	leaq	.LC48(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE34:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	-1632(%rbp), %rax
	movq	%r14, %rdi
	movq	$0, -904(%rbp)
	movb	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	-1016(%rbp), %rax
	leaq	2(%rax), %rsi
.LEHB35:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-904(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L530
	movl	$2, %edx
	leaq	.LC49(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1016(%rbp), %rdx
	movq	-1024(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE35:
	movabsq	$4611686018427387903, %rax
	subq	-904(%rbp), %rax
	cmpq	$43, %rax
	jbe	.L531
	movl	$44, %edx
	leaq	.LC50(%rip), %rsi
	movq	%r14, %rdi
.LEHB36:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE36:
	movq	%r13, -448(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L532
	movq	%rcx, -448(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -432(%rbp)
.L248:
	movq	8(%rax), %rcx
	movq	%rcx, -440(%rbp)
	movq	%rdx, (%rax)
	movq	-1184(%rbp), %rdi
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-448(%rbp), %rdx
	cmpq	%r13, %rdx
	je	.L533
	movq	-440(%rbp), %rax
	movq	-432(%rbp), %rcx
	cmpq	-1664(%rbp), %rdi
	je	.L534
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	-1168(%rbp), %rsi
	movq	%rdx, -1184(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -1176(%rbp)
	testq	%rdi, %rdi
	je	.L254
	movq	%rdi, -448(%rbp)
	movq	%rsi, -432(%rbp)
.L252:
	movq	$0, -440(%rbp)
	movb	$0, (%rdi)
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	-912(%rbp), %rdi
	cmpq	-1632(%rbp), %rdi
	je	.L256
	call	_ZdlPv@PLT
.L256:
	movq	-1144(%rbp), %rdx
	leaq	-1152(%rbp), %rdi
	movl	$15, %r8d
	xorl	%esi, %esi
	leaq	.LC51(%rip), %rcx
.LEHB37:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1112(%rbp), %rdx
	leaq	-1120(%rbp), %rdi
	movl	$25, %r8d
	xorl	%esi, %esi
	leaq	.LC52(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE37:
	movq	-1024(%rbp), %rdi
	cmpq	-1600(%rbp), %rdi
	je	.L257
	call	_ZdlPv@PLT
.L257:
	movq	-1088(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	-1720(%rbp), %rbx
	movl	$58, %edx
	leaq	.LC53(%rip), %rsi
	movq	%rbx, %rdi
.LEHB38:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	56(%r15), %rdx
	movq	48(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$4, %edx
	leaq	.LC54(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1368(%rbp), %rdx
	movq	-1376(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$4, %edx
	leaq	.LC54(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1400(%rbp), %rdx
	movq	-1408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC55(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1208(%rbp), %rdx
	movq	-1216(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC56(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1176(%rbp), %rdx
	movq	-1184(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$4, %edx
	leaq	.LC57(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1680(%rbp), %rbx
	movl	$12, %edx
	leaq	.LC58(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1208(%rbp), %rdx
	movq	-1216(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$10, %edx
	leaq	.LC59(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$8, %edx
	leaq	.LC46(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1464(%rbp), %rdx
	movq	-1472(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC60(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1240(%rbp), %rdx
	movq	-1248(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$28, %edx
	leaq	.LC61(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1144(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$9, %edx
	leaq	.LC62(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$13, %edx
	leaq	.LC63(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC64(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1208(%rbp), %rdx
	movq	-1216(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$11, %edx
	leaq	.LC65(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$40, %edx
	leaq	.LC66(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	88(%r15), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	%rax, %rdi
	movl	$2, %edx
	leaq	.LC67(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	leaq	.LC68(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1464(%rbp), %rdx
	movq	-1472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$4, %edx
	leaq	.LC69(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC64(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1240(%rbp), %rdx
	movq	-1248(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$27, %edx
	leaq	.LC70(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1144(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$10, %edx
	leaq	.LC71(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC72(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1432(%rbp), %rdx
	movq	-1440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$10, %edx
	leaq	.LC73(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$44, %edx
	leaq	.LC74(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1208(%rbp), %rdx
	movq	-1216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC56(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1112(%rbp), %rdx
	movq	-1120(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$54, %edx
	leaq	.LC75(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$20, %edx
	leaq	.LC76(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movzbl	-1608(%rbp), %eax
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	cmpb	$1, %al
	sbbq	%rdx, %rdx
	andq	$-22, %rdx
	addq	$27, %rdx
	testb	%al, %al
	leaq	.LC6(%rip), %rax
	cmovne	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$3, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE38:
.L502:
	movq	-1120(%rbp), %rdi
	cmpq	-1712(%rbp), %rdi
	je	.L267
	call	_ZdlPv@PLT
.L267:
	movq	-1152(%rbp), %rdi
	cmpq	-1704(%rbp), %rdi
	je	.L268
	call	_ZdlPv@PLT
.L268:
	movq	-1184(%rbp), %rdi
	cmpq	-1664(%rbp), %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	movq	-1216(%rbp), %rdi
	cmpq	-1696(%rbp), %rdi
	je	.L270
	call	_ZdlPv@PLT
.L270:
	movq	-1248(%rbp), %rdi
	cmpq	-1688(%rbp), %rdi
	je	.L271
.L492:
	call	_ZdlPv@PLT
.L271:
	movq	-1376(%rbp), %rdi
	cmpq	-1648(%rbp), %rdi
	je	.L272
	call	_ZdlPv@PLT
.L272:
	movq	-1408(%rbp), %rdi
	cmpq	-1624(%rbp), %rdi
	je	.L273
	call	_ZdlPv@PLT
.L273:
	movq	-1440(%rbp), %rdi
	cmpq	-1656(%rbp), %rdi
	je	.L274
	call	_ZdlPv@PLT
.L274:
	movq	-1472(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L275
	call	_ZdlPv@PLT
.L275:
	movq	-1504(%rbp), %rdi
	cmpq	-1616(%rbp), %rdi
	je	.L108
	call	_ZdlPv@PLT
.L108:
	addq	$104, %r15
	cmpq	%r15, -1728(%rbp)
	je	.L103
.L102:
	movq	80(%r15), %r14
.LEHB39:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE39:
	leaq	-448(%rbp), %rax
	leaq	-432(%rbp), %r13
	movl	$1684631414, -432(%rbp)
	movq	%rax, %rdi
	movq	%rax, -1592(%rbp)
	movq	%r13, -448(%rbp)
	movq	$4, -440(%rbp)
	movb	$0, -428(%rbp)
.LEHB40:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE40:
	movq	-448(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%r13, %rdi
	je	.L106
	call	_ZdlPv@PLT
.L106:
	cmpq	%rbx, %r14
	je	.L108
	leaq	48(%r15), %rax
	movq	%rax, -1600(%rbp)
	movq	(%r14), %rax
	movq	16(%rax), %rbx
.LEHB41:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE41:
	movl	$25701, %edi
	movq	%r13, -448(%rbp)
	movw	%di, 4(%r13)
	movq	-1592(%rbp), %rdi
	movl	$1734828372, 0(%r13)
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
.LEHB42:
	call	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE42:
	movq	-448(%rbp), %rdi
	movq	%rax, %rsi
	cmpq	%r13, %rdi
	je	.L112
	movq	%rax, -1600(%rbp)
	call	_ZdlPv@PLT
	movq	-1600(%rbp), %rsi
.L112:
	movq	%r14, %rdi
.LEHB43:
	call	*%rbx
	movq	%r14, %rdi
	movb	%al, -1608(%rbp)
	call	_ZNK2v88internal6torque4Type14ClassSupertypeEv@PLT
.LEHE43:
	movl	%eax, %ebx
	movq	%r15, %rsi
	leaq	-1488(%rbp), %rax
	movq	%rdx, -1664(%rbp)
	movq	%rax, -1616(%rbp)
	movq	%rax, -1504(%rbp)
	leaq	-1024(%rbp), %rax
	movq	%rax, %rdi
	movb	$0, -1488(%rbp)
	movq	$0, -1496(%rbp)
	movq	%rax, -1736(%rbp)
.LEHB44:
	call	_ZNK2v88internal6torque5Field23GetFieldSizeInformationB5cxx11Ev@PLT
.LEHE44:
	movq	-1024(%rbp), %rax
	leaq	-1008(%rbp), %rcx
	movq	-1504(%rbp), %rdi
	movq	%rcx, -1600(%rbp)
	movq	-1016(%rbp), %rdx
	cmpq	%rcx, %rax
	je	.L535
	movq	-1008(%rbp), %rcx
	cmpq	-1616(%rbp), %rdi
	je	.L536
	movq	%rdx, %xmm0
	movq	%rcx, %xmm2
	movq	-1488(%rbp), %rsi
	movq	%rax, -1504(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -1496(%rbp)
	testq	%rdi, %rdi
	je	.L121
	movq	%rdi, -1024(%rbp)
	movq	%rsi, -1008(%rbp)
.L119:
	movq	$0, -1016(%rbp)
	movb	$0, (%rdi)
	movq	-1024(%rbp), %rdi
	cmpq	-1600(%rbp), %rdi
	je	.L122
	call	_ZdlPv@PLT
.L122:
	leaq	-1472(%rbp), %rax
	cmpb	$0, -1608(%rbp)
	movq	$0, -1464(%rbp)
	movq	%rax, -1632(%rbp)
	leaq	-1456(%rbp), %rax
	movq	%rax, -1640(%rbp)
	movq	%rax, -1472(%rbp)
	leaq	-1424(%rbp), %rax
	movq	%rax, -1656(%rbp)
	movq	%rax, -1440(%rbp)
	leaq	-1392(%rbp), %rax
	movq	%rax, -1624(%rbp)
	movq	%rax, -1408(%rbp)
	leaq	-1360(%rbp), %rax
	movb	$0, -1456(%rbp)
	movq	$0, -1432(%rbp)
	movb	$0, -1424(%rbp)
	movq	$0, -1400(%rbp)
	movb	$0, -1392(%rbp)
	movq	%rax, -1648(%rbp)
	movq	%rax, -1376(%rbp)
	movq	$0, -1368(%rbp)
	movb	$0, -1360(%rbp)
	je	.L123
	movq	-1632(%rbp), %rdi
	movl	$9, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC35(%rip), %rcx
.LEHB45:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1432(%rbp), %rdx
	leaq	-1440(%rbp), %rdi
	movl	$11, %r8d
	xorl	%esi, %esi
	leaq	.LC36(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE45:
	movq	%r13, -448(%rbp)
	testb	%bl, %bl
	je	.L124
	movq	-1664(%rbp), %rax
	movq	112(%rax), %r14
	movq	120(%rax), %rbx
	movq	%r14, %rax
	addq	%rbx, %rax
	je	.L125
	testq	%r14, %r14
	je	.L537
.L125:
	movq	%rbx, -1576(%rbp)
	cmpq	$15, %rbx
	ja	.L538
	cmpq	$1, %rbx
	jne	.L128
	movzbl	(%r14), %eax
	movb	%al, -432(%rbp)
	movq	%r13, %rax
.L129:
	movq	%rbx, -440(%rbp)
	movb	$0, (%rax,%rbx)
	movq	-440(%rbp), %rax
	leaq	14(%rax), %rsi
.L130:
	leaq	-1344(%rbp), %r14
	leaq	-1328(%rbp), %rbx
	movq	$0, -1336(%rbp)
	movq	%r14, %rdi
	movq	%rbx, -1344(%rbp)
	movb	$0, -1328(%rbp)
.LEHB46:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-1336(%rbp), %rax
	cmpq	$13, %rax
	jbe	.L539
	movl	$14, %edx
	leaq	.LC38(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE46:
	movq	-1344(%rbp), %rax
	movq	-1408(%rbp), %rdi
	movq	-1336(%rbp), %rdx
	cmpq	%rbx, %rax
	je	.L540
	movq	-1328(%rbp), %rcx
	cmpq	-1624(%rbp), %rdi
	je	.L541
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	-1392(%rbp), %rsi
	movq	%rax, -1408(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -1400(%rbp)
	testq	%rdi, %rdi
	je	.L142
	movq	%rdi, -1344(%rbp)
	movq	%rsi, -1328(%rbp)
.L140:
	movq	$0, -1336(%rbp)
	movb	$0, (%rdi)
	movq	-1344(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L143
	call	_ZdlPv@PLT
.L143:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	movq	-1408(%rbp), %rax
	movq	-1400(%rbp), %r14
	leaq	-1296(%rbp), %rbx
	movq	%rbx, -1312(%rbp)
	movq	%rax, %rcx
	movq	%rax, -1632(%rbp)
	addq	%r14, %rcx
	je	.L145
	testq	%rax, %rax
	je	.L542
.L145:
	movq	%r14, -1576(%rbp)
	cmpq	$15, %r14
	ja	.L543
	cmpq	$1, %r14
	jne	.L148
	movq	-1632(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1296(%rbp)
	movq	%rbx, %rax
.L149:
	movq	%r14, -1304(%rbp)
	movb	$0, (%rax,%r14)
	movq	-1312(%rbp), %rdx
	movq	-1376(%rbp), %rdi
	cmpq	%rbx, %rdx
	je	.L544
	movq	-1296(%rbp), %rcx
	movq	-1304(%rbp), %rax
	cmpq	-1648(%rbp), %rdi
	je	.L545
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	-1360(%rbp), %rsi
	movq	%rdx, -1376(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -1368(%rbp)
	testq	%rdi, %rdi
	je	.L155
	movq	%rdi, -1312(%rbp)
	movq	%rsi, -1296(%rbp)
.L153:
	movq	$0, -1304(%rbp)
	movb	$0, (%rdi)
	movq	-1312(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L157
	call	_ZdlPv@PLT
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L329:
	xorl	%r9d, %r9d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L87:
	testq	%r15, %r15
	jne	.L546
	movq	-1760(%rbp), %rax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L535:
	testq	%rdx, %rdx
	je	.L117
	cmpq	$1, %rdx
	je	.L547
	movq	%rcx, %rsi
	call	memcpy@PLT
	movq	-1016(%rbp), %rdx
	movq	-1504(%rbp), %rdi
.L117:
	movq	%rdx, -1496(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-1024(%rbp), %rdi
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L519:
	movdqu	16(%rax), %xmm5
	movaps	%xmm5, -432(%rbp)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L524:
	movdqu	16(%rax), %xmm5
	movaps	%xmm5, -1200(%rbp)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L522:
	movdqu	16(%rax), %xmm7
	movaps	%xmm7, -432(%rbp)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L521:
	movdqu	16(%rax), %xmm6
	movaps	%xmm6, -1232(%rbp)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L536:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm6
	movq	%rax, -1504(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -1496(%rbp)
.L121:
	movq	-1600(%rbp), %rax
	movq	%rax, -1024(%rbp)
	leaq	-1008(%rbp), %rax
	movq	%rax, -1600(%rbp)
	movq	%rax, %rdi
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L172:
	testq	%r14, %r14
	jne	.L548
	movq	%rbx, %rax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L515:
	leaq	-1576(%rbp), %rsi
	leaq	-1088(%rbp), %rdi
	xorl	%edx, %edx
.LEHB47:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE47:
	movq	%rax, -1088(%rbp)
	movq	%rax, %rdi
	movq	-1576(%rbp), %rax
	movq	%rax, -1072(%rbp)
.L171:
	movq	-1664(%rbp), %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	-1576(%rbp), %r14
	movq	-1088(%rbp), %rax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L103:
	movq	-1680(%rbp), %rdi
	movl	$3, %edx
	leaq	.LC34(%rip), %rsi
.LEHB48:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$48, %edx
	leaq	.LC77(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$52, %edx
	leaq	.LC78(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$58, %edx
	leaq	.LC79(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1528(%rbp), %rdx
	movq	-1536(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$27, %edx
	leaq	.LC80(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE48:
	movq	-784(%rbp), %rax
	leaq	-432(%rbp), %r13
	movq	$0, -440(%rbp)
	leaq	-448(%rbp), %rdi
	movq	%r13, -448(%rbp)
	movb	$0, -432(%rbp)
	testq	%rax, %rax
	je	.L281
	movq	-800(%rbp), %r8
	movq	-792(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L282
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB49:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE49:
.L283:
	movq	-440(%rbp), %rdx
	movq	-448(%rbp), %rsi
	movq	%r12, %rdi
.LEHB50:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE50:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	movl	$17, %edx
	leaq	.LC81(%rip), %rsi
	movq	%r12, %rdi
.LEHB51:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$2, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE51:
	movq	.LC2(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-752(%rbp), %rdi
	movq	%rax, -848(%rbp)
	addq	$80, %rax
	movhps	.LC4(%rip), %xmm0
	movq	%rax, -720(%rbp)
	movaps	%xmm0, -832(%rbp)
	cmpq	-1784(%rbp), %rdi
	je	.L288
	call	_ZdlPv@PLT
.L288:
	movq	-1776(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -824(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	-1752(%rbp), %rdi
	movq	%rax, -848(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -848(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -832(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -832(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -848(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -848(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -840(%rbp)
	movq	%rax, -720(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1536(%rbp), %rdi
	cmpq	-1768(%rbp), %rdi
	je	.L289
	call	_ZdlPv@PLT
.L289:
	movq	-1568(%rbp), %rdi
	cmpq	-1760(%rbp), %rdi
	je	.L77
	call	_ZdlPv@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L517:
	testq	%rdx, %rdx
	je	.L180
	cmpq	$1, %rdx
	je	.L549
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-1080(%rbp), %rdx
	movq	-1472(%rbp), %rdi
.L180:
	movq	%rdx, -1464(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-1088(%rbp), %rdi
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L512:
	testq	%rdx, %rdx
	je	.L163
	cmpq	$1, %rdx
	je	.L550
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-1272(%rbp), %rdx
	movq	-1408(%rbp), %rdi
.L163:
	movq	%rdx, -1400(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-1280(%rbp), %rdi
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$29795, %esi
	movl	$1701470799, 0(%r13)
	movw	%si, 4(%r13)
	movl	$20, %esi
	movq	$6, -440(%rbp)
	movb	$0, -426(%rbp)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L525:
	movq	-1080(%rbp), %rdx
	leaq	-1088(%rbp), %rdi
	movl	$9, %r8d
	xorl	%esi, %esi
	leaq	.LC35(%rip), %rcx
.LEHB52:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1016(%rbp), %rdx
	movq	-1736(%rbp), %rdi
	movl	$58, %r8d
	xorl	%esi, %esi
	leaq	.LC43(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE52:
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L511:
	movq	-1592(%rbp), %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdi
.LEHB53:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE53:
	leaq	-976(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
.LEHB54:
	call	_ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE54:
	movq	%rbx, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-976(%rbp), %rdi
	leaq	-960(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L159
	call	_ZdlPv@PLT
.L159:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	jne	.L492
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L513:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm7
	movq	%rax, -1408(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -1400(%rbp)
.L167:
	movq	%rbx, -1280(%rbp)
	leaq	-1264(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L518:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, -1472(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -1464(%rbp)
.L184:
	movq	%rbx, -1088(%rbp)
	leaq	-1072(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L148:
	testq	%r14, %r14
	jne	.L551
	movq	%rbx, %rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L543:
	leaq	-1576(%rbp), %rsi
	leaq	-1312(%rbp), %rdi
	xorl	%edx, %edx
.LEHB55:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE55:
	movq	%rax, -1312(%rbp)
	movq	%rax, %rdi
	movq	-1576(%rbp), %rax
	movq	%rax, -1296(%rbp)
.L147:
	movq	-1632(%rbp), %rsi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	-1576(%rbp), %r14
	movq	-1312(%rbp), %rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L533:
	movq	-440(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L250
	cmpq	$1, %rdx
	je	.L552
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-440(%rbp), %rdx
	movq	-1184(%rbp), %rdi
.L250:
	movq	%rdx, -1176(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-448(%rbp), %rdi
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L532:
	movdqu	16(%rax), %xmm6
	movaps	%xmm6, -432(%rbp)
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L544:
	movq	-1304(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L151
	cmpq	$1, %rdx
	je	.L553
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-1304(%rbp), %rdx
	movq	-1376(%rbp), %rdi
.L151:
	movq	%rdx, -1368(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-1312(%rbp), %rdi
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L540:
	testq	%rdx, %rdx
	je	.L138
	cmpq	$1, %rdx
	je	.L554
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-1336(%rbp), %rdx
	movq	-1408(%rbp), %rdi
.L138:
	movq	%rdx, -1400(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-1344(%rbp), %rdi
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L282:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB56:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE56:
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L534:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, -1184(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -1176(%rbp)
.L254:
	movq	%r13, -448(%rbp)
	leaq	-432(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L545:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, -1376(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -1368(%rbp)
.L155:
	movq	%rbx, -1312(%rbp)
	leaq	-1296(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm5
	movq	%rax, -1408(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -1400(%rbp)
.L142:
	movq	%rbx, -1344(%rbp)
	leaq	-1328(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L528:
	testq	%rdx, %rdx
	je	.L218
	cmpq	$1, %rdx
	je	.L555
	movq	%r8, %rsi
	movq	%r8, -1672(%rbp)
	call	memcpy@PLT
	movq	-1048(%rbp), %rdx
	movq	-1088(%rbp), %rdi
	movq	-1672(%rbp), %r8
.L218:
	movq	%rdx, -1080(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-1056(%rbp), %rdi
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L526:
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1608(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	xorl	%eax, %eax
	xorl	%esi, %esi
	movq	%rcx, -320(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp)
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rcx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-1592(%rbp), %rcx
	addq	-24(%rax), %rcx
	movq	%rcx, %rdi
.LEHB57:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE57:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rcx, -432(%rbp)
	movq	-24(%rcx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r13, %rcx
	movq	%rcx, %rdi
.LEHB58:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE58:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-1808(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	%rcx, -448(%rbp)
	movq	-24(%rcx), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp)
	addq	$80, %rcx
	movq	%rcx, -320(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1736(%rbp)
	movaps	%xmm7, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1608(%rbp), %rdi
	movl	$24, -360(%rbp)
	movq	%rcx, -424(%rbp)
	leaq	-336(%rbp), %rcx
	movq	%rcx, -1744(%rbp)
	movq	%rcx, -352(%rbp)
	leaq	-424(%rbp), %rcx
	movq	%rcx, %rsi
	movb	$0, -336(%rbp)
	movq	$0, -344(%rbp)
	movq	%rcx, -1792(%rbp)
.LEHB59:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE59:
	movl	$24, %edx
	leaq	.LC45(%rip), %rsi
	movq	%r13, %rdi
.LEHB60:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1672(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
.LEHE60:
	movq	-384(%rbp), %rax
	leaq	-976(%rbp), %rdi
	leaq	-960(%rbp), %r13
	movq	$0, -968(%rbp)
	movq	%rdi, -1672(%rbp)
	movq	%r13, -976(%rbp)
	movb	$0, -960(%rbp)
	testq	%rax, %rax
	je	.L228
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L556
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB61:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE61:
.L230:
	movq	-1672(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
.LEHB62:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE62:
	movq	-976(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movq	.LC2(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC4(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-1744(%rbp), %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	-1736(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1608(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
.L478:
	movq	-912(%rbp), %rdi
	cmpq	-1632(%rbp), %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	movq	-1024(%rbp), %rdi
	cmpq	-1600(%rbp), %rdi
	je	.L259
	call	_ZdlPv@PLT
.L259:
	movq	-1088(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L502
	call	_ZdlPv@PLT
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L507:
	leaq	-1568(%rbp), %rdi
	leaq	-1576(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -1592(%rbp)
.LEHB63:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE63:
	movq	-1592(%rbp), %r9
	movq	%rax, -1568(%rbp)
	movq	%rax, %rdi
	movq	-1576(%rbp), %rax
	movq	%rax, -1552(%rbp)
.L86:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r9, -1592(%rbp)
	call	memcpy@PLT
	movq	-1576(%rbp), %r15
	movq	-1568(%rbp), %rax
	movq	-1592(%rbp), %r9
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L128:
	testq	%rbx, %rbx
	jne	.L557
	movq	%r13, %rax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L538:
	movq	-1592(%rbp), %rdi
	leaq	-1576(%rbp), %rsi
	xorl	%edx, %edx
.LEHB64:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE64:
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-1576(%rbp), %rax
	movq	%rax, -432(%rbp)
.L127:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-1576(%rbp), %rbx
	movq	-448(%rbp), %rax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L508:
	movl	$29795, %r9d
	movb	$0, -1514(%rbp)
	movl	$1701470799, -1520(%rbp)
	movw	%r9w, -1516(%rbp)
	movq	$6, -1528(%rbp)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L547:
	movzbl	-1008(%rbp), %eax
	movb	%al, (%rdi)
	movq	-1016(%rbp), %rdx
	movq	-1504(%rbp), %rdi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-1672(%rbp), %rsi
	movq	%r14, %rdi
.LEHB65:
	call	_ZNK2v88internal6torque4Type8ToStringB5cxx11Ev@PLT
.LEHE65:
	movq	%r14, %rsi
	movq	-1592(%rbp), %r14
	movq	%r14, %rdi
.LEHB66:
	call	_ZN2v88internal6torqueL7MessageIJRA7_KcNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA38_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE66:
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L478
	call	_ZdlPv@PLT
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, -1088(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -1080(%rbp)
.L222:
	movq	%r8, -1056(%rbp)
	leaq	-1040(%rbp), %r8
	movq	%r8, %rdi
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L94:
	testq	%r15, %r15
	jne	.L558
	movq	-1768(%rbp), %rax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L510:
	leaq	-1576(%rbp), %rsi
	leaq	-1536(%rbp), %rdi
	xorl	%edx, %edx
.LEHB67:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE67:
	movq	%rax, -1536(%rbp)
	movq	%rax, %rdi
	movq	-1576(%rbp), %rax
	movq	%rax, -1520(%rbp)
.L93:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-1576(%rbp), %r15
	movq	-1536(%rbp), %rax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L281:
	leaq	-752(%rbp), %rsi
.LEHB68:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE68:
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L550:
	movzbl	-1264(%rbp), %eax
	movb	%al, (%rdi)
	movq	-1272(%rbp), %rdx
	movq	-1408(%rbp), %rdi
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L549:
	movzbl	-1072(%rbp), %eax
	movb	%al, (%rdi)
	movq	-1080(%rbp), %rdx
	movq	-1472(%rbp), %rdi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L556:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB69:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L230
.L555:
	movzbl	-1040(%rbp), %eax
	movb	%al, (%rdi)
	movq	-1048(%rbp), %rdx
	movq	-1088(%rbp), %rdi
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L552:
	movzbl	-432(%rbp), %eax
	movb	%al, (%rdi)
	movq	-440(%rbp), %rdx
	movq	-1184(%rbp), %rdi
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L554:
	movzbl	-1328(%rbp), %eax
	movb	%al, (%rdi)
	movq	-1336(%rbp), %rdx
	movq	-1408(%rbp), %rdi
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L553:
	movzbl	-1296(%rbp), %eax
	movb	%al, (%rdi)
	movq	-1304(%rbp), %rdx
	movq	-1376(%rbp), %rdi
	jmp	.L151
.L228:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE69:
	jmp	.L230
.L523:
	leaq	.LC37(%rip), %rdi
.LEHB70:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE70:
.L520:
	leaq	.LC37(%rip), %rdi
.LEHB71:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE71:
.L539:
	leaq	.LC37(%rip), %rdi
.LEHB72:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE72:
.L531:
	leaq	.LC37(%rip), %rdi
.LEHB73:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE73:
.L530:
	leaq	.LC37(%rip), %rdi
.LEHB74:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE74:
.L516:
	leaq	.LC37(%rip), %rdi
.LEHB75:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE75:
.L504:
	call	__stack_chk_fail@PLT
.L542:
	leaq	.LC8(%rip), %rdi
.LEHB76:
	call	_ZSt19__throw_logic_errorPKc@PLT
.L537:
	leaq	.LC8(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE76:
.L558:
	movq	-1768(%rbp), %rdi
	jmp	.L93
.L551:
	movq	%rbx, %rdi
	jmp	.L147
.L548:
	movq	%rbx, %rdi
	jmp	.L171
.L546:
	movq	-1760(%rbp), %rdi
	jmp	.L86
.L509:
	leaq	.LC8(%rip), %rdi
.LEHB77:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE77:
.L506:
	leaq	.LC8(%rip), %rdi
.LEHB78:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE78:
.L557:
	movq	%r13, %rdi
	jmp	.L127
.L514:
	leaq	.LC8(%rip), %rdi
.LEHB79:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE79:
.L363:
	endbr64
	movq	%rax, %r12
	jmp	.L239
.L367:
	endbr64
	movq	%rax, %r12
	jmp	.L232
.L347:
	endbr64
	movq	%rax, %r12
	jmp	.L306
.L339:
	endbr64
	movq	%rax, %rbx
	jmp	.L179
.L364:
	endbr64
	movq	%rax, %r12
	jmp	.L226
.L340:
	endbr64
	movq	%rax, %rbx
	jmp	.L291
.L341:
	endbr64
	movq	%rax, %rbx
	jmp	.L295
.L358:
	endbr64
	movq	%rax, %r12
	jmp	.L133
.L352:
	endbr64
	movq	%rax, %rbx
	jmp	.L322
.L369:
	endbr64
	movq	%rax, %rbx
	jmp	.L285
.L359:
	endbr64
	movq	%rax, %r12
	jmp	.L177
.L336:
	endbr64
	movq	%rax, %rbx
	jmp	.L325
.L338:
	endbr64
	movq	%rax, %rbx
	jmp	.L320
.L357:
	endbr64
	movq	%rax, %rbx
	jmp	.L114
.L356:
	endbr64
	movq	%rax, %rbx
	jmp	.L109
.L351:
	endbr64
	movq	%rax, %rbx
	jmp	.L111
.L354:
	endbr64
	movq	%rax, %r12
	jmp	.L99
.L355:
	endbr64
	movq	%rax, %r12
	jmp	.L97
.L353:
	endbr64
	movq	%rax, %r12
	jmp	.L98
.L337:
	endbr64
	movq	%rax, %rbx
	jmp	.L100
.L362:
	endbr64
	movq	%rax, %r12
	jmp	.L234
.L344:
	endbr64
	movq	%rax, %rbx
	jmp	.L299
.L346:
	endbr64
	movq	%rax, %r12
	jmp	.L206
.L345:
	endbr64
	movq	%rax, %rbx
	jmp	.L297
.L368:
	endbr64
	movq	%rax, %r12
	jmp	.L306
.L348:
	endbr64
	movq	%rax, %r12
	jmp	.L304
.L361:
	endbr64
	movq	%rax, %r12
	jmp	.L304
.L360:
	endbr64
	movq	%rax, %r12
	jmp	.L304
.L365:
	endbr64
	movq	%rax, %r12
	jmp	.L227
.L366:
	endbr64
	movq	%rax, %rdx
	jmp	.L225
.L343:
	endbr64
	movq	%rax, %rbx
	jmp	.L301
.L342:
	endbr64
	movq	%rax, %rbx
	jmp	.L293
.L350:
	endbr64
	movq	%rax, %rbx
	jmp	.L310
.L349:
	endbr64
	movq	%rax, %r12
	jmp	.L306
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE,"a",@progbits
.LLSDA7031:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7031-.LLSDACSB7031
.LLSDACSB7031:
	.uleb128 .LEHB15-.LFB7031
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB16-.LFB7031
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L337-.LFB7031
	.uleb128 0
	.uleb128 .LEHB17-.LFB7031
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L353-.LFB7031
	.uleb128 0
	.uleb128 .LEHB18-.LFB7031
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L355-.LFB7031
	.uleb128 0
	.uleb128 .LEHB19-.LFB7031
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L354-.LFB7031
	.uleb128 0
	.uleb128 .LEHB20-.LFB7031
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L351-.LFB7031
	.uleb128 0
	.uleb128 .LEHB21-.LFB7031
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L339-.LFB7031
	.uleb128 0
	.uleb128 .LEHB22-.LFB7031
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L359-.LFB7031
	.uleb128 0
	.uleb128 .LEHB23-.LFB7031
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L339-.LFB7031
	.uleb128 0
	.uleb128 .LEHB24-.LFB7031
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L341-.LFB7031
	.uleb128 0
	.uleb128 .LEHB25-.LFB7031
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L342-.LFB7031
	.uleb128 0
	.uleb128 .LEHB26-.LFB7031
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L343-.LFB7031
	.uleb128 0
	.uleb128 .LEHB27-.LFB7031
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L344-.LFB7031
	.uleb128 0
	.uleb128 .LEHB28-.LFB7031
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L345-.LFB7031
	.uleb128 0
	.uleb128 .LEHB29-.LFB7031
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L346-.LFB7031
	.uleb128 0
	.uleb128 .LEHB30-.LFB7031
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L360-.LFB7031
	.uleb128 0
	.uleb128 .LEHB31-.LFB7031
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L346-.LFB7031
	.uleb128 0
	.uleb128 .LEHB32-.LFB7031
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L361-.LFB7031
	.uleb128 0
	.uleb128 .LEHB33-.LFB7031
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L346-.LFB7031
	.uleb128 0
	.uleb128 .LEHB34-.LFB7031
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L348-.LFB7031
	.uleb128 0
	.uleb128 .LEHB35-.LFB7031
	.uleb128 .LEHE35-.LEHB35
	.uleb128 .L368-.LFB7031
	.uleb128 0
	.uleb128 .LEHB36-.LFB7031
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L349-.LFB7031
	.uleb128 0
	.uleb128 .LEHB37-.LFB7031
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L346-.LFB7031
	.uleb128 0
	.uleb128 .LEHB38-.LFB7031
	.uleb128 .LEHE38-.LEHB38
	.uleb128 .L350-.LFB7031
	.uleb128 0
	.uleb128 .LEHB39-.LFB7031
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L351-.LFB7031
	.uleb128 0
	.uleb128 .LEHB40-.LFB7031
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L356-.LFB7031
	.uleb128 0
	.uleb128 .LEHB41-.LFB7031
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L351-.LFB7031
	.uleb128 0
	.uleb128 .LEHB42-.LFB7031
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L357-.LFB7031
	.uleb128 0
	.uleb128 .LEHB43-.LFB7031
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L351-.LFB7031
	.uleb128 0
	.uleb128 .LEHB44-.LFB7031
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L338-.LFB7031
	.uleb128 0
	.uleb128 .LEHB45-.LFB7031
	.uleb128 .LEHE45-.LEHB45
	.uleb128 .L339-.LFB7031
	.uleb128 0
	.uleb128 .LEHB46-.LFB7031
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L358-.LFB7031
	.uleb128 0
	.uleb128 .LEHB47-.LFB7031
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L339-.LFB7031
	.uleb128 0
	.uleb128 .LEHB48-.LFB7031
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L351-.LFB7031
	.uleb128 0
	.uleb128 .LEHB49-.LFB7031
	.uleb128 .LEHE49-.LEHB49
	.uleb128 .L369-.LFB7031
	.uleb128 0
	.uleb128 .LEHB50-.LFB7031
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L352-.LFB7031
	.uleb128 0
	.uleb128 .LEHB51-.LFB7031
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L351-.LFB7031
	.uleb128 0
	.uleb128 .LEHB52-.LFB7031
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L346-.LFB7031
	.uleb128 0
	.uleb128 .LEHB53-.LFB7031
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L339-.LFB7031
	.uleb128 0
	.uleb128 .LEHB54-.LFB7031
	.uleb128 .LEHE54-.LEHB54
	.uleb128 .L340-.LFB7031
	.uleb128 0
	.uleb128 .LEHB55-.LFB7031
	.uleb128 .LEHE55-.LEHB55
	.uleb128 .L339-.LFB7031
	.uleb128 0
	.uleb128 .LEHB56-.LFB7031
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L369-.LFB7031
	.uleb128 0
	.uleb128 .LEHB57-.LFB7031
	.uleb128 .LEHE57-.LEHB57
	.uleb128 .L364-.LFB7031
	.uleb128 0
	.uleb128 .LEHB58-.LFB7031
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L366-.LFB7031
	.uleb128 0
	.uleb128 .LEHB59-.LFB7031
	.uleb128 .LEHE59-.LEHB59
	.uleb128 .L365-.LFB7031
	.uleb128 0
	.uleb128 .LEHB60-.LFB7031
	.uleb128 .LEHE60-.LEHB60
	.uleb128 .L362-.LFB7031
	.uleb128 0
	.uleb128 .LEHB61-.LFB7031
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L367-.LFB7031
	.uleb128 0
	.uleb128 .LEHB62-.LFB7031
	.uleb128 .LEHE62-.LEHB62
	.uleb128 .L363-.LFB7031
	.uleb128 0
	.uleb128 .LEHB63-.LFB7031
	.uleb128 .LEHE63-.LEHB63
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB64-.LFB7031
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L339-.LFB7031
	.uleb128 0
	.uleb128 .LEHB65-.LFB7031
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L346-.LFB7031
	.uleb128 0
	.uleb128 .LEHB66-.LFB7031
	.uleb128 .LEHE66-.LEHB66
	.uleb128 .L347-.LFB7031
	.uleb128 0
	.uleb128 .LEHB67-.LFB7031
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L336-.LFB7031
	.uleb128 0
	.uleb128 .LEHB68-.LFB7031
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L369-.LFB7031
	.uleb128 0
	.uleb128 .LEHB69-.LFB7031
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L367-.LFB7031
	.uleb128 0
	.uleb128 .LEHB70-.LFB7031
	.uleb128 .LEHE70-.LEHB70
	.uleb128 .L345-.LFB7031
	.uleb128 0
	.uleb128 .LEHB71-.LFB7031
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L342-.LFB7031
	.uleb128 0
	.uleb128 .LEHB72-.LFB7031
	.uleb128 .LEHE72-.LEHB72
	.uleb128 .L358-.LFB7031
	.uleb128 0
	.uleb128 .LEHB73-.LFB7031
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L349-.LFB7031
	.uleb128 0
	.uleb128 .LEHB74-.LFB7031
	.uleb128 .LEHE74-.LEHB74
	.uleb128 .L368-.LFB7031
	.uleb128 0
	.uleb128 .LEHB75-.LFB7031
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L359-.LFB7031
	.uleb128 0
	.uleb128 .LEHB76-.LFB7031
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L339-.LFB7031
	.uleb128 0
	.uleb128 .LEHB77-.LFB7031
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L336-.LFB7031
	.uleb128 0
	.uleb128 .LEHB78-.LFB7031
	.uleb128 .LEHE78-.LEHB78
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB79-.LFB7031
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L339-.LFB7031
	.uleb128 0
.LLSDACSE7031:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC7031
	.type	_ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.cold, @function
_ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.cold:
.LFSB7031:
.L239:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-976(%rbp), %rdi
	cmpq	%r13, %rdi
	jne	.L559
.L234:
	movq	-1592(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L206:
	movq	-1024(%rbp), %rdi
	cmpq	-1600(%rbp), %rdi
	je	.L308
	call	_ZdlPv@PLT
.L308:
	movq	-1088(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L309
	call	_ZdlPv@PLT
.L309:
	movq	%r12, %rbx
.L310:
	movq	-1120(%rbp), %rdi
	cmpq	-1712(%rbp), %rdi
	je	.L311
	call	_ZdlPv@PLT
.L311:
	movq	-1152(%rbp), %rdi
	cmpq	-1704(%rbp), %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	movq	-1184(%rbp), %rdi
	cmpq	-1664(%rbp), %rdi
	je	.L313
	call	_ZdlPv@PLT
.L313:
	movq	-1216(%rbp), %rdi
	cmpq	-1696(%rbp), %rdi
	je	.L301
	call	_ZdlPv@PLT
.L301:
	movq	-1248(%rbp), %rdi
	cmpq	-1688(%rbp), %rdi
	je	.L179
.L476:
	call	_ZdlPv@PLT
.L179:
	movq	-1376(%rbp), %rdi
	cmpq	-1648(%rbp), %rdi
	je	.L316
	call	_ZdlPv@PLT
.L316:
	movq	-1408(%rbp), %rdi
	cmpq	-1624(%rbp), %rdi
	je	.L317
	call	_ZdlPv@PLT
.L317:
	movq	-1440(%rbp), %rdi
	cmpq	-1656(%rbp), %rdi
	je	.L318
	call	_ZdlPv@PLT
.L318:
	movq	-1472(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L320
	call	_ZdlPv@PLT
.L320:
	movq	-1504(%rbp), %rdi
	cmpq	-1616(%rbp), %rdi
	jne	.L560
.L111:
	movq	-1816(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L100:
	movq	-1536(%rbp), %rdi
	cmpq	-1768(%rbp), %rdi
	je	.L325
	call	_ZdlPv@PLT
.L325:
	movq	-1568(%rbp), %rdi
	cmpq	-1760(%rbp), %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	%rbx, %rdi
.LEHB80:
	call	_Unwind_Resume@PLT
.LEHE80:
.L232:
	movq	-976(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L234
	call	_ZdlPv@PLT
	jmp	.L234
.L559:
	call	_ZdlPv@PLT
	jmp	.L234
.L306:
	movq	-912(%rbp), %rdi
	cmpq	-1632(%rbp), %rdi
	je	.L206
.L472:
	call	_ZdlPv@PLT
	jmp	.L206
.L560:
	call	_ZdlPv@PLT
	jmp	.L111
.L225:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rdx, %r12
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L226:
	movq	-1608(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L206
.L291:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	jne	.L476
	jmp	.L179
.L293:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	-912(%rbp), %rdi
	leaq	-896(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L476
	jmp	.L179
.L133:
	movq	-1344(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L136
.L135:
	call	_ZdlPv@PLT
.L136:
	movq	%r12, %rbx
	jmp	.L179
.L322:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L111
	call	_ZdlPv@PLT
	jmp	.L111
.L285:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L111
	call	_ZdlPv@PLT
	jmp	.L111
.L177:
	movq	-1088(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L135
	jmp	.L136
.L114:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L111
	call	_ZdlPv@PLT
	jmp	.L111
.L109:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L111
	call	_ZdlPv@PLT
	jmp	.L111
.L99:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	%r14, -848(%rbp)
	movq	-24(%r14), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -848(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -832(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -832(%rbp,%rax)
	movq	%rbx, -848(%rbp)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -848(%rbp,%rax)
	movq	$0, -840(%rbp)
.L98:
	movq	-1752(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r12, %rbx
	movq	%rax, -720(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L100
.L97:
	movq	%rbx, -848(%rbp)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -848(%rbp,%rax)
	movq	$0, -840(%rbp)
	jmp	.L98
.L297:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L299
	call	_ZdlPv@PLT
.L299:
	movq	-912(%rbp), %rdi
	cmpq	-1632(%rbp), %rdi
	je	.L301
	call	_ZdlPv@PLT
	jmp	.L301
.L304:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	jne	.L472
	jmp	.L206
.L227:
	movq	-1792(%rbp), %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L226
	.cfi_endproc
.LFE7031:
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE
.LLSDAC7031:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC7031-.LLSDACSBC7031
.LLSDACSBC7031:
	.uleb128 .LEHB80-.LCOLDB82
	.uleb128 .LEHE80-.LEHB80
	.uleb128 0
	.uleb128 0
.LLSDACSEC7031:
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE
	.size	_ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE, .-_ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE
	.size	_ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.cold, .-_ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE.cold
.LCOLDE82:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE
.LHOTE82:
	.section	.rodata._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC83:
	.string	"// Provides the ability to read object properties in\n"
	.align 8
.LC84:
	.string	"// postmortem or remote scenarios, where the debuggee's\n"
	.align 8
.LC85:
	.string	"// memory is not part of the current process's address\n"
	.align 8
.LC86:
	.string	"// space and must be read using a callback function.\n\n"
	.section	.rodata._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC87:
	.string	".h"
.LC88:
	.string	"#include <cstdint>\n"
.LC89:
	.string	"#include <vector>\n"
	.section	.rodata._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.8
	.align 8
.LC90:
	.string	"\n#include \"tools/debug_helper/debug-helper-internal.h\"\n\n"
	.section	.rodata._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1
.LC91:
	.string	"#include \"torque-generated/"
.LC92:
	.string	".h\"\n"
	.section	.rodata._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.8
	.align 8
.LC93:
	.string	"#include \"include/v8-internal.h\"\n\n"
	.section	.rodata._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1
.LC94:
	.string	"namespace i = v8::internal;\n\n"
.LC95:
	.string	"\nclass TqObjectVisitor {\n"
	.section	.rodata._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.8
	.align 8
.LC96:
	.string	"  virtual void VisitObject(const TqObject* object) {}\n"
	.section	.rodata._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1
.LC98:
	.string	"/"
.LC99:
	.string	".cc"
	.section	.text.unlikely._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB102:
	.section	.text._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB102:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB7056:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7056
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1120(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-1248(%rbp), %rbx
	subq	$1672, %rsp
	movq	%rdi, -1568(%rbp)
	leaq	-1392(%rbp), %rdi
	movq	%rsi, -1576(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1376(%rbp), %rax
	movq	$22, -1456(%rbp)
	movq	%rax, -1624(%rbp)
	movq	%rax, -1392(%rbp)
	leaq	-1456(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -1528(%rbp)
.LEHB81:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE81:
	movq	-1456(%rbp), %rdx
	movl	$29044, %ecx
	movq	%r15, %rdi
	movdqa	.LC100(%rip), %xmm0
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movups	%xmm0, (%rax)
	movq	-1392(%rbp), %rdx
	movw	%cx, 20(%rax)
	movl	$762540645, 16(%rax)
	movq	-1456(%rbp), %rax
	movq	%rbx, -1712(%rbp)
	movq	%rax, -1384(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r15, -1616(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -1120(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movw	%si, -896(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -1248(%rbp)
	movq	$0, -904(%rbp)
	movups	%xmm0, -888(%rbp)
	movups	%xmm0, -872(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1248(%rbp,%rax)
	movq	-1248(%rbp), %rax
	movq	$0, -1240(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB82:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE82:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-1232(%rbp), %r12
	xorl	%esi, %esi
	movq	%rax, -1232(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1232(%rbp,%rax)
	movq	-1232(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
.LEHB83:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE83:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	leaq	-1224(%rbp), %r13
	movq	.LC2(%rip), %xmm1
	movq	%rax, -1248(%rbp)
	movq	-24(%rax), %rax
	movhps	.LC3(%rip), %xmm1
	movq	%rcx, -1248(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -1248(%rbp)
	addq	$80, %rax
	movq	%rax, -1120(%rbp)
	leaq	-1168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1640(%rbp)
	movaps	%xmm1, -1552(%rbp)
	movaps	%xmm1, -1232(%rbp)
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm0, -1200(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -1224(%rbp)
	leaq	-1136(%rbp), %rax
	movl	$24, -1160(%rbp)
	movq	%rax, -1648(%rbp)
	movq	%rax, -1152(%rbp)
	movq	$0, -1144(%rbp)
	movb	$0, -1136(%rbp)
.LEHB84:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE84:
	leaq	-720(%rbp), %r15
	leaq	-848(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, -1704(%rbp)
	movq	%r15, -1632(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%rax, -720(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movw	%dx, -496(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -848(%rbp)
	movq	$0, -504(%rbp)
	movups	%xmm0, -488(%rbp)
	movups	%xmm0, -472(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -848(%rbp,%rax)
	movq	-848(%rbp), %rax
	xorl	%esi, %esi
	movq	$0, -840(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB85:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE85:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-832(%rbp), %r13
	xorl	%esi, %esi
	movq	%rax, -832(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -832(%rbp,%rax)
	movq	-832(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
.LEHB86:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE86:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	pxor	%xmm0, %xmm0
	leaq	-824(%rbp), %r14
	movdqa	-1552(%rbp), %xmm3
	movq	%rax, -848(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -848(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -848(%rbp)
	addq	$80, %rax
	movq	%rax, -720(%rbp)
	leaq	-768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1672(%rbp)
	movaps	%xmm3, -832(%rbp)
	movaps	%xmm0, -816(%rbp)
	movaps	%xmm0, -800(%rbp)
	movaps	%xmm0, -784(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -824(%rbp)
	leaq	-736(%rbp), %rax
	movl	$24, -760(%rbp)
	movq	%rax, -1680(%rbp)
	movq	%rax, -752(%rbp)
	movq	$0, -744(%rbp)
	movb	$0, -736(%rbp)
.LEHB87:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE87:
	movl	$53, %edx
	leaq	.LC83(%rip), %rsi
	movq	%r12, %rdi
.LEHB88:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$56, %edx
	leaq	.LC84(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$55, %edx
	leaq	.LC85(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$54, %edx
	leaq	.LC86(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE88:
	movq	-1392(%rbp), %rbx
	movq	-1384(%rbp), %r14
	leaq	-432(%rbp), %r15
	movq	%r15, -448(%rbp)
	movq	%rbx, %rax
	addq	%r14, %rax
	je	.L572
	testq	%rbx, %rbx
	je	.L780
.L572:
	movq	%r14, -1456(%rbp)
	cmpq	$15, %r14
	ja	.L781
	cmpq	$1, %r14
	jne	.L575
	movzbl	(%rbx), %eax
	movb	%al, -432(%rbp)
	movq	%r15, %rax
.L576:
	movq	%r14, -440(%rbp)
	movb	$0, (%rax,%r14)
	movabsq	$4611686018427387903, %rax
	subq	-440(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L782
	leaq	-448(%rbp), %rbx
	movl	$2, %edx
	leaq	.LC87(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rbx, -1536(%rbp)
.LEHB89:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE89:
	leaq	-1296(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1608(%rbp)
.LEHB90:
	call	_ZN2v88internal6torque17IncludeGuardScopeC1ERSoNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE90:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L582
	call	_ZdlPv@PLT
.L582:
	movl	$19, %edx
	leaq	.LC88(%rip), %rsi
	movq	%r12, %rdi
.LEHB91:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$18, %edx
	leaq	.LC89(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$56, %edx
	leaq	.LC90(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$27, %edx
	leaq	.LC91(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1384(%rbp), %rdx
	movq	-1392(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	movl	$4, %edx
	leaq	.LC92(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$34, %edx
	leaq	.LC93(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$29, %edx
	leaq	.LC94(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1536(%rbp), %rbx
	movq	-1528(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, -448(%rbp)
	movq	$24, -1456(%rbp)
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE91:
	movq	-1456(%rbp), %rdx
	movdqa	.LC101(%rip), %xmm0
	movq	%r12, %rsi
	movabsq	$7809644666444607081, %rcx
	movq	%rax, -448(%rbp)
	movq	%rdx, -432(%rbp)
	movq	%rcx, 16(%rax)
	movl	$1, %ecx
	movups	%xmm0, (%rax)
	movq	-1456(%rbp), %rax
	movq	-448(%rbp), %rdx
	movq	%rax, -440(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-1520(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, -1656(%rbp)
.LEHB92:
	call	_ZN2v88internal6torque14NamespaceScopeC1ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE@PLT
.LEHE92:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L583
	call	_ZdlPv@PLT
.L583:
	movq	-1536(%rbp), %rbx
	movq	-1528(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, -448(%rbp)
	movq	$24, -1456(%rbp)
	movq	%rbx, %rdi
.LEHB93:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE93:
	movq	-1456(%rbp), %rdx
	movdqa	.LC101(%rip), %xmm0
	movabsq	$7809644666444607081, %rsi
	movq	%rax, -448(%rbp)
	movl	$1, %ecx
	movq	%rdx, -432(%rbp)
	movq	%rsi, 16(%rax)
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	movq	-1456(%rbp), %rax
	movq	-448(%rbp), %rdx
	movq	%rax, -440(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-1488(%rbp), %rax
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%rax, -1664(%rbp)
.LEHB94:
	call	_ZN2v88internal6torque14NamespaceScopeC1ERSoSt16initializer_listINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE@PLT
.LEHE94:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L588
	call	_ZdlPv@PLT
.L588:
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	-1536(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rdi
.LEHB95:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE95:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	xorl	%esi, %esi
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
.LEHB96:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE96:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	leaq	-424(%rbp), %r14
	movdqa	-1552(%rbp), %xmm4
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1688(%rbp)
	movaps	%xmm4, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rsi
	movq	-1584(%rbp), %rdi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -1696(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB97:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE97:
	movl	$25, %edx
	leaq	.LC95(%rip), %rsi
	movq	%r15, %rdi
.LEHB98:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$9, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$54, %edx
	leaq	.LC96(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE98:
	leaq	-1408(%rbp), %rax
	movq	$1, -1448(%rbp)
	movq	%rax, -1552(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -1440(%rbp)
	movq	$0, -1432(%rbp)
	movl	$0x3f800000, -1424(%rbp)
	movq	$0, -1416(%rbp)
	movq	$0, -1408(%rbp)
.LEHB99:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	216(%rax), %r14
	movq	208(%rax), %rbx
	cmpq	%r14, %rbx
	jne	.L603
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L783:
	movq	96(%rdi), %rdi
.L601:
	testq	%rdi, %rdi
	je	.L602
	cmpl	$5, 8(%rdi)
	movl	$0, %eax
	cmovne	%rax, %rdi
.L602:
	movq	-1528(%rbp), %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal6torque12_GLOBAL__N_124GenerateClassDebugReaderERKNS1_9ClassTypeERSoS6_S6_PSt13unordered_setIPS4_St4hashIS8_ESt8equal_toIS8_ESaIS8_EE
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L604
.L603:
	movq	(%rbx), %rdi
	cmpb	$0, 88(%rdi)
	jne	.L783
	call	_ZNK2v88internal6torque9TypeAlias7ResolveEv@PLT
	movq	%rax, %rdi
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L575:
	testq	%r14, %r14
	jne	.L784
	movq	%r15, %rax
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L604:
	movl	$3, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE99:
	movq	-384(%rbp), %rax
	leaq	-1328(%rbp), %rdi
	leaq	-1312(%rbp), %r14
	movq	$0, -1320(%rbp)
	movq	%rdi, -1560(%rbp)
	movq	%r14, -1328(%rbp)
	movb	$0, -1312(%rbp)
	testq	%rax, %rax
	je	.L605
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L606
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB100:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE100:
.L607:
	movq	-1320(%rbp), %rdx
	movq	-1328(%rbp), %rsi
	movq	%r12, %rdi
.LEHB101:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE101:
	movq	-1328(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L612
	call	_ZdlPv@PLT
.L612:
	movq	-1440(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L616
	.p2align 4,,10
	.p2align 3
.L613:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L613
.L616:
	movq	-1448(%rbp), %rax
	movq	-1456(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-1456(%rbp), %rdi
	movq	$0, -1432(%rbp)
	movq	$0, -1440(%rbp)
	cmpq	-1552(%rbp), %rdi
	je	.L614
	call	_ZdlPv@PLT
.L614:
	movq	.LC2(%rip), %xmm2
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC4(%rip), %xmm2
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -1600(%rbp)
	movaps	%xmm2, -432(%rbp)
	cmpq	-1696(%rbp), %rdi
	je	.L617
	call	_ZdlPv@PLT
.L617:
	movq	-1688(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1584(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1664(%rbp), %rdi
	call	_ZN2v88internal6torque14NamespaceScopeD1Ev@PLT
	movq	-1656(%rbp), %rdi
	call	_ZN2v88internal6torque14NamespaceScopeD1Ev@PLT
	movq	-1608(%rbp), %rdi
	call	_ZN2v88internal6torque17IncludeGuardScopeD1Ev@PLT
	movq	-1184(%rbp), %rax
	movq	%r15, -448(%rbp)
	movq	$0, -440(%rbp)
	movb	$0, -432(%rbp)
	testq	%rax, %rax
	je	.L618
	movq	-1200(%rbp), %r8
	movq	-1192(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L619
	subq	%rcx, %rax
	movq	-1536(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB102:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE102:
.L620:
	leaq	-1344(%rbp), %rax
	movq	%rax, -1552(%rbp)
	movq	%rax, -1360(%rbp)
	movq	-1576(%rbp), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r12
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L623
	testq	%r13, %r13
	je	.L785
.L623:
	movq	%r12, -1456(%rbp)
	cmpq	$15, %r12
	ja	.L786
	cmpq	$1, %r12
	jne	.L628
	movzbl	0(%r13), %eax
	movb	%al, -1344(%rbp)
	movq	-1552(%rbp), %rax
.L629:
	movq	%r12, -1352(%rbp)
	movb	$0, (%rax,%r12)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -1352(%rbp)
	je	.L787
	leaq	-1360(%rbp), %r13
	movl	$1, %edx
	leaq	.LC98(%rip), %rsi
	movq	%r13, %rdi
.LEHB103:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE103:
	movq	-1384(%rbp), %rdx
	movq	-1392(%rbp), %rsi
	movq	%r13, %rdi
.LEHB104:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE104:
	movq	%r14, -1328(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L788
	movq	%rcx, -1328(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -1312(%rbp)
.L636:
	movq	8(%rax), %rcx
	movq	%rcx, -1320(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-1320(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L789
	movq	-1560(%rbp), %rdi
	movl	$2, %edx
	leaq	.LC87(%rip), %rsi
.LEHB105:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE105:
	leaq	-1280(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -1296(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L790
	movq	%rcx, -1296(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -1280(%rbp)
.L639:
	movq	8(%rax), %rcx
	movq	%rcx, -1288(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-1568(%rbp), %rax
	cmpb	$0, 536(%rax)
	jne	.L643
	movq	-1536(%rbp), %rsi
	movq	-1608(%rbp), %rdi
.LEHB106:
	call	_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_@PLT
.LEHE106:
.L643:
	movq	-1296(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	movq	-1328(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L644
	call	_ZdlPv@PLT
.L644:
	movq	-1360(%rbp), %rdi
	cmpq	-1552(%rbp), %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L646
	call	_ZdlPv@PLT
.L646:
	movq	-784(%rbp), %rax
	movq	%r15, -448(%rbp)
	movq	$0, -440(%rbp)
	movb	$0, -432(%rbp)
	testq	%rax, %rax
	je	.L647
	movq	-800(%rbp), %r8
	movq	-792(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L648
	subq	%rcx, %rax
	movq	-1536(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB107:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE107:
.L649:
	movq	-1576(%rbp), %rcx
	movq	-1552(%rbp), %rax
	movq	%rax, -1360(%rbp)
	movq	(%rcx), %rax
	movq	8(%rcx), %r12
	movq	%rax, %rcx
	movq	%rax, -1576(%rbp)
	addq	%r12, %rcx
	je	.L652
	testq	%rax, %rax
	je	.L791
.L652:
	movq	%r12, -1456(%rbp)
	cmpq	$15, %r12
	ja	.L792
	cmpq	$1, %r12
	jne	.L657
	movq	-1576(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -1344(%rbp)
	movq	-1552(%rbp), %rax
.L658:
	movq	%r12, -1352(%rbp)
	movb	$0, (%rax,%r12)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -1352(%rbp)
	je	.L793
	movl	$1, %edx
	leaq	.LC98(%rip), %rsi
	movq	%r13, %rdi
.LEHB108:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE108:
	movq	-1384(%rbp), %rdx
	movq	-1392(%rbp), %rsi
	movq	%r13, %rdi
.LEHB109:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE109:
	movq	%r14, -1328(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L794
	movq	%rcx, -1328(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -1312(%rbp)
.L665:
	movq	8(%rax), %rcx
	movq	%rcx, -1320(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-1320(%rbp), %rax
	cmpq	$2, %rax
	jbe	.L795
	movq	-1560(%rbp), %rdi
	movl	$3, %edx
	leaq	.LC99(%rip), %rsi
.LEHB110:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE110:
	movq	%rbx, -1296(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L796
	movq	%rcx, -1296(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -1280(%rbp)
.L668:
	movq	8(%rax), %rcx
	movq	%rcx, -1288(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-1568(%rbp), %rax
	cmpb	$0, 536(%rax)
	jne	.L672
	movq	-1536(%rbp), %rsi
	movq	-1608(%rbp), %rdi
.LEHB111:
	call	_ZN2v88internal6torque30ReplaceFileContentsIfDifferentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES9_@PLT
.LEHE111:
.L672:
	movq	-1296(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L671
	call	_ZdlPv@PLT
.L671:
	movq	-1328(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L673
	call	_ZdlPv@PLT
.L673:
	movq	-1360(%rbp), %rdi
	cmpq	-1552(%rbp), %rdi
	je	.L674
	call	_ZdlPv@PLT
.L674:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L675
	call	_ZdlPv@PLT
.L675:
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-1600(%rbp), %xmm5
	movq	-752(%rbp), %rdi
	movq	%rax, -848(%rbp)
	addq	$80, %rax
	movq	%rax, -720(%rbp)
	movaps	%xmm5, -832(%rbp)
	cmpq	-1680(%rbp), %rdi
	je	.L676
	call	_ZdlPv@PLT
.L676:
	movq	-1672(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -824(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -848(%rbp)
	movq	-24(%rax), %rax
	movq	-1632(%rbp), %rdi
	movq	%rcx, -848(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -832(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -832(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -848(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -848(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -720(%rbp)
	movq	$0, -840(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-1600(%rbp), %xmm6
	movq	-1152(%rbp), %rdi
	movq	%rax, -1248(%rbp)
	addq	$80, %rax
	movq	%rax, -1120(%rbp)
	movaps	%xmm6, -1232(%rbp)
	cmpq	-1648(%rbp), %rdi
	je	.L677
	call	_ZdlPv@PLT
.L677:
	movq	-1640(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1224(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rax, -1248(%rbp)
	movq	-24(%rax), %rax
	movq	-1616(%rbp), %rdi
	movq	%rcx, -1248(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -1232(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -1232(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -1248(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -1248(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -1240(%rbp)
	movq	%rax, -1120(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1392(%rbp), %rdi
	cmpq	-1624(%rbp), %rdi
	je	.L561
	call	_ZdlPv@PLT
.L561:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L797
	addq	$1672, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_restore_state
	movq	-1536(%rbp), %rdi
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB112:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE112:
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L606:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB113:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE113:
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L648:
	movq	-1536(%rbp), %rdi
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB114:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE114:
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L628:
	testq	%r12, %r12
	jne	.L798
	movq	-1552(%rbp), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L657:
	testq	%r12, %r12
	jne	.L799
	movq	-1552(%rbp), %rax
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L786:
	movq	-1528(%rbp), %rsi
	leaq	-1360(%rbp), %rdi
	xorl	%edx, %edx
.LEHB115:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE115:
	movq	%rax, -1360(%rbp)
	movq	%rax, %rdi
	movq	-1456(%rbp), %rax
	movq	%rax, -1344(%rbp)
.L627:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-1456(%rbp), %r12
	movq	-1360(%rbp), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L781:
	movq	-1528(%rbp), %rsi
	leaq	-448(%rbp), %rdi
	xorl	%edx, %edx
.LEHB116:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE116:
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-1456(%rbp), %rax
	movq	%rax, -432(%rbp)
.L574:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-1456(%rbp), %r14
	movq	-448(%rbp), %rax
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L792:
	movq	-1528(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
.LEHB117:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE117:
	movq	%rax, -1360(%rbp)
	movq	%rax, %rdi
	movq	-1456(%rbp), %rax
	movq	%rax, -1344(%rbp)
.L656:
	movq	-1576(%rbp), %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	-1456(%rbp), %r12
	movq	-1360(%rbp), %rax
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L790:
	movdqu	16(%rax), %xmm7
	movaps	%xmm7, -1280(%rbp)
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L788:
	movdqu	16(%rax), %xmm7
	movaps	%xmm7, -1312(%rbp)
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L796:
	movdqu	16(%rax), %xmm7
	movaps	%xmm7, -1280(%rbp)
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L794:
	movdqu	16(%rax), %xmm7
	movaps	%xmm7, -1312(%rbp)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L605:
	leaq	-352(%rbp), %rsi
.LEHB118:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE118:
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L618:
	movq	-1536(%rbp), %rdi
	leaq	-1152(%rbp), %rsi
.LEHB119:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE119:
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L647:
	movq	-1536(%rbp), %rdi
	leaq	-752(%rbp), %rsi
.LEHB120:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE120:
	jmp	.L649
.L782:
	leaq	.LC37(%rip), %rdi
.LEHB121:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE121:
.L795:
	leaq	.LC37(%rip), %rdi
.LEHB122:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE122:
.L797:
	call	__stack_chk_fail@PLT
.L798:
	movq	-1552(%rbp), %rdi
	jmp	.L627
.L799:
	movq	-1552(%rbp), %rdi
	jmp	.L656
.L780:
	leaq	.LC8(%rip), %rdi
.LEHB123:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE123:
.L785:
	leaq	.LC8(%rip), %rdi
.LEHB124:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE124:
.L787:
	leaq	.LC37(%rip), %rdi
.LEHB125:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE125:
.L784:
	movq	%r15, %rdi
	jmp	.L574
.L791:
	leaq	.LC8(%rip), %rdi
.LEHB126:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE126:
.L793:
	leaq	.LC37(%rip), %rdi
.LEHB127:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE127:
.L789:
	leaq	.LC37(%rip), %rdi
.LEHB128:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE128:
.L726:
	endbr64
	movq	%rax, %rbx
	jmp	.L569
.L729:
	endbr64
	movq	%rax, %rbx
	jmp	.L663
.L710:
	endbr64
	movq	%rax, %rbx
	jmp	.L590
.L730:
	endbr64
	movq	%rax, %rbx
	jmp	.L595
.L734:
	endbr64
	movq	%rax, %rbx
	jmp	.L663
.L717:
	endbr64
	movq	%rax, %rbx
	jmp	.L689
.L718:
	endbr64
	movq	%rax, %r12
	jmp	.L687
.L715:
	endbr64
	movq	%rax, %rbx
	jmp	.L663
.L714:
	endbr64
	movq	%rax, %rbx
	jmp	.L681
.L721:
	endbr64
	movq	%rax, %rbx
	jmp	.L696
.L722:
	endbr64
	movq	%rax, %r12
	jmp	.L694
.L720:
	endbr64
	movq	%rax, %rbx
	jmp	.L698
.L737:
	endbr64
	movq	%rax, %rbx
	jmp	.L661
.L708:
	endbr64
	movq	%rax, %rbx
	jmp	.L587
.L707:
	endbr64
	movq	%rax, %rbx
	jmp	.L663
.L709:
	endbr64
	movq	%rax, %rbx
	jmp	.L585
.L711:
	endbr64
	movq	%rax, %rbx
	jmp	.L592
.L736:
	endbr64
	movq	%rax, %rbx
	jmp	.L663
.L728:
	endbr64
	movq	%rax, %rbx
	jmp	.L568
.L723:
	endbr64
	movq	%rax, %rbx
	jmp	.L564
.L724:
	endbr64
	movq	%rax, %rbx
	jmp	.L565
.L719:
	endbr64
	movq	%rax, %rbx
	jmp	.L663
.L713:
	endbr64
	movq	%rax, %rbx
	jmp	.L611
.L735:
	endbr64
	movq	%rax, %rbx
	jmp	.L632
.L716:
	endbr64
	movq	%rax, %rbx
	jmp	.L691
.L733:
	endbr64
	movq	%rax, %rbx
	jmp	.L609
.L712:
	endbr64
	movq	%rax, %rbx
	jmp	.L686
.L725:
	endbr64
	movq	%rax, %rbx
	jmp	.L563
.L706:
	endbr64
	movq	%rax, %rbx
	jmp	.L581
.L727:
	endbr64
	movq	%rax, %rbx
	jmp	.L570
.L732:
	endbr64
	movq	%rax, %rbx
	jmp	.L594
.L731:
	endbr64
	movq	%rax, %rbx
	jmp	.L596
	.section	.gcc_except_table._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA7056:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7056-.LLSDACSB7056
.LLSDACSB7056:
	.uleb128 .LEHB81-.LFB7056
	.uleb128 .LEHE81-.LEHB81
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB82-.LFB7056
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L723-.LFB7056
	.uleb128 0
	.uleb128 .LEHB83-.LFB7056
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L725-.LFB7056
	.uleb128 0
	.uleb128 .LEHB84-.LFB7056
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L724-.LFB7056
	.uleb128 0
	.uleb128 .LEHB85-.LFB7056
	.uleb128 .LEHE85-.LEHB85
	.uleb128 .L726-.LFB7056
	.uleb128 0
	.uleb128 .LEHB86-.LFB7056
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L728-.LFB7056
	.uleb128 0
	.uleb128 .LEHB87-.LFB7056
	.uleb128 .LEHE87-.LEHB87
	.uleb128 .L727-.LFB7056
	.uleb128 0
	.uleb128 .LEHB88-.LFB7056
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L706-.LFB7056
	.uleb128 0
	.uleb128 .LEHB89-.LFB7056
	.uleb128 .LEHE89-.LEHB89
	.uleb128 .L729-.LFB7056
	.uleb128 0
	.uleb128 .LEHB90-.LFB7056
	.uleb128 .LEHE90-.LEHB90
	.uleb128 .L707-.LFB7056
	.uleb128 0
	.uleb128 .LEHB91-.LFB7056
	.uleb128 .LEHE91-.LEHB91
	.uleb128 .L708-.LFB7056
	.uleb128 0
	.uleb128 .LEHB92-.LFB7056
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L709-.LFB7056
	.uleb128 0
	.uleb128 .LEHB93-.LFB7056
	.uleb128 .LEHE93-.LEHB93
	.uleb128 .L711-.LFB7056
	.uleb128 0
	.uleb128 .LEHB94-.LFB7056
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L710-.LFB7056
	.uleb128 0
	.uleb128 .LEHB95-.LFB7056
	.uleb128 .LEHE95-.LEHB95
	.uleb128 .L730-.LFB7056
	.uleb128 0
	.uleb128 .LEHB96-.LFB7056
	.uleb128 .LEHE96-.LEHB96
	.uleb128 .L732-.LFB7056
	.uleb128 0
	.uleb128 .LEHB97-.LFB7056
	.uleb128 .LEHE97-.LEHB97
	.uleb128 .L731-.LFB7056
	.uleb128 0
	.uleb128 .LEHB98-.LFB7056
	.uleb128 .LEHE98-.LEHB98
	.uleb128 .L712-.LFB7056
	.uleb128 0
	.uleb128 .LEHB99-.LFB7056
	.uleb128 .LEHE99-.LEHB99
	.uleb128 .L713-.LFB7056
	.uleb128 0
	.uleb128 .LEHB100-.LFB7056
	.uleb128 .LEHE100-.LEHB100
	.uleb128 .L733-.LFB7056
	.uleb128 0
	.uleb128 .LEHB101-.LFB7056
	.uleb128 .LEHE101-.LEHB101
	.uleb128 .L714-.LFB7056
	.uleb128 0
	.uleb128 .LEHB102-.LFB7056
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L734-.LFB7056
	.uleb128 0
	.uleb128 .LEHB103-.LFB7056
	.uleb128 .LEHE103-.LEHB103
	.uleb128 .L735-.LFB7056
	.uleb128 0
	.uleb128 .LEHB104-.LFB7056
	.uleb128 .LEHE104-.LEHB104
	.uleb128 .L716-.LFB7056
	.uleb128 0
	.uleb128 .LEHB105-.LFB7056
	.uleb128 .LEHE105-.LEHB105
	.uleb128 .L717-.LFB7056
	.uleb128 0
	.uleb128 .LEHB106-.LFB7056
	.uleb128 .LEHE106-.LEHB106
	.uleb128 .L718-.LFB7056
	.uleb128 0
	.uleb128 .LEHB107-.LFB7056
	.uleb128 .LEHE107-.LEHB107
	.uleb128 .L736-.LFB7056
	.uleb128 0
	.uleb128 .LEHB108-.LFB7056
	.uleb128 .LEHE108-.LEHB108
	.uleb128 .L737-.LFB7056
	.uleb128 0
	.uleb128 .LEHB109-.LFB7056
	.uleb128 .LEHE109-.LEHB109
	.uleb128 .L720-.LFB7056
	.uleb128 0
	.uleb128 .LEHB110-.LFB7056
	.uleb128 .LEHE110-.LEHB110
	.uleb128 .L721-.LFB7056
	.uleb128 0
	.uleb128 .LEHB111-.LFB7056
	.uleb128 .LEHE111-.LEHB111
	.uleb128 .L722-.LFB7056
	.uleb128 0
	.uleb128 .LEHB112-.LFB7056
	.uleb128 .LEHE112-.LEHB112
	.uleb128 .L734-.LFB7056
	.uleb128 0
	.uleb128 .LEHB113-.LFB7056
	.uleb128 .LEHE113-.LEHB113
	.uleb128 .L733-.LFB7056
	.uleb128 0
	.uleb128 .LEHB114-.LFB7056
	.uleb128 .LEHE114-.LEHB114
	.uleb128 .L736-.LFB7056
	.uleb128 0
	.uleb128 .LEHB115-.LFB7056
	.uleb128 .LEHE115-.LEHB115
	.uleb128 .L715-.LFB7056
	.uleb128 0
	.uleb128 .LEHB116-.LFB7056
	.uleb128 .LEHE116-.LEHB116
	.uleb128 .L706-.LFB7056
	.uleb128 0
	.uleb128 .LEHB117-.LFB7056
	.uleb128 .LEHE117-.LEHB117
	.uleb128 .L719-.LFB7056
	.uleb128 0
	.uleb128 .LEHB118-.LFB7056
	.uleb128 .LEHE118-.LEHB118
	.uleb128 .L733-.LFB7056
	.uleb128 0
	.uleb128 .LEHB119-.LFB7056
	.uleb128 .LEHE119-.LEHB119
	.uleb128 .L734-.LFB7056
	.uleb128 0
	.uleb128 .LEHB120-.LFB7056
	.uleb128 .LEHE120-.LEHB120
	.uleb128 .L736-.LFB7056
	.uleb128 0
	.uleb128 .LEHB121-.LFB7056
	.uleb128 .LEHE121-.LEHB121
	.uleb128 .L729-.LFB7056
	.uleb128 0
	.uleb128 .LEHB122-.LFB7056
	.uleb128 .LEHE122-.LEHB122
	.uleb128 .L721-.LFB7056
	.uleb128 0
	.uleb128 .LEHB123-.LFB7056
	.uleb128 .LEHE123-.LEHB123
	.uleb128 .L706-.LFB7056
	.uleb128 0
	.uleb128 .LEHB124-.LFB7056
	.uleb128 .LEHE124-.LEHB124
	.uleb128 .L715-.LFB7056
	.uleb128 0
	.uleb128 .LEHB125-.LFB7056
	.uleb128 .LEHE125-.LEHB125
	.uleb128 .L735-.LFB7056
	.uleb128 0
	.uleb128 .LEHB126-.LFB7056
	.uleb128 .LEHE126-.LEHB126
	.uleb128 .L719-.LFB7056
	.uleb128 0
	.uleb128 .LEHB127-.LFB7056
	.uleb128 .LEHE127-.LEHB127
	.uleb128 .L737-.LFB7056
	.uleb128 0
	.uleb128 .LEHB128-.LFB7056
	.uleb128 .LEHE128-.LEHB128
	.uleb128 .L717-.LFB7056
	.uleb128 0
.LLSDACSE7056:
	.section	.text._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC7056
	.type	_ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB7056:
.L568:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -848(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -848(%rbp,%rax)
	movq	$0, -840(%rbp)
.L569:
	movq	-1632(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -720(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L571:
	movq	-1712(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L566:
	movq	-1392(%rbp), %rdi
	cmpq	-1624(%rbp), %rdi
	je	.L701
	call	_ZdlPv@PLT
.L701:
	movq	%rbx, %rdi
.LEHB129:
	call	_Unwind_Resume@PLT
.LEHE129:
.L661:
	movq	-1360(%rbp), %rdi
	cmpq	-1552(%rbp), %rdi
	je	.L663
	call	_ZdlPv@PLT
.L663:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L581
	call	_ZdlPv@PLT
	jmp	.L581
.L590:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L592
	call	_ZdlPv@PLT
	jmp	.L592
.L594:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L595:
	movq	-1584(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L597:
	movq	-1664(%rbp), %rdi
	call	_ZN2v88internal6torque14NamespaceScopeD1Ev@PLT
.L592:
	movq	-1656(%rbp), %rdi
	call	_ZN2v88internal6torque14NamespaceScopeD1Ev@PLT
.L587:
	movq	-1608(%rbp), %rdi
	call	_ZN2v88internal6torque17IncludeGuardScopeD1Ev@PLT
.L581:
	movq	-1704(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	jmp	.L571
.L687:
	movq	-1296(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L688
	call	_ZdlPv@PLT
.L688:
	movq	%r12, %rbx
.L689:
	movq	-1328(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L691
	call	_ZdlPv@PLT
.L691:
	movq	-1360(%rbp), %rdi
	cmpq	-1552(%rbp), %rdi
	je	.L663
	call	_ZdlPv@PLT
	jmp	.L663
.L681:
	movq	-1328(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L611
	call	_ZdlPv@PLT
.L611:
	movq	-1440(%rbp), %rdi
.L684:
	testq	%rdi, %rdi
	jne	.L800
	movq	-1448(%rbp), %rax
	movq	-1456(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-1456(%rbp), %rdi
	movq	$0, -1432(%rbp)
	movq	$0, -1440(%rbp)
	cmpq	-1552(%rbp), %rdi
	je	.L686
	call	_ZdlPv@PLT
.L686:
	movq	-1536(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	jmp	.L597
.L694:
	movq	-1296(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L695
	call	_ZdlPv@PLT
.L695:
	movq	%r12, %rbx
.L696:
	movq	-1328(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L698
	call	_ZdlPv@PLT
.L698:
	movq	-1360(%rbp), %rdi
	cmpq	-1552(%rbp), %rdi
	je	.L663
	call	_ZdlPv@PLT
	jmp	.L663
.L800:
	movq	(%rdi), %r12
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	jmp	.L684
.L585:
	movq	-448(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L587
	call	_ZdlPv@PLT
	jmp	.L587
.L563:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -1248(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1248(%rbp,%rax)
	movq	$0, -1240(%rbp)
.L564:
	movq	-1616(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1120(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L566
.L565:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -1248(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1248(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -1232(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1232(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -1248(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1248(%rbp,%rax)
	movq	$0, -1240(%rbp)
	jmp	.L564
.L632:
	movq	-1360(%rbp), %rdi
	cmpq	-1552(%rbp), %rdi
	je	.L663
	call	_ZdlPv@PLT
	jmp	.L663
.L609:
	movq	-1328(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L611
	call	_ZdlPv@PLT
	jmp	.L611
.L570:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -848(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -848(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -832(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -832(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -848(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -848(%rbp,%rax)
	movq	$0, -840(%rbp)
	jmp	.L569
.L596:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEED1Ev
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L595
	.cfi_endproc
.LFE7056:
	.section	.gcc_except_table._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC7056:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC7056-.LLSDACSBC7056
.LLSDACSBC7056:
	.uleb128 .LEHB129-.LCOLDB102
	.uleb128 .LEHE129-.LEHB129
	.uleb128 0
	.uleb128 0
.LLSDACSEC7056:
	.section	.text.unlikely._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE102:
	.section	.text._ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE102:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_GLOBAL__sub_I__ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB12708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE12708:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_GLOBAL__sub_I__ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC2:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC3:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC4:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC100:
	.quad	7306014469382040675
	.quad	7233174017596159330
	.align 16
.LC101:
	.quad	7454972945796905078
	.quad	6877671114477496415
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
