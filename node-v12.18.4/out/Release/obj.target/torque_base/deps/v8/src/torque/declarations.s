	.file	"declarations.cc"
	.text
	.section	.rodata._ZNK2v88internal6torque10Declarable9type_nameEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<<unknown>>"
	.section	.text._ZNK2v88internal6torque10Declarable9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque10Declarable9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque10Declarable9type_nameEv
	.type	_ZNK2v88internal6torque10Declarable9type_nameEv, @function
_ZNK2v88internal6torque10Declarable9type_nameEv:
.LFB5936:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE5936:
	.size	_ZNK2v88internal6torque10Declarable9type_nameEv, .-_ZNK2v88internal6torque10Declarable9type_nameEv
	.section	.rodata._ZNK2v88internal6torque5Scope9type_nameEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"scope"
	.section	.text._ZNK2v88internal6torque5Scope9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque5Scope9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque5Scope9type_nameEv
	.type	_ZNK2v88internal6torque5Scope9type_nameEv, @function
_ZNK2v88internal6torque5Scope9type_nameEv:
.LFB5949:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	ret
	.cfi_endproc
.LFE5949:
	.size	_ZNK2v88internal6torque5Scope9type_nameEv, .-_ZNK2v88internal6torque5Scope9type_nameEv
	.section	.text._ZN2v88internal6torque10DeclarableD2Ev,"axG",@progbits,_ZN2v88internal6torque10DeclarableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10DeclarableD2Ev
	.type	_ZN2v88internal6torque10DeclarableD2Ev, @function
_ZN2v88internal6torque10DeclarableD2Ev:
.LFB5954:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5954:
	.size	_ZN2v88internal6torque10DeclarableD2Ev, .-_ZN2v88internal6torque10DeclarableD2Ev
	.weak	_ZN2v88internal6torque10DeclarableD1Ev
	.set	_ZN2v88internal6torque10DeclarableD1Ev,_ZN2v88internal6torque10DeclarableD2Ev
	.section	.rodata._ZNK2v88internal6torque9Namespace9type_nameEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"namespace"
	.section	.text._ZNK2v88internal6torque9Namespace9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque9Namespace9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9Namespace9type_nameEv
	.type	_ZNK2v88internal6torque9Namespace9type_nameEv, @function
_ZNK2v88internal6torque9Namespace9type_nameEv:
.LFB6003:
	.cfi_startproc
	endbr64
	leaq	.LC2(%rip), %rax
	ret
	.cfi_endproc
.LFE6003:
	.size	_ZNK2v88internal6torque9Namespace9type_nameEv, .-_ZNK2v88internal6torque9Namespace9type_nameEv
	.section	.rodata._ZNK2v88internal6torque5Value9type_nameEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"value"
	.section	.text._ZNK2v88internal6torque5Value9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque5Value9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque5Value9type_nameEv
	.type	_ZNK2v88internal6torque5Value9type_nameEv, @function
_ZNK2v88internal6torque5Value9type_nameEv:
.LFB6017:
	.cfi_startproc
	endbr64
	leaq	.LC3(%rip), %rax
	ret
	.cfi_endproc
.LFE6017:
	.size	_ZNK2v88internal6torque5Value9type_nameEv, .-_ZNK2v88internal6torque5Value9type_nameEv
	.section	.text._ZNK2v88internal6torque5Value7IsConstEv,"axG",@progbits,_ZNK2v88internal6torque5Value7IsConstEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque5Value7IsConstEv
	.type	_ZNK2v88internal6torque5Value7IsConstEv, @function
_ZNK2v88internal6torque5Value7IsConstEv:
.LFB6021:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE6021:
	.size	_ZNK2v88internal6torque5Value7IsConstEv, .-_ZNK2v88internal6torque5Value7IsConstEv
	.section	.rodata._ZNK2v88internal6torque17NamespaceConstant9type_nameEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"constant"
	.section	.text._ZNK2v88internal6torque17NamespaceConstant9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque17NamespaceConstant9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque17NamespaceConstant9type_nameEv
	.type	_ZNK2v88internal6torque17NamespaceConstant9type_nameEv, @function
_ZNK2v88internal6torque17NamespaceConstant9type_nameEv:
.LFB6057:
	.cfi_startproc
	endbr64
	leaq	.LC4(%rip), %rax
	ret
	.cfi_endproc
.LFE6057:
	.size	_ZNK2v88internal6torque17NamespaceConstant9type_nameEv, .-_ZNK2v88internal6torque17NamespaceConstant9type_nameEv
	.section	.text._ZNK2v88internal6torque14ExternConstant9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque14ExternConstant9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque14ExternConstant9type_nameEv
	.type	_ZNK2v88internal6torque14ExternConstant9type_nameEv, @function
_ZNK2v88internal6torque14ExternConstant9type_nameEv:
.LFB6071:
	.cfi_startproc
	endbr64
	leaq	.LC4(%rip), %rax
	ret
	.cfi_endproc
.LFE6071:
	.size	_ZNK2v88internal6torque14ExternConstant9type_nameEv, .-_ZNK2v88internal6torque14ExternConstant9type_nameEv
	.section	.rodata._ZNK2v88internal6torque8Callable9type_nameEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"callable"
	.section	.text._ZNK2v88internal6torque8Callable9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque8Callable9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque8Callable9type_nameEv
	.type	_ZNK2v88internal6torque8Callable9type_nameEv, @function
_ZNK2v88internal6torque8Callable9type_nameEv:
.LFB6082:
	.cfi_startproc
	endbr64
	leaq	.LC5(%rip), %rax
	ret
	.cfi_endproc
.LFE6082:
	.size	_ZNK2v88internal6torque8Callable9type_nameEv, .-_ZNK2v88internal6torque8Callable9type_nameEv
	.section	.text._ZNK2v88internal6torque8Callable15ShouldBeInlinedEv,"axG",@progbits,_ZNK2v88internal6torque8Callable15ShouldBeInlinedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque8Callable15ShouldBeInlinedEv
	.type	_ZNK2v88internal6torque8Callable15ShouldBeInlinedEv, @function
_ZNK2v88internal6torque8Callable15ShouldBeInlinedEv:
.LFB6095:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6095:
	.size	_ZNK2v88internal6torque8Callable15ShouldBeInlinedEv, .-_ZNK2v88internal6torque8Callable15ShouldBeInlinedEv
	.section	.text._ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv,"axG",@progbits,_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv
	.type	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv, @function
_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv:
.LFB6096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*24(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE6096:
	.size	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv, .-_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv
	.section	.rodata._ZNK2v88internal6torque5Macro9type_nameEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"macro"
	.section	.text._ZNK2v88internal6torque5Macro9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque5Macro9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque5Macro9type_nameEv
	.type	_ZNK2v88internal6torque5Macro9type_nameEv, @function
_ZNK2v88internal6torque5Macro9type_nameEv:
.LFB6109:
	.cfi_startproc
	endbr64
	leaq	.LC6(%rip), %rax
	ret
	.cfi_endproc
.LFE6109:
	.size	_ZNK2v88internal6torque5Macro9type_nameEv, .-_ZNK2v88internal6torque5Macro9type_nameEv
	.section	.text._ZNK2v88internal6torque5Macro15ShouldBeInlinedEv,"axG",@progbits,_ZNK2v88internal6torque5Macro15ShouldBeInlinedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque5Macro15ShouldBeInlinedEv
	.type	_ZNK2v88internal6torque5Macro15ShouldBeInlinedEv, @function
_ZNK2v88internal6torque5Macro15ShouldBeInlinedEv:
.LFB6112:
	.cfi_startproc
	endbr64
	movq	304(%rdi), %rsi
	movq	312(%rdi), %r8
	cmpq	%r8, %rsi
	je	.L16
.L20:
	movq	8(%rsi), %rax
	movq	16(%rsi), %rcx
	cmpq	%rax, %rcx
	jne	.L19
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L17
.L19:
	movq	(%rax), %rdx
	cmpl	$4, 8(%rdx)
	jne	.L24
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$32, %rsi
	cmpq	%rsi, %r8
	jne	.L20
.L16:
	movq	160(%rdi), %rax
	cmpb	$37, (%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE6112:
	.size	_ZNK2v88internal6torque5Macro15ShouldBeInlinedEv, .-_ZNK2v88internal6torque5Macro15ShouldBeInlinedEv
	.section	.rodata._ZNK2v88internal6torque11ExternMacro9type_nameEv.str1.1,"aMS",@progbits,1
.LC7:
	.string	"ExternMacro"
	.section	.text._ZNK2v88internal6torque11ExternMacro9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque11ExternMacro9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque11ExternMacro9type_nameEv
	.type	_ZNK2v88internal6torque11ExternMacro9type_nameEv, @function
_ZNK2v88internal6torque11ExternMacro9type_nameEv:
.LFB6130:
	.cfi_startproc
	endbr64
	leaq	.LC7(%rip), %rax
	ret
	.cfi_endproc
.LFE6130:
	.size	_ZNK2v88internal6torque11ExternMacro9type_nameEv, .-_ZNK2v88internal6torque11ExternMacro9type_nameEv
	.section	.rodata._ZNK2v88internal6torque11TorqueMacro9type_nameEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"TorqueMacro"
	.section	.text._ZNK2v88internal6torque11TorqueMacro9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque11TorqueMacro9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque11TorqueMacro9type_nameEv
	.type	_ZNK2v88internal6torque11TorqueMacro9type_nameEv, @function
_ZNK2v88internal6torque11TorqueMacro9type_nameEv:
.LFB6143:
	.cfi_startproc
	endbr64
	leaq	.LC8(%rip), %rax
	ret
	.cfi_endproc
.LFE6143:
	.size	_ZNK2v88internal6torque11TorqueMacro9type_nameEv, .-_ZNK2v88internal6torque11TorqueMacro9type_nameEv
	.section	.rodata._ZNK2v88internal6torque6Method9type_nameEv.str1.1,"aMS",@progbits,1
.LC9:
	.string	"Method"
	.section	.text._ZNK2v88internal6torque6Method9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque6Method9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque6Method9type_nameEv
	.type	_ZNK2v88internal6torque6Method9type_nameEv, @function
_ZNK2v88internal6torque6Method9type_nameEv:
.LFB6155:
	.cfi_startproc
	endbr64
	leaq	.LC9(%rip), %rax
	ret
	.cfi_endproc
.LFE6155:
	.size	_ZNK2v88internal6torque6Method9type_nameEv, .-_ZNK2v88internal6torque6Method9type_nameEv
	.section	.rodata._ZNK2v88internal6torque7Builtin9type_nameEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"builtin"
	.section	.text._ZNK2v88internal6torque7Builtin9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque7Builtin9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque7Builtin9type_nameEv
	.type	_ZNK2v88internal6torque7Builtin9type_nameEv, @function
_ZNK2v88internal6torque7Builtin9type_nameEv:
.LFB6169:
	.cfi_startproc
	endbr64
	leaq	.LC10(%rip), %rax
	ret
	.cfi_endproc
.LFE6169:
	.size	_ZNK2v88internal6torque7Builtin9type_nameEv, .-_ZNK2v88internal6torque7Builtin9type_nameEv
	.section	.rodata._ZNK2v88internal6torque15RuntimeFunction9type_nameEv.str1.1,"aMS",@progbits,1
.LC11:
	.string	"runtime"
	.section	.text._ZNK2v88internal6torque15RuntimeFunction9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque15RuntimeFunction9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque15RuntimeFunction9type_nameEv
	.type	_ZNK2v88internal6torque15RuntimeFunction9type_nameEv, @function
_ZNK2v88internal6torque15RuntimeFunction9type_nameEv:
.LFB6181:
	.cfi_startproc
	endbr64
	leaq	.LC11(%rip), %rax
	ret
	.cfi_endproc
.LFE6181:
	.size	_ZNK2v88internal6torque15RuntimeFunction9type_nameEv, .-_ZNK2v88internal6torque15RuntimeFunction9type_nameEv
	.section	.rodata._ZNK2v88internal6torque9Intrinsic9type_nameEv.str1.1,"aMS",@progbits,1
.LC12:
	.string	"intrinsic"
	.section	.text._ZNK2v88internal6torque9Intrinsic9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque9Intrinsic9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9Intrinsic9type_nameEv
	.type	_ZNK2v88internal6torque9Intrinsic9type_nameEv, @function
_ZNK2v88internal6torque9Intrinsic9type_nameEv:
.LFB6189:
	.cfi_startproc
	endbr64
	leaq	.LC12(%rip), %rax
	ret
	.cfi_endproc
.LFE6189:
	.size	_ZNK2v88internal6torque9Intrinsic9type_nameEv, .-_ZNK2v88internal6torque9Intrinsic9type_nameEv
	.section	.rodata._ZNK2v88internal6torque7Generic9type_nameEv.str1.1,"aMS",@progbits,1
.LC13:
	.string	"generic"
	.section	.text._ZNK2v88internal6torque7Generic9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque7Generic9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque7Generic9type_nameEv
	.type	_ZNK2v88internal6torque7Generic9type_nameEv, @function
_ZNK2v88internal6torque7Generic9type_nameEv:
.LFB6203:
	.cfi_startproc
	endbr64
	leaq	.LC13(%rip), %rax
	ret
	.cfi_endproc
.LFE6203:
	.size	_ZNK2v88internal6torque7Generic9type_nameEv, .-_ZNK2v88internal6torque7Generic9type_nameEv
	.section	.rodata._ZNK2v88internal6torque17GenericStructType9type_nameEv.str1.1,"aMS",@progbits,1
.LC14:
	.string	"generic_type"
	.section	.text._ZNK2v88internal6torque17GenericStructType9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque17GenericStructType9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque17GenericStructType9type_nameEv
	.type	_ZNK2v88internal6torque17GenericStructType9type_nameEv, @function
_ZNK2v88internal6torque17GenericStructType9type_nameEv:
.LFB6221:
	.cfi_startproc
	endbr64
	leaq	.LC14(%rip), %rax
	ret
	.cfi_endproc
.LFE6221:
	.size	_ZNK2v88internal6torque17GenericStructType9type_nameEv, .-_ZNK2v88internal6torque17GenericStructType9type_nameEv
	.section	.rodata._ZNK2v88internal6torque9TypeAlias9type_nameEv.str1.1,"aMS",@progbits,1
.LC15:
	.string	"type_alias"
	.section	.text._ZNK2v88internal6torque9TypeAlias9type_nameEv,"axG",@progbits,_ZNK2v88internal6torque9TypeAlias9type_nameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque9TypeAlias9type_nameEv
	.type	_ZNK2v88internal6torque9TypeAlias9type_nameEv, @function
_ZNK2v88internal6torque9TypeAlias9type_nameEv:
.LFB6239:
	.cfi_startproc
	endbr64
	leaq	.LC15(%rip), %rax
	ret
	.cfi_endproc
.LFE6239:
	.size	_ZNK2v88internal6torque9TypeAlias9type_nameEv, .-_ZNK2v88internal6torque9TypeAlias9type_nameEv
	.section	.text._ZN2v88internal6torque9TypeAliasD2Ev,"axG",@progbits,_ZN2v88internal6torque9TypeAliasD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9TypeAliasD2Ev
	.type	_ZN2v88internal6torque9TypeAliasD2Ev, @function
_ZN2v88internal6torque9TypeAliasD2Ev:
.LFB9528:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9528:
	.size	_ZN2v88internal6torque9TypeAliasD2Ev, .-_ZN2v88internal6torque9TypeAliasD2Ev
	.weak	_ZN2v88internal6torque9TypeAliasD1Ev
	.set	_ZN2v88internal6torque9TypeAliasD1Ev,_ZN2v88internal6torque9TypeAliasD2Ev
	.section	.text._ZN2v88internal6torque10DeclarableD0Ev,"axG",@progbits,_ZN2v88internal6torque10DeclarableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10DeclarableD0Ev
	.type	_ZN2v88internal6torque10DeclarableD0Ev, @function
_ZN2v88internal6torque10DeclarableD0Ev:
.LFB5956:
	.cfi_startproc
	endbr64
	movl	$72, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5956:
	.size	_ZN2v88internal6torque10DeclarableD0Ev, .-_ZN2v88internal6torque10DeclarableD0Ev
	.section	.text._ZN2v88internal6torque9TypeAliasD0Ev,"axG",@progbits,_ZN2v88internal6torque9TypeAliasD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9TypeAliasD0Ev
	.type	_ZN2v88internal6torque9TypeAliasD0Ev, @function
_ZN2v88internal6torque9TypeAliasD0Ev:
.LFB9530:
	.cfi_startproc
	endbr64
	movl	$128, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9530:
	.size	_ZN2v88internal6torque9TypeAliasD0Ev, .-_ZN2v88internal6torque9TypeAliasD0Ev
	.section	.text._ZNK2v88internal6torque6Method15ShouldBeInlinedEv,"axG",@progbits,_ZNK2v88internal6torque6Method15ShouldBeInlinedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque6Method15ShouldBeInlinedEv
	.type	_ZNK2v88internal6torque6Method15ShouldBeInlinedEv, @function
_ZNK2v88internal6torque6Method15ShouldBeInlinedEv:
.LFB6158:
	.cfi_startproc
	endbr64
	movq	304(%rdi), %rsi
	movq	312(%rdi), %r8
	cmpq	%rsi, %r8
	je	.L38
.L42:
	movq	8(%rsi), %rax
	movq	16(%rsi), %rcx
	cmpq	%rax, %rcx
	jne	.L41
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$8, %rax
	cmpq	%rax, %rcx
	je	.L39
.L41:
	movq	(%rax), %rdx
	cmpl	$4, 8(%rdx)
	jne	.L47
	movl	$1, %eax
.L37:
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$32, %rsi
	cmpq	%rsi, %r8
	jne	.L42
.L38:
	movq	160(%rdi), %rdx
	movl	$1, %eax
	cmpb	$37, (%rdx)
	je	.L37
	movq	288(%rdi), %rdx
	movq	256(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	cmpl	$4, 8(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE6158:
	.size	_ZNK2v88internal6torque6Method15ShouldBeInlinedEv, .-_ZNK2v88internal6torque6Method15ShouldBeInlinedEv
	.section	.text._ZN2v88internal6torque7GenericD2Ev,"axG",@progbits,_ZN2v88internal6torque7GenericD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque7GenericD2Ev
	.type	_ZN2v88internal6torque7GenericD2Ev, @function
_ZN2v88internal6torque7GenericD2Ev:
.LFB9692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque7GenericE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	128(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L52
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L65:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L49
.L51:
	movq	%r13, %r12
.L52:
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L65
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L51
.L49:
	movq	120(%rbx), %rax
	movq	112(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%rbx), %rdi
	leaq	160(%rbx), %rax
	movq	$0, 136(%rbx)
	movq	$0, 128(%rbx)
	cmpq	%rax, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	72(%rbx), %rdi
	addq	$88, %rbx
	cmpq	%rbx, %rdi
	je	.L48
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9692:
	.size	_ZN2v88internal6torque7GenericD2Ev, .-_ZN2v88internal6torque7GenericD2Ev
	.weak	_ZN2v88internal6torque7GenericD1Ev
	.set	_ZN2v88internal6torque7GenericD1Ev,_ZN2v88internal6torque7GenericD2Ev
	.section	.text._ZN2v88internal6torque17GenericStructTypeD2Ev,"axG",@progbits,_ZN2v88internal6torque17GenericStructTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque17GenericStructTypeD2Ev
	.type	_ZN2v88internal6torque17GenericStructTypeD2Ev, @function
_ZN2v88internal6torque17GenericStructTypeD2Ev:
.LFB9711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque17GenericStructTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	128(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L70
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L83:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L67
.L69:
	movq	%r13, %r12
.L70:
	movq	8(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L83
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L69
.L67:
	movq	120(%rbx), %rax
	movq	112(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%rbx), %rdi
	leaq	160(%rbx), %rax
	movq	$0, 136(%rbx)
	movq	$0, 128(%rbx)
	cmpq	%rax, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	72(%rbx), %rdi
	addq	$88, %rbx
	cmpq	%rbx, %rdi
	je	.L66
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9711:
	.size	_ZN2v88internal6torque17GenericStructTypeD2Ev, .-_ZN2v88internal6torque17GenericStructTypeD2Ev
	.weak	_ZN2v88internal6torque17GenericStructTypeD1Ev
	.set	_ZN2v88internal6torque17GenericStructTypeD1Ev,_ZN2v88internal6torque17GenericStructTypeD2Ev
	.section	.rodata._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0:
.LFB14285:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L85
	testq	%rsi, %rsi
	je	.L101
.L85:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L102
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L88
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L89:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L89
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L87:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L89
.L101:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14285:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
	.section	.text._ZN2v88internal6torque17GenericStructTypeD0Ev,"axG",@progbits,_ZN2v88internal6torque17GenericStructTypeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque17GenericStructTypeD0Ev
	.type	_ZN2v88internal6torque17GenericStructTypeD0Ev, @function
_ZN2v88internal6torque17GenericStructTypeD0Ev:
.LFB9713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque17GenericStructTypeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	128(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L108
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L121:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L105
.L107:
	movq	%rbx, %r13
.L108:
	movq	8(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	jne	.L121
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L107
.L105:
	movq	120(%r12), %rax
	movq	112(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%r12), %rdi
	leaq	160(%r12), %rax
	movq	$0, 136(%r12)
	movq	$0, 128(%r12)
	cmpq	%rax, %rdi
	je	.L109
	call	_ZdlPv@PLT
.L109:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L110
	call	_ZdlPv@PLT
.L110:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9713:
	.size	_ZN2v88internal6torque17GenericStructTypeD0Ev, .-_ZN2v88internal6torque17GenericStructTypeD0Ev
	.section	.text._ZN2v88internal6torque7GenericD0Ev,"axG",@progbits,_ZN2v88internal6torque7GenericD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque7GenericD0Ev
	.type	_ZN2v88internal6torque7GenericD0Ev, @function
_ZN2v88internal6torque7GenericD0Ev:
.LFB9694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque7GenericE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	128(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L126
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L139:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L123
.L125:
	movq	%rbx, %r13
.L126:
	movq	8(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	jne	.L139
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L125
.L123:
	movq	120(%r12), %rax
	movq	112(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	112(%r12), %rdi
	leaq	160(%r12), %rax
	movq	$0, 136(%r12)
	movq	$0, 128(%r12)
	cmpq	%rax, %rdi
	je	.L127
	call	_ZdlPv@PLT
.L127:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L128
	call	_ZdlPv@PLT
.L128:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9694:
	.size	_ZN2v88internal6torque7GenericD0Ev, .-_ZN2v88internal6torque7GenericD0Ev
	.section	.text._ZN2v88internal6torque5ScopeD2Ev,"axG",@progbits,_ZN2v88internal6torque5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5ScopeD2Ev
	.type	_ZN2v88internal6torque5ScopeD2Ev, @function
_ZN2v88internal6torque5ScopeD2Ev:
.LFB6008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L145
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L160:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L141
.L144:
	movq	%r13, %r12
.L145:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L160
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L144
.L141:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L140
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6008:
	.size	_ZN2v88internal6torque5ScopeD2Ev, .-_ZN2v88internal6torque5ScopeD2Ev
	.weak	_ZN2v88internal6torque5ScopeD1Ev
	.set	_ZN2v88internal6torque5ScopeD1Ev,_ZN2v88internal6torque5ScopeD2Ev
	.section	.text._ZN2v88internal6torque9NamespaceD0Ev,"axG",@progbits,_ZN2v88internal6torque9NamespaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9NamespaceD0Ev
	.type	_ZN2v88internal6torque9NamespaceD0Ev, @function
_ZN2v88internal6torque9NamespaceD0Ev:
.LFB9084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L162
	call	_ZdlPv@PLT
.L162:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L167
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L182:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L163
.L166:
	movq	%rbx, %r13
.L167:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L164
	call	_ZdlPv@PLT
.L164:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L182
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L166
.L163:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L168
	call	_ZdlPv@PLT
.L168:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$160, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9084:
	.size	_ZN2v88internal6torque9NamespaceD0Ev, .-_ZN2v88internal6torque9NamespaceD0Ev
	.section	.text._ZN2v88internal6torque5ScopeD0Ev,"axG",@progbits,_ZN2v88internal6torque5ScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5ScopeD0Ev
	.type	_ZN2v88internal6torque5ScopeD0Ev, @function
_ZN2v88internal6torque5ScopeD0Ev:
.LFB6010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L188
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L203:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L184
.L187:
	movq	%rbx, %r13
.L188:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L185
	call	_ZdlPv@PLT
.L185:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L203
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L187
.L184:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L189
	call	_ZdlPv@PLT
.L189:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6010:
	.size	_ZN2v88internal6torque5ScopeD0Ev, .-_ZN2v88internal6torque5ScopeD0Ev
	.section	.text._ZN2v88internal6torque9NamespaceD2Ev,"axG",@progbits,_ZN2v88internal6torque9NamespaceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9NamespaceD2Ev
	.type	_ZN2v88internal6torque9NamespaceD2Ev, @function
_ZN2v88internal6torque9NamespaceD2Ev:
.LFB9082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L210
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L225:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L206
.L209:
	movq	%r13, %r12
.L210:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L207
	call	_ZdlPv@PLT
.L207:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L225
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L209
.L206:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L204
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9082:
	.size	_ZN2v88internal6torque9NamespaceD2Ev, .-_ZN2v88internal6torque9NamespaceD2Ev
	.weak	_ZN2v88internal6torque9NamespaceD1Ev
	.set	_ZN2v88internal6torque9NamespaceD1Ev,_ZN2v88internal6torque9NamespaceD2Ev
	.section	.text._ZN2v88internal6torque7BuiltinD2Ev,"axG",@progbits,_ZN2v88internal6torque7BuiltinD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque7BuiltinD2Ev
	.type	_ZN2v88internal6torque7BuiltinD2Ev, @function
_ZN2v88internal6torque7BuiltinD2Ev:
.LFB9618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	312(%rdi), %r13
	movq	304(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L227
	.p2align 4,,10
	.p2align 3
.L231:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L228
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L231
.L229:
	movq	304(%rbx), %r12
.L227:
	testq	%r12, %r12
	je	.L232
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L232:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L233
	call	_ZdlPv@PLT
.L233:
	cmpb	$0, 216(%rbx)
	je	.L234
	movq	224(%rbx), %rdi
	leaq	240(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	128(%rbx), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L237
	call	_ZdlPv@PLT
.L237:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L238
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L266:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L243
.L244:
	movq	%r13, %r12
.L238:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L266
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L244
.L243:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L267
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L231
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L267:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9618:
	.size	_ZN2v88internal6torque7BuiltinD2Ev, .-_ZN2v88internal6torque7BuiltinD2Ev
	.weak	_ZN2v88internal6torque7BuiltinD1Ev
	.set	_ZN2v88internal6torque7BuiltinD1Ev,_ZN2v88internal6torque7BuiltinD2Ev
	.section	.text._ZN2v88internal6torque11TorqueMacroD2Ev,"axG",@progbits,_ZN2v88internal6torque11TorqueMacroD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque11TorqueMacroD2Ev
	.type	_ZN2v88internal6torque11TorqueMacroD2Ev, @function
_ZN2v88internal6torque11TorqueMacroD2Ev:
.LFB6162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	312(%rdi), %r13
	movq	304(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L269
	.p2align 4,,10
	.p2align 3
.L273:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L270
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L273
.L271:
	movq	304(%rbx), %r12
.L269:
	testq	%r12, %r12
	je	.L274
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L274:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L275
	call	_ZdlPv@PLT
.L275:
	cmpb	$0, 216(%rbx)
	je	.L276
	movq	224(%rbx), %rdi
	leaq	240(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L276
	call	_ZdlPv@PLT
.L276:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L277
	call	_ZdlPv@PLT
.L277:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L278
	call	_ZdlPv@PLT
.L278:
	movq	128(%rbx), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L279
	call	_ZdlPv@PLT
.L279:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L280
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L308:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L285
.L286:
	movq	%r13, %r12
.L280:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L283
	call	_ZdlPv@PLT
.L283:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L308
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L286
.L285:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L309
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L273
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L309:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6162:
	.size	_ZN2v88internal6torque11TorqueMacroD2Ev, .-_ZN2v88internal6torque11TorqueMacroD2Ev
	.weak	_ZN2v88internal6torque11TorqueMacroD1Ev
	.set	_ZN2v88internal6torque11TorqueMacroD1Ev,_ZN2v88internal6torque11TorqueMacroD2Ev
	.section	.text._ZN2v88internal6torque9IntrinsicD2Ev,"axG",@progbits,_ZN2v88internal6torque9IntrinsicD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9IntrinsicD2Ev
	.type	_ZN2v88internal6torque9IntrinsicD2Ev, @function
_ZN2v88internal6torque9IntrinsicD2Ev:
.LFB9599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	312(%rdi), %r13
	movq	304(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L311
	.p2align 4,,10
	.p2align 3
.L315:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L312
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L315
.L313:
	movq	304(%rbx), %r12
.L311:
	testq	%r12, %r12
	je	.L316
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L316:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L317
	call	_ZdlPv@PLT
.L317:
	cmpb	$0, 216(%rbx)
	je	.L318
	movq	224(%rbx), %rdi
	leaq	240(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L318
	call	_ZdlPv@PLT
.L318:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L320
	call	_ZdlPv@PLT
.L320:
	movq	128(%rbx), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L321
	call	_ZdlPv@PLT
.L321:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L322
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L350:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L327
.L328:
	movq	%r13, %r12
.L322:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L325
	call	_ZdlPv@PLT
.L325:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L350
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L328
.L327:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L351
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L315
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L351:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9599:
	.size	_ZN2v88internal6torque9IntrinsicD2Ev, .-_ZN2v88internal6torque9IntrinsicD2Ev
	.weak	_ZN2v88internal6torque9IntrinsicD1Ev
	.set	_ZN2v88internal6torque9IntrinsicD1Ev,_ZN2v88internal6torque9IntrinsicD2Ev
	.section	.text._ZN2v88internal6torque5MacroD2Ev,"axG",@progbits,_ZN2v88internal6torque5MacroD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5MacroD2Ev
	.type	_ZN2v88internal6torque5MacroD2Ev, @function
_ZN2v88internal6torque5MacroD2Ev:
.LFB6136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	312(%rdi), %r13
	movq	304(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L353
	.p2align 4,,10
	.p2align 3
.L357:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L354
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L357
.L355:
	movq	304(%rbx), %r12
.L353:
	testq	%r12, %r12
	je	.L358
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L358:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	cmpb	$0, 216(%rbx)
	je	.L360
	movq	224(%rbx), %rdi
	leaq	240(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L361
	call	_ZdlPv@PLT
.L361:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L362
	call	_ZdlPv@PLT
.L362:
	movq	128(%rbx), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L363
	call	_ZdlPv@PLT
.L363:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L364
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L392:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L369
.L370:
	movq	%r13, %r12
.L364:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L392
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L370
.L369:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L393
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L357
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L393:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6136:
	.size	_ZN2v88internal6torque5MacroD2Ev, .-_ZN2v88internal6torque5MacroD2Ev
	.weak	_ZN2v88internal6torque5MacroD1Ev
	.set	_ZN2v88internal6torque5MacroD1Ev,_ZN2v88internal6torque5MacroD2Ev
	.section	.text._ZN2v88internal6torque15RuntimeFunctionD2Ev,"axG",@progbits,_ZN2v88internal6torque15RuntimeFunctionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque15RuntimeFunctionD2Ev
	.type	_ZN2v88internal6torque15RuntimeFunctionD2Ev, @function
_ZN2v88internal6torque15RuntimeFunctionD2Ev:
.LFB9658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	312(%rdi), %r13
	movq	304(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L395
	.p2align 4,,10
	.p2align 3
.L399:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L396
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L399
.L397:
	movq	304(%rbx), %r12
.L395:
	testq	%r12, %r12
	je	.L400
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L400:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L401
	call	_ZdlPv@PLT
.L401:
	cmpb	$0, 216(%rbx)
	je	.L402
	movq	224(%rbx), %rdi
	leaq	240(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L402
	call	_ZdlPv@PLT
.L402:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L403
	call	_ZdlPv@PLT
.L403:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L404
	call	_ZdlPv@PLT
.L404:
	movq	128(%rbx), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L406
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L434:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L411
.L412:
	movq	%r13, %r12
.L406:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L409
	call	_ZdlPv@PLT
.L409:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L434
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L412
.L411:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L435
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L399
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L435:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9658:
	.size	_ZN2v88internal6torque15RuntimeFunctionD2Ev, .-_ZN2v88internal6torque15RuntimeFunctionD2Ev
	.weak	_ZN2v88internal6torque15RuntimeFunctionD1Ev
	.set	_ZN2v88internal6torque15RuntimeFunctionD1Ev,_ZN2v88internal6torque15RuntimeFunctionD2Ev
	.section	.text._ZN2v88internal6torque6MethodD2Ev,"axG",@progbits,_ZN2v88internal6torque6MethodD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque6MethodD2Ev
	.type	_ZN2v88internal6torque6MethodD2Ev, @function
_ZN2v88internal6torque6MethodD2Ev:
.LFB9581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	312(%rdi), %r13
	movq	304(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L437
	.p2align 4,,10
	.p2align 3
.L441:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L438
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L441
.L439:
	movq	304(%rbx), %r12
.L437:
	testq	%r12, %r12
	je	.L442
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L442:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L443
	call	_ZdlPv@PLT
.L443:
	cmpb	$0, 216(%rbx)
	je	.L444
	movq	224(%rbx), %rdi
	leaq	240(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L444
	call	_ZdlPv@PLT
.L444:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	movq	128(%rbx), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L448
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L476:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L453
.L454:
	movq	%r13, %r12
.L448:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L476
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L454
.L453:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L477
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L441
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L477:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9581:
	.size	_ZN2v88internal6torque6MethodD2Ev, .-_ZN2v88internal6torque6MethodD2Ev
	.weak	_ZN2v88internal6torque6MethodD1Ev
	.set	_ZN2v88internal6torque6MethodD1Ev,_ZN2v88internal6torque6MethodD2Ev
	.section	.text._ZN2v88internal6torque8CallableD0Ev,"axG",@progbits,_ZN2v88internal6torque8CallableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque8CallableD0Ev
	.type	_ZN2v88internal6torque8CallableD0Ev, @function
_ZN2v88internal6torque8CallableD0Ev:
.LFB6125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	312(%rdi), %rbx
	movq	304(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L479
	.p2align 4,,10
	.p2align 3
.L483:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L480
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L483
.L481:
	movq	304(%r12), %r13
.L479:
	testq	%r13, %r13
	je	.L484
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L484:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L485
	call	_ZdlPv@PLT
.L485:
	cmpb	$0, 216(%r12)
	je	.L486
	movq	224(%r12), %rdi
	leaq	240(%r12), %rax
	cmpq	%rax, %rdi
	je	.L486
	call	_ZdlPv@PLT
.L486:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L487
	call	_ZdlPv@PLT
.L487:
	movq	160(%r12), %rdi
	leaq	176(%r12), %rax
	cmpq	%rax, %rdi
	je	.L488
	call	_ZdlPv@PLT
.L488:
	movq	128(%r12), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L489
	call	_ZdlPv@PLT
.L489:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L490
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L518:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L495
.L496:
	movq	%rbx, %r13
.L490:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L493
	call	_ZdlPv@PLT
.L493:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L518
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L496
.L495:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L491
	call	_ZdlPv@PLT
.L491:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$360, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L483
	jmp	.L481
	.cfi_endproc
.LFE6125:
	.size	_ZN2v88internal6torque8CallableD0Ev, .-_ZN2v88internal6torque8CallableD0Ev
	.section	.text._ZN2v88internal6torque11ExternMacroD0Ev,"axG",@progbits,_ZN2v88internal6torque11ExternMacroD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque11ExternMacroD0Ev
	.type	_ZN2v88internal6torque11ExternMacroD0Ev, @function
_ZN2v88internal6torque11ExternMacroD0Ev:
.LFB9563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque11ExternMacroE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	368(%rdi), %rdi
	leaq	384(%r12), %rax
	cmpq	%rax, %rdi
	je	.L520
	call	_ZdlPv@PLT
.L520:
	movq	312(%r12), %rbx
	movq	304(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rax, (%r12)
	cmpq	%r13, %rbx
	je	.L521
	.p2align 4,,10
	.p2align 3
.L525:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L522
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L525
.L523:
	movq	304(%r12), %r13
.L521:
	testq	%r13, %r13
	je	.L526
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L526:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L527
	call	_ZdlPv@PLT
.L527:
	cmpb	$0, 216(%r12)
	je	.L528
	movq	224(%r12), %rdi
	leaq	240(%r12), %rax
	cmpq	%rax, %rdi
	je	.L528
	call	_ZdlPv@PLT
.L528:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L529
	call	_ZdlPv@PLT
.L529:
	movq	160(%r12), %rdi
	leaq	176(%r12), %rax
	cmpq	%rax, %rdi
	je	.L530
	call	_ZdlPv@PLT
.L530:
	movq	128(%r12), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L531
	call	_ZdlPv@PLT
.L531:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L532
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L560:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L537
.L538:
	movq	%rbx, %r13
.L532:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L535
	call	_ZdlPv@PLT
.L535:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L560
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L538
.L537:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L533
	call	_ZdlPv@PLT
.L533:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$400, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L525
	jmp	.L523
	.cfi_endproc
.LFE9563:
	.size	_ZN2v88internal6torque11ExternMacroD0Ev, .-_ZN2v88internal6torque11ExternMacroD0Ev
	.section	.text._ZN2v88internal6torque11TorqueMacroD0Ev,"axG",@progbits,_ZN2v88internal6torque11TorqueMacroD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque11TorqueMacroD0Ev
	.type	_ZN2v88internal6torque11TorqueMacroD0Ev, @function
_ZN2v88internal6torque11TorqueMacroD0Ev:
.LFB6164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	312(%rdi), %rbx
	movq	304(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L562
	.p2align 4,,10
	.p2align 3
.L566:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L563
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L566
.L564:
	movq	304(%r12), %r13
.L562:
	testq	%r13, %r13
	je	.L567
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L567:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L568
	call	_ZdlPv@PLT
.L568:
	cmpb	$0, 216(%r12)
	je	.L569
	movq	224(%r12), %rdi
	leaq	240(%r12), %rax
	cmpq	%rax, %rdi
	je	.L569
	call	_ZdlPv@PLT
.L569:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L570
	call	_ZdlPv@PLT
.L570:
	movq	160(%r12), %rdi
	leaq	176(%r12), %rax
	cmpq	%rax, %rdi
	je	.L571
	call	_ZdlPv@PLT
.L571:
	movq	128(%r12), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L572
	call	_ZdlPv@PLT
.L572:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L573
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L601:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L578
.L579:
	movq	%rbx, %r13
.L573:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L576
	call	_ZdlPv@PLT
.L576:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L601
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L579
.L578:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L574
	call	_ZdlPv@PLT
.L574:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$368, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L563:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L566
	jmp	.L564
	.cfi_endproc
.LFE6164:
	.size	_ZN2v88internal6torque11TorqueMacroD0Ev, .-_ZN2v88internal6torque11TorqueMacroD0Ev
	.section	.text._ZN2v88internal6torque9IntrinsicD0Ev,"axG",@progbits,_ZN2v88internal6torque9IntrinsicD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9IntrinsicD0Ev
	.type	_ZN2v88internal6torque9IntrinsicD0Ev, @function
_ZN2v88internal6torque9IntrinsicD0Ev:
.LFB9601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	312(%rdi), %rbx
	movq	304(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L603
	.p2align 4,,10
	.p2align 3
.L607:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L604
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L607
.L605:
	movq	304(%r12), %r13
.L603:
	testq	%r13, %r13
	je	.L608
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L608:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L609
	call	_ZdlPv@PLT
.L609:
	cmpb	$0, 216(%r12)
	je	.L610
	movq	224(%r12), %rdi
	leaq	240(%r12), %rax
	cmpq	%rax, %rdi
	je	.L610
	call	_ZdlPv@PLT
.L610:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L611
	call	_ZdlPv@PLT
.L611:
	movq	160(%r12), %rdi
	leaq	176(%r12), %rax
	cmpq	%rax, %rdi
	je	.L612
	call	_ZdlPv@PLT
.L612:
	movq	128(%r12), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L613
	call	_ZdlPv@PLT
.L613:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L614
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L642:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L619
.L620:
	movq	%rbx, %r13
.L614:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L617
	call	_ZdlPv@PLT
.L617:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L642
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L620
.L619:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L615
	call	_ZdlPv@PLT
.L615:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$360, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L607
	jmp	.L605
	.cfi_endproc
.LFE9601:
	.size	_ZN2v88internal6torque9IntrinsicD0Ev, .-_ZN2v88internal6torque9IntrinsicD0Ev
	.section	.text._ZN2v88internal6torque7BuiltinD0Ev,"axG",@progbits,_ZN2v88internal6torque7BuiltinD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque7BuiltinD0Ev
	.type	_ZN2v88internal6torque7BuiltinD0Ev, @function
_ZN2v88internal6torque7BuiltinD0Ev:
.LFB9620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	312(%rdi), %rbx
	movq	304(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L644
	.p2align 4,,10
	.p2align 3
.L648:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L645
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L648
.L646:
	movq	304(%r12), %r13
.L644:
	testq	%r13, %r13
	je	.L649
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L649:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L650
	call	_ZdlPv@PLT
.L650:
	cmpb	$0, 216(%r12)
	je	.L651
	movq	224(%r12), %rdi
	leaq	240(%r12), %rax
	cmpq	%rax, %rdi
	je	.L651
	call	_ZdlPv@PLT
.L651:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L652
	call	_ZdlPv@PLT
.L652:
	movq	160(%r12), %rdi
	leaq	176(%r12), %rax
	cmpq	%rax, %rdi
	je	.L653
	call	_ZdlPv@PLT
.L653:
	movq	128(%r12), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L655
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L683:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L660
.L661:
	movq	%rbx, %r13
.L655:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L658
	call	_ZdlPv@PLT
.L658:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L683
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L661
.L660:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L656
	call	_ZdlPv@PLT
.L656:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$368, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L648
	jmp	.L646
	.cfi_endproc
.LFE9620:
	.size	_ZN2v88internal6torque7BuiltinD0Ev, .-_ZN2v88internal6torque7BuiltinD0Ev
	.section	.text._ZN2v88internal6torque6MethodD0Ev,"axG",@progbits,_ZN2v88internal6torque6MethodD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque6MethodD0Ev
	.type	_ZN2v88internal6torque6MethodD0Ev, @function
_ZN2v88internal6torque6MethodD0Ev:
.LFB9583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	312(%rdi), %rbx
	movq	304(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L685
	.p2align 4,,10
	.p2align 3
.L689:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L686
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L689
.L687:
	movq	304(%r12), %r13
.L685:
	testq	%r13, %r13
	je	.L690
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L690:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L691
	call	_ZdlPv@PLT
.L691:
	cmpb	$0, 216(%r12)
	je	.L692
	movq	224(%r12), %rdi
	leaq	240(%r12), %rax
	cmpq	%rax, %rdi
	je	.L692
	call	_ZdlPv@PLT
.L692:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L693
	call	_ZdlPv@PLT
.L693:
	movq	160(%r12), %rdi
	leaq	176(%r12), %rax
	cmpq	%rax, %rdi
	je	.L694
	call	_ZdlPv@PLT
.L694:
	movq	128(%r12), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L695
	call	_ZdlPv@PLT
.L695:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L696
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L724:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L701
.L702:
	movq	%rbx, %r13
.L696:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L699
	call	_ZdlPv@PLT
.L699:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L724
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L702
.L701:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L697
	call	_ZdlPv@PLT
.L697:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$376, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L689
	jmp	.L687
	.cfi_endproc
.LFE9583:
	.size	_ZN2v88internal6torque6MethodD0Ev, .-_ZN2v88internal6torque6MethodD0Ev
	.section	.text._ZN2v88internal6torque15RuntimeFunctionD0Ev,"axG",@progbits,_ZN2v88internal6torque15RuntimeFunctionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque15RuntimeFunctionD0Ev
	.type	_ZN2v88internal6torque15RuntimeFunctionD0Ev, @function
_ZN2v88internal6torque15RuntimeFunctionD0Ev:
.LFB9660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	312(%rdi), %rbx
	movq	304(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L726
	.p2align 4,,10
	.p2align 3
.L730:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L727
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L730
.L728:
	movq	304(%r12), %r13
.L726:
	testq	%r13, %r13
	je	.L731
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L731:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L732
	call	_ZdlPv@PLT
.L732:
	cmpb	$0, 216(%r12)
	je	.L733
	movq	224(%r12), %rdi
	leaq	240(%r12), %rax
	cmpq	%rax, %rdi
	je	.L733
	call	_ZdlPv@PLT
.L733:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L734
	call	_ZdlPv@PLT
.L734:
	movq	160(%r12), %rdi
	leaq	176(%r12), %rax
	cmpq	%rax, %rdi
	je	.L735
	call	_ZdlPv@PLT
.L735:
	movq	128(%r12), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L736
	call	_ZdlPv@PLT
.L736:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L737
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L765:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L742
.L743:
	movq	%rbx, %r13
.L737:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L740
	call	_ZdlPv@PLT
.L740:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L765
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L743
.L742:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L738
	call	_ZdlPv@PLT
.L738:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$360, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L727:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L730
	jmp	.L728
	.cfi_endproc
.LFE9660:
	.size	_ZN2v88internal6torque15RuntimeFunctionD0Ev, .-_ZN2v88internal6torque15RuntimeFunctionD0Ev
	.section	.text._ZN2v88internal6torque5MacroD0Ev,"axG",@progbits,_ZN2v88internal6torque5MacroD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5MacroD0Ev
	.type	_ZN2v88internal6torque5MacroD0Ev, @function
_ZN2v88internal6torque5MacroD0Ev:
.LFB6138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	312(%rdi), %rbx
	movq	304(%rdi), %r13
	movq	%rax, (%rdi)
	cmpq	%r13, %rbx
	je	.L767
	.p2align 4,,10
	.p2align 3
.L771:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L768
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L771
.L769:
	movq	304(%r12), %r13
.L767:
	testq	%r13, %r13
	je	.L772
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L772:
	movq	256(%r12), %rdi
	testq	%rdi, %rdi
	je	.L773
	call	_ZdlPv@PLT
.L773:
	cmpb	$0, 216(%r12)
	je	.L774
	movq	224(%r12), %rdi
	leaq	240(%r12), %rax
	cmpq	%rax, %rdi
	je	.L774
	call	_ZdlPv@PLT
.L774:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L775
	call	_ZdlPv@PLT
.L775:
	movq	160(%r12), %rdi
	leaq	176(%r12), %rax
	cmpq	%rax, %rdi
	je	.L776
	call	_ZdlPv@PLT
.L776:
	movq	128(%r12), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L777
	call	_ZdlPv@PLT
.L777:
	movq	88(%r12), %r13
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	jne	.L778
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L806:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L783
.L784:
	movq	%rbx, %r13
.L778:
	movq	40(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	je	.L781
	call	_ZdlPv@PLT
.L781:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L806
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L784
.L783:
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	leaq	120(%r12), %rax
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rax, %rdi
	je	.L779
	call	_ZdlPv@PLT
.L779:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$368, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L768:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L771
	jmp	.L769
	.cfi_endproc
.LFE6138:
	.size	_ZN2v88internal6torque5MacroD0Ev, .-_ZN2v88internal6torque5MacroD0Ev
	.section	.text._ZN2v88internal6torque11ExternMacroD2Ev,"axG",@progbits,_ZN2v88internal6torque11ExternMacroD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque11ExternMacroD2Ev
	.type	_ZN2v88internal6torque11ExternMacroD2Ev, @function
_ZN2v88internal6torque11ExternMacroD2Ev:
.LFB9561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque11ExternMacroE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	368(%rdi), %rdi
	leaq	384(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L808
	call	_ZdlPv@PLT
.L808:
	movq	312(%rbx), %r13
	movq	304(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rax, (%rbx)
	cmpq	%r12, %r13
	je	.L809
	.p2align 4,,10
	.p2align 3
.L813:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L810
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L813
.L811:
	movq	304(%rbx), %r12
.L809:
	testq	%r12, %r12
	je	.L814
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L814:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L815
	call	_ZdlPv@PLT
.L815:
	cmpb	$0, 216(%rbx)
	je	.L816
	movq	224(%rbx), %rdi
	leaq	240(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L816
	call	_ZdlPv@PLT
.L816:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L817
	call	_ZdlPv@PLT
.L817:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L818
	call	_ZdlPv@PLT
.L818:
	movq	128(%rbx), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L819
	call	_ZdlPv@PLT
.L819:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L820
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L848:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L825
.L826:
	movq	%r13, %r12
.L820:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L823
	call	_ZdlPv@PLT
.L823:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L848
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L826
.L825:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L849
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L810:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L813
	jmp	.L811
	.p2align 4,,10
	.p2align 3
.L849:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9561:
	.size	_ZN2v88internal6torque11ExternMacroD2Ev, .-_ZN2v88internal6torque11ExternMacroD2Ev
	.weak	_ZN2v88internal6torque11ExternMacroD1Ev
	.set	_ZN2v88internal6torque11ExternMacroD1Ev,_ZN2v88internal6torque11ExternMacroD2Ev
	.section	.text._ZN2v88internal6torque8CallableD2Ev,"axG",@progbits,_ZN2v88internal6torque8CallableD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque8CallableD2Ev
	.type	_ZN2v88internal6torque8CallableD2Ev, @function
_ZN2v88internal6torque8CallableD2Ev:
.LFB6123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	312(%rdi), %r13
	movq	304(%rdi), %r12
	movq	%rax, (%rdi)
	cmpq	%r12, %r13
	je	.L851
	.p2align 4,,10
	.p2align 3
.L855:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L852
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L855
.L853:
	movq	304(%rbx), %r12
.L851:
	testq	%r12, %r12
	je	.L856
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L856:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L857
	call	_ZdlPv@PLT
.L857:
	cmpb	$0, 216(%rbx)
	je	.L858
	movq	224(%rbx), %rdi
	leaq	240(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L858
	call	_ZdlPv@PLT
.L858:
	movq	192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L859
	call	_ZdlPv@PLT
.L859:
	movq	160(%rbx), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L860
	call	_ZdlPv@PLT
.L860:
	movq	128(%rbx), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L861
	call	_ZdlPv@PLT
.L861:
	movq	88(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%r12, %r12
	jne	.L862
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L890:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L867
.L868:
	movq	%r13, %r12
.L862:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L865
	call	_ZdlPv@PLT
.L865:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L890
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L868
.L867:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	addq	$120, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L891
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L855
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L891:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6123:
	.size	_ZN2v88internal6torque8CallableD2Ev, .-_ZN2v88internal6torque8CallableD2Ev
	.weak	_ZN2v88internal6torque8CallableD1Ev
	.set	_ZN2v88internal6torque8CallableD1Ev,_ZN2v88internal6torque8CallableD2Ev
	.section	.rodata._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0.str1.1,"aMS",@progbits,1
.LC17:
	.string	"%lu"
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB14302:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -40
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L893
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L893:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L895
.L915:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L915
.L895:
	subq	$48, %rsp
	orq	$0, 40(%rsp)
	leaq	.LC17(%rip), %r8
	movl	$32, %ecx
	movl	$32, %esi
	leaq	15(%rsp), %rdx
	leaq	16(%rbp), %rax
	movl	$32, -240(%rbp)
	movq	%rdx, %rbx
	movl	$1, %edx
	movq	%rax, -232(%rbp)
	leaq	-240(%rbp), %r9
	andq	$-16, %rbx
	leaq	-208(%rbp), %rax
	movl	$48, -236(%rbp)
	movq	%rbx, %rdi
	movq	%rax, -224(%rbp)
	call	__vsnprintf_chk@PLT
	movslq	%eax, %rdx
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	movq	%rdx, %r13
	movq	%rdx, -248(%rbp)
	cmpl	$15, %edx
	jg	.L916
	cmpq	$1, %rdx
	jne	.L898
	movzbl	(%rbx), %ecx
	movb	%cl, 16(%r12)
.L899:
	movq	%rdx, 8(%r12)
	movb	$0, (%rax,%rdx)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L917
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L916:
	.cfi_restore_state
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-248(%rbp), %rcx
	movq	%rax, (%r12)
	movq	%rcx, 16(%r12)
.L898:
	cmpl	$8, %r13d
	jnb	.L900
	testb	$4, %r13b
	jne	.L918
	testl	%r13d, %r13d
	je	.L901
	movzbl	(%rbx), %edx
	movb	%dl, (%rax)
	testb	$2, %r13b
	jne	.L919
.L901:
	movq	-248(%rbp), %rdx
	movq	(%r12), %rax
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L900:
	movq	(%rbx), %rdx
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%r13d, %edx
	movq	-8(%rbx,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	subq	%rax, %rbx
	addl	%r13d, %eax
	andl	$-8, %eax
	movq	%rbx, %rdx
	cmpl	$8, %eax
	jb	.L901
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L904:
	movl	%ecx, %esi
	addl	$8, %ecx
	movq	(%rdx,%rsi), %r8
	movq	%r8, (%rdi,%rsi)
	cmpl	%eax, %ecx
	jb	.L904
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L918:
	movl	(%rbx), %edx
	movl	%r13d, %r13d
	movl	%edx, (%rax)
	movl	-4(%rbx,%r13), %edx
	movl	%edx, -4(%rax,%r13)
	jmp	.L901
.L919:
	movl	%r13d, %r13d
	movzwl	-2(%rbx,%r13), %edx
	movw	%dx, -2(%rax,%r13)
	jmp	.L901
.L917:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14302:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque14MessageBuilderD2Ev
	.type	_ZN2v88internal6torque14MessageBuilderD2Ev, @function
_ZN2v88internal6torque14MessageBuilderD2Ev:
.LFB4670:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA4670
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$16, %rbx
	subq	$8, %rsp
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-16(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.L920
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4670:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v88internal6torque14MessageBuilderD2Ev,"aG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
.LLSDA4670:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4670-.LLSDACSB4670
.LLSDACSB4670:
.LLSDACSE4670:
	.section	.text._ZN2v88internal6torque14MessageBuilderD2Ev,"axG",@progbits,_ZN2v88internal6torque14MessageBuilderD5Ev,comdat
	.size	_ZN2v88internal6torque14MessageBuilderD2Ev, .-_ZN2v88internal6torque14MessageBuilderD2Ev
	.weak	_ZN2v88internal6torque14MessageBuilderD1Ev
	.set	_ZN2v88internal6torque14MessageBuilderD1Ev,_ZN2v88internal6torque14MessageBuilderD2Ev
	.section	.text._ZN2v88internal6torque13QualifiedNameD2Ev,"axG",@progbits,_ZN2v88internal6torque13QualifiedNameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13QualifiedNameD2Ev
	.type	_ZN2v88internal6torque13QualifiedNameD2Ev, @function
_ZN2v88internal6torque13QualifiedNameD2Ev:
.LFB5915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	40(%rbx), %rax
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L924
	call	_ZdlPv@PLT
.L924:
	movq	8(%rbx), %r13
	movq	(%rbx), %r12
	cmpq	%r12, %r13
	je	.L925
	.p2align 4,,10
	.p2align 3
.L929:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L926
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L929
.L927:
	movq	(%rbx), %r12
.L925:
	testq	%r12, %r12
	je	.L923
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L926:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L929
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L923:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5915:
	.size	_ZN2v88internal6torque13QualifiedNameD2Ev, .-_ZN2v88internal6torque13QualifiedNameD2Ev
	.weak	_ZN2v88internal6torque13QualifiedNameD1Ev
	.set	_ZN2v88internal6torque13QualifiedNameD1Ev,_ZN2v88internal6torque13QualifiedNameD2Ev
	.section	.text._ZN2v88internal6torque9SignatureD2Ev,"axG",@progbits,_ZN2v88internal6torque9SignatureD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9SignatureD2Ev
	.type	_ZN2v88internal6torque9SignatureD2Ev, @function
_ZN2v88internal6torque9SignatureD2Ev:
.LFB6102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	120(%rdi), %r13
	movq	112(%rdi), %r12
	cmpq	%r12, %r13
	je	.L933
	.p2align 4,,10
	.p2align 3
.L937:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L934
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r13, %r12
	jne	.L937
.L935:
	movq	112(%rbx), %r12
.L933:
	testq	%r12, %r12
	je	.L938
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L938:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L939
	call	_ZdlPv@PLT
.L939:
	cmpb	$0, 24(%rbx)
	je	.L940
	movq	32(%rbx), %rdi
	leaq	48(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L940
	call	_ZdlPv@PLT
.L940:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L932
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L934:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L937
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L932:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6102:
	.size	_ZN2v88internal6torque9SignatureD2Ev, .-_ZN2v88internal6torque9SignatureD2Ev
	.weak	_ZN2v88internal6torque9SignatureD1Ev
	.set	_ZN2v88internal6torque9SignatureD1Ev,_ZN2v88internal6torque9SignatureD2Ev
	.section	.text._ZN2v88internal6torque9SignatureC2ERKS2_,"axG",@progbits,_ZN2v88internal6torque9SignatureC5ERKS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque9SignatureC2ERKS2_
	.type	_ZN2v88internal6torque9SignatureC2ERKS2_, @function
_ZN2v88internal6torque9SignatureC2ERKS2_:
.LFB6120:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6120
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %r13
	subq	(%rsi), %r13
	movups	%xmm0, (%rdi)
	movq	%r13, %rax
	movq	$0, 16(%rdi)
	sarq	$3, %rax
	je	.L1008
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L1009
	movq	%r13, %rdi
.LEHB0:
	call	_Znwm@PLT
.LEHE0:
	movq	%rax, %rcx
.L951:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	movq	%rax, %r13
	subq	%rsi, %r13
	cmpq	%rsi, %rax
	je	.L954
	movq	%rcx, %rdi
	movq	%r13, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L954:
	addq	%r13, %rcx
	movb	$0, 24(%rbx)
	movq	%rcx, 8(%rbx)
	movb	$0, 32(%rbx)
	cmpb	$0, 24(%r12)
	jne	.L1010
.L955:
	movq	72(%r12), %rax
	subq	64(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 80(%rbx)
	movq	%rax, %r13
	sarq	$3, %rax
	movups	%xmm0, 64(%rbx)
	je	.L1011
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L1012
	movq	%r13, %rdi
.LEHB1:
	call	_Znwm@PLT
.LEHE1:
	movq	%rax, %rcx
.L957:
	movq	%rcx, %xmm0
	leaq	(%rcx,%r13), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movq	72(%r12), %rax
	movq	64(%r12), %rsi
	movq	%rax, %r13
	subq	%rsi, %r13
	cmpq	%rsi, %rax
	je	.L963
	movq	%rcx, %rdi
	movq	%r13, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L963:
	addq	%r13, %rcx
	movq	%rcx, 72(%rbx)
	movzbl	88(%r12), %eax
	movb	%al, 88(%rbx)
	movq	96(%r12), %rax
	movq	%rax, 96(%rbx)
	movq	104(%r12), %rax
	movq	%rax, 104(%rbx)
	movq	120(%r12), %r13
	subq	112(%r12), %r13
	movq	$0, 112(%rbx)
	movq	%r13, %rax
	movq	$0, 120(%rbx)
	movq	$0, 128(%rbx)
	sarq	$5, %rax
	je	.L1013
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1014
	movq	%r13, %rdi
.LEHB2:
	call	_Znwm@PLT
.LEHE2:
	movq	%rax, -64(%rbp)
.L965:
	movq	-64(%rbp), %r14
	movq	%r14, %xmm0
	addq	%r14, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movq	120(%r12), %rax
	movq	112(%r12), %r13
	movq	%rax, -56(%rbp)
	cmpq	%r13, %rax
	jne	.L974
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L968:
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L1015
	movq	%r15, %rdi
.LEHB3:
	call	_Znwm@PLT
.LEHE3:
	movq	%rax, %rcx
.L969:
	movq	%rcx, %xmm0
	addq	%rcx, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 24(%r14)
	movups	%xmm0, 8(%r14)
	movq	16(%r13), %rax
	movq	8(%r13), %rsi
	movq	%rax, %r15
	subq	%rsi, %r15
	cmpq	%rsi, %rax
	je	.L1006
	movq	%rcx, %rdi
	movq	%r15, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1006:
	addq	%r15, %rcx
	addq	$32, %r14
	addq	$32, %r13
	movq	%rcx, -16(%r14)
	cmpq	%r13, -56(%rbp)
	je	.L967
.L974:
	movq	0(%r13), %rax
	movq	%rax, (%r14)
	movq	16(%r13), %r15
	subq	8(%r13), %r15
	movq	$0, 8(%r14)
	movq	%r15, %rax
	movq	$0, 16(%r14)
	sarq	$3, %rax
	movq	$0, 24(%r14)
	jne	.L968
	xorl	%ecx, %ecx
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L967:
	movq	%r14, 120(%rbx)
	movzbl	136(%r12), %eax
	movb	%al, 136(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	.cfi_restore_state
	leaq	48(%rbx), %r13
	leaq	32(%rbx), %rdi
	movq	%r13, 32(%rbx)
	movq	32(%r12), %rsi
	movq	40(%r12), %rdx
	addq	%rsi, %rdx
.LEHB4:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE4:
	movb	$1, 24(%rbx)
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L1008:
	xorl	%ecx, %ecx
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L1011:
	xorl	%ecx, %ecx
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L1013:
	movq	$0, -64(%rbp)
	jmp	.L965
.L1015:
.LEHB5:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE5:
.L1009:
.LEHB6:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE6:
.L1012:
.LEHB7:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE7:
.L1014:
.LEHB8:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE8:
.L989:
	endbr64
	movq	%rax, %r12
	jmp	.L958
.L987:
	endbr64
	movq	%rax, %r12
	jmp	.L983
.L958:
	cmpb	$0, 24(%rbx)
	je	.L960
	movq	32(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L960
.L1007:
	call	_ZdlPv@PLT
.L960:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L985
	call	_ZdlPv@PLT
.L985:
	movq	%r12, %rdi
.LEHB9:
	call	_Unwind_Resume@PLT
.LEHE9:
.L979:
	call	__cxa_end_catch@PLT
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L981
	call	_ZdlPv@PLT
.L981:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L983
	call	_ZdlPv@PLT
.L983:
	cmpb	$0, 24(%rbx)
	je	.L960
	movq	32(%rbx), %rdi
	leaq	48(%rbx), %rax
	cmpq	%rax, %rdi
	jne	.L1007
	jmp	.L960
.L988:
	endbr64
	movq	%rax, %r12
	jmp	.L981
.L991:
	endbr64
	movq	%rax, %rdi
.L975:
	call	__cxa_begin_catch@PLT
.L978:
	cmpq	-64(%rbp), %r14
	jne	.L1016
.LEHB10:
	call	__cxa_rethrow@PLT
.LEHE10:
.L1016:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L977
	call	_ZdlPv@PLT
.L977:
	addq	$32, -64(%rbp)
	jmp	.L978
.L990:
	endbr64
	movq	%rax, %r12
	jmp	.L979
	.cfi_endproc
.LFE6120:
	.section	.gcc_except_table._ZN2v88internal6torque9SignatureC2ERKS2_,"aG",@progbits,_ZN2v88internal6torque9SignatureC5ERKS2_,comdat
	.align 4
.LLSDA6120:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6120-.LLSDATTD6120
.LLSDATTD6120:
	.byte	0x1
	.uleb128 .LLSDACSE6120-.LLSDACSB6120
.LLSDACSB6120:
	.uleb128 .LEHB0-.LFB6120
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB6120
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L987-.LFB6120
	.uleb128 0
	.uleb128 .LEHB2-.LFB6120
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L988-.LFB6120
	.uleb128 0
	.uleb128 .LEHB3-.LFB6120
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L991-.LFB6120
	.uleb128 0x1
	.uleb128 .LEHB4-.LFB6120
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L989-.LFB6120
	.uleb128 0
	.uleb128 .LEHB5-.LFB6120
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L991-.LFB6120
	.uleb128 0x1
	.uleb128 .LEHB6-.LFB6120
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB6120
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L987-.LFB6120
	.uleb128 0
	.uleb128 .LEHB8-.LFB6120
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L988-.LFB6120
	.uleb128 0
	.uleb128 .LEHB9-.LFB6120
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB10-.LFB6120
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L990-.LFB6120
	.uleb128 0
.LLSDACSE6120:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6120:
	.section	.text._ZN2v88internal6torque9SignatureC2ERKS2_,"axG",@progbits,_ZN2v88internal6torque9SignatureC5ERKS2_,comdat
	.size	_ZN2v88internal6torque9SignatureC2ERKS2_, .-_ZN2v88internal6torque9SignatureC2ERKS2_
	.weak	_ZN2v88internal6torque9SignatureC1ERKS2_
	.set	_ZN2v88internal6torque9SignatureC1ERKS2_,_ZN2v88internal6torque9SignatureC2ERKS2_
	.section	.text._ZN2v88internal6torque12Declarations31FindSomeInternalBuiltinWithTypeEPKNS1_18BuiltinPointerTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations31FindSomeInternalBuiltinWithTypeEPKNS1_18BuiltinPointerTypeE
	.type	_ZN2v88internal6torque12Declarations31FindSomeInternalBuiltinWithTypeEPKNS1_18BuiltinPointerTypeE, @function
_ZN2v88internal6torque12Declarations31FindSomeInternalBuiltinWithTypeEPKNS1_18BuiltinPointerTypeE:
.LFB6525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	112(%rax), %rbx
	movq	120(%rax), %r13
	cmpq	%r13, %rbx
	je	.L1021
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1019
	cmpl	$4, 8(%r12)
	jne	.L1019
	cmpb	$0, 344(%r12)
	je	.L1019
	movl	360(%r12), %eax
	testl	%eax, %eax
	jne	.L1019
	movq	96(%r14), %rax
	cmpq	%rax, 296(%r12)
	je	.L1033
	.p2align 4,,10
	.p2align 3
.L1019:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L1020
.L1021:
	xorl	%r12d, %r12d
.L1017:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1033:
	.cfi_restore_state
	movq	256(%r12), %rdi
	movq	264(%r12), %rdx
	movq	72(%r14), %rsi
	movq	80(%r14), %rax
	subq	%rdi, %rdx
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	jne	.L1019
	testq	%rdx, %rdx
	je	.L1017
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L1017
	jmp	.L1019
	.cfi_endproc
.LFE6525:
	.size	_ZN2v88internal6torque12Declarations31FindSomeInternalBuiltinWithTypeEPKNS1_18BuiltinPointerTypeE, .-_ZN2v88internal6torque12Declarations31FindSomeInternalBuiltinWithTypeEPKNS1_18BuiltinPointerTypeE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE,"ax",@progbits
	.align 2
.LCOLDB18:
	.section	.text._ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE,"ax",@progbits
.LHOTB18:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.type	_ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE, @function
_ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE:
.LFB6609:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6609
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	movq	(%rsi), %r13
	movq	8(%rsi), %r12
	movq	%rax, -192(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L1035
	testq	%r13, %r13
	je	.L1071
.L1035:
	movq	%r12, -168(%rbp)
	cmpq	$15, %r12
	ja	.L1072
	cmpq	$1, %r12
	jne	.L1038
	movzbl	0(%r13), %eax
	movb	%al, 16(%r14)
	movq	-192(%rbp), %rax
.L1039:
	movq	%r12, 8(%r14)
	leaq	-160(%rbp), %r15
	leaq	-128(%rbp), %r13
	movb	$0, (%rax,%r12)
	movq	8(%rbx), %rax
	movq	(%rbx), %rbx
	movq	%rax, -184(%rbp)
	cmpq	%rbx, %rax
	je	.L1034
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	movq	(%rsi), %rax
.LEHB11:
	call	*24(%rax)
.LEHE11:
	movq	-152(%rbp), %r8
	movl	$32, %edx
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC17(%rip), %rcx
.LEHB12:
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE12:
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r13, %rdi
.LEHB13:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE13:
	leaq	-80(%rbp), %r12
	leaq	16(%rax), %rdx
	movq	%r12, -96(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1073
	movq	%rcx, -96(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L1042:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%r14, %rdi
	movq	%rcx, -88(%rbp)
	movq	%rdx, (%rax)
	movq	-96(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	-88(%rbp), %rdx
.LEHB14:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE14:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1043
	call	_ZdlPv@PLT
.L1043:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1044
	call	_ZdlPv@PLT
.L1044:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1045
	call	_ZdlPv@PLT
	addq	$8, %rbx
	cmpq	%rbx, -184(%rbp)
	jne	.L1048
.L1034:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1074
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1038:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L1075
	movq	-192(%rbp), %rax
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1045:
	addq	$8, %rbx
	cmpq	%rbx, -184(%rbp)
	jne	.L1048
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1073:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	%r14, %rdi
	leaq	-168(%rbp), %rsi
	xorl	%edx, %edx
.LEHB15:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, 16(%r14)
.L1037:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-168(%rbp), %r12
	movq	(%r14), %rax
	jmp	.L1039
.L1074:
	call	__stack_chk_fail@PLT
.L1075:
	movq	-192(%rbp), %rdi
	jmp	.L1037
.L1071:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE15:
.L1059:
	endbr64
	movq	%rax, %r12
	jmp	.L1055
.L1060:
	endbr64
	movq	%rax, %r12
	jmp	.L1053
.L1061:
	endbr64
	movq	%rax, %r12
	jmp	.L1051
.L1062:
	endbr64
	movq	%rax, %rbx
	jmp	.L1049
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE,"a",@progbits
.LLSDA6609:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6609-.LLSDACSB6609
.LLSDACSB6609:
	.uleb128 .LEHB11-.LFB6609
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L1059-.LFB6609
	.uleb128 0
	.uleb128 .LEHB12-.LFB6609
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L1060-.LFB6609
	.uleb128 0
	.uleb128 .LEHB13-.LFB6609
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L1061-.LFB6609
	.uleb128 0
	.uleb128 .LEHB14-.LFB6609
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L1062-.LFB6609
	.uleb128 0
	.uleb128 .LEHB15-.LFB6609
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
.LLSDACSE6609:
	.section	.text._ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6609
	.type	_ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE.cold, @function
_ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE.cold:
.LFSB6609:
.L1049:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1050
	call	_ZdlPv@PLT
.L1050:
	movq	%rbx, %r12
.L1051:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1053
	call	_ZdlPv@PLT
.L1053:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1055
	call	_ZdlPv@PLT
.L1055:
	movq	(%r14), %rdi
	cmpq	%rdi, -192(%rbp)
	je	.L1056
	call	_ZdlPv@PLT
.L1056:
	movq	%r12, %rdi
.LEHB16:
	call	_Unwind_Resume@PLT
.LEHE16:
	.cfi_endproc
.LFE6609:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
.LLSDAC6609:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6609-.LLSDACSBC6609
.LLSDACSBC6609:
	.uleb128 .LEHB16-.LCOLDB18
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0
	.uleb128 0
.LLSDACSEC6609:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.section	.text._ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.size	_ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE, .-_ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.size	_ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE.cold, .-_ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE.cold
.LCOLDE18:
	.section	.text._ZN2v88internal6torque12Declarations24GetGeneratedCallableNameERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
.LHOTE18:
	.section	.text._ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_,"axG",@progbits,_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC5EOS9_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_
	.type	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_, @function
_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_:
.LFB7088:
	.cfi_startproc
	endbr64
	movb	$0, (%rdi)
	movb	$0, 8(%rdi)
	cmpb	$0, (%rsi)
	je	.L1076
	leaq	24(%rdi), %rax
	movq	%rax, 8(%rdi)
	movq	8(%rsi), %rdx
	leaq	24(%rsi), %rax
	cmpq	%rax, %rdx
	je	.L1080
	movq	%rdx, 8(%rdi)
	movq	24(%rsi), %rdx
	movq	%rdx, 24(%rdi)
.L1079:
	movq	16(%rsi), %rdx
	movq	%rax, 8(%rsi)
	movq	$0, 16(%rsi)
	movq	%rdx, 16(%rdi)
	movb	$1, (%rdi)
	movb	$0, 24(%rsi)
.L1076:
	ret
	.p2align 4,,10
	.p2align 3
.L1080:
	movdqu	24(%rsi), %xmm0
	movups	%xmm0, 24(%rdi)
	jmp	.L1079
	.cfi_endproc
.LFE7088:
	.size	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_, .-_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_
	.weak	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	.set	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_,_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC2EOS9_
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB19:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB19:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB14303:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA14303
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-432(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$456, %rsp
	movq	%rsi, -488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB17:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE17:
	movq	-488(%rbp), %rsi
	movq	%r13, %rdi
.LEHB18:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-480(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE18:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB19:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE19:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1082
	call	_ZdlPv@PLT
.L1082:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1091
	addq	$456, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1091:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L1088:
	endbr64
	movq	%rax, %r12
	jmp	.L1083
.L1087:
	endbr64
	movq	%rax, %r12
	jmp	.L1085
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA14303:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14303-.LLSDACSB14303
.LLSDACSB14303:
	.uleb128 .LEHB17-.LFB14303
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB18-.LFB14303
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L1087-.LFB14303
	.uleb128 0
	.uleb128 .LEHB19-.LFB14303
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L1088-.LFB14303
	.uleb128 0
.LLSDACSE14303:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC14303
	.type	_ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB14303:
.L1083:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1085
	call	_ZdlPv@PLT
.L1085:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB20:
	call	_Unwind_Resume@PLT
.LEHE20:
	.cfi_endproc
.LFE14303:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC14303:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC14303-.LLSDACSBC14303
.LLSDACSBC14303:
	.uleb128 .LEHB20-.LCOLDB19
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0
	.uleb128 0
.LLSDACSEC14303:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE19:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE19:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB20:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB20:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB14292:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA14292
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$456, %rsp
	movq	%rsi, -496(%rbp)
	movq	%rdx, -488(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
.LEHB21:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE21:
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
.LEHB22:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-488(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-480(%rbp), %r12
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE22:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
.LEHB23:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE23:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1093
	call	_ZdlPv@PLT
.L1093:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1102
	addq	$456, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L1099:
	endbr64
	movq	%rax, %r12
	jmp	.L1094
.L1098:
	endbr64
	movq	%rax, %r12
	jmp	.L1096
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA14292:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14292-.LLSDACSB14292
.LLSDACSB14292:
	.uleb128 .LEHB21-.LFB14292
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB22-.LFB14292
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L1098-.LFB14292
	.uleb128 0
	.uleb128 .LEHB23-.LFB14292
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L1099-.LFB14292
	.uleb128 0
.LLSDACSE14292:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC14292
	.type	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB14292:
.L1094:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1096
	call	_ZdlPv@PLT
.L1096:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB24:
	call	_Unwind_Resume@PLT
.LEHE24:
	.cfi_endproc
.LFE14292:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC14292:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC14292-.LLSDACSBC14292
.LLSDACSBC14292:
	.uleb128 .LEHB24-.LCOLDB20
	.uleb128 .LEHE24-.LEHB24
	.uleb128 0
	.uleb128 0
.LLSDACSEC14292:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE20:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE20:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB21:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB21:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB14290:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA14290
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$456, %rsp
	movq	%rsi, -496(%rbp)
	movq	%rdx, -488(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
.LEHB25:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE25:
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
.LEHB26:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-488(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_13QualifiedNameE@PLT
	leaq	-480(%rbp), %r12
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE26:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
.LEHB27:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE27:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1104
	call	_ZdlPv@PLT
.L1104:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1113
	addq	$456, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1113:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L1110:
	endbr64
	movq	%rax, %r12
	jmp	.L1105
.L1109:
	endbr64
	movq	%rax, %r12
	jmp	.L1107
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA14290:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14290-.LLSDACSB14290
.LLSDACSB14290:
	.uleb128 .LEHB25-.LFB14290
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB26-.LFB14290
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L1109-.LFB14290
	.uleb128 0
	.uleb128 .LEHB27-.LFB14290
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L1110-.LFB14290
	.uleb128 0
.LLSDACSE14290:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC14290
	.type	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB14290:
.L1105:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1107
	call	_ZdlPv@PLT
.L1107:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB28:
	call	_Unwind_Resume@PLT
.LEHE28:
	.cfi_endproc
.LFE14290:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC14290:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC14290-.LLSDACSBC14290
.LLSDACSBC14290:
	.uleb128 .LEHB28-.LCOLDB21
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0
	.uleb128 0
.LLSDACSEC14290:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE21:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE21:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LCOLDB22:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"ax",@progbits
.LHOTB22:
	.p2align 4
	.type	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, @function
_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0:
.LFB14287:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA14287
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB29:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE29:
	leaq	-416(%rbp), %rdi
	movq	%r13, %rsi
.LEHB30:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-464(%rbp), %r13
	leaq	-408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE30:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB31:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE31:
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1115
	call	_ZdlPv@PLT
.L1115:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1124
	addq	$440, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1124:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L1121:
	endbr64
	movq	%rax, %r12
	jmp	.L1116
.L1120:
	endbr64
	movq	%rax, %r12
	jmp	.L1118
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0,"a",@progbits
.LLSDA14287:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14287-.LLSDACSB14287
.LLSDACSB14287:
	.uleb128 .LEHB29-.LFB14287
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB30-.LFB14287
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L1120-.LFB14287
	.uleb128 0
	.uleb128 .LEHB31-.LFB14287
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L1121-.LFB14287
	.uleb128 0
.LLSDACSE14287:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC14287
	.type	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, @function
_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold:
.LFSB14287:
.L1116:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-464(%rbp), %rdi
	leaq	-448(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1118
	call	_ZdlPv@PLT
.L1118:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB32:
	call	_Unwind_Resume@PLT
.LEHE32:
	.cfi_endproc
.LFE14287:
	.section	.gcc_except_table._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LLSDAC14287:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC14287-.LLSDACSBC14287
.LLSDACSBC14287:
	.uleb128 .LEHB32-.LCOLDB22
	.uleb128 .LEHE32-.LEHB32
	.uleb128 0
	.uleb128 0
.LLSDACSEC14287:
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0, .-_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.section	.text.unlikely._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
	.size	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold, .-_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0.cold
.LCOLDE22:
	.section	.text._ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LHOTE22:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB7507:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7507
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r13
	leaq	-416(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB33:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE33:
	movq	%r12, %rsi
	movq	%r14, %rdi
.LEHB34:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-528(%rbp), %r14
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE34:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB35:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE35:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1126
	call	_ZdlPv@PLT
.L1126:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB36:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE36:
.L1133:
	endbr64
	movq	%rax, %r12
	jmp	.L1129
.L1132:
	endbr64
	movq	%rax, %r13
	jmp	.L1130
.L1134:
	endbr64
	movq	%rax, %r12
.L1127:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1129
	call	_ZdlPv@PLT
.L1129:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB37:
	call	_Unwind_Resume@PLT
.L1130:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE37:
	.cfi_endproc
.LFE7507:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA7507:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7507-.LLSDACSB7507
.LLSDACSB7507:
	.uleb128 .LEHB33-.LFB7507
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB34-.LFB7507
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L1133-.LFB7507
	.uleb128 0
	.uleb128 .LEHB35-.LFB7507
	.uleb128 .LEHE35-.LEHB35
	.uleb128 .L1134-.LFB7507
	.uleb128 0
	.uleb128 .LEHB36-.LFB7507
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L1132-.LFB7507
	.uleb128 0
	.uleb128 .LEHB37-.LFB7507
	.uleb128 .LEHE37-.LEHB37
	.uleb128 0
	.uleb128 0
.LLSDACSE7507:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.text._ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev,"axG",@progbits,_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev
	.type	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev, @function
_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev:
.LFB7536:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	je	.L1136
	cmpb	$0, 16(%rdi)
	je	.L1136
	movq	24(%rdi), %r8
	addq	$40, %rdi
	cmpq	%rdi, %r8
	je	.L1136
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1136:
	ret
	.cfi_endproc
.LFE7536:
	.size	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev, .-_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev
	.weak	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED1Ev
	.set	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED1Ev,_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev
	.section	.text._ZN2v88internal6torque5ValueD2Ev,"axG",@progbits,_ZN2v88internal6torque5ValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5ValueD2Ev
	.type	_ZN2v88internal6torque5ValueD2Ev, @function
_ZN2v88internal6torque5ValueD2Ev:
.LFB6064:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal6torque5ValueE(%rip), %rax
	addq	$88, %rdi
	movq	%rax, -88(%rdi)
	jmp	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev
	.cfi_endproc
.LFE6064:
	.size	_ZN2v88internal6torque5ValueD2Ev, .-_ZN2v88internal6torque5ValueD2Ev
	.weak	_ZN2v88internal6torque5ValueD1Ev
	.set	_ZN2v88internal6torque5ValueD1Ev,_ZN2v88internal6torque5ValueD2Ev
	.section	.text._ZN2v88internal6torque5ValueD0Ev,"axG",@progbits,_ZN2v88internal6torque5ValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5ValueD0Ev
	.type	_ZN2v88internal6torque5ValueD0Ev, @function
_ZN2v88internal6torque5ValueD0Ev:
.LFB6066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque5ValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	88(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -88(%rdi)
	call	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6066:
	.size	_ZN2v88internal6torque5ValueD0Ev, .-_ZN2v88internal6torque5ValueD0Ev
	.section	.text._ZN2v88internal6torque14ExternConstantD2Ev,"axG",@progbits,_ZN2v88internal6torque14ExternConstantD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque14ExternConstantD2Ev
	.type	_ZN2v88internal6torque14ExternConstantD2Ev, @function
_ZN2v88internal6torque14ExternConstantD2Ev:
.LFB13583:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal6torque5ValueE(%rip), %rax
	addq	$88, %rdi
	movq	%rax, -88(%rdi)
	jmp	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev
	.cfi_endproc
.LFE13583:
	.size	_ZN2v88internal6torque14ExternConstantD2Ev, .-_ZN2v88internal6torque14ExternConstantD2Ev
	.weak	_ZN2v88internal6torque14ExternConstantD1Ev
	.set	_ZN2v88internal6torque14ExternConstantD1Ev,_ZN2v88internal6torque14ExternConstantD2Ev
	.section	.text._ZN2v88internal6torque14ExternConstantD0Ev,"axG",@progbits,_ZN2v88internal6torque14ExternConstantD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque14ExternConstantD0Ev
	.type	_ZN2v88internal6torque14ExternConstantD0Ev, @function
_ZN2v88internal6torque14ExternConstantD0Ev:
.LFB13585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque5ValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	88(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -88(%rdi)
	call	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13585:
	.size	_ZN2v88internal6torque14ExternConstantD0Ev, .-_ZN2v88internal6torque14ExternConstantD0Ev
	.section	.text._ZN2v88internal6torque17NamespaceConstantD2Ev,"axG",@progbits,_ZN2v88internal6torque17NamespaceConstantD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque17NamespaceConstantD2Ev
	.type	_ZN2v88internal6torque17NamespaceConstantD2Ev, @function
_ZN2v88internal6torque17NamespaceConstantD2Ev:
.LFB13587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque17NamespaceConstantE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	168(%rdi), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1145
	call	_ZdlPv@PLT
.L1145:
	leaq	16+_ZTVN2v88internal6torque5ValueE(%rip), %rax
	leaq	88(%rbx), %rdi
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev
	.cfi_endproc
.LFE13587:
	.size	_ZN2v88internal6torque17NamespaceConstantD2Ev, .-_ZN2v88internal6torque17NamespaceConstantD2Ev
	.weak	_ZN2v88internal6torque17NamespaceConstantD1Ev
	.set	_ZN2v88internal6torque17NamespaceConstantD1Ev,_ZN2v88internal6torque17NamespaceConstantD2Ev
	.section	.text._ZN2v88internal6torque17NamespaceConstantD0Ev,"axG",@progbits,_ZN2v88internal6torque17NamespaceConstantD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque17NamespaceConstantD0Ev
	.type	_ZN2v88internal6torque17NamespaceConstantD0Ev, @function
_ZN2v88internal6torque17NamespaceConstantD0Ev:
.LFB13589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal6torque17NamespaceConstantE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	168(%rdi), %rdi
	leaq	184(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1148
	call	_ZdlPv@PLT
.L1148:
	leaq	16+_ZTVN2v88internal6torque5ValueE(%rip), %rax
	leaq	88(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$208, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13589:
	.size	_ZN2v88internal6torque17NamespaceConstantD0Ev, .-_ZN2v88internal6torque17NamespaceConstantD0Ev
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_:
.LFB7548:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7548
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB38:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE38:
	movq	%r12, %rdi
.LEHB39:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE39:
.L1153:
	endbr64
	movq	%rax, %r13
.L1151:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB40:
	call	_Unwind_Resume@PLT
.LEHE40:
	.cfi_endproc
.LFE7548:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_,comdat
.LLSDA7548:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7548-.LLSDACSB7548
.LLSDACSB7548:
	.uleb128 .LEHB38-.LFB7548
	.uleb128 .LEHE38-.LEHB38
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB39-.LFB7548
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L1153-.LFB7548
	.uleb128 0
	.uleb128 .LEHB40-.LFB7548
	.uleb128 .LEHE40-.LEHB40
	.uleb128 0
	.uleb128 0
.LLSDACSE7548:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_:
.LFB7645:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7645
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-448(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$504, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB41:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE41:
	movq	%r15, %rsi
	movq	%r12, %rdi
.LEHB42:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torquelsERSoRKNS1_13QualifiedNameE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE42:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB43:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE43:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1156
	call	_ZdlPv@PLT
.L1156:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB44:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE44:
.L1163:
	endbr64
	movq	%rax, %r12
	jmp	.L1159
.L1162:
	endbr64
	movq	%rax, %r13
	jmp	.L1160
.L1164:
	endbr64
	movq	%rax, %r12
.L1157:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1159
	call	_ZdlPv@PLT
.L1159:
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB45:
	call	_Unwind_Resume@PLT
.L1160:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE45:
	.cfi_endproc
.LFE7645:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_,comdat
.LLSDA7645:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7645-.LLSDACSB7645
.LLSDACSB7645:
	.uleb128 .LEHB41-.LFB7645
	.uleb128 .LEHE41-.LEHB41
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB42-.LFB7645
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L1163-.LFB7645
	.uleb128 0
	.uleb128 .LEHB43-.LFB7645
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L1164-.LFB7645
	.uleb128 0
	.uleb128 .LEHB44-.LFB7645
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L1162-.LFB7645
	.uleb128 0
	.uleb128 .LEHB45-.LFB7645
	.uleb128 .LEHE45-.LEHB45
	.uleb128 0
	.uleb128 0
.LLSDACSE7645:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB7913:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7913
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB46:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE46:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	leaq	-416(%rbp), %rdi
.LEHB47:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-528(%rbp), %r14
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE47:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB48:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE48:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1167
	call	_ZdlPv@PLT
.L1167:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB49:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE49:
.L1174:
	endbr64
	movq	%rax, %r12
	jmp	.L1170
.L1173:
	endbr64
	movq	%rax, %r13
	jmp	.L1171
.L1175:
	endbr64
	movq	%rax, %r12
.L1168:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1170
	call	_ZdlPv@PLT
.L1170:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB50:
	call	_Unwind_Resume@PLT
.L1171:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE50:
	.cfi_endproc
.LFE7913:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA7913:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7913-.LLSDACSB7913
.LLSDACSB7913:
	.uleb128 .LEHB46-.LFB7913
	.uleb128 .LEHE46-.LEHB46
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB47-.LFB7913
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L1174-.LFB7913
	.uleb128 0
	.uleb128 .LEHB48-.LFB7913
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L1175-.LFB7913
	.uleb128 0
	.uleb128 .LEHB49-.LFB7913
	.uleb128 .LEHE49-.LEHB49
	.uleb128 .L1173-.LFB7913
	.uleb128 0
	.uleb128 .LEHB50-.LFB7913
	.uleb128 .LEHE50-.LEHB50
	.uleb128 0
	.uleb128 0
.LLSDACSE7913:
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.rodata._ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_.str1.1,"aMS",@progbits,1
.LC23:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB8816:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1191
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1187
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1192
.L1179:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1186:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1193
	testq	%r13, %r13
	jg	.L1182
	testq	%r9, %r9
	jne	.L1185
.L1183:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1193:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1182
.L1185:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1192:
	testq	%rsi, %rsi
	jne	.L1180
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1183
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1187:
	movl	$8, %r14d
	jmp	.L1179
.L1191:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1180:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1179
	.cfi_endproc
.LFE8816:
	.size	_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB8962:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1208
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1204
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1209
.L1196:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1203:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1210
	testq	%r13, %r13
	jg	.L1199
	testq	%r9, %r9
	jne	.L1202
.L1200:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1210:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1199
.L1202:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1209:
	testq	%rsi, %rsi
	jne	.L1197
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1200
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1204:
	movl	$8, %r14d
	jmp	.L1196
.L1208:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1197:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1196
	.cfi_endproc
.LFE8962:
	.size	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_:
.LFB9385:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9385
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB51:
	call	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE51:
	movq	%r12, %rdi
.LEHB52:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE52:
.L1214:
	endbr64
	movq	%rax, %r13
.L1212:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB53:
	call	_Unwind_Resume@PLT
.LEHE53:
	.cfi_endproc
.LFE9385:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_,comdat
.LLSDA9385:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE9385-.LLSDACSB9385
.LLSDACSB9385:
	.uleb128 .LEHB51-.LFB9385
	.uleb128 .LEHE51-.LEHB51
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB52-.LFB9385
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L1214-.LFB9385
	.uleb128 0
	.uleb128 .LEHB53-.LFB9385
	.uleb128 .LEHE53-.LEHB53
	.uleb128 0
	.uleb128 0
.LLSDACSE9385:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_:
.LFB9390:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9390
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB54:
	call	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE54:
	movq	%r12, %rdi
.LEHB55:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE55:
.L1219:
	endbr64
	movq	%rax, %r13
.L1217:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB56:
	call	_Unwind_Resume@PLT
.LEHE56:
	.cfi_endproc
.LFE9390:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_,comdat
.LLSDA9390:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE9390-.LLSDACSB9390
.LLSDACSB9390:
	.uleb128 .LEHB54-.LFB9390
	.uleb128 .LEHE54-.LEHB54
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB55-.LFB9390
	.uleb128 .LEHE55-.LEHB55
	.uleb128 .L1219-.LFB9390
	.uleb128 0
	.uleb128 .LEHB56-.LFB9390
	.uleb128 .LEHE56-.LEHB56
	.uleb128 0
	.uleb128 0
.LLSDACSE9390:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB9392:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9392
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB57:
	call	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE57:
	movq	%r12, %rdi
.LEHB58:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE58:
.L1224:
	endbr64
	movq	%rax, %r13
.L1222:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB59:
	call	_Unwind_Resume@PLT
.LEHE59:
	.cfi_endproc
.LFE9392:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA9392:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE9392-.LLSDACSB9392
.LLSDACSB9392:
	.uleb128 .LEHB57-.LFB9392
	.uleb128 .LEHE57-.LEHB57
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB58-.LFB9392
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L1224-.LFB9392
	.uleb128 0
	.uleb128 .LEHB59-.LFB9392
	.uleb128 .LEHE59-.LEHB59
	.uleb128 0
	.uleb128 0
.LLSDACSE9392:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB9393:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9393
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB60:
	call	_ZN2v88internal6torqueL7MessageIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE60:
	movq	%r12, %rdi
.LEHB61:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE61:
.L1229:
	endbr64
	movq	%rax, %r13
.L1227:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
.LEHB62:
	call	_Unwind_Resume@PLT
.LEHE62:
	.cfi_endproc
.LFE9393:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA9393:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE9393-.LLSDACSB9393
.LLSDACSB9393:
	.uleb128 .LEHB60-.LFB9393
	.uleb128 .LEHE60-.LEHB60
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB61-.LFB9393
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L1229-.LFB9393
	.uleb128 0
	.uleb128 .LEHB62-.LFB9393
	.uleb128 .LEHE62-.LEHB62
	.uleb128 0
	.uleb128 0
.LLSDACSE9393:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_:
.LFB9518:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9518
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$536, %rsp
	movq	%rdi, -568(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -552(%rbp)
	movq	%r8, -560(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
.LEHB63:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE63:
	movq	-568(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
.LEHB64:
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-552(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	-560(%rbp), %rax
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-544(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE64:
	leaq	-512(%rbp), %r12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
.LEHB65:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE65:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1232
	call	_ZdlPv@PLT
.L1232:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB66:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE66:
.L1239:
	endbr64
	movq	%rax, %r12
	jmp	.L1235
.L1238:
	endbr64
	movq	%rax, %r13
	jmp	.L1236
.L1240:
	endbr64
	movq	%rax, %r12
.L1233:
	movq	-544(%rbp), %rdi
	leaq	-528(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1235
	call	_ZdlPv@PLT
.L1235:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB67:
	call	_Unwind_Resume@PLT
.L1236:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE67:
	.cfi_endproc
.LFE9518:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_,comdat
.LLSDA9518:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE9518-.LLSDACSB9518
.LLSDACSB9518:
	.uleb128 .LEHB63-.LFB9518
	.uleb128 .LEHE63-.LEHB63
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB64-.LFB9518
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L1239-.LFB9518
	.uleb128 0
	.uleb128 .LEHB65-.LFB9518
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L1240-.LFB9518
	.uleb128 0
	.uleb128 .LEHB66-.LFB9518
	.uleb128 .LEHE66-.LEHB66
	.uleb128 .L1238-.LFB9518
	.uleb128 0
	.uleb128 .LEHB67-.LFB9518
	.uleb128 .LEHE67-.LEHB67
	.uleb128 0
	.uleb128 0
.LLSDACSE9518:
	.section	.text._ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal6torque9TypeAliasESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque9TypeAliasESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque9TypeAliasESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque9TypeAliasESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque9TypeAliasESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB10626:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1256
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1252
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1257
.L1244:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1251:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1258
	testq	%r13, %r13
	jg	.L1247
	testq	%r9, %r9
	jne	.L1250
.L1248:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1258:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1247
.L1250:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1257:
	testq	%rsi, %rsi
	jne	.L1245
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1248
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1252:
	movl	$8, %r14d
	jmp	.L1244
.L1256:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1245:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1244
	.cfi_endproc
.LFE10626:
	.size	_ZNSt6vectorIPN2v88internal6torque9TypeAliasESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque9TypeAliasESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal6torque5ValueESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque5ValueESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque5ValueESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque5ValueESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque5ValueESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB10661:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1273
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1269
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1274
.L1261:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1268:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1275
	testq	%r13, %r13
	jg	.L1264
	testq	%r9, %r9
	jne	.L1267
.L1265:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1275:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1264
.L1267:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1274:
	testq	%rsi, %rsi
	jne	.L1262
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1265
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1269:
	movl	$8, %r14d
	jmp	.L1261
.L1273:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1262:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1261
	.cfi_endproc
.LFE10661:
	.size	_ZNSt6vectorIPN2v88internal6torque5ValueESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque5ValueESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE,"axG",@progbits,_ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE
	.type	_ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE, @function
_ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE:
.LFB7942:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7942
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	(%rsi), %rbx
	movq	8(%rsi), %r13
	cmpq	%rbx, %r13
	je	.L1276
	leaq	-48(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L1282:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L1279
	movl	8(%rax), %ecx
	leal	-10(%rcx), %edx
	cmpl	$1, %edx
	ja	.L1279
	movq	%rax, -48(%rbp)
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1295
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
.L1279:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L1282
.L1276:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1296
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1295:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rdi
.LEHB68:
	call	_ZNSt6vectorIPN2v88internal6torque5ValueESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE68:
	jmp	.L1279
.L1296:
	call	__stack_chk_fail@PLT
.L1288:
	endbr64
	movq	%rax, %r13
.L1283:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1284
	call	_ZdlPv@PLT
.L1284:
	movq	%r13, %rdi
.LEHB69:
	call	_Unwind_Resume@PLT
.LEHE69:
	.cfi_endproc
.LFE7942:
	.section	.gcc_except_table._ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE,"aG",@progbits,_ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE,comdat
.LLSDA7942:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7942-.LLSDACSB7942
.LLSDACSB7942:
	.uleb128 .LEHB68-.LFB7942
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L1288-.LFB7942
	.uleb128 0
	.uleb128 .LEHB69-.LFB7942
	.uleb128 .LEHE69-.LEHB69
	.uleb128 0
	.uleb128 0
.LLSDACSE7942:
	.section	.text._ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE,"axG",@progbits,_ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE,comdat
	.size	_ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE, .-_ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE
	.section	.text._ZNSt6vectorIPN2v88internal6torque7GenericESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque7GenericESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque7GenericESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque7GenericESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque7GenericESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB10706:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1311
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1307
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1312
.L1299:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1306:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1313
	testq	%r13, %r13
	jg	.L1302
	testq	%r9, %r9
	jne	.L1305
.L1303:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1313:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1302
.L1305:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1312:
	testq	%rsi, %rsi
	jne	.L1300
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1302:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1303
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1307:
	movl	$8, %r14d
	jmp	.L1299
.L1311:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1300:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1299
	.cfi_endproc
.LFE10706:
	.size	_ZNSt6vectorIPN2v88internal6torque7GenericESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque7GenericESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal6torque17GenericStructTypeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque17GenericStructTypeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque17GenericStructTypeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque17GenericStructTypeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque17GenericStructTypeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB10726:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1328
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1324
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1329
.L1316:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1323:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1330
	testq	%r13, %r13
	jg	.L1319
	testq	%r9, %r9
	jne	.L1322
.L1320:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1330:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1319
.L1322:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1329:
	testq	%rsi, %rsi
	jne	.L1317
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1320
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1324:
	movl	$8, %r14d
	jmp	.L1316
.L1328:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1317:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1316
	.cfi_endproc
.LFE10726:
	.size	_ZNSt6vectorIPN2v88internal6torque17GenericStructTypeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque17GenericStructTypeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm:
.LFB11447:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11447
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L1353
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L1354
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB70:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L1333:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1335
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L1338:
	testq	%rsi, %rsi
	je	.L1335
.L1336:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	64(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L1337
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L1342
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L1336
	.p2align 4,,10
	.p2align 3
.L1335:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L1339
	call	_ZdlPv@PLT
.L1339:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1342:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1353:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L1333
.L1354:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE70:
.L1343:
	endbr64
	movq	%rax, %rdi
.L1340:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB71:
	call	__cxa_rethrow@PLT
.LEHE71:
.L1344:
	endbr64
	movq	%rax, %r12
.L1341:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB72:
	call	_Unwind_Resume@PLT
.LEHE72:
	.cfi_endproc
.LFE11447:
	.section	.gcc_except_table._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA11447:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11447-.LLSDATTD11447
.LLSDATTD11447:
	.byte	0x1
	.uleb128 .LLSDACSE11447-.LLSDACSB11447
.LLSDACSB11447:
	.uleb128 .LEHB70-.LFB11447
	.uleb128 .LEHE70-.LEHB70
	.uleb128 .L1343-.LFB11447
	.uleb128 0x1
	.uleb128 .LEHB71-.LFB11447
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L1344-.LFB11447
	.uleb128 0
	.uleb128 .LEHB72-.LFB11447
	.uleb128 .LEHE72-.LEHB72
	.uleb128 0
	.uleb128 0
.LLSDACSE11447:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT11447:
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm:
.LFB10143:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10143
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB73:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE73:
	testb	%al, %al
	je	.L1356
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB74:
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
.LEHE74:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L1356:
	movq	%r14, 64(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L1357
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L1358:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1375
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1357:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1359
	movq	64(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L1359:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1358
.L1375:
	call	__stack_chk_fail@PLT
.L1365:
	endbr64
	movq	%rax, %rdi
.L1360:
	call	__cxa_begin_catch@PLT
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1361
	call	_ZdlPv@PLT
.L1361:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1362
	call	_ZdlPv@PLT
.L1362:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB75:
	call	__cxa_rethrow@PLT
.LEHE75:
.L1366:
	endbr64
	movq	%rax, %r12
.L1363:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB76:
	call	_Unwind_Resume@PLT
.LEHE76:
	.cfi_endproc
.LFE10143:
	.section	.gcc_except_table._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,"aG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,comdat
	.align 4
.LLSDA10143:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT10143-.LLSDATTD10143
.LLSDATTD10143:
	.byte	0x1
	.uleb128 .LLSDACSE10143-.LLSDACSB10143
.LLSDACSB10143:
	.uleb128 .LEHB73-.LFB10143
	.uleb128 .LEHE73-.LEHB73
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB74-.LFB10143
	.uleb128 .LEHE74-.LEHB74
	.uleb128 .L1365-.LFB10143
	.uleb128 0x1
	.uleb128 .LEHB75-.LFB10143
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L1366-.LFB10143
	.uleb128 0
	.uleb128 .LEHB76-.LFB10143
	.uleb128 .LEHE76-.LEHB76
	.uleb128 0
	.uleb128 0
.LLSDACSE10143:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT10143:
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,comdat
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_:
.LFB8915:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8915
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r15), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r14), %rcx
	xorl	%edx, %edx
	movq	%rax, %r12
	divq	%rcx
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rbx
	testq	%rax, %rax
	je	.L1377
	movq	(%rax), %r13
	movq	64(%r13), %rsi
.L1380:
	cmpq	%rsi, %r12
	je	.L1415
.L1378:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L1377
	movq	64(%r13), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %rbx
	je	.L1380
.L1377:
	movl	$72, %edi
.LEHB77:
	call	_Znwm@PLT
	leaq	24(%rax), %rdi
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%rdi, 8(%rax)
	movq	(%r15), %rax
	movq	8(%r15), %r15
	movq	%rax, %rcx
	movq	%rax, -72(%rbp)
	addq	%r15, %rcx
	je	.L1381
	testq	%rax, %rax
	je	.L1416
.L1381:
	movq	%r15, -64(%rbp)
	cmpq	$15, %r15
	ja	.L1417
	cmpq	$1, %r15
	jne	.L1384
	movq	-72(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, 24(%r13)
.L1385:
	movq	%r15, 16(%r13)
	pxor	%xmm0, %xmm0
	movq	%r13, %rcx
	movq	%r12, %rdx
	movb	$0, (%rdi,%r15)
	movl	$1, %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	$0, 56(%r13)
	movups	%xmm0, 40(%r13)
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_St6vectorIPN2v88internal6torque10DeclarableESaISD_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm
.LEHE77:
	addq	$40, %rax
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	8(%r15), %rdx
	cmpq	16(%r13), %rdx
	jne	.L1378
	movq	%rcx, -72(%rbp)
	testq	%rdx, %rdx
	je	.L1379
	movq	8(%r13), %rsi
	movq	(%r15), %rdi
	call	memcmp@PLT
	movq	-72(%rbp), %rcx
	testl	%eax, %eax
	jne	.L1378
.L1379:
	leaq	40(%r13), %rax
.L1376:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1418
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1384:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L1385
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1417:
	leaq	-64(%rbp), %rsi
	leaq	8(%r13), %rdi
	xorl	%edx, %edx
.LEHB78:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 8(%r13)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%r13)
.L1383:
	movq	-72(%rbp), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %r15
	movq	8(%r13), %rdi
	jmp	.L1385
.L1418:
	call	__stack_chk_fail@PLT
.L1416:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE78:
.L1390:
	endbr64
	movq	%rax, %rdi
.L1387:
	call	__cxa_begin_catch@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.LEHB79:
	call	__cxa_rethrow@PLT
.LEHE79:
.L1391:
	endbr64
	movq	%rax, %r12
.L1388:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB80:
	call	_Unwind_Resume@PLT
.LEHE80:
	.cfi_endproc
.LFE8915:
	.section	.gcc_except_table._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"aG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.align 4
.LLSDA8915:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT8915-.LLSDATTD8915
.LLSDATTD8915:
	.byte	0x1
	.uleb128 .LLSDACSE8915-.LLSDACSB8915
.LLSDACSB8915:
	.uleb128 .LEHB77-.LFB8915
	.uleb128 .LEHE77-.LEHB77
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB78-.LFB8915
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L1390-.LFB8915
	.uleb128 0x1
	.uleb128 .LEHB79-.LFB8915
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L1391-.LFB8915
	.uleb128 0
	.uleb128 .LEHB80-.LFB8915
	.uleb128 .LEHE80-.LEHB80
	.uleb128 0
	.uleb128 0
.LLSDACSE8915:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT8915:
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.section	.rodata._ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"ambiguous reference to scope "
	.section	.rodata._ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,"axG",@progbits,_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE, @function
_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE:
.LFB5980:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5980
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%rdi, -176(%rbp)
	movq	(%rdx), %r8
	leaq	72(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	8(%rdx), %r8
	je	.L1492
	movq	%r8, %rsi
.LEHB81:
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	8(%rdx), %rcx
	cmpq	%rax, %rcx
	je	.L1427
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1428
	cmpl	$6, 8(%rdx)
	ja	.L1428
	testq	%r13, %r13
	jne	.L1493
	movq	%rdx, %r13
.L1428:
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.L1429
	testq	%r13, %r13
	je	.L1427
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rdx
	leaq	-128(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	%rax, -184(%rbp)
	addq	%rsi, %rdx
	movq	%rax, -144(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE81:
	movq	(%rbx), %rax
	leaq	32(%rax), %r14
	movq	8(%rbx), %rax
	movq	%rax, %rbx
	movq	%rax, -168(%rbp)
	subq	%r14, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	testq	%rbx, %rbx
	js	.L1494
	testq	%rax, %rax
	je	.L1465
	movq	%rbx, %rdi
.LEHB82:
	call	_Znwm@PLT
.LEHE82:
	movq	%rax, -200(%rbp)
.L1432:
	movq	-200(%rbp), %rax
	addq	%rax, %rbx
	movq	%rbx, -208(%rbp)
	movq	%rax, %rbx
	cmpq	%r14, -168(%rbp)
	je	.L1435
	leaq	-152(%rbp), %rax
	movq	%rax, -192(%rbp)
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1437:
	cmpq	$1, %r12
	jne	.L1439
	movzbl	(%r15), %eax
	movb	%al, 16(%rbx)
.L1440:
	movq	%r12, 8(%rbx)
	addq	$32, %r14
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r14, -168(%rbp)
	je	.L1435
.L1441:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	(%r14), %r15
	movq	8(%r14), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L1436
	testq	%r15, %r15
	je	.L1495
.L1436:
	movq	%r12, -152(%rbp)
	cmpq	$15, %r12
	jbe	.L1437
	movq	-192(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB83:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE83:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-152(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1438:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-152(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	-176(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
.L1419:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1496
	movq	-176(%rbp), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1439:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1440
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	-200(%rbp), %xmm0
	movq	-208(%rbp), %rax
	movq	%rbx, %xmm1
	leaq	-72(%rbp), %rbx
	movq	%rbx, -88(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	cmpq	-184(%rbp), %rax
	je	.L1497
	movq	%rax, -88(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -72(%rbp)
.L1452:
	movq	-136(%rbp), %rax
	leaq	-112(%rbp), %r12
	movq	%r13, %rsi
	movb	$0, -128(%rbp)
	movq	-176(%rbp), %rdi
	movq	%r12, %rdx
	movq	$0, -136(%rbp)
	movq	%rax, -80(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -144(%rbp)
.LEHB84:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE84:
	movq	-88(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1453
	call	_ZdlPv@PLT
.L1453:
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1454
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1455
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1458
.L1456:
	movq	-112(%rbp), %r12
.L1454:
	testq	%r12, %r12
	je	.L1459
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1459:
	movq	-144(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1419
	call	_ZdlPv@PLT
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1455:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1458
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1492:
	leaq	24(%rdx), %rsi
.LEHB85:
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	pxor	%xmm0, %xmm0
	movq	8(%rax), %rbx
	movq	%rax, %r12
	subq	(%rax), %rbx
	movq	-176(%rbp), %rax
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rbx, %rax
	sarq	$3, %rax
	je	.L1498
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L1499
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rax, %rcx
.L1422:
	movq	-176(%rbp), %rax
	movq	%rcx, %xmm0
	addq	%rcx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	8(%r12), %rax
	movq	(%r12), %rsi
	movq	%rax, %rbx
	subq	%rsi, %rbx
	cmpq	%rsi, %rax
	je	.L1425
	movq	%rcx, %rdi
	movq	%rbx, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L1425:
	movq	-176(%rbp), %rax
	addq	%rbx, %rcx
	movq	%rcx, 8(%rax)
	jmp	.L1419
.L1497:
	movdqa	-128(%rbp), %xmm2
	movups	%xmm2, -72(%rbp)
	jmp	.L1452
.L1465:
	movq	$0, -200(%rbp)
	jmp	.L1432
.L1498:
	xorl	%ecx, %ecx
	jmp	.L1422
.L1493:
	movq	(%rbx), %rsi
	leaq	.LC24(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA30_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE85:
.L1495:
	leaq	.LC16(%rip), %rdi
.LEHB86:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE86:
.L1499:
.LEHB87:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE87:
.L1494:
	leaq	.LC25(%rip), %rdi
.LEHB88:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE88:
.L1496:
	call	__stack_chk_fail@PLT
.L1467:
	endbr64
	movq	%rax, %rbx
	jmp	.L1461
.L1471:
	endbr64
.L1491:
	movq	%rax, %rbx
	jmp	.L1450
.L1461:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
.L1451:
	movq	-144(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1462
	call	_ZdlPv@PLT
.L1462:
	movq	%rbx, %rdi
.LEHB89:
	call	_Unwind_Resume@PLT
.LEHE89:
.L1469:
	endbr64
	movq	%rax, %rdi
	jmp	.L1444
.L1470:
	endbr64
	jmp	.L1491
.L1444:
	call	__cxa_begin_catch@PLT
	movq	-200(%rbp), %r12
.L1447:
	cmpq	%rbx, %r12
	jne	.L1500
.LEHB90:
	call	__cxa_rethrow@PLT
.LEHE90:
.L1450:
	jmp	.L1451
.L1500:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1446
	call	_ZdlPv@PLT
.L1446:
	addq	$32, %r12
	jmp	.L1447
.L1468:
	endbr64
	movq	%rax, %rbx
.L1448:
	call	__cxa_end_catch@PLT
	cmpq	$0, -200(%rbp)
	je	.L1451
	movq	-200(%rbp), %rdi
	call	_ZdlPv@PLT
	jmp	.L1451
	.cfi_endproc
.LFE5980:
	.section	.gcc_except_table._ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,"aG",@progbits,_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,comdat
	.align 4
.LLSDA5980:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5980-.LLSDATTD5980
.LLSDATTD5980:
	.byte	0x1
	.uleb128 .LLSDACSE5980-.LLSDACSB5980
.LLSDACSB5980:
	.uleb128 .LEHB81-.LFB5980
	.uleb128 .LEHE81-.LEHB81
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB82-.LFB5980
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L1471-.LFB5980
	.uleb128 0
	.uleb128 .LEHB83-.LFB5980
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L1469-.LFB5980
	.uleb128 0x1
	.uleb128 .LEHB84-.LFB5980
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L1467-.LFB5980
	.uleb128 0
	.uleb128 .LEHB85-.LFB5980
	.uleb128 .LEHE85-.LEHB85
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB86-.LFB5980
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L1469-.LFB5980
	.uleb128 0x1
	.uleb128 .LEHB87-.LFB5980
	.uleb128 .LEHE87-.LEHB87
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB88-.LFB5980
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L1470-.LFB5980
	.uleb128 0
	.uleb128 .LEHB89-.LFB5980
	.uleb128 .LEHE89-.LEHB89
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB90-.LFB5980
	.uleb128 .LEHE90-.LEHB90
	.uleb128 .L1468-.LFB5980
	.uleb128 0
.LLSDACSE5980:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT5980:
	.section	.text._ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,"axG",@progbits,_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE,comdat
	.size	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE, .-_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
	.section	.text._ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE,"axG",@progbits,_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE, @function
_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE:
.LFB5996:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5996
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L1502
	movq	%r15, %rdi
.LEHB91:
	call	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	movdqa	-80(%rbp), %xmm1
	movq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	(%r12), %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%rax, 16(%r12)
	movups	%xmm1, (%r12)
	testq	%rdi, %rdi
	je	.L1502
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1502
	call	_ZdlPv@PLT
.L1502:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE91:
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %r13
	cmpq	%rdi, %r13
	je	.L1506
	movq	%rdi, %rbx
	leaq	-88(%rbp), %r14
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1536:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
	cmpq	%rbx, %r13
	je	.L1535
.L1509:
	movq	(%rbx), %rax
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	jne	.L1536
	movq	%r14, %rdx
	movq	%r12, %rdi
.LEHB92:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE92:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L1509
	.p2align 4,,10
	.p2align 3
.L1535:
	movq	-80(%rbp), %rdi
.L1506:
	testq	%rdi, %rdi
	je	.L1501
	call	_ZdlPv@PLT
.L1501:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1537
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1537:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L1516:
	endbr64
	movq	%rax, %r13
	jmp	.L1513
.L1517:
	endbr64
	movq	%rax, %r13
.L1511:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1513
	call	_ZdlPv@PLT
.L1513:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1514
	call	_ZdlPv@PLT
.L1514:
	movq	%r13, %rdi
.LEHB93:
	call	_Unwind_Resume@PLT
.LEHE93:
	.cfi_endproc
.LFE5996:
	.section	.gcc_except_table._ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE,"aG",@progbits,_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE,comdat
.LLSDA5996:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5996-.LLSDACSB5996
.LLSDACSB5996:
	.uleb128 .LEHB91-.LFB5996
	.uleb128 .LEHE91-.LEHB91
	.uleb128 .L1516-.LFB5996
	.uleb128 0
	.uleb128 .LEHB92-.LFB5996
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L1517-.LFB5996
	.uleb128 0
	.uleb128 .LEHB93-.LFB5996
	.uleb128 .LEHE93-.LEHB93
	.uleb128 0
	.uleb128 0
.LLSDACSE5996:
	.section	.text._ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE,"axG",@progbits,_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE,comdat
	.size	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE, .-_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	.section	.rodata._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0.str1.1,"aMS",@progbits,1
.LC26:
	.string	"type"
.LC27:
	.string	")"
.LC28:
	.string	" (type "
.LC29:
	.string	"cannot redeclare "
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0,"ax",@progbits
.LCOLDB30:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0,"ax",@progbits
.LHOTB30:
	.p2align 4
	.type	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0, @function
_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0:
.LFB14301:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA14301
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rax
	movq	%r13, -176(%rbp)
	movq	%rax, -256(%rbp)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L1539
	testq	%r15, %r15
	je	.L1602
.L1539:
	movq	%r12, -208(%rbp)
	cmpq	$15, %r12
	ja	.L1603
	cmpq	$1, %r12
	jne	.L1542
	movzbl	(%r15), %eax
	movb	%al, -160(%rbp)
	movq	%r13, %rax
.L1543:
	movq	%r12, -168(%rbp)
	movb	$0, (%rax,%r12)
	movq	-176(%rbp), %rax
	cmpq	%r13, %rax
	je	.L1604
	leaq	-72(%rbp), %rsi
	movq	-160(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	-168(%rbp), %rdx
	movq	%rsi, -264(%rbp)
	movq	%rsi, -88(%rbp)
	leaq	-128(%rbp), %rsi
	movq	%rax, -144(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -168(%rbp)
	movb	$0, -160(%rbp)
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	cmpq	%rsi, %rax
	je	.L1545
	movq	%rax, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L1547:
	movq	%rdx, -80(%rbp)
	leaq	-112(%rbp), %r15
.LEHB94:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	leaq	-208(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rdi, -280(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE94:
	movq	-208(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	-200(%rbp), %r12
	movq	$0, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	cmpq	%r12, %rbx
	je	.L1548
	leaq	-248(%rbp), %rax
	movq	%rax, -272(%rbp)
	jmp	.L1553
	.p2align 4,,10
	.p2align 3
.L1606:
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
.L1550:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L1605
.L1553:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L1550
	cmpl	$9, 8(%rax)
	jne	.L1550
	movq	%rax, -248(%rbp)
	movq	-232(%rbp), %rsi
	cmpq	-224(%rbp), %rsi
	jne	.L1606
	movq	-272(%rbp), %rdx
	leaq	-240(%rbp), %rdi
.LEHB95:
	call	_ZNSt6vectorIPN2v88internal6torque9TypeAliasESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE95:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L1553
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	-208(%rbp), %r12
.L1548:
	testq	%r12, %r12
	je	.L1558
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1558:
	movq	-88(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L1559
	call	_ZdlPv@PLT
.L1559:
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1560
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1561
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1564
.L1562:
	movq	-112(%rbp), %r12
.L1560:
	testq	%r12, %r12
	je	.L1565
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1565:
	movq	-176(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1566
	call	_ZdlPv@PLT
.L1566:
	movq	-240(%rbp), %rdi
	cmpq	%rdi, -232(%rbp)
	jne	.L1607
	testq	%rdi, %rdi
	je	.L1538
	call	_ZdlPv@PLT
.L1538:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1608
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1542:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L1609
	movq	%r13, %rax
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1561:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1564
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1603:
	leaq	-176(%rbp), %rdi
	leaq	-208(%rbp), %rsi
	xorl	%edx, %edx
.LEHB96:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -176(%rbp)
	movq	%rax, %rdi
	movq	-208(%rbp), %rax
	movq	%rax, -160(%rbp)
.L1541:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-208(%rbp), %r12
	movq	-176(%rbp), %rax
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1604:
	leaq	-72(%rbp), %rax
	movdqa	-160(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	-168(%rbp), %rdx
	movb	$0, -160(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -264(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
.L1545:
	movdqa	-128(%rbp), %xmm2
	movups	%xmm2, -72(%rbp)
	jmp	.L1547
.L1602:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE96:
.L1607:
.LEHB97:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	-280(%rbp), %r8
	leaq	-256(%rbp), %rcx
	leaq	.LC27(%rip), %r9
	leaq	.LC28(%rip), %rdx
	movq	%r14, %rsi
	leaq	.LC29(%rip), %rdi
	movq	(%rax), %rax
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_
.LEHE97:
.L1608:
	call	__stack_chk_fail@PLT
.L1609:
	movq	%r13, %rdi
	jmp	.L1541
.L1577:
	endbr64
	movq	%rax, %r12
	jmp	.L1554
.L1576:
	endbr64
	movq	%rax, %r12
	jmp	.L1569
.L1575:
	endbr64
	movq	%rax, %r12
	jmp	.L1571
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0,"a",@progbits
.LLSDA14301:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14301-.LLSDACSB14301
.LLSDACSB14301:
	.uleb128 .LEHB94-.LFB14301
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L1576-.LFB14301
	.uleb128 0
	.uleb128 .LEHB95-.LFB14301
	.uleb128 .LEHE95-.LEHB95
	.uleb128 .L1577-.LFB14301
	.uleb128 0
	.uleb128 .LEHB96-.LFB14301
	.uleb128 .LEHE96-.LEHB96
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB97-.LFB14301
	.uleb128 .LEHE97-.LEHB97
	.uleb128 .L1575-.LFB14301
	.uleb128 0
.LLSDACSE14301:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC14301
	.type	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0.cold, @function
_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0.cold:
.LFSB14301:
.L1554:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1555
	call	_ZdlPv@PLT
.L1555:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1569
	call	_ZdlPv@PLT
.L1569:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	-176(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1572
.L1601:
	call	_ZdlPv@PLT
.L1572:
	movq	%r12, %rdi
.LEHB98:
	call	_Unwind_Resume@PLT
.LEHE98:
.L1571:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1601
	jmp	.L1572
	.cfi_endproc
.LFE14301:
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
.LLSDAC14301:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC14301-.LLSDACSBC14301
.LLSDACSBC14301:
	.uleb128 .LEHB98-.LCOLDB30
	.uleb128 .LEHE98-.LEHB98
	.uleb128 0
	.uleb128 0
.LLSDACSEC14301:
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.size	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0, .-_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.size	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0.cold, .-_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0.cold
.LCOLDE30:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
.LHOTE30:
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0,"ax",@progbits
.LCOLDB31:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0,"ax",@progbits
.LHOTB31:
	.p2align 4
	.type	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0, @function
_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0:
.LFB14306:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA14306
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-160(%rbp), %rbx
	subq	$216, %rsp
	movq	(%rdi), %r14
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC4(%rip), %rax
	movq	%rbx, -176(%rbp)
	movq	%rax, -248(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1611
	testq	%r14, %r14
	je	.L1665
.L1611:
	movq	%r12, -208(%rbp)
	cmpq	$15, %r12
	ja	.L1666
	cmpq	$1, %r12
	jne	.L1614
	movzbl	(%r14), %eax
	movb	%al, -160(%rbp)
	movq	%rbx, %rax
.L1615:
	movq	%r12, -168(%rbp)
	movb	$0, (%rax,%r12)
	movq	-176(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L1667
	movq	-160(%rbp), %rcx
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %r12
	leaq	-128(%rbp), %rsi
	movq	-168(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -168(%rbp)
	movb	$0, -160(%rbp)
	movq	$0, -96(%rbp)
	movq	%r12, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	cmpq	%rsi, %rax
	je	.L1617
	movq	%rax, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L1619:
	movq	%rdx, -80(%rbp)
	leaq	-112(%rbp), %r14
.LEHB99:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	leaq	-208(%rbp), %r15
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE99:
	leaq	-240(%rbp), %rdi
	movq	%r15, %rsi
.LEHB100:
	call	_ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE
.LEHE100:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1620
	call	_ZdlPv@PLT
.L1620:
	movq	-88(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1621
	call	_ZdlPv@PLT
.L1621:
	movq	-104(%rbp), %r14
	movq	-112(%rbp), %r12
	cmpq	%r12, %r14
	je	.L1622
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1623
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r14, %r12
	jne	.L1626
.L1624:
	movq	-112(%rbp), %r12
.L1622:
	testq	%r12, %r12
	je	.L1627
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1627:
	movq	-176(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1628
	call	_ZdlPv@PLT
.L1628:
	movq	-240(%rbp), %rdi
	cmpq	%rdi, -232(%rbp)
	jne	.L1668
	testq	%rdi, %rdi
	je	.L1610
	call	_ZdlPv@PLT
.L1610:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1669
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1614:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L1670
	movq	%rbx, %rax
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1623:
	addq	$32, %r12
	cmpq	%r12, %r14
	jne	.L1626
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1666:
	leaq	-176(%rbp), %rdi
	leaq	-208(%rbp), %rsi
	xorl	%edx, %edx
.LEHB101:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -176(%rbp)
	movq	%rax, %rdi
	movq	-208(%rbp), %rax
	movq	%rax, -160(%rbp)
.L1613:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-208(%rbp), %r12
	movq	-176(%rbp), %rax
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1667:
	leaq	-72(%rbp), %r12
	movdqa	-160(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	-168(%rbp), %rdx
	movb	$0, -160(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -96(%rbp)
	movq	%r12, -88(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
.L1617:
	movdqa	-128(%rbp), %xmm2
	movups	%xmm2, -72(%rbp)
	jmp	.L1619
.L1665:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE101:
.L1668:
.LEHB102:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	leaq	-248(%rbp), %rcx
	movq	%r15, %r8
	movq	%r13, %rsi
	leaq	.LC27(%rip), %r9
	leaq	.LC28(%rip), %rdx
	movq	(%rax), %rax
	leaq	.LC29(%rip), %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_
.LEHE102:
.L1669:
	call	__stack_chk_fail@PLT
.L1670:
	movq	%rbx, %rdi
	jmp	.L1613
.L1641:
	endbr64
	movq	%rax, %r12
	jmp	.L1631
.L1640:
	endbr64
	movq	%rax, %r12
	jmp	.L1633
.L1639:
	endbr64
	movq	%rax, %r12
	jmp	.L1635
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0,"a",@progbits
.LLSDA14306:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14306-.LLSDACSB14306
.LLSDACSB14306:
	.uleb128 .LEHB99-.LFB14306
	.uleb128 .LEHE99-.LEHB99
	.uleb128 .L1640-.LFB14306
	.uleb128 0
	.uleb128 .LEHB100-.LFB14306
	.uleb128 .LEHE100-.LEHB100
	.uleb128 .L1641-.LFB14306
	.uleb128 0
	.uleb128 .LEHB101-.LFB14306
	.uleb128 .LEHE101-.LEHB101
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB102-.LFB14306
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L1639-.LFB14306
	.uleb128 0
.LLSDACSE14306:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC14306
	.type	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0.cold, @function
_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0.cold:
.LFSB14306:
.L1631:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1633
	call	_ZdlPv@PLT
.L1633:
	movq	%r14, %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	-176(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1636
.L1664:
	call	_ZdlPv@PLT
.L1636:
	movq	%r12, %rdi
.LEHB103:
	call	_Unwind_Resume@PLT
.LEHE103:
.L1635:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1664
	jmp	.L1636
	.cfi_endproc
.LFE14306:
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
.LLSDAC14306:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC14306-.LLSDACSBC14306
.LLSDACSBC14306:
	.uleb128 .LEHB103-.LCOLDB31
	.uleb128 .LEHE103-.LEHB103
	.uleb128 0
	.uleb128 0
.LLSDACSEC14306:
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.size	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0, .-_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	.size	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0.cold, .-_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0.cold
.LCOLDE31:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
.LHOTE31:
	.section	.rodata._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"generic struct"
.LC33:
	.string	" "
.LC34:
	.string	"ambiguous reference to "
	.section	.text.unlikely._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE,"ax",@progbits
	.align 2
.LCOLDB35:
	.section	.text._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE,"ax",@progbits
.LHOTB35:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE, @function
_ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE:
.LFB6561:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6561
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB104:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE104:
	pxor	%xmm0, %xmm0
	movq	(%rax), %rax
	movq	(%rax), %r15
	movaps	%xmm0, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	16(%r15), %r14
	testq	%r14, %r14
	je	.L1672
	movq	$0, -192(%rbp)
	movq	16(%r14), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L1673
	movq	$0, -160(%rbp)
	movq	16(%rax), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L1674
	movq	$0, -128(%rbp)
	movq	16(%rax), %r12
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -144(%rbp)
	testq	%r12, %r12
	je	.L1675
	movq	$0, -96(%rbp)
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -112(%rbp)
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1676
	movq	%rbx, %rdx
	movq	%r13, %rdi
.LEHB105:
	call	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	movdqa	-80(%rbp), %xmm5
	movq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	-112(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
	testq	%rdi, %rdi
	je	.L1676
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1676
	call	_ZdlPv@PLT
.L1676:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE105:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	%rax, -264(%rbp)
	cmpq	%rax, %r12
	je	.L1680
	leaq	-248(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1864:
	movq	%rax, (%rsi)
	addq	$8, %r12
	addq	$8, -104(%rbp)
	cmpq	%r12, -264(%rbp)
	je	.L1863
.L1683:
	movq	(%r12), %rax
	movq	-104(%rbp), %rsi
	movq	%rax, -248(%rbp)
	cmpq	-96(%rbp), %rsi
	jne	.L1864
	movq	-288(%rbp), %rdx
	leaq	-112(%rbp), %rdi
.LEHB106:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE106:
	addq	$8, %r12
	cmpq	%r12, -264(%rbp)
	jne	.L1683
	.p2align 4,,10
	.p2align 3
.L1863:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L1680:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L1684
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1684:
	movdqa	-112(%rbp), %xmm4
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movq	-144(%rbp), %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	testq	%rdi, %rdi
	je	.L1675
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1675
	call	_ZdlPv@PLT
.L1675:
	movq	-280(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
.LEHB107:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE107:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	%rax, -264(%rbp)
	cmpq	%rax, %r12
	je	.L1693
	leaq	-112(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	%rax, (%rsi)
	addq	$8, %r12
	addq	$8, -136(%rbp)
	cmpq	%r12, -264(%rbp)
	je	.L1865
.L1696:
	movq	(%r12), %rax
	movq	-136(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-128(%rbp), %rsi
	jne	.L1866
	movq	-280(%rbp), %rdx
	leaq	-144(%rbp), %rdi
.LEHB108:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE108:
	addq	$8, %r12
	cmpq	%r12, -264(%rbp)
	jne	.L1696
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L1693:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L1697
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1697:
	movdqa	-144(%rbp), %xmm3
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movq	-176(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm3, -176(%rbp)
	testq	%rdi, %rdi
	je	.L1674
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1674
	call	_ZdlPv@PLT
.L1674:
	movq	-272(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
.LEHB109:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE109:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	%rax, -264(%rbp)
	cmpq	%rax, %r12
	je	.L1705
	leaq	-112(%rbp), %rax
	movq	%rax, -272(%rbp)
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L1868:
	movq	%rax, (%rsi)
	addq	$8, %r12
	addq	$8, -168(%rbp)
	cmpq	%r12, -264(%rbp)
	je	.L1867
.L1708:
	movq	(%r12), %rax
	movq	-168(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-160(%rbp), %rsi
	jne	.L1868
	movq	-272(%rbp), %rdx
	leaq	-176(%rbp), %rdi
.LEHB110:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE110:
	addq	$8, %r12
	cmpq	%r12, -264(%rbp)
	jne	.L1708
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L1705:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L1709
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1709:
	movdqa	-176(%rbp), %xmm2
	movq	-160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movq	-208(%rbp), %rdi
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -192(%rbp)
	movaps	%xmm2, -208(%rbp)
	testq	%rdi, %rdi
	je	.L1673
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1673
	call	_ZdlPv@PLT
.L1673:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB111:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE111:
	movq	-80(%rbp), %r12
	movq	-72(%rbp), %r14
	cmpq	%r14, %r12
	je	.L1717
	leaq	-112(%rbp), %rax
	movq	%rax, -264(%rbp)
	jmp	.L1720
	.p2align 4,,10
	.p2align 3
.L1870:
	addq	$8, %r12
	movq	%rax, (%rsi)
	addq	$8, -200(%rbp)
	cmpq	%r12, %r14
	je	.L1869
.L1720:
	movq	(%r12), %rax
	movq	-200(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-192(%rbp), %rsi
	jne	.L1870
	movq	-264(%rbp), %rdx
	leaq	-208(%rbp), %rdi
.LEHB112:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE112:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L1720
	.p2align 4,,10
	.p2align 3
.L1869:
	movq	-80(%rbp), %r14
.L1717:
	testq	%r14, %r14
	je	.L1721
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1721:
	movdqa	-208(%rbp), %xmm1
	movq	-192(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	-240(%rbp), %rdi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -224(%rbp)
	movaps	%xmm1, -240(%rbp)
	testq	%rdi, %rdi
	je	.L1672
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1672
	call	_ZdlPv@PLT
.L1672:
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
.LEHB113:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE113:
	movq	-80(%rbp), %r12
	movq	-72(%rbp), %r14
	cmpq	%r14, %r12
	je	.L1729
	leaq	-112(%rbp), %r15
	jmp	.L1732
	.p2align 4,,10
	.p2align 3
.L1872:
	addq	$8, %r12
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
	cmpq	%r12, %r14
	je	.L1871
.L1732:
	movq	(%r12), %rax
	movq	-232(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-224(%rbp), %rsi
	jne	.L1872
	leaq	-240(%rbp), %rdi
	movq	%r15, %rdx
.LEHB114:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE114:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L1732
	.p2align 4,,10
	.p2align 3
.L1871:
	movq	-80(%rbp), %r14
.L1729:
	testq	%r14, %r14
	je	.L1733
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1733:
	movq	-232(%rbp), %r14
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %r15
	movq	-240(%rbp), %r12
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%r14, %r12
	jne	.L1743
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L1874:
	movq	%rax, (%rsi)
	addq	$8, -72(%rbp)
.L1740:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L1873
.L1743:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1740
	cmpl	$8, 8(%rax)
	jne	.L1740
	movq	%rax, -112(%rbp)
	movq	-72(%rbp), %rsi
	cmpq	-64(%rbp), %rsi
	jne	.L1874
	movq	%r15, %rdx
	movq	%r13, %rdi
.LEHB115:
	call	_ZNSt6vectorIPN2v88internal6torque17GenericStructTypeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE115:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L1743
	.p2align 4,,10
	.p2align 3
.L1873:
	movq	-240(%rbp), %r14
.L1735:
	testq	%r14, %r14
	je	.L1748
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1748:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rdi
	cmpq	%rdi, %rax
	je	.L1875
	leaq	.LC32(%rip), %rcx
	subq	%rdi, %rax
	movq	%rcx, -112(%rbp)
	cmpq	$8, %rax
	jne	.L1876
	movq	(%rdi), %r12
	movl	$1, %ebx
.L1751:
	call	_ZdlPv@PLT
.L1752:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1877
	addq	$248, %rsp
	movl	%ebx, %eax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1875:
	.cfi_restore_state
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	testq	%rdi, %rdi
	je	.L1752
	jmp	.L1751
.L1877:
	call	__stack_chk_fail@PLT
.L1876:
	leaq	24(%rbx), %rcx
	leaq	-112(%rbp), %rsi
	leaq	.LC33(%rip), %rdx
	leaq	.LC34(%rip), %rdi
.LEHB116:
	call	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE116:
.L1761:
	endbr64
	movq	%rax, %r12
	jmp	.L1715
.L1757:
	endbr64
	movq	%rax, %r12
	jmp	.L1754
.L1767:
	endbr64
	movq	%rax, %r12
	jmp	.L1689
.L1768:
	endbr64
	movq	%rax, %r12
	jmp	.L1687
.L1758:
	endbr64
	movq	%rax, %r12
	jmp	.L1744
.L1760:
	endbr64
	movq	%rax, %r12
	jmp	.L1736
.L1763:
	endbr64
	movq	%rax, %r12
	jmp	.L1703
.L1764:
	endbr64
	movq	%rax, %r12
	jmp	.L1712
.L1759:
	endbr64
	movq	%rax, %r12
	jmp	.L1727
.L1765:
	endbr64
	movq	%rax, %r12
	jmp	.L1691
.L1766:
	endbr64
	movq	%rax, %r12
	jmp	.L1700
.L1762:
	endbr64
	movq	%rax, %r12
	jmp	.L1724
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE,"a",@progbits
.LLSDA6561:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6561-.LLSDACSB6561
.LLSDACSB6561:
	.uleb128 .LEHB104-.LFB6561
	.uleb128 .LEHE104-.LEHB104
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB105-.LFB6561
	.uleb128 .LEHE105-.LEHB105
	.uleb128 .L1767-.LFB6561
	.uleb128 0
	.uleb128 .LEHB106-.LFB6561
	.uleb128 .LEHE106-.LEHB106
	.uleb128 .L1768-.LFB6561
	.uleb128 0
	.uleb128 .LEHB107-.LFB6561
	.uleb128 .LEHE107-.LEHB107
	.uleb128 .L1765-.LFB6561
	.uleb128 0
	.uleb128 .LEHB108-.LFB6561
	.uleb128 .LEHE108-.LEHB108
	.uleb128 .L1766-.LFB6561
	.uleb128 0
	.uleb128 .LEHB109-.LFB6561
	.uleb128 .LEHE109-.LEHB109
	.uleb128 .L1763-.LFB6561
	.uleb128 0
	.uleb128 .LEHB110-.LFB6561
	.uleb128 .LEHE110-.LEHB110
	.uleb128 .L1764-.LFB6561
	.uleb128 0
	.uleb128 .LEHB111-.LFB6561
	.uleb128 .LEHE111-.LEHB111
	.uleb128 .L1761-.LFB6561
	.uleb128 0
	.uleb128 .LEHB112-.LFB6561
	.uleb128 .LEHE112-.LEHB112
	.uleb128 .L1762-.LFB6561
	.uleb128 0
	.uleb128 .LEHB113-.LFB6561
	.uleb128 .LEHE113-.LEHB113
	.uleb128 .L1759-.LFB6561
	.uleb128 0
	.uleb128 .LEHB114-.LFB6561
	.uleb128 .LEHE114-.LEHB114
	.uleb128 .L1760-.LFB6561
	.uleb128 0
	.uleb128 .LEHB115-.LFB6561
	.uleb128 .LEHE115-.LEHB115
	.uleb128 .L1758-.LFB6561
	.uleb128 0
	.uleb128 .LEHB116-.LFB6561
	.uleb128 .LEHE116-.LEHB116
	.uleb128 .L1757-.LFB6561
	.uleb128 0
.LLSDACSE6561:
	.section	.text._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6561
	.type	_ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE.cold, @function
_ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE.cold:
.LFSB6561:
.L1687:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1689
	call	_ZdlPv@PLT
.L1689:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1691
	call	_ZdlPv@PLT
.L1691:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1703
	call	_ZdlPv@PLT
.L1703:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1715
	call	_ZdlPv@PLT
.L1715:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1727
	call	_ZdlPv@PLT
.L1727:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1738
	call	_ZdlPv@PLT
.L1738:
	movq	%r12, %rdi
.LEHB117:
	call	_Unwind_Resume@PLT
.L1754:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1755
	call	_ZdlPv@PLT
.L1755:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L1744:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1745
	call	_ZdlPv@PLT
.L1745:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1747
	call	_ZdlPv@PLT
.L1747:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE117:
.L1736:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1727
	call	_ZdlPv@PLT
	jmp	.L1727
.L1700:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1691
	call	_ZdlPv@PLT
	jmp	.L1691
.L1712:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1703
	call	_ZdlPv@PLT
	jmp	.L1703
.L1724:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1715
	call	_ZdlPv@PLT
	jmp	.L1715
	.cfi_endproc
.LFE6561:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE
.LLSDAC6561:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6561-.LLSDACSBC6561
.LLSDACSBC6561:
	.uleb128 .LEHB117-.LCOLDB35
	.uleb128 .LEHE117-.LEHB117
	.uleb128 0
	.uleb128 0
.LLSDACSEC6561:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE
	.section	.text._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE, .-_ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE.cold, .-_ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE.cold
.LCOLDE35:
	.section	.text._ZN2v88internal6torque12Declarations26TryLookupGenericStructTypeERKNS1_13QualifiedNameE
.LHOTE35:
	.section	.rodata._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"\""
.LC37:
	.string	"cannot find \""
.LC38:
	.string	" named "
.LC39:
	.string	"there is no "
	.section	.text.unlikely._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE,"ax",@progbits
	.align 2
.LCOLDB40:
	.section	.text._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE,"ax",@progbits
.LHOTB40:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE, @function
_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE:
.LFB6560:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6560
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB118:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE118:
	pxor	%xmm0, %xmm0
	movq	(%rax), %rax
	movq	(%rax), %r15
	movaps	%xmm0, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	16(%r15), %r14
	testq	%r14, %r14
	je	.L1879
	movq	$0, -192(%rbp)
	movq	16(%r14), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L1880
	movq	$0, -160(%rbp)
	movq	16(%rax), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L1881
	movq	$0, -128(%rbp)
	movq	16(%rax), %rbx
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -144(%rbp)
	testq	%rbx, %rbx
	je	.L1882
	movq	$0, -96(%rbp)
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -112(%rbp)
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1883
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB119:
	call	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	movdqa	-80(%rbp), %xmm5
	movq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	-112(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
	testq	%rdi, %rdi
	je	.L1883
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1883
	call	_ZdlPv@PLT
.L1883:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE119:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L1887
	leaq	-248(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L1890
	.p2align 4,,10
	.p2align 3
.L2080:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -104(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2079
.L1890:
	movq	(%rbx), %rax
	movq	-104(%rbp), %rsi
	movq	%rax, -248(%rbp)
	cmpq	-96(%rbp), %rsi
	jne	.L2080
	movq	-288(%rbp), %rdx
	leaq	-112(%rbp), %rdi
.LEHB120:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE120:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L1890
	.p2align 4,,10
	.p2align 3
.L2079:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L1887:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L1891
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1891:
	movdqa	-112(%rbp), %xmm4
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movq	-144(%rbp), %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	testq	%rdi, %rdi
	je	.L1882
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1882
	call	_ZdlPv@PLT
.L1882:
	movq	-280(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB121:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE121:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L1900
	leaq	-112(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L1903
	.p2align 4,,10
	.p2align 3
.L2082:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -136(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2081
.L1903:
	movq	(%rbx), %rax
	movq	-136(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-128(%rbp), %rsi
	jne	.L2082
	movq	-280(%rbp), %rdx
	leaq	-144(%rbp), %rdi
.LEHB122:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE122:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L1903
	.p2align 4,,10
	.p2align 3
.L2081:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L1900:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L1904
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1904:
	movdqa	-144(%rbp), %xmm3
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movq	-176(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm3, -176(%rbp)
	testq	%rdi, %rdi
	je	.L1881
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1881
	call	_ZdlPv@PLT
.L1881:
	movq	-272(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB123:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE123:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L1912
	leaq	-112(%rbp), %rax
	movq	%rax, -272(%rbp)
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L2084:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -168(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2083
.L1915:
	movq	(%rbx), %rax
	movq	-168(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-160(%rbp), %rsi
	jne	.L2084
	movq	-272(%rbp), %rdx
	leaq	-176(%rbp), %rdi
.LEHB124:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE124:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L1915
	.p2align 4,,10
	.p2align 3
.L2083:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L1912:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L1916
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1916:
	movdqa	-176(%rbp), %xmm2
	movq	-160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movq	-208(%rbp), %rdi
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -192(%rbp)
	movaps	%xmm2, -208(%rbp)
	testq	%rdi, %rdi
	je	.L1880
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1880
	call	_ZdlPv@PLT
.L1880:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB125:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE125:
	movq	-80(%rbp), %rbx
	movq	-72(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1924
	leaq	-112(%rbp), %rax
	movq	%rax, -264(%rbp)
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L2086:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -200(%rbp)
	cmpq	%rbx, %r14
	je	.L2085
.L1927:
	movq	(%rbx), %rax
	movq	-200(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-192(%rbp), %rsi
	jne	.L2086
	movq	-264(%rbp), %rdx
	leaq	-208(%rbp), %rdi
.LEHB126:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE126:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L1927
	.p2align 4,,10
	.p2align 3
.L2085:
	movq	-80(%rbp), %r14
.L1924:
	testq	%r14, %r14
	je	.L1928
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1928:
	movdqa	-208(%rbp), %xmm1
	movq	-192(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	-240(%rbp), %rdi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -224(%rbp)
	movaps	%xmm1, -240(%rbp)
	testq	%rdi, %rdi
	je	.L1879
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1879
	call	_ZdlPv@PLT
.L1879:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
.LEHB127:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE127:
	movq	-80(%rbp), %rbx
	movq	-72(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1936
	leaq	-112(%rbp), %r15
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L2088:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
	cmpq	%rbx, %r14
	je	.L2087
.L1939:
	movq	(%rbx), %rax
	movq	-232(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-224(%rbp), %rsi
	jne	.L2088
	leaq	-240(%rbp), %rdi
	movq	%r15, %rdx
.LEHB128:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE128:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L1939
	.p2align 4,,10
	.p2align 3
.L2087:
	movq	-80(%rbp), %r14
.L1936:
	testq	%r14, %r14
	je	.L1940
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1940:
	movq	-232(%rbp), %r14
	movq	-240(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L2089
	movq	$0, -64(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	-112(%rbp), %r15
	movaps	%xmm0, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L1948
	cmpl	$8, 8(%rax)
	jne	.L1948
	movq	%rax, -112(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L1949
	movq	%rax, (%rsi)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -72(%rbp)
.L1948:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L1950
	movq	-80(%rbp), %rdi
	leaq	.LC32(%rip), %rax
	movq	%rax, -112(%rbp)
	cmpq	%rdi, %rsi
	je	.L2090
	subq	%rdi, %rsi
	cmpq	$8, %rsi
	jne	.L2091
	movq	(%rdi), %r12
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1878
	call	_ZdlPv@PLT
.L1878:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2092
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1949:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r13, %rdi
.LEHB129:
	call	_ZNSt6vectorIPN2v88internal6torque17GenericStructTypeESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE129:
	movq	-72(%rbp), %rsi
	jmp	.L1948
.L2091:
	leaq	-112(%rbp), %rsi
	movq	%r12, %rcx
	leaq	.LC33(%rip), %rdx
	leaq	.LC34(%rip), %rdi
.LEHB130:
	call	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_
.L2090:
	leaq	-112(%rbp), %rsi
	movq	%r12, %rcx
	leaq	.LC38(%rip), %rdx
	leaq	.LC39(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_
.LEHE130:
.L2089:
	leaq	.LC36(%rip), %rdx
	movq	%r12, %rsi
	leaq	.LC37(%rip), %rdi
.LEHB131:
	call	_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_
.LEHE131:
.L2092:
	call	__stack_chk_fail@PLT
.L1964:
	endbr64
	movq	%rax, %r12
	jmp	.L1946
.L1969:
	endbr64
	movq	%rax, %r12
	jmp	.L1910
.L1965:
	endbr64
	movq	%rax, %r12
	jmp	.L1934
.L1970:
	endbr64
	movq	%rax, %r12
	jmp	.L1919
.L1968:
	endbr64
	movq	%rax, %r12
	jmp	.L1931
.L1973:
	endbr64
	movq	%rax, %r12
	jmp	.L1896
.L1974:
	endbr64
	movq	%rax, %r12
	jmp	.L1894
.L1971:
	endbr64
	movq	%rax, %r12
	jmp	.L1898
.L1967:
	endbr64
	movq	%rax, %r12
	jmp	.L1922
.L1962:
	endbr64
	movq	%rax, %r12
	jmp	.L1958
.L1963:
	endbr64
	movq	%rax, %r12
	jmp	.L1958
.L1966:
	endbr64
	movq	%rax, %r12
	jmp	.L1943
.L1972:
	endbr64
	movq	%rax, %r12
	jmp	.L1907
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE,"a",@progbits
.LLSDA6560:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6560-.LLSDACSB6560
.LLSDACSB6560:
	.uleb128 .LEHB118-.LFB6560
	.uleb128 .LEHE118-.LEHB118
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB119-.LFB6560
	.uleb128 .LEHE119-.LEHB119
	.uleb128 .L1973-.LFB6560
	.uleb128 0
	.uleb128 .LEHB120-.LFB6560
	.uleb128 .LEHE120-.LEHB120
	.uleb128 .L1974-.LFB6560
	.uleb128 0
	.uleb128 .LEHB121-.LFB6560
	.uleb128 .LEHE121-.LEHB121
	.uleb128 .L1971-.LFB6560
	.uleb128 0
	.uleb128 .LEHB122-.LFB6560
	.uleb128 .LEHE122-.LEHB122
	.uleb128 .L1972-.LFB6560
	.uleb128 0
	.uleb128 .LEHB123-.LFB6560
	.uleb128 .LEHE123-.LEHB123
	.uleb128 .L1969-.LFB6560
	.uleb128 0
	.uleb128 .LEHB124-.LFB6560
	.uleb128 .LEHE124-.LEHB124
	.uleb128 .L1970-.LFB6560
	.uleb128 0
	.uleb128 .LEHB125-.LFB6560
	.uleb128 .LEHE125-.LEHB125
	.uleb128 .L1967-.LFB6560
	.uleb128 0
	.uleb128 .LEHB126-.LFB6560
	.uleb128 .LEHE126-.LEHB126
	.uleb128 .L1968-.LFB6560
	.uleb128 0
	.uleb128 .LEHB127-.LFB6560
	.uleb128 .LEHE127-.LEHB127
	.uleb128 .L1965-.LFB6560
	.uleb128 0
	.uleb128 .LEHB128-.LFB6560
	.uleb128 .LEHE128-.LEHB128
	.uleb128 .L1966-.LFB6560
	.uleb128 0
	.uleb128 .LEHB129-.LFB6560
	.uleb128 .LEHE129-.LEHB129
	.uleb128 .L1963-.LFB6560
	.uleb128 0
	.uleb128 .LEHB130-.LFB6560
	.uleb128 .LEHE130-.LEHB130
	.uleb128 .L1962-.LFB6560
	.uleb128 0
	.uleb128 .LEHB131-.LFB6560
	.uleb128 .LEHE131-.LEHB131
	.uleb128 .L1964-.LFB6560
	.uleb128 0
.LLSDACSE6560:
	.section	.text._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6560
	.type	_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE.cold, @function
_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE.cold:
.LFSB6560:
.L1946:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1947
	call	_ZdlPv@PLT
.L1947:
	movq	%r12, %rdi
.LEHB132:
	call	_Unwind_Resume@PLT
.L1894:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1896
	call	_ZdlPv@PLT
.L1896:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1898
	call	_ZdlPv@PLT
.L1898:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1910
	call	_ZdlPv@PLT
.L1910:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1922
	call	_ZdlPv@PLT
.L1922:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1934
	call	_ZdlPv@PLT
.L1934:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1945
	call	_ZdlPv@PLT
.L1945:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L1919:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1910
	call	_ZdlPv@PLT
	jmp	.L1910
.L1931:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1922
	call	_ZdlPv@PLT
	jmp	.L1922
.L1958:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1954
	call	_ZdlPv@PLT
.L1954:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1960
	call	_ZdlPv@PLT
.L1960:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE132:
.L1943:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1934
	call	_ZdlPv@PLT
	jmp	.L1934
.L1907:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1898
	call	_ZdlPv@PLT
	jmp	.L1898
	.cfi_endproc
.LFE6560:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE
.LLSDAC6560:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6560-.LLSDACSBC6560
.LLSDACSBC6560:
	.uleb128 .LEHB132-.LCOLDB40
	.uleb128 .LEHE132-.LEHB132
	.uleb128 0
	.uleb128 0
.LLSDACSEC6560:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE
	.section	.text._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE, .-_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE.cold, .-_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE.cold
.LCOLDE40:
	.section	.text._ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE
.LHOTE40:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE,"ax",@progbits
	.align 2
.LCOLDB41:
	.section	.text._ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE,"ax",@progbits
.LHOTB41:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE, @function
_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE:
.LFB6521:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6521
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB133:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE133:
	pxor	%xmm0, %xmm0
	movq	(%rax), %rax
	movq	(%rax), %r15
	movaps	%xmm0, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	16(%r15), %r14
	testq	%r14, %r14
	je	.L2094
	movq	$0, -192(%rbp)
	movq	16(%r14), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L2095
	movq	$0, -160(%rbp)
	movq	16(%rax), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L2096
	movq	$0, -128(%rbp)
	movq	16(%rax), %rbx
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -144(%rbp)
	testq	%rbx, %rbx
	je	.L2097
	movq	$0, -96(%rbp)
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -112(%rbp)
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2098
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB134:
	call	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	movdqa	-80(%rbp), %xmm5
	movq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	-112(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
	testq	%rdi, %rdi
	je	.L2098
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2098
	call	_ZdlPv@PLT
.L2098:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE134:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L2102
	leaq	-248(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L2105
	.p2align 4,,10
	.p2align 3
.L2295:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -104(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2294
.L2105:
	movq	(%rbx), %rax
	movq	-104(%rbp), %rsi
	movq	%rax, -248(%rbp)
	cmpq	-96(%rbp), %rsi
	jne	.L2295
	movq	-288(%rbp), %rdx
	leaq	-112(%rbp), %rdi
.LEHB135:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE135:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L2105
	.p2align 4,,10
	.p2align 3
.L2294:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L2102:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L2106
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2106:
	movdqa	-112(%rbp), %xmm4
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movq	-144(%rbp), %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	testq	%rdi, %rdi
	je	.L2097
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2097
	call	_ZdlPv@PLT
.L2097:
	movq	-280(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB136:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE136:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L2115
	leaq	-112(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L2297:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -136(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2296
.L2118:
	movq	(%rbx), %rax
	movq	-136(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-128(%rbp), %rsi
	jne	.L2297
	movq	-280(%rbp), %rdx
	leaq	-144(%rbp), %rdi
.LEHB137:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE137:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L2118
	.p2align 4,,10
	.p2align 3
.L2296:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L2115:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L2119
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2119:
	movdqa	-144(%rbp), %xmm3
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movq	-176(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm3, -176(%rbp)
	testq	%rdi, %rdi
	je	.L2096
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2096
	call	_ZdlPv@PLT
.L2096:
	movq	-272(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB138:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE138:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L2127
	leaq	-112(%rbp), %rax
	movq	%rax, -272(%rbp)
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2299:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -168(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2298
.L2130:
	movq	(%rbx), %rax
	movq	-168(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-160(%rbp), %rsi
	jne	.L2299
	movq	-272(%rbp), %rdx
	leaq	-176(%rbp), %rdi
.LEHB139:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE139:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L2130
	.p2align 4,,10
	.p2align 3
.L2298:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L2127:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L2131
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2131:
	movdqa	-176(%rbp), %xmm2
	movq	-160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movq	-208(%rbp), %rdi
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -192(%rbp)
	movaps	%xmm2, -208(%rbp)
	testq	%rdi, %rdi
	je	.L2095
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2095
	call	_ZdlPv@PLT
.L2095:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB140:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE140:
	movq	-80(%rbp), %rbx
	movq	-72(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2139
	leaq	-112(%rbp), %rax
	movq	%rax, -264(%rbp)
	jmp	.L2142
	.p2align 4,,10
	.p2align 3
.L2301:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -200(%rbp)
	cmpq	%rbx, %r14
	je	.L2300
.L2142:
	movq	(%rbx), %rax
	movq	-200(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-192(%rbp), %rsi
	jne	.L2301
	movq	-264(%rbp), %rdx
	leaq	-208(%rbp), %rdi
.LEHB141:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE141:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2142
	.p2align 4,,10
	.p2align 3
.L2300:
	movq	-80(%rbp), %r14
.L2139:
	testq	%r14, %r14
	je	.L2143
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2143:
	movdqa	-208(%rbp), %xmm1
	movq	-192(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	-240(%rbp), %rdi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -224(%rbp)
	movaps	%xmm1, -240(%rbp)
	testq	%rdi, %rdi
	je	.L2094
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2094
	call	_ZdlPv@PLT
.L2094:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
.LEHB142:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE142:
	movq	-80(%rbp), %rbx
	movq	-72(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2151
	leaq	-112(%rbp), %r15
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2303:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
	cmpq	%rbx, %r14
	je	.L2302
.L2154:
	movq	(%rbx), %rax
	movq	-232(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-224(%rbp), %rsi
	jne	.L2303
	leaq	-240(%rbp), %rdi
	movq	%r15, %rdx
.LEHB143:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE143:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2154
	.p2align 4,,10
	.p2align 3
.L2302:
	movq	-80(%rbp), %r14
.L2151:
	testq	%r14, %r14
	je	.L2155
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2155:
	movq	-232(%rbp), %r14
	movq	-240(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L2304
	movq	$0, -64(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	-112(%rbp), %r15
	movaps	%xmm0, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L2165:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L2163
	cmpl	$9, 8(%rax)
	jne	.L2163
	movq	%rax, -112(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L2164
	movq	%rax, (%rsi)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -72(%rbp)
.L2163:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2165
	movq	-80(%rbp), %rdi
	leaq	.LC26(%rip), %rax
	movq	%rax, -112(%rbp)
	cmpq	%rdi, %rsi
	je	.L2305
	subq	%rdi, %rsi
	cmpq	$8, %rsi
	jne	.L2306
	movq	(%rdi), %r12
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2093
	call	_ZdlPv@PLT
.L2093:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2307
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2164:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r13, %rdi
.LEHB144:
	call	_ZNSt6vectorIPN2v88internal6torque9TypeAliasESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE144:
	movq	-72(%rbp), %rsi
	jmp	.L2163
.L2306:
	leaq	-112(%rbp), %rsi
	movq	%r12, %rcx
	leaq	.LC33(%rip), %rdx
	leaq	.LC34(%rip), %rdi
.LEHB145:
	call	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_
.L2305:
	leaq	-112(%rbp), %rsi
	movq	%r12, %rcx
	leaq	.LC38(%rip), %rdx
	leaq	.LC39(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_
.LEHE145:
.L2304:
	leaq	.LC36(%rip), %rdx
	movq	%r12, %rsi
	leaq	.LC37(%rip), %rdi
.LEHB146:
	call	_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_
.LEHE146:
.L2307:
	call	__stack_chk_fail@PLT
.L2179:
	endbr64
	movq	%rax, %r12
	jmp	.L2161
.L2184:
	endbr64
	movq	%rax, %r12
	jmp	.L2125
.L2180:
	endbr64
	movq	%rax, %r12
	jmp	.L2149
.L2185:
	endbr64
	movq	%rax, %r12
	jmp	.L2134
.L2183:
	endbr64
	movq	%rax, %r12
	jmp	.L2146
.L2188:
	endbr64
	movq	%rax, %r12
	jmp	.L2111
.L2189:
	endbr64
	movq	%rax, %r12
	jmp	.L2109
.L2186:
	endbr64
	movq	%rax, %r12
	jmp	.L2113
.L2182:
	endbr64
	movq	%rax, %r12
	jmp	.L2137
.L2177:
	endbr64
	movq	%rax, %r12
	jmp	.L2173
.L2178:
	endbr64
	movq	%rax, %r12
	jmp	.L2173
.L2181:
	endbr64
	movq	%rax, %r12
	jmp	.L2158
.L2187:
	endbr64
	movq	%rax, %r12
	jmp	.L2122
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE,"a",@progbits
.LLSDA6521:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6521-.LLSDACSB6521
.LLSDACSB6521:
	.uleb128 .LEHB133-.LFB6521
	.uleb128 .LEHE133-.LEHB133
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB134-.LFB6521
	.uleb128 .LEHE134-.LEHB134
	.uleb128 .L2188-.LFB6521
	.uleb128 0
	.uleb128 .LEHB135-.LFB6521
	.uleb128 .LEHE135-.LEHB135
	.uleb128 .L2189-.LFB6521
	.uleb128 0
	.uleb128 .LEHB136-.LFB6521
	.uleb128 .LEHE136-.LEHB136
	.uleb128 .L2186-.LFB6521
	.uleb128 0
	.uleb128 .LEHB137-.LFB6521
	.uleb128 .LEHE137-.LEHB137
	.uleb128 .L2187-.LFB6521
	.uleb128 0
	.uleb128 .LEHB138-.LFB6521
	.uleb128 .LEHE138-.LEHB138
	.uleb128 .L2184-.LFB6521
	.uleb128 0
	.uleb128 .LEHB139-.LFB6521
	.uleb128 .LEHE139-.LEHB139
	.uleb128 .L2185-.LFB6521
	.uleb128 0
	.uleb128 .LEHB140-.LFB6521
	.uleb128 .LEHE140-.LEHB140
	.uleb128 .L2182-.LFB6521
	.uleb128 0
	.uleb128 .LEHB141-.LFB6521
	.uleb128 .LEHE141-.LEHB141
	.uleb128 .L2183-.LFB6521
	.uleb128 0
	.uleb128 .LEHB142-.LFB6521
	.uleb128 .LEHE142-.LEHB142
	.uleb128 .L2180-.LFB6521
	.uleb128 0
	.uleb128 .LEHB143-.LFB6521
	.uleb128 .LEHE143-.LEHB143
	.uleb128 .L2181-.LFB6521
	.uleb128 0
	.uleb128 .LEHB144-.LFB6521
	.uleb128 .LEHE144-.LEHB144
	.uleb128 .L2178-.LFB6521
	.uleb128 0
	.uleb128 .LEHB145-.LFB6521
	.uleb128 .LEHE145-.LEHB145
	.uleb128 .L2177-.LFB6521
	.uleb128 0
	.uleb128 .LEHB146-.LFB6521
	.uleb128 .LEHE146-.LEHB146
	.uleb128 .L2179-.LFB6521
	.uleb128 0
.LLSDACSE6521:
	.section	.text._ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6521
	.type	_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE.cold, @function
_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE.cold:
.LFSB6521:
.L2161:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2162
	call	_ZdlPv@PLT
.L2162:
	movq	%r12, %rdi
.LEHB147:
	call	_Unwind_Resume@PLT
.L2109:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2111
	call	_ZdlPv@PLT
.L2111:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2113
	call	_ZdlPv@PLT
.L2113:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2125
	call	_ZdlPv@PLT
.L2125:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2137
	call	_ZdlPv@PLT
.L2137:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2149
	call	_ZdlPv@PLT
.L2149:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2160
	call	_ZdlPv@PLT
.L2160:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L2134:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2125
	call	_ZdlPv@PLT
	jmp	.L2125
.L2146:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2137
	call	_ZdlPv@PLT
	jmp	.L2137
.L2173:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2169
	call	_ZdlPv@PLT
.L2169:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2175
	call	_ZdlPv@PLT
.L2175:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE147:
.L2158:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2149
	call	_ZdlPv@PLT
	jmp	.L2149
.L2122:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2113
	call	_ZdlPv@PLT
	jmp	.L2113
	.cfi_endproc
.LFE6521:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
.LLSDAC6521:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6521-.LLSDACSBC6521
.LLSDACSBC6521:
	.uleb128 .LEHB147-.LCOLDB41
	.uleb128 .LEHE147-.LEHB147
	.uleb128 0
	.uleb128 0
.LLSDACSEC6521:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
	.section	.text._ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE, .-_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE.cold, .-_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE.cold
.LCOLDE41:
	.section	.text._ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
.LHOTE41:
	.section	.text._ZN2v88internal6torque12Declarations10LookupTypeERKNS1_13QualifiedNameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations10LookupTypeERKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torque12Declarations10LookupTypeERKNS1_13QualifiedNameE, @function
_ZN2v88internal6torque12Declarations10LookupTypeERKNS1_13QualifiedNameE:
.LFB6522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
	cmpb	$0, 88(%rax)
	je	.L2309
	movq	96(%rax), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2309:
	.cfi_restore_state
	movq	%rax, %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal6torque9TypeAlias7ResolveEv@PLT
	.cfi_endproc
.LFE6522:
	.size	_ZN2v88internal6torque12Declarations10LookupTypeERKNS1_13QualifiedNameE, .-_ZN2v88internal6torque12Declarations10LookupTypeERKNS1_13QualifiedNameE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE,"ax",@progbits
	.align 2
.LCOLDB42:
	.section	.text._ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE,"ax",@progbits
.LHOTB42:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE
	.type	_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE, @function
_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE:
.LFB6523:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6523
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	32(%rdi), %r13
	movq	40(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -176(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L2314
	testq	%r13, %r13
	je	.L2349
.L2314:
	movq	%r12, -208(%rbp)
	cmpq	$15, %r12
	ja	.L2350
	cmpq	$1, %r12
	jne	.L2317
	movzbl	0(%r13), %eax
	movb	%al, -160(%rbp)
	movq	%r14, %rax
.L2318:
	movq	%r12, -168(%rbp)
	movb	$0, (%rax,%r12)
	movq	-176(%rbp), %rax
	cmpq	%r14, %rax
	je	.L2351
	movq	-160(%rbp), %rcx
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %r12
	leaq	-128(%rbp), %rsi
	movq	-168(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%r14, -176(%rbp)
	movq	$0, -168(%rbp)
	movb	$0, -160(%rbp)
	movq	$0, -96(%rbp)
	movq	%r12, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	cmpq	%rsi, %rax
	je	.L2320
	movq	%rax, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L2322:
	leaq	-112(%rbp), %r13
	movq	%rdx, -80(%rbp)
	movq	%r13, %rdi
.LEHB148:
	call	_ZN2v88internal6torque12Declarations15LookupTypeAliasERKNS1_13QualifiedNameE
.LEHE148:
	movq	-88(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%r12, %rdi
	je	.L2323
	call	_ZdlPv@PLT
.L2323:
	movq	-104(%rbp), %r15
	movq	-112(%rbp), %r12
	cmpq	%r12, %r15
	je	.L2324
	.p2align 4,,10
	.p2align 3
.L2328:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2325
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r15, %r12
	jne	.L2328
.L2326:
	movq	-112(%rbp), %r12
.L2324:
	testq	%r12, %r12
	je	.L2329
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2329:
	movq	-176(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2330
	call	_ZdlPv@PLT
.L2330:
.LEHB149:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	cmpb	$0, (%rax)
	jne	.L2352
.L2331:
	cmpb	$0, 88(%r13)
	je	.L2332
	movq	96(%r13), %rax
.L2313:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2353
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2317:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L2354
	movq	%r14, %rax
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2325:
	addq	$32, %r12
	cmpq	%r12, %r15
	jne	.L2328
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2350:
	leaq	-176(%rbp), %rdi
	leaq	-208(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -176(%rbp)
	movq	%rax, %rdi
	movq	-208(%rbp), %rax
	movq	%rax, -160(%rbp)
.L2316:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-208(%rbp), %r12
	movq	-176(%rbp), %rax
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2332:
	movq	%r13, %rdi
	call	_ZNK2v88internal6torque9TypeAlias7ResolveEv@PLT
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2352:
	movdqu	108(%r13), %xmm0
	subq	$48, %rsp
	movdqu	12(%rbx), %xmm1
	movaps	%xmm0, -208(%rbp)
	movl	124(%r13), %eax
	movups	%xmm0, 24(%rsp)
	movl	%eax, 40(%rsp)
	movl	%eax, -192(%rbp)
	movl	28(%rbx), %eax
	movups	%xmm1, (%rsp)
	movl	%eax, 16(%rsp)
	call	_ZN2v88internal6torque18LanguageServerData13AddDefinitionENS1_14SourcePositionES3_@PLT
	addq	$48, %rsp
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2351:
	leaq	-72(%rbp), %r12
	movdqa	-160(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	-168(%rbp), %rdx
	movb	$0, -160(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -96(%rbp)
	movq	%r12, -88(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
.L2320:
	movdqa	-128(%rbp), %xmm3
	movups	%xmm3, -72(%rbp)
	jmp	.L2322
.L2349:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE149:
.L2353:
	call	__stack_chk_fail@PLT
.L2354:
	movq	%r14, %rdi
	jmp	.L2316
.L2338:
	endbr64
	movq	%rax, %r12
	jmp	.L2334
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE,"a",@progbits
.LLSDA6523:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6523-.LLSDACSB6523
.LLSDACSB6523:
	.uleb128 .LEHB148-.LFB6523
	.uleb128 .LEHE148-.LEHB148
	.uleb128 .L2338-.LFB6523
	.uleb128 0
	.uleb128 .LEHB149-.LFB6523
	.uleb128 .LEHE149-.LEHB149
	.uleb128 0
	.uleb128 0
.LLSDACSE6523:
	.section	.text._ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6523
	.type	_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE.cold, @function
_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE.cold:
.LFSB6523:
.L2334:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r13, %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	-176(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2335
	call	_ZdlPv@PLT
.L2335:
	movq	%r12, %rdi
.LEHB150:
	call	_Unwind_Resume@PLT
.LEHE150:
	.cfi_endproc
.LFE6523:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE
.LLSDAC6523:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6523-.LLSDACSBC6523
.LLSDACSBC6523:
	.uleb128 .LEHB150-.LCOLDB42
	.uleb128 .LEHE150-.LEHB150
	.uleb128 0
	.uleb128 0
.LLSDACSEC6523:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE
	.section	.text._ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE
	.size	_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE, .-_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE
	.size	_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE.cold, .-_ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE.cold
.LCOLDE42:
	.section	.text._ZN2v88internal6torque12Declarations10LookupTypeEPKNS1_10IdentifierE
.LHOTE42:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE,"ax",@progbits
	.align 2
.LCOLDB43:
	.section	.text._ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE,"ax",@progbits
.LHOTB43:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE, @function
_ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE:
.LFB6559:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6559
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB151:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE151:
	pxor	%xmm0, %xmm0
	movq	(%rax), %rax
	movq	(%rax), %r15
	movaps	%xmm0, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	16(%r15), %r14
	testq	%r14, %r14
	je	.L2356
	movq	$0, -192(%rbp)
	movq	16(%r14), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L2357
	movq	$0, -160(%rbp)
	movq	16(%rax), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L2358
	movq	$0, -128(%rbp)
	movq	16(%rax), %rbx
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -144(%rbp)
	testq	%rbx, %rbx
	je	.L2359
	movq	$0, -96(%rbp)
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -112(%rbp)
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2360
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB152:
	call	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	movdqa	-80(%rbp), %xmm5
	movq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	-112(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
	testq	%rdi, %rdi
	je	.L2360
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2360
	call	_ZdlPv@PLT
.L2360:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE152:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L2364
	leaq	-248(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L2367
	.p2align 4,,10
	.p2align 3
.L2557:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -104(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2556
.L2367:
	movq	(%rbx), %rax
	movq	-104(%rbp), %rsi
	movq	%rax, -248(%rbp)
	cmpq	-96(%rbp), %rsi
	jne	.L2557
	movq	-288(%rbp), %rdx
	leaq	-112(%rbp), %rdi
.LEHB153:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE153:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L2367
	.p2align 4,,10
	.p2align 3
.L2556:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L2364:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L2368
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2368:
	movdqa	-112(%rbp), %xmm4
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movq	-144(%rbp), %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	testq	%rdi, %rdi
	je	.L2359
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2359
	call	_ZdlPv@PLT
.L2359:
	movq	-280(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB154:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE154:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L2377
	leaq	-112(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2559:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -136(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2558
.L2380:
	movq	(%rbx), %rax
	movq	-136(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-128(%rbp), %rsi
	jne	.L2559
	movq	-280(%rbp), %rdx
	leaq	-144(%rbp), %rdi
.LEHB155:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE155:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L2380
	.p2align 4,,10
	.p2align 3
.L2558:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L2377:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L2381
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2381:
	movdqa	-144(%rbp), %xmm3
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movq	-176(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm3, -176(%rbp)
	testq	%rdi, %rdi
	je	.L2358
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2358
	call	_ZdlPv@PLT
.L2358:
	movq	-272(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB156:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE156:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L2389
	leaq	-112(%rbp), %rax
	movq	%rax, -272(%rbp)
	jmp	.L2392
	.p2align 4,,10
	.p2align 3
.L2561:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -168(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2560
.L2392:
	movq	(%rbx), %rax
	movq	-168(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-160(%rbp), %rsi
	jne	.L2561
	movq	-272(%rbp), %rdx
	leaq	-176(%rbp), %rdi
.LEHB157:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE157:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L2392
	.p2align 4,,10
	.p2align 3
.L2560:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L2389:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L2393
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2393:
	movdqa	-176(%rbp), %xmm2
	movq	-160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movq	-208(%rbp), %rdi
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -192(%rbp)
	movaps	%xmm2, -208(%rbp)
	testq	%rdi, %rdi
	je	.L2357
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2357
	call	_ZdlPv@PLT
.L2357:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB158:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE158:
	movq	-80(%rbp), %rbx
	movq	-72(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2401
	leaq	-112(%rbp), %rax
	movq	%rax, -264(%rbp)
	jmp	.L2404
	.p2align 4,,10
	.p2align 3
.L2563:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -200(%rbp)
	cmpq	%rbx, %r14
	je	.L2562
.L2404:
	movq	(%rbx), %rax
	movq	-200(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-192(%rbp), %rsi
	jne	.L2563
	movq	-264(%rbp), %rdx
	leaq	-208(%rbp), %rdi
.LEHB159:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE159:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2404
	.p2align 4,,10
	.p2align 3
.L2562:
	movq	-80(%rbp), %r14
.L2401:
	testq	%r14, %r14
	je	.L2405
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2405:
	movdqa	-208(%rbp), %xmm1
	movq	-192(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	-240(%rbp), %rdi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -224(%rbp)
	movaps	%xmm1, -240(%rbp)
	testq	%rdi, %rdi
	je	.L2356
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2356
	call	_ZdlPv@PLT
.L2356:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
.LEHB160:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE160:
	movq	-80(%rbp), %rbx
	movq	-72(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2413
	leaq	-112(%rbp), %r15
	jmp	.L2416
	.p2align 4,,10
	.p2align 3
.L2565:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
	cmpq	%rbx, %r14
	je	.L2564
.L2416:
	movq	(%rbx), %rax
	movq	-232(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-224(%rbp), %rsi
	jne	.L2565
	leaq	-240(%rbp), %rdi
	movq	%r15, %rdx
.LEHB161:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE161:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2416
	.p2align 4,,10
	.p2align 3
.L2564:
	movq	-80(%rbp), %r14
.L2413:
	testq	%r14, %r14
	je	.L2417
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2417:
	movq	-232(%rbp), %r14
	movq	-240(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L2566
	movq	$0, -64(%rbp)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	-112(%rbp), %r15
	movaps	%xmm0, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L2427:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L2425
	cmpl	$7, 8(%rax)
	jne	.L2425
	movq	%rax, -112(%rbp)
	cmpq	-64(%rbp), %rsi
	je	.L2426
	movq	%rax, (%rsi)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -72(%rbp)
.L2425:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2427
	movq	-80(%rbp), %rdi
	leaq	.LC13(%rip), %rax
	movq	%rax, -112(%rbp)
	cmpq	%rdi, %rsi
	je	.L2567
	subq	%rdi, %rsi
	cmpq	$8, %rsi
	jne	.L2568
	movq	(%rdi), %r12
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2355
	call	_ZdlPv@PLT
.L2355:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2569
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2426:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r13, %rdi
.LEHB162:
	call	_ZNSt6vectorIPN2v88internal6torque7GenericESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE162:
	movq	-72(%rbp), %rsi
	jmp	.L2425
.L2568:
	leaq	-112(%rbp), %rsi
	movq	%r12, %rcx
	leaq	.LC33(%rip), %rdx
	leaq	.LC34(%rip), %rdi
.LEHB163:
	call	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_
.L2567:
	leaq	-112(%rbp), %rsi
	movq	%r12, %rcx
	leaq	.LC38(%rip), %rdx
	leaq	.LC39(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_
.LEHE163:
.L2566:
	leaq	.LC36(%rip), %rdx
	movq	%r12, %rsi
	leaq	.LC37(%rip), %rdi
.LEHB164:
	call	_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_
.LEHE164:
.L2569:
	call	__stack_chk_fail@PLT
.L2441:
	endbr64
	movq	%rax, %r12
	jmp	.L2423
.L2446:
	endbr64
	movq	%rax, %r12
	jmp	.L2387
.L2442:
	endbr64
	movq	%rax, %r12
	jmp	.L2411
.L2447:
	endbr64
	movq	%rax, %r12
	jmp	.L2396
.L2445:
	endbr64
	movq	%rax, %r12
	jmp	.L2408
.L2450:
	endbr64
	movq	%rax, %r12
	jmp	.L2373
.L2451:
	endbr64
	movq	%rax, %r12
	jmp	.L2371
.L2448:
	endbr64
	movq	%rax, %r12
	jmp	.L2375
.L2444:
	endbr64
	movq	%rax, %r12
	jmp	.L2399
.L2439:
	endbr64
	movq	%rax, %r12
	jmp	.L2435
.L2440:
	endbr64
	movq	%rax, %r12
	jmp	.L2435
.L2443:
	endbr64
	movq	%rax, %r12
	jmp	.L2420
.L2449:
	endbr64
	movq	%rax, %r12
	jmp	.L2384
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE,"a",@progbits
.LLSDA6559:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6559-.LLSDACSB6559
.LLSDACSB6559:
	.uleb128 .LEHB151-.LFB6559
	.uleb128 .LEHE151-.LEHB151
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB152-.LFB6559
	.uleb128 .LEHE152-.LEHB152
	.uleb128 .L2450-.LFB6559
	.uleb128 0
	.uleb128 .LEHB153-.LFB6559
	.uleb128 .LEHE153-.LEHB153
	.uleb128 .L2451-.LFB6559
	.uleb128 0
	.uleb128 .LEHB154-.LFB6559
	.uleb128 .LEHE154-.LEHB154
	.uleb128 .L2448-.LFB6559
	.uleb128 0
	.uleb128 .LEHB155-.LFB6559
	.uleb128 .LEHE155-.LEHB155
	.uleb128 .L2449-.LFB6559
	.uleb128 0
	.uleb128 .LEHB156-.LFB6559
	.uleb128 .LEHE156-.LEHB156
	.uleb128 .L2446-.LFB6559
	.uleb128 0
	.uleb128 .LEHB157-.LFB6559
	.uleb128 .LEHE157-.LEHB157
	.uleb128 .L2447-.LFB6559
	.uleb128 0
	.uleb128 .LEHB158-.LFB6559
	.uleb128 .LEHE158-.LEHB158
	.uleb128 .L2444-.LFB6559
	.uleb128 0
	.uleb128 .LEHB159-.LFB6559
	.uleb128 .LEHE159-.LEHB159
	.uleb128 .L2445-.LFB6559
	.uleb128 0
	.uleb128 .LEHB160-.LFB6559
	.uleb128 .LEHE160-.LEHB160
	.uleb128 .L2442-.LFB6559
	.uleb128 0
	.uleb128 .LEHB161-.LFB6559
	.uleb128 .LEHE161-.LEHB161
	.uleb128 .L2443-.LFB6559
	.uleb128 0
	.uleb128 .LEHB162-.LFB6559
	.uleb128 .LEHE162-.LEHB162
	.uleb128 .L2440-.LFB6559
	.uleb128 0
	.uleb128 .LEHB163-.LFB6559
	.uleb128 .LEHE163-.LEHB163
	.uleb128 .L2439-.LFB6559
	.uleb128 0
	.uleb128 .LEHB164-.LFB6559
	.uleb128 .LEHE164-.LEHB164
	.uleb128 .L2441-.LFB6559
	.uleb128 0
.LLSDACSE6559:
	.section	.text._ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6559
	.type	_ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE.cold, @function
_ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE.cold:
.LFSB6559:
.L2423:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2424
	call	_ZdlPv@PLT
.L2424:
	movq	%r12, %rdi
.LEHB165:
	call	_Unwind_Resume@PLT
.L2371:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2373
	call	_ZdlPv@PLT
.L2373:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2375
	call	_ZdlPv@PLT
.L2375:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2387
	call	_ZdlPv@PLT
.L2387:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2399
	call	_ZdlPv@PLT
.L2399:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2411
	call	_ZdlPv@PLT
.L2411:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2422
	call	_ZdlPv@PLT
.L2422:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L2396:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2387
	call	_ZdlPv@PLT
	jmp	.L2387
.L2408:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2399
	call	_ZdlPv@PLT
	jmp	.L2399
.L2435:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2431
	call	_ZdlPv@PLT
.L2431:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2437
	call	_ZdlPv@PLT
.L2437:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE165:
.L2420:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2411
	call	_ZdlPv@PLT
	jmp	.L2411
.L2384:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2375
	call	_ZdlPv@PLT
	jmp	.L2375
	.cfi_endproc
.LFE6559:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE
.LLSDAC6559:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6559-.LLSDACSBC6559
.LLSDACSBC6559:
	.uleb128 .LEHB165-.LCOLDB43
	.uleb128 .LEHE165-.LEHB165
	.uleb128 0
	.uleb128 0
.LLSDACSEC6559:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE
	.section	.text._ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE, .-_ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE.cold, .-_ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE.cold
.LCOLDE43:
	.section	.text._ZN2v88internal6torque12Declarations19LookupUniqueGenericERKNS1_13QualifiedNameE
.LHOTE43:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE,"ax",@progbits
	.align 2
.LCOLDB44:
	.section	.text._ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE,"ax",@progbits
.LHOTB44:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE, @function
_ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE:
.LFB6528:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6528
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB166:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE166:
	pxor	%xmm0, %xmm0
	movq	(%rax), %rax
	movq	(%rax), %r15
	movaps	%xmm0, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	16(%r15), %r14
	testq	%r14, %r14
	je	.L2571
	movq	$0, -192(%rbp)
	movq	16(%r14), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L2572
	movq	$0, -160(%rbp)
	movq	16(%rax), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L2573
	movq	$0, -128(%rbp)
	movq	16(%rax), %rbx
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -144(%rbp)
	testq	%rbx, %rbx
	je	.L2574
	movq	$0, -96(%rbp)
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -112(%rbp)
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2575
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB167:
	call	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	movdqa	-80(%rbp), %xmm5
	movq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	-112(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
	testq	%rdi, %rdi
	je	.L2575
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2575
	call	_ZdlPv@PLT
.L2575:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE167:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L2579
	leaq	-248(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L2582
	.p2align 4,,10
	.p2align 3
.L2756:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -104(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2755
.L2582:
	movq	(%rbx), %rax
	movq	-104(%rbp), %rsi
	movq	%rax, -248(%rbp)
	cmpq	-96(%rbp), %rsi
	jne	.L2756
	movq	-288(%rbp), %rdx
	leaq	-112(%rbp), %rdi
.LEHB168:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE168:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L2582
	.p2align 4,,10
	.p2align 3
.L2755:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L2579:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L2583
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2583:
	movdqa	-112(%rbp), %xmm4
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movq	-144(%rbp), %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	testq	%rdi, %rdi
	je	.L2574
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2574
	call	_ZdlPv@PLT
.L2574:
	movq	-280(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB169:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE169:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L2592
	leaq	-112(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L2595
	.p2align 4,,10
	.p2align 3
.L2758:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -136(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2757
.L2595:
	movq	(%rbx), %rax
	movq	-136(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-128(%rbp), %rsi
	jne	.L2758
	movq	-280(%rbp), %rdx
	leaq	-144(%rbp), %rdi
.LEHB170:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE170:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L2595
	.p2align 4,,10
	.p2align 3
.L2757:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L2592:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L2596
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2596:
	movdqa	-144(%rbp), %xmm3
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movq	-176(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm3, -176(%rbp)
	testq	%rdi, %rdi
	je	.L2573
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2573
	call	_ZdlPv@PLT
.L2573:
	movq	-272(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
.LEHB171:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE171:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rbx
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rbx
	je	.L2604
	leaq	-112(%rbp), %rax
	movq	%rax, -272(%rbp)
	jmp	.L2607
	.p2align 4,,10
	.p2align 3
.L2760:
	movq	%rax, (%rsi)
	addq	$8, %rbx
	addq	$8, -168(%rbp)
	cmpq	%rbx, -264(%rbp)
	je	.L2759
.L2607:
	movq	(%rbx), %rax
	movq	-168(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-160(%rbp), %rsi
	jne	.L2760
	movq	-272(%rbp), %rdx
	leaq	-176(%rbp), %rdi
.LEHB172:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE172:
	addq	$8, %rbx
	cmpq	%rbx, -264(%rbp)
	jne	.L2607
	.p2align 4,,10
	.p2align 3
.L2759:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L2604:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L2608
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2608:
	movdqa	-176(%rbp), %xmm2
	movq	-160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movq	-208(%rbp), %rdi
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -192(%rbp)
	movaps	%xmm2, -208(%rbp)
	testq	%rdi, %rdi
	je	.L2572
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2572
	call	_ZdlPv@PLT
.L2572:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB173:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE173:
	movq	-80(%rbp), %rbx
	movq	-72(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2616
	leaq	-112(%rbp), %rax
	movq	%rax, -264(%rbp)
	jmp	.L2619
	.p2align 4,,10
	.p2align 3
.L2762:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -200(%rbp)
	cmpq	%rbx, %r14
	je	.L2761
.L2619:
	movq	(%rbx), %rax
	movq	-200(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-192(%rbp), %rsi
	jne	.L2762
	movq	-264(%rbp), %rdx
	leaq	-208(%rbp), %rdi
.LEHB174:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE174:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2619
	.p2align 4,,10
	.p2align 3
.L2761:
	movq	-80(%rbp), %r14
.L2616:
	testq	%r14, %r14
	je	.L2620
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2620:
	movdqa	-208(%rbp), %xmm1
	movq	-192(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	-240(%rbp), %rdi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -224(%rbp)
	movaps	%xmm1, -240(%rbp)
	testq	%rdi, %rdi
	je	.L2571
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2571
	call	_ZdlPv@PLT
.L2571:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
.LEHB175:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE175:
	movq	-80(%rbp), %rbx
	movq	-72(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2628
	leaq	-112(%rbp), %r15
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2764:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
	cmpq	%rbx, %r14
	je	.L2763
.L2631:
	movq	(%rbx), %rax
	movq	-232(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-224(%rbp), %rsi
	jne	.L2764
	leaq	-240(%rbp), %rdi
	movq	%r15, %rdx
.LEHB176:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE176:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2631
	.p2align 4,,10
	.p2align 3
.L2763:
	movq	-80(%rbp), %r14
.L2628:
	testq	%r14, %r14
	je	.L2632
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2632:
	movq	-240(%rbp), %rax
	cmpq	%rax, -232(%rbp)
	je	.L2765
	leaq	-240(%rbp), %rsi
	movq	%r13, %rdi
.LEHB177:
	call	_ZN2v88internal6torque17FilterDeclarablesINS1_5ValueEEESt6vectorIPT_SaIS6_EES4_IPNS1_10DeclarableESaISA_EE
.LEHE177:
	leaq	.LC3(%rip), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, -112(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rdi, %rax
	je	.L2766
	subq	%rdi, %rax
	cmpq	$8, %rax
	jne	.L2767
	movq	(%rdi), %r12
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2570
	call	_ZdlPv@PLT
.L2570:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2768
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2767:
	.cfi_restore_state
	leaq	-112(%rbp), %rsi
	movq	%r12, %rcx
	leaq	.LC33(%rip), %rdx
	leaq	.LC34(%rip), %rdi
.LEHB178:
	call	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNS1_13QualifiedNameEEEEvDpOT_
.L2766:
	leaq	-112(%rbp), %rsi
	movq	%r12, %rcx
	leaq	.LC38(%rip), %rdx
	leaq	.LC39(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNS1_13QualifiedNameEEEEvDpOT_
.LEHE178:
.L2765:
	leaq	.LC36(%rip), %rdx
	movq	%r12, %rsi
	leaq	.LC37(%rip), %rdi
.LEHB179:
	call	_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_
.LEHE179:
.L2768:
	call	__stack_chk_fail@PLT
.L2650:
	endbr64
	movq	%rax, %r12
	jmp	.L2638
.L2655:
	endbr64
	movq	%rax, %r12
	jmp	.L2602
.L2651:
	endbr64
	movq	%rax, %r12
	jmp	.L2626
.L2656:
	endbr64
	movq	%rax, %r12
	jmp	.L2611
.L2654:
	endbr64
	movq	%rax, %r12
	jmp	.L2623
.L2659:
	endbr64
	movq	%rax, %r12
	jmp	.L2588
.L2660:
	endbr64
	movq	%rax, %r12
	jmp	.L2586
.L2657:
	endbr64
	movq	%rax, %r12
	jmp	.L2590
.L2653:
	endbr64
	movq	%rax, %r12
	jmp	.L2614
.L2649:
	endbr64
	movq	%rax, %r12
	jmp	.L2643
.L2648:
	endbr64
	movq	%rax, %r12
	jmp	.L2645
.L2652:
	endbr64
	movq	%rax, %r12
	jmp	.L2635
.L2658:
	endbr64
	movq	%rax, %r12
	jmp	.L2599
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE,"a",@progbits
.LLSDA6528:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6528-.LLSDACSB6528
.LLSDACSB6528:
	.uleb128 .LEHB166-.LFB6528
	.uleb128 .LEHE166-.LEHB166
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB167-.LFB6528
	.uleb128 .LEHE167-.LEHB167
	.uleb128 .L2659-.LFB6528
	.uleb128 0
	.uleb128 .LEHB168-.LFB6528
	.uleb128 .LEHE168-.LEHB168
	.uleb128 .L2660-.LFB6528
	.uleb128 0
	.uleb128 .LEHB169-.LFB6528
	.uleb128 .LEHE169-.LEHB169
	.uleb128 .L2657-.LFB6528
	.uleb128 0
	.uleb128 .LEHB170-.LFB6528
	.uleb128 .LEHE170-.LEHB170
	.uleb128 .L2658-.LFB6528
	.uleb128 0
	.uleb128 .LEHB171-.LFB6528
	.uleb128 .LEHE171-.LEHB171
	.uleb128 .L2655-.LFB6528
	.uleb128 0
	.uleb128 .LEHB172-.LFB6528
	.uleb128 .LEHE172-.LEHB172
	.uleb128 .L2656-.LFB6528
	.uleb128 0
	.uleb128 .LEHB173-.LFB6528
	.uleb128 .LEHE173-.LEHB173
	.uleb128 .L2653-.LFB6528
	.uleb128 0
	.uleb128 .LEHB174-.LFB6528
	.uleb128 .LEHE174-.LEHB174
	.uleb128 .L2654-.LFB6528
	.uleb128 0
	.uleb128 .LEHB175-.LFB6528
	.uleb128 .LEHE175-.LEHB175
	.uleb128 .L2651-.LFB6528
	.uleb128 0
	.uleb128 .LEHB176-.LFB6528
	.uleb128 .LEHE176-.LEHB176
	.uleb128 .L2652-.LFB6528
	.uleb128 0
	.uleb128 .LEHB177-.LFB6528
	.uleb128 .LEHE177-.LEHB177
	.uleb128 .L2648-.LFB6528
	.uleb128 0
	.uleb128 .LEHB178-.LFB6528
	.uleb128 .LEHE178-.LEHB178
	.uleb128 .L2649-.LFB6528
	.uleb128 0
	.uleb128 .LEHB179-.LFB6528
	.uleb128 .LEHE179-.LEHB179
	.uleb128 .L2650-.LFB6528
	.uleb128 0
.LLSDACSE6528:
	.section	.text._ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6528
	.type	_ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE.cold, @function
_ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE.cold:
.LFSB6528:
.L2638:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2639
	call	_ZdlPv@PLT
.L2639:
	movq	%r12, %rdi
.LEHB180:
	call	_Unwind_Resume@PLT
.L2586:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2588
	call	_ZdlPv@PLT
.L2588:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2590
	call	_ZdlPv@PLT
.L2590:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2602
	call	_ZdlPv@PLT
.L2602:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2614
	call	_ZdlPv@PLT
.L2614:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2626
	call	_ZdlPv@PLT
.L2626:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2637
	call	_ZdlPv@PLT
.L2637:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L2611:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2602
	call	_ZdlPv@PLT
	jmp	.L2602
.L2623:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2614
	call	_ZdlPv@PLT
	jmp	.L2614
.L2643:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2645
	call	_ZdlPv@PLT
.L2645:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2646
	call	_ZdlPv@PLT
.L2646:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE180:
.L2635:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2626
	call	_ZdlPv@PLT
	jmp	.L2626
.L2599:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2590
	call	_ZdlPv@PLT
	jmp	.L2590
	.cfi_endproc
.LFE6528:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE
.LLSDAC6528:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6528-.LLSDACSBC6528
.LLSDACSBC6528:
	.uleb128 .LEHB180-.LCOLDB44
	.uleb128 .LEHE180-.LEHB180
	.uleb128 0
	.uleb128 0
.LLSDACSEC6528:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE
	.section	.text._ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE, .-_ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE.cold, .-_ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE.cold
.LCOLDE44:
	.section	.text._ZN2v88internal6torque12Declarations11LookupValueERKNS1_13QualifiedNameE
.LHOTE44:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB45:
	.section	.text._ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB45:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6549:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6549
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r14
	movq	8(%rsi), %r13
	movq	%rdi, -400(%rbp)
	movq	%rsi, -440(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-160(%rbp), %rax
	movq	%rax, -392(%rbp)
	movq	%rax, -176(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L2770
	testq	%r14, %r14
	je	.L2999
.L2770:
	movq	%r13, -208(%rbp)
	cmpq	$15, %r13
	ja	.L3000
	cmpq	$1, %r13
	jne	.L2773
	movzbl	(%r14), %eax
	movb	%al, -160(%rbp)
	movq	-392(%rbp), %rax
.L2774:
	movq	%r13, -168(%rbp)
	movb	$0, (%rax,%r13)
	movq	-176(%rbp), %rax
	cmpq	-392(%rbp), %rax
	je	.L3001
	movq	-392(%rbp), %rbx
	movq	-160(%rbp), %rcx
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	-168(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	%rbx, -176(%rbp)
	leaq	-72(%rbp), %rbx
	movq	%rcx, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	$0, -168(%rbp)
	movb	$0, -160(%rbp)
	movq	$0, -96(%rbp)
	movq	%rbx, -424(%rbp)
	movq	%rbx, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	cmpq	%rsi, %rax
	je	.L2776
	movq	%rax, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L2778:
	movq	%rdx, -80(%rbp)
.LEHB181:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE181:
	movq	(%rax), %rax
	pxor	%xmm0, %xmm0
	leaq	-208(%rbp), %r12
	leaq	-112(%rbp), %r13
	movq	(%rax), %rax
	movaps	%xmm0, -368(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, -416(%rbp)
	movq	16(%rax), %rax
	movq	%rax, -408(%rbp)
	testq	%rax, %rax
	je	.L2779
	movq	$0, -320(%rbp)
	movq	16(%rax), %rax
	leaq	-208(%rbp), %r12
	leaq	-112(%rbp), %r13
	movaps	%xmm0, -336(%rbp)
	movq	%rax, -432(%rbp)
	testq	%rax, %rax
	je	.L2780
	movq	$0, -288(%rbp)
	movq	16(%rax), %r15
	leaq	-208(%rbp), %r12
	leaq	-112(%rbp), %r13
	movaps	%xmm0, -304(%rbp)
	testq	%r15, %r15
	je	.L2781
	movq	$0, -256(%rbp)
	movq	16(%r15), %r14
	leaq	-208(%rbp), %r12
	leaq	-112(%rbp), %r13
	movaps	%xmm0, -272(%rbp)
	testq	%r14, %r14
	je	.L2782
	movq	$0, -224(%rbp)
	leaq	-208(%rbp), %r12
	leaq	-112(%rbp), %r13
	movaps	%xmm0, -240(%rbp)
	movq	16(%r14), %rsi
	testq	%rsi, %rsi
	je	.L2783
	movq	%r13, %rdx
	movq	%r12, %rdi
.LEHB182:
	call	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	movdqa	-208(%rbp), %xmm7
	movq	-192(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	-240(%rbp), %rdi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -224(%rbp)
	movaps	%xmm7, -240(%rbp)
	testq	%rdi, %rdi
	je	.L2783
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2783
	call	_ZdlPv@PLT
.L2783:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE182:
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2787
	leaq	-376(%rbp), %rax
	movq	%rax, -448(%rbp)
	jmp	.L2790
	.p2align 4,,10
	.p2align 3
.L3003:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
	cmpq	%rbx, %r14
	je	.L3002
.L2790:
	movq	(%rbx), %rax
	movq	-232(%rbp), %rsi
	movq	%rax, -376(%rbp)
	cmpq	-224(%rbp), %rsi
	jne	.L3003
	movq	-448(%rbp), %rdx
	leaq	-240(%rbp), %rdi
.LEHB183:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE183:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2790
	.p2align 4,,10
	.p2align 3
.L3002:
	movq	-208(%rbp), %r14
.L2787:
	testq	%r14, %r14
	je	.L2791
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2791:
	movdqa	-240(%rbp), %xmm7
	movq	-224(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -224(%rbp)
	movq	-272(%rbp), %rdi
	movaps	%xmm0, -240(%rbp)
	movq	%rax, -256(%rbp)
	movaps	%xmm7, -272(%rbp)
	testq	%rdi, %rdi
	je	.L2782
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2782
	call	_ZdlPv@PLT
.L2782:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
.LEHB184:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE184:
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2800
	leaq	-240(%rbp), %r15
	jmp	.L2803
	.p2align 4,,10
	.p2align 3
.L3005:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -264(%rbp)
	cmpq	%rbx, %r14
	je	.L3004
.L2803:
	movq	(%rbx), %rax
	movq	-264(%rbp), %rsi
	movq	%rax, -240(%rbp)
	cmpq	-256(%rbp), %rsi
	jne	.L3005
	leaq	-272(%rbp), %rdi
	movq	%r15, %rdx
.LEHB185:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE185:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2803
	.p2align 4,,10
	.p2align 3
.L3004:
	movq	-208(%rbp), %r14
.L2800:
	testq	%r14, %r14
	je	.L2804
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2804:
	movdqa	-272(%rbp), %xmm4
	movq	-256(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -256(%rbp)
	movq	-304(%rbp), %rdi
	movaps	%xmm0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movaps	%xmm4, -304(%rbp)
	testq	%rdi, %rdi
	je	.L2781
	call	_ZdlPv@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2781
	call	_ZdlPv@PLT
.L2781:
	movq	-432(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
.LEHB186:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE186:
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2812
	leaq	-240(%rbp), %r15
	jmp	.L2815
	.p2align 4,,10
	.p2align 3
.L3007:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -296(%rbp)
	cmpq	%rbx, %r14
	je	.L3006
.L2815:
	movq	(%rbx), %rax
	movq	-296(%rbp), %rsi
	movq	%rax, -240(%rbp)
	cmpq	-288(%rbp), %rsi
	jne	.L3007
	leaq	-304(%rbp), %rdi
	movq	%r15, %rdx
.LEHB187:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE187:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2815
	.p2align 4,,10
	.p2align 3
.L3006:
	movq	-208(%rbp), %r14
.L2812:
	testq	%r14, %r14
	je	.L2816
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2816:
	movdqa	-304(%rbp), %xmm3
	movq	-288(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -288(%rbp)
	movq	-336(%rbp), %rdi
	movaps	%xmm0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm3, -336(%rbp)
	testq	%rdi, %rdi
	je	.L2780
	call	_ZdlPv@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2780
	call	_ZdlPv@PLT
.L2780:
	movq	-408(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
.LEHB188:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE188:
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2824
	leaq	-240(%rbp), %r15
	jmp	.L2827
	.p2align 4,,10
	.p2align 3
.L3009:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -328(%rbp)
	cmpq	%rbx, %r14
	je	.L3008
.L2827:
	movq	(%rbx), %rax
	movq	-328(%rbp), %rsi
	movq	%rax, -240(%rbp)
	cmpq	-320(%rbp), %rsi
	jne	.L3009
	leaq	-336(%rbp), %rdi
	movq	%r15, %rdx
.LEHB189:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE189:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2827
	.p2align 4,,10
	.p2align 3
.L3008:
	movq	-208(%rbp), %r14
.L2824:
	testq	%r14, %r14
	je	.L2828
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2828:
	movdqa	-336(%rbp), %xmm2
	movq	-320(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -320(%rbp)
	movq	-368(%rbp), %rdi
	movaps	%xmm0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movaps	%xmm2, -368(%rbp)
	testq	%rdi, %rdi
	je	.L2779
	call	_ZdlPv@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2779
	call	_ZdlPv@PLT
.L2779:
	movq	-416(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
.LEHB190:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE190:
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L2836
	leaq	-240(%rbp), %r15
	jmp	.L2839
	.p2align 4,,10
	.p2align 3
.L3011:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -360(%rbp)
	cmpq	%rbx, %r14
	je	.L3010
.L2839:
	movq	(%rbx), %rax
	movq	-360(%rbp), %rsi
	movq	%rax, -240(%rbp)
	cmpq	-352(%rbp), %rsi
	jne	.L3011
	leaq	-368(%rbp), %rdi
	movq	%r15, %rdx
.LEHB191:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE191:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2839
	.p2align 4,,10
	.p2align 3
.L3010:
	movq	-208(%rbp), %r14
.L2836:
	testq	%r14, %r14
	je	.L2840
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2840:
	movq	-360(%rbp), %r14
	movq	-368(%rbp), %rbx
	cmpq	%rbx, %r14
	je	.L3012
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	-240(%rbp), %r15
	movq	$0, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	.p2align 4,,10
	.p2align 3
.L2851:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L2849
	cmpl	$7, 8(%rax)
	jne	.L2849
	movq	%rax, -240(%rbp)
	cmpq	%rsi, -192(%rbp)
	je	.L2850
	movq	%rax, (%rsi)
	movq	-200(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -200(%rbp)
.L2849:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L2851
	leaq	.LC13(%rip), %rax
	movq	%rax, -240(%rbp)
	movq	-208(%rbp), %rax
	cmpq	%rsi, %rax
	je	.L3013
	movq	-400(%rbp), %rcx
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	-192(%rbp), %rax
	movq	-368(%rbp), %rdi
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 16(%rcx)
	movups	%xmm0, (%rcx)
	testq	%rdi, %rdi
	je	.L2857
	call	_ZdlPv@PLT
.L2857:
	movq	-88(%rbp), %rdi
	cmpq	-424(%rbp), %rdi
	je	.L2858
	call	_ZdlPv@PLT
.L2858:
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2859
	.p2align 4,,10
	.p2align 3
.L2863:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2860
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2863
.L2861:
	movq	-112(%rbp), %r12
.L2859:
	testq	%r12, %r12
	je	.L2864
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2864:
	movq	-176(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L2769
	call	_ZdlPv@PLT
.L2769:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3014
	movq	-400(%rbp), %rax
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2773:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L3015
	movq	-392(%rbp), %rax
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L2860:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2863
	jmp	.L2861
	.p2align 4,,10
	.p2align 3
.L2850:
	movq	%r15, %rdx
	movq	%r12, %rdi
.LEHB192:
	call	_ZNSt6vectorIPN2v88internal6torque7GenericESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE192:
	movq	-200(%rbp), %rsi
	jmp	.L2849
	.p2align 4,,10
	.p2align 3
.L3000:
	leaq	-208(%rbp), %r12
	leaq	-176(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
.LEHB193:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -176(%rbp)
	movq	%rax, %rdi
	movq	-208(%rbp), %rax
	movq	%rax, -160(%rbp)
.L2772:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-208(%rbp), %r13
	movq	-176(%rbp), %rax
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L3001:
	leaq	-72(%rbp), %rax
	movdqa	-160(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	-168(%rbp), %rdx
	movb	$0, -160(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -424(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
.L2776:
	movdqa	-128(%rbp), %xmm6
	movups	%xmm6, -72(%rbp)
	jmp	.L2778
.L2999:
	leaq	.LC16(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE193:
.L3012:
	leaq	.LC36(%rip), %rdx
	movq	%r13, %rsi
	leaq	.LC37(%rip), %rdi
.LEHB194:
	call	_ZN2v88internal6torque11ReportErrorIJRA14_KcRKNS1_13QualifiedNameERA2_S3_EEEvDpOT_
.LEHE194:
.L3013:
	movq	-440(%rbp), %rcx
	leaq	-240(%rbp), %rsi
	leaq	.LC38(%rip), %rdx
	leaq	.LC39(%rip), %rdi
.LEHB195:
	call	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE195:
.L3014:
	call	__stack_chk_fail@PLT
.L3015:
	movq	-392(%rbp), %rdi
	jmp	.L2772
.L2874:
	endbr64
	movq	%rax, %r12
	jmp	.L2847
.L2873:
	endbr64
	movq	%rax, %r12
	jmp	.L2866
.L2875:
	endbr64
	movq	%rax, %r12
	jmp	.L2834
.L2876:
	endbr64
	movq	%rax, %r12
	jmp	.L2843
.L2877:
	endbr64
	movq	%rax, %r12
	jmp	.L2822
.L2878:
	endbr64
	movq	%rax, %r12
	jmp	.L2831
.L2879:
	endbr64
	movq	%rax, %r12
	jmp	.L2810
.L2880:
	endbr64
	movq	%rax, %r12
	jmp	.L2819
.L2881:
	endbr64
	movq	%rax, %r12
	jmp	.L2798
.L2882:
	endbr64
	movq	%rax, %r12
	jmp	.L2807
.L2884:
	endbr64
	movq	%rax, %r12
	jmp	.L2794
.L2872:
	endbr64
	movq	%rax, %r12
	leaq	-112(%rbp), %r13
	jmp	.L2846
.L2883:
	endbr64
	movq	%rax, %r12
	jmp	.L2796
.L2885:
	endbr64
	movq	%rax, %r12
	jmp	.L2866
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6549:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6549-.LLSDACSB6549
.LLSDACSB6549:
	.uleb128 .LEHB181-.LFB6549
	.uleb128 .LEHE181-.LEHB181
	.uleb128 .L2872-.LFB6549
	.uleb128 0
	.uleb128 .LEHB182-.LFB6549
	.uleb128 .LEHE182-.LEHB182
	.uleb128 .L2883-.LFB6549
	.uleb128 0
	.uleb128 .LEHB183-.LFB6549
	.uleb128 .LEHE183-.LEHB183
	.uleb128 .L2884-.LFB6549
	.uleb128 0
	.uleb128 .LEHB184-.LFB6549
	.uleb128 .LEHE184-.LEHB184
	.uleb128 .L2881-.LFB6549
	.uleb128 0
	.uleb128 .LEHB185-.LFB6549
	.uleb128 .LEHE185-.LEHB185
	.uleb128 .L2882-.LFB6549
	.uleb128 0
	.uleb128 .LEHB186-.LFB6549
	.uleb128 .LEHE186-.LEHB186
	.uleb128 .L2879-.LFB6549
	.uleb128 0
	.uleb128 .LEHB187-.LFB6549
	.uleb128 .LEHE187-.LEHB187
	.uleb128 .L2880-.LFB6549
	.uleb128 0
	.uleb128 .LEHB188-.LFB6549
	.uleb128 .LEHE188-.LEHB188
	.uleb128 .L2877-.LFB6549
	.uleb128 0
	.uleb128 .LEHB189-.LFB6549
	.uleb128 .LEHE189-.LEHB189
	.uleb128 .L2878-.LFB6549
	.uleb128 0
	.uleb128 .LEHB190-.LFB6549
	.uleb128 .LEHE190-.LEHB190
	.uleb128 .L2875-.LFB6549
	.uleb128 0
	.uleb128 .LEHB191-.LFB6549
	.uleb128 .LEHE191-.LEHB191
	.uleb128 .L2876-.LFB6549
	.uleb128 0
	.uleb128 .LEHB192-.LFB6549
	.uleb128 .LEHE192-.LEHB192
	.uleb128 .L2885-.LFB6549
	.uleb128 0
	.uleb128 .LEHB193-.LFB6549
	.uleb128 .LEHE193-.LEHB193
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB194-.LFB6549
	.uleb128 .LEHE194-.LEHB194
	.uleb128 .L2874-.LFB6549
	.uleb128 0
	.uleb128 .LEHB195-.LFB6549
	.uleb128 .LEHE195-.LEHB195
	.uleb128 .L2873-.LFB6549
	.uleb128 0
.LLSDACSE6549:
	.section	.text._ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6549
	.type	_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6549:
.L2847:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2846
	call	_ZdlPv@PLT
.L2846:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	-176(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L2869
	call	_ZdlPv@PLT
.L2869:
	movq	%r12, %rdi
.LEHB196:
	call	_Unwind_Resume@PLT
.LEHE196:
.L2866:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2855
	call	_ZdlPv@PLT
.L2855:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2846
	call	_ZdlPv@PLT
	jmp	.L2846
.L2794:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2796
	call	_ZdlPv@PLT
.L2796:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2798
	call	_ZdlPv@PLT
.L2798:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2810
	call	_ZdlPv@PLT
.L2810:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2822
	call	_ZdlPv@PLT
.L2822:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2834
	call	_ZdlPv@PLT
.L2834:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2846
	call	_ZdlPv@PLT
	jmp	.L2846
.L2843:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2834
	call	_ZdlPv@PLT
	jmp	.L2834
.L2831:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2822
	call	_ZdlPv@PLT
	jmp	.L2822
.L2819:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2810
	call	_ZdlPv@PLT
	jmp	.L2810
.L2807:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2798
	call	_ZdlPv@PLT
	jmp	.L2798
	.cfi_endproc
.LFE6549:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6549:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6549-.LLSDACSBC6549
.LLSDACSBC6549:
	.uleb128 .LEHB196-.LCOLDB45
	.uleb128 .LEHE196-.LEHB196
	.uleb128 0
	.uleb128 0
.LLSDACSEC6549:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE45:
	.section	.text._ZN2v88internal6torque12Declarations13LookupGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE45:
	.section	.rodata._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC46:
	.string	"\" in global scope"
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB47:
	.section	.text._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB47:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6520:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6520
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$712, %rsp
	movq	%rsi, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB197:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE197:
	movq	(%rbx), %r15
	movq	8(%rbx), %r14
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -712(%rbp)
	leaq	-496(%rbp), %rax
	movq	%rax, -696(%rbp)
	movq	%rax, -512(%rbp)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L3017
	testq	%r15, %r15
	je	.L3217
.L3017:
	movq	%r14, -544(%rbp)
	cmpq	$15, %r14
	ja	.L3218
	cmpq	$1, %r14
	jne	.L3020
	movzbl	(%r15), %eax
	leaq	-544(%rbp), %r13
	movb	%al, -496(%rbp)
	movq	-696(%rbp), %rax
.L3021:
	movq	%r14, -504(%rbp)
	movb	$0, (%rax,%r14)
	movq	-512(%rbp), %rax
	cmpq	-696(%rbp), %rax
	je	.L3219
	movq	-696(%rbp), %rsi
	movq	-496(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, -480(%rbp)
	movq	-504(%rbp), %rdx
	movb	$0, -496(%rbp)
	movq	%rsi, -512(%rbp)
	leaq	-408(%rbp), %rsi
	movq	%rsi, -728(%rbp)
	movq	%rsi, -424(%rbp)
	leaq	-464(%rbp), %rsi
	movq	%rcx, -464(%rbp)
	movq	%rdx, -472(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -432(%rbp)
	movaps	%xmm0, -448(%rbp)
	cmpq	%rsi, %rax
	je	.L3023
	movq	%rax, -424(%rbp)
	movq	%rcx, -408(%rbp)
.L3025:
	movq	-712(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rdx, -416(%rbp)
	movq	$0, 16(%r12)
	movups	%xmm0, (%r12)
	movq	16(%rax), %rax
	movq	%rax, -720(%rbp)
	testq	%rax, %rax
	je	.L3220
	movq	$0, -656(%rbp)
	movq	16(%rax), %rax
	movaps	%xmm0, -672(%rbp)
	movq	%rax, -736(%rbp)
	testq	%rax, %rax
	je	.L3221
	movq	$0, -624(%rbp)
	movq	16(%rax), %r15
	movaps	%xmm0, -640(%rbp)
	testq	%r15, %r15
	je	.L3222
	movq	$0, -592(%rbp)
	movq	16(%r15), %r14
	movaps	%xmm0, -608(%rbp)
	testq	%r14, %r14
	je	.L3223
	movaps	%xmm0, -576(%rbp)
	leaq	-448(%rbp), %rax
	movq	$0, -560(%rbp)
	movq	16(%r14), %rsi
	movq	%rax, -704(%rbp)
	testq	%rsi, %rsi
	je	.L3030
	movq	%rax, %rdx
	movq	%r13, %rdi
.LEHB198:
	call	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	movdqa	-544(%rbp), %xmm7
	movq	-528(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -528(%rbp)
	movq	-576(%rbp), %rdi
	movaps	%xmm0, -544(%rbp)
	movq	%rax, -560(%rbp)
	movaps	%xmm7, -576(%rbp)
	testq	%rdi, %rdi
	je	.L3030
	call	_ZdlPv@PLT
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3030
	call	_ZdlPv@PLT
.L3030:
	movq	-704(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE198:
	movq	-544(%rbp), %rbx
	movq	-536(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3034
	leaq	-680(%rbp), %rax
	movq	%rax, -752(%rbp)
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3225:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -568(%rbp)
	cmpq	%rbx, %r14
	je	.L3224
.L3037:
	movq	(%rbx), %rax
	movq	-568(%rbp), %rsi
	movq	%rax, -680(%rbp)
	cmpq	-560(%rbp), %rsi
	jne	.L3225
	movq	-752(%rbp), %rdx
	leaq	-576(%rbp), %rdi
.LEHB199:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE199:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L3037
	.p2align 4,,10
	.p2align 3
.L3224:
	movq	-544(%rbp), %r14
.L3034:
	testq	%r14, %r14
	je	.L3038
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3038:
	movdqa	-576(%rbp), %xmm6
	movq	-560(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -560(%rbp)
	movq	-608(%rbp), %rdi
	movaps	%xmm0, -576(%rbp)
	movq	%rax, -592(%rbp)
	movaps	%xmm6, -608(%rbp)
	testq	%rdi, %rdi
	je	.L3029
	call	_ZdlPv@PLT
	movq	-576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3029
	call	_ZdlPv@PLT
.L3029:
	movq	-704(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
.LEHB200:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE200:
	movq	-544(%rbp), %rbx
	movq	-536(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3047
	leaq	-576(%rbp), %r15
	jmp	.L3050
	.p2align 4,,10
	.p2align 3
.L3227:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -600(%rbp)
	cmpq	%rbx, %r14
	je	.L3226
.L3050:
	movq	(%rbx), %rax
	movq	-600(%rbp), %rsi
	movq	%rax, -576(%rbp)
	cmpq	-592(%rbp), %rsi
	jne	.L3227
	leaq	-608(%rbp), %rdi
	movq	%r15, %rdx
.LEHB201:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE201:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L3050
	.p2align 4,,10
	.p2align 3
.L3226:
	movq	-544(%rbp), %r14
.L3047:
	testq	%r14, %r14
	je	.L3051
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3051:
	movdqa	-608(%rbp), %xmm3
	movq	-592(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -592(%rbp)
	movq	-640(%rbp), %rdi
	movaps	%xmm0, -608(%rbp)
	movq	%rax, -624(%rbp)
	movaps	%xmm3, -640(%rbp)
	testq	%rdi, %rdi
	je	.L3028
	call	_ZdlPv@PLT
	movq	-608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3028
	call	_ZdlPv@PLT
.L3028:
	movq	-704(%rbp), %rdx
	movq	-736(%rbp), %rsi
	movq	%r13, %rdi
.LEHB202:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE202:
	movq	-544(%rbp), %rbx
	movq	-536(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3059
	leaq	-576(%rbp), %r15
	jmp	.L3062
	.p2align 4,,10
	.p2align 3
.L3229:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -632(%rbp)
	cmpq	%rbx, %r14
	je	.L3228
.L3062:
	movq	(%rbx), %rax
	movq	-632(%rbp), %rsi
	movq	%rax, -576(%rbp)
	cmpq	-624(%rbp), %rsi
	jne	.L3229
	leaq	-640(%rbp), %rdi
	movq	%r15, %rdx
.LEHB203:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE203:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L3062
	.p2align 4,,10
	.p2align 3
.L3228:
	movq	-544(%rbp), %r14
.L3059:
	testq	%r14, %r14
	je	.L3063
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3063:
	movdqa	-640(%rbp), %xmm2
	movq	-624(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -624(%rbp)
	movq	-672(%rbp), %rdi
	movaps	%xmm0, -640(%rbp)
	movq	%rax, -656(%rbp)
	movaps	%xmm2, -672(%rbp)
	testq	%rdi, %rdi
	je	.L3027
	call	_ZdlPv@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3027
	call	_ZdlPv@PLT
.L3027:
	movq	-704(%rbp), %rdx
	movq	-720(%rbp), %rsi
	movq	%r13, %rdi
.LEHB204:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE204:
	movq	-544(%rbp), %rbx
	movq	-536(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3071
	leaq	-576(%rbp), %r15
	jmp	.L3074
	.p2align 4,,10
	.p2align 3
.L3231:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -664(%rbp)
	cmpq	%rbx, %r14
	je	.L3230
.L3074:
	movq	(%rbx), %rax
	movq	-664(%rbp), %rsi
	movq	%rax, -576(%rbp)
	cmpq	-656(%rbp), %rsi
	jne	.L3231
	leaq	-672(%rbp), %rdi
	movq	%r15, %rdx
.LEHB205:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE205:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L3074
	.p2align 4,,10
	.p2align 3
.L3230:
	movq	-544(%rbp), %r14
.L3071:
	testq	%r14, %r14
	je	.L3075
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3075:
	movdqa	-672(%rbp), %xmm1
	movq	-656(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -656(%rbp)
	movq	(%r12), %rdi
	movaps	%xmm0, -672(%rbp)
	movq	%rax, 16(%r12)
	movups	%xmm1, (%r12)
	testq	%rdi, %rdi
	je	.L3026
	call	_ZdlPv@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3026
	call	_ZdlPv@PLT
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3020:
	testq	%r14, %r14
	jne	.L3232
	movq	-696(%rbp), %rax
	leaq	-544(%rbp), %r13
	jmp	.L3021
	.p2align 4,,10
	.p2align 3
.L3218:
	leaq	-544(%rbp), %r13
	leaq	-512(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
.LEHB206:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE206:
	movq	%rax, -512(%rbp)
	movq	%rax, %rdi
	movq	-544(%rbp), %rax
	movq	%rax, -496(%rbp)
.L3019:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-544(%rbp), %r14
	movq	-512(%rbp), %rax
	jmp	.L3021
	.p2align 4,,10
	.p2align 3
.L3220:
	leaq	-448(%rbp), %rax
	movq	%rax, -704(%rbp)
.L3026:
	movq	-704(%rbp), %rdx
	movq	-712(%rbp), %rsi
	movq	%r13, %rdi
.LEHB207:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE207:
	movq	-544(%rbp), %rbx
	movq	-536(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L3083
	leaq	-576(%rbp), %r14
	jmp	.L3086
	.p2align 4,,10
	.p2align 3
.L3234:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
	cmpq	%rbx, %r13
	je	.L3233
.L3086:
	movq	(%rbx), %rax
	movq	8(%r12), %rsi
	movq	%rax, -576(%rbp)
	cmpq	16(%r12), %rsi
	jne	.L3234
	movq	%r14, %rdx
	movq	%r12, %rdi
.LEHB208:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE208:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L3086
	.p2align 4,,10
	.p2align 3
.L3233:
	movq	-544(%rbp), %r13
.L3083:
	testq	%r13, %r13
	je	.L3087
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3087:
	movq	-424(%rbp), %rdi
	cmpq	-728(%rbp), %rdi
	je	.L3089
	call	_ZdlPv@PLT
.L3089:
	movq	-440(%rbp), %rbx
	movq	-448(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L3095
	.p2align 4,,10
	.p2align 3
.L3099:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3096
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L3099
.L3097:
	movq	-448(%rbp), %r13
.L3095:
	testq	%r13, %r13
	je	.L3100
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3100:
	movq	-512(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	je	.L3101
	call	_ZdlPv@PLT
.L3101:
	movq	(%r12), %rax
	cmpq	%rax, 8(%r12)
	je	.L3235
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3236
	addq	$712, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3096:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L3099
	jmp	.L3097
	.p2align 4,,10
	.p2align 3
.L3219:
	leaq	-408(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-496(%rbp), %xmm4
	movq	-504(%rbp), %rdx
	movb	$0, -496(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -432(%rbp)
	movq	%rax, -728(%rbp)
	movq	%rax, -424(%rbp)
	movaps	%xmm4, -464(%rbp)
	movaps	%xmm0, -448(%rbp)
.L3023:
	movdqa	-464(%rbp), %xmm5
	movups	%xmm5, -408(%rbp)
	jmp	.L3025
	.p2align 4,,10
	.p2align 3
.L3221:
	leaq	-448(%rbp), %rax
	movq	%rax, -704(%rbp)
	jmp	.L3027
	.p2align 4,,10
	.p2align 3
.L3222:
	leaq	-448(%rbp), %rax
	movq	%rax, -704(%rbp)
	jmp	.L3028
	.p2align 4,,10
	.p2align 3
.L3223:
	leaq	-448(%rbp), %rax
	movq	%rax, -704(%rbp)
	jmp	.L3029
.L3217:
	leaq	.LC16(%rip), %rdi
.LEHB209:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE209:
.L3235:
	movq	-704(%rbp), %rdi
.LEHB210:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE210:
	leaq	-432(%rbp), %r13
	movl	$13, %edx
	leaq	.LC37(%rip), %rsi
	movq	%r13, %rdi
.LEHB211:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-744(%rbp), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	leaq	.LC46(%rip), %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	leaq	-480(%rbp), %r13
	leaq	-424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE211:
	movq	%r13, %rdi
.LEHB212:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE212:
.L3236:
	call	__stack_chk_fail@PLT
.L3232:
	movq	-696(%rbp), %rdi
	leaq	-544(%rbp), %r13
	jmp	.L3019
.L3113:
	endbr64
	movq	%rax, %r13
	jmp	.L3081
.L3118:
	endbr64
	movq	%rax, %r13
	jmp	.L3066
.L3116:
	endbr64
	movq	%rax, %r13
	jmp	.L3078
.L3115:
	endbr64
	movq	%rax, %r13
	jmp	.L3069
.L3121:
	endbr64
	movq	%rax, %r13
	jmp	.L3043
.L3122:
	endbr64
	movq	%rax, %r13
	jmp	.L3041
.L3120:
	endbr64
	movq	%rax, %r13
	jmp	.L3054
.L3114:
	endbr64
	movq	%rax, %r13
	jmp	.L3090
.L3119:
	endbr64
	movq	%rax, %r13
	jmp	.L3045
.L3117:
	endbr64
	movq	%rax, %r13
	jmp	.L3057
.L3111:
	endbr64
	movq	%rax, %r13
	jmp	.L3105
.L3110:
	endbr64
	movq	%rax, %r13
	jmp	.L3106
.L3112:
	endbr64
	movq	%rax, %r13
	jmp	.L3103
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6520:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6520-.LLSDACSB6520
.LLSDACSB6520:
	.uleb128 .LEHB197-.LFB6520
	.uleb128 .LEHE197-.LEHB197
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB198-.LFB6520
	.uleb128 .LEHE198-.LEHB198
	.uleb128 .L3121-.LFB6520
	.uleb128 0
	.uleb128 .LEHB199-.LFB6520
	.uleb128 .LEHE199-.LEHB199
	.uleb128 .L3122-.LFB6520
	.uleb128 0
	.uleb128 .LEHB200-.LFB6520
	.uleb128 .LEHE200-.LEHB200
	.uleb128 .L3119-.LFB6520
	.uleb128 0
	.uleb128 .LEHB201-.LFB6520
	.uleb128 .LEHE201-.LEHB201
	.uleb128 .L3120-.LFB6520
	.uleb128 0
	.uleb128 .LEHB202-.LFB6520
	.uleb128 .LEHE202-.LEHB202
	.uleb128 .L3117-.LFB6520
	.uleb128 0
	.uleb128 .LEHB203-.LFB6520
	.uleb128 .LEHE203-.LEHB203
	.uleb128 .L3118-.LFB6520
	.uleb128 0
	.uleb128 .LEHB204-.LFB6520
	.uleb128 .LEHE204-.LEHB204
	.uleb128 .L3115-.LFB6520
	.uleb128 0
	.uleb128 .LEHB205-.LFB6520
	.uleb128 .LEHE205-.LEHB205
	.uleb128 .L3116-.LFB6520
	.uleb128 0
	.uleb128 .LEHB206-.LFB6520
	.uleb128 .LEHE206-.LEHB206
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB207-.LFB6520
	.uleb128 .LEHE207-.LEHB207
	.uleb128 .L3113-.LFB6520
	.uleb128 0
	.uleb128 .LEHB208-.LFB6520
	.uleb128 .LEHE208-.LEHB208
	.uleb128 .L3114-.LFB6520
	.uleb128 0
	.uleb128 .LEHB209-.LFB6520
	.uleb128 .LEHE209-.LEHB209
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB210-.LFB6520
	.uleb128 .LEHE210-.LEHB210
	.uleb128 .L3110-.LFB6520
	.uleb128 0
	.uleb128 .LEHB211-.LFB6520
	.uleb128 .LEHE211-.LEHB211
	.uleb128 .L3111-.LFB6520
	.uleb128 0
	.uleb128 .LEHB212-.LFB6520
	.uleb128 .LEHE212-.LEHB212
	.uleb128 .L3112-.LFB6520
	.uleb128 0
.LLSDACSE6520:
	.section	.text._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6520
	.type	_ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6520:
.L3041:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3043
	call	_ZdlPv@PLT
.L3043:
	movq	-576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3045
	call	_ZdlPv@PLT
.L3045:
	movq	-608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3057
	call	_ZdlPv@PLT
.L3057:
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3069
	call	_ZdlPv@PLT
.L3069:
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3081
	call	_ZdlPv@PLT
.L3081:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3092
	call	_ZdlPv@PLT
.L3092:
	movq	-704(%rbp), %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	-512(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	je	.L3107
.L3216:
	call	_ZdlPv@PLT
.L3107:
	movq	%r13, %rdi
.LEHB213:
	call	_Unwind_Resume@PLT
.LEHE213:
.L3066:
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3057
	call	_ZdlPv@PLT
	jmp	.L3057
.L3078:
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3069
	call	_ZdlPv@PLT
	jmp	.L3069
.L3054:
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3045
	call	_ZdlPv@PLT
	jmp	.L3045
.L3090:
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3081
	call	_ZdlPv@PLT
	jmp	.L3081
.L3103:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3105
	call	_ZdlPv@PLT
.L3105:
	movq	-704(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L3106:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L3216
	jmp	.L3107
	.cfi_endproc
.LFE6520:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6520:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6520-.LLSDACSBC6520
.LLSDACSBC6520:
	.uleb128 .LEHB213-.LCOLDB47
	.uleb128 .LEHE213-.LEHB213
	.uleb128 0
	.uleb128 0
.LLSDACSEC6520:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE47:
	.section	.text._ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE47:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB48:
	.section	.text._ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB48:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6524:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6524
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-96(%rbp), %rdi
	pushq	%rbx
	movq	%r12, %rsi
	subq	$80, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB214:
	call	_ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LEHE214:
	movq	-96(%rbp), %rbx
	movq	-88(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	$0, -48(%rbp)
	movaps	%xmm0, -64(%rbp)
	cmpq	%r13, %rbx
	je	.L3238
	xorl	%esi, %esi
	leaq	-104(%rbp), %r14
	jmp	.L3241
	.p2align 4,,10
	.p2align 3
.L3278:
	movq	%rax, (%rsi)
	movq	-56(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -56(%rbp)
.L3239:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L3277
.L3241:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L3239
	cmpl	$9, 8(%rax)
	jne	.L3239
	movq	%rax, -104(%rbp)
	cmpq	-48(%rbp), %rsi
	jne	.L3278
	leaq	-64(%rbp), %rdi
	movq	%r14, %rdx
.LEHB215:
	call	_ZNSt6vectorIPN2v88internal6torque9TypeAliasESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE215:
	addq	$8, %rbx
	movq	-56(%rbp), %rsi
	cmpq	%rbx, %r13
	jne	.L3241
	.p2align 4,,10
	.p2align 3
.L3277:
	movq	-64(%rbp), %rdi
	leaq	.LC26(%rip), %rax
	movq	%rax, -104(%rbp)
	cmpq	%rdi, %rsi
	je	.L3242
	subq	%rdi, %rsi
	cmpq	$8, %rsi
	jne	.L3279
	movq	(%rdi), %r12
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3248
	call	_ZdlPv@PLT
.L3248:
	cmpb	$0, 88(%r12)
	je	.L3249
	movq	96(%r12), %rax
.L3237:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3280
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3249:
	.cfi_restore_state
	movq	%r12, %rdi
.LEHB216:
	call	_ZNK2v88internal6torque9TypeAlias7ResolveEv@PLT
.LEHE216:
	jmp	.L3237
.L3279:
	leaq	-104(%rbp), %rsi
	movq	%r12, %rcx
	leaq	.LC33(%rip), %rdx
	leaq	.LC34(%rip), %rdi
.LEHB217:
	call	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.L3238:
	leaq	.LC26(%rip), %rax
	movq	%rax, -104(%rbp)
.L3242:
	leaq	-104(%rbp), %rsi
	movq	%r12, %rcx
	leaq	.LC38(%rip), %rdx
	leaq	.LC39(%rip), %rdi
	call	_ZN2v88internal6torque11ReportErrorIJRA13_KcRPS3_RA8_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE217:
.L3280:
	call	__stack_chk_fail@PLT
.L3256:
	endbr64
	movq	%rax, %r12
	jmp	.L3251
.L3255:
	endbr64
	movq	%rax, %r12
	jmp	.L3251
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6524:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6524-.LLSDACSB6524
.LLSDACSB6524:
	.uleb128 .LEHB214-.LFB6524
	.uleb128 .LEHE214-.LEHB214
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB215-.LFB6524
	.uleb128 .LEHE215-.LEHB215
	.uleb128 .L3256-.LFB6524
	.uleb128 0
	.uleb128 .LEHB216-.LFB6524
	.uleb128 .LEHE216-.LEHB216
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB217-.LFB6524
	.uleb128 .LEHE217-.LEHB217
	.uleb128 .L3255-.LFB6524
	.uleb128 0
.LLSDACSE6524:
	.section	.text._ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6524
	.type	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6524:
.L3251:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3246
	call	_ZdlPv@PLT
.L3246:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3253
	call	_ZdlPv@PLT
.L3253:
	movq	%r12, %rdi
.LEHB218:
	call	_Unwind_Resume@PLT
.LEHE218:
	.cfi_endproc
.LFE6524:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6524:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6524-.LLSDACSBC6524
.LLSDACSBC6524:
	.uleb128 .LEHB218-.LCOLDB48
	.uleb128 .LEHE218-.LEHB218
	.uleb128 0
	.uleb128 0
.LLSDACSEC6524:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE48:
	.section	.text._ZN2v88internal6torque12Declarations16LookupGlobalTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE48:
	.section	.text._ZNSt6vectorIPN2v88internal6torque5MacroESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque5MacroESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque5MacroESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque5MacroESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque5MacroESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB11855:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3295
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L3291
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L3296
.L3283:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L3290:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L3297
	testq	%r13, %r13
	jg	.L3286
	testq	%r9, %r9
	jne	.L3289
.L3287:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3297:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L3286
.L3289:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L3287
	.p2align 4,,10
	.p2align 3
.L3296:
	testq	%rsi, %rsi
	jne	.L3284
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L3290
	.p2align 4,,10
	.p2align 3
.L3286:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L3287
	jmp	.L3289
	.p2align 4,,10
	.p2align 3
.L3291:
	movl	$8, %r14d
	jmp	.L3283
.L3295:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3284:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L3283
	.cfi_endproc
.LFE11855:
	.size	_ZNSt6vectorIPN2v88internal6torque5MacroESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque5MacroESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE,"ax",@progbits
	.align 2
.LCOLDB49:
	.section	.text._ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE,"ax",@progbits
.LHOTB49:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.type	_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE, @function
_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE:
.LFB6529:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6529
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-160(%rbp), %rax
	movq	%rax, -392(%rbp)
	movq	%rax, -176(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L3299
	testq	%r14, %r14
	je	.L3538
.L3299:
	movq	%r13, -208(%rbp)
	cmpq	$15, %r13
	ja	.L3539
	cmpq	$1, %r13
	jne	.L3302
	movzbl	(%r14), %eax
	movb	%al, -160(%rbp)
	movq	-392(%rbp), %rax
.L3303:
	movq	%r13, -168(%rbp)
	movb	$0, (%rax,%r13)
	movq	-176(%rbp), %rax
	cmpq	-392(%rbp), %rax
	je	.L3540
	movq	-392(%rbp), %rsi
	movq	-160(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, -144(%rbp)
	movq	-168(%rbp), %rdx
	movb	$0, -160(%rbp)
	movq	%rsi, -176(%rbp)
	leaq	-72(%rbp), %rsi
	movq	%rsi, -424(%rbp)
	movq	%rsi, -88(%rbp)
	leaq	-128(%rbp), %rsi
	movq	%rcx, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	cmpq	%rsi, %rax
	je	.L3305
	movq	%rax, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L3307:
	movq	%rdx, -80(%rbp)
.LEHB219:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE219:
	movq	(%rax), %rax
	pxor	%xmm0, %xmm0
	movq	(%rax), %rax
	movaps	%xmm0, -368(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, -416(%rbp)
	movq	16(%rax), %rax
	movq	%rax, -408(%rbp)
	testq	%rax, %rax
	je	.L3541
	movq	$0, -320(%rbp)
	movq	16(%rax), %rax
	movaps	%xmm0, -336(%rbp)
	movq	%rax, -432(%rbp)
	testq	%rax, %rax
	je	.L3542
	movq	$0, -288(%rbp)
	movq	16(%rax), %r14
	movaps	%xmm0, -304(%rbp)
	testq	%r14, %r14
	je	.L3543
	movq	$0, -256(%rbp)
	movq	16(%r14), %r13
	movaps	%xmm0, -272(%rbp)
	testq	%r13, %r13
	je	.L3544
	movq	$0, -224(%rbp)
	leaq	-112(%rbp), %rax
	leaq	-208(%rbp), %r12
	movaps	%xmm0, -240(%rbp)
	movq	16(%r13), %rsi
	movq	%rax, -400(%rbp)
	testq	%rsi, %rsi
	je	.L3312
	movq	%rax, %rdx
	movq	%r12, %rdi
.LEHB220:
	call	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	movdqa	-208(%rbp), %xmm7
	movq	-192(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	-240(%rbp), %rdi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -224(%rbp)
	movaps	%xmm7, -240(%rbp)
	testq	%rdi, %rdi
	je	.L3312
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3312
	call	_ZdlPv@PLT
.L3312:
	movq	-400(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE220:
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L3316
	leaq	-376(%rbp), %rax
	movq	%rax, -440(%rbp)
	jmp	.L3319
	.p2align 4,,10
	.p2align 3
.L3546:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
	cmpq	%rbx, %r13
	je	.L3545
.L3319:
	movq	(%rbx), %rax
	movq	-232(%rbp), %rsi
	movq	%rax, -376(%rbp)
	cmpq	-224(%rbp), %rsi
	jne	.L3546
	movq	-440(%rbp), %rdx
	leaq	-240(%rbp), %rdi
.LEHB221:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE221:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L3319
	.p2align 4,,10
	.p2align 3
.L3545:
	movq	-208(%rbp), %r13
.L3316:
	testq	%r13, %r13
	je	.L3320
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3320:
	movdqa	-240(%rbp), %xmm6
	movq	-224(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -224(%rbp)
	movq	-272(%rbp), %rdi
	movaps	%xmm0, -240(%rbp)
	movq	%rax, -256(%rbp)
	movaps	%xmm6, -272(%rbp)
	testq	%rdi, %rdi
	je	.L3311
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3311
	call	_ZdlPv@PLT
.L3311:
	movq	-400(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB222:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE222:
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L3329
	leaq	-240(%rbp), %r14
	jmp	.L3332
	.p2align 4,,10
	.p2align 3
.L3548:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -264(%rbp)
	cmpq	%rbx, %r13
	je	.L3547
.L3332:
	movq	(%rbx), %rax
	movq	-264(%rbp), %rsi
	movq	%rax, -240(%rbp)
	cmpq	-256(%rbp), %rsi
	jne	.L3548
	leaq	-272(%rbp), %rdi
	movq	%r14, %rdx
.LEHB223:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE223:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L3332
	.p2align 4,,10
	.p2align 3
.L3547:
	movq	-208(%rbp), %r13
.L3329:
	testq	%r13, %r13
	je	.L3333
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3333:
	movdqa	-272(%rbp), %xmm3
	movq	-256(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -256(%rbp)
	movq	-304(%rbp), %rdi
	movaps	%xmm0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movaps	%xmm3, -304(%rbp)
	testq	%rdi, %rdi
	je	.L3310
	call	_ZdlPv@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3310
	call	_ZdlPv@PLT
.L3310:
	movq	-400(%rbp), %rdx
	movq	-432(%rbp), %rsi
	movq	%r12, %rdi
.LEHB224:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE224:
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L3341
	leaq	-240(%rbp), %r14
	jmp	.L3344
	.p2align 4,,10
	.p2align 3
.L3550:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -296(%rbp)
	cmpq	%rbx, %r13
	je	.L3549
.L3344:
	movq	(%rbx), %rax
	movq	-296(%rbp), %rsi
	movq	%rax, -240(%rbp)
	cmpq	-288(%rbp), %rsi
	jne	.L3550
	leaq	-304(%rbp), %rdi
	movq	%r14, %rdx
.LEHB225:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE225:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L3344
	.p2align 4,,10
	.p2align 3
.L3549:
	movq	-208(%rbp), %r13
.L3341:
	testq	%r13, %r13
	je	.L3345
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3345:
	movdqa	-304(%rbp), %xmm2
	movq	-288(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -288(%rbp)
	movq	-336(%rbp), %rdi
	movaps	%xmm0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movaps	%xmm2, -336(%rbp)
	testq	%rdi, %rdi
	je	.L3309
	call	_ZdlPv@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3309
	call	_ZdlPv@PLT
.L3309:
	movq	-400(%rbp), %rdx
	movq	-408(%rbp), %rsi
	movq	%r12, %rdi
.LEHB226:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE226:
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L3353
	leaq	-240(%rbp), %r14
	jmp	.L3356
	.p2align 4,,10
	.p2align 3
.L3552:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -328(%rbp)
	cmpq	%rbx, %r13
	je	.L3551
.L3356:
	movq	(%rbx), %rax
	movq	-328(%rbp), %rsi
	movq	%rax, -240(%rbp)
	cmpq	-320(%rbp), %rsi
	jne	.L3552
	leaq	-336(%rbp), %rdi
	movq	%r14, %rdx
.LEHB227:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE227:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L3356
	.p2align 4,,10
	.p2align 3
.L3551:
	movq	-208(%rbp), %r13
.L3353:
	testq	%r13, %r13
	je	.L3357
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3357:
	movdqa	-336(%rbp), %xmm1
	movq	-320(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -320(%rbp)
	movq	-368(%rbp), %rdi
	movaps	%xmm0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movaps	%xmm1, -368(%rbp)
	testq	%rdi, %rdi
	je	.L3308
	call	_ZdlPv@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3308
	call	_ZdlPv@PLT
	jmp	.L3308
	.p2align 4,,10
	.p2align 3
.L3302:
	testq	%r13, %r13
	jne	.L3553
	movq	-392(%rbp), %rax
	jmp	.L3303
	.p2align 4,,10
	.p2align 3
.L3539:
	leaq	-208(%rbp), %r12
	leaq	-176(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
.LEHB228:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE228:
	movq	%rax, -176(%rbp)
	movq	%rax, %rdi
	movq	-208(%rbp), %rax
	movq	%rax, -160(%rbp)
.L3301:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-208(%rbp), %r13
	movq	-176(%rbp), %rax
	jmp	.L3303
	.p2align 4,,10
	.p2align 3
.L3541:
	leaq	-112(%rbp), %rax
	leaq	-208(%rbp), %r12
	movq	%rax, -400(%rbp)
.L3308:
	movq	-400(%rbp), %rdx
	movq	-416(%rbp), %rsi
	movq	%r12, %rdi
.LEHB229:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE229:
	movq	-208(%rbp), %rbx
	movq	-200(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L3365
	leaq	-240(%rbp), %r14
	jmp	.L3368
	.p2align 4,,10
	.p2align 3
.L3555:
	addq	$8, %rbx
	movq	%rax, (%rsi)
	addq	$8, -360(%rbp)
	cmpq	%rbx, %r13
	je	.L3554
.L3368:
	movq	(%rbx), %rax
	movq	-360(%rbp), %rsi
	movq	%rax, -240(%rbp)
	cmpq	-352(%rbp), %rsi
	jne	.L3555
	leaq	-368(%rbp), %rdi
	movq	%r14, %rdx
.LEHB230:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE230:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L3368
	.p2align 4,,10
	.p2align 3
.L3554:
	movq	-208(%rbp), %r13
.L3365:
	testq	%r13, %r13
	je	.L3369
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3369:
	movq	-360(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	-368(%rbp), %rbx
	movq	$0, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	leaq	-240(%rbp), %r14
	cmpq	%r13, %rbx
	jne	.L3380
	jmp	.L3371
	.p2align 4,,10
	.p2align 3
.L3412:
	movq	%rax, (%rsi)
	addq	$8, -200(%rbp)
.L3377:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L3556
.L3380:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L3377
	movl	8(%rax), %ecx
	leal	-1(%rcx), %edx
	cmpl	$2, %edx
	ja	.L3377
	movq	%rax, -240(%rbp)
	movq	-200(%rbp), %rsi
	cmpq	-192(%rbp), %rsi
	jne	.L3412
	movq	%r14, %rdx
	movq	%r12, %rdi
.LEHB231:
	call	_ZNSt6vectorIPN2v88internal6torque5MacroESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE231:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L3380
	.p2align 4,,10
	.p2align 3
.L3556:
	movq	-368(%rbp), %r13
.L3371:
	testq	%r13, %r13
	je	.L3385
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L3385:
	movq	-88(%rbp), %rdi
	cmpq	-424(%rbp), %rdi
	je	.L3387
	call	_ZdlPv@PLT
.L3387:
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L3388
	.p2align 4,,10
	.p2align 3
.L3392:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3389
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L3392
.L3390:
	movq	-112(%rbp), %r12
.L3388:
	testq	%r12, %r12
	je	.L3393
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3393:
	movq	-176(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L3394
	call	_ZdlPv@PLT
.L3394:
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %rdi
	movq	%rax, -392(%rbp)
	cmpq	%rdi, %rax
	je	.L3416
	movq	%rdi, %rbx
	jmp	.L3409
	.p2align 4,,10
	.p2align 3
.L3559:
	movq	%r13, %rsi
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	(%r15), %rsi
	movq	%rax, %rdi
	movq	8(%r15), %rax
	subq	%rsi, %rax
	cmpq	%rax, %r12
	je	.L3399
.L3400:
	call	_ZdlPv@PLT
	addq	$8, %rbx
	cmpq	%rbx, -392(%rbp)
	je	.L3557
.L3409:
	movabsq	$1152921504606846975, %rcx
	movq	(%rbx), %rax
	movq	264(%rax), %r14
	movq	288(%rax), %rdx
	movq	256(%rax), %rax
	movq	%r14, %r12
	leaq	(%rax,%rdx,8), %r13
	subq	%r13, %r12
	movq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	ja	.L3558
	testq	%rax, %rax
	je	.L3417
	movq	%r12, %rdi
.LEHB232:
	call	_Znwm@PLT
	movq	%rax, %rdi
.L3397:
	cmpq	%r13, %r14
	jne	.L3559
	movq	(%r15), %rsi
	movq	8(%r15), %rax
	subq	%rsi, %rax
	cmpq	%rax, %r12
	je	.L3399
.L3401:
	testq	%rdi, %rdi
	jne	.L3400
	addq	$8, %rbx
	cmpq	%rbx, -392(%rbp)
	jne	.L3409
.L3557:
	movq	-208(%rbp), %rdi
	xorl	%r12d, %r12d
	jmp	.L3395
	.p2align 4,,10
	.p2align 3
.L3399:
	testq	%r12, %r12
	jne	.L3560
.L3405:
	movq	(%rbx), %r12
	cmpb	$0, 280(%r12)
	jne	.L3401
	testq	%rdi, %rdi
	je	.L3407
	call	_ZdlPv@PLT
.L3407:
	movq	-208(%rbp), %rdi
.L3395:
	testq	%rdi, %rdi
	je	.L3298
	call	_ZdlPv@PLT
.L3298:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3561
	addq	$408, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3389:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L3392
	jmp	.L3390
	.p2align 4,,10
	.p2align 3
.L3417:
	xorl	%edi, %edi
	jmp	.L3397
	.p2align 4,,10
	.p2align 3
.L3560:
	movq	%r12, %rdx
	movq	%rdi, -400(%rbp)
	call	memcmp@PLT
	movq	-400(%rbp), %rdi
	testl	%eax, %eax
	jne	.L3400
	jmp	.L3405
	.p2align 4,,10
	.p2align 3
.L3540:
	leaq	-72(%rbp), %rax
	movdqa	-160(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	-168(%rbp), %rdx
	movb	$0, -160(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, -424(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
.L3305:
	movdqa	-128(%rbp), %xmm5
	movups	%xmm5, -72(%rbp)
	jmp	.L3307
	.p2align 4,,10
	.p2align 3
.L3542:
	leaq	-112(%rbp), %rax
	leaq	-208(%rbp), %r12
	movq	%rax, -400(%rbp)
	jmp	.L3309
	.p2align 4,,10
	.p2align 3
.L3543:
	leaq	-112(%rbp), %rax
	leaq	-208(%rbp), %r12
	movq	%rax, -400(%rbp)
	jmp	.L3310
.L3544:
	leaq	-112(%rbp), %rax
	leaq	-208(%rbp), %r12
	movq	%rax, -400(%rbp)
	jmp	.L3311
.L3416:
	xorl	%r12d, %r12d
	jmp	.L3395
.L3558:
	leaq	.LC25(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE232:
.L3553:
	movq	-392(%rbp), %rdi
	jmp	.L3301
.L3561:
	call	__stack_chk_fail@PLT
.L3538:
	leaq	.LC16(%rip), %rdi
.LEHB233:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE233:
.L3418:
	endbr64
	movq	%rax, %r12
	leaq	-112(%rbp), %rax
	movq	%rax, -400(%rbp)
	jmp	.L3375
.L3427:
	endbr64
	movq	%rax, %r12
	jmp	.L3325
.L3419:
	endbr64
	movq	%rax, %r12
	jmp	.L3363
.L3428:
	endbr64
	movq	%rax, %r12
	jmp	.L3323
.L3420:
	endbr64
	movq	%rax, %r12
	jmp	.L3372
.L3423:
	endbr64
	movq	%rax, %r12
	jmp	.L3339
.L3424:
	endbr64
	movq	%rax, %r12
	jmp	.L3348
.L3421:
	endbr64
	movq	%rax, %r12
	jmp	.L3351
.L3422:
	endbr64
	movq	%rax, %r12
	jmp	.L3360
.L3425:
	endbr64
	movq	%rax, %r12
	jmp	.L3327
.L3426:
	endbr64
	movq	%rax, %r12
	jmp	.L3336
.L3430:
	endbr64
	movq	%rax, %r12
	jmp	.L3402
.L3429:
	endbr64
	movq	%rax, %r12
	jmp	.L3381
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE,"a",@progbits
.LLSDA6529:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6529-.LLSDACSB6529
.LLSDACSB6529:
	.uleb128 .LEHB219-.LFB6529
	.uleb128 .LEHE219-.LEHB219
	.uleb128 .L3418-.LFB6529
	.uleb128 0
	.uleb128 .LEHB220-.LFB6529
	.uleb128 .LEHE220-.LEHB220
	.uleb128 .L3427-.LFB6529
	.uleb128 0
	.uleb128 .LEHB221-.LFB6529
	.uleb128 .LEHE221-.LEHB221
	.uleb128 .L3428-.LFB6529
	.uleb128 0
	.uleb128 .LEHB222-.LFB6529
	.uleb128 .LEHE222-.LEHB222
	.uleb128 .L3425-.LFB6529
	.uleb128 0
	.uleb128 .LEHB223-.LFB6529
	.uleb128 .LEHE223-.LEHB223
	.uleb128 .L3426-.LFB6529
	.uleb128 0
	.uleb128 .LEHB224-.LFB6529
	.uleb128 .LEHE224-.LEHB224
	.uleb128 .L3423-.LFB6529
	.uleb128 0
	.uleb128 .LEHB225-.LFB6529
	.uleb128 .LEHE225-.LEHB225
	.uleb128 .L3424-.LFB6529
	.uleb128 0
	.uleb128 .LEHB226-.LFB6529
	.uleb128 .LEHE226-.LEHB226
	.uleb128 .L3421-.LFB6529
	.uleb128 0
	.uleb128 .LEHB227-.LFB6529
	.uleb128 .LEHE227-.LEHB227
	.uleb128 .L3422-.LFB6529
	.uleb128 0
	.uleb128 .LEHB228-.LFB6529
	.uleb128 .LEHE228-.LEHB228
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB229-.LFB6529
	.uleb128 .LEHE229-.LEHB229
	.uleb128 .L3419-.LFB6529
	.uleb128 0
	.uleb128 .LEHB230-.LFB6529
	.uleb128 .LEHE230-.LEHB230
	.uleb128 .L3420-.LFB6529
	.uleb128 0
	.uleb128 .LEHB231-.LFB6529
	.uleb128 .LEHE231-.LEHB231
	.uleb128 .L3429-.LFB6529
	.uleb128 0
	.uleb128 .LEHB232-.LFB6529
	.uleb128 .LEHE232-.LEHB232
	.uleb128 .L3430-.LFB6529
	.uleb128 0
	.uleb128 .LEHB233-.LFB6529
	.uleb128 .LEHE233-.LEHB233
	.uleb128 0
	.uleb128 0
.LLSDACSE6529:
	.section	.text._ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6529
	.type	_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE.cold, @function
_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE.cold:
.LFSB6529:
.L3323:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3325
	call	_ZdlPv@PLT
.L3325:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3327
	call	_ZdlPv@PLT
.L3327:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3339
	call	_ZdlPv@PLT
.L3339:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3351
	call	_ZdlPv@PLT
.L3351:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3363
	call	_ZdlPv@PLT
.L3363:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3375
	call	_ZdlPv@PLT
.L3375:
	movq	-400(%rbp), %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	-176(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L3404
.L3403:
	call	_ZdlPv@PLT
.L3404:
	movq	%r12, %rdi
.LEHB234:
	call	_Unwind_Resume@PLT
.LEHE234:
.L3372:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3363
	call	_ZdlPv@PLT
	jmp	.L3363
.L3348:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3339
	call	_ZdlPv@PLT
	jmp	.L3339
.L3360:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3351
	call	_ZdlPv@PLT
	jmp	.L3351
.L3336:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3327
	call	_ZdlPv@PLT
	jmp	.L3327
.L3402:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3403
	jmp	.L3404
.L3381:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3382
	call	_ZdlPv@PLT
.L3382:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3375
	call	_ZdlPv@PLT
	jmp	.L3375
	.cfi_endproc
.LFE6529:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
.LLSDAC6529:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6529-.LLSDACSBC6529
.LLSDACSBC6529:
	.uleb128 .LEHB234-.LCOLDB49
	.uleb128 .LEHE234-.LEHB234
	.uleb128 0
	.uleb128 0
.LLSDACSEC6529:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.section	.text._ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.size	_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE, .-_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
	.size	_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE.cold, .-_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE.cold
.LCOLDE49:
	.section	.text._ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
.LHOTE49:
	.section	.text._ZNSt6vectorIPN2v88internal6torque7BuiltinESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque7BuiltinESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque7BuiltinESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque7BuiltinESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque7BuiltinESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB11869:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3576
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L3572
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L3577
.L3564:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L3571:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L3578
	testq	%r13, %r13
	jg	.L3567
	testq	%r9, %r9
	jne	.L3570
.L3568:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3578:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L3567
.L3570:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L3568
	.p2align 4,,10
	.p2align 3
.L3577:
	testq	%rsi, %rsi
	jne	.L3565
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L3571
	.p2align 4,,10
	.p2align 3
.L3567:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L3568
	jmp	.L3570
	.p2align 4,,10
	.p2align 3
.L3572:
	movl	$8, %r14d
	jmp	.L3564
.L3576:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3565:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L3564
	.cfi_endproc
.LFE11869:
	.size	_ZNSt6vectorIPN2v88internal6torque7BuiltinESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque7BuiltinESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE,"ax",@progbits
	.align 2
.LCOLDB50:
	.section	.text._ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE,"ax",@progbits
.LHOTB50:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE
	.type	_ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE, @function
_ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE:
.LFB6539:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6539
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB235:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE235:
	pxor	%xmm0, %xmm0
	movq	(%rax), %rax
	movq	(%rax), %r15
	movaps	%xmm0, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	16(%r15), %r14
	testq	%r14, %r14
	je	.L3580
	movq	$0, -192(%rbp)
	movq	16(%r14), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L3581
	movq	$0, -160(%rbp)
	movq	16(%rax), %rax
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -280(%rbp)
	testq	%rax, %rax
	je	.L3582
	movq	$0, -128(%rbp)
	movq	16(%rax), %r12
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -144(%rbp)
	testq	%r12, %r12
	je	.L3583
	movq	$0, -96(%rbp)
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -112(%rbp)
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.L3584
	movq	%rbx, %rdx
	movq	%r13, %rdi
.LEHB236:
	call	_ZN2v88internal6torque5Scope6LookupERKNS1_13QualifiedNameE
	movdqa	-80(%rbp), %xmm5
	movq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	-112(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
	testq	%rdi, %rdi
	je	.L3584
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3584
	call	_ZdlPv@PLT
.L3584:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE236:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	%rax, -264(%rbp)
	cmpq	%rax, %r12
	je	.L3588
	leaq	-248(%rbp), %rax
	movq	%rax, -288(%rbp)
	jmp	.L3591
	.p2align 4,,10
	.p2align 3
.L3772:
	movq	%rax, (%rsi)
	addq	$8, %r12
	addq	$8, -104(%rbp)
	cmpq	%r12, -264(%rbp)
	je	.L3771
.L3591:
	movq	(%r12), %rax
	movq	-104(%rbp), %rsi
	movq	%rax, -248(%rbp)
	cmpq	-96(%rbp), %rsi
	jne	.L3772
	movq	-288(%rbp), %rdx
	leaq	-112(%rbp), %rdi
.LEHB237:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE237:
	addq	$8, %r12
	cmpq	%r12, -264(%rbp)
	jne	.L3591
	.p2align 4,,10
	.p2align 3
.L3771:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L3588:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L3592
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3592:
	movdqa	-112(%rbp), %xmm4
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	movq	-144(%rbp), %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	testq	%rdi, %rdi
	je	.L3583
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3583
	call	_ZdlPv@PLT
.L3583:
	movq	-280(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
.LEHB238:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE238:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	%rax, -264(%rbp)
	cmpq	%rax, %r12
	je	.L3601
	leaq	-112(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L3604
	.p2align 4,,10
	.p2align 3
.L3774:
	movq	%rax, (%rsi)
	addq	$8, %r12
	addq	$8, -136(%rbp)
	cmpq	%r12, -264(%rbp)
	je	.L3773
.L3604:
	movq	(%r12), %rax
	movq	-136(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-128(%rbp), %rsi
	jne	.L3774
	movq	-280(%rbp), %rdx
	leaq	-144(%rbp), %rdi
.LEHB239:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE239:
	addq	$8, %r12
	cmpq	%r12, -264(%rbp)
	jne	.L3604
	.p2align 4,,10
	.p2align 3
.L3773:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L3601:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L3605
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3605:
	movdqa	-144(%rbp), %xmm3
	movq	-128(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movq	-176(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm3, -176(%rbp)
	testq	%rdi, %rdi
	je	.L3582
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3582
	call	_ZdlPv@PLT
.L3582:
	movq	-272(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
.LEHB240:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE240:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %r12
	movq	%rax, -264(%rbp)
	cmpq	%rax, %r12
	je	.L3613
	leaq	-112(%rbp), %rax
	movq	%rax, -272(%rbp)
	jmp	.L3616
	.p2align 4,,10
	.p2align 3
.L3776:
	movq	%rax, (%rsi)
	addq	$8, %r12
	addq	$8, -168(%rbp)
	cmpq	%r12, -264(%rbp)
	je	.L3775
.L3616:
	movq	(%r12), %rax
	movq	-168(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-160(%rbp), %rsi
	jne	.L3776
	movq	-272(%rbp), %rdx
	leaq	-176(%rbp), %rdi
.LEHB241:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE241:
	addq	$8, %r12
	cmpq	%r12, -264(%rbp)
	jne	.L3616
	.p2align 4,,10
	.p2align 3
.L3775:
	movq	-80(%rbp), %rax
	movq	%rax, -264(%rbp)
.L3613:
	movq	-264(%rbp), %rax
	testq	%rax, %rax
	je	.L3617
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3617:
	movdqa	-176(%rbp), %xmm2
	movq	-160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movq	-208(%rbp), %rdi
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -192(%rbp)
	movaps	%xmm2, -208(%rbp)
	testq	%rdi, %rdi
	je	.L3581
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3581
	call	_ZdlPv@PLT
.L3581:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
.LEHB242:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE242:
	movq	-80(%rbp), %r12
	movq	-72(%rbp), %r14
	cmpq	%r14, %r12
	je	.L3625
	leaq	-112(%rbp), %rax
	movq	%rax, -264(%rbp)
	jmp	.L3628
	.p2align 4,,10
	.p2align 3
.L3778:
	addq	$8, %r12
	movq	%rax, (%rsi)
	addq	$8, -200(%rbp)
	cmpq	%r12, %r14
	je	.L3777
.L3628:
	movq	(%r12), %rax
	movq	-200(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-192(%rbp), %rsi
	jne	.L3778
	movq	-264(%rbp), %rdx
	leaq	-208(%rbp), %rdi
.LEHB243:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE243:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L3628
	.p2align 4,,10
	.p2align 3
.L3777:
	movq	-80(%rbp), %r14
.L3625:
	testq	%r14, %r14
	je	.L3629
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3629:
	movdqa	-208(%rbp), %xmm1
	movq	-192(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	-240(%rbp), %rdi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -224(%rbp)
	movaps	%xmm1, -240(%rbp)
	testq	%rdi, %rdi
	je	.L3580
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3580
	call	_ZdlPv@PLT
.L3580:
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
.LEHB244:
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE244:
	movq	-80(%rbp), %r12
	movq	-72(%rbp), %r14
	cmpq	%r14, %r12
	je	.L3637
	leaq	-112(%rbp), %r15
	jmp	.L3640
	.p2align 4,,10
	.p2align 3
.L3780:
	addq	$8, %r12
	movq	%rax, (%rsi)
	addq	$8, -232(%rbp)
	cmpq	%r12, %r14
	je	.L3779
.L3640:
	movq	(%r12), %rax
	movq	-232(%rbp), %rsi
	movq	%rax, -112(%rbp)
	cmpq	-224(%rbp), %rsi
	jne	.L3780
	leaq	-240(%rbp), %rdi
	movq	%r15, %rdx
.LEHB245:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE245:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L3640
	.p2align 4,,10
	.p2align 3
.L3779:
	movq	-80(%rbp), %r14
.L3637:
	testq	%r14, %r14
	je	.L3641
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3641:
	movq	-232(%rbp), %r14
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %r15
	movq	-240(%rbp), %r12
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	cmpq	%r14, %r12
	jne	.L3651
	jmp	.L3643
	.p2align 4,,10
	.p2align 3
.L3782:
	movq	%rax, (%rsi)
	addq	$8, -72(%rbp)
.L3648:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L3781
.L3651:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L3648
	cmpl	$4, 8(%rax)
	jne	.L3648
	movq	%rax, -112(%rbp)
	movq	-72(%rbp), %rsi
	cmpq	-64(%rbp), %rsi
	jne	.L3782
	movq	%r15, %rdx
	movq	%r13, %rdi
.LEHB246:
	call	_ZNSt6vectorIPN2v88internal6torque7BuiltinESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE246:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L3651
	.p2align 4,,10
	.p2align 3
.L3781:
	movq	-240(%rbp), %r14
.L3643:
	testq	%r14, %r14
	je	.L3656
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3656:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rdi
	cmpq	%rdi, %rax
	je	.L3783
	leaq	.LC10(%rip), %rcx
	subq	%rdi, %rax
	movq	%rcx, -112(%rbp)
	cmpq	$8, %rax
	jne	.L3784
	movq	(%rdi), %r12
	movl	$1, %ebx
.L3659:
	call	_ZdlPv@PLT
.L3660:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3785
	addq	$248, %rsp
	movl	%ebx, %eax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3783:
	.cfi_restore_state
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	testq	%rdi, %rdi
	je	.L3660
	jmp	.L3659
.L3785:
	call	__stack_chk_fail@PLT
.L3784:
	leaq	24(%rbx), %rcx
	leaq	-112(%rbp), %rsi
	leaq	.LC33(%rip), %rdx
	leaq	.LC34(%rip), %rdi
.LEHB247:
	call	_ZN2v88internal6torque11ReportErrorIJRA24_KcRPS3_RA2_S3_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE247:
.L3669:
	endbr64
	movq	%rax, %r12
	jmp	.L3623
.L3665:
	endbr64
	movq	%rax, %r12
	jmp	.L3662
.L3675:
	endbr64
	movq	%rax, %r12
	jmp	.L3597
.L3676:
	endbr64
	movq	%rax, %r12
	jmp	.L3595
.L3666:
	endbr64
	movq	%rax, %r12
	jmp	.L3652
.L3668:
	endbr64
	movq	%rax, %r12
	jmp	.L3644
.L3671:
	endbr64
	movq	%rax, %r12
	jmp	.L3611
.L3672:
	endbr64
	movq	%rax, %r12
	jmp	.L3620
.L3667:
	endbr64
	movq	%rax, %r12
	jmp	.L3635
.L3673:
	endbr64
	movq	%rax, %r12
	jmp	.L3599
.L3674:
	endbr64
	movq	%rax, %r12
	jmp	.L3608
.L3670:
	endbr64
	movq	%rax, %r12
	jmp	.L3632
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE,"a",@progbits
.LLSDA6539:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6539-.LLSDACSB6539
.LLSDACSB6539:
	.uleb128 .LEHB235-.LFB6539
	.uleb128 .LEHE235-.LEHB235
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB236-.LFB6539
	.uleb128 .LEHE236-.LEHB236
	.uleb128 .L3675-.LFB6539
	.uleb128 0
	.uleb128 .LEHB237-.LFB6539
	.uleb128 .LEHE237-.LEHB237
	.uleb128 .L3676-.LFB6539
	.uleb128 0
	.uleb128 .LEHB238-.LFB6539
	.uleb128 .LEHE238-.LEHB238
	.uleb128 .L3673-.LFB6539
	.uleb128 0
	.uleb128 .LEHB239-.LFB6539
	.uleb128 .LEHE239-.LEHB239
	.uleb128 .L3674-.LFB6539
	.uleb128 0
	.uleb128 .LEHB240-.LFB6539
	.uleb128 .LEHE240-.LEHB240
	.uleb128 .L3671-.LFB6539
	.uleb128 0
	.uleb128 .LEHB241-.LFB6539
	.uleb128 .LEHE241-.LEHB241
	.uleb128 .L3672-.LFB6539
	.uleb128 0
	.uleb128 .LEHB242-.LFB6539
	.uleb128 .LEHE242-.LEHB242
	.uleb128 .L3669-.LFB6539
	.uleb128 0
	.uleb128 .LEHB243-.LFB6539
	.uleb128 .LEHE243-.LEHB243
	.uleb128 .L3670-.LFB6539
	.uleb128 0
	.uleb128 .LEHB244-.LFB6539
	.uleb128 .LEHE244-.LEHB244
	.uleb128 .L3667-.LFB6539
	.uleb128 0
	.uleb128 .LEHB245-.LFB6539
	.uleb128 .LEHE245-.LEHB245
	.uleb128 .L3668-.LFB6539
	.uleb128 0
	.uleb128 .LEHB246-.LFB6539
	.uleb128 .LEHE246-.LEHB246
	.uleb128 .L3666-.LFB6539
	.uleb128 0
	.uleb128 .LEHB247-.LFB6539
	.uleb128 .LEHE247-.LEHB247
	.uleb128 .L3665-.LFB6539
	.uleb128 0
.LLSDACSE6539:
	.section	.text._ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6539
	.type	_ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE.cold, @function
_ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE.cold:
.LFSB6539:
.L3595:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3597
	call	_ZdlPv@PLT
.L3597:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3599
	call	_ZdlPv@PLT
.L3599:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3611
	call	_ZdlPv@PLT
.L3611:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3623
	call	_ZdlPv@PLT
.L3623:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3635
	call	_ZdlPv@PLT
.L3635:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3646
	call	_ZdlPv@PLT
.L3646:
	movq	%r12, %rdi
.LEHB248:
	call	_Unwind_Resume@PLT
.L3662:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3663
	call	_ZdlPv@PLT
.L3663:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L3652:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3653
	call	_ZdlPv@PLT
.L3653:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3655
	call	_ZdlPv@PLT
.L3655:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE248:
.L3644:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3635
	call	_ZdlPv@PLT
	jmp	.L3635
.L3608:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3599
	call	_ZdlPv@PLT
	jmp	.L3599
.L3620:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3611
	call	_ZdlPv@PLT
	jmp	.L3611
.L3632:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3623
	call	_ZdlPv@PLT
	jmp	.L3623
	.cfi_endproc
.LFE6539:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE
.LLSDAC6539:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6539-.LLSDACSBC6539
.LLSDACSBC6539:
	.uleb128 .LEHB248-.LCOLDB50
	.uleb128 .LEHE248-.LEHB248
	.uleb128 0
	.uleb128 0
.LLSDACSEC6539:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE
	.section	.text._ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE, .-_ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE
	.size	_ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE.cold, .-_ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE.cold
.LCOLDE50:
	.section	.text._ZN2v88internal6torque12Declarations16TryLookupBuiltinERKNS1_13QualifiedNameE
.LHOTE50:
	.section	.text._ZNSt6vectorIPN2v88internal6torque15RuntimeFunctionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque15RuntimeFunctionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque15RuntimeFunctionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque15RuntimeFunctionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque15RuntimeFunctionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB11980:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3800
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L3796
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L3801
.L3788:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L3795:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L3802
	testq	%r13, %r13
	jg	.L3791
	testq	%r9, %r9
	jne	.L3794
.L3792:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3802:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L3791
.L3794:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L3792
	.p2align 4,,10
	.p2align 3
.L3801:
	testq	%rsi, %rsi
	jne	.L3789
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L3795
	.p2align 4,,10
	.p2align 3
.L3791:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L3792
	jmp	.L3794
	.p2align 4,,10
	.p2align 3
.L3796:
	movl	$8, %r14d
	jmp	.L3788
.L3800:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3789:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L3788
	.cfi_endproc
.LFE11980:
	.size	_ZNSt6vectorIPN2v88internal6torque15RuntimeFunctionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque15RuntimeFunctionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB12023:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3817
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L3813
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L3818
.L3805:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L3812:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L3819
	testq	%r13, %r13
	jg	.L3808
	testq	%r9, %r9
	jne	.L3811
.L3809:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3819:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L3808
.L3811:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L3809
	.p2align 4,,10
	.p2align 3
.L3818:
	testq	%rsi, %rsi
	jne	.L3806
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L3812
	.p2align 4,,10
	.p2align 3
.L3808:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L3809
	jmp	.L3811
	.p2align 4,,10
	.p2align 3
.L3813:
	movl	$8, %r14d
	jmp	.L3805
.L3817:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3806:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L3805
	.cfi_endproc
.LFE12023:
	.size	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZN2v88internal6torque12Declarations15DeclareOperatorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_5MacroE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations15DeclareOperatorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_5MacroE
	.type	_ZN2v88internal6torque12Declarations15DeclareOperatorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_5MacroE, @function
_ZN2v88internal6torque12Declarations15DeclareOperatorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_5MacroE:
.LFB6610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	addq	$72, %rdi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	%r12, -32(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L3821
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L3822:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3825
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3821:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L3822
.L3825:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6610:
	.size	_ZN2v88internal6torque12Declarations15DeclareOperatorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_5MacroE, .-_ZN2v88internal6torque12Declarations15DeclareOperatorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_5MacroE
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB12500:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3850
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L3841
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L3851
.L3828:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L3840:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L3830
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L3834:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3831
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L3834
.L3832:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L3830:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L3835
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L3843
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L3837:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L3837
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L3838
.L3836:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L3838:
	leaq	8(%rcx,%r9), %rcx
.L3835:
	testq	%r12, %r12
	je	.L3839
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L3839:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3831:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L3834
	jmp	.L3832
	.p2align 4,,10
	.p2align 3
.L3851:
	testq	%rdi, %rdi
	jne	.L3829
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L3840
	.p2align 4,,10
	.p2align 3
.L3841:
	movl	$8, %r13d
	jmp	.L3828
.L3843:
	movq	%rcx, %rdx
	jmp	.L3836
.L3829:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L3828
.L3850:
	leaq	.LC23(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12500:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.rodata._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE.str1.1,"aMS",@progbits,1
.LC51:
	.string	"basic_string::append"
.LC52:
	.string	"_"
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE,"ax",@progbits
	.align 2
.LCOLDB53:
	.section	.text._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE,"ax",@progbits
.LHOTB53:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE
	.type	_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE, @function
_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE:
.LFB6602:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6602
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	32(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$168, %rsp
	movq	%rsi, -184(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB249:
	call	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC17(%rip), %rcx
	movq	(%rax), %rax
	movq	232(%rax), %r8
	leaq	1(%r8), %rdx
	movq	%rdx, 232(%rax)
	leaq	-96(%rbp), %rax
	movl	$32, %edx
	movq	%rax, -200(%rbp)
	movq	%rax, %rdi
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE249:
	movq	32(%rbx), %r15
	movq	40(%rbx), %r12
	movq	%r13, -128(%rbp)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L3853
	testq	%r15, %r15
	je	.L3930
.L3853:
	movq	%r12, -168(%rbp)
	cmpq	$15, %r12
	ja	.L3931
	cmpq	$1, %r12
	jne	.L3856
	movzbl	(%r15), %eax
	movb	%al, -112(%rbp)
	movq	%r13, %rax
.L3857:
	movq	%r12, -120(%rbp)
	movb	$0, (%rax,%r12)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -120(%rbp)
	je	.L3932
	leaq	-128(%rbp), %r15
	movl	$1, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r15, %rdi
.LEHB250:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE250:
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %r8
	movl	$15, %eax
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	movq	-96(%rbp), %rsi
	cmpq	%r13, %r9
	cmovne	-112(%rbp), %rdi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L3933
	leaq	-80(%rbp), %r12
	cmpq	%r12, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L3934
.L3864:
	movq	%r15, %rdi
.LEHB251:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE251:
.L3866:
	leaq	-144(%rbp), %r15
	leaq	16(%rax), %rdx
	movq	%r15, -160(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L3935
	movq	%rcx, -160(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L3868:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -152(%rbp)
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	cmpq	%r13, %rdi
	je	.L3869
	call	_ZdlPv@PLT
.L3869:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L3870
	call	_ZdlPv@PLT
.L3870:
	movq	-160(%rbp), %rax
	movq	%r12, -96(%rbp)
	cmpq	%r15, %rax
	je	.L3936
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -80(%rbp)
.L3872:
	movq	-152(%rbp), %rax
	movl	$208, %edi
	movq	%r15, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	%rax, -88(%rbp)
	movb	$0, -144(%rbp)
.LEHB252:
	call	_Znwm@PLT
.LEHE252:
	movq	%rax, %r13
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, 0(%r13)
	movl	$11, 8(%r13)
.LEHB253:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r13)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE253:
	movq	(%rax), %rax
	pcmpeqd	%xmm0, %xmm0
	movq	%rbx, %xmm2
	movl	$-1, 60(%r13)
	movups	%xmm0, 44(%r13)
	movq	-184(%rbp), %xmm0
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movb	$1, 64(%r13)
	punpcklqdq	%xmm2, %xmm0
	movb	$0, 88(%r13)
	movl	%eax, 40(%r13)
	leaq	16+_ZTVN2v88internal6torque17NamespaceConstantE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	184(%r13), %rax
	movq	%rax, 168(%r13)
	movq	-96(%rbp), %rax
	movb	$0, 96(%r13)
	movups	%xmm1, 24(%r13)
	movups	%xmm0, 72(%r13)
	cmpq	%r12, %rax
	je	.L3937
	movq	%rax, 168(%r13)
	movq	-80(%rbp), %rax
	movq	%rax, 184(%r13)
.L3874:
	movq	-88(%rbp), %rax
	movq	%rax, 176(%r13)
	movq	-192(%rbp), %rax
	movq	%rax, 200(%r13)
.LEHB254:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE254:
	movq	(%rax), %rax
	movq	(%rax), %rbx
.LEHB255:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE255:
	movq	(%rax), %rdi
	movq	%r13, -168(%rbp)
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	je	.L3875
	movq	$0, -168(%rbp)
	movq	%r13, (%rsi)
	addq	$8, 120(%rdi)
.L3876:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3877
	movq	(%rdi), %rax
	call	*8(%rax)
.L3877:
	leaq	72(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r13, -168(%rbp)
.LEHB256:
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
.LEHE256:
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L3938
	movq	-168(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 8(%rax)
.L3884:
	movq	-160(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3852
	call	_ZdlPv@PLT
.L3852:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3939
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3856:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L3940
	movq	%r13, %rax
	jmp	.L3857
	.p2align 4,,10
	.p2align 3
.L3931:
	leaq	-168(%rbp), %rsi
	leaq	-128(%rbp), %rdi
	xorl	%edx, %edx
.LEHB257:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE257:
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, -112(%rbp)
.L3855:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-168(%rbp), %r12
	movq	-128(%rbp), %rax
	jmp	.L3857
	.p2align 4,,10
	.p2align 3
.L3933:
	leaq	-80(%rbp), %r12
	jmp	.L3864
	.p2align 4,,10
	.p2align 3
.L3937:
	movdqa	-80(%rbp), %xmm5
	movups	%xmm5, 184(%r13)
	jmp	.L3874
	.p2align 4,,10
	.p2align 3
.L3936:
	movdqa	-144(%rbp), %xmm4
	movaps	%xmm4, -80(%rbp)
	jmp	.L3872
	.p2align 4,,10
	.p2align 3
.L3935:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -144(%rbp)
	jmp	.L3868
	.p2align 4,,10
	.p2align 3
.L3934:
	movq	-200(%rbp), %rdi
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB258:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE258:
	jmp	.L3866
	.p2align 4,,10
	.p2align 3
.L3938:
	leaq	-168(%rbp), %rdx
	movq	%rax, %rdi
.LEHB259:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE259:
	jmp	.L3884
	.p2align 4,,10
	.p2align 3
.L3875:
	leaq	-168(%rbp), %rdx
	addq	$112, %rdi
.LEHB260:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE260:
	jmp	.L3876
.L3930:
	leaq	.LC16(%rip), %rdi
.LEHB261:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE261:
.L3932:
	leaq	.LC51(%rip), %rdi
.LEHB262:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE262:
.L3939:
	call	__stack_chk_fail@PLT
.L3940:
	movq	%r13, %rdi
	jmp	.L3855
.L3909:
	endbr64
	movq	%rax, %rbx
	jmp	.L3881
.L3908:
	endbr64
.L3929:
	movq	%rax, %rbx
	jmp	.L3896
.L3905:
	endbr64
	movq	%rax, %rbx
	jmp	.L3894
.L3903:
	endbr64
	movq	%rax, %rbx
	leaq	-80(%rbp), %r12
	jmp	.L3863
.L3904:
	endbr64
	movq	%rax, %rbx
	jmp	.L3890
.L3912:
	endbr64
	jmp	.L3929
.L3906:
	endbr64
	movq	%rax, %rbx
	jmp	.L3893
.L3911:
	endbr64
	movq	%rax, %rbx
	jmp	.L3897
.L3910:
	endbr64
	movq	%rax, %rbx
	jmp	.L3897
.L3907:
	endbr64
	movq	%rax, %rbx
	jmp	.L3861
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE,"a",@progbits
.LLSDA6602:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6602-.LLSDACSB6602
.LLSDACSB6602:
	.uleb128 .LEHB249-.LFB6602
	.uleb128 .LEHE249-.LEHB249
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB250-.LFB6602
	.uleb128 .LEHE250-.LEHB250
	.uleb128 .L3907-.LFB6602
	.uleb128 0
	.uleb128 .LEHB251-.LFB6602
	.uleb128 .LEHE251-.LEHB251
	.uleb128 .L3904-.LFB6602
	.uleb128 0
	.uleb128 .LEHB252-.LFB6602
	.uleb128 .LEHE252-.LEHB252
	.uleb128 .L3905-.LFB6602
	.uleb128 0
	.uleb128 .LEHB253-.LFB6602
	.uleb128 .LEHE253-.LEHB253
	.uleb128 .L3906-.LFB6602
	.uleb128 0
	.uleb128 .LEHB254-.LFB6602
	.uleb128 .LEHE254-.LEHB254
	.uleb128 .L3910-.LFB6602
	.uleb128 0
	.uleb128 .LEHB255-.LFB6602
	.uleb128 .LEHE255-.LEHB255
	.uleb128 .L3911-.LFB6602
	.uleb128 0
	.uleb128 .LEHB256-.LFB6602
	.uleb128 .LEHE256-.LEHB256
	.uleb128 .L3912-.LFB6602
	.uleb128 0
	.uleb128 .LEHB257-.LFB6602
	.uleb128 .LEHE257-.LEHB257
	.uleb128 .L3903-.LFB6602
	.uleb128 0
	.uleb128 .LEHB258-.LFB6602
	.uleb128 .LEHE258-.LEHB258
	.uleb128 .L3904-.LFB6602
	.uleb128 0
	.uleb128 .LEHB259-.LFB6602
	.uleb128 .LEHE259-.LEHB259
	.uleb128 .L3908-.LFB6602
	.uleb128 0
	.uleb128 .LEHB260-.LFB6602
	.uleb128 .LEHE260-.LEHB260
	.uleb128 .L3909-.LFB6602
	.uleb128 0
	.uleb128 .LEHB261-.LFB6602
	.uleb128 .LEHE261-.LEHB261
	.uleb128 .L3903-.LFB6602
	.uleb128 0
	.uleb128 .LEHB262-.LFB6602
	.uleb128 .LEHE262-.LEHB262
	.uleb128 .L3907-.LFB6602
	.uleb128 0
.LLSDACSE6602:
	.section	.text._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6602
	.type	_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE.cold, @function
_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE.cold:
.LFSB6602:
.L3881:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3896
	movq	(%rdi), %rax
	call	*8(%rax)
.L3896:
	movq	-160(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L3898
	call	_ZdlPv@PLT
.L3898:
	movq	%rbx, %rdi
.LEHB263:
	call	_Unwind_Resume@PLT
.L3893:
	movl	$208, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3894:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L3896
	call	_ZdlPv@PLT
	jmp	.L3896
.L3861:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3862
	call	_ZdlPv@PLT
.L3862:
	leaq	-80(%rbp), %r12
.L3863:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L3892
	call	_ZdlPv@PLT
.L3892:
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE263:
.L3890:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3863
	call	_ZdlPv@PLT
	jmp	.L3863
.L3897:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L3896
	.cfi_endproc
.LFE6602:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE
.LLSDAC6602:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6602-.LLSDACSBC6602
.LLSDACSBC6602:
	.uleb128 .LEHB263-.LCOLDB53
	.uleb128 .LEHE263-.LEHB263
	.uleb128 0
	.uleb128 0
.LLSDACSEC6602:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE
	.section	.text._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE
	.size	_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE, .-_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE
	.size	_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE.cold, .-_ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE.cold
.LCOLDE53:
	.section	.text._ZN2v88internal6torque12Declarations24DeclareNamespaceConstantEPNS1_10IdentifierEPKNS1_4TypeEPNS1_10ExpressionE
.LHOTE53:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.type	_ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, @function
_ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE:
.LFB9721:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9721
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB264:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE264:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%r12, -32(%rbp)
	movq	120(%rax), %rsi
	cmpq	128(%rax), %rsi
	je	.L3942
	movq	$0, -32(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rax)
.L3943:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3941
	movq	(%rdi), %rax
	call	*8(%rax)
.L3941:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3956
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3942:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	112(%rax), %rdi
.LEHB265:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE265:
	jmp	.L3943
.L3956:
	call	__stack_chk_fail@PLT
.L3948:
	endbr64
	movq	%rax, %r12
.L3945:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3946
	movq	(%rdi), %rax
	call	*8(%rax)
.L3946:
	movq	%r12, %rdi
.LEHB266:
	call	_Unwind_Resume@PLT
.LEHE266:
	.cfi_endproc
.LFE9721:
	.section	.gcc_except_table._ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"aG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
.LLSDA9721:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE9721-.LLSDACSB9721
.LLSDACSB9721:
	.uleb128 .LEHB264-.LFB9721
	.uleb128 .LEHE264-.LEHB264
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB265-.LFB9721
	.uleb128 .LEHE265-.LEHB265
	.uleb128 .L3948-.LFB9721
	.uleb128 0
	.uleb128 .LEHB266-.LFB9721
	.uleb128 .LEHE266-.LEHB266
	.uleb128 0
	.uleb128 0
.LLSDACSE9721:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.size	_ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, .-_ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE,"ax",@progbits
	.align 2
.LCOLDB55:
	.section	.text._ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE,"ax",@progbits
.LHOTB55:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE
	.type	_ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE, @function
_ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE:
.LFB6606:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6606
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$168, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB267:
	call	_Znwm@PLT
.LEHE267:
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
	movl	$8, 8(%r12)
.LEHB268:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE268:
	movq	(%rax), %rax
	movq	(%r14), %r15
	pcmpeqd	%xmm0, %xmm0
	leaq	88(%r12), %rdi
	movq	8(%r14), %r13
	movb	$1, 64(%r12)
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm0, 44(%r12)
	movl	$-1, 60(%r12)
	movl	%eax, 40(%r12)
	leaq	16+_ZTVN2v88internal6torque17GenericStructTypeE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r15, %rax
	addq	%r13, %rax
	movq	%rdi, 72(%r12)
	movups	%xmm1, 24(%r12)
	je	.L3958
	testq	%r15, %r15
	je	.L3991
.L3958:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L3992
	cmpq	$1, %r13
	jne	.L3961
	movzbl	(%r15), %eax
	movb	%al, 88(%r12)
.L3962:
	leaq	160(%r12), %rax
	movq	%r13, 80(%r12)
	movb	$0, (%rdi,%r13)
	movq	%rbx, 104(%r12)
	movq	%rax, 112(%r12)
	movq	$1, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movl	$0x3f800000, 144(%r12)
	movq	$0, 152(%r12)
	movq	$0, 160(%r12)
.LEHB269:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE269:
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	(%rax), %rbx
	movq	%r12, -64(%rbp)
.LEHB270:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_17GenericStructTypeEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	leaq	72(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %r12
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
.LEHE270:
	movq	%r12, -72(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L3963
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L3964:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3957
	movq	(%rdi), %rax
	call	*8(%rax)
.L3957:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3993
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3961:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L3962
	jmp	.L3960
	.p2align 4,,10
	.p2align 3
.L3992:
	leaq	-64(%rbp), %rsi
	leaq	72(%r12), %rdi
	xorl	%edx, %edx
.LEHB271:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE271:
	movq	%rax, 72(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 88(%r12)
.L3960:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	72(%r12), %rdi
	jmp	.L3962
	.p2align 4,,10
	.p2align 3
.L3963:
	leaq	-72(%rbp), %rdx
	movq	%rax, %rdi
.LEHB272:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE272:
	jmp	.L3964
.L3991:
	leaq	.LC16(%rip), %rdi
.LEHB273:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE273:
.L3993:
	call	__stack_chk_fail@PLT
.L3973:
	endbr64
	movq	%rax, %r13
	jmp	.L3966
.L3972:
	endbr64
	movq	%rax, %r13
	jmp	.L3969
.L3974:
	endbr64
	movq	%rax, %r13
	jmp	.L3970
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE,"a",@progbits
.LLSDA6606:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6606-.LLSDACSB6606
.LLSDACSB6606:
	.uleb128 .LEHB267-.LFB6606
	.uleb128 .LEHE267-.LEHB267
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB268-.LFB6606
	.uleb128 .LEHE268-.LEHB268
	.uleb128 .L3972-.LFB6606
	.uleb128 0
	.uleb128 .LEHB269-.LFB6606
	.uleb128 .LEHE269-.LEHB269
	.uleb128 .L3974-.LFB6606
	.uleb128 0
	.uleb128 .LEHB270-.LFB6606
	.uleb128 .LEHE270-.LEHB270
	.uleb128 .L3973-.LFB6606
	.uleb128 0
	.uleb128 .LEHB271-.LFB6606
	.uleb128 .LEHE271-.LEHB271
	.uleb128 .L3972-.LFB6606
	.uleb128 0
	.uleb128 .LEHB272-.LFB6606
	.uleb128 .LEHE272-.LEHB272
	.uleb128 .L3973-.LFB6606
	.uleb128 0
	.uleb128 .LEHB273-.LFB6606
	.uleb128 .LEHE273-.LEHB273
	.uleb128 .L3972-.LFB6606
	.uleb128 0
.LLSDACSE6606:
	.section	.text._ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6606
	.type	_ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE.cold, @function
_ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE.cold:
.LFSB6606:
.L3966:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3968
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3968
.L3969:
	movq	%r12, %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB274:
	call	_Unwind_Resume@PLT
.L3970:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L3968:
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE274:
	.cfi_endproc
.LFE6606:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE
.LLSDAC6606:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6606-.LLSDACSBC6606
.LLSDACSBC6606:
	.uleb128 .LEHB274-.LCOLDB55
	.uleb128 .LEHE274-.LEHB274
	.uleb128 0
	.uleb128 0
.LLSDACSEC6606:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE
	.section	.text._ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE
	.size	_ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE, .-_ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE
	.size	_ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE.cold, .-_ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE.cold
.LCOLDE55:
	.section	.text._ZN2v88internal6torque12Declarations24DeclareGenericStructTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_17StructDeclarationE
.LHOTE55:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.type	_ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, @function
_ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE:
.LFB8175:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8175
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB275:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE275:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%r12, -32(%rbp)
	movq	120(%rax), %rsi
	cmpq	128(%rax), %rsi
	je	.L3995
	movq	$0, -32(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rax)
.L3996:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3994
	movq	(%rdi), %rax
	call	*8(%rax)
.L3994:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4009
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3995:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	112(%rax), %rdi
.LEHB276:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE276:
	jmp	.L3996
.L4009:
	call	__stack_chk_fail@PLT
.L4001:
	endbr64
	movq	%rax, %r12
.L3998:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3999
	movq	(%rdi), %rax
	call	*8(%rax)
.L3999:
	movq	%r12, %rdi
.LEHB277:
	call	_Unwind_Resume@PLT
.LEHE277:
	.cfi_endproc
.LFE8175:
	.section	.gcc_except_table._ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"aG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
.LLSDA8175:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8175-.LLSDACSB8175
.LLSDACSB8175:
	.uleb128 .LEHB275-.LFB8175
	.uleb128 .LEHE275-.LEHB275
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB276-.LFB8175
	.uleb128 .LEHE276-.LEHB276
	.uleb128 .L4001-.LFB8175
	.uleb128 0
	.uleb128 .LEHB277-.LFB8175
	.uleb128 .LEHE277-.LEHB277
	.uleb128 0
	.uleb128 0
.LLSDACSE8175:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.size	_ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, .-_ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.section	.rodata._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"Varargs are not supported for intrinsics."
	.section	.text.unlikely._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE,"ax",@progbits
	.align 2
.LCOLDB57:
	.section	.text._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE,"ax",@progbits
.LHOTB57:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.type	_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE, @function
_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE:
.LFB6588:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6588
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$312, %rsp
	movq	(%rdi), %r13
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-288(%rbp), %rax
	movq	%rax, -336(%rbp)
	movq	%rax, -304(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L4011
	testq	%r13, %r13
	je	.L4155
.L4011:
	movq	%r12, -312(%rbp)
	cmpq	$15, %r12
	ja	.L4156
	cmpq	$1, %r12
	jne	.L4014
	movzbl	0(%r13), %eax
	movb	%al, -288(%rbp)
	movq	-336(%rbp), %rax
.L4015:
	movq	%r12, -296(%rbp)
	movl	$360, %edi
	movb	$0, (%rax,%r12)
.LEHB278:
	call	_Znwm@PLT
.LEHE278:
	movq	%rax, %r12
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	%rax, %r13
	movaps	%xmm0, -208(%rbp)
	subq	%rsi, %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	je	.L4157
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L4158
	movq	%r13, %rdi
.LEHB279:
	call	_Znwm@PLT
.LEHE279:
	movq	%rax, %rcx
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L4017:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	%rsi, %rax
	je	.L4019
	movq	%rcx, %rdi
	movq	%r14, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L4019:
	addq	%r14, %rcx
	cmpb	$0, 24(%rbx)
	movb	$0, -184(%rbp)
	movq	%rcx, -200(%rbp)
	movb	$0, -176(%rbp)
	jne	.L4159
.L4020:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	je	.L4160
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L4161
	movq	%r13, %rdi
.LEHB280:
	call	_Znwm@PLT
.LEHE280:
	movq	%rax, %rcx
	movq	72(%rbx), %rax
	movq	64(%rbx), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L4022:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	cmpq	%rsi, %rax
	je	.L4027
	movq	%rcx, %rdi
	movq	%r14, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L4027:
	movzbl	88(%rbx), %eax
	movq	120(%rbx), %r15
	addq	%r14, %rcx
	movq	$0, -96(%rbp)
	movq	112(%rbx), %r13
	movq	%rcx, -136(%rbp)
	movb	%al, -120(%rbp)
	movq	96(%rbx), %rax
	movq	%r15, %r14
	subq	%r13, %r14
	movq	$0, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	104(%rbx), %rax
	movq	$0, -80(%rbp)
	movq	%rax, -104(%rbp)
	movq	%r14, %rax
	sarq	$5, %rax
	je	.L4162
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L4163
	movq	%r14, %rdi
.LEHB281:
	call	_Znwm@PLT
.LEHE281:
	movq	120(%rbx), %r15
	movq	112(%rbx), %r13
	movq	%rax, -344(%rbp)
.L4029:
	movq	-344(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, -80(%rbp)
	movq	%rax, %r14
	movaps	%xmm0, -96(%rbp)
	cmpq	%r13, %r15
	jne	.L4038
	jmp	.L4031
	.p2align 4,,10
	.p2align 3
.L4032:
	movabsq	$1152921504606846975, %rcx
	cmpq	%rcx, %rax
	ja	.L4164
	movq	-328(%rbp), %rdi
.LEHB282:
	call	_Znwm@PLT
.LEHE282:
	movq	%rax, %rcx
.L4033:
	movq	-328(%rbp), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	addq	%rcx, %rax
	movups	%xmm0, 8(%r14)
	movq	%rax, 24(%r14)
	movq	16(%r13), %rax
	movq	8(%r13), %rsi
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%rsi, %rax
	je	.L4153
	movq	%rcx, %rdi
	movq	%rdx, -328(%rbp)
	call	memmove@PLT
	movq	-328(%rbp), %rdx
	movq	%rax, %rcx
.L4153:
	addq	%rdx, %rcx
	addq	$32, %r13
	addq	$32, %r14
	movq	%rcx, -16(%r14)
	cmpq	%r13, %r15
	je	.L4031
.L4038:
	movq	0(%r13), %rax
	movq	%rax, (%r14)
	movq	16(%r13), %rax
	subq	8(%r13), %rax
	movq	$0, 8(%r14)
	movq	%rax, -328(%rbp)
	sarq	$3, %rax
	movq	$0, 16(%r14)
	movq	$0, 24(%r14)
	jne	.L4032
	xorl	%ecx, %ecx
	jmp	.L4033
	.p2align 4,,10
	.p2align 3
.L4014:
	testq	%r12, %r12
	jne	.L4165
	movq	-336(%rbp), %rax
	jmp	.L4015
	.p2align 4,,10
	.p2align 3
.L4031:
	movzbl	136(%rbx), %eax
	movq	-304(%rbp), %r15
	movq	%r14, -88(%rbp)
	leaq	-224(%rbp), %r14
	movq	-296(%rbp), %r13
	movq	%r14, -240(%rbp)
	movb	%al, -72(%rbp)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L4041
	testq	%r15, %r15
	je	.L4166
.L4041:
	movq	%r13, -312(%rbp)
	cmpq	$15, %r13
	ja	.L4167
	cmpq	$1, %r13
	jne	.L4056
	movzbl	(%r15), %eax
	movb	%al, -224(%rbp)
	movq	%r14, %rax
.L4057:
	movq	%r13, -232(%rbp)
	leaq	-256(%rbp), %r15
	movb	$0, (%rax,%r13)
	movq	-304(%rbp), %rax
	movq	-296(%rbp), %r13
	movq	%r15, -272(%rbp)
	movq	%rax, %rdx
	movq	%rax, -328(%rbp)
	addq	%r13, %rdx
	je	.L4058
	testq	%rax, %rax
	je	.L4168
.L4058:
	movq	%r13, -312(%rbp)
	cmpq	$15, %r13
	ja	.L4169
	cmpq	$1, %r13
	jne	.L4061
	movq	-328(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -256(%rbp)
	movq	%r15, %rax
.L4062:
	movq	%r13, -264(%rbp)
	movb	$0, (%rax,%r13)
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
	movl	$6, 8(%r12)
.LEHB283:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE283:
	movq	(%rax), %rax
	pcmpeqd	%xmm0, %xmm0
	movb	$1, 64(%r12)
	movl	$-1, 60(%r12)
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm0, 44(%r12)
	movq	$1, 80(%r12)
	movl	%eax, 40(%r12)
	leaq	120(%r12), %rax
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 128(%r12)
	movq	-272(%rbp), %rax
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movl	$0x3f800000, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movups	%xmm1, 24(%r12)
	cmpq	%r15, %rax
	je	.L4170
	movq	%rax, 128(%r12)
	movq	-256(%rbp), %rax
	movq	%rax, 144(%r12)
.L4064:
	movq	-264(%rbp), %rax
	movq	%r15, -272(%rbp)
	movq	$0, -264(%rbp)
	movq	%rax, 136(%r12)
	leaq	176(%r12), %rax
	movq	%rax, 160(%r12)
	movq	-240(%rbp), %rax
	movb	$0, -256(%rbp)
	cmpq	%r14, %rax
	je	.L4171
	movq	%rax, 160(%r12)
	movq	-224(%rbp), %rax
	movq	%rax, 176(%r12)
.L4066:
	movq	-232(%rbp), %rax
	movdqa	-208(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	216(%r12), %rdi
	leaq	-184(%rbp), %rsi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, 168(%r12)
	movq	-192(%rbp), %rax
	movups	%xmm2, 192(%r12)
	movq	%rax, 208(%r12)
	movq	%r14, -240(%rbp)
	movq	$0, -232(%rbp)
	movb	$0, -224(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	movq	-128(%rbp), %rax
	movdqu	-88(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movdqa	-144(%rbp), %xmm3
	movdqu	-104(%rbp), %xmm4
	movq	$0, -128(%rbp)
	movq	%rax, 272(%r12)
	movzbl	-120(%rbp), %eax
	movq	-272(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movb	%al, 280(%r12)
	movq	-112(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, 288(%r12)
	movzbl	-72(%rbp), %eax
	movq	$0, 336(%r12)
	movb	%al, 328(%r12)
	movb	$0, 344(%r12)
	movb	$0, 352(%r12)
	movups	%xmm3, 256(%r12)
	movups	%xmm4, 296(%r12)
	movups	%xmm5, 312(%r12)
	movaps	%xmm0, -96(%rbp)
	cmpq	%r15, %rdi
	je	.L4067
	call	_ZdlPv@PLT
.L4067:
	movq	-240(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4068
	call	_ZdlPv@PLT
.L4068:
	movq	-88(%rbp), %r14
	movq	-96(%rbp), %r13
	cmpq	%r13, %r14
	je	.L4069
	.p2align 4,,10
	.p2align 3
.L4073:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4070
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %r14
	jne	.L4073
.L4071:
	movq	-96(%rbp), %r13
.L4069:
	testq	%r13, %r13
	je	.L4074
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4074:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4075
	call	_ZdlPv@PLT
.L4075:
	cmpb	$0, -184(%rbp)
	je	.L4076
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4076
	call	_ZdlPv@PLT
.L4076:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4077
	call	_ZdlPv@PLT
.L4077:
	leaq	16+_ZTVN2v88internal6torque9IntrinsicE(%rip), %rax
	cmpb	$0, 88(%rbx)
	movq	%rax, (%r12)
	jne	.L4172
	leaq	-312(%rbp), %rdi
	movq	%r12, -312(%rbp)
.LEHB284:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_9IntrinsicEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
.LEHE284:
	movq	-312(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L4086
	movq	(%rdi), %rax
	call	*8(%rax)
.L4086:
	movq	-304(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L4010
	call	_ZdlPv@PLT
.L4010:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4173
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4070:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %r14
	jne	.L4073
	jmp	.L4071
	.p2align 4,,10
	.p2align 3
.L4056:
	testq	%r13, %r13
	jne	.L4174
	movq	%r14, %rax
	jmp	.L4057
	.p2align 4,,10
	.p2align 3
.L4061:
	testq	%r13, %r13
	jne	.L4175
	movq	%r15, %rax
	jmp	.L4062
	.p2align 4,,10
	.p2align 3
.L4156:
	leaq	-304(%rbp), %rdi
	leaq	-312(%rbp), %rsi
	xorl	%edx, %edx
.LEHB285:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE285:
	movq	%rax, -304(%rbp)
	movq	%rax, %rdi
	movq	-312(%rbp), %rax
	movq	%rax, -288(%rbp)
.L4013:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-312(%rbp), %r12
	movq	-304(%rbp), %rax
	jmp	.L4015
	.p2align 4,,10
	.p2align 3
.L4159:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	leaq	-160(%rbp), %r13
	leaq	-176(%rbp), %rdi
	movq	%r13, -176(%rbp)
	addq	%rsi, %rdx
.LEHB286:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE286:
	movb	$1, -184(%rbp)
	jmp	.L4020
	.p2align 4,,10
	.p2align 3
.L4167:
	leaq	-312(%rbp), %rsi
	leaq	-240(%rbp), %rdi
	xorl	%edx, %edx
.LEHB287:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE287:
	movq	%rax, -240(%rbp)
	movq	%rax, %rdi
	movq	-312(%rbp), %rax
	movq	%rax, -224(%rbp)
.L4055:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-312(%rbp), %r13
	movq	-240(%rbp), %rax
	jmp	.L4057
	.p2align 4,,10
	.p2align 3
.L4169:
	leaq	-312(%rbp), %rsi
	leaq	-272(%rbp), %rdi
	xorl	%edx, %edx
.LEHB288:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE288:
	movq	%rax, -272(%rbp)
	movq	%rax, %rdi
	movq	-312(%rbp), %rax
	movq	%rax, -256(%rbp)
.L4060:
	movq	-328(%rbp), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-312(%rbp), %r13
	movq	-272(%rbp), %rax
	jmp	.L4062
	.p2align 4,,10
	.p2align 3
.L4171:
	movdqa	-224(%rbp), %xmm7
	movups	%xmm7, 176(%r12)
	jmp	.L4066
	.p2align 4,,10
	.p2align 3
.L4170:
	movdqa	-256(%rbp), %xmm6
	movups	%xmm6, 144(%r12)
	jmp	.L4064
	.p2align 4,,10
	.p2align 3
.L4157:
	movq	%r13, %r14
	xorl	%ecx, %ecx
	jmp	.L4017
	.p2align 4,,10
	.p2align 3
.L4160:
	movq	%r13, %r14
	xorl	%ecx, %ecx
	jmp	.L4022
	.p2align 4,,10
	.p2align 3
.L4162:
	movq	$0, -344(%rbp)
	jmp	.L4029
.L4155:
	leaq	.LC16(%rip), %rdi
.LEHB289:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE289:
	.p2align 4,,10
	.p2align 3
.L4164:
.LEHB290:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE290:
.L4173:
	call	__stack_chk_fail@PLT
.L4172:
	leaq	-208(%rbp), %r13
	leaq	.LC56(%rip), %rsi
	movq	%r13, %rdi
.LEHB291:
	call	_ZN2v88internal6torqueL7MessageIJRA38_KcEEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE291:
	movq	%r13, %rdi
.LEHB292:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE292:
.L4163:
.LEHB293:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE293:
.L4158:
.LEHB294:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE294:
.L4161:
.LEHB295:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE295:
.L4166:
	leaq	.LC16(%rip), %rdi
.LEHB296:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE296:
.L4168:
	leaq	.LC16(%rip), %rdi
.LEHB297:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE297:
.L4165:
	movq	-336(%rbp), %rdi
	jmp	.L4013
.L4174:
	movq	%r14, %rdi
	jmp	.L4055
.L4175:
	movq	%r15, %rdi
	jmp	.L4060
.L4099:
	endbr64
	movq	%rax, %rbx
	jmp	.L4089
.L4104:
	endbr64
	movq	%rax, %rbx
	jmp	.L4050
.L4105:
	endbr64
	movq	%rax, %rbx
	jmp	.L4048
.L4106:
	endbr64
	movq	%rax, %rbx
	jmp	.L4023
.L4109:
	endbr64
	movq	%rax, %rbx
	jmp	.L4079
.L4100:
	endbr64
	movq	%rax, %rbx
	jmp	.L4080
.L4103:
	endbr64
	movq	%rax, %rbx
	jmp	.L4081
.L4098:
	endbr64
	movq	%rax, %rbx
	jmp	.L4053
.L4101:
	endbr64
	movq	%rax, %rbx
	jmp	.L4085
.L4102:
	endbr64
	movq	%rax, %rbx
	jmp	.L4083
.L4097:
	endbr64
	movq	%rax, %rbx
	jmp	.L4088
.L4108:
	endbr64
	movq	%rax, %rdi
	jmp	.L4042
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE,"a",@progbits
	.align 4
.LLSDA6588:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6588-.LLSDATTD6588
.LLSDATTD6588:
	.byte	0x1
	.uleb128 .LLSDACSE6588-.LLSDACSB6588
.LLSDACSB6588:
	.uleb128 .LEHB278-.LFB6588
	.uleb128 .LEHE278-.LEHB278
	.uleb128 .L4097-.LFB6588
	.uleb128 0
	.uleb128 .LEHB279-.LFB6588
	.uleb128 .LEHE279-.LEHB279
	.uleb128 .L4098-.LFB6588
	.uleb128 0
	.uleb128 .LEHB280-.LFB6588
	.uleb128 .LEHE280-.LEHB280
	.uleb128 .L4104-.LFB6588
	.uleb128 0
	.uleb128 .LEHB281-.LFB6588
	.uleb128 .LEHE281-.LEHB281
	.uleb128 .L4105-.LFB6588
	.uleb128 0
	.uleb128 .LEHB282-.LFB6588
	.uleb128 .LEHE282-.LEHB282
	.uleb128 .L4108-.LFB6588
	.uleb128 0x1
	.uleb128 .LEHB283-.LFB6588
	.uleb128 .LEHE283-.LEHB283
	.uleb128 .L4103-.LFB6588
	.uleb128 0
	.uleb128 .LEHB284-.LFB6588
	.uleb128 .LEHE284-.LEHB284
	.uleb128 .L4099-.LFB6588
	.uleb128 0
	.uleb128 .LEHB285-.LFB6588
	.uleb128 .LEHE285-.LEHB285
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB286-.LFB6588
	.uleb128 .LEHE286-.LEHB286
	.uleb128 .L4106-.LFB6588
	.uleb128 0
	.uleb128 .LEHB287-.LFB6588
	.uleb128 .LEHE287-.LEHB287
	.uleb128 .L4101-.LFB6588
	.uleb128 0
	.uleb128 .LEHB288-.LFB6588
	.uleb128 .LEHE288-.LEHB288
	.uleb128 .L4102-.LFB6588
	.uleb128 0
	.uleb128 .LEHB289-.LFB6588
	.uleb128 .LEHE289-.LEHB289
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB290-.LFB6588
	.uleb128 .LEHE290-.LEHB290
	.uleb128 .L4108-.LFB6588
	.uleb128 0x1
	.uleb128 .LEHB291-.LFB6588
	.uleb128 .LEHE291-.LEHB291
	.uleb128 .L4100-.LFB6588
	.uleb128 0
	.uleb128 .LEHB292-.LFB6588
	.uleb128 .LEHE292-.LEHB292
	.uleb128 .L4109-.LFB6588
	.uleb128 0
	.uleb128 .LEHB293-.LFB6588
	.uleb128 .LEHE293-.LEHB293
	.uleb128 .L4105-.LFB6588
	.uleb128 0
	.uleb128 .LEHB294-.LFB6588
	.uleb128 .LEHE294-.LEHB294
	.uleb128 .L4098-.LFB6588
	.uleb128 0
	.uleb128 .LEHB295-.LFB6588
	.uleb128 .LEHE295-.LEHB295
	.uleb128 .L4104-.LFB6588
	.uleb128 0
	.uleb128 .LEHB296-.LFB6588
	.uleb128 .LEHE296-.LEHB296
	.uleb128 .L4101-.LFB6588
	.uleb128 0
	.uleb128 .LEHB297-.LFB6588
	.uleb128 .LEHE297-.LEHB297
	.uleb128 .L4102-.LFB6588
	.uleb128 0
.LLSDACSE6588:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6588:
	.section	.text._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6588
	.type	_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE.cold, @function
_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE.cold:
.LFSB6588:
.L4089:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4088
	movq	(%rdi), %rax
	call	*8(%rax)
.L4088:
	movq	-304(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L4091
	call	_ZdlPv@PLT
.L4091:
	movq	%rbx, %rdi
.LEHB298:
	call	_Unwind_Resume@PLT
.LEHE298:
.L4107:
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4048
	call	_ZdlPv@PLT
.L4048:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4050
	call	_ZdlPv@PLT
.L4050:
	cmpb	$0, -184(%rbp)
	je	.L4025
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4025
.L4154:
	call	_ZdlPv@PLT
.L4025:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4053
	call	_ZdlPv@PLT
.L4053:
	movl	$360, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L4088
.L4023:
	cmpb	$0, -184(%rbp)
	je	.L4025
	movq	-176(%rbp), %rdi
	cmpq	%r13, %rdi
	jne	.L4154
	jmp	.L4025
.L4079:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
.L4080:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque8CallableD2Ev
	jmp	.L4053
.L4081:
	movq	-272(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4083
	call	_ZdlPv@PLT
.L4083:
	movq	-240(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4085
	call	_ZdlPv@PLT
.L4085:
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	jmp	.L4053
.L4042:
	call	__cxa_begin_catch@PLT
.L4045:
	cmpq	%r14, -344(%rbp)
	jne	.L4176
.LEHB299:
	call	__cxa_rethrow@PLT
.LEHE299:
.L4176:
	movq	-344(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4044
	call	_ZdlPv@PLT
.L4044:
	addq	$32, -344(%rbp)
	jmp	.L4045
	.cfi_endproc
.LFE6588:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.align 4
.LLSDAC6588:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6588-.LLSDATTDC6588
.LLSDATTDC6588:
	.byte	0x1
	.uleb128 .LLSDACSEC6588-.LLSDACSBC6588
.LLSDACSBC6588:
	.uleb128 .LEHB298-.LCOLDB57
	.uleb128 .LEHE298-.LEHB298
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB299-.LCOLDB57
	.uleb128 .LEHE299-.LEHB299
	.uleb128 .L4107-.LCOLDB57
	.uleb128 0
.LLSDACSEC6588:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6588:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.section	.text._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.size	_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE, .-_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.size	_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE.cold, .-_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE.cold
.LCOLDE57:
	.section	.text._ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
.LHOTE57:
	.section	.text._ZN2v88internal6torque12Declarations16DeclareIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations16DeclareIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.type	_ZN2v88internal6torque12Declarations16DeclareIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE, @function
_ZN2v88internal6torque12Declarations16DeclareIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE:
.LFB6593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal6torque12Declarations15CreateIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	movq	%rax, %r12
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	addq	$72, %rdi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	%r12, -32(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L4178
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L4177:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4182
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4178:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L4177
.L4182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6593:
	.size	_ZN2v88internal6torque12Declarations16DeclareIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE, .-_ZN2v88internal6torque12Declarations16DeclareIntrinsicERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.type	_ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, @function
_ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE:
.LFB8136:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8136
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB300:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE300:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%r12, -32(%rbp)
	movq	120(%rax), %rsi
	cmpq	128(%rax), %rsi
	je	.L4184
	movq	$0, -32(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rax)
.L4185:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4183
	movq	(%rdi), %rax
	call	*8(%rax)
.L4183:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4198
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4184:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	112(%rax), %rdi
.LEHB301:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE301:
	jmp	.L4185
.L4198:
	call	__stack_chk_fail@PLT
.L4190:
	endbr64
	movq	%rax, %r12
.L4187:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4188
	movq	(%rdi), %rax
	call	*8(%rax)
.L4188:
	movq	%r12, %rdi
.LEHB302:
	call	_Unwind_Resume@PLT
.LEHE302:
	.cfi_endproc
.LFE8136:
	.section	.gcc_except_table._ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"aG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
.LLSDA8136:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8136-.LLSDACSB8136
.LLSDACSB8136:
	.uleb128 .LEHB300-.LFB8136
	.uleb128 .LEHE300-.LEHB300
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB301-.LFB8136
	.uleb128 .LEHE301-.LEHB301
	.uleb128 .L4190-.LFB8136
	.uleb128 0
	.uleb128 .LEHB302-.LFB8136
	.uleb128 .LEHE302-.LEHB302
	.uleb128 0
	.uleb128 0
.LLSDACSE8136:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.size	_ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, .-_ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.section	.rodata._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE.str1.8,"aMS",@progbits,1
	.align 8
.LC58:
	.string	"Varargs are not supported for macros."
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE,"ax",@progbits
	.align 2
.LCOLDB59:
	.section	.text._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE,"ax",@progbits
.LHOTB59:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE
	.type	_ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE, @function
_ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE:
.LFB6580:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6580
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-496(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$520, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rsi), %rax
	movq	%r14, -512(%rbp)
	cmpq	%rdx, %rax
	je	.L4309
	movq	%rdx, -512(%rbp)
	movq	16(%rsi), %rdx
	movq	%rdx, -496(%rbp)
.L4201:
	movq	8(%rsi), %rdx
	movdqu	(%rbx), %xmm1
	movq	%rax, (%rsi)
	pxor	%xmm0, %xmm0
	movq	16(%rbx), %rax
	movb	$0, 16(%rsi)
	leaq	-328(%rbp), %rdi
	movq	$0, 8(%rsi)
	leaq	24(%rbx), %rsi
	movq	$0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	%rax, -336(%rbp)
	movq	%rdx, -504(%rbp)
	movaps	%xmm1, -352(%rbp)
	call	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	movq	80(%rbx), %rax
	movdqu	120(%rbx), %xmm4
	movq	$0, 80(%rbx)
	movdqu	64(%rbx), %xmm2
	movdqu	104(%rbx), %xmm3
	pxor	%xmm0, %xmm0
	movq	$0, 128(%rbx)
	movq	%rax, -272(%rbp)
	movzbl	88(%rbx), %eax
	movl	$400, %edi
	movups	%xmm0, 64(%rbx)
	movb	%al, -264(%rbp)
	movq	96(%rbx), %rax
	movups	%xmm0, 112(%rbx)
	movq	%rax, -256(%rbp)
	movzbl	136(%rbx), %eax
	movaps	%xmm2, -288(%rbp)
	movb	%al, -216(%rbp)
	movups	%xmm3, -248(%rbp)
	movups	%xmm4, -232(%rbp)
.LEHB303:
	call	_Znwm@PLT
.LEHE303:
	movq	%rax, %r12
	movq	(%r15), %rax
	movq	8(%r15), %r13
	leaq	-432(%rbp), %rbx
	movq	%rbx, -448(%rbp)
	movq	%rax, %rcx
	movq	%rax, -536(%rbp)
	addq	%r13, %rcx
	je	.L4202
	testq	%rax, %rax
	je	.L4310
.L4202:
	movq	%r13, -520(%rbp)
	cmpq	$15, %r13
	ja	.L4311
	cmpq	$1, %r13
	jne	.L4205
	movq	-536(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -432(%rbp)
	movq	%rbx, %rax
.L4206:
	movq	%r13, -440(%rbp)
	movb	$0, (%rax,%r13)
	movq	(%r15), %rax
	leaq	-464(%rbp), %r13
	movq	8(%r15), %r15
	movq	%r13, -480(%rbp)
	movq	%rax, %rcx
	movq	%rax, -536(%rbp)
	addq	%r15, %rcx
	je	.L4207
	testq	%rax, %rax
	je	.L4312
.L4207:
	movq	%r15, -520(%rbp)
	cmpq	$15, %r15
	ja	.L4313
	cmpq	$1, %r15
	jne	.L4210
	movq	-536(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -464(%rbp)
	movq	%r13, %rax
.L4211:
	movq	%r15, -472(%rbp)
	leaq	-352(%rbp), %rsi
	movb	$0, (%rax,%r15)
	leaq	-208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rsi, -544(%rbp)
	movq	%rax, -552(%rbp)
.LEHB304:
	call	_ZN2v88internal6torque9SignatureC1ERKS2_
.LEHE304:
	leaq	-368(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, -384(%rbp)
	movq	-448(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L4314
	movq	%rax, -384(%rbp)
	movq	-432(%rbp), %rax
	movq	%rax, -368(%rbp)
.L4213:
	movq	-440(%rbp), %rax
	leaq	-400(%rbp), %r15
	movq	%rbx, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	%rax, -376(%rbp)
	movq	-480(%rbp), %rax
	movb	$0, -432(%rbp)
	movq	%r15, -416(%rbp)
	cmpq	%r13, %rax
	je	.L4315
	movq	%rax, -416(%rbp)
	movq	-464(%rbp), %rax
	movq	%rax, -400(%rbp)
.L4215:
	movq	-472(%rbp), %rax
	movq	%r13, -480(%rbp)
	movl	$2, 8(%r12)
	movq	%rax, -408(%rbp)
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
	movq	$0, -472(%rbp)
	movb	$0, -464(%rbp)
.LEHB305:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE305:
	movq	(%rax), %rax
	pcmpeqd	%xmm0, %xmm0
	movb	$1, 64(%r12)
	movl	$-1, 60(%r12)
	movdqu	(%rax), %xmm5
	movl	16(%rax), %eax
	movups	%xmm0, 44(%r12)
	movq	$1, 80(%r12)
	movl	%eax, 40(%r12)
	leaq	120(%r12), %rax
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 128(%r12)
	movq	-416(%rbp), %rax
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movl	$0x3f800000, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movups	%xmm5, 24(%r12)
	cmpq	%r15, %rax
	je	.L4316
	movq	%rax, 128(%r12)
	movq	-400(%rbp), %rax
	movq	%rax, 144(%r12)
.L4217:
	movq	-408(%rbp), %rax
	movq	%r15, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	%rax, 136(%r12)
	leaq	176(%r12), %rax
	movq	%rax, 160(%r12)
	movq	-384(%rbp), %rax
	movb	$0, -400(%rbp)
	cmpq	-536(%rbp), %rax
	je	.L4317
	movq	%rax, 160(%r12)
	movq	-368(%rbp), %rax
	movq	%rax, 176(%r12)
.L4219:
	movq	-376(%rbp), %rax
	movdqa	-208(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	leaq	216(%r12), %rdi
	leaq	-184(%rbp), %rsi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, 168(%r12)
	movq	-536(%rbp), %rax
	movups	%xmm6, 192(%r12)
	movq	%rax, -384(%rbp)
	movq	-192(%rbp), %rax
	movq	$0, -376(%rbp)
	movq	%rax, 208(%r12)
	movb	$0, -368(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	movq	-128(%rbp), %rax
	movdqu	-88(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movdqa	-144(%rbp), %xmm7
	movdqu	-104(%rbp), %xmm5
	movq	$0, -128(%rbp)
	movq	%rax, 272(%r12)
	movzbl	-120(%rbp), %eax
	movq	-416(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movb	%al, 280(%r12)
	movq	-112(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, 288(%r12)
	movzbl	-72(%rbp), %eax
	movq	$0, 336(%r12)
	movb	%al, 328(%r12)
	movb	$0, 344(%r12)
	movb	$0, 352(%r12)
	movups	%xmm7, 256(%r12)
	movups	%xmm5, 296(%r12)
	movups	%xmm6, 312(%r12)
	movaps	%xmm0, -96(%rbp)
	cmpq	%r15, %rdi
	je	.L4220
	call	_ZdlPv@PLT
.L4220:
	movq	-384(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L4221
	call	_ZdlPv@PLT
.L4221:
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %r15
	cmpq	%r15, %rax
	je	.L4222
	.p2align 4,,10
	.p2align 3
.L4226:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4223
	movq	%rax, -536(%rbp)
	addq	$32, %r15
	call	_ZdlPv@PLT
	movq	-536(%rbp), %rax
	cmpq	%r15, %rax
	jne	.L4226
.L4224:
	movq	-96(%rbp), %r15
.L4222:
	testq	%r15, %r15
	je	.L4227
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L4227:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4228
	call	_ZdlPv@PLT
.L4228:
	cmpb	$0, -184(%rbp)
	je	.L4229
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4229
	call	_ZdlPv@PLT
.L4229:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4230
	call	_ZdlPv@PLT
.L4230:
	leaq	16+_ZTVN2v88internal6torque5MacroE(%rip), %rax
	cmpb	$0, -264(%rbp)
	movb	$0, 360(%r12)
	movq	%rax, (%r12)
	jne	.L4318
	movq	-480(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4237
	call	_ZdlPv@PLT
.L4237:
	movq	-448(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4238
	call	_ZdlPv@PLT
.L4238:
	leaq	16+_ZTVN2v88internal6torque11ExternMacroE(%rip), %rax
	movq	%rax, (%r12)
	leaq	384(%r12), %rax
	movq	%rax, 368(%r12)
	movq	-512(%rbp), %rax
	cmpq	%r14, %rax
	je	.L4319
	movq	%rax, 368(%r12)
	movq	-496(%rbp), %rax
	movq	%rax, 384(%r12)
.L4240:
	movq	-504(%rbp), %rax
	leaq	-520(%rbp), %rdi
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movq	%rax, 376(%r12)
	movb	$0, -496(%rbp)
	movq	%r12, -520(%rbp)
.LEHB306:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_11ExternMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
.LEHE306:
	movq	-520(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L4246
	movq	(%rdi), %rax
	call	*8(%rax)
.L4246:
	movq	-232(%rbp), %rbx
	movq	-240(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L4247
	.p2align 4,,10
	.p2align 3
.L4251:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4248
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L4251
.L4249:
	movq	-240(%rbp), %r12
.L4247:
	testq	%r12, %r12
	je	.L4252
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4252:
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4253
	call	_ZdlPv@PLT
.L4253:
	cmpb	$0, -328(%rbp)
	je	.L4254
	movq	-320(%rbp), %rdi
	leaq	-304(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4254
	call	_ZdlPv@PLT
.L4254:
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4255
	call	_ZdlPv@PLT
.L4255:
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4199
	call	_ZdlPv@PLT
.L4199:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4320
	addq	$520, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4205:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L4321
	movq	%rbx, %rax
	jmp	.L4206
	.p2align 4,,10
	.p2align 3
.L4223:
	addq	$32, %r15
	cmpq	%r15, %rax
	jne	.L4226
	jmp	.L4224
	.p2align 4,,10
	.p2align 3
.L4248:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L4251
	jmp	.L4249
	.p2align 4,,10
	.p2align 3
.L4210:
	testq	%r15, %r15
	jne	.L4322
	movq	%r13, %rax
	jmp	.L4211
	.p2align 4,,10
	.p2align 3
.L4311:
	leaq	-520(%rbp), %rsi
	leaq	-448(%rbp), %rdi
	xorl	%edx, %edx
.LEHB307:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE307:
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-520(%rbp), %rax
	movq	%rax, -432(%rbp)
.L4204:
	movq	-536(%rbp), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-520(%rbp), %r13
	movq	-448(%rbp), %rax
	jmp	.L4206
	.p2align 4,,10
	.p2align 3
.L4313:
	leaq	-520(%rbp), %rsi
	leaq	-480(%rbp), %rdi
	xorl	%edx, %edx
.LEHB308:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE308:
	movq	%rax, -480(%rbp)
	movq	%rax, %rdi
	movq	-520(%rbp), %rax
	movq	%rax, -464(%rbp)
.L4209:
	movq	-536(%rbp), %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	-520(%rbp), %r15
	movq	-480(%rbp), %rax
	jmp	.L4211
	.p2align 4,,10
	.p2align 3
.L4309:
	movdqu	16(%rsi), %xmm7
	movaps	%xmm7, -496(%rbp)
	jmp	.L4201
	.p2align 4,,10
	.p2align 3
.L4316:
	movdqa	-400(%rbp), %xmm3
	movups	%xmm3, 144(%r12)
	jmp	.L4217
	.p2align 4,,10
	.p2align 3
.L4315:
	movdqa	-464(%rbp), %xmm2
	movaps	%xmm2, -400(%rbp)
	jmp	.L4215
	.p2align 4,,10
	.p2align 3
.L4314:
	movdqa	-432(%rbp), %xmm1
	movaps	%xmm1, -368(%rbp)
	jmp	.L4213
	.p2align 4,,10
	.p2align 3
.L4317:
	movdqa	-368(%rbp), %xmm4
	movups	%xmm4, 176(%r12)
	jmp	.L4219
	.p2align 4,,10
	.p2align 3
.L4319:
	movdqa	-496(%rbp), %xmm1
	movups	%xmm1, 384(%r12)
	jmp	.L4240
.L4310:
	leaq	.LC16(%rip), %rdi
.LEHB309:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE309:
.L4318:
	leaq	.LC58(%rip), %rdi
.LEHB310:
	call	_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_
.LEHE310:
.L4320:
	call	__stack_chk_fail@PLT
.L4312:
	leaq	.LC16(%rip), %rdi
.LEHB311:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE311:
.L4321:
	movq	%rbx, %rdi
	jmp	.L4204
.L4322:
	movq	%r13, %rdi
	jmp	.L4209
.L4270:
	endbr64
	movq	%rax, -560(%rbp)
	jmp	.L4232
.L4266:
	endbr64
	movq	%rax, %r12
	jmp	.L4258
.L4268:
	endbr64
	movq	%rax, %r15
	jmp	.L4235
.L4264:
	endbr64
	movq	%rax, %r12
	leaq	-352(%rbp), %rax
	movq	%rax, -544(%rbp)
	jmp	.L4257
.L4265:
	endbr64
	movq	%rax, %r15
	leaq	-352(%rbp), %rax
	movq	%rax, -544(%rbp)
	jmp	.L4245
.L4267:
	endbr64
	movq	%rax, %r15
	leaq	-352(%rbp), %rax
	movq	%rax, -544(%rbp)
	jmp	.L4243
.L4269:
	endbr64
	movq	%rax, %r15
	jmp	.L4236
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE,"a",@progbits
.LLSDA6580:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6580-.LLSDACSB6580
.LLSDACSB6580:
	.uleb128 .LEHB303-.LFB6580
	.uleb128 .LEHE303-.LEHB303
	.uleb128 .L4264-.LFB6580
	.uleb128 0
	.uleb128 .LEHB304-.LFB6580
	.uleb128 .LEHE304-.LEHB304
	.uleb128 .L4268-.LFB6580
	.uleb128 0
	.uleb128 .LEHB305-.LFB6580
	.uleb128 .LEHE305-.LEHB305
	.uleb128 .L4270-.LFB6580
	.uleb128 0
	.uleb128 .LEHB306-.LFB6580
	.uleb128 .LEHE306-.LEHB306
	.uleb128 .L4266-.LFB6580
	.uleb128 0
	.uleb128 .LEHB307-.LFB6580
	.uleb128 .LEHE307-.LEHB307
	.uleb128 .L4265-.LFB6580
	.uleb128 0
	.uleb128 .LEHB308-.LFB6580
	.uleb128 .LEHE308-.LEHB308
	.uleb128 .L4267-.LFB6580
	.uleb128 0
	.uleb128 .LEHB309-.LFB6580
	.uleb128 .LEHE309-.LEHB309
	.uleb128 .L4265-.LFB6580
	.uleb128 0
	.uleb128 .LEHB310-.LFB6580
	.uleb128 .LEHE310-.LEHB310
	.uleb128 .L4269-.LFB6580
	.uleb128 0
	.uleb128 .LEHB311-.LFB6580
	.uleb128 .LEHE311-.LEHB311
	.uleb128 .L4267-.LFB6580
	.uleb128 0
.LLSDACSE6580:
	.section	.text._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6580
	.type	_ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE.cold, @function
_ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE.cold:
.LFSB6580:
.L4232:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-416(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4233
	call	_ZdlPv@PLT
.L4233:
	movq	-384(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L4234
	call	_ZdlPv@PLT
.L4234:
	movq	-552(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	-560(%rbp), %r15
.L4235:
	movq	-480(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4243
	call	_ZdlPv@PLT
.L4243:
	movq	-448(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4245
	call	_ZdlPv@PLT
.L4245:
	movq	%r12, %rdi
	movl	$400, %esi
	movq	%r15, %r12
	call	_ZdlPvm@PLT
.L4257:
	movq	-544(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4260
	call	_ZdlPv@PLT
.L4260:
	movq	%r12, %rdi
.LEHB312:
	call	_Unwind_Resume@PLT
.LEHE312:
.L4258:
	movq	-520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4257
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L4257
.L4236:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque8CallableD2Ev
	jmp	.L4235
	.cfi_endproc
.LFE6580:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE
.LLSDAC6580:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6580-.LLSDACSBC6580
.LLSDACSBC6580:
	.uleb128 .LEHB312-.LCOLDB59
	.uleb128 .LEHE312-.LEHB312
	.uleb128 0
	.uleb128 0
.LLSDACSEC6580:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE
	.section	.text._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE
	.size	_ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE, .-_ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE
	.size	_ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE.cold, .-_ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE.cold
.LCOLDE59:
	.section	.text._ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE
.LHOTE59:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.type	_ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, @function
_ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE:
.LFB8159:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8159
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB313:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE313:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%r12, -32(%rbp)
	movq	120(%rax), %rsi
	cmpq	128(%rax), %rsi
	je	.L4324
	movq	$0, -32(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rax)
.L4325:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4323
	movq	(%rdi), %rax
	call	*8(%rax)
.L4323:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4338
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4324:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	112(%rax), %rdi
.LEHB314:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE314:
	jmp	.L4325
.L4338:
	call	__stack_chk_fail@PLT
.L4330:
	endbr64
	movq	%rax, %r12
.L4327:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4328
	movq	(%rdi), %rax
	call	*8(%rax)
.L4328:
	movq	%r12, %rdi
.LEHB315:
	call	_Unwind_Resume@PLT
.LEHE315:
	.cfi_endproc
.LFE8159:
	.section	.gcc_except_table._ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"aG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
.LLSDA8159:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8159-.LLSDACSB8159
.LLSDACSB8159:
	.uleb128 .LEHB313-.LFB8159
	.uleb128 .LEHE313-.LEHB313
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB314-.LFB8159
	.uleb128 .LEHE314-.LEHB314
	.uleb128 .L4330-.LFB8159
	.uleb128 0
	.uleb128 .LEHB315-.LFB8159
	.uleb128 .LEHE315-.LEHB315
	.uleb128 0
	.uleb128 0
.LLSDACSE8159:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.size	_ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, .-_ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.section	.rodata._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE.str1.1,"aMS",@progbits,1
.LC60:
	.string	"_method_"
	.section	.text.unlikely._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE,"ax",@progbits
	.align 2
.LCOLDB61:
	.section	.text._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE,"ax",@progbits
.LHOTB61:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE
	.type	_ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE, @function
_ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE:
.LFB6585:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6585
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-240(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$552, %rsp
	movq	%rdx, -560(%rbp)
	movq	%rcx, -576(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	120(%rdi), %rax
	movq	%r14, %rdi
	movq	%r13, -240(%rbp)
	movq	$0, -232(%rbp)
	leaq	8(%rax), %rsi
	movb	$0, -224(%rbp)
.LEHB316:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-232(%rbp), %rax
	cmpq	$7, %rax
	jbe	.L4476
	movl	$8, %edx
	leaq	.LC60(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	120(%rbx), %rdx
	movq	112(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE316:
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -232(%rbp)
	je	.L4477
	movl	$1, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r14, %rdi
.LEHB317:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE317:
	leaq	-192(%rbp), %r15
	leaq	16(%rax), %rdx
	movq	%r15, -208(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L4478
	movq	%rcx, -208(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -192(%rbp)
.L4346:
	movq	8(%rax), %rcx
	movq	%rcx, -200(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	leaq	-208(%rbp), %rax
	movq	8(%r12), %rdx
	movq	(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, -568(%rbp)
.LEHB318:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE318:
	leaq	-480(%rbp), %rcx
	leaq	16(%rax), %rdx
	movq	%rcx, -584(%rbp)
	movq	%rcx, -496(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L4479
	movq	%rcx, -496(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -480(%rbp)
.L4348:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -488(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-208(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4349
	call	_ZdlPv@PLT
.L4349:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4350
	call	_ZdlPv@PLT
.L4350:
	movq	120(%rbx), %rax
	movq	%r14, %rdi
	movq	%r13, -240(%rbp)
	movq	$0, -232(%rbp)
	leaq	8(%rax), %rsi
	movb	$0, -224(%rbp)
.LEHB319:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-232(%rbp), %rax
	cmpq	$7, %rax
	jbe	.L4480
	movl	$8, %edx
	leaq	.LC60(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	120(%rbx), %rdx
	movq	112(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE319:
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -232(%rbp)
	je	.L4481
	movl	$1, %edx
	leaq	.LC52(%rip), %rsi
	movq	%r14, %rdi
.LEHB320:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE320:
	movq	%r15, -208(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L4482
	movq	%rcx, -208(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -192(%rbp)
.L4363:
	movq	8(%rax), %rcx
	movq	-568(%rbp), %rdi
	movq	%rcx, -200(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	8(%r12), %rdx
	movq	(%r12), %rsi
.LEHB321:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE321:
	leaq	-448(%rbp), %rsi
	leaq	16(%rax), %rdx
	movq	%rsi, -464(%rbp)
	movq	(%rax), %rcx
	movq	%rsi, -552(%rbp)
	cmpq	%rdx, %rcx
	je	.L4483
	movq	%rcx, -464(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -448(%rbp)
.L4365:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -456(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	-208(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4366
	call	_ZdlPv@PLT
.L4366:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4367
	call	_ZdlPv@PLT
.L4367:
	movq	(%r12), %r14
	movq	8(%r12), %r12
	leaq	-416(%rbp), %r15
	movq	%r15, -432(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L4370
	testq	%r14, %r14
	je	.L4484
.L4370:
	movq	%r12, -504(%rbp)
	cmpq	$15, %r12
	ja	.L4485
	cmpq	$1, %r12
	jne	.L4377
	movzbl	(%r14), %eax
	movb	%al, -416(%rbp)
	movq	%r15, %rax
.L4378:
	movq	%r12, -424(%rbp)
	movl	$376, %edi
	movb	$0, (%rax,%r12)
.LEHB322:
	call	_Znwm@PLT
.LEHE322:
	movq	-432(%rbp), %rdx
	movq	%rax, %r12
	leaq	-352(%rbp), %rax
	movq	%rax, -528(%rbp)
	cmpq	%r15, %rdx
	je	.L4486
	movq	-416(%rbp), %rax
	movq	%rax, -352(%rbp)
.L4380:
	leaq	-384(%rbp), %rax
	movq	-424(%rbp), %rsi
	movq	%r15, -432(%rbp)
	movq	%rax, -520(%rbp)
	movq	-464(%rbp), %rax
	movq	$0, -424(%rbp)
	movb	$0, -416(%rbp)
	cmpq	-552(%rbp), %rax
	je	.L4487
	movq	-448(%rbp), %rcx
	movq	%rcx, -384(%rbp)
.L4382:
	movq	-552(%rbp), %rdi
	movq	-456(%rbp), %rcx
	movb	$0, -448(%rbp)
	movq	$0, -456(%rbp)
	movq	%rdi, -464(%rbp)
	leaq	-288(%rbp), %rdi
	movq	%rdi, -544(%rbp)
	movq	%rdi, -304(%rbp)
	cmpq	-528(%rbp), %rdx
	je	.L4488
	movq	%rdx, -304(%rbp)
	movq	-352(%rbp), %rdx
	movq	%rdx, -288(%rbp)
.L4384:
	movq	%rsi, -296(%rbp)
	movq	-528(%rbp), %rsi
	leaq	-320(%rbp), %rdx
	movq	$0, -360(%rbp)
	movq	%rsi, -368(%rbp)
	movb	$0, -352(%rbp)
	movq	%rdx, -536(%rbp)
	movq	%rdx, -336(%rbp)
	cmpq	-520(%rbp), %rax
	je	.L4489
	movq	%rax, -336(%rbp)
	movq	-384(%rbp), %rax
	movq	%rax, -320(%rbp)
.L4386:
	movq	-520(%rbp), %rax
	movq	-560(%rbp), %rsi
	movq	%rcx, -328(%rbp)
	movq	-568(%rbp), %rdi
	movb	$0, -384(%rbp)
	movq	%rax, -400(%rbp)
	movq	$0, -392(%rbp)
.LEHB323:
	call	_ZN2v88internal6torque9SignatureC1ERKS2_
.LEHE323:
	movq	%r13, -240(%rbp)
	movq	-304(%rbp), %rax
	cmpq	-544(%rbp), %rax
	je	.L4490
	movq	%rax, -240(%rbp)
	movq	-288(%rbp), %rax
	movq	%rax, -224(%rbp)
.L4388:
	movq	-296(%rbp), %rax
	leaq	-256(%rbp), %r14
	movq	$0, -296(%rbp)
	movb	$0, -288(%rbp)
	movq	%rax, -232(%rbp)
	movq	-544(%rbp), %rax
	movq	%r14, -272(%rbp)
	movq	%rax, -304(%rbp)
	movq	-336(%rbp), %rax
	cmpq	-536(%rbp), %rax
	je	.L4491
	movq	%rax, -272(%rbp)
	movq	-320(%rbp), %rax
	movq	%rax, -256(%rbp)
.L4390:
	movq	-328(%rbp), %rax
	movb	$0, -320(%rbp)
	movl	$3, 8(%r12)
	movq	%rax, -264(%rbp)
	movq	-536(%rbp), %rax
	movq	$0, -328(%rbp)
	movq	%rax, -336(%rbp)
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
.LEHB324:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE324:
	movq	(%rax), %rax
	pcmpeqd	%xmm0, %xmm0
	movb	$1, 64(%r12)
	movl	$-1, 60(%r12)
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm0, 44(%r12)
	movq	$1, 80(%r12)
	movl	%eax, 40(%r12)
	leaq	120(%r12), %rax
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 128(%r12)
	movq	-272(%rbp), %rax
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movl	$0x3f800000, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movups	%xmm1, 24(%r12)
	cmpq	%r14, %rax
	je	.L4492
	movq	%rax, 128(%r12)
	movq	-256(%rbp), %rax
	movq	%rax, 144(%r12)
.L4392:
	movq	-264(%rbp), %rax
	movq	%r14, -272(%rbp)
	movq	$0, -264(%rbp)
	movq	%rax, 136(%r12)
	leaq	176(%r12), %rax
	movq	%rax, 160(%r12)
	movq	-240(%rbp), %rax
	movb	$0, -256(%rbp)
	cmpq	%r13, %rax
	je	.L4493
	movq	%rax, 160(%r12)
	movq	-224(%rbp), %rax
	movq	%rax, 176(%r12)
.L4394:
	movq	-232(%rbp), %rax
	movdqa	-208(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	216(%r12), %rdi
	leaq	-184(%rbp), %rsi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, 168(%r12)
	movq	-192(%rbp), %rax
	movups	%xmm2, 192(%r12)
	movq	%rax, 208(%r12)
	movq	%r13, -240(%rbp)
	movq	$0, -232(%rbp)
	movb	$0, -224(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	movq	-128(%rbp), %rax
	movdqu	-88(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movdqa	-144(%rbp), %xmm3
	movdqu	-104(%rbp), %xmm4
	movq	$0, -128(%rbp)
	movq	%rax, 272(%r12)
	movzbl	-120(%rbp), %eax
	movq	-272(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movb	%al, 280(%r12)
	movq	-112(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, 288(%r12)
	movzbl	-72(%rbp), %eax
	movq	$0, 336(%r12)
	movb	%al, 328(%r12)
	movq	-576(%rbp), %rax
	movb	$1, 344(%r12)
	movq	%rax, 352(%r12)
	movups	%xmm3, 256(%r12)
	movups	%xmm4, 296(%r12)
	movups	%xmm5, 312(%r12)
	movaps	%xmm0, -96(%rbp)
	cmpq	%r14, %rdi
	je	.L4395
	call	_ZdlPv@PLT
.L4395:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4396
	call	_ZdlPv@PLT
.L4396:
	movq	-88(%rbp), %r14
	movq	-96(%rbp), %r13
	cmpq	%r13, %r14
	je	.L4397
	.p2align 4,,10
	.p2align 3
.L4401:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4398
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %r14
	jne	.L4401
.L4399:
	movq	-96(%rbp), %r13
.L4397:
	testq	%r13, %r13
	je	.L4402
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4402:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4403
	call	_ZdlPv@PLT
.L4403:
	cmpb	$0, -184(%rbp)
	je	.L4404
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4404
	call	_ZdlPv@PLT
.L4404:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4405
	call	_ZdlPv@PLT
.L4405:
	movb	$0, 360(%r12)
	leaq	16+_ZTVN2v88internal6torque5MacroE(%rip), %rax
	movq	%rax, (%r12)
	movq	-560(%rbp), %rax
	cmpb	$0, 88(%rax)
	jne	.L4494
	movq	-336(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L4412
	call	_ZdlPv@PLT
.L4412:
	movq	-304(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L4413
	call	_ZdlPv@PLT
.L4413:
	leaq	16+_ZTVN2v88internal6torque11TorqueMacroE(%rip), %rax
	movb	$1, 64(%r12)
	movq	-400(%rbp), %rdi
	movq	%rax, (%r12)
	movb	$0, 361(%r12)
	cmpq	-520(%rbp), %rdi
	je	.L4415
	call	_ZdlPv@PLT
.L4415:
	movq	-368(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L4420
	call	_ZdlPv@PLT
.L4420:
	leaq	16+_ZTVN2v88internal6torque6MethodE(%rip), %rax
	movq	%rbx, 368(%r12)
	leaq	-504(%rbp), %r13
	movq	%rax, (%r12)
	movq	%r13, %rdi
	movq	%r12, -504(%rbp)
.LEHB325:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_6MethodEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
.LEHE325:
	movq	-504(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L4424
	movq	(%rdi), %rax
	call	*8(%rax)
.L4424:
	movq	-432(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4425
	call	_ZdlPv@PLT
.L4425:
	movq	-464(%rbp), %rdi
	cmpq	-552(%rbp), %rdi
	je	.L4426
	call	_ZdlPv@PLT
.L4426:
	movq	%r12, -504(%rbp)
	movq	152(%rbx), %rsi
	cmpq	160(%rbx), %rsi
	je	.L4427
	movq	%r12, (%rsi)
	addq	$8, 152(%rbx)
.L4428:
	movq	-496(%rbp), %rdi
	cmpq	-584(%rbp), %rdi
	je	.L4339
	call	_ZdlPv@PLT
.L4339:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4495
	addq	$552, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4377:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L4496
	movq	%r15, %rax
	jmp	.L4378
	.p2align 4,,10
	.p2align 3
.L4398:
	addq	$32, %r13
	cmpq	%r13, %r14
	jne	.L4401
	jmp	.L4399
	.p2align 4,,10
	.p2align 3
.L4485:
	leaq	-504(%rbp), %rsi
	leaq	-432(%rbp), %rdi
	xorl	%edx, %edx
.LEHB326:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE326:
	movq	%rax, -432(%rbp)
	movq	%rax, %rdi
	movq	-504(%rbp), %rax
	movq	%rax, -416(%rbp)
.L4376:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-504(%rbp), %r12
	movq	-432(%rbp), %rax
	jmp	.L4378
	.p2align 4,,10
	.p2align 3
.L4478:
	movdqu	16(%rax), %xmm6
	movaps	%xmm6, -192(%rbp)
	jmp	.L4346
	.p2align 4,,10
	.p2align 3
.L4479:
	movdqu	16(%rax), %xmm7
	movaps	%xmm7, -480(%rbp)
	jmp	.L4348
	.p2align 4,,10
	.p2align 3
.L4483:
	movdqu	16(%rax), %xmm7
	movaps	%xmm7, -448(%rbp)
	jmp	.L4365
	.p2align 4,,10
	.p2align 3
.L4482:
	movdqu	16(%rax), %xmm6
	movaps	%xmm6, -192(%rbp)
	jmp	.L4363
	.p2align 4,,10
	.p2align 3
.L4493:
	movdqa	-224(%rbp), %xmm4
	movups	%xmm4, 176(%r12)
	jmp	.L4394
	.p2align 4,,10
	.p2align 3
.L4492:
	movdqa	-256(%rbp), %xmm3
	movups	%xmm3, 144(%r12)
	jmp	.L4392
	.p2align 4,,10
	.p2align 3
.L4491:
	movdqa	-320(%rbp), %xmm2
	movaps	%xmm2, -256(%rbp)
	jmp	.L4390
	.p2align 4,,10
	.p2align 3
.L4490:
	movdqa	-288(%rbp), %xmm1
	movaps	%xmm1, -224(%rbp)
	jmp	.L4388
	.p2align 4,,10
	.p2align 3
.L4489:
	movdqa	-384(%rbp), %xmm7
	movaps	%xmm7, -320(%rbp)
	jmp	.L4386
	.p2align 4,,10
	.p2align 3
.L4488:
	movdqa	-352(%rbp), %xmm6
	movaps	%xmm6, -288(%rbp)
	jmp	.L4384
	.p2align 4,,10
	.p2align 3
.L4487:
	movdqa	-448(%rbp), %xmm7
	movq	-520(%rbp), %rax
	movaps	%xmm7, -384(%rbp)
	jmp	.L4382
	.p2align 4,,10
	.p2align 3
.L4486:
	movdqa	-416(%rbp), %xmm6
	movq	%rax, %rdx
	movaps	%xmm6, -352(%rbp)
	jmp	.L4380
	.p2align 4,,10
	.p2align 3
.L4427:
	leaq	144(%rbx), %rdi
	movq	%r13, %rdx
.LEHB327:
	call	_ZNSt6vectorIPN2v88internal6torque6MethodESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE327:
	jmp	.L4428
.L4484:
	leaq	.LC16(%rip), %rdi
.LEHB328:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE328:
.L4477:
	leaq	.LC51(%rip), %rdi
.LEHB329:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE329:
.L4476:
	leaq	.LC51(%rip), %rdi
.LEHB330:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE330:
.L4481:
	leaq	.LC51(%rip), %rdi
.LEHB331:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE331:
.L4480:
	leaq	.LC51(%rip), %rdi
.LEHB332:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE332:
.L4494:
	leaq	.LC58(%rip), %rdi
.LEHB333:
	call	_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_
.LEHE333:
.L4495:
	call	__stack_chk_fail@PLT
.L4496:
	movq	%r15, %rdi
	jmp	.L4376
.L4441:
	endbr64
	movq	%rax, %rbx
	jmp	.L4430
.L4438:
	endbr64
	movq	%rax, %r12
	jmp	.L4360
.L4445:
	endbr64
	movq	%rax, %r12
	jmp	.L4352
.L4449:
	endbr64
	movq	%rax, %rbx
	jmp	.L4411
.L4447:
	endbr64
	movq	%rax, %r12
	jmp	.L4373
.L4444:
	endbr64
	movq	%rax, %r12
	jmp	.L4354
.L4443:
	endbr64
	movq	%rax, %r12
	jmp	.L4371
.L4442:
	endbr64
	movq	%rax, %r12
	jmp	.L4373
.L4439:
	endbr64
	movq	%rax, %rbx
	jmp	.L4433
.L4446:
	endbr64
	movq	%rax, %r12
	jmp	.L4342
.L4448:
	endbr64
	movq	%rax, %rbx
	jmp	.L4410
.L4440:
	endbr64
	movq	%rax, %rbx
	jmp	.L4423
.L4450:
	endbr64
	movq	%rax, %rbx
	jmp	.L4407
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE,"a",@progbits
.LLSDA6585:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6585-.LLSDACSB6585
.LLSDACSB6585:
	.uleb128 .LEHB316-.LFB6585
	.uleb128 .LEHE316-.LEHB316
	.uleb128 .L4446-.LFB6585
	.uleb128 0
	.uleb128 .LEHB317-.LFB6585
	.uleb128 .LEHE317-.LEHB317
	.uleb128 .L4444-.LFB6585
	.uleb128 0
	.uleb128 .LEHB318-.LFB6585
	.uleb128 .LEHE318-.LEHB318
	.uleb128 .L4445-.LFB6585
	.uleb128 0
	.uleb128 .LEHB319-.LFB6585
	.uleb128 .LEHE319-.LEHB319
	.uleb128 .L4447-.LFB6585
	.uleb128 0
	.uleb128 .LEHB320-.LFB6585
	.uleb128 .LEHE320-.LEHB320
	.uleb128 .L4442-.LFB6585
	.uleb128 0
	.uleb128 .LEHB321-.LFB6585
	.uleb128 .LEHE321-.LEHB321
	.uleb128 .L4443-.LFB6585
	.uleb128 0
	.uleb128 .LEHB322-.LFB6585
	.uleb128 .LEHE322-.LEHB322
	.uleb128 .L4440-.LFB6585
	.uleb128 0
	.uleb128 .LEHB323-.LFB6585
	.uleb128 .LEHE323-.LEHB323
	.uleb128 .L4448-.LFB6585
	.uleb128 0
	.uleb128 .LEHB324-.LFB6585
	.uleb128 .LEHE324-.LEHB324
	.uleb128 .L4450-.LFB6585
	.uleb128 0
	.uleb128 .LEHB325-.LFB6585
	.uleb128 .LEHE325-.LEHB325
	.uleb128 .L4441-.LFB6585
	.uleb128 0
	.uleb128 .LEHB326-.LFB6585
	.uleb128 .LEHE326-.LEHB326
	.uleb128 .L4439-.LFB6585
	.uleb128 0
	.uleb128 .LEHB327-.LFB6585
	.uleb128 .LEHE327-.LEHB327
	.uleb128 .L4438-.LFB6585
	.uleb128 0
	.uleb128 .LEHB328-.LFB6585
	.uleb128 .LEHE328-.LEHB328
	.uleb128 .L4439-.LFB6585
	.uleb128 0
	.uleb128 .LEHB329-.LFB6585
	.uleb128 .LEHE329-.LEHB329
	.uleb128 .L4444-.LFB6585
	.uleb128 0
	.uleb128 .LEHB330-.LFB6585
	.uleb128 .LEHE330-.LEHB330
	.uleb128 .L4446-.LFB6585
	.uleb128 0
	.uleb128 .LEHB331-.LFB6585
	.uleb128 .LEHE331-.LEHB331
	.uleb128 .L4442-.LFB6585
	.uleb128 0
	.uleb128 .LEHB332-.LFB6585
	.uleb128 .LEHE332-.LEHB332
	.uleb128 .L4447-.LFB6585
	.uleb128 0
	.uleb128 .LEHB333-.LFB6585
	.uleb128 .LEHE333-.LEHB333
	.uleb128 .L4449-.LFB6585
	.uleb128 0
.LLSDACSE6585:
	.section	.text._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6585
	.type	_ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE.cold, @function
_ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE.cold:
.LFSB6585:
.L4430:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-504(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4423
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L4423
.L4371:
	movq	-208(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4373
	call	_ZdlPv@PLT
.L4373:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4360
	call	_ZdlPv@PLT
.L4360:
	movq	-496(%rbp), %rdi
	cmpq	-584(%rbp), %rdi
	je	.L4435
	call	_ZdlPv@PLT
.L4435:
	movq	%r12, %rdi
.LEHB334:
	call	_Unwind_Resume@PLT
.L4411:
	movq	%r12, %rdi
	call	_ZN2v88internal6torque8CallableD2Ev
.L4410:
	movq	-336(%rbp), %rdi
	cmpq	-536(%rbp), %rdi
	je	.L4416
	call	_ZdlPv@PLT
.L4416:
	movq	-304(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L4417
	call	_ZdlPv@PLT
.L4417:
	movq	-400(%rbp), %rdi
	cmpq	-520(%rbp), %rdi
	je	.L4419
	call	_ZdlPv@PLT
.L4419:
	movq	-368(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	jne	.L4497
.L4422:
	movl	$376, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4423:
	movq	-432(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4433
	call	_ZdlPv@PLT
.L4433:
	movq	-464(%rbp), %rdi
	cmpq	-552(%rbp), %rdi
	je	.L4434
	call	_ZdlPv@PLT
.L4434:
	movq	%rbx, %r12
	jmp	.L4360
.L4352:
	movq	-208(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4354
	call	_ZdlPv@PLT
.L4354:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4355
	call	_ZdlPv@PLT
.L4355:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L4497:
	call	_ZdlPv@PLT
	jmp	.L4422
.L4342:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4343
	call	_ZdlPv@PLT
.L4343:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE334:
.L4407:
	movq	-272(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4408
	call	_ZdlPv@PLT
.L4408:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4409
	call	_ZdlPv@PLT
.L4409:
	movq	-568(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	jmp	.L4410
	.cfi_endproc
.LFE6585:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE
.LLSDAC6585:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6585-.LLSDACSBC6585
.LLSDACSBC6585:
	.uleb128 .LEHB334-.LCOLDB61
	.uleb128 .LEHE334-.LEHB334
	.uleb128 0
	.uleb128 0
.LLSDACSEC6585:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE
	.section	.text._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE
	.size	_ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE, .-_ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE
	.size	_ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE.cold, .-_ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE.cold
.LCOLDE61:
	.section	.text._ZN2v88internal6torque12Declarations12CreateMethodEPNS1_13AggregateTypeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_9SignatureEPNS1_9StatementE
.LHOTE61:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.type	_ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, @function
_ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE:
.LFB8192:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8192
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB335:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE335:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%r12, -32(%rbp)
	movq	120(%rax), %rsi
	cmpq	128(%rax), %rsi
	je	.L4499
	movq	$0, -32(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rax)
.L4500:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4498
	movq	(%rdi), %rax
	call	*8(%rax)
.L4498:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4513
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4499:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	112(%rax), %rdi
.LEHB336:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE336:
	jmp	.L4500
.L4513:
	call	__stack_chk_fail@PLT
.L4505:
	endbr64
	movq	%rax, %r12
.L4502:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4503
	movq	(%rdi), %rax
	call	*8(%rax)
.L4503:
	movq	%r12, %rdi
.LEHB337:
	call	_Unwind_Resume@PLT
.LEHE337:
	.cfi_endproc
.LFE8192:
	.section	.gcc_except_table._ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"aG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
.LLSDA8192:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8192-.LLSDACSB8192
.LLSDACSB8192:
	.uleb128 .LEHB335-.LFB8192
	.uleb128 .LEHE335-.LEHB335
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB336-.LFB8192
	.uleb128 .LEHE336-.LEHB336
	.uleb128 .L4505-.LFB8192
	.uleb128 0
	.uleb128 .LEHB337-.LFB8192
	.uleb128 .LEHE337-.LEHB337
	.uleb128 0
	.uleb128 0
.LLSDACSE8192:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.size	_ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, .-_ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE,"ax",@progbits
	.align 2
.LCOLDB62:
	.section	.text._ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE,"ax",@progbits
.LHOTB62:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.type	_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE, @function
_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE:
.LFB6594:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6594
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$344, %rsp
	movl	%edx, -360(%rbp)
	movq	(%rdi), %rdx
	movq	%r9, -368(%rbp)
	movq	%r8, -376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rbx, -336(%rbp)
	cmpq	%rax, %rdx
	je	.L4573
	movq	%rdx, -336(%rbp)
	movq	16(%rdi), %rdx
	movq	%rdx, -320(%rbp)
.L4516:
	movq	8(%rdi), %rdx
	movq	%rax, (%rdi)
	leaq	16(%rsi), %rax
	leaq	-288(%rbp), %r13
	movq	$0, 8(%rdi)
	movq	%rdx, -328(%rbp)
	movq	(%rsi), %rdx
	movb	$0, 16(%rdi)
	movq	%r13, -304(%rbp)
	cmpq	%rdx, %rax
	je	.L4574
	movq	%rdx, -304(%rbp)
	movq	16(%rsi), %rdx
	movq	%rdx, -288(%rbp)
.L4518:
	movq	8(%rsi), %rdx
	movq	%rax, (%rsi)
	movl	$368, %edi
	movq	$0, 8(%rsi)
	movb	$0, 16(%rsi)
	movq	%rdx, -296(%rbp)
.LEHB338:
	call	_Znwm@PLT
.LEHE338:
	movq	%rax, %r12
	leaq	-208(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
.LEHB339:
	call	_ZN2v88internal6torque9SignatureC1ERKS2_
.LEHE339:
	movq	-304(%rbp), %rax
	leaq	-224(%rbp), %r14
	movq	%r14, -240(%rbp)
	cmpq	%r13, %rax
	je	.L4575
	movq	%rax, -240(%rbp)
	movq	-288(%rbp), %rax
	movq	%rax, -224(%rbp)
.L4520:
	movq	-296(%rbp), %rax
	leaq	-256(%rbp), %r15
	movq	%r13, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	%rax, -232(%rbp)
	movq	-336(%rbp), %rax
	movb	$0, -288(%rbp)
	movq	%r15, -272(%rbp)
	cmpq	%rbx, %rax
	je	.L4576
	movq	%rax, -272(%rbp)
	movq	-320(%rbp), %rax
	movq	%rax, -256(%rbp)
.L4522:
	movq	-328(%rbp), %rax
	movq	%rbx, -336(%rbp)
	movl	$4, 8(%r12)
	movq	%rax, -264(%rbp)
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
	movq	$0, -328(%rbp)
	movb	$0, -320(%rbp)
.LEHB340:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE340:
	movq	(%rax), %rax
	pcmpeqd	%xmm0, %xmm0
	movb	$1, 64(%r12)
	movl	$-1, 60(%r12)
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm0, 44(%r12)
	movq	$1, 80(%r12)
	movl	%eax, 40(%r12)
	leaq	120(%r12), %rax
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 128(%r12)
	movq	-272(%rbp), %rax
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movl	$0x3f800000, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movups	%xmm1, 24(%r12)
	cmpq	%r15, %rax
	je	.L4577
	movq	%rax, 128(%r12)
	movq	-256(%rbp), %rax
	movq	%rax, 144(%r12)
.L4524:
	movq	-264(%rbp), %rax
	movq	%r15, -272(%rbp)
	movq	$0, -264(%rbp)
	movq	%rax, 136(%r12)
	leaq	176(%r12), %rax
	movq	%rax, 160(%r12)
	movq	-240(%rbp), %rax
	movb	$0, -256(%rbp)
	cmpq	%r14, %rax
	je	.L4578
	movq	%rax, 160(%r12)
	movq	-224(%rbp), %rax
	movq	%rax, 176(%r12)
.L4526:
	movq	-232(%rbp), %rax
	movdqa	-208(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	216(%r12), %rdi
	leaq	-184(%rbp), %rsi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, 168(%r12)
	movq	-192(%rbp), %rax
	movups	%xmm2, 192(%r12)
	movq	%rax, 208(%r12)
	movq	%r14, -240(%rbp)
	movq	$0, -232(%rbp)
	movb	$0, -224(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	movq	-128(%rbp), %rax
	movdqu	-88(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movdqa	-144(%rbp), %xmm3
	movdqu	-104(%rbp), %xmm4
	movq	$0, -128(%rbp)
	movq	%rax, 272(%r12)
	movzbl	-120(%rbp), %eax
	movq	-272(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movb	%al, 280(%r12)
	movq	-112(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, 288(%r12)
	movzbl	-72(%rbp), %eax
	movq	$0, 336(%r12)
	movb	%al, 328(%r12)
	movq	-376(%rbp), %rax
	movups	%xmm3, 256(%r12)
	movq	%rax, 344(%r12)
	movq	-368(%rbp), %rax
	movups	%xmm4, 296(%r12)
	movq	%rax, 352(%r12)
	movups	%xmm5, 312(%r12)
	movaps	%xmm0, -96(%rbp)
	cmpq	%r15, %rdi
	je	.L4527
	call	_ZdlPv@PLT
.L4527:
	movq	-240(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4528
	call	_ZdlPv@PLT
.L4528:
	movq	-88(%rbp), %r15
	movq	-96(%rbp), %r14
	cmpq	%r14, %r15
	je	.L4529
	.p2align 4,,10
	.p2align 3
.L4533:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4530
	call	_ZdlPv@PLT
	addq	$32, %r14
	cmpq	%r14, %r15
	jne	.L4533
.L4531:
	movq	-96(%rbp), %r14
.L4529:
	testq	%r14, %r14
	je	.L4534
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L4534:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4535
	call	_ZdlPv@PLT
.L4535:
	cmpb	$0, -184(%rbp)
	je	.L4536
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4536
	call	_ZdlPv@PLT
.L4536:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4537
	call	_ZdlPv@PLT
.L4537:
	leaq	16+_ZTVN2v88internal6torque7BuiltinE(%rip), %rax
	leaq	-344(%rbp), %rdi
	movq	%r12, -344(%rbp)
	movq	%rax, (%r12)
	movl	-360(%rbp), %eax
	movl	%eax, 360(%r12)
.LEHB341:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_7BuiltinEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
.LEHE341:
	movq	-344(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L4543
	movq	(%rdi), %rax
	call	*8(%rax)
.L4543:
	movq	-304(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4544
	call	_ZdlPv@PLT
.L4544:
	movq	-336(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4514
	call	_ZdlPv@PLT
.L4514:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4579
	addq	$344, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4530:
	.cfi_restore_state
	addq	$32, %r14
	cmpq	%r14, %r15
	jne	.L4533
	jmp	.L4531
	.p2align 4,,10
	.p2align 3
.L4578:
	movdqa	-224(%rbp), %xmm7
	movups	%xmm7, 176(%r12)
	jmp	.L4526
	.p2align 4,,10
	.p2align 3
.L4577:
	movdqa	-256(%rbp), %xmm6
	movups	%xmm6, 144(%r12)
	jmp	.L4524
	.p2align 4,,10
	.p2align 3
.L4576:
	movdqa	-320(%rbp), %xmm7
	movaps	%xmm7, -256(%rbp)
	jmp	.L4522
	.p2align 4,,10
	.p2align 3
.L4575:
	movdqa	-288(%rbp), %xmm6
	movaps	%xmm6, -224(%rbp)
	jmp	.L4520
	.p2align 4,,10
	.p2align 3
.L4574:
	movdqu	16(%rsi), %xmm7
	movaps	%xmm7, -288(%rbp)
	jmp	.L4518
	.p2align 4,,10
	.p2align 3
.L4573:
	movdqu	16(%rdi), %xmm6
	movaps	%xmm6, -320(%rbp)
	jmp	.L4516
.L4579:
	call	__stack_chk_fail@PLT
.L4555:
	endbr64
	movq	%rax, -360(%rbp)
	jmp	.L4539
.L4554:
	endbr64
	movq	%rax, %r12
	jmp	.L4547
.L4553:
	endbr64
	movq	%rax, %r14
	jmp	.L4542
.L4552:
	endbr64
	movq	%rax, %r12
	jmp	.L4546
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE,"a",@progbits
.LLSDA6594:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6594-.LLSDACSB6594
.LLSDACSB6594:
	.uleb128 .LEHB338-.LFB6594
	.uleb128 .LEHE338-.LEHB338
	.uleb128 .L4552-.LFB6594
	.uleb128 0
	.uleb128 .LEHB339-.LFB6594
	.uleb128 .LEHE339-.LEHB339
	.uleb128 .L4553-.LFB6594
	.uleb128 0
	.uleb128 .LEHB340-.LFB6594
	.uleb128 .LEHE340-.LEHB340
	.uleb128 .L4555-.LFB6594
	.uleb128 0
	.uleb128 .LEHB341-.LFB6594
	.uleb128 .LEHE341-.LEHB341
	.uleb128 .L4554-.LFB6594
	.uleb128 0
.LLSDACSE6594:
	.section	.text._ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6594
	.type	_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold, @function
_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold:
.LFSB6594:
.L4539:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-272(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4540
	call	_ZdlPv@PLT
.L4540:
	movq	-240(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4541
	call	_ZdlPv@PLT
.L4541:
	movq	-384(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	-360(%rbp), %r14
.L4542:
	movq	%r12, %rdi
	movl	$368, %esi
	movq	%r14, %r12
	call	_ZdlPvm@PLT
.L4546:
	movq	-304(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4549
	call	_ZdlPv@PLT
.L4549:
	movq	-336(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4550
	call	_ZdlPv@PLT
.L4550:
	movq	%r12, %rdi
.LEHB342:
	call	_Unwind_Resume@PLT
.LEHE342:
.L4547:
	movq	-344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4546
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L4546
	.cfi_endproc
.LFE6594:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
.LLSDAC6594:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6594-.LLSDACSBC6594
.LLSDACSBC6594:
	.uleb128 .LEHB342-.LCOLDB62
	.uleb128 .LEHE342-.LEHB342
	.uleb128 0
	.uleb128 0
.LLSDACSEC6594:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.section	.text._ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.size	_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE, .-_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.size	_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold, .-_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold
.LCOLDE62:
	.section	.text._ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
.LHOTE62:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE,"ax",@progbits
	.align 2
.LCOLDB63:
	.section	.text._ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE,"ax",@progbits
.LHOTB63:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.type	_ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE, @function
_ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE:
.LFB6597:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6597
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-256(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movl	%esi, -372(%rbp)
	movq	(%rdi), %r14
	movq	%rdx, -368(%rbp)
	movq	8(%rdi), %r13
	movl	%ecx, -376(%rbp)
	movq	%r8, -384(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC10(%rip), %rax
	movq	%r15, -272(%rbp)
	movq	%rax, -352(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L4581
	testq	%r14, %r14
	je	.L4698
.L4581:
	movq	%r13, -304(%rbp)
	cmpq	$15, %r13
	ja	.L4699
	cmpq	$1, %r13
	jne	.L4584
	movzbl	(%r14), %eax
	movb	%al, -256(%rbp)
	movq	%r15, %rax
.L4585:
	movq	%r13, -264(%rbp)
	movb	$0, (%rax,%r13)
	movq	-272(%rbp), %rax
	cmpq	%r15, %rax
	je	.L4700
	movq	-256(%rbp), %rcx
	movq	-264(%rbp), %rdx
	pxor	%xmm0, %xmm0
	leaq	-168(%rbp), %r14
	leaq	-224(%rbp), %rsi
	movq	%rax, -240(%rbp)
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%r15, -272(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -256(%rbp)
	movq	$0, -192(%rbp)
	movq	%r14, -184(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	%rsi, %rax
	je	.L4587
	movq	%rax, -184(%rbp)
	movq	%rcx, -168(%rbp)
.L4589:
	leaq	-208(%rbp), %rax
	movq	%rdx, -176(%rbp)
	movq	%rax, -360(%rbp)
.LEHB343:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	leaq	-304(%rbp), %rdi
	leaq	-208(%rbp), %rdx
	movq	%rdi, -392(%rbp)
	movq	(%rax), %rsi
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE343:
	movq	-304(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	-296(%rbp), %r13
	movq	$0, -320(%rbp)
	movaps	%xmm0, -336(%rbp)
	cmpq	%r13, %rbx
	je	.L4590
	leaq	-344(%rbp), %rax
	movq	%rax, -400(%rbp)
	jmp	.L4595
	.p2align 4,,10
	.p2align 3
.L4702:
	movq	%rax, (%rsi)
	addq	$8, -328(%rbp)
.L4592:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L4701
.L4595:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L4592
	cmpl	$4, 8(%rax)
	jne	.L4592
	movq	%rax, -344(%rbp)
	movq	-328(%rbp), %rsi
	cmpq	-320(%rbp), %rsi
	jne	.L4702
	movq	-400(%rbp), %rdx
	leaq	-336(%rbp), %rdi
.LEHB344:
	call	_ZNSt6vectorIPN2v88internal6torque7BuiltinESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE344:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L4595
	.p2align 4,,10
	.p2align 3
.L4701:
	movq	-304(%rbp), %r13
.L4590:
	testq	%r13, %r13
	je	.L4600
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4600:
	movq	-184(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4601
	call	_ZdlPv@PLT
.L4601:
	movq	-200(%rbp), %rbx
	movq	-208(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L4602
	.p2align 4,,10
	.p2align 3
.L4606:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L4603
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L4606
.L4604:
	movq	-208(%rbp), %r13
.L4602:
	testq	%r13, %r13
	je	.L4607
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4607:
	movq	-272(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4608
	call	_ZdlPv@PLT
.L4608:
	movq	-336(%rbp), %rdi
	cmpq	%rdi, -328(%rbp)
	jne	.L4703
	testq	%rdi, %rdi
	je	.L4610
	call	_ZdlPv@PLT
.L4610:
	movq	-368(%rbp), %rsi
	movq	-360(%rbp), %rdi
.LEHB345:
	call	_ZN2v88internal6torque9SignatureC1ERKS2_
.LEHE345:
	movq	(%r12), %r14
	movq	8(%r12), %r13
	leaq	-224(%rbp), %rax
	movq	%rax, -368(%rbp)
	movq	%rax, -240(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L4613
	testq	%r14, %r14
	je	.L4704
.L4613:
	movq	%r13, -304(%rbp)
	cmpq	$15, %r13
	ja	.L4705
	cmpq	$1, %r13
	jne	.L4620
	movzbl	(%r14), %eax
	movb	%al, -224(%rbp)
	movq	-368(%rbp), %rax
.L4621:
	movq	%r13, -232(%rbp)
	movb	$0, (%rax,%r13)
	movq	(%r12), %r14
	movq	8(%r12), %r13
	movq	%r15, -272(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L4622
	testq	%r14, %r14
	je	.L4706
.L4622:
	movq	%r13, -304(%rbp)
	cmpq	$15, %r13
	ja	.L4707
	cmpq	$1, %r13
	jne	.L4625
	movzbl	(%r14), %eax
	leaq	-272(%rbp), %rbx
	movb	%al, -256(%rbp)
	movq	%r15, %rax
.L4626:
	movq	%r13, -264(%rbp)
	movl	-372(%rbp), %edx
	movq	%rbx, %rdi
	leaq	-240(%rbp), %rsi
	movb	$0, (%rax,%r13)
	movl	-376(%rbp), %r8d
	movq	-384(%rbp), %r9
	movq	-360(%rbp), %rcx
.LEHB346:
	call	_ZN2v88internal6torque12Declarations13CreateBuiltinENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_7Builtin4KindENS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	movq	%rax, %r13
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%r12, %rsi
	movq	(%rax), %rdi
	addq	$72, %rdi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
.LEHE346:
	movq	%r13, -304(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L4627
	movq	%r13, (%rsi)
	addq	$8, 8(%rax)
.L4628:
	movq	-272(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4629
	call	_ZdlPv@PLT
.L4629:
	movq	-240(%rbp), %rdi
	cmpq	-368(%rbp), %rdi
	je	.L4630
	call	_ZdlPv@PLT
.L4630:
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L4631
	.p2align 4,,10
	.p2align 3
.L4635:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4632
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L4635
.L4633:
	movq	-96(%rbp), %r12
.L4631:
	testq	%r12, %r12
	je	.L4636
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L4636:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4637
	call	_ZdlPv@PLT
.L4637:
	cmpb	$0, -184(%rbp)
	je	.L4638
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4638
	call	_ZdlPv@PLT
.L4638:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4580
	call	_ZdlPv@PLT
.L4580:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4708
	addq	$360, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4584:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L4709
	movq	%r15, %rax
	jmp	.L4585
	.p2align 4,,10
	.p2align 3
.L4632:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L4635
	jmp	.L4633
	.p2align 4,,10
	.p2align 3
.L4603:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L4606
	jmp	.L4604
	.p2align 4,,10
	.p2align 3
.L4620:
	testq	%r13, %r13
	jne	.L4710
	movq	-368(%rbp), %rax
	jmp	.L4621
	.p2align 4,,10
	.p2align 3
.L4625:
	testq	%r13, %r13
	jne	.L4711
	movq	%r15, %rax
	leaq	-272(%rbp), %rbx
	jmp	.L4626
	.p2align 4,,10
	.p2align 3
.L4699:
	leaq	-272(%rbp), %rbx
	leaq	-304(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB347:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE347:
	movq	%rax, -272(%rbp)
	movq	%rax, %rdi
	movq	-304(%rbp), %rax
	movq	%rax, -256(%rbp)
.L4583:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-304(%rbp), %r13
	movq	-272(%rbp), %rax
	jmp	.L4585
	.p2align 4,,10
	.p2align 3
.L4705:
	movq	-392(%rbp), %rsi
	leaq	-240(%rbp), %rdi
	xorl	%edx, %edx
.LEHB348:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE348:
	movq	%rax, -240(%rbp)
	movq	%rax, %rdi
	movq	-304(%rbp), %rax
	movq	%rax, -224(%rbp)
.L4619:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-304(%rbp), %r13
	movq	-240(%rbp), %rax
	jmp	.L4621
	.p2align 4,,10
	.p2align 3
.L4707:
	leaq	-272(%rbp), %rbx
	movq	-392(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB349:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE349:
	movq	%rax, -272(%rbp)
	movq	%rax, %rdi
	movq	-304(%rbp), %rax
	movq	%rax, -256(%rbp)
.L4624:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-304(%rbp), %r13
	movq	-272(%rbp), %rax
	jmp	.L4626
	.p2align 4,,10
	.p2align 3
.L4700:
	leaq	-168(%rbp), %r14
	pxor	%xmm0, %xmm0
	movdqa	-256(%rbp), %xmm1
	movq	-264(%rbp), %rdx
	movb	$0, -256(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -192(%rbp)
	movq	%r14, -184(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
.L4587:
	movdqa	-224(%rbp), %xmm2
	movups	%xmm2, -168(%rbp)
	jmp	.L4589
	.p2align 4,,10
	.p2align 3
.L4627:
	movq	-392(%rbp), %rdx
	movq	%rax, %rdi
.LEHB350:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE350:
	jmp	.L4628
.L4698:
	leaq	.LC16(%rip), %rdi
.LEHB351:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE351:
.L4703:
.LEHB352:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	-392(%rbp), %r8
	leaq	-352(%rbp), %rcx
	leaq	.LC27(%rip), %r9
	leaq	.LC28(%rip), %rdx
	movq	%r12, %rsi
	leaq	.LC29(%rip), %rdi
	movq	(%rax), %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_
.LEHE352:
.L4708:
	call	__stack_chk_fail@PLT
.L4706:
	leaq	.LC16(%rip), %rdi
.LEHB353:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE353:
.L4704:
	leaq	.LC16(%rip), %rdi
.LEHB354:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE354:
.L4710:
	movq	-368(%rbp), %rdi
	jmp	.L4619
.L4711:
	movq	%r15, %rdi
	leaq	-272(%rbp), %rbx
	jmp	.L4624
.L4709:
	movq	%r15, %rdi
	jmp	.L4583
.L4653:
	endbr64
	movq	%rax, %r12
	jmp	.L4616
.L4651:
	endbr64
	movq	%rax, %r12
	jmp	.L4640
.L4649:
	endbr64
	movq	%rax, %r12
	jmp	.L4644
.L4650:
	endbr64
	movq	%rax, %r12
	jmp	.L4642
.L4652:
	endbr64
	movq	%rax, %r12
	jmp	.L4614
.L4654:
	endbr64
	movq	%rax, %r12
	jmp	.L4596
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE,"a",@progbits
.LLSDA6597:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6597-.LLSDACSB6597
.LLSDACSB6597:
	.uleb128 .LEHB343-.LFB6597
	.uleb128 .LEHE343-.LEHB343
	.uleb128 .L4652-.LFB6597
	.uleb128 0
	.uleb128 .LEHB344-.LFB6597
	.uleb128 .LEHE344-.LEHB344
	.uleb128 .L4654-.LFB6597
	.uleb128 0
	.uleb128 .LEHB345-.LFB6597
	.uleb128 .LEHE345-.LEHB345
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB346-.LFB6597
	.uleb128 .LEHE346-.LEHB346
	.uleb128 .L4651-.LFB6597
	.uleb128 0
	.uleb128 .LEHB347-.LFB6597
	.uleb128 .LEHE347-.LEHB347
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB348-.LFB6597
	.uleb128 .LEHE348-.LEHB348
	.uleb128 .L4649-.LFB6597
	.uleb128 0
	.uleb128 .LEHB349-.LFB6597
	.uleb128 .LEHE349-.LEHB349
	.uleb128 .L4650-.LFB6597
	.uleb128 0
	.uleb128 .LEHB350-.LFB6597
	.uleb128 .LEHE350-.LEHB350
	.uleb128 .L4651-.LFB6597
	.uleb128 0
	.uleb128 .LEHB351-.LFB6597
	.uleb128 .LEHE351-.LEHB351
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB352-.LFB6597
	.uleb128 .LEHE352-.LEHB352
	.uleb128 .L4653-.LFB6597
	.uleb128 0
	.uleb128 .LEHB353-.LFB6597
	.uleb128 .LEHE353-.LEHB353
	.uleb128 .L4650-.LFB6597
	.uleb128 0
	.uleb128 .LEHB354-.LFB6597
	.uleb128 .LEHE354-.LEHB354
	.uleb128 .L4649-.LFB6597
	.uleb128 0
.LLSDACSE6597:
	.section	.text._ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6597
	.type	_ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold, @function
_ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold:
.LFSB6597:
.L4616:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4696
.L4697:
	call	_ZdlPv@PLT
.L4696:
	movq	%r12, %rdi
.LEHB355:
	call	_Unwind_Resume@PLT
.LEHE355:
.L4640:
	movq	-272(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4642
	call	_ZdlPv@PLT
.L4642:
	movq	-240(%rbp), %rdi
	cmpq	-368(%rbp), %rdi
	je	.L4644
	call	_ZdlPv@PLT
.L4644:
	movq	-360(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	jmp	.L4696
.L4596:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4597
	call	_ZdlPv@PLT
.L4597:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4614
	call	_ZdlPv@PLT
.L4614:
	movq	-360(%rbp), %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	-272(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L4697
	jmp	.L4696
	.cfi_endproc
.LFE6597:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
.LLSDAC6597:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6597-.LLSDACSBC6597
.LLSDACSBC6597:
	.uleb128 .LEHB355-.LCOLDB63
	.uleb128 .LEHE355-.LEHB355
	.uleb128 0
	.uleb128 0
.LLSDACSEC6597:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.section	.text._ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.size	_ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE, .-_ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
	.size	_ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold, .-_ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE.cold
.LCOLDE63:
	.section	.text._ZN2v88internal6torque12Declarations14DeclareBuiltinERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_7Builtin4KindERKNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEE
.LHOTE63:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.type	_ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, @function
_ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE:
.LFB8120:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8120
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB356:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE356:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%r12, -32(%rbp)
	movq	120(%rax), %rsi
	cmpq	128(%rax), %rsi
	je	.L4713
	movq	$0, -32(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rax)
.L4714:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4712
	movq	(%rdi), %rax
	call	*8(%rax)
.L4712:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4727
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4713:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	112(%rax), %rdi
.LEHB357:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE357:
	jmp	.L4714
.L4727:
	call	__stack_chk_fail@PLT
.L4719:
	endbr64
	movq	%rax, %r12
.L4716:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4717
	movq	(%rdi), %rax
	call	*8(%rax)
.L4717:
	movq	%r12, %rdi
.LEHB358:
	call	_Unwind_Resume@PLT
.LEHE358:
	.cfi_endproc
.LFE8120:
	.section	.gcc_except_table._ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"aG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
.LLSDA8120:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8120-.LLSDACSB8120
.LLSDACSB8120:
	.uleb128 .LEHB356-.LFB8120
	.uleb128 .LEHE356-.LEHB356
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB357-.LFB8120
	.uleb128 .LEHE357-.LEHB357
	.uleb128 .L4719-.LFB8120
	.uleb128 0
	.uleb128 .LEHB358-.LFB8120
	.uleb128 .LEHE358-.LEHB358
	.uleb128 0
	.uleb128 0
.LLSDACSE8120:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.size	_ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, .-_ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb,"ax",@progbits
	.align 2
.LCOLDB64:
	.section	.text._ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb,"ax",@progbits
.LHOTB64:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb
	.type	_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb, @function
_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb:
.LFB6577:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6577
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-240(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %eax
	movl	%edx, -540(%rbp)
	movq	%rcx, -520(%rbp)
	movq	%r9, -528(%rbp)
	movq	%r8, -536(%rbp)
	movl	%eax, -544(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB359:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC17(%rip), %rcx
	movq	%r14, %rdi
	movq	(%rax), %rax
	movq	232(%rax), %r8
	leaq	1(%r8), %rdx
	movq	%rdx, 232(%rax)
	movl	$32, %edx
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
.LEHE359:
	movl	$1, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC52(%rip), %rcx
	movq	%r14, %rdi
.LEHB360:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE360:
	leaq	-192(%rbp), %rbx
	leaq	16(%rax), %rdx
	movq	%rbx, -208(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L4818
	movq	%rcx, -208(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -192(%rbp)
.L4730:
	movq	8(%rax), %rcx
	movq	%r13, %rdi
	movq	%rcx, -200(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rsi
.LEHB361:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE361:
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4731
	call	_ZdlPv@PLT
.L4731:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L4732
	call	_ZdlPv@PLT
.L4732:
	movq	0(%r13), %rdx
	leaq	-448(%rbp), %r15
	leaq	16(%r13), %rax
	movq	%r15, -464(%rbp)
	cmpq	%rax, %rdx
	je	.L4819
	movq	%rdx, -464(%rbp)
	movq	16(%r13), %rdx
	movq	%rdx, -448(%rbp)
.L4734:
	movq	8(%r13), %rdx
	movq	%rax, 0(%r13)
	leaq	-416(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	%rdx, -456(%rbp)
	movq	(%r12), %rdx
	movq	%rax, -432(%rbp)
	leaq	16(%r12), %rax
	movq	$0, 8(%r13)
	movb	$0, 16(%r13)
	cmpq	%rax, %rdx
	je	.L4820
	movq	%rdx, -432(%rbp)
	movq	16(%r12), %rdx
	movq	%rdx, -416(%rbp)
.L4736:
	movq	8(%r12), %rdx
	movq	%rax, (%r12)
	movl	$368, %edi
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	movq	%rdx, -424(%rbp)
.LEHB362:
	call	_Znwm@PLT
.LEHE362:
	movq	%rax, %r12
	leaq	-352(%rbp), %rax
	movq	-432(%rbp), %rdx
	movq	%rax, -496(%rbp)
	cmpq	-512(%rbp), %rdx
	je	.L4821
	movq	-416(%rbp), %rax
	movq	%rax, -352(%rbp)
.L4738:
	movq	-512(%rbp), %rax
	movq	-424(%rbp), %rsi
	movb	$0, -416(%rbp)
	movq	$0, -424(%rbp)
	movq	%rax, -432(%rbp)
	leaq	-384(%rbp), %rax
	movq	%rax, -488(%rbp)
	movq	-464(%rbp), %rax
	cmpq	%r15, %rax
	je	.L4822
	movq	-448(%rbp), %rcx
	movq	%rcx, -384(%rbp)
.L4740:
	leaq	-288(%rbp), %rdi
	movq	-456(%rbp), %rcx
	movq	%r15, -464(%rbp)
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	movq	%rdi, -504(%rbp)
	movq	%rdi, -304(%rbp)
	cmpq	-496(%rbp), %rdx
	je	.L4823
	movq	%rdx, -304(%rbp)
	movq	-352(%rbp), %rdx
	movq	%rdx, -288(%rbp)
.L4742:
	movq	%rsi, -296(%rbp)
	movq	-496(%rbp), %rsi
	leaq	-320(%rbp), %r14
	movq	$0, -360(%rbp)
	movq	%rsi, -368(%rbp)
	movb	$0, -352(%rbp)
	movq	%r14, -336(%rbp)
	cmpq	-488(%rbp), %rax
	je	.L4824
	movq	%rax, -336(%rbp)
	movq	-384(%rbp), %rax
	movq	%rax, -320(%rbp)
.L4744:
	movq	-488(%rbp), %rax
	movq	-520(%rbp), %rsi
	movq	%rcx, -328(%rbp)
	movq	$0, -392(%rbp)
	movq	%rax, -400(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, %rdi
	movb	$0, -384(%rbp)
	movq	%rax, -552(%rbp)
.LEHB363:
	call	_ZN2v88internal6torque9SignatureC1ERKS2_
.LEHE363:
	movq	%rbx, -240(%rbp)
	movq	-304(%rbp), %rax
	cmpq	-504(%rbp), %rax
	je	.L4825
	movq	%rax, -240(%rbp)
	movq	-288(%rbp), %rax
	movq	%rax, -224(%rbp)
.L4746:
	movq	-296(%rbp), %rax
	leaq	-256(%rbp), %r13
	movq	$0, -296(%rbp)
	movb	$0, -288(%rbp)
	movq	%rax, -232(%rbp)
	movq	-504(%rbp), %rax
	movq	%r13, -272(%rbp)
	movq	%rax, -304(%rbp)
	movq	-336(%rbp), %rax
	cmpq	%r14, %rax
	je	.L4826
	movq	%rax, -272(%rbp)
	movq	-320(%rbp), %rax
	movq	%rax, -256(%rbp)
.L4748:
	movq	-328(%rbp), %rax
	movq	%r14, -336(%rbp)
	movl	$1, 8(%r12)
	movq	%rax, -264(%rbp)
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
	movq	$0, -328(%rbp)
	movb	$0, -320(%rbp)
.LEHB364:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE364:
	movq	(%rax), %rax
	pcmpeqd	%xmm0, %xmm0
	movb	$1, 64(%r12)
	movl	$-1, 60(%r12)
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm0, 44(%r12)
	movq	$1, 80(%r12)
	movl	%eax, 40(%r12)
	leaq	120(%r12), %rax
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 128(%r12)
	movq	-272(%rbp), %rax
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movl	$0x3f800000, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movups	%xmm1, 24(%r12)
	cmpq	%r13, %rax
	je	.L4827
	movq	%rax, 128(%r12)
	movq	-256(%rbp), %rax
	movq	%rax, 144(%r12)
.L4750:
	movq	-264(%rbp), %rax
	movq	%r13, -272(%rbp)
	movq	$0, -264(%rbp)
	movq	%rax, 136(%r12)
	leaq	176(%r12), %rax
	movq	%rax, 160(%r12)
	movq	-240(%rbp), %rax
	movb	$0, -256(%rbp)
	cmpq	%rbx, %rax
	je	.L4828
	movq	%rax, 160(%r12)
	movq	-224(%rbp), %rax
	movq	%rax, 176(%r12)
.L4752:
	movq	-232(%rbp), %rax
	movdqa	-208(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	216(%r12), %rdi
	leaq	-184(%rbp), %rsi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, 168(%r12)
	movq	-192(%rbp), %rax
	movups	%xmm2, 192(%r12)
	movq	%rax, 208(%r12)
	movq	%rbx, -240(%rbp)
	movq	$0, -232(%rbp)
	movb	$0, -224(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	movq	-128(%rbp), %rax
	movdqu	-88(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movdqa	-144(%rbp), %xmm3
	movdqu	-104(%rbp), %xmm4
	movq	$0, -128(%rbp)
	movq	%rax, 272(%r12)
	movzbl	-120(%rbp), %eax
	movq	-272(%rbp), %rdi
	movaps	%xmm0, -144(%rbp)
	movb	%al, 280(%r12)
	movq	-112(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, 288(%r12)
	movzbl	-72(%rbp), %eax
	movq	$0, 336(%r12)
	movb	%al, 328(%r12)
	movq	-536(%rbp), %rax
	movups	%xmm3, 256(%r12)
	movq	%rax, 344(%r12)
	movq	-528(%rbp), %rax
	movups	%xmm4, 296(%r12)
	movq	%rax, 352(%r12)
	movups	%xmm5, 312(%r12)
	movaps	%xmm0, -96(%rbp)
	cmpq	%r13, %rdi
	je	.L4753
	call	_ZdlPv@PLT
.L4753:
	movq	-240(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4754
	call	_ZdlPv@PLT
.L4754:
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L4755
	.p2align 4,,10
	.p2align 3
.L4759:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4756
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L4759
.L4757:
	movq	-96(%rbp), %r13
.L4755:
	testq	%r13, %r13
	je	.L4760
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4760:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4761
	call	_ZdlPv@PLT
.L4761:
	cmpb	$0, -184(%rbp)
	je	.L4762
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4762
	call	_ZdlPv@PLT
.L4762:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4763
	call	_ZdlPv@PLT
.L4763:
	movb	$0, 360(%r12)
	leaq	16+_ZTVN2v88internal6torque5MacroE(%rip), %rax
	movq	%rax, (%r12)
	movq	-520(%rbp), %rax
	cmpb	$0, 88(%rax)
	jne	.L4829
	movq	-336(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4770
	call	_ZdlPv@PLT
.L4770:
	movq	-304(%rbp), %rdi
	cmpq	-504(%rbp), %rdi
	je	.L4771
	call	_ZdlPv@PLT
.L4771:
	leaq	16+_ZTVN2v88internal6torque11TorqueMacroE(%rip), %rax
	movq	-400(%rbp), %rdi
	movq	%rax, (%r12)
	movzbl	-540(%rbp), %eax
	movb	%al, 361(%r12)
	movzbl	-544(%rbp), %eax
	movb	%al, 64(%r12)
	cmpq	-488(%rbp), %rdi
	je	.L4773
	call	_ZdlPv@PLT
.L4773:
	movq	-368(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L4778
	call	_ZdlPv@PLT
.L4778:
	leaq	-472(%rbp), %rdi
	movq	%r12, -472(%rbp)
.LEHB365:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_11TorqueMacroEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
.LEHE365:
	movq	-472(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L4782
	movq	(%rdi), %rax
	call	*8(%rax)
.L4782:
	movq	-432(%rbp), %rdi
	cmpq	-512(%rbp), %rdi
	je	.L4783
	call	_ZdlPv@PLT
.L4783:
	movq	-464(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4728
	call	_ZdlPv@PLT
.L4728:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4830
	addq	$520, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4756:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L4759
	jmp	.L4757
	.p2align 4,,10
	.p2align 3
.L4828:
	movdqa	-224(%rbp), %xmm3
	movups	%xmm3, 176(%r12)
	jmp	.L4752
	.p2align 4,,10
	.p2align 3
.L4827:
	movdqa	-256(%rbp), %xmm2
	movups	%xmm2, 144(%r12)
	jmp	.L4750
	.p2align 4,,10
	.p2align 3
.L4826:
	movdqa	-320(%rbp), %xmm1
	movaps	%xmm1, -256(%rbp)
	jmp	.L4748
	.p2align 4,,10
	.p2align 3
.L4825:
	movdqa	-288(%rbp), %xmm7
	movaps	%xmm7, -224(%rbp)
	jmp	.L4746
	.p2align 4,,10
	.p2align 3
.L4824:
	movdqa	-384(%rbp), %xmm6
	movaps	%xmm6, -320(%rbp)
	jmp	.L4744
	.p2align 4,,10
	.p2align 3
.L4823:
	movdqa	-352(%rbp), %xmm7
	movaps	%xmm7, -288(%rbp)
	jmp	.L4742
	.p2align 4,,10
	.p2align 3
.L4822:
	movdqa	-448(%rbp), %xmm6
	movq	-488(%rbp), %rax
	movaps	%xmm6, -384(%rbp)
	jmp	.L4740
	.p2align 4,,10
	.p2align 3
.L4821:
	movdqa	-416(%rbp), %xmm7
	movq	%rax, %rdx
	movaps	%xmm7, -352(%rbp)
	jmp	.L4738
	.p2align 4,,10
	.p2align 3
.L4820:
	movdqu	16(%r12), %xmm6
	movaps	%xmm6, -416(%rbp)
	jmp	.L4736
	.p2align 4,,10
	.p2align 3
.L4819:
	movdqu	16(%r13), %xmm7
	movaps	%xmm7, -448(%rbp)
	jmp	.L4734
	.p2align 4,,10
	.p2align 3
.L4818:
	movdqu	16(%rax), %xmm6
	movaps	%xmm6, -192(%rbp)
	jmp	.L4730
.L4830:
	call	__stack_chk_fail@PLT
.L4829:
	leaq	.LC58(%rip), %rdi
.LEHB366:
	call	_ZN2v88internal6torque11ReportErrorIJRA38_KcEEEvDpOT_
.LEHE366:
.L4796:
	endbr64
	movq	%rax, %rbx
	jmp	.L4781
.L4794:
	endbr64
	movq	%rax, %r12
	jmp	.L4787
.L4799:
	endbr64
	movq	%rax, %rbx
	jmp	.L4769
.L4797:
	endbr64
	movq	%rax, %rbx
	jmp	.L4789
.L4800:
	endbr64
	movq	%rax, -520(%rbp)
	jmp	.L4765
.L4798:
	endbr64
	movq	%rax, %rbx
	jmp	.L4768
.L4795:
	endbr64
	movq	%rax, %r12
	jmp	.L4785
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb,"a",@progbits
.LLSDA6577:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6577-.LLSDACSB6577
.LLSDACSB6577:
	.uleb128 .LEHB359-.LFB6577
	.uleb128 .LEHE359-.LEHB359
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB360-.LFB6577
	.uleb128 .LEHE360-.LEHB360
	.uleb128 .L4794-.LFB6577
	.uleb128 0
	.uleb128 .LEHB361-.LFB6577
	.uleb128 .LEHE361-.LEHB361
	.uleb128 .L4795-.LFB6577
	.uleb128 0
	.uleb128 .LEHB362-.LFB6577
	.uleb128 .LEHE362-.LEHB362
	.uleb128 .L4796-.LFB6577
	.uleb128 0
	.uleb128 .LEHB363-.LFB6577
	.uleb128 .LEHE363-.LEHB363
	.uleb128 .L4798-.LFB6577
	.uleb128 0
	.uleb128 .LEHB364-.LFB6577
	.uleb128 .LEHE364-.LEHB364
	.uleb128 .L4800-.LFB6577
	.uleb128 0
	.uleb128 .LEHB365-.LFB6577
	.uleb128 .LEHE365-.LEHB365
	.uleb128 .L4797-.LFB6577
	.uleb128 0
	.uleb128 .LEHB366-.LFB6577
	.uleb128 .LEHE366-.LEHB366
	.uleb128 .L4799-.LFB6577
	.uleb128 0
.LLSDACSE6577:
	.section	.text._ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6577
	.type	_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb.cold, @function
_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb.cold:
.LFSB6577:
.L4769:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	call	_ZN2v88internal6torque8CallableD2Ev
.L4768:
	movq	-336(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L4831
.L4774:
	movq	-304(%rbp), %rdi
	cmpq	-504(%rbp), %rdi
	je	.L4775
	call	_ZdlPv@PLT
.L4775:
	movq	-400(%rbp), %rdi
	cmpq	-488(%rbp), %rdi
	je	.L4777
	call	_ZdlPv@PLT
.L4777:
	movq	-368(%rbp), %rdi
	cmpq	-496(%rbp), %rdi
	je	.L4780
	call	_ZdlPv@PLT
.L4780:
	movl	$368, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4781:
	movq	-432(%rbp), %rdi
	cmpq	-512(%rbp), %rdi
	je	.L4791
	call	_ZdlPv@PLT
.L4791:
	movq	-464(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4792
	call	_ZdlPv@PLT
.L4792:
	movq	%rbx, %rdi
.LEHB367:
	call	_Unwind_Resume@PLT
.L4785:
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4787
	call	_ZdlPv@PLT
.L4787:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4788
	call	_ZdlPv@PLT
.L4788:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE367:
.L4789:
	movq	-472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4781
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L4781
.L4765:
	movq	-272(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4766
	call	_ZdlPv@PLT
.L4766:
	movq	-240(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4767
	call	_ZdlPv@PLT
.L4767:
	movq	-552(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	-520(%rbp), %rbx
	jmp	.L4768
.L4831:
	call	_ZdlPv@PLT
	jmp	.L4774
	.cfi_endproc
.LFE6577:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb
.LLSDAC6577:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6577-.LLSDACSBC6577
.LLSDACSBC6577:
	.uleb128 .LEHB367-.LCOLDB64
	.uleb128 .LEHE367-.LEHB367
	.uleb128 0
	.uleb128 0
.LLSDACSEC6577:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb
	.section	.text._ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb
	.size	_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb, .-_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb
	.section	.text.unlikely._ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb
	.size	_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb.cold, .-_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb.cold
.LCOLDE64:
	.section	.text._ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb
.LHOTE64:
	.section	.rodata._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b.str1.8,"aMS",@progbits,1
	.align 8
.LC65:
	.string	" with identical explicit parameters"
	.section	.rodata._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b.str1.1,"aMS",@progbits,1
.LC66:
	.string	"cannot redeclare macro "
.LC67:
	.string	"cannot redeclare operator "
	.section	.text.unlikely._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b,"ax",@progbits
	.align 2
.LCOLDB68:
	.section	.text._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b,"ax",@progbits
.LHOTB68:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b
	.type	_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b, @function
_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b:
.LFB6583:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6583
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$328, %rsp
	movq	16(%rbp), %rax
	movl	%esi, -340(%rbp)
	movq	96(%r14), %rdx
	movq	72(%rcx), %rcx
	movl	%r8d, -344(%rbp)
	movq	%rax, -320(%rbp)
	movl	24(%rbp), %eax
	movq	%r9, -352(%rbp)
	movl	%eax, -328(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%r14), %rax
	movq	%rcx, -312(%rbp)
	movq	$0, -288(%rbp)
	leaq	(%rax,%rdx,8), %r13
	movaps	%xmm0, -304(%rbp)
	subq	%r13, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	testq	%rcx, %rcx
	js	.L4961
	movq	%rdi, %r15
	movq	%rcx, %r12
	testq	%rax, %rax
	je	.L4896
	movq	%rcx, %rdi
.LEHB368:
	call	_Znwm@PLT
.LEHE368:
	movq	%rax, %rdi
.L4834:
	leaq	(%rdi,%r12), %rcx
	movq	%rdi, -304(%rbp)
	movq	%rcx, -288(%rbp)
	cmpq	-312(%rbp), %r13
	je	.L4835
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rcx, -312(%rbp)
	call	memcpy@PLT
	movq	-312(%rbp), %rcx
.L4835:
	leaq	-304(%rbp), %rax
	movq	%r15, %rdi
	movq	%rcx, -296(%rbp)
	movq	%rax, %rsi
	movq	%rax, -336(%rbp)
.LEHB369:
	call	_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
.LEHE369:
	movq	-304(%rbp), %rdi
	movq	%rax, -312(%rbp)
	testq	%rdi, %rdi
	je	.L4839
	call	_ZdlPv@PLT
.L4839:
	cmpq	$0, -312(%rbp)
	jne	.L4962
	cmpb	$0, (%rbx)
	je	.L4842
	leaq	-208(%rbp), %rax
	movq	%r14, %rsi
	leaq	-224(%rbp), %r13
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
.LEHB370:
	call	_ZN2v88internal6torque9SignatureC1ERKS2_
.LEHE370:
	movq	8(%rbx), %rdx
	leaq	24(%rbx), %rax
	movq	%r13, -240(%rbp)
	cmpq	%rax, %rdx
	je	.L4963
	movq	%rdx, -240(%rbp)
	movq	24(%rbx), %rdx
	movq	%rdx, -224(%rbp)
.L4844:
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rbx)
	leaq	-272(%rbp), %r12
	movq	$0, 16(%rbx)
	movq	%r12, %rdi
	movb	$0, 24(%rbx)
	movq	(%r15), %rsi
	leaq	-256(%rbp), %rbx
	movq	%rdx, -232(%rbp)
	movq	8(%r15), %rdx
	movq	%rbx, -272(%rbp)
	addq	%rsi, %rdx
.LEHB371:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE371:
	movq	-328(%rbp), %rdx
	leaq	-240(%rbp), %rsi
	movq	%r12, %rdi
.LEHB372:
	call	_ZN2v88internal6torque12Declarations17CreateExternMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_NS1_9SignatureE
.LEHE372:
	movq	-272(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%rbx, %rdi
	je	.L4845
	call	_ZdlPv@PLT
.L4845:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4846
	call	_ZdlPv@PLT
.L4846:
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L4859
	.p2align 4,,10
	.p2align 3
.L4851:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4848
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L4851
.L4861:
	movq	-96(%rbp), %r13
.L4859:
	testq	%r13, %r13
	je	.L4864
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L4864:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4865
	call	_ZdlPv@PLT
.L4865:
	cmpb	$0, -184(%rbp)
	je	.L4866
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4866
	call	_ZdlPv@PLT
.L4866:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4856
	call	_ZdlPv@PLT
.L4856:
.LEHB373:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	%r15, %rsi
	movq	(%rax), %rax
	movq	(%rax), %rdi
	addq	$72, %rdi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	%r12, -304(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L4868
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L4869:
	movq	-320(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L4964
.L4832:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4965
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4842:
	.cfi_restore_state
	movzbl	-328(%rbp), %eax
	movq	%r14, %rsi
	leaq	-224(%rbp), %r13
	movl	%eax, -356(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal6torque9SignatureC1ERKS2_
.LEHE373:
	movq	(%r15), %rsi
	movq	8(%r15), %rdx
	leaq	-240(%rbp), %rax
	movzbl	-340(%rbp), %ebx
	movq	%rax, %rdi
	movq	%r13, -240(%rbp)
	addq	%rsi, %rdx
	movq	%rax, -368(%rbp)
	movl	%ebx, -340(%rbp)
.LEHB374:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE374:
	movq	(%r15), %rsi
	movq	8(%r15), %rax
	leaq	-272(%rbp), %r12
	leaq	-256(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, -272(%rbp)
	addq	%rsi, %rax
	movq	%rax, %rdx
.LEHB375:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE375:
	movl	-356(%rbp), %eax
	subq	$8, %rsp
	movl	-344(%rbp), %r8d
	movq	%r12, %rdi
	movq	-352(%rbp), %r9
	movq	-328(%rbp), %rcx
	movl	-340(%rbp), %edx
	movq	-368(%rbp), %rsi
	pushq	%rax
.LEHB376:
	.cfi_escape 0x2e,0x10
	call	_ZN2v88internal6torque12Declarations17CreateTorqueMacroENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_bNS1_9SignatureENS_4base8OptionalIPNS1_9StatementEEEb
.LEHE376:
	movq	-272(%rbp), %rdi
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	cmpq	%rbx, %rdi
	je	.L4857
	call	_ZdlPv@PLT
.L4857:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4858
	call	_ZdlPv@PLT
.L4858:
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L4859
.L4863:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L4860
.L4966:
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	je	.L4861
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L4966
.L4860:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L4863
	jmp	.L4861
	.p2align 4,,10
	.p2align 3
.L4848:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L4851
	jmp	.L4861
	.p2align 4,,10
	.p2align 3
.L4964:
	movq	64(%r14), %rax
	movq	96(%r14), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -288(%rbp)
	movq	72(%r14), %rcx
	movaps	%xmm0, -304(%rbp)
	leaq	(%rax,%rdx,8), %r14
	movq	%rcx, %rbx
	subq	%r14, %rcx
	movq	%rcx, %rax
	movq	%rcx, %r13
	sarq	$3, %rax
	testq	%rcx, %rcx
	js	.L4967
	testq	%rax, %rax
	je	.L4872
	movq	%rcx, %rdi
.LEHB377:
	.cfi_escape 0x2e,0
	call	_Znwm@PLT
.LEHE377:
	movq	%rax, -312(%rbp)
.L4872:
	movq	-312(%rbp), %rdi
	leaq	(%rdi,%r13), %rcx
	movq	%rdi, -304(%rbp)
	movq	%rcx, -288(%rbp)
	cmpq	%r14, %rbx
	je	.L4873
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rcx, -312(%rbp)
	call	memcpy@PLT
	movq	-312(%rbp), %rcx
.L4873:
	movq	-320(%rbp), %r13
	movq	-336(%rbp), %rsi
	movq	%rcx, -296(%rbp)
	addq	$8, %r13
	movq	%r13, %rdi
.LEHB378:
	call	_ZN2v88internal6torque12Declarations14TryLookupMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKSt6vectorIPKNS1_4TypeESaISE_EE
.LEHE378:
	movq	-304(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4877
	call	_ZdlPv@PLT
.L4877:
	testq	%rbx, %rbx
	jne	.L4968
.LEHB379:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	%r13, %rsi
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	addq	$72, %rdi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	%r12, -304(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L4880
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
	jmp	.L4832
	.p2align 4,,10
	.p2align 3
.L4896:
	xorl	%edi, %edi
	jmp	.L4834
	.p2align 4,,10
	.p2align 3
.L4868:
	movq	-336(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L4869
	.p2align 4,,10
	.p2align 3
.L4963:
	movdqu	24(%rbx), %xmm1
	movaps	%xmm1, -224(%rbp)
	jmp	.L4844
	.p2align 4,,10
	.p2align 3
.L4880:
	movq	-336(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L4832
.L4965:
	call	__stack_chk_fail@PLT
.L4962:
	leaq	-208(%rbp), %r13
	leaq	.LC65(%rip), %rcx
	movq	%r15, %rdx
	movq	%r13, %rdi
	leaq	.LC66(%rip), %rsi
	call	_ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE379:
	movq	%r13, %rdi
.LEHB380:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE380:
.L4961:
	leaq	.LC25(%rip), %rdi
.LEHB381:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE381:
.L4967:
	leaq	.LC25(%rip), %rdi
.LEHB382:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE382:
.L4968:
	movq	-328(%rbp), %rbx
	leaq	.LC65(%rip), %rcx
	movq	%r15, %rdx
	leaq	.LC67(%rip), %rsi
	movq	%rbx, %rdi
.LEHB383:
	call	_ZN2v88internal6torqueL7MessageIJRA24_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA36_S3_EEENS1_14MessageBuilderENS1_13TorqueMessage4KindEDpOT_.constprop.0
.LEHE383:
	movq	%rbx, %rdi
.LEHB384:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE384:
.L4897:
	endbr64
	movq	%rax, %r12
	jmp	.L4882
.L4905:
	endbr64
	movq	%rax, %r12
	jmp	.L4841
.L4907:
	endbr64
	movq	%rax, %r12
	jmp	.L4879
.L4904:
	endbr64
	movq	%rax, %r12
	jmp	.L4837
.L4900:
	endbr64
	movq	%rax, %rbx
	jmp	.L4892
.L4901:
	endbr64
	movq	%rax, %rbx
	jmp	.L4890
.L4898:
	endbr64
	movq	%rax, %r12
	jmp	.L4886
.L4899:
	endbr64
	movq	%rax, %r12
	jmp	.L4884
.L4902:
	endbr64
	movq	%rax, %r12
	jmp	.L4888
.L4903:
	endbr64
	movq	%rax, %r12
	jmp	.L4893
.L4906:
	endbr64
	movq	%rax, %r12
	jmp	.L4875
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b,"a",@progbits
.LLSDA6583:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6583-.LLSDACSB6583
.LLSDACSB6583:
	.uleb128 .LEHB368-.LFB6583
	.uleb128 .LEHE368-.LEHB368
	.uleb128 .L4904-.LFB6583
	.uleb128 0
	.uleb128 .LEHB369-.LFB6583
	.uleb128 .LEHE369-.LEHB369
	.uleb128 .L4897-.LFB6583
	.uleb128 0
	.uleb128 .LEHB370-.LFB6583
	.uleb128 .LEHE370-.LEHB370
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB371-.LFB6583
	.uleb128 .LEHE371-.LEHB371
	.uleb128 .L4898-.LFB6583
	.uleb128 0
	.uleb128 .LEHB372-.LFB6583
	.uleb128 .LEHE372-.LEHB372
	.uleb128 .L4899-.LFB6583
	.uleb128 0
	.uleb128 .LEHB373-.LFB6583
	.uleb128 .LEHE373-.LEHB373
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB374-.LFB6583
	.uleb128 .LEHE374-.LEHB374
	.uleb128 .L4900-.LFB6583
	.uleb128 0
	.uleb128 .LEHB375-.LFB6583
	.uleb128 .LEHE375-.LEHB375
	.uleb128 .L4901-.LFB6583
	.uleb128 0
	.uleb128 .LEHB376-.LFB6583
	.uleb128 .LEHE376-.LEHB376
	.uleb128 .L4902-.LFB6583
	.uleb128 0
	.uleb128 .LEHB377-.LFB6583
	.uleb128 .LEHE377-.LEHB377
	.uleb128 .L4906-.LFB6583
	.uleb128 0
	.uleb128 .LEHB378-.LFB6583
	.uleb128 .LEHE378-.LEHB378
	.uleb128 .L4903-.LFB6583
	.uleb128 0
	.uleb128 .LEHB379-.LFB6583
	.uleb128 .LEHE379-.LEHB379
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB380-.LFB6583
	.uleb128 .LEHE380-.LEHB380
	.uleb128 .L4905-.LFB6583
	.uleb128 0
	.uleb128 .LEHB381-.LFB6583
	.uleb128 .LEHE381-.LEHB381
	.uleb128 .L4904-.LFB6583
	.uleb128 0
	.uleb128 .LEHB382-.LFB6583
	.uleb128 .LEHE382-.LEHB382
	.uleb128 .L4906-.LFB6583
	.uleb128 0
	.uleb128 .LEHB383-.LFB6583
	.uleb128 .LEHE383-.LEHB383
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB384-.LFB6583
	.uleb128 .LEHE384-.LEHB384
	.uleb128 .L4907-.LFB6583
	.uleb128 0
.LLSDACSE6583:
	.section	.text._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6583
	.type	_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b.cold, @function
_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b.cold:
.LFSB6583:
.L4882:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4883
	call	_ZdlPv@PLT
.L4883:
	movq	%r12, %rdi
.LEHB385:
	call	_Unwind_Resume@PLT
.L4841:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L4879:
	movq	-328(%rbp), %rdi
	call	_ZN2v88internal6torque14MessageBuilderD1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L4837:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4838
	call	_ZdlPv@PLT
.L4838:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L4888:
	movq	-272(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4889
	call	_ZdlPv@PLT
.L4889:
	movq	%r12, %rbx
.L4890:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4892
	call	_ZdlPv@PLT
.L4892:
	movq	-328(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.L4884:
	movq	-272(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4886
	call	_ZdlPv@PLT
.L4886:
	movq	-240(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L4887
	call	_ZdlPv@PLT
.L4887:
	movq	-328(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L4893:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4894
	call	_ZdlPv@PLT
.L4894:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.L4875:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4876
	call	_ZdlPv@PLT
.L4876:
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE385:
	.cfi_endproc
.LFE6583:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b
.LLSDAC6583:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6583-.LLSDACSBC6583
.LLSDACSBC6583:
	.uleb128 .LEHB385-.LCOLDB68
	.uleb128 .LEHE385-.LEHB385
	.uleb128 0
	.uleb128 0
.LLSDACSEC6583:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b
	.section	.text._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b
	.size	_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b, .-_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b
	.section	.text.unlikely._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b
	.size	_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b.cold, .-_ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b.cold
.LCOLDE68:
	.section	.text._ZN2v88internal6torque12Declarations12DeclareMacroERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEbNS_4base8OptionalIS8_EERKNS1_9SignatureENSC_IPNS1_9StatementEEESD_b
.LHOTE68:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.type	_ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, @function
_ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE:
.LFB8220:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8220
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB386:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE386:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%r12, -32(%rbp)
	movq	120(%rax), %rsi
	cmpq	128(%rax), %rsi
	je	.L4970
	movq	$0, -32(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rax)
.L4971:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4969
	movq	(%rdi), %rax
	call	*8(%rax)
.L4969:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4984
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4970:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	112(%rax), %rdi
.LEHB387:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE387:
	jmp	.L4971
.L4984:
	call	__stack_chk_fail@PLT
.L4976:
	endbr64
	movq	%rax, %r12
.L4973:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4974
	movq	(%rdi), %rax
	call	*8(%rax)
.L4974:
	movq	%r12, %rdi
.LEHB388:
	call	_Unwind_Resume@PLT
.LEHE388:
	.cfi_endproc
.LFE8220:
	.section	.gcc_except_table._ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"aG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
.LLSDA8220:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE8220-.LLSDACSB8220
.LLSDACSB8220:
	.uleb128 .LEHB386-.LFB8220
	.uleb128 .LEHE386-.LEHB386
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB387-.LFB8220
	.uleb128 .LEHE387-.LEHB387
	.uleb128 .L4976-.LFB8220
	.uleb128 0
	.uleb128 .LEHB388-.LFB8220
	.uleb128 .LEHE388-.LEHB388
	.uleb128 0
	.uleb128 0
.LLSDACSE8220:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.size	_ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, .-_ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.section	.rodata._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE.str1.1,"aMS",@progbits,1
.LC69:
	.string	"runtime function"
	.section	.text.unlikely._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE,"ax",@progbits
	.align 2
.LCOLDB70:
	.section	.text._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE,"ax",@progbits
.LHOTB70:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.type	_ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE, @function
_ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE:
.LFB6598:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6598
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$360, %rsp
	movq	(%rdi), %r13
	movq	8(%rdi), %r12
	movq	%rdi, -376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	.LC69(%rip), %rax
	movq	%rax, -352(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, -360(%rbp)
	movq	%rax, -272(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L4986
	testq	%r13, %r13
	je	.L5186
.L4986:
	movq	%r12, -304(%rbp)
	cmpq	$15, %r12
	ja	.L5187
	cmpq	$1, %r12
	jne	.L4989
	movzbl	0(%r13), %eax
	movb	%al, -256(%rbp)
	movq	-360(%rbp), %rax
.L4990:
	movq	%r12, -264(%rbp)
	movb	$0, (%rax,%r12)
	movq	-272(%rbp), %rax
	cmpq	-360(%rbp), %rax
	je	.L5188
	movq	-360(%rbp), %rsi
	movq	-256(%rbp), %rcx
	pxor	%xmm0, %xmm0
	leaq	-168(%rbp), %r14
	movq	-264(%rbp), %rdx
	movq	%rax, -240(%rbp)
	movq	%rsi, -272(%rbp)
	leaq	-224(%rbp), %rsi
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	$0, -264(%rbp)
	movb	$0, -256(%rbp)
	movq	$0, -192(%rbp)
	movq	%r14, -184(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	%rsi, %rax
	je	.L4992
	movq	%rax, -184(%rbp)
	movq	%rcx, -168(%rbp)
.L4994:
	leaq	-208(%rbp), %rax
	movq	%rdx, -176(%rbp)
	movq	%rax, -392(%rbp)
.LEHB389:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	leaq	-304(%rbp), %rdi
	leaq	-208(%rbp), %rdx
	movq	%rdi, -400(%rbp)
	movq	(%rax), %rsi
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal6torque5Scope13LookupShallowERKNS1_13QualifiedNameE
.LEHE389:
	movq	-304(%rbp), %r12
	pxor	%xmm0, %xmm0
	movq	-296(%rbp), %r13
	movq	$0, -320(%rbp)
	movaps	%xmm0, -336(%rbp)
	cmpq	%r13, %r12
	je	.L4995
	leaq	-344(%rbp), %r15
	jmp	.L5000
	.p2align 4,,10
	.p2align 3
.L5190:
	movq	%rax, (%rsi)
	addq	$8, -328(%rbp)
.L4997:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L5189
.L5000:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L4997
	cmpl	$5, 8(%rax)
	jne	.L4997
	movq	%rax, -344(%rbp)
	movq	-328(%rbp), %rsi
	cmpq	-320(%rbp), %rsi
	jne	.L5190
	leaq	-336(%rbp), %rdi
	movq	%r15, %rdx
.LEHB390:
	call	_ZNSt6vectorIPN2v88internal6torque15RuntimeFunctionESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE390:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L5000
	.p2align 4,,10
	.p2align 3
.L5189:
	movq	-304(%rbp), %r13
.L4995:
	testq	%r13, %r13
	je	.L5005
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L5005:
	movq	-184(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L5006
	call	_ZdlPv@PLT
.L5006:
	movq	-200(%rbp), %r13
	movq	-208(%rbp), %r12
	cmpq	%r12, %r13
	je	.L5007
	.p2align 4,,10
	.p2align 3
.L5011:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5008
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L5011
.L5009:
	movq	-208(%rbp), %r12
.L5007:
	testq	%r12, %r12
	je	.L5012
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L5012:
	movq	-272(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L5013
	call	_ZdlPv@PLT
.L5013:
	movq	-336(%rbp), %rdi
	cmpq	%rdi, -328(%rbp)
	jne	.L5191
	testq	%rdi, %rdi
	je	.L5015
	call	_ZdlPv@PLT
.L5015:
	movl	$360, %edi
.LEHB391:
	call	_Znwm@PLT
.LEHE391:
	movq	(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movq	%rax, %r12
	movq	8(%rbx), %rax
	movaps	%xmm0, -208(%rbp)
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	je	.L5192
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L5193
	movq	%r13, %rdi
.LEHB392:
	call	_Znwm@PLT
.LEHE392:
	movq	%rax, %rcx
	movq	8(%rbx), %rax
	movq	(%rbx), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L5017:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	%rax, %rsi
	je	.L5023
	movq	%rcx, %rdi
	movq	%r14, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L5023:
	addq	%r14, %rcx
	cmpb	$0, 24(%rbx)
	movb	$0, -184(%rbp)
	movq	%rcx, -200(%rbp)
	movb	$0, -176(%rbp)
	jne	.L5194
.L5024:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rsi
	pxor	%xmm0, %xmm0
	movq	$0, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	je	.L5195
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L5196
	movq	%r13, %rdi
.LEHB393:
	call	_Znwm@PLT
.LEHE393:
	movq	%rax, %rcx
	movq	72(%rbx), %rax
	movq	64(%rbx), %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
.L5031:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, -128(%rbp)
	movaps	%xmm0, -144(%rbp)
	cmpq	%rsi, %rax
	je	.L5036
	movq	%rcx, %rdi
	movq	%r14, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L5036:
	movzbl	88(%rbx), %eax
	movq	112(%rbx), %r13
	addq	%r14, %rcx
	movq	$0, -96(%rbp)
	movq	%rcx, -136(%rbp)
	movb	%al, -120(%rbp)
	movq	96(%rbx), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	104(%rbx), %rax
	movq	$0, -80(%rbp)
	movq	%rax, -104(%rbp)
	movq	120(%rbx), %rax
	movq	%rax, %r14
	movq	%rax, -368(%rbp)
	subq	%r13, %r14
	movq	%r14, %rax
	sarq	$5, %rax
	je	.L5197
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L5198
	movq	%r14, %rdi
.LEHB394:
	call	_Znwm@PLT
.LEHE394:
	movq	%rax, -384(%rbp)
	movq	120(%rbx), %rax
	movq	112(%rbx), %r13
	movq	%rax, -368(%rbp)
.L5038:
	movq	-384(%rbp), %rax
	movq	%rax, %xmm0
	addq	%rax, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, -80(%rbp)
	movq	%rax, %r14
	movaps	%xmm0, -96(%rbp)
	cmpq	%r13, -368(%rbp)
	jne	.L5047
	jmp	.L5040
	.p2align 4,,10
	.p2align 3
.L5041:
	movabsq	$1152921504606846975, %rdx
	cmpq	%rdx, %rax
	ja	.L5199
	movq	%r15, %rdi
.LEHB395:
	call	_Znwm@PLT
.LEHE395:
	movq	%rax, %rcx
.L5042:
	movq	%rcx, %xmm0
	addq	%rcx, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 24(%r14)
	movups	%xmm0, 8(%r14)
	movq	16(%r13), %rax
	movq	8(%r13), %rsi
	movq	%rax, %r15
	subq	%rsi, %r15
	cmpq	%rsi, %rax
	je	.L5183
	movq	%rcx, %rdi
	movq	%r15, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L5183:
	addq	%r15, %rcx
	addq	$32, %r14
	addq	$32, %r13
	movq	%rcx, -16(%r14)
	cmpq	%r13, -368(%rbp)
	je	.L5040
.L5047:
	movq	0(%r13), %rax
	movq	%rax, (%r14)
	movq	16(%r13), %r15
	subq	8(%r13), %r15
	movq	$0, 8(%r14)
	movq	%r15, %rax
	movq	$0, 16(%r14)
	sarq	$3, %rax
	movq	$0, 24(%r14)
	jne	.L5041
	xorl	%ecx, %ecx
	jmp	.L5042
	.p2align 4,,10
	.p2align 3
.L4989:
	testq	%r12, %r12
	jne	.L5200
	movq	-360(%rbp), %rax
	jmp	.L4990
	.p2align 4,,10
	.p2align 3
.L5008:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L5011
	jmp	.L5009
	.p2align 4,,10
	.p2align 3
.L5040:
	movzbl	136(%rbx), %eax
	movq	%r14, -88(%rbp)
	leaq	-224(%rbp), %rbx
	movq	%rbx, -240(%rbp)
	movb	%al, -72(%rbp)
	movq	-376(%rbp), %rax
	movq	(%rax), %r14
	movq	8(%rax), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L5050
	testq	%r14, %r14
	je	.L5201
.L5050:
	movq	%r13, -304(%rbp)
	cmpq	$15, %r13
	ja	.L5202
	cmpq	$1, %r13
	jne	.L5065
	movzbl	(%r14), %eax
	movb	%al, -224(%rbp)
	movq	%rbx, %rax
.L5066:
	movq	%r13, -232(%rbp)
	movb	$0, (%rax,%r13)
	movq	-360(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	-376(%rbp), %rax
	movq	(%rax), %r14
	movq	8(%rax), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L5067
	testq	%r14, %r14
	je	.L5203
.L5067:
	movq	%r13, -304(%rbp)
	cmpq	$15, %r13
	ja	.L5204
	cmpq	$1, %r13
	jne	.L5070
	movzbl	(%r14), %eax
	movb	%al, -256(%rbp)
	movq	-360(%rbp), %rax
.L5071:
	movq	%r13, -264(%rbp)
	movb	$0, (%rax,%r13)
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
	movl	$5, 8(%r12)
.LEHB396:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE396:
	movq	(%rax), %rax
	pcmpeqd	%xmm0, %xmm0
	movb	$1, 64(%r12)
	movl	$-1, 60(%r12)
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm0, 44(%r12)
	movq	$1, 80(%r12)
	movl	%eax, 40(%r12)
	leaq	120(%r12), %rax
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN2v88internal6torque8CallableE(%rip), %rax
	movq	%rax, (%r12)
	leaq	144(%r12), %rax
	movq	%rax, 128(%r12)
	movq	-272(%rbp), %rax
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movl	$0x3f800000, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movups	%xmm1, 24(%r12)
	cmpq	-360(%rbp), %rax
	je	.L5205
	movq	%rax, 128(%r12)
	movq	-256(%rbp), %rax
	movq	%rax, 144(%r12)
.L5073:
	movq	-264(%rbp), %rax
	movb	$0, -256(%rbp)
	movq	$0, -264(%rbp)
	movq	%rax, 136(%r12)
	movq	-360(%rbp), %rax
	movq	%rax, -272(%rbp)
	leaq	176(%r12), %rax
	movq	%rax, 160(%r12)
	movq	-240(%rbp), %rax
	cmpq	%rbx, %rax
	je	.L5206
	movq	%rax, 160(%r12)
	movq	-224(%rbp), %rax
	movq	%rax, 176(%r12)
.L5075:
	movq	-232(%rbp), %rax
	movdqa	-208(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	216(%r12), %rdi
	leaq	-184(%rbp), %rsi
	movaps	%xmm0, -208(%rbp)
	movq	%rax, 168(%r12)
	movq	-192(%rbp), %rax
	movups	%xmm2, 192(%r12)
	movq	%rax, 208(%r12)
	movq	%rbx, -240(%rbp)
	movq	$0, -232(%rbp)
	movb	$0, -224(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v84base8internal15OptionalStorageINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEELb0ELb0EEC1EOS9_
	movq	-128(%rbp), %rax
	movdqu	-88(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movdqa	-144(%rbp), %xmm3
	movdqu	-104(%rbp), %xmm4
	movq	$0, -128(%rbp)
	movq	%rax, 272(%r12)
	movzbl	-120(%rbp), %eax
	movq	$0, -80(%rbp)
	movq	-272(%rbp), %rdi
	movb	%al, 280(%r12)
	movq	-112(%rbp), %rax
	movq	$0, 336(%r12)
	movq	%rax, 288(%r12)
	movzbl	-72(%rbp), %eax
	movb	$0, 344(%r12)
	movb	%al, 328(%r12)
	movb	$0, 352(%r12)
	movups	%xmm3, 256(%r12)
	movaps	%xmm0, -144(%rbp)
	movups	%xmm4, 296(%r12)
	movups	%xmm5, 312(%r12)
	movaps	%xmm0, -96(%rbp)
	cmpq	-360(%rbp), %rdi
	je	.L5076
	call	_ZdlPv@PLT
.L5076:
	movq	-240(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5077
	call	_ZdlPv@PLT
.L5077:
	movq	-88(%rbp), %rbx
	movq	-96(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L5078
	.p2align 4,,10
	.p2align 3
.L5082:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5079
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L5082
.L5080:
	movq	-96(%rbp), %r13
.L5078:
	testq	%r13, %r13
	je	.L5083
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L5083:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5084
	call	_ZdlPv@PLT
.L5084:
	cmpb	$0, -184(%rbp)
	je	.L5085
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5085
	call	_ZdlPv@PLT
.L5085:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5086
	call	_ZdlPv@PLT
.L5086:
	leaq	16+_ZTVN2v88internal6torque15RuntimeFunctionE(%rip), %rax
	leaq	-336(%rbp), %rdi
	movq	%r12, -336(%rbp)
	movq	%rax, (%r12)
.LEHB397:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_15RuntimeFunctionEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	movq	%rax, %r12
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	-376(%rbp), %rsi
	movq	(%rax), %rdi
	addq	$72, %rdi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
.LEHE397:
	movq	%r12, -304(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L5093
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L5094:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4985
	movq	(%rdi), %rax
	call	*8(%rax)
.L4985:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5207
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5079:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L5082
	jmp	.L5080
	.p2align 4,,10
	.p2align 3
.L5070:
	testq	%r13, %r13
	jne	.L5208
	movq	-360(%rbp), %rax
	jmp	.L5071
	.p2align 4,,10
	.p2align 3
.L5065:
	testq	%r13, %r13
	jne	.L5209
	movq	%rbx, %rax
	jmp	.L5066
	.p2align 4,,10
	.p2align 3
.L5187:
	leaq	-272(%rbp), %rdi
	leaq	-304(%rbp), %rsi
	xorl	%edx, %edx
.LEHB398:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE398:
	movq	%rax, -272(%rbp)
	movq	%rax, %rdi
	movq	-304(%rbp), %rax
	movq	%rax, -256(%rbp)
.L4988:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-304(%rbp), %r12
	movq	-272(%rbp), %rax
	jmp	.L4990
	.p2align 4,,10
	.p2align 3
.L5194:
	movq	32(%rbx), %r14
	movq	40(%rbx), %r13
	leaq	-160(%rbp), %r15
	movq	%r15, -176(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L5025
	testq	%r14, %r14
	je	.L5210
.L5025:
	movq	%r13, -304(%rbp)
	cmpq	$15, %r13
	ja	.L5211
	cmpq	$1, %r13
	jne	.L5028
	movzbl	(%r14), %eax
	movb	%al, -160(%rbp)
.L5029:
	movq	%r13, -168(%rbp)
	movb	$0, (%r15,%r13)
	movb	$1, -184(%rbp)
	jmp	.L5024
	.p2align 4,,10
	.p2align 3
.L5202:
	movq	-400(%rbp), %rsi
	leaq	-240(%rbp), %rdi
	xorl	%edx, %edx
.LEHB399:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE399:
	movq	%rax, -240(%rbp)
	movq	%rax, %rdi
	movq	-304(%rbp), %rax
	movq	%rax, -224(%rbp)
.L5064:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-304(%rbp), %r13
	movq	-240(%rbp), %rax
	jmp	.L5066
	.p2align 4,,10
	.p2align 3
.L5204:
	movq	-400(%rbp), %rsi
	leaq	-272(%rbp), %rdi
	xorl	%edx, %edx
.LEHB400:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE400:
	movq	%rax, -272(%rbp)
	movq	%rax, %rdi
	movq	-304(%rbp), %rax
	movq	%rax, -256(%rbp)
.L5069:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-304(%rbp), %r13
	movq	-272(%rbp), %rax
	jmp	.L5071
	.p2align 4,,10
	.p2align 3
.L5188:
	leaq	-168(%rbp), %r14
	pxor	%xmm0, %xmm0
	movdqa	-256(%rbp), %xmm6
	movq	-264(%rbp), %rdx
	movb	$0, -256(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -192(%rbp)
	movq	%r14, -184(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
.L4992:
	movdqa	-224(%rbp), %xmm7
	movups	%xmm7, -168(%rbp)
	jmp	.L4994
	.p2align 4,,10
	.p2align 3
.L5206:
	movdqa	-224(%rbp), %xmm7
	movups	%xmm7, 176(%r12)
	jmp	.L5075
	.p2align 4,,10
	.p2align 3
.L5205:
	movdqa	-256(%rbp), %xmm6
	movups	%xmm6, 144(%r12)
	jmp	.L5073
	.p2align 4,,10
	.p2align 3
.L5195:
	movq	%r13, %r14
	xorl	%ecx, %ecx
	jmp	.L5031
	.p2align 4,,10
	.p2align 3
.L5192:
	movq	%r13, %r14
	xorl	%ecx, %ecx
	jmp	.L5017
	.p2align 4,,10
	.p2align 3
.L5197:
	movq	$0, -384(%rbp)
	jmp	.L5038
	.p2align 4,,10
	.p2align 3
.L5028:
	testq	%r13, %r13
	je	.L5029
	movq	%r15, %rdi
	jmp	.L5027
	.p2align 4,,10
	.p2align 3
.L5093:
	movq	-400(%rbp), %rdx
	movq	%rax, %rdi
.LEHB401:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE401:
	jmp	.L5094
	.p2align 4,,10
	.p2align 3
.L5211:
	movq	-400(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	xorl	%edx, %edx
.LEHB402:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE402:
	movq	%rax, -176(%rbp)
	movq	%rax, %rdi
	movq	-304(%rbp), %rax
	movq	%rax, -160(%rbp)
.L5027:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-304(%rbp), %r13
	movq	-176(%rbp), %r15
	jmp	.L5029
.L5186:
	leaq	.LC16(%rip), %rdi
.LEHB403:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE403:
	.p2align 4,,10
	.p2align 3
.L5199:
.LEHB404:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE404:
.L5207:
	call	__stack_chk_fail@PLT
.L5191:
.LEHB405:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	-400(%rbp), %r8
	leaq	-352(%rbp), %rcx
	leaq	.LC27(%rip), %r9
	movq	-376(%rbp), %rsi
	leaq	.LC28(%rip), %rdx
	leaq	.LC29(%rip), %rdi
	movq	(%rax), %rax
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal6torque11ReportErrorIJRA18_KcRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERA8_S3_RPS3_RPNS1_5ScopeERA2_S3_EEEvDpOT_
.LEHE405:
.L5193:
.LEHB406:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE406:
.L5196:
.LEHB407:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE407:
.L5198:
.LEHB408:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE408:
.L5203:
	leaq	.LC16(%rip), %rdi
.LEHB409:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE409:
.L5201:
	leaq	.LC16(%rip), %rdi
.LEHB410:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE410:
.L5200:
	movq	-360(%rbp), %rdi
	jmp	.L4988
.L5208:
	movq	-360(%rbp), %rdi
	jmp	.L5069
.L5209:
	movq	%rbx, %rdi
	jmp	.L5064
.L5210:
	leaq	.LC16(%rip), %rdi
.LEHB411:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE411:
.L5111:
	endbr64
	movq	%rax, %r12
	jmp	.L5001
.L5104:
	endbr64
	movq	%rax, %r13
	jmp	.L5062
.L5116:
	endbr64
	movq	%rax, %rdi
	jmp	.L5051
.L5106:
	endbr64
	movq	%rax, %r13
	jmp	.L5092
.L5114:
	endbr64
	movq	%rax, %r13
	jmp	.L5032
.L5110:
	endbr64
	movq	%rax, %r12
	jmp	.L5020
.L5112:
	endbr64
	movq	%rax, %r13
	jmp	.L5059
.L5113:
	endbr64
	movq	%rax, %r13
	jmp	.L5057
.L5107:
	endbr64
	movq	%rax, %r13
	jmp	.L5090
.L5109:
	endbr64
	movq	%rax, %r12
	jmp	.L5018
.L5108:
	endbr64
	movq	%rax, %r13
	jmp	.L5088
.L5105:
	endbr64
	movq	%rax, %r12
	jmp	.L5096
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE,"a",@progbits
	.align 4
.LLSDA6598:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6598-.LLSDATTD6598
.LLSDATTD6598:
	.byte	0x1
	.uleb128 .LLSDACSE6598-.LLSDACSB6598
.LLSDACSB6598:
	.uleb128 .LEHB389-.LFB6598
	.uleb128 .LEHE389-.LEHB389
	.uleb128 .L5109-.LFB6598
	.uleb128 0
	.uleb128 .LEHB390-.LFB6598
	.uleb128 .LEHE390-.LEHB390
	.uleb128 .L5111-.LFB6598
	.uleb128 0
	.uleb128 .LEHB391-.LFB6598
	.uleb128 .LEHE391-.LEHB391
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB392-.LFB6598
	.uleb128 .LEHE392-.LEHB392
	.uleb128 .L5104-.LFB6598
	.uleb128 0
	.uleb128 .LEHB393-.LFB6598
	.uleb128 .LEHE393-.LEHB393
	.uleb128 .L5112-.LFB6598
	.uleb128 0
	.uleb128 .LEHB394-.LFB6598
	.uleb128 .LEHE394-.LEHB394
	.uleb128 .L5113-.LFB6598
	.uleb128 0
	.uleb128 .LEHB395-.LFB6598
	.uleb128 .LEHE395-.LEHB395
	.uleb128 .L5116-.LFB6598
	.uleb128 0x1
	.uleb128 .LEHB396-.LFB6598
	.uleb128 .LEHE396-.LEHB396
	.uleb128 .L5108-.LFB6598
	.uleb128 0
	.uleb128 .LEHB397-.LFB6598
	.uleb128 .LEHE397-.LEHB397
	.uleb128 .L5105-.LFB6598
	.uleb128 0
	.uleb128 .LEHB398-.LFB6598
	.uleb128 .LEHE398-.LEHB398
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB399-.LFB6598
	.uleb128 .LEHE399-.LEHB399
	.uleb128 .L5106-.LFB6598
	.uleb128 0
	.uleb128 .LEHB400-.LFB6598
	.uleb128 .LEHE400-.LEHB400
	.uleb128 .L5107-.LFB6598
	.uleb128 0
	.uleb128 .LEHB401-.LFB6598
	.uleb128 .LEHE401-.LEHB401
	.uleb128 .L5105-.LFB6598
	.uleb128 0
	.uleb128 .LEHB402-.LFB6598
	.uleb128 .LEHE402-.LEHB402
	.uleb128 .L5114-.LFB6598
	.uleb128 0
	.uleb128 .LEHB403-.LFB6598
	.uleb128 .LEHE403-.LEHB403
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB404-.LFB6598
	.uleb128 .LEHE404-.LEHB404
	.uleb128 .L5116-.LFB6598
	.uleb128 0x1
	.uleb128 .LEHB405-.LFB6598
	.uleb128 .LEHE405-.LEHB405
	.uleb128 .L5110-.LFB6598
	.uleb128 0
	.uleb128 .LEHB406-.LFB6598
	.uleb128 .LEHE406-.LEHB406
	.uleb128 .L5104-.LFB6598
	.uleb128 0
	.uleb128 .LEHB407-.LFB6598
	.uleb128 .LEHE407-.LEHB407
	.uleb128 .L5112-.LFB6598
	.uleb128 0
	.uleb128 .LEHB408-.LFB6598
	.uleb128 .LEHE408-.LEHB408
	.uleb128 .L5113-.LFB6598
	.uleb128 0
	.uleb128 .LEHB409-.LFB6598
	.uleb128 .LEHE409-.LEHB409
	.uleb128 .L5107-.LFB6598
	.uleb128 0
	.uleb128 .LEHB410-.LFB6598
	.uleb128 .LEHE410-.LEHB410
	.uleb128 .L5106-.LFB6598
	.uleb128 0
	.uleb128 .LEHB411-.LFB6598
	.uleb128 .LEHE411-.LEHB411
	.uleb128 .L5114-.LFB6598
	.uleb128 0
.LLSDACSE6598:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6598:
	.section	.text._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6598
	.type	_ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE.cold, @function
_ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE.cold:
.LFSB6598:
.L5001:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5002
	call	_ZdlPv@PLT
.L5002:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5018
	call	_ZdlPv@PLT
.L5018:
	movq	-392(%rbp), %rdi
	call	_ZN2v88internal6torque13QualifiedNameD1Ev
	movq	-272(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L5097
.L5185:
	call	_ZdlPv@PLT
.L5097:
	movq	%r12, %rdi
.LEHB412:
	call	_Unwind_Resume@PLT
.LEHE412:
.L5115:
	endbr64
	movq	%rax, %r13
	call	__cxa_end_catch@PLT
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5057
	call	_ZdlPv@PLT
.L5057:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5059
	call	_ZdlPv@PLT
.L5059:
	cmpb	$0, -184(%rbp)
	je	.L5034
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5034
.L5184:
	call	_ZdlPv@PLT
.L5034:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5062
	call	_ZdlPv@PLT
.L5062:
	movq	%r12, %rdi
	movl	$360, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB413:
	call	_Unwind_Resume@PLT
.LEHE413:
.L5051:
	call	__cxa_begin_catch@PLT
	movq	-384(%rbp), %rbx
.L5054:
	cmpq	%r14, %rbx
	jne	.L5212
.LEHB414:
	call	__cxa_rethrow@PLT
.LEHE414:
.L5088:
	movq	-272(%rbp), %rdi
	cmpq	-360(%rbp), %rdi
	je	.L5090
	call	_ZdlPv@PLT
.L5090:
	movq	-240(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5092
	call	_ZdlPv@PLT
.L5092:
	movq	-392(%rbp), %rdi
	call	_ZN2v88internal6torque9SignatureD1Ev
	jmp	.L5062
.L5032:
	cmpb	$0, -184(%rbp)
	je	.L5034
	movq	-176(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L5184
	jmp	.L5034
.L5020:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L5185
	jmp	.L5097
.L5212:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5053
	call	_ZdlPv@PLT
.L5053:
	addq	$32, %rbx
	jmp	.L5054
.L5096:
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5097
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L5097
	.cfi_endproc
.LFE6598:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.align 4
.LLSDAC6598:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6598-.LLSDATTDC6598
.LLSDATTDC6598:
	.byte	0x1
	.uleb128 .LLSDACSEC6598-.LLSDACSBC6598
.LLSDACSBC6598:
	.uleb128 .LEHB412-.LCOLDB70
	.uleb128 .LEHE412-.LEHB412
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB413-.LCOLDB70
	.uleb128 .LEHE413-.LEHB413
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB414-.LCOLDB70
	.uleb128 .LEHE414-.LEHB414
	.uleb128 .L5115-.LCOLDB70
	.uleb128 0
.LLSDACSEC6598:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6598:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.section	.text._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.size	_ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE, .-_ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
	.size	_ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE.cold, .-_ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE.cold
.LCOLDE70:
	.section	.text._ZN2v88internal6torque12Declarations22DeclareRuntimeFunctionERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS1_9SignatureE
.LHOTE70:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.type	_ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, @function
_ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE:
.LFB9702:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9702
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB415:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE415:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%r12, -32(%rbp)
	movq	120(%rax), %rsi
	cmpq	128(%rax), %rsi
	je	.L5214
	movq	$0, -32(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rax)
.L5215:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5213
	movq	(%rdi), %rax
	call	*8(%rax)
.L5213:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5228
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5214:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	112(%rax), %rdi
.LEHB416:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE416:
	jmp	.L5215
.L5228:
	call	__stack_chk_fail@PLT
.L5220:
	endbr64
	movq	%rax, %r12
.L5217:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5218
	movq	(%rdi), %rax
	call	*8(%rax)
.L5218:
	movq	%r12, %rdi
.LEHB417:
	call	_Unwind_Resume@PLT
.LEHE417:
	.cfi_endproc
.LFE9702:
	.section	.gcc_except_table._ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"aG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
.LLSDA9702:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE9702-.LLSDACSB9702
.LLSDACSB9702:
	.uleb128 .LEHB415-.LFB9702
	.uleb128 .LEHE415-.LEHB415
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB416-.LFB9702
	.uleb128 .LEHE416-.LEHB416
	.uleb128 .L5220-.LFB9702
	.uleb128 0
	.uleb128 .LEHB417-.LFB9702
	.uleb128 .LEHE417-.LEHB417
	.uleb128 0
	.uleb128 0
.LLSDACSE9702:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.size	_ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, .-_ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE,"ax",@progbits
	.align 2
.LCOLDB71:
	.section	.text._ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE,"ax",@progbits
.LHOTB71:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE
	.type	_ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE, @function
_ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE:
.LFB6603:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6603
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$168, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB418:
	call	_Znwm@PLT
.LEHE418:
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
	movl	$7, 8(%r12)
.LEHB419:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE419:
	movq	(%rax), %rax
	movq	(%r14), %r15
	pcmpeqd	%xmm0, %xmm0
	leaq	88(%r12), %rdi
	movq	8(%r14), %r13
	movb	$1, 64(%r12)
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movups	%xmm0, 44(%r12)
	movl	$-1, 60(%r12)
	movl	%eax, 40(%r12)
	leaq	16+_ZTVN2v88internal6torque7GenericE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r15, %rax
	addq	%r13, %rax
	movq	%rdi, 72(%r12)
	movups	%xmm1, 24(%r12)
	je	.L5230
	testq	%r15, %r15
	je	.L5263
.L5230:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L5264
	cmpq	$1, %r13
	jne	.L5233
	movzbl	(%r15), %eax
	movb	%al, 88(%r12)
.L5234:
	leaq	160(%r12), %rax
	movq	%r13, 80(%r12)
	movb	$0, (%rdi,%r13)
	movq	%rbx, 104(%r12)
	movq	%rax, 112(%r12)
	movq	$1, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movl	$0x3f800000, 144(%r12)
	movq	$0, 152(%r12)
	movq	$0, 160(%r12)
.LEHB420:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE420:
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	(%rax), %rbx
	movq	%r12, -64(%rbp)
.LEHB421:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_7GenericEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	leaq	72(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %r12
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
.LEHE421:
	movq	%r12, -72(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L5235
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L5236:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5229
	movq	(%rdi), %rax
	call	*8(%rax)
.L5229:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5265
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5233:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L5234
	jmp	.L5232
	.p2align 4,,10
	.p2align 3
.L5264:
	leaq	-64(%rbp), %rsi
	leaq	72(%r12), %rdi
	xorl	%edx, %edx
.LEHB422:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE422:
	movq	%rax, 72(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 88(%r12)
.L5232:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	72(%r12), %rdi
	jmp	.L5234
	.p2align 4,,10
	.p2align 3
.L5235:
	leaq	-72(%rbp), %rdx
	movq	%rax, %rdi
.LEHB423:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE423:
	jmp	.L5236
.L5263:
	leaq	.LC16(%rip), %rdi
.LEHB424:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE424:
.L5265:
	call	__stack_chk_fail@PLT
.L5245:
	endbr64
	movq	%rax, %r13
	jmp	.L5238
.L5244:
	endbr64
	movq	%rax, %r13
	jmp	.L5241
.L5246:
	endbr64
	movq	%rax, %r13
	jmp	.L5242
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE,"a",@progbits
.LLSDA6603:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6603-.LLSDACSB6603
.LLSDACSB6603:
	.uleb128 .LEHB418-.LFB6603
	.uleb128 .LEHE418-.LEHB418
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB419-.LFB6603
	.uleb128 .LEHE419-.LEHB419
	.uleb128 .L5244-.LFB6603
	.uleb128 0
	.uleb128 .LEHB420-.LFB6603
	.uleb128 .LEHE420-.LEHB420
	.uleb128 .L5246-.LFB6603
	.uleb128 0
	.uleb128 .LEHB421-.LFB6603
	.uleb128 .LEHE421-.LEHB421
	.uleb128 .L5245-.LFB6603
	.uleb128 0
	.uleb128 .LEHB422-.LFB6603
	.uleb128 .LEHE422-.LEHB422
	.uleb128 .L5244-.LFB6603
	.uleb128 0
	.uleb128 .LEHB423-.LFB6603
	.uleb128 .LEHE423-.LEHB423
	.uleb128 .L5245-.LFB6603
	.uleb128 0
	.uleb128 .LEHB424-.LFB6603
	.uleb128 .LEHE424-.LEHB424
	.uleb128 .L5244-.LFB6603
	.uleb128 0
.LLSDACSE6603:
	.section	.text._ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6603
	.type	_ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE.cold, @function
_ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE.cold:
.LFSB6603:
.L5238:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5240
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L5240
.L5241:
	movq	%r12, %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
.LEHB425:
	call	_Unwind_Resume@PLT
.L5242:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L5240:
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE425:
	.cfi_endproc
.LFE6603:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE
.LLSDAC6603:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6603-.LLSDACSBC6603
.LLSDACSBC6603:
	.uleb128 .LEHB425-.LCOLDB71
	.uleb128 .LEHE425-.LEHB425
	.uleb128 0
	.uleb128 0
.LLSDACSEC6603:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE
	.section	.text._ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE
	.size	_ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE, .-_ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE
	.size	_ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE.cold, .-_ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE.cold
.LCOLDE71:
	.section	.text._ZN2v88internal6torque12Declarations14DeclareGenericERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS1_18GenericDeclarationE
.LHOTE71:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.type	_ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, @function
_ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE:
.LFB9516:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9516
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB426:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE426:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%r12, -32(%rbp)
	movq	120(%rax), %rsi
	cmpq	128(%rax), %rsi
	je	.L5267
	movq	$0, -32(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rax)
.L5268:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5266
	movq	(%rdi), %rax
	call	*8(%rax)
.L5266:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5281
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5267:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	112(%rax), %rdi
.LEHB427:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE427:
	jmp	.L5268
.L5281:
	call	__stack_chk_fail@PLT
.L5273:
	endbr64
	movq	%rax, %r12
.L5270:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5271
	movq	(%rdi), %rax
	call	*8(%rax)
.L5271:
	movq	%r12, %rdi
.LEHB428:
	call	_Unwind_Resume@PLT
.LEHE428:
	.cfi_endproc
.LFE9516:
	.section	.gcc_except_table._ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"aG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
.LLSDA9516:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE9516-.LLSDACSB9516
.LLSDACSB9516:
	.uleb128 .LEHB426-.LFB9516
	.uleb128 .LEHE426-.LEHB426
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB427-.LFB9516
	.uleb128 .LEHE427-.LEHB427
	.uleb128 .L5273-.LFB9516
	.uleb128 0
	.uleb128 .LEHB428-.LFB9516
	.uleb128 .LEHE428-.LEHB428
	.uleb128 0
	.uleb128 0
.LLSDACSE9516:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.size	_ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, .-_ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB72:
	.section	.text._ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB72:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6571:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6571
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$160, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB429:
	call	_Znwm@PLT
.LEHE429:
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
	movl	$0, 8(%r12)
.LEHB430:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE430:
	movq	(%rax), %rax
	movq	(%r14), %r15
	pcmpeqd	%xmm0, %xmm0
	leaq	120(%r12), %rbx
	movq	8(%r14), %r13
	leaq	144(%r12), %rdi
	movl	$-1, 60(%r12)
	movdqu	(%rax), %xmm1
	movl	16(%rax), %eax
	movb	$1, 64(%r12)
	movq	%rbx, 72(%r12)
	movl	%eax, 40(%r12)
	leaq	16+_ZTVN2v88internal6torque9NamespaceE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r15, %rax
	addq	%r13, %rax
	movups	%xmm1, 24(%r12)
	movq	$1, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movl	$0x3f800000, 104(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	movq	%rdi, 128(%r12)
	movups	%xmm0, 44(%r12)
	je	.L5283
	testq	%r15, %r15
	je	.L5328
.L5283:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L5329
	cmpq	$1, %r13
	jne	.L5286
	movzbl	(%r15), %eax
	movb	%al, 144(%r12)
.L5287:
	movq	%r13, 136(%r12)
	movb	$0, (%rdi,%r13)
.LEHB431:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE431:
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	(%rax), %rbx
	movq	%r12, -64(%rbp)
.LEHB432:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_9NamespaceEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	leaq	72(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %r12
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
.LEHE432:
	movq	%r12, -72(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L5296
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L5297:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5282
	movq	(%rdi), %rax
	call	*8(%rax)
.L5282:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5330
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5286:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L5287
	jmp	.L5285
	.p2align 4,,10
	.p2align 3
.L5329:
	leaq	-64(%rbp), %rsi
	leaq	128(%r12), %rdi
	xorl	%edx, %edx
.LEHB433:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE433:
	movq	%rax, 128(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 144(%r12)
.L5285:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	128(%r12), %rdi
	jmp	.L5287
	.p2align 4,,10
	.p2align 3
.L5296:
	leaq	-72(%rbp), %rdx
	movq	%rax, %rdi
.LEHB434:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE434:
	jmp	.L5297
.L5328:
	leaq	.LC16(%rip), %rdi
.LEHB435:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE435:
.L5330:
	call	__stack_chk_fail@PLT
.L5305:
	endbr64
	movq	%rax, %r14
	jmp	.L5289
.L5306:
	endbr64
	movq	%rax, %r13
	jmp	.L5299
.L5304:
	endbr64
	movq	%rax, %r14
	jmp	.L5295
.L5307:
	endbr64
	movq	%rax, %r13
	jmp	.L5302
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6571:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6571-.LLSDACSB6571
.LLSDACSB6571:
	.uleb128 .LEHB429-.LFB6571
	.uleb128 .LEHE429-.LEHB429
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB430-.LFB6571
	.uleb128 .LEHE430-.LEHB430
	.uleb128 .L5304-.LFB6571
	.uleb128 0
	.uleb128 .LEHB431-.LFB6571
	.uleb128 .LEHE431-.LEHB431
	.uleb128 .L5307-.LFB6571
	.uleb128 0
	.uleb128 .LEHB432-.LFB6571
	.uleb128 .LEHE432-.LEHB432
	.uleb128 .L5306-.LFB6571
	.uleb128 0
	.uleb128 .LEHB433-.LFB6571
	.uleb128 .LEHE433-.LEHB433
	.uleb128 .L5305-.LFB6571
	.uleb128 0
	.uleb128 .LEHB434-.LFB6571
	.uleb128 .LEHE434-.LEHB434
	.uleb128 .L5306-.LFB6571
	.uleb128 0
	.uleb128 .LEHB435-.LFB6571
	.uleb128 .LEHE435-.LEHB435
	.uleb128 .L5305-.LFB6571
	.uleb128 0
.LLSDACSE6571:
	.section	.text._ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6571
	.type	_ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6571:
.L5289:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	16+_ZTVN2v88internal6torque5ScopeE(%rip), %rax
	movq	88(%r12), %r13
	movq	%rax, (%r12)
.L5293:
	testq	%r13, %r13
	jne	.L5331
	movq	80(%r12), %rax
	movq	72(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r12), %rdi
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	cmpq	%rdi, %rbx
	jne	.L5332
.L5295:
	movq	%r12, %rdi
	movl	$160, %esi
	call	_ZdlPvm@PLT
	movq	%r14, %rdi
.LEHB436:
	call	_Unwind_Resume@PLT
.L5299:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5301
	movq	(%rdi), %rax
	call	*8(%rax)
.L5301:
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE436:
.L5302:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L5301
.L5331:
	movq	40(%r13), %rdi
	movq	0(%r13), %r15
	testq	%rdi, %rdi
	je	.L5291
	call	_ZdlPv@PLT
.L5291:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L5292
	call	_ZdlPv@PLT
.L5292:
	movq	%r13, %rdi
	movq	%r15, %r13
	call	_ZdlPv@PLT
	jmp	.L5293
.L5332:
	call	_ZdlPv@PLT
	jmp	.L5295
	.cfi_endproc
.LFE6571:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6571:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6571-.LLSDACSBC6571
.LLSDACSBC6571:
	.uleb128 .LEHB436-.LCOLDB72
	.uleb128 .LEHE436-.LEHB436
	.uleb128 0
	.uleb128 0
.LLSDACSEC6571:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE72:
	.section	.text._ZN2v88internal6torque12Declarations16DeclareNamespaceERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE72:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.type	_ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, @function
_ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE:
.LFB9538:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9538
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
.LEHB437:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE437:
	movq	(%rbx), %r12
	movq	(%rax), %rax
	movq	$0, (%rbx)
	movq	%r12, -32(%rbp)
	movq	120(%rax), %rsi
	cmpq	128(%rax), %rsi
	je	.L5334
	movq	$0, -32(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rax)
.L5335:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5333
	movq	(%rdi), %rax
	call	*8(%rax)
.L5333:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5348
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5334:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	112(%rax), %rdi
.LEHB438:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE438:
	jmp	.L5335
.L5348:
	call	__stack_chk_fail@PLT
.L5340:
	endbr64
	movq	%rax, %r12
.L5337:
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5338
	movq	(%rdi), %rax
	call	*8(%rax)
.L5338:
	movq	%r12, %rdi
.LEHB439:
	call	_Unwind_Resume@PLT
.LEHE439:
	.cfi_endproc
.LFE9538:
	.section	.gcc_except_table._ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"aG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
.LLSDA9538:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE9538-.LLSDACSB9538
.LLSDACSB9538:
	.uleb128 .LEHB437-.LFB9538
	.uleb128 .LEHE437-.LEHB437
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB438-.LFB9538
	.uleb128 .LEHE438-.LEHB438
	.uleb128 .L5340-.LFB9538
	.uleb128 0
	.uleb128 .LEHB439-.LFB9538
	.uleb128 .LEHE439-.LEHB439
	.uleb128 0
	.uleb128 0
.LLSDACSE9538:
	.section	.text._ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,"axG",@progbits,_ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE,comdat
	.size	_ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE, .-_ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb,"ax",@progbits
	.align 2
.LCOLDB73:
	.section	.text._ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb,"ax",@progbits
.LHOTB73:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb
	.type	_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb, @function
_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb:
.LFB6575:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6575
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB440:
	call	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	movl	12(%r12), %eax
	movl	16(%r12), %ecx
	movl	$128, %edi
	movl	20(%r12), %edx
	movl	24(%r12), %esi
	movl	28(%r12), %r14d
	movl	%eax, -88(%rbp)
	movl	%ecx, -92(%rbp)
	movl	%edx, -96(%rbp)
	movl	%esi, -84(%rbp)
	call	_Znwm@PLT
.LEHE440:
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rdi
	movq	%rdi, (%rax)
	movq	%rax, %r12
	movl	$9, 8(%rax)
.LEHB441:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE441:
	movq	(%rax), %rax
	pcmpeqd	%xmm0, %xmm0
	movd	-96(%rbp), %xmm1
	leaq	16+_ZTVN2v88internal6torque9TypeAliasE(%rip), %rdi
	movd	-84(%rbp), %xmm3
	movd	-92(%rbp), %xmm4
	movups	%xmm0, 44(%r12)
	movdqu	(%rax), %xmm2
	movl	16(%rax), %eax
	movl	$-1, 60(%r12)
	movd	-88(%rbp), %xmm0
	punpckldq	%xmm3, %xmm1
	movq	%rdi, (%r12)
	movl	%eax, 40(%r12)
	movl	$1, %eax
	punpckldq	%xmm4, %xmm0
	movw	%ax, 64(%r12)
	movb	$1, 72(%r12)
	punpcklqdq	%xmm1, %xmm0
	movq	%rbx, 80(%r12)
	movb	$0, 88(%r12)
	movb	$0, 96(%r12)
	movb	%r15b, 104(%r12)
	movl	%r14d, 124(%r12)
	movups	%xmm2, 24(%r12)
	movups	%xmm0, 108(%r12)
.LEHB442:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE442:
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	(%rax), %rbx
	movq	%r12, -64(%rbp)
.LEHB443:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	leaq	72(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	%r12, -72(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L5350
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L5351:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5349
	movq	(%rdi), %rax
	call	*8(%rax)
.L5349:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5369
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5350:
	.cfi_restore_state
	leaq	-72(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE443:
	jmp	.L5351
.L5369:
	call	__stack_chk_fail@PLT
.L5360:
	endbr64
	movq	%rax, %r13
	jmp	.L5353
.L5361:
	endbr64
	movq	%rax, %r13
	jmp	.L5357
.L5359:
	endbr64
	movq	%rax, %r13
	jmp	.L5356
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb,"a",@progbits
.LLSDA6575:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6575-.LLSDACSB6575
.LLSDACSB6575:
	.uleb128 .LEHB440-.LFB6575
	.uleb128 .LEHE440-.LEHB440
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB441-.LFB6575
	.uleb128 .LEHE441-.LEHB441
	.uleb128 .L5359-.LFB6575
	.uleb128 0
	.uleb128 .LEHB442-.LFB6575
	.uleb128 .LEHE442-.LEHB442
	.uleb128 .L5361-.LFB6575
	.uleb128 0
	.uleb128 .LEHB443-.LFB6575
	.uleb128 .LEHE443-.LEHB443
	.uleb128 .L5360-.LFB6575
	.uleb128 0
.LLSDACSE6575:
	.section	.text._ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6575
	.type	_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb.cold, @function
_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb.cold:
.LFSB6575:
.L5353:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5355
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L5355
.L5357:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L5355:
	movq	%r13, %rdi
.LEHB444:
	call	_Unwind_Resume@PLT
.L5356:
	movq	%r12, %rdi
	movl	$128, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE444:
	.cfi_endproc
.LFE6575:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb
.LLSDAC6575:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6575-.LLSDACSBC6575
.LLSDACSBC6575:
	.uleb128 .LEHB444-.LCOLDB73
	.uleb128 .LEHE444-.LEHB444
	.uleb128 0
	.uleb128 0
.LLSDACSEC6575:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb
	.section	.text._ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb
	.size	_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb, .-_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb
	.section	.text.unlikely._ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb
	.size	_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb.cold, .-_ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb.cold
.LCOLDE73:
	.section	.text._ZN2v88internal6torque12Declarations19PredeclareTypeAliasEPKNS1_10IdentifierEPNS1_15TypeDeclarationEb
.LHOTE73:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE,"ax",@progbits
	.align 2
.LCOLDB74:
	.section	.text._ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE,"ax",@progbits
.LHOTB74:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE
	.type	_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE, @function
_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE:
.LFB6572:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6572
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	32(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB445:
	call	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_9TypeAliasEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
	movl	12(%r12), %eax
	movl	16(%r12), %ecx
	movl	$128, %edi
	movl	20(%r12), %edx
	movl	24(%r12), %r15d
	movl	28(%r12), %r14d
	movl	%eax, -84(%rbp)
	movl	%ecx, -88(%rbp)
	movl	%edx, -92(%rbp)
	call	_Znwm@PLT
.LEHE445:
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rsi
	movq	%rsi, (%rax)
	movq	%rax, %r12
	movl	$9, 8(%rax)
.LEHB446:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE446:
	movq	(%rax), %rax
	pcmpeqd	%xmm0, %xmm0
	movd	-92(%rbp), %xmm1
	leaq	16+_ZTVN2v88internal6torque9TypeAliasE(%rip), %rsi
	movd	-88(%rbp), %xmm4
	movups	%xmm0, 44(%r12)
	movd	-84(%rbp), %xmm0
	movd	%r15d, %xmm3
	movdqu	(%rax), %xmm2
	movl	16(%rax), %eax
	punpckldq	%xmm3, %xmm1
	movq	%rsi, (%r12)
	punpckldq	%xmm4, %xmm0
	movb	$0, 72(%r12)
	movl	%eax, 40(%r12)
	movl	$1, %eax
	punpcklqdq	%xmm1, %xmm0
	movl	$-1, 60(%r12)
	movw	%ax, 64(%r12)
	movb	$0, 80(%r12)
	movb	$1, 88(%r12)
	movq	%rbx, 96(%r12)
	movb	$1, 104(%r12)
	movl	%r14d, 124(%r12)
	movups	%xmm2, 24(%r12)
	movups	%xmm0, 108(%r12)
.LEHB447:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE447:
	movq	(%rax), %rax
	leaq	-64(%rbp), %rdi
	movq	(%rax), %rbx
	movq	%r12, -64(%rbp)
.LEHB448:
	call	_ZN2v88internal6torque18RegisterDeclarableINS1_9TypeAliasEEEPT_St10unique_ptrIS4_St14default_deleteIS4_EE
	leaq	72(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	%r12, -72(%rbp)
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L5371
	movq	%r12, (%rsi)
	addq	$8, 8(%rax)
.L5372:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5370
	movq	(%rdi), %rax
	call	*8(%rax)
.L5370:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5390
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5371:
	.cfi_restore_state
	leaq	-72(%rbp), %rdx
	movq	%rax, %rdi
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE448:
	jmp	.L5372
.L5390:
	call	__stack_chk_fail@PLT
.L5381:
	endbr64
	movq	%rax, %r13
	jmp	.L5374
.L5382:
	endbr64
	movq	%rax, %r13
	jmp	.L5378
.L5380:
	endbr64
	movq	%rax, %r13
	jmp	.L5377
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE,"a",@progbits
.LLSDA6572:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6572-.LLSDACSB6572
.LLSDACSB6572:
	.uleb128 .LEHB445-.LFB6572
	.uleb128 .LEHE445-.LEHB445
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB446-.LFB6572
	.uleb128 .LEHE446-.LEHB446
	.uleb128 .L5380-.LFB6572
	.uleb128 0
	.uleb128 .LEHB447-.LFB6572
	.uleb128 .LEHE447-.LEHB447
	.uleb128 .L5382-.LFB6572
	.uleb128 0
	.uleb128 .LEHB448-.LFB6572
	.uleb128 .LEHE448-.LEHB448
	.uleb128 .L5381-.LFB6572
	.uleb128 0
.LLSDACSE6572:
	.section	.text._ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6572
	.type	_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE.cold, @function
_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE.cold:
.LFSB6572:
.L5374:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5376
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L5376
.L5378:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L5376:
	movq	%r13, %rdi
.LEHB449:
	call	_Unwind_Resume@PLT
.L5377:
	movq	%r12, %rdi
	movl	$128, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE449:
	.cfi_endproc
.LFE6572:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE
.LLSDAC6572:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6572-.LLSDACSBC6572
.LLSDACSBC6572:
	.uleb128 .LEHB449-.LCOLDB74
	.uleb128 .LEHE449-.LEHB449
	.uleb128 0
	.uleb128 0
.LLSDACSEC6572:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE
	.section	.text._ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE
	.size	_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE, .-_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE
	.size	_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE.cold, .-_ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE.cold
.LCOLDE74:
	.section	.text._ZN2v88internal6torque12Declarations11DeclareTypeEPKNS1_10IdentifierEPKNS1_4TypeE
.LHOTE74:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB75:
	.section	.text._ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB75:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6601:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6601
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	32(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-144(%rbp), %rbx
	subq	$152, %rsp
	movq	%rsi, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB450:
	call	_ZN2v88internal6torque12_GLOBAL__N_120CheckAlreadyDeclaredINS1_5ValueEEEvRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKc.constprop.0
.LEHE450:
	movq	(%r12), %r14
	movq	8(%r12), %r12
	movq	%rbx, -160(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L5392
	testq	%r14, %r14
	je	.L5481
.L5392:
	movq	%r12, -168(%rbp)
	cmpq	$15, %r12
	ja	.L5482
	cmpq	$1, %r12
	jne	.L5395
	movzbl	(%r14), %eax
	movb	%al, -144(%rbp)
	movq	%rbx, %rax
.L5396:
	movq	%r12, -152(%rbp)
	movl	$168, %edi
	movb	$0, (%rax,%r12)
.LEHB451:
	call	_Znwm@PLT
.LEHE451:
	movq	%rax, %r12
	leaq	16+_ZTVN2v88internal6torque10DeclarableE(%rip), %rax
	movq	%rax, (%r12)
	movl	$10, 8(%r12)
.LEHB452:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 16(%r12)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE452:
	movq	(%rax), %rax
	movq	-160(%rbp), %r14
	pcmpeqd	%xmm0, %xmm0
	movq	%r13, %xmm3
	movq	-184(%rbp), %xmm1
	movq	-152(%rbp), %r13
	movups	%xmm0, 44(%r12)
	movdqu	(%rax), %xmm2
	movl	16(%rax), %eax
	movb	$1, 64(%r12)
	movdqa	%xmm1, %xmm0
	movb	$0, 88(%r12)
	movl	%eax, 40(%r12)
	leaq	16+_ZTVN2v88internal6torque14ExternConstantE(%rip), %rax
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, (%r12)
	leaq	-96(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%rax, -112(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	movb	$0, 96(%r12)
	movl	$-1, 60(%r12)
	movb	$1, -120(%rbp)
	movups	%xmm2, 24(%r12)
	movups	%xmm0, 72(%r12)
	movq	%xmm1, -128(%rbp)
	je	.L5397
	testq	%r14, %r14
	je	.L5483
.L5397:
	movq	%r13, -168(%rbp)
	cmpq	$15, %r13
	ja	.L5484
	cmpq	$1, %r13
	jne	.L5400
	movzbl	(%r14), %eax
	movb	%al, -96(%rbp)
	movq	-184(%rbp), %rax
.L5401:
	movq	%r13, -104(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %rax
	cmpb	$0, 88(%r12)
	movb	$0, -80(%rbp)
	movb	$0, -72(%rbp)
	movq	%rax, 96(%r12)
	je	.L5402
	cmpb	$0, -120(%rbp)
	movzbl	104(%r12), %eax
	jne	.L5485
	testb	%al, %al
	je	.L5406
	movq	112(%r12), %rdi
	leaq	128(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5486
	call	_ZdlPv@PLT
	movb	$0, 104(%r12)
	movzbl	-80(%rbp), %ecx
	movzbl	-120(%rbp), %eax
.L5405:
	movzbl	144(%r12), %edx
	testb	%cl, %cl
	je	.L5410
	testb	%dl, %dl
	je	.L5411
	movdqu	-72(%rbp), %xmm6
	movups	%xmm6, 152(%r12)
.L5412:
	testb	%al, %al
	je	.L5415
	movq	-112(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L5415
	call	_ZdlPv@PLT
	jmp	.L5415
	.p2align 4,,10
	.p2align 3
.L5395:
	testq	%r12, %r12
	jne	.L5487
	movq	%rbx, %rax
	jmp	.L5396
	.p2align 4,,10
	.p2align 3
.L5402:
	cmpb	$0, -120(%rbp)
	movb	$0, 104(%r12)
	movb	$0, 112(%r12)
	jne	.L5414
	movq	-64(%rbp), %rax
	movdqa	-80(%rbp), %xmm4
	movb	$1, 88(%r12)
	movq	%rax, 160(%r12)
	movups	%xmm4, 144(%r12)
.L5415:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5420
	call	_ZdlPv@PLT
.L5420:
.LEHB453:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE453:
	movq	(%rax), %rax
	movq	(%rax), %rbx
.LEHB454:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE454:
	movq	(%rax), %rdi
	movq	%r12, -168(%rbp)
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	je	.L5424
	movq	$0, -168(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 120(%rdi)
.L5425:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5426
	movq	(%rdi), %rax
	call	*8(%rax)
.L5426:
	leaq	72(%rbx), %rdi
	movq	%r15, %rsi
	movq	%r12, -168(%rbp)
.LEHB455:
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St6vectorIPN2v88internal6torque10DeclarableESaISE_EEESaISH_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
.LEHE455:
	movq	8(%rax), %rsi
	cmpq	16(%rax), %rsi
	je	.L5488
	movq	-168(%rbp), %rdx
	movq	%rdx, (%rsi)
	addq	$8, 8(%rax)
.L5391:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5489
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5400:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L5490
	movq	-184(%rbp), %rax
	jmp	.L5401
	.p2align 4,,10
	.p2align 3
.L5482:
	leaq	-160(%rbp), %rdi
	leaq	-168(%rbp), %rsi
	xorl	%edx, %edx
.LEHB456:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE456:
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, -144(%rbp)
.L5394:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-168(%rbp), %r12
	movq	-160(%rbp), %rax
	jmp	.L5396
	.p2align 4,,10
	.p2align 3
.L5484:
	leaq	-168(%rbp), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%edx, %edx
.LEHB457:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE457:
	movq	%rax, -112(%rbp)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, -96(%rbp)
.L5399:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-168(%rbp), %r13
	movq	-112(%rbp), %rax
	jmp	.L5401
	.p2align 4,,10
	.p2align 3
.L5486:
	cmpb	$0, 144(%r12)
	movb	$0, 104(%r12)
	je	.L5415
.L5408:
	movb	$0, 144(%r12)
	jmp	.L5415
	.p2align 4,,10
	.p2align 3
.L5424:
	leaq	-168(%rbp), %rdx
	addq	$112, %rdi
.LEHB458:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque10DeclarableESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE458:
	jmp	.L5425
	.p2align 4,,10
	.p2align 3
.L5488:
	leaq	-168(%rbp), %rdx
	movq	%rax, %rdi
.LEHB459:
	call	_ZNSt6vectorIPN2v88internal6torque10DeclarableESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
.LEHE459:
	jmp	.L5391
	.p2align 4,,10
	.p2align 3
.L5414:
	leaq	128(%r12), %r13
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	leaq	112(%r12), %rdi
	movq	%r13, 112(%r12)
	addq	%rsi, %rdx
.LEHB460:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE460:
	movdqa	-80(%rbp), %xmm5
	movq	-64(%rbp), %rdx
	movb	$1, 104(%r12)
	movb	$1, 88(%r12)
	movzbl	-120(%rbp), %eax
	movq	%rdx, 160(%r12)
	movups	%xmm5, 144(%r12)
	jmp	.L5412
	.p2align 4,,10
	.p2align 3
.L5485:
	leaq	112(%r12), %rdi
	testb	%al, %al
	je	.L5404
	leaq	-112(%rbp), %rsi
.LEHB461:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.L5479:
	movzbl	-80(%rbp), %ecx
	movzbl	-120(%rbp), %eax
	jmp	.L5405
	.p2align 4,,10
	.p2align 3
.L5411:
	movb	$1, 144(%r12)
	movdqu	-72(%rbp), %xmm7
	movups	%xmm7, 152(%r12)
	jmp	.L5412
	.p2align 4,,10
	.p2align 3
.L5406:
	cmpb	$0, 144(%r12)
	jne	.L5408
	jmp	.L5415
	.p2align 4,,10
	.p2align 3
.L5404:
	leaq	128(%r12), %rax
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, 112(%r12)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag.constprop.0
.LEHE461:
	movb	$1, 104(%r12)
	jmp	.L5479
.L5481:
	leaq	.LC16(%rip), %rdi
.LEHB462:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE462:
	.p2align 4,,10
	.p2align 3
.L5410:
	testb	%dl, %dl
	je	.L5412
	movb	$0, 144(%r12)
	jmp	.L5412
.L5489:
	call	__stack_chk_fail@PLT
.L5483:
	leaq	.LC16(%rip), %rdi
.LEHB463:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE463:
.L5490:
	movq	-184(%rbp), %rdi
	jmp	.L5399
.L5487:
	movq	%rbx, %rdi
	jmp	.L5394
.L5443:
	endbr64
	movq	%rax, %r13
	jmp	.L5437
.L5444:
	endbr64
	movq	%rax, %r13
	jmp	.L5423
.L5445:
	endbr64
	movq	%rax, %r13
	jmp	.L5422
.L5452:
	endbr64
.L5480:
	movq	%rax, %rbx
	jmp	.L5436
.L5450:
	endbr64
	movq	%rax, %rbx
	jmp	.L5439
.L5451:
	endbr64
	movq	%rax, %rbx
	jmp	.L5439
.L5446:
	endbr64
	movq	%rax, %r13
	jmp	.L5418
.L5447:
	endbr64
	movq	%rax, %r14
	jmp	.L5416
.L5448:
	endbr64
	jmp	.L5480
.L5449:
	endbr64
	movq	%rax, %rbx
	jmp	.L5430
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA6601:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6601-.LLSDACSB6601
.LLSDACSB6601:
	.uleb128 .LEHB450-.LFB6601
	.uleb128 .LEHE450-.LEHB450
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB451-.LFB6601
	.uleb128 .LEHE451-.LEHB451
	.uleb128 .L5443-.LFB6601
	.uleb128 0
	.uleb128 .LEHB452-.LFB6601
	.uleb128 .LEHE452-.LEHB452
	.uleb128 .L5444-.LFB6601
	.uleb128 0
	.uleb128 .LEHB453-.LFB6601
	.uleb128 .LEHE453-.LEHB453
	.uleb128 .L5450-.LFB6601
	.uleb128 0
	.uleb128 .LEHB454-.LFB6601
	.uleb128 .LEHE454-.LEHB454
	.uleb128 .L5451-.LFB6601
	.uleb128 0
	.uleb128 .LEHB455-.LFB6601
	.uleb128 .LEHE455-.LEHB455
	.uleb128 .L5452-.LFB6601
	.uleb128 0
	.uleb128 .LEHB456-.LFB6601
	.uleb128 .LEHE456-.LEHB456
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB457-.LFB6601
	.uleb128 .LEHE457-.LEHB457
	.uleb128 .L5445-.LFB6601
	.uleb128 0
	.uleb128 .LEHB458-.LFB6601
	.uleb128 .LEHE458-.LEHB458
	.uleb128 .L5449-.LFB6601
	.uleb128 0
	.uleb128 .LEHB459-.LFB6601
	.uleb128 .LEHE459-.LEHB459
	.uleb128 .L5448-.LFB6601
	.uleb128 0
	.uleb128 .LEHB460-.LFB6601
	.uleb128 .LEHE460-.LEHB460
	.uleb128 .L5447-.LFB6601
	.uleb128 0
	.uleb128 .LEHB461-.LFB6601
	.uleb128 .LEHE461-.LEHB461
	.uleb128 .L5446-.LFB6601
	.uleb128 0
	.uleb128 .LEHB462-.LFB6601
	.uleb128 .LEHE462-.LEHB462
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB463-.LFB6601
	.uleb128 .LEHE463-.LEHB463
	.uleb128 .L5445-.LFB6601
	.uleb128 0
.LLSDACSE6601:
	.section	.text._ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6601
	.type	_ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB6601:
.L5416:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpb	$0, 104(%r12)
	je	.L5417
	movq	112(%r12), %rdi
	cmpq	%rdi, %r13
	je	.L5417
	call	_ZdlPv@PLT
.L5417:
	movq	%r14, %r13
.L5418:
	cmpb	$0, -120(%rbp)
	je	.L5422
	movq	-112(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L5422
	call	_ZdlPv@PLT
.L5422:
	leaq	16+_ZTVN2v88internal6torque5ValueE(%rip), %rax
	leaq	88(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11VisitResultELb0EED2Ev
.L5423:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5437:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5438
	call	_ZdlPv@PLT
.L5438:
	movq	%r13, %rdi
.LEHB464:
	call	_Unwind_Resume@PLT
.L5430:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5436
	movq	(%rdi), %rax
	call	*8(%rax)
.L5436:
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE464:
.L5439:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L5436
	.cfi_endproc
.LFE6601:
	.section	.gcc_except_table._ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC6601:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC6601-.LLSDACSBC6601
.LLSDACSBC6601:
	.uleb128 .LEHB464-.LCOLDB75
	.uleb128 .LEHE464-.LEHB464
	.uleb128 0
	.uleb128 0
.LLSDACSEC6601:
	.section	.text.unlikely._ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE75:
	.section	.text._ZN2v88internal6torque12Declarations21DeclareExternConstantEPNS1_10IdentifierEPKNS1_4TypeENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE75:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_GLOBAL__sub_I__ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB13853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13853:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_GLOBAL__sub_I__ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque12Declarations17LookupGlobalScopeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.weak	_ZTVN2v88internal6torque10DeclarableE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque10DeclarableE,"awG",@progbits,_ZTVN2v88internal6torque10DeclarableE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque10DeclarableE, @object
	.size	_ZTVN2v88internal6torque10DeclarableE, 40
_ZTVN2v88internal6torque10DeclarableE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque10DeclarableD1Ev
	.quad	_ZN2v88internal6torque10DeclarableD0Ev
	.quad	_ZNK2v88internal6torque10Declarable9type_nameEv
	.weak	_ZTVN2v88internal6torque5ScopeE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque5ScopeE,"awG",@progbits,_ZTVN2v88internal6torque5ScopeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque5ScopeE, @object
	.size	_ZTVN2v88internal6torque5ScopeE, 40
_ZTVN2v88internal6torque5ScopeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque5ScopeD1Ev
	.quad	_ZN2v88internal6torque5ScopeD0Ev
	.quad	_ZNK2v88internal6torque5Scope9type_nameEv
	.weak	_ZTVN2v88internal6torque9NamespaceE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque9NamespaceE,"awG",@progbits,_ZTVN2v88internal6torque9NamespaceE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque9NamespaceE, @object
	.size	_ZTVN2v88internal6torque9NamespaceE, 40
_ZTVN2v88internal6torque9NamespaceE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque9NamespaceD1Ev
	.quad	_ZN2v88internal6torque9NamespaceD0Ev
	.quad	_ZNK2v88internal6torque9Namespace9type_nameEv
	.weak	_ZTVN2v88internal6torque5ValueE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque5ValueE,"awG",@progbits,_ZTVN2v88internal6torque5ValueE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque5ValueE, @object
	.size	_ZTVN2v88internal6torque5ValueE, 48
_ZTVN2v88internal6torque5ValueE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque5ValueD1Ev
	.quad	_ZN2v88internal6torque5ValueD0Ev
	.quad	_ZNK2v88internal6torque5Value9type_nameEv
	.quad	_ZNK2v88internal6torque5Value7IsConstEv
	.weak	_ZTVN2v88internal6torque17NamespaceConstantE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque17NamespaceConstantE,"awG",@progbits,_ZTVN2v88internal6torque17NamespaceConstantE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque17NamespaceConstantE, @object
	.size	_ZTVN2v88internal6torque17NamespaceConstantE, 48
_ZTVN2v88internal6torque17NamespaceConstantE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque17NamespaceConstantD1Ev
	.quad	_ZN2v88internal6torque17NamespaceConstantD0Ev
	.quad	_ZNK2v88internal6torque17NamespaceConstant9type_nameEv
	.quad	_ZNK2v88internal6torque5Value7IsConstEv
	.weak	_ZTVN2v88internal6torque14ExternConstantE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque14ExternConstantE,"awG",@progbits,_ZTVN2v88internal6torque14ExternConstantE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque14ExternConstantE, @object
	.size	_ZTVN2v88internal6torque14ExternConstantE, 48
_ZTVN2v88internal6torque14ExternConstantE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque14ExternConstantD1Ev
	.quad	_ZN2v88internal6torque14ExternConstantD0Ev
	.quad	_ZNK2v88internal6torque14ExternConstant9type_nameEv
	.quad	_ZNK2v88internal6torque5Value7IsConstEv
	.weak	_ZTVN2v88internal6torque8CallableE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque8CallableE,"awG",@progbits,_ZTVN2v88internal6torque8CallableE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque8CallableE, @object
	.size	_ZTVN2v88internal6torque8CallableE, 56
_ZTVN2v88internal6torque8CallableE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque8CallableD1Ev
	.quad	_ZN2v88internal6torque8CallableD0Ev
	.quad	_ZNK2v88internal6torque8Callable9type_nameEv
	.quad	_ZNK2v88internal6torque8Callable15ShouldBeInlinedEv
	.quad	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv
	.weak	_ZTVN2v88internal6torque5MacroE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque5MacroE,"awG",@progbits,_ZTVN2v88internal6torque5MacroE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque5MacroE, @object
	.size	_ZTVN2v88internal6torque5MacroE, 56
_ZTVN2v88internal6torque5MacroE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque5MacroD1Ev
	.quad	_ZN2v88internal6torque5MacroD0Ev
	.quad	_ZNK2v88internal6torque5Macro9type_nameEv
	.quad	_ZNK2v88internal6torque5Macro15ShouldBeInlinedEv
	.quad	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv
	.weak	_ZTVN2v88internal6torque11ExternMacroE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque11ExternMacroE,"awG",@progbits,_ZTVN2v88internal6torque11ExternMacroE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque11ExternMacroE, @object
	.size	_ZTVN2v88internal6torque11ExternMacroE, 56
_ZTVN2v88internal6torque11ExternMacroE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque11ExternMacroD1Ev
	.quad	_ZN2v88internal6torque11ExternMacroD0Ev
	.quad	_ZNK2v88internal6torque11ExternMacro9type_nameEv
	.quad	_ZNK2v88internal6torque5Macro15ShouldBeInlinedEv
	.quad	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv
	.weak	_ZTVN2v88internal6torque11TorqueMacroE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque11TorqueMacroE,"awG",@progbits,_ZTVN2v88internal6torque11TorqueMacroE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque11TorqueMacroE, @object
	.size	_ZTVN2v88internal6torque11TorqueMacroE, 56
_ZTVN2v88internal6torque11TorqueMacroE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque11TorqueMacroD1Ev
	.quad	_ZN2v88internal6torque11TorqueMacroD0Ev
	.quad	_ZNK2v88internal6torque11TorqueMacro9type_nameEv
	.quad	_ZNK2v88internal6torque5Macro15ShouldBeInlinedEv
	.quad	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv
	.weak	_ZTVN2v88internal6torque6MethodE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque6MethodE,"awG",@progbits,_ZTVN2v88internal6torque6MethodE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque6MethodE, @object
	.size	_ZTVN2v88internal6torque6MethodE, 56
_ZTVN2v88internal6torque6MethodE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque6MethodD1Ev
	.quad	_ZN2v88internal6torque6MethodD0Ev
	.quad	_ZNK2v88internal6torque6Method9type_nameEv
	.quad	_ZNK2v88internal6torque6Method15ShouldBeInlinedEv
	.quad	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv
	.weak	_ZTVN2v88internal6torque7BuiltinE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque7BuiltinE,"awG",@progbits,_ZTVN2v88internal6torque7BuiltinE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque7BuiltinE, @object
	.size	_ZTVN2v88internal6torque7BuiltinE, 56
_ZTVN2v88internal6torque7BuiltinE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque7BuiltinD1Ev
	.quad	_ZN2v88internal6torque7BuiltinD0Ev
	.quad	_ZNK2v88internal6torque7Builtin9type_nameEv
	.quad	_ZNK2v88internal6torque8Callable15ShouldBeInlinedEv
	.quad	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv
	.weak	_ZTVN2v88internal6torque15RuntimeFunctionE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque15RuntimeFunctionE,"awG",@progbits,_ZTVN2v88internal6torque15RuntimeFunctionE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque15RuntimeFunctionE, @object
	.size	_ZTVN2v88internal6torque15RuntimeFunctionE, 56
_ZTVN2v88internal6torque15RuntimeFunctionE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque15RuntimeFunctionD1Ev
	.quad	_ZN2v88internal6torque15RuntimeFunctionD0Ev
	.quad	_ZNK2v88internal6torque15RuntimeFunction9type_nameEv
	.quad	_ZNK2v88internal6torque8Callable15ShouldBeInlinedEv
	.quad	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv
	.weak	_ZTVN2v88internal6torque9IntrinsicE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque9IntrinsicE,"awG",@progbits,_ZTVN2v88internal6torque9IntrinsicE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque9IntrinsicE, @object
	.size	_ZTVN2v88internal6torque9IntrinsicE, 56
_ZTVN2v88internal6torque9IntrinsicE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque9IntrinsicD1Ev
	.quad	_ZN2v88internal6torque9IntrinsicD0Ev
	.quad	_ZNK2v88internal6torque9Intrinsic9type_nameEv
	.quad	_ZNK2v88internal6torque8Callable15ShouldBeInlinedEv
	.quad	_ZNK2v88internal6torque8Callable26ShouldGenerateExternalCodeEv
	.weak	_ZTVN2v88internal6torque7GenericE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque7GenericE,"awG",@progbits,_ZTVN2v88internal6torque7GenericE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque7GenericE, @object
	.size	_ZTVN2v88internal6torque7GenericE, 40
_ZTVN2v88internal6torque7GenericE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque7GenericD1Ev
	.quad	_ZN2v88internal6torque7GenericD0Ev
	.quad	_ZNK2v88internal6torque7Generic9type_nameEv
	.weak	_ZTVN2v88internal6torque17GenericStructTypeE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque17GenericStructTypeE,"awG",@progbits,_ZTVN2v88internal6torque17GenericStructTypeE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque17GenericStructTypeE, @object
	.size	_ZTVN2v88internal6torque17GenericStructTypeE, 40
_ZTVN2v88internal6torque17GenericStructTypeE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque17GenericStructTypeD1Ev
	.quad	_ZN2v88internal6torque17GenericStructTypeD0Ev
	.quad	_ZNK2v88internal6torque17GenericStructType9type_nameEv
	.weak	_ZTVN2v88internal6torque9TypeAliasE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque9TypeAliasE,"awG",@progbits,_ZTVN2v88internal6torque9TypeAliasE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque9TypeAliasE, @object
	.size	_ZTVN2v88internal6torque9TypeAliasE, 40
_ZTVN2v88internal6torque9TypeAliasE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque9TypeAliasD1Ev
	.quad	_ZN2v88internal6torque9TypeAliasD0Ev
	.quad	_ZNK2v88internal6torque9TypeAlias9type_nameEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
