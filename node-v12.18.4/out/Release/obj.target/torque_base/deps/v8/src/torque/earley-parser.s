	.file	"earley-parser.cc"
	.text
	.section	.text._ZN2v88internal6torque13DefaultActionEPNS1_19ParseResultIteratorE,"axG",@progbits,_ZN2v88internal6torque13DefaultActionEPNS1_19ParseResultIteratorE,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque13DefaultActionEPNS1_19ParseResultIteratorE
	.type	_ZN2v88internal6torque13DefaultActionEPNS1_19ParseResultIteratorE, @function
_ZN2v88internal6torque13DefaultActionEPNS1_19ParseResultIteratorE:
.LFB5593:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rdx
	movq	%rdi, %rax
	movq	(%rsi), %rdi
	movq	24(%rsi), %rcx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rcx
	jb	.L2
	movb	$0, (%rax)
	movb	$0, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	1(%rcx), %rdx
	movq	%rdx, 24(%rsi)
	leaq	(%rdi,%rcx,8), %rdx
	movq	(%rdx), %rcx
	movq	$0, (%rdx)
	movb	$1, (%rax)
	movq	%rcx, 8(%rax)
	ret
	.cfi_endproc
.LFE5593:
	.size	_ZN2v88internal6torque13DefaultActionEPNS1_19ParseResultIteratorE, .-_ZN2v88internal6torque13DefaultActionEPNS1_19ParseResultIteratorE
	.section	.text.unlikely._ZNKSt14default_deleteIN2v88internal6torque4RuleEEclEPS3_.isra.0,"ax",@progbits
	.align 2
	.type	_ZNKSt14default_deleteIN2v88internal6torque4RuleEEclEPS3_.isra.0, @function
_ZNKSt14default_deleteIN2v88internal6torque4RuleEEclEPS3_.isra.0:
.LFB9657:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L5
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rdx
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	popq	%rax
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L5:
	ret
	.cfi_endproc
.LFE9657:
	.size	_ZNKSt14default_deleteIN2v88internal6torque4RuleEEclEPS3_.isra.0, .-_ZNKSt14default_deleteIN2v88internal6torque4RuleEEclEPS3_.isra.0
	.section	.rodata._ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"start.pos.source == end.pos.source"
	.section	.rodata._ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE,"axG",@progbits,_ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE
	.type	_ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE, @function
_ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE:
.LFB5657:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	16(%rsi), %rdi
	movq	24(%rdx), %r8
	movq	24(%rsi), %rsi
	leaq	(%rdi,%rdi,4), %rdx
	leaq	(%r8,%rdx,8), %rdx
	movq	%rdx, %rcx
	cmpq	%rsi, %rdi
	je	.L15
	leaq	-5(%rsi,%rsi,4), %rcx
	leaq	(%r8,%rcx,8), %rcx
.L15:
	movl	16(%rdx), %edi
	cmpl	%edi, 16(%rcx)
	jne	.L22
	movq	(%rdx), %xmm0
	movl	24(%rdx), %edi
	movq	16(%rdx), %r8
	movq	28(%rcx), %rsi
	movhps	8(%rcx), %xmm0
	movl	%edi, 24(%rax)
	movq	%r8, 16(%rax)
	movq	%rsi, 28(%rax)
	movups	%xmm0, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5657:
	.size	_ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE, .-_ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE
	.section	.rodata._ZNK2v88internal6torque4Item8ChildrenEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"vector::_M_realloc_insert"
	.section	.text.unlikely._ZNK2v88internal6torque4Item8ChildrenEv,"ax",@progbits
	.align 2
.LCOLDB3:
	.section	.text._ZNK2v88internal6torque4Item8ChildrenEv,"ax",@progbits
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Item8ChildrenEv
	.type	_ZNK2v88internal6torque4Item8ChildrenEv, @function
_ZNK2v88internal6torque4Item8ChildrenEv:
.LFB5795:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5795
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	cmpq	$0, 32(%rsi)
	je	.L23
	movq	%rsi, %rbx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L65:
	movq	40(%rbx), %rax
	movq	%rax, (%r14)
	movq	8(%r12), %rax
	leaq	8(%rax), %r14
	movq	%r14, 8(%r12)
	movq	32(%rbx), %rbx
	cmpq	$0, 32(%rbx)
	je	.L64
.L33:
	movq	16(%r12), %rax
.L35:
	cmpq	%rax, %r14
	jne	.L65
	movabsq	$1152921504606846975, %rdi
	movq	(%r12), %r15
	subq	%r15, %r14
	movq	%r14, %rax
	movq	%r14, %r13
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L66
	testq	%rax, %rax
	je	.L44
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L67
.L28:
	movq	%r14, %rdi
.LEHB0:
	call	_Znwm@PLT
	movq	%rax, %rcx
	leaq	(%rax,%r14), %r8
.L29:
	movq	40(%rbx), %rax
	leaq	8(%rcx,%r13), %r14
	movq	%rax, (%rcx,%r13)
	testq	%r13, %r13
	jg	.L68
	testq	%r15, %r15
	jne	.L31
.L32:
	movq	%rcx, %xmm0
	movq	%r14, %xmm3
	movq	%r8, 16(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r12)
	movq	32(%rbx), %rbx
	cmpq	$0, 32(%rbx)
	jne	.L33
	.p2align 4,,10
	.p2align 3
.L64:
	movq	(%r12), %rcx
	cmpq	%r14, %rcx
	je	.L23
	leaq	-8(%r14), %rdx
	cmpq	%rcx, %rdx
	jbe	.L23
	leaq	-9(%r14), %r8
	movq	%rcx, %rax
	subq	%rcx, %r8
	movq	%r8, %rsi
	shrq	$4, %rsi
	leaq	1(%rsi), %rdi
	notq	%rsi
	leaq	(%r14,%rsi,8), %r9
	leaq	(%rcx,%rdi,8), %rsi
	cmpq	%rsi, %r9
	setnb	%r9b
	cmpq	%r14, %rcx
	setnb	%sil
	orb	%sil, %r9b
	je	.L40
	cmpq	$31, %r8
	jbe	.L40
	movq	%rdi, %rsi
	subq	$16, %r14
	shrq	%rsi
	salq	$4, %rsi
	addq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L37:
	movdqu	(%r14), %xmm1
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	subq	$16, %r14
	shufpd	$1, %xmm1, %xmm1
	shufpd	$1, %xmm0, %xmm0
	movups	%xmm1, -16(%rax)
	movups	%xmm0, 16(%r14)
	cmpq	%rsi, %rax
	jne	.L37
	movq	%rdi, %rsi
	andq	$-2, %rsi
	movq	%rsi, %rax
	negq	%rax
	leaq	(%rdx,%rax,8), %rax
	leaq	(%rcx,%rsi,8), %rdx
	cmpq	%rsi, %rdi
	je	.L23
	movq	(%rdx), %rcx
	movq	(%rax), %rsi
	movq	%rsi, (%rdx)
	movq	%rcx, (%rax)
.L23:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L69
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%rax), %rcx
	movq	(%rdx), %rsi
	addq	$8, %rax
	subq	$8, %rdx
	movq	%rsi, -8(%rax)
	movq	%rcx, 8(%rdx)
	cmpq	%rdx, %rax
	jb	.L40
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rcx
.L31:
	movq	%r15, %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$8, %r14d
	jmp	.L28
.L66:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE0:
.L69:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %r14
	jmp	.L28
.L47:
	endbr64
	movq	%rax, %r13
	jmp	.L42
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZNK2v88internal6torque4Item8ChildrenEv,"a",@progbits
.LLSDA5795:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5795-.LLSDACSB5795
.LLSDACSB5795:
	.uleb128 .LEHB0-.LFB5795
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L47-.LFB5795
	.uleb128 0
.LLSDACSE5795:
	.section	.text._ZNK2v88internal6torque4Item8ChildrenEv
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque4Item8ChildrenEv
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5795
	.type	_ZNK2v88internal6torque4Item8ChildrenEv.cold, @function
_ZNK2v88internal6torque4Item8ChildrenEv.cold:
.LFSB5795:
.L42:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	%r13, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
	.cfi_endproc
.LFE5795:
	.section	.gcc_except_table._ZNK2v88internal6torque4Item8ChildrenEv
.LLSDAC5795:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5795-.LLSDACSBC5795
.LLSDACSBC5795:
	.uleb128 .LEHB1-.LCOLDB3
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSEC5795:
	.section	.text.unlikely._ZNK2v88internal6torque4Item8ChildrenEv
	.section	.text._ZNK2v88internal6torque4Item8ChildrenEv
	.size	_ZNK2v88internal6torque4Item8ChildrenEv, .-_ZNK2v88internal6torque4Item8ChildrenEv
	.section	.text.unlikely._ZNK2v88internal6torque4Item8ChildrenEv
	.size	_ZNK2v88internal6torque4Item8ChildrenEv.cold, .-_ZNK2v88internal6torque4Item8ChildrenEv.cold
.LCOLDE3:
	.section	.text._ZNK2v88internal6torque4Item8ChildrenEv
.LHOTE3:
	.section	.rodata._ZN2v88internal6torque5Lexer10MatchTokenEPPKcS4_.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal6torque5Lexer10MatchTokenEPPKcS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque5Lexer10MatchTokenEPPKcS4_
	.type	_ZN2v88internal6torque5Lexer10MatchTokenEPPKcS4_, @function
_ZN2v88internal6torque5Lexer10MatchTokenEPPKcS4_:
.LFB5826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	64(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rdx, -128(%rbp)
	movq	32(%rdi), %r14
	movq	%rsi, -136(%rbp)
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$0, -144(%rbp)
	movq	%rax, -120(%rbp)
	cmpq	%r14, %rax
	je	.L71
	leaq	-104(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r13, -104(%rbp)
	movq	%r15, %rdi
	call	*32(%r14)
	testb	%al, %al
	je	.L72
	movq	-136(%rbp), %rsi
	movq	-104(%rbp), %rax
	cmpq	%rax, (%rsi)
	jnb	.L72
	movq	%rax, (%rsi)
	leaq	40(%r14), %rax
	movq	%rax, -144(%rbp)
.L72:
	movq	%r14, %rdi
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r14
	cmpq	-120(%rbp), %rax
	jne	.L73
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	subq	%r13, %rax
	movq	%rax, -120(%rbp)
	cmpq	%r12, 80(%rbx)
	je	.L74
.L91:
	movq	-128(%rbp), %r15
	leaq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	subq	%r13, %r15
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L117:
	cmpq	-120(%rbp), %r14
	jnb	.L76
.L116:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%r12, 80(%rbx)
	je	.L74
.L90:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %r14
	movq	%rax, %rcx
	cmpq	%r14, %r15
	jnb	.L117
	movq	%rax, %r12
	cmpq	%r12, 80(%rbx)
	jne	.L90
.L74:
	cmpq	$0, -120(%rbp)
	je	.L92
.L70:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	movq	-144(%rbp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	-128(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	%r13, %rax
	addq	%r14, %rax
	je	.L77
	testq	%r13, %r13
	je	.L119
.L77:
	movq	%r14, -104(%rbp)
	cmpq	$15, %r14
	ja	.L120
	cmpq	$1, %r14
	jne	.L80
	movzbl	0(%r13), %eax
	movb	%al, -80(%rbp)
	movq	-128(%rbp), %rax
.L81:
	movq	%r14, -88(%rbp)
	movb	$0, (%rax,%r14)
	movq	40(%rcx), %rdx
	movq	-96(%rbp), %r8
	cmpq	-88(%rbp), %rdx
	je	.L82
.L83:
	cmpq	-128(%rbp), %r8
	je	.L116
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L82:
	testq	%rdx, %rdx
	je	.L84
	movq	32(%rcx), %rdi
	movq	%r8, %rsi
	movq	%rcx, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%r8, -152(%rbp)
	call	memcmp@PLT
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %rdx
	testl	%eax, %eax
	movq	-168(%rbp), %rcx
	jne	.L83
.L84:
	movq	%rcx, -120(%rbp)
	cmpq	-128(%rbp), %r8
	je	.L88
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rcx
	movq	40(%rcx), %rdx
.L88:
	movq	-136(%rbp), %rax
	addq	%rdx, %r13
	movq	%r12, %rdi
	movq	%r13, (%rax)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	addq	$64, %rax
	movq	%rax, -144(%rbp)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L80:
	testq	%r14, %r14
	jne	.L121
	movq	-128(%rbp), %rax
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L79:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rcx, -152(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r14
	movq	-96(%rbp), %rax
	movq	-152(%rbp), %rcx
	jmp	.L81
.L71:
	movq	$0, -120(%rbp)
	cmpq	%r12, 80(%rdi)
	jne	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	movq	$0, -144(%rbp)
	jmp	.L70
.L118:
	call	__stack_chk_fail@PLT
.L119:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L121:
	movq	-128(%rbp), %rdi
	jmp	.L79
	.cfi_endproc
.LFE5826:
	.size	_ZN2v88internal6torque5Lexer10MatchTokenEPPKcS4_, .-_ZN2v88internal6torque5Lexer10MatchTokenEPPKcS4_
	.section	.text._ZN2v88internal6torque7Grammar9MatchCharEPFiiEPPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque7Grammar9MatchCharEPFiiEPPKc
	.type	_ZN2v88internal6torque7Grammar9MatchCharEPFiiEPPKc, @function
_ZN2v88internal6torque7Grammar9MatchCharEPFiiEPPKc:
.LFB5882:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	%rdi, %rax
	movzbl	(%rdx), %edi
	testb	%dil, %dil
	jne	.L123
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	*%rax
	testl	%eax, %eax
	jne	.L132
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	addq	$1, (%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5882:
	.size	_ZN2v88internal6torque7Grammar9MatchCharEPFiiEPPKc, .-_ZN2v88internal6torque7Grammar9MatchCharEPFiiEPPKc
	.section	.text._ZN2v88internal6torque7Grammar9MatchCharEPFbcEPPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque7Grammar9MatchCharEPFbcEPPKc
	.type	_ZN2v88internal6torque7Grammar9MatchCharEPFbcEPPKc, @function
_ZN2v88internal6torque7Grammar9MatchCharEPFbcEPPKc:
.LFB5883:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	%rdi, %rax
	movsbl	(%rdx), %edi
	testb	%dil, %dil
	jne	.L134
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	*%rax
	testb	%al, %al
	jne	.L143
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	addq	$1, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5883:
	.size	_ZN2v88internal6torque7Grammar9MatchCharEPFbcEPPKc, .-_ZN2v88internal6torque7Grammar9MatchCharEPFbcEPPKc
	.section	.text._ZN2v88internal6torque7Grammar11MatchStringEPKcPS4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque7Grammar11MatchStringEPKcPS4_
	.type	_ZN2v88internal6torque7Grammar11MatchStringEPKcPS4_, @function
_ZN2v88internal6torque7Grammar11MatchStringEPKcPS4_:
.LFB5884:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	movq	(%rsi), %rdx
	testb	%al, %al
	jne	.L147
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L153:
	movzbl	1(%rdi), %eax
	addq	$1, %rdi
	addq	$1, %rdx
	testb	%al, %al
	je	.L145
.L147:
	cmpb	%al, (%rdx)
	je	.L153
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%rdx, (%rsi)
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE5884:
	.size	_ZN2v88internal6torque7Grammar11MatchStringEPKcPS4_, .-_ZN2v88internal6torque7Grammar11MatchStringEPKcPS4_
	.section	.text._ZN2v88internal6torque7Grammar12MatchAnyCharEPPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque7Grammar12MatchAnyCharEPPKc
	.type	_ZN2v88internal6torque7Grammar12MatchAnyCharEPPKc, @function
_ZN2v88internal6torque7Grammar12MatchAnyCharEPPKc:
.LFB5885:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%r8d, %r8d
	cmpb	$0, (%rax)
	je	.L154
	addq	$1, %rax
	movl	$1, %r8d
	movq	%rax, (%rdi)
.L154:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE5885:
	.size	_ZN2v88internal6torque7Grammar12MatchAnyCharEPPKc, .-_ZN2v88internal6torque7Grammar12MatchAnyCharEPPKc
	.section	.text._ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED2Ev,"axG",@progbits,_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED2Ev
	.type	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED2Ev, @function
_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED2Ev:
.LFB6543:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	je	.L157
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L157
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L157:
	ret
	.cfi_endproc
.LFE6543:
	.size	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED2Ev, .-_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED2Ev
	.weak	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED1Ev
	.set	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED1Ev,_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED2Ev
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED2Ev,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED2Ev
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED2Ev, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED2Ev:
.LFB6572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L163
	movq	%rdi, %r14
	.p2align 4,,10
	.p2align 3
.L166:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L164
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L165
	call	_ZdlPv@PLT
.L165:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L164:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L166
	movq	(%r14), %r12
.L163:
	testq	%r12, %r12
	je	.L162
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6572:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED2Ev, .-_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED2Ev
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED1Ev
	.set	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED1Ev,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED2Ev
	.section	.rodata._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"  "
	.section	.text.unlikely._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE,"ax",@progbits
	.align 2
.LCOLDB9:
	.section	.text._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE,"ax",@progbits
.LHOTB9:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
	.type	_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE, @function
_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE:
.LFB5811:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5811
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -552(%rbp)
	movq	(%rsi), %rdx
	movq	%rdi, -560(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	cmpq	$8, %rax
	je	.L235
.L177:
	leaq	-320(%rbp), %r14
	leaq	-448(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rbx, -600(%rbp)
	movq	%r14, -592(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -104(%rbp)
	movq	%rax, -448(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %rbx
	movq	%rbx, %rdi
.LEHB2:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE2:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	-432(%rbp), %r15
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
.LEHB3:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE3:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	.LC6(%rip), %xmm0
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movhps	.LC7(%rip), %xmm0
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -576(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movl	$24, -360(%rbp)
	movq	%rax, -584(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB4:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE4:
	leaq	-512(%rbp), %rdi
	movq	%r12, %rsi
.LEHB5:
	call	_ZNK2v88internal6torque4Item8ChildrenEv
.LEHE5:
	movq	-504(%rbp), %rax
	movq	-512(%rbp), %r13
	movq	%rax, -544(%rbp)
	cmpq	%r13, %rax
	je	.L184
	leaq	-480(%rbp), %rax
	movb	$1, -529(%rbp)
	movq	%rax, -568(%rbp)
.L197:
	movq	0(%r13), %rbx
	testq	%rbx, %rbx
	je	.L212
	cmpb	$0, -529(%rbp)
	je	.L236
.L186:
	movq	-552(%rbp), %rcx
	movq	16(%rbx), %rax
	movq	24(%rbx), %rsi
	movq	24(%rcx), %rdi
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movq	%rdx, %rcx
	cmpq	%rsi, %rax
	je	.L187
	leaq	-5(%rsi,%rsi,4), %rax
	leaq	(%rdi,%rax,8), %rcx
.L187:
	movl	16(%rcx), %eax
	cmpl	%eax, 16(%rdx)
	jne	.L237
	movq	(%rdx), %r14
	movq	8(%rcx), %r12
	leaq	-464(%rbp), %rbx
	movq	%rbx, -480(%rbp)
	testq	%r14, %r14
	sete	%dl
	testq	%r12, %r12
	setne	%al
	andb	%al, %dl
	movb	%dl, -529(%rbp)
	jne	.L238
	subq	%r14, %r12
	movq	%r12, -520(%rbp)
	cmpq	$15, %r12
	ja	.L239
	cmpq	$1, %r12
	jne	.L192
	movzbl	(%r14), %eax
	movb	%al, -464(%rbp)
	movq	%rbx, %rax
.L193:
	movq	%r12, -472(%rbp)
	movq	%r15, %rdi
	movb	$0, (%rax,%r12)
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
.LEHB6:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE6:
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L194
	call	_ZdlPv@PLT
.L194:
	leaq	8(%r13), %r12
	cmpq	%r12, -544(%rbp)
	je	.L234
	movq	8(%r13), %rbx
	testq	%rbx, %rbx
	je	.L185
.L196:
	movl	$2, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
.LEHB7:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %r13
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L192:
	testq	%r12, %r12
	jne	.L240
	movq	%rbx, %rax
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-568(%rbp), %rdi
	leaq	-520(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE7:
	movq	%rax, -480(%rbp)
	movq	%rax, %rdi
	movq	-520(%rbp), %rax
	movq	%rax, -464(%rbp)
.L191:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-520(%rbp), %r12
	movq	-480(%rbp), %rax
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%r13, %r12
.L185:
	leaq	8(%r12), %r13
	cmpq	%r13, -544(%rbp)
	jne	.L197
	.p2align 4,,10
	.p2align 3
.L234:
	movq	-512(%rbp), %r13
.L184:
	testq	%r13, %r13
	je	.L198
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L198:
	movq	-560(%rbp), %rdi
	movq	-384(%rbp), %rax
	leaq	16(%rdi), %rbx
	movq	$0, 8(%rdi)
	movq	%rbx, (%rdi)
	movb	$0, 16(%rdi)
	testq	%rax, %rax
	je	.L199
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L200
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
.LEHB8:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE8:
.L201:
	movq	.LC6(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC8(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-584(%rbp), %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	movq	-576(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	-592(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rsi, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L176:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	movq	-560(%rbp), %rax
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	leaq	-512(%rbp), %rdi
.LEHB9:
	call	_ZNK2v88internal6torque4Item8ChildrenEv
	movq	-512(%rbp), %rdi
	movq	(%rdi), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L177
	movq	-552(%rbp), %rdx
	movq	-560(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
.LEHE9:
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
.LEHB10:
	call	_Z8V8_FatalPKcz@PLT
.LEHE10:
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%r13, %r12
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L200:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB11:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L199:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE11:
	jmp	.L201
.L241:
	call	__stack_chk_fail@PLT
.L238:
	leaq	.LC4(%rip), %rdi
.LEHB12:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE12:
.L240:
	movq	%rbx, %rdi
	jmp	.L191
.L216:
	endbr64
	movq	%rax, %r12
	jmp	.L207
.L215:
	endbr64
	movq	%rax, %r12
	jmp	.L209
.L221:
	endbr64
	movq	%rax, %r12
	jmp	.L204
.L217:
	endbr64
	movq	%rax, %r12
	jmp	.L206
.L219:
	endbr64
	movq	%rax, %rbx
	jmp	.L182
.L220:
	endbr64
	movq	%rax, %rbx
	jmp	.L180
.L218:
	endbr64
	movq	%rax, %rbx
	jmp	.L181
	.section	.gcc_except_table._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE,"a",@progbits
.LLSDA5811:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5811-.LLSDACSB5811
.LLSDACSB5811:
	.uleb128 .LEHB2-.LFB5811
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L218-.LFB5811
	.uleb128 0
	.uleb128 .LEHB3-.LFB5811
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L220-.LFB5811
	.uleb128 0
	.uleb128 .LEHB4-.LFB5811
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L219-.LFB5811
	.uleb128 0
	.uleb128 .LEHB5-.LFB5811
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L217-.LFB5811
	.uleb128 0
	.uleb128 .LEHB6-.LFB5811
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L216-.LFB5811
	.uleb128 0
	.uleb128 .LEHB7-.LFB5811
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L215-.LFB5811
	.uleb128 0
	.uleb128 .LEHB8-.LFB5811
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L221-.LFB5811
	.uleb128 0
	.uleb128 .LEHB9-.LFB5811
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB10-.LFB5811
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L215-.LFB5811
	.uleb128 0
	.uleb128 .LEHB11-.LFB5811
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L221-.LFB5811
	.uleb128 0
	.uleb128 .LEHB12-.LFB5811
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L215-.LFB5811
	.uleb128 0
.LLSDACSE5811:
	.section	.text._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5811
	.type	_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE.cold, @function
_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE.cold:
.LFSB5811:
.L207:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L209
	call	_ZdlPv@PLT
.L209:
	movq	-512(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L206
	call	_ZdlPv@PLT
	jmp	.L206
.L204:
	movq	-560(%rbp), %rax
	movq	(%rax), %rdi
	cmpq	%rdi, %rbx
	je	.L206
	call	_ZdlPv@PLT
.L206:
	movq	-600(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB13:
	call	_Unwind_Resume@PLT
.L182:
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -424(%rbp)
	cmpq	-584(%rbp), %rdi
	je	.L183
	call	_ZdlPv@PLT
.L183:
	movq	-576(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L181:
	movq	-592(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE13:
.L180:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L181
	.cfi_endproc
.LFE5811:
	.section	.gcc_except_table._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
.LLSDAC5811:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5811-.LLSDACSBC5811
.LLSDACSBC5811:
	.uleb128 .LEHB13-.LCOLDB9
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
.LLSDACSEC5811:
	.section	.text.unlikely._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
	.section	.text._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
	.size	_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE, .-_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
	.section	.text.unlikely._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
	.size	_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE.cold, .-_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE.cold
.LCOLDE9:
	.section	.text._ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
.LHOTE9:
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.p2align 4
	.weak	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.type	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, @function
_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_:
.LFB6753:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6753
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-432(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$496, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
.LEHB14:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE14:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	leaq	-416(%rbp), %rdi
.LEHB15:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-528(%rbp), %r14
	leaq	-408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE15:
	leaq	-496(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB16:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE16:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L243
	call	_ZdlPv@PLT
.L243:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB17:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE17:
.L251:
	endbr64
	movq	%rax, %r12
	jmp	.L246
.L250:
	endbr64
	movq	%rax, %r13
	jmp	.L247
.L252:
	endbr64
	movq	%rax, %r12
.L244:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L246
	call	_ZdlPv@PLT
.L246:
	movq	%r13, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB18:
	call	_Unwind_Resume@PLT
.LEHE18:
.L247:
	movq	%r12, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-496(%rbp), %rdi
	leaq	-480(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L248
	call	_ZdlPv@PLT
.L248:
	movq	%r13, %rdi
.LEHB19:
	call	_Unwind_Resume@PLT
.LEHE19:
	.cfi_endproc
.LFE6753:
	.section	.gcc_except_table._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"aG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
.LLSDA6753:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6753-.LLSDACSB6753
.LLSDACSB6753:
	.uleb128 .LEHB14-.LFB6753
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB15-.LFB6753
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L251-.LFB6753
	.uleb128 0
	.uleb128 .LEHB16-.LFB6753
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L252-.LFB6753
	.uleb128 0
	.uleb128 .LEHB17-.LFB6753
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L250-.LFB6753
	.uleb128 0
	.uleb128 .LEHB18-.LFB6753
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB19-.LFB6753
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSE6753:
	.section	.text._ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,"axG",@progbits,_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_,comdat
	.size	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_, .-_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
	.section	.text._ZNSt10_HashtableIN2v88internal6torque4ItemES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS3_,"axG",@progbits,_ZNSt10_HashtableIN2v88internal6torque4ItemES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal6torque4ItemES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS3_
	.type	_ZNSt10_HashtableIN2v88internal6torque4ItemES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS3_, @function
_ZNSt10_HashtableIN2v88internal6torque4ItemES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS3_:
.LFB7579:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%rsi, %rbx
	call	_ZN2v84base10hash_valueEm@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r13
	call	_ZN2v84base10hash_valueEm@PLT
	movq	24(%rbx), %rsi
	movq	16(%rbx), %rdi
	movq	%rax, %r14
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	8(%r12), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L255
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	56(%rcx), %r8
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L256:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L255
	movq	56(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L255
.L258:
	cmpq	%r8, %rsi
	jne	.L256
	movq	8(%rcx), %rax
	cmpq	%rax, (%rbx)
	jne	.L256
	movq	16(%rcx), %rax
	cmpq	%rax, 8(%rbx)
	jne	.L256
	movq	24(%rcx), %rax
	cmpq	%rax, 16(%rbx)
	jne	.L256
	movq	32(%rcx), %rax
	cmpq	%rax, 24(%rbx)
	jne	.L256
	popq	%rbx
	movq	%rcx, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7579:
	.size	_ZNSt10_HashtableIN2v88internal6torque4ItemES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS3_, .-_ZNSt10_HashtableIN2v88internal6torque4ItemES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS3_
	.section	.text._ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_:
.LFB7875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r13
	movq	%rdi, -88(%rbp)
	movabsq	$1152921504606846975, %rdi
	movq	%rax, -80(%rbp)
	subq	%r13, %rax
	sarq	$3, %rax
	cmpq	%rdi, %rax
	je	.L299
	movq	%rsi, %r8
	movq	%rsi, %r12
	movq	%rsi, %rbx
	subq	%r13, %r8
	testq	%rax, %rax
	je	.L283
	movabsq	$9223372036854775800, %rsi
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L300
.L271:
	movq	%rsi, %rdi
	movq	%rdx, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-96(%rbp), %r8
	movq	%rax, -56(%rbp)
	movq	-104(%rbp), %rdx
	addq	%rax, %rsi
	movq	%rsi, -72(%rbp)
	leaq	8(%rax), %rsi
.L282:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%rdx)
	movq	%rax, (%rcx,%r8)
	cmpq	%r13, %r12
	je	.L273
	movq	%rcx, %r14
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L276:
	movq	(%r15), %rsi
	movq	$0, (%r15)
	movq	%rsi, (%r14)
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L274
	movq	8(%r8), %rdi
	testq	%rdi, %rdi
	je	.L275
	movq	%r8, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r8
.L275:
	movl	$40, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L274:
	addq	$8, %r15
	addq	$8, %r14
	cmpq	%r15, %r12
	jne	.L276
	movq	-56(%rbp), %rcx
	movq	%r12, %rax
	subq	%r13, %rax
	leaq	8(%rcx,%rax), %rsi
.L273:
	movq	-80(%rbp), %rax
	cmpq	%rax, %r12
	je	.L277
	movq	%rax, %r15
	subq	%r12, %r15
	leaq	-8(%r15), %r8
	movq	%r8, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r8, %r8
	je	.L285
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L279:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L279
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %rbx
	leaq	(%rsi,%rbx), %rdx
	addq	%r12, %rbx
	cmpq	%rax, %rdi
	je	.L280
.L278:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L280:
	leaq	8(%rsi,%r8), %rsi
.L277:
	testq	%r13, %r13
	je	.L281
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rsi
.L281:
	movq	-56(%rbp), %xmm0
	movq	-88(%rbp), %rax
	movq	%rsi, %xmm2
	movq	-72(%rbp), %rcx
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L272
	movq	$0, -72(%rbp)
	movl	$8, %esi
	movq	$0, -56(%rbp)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$8, %esi
	jmp	.L271
.L285:
	movq	%rsi, %rdx
	jmp	.L278
.L272:
	cmpq	%rdi, %r9
	cmovbe	%r9, %rdi
	movq	%rdi, %rsi
	salq	$3, %rsi
	jmp	.L271
.L299:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7875:
	.size	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
	.section	.text.unlikely._ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE,"ax",@progbits
	.align 2
.LCOLDB10:
	.section	.text._ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE,"ax",@progbits
.LHOTB10:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE
	.type	_ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE, @function
_ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE:
.LFB5794:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5794
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r15
	movq	8(%rdi), %r12
	movq	%rdx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r12, %r15
	je	.L302
	movq	%r15, %r13
	.p2align 4,,10
	.p2align 3
.L305:
	movq	0(%r13), %r8
	testq	%r8, %r8
	je	.L303
	movq	8(%r8), %rdi
	testq	%rdi, %rdi
	je	.L304
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
.L304:
	movl	$40, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L303:
	addq	$8, %r13
	cmpq	%r13, %r12
	jne	.L305
	movq	%r15, 8(%r14)
.L302:
	movq	-80(%rbp), %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rbx
	jne	.L317
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L307:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L341
	movq	%r13, %rdi
.LEHB20:
	call	_Znwm@PLT
.LEHE20:
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	8(%rbx), %rsi
	movq	%rax, %r15
	subq	%rsi, %r15
.L308:
	movq	%rcx, %xmm0
	addq	%rcx, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 24(%r12)
	movups	%xmm0, 8(%r12)
	cmpq	%rax, %rsi
	je	.L310
	movq	%rcx, %rdi
	movq	%r15, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L310:
	movq	32(%rbx), %rax
	addq	%r15, %rcx
	movq	8(%r14), %rsi
	movq	%r12, -64(%rbp)
	movq	%rcx, 16(%r12)
	movq	%rax, 32(%r12)
	cmpq	16(%r14), %rsi
	je	.L342
	movq	$0, -64(%rbp)
	movq	%r12, (%rsi)
	addq	$8, 8(%r14)
.L314:
	movq	-64(%rbp), %r12
	testq	%r12, %r12
	je	.L315
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L316
	call	_ZdlPv@PLT
.L316:
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L315:
	movq	8(%r14), %rax
	addq	$40, %rbx
	movq	-8(%rax), %rax
	movq	%r14, (%rax)
	cmpq	%rbx, -72(%rbp)
	je	.L306
.L317:
	movl	$40, %edi
.LEHB21:
	call	_Znwm@PLT
.LEHE21:
	movq	8(%rbx), %rsi
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	$0, 8(%r12)
	movq	%rax, (%r12)
	movq	16(%rbx), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	%rax, %r13
	subq	%rsi, %r13
	movq	%r13, %rdx
	sarq	$3, %rdx
	jne	.L307
	movq	%r13, %r15
	xorl	%ecx, %ecx
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L342:
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
.LEHB22:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE22:
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L343
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L341:
	.cfi_restore_state
.LEHB23:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE23:
.L343:
	call	__stack_chk_fail@PLT
.L321:
	endbr64
	movq	%rax, %r12
	jmp	.L318
.L322:
	endbr64
	movq	%rax, %r13
	jmp	.L313
	.section	.gcc_except_table._ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE,"a",@progbits
.LLSDA5794:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5794-.LLSDACSB5794
.LLSDACSB5794:
	.uleb128 .LEHB20-.LFB5794
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L322-.LFB5794
	.uleb128 0
	.uleb128 .LEHB21-.LFB5794
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB22-.LFB5794
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L321-.LFB5794
	.uleb128 0
	.uleb128 .LEHB23-.LFB5794
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L322-.LFB5794
	.uleb128 0
.LLSDACSE5794:
	.section	.text._ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5794
	.type	_ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE.cold, @function
_ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE.cold:
.LFSB5794:
.L318:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L319
	call	_ZNKSt14default_deleteIN2v88internal6torque4RuleEEclEPS3_.isra.0
.L319:
	movq	%r12, %rdi
.LEHB24:
	call	_Unwind_Resume@PLT
.L313:
	movq	%r12, %rdi
	movl	$40, %esi
	call	_ZdlPvm@PLT
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE24:
	.cfi_endproc
.LFE5794:
	.section	.gcc_except_table._ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE
.LLSDAC5794:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5794-.LLSDACSBC5794
.LLSDACSBC5794:
	.uleb128 .LEHB24-.LCOLDB10
	.uleb128 .LEHE24-.LEHB24
	.uleb128 0
	.uleb128 0
.LLSDACSEC5794:
	.section	.text.unlikely._ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE
	.section	.text._ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE
	.size	_ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE, .-_ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE
	.section	.text.unlikely._ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE
	.size	_ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE.cold, .-_ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE.cold
.LCOLDE10:
	.section	.text._ZN2v88internal6torque6SymbolaSESt16initializer_listINS1_4RuleEE
.LHOTE10:
	.section	.text._ZNSt6vectorIN2v88internal6torque11ParseResultESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque11ParseResultESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque11ParseResultESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6torque11ParseResultESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6torque11ParseResultESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB7973:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L368
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L359
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L369
.L346:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L358:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L348
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L352:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L349
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L352
.L350:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L348:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L353
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L361
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L355:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L355
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L356
.L354:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L356:
	leaq	8(%rcx,%r9), %rcx
.L353:
	testq	%r12, %r12
	je	.L357
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L357:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L352
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L369:
	testq	%rdi, %rdi
	jne	.L347
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L359:
	movl	$8, %r13d
	jmp	.L346
.L361:
	movq	%rcx, %rdx
	jmp	.L354
.L347:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L346
.L368:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7973:
	.size	_ZNSt6vectorIN2v88internal6torque11ParseResultESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal6torque11ParseResultESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.rodata._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"results_.size() == i_"
	.section	.text.unlikely._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE,"ax",@progbits
	.align 2
.LCOLDB12:
	.section	.text._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE,"ax",@progbits
.LHOTB12:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
	.type	_ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE, @function
_ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE:
.LFB5786:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5786
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%rdx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-128(%rbp), %rax
	movaps	%xmm0, -192(%rbp)
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	movq	$0, -176(%rbp)
.LEHB25:
	call	_ZNK2v88internal6torque4Item8ChildrenEv
.LEHE25:
	movq	-128(%rbp), %rdi
	movq	-120(%rbp), %r15
	cmpq	%rdi, %r15
	je	.L371
	movq	%rdi, %rbx
	leaq	-160(%rbp), %r14
.L378:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L373
	movq	(%rax), %rsi
	movq	%r12, %rcx
	movq	%rax, %rdx
	movq	%r14, %rdi
.LEHB26:
	call	_ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
.LEHE26:
	cmpb	$0, -160(%rbp)
	je	.L375
	movq	-184(%rbp), %rsi
	cmpq	-176(%rbp), %rsi
	je	.L376
	movq	-152(%rbp), %rax
	movq	$0, -152(%rbp)
	movq	%rax, (%rsi)
	addq	$8, -184(%rbp)
.L375:
	movq	%r14, %rdi
	call	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED2Ev
.L373:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.L378
	movq	-128(%rbp), %rdi
.L371:
	testq	%rdi, %rdi
	je	.L379
	call	_ZdlPv@PLT
.L379:
	movq	16(%r13), %rcx
	movq	24(%r12), %rdi
	movq	24(%r13), %rsi
	leaq	(%rcx,%rcx,4), %rax
	leaq	(%rdi,%rax,8), %rax
	movq	%rax, %rdx
	cmpq	%rsi, %rcx
	je	.L380
	leaq	-5(%rsi,%rsi,4), %rdx
	leaq	(%rdi,%rdx,8), %rdx
.L380:
	movl	16(%rax), %ebx
	cmpl	16(%rdx), %ebx
	jne	.L441
	movl	20(%rax), %r12d
	movl	24(%rax), %r14d
	movq	8(%rdx), %rcx
	movq	(%rax), %rax
	movl	28(%rdx), %r15d
	movl	32(%rdx), %r13d
	movl	%ebx, -160(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movl	%r12d, -156(%rbp)
	movl	%r14d, -152(%rbp)
	movl	%r15d, -148(%rbp)
	movl	%r13d, -144(%rbp)
.LEHB27:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE27:
	leaq	-160(%rbp), %rdx
	pxor	%xmm1, %xmm1
	movd	%r15d, %xmm2
	movq	-216(%rbp), %rsi
	movq	%rdx, (%rax)
	movdqa	-192(%rbp), %xmm0
	movd	%r12d, %xmm3
	movq	-176(%rbp), %rax
	movq	-200(%rbp), %rdi
	movl	%r13d, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-224(%rbp), %xmm0
	movaps	%xmm1, -192(%rbp)
	movd	%r14d, %xmm1
	movhps	-232(%rbp), %xmm0
	movq	%rax, -112(%rbp)
	punpckldq	%xmm2, %xmm1
	movq	-208(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movd	%ebx, %xmm0
	punpckldq	%xmm3, %xmm0
	movq	$0, -104(%rbp)
	movq	$0, -176(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
.LEHB28:
	call	*32(%rax)
.LEHE28:
	movq	-120(%rbp), %r12
	movq	-128(%rbp), %rbx
	movq	%r12, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	%rax, -104(%rbp)
	jne	.L442
	cmpq	%r12, %rbx
	je	.L383
	.p2align 4,,10
	.p2align 3
.L387:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L384
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L387
.L385:
	movq	-128(%rbp), %r12
.L383:
	testq	%r12, %r12
	je	.L388
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L388:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-136(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-184(%rbp), %rbx
	movq	-192(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L389
	.p2align 4,,10
	.p2align 3
.L393:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L390
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L393
.L391:
	movq	-192(%rbp), %r12
.L389:
	testq	%r12, %r12
	je	.L370
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L370:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L443
	movq	-200(%rbp), %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L387
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L390:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L393
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	-152(%rbp), %rdx
	leaq	-192(%rbp), %rdi
.LEHB29:
	call	_ZNSt6vectorIN2v88internal6torque11ParseResultESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE29:
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L441:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
.LEHB30:
	call	_Z8V8_FatalPKcz@PLT
.LEHE30:
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	.LC11(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L443:
	call	__stack_chk_fail@PLT
.L412:
	endbr64
	movq	%rax, %r12
	jmp	.L398
.L413:
	endbr64
	movq	%rax, %r12
	jmp	.L399
.L410:
	endbr64
	movq	%rax, %r12
	jmp	.L396
.L411:
	endbr64
	movq	%rax, %r12
	jmp	.L395
	.section	.gcc_except_table._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE,"a",@progbits
.LLSDA5786:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5786-.LLSDACSB5786
.LLSDACSB5786:
	.uleb128 .LEHB25-.LFB5786
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L412-.LFB5786
	.uleb128 0
	.uleb128 .LEHB26-.LFB5786
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L410-.LFB5786
	.uleb128 0
	.uleb128 .LEHB27-.LFB5786
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L412-.LFB5786
	.uleb128 0
	.uleb128 .LEHB28-.LFB5786
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L413-.LFB5786
	.uleb128 0
	.uleb128 .LEHB29-.LFB5786
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L411-.LFB5786
	.uleb128 0
	.uleb128 .LEHB30-.LFB5786
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L412-.LFB5786
	.uleb128 0
.LLSDACSE5786:
	.section	.text._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5786
	.type	_ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE.cold, @function
_ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE.cold:
.LFSB5786:
.L395:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r14, %rdi
	call	_ZN2v84base8internal19OptionalStorageBaseINS_8internal6torque11ParseResultELb0EED2Ev
.L396:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L398
	call	_ZdlPv@PLT
.L398:
	movq	-184(%rbp), %r13
	movq	-192(%rbp), %rbx
.L406:
	cmpq	%rbx, %r13
	jne	.L444
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L407
	call	_ZdlPv@PLT
.L407:
	movq	%r12, %rdi
.LEHB31:
	call	_Unwind_Resume@PLT
.LEHE31:
.L399:
	movq	-120(%rbp), %r13
	movq	-128(%rbp), %rbx
	movq	%r13, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	%rax, -104(%rbp)
	je	.L400
	leaq	.LC11(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L445:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L402
	movq	(%rdi), %rax
	call	*8(%rax)
.L402:
	addq	$8, %rbx
.L400:
	cmpq	%rbx, %r13
	jne	.L445
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L403
	call	_ZdlPv@PLT
.L403:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-136(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L398
.L444:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L405
	movq	(%rdi), %rax
	call	*8(%rax)
.L405:
	addq	$8, %rbx
	jmp	.L406
	.cfi_endproc
.LFE5786:
	.section	.gcc_except_table._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
.LLSDAC5786:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5786-.LLSDACSBC5786
.LLSDACSBC5786:
	.uleb128 .LEHB31-.LCOLDB12
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0
	.uleb128 0
.LLSDACSEC5786:
	.section	.text.unlikely._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
	.section	.text._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
	.size	_ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE, .-_ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
	.section	.text.unlikely._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
	.size	_ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE.cold, .-_ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE.cold
.LCOLDE12:
	.section	.text._ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
.LHOTE12:
	.section	.text._ZNSt6vectorIN2v88internal6torque12MatchedInputESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque12MatchedInputESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque12MatchedInputESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6torque12MatchedInputESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6torque12MatchedInputESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB8021:
	.cfi_startproc
	endbr64
	movabsq	$-3689348814741910323, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$230584300921369395, %rcx
	cmpq	%rcx, %rax
	je	.L463
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L455
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L464
.L448:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	40(%r13), %rbx
.L454:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	movq	32(%rdx), %rax
	movups	%xmm3, 0(%r13,%r8)
	movq	%rax, 32(%r13,%r8)
	movups	%xmm4, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L450
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L451:
	movdqu	(%rax), %xmm1
	addq	$40, %rax
	addq	$40, %rdx
	movups	%xmm1, -40(%rdx)
	movdqu	-24(%rax), %xmm2
	movups	%xmm2, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L451
	leaq	-40(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	80(%r13,%rax,8), %rbx
.L450:
	cmpq	%rsi, %r12
	je	.L452
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-40(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	40(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L452:
	testq	%r14, %r14
	je	.L453
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L453:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L449
	movq	$0, -56(%rbp)
	movl	$40, %ebx
	xorl	%r13d, %r13d
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L455:
	movl	$40, %ebx
	jmp	.L448
.L449:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$40, %rbx, %rbx
	jmp	.L448
.L463:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8021:
	.size	_ZNSt6vectorIN2v88internal6torque12MatchedInputESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal6torque12MatchedInputESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.rodata._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"Lexer Error: unknown token "
	.section	.text.unlikely._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
	.align 2
.LCOLDB14:
	.section	.text._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB14:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5813:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5813
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movq	(%rdx), %rbx
	movq	8(%rdx), %rax
	addq	%rbx, %rax
	movq	%rbx, -224(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	%rax, %rdi
.LEHB32:
	call	*(%rsi)
	movq	-224(%rbp), %rdx
	movq	%rdx, -232(%rbp)
	cmpq	%rbx, %rdx
	je	.L466
	movq	%rbx, %rax
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L470:
	addl	$1, %r12d
	cmpb	$10, (%rax)
	jne	.L468
	addl	$1, %r15d
	xorl	%r12d, %r12d
.L468:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L470
	movq	-256(%rbp), %rdi
	cmpq	%rdi, -232(%rbp)
	je	.L564
	.p2align 4,,10
	.p2align 3
.L503:
	movq	-256(%rbp), %rdx
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rdi
	call	_ZN2v88internal6torque5Lexer10MatchTokenEPPKcS4_
	movq	%rax, -248(%rbp)
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %rax
	cmpq	%rax, %rbx
	je	.L525
	movl	%r15d, %edx
	movl	%r12d, %r13d
	.p2align 4,,10
	.p2align 3
.L476:
	addl	$1, %r13d
	cmpb	$10, (%rax)
	jne	.L475
	addl	$1, %edx
	xorl	%r13d, %r13d
.L475:
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L476
	movl	%edx, -236(%rbp)
.L473:
	cmpq	$0, -248(%rbp)
	je	.L565
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	je	.L491
	movq	-248(%rbp), %rdi
	movq	%rdi, (%rax)
	addq	$8, 8(%r14)
.L492:
	leaq	24(%r14), %rax
	movq	%rax, -280(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	32(%r14), %rsi
	movq	-232(%rbp), %xmm0
	movl	(%rax), %eax
	movl	%r15d, -188(%rbp)
	movhps	-248(%rbp), %xmm0
	movl	%r12d, -184(%rbp)
	movl	%eax, -192(%rbp)
	movl	-236(%rbp), %eax
	movl	%r13d, -176(%rbp)
	movl	%eax, -180(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	40(%r14), %rsi
	je	.L499
	movdqa	-208(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movdqa	-192(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	movq	-176(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 32(%r14)
.L500:
	movq	-264(%rbp), %rax
	movq	-272(%rbp), %rdi
	call	*(%rax)
	movq	-224(%rbp), %rax
	movl	-236(%rbp), %r15d
	movl	%r13d, %r12d
	cmpq	%rbx, %rax
	je	.L506
	.p2align 4,,10
	.p2align 3
.L501:
	addl	$1, %r12d
	cmpb	$10, (%rbx)
	jne	.L505
	addl	$1, %r15d
	xorl	%r12d, %r12d
.L505:
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L501
.L506:
	cmpq	-256(%rbp), %rax
	je	.L502
	movq	%rax, -232(%rbp)
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L499:
	leaq	-208(%rbp), %rdx
	leaq	24(%r14), %rdi
	call	_ZNSt6vectorIN2v88internal6torque12MatchedInputESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L491:
	movq	(%r14), %rcx
	subq	%rcx, %rax
	movq	%rcx, -280(%rbp)
	movabsq	$1152921504606846975, %rcx
	movq	%rax, -296(%rbp)
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L566
	testq	%rax, %rax
	je	.L527
	movabsq	$9223372036854775800, %rdi
	leaq	(%rax,%rax), %rdx
	movq	%rdi, -288(%rbp)
	cmpq	%rdx, %rax
	jbe	.L567
.L494:
	movq	-288(%rbp), %rdi
	call	_Znwm@PLT
	movq	-288(%rbp), %r8
	movq	%rax, %rcx
	addq	%rax, %r8
.L495:
	movq	-248(%rbp), %rax
	movq	-296(%rbp), %rdx
	movq	%rax, (%rcx,%rdx)
	leaq	8(%rcx,%rdx), %rax
	movq	%rax, -248(%rbp)
	testq	%rdx, %rdx
	jg	.L568
	cmpq	$0, -280(%rbp)
	jne	.L497
.L498:
	movq	%rcx, %xmm0
	movq	%r8, 16(%r14)
	movhps	-248(%rbp), %xmm0
	movups	%xmm0, (%r14)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L525:
	movl	%r15d, -236(%rbp)
	movl	%r12d, %r13d
	jmp	.L473
.L568:
	movq	-280(%rbp), %rsi
	movq	%rcx, %rdi
	movq	%r8, -288(%rbp)
	call	memmove@PLT
	movq	-288(%rbp), %r8
	movq	%rax, %rcx
.L497:
	movq	-280(%rbp), %rdi
	movq	%rcx, -296(%rbp)
	movq	%r8, -288(%rbp)
	call	_ZdlPv@PLT
	movq	-296(%rbp), %rcx
	movq	-288(%rbp), %r8
	jmp	.L498
.L567:
	testq	%rdx, %rdx
	jne	.L569
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L495
.L527:
	movq	$8, -288(%rbp)
	jmp	.L494
.L502:
	movq	-232(%rbp), %rcx
	cmpq	%rcx, -256(%rbp)
	je	.L570
.L472:
	movq	-232(%rbp), %rax
	movq	-256(%rbp), %rdx
	movl	%r15d, %r13d
	movl	%r12d, %ebx
	.p2align 4,,10
	.p2align 3
.L509:
	addl	$1, %ebx
	cmpb	$10, (%rax)
	jne	.L508
	addl	$1, %r13d
	xorl	%ebx, %ebx
.L508:
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L509
.L471:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	32(%r14), %rsi
	movq	-256(%rbp), %xmm0
	movl	(%rax), %eax
	movl	%r15d, -188(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movl	%r12d, -184(%rbp)
	movl	%eax, -192(%rbp)
	movl	%r13d, -180(%rbp)
	movl	%ebx, -176(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	40(%r14), %rsi
	je	.L510
	movdqa	-208(%rbp), %xmm3
	movups	%xmm3, (%rsi)
	movdqa	-192(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	movq	-176(%rbp), %rax
	movq	%rax, 32(%rsi)
	addq	$40, 32(%r14)
.L465:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L571
	addq	$264, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L510:
	.cfi_restore_state
	movq	-280(%rbp), %rdi
	leaq	-208(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal6torque12MatchedInputESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L465
.L466:
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	cmpq	-256(%rbp), %rbx
	jne	.L503
	leaq	24(%r14), %rax
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	movq	%rax, -280(%rbp)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L570:
	movl	%r15d, %r13d
	movl	%r12d, %ebx
	jmp	.L471
.L564:
	leaq	24(%r14), %rax
	movq	%rax, -280(%rbp)
	movq	-232(%rbp), %rax
	movq	%rbx, -232(%rbp)
	movq	%rax, -256(%rbp)
	jmp	.L472
.L566:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L565:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movl	(%rax), %eax
	movl	%r15d, -204(%rbp)
	movl	%r12d, -200(%rbp)
	movl	%eax, -208(%rbp)
	movl	-236(%rbp), %eax
	movl	%r13d, -192(%rbp)
	movl	%eax, -196(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE32:
	movq	-232(%rbp), %rcx
	movq	-256(%rbp), %r12
	leaq	-208(%rbp), %rdx
	leaq	-144(%rbp), %rbx
	movq	%rdx, (%rax)
	movl	$10, %eax
	subq	%rcx, %r12
	movq	%rbx, -160(%rbp)
	cmpq	$10, %r12
	cmovg	%rax, %r12
	movq	%rcx, %rax
	addq	%r12, %rax
	je	.L478
	testq	%rcx, %rcx
	je	.L572
.L478:
	movq	%r12, -216(%rbp)
	cmpq	$15, %r12
	ja	.L573
	cmpq	$1, %r12
	jne	.L481
	movq	-232(%rbp), %rax
	leaq	-160(%rbp), %r13
	movzbl	(%rax), %eax
	movb	%al, -144(%rbp)
	movq	%rbx, %rax
.L482:
	movq	%r12, -152(%rbp)
	movq	%r13, %rsi
	movb	$0, (%rax,%r12)
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
.LEHB33:
	call	_ZN2v88internal6torque18StringLiteralQuoteERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE33:
	movl	$27, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	.LC13(%rip), %rcx
.LEHB34:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE34:
	leaq	-80(%rbp), %r13
	leaq	16(%rax), %rdx
	movq	%r13, -96(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L574
	movq	%rcx, -96(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L490:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	leaq	-96(%rbp), %rdi
	movq	%rcx, -88(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
.LEHB35:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE35:
.L571:
	call	__stack_chk_fail@PLT
.L569:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -288(%rbp)
	jmp	.L494
.L572:
	leaq	.LC4(%rip), %rdi
.LEHB36:
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L574:
	movdqu	16(%rax), %xmm5
	movaps	%xmm5, -80(%rbp)
	jmp	.L490
.L481:
	movq	%rbx, %rax
	leaq	-160(%rbp), %r13
	testq	%r12, %r12
	je	.L482
.L480:
	cmpq	$8, %r12
	jnb	.L483
	testb	$4, %r12b
	jne	.L575
	testq	%r12, %r12
	je	.L484
	movq	-232(%rbp), %rcx
	movzbl	(%rcx), %edx
	movb	%dl, (%rax)
	testb	$2, %r12b
	jne	.L576
.L484:
	movq	-216(%rbp), %r12
	movq	-160(%rbp), %rax
	jmp	.L482
.L575:
	movq	-232(%rbp), %rdi
	movl	(%rdi), %edx
	movl	%edx, (%rax)
	movl	-4(%rdi,%r12), %edx
	movl	%edx, -4(%rax,%r12)
	jmp	.L484
.L483:
	movq	-232(%rbp), %rdi
	movq	(%rdi), %rdx
	movq	%rdi, %rcx
	movq	%rdx, (%rax)
	movq	-8(%rdi,%r12), %rdx
	movq	%rdx, -8(%rax,%r12)
	leaq	8(%rax), %rdx
	andq	$-8, %rdx
	subq	%rdx, %rax
	addq	%rax, %r12
	subq	%rax, %rcx
	andq	$-8, %r12
	cmpq	$8, %r12
	jb	.L484
	andq	$-8, %r12
	xorl	%eax, %eax
.L487:
	movq	(%rcx,%rax), %rsi
	movq	%rsi, (%rdx,%rax)
	addq	$8, %rax
	cmpq	%r12, %rax
	jb	.L487
	jmp	.L484
.L576:
	movq	-232(%rbp), %rcx
	movzwl	-2(%rcx,%r12), %edx
	movw	%dx, -2(%rax,%r12)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L573:
	leaq	-160(%rbp), %r13
	leaq	-216(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE36:
	movq	-216(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	jmp	.L480
.L532:
	endbr64
	movq	%rax, %r12
	jmp	.L519
.L536:
	endbr64
	movq	%rax, %r12
	jmp	.L512
.L535:
	endbr64
	movq	%rax, %r12
	jmp	.L514
.L533:
	endbr64
	movq	%rax, %r12
	jmp	.L518
.L534:
	endbr64
	movq	%rax, %r12
	jmp	.L516
	.section	.gcc_except_table._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
.LLSDA5813:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5813-.LLSDACSB5813
.LLSDACSB5813:
	.uleb128 .LEHB32-.LFB5813
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L532-.LFB5813
	.uleb128 0
	.uleb128 .LEHB33-.LFB5813
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L534-.LFB5813
	.uleb128 0
	.uleb128 .LEHB34-.LFB5813
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L535-.LFB5813
	.uleb128 0
	.uleb128 .LEHB35-.LFB5813
	.uleb128 .LEHE35-.LEHB35
	.uleb128 .L536-.LFB5813
	.uleb128 0
	.uleb128 .LEHB36-.LFB5813
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L533-.LFB5813
	.uleb128 0
.LLSDACSE5813:
	.section	.text._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5813
	.type	_ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB5813:
.L512:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L514
	call	_ZdlPv@PLT
.L514:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L516
	call	_ZdlPv@PLT
.L516:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L518
	call	_ZdlPv@PLT
.L518:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-184(%rbp), %rdx
	movq	%rdx, (%rax)
.L519:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L520
	call	_ZdlPv@PLT
.L520:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L521
	call	_ZdlPv@PLT
.L521:
	movq	%r12, %rdi
.LEHB37:
	call	_Unwind_Resume@PLT
.LEHE37:
	.cfi_endproc
.LFE5813:
	.section	.gcc_except_table._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LLSDAC5813:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5813-.LLSDACSBC5813
.LLSDACSBC5813:
	.uleb128 .LEHB37-.LCOLDB14
	.uleb128 .LEHE37-.LEHB37
	.uleb128 0
	.uleb128 0
.LLSDACSEC5813:
	.section	.text.unlikely._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE14:
	.section	.text._ZN2v88internal6torque5Lexer8RunLexerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE14:
	.section	.text._ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB8040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movabsq	$-6148914691236517205, %rsi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r14
	movabsq	$192153584101141162, %rdi
	movq	%rcx, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	imulq	%rsi, %rax
	cmpq	%rdi, %rax
	je	.L594
	movq	%r12, %rsi
	subq	%r14, %rsi
	testq	%rax, %rax
	je	.L586
	movabsq	$9223372036854775776, %r13
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L595
.L579:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r13), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	48(%rbx), %r13
.L585:
	movdqu	(%rdx), %xmm4
	movdqu	16(%rdx), %xmm5
	movdqu	32(%rdx), %xmm6
	movups	%xmm4, (%rbx,%rsi)
	movups	%xmm5, 16(%rbx,%rsi)
	movups	%xmm6, 32(%rbx,%rsi)
	cmpq	%r14, %r12
	je	.L581
	movq	%rbx, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L582:
	movdqu	(%rax), %xmm1
	addq	$48, %rax
	addq	$48, %rdx
	movups	%xmm1, -48(%rdx)
	movdqu	-32(%rax), %xmm2
	movups	%xmm2, -32(%rdx)
	movdqu	-16(%rax), %xmm3
	movups	%xmm3, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L582
	movabsq	$768614336404564651, %rdx
	leaq	-48(%r12), %rax
	subq	%r14, %rax
	shrq	$4, %rax
	imulq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	andq	%rdx, %rax
	leaq	6(%rax,%rax,2), %r8
	salq	$4, %r8
	leaq	(%rbx,%r8), %r13
.L581:
	cmpq	%rcx, %r12
	je	.L583
	subq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movabsq	$768614336404564651, %rdx
	leaq	-48(%rcx), %rax
	shrq	$4, %rax
	imulq	%rdx, %rax
	movabsq	$1152921504606846975, %rdx
	andq	%rdx, %rax
	leaq	3(%rax,%rax,2), %rdx
	salq	$4, %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %r13
.L583:
	testq	%r14, %r14
	je	.L584
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L584:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L595:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L580
	movq	$0, -56(%rbp)
	movl	$48, %r13d
	xorl	%ebx, %ebx
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L586:
	movl	$48, %r13d
	jmp	.L579
.L580:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	imulq	$48, %rdi, %r13
	jmp	.L579
.L594:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8040:
	.size	_ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB8158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L597
	testq	%rsi, %rsi
	je	.L613
.L597:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L614
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L600
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L601:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L615
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L601
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L614:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L599:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L601
.L613:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L615:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8158:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.section	.rodata._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"Ambiguous grammer rules for \""
.LC16:
	.string	"\":\n   "
.LC17:
	.string	"\nvs\n   "
.LC18:
	.string	"  ...\nvs\n   "
.LC19:
	.string	"  ..."
	.section	.text.unlikely._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE,"ax",@progbits
	.align 2
.LCOLDB20:
	.section	.text._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE,"ax",@progbits
.LHOTB20:
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE
	.type	_ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE, @function
_ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE:
.LFB5812:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5812
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$568, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	cmpq	%rax, 40(%rdi)
	jne	.L655
	movq	32(%rsi), %rax
	cmpq	%rax, 32(%rdi)
	jne	.L656
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L657
	addq	$568, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L655:
	.cfi_restore_state
	leaq	-448(%rbp), %r15
	leaq	-432(%rbp), %rbx
	movq	%r15, %rdi
.LEHB38:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE38:
	movl	$29, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
.LEHB39:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r12), %rsi
	leaq	-592(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE
	movq	-584(%rbp), %rdx
	movq	-592(%rbp), %rsi
	leaq	-528(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	%rax, -600(%rbp)
	movq	%rax, -544(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
.LEHE39:
	movq	-536(%rbp), %rdx
	movq	-544(%rbp), %rsi
	movq	%rbx, %rdi
.LEHB40:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r12), %rsi
	leaq	-512(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
.LEHE40:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rsi
	movq	%rbx, %rdi
.LEHB41:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$7, %edx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-480(%rbp), %r12
	movq	40(%r13), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
.LEHE41:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%rbx, %rdi
.LEHB42:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE42:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L618
	call	_ZdlPv@PLT
.L618:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	-544(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L620
	call	_ZdlPv@PLT
.L620:
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
.LEHB43:
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE43:
	movq	%r12, %rdi
.LEHB44:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE44:
.L657:
	call	__stack_chk_fail@PLT
.L656:
	leaq	-448(%rbp), %r15
	leaq	-432(%rbp), %rbx
	movq	%r15, %rdi
.LEHB45:
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1Ev@PLT
.LEHE45:
	movl	$29, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
.LEHB46:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-592(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZNK2v88internal6torque4Item15GetMatchedInputERKNS1_11LexerResultE
	movq	-584(%rbp), %rdx
	movq	-592(%rbp), %rsi
	leaq	-528(%rbp), %rax
	leaq	-544(%rbp), %rdi
	movq	%rax, -600(%rbp)
	movq	%rax, -544(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
.LEHE46:
	movq	-536(%rbp), %rdx
	movq	-544(%rbp), %rsi
	movq	%rbx, %rdi
.LEHB47:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$6, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-512(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
.LEHE47:
	movq	-504(%rbp), %rdx
	movq	-512(%rbp), %rsi
	movq	%rbx, %rdi
.LEHB48:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$12, %edx
	leaq	.LC18(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-480(%rbp), %r12
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal6torque4Item15SplitByChildrenB5cxx11ERKNS1_11LexerResultE
.LEHE48:
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%rbx, %rdi
.LEHB49:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rax, %rdi
	leaq	.LC19(%rip), %rsi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
.LEHE49:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rbx
	cmpq	%rbx, %rdi
	je	.L622
	call	_ZdlPv@PLT
.L622:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L623
	call	_ZdlPv@PLT
.L623:
	movq	-544(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L624
	call	_ZdlPv@PLT
.L624:
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
.LEHB50:
	call	_ZNKSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE3strEv@PLT
.LEHE50:
	movq	%r12, %rdi
.LEHB51:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE51:
.L652:
	endbr64
	movq	%rax, %r12
	jmp	.L634
.L653:
	endbr64
	movq	%rax, %r12
	jmp	.L641
.L651:
	endbr64
	movq	%rax, %r12
	jmp	.L636
.L650:
	endbr64
	movq	%rax, %r12
	jmp	.L638
.L649:
	endbr64
	movq	%rax, %r12
	jmp	.L640
.L648:
	endbr64
	movq	%rax, %r12
	jmp	.L632
.L647:
	endbr64
	movq	%rax, %r12
	jmp	.L625
.L645:
	endbr64
	movq	%rax, %r12
	jmp	.L629
.L646:
	endbr64
	movq	%rax, %r12
	jmp	.L627
.L644:
	endbr64
	movq	%rax, %r12
	jmp	.L631
	.section	.gcc_except_table._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE,"a",@progbits
.LLSDA5812:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5812-.LLSDACSB5812
.LLSDACSB5812:
	.uleb128 .LEHB38-.LFB5812
	.uleb128 .LEHE38-.LEHB38
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB39-.LFB5812
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L644-.LFB5812
	.uleb128 0
	.uleb128 .LEHB40-.LFB5812
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L645-.LFB5812
	.uleb128 0
	.uleb128 .LEHB41-.LFB5812
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L646-.LFB5812
	.uleb128 0
	.uleb128 .LEHB42-.LFB5812
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L647-.LFB5812
	.uleb128 0
	.uleb128 .LEHB43-.LFB5812
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L644-.LFB5812
	.uleb128 0
	.uleb128 .LEHB44-.LFB5812
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L648-.LFB5812
	.uleb128 0
	.uleb128 .LEHB45-.LFB5812
	.uleb128 .LEHE45-.LEHB45
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB46-.LFB5812
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L649-.LFB5812
	.uleb128 0
	.uleb128 .LEHB47-.LFB5812
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L650-.LFB5812
	.uleb128 0
	.uleb128 .LEHB48-.LFB5812
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L651-.LFB5812
	.uleb128 0
	.uleb128 .LEHB49-.LFB5812
	.uleb128 .LEHE49-.LEHB49
	.uleb128 .L652-.LFB5812
	.uleb128 0
	.uleb128 .LEHB50-.LFB5812
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L649-.LFB5812
	.uleb128 0
	.uleb128 .LEHB51-.LFB5812
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L653-.LFB5812
	.uleb128 0
.LLSDACSE5812:
	.section	.text._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE
	.cfi_endproc
	.section	.text.unlikely._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5812
	.type	_ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE.cold, @function
_ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE.cold:
.LFSB5812:
.L634:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L636
	call	_ZdlPv@PLT
.L636:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L638
	call	_ZdlPv@PLT
.L638:
	movq	-544(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L640
	call	_ZdlPv@PLT
.L640:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
.LEHB52:
	call	_Unwind_Resume@PLT
.L641:
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L640
	call	_ZdlPv@PLT
	jmp	.L640
.L632:
	movq	-480(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L631
	call	_ZdlPv@PLT
.L631:
	movq	%r15, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE52:
.L625:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L627
	call	_ZdlPv@PLT
.L627:
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L629
	call	_ZdlPv@PLT
.L629:
	movq	-544(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L631
	call	_ZdlPv@PLT
	jmp	.L631
	.cfi_endproc
.LFE5812:
	.section	.gcc_except_table._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE
.LLSDAC5812:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC5812-.LLSDACSBC5812
.LLSDACSBC5812:
	.uleb128 .LEHB52-.LCOLDB20
	.uleb128 .LEHE52-.LEHB52
	.uleb128 0
	.uleb128 0
.LLSDACSEC5812:
	.section	.text.unlikely._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE
	.section	.text._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE
	.size	_ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE, .-_ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE
	.section	.text.unlikely._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE
	.size	_ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE.cold, .-_ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE.cold
.LCOLDE20:
	.section	.text._ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE
.LHOTE20:
	.section	.text._ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm:
.LFB8525:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8525
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L680
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L681
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB53:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L660:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L662
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L664:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L665:
	testq	%rsi, %rsi
	je	.L662
.L663:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	72(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L664
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L669
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L663
	.p2align 4,,10
	.p2align 3
.L662:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L666
	call	_ZdlPv@PLT
.L666:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L680:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L660
.L681:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE53:
.L670:
	endbr64
	movq	%rax, %rdi
.L667:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB54:
	call	__cxa_rethrow@PLT
.LEHE54:
.L671:
	endbr64
	movq	%rax, %r12
.L668:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB55:
	call	_Unwind_Resume@PLT
.LEHE55:
	.cfi_endproc
.LFE8525:
	.section	.gcc_except_table._ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA8525:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT8525-.LLSDATTD8525
.LLSDATTD8525:
	.byte	0x1
	.uleb128 .LLSDACSE8525-.LLSDACSB8525
.LLSDACSB8525:
	.uleb128 .LEHB53-.LFB8525
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L670-.LFB8525
	.uleb128 0x1
	.uleb128 .LEHB54-.LFB8525
	.uleb128 .LEHE54-.LEHB54
	.uleb128 .L671-.LFB8525
	.uleb128 0
	.uleb128 .LEHB55-.LFB8525
	.uleb128 .LEHE55-.LEHB55
	.uleb128 0
	.uleb128 0
.LLSDACSE8525:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT8525:
	.section	.text._ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB9202:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L690
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L684:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L684
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE9202:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm
	.type	_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm, @function
_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm:
.LFB8106:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8106
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB56:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE56:
	testb	%al, %al
	je	.L694
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB57:
	call	_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
.LEHE57:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L694:
	movq	%r14, 72(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L695
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L696:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L708
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L697
	movq	72(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L697:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L696
.L708:
	call	__stack_chk_fail@PLT
.L701:
	endbr64
	movq	%rax, %rdi
.L698:
	call	__cxa_begin_catch@PLT
	movq	40(%r12), %rsi
	leaq	24(%r12), %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB58:
	call	__cxa_rethrow@PLT
.LEHE58:
.L702:
	endbr64
	movq	%rax, %r12
.L699:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB59:
	call	_Unwind_Resume@PLT
.LEHE59:
	.cfi_endproc
.LFE8106:
	.section	.gcc_except_table._ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,"aG",@progbits,_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,comdat
	.align 4
.LLSDA8106:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT8106-.LLSDATTD8106
.LLSDATTD8106:
	.byte	0x1
	.uleb128 .LLSDACSE8106-.LLSDACSB8106
.LLSDACSB8106:
	.uleb128 .LEHB56-.LFB8106
	.uleb128 .LEHE56-.LEHB56
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB57-.LFB8106
	.uleb128 .LEHE57-.LEHB57
	.uleb128 .L701-.LFB8106
	.uleb128 0x1
	.uleb128 .LEHB58-.LFB8106
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L702-.LFB8106
	.uleb128 0
	.uleb128 .LEHB59-.LFB8106
	.uleb128 .LEHE59-.LEHB59
	.uleb128 0
	.uleb128 0
.LLSDACSE8106:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT8106:
	.section	.text._ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm,comdat
	.size	_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm, .-_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm
	.section	.text._ZNSt8__detail9_Map_baseISt4pairImPN2v88internal6torque6SymbolEES1_IKS7_St3setIPKNS4_4ItemESt4lessISC_ESaISC_EEESaISH_ENS_10_Select1stESt8equal_toIS7_ENS2_4base4hashIS7_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS7_,"axG",@progbits,_ZNSt8__detail9_Map_baseISt4pairImPN2v88internal6torque6SymbolEES1_IKS7_St3setIPKNS4_4ItemESt4lessISC_ESaISC_EEESaISH_ENS_10_Select1stESt8equal_toIS7_ENS2_4base4hashIS7_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseISt4pairImPN2v88internal6torque6SymbolEES1_IKS7_St3setIPKNS4_4ItemESt4lessISC_ESaISC_EEESaISH_ENS_10_Select1stESt8equal_toIS7_ENS2_4base4hashIS7_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS7_
	.type	_ZNSt8__detail9_Map_baseISt4pairImPN2v88internal6torque6SymbolEES1_IKS7_St3setIPKNS4_4ItemESt4lessISC_ESaISC_EEESaISH_ENS_10_Select1stESt8equal_toIS7_ENS2_4base4hashIS7_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS7_, @function
_ZNSt8__detail9_Map_baseISt4pairImPN2v88internal6torque6SymbolEES1_IKS7_St3setIPKNS4_4ItemESt4lessISC_ESaISC_EEESaISH_ENS_10_Select1stESt8equal_toIS7_ENS2_4base4hashIS7_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS7_:
.LFB7560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%rsi, %rbx
	call	_ZN2v84base10hash_valueEm@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r12
	call	_ZN2v84base10hash_valueEm@PLT
	xorl	%edi, %edi
	movq	%rax, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	8(%r13), %rsi
	xorl	%edx, %edx
	movq	%rax, %r12
	divq	%rsi
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L710
	movq	(%rax), %rcx
	movq	72(%rcx), %rdi
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L711:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L710
	movq	72(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L710
.L713:
	cmpq	%r12, %rdi
	jne	.L711
	movq	8(%rcx), %rax
	cmpq	%rax, (%rbx)
	jne	.L711
	movq	16(%rcx), %rax
	cmpq	%rax, 8(%rbx)
	jne	.L711
	popq	%rbx
	leaq	24(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L710:
	.cfi_restore_state
	movl	$80, %edi
	call	_Znwm@PLT
	movdqu	(%rbx), %xmm0
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	leaq	32(%rax), %rax
	movq	%r13, %rdi
	movups	%xmm0, -24(%rax)
	movl	$1, %r8d
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 48(%rcx)
	movq	%rax, 56(%rcx)
	movq	$0, 64(%rcx)
	call	_ZNSt10_HashtableISt4pairImPN2v88internal6torque6SymbolEES0_IKS6_St3setIPKNS3_4ItemESt4lessISB_ESaISB_EEESaISG_ENSt8__detail10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENSI_18_Mod_range_hashingENSI_20_Default_ranged_hashENSI_20_Prime_rehash_policyENSI_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSI_10_Hash_nodeISG_Lb1EEEm
	popq	%rbx
	popq	%r12
	addq	$24, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7560:
	.size	_ZNSt8__detail9_Map_baseISt4pairImPN2v88internal6torque6SymbolEES1_IKS7_St3setIPKNS4_4ItemESt4lessISC_ESaISC_EEESaISH_ENS_10_Select1stESt8equal_toIS7_ENS2_4base4hashIS7_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS7_, .-_ZNSt8__detail9_Map_baseISt4pairImPN2v88internal6torque6SymbolEES1_IKS7_St3setIPKNS4_4ItemESt4lessISC_ESaISC_EEESaISH_ENS_10_Select1stESt8equal_toIS7_ENS2_4base4hashIS7_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS7_
	.section	.rodata._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"basic_string::append"
.LC24:
	.string	"unexpected token \""
.LC25:
	.string	"\""
.LC26:
	.string	"unexpected end of input"
.LC27:
	.string	"Parser Error: "
	.section	.text.unlikely._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE,"ax",@progbits
.LCOLDB28:
	.section	.text._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE,"ax",@progbits
.LHOTB28:
	.p2align 4
	.globl	_ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE
	.type	_ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE, @function
_ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE:
.LFB5827:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA5827
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$520, %rsp
	movq	%rsi, -480(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -432(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
.LEHB60:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movl	(%rax), %eax
	movq	$0, -332(%rbp)
	movq	$0, -324(%rbp)
	movl	%eax, -336(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
.LEHE60:
	leaq	-336(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	%rdx, (%rax)
	leaq	-208(%rbp), %rax
	movq	%rax, -560(%rbp)
	movq	%rax, -256(%rbp)
	movq	$1, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movl	$0x3f800000, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
.LEHB61:
	call	_Znwm@PLT
.LEHE61:
	movq	%rbx, (%rax)
	movl	$40, %edi
	movq	%rax, %r12
.LEHB62:
	call	_Znwm@PLT
.LEHE62:
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	%rax, %r13
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
.LEHB63:
	call	_Znwm@PLT
.LEHE63:
	movq	(%r12), %rcx
	leaq	8(%rax), %rdx
	movq	%rax, 8(%r13)
	movq	%rdx, 24(%r13)
	movq	-360(%rbp), %rsi
	movq	%rcx, (%rax)
	leaq	_ZN2v88internal6torque13DefaultActionEPNS1_19ParseResultIteratorE(%rip), %rax
	movq	%rdx, 16(%r13)
	movq	%rax, 32(%r13)
	movq	%r13, -304(%rbp)
	cmpq	-352(%rbp), %rsi
	je	.L1004
	movq	$0, -304(%rbp)
	leaq	-368(%rbp), %rax
	movq	%r13, (%rsi)
	addq	$8, -360(%rbp)
	movq	%rax, -552(%rbp)
.L731:
	movq	-304(%rbp), %r13
	testq	%r13, %r13
	je	.L732
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L733
	call	_ZdlPv@PLT
.L733:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L732:
	movq	-360(%rbp), %rax
	movq	-552(%rbp), %rdx
	movq	%r12, %rdi
	movq	-8(%rax), %rax
	movq	%rdx, (%rax)
	call	_ZdlPv@PLT
	movq	-368(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-424(%rbp), %rsi
	movq	(%rax), %rax
	movups	%xmm0, -296(%rbp)
	movq	$0, -280(%rbp)
	movq	%rax, -304(%rbp)
	movaps	%xmm0, -272(%rbp)
	cmpq	-416(%rbp), %rsi
	je	.L1005
	movdqa	-304(%rbp), %xmm4
	movups	%xmm4, (%rsi)
	movq	-424(%rbp), %rax
	movdqa	-288(%rbp), %xmm4
	leaq	48(%rax), %rdx
	movups	%xmm4, 16(%rsi)
	movdqa	-272(%rbp), %xmm4
	movq	%rdx, -424(%rbp)
	movups	%xmm4, 32(%rsi)
.L736:
	movq	-480(%rbp), %rsi
	xorl	%r15d, %r15d
	movq	$0, -512(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -536(%rbp)
	movq	8(%rsi), %rax
	subq	(%rsi), %rax
	leaq	16(%r14), %rsi
	sarq	$3, %rax
	movq	%rsi, -520(%rbp)
	movq	%rax, -544(%rbp)
	movq	-432(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L799:
	leaq	(%r15,%r15,4), %rcx
	movq	%rdx, %r12
	leaq	0(,%rcx,8), %rsi
	movq	%rsi, -496(%rbp)
	leaq	0(,%r15,8), %rsi
	movq	%rsi, -528(%rbp)
	.p2align 4,,10
	.p2align 3
.L739:
	leaq	32(%r14), %rdx
	movq	%rdx, -504(%rbp)
	cmpq	%r12, %rax
	je	.L740
.L1017:
	movq	-48(%r12), %rdi
.LEHB64:
	call	_ZN2v84base10hash_valueEm@PLT
	movq	-40(%r12), %rdi
	movq	%rax, %r13
	call	_ZN2v84base10hash_valueEm@PLT
	movq	-24(%r12), %rsi
	movq	-32(%r12), %rdi
	movq	%rax, %rbx
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	8(%r14), %rsi
	xorl	%edx, %edx
	movq	%rax, %rbx
	divq	%rsi
	leaq	0(,%rdx,8), %rax
	movq	%rdx, %rdi
	movq	%rax, -440(%rbp)
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L741
	movq	(%rax), %r13
	movq	56(%r13), %rcx
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L742:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L741
	movq	56(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L741
.L744:
	cmpq	%rbx, %rcx
	jne	.L742
	movq	8(%r13), %rax
	cmpq	%rax, -48(%r12)
	jne	.L742
	movq	16(%r13), %rax
	cmpq	%rax, -40(%r12)
	jne	.L742
	movq	24(%r13), %rax
	cmpq	%rax, -32(%r12)
	jne	.L742
	movq	32(%r13), %rax
	cmpq	%rax, -24(%r12)
	jne	.L742
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L743:
	movq	-480(%rbp), %rsi
	leaq	8(%r13), %rax
	movq	%rax, -472(%rbp)
	movq	-496(%rbp), %rax
	addq	24(%rsi), %rax
	movl	16(%rax), %edx
	movl	20(%rax), %esi
	movl	24(%rax), %ecx
	movl	28(%rax), %edi
	movl	%edx, -464(%rbp)
	movl	32(%rax), %r12d
	movl	%esi, -448(%rbp)
	movl	%ecx, -456(%rbp)
	movl	%edi, -440(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movd	-456(%rbp), %xmm1
	movd	-440(%rbp), %xmm6
	movd	-464(%rbp), %xmm0
	movd	-448(%rbp), %xmm7
	movq	(%rax), %rax
	punpckldq	%xmm6, %xmm1
	punpckldq	%xmm7, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movl	%r12d, 16(%rax)
	movups	%xmm0, (%rax)
	testb	%bl, %bl
	je	.L1006
	subq	$48, -424(%rbp)
	movq	-512(%rbp), %rdx
	cmpq	%rdx, -488(%rbp)
	je	.L1007
	movq	-488(%rbp), %rax
	leaq	8(%r13), %rdx
	movq	%rdx, (%rax)
	addq	$8, %rax
	movq	%rax, -488(%rbp)
.L763:
	movq	8(%r13), %rdx
	movq	16(%r13), %rcx
	movq	8(%rdx), %rsi
	movq	16(%rdx), %rax
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rcx
	je	.L1008
	movq	-480(%rbp), %rax
	movq	(%rsi,%rcx,8), %rdi
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	%rdi, -464(%rbp)
	movq	%rax, -440(%rbp)
	subq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%r15, %rax
	jbe	.L776
	movq	-528(%rbp), %rax
	cmpq	%rdi, (%rsi,%rax)
	je	.L1009
.L776:
	movq	-464(%rbp), %rax
	movq	(%rax), %rsi
	cmpq	%rsi, 8(%rax)
	je	.L1000
	movq	%rax, -296(%rbp)
	leaq	-304(%rbp), %rax
	leaq	-256(%rbp), %rdi
	movq	%rax, %rsi
	movq	%r15, -304(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZNSt8__detail9_Map_baseISt4pairImPN2v88internal6torque6SymbolEES1_IKS7_St3setIPKNS4_4ItemESt4lessISC_ESaISC_EEESaISH_ENS_10_Select1stESt8equal_toIS7_ENS2_4base4hashIS7_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS7_
	movq	16(%rax), %r12
	movq	%rax, -448(%rbp)
	addq	$8, %rax
	leaq	8(%r13), %rsi
	movq	%rax, -440(%rbp)
	testq	%r12, %r12
	jne	.L781
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	16(%r12), %rax
	movl	%ebx, %ecx
	testq	%rax, %rax
	je	.L782
.L1012:
	movq	%rax, %r12
.L781:
	movq	32(%r12), %rdx
	cmpq	%rsi, %rdx
	ja	.L1011
	movq	24(%r12), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	jne	.L1012
.L782:
	testb	%cl, %cl
	jne	.L1013
	cmpq	-472(%rbp), %rdx
	jnb	.L867
.L866:
	cmpq	-440(%rbp), %r12
	jne	.L1014
.L788:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	-472(%rbp), %rax
	movzbl	%bl, %edi
	movq	%r12, %rdx
	movq	-440(%rbp), %rcx
	movq	%rax, 32(%rsi)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	movq	-448(%rbp), %rax
	addq	$1, 40(%rax)
.L867:
	movq	-464(%rbp), %rax
	movq	(%rax), %rdx
	cmpq	%rdx, 8(%rax)
	je	.L1000
	movq	$0, -440(%rbp)
	.p2align 4,,10
	.p2align 3
.L789:
	movq	-440(%rbp), %rax
	movq	(%rdx,%rax,8), %r12
	movq	16(%r12), %rbx
	movq	%r12, %rdi
	subq	8(%r12), %rbx
	sarq	$3, %rbx
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%rbx, %rdi
	movq	%rax, -448(%rbp)
	call	_ZN2v84base10hash_valueEm@PLT
	movq	%r15, %rsi
	movq	%r15, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-456(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	-448(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	8(%r14), %r8
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	-424(%rbp), %r9
	movq	-416(%rbp), %r11
	divq	%r8
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L790
	movq	(%rax), %rcx
	movq	56(%rcx), %rdi
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L791:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L790
	movq	56(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L790
.L793:
	cmpq	%rsi, %rdi
	jne	.L791
	cmpq	8(%rcx), %r12
	jne	.L791
	cmpq	16(%rcx), %rbx
	jne	.L791
	cmpq	%r15, 24(%rcx)
	jne	.L791
	cmpq	%r15, 32(%rcx)
	jne	.L791
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	addq	$8, %rcx
	movq	8(%r13), %rsi
	movq	%r15, -280(%rbp)
	addq	$1, %rax
	movq	%rdx, -288(%rbp)
	movq	%rax, -296(%rbp)
	movq	-472(%rbp), %rax
	movq	%rsi, -304(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rcx, -264(%rbp)
	cmpq	%r9, %r11
	je	.L986
	movdqa	-304(%rbp), %xmm2
	movups	%xmm2, (%r9)
	movdqa	-288(%rbp), %xmm3
	addq	$48, -424(%rbp)
	movups	%xmm3, 16(%r9)
	movdqa	-272(%rbp), %xmm4
	movups	%xmm4, 32(%r9)
.L797:
	movq	-464(%rbp), %rax
	addq	$1, -440(%rbp)
	movq	-440(%rbp), %rbx
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	%rax, -448(%rbp)
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L789
.L1000:
	movq	-424(%rbp), %r12
.L772:
	movq	-432(%rbp), %rax
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L741:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	%rax, %r13
	movq	24(%r14), %rdx
	movq	8(%r14), %rsi
	movl	$1, %ecx
	movq	$0, (%rax)
	movdqu	-48(%r12), %xmm6
	movq	-504(%rbp), %rdi
	movups	%xmm6, 8(%rax)
	movdqu	-32(%r12), %xmm7
	movups	%xmm7, 24(%rax)
	movdqu	-16(%r12), %xmm2
	movups	%xmm2, 40(%rax)
	movq	40(%r14), %rax
	movq	%rax, -448(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE64:
	movq	%rdx, %r12
	testb	%al, %al
	je	.L745
	cmpq	$1, %rdx
	je	.L1015
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1016
	leaq	0(,%rdx,8), %rax
	movq	%rax, %rdi
	movq	%rax, -440(%rbp)
.LEHB65:
	call	_Znwm@PLT
.LEHE65:
	movq	-440(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r14), %r8
	movq	%rax, %r10
.L747:
	movq	16(%r14), %rsi
	movq	$0, 16(%r14)
	testq	%rsi, %rsi
	je	.L749
	movq	-520(%rbp), %r9
	xorl	%edi, %edi
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L751:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L752:
	testq	%rsi, %rsi
	je	.L749
.L750:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	56(%rcx), %rax
	divq	%r12
	leaq	(%r10,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L751
	movq	16(%r14), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r14)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L871
	movq	%rcx, (%r10,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L750
	.p2align 4,,10
	.p2align 3
.L749:
	movq	(%r14), %rdi
	cmpq	%rdi, %r8
	je	.L753
	movq	%r10, -440(%rbp)
	call	_ZdlPv@PLT
	movq	-440(%rbp), %r10
.L753:
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%r14)
	divq	%r12
	movq	%r10, (%r14)
	leaq	0(,%rdx,8), %rax
	movq	%rax, -440(%rbp)
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	-424(%rbp), %rax
	movq	-480(%rbp), %rdx
	movq	-472(%rbp), %rdi
	leaq	-48(%rax), %rsi
.LEHB66:
	call	_ZNK2v88internal6torque4Item14CheckAmbiguityERKS2_RKNS1_11LexerResultE
	movq	-424(%rbp), %rax
	leaq	-48(%rax), %r12
	movq	-432(%rbp), %rax
	movq	%r12, -424(%rbp)
	cmpq	%r12, %rax
	jne	.L1017
.L740:
	movq	-416(%rbp), %rcx
	movq	-384(%rbp), %rsi
	addq	$1, %r15
	movq	%r12, %xmm0
	movdqa	-400(%rbp), %xmm7
	punpcklqdq	%xmm0, %xmm0
	movq	-400(%rbp), %rax
	movq	-392(%rbp), %rdx
	movq	%rsi, -416(%rbp)
	movq	%rcx, -384(%rbp)
	movaps	%xmm7, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	cmpq	-544(%rbp), %r15
	jbe	.L799
	movq	-368(%rbp), %rax
	movdqa	.LC22(%rip), %xmm0
	leaq	-304(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rbx, -504(%rbp)
	movq	(%rax), %rax
	movups	%xmm0, -296(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	%rax, -304(%rbp)
	movq	-544(%rbp), %rax
	movq	%rax, -280(%rbp)
	call	_ZNSt10_HashtableIN2v88internal6torque4ItemES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ENS0_4base4hashIS3_EENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb1ELb1ELb1EEEE4findERKS3_
	testq	%rax, %rax
	je	.L800
	leaq	8(%rax), %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal6torque4Item8ChildrenEv
.LEHE66:
	movq	-304(%rbp), %rdi
	movq	(%rdi), %r13
	call	_ZdlPv@PLT
	movq	-360(%rbp), %rbx
	movq	-368(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L801
	.p2align 4,,10
	.p2align 3
.L804:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L802
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L803
	call	_ZdlPv@PLT
.L803:
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L802:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L804
	movq	-368(%rbp), %r12
.L801:
	testq	%r12, %r12
	je	.L805
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L805:
	movq	-536(%rbp), %rax
	testq	%rax, %rax
	je	.L806
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L806:
	movq	-240(%rbp), %r12
	testq	%r12, %r12
	je	.L811
	.p2align 4,,10
	.p2align 3
.L807:
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	40(%r15), %rbx
	leaq	24(%r15), %r14
	testq	%rbx, %rbx
	je	.L812
.L810:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L810
.L812:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L807
.L811:
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-256(%rbp), %rdi
	movq	$0, -232(%rbp)
	movq	$0, -240(%rbp)
	cmpq	-560(%rbp), %rdi
	je	.L808
	call	_ZdlPv@PLT
.L808:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-312(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L813
	call	_ZdlPv@PLT
.L813:
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L723
	call	_ZdlPv@PLT
.L723:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1018
	addq	$520, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	.cfi_restore_state
	movq	(%r14), %r10
.L754:
	movq	-440(%rbp), %rax
	movq	%rbx, 56(%r13)
	addq	%r10, %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L757
	movq	(%rdx), %rdx
	movq	%rdx, 0(%r13)
	movq	(%rax), %rax
	movq	%r13, (%rax)
.L758:
	addq	$1, 24(%r14)
	movl	$1, %ebx
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L790:
	pxor	%xmm5, %xmm5
	movq	%r12, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	%r15, -288(%rbp)
	movq	%r15, -280(%rbp)
	movaps	%xmm5, -272(%rbp)
	cmpq	%r9, %r11
	je	.L986
	movdqa	-304(%rbp), %xmm6
	movups	%xmm6, (%r9)
	movdqa	-288(%rbp), %xmm7
	addq	$48, -424(%rbp)
	movups	%xmm7, 16(%r9)
	movdqa	-272(%rbp), %xmm5
	movups	%xmm5, 32(%r9)
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L986:
	movq	-504(%rbp), %rdx
	leaq	-432(%rbp), %rdi
	movq	%r11, %rsi
.LEHB67:
	call	_ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L871:
	movq	%rdx, %rdi
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L757:
	movq	16(%r14), %rdx
	movq	%r13, 16(%r14)
	movq	%rdx, 0(%r13)
	testq	%rdx, %rdx
	je	.L759
	movq	56(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r14)
	movq	-440(%rbp), %rax
	movq	%r13, (%r10,%rdx,8)
	addq	(%r14), %rax
.L759:
	movq	-520(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L1013:
	movq	-448(%rbp), %rax
	cmpq	24(%rax), %r12
	je	.L866
.L787:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-472(%rbp), %rdx
	cmpq	32(%rax), %rdx
	jbe	.L867
	cmpq	-440(%rbp), %r12
	je	.L788
.L1014:
	movq	-472(%rbp), %rax
	cmpq	32(%r12), %rax
	setb	%bl
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	-488(%rbp), %r12
	subq	-536(%rbp), %r12
	movabsq	$1152921504606846975, %rdx
	movq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	je	.L1019
	testq	%rax, %rax
	je	.L1020
	leaq	(%rax,%rax), %rdx
	cmpq	%rax, %rdx
	jb	.L1021
	movq	$0, -512(%rbp)
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	jne	.L1022
.L767:
	leaq	8(%r13), %rax
	movq	%rax, (%rcx,%r12)
	leaq	8(%rcx,%r12), %rax
	movq	%rax, -488(%rbp)
	testq	%r12, %r12
	jg	.L1023
	cmpq	$0, -536(%rbp)
	jne	.L769
.L770:
	movq	%rcx, -536(%rbp)
	jmp	.L763
.L1023:
	movq	-536(%rbp), %rsi
	movq	%rcx, %rdi
	movq	%r12, %rdx
	call	memmove@PLT
	movq	%rax, %rcx
.L769:
	movq	-536(%rbp), %rdi
	movq	%rcx, -440(%rbp)
	call	_ZdlPv@PLT
	movq	-440(%rbp), %rcx
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	(%rdx), %rax
	movq	24(%r13), %rcx
	leaq	-256(%rbp), %rdi
	movq	%rax, -296(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, %rsi
	movq	%rcx, -304(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZNSt8__detail9_Map_baseISt4pairImPN2v88internal6torque6SymbolEES1_IKS7_St3setIPKNS4_4ItemESt4lessISC_ESaISC_EEESaISH_ENS_10_Select1stESt8equal_toIS7_ENS2_4base4hashIS7_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS7_
	movq	24(%rax), %r13
	leaq	8(%rax), %rbx
	movq	-424(%rbp), %r12
	cmpq	%r13, %rbx
	je	.L772
	leaq	-432(%rbp), %rax
	movq	%rax, -440(%rbp)
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L1024:
	movdqa	-304(%rbp), %xmm3
	movups	%xmm3, (%r12)
	movdqa	-288(%rbp), %xmm4
	movq	-424(%rbp), %rax
	movups	%xmm4, 16(%r12)
	movdqa	-272(%rbp), %xmm6
	movups	%xmm6, 32(%r12)
	leaq	48(%rax), %r12
	movq	%r12, -424(%rbp)
.L774:
	movq	%r13, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r13
	cmpq	%rax, %rbx
	je	.L772
.L775:
	movq	32(%r13), %rax
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	leaq	1(%rsi), %rdx
	movq	(%rax), %rsi
	movq	%rax, -272(%rbp)
	movq	-472(%rbp), %rax
	movq	%rdx, -296(%rbp)
	movq	%rsi, -304(%rbp)
	movq	%rcx, -288(%rbp)
	movq	%r15, -280(%rbp)
	movq	%rax, -264(%rbp)
	cmpq	-416(%rbp), %r12
	jne	.L1024
	movq	-504(%rbp), %rdx
	movq	-440(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-424(%rbp), %r12
	jmp	.L774
.L1009:
	movq	24(%r13), %rax
	addq	$1, %rcx
	movq	%rdx, -304(%rbp)
	movq	%rcx, -296(%rbp)
	movq	-392(%rbp), %rsi
	movq	%rax, -288(%rbp)
	leaq	1(%r15), %rax
	movq	%rax, -280(%rbp)
	leaq	8(%r13), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -264(%rbp)
	cmpq	-384(%rbp), %rsi
	je	.L777
	movdqa	-304(%rbp), %xmm3
	movups	%xmm3, (%rsi)
	movdqa	-288(%rbp), %xmm4
	addq	$48, -392(%rbp)
	movups	%xmm4, 16(%rsi)
	movdqa	-272(%rbp), %xmm3
	movups	%xmm3, 32(%rsi)
	jmp	.L776
.L1020:
	movq	$8, -440(%rbp)
.L765:
	movq	-440(%rbp), %rdi
	call	_Znwm@PLT
.LEHE67:
	movq	%rax, %rcx
	movq	-440(%rbp), %rax
	addq	%rcx, %rax
	movq	%rax, -512(%rbp)
	jmp	.L767
.L1010:
	movq	-448(%rbp), %rax
	movq	24(%rax), %r12
	cmpq	%r12, -440(%rbp)
	je	.L866
	movq	-440(%rbp), %r12
	jmp	.L787
.L1015:
	leaq	48(%r14), %r10
	movq	$0, 48(%r14)
	movq	%r10, %r8
	jmp	.L747
.L1021:
	movabsq	$9223372036854775800, %rax
	movq	%rax, -440(%rbp)
	jmp	.L765
.L1022:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, -440(%rbp)
	jmp	.L765
.L800:
	movq	-488(%rbp), %rax
	movq	-480(%rbp), %rsi
	leaq	-176(%rbp), %rbx
	movq	$0, -184(%rbp)
	movq	%rbx, -192(%rbp)
	leaq	-192(%rbp), %rdi
	movq	-8(%rax), %rax
	movb	$0, -176(%rbp)
	movq	24(%rax), %rdx
	movq	8(%rsi), %rax
	subq	(%rsi), %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	jnb	.L815
	movq	24(%rsi), %rax
	leaq	(%rdx,%rdx,4), %rdx
	leaq	-144(%rbp), %r13
	leaq	(%rax,%rdx,8), %rax
	movq	(%rax), %r14
	movq	8(%rax), %r12
	movq	%r13, -160(%rbp)
	testq	%r14, %r14
	jne	.L816
	testq	%r12, %r12
	jne	.L1025
.L816:
	subq	%r14, %r12
	movq	%r12, -304(%rbp)
	cmpq	$15, %r12
	ja	.L1026
	cmpq	$1, %r12
	jne	.L819
	movzbl	(%r14), %eax
	movb	%al, -144(%rbp)
	movq	%r13, %rax
.L820:
	movq	%r12, -152(%rbp)
	leaq	-112(%rbp), %r14
	movb	$0, (%rax,%r12)
	movq	-152(%rbp), %rax
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	movq	%r14, -128(%rbp)
	leaq	18(%rax), %rsi
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
.LEHB68:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-120(%rbp), %rax
	cmpq	$17, %rax
	jbe	.L1027
	movl	$18, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-152(%rbp), %rdx
	movq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE68:
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -120(%rbp)
	je	.L1028
	movl	$1, %edx
	leaq	.LC25(%rip), %rsi
	movq	%r12, %rdi
.LEHB69:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE69:
	leaq	-80(%rbp), %r12
	leaq	16(%rax), %rdx
	movq	%r12, -96(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L1029
	movq	%rcx, -96(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L828:
	movq	8(%rax), %rcx
	movq	%rcx, -88(%rbp)
	movq	%rdx, (%rax)
	movq	-192(%rbp), %rdi
	movq	$0, 8(%rax)
	movb	$0, 16(%rax)
	movq	-96(%rbp), %rax
	cmpq	%r12, %rax
	je	.L1030
	movq	-88(%rbp), %xmm0
	movq	-80(%rbp), %xmm1
	cmpq	%rbx, %rdi
	je	.L1031
	punpcklqdq	%xmm1, %xmm0
	movq	-176(%rbp), %rdx
	movq	%rax, -192(%rbp)
	movups	%xmm0, -184(%rbp)
	testq	%rdi, %rdi
	je	.L834
	movq	%rdi, -96(%rbp)
	movq	%rdx, -80(%rbp)
.L832:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L835
	call	_ZdlPv@PLT
.L835:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L836
	call	_ZdlPv@PLT
.L836:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L838
	call	_ZdlPv@PLT
	jmp	.L838
.L777:
	leaq	-304(%rbp), %rdx
	leaq	-400(%rbp), %rdi
.LEHB70:
	call	_ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE70:
	jmp	.L776
.L815:
	movl	$23, %r8d
	leaq	.LC26(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
.LEHB71:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE71:
	leaq	-80(%rbp), %r12
.L838:
	movq	-184(%rbp), %rax
	leaq	-96(%rbp), %r13
	movq	%r12, -96(%rbp)
	movq	%r13, %rdi
	movq	$0, -88(%rbp)
	leaq	14(%rax), %rsi
	movb	$0, -80(%rbp)
.LEHB72:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	-88(%rbp), %rax
	cmpq	$13, %rax
	jbe	.L1032
	movl	$14, %edx
	leaq	.LC27(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.LEHE72:
	movq	%r13, %rdi
.LEHB73:
	call	_ZN2v88internal6torque11ReportErrorIJNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEEvDpOT_
.LEHE73:
.L1026:
	movq	-504(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	xorl	%edx, %edx
.LEHB74:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE74:
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	-304(%rbp), %rax
	movq	%rax, -144(%rbp)
.L818:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-304(%rbp), %r12
	movq	-160(%rbp), %rax
	jmp	.L820
.L1005:
	leaq	-304(%rbp), %rdx
	leaq	-432(%rbp), %rdi
.LEHB75:
	call	_ZNSt6vectorIN2v88internal6torque4ItemESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
.LEHE75:
	movq	-424(%rbp), %rdx
	jmp	.L736
.L1004:
	leaq	-368(%rbp), %rax
	leaq	-304(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
.LEHB76:
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EE17_M_realloc_insertIJS7_EEEvN9__gnu_cxx17__normal_iteratorIPS7_S9_EEDpOT_
.LEHE76:
	jmp	.L731
.L1030:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L830
	cmpq	$1, %rdx
	je	.L1033
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	-192(%rbp), %rdi
.L830:
	movq	%rdx, -184(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L832
.L1029:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -80(%rbp)
	jmp	.L828
.L819:
	testq	%r12, %r12
	jne	.L1034
	movq	%r13, %rax
	jmp	.L820
.L1031:
	movq	%rax, -192(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -184(%rbp)
.L834:
	movq	%r12, -96(%rbp)
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L832
.L1025:
	leaq	.LC4(%rip), %rdi
.LEHB77:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE77:
	.p2align 4,,10
	.p2align 3
.L1033:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	-192(%rbp), %rdi
	jmp	.L830
.L1028:
	leaq	.LC23(%rip), %rdi
.LEHB78:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE78:
.L1027:
	leaq	.LC23(%rip), %rdi
.LEHB79:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE79:
.L1018:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L1016:
.LEHB80:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE80:
.L1019:
	leaq	.LC2(%rip), %rdi
.LEHB81:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE81:
.L1032:
	leaq	.LC23(%rip), %rdi
.LEHB82:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE82:
.L1034:
	movq	%r13, %rdi
	jmp	.L818
.L884:
	endbr64
	movq	%rax, %r13
	jmp	.L847
.L893:
	endbr64
	movq	%rax, %rbx
	jmp	.L738
.L891:
	endbr64
	movq	%rax, %r13
	jmp	.L841
.L885:
	endbr64
	movq	%rax, %rbx
	jmp	.L734
.L883:
	endbr64
	movq	%rax, %r13
	jmp	.L843
.L882:
	endbr64
	movq	%rax, %r12
	jmp	.L844
.L890:
	endbr64
	movq	%rax, %r12
	jmp	.L844
.L886:
	endbr64
	movq	%rax, %rbx
	jmp	.L729
.L881:
	endbr64
	movq	%rax, %rbx
	jmp	.L726
.L880:
	endbr64
	movq	%rax, %rbx
	leaq	-368(%rbp), %rax
	movq	%rax, -552(%rbp)
	jmp	.L730
.L892:
	endbr64
	movq	%rax, %rbx
	jmp	.L725
.L879:
	endbr64
	movq	%rax, %rbx
	jmp	.L856
.L889:
	endbr64
	movq	%rax, %rdi
	jmp	.L755
	.section	.gcc_except_table._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE,"a",@progbits
	.align 4
.LLSDA5827:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT5827-.LLSDATTD5827
.LLSDATTD5827:
	.byte	0x1
	.uleb128 .LLSDACSE5827-.LLSDACSB5827
.LLSDACSB5827:
	.uleb128 .LEHB60-.LFB5827
	.uleb128 .LEHE60-.LEHB60
	.uleb128 .L879-.LFB5827
	.uleb128 0
	.uleb128 .LEHB61-.LFB5827
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L892-.LFB5827
	.uleb128 0
	.uleb128 .LEHB62-.LFB5827
	.uleb128 .LEHE62-.LEHB62
	.uleb128 .L880-.LFB5827
	.uleb128 0
	.uleb128 .LEHB63-.LFB5827
	.uleb128 .LEHE63-.LEHB63
	.uleb128 .L886-.LFB5827
	.uleb128 0
	.uleb128 .LEHB64-.LFB5827
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L881-.LFB5827
	.uleb128 0
	.uleb128 .LEHB65-.LFB5827
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L889-.LFB5827
	.uleb128 0x1
	.uleb128 .LEHB66-.LFB5827
	.uleb128 .LEHE66-.LEHB66
	.uleb128 .L881-.LFB5827
	.uleb128 0
	.uleb128 .LEHB67-.LFB5827
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L881-.LFB5827
	.uleb128 0
	.uleb128 .LEHB68-.LFB5827
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L890-.LFB5827
	.uleb128 0
	.uleb128 .LEHB69-.LFB5827
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L882-.LFB5827
	.uleb128 0
	.uleb128 .LEHB70-.LFB5827
	.uleb128 .LEHE70-.LEHB70
	.uleb128 .L881-.LFB5827
	.uleb128 0
	.uleb128 .LEHB71-.LFB5827
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L883-.LFB5827
	.uleb128 0
	.uleb128 .LEHB72-.LFB5827
	.uleb128 .LEHE72-.LEHB72
	.uleb128 .L891-.LFB5827
	.uleb128 0
	.uleb128 .LEHB73-.LFB5827
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L884-.LFB5827
	.uleb128 0
	.uleb128 .LEHB74-.LFB5827
	.uleb128 .LEHE74-.LEHB74
	.uleb128 .L883-.LFB5827
	.uleb128 0
	.uleb128 .LEHB75-.LFB5827
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L893-.LFB5827
	.uleb128 0
	.uleb128 .LEHB76-.LFB5827
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L885-.LFB5827
	.uleb128 0
	.uleb128 .LEHB77-.LFB5827
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L883-.LFB5827
	.uleb128 0
	.uleb128 .LEHB78-.LFB5827
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L882-.LFB5827
	.uleb128 0
	.uleb128 .LEHB79-.LFB5827
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L890-.LFB5827
	.uleb128 0
	.uleb128 .LEHB80-.LFB5827
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L889-.LFB5827
	.uleb128 0x1
	.uleb128 .LEHB81-.LFB5827
	.uleb128 .LEHE81-.LEHB81
	.uleb128 .L881-.LFB5827
	.uleb128 0
	.uleb128 .LEHB82-.LFB5827
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L891-.LFB5827
	.uleb128 0
.LLSDACSE5827:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT5827:
	.section	.text._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC5827
	.type	_ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE.cold, @function
_ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE.cold:
.LFSB5827:
.L847:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L843
	call	_ZdlPv@PLT
.L843:
	movq	-192(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L849
	call	_ZdlPv@PLT
.L849:
	movq	%r13, %rbx
.L726:
	movq	-552(%rbp), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN2v88internal6torque4RuleESt14default_deleteIS4_EESaIS7_EED1Ev
	cmpq	$0, -536(%rbp)
	je	.L850
	movq	-536(%rbp), %rdi
	call	_ZdlPv@PLT
.L850:
	movq	-240(%rbp), %r12
.L854:
	testq	%r12, %r12
	jne	.L1035
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-256(%rbp), %rdi
	movq	$0, -232(%rbp)
	movq	$0, -240(%rbp)
	cmpq	-560(%rbp), %rdi
	je	.L855
	call	_ZdlPv@PLT
.L855:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_21CurrentSourcePositionEEERPNT_5ScopeEv@PLT
	movq	-312(%rbp), %rdx
	movq	%rdx, (%rax)
.L856:
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L857
	call	_ZdlPv@PLT
.L857:
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L858
	call	_ZdlPv@PLT
.L858:
	movq	%rbx, %rdi
.LEHB83:
	call	_Unwind_Resume@PLT
.LEHE83:
.L738:
	movq	$0, -536(%rbp)
	jmp	.L726
.L841:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L843
	call	_ZdlPv@PLT
	jmp	.L843
.L734:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L730
	call	_ZNKSt14default_deleteIN2v88internal6torque4RuleEEclEPS3_.isra.0
.L730:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	$0, -536(%rbp)
	jmp	.L726
.L1035:
	movq	(%r12), %rax
	movq	40(%r12), %r13
	leaq	24(%r12), %r14
	movq	%rax, -440(%rbp)
.L853:
	testq	%r13, %r13
	jne	.L1036
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	-440(%rbp), %r12
	jmp	.L854
.L1036:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4ItemES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	16(%r13), %r15
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	movq	%r15, %r13
	jmp	.L853
.L844:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L825
	call	_ZdlPv@PLT
.L825:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L846
	call	_ZdlPv@PLT
.L846:
	movq	%r12, %r13
	jmp	.L843
.L729:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	leaq	-368(%rbp), %rax
	movq	%rax, -552(%rbp)
	jmp	.L730
.L725:
	leaq	-368(%rbp), %rax
	movq	$0, -536(%rbp)
	movq	%rax, -552(%rbp)
	jmp	.L726
.L755:
	call	__cxa_begin_catch@PLT
	movq	-448(%rbp), %rax
	movq	%rax, 40(%r14)
.LEHB84:
	call	__cxa_rethrow@PLT
.LEHE84:
.L888:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
	call	__cxa_begin_catch@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.LEHB85:
	call	__cxa_rethrow@PLT
.LEHE85:
.L887:
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	jmp	.L726
	.cfi_endproc
.LFE5827:
	.section	.gcc_except_table._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE
	.align 4
.LLSDAC5827:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC5827-.LLSDATTDC5827
.LLSDATTDC5827:
	.byte	0x1
	.uleb128 .LLSDACSEC5827-.LLSDACSBC5827
.LLSDACSBC5827:
	.uleb128 .LEHB83-.LCOLDB28
	.uleb128 .LEHE83-.LEHB83
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB84-.LCOLDB28
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L888-.LCOLDB28
	.uleb128 0x1
	.uleb128 .LEHB85-.LCOLDB28
	.uleb128 .LEHE85-.LEHB85
	.uleb128 .L887-.LCOLDB28
	.uleb128 0
.LLSDACSEC5827:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC5827:
	.section	.text.unlikely._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE
	.section	.text._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE
	.size	_ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE, .-_ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE
	.section	.text.unlikely._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE
	.size	_ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE.cold, .-_ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE.cold
.LCOLDE28:
	.section	.text._ZN2v88internal6torque18RunEarleyAlgorithmEPNS1_6SymbolERKNS1_11LexerResultEPSt13unordered_setINS1_4ItemENS_4base4hashIS8_EESt8equal_toIS8_ESaIS8_EE
.LHOTE28:
	.section	.text.startup._GLOBAL__sub_I__ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE, @function
_GLOBAL__sub_I__ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE:
.LFB9565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9565:
	.size	_GLOBAL__sub_I__ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE, .-_GLOBAL__sub_I__ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZNK2v88internal6torque4Rule9RunActionEPKNS1_4ItemERKNS1_11LexerResultE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC6:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC7:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC8:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC22:
	.quad	1
	.quad	0
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
