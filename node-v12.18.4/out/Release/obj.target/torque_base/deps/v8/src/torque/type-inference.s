	.file	"type-inference.cc"
	.text
	.section	.rodata._ZNK2v88internal6torque21TypeArgumentInference9GetResultEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"!HasFailed()"
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZNK2v88internal6torque21TypeArgumentInference9GetResultEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNK2v88internal6torque21TypeArgumentInference9GetResultEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6torque21TypeArgumentInference9GetResultEv
	.type	_ZNK2v88internal6torque21TypeArgumentInference9GetResultEv, @function
_ZNK2v88internal6torque21TypeArgumentInference9GetResultEv:
.LFB6305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpb	$0, 88(%rsi)
	jne	.L23
	movq	72(%rsi), %rax
	subq	64(%rsi), %rax
	movq	%rsi, %rbx
	movabsq	$1152921504606846975, %rdx
	sarq	$4, %rax
	cmpq	%rdx, %rax
	ja	.L24
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movq	%rdi, %r12
	leaq	0(,%rax,8), %r13
	movups	%xmm0, (%rdi)
	testq	%rax, %rax
	je	.L4
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%r13, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r13), %r14
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	%r14, 16(%r12)
	call	memset@PLT
	movq	%rax, %rcx
.L11:
	movq	%r14, 8(%r12)
	movq	72(%rbx), %r8
	movq	64(%rbx), %rdx
	cmpq	%rdx, %r8
	je	.L1
	leaq	-16(%r8), %rsi
	subq	%rdx, %rsi
	movq	%rsi, %r9
	leaq	16(%rdx,%rsi), %rax
	shrq	$4, %r9
	cmpq	%rax, %rcx
	leaq	8(%rdx), %rax
	leaq	8(,%r9,8), %rdi
	setnb	%r10b
	leaq	(%rcx,%rdi), %r11
	cmpq	%rax, %r11
	setbe	%al
	orb	%al, %r10b
	je	.L12
	cmpq	$80, %rsi
	jbe	.L12
	shrq	$5, %rsi
	xorl	%eax, %eax
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L7:
	movdqu	8(%rdx,%rax,2), %xmm0
	movdqu	24(%rdx,%rax,2), %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rsi, %rax
	jne	.L7
	movq	%r9, %rax
	andq	$-2, %rax
	movq	%rax, %rsi
	leaq	(%rcx,%rax,8), %rax
	salq	$4, %rsi
	addq	%rsi, %rdx
	movq	8(%rdx), %rcx
	movq	%rcx, (%rax)
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %r8
	je	.L1
	movq	24(%rdx), %rdx
	movq	%rdx, 8(%rax)
.L1:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L6:
	movq	8(%rdx,%rax,2), %rsi
	movq	%rsi, (%rcx,%rax)
	addq	$8, %rax
	cmpq	%rdi, %rax
	jne	.L6
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%ecx, %ecx
	xorl	%r14d, %r14d
	jmp	.L11
.L24:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6305:
	.size	_ZNK2v88internal6torque21TypeArgumentInference9GetResultEv, .-_ZNK2v88internal6torque21TypeArgumentInference9GetResultEv
	.section	.rodata._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"basic_string::_M_construct null not valid"
	.align 8
.LC4:
	.string	"found conflicting generic type constructors"
	.align 8
.LC5:
	.string	"cannot infer types from generic-struct-typed parameter with incompatible number of arguments"
	.section	.text.unlikely._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE,"ax",@progbits
	.align 2
.LCOLDB9:
	.section	.text._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE,"ax",@progbits
.LHOTB9:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
	.type	_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE, @function
_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE:
.LFB6311:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6311
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$632, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -632(%rbp)
	movq	32(%rsi), %r13
	movq	%rdx, -648(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rsi), %rax
	movq	%rax, %rbx
	movq	%rax, -640(%rbp)
	subq	%r13, %rbx
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L162
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L163
	movq	%rbx, %rdi
.LEHB0:
	call	_Znwm@PLT
.LEHE0:
	movq	32(%r15), %r13
	movq	%rax, -664(%rbp)
	movq	40(%r15), %rax
	movq	%rax, -640(%rbp)
.L27:
	movq	-664(%rbp), %rax
	addq	%rax, %rbx
	movq	%rbx, -672(%rbp)
	movq	%rax, %rbx
	cmpq	%r13, -640(%rbp)
	je	.L29
	leaq	-616(%rbp), %rax
	movq	%rax, -656(%rbp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L166:
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L34:
	movq	%r12, 8(%rbx)
	addq	$32, %r13
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, -640(%rbp)
	je	.L29
.L35:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L30
	testq	%r14, %r14
	je	.L164
.L30:
	movq	%r12, -616(%rbp)
	cmpq	$15, %r12
	ja	.L165
	cmpq	$1, %r12
	je	.L166
	testq	%r12, %r12
	je	.L34
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L165:
	movq	-656(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB1:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE1:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-616(%rbp), %rax
	movq	%rax, 16(%rbx)
.L32:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-616(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L29:
	movq	64(%r15), %r13
	movq	72(%r15), %r12
	leaq	-432(%rbp), %r14
	movq	%r14, -448(%rbp)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L38
	testq	%r13, %r13
	je	.L167
.L38:
	movq	%r12, -616(%rbp)
	cmpq	$15, %r12
	ja	.L168
	cmpq	$1, %r12
	jne	.L47
	movzbl	0(%r13), %eax
	movb	%al, -432(%rbp)
	movq	%r14, %rax
.L48:
	movq	%r12, -440(%rbp)
	movq	%rbx, %xmm4
	movq	-664(%rbp), %xmm0
	leaq	-536(%rbp), %rbx
	movb	$0, (%rax,%r12)
	movq	-672(%rbp), %rax
	punpcklqdq	%xmm4, %xmm0
	movq	%rbx, -552(%rbp)
	movq	%rax, -560(%rbp)
	movq	-448(%rbp), %rax
	movaps	%xmm0, -576(%rbp)
	cmpq	%r14, %rax
	je	.L169
	movq	%rax, -552(%rbp)
	movq	-432(%rbp), %rax
	movq	%rax, -536(%rbp)
.L50:
	movq	-440(%rbp), %rax
	leaq	-576(%rbp), %rdi
	movq	%rax, -544(%rbp)
.LEHB2:
	call	_ZN2v88internal6torque12Declarations29LookupUniqueGenericStructTypeERKNS1_13QualifiedNameE@PLT
.LEHE2:
	movq	-648(%rbp), %rcx
	cmpb	$0, 176(%rcx)
	je	.L51
	cmpq	%rax, 184(%rcx)
	je	.L52
.L51:
	movq	-632(%rbp), %rax
	cmpb	$0, 88(%rax)
	je	.L170
	leaq	.LC4(%rip), %rcx
	movq	%rcx, 96(%rax)
.L57:
	movq	-552(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L89
	call	_ZdlPv@PLT
.L89:
	movq	-568(%rbp), %rbx
	movq	-576(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L90
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L91
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L94
.L92:
	movq	-576(%rbp), %r12
.L90:
	testq	%r12, %r12
	je	.L25
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L25:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$632, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L172
	movq	%r14, %rax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L91:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L94
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	.LC4(%rip), %rcx
	movb	$1, 88(%rax)
	movq	%rcx, 96(%rax)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	-616(%rbp), %rsi
	leaq	-448(%rbp), %rdi
	xorl	%edx, %edx
.LEHB3:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE3:
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-616(%rbp), %rax
	movq	%rax, -432(%rbp)
.L46:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-616(%rbp), %r12
	movq	-448(%rbp), %rax
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L169:
	movdqa	-432(%rbp), %xmm5
	movups	%xmm5, -536(%rbp)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L52:
	movq	192(%rcx), %rax
	movq	96(%r15), %rdx
	movq	200(%rcx), %rcx
	movq	104(%r15), %rsi
	movq	%rcx, -640(%rbp)
	subq	%rdx, %rsi
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jne	.L173
	testq	%rcx, %rcx
	je	.L78
	xorl	%r12d, %r12d
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L174:
	movq	-648(%rbp), %rax
	movq	192(%rax), %rax
.L80:
	movq	(%rdx,%r12,8), %rsi
	leaq	0(,%r12,8), %rcx
	testq	%rsi, %rsi
	je	.L79
	cmpl	$20, 8(%rsi)
	jne	.L79
	movq	(%rax,%rcx), %rdx
	movq	-632(%rbp), %rdi
.LEHB4:
	call	_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE.part.0
.LEHE4:
.L79:
	movq	-632(%rbp), %rax
	cmpb	$0, 88(%rax)
	jne	.L57
	movq	96(%r15), %rdx
	movq	104(%r15), %rax
	addq	$1, %r12
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r12
	jb	.L174
.L78:
	movq	-552(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	-568(%rbp), %rbx
	movq	-576(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L90
.L86:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L83
.L175:
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	je	.L92
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L175
.L83:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L86
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L162:
	movq	$0, -664(%rbp)
	jmp	.L27
.L164:
	leaq	.LC3(%rip), %rdi
.LEHB5:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE5:
.L171:
	call	__stack_chk_fail@PLT
.L163:
.LEHB6:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE6:
.L173:
	leaq	-320(%rbp), %rax
	leaq	-448(%rbp), %r13
	movq	%rax, %rdi
	movq	%r13, -664(%rbp)
	movq	%rax, -640(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, -320(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, -448(%rbp)
	movq	$0, -104(%rbp)
	movw	$0, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%r12), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	addq	-24(%rax), %r13
	movq	%r13, %rdi
.LEHB7:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE7:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rcx, -432(%rbp)
	movq	-24(%rcx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r14, %rcx
	movq	%rcx, %rdi
.LEHB8:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE8:
	movq	.LC6(%rip), %xmm0
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp)
	movq	-24(%rcx), %rax
	movhps	.LC7(%rip), %xmm0
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp)
	addq	$80, %rcx
	movq	%rcx, -320(%rbp)
	leaq	-368(%rbp), %rcx
	movq	%rcx, %rdi
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -656(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-640(%rbp), %rdi
	leaq	-424(%rbp), %rsi
	movq	%rcx, -424(%rbp)
	leaq	-336(%rbp), %rcx
	movl	$24, -360(%rbp)
	movq	%rcx, -648(%rbp)
	movq	%rcx, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB9:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE9:
	movl	$92, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
.LEHB10:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE10:
	leaq	-592(%rbp), %rax
	movb	$0, -592(%rbp)
	leaq	-608(%rbp), %r13
	movq	%rax, -632(%rbp)
	movq	%rax, -608(%rbp)
	movq	-384(%rbp), %rax
	movq	$0, -600(%rbp)
	testq	%rax, %rax
	je	.L64
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L65
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
.LEHB11:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE11:
.L66:
	leaq	-512(%rbp), %r14
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
.LEHB12:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE12:
	movq	-608(%rbp), %rdi
	cmpq	-632(%rbp), %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	.LC6(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC8(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-648(%rbp), %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	-656(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-640(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, -448(%rbp)
	movq	-24(%r12), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, -480(%rbp)
	movl	28(%r15), %eax
	movd	12(%r15), %xmm0
	movd	16(%r15), %xmm2
	movd	20(%r15), %xmm1
	movd	24(%r15), %xmm3
	je	.L176
	punpckldq	%xmm3, %xmm1
	punpckldq	%xmm2, %xmm0
	movl	%eax, -460(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -476(%rbp)
.L77:
	movq	%r14, %rdi
.LEHB13:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE13:
.L65:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
.LEHB14:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L66
.L64:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE14:
	jmp	.L66
.L176:
	punpckldq	%xmm3, %xmm1
	punpckldq	%xmm2, %xmm0
	movl	%eax, -460(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movb	$1, -480(%rbp)
	movups	%xmm0, -476(%rbp)
	jmp	.L77
.L167:
	leaq	.LC3(%rip), %rdi
.LEHB15:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE15:
.L172:
	movq	%r14, %rdi
	jmp	.L46
.L119:
	endbr64
	movq	%rax, %r13
	jmp	.L61
.L120:
	endbr64
	movq	%rax, %r13
	jmp	.L59
.L118:
	endbr64
	movq	%rax, %r12
	jmp	.L60
.L111:
	endbr64
	movq	%rax, %r13
	jmp	.L96
.L116:
	endbr64
	movq	%rax, %r12
	jmp	.L70
.L112:
	endbr64
	movq	%rax, %r12
	jmp	.L63
.L117:
	endbr64
	movq	%rax, %r12
	jmp	.L75
.L121:
	endbr64
	movq	%rax, %r12
	jmp	.L68
.L113:
	endbr64
	movq	%rax, %r12
	jmp	.L101
.L115:
	endbr64
	movq	%rax, %rdi
	jmp	.L39
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE,"a",@progbits
	.align 4
.LLSDA6311:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6311-.LLSDATTD6311
.LLSDATTD6311:
	.byte	0x1
	.uleb128 .LLSDACSE6311-.LLSDACSB6311
.LLSDACSB6311:
	.uleb128 .LEHB0-.LFB6311
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB6311
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L115-.LFB6311
	.uleb128 0x1
	.uleb128 .LEHB2-.LFB6311
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L112-.LFB6311
	.uleb128 0
	.uleb128 .LEHB3-.LFB6311
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L111-.LFB6311
	.uleb128 0
	.uleb128 .LEHB4-.LFB6311
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L112-.LFB6311
	.uleb128 0
	.uleb128 .LEHB5-.LFB6311
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L115-.LFB6311
	.uleb128 0x1
	.uleb128 .LEHB6-.LFB6311
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB6311
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L118-.LFB6311
	.uleb128 0
	.uleb128 .LEHB8-.LFB6311
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L120-.LFB6311
	.uleb128 0
	.uleb128 .LEHB9-.LFB6311
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L119-.LFB6311
	.uleb128 0
	.uleb128 .LEHB10-.LFB6311
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L116-.LFB6311
	.uleb128 0
	.uleb128 .LEHB11-.LFB6311
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L121-.LFB6311
	.uleb128 0
	.uleb128 .LEHB12-.LFB6311
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L117-.LFB6311
	.uleb128 0
	.uleb128 .LEHB13-.LFB6311
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L113-.LFB6311
	.uleb128 0
	.uleb128 .LEHB14-.LFB6311
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L121-.LFB6311
	.uleb128 0
	.uleb128 .LEHB15-.LFB6311
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L111-.LFB6311
	.uleb128 0
.LLSDACSE6311:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6311:
	.section	.text._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6311
	.type	_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE.cold, @function
_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE.cold:
.LFSB6311:
.L61:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -424(%rbp)
	cmpq	-648(%rbp), %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	-656(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, -448(%rbp)
	movq	-24(%r12), %rax
	movq	%r13, %r12
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L60:
	movq	-640(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L63:
	movq	-552(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L103
	call	_ZdlPv@PLT
.L103:
	movq	-568(%rbp), %r13
	movq	-576(%rbp), %rbx
.L106:
	cmpq	%rbx, %r13
	jne	.L177
	movq	-576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	movq	%r12, %rdi
.LEHB16:
	call	_Unwind_Resume@PLT
.L59:
	movq	%r12, -448(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-24(%r12), %rax
	movq	%r13, %r12
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L60
.L75:
	movq	-608(%rbp), %rdi
	cmpq	-632(%rbp), %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	-664(%rbp), %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
	jmp	.L63
.L68:
	movq	-608(%rbp), %rdi
	cmpq	-632(%rbp), %rdi
	je	.L70
	call	_ZdlPv@PLT
	jmp	.L70
.L96:
	movq	-664(%rbp), %r12
.L99:
	cmpq	%r12, %rbx
	jne	.L178
	cmpq	$0, -664(%rbp)
	je	.L100
	movq	-664(%rbp), %rdi
	call	_ZdlPv@PLT
.L100:
	movq	%r13, %rdi
	call	_Unwind_Resume@PLT
.LEHE16:
.L101:
	movq	%r14, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L63
	call	_ZdlPv@PLT
	jmp	.L63
.L178:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L98
	call	_ZdlPv@PLT
.L98:
	addq	$32, %r12
	jmp	.L99
.L39:
	call	__cxa_begin_catch@PLT
	movq	-664(%rbp), %r12
.L42:
	cmpq	%rbx, %r12
	jne	.L179
.LEHB17:
	call	__cxa_rethrow@PLT
.LEHE17:
.L177:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L105
	call	_ZdlPv@PLT
.L105:
	addq	$32, %rbx
	jmp	.L106
.L179:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	addq	$32, %r12
	jmp	.L42
.L114:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
	cmpq	$0, -664(%rbp)
	je	.L107
	movq	-664(%rbp), %rdi
	call	_ZdlPv@PLT
	jmp	.L107
	.cfi_endproc
.LFE6311:
	.section	.gcc_except_table._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
	.align 4
.LLSDAC6311:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6311-.LLSDATTDC6311
.LLSDATTDC6311:
	.byte	0x1
	.uleb128 .LLSDACSEC6311-.LLSDACSBC6311
.LLSDACSBC6311:
	.uleb128 .LEHB16-.LCOLDB9
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB17-.LCOLDB9
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L114-.LCOLDB9
	.uleb128 0
.LLSDACSEC6311:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6311:
	.section	.text.unlikely._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
	.section	.text._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
	.size	_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE, .-_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
	.section	.text.unlikely._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
	.size	_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE.cold, .-_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE.cold
.LCOLDE9:
	.section	.text._ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
.LHOTE9:
	.section	.rodata._ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE.part.0.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"found conflicting types for generic parameter"
	.section	.text._ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE.part.0, @function
_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE.part.0:
.LFB10243:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA10243
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %rax
	cmpq	%rax, 40(%rsi)
	je	.L205
.L181:
	movq	96(%r12), %rax
	cmpq	%rax, 104(%r12)
	je	.L180
	testq	%r13, %r13
	je	.L180
	cmpl	$4, 8(%r13)
	jne	.L180
	addq	$24, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
.LEHB18:
	jmp	_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
.LEHE18:
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	movq	72(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L182
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	testq	%rdx, %rdx
	je	.L183
	movq	8(%rbx), %rsi
	movq	64(%r12), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	jne	.L182
.L183:
	movq	40(%rbx), %rax
	cmpq	(%r14), %rax
	jb	.L180
	salq	$4, %rax
	addq	64(%r14), %rax
	cmpb	$0, (%rax)
	jne	.L206
	movq	%r13, 8(%rax)
	movb	$1, (%rax)
.L180:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	cmpb	$0, 56(%rsi)
	jne	.L181
	movq	72(%rsi), %rsi
	movq	64(%r12), %rdi
	movl	$3339675911, %edx
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	16(%r14), %r15
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%r15
	movq	8(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L181
	movq	(%rax), %rbx
	movq	48(%rbx), %rcx
.L184:
	cmpq	%rcx, %r8
	je	.L207
.L182:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L181
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	je	.L184
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L206:
	cmpq	%r13, 8(%rax)
	je	.L180
	leaq	.LC10(%rip), %rax
	cmpb	$0, 88(%r14)
	movq	%rax, 96(%r14)
	jne	.L180
	movb	$1, 88(%r14)
	jmp	.L180
	.cfi_endproc
.LFE10243:
	.section	.gcc_except_table._ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE.part.0,"a",@progbits
.LLSDA10243:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE10243-.LLSDACSB10243
.LLSDACSB10243:
	.uleb128 .LEHB18-.LFB10243
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0
	.uleb128 0
.LLSDACSE10243:
	.section	.text._ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE.part.0
	.size	_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE.part.0, .-_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE.part.0
	.section	.text._ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE
	.type	_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE, @function
_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE:
.LFB6309:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6309
	endbr64
	testq	%rsi, %rsi
	je	.L234
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	cmpl	$20, 8(%rsi)
	jne	.L208
	movq	32(%rsi), %rax
	movq	%rdi, %r14
	movq	%rdx, %r13
	cmpq	%rax, 40(%rsi)
	je	.L237
.L212:
	movq	96(%r12), %rax
	cmpq	%rax, 104(%r12)
	je	.L208
	testq	%r13, %r13
	je	.L208
	cmpl	$4, 8(%r13)
	jne	.L208
	addq	$24, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
.LEHB19:
	jmp	_ZN2v88internal6torque21TypeArgumentInference12MatchGenericEPNS1_19BasicTypeExpressionEPKNS1_10StructTypeE
.LEHE19:
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movq	72(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L213
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	testq	%rdx, %rdx
	je	.L214
	movq	8(%rbx), %rsi
	movq	64(%r12), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	jne	.L213
.L214:
	movq	40(%rbx), %rax
	cmpq	(%r14), %rax
	jb	.L208
	salq	$4, %rax
	addq	64(%r14), %rax
	cmpb	$0, (%rax)
	jne	.L238
	movq	%r13, 8(%rax)
	movb	$1, (%rax)
.L208:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	cmpb	$0, 56(%rsi)
	jne	.L212
	movq	72(%rsi), %rsi
	movq	64(%r12), %rdi
	movl	$3339675911, %edx
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	16(%r14), %r15
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%r15
	movq	8(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L212
	movq	(%rax), %rbx
	movq	48(%rbx), %rcx
.L215:
	cmpq	%rcx, %r8
	je	.L239
.L213:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L212
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	je	.L215
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	8(%rax), %r13
	je	.L208
	leaq	.LC10(%rip), %rax
	cmpb	$0, 88(%r14)
	movq	%rax, 96(%r14)
	jne	.L208
	movb	$1, 88(%r14)
	jmp	.L208
	.cfi_endproc
.LFE6309:
	.section	.gcc_except_table._ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE,"a",@progbits
.LLSDA6309:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6309-.LLSDACSB6309
.LLSDACSB6309:
	.uleb128 .LEHB19-.LFB6309
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSE6309:
	.section	.text._ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE
	.size	_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE, .-_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED2Ev,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED2Ev
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED2Ev, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED2Ev:
.LFB7382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r12
	testq	%r12, %r12
	jne	.L244
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L256:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L241
.L243:
	movq	%r13, %r12
.L244:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	jne	.L256
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L243
.L241:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	addq	$48, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L240
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7382:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED2Ev, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED2Ev
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED1Ev
	.set	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED1Ev,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED2Ev
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm:
.LFB9094:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA9094
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	cmpq	$1, %rsi
	je	.L279
	movabsq	$1152921504606846975, %rax
	movq	%rdx, %r13
	cmpq	%rax, %rsi
	ja	.L280
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
.LEHB20:
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%rbx), %r9
.L259:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L261
	xorl	%r8d, %r8d
	leaq	16(%rbx), %r10
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L263:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%rdi), %rax
	movq	%rcx, (%rax)
.L264:
	testq	%rsi, %rsi
	je	.L261
.L262:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	48(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L263
	movq	16(%rbx), %rax
	movq	%rax, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rdi)
	cmpq	$0, (%rcx)
	je	.L268
	movq	%rcx, 0(%r13,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L262
	.p2align 4,,10
	.p2align 3
.L261:
	movq	(%rbx), %rdi
	cmpq	%r9, %rdi
	je	.L265
	call	_ZdlPv@PLT
.L265:
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	%rdx, %r8
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, %r9
	jmp	.L259
.L280:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE20:
.L269:
	endbr64
	movq	%rax, %rdi
.L266:
	call	__cxa_begin_catch@PLT
	movq	0(%r13), %rax
	movq	%rax, 40(%rbx)
.LEHB21:
	call	__cxa_rethrow@PLT
.LEHE21:
.L270:
	endbr64
	movq	%rax, %r12
.L267:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB22:
	call	_Unwind_Resume@PLT
.LEHE22:
	.cfi_endproc
.LFE9094:
	.section	.gcc_except_table._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"aG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.align 4
.LLSDA9094:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT9094-.LLSDATTD9094
.LLSDATTD9094:
	.byte	0x1
	.uleb128 .LLSDACSE9094-.LLSDACSB9094
.LLSDACSB9094:
	.uleb128 .LEHB20-.LFB9094
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L269-.LFB9094
	.uleb128 0x1
	.uleb128 .LEHB21-.LFB9094
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L270-.LFB9094
	.uleb128 0
	.uleb128 .LEHB22-.LFB9094
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0
	.uleb128 0
.LLSDACSE9094:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT9094:
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm,comdat
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm:
.LFB8741:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA8741
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$16, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rax, -48(%rbp)
.LEHB23:
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
.LEHE23:
	testb	%al, %al
	je	.L282
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rdx
.LEHB24:
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE9_M_rehashEmRKm
.LEHE24:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%rdx, %r13
.L282:
	movq	%r14, 48(%r12)
	movq	(%rbx), %rax
	leaq	0(,%r13,8), %rcx
	movq	(%rax,%r13,8), %rax
	testq	%rax, %rax
	je	.L283
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%rbx), %rax
	movq	(%rax,%r13,8), %rax
	movq	%r12, (%rax)
.L284:
	addq	$1, 24(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L297
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L285
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L285:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L284
.L297:
	call	__stack_chk_fail@PLT
.L290:
	endbr64
	movq	%rax, %rdi
.L286:
	call	__cxa_begin_catch@PLT
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.LEHB25:
	call	__cxa_rethrow@PLT
.LEHE25:
.L291:
	endbr64
	movq	%rax, %r12
.L288:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB26:
	call	_Unwind_Resume@PLT
.LEHE26:
	.cfi_endproc
.LFE8741:
	.section	.gcc_except_table._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm,"aG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm,comdat
	.align 4
.LLSDA8741:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT8741-.LLSDATTD8741
.LLSDATTD8741:
	.byte	0x1
	.uleb128 .LLSDACSE8741-.LLSDACSB8741
.LLSDACSB8741:
	.uleb128 .LEHB23-.LFB8741
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB24-.LFB8741
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L290-.LFB8741
	.uleb128 0x1
	.uleb128 .LEHB25-.LFB8741
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L291-.LFB8741
	.uleb128 0
	.uleb128 .LEHB26-.LFB8741
	.uleb128 .LEHE26-.LEHB26
	.uleb128 0
	.uleb128 0
.LLSDACSE8741:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT8741:
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm,comdat
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm
	.section	.rodata._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"more explicit type arguments than expected"
	.align 8
.LC13:
	.string	"number of term parameters does not match number of term arguments!"
	.align 8
.LC14:
	.string	"failed to infer arguments for all type parameters"
	.section	.text.unlikely._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_,"ax",@progbits
	.align 2
.LCOLDB15:
	.section	.text._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_,"ax",@progbits
.LHOTB15:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.type	_ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_, @function
_ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_:
.LFB6303:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA6303
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	56(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rdx, -120(%rbp)
	movq	8(%rsi), %rsi
	movq	%rcx, -88(%rbp)
	subq	(%r12), %rsi
	movq	%r8, -96(%rbp)
	sarq	$3, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	subq	(%rdx), %rax
	movq	%r14, 8(%rdi)
	sarq	$3, %rax
	movq	$1, 16(%rdi)
	movq	%rax, (%rdi)
	leaq	8(%rdi), %rax
	leaq	40(%rdi), %rdi
	movq	$0, -16(%rdi)
	movq	$0, -8(%rdi)
	movl	$0x3f800000, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	%rax, -104(%rbp)
.LEHB27:
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %r13
	cmpq	16(%rbx), %rax
	jbe	.L299
	cmpq	$1, %rax
	je	.L390
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r13
	ja	.L391
	leaq	0(,%r13,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
.LEHE27:
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	memset@PLT
.L301:
	movq	%r14, 8(%rbx)
	movq	%r13, 16(%rbx)
.L299:
	movabsq	$576460752303423487, %rdx
	movq	8(%r12), %rax
	subq	(%r12), %rax
	sarq	$3, %rax
	cmpq	%rdx, %rax
	ja	.L392
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	movq	$0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	salq	$4, %r13
	testq	%rax, %rax
	je	.L306
	movq	%r13, %rdi
.LEHB28:
	call	_Znwm@PLT
.LEHE28:
	addq	%rax, %r13
	movq	%rax, 64(%rbx)
	movq	%r13, 80(%rbx)
	.p2align 4,,10
	.p2align 3
.L307:
	movb	$0, (%rax)
	addq	$16, %rax
	movb	$0, -8(%rax)
	cmpq	%rax, %r13
	jne	.L307
.L341:
	movq	%r13, 72(%rbx)
	movq	(%rbx), %rdi
	movb	$0, 88(%rbx)
	movb	$0, 96(%rbx)
	movq	(%r12), %rdx
	movq	8(%r12), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdi
	ja	.L393
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %r9
	movq	8(%rcx), %rsi
	subq	(%rcx), %rsi
	movq	8(%r9), %rcx
	subq	(%r9), %rcx
	cmpq	%rcx, %rsi
	jne	.L394
	leaq	-64(%rbp), %rcx
	xorl	%r14d, %r14d
	movq	%rcx, -128(%rbp)
	testq	%rax, %rax
	je	.L327
	.p2align 4,,10
	.p2align 3
.L311:
	movq	(%rdx,%r14,8), %r13
	movl	$3339675911, %edx
	movq	40(%r13), %rsi
	movq	32(%r13), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	16(%rbx), %r8
	xorl	%edx, %edx
	movq	%rax, -72(%rbp)
	divq	%r8
	movq	8(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, -80(%rbp)
	testq	%rax, %rax
	je	.L314
	movq	(%rax), %r15
	movq	48(%r15), %rcx
.L317:
	cmpq	%rcx, -72(%rbp)
	je	.L395
.L315:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L314
	movq	48(%r15), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, -80(%rbp)
	je	.L317
.L314:
	movl	$56, %edi
.LEHB29:
	call	_Znwm@PLT
	leaq	24(%rax), %rdi
	movq	$0, (%rax)
	movq	%rax, %r15
	movq	%rdi, 8(%rax)
	movq	32(%r13), %rax
	movq	40(%r13), %r13
	movq	%rax, %rcx
	movq	%rax, -112(%rbp)
	addq	%r13, %rcx
	je	.L318
	testq	%rax, %rax
	je	.L396
.L318:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L397
	cmpq	$1, %r13
	jne	.L321
	movq	-112(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, 24(%r15)
.L322:
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r13, 16(%r15)
	movq	%r15, %rcx
	movb	$0, (%rdi,%r13)
	movq	-104(%rbp), %rdi
	movl	$1, %r8d
	movq	$0, 40(%r15)
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb1EEEm
	leaq	40(%rax), %r15
.L340:
	movq	%r14, (%r15)
	movq	(%r12), %rdx
	addq	$1, %r14
	movq	8(%r12), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r14
	jb	.L311
	movq	(%rbx), %rdi
.L327:
	testq	%rdi, %rdi
	je	.L312
	movq	-120(%rbp), %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L313:
	movq	(%rsi), %rax
	movq	(%rax,%rdx,8), %rcx
	movq	%rdx, %rax
	salq	$4, %rax
	addq	64(%rbx), %rax
	cmpb	$0, (%rax)
	movq	%rcx, 8(%rax)
	je	.L330
	addq	$1, %rdx
	cmpq	(%rbx), %rdx
	jb	.L313
.L312:
	movq	-88(%rbp), %rax
	xorl	%r13d, %r13d
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	je	.L329
	.p2align 4,,10
	.p2align 3
.L328:
	movq	(%rdx,%r13,8), %rsi
	leaq	0(,%r13,8), %rax
	testq	%rsi, %rsi
	je	.L333
	cmpl	$20, 8(%rsi)
	jne	.L333
	movq	-96(%rbp), %rdi
	movq	(%rdi), %rdx
	movq	%rbx, %rdi
	movq	(%rdx,%rax), %rdx
	call	_ZN2v88internal6torque21TypeArgumentInference5MatchEPNS1_14TypeExpressionEPKNS1_4TypeE.part.0
.LEHE29:
.L333:
	cmpb	$0, 88(%rbx)
	jne	.L298
	movq	-88(%rbp), %rax
	addq	$1, %r13
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	%rax, -72(%rbp)
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %r13
	jb	.L328
.L329:
	movq	8(%r12), %rcx
	subq	(%r12), %rcx
	sarq	$3, %rcx
	je	.L298
	movq	64(%rbx), %rsi
	xorl	%eax, %eax
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L335:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L298
.L337:
	movq	%rax, %rdx
	salq	$4, %rdx
	cmpb	$0, (%rsi,%rdx)
	jne	.L335
	leaq	.LC14(%rip), %rax
	cmpb	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	jne	.L298
	movb	$1, 88(%rbx)
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	.LC13(%rip), %rax
	movb	$1, 88(%rbx)
	movq	%rax, 96(%rbx)
.L298:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L398
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	movq	40(%r13), %rdx
	cmpq	16(%r15), %rdx
	jne	.L315
	movq	%r8, -112(%rbp)
	testq	%rdx, %rdx
	je	.L316
	movq	8(%r15), %rsi
	movq	32(%r13), %rdi
	call	memcmp@PLT
	movq	-112(%rbp), %r8
	testl	%eax, %eax
	jne	.L315
.L316:
	addq	$40, %r15
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L330:
	movb	$1, (%rax)
	addq	$1, %rdx
	cmpq	%rdx, (%rbx)
	ja	.L313
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L321:
	testq	%r13, %r13
	je	.L322
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L397:
	movq	-128(%rbp), %rsi
	leaq	8(%r15), %rdi
	xorl	%edx, %edx
.LEHB30:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE30:
	movq	%rax, 8(%r15)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%r15)
.L320:
	movq	-112(%rbp), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	8(%r15), %rdi
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	.LC12(%rip), %rax
	movb	$1, 88(%rbx)
	movq	%rax, 96(%rbx)
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L390:
	movq	$0, 56(%rbx)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L306:
	xorl	%r13d, %r13d
	jmp	.L341
.L391:
.LEHB31:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE31:
.L398:
	call	__stack_chk_fail@PLT
.L392:
	leaq	.LC2(%rip), %rdi
.LEHB32:
	call	_ZSt20__throw_length_errorPKc@PLT
.LEHE32:
.L396:
	leaq	.LC3(%rip), %rdi
.LEHB33:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE33:
.L347:
	endbr64
	movq	%rax, %r12
	jmp	.L305
.L345:
	endbr64
	movq	%rax, %r12
	jmp	.L339
.L346:
	endbr64
	movq	%rax, %r12
	jmp	.L326
.L349:
	endbr64
	movq	%rax, %rdi
	jmp	.L324
	.section	.gcc_except_table._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_,"a",@progbits
	.align 4
.LLSDA6303:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT6303-.LLSDATTD6303
.LLSDATTD6303:
	.byte	0x1
	.uleb128 .LLSDACSE6303-.LLSDACSB6303
.LLSDACSB6303:
	.uleb128 .LEHB27-.LFB6303
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L347-.LFB6303
	.uleb128 0
	.uleb128 .LEHB28-.LFB6303
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L345-.LFB6303
	.uleb128 0
	.uleb128 .LEHB29-.LFB6303
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L346-.LFB6303
	.uleb128 0
	.uleb128 .LEHB30-.LFB6303
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L349-.LFB6303
	.uleb128 0x1
	.uleb128 .LEHB31-.LFB6303
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L347-.LFB6303
	.uleb128 0
	.uleb128 .LEHB32-.LFB6303
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L345-.LFB6303
	.uleb128 0
	.uleb128 .LEHB33-.LFB6303
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L349-.LFB6303
	.uleb128 0x1
.LLSDACSE6303:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT6303:
	.section	.text._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC6303
	.type	_ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_.cold, @function
_ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_.cold:
.LFSB6303:
.L305:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-104(%rbp), %rdi
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED1Ev
	movq	%r12, %rdi
.LEHB34:
	call	_Unwind_Resume@PLT
.LEHE34:
.L348:
	endbr64
	movq	%rax, %r12
	call	__cxa_end_catch@PLT
.L326:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	movq	-104(%rbp), %rdi
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEED1Ev
	movq	%r12, %rdi
.LEHB35:
	call	_Unwind_Resume@PLT
.LEHE35:
.L324:
	call	__cxa_begin_catch@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.LEHB36:
	call	__cxa_rethrow@PLT
.LEHE36:
	.cfi_endproc
.LFE6303:
	.section	.gcc_except_table._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.align 4
.LLSDAC6303:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC6303-.LLSDATTDC6303
.LLSDATTDC6303:
	.byte	0x1
	.uleb128 .LLSDACSEC6303-.LLSDACSBC6303
.LLSDACSBC6303:
	.uleb128 .LEHB34-.LCOLDB15
	.uleb128 .LEHE34-.LEHB34
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB35-.LCOLDB15
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB36-.LCOLDB15
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L348-.LCOLDB15
	.uleb128 0
.LLSDACSEC6303:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC6303:
	.section	.text.unlikely._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.section	.text._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.size	_ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_, .-_ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.section	.text.unlikely._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.size	_ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_.cold, .-_ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_.cold
.LCOLDE15:
	.section	.text._ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
.LHOTE15:
	.globl	_ZN2v88internal6torque21TypeArgumentInferenceC1ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.set	_ZN2v88internal6torque21TypeArgumentInferenceC1ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_,_ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_, @function
_GLOBAL__sub_I__ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_:
.LFB10110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10110:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_, .-_GLOBAL__sub_I__ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque21TypeArgumentInferenceC2ERKSt6vectorIPNS1_10IdentifierESaIS5_EERKS3_IPKNS1_4TypeESaISC_EERKS3_IPNS1_14TypeExpressionESaISI_EESG_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC6:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC7:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC8:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
