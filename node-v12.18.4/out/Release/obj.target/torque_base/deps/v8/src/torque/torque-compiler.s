	.file	"torque-compiler.cc"
	.text
	.section	.text._ZN2v88internal6torque13NullStreambuf8overflowEi,"axG",@progbits,_ZN2v88internal6torque13NullStreambuf8overflowEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13NullStreambuf8overflowEi
	.type	_ZN2v88internal6torque13NullStreambuf8overflowEi, @function
_ZN2v88internal6torque13NullStreambuf8overflowEi:
.LFB4589:
	.cfi_startproc
	endbr64
	leaq	64(%rdi), %rdx
	movl	%esi, %eax
	cmpl	$-1, %esi
	movq	%rdx, 40(%rdi)
	movq	%rdx, 32(%rdi)
	leaq	128(%rdi), %rdx
	movq	%rdx, 48(%rdi)
	movl	$0, %edx
	cmove	%edx, %eax
	ret
	.cfi_endproc
.LFE4589:
	.size	_ZN2v88internal6torque13NullStreambuf8overflowEi, .-_ZN2v88internal6torque13NullStreambuf8overflowEi
	.section	.text._ZN2v88internal6torque13NullStreambufD2Ev,"axG",@progbits,_ZN2v88internal6torque13NullStreambufD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13NullStreambufD2Ev
	.type	_ZN2v88internal6torque13NullStreambufD2Ev, @function
_ZN2v88internal6torque13NullStreambufD2Ev:
.LFB13868:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	addq	$56, %rdi
	movq	%rax, -56(%rdi)
	jmp	_ZNSt6localeD1Ev@PLT
	.cfi_endproc
.LFE13868:
	.size	_ZN2v88internal6torque13NullStreambufD2Ev, .-_ZN2v88internal6torque13NullStreambufD2Ev
	.weak	_ZN2v88internal6torque13NullStreambufD1Ev
	.set	_ZN2v88internal6torque13NullStreambufD1Ev,_ZN2v88internal6torque13NullStreambufD2Ev
	.section	.text._ZN2v88internal6torque13NullStreambufD0Ev,"axG",@progbits,_ZN2v88internal6torque13NullStreambufD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13NullStreambufD0Ev
	.type	_ZN2v88internal6torque13NullStreambufD0Ev, @function
_ZN2v88internal6torque13NullStreambufD0Ev:
.LFB13870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	56(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -56(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13870:
	.size	_ZN2v88internal6torque13NullStreambufD0Ev, .-_ZN2v88internal6torque13NullStreambufD0Ev
	.section	.text._ZN2v88internal6torque11NullOStreamD1Ev,"axG",@progbits,_ZN2v88internal6torque11NullOStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque11NullOStreamD1Ev
	.type	_ZN2v88internal6torque11NullOStreamD1Ev, @function
_ZN2v88internal6torque11NullOStreamD1Ev:
.LFB13865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 72(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal6torque11NullOStreamE0_So(%rip), %rax
	leaq	136(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 136(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE13865:
	.size	_ZN2v88internal6torque11NullOStreamD1Ev, .-_ZN2v88internal6torque11NullOStreamD1Ev
	.section	.text._ZN2v88internal6torque11NullOStreamD0Ev,"axG",@progbits,_ZN2v88internal6torque11NullOStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal6torque11NullOStreamD0Ev
	.type	_ZTv0_n24_N2v88internal6torque11NullOStreamD0Ev, @function
_ZTv0_n24_N2v88internal6torque11NullOStreamD0Ev:
.LFB14313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rax
	movq	%rax, 136(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal6torque11NullOStreamE0_So(%rip), %rax
	leaq	136(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 136(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$400, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE14313:
	.size	_ZTv0_n24_N2v88internal6torque11NullOStreamD0Ev, .-_ZTv0_n24_N2v88internal6torque11NullOStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque11NullOStreamD0Ev
	.type	_ZN2v88internal6torque11NullOStreamD0Ev, @function
_ZN2v88internal6torque11NullOStreamD0Ev:
.LFB13866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 72(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal6torque11NullOStreamE0_So(%rip), %rax
	leaq	136(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 136(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$400, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13866:
	.size	_ZN2v88internal6torque11NullOStreamD0Ev, .-_ZN2v88internal6torque11NullOStreamD0Ev
	.section	.text._ZN2v88internal6torque11NullOStreamD1Ev,"axG",@progbits,_ZN2v88internal6torque11NullOStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal6torque11NullOStreamD1Ev
	.type	_ZTv0_n24_N2v88internal6torque11NullOStreamD1Ev, @function
_ZTv0_n24_N2v88internal6torque11NullOStreamD1Ev:
.LFB14314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 136(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal6torque11NullOStreamE0_So(%rip), %rax
	leaq	136(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 136(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE14314:
	.size	_ZTv0_n24_N2v88internal6torque11NullOStreamD1Ev, .-_ZTv0_n24_N2v88internal6torque11NullOStreamD1Ev
	.section	.text._ZN2v88internal6torque21ImplementationVisitorD2Ev,"axG",@progbits,_ZN2v88internal6torque21ImplementationVisitorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque21ImplementationVisitorD2Ev
	.type	_ZN2v88internal6torque21ImplementationVisitorD2Ev, @function
_ZN2v88internal6torque21ImplementationVisitorD2Ev:
.LFB7199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$200, %rdi
	subq	$24, %rsp
	movq	%rax, 72(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal6torque11NullOStreamE0_So(%rip), %rax
	leaq	272(%rbx), %rdi
	movq	%rax, 136(%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 272(%rbx)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, (%rbx)
	jne	.L37
.L15:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	64(%rbx), %rdi
	leaq	32(%rbx), %r15
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	32(%rbx), %r13
	cmpq	%r13, %r15
	je	.L27
	.p2align 4,,10
	.p2align 3
.L18:
	cmpb	$0, 48(%r13)
	movq	0(%r13), %r14
	je	.L20
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	32(%r13), %rax
	movq	24(%r13), %r12
	cmpq	%r12, %rax
	je	.L21
	.p2align 4,,10
	.p2align 3
.L25:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rdx
	movq	%rax, -56(%rbp)
	addq	$16, %r12
	call	*24(%rdx)
	movq	-56(%rbp), %rax
	cmpq	%rax, %r12
	jne	.L25
.L23:
	movq	24(%r13), %r12
.L21:
	testq	%r12, %r12
	je	.L26
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	cmpq	%r14, %r15
	je	.L27
.L28:
	movq	%r14, %r13
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L22:
	addq	$16, %r12
	cmpq	%r12, %rax
	jne	.L25
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	cmpq	%r14, %r15
	jne	.L28
.L27:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L15
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE7199:
	.size	_ZN2v88internal6torque21ImplementationVisitorD2Ev, .-_ZN2v88internal6torque21ImplementationVisitorD2Ev
	.weak	_ZN2v88internal6torque21ImplementationVisitorD1Ev
	.set	_ZN2v88internal6torque21ImplementationVisitorD1Ev,_ZN2v88internal6torque21ImplementationVisitorD2Ev
	.section	.text._ZN2v88internal6torque21TorqueCompilerOptionsD2Ev,"axG",@progbits,_ZN2v88internal6torque21TorqueCompilerOptionsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev
	.type	_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev, @function
_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev:
.LFB7398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	48(%rbx), %rax
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movq	(%rbx), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L38
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7398:
	.size	_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev, .-_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev
	.weak	_ZN2v88internal6torque21TorqueCompilerOptionsD1Ev
	.set	_ZN2v88internal6torque21TorqueCompilerOptionsD1Ev,_ZN2v88internal6torque21TorqueCompilerOptionsD2Ev
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev:
.LFB7724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L43
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L44
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L47
.L45:
	movq	0(%r13), %r12
.L43:
	testq	%r12, %r12
	je	.L42
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L47
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L42:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7724:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	.set	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED2Ev
	.section	.text._ZN2v88internal6torque12CfgAssemblerD2Ev,"axG",@progbits,_ZN2v88internal6torque12CfgAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque12CfgAssemblerD2Ev
	.type	_ZN2v88internal6torque12CfgAssemblerD2Ev, @function
_ZN2v88internal6torque12CfgAssemblerD2Ev:
.LFB8963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	24(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	24(%rbx), %r13
	cmpq	%r13, %r14
	je	.L62
	.p2align 4,,10
	.p2align 3
.L52:
	cmpb	$0, 48(%r13)
	movq	0(%r13), %r15
	je	.L55
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	32(%r13), %rax
	movq	24(%r13), %r12
	cmpq	%r12, %rax
	je	.L56
	.p2align 4,,10
	.p2align 3
.L60:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L57
	movq	(%rdi), %rdx
	movq	%rax, -56(%rbp)
	addq	$16, %r12
	call	*24(%rdx)
	movq	-56(%rbp), %rax
	cmpq	%r12, %rax
	jne	.L60
.L58:
	movq	24(%r13), %r12
.L56:
	testq	%r12, %r12
	je	.L61
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	cmpq	%r15, %r14
	je	.L62
.L63:
	movq	%r15, %r13
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$16, %r12
	cmpq	%r12, %rax
	jne	.L60
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	cmpq	%r15, %r14
	jne	.L63
.L62:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L73
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8963:
	.size	_ZN2v88internal6torque12CfgAssemblerD2Ev, .-_ZN2v88internal6torque12CfgAssemblerD2Ev
	.weak	_ZN2v88internal6torque12CfgAssemblerD1Ev
	.set	_ZN2v88internal6torque12CfgAssemblerD1Ev,_ZN2v88internal6torque12CfgAssemblerD2Ev
	.section	.text._ZN2v88internal6torque13SourceFileMapD2Ev,"axG",@progbits,_ZN2v88internal6torque13SourceFileMapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13SourceFileMapD2Ev
	.type	_ZN2v88internal6torque13SourceFileMapD2Ev, @function
_ZN2v88internal6torque13SourceFileMapD2Ev:
.LFB9092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	40(%rbx), %rax
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	8(%rbx), %r13
	movq	(%rbx), %r12
	cmpq	%r12, %r13
	je	.L76
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L77
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L80
.L78:
	movq	(%rbx), %r12
.L76:
	testq	%r12, %r12
	je	.L74
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L80
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L74:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9092:
	.size	_ZN2v88internal6torque13SourceFileMapD2Ev, .-_ZN2v88internal6torque13SourceFileMapD2Ev
	.weak	_ZN2v88internal6torque13SourceFileMapD1Ev
	.set	_ZN2v88internal6torque13SourceFileMapD1Ev,_ZN2v88internal6torque13SourceFileMapD2Ev
	.section	.text._ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED2Ev,"axG",@progbits,_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED2Ev
	.type	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED2Ev, @function
_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED2Ev:
.LFB9203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	(%rdi), %r12
	cmpq	%r12, %rbx
	je	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L85
	call	_ZdlPv@PLT
	addq	$64, %r12
	cmpq	%r12, %rbx
	jne	.L88
.L86:
	movq	0(%r13), %r12
.L84:
	testq	%r12, %r12
	je	.L83
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	addq	$64, %r12
	cmpq	%r12, %rbx
	jne	.L88
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L83:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9203:
	.size	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED2Ev, .-_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED2Ev
	.weak	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED1Ev
	.set	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED1Ev,_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED2Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB9661:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L106
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L95:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L93
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L91
.L94:
	movq	%rbx, %r12
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L94
.L91:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE9661:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB9735:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L117
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L111:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L111
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE9735:
	.size	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN2v88internal6torque10TypeOracleD2Ev,"axG",@progbits,_ZN2v88internal6torque10TypeOracleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque10TypeOracleD2Ev
	.type	_ZN2v88internal6torque10TypeOracleD2Ev, @function
_ZN2v88internal6torque10TypeOracleD2Ev:
.LFB7325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	216(%rdi), %r13
	movq	208(%rdi), %r12
	cmpq	%r12, %r13
	je	.L121
	.p2align 4,,10
	.p2align 3
.L125:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L122
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L125
.L123:
	movq	208(%rbx), %r12
.L121:
	testq	%r12, %r12
	je	.L126
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L126:
	movq	192(%rbx), %r13
	movq	184(%rbx), %r12
	cmpq	%r12, %r13
	je	.L127
	.p2align 4,,10
	.p2align 3
.L131:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L128
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L131
.L129:
	movq	184(%rbx), %r12
.L127:
	testq	%r12, %r12
	je	.L132
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L132:
	movq	168(%rbx), %r13
	movq	160(%rbx), %r12
	cmpq	%r12, %r13
	je	.L133
	.p2align 4,,10
	.p2align 3
.L137:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L134
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L137
.L135:
	movq	160(%rbx), %r12
.L133:
	testq	%r12, %r12
	je	.L138
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L138:
	movq	144(%rbx), %r13
	movq	136(%rbx), %r12
	cmpq	%r12, %r13
	je	.L139
	.p2align 4,,10
	.p2align 3
.L143:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L143
.L141:
	movq	136(%rbx), %r12
.L139:
	testq	%r12, %r12
	je	.L144
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L144:
	movq	96(%rbx), %r12
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -56(%rbp)
	testq	%r12, %r12
	je	.L152
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r12, %r14
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	(%r12), %r12
	movq	96(%r14), %r13
	movq	%rax, 8(%r14)
	leaq	80(%r14), %r15
	testq	%r13, %r13
	je	.L151
.L148:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L148
.L151:
	movq	-56(%rbp), %rax
	movq	48(%r14), %r15
	movq	%rax, 8(%r14)
	leaq	32(%r14), %rax
	movq	%rax, -64(%rbp)
	testq	%r15, %r15
	je	.L149
.L150:
	movq	-64(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L153
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L149
.L154:
	movq	%r13, %r15
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L154
.L149:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L145
.L152:
	movq	88(%rbx), %rax
	movq	80(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%rbx), %rdi
	leaq	128(%rbx), %rax
	movq	$0, 104(%rbx)
	movq	$0, 96(%rbx)
	cmpq	%rax, %rdi
	je	.L146
	call	_ZdlPv@PLT
.L146:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	16(%rbx), %r13
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -56(%rbp)
	testq	%r13, %r13
	je	.L161
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r13, %r12
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	0(%r13), %r13
	movq	80(%r12), %rdi
	movq	%rax, 8(%r12)
	testq	%rdi, %rdi
	je	.L159
	call	_ZdlPv@PLT
.L159:
	movq	-56(%rbp), %rax
	movq	48(%r12), %r15
	movq	%rax, 8(%r12)
	leaq	32(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%r15, %r15
	je	.L163
.L160:
	movq	-64(%rbp), %rdi
	movq	24(%r15), %rsi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r14
	cmpq	%rax, %rdi
	je	.L162
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L163
.L164:
	movq	%r14, %r15
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L164
.L163:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L156
.L161:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	addq	$48, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L216
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L143
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L134:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L137
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L128:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L131
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L122:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L125
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L216:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7325:
	.size	_ZN2v88internal6torque10TypeOracleD2Ev, .-_ZN2v88internal6torque10TypeOracleD2Ev
	.weak	_ZN2v88internal6torque10TypeOracleD1Ev
	.set	_ZN2v88internal6torque10TypeOracleD1Ev,_ZN2v88internal6torque10TypeOracleD2Ev
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB10415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L217
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	%rdx, %xmm2
	movq	%rax, %xmm3
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	%rsi, %rbx
	punpcklqdq	%xmm3, %xmm2
	movaps	%xmm2, -64(%rbp)
.L222:
	movq	24(%rbx), %rsi
	movq	-72(%rbp), %rdi
	movq	%rbx, %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16(%rbx), %rbx
	movdqa	-64(%rbp), %xmm0
	movq	%rax, 432(%r15)
	movq	528(%r15), %rdi
	addq	$80, %rax
	movq	%rax, 560(%r15)
	leaq	544(%r15), %rax
	movups	%xmm0, 448(%r15)
	cmpq	%rax, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	512(%r15), %rdi
	movq	%rax, 456(%r15)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 432(%r15)
	movq	-24(%r14), %rax
	leaq	560(%r15), %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, 432(%r15,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, 448(%r15)
	movq	-24(%r13), %rax
	movq	%rdx, 448(%r15,%rax)
	movq	%r12, 432(%r15)
	movq	-24(%r12), %rax
	movq	%rcx, 432(%r15,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 560(%r15)
	movq	$0, 440(%r15)
	call	_ZNSt8ios_baseD2Ev@PLT
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-64(%rbp), %xmm1
	movq	136(%r15), %rdi
	movq	%rax, 40(%r15)
	addq	$80, %rax
	movq	%rax, 168(%r15)
	leaq	152(%r15), %rax
	movups	%xmm1, 56(%r15)
	cmpq	%rax, %rdi
	je	.L220
	call	_ZdlPv@PLT
.L220:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	120(%r15), %rdi
	movq	%rax, 64(%r15)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 40(%r15)
	movq	-24(%r14), %rax
	leaq	168(%r15), %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rdx, 40(%r15,%rax)
	movq	%r13, 56(%r15)
	movq	-24(%r13), %rax
	movq	%rsi, 56(%r15,%rax)
	movq	%r12, 40(%r15)
	movq	-24(%r12), %rax
	movq	%rcx, 40(%r15,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, 48(%r15)
	movq	%rax, 168(%r15)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L222
.L217:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10415:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E:
.LFB10507:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L242
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L231:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L229
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L227
.L230:
	movq	%rbx, %r12
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L230
.L227:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE10507:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E:
.LFB10515:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L260
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L249:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L247
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L245
.L248:
	movq	%rbx, %r12
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L248
.L245:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE10515:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_:
.LFB11561:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA11561
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%rdi, %rsi
	je	.L264
	movq	8(%rsi), %rax
	movq	(%rsi), %rbx
	movq	%rsi, %r12
	movq	(%rdi), %r15
	movq	16(%rdi), %rdx
	movq	%rax, -72(%rbp)
	subq	%rbx, %rax
	movq	%rax, %rcx
	subq	%r15, %rdx
	movq	%rax, -88(%rbp)
	sarq	$5, %rcx
	sarq	$5, %rdx
	movq	%rcx, %rax
	cmpq	%rcx, %rdx
	jb	.L357
	movq	8(%rdi), %rcx
	movq	%rcx, %rdx
	movq	%rcx, -80(%rbp)
	subq	%r15, %rdx
	movq	%rdx, %r14
	sarq	$5, %r14
	cmpq	%r14, %rax
	ja	.L290
	cmpq	$0, -88(%rbp)
	movq	%r15, %r12
	jle	.L356
	.p2align 4,,10
	.p2align 3
.L291:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	addq	$32, %rbx
.LEHB0:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-72(%rbp), %rax
	addq	$32, %r12
	subq	$1, %rax
	jne	.L291
	movq	-88(%rbp), %rcx
	movl	$32, %eax
	testq	%rcx, %rcx
	cmovg	%rcx, %rax
	addq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L356:
	cmpq	%r15, -80(%rbp)
	je	.L353
.L298:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L295
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%r15, -80(%rbp)
	jne	.L298
.L353:
	movq	-88(%rbp), %rax
	addq	0(%r13), %rax
.L289:
	movq	%rax, 8(%r13)
.L264:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L358
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L313
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %rcx
	ja	.L359
	movq	-88(%rbp), %rdi
	call	_Znwm@PLT
.LEHE0:
	movq	%rax, -96(%rbp)
.L266:
	leaq	-64(%rbp), %rax
	movq	-96(%rbp), %r14
	movq	%rax, -80(%rbp)
	cmpq	-72(%rbp), %rbx
	jne	.L276
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L272:
	cmpq	$1, %r12
	jne	.L274
	movzbl	(%r15), %eax
	movb	%al, 16(%r14)
.L275:
	movq	%r12, 8(%r14)
	addq	$32, %rbx
	addq	$32, %r14
	movb	$0, (%rdi,%r12)
	cmpq	%rbx, -72(%rbp)
	je	.L277
.L276:
	leaq	16(%r14), %rdi
	movq	%rdi, (%r14)
	movq	(%rbx), %r15
	movq	8(%rbx), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L271
	testq	%r15, %r15
	je	.L360
.L271:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	jbe	.L272
	movq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
.LEHB1:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE1:
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r14)
.L273:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	(%r14), %rdi
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L295:
	addq	$32, %r15
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L290:
	testq	%rdx, %rdx
	jle	.L299
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	addq	$32, %rbx
	addq	$32, %r15
.LEHB2:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE2:
	subq	$1, %r14
	jne	.L300
	movq	8(%r12), %rsi
	movq	8(%r13), %rax
	movq	0(%r13), %r15
	movq	(%r12), %rbx
	movq	%rsi, -72(%rbp)
	movq	%rax, -80(%rbp)
	subq	%r15, %rax
	movq	%rax, %rdx
.L299:
	leaq	-64(%rbp), %rax
	movq	-80(%rbp), %r14
	addq	%rdx, %rbx
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	addq	%r15, %rax
	cmpq	-72(%rbp), %rbx
	jne	.L301
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L363:
	movzbl	(%r15), %eax
	movb	%al, (%rdi)
	movq	-64(%rbp), %r12
	movq	(%r14), %rdi
.L306:
	movq	%r12, 8(%r14)
	addq	$32, %rbx
	addq	$32, %r14
	movb	$0, (%rdi,%r12)
	cmpq	-72(%rbp), %rbx
	je	.L353
.L301:
	leaq	16(%r14), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %r15
	movq	8(%rbx), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L302
	testq	%r15, %r15
	je	.L361
.L302:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L362
	movq	(%r14), %rdi
	cmpq	$1, %r12
	je	.L363
	testq	%r12, %r12
	je	.L306
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L362:
	movq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
.LEHB3:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE3:
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r14)
.L304:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	(%r14), %rdi
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L313:
	movq	$0, -96(%rbp)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L274:
	testq	%r12, %r12
	je	.L275
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L277:
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L269
	.p2align 4,,10
	.p2align 3
.L270:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L285
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L270
.L286:
	movq	0(%r13), %r12
.L269:
	testq	%r12, %r12
	je	.L288
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L288:
	movq	-96(%rbp), %rax
	movq	%rax, 0(%r13)
	addq	-88(%rbp), %rax
	movq	%rax, 16(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L285:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L270
	jmp	.L286
.L358:
	call	__stack_chk_fail@PLT
.L360:
	leaq	.LC0(%rip), %rdi
.LEHB4:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE4:
.L361:
	leaq	.LC0(%rip), %rdi
.LEHB5:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE5:
.L359:
.LEHB6:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE6:
.L320:
	endbr64
	movq	%rax, %rdi
	jmp	.L307
.L318:
	endbr64
	movq	%rax, %rdi
	jmp	.L278
.L307:
	call	__cxa_begin_catch@PLT
.L310:
	cmpq	%r14, -80(%rbp)
	jne	.L364
.LEHB7:
	call	__cxa_rethrow@PLT
.LEHE7:
.L278:
	call	__cxa_begin_catch@PLT
	movq	-96(%rbp), %rbx
.L281:
	cmpq	%rbx, %r14
	jne	.L365
.LEHB8:
	call	__cxa_rethrow@PLT
.LEHE8:
.L364:
	movq	-80(%rbp), %rax
	movq	(%rax), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L309
	call	_ZdlPv@PLT
.L309:
	addq	$32, -80(%rbp)
	jmp	.L310
.L319:
	endbr64
	movq	%rax, %r12
	jmp	.L311
.L365:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L280
	call	_ZdlPv@PLT
.L280:
	addq	$32, %rbx
	jmp	.L281
.L311:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB9:
	call	_Unwind_Resume@PLT
.LEHE9:
.L317:
	endbr64
	movq	%rax, %r12
.L282:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
	call	__cxa_begin_catch@PLT
	cmpq	$0, -96(%rbp)
	je	.L283
	movq	-96(%rbp), %rdi
	call	_ZdlPv@PLT
.L283:
.LEHB10:
	call	__cxa_rethrow@PLT
.LEHE10:
.L316:
	endbr64
	movq	%rax, %r12
.L284:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB11:
	call	_Unwind_Resume@PLT
.LEHE11:
	.cfi_endproc
.LFE11561:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,"aG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,comdat
	.align 4
.LLSDA11561:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT11561-.LLSDATTD11561
.LLSDATTD11561:
	.byte	0x1
	.uleb128 .LLSDACSE11561-.LLSDACSB11561
.LLSDACSB11561:
	.uleb128 .LEHB0-.LFB11561
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB11561
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L318-.LFB11561
	.uleb128 0x1
	.uleb128 .LEHB2-.LFB11561
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB11561
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L320-.LFB11561
	.uleb128 0x1
	.uleb128 .LEHB4-.LFB11561
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L318-.LFB11561
	.uleb128 0x1
	.uleb128 .LEHB5-.LFB11561
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L320-.LFB11561
	.uleb128 0x1
	.uleb128 .LEHB6-.LFB11561
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB11561
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L319-.LFB11561
	.uleb128 0
	.uleb128 .LEHB8-.LFB11561
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L317-.LFB11561
	.uleb128 0x3
	.uleb128 .LEHB9-.LFB11561
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB10-.LFB11561
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L316-.LFB11561
	.uleb128 0
	.uleb128 .LEHB11-.LFB11561
	.uleb128 .LEHE11-.LEHB11
	.uleb128 0
	.uleb128 0
.LLSDACSE11561:
	.byte	0x1
	.byte	0
	.byte	0
	.byte	0x7d
	.align 4
	.long	0

.LLSDATT11561:
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,comdat
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag:
.LFB12233:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA12233
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %r8d
	movq	$15, -64(%rbp)
	sete	-73(%rbp)
	xorl	%r12d, %r12d
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L399:
	movzbl	-73(%rbp), %edx
.L368:
	cmpb	%cl, %dl
	je	.L392
	cmpq	%r12, -64(%rbp)
	jbe	.L392
	leaq	1(%r12), %rdx
	addq	(%r14), %r12
	testb	%sil, %sil
	jne	.L422
.L371:
	movb	%r13b, (%r12)
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L374
.L434:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
.L375:
	movq	%rdx, %r12
	movl	$-1, %r13d
.L376:
	cmpl	$-1, %r13d
	sete	%cl
	testq	%rbx, %rbx
	setne	%al
	andb	%cl, %al
	movl	%eax, %esi
	jne	.L423
.L367:
	testq	%r15, %r15
	setne	%dl
	andb	-73(%rbp), %dl
	je	.L399
	movq	24(%r15), %rax
	cmpq	%rax, 16(%r15)
	jnb	.L424
	xorl	%edx, %edx
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L428:
	movq	(%r14), %rcx
.L381:
	addq	%rcx, %r12
	testq	%rbx, %rbx
	je	.L387
	cmpb	$0, -72(%rbp)
	jne	.L425
.L387:
	movb	%r13b, (%r12)
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L390
.L435:
	addq	$1, %rax
	movq	%rax, 16(%rbx)
.L391:
	movq	-88(%rbp), %r12
	movl	$-1, %r13d
.L392:
	cmpl	$-1, %r13d
	sete	-72(%rbp)
	testq	%rbx, %rbx
	movzbl	-72(%rbp), %edi
	setne	%al
	andb	%dil, %al
	movb	%al, -88(%rbp)
	jne	.L426
	movb	%dil, -88(%rbp)
.L377:
	testq	%r15, %r15
	setne	%al
	andb	-73(%rbp), %al
	movb	%al, -96(%rbp)
	jne	.L427
	movzbl	-73(%rbp), %eax
	movb	%al, -96(%rbp)
	movl	%eax, %esi
.L378:
	cmpb	%sil, -88(%rbp)
	je	.L379
.L431:
	leaq	1(%r12), %rax
	movq	%rax, -88(%rbp)
	cmpq	%r12, -64(%rbp)
	jne	.L428
	leaq	1(%r12), %rax
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
.LEHB12:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE12:
	movq	(%r14), %rdi
	movq	%rax, %rcx
	cmpq	$1, %r12
	je	.L429
	testq	%r12, %r12
	je	.L383
	movq	%rdi, %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	(%r14), %rdi
	movq	%rax, %rcx
.L383:
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L384
	movq	%rcx, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rcx
.L384:
	movq	-64(%rbp), %rax
	movq	%rcx, (%r14)
	movq	%rax, 16(%r14)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L423:
	movq	24(%rbx), %rax
	xorl	%ecx, %ecx
	cmpq	%rax, 16(%rbx)
	jb	.L367
	movq	(%rbx), %rax
	movb	%cl, -88(%rbp)
	movq	%rbx, %rdi
	movb	%sil, -72(%rbp)
.LEHB13:
	call	*72(%rax)
	movzbl	-72(%rbp), %esi
	movzbl	-88(%rbp), %ecx
	cmpl	$-1, %eax
	jne	.L367
	movl	%esi, %ecx
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L427:
	movq	24(%r15), %rax
	cmpq	%rax, 16(%r15)
	jnb	.L430
	movb	$0, -96(%rbp)
	movzbl	-96(%rbp), %esi
	cmpb	%sil, -88(%rbp)
	jne	.L431
.L379:
	movq	(%r14), %rax
	movq	%r12, 8(%r14)
	movb	$0, (%rax,%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L432
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	24(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	jnb	.L433
	movb	$0, -88(%rbp)
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L422:
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L372
	movzbl	(%rax), %r13d
	movb	%r13b, (%r12)
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L434
.L374:
	movq	(%rbx), %rax
	movq	%rdx, -72(%rbp)
	movq	%rbx, %rdi
	call	*80(%rax)
.LEHE13:
	movq	-72(%rbp), %rdx
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L425:
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jnb	.L388
	movzbl	(%rax), %r13d
	movb	%r13b, (%r12)
	movq	16(%rbx), %rax
	cmpq	24(%rbx), %rax
	jb	.L435
.L390:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
.LEHB14:
	call	*80(%rax)
.LEHE14:
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L429:
	movzbl	(%rdi), %eax
	movb	%al, (%rcx)
	movq	(%r14), %rdi
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L424:
	movq	(%r15), %rax
	movb	%cl, -96(%rbp)
	movq	%r15, %rdi
	movb	%sil, -88(%rbp)
	movb	%dl, -72(%rbp)
.LEHB15:
	call	*72(%rax)
.LEHE15:
	movzbl	-72(%rbp), %edx
	movl	$0, %ecx
	movzbl	-88(%rbp), %esi
	cmpl	$-1, %eax
	movl	$0, %eax
	cmovne	%ecx, %edx
	cmove	%rax, %r15
	movzbl	-96(%rbp), %ecx
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L433:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
.LEHB16:
	call	*72(%rax)
	cmpl	$-1, %eax
	movl	$0, %ecx
	movl	$0, %eax
	cmove	%rcx, %rbx
	movzbl	-88(%rbp), %ecx
	cmovne	%eax, %ecx
	movb	%cl, -88(%rbp)
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L430:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*72(%rax)
.LEHE16:
	cmpl	$-1, %eax
	movl	$0, %ecx
	movl	$0, %eax
	cmove	%rcx, %r15
	movzbl	-96(%rbp), %ecx
	cmovne	%eax, %ecx
	movb	%cl, -96(%rbp)
	movl	%ecx, %esi
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L372:
	movq	(%rbx), %rax
	movq	%rdx, -72(%rbp)
	movq	%rbx, %rdi
.LEHB17:
	call	*72(%rax)
.LEHE17:
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L389
	movq	-72(%rbp), %rdx
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L388:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
.LEHB18:
	call	*72(%rax)
.LEHE18:
	movl	%eax, %r13d
	cmpl	$-1, %eax
	jne	.L387
.L389:
	movb	$-1, (%r12)
	movq	16, %rax
	ud2
.L432:
	call	__stack_chk_fail@PLT
.L408:
	endbr64
	movq	%rax, %rdi
.L393:
	call	__cxa_begin_catch@PLT
	movq	(%r14), %rdi
	addq	$16, %r14
	cmpq	%r14, %rdi
	je	.L394
	call	_ZdlPv@PLT
.L394:
.LEHB19:
	call	__cxa_rethrow@PLT
.LEHE19:
.L409:
	endbr64
	movq	%rax, %r12
.L395:
	call	__cxa_end_catch@PLT
	movq	%r12, %rdi
.LEHB20:
	call	_Unwind_Resume@PLT
.LEHE20:
	.cfi_endproc
.LFE12233:
	.section	.gcc_except_table._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,"aG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,comdat
	.align 4
.LLSDA12233:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT12233-.LLSDATTD12233
.LLSDATTD12233:
	.byte	0x1
	.uleb128 .LLSDACSE12233-.LLSDACSB12233
.LLSDACSB12233:
	.uleb128 .LEHB12-.LFB12233
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L408-.LFB12233
	.uleb128 0x1
	.uleb128 .LEHB13-.LFB12233
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB14-.LFB12233
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L408-.LFB12233
	.uleb128 0x1
	.uleb128 .LEHB15-.LFB12233
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB16-.LFB12233
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L408-.LFB12233
	.uleb128 0x1
	.uleb128 .LEHB17-.LFB12233
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB18-.LFB12233
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L408-.LFB12233
	.uleb128 0x1
	.uleb128 .LEHB19-.LFB12233
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L409-.LFB12233
	.uleb128 0
	.uleb128 .LEHB20-.LFB12233
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0
	.uleb128 0
.LLSDACSE12233:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT12233:
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag,comdat
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LCOLDB1:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"ax",@progbits
.LHOTB1:
	.p2align 4
	.type	_ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB7169:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7169
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r14
	leaq	-576(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -616(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movw	%dx, -96(%rbp)
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rbx, -576(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -576(%rbp,%rax)
	movq	-576(%rbp), %rax
	movq	$0, -568(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
.LEHB21:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE21:
	leaq	24+_ZTVSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	leaq	-560(%rbp), %r13
	movq	%rax, -576(%rbp)
	movq	%r13, %rdi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
.LEHB22:
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
.LEHE22:
	movq	%r13, %rsi
	movq	%r14, %rdi
.LEHB23:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-616(%rbp), %rax
	movl	$8, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	-576(%rbp), %rdx
	movq	-24(%rdx), %rdi
	addq	%r15, %rdi
	testq	%rax, %rax
	je	.L459
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.LEHE23:
.L438:
	movl	-288(%rbp), %eax
	testl	%eax, %eax
	je	.L460
	movb	$0, (%r12)
	movb	$0, 8(%r12)
.L444:
	leaq	24+_ZTVSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -576(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -560(%rbp)
.LEHB24:
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
.LEHE24:
.L448:
	leaq	-456(%rbp), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-504(%rbp), %rdi
	movq	%rax, -560(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -576(%rbp)
	movq	-24(%rbx), %rax
	movq	%r14, %rdi
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rcx, -576(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -568(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L461
	addq	$584, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	.cfi_restore_state
	movq	-576(%rbp), %rax
	movl	$4294967295, %edx
	leaq	-608(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%rdx, %r8
	movq	-24(%rax), %rax
	movq	-344(%rbp,%rax), %rsi
	leaq	-592(%rbp), %rax
	movq	%rax, -616(%rbp)
	movq	%rax, -608(%rbp)
.LEHB25:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructISt19istreambuf_iteratorIcS2_EEEvT_S8_St18input_iterator_tag
.LEHE25:
	leaq	24(%r12), %rax
	movb	$1, (%r12)
	movq	%rax, 8(%r12)
	movq	-608(%rbp), %rax
	cmpq	-616(%rbp), %rax
	je	.L462
	movq	%rax, 8(%r12)
	movq	-592(%rbp), %rax
	movq	%rax, 24(%r12)
.L446:
	movq	-600(%rbp), %rax
	movq	%rax, 16(%r12)
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L459:
	movl	32(%rdi), %esi
	orl	$4, %esi
.LEHB26:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.LEHE26:
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L462:
	movdqa	-592(%rbp), %xmm1
	movups	%xmm1, 24(%r12)
	jmp	.L446
.L461:
	call	__stack_chk_fail@PLT
.L455:
	endbr64
	movq	%rax, %rdi
	jmp	.L447
.L453:
	endbr64
	movq	%rax, %r12
	jmp	.L442
.L451:
	endbr64
	movq	%rax, %r12
	jmp	.L449
.L452:
	endbr64
	movq	%rax, %r12
	jmp	.L443
.L454:
	endbr64
	movq	%rax, %r12
	jmp	.L441
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"a",@progbits
	.align 4
.LLSDA7169:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT7169-.LLSDATTD7169
.LLSDATTD7169:
	.byte	0x1
	.uleb128 .LLSDACSE7169-.LLSDACSB7169
.LLSDACSB7169:
	.uleb128 .LEHB21-.LFB7169
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L452-.LFB7169
	.uleb128 0
	.uleb128 .LEHB22-.LFB7169
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L453-.LFB7169
	.uleb128 0
	.uleb128 .LEHB23-.LFB7169
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L454-.LFB7169
	.uleb128 0
	.uleb128 .LEHB24-.LFB7169
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L455-.LFB7169
	.uleb128 0x1
	.uleb128 .LEHB25-.LFB7169
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L451-.LFB7169
	.uleb128 0
	.uleb128 .LEHB26-.LFB7169
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L454-.LFB7169
	.uleb128 0
.LLSDACSE7169:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATT7169:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC7169
	.type	_ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, @function
_ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold:
.LFSB7169:
.L447:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	__cxa_begin_catch@PLT
	call	__cxa_end_catch@PLT
	jmp	.L448
.L441:
	movq	%r13, %rdi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEED1Ev@PLT
.L442:
	movq	%rbx, -576(%rbp)
	movq	-24(%rbx), %rax
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rbx
	movq	%rbx, -576(%rbp,%rax)
	movq	$0, -568(%rbp)
.L443:
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
.LEHB27:
	call	_Unwind_Resume@PLT
.L449:
	movq	%r15, %rdi
	call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE27:
	.cfi_endproc
.LFE7169:
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 4
.LLSDAC7169:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC7169-.LLSDATTDC7169
.LLSDATTDC7169:
	.byte	0x1
	.uleb128 .LLSDACSEC7169-.LLSDACSBC7169
.LLSDACSBC7169:
	.uleb128 .LEHB27-.LCOLDB1
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0
	.uleb128 0
.LLSDACSEC7169:
	.byte	0x1
	.byte	0
	.align 4
	.long	0

.LLSDATTC7169:
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.size	_ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold, .-_ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.cold
.LCOLDE1:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LHOTE1:
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E:
.LFB13346:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L471
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L465:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L465
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE13346:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	.section	.text._ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,"axG",@progbits,_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.type	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, @function
_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E:
.LFB9411:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L488
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
.L478:
	movq	24(%r12), %rsi
	movq	%r12, %r13
	movq	%r14, %rdi
	leaq	40(%r13), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r13), %rbx
	movq	16(%r12), %r12
	testq	%rbx, %rbx
	je	.L476
.L477:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L477
.L476:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L478
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE9411:
	.size	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E, .-_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	.section	.text._ZN2v88internal6torque3AstD2Ev,"axG",@progbits,_ZN2v88internal6torque3AstD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque3AstD2Ev
	.type	_ZN2v88internal6torque3AstD2Ev, @function
_ZN2v88internal6torque3AstD2Ev:
.LFB7221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	48(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	64(%rdi), %r12
	testq	%r12, %r12
	je	.L496
.L492:
	movq	24(%r12), %rsi
	movq	%r12, %r14
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r14), %r13
	movq	16(%r12), %r12
	leaq	40(%r14), %rax
	testq	%r13, %r13
	je	.L497
.L495:
	movq	24(%r13), %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	testq	%r13, %r13
	jne	.L495
.L497:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L492
.L496:
	movq	32(%rbx), %r13
	movq	24(%rbx), %r12
	cmpq	%r12, %r13
	je	.L493
	.p2align 4,,10
	.p2align 3
.L494:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L498
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L494
.L499:
	movq	24(%rbx), %r12
.L493:
	testq	%r12, %r12
	je	.L501
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L501:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L491
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L494
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L491:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7221:
	.size	_ZN2v88internal6torque3AstD2Ev, .-_ZN2v88internal6torque3AstD2Ev
	.weak	_ZN2v88internal6torque3AstD1Ev
	.set	_ZN2v88internal6torque3AstD1Ev,_ZN2v88internal6torque3AstD2Ev
	.section	.text._ZN2v88internal6torque13GlobalContextD2Ev,"axG",@progbits,_ZN2v88internal6torque13GlobalContextD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque13GlobalContextD2Ev
	.type	_ZN2v88internal6torque13GlobalContextD2Ev, @function
_ZN2v88internal6torque13GlobalContextD2Ev:
.LFB7259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	208(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L517
	call	_ZdlPv@PLT
.L517:
	movq	176(%rbx), %r12
	leaq	160(%rbx), %rax
	movq	%rax, -72(%rbp)
	testq	%r12, %r12
	je	.L523
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	%rdx, %xmm0
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -64(%rbp)
.L524:
	movq	24(%r12), %rsi
	movq	-72(%rbp), %rdi
	movq	%r12, %r13
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%r12), %r12
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-64(%rbp), %xmm1
	movq	%rax, 432(%r13)
	movq	528(%r13), %rdi
	addq	$80, %rax
	movq	%rax, 560(%r13)
	leaq	544(%r13), %rax
	movups	%xmm1, 448(%r13)
	cmpq	%rax, %rdi
	je	.L521
	call	_ZdlPv@PLT
.L521:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	512(%r13), %rdi
	movq	%rax, 456(%r13)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, 432(%r13)
	movq	-24(%r15), %rax
	leaq	560(%r13), %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, 432(%r13,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, 448(%r13)
	movq	-24(%r14), %rax
	movq	%rdx, 448(%r13,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 432(%r13)
	movq	-24(%rax), %rax
	movq	%rcx, 432(%r13,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 560(%r13)
	movq	$0, 440(%r13)
	call	_ZNSt8ios_baseD2Ev@PLT
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-64(%rbp), %xmm2
	movq	136(%r13), %rdi
	movq	%rax, 40(%r13)
	addq	$80, %rax
	movq	%rax, 168(%r13)
	leaq	152(%r13), %rax
	movups	%xmm2, 56(%r13)
	cmpq	%rax, %rdi
	je	.L522
	call	_ZdlPv@PLT
.L522:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	120(%r13), %rdi
	movq	%rax, 64(%r13)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, 40(%r13)
	movq	-24(%r15), %rax
	leaq	168(%r13), %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rdx, 40(%r13,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r14, 56(%r13)
	movq	-24(%r14), %rax
	movq	%rcx, 56(%r13,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 40(%r13)
	movq	-24(%rax), %rax
	movq	%rdx, 40(%r13,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, 48(%r13)
	movq	%rax, 168(%r13)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L524
.L523:
	movq	144(%rbx), %r13
	movq	136(%rbx), %r12
	cmpq	%r12, %r13
	je	.L519
	.p2align 4,,10
	.p2align 3
.L520:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L525
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L520
.L526:
	movq	136(%rbx), %r12
.L519:
	testq	%r12, %r12
	je	.L528
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L528:
	movq	120(%rbx), %r13
	movq	112(%rbx), %r12
	cmpq	%r12, %r13
	je	.L529
	.p2align 4,,10
	.p2align 3
.L533:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L530
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L533
.L531:
	movq	112(%rbx), %r12
.L529:
	testq	%r12, %r12
	je	.L534
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L534:
	movq	80(%rbx), %r12
	leaq	64(%rbx), %r15
	testq	%r12, %r12
	je	.L539
	movq	%rbx, -64(%rbp)
.L535:
	movq	24(%r12), %rsi
	movq	%r12, %r14
	movq	%r15, %rdi
	leaq	40(%r14), %rbx
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r14), %r13
	movq	16(%r12), %r12
	testq	%r13, %r13
	je	.L540
.L538:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L538
.L540:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L535
	movq	-64(%rbp), %rbx
.L539:
	movq	48(%rbx), %r13
	movq	40(%rbx), %r12
	cmpq	%r12, %r13
	je	.L536
	.p2align 4,,10
	.p2align 3
.L537:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L541
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L537
.L542:
	movq	40(%rbx), %r12
.L536:
	testq	%r12, %r12
	je	.L544
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L544:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L516
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L541:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L537
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L530:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L533
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L525:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L520
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L516:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7259:
	.size	_ZN2v88internal6torque13GlobalContextD2Ev, .-_ZN2v88internal6torque13GlobalContextD2Ev
	.weak	_ZN2v88internal6torque13GlobalContextD1Ev
	.set	_ZN2v88internal6torque13GlobalContextD1Ev,_ZN2v88internal6torque13GlobalContextD2Ev
	.section	.text._ZN2v88internal6torque20TorqueCompilerResultD2Ev,"axG",@progbits,_ZN2v88internal6torque20TorqueCompilerResultD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque20TorqueCompilerResultD2Ev
	.type	_ZN2v88internal6torque20TorqueCompilerResultD2Ev, @function
_ZN2v88internal6torque20TorqueCompilerResultD2Ev:
.LFB7392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	184(%rdi), %rbx
	movq	176(%rdi), %r12
	movq	%rdi, -56(%rbp)
	cmpq	%r12, %rbx
	je	.L574
	.p2align 4,,10
	.p2align 3
.L578:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L575
	call	_ZdlPv@PLT
	addq	$64, %r12
	cmpq	%r12, %rbx
	jne	.L578
.L576:
	movq	-56(%rbp), %rax
	movq	176(%rax), %r12
.L574:
	testq	%r12, %r12
	je	.L579
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L579:
	movq	-56(%rbp), %rax
	movq	168(%rax), %r12
	leaq	64(%rax), %rcx
	movq	%rcx, -64(%rbp)
	testq	%r12, %r12
	je	.L580
	movq	216(%r12), %rbx
	movq	208(%r12), %r13
	cmpq	%r13, %rbx
	je	.L581
	.p2align 4,,10
	.p2align 3
.L585:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L582
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L585
	movq	208(%r12), %r13
.L581:
	testq	%r13, %r13
	je	.L586
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L586:
	movq	192(%r12), %rbx
	movq	184(%r12), %r13
	cmpq	%r13, %rbx
	je	.L587
	.p2align 4,,10
	.p2align 3
.L591:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L588
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L591
	movq	184(%r12), %r13
.L587:
	testq	%r13, %r13
	je	.L592
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L592:
	movq	168(%r12), %rbx
	movq	160(%r12), %r13
	cmpq	%r13, %rbx
	je	.L593
	.p2align 4,,10
	.p2align 3
.L597:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L594
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L597
	movq	160(%r12), %r13
.L593:
	testq	%r13, %r13
	je	.L598
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L598:
	movq	144(%r12), %rbx
	movq	136(%r12), %r13
	cmpq	%r13, %rbx
	je	.L599
	.p2align 4,,10
	.p2align 3
.L603:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L600
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L603
	movq	136(%r12), %r13
.L599:
	testq	%r13, %r13
	je	.L604
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L604:
	movq	96(%r12), %rbx
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -80(%rbp)
	testq	%rbx, %rbx
	je	.L612
	movq	%r12, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L605:
	movq	%rbx, %r15
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	(%rbx), %rbx
	movq	96(%r15), %r13
	movq	%rax, 8(%r15)
	leaq	80(%r15), %r14
	testq	%r13, %r13
	je	.L611
.L608:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L608
.L611:
	movq	-80(%rbp), %rax
	movq	48(%r15), %r12
	leaq	32(%r15), %r14
	movq	%rax, 8(%r15)
	testq	%r12, %r12
	je	.L609
.L610:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L613
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L609
.L614:
	movq	%r13, %r12
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L613:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L614
.L609:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L605
	movq	-88(%rbp), %r12
.L612:
	movq	88(%r12), %rax
	movq	80(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%r12), %rdi
	leaq	128(%r12), %rax
	movq	$0, 104(%r12)
	movq	$0, 96(%r12)
	cmpq	%rax, %rdi
	je	.L606
	call	_ZdlPv@PLT
.L606:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L615
	call	_ZdlPv@PLT
.L615:
	movq	16(%r12), %rbx
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -80(%rbp)
	testq	%rbx, %rbx
	je	.L621
	movq	%r12, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L616:
	movq	%rbx, %r13
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	(%rbx), %rbx
	movq	80(%r13), %rdi
	movq	%rax, 8(%r13)
	testq	%rdi, %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	-80(%rbp), %rax
	movq	48(%r13), %r12
	leaq	32(%r13), %r15
	movq	%rax, 8(%r13)
	testq	%r12, %r12
	je	.L623
.L620:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r14
	cmpq	%rax, %rdi
	je	.L622
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L623
.L624:
	movq	%r14, %r12
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L622:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L624
.L623:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L616
	movq	-88(%rbp), %r12
.L621:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L617
	call	_ZdlPv@PLT
.L617:
	movl	$232, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L580:
	movq	-56(%rbp), %rax
	movq	160(%rax), %r12
	testq	%r12, %r12
	je	.L625
	movq	208(%r12), %rdi
	testq	%rdi, %rdi
	je	.L626
	call	_ZdlPv@PLT
.L626:
	movq	176(%r12), %rbx
	leaq	160(%r12), %rax
	movq	%rax, %rcx
	testq	%rbx, %rbx
	je	.L632
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, -88(%rbp)
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	%rdx, %xmm0
	movq	%rax, %xmm3
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	movq	%rcx, %r13
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
.L633:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	movq	%rbx, %r12
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16(%rbx), %rbx
	movdqa	-80(%rbp), %xmm1
	movq	%rax, 432(%r12)
	movq	528(%r12), %rdi
	addq	$80, %rax
	movq	%rax, 560(%r12)
	leaq	544(%r12), %rax
	movups	%xmm1, 448(%r12)
	cmpq	%rax, %rdi
	je	.L630
	call	_ZdlPv@PLT
.L630:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	512(%r12), %rdi
	movq	%rax, 456(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 432(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	560(%r12), %rdi
	movq	-24(%r14), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, 432(%r12,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 448(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 448(%r12,%rax)
	movq	%r15, 432(%r12)
	movq	-24(%r15), %rax
	movq	%rdx, 432(%r12,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 560(%r12)
	movq	$0, 440(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-80(%rbp), %xmm2
	movq	136(%r12), %rdi
	movq	%rax, 40(%r12)
	addq	$80, %rax
	movq	%rax, 168(%r12)
	leaq	152(%r12), %rax
	movups	%xmm2, 56(%r12)
	cmpq	%rax, %rdi
	je	.L631
	call	_ZdlPv@PLT
.L631:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	120(%r12), %rdi
	movq	%rax, 64(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 40(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	168(%r12), %rdi
	movq	-24(%r14), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, 40(%r12,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 56(%r12)
	movq	-24(%rax), %rax
	movq	%rdx, 56(%r12,%rax)
	movq	%r15, 40(%r12)
	movq	-24(%r15), %rax
	movq	%rcx, 40(%r12,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, 48(%r12)
	movq	%rax, 168(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L633
	movq	-88(%rbp), %r12
.L632:
	movq	144(%r12), %rbx
	movq	136(%r12), %r13
	cmpq	%r13, %rbx
	je	.L628
	.p2align 4,,10
	.p2align 3
.L629:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L634
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L629
	movq	136(%r12), %r13
.L628:
	testq	%r13, %r13
	je	.L637
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L637:
	movq	120(%r12), %rbx
	movq	112(%r12), %r13
	cmpq	%r13, %rbx
	je	.L638
	.p2align 4,,10
	.p2align 3
.L642:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L639
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L642
	movq	112(%r12), %r13
.L638:
	testq	%r13, %r13
	je	.L643
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L643:
	movq	80(%r12), %r14
	leaq	64(%r12), %r13
	testq	%r14, %r14
	je	.L648
	movq	%r12, -80(%rbp)
.L644:
	movq	24(%r14), %rsi
	movq	%r14, %r15
	movq	%r13, %rdi
	leaq	40(%r15), %r12
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r15), %rbx
	movq	16(%r14), %r14
	testq	%rbx, %rbx
	je	.L649
.L647:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L647
.L649:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L644
	movq	-80(%rbp), %r12
.L648:
	movq	48(%r12), %rbx
	movq	40(%r12), %r13
	cmpq	%r13, %rbx
	je	.L645
	.p2align 4,,10
	.p2align 3
.L646:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L650
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L646
	movq	40(%r12), %r13
.L645:
	testq	%r13, %r13
	je	.L653
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L653:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L625:
	movq	-56(%rbp), %rax
	movq	128(%rax), %r12
	leaq	112(%rax), %r13
	testq	%r12, %r12
	je	.L659
.L655:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L658
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L659
.L660:
	movq	%rbx, %r12
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L594:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L597
	movq	160(%r12), %r13
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L600:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L603
	movq	136(%r12), %r13
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L650:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L646
	movq	40(%r12), %r13
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L575:
	addq	$64, %r12
	cmpq	%r12, %rbx
	jne	.L578
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L634:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L629
	movq	136(%r12), %r13
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L639:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L642
	movq	112(%r12), %r13
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L582:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L585
	movq	208(%r12), %r13
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L588:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L591
	movq	184(%r12), %r13
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L658:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L660
.L659:
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %r13
	movq	80(%rax), %r12
	testq	%r12, %r12
	je	.L656
.L657:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L663
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L656
.L664:
	movq	%rbx, %r12
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L664
.L656:
	movq	-56(%rbp), %rax
	cmpb	$0, (%rax)
	jne	.L783
.L573:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L783:
	.cfi_restore_state
	movq	32(%rax), %rdi
	addq	$48, %rax
	cmpq	%rax, %rdi
	je	.L665
	call	_ZdlPv@PLT
.L665:
	movq	-56(%rbp), %rax
	movq	16(%rax), %rbx
	movq	8(%rax), %r12
	cmpq	%r12, %rbx
	je	.L666
	.p2align 4,,10
	.p2align 3
.L670:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L667
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L670
.L668:
	movq	-56(%rbp), %rax
	movq	8(%rax), %r12
.L666:
	testq	%r12, %r12
	je	.L573
	addq	$56, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L670
	jmp	.L668
	.cfi_endproc
.LFE7392:
	.size	_ZN2v88internal6torque20TorqueCompilerResultD2Ev, .-_ZN2v88internal6torque20TorqueCompilerResultD2Ev
	.weak	_ZN2v88internal6torque20TorqueCompilerResultD1Ev
	.set	_ZN2v88internal6torque20TorqueCompilerResultD1Ev,_ZN2v88internal6torque20TorqueCompilerResultD2Ev
	.section	.text._ZN2v88internal6torque18LanguageServerDataD2Ev,"axG",@progbits,_ZN2v88internal6torque18LanguageServerDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6torque18LanguageServerDataD2Ev
	.type	_ZN2v88internal6torque18LanguageServerDataD2Ev, @function
_ZN2v88internal6torque18LanguageServerDataD2Ev:
.LFB7378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	104(%rdi), %r12
	movq	%rdi, -56(%rbp)
	testq	%r12, %r12
	je	.L785
	movq	216(%r12), %rbx
	movq	208(%r12), %r13
	cmpq	%r13, %rbx
	je	.L786
	.p2align 4,,10
	.p2align 3
.L790:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L787
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L790
	movq	208(%r12), %r13
.L786:
	testq	%r13, %r13
	je	.L791
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L791:
	movq	192(%r12), %rbx
	movq	184(%r12), %r13
	cmpq	%r13, %rbx
	je	.L792
	.p2align 4,,10
	.p2align 3
.L796:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L793
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L796
	movq	184(%r12), %r13
.L792:
	testq	%r13, %r13
	je	.L797
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L797:
	movq	168(%r12), %rbx
	movq	160(%r12), %r13
	cmpq	%r13, %rbx
	je	.L798
	.p2align 4,,10
	.p2align 3
.L802:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L799
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L802
	movq	160(%r12), %r13
.L798:
	testq	%r13, %r13
	je	.L803
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L803:
	movq	144(%r12), %rbx
	movq	136(%r12), %r13
	cmpq	%r13, %rbx
	je	.L804
	.p2align 4,,10
	.p2align 3
.L808:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L805
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L808
	movq	136(%r12), %r13
.L804:
	testq	%r13, %r13
	je	.L809
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L809:
	movq	96(%r12), %rbx
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -80(%rbp)
	testq	%rbx, %rbx
	je	.L817
	movq	%r12, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%rbx, %r15
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	(%rbx), %rbx
	movq	96(%r15), %r13
	movq	%rax, 8(%r15)
	leaq	80(%r15), %r14
	testq	%r13, %r13
	je	.L816
.L813:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L813
.L816:
	movq	-80(%rbp), %rax
	movq	48(%r15), %r12
	leaq	32(%r15), %r14
	movq	%rax, 8(%r15)
	testq	%r12, %r12
	je	.L814
.L815:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L818
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L814
.L819:
	movq	%r13, %r12
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L818:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L819
.L814:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L810
	movq	-64(%rbp), %r12
.L817:
	movq	88(%r12), %rax
	movq	80(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%r12), %rdi
	leaq	128(%r12), %rax
	movq	$0, 104(%r12)
	movq	$0, 96(%r12)
	cmpq	%rax, %rdi
	je	.L811
	call	_ZdlPv@PLT
.L811:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L820
	call	_ZdlPv@PLT
.L820:
	movq	16(%r12), %rbx
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -80(%rbp)
	testq	%rbx, %rbx
	je	.L826
	movq	%r12, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L821:
	movq	%rbx, %r13
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	(%rbx), %rbx
	movq	80(%r13), %rdi
	movq	%rax, 8(%r13)
	testq	%rdi, %rdi
	je	.L824
	call	_ZdlPv@PLT
.L824:
	movq	-80(%rbp), %rax
	movq	48(%r13), %r12
	leaq	32(%r13), %r15
	movq	%rax, 8(%r13)
	testq	%r12, %r12
	je	.L828
.L825:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r14
	cmpq	%rax, %rdi
	je	.L827
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L828
.L829:
	movq	%r14, %r12
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L827:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L829
.L828:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L821
	movq	-64(%rbp), %r12
.L826:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L822
	call	_ZdlPv@PLT
.L822:
	movl	$232, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L785:
	movq	-56(%rbp), %rax
	movq	96(%rax), %r12
	testq	%r12, %r12
	je	.L830
	movq	208(%r12), %rdi
	testq	%rdi, %rdi
	je	.L831
	call	_ZdlPv@PLT
.L831:
	movq	176(%r12), %rbx
	leaq	160(%r12), %rax
	movq	%rax, %rcx
	testq	%rbx, %rbx
	je	.L837
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, -64(%rbp)
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	%rdx, %xmm0
	movq	%rax, %xmm3
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	movq	%rcx, %r13
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
.L838:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	movq	%rbx, %r12
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16(%rbx), %rbx
	movdqa	-80(%rbp), %xmm1
	movq	%rax, 432(%r12)
	movq	528(%r12), %rdi
	addq	$80, %rax
	movq	%rax, 560(%r12)
	leaq	544(%r12), %rax
	movups	%xmm1, 448(%r12)
	cmpq	%rax, %rdi
	je	.L835
	call	_ZdlPv@PLT
.L835:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	512(%r12), %rdi
	movq	%rax, 456(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 432(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	560(%r12), %rdi
	movq	-24(%r14), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 448(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 448(%r12,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r15, 432(%r12)
	movq	-24(%r15), %rax
	movq	%rcx, 432(%r12,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 560(%r12)
	movq	$0, 440(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movdqa	-80(%rbp), %xmm2
	movq	136(%r12), %rdi
	movq	%rax, 40(%r12)
	addq	$80, %rax
	movq	%rax, 168(%r12)
	leaq	152(%r12), %rax
	movups	%xmm2, 56(%r12)
	cmpq	%rax, %rdi
	je	.L836
	call	_ZdlPv@PLT
.L836:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	120(%r12), %rdi
	movq	%rax, 64(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 40(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	168(%r12), %rdi
	movq	-24(%r14), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rdx, 40(%r12,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, 56(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 56(%r12,%rax)
	movq	%r15, 40(%r12)
	movq	-24(%r15), %rax
	movq	%rdx, 40(%r12,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, 48(%r12)
	movq	%rax, 168(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L838
	movq	-64(%rbp), %r12
.L837:
	movq	144(%r12), %rbx
	movq	136(%r12), %r13
	cmpq	%r13, %rbx
	je	.L833
	.p2align 4,,10
	.p2align 3
.L834:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L839
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L834
	movq	136(%r12), %r13
.L833:
	testq	%r13, %r13
	je	.L842
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L842:
	movq	120(%r12), %rbx
	movq	112(%r12), %r13
	cmpq	%r13, %rbx
	je	.L843
	.p2align 4,,10
	.p2align 3
.L847:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L844
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L847
	movq	112(%r12), %r13
.L843:
	testq	%r13, %r13
	je	.L848
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L848:
	movq	80(%r12), %r13
	leaq	64(%r12), %r14
	testq	%r13, %r13
	je	.L853
	movq	%r12, -80(%rbp)
.L849:
	movq	24(%r13), %rsi
	movq	%r13, %r15
	movq	%r14, %rdi
	leaq	40(%r15), %r12
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r15), %rbx
	movq	16(%r13), %r13
	testq	%rbx, %rbx
	je	.L854
.L852:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L852
.L854:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L849
	movq	-80(%rbp), %r12
.L853:
	movq	48(%r12), %rbx
	movq	40(%r12), %r13
	cmpq	%r13, %rbx
	je	.L850
	.p2align 4,,10
	.p2align 3
.L851:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L855
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L851
	movq	40(%r12), %r13
.L850:
	testq	%r13, %r13
	je	.L858
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L858:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L859
	call	_ZdlPv@PLT
.L859:
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L830:
	movq	-56(%rbp), %rax
	movq	64(%rax), %r12
	leaq	48(%rax), %r13
	testq	%r12, %r12
	je	.L864
.L860:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L863
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L864
.L865:
	movq	%rbx, %r12
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L839:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L834
	movq	136(%r12), %r13
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L863:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L865
.L864:
	movq	-56(%rbp), %rax
	movq	16(%rax), %r12
	movq	%rax, %r13
	testq	%r12, %r12
	je	.L784
.L862:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L866
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L784
.L867:
	movq	%rbx, %r12
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L844:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L847
	movq	112(%r12), %r13
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L855:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L851
	movq	40(%r12), %r13
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L866:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L867
.L784:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L805:
	.cfi_restore_state
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L808
	movq	136(%r12), %r13
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L787:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L790
	movq	208(%r12), %r13
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L793:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L796
	movq	184(%r12), %r13
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L799:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L802
	movq	160(%r12), %r13
	jmp	.L798
	.cfi_endproc
.LFE7378:
	.size	_ZN2v88internal6torque18LanguageServerDataD2Ev, .-_ZN2v88internal6torque18LanguageServerDataD2Ev
	.weak	_ZN2v88internal6torque18LanguageServerDataD1Ev
	.set	_ZN2v88internal6torque18LanguageServerDataD1Ev,_ZN2v88internal6torque18LanguageServerDataD2Ev
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE,"ax",@progbits
.LCOLDB4:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE,"ax",@progbits
.LHOTB4:
	.p2align 4
	.type	_ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE, @function
_ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE:
.LFB7173:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7173
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1512, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
.LEHB28:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
.LEHE28:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	movq	%rdx, -1136(%rbp)
	movq	8(%rax), %rdx
	movq	%rdx, -1128(%rbp)
	movq	16(%rax), %rdx
	movq	%rdx, -1120(%rbp)
	movq	24(%rax), %rdx
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	movq	%rdx, -1112(%rbp)
	movq	32(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movq	40(%rax), %rdx
	movq	%rdx, -1096(%rbp)
	movq	64(%rax), %rdx
	movq	$0, 40(%rax)
	movq	$0, 32(%rax)
	movq	$0, 24(%rax)
	testq	%rdx, %rdx
	je	.L975
	movl	56(%rax), %ecx
	movq	%rdx, -1072(%rbp)
	movl	%ecx, -1080(%rbp)
	movq	72(%rax), %rcx
	movq	%rcx, -1064(%rbp)
	movq	80(%rax), %rcx
	movq	%rcx, -1056(%rbp)
	leaq	-1080(%rbp), %rcx
	movq	%rcx, 8(%rdx)
	movq	88(%rax), %rdx
	movq	%rdx, -1048(%rbp)
	leaq	56(%rax), %rdx
	movq	$0, 64(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdx, 80(%rax)
	movq	$0, 88(%rax)
.L976:
	leaq	-896(%rbp), %rax
	leaq	-1136(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rsi, -1456(%rbp)
	movq	%rax, -1480(%rbp)
.LEHB29:
	call	_ZN2v88internal6torque13GlobalContextC1ENS1_3AstE@PLT
.LEHE29:
	movq	-1072(%rbp), %r12
	leaq	-1088(%rbp), %rax
	movq	%rax, -1416(%rbp)
	testq	%r12, %r12
	je	.L981
.L977:
	movq	24(%r12), %rsi
	movq	-1416(%rbp), %rdi
	movq	%r12, %r14
	leaq	40(%r14), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r14), %r13
	movq	16(%r12), %r12
	testq	%r13, %r13
	je	.L982
.L980:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L980
.L982:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L977
.L981:
	movq	-1104(%rbp), %r13
	movq	-1112(%rbp), %r12
	cmpq	%r12, %r13
	je	.L978
	.p2align 4,,10
	.p2align 3
.L979:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L983
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L979
.L984:
	movq	-1112(%rbp), %r12
.L978:
	testq	%r12, %r12
	je	.L986
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L986:
	movq	-1136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L987
	call	_ZdlPv@PLT
.L987:
.LEHB30:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE30:
	movq	-1480(%rbp), %rcx
	cmpb	$0, 64(%rbx)
	movq	%rcx, (%rax)
	jne	.L1651
.L989:
	cmpb	$0, 65(%rbx)
	je	.L992
.LEHB31:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movb	$1, 1(%rax)
.L992:
	leaq	-1392(%rbp), %r12
	movzbl	66(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6torque18TargetArchitectureC1Eb@PLT
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -1384(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv@PLT
.LEHE31:
	movq	%r12, (%rax)
	movss	.LC2(%rip), %xmm1
	leaq	-1328(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -1496(%rbp)
	movq	%rax, -1376(%rbp)
	leaq	-1248(%rbp), %rax
	movq	$1, -1368(%rbp)
	movq	$0, -1360(%rbp)
	movq	$0, -1352(%rbp)
	movq	$0, -1336(%rbp)
	movq	%rax, -1504(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$1, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	$0, -1272(%rbp)
	movq	$0, -1256(%rbp)
	movq	$0, -1152(%rbp)
	movss	%xmm1, -1344(%rbp)
	movaps	%xmm0, -1328(%rbp)
	movaps	%xmm0, -1312(%rbp)
	movss	%xmm1, -1264(%rbp)
	movaps	%xmm0, -1248(%rbp)
	movaps	%xmm0, -1232(%rbp)
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm0, -1200(%rbp)
	movaps	%xmm0, -1184(%rbp)
	movaps	%xmm0, -1168(%rbp)
.LEHB32:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -1144(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE32:
	leaq	-1376(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	%rcx, -1544(%rbp)
.LEHB33:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %r12
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -1136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE33:
	movq	-1456(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	24(%r12), %r13
	movq	16(%r12), %r12
	cmpq	%r13, %r12
	je	.L999
	.p2align 4,,10
	.p2align 3
.L998:
	movq	(%r12), %rdi
.LEHB34:
	call	_ZN2v88internal6torque21PredeclarationVisitor10PredeclareEPNS1_11DeclarationE@PLT
.LEHE34:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L998
.L999:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-1128(%rbp), %rdx
	movq	%rdx, (%rax)
.LEHB35:
	call	_ZN2v88internal6torque21PredeclarationVisitor22ResolvePredeclarationsEv@PLT
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %r12
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -1136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
.LEHE35:
	movq	-1456(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	24(%r12), %r13
	movq	16(%r12), %r12
	cmpq	%r13, %r12
	je	.L1005
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	(%r12), %rdi
.LEHB36:
	call	_ZN2v88internal6torque18DeclarationVisitor5VisitEPNS1_11DeclarationE@PLT
.LEHE36:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L1004
.L1005:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-1128(%rbp), %rdx
	movq	%rdx, (%rax)
.LEHB37:
	call	_ZN2v88internal6torque10TypeOracle22FinalizeAggregateTypesEv@PLT
.LEHE37:
	movq	(%rbx), %r12
	leaq	-624(%rbp), %rax
	movq	8(%rbx), %r13
	movq	%rax, -1424(%rbp)
	movq	%rax, -640(%rbp)
	movq	%r12, %rax
	addq	%r13, %rax
	je	.L1007
	testq	%r12, %r12
	je	.L1652
.L1007:
	movq	%r13, -1136(%rbp)
	cmpq	$15, %r13
	ja	.L1653
	cmpq	$1, %r13
	jne	.L1010
	movzbl	(%r12), %eax
	movb	%al, -624(%rbp)
	movq	-1424(%rbp), %rax
.L1011:
	movq	%r13, -632(%rbp)
	leaq	-336(%rbp), %rbx
	movb	$0, (%rax,%r13)
	movq	%rbx, %rdi
	movb	$0, -608(%rbp)
	movb	$0, -600(%rbp)
	movq	%rbx, -1488(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movw	%ax, -112(%rbp)
	leaq	24+_ZTCN2v88internal6torque11NullOStreamE0_So(%rip), %rax
	leaq	-464(%rbp), %rsi
	movq	%rax, -472(%rbp)
	addq	$40, %rax
	movq	$0, -120(%rbp)
	movq	%rax, -336(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
.LEHB38:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE38:
	leaq	24+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	-608(%rbp), %r15
	movq	%rax, %xmm2
	addq	$40, %rax
	movups	%xmm0, -456(%rbp)
	movq	%rax, -336(%rbp)
	leaq	-408(%rbp), %rax
	movhps	.LC3(%rip), %xmm2
	movq	%rax, %rdi
	movq	%rax, -1528(%rbp)
	movaps	%xmm2, -1520(%rbp)
	movups	%xmm2, -472(%rbp)
	movups	%xmm0, -440(%rbp)
	movups	%xmm0, -424(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVN2v88internal6torque13NullStreambufE(%rip), %rax
	cmpq	$0, -632(%rbp)
	movq	%r15, %rdi
	movq	%rax, -464(%rbp)
	sete	-72(%rbp)
.LEHB39:
	call	_ZN2v88internal6torque21ImplementationVisitor13BeginCSAFilesEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor19VisitAllDeclarablesEv@PLT
	call	_ZN2v88internal6torque21ReportAllUnusedMacrosEv@PLT
	leaq	-640(%rbp), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal6torque21ImplementationVisitor26GenerateBuiltinDefinitionsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor25GenerateClassFieldOffsetsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor24GeneratePrintDefinitionsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor24GenerateClassDefinitionsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor22GenerateClassVerifiersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor25GenerateClassDebugReadersERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor31GenerateExportedMacrosAssemblerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor16GenerateCSATypesERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor21GenerateInstanceTypesERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor29GenerateCppForInternalClassesERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor11EndCSAFilesEv@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitor22GenerateImplementationERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	cmpb	$0, (%rax)
	je	.L1016
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE39:
	movq	(%rax), %rax
	movzbl	(%rax), %edx
	movb	%dl, -1136(%rbp)
	movzbl	1(%rax), %edx
	movb	%dl, -1135(%rbp)
	movq	8(%rax), %rdx
	movq	%rdx, -1128(%rbp)
	movq	16(%rax), %rdx
	movq	%rdx, -1120(%rbp)
	movq	24(%rax), %rdx
	movq	%rdx, -1112(%rbp)
	movq	32(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movq	40(%rax), %rdx
	movq	$0, 32(%rax)
	movq	$0, 24(%rax)
	movq	$0, 16(%rax)
	movq	%rdx, -1096(%rbp)
	movq	48(%rax), %rdx
	movq	%rdx, -1088(%rbp)
	movq	56(%rax), %rdx
	movq	%rdx, -1080(%rbp)
	movq	80(%rax), %rdx
	movq	$0, 56(%rax)
	movq	$0, 48(%rax)
	movq	$0, 40(%rax)
	testq	%rdx, %rdx
	je	.L1017
	movl	72(%rax), %ecx
	movq	%rdx, -1056(%rbp)
	leaq	-1064(%rbp), %r13
	movl	%ecx, -1064(%rbp)
	movq	88(%rax), %rcx
	movq	%rcx, -1048(%rbp)
	movq	96(%rax), %rcx
	movq	%rcx, -1040(%rbp)
	movq	%r13, 8(%rdx)
	movq	104(%rax), %rdx
	movq	%rdx, -1032(%rbp)
	leaq	72(%rax), %rdx
	movq	$0, 80(%rax)
	movq	%rdx, 88(%rax)
	movq	%rdx, 96(%rax)
	movq	$0, 104(%rax)
.L1018:
	movq	112(%rax), %rdx
	movq	%rdx, -1024(%rbp)
	movq	120(%rax), %rdx
	movq	%rdx, -1016(%rbp)
	movq	128(%rax), %rdx
	movq	%rdx, -1008(%rbp)
	movq	136(%rax), %rdx
	movq	$0, 128(%rax)
	movq	$0, 120(%rax)
	movq	$0, 112(%rax)
	movq	%rdx, -1000(%rbp)
	movq	144(%rax), %rdx
	movq	%rdx, -992(%rbp)
	movq	152(%rax), %rdx
	movq	%rdx, -984(%rbp)
	movq	176(%rax), %rdx
	movq	$0, 152(%rax)
	movq	$0, 144(%rax)
	movq	$0, 136(%rax)
	testq	%rdx, %rdx
	je	.L1019
	movl	168(%rax), %ecx
	movq	%rdx, -960(%rbp)
	leaq	-968(%rbp), %rbx
	movl	%ecx, -968(%rbp)
	movq	184(%rax), %rcx
	movq	%rcx, -952(%rbp)
	movq	192(%rax), %rcx
	movq	%rcx, -944(%rbp)
	movq	%rbx, 8(%rdx)
	movq	200(%rax), %rdx
	movq	%rdx, -936(%rbp)
	leaq	168(%rax), %rdx
	movq	$0, 176(%rax)
	movq	%rdx, 184(%rax)
	movq	%rdx, 192(%rax)
	movq	$0, 200(%rax)
.L1020:
	movdqu	208(%rax), %xmm7
	pxor	%xmm0, %xmm0
	movl	$240, %edi
	movaps	%xmm7, -928(%rbp)
	movq	224(%rax), %rdx
	movq	%rdx, -912(%rbp)
	movq	$0, 224(%rax)
	movups	%xmm0, 208(%rax)
	movq	232(%rax), %rax
	movq	%rax, -904(%rbp)
.LEHB40:
	call	_Znwm@PLT
.LEHE40:
	movq	%rax, %r12
	movzwl	-1136(%rbp), %eax
	pxor	%xmm0, %xmm0
	leaq	72(%r12), %rdx
	movw	%ax, (%r12)
	movq	-1128(%rbp), %rax
	movq	%rax, 8(%r12)
	movq	-1120(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-1112(%rbp), %rax
	movaps	%xmm0, -1120(%rbp)
	movq	%rax, 24(%r12)
	movq	-1104(%rbp), %rax
	movq	%rax, 32(%r12)
	movq	-1096(%rbp), %rax
	movaps	%xmm0, -1104(%rbp)
	movq	%rax, 40(%r12)
	movq	-1088(%rbp), %rax
	movq	%rax, 48(%r12)
	movq	-1080(%rbp), %rax
	movaps	%xmm0, -1088(%rbp)
	movq	%rax, 56(%r12)
	movq	-1056(%rbp), %rax
	testq	%rax, %rax
	je	.L1021
	movl	-1064(%rbp), %ecx
	movq	%rax, 80(%r12)
	movl	%ecx, 72(%r12)
	movq	-1048(%rbp), %rcx
	movq	%rcx, 88(%r12)
	movq	-1040(%rbp), %rcx
	movq	%rcx, 96(%r12)
	movq	%rdx, 8(%rax)
	movq	-1032(%rbp), %rax
	movq	$0, -1056(%rbp)
	movq	%rax, 104(%r12)
	movq	%r13, -1048(%rbp)
	movq	%r13, -1040(%rbp)
	movq	$0, -1032(%rbp)
.L1022:
	movq	-1024(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	168(%r12), %rdx
	movq	%rax, 112(%r12)
	movq	-1016(%rbp), %rax
	movaps	%xmm0, -1024(%rbp)
	movq	%rax, 120(%r12)
	movq	-1008(%rbp), %rax
	movq	%rax, 128(%r12)
	movq	-1000(%rbp), %rax
	movaps	%xmm0, -1008(%rbp)
	movq	%rax, 136(%r12)
	movq	-992(%rbp), %rax
	movq	%rax, 144(%r12)
	movq	-984(%rbp), %rax
	movaps	%xmm0, -992(%rbp)
	movq	%rax, 152(%r12)
	movq	-960(%rbp), %rax
	testq	%rax, %rax
	je	.L1023
	movl	-968(%rbp), %ecx
	movq	%rax, 176(%r12)
	movl	%ecx, 168(%r12)
	movq	-952(%rbp), %rcx
	movq	%rcx, 184(%r12)
	movq	-944(%rbp), %rcx
	movq	%rcx, 192(%r12)
	movq	%rdx, 8(%rax)
	movq	-936(%rbp), %rax
	movq	$0, -960(%rbp)
	movq	%rax, 200(%r12)
	movq	%rbx, -952(%rbp)
	movq	%rbx, -944(%rbp)
	movq	$0, -936(%rbp)
.L1024:
	movq	-912(%rbp), %rax
	movdqa	-928(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	$0, -912(%rbp)
	movaps	%xmm0, -928(%rbp)
	movq	%rax, 224(%r12)
	movq	-904(%rbp), %rax
	movups	%xmm3, 208(%r12)
	movq	%rax, 232(%r12)
.LEHB41:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
.LEHE41:
	movq	(%rax), %rax
	movq	96(%rax), %rdx
	movq	%r12, 96(%rax)
	movq	%rdx, -1416(%rbp)
	testq	%rdx, %rdx
	je	.L1025
	movq	208(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1026
	call	_ZdlPv@PLT
.L1026:
	movq	-1416(%rbp), %rax
	movq	176(%rax), %rbx
	leaq	160(%rax), %rcx
	movq	%rcx, -1536(%rbp)
	testq	%rbx, %rbx
	je	.L1032
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	leaq	-40(%rax), %rsi
	movq	%rax, %xmm6
	movq	%rdx, %xmm5
	movq	%r15, -1552(%rbp)
	movq	%rsi, -1440(%rbp)
	leaq	40(%rax), %rsi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	punpcklqdq	%xmm5, %xmm6
	movq	%rsi, -1408(%rbp)
	movq	%rcx, %r15
	movaps	%xmm6, -1472(%rbp)
.L1033:
	movq	24(%rbx), %rsi
	movq	-1536(%rbp), %rdi
	movq	%rbx, %r12
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	-1440(%rbp), %rax
	movq	16(%rbx), %rbx
	movdqa	-1472(%rbp), %xmm6
	movq	528(%r12), %rdi
	movq	%rax, 432(%r12)
	movq	-1408(%rbp), %rax
	movups	%xmm6, 448(%r12)
	movq	%rax, 560(%r12)
	leaq	544(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1030
	call	_ZdlPv@PLT
.L1030:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	512(%r12), %rdi
	movq	%rax, 456(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 432(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	560(%r12), %rdi
	movq	-24(%r14), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rdx, 432(%r12,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, 448(%r12)
	movq	-24(%r13), %rax
	movq	%rcx, 448(%r12,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 432(%r12)
	movq	-24(%rax), %rax
	movq	%rdx, 432(%r12,%rax)
	movq	$0, 440(%r12)
	movq	%r15, 560(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1440(%rbp), %rax
	movdqa	-1472(%rbp), %xmm7
	movq	136(%r12), %rdi
	movq	%rax, 40(%r12)
	movq	-1408(%rbp), %rax
	movups	%xmm7, 56(%r12)
	movq	%rax, 168(%r12)
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1031
	call	_ZdlPv@PLT
.L1031:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	120(%r12), %rdi
	movq	%rax, 64(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 40(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	168(%r12), %rdi
	movq	-24(%r14), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, 40(%r12,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, 56(%r12)
	movq	-24(%r13), %rax
	movq	%rdx, 56(%r12,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 40(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 40(%r12,%rax)
	movq	$0, 48(%r12)
	movq	%r15, 168(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1033
	movq	-1552(%rbp), %r15
.L1032:
	movq	-1416(%rbp), %rax
	movq	144(%rax), %rbx
	movq	136(%rax), %r12
	cmpq	%r12, %rbx
	je	.L1028
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1034
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1029
.L1035:
	movq	-1416(%rbp), %rax
	movq	136(%rax), %r12
.L1028:
	testq	%r12, %r12
	je	.L1037
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1037:
	movq	-1416(%rbp), %rax
	movq	120(%rax), %rbx
	movq	112(%rax), %r12
	cmpq	%r12, %rbx
	je	.L1038
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1039
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1042
.L1040:
	movq	-1416(%rbp), %rax
	movq	112(%rax), %r12
.L1038:
	testq	%r12, %r12
	je	.L1043
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1043:
	movq	-1416(%rbp), %rax
	movq	80(%rax), %r13
	leaq	64(%rax), %rcx
	testq	%r13, %r13
	je	.L1048
	movq	%r15, -1408(%rbp)
	movq	%rcx, %r14
.L1044:
	movq	24(%r13), %rsi
	movq	%r13, %r12
	movq	%r14, %rdi
	leaq	40(%r12), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r12), %rbx
	movq	16(%r13), %r13
	testq	%rbx, %rbx
	je	.L1049
.L1047:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1047
.L1049:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1044
	movq	-1408(%rbp), %r15
.L1048:
	movq	-1416(%rbp), %rax
	movq	48(%rax), %rbx
	movq	40(%rax), %r12
	cmpq	%r12, %rbx
	je	.L1045
	.p2align 4,,10
	.p2align 3
.L1046:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1050
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1046
.L1051:
	movq	-1416(%rbp), %rax
	movq	40(%rax), %r12
.L1045:
	testq	%r12, %r12
	je	.L1053
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1053:
	movq	-1416(%rbp), %rax
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1054
	call	_ZdlPv@PLT
.L1054:
	movq	-1416(%rbp), %rdi
	movl	$240, %esi
	call	_ZdlPvm@PLT
.L1025:
.LEHB42:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rdi
	call	_ZN2v88internal6torque18LanguageServerData27PrepareAllDeclarableSymbolsEv@PLT
.LEHE42:
	movq	-928(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1080
	call	_ZdlPv@PLT
.L1080:
	movq	-960(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1086
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	%rax, -1416(%rbp)
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rdx, %xmm3
	leaq	40(%rax), %rsi
	movq	%rax, %xmm5
	leaq	-40(%rax), %rcx
	movq	%r15, -1536(%rbp)
	movq	%rsi, -1408(%rbp)
	punpcklqdq	%xmm3, %xmm5
	movq	%rcx, %r15
	leaq	-976(%rbp), %rsi
	movq	%rsi, -1472(%rbp)
	movaps	%xmm5, -1440(%rbp)
.L1087:
	movq	24(%rbx), %rsi
	movq	-1472(%rbp), %rdi
	movq	%rbx, %r12
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	-1408(%rbp), %rax
	movq	16(%rbx), %rbx
	movq	%r15, 432(%r12)
	movdqa	-1440(%rbp), %xmm4
	movq	528(%r12), %rdi
	movq	%rax, 560(%r12)
	leaq	544(%r12), %rax
	movups	%xmm4, 448(%r12)
	cmpq	%rax, %rdi
	je	.L1084
	call	_ZdlPv@PLT
.L1084:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	512(%r12), %rdi
	movq	%rax, 456(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 432(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	560(%r12), %rdi
	movq	-24(%r14), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, 448(%r12)
	movq	-24(%r13), %rax
	movq	%rcx, 448(%r12,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 432(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	-1416(%rbp), %rax
	movq	$0, 440(%r12)
	movq	%rax, 560(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1408(%rbp), %rax
	movq	%r15, 40(%r12)
	movdqa	-1440(%rbp), %xmm5
	movq	136(%r12), %rdi
	movq	%rax, 168(%r12)
	leaq	152(%r12), %rax
	movups	%xmm5, 56(%r12)
	cmpq	%rax, %rdi
	je	.L1085
	call	_ZdlPv@PLT
.L1085:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	120(%r12), %rdi
	movq	%rax, 64(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 40(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	168(%r12), %rdi
	movq	-24(%r14), %rax
	movq	%rcx, 40(%r12,%rax)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, 56(%r12)
	movq	-24(%r13), %rax
	movq	%rcx, 56(%r12,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 40(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 40(%r12,%rax)
	movq	-1416(%rbp), %rax
	movq	$0, 48(%r12)
	movq	%rax, 168(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1087
	movq	-1536(%rbp), %r15
.L1086:
	movq	-992(%rbp), %rbx
	movq	-1000(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1082
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1088
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1083
.L1089:
	movq	-1000(%rbp), %r12
.L1082:
	testq	%r12, %r12
	je	.L1091
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1091:
	movq	-1016(%rbp), %rbx
	movq	-1024(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1092
	.p2align 4,,10
	.p2align 3
.L1096:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1093
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1096
.L1094:
	movq	-1024(%rbp), %r12
.L1092:
	testq	%r12, %r12
	je	.L1097
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1097:
	movq	-1056(%rbp), %r13
	leaq	-1072(%rbp), %rbx
	testq	%r13, %r13
	je	.L1102
	movq	%r15, -1408(%rbp)
.L1098:
	movq	24(%r13), %rsi
	movq	%r13, %r14
	movq	%rbx, %rdi
	leaq	40(%r14), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r14), %r12
	movq	16(%r13), %r13
	testq	%r12, %r12
	je	.L1103
.L1101:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1101
.L1103:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1098
	movq	-1408(%rbp), %r15
.L1102:
	movq	-1088(%rbp), %rbx
	movq	-1096(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1099
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1104
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1100
.L1105:
	movq	-1096(%rbp), %r12
.L1099:
	testq	%r12, %r12
	je	.L1107
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1107:
	movq	-1120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1108
	call	_ZdlPv@PLT
.L1108:
.LEHB43:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
.LEHE43:
	movq	(%rax), %rcx
	movq	(%rcx), %rdi
	leaq	48(%rcx), %r8
	movq	%rdi, -1136(%rbp)
	movq	8(%rcx), %rsi
	movq	%rsi, -1128(%rbp)
	movq	16(%rcx), %rax
	movq	%rax, -1120(%rbp)
	movq	24(%rcx), %rdx
	movq	%rdx, -1112(%rbp)
	movdqu	32(%rcx), %xmm5
	movq	$0, -1088(%rbp)
	movaps	%xmm5, -1104(%rbp)
	cmpq	%r8, (%rcx)
	je	.L1654
.L1109:
	testq	%rax, %rax
	je	.L1110
	movq	120(%rax), %rax
	xorl	%edx, %edx
	divq	%rsi
	leaq	-1120(%rbp), %rax
	movq	%rax, (%rdi,%rdx,8)
.L1110:
	movq	56(%rcx), %rax
	movq	%r8, (%rcx)
	leaq	128(%rcx), %r8
	movq	$0, 40(%rcx)
	movq	$1, 8(%rcx)
	movq	$0, 48(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 24(%rcx)
	movq	%rax, -1080(%rbp)
	movq	64(%rcx), %rax
	movq	%rax, -1072(%rbp)
	movq	72(%rcx), %rax
	movq	%rax, -1064(%rbp)
	movq	80(%rcx), %rdi
	movq	$0, 72(%rcx)
	movq	$0, 64(%rcx)
	movq	$0, 56(%rcx)
	movq	%rdi, -1056(%rbp)
	movq	88(%rcx), %rsi
	movq	%rsi, -1048(%rbp)
	movq	96(%rcx), %rax
	movq	%rax, -1040(%rbp)
	movq	104(%rcx), %rdx
	movq	%rdx, -1032(%rbp)
	movdqu	112(%rcx), %xmm4
	movq	$0, -1008(%rbp)
	movaps	%xmm4, -1024(%rbp)
	cmpq	%r8, 80(%rcx)
	je	.L1655
.L1111:
	testq	%rax, %rax
	je	.L1112
	movq	128(%rax), %rax
	xorl	%edx, %edx
	divq	%rsi
	leaq	-1040(%rbp), %rax
	movq	%rax, (%rdi,%rdx,8)
.L1112:
	movq	136(%rcx), %rax
	movq	%r8, 80(%rcx)
	movl	$232, %edi
	movq	$0, 120(%rcx)
	movq	$1, 88(%rcx)
	movq	$0, 128(%rcx)
	movq	$0, 96(%rcx)
	movq	$0, 104(%rcx)
	movq	%rax, -1000(%rbp)
	movq	144(%rcx), %rax
	movq	%rax, -992(%rbp)
	movq	152(%rcx), %rax
	movq	%rax, -984(%rbp)
	movq	160(%rcx), %rax
	movq	$0, 152(%rcx)
	movq	$0, 144(%rcx)
	movq	$0, 136(%rcx)
	movq	%rax, -976(%rbp)
	movq	168(%rcx), %rax
	movq	%rax, -968(%rbp)
	movq	176(%rcx), %rax
	movq	%rax, -960(%rbp)
	movq	184(%rcx), %rax
	movq	$0, 176(%rcx)
	movq	$0, 168(%rcx)
	movq	$0, 160(%rcx)
	movq	%rax, -952(%rbp)
	movq	192(%rcx), %rax
	movq	%rax, -944(%rbp)
	movq	200(%rcx), %rax
	movq	%rax, -936(%rbp)
	movq	$0, 200(%rcx)
	movq	$0, 192(%rcx)
	movq	$0, 184(%rcx)
	movq	208(%rcx), %rax
	movq	%rax, -928(%rbp)
	movq	216(%rcx), %rax
	movq	%rax, -920(%rbp)
	movq	224(%rcx), %rax
	movq	%rax, -912(%rbp)
	movq	$0, 224(%rcx)
	movq	$0, 216(%rcx)
	movq	$0, 208(%rcx)
.LEHB44:
	call	_Znwm@PLT
.LEHE44:
	movq	-1136(%rbp), %rcx
	movq	-1128(%rbp), %rsi
	movq	%rax, %r13
	movq	-1112(%rbp), %rdx
	movdqa	-1104(%rbp), %xmm1
	movq	%rcx, (%rax)
	movq	%rsi, 8(%rax)
	movq	-1120(%rbp), %rax
	movq	%rdx, 24(%r13)
	leaq	-1088(%rbp), %rdx
	movq	%rax, 16(%r13)
	movq	$0, 48(%r13)
	movq	%rdx, -1416(%rbp)
	movups	%xmm1, 32(%r13)
	cmpq	%rdx, %rcx
	je	.L1656
.L1113:
	testq	%rax, %rax
	je	.L1114
	movq	120(%rax), %rax
	xorl	%edx, %edx
	divq	%rsi
	leaq	16(%r13), %rax
	movq	%rax, (%rcx,%rdx,8)
.L1114:
	movq	-1416(%rbp), %rax
	movq	-1032(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -1096(%rbp)
	movq	-1056(%rbp), %rcx
	movq	-1048(%rbp), %rsi
	movq	$1, -1128(%rbp)
	movq	%rax, -1136(%rbp)
	movq	-1080(%rbp), %rax
	movdqa	-1024(%rbp), %xmm7
	movq	%rdx, 104(%r13)
	leaq	-1008(%rbp), %rdx
	movq	%rax, 56(%r13)
	movq	-1072(%rbp), %rax
	movq	$0, -1120(%rbp)
	movq	%rax, 64(%r13)
	movq	-1064(%rbp), %rax
	movq	$0, -1112(%rbp)
	movq	%rax, 72(%r13)
	movq	-1040(%rbp), %rax
	movq	%rcx, 80(%r13)
	movq	%rsi, 88(%r13)
	movq	%rax, 96(%r13)
	movq	$0, 128(%r13)
	movq	%rdx, -1472(%rbp)
	movaps	%xmm0, -1088(%rbp)
	movaps	%xmm0, -1072(%rbp)
	movups	%xmm7, 112(%r13)
	cmpq	%rdx, %rcx
	je	.L1657
.L1115:
	testq	%rax, %rax
	je	.L1116
	movq	128(%rax), %rax
	xorl	%edx, %edx
	divq	%rsi
	leaq	96(%r13), %rax
	movq	%rax, (%rcx,%rdx,8)
.L1116:
	movq	-1472(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -1016(%rbp)
	movq	$1, -1048(%rbp)
	movq	%rax, -1056(%rbp)
	movq	-1000(%rbp), %rax
	movq	$0, -1040(%rbp)
	movq	%rax, 136(%r13)
	movq	-992(%rbp), %rax
	movq	$0, -1032(%rbp)
	movq	%rax, 144(%r13)
	movq	-984(%rbp), %rax
	movaps	%xmm0, -1008(%rbp)
	movq	%rax, 152(%r13)
	movq	-976(%rbp), %rax
	movq	%rax, 160(%r13)
	movq	-968(%rbp), %rax
	movq	%rax, 168(%r13)
	movq	-960(%rbp), %rax
	movq	%rax, 176(%r13)
	movq	-952(%rbp), %rax
	movq	%rax, 184(%r13)
	movq	-944(%rbp), %rax
	movq	%rax, 192(%r13)
	movq	-936(%rbp), %rax
	movq	%rax, 200(%r13)
	movq	-928(%rbp), %rax
	movq	%rax, 208(%r13)
	movq	-920(%rbp), %rax
	movq	%rax, 216(%r13)
	movq	-912(%rbp), %rax
	movq	$0, -912(%rbp)
	movq	%rax, 224(%r13)
	movaps	%xmm0, -992(%rbp)
	movaps	%xmm0, -976(%rbp)
	movaps	%xmm0, -960(%rbp)
	movaps	%xmm0, -944(%rbp)
	movaps	%xmm0, -928(%rbp)
.LEHB45:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
.LEHE45:
	movq	(%rax), %rax
	movq	104(%rax), %r12
	movq	%r13, 104(%rax)
	testq	%r12, %r12
	je	.L1117
	movq	216(%r12), %rbx
	movq	208(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1118
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1119
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L1122
.L1120:
	movq	208(%r12), %r13
.L1118:
	testq	%r13, %r13
	je	.L1123
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1123:
	movq	192(%r12), %rbx
	movq	184(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1124
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1125
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L1128
.L1126:
	movq	184(%r12), %r13
.L1124:
	testq	%r13, %r13
	je	.L1129
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1129:
	movq	168(%r12), %rbx
	movq	160(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1130
	.p2align 4,,10
	.p2align 3
.L1134:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1131
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L1134
.L1132:
	movq	160(%r12), %r13
.L1130:
	testq	%r13, %r13
	je	.L1135
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1135:
	movq	144(%r12), %rbx
	movq	136(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1136
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1137
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L1140
.L1138:
	movq	136(%r12), %r13
.L1136:
	testq	%r13, %r13
	je	.L1141
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1141:
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	96(%r12), %rbx
	movq	%rax, -1440(%rbp)
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -1408(%rbp)
	testq	%rbx, %rbx
	je	.L1149
	movq	%r12, -1456(%rbp)
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	%rbx, %r12
	movq	-1440(%rbp), %rax
	movq	(%rbx), %rbx
	movq	96(%r12), %r13
	leaq	80(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r13, %r13
	je	.L1148
.L1145:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1145
.L1148:
	movq	-1408(%rbp), %rax
	movq	48(%r12), %r15
	leaq	32(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r15, %r15
	je	.L1146
.L1147:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L1150
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L1146
.L1151:
	movq	%r13, %r15
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L983:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L979
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1010:
	testq	%r13, %r13
	jne	.L1658
	movq	-1424(%rbp), %rax
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1104:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1100
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1093:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1096
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1034:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1029
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1150:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1151
.L1146:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1142
	movq	-1456(%rbp), %r12
.L1149:
	movq	88(%r12), %rax
	movq	80(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%r12), %rdi
	leaq	128(%r12), %rax
	movq	$0, 104(%r12)
	movq	$0, 96(%r12)
	cmpq	%rax, %rdi
	je	.L1143
	call	_ZdlPv@PLT
.L1143:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1152
	call	_ZdlPv@PLT
.L1152:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	16(%r12), %rbx
	movq	%rax, -1408(%rbp)
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	%rax, -1440(%rbp)
	testq	%rbx, %rbx
	je	.L1158
	movq	%r12, -1456(%rbp)
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	%rbx, %r13
	movq	-1440(%rbp), %rax
	movq	(%rbx), %rbx
	movq	80(%r13), %rdi
	movq	%rax, 8(%r13)
	testq	%rdi, %rdi
	je	.L1156
	call	_ZdlPv@PLT
.L1156:
	movq	-1408(%rbp), %rax
	movq	48(%r13), %r12
	leaq	32(%r13), %r15
	movq	%rax, 8(%r13)
	testq	%r12, %r12
	je	.L1160
.L1157:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r14
	cmpq	%rax, %rdi
	je	.L1159
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L1160
.L1161:
	movq	%r14, %r12
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1088:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1083
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L1161
.L1160:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1153
	movq	-1456(%rbp), %r12
.L1158:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L1154
	call	_ZdlPv@PLT
.L1154:
	movl	$232, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1117:
	movq	-920(%rbp), %rbx
	movq	-928(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1162
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1166
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1163
.L1167:
	movq	-928(%rbp), %r12
.L1162:
	testq	%r12, %r12
	je	.L1169
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1169:
	movq	-944(%rbp), %rbx
	movq	-952(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1170
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1171
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1174
.L1172:
	movq	-952(%rbp), %r12
.L1170:
	testq	%r12, %r12
	je	.L1175
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1175:
	movq	-968(%rbp), %rbx
	movq	-976(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1176
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1177
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1180
.L1178:
	movq	-976(%rbp), %r12
.L1176:
	testq	%r12, %r12
	je	.L1181
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1181:
	movq	-992(%rbp), %rbx
	movq	-1000(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1182
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1183
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1186
.L1184:
	movq	-1000(%rbp), %r12
.L1182:
	testq	%r12, %r12
	je	.L1187
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1187:
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	-1040(%rbp), %rbx
	movq	%rax, -1440(%rbp)
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -1408(%rbp)
	testq	%rbx, %rbx
	je	.L1195
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	%rbx, %r14
	movq	-1440(%rbp), %rax
	movq	(%rbx), %rbx
	movq	96(%r14), %r12
	leaq	80(%r14), %r13
	movq	%rax, 8(%r14)
	testq	%r12, %r12
	je	.L1194
.L1191:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1191
.L1194:
	movq	-1408(%rbp), %rax
	movq	48(%r14), %r15
	leaq	32(%r14), %r13
	movq	%rax, 8(%r14)
	testq	%r15, %r15
	je	.L1192
.L1193:
	movq	24(%r15), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r12
	cmpq	%rax, %rdi
	je	.L1196
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1192
.L1197:
	movq	%r12, %r15
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1177:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1180
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1183:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1186
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1197
.L1192:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1188
.L1195:
	movq	-1048(%rbp), %rax
	movq	-1056(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-1056(%rbp), %rdi
	movq	$0, -1032(%rbp)
	movq	$0, -1040(%rbp)
	cmpq	-1472(%rbp), %rdi
	je	.L1189
	call	_ZdlPv@PLT
.L1189:
	movq	-1080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1198
	call	_ZdlPv@PLT
.L1198:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	-1120(%rbp), %rbx
	movq	%rax, -1408(%rbp)
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	%rax, -1440(%rbp)
	testq	%rbx, %rbx
	je	.L1204
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	%rbx, %r12
	movq	-1440(%rbp), %rax
	movq	(%rbx), %rbx
	movq	80(%r12), %rdi
	movq	%rax, 8(%r12)
	testq	%rdi, %rdi
	je	.L1202
	call	_ZdlPv@PLT
.L1202:
	movq	-1408(%rbp), %rax
	movq	48(%r12), %r15
	leaq	32(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r15, %r15
	je	.L1206
.L1203:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L1205
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L1206
.L1207:
	movq	%r13, %r15
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1171:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1174
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1166:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1163
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1205:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1207
.L1206:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1199
.L1204:
	movq	-1128(%rbp), %rax
	movq	-1136(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-1136(%rbp), %rdi
	movq	$0, -1112(%rbp)
	movq	$0, -1120(%rbp)
	cmpq	-1416(%rbp), %rdi
	je	.L1016
	call	_ZdlPv@PLT
.L1016:
	movdqa	-1520(%rbp), %xmm4
	movq	-1528(%rbp), %rdi
	leaq	64+_ZTVN2v88internal6torque11NullOStreamE(%rip), %rax
	movq	%rax, -336(%rbp)
	movups	%xmm4, -472(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal6torque11NullOStreamE0_So(%rip), %rax
	movq	-1488(%rbp), %rdi
	movq	%rax, -472(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1416(%rbp)
	movq	%rax, -336(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, -608(%rbp)
	jne	.L1659
.L1208:
	movq	-640(%rbp), %rdi
	cmpq	-1424(%rbp), %rdi
	je	.L1221
	call	_ZdlPv@PLT
.L1221:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	movq	-1144(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-1160(%rbp), %rbx
	movq	-1168(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1222
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1223
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1226
.L1224:
	movq	-1168(%rbp), %r12
.L1222:
	testq	%r12, %r12
	je	.L1227
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1227:
	movq	-1184(%rbp), %rbx
	movq	-1192(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1228
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1229
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1232
.L1230:
	movq	-1192(%rbp), %r12
.L1228:
	testq	%r12, %r12
	je	.L1233
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1233:
	movq	-1208(%rbp), %rbx
	movq	-1216(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1234
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1235
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1238
.L1236:
	movq	-1216(%rbp), %r12
.L1234:
	testq	%r12, %r12
	je	.L1239
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1239:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1240
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1241
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L1244
.L1242:
	movq	-1240(%rbp), %r12
.L1240:
	testq	%r12, %r12
	je	.L1245
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1245:
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rax
	movq	-1280(%rbp), %rbx
	movq	%rax, -1440(%rbp)
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	%rax, -1408(%rbp)
	testq	%rbx, %rbx
	je	.L1253
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	%rbx, %r14
	movq	-1440(%rbp), %rax
	movq	(%rbx), %rbx
	movq	96(%r14), %r12
	leaq	80(%r14), %r13
	movq	%rax, 8(%r14)
	testq	%r12, %r12
	je	.L1252
.L1249:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1249
.L1252:
	movq	-1408(%rbp), %rax
	movq	48(%r14), %r15
	leaq	32(%r14), %r13
	movq	%rax, 8(%r14)
	testq	%r15, %r15
	je	.L1250
.L1251:
	movq	24(%r15), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r12
	cmpq	%rax, %rdi
	je	.L1254
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1250
.L1255:
	movq	%r12, %r15
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1241:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1244
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1235:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1238
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1254:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1255
.L1250:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1246
.L1253:
	movq	-1288(%rbp), %rax
	movq	-1296(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-1296(%rbp), %rdi
	movq	$0, -1272(%rbp)
	movq	$0, -1280(%rbp)
	cmpq	-1504(%rbp), %rdi
	je	.L1247
	call	_ZdlPv@PLT
.L1247:
	movq	-1320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1256
	call	_ZdlPv@PLT
.L1256:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	-1360(%rbp), %rbx
	movq	%rax, -1408(%rbp)
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	%rax, -1440(%rbp)
	testq	%rbx, %rbx
	je	.L1262
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	%rbx, %r12
	movq	-1440(%rbp), %rax
	movq	(%rbx), %rbx
	movq	80(%r12), %rdi
	movq	%rax, 8(%r12)
	testq	%rdi, %rdi
	je	.L1260
	call	_ZdlPv@PLT
.L1260:
	movq	-1408(%rbp), %rax
	movq	48(%r12), %r15
	leaq	32(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r15, %r15
	je	.L1264
.L1261:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L1263
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L1264
.L1265:
	movq	%r13, %r15
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1223:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1226
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1229:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1232
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1265
.L1264:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1257
.L1262:
	movq	-1368(%rbp), %rax
	movq	-1376(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-1376(%rbp), %rdi
	movq	$0, -1352(%rbp)
	movq	$0, -1360(%rbp)
	cmpq	-1496(%rbp), %rdi
	je	.L1258
	call	_ZdlPv@PLT
.L1258:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv@PLT
	movq	-1384(%rbp), %rdx
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	-656(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1266
	call	_ZdlPv@PLT
.L1266:
	movq	-720(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1272
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	leaq	-40(%rax), %rcx
	movq	%rax, %xmm3
	movq	%rdx, %xmm1
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	%rcx, -1440(%rbp)
	leaq	40(%rax), %rcx
	punpcklqdq	%xmm1, %xmm3
	movq	%rcx, -1408(%rbp)
	leaq	-736(%rbp), %rcx
	movq	%rcx, -1424(%rbp)
	movaps	%xmm3, -1456(%rbp)
.L1273:
	movq	24(%rbx), %rsi
	movq	-1424(%rbp), %rdi
	movq	%rbx, %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	-1440(%rbp), %rax
	movq	16(%rbx), %rbx
	movdqa	-1456(%rbp), %xmm6
	movq	528(%r15), %rdi
	movq	%rax, 432(%r15)
	movq	-1408(%rbp), %rax
	movups	%xmm6, 448(%r15)
	movq	%rax, 560(%r15)
	leaq	544(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1270
	call	_ZdlPv@PLT
.L1270:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	512(%r15), %rdi
	movq	%rax, 456(%r15)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 432(%r15)
	movq	-24(%r14), %rax
	leaq	560(%r15), %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, 432(%r15,%rax)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, 448(%r15)
	movq	-24(%r13), %rax
	movq	%rcx, 448(%r15,%rax)
	movq	%r12, 432(%r15)
	movq	-24(%r12), %rax
	movq	%rdx, 432(%r15,%rax)
	movq	-1416(%rbp), %rax
	movq	$0, 440(%r15)
	movq	%rax, 560(%r15)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-1440(%rbp), %rax
	movdqa	-1456(%rbp), %xmm7
	movq	136(%r15), %rdi
	movq	%rax, 40(%r15)
	movq	-1408(%rbp), %rax
	movups	%xmm7, 56(%r15)
	movq	%rax, 168(%r15)
	leaq	152(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1271
	call	_ZdlPv@PLT
.L1271:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	120(%r15), %rdi
	movq	%rax, 64(%r15)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 40(%r15)
	movq	-24(%r14), %rax
	leaq	168(%r15), %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, 40(%r15,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, 56(%r15)
	movq	-24(%r13), %rax
	movq	%rdx, 56(%r15,%rax)
	movq	%r12, 40(%r15)
	movq	-24(%r12), %rax
	movq	%rcx, 40(%r15,%rax)
	movq	-1416(%rbp), %rax
	movq	$0, 48(%r15)
	movq	%rax, 168(%r15)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1273
.L1272:
	movq	-752(%rbp), %rbx
	movq	-760(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1268
	.p2align 4,,10
	.p2align 3
.L1269:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1274
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1269
.L1275:
	movq	-760(%rbp), %r12
.L1268:
	testq	%r12, %r12
	je	.L1277
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1277:
	movq	-776(%rbp), %rbx
	movq	-784(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1278
	.p2align 4,,10
	.p2align 3
.L1282:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1279
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L1282
.L1280:
	movq	-784(%rbp), %r12
.L1278:
	testq	%r12, %r12
	je	.L1283
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1283:
	movq	-816(%rbp), %r12
	leaq	-832(%rbp), %r15
	testq	%r12, %r12
	je	.L1288
.L1284:
	movq	24(%r12), %rsi
	movq	%r12, %r13
	movq	%r15, %rdi
	leaq	40(%r13), %r14
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r13), %rbx
	movq	16(%r12), %r12
	testq	%rbx, %rbx
	je	.L1289
.L1287:
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1287
.L1289:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1284
.L1288:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1285
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1290
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L1286
.L1291:
	movq	-856(%rbp), %r12
.L1285:
	testq	%r12, %r12
	je	.L1293
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1293:
	movq	-880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L974
	call	_ZdlPv@PLT
.L974:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1660
	addq	$1512, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1290:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1286
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1274:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1269
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1279:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1282
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1050:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1046
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1137:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1140
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1131:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1134
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1119:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1122
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1125:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1128
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1039:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1042
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1659:
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1209
	call	_ZdlPv@PLT
.L1209:
	movq	-576(%rbp), %r14
	leaq	-576(%rbp), %r12
	cmpq	%r12, %r14
	je	.L1219
	.p2align 4,,10
	.p2align 3
.L1210:
	cmpb	$0, 48(%r14)
	movq	(%r14), %rbx
	je	.L1212
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1212
	call	_ZdlPv@PLT
.L1212:
	movq	32(%r14), %r15
	movq	24(%r14), %r13
	cmpq	%r13, %r15
	je	.L1213
	.p2align 4,,10
	.p2align 3
.L1217:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1214
	movq	(%rdi), %rax
	addq	$16, %r13
	call	*24(%rax)
	cmpq	%r13, %r15
	jne	.L1217
.L1215:
	movq	24(%r14), %r13
.L1213:
	testq	%r13, %r13
	je	.L1218
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	cmpq	%r12, %rbx
	je	.L1219
.L1220:
	movq	%rbx, %r14
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1214:
	addq	$16, %r13
	cmpq	%r13, %r15
	jne	.L1217
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	cmpq	%r12, %rbx
	jne	.L1220
.L1219:
	movq	-600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1208
	call	_ZdlPv@PLT
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	-1456(%rbp), %rsi
	leaq	-640(%rbp), %rdi
	xorl	%edx, %edx
.LEHB46:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE46:
	movq	%rax, -640(%rbp)
	movq	%rax, %rdi
	movq	-1136(%rbp), %rax
	movq	%rax, -624(%rbp)
.L1009:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-1136(%rbp), %r13
	movq	-640(%rbp), %rax
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1651:
.LEHB47:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
.LEHE47:
	movq	(%rax), %rax
	movb	$1, (%rax)
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L975:
	leaq	-1080(%rbp), %rax
	movl	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1064(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1048(%rbp)
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1023:
	movl	$0, 168(%r12)
	movq	$0, 176(%r12)
	movq	%rdx, 184(%r12)
	movq	%rdx, 192(%r12)
	movq	$0, 200(%r12)
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1021:
	movl	$0, 72(%r12)
	movq	$0, 80(%r12)
	movq	%rdx, 88(%r12)
	movq	%rdx, 96(%r12)
	movq	$0, 104(%r12)
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1019:
	leaq	-968(%rbp), %rbx
	movl	$0, -968(%rbp)
	movq	$0, -960(%rbp)
	movq	%rbx, -952(%rbp)
	movq	%rbx, -944(%rbp)
	movq	$0, -936(%rbp)
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1017:
	leaq	-1064(%rbp), %r13
	movl	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%r13, -1048(%rbp)
	movq	%r13, -1040(%rbp)
	movq	$0, -1032(%rbp)
	jmp	.L1018
.L1652:
	leaq	.LC0(%rip), %rdi
.LEHB48:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE48:
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	-1088(%rbp), %rdx
	leaq	48(%r13), %rcx
	movq	%rcx, 0(%r13)
	movq	%rdx, 48(%r13)
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	-1008(%rbp), %rdx
	leaq	128(%r13), %rcx
	movq	%rcx, 80(%r13)
	movq	%rdx, 128(%r13)
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1655:
	leaq	-1008(%rbp), %rdi
	movq	%rdi, -1056(%rbp)
	movq	128(%rcx), %rdx
	movq	%rdx, -1008(%rbp)
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1654:
	leaq	-1088(%rbp), %rdi
	movq	%rdi, -1136(%rbp)
	movq	48(%rcx), %rdx
	movq	%rdx, -1088(%rbp)
	jmp	.L1109
.L1660:
	call	__stack_chk_fail@PLT
.L1658:
	movq	-1424(%rbp), %rdi
	jmp	.L1009
.L1300:
	endbr64
	movq	%rax, %r12
	jmp	.L1297
.L1301:
	endbr64
	movq	%rax, %rbx
	jmp	.L1001
.L1304:
	endbr64
	movq	%rax, %rbx
	jmp	.L1165
.L1307:
	endbr64
	movq	%rax, %rbx
	jmp	.L994
.L1311:
	endbr64
	movq	%rax, -1472(%rbp)
	jmp	.L1056
.L1309:
	endbr64
	movq	%rax, %rbx
	jmp	.L1006
.L1308:
	endbr64
	movq	%rax, %rbx
	jmp	.L1000
.L1310:
	endbr64
	movq	%rax, %rbx
	jmp	.L1013
.L1312:
	endbr64
	movq	%rax, %rbx
	jmp	.L1164
.L1302:
	endbr64
	movq	%rax, %rbx
	jmp	.L1295
.L1303:
	endbr64
	movq	%rax, %rbx
	jmp	.L1079
.L1305:
	endbr64
	movq	%rax, %r12
	jmp	.L991
.L1306:
	endbr64
	movq	%rax, %r12
	jmp	.L990
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE,"a",@progbits
.LLSDA7173:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE7173-.LLSDACSB7173
.LLSDACSB7173:
	.uleb128 .LEHB28-.LFB7173
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB29-.LFB7173
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L1306-.LFB7173
	.uleb128 0
	.uleb128 .LEHB30-.LFB7173
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L1305-.LFB7173
	.uleb128 0
	.uleb128 .LEHB31-.LFB7173
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L1300-.LFB7173
	.uleb128 0
	.uleb128 .LEHB32-.LFB7173
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L1307-.LFB7173
	.uleb128 0
	.uleb128 .LEHB33-.LFB7173
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L1301-.LFB7173
	.uleb128 0
	.uleb128 .LEHB34-.LFB7173
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L1308-.LFB7173
	.uleb128 0
	.uleb128 .LEHB35-.LFB7173
	.uleb128 .LEHE35-.LEHB35
	.uleb128 .L1301-.LFB7173
	.uleb128 0
	.uleb128 .LEHB36-.LFB7173
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L1309-.LFB7173
	.uleb128 0
	.uleb128 .LEHB37-.LFB7173
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L1301-.LFB7173
	.uleb128 0
	.uleb128 .LEHB38-.LFB7173
	.uleb128 .LEHE38-.LEHB38
	.uleb128 .L1310-.LFB7173
	.uleb128 0
	.uleb128 .LEHB39-.LFB7173
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L1302-.LFB7173
	.uleb128 0
	.uleb128 .LEHB40-.LFB7173
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L1303-.LFB7173
	.uleb128 0
	.uleb128 .LEHB41-.LFB7173
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L1311-.LFB7173
	.uleb128 0
	.uleb128 .LEHB42-.LFB7173
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L1303-.LFB7173
	.uleb128 0
	.uleb128 .LEHB43-.LFB7173
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L1302-.LFB7173
	.uleb128 0
	.uleb128 .LEHB44-.LFB7173
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L1304-.LFB7173
	.uleb128 0
	.uleb128 .LEHB45-.LFB7173
	.uleb128 .LEHE45-.LEHB45
	.uleb128 .L1312-.LFB7173
	.uleb128 0
	.uleb128 .LEHB46-.LFB7173
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L1301-.LFB7173
	.uleb128 0
	.uleb128 .LEHB47-.LFB7173
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L1300-.LFB7173
	.uleb128 0
	.uleb128 .LEHB48-.LFB7173
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L1301-.LFB7173
	.uleb128 0
.LLSDACSE7173:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC7173
	.type	_ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE.cold, @function
_ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE.cold:
.LFSB7173:
.L1006:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-1128(%rbp), %rdx
	movq	%rdx, (%rax)
.L1001:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10TypeOracleEEERPNT_5ScopeEv@PLT
	movq	-1144(%rbp), %rdx
	movq	-1544(%rbp), %rdi
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque10TypeOracleD1Ev
.L995:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18TargetArchitectureEEERPNT_5ScopeEv@PLT
	movq	-1384(%rbp), %rdx
	movq	%rbx, %r12
	movq	%rdx, (%rax)
.L1297:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13GlobalContextEEERPNT_5ScopeEv@PLT
	movq	-656(%rbp), %rdx
	movq	-1480(%rbp), %rdi
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque13GlobalContextD1Ev
	movq	%r12, %rdi
.LEHB49:
	call	_Unwind_Resume@PLT
.LEHE49:
.L994:
	leaq	-1376(%rbp), %rdi
	call	_ZN2v88internal6torque10TypeOracleD1Ev
	jmp	.L995
.L1164:
	movq	%r13, %rdi
	call	_ZN2v88internal6torque10TypeOracleD1Ev
	movl	$232, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1165:
	movq	-1456(%rbp), %rdi
	call	_ZN2v88internal6torque10TypeOracleD1Ev
.L1295:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque21ImplementationVisitorD1Ev
.L1015:
	movq	-640(%rbp), %rdi
	cmpq	-1424(%rbp), %rdi
	je	.L1001
	call	_ZdlPv@PLT
	jmp	.L1001
.L1056:
	movq	208(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1057
	call	_ZdlPv@PLT
.L1057:
	leaq	160(%r12), %rax
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, -1488(%rbp)
	movq	176(%r12), %r13
	movq	%rax, -1416(%rbp)
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rdx, %xmm6
	movq	%rax, %xmm4
	movq	%r15, -1496(%rbp)
	punpcklqdq	%xmm4, %xmm6
	movaps	%xmm6, -1408(%rbp)
.L1061:
	testq	%r13, %r13
	jne	.L1661
	movq	-1488(%rbp), %r12
	movq	-1496(%rbp), %r15
	movq	144(%r12), %rbx
	movq	136(%r12), %r13
.L1064:
	cmpq	%r13, %rbx
	jne	.L1662
	movq	136(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1065
	call	_ZdlPv@PLT
.L1065:
	movq	120(%r12), %rbx
	movq	112(%r12), %r13
.L1068:
	cmpq	%r13, %rbx
	jne	.L1663
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1069
	call	_ZdlPv@PLT
.L1069:
	movq	80(%r12), %r13
	leaq	64(%r12), %rbx
	movq	%r13, %r14
	movq	%r12, %r13
	movq	%rbx, %r12
.L1073:
	testq	%r14, %r14
	jne	.L1664
	movq	48(%r13), %rbx
	movq	%r13, %r12
	movq	40(%r13), %r13
.L1076:
	cmpq	%r13, %rbx
	jne	.L1665
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1077
	call	_ZdlPv@PLT
.L1077:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1078
	call	_ZdlPv@PLT
.L1078:
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-1472(%rbp), %rbx
.L1079:
	movq	-1456(%rbp), %rdi
	call	_ZN2v88internal6torque13GlobalContextD1Ev
	jmp	.L1295
.L1000:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_12CurrentScopeEEERPNT_5ScopeEv@PLT
	movq	-1128(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L1001
.L1013:
	movq	-1488(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -336(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpb	$0, -608(%rbp)
	je	.L1015
	leaq	-600(%rbp), %rdi
	call	_ZN2v88internal6torque12CfgAssemblerD1Ev
	jmp	.L1015
.L1661:
	movq	-1416(%rbp), %rdi
	movq	24(%r13), %rsi
	leaq	432(%r13), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	16(%r13), %rax
	movdqa	-1408(%rbp), %xmm6
	movq	528(%r13), %rdi
	movq	%rax, -1440(%rbp)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 432(%r13)
	addq	$80, %rax
	movq	%rax, -1504(%rbp)
	movq	%rax, 560(%r13)
	leaq	544(%r13), %rax
	movups	%xmm6, 448(%r13)
	cmpq	%rax, %rdi
	je	.L1059
	call	_ZdlPv@PLT
.L1059:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	512(%r13), %rdi
	movq	%rax, 456(%r13)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	560(%r13), %rdi
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r12, 432(%r13)
	movq	-24(%r12), %rax
	movq	%rcx, (%r15,%rax)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, 448(%r13)
	movq	-24(%r14), %rax
	movq	%rcx, 448(%r13,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, 432(%r13)
	movq	-24(%rbx), %rax
	movq	%rcx, (%r15,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r15
	movq	$0, 440(%r13)
	movq	%r15, 560(%r13)
	call	_ZNSt8ios_baseD2Ev@PLT
	leaq	40(%r13), %rax
	movdqa	-1408(%rbp), %xmm6
	movq	136(%r13), %rdi
	movq	%rax, -1520(%rbp)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 40(%r13)
	movq	-1504(%rbp), %rax
	movups	%xmm6, 56(%r13)
	movq	%rax, 168(%r13)
	leaq	152(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1060
	call	_ZdlPv@PLT
.L1060:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	120(%r13), %rdi
	movq	%rax, 64(%r13)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r12, 40(%r13)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	leaq	168(%r13), %rdi
	movq	-24(%r12), %rax
	movq	%rsi, 40(%r13,%rax)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%r14, 56(%r13)
	movq	-24(%r14), %rax
	movq	%rsi, 56(%r13,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rbx, 40(%r13)
	movq	-24(%rbx), %rax
	movq	%rsi, 40(%r13,%rax)
	movq	$0, 48(%r13)
	movq	%r15, 168(%r13)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	movq	-1440(%rbp), %r13
	jmp	.L1061
.L990:
	movq	-1456(%rbp), %rdi
	call	_ZN2v88internal6torque3AstD1Ev
	movq	%r12, %rdi
.LEHB50:
	call	_Unwind_Resume@PLT
.L991:
	movq	-1480(%rbp), %rdi
	call	_ZN2v88internal6torque13GlobalContextD1Ev
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE50:
.L1662:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1063
	call	_ZdlPv@PLT
.L1063:
	addq	$32, %r13
	jmp	.L1064
.L1663:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1067
	movq	(%rdi), %rax
	call	*8(%rax)
.L1067:
	addq	$8, %r13
	jmp	.L1068
.L1664:
	movq	24(%r14), %rsi
	movq	%r12, %rdi
	leaq	40(%r14), %rbx
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	16(%r14), %rax
	movq	56(%r14), %r9
	movq	%rax, -1408(%rbp)
.L1072:
	testq	%r9, %r9
	jne	.L1666
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	movq	-1408(%rbp), %r14
	jmp	.L1073
.L1666:
	movq	24(%r9), %rsi
	movq	%rbx, %rdi
	movq	%r9, -1416(%rbp)
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	-1416(%rbp), %r9
	movq	16(%r9), %rax
	movq	%r9, %rdi
	movq	%rax, -1416(%rbp)
	call	_ZdlPv@PLT
	movq	-1416(%rbp), %r9
	jmp	.L1072
.L1665:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1075
	movq	(%rdi), %rax
	call	*8(%rax)
.L1075:
	addq	$8, %r13
	jmp	.L1076
	.cfi_endproc
.LFE7173:
	.section	.gcc_except_table._ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE
.LLSDAC7173:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSEC7173-.LLSDACSBC7173
.LLSDACSBC7173:
	.uleb128 .LEHB49-.LCOLDB4
	.uleb128 .LEHE49-.LEHB49
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB50-.LCOLDB4
	.uleb128 .LEHE50-.LEHB50
	.uleb128 0
	.uleb128 0
.LLSDACSEC7173:
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE
	.size	_ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE, .-_ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE
	.section	.text.unlikely._ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE
	.size	_ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE.cold, .-_ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE.cold
.LCOLDE4:
	.section	.text._ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE
.LHOTE4:
	.section	.text.unlikely._ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE,"ax",@progbits
.LCOLDB6:
	.section	.text._ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE,"ax",@progbits
.LHOTB6:
	.p2align 4
	.globl	_ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
	.type	_ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE, @function
_ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE:
.LFB7327:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7327
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$568, %rsp
	movq	32(%rdx), %r8
	movq	40(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -128(%rbp)
	movq	%r8, %rax
	addq	%r14, %rax
	je	.L1668
	testq	%r8, %r8
	je	.L2244
.L1668:
	movq	%r14, -320(%rbp)
	cmpq	$15, %r14
	ja	.L2245
	cmpq	$1, %r14
	jne	.L1671
	movzbl	(%r8), %eax
	movb	%al, -112(%rbp)
	movq	%r12, %rax
.L1672:
	movq	%r14, -120(%rbp)
	pxor	%xmm0, %xmm0
	movb	$0, (%rax,%r14)
	leaq	-152(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -168(%rbp)
	movq	-128(%rbp), %rax
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	cmpq	%r12, %rax
	je	.L2246
	movq	%rax, -168(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -152(%rbp)
.L1674:
	movq	-120(%rbp), %rax
	movq	%rax, -160(%rbp)
.LEHB51:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
.LEHE51:
	leaq	-192(%rbp), %rcx
	leaq	-128(%rbp), %r14
	xorl	%edx, %edx
	movq	%rcx, (%rax)
	leaq	-320(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%r12, -128(%rbp)
	movq	%rcx, -600(%rbp)
	movq	$17, -320(%rbp)
	movq	%rax, -528(%rbp)
.LEHB52:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE52:
	movq	-320(%rbp), %rdx
	movdqa	.LC5(%rip), %xmm0
	movq	%rax, -128(%rbp)
	movq	%r14, %rdi
	movq	%rdx, -112(%rbp)
	movb	$113, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-320(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
.LEHB53:
	call	_ZN2v88internal6torque13SourceFileMap9AddSourceENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movl	%eax, -480(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
.LEHE53:
	leaq	-480(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1677
	call	_ZdlPv@PLT
.L1677:
	pxor	%xmm0, %xmm0
	leaq	-376(%rbp), %rax
	movl	$0, -376(%rbp)
	movq	$0, -368(%rbp)
	movq	%rax, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -344(%rbp)
	movaps	%xmm0, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
.LEHB54:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
.LEHE54:
	leaq	-432(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rax)
	movq	%rcx, -592(%rbp)
	movq	$0, -448(%rbp)
	movaps	%xmm0, -464(%rbp)
.LEHB55:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -440(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv@PLT
.LEHE55:
	leaq	-464(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rax)
	leaq	-312(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	%rax, -288(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rcx, -584(%rbp)
	movl	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -280(%rbp)
	movl	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -248(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -232(%rbp)
	movaps	%xmm0, -224(%rbp)
.LEHB56:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
.LEHE56:
	movq	-528(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rcx, (%rax)
	leaq	72(%r15), %rax
	movq	%rax, -536(%rbp)
	movq	%rax, 88(%r15)
	movq	%rax, 96(%r15)
	leaq	120(%r15), %rax
	movb	$0, (%r15)
	movb	$0, 8(%r15)
	movl	$0, 72(%r15)
	movq	$0, 80(%r15)
	movq	$0, 104(%r15)
	movl	$0, 120(%r15)
	movq	$0, 128(%r15)
	movq	%rax, 136(%r15)
	movq	%rax, 144(%r15)
	movq	$0, 152(%r15)
	movq	$0, 192(%r15)
	movups	%xmm0, 160(%r15)
	movups	%xmm0, 176(%r15)
	movq	%rax, -520(%rbp)
.LEHB57:
	call	_ZN2v88internal6torque11ParseTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE57:
	movq	(%rbx), %rax
	movq	8(%rbx), %r13
	movq	%r12, -128(%rbp)
	movq	%rax, %rcx
	movq	%rax, -512(%rbp)
	addq	%r13, %rcx
	je	.L1687
	testq	%rax, %rax
	je	.L2247
.L1687:
	movq	%r13, -488(%rbp)
	cmpq	$15, %r13
	ja	.L2248
	cmpq	$1, %r13
	jne	.L1690
	movq	-512(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -112(%rbp)
	movq	%r12, %rax
.L1691:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	leaq	-80(%rbp), %rax
	movq	40(%rbx), %r13
	movq	%rax, -512(%rbp)
	movq	%rax, -96(%rbp)
	movq	32(%rbx), %rax
	movq	%rax, %rcx
	movq	%rax, -552(%rbp)
	addq	%r13, %rcx
	je	.L1692
	testq	%rax, %rax
	je	.L2249
.L1692:
	movq	%r13, -488(%rbp)
	cmpq	$15, %r13
	ja	.L2250
	cmpq	$1, %r13
	jne	.L1695
	movq	-552(%rbp), %rax
	movzbl	(%rax), %eax
	movb	%al, -80(%rbp)
	movq	-512(%rbp), %rax
.L1696:
	movq	%r13, -88(%rbp)
	movq	%r14, %rdi
	movb	$0, (%rax,%r13)
	movzbl	66(%rbx), %eax
	movzwl	64(%rbx), %edx
	movb	%al, -62(%rbp)
	movw	%dx, -64(%rbp)
.LEHB58:
	call	_ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE
.LEHE58:
	movq	-96(%rbp), %rdi
	cmpq	-512(%rbp), %rdi
	je	.L1701
	call	_ZdlPv@PLT
.L1701:
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1935
	call	_ZdlPv@PLT
.L1935:
.LEHB59:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	leaq	8(%r15), %rdi
	cmpb	$0, (%r15)
	movq	(%rax), %r13
	movq	%rdi, -576(%rbp)
	je	.L1703
	movq	%r13, %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	leaq	24(%r13), %rsi
	leaq	32(%r15), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.L1704:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
	movq	80(%r15), %r14
	movq	(%rax), %rbx
	leaq	64(%r15), %r13
	testq	%r14, %r14
	je	.L1734
.L1730:
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r14), %rdi
	movq	16(%r14), %r12
	testq	%rdi, %rdi
	je	.L1733
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1734
.L1735:
	movq	%r12, %r14
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1671:
	testq	%r14, %r14
	jne	.L2251
	movq	%r12, %rax
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1735
.L1734:
	movq	-536(%rbp), %rax
	movq	$0, 80(%r15)
	movq	$0, 104(%r15)
	movq	%rax, 88(%r15)
	movq	%rax, 96(%r15)
	cmpq	$0, 16(%rbx)
	je	.L1732
	movq	%rax, %rcx
	movl	8(%rbx), %eax
	movl	%eax, 72(%r15)
	movq	16(%rbx), %rax
	movq	%rax, 80(%r15)
	movq	24(%rbx), %rdx
	movq	%rdx, 88(%r15)
	movq	32(%rbx), %rdx
	movq	%rdx, 96(%r15)
	movq	%rcx, 8(%rax)
	movq	40(%rbx), %rax
	movq	%rax, 104(%r15)
	leaq	8(%rbx), %rax
	movq	$0, 16(%rbx)
	movq	%rax, 24(%rbx)
	movq	%rax, 32(%rbx)
	movq	$0, 40(%rbx)
.L1732:
	movq	128(%r15), %r14
	leaq	112(%r15), %r13
	testq	%r14, %r14
	je	.L1740
.L1736:
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r14), %rdi
	movq	16(%r14), %r12
	testq	%rdi, %rdi
	je	.L1739
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L1740
.L1741:
	movq	%r12, %r14
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1739:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1741
.L1740:
	movq	-520(%rbp), %rax
	movq	$0, 128(%r15)
	movq	$0, 152(%r15)
	movq	%rax, 136(%r15)
	movq	%rax, 144(%r15)
	cmpq	$0, 64(%rbx)
	je	.L1738
	movl	56(%rbx), %eax
	movl	%eax, 120(%r15)
	movq	64(%rbx), %rax
	movq	%rax, 128(%r15)
	movq	72(%rbx), %rdx
	movq	%rdx, 136(%r15)
	movq	80(%rbx), %rdx
	movq	%rdx, 144(%r15)
	movq	-520(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	88(%rbx), %rax
	movq	%rax, 152(%r15)
	leaq	56(%rbx), %rax
	movq	$0, 64(%rbx)
	movq	%rax, 72(%rbx)
	movq	%rax, 80(%rbx)
	movq	$0, 88(%rbx)
.L1738:
	movq	96(%rbx), %rax
	movq	$0, 96(%rbx)
	movq	160(%r15), %r12
	movq	%rax, 160(%r15)
	testq	%r12, %r12
	je	.L1742
	movq	208(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1743
	call	_ZdlPv@PLT
.L1743:
	movq	176(%r12), %r13
	leaq	160(%r12), %rax
	movq	%rax, -536(%rbp)
	testq	%r13, %r13
	je	.L1749
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, -560(%rbp)
	leaq	-40(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rdx, %xmm7
	movq	%rbx, -552(%rbp)
	movq	%rcx, -520(%rbp)
	punpcklqdq	%xmm7, %xmm1
	leaq	40(%rax), %rcx
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rsi
	movq	%r15, -568(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r14
	movq	%rcx, %r15
	movq	%rsi, %rbx
	movaps	%xmm1, -512(%rbp)
.L1750:
	movq	24(%r13), %rsi
	movq	-536(%rbp), %rdi
	movq	%r13, %r12
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	-520(%rbp), %rax
	movq	16(%r13), %r13
	movq	%r15, 560(%r12)
	movdqa	-512(%rbp), %xmm3
	movq	528(%r12), %rdi
	movq	%rax, 432(%r12)
	leaq	544(%r12), %rax
	movups	%xmm3, 448(%r12)
	cmpq	%rax, %rdi
	je	.L1747
	call	_ZdlPv@PLT
.L1747:
	movq	%rbx, 456(%r12)
	leaq	512(%r12), %rdi
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	560(%r12), %rdi
	movq	%rax, 432(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 448(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 448(%r12,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 432(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	$0, 440(%r12)
	movq	%r14, 560(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rax
	movdqa	-512(%rbp), %xmm4
	movq	%r15, 168(%r12)
	movq	136(%r12), %rdi
	movq	%rax, 40(%r12)
	leaq	152(%r12), %rax
	movups	%xmm4, 56(%r12)
	cmpq	%rax, %rdi
	je	.L1748
	call	_ZdlPv@PLT
.L1748:
	movq	%rbx, 64(%r12)
	leaq	120(%r12), %rdi
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	168(%r12), %rdi
	movq	%rax, 40(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 40(%r12,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 56(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 56(%r12,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 40(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 40(%r12,%rax)
	movq	$0, 48(%r12)
	movq	%r14, 168(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1750
	movq	-552(%rbp), %rbx
	movq	-560(%rbp), %r12
	movq	-568(%rbp), %r15
.L1749:
	movq	144(%r12), %r14
	movq	136(%r12), %r13
	cmpq	%r13, %r14
	je	.L1745
	.p2align 4,,10
	.p2align 3
.L1746:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1751
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %r14
	jne	.L1746
.L1752:
	movq	136(%r12), %r13
.L1745:
	testq	%r13, %r13
	je	.L1754
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1754:
	movq	120(%r12), %r14
	movq	112(%r12), %r13
	cmpq	%r13, %r14
	je	.L1755
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1756
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %r14
	jne	.L1759
.L1757:
	movq	112(%r12), %r13
.L1755:
	testq	%r13, %r13
	je	.L1760
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1760:
	movq	80(%r12), %r13
	leaq	64(%r12), %rax
	testq	%r13, %r13
	je	.L1765
	movq	%r12, -520(%rbp)
	movq	%r15, -536(%rbp)
	movq	%rbx, -512(%rbp)
	movq	%r13, %rbx
	movq	%rax, %r13
.L1761:
	movq	24(%rbx), %rsi
	movq	%rbx, %r12
	movq	%r13, %rdi
	leaq	40(%r12), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r12), %r14
	movq	16(%rbx), %rbx
	testq	%r14, %r14
	je	.L1766
.L1764:
	movq	24(%r14), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r14, %rdi
	movq	16(%r14), %r14
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L1764
.L1766:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1761
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r12
	movq	-536(%rbp), %r15
.L1765:
	movq	48(%r12), %r14
	movq	40(%r12), %r13
	cmpq	%r13, %r14
	je	.L1762
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1767
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %r14
	jne	.L1763
.L1768:
	movq	40(%r12), %r13
.L1762:
	testq	%r13, %r13
	je	.L1770
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1770:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1771
	call	_ZdlPv@PLT
.L1771:
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1742:
	movq	104(%rbx), %rax
	movq	$0, 104(%rbx)
	movq	168(%r15), %r12
	movq	%rax, 168(%r15)
	testq	%r12, %r12
	je	.L1772
	movq	216(%r12), %rbx
	movq	208(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1773
	.p2align 4,,10
	.p2align 3
.L1777:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1774
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L1777
.L1775:
	movq	208(%r12), %r13
.L1773:
	testq	%r13, %r13
	je	.L1778
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1778:
	movq	192(%r12), %rbx
	movq	184(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1779
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1780
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L1783
.L1781:
	movq	184(%r12), %r13
.L1779:
	testq	%r13, %r13
	je	.L1784
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1784:
	movq	168(%r12), %rbx
	movq	160(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1785
	.p2align 4,,10
	.p2align 3
.L1789:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1786
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L1789
.L1787:
	movq	160(%r12), %r13
.L1785:
	testq	%r13, %r13
	je	.L1790
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1790:
	movq	144(%r12), %rbx
	movq	136(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1791
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1792
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L1795
.L1793:
	movq	136(%r12), %r13
.L1791:
	testq	%r13, %r13
	je	.L1796
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1796:
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rcx
	movq	96(%r12), %rax
	movq	%rcx, -520(%rbp)
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rcx
	movq	%rcx, -512(%rbp)
	testq	%rax, %rax
	je	.L1804
	movq	%r12, -536(%rbp)
	movq	%rax, %rbx
	movq	%r15, -552(%rbp)
	.p2align 4,,10
	.p2align 3
.L1797:
	movq	%rbx, %r12
	movq	-520(%rbp), %rax
	movq	(%rbx), %rbx
	movq	96(%r12), %r13
	leaq	80(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r13, %r13
	je	.L1803
.L1800:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1800
.L1803:
	movq	-512(%rbp), %rax
	movq	48(%r12), %r15
	leaq	32(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r15, %r15
	je	.L1801
.L1802:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L1805
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L1801
.L1806:
	movq	%r13, %r15
	jmp	.L1802
	.p2align 4,,10
	.p2align 3
.L1792:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1795
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1786:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1789
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1805:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1806
.L1801:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1797
	movq	-536(%rbp), %r12
	movq	-552(%rbp), %r15
.L1804:
	movq	88(%r12), %rax
	movq	80(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%r12), %rdi
	leaq	128(%r12), %rax
	movq	$0, 104(%r12)
	movq	$0, 96(%r12)
	cmpq	%rax, %rdi
	je	.L1798
	call	_ZdlPv@PLT
.L1798:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1807
	call	_ZdlPv@PLT
.L1807:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	16(%r12), %rbx
	movq	%rax, -512(%rbp)
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	%rax, -520(%rbp)
	testq	%rbx, %rbx
	je	.L1813
	movq	%r12, -536(%rbp)
	movq	%r15, -552(%rbp)
	.p2align 4,,10
	.p2align 3
.L1808:
	movq	%rbx, %r12
	movq	-520(%rbp), %rax
	movq	(%rbx), %rbx
	movq	80(%r12), %rdi
	movq	%rax, 8(%r12)
	testq	%rdi, %rdi
	je	.L1811
	call	_ZdlPv@PLT
.L1811:
	movq	-512(%rbp), %rax
	movq	48(%r12), %r15
	leaq	32(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r15, %r15
	je	.L1815
.L1812:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L1814
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L1815
.L1816:
	movq	%r13, %r15
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L1780:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1783
	jmp	.L1781
	.p2align 4,,10
	.p2align 3
.L1774:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1777
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1814:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1816
.L1815:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1808
	movq	-536(%rbp), %r12
	movq	-552(%rbp), %r15
.L1813:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L1809
	call	_ZdlPv@PLT
.L1809:
	movl	$232, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1772:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv@PLT
.LEHE59:
	movq	(%rax), %rax
	movq	176(%r15), %r12
	pxor	%xmm0, %xmm0
	movq	184(%r15), %r13
	movq	(%rax), %rdx
	movq	%r12, %rbx
	movq	%rdx, 176(%r15)
	movq	8(%rax), %rdx
	movq	%rdx, 184(%r15)
	movq	16(%rax), %rdx
	movq	%rdx, 192(%r15)
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	%r13, %r12
	je	.L1821
	.p2align 4,,10
	.p2align 3
.L1817:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1820
	call	_ZdlPv@PLT
	addq	$64, %rbx
	cmpq	%r13, %rbx
	jne	.L1817
.L1821:
	testq	%r12, %r12
	je	.L1819
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1819:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
	movq	-208(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-216(%rbp), %r12
	testq	%r12, %r12
	je	.L1823
	movq	216(%r12), %rbx
	movq	208(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1824
	.p2align 4,,10
	.p2align 3
.L1828:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1825
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L1828
.L1826:
	movq	208(%r12), %r13
.L1824:
	testq	%r13, %r13
	je	.L1829
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1829:
	movq	192(%r12), %rbx
	movq	184(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1830
	.p2align 4,,10
	.p2align 3
.L1834:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1831
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%rbx, %r13
	jne	.L1834
.L1832:
	movq	184(%r12), %r13
.L1830:
	testq	%r13, %r13
	je	.L1835
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1835:
	movq	168(%r12), %rbx
	movq	160(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1836
	.p2align 4,,10
	.p2align 3
.L1840:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1837
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L1840
.L1838:
	movq	160(%r12), %r13
.L1836:
	testq	%r13, %r13
	je	.L1841
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1841:
	movq	144(%r12), %rbx
	movq	136(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1842
	.p2align 4,,10
	.p2align 3
.L1846:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1843
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L1846
.L1844:
	movq	136(%r12), %r13
.L1842:
	testq	%r13, %r13
	je	.L1847
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1847:
	movq	96(%r12), %rax
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rdx
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rcx
	movq	%rdx, -520(%rbp)
	movq	%rcx, -512(%rbp)
	testq	%rax, %rax
	je	.L1855
	movq	%r12, -536(%rbp)
	movq	%rax, %rbx
	movq	%r15, -552(%rbp)
	.p2align 4,,10
	.p2align 3
.L1848:
	movq	%rbx, %r12
	movq	-520(%rbp), %rax
	movq	(%rbx), %rbx
	movq	96(%r12), %r13
	leaq	80(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r13, %r13
	je	.L1854
.L1851:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1851
.L1854:
	movq	-512(%rbp), %rax
	movq	48(%r12), %r15
	leaq	32(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r15, %r15
	je	.L1852
.L1853:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L1856
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L1852
.L1857:
	movq	%r13, %r15
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1843:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1846
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1856:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1857
.L1852:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1848
	movq	-536(%rbp), %r12
	movq	-552(%rbp), %r15
.L1855:
	movq	88(%r12), %rax
	movq	80(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%r12), %rdi
	leaq	128(%r12), %rax
	movq	$0, 104(%r12)
	movq	$0, 96(%r12)
	cmpq	%rax, %rdi
	je	.L1849
	call	_ZdlPv@PLT
.L1849:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1858
	call	_ZdlPv@PLT
.L1858:
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rax
	movq	16(%r12), %rbx
	movq	%rax, -512(%rbp)
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rax
	movq	%rax, -520(%rbp)
	testq	%rbx, %rbx
	je	.L1864
	movq	%r12, -536(%rbp)
	movq	%r15, -552(%rbp)
	.p2align 4,,10
	.p2align 3
.L1859:
	movq	%rbx, %r12
	movq	-520(%rbp), %rax
	movq	(%rbx), %rbx
	movq	80(%r12), %rdi
	movq	%rax, 8(%r12)
	testq	%rdi, %rdi
	je	.L1862
	call	_ZdlPv@PLT
.L1862:
	movq	-512(%rbp), %rax
	movq	48(%r12), %r15
	leaq	32(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r15, %r15
	je	.L1866
.L1863:
	movq	24(%r15), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r15), %rdi
	leaq	48(%r15), %rax
	movq	16(%r15), %r13
	cmpq	%rax, %rdi
	je	.L1865
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L1866
.L1867:
	movq	%r13, %r15
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1837:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1840
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1820:
	addq	$64, %rbx
	cmpq	%rbx, %r13
	jne	.L1817
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1825:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1828
	jmp	.L1826
	.p2align 4,,10
	.p2align 3
.L1831:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1834
	jmp	.L1832
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1867
.L1866:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1859
	movq	-536(%rbp), %r12
	movq	-552(%rbp), %r15
.L1864:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L1860
	call	_ZdlPv@PLT
.L1860:
	movl	$232, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1823:
	movq	-224(%rbp), %r12
	testq	%r12, %r12
	je	.L1868
	movq	208(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1869
	call	_ZdlPv@PLT
.L1869:
	movq	176(%r12), %rbx
	leaq	160(%r12), %rax
	movq	%rax, -552(%rbp)
	testq	%rbx, %rbx
	je	.L1875
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, -560(%rbp)
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	leaq	40(%rax), %rsi
	movq	%rax, %xmm2
	movq	%rdx, %xmm7
	movq	%r15, -568(%rbp)
	movq	%rsi, -520(%rbp)
	leaq	-40(%rax), %rcx
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rsi
	punpcklqdq	%xmm7, %xmm2
	movq	%rsi, -536(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r13
	movq	%rcx, %r15
	movaps	%xmm2, -512(%rbp)
.L1876:
	movq	24(%rbx), %rsi
	movq	-552(%rbp), %rdi
	movq	%rbx, %r12
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	-520(%rbp), %rax
	movq	16(%rbx), %rbx
	movq	%r15, 432(%r12)
	movdqa	-512(%rbp), %xmm5
	movq	528(%r12), %rdi
	movq	%rax, 560(%r12)
	leaq	544(%r12), %rax
	movups	%xmm5, 448(%r12)
	cmpq	%rax, %rdi
	je	.L1873
	call	_ZdlPv@PLT
.L1873:
	movq	-536(%rbp), %rax
	leaq	512(%r12), %rdi
	movq	%rax, 456(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 432(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	560(%r12), %rdi
	movq	-24(%r14), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 448(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 448(%r12,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 432(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	$0, 440(%r12)
	movq	%r13, 560(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-520(%rbp), %rax
	movq	%r15, 40(%r12)
	movdqa	-512(%rbp), %xmm6
	movq	136(%r12), %rdi
	movq	%rax, 168(%r12)
	leaq	152(%r12), %rax
	movups	%xmm6, 56(%r12)
	cmpq	%rax, %rdi
	je	.L1874
	call	_ZdlPv@PLT
.L1874:
	movq	-536(%rbp), %rax
	leaq	120(%r12), %rdi
	movq	%rax, 64(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, 40(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	168(%r12), %rdi
	movq	-24(%r14), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rdx, 40(%r12,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, 56(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 56(%r12,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, 40(%r12)
	movq	-24(%rax), %rax
	movq	%rdx, 40(%r12,%rax)
	movq	$0, 48(%r12)
	movq	%r13, 168(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1876
	movq	-560(%rbp), %r12
	movq	-568(%rbp), %r15
.L1875:
	movq	144(%r12), %rbx
	movq	136(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1871
	.p2align 4,,10
	.p2align 3
.L1872:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1877
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1872
.L1878:
	movq	136(%r12), %r13
.L1871:
	testq	%r13, %r13
	je	.L1880
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1880:
	movq	120(%r12), %rbx
	movq	112(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1881
	.p2align 4,,10
	.p2align 3
.L1885:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1882
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L1885
.L1883:
	movq	112(%r12), %r13
.L1881:
	testq	%r13, %r13
	je	.L1886
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1886:
	movq	80(%r12), %r14
	leaq	64(%r12), %rbx
	testq	%r14, %r14
	je	.L1891
	movq	%r12, -512(%rbp)
	movq	%r15, -520(%rbp)
.L1887:
	movq	24(%r14), %rsi
	movq	%r14, %r12
	movq	%rbx, %rdi
	leaq	40(%r12), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r12), %r13
	movq	16(%r14), %r14
	testq	%r13, %r13
	je	.L1892
.L1890:
	movq	24(%r13), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L1890
.L1892:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L1887
	movq	-512(%rbp), %r12
	movq	-520(%rbp), %r15
.L1891:
	movq	48(%r12), %rbx
	movq	40(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1888
	.p2align 4,,10
	.p2align 3
.L1889:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1893
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r13, %rbx
	jne	.L1889
.L1894:
	movq	40(%r12), %r13
.L1888:
	testq	%r13, %r13
	je	.L1896
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1896:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1897
	call	_ZdlPv@PLT
.L1897:
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1868:
	movq	-256(%rbp), %r13
	leaq	-272(%rbp), %r12
	testq	%r13, %r13
	je	.L1902
.L1898:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L1901
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1902
.L1903:
	movq	%rbx, %r13
	jmp	.L1898
	.p2align 4,,10
	.p2align 3
.L1893:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1889
	jmp	.L1894
	.p2align 4,,10
	.p2align 3
.L1882:
	addq	$8, %r13
	cmpq	%r13, %rbx
	jne	.L1885
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1877:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1872
	jmp	.L1878
	.p2align 4,,10
	.p2align 3
.L1901:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1903
.L1902:
	movq	-304(%rbp), %r12
	movq	-528(%rbp), %r13
	testq	%r12, %r12
	je	.L1899
.L1900:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L1906
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1899
.L1907:
	movq	%rbx, %r12
	jmp	.L1900
	.p2align 4,,10
	.p2align 3
.L1767:
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L1763
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1751:
	addq	$32, %r13
	cmpq	%r13, %r14
	jne	.L1746
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1756:
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	.L1759
	jmp	.L1757
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1907
.L1899:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv@PLT
	movq	-440(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-456(%rbp), %rbx
	movq	-464(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1904
	.p2align 4,,10
	.p2align 3
.L1905:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1908
	call	_ZdlPv@PLT
	addq	$64, %r12
	cmpq	%r12, %rbx
	jne	.L1905
.L1909:
	movq	-464(%rbp), %r12
.L1904:
	testq	%r12, %r12
	je	.L1911
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1911:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	-336(%rbp), %rdx
	leaq	-384(%rbp), %r13
	movq	%rdx, (%rax)
	movq	-368(%rbp), %r12
	testq	%r12, %r12
	je	.L1916
	movq	%r15, -512(%rbp)
.L1912:
	movq	24(%r12), %rsi
	movq	%r12, %r14
	movq	%r13, %rdi
	leaq	40(%r14), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r14), %rbx
	movq	16(%r12), %r12
	testq	%rbx, %rbx
	je	.L1917
.L1915:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1915
.L1917:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L1912
	movq	-512(%rbp), %r15
.L1916:
	movq	-400(%rbp), %rbx
	movq	-408(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1913
	.p2align 4,,10
	.p2align 3
.L1914:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1918
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L1914
.L1919:
	movq	-408(%rbp), %r12
.L1913:
	testq	%r12, %r12
	je	.L1921
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1921:
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1922
	call	_ZdlPv@PLT
.L1922:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	-472(%rbp), %rdx
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	movq	-136(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-168(%rbp), %rdi
	cmpq	-544(%rbp), %rdi
	je	.L1923
	call	_ZdlPv@PLT
.L1923:
	movq	-184(%rbp), %rbx
	movq	-192(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1924
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1925
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1928
.L1926:
	movq	-192(%rbp), %r12
.L1924:
	testq	%r12, %r12
	je	.L1667
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1667:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2252
	addq	$568, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1918:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L1914
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1925:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1928
	jmp	.L1926
	.p2align 4,,10
	.p2align 3
.L1908:
	addq	$64, %r12
	cmpq	%r12, %rbx
	jne	.L1905
	jmp	.L1909
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	8(%r13), %rbx
	subq	0(%r13), %rbx
	pxor	%xmm0, %xmm0
	movq	$0, 24(%r15)
	movq	%rbx, %rax
	movups	%xmm0, 8(%r15)
	sarq	$5, %rax
	je	.L2253
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L2254
	movq	%rbx, %rdi
.LEHB60:
	call	_Znwm@PLT
.LEHE60:
	movq	%rax, -568(%rbp)
.L1706:
	movq	-568(%rbp), %rdx
	movq	%rdx, %xmm0
	addq	%rdx, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 24(%r15)
	movups	%xmm0, 8(%r15)
	movq	8(%r13), %rax
	movq	%rax, -552(%rbp)
	movq	%rax, %rcx
	movq	0(%r13), %rax
	cmpq	%rax, %rcx
	je	.L1940
	movq	%rax, -512(%rbp)
	leaq	-488(%rbp), %rax
	movq	%rdx, %rbx
	movq	%rax, -560(%rbp)
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L2257:
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L1713:
	movq	%r12, 8(%rbx)
	addq	$32, %rbx
	addq	$32, -512(%rbp)
	movq	-512(%rbp), %rax
	movb	$0, (%rdi,%r12)
	cmpq	%rax, -552(%rbp)
	je	.L1708
.L1714:
	movq	-512(%rbp), %rax
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	(%rax), %r14
	movq	8(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1709
	testq	%r14, %r14
	je	.L2255
.L1709:
	movq	%r12, -488(%rbp)
	cmpq	$15, %r12
	ja	.L2256
	cmpq	$1, %r12
	je	.L2257
	testq	%r12, %r12
	je	.L1713
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L2256:
	movq	-560(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB61:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE61:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1711:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1713
	.p2align 4,,10
	.p2align 3
.L1940:
	movq	%rdx, %rbx
	.p2align 4,,10
	.p2align 3
.L1708:
	leaq	48(%r15), %rdi
	movq	%rbx, 16(%r15)
	movq	%rdi, 32(%r15)
	movq	24(%r13), %r12
	movq	32(%r13), %r13
	movq	%r12, %rax
	addq	%r13, %rax
	je	.L1717
	testq	%r12, %r12
	je	.L2258
.L1717:
	movq	%r13, -488(%rbp)
	cmpq	$15, %r13
	ja	.L2259
	cmpq	$1, %r13
	jne	.L1727
	movzbl	(%r12), %eax
	movb	%al, 48(%r15)
.L1728:
	movq	%r13, 40(%r15)
	movb	$0, (%rdi,%r13)
	movb	$1, (%r15)
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1695:
	testq	%r13, %r13
	jne	.L2260
	movq	-512(%rbp), %rax
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1690:
	testq	%r13, %r13
	jne	.L2261
	movq	%r12, %rax
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L2245:
	leaq	-128(%rbp), %rdi
	leaq	-320(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -512(%rbp)
.LEHB62:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE62:
	movq	-512(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-320(%rbp), %rax
	movq	%rax, -112(%rbp)
.L1670:
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-320(%rbp), %r14
	movq	-128(%rbp), %rax
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L2250:
	leaq	-488(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
.LEHB63:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE63:
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -80(%rbp)
.L1694:
	movq	-552(%rbp), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-488(%rbp), %r13
	movq	-96(%rbp), %rax
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L2248:
	leaq	-488(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
.LEHB64:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE64:
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -112(%rbp)
.L1689:
	movq	-512(%rbp), %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	-488(%rbp), %r13
	movq	-128(%rbp), %rax
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L2246:
	movdqa	-112(%rbp), %xmm7
	movups	%xmm7, -152(%rbp)
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1727:
	testq	%r13, %r13
	je	.L1728
	jmp	.L1726
	.p2align 4,,10
	.p2align 3
.L2259:
	leaq	-488(%rbp), %rsi
	leaq	32(%r15), %rdi
	xorl	%edx, %edx
.LEHB65:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE65:
	movq	%rax, 32(%r15)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, 48(%r15)
.L1726:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %r13
	movq	32(%r15), %rdi
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	$0, -568(%rbp)
	jmp	.L1706
.L2244:
	leaq	.LC0(%rip), %rdi
.LEHB66:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE66:
.L2255:
	leaq	.LC0(%rip), %rdi
.LEHB67:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE67:
.L2252:
	call	__stack_chk_fail@PLT
.L2258:
	leaq	.LC0(%rip), %rdi
.LEHB68:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE68:
.L2247:
	leaq	.LC0(%rip), %rdi
.LEHB69:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE69:
.L2249:
	leaq	.LC0(%rip), %rdi
.LEHB70:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE70:
.L2254:
.LEHB71:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE71:
.L2251:
	movq	%r12, %rdi
	jmp	.L1670
.L2261:
	movq	%r12, %rdi
	jmp	.L1689
.L2260:
	movq	-512(%rbp), %rdi
	jmp	.L1694
.L1943:
	endbr64
	movq	%rax, %rbx
	jmp	.L1932
.L1942:
	endbr64
	movq	%rax, %rbx
	jmp	.L1930
.L1948:
	endbr64
	movq	%rax, %rbx
	jmp	.L1679
.L1949:
	endbr64
	movq	%rax, %rbx
	jmp	.L1682
.L1947:
	endbr64
	movq	%rax, %r12
	jmp	.L1676
.L1952:
	endbr64
	movq	%rax, %rbx
	jmp	.L1729
.L1946:
	endbr64
	movq	%rax, %rbx
	jmp	.L1724
.L1951:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r13
	jmp	.L1698
.L1950:
	endbr64
	movq	%rax, %rbx
	jmp	.L2243
.L1944:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L1700
.L1945:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L1933
.L1954:
	endbr64
	movq	%rax, %rdi
	jmp	.L1718
	.section	.gcc_except_table._ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE,"a",@progbits
	.align 4
.LLSDA7327:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT7327-.LLSDATTD7327
.LLSDATTD7327:
	.byte	0x1
	.uleb128 .LLSDACSE7327-.LLSDACSB7327
.LLSDACSB7327:
	.uleb128 .LEHB51-.LFB7327
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L1947-.LFB7327
	.uleb128 0
	.uleb128 .LEHB52-.LFB7327
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L1943-.LFB7327
	.uleb128 0
	.uleb128 .LEHB53-.LFB7327
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L1942-.LFB7327
	.uleb128 0
	.uleb128 .LEHB54-.LFB7327
	.uleb128 .LEHE54-.LEHB54
	.uleb128 .L1948-.LFB7327
	.uleb128 0
	.uleb128 .LEHB55-.LFB7327
	.uleb128 .LEHE55-.LEHB55
	.uleb128 .L1949-.LFB7327
	.uleb128 0
	.uleb128 .LEHB56-.LFB7327
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L1950-.LFB7327
	.uleb128 0
	.uleb128 .LEHB57-.LFB7327
	.uleb128 .LEHE57-.LEHB57
	.uleb128 .L1944-.LFB7327
	.uleb128 0x3
	.uleb128 .LEHB58-.LFB7327
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L1945-.LFB7327
	.uleb128 0x3
	.uleb128 .LEHB59-.LFB7327
	.uleb128 .LEHE59-.LEHB59
	.uleb128 .L1946-.LFB7327
	.uleb128 0
	.uleb128 .LEHB60-.LFB7327
	.uleb128 .LEHE60-.LEHB60
	.uleb128 .L1946-.LFB7327
	.uleb128 0
	.uleb128 .LEHB61-.LFB7327
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L1954-.LFB7327
	.uleb128 0x5
	.uleb128 .LEHB62-.LFB7327
	.uleb128 .LEHE62-.LEHB62
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB63-.LFB7327
	.uleb128 .LEHE63-.LEHB63
	.uleb128 .L1951-.LFB7327
	.uleb128 0x3
	.uleb128 .LEHB64-.LFB7327
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L1944-.LFB7327
	.uleb128 0x3
	.uleb128 .LEHB65-.LFB7327
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L1952-.LFB7327
	.uleb128 0
	.uleb128 .LEHB66-.LFB7327
	.uleb128 .LEHE66-.LEHB66
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB67-.LFB7327
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L1954-.LFB7327
	.uleb128 0x5
	.uleb128 .LEHB68-.LFB7327
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L1952-.LFB7327
	.uleb128 0
	.uleb128 .LEHB69-.LFB7327
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L1944-.LFB7327
	.uleb128 0x3
	.uleb128 .LEHB70-.LFB7327
	.uleb128 .LEHE70-.LEHB70
	.uleb128 .L1951-.LFB7327
	.uleb128 0x3
	.uleb128 .LEHB71-.LFB7327
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L1946-.LFB7327
	.uleb128 0
.LLSDACSE7327:
	.byte	0
	.byte	0
	.byte	0x1
	.byte	0x7d
	.byte	0x2
	.byte	0
	.align 4
	.long	0

	.long	DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE-.
.LLSDATT7327:
	.section	.text._ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC7327
	.type	_ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE.cold, @function
_ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE.cold:
.LFSB7327:
.L1679:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-432(%rbp), %rdi
	call	_ZN2v88internal6torque3AstD1Ev
.L1680:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	-472(%rbp), %rdx
	movq	%rdx, (%rax)
.L1932:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	movq	-136(%rbp), %rdx
	movq	-600(%rbp), %rdi
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque13SourceFileMapD1Ev
	movq	%rbx, %rdi
.LEHB72:
	call	_Unwind_Resume@PLT
.LEHE72:
.L1930:
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1932
	call	_ZdlPv@PLT
	jmp	.L1932
.L1682:
	leaq	-464(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED1Ev
.L1683:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	-336(%rbp), %rdx
	movq	-592(%rbp), %rdi
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque3AstD1Ev
	jmp	.L1680
.L1676:
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal6torque13SourceFileMapD1Ev
	movq	%r12, %rdi
.LEHB73:
	call	_Unwind_Resume@PLT
.LEHE73:
.L1729:
	movq	-576(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
.L1724:
	movq	%r15, %rdi
	call	_ZN2v88internal6torque20TorqueCompilerResultD1Ev
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
	movq	-208(%rbp), %rdx
	movq	%rdx, (%rax)
.L2243:
	movq	-528(%rbp), %rdi
	call	_ZN2v88internal6torque18LanguageServerDataD1Ev
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv@PLT
	movq	-440(%rbp), %rdx
	movq	-584(%rbp), %rdi
	movq	%rdx, (%rax)
	call	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED1Ev
	jmp	.L1683
.L1698:
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1699
	call	_ZdlPv@PLT
.L1699:
	movq	%r13, %r12
.L1700:
	subq	$1, %r12
	jne	.L1724
	movq	%rbx, %rdi
	call	__cxa_begin_catch@PLT
	call	__cxa_end_catch@PLT
	jmp	.L1935
.L1718:
	call	__cxa_begin_catch@PLT
	movq	-568(%rbp), %r12
.L1721:
	cmpq	%rbx, %r12
	jne	.L2262
.LEHB74:
	call	__cxa_rethrow@PLT
.LEHE74:
.L1933:
	movq	%r14, %rdi
	call	_ZN2v88internal6torque21TorqueCompilerOptionsD1Ev
	jmp	.L1700
.L2262:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1720
	call	_ZdlPv@PLT
.L1720:
	addq	$32, %r12
	jmp	.L1721
.L1953:
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1724
	call	_ZdlPv@PLT
	jmp	.L1724
	.cfi_endproc
.LFE7327:
	.section	.gcc_except_table._ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
	.align 4
.LLSDAC7327:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC7327-.LLSDATTDC7327
.LLSDATTDC7327:
	.byte	0x1
	.uleb128 .LLSDACSEC7327-.LLSDACSBC7327
.LLSDACSBC7327:
	.uleb128 .LEHB72-.LCOLDB6
	.uleb128 .LEHE72-.LEHB72
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB73-.LCOLDB6
	.uleb128 .LEHE73-.LEHB73
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB74-.LCOLDB6
	.uleb128 .LEHE74-.LEHB74
	.uleb128 .L1953-.LCOLDB6
	.uleb128 0
.LLSDACSEC7327:
	.byte	0
	.byte	0
	.byte	0x1
	.byte	0x7d
	.byte	0x2
	.byte	0
	.align 4
	.long	0

	.long	DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE-.
.LLSDATTC7327:
	.section	.text.unlikely._ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
	.section	.text._ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
	.size	_ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE, .-_ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
	.section	.text.unlikely._ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
	.size	_ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE.cold, .-_ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE.cold
.LCOLDE6:
	.section	.text._ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
.LHOTE6:
	.section	.rodata._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"Cannot open file path/uri: "
	.section	.text.unlikely._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE,"ax",@progbits
.LCOLDB8:
	.section	.text._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE,"ax",@progbits
.LHOTB8:
	.p2align 4
	.globl	_ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE
	.type	_ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE, @function
_ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE:
.LFB7448:
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDA7448
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-432(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1064, %rsp
	movq	32(%rax), %r15
	movq	40(%rax), %r14
	movq	%rdi, -976(%rbp)
	movq	%rdx, -984(%rbp)
	movq	%r15, %rax
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	addq	%r14, %rax
	movq	%r13, -448(%rbp)
	je	.L2264
	testq	%r15, %r15
	je	.L2920
.L2264:
	movq	%r14, -784(%rbp)
	cmpq	$15, %r14
	ja	.L2921
	cmpq	$1, %r14
	jne	.L2267
	movzbl	(%r15), %eax
	movb	%al, -432(%rbp)
	movq	%r13, %rax
.L2268:
	movq	%r14, -440(%rbp)
	pxor	%xmm0, %xmm0
	movb	$0, (%rax,%r14)
	leaq	-536(%rbp), %rax
	movq	%rax, -1048(%rbp)
	movq	%rax, -552(%rbp)
	movq	-448(%rbp), %rax
	movq	$0, -560(%rbp)
	movaps	%xmm0, -576(%rbp)
	cmpq	%r13, %rax
	je	.L2922
	movq	%rax, -552(%rbp)
	movq	-432(%rbp), %rax
	movq	%rax, -536(%rbp)
.L2270:
	movq	-440(%rbp), %rax
	movq	%rax, -544(%rbp)
.LEHB75:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
.LEHE75:
	leaq	-576(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	%rdx, -1072(%rbp)
	movl	$-1, -960(%rbp)
.LEHB76:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -952(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
.LEHE76:
	leaq	-960(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdx, (%rax)
	leaq	-840(%rbp), %rax
	movl	$0, -840(%rbp)
	movq	$0, -832(%rbp)
	movq	%rax, -824(%rbp)
	movq	%rax, -816(%rbp)
	movq	$0, -808(%rbp)
	movaps	%xmm0, -896(%rbp)
	movaps	%xmm0, -880(%rbp)
	movaps	%xmm0, -864(%rbp)
.LEHB77:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -800(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
.LEHE77:
	leaq	-896(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdx, (%rax)
	movq	%rdx, -1064(%rbp)
	movq	$0, -912(%rbp)
	movaps	%xmm0, -928(%rbp)
.LEHB78:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -904(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv@PLT
.LEHE78:
	leaq	-928(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rax)
	leaq	-776(%rbp), %rax
	movq	%rax, -760(%rbp)
	movq	%rax, -752(%rbp)
	leaq	-728(%rbp), %rax
	movq	%rcx, -1008(%rbp)
	movl	$0, -776(%rbp)
	movq	$0, -768(%rbp)
	movq	$0, -744(%rbp)
	movl	$0, -728(%rbp)
	movq	$0, -720(%rbp)
	movq	%rax, -712(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -696(%rbp)
	movaps	%xmm0, -688(%rbp)
.LEHB79:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
.LEHE79:
	leaq	-784(%rbp), %rdx
	pxor	%xmm0, %xmm0
	leaq	-448(%rbp), %r14
	movq	%rdx, (%rax)
	movq	-976(%rbp), %rax
	movq	%rdx, -1056(%rbp)
	leaq	72(%rax), %rcx
	movb	$0, (%rax)
	movq	%rcx, 88(%rax)
	movq	%rcx, 96(%rax)
	movq	%rcx, -1040(%rbp)
	leaq	120(%rax), %rcx
	movb	$0, 8(%rax)
	movl	$0, 72(%rax)
	movq	$0, 80(%rax)
	movq	$0, 104(%rax)
	movl	$0, 120(%rax)
	movq	$0, 128(%rax)
	movq	%rcx, 136(%rax)
	movq	%rcx, 144(%rax)
	movq	$0, 152(%rax)
	movq	$0, 192(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	8(%rbx), %rax
	movq	(%rbx), %rbx
	movq	%rcx, -1000(%rbp)
	movq	%rax, -968(%rbp)
	cmpq	%rax, %rbx
	jne	.L2327
	jmp	.L2280
	.p2align 4,,10
	.p2align 3
.L2290:
	leaq	-616(%rbp), %rdi
.LEHB80:
	call	_ZN2v88internal6torque11ParseTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE80:
	cmpb	$0, -624(%rbp)
	je	.L2326
	movq	-616(%rbp), %rdi
	leaq	-600(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2326
	call	_ZdlPv@PLT
.L2326:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	-936(%rbp), %rdx
	addq	$32, %rbx
	movq	%rdx, (%rax)
	cmpq	%rbx, -968(%rbp)
	je	.L2280
.L2327:
	movq	(%rbx), %r15
	movq	8(%rbx), %r12
	movq	%r13, -448(%rbp)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L2283
	testq	%r15, %r15
	je	.L2923
.L2283:
	movq	%r12, -944(%rbp)
	cmpq	$15, %r12
	ja	.L2924
	cmpq	$1, %r12
	jne	.L2286
	movzbl	(%r15), %eax
	movb	%al, -432(%rbp)
	movq	%r13, %rax
.L2287:
	movq	%r12, -440(%rbp)
	movq	%r14, %rdi
	movb	$0, (%rax,%r12)
.LEHB81:
	call	_ZN2v88internal6torque13SourceFileMap9AddSourceENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE81:
	movq	-448(%rbp), %rdi
	movl	%eax, %r12d
	cmpq	%r13, %rdi
	je	.L2288
	call	_ZdlPv@PLT
.L2288:
	movl	%r12d, -944(%rbp)
.LEHB82:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rax
	movq	%rax, -936(%rbp)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
.LEHE82:
	leaq	-944(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r14, %rdi
	movq	%rdx, (%rax)
.LEHB83:
	call	_ZN2v88internal6torque13SourceFileMap12AbsolutePathB5cxx11ENS1_8SourceIdE@PLT
.LEHE83:
	leaq	-624(%rbp), %rdi
	movq	%r14, %rsi
.LEHB84:
	call	_ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LEHE84:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2289
	call	_ZdlPv@PLT
.L2289:
	cmpb	$0, -624(%rbp)
	jne	.L2290
	leaq	-512(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r15, %rdi
.LEHB85:
	call	_ZN2v88internal6torque13FileUriDecodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.LEHE85:
	cmpb	$0, -512(%rbp)
	jne	.L2925
.L2292:
	cmpb	$0, -624(%rbp)
	jne	.L2290
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1080(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	xorl	%eax, %eax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rcx, -320(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, -968(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	$0, -104(%rbp)
	movq	%rcx, -448(%rbp)
	movw	%ax, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rcx), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdx
	addq	%r14, %rdx
	movq	%rdx, %rdi
.LEHB86:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE86:
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rcx, -432(%rbp)
	movq	-24(%rcx), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rcx
	addq	%r13, %rcx
	movq	%rcx, %rdi
.LEHB87:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE87:
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	%rdx, %xmm0
	movq	%rdx, -1096(%rbp)
	movq	%rcx, -448(%rbp)
	movq	-24(%rcx), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rcx, -448(%rbp,%rax)
	leaq	-40(%rdx), %rcx
	movq	%rcx, -984(%rbp)
	movq	%rcx, -448(%rbp)
	leaq	40(%rdx), %rcx
	leaq	-368(%rbp), %rdx
	movq	%rcx, -992(%rbp)
	movq	%rdx, %rdi
	movq	%rcx, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rcx
	movq	%rcx, -1024(%rbp)
	movhps	-1024(%rbp), %xmm0
	movq	%rdx, -1024(%rbp)
	movaps	%xmm0, -432(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	movq	-1080(%rbp), %rdi
	leaq	-336(%rbp), %rdx
	leaq	-424(%rbp), %rsi
	movq	%r12, -424(%rbp)
	movl	$24, -360(%rbp)
	movq	%rdx, -1088(%rbp)
	movq	%rdx, -352(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
.LEHB88:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
.LEHE88:
	movl	$27, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
.LEHB89:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.LEHE89:
	movq	-384(%rbp), %rax
	leaq	-640(%rbp), %rbx
	movq	$0, -648(%rbp)
	leaq	-656(%rbp), %r13
	movq	%rbx, -656(%rbp)
	movb	$0, -640(%rbp)
	testq	%rax, %rax
	je	.L2315
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L2316
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
.LEHB90:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.LEHE90:
.L2317:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
.LEHB91:
	call	_ZN2v88internal6torque14MessageBuilderC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_13TorqueMessage4KindE@PLT
.LEHE91:
	movq	-656(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2322
	call	_ZdlPv@PLT
.L2322:
	movq	-984(%rbp), %rax
	movq	%r12, %xmm5
	movq	-1096(%rbp), %xmm0
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-992(%rbp), %rax
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -432(%rbp)
	movq	%rax, -320(%rbp)
	cmpq	-1088(%rbp), %rdi
	je	.L2323
	call	_ZdlPv@PLT
.L2323:
	movq	-1024(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1080(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-968(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r15, %rdi
.LEHB92:
	call	_ZNK2v88internal6torque14MessageBuilder5ThrowEv@PLT
.LEHE92:
	.p2align 4,,10
	.p2align 3
.L2267:
	testq	%r14, %r14
	jne	.L2926
	movq	%r13, %rax
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2286:
	testq	%r12, %r12
	jne	.L2927
	movq	%r13, %rax
	jmp	.L2287
	.p2align 4,,10
	.p2align 3
.L2924:
	leaq	-944(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
.LEHB93:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE93:
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-944(%rbp), %rax
	movq	%rax, -432(%rbp)
.L2285:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-944(%rbp), %r12
	movq	-448(%rbp), %rax
	jmp	.L2287
	.p2align 4,,10
	.p2align 3
.L2280:
	movq	-984(%rbp), %rax
	movq	%r13, -448(%rbp)
	movq	(%rax), %r14
	movq	8(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L2339
	testq	%r14, %r14
	je	.L2928
.L2339:
	movq	%r12, -944(%rbp)
	cmpq	$15, %r12
	ja	.L2929
	cmpq	$1, %r12
	jne	.L2342
	movzbl	(%r14), %eax
	movb	%al, -432(%rbp)
	movq	%r13, %rax
.L2343:
	movq	%r12, -440(%rbp)
	leaq	-400(%rbp), %rbx
	movb	$0, (%rax,%r12)
	movq	-984(%rbp), %rax
	movq	%rbx, -416(%rbp)
	movq	32(%rax), %r14
	movq	40(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L2344
	testq	%r14, %r14
	je	.L2930
.L2344:
	movq	%r12, -944(%rbp)
	cmpq	$15, %r12
	ja	.L2931
	cmpq	$1, %r12
	jne	.L2347
	movzbl	(%r14), %eax
	movb	%al, -400(%rbp)
	movq	%rbx, %rax
.L2348:
	movq	-984(%rbp), %rcx
	movq	%r12, -408(%rbp)
	movb	$0, (%rax,%r12)
	leaq	-448(%rbp), %r12
	movzbl	66(%rcx), %eax
	movzwl	64(%rcx), %edx
	movq	%r12, %rdi
	movw	%dx, -384(%rbp)
	movb	%al, -382(%rbp)
.LEHB94:
	call	_ZN2v88internal6torque12_GLOBAL__N_117CompileCurrentAstENS1_21TorqueCompilerOptionsE
.LEHE94:
	movq	-416(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2352
	call	_ZdlPv@PLT
.L2352:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2583
	call	_ZdlPv@PLT
.L2583:
.LEHB95:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	movq	-976(%rbp), %rbx
	movq	(%rax), %r15
	leaq	8(%rbx), %rdi
	cmpb	$0, (%rbx)
	movq	%rbx, %rax
	movq	%rdi, -1024(%rbp)
	je	.L2354
	movq	%r15, %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	leaq	24(%r15), %rsi
	leaq	32(%rbx), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.L2355:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
	movq	(%rax), %rbx
	movq	-976(%rbp), %rax
	movq	80(%rax), %r13
	leaq	64(%rax), %r12
	testq	%r13, %r13
	je	.L2385
.L2381:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r13), %rdi
	movq	16(%r13), %r14
	testq	%rdi, %rdi
	je	.L2384
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L2385
.L2386:
	movq	%r14, %r13
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2384:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L2386
.L2385:
	movq	-976(%rbp), %rax
	movq	-1040(%rbp), %rdx
	movq	$0, 80(%rax)
	movq	%rdx, 88(%rax)
	movq	%rdx, 96(%rax)
	movq	$0, 104(%rax)
	cmpq	$0, 16(%rbx)
	je	.L2383
	movq	%rax, %rcx
	movl	8(%rbx), %eax
	movl	%eax, 72(%rcx)
	movq	16(%rbx), %rax
	movq	%rax, 80(%rcx)
	movq	24(%rbx), %rdx
	movq	%rdx, 88(%rcx)
	movq	32(%rbx), %rdx
	movq	%rdx, 96(%rcx)
	movq	-1040(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	40(%rbx), %rax
	movq	%rax, 104(%rcx)
	leaq	8(%rbx), %rax
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	%rax, 24(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rcx, %rax
.L2383:
	movq	128(%rax), %r13
	leaq	112(%rax), %r12
	testq	%r13, %r13
	je	.L2391
.L2387:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r13), %rdi
	movq	16(%r13), %r14
	testq	%rdi, %rdi
	je	.L2390
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L2391
.L2392:
	movq	%r14, %r13
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2390:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L2392
.L2391:
	movq	-976(%rbp), %rax
	movq	-1000(%rbp), %rcx
	movq	$0, 128(%rax)
	movq	%rax, %rdx
	movq	%rcx, 136(%rax)
	movq	%rcx, 144(%rax)
	movq	$0, 152(%rax)
	cmpq	$0, 64(%rbx)
	je	.L2389
	movq	%rax, %rcx
	movl	56(%rbx), %eax
	movl	%eax, 120(%rdx)
	movq	64(%rbx), %rax
	movq	%rax, 128(%rdx)
	movq	72(%rbx), %rdx
	movq	%rdx, 136(%rcx)
	movq	80(%rbx), %rdx
	movq	%rdx, 144(%rcx)
	movq	-1000(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	88(%rbx), %rax
	movq	%rcx, %rdx
	movq	%rax, 152(%rcx)
	leaq	56(%rbx), %rax
	movq	$0, 64(%rbx)
	movq	%rax, 72(%rbx)
	movq	%rax, 80(%rbx)
	movq	$0, 88(%rbx)
.L2389:
	movq	96(%rbx), %rax
	movq	$0, 96(%rbx)
	movq	160(%rdx), %rcx
	movq	%rax, 160(%rdx)
	movq	%rcx, -1000(%rbp)
	testq	%rcx, %rcx
	je	.L2393
	movq	208(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L2394
	call	_ZdlPv@PLT
.L2394:
	movq	-1000(%rbp), %rax
	movq	176(%rax), %r13
	leaq	160(%rax), %rcx
	movq	%rcx, -1040(%rbp)
	testq	%r13, %r13
	je	.L2400
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	%rax, -968(%rbp)
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %xmm7
	leaq	40(%rax), %rdx
	leaq	-40(%rax), %rcx
	movq	%rax, %xmm5
	movq	%rbx, -1080(%rbp)
	movq	%rdx, -992(%rbp)
	punpcklqdq	%xmm7, %xmm5
	movq	%rcx, -984(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rcx
	movaps	%xmm5, -1024(%rbp)
	movq	%rcx, %rbx
.L2401:
	movq	24(%r13), %rsi
	movq	-1040(%rbp), %rdi
	movq	%r13, %r12
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	-984(%rbp), %rax
	movq	16(%r13), %r13
	movdqa	-1024(%rbp), %xmm1
	movq	528(%r12), %rdi
	movq	%rax, 432(%r12)
	movq	-992(%rbp), %rax
	movups	%xmm1, 448(%r12)
	movq	%rax, 560(%r12)
	leaq	544(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2398
	call	_ZdlPv@PLT
.L2398:
	movq	%rbx, 456(%r12)
	leaq	512(%r12), %rdi
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, 432(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	560(%r12), %rdi
	movq	-24(%r15), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, 448(%r12)
	movq	-24(%r14), %rax
	movq	%rcx, 448(%r12,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 432(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	-968(%rbp), %rax
	movq	$0, 440(%r12)
	movq	%rax, 560(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-984(%rbp), %rax
	movdqa	-1024(%rbp), %xmm2
	movq	136(%r12), %rdi
	movq	%rax, 40(%r12)
	movq	-992(%rbp), %rax
	movups	%xmm2, 56(%r12)
	movq	%rax, 168(%r12)
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2399
	call	_ZdlPv@PLT
.L2399:
	movq	%rbx, 64(%r12)
	leaq	120(%r12), %rdi
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, 40(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	168(%r12), %rdi
	movq	-24(%r15), %rax
	movq	%rcx, 40(%r12,%rax)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, 56(%r12)
	movq	-24(%r14), %rax
	movq	%rcx, 56(%r12,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, 40(%r12)
	movq	-24(%rax), %rax
	movq	%rcx, 40(%r12,%rax)
	movq	-968(%rbp), %rax
	movq	$0, 48(%r12)
	movq	%rax, 168(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L2401
	movq	-1080(%rbp), %rbx
.L2400:
	movq	-1000(%rbp), %rax
	movq	144(%rax), %r12
	movq	136(%rax), %r13
	cmpq	%r13, %r12
	je	.L2396
	.p2align 4,,10
	.p2align 3
.L2397:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2402
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r12, %r13
	jne	.L2397
.L2403:
	movq	-1000(%rbp), %rax
	movq	136(%rax), %r13
.L2396:
	testq	%r13, %r13
	je	.L2405
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2405:
	movq	-1000(%rbp), %rax
	movq	120(%rax), %r12
	movq	112(%rax), %r13
	cmpq	%r13, %r12
	je	.L2406
	.p2align 4,,10
	.p2align 3
.L2410:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2407
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L2410
.L2408:
	movq	-1000(%rbp), %rax
	movq	112(%rax), %r13
.L2406:
	testq	%r13, %r13
	je	.L2411
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2411:
	movq	-1000(%rbp), %rax
	movq	80(%rax), %r12
	leaq	64(%rax), %rdx
	testq	%r12, %r12
	je	.L2416
	movq	%rbx, -968(%rbp)
	movq	%rdx, %r15
.L2412:
	movq	24(%r12), %rsi
	movq	%r12, %r14
	movq	%r15, %rdi
	leaq	40(%r14), %r13
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r14), %rbx
	movq	16(%r12), %r12
	testq	%rbx, %rbx
	je	.L2417
.L2415:
	movq	24(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2415
.L2417:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L2412
	movq	-968(%rbp), %rbx
.L2416:
	movq	-1000(%rbp), %rax
	movq	48(%rax), %r12
	movq	40(%rax), %r13
	cmpq	%r13, %r12
	je	.L2413
	.p2align 4,,10
	.p2align 3
.L2414:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2418
	movq	(%rdi), %rax
	addq	$8, %r13
	call	*8(%rax)
	cmpq	%r12, %r13
	jne	.L2414
.L2419:
	movq	-1000(%rbp), %rax
	movq	40(%rax), %r13
.L2413:
	testq	%r13, %r13
	je	.L2421
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2421:
	movq	-1000(%rbp), %rax
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2422
	call	_ZdlPv@PLT
.L2422:
	movq	-1000(%rbp), %rdi
	movl	$240, %esi
	call	_ZdlPvm@PLT
.L2393:
	movq	-976(%rbp), %rcx
	movq	104(%rbx), %rax
	movq	$0, 104(%rbx)
	movq	168(%rcx), %r14
	movq	%rax, 168(%rcx)
	testq	%r14, %r14
	je	.L2423
	movq	216(%r14), %rbx
	movq	208(%r14), %r12
	cmpq	%r12, %rbx
	je	.L2424
	.p2align 4,,10
	.p2align 3
.L2428:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2425
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L2428
.L2426:
	movq	208(%r14), %r12
.L2424:
	testq	%r12, %r12
	je	.L2429
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2429:
	movq	192(%r14), %rbx
	movq	184(%r14), %r12
	cmpq	%r12, %rbx
	je	.L2430
	.p2align 4,,10
	.p2align 3
.L2434:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2431
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L2434
.L2432:
	movq	184(%r14), %r12
.L2430:
	testq	%r12, %r12
	je	.L2435
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2435:
	movq	168(%r14), %rbx
	movq	160(%r14), %r12
	cmpq	%r12, %rbx
	je	.L2436
	.p2align 4,,10
	.p2align 3
.L2440:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2437
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L2440
.L2438:
	movq	160(%r14), %r12
.L2436:
	testq	%r12, %r12
	je	.L2441
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2441:
	movq	144(%r14), %rbx
	movq	136(%r14), %r12
	cmpq	%r12, %rbx
	je	.L2442
	.p2align 4,,10
	.p2align 3
.L2446:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2443
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L2446
.L2444:
	movq	136(%r14), %r12
.L2442:
	testq	%r12, %r12
	je	.L2447
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2447:
	movq	96(%r14), %rax
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rcx
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rdx
	movq	%rcx, -984(%rbp)
	movq	%rdx, -968(%rbp)
	testq	%rax, %rax
	je	.L2455
	movq	%r14, -992(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2448:
	movq	%rbx, %r12
	movq	-984(%rbp), %rax
	movq	(%rbx), %rbx
	movq	96(%r12), %r13
	leaq	80(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r13, %r13
	je	.L2454
.L2451:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L2451
.L2454:
	movq	-968(%rbp), %rax
	movq	48(%r12), %r14
	leaq	32(%r12), %r13
	movq	%rax, 8(%r12)
	testq	%r14, %r14
	je	.L2452
.L2453:
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r14), %rdi
	leaq	48(%r14), %rax
	movq	16(%r14), %r15
	cmpq	%rax, %rdi
	je	.L2456
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L2452
.L2457:
	movq	%r15, %r14
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2456:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L2457
.L2452:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2448
	movq	-992(%rbp), %r14
.L2455:
	movq	88(%r14), %rax
	movq	80(%r14), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%r14), %rdi
	leaq	128(%r14), %rax
	movq	$0, 104(%r14)
	movq	$0, 96(%r14)
	cmpq	%rax, %rdi
	je	.L2449
	call	_ZdlPv@PLT
.L2449:
	movq	56(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2458
	call	_ZdlPv@PLT
.L2458:
	movq	16(%r14), %rax
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rcx
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rdx
	movq	%rcx, -968(%rbp)
	movq	%rdx, -984(%rbp)
	testq	%rax, %rax
	je	.L2464
	movq	%r14, -992(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2459:
	movq	%rbx, %r12
	movq	-984(%rbp), %rax
	movq	(%rbx), %rbx
	movq	80(%r12), %rdi
	movq	%rax, 8(%r12)
	testq	%rdi, %rdi
	je	.L2462
	call	_ZdlPv@PLT
.L2462:
	movq	-968(%rbp), %rax
	movq	48(%r12), %r14
	leaq	32(%r12), %r13
	movq	%rax, 8(%r12)
	testq	%r14, %r14
	je	.L2466
.L2463:
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r14), %rdi
	leaq	48(%r14), %rax
	movq	16(%r14), %r15
	cmpq	%rax, %rdi
	je	.L2465
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L2466
.L2467:
	movq	%r15, %r14
	jmp	.L2463
	.p2align 4,,10
	.p2align 3
.L2465:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L2467
.L2466:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2459
	movq	-992(%rbp), %r14
.L2464:
	movq	8(%r14), %rax
	movq	(%r14), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r14), %rdi
	leaq	48(%r14), %rax
	movq	$0, 24(%r14)
	movq	$0, 16(%r14)
	cmpq	%rax, %rdi
	je	.L2460
	call	_ZdlPv@PLT
.L2460:
	movl	$232, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2423:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv@PLT
.LEHE95:
	movq	(%rax), %rax
	movq	-976(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	(%rax), %rdx
	movq	176(%rcx), %r12
	movq	184(%rcx), %rbx
	movq	%rdx, 176(%rcx)
	movq	8(%rax), %rdx
	movq	%r12, %r13
	movq	%rdx, 184(%rcx)
	movq	16(%rax), %rdx
	movq	%rdx, 192(%rcx)
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	%rbx, %r12
	je	.L2472
	.p2align 4,,10
	.p2align 3
.L2468:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2471
	call	_ZdlPv@PLT
	addq	$64, %r13
	cmpq	%rbx, %r13
	jne	.L2468
.L2472:
	testq	%r12, %r12
	je	.L2470
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2470:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
	movq	-672(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-680(%rbp), %r13
	testq	%r13, %r13
	je	.L2474
	movq	216(%r13), %rbx
	movq	208(%r13), %r12
	cmpq	%r12, %rbx
	je	.L2475
	.p2align 4,,10
	.p2align 3
.L2479:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2476
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L2479
.L2477:
	movq	208(%r13), %r12
.L2475:
	testq	%r12, %r12
	je	.L2480
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2480:
	movq	192(%r13), %rbx
	movq	184(%r13), %r12
	cmpq	%r12, %rbx
	je	.L2481
	.p2align 4,,10
	.p2align 3
.L2485:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2482
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L2485
.L2483:
	movq	184(%r13), %r12
.L2481:
	testq	%r12, %r12
	je	.L2486
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2486:
	movq	168(%r13), %rbx
	movq	160(%r13), %r12
	cmpq	%r12, %rbx
	je	.L2487
	.p2align 4,,10
	.p2align 3
.L2491:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2488
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L2491
.L2489:
	movq	160(%r13), %r12
.L2487:
	testq	%r12, %r12
	je	.L2492
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2492:
	movq	144(%r13), %rbx
	movq	136(%r13), %r12
	cmpq	%r12, %rbx
	je	.L2493
	.p2align 4,,10
	.p2align 3
.L2497:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2494
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L2497
.L2495:
	movq	136(%r13), %r12
.L2493:
	testq	%r12, %r12
	je	.L2498
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2498:
	movq	96(%r13), %rax
	leaq	16+_ZTVN2v88internal6torque9UnionTypeE(%rip), %rcx
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rdx
	movq	%rcx, -984(%rbp)
	movq	%rdx, -968(%rbp)
	testq	%rax, %rax
	je	.L2506
	movq	%r13, -992(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2499:
	movq	%rbx, %r12
	movq	-984(%rbp), %rax
	movq	(%rbx), %rbx
	movq	96(%r12), %r13
	leaq	80(%r12), %r14
	movq	%rax, 8(%r12)
	testq	%r13, %r13
	je	.L2505
.L2502:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPKN2v88internal6torque4TypeES5_St9_IdentityIS5_ENS2_8TypeLessESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L2502
.L2505:
	movq	-968(%rbp), %rax
	movq	48(%r12), %r14
	leaq	32(%r12), %r13
	movq	%rax, 8(%r12)
	testq	%r14, %r14
	je	.L2503
.L2504:
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r14), %rdi
	leaq	48(%r14), %rdx
	movq	16(%r14), %r15
	cmpq	%rdx, %rdi
	je	.L2507
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L2503
.L2508:
	movq	%r15, %r14
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2507:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L2508
.L2503:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2499
	movq	-992(%rbp), %r13
.L2506:
	movq	88(%r13), %rax
	movq	80(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	80(%r13), %rdi
	leaq	128(%r13), %rax
	movq	$0, 104(%r13)
	movq	$0, 96(%r13)
	cmpq	%rax, %rdi
	je	.L2500
	call	_ZdlPv@PLT
.L2500:
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2509
	call	_ZdlPv@PLT
.L2509:
	movq	16(%r13), %rax
	leaq	16+_ZTVN2v88internal6torque4TypeE(%rip), %rcx
	leaq	16+_ZTVN2v88internal6torque18BuiltinPointerTypeE(%rip), %rdx
	movq	%rcx, -968(%rbp)
	movq	%rdx, -984(%rbp)
	testq	%rax, %rax
	je	.L2515
	movq	%r13, -992(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2510:
	movq	%rbx, %r12
	movq	-984(%rbp), %rax
	movq	(%rbx), %rbx
	movq	80(%r12), %rdi
	movq	%rax, 8(%r12)
	testq	%rdi, %rdi
	je	.L2513
	call	_ZdlPv@PLT
.L2513:
	movq	-968(%rbp), %rax
	movq	48(%r12), %r14
	leaq	32(%r12), %r13
	movq	%rax, 8(%r12)
	testq	%r14, %r14
	je	.L2517
.L2514:
	movq	24(%r14), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r14), %rdi
	leaq	48(%r14), %rdx
	movq	16(%r14), %r15
	cmpq	%rdx, %rdi
	je	.L2516
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L2517
.L2518:
	movq	%r15, %r14
	jmp	.L2514
	.p2align 4,,10
	.p2align 3
.L2516:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L2518
.L2517:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2510
	movq	-992(%rbp), %r13
.L2515:
	movq	8(%r13), %rax
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	0(%r13), %rdi
	leaq	48(%r13), %rax
	movq	$0, 24(%r13)
	movq	$0, 16(%r13)
	cmpq	%rax, %rdi
	je	.L2511
	call	_ZdlPv@PLT
.L2511:
	movl	$232, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2474:
	movq	-688(%rbp), %rax
	movq	%rax, -1000(%rbp)
	testq	%rax, %rax
	je	.L2519
	movq	208(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2520
	call	_ZdlPv@PLT
.L2520:
	movq	-1000(%rbp), %rax
	movq	176(%rax), %rbx
	leaq	160(%rax), %rcx
	movq	%rcx, -1008(%rbp)
	testq	%rbx, %rbx
	je	.L2526
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rcx
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r15
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	movq	%rax, -968(%rbp)
	leaq	64+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	leaq	-40(%rax), %rdx
	movq	%rax, %xmm6
	movq	%r12, %xmm7
	movq	%rcx, -1024(%rbp)
	movq	%rdx, -984(%rbp)
	leaq	40(%rax), %rdx
	punpcklqdq	%xmm7, %xmm6
	movq	%rdx, -992(%rbp)
	movaps	%xmm6, -1040(%rbp)
.L2527:
	movq	24(%rbx), %rsi
	movq	-1008(%rbp), %rdi
	movq	%rbx, %r12
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_NS2_13GlobalContext14PerFileStreamsEESt10_Select1stIS8_ESt4lessIS3_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	-984(%rbp), %rax
	movq	16(%rbx), %rbx
	movdqa	-1040(%rbp), %xmm3
	movq	528(%r12), %rdi
	movq	%rax, 432(%r12)
	movq	-992(%rbp), %rax
	movups	%xmm3, 448(%r12)
	movq	%rax, 560(%r12)
	leaq	544(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2524
	call	_ZdlPv@PLT
.L2524:
	movq	-1024(%rbp), %rax
	leaq	512(%r12), %rdi
	movq	%rax, 456(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, 432(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	560(%r12), %rdi
	movq	-24(%r15), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, 448(%r12)
	movq	-24(%r14), %rax
	movq	%rcx, 448(%r12,%rax)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, 432(%r12)
	movq	-24(%r13), %rax
	movq	%rcx, 432(%r12,%rax)
	movq	-968(%rbp), %rax
	movq	$0, 440(%r12)
	movq	%rax, 560(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-984(%rbp), %rax
	movdqa	-1040(%rbp), %xmm4
	movq	136(%r12), %rdi
	movq	%rax, 40(%r12)
	movq	-992(%rbp), %rax
	movups	%xmm4, 56(%r12)
	movq	%rax, 168(%r12)
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2525
	call	_ZdlPv@PLT
.L2525:
	movq	-1024(%rbp), %rax
	leaq	120(%r12), %rdi
	movq	%rax, 64(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, 40(%r12)
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	leaq	168(%r12), %rdi
	movq	-24(%r15), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rcx, 40(%r12,%rax)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, 56(%r12)
	movq	-24(%r14), %rax
	movq	%rcx, 56(%r12,%rax)
	movq	%r13, 40(%r12)
	movq	-24(%r13), %rax
	movq	%rdx, 40(%r12,%rax)
	movq	-968(%rbp), %rax
	movq	$0, 48(%r12)
	movq	%rax, 168(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2527
.L2526:
	movq	-1000(%rbp), %rax
	movq	144(%rax), %rbx
	movq	136(%rax), %r12
	cmpq	%r12, %rbx
	je	.L2522
	.p2align 4,,10
	.p2align 3
.L2523:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2528
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2523
.L2529:
	movq	-1000(%rbp), %rax
	movq	136(%rax), %r12
.L2522:
	testq	%r12, %r12
	je	.L2531
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2531:
	movq	-1000(%rbp), %rax
	movq	120(%rax), %rbx
	movq	112(%rax), %r12
	cmpq	%r12, %rbx
	je	.L2532
	.p2align 4,,10
	.p2align 3
.L2536:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2533
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L2536
.L2534:
	movq	-1000(%rbp), %rax
	movq	112(%rax), %r12
.L2532:
	testq	%r12, %r12
	je	.L2537
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2537:
	movq	-1000(%rbp), %rax
	movq	80(%rax), %rbx
	leaq	64(%rax), %r14
	testq	%rbx, %rbx
	je	.L2542
.L2538:
	movq	24(%rbx), %rsi
	movq	%rbx, %r15
	movq	%r14, %rdi
	leaq	40(%r15), %r13
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r15), %r12
	movq	16(%rbx), %rbx
	testq	%r12, %r12
	je	.L2543
.L2541:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L2541
.L2543:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2538
.L2542:
	movq	-1000(%rbp), %rax
	movq	48(%rax), %rbx
	movq	40(%rax), %r12
	cmpq	%r12, %rbx
	je	.L2539
	.p2align 4,,10
	.p2align 3
.L2540:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2544
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%r12, %rbx
	jne	.L2540
.L2545:
	movq	-1000(%rbp), %rax
	movq	40(%rax), %r12
.L2539:
	testq	%r12, %r12
	je	.L2547
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2547:
	movq	-1000(%rbp), %rax
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2548
	call	_ZdlPv@PLT
.L2548:
	movq	-1000(%rbp), %rdi
	movl	$240, %esi
	call	_ZdlPvm@PLT
.L2519:
	movq	-720(%rbp), %r12
	leaq	-736(%rbp), %rbx
	testq	%r12, %r12
	je	.L2553
.L2549:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIPNS2_10DeclarableESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %r13
	testq	%rdi, %rdi
	je	.L2552
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L2553
.L2554:
	movq	%r13, %r12
	jmp	.L2549
	.p2align 4,,10
	.p2align 3
.L2471:
	addq	$64, %r13
	cmpq	%r13, %rbx
	jne	.L2468
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2528:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2523
	jmp	.L2529
	.p2align 4,,10
	.p2align 3
.L2402:
	addq	$32, %r13
	cmpq	%r13, %r12
	jne	.L2397
	jmp	.L2403
	.p2align 4,,10
	.p2align 3
.L2552:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L2554
.L2553:
	movq	-768(%rbp), %r12
	movq	-1056(%rbp), %r13
	testq	%r12, %r12
	je	.L2550
.L2551:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St6vectorIS4_INS2_14SourcePositionES7_ESaIS8_EEESt10_Select1stISB_ESt4lessIS3_ESaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L2557
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2550
.L2558:
	movq	%rbx, %r12
	jmp	.L2551
	.p2align 4,,10
	.p2align 3
.L2557:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2558
.L2550:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv@PLT
	movq	-904(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-920(%rbp), %rbx
	movq	-928(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2555
	.p2align 4,,10
	.p2align 3
.L2556:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2559
	call	_ZdlPv@PLT
	addq	$64, %r12
	cmpq	%r12, %rbx
	jne	.L2556
.L2560:
	movq	-928(%rbp), %r12
.L2555:
	testq	%r12, %r12
	je	.L2562
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2562:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	-800(%rbp), %rdx
	leaq	-848(%rbp), %r13
	movq	%rdx, (%rax)
	movq	-832(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2567
.L2563:
	movq	24(%rbx), %rsi
	movq	%rbx, %r14
	movq	%r13, %rdi
	leaq	40(%r14), %r15
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdESt4pairIKS3_St3setIS3_St4lessIS3_ESaIS3_EEESt10_Select1stISB_ES8_SaISB_EE8_M_eraseEPSt13_Rb_tree_nodeISB_E
	movq	56(%r14), %r12
	movq	16(%rbx), %rbx
	testq	%r12, %r12
	je	.L2568
.L2566:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeIN2v88internal6torque8SourceIdES3_St9_IdentityIS3_ESt4lessIS3_ESaIS3_EE8_M_eraseEPSt13_Rb_tree_nodeIS3_E
	movq	%r12, %rdi
	movq	16(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L2566
.L2568:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2563
.L2567:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2564
	.p2align 4,,10
	.p2align 3
.L2565:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2569
	movq	(%rdi), %rax
	addq	$8, %r12
	call	*8(%rax)
	cmpq	%rbx, %r12
	jne	.L2565
.L2570:
	movq	-872(%rbp), %r12
.L2564:
	testq	%r12, %r12
	je	.L2572
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2572:
	movq	-896(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2573
	call	_ZdlPv@PLT
.L2573:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	-952(%rbp), %rdx
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	movq	-520(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-552(%rbp), %rdi
	cmpq	-1048(%rbp), %rdi
	je	.L2574
	call	_ZdlPv@PLT
.L2574:
	movq	-568(%rbp), %rbx
	movq	-576(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2575
	.p2align 4,,10
	.p2align 3
.L2579:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2576
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L2579
.L2577:
	movq	-576(%rbp), %r12
.L2575:
	testq	%r12, %r12
	je	.L2263
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2263:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2932
	movq	-976(%rbp), %rax
	addq	$1064, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2576:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2579
	jmp	.L2577
	.p2align 4,,10
	.p2align 3
.L2559:
	addq	$64, %r12
	cmpq	%r12, %rbx
	jne	.L2556
	jmp	.L2560
	.p2align 4,,10
	.p2align 3
.L2569:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2565
	jmp	.L2570
	.p2align 4,,10
	.p2align 3
.L2431:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2434
	jmp	.L2432
	.p2align 4,,10
	.p2align 3
.L2425:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2428
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2418:
	addq	$8, %r13
	cmpq	%r13, %r12
	jne	.L2414
	jmp	.L2419
	.p2align 4,,10
	.p2align 3
.L2494:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2497
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2407:
	addq	$8, %r13
	cmpq	%r13, %r12
	jne	.L2410
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2488:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2491
	jmp	.L2489
	.p2align 4,,10
	.p2align 3
.L2476:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2479
	jmp	.L2477
	.p2align 4,,10
	.p2align 3
.L2482:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2485
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2533:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2536
	jmp	.L2534
	.p2align 4,,10
	.p2align 3
.L2544:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2540
	jmp	.L2545
	.p2align 4,,10
	.p2align 3
.L2437:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2440
	jmp	.L2438
	.p2align 4,,10
	.p2align 3
.L2443:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L2446
	jmp	.L2444
	.p2align 4,,10
	.p2align 3
.L2925:
	leaq	-504(%rbp), %rsi
	movq	%r14, %rdi
.LEHB96:
	call	_ZN2v88internal6torque12_GLOBAL__N_18ReadFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LEHE96:
	cmpb	$0, -448(%rbp)
	movzbl	-624(%rbp), %eax
	jne	.L2933
	testb	%al, %al
	je	.L2306
	movq	-616(%rbp), %rdi
	leaq	-600(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2934
	call	_ZdlPv@PLT
	movb	$0, -624(%rbp)
	movzbl	-448(%rbp), %eax
.L2301:
	testb	%al, %al
	je	.L2306
	movq	-440(%rbp), %rdi
	leaq	-424(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2306
	call	_ZdlPv@PLT
.L2306:
	cmpb	$0, -512(%rbp)
	je	.L2292
	movq	-504(%rbp), %rdi
	leaq	-488(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2292
	call	_ZdlPv@PLT
	jmp	.L2292
	.p2align 4,,10
	.p2align 3
.L2354:
	movq	8(%r15), %rbx
	pxor	%xmm0, %xmm0
	subq	(%r15), %rbx
	movq	$0, 24(%rax)
	movups	%xmm0, 8(%rax)
	movq	%rbx, %rax
	sarq	$5, %rax
	je	.L2935
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L2936
	movq	%rbx, %rdi
.LEHB97:
	call	_Znwm@PLT
.LEHE97:
	movq	%rax, -992(%rbp)
.L2357:
	movq	-992(%rbp), %rax
	movq	-976(%rbp), %rdx
	movq	%rax, %xmm0
	addq	%rax, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, 24(%rdx)
	movq	%rax, %rbx
	movups	%xmm0, 8(%rdx)
	movq	8(%r15), %rcx
	movq	(%r15), %r13
	movq	%rcx, -968(%rbp)
	cmpq	%r13, %rcx
	je	.L2359
	leaq	-944(%rbp), %rax
	movq	%rax, -984(%rbp)
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2361:
	cmpq	$1, %r12
	jne	.L2363
	movzbl	(%r14), %eax
	movb	%al, 16(%rbx)
.L2364:
	movq	%r12, 8(%rbx)
	addq	$32, %r13
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r13, -968(%rbp)
	je	.L2359
.L2365:
	leaq	16(%rbx), %rdi
	movq	%rdi, (%rbx)
	movq	0(%r13), %r14
	movq	8(%r13), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L2360
	testq	%r14, %r14
	je	.L2937
.L2360:
	movq	%r12, -944(%rbp)
	cmpq	$15, %r12
	jbe	.L2361
	movq	-984(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
.LEHB98:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE98:
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-944(%rbp), %rax
	movq	%rax, 16(%rbx)
.L2362:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-944(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2363:
	testq	%r12, %r12
	je	.L2364
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2359:
	movq	-976(%rbp), %rax
	leaq	48(%rax), %rdi
	movq	%rbx, 16(%rax)
	movq	%rdi, 32(%rax)
	movq	24(%r15), %r12
	movq	32(%r15), %r13
	movq	%r12, %rax
	addq	%r13, %rax
	je	.L2368
	testq	%r12, %r12
	je	.L2938
.L2368:
	movq	%r13, -944(%rbp)
	cmpq	$15, %r13
	ja	.L2939
	cmpq	$1, %r13
	jne	.L2378
	movzbl	(%r12), %eax
	movq	-976(%rbp), %rdx
	movb	%al, 48(%rdx)
.L2379:
	movq	-976(%rbp), %rax
	movq	%r13, 40(%rax)
	movb	$0, (%rdi,%r13)
	movb	$1, (%rax)
	jmp	.L2355
	.p2align 4,,10
	.p2align 3
.L2347:
	testq	%r12, %r12
	jne	.L2940
	movq	%rbx, %rax
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2342:
	testq	%r12, %r12
	jne	.L2941
	movq	%r13, %rax
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2921:
	leaq	-448(%rbp), %r12
	leaq	-784(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
.LEHB99:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE99:
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-784(%rbp), %rax
	movq	%rax, -432(%rbp)
.L2266:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-784(%rbp), %r14
	movq	-448(%rbp), %rax
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2931:
	leaq	-944(%rbp), %rsi
	leaq	-416(%rbp), %rdi
	xorl	%edx, %edx
.LEHB100:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE100:
	movq	%rax, -416(%rbp)
	movq	%rax, %rdi
	movq	-944(%rbp), %rax
	movq	%rax, -400(%rbp)
.L2346:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-944(%rbp), %r12
	movq	-416(%rbp), %rax
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2929:
	leaq	-944(%rbp), %rsi
	leaq	-448(%rbp), %rdi
	xorl	%edx, %edx
.LEHB101:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE101:
	movq	%rax, -448(%rbp)
	movq	%rax, %rdi
	movq	-944(%rbp), %rax
	movq	%rax, -432(%rbp)
.L2341:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-944(%rbp), %r12
	movq	-448(%rbp), %rax
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2922:
	movdqa	-432(%rbp), %xmm7
	movups	%xmm7, -536(%rbp)
	jmp	.L2270
	.p2align 4,,10
	.p2align 3
.L2933:
	movq	-440(%rbp), %rsi
	movq	-432(%rbp), %rdx
	testb	%al, %al
	jne	.L2942
	leaq	-600(%rbp), %rax
	movq	%rax, -616(%rbp)
	leaq	-424(%rbp), %rax
	cmpq	%rax, %rsi
	je	.L2943
	movq	-424(%rbp), %rax
	movq	%rsi, -616(%rbp)
	movq	%rax, -600(%rbp)
.L2303:
	movq	%rdx, -608(%rbp)
	movb	$1, -624(%rbp)
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2378:
	testq	%r13, %r13
	je	.L2379
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2942:
	leaq	-424(%rbp), %rax
	movq	-616(%rbp), %rdi
	cmpq	%rax, %rsi
	je	.L2944
	leaq	-600(%rbp), %r8
	movq	-424(%rbp), %rcx
	cmpq	%r8, %rdi
	je	.L2945
	movq	%rdx, %xmm0
	movq	%rcx, %xmm6
	movq	-600(%rbp), %r8
	movq	%rsi, -616(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -608(%rbp)
	testq	%rdi, %rdi
	je	.L2300
	movq	%rdi, -440(%rbp)
	movq	%r8, -424(%rbp)
.L2298:
	movq	$0, -432(%rbp)
	movb	$0, (%rdi)
	movzbl	-448(%rbp), %eax
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2934:
	movb	$0, -624(%rbp)
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2939:
	movq	-976(%rbp), %rbx
	leaq	-944(%rbp), %rsi
	xorl	%edx, %edx
	leaq	32(%rbx), %rdi
.LEHB102:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
.LEHE102:
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	movq	-944(%rbp), %rax
	movq	%rax, 48(%rbx)
.L2377:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-976(%rbp), %rax
	movq	-944(%rbp), %r13
	movq	32(%rax), %rdi
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2935:
	movq	$0, -992(%rbp)
	jmp	.L2357
.L2943:
	movdqu	-424(%rbp), %xmm5
	movups	%xmm5, -600(%rbp)
	jmp	.L2303
.L2945:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm6
	movq	%rsi, -616(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -608(%rbp)
.L2300:
	movq	%rax, -440(%rbp)
	leaq	-424(%rbp), %rdi
	jmp	.L2298
.L2944:
	testq	%rdx, %rdx
	je	.L2296
	cmpq	$1, %rdx
	je	.L2946
	call	memcpy@PLT
	movq	-432(%rbp), %rdx
	movq	-616(%rbp), %rdi
.L2296:
	movq	%rdx, -608(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-440(%rbp), %rdi
	jmp	.L2298
.L2946:
	movzbl	-424(%rbp), %eax
	movb	%al, (%rdi)
	movq	-432(%rbp), %rdx
	movq	-616(%rbp), %rdi
	jmp	.L2296
.L2316:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
.LEHB103:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2317
.L2315:
	leaq	-352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.LEHE103:
	jmp	.L2317
.L2937:
	leaq	.LC0(%rip), %rdi
.LEHB104:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE104:
.L2923:
	leaq	.LC0(%rip), %rdi
.LEHB105:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE105:
.L2926:
	movq	%r13, %rdi
	jmp	.L2266
.L2936:
.LEHB106:
	call	_ZSt17__throw_bad_allocv@PLT
.LEHE106:
.L2938:
	leaq	.LC0(%rip), %rdi
.LEHB107:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE107:
.L2930:
	leaq	.LC0(%rip), %rdi
.LEHB108:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE108:
.L2932:
	call	__stack_chk_fail@PLT
.L2920:
	leaq	.LC0(%rip), %rdi
.LEHB109:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE109:
.L2927:
	movq	%r13, %rdi
	jmp	.L2285
.L2928:
	leaq	.LC0(%rip), %rdi
.LEHB110:
	call	_ZSt19__throw_logic_errorPKc@PLT
.LEHE110:
.L2940:
	movq	%rbx, %rdi
	jmp	.L2346
.L2941:
	movq	%r13, %rdi
	jmp	.L2341
.L2594:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r13
	jmp	.L2581
.L2607:
	endbr64
	movq	%rax, %r13
	movq	%rdx, %r12
	jmp	.L2324
.L2611:
	endbr64
	movq	%rax, %r13
	movq	%rdx, %r12
	jmp	.L2319
.L2606:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2321
.L2613:
	endbr64
	movq	%rax, %rbx
	jmp	.L2380
.L2612:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2350
.L2603:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2334
.L2615:
	endbr64
	movq	%rax, %rdi
	jmp	.L2369
.L2605:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2328
.L2598:
	endbr64
	movq	%rax, %rbx
	jmp	.L2277
.L2599:
	endbr64
	movq	%rax, %rbx
	jmp	.L2281
.L2593:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2330
.L2600:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2333
.L2604:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2331
.L2601:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2314
.L2595:
	endbr64
	movq	%rax, %rbx
	jmp	.L2375
.L2596:
	endbr64
	movq	%rax, %r12
	jmp	.L2272
.L2609:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r13
	jmp	.L2312
.L2610:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2310
.L2592:
	endbr64
	movq	%rax, %r12
	jmp	.L2584
.L2597:
	endbr64
	movq	%rax, %rbx
	jmp	.L2274
.L2608:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2311
.L2602:
	endbr64
	movq	%rax, %rbx
	movq	%rdx, %r12
	jmp	.L2336
	.section	.gcc_except_table._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE,"a",@progbits
	.align 4
.LLSDA7448:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATT7448-.LLSDATTD7448
.LLSDATTD7448:
	.byte	0x1
	.uleb128 .LLSDACSE7448-.LLSDACSB7448
.LLSDACSB7448:
	.uleb128 .LEHB75-.LFB7448
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L2596-.LFB7448
	.uleb128 0
	.uleb128 .LEHB76-.LFB7448
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L2592-.LFB7448
	.uleb128 0
	.uleb128 .LEHB77-.LFB7448
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L2597-.LFB7448
	.uleb128 0
	.uleb128 .LEHB78-.LFB7448
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L2598-.LFB7448
	.uleb128 0
	.uleb128 .LEHB79-.LFB7448
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L2599-.LFB7448
	.uleb128 0
	.uleb128 .LEHB80-.LFB7448
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L2601-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB81-.LFB7448
	.uleb128 .LEHE81-.LEHB81
	.uleb128 .L2605-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB82-.LFB7448
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L2593-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB83-.LFB7448
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L2600-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB84-.LFB7448
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L2604-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB85-.LFB7448
	.uleb128 .LEHE85-.LEHB85
	.uleb128 .L2601-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB86-.LFB7448
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L2608-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB87-.LFB7448
	.uleb128 .LEHE87-.LEHB87
	.uleb128 .L2610-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB88-.LFB7448
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L2609-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB89-.LFB7448
	.uleb128 .LEHE89-.LEHB89
	.uleb128 .L2606-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB90-.LFB7448
	.uleb128 .LEHE90-.LEHB90
	.uleb128 .L2611-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB91-.LFB7448
	.uleb128 .LEHE91-.LEHB91
	.uleb128 .L2607-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB92-.LFB7448
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L2602-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB93-.LFB7448
	.uleb128 .LEHE93-.LEHB93
	.uleb128 .L2593-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB94-.LFB7448
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L2594-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB95-.LFB7448
	.uleb128 .LEHE95-.LEHB95
	.uleb128 .L2595-.LFB7448
	.uleb128 0
	.uleb128 .LEHB96-.LFB7448
	.uleb128 .LEHE96-.LEHB96
	.uleb128 .L2603-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB97-.LFB7448
	.uleb128 .LEHE97-.LEHB97
	.uleb128 .L2595-.LFB7448
	.uleb128 0
	.uleb128 .LEHB98-.LFB7448
	.uleb128 .LEHE98-.LEHB98
	.uleb128 .L2615-.LFB7448
	.uleb128 0x5
	.uleb128 .LEHB99-.LFB7448
	.uleb128 .LEHE99-.LEHB99
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB100-.LFB7448
	.uleb128 .LEHE100-.LEHB100
	.uleb128 .L2612-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB101-.LFB7448
	.uleb128 .LEHE101-.LEHB101
	.uleb128 .L2593-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB102-.LFB7448
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L2613-.LFB7448
	.uleb128 0
	.uleb128 .LEHB103-.LFB7448
	.uleb128 .LEHE103-.LEHB103
	.uleb128 .L2611-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB104-.LFB7448
	.uleb128 .LEHE104-.LEHB104
	.uleb128 .L2615-.LFB7448
	.uleb128 0x5
	.uleb128 .LEHB105-.LFB7448
	.uleb128 .LEHE105-.LEHB105
	.uleb128 .L2593-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB106-.LFB7448
	.uleb128 .LEHE106-.LEHB106
	.uleb128 .L2595-.LFB7448
	.uleb128 0
	.uleb128 .LEHB107-.LFB7448
	.uleb128 .LEHE107-.LEHB107
	.uleb128 .L2613-.LFB7448
	.uleb128 0
	.uleb128 .LEHB108-.LFB7448
	.uleb128 .LEHE108-.LEHB108
	.uleb128 .L2612-.LFB7448
	.uleb128 0x3
	.uleb128 .LEHB109-.LFB7448
	.uleb128 .LEHE109-.LEHB109
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB110-.LFB7448
	.uleb128 .LEHE110-.LEHB110
	.uleb128 .L2593-.LFB7448
	.uleb128 0x3
.LLSDACSE7448:
	.byte	0
	.byte	0
	.byte	0x1
	.byte	0x7d
	.byte	0x2
	.byte	0
	.align 4
	.long	0

	.long	DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE-.
.LLSDATT7448:
	.section	.text._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE
	.cfi_startproc
	.cfi_personality 0x9b,DW.ref.__gxx_personality_v0
	.cfi_lsda 0x1b,.LLSDAC7448
	.type	_ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE.cold, @function
_ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE.cold:
.LFSB7448:
.L2581:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rdi
	movq	%r13, %r12
	call	_ZN2v88internal6torque21TorqueCompilerOptionsD1Ev
.L2330:
	subq	$1, %r12
	je	.L2947
.L2375:
	movq	-976(%rbp), %rdi
	call	_ZN2v88internal6torque20TorqueCompilerResultD1Ev
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_18LanguageServerDataEEERPNT_5ScopeEv@PLT
	movq	-672(%rbp), %rdx
	movq	-1056(%rbp), %rdi
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque18LanguageServerDataD1Ev
.L2282:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_14TorqueMessagesEEERPNT_5ScopeEv@PLT
	movq	-904(%rbp), %rdx
	movq	-1008(%rbp), %rdi
	movq	%rdx, (%rax)
	call	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED1Ev
.L2278:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_10CurrentAstEEERPNT_5ScopeEv@PLT
	movq	-800(%rbp), %rdx
	movq	-1064(%rbp), %rdi
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque3AstD1Ev
.L2275:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	-952(%rbp), %rdx
	movq	%rbx, %r12
	movq	%rdx, (%rax)
.L2584:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_13SourceFileMapEEERPNT_5ScopeEv@PLT
	movq	-520(%rbp), %rdx
	movq	-1072(%rbp), %rdi
	movq	%rdx, (%rax)
	call	_ZN2v88internal6torque13SourceFileMapD1Ev
	movq	%r12, %rdi
.LEHB111:
	call	_Unwind_Resume@PLT
.LEHE111:
.L2324:
	movq	-656(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2325
	call	_ZdlPv@PLT
.L2325:
	movq	%r13, %rbx
.L2321:
	movq	%r14, %rdi
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev@PLT
.L2314:
	cmpb	$0, -624(%rbp)
	je	.L2333
	movq	-616(%rbp), %rdi
	leaq	-600(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2333
	call	_ZdlPv@PLT
.L2333:
	call	_ZN2v88internal6torque21ContextualVariableTopINS1_17CurrentSourceFileEEERPNT_5ScopeEv@PLT
	movq	-936(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L2330
.L2319:
	movq	-656(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2320
	call	_ZdlPv@PLT
.L2320:
	movq	%r13, %rbx
	jmp	.L2321
.L2350:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2330
	call	_ZdlPv@PLT
	jmp	.L2330
.L2334:
	cmpb	$0, -512(%rbp)
	je	.L2314
	movq	-504(%rbp), %rdi
	leaq	-488(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2314
	call	_ZdlPv@PLT
	jmp	.L2314
.L2947:
	movq	%rbx, %rdi
	call	__cxa_begin_catch@PLT
	call	__cxa_end_catch@PLT
	jmp	.L2583
.L2380:
	movq	-1024(%rbp), %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EED1Ev
	jmp	.L2375
.L2369:
	call	__cxa_begin_catch@PLT
	movq	-992(%rbp), %r12
.L2372:
	cmpq	%rbx, %r12
	jne	.L2948
.LEHB112:
	call	__cxa_rethrow@PLT
.LEHE112:
.L2328:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2330
	call	_ZdlPv@PLT
	jmp	.L2330
.L2948:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2371
	call	_ZdlPv@PLT
.L2371:
	addq	$32, %r12
	jmp	.L2372
.L2277:
	leaq	-928(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal6torque13TorqueMessageESaIS3_EED1Ev
	jmp	.L2278
.L2281:
	leaq	-784(%rbp), %rdi
	call	_ZN2v88internal6torque18LanguageServerDataD1Ev
	jmp	.L2282
.L2331:
	movq	-448(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2333
	call	_ZdlPv@PLT
	jmp	.L2333
.L2272:
	leaq	-576(%rbp), %rdi
	call	_ZN2v88internal6torque13SourceFileMapD1Ev
	movq	%r12, %rdi
.LEHB113:
	call	_Unwind_Resume@PLT
.LEHE113:
.L2312:
	movq	%r12, -424(%rbp)
	movq	-352(%rbp), %rdi
	cmpq	-1088(%rbp), %rdi
	je	.L2313
	call	_ZdlPv@PLT
.L2313:
	movq	-1024(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %r12
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
.L2311:
	movq	-968(%rbp), %rax
	movq	-1080(%rbp), %rdi
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L2314
.L2310:
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	jmp	.L2311
.L2614:
	endbr64
	movq	%rax, %rbx
	call	__cxa_end_catch@PLT
	movq	-976(%rbp), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2375
	call	_ZdlPv@PLT
	jmp	.L2375
.L2274:
	leaq	-896(%rbp), %rdi
	call	_ZN2v88internal6torque3AstD1Ev
	jmp	.L2275
.L2336:
	movq	%r15, %rdi
	call	_ZNK2v88internal6torque14MessageBuilder6ReportEv@PLT
	movq	-512(%rbp), %rdi
	leaq	-496(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2314
	call	_ZdlPv@PLT
	jmp	.L2314
	.cfi_endproc
.LFE7448:
	.section	.gcc_except_table._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE
	.align 4
.LLSDAC7448:
	.byte	0xff
	.byte	0x9b
	.uleb128 .LLSDATTC7448-.LLSDATTDC7448
.LLSDATTDC7448:
	.byte	0x1
	.uleb128 .LLSDACSEC7448-.LLSDACSBC7448
.LLSDACSBC7448:
	.uleb128 .LEHB111-.LCOLDB8
	.uleb128 .LEHE111-.LEHB111
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB112-.LCOLDB8
	.uleb128 .LEHE112-.LEHB112
	.uleb128 .L2614-.LCOLDB8
	.uleb128 0
	.uleb128 .LEHB113-.LCOLDB8
	.uleb128 .LEHE113-.LEHB113
	.uleb128 0
	.uleb128 0
.LLSDACSEC7448:
	.byte	0
	.byte	0
	.byte	0x1
	.byte	0x7d
	.byte	0x2
	.byte	0
	.align 4
	.long	0

	.long	DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE-.
.LLSDATTC7448:
	.section	.text.unlikely._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE
	.section	.text._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE
	.size	_ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE, .-_ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE
	.section	.text.unlikely._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE
	.size	_ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE.cold, .-_ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE.cold
.LCOLDE8:
	.section	.text._ZN2v88internal6torque13CompileTorqueESt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS8_EENS1_21TorqueCompilerOptionsE
.LHOTE8:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE, @function
_GLOBAL__sub_I__ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE:
.LFB13906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13906:
	.size	_GLOBAL__sub_I__ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE, .-_GLOBAL__sub_I__ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6torque13CompileTorqueERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS1_21TorqueCompilerOptionsE
	.weak	_ZTSN2v88internal6torque22TorqueAbortCompilationE
	.section	.rodata._ZTSN2v88internal6torque22TorqueAbortCompilationE,"aG",@progbits,_ZTSN2v88internal6torque22TorqueAbortCompilationE,comdat
	.align 32
	.type	_ZTSN2v88internal6torque22TorqueAbortCompilationE, @object
	.size	_ZTSN2v88internal6torque22TorqueAbortCompilationE, 46
_ZTSN2v88internal6torque22TorqueAbortCompilationE:
	.string	"N2v88internal6torque22TorqueAbortCompilationE"
	.weak	_ZTIN2v88internal6torque22TorqueAbortCompilationE
	.section	.data.rel.ro._ZTIN2v88internal6torque22TorqueAbortCompilationE,"awG",@progbits,_ZTIN2v88internal6torque22TorqueAbortCompilationE,comdat
	.align 8
	.type	_ZTIN2v88internal6torque22TorqueAbortCompilationE, @object
	.size	_ZTIN2v88internal6torque22TorqueAbortCompilationE, 16
_ZTIN2v88internal6torque22TorqueAbortCompilationE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN2v88internal6torque22TorqueAbortCompilationE
	.weak	_ZTVN2v88internal6torque13NullStreambufE
	.section	.data.rel.ro._ZTVN2v88internal6torque13NullStreambufE,"awG",@progbits,_ZTVN2v88internal6torque13NullStreambufE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque13NullStreambufE, @object
	.size	_ZTVN2v88internal6torque13NullStreambufE, 128
_ZTVN2v88internal6torque13NullStreambufE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque13NullStreambufD1Ev
	.quad	_ZN2v88internal6torque13NullStreambufD0Ev
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE5imbueERKSt6locale
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE6setbufEPcl
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE7seekoffElSt12_Ios_SeekdirSt13_Ios_Openmode
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE7seekposESt4fposI11__mbstate_tESt13_Ios_Openmode
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE4syncEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE9showmanycEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE6xsgetnEPcl
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE9underflowEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE5uflowEv
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE9pbackfailEi
	.quad	_ZNSt15basic_streambufIcSt11char_traitsIcEE6xsputnEPKcl
	.quad	_ZN2v88internal6torque13NullStreambuf8overflowEi
	.hidden	_ZTCN2v88internal6torque11NullOStreamE0_So
	.weak	_ZTCN2v88internal6torque11NullOStreamE0_So
	.section	.rodata._ZTCN2v88internal6torque11NullOStreamE0_So,"aG",@progbits,_ZTVN2v88internal6torque11NullOStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal6torque11NullOStreamE0_So, @object
	.size	_ZTCN2v88internal6torque11NullOStreamE0_So, 80
_ZTCN2v88internal6torque11NullOStreamE0_So:
	.quad	136
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-136
	.quad	-136
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal6torque11NullOStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal6torque11NullOStreamE,"awG",@progbits,_ZTVN2v88internal6torque11NullOStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal6torque11NullOStreamE, @object
	.size	_ZTTN2v88internal6torque11NullOStreamE, 32
_ZTTN2v88internal6torque11NullOStreamE:
	.quad	_ZTVN2v88internal6torque11NullOStreamE+24
	.quad	_ZTCN2v88internal6torque11NullOStreamE0_So+24
	.quad	_ZTCN2v88internal6torque11NullOStreamE0_So+64
	.quad	_ZTVN2v88internal6torque11NullOStreamE+64
	.weak	_ZTVN2v88internal6torque11NullOStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal6torque11NullOStreamE,"awG",@progbits,_ZTVN2v88internal6torque11NullOStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal6torque11NullOStreamE, @object
	.size	_ZTVN2v88internal6torque11NullOStreamE, 80
_ZTVN2v88internal6torque11NullOStreamE:
	.quad	136
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6torque11NullOStreamD1Ev
	.quad	_ZN2v88internal6torque11NullOStreamD0Ev
	.quad	-136
	.quad	-136
	.quad	0
	.quad	_ZTv0_n24_N2v88internal6torque11NullOStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal6torque11NullOStreamD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	1065353216
	.section	.data.rel.ro,"aw"
	.align 8
.LC3:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.quad	7594807821157102948
	.quad	8371740277788796268
	.hidden	DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE
	.weak	DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE
	.section	.data.rel.local.DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE,"awG",@progbits,DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE,comdat
	.align 8
	.type	DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE, @object
	.size	DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE, 8
DW.ref._ZTIN2v88internal6torque22TorqueAbortCompilationE:
	.quad	_ZTIN2v88internal6torque22TorqueAbortCompilationE
	.hidden	DW.ref.__gxx_personality_v0
	.weak	DW.ref.__gxx_personality_v0
	.section	.data.rel.local.DW.ref.__gxx_personality_v0,"awG",@progbits,DW.ref.__gxx_personality_v0,comdat
	.align 8
	.type	DW.ref.__gxx_personality_v0, @object
	.size	DW.ref.__gxx_personality_v0, 8
DW.ref.__gxx_personality_v0:
	.quad	__gxx_personality_v0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
